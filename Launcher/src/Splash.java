import javax.imageio.ImageIO;
import javax.swing.*;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
 
@SuppressWarnings("serial")
public class Splash extends JFrame {


	private static String downloadUrl = "http://www.soulplayps.com/excludecf/SoulSplit.jar";
	private static String fileName = "SoulSplit.jar";
	private static String serverName = "SoulSplit";
	private static String backgroundImageUrl = "http://www.soulplayps.com/excludecf/splash.png";
	private static String saveDirectory = System.getProperty("user.home")+"/soulplaycache/";
 
	public static URL url;
    private JLabel imglabel;
    private ImageIcon img;
    private BufferedImage image;
    private static JProgressBar pbar;
    Thread t = null;
 
    public Splash() {
        super("Splash");
        
        File file = new File(saveDirectory + fileName);
        
		try {
			url = new URL(downloadUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
        setSize(543, 391);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setUndecorated(true);
        
        try {
        	
        	URL url1 = new URL(backgroundImageUrl);
        	
        	HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
        	connection.addRequestProperty("User-Agent", "Mozilla/4.76");
        	image = ImageIO.read(connection.getInputStream());
        	img = new ImageIcon(image);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        
        imglabel = new JLabel(img);
        add(imglabel);
        setLayout(null);
        pbar = new JProgressBar();
        pbar.setMinimum(0);
        pbar.setMaximum(100);
        pbar.setStringPainted(true);
        pbar.setForeground(Color.LIGHT_GRAY);
        imglabel.setBounds(0, 0, 543, 391);
        add(pbar);
        pbar.setPreferredSize(new Dimension(310, 30));
        pbar.setBounds(70, 320, 404, 20);


        try {
        	if (!file.exists()) {
        		 File dir = new File(saveDirectory);
        		 if (!dir.exists()) {
        				if (dir.mkdir()) {
//        					JOptionPane.showMessageDialog(this, "Directory is created!");
        				} else {
        					JOptionPane.showMessageDialog(this, "Failed to create directory! Report this error to the Staff please.");
        				}
        			}
        	}
            if (file.exists()) {
            	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            	connection.addRequestProperty("User-Agent", "Mozilla/4.76");
            	connection.connect();
            	
    			long time = connection.getLastModified();
    			
    			int localFileSize = (int)file.length();
    			
        		int onlineFileLength = connection.getContentLength();
        		
    			//System.out.println("file size = "+ localFileSize + " online Size:"+ getOnlineFileSize(connection) + " length:"+onlineFileLength);
        		
    			if (time > file.lastModified() || localFileSize == 0 || (onlineFileLength > 0 && localFileSize != onlineFileLength)) {
                    if (!startDialogue()) {
                    	startApplication();
                        return;
                    }
    			} else {
                    setVisible(true);
                    Thread.sleep(500);
                    startApplication();
                    return;
                }
    		}
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        Thread t = new Thread() {
 
            public void run() {
            	OutputStream dest = null;
            	HttpURLConnection download;
            	InputStream readFileToDownload = null;
            	try {
            		dest = new BufferedOutputStream(new FileOutputStream(saveDirectory + fileName)); 
            		download = (HttpURLConnection) url.openConnection();
            		download.addRequestProperty("User-Agent", "Mozilla/4.76");
            		readFileToDownload = download.getInputStream();
            		byte[] data = new byte[1024];
            		int numRead;
            		long numWritten = 0;
            		int length = download.getContentLength();
            		while ((numRead = readFileToDownload.read(data)) != -1) {
            			dest.write(data, 0, numRead);
            			numWritten += numRead;
            			int percent = (int)(((double)numWritten / (double)length) * 100D);
            			pbar.setValue(percent);
            			pbar.setString(""+(percent < 99 ? "Downloading "+serverName+" - "+percent+"%" : "Complete")+"");
            		}
            	} catch (Exception exception) {
            		exception.printStackTrace();
            	} finally {
            		try {
            			if (readFileToDownload != null)
            				readFileToDownload.close();
            			if (dest != null)
            				dest.close();
            			Thread.sleep(1000L);
            			startApplication();
            		} catch (IOException | InterruptedException ioe) {
            				
            		}
            	}
            }
        };
        t.start();
    }
    
    public boolean startDialogue() {
        setVisible(true);
        int selection = JOptionPane.showConfirmDialog(null, "An update is available. Do you wish to download?", "Update Available", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        return selection == JOptionPane.OK_OPTION;
    }


    /**
     * Launches the downloaded Jar file and closes the progress bar
     */
    public static void startApplication() {
    	try {
    		
    		Runtime r = Runtime.getRuntime();
    		Process p =null;
    		String[] cmdarray = new String[3];
    		cmdarray[0] = "java";
    		cmdarray[1] = "-jar";
    		cmdarray[2] = (saveDirectory + fileName);
    		p = r.exec(cmdarray);
	        
			Thread.sleep(1000L);
			System.exit(0);
		} catch (Throwable t) {
			JOptionPane.showMessageDialog(null, "error runTime");
			t.printStackTrace();
		}
    }


}