package com.zarketh.loginserver.net;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.zarketh.loginserver.net.SQLDatabase.DatabaseConnection.State;

/**
 * Handles a database connection.
 * 
 * @author Ultimate1
 */

public class SQLDatabase implements Runnable {

	/**
	 * The logger.
	 */
	private static Logger logger = Logger
			.getLogger(SQLDatabase.class.getName());

	/**
	 * The alias of the database we are connection to.
	 */
	@SuppressWarnings("unused")
	private String alias;

	/**
	 * The database connection pool.
	 */
	public final DatabaseConnection[] pool;

	/**
	 * The work service.
	 */
	private final ExecutorService workService;

	/**
	 * Queries that failed execution.
	 */
	private final Queue<Query> failedQueries = new LinkedList<Query>();

	/**
	 * The interval at which connections are pinged.
	 */
	private long pingInterval = 300000;

	/**
	 * Sets the interval at which connections are pinged in minutes.
	 * 
	 * @param pingInterval
	 */
	public void setPingInterval(int pingInterval) {
		this.pingInterval = pingInterval * 60000;
	}

	/**
	 * Determines if the <code>DatabaseManager</code> is running.
	 */
	private boolean isRunning = true;

	/**
	 * Initialise the database manager.
	 * 
	 * @param username
	 * @param password
	 * @param url
	 * @param poolSize
	 */
	public SQLDatabase(String username, String password, String url,
			int poolSize) {
		pool = new DatabaseConnection[poolSize];
		workService = Executors.newFixedThreadPool(pool.length);
		for (int id = 0; id < pool.length; id++) {
			pool[id] = new DatabaseConnection(id, username, password, url);
		}
		new Thread(this, "DatabaseConnection").start();
	}

	/**
	 * Connect all the connections.
	 * 
	 * @param alias
	 */
	public void initialise(String alias) {
		logger.info("Connecting to " + alias + " database..");
		for (DatabaseConnection poolConnection : pool) {
			if (!poolConnection.connect()) {
				continue;
			}
			poolConnection.setState(State.IDLE);
			logger.info("Successfully connected to " + alias + " database!");
		}
		this.alias = alias;
	}

	/**
	 * Shuts down the database manager.
	 */
	public void shutdown() {
		isRunning = false;
		failedQueries.clear();
		if (workService != null) {
			workService.shutdown();
		}
		for (DatabaseConnection connection : pool) {
			connection.close();
		}
	}

	/**
	 * Gets the first available connection from the pool.
	 * 
	 * @return
	 */
	public DatabaseConnection getPooledConnection() {
		for (DatabaseConnection connection : pool) {
			if (connection.getState().equals(State.IDLE)) {
				return connection;
			}
		}
		return null;
	}

	/**
	 * Adds a failed query to the list.
	 * 
	 * @param query
	 */
	public void addFailedQuery(Query query) {
		synchronized (this) {
			failedQueries.add(query);
		}
	}

	/**
	 * Executes a database query.
	 * 
	 * @param query
	 */
	public ResultSet executeQuery(String query) {
		ResultSet result = null;
		if (!query.toLowerCase().startsWith("select")) {
			workService.submit(new Query(query));
		} else {
			Future<?> future = workService.submit(new Query(query));
			try {
				result = (ResultSet) future.get();
			} catch (Exception e) {
				logger.log(Level.WARNING, "Error executing query!", e);
			}
		}
		return result;
	}

	@Override
	public void run() {
		while (isRunning) {

			// Sleep a little..
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				continue;
			}

			// Ping the connections in the pool.
			for (DatabaseConnection connection : pool) {
				if (System.currentTimeMillis() - connection.lastPing <= pingInterval) {
					continue;
				}
				if (!connection.ping()) {
					boolean reconnected = connection.connect();
					if (reconnected) {
						connection.setState(State.IDLE);
					}
				}
			}

			// Execute queries that previously failed.
			synchronized (this) {
				if (!failedQueries.isEmpty()) {
					Query query;
					while ((query = failedQueries.poll()) != null) {
						workService.submit(query);
					}
				}
			}

		}

	}

	/**
	 * Represents a connection in the pool.
	 * 
	 * @author Ultimate1
	 */
	public static class DatabaseConnection {

		/**
		 * The id of the pool connection.
		 */
		@SuppressWarnings("unused")
		private final int id;

		/**
		 * The database username.
		 */
		private final String username;

		/**
		 * The database password.
		 */
		private final String password;

		/**
		 * The url of the database.
		 */
		private final String url;

		/**
		 * The database connection.
		 */
		private Connection connection;

		/**
		 * The last time the connection was pinged.
		 */
		private long lastPing = System.currentTimeMillis();

		/**
		 * The sate of the connection.
		 */
		private State state = State.INACTIVE;

		/**
		 * The state of the connection.
		 * 
		 * @author Ultimate1
		 */
		public static enum State {
			IDLE, INACTIVE, BUSY,
		}

		/**
		 * @return the connection.
		 */
		public Connection getConnection() {
			return connection;
		}

		/**
		 * Initialises the MySQL driver and database settings.
		 * 
		 * @param id
		 * @param username
		 * @param password
		 * @param url
		 */
		public DatabaseConnection(int id, String username, String password,
				String url) {
			this.id = id;
			this.username = username;
			this.password = password;
			this.url = "jdbc:mysql://" + url;
		}

		/**
		 * @return the state of the connection.
		 */
		public State getState() {
			synchronized (this) {
				return state;
			}
		}

		/**
		 * Sets the state of the connection.
		 * 
		 * @param state
		 */
		public void setState(State state) {
			synchronized (this) {
				this.state = state;
			}
		}

		/**
		 * Attempts to connect to the database.
		 * 
		 * @return
		 */
		public boolean connect() {
			this.setState(State.INACTIVE);
			try {
				connection = DriverManager.getConnection(url, username,
						password);
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Error connecting to MySQL database ("
						+ url + ") !", e);
				return false;
			}
			return true;
		}

		/**
		 * Closes the datbase connection.
		 */
		public void close() {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.log(Level.WARNING,
							"Error closing database connection", e);
				}
				setState(State.INACTIVE);
			}
		}

		/**
		 * @return true if the ping was successful.
		 */
		public boolean ping() {
			lastPing = System.currentTimeMillis();
			if (connection != null) {
				try {
					connection.prepareStatement("SELECT 1").executeQuery();
				} catch (SQLException e) {
					return false;
				}
				return true;
			}
			return false;
		}

	}

	/**
	 * Represents a database query.
	 * 
	 * @author Ultimate1
	 */
	public class Query implements Callable<ResultSet> {

		/**
		 * The query to be executed.
		 */
		private final String query;

		/**
		 * Initialise the query.
		 * 
		 * @param query
		 */
		private Query(String query) {
			this.query = query;
		}

		@Override
		public String toString() {
			return query;
		}

		/**
		 * Executes the query.
		 * 
		 * @param dbc
		 * @return
		 */
		@Override
		public ResultSet call() {
			PreparedStatement statement = null;
			boolean isUpdating = !query.toLowerCase().startsWith("select");
			DatabaseConnection pooledConnection = getPooledConnection();
			if (pooledConnection == null) {
				if (isUpdating) {
					addFailedQuery(this);
				}
				return null;
			}
			pooledConnection.setState(State.BUSY);
			try {
				statement = pooledConnection.getConnection().prepareStatement(
						query);
				if (isUpdating) {
					statement.executeUpdate();
				} else {
					return statement.executeQuery();
				}
			} catch (SQLException e) {
				if (isUpdating) {
					addFailedQuery(this);
				}
			} finally {
				if (statement != null && isUpdating) {
					try {
						statement.close();
					} catch (SQLException e) {
						logger.log(Level.WARNING, "Error closing statement", e);
					}
				}
				pooledConnection.setState(State.IDLE);
			}
			return null;
		}

	}

}