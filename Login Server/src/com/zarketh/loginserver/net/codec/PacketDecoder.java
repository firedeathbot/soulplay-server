package com.zarketh.loginserver.net.codec;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketManager;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.ReferenceCountUtil;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.logging.Logger;

/**
 * Packet decoder.
 *
 * @author Ultimate1
 * @author Jire - fixed packet fragmentation.
 */
public final class PacketDecoder extends ByteToMessageDecoder {
	
	private static Logger logger = Logger.getLogger(PacketDecoder.class.getName());
	
	@Override
	public void channelRegistered(ChannelHandlerContext ctx) {
		logger.info("Connection accepted from " + ctx.channel().remoteAddress() + ".");
	}
	
	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) {
		NodeManager.getInstance().pushTask(new Runnable() {
			Node node = ctx.channel().attr(Node.NODE_ATTRIBUTE).get();
			
			@Override
			public void run() {
				if (node != null) {
					NodeManager.getInstance().unregister(node);
				}
			}
		});
	}
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> list) throws Exception {
		String host = ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();
		if (!allowedIp(host)) {
			ctx.close();
			return;
		}
		
		while (in.readableBytes() >= 2) {
			int packetLength = in.readShort();
			if (packetLength < 0 || in.readableBytes() < packetLength) {
				rewind(in, 2);
				return;
			}
			
			final ByteBuf buffer = ctx.alloc().buffer(packetLength);
			buffer.writeBytes(in, packetLength);
			
			final Packet packet = new Packet(buffer);
			NodeManager.getInstance().pushTask(new Runnable() {
				@Override
				public void run() {
					try {
						Node node = ctx.channel().attr(Node.NODE_ATTRIBUTE).get();
						if (node != null) {
							PacketManager.handle(packet, node);
						} else {
							int opcode = packet.readByte();
							if (opcode == 1) {
								int nodeId = packet.readByte();
								String authKey = packet.readString();
								if (!authKey.equals(Server.getPassword())) {
									ctx.writeAndFlush(new Packet(
											PacketsToServerConfig.CONNECTED_TO_GAMESERVER_RESPONSE).writeByte(0).finish())
											.addListener(ChannelFutureListener.CLOSE);
								} else {
									node = new Node(nodeId, ctx.channel());
									if (!NodeManager.getInstance().register(node)) {
										ctx.writeAndFlush(new Packet(PacketsToServerConfig.CONNECTED_TO_GAMESERVER_RESPONSE)
												.writeByte(1).finish()).addListener(ChannelFutureListener.CLOSE);
									} else {
										ctx.writeAndFlush(new Packet(PacketsToServerConfig.CONNECTED_TO_GAMESERVER_RESPONSE)
												.writeByte(2).finish(), ctx.voidPromise());
										ctx.channel().attr(Node.NODE_ATTRIBUTE).set(node);
									}
								}
							}
						}
					} finally {
						ReferenceCountUtil.release(buffer);
					}
				}
			});
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
	
	private static void rewind(ByteBuf buf, int bytes) {
		buf.readerIndex(buf.readerIndex() - bytes);
	}
	
	private static boolean allowedIp(String ip) {
		switch (ip) {
			case "0.0.0.0":
			case "127.0.0.1":
			case "213.32.64.180":
			case "97.88.160.11":
				return true;
		}
		return false;
	}
	
}
