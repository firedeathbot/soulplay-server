package com.zarketh.loginserver.net.slack.wrapper;

import com.ullink.slack.simpleslackapi.SlackUser;

public class SlackNotification {

	String message;
	String user;
	String type;
	String reason = "None";

	public SlackNotification(String message, String user, String reason) {
		this.message = message;
		this.user = user;
		this.type = (message.startsWith("lof") ? "UUID" : (message.contains(".") ? "IP" : "Username"));
		if (reason != null) {
			this.reason = reason;
		}
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getType() {
		return type;
	}
	
	public String getReason() {
		return reason;
	}
}
