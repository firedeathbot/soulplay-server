package com.zarketh.loginserver.net.slack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.ullink.slack.simpleslackapi.SlackPreparedMessage;
import com.ullink.slack.simpleslackapi.SlackUser;
import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.slack.wrapper.SlackNotification;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.impl.PunishPlayer.Punishment;
import com.zarketh.loginserver.packet.impl.PunishPlayer.RemovePunishment;
import com.zarketh.loginserver.util.HttpPost;
import com.zarketh.loginserver.util.Utils;

public class SlackMessageBuilder {
	
	public static SlackMessage buildExceptionMessage(Class<?> location, Throwable e) {
		SlackMessage sm = new SlackMessage();
		com.zarketh.loginserver.net.slack.SlackAttachment sa = new SlackAttachment();
		SlackField classField = new SlackField();
		SlackField exceptionField = new SlackField();
		SlackField stackTraceField = new SlackField();
		SlackField serverField = new SlackField();
		StringBuilder stackTrace = new StringBuilder();
		
		for (StackTraceElement s : e.getStackTrace()) {
			stackTrace.append(s.toString()+"\n");
		}
		
		sa.setColor("#FF0000");
		sa.setPretext("");
		
		classField.setTitle("Class:").setValue(location.toString());
		exceptionField.setTitle("Exception:").setValue(e.toString());
		stackTraceField.setTitle("Stack Trace:").setValue(stackTrace.toString());
		serverField.setTitle("Server:").setValue("Login Server");
		sa.setFallback("Exception in "+e.getClass().toString());
		
		sa.addFields(classField).addFields(exceptionField).addFields(stackTraceField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText(""); 
		sm.setChannel("#exceptions");
		sm.setUsername("Exceptor");
		sm.setIcon(":exclamation:");
		return sm;
	}
	
	public static SlackMessage buildSlackMessage(String message) {
		SlackMessage sm = new SlackMessage();
		sm.setText(message); 
		sm.setChannel("#general");
		sm.setUsername("SoulBot");
		sm.setIcon(":sp:");
		return sm;
	}
	
	public static SlackPreparedMessage buildSimpleMessage(String message) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		builder.withMessage(message);
		return builder.build();
	}

	public static SlackPreparedMessage buildPlayerOnlineMessage(SlackUser sender) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		int totalCount = 0;
		int world = 1;
		for (Entry<Integer, Node> entry : Server.getNodeManager().getNodes().entrySet()) {
			int playerCount = entry.getValue().getPlayerCount();
			if (playerCount > 0) {
				sa.addField("World "+(world), Integer.toString(playerCount), true);
				totalCount += playerCount;
			}
			world++;
		}
		sa.setColor("#0000FF");
		builder.addAttachment(sa);
		builder.withMessage("There's " + totalCount + " players online, @" + sender.getUserName());
		return builder.build();
	}
	
	public static SlackPreparedMessage buildStaffOnlineMessage(SlackUser sender) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		int totalCount = 0;
		int world = 1;
		for (Entry<Integer, Node> entry : Server.getNodeManager().getNodes().entrySet()) {
			int staffCount = 0;
			List<String> staffMembers = new ArrayList<>();
			for (Player p : entry.getValue().getPlayers()) {
				if ((p.getPrimaryRights() >= 1 && p.getPrimaryRights() < 4) || p.getPrimaryRights() == 7) {
					staffCount++;
					staffMembers.add(p.getUsername());
				}
			}
			if (staffCount > 0) {
			sa.addField("World "+(world), Integer.toString(staffCount), true);
			sa.addField("Staff", Utils.implode(", ", staffMembers), false);
			totalCount += staffCount;
			}
			world++;
		}
		sa.setColor("#0000FF");
		builder.addAttachment(sa);
		builder.withMessage("There's " + totalCount + " staff members online, @" + sender.getUserName());
		return builder.build();
	}
	
	public static SlackPreparedMessage buildPlayersOnWorldMessage(SlackUser sender, String worldString) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		int world = Integer.parseInt(worldString);
		StringBuilder message = new StringBuilder();
		message.append("Players online on world " + world + ":\n");
		for (Entry<Integer, Node> entry : Server.getNodeManager().getNodes().entrySet()) {
			if (entry.getValue().getNodeId() == world) {
				for (Player p : entry.getValue().getPlayers()) {
					if ((p.getPrimaryRights() >= 1 && p.getPrimaryRights() < 4) || p.getPrimaryRights() == 7) {
						message.append(p.getUsername() + " :sp: \n");
					} else {
						message.append(p.getUsername() + "\n");
					}
				}
			}
		}
		builder.withMessage(message.toString());
		return builder.build();
	}
	
	public static SlackPreparedMessage buildEvidenceMessage(String player, Punishment punishment, int duration, String reason, String mod) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		
		sa.setColor("#14CCA2");
		sa.setPretext("");
		
		sa.addField("Player:", player, true);
		sa.addField("Offense:", punishment.getDesc(), true);
		sa.addField("Duration:", Integer.toString(duration), true);
		sa.addField("Reason:", reason, true);
		sa.addField("Issuer:", mod, true);
		sa.setFallback("Evidence for: "+player);
		builder.addAttachment(sa);
		
		return builder.build();
	}
	
	public static SlackPreparedMessage buildEvidenceMessage(String player, RemovePunishment punishment, int duration, String reason, String mod) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		
		sa.setColor("#6cd0cb");
		sa.setPretext("");
		
		sa.addField("Player:", player, true);
		sa.addField("Offense:", punishment.getDesc(), true);
		sa.addField("Duration:", Integer.toString(duration), true);
		sa.addField("Reason:", reason, true);
		sa.addField("Issuer:", mod, true);
		sa.setFallback("Evidence for: "+player);
		builder.addAttachment(sa);
		
		return builder.build();
	}

	public static SlackPreparedMessage buildNotificationAddedMessage(SlackNotification sn) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		
		sa.setColor("#00FF00");
		sa.setPretext("");
		
		sa.setFallback(sn.getMessage() + "("+sn.getType()+") added by " + sn.getUser());
		sa.addField("Value:", sn.getMessage(), true);
		sa.addField("Type:", sn.getType(), true);
		sa.addField("Issuer:", sn.getUser(), true);
		sa.addField("Reason:", sn.getReason(), true);
		builder.addAttachment(sa);
		builder.withMessage("Added entry:");
		return builder.build();
	}
	
	public static SlackPreparedMessage buildNotificationRemovedMessage(SlackNotification sn) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		
		sa.setColor("#6cd0cb");
		sa.setPretext("Removed Entry");
		
		sa.setFallback(sn.getMessage() + "("+sn.getType()+") removed by " + sn.getUser());
		sa.addField("Value:", sn.getMessage(), true);
		sa.addField("Type:", sn.getType(), true);
		sa.addField("Reason", sn.getReason(), true);
		sa.addField("Issuer:", sn.getUser(), true);
		builder.addAttachment(sa);
		builder.withMessage("");
		return builder.build();
	}
	
	public static SlackPreparedMessage buildLoginMessage(SlackNotification sn, String playername, int node) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		
		sa.setColor("#FF0000");
		sa.setPretext("Tracked Player logged in");
		
		sa.setFallback(sn.getMessage() + "("+sn.getType()+") logged in");
		sa.addField("Value:", sn.getMessage(), true);
		sa.addField("Type:", sn.getType(), true);
		sa.addField("Username:", playername, true);
		sa.addField("Added by:", sn.getUser(), true);
		sa.addField("Reason", sn.getReason(), true);
		sa.addField("Server: ", Integer.toString(node), true);
		builder.addAttachment(sa);
		builder.withMessage("");
		return builder.build();
	}
	
}
