package com.zarketh.loginserver.net.slack;

import com.zarketh.loginserver.Server;

public class SlackThread {
	
	public SlackThread() throws InterruptedException {
		Server.getSlackSession().addMessagePostedListener(new SoulBotMessagePostedListener());
	}
}
