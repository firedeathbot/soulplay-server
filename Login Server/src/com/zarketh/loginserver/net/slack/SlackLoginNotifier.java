package com.zarketh.loginserver.net.slack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.SplitPaneUI;

import com.ullink.slack.simpleslackapi.SlackPreparedMessage;
import com.ullink.slack.simpleslackapi.SlackUser;
import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.slack.wrapper.SlackNotification;

public class SlackLoginNotifier {
	
	public static List<SlackNotification> notificationList = new ArrayList<>();
	
	public static void addToNotificationList(String message, SlackUser user, String reason) {
		SlackNotification sn = new SlackNotification(message.toLowerCase(), user.getUserName().toLowerCase(), reason);
		notificationList.add(sn);
		saveList(notificationList);
		Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("tracing"), SlackMessageBuilder.buildNotificationAddedMessage(sn));
	}
	
	public static void removeFromNotificationList(String message, SlackUser user) {
		SlackNotification sn = null;
		for (SlackNotification not : notificationList) {
			if (not.getMessage().equalsIgnoreCase(message)) {
				sn = not;
				notificationList.remove(not);
				saveList(notificationList);
				break;
			}
		}
		if (sn != null) {
			Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("tracing"), SlackMessageBuilder.buildNotificationRemovedMessage(sn));
		} else {
			Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("tracing"), SlackMessageBuilder.buildSimpleMessage("Could not remove " +message));
		}
	}
	
	public static void displayCurrentNotifications() {
		StringBuilder sb = new StringBuilder();
		if (!notificationList.isEmpty()) {
			sb.append("Current entries are:\n");
			for (SlackNotification not : notificationList) {
				sb.append(not.getMessage() + "("+not.getType()+") , Reason: " + not.getReason() + " Added by: " + not.getUser() +"\n");
			}
			Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("tracing"), SlackMessageBuilder.buildSimpleMessage(sb.toString()));
		}
	}
	
	public static void checkNotificationEntry(String username, int node, String... message) {
		for (String mess : message) {
			for (SlackNotification not : notificationList) {
				if (not.getMessage().equalsIgnoreCase(mess)) {
					sendLoginNotification(not, username, node);
					return;
				}
			}
		}
	}
	
	public static void loadListOnStartup() {
		notificationList = loadList();
	}
	
	private static void sendLoginNotification(SlackNotification not, String username, int node) {
		Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("tracing"), SlackMessageBuilder.buildLoginMessage(not, username, node));
	}
	
	private static void saveList(List<SlackNotification> notificationList) {
	    String fileName = "tracing.txt";
	
	    try {
	        FileWriter fileWriter = new FileWriter(fileName);
	
	        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	
	        for (SlackNotification sn : notificationList) {
	        	bufferedWriter.write(sn.getMessage().replaceAll(" ", "_") + " " + sn.getUser() + " " + sn.getReason());
	        	bufferedWriter.newLine();
	        }
	        bufferedWriter.close();
	    }
	    catch(IOException ex) {
	        ex.printStackTrace();
	    }
	}
	
	private static List<SlackNotification> loadList() {
		String fileName = "tracing.txt";

        String line = null;

        List<SlackNotification> notificationList = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
            	String[] splittedLine = line.split(" ", 3); 
                String message = splittedLine[0].replaceAll("_", " ");
                String user = splittedLine[1];
                String reason = splittedLine[2];
                SlackNotification sn = new SlackNotification(message, user, reason);
                notificationList.add(sn);
            }   
            Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("tracing"), new SlackPreparedMessage.Builder().withMessage("Loaded " + notificationList.size() + " names for the tracing list.").build());
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
        	ex.printStackTrace();
        }
        catch(IOException ex) {
        	ex.printStackTrace();
        }
        return notificationList;
    }
}
