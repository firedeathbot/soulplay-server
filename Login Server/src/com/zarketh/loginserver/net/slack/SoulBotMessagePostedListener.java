package com.zarketh.loginserver.net.slack;

import com.ullink.slack.simpleslackapi.SlackPreparedMessage;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.node.PunishmentHandler;
import com.zarketh.loginserver.packet.impl.PunishPlayer;
import com.zarketh.loginserver.packet.impl.PunishPlayer.Punishment;
import com.zarketh.loginserver.packet.impl.PunishPlayer.RemovePunishment;

public class SoulBotMessagePostedListener implements SlackMessagePostedListener {

	@Override
	public void onEvent(SlackMessagePosted event, SlackSession session) {
		if (event.getSender().getId().equals(session.sessionPersona().getId())) {
			return;
		}
		String[] message = event.getMessageContent().toLowerCase().split(" ", 3);
		String startCommand = message[0];
		boolean direct = event.getChannel().isDirect();
		if (startCommand.startsWith("<@U15BSJXJN>".toLowerCase()) || direct) {
			if ("players".equals(message[1]) || (direct && "players".equals(message[0]))) {
				session.sendMessage(event.getChannel(), SlackMessageBuilder.buildPlayerOnlineMessage(event.getSender()));
			} else if ("staff".equals(message[1]) || (direct && "staff".equals(message[0]))) {
				session.sendMessage(event.getChannel(), SlackMessageBuilder.buildStaffOnlineMessage(event.getSender()));
			}
			if ("admins".equals(event.getChannel().getName())) {
				if ("names".equals(message[1])) {
					if(message.length > 2) {
						session.sendMessage(event.getChannel(), SlackMessageBuilder.buildPlayersOnWorldMessage(event.getSender(), message[2]));
					}
				}
			}
		} else if (event.getChannel().getName().equals("possible-bots")) {
			message = event.getMessageContent().split(" ", 3);
			if ("names".equals(message[0].toLowerCase())) {
				session.sendMessage(event.getChannel(), SlackMessageBuilder.buildPlayersOnWorldMessage(event.getSender(), message[1]));
				return;
			}
		} else if (event.getChannel().getName().equals("tracing")) {
			message = event.getMessageContent().split(" ", 3);
			if ("add".equals(message[0].toLowerCase())) {
				SlackLoginNotifier.addToNotificationList(message[1].replaceAll("_", " "), event.getSender(), message[2]);
				return;
			} else if ("remove".equals(message[0].toLowerCase())) {
				SlackLoginNotifier.removeFromNotificationList(message[1].replaceAll("_", " "), event.getSender());
				return;
			} else if ("list".equals(message[0].toLowerCase())) {
				SlackLoginNotifier.displayCurrentNotifications();
				return;
			}
		} else if (event.getChannel().getName().equals("punishment")) {
			message = event.getMessageContent().split(" ", 3);
			if ("remove".equals(message[0])) {
				if (PunishmentHandler.deleteRecord(message[1])) {
					session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage(message[1] + " has been removed from the punishment database").build());
				} else {
					session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Error :(").build());
				}
			}
			Player player = Player.getPlayer(message[1].replace("_", " "));
			switch(message[0]) {
				case "mute":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishmentHandler.punishPlayer(player, Punishment.MUTE_NAME.getPunishmentType(), 0, message[2], event.getSender().getUserName());
					session.sendMessage(session.findChannelByName("evidence"), SlackMessageBuilder.buildEvidenceMessage(player.getUsername(), Punishment.MUTE_NAME, 0, message[2], event.getSender().getUserName()));
					break;
				case "ipmute":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishmentHandler.punishPlayer(player, Punishment.MUTE_IP.getPunishmentType(), 0, message[2], event.getSender().getUserName());
					session.sendMessage(session.findChannelByName("evidence"), SlackMessageBuilder.buildEvidenceMessage(player.getUsername(), Punishment.MUTE_IP, 0, message[2], event.getSender().getUserName()));
					break;
				case "macmute":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishmentHandler.punishPlayer(player, Punishment.MUTE_UUID.getPunishmentType(), 0, message[2], event.getSender().getUserName());
					session.sendMessage(session.findChannelByName("evidence"), SlackMessageBuilder.buildEvidenceMessage(player.getUsername(), Punishment.MUTE_UUID, 0, message[2], event.getSender().getUserName()));
					break;
				case "unmute":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishmentHandler.freePlayer(player.getUsername(), Punishment.MUTE_NAME.getPunishmentType());
					session.sendMessage(session.findChannelByName("evidence"), SlackMessageBuilder.buildEvidenceMessage(player.getUsername(), RemovePunishment.UNMUTED_NAME, 0, message[2], event.getSender().getUserName()));
					break;
				case "unipmute":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishmentHandler.freePlayer(player.getUsername(), Punishment.MUTE_IP.getPunishmentType());
					session.sendMessage(session.findChannelByName("evidence"), SlackMessageBuilder.buildEvidenceMessage(player.getUsername(), RemovePunishment.UNMUTED_NAME, 0, message[2], event.getSender().getUserName()));
					break;
				case "unuuidmute":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishmentHandler.freePlayer(player.getUsername(), Punishment.MUTE_UUID.getPunishmentType());
					session.sendMessage(session.findChannelByName("evidence"), SlackMessageBuilder.buildEvidenceMessage(player.getUsername(), RemovePunishment.UNMUTED_NAME, 0, message[2], event.getSender().getUserName()));
					break;
				case "kick":
					if (player == null) {
						session.sendMessage(event.getChannel(), new SlackPreparedMessage.Builder().withMessage("Player is currently offline").build());
						return;
					}
					PunishPlayer.kickPlayer(player.getNode(), player.getUsername());
					break;
			}
		} else {
			return;
		}
	}
}
