package com.zarketh.loginserver.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.ReferenceCountUtil;

/**
 * Represents a packet.
 *
 * @author Ultimate1
 */

public class Packet {
	
	private final ByteBuf content;
	
	public Packet(int opcode, ByteBufAllocator allocator) {
		content = allocator.buffer();
		this.writeByte(opcode);
		if (opcode == 0)
			System.out.println("created new packet opcode 0");
	}
	
	public Packet(int opcode) {
		this(opcode, ByteBufAllocator.DEFAULT);
	}
	
	public Packet(ByteBuf inBuffer) {
		content = inBuffer;
	}
	
	public Packet writeByte(int i) {
		content.writeByte(i);
		return this;
	}
	
	public int readByte() {
		return content.readUnsignedByte();
	}
	
	public Packet writeShort(int i) {
		content.writeShort(i);
		return this;
	}
	
	public int readShort() {
		return readByte() << 8 | readByte();
	}
	
	public Packet writeInt(int i) {
		content.writeInt(i);
		return this;
	}
	
	public int readInt() {
		return readShort() << 16 | readShort();
	}
	
	public Packet writeLong(long l) {
		content.writeLong(l);
		return this;
	}
	
	public long readLong() {
		long value = 0;
		value |= (long) readByte() << 56L;
		value |= (long) readByte() << 48L;
		value |= (long) readByte() << 40L;
		value |= (long) readByte() << 32L;
		value |= (long) readByte() << 24L;
		value |= (long) readByte() << 16L;
		value |= (long) readByte() << 8L;
		value |= readByte();
		return value;
	}
	
	public Packet writeString(String s) {
		for (byte b : s.getBytes()) {
			writeByte(b);
		}
		writeByte(10);
		return this;
	}
	
	public String readString() {
		int c;
		StringBuilder builder = new StringBuilder();
		while ((c = readByte()) != 10) {
			builder.append((char) c);
		}
		return builder.toString();
	}
	
	public ByteBuf getContent() {
		return content;
	}
	
	public int getLength() {
		return content.writerIndex();
	}
	
	public ByteBuf finish() {
		try {
			final int length = getLength();
			final ByteBuf buf = content.alloc().buffer(2 + length);
			buf.writeShort(length);
			buf.writeBytes(content);
			return buf;
		} finally {
			ReferenceCountUtil.release(content);
		}
	}
	
}