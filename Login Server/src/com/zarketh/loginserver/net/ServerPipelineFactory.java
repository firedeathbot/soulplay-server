package com.zarketh.loginserver.net;

import com.zarketh.loginserver.net.codec.PacketDecoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public final class ServerPipelineFactory extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ch.pipeline().addLast(new PacketDecoder());
	}

}