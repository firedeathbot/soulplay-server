package com.zarketh.loginserver.util.login;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LoginRequest {

	private static final ConcurrentMap<String, LoginRequest> nameToLoginRequests = new ConcurrentHashMap<>();
	
	public static ConcurrentMap<String, LoginRequest> getNameToLoginRequests() {
		return nameToLoginRequests;
	}
	
	public static final int EXPIRED_LOGIN = 15000;
	
	private final int mysqlID;
	private final int world;
	private final long requestTime;
	public LoginRequest(int mysqlIndex, int world) {
		this.mysqlID = mysqlIndex;
		this.world = world;
		this.requestTime = System.currentTimeMillis();
	}
	
	public int getMysqlID() {
		return mysqlID;
	}
	public int getWorld() {
		return world;
	}

	public long getRequestTime() {
		return requestTime;
	}
	
	public boolean expired() {
		final long now = System.currentTimeMillis();
		return now - requestTime >= EXPIRED_LOGIN;
	}
	
}
