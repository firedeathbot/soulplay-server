package com.zarketh.loginserver.util;

import java.util.List;
import java.util.StringJoiner;
import java.util.regex.Pattern;

public class Utils {
	
	private static final Pattern SPACES_OR_EMPTY = Pattern.compile(" *");
	public static String implode(String separator, List<?> list) {
		StringJoiner sb = new StringJoiner(separator);
		for (Object object : list) {
			if (!SPACES_OR_EMPTY.matcher(object.toString()).matches()) {
				sb.add(object.toString());
			}
		}
		return sb.toString();
	}
}
