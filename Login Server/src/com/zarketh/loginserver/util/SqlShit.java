package com.zarketh.loginserver.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zarketh.loginserver.Server;

public class SqlShit {
	
	private static final String GET_NAME_QUERY = "SELECT PlayerName, display_name from players.accounts WHERE PlayerName = ? LIMIT 1";
	
	public static String getDisplayNameByUsername(final long usernameLong) {
		
		final String usernameFromLong = NameUtils.longToString(usernameLong);
		
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(GET_NAME_QUERY)) {
			ps.setString(1, usernameFromLong);
			try (ResultSet results = ps.executeQuery()) {
				if (results.next()) {
					String username = results.getString("PlayerName");
					String displayName = results.getString("display_name");
					if (displayName != null)
						return displayName;
					else
						return username;
				}
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static final String GET_USERNAME_QUERY = "SELECT PlayerName, display_name from players.accounts WHERE PlayerName = ? OR display_name = ? LIMIT 1";
	
	public static String getUserNameByDisplayName(final String displayName) {
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(GET_USERNAME_QUERY)) {
			ps.setString(1, displayName);
			ps.setString(2, displayName);
			try (ResultSet results = ps.executeQuery()) {
				if (results.next()) {
					String username = results.getString("PlayerName");
					return username;
				}
				return displayName;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return displayName;
	}

}
