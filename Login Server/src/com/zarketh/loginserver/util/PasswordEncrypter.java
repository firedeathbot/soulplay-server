package com.zarketh.loginserver.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The password encrypter.
 * 
 * @author Ultimate1
 */

public class PasswordEncrypter {

	/**
	 * The algorithm to use.
	 */
	private static MessageDigest md;

	/**
	 * Initialises the password encrypter.
	 */
	public static void initialise() {
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Encrypts the password.
	 * 
	 * @param password
	 * @return
	 */
	public static String encrypt(String password) {
		byte[] hash = md.digest(password.getBytes());
		StringBuffer hexValue = new StringBuffer();
		for (byte element : hash) {
			int val = element & 0xff;
			if (val < 16) {
				hexValue.append("0");
			}
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}

}