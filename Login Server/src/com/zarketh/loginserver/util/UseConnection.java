package com.zarketh.loginserver.util;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Jire
 */
@FunctionalInterface
public interface UseConnection<R> {
	
	R used(Connection connection) throws SQLException;
	
}
