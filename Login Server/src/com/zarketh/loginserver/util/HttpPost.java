package com.zarketh.loginserver.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class HttpPost {
	
	private static final ScheduledExecutorService HTTP_EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();
	
	public static void updatePlayerCount(int playerCount) {
		HTTP_EXECUTOR.execute(new Runnable() {
			
			@Override
			public void run() {
				String targetURL = "http://167.114.0.218/updateCount.php?playercount="+(playerCount);
				 HttpURLConnection connection = null;  
				  try {
				    //Create connection
				    URL url = new URL(targetURL);
				    connection = (HttpURLConnection)url.openConnection();

				    connection.setUseCaches(false);
				    connection.setDoOutput(true);

				    //Send request
				    DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
				    wr.close();
				    
				  //Get Response  
				    InputStream is = connection.getInputStream();
				    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				    StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+ 
				    String line;
				    while((line = rd.readLine()) != null) {
				      response.append(line);
				      response.append('\r');
				    }
				    rd.close();
//				    String resp = response.toString();
				    
				  } catch (Exception e) {
				    e.printStackTrace();
				  } finally {
				    if(connection != null) {
				      connection.disconnect(); 
				    }
				  }				
			}
		});
		
	}
	
}
