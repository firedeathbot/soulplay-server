package com.zarketh.loginserver;

/**
 * The Configuration File Of The Server
 * 
 */
public class Config {
	
	/**
	 * Your server name.
	 */
	public static /*final*/ String SERVER_NAME = "SoulPlay Login Server";
	
	public static final String LOGIN_SERVER_PASSWORD = "mywordss";
	
	public static final String MYSQL_USERNAME = "root";
	public static final String MYSQL_PASSWORD = "root";
	
	public static final String MYSQL_PLAYER_DB = "players";
	public static final String MYSQL_PLAYER_TABLE = "accounts";
	
	
	public static boolean isOwner(String name) {
		switch (name) {
		case "Michael":
		case "Julius":
		case "Diamondfan":
		case "Shine":
		case "Danny":
		case "Fearless":
		case "Leanbow":
			return true;	
		default:
			return false;
		}
	}
	
	public static boolean isDev(String name) {
		switch (name) {
		case "Julius":
		case "Leanbow":
			return true;
		default:
				return false;
		}
	}

}