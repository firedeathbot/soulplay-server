package com.zarketh.loginserver.node;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.NameUtils;

import io.netty.channel.Channel;
import io.netty.util.AttributeKey;

/**
 * Represents a connected Node.
 *
 * @author Ultimate1
 */

public class Node {
	
	public static final AttributeKey<Node> NODE_ATTRIBUTE = AttributeKey.newInstance("NODE_ATTRIBUTE");
	
	/**
	 * The id of the node.
	 */
	private final int nodeId;
	
	/**
	 * The channel.
	 */
	private final Channel channel;
	
	/**
	 * The maximum amount of players.
	 */
	public static final int MAXIMUM_PLAYERS = 2000;
	
	/**
	 * The <code>NodeManager</code>.
	 */
	private NodeManager nodeManager = NodeManager.getInstance();
	
	/**
	 * An map containing players for this node.
	 */
	private List<Player> players = new ArrayList<Player>();
	
	/**
	 * The logger.
	 */
	private static Logger logger = Logger.getLogger(Node.class.getName());
	
	private static Object LOCK = new Object();
	
	/**
	 * Initialises the node.
	 *
	 * @param nodeId
	 * @param channel
	 */
	public Node(int nodeId, Channel channel) {
		this.nodeId = nodeId;
		this.channel = channel;
	}
	
	/**
	 * Registers a player.
	 *
	 * @param player
	 * @return
	 */
	public boolean register(Player player) {
		synchronized (LOCK) {
			if (getPlayers()/* players */.size() >= MAXIMUM_PLAYERS) {
				System.out.println("Maximum players! Not able to register playerName:" + player.getUsername());
				return false;
			}
			getPlayers().add(player); // players.add(player);
			// logger.info("[World " + nodeId + "]: " + player.getUsername() + " has
			// registered. (" + players.size() + " online)");
			return false;
		}
	}
	
	/**
	 * Unregisters a player.
	 *
	 * @param player
	 * @return
	 */
	public boolean unregister(Player player) {
		synchronized (LOCK) {
			// final String nameLowercase = player.getUsername().toLowerCase();
			// final String nameAndWorld = nameLowercase+";"+nodeId;
			// final int nodeID = nodeId;
			// NodeManager.getInstance().addRecentlyLoggedOut(/*nameLowercase*/nameLowercase,
			// nodeId);
			getPlayers().remove(player); // players.remove(player);
			try {
				Friends.saveList(player);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			try {
				Friends.saveIgnoreList(player);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Server.getTaskScheduler().schedule(new Task(10) {
			//
			// @Override
			// protected void execute() {
			// NodeManager.getInstance().removeRecentlyLoggedOut(nameLowercase);
			// this.stop();
			// }
			// });

			// logger.info("[World " + player.getNode().getNodeId() + "]: " +
			// player.getUsername() + " has unregistered. (" + players.size() + "
			// online)");
			return true;
		}
	}
	
	/**
	 * Gets a player using their name.
	 *
	 * @param name
	 * @return
	 */
	public Player getPlayerByName(String name) {
		for (Player player : nodeManager.getConnectedPlayers()) {
			if (player.getUsername().equalsIgnoreCase(name)) {
				return player;
			}
		}
		return null;
	}
	
	public Player getLocalPlayer(String name) {
		for (Player player : getPlayers()) {
			if (player.getUsername().equalsIgnoreCase(name)) {
				return player;
			}
		}
		return null;
	}
	
	public Player getPlayerByID(int mysqlId) {
		for (Player player : nodeManager.getConnectedPlayers()) {
			if (mysqlId == player.getMysqlIndex()) {
				return player;
			}
		}
		return null;
	}
	
	public boolean maxConnection(String ipOrUUID, int type0UUID1IP) {
		int uuidCons = 0;
		int ipCons = 0;
		if (type0UUID1IP == 0) {
			for (Player player : nodeManager.getConnectedPlayers()) {
				if (player.getUUID().equals(ipOrUUID)) {
					uuidCons++;
					if (uuidCons > 3) {
						return true;
					}
				}
			}
		} else if (type0UUID1IP == 1) {
			for (Player player : nodeManager.getConnectedPlayers()) {
				if (player.getConnectedFrom().equals(ipOrUUID)) {
					ipCons++;
					if (ipCons > 5) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Gets a player using their long name.
	 *
	 * @param nameAsLong
	 * @return
	 */
	public Player getPlayerByLongName(long nameAsLong) {
		if (nameAsLong < 1) return null;
		String name = NameUtils.longToString(nameAsLong);
		for (Player player : nodeManager.getConnectedPlayers()) {
			if (player.getUsername().equalsIgnoreCase(name)) {
				return player;
			}
		}
		return null;
	}
	
	/**
	 * Handles the disconnection of the node.
	 */
	public void disconnect() {
		synchronized (LOCK) {
			for (Player player : players) {
				int chatMode = player.getPrivateChatMode();
				for (Player other : nodeManager.getConnectedPlayers()) {
					if (other.getNode() == this) {
						continue;
					}
					if (player == other || !other.hasFriend(player.getNameAsLong())) {
						continue;
					}
					if (chatMode == 1 && !player.hasFriend(other.getNameAsLong()) || chatMode == 2) {
						continue;
					}
					Packet unregister = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS, channel.alloc());
					unregister.writeString(other.getUsername());
					unregister.writeLong(player.getDisplayNameLong());
					unregister.writeByte(0);
					other.getNode().write(unregister);
				}
			}
			players.clear();
			channel.close();
		}
	}
	
	/**
	 * Writes a packet to the world server.
	 *
	 * @param packet
	 */
	public void write(Packet packet) {
		channel.writeAndFlush(packet.finish(), channel.voidPromise());
	}
	
	/**
	 * @return the id of the node.
	 */
	public int getNodeId() {
		return nodeId;
	}
	
	/**
	 * @return the channel.
	 */
	public Channel getChannel() {
		return channel;
	}
	
	/**
	 * @return the amount of players online.
	 */
	public int getPlayerCount() {
		return players.size();
	}
	
	/**
	 * @return An map containing players for this node.
	 */
	public List<Player> getPlayers() {
		synchronized (LOCK) {
			return players;
		}
	}
	
	/**
	 * Updates the player count for the node.
	 */
	public void updatePlayerCount() {
		nodeManager.getConnection().executeQuery("insert into playersonlinelog (nodeId, count, time) values ('" + nodeId + "', '" + Integer.valueOf(players.size()) + "', '" + System.currentTimeMillis() + "')");
	}
	
}