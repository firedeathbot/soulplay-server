package com.zarketh.loginserver.node;

import java.sql.ResultSet;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.slack.SlackMessageBuilder;
import com.zarketh.loginserver.packet.impl.PunishPlayer.Punishment;

/**
 * 
 * @author Ultimate1
 * 
 */
public class PunishmentHandler {
	// no, now the others
	public static boolean checkUser(String name) {
		boolean ok = false;
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT * FROM badplayers where username = '"
									+ name + "' and type = 0"); ) {
			long time = 0;
			if (rs.next()) {
				time = rs.getLong("time");
//				rs.close();
				ok = true;
			} else {
//				rs.close();
				ok = false;
			}
			if (time != 0 && time < System.currentTimeMillis() && ok) {
				try (ResultSet delete = NodeManager
						.getInstance()
						.getConnection()
						.executeQuery(
								"DELETE FROM badplayers where username = '"
										+ name + "' and type = 0");) {} catch (Exception ex) {ex.printStackTrace();};
				return false;
			} else if (ok) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	/**
	 * checks my mysql index of player account, which is a unique id for each player
	 * @param mid - mysql index of account
	 * @return
	 */
	public static boolean checkUser(int mid) {
		boolean ok = false;
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT * FROM badplayers where mid = '"
									+ mid + "' AND (type = "+Punishment.BAN_NAME.getPunishmentType()+" OR type = "+Punishment.BAN_IP.getPunishmentType()+" OR type = "+Punishment.BAN_UUID.getPunishmentType() + ")"); ) {
			long time = 0;
			if (rs.next()) {
				time = rs.getLong("time");
				
//				rs.close();
				ok = true;
			} else {
//				rs.close();
				ok = false;
			}
			if (time != 0 && time < System.currentTimeMillis() && ok) {
				try (ResultSet delete = NodeManager
						.getInstance()
						.getConnection()
						.executeQuery(
								"DELETE FROM badplayers where mid = '"
										+ mid + "' and type = 0");) {} catch (Exception ex) {ex.printStackTrace();};
				return false;
			} else if (ok) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static boolean checkMuted(String name) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT null FROM badplayers where username = '"
									+ name + "' and type = 2"); ) {
			if (rs.next()) {
//				rs.close();
				return true;
			} else {
//				rs.close();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean checkIpMuted(String ip) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT null FROM badplayers where ip = '"
									+ ip + "' and type = 3"); ) {
			if (rs.next()) {
//				rs.close();
				return true;
			} else {
//				rs.close();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean checkIP(String ip) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT null FROM badplayers where ip = '" + ip
									+ "' and type = 1"); ) {
			if (rs.next()) {
//				rs.close();
				return true;
			} else {
//				rs.close();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean checkUUID(String UUID) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT null FROM badplayers where uuid = '" + UUID
									+ "' and type = 4"); ) {
			if (rs.next()) {
//				rs.close();
				return true;
			} else {
//				rs.close();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean checkUUIDMute(String UUID) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"SELECT null FROM badplayers where uuid = '" + UUID
									+ "' and type = 5"); ) {
			if (rs.next()) {
//				rs.close();
				return true;
			} else {
//				rs.close();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static void punishPlayer(Player p, int punishmentType, long time, String reason, String punishedBy) {
		
		
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"INSERT INTO badplayers (username, mid, ip, uuid, type, time, reason, punishedby) VALUES (\""+p.getUsername().toLowerCase()+"\", \""+p.getMysqlIndex()+"\", \""+p.getConnectedFrom()+"\", \""+p.getUUID()+"\", "+punishmentType+", "+time+", \""+reason+"\", \""+punishedBy+"\")");
			//TODO: add these punishments to a second table that is held as archive of bans
			ResultSet rs2 = NodeManager.getInstance().getConnection().executeQuery("INSERT INTO punishments(username, mid, ip, uuid, type, time, reason, issuer) VALUES (\""+p.getUsername().toLowerCase()+"\", \""+p.getMysqlIndex()+"\", \""+p.getConnectedFrom()+"\", \""+p.getUUID()+"\", "+punishmentType+", "+time+", \""+reason+"\", \""+punishedBy+"\")"); ) {
			Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("punishment"), SlackMessageBuilder.buildEvidenceMessage(p.getUsername().toLowerCase(), Punishment.getPunishmentForType(punishmentType), (int) time, reason, punishedBy));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void freePlayer(String unbanName, int punishmentType) {
		try (ResultSet rs = NodeManager.getInstance()
					.getConnection()
					.executeQuery(
							"DELETE FROM badplayers WHERE username='"+unbanName+"' AND type="+punishmentType+""); ) {
			
				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean deleteRecord(String unbanName) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery(
							"DELETE FROM badplayers WHERE username='"+unbanName+"'"); ) {
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static int getPunishmentTypeByName(String name) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery("SELECT * FROM badplayers where username = '" + name + "'"); ) {
			
			int type = 99;
			if (rs.next()) {
				type = rs.getInt("type");
//				rs.close();
				return type;
			} else {
//				rs.close();
				return 99;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 99;
		}
	}
	
	public static int getPunishmentTypeByID(int index) {
		try (ResultSet rs = NodeManager
					.getInstance()
					.getConnection()
					.executeQuery("SELECT * FROM badplayers where mid = '" + index + "'"); ) {
			
			int type = 99;
			if (rs.next()) {
				type = rs.getInt("type");
//				rs.close();
				return type;
			} else {
//				rs.close();
				return 99;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 99;
		}
	}
	

}
