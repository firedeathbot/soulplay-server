package com.zarketh.loginserver.node;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.event.Task;
import com.zarketh.loginserver.net.SQLDatabase;

/**
 * Manages all connected nodes.
 * 
 * @author Ultimate1
 */

public class NodeManager implements Runnable {

	/**
	 * The logger.
	 */
	private static Logger logger = Logger
			.getLogger(NodeManager.class.getName());

	/**
	 * An instance of the NodeManager.
	 */
	private static final NodeManager INSTANCE = new NodeManager();

	/**
	 * @return an instance of the NodeManager.
	 */
	public static NodeManager getInstance() {
		return INSTANCE;
	}
	
	public final Object lock = new Object();

	private HashMap<String, Integer> recentlyLoggedOut = new HashMap<String, Integer>();
	
	private ArrayList<String> recentlyLoggedIn = new ArrayList<String>();
	
	public HashMap<String, Integer> getRecentlyLoggedOut() {
		synchronized (lock) {
			return recentlyLoggedOut;
		}
	}

//	public void setRecentlyLoggedOut(ArrayList<String> recentlyLoggedOut) {
//		synchronized (lock) {
//			this.recentlyLoggedOut = recentlyLoggedOut;
//		}
//	}
	
	public void addRecentlyLoggedOut(String name, int node) {
		synchronized (lock) {
			this.recentlyLoggedOut.put(name, node);
		}
	}
	
	public void removeRecentlyLoggedOut(String name) {
		synchronized (lock) {
			this.recentlyLoggedOut.remove(name);
		}
	}
	
	public boolean recentlyLoggedOut(String name, int currentNode) {
		synchronized (lock) {
			if (recentlyLoggedOut.get(name) != null && recentlyLoggedOut.get(name) != currentNode) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public void addRecentlyLoggedIn(final String name) {
		synchronized (lock) {
			this.recentlyLoggedIn.add(name);
		}
		
		Server.getTaskScheduler().schedule(new Task(10) {

			@Override
			protected void execute() {
				NodeManager.getInstance().removeRecentlyLoggedIn(name);
			}
		});
	}
	
	public void removeRecentlyLoggedIn(String name) {
		synchronized (lock) {
			this.recentlyLoggedIn.remove(name);
		}
	}
	
	public boolean recentlyLoggedIn(String name) {
		synchronized (lock) {
			return this.recentlyLoggedIn.contains(name);
		}
	}
	
	public void clearLoginList() {
		synchronized (lock) {
			this.recentlyLoggedIn.clear();
		}
	}
	

	/**
	 * The thread.
	 */
	private Thread thread;

	/**
	 * The maximum amount of nodes.
	 */
	public static final int MAXIMUM_NODES = 100;

	/**
	 * The connection to the game server.
	 */
	private final SQLDatabase database = new SQLDatabase("loginserveruser",
			"VtWHf3CYMWS8o5yh", "127.0.0.1/loginserver", 2);

	/**
	 * The task queue.
	 */
	private final Deque<Runnable> tasks = new ArrayDeque<Runnable>();
	
	private final Lock tasksLock = new ReentrantLock();

	/**
	 * An array containing nodes.
	 */
	private final Map<Integer, Node> nodes = new HashMap<Integer, Node>(MAXIMUM_NODES);

	/**
	 * @return the amount of active nodes.
	 */
	public int getActiveNodeCount() {
		return nodes.size();
	}

	/**
	 * @return a map of all the connected nodes.
	 */
	public Map<Integer, Node> getNodes() {
		return nodes;
	}

	/**
	 * Get the node with the specified id.
	 * 
	 * @param nodeId
	 * @return
	 */
	public Node getNode(int nodeId) {
		return nodes.get(nodeId);
	}

	/**
	 * Gets the connection to the gameserver
	 * 
	 * @return
	 */
	public SQLDatabase getConnection() {
		return database;
	}

	/**
	 * Pushses a task onto the queue.
	 * 
	 * @param runnable
	 *            The runnable.
	 */
	public void pushTask(Runnable runnable) {
		tasksLock.lock();
		try {
			tasks.add(runnable);
		} finally {
			tasksLock.unlock();
		}
	}

	/**
	 * Starts the <code>NodeManager</code>.
	 */
	public void start() {
		if (thread == null) {
			database.initialise("game");
			thread = new Thread(this, "NodeManager");
			thread.start();
		}
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			List<Runnable> tasksCopy = null;
			tasksLock.lock();
			try {
				tasksCopy = new ArrayList<>(tasks);
				tasks.clear();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				tasksLock.unlock();
			}
			
			final long start = System.currentTimeMillis();
			
			if (tasksCopy != null) {
				for (Runnable r : tasksCopy) {
					r.run();
				}
			}
			
			final long elapsed = System.currentTimeMillis() - start;
			final long sleepTime = 100L - elapsed;
			
			if (sleepTime > 0) {
				try {
					Thread.sleep(sleepTime); // give this thread a break
				} catch (InterruptedException e) {
					e.printStackTrace();
					return;
				}
			}
			
		}
	}

	/**
	 * Registers a node.
	 * 
	 * @param node
	 * @return
	 */
	public boolean register(Node node) {
		if (nodes.containsKey(node.getNodeId())) {
			return false;
		}
		nodes.put(node.getNodeId(), node);
		logger.info("[World " + node.getNodeId() + "]: Connected!");
		return true;
	}

	/**
	 * Unregisters a node.
	 * 
	 * @param node
	 */
	public void unregister(Node node) {
		if (!nodes.containsValue(node)) {
			return;
		}
		node.disconnect();
		nodes.remove(node.getNodeId());
		logger.info("[World " + node.getNodeId() + "]: Disconnected!");
	}

	/**
	 * @return a list of all connected players.
	 */
	public List<Player> getConnectedPlayers() {
		final List<Player> list = new ArrayList<Player>();
		for (Node node : nodes.values()) {
			list.addAll(node.getPlayers());
		}
		return list;
	}
	
	public Player getPlayerByLongDisplayNameSmart(long displayNameLong) {
		for (Player player : getConnectedPlayers()) {
			if (player.getDisplayNameLong() == displayNameLong) {
				return player;
			}
		}
		return null;
	}

}