package com.zarketh.loginserver.node;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class responsible for the loading and saving of the friends list.
 * 
 * @author Ultimate1
 * 
 */

public class Friends {
	
	public static final String FRIEND_FOLDER = "./data/friends/";
	public static final String IGNORE_FOLDER = "./data/ignores/";

	public static void saveList(Player player) {
		DataOutputStream write = null;
		long[] friends = new long[player.getFriends().size()];
		if (friends == null || friends.length == 0) {
			// We don't have any friends to save.
			File file = new File(FRIEND_FOLDER + player.getUsername()
			+ ".dat");
			if (file.exists())
				file.delete();
			return;
		}
		for (int i = 0; i < friends.length; i++) {
			if (player.getFriends().get(i) != null)
				friends[i] = player.getFriends().get(i);
		}
		try {
			File file = new File(FRIEND_FOLDER + player.getUsername()
					+ ".dat");
			FileOutputStream outFile = new FileOutputStream(file);
			write = new DataOutputStream(outFile);
			write.writeInt(friends.length);
			for (int i = 0; i < friends.length; i++) {
				write.writeLong(friends[i]);
			}
			write.writeByte(player.getPrivateChatMode());
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (write != null) {
				try {
					write.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static Object[] loadList(String name) {
		DataInputStream load = null;
		try { // copy over
			File file = new File(FRIEND_FOLDER + name + ".dat");
			if (!file.exists()) {
				return null;
			}
			FileInputStream inFile = new FileInputStream(file);
			load = new DataInputStream(inFile);
			int looper = load.readInt();

			long[] friends = new long[looper >= 0 ? looper : 0];
			for (int i = 0; i < looper; i++) {
				friends[i] = load.readLong();
			}
			int privateChatMode = load.readByte();

			return new Object[] { friends, privateChatMode };
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (load != null) {
				try {
					load.close();
				} catch (IOException e) {
				}
			}
		}
		return null;
	}
	
	public static void saveIgnoreList(Player player) {
		DataOutputStream write = null;
		if (player.getIgnoreList().size() == 0) {
			// We don't have any friends to save.
			File file = new File(IGNORE_FOLDER + player.getUsername() + ".dat");
			if (file.exists())
				file.delete();
			return;
		}
		try {
			File file = new File(IGNORE_FOLDER + player.getUsername() + ".dat");
			FileOutputStream outFile = new FileOutputStream(file);
			write = new DataOutputStream(outFile);
			write.writeInt(player.getIgnoreList().size());
			for (long ign : player.getIgnoreList()) {
				write.writeLong(ign);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (write != null) {
				try {
					write.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static void loadIgnoreList(Player p, String name) {
		DataInputStream load = null;
		try { // copy over
			File file = new File(IGNORE_FOLDER + name + ".dat");
			if (!file.exists()) {
				return;
			}
			FileInputStream inFile = new FileInputStream(file);
			load = new DataInputStream(inFile);
			int looper = load.readInt();

			for (int i = 0; i < looper; i++) {
				p.getIgnoreList().add(load.readLong());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (load != null) {
				try {
					load.close();
				} catch (IOException e) {
				}
			}
		}
	}

}