package com.zarketh.loginserver.node;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.zarketh.loginserver.util.NameUtils;

/**
 * Represents a player.
 * 
 * @author Ultimate1
 */

public class Player {

	/**
	 * The node the player is on.
	 */
	private Node node;

	/**
	 * The name of the player.
	 */
	private String username;

	/**
	 * The player's primary rights.
	 */
	private int primaryRights;

	/**
	 * The display player rights.
	 */
	private int displayRights;

	/**
	 * The player's private chat mode.
	 */
	private int privateChatMode;
	
	private String connectedFrom;
	
	private String UUID;
	
	private int mysqlIndex;
	
	private long displayNameLong;

	/**
	 * Friends on the player's list.
	 */
	private Map<Integer, Long> friends = new HashMap<Integer, Long>();
	
	private final Set<Long> ignoreList = new HashSet<Long>();

	/**
	 * Initialise the player.
	 * 
	 * @param username
	 * @param node
	 */
	public Player(String username, Node node) {
		this.username = username;
		this.node = node;
	}

	/**
	 * @return the node the player is on.
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * @return the username of the player.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the username as a long.
	 */
	public long getNameAsLong() {
		return NameUtils.nameToLong(username);
	}

	/**
	 * @return the private chat mode.
	 */
	public int getPrivateChatMode() {
		return privateChatMode;
	}

	/**
	 * Sets the private chat mode.
	 * 
	 * @param privateChatMode
	 */
	public void setPrivateChatMode(int privateChatMode) {
		this.privateChatMode = privateChatMode;
	}

	/**
	 * @return the primary rights.
	 */
	public int getPrimaryRights() {
		return primaryRights;
	}

	public static final int MOD_RIGHTS = 1;
	public static final int ADMIN_RIGHTS = 2;
	public static final int CO_OWNER_RIGHTS = 3;
	public static final int OWNER_RIGHTS = 4;
	public static final int HIDDEN_MOD_RIGHTS = 5;
	public static final int HIDDEN_ADMIN_RIGHTS = 6;
	public static final int HIDDEN_OWNER_RIGHTS = 7;
	public static final int HELPER_RIGHTS = 8;

	public static final int STANDARD_DONATOR_RIGHTS = 1;
	public static final int SUPER_DONATOR_RIGHTS = 2;

	/**
	 * Checks if the player has a certain right.
	 * 
	 * @param rightsToCheck
	 * @return
	 */
	public boolean hasRights(int rightsToCheck) {
		switch (primaryRights) {
		case HIDDEN_MOD_RIGHTS:
			return rightsToCheck == MOD_RIGHTS;
		case HIDDEN_ADMIN_RIGHTS:
			return rightsToCheck == ADMIN_RIGHTS;
		case HIDDEN_OWNER_RIGHTS:
			return rightsToCheck == OWNER_RIGHTS;
		}
		return primaryRights == rightsToCheck;
	}

	/**
	 * @return the client rights
	 */
	public int getClientRights() {
		return primaryRights;

//		// Check if rights is hidden
//		boolean isHiddenStaff = false;
//		switch (primaryRights) {
//		case Player.HIDDEN_MOD_RIGHTS:
//		case Player.HIDDEN_ADMIN_RIGHTS:
//		case Player.HIDDEN_OWNER_RIGHTS:
//			isHiddenStaff = true;
//			break;
//		}
//		
//		
//
//		// Primary rights have priorty
//		if (!isHiddenStaff && primaryRights > 0) {
//			switch (primaryRights) {
//			case Player.HELPER_RIGHTS:
//				return 6;
//			case Player.CO_OWNER_RIGHTS:
//			case Player.OWNER_RIGHTS:
//				return 2;
//			}
//			return primaryRights;
//		}
//
//		// Try donator rights if we haven't got a primary one
//		switch (secondaryRights) {
//		case Player.STANDARD_DONATOR_RIGHTS:
//			return 3;
//		case Player.SUPER_DONATOR_RIGHTS:
//			return 8;
//		}
//
//		// We are just an ordinary player
//		return 0;
	}

	/**
	 * Sets a friend on the player's list.
	 * 
	 * @param index
	 * @param val
	 */
	public void setFriend(int index, long val) {
		friends.put(index, val);
	}
	
	public void removeFriend(int index) {
		friends.remove(index);
	}

	/**
	 * @return the friends on the player's list.
	 */
	public Map<Integer, Long> getFriends() {
		return friends;
	}

	/**
	 * Sets the rights of the player.
	 * 
	 * @param primaryRights
	 * @param displayRights
	 */
	public void setRights(int primaryRights, int displayRights) {
		this.primaryRights = primaryRights;
		this.displayRights = displayRights;
	}

	/**
	 * Check if the specified friend is on the player list.
	 * 
	 * @param name
	 * @return
	 */
	public boolean hasFriend(long name) {
		for (long friend : friends.values()) {
			if (friend == name) {
				return true;
			}
		}
		return false;
	}

	public String getConnectedFrom() {
		return connectedFrom;
	}

	public void setConnectedFrom(String connectedFrom) {
		this.connectedFrom = connectedFrom;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}
	
	public static Player getPlayer(String playerName) {
		for (Player p : NodeManager.getInstance().getConnectedPlayers()) {
			if (playerName.equals(p.getUsername())) {
				return p;
			}
		}
		return null;
	}
	
	public static Player getPlayer(int mysqlIndex) {
		for (Player p : NodeManager.getInstance().getConnectedPlayers()) {
			if (mysqlIndex == p.getMysqlIndex()) {
				return p;
			}
		}
		return null;
	}

	public int getMysqlIndex() {
		return mysqlIndex;
	}

	public void setMysqlIndex(int mysqlIndex) {
		this.mysqlIndex = mysqlIndex;
	}

	public Set<Long> getIgnoreList() {
		return ignoreList;
	}

	public int getDisplayRights() {
		if (username.toLowerCase().equals("aleksandr")) {
			return 0;
		}

		return displayRights;
	}

	public long getDisplayNameLong() {
		return displayNameLong;
	}

	public void setDisplayNameLong(long displayNameLong) {
		this.displayNameLong = displayNameLong;
	}

}