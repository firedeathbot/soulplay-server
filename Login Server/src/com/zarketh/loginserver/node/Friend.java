package com.zarketh.loginserver.node;

/**
 * 
 * @author Rene Roosen Represents a friend
 */
public class Friend {

	/**
	 * The username
	 */
	private String username;

	/**
	 * The username as along, so we don't have to convert it each time
	 */
	private long usernameAsLong;

	/**
	 * The player's sesion index
	 */
	private int index;

	/**
	 * Reconstructs the friend
	 * 
	 * @param username
	 * @param usernameAsLong
	 * @param index
	 */
	public Friend(String username, long usernameAsLong, int index) {
		this.username = username;
		this.usernameAsLong = usernameAsLong;
		this.index = index;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Set the username
	 * 
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Return username as a long
	 * 
	 * @return
	 */
	public long getUsernameAsLong() {
		return usernameAsLong;
	}

	/**
	 * Sets the username as long
	 * 
	 * @param usernameAsLong
	 */
	public void setUsernameAsLong(long usernameAsLong) {
		this.usernameAsLong = usernameAsLong;
	}

	/**
	 * Gets the index
	 * 
	 * @return
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Sets the index
	 * 
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

}
