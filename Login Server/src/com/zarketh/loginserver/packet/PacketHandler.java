package com.zarketh.loginserver.packet;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;

/**
 * The packet handler
 * 
 * @author Ultimate1
 */

public interface PacketHandler {

	public void handlePacket(Node node, Packet packet) throws Exception;

}
