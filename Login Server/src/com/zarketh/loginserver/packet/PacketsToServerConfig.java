package com.zarketh.loginserver.packet;

public class PacketsToServerConfig {

	public static final int CONNECTED_TO_GAMESERVER_RESPONSE = 1;
	public static final int LOGIN_RESPONSE_CODE = 2;
	public static final int NOTUSED3 = 3;
	public static final int SEND_PM = 4;
	public static final int SEND_PM_ONLINE_STATUS = 5;
	public static final int SEND_GLOBAL_MESSAGE = 6;
	public static final int NODE_PLAYER_COUNT = 7;
	public static final int SEND_FRIENDS_LIST = 8;
	public static final int PUNISH_PLAYER = 9;
	public static final int KICK_PLAYER = 10;
	public static final int NOTIFY_PLAYER = 11; // add to notify player if ban was successful or not, etc. sendmessage to only one player
	public static final int SEND_ITEM_PRICE = 12;
	public static final int SEND_PACKET_TO_SAVE_ACCOUNTS = 13;
	public static final int UPDATE_PLAYER_COUNT = 15;
	public static final int PLAYER_LOGGED_WORLD_DUPECHECK = 16;
	
	
}
