package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

/**
 * A packet that handles the de-registration of a player.
 * 
 * @author Ultimate1
 */

public class UnregisterPlayer implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		Player player = node.getPlayerByName(name);
		if (player != null) {
//			if (node.getNodeId() != player.getNode().getNodeId())
//				return;
			int chatMode = player.getPrivateChatMode();
			for (Player other : NodeManager.getInstance().getConnectedPlayers()) {
				if (other == null || other == player || !other.hasFriend(player.getNameAsLong())) {
					continue;
				}
				if (chatMode == 1 && !player.hasFriend(other.getNameAsLong())
						|| chatMode == 2) {
					// Do not update!
					continue;
				}
				Packet unregister = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS);
				unregister.writeString(other.getUsername());
				unregister.writeLong(player.getDisplayNameLong());
				unregister.writeByte(0);
				other.getNode().write(unregister);
			}
			
			Server.SQL_SERVICE.submit(() -> node.unregister(player));
		}
	}

}