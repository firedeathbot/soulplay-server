package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

public class CheckDupedAccount implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		int worldId = packet.readByte();

		Player p = node.getPlayerByName(name);
		
		int nodeId = 0; // 0 means he is not registered in login server! that is bad!
		
		if (p != null) {
			nodeId = p.getNode().getNodeId();
		}
		
		Packet sendPacket = new Packet(PacketsToServerConfig.PLAYER_LOGGED_WORLD_DUPECHECK); // Construct the packet

		sendPacket.writeString(name);
		sendPacket.writeByte(nodeId);

		node.write(sendPacket);
		
		if (worldId != nodeId)
			PunishPlayer.kickPlayer(node, name);
	}

}