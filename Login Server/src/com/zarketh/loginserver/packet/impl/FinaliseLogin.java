package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.net.slack.SlackLoginNotifier;
import com.zarketh.loginserver.node.Friends;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.NameUtils;
import com.zarketh.loginserver.util.SqlShit;
import com.zarketh.loginserver.util.login.LoginRequest;

public class FinaliseLogin implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String nameToRead = packet.readString();
		int primaryRights = packet.readByte();
		int secondaryRights = packet.readByte();
		String connectedFrom = packet.readString();
		String UUID = packet.readString();
		int mysqlIndex = packet.readInt();
		final long displayNameSmart = packet.readLong();
		final String name = nameToRead.toLowerCase();
		
		Server.LOGIN_SINGLE_THREAD.submit(new Runnable() {
			
			@Override
			public void run() {
				
				LoginRequest login = LoginRequest.getNameToLoginRequests().get(name);
				
				if (login != null) {
					if (login.expired()) {
						System.out.println("FinalizeLogin: login expired for sqlID:"+mysqlIndex+" name:"+name);
					}
					
					if (login.getWorld() != node.getNodeId()) {
						System.out.println("FinalizeLogin: Player logged into wrong world. Login Runnig for World:"+login.getWorld()+" but finalized on world:"+node.getNodeId());
					}
					
					LoginRequest.getNameToLoginRequests().remove(name);
				} else {
					System.out.println("FinalizeLogin: No login request existing for sqlID:" + mysqlIndex + " name:"+ name);
				}

				int chatMode = 0;
				long[] friends = null;
				Player player = new Player(name, node);
				player.setRights(primaryRights, secondaryRights);
				player.setConnectedFrom(connectedFrom);
				player.setUUID(UUID);
				player.setMysqlIndex(mysqlIndex);
				player.setDisplayNameLong(displayNameSmart);
				final long myLongName = player.getNameAsLong();
				boolean online = node.getPlayerByName(name) != null;
				if (!online) {
					node.register(player);
					Object[] friendsData = Friends.loadList(name);
					if (friendsData != null) {
						friends = (long[]) friendsData[0];
						for (int i = 0; i < friends.length; i++) {
							player.setFriend(i, friends[i]);
						}
						player.setPrivateChatMode((Integer) friendsData[1]);
						chatMode = player.getPrivateChatMode();
					}
					
					Friends.loadIgnoreList(player, name);
					
					for (Player other : NodeManager.getInstance().getConnectedPlayers()) { //refresh other's friendlist
						if (other == player || !other.hasFriend(myLongName)) {
							continue;
						}
						final long otherLongName = other.getNameAsLong();
						if ((chatMode == 1 && !player.hasFriend(otherLongName))
								|| chatMode == 2) {
							continue;
						}
						if (player.getIgnoreList().contains(otherLongName))
							continue;
//						System.out.println("longFriend to game:"+player.getUsername());
						Packet register = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS);
						register.writeString(other.getUsername());
						register.writeLong(player.getDisplayNameLong());
						register.writeByte(node.getNodeId());
						other.getNode().write(register);
					}
					//NodeManager.getInstance().removeRecentlyLoggedIn(name);
					SlackLoginNotifier.checkNotificationEntry(name, node.getNodeId(), UUID, connectedFrom, player.getUsername());
				}
				
				try {
					//refresh own friends/ignore list
					Packet friendsListPacket = new Packet(PacketsToServerConfig.SEND_FRIENDS_LIST);
					friendsListPacket.writeString(name);
					friendsListPacket.writeByte(chatMode);
					if (friends == null) {
						friendsListPacket.writeByte(0);
					} else {
						friendsListPacket.writeByte(friends.length);
						for (int i = 0; i < friends.length; i++) {
							Player friend = node.getPlayerByLongName(friends[i]);
							
							final String displayName = SqlShit.getDisplayNameByUsername(friends[i]);
							if (displayName == null) {
								friendsListPacket.writeLong(friends[i]);
								friendsListPacket.writeByte(0);
								//player.removeFriend(i);
								continue;
							}

							final long newLongName = NameUtils.nameToLong(displayName);
							friendsListPacket.writeLong(newLongName);
							if (friend == null) {
								friendsListPacket.writeByte(0);
							} else {
								if (friend.getPrivateChatMode() == 2 || (friend.getPrivateChatMode() == 1 && !friend.hasFriend(myLongName))) {
									friendsListPacket.writeByte(0);
								} else {
									friendsListPacket.writeByte(friend.getNode().getNodeId());
								}
							}
						}
					}
					
					friendsListPacket.writeByte(player.getIgnoreList().size());
					for (long ign : player.getIgnoreList()) {
						friendsListPacket.writeLong(ign);
					}
					
					node.write(friendsListPacket);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

}