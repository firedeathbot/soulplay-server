package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

/**
 * A packet that gets the status of a player on the node.
 * 
 * @author Ultimate1
 */

public class FriendStatus implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		Player player = node.getPlayerByName(name);
		long friend = packet.readLong();
		if (player != null) {
			int world = 0;
			Player other = node.getPlayerByLongName(friend);
			Packet response = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS);
			response.writeString(player.getUsername());
//			System.out.println("longFriend to game:"+friend);
			if (other != null) {
				response.writeLong(other.getDisplayNameLong());
				int otherChatMode = other.getPrivateChatMode();
				if (otherChatMode == 1
						&& other.hasFriend(player.getNameAsLong())
						|| otherChatMode == 0) {
					world = other.getNode().getNodeId();
				}
			} else {
				response.writeLong(friend);
			}
			response.writeByte(world);
			node.write(response);
		}
	}

}