package com.zarketh.loginserver.packet.impl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.PunishmentHandler;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.PasswordHash;
import com.zarketh.loginserver.util.login.LoginRequest;

/**
 * A packet that replies with the login response for the specified player.
 * 
 * @author Ultimate1
 */

public class LoginHandler implements PacketHandler {

	private static final Logger LOG = Logger.getLogger(
			LoginHandler.class.getName(), null);

	/**
	 * Constructs the login reply packet.
	 * 
	 * @param name
	 * @param code
	 * @return
	 */
	private Packet constructResp(String name, int code) {
		Packet packet = new Packet(PacketsToServerConfig.LOGIN_RESPONSE_CODE);
		packet.writeString(name);
		packet.writeByte(code);
		return packet;
	}

	@Override
	public void handlePacket(final Node node, final Packet packet)
			throws Exception {
		String readName = packet.readString();
		String password = packet.readString();
		String host = packet.readString();
		String UUID = packet.readString();
		final String name = readName.toLowerCase();
		//password = password;
		
		/**
		 * Either use single thread or put sync block from LoginRequest.get() to LoginRequest.put(), if sync, then do it in FinaliseLogin.java too, as it removes from map
		 */
		Server.LOGIN_SINGLE_THREAD.submit(() -> {
			int code = 11;
			int mid = getMID(readName);
			
			LoginRequest login = LoginRequest.getNameToLoginRequests().get(name);
			
			if (login != null && !login.expired()) {
				code = 5;
			} else if (node.getPlayerByName(name) != null) {
				code = 5;
			} else if (PunishmentHandler.checkUUID(UUID) || PunishmentHandler.checkIP(host) || PunishmentHandler.checkUser(name)) {
				code = 4;
			} else if (mid > 0 && PunishmentHandler.checkUser(mid)) {
				code = 4;
			} else if (node.maxConnection(host, 1) || node.maxConnection(UUID, 0)) {
				code = 9;
//			} else if (NodeManager.getInstance().recentlyLoggedOut(name, node.getNodeId())) {
//				code = 23;
//			} else if (NodeManager.getInstance().recentlyLoggedIn(name)) {
//				code = 5;
			} else {
				try {
					int returnedInts = loadFromMainAccount(name, password);//int correctPass = loadFromMainAccount(name, password);
					
					if (returnedInts > 0) { // already logged in
						code = 10;
					} else if (returnedInts == -1) { // wrong password
						code = 3;
					} else if (returnedInts == 0) {
						code = 2;
					}
					
					if (code == 2) { //if correct password then set muted for those who are muted...	
						LoginRequest.getNameToLoginRequests().put(name, new LoginRequest(mid, node.getNodeId())); //NodeManager.getInstance().addRecentlyLoggedIn(name);
						if (PunishmentHandler.checkMuted(name) || PunishmentHandler.checkIpMuted(host) || PunishmentHandler.checkUUIDMute(UUID)) {
							code = 222; // code 222 means they are muted
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
					code = 3;
				}
			}//this is off the netty thread cuz it's to that serivce
			//
			
			
//			LOG.info("complete. ReturnCode:" + code);
//			Packet packet2 = null;
			node.write(/*packet2 =*/ constructResp(name, code));
//			LOG.info("finished writing: " + name + ", " + code + ", packet2=" + packet2);
		});
	}
	
	
	/**
	 * @param playerName
	 * @param playerPass
	 * @return  return > 0 if logged in | return -1 if wrong password, 0 is like ret2
	 */
	public int loadFromMainAccount(String playerName, String playerPass) {
		int ret = -1;
		int world = 0;

		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();) {

			try (ResultSet rs = stmt.executeQuery("select Password, logged_into from `accounts` where PlayerName = '" + playerName + "' limit 1")) {
				if(rs.next()) {
					String Pass = rs.getString("Password");
					ret = rs.getInt("logged_into");
					if (PasswordHash.validatePassword(playerPass, Pass)) {
						ret = 0;
					} else {
						ret = -1;
					}

					if (world > 0)
						ret = world;

				} else { //Does not have an account in "account" Table so create new account after login is succesful
					ret = 0;
				}

			} catch (SQLException e) {
				e.printStackTrace();
				ret = -1;
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				ret = -1;
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
				ret = -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			ret = -1;
		}
		return ret;
	}
	
	
	public static int getMID(String playerName) {
		int mid = 0;
		Connection connection = Server.getConnection();
		
		Statement stmt = null;
		ResultSet rs = null;

		try 
		{
			
			stmt = connection.createStatement();
			rs = stmt.executeQuery("select ID from `accounts` where PlayerName = '" + playerName + "' limit 1");
			if(rs.next()) {
				mid = rs.getInt("ID");
			} else { //Does not have an account in "account" Table so create new account after login is succesful
				mid = 0;
			}
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			mid = 0;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return mid;
	}
	

}