package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

/**
 * A packet that handles the update of the player's chat chat mode.
 * 
 * @author Ultimate1
 */

public class UpdateChatMode implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		int privateChatMode = packet.readByte();
		Player player = node.getPlayerByName(name);
		if (player != null) {
			player.setPrivateChatMode(privateChatMode);
			for (Player other : NodeManager.getInstance().getConnectedPlayers()) {
				if (other == player || !other.hasFriend(player.getNameAsLong())) {
					continue;
				}
				int world = 0;
				if (privateChatMode == 1 && player.hasFriend(other.getNameAsLong()) || privateChatMode == 0) {
					world = player.getNode().getNodeId();
				}
				Packet unregister = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS);
				unregister.writeString(other.getUsername());
				unregister.writeLong(player.getDisplayNameLong());
				unregister.writeByte(world);
				other.getNode().write(unregister);
			}
		}
	}

}