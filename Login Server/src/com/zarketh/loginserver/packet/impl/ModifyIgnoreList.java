package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.NameUtils;

public class ModifyIgnoreList implements PacketHandler {


	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		boolean addingToIgnoreList = packet.readByte() == 1;
		long ignoreLongName = packet.readLong();
		Player player = node.getLocalPlayer(name);
		if (player != null) {
			if (ignoreLongName > 0) {
				if (addingToIgnoreList) {
					if (!player.getIgnoreList().contains(ignoreLongName))
						player.getIgnoreList().add(ignoreLongName);
				} else {
					player.getIgnoreList().remove(ignoreLongName);
				}
				Player other = node.getPlayerByName(NameUtils
						.longToString(ignoreLongName));
//				if (other == null // dunno what this is
//						&& player.getFriends().get(friendIndex) != null) {
//					other = node.getPlayerByName(NameUtils.longToString(player
//							.getFriends().get(friendIndex)));
//				}
				if (other != null) {
					if (other.hasFriend(player.getNameAsLong())) {
						Packet resp = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS);
						resp.writeString(other.getUsername());
						resp.writeLong(player.getDisplayNameLong());
						if (addingToIgnoreList) {
							resp.writeByte(0);
						} else if (!addingToIgnoreList && player.getPrivateChatMode() == 0) {
							resp.writeByte(node.getNodeId());
						}
						other.getNode().write(resp);
					}
				}
			}
		}
	}

	
}
