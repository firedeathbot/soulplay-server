package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

/**
 * A packet that handles the relaying of private messages.
 * 
 * @author Ultimate1
 */

public class PrivateMessage implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		int chatCrown = packet.readByte();
		long nameOfRecipient = packet.readLong();
		int messageLength = packet.readInt();
		byte[] message = new byte[messageLength];
		for (int i = 0; i < message.length; i++) {
			message[i] = (byte) packet.readByte();
		}
		Player recipient = NodeManager.getInstance().getPlayerByLongDisplayNameSmart(nameOfRecipient);
		Player sender = node.getPlayerByName(name);
		if (sender != null) {
			if (recipient == null || recipient.getPrivateChatMode() == 2 || (recipient.getPrivateChatMode() == 1 && !recipient.hasFriend(sender.getNameAsLong()))) {
				SendMessage.sendMessage(sender.getNode(), sender.getUsername(), "<col=ff0000>Player is offline.");
				return;
			}
			Packet messagePacket = new Packet(PacketsToServerConfig.SEND_PM);
			messagePacket.writeLong(sender.getDisplayNameLong());
			messagePacket.writeLong(nameOfRecipient);
			messagePacket.writeByte(chatCrown);
			messagePacket.writeInt(messageLength);
			for (byte element : message) {
				messagePacket.writeByte(element);
			}
			recipient.getNode().write(messagePacket);
		}
	}

}