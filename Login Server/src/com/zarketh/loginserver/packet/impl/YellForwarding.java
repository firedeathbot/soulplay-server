package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

/**
 * The global yell packet.
 * 
 * @author Ultimate1
 */

public class YellForwarding implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String displayName = packet.readString();
		String message = packet.readString();
			
		if (message.startsWith(":clan")) {
			for (Node n : NodeManager.getInstance().getNodes().values()) {
				n.write(new Packet(PacketsToServerConfig.SEND_GLOBAL_MESSAGE).writeByte(node.getNodeId())
						.writeByte(0).writeString(displayName)
						.writeString(message));
			}
		} else {
			for (Node n : NodeManager.getInstance().getNodes().values()) {
				n.write(new Packet(PacketsToServerConfig.SEND_GLOBAL_MESSAGE).writeByte(node.getNodeId())
						.writeByte(0).writeString(displayName)
						.writeString(message));
			}
		}
	}

}