package com.zarketh.loginserver.packet.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

public class PriceChecker implements PacketHandler {

	private static int[] prices = new int[32343];
	
	private final static String FILE_LOCATION = "./data/databases/pricecheck.json";
	
	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		int itemId = packet.readInt();
		int itemPrice = packet.readInt();
		
		if (itemPrice == 0) { // using 0 for only receiving item price.
			
			for (Node nodeList : NodeManager.getInstance().getNodes().values()) {
				if (nodeList.getNodeId() == 1 || nodeList.getNodeId() == 2) {
					sendItemPrice(nodeList, itemId);
				}
			}
			
		} else { // add
			if (prices == null)
				return;
			if (itemId < 0 || itemId > prices.length)
				return;
			if (prices[itemId] > 0) {
				long averageTotal = prices[itemId] + itemPrice;
				
				double averageDouble = averageTotal / 2;
				
				prices[itemId] = (int) averageDouble;
				
			} else { // item average price not recorded yet
				prices[itemId] = itemPrice;
			}
			
			for (Node nodeList : NodeManager.getInstance().getNodes().values()) {
				if (nodeList.getNodeId() == 1 || nodeList.getNodeId() == 2) {
					sendItemPrice(nodeList, itemId);
				}
			}
			
		}
		
	}

	
	public static void saveCachedPrices() {
		try (Writer writer = new FileWriter(FILE_LOCATION)) {
		    Gson gson = new GsonBuilder().create();
		    gson.toJson(prices, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadCachedPrices() throws IOException {
		prices = new Gson().fromJson(FileUtils.readFileToString(new File(FILE_LOCATION)),new TypeToken<int[]>() { }.getType());
	}
	
	public static void sendItemPrice(Node node, int itemId) {
		Packet sendPacket = new Packet(PacketsToServerConfig.SEND_ITEM_PRICE); // Construct the packet
		sendPacket.writeInt(itemId);
		sendPacket.writeInt(prices[itemId]);
		node.write(sendPacket);
	}
	
}
