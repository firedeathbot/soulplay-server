package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

public class SendMessage implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		final String name = packet.readString();
		final String msg = packet.readString();
		
		sendMessage(node, name, msg);
		
	}

	public static void sendMessage(Node node, String receiverName, String msg) {
		Player player = node.getPlayerByName(receiverName);
		if (player != null) {
			Packet notifyPlayer = new Packet(PacketsToServerConfig.NOTIFY_PLAYER);
			notifyPlayer.writeString(receiverName);
			notifyPlayer.writeString(msg);
			player.getNode().write(notifyPlayer);
		}
	}

}
