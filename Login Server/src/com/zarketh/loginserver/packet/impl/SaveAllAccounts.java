package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

public class SaveAllAccounts implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		int nodeId = packet.readInt();
		
		Node saveNode = NodeManager.getInstance().getNode(nodeId);
		
		if (saveNode == null) {
			System.out.println("node id:"+nodeId+" is null. unable to save all accounts on that node.");
			return;
		} else {
			sendPacketToSaveAccounts(saveNode);
			System.out.println("Saving all accounts on node: "+nodeId);
		}
		
	}
	
	public static void sendPacketToSaveAccounts(Node node) {
		Packet sendPacket = new Packet(PacketsToServerConfig.SEND_PACKET_TO_SAVE_ACCOUNTS); // Construct the packet
		sendPacket.writeInt(1);
		node.write(sendPacket);
	}

}
