package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.Server;
import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.node.PunishmentHandler;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.NameUtils;
import com.zarketh.loginserver.util.SqlShit;

/**
 * A packet that handles the punishment of a player.
 * 
 * @author Aleksandr
 */

public class PunishPlayer implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		final String moderator = packet.readString();
		final String punishedName = packet.readString();
		final int punishmentType = packet.readByte();
		final long time = packet.readLong();
		final String reason = packet.readString();
		
		Server.SQL_SERVICE.submit(new Runnable() {
			
			@Override
			public void run() {
				
				final String punishedLoginName = SqlShit.getUserNameByDisplayName(punishedName);
				
				if (punishmentType == 254) { // regular kick command
					Player player = node.getPlayerByName(punishedLoginName);
					if (player != null) {
						Packet kickPlayer = new Packet(PacketsToServerConfig.KICK_PLAYER);
						kickPlayer.writeString(punishedLoginName);
						player.getNode().write(kickPlayer);
					}
					return;
				}
				
				int mId = LoginHandler.getMID(punishedLoginName);
				
				int currentPunishment = PunishmentHandler.getPunishmentTypeByID(mId); // if 99 then player is not banned
				
				if (currentPunishment == 99 && punishmentType >= Punishment.values.length) {
					SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(punishedLoginName)+" is not banned/muted");
					return;
				}
				
				if (punishmentType == Punishment.BAN_NAME.getPunishmentType()) { //regular playername ban
					if (PunishmentHandler.checkUser(punishedLoginName)) {
						//TODO: add NOTIFY_PLAYER packet to notify the player is already banned.
						SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(punishedLoginName)+" is already banned.");
						return;
					}
				} else if (punishmentType == Punishment.MUTE_NAME.getPunishmentType()) { // check if muted
					if (PunishmentHandler.checkMuted(punishedLoginName)) {
						SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(punishedLoginName)+" is already muted.");
						return;
					}
				} else if (punishmentType == Punishment.MUTE_IP.getPunishmentType()) { // check if ip muted
					if (PunishmentHandler.checkIpMuted(punishedLoginName)) {
						SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(punishedLoginName)+" is already IP muted.");
						return;
					}
				} else if (punishmentType == currentPunishment) { // new punishment checker handling unlike the top three methods lol
					String actionTaken = "None";
					for (Punishment p : Punishment.values) {
						if (p.getPunishmentType() == currentPunishment) {
							actionTaken = p.getDesc();
							break;
						}
					}
					SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(punishedLoginName)+" is already "+actionTaken);
					return;
				} else if (punishmentType >= Punishment.values.length) {
					int unbanType = punishmentType % Punishment.values.length;
					if (currentPunishment != 99) {
						if (currentPunishment == unbanType) {
							//TODO: unban
							PunishmentHandler.freePlayer(punishedLoginName, currentPunishment);


							String unBanType = "None";
							for (RemovePunishment p : RemovePunishment.values) {
								if (p.getPunishmentType() == unbanType) {
									unBanType = p.getDesc();
									break;
								}
							}
							
							SendMessage.sendMessage(node, moderator, "PlayerName:"+NameUtils.formatDisplayName(punishedLoginName)+" | Action Taken:"+unBanType);
							return;
						} else {

							String banType = "None";
							for (Punishment p : Punishment.values) {
								if (p.getPunishmentType() == unbanType) {
									banType = p.getDesc();
									break;
								}
							}
							
							SendMessage.sendMessage(node, moderator, "Unban/unmute error: "+NameUtils.formatDisplayName(punishedLoginName)+" is actually "+banType);
							return;
						}
					}
				}
				
				
				Player player = node.getPlayerByName(punishedLoginName);
				if (player != null) {
					
//					if (punishmentType == 1) { // check if ip banned //not possible to ip ban someone who is already ip banned lol, this is checking online players XD
//						if (PunishmentHandler.checkIP(player.getConnectedFrom())) {
//							notifyMod(node, moderator, NameUtils.formatDisplayName(punishedName)+" is already IP banned.");
//							return;
//						}
//					}
					
					PunishmentHandler.punishPlayer(player, punishmentType, time, reason, moderator);
					if (punishmentType == Punishment.BAN_NAME.getPunishmentType()) { // name ban
						if (time > 0) { // timed ban
							SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully time banned.");
						} else { // perma ban on name
							SendMessage.sendMessage(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully perma banned.");
						}
					} else {
						String actionTaken = "None";
						for (Punishment p : Punishment.values) {
							if (p.getPunishmentType() == punishmentType) {
								actionTaken = p.getDesc();
								break;
							}
						}
						SendMessage.sendMessage(node, moderator, "PlayerName:"+NameUtils.formatDisplayName(player.getUsername())+" | Action Taken:"+actionTaken);
					}
						
//						if (punishmentType == Punishment.BAN_IP.getPunishmentType()) { // IP ban
//						notifyMod(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully IP banned.");
//					} else if (punishmentType == Punishment.MUTE_NAME.getPunishmentType()) { // muted name
//						notifyMod(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully Muted.");
//					} else if (punishmentType == Punishment.MUTE_IP.getPunishmentType()) { // IP muted
//						notifyMod(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully IP muted.");
//					} else if (punishmentType == Punishment.BAN_UUID.getPunishmentType()) { // mac ban
//						notifyMod(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully mac banned.");
//					} else if (punishmentType == Punishment.MUTE_UUID.getPunishmentType()) { // mac mute
//						notifyMod(node, moderator, NameUtils.formatDisplayName(player.getUsername())+" has been successfully mac muted.");
//					}
					
					
					
					if (punishmentType == Punishment.BAN_NAME.getPunishmentType() || punishmentType == Punishment.BAN_IP.getPunishmentType() || punishmentType == Punishment.BAN_UUID.getPunishmentType()) {
						Packet kickPlayer = new Packet(PacketsToServerConfig.KICK_PLAYER);
						kickPlayer.writeString(punishedLoginName);
						player.getNode().write(kickPlayer);
					}
					
					sendPunishmentPacket(node, punishedLoginName, punishmentType);
				}
			}
		});
		
			
	}
	
	public void sendPunishmentPacket(Node node, String name, int punishmentType/*, long banLong*/) {
		Player player = node.getPlayerByName(name);
		if (player != null) {
			Packet punishPlayerPacket = new Packet(PacketsToServerConfig.PUNISH_PLAYER);
			punishPlayerPacket.writeString(name);
			punishPlayerPacket.writeByte(punishmentType);
//			punishPlayerPacket.writeLong(banLong);
			player.getNode().write(punishPlayerPacket);
		}
	}
	
	public enum Punishment {
		BAN_NAME(0, "Account Banned"),
		BAN_IP(1, "IP Banned"),
		MUTE_NAME(2, "Account Muted"),
		MUTE_IP(3, "IP Muted"),
		BAN_UUID(4, "MAC Banned"),
		MUTE_UUID(5, "MAC Muted")
//		UNBAN_NAME(6, "Unbanned Account"),
//		UNBAN_IP(7, "Unbanned IP"),
//		UNMUTED_NAME(8, "Unmutted Account"),
//		UNMUTED_IP(9, "Unmutted IP"),
//		UNBAN_UUID(10, "Unbanned MAC"),
//		UNMUTE_UUID(11, "Unmuted MAC")
		
		;
		
		public static final Punishment[] values = Punishment.values();
		
		private final int punishmentType;

		private final String desc;
		
		Punishment(int type, String description) {
			this.punishmentType = type;
			this.desc = description;
		}
		
		public int getPunishmentType() {
			return punishmentType;
		}

		public String getDesc() {
			return desc;
		}
		
		public static String getDescForType(int type) {
			for (Punishment p : Punishment.values)
				if (p.getPunishmentType() == type)
					return p.getDesc();
			return "";
		}
		
		public static Punishment getPunishmentForType(int type) {
			for (Punishment p : Punishment.values)
				if (p.getPunishmentType() == type)
					return p;
			return null;
		}
	}
	
	public enum RemovePunishment {
		UNBAN_NAME(Punishment.BAN_NAME.getPunishmentType(), "Unbanned Account"),
		UNBAN_IP(Punishment.BAN_IP.getPunishmentType(), "Unbanned IP"),
		UNMUTED_NAME(Punishment.MUTE_NAME.getPunishmentType(), "Unmuted Account"),
		UNMUTED_IP(Punishment.MUTE_IP.getPunishmentType(), "Unmuted IP"),
		UNBAN_UUID(Punishment.BAN_UUID.getPunishmentType(), "Unbanned MAC"),
		UNMUTE_UUID(Punishment.MUTE_UUID.getPunishmentType(), "Unmuted MAC");
		
		public static final RemovePunishment[] values = RemovePunishment.values();
		
		private final int punishmentType;

		private final String desc;
		
		RemovePunishment(int type, String description) {
			this.punishmentType = type;
			this.desc = description;
		}
		
		public int getPunishmentType() {
			return punishmentType;
		}

		public String getDesc() {
			return desc;
		}
		
	}
	
	public static void kickPlayer(Node node, String punishedName) {
		Packet kickPlayer = new Packet(PacketsToServerConfig.KICK_PLAYER);
		kickPlayer.writeString(punishedName);
		node.write(kickPlayer);
	}

}