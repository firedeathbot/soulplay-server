package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;

/**
 * 
 * @author Rene Roosen Gets the amount of players online from the loginserver,
 *         so we can display the total amount of multiple worlds
 */
public class PlayersOnline implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		short playerCount = 0;
		for (Node nodes : NodeManager.getInstance().getNodes().values()) {
			if (nodes == null) {
				continue;
			}
			playerCount += nodes.getPlayerCount();
		}

		Packet players = new Packet(PacketsToServerConfig.UPDATE_PLAYER_COUNT); // Construct the packet

		players.writeShort(playerCount); // Add the playercount to it

		for (Node nodes : NodeManager.getInstance().getNodes().values()) {
			if (nodes == null) {
				continue;
			}
			nodes.write(players);
		}

	}

}
