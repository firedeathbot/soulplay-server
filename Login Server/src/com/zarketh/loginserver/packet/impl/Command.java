package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.HttpPost;

/**
 * A packet that handles commands sent from the server.
 * 
 * @author Ultimate1
 */

public class Command implements PacketHandler {

	private final NodeManager nodeManager = NodeManager.getInstance();

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String command = packet.readString();
		if (command.equals("req_player_count")) {
			Packet response = new Packet(PacketsToServerConfig.NODE_PLAYER_COUNT);
			response.writeByte(nodeManager.getActiveNodeCount());
			for (Node n : nodeManager.getNodes().values()) {
				response.writeByte(n.getNodeId());
				response.writeShort(n.getPlayerCount());
			}
			node.write(response);
//			HttpPost.updatePlayerCount(count);
		}
		
		if (command.equals("disconnect_name")) {
			Packet response = new Packet(PacketsToServerConfig.NODE_PLAYER_COUNT);
			response.writeByte(nodeManager.getActiveNodeCount());
			for (Node n : nodeManager.getNodes().values()) {
				response.writeByte(n.getNodeId());
				response.writeShort(n.getPlayerCount());
			}
			node.write(response);
		}
		
	}

}