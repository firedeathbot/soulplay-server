package com.zarketh.loginserver.packet.impl;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.Player;
import com.zarketh.loginserver.packet.PacketHandler;
import com.zarketh.loginserver.packet.PacketsToServerConfig;
import com.zarketh.loginserver.util.NameUtils;

/**
 * A packet that handles the modification of the friends list.
 * 
 * @author Ultimate1
 */

public class ModifyFriends implements PacketHandler {

	@Override
	public void handlePacket(Node node, Packet packet) throws Exception {
		String name = packet.readString();
		int friendIndex = packet.readByte();
		long friend = packet.readLong();
		Player player = node.getPlayerByName(name);
		if (player != null) {
			if (friend > 0) { // adds
				Player other = node.getPlayerByName(NameUtils
						.longToString(friend));
				if (other == null
						&& player.getFriends().get(friendIndex) != null) {
					other = node.getPlayerByName(NameUtils.longToString(player
							.getFriends().get(friendIndex)));
				}
				player.setFriend(friendIndex, friend);
				if (other != null) {
					if (player.getPrivateChatMode() != 1) {
						// Stop this if we are not using the Friends chat mode.
						return;
					}
					if (other.hasFriend(player.getNameAsLong())) {
						Packet resp = new Packet(PacketsToServerConfig.SEND_PM_ONLINE_STATUS);
						resp.writeString(other.getUsername());
						resp.writeLong(player.getDisplayNameLong());
						if (friend == 0) {
							resp.writeByte(0);
						} else {
							resp.writeByte(node.getNodeId());
						}
						other.getNode().write(resp);
					}
				}
			} else if (friend == 0) { // removes
				player.removeFriend(friendIndex);
			}
		}
	}

}