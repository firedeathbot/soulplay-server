package com.zarketh.loginserver.packet;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.zarketh.loginserver.net.Packet;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.packet.impl.CheckDupedAccount;
import com.zarketh.loginserver.packet.impl.Command;
import com.zarketh.loginserver.packet.impl.FinaliseLogin;
import com.zarketh.loginserver.packet.impl.FriendStatus;
import com.zarketh.loginserver.packet.impl.LoginHandler;
import com.zarketh.loginserver.packet.impl.ModifyFriends;
import com.zarketh.loginserver.packet.impl.ModifyIgnoreList;
import com.zarketh.loginserver.packet.impl.PlayersOnline;
import com.zarketh.loginserver.packet.impl.PriceChecker;
import com.zarketh.loginserver.packet.impl.PrivateMessage;
import com.zarketh.loginserver.packet.impl.PunishPlayer;
import com.zarketh.loginserver.packet.impl.SaveAllAccounts;
import com.zarketh.loginserver.packet.impl.SendMessage;
import com.zarketh.loginserver.packet.impl.UnregisterPlayer;
import com.zarketh.loginserver.packet.impl.UpdateChatMode;
import com.zarketh.loginserver.packet.impl.YellForwarding;

/**
 * The login server packet manager.
 * 
 * @author Ultimate1
 */

public class PacketManager {

	/**
	 * The logger.
	 */
	private static Logger logger = Logger.getLogger(PacketManager.class
			.getName());

	/**
	 * Valid packets.
	 */
	private static final Map<Integer, PacketHandler> packetHandlers = new HashMap<Integer, PacketHandler>();

	/**
	 * Load the packet handlers.
	 */
	public static void initialise() {
		packetHandlers.put(GameServerPacketOpcode.UNREGISTER_PLAYER, new UnregisterPlayer());
		packetHandlers.put(GameServerPacketOpcode.PUNISH_PLAYER, new PunishPlayer());
		packetHandlers.put(GameServerPacketOpcode.FRIEND_STATUS, new FriendStatus());
		packetHandlers.put(GameServerPacketOpcode.PRIVATE_MESSAGE, new PrivateMessage());
		packetHandlers.put(GameServerPacketOpcode.LOGIN_VERIFICATION, new LoginHandler());
		packetHandlers.put(GameServerPacketOpcode.MODIFY_FRIENDS, new ModifyFriends());
		packetHandlers.put(GameServerPacketOpcode.MODIFY_IGNORES, new ModifyIgnoreList());
		packetHandlers.put(GameServerPacketOpcode.UPDATE_CHAT_MODE, new UpdateChatMode());
		packetHandlers.put(GameServerPacketOpcode.GLOBAL_MESSAGE, new YellForwarding());
		packetHandlers.put(GameServerPacketOpcode.SEND_MESSAGE, new SendMessage());
		packetHandlers.put(GameServerPacketOpcode.COMMAND_FROM_SERVER, new Command());
		packetHandlers.put(GameServerPacketOpcode.FINALIZE_LOGIN, new FinaliseLogin());
		packetHandlers.put(GameServerPacketOpcode.PLAYERS_ONLINE_REQUEST, new PlayersOnline());
		packetHandlers.put(GameServerPacketOpcode.CHECK_DUPED_ACCOUNT, new CheckDupedAccount());
		packetHandlers.put(GameServerPacketOpcode.PRICE_CHECK, new PriceChecker());
		packetHandlers.put(GameServerPacketOpcode.SAVE_CHARACTER_FILES, new SaveAllAccounts());
	}

	/**
	 * Handles a packet sent to the specified node.
	 * 
	 * @param packet
	 * @param node
	 */
	public static void handle(Packet packet, Node node) {
		int opcode = packet.readByte();
		if (!packetHandlers.containsKey(opcode)) {
			logger.info("Unhandled opcode : " + opcode + ".");
			return;
		}
		PacketHandler handler = packetHandlers.get(opcode);
		if (handler != null) {
			try {
				handler.handlePacket(node, packet);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error handling packet " + opcode
						+ ".", e);
			}
		}
	}

}