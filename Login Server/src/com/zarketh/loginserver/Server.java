package com.zarketh.loginserver;

import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import com.zarketh.loginserver.event.Task;
import com.zarketh.loginserver.event.TaskScheduler;
import com.zarketh.loginserver.net.ServerPipelineFactory;
import com.zarketh.loginserver.net.slack.SlackApi;
import com.zarketh.loginserver.net.slack.SlackLoginNotifier;
import com.zarketh.loginserver.net.slack.SlackMessageBuilder;
import com.zarketh.loginserver.net.slack.SlackThread;
import com.zarketh.loginserver.node.Node;
import com.zarketh.loginserver.node.NodeManager;
import com.zarketh.loginserver.packet.PacketManager;
import com.zarketh.loginserver.packet.impl.PriceChecker;
import com.zarketh.loginserver.util.PasswordEncrypter;
import com.zarketh.loginserver.util.SQL;
import com.zaxxer.hikari.HikariDataSource;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Logger;

/**
 * The main class thats up the login server.
 *
 * @author Ultimate1
 */

public class Server {
	
	public static final ExecutorService SQL_SERVICE = Executors.newCachedThreadPool();
	public static final ScheduledExecutorService LOGIN_SINGLE_THREAD = Executors.newSingleThreadScheduledExecutor();
	
	private static SlackApi slackApi = new SlackApi("https://hooks.slack.com/services/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");
	private static SlackSession slackSession = SlackSessionFactory.createWebSocketSlackSession("xoxb-39400643634-Jxdt42zxUq9QaQlIcJVy0Irw");
	private static SlackThread slackThread;
	
	public static SlackApi getSlackApi() {
		return slackApi;
	}
	
	public static SlackSession getSlackSession() {
		return slackSession;
	}
	
	private static final TaskScheduler scheduler = new TaskScheduler();
	
	public static TaskScheduler getTaskScheduler() {
		return scheduler;
	}
	
	/**
	 * The logger.
	 */
	private static final Logger logger = Logger.getLogger(Server.class
			.getName());
	
	/**
	 * The server bootstrap.
	 */
	private ServerBootstrap bootstrap = new ServerBootstrap();
	
	/**
	 * The server password.
	 */
	private static String password;
	
	/**
	 * @return the server password.
	 */
	public static String getPassword() {
		return password;
	}
	
	/**
	 * Initialises the server.
	 */
	public Server() {
		//bootstrap.setPipelineFactory(new ServerPipelineFactory());
		final boolean epoll = Epoll.isAvailable();
		bootstrap.group(epoll ? new EpollEventLoopGroup() : new NioEventLoopGroup());
		bootstrap.channel(epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class);
		bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
		bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
		bootstrap.childHandler(new ServerPipelineFactory());
	}
	
	/**
	 * Starts the server.
	 *
	 * @param host
	 * @param port
	 * @param password
	 * @throws InterruptedException
	 */
	static NodeManager nodeManager;
	
	public void start(String host, int port, String password) {
		port = 43597;
		password = Config.LOGIN_SERVER_PASSWORD;//"secretword";
		PacketManager.initialise();
		PasswordEncrypter.initialise();
		nodeManager = NodeManager.getInstance();
		final NodeManager nodeManager = NodeManager.getInstance();
		nodeManager.start();
		
		try {
			PriceChecker.loadCachedPrices();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		logger.info("Binding to port : " + port + "..");
		bootstrap.bind(new InetSocketAddress(host, port));
		logger.info("Ready and listening on port : " + port + ".");
		/*try {
			slackSession.connect();
			slackThread = new SlackThread();
		} catch (InterruptedException | IOException e) {
			getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(this.getClass(), e));
		}
		SlackLoginNotifier.loadListOnStartup();*/
		Server.password = password;
		// Shut down gracefully...
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				for (Node node : nodeManager.getNodes().values()) {
					nodeManager.unregister(node);
				}
				PriceChecker.saveCachedPrices();
			}
		});

//		Runnable playerCountUpdater = new Runnable() {
//			@Override
//			public void run() {
//				while (true) {
//					try {
//						new PlayersOnline().handlePacket(null, null);
//						Thread.sleep(30000);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		};
//		playerCountUpdater.run();
	
	}
	
	/**
	 * The main method that starts up the server.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
//		ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID);
		
		/**
		 * for mysql player saving
		 */
		try {
			SQL.initializeMySQLDriver();
			connectionPool = SQL.buildDataSource("localhost", 3306,
					Config.MYSQL_PLAYER_DB, Config.MYSQL_USERNAME, Config.MYSQL_PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("Starting Login Server");
		new Server().start("0.0.0.0", 43594, "none");

//		scheduler.schedule(new Task() {
//			@Override
//			protected void execute() {
//				
//			}
//		});
		
		getTaskScheduler().schedule(new Task(300) {
			@Override
			protected void execute() {
				PriceChecker.saveCachedPrices();
				
			}
		});
	}
	
	
	/**
	 * Connection pool for mysql player saves
	 */
	static HikariDataSource connectionPool = null;
	
	public static java.sql.Connection getConnection() {
		java.sql.Connection conn = null;
		try {
			conn = connectionPool.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static NodeManager getNodeManager() {
		return nodeManager;
	}
	
}
