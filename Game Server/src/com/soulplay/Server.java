package com.soulplay;

import com.soulplay.cache.EntityDef;
import com.soulplay.cache.ItemDef;
import com.soulplay.cache.ObjectDef;
import com.soulplay.config.World;
import com.soulplay.content.TeleportInterface;
import com.soulplay.content.clans.ClanManager;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.donate.DonationInterface;
import com.soulplay.content.event.randomevent.ShootingStar.StarInfo;
import com.soulplay.content.items.Food;
import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.impl.FairyRing;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.minigames.FPLottery;
import com.soulplay.content.minigames.FightPitsTournament;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.championsguild.ChampionsGuild.Champions;
import com.soulplay.content.minigames.ddmtournament.OfficialTournamentVariables;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager.LocationData;
import com.soulplay.content.minigames.penguin.PenguinHandler;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.treasuretrails.ClueManager;
import com.soulplay.content.npcs.impl.EmblemTrader;
import com.soulplay.content.npcs.impl.bosses.BossKCEnum;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareManager;
import com.soulplay.content.player.ArtifactData;
import com.soulplay.content.player.achievement.SkillCapes;
import com.soulplay.content.player.combat.magic.SpellsOfPI;
import com.soulplay.content.player.combat.range.BoltEffect;
import com.soulplay.content.player.dailytasks.task.TaskCategory;
import com.soulplay.content.player.pets.Egg.EggData;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.construction.StorageObjectEnum;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotFactory;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.impl.BedRoom;
import com.soulplay.content.player.skills.construction.room.impl.WorkshopRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.BindingData;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Manager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.items.DungeonItems;
import com.soulplay.content.player.skills.dungeoneeringv2.items.shop.DungeonRewardData;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonBosses;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonNpcs;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting.LeatherData;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting.Spinning;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.farming.Harvest;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching.ArrowData;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching.FletchingData;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching.StringingData;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.SmeltingDataDung;
import com.soulplay.content.player.skills.farming.herb.Herb;
import com.soulplay.content.player.skills.farming.tree.Tree;
import com.soulplay.content.player.skills.firemaking.LogData;
import com.soulplay.content.player.skills.hunting.trap.BoxTrap;
import com.soulplay.content.player.skills.magic.EnchantJewelry.Enchant;
import com.soulplay.content.player.skills.mining.Pickaxes;
import com.soulplay.content.player.skills.mining.Rocks;
import com.soulplay.content.player.skills.smithing.SmeltingData;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.content.player.skills.smithing.impl.*;
import com.soulplay.content.player.skills.summoning.Creation;
import com.soulplay.content.player.skills.summoning.Summoning.Familiar;
import com.soulplay.content.player.skills.woodcutting.Hatchets;
import com.soulplay.content.player.skills.woodcutting.Trees;
import com.soulplay.content.poll.PollManager;
import com.soulplay.content.raids.RaidsRewards;
import com.soulplay.content.vote.VoteInterfaceStoreData;
import com.soulplay.content.vote.VoteInterfaceStreak;
import com.soulplay.discord.DiscordBot;
import com.soulplay.game.event.*;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.NpcPlugins;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.*;
import com.soulplay.game.model.region.RegionManager;
import com.soulplay.game.sounds.Music;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.ShopHandler;
import com.soulplay.game.world.StillGraphicsManager;
import com.soulplay.net.ChannelHandler;
import com.soulplay.net.Connection;
import com.soulplay.net.login.RS2Encoder;
import com.soulplay.net.login.RS2LoginProtocol;
import com.soulplay.net.loginserver.GameServerPacketOpcode;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.loginserver.LoginServerPacket;
import com.soulplay.net.packet.PacketHandler;
import com.soulplay.net.slack.SlackApi;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.net.slack.SlackThread;
import com.soulplay.plugin.PluginManager;
import com.soulplay.util.ControlPanel;
import com.soulplay.util.PlayerLoopThread;
import com.soulplay.util.SystemMonitor;
import com.soulplay.util.log.LogWriter;
import com.soulplay.util.log.Logger;
import com.soulplay.util.log.TestLogWriter;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.sql.LogHandler;
import com.soulplay.util.sql.SQL;
import com.soulplay.util.sql.SqlHandler;
import com.soulplay.util.sql.WebsiteSQL;
import com.soulplay.util.sql.clanelo.ClanEloHandler;
import com.soulplay.util.sql.configs.CombatConfigs;
import com.soulplay.util.sql.scoregrab.DuelScoreBoard;
import com.soulplay.util.sql.scoregrab.LmsScoreBoard;
import com.soulplay.util.sql.scoregrab.WildyEloRating;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.matchprocessor.SubclassMatchProcessor;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import plugin.skills.construction.TeleportFocusPlugin;
import plugin.skills.magic.HumidifyPlugin;

import java.net.InetSocketAddress;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * The main class needed to start the server.
 *
 * @author Garbage people, cuz garbage people are awesome
 */
public class Server {
	
	public static final int CYCLE_RATE = 600;
	
	private static Map<String, Integer> spammers = new HashMap<>();
	
	private static StillGraphicsManager stillGraphicsManager = null;
	
	private static boolean keepLogs = false;
	
	private static boolean systemLogs = true;
	
	private static boolean savedLists = false;
	
	private static LogWriter logWriter;
	
	private static TestLogWriter testLogWriter;
	
	private static volatile boolean updateServer = false;
	
	private static PlayerLoopThread playerLoopThread;
	
	private static PlayerSaveThread playerSaveThread;
	
	private static long sleepTime;
	
	private static int serverlistenerPort;
	
	private static HikariDataSource connectionPool;
	
	private static SlackApi slackApi = new SlackApi("https://hooks.slack.com/services/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");
	
	private static SlackSession slackSession = SlackSessionFactory.createWebSocketSlackSession("xoxb-39400643634-Jxdt42zxUq9QaQlIcJVy0Irw");
	
	private static SystemMonitor systemMonitor;
	
	private static SlackThread slackThread;
	
	private static Calendar startupTime;
	
	private static final TaskScheduler scheduler = new TaskScheduler();
	
	private static ControlPanel panel;
	
	private static CalendarHandler calendar = new CalendarHandler();
	
	private static CalendarEvents calEvent = new CalendarEvents();
	
	private static CalendarScheduler calScheduler = new CalendarScheduler();
	
	private static volatile int totalTicks = 0;
	
	private static int tick5 = 0;
	
	private static volatile int newbieTick = 0;
	
	private static long delay2 = 0;
	
	private static long delay3 = 0;
	
	private static RegionManager regionManager = new RegionManager();
	
	private static final ServerBootstrap bootstrap = new ServerBootstrap();
	
	private static volatile boolean launched;
	
	private static volatile boolean runningMainTask = false;
	
	private static DBConfig dbConfig;
	
	public static boolean displayError = true;
	
	private static World world;
	
	static {
		if (!Config.SERVER_DEBUG) {
			serverlistenerPort = Config.SERVER_PORT;
		} else {
			serverlistenerPort = 43590;
		}
		sleepTime = 0;
	}
	
	/**
	 * Java connection. Ports.
	 */
	
	private static void bind() {
		final int nativeThreads = Runtime.getRuntime().availableProcessors();
		final boolean epoll = Epoll.isAvailable();
		final int gameThreads = nativeThreads > 1 ? nativeThreads - 1 : 1;
		final EventLoopGroup group = epoll ? new EpollEventLoopGroup(gameThreads) : new NioEventLoopGroup(gameThreads);
		final Class<? extends ServerSocketChannel> serverChannelClass = epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class;
		final Class<? extends SocketChannel> clientChannelClass = epoll ? EpollSocketChannel.class : NioSocketChannel.class;
		
		//ye seems shared, maybe we can try not sharing?
		
		final RS2Encoder rs2Encoder = new RS2Encoder();//where's the login server one?
		bootstrap
				.group(group)
				.channel(serverChannelClass)
				.childHandler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel ch) throws Exception {
						ChannelPipeline pipeline = ch.pipeline();
						ChannelHandler channelHandler = new ChannelHandler();
						
						pipeline.addLast("timeout", new IdleStateHandler(15, 0, 0));
						pipeline.addLast("encoder", rs2Encoder);
						pipeline.addLast("decoder", new RS2LoginProtocol(channelHandler));
						pipeline.addLast("handler", channelHandler);
					}
					
				}).childOption(ChannelOption.TCP_NODELAY, true);
		try {
			bootstrap.bind(new InetSocketAddress("0.0.0.0", serverlistenerPort)).sync();
		} catch (InterruptedException e) {
			getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(Server.class, e));
		}
		
		final int loginThreads = 1;
		LoginServerConnection.group = epoll ? new EpollEventLoopGroup(loginThreads) : new NioEventLoopGroup(loginThreads);
		LoginServerConnection.channelClass = clientChannelClass;
		
		connectToLoginServer();
	}
	
	public static void connectToLoginServer() {
		LoginServerConnection c = LoginServerConnection.instance();
		ChannelFuture f = c.connect(/* "127.0.0.1", 43598 */ Config.LOGIN_SERVER_IP, Config.LOGIN_SERVER_PORT);
		f.addListener((ChannelFuture future) -> {
			boolean success = future.isSuccess();
			System.out.println("Successfully Connected to Login Server: " + success);
			if (success) {
				Channel channel = future.channel();
				channel.writeAndFlush(new LoginServerPacket(
								GameServerPacketOpcode.REQUEST_CONNECTION_TO_LOGINSERVER)
								.writeByte(Config.NODE_ID).writeString(
								Config.LOGIN_SERVER_PASSWORD),
						channel.voidPromise());
			}
		});
	}
	
	public static CalendarHandler getCalendar() {
		return calendar;
	}
	
	public static CalendarEvents getCalEvent() {
		return calEvent;
	}
	
	public static CalendarScheduler getCalScheduler() {
		return calScheduler;
	}
	
	public static java.sql.Connection getConnection() {
		java.sql.Connection conn = null;
		try {
			conn = connectionPool.getConnection();
		} catch (Exception e) {
			getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(Server.class, e));
		}
		return conn;
	}
	
	public static DBConfig getDBConfig() {
		return dbConfig;
	}
	
	public static boolean getKeepLogs() {
		return keepLogs;
	}
	
	public static LogWriter getLogWriter() {
		return logWriter;
	}
	
	public static int getNewbieTick() {
		return newbieTick;
	}
	
	public static ControlPanel getPanel() {
		return panel;
	}
	
	public static PlayerLoopThread getPlayerLoopThread() {
		return playerLoopThread;
	}
	
	public static PlayerSaveThread getPlayerSaveThread() {
		return playerSaveThread;
	}
	
	public static RegionManager getRegionManager() {
		return regionManager;
	}
	
	public static SlackApi getSlackApi() {
		return slackApi;
	}
	
	public static SlackSession getSlackSession() {
		return slackSession;
	}
	
	public static SlackThread getSlackThread() {
		return slackThread;
	}
	
	/**
	 * Gets the sleep mode timer and puts the server into sleep mode.
	 */
	public static long getSleepTimer() {
		return sleepTime;
	}
	
	public static Map<String, Integer> getSpammers() {
		return spammers;
	}
	
	public static Calendar getStartupTime() {
		return startupTime;
	}
	
	/**
	 * Gets the Graphics manager.
	 */
	public static StillGraphicsManager getStillGraphicsManager() {
		return stillGraphicsManager;
	}
	
	public static boolean getSystemLogs() {
		return systemLogs;
	}
	
	public static SystemMonitor getSystemMonitor() {
		return systemMonitor;
	}
	
	public static TaskScheduler getTaskScheduler() {
		return scheduler;
	}
	
	public static TestLogWriter getTestLogWriter() {
		return testLogWriter;
	}
	
	public static boolean getUpdateServer() {
		return updateServer;
	}
	
	public static boolean isLaunched() {
		return launched;
	}
	
	public static boolean isRunningMainTask() {
		return runningMainTask;
	}
	
	public static World getWorld() {
		return world;
	}
	
	/**
	 * Starts the server.
	 *
	 * @throws Exception
	 */
	
	public static void main(java.lang.String args[]) throws Exception {
//		ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID); // for
		// now,
		// need
		// to
		// disable
		// for
		// production!
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			
			@Override
			public void run() {
				if (!updateServer) {
					PlayerHandler.setKickAllPlayers(true);
					for (Player p : PlayerHandler.players) {
						if (p == null) {
							continue;
						}
						p.skipThreadCheck = true;
						Client c = (Client) p;
						c.handleDCLogoutBeforeSave();
						PlayerSave.saveGame(c, true);
						c.destruct();
					}
					if (Server.getWorld().getId() >= 1 && Server.getWorld().getId() <= 5 && Config.RUN_ON_DEDI && !Config.SERVER_DEBUG) {
						getSlackApi().call(SlackMessageBuilder.buildStartMessage("SoulSplit", true, "#automation"));
						getSlackApi().call(SlackMessageBuilder.buildStartMessage("SoulSplit", true, "#developers"));
					}
					saveLists();
				}
			}
		});
		
		long startTime = System.currentTimeMillis();
		
		if (args.length > 0) {
			if (args[0] != null && "false".equals(args[0])) {
				Config.RUN_ON_DEDI = false;
				ConstructionUtils.DISABLED_DROP = true;
				System.out.println("Setting run on dedi false");
			}
			if (args.length > 1 && args[1] != null && args[1].length() > 0) {
				serverlistenerPort = Integer.parseInt(args[1]);
				System.out.println("Setting Port On Server: " + serverlistenerPort);
			}
			if (args.length > 2 && args[2] != null && args[2].length() > 0) {
				Config.SERVER_NAME = args[2];
				Config.SERVER_NAME_SHORT = args[2];
				System.out.println("Setting Server Name: " + Config.SERVER_NAME);
			}
			if (args.length > 3 && args[3] != null && args[3].length() > 0) {
				Config.NODE_ID = Integer.parseInt(args[3]);
				System.out.println("Setting Node ID: " + Config.NODE_ID);
			}
			if (args.length > 4 && args[4] != null && args[4].length() > 0) {
				Config.LOGIN_SERVER_IP = args[4];
				System.out.println("Setting Login Server IP: " + Config.LOGIN_SERVER_IP);
			}
			
			if (args.length > 5 && args[5] != null && args[5].equals("false")) {
				Config.SHOW_CPANEL = false;
				System.out.println("Setting Control Panel Visibility off");
			}
		}
		
		if (Config.NODE_ID == 1) {
			if ("SoulEco".equals(Config.SERVER_NAME)) {
				Config.NODE_ID = 1;
			} else if ("SoulPvP".equals(Config.SERVER_NAME)) {
				Config.NODE_ID = 5;
			}
		}
		
		if (Config.NODE_ID == 3 || Config.NODE_ID == 4) {
//			Config.setAsPvpWorld();
		}
		
		if (Config.NODE_ID == 8) {
			Config.SERVER_DEBUG = true;
			ConstructionUtils.DISABLED_DROP = true;
			Config.TRADING_POST_ENABLED = false;
		}
		
		if (Config.SHOW_CPANEL) {
			panel = new ControlPanel(Config.SHOW_CPANEL);
			panel.setTitle(Config.SERVER_NAME + " World: " + Config.NODE_ID + " ControlPanel");
		}
		
		//set the world
		world = World.getWorld();

		System.setOut(new Logger(System.out));
		System.setErr(new Logger(System.err));

		// System.setOut(new OutLogger(System.out)); // print out class name and
		// line
		// System.setErr(new OutLogger(System.err)); // print out class name and
		// line
		
		if (QuestIndex.checkDuplicates()) {
			System.out.println("ERROR DUPLICATE ENTRIES IN QUESTINDEX ENTRIES!!!!");
			return;
		}
		
		/**
		 * for mysql player saving
		 */
		if (!Config.SERVER_DEBUG) {
			try {
				SQL.initializeMySQLDriver();
				HikariConfig config = SQL.getDefaultConfig();
				if (Config.TEST_SQL) {
					config.setJdbcUrl("jdbc:mysql://" + Config.MYSQL_HOST_TEST + ":3306/" + Config.MYSQL_PLAYER_DB + Config.MYSQL_DB_EXTENSIONS);// Database
					config.setUsername(Config.MYSQL_USERNAME_TEST);// User
					config.setPassword(Config.MYSQL_PASSWORD_TEST);// Password
				} else {
					if (Config.NODE_ID == 2) { // eu world // shit version of
						// doing it
						config.setJdbcUrl("jdbc:mysql://" + Config.MYSQL_PLAYERSAVE_HOST + ":3306/" + Config.MYSQL_PLAYER_DB + Config.MYSQL_DB_EXTENSIONS);// Database
						config.setUsername(Config.MYSQL_USERNAME_EU);// User
						config.setPassword(Config.MYSQL_PASSWORD_EU);// Password
					} else { // local worlds
						config.setJdbcUrl("jdbc:mysql://localhost/" + Config.MYSQL_PLAYER_DB + Config.MYSQL_DB_EXTENSIONS);// Database
						config.setUsername(Config.MYSQL_USERNAME);// User
						config.setPassword(Config.MYSQL_PASSWORD);// Password
					}
				}
				
				connectionPool = new HikariDataSource(config);
			} catch (ClassNotFoundException e) {
				if (Config.RUN_ON_DEDI) {
					Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(Server.class, e));
				} else {
					e.printStackTrace();
				}
			}
		}
		
		PlayerSaveSql.nameThread();
		
		//filter LMS items
		//AlterAccounts.removeItems(30100, 30101, 30102, 30103, 30104, 30105, 30106, 30107, 30123, 30124, 30125, 30126, 30127, 30128, 30129, 30131, 30132, 30133, 30134, 30135);
		
		// Loading Entity Region
		
		if (Config.SERVER_CLIPPING) {
			ObjectDef.unpackConfig(); // 474 object configs
			RegionClip.loadMaps();
		}
		ObjectManager.importCustomObjects();
		
		RegionClip.finishLoadingMaps = true;
		
		// Start fight pits process
		FightPitsTournament.startProcess();
		
		SpellsOfPI.init();
		
		// temp shit like halloween event
		
		try {
			SqlHandler.init();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(Server.class, e));
		}
		
		try {
			new LogHandler();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(Server.class, e));
		}
		
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			testSettings();
		}
		
		if (Config.USE_NEW_DBCONFIG) {
			
			TaskExecutor.executeWebsiteSQL(() -> {
				
				try {
					System.out.println("Connecting to websitesql for database config.");
					dbConfig = new DBConfig();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				try {
					System.out.println("Updating settings from SQL");
					DBConfig.updateSettings();
					System.out.println("Finished updating settings from SQL");
				} catch (Exception e2) {
					System.out.println("CANNOT update settings from SQL");
					e2.printStackTrace();
				}
				
				// try {
				// new AnnouncementManager();
				// } catch (InterruptedException e3) {
				// e3.printStackTrace();
				// } catch (Exception e4) {
				// e4.printStackTrace();
				// }
				
			});
		}
		
		if (!Config.SERVER_DEBUG) {
			// PKBoards.loadBoards();
			FPLottery.loadLists();
		}
		
		Manager.init(); //load this before NPCHandler.loadAutoSpawn();
		NpcPlugins.init();
		/**
		 * loading definitions
		 */
		EntityDef.unpackConfig();
		ItemDef.unpackConfig();
		
		NPCHandler.init();
		NPCHandler.loadNpcHandler();
		
		CastleWars.init(); // start the cw process
		SoulWars.init();
		LmsManager.init();
		
		//Random shit
		long time = System.currentTimeMillis();
		SkillsSaveEnum.init();
		TeleportFocusPlugin.load();
		HumidifyPlugin.load();
		PacketHandler.load();
		ArtifactData.load();
		Music.load();
		GsonSave.load();
		TeleportInterface.load();
		HotspotType.load();
		BedRoom.load();
		Trees.load();
		Hatchets.load();
		Creation.Data.load();
		SmeltingData.load();
		SmithingInterface.load();
		Bronze.load();
		Iron.load();
		Steel.load();
		Mithril.load();
		Adamant.load();
		Rune.load();
		Dragon.load();
		Argonite.load();
		Bathus.load();
		Fractite.load();
		Gorgonite.load();
		Katagon.load();
		Kratonite.load();
		Marmaros.load();
		Novite.load();
		Promethium.load();
		Zephyrium.load();
		Pickaxes.load();
		Rocks.load();
		Enchant.load();
		BoxTrap.load();
		LogData.load();
		Herb.load();
		Tree.load();
		DungeonConstants.load();
		DungeonRewardData.load();
		DungeonBosses.load();
		DungeonNpcs.load();
		DungeonItems.load();
		LeatherData.load();
		Spinning.load();
		Harvest.load();
		ArrowData.load();
		FletchingData.load();
		StringingData.load();
		SmeltingDataDung.load();
		BindingData.load();
		StorageObjectEnum.load();
		WorkshopRoom.load();
		Pet.load();
		PokemonData.load();
		BoltEffect.load();
		TaskCategory.load();
		SkillCapes.load();
		BossKCEnum.load();
		Champions.load();
		LocationData.load();
		CompletionistCape.load();
		FairyRing.load();
		Food.load();
		StarInfo.load();
		SpecialAttack.load();
		SoulPlayDate.createLastSunday();
		ClueManager.load();
		EggData.load();
		RaidsRewards.load();
		VoteInterfaceStreak.load();
		VoteInterfaceStoreData.load();
		System.out.println("Loaded random data in " + (System.currentTimeMillis() - time) + "ms");

		PollManager.loadPolls();
		SeasonPassManager.updateSeasonPassSettings(true);
		OfficialTournamentVariables.pullSettings();
		ItemDefinition.load();
		System.out.println("Loading ItemHandler");
		ItemHandler.init();
		ItemProjectInsanity.load();
		FifthItemAction.loadMap();
		CombatConfigs.parse();
//		System.out.println("start ---------------------------------------------------");
//		AlterAccounts.scanStrangeEquip();
//		System.out.println("done-------------------------------------------------");
		PenguinHandler.initialize();
		EmblemTrader.spawnEmblemTrader();
		NightmareManager.spawn();
		//christmas event start
		//SnowmanPlugin.spawnSnow();
		//PresentSpawn.spawn();
		//christmas event end
		//HalloweenDungeonNpc.spawnHalloweenDungeonNpc();
		//HalloweenCityNpc.spawnHalloweenCityNpc();
		//HalloweenWildernessNpc.spawnHalloweenWildernessNpc();
		//HalloweenMinigameNpc.spawnHalloweenMiningameNpc();
		System.out.println("Loading Familiars.");
		Familiar.loadFamiliars();
		System.out.println("Familiars Loaded.");
		
		System.out.println("Loading drop table");
		DropTable.loadDropTables(null);
		DropTable.initDropMaker();
		System.out.println("Loaded drop maker XD");
		System.out.println("Lauching: " + Config.SERVER_NAME + " V" + Config.CLIENT_VERSION);
		System.out.println("NPC Drops Loaded");
		System.out.println("NPC Spawns Loaded");
		ShopHandler.init();
		System.out.println("Shops Loaded");
		System.out.println("Object Spawns Loaded");
		
		PluginManager.load();
		
		loadExtraResources();
		
		try {
			PriceChecker.load();
			PriceChecker.loadCachedPrices();
			System.out.println("Loaded price checker.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (Config.RUN_ON_DEDI) {
			try {
				WebsiteSQL.init();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		DonationInterface.load();

		try {
			WildyEloRating.reloadWildyEloScores();
			ClanEloHandler.reloadClanEloScores();
			DuelScoreBoard.reloadDuelScores();
			LmsScoreBoard.reloadDuelScores();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		// checkItemArray(); To check reward items and shit with names
		
		stillGraphicsManager = new StillGraphicsManager();
		
		startupTime = Calendar.getInstance();
		if (DBConfig.UPDATE_UPTIME) {
			SqlHandler.setStartupTime(); // this runs in
			// TaskExecutor.executeWebsiteSQL so
			// it will need to wait until a
			// connection is made anyways, no
			// need to move this.
		}
		playerLoopThread = new PlayerLoopThread();
		// if (Config.NODE_ID == 2) {//if (Config.RUN_ON_DEDI &&
		// !Config.SERVER_DEBUG && Config.SERVER_PORT == 43599) {
		// playerSaveThread = new PlayerSaveThread();
		// }
		logWriter = new LogWriter();
		System.out.println("Logger thread started");
		// testLogWriter = new TestLogWriter();
		// System.out.println("Test Logger thread started");
		// loginQueueProcess = new LoginQueueProcess();
		Connection.initialize();
		
		ItemDefinition.isLoaded = true;
		
		// systemMonitor = new SystemMonitor();
		// System.out.println("System Monitor started");
		
		long endTime = System.currentTimeMillis();
		long elapsed = endTime - startTime;
		System.out.println("Server started up in " + elapsed + " ms");
		System.out.println("Server listening on port 127.0.0.1:" + serverlistenerPort);
		if (Config.RUN_ON_DEDI && !Config.SERVER_DEBUG && Config.SERVER_PORT == 43599) {
			getSlackApi().call(SlackMessageBuilder.buildStartMessage(Config.SERVER_NAME + " Node:" + Config.NODE_ID, false, "#developers"));
			getSlackApi().call(SlackMessageBuilder.buildStartMessage(Config.SERVER_NAME + " Node:" + Config.NODE_ID, false, "#automation"));
		}
		
		// PlayerSaveSql.fixPasswords();

//		slackThread = new SlackThread();
		
		//slackSession.connect();
		calScheduler.initialize();
		
		System.gc(); // run gc after startup
		
		bind();
		
		// scheduler.schedule(new Task(false, false, false) {
		//
		// @Override
		// protected void execute() {
		// PlayerHandler.processPackets300ms();
		// // PlayerHandler.unregisterPlayers();
		// }
		// });
		
		scheduler.schedule(new Task() {
			
			@Override
			protected void execute() {
				runningMainTask = true;
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(600 - (time - delay2));
					if (delta > 3) {
						System.out.println("Server Lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				CycleEventHandler.getSingleton().process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("CycleEvent lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				PlayerHandler.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("PlayerHandler lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				NPCHandler.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("NPCHandler lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				ItemHandler.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("ItemHandler lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				ShopHandler.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("ShopHandler lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				ObjectManager.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("ObjectManager lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				PestControl.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("PestControl lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				FPLottery.process();
				
				if (DBConfig.LOG_LAG) {
					long time = System.currentTimeMillis();
					long delta = Math.abs(time - delay3);
					if (delta > 3) {
						System.out.println("FPLottery lag: " + delta);
					}
					
					delay3 = System.currentTimeMillis();
				}
				
				if (tick5 > 2) {
					tick5 = 0;
					if (!Connection.getLastIpConnecting().isEmpty()) {
						Connection.getLastIpConnecting().clear(); // the other
						// method
						// was too
						// slow.
					}
					if (!Connection.getLastUUIDConnecting().isEmpty()) {
						Connection.getLastUUIDConnecting().clear();
					}
				}
				tick5++;
				
				// if (totalTicks >= 800 && totalTicks % 800 == 0) {
				// // clear the IP to connected list every 100 ticks
				// Connection.IP_TO_CONNECTED.clear();
				// }

//				if (Config.RUN_ON_DEDI)
//					if (totalTicks >= 1500 && totalTicks % 1500 == 0) { // save
//						// every
//						// 15
//						// minutes
//						TaskExecutor.execute(new Runnable() {
//							
//							@Override
//							public void run() {
//								try {
//									PlayerSaveThread.runSaves();
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
//							}
//						});
//					}
				
				if (newbieTick > 0) {
					newbieTick--;
				}
				
				// PlayerHandler.unregisterPlayers();
				
				if (DBConfig.LOG_LAG) {
					delay2 = System.currentTimeMillis();
				}
				
				if (ItemProjectInsanity.reloadItems) {
					ItemProjectInsanity.reloadItems();
					FifthItemAction.loadMap();
					System.out.println("RELOADING ITEMS FROM DATABASE!!!!");
					ItemProjectInsanity.reloadItems = false;
				}
				
				RS2LoginProtocol.removeFirstQueue();
				
				if (!Server.displayError) {
					Server.displayError = true;
				}
				
				setTotalTicks(getTotalTicks() + 1);
				
				runningMainTask = false;
			}
		});
		
		launched = true;
	}

	public static boolean ensureOnMainThread() {
		/*Thread thread = Thread.currentThread();
		String threadName = thread.getName();
		boolean mainThread = threadName.equals("pool-4-thread-1");
		if (!mainThread) {
			new Exception("Not on main thread. On thread \"" + threadName + "\"").printStackTrace();
			return false;
		}*/

		return true;
	}

	private static void loadExtraResources() {
		
		// used for construction so i don't have to instantiate 100+ objects
		// manually, instead this library
		// will find all that classes that match a certain type and it will
		// instantiate them and cache them for me.
		// this is also useful i don't have to have a gigantic switch statement
		// or hardcode a map with a ton of mappings.
		FastClasspathScanner scanner = new FastClasspathScanner();
		
		scanner.matchSubclassesOf(Hotspot.class, new SubclassMatchProcessor<Hotspot>() {
			
			@Override
			public void processMatch(Class<? extends Hotspot> clazz) {
				
				try {
					Hotspot hotspot = clazz.newInstance();
					
					HotspotFactory.hotspots.put(hotspot.getType(), hotspot);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				
			}
			
		}).scan();
	}
	
	public static void saveLists() {
		if (savedLists) {
			return;
		}
		savedLists = true;
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			return;
		}
		try {
			PriceChecker.saveToFile();
			PriceChecker.saveCachedPrices();
			// PKBoards.saveBoards();
			FPLottery.saveLists();
			Connection.saveLists();
			SqlHandler.saveScrollInfo(PlayerHandler.scrollsBought, PlayerHandler.scrollsUsed);
			ClanManager.saveAllActiveClans();
			PenguinHandler.savePenguinNames();
			SqlHandler.updatePlayerCount(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		} catch (Exception e) {
			getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(Server.class, e));
		}
	}
	
	public static void schedule(int delay, Runnable runnable) {
		getTaskScheduler().schedule(delay, runnable);
	}
	
	public static void schedule(Runnable runnable) {
		schedule(1, runnable);
	}
	
	public static void setNewbieTick(int newbieTick) {
		Server.newbieTick = newbieTick;
	}
	
	public static void setUpdateServer(boolean value) {
		updateServer = value;
	}
	
	public static void testSettings() {
		DBConfig.LOG_DROP = true;
		DBConfig.LOG_PICKUP = true;
		DBConfig.LOG_STAKE = true;
		DBConfig.LOG_TRADE = true;
		DBConfig.LOG_STAFF = false;
		DBConfig.LOG_KILL = true;
		DBConfig.LOG_LOGIN = true;
		DBConfig.UPDATE_UPTIME = false;
		DBConfig.LOG_LAG = false;
	}
	
	public static void toggleKeepLogs() {
		keepLogs = !keepLogs;
	}
	
	public static void toggleSystemLogs() {
		systemLogs = !systemLogs;
	}
	
	private Server() {
	}
	
	public static int getTotalTicks() {
		return totalTicks;
	}
	
	public static void setTotalTicks(int totalTicks) {
		Server.totalTicks = totalTicks;
	}
	
}
