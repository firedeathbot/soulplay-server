package com.soulplay.config;

import com.soulplay.Server;

public enum WorldType {
	ECONOMY(0),
	SPAWN(1),
	SEASONAL(2);

	private final int id;

	private WorldType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static boolean equalsType(WorldType type) {
		return Server.getWorld().getType() == type;
	}

	public static boolean isSeasonal() {
		return Server.getWorld().getType() == SEASONAL;
	}

}
