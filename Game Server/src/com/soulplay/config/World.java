package com.soulplay.config;

import com.soulplay.Config;

public enum World {
	NA_ECO(1, "NA SoulEco", WorldType.ECONOMY),
	EU_ECO(2, "EU SoulEco", WorldType.ECONOMY),
	NA_PVP(3, "NA SoulPvp", WorldType.ECONOMY),
	NA_SPAWN(5, "NA SpawnPvp", WorldType.SPAWN),
	TEST_SERVER1(6, "Test Server", WorldType.ECONOMY),
	PORT_WORLD(7, "Port World", WorldType.ECONOMY),
	SEASONAL_WORLD(9, "Seasonal World", WorldType.SEASONAL);

	private static World[] values = World.values();
	private final int id;
	private final String desc;
	private final WorldType type;

	private World(int id, String desc, WorldType type) {
		this.id = id;
		this.desc = desc;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

	public WorldType getType() {
		return type;
	}

	public static World getWorld() {
		return getWorld(Config.NODE_ID);
	}

	public static World getWorld(int value) {
		for (int i = 0; i < values.length; i++) {
			if (values[i].getId() == value)
				return values[i];
		}

		return TEST_SERVER1;
	}

}