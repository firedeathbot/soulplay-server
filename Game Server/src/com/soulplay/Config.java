package com.soulplay;

import java.util.Calendar;

import com.soulplay.config.WorldType;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;

/**
 * The Configuration File Of The Server
 */
public class Config {

	/**
	 * The client version you are using.
	 */
	public static final int CLIENT_VERSION = 31;

	/**
	 * The value that indicates the default brightness that will be set on the players client upon account creation.
	 *
	 * The values range from 1-4, 1 being the lowest setting (dark) and 4 being the highest (brightest)
	 */
	public static final int DEFAULT_BRIGHTNESS = 3;
	public static final int DEFAULT_MUSIC_LEVEL = 2; // level 3 (middle)

	/**
	 * Enable or disable server debugging.
	 */
	public static boolean SERVER_DEBUG = false;

	public static final boolean SERVER_CLIPPING = true;

	public static final boolean TEST_SQL = false; // keep false

	public static boolean RUN_ON_DEDI = false; // keep true

	public static boolean USE_NEW_DBCONFIG = true; // Auto updating config
													// settings

	public static boolean USE_NEW_HIGHSCORES = true;

	public static int NODE_ID = 1; // 1 = eco world - 2 = pvp world

	/**
	 * Your server name.
	 */
	public static /* final */ String SERVER_NAME = "SoulEco";

	public static /* final */ String SERVER_NAME_SHORT = "SoulEco";

	public static final int SERVER_PORT = 43599;

	public static String LOGIN_SERVER_IP = "127.0.0.1"; // "127.0.0.1";

	public static final int LOGIN_SERVER_PORT = 43597;

	/**
	 * The welcome message displayed once logged in the server.
	 */
	public static final String WELCOME_MESSAGE = "Welcome To SoulPlay";

	/**
	 * A URL to your server forums. Not necessary needed.
	 */
	public static final String FORUMS = "http://www.soulplayps.com/forums/index.php";

	public static final String VOTE = "http://www.soulplayps.com/vote/?username=%s";

	public static final String FACEBOOK = "http://www.facebook.com/SoulSplit-3-488838094623168/?fref=ts";// CHANGE
																											// TO
																											// SS
																											// FACEBOOK

	public static final String MOTIVOTE_URL = "http://www.soulplayps.com/vote/";

	public static final String MOTIVOTE_ID = "9e740b89";

	public static final String CLANPOINTS_URL = "http://www.soulplayps.com/excludecf/addclanpoints.php";

	public static final boolean XENFORO_LINK = false;

	public static final String XENFORO_PHP_LINK = "http://www.soulplayps.com/forums/test.php";

	public static String LOGOUT_MESSAGE = "Click here to logout!";

	public static String DEATH_MESSAGE = "Oh dear you are dead!";

	public static final int MAX_WORLDS = 14;

	/**
	 * The delay it takes to type and or send a message.
	 */
	public static int MESSAGE_DELAY = 6000;

	public static final int BANK_PIN_FAIL_LIMIT = 5;

	/**
	 * The highest amount ID. Change is not needed here unless loading items
	 * higher than the 667 revision.
	 */
	public static final int ITEM_LIMIT = 200_000;// 22335<-667

	/**
	 * Max npc ID loaded from npc definitions
	 */
	public static int MAX_NPC_ID;

	/**
	 * An integer needed for the above code.
	 */
	public static final long MAXITEM_AMOUNT = Integer.MAX_VALUE;

	/**
	 * The size of a players bank.
	 */

	public static final int BANK_SIZE = 352 * 9; // * THE AMOUNT OF TABS

	public static final int BANK_TAB_SIZE = 352; // THIS USED TO BE BANK_SIZE.
													// IT'S REFACTORED NOW TO
													// BANK_TAB_SIZE

	/**
	 * Inscrease point gain when claiming. this is the percentage
	 */
	public static double increasedFPPoints = 0.0;

	/**
	 * The max amount of players until your server is full.
	 */
	public static final int MAX_PLAYERS = 2000;

	public static final int MAX_PLAYERS_DISPLAY = 254;

	public static final int MAX_NPCS = NPCHandler.maxNPCs;

	public static boolean PRICE_CHECK_ENABLED = true;

	public static boolean PRICE_CHECK_PERCENTAGE_FILTER = false;

	/**
	 * Change to true if you want to stop the world --8. Can cause screen
	 * freezes on SilabSoft Clients. Change is not needed.
	 */
	public static final boolean WORLD_LIST_FIX = false;

	public static final int[] CATS = {9303, 9302, 9301, 9300, 9299, 9298, 9297,
			9296, 9295, 9294, 9293, 9292, 9291, 9290, 9289, 9288, 9287, 9286,
			9285, 9284, 9283, 9282, 9281, 9280, 9279, 9278, 9277, 9276, 9275,
			9274, 9273, 9272, 8736};

	public static final int[] destroyableItems = new int[]{10512, 10542, 10543,
			10544, 10545, 10546, 10541, 10540, 15439, 10531, 10532, 10533,
			10534, 10535, 10536, 10537, 6570, 12158, 12159, 12160, 12161, 12162, 12163, 12164, 12165, 12166, 12167, 12168, 14639, 14646 };

	/**
	 * The re-spawn point of when a duel ends.
	 */
	public static final int DUELING_RESPAWN_X = 3362;

	public static final int DUELING_RESPAWN_Y = 3263;

	/**
	 * The point in where you spawn in a duel. Do not change this.
	 */
	public static final int RANDOM_DUELING_RESPAWN = 5;

	/**
	 * The level in which you can not teleport in the wild, and higher.
	 */
	public static final int NO_TELEPORT_WILD_LEVEL = 20;

	/**
	 * The timer in which you are skulled goes away. Seconds x2 Ex. 60x2=120
	 * Skull timer would be 2 minute.
	 */
	public static final int SKULL_TIMER = 1200;

	/**
	 * How long the teleport block effect takes.
	 */
	public static final int TELEBLOCK_DELAY = 20000;

	/**
	 * makes wear item packet instant, causing spec bar to show up right away.
	 * Might cause DC for player switching
	 */
	public static boolean INSTANT_SWITCHES = false;

	/**
	 * Single and multi player killing zones.
	 */
	public static final boolean SINGLE_AND_MULTI_ZONES = true;

	/**
	 * Wilderness levels and combat level differences. Used when attacking
	 * players.
	 */
	public static final boolean COMBAT_LEVEL_DIFFERENCE = true;

	/**
	 * Combat level requirements needed to wield items.
	 */
	public static final boolean itemRequirements = true;
	
	private static boolean isPvpWorld = false;
	
	public static void setAsPvpWorld() {
		isPvpWorld = true;
	}
	
	public static boolean isPvpWorld() {
		return isPvpWorld;
	}

	/**
	 * Combat experience rates.
	 */
	// public static final int MELEE_EXP_RATE = 10000;
	// public static final int RANGE_EXP_RATE = 10000;
	// public static final int MAGIC_EXP_RATE = 10000;
	public static final int EASY_MODE_COMBAT_EXP_RATE = 10;

	public static final int IKOV_NORMAL_MODE_COMBAT_EXP_RATE = 5;

	public static final int MEDIUM_MODE_COMBAT_EXP_RATE = 125;

	public static final int HARD_MODE_COMBAT_EXP_RATE = 2;

	/**
	 * How fast the special attack bar refills.
	 */
	public static final int INCREASE_SPECIAL_AMOUNT = 17500;

	/**
	 * If you need more than one prayer point to use prayer.
	 */
	public static final boolean PRAYER_POINTS_REQUIRED = true;

	/**
	 * If you need a certain prayer level to use a certain prayer.
	 */
	public static final boolean PRAYER_LEVEL_REQUIRED = true;

	/**
	 * If you need a certain magic level to use a certain spell.
	 */
	public static final boolean MAGIC_LEVEL_REQUIRED = true;

	/**
	 * Special server experience bonus rates. (Double experience weekend etc)
	 */

	/**
	 * If you need runes to use magic spells.
	 */
	public static final boolean RUNES_REQUIRED = true;

	/**
	 * If the crystal bow degrades.
	 */
	public static final boolean CRYSTAL_BOW_DEGRADES = true;

	/**
	 * How often the server saves data.
	 */
	public static final int SAVE_TIMER = 60; // Saves every one minute.

	/**
	 * How far NPCs can walk.
	 */
	public static final int NPC_RANDOM_WALK_DISTANCE = 5; // 5x5 square, NPCs
															// would be able to
															// walk 25 squares
															// around.

	/**
	 * How far NPCs can follow you when attacked.
	 */
	public static final int NPC_FOLLOW_DISTANCE = 10; // 10 squares

	/**
	 * NPCs that act as if they are dead. (For salve amulet, etc)
	 */
	public static final int[] UNDEAD_NPCS = {90, 91, 92, 93, 94, 103, 104, 73,
			74, 75, 76, 77};

	// display control panel
	public static boolean SHOW_CPANEL = true;

	public static final String MYSQL_USERNAME = "root";

	public static final String MYSQL_PASSWORD = "root";

	public static final String MYSQL_PLAYERSAVE_HOST = "127.0.0.1";//"8.26.94.80";

	public static final String MYSQL_USERNAME_EU = "soulplay_eu";

	//

	public static final String MYSQL_PASSWORD_EU = "KrF794gLcTeUmTZi";

	public static final String MYSQL_USERNAME_TEST = "julius";

	public static final String MYSQL_PASSWORD_TEST = "testing";

	public static final String MYSQL_HOST_TEST = "8.26.94.80";

	public static final String MYSQL_PLAYER_DB = "players";
	
	public static final String MYSQL_DB_EXTENSIONS = "?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false&characterEncoding=utf8&serverTimezone=UTC";

	public static final String MYSQL_PLAYER_TABLE = "accounts";

	public static final String MYSQL_PLAYER_TABLE_PVP = "accountspvp";

	public static final String HS_DB_NAME = "zaryte";

	public static final String EASY_HS_TABLE_NAME = "hs_users";

	public static final String MEDIUM_HS_TABLE_NAME = "hs_users_medium";

	public static final String HARD_HS_TABLE_NAME = "hs_users_hard";

	public static final String LEGENDARY_HS_TABLE_NAME = "hs_users_legendary";

	public static final String IRONMAN_HS_TABLE_NAME = "hs_users_ironman";

	public static final String WHEEL_TABLE_NAME = "spins";

	public static final String LOGIN_SERVER_PASSWORD = "mywordss";

	public static final String LOG_SQL_HOST = "158.69.195.230";

	public static final String LOG_SQL_USERNAME = "logsdb";

	public static final String LOG_SQL_PASSWORD = "gk675lpeu6RJ0qNw";

	public static final String LOG_SQL_DATABASE = "log_db";

	/**
	 * Timeout time.
	 */
	public static final int TIMEOUT = 20;

	/**
	 * Cycle time.
	 */
	public static final int CYCLE_TIME = 600;

	/**
	 * Buffer size.
	 */
	public static final int BUFFER_SIZE = 512;

	/**
	 * Slayer Variables.
	 */
	public static final int[][] SLAYER_TASKS = {{1, 87, 90, 4, 5}, // Low
																	// tasks
			{6, 7, 8, 9, 10}, // Medium tasks
			{11, 12, 13, 14, 15}, // High tasks
			{1, 1, 15, 20, 25}, // Low requirements
			{30, 35, 40, 45, 50}, // Medium requirements
			{60, 75, 80, 85, 90}}; // High requirements

	public static final String[] GAMBLINGRULES = {
			"When placing bets please use a Trusted Middleman",
			"if you are not betting on a Trusted Host.",
			"Players with these titles are trusted",
			"by the staff team to not scam.",
			"Should you fail to use these players",
			"and you use a normal host or middleman",
			"and you have no proof of being scammed",
			"we can not assist you in getting your items back.",
			"",
			"::Thread 35749 Trusted mm and hosts",
			"::Thread 34406 Gambling rules",
			"::scammed - Reports the last Gamble to the staff as proof."};
	
	public static final String[] DUELRULES = {
			"We recommend to record everything",
			"from the rule and betting screen to the",
			"victory/defeat screen.",
			"Without video proof nothing can be done in",
			"the event of a scam.",
			"Always check the duel rules before accepting.",
			};

	public static final String[] COMMANDS = {"::players - Display player count",
			"::vote - Opens up the vote website",
			"::toggle - Locks and Unlocks your Combat experience",
			"::discord - Joins our Discord Channel",
			"::market - Buy and sell from players",
			"::webmarket - Opens up recent market trades",
			"::skull - Display wildy skull above your head",
			"::blacklist - Black list players from attacking you in Edge Wild.",
			"::rag - Players with gear too strong or weak wont be able attack you",
			"TO SET/RESET CMB SKILLS PRESS ON THEM IN SKILL TAB",
			"::ucp - Opens up player manager",
			"::crushvials - Automatically crush empty vials from potions.",
			"::xpbar - toggles the xp counter bar",
			"::resetpray - Resets your prayer levels",
			"::resetatt - Resets your attack levels",
			"::resetstr - Resets your strenght levels",
			"::resetdef - Resets your defence levels",
			"::resetrange - Resets your range levels",
			"::resetmage - Resets your mage levels",
			"::resetcombat - Resets your combat stats",
			"::npcdrops - Look up NPC drops on the website",
			"::checkdrops monstername - shows you a list of their drops",
			"::whatdrops itemname - shows a list of monster that drop the item",
			"::fishing - Teleports you to fishing",
			"::mining - Teleports you to mining",
			"::barrows - Teleports you to barrows",
			"::train - Teleports you to training",
			"::skill - Teleports you to Home Skilling",
			"::duel - Teleports you to duel arena",
			"::slotmachine - Teleports you to slotmachine",
			"::Empty - Empty your inventory",
			"::changepassword (new password) - Changes your password",
			"::resettask - Resets your slayer task ",
			"::forums - Opens up the forums website",
			"::donate - Opens up the donate website",
			"::promo - Opens up the promo website",
			"::sof - Opens up Squeal Of Fortune",
			"::checkspins - Claim your Squeal Of Fortune rewards",
			"::highscore - Opens up the highscore website",
			"::youtube - Opens up our Youtube channel",
			"::twitter - Opens up our twitter website",
			"::facebook - Opens up our facebook website",
			//"::roofoff - Hide the roofs", "::roofon - Display the roofs",
			//"::hitmarks - Toggle between new or old Hitmarks",
			//"::hpbar - Toggle between new or old HP Bars",
			// "::hd - Toggles between Low Definition or High Definition",
			"::opencache - opens the Cache Folder for Soulsplit",
			"::toggleallexp - Locks and Unlocks all skill experience",
			"::expoff - Will remove all current double exp",
			"::threads - Shows a list of the most common threads",
			"::checkmute - Checks the remaining time on your mute",
			"::suggest - Help us improve with your suggestions!"};

	public static final String[] URLS = {"www.soulplayps.com/vote/", // 0
			"www.soulplayps.com/hiscores/", // 1
			"www.soulplayps.com/play/", // 2
			"www.soulplayps.com/store/", // 3
			"www.youtube.com/channel/UC1DZmIg_SdsxVVaFoYW51MQ", // 4
			"www.soulplayps.com/forums/index.php?forums/pending.22/", // 5
			"www.twitter.com/SoulPlayps", // 6
			"www.youtube.com/channel/UC1DZmIg_SdsxVVaFoYW51MQ", // 7
			"http://soulplay-ps.wikia.com/wiki/Soulplay_ps_Wikia", // 8
			"http://www.soulplayps.com/squeal-of-fortune/", // 9
			"https://soulpvp.soulplayps.com/donate/", // SoulPvP donate 10
			"http://www.soulplayps.com/ucp/", // 11
			"http://www.soulplayps.com/promo/", // 12
			"http://www.soulplayps.com/clans/", // 13
			"http://discord.gg/wRgYyCw", // 14
			"http://www.soulplayps.com/donate-osrs/", // 15
			"http://www.soulplayps.com/forums/index.php?forums/soulplay-suggestions.139/", // 16
			"http://www.soulplayps.com/", //17
	};

	public static final String[] THREADS = {"General",
			"",
			"Donator Perks - ::thread 54032",
			"Prestige - ::thread 11161",
			"Rules - ::rules",
			"Staff - ::thread 24498",
			"",
			"","General",
			"",
			"Donator Perks - ::thread 8542",
			"Prestige - ::thread 11161",
			"Yell rules - ::thread 15863",
			"Staff - ::thread 24498",
			"",
			"Miscellaneous",
			"",
			"Kiln Cape Guide - ::thread 4740  30956",
			"Starter Guide - ::thread 9781",
			"Melee Armour Set Guide - ::thread 29436",
			"Pet Guide - ::thread 12814",
			"Achievements - ::thread 19744",
			"Ironman - ::thread 12655",
			"How to make a clan - ::thread 16834",
			"Armadyl Runes - ::thread 8909",
			"",
			"Skills",
			"",
			"All skills - ::thread 19137",
			"Agility Guide - ::thread 35854",
			"Crafting Guide - ::thread 34974",
			"Runecrafting Guide - ::thread 27402",
			"Dungeoneering Guide - ::thread 38544",
			"Farming Guide - ::thread 13355",
			"Herblore Guide - ::thread 4791",
			"Summoning Guide - ::thread 35674 / 7410 / 5989",
			"Mining / Smithing Guide - ::thread 5700",
			"Fishing Guide - ::thread 212",
			"Karambwan Fishing Guide - ::thread 3539",
			"Hunter / Impling jars - ::thread 33384",
			"Woodcutting / Fletching Guide - ::thread 29512",
			"",
			"Slayer Guide - ::thread 10742 / 4136",
			"Slayer Maps - ::thread 22932",
			"Chaos tunnel map - ::thread 1868",
			"",
			"Lamp Guide - ::thread 29384",
			"Shooting Star- ::thread 5002",
			"",
			"Bosses",
			"",
			"Godwars Bandos - ::thread 4133",
			"Godwars Armadyl - ::thread 2938",
			"Dagganoth Kings - ::thread 569",
			"Nex - ::thread 11023",
			"Corporal Beast - ::thread 9960",
			"Zulrah - ::thread 27036",
			"Tormented Demons - ::thread 4154",
			"Nomad - ::thread 7019 / 8186",
			"Bandos Avatar - ::thread 10478",
			"",
			"Minigames",
			"",
			"Multi Clanwar - ::thread 32150",
			"Castle Wars - ::thread 19655",
			"Soul Wars - ::thread 35630",
			"Warriors Guild - ::thread 1767",
			"Pest Control - ::thread 704",
			"Fishing Contest - ::thread 29303",
			"",
			"Rewards",
			"",
			"Golden Chest - ::thread 29433",
			"Clue Scroll Guide - ::thread 4879",
			"Soulwars Random Rewards - ::thread 3588",
			"Wilderness Keys - ::thread 4371 / 6258",
			"Mystery Box, Clue Scroll, Crystal Chest - ::thread 9951",
			"",
			"Gambling",
			"",
			"Gambling guide - ::thread 34406",
			"Dice - ::thread 10031",
			"Trusted Hosts - ::thread 35749",
			"Miscellaneous",
			"",
			"Firecape Guide - ::thread 7823",
			"Kiln Cape Guide - ::thread 4740  30956",
			"Starter Guide - ::thread 9781",
			"Melee Armour Set Guide - ::thread 29436",
			"Pet Guide - ::thread 12814",
			"Achievements - ::thread 19744",
			"Ironman - ::thread 12655",
			"How to make a clan - ::thread 16834",
			"Armadyl Runes - ::thread 8909",
			"",
			"Skills",
			"",
			"All skills - ::thread 19137",
			"Agility Guide - ::thread 35854",
			"Crafting Guide - ::thread 34974",
			"Runecrafting Guide - ::thread 27402 / 7824",
			"Dungeoneering Guide - ::thread 19480",
			"Farming Guide - ::thread 13355",
			"Herblore Guide - ::thread 4791",
			"Summoning Guide - ::thread 7410 / 5989",
			"Mining / Smithing Guide - ::thread 5700",
			"Fishing Guide - ::thread 212",
			"Karambwan Fishing Guide - ::thread 3539",
			"Hunter / Impling jars - ::thread 33384",
			"Woodcutting / Fletching Guide - ::thread 29512",
			"",
			"Slayer Guide (locations/masters) - ::thread 10742 / 4136",
			"Slayer Maps - ::thread 22932",
			"Chaos tunnel map - ::thread 1868",
			"",
			"Lamp Guide - ::thread 29384",
			"Shooting Star- ::thread 5002",
			"",
			"Bosses",
			"",
			"Godwars Bandos - ::thread 4133",
			"Godwars Armadyl - ::thread 2938",
			"Dagganoth Kings - ::thread 569",
			"Nex - ::thread 11023",
			"Corporal Beast - ::thread 9960",
			"Zulrah - ::thread 27036",
			"Tormented Demons - ::thread 4154",
			"Nomad - ::thread 7019 / 8186",
			"Bandos Avatar - ::thread 10478",
			"",
			"Minigames",
			"",
			"Multi Clanwar - ::thread 32150",
			"Castle Wars - ::thread 19655",
			"Soul Wars - ::thread 3560",
			"Warriors Guild - ::thread 1767",
			"Pest Control - ::thread 704",
			"Fishing Contest - ::thread 29303",
			"",
			"",
			"Rewards",
			"",
			"Golden Chest - ::thread 29433",
			"Clue Scroll Guide - ::thread 4879",
			"Soulwars Random Rewards - ::thread 3588",
			"Wilderness Keys - ::thread 4371 / 6258",
			"Mystery Box / Clue Scroll / Crystal Chest - ::thread 9951",
			"",
			"Gambling",
			"",
			"Gambling guide - ::thread 34406",
			"Dice - ::thread 10031",
			"Trusted Hosts - ::thread 35749"};

	public static final boolean CONSTRUCTIONENABLED = true;

	public static final boolean constructionDungeons = false;

	public static final boolean constructionUpstairs = false;
	
	public static boolean TRADING_POST_ENABLED = true;

	public static boolean exportToSql = false;

	public static boolean saveGame = true;
	
	public static final int OSRS_GFX_OFFSET = 4000;
	public static final int OSRS_ANIM_OFFSET = 20000;

	public static String cfgDir() {
		if (isSpawnPVP()) {
			return "pvpcfg";
		}
		return "cfg";
	}

	public static String dataxml() {
		if (Config.NODE_ID == 2) {
			return "dbeu";
		}
		if (WorldType.equalsType(WorldType.SPAWN)) {
			return "pvpdatabase";
		}
		return "database";
	}

	public static boolean isDev(String name) {
		name = name.toLowerCase();
		switch (name) {
			case "julius":
			case "michael":
			case "diamondfan":
				return true;
			default:
				return false;
		}
	}

	public static boolean canLogin(String name) {
		name = name.toLowerCase();
		switch (name) {
			case "julius":
			case "michael":
			case "diamondfan":
			case "leanbow":
			case "x":
			case "nexus":
			case "angel_ranger":
			case "angel ranger":
			case "x_sam_x":
			case "x sam x":
			case "ginger":
				return true;
			default:
				return false;
		}
	}

	public static boolean isOwner(Player c) {
		return c.playerRights == 3;
	}

	public static boolean isStaff(Player player) {
		return player.playerRights >= 2 && player.playerRights <= 3;
	}

	public static boolean isModerator(Player player) {
		return player.playerRights == 1;
	}

	public static boolean isLegendaryDonator(Player player) {
		return player.getDonatedTotalAmount() >= 1000 || Config.isStaff(player);
	}
	
	public static boolean isUberDonator(Player player) {
		return player.getDonatedTotalAmount() >= 5000 || Config.isStaff(player);
	}

	public static boolean isSpawnPVP() {
		return WorldType.equalsType(WorldType.SPAWN);
	}

	public static boolean isValentines() {
		Calendar cal = Calendar.getInstance();
		if (cal.get(Calendar.DATE) == 14) {
			return cal.get(Calendar.MONTH) == 1;
		}
		return false;
	}

	public static boolean WATCH_SPAMMERS = false;
	
}
