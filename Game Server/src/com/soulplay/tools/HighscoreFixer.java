package com.soulplay.tools;

import com.soulplay.Config;
import com.soulplay.util.sql.SQL;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.*;

public class HighscoreFixer {
	
	private static String updateStatement = "UPDATE `ssforums`.`highscore2` SET `playername`=?, `player_rights` = ?, `player_difficulty` = ?, `total_lvl`=?, `attack_lvl`=?, `attack_exp`=?, `defence_lvl`=?, `defence_exp`=?, `strength_lvl`=?, `strength_exp`=?, `hitpoints_lvl`=?, `hitpoints_exp`=?, `range_lvl`=?, `range_exp`=?, `prayer_lvl`=?, `prayer_exp`=?, `magic_lvl`=?, `magic_exp`=?, `cooking_lvl`=?, `cooking_exp`=?, `woodcutting_lvl`=?, `woodcutting_exp`=?, `fletching_lvl`=?, `fletching_exp`=?, `fishing_lvl`=?, `fishing_exp`=?, `firemaking_lvl`=?, `firemaking_exp`=?, `crafting_lvl`=?, `crafting_exp`=?, `smithing_lvl`=?, `smithing_exp`=?, `mining_lvl`=?, `mining_exp`=?, `herblore_lvl`=?, `herblore_exp`=?, `agility_lvl`=?, `agility_exp`=?, `thieving_lvl`=?, `thieving_exp`=?, `slayer_lvl`=?, `slayer_exp`=?, `farming_lvl`=?, `farming_exp`=?, `runecrafting_lvl`=?, `runecrafting_exp`=?, `dungeoneering_lvl`=?, `dungeoneering_exp`=?, `hunter_lvl`=?, `hunter_exp`=?, `summoning_lvl`=?, `summoning_exp`=?, `construction_lvl` =?, `construction_exp`=?, `total_exp`=? WHERE `id`=?;";
	
	private static HikariDataSource connectionPool;
	
	private static int CONTINUE_ID = 1;
	
	public static void createHighscoreEntry(int mysqlIndex, String playerName)
			throws SQLException {
		try {
			
			Connection connection = getConnection();
			connection.createStatement().executeUpdate(
					"INSERT INTO `highscore2` (player_id, playername) values ('"
							+ mysqlIndex + "' , '" + playerName + "')");
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static java.sql.Connection getConnection() {
		java.sql.Connection conn = null;
		try {
			conn = connectionPool.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static boolean hasHighscoreEntry(int playerID) {
		Boolean hasEntry = SQL.useConnection(connectionPool, connection -> {
			PreparedStatement ps = connection.prepareStatement(
					"SELECT * from `highscore2` WHERE player_id = ?");
			ps.setInt(1, playerID);
			ResultSet result = ps.executeQuery();
			if (result.next()) {
				ps.close();
				connection.close();
				return true;
			}
			ps.close();
			return false;
		});
		return hasEntry != null && hasEntry;
	}
	
	public static void loadPlayers() throws InterruptedException {
		// List<Client> list = new ArrayList<>();
		java.sql.Connection conn = null;
		java.sql.Connection conn2 = null;
		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/ssforums" + Config.MYSQL_DB_EXTENSIONS, "root",
					"brandus887");
			conn2 = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/ssforums" + Config.MYSQL_DB_EXTENSIONS, "root",
					"brandus887");
			java.sql.Statement stmt = conn.createStatement();
			int amount = 0;
			boolean continueQuery = true;
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM accounts WHERE disabled = 0");
			while (rs.next()) {
				if (continueQuery) {
					Client cl = new Client(null);
					cl.setPlayerName(rs.getString("PlayerName"));
					cl.playerPass = rs.getString("Password");
					cl.skipPassword = true;
					PlayerSaveSql.loadGame(cl, cl.getName(), cl.playerPass,
							conn2);
					System.out.println(cl.mySQLIndex + " loaded");
					amount++;
					if (!hasHighscoreEntry(cl.mySQLIndex)) {
						System.out
								.println("Account not found: " + cl.getName());
						createHighscoreEntry(cl.mySQLIndex, cl.getName());
					}
					updateHighscore(cl);
					System.out.println(amount + " - " + cl.getName() + "("
							+ cl.mySQLIndex + ")");
				}
				if (rs.getInt("ID") == CONTINUE_ID) {
					continueQuery = true;
				}
			}
			conn.close();
			conn2.close();
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		try {
			SQL.initializeMySQLDriver();
			connectionPool = SQL.buildDataSource("localhost", 3306,
					Config.MYSQL_PLAYER_DB, Config.MYSQL_USERNAME, Config.MYSQL_PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		loadPlayers();
		// fixHighScores(players);
	}
	
	public static void updateHighscore(final Client cl) {
		try {
			Connection connection = getConnection();
			PreparedStatement ps = connection.prepareStatement(updateStatement);
			ps.setString(1, cl.getName());
			ps.setInt(2, cl.playerRights);
			ps.setInt(3, cl.difficulty);
			ps.setInt(4, cl.getPA().getTotalLevel());
			int psIndex = 5;
			int totalXP = 0; // this needs to be long. total exp way above 2B
			for (int skill = 0; skill < cl.getSkills().getExperienceArray().length; skill++) {
				ps.setInt(psIndex++, cl.getSkills().getStaticLevel(skill));
				ps.setInt(psIndex++, cl.getSkills().getExperience(skill));
				totalXP += cl.getSkills().getExperience(skill);
			}
			ps.setInt(psIndex, totalXP);
			psIndex++;
			ps.setInt(psIndex, cl.mySQLIndex);
			ps.executeUpdate();
			ps.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
