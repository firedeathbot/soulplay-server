/**
 *
 */
package com.soulplay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.soulplay.content.minigames.ddmtournament.OfficialTournamentVariables;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.content.poll.PollManager;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.util.sql.SqlHandler;

/**
 * @author Ray
 */
public class DBConfig {

	/**
	 * Security Settings
	 */

	/*
	 * The delay of logging in from connections. Do not change this.
	 */
	public static int CONNECTION_DELAY = 100;

	/*
	 * How many IP addresses can connect from the same network until the server
	 * rejects another.
	 */
	public static int IPS_ALLOWED = 4;

	public static boolean DETECT_PROXY = false;

	/**
	 * Admin Settings
	 */

	/*
	 * If administrators can trade or not.
	 */
	public static boolean ADMIN_CAN_TRADE = false;

	/*
	 * If administrators can sell items or not.
	 */
	public static boolean ADMIN_CAN_SELL_ITEMS = false;

	/*
	 * Should we check if the user is using a proxy / VPN ?
	 */

	/*
	 * If administrators can drop items or not.
	 */
	public static boolean ADMIN_DROP_ITEMS = false;

	/*
	 * Allow TPA command
	 */
	public static boolean TPA = true;

	/**
	 * Location Settings
	 */

	/*
	 * Which tours to start at, which city.
	 */
	public static String TOUR_VERSION = "edgeville";

	/*
	 * The starting location of your server.
	 */
	public static int START_LOCATION_X = (DBConfig.TOUR_VERSION
			.equals("edgeville") ? 3087 : 3210);

	public static int START_LOCATION_Y = (DBConfig.TOUR_VERSION
			.equals("edgeville") ? 3491 : 3424);

	/*
	 * The re-spawn point of when someone dies.
	 */
	public static int RESPAWN_X = 3210;

	public static int RESPAWN_Y = 3424;

	/**
	 * Minigames etc.
	 */

	public static boolean MINI_GAMES = true; // actually enable duel

	public static boolean STAKING = true;

	public static boolean DUELING = true;

	public static boolean PENGUINS = true;

	public static boolean CLAN_STAKING = true;

	/**
	 * Drop and EXP
	 */

	/*
	 * Drop rate modifier
	 */
	public static double RARE_DROP_MODIFIER = 1.0;

	public static double SUPER_RARE_DROP_MODIFIER = 1.0;

	public static double EASY_MODE_DROPRATE = 1.0;

	public static double MEDIUM_MODE_DROPRATE = 1.1;

	public static double HARD_MODE_DROPRATE = 1.2;

	public static double LEGENDARY_MODE_DROPRATE = 1.4;

	public static double IRONMAN_MODE_DROPRATE = 1.0;

	/*
	 * Special server experience bonus rates. (Double experience weekend etc)
	 */
	public static double SERVER_EXP_BONUS = 1;

	public static boolean DOUBLE_EXP = false;

	public static boolean GLOBAL_EXP = false;

	public static boolean BETTER_HUNT_REWARD_CHANCE = false;

	public static boolean DOUBLE_DROP_RATE = false;

	public static boolean LOCK_EXPERIENCE = false;

	/**
	 * Logging
	 */

	public static boolean LOG_STAFF = true;

	public static boolean LOG_COMMANDS = true;

	public static boolean LOG_TRADE = true;

	public static boolean LOG_PICKUP = true;

	public static boolean LOG_DROP = true;

	public static boolean LOG_STAKE = true;

	public static boolean LOG_KILL = true;

	public static boolean LOG_LOGIN = true;

	public static boolean LOG_LAG = false;

	public static boolean LOG_EMPTY = false;

	public static boolean LOG_SHOP_BUY = false;

	public static boolean LOG_SHOP_SELL = false;

	public static boolean LOG_PSHOP_BUY = false;
	
	public static boolean LOG_TRADING_POST = false;
	
	public static boolean LOG_NAME_CHANGE = false;

	public static boolean LOG_ALCH = false;

	public static boolean PRINT_MISC = false;

	public static boolean UPDATE_UPTIME = true;

	/**
	 * Items
	 */

	public static List<Integer> ITEM_SELLABLE = Arrays.asList(new Integer[]{
			2996, 15098, 15088, 1050, 1051, 1044, 1045, 1046, 1047, 1048, 1049,
			1052, 3842, 3844, 3840, 8844, 8845, 8846, 8847, 8848, 8849, 8850,
			20072, 10548, 6570, 7462, 7461, 7460, 7459, 7458, 7457, 7456, 7455,
			7454, 9748, 9754, 9751, 9769, 9757, 9760, 9763, 9802, 9808, 9784,
			9799, 9805, 9781, 9796, 9793, 9775, 9772, 9778, 9787, 9811, 9766,
			9749, 9755, 9752, 9770, 9758, 9761, 9764, 9803, 9809, 9785, 9800,
			9806, 9782, 9797, 9794, 9776, 9773, 9779, 9788, 9812, 9767, 9747,
			9753, 9750, 9768, 9756, 9759, 9762, 9801, 9807, 9783, 9798, 9804,
			9780, 9795, 9792, 9774, 9771, 9777, 9786, 9810, 9765, 8839, 8840,
			8842, 11663, 11664, 11665, 10499, 995, 9038, 9034, 21570, 10832,
			10833, 10834, 10835, 9920, 13758, 6722, 4837, 746, 7863, 21805,
			15170, 9789, 9790, 9791, 18510, 14893, 14894, 14896, 14897, 14898,
			14901, 14902, 14903, 14904, 14905, 14899, 14900, 14906, 14907,
			14908, 14909, 20767, 20768, 20769, 20770, 4067, 14598, 9271, 15389,
			1543, 1546, 1548, 1544, 7535, 7534, 20763, 20764, 13727, 744, 13661,
			20787, 20788, 20789, 20790, 20791, 21776, 21775, 9952, 12646, 12647,
			12648, 12654, 12655, 12652, 12651, 10729, 12656, 12634, 12645,
			12657});

	/**
	 * Items that can not be traded or staked.
	 */
	public static List<Integer> ITEM_TRADEABLE = Arrays.asList(new Integer[]{
			3842, 3844, 3840, 7462, 8844, 8845, 8846, 8847, 8848, 8849, 8850,
			20072, 6570, 9748, 9754, 9751, 9769, 9757, 9760, 9763, 9802, 9808,
			9784, 9799, 9805, 9781, 9796, 9793, 9775, 9772, 9778, 9787, 9811,
			9766, 9749, 9755, 9752, 9770, 9758, 9761, 9764, 9803, 9809, 9785,
			9800, 9806, 9782, 9797, 9794, 9776, 9773, 9779, 9788, 9812, 9767,
			9747, 9753, 9750, 9768, 9756, 9759, 9762, 9801, 9807, 9783, 9798,
			9804, 9780, 9795, 9792, 9774, 9771, 9777, 9786, 9810, 9765, 9948,
			9949, 10548, 10551, 12169, 12170, 15098, 15088, 17273, 18508, 18509,
			23639, 8839, 8840, 10499, 10498, 11663, 11664, 11665, 11676, 8842,
			19785, 19786, 19787, 19788, 19789, 19790, 9038, 9034, 21570, 5070,
			5071, 5072, 5076, 5077, 5078, 11964, 11966, 15021, 15022, 15023,
			15024, 15025, 15026, 15027, 15028, 15029, 15030, 15031, 15032,
			15033, 15034, 15035, 15036, 15037, 15038, 15039, 15040, 15041,
			15042, 15043, 15044, 8851, 2528, 761, 12158, 12159, 12160, 12161,
			12162, 12163, 12164, 12165, 12166, 12167, 12168, 10867,
			10868, 9920, 13758, 6722, 4837, 746, 7863, 21805, 15170, 9789, 9790,
			9791, 18510, 14893, 14894, 14896, 14897, 14898, 14901, 14902, 14903,
			14904, 14905, 14899, 14900, 14906, 14907, 14908, 14909, 20767,
			20768, 20769, 20770, 4067, 14598, 9271, 13663, 19803, 19804, 15389,
			1543, 1546, 1548, 1544, 7535, 7534, 20763, 20764, 13727, 13661,
			20787, 20788, 20789, 20790, 20791, 21776, 21775, 9952,
			// pvp stuff
			// 13889, 13892, 13895, 13886, 13898, 13872, 13875, 13878,
			// 13860, 13893, 13866, 13863, // pvp armor deg
			// 13913, 13919, 13910, 13916, 13922, 13946, 13949, 13952,
			// 13934, 13947, 13940, 13937, // corrupt pvp armor deg
			// 13901, 13907, 13904, 13869, // pvp weapons deg
			// 13925, 13931, 13928, 13943, // corrupted pvp weapons deg

			// christmas stuff
			6865, 6867, 6866, 14664, 14595, 14604, 14602, 14605, 6868, 6870,
			6869, 6864, 6878, 6874, 6875, 6871, 6879, 6876, 6872, 6880, 6877,
			6873, 6881, 6882, 744, // valentines heart
			// easter event stuff
			12646, 12647, 12648, 12654, 12655, 12652, 12651, 10729, 12656,
			12634, 12645, 12657, 14713});

	/**
	 * Items that can not be dropped.
	 */
	public static List<Integer> UNDROPPABLE_ITEMS = Arrays.asList(new Integer[]{
			3842, 3844, 3840, 7462, 8844, 8845, 8846, 8847, 8848, 8849, 8850,
			20072, 6570, 9748, 9754, 9751, 9769, 9757, 9760, 9763, 9802, 9808,
			9784, 9799, 9805, 9781, 9796, 9793, 9775, 9772, 9778, 9787, 9811,
			9766, 9749, 9755, 9752, 9770, 9758, 9761, 9764, 9803, 9809, 9785,
			9800, 9806, 9782, 9797, 9794, 9776, 9773, 9779, 9788, 9812, 9767,
			9747, 9753, 9750, 9768, 9756, 9759, 9762, 9801, 9807, 9783, 9798,
			9804, 9780, 9795, 9792, 9774, 9771, 9777, 9786, 9810, 9765, 9948,
			9949, 10548, 10551, 12169, 12170, 15098, 15088, 17273, 18508, 18509,
			23639, 8839, 8840, 11663, 11664, 11665, 11676, 8842, 19785, 19786,
			19787, 19788, 19789, 19790, 9038, 9034, 21570, 5070, 5071, 5072,
			5076, 5077, 5078, 11964, 11966, 15021, 15022, 15023, 15024, 15025,
			15026, 15027, 15028, 15029, 15030, 15031, 15032, 15033, 15034,
			15035, 15036, 15037, 15038, 15039, 15040, 15041, 15042, 15043,
			15044, 8851, 2528, 761, 12158, 12159, 12160, 12161, 12162, 12163,
			12164, 12165, 12166, 12167, 12168, 10867, 10868, 9920, 13758,
			6722, 4837, 746, 7863, 21805, 15170, 9789, 9790, 9791, 18510, 14893,
			14894, 14896, 14897, 14898, 14901, 14902, 14903, 14904, 14905,
			14899, 14900, 14906, 14907, 14908, 14909, 20767, 20768, 20769,
			20770, 4067, 14598, 9271, 13663, 15389, 13889, 13892, 13895, 13886,
			13898, 13872, 13875, 13878, 13860, 13893, 13866, 13913, 13919,
			13910, 13916, 13922, 13946, 13949, 13952, 13934, 13947, 13940,
			13901, 13907, 13904, 13869, 13925, 13931, 13928, 13943, 6865, 6867,
			6866, 14664, 14595, 14604, 14602, 14605, 6868, 6870, 6869, 6864,
			6878, 6874, 6875, 6871, 6879, 6876, 6872, 6880, 6877, 6873, 6881,
			6882, 1543, 1546, 1548, 1544, 7535, 7534, 20763, 20764, 13727, 744,
			13661, 20787, 20788, 20789, 20790, 20791, 21776, 21775, 9952, 12646,
			12647, 12648, 12654, 12655, 12652, 12651, 10729, 12656, 12634,
			12645, 12657});

	/*
	 * Items that are listed as fun weapons for dueling.
	 */
	public static List<Integer> FUN_WEAPONS = Arrays.asList(
			new Integer[]{2460, 2461, 2462, 2463, 2464, 2465, 2466, 2467, 2468,
					2469, 2470, 2471, 2471, 2473, 2474, 2475, 2476, 2477});

	public static List<Integer> DUNG_REWARDS = Arrays.asList(
			new Integer[]{18349, 18350, 18351, 18352, 18353, 18354, 18355,
					18356, 18357, 18358, 18359, 18360, 18361, 18362, 18363,
					18364, 18347, 18348, 18333, 18334, 18335, 19669, 18346});

	public static List<String> SPLIT_SIGNS = new ArrayList<>(
			Arrays.asList(",", ".", "(.)", "(,)"));

	public static List<String> BAD_WORDS = new ArrayList<>(
			Arrays.asList("c o m", "eu", "com", "org", "no-ip", "biz", "net",
					"nl", "tk", "o r g"));

	public static List<String> GOOD_WORDS = new ArrayList<>(Arrays.asList(
			"clants", "ogre", "complete", "coming", "command", "come", "wikia",
			"teamviewer", "commands", "teamspeak", "tserverhq.com",
			"teamspeak3.com", "soulsplit.com", "freets3.net", "tinypic",
			"soulsplit3", "soulplayps", "imgur", "prntscr", "gyazo", "google"));

	/*
	 * Chat filters
	 */

	public static List<String> RSPS_NAMES = new ArrayList<>(
			Arrays.asList("runique", "astrect"));

	public static boolean USE_SCROLLS = false;

	public static boolean UPDATE_ANNOUNCEMENTS = true;

	public static boolean ANNOUNCE = true;

	/**
	 * Hikari stuff
	 */

	private static final Logger LOG = Logger
			.getLogger(DBConfig.class.getName());

	public static List<Integer> getIntList(final String stringToSplit) {
		List<Integer> list = new ArrayList<>();
		for (String string : stringToSplit.split(";")) {
			list.add(Integer.parseInt(string));
		}
		return list;
	}

	public static List<String> getStringList(final String stringToSplit) {
		List<String> list = new ArrayList<>();
		for (String string : stringToSplit.split(";")) {
			list.add(string);
		}
		return list;
	}

	public static String listToString(final List<? extends Object> list) {
		StringBuilder string = new StringBuilder();
		for (Object o : list) {
			string.append(o);
			string.append(";");
		}
		return string.toString();
	}

	private static final String SELECT_SETTINGS = "SELECT * FROM settings WHERE world = ?";
	
	public static void updateSettings() {
		if (Config.SERVER_DEBUG/* || !Config.RUN_ON_DEDI */) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;
		final int world = Config.RUN_ON_DEDI ? Config.NODE_ID : 1;

		try {
			connection = SqlHandler.getHikariZaryteTable().getConnection();

			ps = connection.prepareStatement(SELECT_SETTINGS);
			
			ps.setInt(1, world);
			
			ResultSet results = ps.executeQuery();
			
			while (results.next()) {
				CONNECTION_DELAY = results.getInt("connection_delay");
				IPS_ALLOWED = results.getInt("ips_allowed");
				DETECT_PROXY = results.getBoolean("detect_proxy");

				ADMIN_CAN_TRADE = results.getBoolean("admin_can_trade");
				ADMIN_CAN_SELL_ITEMS = results
						.getBoolean("admin_can_sell_items");
				ADMIN_DROP_ITEMS = results.getBoolean("admin_can_drop_items");
				TPA = results.getBoolean("tpa");

				TOUR_VERSION = results.getString("tour_version");
				START_LOCATION_X = results.getInt("start_location_x");
				START_LOCATION_Y = results.getInt("start_location_y");
				RESPAWN_X = results.getInt("respawn_x");
				RESPAWN_Y = results.getInt("respawn_y");

				MINI_GAMES = results.getBoolean("mini_games");
				STAKING = results.getBoolean("staking");
				DUELING = results.getBoolean("dueling");
				PENGUINS = results.getBoolean("penguins");

				RARE_DROP_MODIFIER = results.getDouble("rare_drop");
				SUPER_RARE_DROP_MODIFIER = results.getDouble("super_rare_drop");
				EASY_MODE_DROPRATE = results.getDouble("easy_mode_droprate");
				MEDIUM_MODE_DROPRATE = results
						.getDouble("medium_mode_droprate");
				HARD_MODE_DROPRATE = results.getDouble("hard_mode_droprate");
				LEGENDARY_MODE_DROPRATE = results
						.getDouble("legendary_mode_droprate");
				IRONMAN_MODE_DROPRATE = results
						.getDouble("ironman_mode_droprate");
				SERVER_EXP_BONUS = results.getDouble("server_exp_bonus");
				DOUBLE_EXP = results.getBoolean("double_exp");
				GLOBAL_EXP = results.getBoolean("global_exp");
				DOUBLE_DROP_RATE = results.getBoolean("double_drop_rate");
				LOCK_EXPERIENCE = results.getBoolean("lock_experience");

				LOG_STAFF = results.getBoolean("log_staff");
				LOG_COMMANDS = results.getBoolean("log_commands");
				LOG_TRADE = results.getBoolean("log_trade");
				LOG_PICKUP = results.getBoolean("log_pickup");
				LOG_DROP = results.getBoolean("log_drop");
				LOG_STAKE = results.getBoolean("log_stake");
				LOG_PSHOP_BUY = results.getBoolean("log_pos_buy");
				LOG_TRADING_POST = results.getBoolean("log_trading_post");
				LOG_NAME_CHANGE = results.getBoolean("log_name_change");
				LOG_SHOP_BUY = results.getBoolean("log_shop_buy");
				LOG_SHOP_SELL = results.getBoolean("log_shop_sell");
				LOG_KILL = results.getBoolean("log_kill");
				LOG_LOGIN = results.getBoolean("log_login");
				LOG_ALCH = results.getBoolean("log_alch");
				LOG_LAG = results.getBoolean("log_lag");
				LOG_EMPTY = results.getBoolean("log_empty");
				PRINT_MISC = results.getBoolean("print_misc");
				UPDATE_UPTIME = results.getBoolean("update_uptime");

				List<Integer> OLD_SELLABLE = ITEM_SELLABLE;
				List<Integer> OLD_TRADEABLE = ITEM_TRADEABLE;
				// List<Integer> OLD_UNDROP = UNDROPPABLE_ITEMS;
				// List<Integer> OLD_FUN_WEAPONS = FUN_WEAPONS;
				// List<Integer> OLD_DUNG_REWARD = DUNG_REWARDS;

				ITEM_SELLABLE = getIntList(results.getString("item_sellable"));
				ITEM_TRADEABLE = getIntList(
						results.getString("item_tradeable"));
				UNDROPPABLE_ITEMS = getIntList(
						results.getString("undroppable_items"));
				FUN_WEAPONS = getIntList(results.getString("fun_weapons"));
				DUNG_REWARDS = getIntList(results.getString("dung_rewards"));

				if (!OLD_TRADEABLE.equals(ITEM_TRADEABLE)
						|| !OLD_SELLABLE.equals(ITEM_SELLABLE)) {
					ItemProjectInsanity.reloadItems = true;
				}

				OLD_SELLABLE = null;
				OLD_TRADEABLE = null;
				// OLD_UNDROP = null;
				// OLD_FUN_WEAPONS = null;
				// OLD_DUNG_REWARD = null;

				SPLIT_SIGNS = getStringList(results.getString("split_signs"));
				BAD_WORDS = getStringList(results.getString("bad_words"));
				GOOD_WORDS = getStringList(results.getString("good_words"));
				RSPS_NAMES = getStringList(results.getString("rsps_names"));

				USE_SCROLLS = results.getBoolean("use_scrolls");

				UPDATE_ANNOUNCEMENTS = results
						.getBoolean("update_announcements");
				ANNOUNCE = results.getBoolean("announce");
				
				boolean updatePoll = results.getBoolean("update_vote_polls");
				
				PollManager.pollInfo = results.getString("vote_poll_info");
				
				boolean updateTournamentSettings = results.getBoolean("update_tournament");
				
				boolean updateSeasonPass = results.getBoolean("update_season_pass"); //ALTER TABLE `settings` ADD `update_season_pass` TINYINT(1) NOT NULL DEFAULT '0' AFTER `update_tournament`;
				
				boolean resetSettings = false;
				
				if (updateTournamentSettings) {
					OfficialTournamentVariables.pullSettings();
					resetSettings = true;
				}
				
				if (PollManager.pollInfo != null && PollManager.polls != null && PollManager.polls[0] != null && !PollManager.polls[0].getInfo().equals(PollManager.pollInfo)) {
					PollManager.polls[0].setInfo(PollManager.pollInfo);
				}
				
				if (updatePoll) {
					PollManager.loadPolls();
					resetSettings = true;
				}
				
				if (updateSeasonPass) {
					SeasonPassManager.updateSeasonPassSettings(false);
					resetSettings = true;
				}
				
				if (resetSettings) {
					resetSettings(world);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static final String RESET_SETTINGS = "UPDATE `settings` SET `update_vote_polls` = 0, `update_season_pass` = 0, `update_tournament` = 0 WHERE world = ?";
	
	private static void resetSettings(final int world) {
		
		try (Connection con = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = con.prepareStatement(RESET_SETTINGS)) {
			
			ps.setInt(1, world);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void uploadSettings() {
		Connection connection = null;
		PreparedStatement ps = null;
		String query = "UPDATE settings SET connection_delay=?, ips_allowed=?, detect_proxy=?,"
				+ "admin_can_trade=?, admin_can_sell_items=?, admin_can_drop_items=?, tpa=?,"
				+ "tour_version=?, start_location_x=?, start_location_y=?, respawn_x=?, respawn_y=?,"
				+ "mini_games=?, staking=?, dueling=?, penguins=?,"
				+ "rare_drop=?, super_rare_drop=?, easy_mode_droprate=?, medium_mode_droprate=?, hard_mode_droprate=?, legendary_mode_droprate=?,"
				+ "ironman_mode_droprate=?, server_exp_bonus=?, double_exp=?, global_exp=?, double_drop_rate=?, lock_experience=?,"
				+ "log_staff=?, log_commands=?, log_trade=?, log_pickup=?, log_drop=?, log_stake=?, log_kill=?, log_login=?, log_lag=?, print_misc=?, update_uptime =?,"
				+ "item_sellable=?, item_tradeable=?, undroppable_items=?, fun_weapons=?, dung_rewards=?, split_sign = ?, bad_words = ?, good_words = ?, rsps_names = ?, use_scrolls = ?, update_announcements = ?, announce = ?, log_empty = ? WHERE world = 1";
		try {
			connection = SqlHandler.getHikariZaryteTable().getConnection();
			ps = connection.prepareStatement(query);
			ps.setInt(1, CONNECTION_DELAY);
			ps.setInt(2, IPS_ALLOWED);
			ps.setBoolean(3, DETECT_PROXY);

			ps.setBoolean(4, ADMIN_CAN_TRADE);
			ps.setBoolean(5, ADMIN_CAN_SELL_ITEMS);
			ps.setBoolean(6, ADMIN_DROP_ITEMS);
			ps.setBoolean(7, TPA);

			ps.setString(8, TOUR_VERSION);
			ps.setInt(9, START_LOCATION_X);
			ps.setInt(10, START_LOCATION_Y);
			ps.setInt(11, RESPAWN_X);
			ps.setInt(12, RESPAWN_Y);

			ps.setBoolean(13, MINI_GAMES);
			ps.setBoolean(14, STAKING);
			ps.setBoolean(15, DUELING);
			ps.setBoolean(16, PENGUINS);

			ps.setDouble(17, RARE_DROP_MODIFIER);
			ps.setDouble(18, SUPER_RARE_DROP_MODIFIER);
			ps.setDouble(19, EASY_MODE_DROPRATE);
			ps.setDouble(20, MEDIUM_MODE_DROPRATE);
			ps.setDouble(21, HARD_MODE_DROPRATE);
			ps.setDouble(22, LEGENDARY_MODE_DROPRATE);
			ps.setDouble(23, IRONMAN_MODE_DROPRATE);

			ps.setDouble(24, SERVER_EXP_BONUS);
			ps.setBoolean(25, DOUBLE_EXP);
			ps.setBoolean(26, GLOBAL_EXP);
			ps.setBoolean(27, DOUBLE_DROP_RATE);
			ps.setBoolean(28, LOCK_EXPERIENCE);

			ps.setBoolean(29, LOG_STAFF);
			ps.setBoolean(30, LOG_COMMANDS);
			ps.setBoolean(31, LOG_TRADE);
			ps.setBoolean(32, LOG_PICKUP);
			ps.setBoolean(33, LOG_DROP);
			ps.setBoolean(34, LOG_STAKE);
			ps.setBoolean(35, LOG_KILL);
			ps.setBoolean(36, LOG_LOGIN);
			ps.setBoolean(37, LOG_LAG);
			ps.setBoolean(38, PRINT_MISC);
			ps.setBoolean(39, UPDATE_UPTIME);

			ps.setString(40, listToString(ITEM_SELLABLE));
			ps.setString(41, listToString(ITEM_TRADEABLE));
			ps.setString(42, listToString(UNDROPPABLE_ITEMS));
			ps.setString(43, listToString(FUN_WEAPONS));
			ps.setString(44, listToString(DUNG_REWARDS));

			ps.setString(45, listToString(SPLIT_SIGNS));
			ps.setString(46, listToString(BAD_WORDS));
			ps.setString(47, listToString(GOOD_WORDS));
			ps.setString(48, listToString(RSPS_NAMES));

			ps.setBoolean(49, USE_SCROLLS);
			ps.setBoolean(50, UPDATE_ANNOUNCEMENTS);
			ps.setBoolean(51, ANNOUNCE);
			ps.setBoolean(52, LOG_EMPTY);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public DBConfig() throws InterruptedException, Exception {
		init();
	}

	public void init() throws Exception {
		LOG.log(Level.INFO, "initiating database config connection...");
	}
}
