package com.soulplay.cache;

import com.soulplay.util.Misc;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public final class ItemDef {

	private static Stream stream;
	private static int[] streamIndices;
	private static Stream streamOsrs;
	private static int[] streamIndicesOsrs;
	public static int totalItems;
	public static int totalItemsOsrs;
	public static final int OSRS_START = 100_000;
	private static Int2ObjectMap<ItemDef> cache;
	private static Int2ObjectMap<ItemDef> cacheOsrs;

	public static int getPossibleNotedId(int itemId) {
		ItemDef def = ItemDef.forID(itemId);
		if (def == null)
			return itemId;
		
		if (!def.isInNotePosition() && def.itemCanBeNoted())
			itemId = def.getNoteId();
		
		return itemId;
	}
	
	public static int getNotNotedId(int itemId) {
		ItemDef def = ItemDef.forID(itemId);
		if (def == null)
			return itemId;
		
		if (def.isInNotePosition())
			itemId = def.getNoteId();
		
		return itemId;
	}

	public static ItemDef forIDOSRS(int i) {
		ItemDef itemDef = cacheOsrs.get(i);
		if (itemDef != null) {
			return itemDef;
		}

		itemDef = new ItemDef();
		itemDef.setDefaults();

		int indexId = i - OSRS_START;
		if (indexId >= 0 && indexId < totalItemsOsrs) {
			streamOsrs.currentOffset = streamIndicesOsrs[indexId];
			itemDef.readValues667(streamOsrs, true);
		}

		if (itemDef.certTemplateID != -1) {
			itemDef.toNote();
		}

		cacheOsrs.put(i, itemDef);
		return itemDef;
	}

	public static ItemDef forID(int i) {
		if (i >= OSRS_START) {
			return forIDOSRS(i);
		}

		ItemDef itemDef = cache.get(i);
		if (itemDef != null) {
			return itemDef;
		}

		itemDef = new ItemDef();
		itemDef.setDefaults();

		if (i >= 0 && i < totalItems) {
			stream.currentOffset = streamIndices[i];
			itemDef.readValues667(stream, false);
		}

		if (itemDef.certTemplateID != -1) {
			itemDef.toNote();
		}

		cache.put(i, itemDef);
		
		switch (i) {
			case 25000:
				itemDef.name = "Power card (Random)";
				itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
				break;
			case 25001:
				itemDef.name = "Power card (Health)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25002:
				itemDef.name = "Power card (Melee Damage)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25003:
				itemDef.name = "Power card (Magic Damage)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25004:
				itemDef.name = "Power card (Range Damage)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25005:
				itemDef.name = "Power card (Strength)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25006:
				itemDef.name = "Power card (Prayer Drain)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25007:
				itemDef.name = "Power card (Defence)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25008:
				itemDef.name = "Power card (Block)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25009:
				itemDef.name = "Power card (Surge)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25010:
				itemDef.name = "Power card (Spiked Shield)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25011:
				itemDef.name = "Power card (Two Hander)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25012:
				itemDef.name = "Power card (One Hander)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25013:
				itemDef.name = "Power card (Ranger)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25014:
				itemDef.name = "Power card (Minor Crit)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25015:
				itemDef.name = "Power card (Health Regen)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25016:
				itemDef.name = "Power card (Melee Accuracy)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25017:
				itemDef.name = "Power card (Range Accuracy)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25018:
				itemDef.name = "Power card (Magic Accuracy)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25019:
				itemDef.name = "Power card (Poison)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25020:
				itemDef.name = "Power card (Poison Lasting)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25021:
				itemDef.name = "Power card (Magic knowledge)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25022:
				itemDef.name = "Power card (Necromancer)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25023:
				itemDef.name = "Power card (Hybrid)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25024:
				itemDef.name = "Power card (Block Mastery)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25025:
				itemDef.name = "Power card (Ignite)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25026:
				itemDef.name = "Power card (Earth Root)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25027:
				itemDef.name = "Power card (Vengeance Heal)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25028:
				itemDef.name = "Power card (Holy Food)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25029:
				itemDef.name = "Power card (Heavyweight)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25030:
				itemDef.name = "Power card (Dragonbreath)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25031:
				itemDef.name = "Power card (Quick)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25032:
				itemDef.name = "Power card (Battlemage)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25033:
				itemDef.name = "Power card (The Beast)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25034:
				itemDef.name = "Power card (Critter)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25035:
				itemDef.name = "Power card (Explosive Ranger)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25036:
				itemDef.name = "Power card (Ninja)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25037:
				itemDef.name = "Power card (Ironman)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25038:
				itemDef.name = "Power card (Mirror)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25039:
				itemDef.name = "Power card (Poison Crit)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25040:
				itemDef.name = "Power card (Lifesteal)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25041:
				itemDef.name = "Power card (Caster)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25042:
				itemDef.name = "Power card (Advanced Ranger)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25043:
				itemDef.name = "Power card (Warrior)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25044:
				itemDef.name = "Power card (Crits)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25045:
				itemDef.name = "Power card (Crit Sprinter)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25046:
				itemDef.name = "Power card (Haste)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25047:
				itemDef.name = "Power card (Range Crit)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25048:
				itemDef.name = "Power card (Fire Wind)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 25049:
				itemDef.name = "Power card (Perfected ingredients)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Info", "Drop" };
				break;
			case 455:
				itemDef.name = "Powers point";
				break;
			case 11180:
				itemDef.name = "Soulplay Token";
				itemDef.stackable = true;
				break;
			case 11341:
				itemDef.name = "Page of Destiny";
				itemDef.inventoryOptions = new String[]{"Learn", null, null, null, "drop"};
				break; 
			case 19705:
				itemDef.name = "Beginner Power Book";
				itemDef.inventoryOptions = new String[]{"Learn", null, null, null, "drop"};
				break;
				
			case 2715:
			case 13054:
			case 13037:
			case 19039:
			case 13077:
				itemDef.stackable = true;
				break;

		case 10943:
			itemDef.name = "Instance Token";
			itemDef.inventoryOptions = new String[]{"Read", "Redeem", null, null, "Drop"};
			break;
				
		case 12502:
			itemDef.name = "Level-up Season Pass";
			itemDef.inventoryOptions = new String[]{"Redeem", null, null, null, "Drop"};
			itemDef.stackable = true;
			break;
		
		case 3150:
			itemDef.stackable = true;
			return itemDef;
		
		case 4278: // ecto-token
			itemDef.name = "Pk Points Token";
			itemDef.inventoryOptions = new String[]{"Redeem", null, null, null, "Drop"};
			return itemDef;
		
			case 5943:
			case 5944:
			case 5945:
			case 5946:
			case 5947:
			case 5948:
			case 5949:
			case 5950:
				itemDef.name = itemDef.name.replace("Antipoison+", "Anti-venom");
				return itemDef;
				
			case 13685:
			case 13686:
			case 13687:
				itemDef.name = "A Stylish Hat (Male)";
				return itemDef;

			case 13688:
			case 13689:
			case 13690:
				itemDef.name = "A Stylish Hat (Female)";
				return itemDef;

			case 13691:
			case 13692:
			case 13693:
				itemDef.name = "Shirt (Male)";
				return itemDef;

			case 13694:
			case 13695:
			case 13696:
				itemDef.name = "Shirt (Female)";
				return itemDef;

			case 13697:
			case 13698:
			case 13699:
				itemDef.name = "Leggings (Male)";
				return itemDef;

			case 13700:
			case 13701:
			case 13702:
				itemDef.name = "Skirt (Female)";
				return itemDef;
				
			case 21805:
				itemDef.name = "Wilderness key";
				itemDef.inventoryOptions = new String[]{"Info", null, null, null, "Drop"};
				return itemDef;
				
			case 30423:
				itemDef.name = "Gold Santa Hat";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.manWear = 189; // male wield model
				itemDef.womanWear = 366; // femArmModel
				break;
				
			case 30422:
				itemDef.name = "Cyan Partyhat";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				break;
				
			case 30421: // skeleton lantern note
				return itemDef;
			
			case 30420: // skeleton lantern
				itemDef.name = "Skeleton lantern";
				return itemDef;
			
			case 30419: // pumpkin lantern note
				return itemDef;
			
			case 30418: // pumpkin lantern
				itemDef.name = "Pumpkin lantern";
				return itemDef;
			
			case 30417: // spookier boots note
				return itemDef;
			
			case 30416: // spookier boots
				itemDef.name = "Spookier boots";
				return itemDef;

			case 30415: // spookier gloves note
				return itemDef;
			
			case 30414: // spookier gloves
				itemDef.name = "Spookier gloves";
				return itemDef;

			case 30413: // spookier skirt note
				return itemDef;
				
			case 30412: // spookier skirt
				itemDef.name = "Spookier skirt";
				return itemDef;

			case 30411: // spookier robe note
				return itemDef;
			
			case 30410: // spookier robe
				itemDef.name = "Spookier robe";
				return itemDef;

			case 30409: // spookier hood note
				return itemDef;
			
			case 30408: // spookier hood
				itemDef.name = "Spooky hood";
				return itemDef;

			case 30407: // spooky boots note
				return itemDef;
			
			case 30406: // spooky boots
				itemDef.name = "Spooky boots";
				return itemDef;

			case 30405: // spooky gloves note
				return itemDef;
			
			case 30404: // spooky gloves
				itemDef.name = "Spooky gloves";
				return itemDef;

			case 30403: // spooky skirt note
				return itemDef;
				
			case 30402: // spooky skirt
				itemDef.name = "Spooky skirt";
				return itemDef;

			case 30401: // spooky robe note
				return itemDef;

			case 30400: // spooky robe
				itemDef.name = "Spooky robe";
				return itemDef;
				
			case 30399: // spooky hood note
				return itemDef;

			case 30398: // spooky hood
				itemDef.name = "Spooky hood";
				return itemDef;
				
			case 30395: //Thammaron's sceptre
				itemDef.name = "Thammaron's sceptre";
				return itemDef;
				
			case 30394: //Thammaron's sceptre (u) noted
				itemDef.name = "Thammaron's sceptre (u)";
				itemDef.stackable = true;
				return itemDef;
				
			case 30393: //Thammaron's sceptre (u)
				itemDef.name = "Thammaron's sceptre (u)";
				return itemDef;
				
			case 30392: //Viggora's chainmace
				itemDef.name = "Viggora's chainmace";
				return itemDef;
				
			case 30391: //Viggora's chainmace (u) noted
				itemDef.name = "Viggora's chainmace (u)";
				itemDef.stackable = true;
				return itemDef;
				
			case 30390: //Viggora's chainmace (u)
				itemDef.name = "Viggora's chainmace (u)";
				return itemDef;
				
			case 30385: //Larran's key
				itemDef.name = "Larran's key";
				itemDef.stackable = true;
				return itemDef;
				
			case 30384: //craw's bow
				itemDef.name = "Craw's bow";
				return itemDef;
				
			case 30383: //craw's bow (u) noted
				itemDef.name = "Craw's bow (u)";
				itemDef.stackable = true;
				return itemDef;
				
			case 30382: //craw's bow (u)
				itemDef.name = "Craw's bow (u)";
				return itemDef;
				
			case 30381: // black and white phat
				itemDef.name = "Checkered Partyhat";
				return itemDef;
			case 30380:// Flash scroll
				itemDef.name = "Flash scroll";
				itemDef.stackable = true;
				return itemDef;
			case 30379:// speed scroll
				itemDef.name = "Speed scroll";
				itemDef.stackable = true;
				return itemDef;
			case 30378: //reset wildy kill count
				itemDef.name = "Reset Wild killcount";
				return itemDef;
			case 30377: //zuk pet
				itemDef.name = "Tzrek-zuk";
				return itemDef;
			case 30376: //Great Olm pet Olmlet
				itemDef.name = "Olmlet";
				return itemDef;
			case 30375: //pet kraken
				itemDef.name = "Pet kraken";
				return itemDef;
			case 30374: //scorpia's offspring
				itemDef.name = "Scorpia's offspring";
				return itemDef;
			case 30373: //callisto cub
				itemDef.name = "Callisto cub";
				return itemDef;
			case 30372: //venenatis spiderling
				itemDef.name = "Venenatis spiderling";
				return itemDef;
			case 30371: //vet'ion jr recolor
				itemDef.name = "Vet'ion jr.";
				return itemDef;
			case 30370: //vet'ion jr
				itemDef.name = "Vet'ion jr.";
				return itemDef;
				
			case 30369:
				itemDef.name = "Vial of blood";
				itemDef.stackable = true;
				return itemDef;
				
			case 30368:
				itemDef.name = "Infernal cape";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
				
			case 30367:
				itemDef.name = "OSRS Coins";
				itemDef.inventoryOptions = new String[] { "Claim", null, null, null, "Drop" };
				itemDef.stackable = true;
				itemDef.cost = 1;
				return itemDef;
				
			case 30356:
				itemDef.setDefaults();
				itemDef.name = "Black chinchompa";
				itemDef.stackable = true;
				return itemDef;
				
			case 30355:
				itemDef.setDefaults();
				itemDef.name = "Black chinchompa";
				itemDef.stackable = true;
				return itemDef;
						
			case 30354:
				itemDef.setDefaults();
				itemDef.name = "Golden Scythe";
				return itemDef;
				
			case 30353:
				itemDef.setDefaults();
				itemDef.name = "Gilded h'ween mask";
				return itemDef;
				
			case 30352:
				itemDef.setDefaults();
				itemDef.name = "White h'ween mask";
				return itemDef;
				
			case 30351:
				itemDef.setDefaults();
				itemDef.name = "Pink h'ween mask";
				return itemDef;
				
			case 30350:
				itemDef.setDefaults();
				itemDef.name = "Uncharged toxic trident";
				itemDef.certTemplateID = 799;
				itemDef.noteId = 30349;
				itemDef.toNote();
				return itemDef;
				
			case 30349:
				itemDef.setDefaults();
				itemDef.noteId = 30350;
				itemDef.name = "Uncharged toxic trident";
				return itemDef;
				
			case 30348:
				itemDef.setDefaults();
				itemDef.name = "Trident of the swamp";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", "Uncharge", "Drop"};
				itemDef.manWear = 14400;//60101; //14400;
				itemDef.womanWear = 14400;//60101; //14400;
				return itemDef;
				
			case 30347:
				itemDef.name = "Giant present";
				itemDef.manWear = 32380;
				itemDef.womanWear = 32380; 
				itemDef.inventoryOptions = new String[]{null, "Wear", "Open", null, "Drop"};
				return itemDef;
				
			case 30346:
				itemDef.name = "Antisanta boots";
				itemDef.manWear = 28988;
				itemDef.womanWear = 29006; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30345:
				itemDef.name = "Antisanta gloves";
				itemDef.manWear = 28977;
				itemDef.womanWear = 28999; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30344:
				itemDef.name = "Antisanta pantaloons";
				itemDef.manWear = 28980;
				itemDef.womanWear = 29000; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30343:
				itemDef.name = "Antisanta jacket";
				itemDef.manWear = 28984;
				itemDef.womanWear = 29004; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30342:
				itemDef.name = "Antisanta mask";
				itemDef.manWear = 28975; 
				itemDef.womanWear = 28996; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30341:
				itemDef.name = "Santa boots";
				itemDef.manWear = 28989;
				itemDef.womanWear = 29007; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30340:
				itemDef.name = "Santa gloves";
				itemDef.manWear = 28978;
				itemDef.womanWear = 28998; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30339:
				itemDef.name = "Santa pantaloons";
				itemDef.manWear = 28979;
				itemDef.womanWear = 29001; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30338:
				itemDef.name = "Santa jacket";
				itemDef.manWear = 28983;
				itemDef.womanWear = 29005; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30337:
				itemDef.name = "Santa mask";
				itemDef.manWear = 28976; 
				itemDef.womanWear = 28997; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, null};
				return itemDef;
				
			case 30336:
				itemDef.name = "Dragonfire ward";
				itemDef.manWear = 34648; 
				itemDef.womanWear = 34648; 
				itemDef.inventoryOptions = new String[]{null, "Wield", "Inspect", "Empty", "Drop"};
				return itemDef;
				
			case 30335:
				itemDef.name = "Skeletal visage";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.noteId = 30334;
				return itemDef;
				
			case 30334:
				itemDef.noteId = 30333;
				itemDef.certTemplateID = 799;
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.toNote();
				return itemDef;
				
			case 30333:
				itemDef.name = "Superior dragon bones";
				itemDef.inventoryOptions = new String[]{"Bury", null, null, null, "Drop"};
				itemDef.noteId = 30334;
			    break;
				
			case 30332:
				itemDef.name = "Dragonbone necklace";
				itemDef.manWear = 34627; 
				itemDef.womanWear = 34627; 
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
				
			case 30331:
				itemDef.name = "Turquoise slayer helmet (i)";
				itemDef.manWear = 34626; //31549;
				itemDef.womanWear = 34638; //31552;
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", "Disassemble", null};
				return itemDef;
				
			case 30330:
				itemDef.name = "Turquoise slayer helmet";
				itemDef.manWear = 34626; //31549;
				itemDef.womanWear = 34638; //31552;
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", "Disassemble", null};
				return itemDef;
			    
			case 30329:
				itemDef.name = "Vorkath's head";
			    break;
				
			case 30328:
				itemDef.name = "Scythe of vitur";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", "Uncharge", "Drop"};
				itemDef.manWear = 35371; //male
				itemDef.womanWear = 35371; // female
				return itemDef;
				
			case 30327:
				itemDef.name = "Scythe of vitur (uncharged)";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Charge", null, "Drop"};
				itemDef.manWear = 35373; //male
				itemDef.womanWear = 35373; // female
				return itemDef;
				
			case 30326:
				itemDef.name = "Hunting knife";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 32332; //male
				itemDef.womanWear = 32332; // female
				return itemDef;
				
			case 30325:
				itemDef.name = "Banshee robe";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32325; //male
				itemDef.womanWear = 32329; // female
			    break;
				
			case 30324:
				itemDef.name = "Banshee top";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32327; //male
				itemDef.womanWear = 32331; // female
				break;
				
			case 30323:
				itemDef.name = "Banshee mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32324; //male
				itemDef.womanWear = 32328; // female
			    break;
				
			case 30322:
				itemDef.name = "Soulplay bond";
				itemDef.inventoryOptions = new String[]{null, "Redeem", null, null, "Drop"};
			    break;
				
			case 30321:
				itemDef.name = "Prospector boots";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28458; //male
				itemDef.womanWear = 28458; // female
			    break;
			    
			case 30320:
				itemDef.name = "Prospector legs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28450; //male
				itemDef.womanWear = 28459; // female
			    break;
					
			case 30319:
				itemDef.name = "Prospector jacket";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28457; //male
				itemDef.womanWear = 28461; // female
				break;
					
			case 30318:
				itemDef.name = "Prospector helmet";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28453; //male
				itemDef.womanWear = 28453; // female
			    break;
				
			case 30317:
				itemDef.name = "Tanzanite helm";
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", null, "Uncharge"};
				itemDef.manWear = 14421;
				itemDef.womanWear = 23994;
			    break;
			    
			case 30316:
				itemDef.name = "Tanzanite helm (uncharged)";
				itemDef.manWear = 14423;
				itemDef.womanWear = 14425;
			    break;
				
			case 30315:
				itemDef.name = "Magma helm";
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", null, "Uncharge"};
				itemDef.manWear = 14424;
				itemDef.womanWear = 14426;
			    break;
			    
			case 30314:
				itemDef.name = "Magma helm (uncharged)";
				itemDef.manWear = 14422;
				itemDef.womanWear = 28436;
			    break;
				
			case 30313:
				itemDef.name = "Magma mutagen";
			    break;
				
			case 30312:
				itemDef.name = "Tanzanite mutagen";
			    break;
				
			case 30311:
				itemDef.name = "Angler boots";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 29409; //male
				itemDef.womanWear = 29414; // female
			    break;
				
			case 30310:
				itemDef.name = "Angler waders";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 29113; //male
				itemDef.womanWear = 29410; // female
			    break;
				
			case 30309:
				itemDef.name = "Angler top";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 29118; //male
				itemDef.womanWear = 29413; // female
			    break;
				
			case 30308:
				itemDef.name = "Angler hat";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 29114; //male
				itemDef.womanWear = 29411; // female
			    break;
				
			case 30307:
				itemDef.name = "Warm gloves";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32212; //male
				itemDef.womanWear = 32220; // female
			    break;
			    
			case 30306:
				itemDef.name = "Pyromancer boots";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32216; //male
				itemDef.womanWear = 32224; // female
			    break;
			    
			case 30305:
				itemDef.name = "Pyromancer hood";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32211; //male
				itemDef.womanWear = 32218; // female
			    break;
				
			case 30304:
				itemDef.name = "Pyromancer robe";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32213; //male
				itemDef.womanWear = 32221; // female
			    break;
				
			case 30303:
				itemDef.name = "Pyromancer garb";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32215; //male
				itemDef.womanWear = 32223; // female
			    break;
				
			case 30302:
				itemDef.name = "Black skin color";
				itemDef.cost = 5;
				itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
			    break;
				
			case 30301:
				itemDef.name = "Purple skin color";
				itemDef.cost = 5;
				itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
			    break;
				
			case 30300:
				itemDef.name = "Red skin color";
				itemDef.cost = 5;
				itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
			    break;
				
			case 30279:
				itemDef.name = "Elite task legs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28210; //male
				itemDef.womanWear = 29057; // female
				return itemDef;
				
			case 30278:
				itemDef.name = "Hard task legs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28211; //male
				itemDef.womanWear = 29059; // female
				return itemDef;
				
			case 30277:
				itemDef.name = "Medium task legs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28209; //male
				itemDef.womanWear = 29058; // female
				return itemDef;
				
			case 30276:
				itemDef.name = "Easy task legs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28208; //male
				itemDef.womanWear = 29060; // female
				return itemDef;
				
			case 30275:
				itemDef.setDefaults();
				itemDef.name = "Elite task armour";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28435; //male
				itemDef.womanWear = 29066; // female
				return itemDef;
				
			case 30274:
				itemDef.setDefaults();
				itemDef.name = "Hard task armour";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28434; //male
				itemDef.womanWear = 29068; // female
				return itemDef;
				
			case 30273:
				itemDef.setDefaults();
				itemDef.name = "Medium task armour";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 29010; //male
				itemDef.womanWear = 29069; // female
				return itemDef;
				
			case 30272:
				itemDef.setDefaults();
				itemDef.name = "Easy task armour";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28317; //male
				itemDef.womanWear = 29067; // female
				return itemDef;
						
			case 30271:
				itemDef.name = "Elite task headgear";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 27610;
				itemDef.womanWear = 29042;
				return itemDef;
						
			case 30270:
				itemDef.name = "Hard task headgear";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 10482;
				itemDef.womanWear = 29043;
				return itemDef;
						
			case 30269:
				itemDef.name = "Medium task headgear";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 25749;
				itemDef.womanWear = 29041;
				return itemDef;
			
			case 30268:
				itemDef.name = "Easy task headgear";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 28212;
				itemDef.womanWear = 29061;
				return itemDef;
							
			case 30267:
				itemDef.name = "Bounty teleport scroll";
				return itemDef;
				
			case 30266:
				itemDef.name = "Elder chaos hood";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28285; //male
				itemDef.womanWear = 32073; // female
				return itemDef;
				
			case 30265:
				itemDef.name = "Elder chaos robe";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32062; //male
				itemDef.womanWear = 32064; // female
				return itemDef;
				
			case 30264:
				itemDef.name = "Elder chaos top";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 32063; //male
				itemDef.womanWear = 32066; // female
				return itemDef;

			case 30262://done
				itemDef.cost = 1;
				return itemDef;

			case 30261://done
				itemDef.cost = 1;
				return itemDef;

			case 30260://done
				itemDef.cost = 1;
				return itemDef;

			case 30259://done
				itemDef.cost = 1;
				return itemDef;

			case 30258:
				itemDef.name = "ring of suffering (ri)";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30257:
				itemDef.name = "Nunchaku";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31906; //male
				itemDef.womanWear = 31906; // female
				return itemDef;
			
			case 30256:
				itemDef.name = "Cyclops head";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31745; //male
				itemDef.womanWear = 31832; // female
				return itemDef;
			
			case 30255:
				itemDef.name = "Zombie head";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31912; //male
				itemDef.womanWear = 31910; // female
				return itemDef;
			
			case 30254:
				itemDef.name = "Explorer backpack";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 28485; //male
				itemDef.womanWear = 28485; // female
				return itemDef;
			
			case 30253:
				itemDef.name = "Wooden shield (g)";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 517; //male
				itemDef.womanWear = 517; // female
				//itemDef.osrs = true;
				return itemDef;
			
			case 30252:
				itemDef.name = "Team cape i";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31770; //male
				itemDef.womanWear = 31770; // female
				return itemDef;
			
			case 30251:
				itemDef.name = "Team cape x";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31773; //male
				itemDef.womanWear = 31773; // female
				return itemDef;
			
			case 30250:
				itemDef.name = "Team cape zero";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31772; //male
				itemDef.womanWear = 31772; // female
				return itemDef;
			
			case 30249:
				itemDef.name = "Imp mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 18556; //male
				itemDef.womanWear = 28553; // female
				return itemDef;
			
			case 30248:
				itemDef.name = "Imp mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 18558; //male
				itemDef.womanWear = 28555; // female
				return itemDef;
			
			case 30247:
				itemDef.name = "Large spade";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Dig", null, "Drop"};
				itemDef.manWear = 13938; //male
				itemDef.womanWear = 13938; // female
				return itemDef;
			
			case 30246:
				itemDef.name = "Torn clue scroll (part 3)";
				itemDef.inventoryOptions = new String[]{"Combine", "Inspect", null, null, "Destroy"};
				return itemDef;
			
			case 30245:
				itemDef.name = "Torn clue scroll (part 2)";
				itemDef.inventoryOptions = new String[]{"Combine", "Inspect", null, null, "Destroy"};
				return itemDef;
			
			case 30244:
				itemDef.name = "Torn clue scroll (part 1)";
				itemDef.inventoryOptions = new String[]{"Combine", "Inspect", null, null, "Destroy"};
				return itemDef;
			
			case 30243:
				itemDef.name = "Light box";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Detroy"};
				return itemDef;
			
			case 30242:
				itemDef.name = "Strange device";
				itemDef.inventoryOptions = new String[]{"Feel", null, null, null, "Drop"};
				return itemDef;
			
			case 30241:
				itemDef.name = "Revenant ether";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.stackable = true;
				return itemDef;
				
			case 30240:
				itemDef.name = "Revenant ether";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.stackable = true;
				return itemDef;
				
			case 30239:
				itemDef.name = "Revenant ether";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.stackable = true;
				return itemDef;
				
			case 30238:
				itemDef.name = "Revenant ether";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.stackable = true;
				return itemDef;
			
			case 30237:
				itemDef.name = "Revenant ether";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.stackable = true;
				return itemDef;
			
			case 30236:
				itemDef.name = "Bracelet of ethereum (uncharged)";
				itemDef.inventoryOptions = new String[]{null, "Wear", "Toggle-absorption", "Dismantle", "Drop"};
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				return itemDef;
			
			case 30235:
				itemDef.name = "Bracelet of ethereum";
				itemDef.inventoryOptions = new String[]{null, "Wear", "Check", "Toggle-absorption", "Drop"};
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				//itemDef.osrs = true;
				return itemDef;
			
			case 30234:
				itemDef.name = "Clue bottle (elite)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
			case 30233:
				itemDef.name = "Clue bottle (hard)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
			case 30232:
				itemDef.name = "Clue bottle (medium)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
			
			case 30231:
				itemDef.name = "Clue bottle (easy)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
			
			case 30230:
				itemDef.name = "Puzzle box (master)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Destroy"};
				return itemDef;
			
			case 30229:
				itemDef.name = "Puzzle box (master)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Destroy"};
				return itemDef;
			
			case 30228:
				itemDef.name = "Puzzle box (master)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Destroy"};
				return itemDef;
			
			case 3569:
			case 3565:
			case 2798:
				itemDef.name = "Puzzle box (hard)";
				return itemDef;
			
			case 30227:
				itemDef.name = "Clue geode (elite)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
			case 30226:
				itemDef.name = "Clue geode (hard)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
			case 30225:
				itemDef.name = "Clue geode (medium)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
			
			case 30224:
				itemDef.name = "Clue geode (easy)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
			
			case 30223:
				itemDef.name = "Clue nest (elite)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
			case 30222:
				itemDef.name = "Clue nest (hard)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
				
			case 30221:
				itemDef.name = "Clue nest (medium)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
			
			case 30220:
				itemDef.name = "Clue nest (easy)";
				itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
				return itemDef;
			
			case 30219:
				itemDef.name = "Dragon scimitar (or)";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31898; //male
				itemDef.womanWear = 31898; // female
				return itemDef;
			
			case 30218:
				itemDef.name = "Dragon scimitar ornament kit";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30217:
				itemDef.name = "Dragon defender (t)";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31904; //male
				itemDef.womanWear = 31897; // female
				return itemDef;
			
			case 30216:
				itemDef.name = "Dragon defender ornament kit";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30215:
				itemDef.name = "Arclight";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31652; //male
				itemDef.womanWear = 31651; // female
				return itemDef;
				
			case 30214:
				itemDef.name = "Fancy tiara";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31795; //male
				itemDef.womanWear = 31864; // female
				return itemDef;
			
			case 30213:
				itemDef.name = "Half moon spectacles";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31791; //male
				itemDef.womanWear = 31861; // female
				return itemDef;
			
			case 30212:
				itemDef.name = "Ale of the gods";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
				itemDef.manWear = 31899; //male
				itemDef.womanWear = 31902; // female
				return itemDef;
			
			case 30211:
				itemDef.name = "Bowl wig";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31753; //male
				itemDef.womanWear = 31822; // female
				return itemDef;
			
			case 30210:
				itemDef.name = "Bloodhound";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30209:
				itemDef.name = "$500 casket";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Open", null, "Drop"};
				itemDef.manWear = 31907; //male
				itemDef.womanWear = 31907; // female
				return itemDef;
			
			case 30208:
				itemDef.name = "Amulet of torture";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31227; //male
				itemDef.womanWear = 31233; // female
				return itemDef;
			
			case 30207:
				itemDef.name = "Tormented bracelet";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				return itemDef;
			
			case 30206:
				itemDef.name = "Necklace of anguish";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31228; //male
				itemDef.womanWear = 31232; // female
				return itemDef;
			
			case 30205:
				itemDef.name = "ring of suffering (r)";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30204:
				itemDef.name = "ring of suffering (i)";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30203:
				itemDef.name = "ring of suffering";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30202:
				itemDef.name = "Zenyte amulet";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31227; //male
				itemDef.womanWear = 31233; // female
				return itemDef;
			
			case 30201:
				itemDef.name = "Zenyte ring";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30200:
				itemDef.name = "Zenyte necklace";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31228; //male
				itemDef.womanWear = 31232; // female
				return itemDef;
			
			case 30199:
				itemDef.name = "Zenyte bracelet";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 24558; //male
				itemDef.womanWear = 24564; // female
				return itemDef;
				
			case 30198:
				itemDef.name = "Zenyte shard";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
				
			case 30197:
				itemDef.name = "Zenyte amulet (u)";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30196:
				itemDef.name = "Uncut zenyte";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30195:
				itemDef.name = "Zenyte";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				//itemDef.osrs = true;
				return itemDef;
			
			case 30194:
				itemDef.name = "Ring of illusion";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30193:
				itemDef.name = "Ring of nature";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			case 30192:
				itemDef.name = "Ring of coins";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				return itemDef;
			
			
			case 30191:
				itemDef.name = "Jungle demon mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31744; //male
				itemDef.womanWear = 31827; // female
				return itemDef;
			
			case 30190:
				itemDef.name = "Old demon mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31748; //male
				itemDef.womanWear = 31835; // female
				return itemDef;
			
			case 30189:
				itemDef.name = "Black demon mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31746; //male
				itemDef.womanWear = 31824; // female
				return itemDef;
			
			case 30188:
				itemDef.name = "Greater demon mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31749; //male
				itemDef.womanWear = 31829; // female
				return itemDef;
			
			case 30187:
				itemDef.name = "Lesser demon mask";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
				itemDef.manWear = 31755; //male
				itemDef.womanWear = 31838; // female
				return itemDef;
				
			case 30186:
				itemDef.name = "Ancient relic";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30185:
				itemDef.name = "Ancient effigy";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30184:
				itemDef.name = "Ancient medallion";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30183:
				itemDef.name = "Ancient statuette";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
			
			case 30182:
				itemDef.name = "Ancient totem";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
				
			case 30181:
				itemDef.name = "Ancient emblem";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				return itemDef;
		case 30180:
			itemDef.name = "White partyhat";
			itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
			break;
		case 30179:
			itemDef.name = "Green skin color";
			itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
		    break;
		case 30178:
			itemDef.name = "Blue skin color";
			itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
		    break;
		case 30177:
			itemDef.name = "White skin color";
			itemDef.inventoryOptions = new String[] {"Drink", null, null, null, "Drop"};
		    break;
		case 30176:
			itemDef.name = "Ultra Mystery box";
			itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
		    break;
		case 30175:
			itemDef.name = "Legendary Mystery box";
			itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
		    break;
		case 30174:
			itemDef.name = "Super Mystery box";
			itemDef.inventoryOptions = new String[] { "Open", null, null, null, "Drop" };
		    break;
		case 6570:
			if (itemDef.inventoryOptions != null) {
				itemDef.inventoryOptions[4] = "Destroy";
			}
		    break;
		case 5733:
			itemDef.inventoryOptions = new String[]{"Eat", "Slice", "Peel", "Mush", "Drop"};
			break;
		case 30173:
			itemDef.name = "Mummy's feet";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31815; //male
			itemDef.womanWear = 31881; // female
			return itemDef;
		
		case 30172:
			itemDef.name = "Mummy's legs";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31784; //male
			itemDef.womanWear = 31855; // female
			return itemDef;
		
		case 30171:
			itemDef.name = "Mummy's hands";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31778; //male
			itemDef.womanWear = 31853; // female
			return itemDef;
		
		case 30170:
			itemDef.name = "Mummy's body";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31808; //male
			itemDef.womanWear = 31878; // female
			return itemDef;
		
		case 30169:
			itemDef.name = "Mummy's head";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31758; //male
			itemDef.womanWear = 31831; // female
			return itemDef;
		
		case 30168:
			itemDef.name = "Boots of darkness";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31818; //male
			itemDef.womanWear = 31885; // female
			return itemDef;
		
		case 30167:
			itemDef.name = "Robe bottom of darkness";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31788; //male
			itemDef.womanWear = 31858; // female
			return itemDef;
		
		case 30166:
			itemDef.name = "Gloves of darkness";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31774; //male
			itemDef.womanWear = 31852; // female
			return itemDef;
		
		case 30165:
			itemDef.name = "Robe top of darkness";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31814; //male
			itemDef.womanWear = 31877; // female
			return itemDef;
		
		case 30164:
			itemDef.name = "Hood of darkness";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31752; //male
			itemDef.womanWear = 31836; // female
			return itemDef;
		
		case 30163:
			itemDef.name = "Ankou socks";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31820; //male
			itemDef.womanWear = 31880; // female
			return itemDef;
		
		case 30162:
			itemDef.name = "Ankou's leggings";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31789; //male
			itemDef.womanWear = 31856; // female
			return itemDef;
		
		case 30161:
			itemDef.name = "Ankou gloves";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31779; //male
			itemDef.womanWear = 31851; // female
			return itemDef;
		
		case 30160:
			itemDef.name = "Ankou top";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31811; //male
			itemDef.womanWear = 31874; // female
			return itemDef;
		
		case 30159:
			itemDef.name = "Ankou mask";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31747; //male
			itemDef.womanWear = 31825; // female
			return itemDef;
		
		case 30158:
			itemDef.name = "Dragon cane";
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.manWear = 28626; //male
			itemDef.womanWear = 28626; // female
			return itemDef;
		
		case 30157:
			itemDef.name = "Shayzien house hood";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			return itemDef;
		
		case 30156:
			itemDef.name = "Shayzien house scarf";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31765; //male
			itemDef.womanWear = 31843; // female
			return itemDef;
		
		case 30155:
			itemDef.name = "Shayzien banner";
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.manWear = 31908; //male
			itemDef.womanWear = 31908; // female
			return itemDef;
		
		case 30154:
			itemDef.name = "Piscarilius house hood";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			return itemDef;
		
		case 30153:
			itemDef.name = "Piscarilius house scarf";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31762; //male
			itemDef.womanWear = 31844; // female
			return itemDef;
		
		case 30152:
			itemDef.name = "Piscarilius banner";
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.manWear = 31901; //male
			itemDef.womanWear = 31901; // female
			return itemDef;
		
		case 30151:
			itemDef.name = "Lovakengj house hood";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			return itemDef;
		
		case 30150:
			itemDef.name = "Lovakengj house scarf";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31763; //male
			itemDef.womanWear = 31841; // female
			return itemDef;
		
		case 30149:
			itemDef.name = "Lovakengj banner";
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.manWear = 31909; //male
			itemDef.womanWear = 31909; // female
			return itemDef;
		
		case 30148:
			itemDef.name = "Hosidius house hood";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			return itemDef;
		
		case 30147:
			itemDef.name = "Hosidius house scarf";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31760; //male
			itemDef.womanWear = 31842; // female
			return itemDef;
		
		case 30146:
			itemDef.name = "Hosidius banner";
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.manWear = 31900; //male
			itemDef.womanWear = 31900; // female
			return itemDef;
		
		case 30145:
			itemDef.name = "Arceuus house hood";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31751; //male
			itemDef.womanWear = 31826; // female
			return itemDef;
		
		case 30144:
			itemDef.name = "Arceuus house scarf";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31764; //male
			itemDef.womanWear = 31845; // female
			return itemDef;
		
		case 30143:
			itemDef.name = "Arceuus banner";
			itemDef.inventoryOptions = new String[]{null, "Wield", null, null, "Drop"};
			itemDef.manWear = 31896; //male
			itemDef.womanWear = 31896; // female
			return itemDef;
		
		case 30142:
			itemDef.name = "Samurai boots";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31816; //male
			itemDef.womanWear = 31882; // female
			return itemDef;
		
		case 30141:
			itemDef.name = "Samurai greaves";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31786; //male
			itemDef.womanWear = 31860; // female
			return itemDef;
		
		case 30140:
			itemDef.name = "Samurai gloves";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31776; //male
			itemDef.womanWear = 31847; // female
			return itemDef;
		
		case 30139:
			itemDef.name = "Samurai shirt";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31809; //male
			itemDef.womanWear = 31875; // female
			return itemDef;
		
		case 30138:
			itemDef.name = "Samurai kasa";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31756; //male
			itemDef.womanWear = 31830; // female
			return itemDef;
		
		case 30137:
			itemDef.name = "Rangers' tunic";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 28810; //male
			itemDef.womanWear = 28813; // female
			return itemDef;
			
		case 30136:
			itemDef.name = "Ranger gloves";
			itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Drop"};
			itemDef.manWear = 31781; //male
			itemDef.womanWear = 31849; // female
			return itemDef;
		
		case 8871:
			itemDef.name = "Magical Crate";
			itemDef.inventoryOptions = new String[]{"Open", "Wield", null, null, "Drop"};
			break;
			
		case 30100:
			itemDef.name = "Catalytic rune pack";
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			break;
			
		case 30101:
			itemDef.name = "Elemental rune pack";
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			break;
			
		case 30102:
			itemDef.name = "Adamant arrow pack";
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			break;
			
		case 30103:
			itemDef.name = "Rune arrow pack";
			itemDef.inventoryOptions = new String[]{"Open", null, null, null, "Drop"};
			break;
			
		case 30104:
			itemDef.name = "Ancient magicks tablet";
			itemDef.inventoryOptions = new String[]{"Break", null, null, null, "Drop"};
			break;
			
		case 30105:
			itemDef.name = "Bloody key";
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			break;
			
		case 30106:
			itemDef.name = "Bloodier key";
			itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
			break;
		
		case 30107:
			itemDef.name = "Survival tokens";
			itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
			itemDef.stackable = true;
			itemDef.cost = 1000;
			return itemDef;
			
			case 30116:
				itemDef.name = "Bunny feet";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 29189;
				itemDef.womanWear = 29190;
				return itemDef;
				
			case 30117:
				itemDef.name = "Bunny top";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 31188;
				itemDef.womanWear = 31192;
				return itemDef;
				
			case 30118:
				itemDef.name = "Bunny legs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 31186;
				itemDef.womanWear = 31190;
				return itemDef;
				
			case 30119:
				itemDef.name = "Bunny paws";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 31185;
				itemDef.womanWear = 31189;
				return itemDef;
				
			case 30120:
				itemDef.name = "Easter egg helm";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null, "Destroy"};
				itemDef.manWear = 6695;
				itemDef.womanWear = 32939;
				return itemDef;
				
				//LMS items
			case 30121:
				itemDef.setDefaults();
				itemDef.name = "Armadyl godsword";
				itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
				itemDef.manWear = 27731;
				itemDef.womanWear = 27731;
				return itemDef;
				
			case 30122:
				itemDef.setDefaults();
				itemDef.name = "Dragon claws";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 43660;
				itemDef.womanWear = 43651;
				return itemDef;
				
			case 30123:
				itemDef.setDefaults();
				itemDef.name = "Dragon 2h sword";
				itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
				itemDef.manWear = 11225;
				itemDef.womanWear = 11225;
				return itemDef;
				
			case 30124:
				itemDef.setDefaults();
				itemDef.name = "Abyssal whip";
				itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
				itemDef.manWear = 5409;
				itemDef.womanWear = 5409;
				return itemDef;
				
			case 30125:
				itemDef.setDefaults();
				itemDef.name = "Dragon warhammer";
				itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
				itemDef.manWear = 4037;
				itemDef.womanWear = 4038;
				return itemDef;
				
			case 30126:
				itemDef.setDefaults();
				itemDef.name = "Dragon sword";
				itemDef.manWear = 32676; 
				itemDef.womanWear = 32676; 
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, null};
				return itemDef;
				
			case 30127:
				itemDef.setDefaults();
				itemDef.name = "Elder maul";
				itemDef.manWear = 32678; 
				itemDef.womanWear = 32678; 
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null, null};
				return itemDef;
				
			case 30128:
				itemDef.name = "Dragon chainbody";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 3820;
				return itemDef;
				
			case 30129: 
				itemDef.name = "Dragon platebody";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 40207;
				itemDef.womanWear = 40427;
				return itemDef;
				
			case 30130:
				itemDef.name = "Dark bow";
				itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
				itemDef.manWear = 26279;
				itemDef.womanWear = 26279;
				return itemDef;
				
			case 30131:
				itemDef.name = "Master wand";
				itemDef.inventoryOptions = new String[] {null, "Wield", null, null, "Drop"};
				itemDef.manWear = 10702;
				itemDef.womanWear = 10702;
				return itemDef;
				
			case 30132:
				itemDef.name = "Third-age robe top";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 20160;
				itemDef.womanWear = 20207;
				return itemDef;
				
			case 30133:
				itemDef.name = "Third-age robe";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 20143;
				itemDef.womanWear = 20191;
				return itemDef;
				
			case 30134:
				itemDef.name = "Ahrim's robe top";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 6669;
				itemDef.womanWear = 6697;
				return itemDef;
				
			case 30135:
				itemDef.name = "Ahrim's robe skirt";
				itemDef.inventoryOptions = new String[] {null, "Wear", null, null, "Drop"};
				itemDef.manWear = 6659;
				itemDef.womanWear = 40595;
				return itemDef;
			
		//Overloads
		case 15332:
			itemDef.noteId = 22322;
			return itemDef;
		case 22322:
			itemDef.noteId = 15332;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15333:
			itemDef.noteId = 22323;
			return itemDef;
		case 22323:
			itemDef.noteId = 15333;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15334:
			itemDef.noteId = 22324;
			return itemDef;
		case 22324:
			itemDef.noteId = 15334;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15335:
			itemDef.noteId = 22325;
			return itemDef;
		case 22325:
			itemDef.noteId = 15335;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
			
		//extreme range
		case 15324:
			itemDef.noteId = 22326;
			return itemDef;
		case 22326:
			itemDef.noteId = 15324;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15325:
			itemDef.noteId = 22327;
			return itemDef;
		case 22327:
			itemDef.noteId = 15325;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15326:
			itemDef.noteId = 22328;
			return itemDef;
		case 22328:
			itemDef.noteId = 15326;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15327:
			itemDef.noteId = 22329;
			return itemDef;
		case 22329:
			itemDef.noteId = 15327;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//extreme strength
		case 15312:
			itemDef.noteId = 22330;
			return itemDef;
		case 22330:
			itemDef.noteId = 15312;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15313:
			itemDef.noteId = 22331;
			return itemDef;
		case 22331:
			itemDef.noteId = 15313;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15314:
			itemDef.noteId = 22332;
			return itemDef;
		case 22332:
			itemDef.noteId = 15314;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15315:
			itemDef.noteId = 22333;
			return itemDef;
		case 22333:
			itemDef.noteId = 15315;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//extreme mage
		case 15320:
			itemDef.noteId = 22334;
			return itemDef;
		case 22334:
			itemDef.noteId = 15320;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15321:
			itemDef.noteId = 22335;
			return itemDef;
		case 22335:
			itemDef.noteId = 15321;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15322:
			itemDef.noteId = 22336;
			return itemDef;
		case 22336:
			itemDef.noteId = 15322;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15323:
			itemDef.noteId = 22337;
			return itemDef;
		case 22337:
			itemDef.noteId = 15323;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//extreme attack
		case 15308:
			itemDef.noteId = 22338;
			return itemDef;
		case 22338:
			itemDef.noteId = 15308;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15309:
			itemDef.noteId = 22339;
			return itemDef;
		case 22339:
			itemDef.noteId = 15309;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15310:
			itemDef.noteId = 22340;
			return itemDef;
		case 22340:
			itemDef.noteId = 15310;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15311:
			itemDef.noteId = 22341;
			return itemDef;
		case 22341:
			itemDef.noteId = 15311;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//extreme defence
		case 15316:
			itemDef.noteId = 22342;
			return itemDef;
		case 22342:
			itemDef.noteId = 15316;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15317:
			itemDef.noteId = 22343;
			return itemDef;
		case 22343:
			itemDef.noteId = 15317;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15318:
			itemDef.noteId = 22344;
			return itemDef;
		case 22344:
			itemDef.noteId = 15318;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15319:
			itemDef.noteId = 22345;
			return itemDef;
		case 22345:
			itemDef.noteId = 15319;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//super prayer
		case 15328:
			itemDef.noteId = 22346;
			return itemDef;
		case 22346:
			itemDef.noteId = 15328;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15329:
			itemDef.noteId = 22347;
			return itemDef;
		case 22347:
			itemDef.noteId = 15329;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15330:
			itemDef.noteId = 22348;
			return itemDef;
		case 22348:
			itemDef.noteId = 15330;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15331:
			itemDef.noteId = 22349;
			return itemDef;
		case 22349:
			itemDef.noteId = 15331;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//super antifire
		case 15304:
			itemDef.noteId = 22350;
			return itemDef;
		case 22350:
			itemDef.noteId = 15304;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15305:
			itemDef.noteId = 22351;
			return itemDef;
		case 22351:
			itemDef.noteId = 15305;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15306:
			itemDef.noteId = 22352;
			return itemDef;
		case 22352:
			itemDef.noteId = 15306;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15307:
			itemDef.noteId = 22353;
			return itemDef;
		case 22353:
			itemDef.noteId = 15307;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		//spec restore
		case 15300:
			itemDef.noteId = 22354;
			return itemDef;
		case 22354:
			itemDef.noteId = 15300;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15301:
			itemDef.noteId = 22355;
			return itemDef;
		case 22355:
			itemDef.noteId = 15301;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15302:
			itemDef.noteId = 22356;
			return itemDef;
		case 22356:
			itemDef.noteId = 15302;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
		case 15303:
			itemDef.noteId = 22357;
			return itemDef;
		case 22357:
			itemDef.noteId = 15303;
			itemDef.certTemplateID = 799;
			itemDef.toNote();
			return itemDef;
			
		case 19668:
		    itemDef.name = "Strong taming rope";
		    return itemDef;
			
	    case 19667:
	    	itemDef.name = "Taming rope";
	    	return itemDef;
	    	
	    case 19702:
	    	itemDef.name = "Taming rope";
	    	return itemDef;
	    	
	    case 19665:
	    	itemDef.name = "Training rope";
	    	return itemDef;
	    	
	    case 19666:
	    	itemDef.name = "Training rope";
	    	return itemDef;
	    	
	    case 20061:
	    	itemDef.name = "Random Pet Box";
	    	return itemDef;
	    	
	    case 15360:
	    	itemDef.name = "Pet Double Exp Scroll";
	    	return itemDef;
	    case 21821:
	    	itemDef.name = "Pet Stat Reset Scroll";
	    	return itemDef;
	    case 21403:
	    	itemDef.name = "Pet Boost";
	    	return itemDef;
			case 21473: // vanguard top
				itemDef.manWear = 44812;
				itemDef.womanWear = 44812;
				break;
			case 21474: // vanguard legs
				itemDef.manWear = 45232;
				break;
			case 21468: // Trickster robe
				itemDef.womanWear = 44786;
				itemDef.manWear = 44786;
				break;
			case 21469: // Trickster legs
				itemDef.womanWear = 44770;
				itemDef.manWear = 44770;
				break;
			case 12391:
				itemDef.name = "Gilded boots";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				itemDef.manWear = 4954;
				itemDef.womanWear = 5031;
				break;
			case 12399:
				itemDef.name = "Partyhat & specs";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				itemDef.manWear = 28505;
				itemDef.womanWear = 28576;
				break;
			case 12002:
				itemDef.name = "Occult necklace";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				itemDef.manWear = 28445;
				itemDef.womanWear = 28445;
				break;

			case 11791:
				itemDef.name = "Staff of the dead";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null,
						"Drop"};
				itemDef.manWear = 5232;
				itemDef.womanWear = 5232;
				break;

			case 11785:
				itemDef.name = "Armadyl crossbow";
				itemDef.inventoryOptions = new String[]{null, "Wield", null, null,
						"Drop"};
				itemDef.manWear = 19839;
				itemDef.womanWear = 19839;
				break;

			case 12004:
				itemDef.name = "Kraken tentacle";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[4] = "Drop";
				break;

			case 12006:
				itemDef.name = "Abyssal tentacle";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.inventoryOptions[3] = "Check";
				itemDef.inventoryOptions[4] = "Dissolve";
				itemDef.manWear = 28446;
				itemDef.womanWear = 28446;
				break;

			case 11924:
				itemDef.name = "Malediction ward";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.inventoryOptions[4] = "Drop";
				itemDef.manWear = 9347;
				itemDef.womanWear = 9347;
				break;

			case 11926:
				itemDef.name = "Odium ward";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.inventoryOptions[4] = "Drop";
				itemDef.manWear = 9347;
				itemDef.womanWear = 9347;
				break;

				// ------------- new osrs items start
				
			case 12924: // done
				itemDef.name = "Toxic blowpipe (empty)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Wield", "Drop" };
				itemDef.manWear = 14405;
				itemDef.womanWear = 14405;
				return itemDef;

			case 12926: // done
				itemDef.name = "Toxic blowpipe";
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Unload", "Uncharge" };
				itemDef.manWear = 14403;
				itemDef.womanWear = 14403;
				return itemDef;
				
			case 30000:
				itemDef.name = "Tanzanite fang";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 30001:
				itemDef.name = "Serpentine visage";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 30002:
				itemDef.name = "Serpentine helm (uncharged)";
				itemDef.inventoryOptions = new String[] { null, null, null, "Wear", "Drop" };
				itemDef.manWear = 14396;
				itemDef.womanWear = 14397;
				return itemDef;

			case 30003:
				itemDef.name = "Serpentine helm";
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", null, "Uncharge" };
				itemDef.manWear = 60084; //14395;
				itemDef.womanWear = 60085; //14398;
				return itemDef;

			case 30004:
				itemDef.name = "Magic fang";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 30005://done
				itemDef.setDefaults();
				itemDef.name = "Zulrah's scales";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.stackable = true;
				return itemDef;
				
			case 30006:
				itemDef.name = "Zul-andra teleport";
				itemDef.inventoryOptions = new String[] { "Teleport", null, null, null, "Drop" };
				return itemDef;

			case 30007:
				itemDef.name = "Pet snakeling";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 30008:
				itemDef.name = "Pet snakeling";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;
				
			case 30009:
				itemDef.name = "Pet snakeling";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;
				

			case 30010:
				itemDef.name = "Pegasian boots";
				itemDef.manWear = 60090; //29252;
				itemDef.womanWear = 60091; //29253;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				return itemDef;
				
			case 30011:
				itemDef.name = "Toxic staff of the dead";
				itemDef.manWear = 60142; //14402;
				itemDef.womanWear = 60142; //14402;
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", null, "Uncharge"};
				return itemDef;

			case 30012:
				itemDef.name = "Primordial Boots";
				itemDef.manWear = 60093; //29250;
				itemDef.womanWear = 60094; //29255;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				return itemDef;

			case 30013:
				itemDef.name = "Eternal boots";
				itemDef.manWear = 60096; //29249;
				itemDef.womanWear = 60097; //29254;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				return itemDef;
				
			case 30014:
				itemDef.setDefaults();
				itemDef.name = "Trident of the seas (Charged)";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", "Uncharge", "Drop"};
				itemDef.manWear = 28230; //28230;
				itemDef.womanWear = 28230; //28230;
				itemDef.noteId = 30015;
				return itemDef;
			
			case 30015:
				itemDef.setDefaults();
				itemDef.name = "Trident of the seas (Charged)";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.certTemplateID = 799;
				itemDef.noteId = 30014;
				itemDef.toNote();
				return itemDef;
			
			case 30016:
				itemDef.setDefaults();
				itemDef.name = "Uncharged Trident of the seas";
				itemDef.inventoryOptions = new String[]{null, "Wield", "Check", null, "Drop"};
				itemDef.manWear = 28230; //28230;
				itemDef.womanWear = 28230; //28230;
				itemDef.noteId = 30017;
				break;
			
			case 30017: //noted
				itemDef.setDefaults();
				itemDef.name = "Uncharged Trident of the seas";
				itemDef.inventoryOptions = new String[]{null, null, null, null, "Drop"};
				itemDef.certTemplateID = 799;
				itemDef.noteId = 30016;
				itemDef.toNote();
				return itemDef;
				
			case 30018:
				itemDef.name = "Abyssal bludgeon";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.manWear = 60103;
				itemDef.womanWear = 60103;
				return itemDef;
				
			case 30019:
				//itemDef.setDefaults();
				itemDef.name = "Volcanic abyssal whip";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.manWear = 60105; //8950;
				itemDef.womanWear = 60105; //8950;
				break;
				
			case 30020:
				itemDef.name = "Abyssal dagger";
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Uncharge", "Drop" };
				itemDef.manWear = 60107; //29425;
				itemDef.womanWear = 60107; //29425;
				return itemDef;
				
			case 30021:
				itemDef.name = "Abyssal dagger(p++)";
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", "Uncharge", "Drop" };
				itemDef.manWear = 60107;
				itemDef.womanWear = 60107;
				return itemDef;
					
			case 30022:
				itemDef.name = "Dragon warhammer";
				itemDef.manWear = 60109; //4037;
				itemDef.womanWear = 60110; //4038;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop", };
				return itemDef;
				
			case 30023:
				itemDef.setDefaults();
				itemDef.name = "Heavy ballista";
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.manWear = 60112; //31236;
				itemDef.womanWear = 60112; //31236;
				return itemDef;
				
			case 30024:
				itemDef.setDefaults();
				itemDef.name = "Light ballista";
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.manWear = 60129; //31237;
				itemDef.womanWear = 60129; //31237;
				return itemDef;
				
			case 30025:
				itemDef.name = "Dragon javelin";
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.stackable = true;
				return itemDef;
				
			case 30026:
				itemDef.name = "Ballista limbs";
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				return itemDef;
				
			case 30027:
				itemDef.setDefaults();
				itemDef.name = "Heavy frame";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
			    return itemDef;
			    
			case 30028:
				itemDef.setDefaults();
				itemDef.name = "Light frame";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
			    return itemDef;
			    
			case 30029:
				itemDef.setDefaults();
				itemDef.name = "Unstrung light ballista";
		    	return itemDef;
				
			case 30030:
				itemDef.setDefaults();
				itemDef.name = "Unstrung heavy ballista";
			    return itemDef;
			   
			case 30031:
				itemDef.setDefaults();
				itemDef.name = "Ballista spring";
			    return itemDef;
			    
			case 30032:
				itemDef.setDefaults();
				itemDef.name = "Incomplete light ballista";
			    return itemDef;
			
			case 30033:
				itemDef.name = "Incomplete heavy ballista";
			    return itemDef;
			    
			case 30034:
				itemDef.name = "Monkey tail";
			    return itemDef;
			
			case 30035:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.stackable = true;
				return itemDef;
				
			case 30036:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.stackable = true;
			    return itemDef;
			
			case 30037:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.stackable = true;
			    return itemDef;
			    
			case 30038:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.stackable = true;
			    return itemDef;
			    
			case 30039:
				itemDef.setDefaults();
				itemDef.name = "Javelin shafts";
				itemDef.stackable = true;
			    return itemDef;
			
			case 30040:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.stackable = true;
				return itemDef;
			
			case 30041:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.stackable = true;
				return itemDef;
				
			case 30042:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.stackable = true;
				return itemDef;
			
			case 30043:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.stackable = true;
			return itemDef;
			
			case 30044:
				itemDef.setDefaults();
				itemDef.name = "Dragon javelin heads";
				itemDef.stackable = true;
			return itemDef;
			    
			case 30045:
				itemDef.name = "Platinum token";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				itemDef.stackable = true;
				itemDef.cost = 1000;
				return itemDef;
				
			case 30046:
				itemDef.name = "Saradomin's tear";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 30047:
				itemDef.setDefaults();
				itemDef.name = "Rune pouch";
				itemDef.inventoryOptions = new String[] { "Open", null, null, "Empty", "Destroy" };
				return itemDef;
				
			case 30048:
				itemDef.setDefaults();
				itemDef.name = "Looting bag";
				itemDef.inventoryOptions = new String[] { "Check", null, "Deposit", null, "Destroy" };
				break;
				
			case 30049:
				itemDef.name = "Dragon pickaxe upgrade kit";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;
				
			case 30050:
				itemDef.name = "Frozen whip mix";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30051:
				itemDef.name = "Volcanic whip mix";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;
				
			case 30052:
				itemDef.name = "Gilded scimitar";
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.manWear = 60123; //490;
				itemDef.womanWear = 60123; //490;
				break;
				
			case 30053:
				itemDef.setDefaults();
				itemDef.name = "Bucket helm (g)";
				itemDef.manWear = 60146; //31759;
				itemDef.womanWear = 60145; //31833;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
			return itemDef;

			case 30054:
				itemDef.name = "Dragon pickaxe (or)";
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Revert" };
				itemDef.manWear = 60118; //8962;
				itemDef.womanWear = 60118; //8962;
				break;
				
			case 30055:
				itemDef.name = "Magic shortbow scroll";
				itemDef.inventoryOptions = new String[] { null, null, null, null, "Drop" };
				break;

			case 30056:
				itemDef.name = "Magic shortbow (i)";
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Drop" };
				itemDef.manWear = 512; 
				itemDef.womanWear = 512; 
				return itemDef;
				
			case 30057:
				itemDef.name = "Lava dragon mask";
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Drop" };
				itemDef.manWear = 60125; //28512;
				itemDef.womanWear = 60126; //28581;
				break;
				
			case 30058:
				itemDef.setDefaults();
				itemDef.name = "Black h'ween mask";
				itemDef.manWear = 60149; //3188;
				itemDef.womanWear = 60148; //3192;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Destroy"};
			    return itemDef;
			
			case 30059:
				itemDef.name = "Rainbow partyhat";
				itemDef.manWear = 60152; //16246;
				itemDef.womanWear = 60151; //16248;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, "Destroy"};
			    return itemDef;
			    
			case 30060:
				itemDef.setDefaults();
				itemDef.name = "3rd age cloak";
				itemDef.manWear = 60155; //28483;
				itemDef.womanWear = 60154; //28560;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
				return itemDef;
				
			case 30061:
				itemDef.name = "3rd age bow";
				itemDef.manWear = 60157; //28622;
				itemDef.womanWear = 60157; //28622;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
			return itemDef;
			
			case 30062:
				itemDef.setDefaults();
				itemDef.name = "3rd age longsword";
				itemDef.manWear = 60159; //28618;
				itemDef.womanWear = 60159; //28618;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
			    return itemDef;
			    
			case 30063:
				itemDef.setDefaults();
				itemDef.name = "3rd age wand";
				itemDef.manWear = 60161; //28619;
				itemDef.womanWear = 60161; //28619;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				return itemDef;
				
			case 30064:
				itemDef.setDefaults();
				itemDef.name = "3rd age pickaxe";
				itemDef.manWear = 60164; //31887;
				itemDef.womanWear = 60163; //31894;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				return itemDef;
			
			case 30065:
				itemDef.setDefaults();
				itemDef.name = "3rd age axe";
				itemDef.manWear = 60167; //31891;
				itemDef.womanWear = 60166; //31888;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				return itemDef;
				
				
			case 30066:
				itemDef.name = "Granite maul";
				itemDef.manWear = 60170; //28992;
				itemDef.womanWear = 60170; //28992;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, "Revert"};
				return itemDef;

			case 30067:
				itemDef.setDefaults();
				itemDef.name = "Granite clamp";
				return itemDef;
				
			case 30068:
				itemDef.name = "Infernal harpoon";
				itemDef.manWear = 60172; //32673;
				itemDef.womanWear = 60172; //32673;
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check", null, null};
			return itemDef;

			case 30069:
				itemDef.setDefaults();
				itemDef.name = "Dragon harpoon";
				itemDef.manWear = 60174; //32670;
				itemDef.womanWear = 60174; //32670;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
			return itemDef;
			
			case 30070:
				itemDef.setDefaults();
				itemDef.name = "Slayer helmet";
				itemDef.manWear = 60208; //16274;
				itemDef.womanWear = 60209; //16276;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
				return itemDef;
			
			case 30071:
				itemDef.name = "Slayer helmet (i)";
				itemDef.manWear = 60208; //16274;
				itemDef.womanWear = 60209; //16276;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
				return itemDef;
				
			case 30072:
				itemDef.name = "Black slayer helmet";
				itemDef.manWear = 60211; //31549;
				itemDef.womanWear = 60212; //31552;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
			return itemDef;
			
			case 30073:
				itemDef.name = "Black slayer helmet (i)";
				itemDef.manWear = 60211; //31549;
				itemDef.womanWear = 60212; //31552;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
			return itemDef;
			
			case 30074:
				itemDef.name = "Green slayer helmet";
				itemDef.manWear = 60214; //31548;
				itemDef.womanWear = 60215; //31553;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
			return itemDef;

			case 30075:
				itemDef.name = "Green slayer helmet (i)";
				itemDef.manWear = 60214; //31548;
				itemDef.womanWear = 60215; //31553;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
			return itemDef;

			case 30076:
				itemDef.name = "Red slayer helmet";
				itemDef.manWear = 60217; //31550;
				itemDef.womanWear = 60218; //31551;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
			return itemDef;

			case 30077:
				itemDef.name = "Red slayer helmet (i)";
				itemDef.manWear = 60217; //31550;
				itemDef.womanWear = 60218; //31551;
				itemDef.inventoryOptions = new String[] { null, "Wear", "Check", "Disassemble", null};
			return itemDef;
			
			case 30081:
				itemDef.name = "Unholy helmet";
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null};
			return itemDef;
			
			case 30082:
				itemDef.name = "King black bonnet";
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null};
			return itemDef;
			
			case 30083:
				itemDef.name = "Kalhpite khat";
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null};
			return itemDef;
	        // RAID ITEMS
			
			case 30084:
				itemDef.name = "Ancestral robe bottom";
				itemDef.manWear = 60199; //32653;
				itemDef.womanWear = 60198; //32662;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
			return itemDef;

			case 30085:
				itemDef.name = "Ancestral robe top";
				itemDef.manWear = 60196; //32657;
				itemDef.womanWear = 60195; //32664;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
			return itemDef;

			case 30086:
				itemDef.name = "Ancestral hat";
				itemDef.manWear = 60193; //32655;
				itemDef.womanWear = 60192; //32663;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
			return itemDef;
			
			case 30087:
				itemDef.setDefaults();
				itemDef.name = "Dragon hunter crossbow";
				itemDef.manWear = 60190; //32668;
				itemDef.womanWear = 60190; //32668;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
			return itemDef;
			
			case 30088:
				itemDef.setDefaults();
				itemDef.name = "Dragon sword";
				itemDef.manWear = 60188; //32676;
				itemDef.womanWear = 60188; //32676;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				return itemDef;
			
			case 30089:
				itemDef.setDefaults();
				itemDef.name = "Kodai wand";
				itemDef.manWear = 60186; //32669;
				itemDef.womanWear = 60186; //32669;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				return itemDef;

			case 30090:
				itemDef.setDefaults();
				itemDef.name = "Elder maul";
				itemDef.manWear = 60184; //32678;
				itemDef.womanWear = 60184; //32678;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				return itemDef;
			
			case 30091:
				itemDef.name = "Twisted buckler";
				itemDef.manWear = 60182; //32667;
				itemDef.womanWear = 60182; //32667;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
				return itemDef;

			case 30092:
				itemDef.setDefaults();
				itemDef.name = "Twisted bow";
				itemDef.manWear = 60180; //32674;
				itemDef.womanWear = 60180; //32674;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
			return itemDef;
			
			case 30093:
				itemDef.setDefaults();
				itemDef.name = "Dragon thrownaxe";
				itemDef.manWear = 60178; //32672;
				itemDef.womanWear = 60178; //32672;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, null, null};
				itemDef.stackable = true;
				return itemDef;
			
			case 30094:
				itemDef.name = "Dinh's bulwark";
				itemDef.manWear = 60176; //32671;
				itemDef.womanWear = 60176; //32671;
				itemDef.inventoryOptions = new String[] { null, "Wear", null, null, null};
			    return itemDef;
			
			case 30095:
				itemDef.name = "Dexterous prayer scroll";
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null};
			    return itemDef;
			
			case 30096:
				itemDef.name = "Arcane prayer scroll";
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null};
			    return itemDef;
			    
			case 30097:
				itemDef.name = "Torn prayer scroll";
				itemDef.inventoryOptions = new String[] { "Read", null, null, null, null};
			    return itemDef;
			    
			case 30098:
				itemDef.name = "Toxic staff (uncharged)";
				itemDef.manWear = 60142; //14402;
				itemDef.womanWear = 60142; //14402;
				itemDef.inventoryOptions = new String[] { null, "Wield", null, "Dismantle", "Drop"};
				return itemDef;
			
	        // END OF RAID ITEMS
			    
				/*case 13227:
				itemDef.name = "Eternal crystal";
				itemDef.modelID = 11092;
				itemDef.modelZoom = 740;
				itemDef.modelRotationY = 429;
				itemDef.modelRotationX = 225;
				itemDef.modelOffsetX = 5;
				itemDef.modelOffsetY = 5;
				itemDef.groundActions = new String[] { null, null, "Take", null, null };
				itemDef.itemActions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 13229:
				itemDef.name = "Pegasian crystal";
				itemDef.modelID = 11093;
				itemDef.modelZoom = 653;
				itemDef.modelRotationY = 229;
				itemDef.modelRotationX = 1818;
				itemDef.modelOffsetX = 0;
				itemDef.modelOffsetY = -8;
				itemDef.groundActions = new String[] { null, null, "Take", null, null };
				itemDef.itemActions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 13231:
				itemDef.name = "Primordial crystal";
				itemDef.modelID = 11094;
				itemDef.modelZoom = 653;
				itemDef.modelRotationY = 229;
				itemDef.modelRotationX = 1818;
				itemDef.modelOffsetX = 0;
				itemDef.modelOffsetY = -8;
				itemDef.groundActions = new String[] { null, null, "Take", null, null };
				itemDef.itemActions = new String[] { null, null, null, null, "Drop" };
				return itemDef;

			case 13233:
				itemDef.name = "Smouldering stone";
				itemDef.modelID = 11095;
				itemDef.modelZoom = 653;
				itemDef.modelRotationY = 229;
				itemDef.modelRotationX = 1818;
				itemDef.modelOffsetX = 0;
				itemDef.modelOffsetY = -8;
				itemDef.groundActions = new String[] { null, null, "Take", null, null };
				itemDef.itemActions = new String[] { null, null, null, null, "Drop" };
				return itemDef;*/
				
			/*
			
		


			

			
				}*/ 
				
				
				// new osrs items end
				
			case 14598:
				itemDef.stackable = true;
				break;

			case 9721:
				itemDef.name = "Yell scroll";
				break;

			case 13663:
				itemDef.name = "Spin ticket";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[0] = "Open ticket";
				break;

			case 15501:
				itemDef.name = "Skilling package";
				break;

			case 6542:
				itemDef.name = "PK package";
				break;

			case 3062:
				itemDef.name = "Coin box";
				break;

			case 786:
				itemDef.name = "$10 Scroll";
				break;

			case 1505:
				itemDef.name = "$50 Scroll";
				break;

			case 2396:
				itemDef.name = "$100 Scroll";
				break;

			case 10831:
				itemDef.name = "Empty money bag";
				break;

			case 10832:
				itemDef.name = "Light money bag";
				break;

			case 10833:
				itemDef.name = "Normal money bag";
				break;

			case 10834:
				itemDef.name = "Hefty money bag";
				break;

			case 10835:
				itemDef.name = "Bulging money bag";
				itemDef.stackable = true;
				break;

			case 10867:
				itemDef.name = "Achievement diary hood";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.manWear = 18914;
				itemDef.womanWear = 18967;
				itemDef.stackable = false;
				break;

			case 10868:
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.inventoryOptions[4] = "Drop";
				itemDef.manWear = 18946;
				itemDef.womanWear = 18984;
				itemDef.name = "Achievement diary cape (t)";
				itemDef.stackable = false;
				break;

			case 15740:
				itemDef.name = "Black Santa Hat";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.manWear = 189; // male wield model
				itemDef.womanWear = 366; // femArmModel
				break;

			case 15741:
				itemDef.name = "Black Partyhat";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				break;

			case 15742:
				itemDef.name = "Pink Partyhat";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.manWear = 187;
				itemDef.womanWear = 363;
				break;
	
			case 23639:
				itemDef.name = "TokHaar-Kal";
				itemDef.cost = 60000;
				itemDef.manWear = 62575;
				itemDef.womanWear = 62582;
				itemDef.stackable = false;
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wear";
				itemDef.inventoryOptions[4] = "Drop";
				break;
			/*
			 * case 21371: itemDef.modelID = 10247; break;
			 */

			case 2996:
				itemDef.name = "Vote Ticket";
				break;

			case 15098:
				itemDef.name = "Dice (up to 100)";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Public-roll";
				itemDef.inventoryOptions[2] = "Switch-dice";
				itemDef.inventoryOptions[4] = "Drop";
				break;
			case 15088:
				itemDef.name = "Dice (2, 6 sides)";
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Public-roll";
				itemDef.inventoryOptions[2] = "Switch-dice";
				itemDef.inventoryOptions[4] = "Drop";
				break;
			case 13263:
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Wield";
				itemDef.manWear = 6775; // male wield model
				itemDef.womanWear = 14112; // femArmModel
				itemDef.name = "Slayer helmet";
				break;
			case 405:
				itemDef.name = "2.5m Cash";
				break;

			case 761:
				itemDef.name = "Double Experience";
				break;

			case 607:
				itemDef.name = "2x Combat";
				break;

			case 608:
				itemDef.name = "2x Skilling";
				break;
			case 6950:
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[1] = "Cast";
				itemDef.name = "<col=ff00>Vengeance";
				break;
			// case 15330:
			// itemDef.name = "Halloween ring";
			// itemDef.modelID = 2677;
			// itemDef.modelZoom = 830;
			// itemDef.modelRotationY = 322;
			// itemDef.modelRotationX = 135;
			// itemDef.itemActions = new String[5];
			// itemDef.itemActions[1] = "Wear";
			// break;
			//
			// case 15332:
			// itemDef.name = "Ring of slaying";
			// itemDef.modelID = 2677;
			// itemDef.modelZoom = 830;
			// itemDef.modelRotationY = 322;
			// itemDef.modelRotationX = 135;
			// itemDef.modelOffsetX = -1;
			// itemDef.modelOffsetX = 1;
			// itemDef.itemActions = new String[5];
			// itemDef.itemActions[1] = "Wear";
			// itemDef.originalModelColors = new int[1];
			// itemDef.modifiedModelColors = new int[1];
			// itemDef.originalModelColors[0] = 51111;
			// itemDef.modifiedModelColors[0] = 127;
			// break;
			//
			// case 15103:
			// itemDef.itemActions = new String[5];
			// itemDef.itemActions[1] = "Wield";
			// itemDef.name = "Hammer"; // Name
			// itemDef.description = "A wieldable hammer.".getBytes();
			// itemDef.modelRotationY = 356;
			// itemDef.modelRotationX = 2012;
			// itemDef.modelOffsetX = -3;
			// itemDef.modelOffsetY = -1;
			// itemDef.modelID = 2376;
			// itemDef.anInt165 = 491;
			// itemDef.anInt200 = 491;// 21886,253,167,491
			// itemDef.modelZoom = 900;
			// break;
			case 12601:
				itemDef.name = "Ring of the gods";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				break;
			case 12603:
				itemDef.name = "Tyrannical ring";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				break;

			case 12605:
				itemDef.name = "Treasonous ring";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				break;
			case 12602:
				itemDef.name = "Ring of the gods (i)";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				break;
			case 12604:
				itemDef.name = "Tyrannical ring (i)";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				break;

			case 12606:
				itemDef.name = "Treasonous ring (i)";
				itemDef.inventoryOptions = new String[]{null, "Wear", null, null,
						"Drop"};
				break;
			case 2568:
				itemDef.inventoryOptions = new String[5];
				itemDef.inventoryOptions[2] = "Check charges";
				break;
				
			//667+ items
			case 24338:
				itemDef.inventoryOptions = new String[] { null, "Wield", "Check state", null, "Destroy" };
				itemDef.manWear = 70671;
				itemDef.womanWear = 70671;
				itemDef.name = "Royal crossbow";
				break;
		}

		return itemDef;
	}

	public static void unpackConfig() {
		stream = new Stream(Misc.readFile("./Data/cache/def/items/667obj.dat"));
		Stream stream = new Stream(Misc.readFile("./Data/cache/def/items/667obj.idx"));
		totalItems = stream.readUnsignedWord();
		streamIndices = new int[totalItems];
		System.out.println("Loaded 667 Items Amount: " + totalItems);
		int i = 2;
		for (int j = 0; j < totalItems; j++) {
			streamIndices[j] = i;
			i += stream.readUnsignedWord();
		}

		streamOsrs = new Stream(Misc.readFile("./Data/cache/def/items/osrsobj.dat"));
		stream = new Stream(Misc.readFile("./Data/cache/def/items/osrsobj.idx"));
		totalItemsOsrs = stream.readUnsignedWord();
		streamIndicesOsrs = new int[totalItemsOsrs];
		System.out.println("Loaded Osrs Items Amount: " + totalItemsOsrs);
		i = 2;
		for (int j = 0; j < totalItemsOsrs; j++) {
			streamIndicesOsrs[j] = i;
			i += stream.readUnsignedWord();
		}

		cache = new Int2ObjectOpenHashMap<>(totalItems);
		cacheOsrs = new Int2ObjectOpenHashMap<>(totalItemsOsrs);
	}

	public int cost;// anInt155

	private int certTemplateID;

	public int manWear;// maleWieldModel

	public String name;// itemName

	public boolean stackable;// itemStackable

	public int noteId; // noted items

	public String inventoryOptions[];// itemMenuOption

	public int womanWear;// femWieldModel

	public int lentItemID;

	public boolean itemIsInNotePosition = false;

	public String getName() {
		if (this.name != null) {
			return name;
		}

		return "No Name";
	}

	public int getNoteId() {
		return this.noteId;
	}

	public int getValue() {
		return this.cost;
	}

	public boolean isInNotePosition() {
		return certTemplateID != -1;
	}

	public boolean isStackable() {
		return this.stackable;
	}

	public boolean itemCanBeNoted() {
		if (this.noteId > 0 && !itemIsInNotePosition) {
			return true;
		}
		return false;
	}

	private void readValues667(Stream stream, boolean osrs) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0) {
				return;
			}
			if (i == 1) {
				stream.readUnsignedWord();
			} else if (i == 2) {
				name = stream.readString();
			} else if (i == 3) {
				stream.readBytes();
			} else if (i == 4) {
				stream.readUnsignedWord();
			} else if (i == 5) {
				stream.readUnsignedWord();
			} else if (i == 6) {
				stream.readUnsignedWord();
			} else if (i == 7) {
				stream.readUnsignedWord();
			} else if (i == 8) {
				stream.readUnsignedWord();
			} else if (i == 10) {
				stream.readUnsignedWord();
			} else if (i == 11) {
				stackable = true;
			} else if (i == 12) {
				cost = stream.readDWord();
			} else if (i == 23) {
				manWear = stream.readUnsignedWord();
				stream.readSignedByte();
			} else if (i == 24) {
				stream.readUnsignedWord();
			} else if (i == 25) {
				womanWear = stream.readUnsignedWord();
				stream.readSignedByte();
			} else if (i == 26) {
				stream.readUnsignedWord();
			} else if (i >= 30 && i < 35) {
				stream.readString();
			} else if (i >= 35 && i < 40) {
				if (inventoryOptions == null) {
					inventoryOptions = new String[5];
				}
				inventoryOptions[i - 35] = stream.readString();
			} else if (i == 40) {
				int j = stream.readUnsignedByte();
				for (int k = 0; k < j; k++) {
					stream.readUnsignedWord();
					stream.readUnsignedWord();
				}
			} else if (i == 78) {
				stream.readUnsignedWord();
			} else if (i == 79) {
				stream.readUnsignedWord();
			} else if (i == 90) {
				stream.readUnsignedWord();
			} else if (i == 91) {
				stream.readUnsignedWord();
			} else if (i == 92) {
				stream.readUnsignedWord();
			} else if (i == 93) {
				stream.readUnsignedWord();
			} else if (i == 95) {
				stream.readUnsignedWord();
			} else if (i == 97) {
				noteId = stream.readUnsignedWord();
				if (osrs) {
					noteId += OSRS_START;
				}
			} else if (i == 98) {
				certTemplateID = stream.readUnsignedWord();
				if (osrs) {
					certTemplateID += OSRS_START;
				}
			} else if (i >= 100 && i < 110) {
				stream.readUnsignedWord();
				stream.readUnsignedWord();
			} else if (i == 110) {
				stream.readUnsignedWord();
			} else if (i == 111) {
				stream.readUnsignedWord();
			} else if (i == 112) {
				stream.readUnsignedWord();
			} else if (i == 113) {
				stream.readSignedByte();
			} else if (i == 114) {
				stream.readSignedByte();
			} else if (i == 115) {
				stream.readUnsignedByte();
			} else if (i == 121) {
				stream.readUnsignedWord();
			} else if (i == 122) {
				lentItemID = stream.readUnsignedWord();
			}
		} while (true);
	}

	private void setDefaults() {
		name = "null";
		stackable = false;
		cost = 1;
		inventoryOptions = null;
		manWear = -1;
		womanWear = -1;
		noteId = -1;
		certTemplateID = -1;
		lentItemID = -1;
		itemIsInNotePosition = false;
	}

	private void toNote() {
		ItemDef itemDef_1 = forID(noteId);
		name = itemDef_1.name;
		cost = itemDef_1.cost;
		stackable = true;
		itemIsInNotePosition = true;
	}

}