package com.soulplay.cache;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Config;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

public final class EntityDef {

	public static Stream stream;
	public static Stream streamOsrs;
	public static int[] streamIndices;
	public static int[] streamIndicesOsrs;
	public static final int OSRS_OFFSET = 100_000;

	public static Map<Integer, EntityDef> cache = new HashMap<>();

	public static EntityDef forIDOsrs(int i) {
		EntityDef entityDef = cache.get(i);
		if (entityDef != null) {
			return entityDef;
		}

		entityDef = new EntityDef();

		int indexId = i - OSRS_OFFSET;
		if (indexId >= 0 && indexId < totalNPCsOsrs) {
			streamOsrs.currentOffset = streamIndicesOsrs[indexId];
			entityDef.readValues667(streamOsrs);
		}

		if (entityDef.standAnim != -1) {
			entityDef.standAnim += Config.OSRS_ANIM_OFFSET;
		}

		if (entityDef.walkAnim != -1) {
			entityDef.walkAnim += Config.OSRS_ANIM_OFFSET;
		}

		if (entityDef.turn180AnimIndex != -1) {
			entityDef.turn180AnimIndex += Config.OSRS_ANIM_OFFSET;
		}

		if (entityDef.turn90CWAnimIndex != -1) {
			entityDef.turn90CWAnimIndex += Config.OSRS_ANIM_OFFSET;
		}

		if (entityDef.turn90CCWAnimIndex != -1) {
			entityDef.turn90CCWAnimIndex += Config.OSRS_ANIM_OFFSET;
		}

		cache.put(i, entityDef);
		return entityDef;
	}

	public static EntityDef forID(int i) {
		if (i >= OSRS_OFFSET) {
			return forIDOsrs(i);
		}
		
		EntityDef entityDef = cache.get(i);

		switch (i) {
		
		 case 107559:
				entityDef.name = "Dominion archer";
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 168;
				break;
		};
		
		if (entityDef != null) {
			return entityDef;
		}

		entityDef = new EntityDef();
		if (i >= 0 && i < totalNPCs) {
			stream.currentOffset = streamIndices[i];
			entityDef.readValues667(stream);
		}

		cache.put(i, entityDef);

		switch (i) {
			
		case 494:
			  entityDef.actions = new String[] {"Talk-to", null, "Bank", "Collect", "Open presets"};
			  break;
		
		case 13986:
			  entityDef.name = "Dominion trader";
			  entityDef.actions = new String[] {"Talk-to", null, "Trade", null, null};
			  break;
			    
		 case 8441:
				entityDef.name = "Dominion mage";
				entityDef.actions = new String[]{null, "Attack", null, null, null};
				entityDef.combatLevel = 174;
				break;
			
		case 9310: //easter bunny temp
			entityDef.name = "Easter bunny boss";
			entityDef.setSize(5);
			entityDef.combatLevel = 1000;
			return entityDef;
		
			//inferno minigame npcs
			case 9011: //jal-nib
				entityDef.name = "Jal-Nib";
				entityDef.setSize(1);
				entityDef.combatLevel = 32;
				return entityDef;
			case 9020: //Jal-ImKot
				entityDef.name = "Jal-ImKot";
				entityDef.setSize(4);
				entityDef.combatLevel = 240;
				return entityDef;
			case 9021: //jal-xil
				entityDef.name = "Jal-Xil";
				entityDef.setSize(3);
				entityDef.combatLevel = 370;
				return entityDef;
			case 9030: //jal-zek
				entityDef.name = "Jal-Zek";
				entityDef.setSize(4);
				entityDef.combatLevel = 490;
				return entityDef;
			case 9031: //yt-hurkot
				entityDef.name = "Yt-Hurkot";
				entityDef.setSize(1);
				entityDef.combatLevel = 141;
				return entityDef;
			case 9040: //JalTok-Jad
				entityDef.name = "JalTok-Jad";
				entityDef.setSize(5);
				entityDef.combatLevel = 900;
				return entityDef;
			case 9041://TzKal-Zuk
				entityDef.name = "TzKal-Zuk";
				entityDef.setSize(7);
				entityDef.combatLevel = 1400;
				return entityDef;
			case 9062: //Jal-MejJak
				entityDef.name = "Jal-MejJak";
				entityDef.setSize(1);
				entityDef.combatLevel = 250;
				return entityDef;
			case 9063://Ancestral Glyph
				entityDef.name = "Ancestral Glyph";
				entityDef.setSize(3);
				return entityDef;
			case 9064: //jal-xil
				entityDef.name = "Jal-Xil";
				entityDef.setSize(3);
				entityDef.combatLevel = 370;
				return entityDef;
			case 9065: //jal-xil
				entityDef.name = "Jal-Zek";
				entityDef.setSize(4);
				entityDef.combatLevel = 490;
				return entityDef;
			case 9104:
				entityDef.name = "JalTok-Jad";
				entityDef.setSize(5);
				entityDef.combatLevel = 900;
				return entityDef;
			case 9000: // //great olm
				entityDef.name = "Great olm";
				entityDef.setSize(5);
				entityDef.combatLevel = 1043;
				return entityDef;
			case 8990: // //olm left claw
				entityDef.name = "Great olm (Left claw)";
				entityDef.setSize(5);
				entityDef.combatLevel = 750;
				return entityDef;
			case 8991: //olm right claw
				entityDef.name = "Great olm (Right claw)";
				entityDef.setSize(5);
				entityDef.combatLevel = 549;
				return entityDef;
				
			case 8729: // Enormous Tentacle
				entityDef.name = "Enormous Tentacle";
				entityDef.setSize(2);
				entityDef.combatLevel = 112;
				return entityDef;
				
			case 8732: // kraken
				entityDef.name = "Kraken";
				entityDef.setSize(5);
				entityDef.combatLevel = 291;
				return entityDef;
				
			case 5000:
				entityDef.name = "Lisa";
				return entityDef;
				
			case 8971: //sleeping vorkath
				entityDef.name = "Vorkath";
				entityDef.setSize(7);
				return entityDef;
			case 8970: // vorkath
				entityDef.name = "Vorkath";
				entityDef.setSize(7);
				entityDef.combatLevel = 732;
				entityDef.standAnim = 27948;
				return entityDef;
				
			case 8981:
				entityDef.name = "Zombified spawn";
				entityDef.setSize(1);
				return entityDef;
				
			case 6607:
			    entityDef.name = "Elder chaos druid";
			    entityDef.combatLevel = 129;
				return entityDef;
				
			case 8980: //scavenger
				entityDef.name = "Scavenger";
				entityDef.setSize(2);
				return entityDef;

		    case 9710:
				entityDef.standAnim = 13412;
				return entityDef;
				
			case 3584:
				entityDef.walkAnim = 283;
				entityDef.standAnim = 286;
				break;

			case 7150:
				entityDef.name = "Tortured gorilla";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 2;
				entityDef.walkAnim = 7233;
				entityDef.standAnim = 7230;
				return entityDef;

			case 7151:
				entityDef.name = "Demonic gorilla";
				entityDef.combatLevel = 275;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 2;
				entityDef.walkAnim = Animation.getOsrsAnimId(7233);
				entityDef.standAnim = Animation.getOsrsAnimId(7230);
				return entityDef;

			case 7152: // melee protect
				entityDef.name = "Demonic gorilla";
				entityDef.combatLevel = 275;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 2;
				entityDef.walkAnim = Animation.getOsrsAnimId(7233);
				entityDef.standAnim = Animation.getOsrsAnimId(7230);
				return entityDef;

			case 7153: // range protect
				entityDef.name = "Demonic gorilla";
				entityDef.combatLevel = 275;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 2;
				entityDef.walkAnim = Animation.getOsrsAnimId(7233);
				entityDef.standAnim = Animation.getOsrsAnimId(7230);
				return entityDef;

			case 7154: // magic protect
				entityDef.name = "Demonic gorilla";
				entityDef.combatLevel = 275;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 2;
				entityDef.walkAnim = Animation.getOsrsAnimId(7233);
				entityDef.standAnim = Animation.getOsrsAnimId(7230);
				return entityDef;

			case 498:
				entityDef.name = "Smoke devil";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.walkAnim = 1828;
				entityDef.standAnim = 1829;
				return entityDef;

			case 499:
				entityDef.name = "Thermonuclear smoke devil";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 4;
				entityDef.walkAnim = 1828;
				entityDef.standAnim = 1829;
				return entityDef;

			case 2011:
				entityDef.name = "Lava dragon";
				entityDef.combatLevel = 252;
				entityDef.actions = new String[5];
				entityDef.actions[0] = "null";
				entityDef.actions[1] = "Attack";
				entityDef.actions[2] = "null";
				entityDef.actions[3] = "null";
				entityDef.actions[4] = "null";
				entityDef.aByte68 = 5;
				entityDef.walkAnim = 4635;
				entityDef.standAnim = 90;
				break;

			case 2012:
				entityDef.name = "Zulrah";
				entityDef.combatLevel = 725;
				entityDef.actions = new String[5];
				entityDef.actions[0] = "null";
				entityDef.actions[1] = "Attack";
				entityDef.actions[2] = "null";
				entityDef.actions[3] = "null";
				entityDef.actions[4] = "null";
				entityDef.aByte68 = 5;
				entityDef.walkAnim = Animation.getOsrsAnimId(5070);
				entityDef.standAnim = Animation.getOsrsAnimId(5070);
				break;

			case 2043:
				entityDef.name = "Zulrah";
				entityDef.combatLevel = 725;
				entityDef.actions = new String[5];
				entityDef.actions[0] = "null";
				entityDef.actions[1] = "Attack";
				entityDef.actions[2] = "null";
				entityDef.actions[3] = "null";
				entityDef.actions[4] = "null";
				entityDef.aByte68 = 5;
				entityDef.walkAnim = Animation.getOsrsAnimId(5070);
				entityDef.standAnim = Animation.getOsrsAnimId(5070);
				break;

			case 2044:
				entityDef.name = "Zulrah";
				entityDef.combatLevel = 725;
				entityDef.actions = new String[5];
				entityDef.actions[0] = "null";
				entityDef.actions[1] = "Attack";
				entityDef.actions[2] = "null";
				entityDef.actions[3] = "null";
				entityDef.actions[4] = "null";
				entityDef.aByte68 = 1;
				entityDef.walkAnim = Animation.getOsrsAnimId(5070);
				entityDef.standAnim = Animation.getOsrsAnimId(5070);
				break;

			case 2045:
				entityDef.name = "Snakeling";
				entityDef.combatLevel = 90;
				entityDef.actions = new String[5];
				entityDef.actions[0] = "null";
				entityDef.actions[1] = "Attack";
				entityDef.actions[2] = "null";
				entityDef.actions[3] = "null";
				entityDef.actions[4] = "null";
				entityDef.aByte68 = 1;
				entityDef.walkAnim = Animation.getOsrsAnimId(2405);
				entityDef.standAnim = Animation.getOsrsAnimId(1721);
				break;

			case 7605:
				entityDef.name = "Julius The Shop Manager";
				entityDef.actions[2] = "Open Market";
				entityDef.actions[3] = "Open your Own Shop";
				break;

			case 8948:
				entityDef.name = "Armour set exchange";
				entityDef.actions[2] = "Exchange armour set";
				entityDef.actions[3] = "Bank";
				break;

			case 3996:
				entityDef.name = "Julius's pet";
				entityDef.aByte68 = 3;
				entityDef.standAnim = 7014;
				entityDef.walkAnim = 7015;
				entityDef.actions[0] = "Pick-up";
				break;

			case 3997:
				entityDef.name = "Baby Mole";
				entityDef.aByte68 = 1;
				entityDef.standAnim = 3309;
				entityDef.walkAnim = 3313;
				entityDef.actions[0] = "Pick-up";
				break;

			case 3998:
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.walkAnim = 6236;
				entityDef.standAnim = 6236;
				entityDef.name = "Kalphite Princess";
				entityDef.aByte68 = 1;
				break;

			case 3999:
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.walkAnim = 6238;
				entityDef.standAnim = 6239;
				entityDef.name = "Kalphite Princess";
				entityDef.aByte68 = 1;
				break;

			case 4000:
				entityDef.name = "Prince Black Dragon";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 90;
				entityDef.walkAnim = 4635;
				entityDef.aByte68 = 1;
				break;

			case 4001:
				entityDef.name = "General Graardor";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 7059;
				entityDef.walkAnim = 7058;
				break;

			case 4002:
				entityDef.name = "TzRek-Jad";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 9274;
				entityDef.walkAnim = 9273;
				break;

			case 4003:
				entityDef.name = "Chaos Elemental";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 3144;
				entityDef.walkAnim = 3145;
				break;

			case 4004:// missing item
				entityDef.name = "Corporeal Beast";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 10056;
				entityDef.walkAnim = 10055;
				entityDef.aByte68 = 1;
				break;

			case 4005:
				entityDef.name = "Kree Arra";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 6972;
				entityDef.walkAnim = 6973;
				break;

			case 4006:
				entityDef.name = "K'ril Tsutsaroth";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 6943;
				entityDef.walkAnim = 6942;
				entityDef.aByte68 = 1;
				break;

			case 4007:
				entityDef.name = "Commander Zilyana";
				entityDef.aByte68 = 1;
				entityDef.standAnim = 6963;
				entityDef.walkAnim = 6962;
				entityDef.actions[0] = "Pick-up";
				break;

			case 4008:
				entityDef.name = "Dagannoth Supreme";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 2850;
				entityDef.walkAnim = 2849;
				break;

			case 4009:
				entityDef.name = "Dagannoth Prime"; // 9940, 9943, 9942
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 2850;
				entityDef.walkAnim = 2849;
				break;

			case 4010:
				entityDef.name = "Dagannoth Rex";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 2850;
				entityDef.walkAnim = 2849;
				break;

			case 4011:
				entityDef.name = "Ahrim the Blighted";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 813;
				entityDef.walkAnim = 1205;
				break;

			case 4012:
				entityDef.name = "Dharok the Wretched";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 2065;
				entityDef.walkAnim = 2064;
				break;

			case 4013:
				entityDef.name = "Guthan the Infested";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 813;
				entityDef.walkAnim = 1205;
				break;

			case 4014:
				entityDef.name = "Karil the Tainted";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 2074;
				entityDef.walkAnim = 2076;
				break;

			case 4015:
				entityDef.name = "Torag the Corrupted";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Pick-up";
				entityDef.standAnim = 808;
				entityDef.walkAnim = 819;
				break;

			case 4016:
				entityDef.name = "Verac the Defiled";
				entityDef.actions = new String[5];
				entityDef.actions[0] = "Talk to";
				entityDef.actions[1] = "Pick-up";
				entityDef.standAnim = 2061;
				entityDef.walkAnim = 2060;
				break;

			case 4174: {
				entityDef.name = "Callisto";
				entityDef.combatLevel = 470;
				EntityDef callisto = forID(105);
				entityDef.standAnim = callisto.standAnim;
				entityDef.walkAnim = callisto.walkAnim;
				entityDef.actions = callisto.actions;

				entityDef.walkAnim = 24923;
				entityDef.standAnim = 24919;
				entityDef.aByte68 = 5;
				break;
			}

			case 4172: {
				entityDef.name = "Scorpia";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				EntityDef scor = forID(107);
				entityDef.standAnim = scor.standAnim;
				entityDef.walkAnim = scor.walkAnim;
				entityDef.combatLevel = 464;
				entityDef.aByte68 = 5;
				break;
			}

			case 4173: {
				entityDef.name = "Venenatis";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.aByte68 = 5;
				EntityDef ven = forID(60);
				entityDef.standAnim = ven.standAnim;
				entityDef.walkAnim = ven.walkAnim;
				entityDef.combatLevel = 464;
				break;
			}

			case 4175: {
				entityDef.name = "Vet'ion";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				EntityDef vet = forID(90);
				entityDef.standAnim = vet.standAnim;
				entityDef.walkAnim = vet.walkAnim;
				entityDef.combatLevel = 464;
				entityDef.aByte68 = 4;
				break;
			}
			case 4176: {
				entityDef.name = "Vet'ion";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				EntityDef vet = forID(90);
				entityDef.standAnim = vet.standAnim;
				entityDef.walkAnim = vet.walkAnim;
				entityDef.combatLevel = 464;
				entityDef.aByte68 = 4;
				break;
			}
			case 4177: {
				entityDef.name = "Vet'ion jr";
				entityDef.actions = new String[]{"Talk-to", null, "Pick-up", null, null};
				EntityDef vet = forID(90);
				entityDef.standAnim = vet.standAnim;
				entityDef.walkAnim = vet.walkAnim;
				entityDef.combatLevel = 0;
				entityDef.aByte68 = 1;
				break;
			}
			case 4178: {
				entityDef.name = "Vet'ion jr";
				entityDef.actions = new String[]{"Talk-to", null, "Pick-up", null, null};
				EntityDef vet = forID(90);
				entityDef.standAnim = vet.standAnim;
				entityDef.walkAnim = vet.walkAnim;
				entityDef.combatLevel = 0;
				entityDef.aByte68 = 1;
				break;
			}
			
			case 4179: {
				entityDef.name = "Scorpia's offspring";
				EntityDef scor = forID(107);
				entityDef.standAnim = scor.standAnim;
				entityDef.walkAnim = scor.walkAnim;
				entityDef.combatLevel = 0;
				entityDef.aByte68 = 2;
				break;
			}

			case 4180: {
				entityDef.name = "Venenatis pet";
				entityDef.aByte68 = 3;
				EntityDef ven = forID(60);
				entityDef.standAnim = ven.standAnim;
				entityDef.walkAnim = ven.walkAnim;
				entityDef.combatLevel = 0;
				break;
			}
			
			case 4181: {
				entityDef.name = "Callisto cub";
				EntityDef callisto = forID(105);
				entityDef.standAnim = callisto.standAnim;
				entityDef.walkAnim = callisto.walkAnim;
				entityDef.actions = callisto.actions;
				entityDef.walkAnim = 24923;
				entityDef.standAnim = 24919;
				entityDef.aByte68 = 5;
				break;
			}
			case 4182: {
				entityDef.name = "Pet Kraken";
				EntityDef kra = forID(8);
				entityDef.combatLevel = 0;
				entityDef.standAnim = 3989;
				entityDef.walkAnim = kra.walkAnim;
				break;
			}

			case 4186: {
				entityDef.name = "Crazy Archaeologist";
				entityDef.combatLevel = 204;
				entityDef.aByte68 = 1;
				break;
			}
			
			case 4187: {
				entityDef.name = "Boulder";
				entityDef.walkAnim = 26120;
				entityDef.setSize(2);
				break;
			}
			
			case 4188: {
				entityDef.name = "Ent";
				entityDef.combatLevel = 101;
				entityDef.actions = new String[] { null, "Attack", null, null, null};
				entityDef.aByte68 = 2;
				entityDef.walkAnim = 27142;
				entityDef.standAnim = 27143;
				break;
			}
			
			case 4189: {
				entityDef.name = "Ent trunk";
				entityDef.combatLevel = 0;
				entityDef.actions = new String[] { "Chop", null, null, null, null};
				entityDef.aByte68 = 2;
				break;
			}
			
			case 4190: {
				entityDef.name = "Chaos fanatic";
				entityDef.combatLevel = 202;
				break;
			}

			case 3845:
				entityDef.name = "Cave kraken";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				EntityDef cave = forID(3848);
				entityDef.combatLevel = 127;
				entityDef.standAnim = 3989;
				entityDef.walkAnim = cave.walkAnim;
				break;

			case 3846: {
				entityDef.name = "Kraken";
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				EntityDef kra = forID(8);
				entityDef.combatLevel = 291;
				entityDef.standAnim = 3989;
				entityDef.walkAnim = kra.walkAnim;
				break;
			}

			case 820:
				entityDef.name = "Spin Ticket Salesman";
				break;

			case 3374:
				entityDef.name = "Max";
				entityDef.combatLevel = -1;
				break;

			case 303:
				entityDef.name = "Vote Store";
				break;

			case 644:
				entityDef.name = "Donate Store";
				break;

			case 6090:
				entityDef.actions = new String[5];
				entityDef.actions[1] = "Attack";
				entityDef.standAnim = 12790;
				entityDef.walkAnim = 12790;
				entityDef.aByte68 = 5;
				entityDef.name = "Wildywyrm";
				entityDef.combatLevel = 755;
				break;

			case 6089:
				entityDef.actions = new String[5];
				entityDef.actions[1] = "Attack";
				entityDef.standAnim = 12790;
				entityDef.walkAnim = 12790;
				entityDef.aByte68 = 5;
				entityDef.name = "Baby Wildywyrm";
				entityDef.combatLevel = 112;
				break;

			case 5529: // yaks
			case 6873:
			case 6874:
			case 9491:
			case 9603:
			case 9604:
			case 9605:
			case 9606:
				entityDef.walkAnim = 543;
				break;
			case 181:
				entityDef.walkAnim = 819;
				break;
			case 1382:
				entityDef.name = "Glacor";
				entityDef.aByte68 = 3;
				entityDef.standAnim = 10867;
				entityDef.walkAnim = 10901;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.combatLevel = 475;
				break;

			case 1383:
				entityDef.name = "Unstable glacyte";
				entityDef.standAnim = 10867;
				entityDef.walkAnim = 10901;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.combatLevel = 101;
				break;
			case 1384:
				entityDef.name = "Sapping glacyte";
				entityDef.standAnim = 10867;
				entityDef.walkAnim = 10901;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.combatLevel = 101;
				break;
			case 1385:
				entityDef.name = "Enduring glacyte";
				entityDef.standAnim = 10867;
				entityDef.walkAnim = 10901;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.combatLevel = 101;
				break;
			case 945:
				entityDef.name = Config.SERVER_NAME + " Guide";
				break;

			case 608:
				entityDef.name = "Sir Amik Varze";
				entityDef.actions[2] = "Exchange Scrolls";
				break;

			/*
			 * case 6203: entityDef.name = "K'ril Tsutsaroth";
			 * //entityDef.anIntArray94 = new int[]{53192, 53194, 53197};
			 * entityDef.aByte68 = 5; entityDef.standAnim = 14967;
			 * entityDef.walkAnim = 14966; entityDef.actions = new
			 * String[]{null, "Attack", null, null, null}; entityDef.combatLevel
			 * = 650; entityDef.anInt91 = 100; entityDef.anInt86 = 100; break;
			 */

			case 6222:
				// entityDef.name = "Kree'arra";
				entityDef.aByte68 = 5;
				// entityDef.standAnim = 6972;
				// entityDef.walkAnim = 6973;
				// entityDef.actions = new String[] {null, "Attack", null, null,
				// null};
				break;

			case 6247:
				entityDef.name = "Commander Zilyana";
				entityDef.aByte68 = 2;
				entityDef.standAnim = 6963;
				entityDef.walkAnim = 6962;
				entityDef.actions = new String[]{null, "Attack", null, null,
						null};
				entityDef.combatLevel = 596;
				break;

		}
		return entityDef;

	}

	public static int totalNPCs;
	public static int totalNPCsOsrs;

	public static void unpackConfig() {
		stream = new Stream(Misc.readFile("./Data/cache/def/npcs/667npc.dat"));
		Stream stream2 = new Stream(Misc.readFile("./Data/cache/def/npcs/667npc.idx"));
		totalNPCs = stream2.readUnsignedWord();
		streamIndices = new int[totalNPCs];
		System.out.println("Loading 667 npcs. Amount: " + totalNPCs);
		int i = 2;
		for (int j = 0; j < totalNPCs; j++) {
			streamIndices[j] = i;
			i += stream2.readUnsignedWord();
		}

		streamOsrs = new Stream(Misc.readFile("./Data/cache/def/npcs/npcosrs.dat"));
		stream2 = new Stream(Misc.readFile("./Data/cache/def/npcs/npcosrs.idx"));
		totalNPCsOsrs = stream2.readUnsignedWord();
		streamIndicesOsrs = new int[totalNPCsOsrs];
		Config.MAX_NPC_ID = totalNPCsOsrs + 100_000;
		System.out.println("Loading Osrs npcs. Amount: " + totalNPCsOsrs);
		i = 2;
		for (int j = 0; j < totalNPCsOsrs; j++) {
			streamIndicesOsrs[j] = i;
			i += stream2.readUnsignedWord();
		}
	}

	public int turn90CCWAnimIndex; // turnLeftAnimationId

	public int turn180AnimIndex; // turnAroundAnimationId

	public int combatLevel;

	public String name;

	public String actions[];

	public int walkAnim;

	public byte aByte68; // boundaryDimension

	public int standAnim;

	public int turn90CWAnimIndex; // turnRightAnimationId

	public EntityDef() {
		turn90CCWAnimIndex = -1;
		turn180AnimIndex = -1;
		combatLevel = -1;
		walkAnim = -1;
		aByte68 = 1;
		standAnim = -1;
		turn90CWAnimIndex = -1;
	}

	public int getCombatLevel() {
		return combatLevel;
	}

	public String getName() {
		if (name != null) {
			return name;
		}
		return "No Name";
	}

	public int getSize() {
		return aByte68;
	}
	
	public void setSize(int size) {
		this.aByte68 = (byte) size;
	}

	public boolean hasAttackOption() {
		if (actions != null) {
			for (int i = 0, length = actions.length; i < length; i++) {
				String option = actions[i];
				if (option != null && option.equalsIgnoreCase("attack")) {
					return true;
				}
			}
		}

		return false;
	}

	private void readValues667(Stream stream) {
		do {
			int i = stream.readUnsignedByte();
			if (i == 0) {
				return;
			}
			if (i == 1) {
				int j = stream.readUnsignedByte();
				for (int j1 = 0; j1 < j; j1++) {
					stream.readUnsignedWord();
				}
			} else if (i == 2) {
				name = stream.readString();
			} else if (i == 3) {
				stream.readBytes();
			} else if (i == 12) {
				aByte68 = stream.readSignedByte();
			} else if (i == 13) {
				standAnim = stream.readUnsignedWord();
				if (standAnim == 65535) {
					standAnim = -1;
				}
			} else if (i == 14) {
				walkAnim = stream.readUnsignedWord();
				if (walkAnim == 65535) {
					walkAnim = -1;
				}
			} else if (i == 17) {
				walkAnim = stream.readUnsignedWord();
				if (walkAnim == 65535) {
					walkAnim = -1;
				}
				turn180AnimIndex = stream.readUnsignedWord();
				if (turn180AnimIndex == 65535) {
					turn180AnimIndex = -1;
				}
				turn90CWAnimIndex = stream.readUnsignedWord();
				if (turn90CWAnimIndex == 65535) {
					turn90CWAnimIndex = -1;
				}
				turn90CCWAnimIndex = stream.readUnsignedWord();
				if (turn90CCWAnimIndex == 65535) {
					turn90CCWAnimIndex = -1;
				}
			} else if (i >= 30 && i < 35) {
				if (actions == null) {
					actions = new String[5];
				}
				actions[i - 30] = stream.readString();
				if (actions[i - 30].equalsIgnoreCase("hidden")) {
					actions[i - 30] = null;
				}
			} else if (i == 40) {
				int k = stream.readUnsignedByte();
				for (int k1 = 0; k1 < k; k1++) {
					stream.readUnsignedWord();
					stream.readUnsignedWord();
				}

			} else if (i == 60) {
				int l = stream.readUnsignedByte();
				for (int l1 = 0; l1 < l; l1++) {
					stream.readUnsignedWord();
				}
			} else if (i == 90) {
				stream.readUnsignedWord();
			} else if (i == 91) {
				stream.readUnsignedWord();
			} else if (i == 92) {
				stream.readUnsignedWord();
			} else if (i == 95) {
				combatLevel = stream.readUnsignedWord();
			} else if (i == 97) {
				stream.readUnsignedWord();
			} else if (i == 98) {
				stream.readUnsignedWord();
			} else if (i == 100) {
				stream.readSignedByte();
			} else if (i == 101) {
				stream.readSignedByte();
			} else if (i == 102) {
				stream.readUnsignedWord();
			} else if (i == 103) {
				stream.readUnsignedWord();
			} else if (i == 106) {
				stream.readUnsignedWord();
				stream.readUnsignedWord();
				int i1 = stream.readUnsignedByte();
				for (int i2 = 0; i2 <= i1; i2++) {
					stream.readUnsignedWord();
				}
			}
		} while (true);
	}

}
