package com.soulplay.cache;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public final class ObjectDef {

	public static int objectsSize667;

	private static Stream stream667;

	public static int[] streamIndices667;

	private static Stream stream474;

	private static int[] streamIndices;

	private static Int2ObjectMap<ObjectDef> objDefs667;
	private static Int2ObjectMap<ObjectDef> objDefs474;
	private static Int2ObjectMap<ObjectDef> objDefs525;
	private static Int2ObjectMap<ObjectDef> objDefsOsrs;

	public static int objectSize474() {
		return objDefs474.size();
	}

	public static ObjectDef forID667(int i) {
		ObjectDef defs = objDefs667.get(i);
		if (defs != null)
			return defs;
		
		switch (i) { // rewriting to osrs object
		case 31555:
		case 31556:
			defs = forIDOSRS(i);
			defs.type = i;
			objDefs667.put(i, defs);
			return defs;
		}
		
		defs = new ObjectDef();
		stream667.currentOffset = streamIndices667[i];
		defs.type = i;
		defs.setDefaults();
		try {
			defs.readValues667(stream667);
			switch (i) {
				case 31436:
				case 54531:
				case 54529:
				case 54527:
				case 54525:
				case 54510:
				case 54508:
				case 54506:
				case 54504:
					defs.aBoolean767 = false;
					break;
				//Fix fires clipping
				case 26185:
				case 26186:
				case 26575:
				case 26576:
				case 20001:
				case 20000:
					defs.aBoolean767 = false;
					defs.aBoolean757 = false;
					defs.clipType = 0;
					break;
			}
			objDefs667.put(i, defs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defs;
	}

	public static ObjectDef forID525(int i) {
		ObjectDef defs = objDefs525.get(i);
		if (defs != null)
			return defs;
		
		return forID667(i);
	}
	
	public static ObjectDef forIDOSRS(int i) {
		int indexId;
		if (i >= ObjectDef.OSRS_START) {
			indexId = i - ObjectDef.OSRS_START;
		} else {
			indexId = i;
		}

		ObjectDef defs = objDefsOsrs.get(indexId);
		if (defs != null)
			return defs;
		
		return forID667(i);
	}

	public static byte[] getBuffer(String s) {
		try {
			java.io.File f = new java.io.File("./Data/cache/def/objects/" + s);
			if (!f.exists()) {
				return null;
			}
			byte[] buffer = new byte[(int) f.length()];
			java.io.DataInputStream dis = new java.io.DataInputStream(
					new java.io.FileInputStream(f));
			dis.readFully(buffer);
			dis.close();
			return buffer;
		} catch (Exception e) {
		}
		return null;
	}
	
	public static ObjectDef getObjectDef(int i) {
		if (i >= OSRS_START) {
			return forIDOSRS(i - OSRS_START);
		}

		if (i >= streamIndices.length) {
			return forID667(i);
		}
		
		ObjectDef defs = objDefs474.get(i);
		if (defs != null)
			return defs;
		
		defs = new ObjectDef();
		
		stream474.currentOffset = streamIndices[i];
		defs.type = i;
		defs.setDefaults();
		defs.readValues474(stream474);
		objDefs474.put(i, defs);

		try {
			switch (i) {
				case 30808:
					defs.itemActions[0] = "Mine";
					defs.itemActions[1] = "1Prospect";
					defs.itemActions[2] = "2Prospect";
					defs.itemActions[3] = "3Prospect";
					defs.itemActions[4] = "4Prospect";
					defs.itemActions[5] = "5Prospect";
					defs.hasActions = true;
					break;
				case 15477:
					defs.itemActions[0] = "Enter";
					defs.itemActions[1] = "Home";
					defs.itemActions[2] = "Build mode";
					defs.itemActions[3] = "Friend's house";
					defs.itemActions[4] = "Popular houses";
					break;
				case 1503:
				case 26392:
				case 6788:
				case 8935:
				case 1460:
				case 9383:
				case 1434:
					defs.hasActions = false;
					break;
				case 5842:
					defs.name = "Concentrated Coal Deposits";
					defs.itemActions[0] = "Mine";
					defs.itemActions[2] = "Prospect";
					defs.hasActions = true;
					break;
				case 4437:
					defs.name = "Concentrated Gold Deposits";
					defs.itemActions[0] = "Mine";
					defs.itemActions[2] = "Prospect";
					defs.hasActions = true;
					break;
				case 13291:
					defs.name = "Magical Chest";
					defs.itemActions[0] = "Loot";
					defs.hasActions = true;
					break;
				case 13615:
					defs.name = "Home portal";
					defs.itemActions[0] = "Enter";
					defs.hasActions = true;
					break;
				case 13616:
					defs.name = "Shops portal";
					defs.itemActions[0] = "Enter";
					defs.hasActions = true;
					break;
				case 2565:
					defs.name = "Money stall";
					defs.hasActions = true;
					break;
				case 11699:
					defs.name=null;
					defs.sizeX=1;
					defs.type=11699;
					defs.aBoolean757=false;
					defs.sizeY=1;
					defs.aBoolean766=false;
					defs.aBoolean767=true;
					defs.anInt768=0;
					defs.hasActions=false;
					defs.itemActions=null;
					break;
				case 11698:
					defs.name=null;
					defs.sizeX=1;
					defs.type=11698;
					defs.aBoolean757=true;
					defs.sizeY=1;
					defs.aBoolean766=false;
					defs.aBoolean767=true;
					defs.anInt768=0;
					defs.hasActions=false;
					defs.itemActions=null;
					break;
				case 10658:
					defs.name = "popo";
					break;
				//Fix fires clipping
				case 26185:
				case 26186:
				case 26575:
				case 26576:
				case 20001:
				case 20000:
					defs.aBoolean767 = false;
					defs.aBoolean757 = false;
					defs.clipType = 0;
					break;
			}
		} catch (Exception e) {
			System.out.println("Fatal error at objectDef.java");
		}

		return defs;
	}

	public static void unpackConfig() {
		stream474 = new Stream(getBuffer("474loc.dat"));
		Stream stream474Idx = new Stream(getBuffer("474loc.idx"));

		stream667 = new Stream(getBuffer("667loc.dat"));
		Stream stream667 = new Stream(getBuffer("667loc.idx"));

		int totalObjects = stream474Idx.readUnsignedWord();
		int totalObjects667 = stream667.readUnsignedWord();
		objectsSize667 = totalObjects667;
		System.out.println("Loaded 474 objects. Amount: " + totalObjects);
		System.out.println("Loaded 667 objects. Amount: " + totalObjects667);
		streamIndices = new int[totalObjects];
		int i = 2;
		for (int j = 0; j < totalObjects; j++) {
			streamIndices[j] = i;
			i += stream474Idx.readUnsignedWord();
		}

		streamIndices667 = new int[totalObjects667];
		i = 2;
		for (int j = 0; j < totalObjects667; j++) {
			streamIndices667[j] = i;
			i += stream667.readUnsignedWord();
		}

		objDefs667 = new Int2ObjectOpenHashMap<>(objectsSize667);
		objDefs474 = new Int2ObjectOpenHashMap<>(totalObjects);
		
		Stream stream = new Stream(getBuffer("525loc.dat"));
		Stream streamIdx = new Stream(getBuffer("525loc.idx"));
		int size = streamIdx.readUnsignedWord();
		
		objDefs525 = new Int2ObjectOpenHashMap<>(size);
		System.out.println("Loaded 525 obj  count:"+size);
		
		i = 2;
		for (int j = 0; j < size; j++) {
			
			ObjectDef defs = new ObjectDef();
			stream.currentOffset = i;
			defs.type = j;
			defs.setDefaults();
			defs.readValues667(stream);
			if (i == 31436) {
				defs.aBoolean767 = false;
			}
			objDefs525.put(j, defs);
			
			i += streamIdx.readUnsignedWord();
		}
		
		stream = new Stream(getBuffer("osrsloc.dat"));
		streamIdx = new Stream(getBuffer("osrsloc.idx"));
		size = streamIdx.readUnsignedWord();
		
		objDefsOsrs = new Int2ObjectOpenHashMap<>(size);
		System.out.println("Loaded Osrs Objects count:"+size);
		
		i = 2;
		for (int j = 0; j < size; j++) {
			
			ObjectDef defs = new ObjectDef();
			stream.currentOffset = i;
			defs.type = j;
			defs.setDefaults();
			try {
				defs.readValues667(stream);
			} catch (Exception e) {
				/* empty */
			}

			objDefsOsrs.put(j, defs);

			i += streamIdx.readUnsignedWord();
			
			customOSRSEDITS(defs);
		}
		
	}
	
	public static void customOSRSEDITS(ObjectDef defs) {
		switch (defs.type) {
		case 32738:
		case 34515:
			defs.reachDistance = 2;
			break;
		case 6557://dmm tournament fence, do not shoot over metal fence
			defs.aBoolean757 = true;
			break;
		case 30283://Inferno exit
			defs.sizeX = 5;
			defs.sizeY = 4;
			break;
		case 30352://Inferno hole
			defs.sizeX = 6;
			defs.sizeY = 6;
			defs.reachDistance = 6;
			break;
		case 31561:
			defs.reachDistance = 2;
			break;
		case 29102:
			defs.reachDistance = 6;
			break;
		case 32297:
			defs.aBoolean767 = true;
			break;
		case 23542: // log walk in wild agility course
			// makes player automatically walk up to the front of the log
			defs.anInt768 = 11;
			break;
		}
	}

	public String name;

	public int sizeX;

	public int type;

	public boolean aBoolean757;

	public int sizeY;

	private boolean aBoolean766;

	public boolean aBoolean767;

	public int anInt768;

	public boolean hasActions;

	private int reachDistance;
	
	private boolean moveNear;

	public String[] itemActions;
	
	private int clipType;

	private ObjectDef() {
		type = -1;
	}

	public boolean requiresClipping() {
		return aBoolean767;
	}

	public boolean clippedProjectile() {
		return aBoolean757;
	}
	
	public boolean alternativePathBlocked() {
		return aBoolean766;
	}
	
	public int getWalkingFlag() {
		return anInt768;
	}
	
	public int getClipType() {
		return clipType;
	}

	public String getName() {
		if (name != null) {
			return name;
		}
		return "";
	}

	public boolean hasActions() {
		return hasActions;
	}

	public boolean hasName() {
		return name != null && name.length() > 1;
	}
	
	public boolean isMoveNear() {
		return moveNear;
	}

	public void readValues474(Stream byteBuffer) {
		int i = -1;
		int[] anIntArray773 = null;
		int[] anIntArray776 = null;
		label0 : do {
			int j;
			do {
				j = byteBuffer.readUnsignedByte();
				if (j == 0) {
					break label0;
				}
				if (j == 1) {
					int k = byteBuffer.readUnsignedByte();
					if (k > 0) {
						if (anIntArray773 == null) {
							anIntArray776 = new int[k];
							anIntArray773 = new int[k];
							for (int k1 = 0; k1 < k; k1++) {
								anIntArray773[k1] = byteBuffer
										.readUnsignedWord();
								anIntArray776[k1] = byteBuffer
										.readUnsignedByte();
							}

						} else {
							byteBuffer.currentOffset += k * 3;
						}
					}
				} else if (j == 2) {
					name = byteBuffer.readString();
				} else if (j == 3) {
					byteBuffer.readBytes();
				} else if (j == 5) {
					int l = byteBuffer.readUnsignedByte();
					if (l > 0) {
						if (anIntArray773 == null) {
							anIntArray776 = null;
							anIntArray773 = new int[l];
							for (int l1 = 0; l1 < l; l1++) {
								anIntArray773[l1] = byteBuffer
										.readUnsignedWord();
							}

						} else {
							byteBuffer.currentOffset += l * 2;
						}
					}
				} else if (j == 14) {
					sizeX = byteBuffer.readUnsignedByte();
				} else if (j == 15) {
					sizeY = byteBuffer.readUnsignedByte();
				} else if (j == 17) {
					aBoolean767 = false;
					aBoolean757 = false;
					clipType = 0;
				} else if (j == 18) {
					aBoolean757 = false;
				} else if (j == 19) {
					i = byteBuffer.readUnsignedByte();
					if (i == 1) {
						hasActions = true;
					}
				} else if (j == 24) {
					byteBuffer.readUnsignedWord();
				} else if (j == 27) {
					clipType = 1;
				} else if (j == 28) {
					byteBuffer.readUnsignedByte();
				} else if (j == 29) {
					byteBuffer.readSignedByte();
				} else if (j == 39) {
					byteBuffer.readSignedByte();
				} else if (j >= 30 && j < 35) {
					if (itemActions == null) {
						itemActions = new String[5];
					}

					int index = j - 30;
					itemActions[index] = byteBuffer.readString();
					if (itemActions[index].equalsIgnoreCase("hidden")) {
						itemActions[index] = null;
					}
				} else if (j == 40) {
					int i1 = byteBuffer.readUnsignedByte();
					for (int i2 = 0; i2 < i1; i2++) {
						byteBuffer.readUnsignedWord();
						byteBuffer.readUnsignedWord();
					}
				} else if (j == 60) {
					byteBuffer.readUnsignedWord();
				} else if (j == 65) {
					byteBuffer.readUnsignedWord();
				} else if (j == 66) {
					byteBuffer.readUnsignedWord();
				} else if (j == 67) {
					byteBuffer.readUnsignedWord();
				} else if (j == 68) {
					byteBuffer.readUnsignedWord();
				} else if (j == 69) {
					anInt768 = byteBuffer.readUnsignedByte();
				} else if (j == 70) {
					byteBuffer.readSignedWord();
				} else if (j == 71) {
					byteBuffer.readSignedWord();
				} else if (j == 72) {
					byteBuffer.readSignedWord();
				} else if (j == 74) {
					aBoolean766 = true;
				} else {
					if (j != 75) {
						continue;
					}
					byteBuffer.readUnsignedByte();
				}
				continue label0;
			} while (j != 77);
			byteBuffer.readUnsignedWord();
			byteBuffer.readUnsignedWord();
			int j1 = byteBuffer.readUnsignedByte();
			for (int j2 = 0; j2 <= j1; j2++) {
				byteBuffer.readUnsignedWord();
			}
		} while (true);
		// if (i == -1) {
		if (i == -1 && name != "null" && name != null) {
			hasActions = anIntArray773 != null
					&& (anIntArray776 == null || anIntArray776[0] == 10);
			if (itemActions != null) {
				hasActions = true;
			}
		}
		if (aBoolean766) {
			aBoolean767 = false;
			aBoolean757 = false;
		}
	}

	private void readValues667(Stream buffer) {
		int i = -1;
		int[] anIntArray773 = null;
		int[] anIntArray776 = null;
		label0 : do {
			int opcode;
			do {
				opcode = buffer.readUnsignedByte();
				if (opcode == 0) {
					break label0;
				}
				if (opcode == 1) {
					int k = buffer.readUnsignedByte();
					if (k > 0) {
						if (anIntArray773 == null) {
							anIntArray776 = new int[k];
							anIntArray773 = new int[k];
							for (int k1 = 0; k1 < k; k1++) {
								anIntArray773[k1] = buffer.readUnsignedWord();
								anIntArray776[k1] = buffer.readUnsignedByte();
							}
						} else {
							buffer.currentOffset += k * 3;
						}
					}
				} else if (opcode == 2) {
					name = buffer.readString();
				} else if (opcode == 3) {
					buffer.readBytes();
				} else if (opcode == 5) {
					int l = buffer.readUnsignedByte();
					if (l > 0) {
						if (anIntArray773 == null) {
							anIntArray776 = null;
							anIntArray773 = new int[l];
							for (int l1 = 0; l1 < l; l1++) {
								anIntArray773[l1] = buffer.readUnsignedWord();
							}
						} else {
							;// buffer.currentOffset += l * 2;
						}
					}
				} else if (opcode == 14) {
					sizeX = buffer.readUnsignedByte();
				} else if (opcode == 15) {
					sizeY = buffer.readUnsignedByte();
				} else if (opcode == 17) {
					aBoolean767 = false;
					aBoolean757 = false;
					clipType = 0;
				} else if (opcode == 18) {
					aBoolean757 = false;
				} else if (opcode == 19) {
					i = buffer.readUnsignedByte();
					if (i == 1) {
						hasActions = true;
					}
				} else if (opcode == 24) {
					buffer.readUnsignedWord();
				} else if (opcode == 27) {
					clipType = 1;
				} else if (opcode == 28) {
					buffer.readUnsignedByte();
				} else if (opcode == 29) {
					buffer.readSignedByte();
				} else if (opcode == 39) {
					buffer.readSignedByte();
				} else if (opcode >= 30 && opcode < 35) {
					if (itemActions == null) {
						itemActions = new String[5];
					}

					int index = opcode - 30;
					itemActions[index] = buffer.readString();
					if (itemActions[index].equalsIgnoreCase("hidden")) {
						itemActions[index] = null;
					}
				} else if (opcode == 40) {
					int i1 = buffer.readUnsignedByte();
					for (int i2 = 0; i2 < i1; i2++) {
						buffer.readUnsignedWord();
						buffer.readUnsignedWord();
					}
				} else if (opcode == 60) {
					buffer.readUnsignedWord();
				} else if (opcode == 65) {
					buffer.readUnsignedWord();
				} else if (opcode == 66) {
					buffer.readUnsignedWord();
				} else if (opcode == 67) {
					buffer.readUnsignedWord();
				} else if (opcode == 68) {
					buffer.readUnsignedWord();
				} else if (opcode == 69) {
					anInt768 = buffer.readUnsignedByte();
				} else if (opcode == 70) {
					buffer.readSignedWord();
				} else if (opcode == 71) {
					buffer.readSignedWord();
				} else if (opcode == 72) {
					buffer.readSignedWord();
				} else if (opcode == 74) {
					aBoolean766 = true;
				} else {
					if (opcode != 75) {
						continue;
					}
					buffer.readUnsignedByte();
				}
				continue label0;
			} while (opcode != 77);
			buffer.readUnsignedWord();
			buffer.readUnsignedWord();
			int j1 = buffer.readUnsignedByte();
			for (int j2 = 0; j2 <= j1; j2++) {
				buffer.readUnsignedWord();
			}

		} while (true);
		if (i == -1) {
			hasActions = anIntArray773 != null
					&& (anIntArray776 == null || anIntArray776[0] == 10);
			if (itemActions != null) {
				hasActions = true;
			}
		}
//		if (aBoolean766) {
//			aBoolean767 = false;
//			aBoolean757 = false;
//		}
	}

	private void setDefaults() {
		name = null;
		sizeX = 1;
		sizeY = 1;
		aBoolean767 = true;
		aBoolean757 = true;
		hasActions = false;
		itemActions = null;
		anInt768 = 0;
		aBoolean766 = false;
		clipType = 2;
	}

    public boolean containsOption(int i, String option) {
    	if (itemActions == null || itemActions[i] == null || itemActions.length <= i) {
    		return false;
    	}

    	return itemActions[i].equals(option);
    }

	public int xLength() {
		return sizeX;
	}

	public int xLength(int direction) {
		if (direction != 1 && direction != 3) {
			return this.xLength();
		} else {
			return this.yLength();
		}
	}

	public int yLength() {
		return sizeY;
	}

	public int yLength(int direction) {
		if (direction != 1 && direction != 3) {
			return this.yLength();
		} else {
			return this.xLength();
		}
	}

	public int getReachDistance() {
		return reachDistance;
	}

	public void setReachDistance(int reachDistance) {
		this.reachDistance = reachDistance;
	}

	public static final int OSRS_START = 100_000;
}