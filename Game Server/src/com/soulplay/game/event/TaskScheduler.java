package com.soulplay.game.event;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSave;
import com.soulplay.net.slack.SlackMessageBuilder;

/**
 * A class which schedules the execution of {@link Task}s.
 *
 * @author Graham
 */
public final class TaskScheduler implements Runnable {

	/**
	 * A logger used to report error messages.
	 */
	private static final Logger logger = Logger
			.getLogger(TaskScheduler.class.getName());

	/**
	 * The time period, in milliseconds, of a single cycle.
	 */
	private static final int TIME_PERIOD = 600;

	/**
	 * The {@link ScheduledExecutorService} which schedules calls to the
	 * {@link #run()} method.
	 */
	private final ScheduledExecutorService service = Executors
			.newSingleThreadScheduledExecutor();

	/**
	 * A list of active tasks.
	 */
	private final List<Task> tasks = new ArrayList<>();

	/**
	 * A queue of tasks that still need to be added.
	 */
	private final Queue<Task> newTasks = new ArrayDeque<>();
	
	private long totalTicks;

	/**
	 * Creates and starts the task scheduler.
	 */
	public TaskScheduler() {
		service.scheduleAtFixedRate(this, 0, TIME_PERIOD,
				TimeUnit.MILLISECONDS);
	}

	public int getNewTasksCount() {
		return newTasks.size();
	}

	public int getTasksCount() {
		return tasks.size();
	}

	/**
	 * This method is automatically called every cycle by the
	 * {@link ScheduledExecutorService} and executes, adds and removes
	 * {@link Task}s. It should not be called directly as this will lead to
	 * concurrency issues and inaccurate time-keeping.
	 */
	@Override
	public void run() {
		try {
		synchronized (newTasks) {
			Task task;
			while ((task = newTasks.poll()) != null) {
				tasks.add(task);
			}
		}

		for (Iterator<Task> it = tasks.iterator(); it.hasNext();) {
			Task task = it.next();
			try {
				if (!task.tick()) {
					it.remove();
				}
			} catch (Throwable t) {

				PlayerHandler.setKickAllPlayers(true);
				for (Player p : PlayerHandler.players) {
					if (p == null) {
						continue;
					}
					p.skipThreadCheck = true;
					Client c = (Client) p;
					c.handleDCLogoutBeforeSave();
					PlayerSave.saveGame(c, true);
					c.destruct();
				}
				Server.saveLists();

				if (!Config.SERVER_DEBUG && Config.RUN_ON_DEDI) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(this.getClass(), t));
				}
				logger.log(Level.SEVERE, "Exception during task execution.", t);
			}
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		totalTicks++;
	}

	public void schedule(int delay, Runnable runnable) {
		schedule(new LambdaTick(delay, runnable));
	}

	public void schedule(Runnable runnable) {
		schedule(1, runnable);
	}

	/**
	 * Schedules the specified task. If this scheduler has been stopped with the
	 * {@link #terminate()} method the task will not be executed or
	 * garbage-collected.
	 *
	 * @param task
	 *            The task to schedule.
	 */
	public void schedule(final Task task) {
		// System.out.println("immediate:"+task.isImmediate());
		try {
			if (task.isImmediate()) {
				service.execute(() -> task.execute());
			}
	
			synchronized (newTasks) {
				newTasks.add(task);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Stops the task scheduler.
	 */
	public void terminate() {
		service.shutdown();
	}
	
	public boolean runningOnMainTick() {
		return totalTicks % 2 == 0;
	}

}
