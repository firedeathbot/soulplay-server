package com.soulplay.game.event;

public class LambdaTick extends Tick {

	private final Runnable whenDone;

	public LambdaTick(int delay, Runnable whenDone) {
		super(delay);
		this.whenDone = whenDone;
	}

	public LambdaTick(Runnable whenDone) {
		this(1, whenDone);
	}

	@Override
	protected void done() {
		whenDone.run();
	}

}
