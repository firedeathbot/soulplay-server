package com.soulplay.game.event;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;

/**
 * Handles all of our cycle based events
 *
 * @author Stuart <RogueX>
 * @author Null++ Revised by Shawn
 */
public class CycleEventHandler {

	/**
	 * The instance of this class.
	 */
	private static CycleEventHandler instance;

	/**
	 * Returns the instance of this class.
	 *
	 * @return
	 */
	public static CycleEventHandler getSingleton() {
		if (instance == null) {
			instance = new CycleEventHandler();
		}
		return instance;
	}

	/**
	 * Holds all of our events currently being ran.
	 */
	private List<CycleEventContainer> events;

	/**
	 * Creates a new instance of this class.
	 */
	public CycleEventHandler() {
		this.events = new ArrayList<>();
	}

	/**
	 * Add an event to the list
	 *
	 * @param id
	 * @param owner
	 * @param event
	 * @param cycles
	 */
	public void addEvent(int id, Object owner, CycleEvent event, int cycles) {
		Server.ensureOnMainThread();
		this.events.add(new CycleEventContainer(id, owner, event, cycles));
	}

	/**
	 * used for pvp combat processing
	 *
	 * @param owner
	 * @param event
	 * @param cycles
	 */
	public CycleEventContainer addEvent(Object owner, boolean processPVPTick, CycleEvent event,
			int cycles) {
		if (cycles < 1) // instant activation
			return addPlayerEvent((Player)owner, event, 0);
		else {
			Server.ensureOnMainThread();
			CycleEventContainer eventContainer = new CycleEventContainer(-1, owner, processPVPTick,
					event, cycles);
			 this.events.add(eventContainer);
			 return eventContainer;
		}
	}
	
	private CycleEventContainer addPlayerEvent(Player owner, CycleEvent event, int cycles) {
		CycleEventContainer eventContainer = new CycleEventContainer(-1, owner, event, cycles);
		owner.addEvent(eventContainer);
		return eventContainer;
	}

	/**
	 * Add an event to the list.
	 *
	 * @param owner
	 * @param event
	 * @param cycles
	 */
	public CycleEventContainer addEvent(Object owner, CycleEvent event, int cycles) {
		Server.ensureOnMainThread();
		CycleEventContainer eventContainer = new CycleEventContainer(-1, owner, event, cycles);
		this.events.add(eventContainer);
		return eventContainer;
	}

	/**
	 * Returns the amount of events currently running.
	 *
	 * @return amount
	 */
	public int getEventsCount() {
		return this.events.size();
	}

	/**
	 * Execute and remove events.
	 */
	public void process() {
		Server.ensureOnMainThread();
		List<CycleEventContainer> eventsCopy = new ArrayList<>(
				events);
		List<CycleEventContainer> remove = new ArrayList<>();
		for (CycleEventContainer c : eventsCopy) {
			if (c != null) {
				if (c.getOwner() == null) {
					remove.add(c);
				} else {
					if (c.needsExecution()) {
						if (c.isPlayerCombatProcess()) {
							// System.out.println("event executed");
							Player p = (Player) c.getOwner();
							if (p != null) {
								// p.addEvent(c.getOwner(), c.getEvent(), 0);
								p.addEvent(c);
							}
							// c.stop();
						} else {
							// System.out.println("event executed222");
							c.execute();
						}
					}
					if (!c.isRunning()) {
						// if (c.isPlayerCombatProcess())
						// System.out.println("properly removed");
						remove.add(c);
					}
				}
			}
		}
		for (CycleEventContainer c : remove) {
			events.remove(c);
		}
		eventsCopy.clear();
		remove.clear();
	}

	/**
	 * Stops a single event that can only be one instance. To not continue loop.
	 *
	 * @param owner
	 * @param id
	 */
	public void stopEvent(Object owner, int id) {
		Server.ensureOnMainThread();
		for (CycleEventContainer c : events) {
			if (c.getOwner() == owner && id == c.getID()) {
				c.stop();
				return;
			}
		}
	}

	/**
	 * Stops all events for a specific owner and ID.
	 *
	 * @param id
	 */
	public void stopEvents(int id) {
		Server.ensureOnMainThread();
		for (CycleEventContainer c : events) {
			if (id == c.getID()) {
				c.stop();
			}
		}
	}

	/**
	 * Stops all events for a specific owner and ID.
	 *
	 * @param owner
	 */
	public void stopEvents(Object owner) {
		Server.ensureOnMainThread();
		for (CycleEventContainer c : events) {
			if (c.getOwner() == owner) {
				c.stop();
			}
		}
	}

	/**
	 * Stops all events for a specific owner and ID.
	 *
	 * @param owner
	 * @param id
	 */
	public void stopEvents(Object owner, int id) {
		Server.ensureOnMainThread();
		for (CycleEventContainer c : events) {
			if (c.getOwner() == owner && id == c.getID()) {
				c.stop();
			}
		}
	}

	/**
	 * Stops all events for a specific owner and ID.
	 *
	 * @param id
	 */
	public void stopPvpEvents(boolean stopRunningTasks) {
		Server.ensureOnMainThread();
		for (CycleEventContainer c : events) {
			if (c.isPlayerCombatProcess() && c
					.isRunning() == stopRunningTasks/*
													 * && c.getOwner() == owner
													 */) {
				c.stop();
			}
		}
	}

}
