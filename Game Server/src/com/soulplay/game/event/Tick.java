package com.soulplay.game.event;

public abstract class Tick extends Task {

	private final int delay;

	private volatile int ticks;

	public Tick() {
		this(1);
	}

	public Tick(int delay) {
		this.delay = ticks = delay;
	}

	public final void countdown() {
		ticks--;
	}

	protected abstract void done();

	@Override
	protected void execute() {
		countdown();
		if (ticks <= 0) {
			try {
				done();
			} catch (Exception e) {
				e.printStackTrace();
			}
			stop();
		}
	}

	public final int getDelay() {
		return delay;
	}

	public final int getTicks() {
		return ticks;
	}

}
