package com.soulplay.game.event;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import com.soulplay.util.Misc;

public class CalendarHandler {

	/**
	 * Creates a new instance of the GregorianCalendar
	 */
	String[] ids;
	SimpleTimeZone pdt;
	GregorianCalendar cal;

	/**
	 * Set the GMT offset
	 */
	private static final int GMT = -5;

	/**
	 * Holds the names of the days of the week
	 */
	private static final String[] DAYS = {"sunday", "monday", "tuesday", "wednesday",
			"thursday", "friday", "saturday"};

	/**
	 * Holds the names of the months of the year
	 */
	private static final String[] MONTHS = {"january", "febuary", "march", "april",
			"may", "june", "july", "august", "september", "october", "november",
			"december"};

	public CalendarHandler() {
		ids = TimeZone.getAvailableIDs(GMT * 60 * 60 * 1000);
		pdt = new SimpleTimeZone(GMT * 60 * 60 * 1000, ids[0]);
		pdt.setStartRule(Calendar.JANUARY, 1, Calendar.SUNDAY,
				2 * 60 * 60 * 1000);
		pdt.setEndRule(Calendar.DECEMBER, -1, Calendar.SUNDAY,
				2 * 60 * 60 * 1000);
		cal = new GregorianCalendar(pdt);
		refreshCalendar();
	}

	/**
	 * Returns the current day of the month
	 */
	public int currentDayOfMonth() {
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Returns the current day id
	 */
	public int currentDayOfWeek() {
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * Returns the current month id
	 */
	public int currentMonth() {
		return cal.get(Calendar.MONTH);
	}

	public int get24Hour() {
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * Gets the name of the current day of the week
	 */
	private String getDayName(int day) {
		return Misc.optimizeText(DAYS[day - 1]);
	}

	/**
	 * Gets the current day of the month with appropriate addons
	 */
	public String getDayOfMonth(int day) {
		String addOn = "th";
		if (day == 1) {
			addOn = "st";
		} else if (day == 2) {
			addOn = "nd";
		} else if (day == 3) {
			addOn = "rd";
		}
		return Integer.toString(day) + addOn;
	}

	/**
	 * Gets the current day of the week
	 */
	public String getDayOfWeek() {
		return getDayName(currentDayOfWeek());
	}

	public int getDayOfYear() {
		return cal.get(Calendar.DAY_OF_YEAR);
	}

	public String getAmOrPm() {
		return cal.get(Calendar.AM_PM) == Calendar.PM ? "PM" : "AM";
	}

	/**
	 * Gets the current hour of the day
	 */
	public int getHour() {
		return cal.get(Calendar.HOUR);
	}

	/**
	 * Gets AM or PM
	 */
	public String getHourType() {
		if (cal.get(Calendar.AM_PM) == 0) {
			return "AM";
		}

		return "PM";
	}

	/**
	 * Returns the whole date Example: Today is Monday, April 29.
	 */
	public String getLongVersion() {
		return "Today is " + getDayName(currentDayOfWeek()) + ", "
				+ getMonthName(currentMonth()) + " "
				+ getDayOfMonth(currentDayOfMonth()) + ".";
	}

	/**
	 * Gets the current minutes of the day
	 */
	public int getMinutes() {
		return cal.get(Calendar.MINUTE);
	}

	/**
	 * Gets the current month of the year
	 */
	public String getMonth() {
		return getMonthName(currentMonth());
	}

	/**
	 * Gets the name of the current month of the year
	 */
	private String getMonthName(int month) {
		return Misc.optimizeText(MONTHS[month]);
	}

	/**
	 * Returns the current time
	 */
	public String getTime() {
		return getHour() + ":" + getMinutes() + getHourType();
	}

	public int getWeekOfYear() {
		return cal.get(Calendar.WEEK_OF_YEAR);
	}

	public int getYear() {
		return cal.get(Calendar.YEAR);
	}

	/**
	 * Returns true if it is the given day
	 */
	public boolean isDay(int day) {
		return (cal.get(Calendar.DAY_OF_WEEK) == cal.get(day));
	}

	/**
	 * Returns true if it is the given month
	 */
	public boolean isMonth(int month) {
		return (cal.get(Calendar.MONTH) == cal.get(month));
	}

	/**
	 * Sets all of the proper calendar variables
	 */
	public void refreshCalendar() {
		cal.setTimeInMillis(System.currentTimeMillis());
	}
	
	public boolean isAnniversaryMonth() {
		return isMonth(Calendar.JULY);
	}
	
	public boolean isXmas() {
		return isMonth(Calendar.DECEMBER);
	}
	
}
