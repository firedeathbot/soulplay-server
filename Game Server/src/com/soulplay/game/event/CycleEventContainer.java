package com.soulplay.game.event;

/**
 * The Wrapper Of The Event.
 *
 * @author Stuart <RogueX>
 * @author Null++ Revised by Shawn.
 */

public class CycleEventContainer {

	/**
	 * Event owner.
	 */
	private Object owner;

	/**
	 * Is the event running or not.
	 */
	private boolean isRunning;

	/**
	 * The amount of cycles per event execution.
	 */
	private int tick;

	/**
	 * The actual event.
	 */
	private CycleEvent event;

	/**
	 * The current amount of cycles passed.
	 */
	private int cyclesPassed;

	/**
	 * The event ID.
	 */
	private int eventID;

	/**
	 * Combat process
	 */
	private boolean playerCombatProcess;

	public CycleEventContainer(int id, Object owner, boolean processPVP,
			CycleEvent event, int tick) {
		this.eventID = id;
		this.owner = owner;
		this.event = event;
		this.isRunning = true;
		this.cyclesPassed = 0;
		this.tick = tick;
		this.playerCombatProcess = processPVP;
	}

	/**
	 * Sets the event containers details.
	 *
	 * @param owner
	 *            , the owner of the event.
	 * @param event
	 *            , the actual event to run.
	 * @param tick
	 *            , the cycles between execution of the event.
	 */
	public CycleEventContainer(int id, Object owner, CycleEvent event,
			int tick) {
		this.eventID = id;
		this.owner = owner;
		this.event = event;
		this.isRunning = true;
		this.cyclesPassed = 0;
		this.tick = tick;
		this.playerCombatProcess = false;
	}

	/**
	 * Execute the contents of the event.
	 */
	public void execute() {
		event.execute(this);
	}

	public int getCycles() {
		return this.cyclesPassed;
	}

	public CycleEvent getEvent() {
		return this.event;
	}

	/**
	 * Returns the event id.
	 *
	 * @return id
	 */
	public int getID() {
		return eventID;
	}

	/**
	 * Returns the owner of the event.
	 *
	 * @return
	 */
	public Object getOwner() {
		return owner;
	}

	public int getTick() {
		return this.tick;
	}

	public boolean isPlayerCombatProcess() {
		return playerCombatProcess;
	}

	/**
	 * Is the event running?.
	 *
	 * @return true yes
	 * @return false no
	 */
	public boolean isRunning() {
		return isRunning;
	}

	/**
	 * Does the event need to be ran?
	 *
	 * @return true yes
	 * @return false no
	 */
	public boolean needsExecution() {
		if (!this.isRunning()) {
			return false;
		}
		if (++this.cyclesPassed >= this.tick) {
			this.cyclesPassed = 0;
			return true;
		}
		return false;
	}

	public void setPlayerCombatProcess(boolean playerCombatProcess) {
		this.playerCombatProcess = playerCombatProcess;
	}

	/**
	 * Set the amount of cycles between the execution.
	 *
	 * @param tick
	 */
	public void setTick(int tick) {
		this.tick = tick;
	}

	/**
	 * Stop the event from running.
	 */
	public void stop() {
		isRunning = false;
		event.stop();
	}

}
