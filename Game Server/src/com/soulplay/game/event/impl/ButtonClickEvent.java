package com.soulplay.game.event.impl;

import com.soulplay.game.event.Event;

public class ButtonClickEvent implements Event {

    private final int button;

    public ButtonClickEvent(int button) {
        this.button = button;
    }

    public int getButton() {
        return button;
    }

    @Override
    public Object[] arguments() {
        return new Object[] {button};
    }

}
