package com.soulplay.game.event.impl;

import com.soulplay.game.event.Event;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;

public class ItemOnNpcEvent implements Event {

    private final NPC npc;
    private final Item used;
    private final int slot;

    public ItemOnNpcEvent(NPC npc, Item used, int slot) {
        this.npc = npc;
        this.used = used;
        this.slot = slot;
    }

    public NPC getNpc() {
        return npc;
    }

    public Item getUsed() {
        return used;
    }

    public int getSlot() {
        return slot;
    }

    @Override
    public Object[] arguments() {
        return new Object[] {npc, used, slot};
    }
}