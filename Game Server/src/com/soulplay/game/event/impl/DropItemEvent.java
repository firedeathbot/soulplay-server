package com.soulplay.game.event.impl;

import com.soulplay.game.event.Event;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.world.entity.Location;

public class DropItemEvent implements Event {

    private final Item item;
    private final int slot;
    private final Location position;

    public DropItemEvent(Item item, int slot, Location position) {
        this.item = item;
        this.slot = slot;
        this.position = position;
    }

    public Item getItem() {
        return item;
    }

    public int getSlot() {
        return slot;
    }

    public Location getPosition() {
        return position;
    }

    @Override
    public Object[] arguments() {
        return new Object[]{item, slot, position};
    }

}
