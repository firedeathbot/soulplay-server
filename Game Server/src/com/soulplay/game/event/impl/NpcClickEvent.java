package com.soulplay.game.event.impl;

import com.soulplay.game.event.Event;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;

/**
 * The {@link Event} that occurs when a {@link Player} clicks/interacts with an {@link NPC}.
 *
 * @author nshusa
 */
public class NpcClickEvent implements Event {

    /**
     * The type of click. (e.g attack, first option, second option)
     */
    private final int type;

    /**
     * The npc that is being interacted with.
     */
    private final NPC npc;

    public NpcClickEvent(int type, NPC npc) {
        this.type = type;
        this.npc = npc;
    }

    public int getType() {
        return type;
    }

    public NPC getNpc() {
        return npc;
    }

    @Override
    public Object[] arguments() {
        return new Object[] {type, npc};
    }

}
