package com.soulplay.game.event;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.event.impl.DailyEvents;
import com.soulplay.content.event.impl.DailyTasks;
import com.soulplay.content.event.impl.Event;
import com.soulplay.content.event.impl.HourlyTasks;
import com.soulplay.content.event.impl.MinuteTasks;

public class CalendarScheduler {

	// Example: public static Event donorPoints = new DonatorPoints();
	public static Event dailyDonorDungeon = new DailyTasks();

	public static Event hourlyEvents = new HourlyTasks();

	public static Event dailyEvents = new DailyEvents();

	public static Event minuteTasks = new MinuteTasks();

	private List<Event> minutes = new ArrayList<>();

	private List<Event> hourly = new ArrayList<>();

	private List<Event> daily = new ArrayList<>();

	private List<Event> monthly = new ArrayList<>();

	public void addDaily(Event event) {
		daily.add(event);
	}

	public void addHourly(Event event) {
		hourly.add(event);
	}

	public void addMinutes(Event event) {
		minutes.add(event);
	}

	public void addMonthly(Event event) {
		monthly.add(event);
	}

	public List<Event> getDaily() {
		return this.daily;
	}

	public List<Event> getHourly() {
		return this.hourly;
	}

	public List<Event> getMinutes() {
		return this.minutes;
	}

	public List<Event> getMonthly() {
		return this.monthly;
	}

	public void initialize() {
		// Example: addMinutes(donorPoints);
		addDaily(dailyDonorDungeon);
		addHourly(hourlyEvents);
		addHourly(dailyEvents);
		addMinutes(minuteTasks);

	}
}
