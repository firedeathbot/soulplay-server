package com.soulplay.game.event;

import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.event.impl.Event;
import com.soulplay.net.slack.SlackMessageBuilder;

public class CalendarEvents {

	private int minutes, hour, month, day;

	public CalendarEvents() {
		this.month = Server.getCalendar().currentMonth();
		this.day = Server.getCalendar().currentDayOfWeek();
		this.hour = Server.getCalendar().getHour();
		this.minutes = Server.getCalendar().getMinutes();
		startTask();
	}

	private void executeEvents(List<Event> list) {
		for (int i = 0, length = list.size(); i < length; i++) {
			Event event = list.get(i);
			if (event != null) {
				if (event.executeEvent()) {
					event.execute();
				} else if (event.finishEvent()) {
					event.finish();
				}
			}
		}
	}

	public int getDay() {
		return this.day;
	}

	public int getHour() {
		return this.hour;
	}

	public int getMinutes() {
		return this.minutes;
	}

	public int getMonth() {
		return this.month;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	private void startTask() {
		Server.getTaskScheduler().schedule(new Task(2 * 15, false) {

			@Override
			protected void execute() {
				try {
					CalendarHandler calendar = Server.getCalendar();
					calendar.refreshCalendar();

					int month = calendar.currentMonth();
					if (getMonth() != month) {
						setMonth(month);
						executeEvents(Server.getCalScheduler().getMonthly());
					}

					int day = calendar.currentDayOfWeek();
					if (getDay() != day) {
						setDay(day);
						executeEvents(Server.getCalScheduler().getDaily());
					}

					int hours = calendar.getHour();
					if (getHour() != hours) {
						setHour(hours);
						executeEvents(Server.getCalScheduler().getHourly());
					}

					int minutes = calendar.getMinutes();
					if (getMinutes() != minutes) {
						setMinutes(minutes);
						executeEvents(Server.getCalScheduler().getMinutes());
					}
				} catch (Exception e) {
					e.printStackTrace();
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(CalendarEvents.class, e));
				}
			}
		});
	}

}
