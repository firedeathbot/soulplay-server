package com.soulplay.game.map.clip.region;

public class MapIndex {
	private final int regionId;
	private final int landscape;
	private final int objects;
	
	public MapIndex(int regionId, int landscape, int objects) {
		this.regionId = regionId;
		this.landscape = landscape;
		this.objects = objects;
	}

	public int getRegionId() {
		return regionId;
	}

	public int landscape() {
		return landscape;
	}

	public int objects() {
		return objects;
	}
}
