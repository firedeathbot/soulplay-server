package com.soulplay.game.map.clip.region;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import com.google.common.io.Files;
import com.soulplay.Config;
import com.soulplay.cache.ObjectDef;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public class RegionClip {

	// private static ArrayList<GameObject> gameObject = new
	// ArrayList<GameObject>();
	// private ArrayList<DoubleDoor> doubleDoors = new ArrayList<DoubleDoor>();
	// private static ArrayList<GameObject> allObjects = new
	// ArrayList<GameObject>();

	// private static RegionClip singleton = null;

	// public static RegionClip getSingleton() {
	// if (singleton == null) {
	// singleton = new RegionClip();
	// }
	// return singleton;
	// }

	public static RegionClip[] regionIdTable;

	static int[] regionIds;

	static short[] mapGroundFileIds;

	static short[] mapObjectsFileIds;

	public static boolean finishLoadingMaps = false;
	
	private static boolean COPY_MODELS = false;
	
	public static boolean LOAD_OSRS_CLIPMAP = true;
	
	// public static int count() {
	// return gameObject.size();
	// }
	
	public static final int WALKING_TILE_BLOCKED = 0x100;

	public static final int PROJECTILE_NORTH_WEST_BLOCKED = 0x200;

	public static final int PROJECTILE_NORTH_BLOCKED = 0x400;

	public static final int PROJECTILE_NORTH_EAST_BLOCKED = 0x800;

	public static final int PROJECTILE_EAST_BLOCKED = 0x1000;
	
	public static final int PROJECTILE_SOUTH_EAST_BLOCKED = 0x2000;

	public static final int PROJECTILE_SOUTH_BLOCKED = 0x4000;

	public static final int PROJECTILE_SOUTH_WEST_BLOCKED = 0x8000;

	public static final int PROJECTILE_WEST_BLOCKED = 0x10000;

	public static final int PROJECTILE_TILE_BLOCKED = 0x20000;

	public static final int UNKNOWN = 0x80000;

	public static final int BLOCKED_TILE = 0x200000; // this is the ocean tile 2097152

	public static final int UNLOADED_TILE = 0x1000000;

	public static final int OCEAN_TILE = 2097152; // 0x200000
	
	public static final int NPC_TILE = 0x2000000;
	
	public static final int BARRICADE_TILE = 0x4000000;
	
	public static final int PROJECTILE_CLIP_START_OR = /*UNLOADED_TILE |*/ /* BLOCKED_TILE | *//*UNKNOWN |*/ PROJECTILE_TILE_BLOCKED;

	private static void addClipping(int x, int y, int height, int shift) {
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;
		if (regionId < 0 || regionId >= regionIdTable.length) {
			//System.out.println("Unable clipping regionID:"+regionId+" X:"+x+" Y:"+y+" Z:"+height);
			return;
		}
		if (regionIdTable[regionId] == null) {
			// System.out.println("missing region "+regionId);
			return;
		}
		regionIdTable[regionId].addClip(x, y, height % 4, shift);
	}

	private static void addClippingForSolidObject(int x, int y, int height,
			int xLength, int yLength, boolean flag) {
		int clipping = 256;
		if (flag) {
			clipping += 0x20000;
		}
		for (int i = x; i < x + xLength; i++) {
			for (int i2 = y; i2 < y + yLength; i2++) {
				addClipping(i, i2, height, clipping);
			}
		}
	}

	private static void addClippingForVariableObject(int x, int y, int height,
			int type, int direction, boolean flag) {
		if (type == 0) {
			if (direction == 0) {
				addClipping(x, y, height, 128);
				addClipping(x - 1, y, height, 8);
			} else if (direction == 1) {
				addClipping(x, y, height, 2);
				addClipping(x, y + 1, height, 32);
			} else if (direction == 2) {
				addClipping(x, y, height, 8);
				addClipping(x + 1, y, height, 128);
			} else if (direction == 3) {
				addClipping(x, y, height, 32);
				addClipping(x, y - 1, height, 2);
			}
		} else if (type == 1 || type == 3) {
			if (direction == 0) {
				addClipping(x, y, height, 1);
				addClipping(x - 1, y, height, 16);
			} else if (direction == 1) {
				addClipping(x, y, height, 4);
				addClipping(x + 1, y + 1, height, 64);
			} else if (direction == 2) {
				addClipping(x, y, height, 16);
				addClipping(x + 1, y - 1, height, 1);
			} else if (direction == 3) {
				addClipping(x, y, height, 64);
				addClipping(x - 1, y - 1, height, 4);
			}
		} else if (type == 2) {
			if (direction == 0) {
				addClipping(x, y, height, 130);
				addClipping(x - 1, y, height, 8);
				addClipping(x, y + 1, height, 32);
			} else if (direction == 1) {
				addClipping(x, y, height, 10);
				addClipping(x, y + 1, height, 32);
				addClipping(x + 1, y, height, 128);
			} else if (direction == 2) {
				addClipping(x, y, height, 40);
				addClipping(x + 1, y, height, 128);
				addClipping(x, y - 1, height, 2);
			} else if (direction == 3) {
				addClipping(x, y, height, 160);
				addClipping(x, y - 1, height, 2);
				addClipping(x - 1, y, height, 8);
			}
		}
		if (flag) {
			if (type == 0) {
				if (direction == 0) {
					addClipping(x, y, height, 65536);
					addClipping(x - 1, y, height, 4096);
				} else if (direction == 1) {
					addClipping(x, y, height, 1024);
					addClipping(x, y + 1, height, 16384);
				} else if (direction == 2) {
					addClipping(x, y, height, 4096);
					addClipping(x + 1, y, height, 65536);
				} else if (direction == 3) {
					addClipping(x, y, height, 16384);
					addClipping(x, y - 1, height, 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (direction == 0) {
					addClipping(x, y, height, 512);
					addClipping(x - 1, y + 1, height, 8192);
				} else if (direction == 1) {
					addClipping(x, y, height, 2048);
					addClipping(x + 1, y + 1, height, 32768);
				} else if (direction == 2) {
					addClipping(x, y, height, 8192);
					addClipping(x + 1, y + 1, height, 512);
				} else if (direction == 3) {
					addClipping(x, y, height, 32768);
					addClipping(x - 1, y - 1, height, 2048);
				}
			} else if (type == 2) {
				if (direction == 0) {
					addClipping(x, y, height, 66560);
					addClipping(x - 1, y, height, 4096);
					addClipping(x, y + 1, height, 16384);
				} else if (direction == 1) {
					addClipping(x, y, height, 5120);
					addClipping(x, y + 1, height, 16384);
					addClipping(x + 1, y, height, 65536);
				} else if (direction == 2) {
					addClipping(x, y, height, 20480);
					addClipping(x + 1, y, height, 65536);
					addClipping(x, y - 1, height, 1024);
				} else if (direction == 3) {
					addClipping(x, y, height, 81920);
					addClipping(x, y - 1, height, 1024);
					addClipping(x - 1, y, height, 4096);
				}
			}
		}
	}

	public static void addGameObject(GameObject o) {
		RegionClip r = getRegion(o.getX(), o.getY());
		if (o.getType() == 35) {
			// System.out.println("Object ID is "+o.getId());
			o.setType(10);
		}
		GameObject o2 = getGameObject(o.getX(), o.getY(), o.getZ(), o.getType(),
				true); // getGameObject(o.getX(), o.getY(), o.getZ(),
						// o.getType(), true);
		if (o2 != null) { // what is this for? i forgot.... some kind of
							// replacing objects? junk code by now since i
							// rewrote things
			if (r != null) {
				
				// remove the clip of the old object just in case new object doesn't clip at all
				if (finishLoadingMaps)
					removeGameObject(o2);
				
				r.addGameObject(o, o.getX(), o.getY(), o.getZ(), o.getType()); // TODO:
																				// changed
																				// the
																				// o2
																				// to
																				// o
				if (isDwarfCannon(o.getId())) {
					return; // do no add dwarf cannon to clipmap
				}
				if (!finishLoadingMaps) {
					return;
				}
				if (o.getId() != -1) {
					addObject(o.getId(), o.getX(), o.getY(), o.getZ(),
							o.getType(), o.getOrientation(), false);
					// System.out.println("added object to clipmap");
				}
				return;
			}
		} else if (r != null) {
			r.addGameObject(o, o.getX(), o.getY(), o.getZ(), o.getType());
			// System.out.println("gameobject size "+r.gameObject.size());
			if (isDwarfCannon(o.getId())) {
				return; // do no add dwarf cannon to clipmap
			}
			if (!finishLoadingMaps) {
				return;
			}
			addObject(o.getId(), o.getX(), o.getY(), o.getZ(), o.getType(),
					o.getOrientation(), false);
			// System.out.println("added object");
		}
	}

	public static void addObject(int objectId, int x, int y, int height,
			int type, int direction, boolean addToGameObjectList) {
		try {
			
			int regionX = x >> 6;
			int regionY = y >> 6;
			int regionId = (regionX << 8) + regionY;

			switch (objectId) {
				case 733:
				case 734:
				case 100733:
				case 100734:
					type = 0;
					break;
			}
			
//			if (objectId == 1728 && x == 3201 && y == 3856) {
//				// System.out.println("object id is :"+objectId+"
//				// ------------------------------------------------H:"+height+"
//				// type:"+type);
//				objectId = 1597;
//			}
//			if (objectId == 1727 && x == 3202 && y == 3856) {
//				// System.out.println("object id is :"+objectId+"
//				// --------------------R:"+direction+"---------------------------H:"+height+"
//				// type:"+type);
//				// System.out.println("O:"+objectId+" x:"+x+" y:"+y+"
//				// h:"+height+" t:"+type+" r:"+direction+"
//				// bool:"+addToGameObjectList);
//				objectId = 1596;
//			}
			
//			if (objectId == 6951) // some strange object with opcode 74 on obj defs
//				System.out.println(String.format("X:%d Y:%d Z:%d", x, y, height));

			ObjectDef def;
			switch (regionId) {
				case 12598:
					def = ObjectDef.forID525(objectId);
					break;
				case 5444:
				case 5445:
				case 5700:
				case 5701:
				case 4676:
				case 4677:
				case 4932:
				case 4933:
					def = ObjectDef.forID667(objectId);
					break;
				default:
					if (objectId >= ObjectDef.OSRS_START)
						def = ObjectDef.forIDOSRS(objectId);
					else if (x <= 650)
						def = ObjectDef.forID667(objectId);
					else if (x >= 6400 || isOsrsRegion(regionId))
						def = ObjectDef.forIDOSRS(objectId);
					else
						def = ObjectDef.getObjectDef(objectId);
					break;
			}

			if (def == null) {
				return;
			}

			int xLength;
			int yLength;
			if (direction != 1 && direction != 3) {
				xLength = def.xLength();
				yLength = def.yLength();
			} else {
				xLength = def.yLength();
				yLength = def.xLength();
			}
			// if (objectId == 4411) {
			// System.out.println("objectType = "+type);
			// }
			if (type == 22) {
				if (def.hasActions() && def.requiresClipping()) {
					addClipping(x, y, height, 0x200000);
				}
			} else if (type >= 9) {
				if (def.requiresClipping()) {
					addClippingForSolidObject(x, y, height, xLength, yLength,
							def.clippedProjectile());
				}
			} else if (type >= 0 && type <= 3) {
				if (def.requiresClipping()) {
					addClippingForVariableObject(x, y, height, type, direction,
							def.clippedProjectile());
				}
			}

			if (addToGameObjectList) {
				GameObject gameObject = new GameObject(objectId, Location.create(x, y, height), direction, type);
				if (gameObject.getId() == -1) {
					removeGameObject(gameObject);
				} else {
					addGameObject(gameObject);
				}
				gameObject = null;

			}

		} catch (Exception e) { // TODO: Put back to check for what objects
								// didn't load or crashed
			// System.out.println("ID:"+objectId+" X:"+x+" Y:"+y+" H:"+height);
			e.printStackTrace();
		}

	}

	public static boolean isOsrsRegion(int regionId) {
		switch (regionId) {
			case 7227:
			case 7228:
			case 7483:
			case 7484:
				return true;
			default:
				return false;
		}
	}

	public static boolean blockedShot(int x, int y, int height, int moveTypeX,
			int moveTypeY, DynamicRegionClip dynClip) {
		height = height & 3;
		if (moveTypeX == -1 && moveTypeY == 0) {
			return (getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_WEST_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_EAST_BLOCKED)) != 0;
		} else if (moveTypeX == 1 && moveTypeY == 0) {
			return (getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_EAST_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_WEST_BLOCKED)) != 0;
		} else if (moveTypeX == 0 && moveTypeY == -1) {
			return (getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_SOUTH_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_NORTH_BLOCKED)) != 0;
		} else if (moveTypeX == 0 && moveTypeY == 1) {
			return (getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_NORTH_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_SOUTH_BLOCKED)) != 0;
		} else if (moveTypeX == -1 && moveTypeY == -1) {
			return ((getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_SOUTH_WEST_BLOCKED | PROJECTILE_SOUTH_BLOCKED | PROJECTILE_WEST_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_NORTH_EAST_BLOCKED | PROJECTILE_NORTH_BLOCKED | PROJECTILE_EAST_BLOCKED)) != 0);
		} else if (moveTypeX == 1 && moveTypeY == -1) {
			return ((getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_SOUTH_BLOCKED | PROJECTILE_EAST_BLOCKED | PROJECTILE_SOUTH_EAST_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_NORTH_BLOCKED | PROJECTILE_WEST_BLOCKED | PROJECTILE_NORTH_WEST_BLOCKED)) != 0);
		} else if (moveTypeX == -1 && moveTypeY == 1) {
			return ((getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_NORTH_WEST_BLOCKED | PROJECTILE_NORTH_BLOCKED | PROJECTILE_WEST_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_SOUTH_EAST_BLOCKED | PROJECTILE_SOUTH_BLOCKED | PROJECTILE_EAST_BLOCKED)) != 0);
		} else if (moveTypeX == 1 && moveTypeY == 1) {
			return ((getClipping(x, y, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_NORTH_BLOCKED | PROJECTILE_EAST_BLOCKED | PROJECTILE_NORTH_EAST_BLOCKED)) != 0
					|| (getClipping(x+moveTypeX, y+moveTypeY, height, dynClip) & (PROJECTILE_CLIP_START_OR | PROJECTILE_WEST_BLOCKED | PROJECTILE_SOUTH_BLOCKED | PROJECTILE_SOUTH_WEST_BLOCKED)) != 0);
		} else {
			return false;
		}
	}

	public static boolean canProjectileMove(int startX, int startY, int endX,
			int endY, int height, int xLength, int yLength) {
		int diffX = endX - startX;
		int diffY = endY - startY;
		// height %= 4;
		int max = Math.max(Math.abs(diffX), Math.abs(diffY));
		for (int ii = 0; ii < max; ii++) {
			int currentX = endX - diffX;
			int currentY = endY - diffY;
			for (int i = 0; i < xLength; i++) {
				for (int i2 = 0; i2 < yLength; i2++) {
					if (diffX < 0 && diffY < 0) {
						if ((RegionClip.getClipping(currentX + i - 1,
								currentY + i2 - 1, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_EAST_BLOCKED
										| PROJECTILE_NORTH_EAST_BLOCKED
										| PROJECTILE_NORTH_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i - 1,
										currentY + i2, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_EAST_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i,
										currentY + i2 - 1, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_NORTH_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX > 0 && diffY > 0) {
						if ((RegionClip.getClipping(currentX + i + 1,
								currentY + i2 + 1, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_WEST_BLOCKED
										| PROJECTILE_SOUTH_WEST_BLOCKED
										| PROJECTILE_SOUTH_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i + 1,
										currentY + i2, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_WEST_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i,
										currentY + i2 + 1, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_SOUTH_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX < 0 && diffY > 0) {
						if ((RegionClip.getClipping(currentX + i - 1,
								currentY + i2 + 1, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_SOUTH_BLOCKED
										| PROJECTILE_SOUTH_EAST_BLOCKED
										| PROJECTILE_EAST_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i - 1,
										currentY + i2, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_EAST_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i,
										currentY + i2 + 1, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_SOUTH_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX > 0 && diffY < 0) {
						if ((RegionClip.getClipping(currentX + i + 1,
								currentY + i2 - 1, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_WEST_BLOCKED
										| PROJECTILE_NORTH_BLOCKED
										| PROJECTILE_NORTH_WEST_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i + 1,
										currentY + i2, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_WEST_BLOCKED)) != 0
								|| (RegionClip.getClipping(currentX + i,
										currentY + i2 - 1, height)
										& (UNLOADED_TILE
												| /* BLOCKED_TILE | */UNKNOWN
												| PROJECTILE_TILE_BLOCKED
												| PROJECTILE_NORTH_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX > 0 && diffY == 0) {
						if ((RegionClip.getClipping(currentX + i + 1,
								currentY + i2, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_WEST_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX < 0 && diffY == 0) {
						if ((RegionClip.getClipping(currentX + i - 1,
								currentY + i2, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_EAST_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX == 0 && diffY > 0) {
						if ((RegionClip.getClipping(currentX + i,
								currentY + i2 + 1, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_SOUTH_BLOCKED)) != 0) {
							return false;
						}
					} else if (diffX == 0 && diffY < 0) {
						if ((RegionClip.getClipping(currentX + i,
								currentY + i2 - 1, height)
								& (UNLOADED_TILE | /* BLOCKED_TILE | */UNKNOWN
										| PROJECTILE_TILE_BLOCKED
										| PROJECTILE_NORTH_BLOCKED)) != 0) {
							return false;
						}
					}
				}
			}
			if (diffX < 0) {
				diffX++;
			} else if (diffX > 0) {
				diffX--;
			}
			if (diffY < 0) {
				diffY++; // change
			} else if (diffY > 0) {
				diffY--;
			}
		}
		return true;
	}

	public static boolean canStandOnSpot(NPC npc, int posX, int posY) {
		int size = npc.getSize();
		int z = npc.getZ();

		for (int x = 0; x < size; x++) {
			int xPos = posX + x;
			for (int y = 0; y < size; y++) {
				int yPos = posY + y;
				if (!isFloorFree(getClipping(xPos, yPos, z, npc.getDynamicRegionClip()))) {
					return false;
				}
			}
		}

		return true;
	}

	public static Location findLocationAround(Mob mob) {
		if (mob == null) {
			return null;
		}

		Location location = mob.getCurrentLocation();
		for (Direction direction : Direction.valuesNoDiagonal) {
			Location gen = location.transform(direction);
			if (gen != null && !gen.matches(location) && RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), mob.getDynamicRegionClip())) {
				return gen;
			}
		}

		return null;
	}

	public static boolean canStandOnSpot(int x, int y, int z, DynamicRegionClip clipMap) { // standing on an area that is completely clipped, or object clipped
		return (isFloorFree(getClipping(x, y, z, clipMap)));
	}

	public static boolean canStandOnSpot(int x, int y, int z) {
		return canStandOnSpot(x, y, z, null);
	}
	
	public static boolean isWallsFree(int mask) {
		return (mask & (0x1 | 0x2 | 0x4 | 0x8 | 0x10 | 0x20 | 0x40 | 0x80)) == 0;
	}
	
	public static boolean isFloorFree(int mask) {
		return (mask & (0x200000 | 0x100 | 0x40000)) == 0;
	}
	
	public static boolean isInsideBuilding(int x, int y, DynamicRegionClip clipMap) {
		for (int z = 0; z < 4; z++) {
			if (getClipping(x, y, z, clipMap) == 131328)
				return true;
		}
		return false;
	}
	
	public static int getClipping(int x, int y, int height, DynamicRegionClip dynClip) {
		return dynClip != null ? dynClip.getClipping(x, y, height) : getClipping(x, y, height);
	}

	public static int getClipping(int x, int y, int height) {
		height = height & 3;
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;

		if (regionId < 0)
			return 0x100;
		if (regionIdTable.length < regionId)
			return 0x100;
		try {

			if (regionIdTable[regionId] != null) {
				return regionIdTable[regionId].getClip(x, y, height);
			}
		} catch (Exception e) {
			return 0x100;
		}
		return 0x100;
	}
	
	public static void rotateWallWalkingClip(boolean clockWise, Location loc) {
		int regionId = ((loc.getX() >> 6) << 8) + (loc.getY() >> 6);
		if (regionIdTable[regionId] == null) {
			return;
		}
		
		regionIdTable[regionId].rotateClip(loc, clockWise);
	}
	
	private void rotateClip(Location loc, boolean clockWise) {
		int currentClip = getClip(loc.getX(), loc.getY(), loc.getZ());
		int wallWalkClip = currentClip & 0xFF;// get the walking clip for walls only
		//rotate once, then nondiagonal will go into diagonal version(example:south->southeast) so we will rotate twice
		// 2+6 = 8 bits to rotate, ignoring the 9th which is objects
		int newWalkClip = !clockWise ? (wallWalkClip >> 2) | (wallWalkClip << 6) : (wallWalkClip << 2) | (wallWalkClip >> 6);
		currentClip >>= 8; // lesik's retarded midnight cheaphax XD
		currentClip <<= 8; // lesik's retarded midnight cheaphax XD
		int newClip = currentClip | newWalkClip;
		setClip(loc, newClip);
	}
	
	public static int getClippingIgnoreBarricade(int x, int y, int height) {
		height = height & 3;
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;
		try {
			if (regionIdTable.length < regionId) {
				return 0;
			}
		} catch (Exception e) {
			return 0;
		}

		if (regionIdTable[regionId] != null) {
			if (regionIdTable[regionId].getBarricade(x, y, height) == 1)
				return 0;
			return regionIdTable[regionId].getClip(x, y, height);
		}
		return 0;
	}

	public static boolean getClippingDirection(int x, int y, int height, int moveTypeX,
			int moveTypeY, DynamicRegionClip dynClip) {
		try {
			height = height & 3;
			int checkX = (x + moveTypeX);
			int checkY = (y + moveTypeY);
			if (moveTypeX == -1 && moveTypeY == 0) {
				return (getClipping(x, y, height, dynClip) & 0x1280108) == 0;
			} else if (moveTypeX == 1 && moveTypeY == 0) {
				return (getClipping(x, y, height, dynClip) & 0x1280180) == 0;
			} else if (moveTypeX == 0 && moveTypeY == -1) {
				return (getClipping(x, y, height, dynClip) & 0x1280102) == 0;
			} else if (moveTypeX == 0 && moveTypeY == 1) {
				return (getClipping(x, y, height, dynClip) & 0x1280120) == 0;
			} else if (moveTypeX == -1 && moveTypeY == -1) {
				return ((getClipping(x, y, height, dynClip) & 0x128010e) == 0
						&& (getClipping(checkX - 1, checkY, height, dynClip)
								& 0x1280108) == 0
						&& (getClipping(checkX - 1, checkY, height, dynClip)
								& 0x1280102) == 0);
			} else if (moveTypeX == 1 && moveTypeY == -1) {
				return ((getClipping(x, y, height, dynClip) & 0x1280183) == 0
						&& (getClipping(checkX + 1, checkY, height, dynClip)
								& 0x1280180) == 0
						&& (getClipping(checkX, checkY - 1, height, dynClip)
								& 0x1280102) == 0);
			} else if (moveTypeX == -1 && moveTypeY == 1) {
				return ((getClipping(x, y, height, dynClip) & 0x1280138) == 0
						&& (getClipping(checkX - 1, checkY, height, dynClip)
								& 0x1280108) == 0
						&& (getClipping(checkX, checkY + 1, height, dynClip)
								& 0x1280120) == 0);
			} else if (moveTypeX == 1 && moveTypeY == 1) {
				return ((getClipping(x, y, height, dynClip) & 0x12801e0) == 0
						&& (getClipping(checkX + 1, checkY, height, dynClip)
								& 0x1280180) == 0
						&& (getClipping(checkX, checkY + 1, height, dynClip)
								& 0x1280120) == 0);
			} else {
//				System.out.println(
//						"[FATAL ERROR]: At getClipping: " + x + ", " + y + ", "
//								+ height + ", " + moveTypeX + ", " + moveTypeY);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println("getClipping error...");
			return true;
		}
	}

	public static boolean getClipping(int x, int y, int height, int moveTypeX,
			int moveTypeY, boolean usingRange, DynamicRegionClip dynClip) {
		try {
			height = height % 4;
			int checkX = (x + moveTypeX);
			int checkY = (y + moveTypeY);
			if (moveTypeX == -1 && moveTypeY == 0) {
				return (getClipping(x, y, height, dynClip) & 0x1280108) == 0;
			} else if (moveTypeX == 1 && moveTypeY == 0) {
				return (getClipping(x, y, height, dynClip) & 0x1280180) == 0;
			} else if (moveTypeX == 0 && moveTypeY == -1) {
				return (getClipping(x, y, height, dynClip) & 0x1280102) == 0;
			} else if (moveTypeX == 0 && moveTypeY == 1) {
				return (getClipping(x, y, height, dynClip) & 0x1280120) == 0;
			} else if (moveTypeX == -1 && moveTypeY == -1) {
				return ((getClipping(x, y, height, dynClip) & 0x128010e) == 0
						&& (getClipping(checkX - 1, checkY, height, dynClip)
								& 0x1280108) == 0
						&& (getClipping(checkX - 1, checkY, height, dynClip)
								& 0x1280102) == 0);
			} else if (moveTypeX == 1 && moveTypeY == -1) {
				return ((getClipping(x, y, height, dynClip) & 0x1280183) == 0
						&& (getClipping(checkX + 1, checkY, height, dynClip)
								& 0x1280180) == 0
						&& (getClipping(checkX, checkY - 1, height, dynClip)
								& 0x1280102) == 0);
			} else if (moveTypeX == -1 && moveTypeY == 1) {
				return ((getClipping(x, y, height, dynClip) & 0x1280138) == 0
						&& (getClipping(checkX - 1, checkY, height, dynClip)
								& 0x1280108) == 0
						&& (getClipping(checkX, checkY + 1, height, dynClip)
								& 0x1280120) == 0);
			} else if (moveTypeX == 1 && moveTypeY == 1) {
				return ((getClipping(x, y, height, dynClip) & 0x12801e0) == 0
						&& (getClipping(checkX + 1, checkY, height, dynClip)
								& 0x1280180) == 0
						&& (getClipping(checkX, checkY + 1, height, dynClip)
								& 0x1280120) == 0);
			} else {
				System.out.println(
						"[FATAL ERROR]: At getClipping: " + x + ", " + y + ", "
								+ height + ", " + moveTypeX + ", " + moveTypeY);
				return false;
			}
		} catch (Exception e) {
			// e.printStackTrace();
			// System.out.println("getClipping error...");
			return true;
		}
	}

	public static GameObject getGameObject(int id, int x, int y, int z) {
		RegionClip r = getRegion(x, y);
		if (r == null) {
			return null;
		}

		int regionAbsX = (r.id >> 8) << 6;
		int regionAbsY = (r.id & 0xff) << 6;
		z = z % 4;

		try {
			//Local var for more speed
			GameObject[][][][] gameObjects = r.gameObjects;

			int localX = x - regionAbsX;
			int localY = y - regionAbsY;

			GameObject instance = gameObjects[z][Misc.OBJECT_SLOTS[10]][localX][localY];

			if (instance != null && instance.getId() == id) {
				return instance;
			}

			for (int i = 0; i < 4; i++) {
				if (i == Misc.OBJECT_SLOTS[10]) continue; // already tried grabbing this type!
				instance = gameObjects[z][i][localX][localY];
				if (instance != null) {
					if (instance.getId() == id) {
						return instance;
					}
				}
			}

		} catch (Exception ex) {
			if (!Config.RUN_ON_DEDI)
				ex.printStackTrace();
			return null;
		}

		return null;
	}

	public static GameObject getGameObject(int x, int y, int z, int type,
			boolean shit) {
		RegionClip r = getRegion(x, y);
		if (r == null) {
			return null;
		}
		int regionAbsX = (r.id >> 8) << 6;
		int regionAbsY = (r.id & 0xff) << 6;
		z = z % 4;
		if (type > 23) {
			type = 10;
		}
		type = Misc.OBJECT_SLOTS[type];
		if (r.gameObjects == null || r.gameObjects[z] == null
				|| r.gameObjects[z][type] == null
				|| r.gameObjects[z][type][x - regionAbsX] == null
				|| r.gameObjects[z][type][x - regionAbsX][y
						- regionAbsY] == null) {
			return null;
		}
		return r.gameObjects[z][type][x - regionAbsX][y - regionAbsY];
	}

	public static GameObject getGameObjectByType(int x, int y, int z, int miscType) {
		RegionClip r = getRegion(x, y);
		if (r == null) {
			return null;
		}

		int regionAbsX = (r.id >> 8) << 6;
		int regionAbsY = (r.id & 0xff) << 6;
		z = z % 4;

		GameObject[][][][] gameObjects = r.gameObjects;
		if (gameObjects == null || gameObjects[z] == null
				|| gameObjects[z][miscType] == null
				|| gameObjects[z][miscType][x - regionAbsX] == null
				|| gameObjects[z][miscType][x - regionAbsX][y - regionAbsY] == null) {
			return null;
		}

		return gameObjects[z][miscType][x - regionAbsX][y - regionAbsY];
	}
	
	public static boolean objectExists(int id, int x, int y, int z, int type) {
		GameObject o = getGameObjectByType(x, y, z, Misc.OBJECT_SLOTS[10]);
		return o != null && o.getId() == id;
	}

	public static RegionClip getRegion(int x, int y) {
		try {
			int regionX = x >> 6;
			int regionY = y >> 6;
			int regionId = (regionX << 8) + regionY;
			if (regionId < 0 || regionId > regionIdTable.length)
				return null;
			/*
			 * for (RegionClip r : regions) { if (r.id() == regionId) { return
			 * r; } } return null;
			 */
			return regionIdTable[regionId];
		} catch (Exception ex) {
			System.out.println(
					"Region crash attemp at region X:" + x + " Y:" + y);
			return null;
		}
	}

	public static boolean isDwarfCannon(int objectId) {
		switch (objectId) {
			case 6:
			case 7:
			case 8:
			case 9:
			case 4900: // cw flag
			case 4901: // cw flag
				return true;
		}
		return false;
	}

	public static void loadMaps() {
		try {
			List<MapIndex> osrsMaps = new ArrayList<MapIndex>();
			
			ByteStream in = new ByteStream(Misc.readFile("./Data/world/map_index"));
			int size = in.getUShort();
			System.out.println("667Maps Size " + size);
			int size667 = size;
			ByteStream osrsIndex = new ByteStream(Misc.readFile("./Data/world/map_index_osrs"));
			int sizeOsrs = osrsIndex.getUShort();
			System.out.println("Osrs Maps size:"+sizeOsrs);
			size = size + sizeOsrs + 30;
			int lastIndex = 0;
			/* int[] */regionIds = new int[size];
			/* int[] */mapGroundFileIds = new short[size];
			/* int[] */mapObjectsFileIds = new short[size];
			// boolean[] isMembers = new boolean[size];
			
			int highest = 0;
			for (int i = 0; i < size667; i++) {
				regionIds[i] = in.getUShort();
				if (regionIds[i] > highest) {
					highest = regionIds[i];
				}

				mapGroundFileIds[i] = (short) in.getUShort();
				mapObjectsFileIds[i] = (short) in.getUShort();
				// isMembers[i] = in.getUByte() == 0;
				lastIndex++;
			}
			loadNewMap(lastIndex++, 9007, 5180, 5181); // zulrah shrine
			loadNewMap(lastIndex++, 7227, 7000, 7001);// last man standing //13658
			loadNewMap(lastIndex++, 7228, 7002, 7003);// last man standing //13659
			loadNewMap(lastIndex++, 7483, 7004, 7005);// last man standing //13914
			loadNewMap(lastIndex++, 7484, 7006, 7007);// last man standing //13915
			
			loadNewMap(lastIndex++, 5444, 6000, 6001);// donator thing
			loadNewMap(lastIndex++, 5445, 6002, 6003);// donator thing
			loadNewMap(lastIndex++, 5700, 6004, 6005);// donator thing
			loadNewMap(lastIndex++, 5701, 6006, 6007);// donator thing
			
			loadNewMap(lastIndex++, 4676, 6008, 6009);// clan citadel
			loadNewMap(lastIndex++, 4677, 6010, 6011);// clan citadel
			loadNewMap(lastIndex++, 4932, 6012, 6013);// clan citadel
			loadNewMap(lastIndex++, 4933, 6014, 6015);// clan citadel

			int last667index = lastIndex;
			
			if (LOAD_OSRS_CLIPMAP) {
				for (int i = 0; i < sizeOsrs; i++) {
					int regionId = osrsIndex.getUShort();
					int landscape = osrsIndex.getUShort();
					int objects = osrsIndex.getUShort();
					if (!osrsMapIn317(regionId)) {
						int chunkX = regionId >> 8;
						int chunkY = regionId & 0xFF;
						chunkX += 100; // set new region area
						regionId = (chunkX << 8) + chunkY;
					}
					if (regionId > highest) {
						highest = regionId;
					}

					osrsMaps.add(new MapIndex(regionId, landscape, objects));
					regionIds[lastIndex++] = regionId; //loadNewMap(lastIndex++, regionId, map1, map2);
				}
			}

			regionIdTable = new RegionClip[highest + 1];
			for (int i = 0; i < size; i++) {
				int id = regionIds[i];
				regionIdTable[id] = new RegionClip(id);
			}

			for (int i = 0; i < last667index; i++) { // this(last667index) will add zulrah map too since i put it above the int creation
				byte[] file1 = Misc.readFileGzip(mapsFolder667 + mapObjectsFileIds[i] + ".gz");
				if (file1 == null) {
					continue;
				}
				byte[] file2 = Misc.readFileGzip(mapsFolder667 + mapGroundFileIds[i] + ".gz");
				if (file2 == null) {
					continue;
				}

				try {
					loadMaps(regionIds[i], new ByteStream(file1),
							new ByteStream(file2));
				} catch (Exception e) {
					System.out.println(
							"Error loading map region: " + regionIds[i] + ", " + mapObjectsFileIds[i] + ", " + mapGroundFileIds[i] + ", coords=" + (regionIds[i] >> 8 << 6) + "," + ((regionIds[i] & 0xff) << 6));
				}
			}
			
			addCastleWarsCustomClips();
			
			if (LOAD_OSRS_CLIPMAP) {
				for (MapIndex m : osrsMaps) {
					byte[] file1 = Misc.readFileGzip(mapsFolderOsrs + m.objects() + ".gz");
					if (file1 == null) {
						continue;
					}

					byte[] file2 = Misc.readFileGzip(mapsFolderOsrs + m.landscape() + ".gz");
					if (file2 == null) {
						continue;
					}

					try {
						if (m.getRegionId() > 0)
							loadMaps(m.getRegionId(), new ByteStream(file1), new ByteStream(file2));
					} catch (Exception e) {
						System.out.println(
								"Error loading map region: " + m.getRegionId() + ", " + m.objects() + ", " + m.landscape() + ", coords=" + (m.getRegionId() >> 8 << 6) + "," + ((m.getRegionId() & 0xff) << 6));
						/*File file = new File(mapsFolderOsrs + m.objects() + ".gz");
						file.delete();
						
						file = new File(mapsFolderOsrs + m.landscape() + ".gz");
						file.delete();*/
					}
				}
			}
			osrsMaps.clear();
			osrsMaps = null;
			regionIds = null;
			mapGroundFileIds = null;
			mapObjectsFileIds = null;
			System.out.println("[Region] Configuration has been loaded...");

//			finishLoadingMaps = true;
			if (COPY_MODELS) {
				try {
					for (int i : objectModels) {
						File model = new File("./667obj/"+i+".dat");
						if (model.exists()) {
							Files.copy(model, new File("./667objcopy/"+i));
						} else {
							System.out.println("missing model ID:"+i);
						}

					}
					System.out.println("done copying files.=========================================");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			objectModels.clear();
			objectModels = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final String mapsFolder667 = "./Data/world/map/";
	private static final String mapsFolderOsrs = "./Data/world/maposrs/";

	public static void loadCustomClips() {
		// add from X2943Y3840H0 to Y++
		
		addClippingForSolidObject(3076, 3510, 0, 2, 1, false); // table npc in edgeville general store
	}

	private static ArrayList<Integer> objectModels = new ArrayList<Integer>();
	
	private static void loadMaps(int regionId, ByteStream str1,
			ByteStream str2) {
		
		boolean isOsrsWildIn317 = osrsMapIn317(regionId);
		
		int absX = (regionId >> 8) << 6;
		int absY = (regionId & 0xff) << 6;
		byte[][][] someArray = new byte[4][64][64];
		for (int i = 0; i < 4; i++) {
			for (int i2 = 0; i2 < 64; i2++) {
				for (int i3 = 0; i3 < 64; i3++) {
					while (true) {
						int v = str2.getUByte();
						if (v == 0) {
							break;
						} else if (v == 1) {
							str2.skip(1);
							break;
						} else if (v <= 49) {
							str2.skip(1);
						} else if (v <= 81) {
							someArray[i][i2][i3] = (byte) (v - 49);
						}
					}
				}
			}
		}
		for (int i = 0; i < 4; i++) {
			for (int i2 = 0; i2 < 64; i2++) {
				for (int i3 = 0; i3 < 64; i3++) {
					if ((someArray[i][i2][i3] & 1) == 1) {
						int height = i;
						if ((someArray[1][i2][i3] & 2) == 2) {
							height--;
						}
						// if (height >= 0 && height <= 3) {
						addClipping(absX + i2, absY + i3, height & 3, 0x200000);
						// }
					}
				}
			}
		}
		int oIncr = -1;
		int incr;
		int objectId = -1;
		while ((incr = str1.readSmart2()) != 0) {
			oIncr += incr;
			objectId = oIncr;
			
			if (isOsrsWildIn317) {
				objectId += ObjectDef.OSRS_START;
			}
			
			int location = 0;
			int incr2;
			while ((incr2 = str1.getUSmart()) != 0) {
				location += incr2 - 1;
				int localX = (location >> 6 & 0x3f);
				int localY = (location & 0x3f);
				int height = location >> 12;
				int objectData = str1.getUByte();
				int type = objectData >> 2;
				int direction = objectData & 0x3;
				if (localX < 0 || localX >= 64 || localY < 0 || localY >= 64) {
					continue;
				}

				if ((someArray[1][localX][localY] & 2) == 2) {
					height--;
				}
				
				// if (height >= 0 && height <= 3) {
				addObject(objectId, absX + localX, absY + localY, height & 3,
						type, direction, true);
//				if (objectId >= 15 && objectId <= 30) {
//					System.out.println("object is in map!!!----------------------------- ID:"+objectId+ " X:"+(absX + localX)+" Y:"+(absY + localY));
//				}
				if (COPY_MODELS) {
					if (regionId == 12598) {
						/*ObjectDef def = ObjectDef.forID667(objectId);
						for (int i = 0; i < def.anIntArray773.length; i++) {
							if (!objectModels.contains(def.anIntArray773[i]))
								objectModels.add(def.anIntArray773[i]);
						}*/
					}
				}
				// }
			}
		}

	}
	
	public static boolean osrsMapIn317(int regionId) {
		switch (regionId) {
			case 10547: // Ardougne rooftop

			case 13617: //lms lobby
			case 13361: //lms lobby

			//wildy
			//case 11831:
			case 11832:
			case 11833:
			case 11834:
			case 11835:
			case 11836:
			case 11837:
			//case 12087:
			case 12088:
			case 12089:
			case 12090:
			case 12091:
			case 12092:
			case 12093:
			//case 12343:
			case 12344:
			case 12345:
			case 12346:
			case 12347:
			case 12348:
			case 12349:
			//case 12599:
			case 12600:
			case 12601:
			case 12602:
			case 12603:
			case 12604:
			case 12605:
			//case 12855:
			case 12856:
			case 12857:
			case 12858:
			case 12859:
			case 12860:
			case 12861:
			//case 13111:
			case 13112:
			case 13113:
			case 13114:
			case 13115:
			case 13116:
			case 13117:
			//case 13367:
			case 13368:
			case 13369:
			case 13370:
			case 13371:
			case 13372:
			case 13373:
				return true;

			default:
				return false;
		}
	}

	public static boolean objectExists(GameObject object) {
		RegionClip r = getRegion(object.getX(), object.getY());
		if (r == null) {
			return false;
		}
		int regionAbsX = (r.id >> 8) << 6;
		int regionAbsY = (r.id & 0xff) << 6;
		for (int i = 0; i < 4; i++) {
			if (r.gameObjects == null
					|| r.gameObjects[object.getZ() % 4] == null
					|| r.gameObjects[object.getZ() % 4][i] == null
					|| r.gameObjects[object.getZ() % 4][i][object.getX()
							- regionAbsX] == null
					|| r.gameObjects[object.getZ() % 4][i][object.getX()
							- regionAbsX][object.getY() - regionAbsY] == null) {
				return false;
			}
			if (r.gameObjects[object.getZ() % 4][i][object.getX()
					- regionAbsX][object.getY() - regionAbsY] != null) {
				if (r.gameObjects[object.getZ() % 4][i][object.getX()
						- regionAbsX][object.getY() - regionAbsY]
								.getId() == object.getId()) {
					return true;
				}
			}
		}

		return false;
	}
	
	public static boolean rayTraceBlocked(int x1, int y1, int x2, int y2, int z,
			boolean usingRange) {
		return rayTraceBlocked(x1, y1, x2, y2, z, usingRange, null);
	}

	public static boolean rayTraceBlocked(int x1, int y1, int x2, int y2, int z,
			boolean usingRange, DynamicRegionClip clipMap) { // LOS
		try {
			
			if (usingRange && x2 < x1) {
				x2 = x2^x1;
				x1 = x2^x1;
				x2 = x2^x1;
				
				y2 = y2^y1;
				y1 = y2^y1;
				y2 = y2^y1;
			}

			int dx = Math.abs(x2 - x1);
			int dy = Math.abs(y2 - y1);
			int sx = (x1 < x2) ? 1 : -1;
			int sy = (y1 < y2) ? 1 : -1;
			int err = dx - dy;
			int n = 1 + dx + dy;

			int x3 = x1;
			int y3 = y1;
			int dirX = 0, dirY = 0;
			
			for (; n > 0; --n) {

				x3 = x1; // first x
				y3 = y1; // first y

				if (x1 == x2 && y1 == y2) {
					break;
				}
				int e2 = 2 * err;
				if (e2 > -dy) {
					err = err - dy;
					x1 = x1 + sx;
				}
				if (e2 < dx) {
					err = err + dx;
					y1 = y1 + sy;
				}
				dirX = x1 - x3;
				dirY = y1 - y3;
				
				if (n > 2 && RegionClip.isBarricadeHere(x1, y1, z, clipMap)) {
					return true;
				}

				// NOTE: X1 Y1 is where we will move to, X3 Y3 is where we move from
				if (!PathFinder.canMove(x3, y3, z, dirX, dirY, clipMap)) {
					if (usingRange) {
//							System.out.println("x3:"+x3+" y3:"+y3+" x1:"+x1+" y1:"+y1+" dirX:"+dirX+" dirY:"+dirY+" test:"+(RegionClip.blockedShot(x3, y3, z, dirX, dirY)));
							if (!RegionClip.blockedShot(x3, y3, z, dirX, dirY, clipMap)) {
								continue;
							}
					} else { //melee
						if (RegionClip.getClipping(x1, y1, z, clipMap) == 2097152
								|| (!RegionClip.canStandOnSpot(x1, y1, z, clipMap) && PathFinder.canStepOffObject(x1, y1, z, dirX*-1, dirY*-1, clipMap)) /*|| RegionClip.getClipping(x1, y1, z) == 256*/) {
							continue;
						}
					}
					// System.out.println("True on "+x3+" "+y3);
					return true;
				}
			}
			// System.out.println("False");
			return false;

		} catch (Exception ex) {
			System.out.println(
					"Crashed at X1:" + x1 + " Y1:" + y1 + " Z:" + z + "  X2:"
							+ x2 + " Y2:" + y2 + " Using Ranged:" + usingRange);
			ex.printStackTrace();
			return false;
		}
	}

	public static boolean rayTraceRegionLocationBlocked(int x1, int y1, int x2,
			int y2, int z, boolean usingRange, DynamicRegionClip dynClip) { // LOS

		int dx = Math.abs(x2 - x1);
		int dy = Math.abs(y2 - y1);
		int sx = (x1 < x2) ? 1 : -1;
		int sy = (y1 < y2) ? 1 : -1;
		int err = dx - dy;
		int n = 1 + dx + dy;

		int x3 = x1;
		int y3 = y1;
		int dirX = 0, dirY = 0;

		for (; n > 0; --n) {

			x3 = x1; // first x
			y3 = y1; // first y

			if (x1 == x2 && y1 == y2) {
				break;
			}
			int e2 = 2 * err;
			if (e2 > -dy) {
				err = err - dy;
				x1 = x1 + sx;
			}
			if (e2 < dx) {
				err = err + dx;
				y1 = y1 + sy;
			}
			dirX = x1 - x3;
			dirY = y1 - y3;
			if (!PathFinder.canMove(x3, y3, z, dirX, dirY, dynClip)) {
				if (usingRange) {
					if (RegionClip.blockedShot(x3, y3, z, dirX, dirY, dynClip)) {
						// System.out.println("Poop2");
						continue;
					}
				}
				return true;
			}
		}

		return false;
	}
	
	
	public static Location rayTraceToBitLocation(int startX, int startY, int endX, int endY, int z, DynamicRegionClip clipMap, int mask) { // LOS
		try {
			
			int dx = Math.abs(endX - startX);
			int dy = Math.abs(endY - startY);
			int sx = (startX < endX) ? 1 : -1;
			int sy = (startY < endY) ? 1 : -1;
			int err = dx - dy;
			int n = 1 + dx + dy;

			int x3 = startX;
			int y3 = startY;
			
			for (; n > 0; --n) {

				x3 = startX; // first x
				y3 = startY; // first y

				if (startX == endX && startY == endY) {
					break;
				}
				int e2 = 2 * err;
				if (e2 > -dy) {
					err = err - dy;
					startX = startX + sx;
				}
				if (e2 < dx) {
					err = err + dx;
					startY = startY + sy;
				}
				
				if ((RegionClip.getClipping(startX, startY, z, clipMap) & mask) != 0) {
					return Location.create(startX, startY, z);
				}

			}
			return null;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public static boolean blockedByWall(Location targetLoc, Direction playerFace) {
		return blockedByWall(targetLoc, playerFace, null);
	}
	public static boolean blockedByWall(Location targetLoc, Direction playerFace, DynamicRegionClip clipMap) {
		int mask = 0;
		if (playerFace == Direction.NORTH) {
			mask = 0x20;
		} else if (playerFace == Direction.SOUTH) {
			mask =  0x2;
		} else if (playerFace == Direction.EAST) {
			mask =  0x80;
		} else if (playerFace == Direction.WEST) {
			mask =  0x8;
		} else if (playerFace == Direction.NORTH_WEST) {
			mask =  0x10;
		} else if (playerFace == Direction.NORTH_EAST) {
			mask =  0x40;
		} else if (playerFace == Direction.SOUTH_WEST) {
			mask =  0x4;
		} else if (playerFace == Direction.SOUTH_EAST) {
			mask =  0x1;
		}
		return ((RegionClip.getClipping(targetLoc.getX(), targetLoc.getY(), targetLoc.getZ(), clipMap) & mask) != 0);
	}

	public static void removeClip(int x, int y, int height) { // Ownerblades
																// snippet for
																// removing
																// clips in one
																// spot
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;
		int regionAbsX = (regionId >> 8) << 6;
		int regionAbsY = (regionId & 0xff) << 6;
		if (regionIdTable[regionId] != null) {
			regionIdTable[regionId].clips[height % 4][x - regionAbsX][y
					- regionAbsY] = 0;
			
			if (!finishLoadingMaps) {
				if (regionIdTable[regionId].originalClip[height % 4] == null) {
					regionIdTable[regionId].originalClip[height % 4] = new int[64][64];
				}
				regionIdTable[regionId].originalClip[height % 4][x - regionAbsX][y - regionAbsY] = 0;
			}
		}
	}
	
	private static void removeClipping(int x, int y, int height, int shift) {
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;
		if (regionId < 0 || regionId >= regionIdTable.length) {
			//System.out.println("Unable clipping regionID:"+regionId+" X:"+x+" Y:"+y+" Z:"+height);
			return;
		}
		RegionClip region = regionIdTable[regionId];
		if (region == null) {
			return;
		}

		region.removeClip(x, y, height, shift);
		// System.out.println("rID:"+regionId+" x:"+x+" y:"+y+" z:"+height);
	}

	public static void removeClippingForSolidObject(int x, int y, int height,
			int xLength, int yLength, boolean flag) {
		int clipping = 256;
		if (flag) {
			clipping += 0x20000;
		}
		for (int i = x; i < x + xLength; i++) {
			for (int i2 = y; i2 < y + yLength; i2++) {
				removeClipping(i, i2, height, clipping);
			}
		}
	}

	public static void removeClippingForVariableObject(int x, int y, int height,
			int type, int direction, boolean flag) {
		if (type == 0) {
			if (direction == 0) {
				removeClipping(x, y, height, 128);
				removeClipping(x - 1, y, height, 8);
			} else if (direction == 1) {
				removeClipping(x, y, height, 2);
				removeClipping(x, y + 1, height, 32);
			} else if (direction == 2) {
				removeClipping(x, y, height, 8);
				removeClipping(x + 1, y, height, 128);
			} else if (direction == 3) {
				removeClipping(x, y, height, 32);
				removeClipping(x, y - 1, height, 2);
			}
		} else if (type == 1 || type == 3) {
			if (direction == 0) {
				removeClipping(x, y, height, 1);
				removeClipping(x - 1, y, height, 16);
			} else if (direction == 1) {
				removeClipping(x, y, height, 4);
				removeClipping(x + 1, y + 1, height, 64);
			} else if (direction == 2) {
				removeClipping(x, y, height, 16);
				removeClipping(x + 1, y - 1, height, 1);
			} else if (direction == 3) {
				removeClipping(x, y, height, 64);
				removeClipping(x - 1, y - 1, height, 4);
			}
		} else if (type == 2) {
			if (direction == 0) {
				removeClipping(x, y, height, 130);
				removeClipping(x - 1, y, height, 8);
				removeClipping(x, y + 1, height, 32);
			} else if (direction == 1) {
				removeClipping(x, y, height, 10);
				removeClipping(x, y + 1, height, 32);
				removeClipping(x + 1, y, height, 128);
			} else if (direction == 2) {
				removeClipping(x, y, height, 40);
				removeClipping(x + 1, y, height, 128);
				removeClipping(x, y - 1, height, 2);
			} else if (direction == 3) {
				removeClipping(x, y, height, 160);
				removeClipping(x, y - 1, height, 2);
				removeClipping(x - 1, y, height, 8);
			}
		}
		if (flag) {
			if (type == 0) {
				if (direction == 0) {
					removeClipping(x, y, height, 65536);
					removeClipping(x - 1, y, height, 4096);
				} else if (direction == 1) {
					removeClipping(x, y, height, 1024);
					removeClipping(x, y + 1, height, 16384);
				} else if (direction == 2) {
					removeClipping(x, y, height, 4096);
					removeClipping(x + 1, y, height, 65536);
				} else if (direction == 3) {
					removeClipping(x, y, height, 16384);
					removeClipping(x, y - 1, height, 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (direction == 0) {
					removeClipping(x, y, height, 512);
					removeClipping(x - 1, y + 1, height, 8192);
				} else if (direction == 1) {
					removeClipping(x, y, height, 2048);
					removeClipping(x + 1, y + 1, height, 32768);
				} else if (direction == 2) {
					removeClipping(x, y, height, 8192);
					removeClipping(x + 1, y + 1, height, 512);
				} else if (direction == 3) {
					removeClipping(x, y, height, 32768);
					removeClipping(x - 1, y - 1, height, 2048);
				}
			} else if (type == 2) {
				if (direction == 0) {
					removeClipping(x, y, height, 66560);
					removeClipping(x - 1, y, height, 4096);
					removeClipping(x, y + 1, height, 16384);
				} else if (direction == 1) {
					removeClipping(x, y, height, 5120);
					removeClipping(x, y + 1, height, 16384);
					removeClipping(x + 1, y, height, 65536);
				} else if (direction == 2) {
					removeClipping(x, y, height, 20480);
					removeClipping(x + 1, y, height, 65536);
					removeClipping(x, y - 1, height, 1024);
				} else if (direction == 3) {
					removeClipping(x, y, height, 81920);
					removeClipping(x, y - 1, height, 1024);
					removeClipping(x - 1, y, height, 4096);
				}
			}
		}
	}

	public static void removeGameObject(GameObject o) {
		o.setActive(false);
		RegionClip r = getRegion(o.getX(), o.getY());
		if (r != null) {
			r.removeGameObject(o.getX(), o.getY(), o.getZ(), o.getType());// r.gameObject.remove(o);
			if (isDwarfCannon(o.getId())) {
				return; // do no add dwarf cannon to clipmap
			}
			if (o.getId() > 0) {
				removeObjectClipping(o.getId(), o.getX(), o.getY(), o.getZ(),
						o.getOrientation(), o.getType());
			}
		}
	}

	public static void removeObjectClipping(int id, int x, int y, int height,
			int direction, int type) {
		ObjectDef def = ObjectDef.getObjectDef(id);
		if (def == null) {
			System.out.println("ID: " + id + " HAS NO DEF");
			return;
		}
		if (id == 734 || id == 733 || id == 10734 || id == 10733) {
			type = 0;
		}
		int xLength = def.xLength(direction);
		int yLength = def.yLength(direction);
		if (type == 22) {
			removeClipping(x, y, height, 0x200000);
		} else if (type >= 9) {
			removeClippingForSolidObject(x, y, height, xLength, yLength,
					def.clippedProjectile());
		} else if (type >= 0 && type <= 3) {
			removeClippingForVariableObject(x, y, height, type, direction,
					def.clippedProjectile());
		}
	}

	public static void revertClipping(int x, int y, int height) {
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;

		if (regionIdTable[regionId] == null) {
			// System.out.println("missing region "+regionId);
			return;
		}
		regionIdTable[regionId].revertClip(x, y, height);
	}

	private int id;

	private int[][][] clips = new int[4][][];

	private int[][][] originalClip = new int[4][][];

	private boolean members = false;

	private byte[][][] barricade = new byte[4][][];

	private GameObject[][][][] gameObjects = new GameObject[4][][][];

	public RegionClip(int id) {
		this.id = id;
	}

	public RegionClip(int id, boolean members) {
		this.id = id;
		this.members = members;
	}

	private void addClip(int x, int y, int height, int shift) {
		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		height = height % 4;
		if (clips[height] == null) {
			clips[height] = new int[64][64];
		}
		try {
			clips[height][x - regionAbsX][y - regionAbsY] |= shift;
		} catch (Exception ex) {
			System.out.println("z1:"+height+" id:"+id);
		}

		if (!finishLoadingMaps) {
			if (originalClip[height] == null) {
				originalClip[height] = new int[64][64];
			}
			originalClip[height][x - regionAbsX][y - regionAbsY] |= shift;
		}
	}

	private void addGameObject(GameObject o, int x, int y, int height,
			int type) {
		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		height = height % 4;
		if (gameObjects[height] == null) {
			gameObjects[height] = new GameObject[4][64][64];
		}

		if (type > 23) {
			type = 10;
		}

		gameObjects[height][Misc.OBJECT_SLOTS[type]][x - regionAbsX][y - regionAbsY] = o;
	}

	private int getBarricade(int x, int y, int height) {
		height = height % 4;
		if (barricade[height] == null) {
			return 0;
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		return barricade[height][x - regionAbsX][y - regionAbsY];
	}
	
	private void setBarricade(int x, int y, int height, boolean barricadeHere) {
		height = height % 4;
		if (barricade[height] == null) {
			if (!barricadeHere)
				return;
			barricade[height] = new byte[64][64];
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		barricade[height][x - regionAbsX][y - regionAbsY] = (byte) (barricadeHere ? 1 : 0);
	}
	
	public static void setBarricadeStatus(int x, int y, int z, boolean barricadeHere, DynamicRegionClip clipmap) {
		if (clipmap == null) {
			setBarricadeStatus(x, y, z, barricadeHere);
		} else {
			if (barricadeHere)
				clipmap.addClipping(x, y, z, BARRICADE_TILE);
			else
				clipmap.removeClipping(x, y, z, BARRICADE_TILE);
		}
	}
	
	public static void setBarricadeStatus(int x, int y, int height, boolean barricadeHere) {
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;

		if (regionId < 0 || regionId >= regionIdTable.length || regionIdTable[regionId] == null) {
			return;
		}

		regionIdTable[regionId].setBarricade(x, y, height, barricadeHere);
	}
	
	public static boolean isBarricadeHere(int x, int y, int z, DynamicRegionClip clipmap) {
		if (clipmap == null) {
			return isBarricadeHere(x, y, z);
		}
		return (clipmap.getClipping(x, y, z) & BARRICADE_TILE) != 0;
	}
	
	public static boolean isBarricadeHere(int x, int y, int height) {
		height = height & 3;
		int regionX = x >> 6;
		int regionY = y >> 6;
		int regionId = (regionX << 8) + regionY;

		if (regionId < 0 || regionId >= regionIdTable.length || regionIdTable[regionId] == null) {
			return false;
		}

		RegionClip clip = regionIdTable[regionId];
		
		if (clip == null) {
		    return false;
		}
		
		return clip.getBarricade(x, y, height) == 1;
	}

	public boolean blockedEast(int x, int y, int z) {
		return (getClipping(x + 1, y, z) & 0x1280180) != 0;
	}

	public boolean blockedNorth(int x, int y, int z) {
		return (getClipping(x, y + 1, z) & 0x1280120) != 0;
	}

	public boolean blockedNorthEast(int x, int y, int z) {
		return (getClipping(x + 1, y + 1, z) & 0x12801e0) != 0;
	}

	public boolean blockedNorthWest(int x, int y, int z) {
		return (getClipping(x - 1, y + 1, z) & 0x1280138) != 0;
	}

	public boolean blockedSouth(int x, int y, int z) {
		return (getClipping(x, y - 1, z) & 0x1280102) != 0;
	}

	public boolean blockedSouthEast(int x, int y, int z) {
		return (getClipping(x + 1, y - 1, z) & 0x1280183) != 0;
	}

	public boolean blockedSouthWest(int x, int y, int z) {
		return (getClipping(x - 1, y - 1, z) & 0x128010e) != 0;
	}

	public boolean blockedWest(int x, int y, int z) {
		return (getClipping(x - 1, y, z) & 0x1280108) != 0;
	}

	private int getClip(int x, int y, int height) {
		height = height % 4;
		if (clips[height] == null) {
			return 0x100;
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		return clips[height][x - regionAbsX][y - regionAbsY];
	}
	
	private void setClip(Location loc, int clip) {
		int z = loc.getZ() % 4;
		if (clips[z] == null) {
			return;
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		clips[z][loc.getX() - regionAbsX][loc.getY() - regionAbsY] = clip;
	}

	public int id() {
		return id;
	}

	public static void loadNewMap(int index, int regionId, int landscapeId,
			int objectsMapId) {

		if (index >= regionIds.length) {
			System.out.println("not enough map size!");
			return;
		}
		regionIds[index] = regionId;
		mapGroundFileIds[index] = (short) landscapeId;
		mapObjectsFileIds[index] = (short) objectsMapId;
	}

	public boolean members() {
		return members;
	}

	private void removeClip(int x, int y, int height, int shift) {
		height = height % 4;
		if (clips == null || clips[height] == null) {
			return;
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		clips[height][x - regionAbsX][y - regionAbsY] &= 0xFFFFFFF - shift;

		if (!finishLoadingMaps) {
			if (originalClip[height] == null) {
				originalClip[height] = new int[64][64];
			}
			originalClip[height][x - regionAbsX][y - regionAbsY] &= 0xFFFFFFF - shift;
		}
	}

	private void removeGameObject(int x, int y, int height, int type) {
		height = height % 4;
		if (gameObjects[height] == null) {
			return;
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		gameObjects[height][Misc.OBJECT_SLOTS[type]][x - regionAbsX][y - regionAbsY] = null;
	}

	private void revertClip(int x, int y, int height) {
		height = height % 4;
		if (clips[height] == null) {
			System.out.println("Clip is null");
			return;
		}

		if (originalClip[height] == null) {
			System.out.println("originalClip is null");
			return;
		}

		int regionAbsX = (id >> 8) << 6;
		int regionAbsY = (id & 0xff) << 6;
		int x1 = x - regionAbsX;
		int y1 = y - regionAbsY;

		clips[height][x1][y1] = originalClip[height][x1][y1];
	}
	
	public static void removeNpcTile(int x, int y, int z, DynamicRegionClip dynClip) {
		if (dynClip != null) {
			dynClip.removeNpcTile(x, y, z);
		} else {
			removeClipping(x, y, z, NPC_TILE);
		}
	}
	
	public static void addNpcTile(int x, int y, int z,  DynamicRegionClip dynClip) {
		if (dynClip != null) {
			dynClip.addNpcTile(x, y, z);
		} else {
			addClipping(x, y, z, NPC_TILE);
		}
	}
	
	public static boolean npcOnTile(int x, int y, int z, DynamicRegionClip dynClip) {
		return (getClipping(x, y, z, dynClip) & NPC_TILE) != 0;
	}
	
	public static boolean canNpcStandOnNextTile(int dirX, int dirY, int npcSize, Location npcLoc, DynamicRegionClip dynClip) {
		if (npcSize <= 1) {
			return !npcOnTile(npcLoc.getX()+dirX, npcLoc.getY()+dirY, npcLoc.getZ(), dynClip);
		}
		if (dirX == 0) { // if moving north/south
			if (dirY > 0) { // moving north
				for (int i = 0; i < npcSize; i++) {
					if (npcOnTile(npcLoc.getX()+i, npcLoc.getY()+npcSize, npcLoc.getZ(), dynClip))
						return false;
				}
			} else { // moving south
				for (int i = 0; i < npcSize; i++) {
					if (npcOnTile(npcLoc.getX()+i, npcLoc.getY()-1, npcLoc.getZ(), dynClip))
						return false;
				}
			}
		} else if (dirY == 0) {
			if (dirX > 0) { // moving east
				for (int i = 0; i < npcSize; i++) {
					if (npcOnTile(npcLoc.getX()+npcSize, npcLoc.getY()+i, npcLoc.getZ(), dynClip))
						return false;
				}
			} else { // moving west
				for (int i = 0; i < npcSize; i++) {
					if (npcOnTile(npcLoc.getX()-1, npcLoc.getY()+i, npcLoc.getZ(), dynClip))
						return false;
				}
			}
		} else { // diagonal
			int moveX = dirX; // move check X
			int moveY = dirY; // move check Y
			if (dirX > 0)
				moveX = npcSize;
			if (dirY > 0)
				moveY = npcSize;
			
			int offset = npcSize-1;

			if (npcOnTile(npcLoc.getX()+moveX, npcLoc.getY()+moveY, npcLoc.getZ(), dynClip))
				return false;
			
			for (int i = 0; i < npcSize-1; i++) {
				if (npcOnTile(npcLoc.getX()+i+(dirX > 0 ? offset : 0), npcLoc.getY()+moveY, npcLoc.getZ(), dynClip))
					return false;
				if (npcOnTile(npcLoc.getX()+moveX, npcLoc.getY()+i+(dirY > 0 ? offset : 0), npcLoc.getZ(), dynClip))
					return false;
			}
		}
		return true;
	}
	
	private static void addCastleWarsCustomClips() {
		addClipping(2415, 3074, 0, PROJECTILE_SOUTH_BLOCKED);
		addClipping(2416, 3074, 0, PROJECTILE_SOUTH_BLOCKED);
		addClipping(2383, 3133, 0, PROJECTILE_NORTH_BLOCKED);
		addClipping(2384, 3133, 0, PROJECTILE_NORTH_BLOCKED);
		removeClip(2264, 3070, 0);
		removeClip(2265, 3069, 0);
	}

}
