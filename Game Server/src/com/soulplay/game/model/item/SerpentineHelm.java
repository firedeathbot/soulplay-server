package com.soulplay.game.model.item;

import java.util.HashMap;
import java.util.Map;

public enum SerpentineHelm {

	SERP_HELM(30002, 30003, 11000),
	MAGMA_HELM(30314, 30315, 11000),
	TANZANITE_HELM(30316, 30317, 11000);
	
	private final int unchargedId, chargedId, maxCharges;
	
	private SerpentineHelm(int unch, int charg, int maxCharges) {
		this.unchargedId = unch;
		this.chargedId = charg;
		this.maxCharges = maxCharges;
	}

	public int getUnchargedId() {
		return unchargedId;
	}

	public int getChargedId() {
		return chargedId;
	}

	public int getMaxCharges() {
		return maxCharges;
	}
	
	private static final Map<Integer, SerpentineHelm> map = new HashMap<>();
	
	public static void init() {
		for (SerpentineHelm helm : SerpentineHelm.values()) {
			map.put(helm.getUnchargedId(), helm);
			map.put(helm.getChargedId(), helm);
		}
	}
	
	public static SerpentineHelm getHelm(int id) {
		return map.get(id);
	}
	
}
