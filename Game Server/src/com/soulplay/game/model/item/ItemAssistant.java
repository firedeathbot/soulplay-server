package com.soulplay.game.model.item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.bot.BotClient;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.combat.data.FightExp;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.items.Food.FoodToWin;
import com.soulplay.content.items.RangeWeapon;
import com.soulplay.content.items.WeaponType;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.itemdata.CombatTab;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.items.useitem.DonationChest;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.combat.melee.MeleeData;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.untradeables.UntradeableManager;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.content.shops.ShopPrice;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.definition.EquipBonusIds;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.item.definition.SpecialsInterfaceInfo;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.map.travel.zone.Zone;
import com.soulplay.net.slack.SlackMessage;
import com.soulplay.util.Misc;

/**
 * Indicates Several Usage Of Items
 *
 * @author Sanity Revised by Shawn Notes by Shawn
 */
public class ItemAssistant {

    @FunctionalInterface
    public interface ItemAction {

	void perform(int itemID, int amount);
    }
    
	public static final int[][] itemsToAddYT = { { 995, 80000000 }, { 11848, 10 }, { 4151, 15 }, { 23639, 2 }, { 6585, 10 },
		{ 11732, 15 }, { 7462, 10 }, { 8839, 1 }, { 8840, 1 }, { 8842, 1 }, { 11663, 1 }, { 11664, 1 },
		{ 11665, 1 }, { 13429, 2 }, { 11846, 6 }, { 11852, 6 }, { 4740, 2000 }, { 15272, 5000 }, { 11694, 1 },
		{ 14484, 1 }, { 19780, 1 } };
	
	public static void addYoutuberSetupBank(Client c) {
		for (int[] element : itemsToAddYT) {
			c.getItems().addItemToBank(element[0], element[1]);
		}
	}

    /**
     * Handles tradable items.
     */
    public static boolean tradeable(int itemId) {
	// for (int j = 0; j < Config.ITEM_TRADEABLE.length; j++) {
	// if (itemId == Config.ITEM_TRADEABLE[j])
	// return false;
	// }
	if (!ItemProjectInsanity.itemIsTradeable(itemId)) {
	    return false;
	}
	if (DBConfig.DUNG_REWARDS.contains(itemId)) {
	    return false;
	}
	return true;
    }

    private Client c;

    /**
     * Trimmed and untrimmed skillcapes.
     */
    public static final int[][] skillcapes = { { 9747, 9748 }, // Attack
	    { 9753, 9754 }, // Defence
	    { 9750, 9751 }, // Strength
	    { 9768, 9769 }, // Hitpoints
	    { 9756, 9757 }, // Range
	    { 9759, 9760 }, // Prayer
	    { 9762, 9763 }, // Magic
	    { 9801, 9802 }, // Cooking
	    { 9807, 9808 }, // Woodcutting
	    { 9783, 9784 }, // Fletching
	    { 9798, 9799 }, // Fishing
	    { 9804, 9805 }, // Firemaking
	    { 9780, 9781 }, // Crafting
	    { 9795, 9796 }, // Smithing
	    { 9792, 9793 }, // Mining
	    { 9774, 9775 }, // Herblore
	    { 9771, 9772 }, // Agility
	    { 9777, 9778 }, // Thieving
	    { 9786, 9787 }, // Slayer
	    { 9810, 9811 }, // Farming
	    { 9765, 9766 } // Runecraft
    };

    /**
     * Broken barrows items.
     */
    public static final int[][] brokenBarrows = { { 4708, 4860 }, { 4710, 4866 }, { 4712, 4872 }, { 4714, 4878 }, { 4716, 4884 },
	    { 4720, 4896 }, { 4718, 4890 }, { 4720, 4896 }, { 4722, 4902 }, { 4732, 4932 }, { 4734, 4938 },
	    { 4736, 4944 }, { 4738, 4950 }, { 4724, 4908 }, { 4726, 4914 }, { 4728, 4920 }, { 4730, 4926 },
	    { 4745, 4956 }, { 4747, 4926 }, { 4749, 4968 }, { 4751, 4794 }, { 4753, 4980 }, { 4755, 4986 },
	    { 4757, 4992 }, { 4759, 4998 } };

    public static final String[] BONUS_NAMES = { "Stab", "Slash", "Crush", "Magic", "Range", "Stab", "Slash", "Crush", "Magic",
	    "Range", "Strength", "Prayer" };

    public ItemAssistant(Client client) {
	this.c = client;
    }
    
    public boolean wildyProhibitedItem(int itemId) {
    	if (itemId > 0) {
    		if (itemId == 20857) {
    			c.sendMessage("You cannot store sir owen's longsword.");
    			return true;
    		}

    		if (c.isHybridPiece(itemId)) {
    			c.sendMessage("You cannot place in hybrid pieces.");
    			return true;
    		}
    	} else {
    		if (c.playerEquipment[PlayerConstants.playerWeapon] == 20857 || c.getItems().playerHasItem(20857)) {
    			c.getPA().movePlayer(c.getX(), 3519, 0);
    			c.sendMessage("You cannot enter the wilderness with sir owen's longsword.");
    			c.getCombat().resetPlayerAttack();
    			c.applyFreeze(2);
    			return true;
    		}

    		if (c.hasHybridGear()) {
    			c.getPA().movePlayer(c.getX(), 3519, 0);
    			c.sendMessage("You cannot enter the wilderness with hybrid gear.");
    			c.getCombat().resetPlayerAttack();
    			c.applyFreeze(2);
    			return true;
    		}
    	}
    	return false;
    }

    public void showInventory(Player other) {
		c.getPacketSender().itemsOnInterface(3214, other.playerItems, other.playerItemsN);
		c.getPacketSender().setSidebarInterfaceAndRemoveButtons(3213);
		c.attr().put("viewing_inventory", true);
	}

	public int getInventoryMaximumAdd(Item item) {
		if (item.isStackable()) {
			int itemId = item.getId();
			if (playerHasItem(itemId, 1)) {
				return Integer.MAX_VALUE - getItemAmount(itemId);
			}

			return freeSlots() > 0 ? Integer.MAX_VALUE : 0;
		}

		return freeSlots();
	}

	public boolean hasInventorySpaceFor(Item item) {
		return item.getAmount() <= getInventoryMaximumAdd(item);
	}

    public boolean addItem(int itemId, int amount) {
    	return addItem(itemId, amount, false, null);
    }

    public boolean addItem(int itemId, int amount, boolean dropItem) {
    	return addItem(itemId, amount, dropItem, dropItem ? "<col=ff0000>Your item has been dropped on the ground since you have no room." : null);
    }

    public boolean addItem(int itemId, int amount, boolean dropItem, String dropMessage) {
    	// synchronized(c) {

    	// c.addItemLength++;
    	//
    	// if (c.addItemLength > 50) {
    	// System.out.println(c.getName()+" is adding too much items
    	// ID:"+itemId+" amount:"+amount+"
    	// MAC:"+c.UUID+" IP:"+c.connectedFrom);
    	// }

    	if (amount < 1) {
    		// amount = 1;
    		return false;
    	}
    	if (itemId <= 0) {
    		return false;
    	}
    	if (itemId == CastleWars.SARA_BANNER || itemId == CastleWars.ZAMMY_BANNER) {
    		CastleWars.dropFlag(c, itemId);
    		return true;
    	}

		if (MagicalCrate.pickingUpCrateInWildAsItem(c, itemId))
			return true;
    	
    	final boolean canStack = ItemProjectInsanity.itemStackable[itemId] && playerHasItem(itemId, 1);

    	if ((ItemProjectInsanity.itemStackable[itemId] && (canStack || freeSlots() >= 1))
    			|| (!ItemProjectInsanity.itemStackable[itemId] && freeSlots() > 0)) {
    		
    		final int inventoryItemId = itemId + 1;

    		if (ItemProjectInsanity.itemStackable[itemId]) {
    			if (canStack) {
    				for (int i = 0; i < c.playerItems.length; i++) {
    					if (c.playerItems[i] == inventoryItemId) {
    						long newAmount = (long)c.playerItemsN[i] + (long)amount;
    						if (newAmount > -1 && newAmount <= Config.MAXITEM_AMOUNT) {
    							c.playerItemsN[i] += amount;
    						} else {
    							c.playerItemsN[i] = (int)Config.MAXITEM_AMOUNT;
    							if (dropItem) {
    								GameItem item = new GameItem(itemId, (int) (newAmount - Config.MAXITEM_AMOUNT));
    				    			ItemHandler.createGroundItem(c, item, c.getCurrentLocation());
    				    			if (dropMessage != null) {
    				    				c.sendMessage(dropMessage);
    				    			}
    							}
    						}
    						// updateInventory = true;
    						c.setUpdateInvInterface(3214); // resetItems(3214);
    						return true;
    					}
    				}
    			} else { // find new slot
    				for (int i = 0; i < c.playerItems.length; i++) {
        				if (c.playerItems[i] <= 0) {
        					c.playerItems[i] = inventoryItemId;
        					c.playerItemsN[i] = amount;
        					c.setUpdateInvInterface(3214); // resetItems(3214);
        					return true;
        				}
        			}
    			}
    		} else { //non stackables
    			for (int i = 0; i < c.playerItems.length; i++) {
    				if (c.playerItems[i] <= 0) {
    					c.playerItems[i] = inventoryItemId;
    					c.playerItemsN[i] = 1;
    					c.setUpdateInvInterface(3214); // resetItems(3214);
    					if (amount > 1) {
    						c.getItems().addItem(itemId, amount - 1, dropItem, dropMessage);
    					}
    					return true;
    				}
    			}
    		}
    		return false;
    	} else {
    		// updateInventory = true;
    		c.setUpdateInvInterface(3214); // resetItems(3214);
    		if (c.isBot()) {
    			System.out.println(c.getName() + " [BOT] does not have enough space in the inventory.");
    		} else {
    			c.sendMessage("Not enough space in your inventory.");
    		}
    		if (dropItem) {
    			GameItem item = new GameItem(itemId, amount);
    			ItemHandler.createGroundItem(c, item, c.getCurrentLocation());
    			c.sendMessage("<col=ff0000>Your item has been dropped on the ground since you have no room.");
    			return true;
    		}
    		return false;
    	}
    	// }
    }

    public void replaceItemSlot(int newId, int slot) {
    	c.playerItems[slot] = newId + 1;
    	c.setUpdateInvInterface(3214);
    }

    /**
     * Adds an item to the bank without checking if the player has it in there
     * inventory
     *
     * @param itemId
     *            the id of the item were banking
     * @param amount
     *            amount of items to bank
     */
    public boolean addItemToBank(int itemId, int amount) {
    	int tab = 0;
    	if (amount < 1) {
    		return false;
    	}
    	
    	if (c.getZones().isInDmmLobby())
			return false;
    	
    	ItemDef def = ItemDef.forID(itemId);
    	if (def == null) {
    		c.sendMessage("Item Definition is null. Report to developer.");
    		return false;
    	}

    	if (def.itemIsInNotePosition) {
    		itemId = def.getNoteId();
    	}
    	
    	if (c.isBanking) {
    		tab = getTabforItem(itemId);
    	} else {
    		tab = findTabForItem(itemId);
    	}

    	int bankedAmount = getBankItemAmountInTab(itemId, tab);

    	if (bankedAmount > 0) {
    		if (((long)bankedAmount + amount) > Integer.MAX_VALUE) {
    			c.sendMessage("You are attempting to store more than the bank can hold.");
    			return false;
    		}
    	}

    	if (tab == 0) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems0[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems0[i] == itemId + 1 && ((long)c.bankItems0N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems0[i] = itemId + 1;
    				c.bankItems0N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems1[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems1[i] == itemId + 1 && ((long)c.bankItems1N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems1[i] = itemId + 1;
    				c.bankItems1N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems2[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems2[i] == itemId + 1 && ((long)c.bankItems2N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems2[i] = itemId + 1;
    				c.bankItems2N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems3[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems3[i] == itemId + 1 && ((long)c.bankItems3N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems3[i] = itemId + 1;
    				c.bankItems3N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems4[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems4[i] == itemId + 1 && ((long)c.bankItems4N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems4[i] = itemId + 1;
    				c.bankItems4N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems5[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems5[i] == itemId + 1 && ((long)c.bankItems5N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems5[i] = itemId + 1;
    				c.bankItems5N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems6[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems6[i] == itemId + 1 && ((long)c.bankItems6N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems6[i] = itemId + 1;
    				c.bankItems6N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems7[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems7[i] == itemId + 1 && ((long)c.bankItems7N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems7[i] = itemId + 1;
    				c.bankItems7N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems8[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems8[i] == itemId + 1 && ((long)c.bankItems8N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems8[i] = itemId + 1;
    				c.bankItems8N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 1) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems1[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems1[i] == itemId + 1 && ((long)c.bankItems1N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems1[i] = itemId + 1;
    				c.bankItems1N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 2) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems2[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems2[i] == itemId + 1 && ((long)c.bankItems2N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems2[i] = itemId + 1;
    				c.bankItems2N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 3) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems3[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems3[i] == itemId + 1 && ((long)c.bankItems3N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems3[i] = itemId + 1;
    				c.bankItems3N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 4) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems4[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems4[i] == itemId + 1 && ((long)c.bankItems4N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems4[i] = itemId + 1;
    				c.bankItems4N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 5) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems5[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems5[i] == itemId + 1 && ((long)c.bankItems5N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems5[i] = itemId + 1;
    				c.bankItems5N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 6) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems6[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems6[i] == itemId + 1 && ((long)c.bankItems6N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems6[i] = itemId + 1;
    				c.bankItems6N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 7) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems7[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems7[i] == itemId + 1 && ((long)c.bankItems7N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems7[i] = itemId + 1;
    				c.bankItems7N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	} else if (tab == 8) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE - 2; i++) {
    			if ((c.bankItems8[i] <= 0 && bankedAmount == 0)
    					|| c.bankItems8[i] == itemId + 1 && ((long)c.bankItems8N[i] + amount) <= Integer.MAX_VALUE) {
    				c.bankItems8[i] = itemId + 1;
    				c.bankItems8N[i] += amount;
    				resetBank();
    				return true;
    			}
    		}
    	}
    	c.sendMessage("Unable to bank item.");
    	return false;
    }

    public boolean addOrBankItem(int itemID, int amount) {
    	if (freeSlots() < amount) {
    		return addItemToBank(itemID, amount);
    	}
    	return addItem(itemID, amount);
    }

	public boolean addOrBankOrDropItem(int itemId, int amount) {
		return addOrBankOrDropItem(itemId, amount, null, null);
	}

	public boolean addOrBankOrDropItem(int itemId, int amount, String bankMessage, String dropMessage) {
		int slotsCheck;
		if (ItemProjectInsanity.itemIsStackable(itemId)) {
			slotsCheck = getItemAmount(itemId) > 0 ? 0 : 1;
		} else {
			slotsCheck = amount;
		}

		if (freeSlots() < slotsCheck) {
			if (addItemToBank(itemId, amount)) {
				if (bankMessage != null) {
					c.sendMessage(bankMessage);
				}

				return true;
			}

			ItemHandler.createGroundItem(c, itemId, c.getX(), c.getY(), c.getHeightLevel(), amount);
			if (dropMessage != null) {
				c.sendMessage(dropMessage);
			}
			return false;
		}

		return addItem(itemId, amount, true, dropMessage);
	}

    /**
     * Adds special attack bar to special attack weapons. Removes special attack
     * bar to weapons that do not have special attacks.
     * @param refreshWeapon TODO
     **/
    public void addSpecialBar(boolean refreshWeapon) {
	if (c.isBot()) {
	    return;
	}
	ItemDefinition def = ItemDefinition.forId(c.playerEquipment[PlayerConstants.playerWeapon]);
	if (def == null) {
	    return;
	}
	SpecialsInterfaceInfo spec = def.getSpecInfo();
	if (spec == null) { // TODO: set what kind of non spec weapon it is
			    // instaed of spamming them
			    // with all interfaces that have no spec bars
		c.getPacketSender().sendFrame171(1, 7674); // spear
	    c.getPacketSender().sendFrame171(1, 7624); // mace interface
	    c.getPacketSender().sendFrame171(1, 7474); // hammer, gmaul
	    c.getPacketSender().sendFrame171(1, 7499); // axe
	    c.getPacketSender().sendFrame171(1, 7549); // bow interface
	    c.getPacketSender().sendFrame171(1, 7574); // sword interface
	    c.getPacketSender().sendFrame171(1, 7599); // scimmy sword interface, for most swords
	    c.getPacketSender().sendFrame171(1, 7649); // throwing axes/javelins
	    c.getPacketSender().sendFrame171(1, 8493); // hally
	    c.getPacketSender().sendFrame171(1, 12323); // whip interface
	    c.getPacketSender().sendFrame171(1, 6117); // staffs
	    return;
	} else {
	    c.getPacketSender().sendFrame171(0, spec.getFrame171SubFrame());
	    specialAmount(c.playerEquipment[PlayerConstants.playerWeapon], c.specAmount, spec.getSpecBarId(), refreshWeapon);
	    return;
	}

    }

    /**
     * Voided items. (Not void knight items..)
     *
     * @param itemId
     */
    public void addToVoidList(int itemId) {
	switch (itemId) {
	case 2518:
	    c.voidStatus[0]++;
	    break;
	case 2520:
	    c.voidStatus[1]++;
	    break;
	case 2522:
	    c.voidStatus[2]++;
	    break;
	case 2524:
	    c.voidStatus[3]++;
	    break;
	case 2526:
	    c.voidStatus[4]++;
	    break;
	}
    }

    public void bankAllBoBToBank() {
	if (c.burdenedItems == null) {
	    c.sendMessage("There is nothing to deposit from BoB.");
	    return;
	}
	c.sendMessage("Banking Beast of Burden items.");
	for (int i = 0; i < c.burdenedItems.length; i++) {
	    int itemId = c.burdenedItems[i];
	    if (itemId > 0 && c.getItems().addItemToBank(itemId - 1, 1)) {
		c.burdenedItems[i] = 0;
	    }
	}
    }

    public void bankContainer(int[] playerItems, int[] playerItemsN, int modifier, ItemAction removeAction,
    		ItemAction successAction) {

    	for (int i = 0; i < playerItems.length; i++) {
    		if (playerItems[i] > 0 && playerItemsN[i] > 0) {

    			if (c.getPA().getBankItems(c.bankingTab) >= Config.BANK_TAB_SIZE) {
    				c.sendMessage("Your bank(tab) is full!");
    				break;
    			}

    			int itemID = playerItems[i] + modifier;
    			int amount = playerItemsN[i];

    			if (itemID <= 0 || amount <= 0) {
    				continue;
    			}

    			if (removeAction != null) {
    				removeAction.perform(itemID, amount);
    			}

    			if (!c.getItems().addItemToBank(itemID, amount)) {
    				break;
    			} else if (successAction != null) {
    				successAction.perform(i, -1);
    			}
    		}
    	}
    }

    /**
     * Banks our equipment for us
     */
    public void bankEquipment() {
    	bankContainer(c.playerEquipment, c.playerEquipmentN, 0, null, c.getItems()::replaceEquipment);
    }

    public boolean bankHasItemAmount(int itemId, int amount) {
    	itemId++;
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (c.bankItems0[i] == itemId) {
    			if (c.bankItems0N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems1[i] == itemId) {
    			if (c.bankItems1N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems2[i] == itemId) {
    			if (c.bankItems2N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems3[i] == itemId) {
    			if (c.bankItems3N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems4[i] == itemId) {
    			if (c.bankItems4N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems5[i] == itemId) {
    			if (c.bankItems5N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems6[i] == itemId) {
    			if (c.bankItems6N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems7[i] == itemId) {
    			if (c.bankItems7N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    		if (c.bankItems8[i] == itemId) {
    			if (c.bankItems8N[i] >= amount) {
    				// c.sendMessage("inBank True");
    				return true;
    			}
    		}
    	}
    	return false;
    }

    public boolean bankItem(int itemID, int fromSlot, int amount) {
    	// if (!c.isBanking)
    	// return false;
    	if (c.getZones().isInDmmLobby())
			return false;
    	if (fromSlot < 0 || fromSlot >= c.playerItems.length) {
    		return false;
    	}
    	if (c.isBobOpen) {
    		c.getSummoning().depositItem(itemID, fromSlot, amount);
    		return false;
    	}
    	if (c.inTrade) {
    		// c.sendMessage("You can't store items while trading!");
    		return false;
    	}

    	if (c.playerItems[fromSlot] <= 0 || c.playerItemsN[fromSlot] <= 0) {
    		return false;
    	}
    	if (c.playerItems[fromSlot] - 1 != itemID) {
    		c.sendMessage("Poop5000");
    		return false;
    	}
    	if (itemID < 0) {
    		return false;
    	}
    	ItemDef def = ItemDef.forID(c.playerItems[fromSlot] - 1);
    	// System.out.println("test: "+(def.getNoteId()));

    	if (def == null) {
    		c.sendMessage("Definitions are null for itemid:" + itemID);
    		return false;
    	}

    	int unNoteId = itemID;
    	if (def.itemIsInNotePosition && def.getNoteId() > 0) {
    		unNoteId = def.getNoteId();
    	}

    	if (c.bankingTab != getTabforItem(unNoteId)) {
    		c.getPA().openUpBank(getTabforItem(unNoteId));// Move to tab item is
    		// in before adding
    	}

    	if (c.getPA().getBankItems(c.bankingTab) >= Config.BANK_TAB_SIZE && !itemInBank(unNoteId)) {
    		c.sendMessage("You can't store any more items in this tab!");
    		return false;
    	}
    	
    	//if banking unnoted items (regular items)
    	if (!def.itemIsInNotePosition) { 
    		if (c.playerItems[fromSlot] <= 0) {
    			return false;
    		}
    		if (ItemProjectInsanity.itemStackable[c.playerItems[fromSlot] - 1] || c.playerItemsN[fromSlot] > 1) {
    			int toBankSlot = 0;
    			boolean alreadyInBank = false;
    			for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    				if (c.bankingItems[i] == c.playerItems[fromSlot]) {
    					if (c.playerItemsN[fromSlot] < amount) {
    						amount = c.playerItemsN[fromSlot];
    					}
    					alreadyInBank = true;
    					toBankSlot = i;
    					i = Config.BANK_TAB_SIZE + 1;
    				}
    			}

    			if (!alreadyInBank && freeBankSlots() > 0) {
    				for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    					if (c.bankingItems[i] <= 0) {
    						toBankSlot = i;
    						i = Config.BANK_TAB_SIZE + 1;
    					}
    				}
    				c.bankingItems[toBankSlot] = c.playerItems[fromSlot];
    				if (c.playerItemsN[fromSlot] < amount) {
    					amount = c.playerItemsN[fromSlot];
    				}

    				if (((long) c.bankingItemsN[toBankSlot] + amount) > Integer.MAX_VALUE) {
    					c.sendMessage("You have reached maximum amount of this item.");
    					return false;
    				}

    				c.bankingItemsN[toBankSlot] += amount;
    				deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
    				resetTempItems();
    				resetBank();
    				return true;
    			} else if (alreadyInBank) {
    				if (((long) c.bankingItemsN[toBankSlot] + amount) > Integer.MAX_VALUE) {
    					c.sendMessage("You have reached maximum amount of this item.");
    					return false;
    				}

    				c.bankingItemsN[toBankSlot] += amount;
    				deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
    				resetTempItems();
    				resetBank();
    				return true;
    			} else {
    				c.sendMessage("Bank full9!");
    				return false;
    			}
    		} else {
    			itemID = c.playerItems[fromSlot];
    			int toBankSlot = 0;
    			boolean alreadyInBank = false;
    			for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    				if (c.bankingItems[i] == c.playerItems[fromSlot]) {
    					alreadyInBank = true;
    					toBankSlot = i;
    					i = Config.BANK_TAB_SIZE + 1;
    				}
    			}
    			if (!alreadyInBank && freeBankSlots() > 0) {
    				for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    					if (c.bankingItems[i] <= 0) {
    						toBankSlot = i;
    						i = Config.BANK_TAB_SIZE + 1;
    					}
    				}
    				int firstPossibleSlot = 0;
    				boolean itemExists = false;
    				while (amount > 0) {
    					itemExists = false;
    					for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
    						if ((c.playerItems[i]) == itemID) {
    							firstPossibleSlot = i;
    							itemExists = true;
    							i = 30;
    						}
    					}
    					if (itemExists) {
    						if (((long) c.bankingItemsN[toBankSlot] + 1) > Integer.MAX_VALUE) {
    							c.sendMessage("You have reached maximum amount of this item.");
    							return false;
    						}

    						c.bankingItems[toBankSlot] = c.playerItems[firstPossibleSlot];
    						c.bankingItemsN[toBankSlot] += 1;
    						deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
    						amount--;
    					} else {
    						amount = 0;
    					}
    				}
    				resetTempItems();
    				resetBank();
    				return true;
    			} else if (alreadyInBank) {
    				int firstPossibleSlot = 0;
    				boolean itemExists = false;
    				while (amount > 0) {
    					itemExists = false;
    					for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
    						if ((c.playerItems[i]) == itemID) {
    							firstPossibleSlot = i;
    							itemExists = true;
    							i = 30;
    						}
    					}
    					if (itemExists) {
    						if (((long) c.bankingItemsN[toBankSlot] + 1) > Integer.MAX_VALUE) {
    							c.sendMessage("You have reached maximum amount of this item.");
    							return false;
    						}
    						c.bankingItemsN[toBankSlot] += 1;
    						deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
    						amount--;
    					} else {
    						amount = 0;
    					}
    				}
    				resetTempItems();
    				resetBank();
    				return true;
    			} else {
    				c.sendMessage("Bank full10!");
    				return false;
    			}
    		}
    	} else if (def.itemIsInNotePosition) { // if banking noted items
    		int unNotedId = def.getNoteId() + 1;
    		if (unNotedId < 1) {
    			c.sendMessage("Error" + (c.playerItems[fromSlot] - 1));
    			return false;
    		}
    		if (c.playerItems[fromSlot] <= 0) {
    			return false;
    		}
    		if (ItemProjectInsanity.itemStackable[c.playerItems[fromSlot] - 1] || c.playerItemsN[fromSlot] > 1) {
    			int toBankSlot = 0;
    			boolean alreadyInBank = false;
    			for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    				if (c.bankingItems[i] == unNotedId) {
    					if (c.playerItemsN[fromSlot] < amount) {
    						amount = c.playerItemsN[fromSlot];
    					}
    					alreadyInBank = true;
    					toBankSlot = i;
    					i = Config.BANK_TAB_SIZE + 1;
    				}
    			}

    			if (!alreadyInBank && freeBankSlots() > 0) {
    				for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    					if (c.bankingItems[i] <= 0) {
    						toBankSlot = i;
    						i = Config.BANK_TAB_SIZE + 1;
    					}
    				}
    				c.bankingItems[toBankSlot] = unNotedId;

    				if (c.playerItemsN[fromSlot] < amount) {
    					amount = c.playerItemsN[fromSlot];
    				}

    				if (((long) c.bankingItemsN[toBankSlot] + amount) > Integer.MAX_VALUE) {
    					c.sendMessage("You have reached maximum amount of this item.");
    					return false;
    				}

    				c.bankingItemsN[toBankSlot] += amount;
    				deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
    				resetTempItems();
    				resetBank();
    				return true;
    			} else if (alreadyInBank) {
    				if (((long) c.bankingItemsN[toBankSlot] + amount) > Integer.MAX_VALUE) {
    					c.sendMessage("You have reached maximum amount of this item.");
    					return false;
    				}

    				c.bankingItemsN[toBankSlot] += amount;
    				deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
    				resetTempItems();
    				resetBank();
    				return true;
    			} else {
    				c.sendMessage("Bank full11!");
    				return false;
    			}
    		} else {
    			itemID = c.playerItems[fromSlot];
    			int toBankSlot = 0;
    			boolean alreadyInBank = false;
    			for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    				if (c.bankingItems[i] == (unNotedId)) {
    					alreadyInBank = true;
    					toBankSlot = i;
    					i = Config.BANK_TAB_SIZE + 1;
    				}
    			}
    			if (!alreadyInBank && freeBankSlots() > 0) {
    				for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    					if (c.bankingItems[i] <= 0) {
    						toBankSlot = i;
    						i = Config.BANK_TAB_SIZE + 1;
    					}
    				}
    				int firstPossibleSlot = 0;
    				boolean itemExists = false;
    				while (amount > 0) {
    					itemExists = false;
    					for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
    						if ((c.playerItems[i]) == itemID) {
    							firstPossibleSlot = i;
    							itemExists = true;
    							i = 30;
    						}
    					}
    					if (itemExists) {
    						if (((long) c.bankingItemsN[toBankSlot] + 1) > Integer.MAX_VALUE) {
    							c.sendMessage("You have reached maximum amount of this item.");
    							return false;
    						}

    						c.bankingItems[toBankSlot] = unNotedId;
    						c.bankingItemsN[toBankSlot] += 1;
    						deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
    						amount--;
    					} else {
    						amount = 0;
    					}
    				}
    				resetTempItems();
    				resetBank();
    				return true;
    			} else if (alreadyInBank) {
    				int firstPossibleSlot = 0;
    				boolean itemExists = false;
    				while (amount > 0) {
    					itemExists = false;
    					for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
    						if ((c.playerItems[i]) == itemID) {
    							firstPossibleSlot = i;
    							itemExists = true;
    							i = 30;
    						}
    					}
    					if (itemExists) {
    						if (((long) c.bankingItemsN[toBankSlot] + 1) > Integer.MAX_VALUE) {
    							c.sendMessage("You have reached maximum amount of this item.");
    							return false;
    						}

    						c.bankingItemsN[toBankSlot] += 1;
    						deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
    						amount--;
    					} else {
    						amount = 0;
    					}
    				}
    				resetTempItems();
    				resetBank();
    				return true;
    			} else {
    				c.sendMessage("Bank full12!");
    				return false;
    			}
    		}
    	} else {
    		c.sendMessage("Item not supported " + (c.playerItems[fromSlot] - 1));
    		return false;
    	}
    }

    /**
     * Banking your item.
     *
     * @param itemID
     * @param fromSlot
     * @param amount
     * @return
     */
    public boolean bankItem(int itemID, int fromSlot, int amount, int[] array, int[] arrayN) {
	if (c.inTrade) {
	    c.sendMessage("You can't store items while trading!");
	    return false;
	}
	if (c.insideDungeoneering()) {
	    c.sendMessage("You can't store items here.");
	    return false;
	}

	if (c.playerItems[fromSlot] <= 0 || c.playerItemsN[fromSlot] <= 0) {
	    return false;
	}

	if (itemID < 0) {
	    return false;
	}
	ItemDef def = ItemDef.forID(c.playerItems[fromSlot] - 1);
	// System.out.println("test: "+(def.getNoteId()));

	if (def == null) {
	    c.sendMessage("Definitions are null for itemid:" + itemID);
	    return false;
	}

	// if (!Item.itemIsNoteable[c.playerItems[fromSlot] - 1]) { //banking
	// not noted stuff
	if (!def.itemIsInNotePosition) {
	    if (c.playerItems[fromSlot] <= 0) {
		return false;
	    }
	    if (ItemProjectInsanity.itemStackable[c.playerItems[fromSlot] - 1] || c.playerItemsN[fromSlot] > 1) {
		int toBankSlot = 0;
		boolean alreadyInBank = false;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
		    if (array[i] == c.playerItems[fromSlot]) {
			if (c.playerItemsN[fromSlot] < amount) {
			    amount = c.playerItemsN[fromSlot];
			}
			alreadyInBank = true;
			toBankSlot = i;
			i = Config.BANK_TAB_SIZE + 1;
		    }
		}

		if (!alreadyInBank && freeBankSlots() > 0) {
		    for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (array[i] <= 0) {
			    toBankSlot = i;
			    i = Config.BANK_TAB_SIZE + 1;
			}
		    }
		    array[toBankSlot] = c.playerItems[fromSlot];
		    if (c.playerItemsN[fromSlot] < amount) {
			amount = c.playerItemsN[fromSlot];
		    }
		    if ((arrayN[toBankSlot] + amount) <= Config.MAXITEM_AMOUNT && (arrayN[toBankSlot] + amount) > -1) {
			arrayN[toBankSlot] += amount;
		    } else {
			c.sendMessage("Bank full1!");
			return false;
		    }
		    deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
		    resetBank();
		    return true;
		} else if (alreadyInBank) {
		    if ((arrayN[toBankSlot] + amount) <= Config.MAXITEM_AMOUNT && (arrayN[toBankSlot] + amount) > -1) {
			arrayN[toBankSlot] += amount;
		    } else {
			c.sendMessage("Bank full2!");
			return false;
		    }
		    deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
		    resetBank();
		    return true;
		} else {
		    c.sendMessage("Bank full3!");
		    return false;
		}
	    } else {
		itemID = c.playerItems[fromSlot];
		int toBankSlot = 0;
		boolean alreadyInBank = false;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
		    if (array[i] == c.playerItems[fromSlot]) {
			alreadyInBank = true;
			toBankSlot = i;
			i = Config.BANK_TAB_SIZE + 1;
		    }
		}
		if (!alreadyInBank && freeBankSlots() > 0) {
		    for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (array[i] <= 0) {
			    toBankSlot = i;
			    i = Config.BANK_TAB_SIZE + 1;
			}
		    }
		    int firstPossibleSlot = 0;
		    boolean itemExists = false;
		    while (amount > 0) {
			itemExists = false;
			for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
			    if ((c.playerItems[i]) == itemID) {
				firstPossibleSlot = i;
				itemExists = true;
				i = 30;
			    }
			}
			if (itemExists) {
			    array[toBankSlot] = c.playerItems[firstPossibleSlot];
			    arrayN[toBankSlot] += 1;
			    deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
			    amount--;
			} else {
			    amount = 0;
			}
		    }
		    resetBank();
		    return true;
		} else if (alreadyInBank) {
		    int firstPossibleSlot = 0;
		    boolean itemExists = false;
		    while (amount > 0) {
			itemExists = false;
			for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
			    if ((c.playerItems[i]) == itemID) {
				firstPossibleSlot = i;
				itemExists = true;
				i = 30;
			    }
			}
			if (itemExists) {
			    arrayN[toBankSlot] += 1;
			    deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
			    amount--;
			} else {
			    amount = 0;
			}
		    }
		    resetBank();
		    return true;
		} else {
		    c.sendMessage("Bank full4!");
		    return false;
		}
	    }
	    // } else if (Item.itemIsNoteable[c.playerItems[fromSlot] - 1] &&
	    // !Item.itemIsNoteable[c.playerItems[fromSlot] - 2]) {
	} else if (def.itemIsInNotePosition) {
	    int unNotedId = def.getNoteId() + 1;
	    if (unNotedId < 1) {
		c.sendMessage("Error3:" + (c.playerItems[fromSlot] - 1));
		return false;
	    }
	    if (c.playerItems[fromSlot] <= 0) {
		return false;
	    }
	    if (ItemProjectInsanity.itemStackable[c.playerItems[fromSlot] - 1] || c.playerItemsN[fromSlot] > 1) {
		int toBankSlot = 0;
		boolean alreadyInBank = false;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
		    if (array[i] == unNotedId/* (c.playerItems[fromSlot] - 1) */) {
			if (c.playerItemsN[fromSlot] < amount) {
			    amount = c.playerItemsN[fromSlot];
			}
			alreadyInBank = true;
			toBankSlot = i;
			i = Config.BANK_TAB_SIZE + 1;
		    }
		}

		if (!alreadyInBank && freeBankSlots() > 0) {
		    for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (array[i] <= 0) {
			    toBankSlot = i;
			    i = Config.BANK_TAB_SIZE + 1;
			}
		    }
		    array[toBankSlot] = unNotedId;// (c.playerItems[fromSlot] -
						  // 1);
		    if (c.playerItemsN[fromSlot] < amount) {
			amount = c.playerItemsN[fromSlot];
		    }
		    if ((arrayN[toBankSlot] + amount) <= Config.MAXITEM_AMOUNT && (arrayN[toBankSlot] + amount) > -1) {
			arrayN[toBankSlot] += amount;
		    } else {
			return false;
		    }
		    deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
		    resetBank();
		    return true;
		} else if (alreadyInBank) {
		    if ((arrayN[toBankSlot] + amount) <= Config.MAXITEM_AMOUNT && (arrayN[toBankSlot] + amount) > -1) {
			arrayN[toBankSlot] += amount;
		    } else {
			return false;
		    }
		    deleteItemInOneSlot((c.playerItems[fromSlot] - 1), fromSlot, amount);
		    resetBank();
		    return true;
		} else {
		    c.sendMessage("Bank full5!");
		    return false;
		}
	    } else {
		itemID = c.playerItems[fromSlot];
		int toBankSlot = 0;
		boolean alreadyInBank = false;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
		    if (array[i] == unNotedId/* (c.playerItems[fromSlot] - 1) */) {
			alreadyInBank = true;
			toBankSlot = i;
			i = Config.BANK_TAB_SIZE + 1;
		    }
		}
		if (!alreadyInBank && freeBankSlots() > 0) {
		    for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (array[i] <= 0) {
			    toBankSlot = i;
			    i = Config.BANK_TAB_SIZE + 1;
			}
		    }
		    int firstPossibleSlot = 0;
		    boolean itemExists = false;
		    while (amount > 0) {
			itemExists = false;
			for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
			    if ((c.playerItems[i]) == itemID) {
				firstPossibleSlot = i;
				itemExists = true;
				i = 30;
			    }
			}
			if (itemExists) {
			    array[toBankSlot] = unNotedId/*
							  * (c.playerItems[
							  * firstPossibleSlot] -
							  * 1)
							  */;
			    arrayN[toBankSlot] += 1;
			    deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
			    amount--;
			} else {
			    amount = 0;
			}
		    }
		    resetTempItems();
		    resetBank();
		    return true;
		} else if (alreadyInBank) {
		    int firstPossibleSlot = 0;
		    boolean itemExists = false;
		    while (amount > 0) {
			itemExists = false;
			for (int i = firstPossibleSlot; i < c.playerItems.length; i++) {
			    if ((c.playerItems[i]) == itemID) {
				firstPossibleSlot = i;
				itemExists = true;
				i = 30;
			    }
			}
			if (itemExists) {
			    arrayN[toBankSlot] += 1;
			    deleteItemInOneSlot((c.playerItems[firstPossibleSlot] - 1), firstPossibleSlot, 1);
			    amount--;
			} else {
			    amount = 0;
			}
		    }
		    resetTempItems();
		    resetBank();
		    return true;
		} else {
		    c.sendMessage("Bank full6!");
		    return false;
		}
	    }
	} else {
	    c.sendMessage("Item not supported " + (c.playerItems[fromSlot] - 1));
	    return false;
	}
    }

    /**
     * Dropping items
     **/
    public void createGroundItemPacket(int itemID, int itemX, int itemY, int itemAmount) {
	if (c.isBot()) {
	    return;
	}
	if (c.getOutStream() == null || !c.isActive) {
	    return;
	}
	// synchronized (c) {
	c.getOutStream().createPacket(85);
	c.getOutStream().writeByteC((itemY - 8 * c.mapRegionY));
	c.getOutStream().writeByteC((itemX - 8 * c.mapRegionX));
	c.getOutStream().createPacket(44);
	c.getOutStream().write3Byte(itemID);
	c.getOutStream().writeShort(itemAmount);
	c.getOutStream().writeByte(0);
	c.flushOutStream();
	// }
    }

    /**
     * Deletes all of a player's items.
     **/
    public void deleteAllItems() {
    	for (int i = 0, length = c.playerItems.length; i < length; i++) {
    		if (c.playerItems[i] < 1) {
    			continue;
    		}

    		deleteItemInSlot(i);
    	}

    	for (int i = 0, length = c.playerEquipment.length; i < length; i++) {
    		if (c.playerEquipment[i] < 0) {
    			continue;
    		}

    		deleteEquipment(c.playerEquipment[i], i);
    	}
    }

    /**
     * Delete arrows.
     **/
    public int deleteArrowSlotAmmo() {
    	int ammoId = 0;
    	if (((c.playerEquipment[PlayerConstants.playerCape] == 10499
    		|| CompletionistCape.isCompletionistCape(c.playerEquipment[PlayerConstants.playerCape]))
    		&& Misc.random(100) <= 72) || (c.playerEquipment[PlayerConstants.playerArrows] == 4740)) {
    	    return ammoId;
    	}

    	if (((c.playerEquipment[PlayerConstants.playerCape] == 122109
    			|| c.playerEquipment[PlayerConstants.playerCape] == 121898)
    			&& Misc.random(100) <= 80) || (c.playerEquipment[PlayerConstants.playerArrows] == 4740)) {
    		return ammoId;
    	}

    	if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926) {
    		if (c.getBlowpipeAmmo() > 0) {
    			c.setBlowpipeAmmo(c.getBlowpipeAmmo() - 1);
    			ammoId = c.getBlowpipeAmmoType();
    			if (c.getBlowpipeAmmo() == 0)
    				c.setBlowpipeAmmoType(0);
    			return ammoId;
    		}
    	}
    	if (c.playerEquipmentN[PlayerConstants.playerArrows] == 1) {
    		ammoId = c.playerEquipment[PlayerConstants.playerArrows];
    	    c.getItems().deleteEquipment(c.playerEquipment[PlayerConstants.playerArrows], PlayerConstants.playerArrows);
    	    return ammoId;
    	}
    	if (c.playerEquipmentN[PlayerConstants.playerArrows] != 0) {
    		ammoId = c.playerEquipment[PlayerConstants.playerArrows];
    	    c.setUpdateEquipment(true, PlayerConstants.playerArrows);

    	    c.playerEquipmentN[PlayerConstants.playerArrows] -= 1;
    	    return ammoId;
    	}
    	return ammoId;
    }

    public int deleteEquipedWeaponSlot() {
    	int weaponId = 0;
    	if (((c.playerEquipment[PlayerConstants.playerCape] == 10499
    			|| CompletionistCape.isCompletionistCape(c.playerEquipment[PlayerConstants.playerCape]))
    			&& Misc.random(100) <= 72)) {
    		return weaponId;
    	}

    	if (((c.playerEquipment[PlayerConstants.playerCape] == 122109
    			|| c.playerEquipment[PlayerConstants.playerCape] == 121898)
    			&& Misc.random(100) <= 80)) {
    		return weaponId;
    	}

    	if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926) {
    		if (c.getBlowpipeAmmo() > 0) {
    			c.setBlowpipeAmmo(c.getBlowpipeAmmo() - 1);
    			weaponId = c.getBlowpipeAmmoType();
    			if (c.getBlowpipeAmmo() == 0)
    				c.setBlowpipeAmmoType(0);
    			return weaponId;
    		}
    		return weaponId;
    	}
    	if (c.playerEquipmentN[PlayerConstants.playerWeapon] == 1) {
    		weaponId = c.playerEquipment[PlayerConstants.playerWeapon];
    		c.getItems().deleteEquipment(c.playerEquipment[PlayerConstants.playerWeapon], PlayerConstants.playerWeapon);
    		return weaponId;
    	}
    	if (c.playerEquipmentN[PlayerConstants.playerWeapon] != 0) {
    		weaponId = c.playerEquipment[PlayerConstants.playerWeapon];
    		c.setUpdateEquipment(true, PlayerConstants.playerWeapon);

    		c.playerEquipmentN[PlayerConstants.playerWeapon] -= 1;

    		if (c.playerEquipmentN[PlayerConstants.playerWeapon] <= 0) {
    			c.playerEquipment[PlayerConstants.playerWeapon] = -1;
    			c.playerEquipmentN[PlayerConstants.playerWeapon] = 0;
    			c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
    		}
    	}
    	return weaponId;

    }

    /**
     * Delete item equipment.
     **/
    public void deleteEquipment(int itemId, int equipmentSlot) {
    	deleteEquipment(itemId, equipmentSlot, c.playerEquipmentN[equipmentSlot]);
    }
    
    public void deleteEquipment(int itemId, int equipmentSlot, int amount) {
    	// synchronized (c) {
    	if (itemId < 0) {
    		return;
    	}

    	c.playerEquipmentN[equipmentSlot] -= amount;
    	
    	if (c.playerEquipmentN[equipmentSlot] <= 0) {
    		c.playerEquipmentN[equipmentSlot] = 0;
        	c.playerEquipment[equipmentSlot] = -1;
    	}
    	if (!c.isBot() && c.isActive && c.getOutStream() != null) {
    		c.getOutStream().createVariableShortPacket(34);
    		c.getOutStream().writeShort(1688);
    		c.getOutStream().writeByte(equipmentSlot);
    		c.getOutStream().write3Byte(0);
    		c.getOutStream().writeByte(0);
    		c.getOutStream().endFrameVarSizeWord();
    	}
    	if (equipmentSlot == PlayerConstants.playerWeapon) {
    		// cheaphax fix to switching back to weapons that use defensive
    		// style but only had 3 attack
    		// styles
    		if ((c.getFightXp() == FightExp.MELEE_DEFENSE
    				|| c.getFightXp() == FightExp.RANGED_DEFENSE) && c.fightMode == 2) {
    			c.fightMode = 3;
    		}
    		refreshWeapon();
    	}
    	c.setUpdateEquipment(true, equipmentSlot);

    	c.getUpdateFlags().add(UpdateFlag.APPEARANCE);

    	// }
    }
    
    public void deleteEquipmentBySlot(int equipmentSlot) {

    	c.playerEquipmentN[equipmentSlot] = 0;
    	c.playerEquipment[equipmentSlot] = -1;
        	
    	if (!c.isBot() && c.isActive && c.getOutStream() != null) {
    		c.getOutStream().createVariableShortPacket(34);
    		c.getOutStream().writeShort(1688);
    		c.getOutStream().writeByte(equipmentSlot);
    		c.getOutStream().write3Byte(0);
    		c.getOutStream().writeByte(0);
    		c.getOutStream().endFrameVarSizeWord();
    	}

    	if (equipmentSlot == PlayerConstants.playerWeapon) {
    		// cheaphax fix to switching back to weapons that use defensive
    		// style but only had 3 attack
    		// styles
    		if ((c.getFightXp() == FightExp.MELEE_DEFENSE
    				|| c.getFightXp() == FightExp.RANGED_DEFENSE) && c.fightMode == 2) {
    			c.fightMode = 3;
    		}
    		refreshWeapon();
    		c.getPA().resetAutocast();
    	}

    	c.setUpdateEquipment(true, equipmentSlot);
    	c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
    }

    public void deleteEquipmentItem(int itemId) {
    	if (c.isBot()) {
    		return;
    	}
    	// itemId++;
    	for (int i = 0; i < c.playerEquipment.length; i++) {
    		if (c.playerEquipment[i] == itemId) {
    			// c.sendMessage("Found item ");
    			c.playerEquipment[i] = -1;
    			c.playerEquipmentN[i] = 0; // = c.playerEquipmentN[i] - 1;
    			if (c.isActive && c.getOutStream() != null) {
    				c.getOutStream().createVariableShortPacket(34);
    				c.getOutStream().writeShort(1688);
    				c.getOutStream().writeByte(i);
    				c.getOutStream().write3Byte(0);
    				c.getOutStream().writeByte(0);
    				c.getOutStream().endFrameVarSizeWord();
    			}
    			if (i == PlayerConstants.playerWeapon) {
    				// cheaphax fix to switching back to weapons that use
    				// defensive style but only had 3
    				// attack styles
    				if ((c.getFightXp() == FightExp.MELEE_DEFENSE
    						|| c.getFightXp() == FightExp.RANGED_DEFENSE) && c.fightMode == 2) {
    					c.fightMode = 3;
    				}
    				sendWeapon(-1, "Unarmed");
    				MeleeData.getPlayerAnimIndex(c, -1);
    				c.getItems().addSpecialBar(false);
    			}
    			c.setUpdateEquipment(true, i);
    			c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
    		}
    	}
    }

    /**
     *  Searches through players inventory and finds all item ids that match argument.
	 *  If found, deletes x amount of them and returns the number of items deleted.
	 *
	 *  @return The number of items that were deleted.
     */
    public int deleteInventoryItem(int itemId, int amount) {
		int count = 0;

    	for (int i = 0; i < c.playerItems.length; i++) {
    		final int id = c.playerItems[i];

    		if (id != (itemId + 1)) {
    			continue;
			}

			if (count >= amount) {
    			break;
			}

			final int itemAmount = c.playerItemsN[i];

			if (c.playerItemsN[i] > amount) {
				c.playerItemsN[i] -= amount;
				count += amount;
			} else {
				c.playerItems[i] = 0;
				c.playerItemsN[i] = 0;
				count += itemAmount;
			}

		}

		updateInventory();
		return count;
	}

    public boolean deleteItem2(int id, int amount) { // searches for the item in
						     // loop. so if we have
						     // nonstackable items,
						     // it will remove that
						     // amount. will remove
						     // all amounts of
						     // unstackable
						     // items.
	int am = amount;
	boolean removed = false;
	for (int i = 0, length = c.playerItems.length; i < length; i++) {
	    if (am == 0) {
		break;
	    }
	    if (c.playerItems[i] == (id + 1)) {
		if (c.playerItemsN[i] > amount) {
		    c.playerItemsN[i] -= amount;
		    removed = true;
		    break;
		} else {
		    c.playerItems[i] = 0;
		    c.playerItemsN[i] = 0;
		    am--;
		    removed = true; // ghetto way, will cause bugs if not used
				    // right lol
		}
	    }
	}
	c.setUpdateInvInterface(3214); // resetItems(3214);
	return removed;
    }

    public boolean takeCoins(int amount) {
    	return takeItem(995, amount, false);
    }
   
    public boolean takeItem(int id, int amount, boolean checkBank) {
    	if (c.getZones().isInDmmLobby()) {
			return false;
    	}
   
    	if (checkBank) {
    		if (removeBankItem(id, amount)) {
    			return true;
    		}
    	}
   
    	if (id == 995 && c.getMoneyPouch() >= amount) {
    		MoneyPouch.removeFromMoneyPouch(c, amount);
    		return true;
    	}

    	if (deleteItem3(id, amount)) {
    		return true;
    	}

    	return false;
    }

    public boolean deleteItem3(int id, int amount) { // searches for the item in
						     // loop. so if we have
						     // nonstackable items,
						     // it will remove that
						     // amount. will remove
						     // all amounts of
						     // unstackable
						     // items.
	boolean removed = false;
	if (amount == 0) {
	    return false;
	}
	if (!playerHasItem(id, amount)) {
	    return false;
	}
	for (int i = 0; i < c.playerItems.length; i++) {
	    if (c.playerItems[i] == (id + 1)) {
		if (c.playerItemsN[i] > amount) {
		    c.playerItemsN[i] -= amount;
		    removed = true;
		} else if (c.playerItemsN[i] == amount) {
		    c.playerItems[i] = 0;
		    c.playerItemsN[i] = 0;
		    removed = true;
		}
	    }
	}
	c.setUpdateInvInterface(3214); // resetItems(3214);
	return removed;
    }

    public int deleteItemFromSlot(int slot, int amount) {
		if (slot < 0 || slot >= c.playerItems.length || amount < 0) {
			return 0;
		}
		int count = c.playerItemsN[slot];
		if (c.playerItemsN[slot] > amount) {
			c.playerItemsN[slot] -= amount;
			count = amount;
		} else {
			c.playerItems[slot] = 0;
			c.playerItemsN[slot] = 0;
		}
		updateInventory();
		return count;
	}

    /**
     * Delete items.
     *
     * @param id
     * @param amount
     */
    public boolean deleteItemInOneSlot(int id, int amount) {
	return deleteItemInOneSlot(id, getItemSlot(id), amount);
    }

    public boolean deleteItemInOneSlot(int id, int slot, int amount) { // delete
								       // from
								       // a
								       // specific
								       // slot,
								       // no
								       // loops
								       // here.
								       // will
								       // only
								       // remove
								       // 1
								       // unstackable
								       // item.
	if (id <= 0 || slot < 0) {
	    return false;
	}
	if (c.playerItemsN[slot] < amount) {
	    return false;
	}
	if (c.playerItems[slot] == (id + 1)) {
	    if (c.playerItemsN[slot] > amount) {
		c.playerItemsN[slot] -= amount;
	    } else {
		c.playerItemsN[slot] = 0;
		c.playerItems[slot] = 0;
	    }
	    // PlayerSave.saveGame(c);
	    if (c.isBot()) {
		BotClient bot = (BotClient) c;
		if (bot.getOwnerClient() != null) {
		    bot.getOwnerClient().getItems().resetItems(3214);
		}
		bot = null;
	    } else {
		c.setUpdateInvInterface(3214); // resetItems(3214);
	    }
	    return true;
	}
	return false;
    }

    public void hardDelete(int itemId) {
    	deleteItem(getInventoryItem(itemId));
    	deleteEquipmentItem(itemId);
    }

    public boolean deleteItems(Item... items) {
    	final Set<Item> invItems = new HashSet<Item>();
    	for (Item item : items) {
    		Item invItem = getInventoryItem(item.getId());
    		if (invItem.getAmount() < item.getAmount())
    			return false;
    		invItems.add(new Item(item.getId(), item.getAmount(), invItem.getSlot()));
    	}
    	for (Item item : invItems) {
    		deleteItem(item);
    	}
    	return true;
    }
    
	public boolean deleteItem(Item item) {
		int id = item.getId();
		if (id < 0) {
			return false;
		}

		int slot = item.getSlot();
		if (slot < 0) {
			return false;
		}

		if (c.playerItems[slot] != (id + 1)) {
			return false;
		}

		int amount = item.getAmount();
		if (c.playerItemsN[slot] < amount) {
			return false;
		}

		if (c.playerItemsN[slot] > amount) {
			c.playerItemsN[slot] -= amount;
		} else {
			c.playerItemsN[slot] = 0;
			c.playerItems[slot] = 0;
		}

		if (c.isBot()) {
			BotClient bot = (BotClient) c;
			if (bot.getOwnerClient() != null) {
				bot.getOwnerClient().getItems().resetItems(3214);
			}
			bot = null;
		} else {
			c.setUpdateInvInterface(3214); // resetItems(3214);
		}

		return true;
	}

    public void removeItemInSlot(int slot) {
	    c.playerItems[slot] = 0;
    	c.playerItemsN[slot] = 0;
    }

    public boolean deleteItemInSlot(int slot) {
		if (c.playerItems[slot] == 0) {
			return false;
		}

		removeItemInSlot(slot);
	    if (c.isBot()) {
		BotClient bot = (BotClient) c;
		if (bot.getOwnerClient() != null) {
		    bot.getOwnerClient().getItems().resetItems(3214);
		}
		bot = null;
	    } else {
		c.setUpdateInvInterface(3214); // resetItems(3214);
	    }
	    return true;
    }

    public boolean deleteItemInSlot(int id, int slot) { // delete from a
							// specific slot, no
							// loops here.
							// will all of
							// stackable/unstackable
							// item.
	if (id <= 0 || slot < 0) {
	    return false;
	}
	if (c.playerItems[slot] == (id + 1)) {
	    c.playerItemsN[slot] = 0;
	    c.playerItems[slot] = 0;
	    if (c.isBot()) {
		BotClient bot = (BotClient) c;
		if (bot.getOwnerClient() != null) {
		    bot.getOwnerClient().getItems().resetItems(3214);
		}
		bot = null;
	    } else {
		c.setUpdateInvInterface(3214); // resetItems(3214);
	    }
	    return true;
	}
	return false;
    }

    private boolean storeDeathItems() {
    	if (c.insideInstance()) {
    		return c.getInstanceManager().storeDeathItems();
    	}

    	for (Zone zone : c.getZones().getZoneList()) {
			if (!zone.isInsideZone()) {
				continue;
			}

			if (zone.storeDeathItems()) {
				return true;
			}
		}

    	return false;
    }

    /**
     * Drops all items for a killer.
     **/
    public void dropAllItems(final Client killer, boolean killedInWildy, boolean killedInLmsArena) {
    	if (killedInWildy) {
    		try { //added try block just in case.
				c.getBrawlers().destroyGlovesInWild();
			} catch (Exception e) {
				e.printStackTrace();
			}

        	if (killer != null && c != killer && c.getUntradeableGoldToDrop() > 0) {
        		if (WorldType.equalsType(WorldType.SPAWN)) {
        			ItemHandler.createGroundItemFromPlayerDeath(killer, c.killedByPlayerName, 8890, c.getX(), c.getY(),
        					c.getHeightLevel(), c.getUntradeableGoldToDrop() / 20000, c);
        			c.setUntradeableGold(0);
        		} else {
        			ItemHandler.createGroundItemFromPlayerDeath(killer, c.killedByPlayerName, 995, c.getX(), c.getY(),
        					c.getHeightLevel(), c.getUntradeableGoldToDrop(), c);
        			c.setUntradeableGold(0);
        		}
        	}
    	}

    	boolean storeItems = false;
    	if (!killedInWildy && !killedInLmsArena) {
    		storeItems = storeDeathItems();
    	}

    	Consumer<GroundItem> instanceConsumer = null;
    	if (c.insideInstance()) {
    		instanceConsumer = item -> {
    			c.getInstanceManager().addGroundItem(item);
    		};
    	}

    	if (storeItems && !ItemRetrievalManager.transfer(c)) {
    		storeItems = false;
    		c.sendMessage("The Grim reaper wasn't able to accept your death items because you didn't empty his coffer.");
    	}

    	if (!storeItems) {
	    	// boolean foundItem = false;
	    	List<DroppedItem> droppedItems = new ArrayList<>();
	
	    	for (int e = 0, length = c.playerEquipment.length; e < length; e++) {
	    		int itemId = c.playerEquipment[e];
	    		if (itemId < 0) {
	    			continue;
	    		}
	
	    		int itemAmount = c.playerEquipmentN[e];
	    		if (c.itemsToKeep.contains(new ComparableItem(itemId, itemAmount))) {
	    			continue;
	    		}
	
	    		if (killer != null) {
	    			if (killedInLmsArena || tradeable(itemId)) {
	    				if (DBConfig.LOG_DROP && !killedInLmsArena)
	    					droppedItems.add(new DroppedItem(c, itemId, itemAmount, "death"));
	    				ItemHandler.createGroundItemFromPlayerDeath(killer, c.killedByPlayerName, itemId,
	    						c.getX(), c.getY(), c.getHeightLevel(), itemAmount, c, instanceConsumer);
	    			} else {
	    				ItemHandler.createGroundItemFromPlayerDeath(killer, killer.getName(), itemId, c.getX(),
	    						c.getY(), c.getHeightLevel(), itemAmount, c, instanceConsumer);
	    			}
	    		} else {
	    			if (killedInLmsArena) {
	    				ItemHandler.createGroundItem(new GroundItem(itemId, c.getX(), c.getY(),
	    						c.getHeightLevel(), itemAmount, ItemHandler.HIDE_TICKS*2), true);
	    			} else 
	    				ItemHandler.createGroundItemFromPlayerDeath(c, c.getName(), itemId, c.getX(), c.getY(),
	    						c.getHeightLevel(), itemAmount, c, instanceConsumer);
	    		}
	    	}
	    	for (int i = 0, length = c.playerItems.length; i < length; i++) {
	    		int itemId = c.playerItems[i] - 1;
	    		if (itemId < 0) {
	    			continue;
	    		}
	
	    		int itemAmount = c.playerItemsN[i];
	    		if (c.itemsToKeep.contains(new ComparableItem(itemId, itemAmount))) {
	    			continue;
	    		}
	
	    		if (killer != null) {
	    			if (killedInLmsArena || tradeable(itemId)) {
	    				if (DBConfig.LOG_DROP && !killedInLmsArena)
	    					droppedItems.add(new DroppedItem(c, itemId, itemAmount, "death"));
	    				ItemHandler.createGroundItemFromPlayerDeath(killer, c.killedByPlayerName, itemId,
	    						c.getX(), c.getY(), c.getHeightLevel(), itemAmount, c, instanceConsumer);
	    			} else { // untradeable items here
	    				// if (specialCase(itemId - 1))
	    				// ItemHandler.createGroundItem(o, 995, c.getX(),
	    				// c.getY(),
	    				// c.getHeightLevel(),
	    				// getUntradePrice(itemId - 1));
	    				ItemHandler.createGroundItemFromPlayerDeath(killer, killer.getName(), itemId, c.getX(),
	    						c.getY(), c.getHeightLevel(), itemAmount, c, instanceConsumer);
	    			}
	    		} else {
	    			if (killedInLmsArena) {
	    				ItemHandler.createGroundItem(new GroundItem(itemId, c.getX(), c.getY(),
	    						c.getHeightLevel(), itemAmount, ItemHandler.HIDE_TICKS*2), true);
	    			} else 
	    				ItemHandler.createGroundItemFromPlayerDeath(c, c.getName(), itemId, c.getX(), c.getY(),
	    						c.getHeightLevel(), itemAmount, c, instanceConsumer);
	    		}
	    	}
	
	    	if (DBConfig.LOG_DROP && !killedInLmsArena) {
	    		Server.getLogWriter().addToDropList(droppedItems);
	    	}
	
	    	droppedItems = null;
    	}

    	//drop bones
    	if (killedInLmsArena) {
    		if (killer != null) {
    			ItemHandler.createGroundItemFromPlayerDeath(killer, c.killedByPlayerName, 526, c.getX(),
        				c.getY(), c.getHeightLevel(), 1, c, instanceConsumer);
    		} else { // fog death
    			ItemHandler.createGroundItem(new GroundItem(526, c.getX(), c.getY(), c.getHeightLevel(), 1, ItemHandler.HIDE_TICKS*2), true);
    		}
    	} else {
    		ItemHandler.createGroundItemFromPlayerDeath((killer != null ? killer : c), (killer == null ? c.getName() : c.killedByPlayerName), 526, c.getX(),
    				c.getY(), c.getHeightLevel(), 1, c, instanceConsumer);
    	}
    }

    /**
     * Ranging arrows.
     */
    public void dropAmmo(int ammoId, Location loc) {
    	if ((c.playerEquipment[PlayerConstants.playerCape] == 10499
    			|| CompletionistCape.isCompletionistCape(c.playerEquipment[PlayerConstants.playerCape])) && Misc.random(100) >= 8) {
    		return;
    	}

      	if (c.playerEquipment[PlayerConstants.playerCape] == 122109
    			|| c.playerEquipment[PlayerConstants.playerCape] == 121898) {
    		return;
    	}

    	if (Misc.random(10) >= 4) {
    		int amountOnFloor = 0;
    		// grab my own arrows only
    		GroundItem item = ItemHandler.getPlayerOwnerGroundItem(c.getName(), ammoId, loc.getX(), loc.getY(),
    				loc.getZ());
    		if (item != null) {
    			amountOnFloor = item.getItemAmount();
    		}

    		if (amountOnFloor == 0) {
    			ItemHandler.createGroundItem(c, ammoId, loc.getX(), loc.getY(), loc.getZ(), 1);
    		} else if (amountOnFloor != 0) {
    			ItemHandler.stackGroundItem(item, c, 1);
    		}
    	}
    }

    /**
     * Finds the item.
     *
     * @param id
     * @param items
     * @param amounts
     * @return
     */
    public int findItem(int id, int[] items, int[] amounts) {
    	for (int i = 0; i < c.playerItems.length; i++) {
    		if (((items[i] - 1) == id) && (amounts[i] > 0)) {
    			return i;
    		}
    	}
    	return -1;
    }

    /**
     * Returns array of information on the item in the bank
     *
     * @param itemID
     *            - real item id (995 = gold)
     * @return array { tabId, itemSlot, itemAmount }
     */
    public int[] findItemInBank(int itemID) {
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (c.bankItems0[i] == itemID + 1) {
    			return new int[] { 0, i, c.bankItems0N[i] };
    		} else if (c.bankItems1[i] == itemID + 1) {
    			return new int[] { 1, i, c.bankItems1N[i] };
    		} else if (c.bankItems2[i] == itemID + 1) {
    			return new int[] { 2, i, c.bankItems2N[i] };
    		} else if (c.bankItems3[i] == itemID + 1) {
    			return new int[] { 3, i, c.bankItems3N[i] };
    		} else if (c.bankItems4[i] == itemID + 1) {
    			return new int[] { 4, i, c.bankItems4N[i] };
    		} else if (c.bankItems5[i] == itemID + 1) {
    			return new int[] { 5, i, c.bankItems5N[i] };
    		} else if (c.bankItems6[i] == itemID + 1) {
    			return new int[] { 6, i, c.bankItems6N[i] };
    		} else if (c.bankItems7[i] == itemID + 1) {
    			return new int[] { 7, i, c.bankItems7N[i] };
    		} else if (c.bankItems8[i] == itemID + 1) {
    			return new int[] { 8, i, c.bankItems8N[i] };
    		}
    	}
    	return new int[] { 0, 0, 0 };// if not in bank
    }

    public int findTabForItem(int itemID) {
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (c.bankItems0[i] == itemID + 1) {
    			return 0;
    		} else if (c.bankItems1[i] == itemID + 1) {
    			return 1;
    		} else if (c.bankItems2[i] == itemID + 1) {
    			return 2;
    		} else if (c.bankItems3[i] == itemID + 1) {
    			return 3;
    		} else if (c.bankItems4[i] == itemID + 1) {
    			return 4;
    		} else if (c.bankItems5[i] == itemID + 1) {
    			return 5;
    		} else if (c.bankItems6[i] == itemID + 1) {
    			return 6;
    		} else if (c.bankItems7[i] == itemID + 1) {
    			return 7;
    		} else if (c.bankItems8[i] == itemID + 1) {
    			return 8;
    		}
    	}
    	return 0;// if not in bank add to any open tab
    }

    /**
     * Items in your bank.
     */

    /**
     * Checks if you have free bank slots.
     */
    public int freeBankSlots() {
	return (Config.BANK_TAB_SIZE - 2)- c.getPA().getBankItems(c.bankingTab/*-1*/);// matts
										 // old
										 // code
    }

    /**
     * Checks if you have a free slot.
     *
     * @return
     */
    public int freeSlots() {
		int count = 0;

		for (int i = 0, length = c.playerItems.length; i < length; i++) {
		    if (c.playerItems[i] <= 0) {
		    	count++;
		    }
		}

		return count;
    }

    public int findFreeSlot() {
    	for (int i = 0, length = c.playerItems.length; i < length; i++) {
    		if (c.playerItems[i] <= 0) {
    			return i;
    		}
    	}

    	return -1;
    }

    /**
     * Getting items from your bank.
     *
     * @param itemID
     * @param fromSlot
     * @param amount
     */
    public void fromBank(int itemID, int fromSlot, int amount) {

    	if (c.getZones().isInDmmLobby())
			return;
    	
    	if (fromSlot < 0 || fromSlot >= Config.BANK_TAB_SIZE) {
    		return;
    	}

    	if (c.toggleBot && c.getBot() != null) {
    		fromBotBank(itemID, fromSlot, amount);
    		return;
    	}
    	if (c.interfaceId == 5383) {
    		if (c.bankItems0[fromSlot] != itemID && c.bankItems1[fromSlot] != itemID && c.bankItems2[fromSlot] != itemID
    				&& c.bankItems3[fromSlot] != itemID && c.bankItems4[fromSlot] != itemID
    				&& c.bankItems5[fromSlot] != itemID && c.bankItems6[fromSlot] != itemID
    				&& c.bankItems7[fromSlot] != itemID && c.bankItems8[fromSlot] != itemID) {
    			return;
    		}
    	} else {
    		if (c.bankItems0[fromSlot] != itemID + 1 && c.bankItems1[fromSlot] != itemID + 1
    				&& c.bankItems2[fromSlot] != itemID + 1 && c.bankItems3[fromSlot] != itemID + 1
    				&& c.bankItems4[fromSlot] != itemID + 1 && c.bankItems5[fromSlot] != itemID + 1
    				&& c.bankItems6[fromSlot] != itemID + 1 && c.bankItems7[fromSlot] != itemID + 1
    				&& c.bankItems8[fromSlot] != itemID + 1) {
    			return;
    		}
    	}

    	if (c.takeAsNote && c.bankingItems[fromSlot] > 0 && ItemDef.forID(c.bankingItems[fromSlot]
    			- 1).itemIsInNotePosition) {
    		if (ItemProjectInsanity.itemStackable[c.bankingItems[fromSlot]]) {
    			long l = c.getItems().getItemAmount(c.bankingItems[fromSlot]);
    			long k = (l + amount);
    			if (k > 2147483647) {
    				c.sendMessage("Your inventory is full.");
    				return;
    			}
    		}
    	}
    	if (!c.takeAsNote) {
    		if (c.bankingItems[fromSlot] > 0 && ItemProjectInsanity.itemStackable[c.bankingItems[fromSlot] - 1]) {
    			long l = c.getItems().getItemAmount(c.bankingItems[fromSlot] - 1);
    			long k = (l + amount);
    			if (k > 2147483647) {
    				c.sendMessage("Your inventory is full.");
    				return;
    			}
    		}
    	}

    	if (amount > 0) {
    		if (c.bankingItems[fromSlot] > 0) {
    			if (c.bankingItemsN[fromSlot] == 0) { // clicked on bank place holder
    				if (amount == 1) {
    					c.sendMessage("Withdraw 1 doesn't remove placeholder. Use other withdraw options to remove it.");
    					return;
    				}
    				c.bankingItems[fromSlot] = 0; //remove the bank place holder
    				resetBank();
    			} else {
    				final boolean stackable = ItemProjectInsanity.itemStackable[c.bankingItems[fromSlot] - 1];
    				final int realItemId = c.bankingItems[fromSlot] - 1;
    				if (!c.takeAsNote) {
    					if (stackable) {
    						if (c.bankingItemsN[fromSlot] > amount) {
    							if (addItem((realItemId), amount)) {
    								c.bankingItemsN[fromSlot] -= amount;
    								if (c.bankingItemsN[fromSlot] == 0 && !c.isPlaceholderEnabled())
    									c.bankingItems[fromSlot] = 0;
    								resetBank();
    								c.getItems().resetItems(5064);
    							}
    						} else {
    							if (addItem((realItemId), c.bankingItemsN[fromSlot])) {
    								if (!c.isPlaceholderEnabled())
    									c.bankingItems[fromSlot] = 0;
    								c.bankingItemsN[fromSlot] = 0;
    								resetBank();
    								c.getItems().resetItems(5064);
    							}
    						}
    					} else {
    						while (amount > 0) {
    							if (c.bankingItemsN[fromSlot] > 0) {
    								if (addItem((realItemId), 1)) {
    									c.bankingItemsN[fromSlot]--;
    									if (c.bankingItemsN[fromSlot] == 0 && !c.isPlaceholderEnabled())
    										c.bankingItems[fromSlot] = 0;
    									amount--;
    								} else {
    									amount = 0;
    								}
    							} else {
    								amount = 0;
    							}
    						}
    						resetBank();
    						c.getItems().resetItems(5064);
    					}
    				} else if (c.takeAsNote && ItemDef.forID(realItemId)
    						.itemCanBeNoted()) {
    					int notedId = ItemDef.forID(realItemId).getNoteId();
    					if (c.bankingItemsN[fromSlot] > amount) {
    						if (addItem(notedId, amount)) {
    							c.bankingItemsN[fromSlot] -= amount;
    							if (c.bankingItemsN[fromSlot] == 0 && !c.isPlaceholderEnabled())
    								c.bankingItems[fromSlot] = 0;
    							resetBank();
    							c.getItems().resetItems(5064);
    						}
    					} else {
    						if (addItem(notedId, c.bankingItemsN[fromSlot])) {
    							if (!c.isPlaceholderEnabled())
    								c.bankingItems[fromSlot] = 0;
    							c.bankingItemsN[fromSlot] = 0;
    							resetBank();
    							c.getItems().resetItems(5064);
    						}
    					}
    				} else {
    					c.sendMessage(
    							"This item can't be withdrawn as a note.");
    					if (stackable) {
    						if (c.bankingItemsN[fromSlot] > amount) {
    							if (addItem((realItemId), amount)) {
    								c.bankingItemsN[fromSlot] -= amount;
    								if (c.bankingItemsN[fromSlot] == 0 && !c.isPlaceholderEnabled())
    									c.bankingItems[fromSlot] = 0;
    								resetBank();
    								c.getItems().resetItems(5064);
    							}
    						} else {
    							if (addItem((realItemId), c.bankingItemsN[fromSlot])) {
    								if (!c.isPlaceholderEnabled())
    									c.bankingItems[fromSlot] = 0;
    								c.bankingItemsN[fromSlot] = 0;
    								resetBank();
    								c.getItems().resetItems(5064);
    							}
    						}
    					} else {
    						while (amount > 0) {
    							if (c.bankingItemsN[fromSlot] > 0) {
    								if (addItem((realItemId), 1)) {
    									c.bankingItemsN[fromSlot]--;
    									if (c.bankingItemsN[fromSlot] == 0 && !c.isPlaceholderEnabled())
    										c.bankingItems[fromSlot] = 0;
    									amount--;
    								} else {
    									amount = 0;
    								}
    							} else {
    								amount = 0;
    							}
    						}
    						resetBank();
    						c.getItems().resetItems(5064);
    					}
    				}
    			}
    		}
    	}
    	
    	// update interface when removed first item in the slot
    	if (c.bankingTab > 0 && fromSlot == 0) {
    		c.getPacketSender().sendFrame126b("1", 27000);
    		c.getPA().openUpBank(c.bankingTab);
    	}
    }

    public void fromBotBank(int itemID, int fromSlot, int amount) {
    	if (c.interfaceId == 5383) {
    		if (c.getBot().bankItems0[fromSlot] != itemID && c.getBot().bankItems1[fromSlot] != itemID
    				&& c.getBot().bankItems2[fromSlot] != itemID && c.getBot().bankItems3[fromSlot] != itemID
    				&& c.getBot().bankItems4[fromSlot] != itemID && c.getBot().bankItems5[fromSlot] != itemID
    				&& c.getBot().bankItems6[fromSlot] != itemID && c.getBot().bankItems7[fromSlot] != itemID
    				&& c.getBot().bankItems8[fromSlot] != itemID) {
    			return;
    		}
    	} else {
    		if (c.getBot().bankItems0[fromSlot] != itemID + 1 && c.getBot().bankItems1[fromSlot] != itemID + 1
    				&& c.getBot().bankItems2[fromSlot] != itemID + 1 && c.getBot().bankItems3[fromSlot] != itemID + 1
    				&& c.getBot().bankItems4[fromSlot] != itemID + 1 && c.getBot().bankItems5[fromSlot] != itemID + 1
    				&& c.getBot().bankItems6[fromSlot] != itemID + 1 && c.getBot().bankItems7[fromSlot] != itemID + 1
    				&& c.getBot().bankItems8[fromSlot] != itemID + 1) {
    			return;
    		}
    	}
    	if (amount > 0) {
    		if (c.getBot().bankingItems[fromSlot] > 0) {
    			if (!c.getBot().takeAsNote) {
    				if (ItemProjectInsanity.itemStackable[c.getBot().bankingItems[fromSlot] - 1]) {
    					if (c.getBot().bankingItemsN[fromSlot] > amount) {
    						if (c.getBot().getItems().addItem((c.getBot().bankingItems[fromSlot] - 1), amount)) {
    							c.getBot().bankingItemsN[fromSlot] -= amount;
    							resetBank();
    							c.getItems().resetItems(5064);
    						}
    					} else {
    						if (c.getBot().getItems().addItem((c.getBot().bankingItems[fromSlot] - 1),
    								c.getBot().bankingItemsN[fromSlot])) {
    							c.getBot().bankingItems[fromSlot] = 0;
    							c.getBot().bankingItemsN[fromSlot] = 0;
    							resetBank();
    							c.getItems().resetItems(5064);
    						}
    					}
    				} else {
    					while (amount > 0) {
    						if (c.getBot().bankingItemsN[fromSlot] > 0) {
    							if (c.getBot().getItems().addItem((c.getBot().bankingItems[fromSlot] - 1), 1)) {
    								c.getBot().bankingItemsN[fromSlot] += -1;
    								amount--;
    							} else {
    								amount = 0;
    							}
    						} else {
    							amount = 0;
    						}
    					}
    					resetBank();
    					c.getItems().resetItems(5064);
    				}
    			} else if (c.getBot().takeAsNote && ItemDef.forID(c.getBot().bankingItems[fromSlot] - 1)
    					.itemCanBeNoted()) {
    				if (c.getBot().bankingItemsN[fromSlot] > amount) {
    					if (c.getBot().getItems().addItem(c.getBot().bankingItems[fromSlot], amount)) {
    						c.getBot().bankingItemsN[fromSlot] -= amount;
    						resetBank();
    						c.getItems().resetItems(5064);
    					}
    				} else {
    					if (c.getBot().getItems().addItem(c.getBot().bankingItems[fromSlot],
    							c.getBot().bankingItemsN[fromSlot])) {
    						c.getBot().bankingItems[fromSlot] = 0;
    						c.getBot().bankingItemsN[fromSlot] = 0;
    						resetBank();
    						c.getItems().resetItems(5064);
    					}
    				}
    			} else {
    				c.sendMessage("[BOT]This item can't be withdrawn as a note.");
    				if (ItemProjectInsanity.itemStackable[c.getBot().bankingItems[fromSlot] - 1]) {
    					if (c.getBot().bankingItemsN[fromSlot] > amount) {
    						if (c.getBot().getItems().addItem((c.getBot().bankingItems[fromSlot] - 1), amount)) {
    							c.getBot().bankingItemsN[fromSlot] -= amount;
    							resetBank();
    							c.getItems().resetItems(5064);
    						}
    					} else {
    						if (c.getBot().getItems().addItem((c.getBot().bankingItems[fromSlot] - 1),
    								c.getBot().bankingItemsN[fromSlot])) {
    							c.getBot().bankingItems[fromSlot] = 0;
    							c.getBot().bankingItemsN[fromSlot] = 0;
    							resetBank();
    							c.getItems().resetItems(5064);
    						}
    					}
    				} else {
    					while (amount > 0) {
    						if (c.getBot().bankingItemsN[fromSlot] > 0) {
    							if (c.getBot().getItems().addItem((c.getBot().bankingItems[fromSlot] - 1), 1)) {
    								c.getBot().bankingItemsN[fromSlot] += -1;
    								amount--;
    							} else {
    								amount = 0;
    							}
    						} else {
    							amount = 0;
    						}
    					}
    					resetBank();
    					c.getItems().resetItems(5064);
    				}
    			}
    		}
    	}

    	c.getPA().openUpBank(c.bankingTab);

    	// update interface when removed first item in the slot
    	if (c.getBot().bankingTab > 0 && fromSlot == 0) {
    		c.getPacketSender().sendFrame126b("1", 27000);
    		c.getPA().openUpBank(c.getBot().bankingTab);
    	}
    	c.setUpdateInvInterface(3214); // resetItems(3214);
    }

    /**
     * search for bank item in certain tab
     *
     * @param itemId
     *            - real item id, 995 for gold
     * @param tab
     *            - tab to search for item
     * @return
     */
    public int getBankItemAmountInTab(int itemId, int tab) {
    	if (tab == 0) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems0[i] == itemId + 1 && c.bankItems0N[i] > 0) {
    				return c.bankItems0N[i];
    			}
    		}
    	} else if (tab == 1) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems1[i] == itemId + 1 && c.bankItems1N[i] > 0) {
    				return c.bankItems1N[i];
    			}
    		}
    	} else if (tab == 2) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems2[i] == itemId + 1 && c.bankItems2N[i] > 0) {
    				return c.bankItems2N[i];
    			}
    		}
    	} else if (tab == 3) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems3[i] == itemId + 1 && c.bankItems3N[i] > 0) {
    				return c.bankItems3N[i];
    			}
    		}
    	} else if (tab == 4) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems4[i] == itemId + 1 && c.bankItems4N[i] > 0) {
    				return c.bankItems4N[i];
    			}
    		}
    	} else if (tab == 5) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems5[i] == itemId + 1 && c.bankItems5N[i] > 0) {
    				return c.bankItems5N[i];
    			}
    		}
    	} else if (tab == 6) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems6[i] == itemId + 1 && c.bankItems6N[i] > 0) {
    				return c.bankItems6N[i];
    			}
    		}
    	} else if (tab == 7) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems7[i] == itemId + 1 && c.bankItems7N[i] > 0) {
    				return c.bankItems7N[i];
    			}
    		}
    	} else if (tab == 8) {
    		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    			if (c.bankItems8[i] == itemId + 1 && c.bankItems8N[i] > 0) {
    				return c.bankItems8N[i];
    			}
    		}
    	}
    	return 0;
    }

    public int getBankItemCount(int itemId) {
    	itemId++;
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (c.bankItems0[i] == itemId) {
    			return c.bankItems0N[i];
    		}
    		if (c.bankItems1[i] == itemId) {
    			return c.bankItems1N[i];
    		}
    		if (c.bankItems2[i] == itemId) {
    			return c.bankItems2N[i];
    		}
    		if (c.bankItems3[i] == itemId) {
    			return c.bankItems3N[i];
    		}
    		if (c.bankItems4[i] == itemId) {
    			return c.bankItems4N[i];
    		}
    		if (c.bankItems5[i] == itemId) {
    			return c.bankItems5N[i];
    		}
    		if (c.bankItems6[i] == itemId) {
    			return c.bankItems6N[i];
    		}
    		if (c.bankItems7[i] == itemId) {
    			return c.bankItems7N[i];
    		}
    		if (c.bankItems8[i] == itemId) {
    			return c.bankItems8N[i];
    		}
    	}
    	return 0;
    }

    public int getBankItemSlot(int itemId) {
    	itemId++;
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (c.bankItems0[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems1[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems2[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems3[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems4[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems5[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems6[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems7[i] == itemId) {
    			return i;
    		}
    		if (c.bankItems8[i] == itemId) {
    			return i;
    		}
    	}
    	return -1;
    }

    public void getBonus() {

    	boolean ignoreAmmoSlot = false;
    	
    	final RangeWeapon rangeWeapon = RangeWeapon.get(c.playerEquipment[PlayerConstants.playerWeapon]);
    	if (rangeWeapon != null && (rangeWeapon.getWeaponType() == WeaponType.THROWN || rangeWeapon.getWeaponType() == WeaponType.DEGRADING || rangeWeapon.getWeaponType() == WeaponType.CHINCHOMPA))
    		ignoreAmmoSlot = true;
    	
    	if (c.playerEquipment[PlayerConstants.playerWeapon] == BlowPipeCharges.BLOWPIPE_FULL && c.getBlowpipeAmmoType() > 0) {
    		ItemDefinition itemDef = ItemDefinition.forId(c.getBlowpipeAmmoType());
    		if (itemDef != null) {
	    		// add darts bonus
	    		CombatStats dartStats = itemDef.getCombatStats();
	    		
	    		c.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] += dartStats.getRangedAccBonus();
				c.getPlayerBonus()[EquipmentBonuses.RANGED_STR.getId()] += dartStats.getRangeStrengthBonus();
    		}
    	}

		for (int i = 0; i < c.playerEquipment.length; i++) {
			final int itemId = c.playerEquipment[i];
		    if (itemId > -1) {
		    	ItemDefinition itemDef = ItemDefinition.forId(itemId);
				if (itemDef == null) {
				    Server.getSlackApi().call(new SlackMessage("lesik777", "Bugged item bonuses: ID:" + itemId));
				    continue;
				}
				
				final CombatStats stats = itemDef.getCombatStats();
		
				c.getPlayerBonus()[EquipmentBonuses.ATTACK_STAB.getId()] += stats.getStabBonus();
				c.getPlayerBonus()[EquipmentBonuses.ATTACK_SLASH.getId()] += stats.getSlashBonus();
				c.getPlayerBonus()[EquipmentBonuses.ATTACK_CRUSH.getId()] += stats.getCrushBonus();
				c.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] += stats.getMagicAccBonus();
				c.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] += stats.getRangedAccBonus();
				c.getPlayerBonus()[EquipmentBonuses.DEFENSE_STAB.getId()] += stats.getStabDefBonus();
				c.getPlayerBonus()[EquipmentBonuses.DEFENSE_SLASH.getId()] += stats.getSlashDefBonus();
				c.getPlayerBonus()[EquipmentBonuses.DEFENSE_CRUSH.getId()] += stats.getCrushDefBonus();
				c.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] += stats.getMagicDefBonus();
				c.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] += stats.getRangedDefBonus();
				c.getPlayerBonus()[EquipmentBonuses.MELEE_STR.getId()] += stats.getStrengthBonus();
				c.getPlayerBonus()[EquipmentBonuses.MAGIC_STR.getId()] += stats.getMagicStrengthBonus();
				c.getPlayerBonus()[EquipmentBonuses.PRAYER.getId()] += stats.getPrayerBonus();
				
				//dont touch the arrow code under here
				if (i == PlayerConstants.playerArrows) {
					if (!ignoreAmmoSlot)
						c.getPlayerBonus()[EquipmentBonuses.RANGED_STR.getId()] += stats.getRangeStrengthBonus();
				} else
					c.getPlayerBonus()[EquipmentBonuses.RANGED_STR.getId()] += stats.getRangeStrengthBonus();
				//dont touch the arrow code above here
						
		    }
		}
		
		switch (c.playerEquipment[PlayerConstants.playerShield]) {
		case 11283:
		case 30336:
			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_STAB.getId()] += c.dfsCharge;
			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_SLASH.getId()] += c.dfsCharge;
			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_CRUSH.getId()] += c.dfsCharge;
			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] += c.dfsCharge;
			break;
		}
		
    }

    // removing item.cfg soon so using this instead
//    public void setBonuses() {
//    	if (c.playerEquipment[PlayerConstants.playerWeapon] == BlowPipeCharges.BLOWPIPE_FULL && c.getBlowpipeAmmoType() > 0) {
//    		// add darts bonus
//    		CombatStats ammoStats = ItemDefinition.forId(c.getBlowpipeAmmoType()).getCombatStats();
//    		c.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] += ammoStats.getRangedAccBonus();
//    	}
//    	
//    	boolean rangeStrOnWeapon = false; // actually needs to be is throwing weapon then ignore ammo slot
//    	
//    	for (int i = 0; i < c.playerEquipment.length; i++) {
//    		final int itemId = c.playerEquipment[i];
//    		if (itemId > -1) {
//    			ItemDefinition def = ItemDefinition.forId(itemId);
//    			if (def == null) {
//    				Server.getSlackApi().call(new SlackMessage("lesik777", "Bugged item bonuses: ID:" + itemId));
//    				continue;
//    			}
//    			
//    			final CombatStats stats = def.getCombatStats();
//    			
//
//    			c.getPlayerBonus()[EquipmentBonuses.ATTACK_STAB.getId()] += stats.getStabBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.ATTACK_SLASH.getId()] += stats.getSlashBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.ATTACK_CRUSH.getId()] += stats.getCrushBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] += stats.getMagicAccBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] += stats.getRangedAccBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_STAB.getId()] += stats.getStabDefBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_SLASH.getId()] += stats.getSlashDefBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_CRUSH.getId()] += stats.getCrushDefBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] += stats.getMagicDefBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] += stats.getRangedDefBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.MELEE_STR.getId()] += stats.getStrengthBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.MAGIC_STR.getId()] += stats.getMagicStrengthBonus();
//    			c.getPlayerBonus()[EquipmentBonuses.PRAYER.getId()] += stats.getPrayerBonus();
//    			
//
//    			if (i == PlayerConstants.playerWeapon && stats.getRangeStrengthBonus() > 0)
//    				rangeStrOnWeapon = true;
//    			
//    			//don't add bonuses from arrows slot if str bonus is from weapon slot
//    			if (rangeStrOnWeapon && i == PlayerConstants.playerArrows)
//    				continue;
//    			c.getPlayerBonus()[EquipmentBonuses.RANGED_STR.getId()] += stats.getRangeStrengthBonus();
//    			
//    		}
//    	}
//    }

    public int getEquipAmount() {
	int carriedItems = 0;
	for (int i : c.playerEquipment) {
	    if (i != -1) {
		carriedItems++;
	    }
	}
	return carriedItems;
    }

    public int getEquipItemSlot(int itemId) {
	itemId++;
	for (int i = 0; i < c.playerEquipment.length; i++) {
	    if (c.playerEquipment[i] == itemId) {
		return i;
	    }
	}
	return -1;
    }

    public Item getInventoryItem(int itemId) {
    	if (itemId < 0) {
    		return new Item(0, 0, 0);
    	}

    	for (int i = 0; i < c.playerItems.length; i++) {
    		int id = c.playerItems[i] - 1;
    		if (id == itemId) {
    			return new Item(id, c.playerItemsN[i], i);
    		}
    	}

    	return new Item(0, 0, 0);
    }

    public int getItemAmount2(int itemId) {
    	if (itemId <= 0) {
    		return 0;
		}

		if (ItemProjectInsanity.itemIsStackable(itemId)) {
    		for (int slot = 0; slot < c.playerItemsN.length; slot++) {
    			if (c.playerItems[slot] == itemId) {
    				return c.playerItemsN[slot];
				}
			}
		}

		int amount = 0;

		for (int slot = 0; slot < c.playerItemsN.length; slot++) {
			if (c.playerItems[slot] == itemId) {
				amount += 1;
			}
		}

		return amount;
	}

    /**
     * Gets the item amount.
     *
     * @param ItemID
     * @return
     */
	public int getItemAmount(int ItemID) {
		if (ItemID <= 0) {
			return 0;
		}

		int length = c.playerItems.length;
		if (ItemProjectInsanity.itemStackable[ItemID]) {
			ItemID++;
			for (int i = 0; i < length; i++) {
				if (c.playerItems[i] == ItemID) {
					return c.playerItemsN[i];
				}
			}

			return 0;
		}

		ItemID++;
		int itemCount = 0;
		for (int i = 0; i < length; i++) {
			if (c.playerItems[i] == ItemID) {
				itemCount += c.playerItemsN[i];
			}
		}

		return itemCount;
	}

    public int getItemAmount(int ItemID, int slot) {
    	int itemCount = 0;
    	if (ItemProjectInsanity.itemStackable[ItemID]) {
    		if ((c.playerItems[slot] - 1) == ItemID) {
    			itemCount += c.playerItemsN[slot];
    		}
    	} else {
    		for (int i = 0; i < c.playerItems.length; i++) {
    			if ((c.playerItems[i] - 1) == ItemID) {
    				itemCount += c.playerItemsN[i];
    			}
    		}
    	}

    	return itemCount;
    }

    public int getItemAmountInSlot(int slot) {
    	if (c.playerItems[slot] > 0) {
    		return c.playerItemsN[slot];
    	}

    	return 0;
    }

    /**
     * Counts (a) player's items.
     *
     * @param itemID
     * @return count start
     */
    public int getItemCount(int itemID) {
    	itemID++;
		int count = 0;
		for (int j = 0; j < c.playerItems.length; j++) {
		    if (c.playerItems[j] == itemID) {
			count += c.playerItemsN[j];
		    }
		}
		return count;
    }

    /**
     * Gets the item slot.
     *
     * @param ItemID
     * @return
     */
    public int getItemSlot(int ItemID) {
    	ItemID++;
		for (int i = 0, length = c.playerItems.length; i < length; i++) {
		    if (c.playerItems[i] == ItemID) {
		    	return i;
		    }
		}

		return -1;
    }

    public Item getItem(int slot) {
    	if (slot < 0 || slot >= c.playerItems.length) {
    		return null;
		}

		return new Item(c.playerItems[slot] - 1, c.playerItemsN[slot], slot);
	}

    public int getRandomFoodNoted() {
		FoodToWin random = FoodToWin.values[Misc.random(FoodToWin.values.length - 1)];
		for (FoodToWin value : FoodToWin.values) {
		    if (value == random) {
		    	return value.getId() + 1;
		    }
		}

		return FoodToWin.BASS.getId() + 1;
    }

    public int getTabforItem(int itemID) {
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (c.bankItems0[i] == itemID + 1) {
    			return 0;
    		} else if (c.bankItems1[i] == itemID + 1 ) {
    			return 1;
    		} else if (c.bankItems2[i] == itemID + 1) {
    			return 2;
    		} else if (c.bankItems3[i] == itemID + 1) {
    			return 3;
    		} else if (c.bankItems4[i] == itemID + 1) {
    			return 4;
    		} else if (c.bankItems5[i] == itemID + 1) {
    			return 5;
    		} else if (c.bankItems6[i] == itemID + 1) {
    			return 6;
    		} else if (c.bankItems7[i] == itemID + 1) {
    			return 7;
    		} else if (c.bankItems8[i] == itemID + 1) {
    			return 8;
    		}
    	}
    	return c.bankingTab;// if not in bank add to current tab
    }

    public int getTotalBonuses() { // get bonuses even for stuff in inventory
    	return c.getTotalBonuses();
    }

    /**
     * Gets the total count of (a) player's items, inside inventory and bank.
     *
     * @param itemID
     * @return
     */
    public int getTotalCount(int itemID) {
	int count = 0;
	for (int j = 0; j < c.playerItems.length; j++) {
	    if (c.playerItems[j] < 1) {
		continue;
	    }
	    if (ItemDef.forID(itemID
		    + 1).itemIsInNotePosition/*
					      * Item.itemIsNoteable[ itemID + 1]
					      */) {
		if (itemID + 2 == c.playerItems[j]) {
		    count += c.playerItemsN[j];
		}
	    }
	    if (!ItemDef.forID(
		    itemID + 1).itemIsInNotePosition/*
						     * !Item.itemIsNoteable[
						     * itemID + 1]
						     */) {
		if (itemID + 1 == c.playerItems[j]) {
		    count += c.playerItemsN[j];
		}
	    }
	}
	for (int j = 0; j < c.bankItems0.length; j++) {
	    if (c.bankItems0[j] == itemID + 1) {
		count += c.bankItems0N[j];
	    }
	}
	return count;
    }

	public void addOrDrop(GameItem item) {
		addOrDrop(item, null);
	}

	public void addOrDrop(GameItem item, String dropMessage) {
		int x;
		int y;
		int z;
		if (c.teleportToX > 0) {
			x = c.teleportToX;
			y = c.teleportToY;
			z = c.teleportToZ;
		} else {
			x = c.getX();
			y = c.getY();
			z = c.getZ();
		}

		if (item.isStackable()) {
			if (c.getPA().checkForMax(item)) {
				return;
			}

			if (c.getItems().addItem(item.getId(), item.getAmount(), true, dropMessage)) {
				return;
			}

			if (dropMessage != null && !dropMessage.isEmpty()) {
				c.sendMessage(dropMessage);
			}

			ItemHandler.createGroundItem(c, item.getId(), x, y, z, item.getAmount());
			return;
		}

		int amount = item.getAmount();
		for (int a = 1; a <= amount; a++) {
			if (c.getItems().addItem(item.getId(), 1)) {
				continue;
			}

			if (dropMessage != null && !dropMessage.isEmpty()) {
				c.sendMessage(dropMessage);
			}

			ItemHandler.createGroundItem(c, item.getId(), x, y, z, 1);
		}
    }

	public void giveBackUntradeables(Client killer, boolean killedInWildy) {
		boolean untradeableLost = false;
		boolean lostVoid = false;
		for (int i = 0, length = c.itemsToKeep.size(); i < length; i++) {
			ComparableItem item = c.itemsToKeep.get(i);
			int itemId = item.getId();
			if (itemId > 0) {
				int untradeablePrice = UntradeableManager.price(itemId);
				boolean untradeable = untradeablePrice > 0;
				if (untradeable && /*c.difficulty != PlayerDifficulty.IRONMAN &&*/ killedInWildy && killer != null
						&& killer != c) {
					if (c.wildLevel >= 20) {
						if (UntradeableManager.isVoidGear(itemId)) {
							lostVoid = true;
							continue;
						}
					}

					untradeableLost = true;
					UntradeableManager.add(c, itemId, item.getAmount());
				} else {
					c.getItems().addItem(itemId, item.getAmount());
				}
			}
		}
		if (untradeableLost) {
			c.sendMessage("You can purchase your untradeables back from Grim Reaper near coffins at home.");
		}
		if (lostVoid) {
			c.sendMessage("Your void gear vanished.");
		}
		c.itemsToKeep.clear();
	}

    public void grabUntradeables() {
    	// int loopCount = 0;
    	// for (int i = 0; i < Config.ITEM_TRADEABLE.length; i++) {
    	for (int i = 0, length = c.playerItems.length; i < length; i++) {
    		int playerItem = c.playerItems[i];
    		if (playerItem > 0) {
    			// loopCount++;
    			int realId = playerItem - 1;
    			if (realId == ResourceArea.PACKAGE_ITEM_ID) {
    				continue;
    			}

    			if ((!ItemProjectInsanity.itemIsTradeable(realId) && !ItemProjectInsanity.itemIsChargeItem(realId)) || (c.isIronMan() && realId == DonationChest.KEY)) { // if (realId ==
    				// Config.ITEM_TRADEABLE[i]
    				// ||
    				// ItemDef.forID(realId).noteId
    				// ==
    				// Config.ITEM_TRADEABLE[i])
    				// {
    				c.itemsToKeep.add(new ComparableItem(realId, c.playerItemsN[i]));
    			}
    		}
    	}
    	for (int i = 0, length = c.playerEquipment.length; i < length; i++) {
    		int itemId = c.playerEquipment[i];
    		if (itemId > 0) {
    			// loopCount++;
    			if (!ItemProjectInsanity.itemIsTradeable(itemId) && !ItemProjectInsanity.itemIsChargeItem(itemId)) { // if (itemId ==
    				// Config.ITEM_TRADEABLE[i])
    				// {
    				c.itemsToKeep.add(new ComparableItem(itemId, c.playerEquipmentN[i]));
    			}
    		}
    	}
    	// }
    	// System.out.println("loop count = "+loopCount);
    }

    public boolean hasAllMari() {
	return playerHasItem(6868, 5) && playerHasItem(6870, 5) && playerHasItem(6869, 5);
    }

    public boolean hasAllMario() {
	return playerHasItem(6865, 5) && playerHasItem(6867, 5) && playerHasItem(6866, 5);
    }

    /**
     * Checks if the player has all the shards.
     *
     * @return
     */
    public boolean hasAllShards() {
	return playerHasItem(11710, 1) && playerHasItem(11712, 1) && playerHasItem(11714, 1);
    }

    public boolean hasBlueArms() {
	return playerHasItem(6877, 1);
    }

    public boolean hasBlueHead() {
	return playerHasItem(6876, 1);
    }

    public boolean hasBlueMari() {
	return playerHasItem(6864, 1) && playerHasItem(6878, 1);
    }

    public boolean hasBlueTorso() {
	return playerHasItem(6875, 1);
    }

    public boolean hasEquippedWeapon(int... itemIds) {
    	for (int i = 0, len = itemIds.length; i < len; i++) {
    		if (c.playerEquipment[PlayerConstants.playerWeapon] == itemIds[i]) {
    			return true;
    		}
    	}
    	return false;
    }

    public boolean hasGreenArms() {
	return playerHasItem(6881, 1);
    }

    public boolean hasGreenHead() {
	return playerHasItem(6880, 1);
    }

    public boolean hasGreenMari() {
	return playerHasItem(6864, 1) && playerHasItem(6882, 1);
    }

    public boolean hasGreenTorso() {
	return playerHasItem(6879, 1);
    }

    public boolean hasHandles() {
	return playerHasItem(946, 1) && playerHasItem(960, 1);
    }

    public boolean hasPlank() {
	return playerHasItem(8794, 1) && playerHasItem(1511, 1);
    }

    public boolean hasRedArms() {
	return playerHasItem(6873, 1);
    }

    public boolean hasRedHead() {
	return playerHasItem(6872, 1);
    }

    public boolean hasRedMari() {
	return playerHasItem(6864, 1) && playerHasItem(6874, 1);
    }

    public boolean hasRedTorso() {
	return playerHasItem(6871, 1);
    }

    public void hybridSet() {
		if (c.getItems().freeSlots() > 23) {
			if (!c.getItems().takeCoins(3_000_000)) {
				c.sendMessage("You need 3 million gp to buy this PK set.");
				return;
			}

		    c.getItems().addItem(10828, 1);
		    c.getItems().addItem(1127, 1);
		    c.getItems().addItem(1079, 1);
		    c.getItems().addItem(1704, 1);
		    c.getItems().addItem(1725, 1);
		    c.getItems().addItem(6568, 1);
		    c.getItems().addItem(2550, 1);
		    c.getItems().addItem(3105, 1);
		    c.getItems().addItem(4587, 1);
		    c.getItems().addItem(1215, 1);
		    c.getItems().addItem(6524, 1);
		    c.getItems().addItem(4101, 1);
		    c.getItems().addItem(4103, 1);
		    c.getItems().addItem(2414, 1);
		    c.getItems().addItem(3842, 1);
		    c.getItems().addItem(4675, 1);
		    c.getItems().addItem(555, 600);
		    c.getItems().addItem(560, 400);
		    c.getItems().addItem(565, 200);
		    c.getItems().addItem(386, 100);
		    c.getItems().addItem(2441, 15);
		    c.getItems().addItem(2437, 15);
		    c.getItems().addItem(6686, 15);
		    c.getItems().addItem(3025, 20);
		} else {
		    c.sendMessage("You must have at least 24 inventory spaces to buy this item.");
		}
    }

    /**
     * Checks if the item is stackable.
     *
     * @param itemID
     * @return
     */
    public boolean isStackable(int itemID) {
	return ItemProjectInsanity.itemStackable[itemID];
    }

    /**
     * Checking item amounts.
     *
     * @param itemID
     * @return
     */
    public int itemAmount(int itemID) {
	int tempAmount = 0;
	for (int i = 0; i < c.playerItems.length; i++) {
	    if (c.playerItems[i] == itemID) {
		tempAmount += c.playerItemsN[i];
	    }
	}
	return tempAmount;
    }

	public long itemAmountLong(int itemID) {
		itemID++;
		long tempAmount = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] == itemID) {
				tempAmount += c.playerItemsN[i];
			}
		}
		return tempAmount;
	}

    /**
     * Check items with itemID++
     *
     * @param itemID
     * @return
     */
    public int itemAmountLoweredID(int itemID) {
	itemID++;
	int tempAmount = 0;
	for (int i = 0; i < c.playerItems.length; i++) {
	    if (c.playerItems[i] == itemID) {
		tempAmount += c.playerItemsN[i];
	    }
	}
	return tempAmount;
    }

    public boolean itemInBank(int itemId) {
	itemId++;
	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
	    if (c.bankItems0[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems1[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems2[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems3[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems4[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems5[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems6[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems7[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	    if (c.bankItems8[i] == itemId) {
		// c.sendMessage("inBank True");
		return true;
	    }
	}
	return false;
    }

	public boolean itemInBankNotPlaceholder(int itemId) {
		itemId++;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (c.bankItems0[i] == itemId && c.bankItems0N[i] > 0) {
				return true;
			}
			if (c.bankItems1[i] == itemId && c.bankItems1N[i] > 0) {
				return true;
			}
			if (c.bankItems2[i] == itemId && c.bankItems2N[i] > 0) {
				return true;
			}
			if (c.bankItems3[i] == itemId && c.bankItems3N[i] > 0) {
				return true;
			}
			if (c.bankItems4[i] == itemId && c.bankItems4N[i] > 0) {
				return true;
			}
			if (c.bankItems5[i] == itemId && c.bankItems5N[i] > 0) {
				return true;
			}
			if (c.bankItems6[i] == itemId && c.bankItems6N[i] > 0) {
				return true;
			}
			if (c.bankItems7[i] == itemId && c.bankItems7N[i] > 0) {
				return true;
			}
			if (c.bankItems8[i] == itemId && c.bankItems8N[i] > 0) {
				return true;
			}
		}
		return false;
	}

    /**
     * Item kept on death
     **/
    public void keepItem(int keepItem, boolean deleteItem) {
	// try {
	int value = 0;
	int item = 0;
	int slotId = 0;
	boolean itemInInventory = false;
	// boolean foundItem = false;
	for (int i = 0, length = c.playerItems.length; i < length; i++) {
		int itemId = c.playerItems[i] - 1;
	    if (itemId <= 0) {
	    	continue;
	    }
	    if (itemId == ResourceArea.PACKAGE_ITEM_ID) {
	    	continue;
	    }
		if (c.itemsToKeep.contains(new ComparableItem(itemId, c.playerItemsN[i]))) {
		    continue;
		}
		ItemDefinition itemDef = ItemDefinition.forId(itemId);
		if (itemDef == null) {
		    System.out.println("Null on itemDef id " + itemId);
		    continue;
		}
		int inventoryItemValue = itemDef.getDropValue();// c.getShops().getItemShopValue(c.playerItems[i]
											       // -
											       // 1);
		if (inventoryItemValue < 1) {
			inventoryItemValue = ShopPrice.getItemShopValue(itemId);
		}
		if (inventoryItemValue > value && (!c.invSlot[i])) {
		    value = inventoryItemValue;
		    item = itemId;
		    slotId = i;
		    itemInInventory = true;
		}
	}
	for (int i1 = 0; i1 < c.playerEquipment.length; i1++) {
	    if (c.playerEquipment[i1] > 0) {
		// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
		// if (c.playerEquipment[i1] == Config.ITEM_TRADEABLE[jj]) {
		// foundItem = true;
		// continue;
		// }
		// }
		// if (foundItem) {
		// foundItem = false;
		// continue;
		// }
		if (c.itemsToKeep.contains(new ComparableItem(c.playerEquipment[i1], c.playerEquipmentN[i1]))) {
		    continue;
		}
		ItemDefinition itemDef = ItemDefinition.forId(c.playerEquipment[i1]);
		if (itemDef == null) {
		    continue;
		}
		int equipmentItemValue = itemDef.getDropValue();// c.getShops().getItemShopValue(c.playerEquipment[i1]);
		if (equipmentItemValue < 1) {
			equipmentItemValue = ShopPrice.getItemShopValue(c.playerEquipment[i1]);
		}
		if (equipmentItemValue > value && (!c.equipSlot[i1])) {
		    value = equipmentItemValue;
		    item = c.playerEquipment[i1];
		    slotId = i1;
		    itemInInventory = false;
		}
	    }
	}
	if (itemInInventory) {
	    c.invSlot[slotId] = true;
	    if (deleteItem) {
		deleteItemInOneSlot(c.playerItems[slotId] - 1, getItemSlot(c.playerItems[slotId] - 1), 1);
	    }
	} else {
	    c.equipSlot[slotId] = true;
	    if (deleteItem) {
		deleteEquipment(item, slotId, 1);
	    }
	}
	c.itemKeptId[keepItem] = item;
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
    }

    public void magicSet() {
		if (c.getItems().freeSlots() > 15) {
			if (!c.getItems().takeCoins(2_000_000)) {
				c.sendMessage("You need 2 million gp to buy this PK set.");
				return;
			}
	
		    c.getItems().addItem(3755, 1);
		    c.getItems().addItem(4101, 1);
		    c.getItems().addItem(4103, 1);
		    c.getItems().addItem(2579, 1);
		    c.getItems().addItem(2414, 1);
		    c.getItems().addItem(3842, 1);
		    c.getItems().addItem(1704, 1);
		    c.getItems().addItem(2550, 1);
		    c.getItems().addItem(4675, 1);
		    c.getItems().addItem(555, 1200);
		    c.getItems().addItem(560, 800);
		    c.getItems().addItem(565, 400);
		    c.getItems().addItem(386, 100);
		    c.getItems().addItem(3041, 15);
		    c.getItems().addItem(6686, 15);
		    c.getItems().addItem(3025, 20);
		} else {
		    c.sendMessage("You must have at least 16 inventory spaces to buy this item.");
		}
    }

    /**
     * Makes the godsword blade.
     */
    public void makeBlade() {
	deleteItemInOneSlot(11710, 1);
	deleteItemInOneSlot(11712, 1);
	deleteItemInOneSlot(11714, 1);
	addItem(11690, 1);
	c.sendMessage("You combine the shards to make a blade.");
    }

    public void makeBlueMari() {
	deleteItemInOneSlot(6864, 1);
	deleteItemInOneSlot(6878, 1);
	addItem(6868, 1);
	c.sendMessage("You make a Blue Marionette.");
    }

    /**
     * Makes the godsword.
     *
     * @param i
     */
    public void makeGodsword(int i) {
	// int godsword = i - 8;
	if (playerHasItem(11690) && playerHasItem(i)) {
	    deleteItemInOneSlot(11690, 1);
	    deleteItemInOneSlot(i, 1);
	    addItem(i - 8, 1);
	    c.sendMessage("You combine the hilt and the blade to make a godsword.");
	}
    }

    public void makeGreenMari() {
	deleteItemInOneSlot(6864, 1);
	deleteItemInOneSlot(6882, 1);
	addItem(6869, 1);
	c.sendMessage("You make a Green Marionette.");
    }

    public void makeHandles() {
	deleteItemInOneSlot(960, 1);
	addItem(6864, 1);
	c.sendMessage("You make some Marionette Handles.");
    }

    public void makePlank() {
	deleteItemInOneSlot(1511, 1);
	addItem(960, 1);
	c.sendMessage("You make an Plank.");
    }

    public void makeRedMari() {
	deleteItemInOneSlot(6864, 1);
	deleteItemInOneSlot(6874, 1);
	addItem(6870, 1);
	c.sendMessage("You make a Red Marionette.");
    }

    public void meleeSet() {
		if (c.getItems().freeSlots() > 18) {
			if (!c.getItems().takeCoins(2_000_000)) {
				c.sendMessage("You need 2 million gp to buy this PK set.");
				return;
			}
	
		    c.getItems().addItem(10828, 1);
		    c.getItems().addItem(1127, 1);
		    c.getItems().addItem(1079, 1);
		    c.getItems().addItem(4131, 1);
		    c.getItems().addItem(2550, 1);
		    c.getItems().addItem(1725, 1);
		    c.getItems().addItem(6568, 1);
		    c.getItems().addItem(6524, 1);
		    c.getItems().addItem(4587, 1);
		    c.getItems().addItem(1215, 1);
		    c.getItems().addItem(557, 500);
		    c.getItems().addItem(9075, 200);
		    c.getItems().addItem(560, 100);
		    c.getItems().addItem(386, 100);
		    c.getItems().addItem(2441, 15);
		    c.getItems().addItem(2437, 15);
		    c.getItems().addItem(2443, 15);
		    c.getItems().addItem(6686, 15);
		    c.getItems().addItem(3025, 20);
		} else {
		    c.sendMessage("You must have at least 19 inventory spaces to buy this item.");
		}
    }

    /**
     * Moving Items in your bag.
     **/
    public void moveItems(int from, int to, int moveWindow, boolean insertMode) {
    	if (moveWindow == 3214) {
    		int tempI;
    		int tempN;
    		tempI = c.playerItems[from];
    		tempN = c.playerItemsN[from];
    		c.playerItems[from] = c.playerItems[to];
    		c.playerItemsN[from] = c.playerItemsN[to];
    		if (c.playerItemsN[from] < 0) {
    			System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 7784");
    		}
    		c.playerItems[to] = tempI;
    		c.playerItemsN[to] = tempN;
    		if (c.playerItemsN[to] < 0) {
    			System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 5564");
    		}
    	}

    	if (c.isBanking && moveWindow == 5382 && from >= 0 && to >= 0 && from < Config.BANK_TAB_SIZE
    			&& to < Config.BANK_TAB_SIZE && to < Config.BANK_TAB_SIZE) {
    		if (insertMode) {
    			int tempFrom = from;
    			for (int tempTo = to; tempFrom != tempTo;) {
    				if (tempFrom > tempTo) {
    					swapBankItem(tempFrom, tempFrom - 1);
    					tempFrom--;
    				} else if (tempFrom < tempTo) {
    					swapBankItem(tempFrom, tempFrom + 1);
    					tempFrom++;
    				}
    			}
    		} else {
    			swapBankItem(from, to);
    		}
    	}

    	if (moveWindow == 5382 && c.isBanking) {
    		resetBank();
    	}
    	if (moveWindow == 5064) {
    		int tempI;
    		int tempN;
    		tempI = c.playerItems[from];
    		tempN = c.playerItemsN[from];

    		c.playerItems[from] = c.playerItems[to];
    		c.playerItemsN[from] = c.playerItemsN[to];
    		if (c.playerItemsN[from] < 0) {
    			System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 1156");
    		}
    		c.playerItems[to] = tempI;
    		c.playerItemsN[to] = tempN;
    		if (c.playerItemsN[to] < 0) {
    			System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 1182");
    		}
    		c.setUpdateInvInterface(3214); // resetItems(3214);
    	}
    	resetTempItems();
    	if (moveWindow == 3214) {
    		c.setUpdateInvInterface(3214); // resetItems(3214);
    	}

    }

    /**
     * Checks if a player owns a cape.
     *
     * @return
     */
    public boolean ownsCape() {
	if (c.getItems().playerHasItem(2412, 1) || c.getItems().playerHasItem(2413, 1)
		|| c.getItems().playerHasItem(2414, 1)) {
	    return true;
	}
	for (int j = 0; j < Config.BANK_TAB_SIZE; j++) {
	    if (c.bankItems0[j] == 2412 || c.bankItems0[j] == 2413 || c.bankItems0[j] == 2414) {
		return true;
	    }
	}
	if (c.playerEquipment[PlayerConstants.playerCape] == 2413
		|| c.playerEquipment[PlayerConstants.playerCape] == 2414
		|| c.playerEquipment[PlayerConstants.playerCape] == 2415) {
	    return true;
	}
	return false;
    }

    public boolean playerHasEquipped(int itemID) {
	for (int element : c.playerEquipment) {
	    if (element == itemID) {
		return true;
	    }
	}
	return false;
    }

    public boolean playerHasEquipped(int itemId, int slot) {
    	if (slot < 0 || slot >= c.playerEquipment.length) {
    		return false;
		}

		return c.playerEquipment[slot] == itemId;
	}
    
    public boolean playerHasEquippedHat(int itemID) {
    	return c.playerEquipment[PlayerConstants.playerHat] == itemID;
    }

    public boolean playerHasEquippedWeapon(int itemID) {
    	return c.playerEquipment[PlayerConstants.playerWeapon] == itemID;
    }

    public boolean playerHasEquippedWeapons(int[] itemIDs) {
    	for (int itemID : itemIDs) {
    		if (c.playerEquipment[PlayerConstants.playerWeapon] == itemID) {
    			return true;
    		}
    	}

    	return false;
    }

    public boolean playerHasEquippedShield(int itemID) {
    	return c.playerEquipment[PlayerConstants.playerShield] == itemID;
    }

    public boolean playerHasItemSafe(int itemID) {
	return itemID <= 0 ? false : playerHasItem(itemID);
    }
    
    public boolean playerHasItem(Item item) {
    	final int i = item.getSlot();
    	return c.playerItems[i] == item.getId()+1 && c.playerItemsN[i] >= item.getAmount();
    }

    public boolean playerHasItem(int itemID) {
	itemID++;
	for (int playerItem : c.playerItems) {
	    if (playerItem == itemID) {
		return true;
	    }
	}
	return false;
    }

    public boolean playerHasAnyItem(int... itemIDs) {
    	for (int playerItem : c.playerItems) {
    		playerItem--;
    		for (int itemID : itemIDs) {
    			if (playerItem == itemID) {
    				return true;
    			}
    		}
    	}
    	return false;
    }
    
    public boolean playerHasItemStrict(int... itemIDs) {
    	int count = 0;
    	int fullMask = 0;
    	for (int i = 0; i < itemIDs.length; i++) {
    		fullMask = Misc.setBit(fullMask, i);
    	}
    	for (int playerItem : c.playerItems) {
    		playerItem--;
    		for (int bit = 0; bit < itemIDs.length; bit++) {
    			if (playerItem == itemIDs[bit]) {
    				count = Misc.setBit(count, bit);
    			}
    		}
    	}
    	if (count == fullMask)
    		return true;
    	
    	return false;
    }

    public int countItems(int... itemIDs) {
    	long count = 0;
		for (int i = 0, length = c.playerItems.length; i < length; i++) {
			int playerItem = c.playerItems[i] - 1;
			for (int itemID : itemIDs) {
				if (playerItem == itemID) {
					count += c.playerItemsN[i];
				}
			}
		}

		return (int) Math.min(count, Integer.MAX_VALUE);
    }

    public boolean playerHasItemRange(int itemIdMin, int itemIdMax) {
    	itemIdMin++;
    	itemIdMax++;
	for (int playerItem : c.playerItems) {
	    if (playerItem >= itemIdMin && playerItem <= itemIdMax) {
		return true;
	    }
	}
	return false;
    }

    public boolean playerHasItem(int itemID, int amt) {
	itemID++;
	int found = 0;
	for (int i = 0; i < c.playerItems.length; i++) {
	    if (c.playerItems[i] == itemID) {
		if (c.playerItemsN[i] >= amt) {
		    return true;
		} else {
		    found++;
		}
	    }
	}
	if (found >= amt) {
	    return true;
	}
	return false;
    }

    /**
     * Checks if the player has the item.
     *
     * @param itemID
     * @param slot
     * @param amt
     * @return
     */
    public boolean playerHasItem(int itemID, int slot, int amt) {
	// int found = 0;
	if (itemID < 0 || amt < 0 || slot < 0) {
	    return false;
	}
	if (slot >= c.playerItems.length) {
	    return false;
	}
	// if (c.playerItems[slot] == (itemID)) {
	// for (int i = 0; i < c.playerItems.length; i++) {
	// if (c.playerItems[i] == itemID) {
	// if (c.playerItemsN[i] >= amt) {
	// return true;
	// } else {
	// found++;
	// }
	// }
	// }
	// if (found >= amt) {
	// return true;
	// }
	// return false;
	// }

	if (c.playerItems[slot] - 1 == itemID && c.playerItemsN[slot] >= amt) {
	    return true;
	}

	// c.sendMessage("return false "+(c.playerItems[slot]-1)+" itemid
	// "+itemID);
	return false;
    }

    /*
     * used for item verification
     */
    public boolean playerHasItemInSlot(int itemID, int slot) {
    	itemID++;
    	if (c.playerItems[slot] == itemID && c.playerItemsN[slot] >= 1) {
    		return true;
    	}
    	return false;
    }

    public void pureSet() {
		if (c.getItems().freeSlots() > 22) {
			if (!c.getItems().takeCoins(3_000_000)) {
				c.sendMessage("You need 3 million gp to buy this PK set.");
				return;
			}
	
		    c.getItems().addItem(662, 1);
		    c.getItems().addItem(1704, 1);
		    c.getItems().addItem(6107, 1);
		    c.getItems().addItem(6108, 1);
		    c.getItems().addItem(3842, 1);
		    c.getItems().addItem(3105, 1);
		    c.getItems().addItem(11126, 1);
		    c.getItems().addItem(2550, 1);
		    c.getItems().addItem(4587, 1);
		    c.getItems().addItem(1215, 1);
		    c.getItems().addItem(2414, 1);
		    c.getItems().addItem(2495, 1);
		    c.getItems().addItem(9185, 1);
		    c.getItems().addItem(9244, 40);
		    c.getItems().addItem(4675, 1);
		    c.getItems().addItem(555, 600);
		    c.getItems().addItem(560, 400);
		    c.getItems().addItem(565, 200);
		    c.getItems().addItem(386, 100);
		    c.getItems().addItem(2441, 15);
		    c.getItems().addItem(2437, 15);
		    c.getItems().addItem(6686, 15);
		    c.getItems().addItem(3025, 20);
		} else {
		    c.sendMessage("You must have at least 23 inventory spaces to buy this item.");
		}
    }

    public void rangeSet() {
		if (c.getItems().freeSlots() > 21) {
			if (!c.getItems().takeCoins(2_000_000)) {
				c.sendMessage("You need 2 million gp to buy this PK set.");
				return;
			}
	
		    c.getItems().addItem(10828, 1);
		    c.getItems().addItem(2503, 1);
		    c.getItems().addItem(1079, 1);
		    c.getItems().addItem(4131, 1);
		    c.getItems().addItem(2489, 1);
		    c.getItems().addItem(10499, 1);
		    c.getItems().addItem(6524, 1);
		    c.getItems().addItem(1704, 1);
		    c.getItems().addItem(2550, 1);
		    c.getItems().addItem(9185, 1);
		    c.getItems().addItem(861, 1);
		    c.getItems().addItem(9144, 500);
		    c.getItems().addItem(892, 500);
		    c.getItems().addItem(868, 500);
		    c.getItems().addItem(557, 500);
		    c.getItems().addItem(9075, 200);
		    c.getItems().addItem(560, 100);
		    c.getItems().addItem(386, 100);
		    c.getItems().addItem(2445, 15);
		    c.getItems().addItem(2443, 15);
		    c.getItems().addItem(6686, 15);
		    c.getItems().addItem(3025, 20);
		} else {
		    c.sendMessage("You must have at least 22 inventory spaces to buy this item.");
		}
    }

    /**
     * BANK
     */

    public void rearrangeBank() {
	int totalItems = 0;
	int highestSlot = 0;
	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
	    if (c.bankingItems[i] != 0) {
		totalItems++;
		if (highestSlot <= i) {
		    highestSlot = i;
		}
	    }
	}

	for (int i = 0; i <= highestSlot; i++) {
	    if (c.bankingItems[i] == 0) {
		boolean stop = false;

		for (int k = i; k <= highestSlot; k++) {
		    if (c.bankingItems[k] != 0 && !stop) {
			int spots = k - i;
			for (int j = k; j <= highestSlot; j++) {
			    c.bankingItems[j - spots] = c.bankingItems[j];
			    c.bankingItemsN[j - spots] = c.bankingItemsN[j];
			    stop = true;
			    c.bankingItems[j] = 0;
			    c.bankingItemsN[j] = 0;
			}
		    }
		}
	    }
	}

	int totalItemsAfter = 0;
	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
	    if (c.bankingItems[i] != 0) {
		totalItemsAfter++;
	    }
	}

	if (totalItems != totalItemsAfter) {
	    System.out.println("BANKING BUG FIND THIS SHIT NOW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}
    }

    public void refreshBonuses() {
		resetBonus();
		getBonus();
		writeBonus();
    }
    
    public void refreshWeightAndTotalBonus() {
    	int bonuses = 0;
    	double weight = 0.0D;
    	
    	c.setVenomImmunity(false);
    	
    	for (int i = 0, len = c.playerEquipment.length; i < len; i++) {
    		int itemId = c.playerEquipment[i];
			if (itemId > -1) {
				ItemDefinition def = ItemDefinition.forId(itemId);
				if (def == null) continue;
				
				if (def.getCombatStats().isVenomImmunity()) { // apply venom immunity from equipment
					c.setVenomImmunity(true);
				}
				
				for (int k = 0; k < c.getPlayerBonus().length; k++) {
					if (k == EquipBonusIds.ATTACK_STAB) {
						bonuses += Math.max(def.getCombatStats().getBonus(EquipBonusIds.ATTACK_STAB), Math.max(def.getCombatStats().getBonus(EquipBonusIds.ATTACK_SLASH), def.getCombatStats().getBonus(EquipBonusIds.ATTACK_CRUSH)));
						k = EquipBonusIds.ATTACK_CRUSH;
						continue;
					}
					if (k == EquipBonusIds.DEF_STAB) {
						bonuses += Math.max(def.getCombatStats().getBonus(EquipBonusIds.DEF_STAB), Math.max(def.getCombatStats().getBonus(EquipBonusIds.DEF_SLASH), def.getCombatStats().getBonus(EquipBonusIds.DEF_CRUSH)));
						k = EquipBonusIds.DEF_CRUSH;
						continue;
					}
					bonuses += def.getCombatStats().getBonus(k);
				}
				weight += def.getWeight();
			}
		}

		for (int i = 0; i < c.playerItems.length; i++) {
			int itemId = c.playerItems[i] - 1; // real item id
			if (itemId < 0) continue;
			ItemDefinition def = ItemDefinition.forId(itemId);
			if (def == null) continue;
			for (int k = 0; k < c.getPlayerBonus().length; k++) {
				if (k == EquipBonusIds.ATTACK_STAB) {
					bonuses += Math.max(def.getCombatStats().getBonus(EquipBonusIds.ATTACK_STAB), Math.max(def.getCombatStats().getBonus(EquipBonusIds.ATTACK_SLASH), def.getCombatStats().getBonus(EquipBonusIds.ATTACK_CRUSH)));
					k = EquipBonusIds.ATTACK_CRUSH;
					continue;
				}
				if (k == EquipBonusIds.DEF_STAB) {
					bonuses += Math.max(def.getCombatStats().getBonus(EquipBonusIds.DEF_STAB), Math.max(def.getCombatStats().getBonus(EquipBonusIds.DEF_SLASH), def.getCombatStats().getBonus(EquipBonusIds.DEF_CRUSH)));
					k = EquipBonusIds.DEF_CRUSH;
					continue;
				}
				bonuses += def.getCombatStats().getBonus(k);
			}
			weight += def.getWeight();
		}
		
		
		c.setWeight(weight);
		c.setTotalBonuses(bonuses);
    }
    	
    /**
     * Removes all items from player's equipment.
     */
    public void removeAllItems() {
	for (int i = 0; i < c.playerItems.length; i++) {
	    c.playerItems[i] = 0;
	}
	for (int i = 0; i < c.playerItemsN.length; i++) {
	    c.playerItemsN[i] = 0;
	}
	c.setUpdateInvInterface(3214); // resetItems(3214);
    }

    public void removeBankItem(int itemId) {
	itemId++;
	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
	    if (c.bankItems0[i] == itemId) {
		c.bankItems0[i] = 0;
		c.bankItems0N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems1[i] == itemId) {
		c.bankItems1[i] = 0;
		c.bankItems1N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems2[i] == itemId) {
		c.bankItems2[i] = 0;
		c.bankItems2N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems3[i] == itemId) {
		c.bankItems3[i] = 0;
		c.bankItems3N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems4[i] == itemId) {
		c.bankItems4[i] = 0;
		c.bankItems4N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems5[i] == itemId) {
		c.bankItems5[i] = 0;
		c.bankItems5N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems6[i] == itemId) {
		c.bankItems6[i] = 0;
		c.bankItems6N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems7[i] == itemId) {
		c.bankItems7[i] = 0;
		c.bankItems7N[i] = 0;
		c.getItems().resetBank();
	    }
	    if (c.bankItems8[i] == itemId) {
		c.bankItems8[i] = 0;
		c.bankItems8N[i] = 0;
		c.getItems().resetBank();
	    }
	}
    }

    public boolean removeBankItem(int itemId, int amount) {
    	itemId++;
    	boolean removed = false;
    	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
    		if (removed)
    			break;
    		if (c.bankItems0[i] == itemId) {
    			if (c.bankItems0N[i] == 0) break;//bank placeholder
    			if (c.bankItems0N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems0[i] = 0;
    				removed = true;
    			}
    			c.bankItems0N[i] -= amount;
    			if (c.bankItems0N[i] < 0)
    				c.bankItems0N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems1[i] == itemId) {
    			if (c.bankItems1N[i] == 0) break;//bank placeholder
    			if (c.bankItems1N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems1[i] = 0;
    				removed = true;
    			}
    			c.bankItems1N[i] -= amount;
    			if (c.bankItems1N[i] < 0)
    				c.bankItems1N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems2[i] == itemId) {
    			if (c.bankItems2N[i] == 0) break;//bank placeholder
    			if (c.bankItems2N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems2[i] = 0;
    				removed = true;
    			}
    			c.bankItems2N[i] -= amount;
    			if (c.bankItems2N[i] < 0)
    				c.bankItems2N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems3[i] == itemId) {
    			if (c.bankItems3N[i] == 0) break;//bank placeholder
    			if (c.bankItems3N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems3[i] = 0;
    				removed = true;
    			}
    			c.bankItems3N[i] -= amount;
    			if (c.bankItems3N[i] < 0)
    				c.bankItems3N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems4[i] == itemId) {
    			if (c.bankItems4N[i] == 0) break;//bank placeholder
    			if (c.bankItems4N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems4[i] = 0;
    				removed = true;
    			}
    			c.bankItems4N[i] -= amount;
    			if (c.bankItems4N[i] < 0)
    				c.bankItems4N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems5[i] == itemId) {
    			if (c.bankItems5N[i] == 0) break;//bank placeholder
    			if (c.bankItems5N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems5[i] = 0;
    				removed = true;
    			}
    			c.bankItems5N[i] -= amount;
    			if (c.bankItems5N[i] < 0)
    				c.bankItems5N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems6[i] == itemId) {
    			if (c.bankItems6N[i] == 0) break;//bank placeholder
    			if (c.bankItems6N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems6[i] = 0;
    				removed = true;
    			}
    			c.bankItems6N[i] -= amount;
    			if (c.bankItems6N[i] < 0)
    				c.bankItems6N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems7[i] == itemId) {
    			if (c.bankItems7N[i] == 0) break;//bank placeholder
    			if (c.bankItems7N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems7[i] = 0;
    				removed = true;
    			}
    			c.bankItems7N[i] -= amount;
    			if (c.bankItems7N[i] < 0)
    				c.bankItems7N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    		if (c.bankItems8[i] == itemId) {
    			if (c.bankItems8N[i] == 0) break;//bank placeholder
    			if (c.bankItems8N[i] <= amount) {
    				if (!c.isPlaceholderEnabled())
    					c.bankItems8[i] = 0;
    				removed = true;
    			}
    			c.bankItems8N[i] -= amount;
    			if (c.bankItems8N[i] < 0)
    				c.bankItems8N[i] = 0;
    			removed = true;
    			c.getItems().resetBank();
    		}
    	}
    	return removed;
    }

    /**
     * Pickup items from the ground.
     **/
    public void removeGroundItem(int itemID, int itemX, int itemY, int Amount) {
	if (c.isBot()) {
	    return;
	}

	if (c.getOutStream() == null || !c.isActive) {
	    return;
	}
	// synchronized (c) {
	c.getOutStream().createPacket(85);
	c.getOutStream().writeByteC((itemY - 8 * c.mapRegionY));
	c.getOutStream().writeByteC((itemX - 8 * c.mapRegionX));
	c.getOutStream().createPacket(156);
	c.getOutStream().writeByteS(0);
	c.getOutStream().write3Byte(itemID);
	c.flushOutStream();
	// }
    }
    
    public boolean forceUnequip(int equipmentSlot) {
    	return removeItem(c.playerEquipment[equipmentSlot], equipmentSlot);
    }

    /**
     * Removes a wielded item.
     **/
    public boolean removeItem(int wearID, int slot) {
    	if (c.playerEquipment[slot] != wearID) {
    		return false;
    	}
    	if (c.getOutStream() != null && c != null) {
    		if (c.playerEquipment[slot] > -1) {
    			if (addItem(c.playerEquipment[slot], c.playerEquipmentN[slot])) {

    				if (c.isThereNexArmourEquipped(slot)) {
    					c.getItems().takeOfNexPiece(wearID);
    				}

    				c.playerEquipment[slot] = -1;
    				c.playerEquipmentN[slot] = 0;
    				// cheaphax fix to switching back to weapons that use
    				// defensive style but only had 3
    				// attack styles
    				// if ((c.getFightXp() == FightExp.MELEE_DEFENSE ||
    				// c.getFightXp() ==
    				// FightExp.RANGED_DEFENSE) && c.fightMode == 2) {
    				// c.fightMode = 3;
    				// }
    				c.getCombat().resetPlayerAttack();
    				c.setUpdateEquipment(true, slot);
    				if (slot == PlayerConstants.playerWeapon) {
    					c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
    					sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
    							ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
    				}
    				if (!c.isBot() && c.isActive && c.getOutStream() != null) {
    					c.getOutStream().createVariableShortPacket(34);
    					c.getOutStream().writeShort(1688);
    					c.getOutStream().writeByte(slot);
    					c.getOutStream().write3Byte(0);
    					c.getOutStream().writeByte(0);
    					c.getOutStream().endFrameVarSizeWord();
    					c.flushOutStream();
    				}

    				c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
    				return true;
    			}
    		}
    	}
    	return false;
    }

    public void removeItemForBot(int wearID, int slot) {
	if (c.getBot().playerEquipment[slot] != wearID) {
	    return;
	}
	if (c.getBot().duelRule[DuelRules.SAME_WEAPONS] && slot == 3) {
	    c.sendMessage("[Bot]Removing weapons has been disabled in this duel!");
	    return;
	}
	if (c.getOutStream() != null && c != null) {
	    if (c.getBot().playerEquipment[slot] > -1) {
		if (c.getBot().getItems().addItem(c.getBot().playerEquipment[slot],
			c.getBot().playerEquipmentN[slot])) {
		    c.getBot().playerEquipment[slot] = -1;
		    c.getBot().playerEquipmentN[slot] = 0;
		    sendWeapon(c.getBot().playerEquipment[PlayerConstants.playerWeapon],
			    ItemConstants.getItemName(c.getBot().playerEquipment[PlayerConstants.playerWeapon]));
		    c.getBot().getItems().resetBonus();
		    c.getBot().getItems().getBonus();
		    writeBonus();
		    c.getBot().getCombat().getPlayerAnimIndex(c.getBot().playerEquipment[PlayerConstants.playerWeapon]);
		    c.getOutStream().createVariableShortPacket(34);
		    c.getOutStream().writeShort(1688);
		    c.getOutStream().writeByte(slot);
		    c.getOutStream().write3Byte(0);
		    c.getOutStream().writeByte(0);
		    c.getOutStream().endFrameVarSizeWord();
		    c.flushOutStream();
		}
	    }
	}
    }

    /**
     * Replaces an equipment item with the specified replaceItem and updates
     * player
     *
     * @param slot
     *            equipment id slot
     * @param replaceItem
     *            the new item
     */
    public void replaceEquipment(int slot, int replaceItem) {
    	if (c.playerEquipment[slot] > 0) {
		    if (c.isThereNexArmourEquipped(slot)) {
		    	c.getItems().takeOfNexPiece(c.playerEquipment[slot]);
		    }

    		c.playerEquipment[slot] = replaceItem;
    		if (replaceItem <= 0) {
    			c.playerEquipmentN[slot] = 0;
    		} else {
    			c.playerEquipmentN[slot] = 1;
    		}
    		c.setUpdateEquipment(true, slot); // c.getItems().updateSlot(slot);
    		c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
    	}
    }
    
	public void refreshWeapon() {
		final int displayOnlyWeapon = c.displayEquipment[PlayerConstants.playerWeapon];
		final int weaponId = displayOnlyWeapon > 0 ? displayOnlyWeapon : c.playerEquipment[PlayerConstants.playerWeapon];
		MeleeData.getPlayerAnimIndex(c, weaponId);
		c.getItems().addSpecialBar(false);
		c.setUpdateEquipment(true, PlayerConstants.playerWeapon);
		c.getItems().sendWeapon(weaponId, ItemConstants.getItemName(weaponId));
	}

	public void replaceItem(int toReplace, int replacement) {
		if (playerHasItem(toReplace, 1)) {
			deleteItemInOneSlot(toReplace, getItemSlot(toReplace), 1);
			addItem(replacement, 1);
		}
	}

	public void replaceItemAll(int toReplace, int replacement) {
		for (int i = 0, length = c.playerItems.length; i < length; i++) {
			replaceItem(toReplace, replacement);
		}
	}

    public boolean switchToNote(int itemId, int slot, int amount) {
    	ItemDef def = ItemDef.forID(itemId);
    	if (def == null)
    		return false;
    	if (def.itemIsInNotePosition || !def.itemCanBeNoted())
    		return false;
    	if (deleteItemInOneSlot(itemId, slot, amount)) {
    		if (addItem(def.getNoteId(), amount)) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public boolean unNoteItem(int itemId, int slot, int amount) {
    	ItemDef def = ItemDef.forID(itemId);
    	if (def == null)
    		return false;
    	if (!def.itemIsInNotePosition || def.itemCanBeNoted())
    		return false;
    	if (freeSlots() < 1)
    		return false;
    	if (deleteItemInOneSlot(itemId, slot, amount)) {
    		if (addItem(def.getNoteId(), amount)) {
    			return true;
    		}
    	}
    	return false;
    }

    public void showEquipment(Player other) {
    	c.attr().put("viewing_equipment", true);
		c.getPacketSender().itemsOnInterface2(1688, other.playerEquipment, other.playerEquipmentN);
		c.getPacketSender().setSidebarInterfaceAndRemoveButtons(1644);
	}

	public void showBank(Player other) {
    	if (!other.isActive || c.getOutStream() == null) {
    		return;
		}

		c.getPA().sendTabs(other);
		c.getPacketSender().showInterface(5292);
		c.getPacketSender().sendFrame126("" + other.getPA().bankingCount(), 22033);
		c.getPacketSender().sendFrame126("     " + other.getNameSmartUp() + "'s Bank", 5383);
		c.getPacketSender().itemsOnInterface(5382, other.bankingItems, other.bankingItemsN);
	}

	public void copyInventory(Player other) {
    	for (int i = 0; i < other.playerItems.length; i++) {
			int id = c.playerItems[i] - 1;
			if (id == 5733) { // rotten potato
				continue;
			}

    		c.playerItems[i] = other.playerItems[i];
			c.playerItemsN[i] = other.playerItemsN[i];
		}
    	updateInventory();
	}

	public void copyBank(Player other) {
    	for (int i = 0; i < c.bankingItems.length; i++) {
    		c.bankingItems[i] = other.bankingItems[i];
    		c.bankingItemsN[i] = other.bankingItemsN[i];

    		c.bankItems0[i] = other.bankItems0[i];
			c.bankItems0N[i] = other.bankItems0N[i];

    		c.bankItems1[i] = other.bankItems1[i];
			c.bankItems1N[i] = other.bankItems1N[i];

    		c.bankItems2[i] = other.bankItems2[i];
			c.bankItems2N[i] = other.bankItems2N[i];

    		c.bankItems3[i] = other.bankItems3[i];
			c.bankItems3N[i] = other.bankItems3N[i];

    		c.bankItems4[i] = other.bankItems4[i];
			c.bankItems4N[i] = other.bankItems4N[i];

    		c.bankItems5[i] = other.bankItems5[i];
			c.bankItems5N[i] = other.bankItems5N[i];

    		c.bankItems6[i] = other.bankItems6[i];
			c.bankItems6N[i] = other.bankItems6N[i];

    		c.bankItems7[i] = other.bankItems7[i];
			c.bankItems7N[i] = other.bankItems7N[i];

    		c.bankItems8[i] = other.bankItems8[i];
			c.bankItems8N[i] = other.bankItems8N[i];
		}

	}

	public void copyEquipment(Player other) {
    	for (int i = 0; i < c.playerEquipment.length; i++) {
			c.playerEquipment[i] = other.playerEquipment[i];
			c.playerEquipmentN[i] = other.playerEquipmentN[i];
		}

		c.getPacketSender().itemsOnInterface2(1688, c.playerEquipment, c.playerEquipmentN);

    	if (c.playerEquipment[PlayerConstants.playerWeapon] > 0) {
			final int weapon = c.playerEquipment[PlayerConstants.playerWeapon];

			ItemDefinition def = ItemDefinition.forId(weapon);
			if (def != null && def.getName() != null) {
				c.getItems().sendWeapon(weapon, def.getName());
			}
		}

		c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

    /**
     * Reseting your bank.
     */
	public void resetBank() {

		if (c.toggleBot) {
			resetBotBank();
			return;
		}

		if (c.getX() == 3308 && c.getY() == 3120) {
			c.getPA().updateGearBonusesInBank();
		}

		if (!c.isBot() && c.refreshBank) {
			if (c.isActive && c.getOutStream() != null) {
				c.getOutStream().createVariableShortPacket(53);
				c.getOutStream().write3Byte(5382); // bank
				c.getOutStream().writeShort(Config.BANK_TAB_SIZE);
			}
		}
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
				if (c.bankingItemsN[i] > 254) {
					c.getOutStream().writeByte(255);
					c.getOutStream().writeDWord_v2(c.bankingItemsN[i]);
				} else {
					c.getOutStream().writeByte(c.bankingItemsN[i]);
				}
			}
//			if (!c.isPlaceholderEnabled() && c.bankingItemsN[i] < 1) {
//				c.bankingItems[i] = 0;
//			}
			if (c.bankingItems[i] > Config.ITEM_LIMIT || c.bankingItems[i] < 0) {
				c.bankingItems[i] = 0;
			}
			if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
				c.getOutStream().write3Byte(c.bankingItems[i]);
			}
		}
		if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
		if (c.refreshBank && c.isActive && c.getOutStream() != null) {
			c.getPacketSender().sendFrame126(Integer.toString(c.getPA().bankingCount()), 22033);
		}

	}

	public void resetBank(int[] itemData, int[] amountData) {
		if (c.isActive && c.getOutStream() != null) {
			c.getOutStream().createVariableShortPacket(53);
			c.getOutStream().write3Byte(5382); // bank
			c.getOutStream().writeShort(Config.BANK_TAB_SIZE);
		}
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (c.isActive && c.getOutStream() != null) {
				if (amountData[i] > 254) {
					c.getOutStream().writeByte(255);
					c.getOutStream().writeDWord_v2(amountData[i]);
				} else {
					c.getOutStream().writeByte(amountData[i]);
				}
			}
			if (amountData[i] < 1) {
				itemData[i] = 0;
			}
			if (itemData[i] > Config.ITEM_LIMIT || itemData[i] < 0) {
				itemData[i] = 0;// Config.ITEM_LIMIT;
			}
			if (c.isActive && c.getOutStream() != null) {
				c.getOutStream().write3Byte(itemData[i]);
			}
		}
		if (c.isActive && c.getOutStream() != null) {
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
	}

    public void resetBonus() {
    	for (int i = 0, length = c.getPlayerBonus().length; i < length; i++) {
    		c.getPlayerBonus()[i] = 0;
    	}
    }

    public void resetBotBank() {
	if (!c.isBot()) {
	    c.getOutStream().createVariableShortPacket(53);
	    c.getOutStream().write3Byte(5382); // bank
	    c.getOutStream().writeShort(Config.BANK_TAB_SIZE);
	}
	for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
	    if (!c.isBot()) {
		if (c.getBot().bankingItemsN[i] > 254) {
		    c.getOutStream().writeByte(255);
		    c.getOutStream().writeDWord_v2(c.getBot().bankingItemsN[i]);
		} else {
		    c.getOutStream().writeByte(c.getBot().bankingItemsN[i]);
		}
	    }
	    if (c.getBot().bankingItemsN[i] < 1) {
		c.getBot().bankingItems[i] = 0;
	    }
	    if (c.getBot().bankingItems[i] > Config.ITEM_LIMIT || c.getBot().bankingItems[i] < 0) {
		c.getBot().bankingItems[i] = Config.ITEM_LIMIT;
	    }
	    if (!c.isBot()) {
		c.getOutStream().write3Byte(c.getBot().bankingItems[i]);
	    }
	}
	if (!c.isBot()) {
	    c.getOutStream().endFrameVarSizeWord();
	    c.flushOutStream();
	}
    }

    public void resetInterfacesForToggleBot() {
	c.setUpdateInvInterface(3214); // resetItems(3214);
	resetItems(5064);
	resetBank();
    }

    public void setInventory(int WriteFrame, int[] array) {
    	int length = array.length;
	    c.getOutStream().createVariableShortPacket(53);
	    c.getOutStream().write3Byte(WriteFrame);
	    c.getOutStream().writeShort(length);
	    for (int i = 0; i < length; i++) {
	    	c.getOutStream().writeByte(1);
			c.getOutStream().write3Byte(array[i]);
	    }
	    c.getOutStream().endFrameVarSizeWord();
	    c.flushOutStream();
	}

    /**
     * Empties all of (a) player's items.
     */
	public void resetItems(int inventoryId) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) {
			return;
		}

		if (c.toggleBot) {
			c.getPacketSender().itemsOnInterface(inventoryId, c.getBot().playerItems, c.getBot().playerItemsN);
			return;
		}

		c.getPacketSender().itemsOnInterface(inventoryId, c.playerItems, c.playerItemsN);
	}

    // Allantois addition
    public void resetItems(int writeFrame, int[] array) {
	if (c.getOutStream() != null && c.isActive) {
	    c.getOutStream().createVariableShortPacket(53);
	    c.getOutStream().write3Byte(writeFrame);
	    c.getOutStream().writeShort(array.length);
	    for (int element : array) {
		c.getOutStream().writeByte(1);
		c.getOutStream().write3Byte(element);
	    }
	    c.getOutStream().endFrameVarSizeWord();
	    c.flushOutStream();
	}
    }

    /**
     * Reset items kept on death.
     **/
    public void resetKeepItems() {
	for (int i = 0; i < c.itemKeptId.length; i++) {
	    c.itemKeptId[i] = -1;
	}
	for (int i1 = 0; i1 < c.invSlot.length; i1++) {
	    c.invSlot[i1] = false;
	}
	for (int i2 = 0; i2 < c.equipSlot.length; i2++) {
	    c.equipSlot[i2] = false;
	}
    }

    /**
     * Resets temporary worn items. Used in minigames, etc
     */
    public void resetTempItems() {
	if (c.toggleBot && c.getBot() != null) {
	    resetTempItemsToggleBot();
	    return;
	}
	// synchronized(c) {
	int itemCount = 0;
	for (int i = 0; i < c.playerItems.length; i++) {
	    if (c.playerItems[i] > -1) {
		itemCount = i;
	    }
	}

	if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
	    c.getOutStream().createVariableShortPacket(53);
	    c.getOutStream().write3Byte(5064);
	    c.getOutStream().writeShort(itemCount + 1);
	}
	for (int i = 0; i < itemCount + 1; i++) {

	    if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
		if (c.playerItemsN[i] > 254) {
		    c.getOutStream().writeByte(255);
		    c.getOutStream().writeDWord_v2(c.playerItemsN[i]);
		} else {
		    if (c.playerItemsN[i] != -1) {
			c.getOutStream().writeByte(c.playerItemsN[i]);
		    }
		}
	    }
	    if (c.playerItems[i] > Config.ITEM_LIMIT || c.playerItems[i] < 0) {
		c.playerItems[i] = 0;// Config.ITEM_LIMIT;
	    }

	    if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
		c.getOutStream().write3Byte(c.playerItems[i]);
	    }
	}

	if (!c.isBot() && c.refreshBank && c.isActive && c.getOutStream() != null) {
	    c.getOutStream().endFrameVarSizeWord();
	    c.flushOutStream();
	}
	// }
    }

    public void resetTempItemsToggleBot() {
	// synchronized(c) {
	int itemCount = 0;
	for (int i = 0; i < c.getBot().playerItems.length; i++) {
	    if (c.getBot().playerItems[i] > -1) {
		itemCount = i;
	    }
	}

	if (!c.isBot()) {
	    c.getOutStream().createVariableShortPacket(53);
	    c.getOutStream().write3Byte(5064);
	    c.getOutStream().writeShort(itemCount + 1);
	}
	for (int i = 0; i < itemCount + 1; i++) {

	    if (!c.isBot()) {
		if (c.getBot().playerItemsN[i] > 254) {
		    c.getOutStream().writeByte(255);
		    c.getOutStream().writeDWord_v2(c.getBot().playerItemsN[i]);
		} else {
		    if (c.getBot().playerItemsN[i] != -1) {
			c.getOutStream().writeByte(c.getBot().playerItemsN[i]);
		    }
		}
	    }
	    if (c.getBot().playerItems[i] > Config.ITEM_LIMIT || c.getBot().playerItems[i] < 0) {
		c.getBot().playerItems[i] = Config.ITEM_LIMIT;
	    }

	    if (!c.isBot()) {
		c.getOutStream().write3Byte(c.getBot().playerItems[i]);
	    }
	}

	if (!c.isBot()) {
	    c.getOutStream().endFrameVarSizeWord();
	    c.flushOutStream();
	}
	// }
    }

    /**
     * Send the items kept on death.
     */
    public void sendItemsKept() {
	if (c.isBot()) {
	    return;
	}
	// synchronized (c) {
	if (c.getOutStream() != null && c.isActive) {
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6963);
		c.getOutStream().writeShort(c.itemKeptId.length);
		for (int i = 0; i < c.itemKeptId.length; i++) {
			if (c.playerItemsN[i] > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(1);
			} else {
				c.getOutStream().writeByte(1);
			}
			if (c.itemKeptId[i] > 0) {
				c.getOutStream().write3Byte(c.itemKeptId[i] + 1);
			} else {
				c.getOutStream().write3Byte(0);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}
	// }
    }
    
    public void sendWeapon(int weaponId, String weaponName) {
    	if (c.isBot()) {
    		return;
    	}
    	
    	if (c.queueGmaulSpec) {
    		c.queueGmaulSpec = false;
    	}
    	
    	c.setRangeWeapon(RangeWeapon.get(weaponId));
    	
    	c.setCombatDistance(CombatFormulas.getCombatDistanceToSet(c));

//    	boolean sameWeapon = false;
//    	if (c.getLastWeaponSent() == weaponId) {
//    		sameWeapon = true;
//    	}
//    	c.setlastWeaponSent(weaponId);
    	
    	CombatTab prevTab = c.getCombatTab();
    	
    	if (weaponName == null) {
    		weaponName = "Unarmed";
    	}
    	
    	CombatTab tab = CombatTab.getByWeaponOrUnarmed(weaponId);
    	c.setCombatTab(tab);
    	
		c.getPacketSender().setSidebarInterface(0, tab.getTabId());
		c.getPacketSender().sendFrame126(weaponName, tab.getInterfaceStringId());
		
    	if (c.lastButtonWasControlledStyle && prevTab != null && tab.getTabId() != prevTab.getTabId() && tab.getButtons().length == 4)
    		c.fightMode = 2;

    	if (prevTab != null && tab.getTabId() != prevTab.getTabId() && prevTab.getButtons().length > tab.getButtons().length) { // more buttons than the previous one
    		if (prevTab.getButtons().length == 4 && c.fightMode == 2)
    			c.lastButtonWasControlledStyle = true;
    		if (prevTab.getButtons()[c.fightMode].getAttackType() == WeaponAttackTypes.DEFENSIVE)
    			c.fightMode = tab.getButtons().length - 1;
    		else {
    			if (c.fightMode != 0)
    				c.fightMode = 1;
    		}
    	}
//    	if (c.fightMode >= tab.getButtons().length) {
//    		c.fightMode = tab.getButtons().length-1;
//    	}
    	
    	CombatTab.setButton(c, c.fightMode, tab);

       	//storing the controlled attack style for when switching back to 4 attack styles weapon from a 3 style weapon, it will set to controlled again.
    	if (tab.getButtons().length == 4 && c.fightMode == 2)
    		c.lastButtonWasControlledStyle = true;
    	else if (tab.getButtons().length == 4 && c.fightMode != 2)
    		c.lastButtonWasControlledStyle = false;
    	
    	//System.out.println("tab:"+tab+" fightMode:"+c.fightMode+" weaponName:"+weaponName);
    	c.setWeaponAnimation(c.getCombat().getWepAnim(weaponName));
    	
    	switch (c.sidebarInterfaceInfo.getOrDefault(0, -1)) {
	    	case 5855:
	    	case 8460:
	    		c.getPacketSender().setConfig(43, ((c.fightMode + 1) % 3));
	    		break;
    		default:
    			c.getPacketSender().setConfig(43, c.fightMode);
    			break;
    	}
    }

    /**
     * Updates the equipment tab.
     **/
    public void setEquipment(int wearID, int amount, int targetSlot) {
	// synchronized (c) {
	if (!c.toggleBot) {
	    c.playerEquipment[targetSlot] = wearID;
	    c.playerEquipmentN[targetSlot] = amount;
	    
	    if (targetSlot == PlayerConstants.playerWeapon) {
	    	c.getCombat().getPlayerAnimIndex(wearID);
	    }
	}

	if (c.getOutStream() == null || !c.isActive) {
	    return;
	}

	if (!c.isBot()) {
	    // c.getOutStream().createVariableShortPacket(34);
	    // c.getOutStream().writeWord(1688);
	    // c.getOutStream().writeByte(targetSlot);
	    // c.getOutStream().write3Byte(wearID + 1);
	    // if (amount > 254) {
	    // c.getOutStream().writeByte(255);
	    // c.getOutStream().writeDWord(amount);
	    // } else {
	    // c.getOutStream().writeByte(amount);
	    // }
	    // c.getOutStream().endFrameVarSizeWord();
	    // c.flushOutStream();
	    c.setUpdateEquipment(true, targetSlot);
	}

	c.getUpdateFlags().add(UpdateFlag.APPEARANCE);

	// }
    }

    /**
     * Special attack bar filling amount.
     * @param refreshWeapon TODO
     **/
    public void specialAmount(int weapon, double specAmount, int barId, boolean refreshWeapon) {
	c.specBarId = barId;
	c.getPacketSender().sendFrame70(specAmount >= 10 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 9 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 8 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 7 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 6 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 5 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 4 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 3 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 2 ? 500 : 0, 0, (--barId));
	c.getPacketSender().sendFrame70(specAmount >= 1 ? 500 : 0, 0, (--barId));
	// long nano = System.nanoTime();
	updateSpecialBar();
	// System.out.println("Delay: "+(System.nanoTime()));
	if (refreshWeapon) {
	sendWeapon(weapon, ItemConstants.getItemName(weapon));
	}
    }

    public void swapBankItem(int from, int to) {
	// int tempI = c.bankItems[from];
	// int tempN = c.bankItemsN[from];
	// c.bankItems[from] = c.bankItems[to];
	// c.bankItemsN[from] = c.bankItemsN[to];
	// c.bankItems[to] = tempI;
	// c.bankItemsN[to] = tempN;

	int tempI = -1;
	int tempN = -1;

	switch (c.bankingTab) {
	case 0:
	    tempI = c.bankItems0[from];
	    tempN = c.bankItems0N[from];
	    c.bankItems0[from] = c.bankItems0[to];
	    c.bankItems0N[from] = c.bankItems0N[to];
	    c.bankItems0[to] = tempI;
	    c.bankItems0N[to] = tempN;
	    break;

	case 1:
	    tempI = c.bankItems1[from];
	    tempN = c.bankItems1N[from];
	    c.bankItems1[from] = c.bankItems1[to];
	    c.bankItems1N[from] = c.bankItems1N[to];
	    c.bankItems1[to] = tempI;
	    c.bankItems1N[to] = tempN;
	    break;

	case 2:
	    tempI = c.bankItems2[from];
	    tempN = c.bankItems2N[from];
	    c.bankItems2[from] = c.bankItems2[to];
	    c.bankItems2N[from] = c.bankItems2N[to];
	    c.bankItems2[to] = tempI;
	    c.bankItems2N[to] = tempN;
	    break;

	case 3:
	    tempI = c.bankItems3[from];
	    tempN = c.bankItems3N[from];
	    c.bankItems3[from] = c.bankItems3[to];
	    c.bankItems3N[from] = c.bankItems3N[to];
	    c.bankItems3[to] = tempI;
	    c.bankItems3N[to] = tempN;
	    break;

	case 4:
	    tempI = c.bankItems4[from];
	    tempN = c.bankItems4N[from];
	    c.bankItems4[from] = c.bankItems4[to];
	    c.bankItems4N[from] = c.bankItems4N[to];
	    c.bankItems4[to] = tempI;
	    c.bankItems4N[to] = tempN;
	    break;

	case 5:
	    tempI = c.bankItems5[from];
	    tempN = c.bankItems5N[from];
	    c.bankItems5[from] = c.bankItems5[to];
	    c.bankItems5N[from] = c.bankItems5N[to];
	    c.bankItems5[to] = tempI;
	    c.bankItems5N[to] = tempN;
	    break;

	case 6:
	    tempI = c.bankItems6[from];
	    tempN = c.bankItems6N[from];
	    c.bankItems6[from] = c.bankItems6[to];
	    c.bankItems6N[from] = c.bankItems6N[to];
	    c.bankItems6[to] = tempI;
	    c.bankItems6N[to] = tempN;
	    break;

	case 7:
	    tempI = c.bankItems7[from];
	    tempN = c.bankItems7N[from];
	    c.bankItems7[from] = c.bankItems7[to];
	    c.bankItems7N[from] = c.bankItems7N[to];
	    c.bankItems7[to] = tempI;
	    c.bankItems7N[to] = tempN;
	    break;

	case 8:
	    tempI = c.bankItems8[from];
	    tempN = c.bankItems8N[from];
	    c.bankItems8[from] = c.bankItems8[to];
	    c.bankItems8N[from] = c.bankItems8N[to];
	    c.bankItems8[to] = tempI;
	    c.bankItems8N[to] = tempN;
	    break;
	}

	if (c.bankingTab > 0 && (from == 0 || to == 0)) {
	    c.getPacketSender().sendFrame126b("1", 27000);
	    c.getPA().openUpBank(c.bankingTab);
	}
    }

    public void takeOfNexPiece(int itemId) {
    	if (c.getZones().isInEdgePk()) { // already set to 0 so no need to reduce
    		return;
    	}
	    int hpDeduct = 0;
	    switch(itemId) {
    	    case 20135:
    	    case 20147:
    	    case 20159:
    	    case 30266://elder chaos hood
    		    hpDeduct = RSConstants.NEX_ARMOR_HELM_HP;
    		    break;
    	    case 20163:
    	    case 20151:
    	    case 20139:
    	    case 30264://elder chaos top
    		    hpDeduct = RSConstants.NEX_ARMOR_BODY_HP;
    		    break;
    	    case 20167:
    	    case 20155:
    	    case 20143:
    	    case 30265://elder chaos robe
    		    hpDeduct = RSConstants.NEX_ARMOR_LEGS_HP;
    		    break;
	    }
	    
    	int maxHp = c.calculateMaxLifePoints() - hpDeduct;
	    if (c.getHitPoints() > maxHp) {
	    	c.getSkills().setLifepoints(maxHp);
	    }
    }

    public void toTab(int tab, int fromSlot) {
    	if (tab == c.bankingTab) {
    		return;
    	}
    	if (fromSlot < 0 || fromSlot > c.bankingItems.length) {
    		return;
    	}
    	if (tab > c.getPA().getTabCount() + 1) {
    		return;
    	}
    	if (c.getPA().getBankItems(tab) >= Config.BANK_TAB_SIZE) {
    		c.sendMessage("You can't store any more items in this tab!1");
    		return;
    	}
    	int id = c.bankingItems[fromSlot];
    	int amount = c.bankingItemsN[fromSlot];
    	
    	if (amount == 0) {
    		c.sendMessage("You cannot move place holder items into tabs.");
    		return;
    	}
    	
    	int[] invItems = new int[28];
    	int[] invItemsN = new int[28];
    	for (int i = 0; i < c.playerItems.length; i++) {
    		invItems[i] = c.playerItems[i];
    		invItemsN[i] = c.playerItemsN[i];
    		c.playerItems[i] = 0;
    		c.playerItemsN[i] = 0;
    	}
    	c.playerItems[0] = id;
    	c.playerItemsN[0] = amount;
    	if (c.playerItemsN[0] < 0) {
    		System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 23448");
    	}
    	c.bankingItems[fromSlot] = 0;
    	c.bankingItemsN[fromSlot] = 0;
    	if (tab == 0) {
    		bankItem(id, 0, amount, c.bankItems0, c.bankItems0N);
    	} else if (tab == 1) {
    		bankItem(id, 0, amount, c.bankItems1, c.bankItems1N);
    	} else if (tab == 2) {
    		bankItem(id, 0, amount, c.bankItems2, c.bankItems2N);
    	} else if (tab == 3) {
    		bankItem(id, 0, amount, c.bankItems3, c.bankItems3N);
    	} else if (tab == 4) {
    		bankItem(id, 0, amount, c.bankItems4, c.bankItems4N);
    	} else if (tab == 5) {
    		bankItem(id, 0, amount, c.bankItems5, c.bankItems5N);
    	} else if (tab == 6) {
    		bankItem(id, 0, amount, c.bankItems6, c.bankItems6N);
    	} else if (tab == 7) {
    		bankItem(id, 0, amount, c.bankItems7, c.bankItems7N);
    	} else if (tab == 8) {
    		bankItem(id, 0, amount, c.bankItems8, c.bankItems8N);
    	}
    	for (int i = 0; i < invItems.length; i++) {
    		c.playerItems[i] = invItems[i];
    		c.playerItemsN[i] = invItemsN[i];
    		if (c.playerItemsN[i] < 0) {
    			System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 4665");
    		}
    	}
    	c.getPA().openUpBank(c.bankingTab);// refresh
    	c.getPA().openUpBank(c.bankingTab);// refresh twice to ensure update
    }

    public void updateInventory() {
    	c.getItems().resetItems(3214);
    }

    /**
     * Updates the slot when wielding an item.
     *
     * @param slot
     */
    public void updateSlot(int slot) {
	if (c.isBot()) {
	    return;
	}
	// synchronized (c) {
	if (c.getOutStream() != null && c.isActive) {
	    c.getOutStream().createVariableShortPacket(34);
	    c.getOutStream().writeShort(1688);
	    c.getOutStream().writeByte(slot);
	    c.getOutStream().write3Byte(c.playerEquipment[slot] + 1);
	    if (c.playerEquipmentN[slot] > 254) {
		c.getOutStream().writeByte(255);
		c.getOutStream().writeInt(c.playerEquipmentN[slot]);
	    } else {
		c.getOutStream().writeByte(c.playerEquipmentN[slot]);
	    }
	    c.getOutStream().endFrameVarSizeWord();
	    c.flushOutStream();
	}
	// }

    }

    /**
     * Special attack text.
     **/
    public void updateSpecialBar() {
	String percent = Double.toString(c.specAmount);
	if (percent.contains(".")) {
	    percent = percent.replace(".", "");
	}
	if (percent.startsWith("0") && !percent.equals("00")) {
	    percent = percent.replace("0", "");
	}
	if (percent.startsWith("0") && percent.equals("00")) {
	    percent = percent.replace("00", "0");
	}
	c.getPacketSender().sendFrame126(
		c.usingSpecial ? "<col=ffff00>Special Attack (" + percent + "%)" : "Special Attack (" + percent + "%)",
		c.specBarId);
    }

    public static boolean canWearItem(Player c, ItemDefinition wearItemDef, boolean message) {
		if (wearItemDef == null) {
			if (message) {
				c.sendMessage("This item isn't added.");
			}
			return false;
		}

    	if (!wearItemDef.isWearable()) {
    		if (message) {
    			c.sendMessage("This item is not a wearable.");
    		}
			return false;
		}

    	int wearID = wearItemDef.getId();
		if (wearItemDef.getSlot() == PlayerConstants.playerCape && ItemConstants.checkForMaxStats(wearID) && !c.getZones().isInDmmLobby() && !c.getZones().isInDmmTourn()) {
			if (!c.getPA().isMaxed() && (c.difficulty < PlayerDifficulty.PRESTIGE_ONE)) {
				if (message) {
					c.sendMessage("You do not meet the requirements to wear this item.");
				}
				return false;
			}
		}

		if (Config.itemRequirements) {
			if (wearItemDef != null) {
				for (int i = 0, length = wearItemDef.getRequirements().length; i < length; i++) {
					if (c.getSkills().getStaticLevel(i) < wearItemDef.getRequirements()[i]) {
						if (message) {
							String skillName = Skills.SKILL_NAME[i];
							c.sendMessage("You need " + Misc.aOrAn(skillName) + " " + skillName + " level of "
									+ wearItemDef.getRequirements()[i] + " to wear this item.");
						}
						return false;
					}
				}
			}
		}

		if (ItemConstants.femaleOnlyItem(wearID) && c.playerAppearance[0] == 0) {
			if (message) {
				c.sendMessage("You must be a female to wear this item.");
			}
			return false;
		}

		if (ItemConstants.maleOnlyItem(wearID) && c.playerAppearance[0] == 1) {
			if (message) {
				c.sendMessage("You must be a male to wear this item.");
			}
			return false;
		}

		return true;
    }

    /**
     * Wielding items.
     **/
    public boolean wearItem(int wearID, int slot) {
    	// if (!c.getItems().playerHasItem(wearID, 1, slot)) {
    	// return false;
    	// }
    	if (slot < 0 || slot >= c.playerItems.length) {
    		return false;
    	}
    	if (c.isBanking || c.openDuel || c.inTrade) {
    		return false;
    	}

    	// wearID = c.playerItems[slot] - 1;

    	if (wearID < 1) {
    		return false;
    	}

    	// synchronized (c) {
    	if (c.playerItems[slot] == (wearID + 1)) {
    		ItemDefinition wearItemDef = ItemDefinition.forId(wearID);

    		if (!canWearItem(c, wearItemDef, true)) {
    			return false;
    		}

    		int targetSlot = wearItemDef.getSlot();

    		if ((targetSlot == PlayerConstants.playerWeapon || targetSlot == PlayerConstants.playerShield) && MagicalCrate.dropMagicalCrateInWild(c)) {
    			c.sendMessage("The crate was dropped on the ground!");
    		}

       		if (c.duelStatus > 0 && !c.getTradeAndDuel().isFollowingDuelRules(wearID, targetSlot)) {
    			return false;
    		}

    		// int wearAmount = c.playerItemsN[slot];
    		if (c.playerItemsN[slot] < 1) { // if (wearAmount < 1) {
    			return false;
    		}

    		if (targetSlot == PlayerConstants.playerWeapon) {
    			c.getPA().resetAutocast();
    		}

    		int removedIntoExistingSlot = -1;
    		if (c.playerEquipment[targetSlot] != -1 && ItemProjectInsanity.itemStackable[c.playerEquipment[targetSlot]]) { // grab
    			// stackable
    			// existing
    			// item
    			// in
    			// inventory
    			removedIntoExistingSlot = getItemSlot(c.playerEquipment[targetSlot]);
    		}

    		if (slot >= 0 && wearID >= 0) {
    			int toEquip = c.playerItems[slot];
    			int toEquipN = c.playerItemsN[slot];
    			int toRemove = c.playerEquipment[targetSlot];
    			int toRemoveN = c.playerEquipmentN[targetSlot];

    			if (c.isThereNexArmourEquipped(targetSlot) && !RSConstants.isThereNexArmourEquipped(toEquip)) {
    				c.getItems().takeOfNexPiece(toRemove);
    			}

    			if (toRemove == CastleWars.SARA_BANNER || toRemove == CastleWars.ZAMMY_BANNER) {
    				CastleWars.dropFlag(c, toRemove);
    				toRemove = -1;
    				toRemoveN = 0;
    			}
    			// c.sendMessage("To remove "+toRemove);
    			if (toEquip == toRemove + 1 && ItemProjectInsanity.itemStackable[toRemove]) { // equipping
    				// stackable
    				// item
    				deleteItemInOneSlot(toRemove, getItemSlot(toRemove), toEquipN);
    				c.playerEquipmentN[targetSlot] += toEquipN;

    			} else if (removedIntoExistingSlot != -1 && toRemove != -1 && ItemProjectInsanity.itemStackable[toRemove]) {
    				if (wearItemDef.isTwoHanded() && c.playerEquipment[PlayerConstants.playerShield] > 0) {
    					c.playerItems[slot] = c.playerEquipment[PlayerConstants.playerShield] + 1;
						c.playerItemsN[slot] = c.playerEquipmentN[PlayerConstants.playerShield];

	    				c.playerItems[removedIntoExistingSlot] = toRemove + 1;
	    				c.playerItemsN[removedIntoExistingSlot] += toRemoveN;
	    				c.playerEquipment[targetSlot] = toEquip - 1;
	    				c.playerEquipmentN[targetSlot] = toEquipN;

	    				c.playerEquipment[PlayerConstants.playerShield] = -1;
						c.playerEquipmentN[PlayerConstants.playerShield] = 0;
						c.setUpdateEquipment(true, PlayerConstants.playerShield);
    				} else {
	    				c.playerItems[slot] = 0;
	    				c.playerItemsN[slot] = 0;

	    				c.playerItems[removedIntoExistingSlot] = toRemove + 1;
	    				c.playerItemsN[removedIntoExistingSlot] += toRemoveN;
	    				c.playerEquipment[targetSlot] = toEquip - 1;
	    				c.playerEquipmentN[targetSlot] = toEquipN;
    				}

    				// c.sendMessage("toRemove id:"+toRemove);
    				// System.out.println("stack "+Item.itemStackable[toRemove]
    				// );
    				// System.out.println("hasitem
    				// "+c.getItems().playerHasItem(toRemove) );
    			} else if (targetSlot != 5 && targetSlot != 3) {
    				c.playerItems[slot] = toRemove + 1;
    				c.playerItemsN[slot] = toRemoveN;
    				if (c.playerItemsN[slot] < 0) {
    					System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 12388");
    				}
    				c.playerEquipment[targetSlot] = toEquip - 1;
    				c.playerEquipmentN[targetSlot] = toEquipN;
    			} else if (targetSlot == 5) {
    				boolean wearing2h = ItemConstants.is2handed(c.playerEquipment[PlayerConstants.playerWeapon]);
    				// boolean wearingShield =
    				// c.playerEquipment[Player.playerShield] > 0;
    				if (wearing2h) {
    					toRemove = c.playerEquipment[PlayerConstants.playerWeapon];
    					toRemoveN = c.playerEquipmentN[PlayerConstants.playerWeapon];
    					if (toRemove == CastleWars.SARA_BANNER || toRemove == CastleWars.ZAMMY_BANNER) {
    						CastleWars.dropFlag(c, toRemove);
    						toRemove = -1;
    						toRemoveN = 0;
    					}
    					c.playerEquipment[PlayerConstants.playerWeapon] = -1;
    					c.playerEquipmentN[PlayerConstants.playerWeapon] = 0;
    					c.setUpdateEquipment(true, PlayerConstants.playerWeapon); // updateSlot(PlayerConstants.playerWeapon);
    				}
    				c.playerItems[slot] = toRemove + 1;
    				c.playerItemsN[slot] = toRemoveN;
    				if (c.playerItemsN[slot] < 0) {
    					System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 4998");
    				}
    				c.playerEquipment[targetSlot] = toEquip - 1;
    				c.playerEquipmentN[targetSlot] = toEquipN;
    			} else if (targetSlot == 3) {
    				boolean is2h = wearItemDef.isTwoHanded();
    				if (is2h) {
    					boolean wearingShield = c.playerEquipment[PlayerConstants.playerShield] > 0;
        				boolean wearingWeapon = c.playerEquipment[PlayerConstants.playerWeapon] > 0;
    					if (wearingShield && wearingWeapon) {
    						if (freeSlots() > 0) {
    							c.playerItems[slot] = toRemove + 1;
    							c.playerItemsN[slot] = toRemoveN;
    							if (c.playerItemsN[slot] < 0) {
    								System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 5775");
    							}
    							c.playerEquipment[targetSlot] = toEquip - 1;
    							c.playerEquipmentN[targetSlot] = toEquipN;
    							removeItem(c.playerEquipment[PlayerConstants.playerShield],
    									PlayerConstants.playerShield);
    						} else {
    							c.sendMessage("You do not have enough inventory space to do this.");
    							return false;
    						}
    					} else if (wearingShield && !wearingWeapon) {
    						c.playerItems[slot] = c.playerEquipment[PlayerConstants.playerShield] + 1;
    						c.playerItemsN[slot] = c.playerEquipmentN[PlayerConstants.playerShield];
    						if (c.playerItemsN[slot] < 0) {
    							System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 45228");
    						}
    						c.playerEquipment[targetSlot] = toEquip - 1;
    						c.playerEquipmentN[targetSlot] = toEquipN;
    						c.playerEquipment[PlayerConstants.playerShield] = -1;
    						c.playerEquipmentN[PlayerConstants.playerShield] = 0;
    						c.setUpdateEquipment(true, PlayerConstants.playerShield); // updateSlot(PlayerConstants.playerShield);
    					} else {
    						c.playerItems[slot] = toRemove + 1;
    						c.playerItemsN[slot] = toRemoveN;
    						if (c.playerItemsN[slot] < 0) {
    							System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 78922");
    						}
    						c.playerEquipment[targetSlot] = toEquip - 1;
    						c.playerEquipmentN[targetSlot] = toEquipN;
    					}
    				} else {
    					c.playerItems[slot] = toRemove + 1;
    					c.playerItemsN[slot] = toRemoveN;
    					if (c.playerItemsN[slot] < 0) {
    						System.out.println("ERROR SETS TO -1 HERE!!!!!!!!!! 6548");
    					}
    					c.playerEquipment[targetSlot] = toEquip - 1;
    					c.playerEquipmentN[targetSlot] = toEquipN;
    				}
    			}
    			c.setUpdateInvInterface(3214); // resetItems(3214);
    		}

    		// if (!c.isBot() && c.getOutStream() != null && c != null) {
    		// c.getOutStream().createVariableShortPacket(34);
    		// c.getOutStream().writeWord(1688);
    		// c.getOutStream().writeByte(targetSlot);
    		// c.getOutStream().write3Byte(wearID + 1);
    		//
    		// if (c.playerEquipmentN[targetSlot] > 254) {
    		// c.getOutStream().writeByte(255);
    		// c.getOutStream().writeDWord(
    		// c.playerEquipmentN[targetSlot]);
    		// } else {
    		// c.getOutStream().writeByte(
    		// c.playerEquipmentN[targetSlot]);
    		// }
    		//
    		// c.getOutStream().endFrameVarSizeWord();
    		// c.flushOutStream();
    		// }
    		c.setUpdateEquipment(true, targetSlot);

    		if (c.isBot()) {
    			BotClient bot = (BotClient) c;
    			if (bot.getOwnerClient() != null) {
    				bot.getOwnerClient().getItems().resetItems(3214);

    				bot.getOwnerClient().getOutStream().createVariableShortPacket(34);
    				bot.getOwnerClient().getOutStream().writeShort(1688);
    				bot.getOwnerClient().getOutStream().writeByte(targetSlot);
    				bot.getOwnerClient().getOutStream().write3Byte(wearID + 1);

    				if (c.playerEquipmentN[targetSlot] > 254) {
    					bot.getOwnerClient().getOutStream().writeByte(255);
    					bot.getOwnerClient().getOutStream().writeInt(c.playerEquipmentN[targetSlot]);
    				} else {
    					bot.getOwnerClient().getOutStream().writeByte(c.playerEquipmentN[targetSlot]);
    				}

    				bot.getOwnerClient().getOutStream().endFrameVarSizeWord();
    				bot.getOwnerClient().flushOutStream();
    				bot.getOwnerClient().getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
    						ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
    				bot.getOwnerClient().getItems().resetBonus();
    				bot.getOwnerClient().getItems().getBonus();
    				bot.getOwnerClient().getItems().writeBonus();
    			}
    		}

    		// cheaphax fix to switching back to weapons that use defensive
    		// style but only had 3 attack
    		// styles
    		// c.sendMess("fight mode was at:"+c.fightMode);
    		if ((c.getFightXp() == FightExp.MELEE_DEFENSE
    				|| c.getFightXp() == FightExp.RANGED_DEFENSE) && c.fightMode == 2) {
    			c.fightMode = 3;
    		}
    		// c.sendMess("fight mode is at:"+c.fightMode);
    		if (targetSlot == PlayerConstants.playerWeapon || targetSlot == PlayerConstants.playerShield) {
    			sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
    					ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
    			c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
    		}

    		c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
    		if (SeasonalData.isBeast(c)) {
    			SeasonalData.endBeastPower(c);
    		}

    		if (targetSlot == PlayerConstants.playerWeapon) {
    			c.usingSpecial = false;
    			addSpecialBar(false);
    		}
    		return true;
    	} else {
    		return false;
    	}
    	// }
    }

    /**
     * Indicates the action to wear an item.
     *
     * @param wearID
     * @param wearAmount
     * @param targetSlot
     */
    public void wearItem(int wearID, int wearAmount, int targetSlot) {
	// synchronized (c) {
	if (!c.isBot()/* && c.getOutStream() != null && c != null */) {
	    // c.getOutStream().createVariableShortPacket(34);
	    // c.getOutStream().writeWord(1688);
	    // c.getOutStream().writeByte(targetSlot);
	    // c.getOutStream().write3Byte(wearID + 1);
	    //
	    // if (wearAmount > 254) {
	    // c.getOutStream().writeByte(255);
	    // c.getOutStream().writeDWord(wearAmount);
	    // } else {
	    // c.getOutStream().writeByte(wearAmount);
	    // }
	    // c.getOutStream().endFrameVarSizeWord();
	    // c.flushOutStream();
	    c.setUpdateEquipment(true, targetSlot);

	    c.playerEquipment[targetSlot] = wearID;
	    c.playerEquipmentN[targetSlot] = wearAmount;

	    // cheaphax fix to switching back to weapons that use defensive
	    // style but only had 3 attack
	    // styles
	    if ((c.getFightXp() == FightExp.MELEE_DEFENSE
		    || c.getFightXp() == FightExp.RANGED_DEFENSE) && c.fightMode == 2) {
		c.fightMode = 3;
	    }

	    c.getCombat().resetPlayerAttack();
	    c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
		    ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
	    c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
	    c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	} else if (c.isBot()) {
	    c.playerEquipment[targetSlot] = wearID;
	    c.playerEquipmentN[targetSlot] = wearAmount;
	    c.getItems().resetBonus();
	    c.getItems().getBonus();
	    c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
	    c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	// }
    }

    /**
     * Gets the bonus' of an item.
     */
	public void writeBonus() {
		if (c.interfaceIdOpenMainScreen != 15106) {
			return;
		}

		for (int i = 0; i < EquipmentBonuses.values.length; i++) {
			EquipmentBonuses bonus = EquipmentBonuses.values[i];
			if (bonus.getStringId() == 0)
				continue;
			c.getPacketSender().sendFrame126(bonus.getStartText() + (c.getPlayerBonus()[i] > 0 ? "+" + Integer.toString(c.getPlayerBonus()[i]) : Integer.toString(c.getPlayerBonus()[i])), bonus.getStringId());
		}

		c.getPacketSender().sendFrame126(String.format(WEIGHT_STR, (int)c.getWeight()), 19155);
	}

	public boolean hasItem(int itemId) {
		return c.getItems().playerHasEquipped(itemId)
				|| c.getItems().playerHasItem(itemId)
				|| c.getItems().itemInBank(itemId);
	}

	private static final String WEIGHT_STR = "%d kg";

}
