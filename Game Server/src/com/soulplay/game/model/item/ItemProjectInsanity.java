package com.soulplay.game.model.item;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.urn.SkillingUrn;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.util.Misc;
import com.soulplay.util.PlayerLoopThread;

public class ItemProjectInsanity {

	public enum WILDSTALKERHELMET {
		TIER_1(20801, "Tier 1", 0, 2216),
		TIER_2(20802, "Tier 2", 10, 2239),
		TIER_3(20803, "Tier 3", 100, 2358),
		TIER_4(20804, "Tier 4", 500, 2432),
		TIER_5(20805, "Tier 5", 2000, 2433),
		TIER_6(20806, "Tier 6", 5000, 2434);

		public static WILDSTALKERHELMET getHelmetForID(int itemID) {
			switch (itemID) {
				case 20801:
					return TIER_1;
				case 20802:
					return TIER_2;
				case 20803:
					return TIER_3;
				case 20804:
					return TIER_4;
				case 20805:
					return TIER_5;
				case 20806:
					return TIER_6;
				default:
					return null;
			}
		}

		public final int id;

		public final String name;

		public final int killcount;

		public final int npc;

		WILDSTALKERHELMET(int id, String name, int killcount, int npc) {
			this.id = id;
			this.name = name;
			this.killcount = killcount;
			this.npc = npc;
		}
	}

	/**
	 * Checks if the item is stackable.
	 */
	public static boolean[] itemStackable = new boolean[Config.ITEM_LIMIT];

	private static boolean[] itemTradeable = new boolean[Config.ITEM_LIMIT];

	private static boolean[] itemSellable = new boolean[Config.ITEM_LIMIT];

	/**
	 * Checks if the item can wield into a slot.
	 */
	public static volatile boolean reloadItems = false;

	/**
	 * Gets an item name from the itemlist.
	 */
	public static String getItemName(int id) {
		ItemDefinition item = ItemDefinition.forId(id);
		if (item != null) {
			return item.getName();
		}
		return null;
	}

	public static WILDSTALKERHELMET getWildstalkerHelmet(int itemID) {

		return WILDSTALKERHELMET.getHelmetForID(itemID);
	}

	public static boolean isDungeoneeringItem(int itemID) {

		if (PlayerLoopThread.inDungFilter(itemID)) {
			return false;
		}

		if (itemID >= 15708 && itemID <= 17260) {
			return true;
		}
		if (itemID >= 17297 && itemID <= 18329) {
			return true;
		}
		return false;
	}

	/**
	 * Calls if an item is a full body item.
	 */
	public static boolean isFullBody(int itemId) {
		// String weapon = getItemName(itemId);
		// if (weapon == null)
		// return false;
		// for (int i = 0; i < fullbody.length; i++) {
		// if (weapon.endsWith(fullbody[i])) {
		// return true;
		// }
		// }
		ItemDefinition item = ItemDefinition.forId(itemId);
		if (item != null) {
			return item.isFullmask();
		}
		return false;
	}

	/**
	 * Calls if an item is a full helm item.
	 */
	public static boolean isFullHelm(int itemId) {
		// String weapon = getItemName(itemId);
		// if (weapon == null)
		// return false;
		// for (int i = 0; i < fullhat.length; i++) {
		// if (weapon.endsWith(fullhat[i])) {
		// return true;
		// }
		// }
		ItemDefinition item = ItemDefinition.forId(itemId);
		if (item != null) {
			// System.out.println("full mask:"+item.isFullmask()+" ID:"+itemId);
			return item.isFullmask(); // !item.showBeard();
		}
		return false;
	}

	/**
	 * Calls if an item is a full mask item.
	 */
	public static boolean isFullMask(int itemId) {
		// String weapon = getItemName(itemId);
		// if (weapon == null)
		// return false;
		// for (int i = 0; i < fullmask.length; i++) {
		// if (weapon.endsWith(fullmask[i])) {
		// return true;
		// }
		// }

		ItemDefinition item = ItemDefinition.forId(itemId);
		if (item != null) {
			// System.out.println("full mask:"+item.isFullmask()+" ID:"+itemId);
			return item.isFullmask();
		}
		return false;
	}

	public static boolean itemIsSellable(int realItemId) {
		return itemSellable[realItemId];
	}

	public static boolean itemIsStackable(int realItemId) {
		return itemStackable[realItemId];
	}

	/**
	 * @param realItemId
	 *            - bank item ids and inventory item ids are +1 of real item id.
	 *            inventory item ids are real ids. to get real id of bank item,
	 *            do (bankitem - 1)
	 * @return true if item is tradeable. false if item is not tradeable
	 */
	public static boolean itemIsTradeable(int realItemId) {
		return itemTradeable[realItemId];
	}
	
	public static boolean itemIsChargeItem(int itemId) {
		ItemDefinition itemDef = ItemDefinition.forId(itemId);
		if (itemDef == null) {
			return false;
		}

		return itemDef.isChargeClassItem();
	}

	public static void load() {
		for (int i = 0; i < Config.ITEM_LIMIT; i++) {

			itemTradeable[i] = true; // set default as true
			itemSellable[i] = true; // set default as true

			ItemDef def = ItemDef.forID(i); // loading runescape 667 cache item
											// defs
			if (def != null) {
				// if (def.value < 1) {
				// itemSellable[i] = false;
				// }
				if (def.itemIsInNotePosition) {
					itemStackable[i] = true;
				}

				itemStackable[i] = def.isStackable();

				if (!def.itemIsInNotePosition) {
					if (DBConfig.ITEM_TRADEABLE.contains(i)) {
						itemTradeable[i] = false;
						itemSellable[i] = false;
						if (def.itemCanBeNoted()) {
							itemTradeable[def.getNoteId()] = false; // create
																	// noted
																	// version
																	// untradeable
							itemSellable[def.getNoteId()] = false; // create
																	// noted
																	// version
																	// untradeable
							// this will not work because stupid loop rewrites
							// them
						}
					}
				}
				// so i must do it seperately here
				if (def.itemIsInNotePosition) {
					if (DBConfig.ITEM_TRADEABLE.contains(def.getNoteId())) {
						itemTradeable[i] = false;
						itemSellable[i] = false;
					}
				}

				if (!def.itemIsInNotePosition) {
					if (DBConfig.ITEM_SELLABLE.contains(i)) {
						itemSellable[i] = false;
						if (def.itemCanBeNoted()) {
							itemSellable[def.getNoteId()] = false; // create
																	// noted
																	// version
																	// untradeable
						}
					}
				}

				// so i must do it seperately here
				if (def.itemIsInNotePosition) {
					if (DBConfig.ITEM_SELLABLE.contains(def.getNoteId())) {
						itemSellable[i] = false;
					}
				}

			}
		}

		// hard coding flameburst defender to be tradeable in noted position
		itemTradeable[17274] = true;

		itemSellable[995] = false;
		itemSellable[8890] = false;
		itemSellable[6529] = false;
		itemSellable[10831] = false;

		for (SkillingUrn data : SkillingUrns.URNS) {
			itemTradeable[data.getUnfired()] = false;
			itemTradeable[data.getRune()] = false;
			itemTradeable[data.getActive()] = false;
			itemTradeable[data.getFull()] = false;

			itemSellable[data.getUnfired()] = false;
			itemSellable[data.getRune()] = false;
			itemSellable[data.getActive()] = false;
			itemSellable[data.getFull()] = false;
		}
	}

	public static void reloadItems() {
		// include anythign that is inside the load() function
		itemTradeable = new boolean[Config.ITEM_LIMIT];

		itemSellable = new boolean[Config.ITEM_LIMIT];

		itemStackable = new boolean[Config.ITEM_LIMIT];

		load();

	}

	public static boolean showBeard(int itemId) {
		ItemDefinition item = ItemDefinition.forId(itemId);
		if (item != null) {
			return item.showBeard();
		}
		return true;
	}
	
	public static boolean loadWeight(String FileName) {
		String line = "";
		String token = "";
		String token2 = "";
		BufferedReader characterfile = null;
		try {
			characterfile = new BufferedReader(new FileReader("./" + FileName));
		} catch (FileNotFoundException fileex) {
			Misc.println(FileName + ": file not found.");
			return false;
		}
		try {
			line = characterfile.readLine();
		} catch (IOException ioexception) {
			Misc.println(FileName + ": error loading file.");
			try {
				characterfile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
		while (line != null) {
			line = line.trim();
			int spot = line.indexOf("-");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();

				final int itemId = Integer.parseInt(token);
				final float weight = Float.parseFloat(token2);

				ItemDefinition def = ItemDefinition.forId(itemId);
				if (def == null) {
					System.out.println("def null for ID:"+itemId);
				} else {
					def.setWeight(weight);
				}
			}
			try {
				line = characterfile.readLine();
			} catch (IOException ioexception1) {
				break;
			}
		}
		try {
			characterfile.close();
		} catch (IOException ioexception) {
			ioexception.printStackTrace();
		}
		return false;
	}

}
