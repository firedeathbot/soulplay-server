package com.soulplay.game.model.item;

import com.soulplay.content.items.degradeable.PvpDegradeableEnum;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.item.definition.ChargeInfo;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;

public class ChargedItems {

	public static void unchargeItemsOnDeath(Client c, Client killer) {
		if (killer == null) { // probably death from npcs
			killer = c;
		}

		if (Tournament.insideArenaWithSpawnedGear(c)) {
			return;
		}

		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {
				ItemDefinition def = ItemDefinition.forId(c.playerItems[i]-1);
				if (def == null || !def.isCharged()) {
					continue;
				}

				if (c.itemsToKeep.contains(new ComparableItem(c.playerItems[i] - 1, c.playerItemsN[i]))) {
				    continue;
				}

				c.playerItems[i] = def.getChargeInfo().getUnchargedId()+1;
				c.setUpdateInvInterface(3214);
				dropChargesAndAmmo(c, killer, def);
			}
		}

		for (int i = 0; i < c.playerEquipment.length; i++) {
			if (c.playerEquipment[i] > 0) {
				ItemDefinition def = ItemDefinition.forId(c.playerEquipment[i]);
				if (def == null || !def.isCharged()) {
					continue;
				}

				if (c.itemsToKeep.contains(new ComparableItem(c.playerEquipment[i], c.playerEquipmentN[i]))) {
				    continue;
				}

				c.getItems().replaceEquipment(i, def.getChargeInfo().getUnchargedId());
				dropChargesAndAmmo(c, killer, def);
			}
		}
	}

	private static void dropChargesAndAmmo(Client c, Client killer, ItemDefinition def) {
		PvpDegradeableEnum body = PvpDegradeableEnum.getBody(def.getId());
		if (body != null) {
			dropPvpGold(c, killer, body.getGoldReward());
			return;
		}
		PvpDegradeableEnum legs = PvpDegradeableEnum.getLegs(def.getId());
		if (legs != null) {
			dropPvpGold(c, killer, legs.getGoldReward());
			return;
		}
		PvpDegradeableEnum head = PvpDegradeableEnum.getHelm(def.getId());
		if (head != null) {
			dropPvpGold(c, killer, head.getGoldReward());
			return;
		}
		PvpDegradeableEnum wep = PvpDegradeableEnum.getWeapon(def.getId());
		if (wep != null) {
			dropPvpGold(c, killer, wep.getGoldReward());
			return;
		}
		
		if (def.getId() == BlowPipeCharges.BLOWPIPE_FULL) {
			dropCharges(c, killer, def);
			if (c.getBlowpipeAmmoType() > 0 && c.getBlowpipeAmmo() > 0) {
				ItemHandler.createGroundItemFromPlayerDeath(killer, killer.getName(), c.getBlowpipeAmmoType(), killer.getX(), killer.getY(), killer.getZ(), c.getBlowpipeAmmo(), c);
				c.setBlowpipeAmmo(0);
				c.setBlowpipeAmmoType(0);
			}
			c.sendMessage("Your blowpipe charges and ammo have been lost.");
		} else if (def.getChargeInfo().getChargeWith() > 0) {
			dropCharges(c, killer, def);
		}
	}
	
	private static void dropCharges(Client c, Client killer, ItemDefinition def) {
		int chargeDropped = c.resetCharge(def.getId());
		if (chargeDropped > 0) {
			ItemHandler.createGroundItemFromPlayerDeath(killer, killer.getName(), def.getChargeInfo().getChargeWith(), killer.getX(), killer.getY(), killer.getZ(), chargeDropped, c);
		}
	}

	public static void chargeItem(Player p, int chargedItemId, Item chargeWith, int max) {
		chargeItem(p, chargedItemId, chargeWith, chargeWith.getAmount(), max);
	}

	public static void chargeItem(Player p, int chargedItemId, Item chargeWith, int chargeAmount, int max) {
		if (Tournament.insideArenaWithSpawnedGear(p)) {
			p.sendMessage("Can't do that here.");
			return;
		}
		if (p.getCharges(chargedItemId) >= max) {
			return;
		}
		int leftOver = p.addCharge(chargedItemId, chargeAmount, max);
		p.getItems().deleteItem(chargeWith);
		if (leftOver > 0) {
			p.getItems().addItem(chargeWith.getId(), leftOver);
		}
	}
	
	private static void dropPvpGold(Client c, Client killer, int amount) {
		ItemHandler.createGroundItemFromPlayerDeath(killer, killer.getName(), 995, killer.getX(), killer.getY(), killer.getZ(), amount, c);
	}
	
	/**
	 * To shorten the code, auto search for what item is a charge item and which is to charge.
	 */
	public static void chargeItems(Player player, Item item1, Item item2, int max) {
		ItemDefinition def1 = ItemDefinition.forId(item1.getId());
		if (def1 == null) {
			player.sendMessage("Invalid item: " + item1.getId());
			return;
		}

		ItemDefinition def2 = ItemDefinition.forId(item2.getId());
		if (def2 == null) {
			player.sendMessage("Invalid item: " + item2.getId());
			return;
		}

		ChargeInfo cInfo = null;
		Item charges = null;
		Item toCharge = null;
		if (def1.isChargeClassItem()) {
			charges = item2;
			toCharge = item1;
			cInfo = def1.getChargeInfo();
		} else if (def2.isChargeClassItem()) {
			charges = item1;
			toCharge = item2;
			cInfo = def2.getChargeInfo();
		}
		
		if (cInfo == null) {
			player.sendMessage("Messed up somewhere...");
			return;
		}
		
		int chargedItemId = cInfo.isChargedItem() ? toCharge.getId() : cInfo.getChargedId();
		
		if (!cInfo.isChargedItem()) {
			if (player.getItems().deleteItem(toCharge))
				player.getItems().addItem(chargedItemId, 1);
		}
		
		ChargedItems.chargeItem(player, chargedItemId, charges, max);
		
	}
	
	public static void unchargeItem(Player player, Item item, boolean returnChargeWith) {
		if (!player.getItems().playerHasItem(item)) {
			return;
		}
		ItemDefinition def = ItemDefinition.forId(item.getId());
		if (def == null) {
			player.sendMessage("Invalid item: " + item.getId());
			return;
		}

		if (!def.isChargeClassItem()) return;
		
		ChargeInfo cInfo = def.getChargeInfo();
		
		if (!cInfo.isChargedItem()) {
			player.sendMessage("Not a charged item.");
			return;
		}
		
		int chargesLeft = player.getCharges(item.getId());
		
		if (chargesLeft > 0) {
			
			int chargeWithItemId = cInfo.getChargeWith();
			
			if (!returnChargeWith || (returnChargeWith && player.getItems().addItem(chargeWithItemId, chargesLeft))) {
				player.resetCharge(item.getId()); //reset charges if added to player inventory
				//replace charged item id to uncharged item
				player.getItems().replaceItemSlot(cInfo.getUnchargedId(), item.getSlot());

				if (!returnChargeWith) {
					player.sendMessage("Your charges disappear.");
				} else {
					player.sendMessage("You have received " + chargesLeft + " charges back.");
				}
			}
			
		} else {
			player.sendMessage("Sorry you have no more charges on this item. Converting to Uncharged.");
			player.getItems().replaceItemSlot(cInfo.getUnchargedId(), item.getSlot());
		}
		
	}

}
