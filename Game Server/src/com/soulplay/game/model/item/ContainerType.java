package com.soulplay.game.model.item;

public enum ContainerType {
	DEFAULT,
	SHOP,
	ALWAYS_STACK,
	NEVER_STACK;
}