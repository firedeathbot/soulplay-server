package com.soulplay.game.model.item;

public class GroundItem {

	/**
	 * The Item ID.
	 */
	private int itemId;

	/**
	 * The coordinates of the item.
	 */
	private int itemX;

	private int itemY;

	private int itemZ;

	/**
	 * The amount of the item.
	 */
	private int itemAmount;

	/**
	 * Controls the ground item(s).
	 */
	private int itemController;

	/**
	 * Tick usage when re-spawning ground items.
	 */
	public int hideTicks;

	public int removeTicks;

	private boolean ironManModeItem = false;

	private boolean displayForOthers = true; // basically after hide tick is 0,
												// to display tradeable items.

	public String originalOwnerName;

	public int originalOwnerMID;

	private boolean playerDropped = false;
	
	private boolean inMinigame = false;
	
	private boolean isServerWideDrop = false; // visible to all and picked up even by ironman
	
	private boolean isActive;

	/**
	 * Name of item.
	 */
	public String ownerName;

	public GroundItem(int id, int x, int y, int z, int amount, int ticks) {
		this.itemId = id;
		this.itemX = x;
		this.itemY = y;
		this.itemZ = z;
		this.itemAmount = amount;
		this.hideTicks = ticks;
		this.ownerName = "server";
		this.setActive(true);
	}

	/**
	 * Handles the usage of ground items.
	 *
	 * @param id
	 * @param x
	 * @param y
	 * @param amount
	 * @param controller
	 * @param hideTicks
	 * @param name
	 */
	public GroundItem(int id, int x, int y, int z, int amount, int controller,
			int hideTicks, String name, int mid, boolean ironManModeItem) {
		this.itemId = id;
		this.itemX = x;
		this.itemY = y;
		this.itemZ = z;
		this.itemAmount = amount;
		this.itemController = controller;
		this.hideTicks = hideTicks;
		this.ownerName = name;
		this.setIronManModeItem(ironManModeItem);
		this.setDisplayForOthers(true);
		// int noteId = ItemDef.forID(id).getNoteId();
		// if (noteId > 0) {
		// for (int i : Config.ITEM_TRADEABLE) {
		// if (i == noteId) {
		// this.displayForOthers = false;
		// }
		// }
		// }
		if (!ItemProjectInsanity.itemIsTradeable(itemId)) {
			this.setDisplayForOthers(false);
		}
		this.originalOwnerName = name;
		this.originalOwnerMID = mid;
		this.setActive(true);
	}

	/**
	 * Item amount.
	 *
	 * @return Amount of item
	 */
	public int getItemAmount() {
		return this.itemAmount;
	}

	/**
	 * Controls ground item.
	 *
	 * @return
	 */
	public int getItemController() {
		return this.itemController;
	}

	/**
	 * Item ID.
	 *
	 * @return item
	 */

	public int getItemId() {
		return this.itemId;
	}

	/**
	 * Item coordinate X.
	 *
	 * @return Coordinate X
	 */
	public int getItemX() {
		return this.itemX;
	}

	/**
	 * Item coordinate Y.
	 *
	 * @return Coordinate Y
	 */
	public int getItemY() {
		return this.itemY;
	}

	/**
	 * Item coordinate Z.
	 *
	 * @return Height
	 */
	public int getItemZ() {
		return this.itemZ;
	}

	public int getOriginalOwnerMID() {
		return originalOwnerMID;
	}

	public String getOriginalOwnerName() {
		return this.originalOwnerName;
	}

	/**
	 * Item name.
	 *
	 * @return
	 */
	public String getOwnerName() {
		return this.ownerName;
	}

	public int getRemoveTicks() {
		return removeTicks;
	}

	public boolean isDisplayForOthers() {
		return displayForOthers;
	}

	public boolean isIronManModeItem() {
		return ironManModeItem;
	}

	public boolean isPlayerDropped() {
		return playerDropped;
	}

	public void setDisplayForOthers(boolean displayForOthers) {
		this.displayForOthers = displayForOthers;
	}

	public void setIronManModeItem(boolean ironManModeItem) {
		this.ironManModeItem = ironManModeItem;
	}

	public void setItemAmount(int amount) {
		this.itemAmount = amount;
	}

	public void setOriginalOwnerMID(int originalOwnerMID) {
		this.originalOwnerMID = originalOwnerMID;
	}

	public void setPlayerDropped(boolean playerDropped) {
		this.playerDropped = playerDropped;
	}

	public void setRemoveTicks(int removeTicks) {
		this.removeTicks = removeTicks;
	}

	public boolean isInMinigame() {
		return inMinigame;
	}

	public void setInMinigame(boolean inMinigame) {
		this.inMinigame = inMinigame;
	}

	public boolean isServerWideDrop() {
		return isServerWideDrop;
	}

	public void setServerWideDrop(boolean isServerWideDrop) {
		this.isServerWideDrop = isServerWideDrop;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}
