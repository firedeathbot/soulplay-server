package com.soulplay.game.model.item;

import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ObjectDef;
import com.soulplay.content.items.PotionMixing;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.impl.DegradableItem.Degradable;
import com.soulplay.content.items.useitem.AmuletStringing;
import com.soulplay.content.items.useitem.AncientEmblemsExchange;
import com.soulplay.content.items.useitem.AvasAssembler;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.items.useitem.BucketWaterFilling;
import com.soulplay.content.items.useitem.ChiselCrafting;
import com.soulplay.content.items.useitem.DonationChest;
import com.soulplay.content.items.useitem.ItemColouring;
import com.soulplay.content.items.useitem.ItemsOnObjects;
import com.soulplay.content.items.useitem.MaxCapeUpgrades;
import com.soulplay.content.items.useitem.RainbowPartyHatCreation;
import com.soulplay.content.items.useitem.SlayerHelmColoring;
import com.soulplay.content.items.useitem.ItemUpgrading;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.junkmachine.JunkMachine;
import com.soulplay.content.minigames.soulwars.SoulObelisk;
import com.soulplay.content.minigames.warriorsguild.WarriorsGuild;
import com.soulplay.content.npcs.impl.Barricades;
import com.soulplay.content.objects.impl.DwarfMultiCannon;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.pets.Egg;
import com.soulplay.content.player.pets.Egg.EggData;
import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.skills.Cooking;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.crafting.Crafting;
import com.soulplay.content.player.skills.crafting.Moulding;
import com.soulplay.content.player.skills.crafting.Pottery;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonItems;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonNpcs;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonObjects;
import com.soulplay.content.player.skills.farming.FarmingManager;
import com.soulplay.content.player.skills.firemaking.Firemaking;
import com.soulplay.content.player.skills.fletching.Fletching;
import com.soulplay.content.player.skills.fletching.stringing.BowStringing;
import com.soulplay.content.player.skills.fletching.stringing.CrossbowStringing;
import com.soulplay.content.player.skills.herblore.Herblore;
import com.soulplay.content.player.skills.prayer.Prayer;
import com.soulplay.content.player.skills.runecrafting.Runecrafting;
import com.soulplay.content.player.skills.runecrafting.RunecraftingData;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.content.shops.ShopPrice;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.net.packet.in.ItemOnPlayer;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemOnNpcExtension;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ItemOnPlayerExtension;
import com.soulplay.util.Misc;

import plugin.interact.npc.lumbridge.LumbridgeCookPlugin;

/**
 * @author Faggots :)
 */

public class UseItem {
	
	public static int getDistance(Client c, int itemId, int currentDist) {
		switch(itemId) {
		
		case 19667:
			return currentDist+10;
		}
		return currentDist;
	}

	/**
	 * Using items on items.
	 *
	 * @param c
	 * @param itemUsed
	 * @param useWith
	 */
	// public static void ItemonItem(Client c, int itemUsed, int useWith) {
	public static void ItemonItem(final Client c, final int itemUsed,
			final int useWith, final int itemUsedSlot, final int usedWithSlot) {
		c.walkingToObject = false;
		
		if (useWith == 299 && itemUsed == 20923) {
			c.getDH().sendOption5("red", "yellow", "orange", "rainbow", "next");
			c.dialogueAction = 20923;
		}

		if (itemUsed == 12435) { // winter storage scroll
			if (c.getSummonedNPC() != null) {
				if (c.getSummonedNPC().getNpcName().toLowerCase()
						.contains("pack yak")) {
					c.getSummoning().winterStorage(useWith, usedWithSlot);
				} else {
					c.sendMessage(
							"This scroll is only used when you have summoned a pack yak.");
				}
			} else {
				c.sendMessage(
						"You do not have a summoned NPC to use the scroll with.");
			}
		}
		
		if ((useWith == BlowPipeCharges.ZULRAH_SCALES || itemUsed == BlowPipeCharges.ZULRAH_SCALES)
				&& (useWith == BlowPipeCharges.BLOWPIPE_EMPTY || itemUsed == BlowPipeCharges.BLOWPIPE_EMPTY
					|| useWith == BlowPipeCharges.BLOWPIPE_FULL || itemUsed == BlowPipeCharges.BLOWPIPE_FULL)) {
			BlowPipeCharges.addScales(c);
			return;
		}
		
		if (useWith == 12926 && (itemUsed >= 806 && itemUsed <= 811 || itemUsed == 3093 || itemUsed == 11230)) { // charging blowpipe
			if (c.getBlowpipeAmmo() > 0) {
				if (c.getItems().addItem(c.getBlowpipeAmmoType(), c.getBlowpipeAmmo())) {
					c.setBlowpipeAmmo(0);
					c.setBlowpipeAmmoType(0);
				} else {
					return;
				}
			}
			int amount = c.getItems().getItemAmount(itemUsed, itemUsedSlot);
			if (itemUsed >= 806 && itemUsed <= 811 || itemUsed == 3093 || itemUsed == 11230) { // darts
				if (c.getItems().deleteItemInOneSlot(itemUsed, itemUsedSlot, amount)) {
					c.setBlowpipeAmmoType(itemUsed);
					c.setBlowpipeAmmo(amount);
				}
				return;
			}
		}
		
		if (useWith == RunePouch.RUNE_POUCH_ITEM) {
			RunePouch.depositRune(c, itemUsed, itemUsedSlot, c.playerItemsN[itemUsedSlot]);
			return;
		}

		if (ItemColouring.colorItem(c, itemUsed, useWith)) {
			return;
		}

		// if (itemUsed == 227 || useWith == 227)
		// c.getHerblore().handlePotMaking(itemUsed, useWith);
		
		if (RainbowPartyHatCreation.usedPhatOnPhat(c, itemUsed, useWith)) {
			RainbowPartyHatCreation.partyHatOnPartyHat(c);
			return;
		}

		if (Herblore.unfinishedPotCombo(itemUsed, useWith)) {
			Herblore.setUpPotionMaking(c, itemUsed, useWith);
			return;
		}

		if (Herblore.potionCombo(itemUsed, useWith)) {
			Herblore.setUpPotionMaking(c, itemUsed, useWith);
			return;
		}

		if (SlayerHelmColoring.handle(c, itemUsed, useWith)) {
			return;
		}

		if (Pottery.handleUrnRuneAdding(c, itemUsed, useWith)) {
			return;
		}

		if (ItemUpgrading.handle(c, itemUsed, useWith)) {
			return;
		}
		
		if (AvasAssembler.handle(c, itemUsed, useWith)) {
			return;
		}
		
		if (MaxCapeUpgrades.handle(c, itemUsed, useWith)) {
			return;
		}
		
		if (AmuletStringing.handle(c, itemUsed, useWith)) {
			return;
		}
		
		if (ChiselCrafting.handle(c, itemUsed, useWith)) {
			return;
		}

		if (DonationChest.handleDonationChest(c, itemUsed, useWith)) {
			return;
		}

		if ((useWith == Crafting.NEEDLE || itemUsed == Crafting.NEEDLE) && Crafting.handleDragonHideLeatherCrafting(c, useWith, itemUsed)) {
			return;
		}

		if ((useWith == Fletching.KNIFE || itemUsed == Fletching.KNIFE) && Fletching.handleLog(c, useWith, itemUsed)) {
			return;
		}

		if ((useWith == BowStringing.BOW_STRING || itemUsed == BowStringing.BOW_STRING) && BowStringing.string(c, useWith, itemUsed)) {
			return;
		}

		if ((useWith == CrossbowStringing.CROSSBOW_STRING || itemUsed == CrossbowStringing.CROSSBOW_STRING) && CrossbowStringing.string(c, useWith, itemUsed)) {
			return;
		}

		if (ItemConstants.getItemName(itemUsed).contains("(")
				&& ItemConstants.getItemName(useWith).contains("(")) {
			PotionMixing.mixPotion2(c, itemUsed, useWith);
		}
		if (itemUsed == 53 || useWith == 53 || itemUsed == 52
				|| useWith == 52) {
			Fletching.makeArrows(c, itemUsed, useWith);
		}
		
		if ((itemUsed == 10561 && useWith == 10535) || (itemUsed == 10535 && useWith == 10561))  {
			c.getItems().deleteItemInOneSlot(10561, 1);
			c.getItems().deleteItemInOneSlot(10535, 1);
			c.getItems().addItem(10536, 1);
		}		
		
		if ((itemUsed == 1540 && useWith == 11286)
				|| (itemUsed == 11286 && useWith == 1540)) {
			if (c.getPlayerLevel()[Skills.SMITHING] >= 95) {
				c.getItems().deleteItemInOneSlot(1540,
						c.getItems().getItemSlot(1540), 1);
				c.getItems().deleteItemInOneSlot(11286,
						c.getItems().getItemSlot(11286), 1);
				c.getItems().addItem(11283, 1);
				c.sendMessage(
						"You combine the two materials to create a dragonfire shield.");
				c.getPA().addSkillXP(500, Skills.SMITHING);
			} else {
				c.sendMessage(
						"You need a smithing level of 95 to create a dragonfire shield.");
			}
		}
		
		if (itemUsed == 590 || useWith == 590 || itemUsed == 17678 || useWith == 17678) {
			Firemaking.attemptFire(c, itemUsed, useWith, c.absX, c.absY, false);
		}

		if (itemUsed == CompletionistCape.NORMAL_CAPE && useWith == 19893 || useWith == CompletionistCape.NORMAL_CAPE && itemUsed == 19893) {
			if (Variables.SPIRIT_CAPE_ADDED_TO_COMP.getValue(c) == 1) {
				c.sendMessage("You already have a Spirit cape added to your Completionist cape.");
				return;
			}

			c.getItems().deleteItemInOneSlot(19893, 1);
			c.setVar(Variables.SPIRIT_CAPE_ADDED_TO_COMP, 1);
			c.sendMessage("You have added Spirit cape effect to your Completionist cape.");
		}

		if (itemUsed >= 11710 && itemUsed <= 11714 && useWith >= 11710
				&& useWith <= 11714) {
			if (c.getItems().hasAllShards()) {
				c.getItems().makeBlade();
			}
		}
		if (itemUsed == 2368 && useWith == 2366
				|| itemUsed == 2366 && useWith == 2368) {
			c.getItems().deleteItemInOneSlot(2368,
					c.getItems().getItemSlot(2368), 1);
			c.getItems().deleteItemInOneSlot(2366,
					c.getItems().getItemSlot(2366), 1);
			c.getItems().addItem(1187, 1);
		}

		if (ItemConstants.isHilt(itemUsed) || ItemConstants.isHilt(useWith)) {
			c.getItems();
			int hilt = ItemConstants.isHilt(itemUsed) ? itemUsed : useWith;
			int blade = ItemConstants.isHilt(itemUsed) ? useWith : itemUsed;
			if (blade == 11690) {
				c.getItems().makeGodsword(hilt);
			}
		}

		if (itemUsed == 13736 && useWith == 13746
				|| itemUsed == 13746 && useWith == 13736) {
			if (c.getSkills().getStaticLevel(Skills.PRAYER) >= 90) {
				c.getItems().deleteItemInOneSlot(useWith,
						c.getItems().getItemSlot(useWith), 1);
				c.getItems().deleteItemInOneSlot(itemUsed,
						c.getItems().getItemSlot(itemUsed), 1);
				c.getItems().addItem(13738, 1);
			} else {
				c.sendMessage(
						"You need a Prayer level of 90 to Make a blessed spiritshield.");
			}
		}
		if (itemUsed == 13736 && useWith == 13748
				|| itemUsed == 13748 && useWith == 13736) {
			if (c.getSkills().getStaticLevel(Skills.PRAYER) >= 90) {
				c.getItems().deleteItemInOneSlot(useWith,
						c.getItems().getItemSlot(useWith), 1);
				c.getItems().deleteItemInOneSlot(itemUsed,
						c.getItems().getItemSlot(itemUsed), 1);
				c.getItems().addItem(13740, 1);
			} else {
				c.sendMessage(
						"You need a Prayer level of 90 to Make a blessed spiritshield.");
			}
		}
		if (itemUsed == 13736 && useWith == 13750
				|| itemUsed == 13750 && useWith == 13736) {
			if (c.getSkills().getStaticLevel(Skills.PRAYER) >= 90) {
				c.getItems().deleteItemInOneSlot(useWith,
						c.getItems().getItemSlot(useWith), 1);
				c.getItems().deleteItemInOneSlot(itemUsed,
						c.getItems().getItemSlot(itemUsed), 1);
				c.getItems().addItem(13742, 1);
			} else {
				c.sendMessage(
						"You need a Prayer level of 90 to Make a blessed spiritshield.");
			}
		}
		if (itemUsed == 13736 && useWith == 13752
				|| itemUsed == 13752 && useWith == 13736) {
			if (c.getSkills().getStaticLevel(Skills.PRAYER) >= 90) {
				c.getItems().deleteItemInOneSlot(useWith,
						c.getItems().getItemSlot(useWith), 1);
				c.getItems().deleteItemInOneSlot(itemUsed,
						c.getItems().getItemSlot(itemUsed), 1);
				c.getItems().addItem(13744, 1);
			} else {
				c.sendMessage(
						"You need a Prayer level of 90 to Make a blessed spiritshield.");
			}
		}

		if (itemUsed == 13734 && useWith == 13754
				|| itemUsed == 13754 && useWith == 13734) {
			if (c.getSkills().getStaticLevel(Skills.PRAYER) >= 75) {
				c.getItems().deleteItemInOneSlot(useWith,
						c.getItems().getItemSlot(useWith), 1);
				c.getItems().deleteItemInOneSlot(itemUsed,
						c.getItems().getItemSlot(itemUsed), 1);
				c.getItems().addItem(13736, 1);
			} else {
				c.sendMessage(
						"You need a Prayer level of 75 to Make a blessed spiritshield.");
			}
		}
		if (itemUsed == 4151 && useWith == 21369
				|| itemUsed == 21369 && useWith == 4151) {
			c.getItems().deleteItemInOneSlot(useWith,
					c.getItems().getItemSlot(useWith), 1);
			c.getItems().deleteItemInOneSlot(itemUsed,
					c.getItems().getItemSlot(itemUsed), 1);
			c.getItems().addItem(21371, 1);
			c.sendMessage(
					"You combine the two materials to create a Abyssal vine whip.");
		}
		if (itemUsed == 4151 && useWith == 12004
				|| itemUsed == 12004 && useWith == 4151) {
			c.getItems().deleteItemInOneSlot(useWith,
					c.getItems().getItemSlot(useWith), 1);
			c.getItems().deleteItemInOneSlot(itemUsed,
					c.getItems().getItemSlot(itemUsed), 1);
			c.getItems().addItem(12006, 1);
			c.sendMessage(
					"You combine the two materials to create a Abyssal tentacle.");
		}

		if (itemUsed == 18342 && useWith == MagicRequirements.LAW
				|| itemUsed == MagicRequirements.LAW && useWith == 18342) {
			int lawCount = Math.min(1000, c.getItems().getItemAmount(MagicRequirements.LAW));
			int currentCharges = c.getLawStaffCharges();
			int chargesToAdd = Math.min(lawCount, 1000 - currentCharges);

			if (chargesToAdd <= 0) {
				c.sendMessage("Your staff is fully charged.");
				return;
			}

			if (c.getItems().deleteItemInOneSlot(MagicRequirements.LAW, chargesToAdd)) {
				c.addLawStaffCharges(chargesToAdd);
				c.sendMessage("You have charged your staff with " + chargesToAdd + " charges.");
			}

			return;
		}

		if (itemUsed == 18341 && useWith == MagicRequirements.NATURE
				|| itemUsed == MagicRequirements.NATURE && useWith == 18341) {
			int natureCount = Math.min(1000, c.getItems().getItemAmount(MagicRequirements.NATURE));
			int currentCharges = c.getNatureStaffCharges();
			int chargesToAdd = Math.min(natureCount, 1000 - currentCharges);

			if (chargesToAdd <= 0) {
				c.sendMessage("Your staff is fully charged.");
				return;
			}

			if (c.getItems().deleteItemInOneSlot(MagicRequirements.NATURE, chargesToAdd)) {
				c.addNatureStaffCharges(chargesToAdd);
				c.sendMessage("You have charged your staff with " + chargesToAdd + " charges.");
			}

			return;
		}

		if (itemUsed == 8794 && useWith == 1511
				|| itemUsed == 1511 && useWith == 8794) {
			if (c.getItems().hasPlank()) {
				c.getItems().makePlank();
			}
		}

		if (itemUsed == 946 && useWith == 960
				|| itemUsed == 960 && useWith == 946) {
			if (c.getItems().hasHandles()) {
				c.getItems().makeHandles();
			}
		}

		if (itemUsed == 6864 && useWith == 6878
				|| itemUsed == 6878 && useWith == 6864) {
			if (c.getItems().hasBlueMari()) {
				c.getItems().makeBlueMari();
			}
		}

		if (itemUsed == 6864 && useWith == 6874
				|| itemUsed == 6874 && useWith == 6864) {
			if (c.getItems().hasRedMari()) {
				c.getItems().makeRedMari();
			}
		}

		if (itemUsed == 6864 && useWith == 6882
				|| itemUsed == 6882 && useWith == 6864) {
			if (c.getItems().hasGreenMari()) {
				c.getItems().makeGreenMari();
			}
		}

		if (itemUsed == 5312 && useWith == 5354) { // oak tree
			if (c.getItems().deleteItemInOneSlot(5312,
					c.getItems().getItemSlot(5312), 1)
					&& c.getItems().deleteItemInOneSlot(5354,
							c.getItems().getItemSlot(5354), 1)) {
				c.getItems().addItem(5370, 1);
			}
		}
		if (itemUsed == 5313 && useWith == 5354) { // willow tree
			if (c.getItems().deleteItemInOneSlot(5313,
					c.getItems().getItemSlot(5313), 1)
					&& c.getItems().deleteItemInOneSlot(5354,
							c.getItems().getItemSlot(5354), 1)) {
				c.getItems().addItem(5371, 1);
			}
		}
		if (itemUsed == 5314 && useWith == 5354) { // maple tree
			if (c.getItems().deleteItemInOneSlot(5314,
					c.getItems().getItemSlot(5314), 1)
					&& c.getItems().deleteItemInOneSlot(5354,
							c.getItems().getItemSlot(5354), 1)) {
				c.getItems().addItem(5372, 1);
			}
		}
		if (itemUsed == 5315 && useWith == 5354) { // yew tree
			if (c.getItems().deleteItemInOneSlot(5315,
					c.getItems().getItemSlot(5315), 1)
					&& c.getItems().deleteItemInOneSlot(5354,
							c.getItems().getItemSlot(5354), 1)) {
				c.getItems().addItem(5373, 1);
			}
		}
		if (itemUsed == 5316 && useWith == 5354) { // magic tree
			if (c.getItems().deleteItemInOneSlot(5316,
					c.getItems().getItemSlot(5316), 1)
					&& c.getItems().deleteItemInOneSlot(5354,
							c.getItems().getItemSlot(5354), 1)) {
				c.getItems().addItem(5374, 1);
			}
		}

		// START TAX BAGS
		if (itemUsed == 10831 && useWith == 995
				|| itemUsed == 995 && useWith == 10831) {
			if (c.getItems().playerHasItem(995, 2000000000)) {
				c.getItems().deleteItemInOneSlot(995, 2000000000);
				c.getItems().deleteItemInOneSlot(10831, 1);
				c.getItems().addItem(10835, 1);
				c.sendMessage(
						"<col=1532693>You add 2 Billion coins into the tax bag!</col>");
			} else {
				if (c.getItems().playerHasItem(995, 1500000000)) {
					c.getItems().deleteItemInOneSlot(995, 1500000000);
					c.getItems().deleteItemInOneSlot(10831, 1);
					c.getItems().addItem(10834, 1);
					c.sendMessage(
							"<col=1532693>You add 1.5 Billion coins into the tax bag!</col>");
				} else {
					if (c.getItems().playerHasItem(995, 1000000000)) {
						c.getItems().deleteItemInOneSlot(995, 1000000000);
						c.getItems().deleteItemInOneSlot(10831, 1);
						c.getItems().addItem(10833, 1);
						c.sendMessage(
								"<col=1532693>You add 1 Billion coins into the tax bag!</col>");
					} else {
						if (c.getItems().playerHasItem(995, 500000000)) {
							c.getItems().deleteItemInOneSlot(995, 500000000);
							c.getItems().deleteItemInOneSlot(10831, 1);
							c.getItems().addItem(10832, 1);
							c.sendMessage(
									"<col=1532693>You add 500 Million coins into the tax bag!</col>");
						} else {
							c.sendMessage(
									"<col=1532693>You don't have enough money to put in the tax bag!</col>");
							return;
						}
					}
				}
			}
		}
		if (itemUsed == 10832 && useWith == 995
				|| itemUsed == 995 && useWith == 10832) {
			if (c.getItems().playerHasItem(995, 1500000000)) {
				c.getItems().deleteItemInOneSlot(995, 1500000000);
				c.getItems().deleteItemInOneSlot(10832, 1);
				c.getItems().addItem(10835, 1);
				c.sendMessage(
						"<col=1532693>You add 1.5 Billion coins into the tax bag</col>");
			} else {
				if (c.getItems().playerHasItem(995, 1000000000)) {
					c.getItems().deleteItemInOneSlot(995, 1000000000);
					c.getItems().deleteItemInOneSlot(10832, 1);
					c.getItems().addItem(10834, 1);
					c.sendMessage(
							"<col=1532693>You add 1 Billion coins into the tax bag</col>");
				} else {
					if (c.getItems().playerHasItem(995, 500000000)) {
						c.getItems().deleteItemInOneSlot(995, 500000000);
						c.getItems().deleteItemInOneSlot(10832, 1);
						c.getItems().addItem(10833, 1);
						c.sendMessage(
								"<col=1532693>You add 500 Million coins into the tax bag</col>");
					} else {
						c.sendMessage(
								"<col=1532693>You don't have enough money to put in the tax bag!</col>");
						return;
					}
				}
			}
		}
		if (itemUsed == 10833 && useWith == 995
				|| itemUsed == 995 && useWith == 10833) {
			if (c.getItems().playerHasItem(995, 1000000000)) {
				c.getItems().deleteItemInOneSlot(995, 1000000000);
				c.getItems().deleteItemInOneSlot(10833, 1);
				c.getItems().addItem(10835, 1);
				c.sendMessage(
						"<col=1532693>You add 1 Billion coins into the tax bag</col>");
			} else {
				if (c.getItems().playerHasItem(995, 500000000)) {
					c.getItems().deleteItemInOneSlot(995, 500000000);
					c.getItems().deleteItemInOneSlot(10833, 1);
					c.getItems().addItem(10834, 1);
					c.sendMessage(
							"<col=1532693>You add 500 Million coins into the tax bag</col>");
				} else {
					c.sendMessage(
							"<col=1532693>You don't have enough money to put in the tax bag!</col>");
					return;
				}
			}
		}
		if (itemUsed == 10834 && useWith == 995
				|| itemUsed == 995 && useWith == 10834) {
			if (c.getItems().playerHasItem(995, 500000000)) {
				c.getItems().deleteItemInOneSlot(995, 500000000);
				c.getItems().deleteItemInOneSlot(10834, 1);
				c.getItems().addItem(10835, 1);
				c.sendMessage(
						"<col=1532693>You add 500 Million coins into the tax bag</col>");
			} else {
				c.sendMessage(
						"<col=1532693>You don't have enough money to put in the tax bag!</col>");
				return;
			}
		}
		if (itemUsed == 10835 && useWith == 995
				|| itemUsed == 995 && useWith == 10835) {
			c.sendMessage("<col=1532693>This bag is full!</col>");
		}
		// END TAX BAGS

		if (itemUsed == 3157 && useWith == 3150
				|| itemUsed == 3150 && useWith == 3157) {
			if (c.getItems().deleteItemInOneSlot(useWith, usedWithSlot, 1)
					&& c.getItems().deleteItemInOneSlot(itemUsed, itemUsedSlot,
							1)) {
				c.getItems().addItem(3159, 1);
			}
			return;
		}

		if (itemUsed == 21775 && useWith == 1391
				|| itemUsed == 1391 && useWith == 21775) {
			if (c.getItems().deleteItemInOneSlot(useWith, usedWithSlot, 1)
					&& c.getItems().deleteItemInOneSlot(itemUsed, itemUsedSlot,
							1)) {
				c.getItems().addItem(21777, 1);
			}
			if (c.getClan() != null) {
				c.getClan().addClanPoints(c, 200, "create_battlestaff");
			}
			c.sendMessage("You create armadyl battlestaff");
		}

		if (itemUsed == 21776 && useWith == 233) {
			if (c.getItems().deleteItemInOneSlot(itemUsed, itemUsedSlot, 1)) {
				c.getItems().addItem(21774, 8);
			}
			if (c.getClan() != null) {
				c.getClan().addClanPoints(c, 2, "crush_shard");
			}
			c.sendMessage("You crush the shard into dusts.");
		}

		if (itemUsed == 19333 && useWith == 6585
				|| itemUsed == 6585 && useWith == 19333) {
			if (c.getItems().deleteItemInOneSlot(useWith, usedWithSlot, 1)
					&& c.getItems().deleteItemInOneSlot(itemUsed, itemUsedSlot,
							1)) {
				c.getItems().addItem(19335, 1);
			}
			c.sendMessage("You create Amulet of fury (or)");
		}

		
		//spiney 4551
		//black mask 8921
		//muffs 4166
		//nose peg	4168
		if ((itemUsed == 4551 && (useWith == 8921 || useWith == 4166 || useWith == 4168)) ||
			(itemUsed == 8921 && (useWith == 4551 || useWith == 4166 || useWith == 4168)) ||
			(itemUsed == 4166 && (useWith == 4551 || useWith == 8921 || useWith == 4168)) ||
			(itemUsed == 4168 && (useWith == 4551 || useWith == 8921 || useWith == 4168))) {
			if (c.getItems().playerHasItem(4551, 1)
					&& c.getItems().playerHasItem(8921, 1)
					&& c.getItems().playerHasItem(4166, 1)
					&& c.getItems().playerHasItem(4168 ,1)) {
				c.getItems().deleteItemInOneSlot(4551, 1);
				c.getItems().deleteItemInOneSlot(8921, 1);
				c.getItems().deleteItemInOneSlot(4166, 1);
				c.getItems().deleteItemInOneSlot(4168, 1);
				c.getItems().addItem(13263, 1);
				c.sendMessage(
						"You carefully assemble the pieces to create a slayer helmet.");
			} else {
				c.sendMessage(
						"You dont have the correct pieces to create a slayer helmet.");
			}
			
		}
		
		if (itemUsed == 15490 && useWith == 13263
				|| itemUsed == 15488 && useWith == 13263) {
			if (c.getItems().playerHasItem(15490, 1)
					&& c.getItems().playerHasItem(13263, 1)
					&& c.getItems().playerHasItem(15488, 1)) {
				if (c.getItems().deleteItemInOneSlot(useWith, usedWithSlot,
						1)) {
					c.getItems().deleteItemInOneSlot(15488, 1);
				}
				c.getItems().deleteItemInOneSlot(15490, 1);
				c.getItems().addItem(15492, 1);
				c.sendMessage(
						"You carefully modify the slayer helmet so the extra pieces fit.");
			} else {
				c.sendMessage(
						"You dont have the correct pieces to modify the slayer helmet.");
			}
		}
		
		if (DungeonItems.handleItemOnItem(c, itemUsed, itemUsedSlot, useWith, usedWithSlot)) {
			return;
		}

		switch (itemUsed) {
			/*
			 * case 1511: case 1521: case 1519: case 1517: case 1515: case 1513:
			 * case 590: c.getFiremaking().checkLogType(itemUsed, useWith);
			 * break;
			 */

			default:
				if (c.playerRights == 3 || c.isDev()) {
					Misc.println("Player used Item id: " + itemUsed
							+ " with Item id: " + useWith);
				}
				break;
		}
	}

	/**
	 * Using items on NPCs.
	 *
	 * @param c
	 * @param itemId
	 * @param slot
	 */
	public static void ItemonNpc(Client c, int itemId, NPC n, int slot) {
		
		c.walkingToObject = false;

		if (n.isDead() || n == null) {
			return;
		}

		if (c.playerRights == 3 || c.debug) {
			c.sendMessage("ItemOnNpc: itemId= " + itemId + " npcId= "
					+ n.npcType + " slot= " + slot);
		}

		if (!c.getItems().playerHasItem(itemId, slot, 1)) {
			return;
		}

		final PluginResult<ItemOnNpcExtension> pluginResult = PluginManager.search(ItemOnNpcExtension.class, n.npcType, itemId);
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOnNpc(c, n, itemId, slot))) {
			return;
		}
		
		//any item on npc
		final PluginResult<ItemOnNpcExtension> anyPluginResult = PluginManager.search(ItemOnNpcExtension.class, n.npcType);
		if (anyPluginResult.invoke(() -> anyPluginResult.extension.onItemOnNpc(c, n, itemId, slot))) {
			return;
		}
		
	//	if (!PathFinder.foundRoute(c, n.getX(), n.getY())) //TODO: needs testing
	//		return;
			
		if (c.summoned == n) {
			c.getSummoning().renewFamiliar();
		}

		if (BarbarianAssault.handleItemOnNpc(c, n, itemId)) {
			return;
		}

		if (DungeonNpcs.handleItemOnNpc(c, n, itemId)) {
			return;
		}
		
		if (n.npcType == 7941) { // Emblem trader
			AncientEmblemsExchange.useOnNpc(n, c);
			return;
		}
		
		if (n.npcType == 3021) { // tool leprechaun
			com.soulplay.content.player.skills.herblore.Herblore.Herb herb = Herblore.getAnyHerb(itemId);
			if (herb != null) {
				for (int i = 0; i < c.playerItems.length; i++)
					if (c.playerItems[i] == itemId+1)
						if (!c.getItems().switchToNote(itemId, i, 1))
							break;
				return;
			}
		}

		switch (itemId) {
			
			case 12656:
				if (n.npcType == 7209) {
					if (c.getItems().playerHasItem(12657, 15)) {
						c.getDH().sendNpcChat2(
								"You have 15 Chocolate chunks, i used some easter",
								" magic to get you back to me.", 13651,
								"Easter Bunny");
						c.getPA().movePlayer(2457, 5332, 0);
					} else {
						c.getItems().addItem(12657, 1);
						c.sendMessage("The chocoatrice uses its gooey gaze and turns the target to chocolate.");
						n.killNpc = 7209;
					}
				}
				break;

			case 20174:
				if (n.npcType == 519) {
					c.degradedItem = Degradable.ZARYTE_BOW_BROKEN;
					c.getDH().sendDialogues(9060, 519);
					break;
				}
			case 20172:
				if (n.npcType == 519) {
					c.degradedItem = Degradable.ZARYTE_BOW_DEGRADED;
					c.getDH().sendDialogues(9060, 519);
					break;
				}
			case 590:
				if (n.npcType == 1532 || n.npcType == 1534) {
					if (n.transformId == -1 || n.transformId == n.npcType) {
						n.transform(n.npcType + 1);
						Barricades.burnBarricade(c, n);
					}
				}
				break;

			case 1929: // bucket of water
				if (n.npcType == 1532 || n.npcType == 1534) // barricades
					if (n.transformId != -1 && n.transformId != 1532 && n.transformId != 1534) {
						if (c.getItems().deleteItemInOneSlot(itemId, slot, 1)) {
							c.getItems().addItem(1925, 1);
							n.transform(n.npcType);
						}
					}
				break;
				
			case 4045:
				if (n.npcType == 1532 || n.npcType == 1534) {
					if (n.transformId == -1 || n.transformId == n.npcType) {
						Barricades.explodeBarricade(c, n, slot);
					}
					return;
				}
				break;
		}

	}

	/**
	 * Using items on an object.
	 *
	 * @param c
	 * @param objectID
	 * @param objectX
	 * @param objectY
	 */
	public static void ItemonObject(final Client c, int objectID, int objectX,
			int objectY, int itemId, int itemSlot) {
		c.walkingToObject = false;

		PluginResult<ItemOnObjectExtension> pluginResult = PluginManager.search(ItemOnObjectExtension.class, itemId, objectID);
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOnObject(c, objectID, objectX, objectY, itemId, itemSlot))) {
			return;
		}
		
		PluginResult<ItemOnObjectExtension> pluginResultAny = PluginManager.search(ItemOnObjectExtension.class, objectID);
		if (pluginResultAny.invoke(() -> pluginResultAny.extension.onItemOnObject(c, objectID, objectX, objectY, itemId, itemSlot))) {
			return;
		}
		
		if (DungeonObjects.handleItem(c, objectID, objectX, objectY, itemId, itemSlot)) {
			return;
		}
		
		if (ItemsOnObjects.handle(c, itemId, objectID)) {
			return;
		}
		
		if (c.playerIsInHouse) {
			Construction.itemOnObject(c, itemId, itemSlot, objectID, objectX, objectY);
			return;
		}
		
		GameObject o = RegionClip.getGameObject(objectID, objectX,
				objectY, c.heightLevel);
		// int objectID = o.getId(), objectX = o.getX(), objectY = o.getY();
		c.clickObjectType = 0;

		if (o == null) {
			c.sendMessage("Object null -item on object");
			return;
		}

		int faceX = 0, faceY = 0;
		if (c.getY() >= o.getY() && c.getY() <= o.getY() + o.getSizeY() - 1) {
			faceX = objectX;
			faceY = c.getY();
		} else {
			faceY = objectY;
			faceX = c.getX();
		}
		if (faceX > 0 && faceY > 0) {
			c.faceLocation(faceX, faceY);
		}

		if (!c.getItems().playerHasItem(itemId, itemSlot, 1)) {
			return;
		}

		if (FarmingManager.cure(c, objectX, objectY, itemId)) {
			return;
		} else if (FarmingManager.applyCompost(c, itemId, objectX, objectY)) {
			return;
		} else if (FarmingManager.clear(c, itemId, objectX, objectY)) {
			return;
		} else if (FarmingManager.plant(c, itemId, objectX, objectY)) {
			return;
		}
		
		if (SoulObelisk.handle(c, objectID, itemId)) {
			return;
		}

		ObjectDef def = ObjectDef.getObjectDef(objectID);

		switch (itemId) {
			case Pottery.SOFT_CLAY:
				if (def == null) {
					c.sendMessage("Null Def Error:44555");
					return;
				}
				if (!def.hasName()) {
					c.sendMessage("No Name error");
					return;
				}

				if (def.name.equals("Potter's Wheel")) {
					Pottery.start(c);
				}
				break;
			case 30195:
				for (int i = 0; i < Crafting.FURNACES.length; i++) {
					if (objectID == Crafting.FURNACES[i]) {
						Crafting.craftZenyte(c);
						return;
					}
				}
				break;
			case 4:
				if (def == null) {
					c.sendMessage("Null Def Error:4482");
					return;
				}
				if (!def.hasName()) {
					c.sendMessage("No Name error");
					return;
				}

				if (def.name.equals("Furnace")) {
					Smelting.smeltCannonballs(c, c.getItems().getItemAmount(2353));
					return;
				}
				break;

			case 1592:
			case 1595:
			case 1597:
			case 2357:
				if (def == null) {
					c.sendMessage("Null Def Error:4483");
					return;
				}
				if (!def.hasName()) {
					return;
				}

				if (def.name.equals("Furnace")) {
					Moulding.mouldInterface(c);
					return;
				}
				break;

			case 5331:
				if (def == null) {
					return;
				}
				if (def.name == null) {
					return;
				}
				if (def.name.contains("Sink") || def.name.contains("Well")) {
					c.getItems().deleteItemInOneSlot(itemId, 1);
					c.getItems().addItem(5340, 1);
					c.sendMessage("You fill your Watering Can.");
				}

				break;

		}

		switch (objectID) {

			case 409:
				Prayer.bonesOnAltar2(c, itemId);
				break;
			case 411:
			case 100411:
				if (c.inWild()) {
					Prayer.bonesOnAltar2(c, itemId);
				}
				break;
		
		case 29091:
			if (ShopPrice.getSurvivalShopSellValue(itemId) > 0) {
				c.isShopping = true;
				c.myShopId = 60;
				c.getShops().sellItem(itemId, itemSlot, 1);
			} else {
				c.sendMessage("You can't trade a " + ItemConstants.getItemName(itemId).toLowerCase() + " to the Vending shrine.");
			}
			break;
		
		case 9684:
		case 873:
		case 879:
		case 884:
		case 24314:
		case 23920:
		case 24265:
		case 24214:
		case 24161:
		case 8699:
		case 11759:
		case 11793:
			BucketWaterFilling.waterFilling(c);
			break;
		
		/*case 28294:
			if (itemId == 11955) {
				c.transform(6742);
				c.getPA().resetMovementAnimation();
				c.getItems().deleteItemInOneSlot(itemId, 1);
			} else if (itemId == 11956) {
				c.transform(6743);
				c.getPA().resetMovementAnimation();
				c.getItems().deleteItemInOneSlot(itemId, 1);
			} else if (itemId == 11957) {
				c.transform(6744);
				c.getPA().resetMovementAnimation();
				c.getItems().deleteItemInOneSlot(itemId, 1);
			} else if (itemId == 11958) {
				c.transform(6745);
				c.getPA().resetMovementAnimation();
				c.getItems().deleteItemInOneSlot(itemId, 1);
			} else if (itemId == 11959) {
				c.transform(6746);
				c.getPA().resetMovementAnimation();
				c.getItems().deleteItemInOneSlot(itemId, 1);
			}
			break;*/
		case 20040:
			JunkMachine.openInterface(c, itemId);
			break;
		case 594: // table
		case 26957: // large table
		case 26958: // small table
			Item item = new Item(itemId, c.playerItemsN[itemSlot], itemSlot);
			FifthItemAction action = FifthItemAction.get(item.getId());
			
			if (action != null) {
				c.sendMessage("You cannot drop that item.");
			} else {
				if (c.playerRights == 2 && !DBConfig.ADMIN_DROP_ITEMS) {
					c.getDialogueBuilder().sendNpcChat(2163, "You are dropping itemID: "+item.getId(),
							"Admins are not allowed to drop. Would you like to destroy the item?").
					sendOption("Yes destroy the item ID:"+item.getId(), ()-> {
						c.getItems().deleteItem(item);
					}, "No, I would like to keep it.", ()-> {
						c.getPA().closeAllWindows();
					})
					.execute();
					return;
				}
				if (c.getItems().deleteItem(item)) {
					c.startAnimation(832);
					c.droppingItem = true;
					ItemHandler.createGroundItem(c, item.getId(), faceX, faceY, c.getHeightLevel(), item.getAmount());
					c.droppingItem = false;
				}
				
				if (DBConfig.LOG_DROP) {
					Server.getLogWriter().addToDropList(new DroppedItem(c, item.getId(), item.getAmount(), "drop"));
				}
			}
			break;
		
		case 2482: // fire altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.FIRE_RUNE);
		    }
		    break;
		    
		case 2481: // earth altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.EARTH_RUNE);
		    }
		    break;
		    
		case 2483: // body altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.BODY_RUNE);
		    }
		    break;
		    
		case 2484: // cosmic altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.COSMIC_RUNE);
		    }
		    break;
		    
		case 2486: // nature altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.NATURE_RUNE);
		    }
		    break;
		    
		case 2487: // chaos altar			    
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.CHAOS_RUNE);
		    }
		    break;
		    
		case 2485: // law altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.LAW_RUNE);
		    }
		    break;
		    
		case 2488: // death altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.DEATH_RUNE);
		    }
		    break;
		    
		case 2480: // water altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.WATER_RUNE);
		    }
		    break;
		    
		case 7138: // soul riff
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.SOUL_RUNE);
		    }
		    break;
		    
		case 2478:	    
		    if ((itemId == Runecrafting.PURE_ESSENCE || itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.DUST_OF_ARMADYL) && c.getItems().playerHasItem(Runecrafting.DUST_OF_ARMADYL)) {
			c.getRunecrafting().craftArmadylRunes();
		    } else if (itemId == Runecrafting.PURE_ESSENCE || itemId == Runecrafting.RUNE_ESSENCE) {
			c.getRunecrafting().craftRunes(RunecraftingData.AIR_RUNE);
		    }
		    break;
		    
		case 2479: // mind altar
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.MIND_RUNE);
		    }
		    break;
		    
		case 17010: // astral runes
			if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
				c.getRunecrafting().craftRunes(RunecraftingData.ASTRAL_RUNE);
			}
			break;
		    
		case 30624:
		    if (itemId == Runecrafting.RUNE_ESSENCE || itemId == Runecrafting.PURE_ESSENCE) {
			    c.getRunecrafting().craftRunes(RunecraftingData.BLOOD_RUNE);
		    }
		    break;

			case 20150:
				if (itemId == 10546 || itemId == 10545 || itemId == 10544
						|| itemId == 10543) {
					if (c.getItems().playerHasItem(10546)
							|| c.getItems().playerHasItem(10545)
							|| c.getItems().playerHasItem(10544)
							|| c.getItems().playerHasItem(10543)) {
						if (c.getItems().playerHasItem(10546)) {
							c.getItems().deleteItemInOneSlot(10546, 1);
						} else if (c.getItems().playerHasItem(10545)) {
							c.getItems().deleteItemInOneSlot(10545, 1);
						} else if (c.getItems().playerHasItem(10544)) {
							c.getItems().deleteItemInOneSlot(10544, 1);
						} else if (c.getItems().playerHasItem(10543)) {
							c.getItems().deleteItemInOneSlot(10543, 1);
						}
						c.getItems().addItem(10542, 1);
						c.sendMessage("You fill the healing vial.");
						c.startAnimation(827);
					}
				}

			/*case 24265:// christmas event
				if (itemId == 6868) { // Blue Marionette
					c.sendMessage("You dip the Blue Marionette in the water.");
					c.sendMessage("You can now control the Blue Marionette!");
					if (c.getItems().deleteItemInOneSlot(6868,
							c.getItems().getItemSlot(6868), 1)) {
						c.getItems().addItem(6865, 1);
					}
					return;
				}
				break;

			case 879:// christmas event
				if (itemId == 6870) { // Red Marionette
					c.sendMessage("You dip the Red Marionette in the water.");
					c.sendMessage("You can now control the Red Marionette!");
					if (c.getItems().deleteItemInOneSlot(6870,
							c.getItems().getItemSlot(6870), 1)) {
						c.getItems().addItem(6867, 1);
					}
					return;
				}
				if (itemId == 6869) { // Green Marionette
					c.sendMessage("You dip the Green Marionette in the water.");
					c.sendMessage("You can now control the Green Marionette!");
					if (c.getItems().deleteItemInOneSlot(6869,
							c.getItems().getItemSlot(6869), 1)) {
						c.getItems().addItem(6866, 1);
					}
					return;
				}
				break;*/

			/*
			 * case 727: if (itemId == 946 && c.absX == 3032 && c.absY == 10331)
			 * { // Blue Marionette
			 * c.sendMessage("You crawl through the small dungeon.");
			 * c.getPA().movePlayer(3032, 3923, 0); return; } if (itemId == 946
			 * && c.absX == 3021 && c.absY == 10333) { // Blue Marionette
			 * c.sendMessage("You crawl through the small dungeon.");
			 * c.getPA().movePlayer(3021, 3921, 0); return; } break;
			 */

			case 28336:
				if (itemId == 4836) {
					if (c.getEggTimer() < 144000) {
						c.sendMessage("The egg needs to be in there for at least 1 day to use this potion.");
					} else if (c.getEggTimer() > 144000 && c.getEggTimer() < 288000) {
						c.getItems().deleteItemInOneSlot(4836, 1);
						c.setEggTimer(288000);
						c.startAnimation(897);
						c.sendMessage("You use the strange potion on your egg.");
					} else if (c.getEggTimer() > 288000) {
						c.sendMessage("The egg is already grown, you don't need to use a potion.");
					}

					return;
				}

				EggData egg = EggData.values.get(itemId);
				if (egg != null) {
					if (egg == EggData.MONSTER_EGG) {
						if (c.getClan() == null || c.getClan().getClanPoints() <= 273741) {
							c.sendMessage("You need to be ranked in a clan lvl 60 or higher.");
							return;
						}
					}

					Egg.incubatorStart(c, egg);
				}
				break;

			case 2461:
				if (itemId == 1436 || itemId == 21774) { // use pure essence on
															// ruins to make
															// armadyl runesasdf
					//Runecrafting.craftRunesOnAltar(c, 72, 10, 21773, 0, 0, 0);
				}
				break;

			case 8985:
				if (itemId == 6202) {
					c.startAnimation(832);
					c.getItems().deleteItemInOneSlot(itemId, 1);
					c.getItems().addItem(6200, 1);
				}
				break;

			case 21304:
			case 2644:
				Crafting.spinningWheel(c);
				break;

			case 4446: // Spawning ropes on Sara walls
				if (c.onCwWalls) {
					return;
				}
				if (itemId == 4047) {
					if (objectX == c.absX) {
						final RSObject rso = new RSObject(4444, objectX, c.absY, c.heightLevel, 3, 4,
								-1, CastleWars.GAME_TIMER);
						rso.setObjectClass(1);
						ObjectManager.addObject(rso);
					} else if (objectY == c.absY) {
						final RSObject rso = new RSObject(4444, c.absX, objectY, c.heightLevel, 2, 4,
								-1, CastleWars.GAME_TIMER);
						rso.setObjectClass(1);
						ObjectManager.addObject(rso);
					}
					c.getItems().deleteItemInOneSlot(itemId, 1);
				}
				break;
			case 4447: // spawning ropes on Zammy walls
				if (c.onCwWalls) {
					return;
				}
				if (itemId == 4047) {
					if (objectX == c.absX) {
						final RSObject rso = new RSObject(4444, objectX, c.absY, c.heightLevel, 1, 4,
								-1, CastleWars.GAME_TIMER);
						rso.setObjectClass(1);
						ObjectManager.addObject(rso);
					} else if (objectY == c.absY) {
						final RSObject rso = new RSObject(4444, c.absX, objectY, c.heightLevel, 0, 4,
								-1, CastleWars.GAME_TIMER);
						rso.setObjectClass(1);
						ObjectManager.addObject(rso);
					}
					c.getItems().deleteItemInOneSlot(itemId, 1);
				}
				break;

			case 4448: // cave wall
				if (itemId == 4045) { // explosive pot
					if ((objectX == 2399 || objectX == 2402)
							&& (objectY == 9511 || objectY == 9514)) { // north
																		// cave
																		// side
						if (CastleWars.northCave) {
							c.sendMessage("Cave is already collapsed");
							return;
						} else {
							// new Object(4437, 2400, 9512, c.heightLevel, 0,
							// 10, 4437, 0, false, 1);
							RSObject rso = ObjectManager.getObject(2400, 9512,
									0);
							if (rso != null) {
								rso.setTick(0);
								CastleWars.collapseCave(0);
								CastleWars.northCave = true;
							}
							rso = null;
						}
					}
					if ((objectX == 2408 || objectX == 2411)
							&& (objectY == 9502 || objectY == 9505)) { // east
																		// cave
																		// side
						if (CastleWars.eastCave) {
							c.sendMessage("Cave is already collapsed");
							return;
						} else {
							// new Object(4437, 2409, 9503, c.heightLevel, 0,
							// 10, 4437, 0, false, 1);
							RSObject rso = ObjectManager.getObject(2409, 9503,
									0);
							if (rso != null) {
								rso.setTick(0);
								CastleWars.collapseCave(1);
								CastleWars.eastCave = true;
							}
							rso = null;
						}
					}
					if ((objectX == 2400 || objectX == 2403)
							&& (objectY == 9493 || objectY == 9496)) { // south
																		// cave
																		// side
						if (CastleWars.southCave) {
							c.sendMessage("Cave is already collapsed");
							return;
						} else {
							// new Object(4437, 2401, 9494, c.heightLevel, 0,
							// 10, 4437, 0, false, 1);
							RSObject rso = ObjectManager.getObject(2401, 9494,
									0);
							if (rso != null) {
								rso.setTick(0);
								CastleWars.collapseCave(2);
								CastleWars.southCave = true;
							}
							rso = null;
						}
					}
					if ((objectX == 2390 || objectX == 2393)
							&& (objectY == 9500 || objectY == 9503)) { // west
																		// cave
																		// side
						if (CastleWars.westCave) {
							c.sendMessage("Cave is already collapsed");
							return;
						} else {
							// new Object(4437, 2391, 9501, c.heightLevel, 0,
							// 10, 4437, 0, false, 1);
							RSObject rso = ObjectManager.getObject(2391, 9501,
									0);
							if (rso != null) {
								rso.setTick(0);
								CastleWars.collapseCave(3);
								CastleWars.westCave = true;
							}
							rso = null;
						}
					}
					c.getItems().deleteItemInOneSlot(itemId, 1);
					c.getPA().createPlayersStillGfx(266, objectX, objectY, 100,
							0);
				}
				break;

			case 4437:
				if (itemId == 4045) {
					c.getPA().createPlayersStillGfx(266, objectX, objectY,
							objectY, 0);
					c.getItems().deleteItemInOneSlot(itemId, 1);
					final RSObject rso = new RSObject(4438, o.getX(), o.getY(), c.heightLevel, 0,
							o.getType(), o.getId(), CastleWars.GAME_TIMER);
					rso.setObjectClass(1);
					ObjectManager.addObject(rso);
				}
				break;
			case 4438:
				if (itemId == 4045) {
					if (objectX == 2400 && objectY == 9512) {
						CastleWars.northCave = false;
					}
					if (objectX == 2409 && objectY == 9503) {
						CastleWars.eastCave = false;
					}
					if (objectX == 2401 && objectY == 9494) {
						CastleWars.southCave = false;
					}
					if (objectX == 2391 && objectY == 9501) {
						CastleWars.westCave = false;
					}
					// new Object(4439, objectX, objectY, c.heightLevel, 0, 10,
					// 4439, 0, false, 1);
					// Change object inside object manager to 4439 which is
					// walkable surface i guess lol
					RSObject rso = ObjectManager.getPlacedObject(o.getX(),
							o.getY(), o.getZ(), o.getType());
					if (rso != null) {
						rso.setNewObjectId(-1);
						ObjectManager.updateNewObject(rso);
						c.getPA().createPlayersStillGfx(266, objectX, objectY,
								objectY, 0);
						c.getItems().deleteItemInOneSlot(itemId, 1);
					}
					rso = null;
				}
				break;

			case 4482:
				if (itemId == 1925) {
					c.startAnimation(832);
					c.sendMessage("You fill a bucket of water.");
					c.getItems().deleteItemInOneSlot(itemId, 1);
					c.getItems().addItem(1929, 1);
				}
				break;

			// warriors guild
			case 15621:
				WarriorsGuild.handleArmor(c, itemId, objectX, objectY);
				break;

			case 6:
				if (itemId == 2) {
					DwarfMultiCannon.loadCannon(c);
				}
				break;
			case 7:
			case 8:
			case 9:
				DwarfMultiCannon.setupCannon(c);
				break;

			case 2783:
			case 102097:
				SmithingInterface.showSmithInterface(c, itemId);
				break;

			case 12269:
			case 3039:
			case 2728:
			case 126185: // wilderness fire
			case 26185: // wilderness fire
			case 2732: // ::fishing fire
			case 21302:
			case 4266:
			case 24283:
			case 10377:
				Cooking.cookThisFood(c, itemId, objectID, o);
				break;
				
			case 114:
				if (QuestHandlerTemp.completedQuest(c, QuestIndex.COOKS_ASSISTANT))
					Cooking.cookThisFood(c, itemId, objectID, o);
				else {
					c.npcType = LumbridgeCookPlugin.COOK_NPC_ID;
					c.getDialogueBuilder()
					.sendNpcChat(Expression.MEAN_FACE, "Hey, who said you could use that?")
					.execute();
				}
				break;
				
			case 2643:
			case 3198:
			case 4308:
			case 5087:
			case 11601:
			case 34802:
			case 61334:
				Pottery.fireUrn(c);
				break;

			default:
				if (c.playerRights == 3 || c.isDev()) {
					Misc.println("Player At Object id: " + objectID
							+ " with Item id: " + itemId);
				}
				break;
		}

	}

	public static void itemOnPlayer(Client c, int interfaceId, Client o,
			int itemId, int itemSlot) {

		c.walkingToObject = false;

		Item itemUsed = c.getItems().getItem(itemSlot);

		if (itemUsed == null) {
			return;
		}

		if (!c.getItems().playerHasItem(itemId, itemSlot, 1)) {
			return;
		}

		PluginResult<ItemOnPlayerExtension> pluginResult = PluginManager.search(ItemOnPlayerExtension.class, itemId);

		if (pluginResult.invoke(() -> pluginResult.extension.onItemOnPlayer(c, o, itemUsed))) {
			return;
		}

		if (BarbarianAssault.handleItemOnPlayer(c, o, itemId)) {
			return;
		}

		switch (itemId) {

			case 962:
				ItemOnPlayer.handleCrackers(c, itemId, c.getId());
				break;

			case 20083:
				ItemOnPlayer.handleGoldenCracker(c, itemId);
				break;

			case 4155:
				if (!c.inMinigame()) {
					Slayer.invitePlayer(c, o);
				} else {
					c.sendMessage("You cannot invite while in a minigame.");
				}
				break;

			default:
				if (c.playerRights == 3 || c.isDev()) {
					Misc.println("Player used Item id: " + itemId
							+ " with Player id: " + o.getId() + " With Slot : "
							+ itemSlot);
				} else {
					c.sendMessage("Nothing interesting happens.");
				}
				break;
		}
	}

}
