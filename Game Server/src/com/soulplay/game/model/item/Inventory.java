package com.soulplay.game.model.item;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import org.jetbrains.annotations.NotNull;

import com.google.gson.reflect.TypeToken;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;

import java.util.Iterator;

public class Inventory implements Iterable<Item> {

	private Item[] items;
	
	private final int capacity;
	
	private SortType sortType;
	
	/**
	 * The current container type.
	 */
	private final ContainerType type;

	/**
	 * Constructs a new {@code Container} {@code Object}.
	 *
	 * @param capacity The capacity.
	 */
	public Inventory(int capacity) {
		this(capacity, ContainerType.DEFAULT);
	}

	public Inventory(int capacity, String str, ContainerType type) {
		this.capacity = capacity;
		this.items = GsonSave.gsond.fromJson(str, new TypeToken<Item[]>() { }.getType());
		if (this.items == null)
			this.items = new Item[capacity];
		this.type = type;
		this.sortType = SortType.ID;
	}

	/**
	 * Constructs a new {@code Container.java} {@code Object}.
	 *
	 * @param capacity the capacity.
	 * @param items    the items to add.
	 */
	public Inventory(int capacity, Item... items) {
		this(capacity);
		add(items);
	}
	
	/**
	 * Constructs a new {@code Container} {@code Object}.
	 *
	 * @param capacity The capacity.
	 * @param type     The container type.
	 */
	public Inventory(int capacity, ContainerType type) {
		this(capacity, type, SortType.ID);
	}
	
	/**
	 * Constructs a new {@code Container} {@code Object}.
	 *
	 * @param capacity The capacity.
	 * @param type     The container type.
	 * @param sortType The sort type.
	 */
	public Inventory(int capacity, ContainerType type, SortType sortType) {
		this.capacity = capacity;
		this.type = type;
		this.items = new Item[capacity];
		this.sortType = sortType;
	}
	
	/**
	 * Adds the items.
	 *
	 * @param items The items to add.
	 * @return {@code True} if successfully added <b>all</b> items.
	 */
	public boolean add(Item... items) {
		boolean addedAll = true;
		for (Item item : items) {
			if (item == null) {
				continue;
			}
			if (!add(item, false)) {
				addedAll = false;
				break;
			}
		}
		update();
		return addedAll;
	}
	
	public boolean addNoted(Item... items) {
		boolean addedAll = true;
		for (Item item : items) {
			if (item == null) {
				continue;
			}
			Item notedItem = item.copy();
			notedItem.setId(item.getDefinition().getNoteId());
			if (!add(notedItem, false)) {
				addedAll = false;
				break;
			}
		}
		update();
		return addedAll;
	}
	
	public boolean add(int... ids) {
		if (ids == null || ids.length < 1) return false;
		Item[] items = new Item[ids.length];
		for (int i = 0; i < items.length; i++)
			items[i] = new Item(ids[i], 1);
		return add(items);
	}
	
	public boolean add(int id) {
		return add(new Item(id));
	}
	
	/**
	 * Inserts an item into a specific slot.
	 *
	 * @param fromSlot The original slot of the item.
	 * @param toSlot   The slot to insert into.
	 */
	public void insert(int fromSlot, int toSlot) {
		insert(fromSlot, toSlot, true);
	}
	
	/**
	 * Inserts an item into a specific slot.
	 *
	 * @param fromSlot The original slot of the item.
	 * @param toSlot   The slot to insert into.
	 * @param update   If the container packets should be sent.
	 */
	public void insert(int fromSlot, int toSlot, boolean update) {
		Item temp = items[fromSlot];
		if (toSlot > fromSlot) {
			for (int i = fromSlot; i < toSlot; i++) {
				replace(get(i + 1), i, false);
			}
		} else if (fromSlot > toSlot) {
			for (int i = fromSlot; i > toSlot; i--) {
				replace(get(i - 1), i, false);
			}
		}
		replace(temp, toSlot, update);
	}
	
	/**
	 * Inserts an item into a specific slot.
	 *
	 * @param fromSlot The original slot of the item.
	 * @param toSlot   The slot to insert into.
	 */
	public void insertSwap(int fromSlot, int toSlot) {
		insertSwap(fromSlot, toSlot, true);
	}
	
	/**
	 * Inserts an item into a specific slot.
	 *
	 * @param fromSlot The original slot of the item.
	 * @param toSlot   The slot to insert into.
	 * @param update   If the container packets should be sent.
	 */
	public void insertSwap(int fromSlot, int toSlot, boolean update) {
		Item temp = items[fromSlot];
		if (toSlot > fromSlot) {
			for (int i = fromSlot; i < toSlot; i++) {
				replaceSwap(get(i + 1), i, false);
			}
		} else if (fromSlot > toSlot) {
			for (int i = fromSlot; i > toSlot; i--) {
				replaceSwap(get(i - 1), i, false);
			}
		}
		replaceSwap(temp, toSlot, update);
	}
	
	/**
	 * Adds an item to this container if full it goes to ground.
	 *
	 * @param item   the item.
	 * @param player the player.
	 * @return {@code True} if added.
	 */
	public boolean add(final Item item, final Player player) {
		if (!add(item, true, -1)) {
			ItemHandler.createGroundItem((Client) player, item, player.getCurrentLocation());
			return false;
		}

		return true;
	}
	
	/**
	 * Adds an item to this container.
	 *
	 * @param item The item.
	 * @return {@code True} if the item got added.
	 */
	public boolean add(Item item) {
		return add(item, true, -1);
	}
	
	/**
	 * Adds an item to this container.
	 *
	 * @param item         The item to add.
	 * @param fireListener If we should update.
	 * @return {@code True} if the item got added.
	 */
	public boolean add(Item item, boolean fireListener) {
		return add(item, fireListener, -1);
	}
	
	/**
	 * Adds an item to this container.
	 *
	 * @param item          The item to add.
	 * @param fireListener  If we should update.
	 * @param preferredSlot The slot to add the item in, when possible.
	 * @return {@code True} if the item got added.
	 */
	public boolean add(Item item, boolean fireListener, int preferredSlot) {
		if (item == null || item.getId() < 0) {
			return false;
		}
		item = item.copy();
		int maximum = getMaximumAdd(item);
		if (maximum == 0) {
			return false;
		}
//		if (preferredSlot > -1 && items[preferredSlot] != null) {
//			preferredSlot = -1;
//		}
		if (item.getAmount() > maximum) {
			item.setAmount(maximum);
		}
		if (type != ContainerType.NEVER_STACK && (item.getDefinition().isStackable() || type == ContainerType.ALWAYS_STACK || type == ContainerType.SHOP)) {
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null) {
					if (items[i].getId() == item.getId()) {
						int totalCount = item.getAmount() + items[i].getAmount();
						Item newItem = new Item(items[i].getId(), totalCount, i);
						items[i] = newItem;
						if (fireListener) {
							update();
						}
						return true;
					}
				}
			}
			int slot = preferredSlot > -1 ? preferredSlot : freeSlot();
			if (slot == -1) {
				return false;
			}
			items[slot] = new Item(item.getId(), item.getAmount(), slot);
			if (fireListener) {
				update();
			}
			return true;
		}
		int slots = freeSlots();
		if (slots >= item.getAmount()) {
			for (int i = 0; i < item.getAmount(); i++) {
				int slot = i == 0 && preferredSlot > -1 ? preferredSlot : freeSlot();
				Item newItem = new Item(item.getId(), 1, slot);
				items[slot] = newItem;
			}
			if (fireListener) {
				update();
			}
			return true;
		}
		return false;
	}

	/**
	 * Adds an item
	 *
	 * @param slot
	 * @param amount
	 * @param container
	 * @return
	 */
	public boolean addItem(int slot, int amount, Inventory container) {
		if (slot < 0 || slot > container.capacity() || amount < 1) {
			return false;
		}
		Item item = container.get(slot);
		if (item == null) {
			return false;
		}
		int maximum = container.getAmount(item);
		if (amount > maximum) {
			amount = maximum;
		}
		int maxCount = getMaximumAdd(item);
		if (amount > maxCount) {
			amount = maxCount;
			if (amount < 1) {
				return false;
			}
		}

		item = new Item(item.getId(), amount);
		boolean unnote = !item.getDefinition().itemIsInNotePosition;
		if (container.remove(item, slot, true)) {
			Item add = unnote ? new Item(item.getDefinition().getNoteId(), amount) : item;
			if (unnote && !add.getDefinition().itemIsInNotePosition) {
				add = item;
			}
			return add(add, true, -1);
		}

		return true;
	}
	
	public boolean addItemNoted(int slot, int amount, Inventory container) {
		if (slot < 0 || slot > container.capacity() || amount < 1) {
			return false;
		}
		Item item = container.get(slot);
		if (item == null) {
			return false;
		}
		int maximum = container.getAmount(item);
		if (amount > maximum) {
			amount = maximum;
		}
		
		int id = item.getId();
		if (item.getDefinition().getNoteId() != -1) {
			id = item.getDefinition().getNoteId();
		}
		Item notedItem = new Item(id, amount);
		int maxCount = getMaximumAdd(notedItem);
		if (amount > maxCount) {
			amount = maxCount;
			if (amount < 1) {
				return false;
			}
		}
		item = new Item(item.getId(), amount);
		if (container.remove(item, slot, true)) {
			return add(notedItem, true, -1);
		}
		return true;
	}
	
	/**
	 * Removes a set of items.
	 *
	 * @param items The set of items.
	 * @return {@code True} if all items got successfully removed.
	 */
	public boolean remove(Item... items) {
		boolean removedAll = true;
		for (Item item : items) {
			if (!remove(item, false)) {
				removedAll = false;
			}
		}
		update();
		return removedAll;
	}
	
	public boolean remove(int... ids) {
		final Item[] items = new Item[ids.length];
		for (int i = 0; i < items.length; i++) items[i] = new Item(ids[i]);
		return remove(items);
	}
	
	public boolean removeAll(Item... items) {
		for (Item item : items) if (!remove(item)) return false;
		return true;
	}
	
	/**
	 * Removes an item.
	 *
	 * @param item The item.
	 * @return {@code True} if the item got removed, {@code false} if not.
	 */
	public boolean remove(Item item) {
		return remove(item, true);
	}
	
	public boolean remove(Item item, int amount) {
		Item itemNew = item.copy();
		itemNew.setAmount(amount);
		return remove(itemNew, true);
	}
	
	/**
	 * Removes an item.
	 *
	 * @param item         The item to remove.
	 * @param fireListener If the fire listener should be "notified".
	 * @return {@code True} if the item got removed, <br> {@code false} if not.
	 */
	public boolean remove(Item item, boolean fireListener) {
		int slot = getSlot(item);
		if (slot > -1) {
			return remove(item, slot, fireListener);
		}
		return false;
	}
	
	public boolean remove(int id) {
		return remove(new Item(id, 1));
	}

	public boolean remove(Item item, int slot, boolean fireListener) {
		Item oldItem = items[slot];
		if (oldItem == null || oldItem.getId() != item.getId()) {
			return false;
		}
		if (item.getAmount() < 1) {
			return true;
		}
		if (!containsItem(new Item(item.getId(), item.getAmount()))) {
			return false;
		}
		if (type == ContainerType.SHOP || type == ContainerType.ALWAYS_STACK || oldItem.getDefinition().isStackable()) {
			if (item.getAmount() >= oldItem.getAmount()) {
				items[slot] = null;
				if (fireListener) {
					update();
				}
				return true;
			}
			items[slot] = new Item(item.getId(), oldItem.getAmount() - item.getAmount(), slot);
			if (fireListener) {
				update();
			}
			return true;
		}
		items[slot] = null;
		int removed = 1;
		for (int i = removed; i < item.getAmount(); i++) {
			slot = getSlot(item);
			if (slot != -1) {
				items[slot] = null;
			} else {
				break;
			}
		}
		if (fireListener) {
			update();
		}
		return true;
	}
	
	/**
	 * Replaces the item on the given slot with the argued item.
	 *
	 * @param item The item.
	 * @param slot The slot.
	 * @return The old item.
	 */
	public Item replace(Item item, int slot) {
		return replace(item, slot, true);
	}
	
	/**
	 * Replaces the item on the given slot with the argued item.
	 *
	 * @param item         The item.
	 * @param slot         The slot.
	 * @param fireListener If the listener should be "notified".
	 * @return The old item.
	 */
	public Item replace(Item item, int slot, boolean fireListener) {
		if (item != null) {
			if (item.getAmount() < 1 && type != ContainerType.SHOP) {
				item = null;
			} else {
				item = item.copy(slot);
			}
		}
		Item oldItem = items[slot];
		items[slot] = item;
		if (fireListener) {
			update();
		}
		return oldItem;
	}
	
	/**
	 * Updates the container.
	 */
	public void update() {
		//TODO
	}
	
	/**
	 * Updates the container.
	 */
	public void update(boolean force) {
		//TODO
	}
	
	/**
	 * Refreshes the entire container.
	 */
	public void refresh() {
		//TODO
	}
	
	/**
	 * Gets the item on the given slot.
	 *
	 * @param slot The slot.
	 * @return The item on the slot, or {@code null} if the item wasn't there.
	 */
	public Item get(int slot) {
		if (slot < 0 || slot >= items.length) {
			return null;
		}
		return items[slot];
	}
	
	/**
	 * Gets the item on the given slot.
	 *
	 * @param slot The slot.
	 * @return The item on the slot, or a new constructed item with id 0 if the
	 * item wasn't there.
	 */
	public Item getNew(int slot) {
		if (slot < 0 || slot >= items.length) {
			return new Item(0);
		}
		Item item = items[slot];
		if (item != null) {
			return item;
		}
		return new Item(0);
	}
	
	/**
	 * Gets the item id on the given slot.
	 *
	 * @param slot The slot.
	 * @return The id of the item on the slot.
	 */
	public int getId(int slot) {
		if (slot >= items.length) {
			return -1;
		}
		Item item = items[slot];
		if (item != null) {
			return item.getId();
		}
		return -1;
	}
	
	/**
	 * Copies the specified container to this container.
	 *
	 * @param c The container to copy.
	 */
	public Inventory copy(Inventory c) {
		items = new Item[c.items.length];
		for (int i = 0; i < items.length; i++) {
			Item it = c.items[i];
			if (it == null) {
				continue;
			}
			items[i] = new Item(it.getId(), it.getAmount(), i);
		}
		return this;
	}
	
	public Inventory copyAndUpdate(Inventory c, boolean force) {
		final Inventory result = copy(c);
		refreshAndUpdate(force);
		return result;
	}
	
	/**
	 * Formats a container for the SQL database.
	 *
	 * @return the string.
	 */
	public String format() {
		final StringBuilder log = new StringBuilder();
		final Int2IntMap map = new Int2IntOpenHashMap(items.length);
		for (final Item item : items) {
			if (item != null) {
				int old = map.containsKey(item.getId()) ? map.get(item.getId()) : -1;
				map.put(item.getId(), old == -1 ? item.getAmount() : old + item.getAmount());
			}
		}
		for (final Int2IntMap.Entry entry : map.int2IntEntrySet()) {
			log.append(entry.getIntKey()).append(',').append(entry.getIntValue()).append('|');
		}
		if (log.length() > 0 && log.charAt(log.length() - 1) == '|') {
			return log.substring(0, log.length() - 1);
		}
		return log.toString();
	}
	
	public boolean containsID(int itemID) {
		for (Item item : items) {
			if (item != null && itemID == item.getId()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if the container contains an item.
	 *
	 * @param item         the Item
	 * @param ignoreCharge - to ignore charge hashing
	 * @return {@code True} if so.
	 */
	public boolean containsItem(Item item) {
		if (item == null) return true;
		int count = 0;
		for (Item it : items) {
			if (it != null && it.getId() == item.getId()) {
				if ((count += it.getAmount()) >= item.getAmount()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks if the containers contains these items.
	 *
	 * @param items the items.
	 * @return {@code True} if so.
	 */
	public boolean containsItems(Item... items) {
		for (Item i : items) {
			if (!containsItem(i)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks if the container contains an item.
	 *
	 * @param itemId The item id.
	 * @param amount The amount.
	 * @return {@code True} if so.
	 */
	public boolean contains(int itemId, int amount) {
		return containsItem(new Item(itemId, amount));
	}
	
	public boolean containsAnyOf(int... ids) {
		for (int id : ids) {
			if (containsID(id)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean containsAnyOfInSlot(int slot, int... ids) {
		int currentId = getId(slot);
		
		for (int id : ids) {
			if (currentId == id) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean contains(int itemId) {
		return contains(itemId, 1);
	}
	
	public boolean removeIfContains(int id, int amount) {
		return contains(id, amount) && remove(new Item(id, amount));
	}
	
	/**
	 * Checks if the containers contains ONE item and ONE item only.
	 *
	 * @param itemId
	 * @return
	 */
	public boolean containsOneItem(int itemId) {
		for (Item item : items) {
			if (item != null && item.getId() == itemId && item.getAmount() == 1) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Adds a container to this container.
	 *
	 * @param container The container.
	 */
	public boolean addAll(Inventory container) {
		return add(container.items);
	}
	
	/**
	 * Checks the maximum amount of this item we can add.
	 *
	 * @param item The item.
	 * @return The maximum amount we can add.
	 */
	public int getMaximumAdd(Item item) {
		if (type != ContainerType.NEVER_STACK) {
			if (item.getDefinition().isStackable() || type == ContainerType.ALWAYS_STACK || type == ContainerType.SHOP) {
				if (containsItem(new Item(item.getId(), 1))) {
					return Integer.MAX_VALUE - getAmount(item);
				}
				return freeSlots() > 0 ? Integer.MAX_VALUE : 0;
			}
		}
		return freeSlots();
	}
	
	/**
	 * Checks if the container has space for the item.
	 *
	 * @param item The item to check.
	 * @return {@code True} if so.
	 */
	public boolean hasSpaceFor(Item item) {
		return item.getAmount() <= getMaximumAdd(item);
	}
	
	/**
	 * Checks if the container has space for adding all the items.
	 *
	 * @param items The items to add.
	 * @return {@code True} if all items can be added.
	 */
	public boolean hasSpaceFor(Item... items) {
		Inventory check = new Inventory(capacity, type);
		check.addAll(this);
		for (Item item : items) {
			if (item != null) {
				if (!check.add(item, false)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Checks if this container has space to add the other container.
	 *
	 * @param c The other container.
	 * @return {@code True} if so.
	 */
	public boolean hasSpaceFor(Inventory c) {
		if (c == null) {
			return false;
		}
		Inventory check = new Inventory(capacity, type);
		check.addAll(this);
		for (Item item : c.items) {
			if (item != null) {
				if (!check.add(item, false)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Gets the item slot.
	 *
	 * @param item The item.
	 * @return The slot of the item in this container.
	 */
	public int getSlot(Item item) {
		if (item == null) {
			return -1;
		}
		int id = item.getId();
		for (int i = 0; i < items.length; i++) {
			Item it = items[i];
			if (it != null && it.getId() == id) {
				return i;
			}
		}
		return -1;
	}
	
	public Item[] getItems() {
		return items;
	}
	
	/**
	 * Gets the item instance.
	 *
	 * @param item the item.
	 * @return the item.
	 */
	public Item getItem(Item item) {
		return get(getSlot(item));
	}
	
	/**
	 * Gets the next free slot.
	 *
	 * @return The slot, or <code>-1</code> if there are no available slots.
	 */
	public int freeSlot() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Gets the slot of where to add the item.
	 *
	 * @param item The item to add.
	 * @return The slot where the item will go.
	 */
	public int getAddSlot(Item item) {
		if (type != ContainerType.NEVER_STACK && (item.getDefinition().isStackable() || type.equals(ContainerType.ALWAYS_STACK) || type == ContainerType.SHOP)) {
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null) {
					if (items[i].getId() == item.getId()) {
						return i;
					}
				}
			}
		}
		return freeSlot();
	}
	
	/**
	 * Gets the number of free slots.
	 *
	 * @return The number of free slots.
	 */
	public int freeSlots() {
		return getCapacity() - itemCount();
	}
	
	public boolean isFull() {
		return freeSlots() < 1;
	}
	
	/**
	 * Gets the size of this container.
	 *
	 * @return The size of this container.
	 */
	public int itemCount() {
		int size = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				size++;
			}
		}
		return size;
	}

	public int size() {
		return itemCount();
	}
	
	/**
	 * Checks if the player has all the item ids in the inventory.
	 *
	 * @param itemIds The item ids.
	 * @return {@code True} if so.
	 */
	public boolean containItems(int... itemIds) {
		for (int i = 0; i < itemIds.length; i++) {
			if (!contains(itemIds[i], 1)) {
				return false;
			}
		}
		return true;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	/**
	 * Gets the amount of an item.
	 *
	 * @param item The item.
	 * @return The amount of this item in this container.
	 */
	public int getAmount(Item item) {
		if (item == null) {
			return 0;
		}
		int count = 0;
		for (Item i : items) {
			if (i != null && i.getId() == item.getId()) {
				count += i.getAmount();
			}
		}
		return count;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @param id the id.
	 * @return the amount.
	 */
	public int getAmount(int id) {
		return getAmount(new Item(id));
	}
	
	/**
	 * Shifts the elements in the <b>Container</b> to the appropriate position.
	 */
	public void shift() {
		final Item[] itemss = items;
		clear(false);
		for (Item item : itemss) {
			if (item == null) {
				continue;
			}
			add(item, false);
		}
		refresh();
	}
	
	/**
	 * Checks if the container is empty.
	 *
	 * @return {@code True} if so.
	 */
	public boolean isEmpty() {
		for (Item item : items) {
			if (item != null) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Clears and updates the container.
	 */
	public void clear() {
		clear(true);
	}
	
	/**
	 * Clears the container.
	 *
	 * @param update If the container should be updated.
	 */
	public void clear(boolean update) {
		items = new Item[capacity];
		if (update) {
			refresh();
		}
	}
	
	/**
	 * Gets the wealth.
	 *
	 * @return the wealth.
	 */
	public long getWealth() {
		long wealth = 0;
		for (int i = 0, length = capacity; i < length; i++) {
			Item item = items[i];
			if (item == null) {
				continue;
			}

			wealth += PriceChecker.getPriceLong(item.getId()) * item.getAmount();
		}

		return wealth;
	}

	public String getWealthString() {
		return Misc.format(getWealth());
	}

	/**
	 * Returns an array representing this container.
	 *
	 * @return The array.
	 */
	public Item[] toArray() {
		return items;
	}
	
	/**
	 * Gets the capacity.
	 *
	 * @return The capacity of this container.
	 */
	public int capacity() {
		return capacity;
	}
	
	/**
	 * Removes all of an item.
	 *
	 * @param itemIDs The IDs of the items to remove.
	 */
	public boolean removeAll(int... itemIDs) {
		for (int itemID : itemIDs) {
			remove(new Item(itemID, Integer.MAX_VALUE)); // cover stackable items
			for (int i = 0; i < capacity(); i++)
				remove(new Item(itemID)); // and unstackable
		}
		return true;
	}
	
	/**
	 * Removes all of an item.
	 *
	 * @param item The item to remove.
	 */
	public boolean removeAll(Item item) {
		return removeAll(item.getId());
	}
	
	/**
	 * Checks if the player's inventory contains one of many items.
	 *
	 * @param itemIds
	 * @return NOTE: THIS SHOULD PROBABLY NOT BE USED FOR VERY MANY THINGS.
	 */
	public boolean containsOneOfManyItems(int... itemIds) {
		for (int itemId : itemIds) {
			if (contains(itemId, 1)) {
				return true;
			}
		}
		return false;
	}
	
	@NotNull
	@Override
	public Iterator<Item> iterator() {
		return new Iterator<Item>() {
			private int cursor = 0;
			
			@Override
			public boolean hasNext() {
				return cursor < items.length;
			}
			
			@Override
			public Item next() {
				Item item;
				do {
					item = items[cursor++];
				} while (item == null && hasNext());
				return item;
			}
		};
	}
	
	public boolean containsThenRemove(Item... items) {
		for (Item item : items) {
			if (!containsItem(item)) {
				return false;
			}
		}
		for (Item item : items) {
			if (!remove(item)) {
				return false;
			}
		}
		return true;
	}
	
	public void refreshAndUpdate(boolean force) {
		refresh();
		update(force);
	}
	
	public SortType getSortType() {
		return sortType;
	}
	
	public ContainerType getType() {
		return type;
	}
	
	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}

	/**
	 * Replaces the item on the given slot with the argued item.
	 *
	 * @param item         The item.
	 * @param slot         The slot.
	 * @param fireListener If the listener should be "notified".
	 * @return The old item.
	 */
	public Item replaceSwap(Item item, int slot, boolean fireListener) {
		return replace(item, slot, fireListener);
	}
	
	public boolean tryRemove(int... ids) {
		for (int id : ids) {
			if (!containsID(id)) {
				return false;
			}
		}
		for (int id : ids) {
			if (!remove(id)) {
				return false;
			}
		}
		return true;
	}

	public String getItemsJson() {
		return GsonSave.gsond.toJson(items);
	}

}
