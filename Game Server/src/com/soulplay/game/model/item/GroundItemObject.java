package com.soulplay.game.model.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;

public class GroundItemObject {

	private String ownerName;

	private int itemId;

	private int itemX;

	private int itemY;

	private int itemZ;

	private int itemAmount;

	private boolean ironmanDrop;

	private int ticks;

	public GroundItemObject(Player player, int id, int amount) {
		this(player.getName(), id, player.getX(), player.getY(), player.getZ(), amount, player.isIronMan());
	}

	public GroundItemObject(Player player, Location location, int id, int amount) {
		this(player.getName(), id, location.getX(), location.getY(), location.getZ(), amount, player.isIronMan());
	}

	public GroundItemObject(String ownerName, int id, int x, int y, int z,
			int amount, boolean ironmanDrop) {
		this.ownerName = ownerName;
		this.itemId = id;
		this.itemX = x;
		this.itemY = y;
		this.itemZ = z;
		this.itemAmount = amount;
		this.ticks = 4;
	}

	public int getItemAmount() {
		return itemAmount;
	}

	public int getItemId() {
		return itemId;
	}

	public int getItemX() {
		return itemX;
	}

	public int getItemY() {
		return itemY;
	}

	public int getItemZ() {
		return itemZ;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public int getTicks() {
		return ticks;
	}

	public boolean isIronmanDrop() {
		return ironmanDrop;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

}
