package com.soulplay.game.model.item;

import com.soulplay.game.model.player.Player;

public class DroppedItem {

	String dropper;

	private int dropperMID;

	GameItem item;

	String cause;

	public DroppedItem(Player dropper, GameItem item, String cause) {
		this.dropper = dropper.getName();
		this.item = item;
		this.cause = cause;
		this.setDropperMID(dropper.mySQLIndex);
	}

	public DroppedItem(Player dropper, int itemID, int amount, String cause) {
		this.dropper = dropper.getName();
		this.item = new GameItem(itemID, amount);
		this.cause = cause;
		this.setDropperMID(dropper.mySQLIndex);
	}

	public String getCause() {
		return cause;
	}

	public String getDropper() {
		return dropper;
	}

	public int getDropperMID() {
		return dropperMID;
	}

	public GameItem getItem() {
		return item;
	}

	public void setDropperMID(int dropperMID) {
		this.dropperMID = dropperMID;
	}
}
