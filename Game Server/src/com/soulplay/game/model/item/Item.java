package com.soulplay.game.model.item;

public class Item extends GameItem {

    private final transient int slot;

    public Item(int id) {
    	this(id, 1);
    }
    
    public Item(int id, int amount) {
    	this(id, amount, -1);
    }
    
    public Item(int id, int amount, int slot) {
    	super(id, amount);
    	this.slot = slot;
    }
    
    public int getSlot() {
    	return slot;
    }

    @Override
	public Item copy() {
		return new Item(getId(), getAmount());
	}
    
    public Item copy(int slot) {
    	return new Item(getId(), getAmount(), slot);
    }

}
