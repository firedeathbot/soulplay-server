package com.soulplay.game.model.item;

public class RangeItem extends GameItem {

	private final int maxAmount;

	public RangeItem(int id, int amount) {
		this(id, amount, amount);
	}

	public RangeItem(int id, int amount, int maxAmount) {
		super(id, amount);
		this.maxAmount = maxAmount;
	}

	public int getMinAmount() {
		return getAmount();
	}

	public int getMaxAmount() {
		return maxAmount;
	}

}
