package com.soulplay.game.model.item;

public class ComparableItem {

	private final int id;
	private final int amount;

	public ComparableItem(int id, int amount) {
		this.id = id;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public int getAmount() {
		return amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amount;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComparableItem other = (ComparableItem) obj;
		if (amount != other.amount)
			return false;
		if (id != other.id)
			return false;
		return true;
	}

}
