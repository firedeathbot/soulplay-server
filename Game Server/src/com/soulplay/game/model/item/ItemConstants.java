package com.soulplay.game.model.item;

import com.soulplay.Config;
import com.soulplay.content.items.degradeable.DegradeData;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.itemdata.CombatTab;
import com.soulplay.content.items.itemdata.CombatTabButton;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.items.useitem.MaxCapeUpgrades;
import com.soulplay.content.items.useitem.MaxCapeUpgrades.Items;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.definition.SpecialsInterfaceInfo;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;

import plugin.item.DawnbringerPlugin;
import plugin.item.SanguinestiStaffPlugin;
import plugin.item.chargeitems.ViggorasChainmacePlugin;
import plugin.item.trident.TridentOfTheSeasPlugin;
import plugin.item.trident.TridentOfTheSwampPlugin;

public class ItemConstants {

	private static final int MAGIC_SHORTBOW_I = 30056;
	private static final int STAFF_OF_THE_DEAD = 11791;
	private static final int TOXIC_STAFF_OF_THE_DEAD = 30011;

	public static boolean isShield(int id) {
		return ItemDefinition.getName(id).toLowerCase().contains("shield");
	}

	public static boolean isPoweredStaff(int id) {
		switch (id) {
			case TridentOfTheSeasPlugin.CHARGED_ITEM_ID:
			case TridentOfTheSeasPlugin.UNCHARGED_ITEM_ID:
			case TridentOfTheSwampPlugin.CHARGED_ITEM_ID:
			case TridentOfTheSwampPlugin.UNCHARGED_ITEM_ID:
			case DawnbringerPlugin.ID:
			case SanguinestiStaffPlugin.CHARGED_ITEM_ID:
			case SanguinestiStaffPlugin.UNCHARGED_ITEM_ID:
				return true;
			default:
				return false;
		}
	}

	public static boolean checkForMaxStats(int capeId) {
		for (Items item : MaxCapeUpgrades.Items.values) {
			if (item.getNewCapeID() == capeId) {
				return true;
			}
		}

		if (CompletionistCape.isCompletionistCape(capeId)) {
			return true;
		}

		if (capeId == 20767) {
			return true;
		}

		return false;
	}

	public static boolean isDragonBaneWeapon(Player player) {
		if (player == null) {
			return false;
		}

		int weaponId = player.playerEquipment[PlayerConstants.playerWeapon];
		switch (weaponId) {
			case 122978://Lance
			case 30087://Crossbow
				return true;
			default:
				return false;
		}
	}

	public static final int[] STAFF_OF_LIGHT_IDS = { 15486, 22206,
			22207, 22208, 22209, 22210, 22211, TOXIC_STAFF_OF_THE_DEAD, DegradeData.TSOTD_UNCHARGED, 124144 };

	public static boolean maleOnlyItem(int id) {
		switch (id) {
			case 13685:
			case 13686:
			case 13687:
			case 13691:
			case 13692:
			case 13693:
			case 13697:
			case 13698:
			case 13699:
				return true;
		}

		return false;
	}

	public static boolean femaleOnlyItem(int id) {
		switch (id) {
			case 13688:
			case 13689:
			case 13690:
			case 13694:
			case 13695:
			case 13696:
			case 13700:
			case 13701:
			case 13702:
				return true;
		}

		return false;
	}

	public static void addSpecialBarDefs(ItemDefinition def) {
		switch (def.getId()) {

			case 12926: // toxic blowpipe
			case 13883: // Morrigan's Throwing Axe
			case 13879: // Morrigan's Javelin
				// c.getPA().sendFrame171(0, 7649);
				// specialAmount(weapon, c.specAmount, 7661);
				def.setSpecInfo(new SpecialsInterfaceInfo(7649, 7661));
				break;

			case 1434: // Dragon Mace
			case 11061: // ancient mace
			case 10887: // Barrelchest Anchor
				// c.getPA().sendFrame171(0, 7624);
				// specialAmount(weapon, c.specAmount, 7636);
				def.setSpecInfo(new SpecialsInterfaceInfo(7624, 7636));
				break;

			case 14484: // dragon claws
				// c.getPA().sendFrame171(0, 7800);
				// specialAmount(weapon, c.specAmount, 7812);
				def.setSpecInfo(new SpecialsInterfaceInfo(7800, 7812));
				break;

			case 4151: // whip
			case 15441: // yellow whip
			case 15442: // blue whip
			case 15443: // white whip
			case 15444: // green whip
			case 12006: // abyssal tentacle
			case 21371: // abyssal vine whip
			case 21372: // abyssal vine whip
			case 21373: // abyssal vine whip
			case 21374: // abyssal vine whip
			case 21375: // abyssal vine whip
				// c.getPA().sendFrame171(0, 12323);
				// specialAmount(weapon, c.specAmount, 12335);
				def.setSpecInfo(new SpecialsInterfaceInfo(12323, 12335));
				break;

			case 859: // magic bows
			case 861:
			case MAGIC_SHORTBOW_I:
			case LmsManager.DARK_BOW:
			case 11235:
			case 15701:
			case 15702:
			case 15703:
			case 15704:
			case 20171:
			case 20173:
			case 6724:// seercull
			case 15241:// hand cannon
			case 11785: // armadyl crossbow
				// c.getPA().sendFrame171(0, 7549);
				// specialAmount(weapon, c.specAmount, 7561);
				def.setSpecInfo(new SpecialsInterfaceInfo(7549, 7561));
				break;

			case 7158: // Dragon 2h Sword
			case 11694: // Armadyl Godsword
			case 11696: // Bandos Godsword
			case 11698: // Saradomin Godsword
			case 11700: // Zamorak Godsword
			case 11730: // Saradomin Sword
				// c.getPA().sendFrame171(0, 7699);
				// specialAmount(weapon, c.specAmount, 7711);
				def.setSpecInfo(new SpecialsInterfaceInfo(7699, 7711));
				break;

			case 4587: // dscimmy
			case 20671: // Brackish blade
			case 19780: // korasi
			case 19784: // korasi new - alk
			case 13899: // Vesta's Longsword
			case 13901: // Vesta LongSword Deg
			case 13923: // Corrupt Vesta's LongSword
			case 13925: // Corrupt Vesta's LongSword
			case 1305: // dragon long
				// c.getPA().sendFrame171(0, 7599);
				// specialAmount(weapon, c.specAmount, 7611);
				def.setSpecInfo(new SpecialsInterfaceInfo(7599, 7611));
				break;

			case 3204: // d hally
				// c.getPA().sendFrame171(0, 8493);
				// specialAmount(weapon, c.specAmount, 8505);
				def.setSpecInfo(new SpecialsInterfaceInfo(8493, 8505));
				break;

			case 1377: // d battleaxe
				// c.getPA().sendFrame171(0, 7499);
				// specialAmount(weapon, c.specAmount, 7511);
				def.setSpecInfo(new SpecialsInterfaceInfo(7499, 7511));
				break;

			case 13902: // statius warhammer
			case 13904: // statius warahmmer (deg)
			case 13926: // statius corrupt warhammer
			case 13927: // statius corrupt warhammer (deg)
			case 4153: // gmaul
			case 30066: //maul
			case 30022: // dragon war hammer
				// c.getPA().sendFrame171(0, 7474);
				// specialAmount(weapon, c.specAmount, 7486);
				def.setSpecInfo(new SpecialsInterfaceInfo(7474, 7486));
				break;

			case 13905: // vesta spear
			case 13907: // vesta spear (deg)
			case 13929: // vesta corrupt spear
			case 13931: // vesta corrupt spear (deg)
			case 1249: // dspear
			case 11716: // zamorakian spear
				// c.getPA().sendFrame171(0, 7674);
				// specialAmount(weapon, c.specAmount, 7686);
				def.setSpecInfo(new SpecialsInterfaceInfo(7674, 7686));
				break;

			case STAFF_OF_THE_DEAD: // staff of the dead
			case TOXIC_STAFF_OF_THE_DEAD:
			case DegradeData.TSOTD_UNCHARGED:
			case 15486: // Staff of light
			case 15502: // sol
			case 22206: // sol
			case 22207: // sol
			case 22208: // sol
			case 22209: // sol
			case 22210: // sol
			case 22211: // sol
			case 22212: // sol
			case 22213: // sol
				// c.getPA().sendFrame171(0, 6117); // 6117 is kinda strange
				// spec bar
				// specialAmount(weapon, c.specAmount, 6129);
				def.setSpecInfo(new SpecialsInterfaceInfo(6117, 6129));
				break;

			case 1215:// dragon dagger
			case 1231:
			case 5680:
			case 5698:
			case 13468: // test dagger
				// c.getPA().sendFrame171(0, 7574);
				// specialAmount(weapon, c.specAmount, 7586);
				def.setSpecInfo(new SpecialsInterfaceInfo(7574, 7586));
				break;

			// default:
			// c.getPA().sendFrame171(1, 7624); // mace interface
			// c.getPA().sendFrame171(1, 7474); // hammer, gmaul
			// c.getPA().sendFrame171(1, 7499); // axe
			// c.getPA().sendFrame171(1, 7549); // bow interface
			// c.getPA().sendFrame171(1, 7574); // sword interface
			// c.getPA().sendFrame171(1, 7599); // scimmy sword interface, for
			// most swords
			// c.getPA().sendFrame171(1, 7649); //throwing axes/javelins
			// c.getPA().sendFrame171(1, 8493); // hally
			// c.getPA().sendFrame171(1, 12323); // whip interface
			// break;
		}
	}

	/**
	 * Gets the item ID from the item.cfg
	 *
	 * @param itemName
	 * @return
	 */
	public static int getItemId(String itemName) { // TODO: REPLACE THIS SHIT
													// WITH ITEMDEF
		for (int i = 0; i < Config.ITEM_LIMIT; i++) {
			ItemDefinition item = ItemDefinition.forId(i);
			if (item != null) {
				if (item.getName().equalsIgnoreCase(itemName)) {
					return item.getId();
				}
			}
		}
		return -1;
	}

	public static String getItemName(int itemID) {
		ItemDefinition item = ItemDefinition.forId(itemID);
		if (item != null) {
			return item.getName();
		}
		return "Unarmed";
	}

	/**
	 * Two handed weapon check.
	 **/
	public static boolean is2handed(int itemId) {
		final ItemDefinition item = ItemDefinition.forId(itemId);
		if (item == null) {
			return false;
		}
		if (item.isTwoHanded()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the item is a godsword hilt.
	 *
	 * @param i
	 * @return
	 */
	public static boolean isHilt(int i) {
		return i >= 11702 && i <= 11708 && i % 2 == 0;
	}

	public static void setHardCodedMovementAnimationIds(ItemDefinition def) {

		// default animations
		def.getMovementAnimIds()[0] = 0x328; // c.playerStandIndex = 0x328;
		def.getMovementAnimIds()[1] = 0x337; // c.playerTurnIndex = 0x337;
		def.getMovementAnimIds()[2] = 0x333; // c.playerWalkIndex = 0x333;
		def.getMovementAnimIds()[3] = 0x334; // c.playerTurn180Index = 0x334;
		def.getMovementAnimIds()[4] = 0x335; // c.playerTurn90CWIndex = 0x335;
		def.getMovementAnimIds()[5] = 0x336; // c.playerTurn90CCWIndex = 0x336;
		def.getMovementAnimIds()[6] = 0x338; // c.playerRunIndex = 0x338;

		final String weaponName = def.getName().toLowerCase();

		if (weaponName.contains("boxing gloves")) {
			def.getMovementAnimIds()[0] = 3677;
			def.getMovementAnimIds()[2] = 3680;
			return;
		}

		if (weaponName.equals("chaotic longsword")
				|| weaponName.contains("vesta's longsword")
				|| def.getId() == 20857) { // i guess these weapons have default
											// anims
			return;
		}
		
		if (weaponName.contains("scythe")) {
			def.getMovementAnimIds()[0] = 847;
			def.getMovementAnimIds()[2] = 9738;
			def.getMovementAnimIds()[6] = 9739;
			return;
		}

		if (def.getId() == 7447) { // kitchen knife
			def.getMovementAnimIds()[0] = 14592;
			def.getMovementAnimIds()[1] = 14593;
			def.getMovementAnimIds()[2] = 14593;
			def.getMovementAnimIds()[3] = 14593;
			def.getMovementAnimIds()[4] = 14593;
			def.getMovementAnimIds()[5] = 14593;
			def.getMovementAnimIds()[6] = 14593;
		}

		if (weaponName.contains("flag") || weaponName.contains("banner")) {
			def.getMovementAnimIds()[0] = 1421;
			def.getMovementAnimIds()[2] = 1422;
			def.getMovementAnimIds()[6] = 1427;
			return;
		}

		if (weaponName.contains("chaotic staff")) {
			def.getMovementAnimIds()[0] = 808;
			def.getMovementAnimIds()[6] = 1210;
			def.getMovementAnimIds()[2] = 1146;
			return;
		}

		if (weaponName.contains("halberd") || weaponName.contains("hasta")
				|| weaponName.contains("guthan")
				|| weaponName.contains("sceptre")) {
			def.getMovementAnimIds()[0] = 813;// 809;
			def.getMovementAnimIds()[2] = 1146;
			def.getMovementAnimIds()[6] = 1210;
			return;
		}
		if (weaponName.contains("sled")) {
			def.getMovementAnimIds()[0] = 1461;
			def.getMovementAnimIds()[2] = 1468;
			def.getMovementAnimIds()[6] = 1467;
			return;
		}
		if (weaponName.startsWith("basket")) {
			def.getMovementAnimIds()[0] = 1836;
			def.getMovementAnimIds()[2] = 1836;
			def.getMovementAnimIds()[6] = 1836;
			return;
		}
		if (weaponName.contains("dharok")) {
			def.getMovementAnimIds()[0] = 0x811;
			def.getMovementAnimIds()[2] = 2064;
			return;
		}
		if (weaponName.contains("ahrim")) {
			def.getMovementAnimIds()[0] = 809;
			def.getMovementAnimIds()[2] = 1146;
			def.getMovementAnimIds()[6] = 1210;
			return;
		}
		if (weaponName.contains("verac")) {
			def.getMovementAnimIds()[0] = 2061/* 1832 */;
			def.getMovementAnimIds()[2] = 2060/* 1830 */;
			// def.getMovementAnimIds()[6] = 1831;
			return;
		}
		if (weaponName.contains("ivandis")) {
			def.getMovementAnimIds()[0] = 9049;
			def.getMovementAnimIds()[2] = 9051;
			def.getMovementAnimIds()[6] = 9050;
			return;
		}
		if (weaponName.contains("novite maul")
				|| weaponName.contains("marmaros maul")
				|| weaponName.contains("kratonite maul")
				|| weaponName.contains("fractite maul")
				|| weaponName.contains("zephyrium maul")
				|| weaponName.contains("argonite maul")
				|| weaponName.contains("katagon maul")
				|| weaponName.contains("gorgonite maul")
				|| weaponName.contains("promethium maul")
				|| weaponName.contains("primal maul")
				|| weaponName.contains("chaotic maul")) {
			def.getMovementAnimIds()[0] = 13217;
			def.getMovementAnimIds()[2] = 13218;
			def.getMovementAnimIds()[6] = 13220;
			return;
		}
		// if (weaponName.contains("wand") || weaponName.contains("staff")) {
		// def.getMovementAnimIds()[0] = 809;
		// def.getMovementAnimIds()[6] = 1210;
		// def.getMovementAnimIds()[2] = 1146;
		// return;
		// }

		if (weaponName.contains("spear")) {
			def.getMovementAnimIds()[0] = 12010;
			def.getMovementAnimIds()[6] = 12016;
			def.getMovementAnimIds()[2] = 12012;

			def.getMovementAnimIds()[3] = 12013;
			def.getMovementAnimIds()[4] = 12014;
			def.getMovementAnimIds()[5] = 12015;
			def.getMovementAnimIds()[1] = 12011;

			return;
		}
		if (weaponName.contains("wand") || weaponName.contains("staff")) {
			def.getMovementAnimIds()[0] = 8980;
			def.getMovementAnimIds()[6] = 1210;
			def.getMovementAnimIds()[2] = 1146;
			return;
		}
		if (weaponName.contains("karil")) {
			def.getMovementAnimIds()[0] = 2074;
			def.getMovementAnimIds()[2] = 2076;
			def.getMovementAnimIds()[6] = 2077;
			return;
		}
		if (weaponName.contains("2h sword") || weaponName.contains("godsword")
				|| weaponName.contains("saradomin sw")) {
			def.getMovementAnimIds()[0] = 7047;
			def.getMovementAnimIds()[2] = 7046;
			def.getMovementAnimIds()[6] = 7039;
			def.getMovementAnimIds()[1] = 7044;
			def.getMovementAnimIds()[3] = 7044;
			def.getMovementAnimIds()[4] = 7044;
			def.getMovementAnimIds()[5] = 7044;
			return;
		}
		if (weaponName.contains("korasi's")
				|| weaponName.contains("dragon scimitar")) {
			def.getMovementAnimIds()[0] = 15069;
			def.getMovementAnimIds()[2] = 15073;
			def.getMovementAnimIds()[6] = 15070;
			def.getMovementAnimIds()[3] = 15075;
			def.getMovementAnimIds()[4] = 15077;
			def.getMovementAnimIds()[5] = 15076;
			return;
		}
		if (weaponName.contains("longsword") || weaponName.contains("sword")
				|| weaponName.contains("scimitar")) {
			def.getMovementAnimIds()[0] = 808;
			def.getMovementAnimIds()[1] = 823;
			def.getMovementAnimIds()[2] = 819;
			def.getMovementAnimIds()[6] = 824;
			def.getMovementAnimIds()[3] = 820;
			def.getMovementAnimIds()[4] = 822;
			def.getMovementAnimIds()[5] = 821;
			return;
		}
		/*
		 * if(weaponName.contains("scimitar") ||
		 * weaponName.contains("korasi's")) { def.getMovementAnimIds()[0] =
		 * 12021; def.getMovementAnimIds()[2] = 12024;
		 * def.getMovementAnimIds()[6] = 12016; def.getMovementAnimIds()[3] =
		 * 12013; def.getMovementAnimIds()[4] = 12014;
		 * def.getMovementAnimIds()[5] = 12015; }
		 */
		if (weaponName.contains("bow") || weaponName.equals("seercull")) {
			def.getMovementAnimIds()[0] = 808;
			def.getMovementAnimIds()[2] = 819;
			def.getMovementAnimIds()[6] = 824;
			return;
		}

		switch (def.getId()) { // adding here might slow down load time

			case 4151:
			case 15444:
			case 15443:
			case 15442:
			case 15441:
			case 21371:
			case 21372:
			case 21373:
			case 21374:
			case 21375:
				def.getMovementAnimIds()[0] = 11973;
				def.getMovementAnimIds()[2] = 1660;
				def.getMovementAnimIds()[6] = 1661;
				break;
			case 12006: // tentacle whip
				def.getMovementAnimIds()[0] = 808;
				def.getMovementAnimIds()[2] = 1660;
				def.getMovementAnimIds()[6] = 1661;
				break;
			case 8871:
				def.getMovementAnimIds()[0] = 4193;
				def.getMovementAnimIds()[2] = 4194; 
				def.getMovementAnimIds()[6] = 4194; 
				break;

			case 7960:
				def.getMovementAnimIds()[0] = 2065;
				def.getMovementAnimIds()[2] = 2064;
				break;
			case 6528:
				def.getMovementAnimIds()[0] = 0x811;
				def.getMovementAnimIds()[2] = 2064;
				def.getMovementAnimIds()[6] = 1664;
				break;
				
			case 20084:
			case 4153:
			case 30066:
			case 18353:
				def.getMovementAnimIds()[0] = 1662;
				def.getMovementAnimIds()[2] = 1663;
				def.getMovementAnimIds()[6] = 1664;
				break;
				
			case 10887:
				def.getMovementAnimIds()[0] = 5869;
				def.getMovementAnimIds()[2] = 5867;
				def.getMovementAnimIds()[6] = 5868;
				break;
			case 11694:
			case 11696:
			case 11730:
			case 11698:
			case 11700:
				def.getMovementAnimIds()[0] = 7047;
				def.getMovementAnimIds()[2] = 7046;
				def.getMovementAnimIds()[6] = 7039;
				def.getMovementAnimIds()[1] = 7044;
				def.getMovementAnimIds()[3] = 7044;
				def.getMovementAnimIds()[4] = 7044;
				def.getMovementAnimIds()[5] = 7044;
				break;
			case 1305:
				def.getMovementAnimIds()[0] = 809;
				break;
			case 15241: // hand cannon
				def.getMovementAnimIds()[0] = 12155;
				def.getMovementAnimIds()[2] = 12154;
				def.getMovementAnimIds()[6] = 12154;
				break;
			case 6724:
				def.getMovementAnimIds()[0] = 808;
				def.getMovementAnimIds()[2] = 819;
				def.getMovementAnimIds()[6] = 824;
				break;

		}
	}

	private ItemConstants() {
	}

	public static int getAttackDelayDump(int weaponId, String s) {
	
		if (weaponId == 10033
				|| weaponId == 10034) { // chins
			return 4;
		}
		if (weaponId == -1) {
			return 4;// unarmed
		}
		if (weaponId == 12926) {
			return 4;
		}
		switch (weaponId) {
			case 11785: // armadyl crossbow
				return 6;
			case LmsManager.DARK_BOW:
			case 11235:
			case 15701:
			case 15702:
			case 15703:
			case 15704:
				return 8;
			case 11730:
			case 11791:
			case 18349:
			case 7447: // kitchen knife
				return 4;
			case 6528:
			case 18353: // chaotic maul
				return 7;
			case 10033:
			case 10034:
			case 18351:
				return 5;
			case 15241: // hand cannon
			case 30087: // dragon hunter crossbow
				return 6; // ss3 had it at 7 cuz op
			case 15443: // white whip
				return 4;
			case 20857: // prestige 3 weapon
				return 2;
			case 20929:
				return 1;
		}
		if (s.endsWith("greataxe")) {
			return 7;
		} else if (s.equals("torags hammers")) {
			return 5;
		} else if (s.equals("barrelchest anchor")) {
			return 7;
		} else if (s.equals("guthans warspear")) {
			return 5;
		} else if (s.equals("veracs flail")) {
			return 5;
		} else if (s.equals("ahrims staff")) {
			return 6;
		} else if (s.contains("staff")) {
			if (s.contains("zamarok") || s.contains("guthix")
					|| s.contains("saradomian") || s.contains("slayer")
					|| s.contains("ancient")) {
				return 4;
			} else {
				return 5;
			}
		} else if (s.contains("bow")) {
			if (s.contains("composite") || s.equals("seercull")) {
				return 5;
			} else if (s.contains("aril")) {
				return 4;
			} else if (s.contains("Ogre")) {
				return 8;
			} else if (s.contains("short") || s.contains("hunt")
					|| s.contains("sword")) {
				return 4;
			} else if (s.contains("long") || s.contains("crystal")) {
				return 6;
			} else if (s.contains("'bow") || s.contains("crossbow")) {
				return 6;
			}
	
			return 5;
		} else if (s.contains("dagger")) {
			return 4;
		} else if (s.contains("godsword")) {
			return 6;
		} else if (s.contains("2h") || s.contains("maul")) {
			return 7;
		} else if (s.contains("longsword")) {
			return 5;
		} else if (s.contains("sword")) {
			return 4;
		} else if (s.contains("scimitar")) {
			return 4;
		} else if (s.contains("mace")) {
			return 5;
		} else if (s.contains("battleaxe")) {
			return 6;
		} else if (s.contains("pickaxe")) {
			return 5;
		} else if (s.contains("thrownaxe")) {
			return 5;
		} else if (s.contains("axe")) {
			return 5;
		} else if (s.contains("warhammer")) {
			return 6;
		} else if (s.contains("2h")) {
			return 7;
		} else if (s.contains("spear")) {
			return 5;
		} else if (s.contains("claw")) {
			return 4;
		} else if (s.contains("halberd")) {
			return 7;
		} else if (s.equals("granite maul")) {
			return 7;
		} else if (s.equals("toktz-xil-ak")) {
			return 4;
		} else if (s.equals("tzhaar-ket-em")) {
			return 5;
		} else if (s.equals("tzhaar-ket-om")) {
			return 7;
		} else if (s.equals("maul")) {
			return 7;
		} else if (s.equals("toktz-xil-ek")) {
			return 4;
		} else if (s.equals("toktz-xil-ul")) {
			return 4;
		} else if (s.equals("toktz-mej-tal")) {
			return 6;
		} else if (s.contains("whip")) {
			return 4;
		} else if (s.contains("tentacle")) {
			return 4;
		} else if (s.contains("dart")) {
			return 3;
		} else if (s.contains("knife")) {
			return 3;
		} else if (s.contains("javelin")) {
			return 6;
		}
		return 5;
	}
	
	public static int getBlockEmoteToDump(int itemId, String name) {
		if (itemId == 13902
				|| itemId == 13926) { // statius
																				// warhammer
			return 13038;
		}
		if (itemId == 7447) { // kitchen																		// knife
			return 14591;
		}
		if (name.contains("battleaxe") || name.contains("hatchet")) {
			return 397;
		}

		if (name.contains("boxing gloves")) {
			return 3679;
		}

		if (name.contains("defender")) {
			return 4177;
		}
		if (name.contains("2h")) {
			return 7050;
		}
		if (name.contains("light")) {
			return 12806;
		}
		if (name.contains("book")
				|| (name.contains("wand") || (name.contains("staff")))) {
			return 420;
		}
		if (name.contains("shield")) {
			return 1156;
		}

		if (name.contains("ivandis")) {
			return 9048;
		}
		if (name.contains("spear")) {
			return PlayerConstants.EMOTE_BLOCK_SPEAR;
		}

		switch (itemId) {
			case 4755:
				return 12004;
			case 15241: // hand cannon
				return 12156;
			case 13899:
			case 13901: // Vesta LongSword Deg
			case 13923: // Corrupt Vesta's LongSword
			case 13925: // Corrupt Vesta's LongSword
				return 13042;
			case 18355:
				return 13046;
			case 14484: // dragon claws
				return 397;
			case 11716:
				return 12008;
			case 4153:
			case 30066:
				return 1666;
			case 4151:
			case 15444:
			case 15443:
			case 15442:
			case 15441:
			case 21371:
			case 21372:
			case 21373:
			case 21374:
			case 21375:
			case 12006:
				return 11974;// 1659;
			case 15486:
				return 12806;
			case 18349:
				return 12030;
				
			case 20084:
			case 18353:
				return 13054;
				
			case 18351:
				return 13042;

			case 11694:
			case 11698:
			case 11700:
			case 11696:
			case 11730:
				return 7050;
			case -1:
				return 424;
			default:
				return 0;
		}
	}

	public static int getRangeDistReqDump(int itemId) {
		switch (itemId) {
		case 12926:
			return RSConstants.KNIVES_DIST;

		case 30061:
		case 24145:
		case 30382:
		case 30384:
			return RSConstants.LONGBOW_DIST;

		case 30092:
			return RSConstants.TWISTED_BOW_DIST;

		case 10033:
		case 10034:
		case 30356:
		case 30023:
		case 30024:
			return RSConstants.LONGBOW_DIST;


		case 30093:
			return RSConstants.DARTS_DIST;
		case 6522: // obby rings
			return RSConstants.SHORTBOW_DIST;
		case 7447:
			return 1;
		}

		ItemDefinition def = ItemDefinition.forId(itemId);
		if (def == null) {
			return 1;
		}
		final String weaponName = def.getName();
		if (weaponName.equals("No Name")) {
			return 1;
		} else if (weaponName.startsWith("Crystal bow")) {
			return RSConstants.CRYSTAL_BOW_DIST;
		} else if (weaponName.equals("Noxious longbow")
				|| weaponName.equals("Wyvern crossbow")
				|| weaponName.equals("Royal crossbow")
				|| weaponName.equals("Sagaie")
				|| weaponName.equals("Composite bows")
				|| weaponName.equals("Sighted")) {
			return RSConstants.NOXIOUS_LONGBOW_DIST;
		} else if (weaponName.equals("Shieldbow")) {
			return RSConstants.SHIELDBOW_DIST;
		} else if (weaponName.equals("Dark bow")
				|| weaponName.toLowerCase().contains("longbow")
				|| weaponName.equals("Chinchompa")
				|| weaponName.equals("Hand cannon")) {
			return RSConstants.LONGBOW_DIST;
		} else if (weaponName.equals("Zaryte bow")
				|| weaponName.equals("Crystal bow")
				|| weaponName.equals("Seercull")
				|| weaponName.equals("Morrigan's javelin")
				|| weaponName.toLowerCase().contains("shortbow")
				|| weaponName.toLowerCase().contains("crossbow")
				|| weaponName.toLowerCase().contains("c'bow")
			    || weaponName.toLowerCase().contains("twisted bow")
			    || weaponName.toLowerCase().contains("hexhunter bow")) {
			return RSConstants.SHORTBOW_DIST;
		} else if (weaponName.equals("Death lotus dart")) {
			return RSConstants.DEATH_LOTUS_DART_DIST;
		} else if (weaponName.toLowerCase().contains("knife")
				|| weaponName.toLowerCase().contains("javelin")
				|| weaponName.equals("Blisterwood stake")) {
			return RSConstants.KNIVES_DIST;
		} else if (weaponName.toLowerCase().contains(" dart")
				|| weaponName.endsWith("thrownaxe")
				|| weaponName.endsWith("throwing axe")) {
			return RSConstants.DARTS_DIST;
		} else if (weaponName.endsWith("halberd")) {
			return 2;
		}
		return 1;
	}

	 public static void dumpCombatTabWeaponInfo(int weaponId, String weaponName) {
			switch (weaponId) {
				case 124417:// Inquisitor's mace
				case ViggorasChainmacePlugin.CHARGED_ID:
				case ViggorasChainmacePlugin.UNCHARGED_ID: {
					CombatTab tab = new CombatTab(3796, 3799, CombatTabButton.MACE_POUND, CombatTabButton.MACE_PUMMEL,
							CombatTabButton.MACE_SPIKE, CombatTabButton.MACE_BLOCK);
					for (int i = 0; i < tab.getButtons().length; i++) {
						if (i == 2)
							tab.getButtons()[i] = tab.getButtons()[i]
									.createNewAttackEmoteButton(Animation.getOsrsAnimId(246));
						else
							tab.getButtons()[i] = tab.getButtons()[i]
									.createNewAttackEmoteButton(Animation.getOsrsAnimId(245));
					}
					CombatTab.combatTabButtons.put(weaponId, tab);
					return;
				}

				case 7671:
				case 7673: {
					CombatTab tab = new CombatTab(5855, 5857, CombatTabButton.BOXING_PUNCH,
							CombatTabButton.UNARMED_KICK, CombatTabButton.BOXING_BLOCK);
					for (int i = 0; i < tab.getButtons().length; i++)
						tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2779);
					CombatTab.combatTabButtons.put(weaponId, tab);
					return;
				}

				case 10033:
				case 10034:
				case 30356: {
					CombatTab tab = new CombatTab(4446, 4449, CombatTabButton.SHORTBOW_ACC,
							CombatTabButton.SHORTBOW_RAPID, CombatTabButton.SHORTBOW_LONG);
					for (int i = 0; i < tab.getButtons().length; i++)
						tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2779);
					CombatTab.combatTabButtons.put(weaponId, tab);
					return;
				}

				case 24145: {
					CombatTab tab = new CombatTab(1764, 1767, CombatTabButton.SHORTBOW_ACC,
							CombatTabButton.SHORTBOW_RAPID, CombatTabButton.SHORTBOW_LONG);
					for (int i = 0; i < tab.getButtons().length; i++)
						tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(12153);
					CombatTab.combatTabButtons.put(weaponId, tab);
					return;
				}

				case 30094: // bulwark
					CombatTab.combatTabButtons.put(weaponId, new CombatTab(425, 428, CombatTabButton.BULWARK_POUND,
							CombatTabButton.BULWARK_PUMMEL, CombatTabButton.BULWARK_BLOCK));
					return;

				case 30327:
				case 30328:
					CombatTab.combatTabButtons.put(weaponId,
							new CombatTab(776, 779, CombatTabButton.SCYTHE_OF_VITUR_REAP,
									CombatTabButton.SCYTHE_OF_VITUR_CHOP, CombatTabButton.SCYTHE_OF_VITUR_JAB,
									CombatTabButton.SCYTHE_OF_VITUR_BLOCK));
					return;

				// warhammers
				case 30018:
				case 4747:
				case 14728: {
					CombatTab tab = new CombatTab(425, 428, CombatTabButton.MAUL_HAMMER_POUND,
							CombatTabButton.MAUL_HAMMER_PUMMEL, CombatTabButton.MAUL_HAMMER_BLOCK);
					CombatTab.combatTabButtons.put(weaponId, tab);
					return;
				}

			}

	    	if (weaponName == null) {
	    		weaponName = "Unarmed";
	    	}

	    	String weaponNameLowerCaseTrimmed = weaponName.replace("Bronze", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Iron", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Steel", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Black", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Mithril", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Adamant", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Rune", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Granite", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Dragon", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Drag", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.replace("Crystal", "");
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.trim();
	    	weaponNameLowerCaseTrimmed = weaponNameLowerCaseTrimmed.toLowerCase();
	    	weaponName = weaponName.replace("_", " ");

	    	/**
	    	 * Attack styles.
	    	 */
	    	if (weaponName.equals("Unarmed")) {
	    		CombatTab.combatTabButtons.put(weaponId, new CombatTab(5855, 5857, CombatTabButton.UNARMED_PUNCH, CombatTabButton.UNARMED_KICK, CombatTabButton.UNARMED_BLOCK));
	    	} else if (weaponName.contains("whip") || weaponName.contains("tentacle") ||  weaponName.contains("Mouse toy")) {
	    		CombatTab tab = new CombatTab(12290, 12293, CombatTabButton.WHIP_FLICK, CombatTabButton.WHIP_LASH, CombatTabButton.WHIP_DEFLECT);
	    		if (weaponId == 12006) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(1658/*Animation.getOsrsAnimId(1658)*/);
	    		}
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		
	    	} else if (weaponName.contains("godsword") || weaponName.contains("2h")
	    			|| weaponName.contains("Saradomin sword")) {
	    		
	    		CombatTab.combatTabButtons.put(weaponId, new CombatTab(4705, 4708, CombatTabButton.TWO_H_CHOP, CombatTabButton.TWO_H_SLASH, CombatTabButton.TWO_H_SMASH, CombatTabButton.TWO_H_BLOCK));

	    	} else if (weaponName.contains("bow") || weaponName.contains("10") || weaponName.contains("full")
	    			|| weaponNameLowerCaseTrimmed.contains("seercull") || weaponName.contains("cannon") || weaponNameLowerCaseTrimmed.contains("ballista")) {
	    		CombatTab tab = new CombatTab(1764, 1767, CombatTabButton.SHORTBOW_ACC, CombatTabButton.SHORTBOW_RAPID, CombatTabButton.SHORTBOW_LONG);

	    		if (weaponId == 15241) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(12153);
	    		} else if (weaponNameLowerCaseTrimmed.contains("karil")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2075);
	    		} else if (weaponNameLowerCaseTrimmed.contains("'bow") || weaponNameLowerCaseTrimmed.contains("crossbow")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(4230);
	    		}
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		
	    	} else if (weaponNameLowerCaseTrimmed.contains("halberd")) {
	    		CombatTab.combatTabButtons.put(weaponId, new CombatTab(8460, 8463, CombatTabButton.HALBERD_JAB, CombatTabButton.HALBERD_SWIPE, CombatTabButton.HALBERD_FEND));

	    	} else if (weaponName.contains("staff") || weaponName.contains("wand") || weaponName.contains("Staff") || weaponId == 6526 || weaponName.contains("of light") || weaponId == TOXIC_STAFF_OF_THE_DEAD || weaponId == DegradeData.TSOTD_UNCHARGED || weaponId == 11791 || weaponNameLowerCaseTrimmed.contains("flag") || weaponNameLowerCaseTrimmed.contains("banner") || weaponNameLowerCaseTrimmed.contains("sceptre") || weaponNameLowerCaseTrimmed.equalsIgnoreCase("dawnbringer")) { // war sticks? or battle staffs?
	    		CombatTab tab = new CombatTab(6103, 6132, CombatTabButton.COMBAT_STICK_BASH, CombatTabButton.COMBAT_STICK_POUND, CombatTabButton.COMBAT_STICK_BLOCK);
	    		if (weaponNameLowerCaseTrimmed.contains("ahrim")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(406);
	    		}

	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    	/*} else if (weaponName.contains("staff") || weaponName.contains("wand") || weaponName.contains("Staff") || weaponId == 6526) {
	    		CombatTab tab = new CombatTab(328, 331, CombatTabButton.COMBAT_STICK_BASH, CombatTabButton.COMBAT_STICK_POUND, CombatTabButton.COMBAT_STICK_BLOCK);

	    		if (weaponNameLowerCaseTrimmed.contains("ahrim")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(406);
	    		}
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		
	    	*/} else if (weaponNameLowerCaseTrimmed.contains("scimitar") || weaponNameLowerCaseTrimmed.contains("longsword")
	    			|| weaponNameLowerCaseTrimmed.contains("korasi") || weaponNameLowerCaseTrimmed.contains("achete") || weaponId == 11037 || weaponId == 7447 || weaponId == 5018) {
	    		CombatTab tab = new CombatTab(2423, 2426, CombatTabButton.LONGSWORD_CHOP, CombatTabButton.LONGSWORD_SLASH, CombatTabButton.LONGSWORD_LUNGE, CombatTabButton.LONGSWORD_BLOCK);
	    		
	    		if (weaponNameLowerCaseTrimmed.contains("scimitar")) {
		    		tab.getButtons()[0] = tab.getButtons()[0].createNewAttackEmoteButton(PlayerConstants.EMOTE_SLASH_SCIMITAR);
	    			tab.getButtons()[1] = tab.getButtons()[1].createNewAttackEmoteButton(PlayerConstants.EMOTE_SLASH_SCIMITAR);
	    			tab.getButtons()[2] = tab.getButtons()[2].createNewAttackEmoteButton(PlayerConstants.EMOTE_STAB_SCIMITAR);
	    			tab.getButtons()[3] = tab.getButtons()[3].createNewAttackEmoteButton(PlayerConstants.EMOTE_SLASH_SCIMITAR);
	    		} else if (weaponId == 7447) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(14589);
	    		}
	    		CombatTab.combatTabButtons.put(weaponId, tab);

	    	} else if (weaponNameLowerCaseTrimmed.contains("dagger") || weaponNameLowerCaseTrimmed.contains("sword") || weaponNameLowerCaseTrimmed.contains("rapier") || weaponNameLowerCaseTrimmed.contains("harpoon") || weaponId == 6523 || weaponId == 6525 || weaponId == 20671) {
	    		CombatTab tab = new CombatTab(2276, 2279, CombatTabButton.SWORD_STAB, CombatTabButton.SWORD_LUNGE, CombatTabButton.SWORD_SLASH, CombatTabButton.SWORD_BLOCK);

	    		if (weaponId == 122324) {
	    			tab.getButtons()[0] = tab.getButtons()[0].createNewAttackEmoteButton(Animation.getOsrsAnimId(8145));
	    			tab.getButtons()[1] = tab.getButtons()[1].createNewAttackEmoteButton(Animation.getOsrsAnimId(8145));
	    			tab.getButtons()[3] = tab.getButtons()[3].createNewAttackEmoteButton(Animation.getOsrsAnimId(8145));
	    		}
//	    		if (weaponNameLowerCaseTrimmed.contains("dagger") || weaponId == 6525) {
//	    			for (int i = 0; i < tab.getButtons().length; i++)
//	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(386);
//	    		}
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		
	    	} else if (weaponNameLowerCaseTrimmed.contains("dart") || weaponNameLowerCaseTrimmed.contains("knife") || weaponNameLowerCaseTrimmed.contains("javelin")
	    			|| weaponNameLowerCaseTrimmed.contains("thrownaxe") || weaponId == 6522
	    			|| weaponNameLowerCaseTrimmed.contains("throwing axe") || weaponName.equals("Toxic Blowpipe")
	    			|| weaponNameLowerCaseTrimmed.toLowerCase().contains("chinchompa") || weaponNameLowerCaseTrimmed.contains("salamander") || weaponId == TridentOfTheSwampPlugin.CHARGED_ITEM_ID || weaponId == TridentOfTheSeasPlugin.CHARGED_ITEM_ID) {
	    		CombatTab tab = new CombatTab(4446, 4449, CombatTabButton.SHORTBOW_ACC, CombatTabButton.SHORTBOW_RAPID, CombatTabButton.SHORTBOW_LONG);

	    		if (weaponNameLowerCaseTrimmed.contains("dart")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(582);
	    		} else if (weaponNameLowerCaseTrimmed.contains("knife")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(i == 1 ? 9057 : 9055);
	    		} else if (weaponNameLowerCaseTrimmed.contains("javelin") || weaponNameLowerCaseTrimmed.contains("thrownaxe")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(10501);
	    		} else if (weaponId == 6522) { // toktz-xil-ul
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2614);
	    		} else if (weaponName.equalsIgnoreCase("morrigan's throwing axe")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(10504);
	    		} else if (weaponId == BlowPipeCharges.BLOWPIPE_FULL) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(25061);
	    		} else if (weaponNameLowerCaseTrimmed.toLowerCase().contains("chinchompa")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2779);
	    		} else if (weaponNameLowerCaseTrimmed.contains("salamander")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2779); // chins attack emote for now XD
	    		}
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		

	    	} else if (weaponNameLowerCaseTrimmed.contains("warhammer") || weaponNameLowerCaseTrimmed.contains("maul") || weaponId == 4566 || weaponId == 10732 || weaponId == 6528 || weaponId == 20084) {
	    		CombatTab tab = new CombatTab(425, 428, CombatTabButton.MAUL_HAMMER_POUND, CombatTabButton.MAUL_HAMMER_PUMMEL, CombatTabButton.MAUL_HAMMER_BLOCK);

	    		if (weaponId == 6528) { // obby maul
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2661);
	    		} else if (weaponNameLowerCaseTrimmed.contains("statius")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(13035);
	    		} else if (weaponId == 4153 || weaponId == 13445 || weaponId == 30066) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(1665);
	    		} else if (weaponNameLowerCaseTrimmed.contains("maul")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(13055);
	    		}
	    		
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		
	    	} else if (weaponNameLowerCaseTrimmed.contains("pickaxe")) {
	    		
	    		CombatTab.combatTabButtons.put(weaponId, new CombatTab(5570, 5573, CombatTabButton.PICKAXE_SPIKE, CombatTabButton.PICKAXE_IMPALE, CombatTabButton.PICKAXE_SMASH, CombatTabButton.PICKAXE_BLOCK));

	    	} else if (weaponNameLowerCaseTrimmed.contains("battleaxe") || weaponNameLowerCaseTrimmed.contains("axe") || weaponNameLowerCaseTrimmed.contains("hatchet") || weaponNameLowerCaseTrimmed.contains("greataxe")) {
	    		
	    		CombatTab tab = new CombatTab(1698, 1701, CombatTabButton.BATTLEAXE_CHOP, CombatTabButton.BATTLEAXE_HACK, CombatTabButton.BATTLEAXE_SMASH, CombatTabButton.BATTLEAXE_BLOCK);
	    		
	    		if (weaponNameLowerCaseTrimmed.contains("greataxe")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(i == 2 ? 2067 : 2066);
	    		}
	    		
	    		CombatTab.combatTabButtons.put(weaponId, tab);
	    		
	    	} else if (weaponNameLowerCaseTrimmed.contains("scythe")) {
	    		
	    		CombatTab.combatTabButtons.put(weaponId, new CombatTab(776, 779, CombatTabButton.SCYTHE_REAP, CombatTabButton.SCYTHE_CHOP, CombatTabButton.SCYTHE_JAB, CombatTabButton.SCYTHE_BLOCK));

	    	} else if (weaponNameLowerCaseTrimmed.contains("spear") || weaponNameLowerCaseTrimmed.contains("hasta") || weaponNameLowerCaseTrimmed.contains("lance")) {
	    		
	    		CombatTab tab = new CombatTab(4679, 4682, CombatTabButton.SPEAR_LUNGE, CombatTabButton.SPEAR_SWIPE, CombatTabButton.SPEAR_POUND, CombatTabButton.SPEAR_BLOCK);

//	    		if (weaponNameLowerCaseTrimmed.contains("hasta")) {
//	    			for (int i = 0; i < tab.getButtons().length; i++)
//	    				tab.getButtons()[i] = tab.getButtons()[i].setAttackEmote(5865);
//	    		}
	    		
	    		CombatTab.combatTabButtons.put(weaponId, tab);

	    	} else if (weaponNameLowerCaseTrimmed.contains("mace") || weaponNameLowerCaseTrimmed.contains("anchor")
	    			|| weaponNameLowerCaseTrimmed.contains("flail") || weaponNameLowerCaseTrimmed.contains("cane")) {
	    		
	    		CombatTab tab = new CombatTab(3796, 3799, CombatTabButton.MACE_POUND, CombatTabButton.MACE_PUMMEL, CombatTabButton.MACE_SPIKE, CombatTabButton.MACE_BLOCK);

	    		if (weaponId == 10887) { // barrelchest anchor
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(5865);
	    		} else if (weaponNameLowerCaseTrimmed.contains("verac")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(2062);
	    		} else if (weaponNameLowerCaseTrimmed.contains("ivandis")) {
	    			for (int i = 0; i < tab.getButtons().length; i++)
	    				tab.getButtons()[i] = tab.getButtons()[i].createNewAttackEmoteButton(9097);
	    		}
	    		
	    		CombatTab.combatTabButtons.put(weaponId, tab);

	    	} else if (weaponNameLowerCaseTrimmed.contains("claw")) {
	    		CombatTab.combatTabButtons.put(weaponId, new CombatTab(7762, 7765, CombatTabButton.CLAWS_CHOP, CombatTabButton.CLAWS_SLASH, CombatTabButton.CLAWS_LUNGE, CombatTabButton.CLAWS_BLOCK));

	    	} else {
	    		// default? unarmed?
	    	}

	    }
	
}
