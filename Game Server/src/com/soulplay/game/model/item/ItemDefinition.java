package com.soulplay.game.model.item;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.definition.ChargeInfo;
import com.soulplay.game.model.item.definition.SpecialsInterfaceInfo;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.util.Misc;

public class ItemDefinition {
	
	public static final String FOLDER_LOC = "./Data/cache/def/items/";
	
	public static boolean isLoaded = false;

	/**
	 * The definitions.
	 */
	private static Map<Integer, ItemDefinition> definitions = new HashMap<>();

	/**
	 * Get an items definition by id.
	 *
	 * @param id
	 *            The id.
	 * @return The item definition.
	 */
	public static ItemDefinition forId(int id) {
		if (id < 0) {
			return null;
		}

		return definitions.get(id);
	}

	public static String getName(int id) {
		ItemDefinition itemDef = forId(id);
		if (itemDef == null) {
			return "null";
		}

		return itemDef.name;
	}

	public static Map<Integer, ItemDefinition> getDefinitions() {
		return definitions;
	}

	/**
	 * Loads item definitions from item_defs.json
	 */
	public static void load() throws IOException {
		System.out.println("Loading item definitions...");

		setupDefaultDefinition();

		loadFile(FOLDER_LOC + "item_defs.json");
		loadFile(FOLDER_LOC + "item_defs_osrs.json");
		//printIdsForClient(FOLDER_LOC + "item_defs_osrs.json");
		System.out.println("Loaded hardcoded movement animations and other PI details");

		//ItemProjectInsanity.loadWeight("./Data/" + Config.cfgDir() + "/unpackedWeights.txt");
		if (Server.isLaunched())
			PriceChecker.updatePrices();
	}

	private static void printIdsForClient(String filePathName) throws FileNotFoundException, IOException {
		String content = new String(Misc.readFile(filePathName));
		List<ItemDefinition> definitions = GsonSave.gsond.fromJson(content,
				new TypeToken<List<ItemDefinition>>() {
				}.getType());

		int size = definitions.size();

		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();
		for (int index = 0; index < size; index++) {
			ItemDefinition def = definitions.get(index);
			if (def == null || !def.isTradable()) {
				continue;
			}

			sb.append(def.id + ", ");
			sb2.append(def.id + " - " + def.name);
			sb2.append("\n");
		}

		System.out.println(sb);
		System.out.println(sb2);
	}

	private static void loadFile(String filePathName) throws FileNotFoundException, IOException {
		String content = new String(Misc.readFile(filePathName));
		List<ItemDefinition> definitions = GsonSave.gsond.fromJson(content,
				new TypeToken<List<ItemDefinition>>() {
				}.getType());

		int size = definitions.size();

		System.out.println("Loaded " + size + " item definitions.");

		for (int index = 0; index < size; index++) {
			ItemDefinition def = definitions.get(index);
			if (def == null) {
				continue;
			}

			ItemDefinition.definitions.put(def.id, def);
			boolean isWeapon = def.getSlot() == 3 && def.combatStats != null;

			if (isWeapon) {
				ItemConstants.dumpCombatTabWeaponInfo(def.id, def.getName());
			}
		}
	}
	
	public static void dumpDefs() {
		try (Writer writer = new FileWriter(FOLDER_LOC+"new_item_defs.json")) {
			GsonSave.gson.toJson(definitions, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void setDefaultToNull() {
		CombatStats defaultCBStats = new CombatStats();
		defaultCBStats.setAttackSpeed(5);// it's 5 for some reason on all defaults
		
		for (int itemId = 0; itemId < Config.ITEM_LIMIT; itemId++) {
			ItemDefinition def = definitions.get(itemId);
			if (def == null) {
				//System.out.println("def is null");
				continue;
			}
			
			//set prices from item def rs cache
//			ItemDef itemDef = ItemDef.forID(itemId);
//			if (itemDef != null && itemDef.getValue() != 0) {
//				def.setDropValue(itemDef.getValue());
//			}
			
			boolean setToNull = true;
			
			if (def.requirements != null) {
				for (int r = 0; r < def.requirements.length; r++) {
					if (def.requirements[r] > 1)
						setToNull = false;
				}
			}
			if (setToNull)
				def.requirements = null;
			
			setToNull = true;//reset
			
			if (def.getMovementAnimIds() != null) {
				if (def.getMovementAnimIds()[0] != 0x328)
					setToNull = false;
				if (def.getMovementAnimIds()[1] != 0x337)
					setToNull = false;
				if (def.getMovementAnimIds()[2] != 0x333)
					setToNull = false;
				if (def.getMovementAnimIds()[3] != 0x334)
					setToNull = false;
				if (def.getMovementAnimIds()[4] != 0x335)
					setToNull = false;
				if (def.getMovementAnimIds()[5] != 0x336)
					setToNull = false;
				if (def.getMovementAnimIds()[6] != 0x338)
					setToNull = false;

				if (setToNull)
					def.movementAnimIndex = null;
			}
			
			setToNull = true;//reset
			
			if (def.combatStats == null || !def.combatStats.equalStats(defaultCBStats))
				setToNull = false;
			if (setToNull)
				def.combatStats = null;
			
		}
	}

	public static void reloadList() {
		definitions.clear();
		try {
			load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The id.
	 */
	private final int id;

	/**
	 * The name.
	 */
	private String name;

	/**
	 * The description.
	 */
	private String desc;

	/**
	 * The value.
	 */
	private int value; // inverted with drop value

	/**
	 * The value of the drop
	 */
	private int dropValue; // inverted XD with value

	/**
	 * Combat stats like gear bonuses.
	 */
	private CombatStats combatStats;

	/**
	 * The slot the item goes in.
	 */
	private byte slot;

	/**
	 * Full mask flag.
	 */
	private boolean fullmask;

	/**
	 * Stackable flag
	 */
	private boolean stackable;

	/**
	 * Notable flag
	 */
	private boolean noteable;

	/**
	 * Stackable flag
	 */
	private boolean tradable;

	/**
	 * Wearable flag
	 */
	private boolean wearable;

	/**
	 * Show beard flag
	 */
	private boolean showBeard;

	/**
	 * Members flag
	 */
	private boolean members;

	/**
	 * Two handed flag
	 */
	private boolean twoHanded;

	/**
	 * Level requirements
	 */
	private /*final*/ byte[] requirements;// = new byte[Skills.STAT_COUNT];

	/**
	 * The animation indexes of walking/running.
	 */
	private short[] movementAnimIndex;

	private SpecialsInterfaceInfo specInfo;
	
	private ChargeInfo chargeInfo;
	
	private int blockEmote;
	
	private float weight;
	
	public ItemDefinition() {
		id = -1;
		name = "";
		desc = "";
		value = 1;
		dropValue = 1;
		slot = 3;
		fullmask = false;
		stackable = false;
		tradable = true;
		wearable = false;
		showBeard = false;
		members = false;
		twoHanded = false;
		specInfo = null;
		chargeInfo = null;
		requirements = null;
		combatStats = null;// CombatStats.createDefaultStats();
	}
	
	private static final ItemDefinition DEFAULT_DEF = new ItemDefinition();
	
	public static ItemDefinition getDefault() {
		return DEFAULT_DEF;
	}
	
	private static void setupDefaultDefinition() {
		DEFAULT_DEF.combatStats = CombatStats.createDefaultStats();
		
		//default movement anims
		DEFAULT_DEF.setDefaultMovements();
		DEFAULT_DEF.requirements = new byte[Skills.STAT_COUNT];
	}
	
	private void setDefaultMovements() {
		movementAnimIndex = new short[7];
		getMovementAnimIds()[0] = 0x328; // c.playerStandIndex = 0x328;
		getMovementAnimIds()[1] = 0x337; // c.playerTurnIndex = 0x337;
		getMovementAnimIds()[2] = 0x333; // c.playerWalkIndex = 0x333;
		getMovementAnimIds()[3] = 0x334; // c.playerTurn180Index = 0x334;
		getMovementAnimIds()[4] = 0x335; // c.playerTurn90CWIndex = 0x335;
		getMovementAnimIds()[5] = 0x336; // c.playerTurn90CCWIndex = 0x336;
		getMovementAnimIds()[6] = 0x338; // c.playerRunIndex = 0x338;
	}
	
	public ItemDefinition(int ItemId, String ItemName,
			String ItemDescription, double ShopValue, double LowAlch,
			double HighAlch, int bonuses[]) {
		// first, search for a free slot
		id = ItemId;

		name = ItemName;
		desc = ItemDescription;
		value = (int) ShopValue;
		dropValue = (int) HighAlch;

		combatStats = new CombatStats();
		combatStats.setStabBonus(bonuses[0]);
		combatStats.setSlashBonus(bonuses[1]);
		combatStats.setCrushBonus(bonuses[2]);
		combatStats.setMagicAccBonus(bonuses[3]);
		combatStats.setRangedAccBonus(bonuses[4]);
		combatStats.setStabDefBonus(bonuses[5]);
		combatStats.setSlashDefBonus(bonuses[6]);
		combatStats.setCrushDefBonus(bonuses[7]);
		combatStats.setMagicDefBonus(bonuses[8]);
		combatStats.setRangedDefBonus(bonuses[9]);
		combatStats.setStrengthBonus(bonuses[10]);
		combatStats.setPrayerBonus(bonuses[11]);
		
		combatStats.setSummoning(bonuses[12]);
		
		combatStats.setRangeStrengthBonus(bonuses[16]);
		combatStats.setMagicStrengthBonus(bonuses[17]);
		
	}
	
	public void copyGoodDefs(ItemDefinition goodDefs) {
		if (getName().equals("No Name") || (getName().equals("Null") && !goodDefs.getName().equals("Null"))) {
			name = goodDefs.name;
			desc = goodDefs.desc;
		}
		
		final int goodDefsRangedStr = goodDefs.getCombatStats().getRangeStrengthBonus();
		final int prevRangedStr = getCombatStats().getRangeStrengthBonus();
		
		final boolean isNoteItem = desc.startsWith("Swap this note at any bank");
		
		boolean skipItems = this.id != 75 && this.id != 579 && this.id != 773 && this.id != 1017 && this.id != 3107 && this.id != 3795 && this.id != 3799 && this.id != 6625 && this.id != 7641 && this.id != 7807 && this.id != 10868 && this.id != 10887 && this.id != 11061 && this.id != 11785 && this.id != 11791 && this.id != 11926 && this.id != 13097 && this.id != 13904 && this.id != 13925 && this.id != 13928 && !(this.id >= 12002 && this.id <= 12006) && !(this.id >= 4972 && this.id <= 4974) && !(this.id >= 2894 && this.id <= 2942) && this.id != 5680 && this.id != 7447 && this.id != 13468 && this.id != 12924 && this.id != 12926 && this.id != 11924 && !(this.id >= 12600 && this.id <= 12607) && !(this.id >= 7453 && this.id <= 7463) && !(this.id >= 14000 && this.id <= 14024);
		
		if (skipItems && !isNoteItem && getCombatStats().equalStatsCount(goodDefs.getCombatStats()) > 1) {//if (!getCombatStats().equalStats(goodDefs.getCombatStats())) {
			if (this.id < 23000 && goodDefs.combatStats.equalStats(DEFAULT_DEF.getCombatStats()))
				System.out.println("combat stats don't match ID:"+ this.id);
		}
		

		if (skipItems)
			getCombatStats().copyStats(goodDefs.getCombatStats());
		
		if (goodDefsRangedStr != 0 && prevRangedStr == 0) {
			getCombatStats().setRangeStrengthBonus(goodDefsRangedStr);
		} else if (goodDefsRangedStr == 0 && prevRangedStr != 0){
			getCombatStats().setRangeStrengthBonus(prevRangedStr);
		}
		

		
		if (isNoteItem)
			setCombatStats(null);
	}

	/**
	 * Get the description.
	 *
	 * @return The description.
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Gets the drop value
	 *
	 * @return
	 */
	public int getDropValue() {
		return dropValue;
	}
	public void setDropValue(int value) {
		this.dropValue = value;
	}

	/**
	 * Get the id.
	 *
	 * @return The id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get the movement animation ids.
	 *
	 * @return The bonus.
	 */
	public short[] getMovementAnimIds() {
		if (movementAnimIndex == null)
			setDefaultMovements();
		return movementAnimIndex;
	}

	/**
	 * Get the name.
	 *
	 * @return The name.
	 */
	public String getName() {
		if (name != null) {
			return name;
		}
		return "No Name";
	}

	/**
	 * Get the level requirements
	 *
	 * @return
	 */
	public byte[] getRequirements() {
		if (requirements == null)
			requirements = DEFAULT_DEF.requirements;
		return requirements;
	}

	/**
	 * Gets the slot
	 *
	 * @return The slot.
	 */
	public byte getSlot() {
		return slot;
	}

	public SpecialsInterfaceInfo getSpecInfo() {
		return specInfo;
	}

	/**
	 * Get the shop value.
	 *
	 * @return The shop value.
	 */
	public int getShopValue() {
		return value;
	}

	/**
	 * Gets the fullmask flag
	 *
	 * @return The fullmask flag
	 */
	public boolean isFullmask() {
		return fullmask;
	}

	/**
	 * Is this a members item
	 *
	 * @return
	 */
	public boolean isMembers() {
		return members;
	}

	/**
	 * Can this item be noted?
	 *
	 * @return
	 */
	public boolean isNoted() {
		return noteable;
	}

	/**
	 * Is this item stackable?
	 *
	 * @return
	 */
	public boolean isStackable() {
		return stackable;
	}

	/**
	 * Is this item tradable?
	 *
	 * @return
	 */
	public boolean isTradable() {
		return tradable;
	}

	/**
	 * Is this item two handed
	 *
	 * @return
	 */
	public boolean isTwoHanded() {
		return twoHanded;
	}

	/**
	 * Can this item be equipped
	 *
	 * @return
	 */
	public boolean isWearable() {
		return wearable;
	}

	public void setMovementAnim(int index, short value) {
		if (movementAnimIndex == null) {
			setDefaultMovements();
//			if (isLoaded)
//				System.out.println("Movements are null for ItemID:" + this.id);
		}
		movementAnimIndex[index] = value;
	}

	public void setSpecInfo(SpecialsInterfaceInfo specInfo) {
		this.specInfo = specInfo;
	}

	/**
	 * Does this item show the players beard
	 *
	 * @return
	 */
	public boolean showBeard() {
		return showBeard;
	}

	public CombatStats getCombatStats() {
		if (combatStats == null) {
//			if (isLoaded) {
//				if (!foundItems.contains(this.id)) {
//					System.out.println("item ID:"+id + " have null combat stats when attempting to grab them.");
//					foundItems.add(this.id);
//				}
//			}
			return DEFAULT_DEF.combatStats;
		}
		return combatStats;
	}
	
//	private static final Set<Short> foundItems = new HashSet<>();

	public void setCombatStats(CombatStats combatStats) {
		this.combatStats = combatStats;
	}

	public int getBlockEmote() {
		return blockEmote;
	}

	public void setBlockEmote(int blockEmote) {
		this.blockEmote = blockEmote;
	}

	public ChargeInfo getChargeInfo() {
		return chargeInfo;
	}

	public void setChargeInfo(ChargeInfo chargeInfo) {
		this.chargeInfo = chargeInfo;
	}
	
	public boolean isChargeClassItem() {
		return this.chargeInfo != null;
	}
	
	public boolean isCharged() {
		if (this.chargeInfo != null && this.chargeInfo.getChargedId() == -1)
			return true;
		return false;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

}
