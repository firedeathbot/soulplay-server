package com.soulplay.game.model.item.definition;

public class ChargeInfo {

	private final int chargedId;
	private final int unchargedId;
	private final int chargeWith;
	
	public ChargeInfo(int charged, int uncharged, int chargeWith) {
		this.chargedId = charged;
		this.unchargedId = uncharged;
		this.chargeWith = chargeWith;
	}

	public int getChargedId() {
		return chargedId;
	}

	public int getUnchargedId() {
		return unchargedId;
	}

	public int getChargeWith() {
		return chargeWith;
	}
	
	public boolean isChargedItem() {
		return getChargedId() == -1;
	}
}
