package com.soulplay.game.model.item.definition;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Location;

public enum TeleTab {
	
	VARROCK(8007, Location.create(3214, 3425, 0)),
	LUMBRIDGE(8008, Location.create(3222, 3218, 0)),
	FALADOR(8009, Location.create(2965, 3378, 0)),
	CAMELOT(8010, Location.create(2757, 3477, 0)),
	ARDOUGNE(8011, Location.create(2662, 3305, 0)),
	WATCHTOWER(8012, Location.create(2549, 3112, 0)),
	HOUSE(8013, Location.create(3079, 3491, 0))
	;
	
	private final int itemId;
	private final Location loc;
	
	private TeleTab(int item, Location loc) {
		this.itemId = item;
		this.loc = loc;
	}

	public int getItemId() {
		return itemId;
	}

	public Location getLoc() {
		return loc;
	}
	
	private static Map<Integer, TeleTab> map = new HashMap<>();
	
	public static void init() {
		for (TeleTab t : TeleTab.values()) {
			map.put(t.getItemId(), t);
		}
	}
	
	public static TeleTab forItemId(int id) {
		return map.get(id);
	}

}
