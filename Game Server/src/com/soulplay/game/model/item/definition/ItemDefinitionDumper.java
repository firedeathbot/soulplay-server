package com.soulplay.game.model.item.definition;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.util.Misc;

public class ItemDefinitionDumper {
	
	private static Map<Integer, ItemDefinition> definitions = new HashMap<>();
	
	
	
	public static void newItemList(int ItemId, String ItemName,
			String ItemDescription, double ShopValue, double LowAlch,
			double HighAlch, int Bonuses[]) {
		// first, search for a free slot
		ItemDefinition def = new ItemDefinition(ItemId, ItemName, ItemDescription, ShopValue, LowAlch, HighAlch, Bonuses);
		ItemDefinition testDef = definitions.get(ItemId);
		if (testDef != null)
			System.out.println("duplicate entry on itemID:"+ItemId);
		definitions.put(ItemId, def);
		
		
		ItemDefinition realDef = ItemDefinition.forId(ItemId);
		if (realDef == null) {
//			System.out.println("null at spot:"+ItemId);
			//ItemDefinition.getDefinitions().add(ItemId, def);
			ItemDefinition.getDefinitions().put(ItemId, def);
		} else {
			if (ItemId == 21463) {
//				System.out.println("before:"+realDef.getCombatStats().toString());
//				System.out.println("dumped:"+def.getCombatStats().toString());
			}
//			realDef.copyGoodDefs(def);
			if (def.getDropValue() == 0)
				def.setDropValue(def.getShopValue());
			if (def.getDropValue() > 0)
				realDef.setDropValue(def.getDropValue());
		}
	}
	
	
	public static boolean loadItemList(String FileName) {
		String line = "";
		String token = "";
		String token2 = "";
		String token2_2 = "";
		String[] token3 = new String[10];
		boolean EndOfFile = false;
		int ReadMode = 0;
		BufferedReader characterfile = null;
		try {
			characterfile = new BufferedReader(new FileReader("./Data/cfg/"
					+ FileName));
		} catch (FileNotFoundException fileex) {
			Misc.println(FileName + ": file not found.");
			return false;
		}
		try {
			line = characterfile.readLine();
		} catch (IOException ioexception) {
			Misc.println(FileName + ": error loading file.");
			return false;
		}
		while (EndOfFile == false && line != null) {
			line = line.trim();
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.equals("item")) {
					int[] Bonuses = new int[18];
					for (int i = 0; i < 18; i++) {
						if (token3[(6 + i)] != null) {
							Bonuses[i] = Integer.parseInt(token3[(6 + i)]);
						} else {
							break;
						}
					}
					newItemList(Short.parseShort(token3[0]),
							token3[1].replaceAll("_", " "),
							token3[2].replaceAll("_", " "),
							Double.parseDouble(token3[4]),
							Double.parseDouble(token3[4]),
							Double.parseDouble(token3[6]), Bonuses);
				}
			} else {
				if (line.equals("[ENDOFITEMLIST]")) {
					try {
						characterfile.close();
					} catch (IOException ioexception) {
					}
					return true;
				}
			}
			try {
				line = characterfile.readLine();
			} catch (IOException ioexception1) {
				EndOfFile = true;
			}
		}
		try {
			characterfile.close();
		} catch (IOException ioexception) {
		}
		return false;
	}

}
