package com.soulplay.game.model.item.definition;

public final class SpecialsInterfaceInfo {

	private int frame171Subframe;

	private int specBarId;

	public SpecialsInterfaceInfo(int frame171, int specBar) {
		frame171Subframe = frame171;
		specBarId = specBar;
	}

	public int getFrame171SubFrame() {
		return frame171Subframe;
	}

	public int getSpecBarId() {
		return specBarId;
	}

}
