package com.soulplay.game.model.item.definition;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;

public enum RecoilRingEnum {

	EYE_OF_THE_RANGER(19672, 0.10, 100_000),
	EYE_OF_THE_WARRIOR(19673, 0.10, 100_000),
	EYE_OF_THE_MAGE(19674, 0.10, 100_000),
	RING_OF_RECOIL(2550, 0.10, 50) {
		@Override
		public void onEmptyAction(Player p) {
			p.getItems().deleteEquipment(123, PlayerConstants.playerRing);
			p.sendMessage("Your ring of recoil shaters!");
			p.getRecoilRings().getCharges()[ordinal()] = 0;
		}
		
		@Override
		public boolean spendCharge(Player p) {
			int currentCharge = p.getRecoilRings().getCharges()[ordinal()];
			if (currentCharge < getMaxCharges()) {
				p.getRecoilRings().getCharges()[ordinal()] = currentCharge + 1;
				if (p.getRecoilRings().getCharges()[ordinal()] == getMaxCharges())
					onEmptyAction(p);
				return true;
			}
			return false;
		}
	},
	RING_OF_SUFFERING_R(30205, 0.10, 100_000) {
		@Override
		public void onEmptyAction(Player p) {
			super.onEmptyAction(p);
			RingOfSufferingEnum ring = RingOfSufferingEnum.getByCharged(getItemId());
			if (ring != null) {
				p.getItems().replaceEquipment(PlayerConstants.playerRing, ring.getUnchargeId());
			}
		}
	},
	RING_OF_SUFFERING_RI(30258, 0.10, 100_000) {
		@Override
		public void onEmptyAction(Player p) {
			super.onEmptyAction(p);
			RingOfSufferingEnum ring = RingOfSufferingEnum.getByCharged(getItemId());
			if (ring != null) {
				p.getItems().replaceEquipment(PlayerConstants.playerRing, ring.getUnchargeId());
			}
		}
	}
	;
	
	private final int itemId;
	private final double recoilPercent;
	private final int maxCharges;
	
	private RecoilRingEnum(int itemId, double recoilPercent, int maxCharges) {
		this.itemId = itemId;
		this.recoilPercent = recoilPercent;
		this.maxCharges = maxCharges;
	}

	public int getItemId() {
		return itemId;
	}

	public int getMaxCharges() {
		return maxCharges;
	}
	
	public double getRecoilPercent() {
		return recoilPercent;
	}

	public boolean spendCharge(Player p) {
		int currentCharge = p.getRecoilRings().getCharges()[ordinal()];
		if (currentCharge > 0) {
			p.getRecoilRings().getCharges()[ordinal()] = currentCharge - 1;
			if (p.getRecoilRings().getCharges()[ordinal()] == 0)
				onEmptyAction(p);
			return true;
		}
		return false;
	}
	
	public void onEmptyAction(Player p) {
		p.sendMessage("Your ring has run out of charges!");
	}

	public static final RecoilRingEnum[] values = RecoilRingEnum.values();
	
	private static final Map<Integer, RecoilRingEnum> map = new HashMap<>();
	
	public static RecoilRingEnum get(int itemId) {
		return map.get(itemId);
	}
	
	public static void init() {
		for (RecoilRingEnum ring : values) {
			map.put(ring.getItemId(), ring);
		}
	}
	
}
