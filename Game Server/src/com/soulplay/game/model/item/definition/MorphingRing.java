package com.soulplay.game.model.item.definition;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public enum MorphingRing {

	RING_OF_STONE(6583, player -> {
		player.sendMessage("As you put on the ring you turn into a rock!");
		player.transform(2626);
	}),
	EASTER_RING(7927, player -> {
		player.sendMessage("As you put on the ring you turn into an egg!");
		player.transform(3689 + Misc.random(5));
	}),
	RING_OF_NATURE(30193, player -> player.transform(7314)),
	RING_OF_COINS(30192, player -> player.transform(7315));

	public static final Map<Integer, MorphingRing> values = new HashMap<>();
	private final int itemId;
	private final Consumer<Player> onMorph;

	private MorphingRing(int itemId, Consumer<Player> onMorph) {
		this.itemId = itemId;
		this.onMorph = onMorph;
	}

	public int getItemId() {
		return itemId;
	}

	public void morph(Player player) {
		if (!canWear(player)) {
			return;
		}

		onMorph.accept(player);
		player.getPA().closeAllWindows();
		player.getPacketSender().setSidebarInterfaceAndRemoveButtons(6014);
		player.getMovement().hardResetWalkingQueue();
		player.getMovement().setTempMovementAnim(-1);
		player.morphingRing = this;
	}

	public void unmorph(Player player) {
		player.morphingRing = null;
		player.transform(-1);
		player.getMovement().revertMovementAnims();
		player.getPA().closeAllWindows();
	}

	private boolean canWear(Player p) {
		if (p.underAttackBy > 0 || p.underAttackBy2 > 0 || p.playerIndex > 0 || p.npcIndex > 0 || p.getTransformId() != -1) {
			p.sendMessage("You cannot transform right now.");
			return false;
		}

		return true;
	}

	public static MorphingRing getRing(Player player) {
		return player.morphingRing;
	}

	public static MorphingRing getRingByItemid(int itemId) {
		return values.get(itemId);
	}

}
