package com.soulplay.game.model.item.definition;

public enum EquipmentBonuses {

	ATTACK_STAB(EquipBonusIds.ATTACK_STAB, 1675, "Stab: "),
	ATTACK_SLASH(EquipBonusIds.ATTACK_SLASH, 1676, "Slash: "),
	ATTACK_CRUSH(EquipBonusIds.ATTACK_CRUSH, 1677, "Crush: "),
	ATTACK_MAGIC(EquipBonusIds.ATTACK_MAGIC, 1678, "Magic: "),
	ATTACK_RANGED(EquipBonusIds.ATTACK_RANGED, 1679, "Ranged: "),
	DEFENSE_STAB(EquipBonusIds.DEF_STAB, 1680, "Stab: "),
	DEFENSE_SLASH(EquipBonusIds.DEF_SLASH, 1681, "Slash: "),
	DEFENSE_CRUSH(EquipBonusIds.DEF_CRUSH, 1682, "Crush: "),
	DEFENSE_MAGIC(EquipBonusIds.DEF_MAGIC, 1683, "Magic: "),
	DEFENSE_RANGED(EquipBonusIds.DEF_RANGED, 1684, "Ranged: "),
	MELEE_STR(EquipBonusIds.MELEE_STR, 1686, "Strength: "),
	RANGED_STR(EquipBonusIds.RANGED_STR, 15291, "Ranged Str: "),
	PRAYER(EquipBonusIds.PRAYER, 1687, "Prayer: "),
	MAGIC_STR(EquipBonusIds.MAGIC_STR, 15292, "Magic Bonus: "),
	SUMMONING(EquipBonusIds.SUMMONING, 0, "Summoning: "),
	
	;
	
	private final int id;
	
	private final int stringId;
	
	private final String stringStart;
	
	private EquipmentBonuses(int id, int stringId, String str) {
		this.id = id;
		this.stringId = stringId;
		this.stringStart = str;
	}

	public int getStringId() {
		return stringId;
	}
	
	public String getStartText() {
		return stringStart;
	}

	public int getId() {
		return id;
	}

	public static EquipmentBonuses[] values = EquipmentBonuses.values();
	

	
}
