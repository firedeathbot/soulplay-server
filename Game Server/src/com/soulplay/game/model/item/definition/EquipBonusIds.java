package com.soulplay.game.model.item.definition;

public class EquipBonusIds {

	public static final int ATTACK_STAB = 0;
	public static final int ATTACK_SLASH = 1;
	public static final int ATTACK_CRUSH = 2;
	public static final int ATTACK_MAGIC = 3;
	public static final int ATTACK_RANGED = 4;
	public static final int DEF_STAB = 5;
	public static final int DEF_SLASH= 6;
	public static final int DEF_CRUSH = 7;
	public static final int DEF_MAGIC = 8;
	public static final int DEF_RANGED = 9;
	public static final int MELEE_STR = 10;
	public static final int RANGED_STR = 11;
	public static final int PRAYER = 12;
	public static final int MAGIC_STR = 13;
	public static final int SUMMONING = 14;
}
