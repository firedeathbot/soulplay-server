package com.soulplay.game.model.item.definition;

public enum RingOfSufferingEnum {

	RING_OF_SUFFERING(30203, 30205),
	RING_OF_SUFFERING_I(30204, 30258);
	
	private final int unchargeId, chargedId;
	
	private RingOfSufferingEnum(int item1, int item2) {
		this.unchargeId = item1;
		this.chargedId = item2;
	}

	public int getUnchargeId() {
		return unchargeId;
	}

	public int getChargedId() {
		return chargedId;
	}
	
	private static final RingOfSufferingEnum[] values = values();
	
	public static RingOfSufferingEnum getByUncharged(int id) {
		for (int i = 0, len = values.length; i < len; i++) {
			if (values[i].getUnchargeId() == id) {
				return values[i];
			}
		}
		return null;
	}
	
	public static RingOfSufferingEnum getByCharged(int id) {
		for (int i = 0, len = values.length; i < len; i++) {
			if (values[i].getChargedId() == id) {
				return values[i];
			}
		}
		return null;
	}
}
