package com.soulplay.game.model.item;

import com.soulplay.game.model.player.Client;

public class PickedUpItem {

	String client;

	private int pickerMID;

	GroundItem item;

	private int originalOwnerMID;

	public PickedUpItem(Client client, GroundItem item) {
		this.client = client.getName();
		this.item = item;
		this.setMysqlIndex(client.mySQLIndex);
		this.setOriginalOwnerMID(item.getOriginalOwnerMID());
	}

	public String getClientName() {
		return client;
	}

	public GroundItem getItem() {
		return item;
	}

	public int getOriginalOwnerMID() {
		return originalOwnerMID;
	}

	public int getPickerMID() {
		return pickerMID;
	}

	public void setMysqlIndex(int mysqlIndex) {
		this.pickerMID = mysqlIndex;
	}

	public void setOriginalOwnerMID(int originalOwnerMID) {
		this.originalOwnerMID = originalOwnerMID;
	}
}
