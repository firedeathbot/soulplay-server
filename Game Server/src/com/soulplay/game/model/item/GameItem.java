package com.soulplay.game.model.item;

import com.soulplay.cache.ItemDef;

public class GameItem {

	private int id;

	private int amount;

	public GameItem(int id) {
		this(id, 1);
	}

	public GameItem(int id, int amount) {
		this.id = id;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public int getAmount() {
		return amount;
	}

	public boolean isStackable() {
		return ItemProjectInsanity.itemStackable[id];
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ItemDef getDefinition() {
		return ItemDef.forID(id);
	}

	public String getName() {
		return ItemDefinition.getName(id);
	}

	public GameItem copy() {
		return new GameItem(getId(), getAmount());
	}

}
