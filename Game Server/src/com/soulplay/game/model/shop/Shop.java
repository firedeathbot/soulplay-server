package com.soulplay.game.model.shop;

import com.soulplay.game.model.item.Item;

public class Shop {

    private final int id;

    private final int sellType;

    private final int buyType;

    private final String name;

    private final Item[] items;

    public Shop(int id, String name, int sellType, int buyType, Item[] items) {
        this.id = id;
        this.name = name;
        this.sellType = sellType;
        this.buyType = buyType;
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public int getSellType() {
        return sellType;
    }

    public int getBuyType() {
        return buyType;
    }

    public String getName() {
        return name;
    }

    public Item[] getItems() {
        return items;
    }
}
