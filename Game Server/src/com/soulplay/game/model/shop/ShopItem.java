package com.soulplay.game.model.shop;

import com.soulplay.game.model.item.Item;

public class ShopItem extends Item {

    private transient int currentStock;

    public ShopItem(int id, int amount, int currentStock) {
        super(id, amount);
        this.currentStock = currentStock;
    }

    public int getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(int currentStock) {
        this.currentStock = currentStock;
    }

}
