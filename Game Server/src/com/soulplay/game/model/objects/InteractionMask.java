package com.soulplay.game.model.objects;

public enum InteractionMask {

	ALL(0, 0, 0),
	NORTH(14, 0, 1),
	SOUTH(11, 0, -1),
	WEST(7, -1, 0),
	EAST(13, 1, 0);
	
	public static final InteractionMask[] values = InteractionMask.values();
	
	private final int dir;
	private final int xOff;
	private final int yOff;
	
	private InteractionMask(int dir, int xOff, int yOff) {
		this.dir = dir;
		this.xOff = xOff;
		this.yOff = yOff;
	}

	public int getDirMask() {
		return dir;
	}

	public int getxOff() {
		return xOff;
	}

	public int getyOff() {
		return yOff;
	}
	
	public static InteractionMask get(int mask) {
		for (InteractionMask dir : InteractionMask.values) {
			if (dir.getDirMask() == mask)
				return dir;
		}
		return ALL;
	}
	
	public static int getNearest(int playerX, int objectX, int objectOffset) {
		if (playerX >= objectX && playerX <= objectX + objectOffset) {
			return playerX;
		} else if (playerX > objectX + objectOffset-1) {
			return objectX + objectOffset-1;
		} else {
			return objectX;
		}
	}
	
	public static int getOppositeMask(int mask) {
		if (mask == NORTH.dir)
			return SOUTH.dir;
		if (mask == SOUTH.dir)
			return NORTH.dir;
		if (mask == WEST.dir)
			return EAST.dir;
		if (mask == EAST.dir)
			return WEST.dir;
		return ALL.dir;
	}
	
}
