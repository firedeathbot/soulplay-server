package com.soulplay.game.model.objects;

import java.util.HashMap;
import java.util.Map;

public enum DoubleDoorEnum {
	
	AL_KHARID_OPEN(SingleDoubleDoor.AL_KHARID_OPEN_LEFT, SingleDoubleDoor.AL_KHARID_OPEN_RIGHT),
	AL_KHARID_CLOSE(SingleDoubleDoor.AL_KHARID_CLOSE_LEFT, SingleDoubleDoor.AL_KHARID_CLOSE_RIGHT),
	LUMBY_OPEN(SingleDoubleDoor.LUMBY_OPEN_LEFT, SingleDoubleDoor.LUMBY_OPEN_RIGHT),
	LUMBY_CLOSE(SingleDoubleDoor.LUMBY_CLOSE_LEFT, SingleDoubleDoor.LUMBY_CLOSE_RIGHT),
	FALLY_OPEN(SingleDoubleDoor.FALLY_OPEN_NORTH, SingleDoubleDoor.FALLY_OPEN_SOUTH),
	FALLY_CLOSE(SingleDoubleDoor.FALLY_CLOSE_NORTH, SingleDoubleDoor.FALLY_CLOSE_SOUTH),
	BLACK_CASTLE_OPEN(SingleDoubleDoor.BLACK_CASTLE_OPEN_NORTH, SingleDoubleDoor.BLACK_CASTLE_OPEN_SOUTH),
	BLACK_CASTLE_CLOSE(SingleDoubleDoor.BLACK_CASTLE_CLOSE_NORTH, SingleDoubleDoor.BLACK_CASTLE_CLOSE_SOUTH),
	BLACK_CASTLE_OPEN_OSRS(SingleDoubleDoor.BLACK_CASTLE_OPEN_NORTH_OSRS, SingleDoubleDoor.BLACK_CASTLE_OPEN_SOUTH_OSRS),
	BLACK_CASTLE_CLOSE_OSRS(SingleDoubleDoor.BLACK_CASTLE_CLOSE_NORTH_OSRS, SingleDoubleDoor.BLACK_CASTLE_CLOSE_SOUTH_OSRS),
	OSRS_CHURCH_OPEN(SingleDoubleDoor.OSRS_CHURCH_OPEN_LEFT, SingleDoubleDoor.OSRS_CHURCH_OPEN_RIGHT),
	OSRS_CHURCH_CLOSE(SingleDoubleDoor.OSRS_CHURCH_CLOSE_LEFT, SingleDoubleDoor.OSRS_CHURCH_CLOSE_RIGHT),
	EDGE_OPEN(SingleDoubleDoor.EDGE_OPEN_NORTH, SingleDoubleDoor.EDGE_OPEN_SOUTH),
	EDGE_CLOSE(SingleDoubleDoor.EDGE_CLOSE_NORTH, SingleDoubleDoor.EDGE_CLOSE_SOUTH),
	
	;
	
	private final SingleDoubleDoor northDoor, southDoor;

	private DoubleDoorEnum(SingleDoubleDoor leftDoor, SingleDoubleDoor rightDoor) {
		this.northDoor = leftDoor;
		this.southDoor = rightDoor;
	}

	public SingleDoubleDoor getNorthDoor() {
		return northDoor;
	}

	public SingleDoubleDoor getSouthDoor() {
		return southDoor;
	}
	
	private static Map<Integer, DoubleDoorEnum> map = new HashMap<>();
	
	public static DoubleDoorEnum get(int objectId) {
		return map.get(objectId);
	}
	
	public static void init() {
		for (DoubleDoorEnum doors : DoubleDoorEnum.values()) {
			map.put(doors.getNorthDoor().getObjectId(), doors);
			map.put(doors.getSouthDoor().getObjectId(), doors);
		}
	}
	
}
