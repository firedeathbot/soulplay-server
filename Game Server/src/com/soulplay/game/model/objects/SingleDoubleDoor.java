package com.soulplay.game.model.objects;

import com.soulplay.game.world.entity.Location;

public enum SingleDoubleDoor {

	AL_KHARID_OPEN_LEFT(1506, Location.create(0, -1), true, Location.create(-1, 0), 1507, 1),
	AL_KHARID_OPEN_RIGHT(1508, Location.create(0, 1), false, Location.create(-1, 0), 1511, 3),
	AL_KHARID_CLOSE_LEFT(1507, Location.create(1, 0), true, Location.create(0, 1), 1506, 3),
	AL_KHARID_CLOSE_RIGHT(1511, Location.create(1, 0), false, Location.create(0, -1), 1508, 1),
	
	LUMBY_OPEN_LEFT(1519, Location.create(0, -1), true, Location.create(-1, 0), 1520, 1),
	LUMBY_OPEN_RIGHT(1516, Location.create(0, 1), false, Location.create(-1, 0), 1517, 3),
	LUMBY_CLOSE_LEFT(1520, Location.create(1, 0), true, Location.create(0, 1), 1519, 3),
	LUMBY_CLOSE_RIGHT(1517, Location.create(1, 0), false, Location.create(0, -1), 1516, 1),

	FALLY_OPEN_NORTH(11721, Location.create(0, -1), true, Location.create(-1, 0), 11723, 1),
	FALLY_OPEN_SOUTH(11716, Location.create(0, 1), false, Location.create(-1, 0), 11722, 3),
	FALLY_CLOSE_NORTH(11723, Location.create(1, 0), true, Location.create(0, 1), 11721, 3),
	FALLY_CLOSE_SOUTH(11722, Location.create(1, 0), false, Location.create(0, -1), 11716, 1),
	
	BLACK_CASTLE_OPEN_NORTH(14752, Location.create(0, -1), true, Location.create(-1, 0), 14754, 1),
	BLACK_CASTLE_OPEN_SOUTH(14751, Location.create(0, 1), false, Location.create(-1, 0), 14753, 3),
	BLACK_CASTLE_CLOSE_NORTH(14754, Location.create(1, 0), true, Location.create(0, 1), 14721, 3),
	BLACK_CASTLE_CLOSE_SOUTH(14753, Location.create(1, 0), false, Location.create(0, -1), 14751, 1),
	
	BLACK_CASTLE_OPEN_NORTH_OSRS(114752, Location.create(0, -1), true, Location.create(-1, 0), 114754, 1),
	BLACK_CASTLE_OPEN_SOUTH_OSRS(114751, Location.create(0, 1), false, Location.create(-1, 0), 114753, 3),
	BLACK_CASTLE_CLOSE_NORTH_OSRS(114754, Location.create(1, 0), true, Location.create(0, 1), 114721, 3),
	BLACK_CASTLE_CLOSE_SOUTH_OSRS(114753, Location.create(1, 0), false, Location.create(0, -1), 114751, 1),
	
	OSRS_CHURCH_OPEN_LEFT(101524, Location.create(0, -1), true, Location.create(-1, 0), 101525, 1),
	OSRS_CHURCH_OPEN_RIGHT(101521, Location.create(0, 1), false, Location.create(-1, 0), 101522, 3),
	OSRS_CHURCH_CLOSE_LEFT(101525, Location.create(1, 0), true, Location.create(0, 1), 101524, 3),
	OSRS_CHURCH_CLOSE_RIGHT(101522, Location.create(1, 0), false, Location.create(0, -1), 101521, 1),
	
	EDGE_OPEN_NORTH(26908, Location.create(0, -1), true, Location.create(-1, 0), 26909, 1),
	EDGE_OPEN_SOUTH(26906, Location.create(0, 1), false, Location.create(-1, 0), 26907, 3),
	EDGE_CLOSE_NORTH(26909, Location.create(1, 0), true, Location.create(0, 1), 26908, 3),
	EDGE_CLOSE_SOUTH(26907, Location.create(1, 0), false, Location.create(0, -1), 26906, 1),
	
	
	;
	
	private final int objectId, tranformId, orientationOff;
	
	private final Location otherLocOffset, move;
	
	/**
	 * is north door when orientation is 0
	 */
	private final boolean isNorthDoor;
	
	private SingleDoubleDoor(int objectId, Location otherDoorLoc, boolean northSide, Location moveLoc, int transformId, int face) {
		this.objectId = objectId;
		this.otherLocOffset = otherDoorLoc;
		this.isNorthDoor = northSide;
		this.move = moveLoc;
		this.tranformId = transformId;
		this.orientationOff = face;
	}

	public int getObjectId() {
		return objectId;
	}

	/**
	 * is north door when orientation is 0
	 */
	public boolean isNorthDoor() {
		return isNorthDoor;
	}

	public int getTranformId() {
		return tranformId;
	}
	
	public Location getOtherLoc(int orientation) {
		switch (orientation) {
		case 0:
			return otherLocOffset;
		case 1:
			return Location.create(otherLocOffset.getY(), otherLocOffset.getX()*-1);
		case 2:
			return Location.create(otherLocOffset.getX()*-1, otherLocOffset.getY()*-1);
		case 3:
			return Location.create(otherLocOffset.getY()*-1, otherLocOffset.getX());
		}
		return otherLocOffset;
	}
	
	public Location getMoveOffset(int orientation) {
		switch (orientation) {
		case 0:
			return move;
		case 1:
			return Location.create(move.getY(), move.getX()*-1);
		case 2:
			return Location.create(move.getX()*-1, move.getY()*-1);
		case 3:
			return Location.create(move.getY()*-1, move.getX());
		}
		return move;
	}

	public int getTransformOrientationOff() {
		return orientationOff;
	}
}
