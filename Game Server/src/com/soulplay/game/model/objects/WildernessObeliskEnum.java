package com.soulplay.game.model.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.game.world.entity.Location;

public enum WildernessObeliskEnum {

	OB1(/*14826*/114826, "Level 43 Wilderness", 2978, 3864),
	OB2(/*14827*/114827, "Level 26 Wilderness", 3033, 3730),
	OB3(/*14828*/114828, "Level 34 Wilderness", 3104, 3792),
	OB4(/*14829*/114829, "Level 12 Wilderness", 3154, 3618),
	OB5(/*14830*/114830, "Level 18 Wilderness", 3225, 3665),
	OB6(/*14831*/114831, "Level 49 Wilderness", 3305, 3914)
	;
	
	private final int id;
	private final Location loc;
	private final Location center;
	private final String text;
	
	private WildernessObeliskEnum(int id, String text, int... loc) {
		this.id = id;
		this.text = text;
		this.loc = Location.create(loc[0], loc[1], 0);
		this.center = Location.create(loc[0] + 2, loc[1] + 2, 0);
	}
	
	public Location getCornerLoc() {
		return loc;
	}
	
	public int getId() {
		return id;
	}

	public Location getCenter() {
		return center;
	}
	
	public String getText() {
		return this.text;
	}
	
	public static final WildernessObeliskEnum[] values = WildernessObeliskEnum.values();
	
	public static final WildernessObeliskEnum[] ORDER_VALUES = new WildernessObeliskEnum[] { OB4, OB5, OB2, OB3, OB1, OB6 };

	private static final Map<Integer, WildernessObeliskEnum> map = new HashMap<>();
	
	public static void init() {
		for (WildernessObeliskEnum ob : WildernessObeliskEnum.values) {
			map.put(ob.id, ob);
			randomList.add(ob);
		}
	}
	
	public static WildernessObeliskEnum get(int objId) {
		return map.get(objId);
	}
	
	private static final List<WildernessObeliskEnum> randomList = new ArrayList<>(values.length);
	
	public static void randomizeObelisks() {
		Collections.shuffle(randomList);
	}
	
	public static WildernessObeliskEnum getNextStep(int currentIndex) {
		int indexLocation = 0;
		for (int i = 0, len = randomList.size(); i < len; i++) {
			if (randomList.get(i).ordinal() == currentIndex) {
				indexLocation = i;
				break;
			}
		}
		indexLocation = (indexLocation+1) % values.length;
		return randomList.get(indexLocation);
	}
	
}
