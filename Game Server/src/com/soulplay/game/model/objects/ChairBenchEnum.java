package com.soulplay.game.model.objects;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

public enum ChairBenchEnum {
	CRUDE_CHAIR(13581, 4073, 4103),
	WOODEN_CHAIR(13582, 4075, 4103),
	ROCKING_CHAIR(13583, 4079, 4103),
	OAK_CHAIR(13584, 4081, 4103),
	OAK_ARMCHAIR(13585, 4083, 4103),
	TEAK_ARMCHAIR(13586, 4085, 4103),
	MAHOGANY_ARMCHAIR(13587, 4087, 4103),
	BENCH_WOODEN(13300, 4089, 4104),
	BENCH_OAK(13301, 4091, 4104),
	BENCH_CARVED_OAK(13302, 4093, 4104),
	BENCH_TEAK(13303, 4095, 4104),
	BENCH_CARVED_TEAK(13304, 4097, 4104),
	BENCH_MAHOGANY(13305, 4099, 4104),
	BENCH_GILDED(13306, 4101, 4104),
	CARVED_TEAK_BENCH(13694, 4097, 4104),
	MAHOGANY_BENCH(13695, 4099, 4104),
	GILDED_BENCH(13696, 4101, 4104),
	OAK_THRONE(13665, 4111, 4103),
	TEAK_THRONE(13666, 4112, 4103),
	MAHOGANY_THRONE(13667, 4113, 4103),
	GILDED_THRONE(13668, 4114, 4103),
	SKELETON_THRONE(13669, 4115, 4103),
	CRYSTAL_THRONE(13670, 4116, 4103),
	DEMONIC_THRONE(13671, 4117, 4103)
	;
	
	
	private final int objectId;
	private final Animation startAnim;
	private final Animation sitAnim;
	
	private ChairBenchEnum(int objectId, int sittingAnim, int startAnimation) {
		this.objectId = objectId;
		this.startAnim = new Animation(startAnimation);
		this.sitAnim = new Animation(sittingAnim);
	}

	public int getObjectId() {
		return objectId;
	}

	public Animation getStartAnim() {
		return startAnim;
	}

	public Animation getSitAnim() {
		return sitAnim;
	}
	
	private static final Map<Integer, ChairBenchEnum> map = new HashMap<>();
	
	public static void init() {
		for (ChairBenchEnum o : ChairBenchEnum.values()) {
			map.put(o.getObjectId(), o);
		}
	}
	
	public static ChairBenchEnum get(int objectId) {
		return map.get(objectId);
	}
	
}
