package com.soulplay.game.model.objects;

public class CustomObject {

	public int objectId;

	public int objectX;

	public int objectY;

	public int height;

	public int face;

	public int type;

	public CustomObject(int id, int x, int y, int height, int face, int type) {
		this.objectId = id;
		this.objectX = x;
		this.objectY = y;
		this.height = height;
		this.face = face;
		this.type = type;
	}

	public int getFace() {
		return face;
	}

	public int getHeight() {
		return height;
	}

	public int getId() {
		return objectId;
	}

	public int getType() {
		return type;
	}

	public int getX() {
		return objectX;
	}

	public int getY() {
		return objectY;
	}

}
