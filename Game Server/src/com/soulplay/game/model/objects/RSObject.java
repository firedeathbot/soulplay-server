package com.soulplay.game.model.objects;

import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.GlobalPackets;

public class RSObject {

	private int newId;

	private int objectX;

	private int objectY;

	private int height;

	private int face;

	private int type;

	private int originalId;

	private int originalX;

	private int originalY;

	private int originalFace;

	private int tick;

	private int objectClass; // objectClass: 0 = default objects, 1 = castlewars objects

	private long objectOwnerLong;
	
	private boolean isActive;
	
	private boolean updateRequired;
	
	private Location linkedLoc;

	public RSObject(int id, Location location, int face, int type) {
		this(id, location.getX(), location.getY(), location.getZ(), face, type);
	}

	public RSObject(int newId, int x, int y, int height, int face, int type) { // for
																				// objects
																				// like
																				// custom
																				// objects
																				// which
																				// are
																				// permanent
		this.newId = newId;
		this.objectX = x;
		this.objectY = y;
		this.height = height;
		this.face = face & 3;
		this.type = type;
		this.isActive = true;
		this.updateRequired = true;
	}

	public RSObject(int newId, int x, int y, int height, int face, int type,
			int originalId, int ticks) {
		this.newId = newId;
		this.objectX = x;
		this.objectY = y;
		this.height = height;
		this.face = face & 3;
		this.type = type;
		this.originalId = originalId;
		this.tick = ticks;
		this.isActive = true;
		this.updateRequired = true;
	}

	public static RSObject createDoor(int newId, int x, int y, int height, int face, int type,
			int originalId, int originalX, int originalY, int ticks) {
		RSObject door = new RSObject(newId, x, y, height, face, type, originalId, ticks);
		door.originalX = originalX;
		door.originalY = originalY;
		return door;
	}

	public void changeObject(int id) {
		this.originalId = id;
		this.newId = id;
		ObjectManager.updateObject(this);
	}

	public int getFace() {
		return face;
	}

	public int getHeight() {
		return height;
	}

	public int getNewObjectId() {
		return newId;
	}


	/**
	 * mostly for objects that are also moved from one spot to another
	 *
	 * @param objectClass  - 0 = default objects, 1 = castlewars objects, 3 = pest control
	 */
	public int getObjectClass() {
		return objectClass;
	}

	public long getObjectOwnerLong() {
		return objectOwnerLong;
	}

	public int getOriginalFace() {
		return originalFace;
	}

	public int getOriginalId() {
		return originalId;
	}

	public int getOriginalX() {
		return originalX;
	}

	public int getOriginalY() {
		return originalY;
	}

	public int getTick() {
		return tick;
	}

	public int getType() {
		return type;
	}

	public int getX() {
		return objectX;
	}

	public int getY() {
		return objectY;
	}

	public void setFace(int face) {
		this.face = face;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setNewObjectId(int objectId) {
		this.newId = objectId;
	}

	/**
	 * mostly for objects that are also moved from one spot to another
	 *
	 * @param objectClass  - 0 = default objects, 1 = castlewars objects, 3 = pest control, 4 = LMS
	 */
	public void setObjectClass(int objectClass) {
		this.objectClass = objectClass;
	}

	public void setObjectOwnerLong(long objectOwnerIndex) {
		this.objectOwnerLong = objectOwnerIndex;
	}

	public void setObjectX(int objectX) {
		this.objectX = objectX;
	}

	public void setObjectY(int objectY) {
		this.objectY = objectY;
	}

	public void setOriginalFace(int originalFace) {
		this.originalFace = originalFace;
	}

	public void setOriginalId(int newId) {
		this.originalId = newId;
	}

	public void setOriginalX(int originalX) {
		this.originalX = originalX;
	}

	public void setOriginalY(int originalY) {
		this.originalY = originalY;
	}

	public void setTick(int tick) {
		this.tick = tick;
		if (this.tick == 0)
			this.isActive = false;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Location getLinkedLoc() {
		return linkedLoc;
	}

	public void setLinkedLoc(Location linkedLoc) {
		this.linkedLoc = linkedLoc;
	}
	
	public void process() {
	}

	public boolean isUpdateRequired() {
		return updateRequired;
	}

	public void setUpdateRequired(boolean updateRequired) {
		this.updateRequired = updateRequired;
	}

	public void animate(int animId) {
		GlobalPackets.objectAnim(getX(), getY(), animId, getType(), getFace(), getHeight());
	}
	
}
