package com.soulplay.game.model.objects;

public enum RevenantCaveEntrance {

	SOUTH_EXIT(9595, 10056),
	SOUTH_ENTER(3074, 3654),
	NORTH_ENTER(3125, 3833),
	NORTH_EXIT(9641, 10235);
	
	private final int x, y;
	
	private RevenantCaveEntrance(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
}
