package com.soulplay.game.model.region;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NpcUpdating;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectSet;

/**
 * Manages the world regions.
 *
 * @author Graham Edgecombe
 */
public class RegionManager {

	/**
	 * The region size.
	 */
	public static final int REGION_SIZE = 32;

	/**
	 * The lower bound that splits the region in half.
	 */
	// private static final int LOWER_BOUND = REGION_SIZE / 2 - 1; //unused, so
	// removed by alk

	/**
	 * The active (loaded) region map.
	 */
	private final Long2ObjectMap<Region> activeRegions = new Long2ObjectOpenHashMap<>();

	public void checkEmptyRegion(Region r) {
		if (r.isEmptyRegion() && r.getDynamicData() == null) {
			removeEmptyRegion(r);
		}
		// System.out.println("regions: "+activeRegions.size());
	}

	public void cleanRegions() {
		ObjectSet<Entry<Region>> set = activeRegions.long2ObjectEntrySet();
		ObjectIterator<Entry<Region>> itr = set.iterator();
		while (itr.hasNext()) {
			Region r = itr.next().getValue();
			if (r.isEmptyRegion()) {
				activeRegions.remove(r.getCoordinates());
				continue;
			}
			if (!r.getPlayers().isEmpty()) {
				Iterator<Player> itr2 = r.getPlayers().iterator();
				while (itr2.hasNext()) {
					Player player = itr2.next();
					if (player != null && player.forceDisconnect) {
						player.destroy();
					}
				}
			}
		}
	}

	private Region getActiveRegion(int x, int y, int z) {
		return getActiveRegion(toBitpack(x, y, z));
	}

	private Region getActiveRegion(long key) {
		return activeRegions.get(key);
	}

	private static final List<GroundItem> EMPTY_LIST = new LinkedList<>();
	
	public Collection<GroundItem> getGroundItemsInOneRegion(int x, int y, int z) {
		Region region = getActiveRegion(x/REGION_SIZE, y/REGION_SIZE, z);
		if (region != null) {
			return region.getGroundItems();
		}
		return Collections.unmodifiableCollection(EMPTY_LIST);
	}

	/**
	 * Gets the local GroundItems around an entity.
	 *
	 * @param entity
	 *            The entity.
	 * @return The collection of local GroundItems.
	 */
	public Collection<GroundItem> getRenderedGroundItemsByZ(Player entity) {
		List<GroundItem> localGroundItems = new LinkedList<>();
		Region[] regions = getSurroundingRegionsExtended(entity.getX(),
				entity.getY(), entity.getHeightLevel());
		for (Region region : regions) {
			if (region != null) {
				for (GroundItem item : region.getGroundItems()) {
					localGroundItems.add(item);
				}
			}
		}
		return Collections.unmodifiableCollection(localGroundItems);
	}

	public Collection<NPC> getLocalNpcs(NPC entity) {
		List<NPC> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getX(), entity.getY(), entity.getHeightLevel());
		for (Region region : regions) {
			if (region != null) {
				for (NPC npc : region.getNpcs()) {
					if (entity.getCurrentLocation().isWithinViewDistance(npc.getCurrentLocation())) {
						// if(npc.goodDistance(npc.getX(), npc.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(npc);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}
	
	public Collection<NPC> getLocalNpcs(Mob entity) {
		List<NPC> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getCurrentLocation().getX(), entity.getCurrentLocation().getY(), entity.getCurrentLocation().getZ());
		for (Region region : regions) {
			if (region != null) {
				for (NPC npc : region.getNpcs()) {
					if (entity.getCurrentLocation().isWithinViewDistance(npc.getCurrentLocation())) {
						// if(npc.goodDistance(npc.getX(), npc.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(npc);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}
	
	public Collection<Mob> getLocalMobs(Mob entity, boolean grabNpcs, boolean random) {
		List<Mob> localEntities = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getCurrentLocation().getX(), entity.getCurrentLocation().getY(), entity.getCurrentLocation().getZ());
		for (Region region : regions) {
			if (region != null) {
				for (Mob mob : (grabNpcs ? region.getNpcs() : region.getPlayers())) {
					if (entity.getCurrentLocation().isWithinViewDistance(mob.getCurrentLocation())) {
						localEntities.add(mob);
					}
				}
			}
		}
		if (random)
			Collections.shuffle(localEntities);
		return Collections.unmodifiableCollection(localEntities);
	}

	/**
	 * Gets the local NPCs around an entity.
	 *
	 * @param entity
	 *            The entity.
	 * @return The collection of local NPCs.
	 */
	public Collection<NPC> getLocalNpcs(Player entity) {
		List<NPC> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getX(), entity.getY(), entity.getHeightLevel());
		for (Region region : regions) {
			if (region != null) {
				for (NPC npc : region.getNpcs()) {
					if (entity.getCurrentLocation().isWithinViewDistance(npc.getCurrentLocation())) {
						// if(npc.goodDistance(npc.getX(), npc.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(npc);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}

	public Collection<NPC> getLocalNpcsRendering(Player player) {
		List<NPC> localNpcs = new LinkedList<>();
		Region[] regions;
		if (player.extendedNpcRendering) {
			regions = getSurroundingRegionsExtended(player.getX(), player.getY(), player.getHeightLevel());
		} else {
			regions = getSurroundingRegions(player.getX(), player.getY(), player.getHeightLevel());
		}

		for (int i = 0, length = regions.length; i < length; i++) {
			Region region = regions[i];
			if (region == null) {
				continue;
			}

			for (NPC npc : region.getNpcs()) {
				if (!NpcUpdating.withinDistance(player, npc)) {
					continue;
				}

				localNpcs.add(npc);
			}
		}

		return localNpcs;
	}

	public Collection<NPC> getLocalNpcsNext(Player entity) {
		List<NPC> npcs = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getX(), entity.getY(), entity.getHeightLevel());
		for (int i = 0, length = regions.length; i < length; i++) {
			Region region = regions[i];
			if (region == null) {
				continue;
			}

			for (NPC npc : region.getNpcs()) {
				if (!npc.withinDistance2(entity.getX(), entity.getY())) {
					continue;
				}

				npcs.add(npc);
			}
		}

		return npcs;
	}

	public Collection<NPC> getLocalNpcsByLocation(int x, int y, int z) {
		List<NPC> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(x, y, z);
		for (Region region : regions) {
			if (region != null) {
				for (NPC npc : region.getNpcs()) {
					if (npc.withinDistance(x, y)) {
						// if(npc.goodDistance(npc.getX(), npc.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(npc);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}
	
	public Collection<Player> getLocalPlayers(Mob entity) {
		List<Player> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getCurrentLocation().getX(), entity.getCurrentLocation().getY(), entity.getCurrentLocation().getZ());
		for (Region region : regions) {
			if (region != null) {
				for (Player player : region.getPlayers()) {
					if (entity.getCurrentLocation().isWithinViewDistance(player.getCurrentLocation())) {
						localPlayers.add(player);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}
	
	private static final ObjectList<Player> localPlayersCached = new ObjectArrayList<>(2000);
	
	public ObjectList<Player> getLocalPlayersOptimized(final NPC entity) {
		localPlayersCached.clear();
		final Region[] regions = getSurroundingRegions(entity.getX(), entity.getY(), entity.getZ());
		for (Region region : regions) {
			if (region != null) {
				final LinkedList<Player> players = region.getPlayersDirect();
				for (int i = 0, length = players.size(); i < length; i++) {
					final Player player = players.get(i);
					if (entity.getCurrentLocation().isWithinViewDistance(player.getCurrentLocation())) {
						// if(player.goodDistance(player.getX(), player.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayersCached.add(player);
					}
				}
			}
		}
		return localPlayersCached;
	}

	public Collection<Player> getLocalPlayers(NPC entity) {
		List<Player> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getX(), entity.getY(), entity.getZ());
		for (Region region : regions) {
			if (region != null) {
				for (Player player : region.getPlayers()) {
					if (entity.getCurrentLocation().isWithinViewDistance(player.getCurrentLocation())) {
						// if(player.goodDistance(player.getX(), player.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(player);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}

	/**
	 * Gets the local players around an entity.
	 *
	 * @param entity
	 *            The entity.
	 * @return The collection of local players.
	 */
	public Collection<Player> getLocalPlayers(Player entity) {
		List<Player> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(entity.getX(), entity.getY(), entity.getZ());
		for (Region region : regions) {
			if (region != null) {
				for (Player player : region.getPlayers()) {
					if (entity.getCurrentLocation().isWithinViewDistance(player.getCurrentLocation())) {
						// if(player.goodDistance(player.getX(), player.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(player);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}

	public Collection<Player> getLocalPlayersByLocation(Location loc) {
		return getLocalPlayersByLocation(loc.getX(), loc.getY(), loc.getZ());
	}
	
	public Collection<Player> getLocalPlayersByLocation(int x, int y,
			int height) {
		List<Player> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegions(x, y, height);
		for (Region region : regions) {
			if (region != null) {
				for (Player player : region.getPlayers()) {
					if (player.withinDistance(x, y)) {
						// if(player.goodDistance(player.getX(), player.getY(),
						// entity.getX(), entity.getY(), 15)) {
						localPlayers.add(player);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}
	
	public Collection<Player> getLocalPlayersByLocationExt(Location loc) {
		return getLocalPlayersByLocation(loc.getX(), loc.getY(), loc.getZ());
	}
	
	public Collection<Player> getLocalPlayersByLocationExt(int x, int y,
			int height) {
		List<Player> localPlayers = new LinkedList<>();
		Region[] regions = getSurroundingRegionsExtended(x, y, height);
		for (Region region : regions) {
			if (region != null) {
				for (Player player : region.getPlayers()) {
					localPlayers.add(player);
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}

	public Collection<Player> getPlayersForRenderedRegionsAllZ(int x, int y,
			int height) {
		List<Player> localPlayers = new LinkedList<>();
		int startZ = height - (height & 3);
		for (int i = 0; i < 4; i++) {
			Region[] regions = getSurroundingRegionsExtended(x, y, startZ+i);
			for (Region region : regions) {
				if (region == null) {
					continue;
				}

				for (Player player : region.getPlayers()) {
					localPlayers.add(player);
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}
	
	public Collection<NPC> getNpcsInRegion(Region r) {
		List<NPC> regionNpcs = new LinkedList<>();
		for (NPC npc : r.getNpcs()) {
			regionNpcs.add(npc);
		}
		return Collections.unmodifiableCollection(regionNpcs);
	}

	/**
	 * Gets a region by its x and y coordinates.
	 *
	 * @param x
	 *            The x coordinate.
	 * @param y
	 *            The y coordinate.
	 * @return The region.
	 */
	private Region getRegion(int x, int y, int z) {
		long key = toBitpack(x, y, z);
		Region region = getActiveRegion(key);
		if (region != null) {
			return region;
		}

		region = new Region(key);
		activeRegions.put(key, region);
		return region;
	}

	/**
	 * Gets a region by location. This method creates active region.
	 *
	 * @param location
	 *            The location.
	 * @return The region.
	 */
	public Region getRegionByLocation(int x, int y, int z) {
		return getRegion(x / REGION_SIZE, y / REGION_SIZE, z);
	}

	public Region getRegionByLocation(Location location) {
		return getRegion(location.getX() / REGION_SIZE, location.getY() / REGION_SIZE, location.getZ());
	}

	/**
	 * Gets the regions surrounding a location.
	 *
	 * @param location
	 *            The location.
	 * @return The regions surrounding the location.
	 */
	public Region[] getSurroundingRegions(int x, int y, int z) {
		int regionX = x / REGION_SIZE;
		int regionY = y / REGION_SIZE;

		int regionPositionX = x % REGION_SIZE;
		int regionPositionY = y % REGION_SIZE;

		Region[] surrounding = new Region[4];
		surrounding[0] = getActiveRegion(regionX, regionY, z);

		/*
		 * if(regionPositionX <= LOWER_BOUND) { if(regionPositionY <=
		 * LOWER_BOUND) { surrounding[1] = getRegion(regionX - 1, regionY - 1);
		 * surrounding[2] = getRegion(regionX - 1, regionY); surrounding[3] =
		 * getRegion(regionX, regionY - 1); } else { surrounding[1] =
		 * getRegion(regionX + 1, regionY - 1); surrounding[2] =
		 * getRegion(regionX + 1, regionY); surrounding[3] = getRegion(regionX,
		 * regionY - 1); } } else { if(regionPositionY <= LOWER_BOUND) {
		 * surrounding[1] = getRegion(regionX - 1, regionY + 1); surrounding[2]
		 * = getRegion(regionX - 1, regionY); surrounding[3] =
		 * getRegion(regionX, regionY + 1); } else { surrounding[1] =
		 * getRegion(regionX + 1, regionY + 1); surrounding[2] =
		 * getRegion(regionX + 1, regionY); surrounding[3] = getRegion(regionX,
		 * regionY + 1); } }
		 */

		if (regionPositionY < 15) {
			surrounding[1] = getActiveRegion(regionX, regionY - 1, z);
		} else if (regionPositionY > 15) {
			surrounding[1] = getActiveRegion(regionX, regionY + 1, z);
		}

		if (regionPositionX < 15) {
			surrounding[2] = getActiveRegion(regionX - 1, regionY, z);
		} else if (regionPositionX > 15) {
			surrounding[2] = getActiveRegion(regionX + 1, regionY, z);
		}

		if (regionPositionX > 15 && regionPositionY > 15) {
			surrounding[3] = getActiveRegion(regionX + 1, regionY + 1, z);
		} else if (regionPositionX < 15 && regionPositionY < 15) {
			surrounding[3] = getActiveRegion(regionX - 1, regionY - 1, z);
		}

		return surrounding;
	}

	/**
	 * Only used for grabbing existing items in regions. Not for use in adding
	 * items to regions. Using a wider range of regions, not just the 4 nearest
	 * blocks but 9 blocks all around the current region reloading ground items
	 * at.
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public Region[] getSurroundingRegionsExtended(int x, int y, int z) {
		int regionX = x / REGION_SIZE;
		int regionY = y / REGION_SIZE;

		Region[] surrounding = new Region[9];
		surrounding[0] = getActiveRegion(regionX, regionY, z);
		surrounding[1] = getActiveRegion(regionX, regionY - 1, z);
		surrounding[2] = getActiveRegion(regionX, regionY + 1, z);
		surrounding[3] = getActiveRegion(regionX - 1, regionY, z);
		surrounding[4] = getActiveRegion(regionX + 1, regionY, z);
		surrounding[5] = getActiveRegion(regionX + 1, regionY + 1, z);
		surrounding[6] = getActiveRegion(regionX - 1, regionY - 1, z);
		surrounding[7] = getActiveRegion(regionX + 1, regionY - 1, z);
		surrounding[8] = getActiveRegion(regionX - 1, regionY + 1, z);

		return surrounding;
	}

	public void playersOnlineUsingRegionCheck() {
		ObjectSet<Entry<Region>> set = activeRegions.long2ObjectEntrySet();
		ObjectIterator<Entry<Region>> itr = set.iterator();
		int count = 0;
		while (itr.hasNext()) {
			Region r = itr.next().getValue();
			if (!r.getPlayers().isEmpty()) {
				count += r.getPlayers().size();
			}

		}
		System.out.println("Count : " + count);
	}

	public void removeEmptyRegion(Region r) {
		activeRegions.remove(r.getCoordinates());
	}

	public static long toBitpack(int x, int y, int z) {
		return (long) (x + (y << 14) + ((long) z << 28));
	}
	
	public static Location unpackLocation(long bitpacked) {
		return Location.create((int)(bitpacked & 0x3FF) * REGION_SIZE, (int)((bitpacked >> 14) & 0x3FF) * REGION_SIZE, (int)(bitpacked >> 28) * REGION_SIZE);
	}

	/**
	 * TESTING
	 */
	/*
	 * public Region[] getSurroundingRegions2(int x, int y) { int regionX = x /
	 * REGION_SIZE; int regionY = y / REGION_SIZE; int regionPositionX = x %
	 * REGION_SIZE; int regionPositionY = y % REGION_SIZE; Region[] surrounding
	 * = new Region[4]; surrounding[0] = getRegion(regionX, regionY); if
	 * (regionPositionY < 15) { surrounding[1] = getRegion(regionX, regionY-1);
	 * } else if(regionPositionY > 15) { surrounding[1] = getRegion(regionX,
	 * regionY+1); } if (regionPositionX < 15) { surrounding[2] =
	 * getRegion(regionX-1, regionY); } else if (regionPositionX > 15) {
	 * surrounding[2] = getRegion(regionX+1, regionY); } if (regionPositionX >
	 * 15 && regionPositionY > 15) { surrounding[3] = getRegion(regionX+1,
	 * regionY+1); } else if (regionPositionX < 15 && regionPositionY < 15) {
	 * surrounding[3] = getRegion(regionX-1, regionY-1); } return surrounding; }
	 */

}
