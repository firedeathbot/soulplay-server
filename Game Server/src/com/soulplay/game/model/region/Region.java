package com.soulplay.game.model.region;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedDeque;

import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;

/**
 * Represents a single region.
 *
 * @author Graham Edgecombe
 */
public class Region {

	/**
	 * The region coordinates.
	 */
	private long coordinate;

	/**
	 * A list of players in this region.
	 */
	private LinkedList<Player> players = new LinkedList<>();

	/**
	 * A list of NPCs in this region.
	 */
	private Deque<NPC> npcs = new ArrayDeque<>(128);

	/**
	 * A list of NPCs in this region.
	 */
	private Deque<GroundItem> items = new ConcurrentLinkedDeque<>();

	/**
	 * A list of objects in this region.
	 */
	private Deque<RSObject> objects = new ArrayDeque<>(32);
	
	public boolean isEmptyRegion() {
		return players.isEmpty() && npcs.isEmpty() && items.isEmpty() && objects.isEmpty();
	}

	private int[][][][] dynamicData;
	
	/**
	 * Creates a region.
	 *
	 * @param coordinate
	 *            The coordinate.
	 */
	public Region(long coordinate) {
		this.coordinate = coordinate;
	}

	/**
	 * Adds a new GroundItem.
	 *
	 * @param item
	 *            The GroundItem to add.
	 */
	public void addGroundItem(GroundItem item) {
		// synchronized(this) {
		items.add(item);
		// }
	}

	/**
	 * Adds a new NPC.
	 *
	 * @param npc
	 *            The NPC to add.
	 */
	public void addNpc(NPC npc) {
		// synchronized(this) {
		npcs.add(npc);
		// }
	}

	/**
	 * Adds a new player.
	 *
	 * @param player
	 *            The player to add.
	 */
	public void addPlayer(Player player) {
		// synchronized(this) {
		players.add(player);
		// }
	}

	/**
	 * Gets the region coordinates.
	 *
	 * @return The region coordinates.
	 */
	public long getCoordinates() {
		return coordinate;
	}

	/**
	 * Gets the list of objects.
	 *
	 * @return The list of objects.
	 */
	public Collection<RSObject> getRSObjects() {
		return objects;
	}
	
	public void addRSObject(RSObject o) {
		objects.add(o);
	}
	
	public void removeRSObject(RSObject o) {
		objects.remove(o);
	}
	
	public void makeDynamicData() {
		if (dynamicData != null) {
			return;
		}

		dynamicData = new int[4][8][8][4];
	}
	
	public void setDynamicData(int chunkX, int chunkY, int chunkZ, int x, int y, int z, int rotation) {
		z = z % 4;
		dynamicData[z][x][y][0] = chunkX;
		dynamicData[z][x][y][1] = chunkY;
		dynamicData[z][x][y][2] = chunkZ;
		dynamicData[z][x][y][3] = rotation;
	}

	public void removeDynamicData() {
		dynamicData = null;
	}

	public int[][][][] getDynamicData() {
		return dynamicData;
	}

	/**
	 * Gets the list of GroundItems.
	 *
	 * @return The list of GroundItems.
	 */
	public Collection<GroundItem> getGroundItems() {
		// synchronized(this) {
		return items;//return Collections.unmodifiableCollection(new LinkedList<>(items));
		// }
	}

	/**
	 * Gets the list of NPCs.
	 *
	 * @return The list of NPCs.
	 */
	public Collection<NPC> getNpcs() {
		// synchronized(this) {
		return npcs; //return Collections.unmodifiableCollection(new LinkedList<>(npcs));
		// }
	}

	/**
	 * Gets the list of players.
	 *
	 * @return The list of players.
	 */
	public Collection<Player> getPlayers() {
		// synchronized(this) {
		return players;//return Collections.unmodifiableCollection(new LinkedList<>(players));
		// }
	}
	
	public LinkedList<Player> getPlayersDirect() {
		return players;
	}

	/**
	 * Removes an old GroundItem.
	 *
	 * @param item
	 *            The GroundItem to remove.
	 */
	public void removeGroundItem(GroundItem item) {
		// synchronized(this) {
		items.remove(item);
		// }
	}

	/**
	 * Removes an old NPC.
	 *
	 * @param npc
	 *            The NPC to remove.
	 */
	public void removeNpc(NPC npc) {
		// synchronized(this) {
		npcs.remove(npc);
		// }
	}

	/**
	 * Removes an old player.
	 *
	 * @param player
	 *            The player to remove.
	 */
	public void removePlayer(Player player) {
		// synchronized(this) {
		players.remove(player);
		// }
	}

}
