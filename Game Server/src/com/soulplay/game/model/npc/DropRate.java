package com.soulplay.game.model.npc;

import com.soulplay.DBConfig;

/*
 * WARNING: Make absolutely sure to update the method "getRate(double)" with new
 * values if you update the rates.
 */ // << READ THIS
public enum DropRate {
	// Common was slightly higher percentage than 1/2, but I didn't want to mess
	// with decimals. Besides, less than 1/2 is just insanely high. //old rates
	// - RARE = 128 VERY_RARE = 312 SUPER_RARE = 512
	ALWAYS(1, "<col=00FF00>"),
	COMMON(2, "<col=62FF00>"),
	UNCOMMON(9, "<col=AEFF00>"),
	VERY_UNCOMMON(24, "<col=F2FF00>"),
	SUPER_UNCOMMON(41, "<col=FFE100>"),
	RARE(82, "<col=FFA200>"),
	VERY_RARE(212, "<col=FF4D00>"),
	SUPER_RARE(412, "<col=FF0000>"),
	MYTHIC_RARE(1936, "<col=5D00FF>"),
	LEGENDARY_RARITY(5000, "<col=FF00FF>"),
	DIVINE_RARITY(15000, "<col=0000FF>"),
	SUPER_DIVINE_RARITY(40000, "<col=0000FF>"),
	ONE_IN_A_MILLION(1000000, "<col=FF0000>");

	// TODO Update this whenever you modify rates.
	// ONE_IN_A_MILLION is not included, as it is currently impossible as the
	// max value is 65545, or something like that (DropTableItem.java)
	public static DropRate getRate(double percent) {
		int i = (int) (percent * 1000000);
		if (i <= (int) ((1 / 40000.0) * 1000000)) {
			return SUPER_DIVINE_RARITY;
		}
		if (i <= (int) ((1 / 15000.0) * 1000000)) {
			return DIVINE_RARITY;
		}
		if (i <= (int) ((1 / 5000.0) * 1000000)) {
			return LEGENDARY_RARITY;
		}
		if (i <= (int) ((1 / 1936.0) * 1000000)) {
			return MYTHIC_RARE;
		}
		if (i <= (int) ((1 / 412.0) * 1000000)) {
			return SUPER_RARE;
		}
		if (i <= (int) ((1 / 212.0) * 1000000)) {
			return VERY_RARE;
		}
		if (i <= (int) ((1 / 82.0) * 1000000)) {
			return RARE;
		}
		if (i <= (int) ((1 / 41.0) * 1000000)) {
			return SUPER_UNCOMMON;
		}
		if (i <= (int) ((1 / 24.0) * 1000000)) {
			return VERY_UNCOMMON;
		}
		if (i <= (int) ((1 / 9.0) * 1000000)) {
			return UNCOMMON;
		}
		if (i <= (int) ((1 / 2.0) * 1000000)) {
			return COMMON;
		}
		return ALWAYS;
	}

	public static DropRate getRate(String s) {
		switch (s.toUpperCase()) {
			case "ALWAYS":
				return ALWAYS;
			case "COMMON":
				return COMMON;
			case "UNCOMMON":
				return UNCOMMON;
			case "SUPER_UNCOMMON":
				return SUPER_UNCOMMON;
			case "VERY_UNCOMMON":
				return VERY_UNCOMMON;
			case "RARE":
				return RARE;
			case "VERY_RARE":
				return VERY_RARE;
			case "EXTREMELY_RARE":
			case "SUPER_RARE":
				return SUPER_RARE;
			case "MYTHIC_RARE":
				return MYTHIC_RARE;
			case "LEGENDARY_RARITY":
				return LEGENDARY_RARITY;
			case "DIVINE_RARITY":
				return DIVINE_RARITY;
			case "SUPER_DIVINE_RARITY":
				return SUPER_DIVINE_RARITY;
			case "ONE_IN_A_MILLION":
				return ONE_IN_A_MILLION;
			default:
				System.err.println("Unknown Rarity: " + s);
				return UNCOMMON;
		}
	}

	private int probability;

	private String colorString;

	private DropRate(int probability, String colorString) {
		this.probability = probability;
		this.colorString = colorString;
	}

	public String getColorString() {
		return colorString;
	}

	public int getProbability() {
		switch (this) {
			case VERY_RARE:
			case RARE:
				return (int) (probability / DBConfig.RARE_DROP_MODIFIER);
			case SUPER_RARE:
				return (int) (probability / DBConfig.SUPER_RARE_DROP_MODIFIER);
			default:
				return probability;
		}
	}

	public int getRawProbability() {
		return probability;
	}
}
