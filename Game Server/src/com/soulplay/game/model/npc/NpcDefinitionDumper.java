package com.soulplay.game.model.npc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

import com.soulplay.cache.EntityDef;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.util.sql.SqlHandler;

public class NpcDefinitionDumper {

	private static final String FILE_LOC = "npc_dump.txt";
	
	public static void readNpcFile() {
		int count = 0;
		
		int npcId = 0;
		
		
		
		try (BufferedReader file = new BufferedReader(new FileReader("./" + FILE_LOC));
				Connection con = SqlHandler.getHikariZaryteTable().getConnection();) {
			
			String line = file.readLine();
			
			boolean endOfFile = false;
			
			boolean newNpc = true;
			CombatStats stats = new CombatStats();
			boolean aggressive = false;
			boolean poison = false;
			boolean venom = false;
			Skills skills = new Skills(null);
			
			NPCDefinitions def = NPCDefinitions.getDefinitions()[0];
			
			EntityDef eDef = EntityDef.forID(0);
			
			while (!endOfFile && line != null) {
				if (line.equals("[END]")) {
					try {
						file.close();
					} catch (IOException ioexception) {
						ioexception.printStackTrace();
					}
					break;
				}
				
				if (newNpc && line.startsWith("id:")) {
					newNpc = false;
					aggressive = false;
					poison = false;
					venom = false;
					line = line.substring(3);
					line = line.trim();
					npcId = Integer.parseInt(line);
					
					stats = new CombatStats();
					skills = new Skills(null);
					
					def = NPCDefinitions.getDefinitions()[npcId];
					
					eDef = EntityDef.forID(npcId);
					
					if (def == null) {
//						System.out.println("def is null for NPC ID:"+npcId);
						def = new NPCDefinitions(npcId);
						def.setNpcHealth(10);
					}

					if (def.getWeakness() == null)
						def.setWeakness(new HashSet<>());
					
					count++;
					
				} else if (line.startsWith("hp:")) {
					line = line.substring(4);
					line = line.replaceAll(",", "");
					String[] array = line.split(" ");
					int hp = 10;
					try {
						for (int i = 0; i < array.length; i++) {
							if (array[i].isEmpty() || array[i].equals(" ") || array[i].equals("")) continue;
							array[i] = array[i].trim();
							if (array[i].contains("0")) break;
							try {
								hp = Integer.parseInt(array[i]);
								break;
							} catch (Exception e) {
								continue;
							}
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it2! npcid:"+npcId);
						endOfFile = true;
					}
					def.setNpcHealth(hp);
					
				} else if (line.startsWith("aggressive:")) {
					if (line.toLowerCase().contains("yes"))
						aggressive = true;
					
				} else if (line.startsWith("poisonous:")) {
					if (line.toLowerCase().contains("venom"))
						venom = true;
					else if (line.toLowerCase().contains("yes"))
						poison = true;
					
				} else if (line.startsWith("weakness:")) {
					
					line = line.toLowerCase();
					
					if (line.contains("stab")) {
						def.getWeakness().add(NpcWeakness.STAB);
					}
					if (line.contains("slash")) {
						def.getWeakness().add(NpcWeakness.SLASH);
					}
					if (line.contains("crush")) {
						def.getWeakness().add(NpcWeakness.CRUSH);
					}
					if (line.contains("ranged")) {
						def.getWeakness().add(NpcWeakness.RANGED);
					}
					if (line.contains("magic")) {
						def.getWeakness().add(NpcWeakness.MAGIC);
					}
					if (line.contains("fire")) {
						def.getWeakness().add(NpcWeakness.FIRE_SPELLS);
					}
					if (line.contains("air")) {
						def.getWeakness().add(NpcWeakness.AIR_SPELLS);
					}
					if (line.contains("water")) {
						def.getWeakness().add(NpcWeakness.WATER_SPELLS);
					}
					if (line.contains("earth")) {
						def.getWeakness().add(NpcWeakness.EARTH_SPELLS);
					}
					if (line.contains("salve") || line.contains("undead")) {
						def.getWeakness().add(NpcWeakness.UNDEAD);
					}
					if (line.contains("keris")) {
						def.getWeakness().add(NpcWeakness.KERIS);
					}
					if (line.contains("axe")) {
						def.getWeakness().add(NpcWeakness.AXES);
					}
					
				} else if (line.startsWith("attack:")) {
					line = line.substring(8);
					String[] array = line.split(" ");
					int attack = 1;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							if (loc.equals("0")) break;
							attack = Integer.parseInt(loc);
							break;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it1! npcid:"+npcId);
						endOfFile = true;
					}
					skills.setStaticLevel(Skills.ATTACK, attack);
					
				} else if (line.startsWith("strength:")) {
					line = line.substring(10);
					String[] array = line.split(" ");
					int str = 1;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							if (loc.equals("0")) break;
							str = Integer.parseInt(loc);
							break;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it2! npcid:"+npcId);
						endOfFile = true;
					}
					skills.setStaticLevel(Skills.STRENGTH, str);
					
				} else if (line.startsWith("defence:")) {
					line = line.substring(9);
					String[] array = line.split(" ");
					int defense = 1;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							if (loc.equals("0")) break;
							defense = Integer.parseInt(loc);
							break;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it3! npcid:"+npcId);
						endOfFile = true;
					}
					skills.setStaticLevel(Skills.DEFENSE, defense);
					
				} else if (line.startsWith("ranged:")) {
					line = line.substring(8);
					String[] array = line.split(" ");
					int ranged = 1;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							if (loc.equals("0")) break;
							ranged = Integer.parseInt(loc);
							break;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it4! npcid:"+npcId);
						endOfFile = true;
					}
					skills.setStaticLevel(Skills.RANGED, ranged);
					
				} else if (line.startsWith("magic:")) {
					line = line.substring(7);
					String[] array = line.split(" ");
					int magicLvl = 1;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							if (loc.equals("0")) break;
							magicLvl = Integer.parseInt(loc);
							break;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it5! npcid:"+npcId);
						endOfFile = true;
					}
					skills.setStaticLevel(Skills.MAGIC, magicLvl);
					
				} else if (line.startsWith("aggressiveStats:")) {
					line = line.substring(17);
					String[] array = line.split(" ");
					int stab = 0;
					int slash = 0;
					int crush = 0;
					int magic = 0;
					int ranged = 0;
					int accCount = 0;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							
							int parsed = 0;

							try {
								parsed = Integer.parseInt(loc);
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							
							switch (accCount) {
							case 0:
								stab = parsed;
								break;
							case 1:
								slash = parsed;
								break;
							case 2:
								crush = parsed;
								break;
							case 3:
								magic = parsed;
								break;
							case 4:
								ranged = parsed;
								break;
							}
							accCount++;
							continue;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it888! npcid:"+npcId);
						endOfFile = true;
					}
					stats.setStabBonus(stab);
					stats.setSlashBonus(slash);
					stats.setCrushBonus(crush);
					stats.setMagicAccBonus(magic);
					stats.setRangedAccBonus(ranged);
					
				} else if (line.startsWith("defensiveStats:")) {
					line = line.substring(16);
					String[] array = line.split(" ");
					int stab = 0;
					int slash = 0;
					int crush = 0;
					int magic = 0;
					int ranged = 0;
					int accCount = 0;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							
							int parsed = 0;

							try {
								parsed = Integer.parseInt(loc);
							} catch (Exception e) {
								//e.printStackTrace();
								System.out.println("failed reading for npc ID:"+npcId);
							}
							
							
							switch (accCount) {
							case 0:
								stab = parsed;
								break;
							case 1:
								slash = parsed;
								break;
							case 2:
								crush = parsed;
								break;
							case 3:
								magic = parsed;
								break;
							case 4:
								ranged = parsed;
								break;
							}
							accCount++;
							continue;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it999! npcid:"+npcId);
						endOfFile = true;
					}
					stats.setStabDefBonus(stab);
					stats.setSlashDefBonus(slash);
					stats.setCrushDefBonus(crush);
					stats.setMagicDefBonus(magic);
					stats.setRangedDefBonus(ranged);
					
				} else if (line.startsWith("otherStats:")) {
					line = line.substring(12);
					String[] array = line.split(" ");
					int attack = 0;
					int str = 0;
					int ranged = 0;
					int accCount = 0;
					try {
						for (int i = 0; i < array.length; i++) {
							String loc = array[i];
							if (loc.isEmpty() || loc.equals(" ") || loc.equals("")) continue;
							loc = loc.trim();
							loc = loc.replaceAll(" ", "");
							
							int parsed = 0;

							try {
								parsed = Integer.parseInt(loc);
							} catch (Exception e) {
								//e.printStackTrace();
								System.out.println("failed reading for npc ID:"+npcId);
							}
							
							
							switch (accCount) {
							case 0:
								str = parsed;
								break;
							case 1:
								ranged = parsed;
								break;
							case 2:
								attack = parsed;
								break;
							}
							accCount++;
							if (accCount == 3)
								break;
							continue;
						}
					} catch (Exception ex) {
						System.out.println("Couldn't do it555! npcid:"+npcId);
						endOfFile = true;
					}
					stats.setStrengthBonus(str);
					stats.setRangeStrengthBonus(ranged);
					stats.setAttackStrength(attack);
					
				} else if (line.startsWith("attackSpeed:")) {
					newNpc = true;
					line = line.substring(13);
					int aS;
					try {
						aS = Integer.parseInt(line);
					} catch (Exception e) {
						aS = 4;
					}
					stats.setAttackSpeed(10-aS);
					
					insertNpc(npcId, eDef, def, stats, aggressive, poison, venom, skills, con);
				}

				try {
					line = file.readLine();
				} catch (IOException ioexception1) {
					endOfFile = true;
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		System.out.println("looped amount:" + count + " lastNpcId:" + npcId);
	}
	
	
	private static final String INSERT = "INSERT INTO `zaryte`.`npc_def`(`npc_id`, `npc_name`, `combat_lvl`, `hp`, `aggro`, `poison`, `venom`, `weakness`, `attack_lvl`, `str_lvl`, `defense_lvl`, `magic_lvl`, `ranged_lvl`, `stab_acc`, `slash_acc`, `crush_acc`, `magic_acc`, `ranged_acc`, `stab_def`, `slash_def`, `crush_def`, `magic_def`, `ranged_def`, `melee_str`, `ranged_str`, `attack_str`, `attack_speed`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `npc_id` = VALUES(`npc_id`), `npc_name` = VALUES(`npc_name`), `combat_lvl` = VALUES(`combat_lvl`), `hp` = VALUES(`hp`), `aggro` = VALUES(`aggro`), `poison` = VALUES(`poison`), `venom` = VALUES(`venom`), `weakness` = VALUES(`weakness`), `attack_lvl` = VALUES(`attack_lvl`), `str_lvl` = VALUES(`str_lvl`), `defense_lvl` = VALUES(`defense_lvl`), `magic_lvl` = VALUES(`magic_lvl`), `ranged_lvl` = VALUES(`ranged_lvl`), `stab_acc` = VALUES(`stab_acc`), `slash_acc` = VALUES(`slash_acc`), `crush_acc` = VALUES(`crush_acc`), `magic_acc` = VALUES(`magic_acc`), `ranged_acc` = VALUES(`ranged_acc`), `stab_def` = VALUES(`stab_def`), `slash_def` = VALUES(`slash_def`), `crush_def` = VALUES(`crush_def`), `magic_def` = VALUES(`magic_def`), `ranged_def` = VALUES(`ranged_def`), `melee_str` = VALUES(`melee_str`), `ranged_str` = VALUES(`ranged_str`), `attack_str` = VALUES(`attack_str`), `attack_speed` = VALUES(`attack_speed`)";
	
	public static void insertNpc(int id, EntityDef eDef, NPCDefinitions nDef, CombatStats stats, boolean aggressive, boolean poison, boolean venom, Skills skills, Connection con) {
		
		if (con == null)
			try {
				con = SqlHandler.getHikariZaryteTable().getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		try (PreparedStatement stmt = con.prepareStatement(INSERT)) {
			stmt.setInt(1, id);
			stmt.setString(2, eDef.getName());
			stmt.setInt(3, eDef.combatLevel);
			stmt.setInt(4, nDef.getNpcHealth());
			stmt.setBoolean(5, aggressive);
			stmt.setBoolean(6, poison);
			stmt.setBoolean(7, venom);
			stmt.setString(8, PlayerSaveSql.createJsonString(nDef.getWeakness()));
			stmt.setInt(9, skills.getStaticLevel(Skills.ATTACK));
			stmt.setInt(10, skills.getStaticLevel(Skills.STRENGTH));
			stmt.setInt(11, skills.getStaticLevel(Skills.DEFENSE));
			stmt.setInt(12, skills.getStaticLevel(Skills.MAGIC));
			stmt.setInt(13, skills.getStaticLevel(Skills.RANGED));
			stmt.setInt(14, stats.getStabBonus());
			stmt.setInt(15, stats.getSlashBonus());
			stmt.setInt(16, stats.getCrushBonus());
			stmt.setInt(17, stats.getMagicAccBonus());
			stmt.setInt(18, stats.getRangedAccBonus());
			stmt.setInt(19, stats.getStabDefBonus());
			stmt.setInt(20, stats.getSlashDefBonus());
			stmt.setInt(21, stats.getCrushDefBonus());
			stmt.setInt(22, stats.getMagicDefBonus());
			stmt.setInt(23, stats.getRangedDefBonus());
			stmt.setInt(24, stats.getStrengthBonus());
			stmt.setInt(25, stats.getRangeStrengthBonus());
			stmt.setInt(26, stats.getAttackStr());
			stmt.setInt(27, stats.getAttackSpeed());
			stmt.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("NPCNAME:"+eDef.getName()+" size:"+ eDef.getName().length());
		}
		
	}
	
	public static void fixHp() {
		try (Connection con = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement stmt = con.prepareStatement("SELECT * FROM `npc_def` WHERE `hp` = 10");
				ResultSet set = stmt.executeQuery();) {
			
			while (set.next()) {
				int npcId = set.getInt(1);
				int dbCmb = set.getInt(3);
				int hp = 10;

				NPCDefinitions def = NPCDefinitions.getDefinitions()[npcId];
				if (dbCmb == 0) {
					if (def != null) {
						dbCmb = def.getNpcCombat();
					}
				}
				if (def != null)
					hp = def.getNpcHealth();

				try (PreparedStatement stmt2 = con.prepareStatement("UPDATE `npc_def` SET hp = "+hp+", combat_lvl = "+dbCmb + " WHERE npc_id = "+npcId);) {
					stmt2.executeUpdate();
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("DONE FIXING HP!!!!!!!!!!!---------------------------------");
	}
	
}
