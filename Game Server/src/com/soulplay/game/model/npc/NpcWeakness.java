package com.soulplay.game.model.npc;

public enum NpcWeakness {

	NONE,
	STAB,
	SLASH,
	CRUSH,
	MELEE,
	RANGED,
	MAGIC,
	FIRE_SPELLS,
	AIR_SPELLS,
	WATER_SPELLS,
	EARTH_SPELLS,
	UNDEAD,//SALVE AMULET, CRUMBLE UNDEAD
	KERIS,
	AXES,
	LIGHT;
	
	public static final NpcWeakness[] values = values();
}
