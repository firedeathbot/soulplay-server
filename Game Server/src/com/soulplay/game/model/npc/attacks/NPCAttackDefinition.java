package com.soulplay.game.model.npc.attacks;

import com.soulplay.game.model.npc.NPC;

public final class NPCAttackDefinition {
	
	private int[] id;
	
	private Attack[] attacks;
	
	public NPCAttackDefinition(int[] id, Attack... attacks) {
		this.id = id;
		this.attacks = attacks;
	}
	
	public Attack[] getAttacks() {
		return attacks;
	}
	
	public int[] getId() {
		return id;
	}
	
	public int countAttacks() {
		boolean hasMelee = false, hasRange = false, hasMage = false;
		int count = 0;
		for (Attack attack : attacks) {
			if (attack.attackType == 2 && !hasMage) {
				hasMage = true;
				count++;
			} else if (attack.attackType == 0 && !hasMelee) {
				hasMelee = true;
				count++;
			} else if (attack.attackType == 1 && !hasRange) {
				hasRange = true;
				count++;
			}
		}
		
		return count;
	}
	
	public boolean hasMageAttack() {
		for (Attack attack : attacks) {
			if (attack.attackType == 2) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasMeleeAttack() {
		for (Attack attack : attacks) {
			if (attack.attackType == 0) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasRangeAttack() {
		for (Attack attack : attacks) {
			if (attack.attackType == 1) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasRangeAndMagicAttacks() {
		boolean hasMagic = false;
		boolean hasRange = false;
		for (Attack attack : attacks) {
			if (attack.attackType == 1) {
				hasRange = true;
			}
			if (attack.attackType == 2) {
				hasMagic = true;
			}
			if (hasRange && hasMagic)
				return true;
		}
		return false;
	}
	
	public boolean setRangeAttack(NPC npc) {
		for (Attack attack : attacks) {
			if (attack.attackType == 1) {
				npc.projectileId = attack.projectileId;
				npc.endGfx = attack.endGfx;
				npc.attackType = attack.attackType;
				npc.currentAttackMulti = attack.multi;
				npc.attackType = 1;
				return true;
			}
		}
		return false;
	}
	
	public boolean setMagicAttack(NPC npc) {
		for (Attack attack : attacks) {
			if (attack.attackType == 2) {
				npc.projectileId = attack.projectileId;
				npc.endGfx = attack.endGfx;
				npc.attackType = attack.attackType;
				npc.currentAttackMulti = attack.multi;
				npc.attackType = 2;
				return true;
			}
		}
		return false;
	}
	
}
