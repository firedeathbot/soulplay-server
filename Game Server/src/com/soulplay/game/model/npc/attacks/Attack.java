package com.soulplay.game.model.npc.attacks;

import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;

public final class Attack {
	
	public int attackType;
	
	public int projectileId;
	
	public int endGfx;
	
	public int startGfx;
	
	public int animId;
	
	public boolean multi;
	
	public Graphic gfxNew;
	
	public Projectile projectileNew;
	
	public Attack(int attackType, int projectileId, int endGfx,
	              int startGfx, int animId) {
		this.attackType = attackType;
		this.projectileId = projectileId;
		this.endGfx = endGfx;
		this.startGfx = startGfx;
		this.animId = animId;
		this.multi = false;
	}
	
	public Attack setGfxNew(Graphic gfxNew) {
		this.gfxNew = gfxNew;
		return this;
	}
	
	public Attack(int attackType, int projectileId, int endGfx,
	              int startGfx, int animId, boolean multi) {
		this(attackType, projectileId, endGfx, startGfx, animId);
		this.multi = multi;
	}
	
}
