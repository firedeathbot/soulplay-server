package com.soulplay.game.model.npc.attacks;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * @author Jire - switched to attack builder system and optimized.
 */
public final class NPCAttackLoader {
	
	private static final Int2ObjectMap<NPCAttackDefinition> POKEMON_DEFINITIONS = new Int2ObjectOpenHashMap<>();
	
	public static Int2ObjectMap<NPCAttackDefinition> getPokemonDefinitions() {
		return POKEMON_DEFINITIONS;
	}
	
	public static NPCAttackDefinition getPokemon(int npcId) {
		return POKEMON_DEFINITIONS.get(npcId);
	}
	
	private static final Int2ObjectMap<NPCAttackDefinition> DEFINITIONS = new Int2ObjectOpenHashMap<>();
	
	public static Int2ObjectMap<NPCAttackDefinition> getDefinitions() {
		return DEFINITIONS;
	}
	
	public static NPCAttackDefinition get(int npcId) {
		return DEFINITIONS.get(npcId);
	}
	
	private static NPCAttackDefinition[] pi_definitions;
	
	private static void addNew(NPCAttackDefinition def) {
		NPCAttackDefinition[] old = pi_definitions;
		NPCAttackDefinition[] newDefinitions = new NPCAttackDefinition[old.length + 1];
		System.arraycopy(old, 0, newDefinitions, 0, old.length);
		newDefinitions[old.length] = def;
		pi_definitions = newDefinitions;
	}
	
	public static Attack getNPCAttack(int npcId) {
		// TODO magic/ranged attacks should always be performed when NPC is not
		// within walking distance.
		// Because using this, the npc will not attack but only walk to the
		// player if a melee attack is chosen.
		NPCAttackDefinition def = get(npcId);
		if (def == null)
			return null;
		Attack[] attacks = def.getAttacks();
		return attacks[Misc.random(attacks.length - 1)];
	}
	
	public static Attack getAnyPokemonAttack(int npcId) {
		NPCAttackDefinition def = getPokemon(npcId);
		if (def == null)
			return null;
		Attack[] attacks = def.getAttacks();
		return attacks[Misc.randomNoPlus(attacks.length)];
	}
	
	public static Attack setAnyPokemonAttack(NPC npc) {
		Attack attack = getAnyPokemonAttack(npc.npcType);
		if (attack != null) {
			npc.projectileId = attack.projectileId;
			npc.endGfx = attack.endGfx;
			npc.attackType = attack.attackType;
			npc.currentAttackMulti = attack.multi;
			return attack;
		}
		return null;
	}
	
	public static Attack setRangeAttack(NPC npc) {
		NPCAttackDefinition def = npc.isPokemon ? getPokemon(npc.npcType) : get(npc.npcType);
		if (def == null)
			return null;
		for (Attack attack : def.getAttacks()) {
			if (attack.attackType == 1) {
				npc.projectileId = attack.projectileId;
				npc.endGfx = attack.endGfx;
				npc.attackType = attack.attackType;
				npc.currentAttackMulti = attack.multi;
				npc.attackType = 1;
				return attack;
			}
		}
		return null;
	}
	
	public static Attack setMagicAttack(NPC npc) {
		NPCAttackDefinition def = npc.isPokemon ? getPokemon(npc.npcType) : get(npc.npcType);
		if (def == null)
			return null;
		for (Attack attack : def.getAttacks()) {
			if (attack.attackType == 2) {
				npc.projectileId = attack.projectileId;
				npc.endGfx = attack.endGfx;
				npc.attackType = attack.attackType;
				npc.currentAttackMulti = attack.multi;
				npc.attackType = 2;
				return attack;
			}
		}
		return null;
	}
	
	public static Attack setMeleeAttack(NPC npc) {
		NPCAttackDefinition def = npc.isPokemon ? getPokemon(npc.npcType) : get(npc.npcType);
		if (def == null)
			return null;
		for (Attack attack : def.getAttacks()) {
			if (attack.attackType == 0) {
				npc.projectileId = attack.projectileId;
				npc.endGfx = attack.endGfx;
				npc.attackType = attack.attackType;
				npc.currentAttackMulti = attack.multi;
				return attack;
			}
		}
		return null;
	}
	
	public static void initialize() {
		if (pi_definitions != null) {
			throw new IllegalStateException(
					"Npc attack definitions already loaded.");
		}
		System.out.println("Loading npc attack definitions...");
		try {
			BufferedReader br = new BufferedReader(
					new FileReader("./Data/cfg/npcCombat.json"));
			pi_definitions = GsonSave.gsond.fromJson(br, NPCAttackDefinition[].class);
			/* Penance Queen */
			/*
			 * addNew(new NPCAttaackDefinition(new int [] { 5247 }, new Attack
			 * [] { new Attack(0, 0, 0, 0, 0), new Attack(1, 871, 872, 0, 0),
			 * }));
			 */
			/* Ahrim the Blighted */
			/*
			 * int attackType, int projectileId, int endGfx,
				int startGfx, int animId
			 */
			addNew(new NPCAttackDefinition(new int[]{2025},
					new AttackBuilder()
							.setAttackType(2)
							.setProjectileId(2735)
							.setEndGfx(2740)
							.setStartGfx(2728)
							.createAttack()
							.setGfxNew(Graphic.create(2728, GraphicType.LOW))));
			/* Dragons */
			int[] dragonIds = new int[]{53, 54, 55, 941, 1590, 1591, 1592, 4677, 4678, 4679, 4680,
					10604, 10605, 10606, 10607, 10608, 10609, 10219, 10220,
					10221, 10222, 10223, 10224, 10776, 10777, 10778, 10779,
					10780, 10781, 10815, 10816, 10817, 10818, 10819, 10820};
			addNew(new NPCAttackDefinition(dragonIds,
					new AttackBuilder()
							.createAttack(),
					new AttackBuilder()
							.setAttackType(3)
							.setProjectileId(393)
							.setAnimId(12259)
							.createAttack()));
			/* Tok-Xil */
			addNew(new NPCAttackDefinition(new int[]{2631},
					new AttackBuilder()
							.setAttackType(1)
							.setProjectileId(443)
							.createAttack(),
					new AttackBuilder()
							.createAttack()));
			/* Ket-Zek */
			addNew(new NPCAttackDefinition(new int[]{2743},
					new AttackBuilder()
							.setAttackType(2)
							.setProjectileId(4445)
							.setEndGfx(4446)
							.createAttack(),
					new AttackBuilder()
							.createAttack()));
			/* Jad */
//			addNew(new NPCAttackDefinition(new int[]{2745},
//					new Attack[]{new Attack(2, 1627, 157, 0, 0),
//							new Attack(1, 0, 451, 1625, 0),
//							new Attack(0, 0, 0, 0, 0),}));
			/* Defiler from Pest control */
			addNew(new NPCAttackDefinition(
					new int[]{3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769,
							3770, 3771},
					new AttackBuilder()
							.setAttackType(1)
							.setProjectileId(657)
							.createAttack()));
			/* Torcher from Pest control */
			addNew(new NPCAttackDefinition(
					new int[]{3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759,
							3760, 3761},
					new AttackBuilder()
							.setAttackType(2)
							.setProjectileId(647)
							.setEndGfx(648)
							.setStartGfx(646)
							.createAttack()));
			
			/* Chaos Dwarf Hand Cannoneer */
			Attack chaosDwarfAttk = new AttackBuilder()
					.setAttackType(1)
					.setAnimId(12141)
					.createAttack();
			chaosDwarfAttk.gfxNew = Graphic.create(2141, Misc.serverToClientTick(1), GraphicType.LOW);
			chaosDwarfAttk.projectileNew = new Projectile(2143, Location.create(0, 0), Location.create(0, 0));
			chaosDwarfAttk.projectileNew.setStartHeight(20);
			chaosDwarfAttk.projectileNew.setEndHeight(22);
			chaosDwarfAttk.projectileNew.setStartDelay(60); // TODO: i'm not sure if distance or delay belongs here
			chaosDwarfAttk.projectileNew.setEndDelay(30);
			chaosDwarfAttk.projectileNew.setSlope(1);
			addNew(new NPCAttackDefinition(
					new int[]{8776, 8777},
					chaosDwarfAttk));
			
			System.out.println("Loaded " + pi_definitions.length
					+ " npc attack definitions.");
			
			putAllPISHITINTOMAP();
			pi_definitions = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		loadPokemonAttacks();
	}
	
	public static void putAllPISHITINTOMAP() {
		for (NPCAttackDefinition def : pi_definitions) {
			int[] id = def.getId();
			for (int i : id) {
				DEFINITIONS.put(i, def);
			}
		}
	}
	
	public static void loadPokemonAttacks() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader("./Data/cfg/pokemonCombat.json"));
			NPCAttackDefinition[] definitions = GsonSave.gsond.fromJson(br, NPCAttackDefinition[].class);
			System.out.println("Loaded " + definitions.length
					+ " npc pokemon attack definitions.");
			for (NPCAttackDefinition def : definitions) {
				int[] id = def.getId();
				for (int i : id) {
					POKEMON_DEFINITIONS.put(i, def);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private NPCAttackLoader() {
		throw new UnsupportedOperationException();
	}
	
}
