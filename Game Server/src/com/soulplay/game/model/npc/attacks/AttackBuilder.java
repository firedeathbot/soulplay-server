package com.soulplay.game.model.npc.attacks;

/**
 * @author Jire
 */
public final class AttackBuilder {
	
	private int attackType;
	private int projectileId;
	private int endGfx;
	private int startGfx;
	private int animId;
	private boolean multi = false;
	
	public AttackBuilder setAttackType(int attackType) {
		this.attackType = attackType;
		return this;
	}
	
	public AttackBuilder setProjectileId(int projectileId) {
		this.projectileId = projectileId;
		return this;
	}
	
	public AttackBuilder setEndGfx(int endGfx) {
		this.endGfx = endGfx;
		return this;
	}
	
	public AttackBuilder setStartGfx(int startGfx) {
		this.startGfx = startGfx;
		return this;
	}
	
	public AttackBuilder setAnimId(int animId) {
		this.animId = animId;
		return this;
	}
	
	public AttackBuilder setMulti(boolean multi) {
		this.multi = multi;
		return this;
	}
	
	public Attack createAttack() {
		return new Attack(attackType, projectileId, endGfx, startGfx, animId);
	}
	
}
