package com.soulplay.game.model.npc;

import java.util.LinkedList;

import com.soulplay.game.map.clip.region.RegionClip;

public class NPCPathFinder {

	private static final NPCPathFinder npcPathFinder = new NPCPathFinder();

	public static boolean canMove(int x, int y, int height, int moveTypeX,
			int moveTypeY) {
		if (height > 3) {
			height = height & 3;
		}
		// Region r = region;//West
		// East
		if (moveTypeX == 1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x, y, height) & 0x1280108) == 0
					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280180) == 0;
		} else
		// West
		if (moveTypeX == -1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x, y, height) & 0x1280180) == 0
					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280108) == 0;
		} else
		// North
		if (moveTypeX == 0 && moveTypeY == 1) {
			// -1)
			return (RegionClip.getClipping(x, y, height) & 0x1280102) == 0
					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280120) == 0;
		} else
		// South
		if (moveTypeX == 0 && moveTypeY == -1) {
			// 1)
			return (RegionClip.getClipping(x, y, height) & 0x1280120) == 0
					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280102) == 0;
		} else
		// NorthEast
		if (moveTypeX == 1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280102) == 0// Check
																			// if
																			// can
																			// go
																			// north.
					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280120) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280108) == 0// Check
																				// if
																				// can
																				// go
																				// East
					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280108) == 0// Check if can go East from North
					&& (RegionClip.getClipping(x + 1, y + 1, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280102) == 0// Check if can go North from East
					&& (RegionClip.getClipping(x + 1, y + 1, height)
							& 0x1280120) == 0;
		} else
		// NorthWest
		if (moveTypeX == -1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280102) == 0// Going
																			// North
					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280120) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280180) == 0// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y + 1, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280102) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y + 1, height)
							& 0x1280120) == 0;
		} else
		// SouthEast
		if (moveTypeX == 1 && moveTypeY == -1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280120) == 0// Going
																			// South
					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280102) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280108) == 0// Going
																				// East
					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280108) == 0// Going East from South
					&& (RegionClip.getClipping(x + 1, y - 1, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280120) == 0// Going East
					&& (RegionClip.getClipping(x + 1, y - 1, height)
							& 0x1280102) == 0;
		} else
		// SouthWest
		if (moveTypeX == -1 && moveTypeY == -1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280120) == 0// Going
																			// North
					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280102) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280180) == 0// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y - 1, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280120) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y - 1, height)
							& 0x1280102) == 0;
		}
		return false;
	}

	public static NPCPathFinder getPathFinder() {
		return npcPathFinder;
	}

	public NPCPathFinder() {
	}

	/**
	 * Path finding for the minions, will walk up if path is blocked?
	 *
	 * @param minion
	 * @param destX
	 * @param destY
	 * @param moveNear
	 * @param xLength
	 * @param yLength
	 */
	public void findRoute(NPC minion, int destX, int destY, boolean moveNear,
			int xLength, int yLength) {
		if (destX == minion.getLocalX() && destY == minion.getLocalY()
				&& !moveNear) {
			return;
		}
		destX = destX - 8 * minion.getMapRegionX();
		destY = destY - 8 * minion.getMapRegionY();
		int[][] via = new int[104][104];
		int[][] cost = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		for (int xx = 0; xx < 104; xx++) {
			for (int yy = 0; yy < 104; yy++) {
				cost[xx][yy] = 99999999;
			}
		}
		int curX = minion.getLocalX();
		int curY = minion.getLocalY();
		via[curX][curY] = 99;
		cost[curX][curY] = 0;
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);

		boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = minion.getMapRegionX() * 8 + curX;
			int curAbsY = minion.getMapRegionY() * 8 + curY;
			if (curX == destX && curY == destY) {
				foundPath = true;
				break;
			}
			tail = (tail + 1) % pathLength;
			int thisCost = cost[curX][curY] + 1;
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							minion.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
				cost[curX][curY - 1] = thisCost;
			}
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							minion.heightLevel) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
				cost[curX - 1][curY] = thisCost;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							minion.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
				cost[curX][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							minion.heightLevel) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
				cost[curX + 1][curY] = thisCost;
			}
			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							minion.heightLevel) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							minion.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							minion.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
				cost[curX - 1][curY - 1] = thisCost;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							minion.heightLevel) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							minion.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							minion.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
				cost[curX - 1][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							minion.heightLevel) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							minion.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							minion.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
				cost[curX + 1][curY - 1] = thisCost;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							minion.heightLevel) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							minion.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							minion.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
				cost[curX + 1][curY + 1] = thisCost;
			}
			minion.moveX = minion.moveY = -1;
		}
		if (!foundPath) {
			if (moveNear) {
				int i_223_ = 1000;
				int thisCost = 100;
				int i_225_ = 10;
				for (int x = destX - i_225_; x <= destX + i_225_; x++) {
					for (int y = destY - i_225_; y <= destY + i_225_; y++) {
						if (x >= 0 && y >= 0 && x < 104 && y < 104
								&& cost[x][y] < 100) {
							int i_228_ = 0;
							if (x < destX) {
								i_228_ = destX - x;
							} else if (x > destX + xLength - 1) {
								i_228_ = x - (destX + xLength - 1);
							}
							int i_229_ = 0;
							if (y < destY) {
								i_229_ = destY - y;
							} else if (y > destY + yLength - 1) {
								i_229_ = y - (destY + yLength - 1);
							}
							int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;
							if (i_230_ < i_223_ || (i_230_ == i_223_
									&& (cost[x][y] < thisCost))) {
								i_223_ = i_230_;
								thisCost = cost[x][y];
								curX = x;
								curY = y;
							}
						}
					}
				}
				minion.moveX = minion.moveY = -1;
				if (i_223_ == 1000) {
					return;
				}
			} else {
				return;
			}
		}
		tail = 0;
		tileQueueX.set(tail, curX);
		tileQueueY.set(tail++, curY);
		int l5;
		for (int j5 = l5 = via[curX][curY]; curX != minion.getLocalX()
				|| curY != minion.getLocalY(); j5 = via[curX][curY]) {
			if (j5 != l5) {
				l5 = j5;
				tileQueueX.set(tail, curX);
				tileQueueY.set(tail++, curY);
			}
			if ((j5 & 2) != 0) {
				curX++;
			} else if ((j5 & 8) != 0) {
				curX--;
			}
			if ((j5 & 1) != 0) {
				curY++;
			} else if ((j5 & 4) != 0) {
				curY--;
			}
		}
		// minion.resetWalkingQueue();
		// int size = tail--;
		int pathX = minion.getMapRegionX() * 8 + tileQueueX.get(tail);
		int pathY = minion.getMapRegionY() * 8 + tileQueueY.get(tail);
		minion.moveX = NPCHandler.GetMove(minion.absX, pathX);// =
																// localize(pathX,
																// minion.getMapRegionX());
																// //TODO: make
																// sure this
																// works
		minion.moveY = NPCHandler.GetMove(minion.absY, pathY);// localize(pathY,
																// minion.getMapRegionY());//TODO:
																// make sure
																// this works

		/*
		 * minion.resetWalkingQueue(); int size = tail--; int pathX =
		 * minion.getMapRegionX() * 8 + tileQueueX.get(tail); int pathY =
		 * minion.getMapRegionY() * 8 + tileQueueY.get(tail);
		 * minion.addToWalkingQueue(localize(pathX, minion.getMapRegionX()),
		 * localize(pathY, minion.getMapRegionY())); for (int i = 1; i < size;
		 * i++) { tail--; pathX = minion.getMapRegionX() * 8 +
		 * tileQueueX.get(tail); pathY = minion.getMapRegionY() * 8 +
		 * tileQueueY.get(tail); minion.addToWalkingQueue(localize(pathX,
		 * minion.getMapRegionX()), localize(pathY, minion.getMapRegionY())); }
		 */

	}

	public int localize(int x, int mapRegion) {
		return x - 8 * mapRegion;
	}

}
