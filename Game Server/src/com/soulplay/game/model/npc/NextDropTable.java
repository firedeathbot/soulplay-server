package com.soulplay.game.model.npc;

public class NextDropTable extends DropTableItem {

	public NextDropTable(DropRate rate) {
		super(65000, 0, rate.getProbability());
	}

}
