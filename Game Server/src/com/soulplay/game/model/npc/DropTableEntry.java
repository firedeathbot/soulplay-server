package com.soulplay.game.model.npc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.game.model.player.Client;

public final class DropTableEntry {

	public static DropTableEntry parseDropsFromString(String line, Client c) {
		int id = -1, itemId = -1, itemAmount1 = -1, itemAmount2 = -1, rate = -1;
		boolean varyingAmount = false;
		String[] tokens = line.split(" ");
		DropTableEntry entry = null;
		boolean comment = false;
		byte state = 0; // States : 0 - just started, 1 - rdtNPC, 2 - itemId, 3
						// - itemAmount1, 4 - rate
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("")) {
				break;
			}
			if (tokens[i].startsWith("//")) {
				continue;
			}
			if (tokens[i].startsWith("/*")) {
				comment = true;
				break;
			}
			if (tokens[i].endsWith("*/")) {
				comment = false;
				break;
			}
			if (comment) {
				break;
			}
			if (tokens[i].contains("//")) {
				tokens[i] = tokens[i].substring(0, tokens[i].indexOf("//"));
			}
			if (tokens[i].contains("/*")) {
				tokens[i] = tokens[i].substring(0, tokens[i].indexOf("/*"));
			}
			if (tokens[i].contains("*/")) {
				tokens[i] = tokens[i].substring(tokens[i].indexOf("*/"),
						tokens[i].length());
			}
			switch (state) {
				case 0:
					try {
						id = Integer.parseInt(tokens[i]);
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out.println("Line " + DropTable.lineNumber
								+ ": Drop table entry could not be parsed from string: "
								+ line);
						if (c != null) {
							c.sendMessage("Line " + DropTable.lineNumber
									+ ": Drop table entry could not be parsed from string: ");
							c.sendMessage(line);
						}
						return null;
					}
					state = 1;
					break;
				case 1:
					entry = new DropTableEntry(id,
							tokens[i].length() == 4
									? tokens[i].equalsIgnoreCase("true")
									: tokens[i].contains("1"));
					state = 2;
					break;
				case 2:
					try {
						itemId = Integer.parseInt(tokens[i]);
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out.println("Line " + DropTable.lineNumber
								+ ": Drop table entry could not be parsed from string: "
								+ line);
						System.err.println(
								"CHECK and make sure that there is or isn't a comma where it should be.");
						if (c != null) {
							c.sendMessage("Line " + DropTable.lineNumber
									+ ": Drop table entry could not be parsed from string: ");
							c.sendMessage(line);
						}
						return null;
					}
					state = 3;
					break;
				case 3:
					try {
						if (tokens[i].contains("-")) {
							String[] rates = tokens[i].split("-");
							itemAmount1 = Integer.parseInt(rates[0]);
							itemAmount2 = Integer.parseInt(rates[1]);
							varyingAmount = true;
						} else {
							itemAmount1 = Integer.parseInt(tokens[i]);
						}
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out.println("Line " + DropTable.lineNumber
								+ ": Drop table entry could not be parsed from string: "
								+ line);
						if (c != null) {
							c.sendMessage("Line " + DropTable.lineNumber
									+ ": Drop table entry could not be parsed from string: ");
							c.sendMessage(line);
						}
						return null;
					}
					state = 4;
					break;
				case 4:
					try {
						rate = startsWithNumber(tokens[i])
								? Integer.parseInt(tokens[i])
								: DropRate.getRate(tokens[i]).getProbability();
						if (varyingAmount) {
							entry.addDrop(itemId, itemAmount1, itemAmount2,
									rate);
						} else {
							entry.addDrop(itemId, itemAmount1, rate);
						}
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out.println("Line " + DropTable.lineNumber
								+ ": Drop table entry could not be parsed from string: "
								+ line);
						if (c != null) {
							c.sendMessage("Line " + DropTable.lineNumber
									+ ": Drop table entry could not be parsed from string: ");
							c.sendMessage(line);
						}
						return null;
					}
					varyingAmount = false;
					state = 2;
					break;
				default:
					System.out.println("Line " + DropTable.lineNumber
							+ ": Something strange happened while loading drop table entry...");
					if (c != null) {
						c.sendMessage("Line " + DropTable.lineNumber
								+ ": Something strange occured. ");
						c.sendMessage(line);
					}
					break;
			}
		}
		return entry;
	}

	private static boolean startsWithNumber(String s) {
		return s.startsWith("0") || s.startsWith("1") || s.startsWith("2")
				|| s.startsWith("3") || s.startsWith("4") || s.startsWith("5")
				|| s.startsWith("6") || s.startsWith("7") || s.startsWith("8")
				|| s.startsWith("9") || s.startsWith("0x");
	}

	private final int npcID;

	private final boolean rdtNPC;

	/*
	 * Warning: Do NOT use items for anything in terms of drops. It's defunct.
	 * rarityGroups has taken it's place
	 */
	@Deprecated // Still Sorta used, only within this class though.
	private List<DropTableItem> items = new ArrayList<>(); // Kept
																		// because
																		// it's
																		// easier
																		// to
																		// reference
																		// them
																		// all
																		// from
																		// same
	// Location instead of jumping into each group and grabbing them. It's just
	// a collective list of items.

	private List<RarityGroup> rarityGroups = new ArrayList<>();

	private int length = 0;

	public DropTableEntry(final int npcID, boolean rdtNPC) {
		this.npcID = npcID;
		this.rdtNPC = rdtNPC;
	}

	public DropTableEntry addAlwaysDrop(int id, int amount) {
		DropTableItem newItem = new DropTableItem(id, amount, amount, 1);
		return addDrop(newItem);
	}

	public DropTableEntry addDrop(DropTableItem item) {
		items.add(item);
		getRarityGroup(item.getRawProbability()).addItem(item);
		length++;
		return this;
	}

	public DropTableEntry addDrop(int id, int amount, int probability) {
		DropTableItem newItem = new DropTableItem(id, amount, amount,
				probability);
		return addDrop(newItem);
	}

	public DropTableEntry addDrop(int id, int amountMin, int amountMax,
			int probability) {
		DropTableItem newItem = new DropTableItem(id, amountMin, amountMax,
				probability);
		return addDrop(newItem);
	}

	public boolean dropsItem(int itemId) {
		return getItemEntry(itemId) != null;
	}

	public DropTableItem getItemEntry(int itemId) {
		for (RarityGroup group : rarityGroups) {
			for (DropTableItem item : group.getItems()) {
				if (item.getItemId() == itemId) {
					return item;
				}
			}
		}
		return null;
	}

	public DropTableItem[] getItems() {
		// It's INTENTIONAL that a copy is returned, instead of original.
		return Arrays.copyOf(items.toArray(new DropTableItem[items.size()]),
				items.size()); // Can't let outside members add stuff.
	}

	// Why might this be commented out? If I added it, there's obviously a
	// reason for it.
	// If you must know, that reason is for use in the future. Maybe you want to
	// add items to the list during runtime, and want to use a DropRate rarity,
	// instead of having to do math/look stuff up.
	// public DropTableEntry addDrop(int id, int amount, DropRate rate) {
	// items.add(new DropTableItem(id, amount, amount, rate.getProbability()));
	// return this;
	// }

	public int getNpcID() {
		return npcID;
	}

	public RarityGroup getRarityGroup(int rawProbability) {
		for (RarityGroup group : rarityGroups) {
			if (group.getRawProbability() == rawProbability) {
				return group;
			}
		}
		RarityGroup group = new RarityGroup(rawProbability);
		rarityGroups.add(group);
		return group;
	}

	// No longer used, get the rarity groups, and use their rarity..
	/*
	 * public DropTableItem getItem(int item) { return items.get(item); }
	 */

	// You cannot trust people with this object, or it could be modified without
	// passing through the intended methods.
	// This is for safety reasons. Why do you think RSPSs are so non-concise?
	// Because of simple things like this.
	public RarityGroup[] getRarityGroups() {
		return Arrays.copyOf(
				rarityGroups.toArray(new RarityGroup[rarityGroups.size()]),
				rarityGroups.size());
	}

	public boolean isRdtNPC() {
		return rdtNPC;
	}

	public int length() {
		return length;
	}

	private void resolveNulls() {
		while (rarityGroups.contains(null)) {
			rarityGroups.remove(null);
			length--;
		}
	}

	public DropTableEntry sort() {
		if (rarityGroups == null) {
			// differently, but meh. Lazybones24/7
			return this;
		}
		if (rarityGroups.contains(null)) {
			resolveNulls();
		}
		Collections.sort(rarityGroups); // TODO Create sorting comparable.
		return this;
	}

	@Override
	public String toString() {
		String string = npcID + " " + rdtNPC + ": ";
		for (int i = 0; i < items.size(); i++) {
			string += ", " + items.get(i).toString();
		}
		return string;
	}
}
