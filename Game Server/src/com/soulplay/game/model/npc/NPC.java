package com.soulplay.game.model.npc;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;

import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.minigames.FPLottery;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.npcs.NPCProjectInsanityStatics;
import com.soulplay.content.npcs.NpcStatsEntry;
import com.soulplay.content.npcs.combat.DistanceRules;
import com.soulplay.content.npcs.combat.NPCCombatData;
import com.soulplay.content.npcs.combat.NpcStatsHardCoded;
import com.soulplay.content.npcs.impl.Chins;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.melee.MeleeExtras;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.pets.pokemon.Capture;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Manager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.RoomReference;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fishing.DungeoneeringFishData;
import com.soulplay.content.raids.boss.greatolm.GreatOlmInstanceHandler;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.attacks.Attack;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.map.travel.zone.addon.WildGodWarsDungeonZone;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

import it.unimi.dsi.fastutil.ints.IntList;

public class NPC extends Mob {

	public boolean despawnNextTick = false;
	public Function<IntList, Integer> selectAggressionPlayer;
	public boolean stopAttackDefault = false;
	public int npcIndex;
	
	public int npcType;
	
	private String npcName;
	
	public String customName = null;
	
	public CycleEventContainer powerUp;
//	public int absX, absY;
	
	public Client summoner;
	
	public int makeX, makeY, maxHit, moveX, moveY, walkingType;

	public TobManager tobManager;

	public boolean inMultiZone;
	
	public boolean inWildPermanent;
	
	public boolean walksOnTopOfNpcs = false;
	
	public int properties;
	public int dungeoneeringType;
	public RoomReference reference;
	public int[][] dungItemData;
	public int dungeonTier = -1;
	public int dungeonWeaponIdentifier = -1;
	public DungeonManager dungeonManager;
	public boolean dropStuff = true;
	public Object[] dungVariables = new Object[10];
	public int dungFishCount;
	public DungeoneeringFishData fishData;
	public boolean dungMultiAttack = false;
	public final TickTimer randomTimer = new TickTimer();
	
	public double prayerReductionMelee = 0.0;
	public double prayerReductionMagic = 0.0;
	public double prayerReductionRanged = 0.0;
	
	private CombatStats combatStats;
	
	private Boss boss;
	
	public boolean isMoving;
	
	public int viewX, viewY;
	
	public int lastX, lastY;
	
	public long singleCombatDelay = 0;
	
	private int walkDistance = 3;
	
	private int aggroRadius = 12;
	
	private boolean attackable = true;
	
	private boolean canWalk = true;
	
	private int retargetTick = 15;
	
	public boolean isSummoningSkillNpc = false;
	
	public boolean isUsingSummonigSpecial;
	
	private boolean isPetNpc = false;
	
	public int hitIcon, hitIcon2, hitMask, hitMask2;
	
	public boolean bossRoomNpc;
	
	private boolean pestControlNPC;
	
	private boolean fightCavesNPC;
	
	public Region currentRegion;
	
	/**
	 * Npc using Protection Prayers; 0 - Melee protect; 1 - Range protect; 2 -
	 * Mage protect;
	 */
	public int protectPrayer = -1;
	
	private boolean usingSoulSplit = false;
	
	private int aliveTick = -1;
	
	public int bandosAvatar = 0;
	
	public int nomad = 0;
	
	public int nomadTele = 0;
	
	public boolean hitByKorasiSpec = false;
	
	/**
	 * attackType: 0 = melee, 1 = range, 2 = mage
	 */
	public int attackType;
	public boolean resetAttackType = true;
	
	public GraphicType endGraphicType = GraphicType.HIGH;
	public int projectileId, endGfx, endGfxDelay, spawnedBy, hitDelayTimer, hitDiff, actionTimer, actionTimer2, enemyX, enemyY;
	public Graphic attackStartGfx;
	public boolean resetsAggro = true;
	public int projectileDelay = 30;
	public int projectiletStartHeight = 43;
	public int projectiletEndHeight = 31;
	public int projectiletSlope = 16;
	public boolean applyDead;
	
	public Animation attackAnim = null;
	
	public boolean needRespawn;
	
	public boolean respawns;
	
	private boolean walkingHome;
	
	public int freezeTimer, underAttackBy;
	
	public boolean stopAttack = false;
	
	public long killedbyNameBase37;
	
	private int killerId = 0;
	
	public int resetAggroTick = 0;
	
	public int instanceId = 0;
	
	public boolean isInstanceNPC = false;
	
	public long lastDamageTaken;
	
	public boolean randomWalk;
	private boolean walksHome = true;
	
	public int transformId = -1;
	
	public int firstAttacker;
	
	public byte stuckCount = 0;
	
	public int specialAttackType = -1;
	
	public boolean alwaysRespawn = false;
	
	private boolean venomousNpc = false;
	
	private boolean safeSpotNPC = true;
	
	private boolean isRevenantNpc = false;
	
	private Chins chin;
	
	/**
	 * Demonic Gorrilas stuff
	 */
	public int currentAttackStyle = -1;
	
	public int dgMissedHits = 0;
	
	public int dgDamageTaken = 0;
	
	/**
	 * Giant Mole Shit
	 */
	public boolean isDigging;
	// end of giant moles shit
	
	/**
	 * end of demonic gorillas
	 */
	
	private int aggroStop;
	
	public int summonedFor;
	
	/********************************************
	 * Allantois summoning and stuff
	 **************************************************/
	
	public int id; // dunno wtf this is for
	
	public boolean currentAttackMulti;
	
	public int underAttackBy2, oldIndexNPC;
	
	/*********************************************
	 * END OF CONSTRUCTION STUFF
	 ******************************************************/
	
	public boolean isAggressive = true;
	
	public boolean isDragon = false;
	
	public boolean walkUpToHit = true;
	
	private boolean retaliates = true;
	
	public int resetAttackNpcTimer;
	
	public int killNpc = -1;
	
	// public Bork borkLeader;
	public int walkingDir;
	
	private int lockMovement;
	
	private TickTimer teleportTick = new TickTimer();
	
	public void startTeleportTimer(int ticks) {
		teleportTick.startTimer(ticks);
	}
	
	public boolean isTeleporting() {
		return !teleportTick.complete();
	}
	
	private boolean dropsItemOnDeath = true;
	
	private boolean clipAttacks = true;
	
	public boolean isClipAttacks() {
		return clipAttacks;
	}
	
	public void setClipAttacks(boolean clip) {
		clipAttacks = clip;
	}
	
	public static final int[][] giantMoleTele = {{1748, 5207}, {1737, 5227}, {1777, 5237},
			{1779, 5210}, {1769, 5200}, {1741, 5189}, {1745, 5170},
			{1739, 5150}, {1756, 5150}, {1760, 5163}, {1770, 5173},
			{1762, 5184},};
	
	public boolean isAttackingAPerson = false, ownerUnderAttackByPlayer = false,
			IsUnderAttackNpc = false, IsAttackingNPC = false,
			changedAttackStyle = false, splash, faceUpdated = false,
			noDeathEmote, caught = false, canTeleport = false;
	
	public boolean didTeleport = false;
	public boolean moveNpcThisTick = false;
	
	public boolean teleporting = false;
	
	public boolean isRunning;
	
	public int teleX, teleY, teleHeight;
	public int teleportDelay = -1;
	public Animation teleEndAnim;
	public Graphic teleEndGfx;
	
	/*********************************************
	 * end of allantois summononing and stuff
	 ****************************************/
	
	public EntityDef def;
	
	/**
	 * Collection of Player instances that have inflicted damage on the non
	 * playable character
	 */
	private Map<Integer, Integer> inflictors = new HashMap<>();
	
	private DistanceRules distanceRules = new DistanceRules(this);
	
	/**
	 * Face
	 **/
	
	public boolean useSmartFollow = false;
	
	public boolean stopFollow = false;
	
	public boolean isPokemon = false;
	public int petScale = 64;
	private boolean canCapture;
	private PokemonData pokemonData;
	
	public boolean canThrowPokeball() {
		return canCapture;
	}
	
	public void setCanThrowPokeball(boolean canCapture) {
		this.canCapture = canCapture;
	}
	
	public void setPetScale(int petScale) {
		this.petScale = petScale;
		this.getUpdateFlags().add(UpdateFlag.PET);
	}

	public int currentX, currentY;
	
	public int hitDiff2 = 0;
	
	public boolean isPoisonous;
	
	private Set<NpcWeakness> weakness = new HashSet<>();
	
	public Set<NpcWeakness> getWeakness() {
		return weakness;
	}
	
	public void setWeakness(Set<NpcWeakness> weakness) {
		this.weakness = weakness;
	}
	
	public int npcAttackBonus;
	
	private boolean barbAssaultNpc = false;
	public boolean barbAssaultRunning = true;
	
	public NPC(int npcSlot, int npcId) {
		super(npcSlot, Location.create(0, 0), false);
		npcIndex = npcSlot;
		npcType = npcId;
		setDead(false);
		applyDead = false;
		actionTimer = 0;
		randomWalk = true;
		killNpc = -1;
		isRunning = false;
		if (npcType == 8349) {
			protectPrayer = Prayers.PROTECT_FROM_MELEE.ordinal();
		}
		this.def = EntityDef.forID(npcType);
		if (def != null)
			setSize(def.getSize());

		updateCenterLocation();
		pokemonData = PokemonData.npcIdToEnum.get(PokemonData.fixId(npcType));
		if (pokemonData != null) {
			canCapture = Misc.random(70) == 1;
		}

		setDataForNewNPC();
		isActive = true;
//		if (Config.RUN_ON_DEDI && !Thread.currentThread().getName().equals("pool-1-thread-1")) {
//			System.out.println("Wrong thread access! Thread:" + Thread.currentThread().getName());
//			for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
//				System.out.println(ste);
//			}
//		}
		onCreate();
	}
	
	private void onCreate() {
		if (npcType == 2998) {
			if (getCurrentAction() == null || !getCurrentAction().isRunning()) {
				startAction(new CycleEvent() {
					
					@Override
					public void execute(CycleEventContainer container) {
						
						if (FPLottery.lotteryFund <= 0) {
							return;
						}
						
						forceChat(String.format("The lottery is currently at %s coins.", Misc.formatNumbersWithCommas(FPLottery.lotteryFund)));
					}
					
					@Override
					public void stop() {
					
					}
				}, Misc.secondsToTicks(60));
			}
		}
	}
	
	public void setDataForNewNPC() {
		setWalkDistance(NPCProjectInsanityStatics.setWalkingDistance(npcType));
		bossRoomNpc = NPCProjectInsanityStatics.bossRoomNpcs(npcType);

		walkUpToHit = NPCProjectInsanityStatics.walkUpForHit(this);
		attackable = def.hasAttackOption();

		// PI aggro shit
		isAggressive = true;
		if (NPCProjectInsanityStatics.setNotAggressive(npcType)) {
			isAggressive = false;
		}

		if (def.combatLevel < 1) {
			isAggressive = false;
		} else {
			if (!attackable) {
				attackable = true;
			}
		}

		if (RSConstants.dzDungeon(getX(), getY())) {
			isAggressive = false;
		}
		
		Server.getTaskScheduler().schedule(() -> { //cheaphax cuz wtf i hate pi shit
			if (isInRevenantDungeon() || WildGodWarsDungeonZone.RECTANGLE.inside(getCurrentLocation())) {
				inMultiZone = true;
				inWildPermanent = true;
			}
		});
		
		if (def.name != null) {
			String lowercase = def.name.toLowerCase();
			if (lowercase.equals("man")
					|| lowercase.equals("guard")
					|| lowercase.equals("hero")
					|| lowercase.equals("knight of ardougne")
					|| lowercase.equals("paladin")) {
				isAggressive = false;
			} else if (lowercase.contains("dragon") || lowercase.contains("hydra") || lowercase.contains("wyrm") || lowercase.contains("wyvern") || lowercase.contains("drake") || lowercase.equalsIgnoreCase("vorkath") || npcType == GreatOlmInstanceHandler.HEAD_NPC_ID) {
				isDragon = true;
			}
			
			this.setNpcName(def.getName());
		}
		
		//set followdistance
		getDistanceRules().setFollowDistance(NPCProjectInsanityStatics.followDistance(this));
		NPCCombatData.setCombatDistanceRequired(this);
		setSafeSpotNPC(NPCCombatData.setAsSafeSpotNPC(npcType));
		NPCHandler.setAggroShit(this);
		setRetaliates(NPCCombatData.retaliates(npcType));
		//setRevenantNpc(NPCProjectInsanityStatics.isRevenantSwitchStatement(npcType));
		NPCProjectInsanityStatics.setRevenantStats(this);
		setStats(npcType, false);
		updateFlags.add(UpdateFlag.PET);
	}

	private void setStats(int id, boolean setHp) {
		// load npc stats from class
		combatStats = CombatStats.createDefaultStats();

		NPCDefinitions npcDef = NPCDefinitions.getDefinitions()[id];
		if (npcDef != null) {
			CombatStats statsNew = npcDef.getStats();
			if (statsNew != null) {
				combatStats.copy(statsNew);
			}

			if (combatStats.getAttackSpeed() == 0) {
				combatStats.setAttackSpeed(NPCHandler.getNpcDelay(id));
			}

			int currentLp = getSkills().getLifepoints();

			Skills newSkills = npcDef.getSkills();
			if (newSkills != null) {
				getSkills().copySkills(newSkills);
			}

			setVenomImmunity(npcDef.isVenomImmune());
			setPoisonImmunity(npcDef.isPoisonImmune());

			int hp = npcDef.getNpcHealth();
			if (hp > 0) {
				if (WorldType.isSeasonal()) {
					double mod = 2.0d;
					getSkills().setMaximumLifepoints((int) (hp * mod));
				} else {
					getSkills().setMaximumLifepoints(hp);
				}
			}

			if (setHp) {
				getSkills().setLifepoints(currentLp);
			}

			maxHit = npcDef.getMaxHit();
		} else {
			getSkills().setStaticLevel(Skills.MAGIC, NpcStatsHardCoded.getMagicLevel(id));
			combatStats.setMagicDefBonus(NpcStatsHardCoded.getMagicDefence(id));
			combatStats.setAttackSpeed(NPCHandler.getNpcDelay(id));
		}
	}

	public void setCustomName(String name) {
		this.customName = name;
		updateFlags.add(UpdateFlag.NAME_CHANGE);
	}
	
	public void applyBoss(boolean pet) {
		Boss boss = setOrGetBoss();
		if (boss != null) {
			boss.onSpawn(this);
			if (!pet) {
				getDistanceRules().setFollowDistance(boss.followDistance());
				setCanWalk(boss.canMove());
			}
		}
	}
	
	public Boss setOrGetBoss() {
		if (isPokemon)
			return null;
		if (boss == null) {
			Class<? extends Boss> clazz = Manager.get(npcType);
			if (clazz != null) {
				try {
					Constructor con = clazz.getConstructor(new Class[]{NPC.class});
					boss = (Boss) con.newInstance(this);
					boss.onInit();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
		return boss;
	}
	
	public void walkTo(int moveX, int moveY) { // new walking method, more clean
		this.moveX = moveX;
		this.moveY = moveY;
	}
	
	public void walkToLocation(Location destination) {
		if (!canWalk())
			return;
		this.moveX = NPCHandler.GetMove(absX, destination.getX());
		this.moveY = NPCHandler.GetMove(absY, destination.getY());
	}
	
	public boolean destinationReached(Location destination) {
		return absX == destination.getX() && absY == destination.getY();
	}
	
	public void lockMovement(int ticks) {
		lockMovement = ticks;
	}
	
	public void resetFollow() {
		resetFace();
		if (walkingType == 1) { // random walk i guess?
			randomWalk = true;
		}
	}
	
	public void addHitToQueue(final int damage, final int mask,
	                          final int icon) {
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			
			@Override
			public void execute(CycleEventContainer container) {
				if (this == null || isDead() || getSkills().getLifepoints() < 1) {
					container.stop();
					return;
				}
				if (!updateFlags.contains(UpdateFlag.HIT)) {
					hitDiff = damage;
					hitMask = mask;
					hitIcon = icon;
					updateFlags.add(UpdateFlag.HIT);
					container.stop();
				} else if (!updateFlags.contains(UpdateFlag.HIT2)) {
					hitDiff2 = damage;
					hitMask2 = mask;
					hitIcon2 = icon;
					updateFlags.add(UpdateFlag.HIT2);
					container.stop();
				}
				
			}
			
			@Override
			public void stop() {
			
			}
			
		}, 1);
	}
	
	public void addInfliction(int mySqlId, int damage) {
		if (!inflictors.containsKey(mySqlId)) {
			inflictors.put(mySqlId, damage);
		} else {
			inflictors.put(mySqlId, inflictors.get(mySqlId) + damage);
		}
	}
	
	/**
	 * Adds this entity to the specified region.
	 *
	 * @param region The region.
	 */
	public void addToRegion(Region region) {
		region.addNpc(this);
	}
	
	public void afterAttack(Player p, int damage) {
		Boss boss = setOrGetBoss();
		if (boss != null) {
			boss.afterNpcAttack(this, p, damage);
		}
	}
	
	public boolean canWalk() {
		if (isShoved() || isStunned() || getFreezeTimer() > 0)
			return false;
		return canWalk;
	}
	
	private CycleEventContainer currentAction;
	
	public void startAction(CycleEvent action, int cycles) {
		this.currentAction = CycleEventHandler.getSingleton().addEvent(this, action, cycles);
		this.currentAction.execute();
	}
	
	public CycleEventContainer getCurrentAction() {
		return currentAction;
	}
	
	@Override
	public void clearUpdateFlags() {
		updateFlags.clear();
		animations.clear();
		graphics.clear();
		forcedChat = "";
		
		facingLocation = null;
		movedX = movedY = -1;

//		if (transformId == npcType) {
//		    transformId = -1;
//		}
		
		didTeleport = false;
	}
	
	public boolean takeDamage = true;
	
	@Override
	public int dealDamage(Hit hit) {
		if (getSkills().getLifepoints() < 1) {
			return 0;
		}
		
		if (!takeDamage)
			return 0;
		
		if (canThrowPokeball() || npcType == TamingQuestHandler.TAMING_GOBLIN) {
			Client c = (Client) PlayerHandler.getPlayerByMID(getKillerBySQLIndex());
			if (c != null && c.isActive) {
				if (!c.isPokemonUnlocked(pokemonData) && ((c.getItems().playerHasItem(Capture.tameRope) || c.getItems().playerHasItem(Capture.strongTameRope)) && QuestHandlerTemp.completedQuest(c, QuestIndex.POKEMON_ADVENTURES) || (c.getItems().playerHasItem(Capture.questTameRope) && npcType == TamingQuestHandler.TAMING_GOBLIN))) {
					int toHit = hit.getDamage();
					int hp = getSkills().getLifepoints() - toHit;
					int hpCap = 1;
					boolean canCapture = false;
					
					if (hp < hpCap) {
						canCapture = true;
						hit.setDamage(getSkills().getLifepoints() - hpCap);
					}
					
					if (canCapture) {
						Capture.capturePet(c, this, pokemonData);
					}
				}
			}
		}
		
		Boss boss = setOrGetBoss();
		
		if (boss != null) {
			boss.proxyDamage(this, hit, hit.getSource());
		}
		
		getSkills().hit(hit.getDamage());//hit.setDamage(getSkills().hit(hit.getDamage()));
		
		handleHitMask(hit.getDamage(), hit.getHitSplat(), hit.getHitIcon());
		return hit.getDamage();
	}
	
	/**
	 * Destroys this entity from the region. THIS SHOULD ONLY BE CALLED FROM NPCHANDLER.PROCESS()
	 */
	public void npcHandlerProcessDestroy() {
		removeNpcClip();
		if (currentRegion != null) {
			removeFromRegion(currentRegion);
		}
		CycleEventHandler.getSingleton().stopEvents(this);
		isActive = false;
		needRespawn = true;

		Player spawnedByPlayer = getSpawnedByPlayer();
		if (spawnedByPlayer != null && spawnedByPlayer.getSeasonalData().spawned == this) {
			spawnedByPlayer.getSeasonalData().spawned = null;
		}

		Boss boss = setOrGetBoss();
		
		if (boss != null) {
			boss.onDestroy(this);
		}
	}
	
	/**
	 * Instantly removes npc from the game.
	 */
	public void removeNpcSafe(boolean instantClipRemove) {
		this.setDead(true);
		this.needRespawn = true;
		if (instantClipRemove) {
			removeNpcClip();
		}
	}
	
	public void removeNpcClip() {
		for (int i = 0; i < getSize(); i++) {
			int x = getX() + i;
			for (int j = 0; j < getSize(); j++) {
				int y = getY() + j;
				RegionClip.removeNpcTile(x, y, getZ(), getDynamicRegionClip());
				if (isBarricadeTypeNpc()) {
					RegionClip.setBarricadeStatus(x, y, getZ(), false, getDynamicRegionClip());
				}
			}
		}
	}
	
	public void die() {
		setDead(true);
		applyDead = false;
		actionTimer = 0;
	}
	
	public Attack doSpecial(Player p) {
		return null;
	}
	
	public void moveNpc(Location loc) {
		moveNpc(loc.getX(), loc.getY(), loc.getZ());
	}
	
	public void moveNpc(int x, int y, int z) {
//		this.teleX = x;
//		this.teleY = y;
//		this.teleHeight = z;
//		this.teleportDelay = 0;
		NPCHandler.trueTeleport(this, x, y, z);
	}

	@Override
	public void face(Mob mob) {
		if (dontFace) {
			return;
		}

		super.face(mob);
	}

	public int getAliveTick() {
		return aliveTick;
	}
	
	public Player[] getAttackablePlayers(Player[] toPick) {
		return toPick;
	}
	
	public int getDistance(int x1, int x2, int y1, int y2) {
		return (int) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}
	
	public EntityDef getEntityDef() {
		return def;
	}
	
	public int getHeightLevel() {
		return heightLevel;
	}
	
	public Map<Integer, Integer> getInflictors() {
		return inflictors;
	}
	
	public int getKillerBySQLIndex() {
		int highestDamage = 0;
		int mySqlId = 0;

		for (Entry<Integer, Integer> entry : inflictors.entrySet()) {
			int damage = entry.getValue();
			if (damage > highestDamage) {
				highestDamage = damage;
				mySqlId = entry.getKey();
			}
		}

		if (mySqlId == 0) {
			if (PlayerHandler.players[getKillerId()] != null) {
				return PlayerHandler.players[getKillerId()].mySQLIndex;
			}
		}

		return mySqlId;// return p != null ? p.playerId : 0;
	}
	
	// temp function
	public boolean npcCanMove() {
		if (lockMovement > 0 && !isShoved()) {
			lockMovement--;
			return false;
		}
		if (freezeTimer > 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * @return the npcIndex
	 */
	public int getNpcIndex() {
		return npcIndex;
	}
	
	public String getNpcName() {
		return npcName;
	}
	
	public int getRetargetTick() {
		return retargetTick;
	}
	
	public int getSpecialChance() {
		return -1;
	}
	
	public int getX() {
		return absX;
	}
	
	/*
	 * public int getNextWalkingDirection() { if (wQueueReadPtr ==
	 * wQueueWritePtr) return -1; int dir; if(moveX > 0 || moveY > 0) { dir =
	 * Misc.direction(absX, absY, (absX + moveX), (absY + moveY)); if (dir ==
	 * -1) return -1; dir >>= 1; absX += moveX; absY += moveY; return dir; } do
	 * { dir = Misc.direction(currentX, currentY, walkingQueueX[wQueueReadPtr],
	 * walkingQueueY[wQueueReadPtr]); if (dir == -1) { wQueueReadPtr =
	 * (wQueueReadPtr + 1) % walkingQueueSize; } else if ((dir & 1) != 0) {
	 * System.out.println("NPC.java Invalid waypoint in walking queue!");
	 * resetWalkingQueue(); return -1; } } while ((dir == -1) && (wQueueReadPtr
	 * != wQueueWritePtr)); if (dir == -1) return -1; dir >>= 1;
	 * if(!PathFinder.canMove(absX, absY, heightLevel,
	 * Misc.directionDeltaX[dir], Misc.directionDeltaY[dir])) {
	 * resetWalkingQueue(); return -1; } lastX = absX; lastY = absY; currentX +=
	 * Misc.directionDeltaX[dir]; currentY += Misc.directionDeltaY[dir]; absX +=
	 * Misc.directionDeltaX[dir]; absY += Misc.directionDeltaY[dir];
	 * setLocation(absX, absY); return dir; }
	 */
	
	public int getY() {
		return absY;
	}
	
	public int getZ() {
		return heightLevel;
	}
	
	// public void getNextNPCMovement(int i) {
	// direction = -1;
	// if (freezeTimer == 0) {
	// direction = getNextWalkingDirection();
	// if (direction > -1)
	// updateRequired = true;
	// }
	// }
	
	/*
	 * // public int dir1 = -1, dir2 = -1; public int mapRegionX, mapRegionY;
	 * public boolean isRunning; public boolean mapRegionDidChange = false;
	 * public void getNextNPCMovement() { mapRegionDidChange = false; direction
	 * = -1; direction = getNextWalkingDirection(); if (direction == -1) return;
	 * // if (isRunning) { // dir2 = getNextWalkingDirection(); // } int deltaX
	 * = 0, deltaY = 0; if (currentX < 2 * 8) { deltaX = 4 * 8; mapRegionX -= 4;
	 * mapRegionDidChange = true; } else if (currentX >= 11 * 8) { deltaX = -4 *
	 * 8; mapRegionX += 4; mapRegionDidChange = true; } if (currentY < 2 * 8) {
	 * deltaY = 4 * 8; mapRegionY -= 4; mapRegionDidChange = true; } else if
	 * (currentY >= 11 * 8) { deltaY = -4 * 8; mapRegionY += 4;
	 * mapRegionDidChange = true; } if (mapRegionDidChange) { currentX +=
	 * deltaX; currentY += deltaY; for (int i = 0; i < walkingQueueSize; i++) {
	 * walkingQueueX[i] += deltaX; walkingQueueY[i] += deltaY; } } //
	 * CoordAssistant.processCoords(this); } private int newWalkCmdX[] = new
	 * int[walkingQueueSize]; private int newWalkCmdY[] = new
	 * int[walkingQueueSize]; public int newWalkCmdSteps = 0; // private boolean
	 * newWalkCmdIsRunning = false; protected int travelBackX[] = new
	 * int[walkingQueueSize]; protected int travelBackY[] = new
	 * int[walkingQueueSize]; protected int numTravelBackSteps = 0; public void
	 * preProcessing() { newWalkCmdSteps = 0; } public synchronized void
	 * postProcessing() { if (newWalkCmdSteps > 0) { int firstX =
	 * getNewWalkCmdX()[0], firstY = getNewWalkCmdY()[0]; int lastDir = 0;
	 * boolean found = false; numTravelBackSteps = 0; int ptr = wQueueReadPtr;
	 * int dir = Misc.direction(currentX, currentY, firstX, firstY); if (dir !=
	 * -1 && (dir & 1) != 0) { do { lastDir = dir; if (--ptr < 0) ptr =
	 * walkingQueueSize - 1; travelBackX[numTravelBackSteps] =
	 * walkingQueueX[ptr]; travelBackY[numTravelBackSteps++] =
	 * walkingQueueY[ptr]; dir = Misc.direction(walkingQueueX[ptr],
	 * walkingQueueY[ptr], firstX, firstY); if (lastDir != dir) { found = true;
	 * break; } } while (ptr != wQueueWritePtr); } else found = true; if
	 * (!found) System.out.
	 * println("NPC.java Fatal: couldn't find connection vertex! Dropping packet."
	 * ); else { wQueueWritePtr = wQueueReadPtr; addToWalkingQueue(currentX,
	 * currentY); if (dir != -1 && (dir & 1) != 0) { for (int i = 0; i <
	 * numTravelBackSteps - 1; i++) { addToWalkingQueue(travelBackX[i],
	 * travelBackY[i]); } int wayPointX2 = travelBackX[numTravelBackSteps - 1],
	 * wayPointY2 = travelBackY[numTravelBackSteps - 1]; int wayPointX1,
	 * wayPointY1; if (numTravelBackSteps == 1) { wayPointX1 = currentX;
	 * wayPointY1 = currentY; } else { wayPointX1 =
	 * travelBackX[numTravelBackSteps - 2]; wayPointY1 =
	 * travelBackY[numTravelBackSteps - 2]; } dir = Misc.direction(wayPointX1,
	 * wayPointY1, wayPointX2, wayPointY2); if (dir == -1 || (dir & 1) != 0) {
	 * System.out.println("NPC.java Fatal: The walking queue is corrupt! wp1=("
	 * + wayPointX1 + ", " + wayPointY1 + "), " + "wp2=(" + wayPointX2 + ", " +
	 * wayPointY2 + ")"); } else { dir >>= 1; found = false; int x = wayPointX1,
	 * y = wayPointY1; while (x != wayPointX2 || y != wayPointY2) { x +=
	 * Misc.directionDeltaX[dir]; y += Misc.directionDeltaY[dir]; if
	 * ((Misc.direction(x, y, firstX, firstY) & 1) == 0) { found = true; break;
	 * } } if (!found) { System.out.
	 * println("NPC.java Fatal: Internal error: unable to determine connection vertex!"
	 * + "  wp1=(" + wayPointX1 + ", " + wayPointY1 + "), wp2=(" + wayPointX2 +
	 * ", " + wayPointY2 + "), " + "first=(" + firstX + ", " + firstY + ")"); }
	 * else addToWalkingQueue(wayPointX1, wayPointY1); } } else { for (int i =
	 * 0; i < numTravelBackSteps; i++) { addToWalkingQueue(travelBackX[i],
	 * travelBackY[i]); } } for (int i = 0; i < newWalkCmdSteps; i++) {
	 * addToWalkingQueue(getNewWalkCmdX()[i], getNewWalkCmdY()[i]); } }
	 * //isRunning = isNewWalkCmdIsRunning() || isRunning2; } } public void
	 * setNewWalkCmdX(int newWalkCmdX[]) { this.newWalkCmdX = newWalkCmdX; }
	 * public int[] getNewWalkCmdX() { return newWalkCmdX; } public void
	 * setNewWalkCmdY(int newWalkCmdY[]) { this.newWalkCmdY = newWalkCmdY; }
	 * public int[] getNewWalkCmdY() { return newWalkCmdY; }
	 */
	
	// public void appendHitUpdate(Stream str) {
	// if (getSkills().getLifepoints() <= 0) {
	// isDead = true;
	// }
	// str.writeByteC(hitDiff);
	// if (hitDiff > 0) {
	// str.writeByteS(1);
	// } else {
	// str.writeByteS(0);
	// }
	// /*
	// * str.writeByteS(getSkills().getLifepoints()); str.writeByteC(MaxHP);
	// */
	// str.writeByteS(Misc.getCurrentHP(getSkills().getLifepoints(), MaxHP, 100));
	// str.writeByteC(100);
	// }
	
	/**
	 * gets the distance from the current npc to a certain object. Includes
	 * npc's size
	 *
	 * @param objectX2
	 * @param objectY2
	 * @param distance
	 * @return true if good distance.
	 */
	public boolean goodDistance(int objectX2, int objectY2, int distance) {
		int size = getSize();
		for (int sizeX = 0; sizeX < size; sizeX++) {
			for (int sizeY = 0; sizeY < size; sizeY++) {
				if (size > 1) {
					if (sizeX > 0 && sizeX < size) {
						sizeX = size;
					}
					if (sizeY > 0 && sizeY < size) {
						sizeY = size;
					}
				}
				if (getDistance(absX + sizeY, objectX2, absY + sizeY,
						objectY2) <= distance) {
					return true;
				} else {
					continue;
				}
			}
		}
		return false;
	}
	
	public boolean goodDistance(int objectX, int objectY, int objectX2,
	                            int objectY2, int distance) {
		return getDistance(objectX, objectX2, objectY, objectY2) <= distance;
	}
	
	public boolean goodDistance(NPC npc, int objectX2, int objectY2,
	                            int distance) {
		for (int sX = 0; sX < npc.getSize(); sX++) {
			for (int sY = 0; sY < npc.getSize(); sY++) {
				if (getDistance(npc.absX + sX, objectX2, npc.absY + sY,
						objectY2) <= distance) {
					return true;
				}
			}
		}
		return false;
	}
	
	// public void appendHitUpdate2(Stream str) {
	// if (getSkills().getLifepoints() <= 0) {
	// isDead = true;
	// }
	// str.writeByteA(hitDiff2);
	// if (hitDiff2 > 0) {
	// str.writeByteC(1);
	// } else {
	// str.writeByteC(0);
	// }
	// str.writeByteA(getSkills().getLifepoints());
	// str.writeByte(MaxHP);
	// }
	
	public boolean goodDistance(NPC npc1, NPC npc2, int distance) {
		int size1 = npc1.getSize();
		int size2 = npc2.getSize();
		for (int sizeX = 0; sizeX < size1; sizeX++) {
			for (int sizeY = 0; sizeY < size1; sizeY++) {
				for (int sizeX2 = 0; sizeX2 < size2; sizeX2++) {
					for (int sizeY2 = 0; sizeY2 < size2; sizeY2++) {
						if (size1 > 1) {
							if (sizeX > 0 && sizeX < size1) {
								sizeX = size1;
							}
							if (sizeY > 0 && sizeY < size1) {
								sizeY = size1;
							}
						}
						if (getDistance(npc1.getX() + sizeX,
								npc2.getX() + sizeX2, npc1.getY() + sizeY,
								npc2.getY() + sizeY2) <= distance) {
							return true;
						} else {
							continue;
						}
					}
				}
			}
		}
		return false;
	}
	
	public void handleDGDamageTaken(int damage, int hitType) {
		dgDamageTaken += damage;
		if (dgDamageTaken > 45) {
			transformGorilla(hitType);
			dgDamageTaken = 0;
		}
	}
	
	/**
	 * displays hitmarks
	 *
	 * @param damage - damage to display
	 * @param mask:  0 - red box 2 - green box
	 * @param icon:  -1:nothing 0 - melee 1 - range 2 - magic 3 - recoil 4 - dwarf
	 *               multicannon
	 */
	public void handleHitMask(int damage, int mask, int icon) {
		// if (damage > getSkills().getLifepoints()) {
		// damage = getSkills().getLifepoints();
		// }
		if (!updateFlags.contains(UpdateFlag.HIT)) {
			hitDiff = damage;
			hitMask = mask;
			hitIcon = icon;
			updateFlags.add(UpdateFlag.HIT);
		} else if (!updateFlags.contains(UpdateFlag.HIT2)) {
			hitDiff2 = damage;
			hitMask2 = mask;
			hitIcon2 = icon;
			updateFlags.add(UpdateFlag.HIT2);
		} else {
			addHitToQueue(damage, mask, icon);
		}
		if (damage > 0) {
			if (isPestControlNPC()) {
				PestControl.updateNpcHP(this);
			}
			if (npcType == 7151) { // demonic gorillas
				handleDGDamageTaken(damage, icon);
			}
		}
	}
	
	public void healNpc(int amount) {
		healNpc(amount, 1, -1);
	}
	
	public void healNpc(int amount, int mask, int icon) {
		
		if (amount < 0) {
			return;
		}
		
		getSkills().heal(amount);
		
		this.handleHitMask(amount, mask, icon);
	}
	
	/******************************************
	 * CONSTRUCTION STUFF
	 *****************************************************************/
	
	public boolean inMulti() {
		if (dungeonManager != null || tobManager != null) {
			return true;
		}

		if (insideInstance()) {
			return getInstanceManager().isMulti();
		}

		if (boss != null) {
			return boss.isMultiCombat();
		}
		
		if (inMultiZone)
			return true;
		
		return RSConstants.inMulti(absX, absY);
	}
	
	
	public boolean isInRevenantDungeon() {
		return RSConstants.isInRevenantDungeon(absX, absY);
	}
	
	public boolean inWild() {
		if (absX > 2941 && absX < 3392 && absY > 3518 && absY < 3966
				|| absX > 2941 && absX < 3392 && absY > 9918 && absY < 10366) {
			return true;
		}
		if (inWildPermanent)
			return true;
		return false;
	}
	
	public boolean isAttackable() {
		return attackable;
	}
	
	public boolean isPetNpc() {
		return isPetNpc;
	}
	
	public boolean isUsingSoulSplit() {
		return usingSoulSplit;
	}
	
	public void npcTeleport(int x, int y, int h) {
		if (!canTeleport) {
			return;
		}
		didTeleport = true;
		teleX = x;
		teleY = y;
		teleHeight = h;
		teleportDelay = 0;
	}

	public void moveNpcNow(Location location) {
		moveNpcNow(location.getX(), location.getY(), location.getZ());
	}

	public void moveNpcNow(int x, int y, int z) {
		setLastLocation(Location.create(getX(), getY(), getZ()));
		absX = x;
		absY = y;
		heightLevel = z;
		setLocation(absX, absY);
	}
	
	public void teleport(final Location loc, final TeleportType type) {
		teleport(loc, 0, type);
	}
	
	public void teleport(final Location loc, final int customTeleDelay, final TeleportType type) {
		
		stopAttack = true;
		attackable = false;
		
		final int delay = customTeleDelay > 0 ? customTeleDelay : type.getEndDelay();
		
		final int walkDelay = customTeleDelay > 0 ? customTeleDelay + 5 : type.getTimer();
		
		resetWalkTemp();
		startTeleportTimer(walkDelay);
		
		if (type.getStartGfx() != null)
			startGraphic(type.getStartGfx());
		if (type.getStartAnimation() != null)
			startAnimation(type.getStartAnimation());
		
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			int timer = 0;
			
			@Override
			public void stop() {
				setCanWalk(true);
				stopAttack = false;
				attackable = true;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (timer == delay - 1) {
					moveNpcNow(loc.getX(), loc.getY(), loc.getZ());
					moveNpcThisTick = true;
				}
				if (timer >= delay) {
					container.stop();
					
					if (type.getEndGfx() != null)
						startGraphic(type.getEndGfx());
					if (type.getEndAnimation() != null)
						startAnimation(type.getEndAnimation());
				}
				
				timer++;
			}
		}, 1);
	}
	
	public void nullNPC() {
		setDead(true);
		applyDead = true;
		actionTimer = 0;
		setDropsItemOnDeath(false);
		
		Boss boss = setOrGetBoss();
		if (boss != null) {
			boss.onNull(this);
		}
	}
	
	public void onDamageApplied(Player p, int damage) {
		if (damage > 0 && isAttackable()) {
			if (p.vengOn) {
				p.getCombat().appendVengeanceNPC(this, damage);
			}
			MeleeExtras.applyRecoilMob(p, damage, this);
		}
		
		p.putInCombatWithNpc(getId());
		NPCHandler.handleSpecialEffects(p, this, damage);
		afterAttack(p, damage);
	}

	public void onDeath() {
		getDotManager().reset();
	}

	public void afterNpcDeath() {
		if (insideInstance()) {
			getInstanceManager().npcDeath(this);
		}
	}

	public void onHitted(Player p, int damage) {
	}
	
	public void onSpawn() {
	}
	
	//PLUS FROM THE NPC X and Y to get the new middle coordinate of the npc
	public int getXtoPlayerLoc(int pX) {
		int size = getSize();
		if (size <= 1)
			return getX();
		else
			return Math.max(getX(), Math.min(getX() + size - 1, pX));
	}
	
	public int getYtoPlayerLoc(int pY) {
		int size = getSize();
		if (size <= 1)
			return getY();
		else
			return Math.max(getY(), Math.min(getY() + size - 1, pY));
	}
	
	public int getXYtoPlayerLoc(int pXY, boolean checkX) {
		final int npcXY = checkX ? getX() : getY();
		if (pXY < npcXY)
			return npcXY;
		final int otherSide = npcXY + getSize() - 1;
		if (pXY >= npcXY && pXY <= otherSide)
			return pXY;
		return otherSide;
	}
	
	// offset of distance to hit npc depending on npc size
	public int getHitOffset() {
		if (getSize() == 5)
			return 2;
		else if (getSize() == 3 || getSize() == 4)
			return 1;
		else
			return 0;
	}
	
	public int getXYtoPlayerXY(int playerXY, int npcXY) {
		final int size = getSize();
		if (getSize() <= 1)
			return npcXY;
		else
			return Math.max(npcXY, Math.min(npcXY + (size - 1), playerXY));
	}
	
	/**
	 * Removes this entity from the specified region.
	 *
	 * @param region The region.
	 */
	public void removeFromRegion(Region region) {
		region.removeNpc(this);
		Server.getRegionManager().checkEmptyRegion(region);
	}
	
	public void removeInflictor(int mySqlId) {
		if (inflictors.containsKey(mySqlId)) {
			inflictors.remove(mySqlId);
		}
	}
	
	public void transform(int Id) {
		if (transformId == Id) {
			return;
		}

		transformId = Id;
		updateFlags.add(UpdateFlag.TRANSFORM);

		int definitionId;
		if (transformId == -1) {
			definitionId = npcType;
		} else {
			definitionId = transformId;
		}

		this.def = EntityDef.forID(definitionId);
		if (def != null) {
			setSize(def.getSize());
			attackable = def.hasAttackOption();
		}

		setStats(definitionId, true);
	}

	public void resetWalkTemp() {
		moveX = moveY = 0;
		getWalkingQueue().reset();
	}
	
	/*
	 * public NPCDefinition getDefinition() { return definition; }
	 */
	
	public void setAliveTick(int aliveTick) {
		this.aliveTick = aliveTick;
	}
	
	public void setAttackable(boolean attackable) {
		this.attackable = attackable;
		this.takeDamage = attackable;
	}
	
	public void setCanWalk(boolean walk) {
		canWalk = walk;
	}
	
	public void setHP(int setHP) {
		getSkills().setLifepoints(setHP);
		if (getSkills().getLifepoints() < 0) {
			// calculations
			getSkills().setLifepoints(0);
		}
		if (getSkills().getLifepoints() > getSkills().getMaximumLifepoints()) {
			getSkills().setLifepoints(getSkills().getMaximumLifepoints());
		}
	}
	
	/**
	 * Sets the current location.
	 */
	public void setLocation(int x, int y) {
		// this.absX = x;
		// this.absY = y;

		int size = getSize();
		Location lastLocation = getLastLocation();
		for (int i = 0; i < size; i++) {
			int lastX = lastLocation.getX() + i;
			int newX = x + i;
			for (int j = 0; j < size; j++) {
				int lastY = lastLocation.getY() + j;
				int newY = y + j;
				RegionClip.removeNpcTile(lastX, lastY, lastLocation.getZ(), getDynamicRegionClip());
				RegionClip.addNpcTile(newX, newY, heightLevel, getDynamicRegionClip());
				if (isBarricadeTypeNpc()) {
					RegionClip.setBarricadeStatus(lastX, lastY, lastLocation.getZ(), false, getDynamicRegionClip());
					RegionClip.setBarricadeStatus(newX, newY, heightLevel, true, getDynamicRegionClip());
				}
			}
		}

		//this is repeated in case old code is used
		getCurrentLocation().setX(x);
		getCurrentLocation().setY(y);
		getCurrentLocation().setZ(heightLevel);
		updateCenterLocation();
		//setCurrentLocation(Location.create(x, y, heightLevel));

		Region newRegion = Server.getRegionManager().getRegionByLocation(x, y, heightLevel);
		if (newRegion != currentRegion) {
			if (currentRegion != null) {
				removeFromRegion(currentRegion);
			}
			currentRegion = newRegion;
			addToRegion(currentRegion);
		}
	}
	
	public void setNpcAsPet(boolean setNpcPet) {
		isPetNpc = setNpcPet;
	}
	
	public void setNpcName(String npcName) {
		this.npcName = npcName;
	}
	
	public void setRetargetTick(int retargetTick) {
		this.retargetTick = retargetTick;
	}
	
	public void setUsingSoulSplit(boolean usingSoulSplit) {
		this.usingSoulSplit = usingSoulSplit;
	}
	
	public void teleportToPlayer(final Player p, final int gfxId) {
		teleporting = true;
		if (gfxId > 0)
			startGraphic(Graphic.create(gfxId));
		moveX = 0;
		moveY = 0;
		final int pX = p.getX();
		final int pY = p.getY();
		
		final NPC npc = this;
		
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			
			@Override
			public void execute(CycleEventContainer container) {
				if (this == null || isDead()) {
					container.stop();
					return;
				}
				
				int spawnX = pX, spawnY = pY;
				boolean breaking = false;
				for (int x = 0; x < 2; x++) {
					if (breaking) {
						break;
					}
					for (int y = 0; y < 2; y++) {
						if (x == 0 && y == 0) {
							continue;
						}
						if (PathFinder.canMove(pX, pY, 0, x, y, p.getDynamicRegionClip())) {
							spawnX = pX + x;
							spawnY = pY + y;
							breaking = true;
							break;
						}
					}
				}
				
				NPCHandler.trueTeleport(npc, spawnX, spawnY, 0);
				didTeleport = true;
				if (gfxId > 0)
					startGraphic(Graphic.create(gfxId));
				container.stop();
				
			}
			
			@Override
			public void stop() {
				if (this != null) {
					teleporting = false;
				}
			}
		}, 2);
	}
	
	public void transformGorilla(int prayerType) {
//		protectPrayer = prayerType;
		switch (prayerType) {
			case 0:// melee
				transform(7152);
				protectPrayer = Prayers.PROTECT_FROM_MELEE.ordinal();
				break;
			case 1:// range
				transform(7153);
				protectPrayer = Prayers.PROTECT_FROM_MISSILES.ordinal();
				break;
			case 2:// mage
				transform(7154);
				protectPrayer = Prayers.PROTECT_FROM_MAGIC.ordinal();
				break;
		}
	}
	
	public void turnNpc(int x, int y) {
		faceLocation(x, y);
	}
	
	public boolean walksHome() {
		return walksHome;
	}
	
	public void setWalksHome(boolean walksHome) {
		this.walksHome = walksHome;
	}
	
	public boolean withinDistance(int absX, int getY) {
		int deltaX = this.getX() - absX, deltaY = this.getY() - getY;
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}
	
	public boolean withinDistance2(int absX, int getY) {
		int deltaX = this.getX() - absX, deltaY = this.getY() - getY;
		int offset = getSize();
		return deltaX <= offset && deltaX >= -offset && deltaY <= offset && deltaY >= -offset;
	}
	
	public boolean withinDistance(NPC npc) {
		if (heightLevel != npc.heightLevel) {
			return false;
		}
		if (npc.needRespawn == true) {
			return false;
		}
		int deltaX = npc.absX - absX, deltaY = npc.absY - absY;
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}
	
	public boolean withinDistance(Player otherPlr) {
		if (heightLevel != otherPlr.heightLevel) {
			return false;
		}
		int deltaX = otherPlr.absX - absX, deltaY = otherPlr.absY - absY;
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}
	
	/**
	 * If npc alwaysRespawns == true then process() will be called even when npc is no longer visible, gotta use isDead() check to avoid that
	 */
	@Override
	public void process() {
		super.process();
	}

	@Override
	public void update() {
		
	}
	
	public boolean isBarbAssaultNpc() {
		return barbAssaultNpc;
	}
	
	public void setBarbAssaultNpc(boolean barbAssaultNpc) {
		this.barbAssaultNpc = barbAssaultNpc;
		if (barbAssaultNpc) {
			setAggroRadius(64);
		}
	}
	
	public int getProjectileLockon() {
		return getId() + 1;
	}
	
	public boolean isVenomousNpc() {
		return venomousNpc;
	}
	
	public void setVenomousNpc(boolean venomousNpc) {
		this.venomousNpc = venomousNpc;
	}

	public CombatStats getCombatStats() {
		return combatStats;
	}

	@Override
	public boolean usingProtectPrayer(boolean usingCurses, int prayerType) {
		Boss boss = setOrGetBoss();
		if (boss != null) {
			Prayers prayer = boss.protectPrayer(getNpcTypeSmart());
			if (prayer == null) {
				return false;
			}
			
			return prayer.ordinal() == prayerType;
		}
		
		return protectPrayer == prayerType;
	}
	
	@Override
	public int protectPrayerDamageReduction(int currentHit) {
		Boss boss = setOrGetBoss();
		if (boss != null) {
			int damage = boss.protectPrayerDamageReduction(currentHit);
			if (damage != -1) {
				return damage;
			}
		}
		
		return currentHit * 60 / 100;
	}
	
	public void stunEntity(final int time, Graphic gfx) {
		if (isStunned()) {
			return;
		}
		final NPC n = this;
		n.resetWalkTemp();
		if (gfx != null)
			startGraphic(gfx);
		setStunned(true);
		resetAttack();
		Server.getTaskScheduler().schedule(new Task(true) {
			
			int ticks = time;
			
			@Override
			protected void execute() {
				if (n == null || n.isDead()) {
					this.stop();
					return;
				}
				if (ticks < 0) {
					n.setStunned(false);
					this.stop();
					return;
				}
				ticks--;
			}
		});
	}
	
	public void rootEntity(final int time) {
		if (lockMovement > 0) {
			return;
		}
		resetWalkTemp();
		lockMovement(time);
	}
	
	public void resetAttack() {
		setKillerId(0);
		killNpc = -1;
	}
	
	public boolean isPestControlNPC() {
		return pestControlNPC;
	}
	
	public void setPestControlNPC(boolean pestControlNPC) {
		this.pestControlNPC = pestControlNPC;
	}
	
	public boolean isFightCavesNPC() {
		return fightCavesNPC;
	}
	
	public void setFightCavesNPC(boolean fightCavesNPC) {
		this.fightCavesNPC = fightCavesNPC;
	}
	
	public int getAggroStop() {
		return aggroStop;
	}
	
	public void setAggroStop(int aggroStop) {
		this.aggroStop = aggroStop;
	}

	public void ignoreAggroTolerence() {
		this.aggroStop = Integer.MAX_VALUE;
	}

	public DistanceRules getDistanceRules() {
		return distanceRules;
	}
	
	public boolean isRetaliates() {
		return retaliates;
	}
	
	public void setRetaliates(boolean retaliates) {
		this.retaliates = retaliates;
	}
	
	@Override
	public void shove(int stunTick, Location attackerLoc) {
		if (getSize() > 1)
			return;
		
		if (!isRetaliates())
			return;
		
		if (isShoved())
			return;
		
		if (stunTick > 0) {
			stunEntity(stunTick, Graphic.create(254, GraphicType.HIGH));
		}
		
		setShoved(true);
		
		Location newLoc = Location.getShoved(attackerLoc, getCurrentLocation(), getDynamicRegionClip());
		
		moveX = newLoc.getX();
		moveY = newLoc.getY();
	}
	
	public boolean isSafeSpotNPC() {
		return safeSpotNPC;
	}
	
	public void setSafeSpotNPC(boolean safeSpotNPC) {
		this.safeSpotNPC = safeSpotNPC;
	}
	
	public boolean isWalkingHome() {
		return walkingHome;
	}
	
	public void setWalkingHome(boolean walkingHome) {
		if (walkingHome && !isRetaliates())
			return;
		this.walkingHome = walkingHome;
	}
	
	public boolean isRevenantNpc() {
		return isRevenantNpc;
	}
	
	public void setRevenantNpc(boolean isRevenantNpc) {
		this.isRevenantNpc = isRevenantNpc;
	}
	
	public int getWalkDistance() {
		return walkDistance;
	}
	
	public void setWalkDistance(int walkDistance) {
		this.walkDistance = walkDistance;
	}
	
	public boolean isDropsItemOnDeath() {
		return dropsItemOnDeath;
	}
	
	public void setDropsItemOnDeath(boolean dropsItemOnDeath) {
		this.dropsItemOnDeath = dropsItemOnDeath;
	}
	
	public Chins getChin() {
		return chin;
	}
	
	public void setChin(Chins chin) {
		this.chin = chin;
	}
	
	public Region getCurrentRegion() {
		return currentRegion;
	}
	
	public int getNpcTypeSmart() {
		if (transformId != -1) {
			return transformId;
		}
		
		return npcType;
	}
	
	public boolean isNpc() {
		return true;
	}
	
	public boolean isPlayer() {
		return false;
	}
	
	@Override
	public String toString() {
		return "NPC [npcIndex=" + npcIndex + ", npcType=" + npcType + ", npcName=" + npcName + ", absX=" + absX + ", absY=" + absY + ", heightLevel=" + heightLevel + ", makeX=" + makeX + ", makeY=" + makeY + ", maxHit=" + maxHit + ", moveX=" + moveX + ", moveY=" + moveY + ", walkingType="
				+ walkingType + ", combatStats=" + combatStats + ", isMoving=" + isMoving + ", viewX=" + viewX + ", viewY=" + viewY + ", lastX=" + lastX + ", lastY=" + lastY + ", singleCombatDelay=" + singleCombatDelay + ", walkDistance=" + walkDistance
				+ ", attackable=" + attackable + ", canWalk=" + canWalk + ", retargetTick=" + retargetTick + ", isSummoningSkillNpc=" + isSummoningSkillNpc + ", isUsingSummonigSpecial=" + isUsingSummonigSpecial + ", isPetNpc=" + isPetNpc + ", hitIcon=" + hitIcon + ", hitIcon2=" + hitIcon2 + ", hitMask=" + hitMask + ", hitMask2=" + hitMask2 + ", bossRoomNpc="
				+ bossRoomNpc + ", pestControlNPC=" + pestControlNPC + ", fightCavesNPC=" + fightCavesNPC + ", currentRegion=" + currentRegion + ", protectPrayer=" + protectPrayer + ", usingSoulSplit=" + usingSoulSplit + ", aliveTick=" + aliveTick + ", bandosAvatar=" + bandosAvatar + ", nomad=" + nomad + ", nomadTele=" + nomadTele + ", hitByKorasiSpec="
				+ hitByKorasiSpec + ", attackType=" + attackType + ", projectileId=" + projectileId + ", endGfx=" + endGfx + ", spawnedBy=" + spawnedBy + ", hitDelayTimer=" + hitDelayTimer + ", hitDiff=" + hitDiff + ", actionTimer=" + actionTimer + ", actionTimer2=" + actionTimer2 + ", enemyX=" + enemyX + ", enemyY=" + enemyY + ", isActive=" + isActive + ", isDead=" + isDead() + ", applyDead=" + applyDead + ", needRespawn=" + needRespawn
				+ ", respawns=" + respawns + ", walkingHome=" + walkingHome + ", freezeTimer=" + freezeTimer + ", underAttackBy=" + underAttackBy + ", killedbyNameBase37=" + killedbyNameBase37 + ", killerId=" + getKillerId() + ", instanceId=" + instanceId + ", isInstanceNPC=" + isInstanceNPC + ", lastDamageTaken=" + lastDamageTaken + ", randomWalk=" + randomWalk
				+ ", transformId=" + transformId + ", firstAttacker=" + firstAttacker + ", stuckCount=" + stuckCount + ", specialAttackType=" + specialAttackType + ", alwaysRespawn=" + alwaysRespawn + ", venomousNpc=" + venomousNpc + ", safeSpotNPC=" + safeSpotNPC + ", isRevenantNpc=" + isRevenantNpc + ", currentAttackStyle=" + currentAttackStyle + ", dgMissedHits="
				+ dgMissedHits + ", dgDamageTaken=" + dgDamageTaken + ", isDigging=" + isDigging + ", aggroStop=" + aggroStop + ", summonedFor=" + summonedFor + ", id=" + id + ", currentAttackMulti=" + currentAttackMulti + ", underAttackBy2=" + underAttackBy2 + ", oldIndexNPC=" + oldIndexNPC + ", isAggressive=" + isAggressive + ", retaliates=" + retaliates + ", resetAttackNpcTimer=" + resetAttackNpcTimer + ", killNpc=" + killNpc + ", walkingDir=" + walkingDir + ", lockMovement=" + lockMovement + ", isAttackingAPerson=" + isAttackingAPerson + ", ownerUnderAttackByPlayer=" + ownerUnderAttackByPlayer + ", IsUnderAttackNpc=" + IsUnderAttackNpc
				+ ", IsAttackingNPC=" + IsAttackingNPC + ", changedAttackStyle=" + changedAttackStyle + ", splash=" + splash + ", faceUpdated=" + faceUpdated + ", noDeathEmote=" + noDeathEmote + ", caught=" + caught + ", canTeleport=" + canTeleport + ", didTeleport=" + didTeleport + ", teleporting=" + teleporting + ", isRunning=" + isRunning + ", teleX=" + teleX + ", teleY=" + teleY
				+ ", teleHeight=" + teleHeight + ", teleportDelay=" + teleportDelay + ", def=" + def + ", inflictors=" + inflictors + ", distanceRules=" + distanceRules + ", currentX=" + currentX + ", currentY=" + currentY + ", hitDiff2=" + hitDiff2 + ", barbAssaultNpc=" + barbAssaultNpc + ", barbAssaultRunning=" + barbAssaultRunning + "]";
	}
	
	public boolean isBarricadeTypeNpc() {
		return Misc.testBit(properties, NpcStatsEntry.BARRICADE);
	}
	
	public boolean isAffectedBySsh() {
		return Misc.testBit(properties, NpcStatsEntry.SSH_HIDE);
	}
	
	public boolean canRemoveSsh() {
		return Misc.testBit(properties, NpcStatsEntry.SSH_TURN_OFF);
	}
	
	public boolean hexHunterEffect() {
		return Misc.testBit(properties, NpcStatsEntry.HEX_HUNTER_EFFECT);
	}
	
	public void teleport(int x, int y, int delay, int animId) {
		setWalksHome(false);
		if (animId == -1) {
			teleEndAnim = null;
		} else {
			startAnimation(8939);
			teleEndAnim = new Animation(animId);
		}
		
		teleEndGfx = Graphic.create(1577);
		teleX = x;
		teleY = y;
		teleHeight = getZ();
		startGraphic(Graphic.create(1576));
		teleportDelay = delay;
	}
	
	public int getAggroRadius() {
		return aggroRadius;
	}
	
	public void setAggroRadius(int aggroRadius) {
		this.aggroRadius = aggroRadius;
	}
	
	public void onHitFromAttackMob(BattleState state) {
	}
	
	;
	
	/**
	 * To prevent PI spell crap to activate and write own spells.
	 *
	 * @return false if we want to load PI npchandler spells
	 */
	public boolean loadedSpell(Player p) {
		return false;
	}
	
	/**
	 * Called when the NPC swings.
	 * @param p the default victim.
	 */
	public void startAttack(Player player) {
	}

	public void attack(Player p) {
		if (p == null) {
			return;
		}

		setKillerId(p.getId());
	}

	public boolean inCombatWithPlayer() {
		return killerId > 0;
	}

	public int getKillerId() {
		return killerId;
	}

	public void setKillerId(int killerId) {
		this.killerId = killerId;
	}

	public Player getKiller() {
		Player killer = PlayerHandler.players[killerId];
		if (killer == null || !killer.isActive()) {
			return null;
		}

		return killer;
	}

	public Player getSpawnedByPlayer() {
		if (spawnedBy <= 0) {
			return null;
		}

		Player player = PlayerHandler.players[spawnedBy];
		if (player == null || !player.isActive()) {
			return null;
		}

		return player;
	}

}
