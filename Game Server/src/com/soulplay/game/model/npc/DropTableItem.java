package com.soulplay.game.model.npc;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class DropTableItem implements Comparable<DropTableItem> {

	public final static int MAX_AMOUNT_VALUE = 0xFFFFFF; // Max of 16.7M

	public final static int MAX_PROBABILITY_VALUE = 0xFFFFF; // Max of 65535

	public static DropTableItem parseDropTableItem(String line) {
		int itemId = -1, amountMin = -1, amountMax = -1, probability = -1;
		boolean varyingAmount = false;
		String[] tokens = line.split(" ");
		boolean comment = false;
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("")) {
				break;
			}
			if (tokens[i].startsWith("//")) {
				continue;
			}
			if (tokens[i].startsWith("/*")) {
				comment = true;
				break;
			}
			if (tokens[i].endsWith("*/")) {
				comment = false;
				break;
			}
			if (comment) {
				break;
			}
			if (tokens[i].contains("//")) {
				break;
			}
			if (tokens[i].contains("/*")) {
				break;
			}
			if (tokens[i].contains("*/")) {
				break;
			}
			switch (i) {
				case 0:
					try {
						itemId = Integer.parseInt(tokens[i]);
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out
								.println("Case 0: Line " + DropTable.lineNumber
										+ ". Drop table item could not be parsed from string: "
										+ line);
						return null;
					}
					break;
				case 1:
					try {
						if (tokens[i].contains("-")) {
							String[] rates = tokens[i].split("-");
							amountMin = Integer.parseInt(rates[0]);
							amountMax = Integer.parseInt(rates[1]);
							varyingAmount = true;
						} else {
							amountMin = Integer.parseInt(tokens[i]);
						}
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out
								.println("Case 1: Line " + DropTable.lineNumber
										+ ". Drop table item could not be parsed from string: "
										+ line);
						return null;
					}
					break;
				case 2:
					try {
						probability = startsWithNumber(tokens[i])
								? Integer.parseInt(tokens[i])
								: DropRate.getRate(tokens[i]).getProbability();
					} catch (NumberFormatException e) {
						if (Config.SERVER_DEBUG) {
							e.printStackTrace();
						}
						System.out
								.println("Case 2: Line " + DropTable.lineNumber
										+ ". Drop table item could not be parsed from string: "
										+ line);
						return null;
					}
					break;
			}
		}
		return varyingAmount
				? new DropTableItem(itemId, amountMin, amountMax, probability)
				: new DropTableItem(itemId, amountMin, probability);
	}

	private static boolean startsWithNumber(String s) {
		return s.startsWith("0") || s.startsWith("1") || s.startsWith("2")
				|| s.startsWith("3") || s.startsWith("4") || s.startsWith("5")
				|| s.startsWith("6") || s.startsWith("7") || s.startsWith("8")
				|| s.startsWith("9") || s.startsWith("0x");
	}

	private final int item;

	private final long data;

	public DropTableItem(final int item, final int amount, int probability) {
		this.item = item;
		long lData = 0;
		lData += (long) amount & MAX_AMOUNT_VALUE;
		lData += ((long) (amount & MAX_AMOUNT_VALUE) << 24);
		lData += ((long) (probability & MAX_PROBABILITY_VALUE) << 48);
		data = lData;
	}

	public DropTableItem(final int item, final int amountMin,
			final int amountMax, int probability) {
		this.item = item;
		long lData = 0;
		lData += amountMin & MAX_AMOUNT_VALUE;
		lData += ((long) (amountMax & MAX_AMOUNT_VALUE) << 24);
		lData += ((long) (probability & MAX_PROBABILITY_VALUE) << 48);
		data = lData;
	}

	public DropTableItem(final int item, final long data) {
		this.item = item;
		this.data = data;
	}

	@Override
	public int compareTo(DropTableItem arg0) {
		return (int) ((arg0.getProbability(null) - getProbability(null)) * 10000000)
				+ ItemConstants.getItemName(getItemId()).compareToIgnoreCase(
						ItemConstants.getItemName(arg0.getItemId()));
	}

	public int getAmount() {
		return (int) (Misc
				.random((int) (((data >> 24) & MAX_AMOUNT_VALUE)
						- (data & MAX_AMOUNT_VALUE)))
				+ (((data) & MAX_AMOUNT_VALUE)));
	}
	
	public int getMinAmount() {
		return (int)(data & MAX_AMOUNT_VALUE);
	}
	
	public int getMaxAmount() {
		return (int)((data >> 24) & MAX_AMOUNT_VALUE);
	}

	public int getItemId() {
		return item;
	}
	
	public long getData() {
		return data;
	}

	public double getProbability(Player player) {
		int probability = getProbability();
		if (probability == 0) {
			probability = 15;
		}

		double modifiers = 1.0;

		if (probability >= DropRate.SUPER_RARE.getRawProbability()) {
			modifiers = DBConfig.SUPER_RARE_DROP_MODIFIER * RSConstants.getDropRatePerMode(player);
		} else if (probability >= DropRate.RARE.getRawProbability()) {
			modifiers = DBConfig.RARE_DROP_MODIFIER * RSConstants.getDropRatePerMode(player);
		}

		return (1.0 / probability) * modifiers;
	}

	public int getRawProbabilityPerMode(Player player) {
		int rarity = getProbability();
		if (rarity == 0) {
			return 15;
		}

		double modifiers = 1.0;
		if (rarity >= DropRate.SUPER_RARE.getRawProbability()) {
			modifiers = DBConfig.SUPER_RARE_DROP_MODIFIER * RSConstants.getDropRatePerMode(player);
		} else if (rarity >= DropRate.RARE.getRawProbability()) {
			modifiers = DBConfig.RARE_DROP_MODIFIER * RSConstants.getDropRatePerMode(player);
		}

		return (int) (rarity / modifiers);
	}
	
	public int getRawProbability() {
		int probability = getProbability();
		if (probability == 0) {
			return 15;
		}

		return probability;
	}

	public int getProbability() {
		return (int) ((data >> 48) & MAX_PROBABILITY_VALUE);
	}

	public static long setProbability(long dataToChange, int newValue) {
		dataToChange &= ~((long) (MAX_PROBABILITY_VALUE) << 48);
		dataToChange |= ((long) (newValue & MAX_PROBABILITY_VALUE) << 48);
		return dataToChange;
	}

	@Override
	public String toString() {
		return item + "";
	}
}
