package com.soulplay.game.model.npc.plugins;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author Jire
 */
public abstract class AutoConstructNPC extends AbstractNpc {
	
	protected Constructor<?> autoConstructor;
	
	public AutoConstructNPC(int slot, int npcType) {
		super(slot, npcType);
	}
	
	@Override
	public AbstractNpc construct(int slot, int npcType) {
		if (autoConstructor == null) {
			autoConstructor = getConstructor();
		}
		try {
			return (AbstractNpc) autoConstructor.newInstance(slot, npcType);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected Constructor<?> getConstructor() {
		Constructor<?>[] constructors = getClass().getDeclaredConstructors();
		for (Constructor<?> constructor : constructors) {
			if (!constructor.isAccessible()) continue;
			Class<?>[] types = constructor.getParameterTypes();
			if (types == null || types.length != 2) continue;
			if (Integer.class.equals(types[0]) && Integer.class.equals(types[1])) {
				return constructor;
			}
		}
		throw new NullPointerException("Could not determine an auto-constructor for " + getClass().getCanonicalName());
	}
	
}
