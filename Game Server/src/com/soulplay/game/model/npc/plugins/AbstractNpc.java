package com.soulplay.game.model.npc.plugins;

import com.soulplay.game.model.npc.NPC;

/**
 * @author Jire - defineConfigs() feature.
 */
public abstract class AbstractNpc extends NPC {
	
	private boolean definedConfigs;

//	public AbstractNpc() {
//		super(-1, -1);
//	}
	
	public AbstractNpc(int slot, int npcType) {
		super(slot, npcType);
		tryDefineConfigs();
	}
	
	public abstract AbstractNpc construct(int slot, int npcType);
	
	public final void tryDefineConfigs() {
		if (!definedConfigs) {
			definedConfigs = true;
			defineConfigs();
		}
	}
	
	public void defineConfigs() {
	}
	
}
