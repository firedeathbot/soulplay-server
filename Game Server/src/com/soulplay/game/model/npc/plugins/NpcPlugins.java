package com.soulplay.game.model.npc.plugins;

import java.lang.reflect.Modifier;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class NpcPlugins {


	private static final Int2ObjectMap<Class<? extends AbstractNpc>> map = new Int2ObjectOpenHashMap<>();

	public static void init() {
		FastClasspathScanner scanner = new FastClasspathScanner();
		scanner.matchSubclassesOf(AbstractNpc.class, clazz -> {
			if (!clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers()) && clazz.isAnnotationPresent(NpcMeta.class)) {
				NpcMeta meta = clazz.getAnnotation(NpcMeta.class);
				for (int id : meta.ids()) {
					map.put(id, clazz);
				}
			}
		}).scan();
	}

	public static Class<? extends AbstractNpc> get(int id) {
		Class<? extends AbstractNpc> clazz = map.get(id);
		if (clazz == null) {
			return null;
		}
		return clazz;
	}

	public static void clear() {
		map.clear();
	}

}
