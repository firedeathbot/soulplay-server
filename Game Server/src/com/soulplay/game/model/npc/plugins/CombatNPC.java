package com.soulplay.game.model.npc.plugins;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

/**
 * @author Jire
 */
public abstract class CombatNPC extends AutoConstructNPC {
	
	public CombatNPC(int slot, int npcType) {
		super(slot, npcType);
	}
	
	public int dealDamage(Player victim, int damage, int delay, Graphic graphic, CombatType style) {
		if (style == null) return NpcHitPlayer.dealTrueDamage(this, victim, damage, delay, graphic);
		switch (style) {
			case MELEE:
				return NpcHitPlayer.dealMeleeDamage(this, victim, damage, delay, graphic);
			case RANGED:
				return NpcHitPlayer.dealRangeDamage(this, victim, damage, delay, graphic);
			case MAGIC:
				return NpcHitPlayer.dealMagicDamage(this, victim, damage, delay, graphic);
			default:
				throw new UnsupportedOperationException("Unsupported combat style " + style);
		}
	}
	
	public int dealDamage(Player victim, int damage, int delay) {
		return dealDamage(victim, damage, delay, null);
	}
	
	public int dealDamage(Player victim, int damage) {
		return dealDamage(victim, damage, 0);
	}
	
	public int dealDamage(Player victim, int damage, int delay, CombatType style) {
		return dealDamage(victim, damage, delay, null, style);
	}
	
	public int dealDamage(Player victim, int damage, CombatType style) {
		return dealDamage(victim, damage, 0, style);
	}
	
	public CombatNPC setStyle(CombatType style) {
		currentAttackStyle = style.getId();
		return this;
	}
	
	public int meleeSwing(Player victim, int maxHit, int animationID, int delay) {
		setStyle(CombatType.MELEE);
		
		startAnimation(animationID);
		int damage = Misc.random(maxHit);
		return NpcHitPlayer.dealMeleeDamage(this, victim, damage, delay, null);
	}
	
	public int meleeSwing(Player victim, int maxHit, int animationID) {
		return meleeSwing(victim, maxHit, animationID, 1);
	}

	public int projectileSwing(Player victim, int projectileID, int maxHit, int delay, Graphic endGraphic, CombatType style) {
		return projectileSwing(victim, Projectile.create(this, victim, projectileID), maxHit, delay, endGraphic, style);
	}

	public int projectileSwing(Player victim, Projectile projectile, int maxHit, int delay, Graphic endGraphic, CombatType style) {
		setStyle(style);
		projectile.send();
		int damage = Misc.random(maxHit);
		return dealDamage(victim, damage, delay, endGraphic, style);
	}

	public int projectileSwing(Player victim, int projectileID, int maxHit, int delay) {
		return projectileSwing(victim, projectileID, maxHit, delay, null, CombatType.RANGED);
	}
	
	public int projectileSwing(Player victim, int projectileID, int maxHit) {
		return projectileSwing(victim, projectileID, maxHit, 3);
	}
	
}
