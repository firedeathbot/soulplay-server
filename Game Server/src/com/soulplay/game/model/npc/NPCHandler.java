package com.soulplay.game.model.npc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Map.Entry;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.event.impl.DailyTasks;
import com.soulplay.content.event.randomevent.WildyWyrm;
import com.soulplay.content.items.degradeable.Degrade;
import com.soulplay.content.minigames.FightCaves;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.TreasureTrailManager;
import com.soulplay.content.npcs.NPCProjectInsanityStatics;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.npcs.NpcStatsEntry;
import com.soulplay.content.npcs.OSRSBoxNpcDataLoader;
import com.soulplay.content.npcs.combat.LoadSpell;
import com.soulplay.content.npcs.combat.NPCCombatData;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.npcs.combat.spells.BoulderFallingFromSkyAndJuliusShittingHisBallss;
import com.soulplay.content.npcs.impl.Barricades;
import com.soulplay.content.npcs.impl.bosses.BossKCEnum;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.combat.AttackNPC;
import com.soulplay.content.player.combat.CombatStatics;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.accuracy.MagicAccuracy;
import com.soulplay.content.player.combat.accuracy.MeleeAccuracy;
import com.soulplay.content.player.combat.accuracy.RangeAccuracy;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy.AccuracyType;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.dailytasks.TaskConstants;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.holiday.halloween.Halloween;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.pets.pokemon.PokemonFollowing;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.drops.DropTableManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.RoomReference;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonRangeAndMageNpcs;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.ForgottenMageSpells;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.summoning.Summoning;
import com.soulplay.content.player.skills.summoning.SummoningData;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.raids.boss.greatolm.GreatOlmInstanceHandler;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.attacks.Attack;
import com.soulplay.game.model.npc.attacks.NPCAttackDefinition;
import com.soulplay.game.model.npc.attacks.NPCAttackLoader;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcPlugins;
import com.soulplay.game.model.npc.spawns.NPCSpawnManager;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.TileControl;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.map.travel.TempPathFinder;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import plugin.item.chargeitems.BraceletOfEthereumPlugin;

public final class NPCHandler {
	
	private NPCHandler() {
		throw new UnsupportedOperationException();
	}
	
	public static int maxNPCs = 10000;// Config.MAX_NPC_ID;//10000;
	
	public static int maxListedNPCs = 10000;
	
	public static int maxNPCDrops = 10000;
	
	public static NPC npcs[] = new NPC[maxNPCs];
	
	public static boolean GOLDEN_CHEST_DROP = true;

	public static NPC findNPC(int npcType) {
		for (NPC n : npcs) {
			if (n == null || n.isDead()) continue;
			if (n.npcType == npcType)
				return n;
		}
		return null;
	}
	
	private static void appendNpcKill(final Client c, final NPC n) {
		c.npcKills += 1;
		c.getAchievement().kill500Monsters();
		c.getAchievement().kill10000Monsters();
		Titles.kill2000Monsters(c);
		c.increaseProgress(TaskType.NPC_KILL, n.npcType);
		
		if (n.isDragon) {
			c.getAchievement().killDragons();
		}
		
		//SuperiorSlayerNpc.handleSuperior(c, n);
		
		if (n.attr().getOrDefault("thiev_guard", false)) {
			c.increaseProgress(TaskType.NPC_KILL, TaskConstants.KILL_THIEV_GUARD);
		}
		
		if (RSConstants.dzDungeon(c.getX(), c.getY())) {
			Integer value = DailyTasks.dungeonCaveKills.get(c.getName());
			int killCount = value != null ? value : 0;
			DailyTasks.dungeonCaveKills.put(c.getName(), ++killCount);
		}
		
		if (NPCProjectInsanityStatics.ZammyKC(n.npcType)) {
			int kc = c.getTempVar().getInt("ZammyKC") + 1;
			c.getTempVar().put("ZammyKC", kc);
			c.getPacketSender().sendFrame126("" + kc + "", 16219);
		}
		if (NPCProjectInsanityStatics.ArmadylKC(n.npcType)) {
			// c.Arma += 1;
			int kc = c.getTempVar().getInt("ArmaKC") + 1;
			c.getTempVar().put("ArmaKC", kc);
			c.getPacketSender().sendFrame126("" + kc + "", 16216);
		}
		if (NPCProjectInsanityStatics.BandosKC(n.npcType)) {
			int kc = c.getTempVar().getInt("BandosKC") + 1;
			c.getTempVar().put("BandosKC", kc);
			c.getPacketSender().sendFrame126("" + kc + "", 16217);
		}
		if (NPCProjectInsanityStatics.SaraKC(n.npcType)) {
			int kc = c.getTempVar().getInt("SaraKC") + 1;
			c.getTempVar().put("SaraKC", kc);
			c.getPacketSender().sendFrame126("" + kc + "", 16218);
		}
		if (NPCProjectInsanityStatics.ZarosKC(n.npcType)) {
			c.setZarosKC(1, true);
			c.getPacketSender().sendFrame126("" + c.getZarosKC() + "", 16221);
		}
		
		switch (n.npcType) {
			case 912:
			case 913:
			case 914:
				c.magePoints += 1;
				c.increaseProgress(TaskType.NPC_KILL, TaskConstants.KILL_BATTLE_MAGE);
				break;
			case 2030:
				c.barrows[5] = 2; // 2 for dead?
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 2029:
				c.barrows[4] = 2; // 2 for dead?
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 2028:
				c.barrows[3] = 2; // 2 for dead?
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 2027:
				c.barrows[2] = 2; // 2 for dead?
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 2026:
				c.barrows[1] = 2; // 2 for dead?
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 2025:
				c.barrows[0] = 2; // 2 for dead?
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 4921:
			case 2035:
				c.barrows[6]++;
				c.getPacketSender().sendFrame126("<col=ff00>" + c.barrows[6], 16137);
				break;
			case 76:
				Halloween.zombiePieces(c);
				break;
			case 54:
				if (Misc.random(9999) == 1) {
					ItemHandler.createGroundItem(c, 12480, n.absX, n.absY,
							c.getHeightLevel(), 1);
					c.sendMessage("The black dragon dropped a egg!");
				}
				break;
			case 55:
				if (Misc.random(9999) == 1) {
					ItemHandler.createGroundItem(c, 12478, n.absX, n.absY,
							c.getHeightLevel(), 1);
					c.sendMessage("The blue dragon dropped a egg!");
				}
				break;
			case 53:
				if (Misc.random(9999) == 1) {
					ItemHandler.createGroundItem(c, 12477, n.absX, n.absY,
							c.getHeightLevel(), 1);
					c.sendMessage("The red dragon dropped a egg!");
				}
				break;
			case 941:
			case 4677:
			case 4678:
			case 4679:
			case 4680:
				if (Misc.random(9999) == 1) {
					ItemHandler.createGroundItem(c, 12479, n.absX, n.absY,
							c.getHeightLevel(), 1);
					c.sendMessage("The green dragon dropped a egg!");
				}
				break;
			case 2881:
				Pet.petRoll(c, Pet.SUPREME);
				c.getAchievement().defeat40DagBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 2, "dks_kill");
				}
				break;
			case 2882:
				Pet.petRoll(c, Pet.PRIME);
				c.getAchievement().defeat40DagBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 2, "dks_kill");
				}
				break;
			case 2883:
				Pet.petRoll(c, Pet.REX);
				c.getAchievement().defeat40DagBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 2, "dks_kill");
				}
				break;
			case 3340:
				Pet.petRoll(c, Pet.BABY_MOLE);
				break;
			case 6222:
				Pet.petRoll(c, Pet.KREE_ARRA);
				c.getAchievement().defeat35GWDBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "gwd_kill");
				}
				break;
			case 6247:
				Pet.petRoll(c, Pet.ZILYANA);
				c.getAchievement().defeat35GWDBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "gwd_kill");
				}
				break;
			case 3835:
				Pet.petRoll(c, Pet.KALPHITE_PRINCESS1);
				break;
			case 6260:
				Pet.petRoll(c, Pet.GENERAL_GRAARDOR);
				c.getAchievement().defeat35GWDBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "gwd_kill");
				}
				break;
			case 6203:
				Pet.petRoll(c, Pet.KRIL_TSUTSAROTH);
				c.getAchievement().defeat35GWDBosses();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "gwd_kill");
				}
				break;
			case 1265:
				c.getAchievement().crabsKilled();
				break;
			case 51:
				c.getAchievement().killFrostDragon();
				break;
			case 50:
				//Pet.petRoll(c, Pet.PRINCE_BLACK_DRAGON);//already added in npc drop table file
				c.getAchievement().killKBD();
				Titles.defeatFiveKBD(c);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "kbd_kill");
				}
				break;
			case 3200:
				Pet.petRoll(c, Pet.CHAOS_ELEMENTAL);
				c.getAchievement().killChaosEle();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 2, "chaos_ele_kill");
				}
				break;
			case 8349:
				c.getAchievement().kill5Torms();
				c.getAchievement().kill150Torms();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 2, "tds_kill");
				}
				break;
			case 13447:
				c.getAchievement().defeatNex();
				Titles.defeatNex(c);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 150, "nex_kill");
				}
				
				for (Entry<Integer, Integer> entry : n.getInflictors().entrySet()) {
					int damage = entry.getValue();
					if (damage < 100) {
						continue;
					}
					
					Client inflictor = (Client) PlayerHandler.getPlayerByMID(entry.getKey());
					if (inflictor == null || !inflictor.isActive) {
						continue;
					}
					
					inflictor.increaseProgress(TaskType.NPC_BOSS_KILL, n.npcType);
				}
				break;
			case 8133:
				c.getAchievement().defeatCorpBeast();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 5, "corp_kill");
				}
				break;
			case 8596:
				c.getAchievement().defeatAOD();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "aod_kill");
				}
				break;
			case 6090:
				c.getAchievement().defeatWildyWyrm();
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 200, "wyrm_kill");
				}
				break;
			case 4540:
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "bava_kill");
				}
				break;
			case 1158:
				Pet.petRoll(c, Pet.KALPHITE_PRINCESS);
				Titles.defeatFiveKalphite(c);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 1, "kalphite_kill");
				}
				break;
			case 13465:
			case 13469:
			case 13471:
			case 13472:
			case 13473:
			case 13474:
			case 13475:
			case 13476:
			case 13477:
			case 13478:
			case 13479:
			case 13480:
			case 13481:
				c.getAchievement().killRev();
				c.increaseProgress(TaskType.NPC_KILL, TaskConstants.KILL_REVENANTS);
				break;
			case 221:
				c.wildyKeys = 2;
				c.sendMessage("You can open the door!");
				break;
			case 8528:
				c.getDotManager().applyPoison(15, n);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 10, "nomad_kill");
				}
				c.sendMessage("Nomad spits venom on you before he dies.");
				break;
		}
	}
	
	public static void applyDamage(int i, Client hitPlayer, boolean multi, Boss boss) {
			if (npcs[i] == null || npcs[i].isDead()) {
				return;
			}
			if (multiAttacks(i) || multi) {
				return;
			}
			if (hitPlayer.playerIndex <= 0 && hitPlayer.npcIndex <= 0) {
				if (hitPlayer.autoRet == 1 && !hitPlayer.walkingToItem && hitPlayer.clickObjectType <= 0) {
					hitPlayer.npcIndex = i;
				}
			}
			if ((hitPlayer.getAttackTimer() == 3 || hitPlayer.getAttackTimer() == 0) && !hitPlayer.isLockActions()
					&& hitPlayer.npcIndex == 0 && hitPlayer.oldNpcIndex == 0) {
				hitPlayer.startAnimation(hitPlayer.getCombat().getBlockEmote());
			}
			if (hitPlayer.isAlive()) {
				int damage = 0;
				AccuracyType accuracyType = null;
				if (npcs[i].attackType == 0 || npcs[i].attackType == 4) {
					accuracyType = MeleeAccuracy.successfulHit(npcs[i], hitPlayer);
					if (accuracyType.isSuccessfulHit() || (boss != null && boss.ignoreMeleeBonus())) {
						damage = Misc.random(npcs[i].maxHit);
					}

					if (damage > 0 && (hitPlayer.prayerActive[Prayers.PROTECT_FROM_MELEE
							.ordinal()]
							|| hitPlayer.curseActive[Curses.DEFLECT_MELEE.ordinal()])) { // protect from melee
						if (hitPlayer.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
							hitPlayer.curses().deflectNPC(npcs[i], damage, 0);
						}
						damage = (int) (damage * npcs[i].prayerReductionMelee);
					}
				} else if (npcs[i].attackType == 1) { // range
					accuracyType = RangeAccuracy.isAccurateImpact(npcs[i], hitPlayer);
					if (accuracyType.isSuccessfulHit() || (boss != null && boss.ignoreRangeBonus())) {
						damage = Misc.random(npcs[i].maxHit);
					}

					if (damage > 0 && (hitPlayer.prayerActive[Prayers.PROTECT_FROM_MISSILES
							.ordinal()]
							|| hitPlayer.curseActive[Curses.DEFLECT_MISSILES.ordinal()])) {
						if (hitPlayer.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
							hitPlayer.curses().deflectNPC(npcs[i], 0, 1);
						}
						damage = (int) (damage * npcs[i].prayerReductionRanged);
					}
					if (npcs[i].endGfx > 0) {
						hitPlayer.startGraphic(Graphic.create(npcs[i].endGfx, npcs[i].endGfxDelay, npcs[i].endGraphicType));
					}
				} else if (npcs[i].attackType == 2) { // magic
					boolean magicFailed = true;
					accuracyType = MagicAccuracy.successfulHit(npcs[i], hitPlayer, null);
					if (accuracyType.isSuccessfulHit() || (boss != null && boss.ignoreMageBonus())) {
						damage = Misc.random(npcs[i].maxHit);
						magicFailed = false;
					}

					if (damage > 0 && (hitPlayer.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
							|| hitPlayer.curseActive[Curses.DEFLECT_MAGIC.ordinal()])) {
						if (hitPlayer.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
							hitPlayer.curses().deflectNPC(npcs[i], 0, 2);
						}
						damage = (int) (damage * npcs[i].prayerReductionMagic);
						if (damage == 0)
							magicFailed = true;
					}
					if (npcs[i].endGfx > 0) {
						if (!magicFailed || npcs[i].isFightCavesNPC()) {
							hitPlayer.startGraphic(Graphic.create(npcs[i].endGfx, npcs[i].endGfxDelay, npcs[i].endGraphicType));
						} else {
							hitPlayer.startGraphic(Graphic.create(85, npcs[i].endGfxDelay, GraphicType.HIGH));
						}
					}
				} else if (npcs[i].attackType == 3) {
					damage = hitPlayer.getPA().handleDragonFireHit(npcs[i], Misc.random(40) + 10);
					if (damage > 0) {
						accuracyType = AccuracyType.HIT;
					} else {
						accuracyType = AccuracyType.ROLL_FAIL;
					}

					hitPlayer.startGraphic(Graphic.create(npcs[i].endGfx, npcs[i].endGfxDelay, GraphicType.LOW));
					hitPlayer.getPA().chargeDfs();
				}
				
				if (npcs[i].npcType == 7151) { // demonic gorilla switches
					if (damage > 0) {
						npcs[i].dgMissedHits = 0;
					} else {
						handleGorillaAttackSwitches(npcs[i]);
					}
				}
				
				int soak = hitPlayer.getCombat().damageSoaked(damage, npcs[i].attackType);
				damage -= soak;
				if (!npcs[i].isSummoningSkillNpc) {
					hitPlayer.getCombat().appendHit(hitPlayer, damage, 0,
							(npcs[i].attackType > 2 ? 2 : npcs[i].attackType),
							false, accuracyType);
				} else {
					hitPlayer.getSA().attackPlayer(i);
				}
				npcs[i].onDamageApplied(hitPlayer, damage);
			}
	}
	
	public static void applyDamageToNpc(int i, NPC kill) {
		if (npcs[i] != null) {
			if (npcs[i].isDead() || kill.isDead()) {
				return;
			}
			if (npcs[i].isPetNpc() && !npcs[i].isPokemon) {
				return;
			}
			if ((kill.getAttackTimer() == 0 || kill.getAttackTimer() == 3) && !kill.isPokemon) {
				int defEmote = NPCAnimations.getDefend(kill/* .npcType */);
				if (defEmote > 0)
					kill.startAnimation(defEmote);
			}
			int damage = 0;
			int maxHit;
			if (npcs[i].isPokemon) {
				maxHit = PokemonData.maxHit(npcs[i].summoner, npcs[i]);
			} else {
				maxHit = npcs[i].maxHit;
			}
			if (npcs[i].attackType == 0) {
				if (MeleeAccuracy.successfulHit(npcs[i], kill).isSuccessfulHit()) {
					damage = Misc.random(maxHit);
				}
			}
			if (npcs[i].attackType == 1) { // range
				if (RangeAccuracy.isAccurateImpact(npcs[i], kill).isSuccessfulHit()) {
					damage = Misc.random(maxHit);
				}
				if (npcs[i].endGfx > 0) {
					kill.startGraphic(Graphic.create(npcs[i].endGfx, npcs[i].endGfxDelay, npcs[i].endGraphicType));
				}
			}
			
			if (npcs[i].attackType == 2) { // magic
				boolean magicFailed = true;
				if (MagicAccuracy.successfulHit(npcs[i], kill, null).isSuccessfulHit()) {
					damage = Misc.random(maxHit);
					magicFailed = false;
				}
				if (npcs[i].endGfx > 0) {
					if (!magicFailed) {
						kill.startGraphic(Graphic.create(npcs[i].endGfx, npcs[i].endGfxDelay, npcs[i].endGraphicType));
					} else {
						kill.startGraphic(Graphic.create(85, npcs[i].endGfxDelay, GraphicType.HIGH));
					}
				}
			}
			kill.dealDamage(new Hit(damage, 0, npcs[i].attackType, npcs[i]));
			if (npcs[i].isPokemon && npcs[i].summoner != null && npcs[i].summoner.summonedPokemon != null) {
				npcs[i].summoner.summonedPokemon.advanceXp(damage * 4, npcs[i].summoner, npcs[i]);
			}
			npcs[i].resetAttackNpcTimer = 0;
			if (kill.killNpc == i) {
				kill.resetAttackNpcTimer = 0;
			}
			
			if (kill.underAttackBy < 1 && kill.isRetaliates()) {
				kill.killNpc = i;
			}
			
			if (kill.underAttackBy2 < 1) {
				kill.underAttackBy2 = i;
			}
			
			kill.lastDamageTaken = System.currentTimeMillis();
			
			if (kill.isPestControlNPC()) {
				PestControl.updateNpcHP(kill);
			}
			
		}
	}
	
	public static void attackNpc(final int i) {
		final NPC npc = npcs[i];
		if (npc == null) {
			return;
		}
		if (npc.isPetNpc() && !npc.isPokemon) {
			return;
		}
		if (npc.isStunned()) {
			npc.resetAttack();
			return;
		}
		if (!AttackNPC.canAttackNpc(npc.npcType)) {
			return;
		}
		if (npc.isDead()) {
			npc.killNpc = -1;
			return;
		}
		int killId = npc.killNpc;
		if (killId < 0) {
			return;
		}
		final NPC kill = npcs[killId];
		
		if (kill == null) {
			npc.killNpc = -1;
			return;
		}
		
		if (kill.isDead() || kill.getSkills().getLifepoints() <= 0) {
			npc.killNpc = -1;
			return;
		}
		
		if (npc.underAttackBy > 0 && !kill.isPokemon) {
			npc.killNpc = -1;
			return;
		}
		
		if (npc.heightLevel != kill.heightLevel) {
			npc.killNpc = -1;
			return;
		}
		
		if (pathBlocked(npc, kill)) {
			npc.resetAttackNpcTimer++;
		}
		
		if (npc.resetAttackNpcTimer == 6) {
			npc.killNpc = -1;
			return;
		}
		
		npc.face(kill);
		
		int distanceToPoint = Misc.distanceToPoint(npc.getX(), npc.getY(),
				kill.getX(), kill.getY());
		
		if (distanceToPoint > 25) {
			npc.killNpc = -1;
			return;
		}
		
		if (distanceToPoint <= distanceRequired(i)) {
			
			npc.attackType = 0;
			
			npc.projectileId = -1;
			npc.endGfx = -1;
			
			Attack attack;
			if (npc.isPokemon) {
				/*PokemonStats stats = npc.summoner.summonedPokemon.stats;
//				int missingType = 0;
				if (stats.range > stats.magic && stats.range > stats.strength) { // range attack
					attack = NPCAttackLoader.setRangeAttack(npc);
//					missingType = 1;
				} else if (stats.magic > stats.range && stats.magic > stats.strength) { // magic attack
					attack = NPCAttackLoader.setMagicAttack(npc);
//					missingType = 2;
				} else { // else melee
					attack = NPCAttackLoader.setMeleeAttack(npc);
				}
				if (attack == null) { // if null then set whatever the basic hit is from npc, or display "MISSING ATTACK STYLE"
					attack = NPCAttackLoader.setAnyPokemonAttack(npc);
					if (attack == null)
						npc.forceChat("Missing attack styles");
					//npc.forceChat("Julius i'm missing attack style:"+(missingType == 0 ? "melee" : (missingType == 1 ? "range" : "magic")));//attack = NPCCombatData.loadSpell(kill, npc);
				}*/
				attack = NPCAttackLoader.setAnyPokemonAttack(npc);
				if (attack == null) {
					if (!Config.RUN_ON_DEDI) {
						npc.forceChat("Missing attack styles");
					}
				} else {
					npc.endGfx = attack.endGfx;
					npc.projectileId = attack.projectileId;
				}
			} else {
				attack = NPCCombatData.loadSpell(kill, npc);
			}
			
			npc.setAttackTimer(npc.getCombatStats().getAttackSpeed());
			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
				
				@Override
				public void execute(CycleEventContainer e) {
					
					if (npcs[i] == null || kill == null || npc.getSkills().getLifepoints() <= 0
							|| kill.getSkills().getLifepoints() <= 0 || npc.isDead() || kill.isDead()) {
						e.stop();
						return;
					}
					
					if (kill.isBarbAssaultNpc())
						BarbarianAssault.onAttackNpc(npc, kill);
					
					if (npc.npcType >= 3747 && npc.npcType <= 3751) {
						if (kill.getSkills().getLifepoints() > 0 && kill.getSkills().getLifepoints() < kill.getSkills().getStaticLevel(Skills.HITPOINTS)
								&& !kill.isDead()) {
							int healAmount = 15 + ((npc.npcType - 3747) * 2);
							if (kill.getSkills().getStaticLevel(Skills.HITPOINTS) < kill.getSkills().getLifepoints() + healAmount) {
								healAmount = kill.getSkills().getStaticLevel(Skills.HITPOINTS) - kill.getSkills().getLifepoints();
							}
							if (kill.getSkills().getLifepoints() > 0 || !kill.isDead()) {
								kill.getSkills().heal(healAmount);
							}
							PestControl.updateNpcHP(kill);
							e.stop();
							return;
						}
						e.stop();
						return;
					} else {
						applyDamageToNpc(i, kill);
						e.stop();
					}
					e.stop();
				}
				
				@Override
				public void stop() {
				
				}
			}, (getHitDelay(i)));
			
			if (npc.projectileId > 0) {
				Location start = (npc.getSize() == 5 || npc.getSize() == 7) ? Projectile.getLocationOffset(npc.getCurrentLocation(), kill.getCurrentLocation(), npc.getSize()) : Location.create(npc.getX() + offset(npc, kill.getCurrentLocation(), true),
						npc.getY() + offset(npc, kill.getCurrentLocation(), false), npc.getHeightLevel());
				
				Projectile proj = new Projectile(npc.projectileId,
						start,
						Location.create(kill.getX(), kill.getY(), kill.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(65);
				proj.setLockon(kill.getProjectileLockon());
				proj.setEndDelay(getProjectileSpeed(i));
				GlobalPackets.createProjectile(proj);
			}
			if (attack != null) {
				if (attack.animId > 0) {
					npc.startAnimation(attack.animId);
				} else {
					Animation anim = NPCAnimations.getCombatEmote(npc.npcType, npc.attackType);
					if (anim != null) {
						npc.startAnimation(anim);
					} else {
						npc.forceChat("I am missing an attack emote. 1");
					}
				}
				if (attack.startGfx > 0) {
					npc.startGraphic(Graphic.create(attack.startGfx));
				}
			} else {
				Animation anim = NPCAnimations.getCombatEmote(npc.npcType, npc.attackType);
				if (anim != null) {
					npc.startAnimation(anim);
				} else {
					npc.forceChat("I am missing an attack emote. 1");
				}
				if (npc.npcType >= 3747 && npc.npcType <= 3751) {
					npc.startAnimation(3911);
					npc.startGraphic(Graphic.create(658));
				}
			}
			
			if (npc.npcType >= 3747 && npc.npcType <= 3751) {
				npc.killNpc = -1;
				npc.randomWalk = true;
			}
		}
	}
	
	/**
	 * NPC Attacking Player
	 **/
	
	public static void attackPlayer(final Client c, final NPC npc, final Boss boss) {
		if (npc == null || !npc.isActive || c == null || !c.isActive) {
			return;
		}
		
		if (npc.isDead()) {
			return;
		}
		
		if (c.teleTimer > 0) {
			npc.setKillerId(0);
			return;
		}
		
		if (c.hidePlayer || c.properLogout || c.isSaveInProgressForDC()) {
			npc.resetFollow();
			npc.resetAttack();
			return;
		}
		
		if (npc.isStunned()) {
			npc.resetAttack();
			return;
		}
		
		if (!c.isAlive()) {
			return;
		}
		
		if (npc.npcType == GreatOlmInstanceHandler.HEAD_NPC_ID) {
			if (boss != null)
				boss.combatLogic(npc, c); //bypass this shizz
			return;
		}
		
		if (npc.npcType == 13447) {
			return;
		}
		
		if (checkStopAttack(npc, c)) {
			npc.setKillerId(0);
			return;
		}
		
		if (!npc.inMulti() && npc.underAttackBy > 0
				&& npc.underAttackBy != c.getId()) {
			npc.setKillerId(0);
			return;
		}
		if (!npc.inMulti() && (c.underAttackBy > 0
				|| (c.underAttackBy2 > 0 && c.underAttackBy2 != npc.getId()))) {
			npc.setKillerId(0);
			return;
		}
		if (npc.heightLevel != c.getHeightLevel()) {
			npc.setKillerId(0);
			return;
		}
		
		//some corp pi shit code
		if (npc.npcType == 8133 && !goodDistance(npc.absX, npc.absY, c.getY(), c.getX(), 1)
				&& npc.attackType == 0) {
			npc.attackType = 1 + Misc.random(1);
			return;
		}
		
		int distance = PlayerConstants.getDistance(c.getX(), npc.getX(), c.getY(), npc.getY());
		
		// if the player is 25 tiles away from the npc trying to attack them,
		// the npc will stop trying to attack them.
		if (distance > npc.getAggroRadius() && npc.underAttackBy < 1) {
			npc.setKillerId(0);
			return;
		}
		
		//c.sendMess("Defence: " + npc.defence + " Attack: " + npc.attack + " Maxhit: " + npc.maxHit + " HP: " + npc.getSkills().getLevel(Skills.HITPOINTS));
		
		npc.face(c);
		if (npc.resetAttackType)
			npc.attackType = 0;
		npc.projectileId = -1;
		npc.endGfx = -1;
		
		NPCAttackDefinition attackDef = NPCAttackLoader.get(npc.npcType);
		
		Attack attack = null;
		
		if (boss != null) {
			boss.combatLogic(npc, c);
		} else {
			attack = NPCCombatData.loadSpell(c, npc);
			
			if (attack == null) {
				loadSpell(c, npc);
			}
		}
		
		if (npc.stopAttack) {
			return;
		}
		
		if (!canNoClipAttacks(npc) && npc.resetsAggro) {
			if (RegionClip.rayTraceBlocked(npc.getXtoPlayerLoc(c.getX()), npc.getYtoPlayerLoc(c.getY()),
					c.getX(), c.getY(), npc.getHeightLevel(),
					npc.projectileId > 0 || npc.attackType != 0, c.getDynamicRegionClip())) {
				npc.resetAggroTick++;
				if (npc.resetAggroTick > 5) {
					npc.resetFace();
					npc.resetAttack();
					npc.randomWalk = true;
					npc.setWalkingHome(true);
					npc.setKillerId(0);
					
					if (npc.npcType == 7151) {
						handleGorillaAttackSwitches(npc);
					}
				}
				return;
			}
		}
		
		int distReq2 = 1;
		
		if (boss != null) {
			distReq2 += boss.extraAttackDistance();
		}
		
		distReq2 += npc.getHitOffset();
		
		boolean withinMeleeDistance = c.getPA().withinDistanceToNpc(npc, distReq2);
		
		int distanceToHit = distanceRequired(npc.getId()) + npc.getHitOffset();
		
		boolean needsToWalkUpToHitPlayer = npc.walkUpToHit;
		if (npc.attackType == 0) { // if melee attack
			if (needsToWalkUpToHitPlayer && !withinMeleeDistance) { // if needs to walk up to hit and not in striking range
				return;
			}
			if (!needsToWalkUpToHitPlayer && !withinMeleeDistance) { // this shit will look buggy with no gfxs but at least attacks will switch if coder dind't code distance check in loadSpell()
				
				if (attack != null) {
					if (attackDef != null) {
						if (attackDef.hasRangeAndMagicAttacks()) { // has both attacks, then choose between them
							if (Misc.random(1) == 0) {
								NPCAttackLoader.setMagicAttack(npc);
							} else {
								NPCAttackLoader.setRangeAttack(npc);
							}
						} else if (NPCAttackLoader.setRangeAttack(npc) != null) {
							
						} else if (NPCAttackLoader.setMagicAttack(npc) != null) {
						}
					}
					
				}
				if (npc.attackType == 0 && !npc.stopAttackDefault) { // if still does not have range abilities then block attack since he is not within melee distance
					return;
				}
			}
			if (needsToWalkUpToHitPlayer) {
				if (attack != null
						&& attackDef != null && attackDef.hasMeleeAttack()) {
					if (attackDef.hasRangeAndMagicAttacks()) { // has both attacks, then choose between them
						if (Misc.random(1) == 0) {
							NPCAttackLoader.setMagicAttack(npc);
						} else {
							NPCAttackLoader.setRangeAttack(npc);
						}
					} else if (NPCAttackLoader.setRangeAttack(npc) != null) {
						//sets range attack on npc
					} else if (NPCAttackLoader.setMagicAttack(npc) != null) {
						// sets magic attack on npc
					}
					
				}
			}
		} else { // if range/magic
			if (attack != null) { // if loadspell from npccombatData shit
				if (withinMeleeDistance) { // switching to melee attack type if nearby
					if (attackDef != null && attackDef.hasMeleeAttack()) {
						if (Misc.random(1) == 1) {
							npc.attackType = 0;
							npc.projectileId = 0;
							attack.startGfx = 0;
							npc.endGfx = 0;
							npc.currentAttackMulti = false;
						}
					}
				} else { // if distance is above 1
					if (needsToWalkUpToHitPlayer && !withinMeleeDistance) {
						return;
					}
					if (!c.getPA().withinDistanceToNpc(npc, distanceToHit)) { // not in spell range?
						return;
					}
				}
			} else { // if it's the PI loadspells shit code
				if (needsToWalkUpToHitPlayer && !withinMeleeDistance) {
					return;
				}
				if (!c.getPA().withinDistanceToNpc(npc, distanceToHit)) { // not in spell range?
					return;
				}
			}
		}
		
		if (boss != null && boss.getAttackSpeed() != -1) {
			npc.setAttackTimer(boss.getAttackSpeed());
		} else {
			npc.setAttackTimer(npc.getCombatStats().getAttackSpeed());
		}
		
		//int distance = c.distanceToPoint(npc.getX(), npc.getY());
		
		npc.resetAggroTick = 0;
		
		boolean dungMultiCode = npc.dungMultiAttack;
		boolean multiAttack = (multiAttacks(npc.getId())
				|| (attack != null && attack.multi));
		
		if (dungMultiCode && npc.dungeonManager != null) {
			for (int i = 0, length = npc.dungeonManager.party.partyMembers.size(); i < length; i++) {
				Player player = npc.dungeonManager.party.partyMembers.get(i);
				if (player.isDead() || player.hidePlayer || !npc.dungeonManager.getCurrentRoomReference(player.getCurrentLocation()).equals(npc.reference)) {
					continue;
				}
				if (RegionClip.rayTraceBlocked(npc.getXtoPlayerLoc(player.getX()),
						npc.getYtoPlayerLoc(player.getY()), player.getX(), player.getY(),
						npc.getHeightLevel(),
						npc.attackType != 0, player.getDynamicRegionClip())) {
					continue;
				}
				
				dungMultiAttackGfx(npc, player);
				
				CycleEventHandler.getSingleton().addEvent(player, true, new CycleEvent() {
					
					@Override
					public void execute(CycleEventContainer e) {
						if (npc == null || player == null) {
							e.stop();
							return;
						}
						
						multiAttackDamageNew(npc, player);
						e.stop();
					}
					
					@Override
					public void stop() {
						/* empty */
					}
					
				}, getHitDelay(npc.getId()));
			}
		} else if (multiAttack) {
			for (Player p : Server.getRegionManager()
					.getLocalPlayers(npc)) {
				if (p.isDead() || p.hidePlayer) {
					continue;
				}
				if (!p.correctlyInitialized || !p.saveFile || p.newPlayer || !p.saveCharacter || !p.setMode
						|| p.performingAction) {
					//System.out.println("Bug npc multi attaack 1: " +p.getNameSmartUp());
					continue;
				}
				if (p.goodDistance(npc, distanceToHit)) {// TODO: add proper distance check of spell here
					if (RegionClip.rayTraceBlocked(npc.getXtoPlayerLoc(p.getX()),
							npc.getYtoPlayerLoc(p.getY()), p.getX(), p.getY(),
							npc.getHeightLevel(),
							npc.attackType != 0, p.getDynamicRegionClip())) {
						continue; // return;
					}
					final Client c2 = (Client) p;
					
					c2.putInCombatWithNpc(npc.getId());
					// do gfx and damage here
					multiAttackGfx(npc, p, npc.projectileId);
					
					CycleEventHandler.getSingleton().addEvent(c2, true,
							new CycleEvent() {
								
								@Override
								public void execute(CycleEventContainer e) {
									if (!c2.correctlyInitialized || !c2.saveFile || c2.newPlayer || !c2.saveCharacter || !c2.setMode
											|| c2.performingAction) {
										//System.out.println("Bug npc multi attaack 2: " +c2.getNameSmartUp());
										e.stop();
										return;
									}
									if (npc == null || c2 == null) {
										e.stop();
									} else {
										if (!(npc.npcType >= 6142
												&& npc.npcType <= 6145)) {
											multiAttackDamage(npc, c2);
										}
										e.stop();
									}
								}
								
								@Override
								public void stop() {
								
								}
								
							}, getHitDelay(npc.getId()));
					
				}
			}
			
		} else if (npc.attackType != 5 && !npc.stopAttackDefault) { // if not corp? doesn't make sense
			
			int hitDelay = 2;
			if (npc.attackType != 0) {
				if (distance > 2) {
					hitDelay += 1;
				}
				
				if (distance > 4) {
					hitDelay += 1;
				}
				if (distance > 8) {
					hitDelay += 1;
				}
			}
			
			final Attack finalAttack = attack;
			
			CycleEventHandler.getSingleton().addEvent(c, true,
					new CycleEvent() {
						
						@Override
						public void execute(CycleEventContainer e) {
							if (npc == null || !npc.isActive || c == null) {
								e.stop();
							} else {
								if (!(npc.npcType >= 6142
										&& npc.npcType <= 6145)) {
									applyDamage(npc.getId(), c, finalAttack == null
											? false
											: finalAttack.multi, boss);
								}
								e.stop();
							}
						}
						
						@Override
						public void stop() {
						
						}
						
					}, hitDelay);
		}
		
		if (multiAttack || dungMultiCode) {
			if (attack != null && attack.animId > 0) {
				npc.startAnimation(attack.animId);
			} else {
				Animation attackEmote = NPCAnimations.getCombatEmote(npc.npcType, npc.attackType);
				if (attackEmote == null) {
					npc.forceChat("I am missing an attack emote. 2");
				} else {
					npc.startAnimation(attackEmote);
				}
			}
			return;
		}
		
		npc.startAttack(c);
		
		if (!npc.stopAttackDefault) {
			if (attack != null) {
				if (attack.gfxNew != null) {
					npc.startGraphic(attack.gfxNew);
				} else if (attack.startGfx > 0)
					npc.startGraphic(Graphic.create(attack.startGfx, GraphicType.HIGH));
			} else if (npc.attackStartGfx != null) {
				npc.startGraphic(npc.attackStartGfx);
				npc.attackStartGfx = null;
			}
		}
		
		if (!multiAttack && !npc.stopAttackDefault) {
			if (attack != null && attack.projectileNew != null) {
				Projectile projectile = attack.projectileNew.copy();
				projectile.setStartLoc((npc.getSize() == 5 || npc.getSize() == 7) ? Projectile.getLocationOffset(npc.getCurrentLocation(), c.getCurrentLocation(), npc.getSize()) : npc.getCurrentLocation());
				projectile.setEndLoc(c.getCurrentLocation());
				projectile.setLockon(c.getProjectileLockon());
				projectile.setStartDelay(53 - distance);
				projectile.setEndDelay(5 * distance + 55);
				GlobalPackets.createProjectile(projectile);
			} else if (npc.projectileId > 0) {
				Location start = (npc.getSize() == 5 || npc.getSize() == 7) ? Projectile.getLocationOffset(npc.getCurrentLocation(), c.getCurrentLocation(), npc.getSize()) : Location.create(npc.getX() + offset(npc, c.getCurrentLocation(), true),
						npc.getY() + offset(npc, c.getCurrentLocation(), false),
						npc.getHeightLevel());
				Projectile proj = new Projectile(npc.projectileId,
						start,
						Location.create(c.getX(), c.getY(), c.getZ()));
				proj.setStartHeight(npc.projectiletStartHeight);//43
				proj.setEndHeight(npc.projectiletEndHeight);
				proj.setStartDelay(npc.projectileDelay);
				proj.setLockon(c.getProjectileLockon());
				proj.setSlope(npc.projectiletSlope);
//				proj.setSpeed(100);
				GlobalPackets.createProjectile(proj);
				
			}
		}
		
		c.putInCombatWithNpc(npc.getId());
		
		if (!npc.stopAttackDefault) {
			if (npc.npcType != 8133 /* && !usingSpecial */) {
				if (attack != null && attack.animId > 0) {
					npc.startAnimation(attack.animId);
				} else if (npc.attackAnim != null) {
					npc.startAnimation(npc.attackAnim);
				} else {
					Animation attackEmote = NPCAnimations.getCombatEmote(npc.npcType, npc.attackType);
					
					if (attackEmote == null && boss != null && boss.getAttackAnimation(npc.attackType) != -1) {
						attackEmote = new Animation(boss.getAttackAnimation(npc.attackType));
					}
					
					if (attackEmote == null && !(npc.npcType >= 10560 && npc.npcType <= 10603)) {
						npc.forceChat("I am missing an attack emote. 3");
					} else {
						npc.startAnimation(attackEmote);
					}
				}
			}
		}

		npc.stuckCount = 0;
		if (c.interfaceIdOpenMainScreen != PowerUpInterface.ID) {
			c.getPA().closeAllWindows();
		}

		Degrade.degradeArmor(c);
		
		if (npc.isVenomousNpc() && Misc.random(99) < 25) {
			c.getDotManager().applyVenom();
		}
	}
	
	public static boolean canNoClipAttacks(NPC npc) {
		if (!npc.isClipAttacks())
			return true;
		if (NPCHandler.isDefiler(npc.npcType) || NPCHandler.isTorcher(npc.npcType)) {
			return true;
		}
		
		if (npc.npcType == 9176/*
		 * || npcType == 4174 || npcType == 4172 || npcType
		 * == 4173
		 */) { // wildy bosses no clip attacks
			return true;
		}
		if (npc.npcType == 8528) {
			return true;
		}
		if (npc.npcType == 8970 || npc.npcType == 8971) {
			return true;
		}
		if (npc.npcType == 8133) {
			return true;
		}
		return false;
	}
	
	public static boolean checkStopAttack(NPC npc, Client p) {
		if (npc.npcType == 8596 && !RSConstants.inSoulWarsRedBoss(p.absX, p.absY)) {
			return true;
		}
		
		if (npc.npcType == 8597 && !RSConstants.inSoulWarsBlueBoss(p.absX, p.absY)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Distanced required to attack
	 **/
	public static int distanceRequired(int i) { // TODO: this is a stupid way of doing this shit. needs to be fixed asap.
		if (npcs[i] == null) { // cheaphax to block the nulled npcs, i dunno what is nulling them too early.
			return 1;
		}
		if (npcs[i].specialAttackType == 1) { // avatar slams prayers off
			npcs[i].specialAttackType = -1;
			return 10;
		}
		
		if (npcs[i].attackType != 3 && npcs[i].walkUpToHit) { // ignore dragon attack shit
			return npcs[i].getDistanceRules().getMeleeDistance();
		}
		
		return npcs[i].getDistanceRules().getDistanceRequiredToHit(npcs[i].attackType);
	}
	
	/**
	 * Dropping Items!
	 **/
	public static void dropItems(final Client c, final NPC n) {
		if (n == null || n.npcType < 0 || !n.isDropsItemOnDeath() || DropTable.isException(n.npcType)) {
			return;
		}

		DropTableManager.doStuff(n, c);
		NPCHandler.randomEventDrop(c, n);
		
		if (n.npcType == 5902 || n.npcType == 5996 || n.npcType == 4972
				|| n.npcType == 13646 || n.npcType == 8834 || n.npcType == 8833
				|| n.npcType == 8377) {
			DropTableItem[] drop = DropTable.getDrop(n.npcType, c, false);
			boolean dropped = false;
			for (DropTableItem item : drop) {
				if (item != null) {
					c.getItems().addItem(item.getItemId(), item.getAmount());
					dropped = true;
				}
			}
			if (dropped) {
				c.sendMessage("The monster dropped a statuette!"); // Etc
			}
		} else {
			DropTable.handleDrops(c, n);
		}
	}

	public static void randomEventDrop(Client c, NPC n) {
		if (WorldType.equalsType(WorldType.SPAWN) || c.insideDungeoneering() || c.raidsManager != null || c.infernoManager != null || c.tobManager != null)
			return;

		// CHRISTMAS SNOWMAN HATS
		// if (Misc.random(100) == 7) {
		// ItemHandler.createGroundItem(c.getName(), 11955+Misc.random(4),
		// n.absX, n.absY, n.heightLevel, 1, true, c.isIronMan());
		// }
		
		int x, y, z;
		
		if (!RegionClip.canStandOnSpot(n.getX(), n.getY(), n.getZ(), n.getDynamicRegionClip())) {
			x = c.getX();
			y = c.getY();
			z = c.getZ();
		} else {
			x = n.getX();
			y = n.getY();
			z = n.getZ();
		}
		
		if (Config.isValentines()) { // VALENTINES SHIT
			if (Misc.random(10) == 1) {
				ItemHandler.createGroundItem(c.getName(), 744, x, y,
						z, 1, true, c.isIronMan());
				c.sendMessage("A shining object drops to the ground.");
			}
		}
		
		if (n.getEntityDef().getName().contains("Pyrefiend")
				|| n.getEntityDef().getName().contains("Forgotten")
				|| n.getEntityDef().getName().contains("Giant rat")
				|| n.getEntityDef().getName().contains("Barricade")
				|| (n.npcType == 1636) || (n.npcType == 1639)
				|| (n.npcType == 8596) || (n.npcType == 8597))
			return;
		
		int baseHp = n.getSkills().getStaticLevel(Skills.HITPOINTS);

//		EasterBunnyQuestPlugin.dropRandomItem(c, x, y, z, baseHp);
		
		if (TreasureTrailManager.enabled) {
			if (baseHp > 101) {
				if (Misc.random(128) == 0 && !c.getTreasureTrailManager().hasClue(ClueDifficulty.MASTER)) {
					ItemHandler.createGroundItem(c.getName(), ClueDifficulty.MASTER.getScrollId(), x, y, z, 1, true, c.isIronMan());
				}
			} else if (baseHp >= 51 && baseHp < 101) {
				if (Misc.random(128) == 0 && !c.getTreasureTrailManager().hasClue(ClueDifficulty.ELITE)) {
					ItemHandler.createGroundItem(c.getName(), ClueDifficulty.ELITE.getScrollId(), x, y, z, 1, true, c.isIronMan());
				}
			} else if (baseHp >= 35 && baseHp < 51) {
				if (Misc.random(128) == 0 && !c.getTreasureTrailManager().hasClue(ClueDifficulty.HARD)) {
					ItemHandler.createGroundItem(c.getName(), ClueDifficulty.HARD.getScrollId(), x, y, z, 1, true, c.isIronMan());
				}
			} else if (baseHp >= 20 && baseHp < 35) {
				if (Misc.random(128) == 0 && !c.getTreasureTrailManager().hasClue(ClueDifficulty.MEDIUM)) {
					ItemHandler.createGroundItem(c.getName(), ClueDifficulty.MEDIUM.getScrollId(), x, y, z, 1, true, c.isIronMan());
				}
			} else {
				if (Misc.random(128) == 0 && !c.getTreasureTrailManager().hasClue(ClueDifficulty.EASY)) {
					ItemHandler.createGroundItem(c.getName(), ClueDifficulty.EASY.getScrollId(), x, y, z, 1, true, c.isIronMan());
				}
			}
		}
		
		if (baseHp < 51) {
			if (Misc.random(4) == 1) {
				ItemHandler.createGroundItem(c.getName(), 995, x,
						y, z, Misc.random(25000), true,
						c.isIronMan());
			}
		} else if (baseHp >= 51 && baseHp < 90) {
			if (Misc.random(5) == 1) {
				ItemHandler.createGroundItem(c.getName(), 995, x,
						y, z, Misc.random(40000), true,
						c.isIronMan());
			}
		} else if (baseHp >= 90 && baseHp <= 160) {
			if (Misc.random(6) == 1) {
				ItemHandler.createGroundItem(c.getName(), 995, x,
						y, z, Misc.random(80000), true,
						c.isIronMan());
			}
		} else {
			if (Misc.random(8) == 1) {
				ItemHandler.createGroundItem(c.getName(), 995, x,
						y, z, Misc.random(200000), true,
						c.isIronMan());
			}
		}

		if (baseHp < 51) {
			if (Misc.random(500) == 1) {
				ItemHandler.createGroundItem(c.getName(), 18778, x,
						y, z, 1, true, c.isIronMan());
			}
		} else if (baseHp >= 51 && baseHp < 90) {
			if (Misc.random(400) == 1) {
				ItemHandler.createGroundItem(c.getName(), 18778, x,
						y, z, 1, true, c.isIronMan());
			}
		} else if (baseHp >= 90 && baseHp <= 160) {
			if (Misc.random(300) == 1) {
				ItemHandler.createGroundItem(c.getName(), 18778, x,
						y, z, 1, true, c.isIronMan());
			}
		} else {
			if (Misc.random(250) == 1) {
				ItemHandler.createGroundItem(c.getName(), 18778, x,
						y, z, 1, true, c.isIronMan());
			}
		}
		
		if (GOLDEN_CHEST_DROP) {
			if (baseHp < 51) {
				if (Misc.random(6000) == 1) {
					ItemHandler.createGroundItem(c.getName(), 6759, x, y, z, 1, true, c.isIronMan());
					PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " has found a Golden Chest.");
				}
			} else if (baseHp >= 51 && baseHp < 90) {
				if (Misc.random(5300) == 1) {
					ItemHandler.createGroundItem(c.getName(), 6759, x, y, z, 1, true, c.isIronMan());
					PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " has found a Golden Chest.");
				}
			} else if (baseHp >= 90 && baseHp <= 160) {
				if (Misc.random(4500) == 1) {
					ItemHandler.createGroundItem(c.getName(), 6759, x, y, z, 1, true, c.isIronMan());
					PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " has found a Golden Chest.");
				}
			} else {
				if (Misc.random(4000) == 1) {
					ItemHandler.createGroundItem(c.getName(), 6759, x, y, z, 1, true, c.isIronMan());
					PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " has found a Golden Chest.");
				}
			}
		}
		if (c.isIronMan()) {
			if (baseHp < 51) {
				if (Misc.random(12000) == 1) {
					ItemHandler.createGroundItem(c.getName(), 4273, x, y, z, 1, true,
							c.isIronMan());
					PlayerHandler
							.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " Has found a Golden Key.");
				}
			} else if (baseHp >= 51 && baseHp < 90) {
				if (Misc.random(10600) == 1) {
					ItemHandler.createGroundItem(c.getName(), 4273, x, y, z, 1, true,
							c.isIronMan());
					PlayerHandler
							.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " Has found a Golden Key.");
				}
			} else if (baseHp >= 90 && baseHp <= 160) {
				if (Misc.random(9000) == 1) {
					ItemHandler.createGroundItem(c.getName(), 4273, x, y, z, 1, true,
							c.isIronMan());
					PlayerHandler
							.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " Has found a Golden Key.");
				}
			} else {
				if (Misc.random(8000) == 1) {
					ItemHandler.createGroundItem(c.getName(), 4273, x, y, z, 1, true,
							c.isIronMan());
					PlayerHandler
							.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " Has found a Golden Key.");
				}
			}
		}
	}
	
	public static void followNpc(int i, int followIndex) {
		if (NPCHandler.npcs[followIndex] == null) {
			return;
		}
		
		if (npcs[i].npcType == 3782) {
			return;
		}
		
		if (!npcs[i].canWalk()) {
			return;
		}
		
		if (NPCHandler.npcs[followIndex].isDead()) {
			npcs[i].resetFace();
			npcs[i].randomWalk = true;
			return;
		}
		
		final int followX = NPCHandler.npcs[followIndex].absX;
		final int followY = NPCHandler.npcs[followIndex].absY;
		final int npcX = npcs[i].absX;
		final int npcY = npcs[i].absY;
		npcs[i].randomWalk = false;
		
		if (goodDistance(followX, followY, npcX, npcY, distanceRequired(i))) {
			return;
		}

		/*if (npcs[i].isRevenantNpc() && npcY < 3544) { // wildy level 4
			npcs[i].resetFace();
			npcs[i].randomWalk = true;
			npcs[i].setWalkingHome(true);
			npcs[i].killerId = 0;
			return;
		}*/
		if ((npcs[i].spawnedBy > 0) || ((npcX < npcs[i].makeX
				+ Config.NPC_FOLLOW_DISTANCE + npcs[i].getDistanceRules().getFollowDistance())
				&& (npcX > npcs[i].makeX - Config.NPC_FOLLOW_DISTANCE
				- npcs[i].getDistanceRules().getFollowDistance())
				&& (npcY < npcs[i].makeY + Config.NPC_FOLLOW_DISTANCE
				+ npcs[i].getDistanceRules().getFollowDistance())
				&& (npcY > npcs[i].makeY - Config.NPC_FOLLOW_DISTANCE
				- npcs[i].getDistanceRules().getFollowDistance()))) {
			if (npcs[i].heightLevel == NPCHandler.npcs[followIndex].heightLevel) {
				if (NPCHandler.npcs[followIndex] != null && npcs[i] != null) {
					if (followY < npcY) {
						npcs[i].moveX = GetMove(npcX, followX);
						npcs[i].moveY = GetMove(npcY, followY);
					} else if (followY > npcY) {
						npcs[i].moveX = GetMove(npcX, followX);
						npcs[i].moveY = GetMove(npcY, followY);
					} else if (followX < npcX) {
						npcs[i].moveX = GetMove(npcX, followX);
						npcs[i].moveY = GetMove(npcY, followY);
					} else if (followX > npcX) {
						npcs[i].moveX = GetMove(npcX, followX);
						npcs[i].moveY = GetMove(npcY, followY);
					} else if (followX == npcX || followY == npcY) {
						int o = Misc.random(3);
						switch (o) {
							case 0:
								npcs[i].moveX = GetMove(npcX, followX);
								npcs[i].moveY = GetMove(npcY, followY + 1);
								break;
							
							case 1:
								npcs[i].moveX = GetMove(npcX, followX);
								npcs[i].moveY = GetMove(npcY, followY - 1);
								break;
							
							case 2:
								npcs[i].moveX = GetMove(npcX, followX + 1);
								npcs[i].moveY = GetMove(npcY, followY);
								break;
							
							case 3:
								npcs[i].moveX = GetMove(npcX, followX - 1);
								npcs[i].moveY = GetMove(npcY, followY);
								break;
						}
					}
					npcs[i].face(NPCHandler.npcs[followIndex]);
					
				}
			}
		} else {
			npcs[i].resetFace();
			npcs[i].randomWalk = true;
		}
	}
	
	/**
	 * @param me
	 * @param toFollow
	 */
	public static void followNpcCB(int me, int toFollow) {
		final NPC followNpc = npcs[toFollow];
		final NPC npc = npcs[me];
		if (followNpc == null) {
			return;
		}
		if (!npcs[me].canWalk()) {
			return;
		}
		if (npc.npcType == 3782) {
			return;
		}
		if (npc.underAttackBy == 0) {
			
			int playerX = followNpc.getX();
			int playerY = followNpc.getY();
			npc.randomWalk = false;
			npc.turnNpc(followNpc.getX(), followNpc.getY());
			boolean sameSpot = (npcs[toFollow].getX() == npcs[me].getX()
					&& npcs[toFollow].getY() == npcs[me].getY());
			if (sameSpot) {
				stepAway(npcs[me]);
				npcs[me].face(followNpc);
				return;
			}
			if (npc.heightLevel == followNpc.heightLevel && goodDistance(
					npc.getX(), npc.getY(), playerX, playerY, 8)) {
				npcs[me].turnNpc(playerX, playerY);
				if (followNpc != null && npc != null) {
					if (playerY < npc.absY) {
						npc.moveX = GetMove(npc.absX, playerX);
						npc.moveY = GetMove(npc.absY, playerY + 1);
					} else if (playerY > npc.absY) {
						npc.moveX = GetMove(npc.absX, playerX);
						npc.moveY = GetMove(npc.absY, playerY - 1);
					} else if (playerX < npc.absX) {
						npc.moveX = GetMove(npc.absX, playerX + 1);
						npc.moveY = GetMove(npc.absY, playerY);
					} else if (playerX > npc.absX) {
						npc.moveX = GetMove(npc.absX, playerX - 1);
						npc.moveY = GetMove(npc.absY, playerY);
					} else if (playerX == npc.absX || playerY == npc.absY) {
						int o = Misc.random(3);
						switch (o) {
							case 0:
								npc.moveX = GetMove(npc.absX, playerX);
								npc.moveY = GetMove(npc.absY, playerY + 1);
								break;
							
							case 1:
								npc.moveX = GetMove(npc.absX, playerX);
								npc.moveY = GetMove(npc.absY, playerY - 1);
								break;
							
							case 2:
								npc.moveX = GetMove(npc.absX, playerX + 1);
								npc.moveY = GetMove(npc.absY, playerY);
								break;
							
							case 3:
								npc.moveX = GetMove(npc.absX, playerX - 1);
								npc.moveY = GetMove(npc.absY, playerY);
								break;
						}
					}
				}
			} else {
				npc.underAttackBy = 0;
			}
			npcs[me].turnNpc(playerX, playerY);
			npc.randomWalk = false;
		}
	}
	
	public static void FollowNpcCB(int i, int z) {
		final NPC c = npcs[z];
		final NPC n = npcs[i];
		if (!npcs[i].canWalk()) {
			return;
		}
		if (n.underAttackBy == 0) {
			
			int playerX = c.getX();
			int playerY = c.getY();
			n.randomWalk = false;
			npcs[i].turnNpc(playerX, playerY);
			if (n.heightLevel == c.heightLevel
					&& goodDistance(n.getX(), n.getY(), playerX, playerY, 8)) {
				npcs[i].turnNpc(playerX, playerY);
				if (c != null && n != null) {
					if (playerY < n.absY) {
						n.moveX = GetMove(n.absX, playerX);
						n.moveY = GetMove(n.absY, playerY + 1);
					} else if (playerY > n.absY) {
						n.moveX = GetMove(n.absX, playerX);
						n.moveY = GetMove(n.absY, playerY - 1);
					} else if (playerX < n.absX) {
						n.moveX = GetMove(n.absX, playerX + 1);
						n.moveY = GetMove(n.absY, playerY);
					} else if (playerX > n.absX) {
						n.moveX = GetMove(n.absX, playerX - 1);
						n.moveY = GetMove(n.absY, playerY);
					} else if (playerX == n.absX || playerY == n.absY) {
						int o = Misc.random(3);
						switch (o) {
							case 0:
								n.moveX = GetMove(n.absX, playerX);
								n.moveY = GetMove(n.absY, playerY + 1);
								break;
							
							case 1:
								n.moveX = GetMove(n.absX, playerX);
								n.moveY = GetMove(n.absY, playerY - 1);
								break;
							
							case 2:
								n.moveX = GetMove(n.absX, playerX + 1);
								n.moveY = GetMove(n.absY, playerY);
								break;
							
							case 3:
								n.moveX = GetMove(n.absX, playerX - 1);
								n.moveY = GetMove(n.absY, playerY);
								break;
						}
					}
					npcs[i].turnNpc(playerX, playerY);
				}
			} else {
				// c.getSummoning().callFamiliar();
				n.underAttackBy = 0;
			}
			npcs[i].turnNpc(playerX, playerY);
			n.randomWalk = false;
		}
	}

	public static boolean allowedToFollowPlayer(NPC npc) {
		if (!npc.isRetaliates()) {
			return false;
		}

		switch (npc.npcType) {
			case 2892:
			case 2894:
				return false;
		}

		return true;
	}

	public static void followPlayer(NPC n, Player c) {
		if (n == null || !n.isActive || n.isDead() || !n.canWalk()) // just in case someone makes a bug
			return;

		if (c == null || c.respawnTimer > 0) {
			n.resetFollow();
			n.setKillerId(0);
			return;
		}

		if (!allowedToFollowPlayer(n)) {
			n.resetFollow();
			n.setKillerId(0);
			return;
		}

		if (n.isPetNpc()) {
			if (!PlayerConstants.goodDistance(c.getCurrentLocation().getX(),
					c.getCurrentLocation().getY(), n.getX(), n.getY(), 8)) {
				c.getSummoning().callOnTeleport();
				return;
			}
		}
		
		final int playerX = c.absX;
		final int playerY = c.absY;
		final int npcX = n.absX;
		final int npcY = n.absY;
		
		final int distanceReq = distanceRequired(n.getId());
		
		final boolean diagonal = Misc.isDiagonal(playerX, playerY, npcX, npcY);
		
		n.randomWalk = false;
		
		if (diagonal && Misc.distanceToPoint(playerX, playerY, npcX, npcY) <= distanceReq) {
			return;
		}
		
		final int sizeOff = n.getSize() - 1;
		if (c.getX() >= n.getX() && c.getX() <= n.getX() + sizeOff &&
				c.getY() >= n.getY() && c.getY() <= n.getY() + sizeOff) {//if (Location.overlaps(c.getX(), c.getY(), 1, n.getX(), n.getY(), n.getSize())) { // npc inside player
			stepBack(n, c);
//			n.forceChat("stepping back" + Misc.random(10000) + " n.size:"+n.getSize());
			if (n.moveX == 0 && n.moveY == 0)
				stepAway(n);
			return;
		}
		
		if (c.withinDistance(n.getXYtoPlayerLoc(c.getX(), true), n.getXYtoPlayerLoc(c.getY(), false), n.getZ(), distanceReq)) { //if (n.getCurrentLocation().isWithinDeltaDistance(c.getCurrentLocation(), distanceReq)) {
			// diagonal check for melee hits
			if (n.attackType == 0) {
				if (((playerX >= npcX && playerX <= npcX + sizeOff) || (playerY >= npcY && playerY <= npcY + sizeOff)))
					return;
			} else {
				return;
			}
		}
		
		/*if (n.isRevenantNpc() && npcY < 3544) { // wildy level 4
			n.resetFollow();
			n.setWalkingHome(true);
			n.killerId = 0;
			return;
		}*/
		
		if ((n.spawnedBy > 0) || ((n.absX < n.makeX
				+ Config.NPC_FOLLOW_DISTANCE + n.getDistanceRules().getFollowDistance())
				&& (n.absX > n.makeX - Config.NPC_FOLLOW_DISTANCE
				- n.getDistanceRules().getFollowDistance())
				&& (n.absY < n.makeY + Config.NPC_FOLLOW_DISTANCE
				+ n.getDistanceRules().getFollowDistance())
				&& (n.absY > n.makeY - Config.NPC_FOLLOW_DISTANCE
				- n.getDistanceRules().getFollowDistance()))) {
			if (n.heightLevel == c.heightLevel) {
				if (playerY < n.getYtoPlayerLoc(playerY)) {
					n.moveX = GetMove(n.getXtoPlayerLoc(playerX), playerX);
					n.moveY = GetMove(n.getYtoPlayerLoc(playerY), playerY);
				} else if (playerY > n.getYtoPlayerLoc(playerY)) {
					n.moveX = GetMove(n.getXtoPlayerLoc(playerX), playerX);
					n.moveY = GetMove(n.getYtoPlayerLoc(playerY), playerY);
				} else if (playerX < n.getXtoPlayerLoc(playerX)) {
					n.moveX = GetMove(n.getXtoPlayerLoc(playerX), playerX);
					n.moveY = GetMove(n.getYtoPlayerLoc(playerY), playerY);
				} else if (playerX > n.getXtoPlayerLoc(playerX)) {
					n.moveX = GetMove(n.getXtoPlayerLoc(playerX), playerX);
					n.moveY = GetMove(n.getYtoPlayerLoc(playerY), playerY);
				} else if (playerX == n.absX
						|| playerY == n.absY) {
					int o = Misc.random(3);
					switch (o) {
						case 0:
							n.moveX = GetMove(n.absX, playerX);
							n.moveY = GetMove(n.absY,
									playerY + 1);
							break;
						
						case 1:
							n.moveX = GetMove(n.absX, playerX);
							n.moveY = GetMove(n.absY,
									playerY - 1);
							break;
						
						case 2:
							n.moveX = GetMove(n.absX,
									playerX + 1);
							n.moveY = GetMove(n.absY, playerY);
							break;
						
						case 3:
							n.moveX = GetMove(n.absX,
									playerX - 1);
							n.moveY = GetMove(n.absY, playerY);
							break;
					}
				}

				n.face(c);
			} else {
				n.resetFollow();
				n.setKillerId(0);
				if (n.walksHome())
					n.setWalkingHome(true);
			}
		} else {
			n.resetFollow();
			n.setKillerId(0);
			if (n.walksHome())
				n.setWalkingHome(true);
		}
	}
	
	/**
	 * Npc following player
	 */
	public static void followEnemyPlayer(NPC npc, int playerId, Client ownerOfNPC) {
		if (PlayerHandler.players[playerId] == null) {
			return;
		}
		Player enemyPlayer = PlayerHandler.players[playerId];
		
		if (!npc.canWalk()) {
			return;
		}
		if (npc.underAttackBy != 0 || ownerOfNPC.underAttackBy != 0
				|| ownerOfNPC.playerIndex != 0) {
			if (!enemyPlayer.isAlive()) {
				npc.resetFace();
				npc.randomWalk = true;
				return;
			}
//			if (!allowedToFollowPlayer(i)) {
//				if (!npc.IsAttackingNPC) {
//					npc.facePlayer(playerId);
//				}
//				return;
//			}
			int playerX = enemyPlayer.getCurrentLocation().getX();
			int playerY = enemyPlayer.getCurrentLocation().getY();
			npc.randomWalk = false;
			boolean sameSpot = (playerX == npc.absX
					&& playerY == npc.absY);
			if (playerX >= npc.absX
					&& playerX <= npc.absX + (npc.getSize() - 1)
					&& playerY >= npc.absY
					&& playerY <= npc.absY + (npc.getSize() - 1)) {
				sameSpot = true;
			}
			if (sameSpot) {
				stepAway(npc);
				if (npc.killNpc == -1) { //not attacking npc
					npc.face(enemyPlayer);
				}
				return;
			}
			if (goodDistance(npc.absX, npc.absY, playerX, playerY,
					distanceRequired(npc.getId()))) {
				return;
			}
			if ((npc.spawnedBy > 0) || ((npc.absX < npc.makeX
					+ npc.getDistanceRules().getFollowDistance())
					&& (npc.absX > npc.makeX
					- npc.getDistanceRules().getFollowDistance())
					&& (npc.absY < npc.makeY
					+ npc.getDistanceRules().getFollowDistance())
					&& (npc.absY > npc.makeY
					- npc.getDistanceRules().getFollowDistance()))) {
				if (npc.heightLevel == enemyPlayer
						.getHeightLevel()) {
					if (enemyPlayer != null
							&& npc != null) {
						if (playerY < npc.absY) {
							npc.moveX = GetMove(npc.absX, playerX);
							npc.moveY = GetMove(npc.absY, playerY + 1);
						} else if (playerY > npc.absY) {
							npc.moveX = GetMove(npc.absX, playerX);
							npc.moveY = GetMove(npc.absY, playerY - 1);
						} else if (playerX < npc.absX) {
							npc.moveX = GetMove(npc.absX, playerX + 1);
							npc.moveY = GetMove(npc.absY, playerY);
						} else if (playerX > npc.absX) {
							npc.moveX = GetMove(npc.absX, playerX - 1);
							npc.moveY = GetMove(npc.absY, playerY);
						} else if (playerX == npc.absX
								|| playerY == npc.absY) {
							int o = Misc.random(3);
							switch (o) {
								case 0:
									npc.moveX = GetMove(npc.absX,
											playerX);
									npc.moveY = GetMove(npc.absY,
											playerY + 1);
									break;
								case 1:
									npc.moveX = GetMove(npc.absX,
											playerX);
									npc.moveY = GetMove(npc.absY,
											playerY - 1);
									break;
								case 2:
									npc.moveX = GetMove(npc.absX,
											playerX + 1);
									npc.moveY = GetMove(npc.absY,
											playerY);
									break;
								case 3:
									npc.moveX = GetMove(npc.absX,
											playerX - 1);
									npc.moveY = GetMove(npc.absY,
											playerY);
									break;
							}
						}
						
						if (npc.IsAttackingNPC != true) {
							npc.face(enemyPlayer);
						}
					}
				}
			}
		}
	}

//	public static int getClosePlayer(int i) {
//		if (npcs[i].spawnedBy > 0) {
//			return npcs[i].spawnedBy;
//		}
//		final NPC npc = npcs[i];
//		for (Player p : Server.getRegionManager().getLocalPlayers(npc)) {
//			if (p != null) {
//				if ((p.getHitPoints() > 0
//						&& goodDistance(p, npc,
//								2 + distanceRequired(i) + npcs[i].getDistanceRules().getFollowDistance()))
//						|| npc.isBarbAssaultNpc()) {
//					if ((p.underAttackBy <= 0 && p.underAttackBy2 <= 0)
//							|| p.inMulti()) {
//						if (pathBlocked(npc, p) && !npc.isBarbAssaultNpc()) {
//							continue;
//						}
//						if (p.hidePlayer) {
//							continue; // avoid invisible players
//						}
//						return p.getId();
//					}
//				}
//			}
//		}
//		return 0;
//	}
	
	private static final IntList cachedPlayers = new IntArrayList(2000);
	
	public static int getCloseRandomPlayer(int i) {
		return getCloseRandomPlayer(i, false);
	}
	
	public static int getCloseRandomPlayer(int i, boolean noClip) {
		return getCloseRandomPlayer(i, noClip, false);
	}
	
	private static void acceptCloseRandomPlayer(final IntList players, final Player p, final NPC npc, final boolean noClip) {
		if ((p.getHitPoints() > 0
				&& goodDistance(p, npc, npc.getAggroRadius()))) {
			if ((p.underAttackBy <= 0 && p.underAttackBy2 <= 0)
					|| p.inMulti()) {
				if (!noClip && !p.inPcGame() &&
						RegionClip.rayTraceBlocked(npc.getX(), npc.getY(), p.getX(), p.getY(), npc.getZ(), !npc.walkUpToHit, npc.getDynamicRegionClip())) {
					return;
				}
				if (npc.npcType == 8596) { // red team avatar from soulwars
					if (p.soulWarsTeam == Team.RED || p.soulWarsTeam == Team.NONE)
						return;
				} else if (npc.npcType == 8597) { // blue team avatar for soulwars
					if (p.soulWarsTeam == Team.BLUE || p.soulWarsTeam == Team.NONE)
						return;
				}
				if (npc.bossRoomNpc) {
					if (p.inBossRoom && !p.hidePlayer) {
						players.add(p.getId());
					}
				} else {
					if (isAggro(p, npc)) {
						players.add(p.getId());
					}
				}
			}
		}
	}
	
	public static int getCloseRandomPlayer(int i, boolean noClip, boolean useCached) {
		final NPC npc = npcs[i];
		if (npc == null) {
			return -1;
		}
		
		if (npc.spawnedBy > 0) {
			return npc.spawnedBy;
		}
		
		final IntList players;
		if (useCached) {
			players = cachedPlayers;
			players.clear();
			final ObjectList<Player> ps = Server.getRegionManager().getLocalPlayersOptimized(npc);
			for (int j = 0, len = ps.size(); j < len; j++) {
				final Player p = ps.get(j);
				if (p != null) {
					acceptCloseRandomPlayer(players, p, npc, noClip);
				}
			}
		} else {
			Collection<Player> regionPlayers = Server.getRegionManager().getLocalPlayers(npc);
			players = new IntArrayList(regionPlayers.size());
			for (Player p : Server.getRegionManager().getLocalPlayers(npc)) {
				if (p != null) {
					acceptCloseRandomPlayer(players, p, npc, noClip);
				}
			}
		}

		int length = players.size();
		if (length > 0) {
			int index;
			if (npc.selectAggressionPlayer != null) {
				index = npc.selectAggressionPlayer.apply(players);
			} else {
				index = players.getInt(Misc.randomNoPlus(length));
			}

			return index;
		}

		return -1;
	}
	
	private static int getDistance(int x1, int x2, int y1, int y2) {
		return (int) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}
	
	/**
	 * Hit delays for multi damage attacks
	 **/
	public static int getHitDelay(int i) {
		if (npcs[i] == null) {
			return 3;
		}
		
		if (npcs[i].attackType == 0) {
			return 2;
		} else if (npcs[i].attackType == 1) {
			return 3;
		} else if (npcs[i].attackType == 2) {
			return 4;
		} else {
			return 3;
		}
	}
	
	public static int getMaxHit(int i) { // multi attack max hit damage
		switch (npcs[i].npcType) {
			
			case 6090:
				if (npcs[i].attackType == 2) {
					return 20;
				} else if (npcs[i].attackType == 0) {
					return 25;
				}
			case 5247:
				if (npcs[i].attackType == 0) {
					return 13;
				} else if (npcs[i].attackType == 1) {
					return 13;
				}
			case 8528:
				if (npcs[i].attackType == 1) {
					return 45;
				} else if (npcs[i].attackType == 2) {
					return 60;
				} else if (npcs[i].attackType == 0) {
					return 20;
				}
			case 8529:
				if (npcs[i].attackType == 1) {
					return 20;
				} else if (npcs[i].attackType == 2) {
					return 20;
				} else if (npcs[i].attackType == 0) {
					return 20;
				}
			case 9176:
				if (npcs[i].attackType == 0) {
					return 45;
				}
			case 6203:
				if (npcs[i].attackType == 0) { // special emote 14412 //magic -14418
					return 40;
				} else {
					return 35;
				}
			case 6222:
				if (npcs[i].attackType == 2) {
					return 28;
				} else {
					return 68;
				}
			case 6247:
				return 31;
			case 6260:
				if (npcs[i].attackType == 0) {
					return 60;
				} else if (npcs[i].attackType == 1) {
					return 35;
				}
			case 4540:
				if (npcs[i].attackType == 1) {
					return 50;
				} else if (npcs[i].attackType == 2) {
					return 35;
				}
			case 4174:
				if (npcs[i].attackType == 2) {
					return 60;
				} else if (npcs[i].attackType == 1) {
					return 3;
				} else if (npcs[i].attackType == 0) {
					return 20;
				}
			case 4173:
				return 50;
			case 10080:
				if (npcs[i].attackType == 1) {
					return 10;
				} else if (npcs[i].attackType == 2) {
					return 12;
				}
			case 12743:
				if (npcs[i].attackType == 1) {
					return 71;
				} else if (npcs[i].attackType == 2) {
					return 88;
				} else {
					return 67;
				}
			case 9771:
				if (npcs[i].attackType == 1) {
					return 21;
				}
			case 12805:
				;
				if (npcs[i].attackType == 2) {
					return 20;
				}
			case 8133:// corp
				if (npcs[i].attackType == 0) {
					return 48;
				} else if (npcs[i].attackType == 1) {
					return 48;
				} else if (npcs[i].attackType == 2) {
					return 60;
				}
			case 8349: // tormented demons - melee protect
			case 8350: // tormented demons - mage protect
			case 8351: // tormented demons - range protect
				if (npcs[i].attackType == 0) {
					return 30;
				} else if (npcs[i].attackType == 1) {
					return 33;
				} else if (npcs[i].attackType == 2) {
					return 33;
				}
				// fight caves monsters
			case 2631:
				if (npcs[i].attackType == 0) {
					return 13;
				} else if (npcs[i].attackType == 1) {
					return 17;
				}
		}
		return 1;
	}
	
	/**
	 * Npc Follow Player
	 **/
	
	public static int GetMove(int from, int to) {
		if ((from - to) == 0) {
			return 0;
		} else if ((from - to) < 0) {
			return 1;
		} else if ((from - to) > 0) {
			return -1;
		}
		return 0;
	}

	/**
	 * Attack delays
	 **/
	public static int getNpcDelay(int id) {
		switch (id) {
			
			case BarbarianAssault.PENANCE_HEALER:
				return 3;
			
			case 2025:
			case 2028:
			case 8133: // corp beast
				return 7;
			
			case 1158:
			case 1160:
				return 6;
			case 2745:
			case 12743:
			case 8777: // chaos dwarf hand cannoneer
				return 8;
			
			case 50:
			case 10815:
			case 10816:
			case 10817:
			case 10818:
			case 10819:
			case 10820:
			case 53:
			case 10219:
			case 10220:
			case 10221:
			case 10222:
			case 10223:
			case 10224:
			case 54:
			case 55:
			case 10604:
			case 10605:
			case 10606:
			case 10607:
			case 10608:
			case 10609:
			case 941:
			case 4677:
			case 4678:
			case 4679:
			case 4680:
			case 5362:
			case 5363:
			case 1590:
			case 10776:
			case 10777:
			case 10778:
			case 10779:
			case 10780:
			case 10781:
			case 1591:
			case 1592:
			case 3590:
			case 51:
			case 10770:
			case 10771:
			case 10772:
			case 10773:
			case 10774:
			case 10775:
			case 2011:
				return 4;
			
			case 6203:
			case 6222: // armadyl boss
			case 6223:
			case 6225:
			case 6227:
			case 6260:
				return 6;
			// saradomin gw boss
			case 6247:
				return 3;

//			case 8349: // tormented demons
//			case 8350:
//			case 8351:
//				if (npcs[i].attackType == 2) {
//					return 4;
//				} else if (npcs[i].attackType == 1) {
//					return 6;
//				} else if (npcs[i].attackType == 0) {
//					return 7;
//				}
			
			default:
				return 4;
		}
	}
	
	/**
	 * Npc Id
	 */
	
	public static int getNpcIdForCheckDrops(String name) {
		for (int i = 0; i < Config.MAX_NPC_ID; i++) {
			
			//NPCS to not check drops for.
			switch (i) {
				case 1850:
				case 1853:
				case 1856:
				case 3334:
				case 8776:
					continue;
			}
			
			EntityDef def = EntityDef.forID(i);
			
			if (def == null || def.name == null) {
				continue;
			}
			
			if (def.actions != null && def.actions.length > 0 && def.actions[0] != null && def.actions[0].equals("Pick-up"))
				continue;
			
			if (def.name.replace("_", " ").equalsIgnoreCase(name.replace("_", " "))) {
				return i;
			}
			
		}
		return -1;
	}

//	public static int getNpcId(String name) {
//		for (int i = 0; i < NPCDefinitions.getDefinitions().length; i++) {
//			if (NPCDefinitions.getDefinitions()[i] != null) {
//				if (NPCDefinitions.getDefinitions()[i].getNpcName()
//						.replace("_", " ")
//						.equalsIgnoreCase(name.replace("_", " "))) {
//					return i;
//				}
//			}
//		}
//		return -1;
//	}
	
	public static int getNpcListHP(int npcId) {
		if (npcId <= -1) {
			return 0;
		}
		if (NPCDefinitions.getDefinitions()[npcId] == null) {
			return 0;
		}
		return NPCDefinitions.getDefinitions()[npcId].getNpcHealth();
		
	}
	
	public static String getNpcName(int npcId) {
		if (npcId <= -1) {
			return "null";
		}
		
		EntityDef def = EntityDef.forID(npcId);
		
		if (def == null) {
			return "null";
		}
		
		return def.getName();
	}
	
	public static NPC[] getNPCs() {
		return npcs;
	}
	
	public static int getProjectileSpeed(int i) {
		switch (npcs[i].npcType) {
			case 2881:
			case 2882:
				return 85;
			
			case 2745:
			case 12743:
				return 130;
			case 1158:
			case 1160:
				return 90;
			
			case 10815:
			case 10816:
			case 10817:
			case 10818:
			case 10819:
			case 10820:
			case 53:
			case 10219:
			case 10220:
			case 10221:
			case 10222:
			case 10223:
			case 10224:
			case 54:
			case 55:
			case 10604:
			case 10605:
			case 10606:
			case 10607:
			case 10608:
			case 10609:
			case 941:
			case 4677:
			case 4678:
			case 4679:
			case 4680:
			case 5362:
			case 5363:
			case 1590:
			case 10776:
			case 10777:
			case 10778:
			case 10779:
			case 10780:
			case 10781:
			case 1591:
			case 1592:
			case 3590:
				return 85;
			case 50:
			case 51:
			case 10770:
			case 10771:
			case 10772:
			case 10773:
			case 10774:
			case 10775:
			case 2011:
				return 90;
			
			case 2025:
				return 85;
			
			case 2028:
				return 80;
			
			default:
				return 85;
		}
	}
	
	/**
	 * Npc respawn time
	 **/
	public static int getRespawnTime(int i) {
		if (!npcs[i].alwaysRespawn) {
			return 0;
		}
		switch (npcs[i].npcType) {
			// Wildy bosses
			case 109:
				return 50;
			case 13447:
				return 120;
			case 6261:
			case 6263:
			case 6265:
			case 8349:
			case 8350:
			case 8351:
			case 4174:
			case 4173:
			case 4172:
			case 8596: // avatar of destruction
			case 8597: // avatar of creation
			case 9176: // skeletal horror
				return 60;
			case 1158:
				return 50;
			case 6203:
			case 6204:
			case 6206:
			case 2881:
			case 2882:
			case 2883:
			case 6222:
			case 6223:
			case 6225:
			case 6227:
			case 6247:
			case 6248:
			case 6250:
			case 6260:
			case 6208:
			case 4540:
				return 60;
			case 8133: // Corporeal beast
			case 3101: // Melee
			case 3102: // Range
			case 3103: // Mage
				return 50;
			
			case 3247: // Godwars
			case 6270:
			case 6219:
			case 6255:
			case 6229:
			case 6277:
			case 6233:
			case 6232:
			case 6218:
			case 6269:
			case 3248:
			case 6212:
			case 6220:
			case 6276:
			case 6256:
			case 6239: // Aviansie
			case 6230:
			case 6221:
			case 6231:
			case 6257:
			case 6278:
			case 6272:
			case 6274:
			case 6254:
			case 6258:
			case 3340: // giant mole
				return 50;
			case 50:// drags
			case 4291: // Cyclops
			case 4292: // Ice cyclops
				return 50;
			case 6142:
			case 6143:
			case 6144:
			case 6145:
				return 500;
			case 13645:
			case 13469:
			case 13471:
			case 13481:
			case 13475:
			case 13476:
			case 13478:
			case 13480:
			case 13479:
			case 13474:
			case 13473:
			case 13472:
			case 13477:
				return 35;
			case 8528:
				return 300;
			case 5996:
				return 100000;
			case 5902:
				return 120000;
			case 6055: // baby impling
			case 6056: // Young Impling
				return 35;
			case 6057: // Gourmet Impling
			case 6058: // Earth Impling
			case 6059: // Essence Impling
				return 40;
			case 6060: // Electic Impling
			case 6061: // Nature Impling
				return 43;
			case 6062: // Magpie Impling
				return 46;
			case 6063: // Ninja Impling
				return 95;
			case 6064: // Dragon Impling
				return 105;
			case 7903: // Kingly Impling
				return 120;
			case 5085: // butterflies
			case 5084:
			case 5083:
			case 5082:
				return 200;
			case 1639:
			case 1636:
				return 40;
			default:
				return 25;
		}
	}
	
	public static boolean getsPulled(int i) {
		switch (npcs[i].npcType) {
			case 6260:
				if (npcs[i].firstAttacker > 0) {
					return false;
				}
				break;
		}
		return true;
	}
	
	public static int getStackedDropAmount(int itemId, int npcId) {
		switch (itemId) {
			case 995:
				switch (npcId) {
					case 1:
						return 50 + Misc.random(50);
					case 9:
						return 133 + Misc.random(100);
					case 1624:
						return 1000 + Misc.random(300);
					case 1618:
						return 1000 + Misc.random(300);
					case 1643:
						return 1000 + Misc.random(300);
					case 1610:
						return 1000 + Misc.random(1000);
					case 1613:
						return 1500 + Misc.random(1250);
					case 1615:
						return 3000;
					case 18:
						return 500;
					case 101:
						return 60;
					case 913:
					case 912:
					case 914:
						return 750 + Misc.random(500);
					case 1612:
						return 250 + Misc.random(500);
					case 1648:
						return 250 + Misc.random(250);
					case 90:
						return 200;
					case 82:
						return 1000 + Misc.random(455);
					case 52:
						return 400 + Misc.random(200);
					case 49:
						return 1500 + Misc.random(2000);
					case 1341:
						return 1500 + Misc.random(500);
					case 26:
						return 500 + Misc.random(100);
					case 20:
						return 750 + Misc.random(100);
					case 21:
						return 890 + Misc.random(125);
					case 117:
						return 500 + Misc.random(250);
					case 2607:
						return 500 + Misc.random(350);
				}
				break;
			case 11212:
				return 10 + Misc.random(4);
			case 565:
			case 561:
				return 10;
			case 560:
			case 563:
			case 562:
				return 15;
			case 555:
			case 554:
			case 556:
			case 557:
				return 20;
			case 892:
				return 40;
			case 886:
				return 100;
			case 6522:
				return 6 + Misc.random(5);
			
		}
		
		return 1;
	}
	
	private static boolean goodDistance(Client c, NPC npc, int distance) {
		int size = 1;// npc.getSize();
		for (int sizeX = 0; sizeX < size; sizeX++) {
			for (int sizeY = 0; sizeY < size; sizeY++) {
				if (size > 1) {
					if (sizeX > 0 && sizeX < size) {
						sizeX = size;
					}
					if (sizeY > 0 && sizeY < size) {
						sizeY = size;
					}
				}
				if (getDistance(npc.absX + sizeX, c.getX(), npc.absY + sizeY,
						c.getY()) <= distance) {
					return true;
				} else {
					continue;
				}
			}
		}
		return false;
	}
	
	public static boolean goodDistance(int objectX, int objectY, int playerX,
	                                   int playerY, int distance) {
		if (getDistance(playerX, objectX, playerY, objectY) <= distance) {
			return true;
		}
		return false;
	}
	
	public static boolean goodDistance(Player p, NPC npc, int distance) {
		int centerX = npc.getXtoPlayerLoc(p.getX());
		int centerY = npc.getYtoPlayerLoc(p.getY());
		
		return p.withinDistance(centerX, centerY, npc.getZ(), distance + (npc.getSize() == 5 ? 1 : 0));
	}
	
	public static boolean goodDistance(Mob p, NPC npc, int distance) {
		int centerX = npc.getXtoPlayerLoc(p.absX);
		int centerY = npc.getYtoPlayerLoc(p.absX);
		
		return p.withinDistance(centerX, centerY, npc.getZ(), distance + (npc.getSize() == 5 ? 1 : 0));
	}
	
	private static void handleBossDeath(final Client c, NPC npc) {
		BossKCEnum boss = BossKCEnum.forId(npc.npcType);
		if (boss == null) {
			return;
		}
		
		if (npc.inWild()) {
			c.increaseProgress(TaskType.NPC_KILL, TaskConstants.KILL_WILDY_BOSS);
		}
		
		if (boss == BossKCEnum.VORKATH && c.getBossKills(boss.ordinal()) == 49) {
			c.getItems().addOrDrop(new GameItem(30329, 1));
			c.sendMessage("For achieving 50 kills, you get Vorkath's Head.");
		}
		
		if (!boss.rewardAll()) {
			//We do logic diff for nex.
			if (boss != BossKCEnum.NEX) {
				c.increaseProgress(TaskType.NPC_BOSS_KILL, npc.npcType);
			}
			
			addBossKill(c, boss);
		} else {
			boolean checkAvg = npc.dungeonManager != null;
			for (int thisId : npc.getInflictors().keySet()) {
				Client inflictor = (Client) PlayerHandler.getPlayerByMID(thisId);
				if (inflictor == null || !inflictor.isActive) {
					continue;
				}
				
				if (checkAvg) {
					int avgDifference = Party.calcAvg(inflictor) - npc.dungeonManager.teamAvg;
					if (avgDifference > 600) {
						inflictor.sendMessage("Inbalanced party, you receive no boss killcount!");
						continue;
					}
				}
				
				addBossKill(inflictor, boss);
			}
		}
	}
	
	private static void addBossKill(Client c, BossKCEnum boss) {
		if (c == null || !c.isActive) {
			return;
		}
		
		c.setBossKills(boss.ordinal(), 1, true);
		final String name = boss.toString();
		c.sendMessage("Your " + name + " killcount is: <col=ff0000>"
				+ c.getBossKills()[boss.ordinal()]);
	}
	
	public static void handleClipping(int i) {
		final NPC npc = npcs[i];
		// int count = 0;
		int addMask = 0;
		if (!npc.walksOnTopOfNpcs)
			addMask = addMask | RegionClip.NPC_TILE;
		
		int size = npcs[i].getSize();
		if (size > 1) {
			for (int sizeOffset = 0; sizeOffset < size; sizeOffset++) {
//				for (int sizeY = 0; sizeY < size; sizeY++) {

//					if (sizeX == 0 || sizeX == size-1 || sizeX == 0 || sizeX == size-1) {
				
				if (npc.moveX == 1 && npc.moveY == 1) {
					if ((RegionClip.getClipping(npc.absX + size,
							npc.absY + size, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x12801e0 | addMask)) != 0) {
						npc.moveX = 0;
						npc.moveY = 0;
						if ((RegionClip.getClipping(npc.absX + sizeOffset,
								npc.absY + size, npc.heightLevel, npc.getDynamicRegionClip())
								& (0x1280120 | addMask)) == 0) {
							npc.moveY = 1;
						} else {
							npc.moveX = 1;
						}
					}
				} else if (npc.moveX == -1 && npc.moveY == -1) {
					if ((RegionClip.getClipping(npc.absX - 1,
							npc.absY - 1, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x128010e | addMask)) != 0) {
						npc.moveX = 0;
						npc.moveY = 0;
						if ((RegionClip.getClipping(npc.absX + sizeOffset,
								npc.absY - 1, npc.heightLevel, npc.getDynamicRegionClip())
								& (0x1280102 | addMask)) == 0) {
							npc.moveY = -1;
						} else {
							npc.moveX = -1;
						}
					}
				} else if (npc.moveX == 1 && npc.moveY == -1) {
					if ((RegionClip.getClipping(npc.absX + size,
							npc.absY - 1, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x1280183 | addMask)) != 0) {
						npc.moveX = 0;
						npc.moveY = 0;
						if ((RegionClip.getClipping(npc.absX + sizeOffset,
								npc.absY - 1, npc.heightLevel, npc.getDynamicRegionClip())
								& (0x1280102 | addMask)) == 0) {
							npc.moveY = -1;
						} else {
							npc.moveX = 1;
						}
					}
				} else if (npc.moveX == -1 && npc.moveY == 1) {
					if ((RegionClip.getClipping(npc.absX - 1,
							npc.absY + size, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x1280138 | addMask)) != 0) {
						npc.moveX = 0;
						npc.moveY = 0;
						if ((RegionClip.getClipping(npc.absX + sizeOffset,
								npc.absY + size, npc.heightLevel, npc.getDynamicRegionClip())
								& (0x1280120 | addMask)) == 0) {
							npc.moveY = 1;
						} else {
							npc.moveX = -1;
						}
					}
				} // Checking Diagonal movement.
				
				if (npc.moveY == -1) {
					if ((RegionClip.getClipping(npc.absX + sizeOffset,
							npc.absY - 1, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x1280102 | addMask)) != 0) {
						npc.moveY = 0;
					}
				} else if (npc.moveY == 1) { // since we move up, we just add y + size since size + y = +1 from npcs top Y location
					if ((RegionClip.getClipping(npc.absX + sizeOffset,
							npc.absY + size, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x1280120 | addMask)) != 0) {
						npc.moveY = 0;
					}
				} // Checking Y movement.
				if (npc.moveX == 1) {
					if ((RegionClip.getClipping(npc.absX + size,
							npc.absY + sizeOffset, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x1280180 | addMask)) != 0) {
						npc.moveX = 0;
					}
				} else if (npc.moveX == -1) {
					if ((RegionClip.getClipping(npc.absX - 1,
							npc.absY + sizeOffset, npc.heightLevel, npc.getDynamicRegionClip())
							& (0x1280108 | addMask)) != 0) {
						npc.moveX = 0;
					}
				} // Checking X movement.
				/**
				 * NPC not walking through npcs
				 */
				
				/*
				 * for (NPC otherNPC :
				 * oSizeY) { npc.moveX = 0; npc.moveY = 0; } } } } }
				 */

//				}
			
			}
//			}
			
			// System.out.println("Count "+count);
			
		} else if (size == 1) {
			if (npc.moveX == 1 && npc.moveY == 1) {
				if ((RegionClip.getClipping(npc.absX + 1, npc.absY + 1,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x12801e0 | addMask)) != 0) {
					npc.moveX = 0;
					npc.moveY = 0;
					if ((RegionClip.getClipping(npc.absX, npc.absY + 1,
							npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280120 | addMask)) == 0) {
						npc.moveY = 1;
					} else {
						npc.moveX = 1;
					}
				}
			} else if (npc.moveX == -1 && npc.moveY == -1) {
				if ((RegionClip.getClipping(npc.absX - 1, npc.absY - 1,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x128010e | addMask)) != 0) {
					npc.moveX = 0;
					npc.moveY = 0;
					if ((RegionClip.getClipping(npc.absX, npc.absY - 1,
							npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280102 | addMask)) == 0) {
						npc.moveY = -1;
					} else {
						npc.moveX = -1;
					}
				}
			} else if (npc.moveX == 1 && npc.moveY == -1) {
				if ((RegionClip.getClipping(npc.absX + 1, npc.absY - 1,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280183 | addMask)) != 0) {
					npc.moveX = 0;
					npc.moveY = 0;
					if ((RegionClip.getClipping(npc.absX, npc.absY - 1,
							npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280102 | addMask)) == 0) {
						npc.moveY = -1;
					} else {
						npc.moveX = 1;
					}
				}
			} else if (npc.moveX == -1 && npc.moveY == 1) {
				if ((RegionClip.getClipping(npc.absX - 1, npc.absY + 1,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280138 | addMask)) != 0) {
					npc.moveX = 0;
					npc.moveY = 0;
					if ((RegionClip.getClipping(npc.absX, npc.absY + 1,
							npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280120 | addMask)) == 0) {
						npc.moveY = 1;
					} else {
						npc.moveX = -1;
					}
				}
			} // Checking Diagonal movement.
			
			if (npc.moveY == -1) {
				if ((RegionClip.getClipping(npc.absX, npc.absY - 1,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280102 | addMask)) != 0) {
					npc.moveY = 0;
				}
			} else if (npc.moveY == 1) {
				if ((RegionClip.getClipping(npc.absX, npc.absY + 1,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280120 | addMask)) != 0) {
					npc.moveY = 0;
				}
			} // Checking Y movement.
			if (npc.moveX == 1) {
				if ((RegionClip.getClipping(npc.absX + 1, npc.absY,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280180 | addMask)) != 0) {
					npc.moveX = 0;
				}
			} else if (npc.moveX == -1) {
				if ((RegionClip.getClipping(npc.absX - 1, npc.absY,
						npc.heightLevel, npc.getDynamicRegionClip()) & (0x1280108 | addMask)) != 0) {
					npc.moveX = 0;
				}
			} // Checking X movement.
			
			// check for npcs in that spot
//			for (NPC n : npcs[i].currentRegion.getNpcs()) { // create a new region that shows all the blocks npcs currently occupy
//				if (n != null && n.getX() == npc.getX()+npc.moveX && n.getY() == npc.getY()+npc.moveY) {
//					npc.moveX = 0;
//					npc.moveY = 0;
//					break;
//				}
//			}
		}
	}
	
	public static void handleGorillaAttackSwitches(NPC n) {
		n.dgMissedHits++;
		if (n.dgMissedHits >= 3) {
			n.dgMissedHits = 0;
			int lastHitType = n.currentAttackStyle;
			do {
				n.currentAttackStyle = Misc.random(2);
			} while (n.currentAttackStyle == lastHitType);
		}
	}
	
	public static void handleJadDeath(NPC n) {
		final Client c = (Client) PlayerHandler.players[n.spawnedBy];
		if (c == null) {
			return;
		}
		if (c.inKilnFightCave) {
			c.getAchievement().defeatJad();
			Pet.petRoll(c, Pet.TZREK_JAD);
			return;
		}
		c.tzhaarToKill++;
		if (c.getDonatedTotalAmount() > 49) {
			if (Misc.random(3) == 1) {
				if (c.getItems().freeSlots() > 1) {
					c.getItems().addItem(6570, 1);
					c.sendMessage("You have received extra fire cape.");
				} else {
					c.getItems().addItemToBank(6570, 1);
					c.sendMessage(
							"You have received extra fire cape in your bank.");
				}
			}
		}
		if (c.getItems().freeSlots() > 0) {
			c.getItems().addItem(6570, 1);
			if (Misc.random(33) == 1) {
				c.getItems().addOrBankItem(6571, 1);
				c.sendMessage("You received a uncut onyx.");
			}
			c.sendMessage(
					"Congratulations on completing the fight caves minigame!");
		} else {
			c.getItems().addItemToBank(6570, 1);
			c.sendMessage(
					"Congratulations on completing the fight caves minigame!");
			c.sendMessage("Fire cape has been added into your bank.");
		}
		c.getPA().resetTzhaar();
		c.getPA().movePlayer(2438, 5168, 0);
		c.getAchievement().defeatJad();
		c.waveId = 300;
		Pet.petRoll(c, Pet.TZREK_JAD);
	}
	
	private static void onNpcDeath(NPC n) {
		switch (n.npcType) {

			case 2745:
				handleJadDeath(n);
				break;
			
			case 8596:
				SoulWars.redAvatar.deaths++;
				SoulWars.redAvatar.level = 125;
				break;
			case 8597:
				SoulWars.blueAvatar.deaths++;
				SoulWars.blueAvatar.level = 125;
				break;
		}
	}
	
	public static void handleSpecialEffects(Player c, NPC npc, int damage) {
		if (c != null) {
			if (damage > 0) {
				switch (npc.npcType) {
					case 2892:
					case 2894:
						if (c.getPlayerLevel()[5] > 0) {
							c.getPlayerLevel()[5]--;
							c.refreshSkill(5);
							c.getDotManager().applyPoison(12, npc);
						}
						break;
				}
			}
		}
	}
	
	public static void init() {
		for (int i = 0; i < maxNPCs; i++) {
			npcs[i] = null;
		}
		Misc.findFreeNpcSlot();//fixes random bug?
	}
	
	public static boolean isAggressive(NPC npc) {
		if (npc.isSummoningSkillNpc || npc.isPetNpc() || !npc.isAggressive || !npc.isRetaliates()
				|| npc.getSkills().getLifepoints() < 1 || npc.getSkills().getStaticLevel(Skills.HITPOINTS) < 1
				|| npc.getEntityDef().getCombatLevel() < 1) {
			return false;
		}
		
		return true;
	}
	
	public static boolean isAggro(Player p, NPC n) {
		if (p.hidePlayer) {
			return false;
		}
		
		if (p.playerIsInHouse) {
			return p.getPOH().isGuest();
		}
		
		if (n.bossRoomNpc) {
			return true; // never to remove aggro from bosses
		}
		if (p.insideDungeoneering()) {
			if ((p.playerEquipment[PlayerConstants.playerHat] == 17279 || p.playerEquipment[PlayerConstants.playerHat] == 15828)) {
				if (n.canRemoveSsh()) {
					p.deactivateShh();
				} else if (n.isAffectedBySsh() && p.dungSshDisable == null) {
					return false;
				}
			}
			
			return true;
		}
		if (n.inWild()) {
			if (p.revenantDungeon() && p.getItems().playerHasEquipped(BraceletOfEthereumPlugin.CHARGED_ID, PlayerConstants.playerHands))
				return false;
			return true;
		}
		
		if (p.combatLevel > n.getEntityDef().getCombatLevel() * 2 && !(n.npcType == 76)) {
			// p.sendMessage("cmb "+ (n.getEntityDef().getCombatLevel() * 2));
			return false;
		}
		
		if (p.getNpcAggroTick() > n.getAggroStop()) {
			return false;
		}
		
		return n.isAggressive;
	}
	
	public static boolean setAggroShit(NPC n) {
		
		if (n.inWild()) {
			n.setAggroStop(Integer.MAX_VALUE);
			return true;
		}
		
		
		// 10 minutes = 1000 // 30 minutes = 3000
		n.setAggroStop(1000); // 10 minutes no more aggro. Max on npcs should be
		// 30 minutes
		
		
		switch (n.npcType) {
			case 8970:
			case 13447: // NEX
				n.setAggroStop(Integer.MAX_VALUE);
				return true;
			case 2045: // snakeling for zulrah
				return true;
			
			// GWD
			// case 6260:
			// case 6261:
			// case 6263:
			// case 6265:
			// case 6222:
			// case 6223:
			// case 6225:
			// case 6227:
			// case 6247:
			// case 6248:
			// case 6250:
			// case 6252:
			// case 1158:
			// case 1160:
			// case 1593:
			// case 50:
			// case 8133:
			// case 6206:
			// case 6208:
			// case 6204:
			// case 6203:
			// return true; // never to remove aggro from bosses
			
			// dagannoth cave
			
			//halloween zombies
			case 76:
			
			case 2889:
			case 2457:
			case 2452:
			case 1341:
			case 2454:
			case 2455:
			case 2456:
				// dagannoth cave
				
				// stronghold of security
			case 4411:
			case 4412:
			case 4413:
			case 4414:
			case 4410:
			case 4406:
			case 4404:
			case 4408:
			case 4407:
			case 4409:
			case 4394:
			case 4395:
			case 4391:
			case 4393:
			case 4390:
			case 4389:
			case 4392:
			case 4383:
			case 4387:
			case 4382:
			case 4386:
			case 4385:
			case 4384:
			case 4388:
			case 4381:
			case 8645:
			case 4402:
			case 4400:
			case 4403:
			case 4401:
			case 4397:
			case 4398:
			case 4399:
				n.setAggroStop(3000); // 30 minutes stronghold of security
			
			case 2892:
			case 2894:
			case 2881:
			case 2882:
			case 2883:
			case 2035:
				n.setAggroStop(1500); // 15 minutes
				break;
		}
		
		
		return true;
	}
	
	public static boolean isDefiler(int id) {
		if (id >= 3762 && id <= 3771) {
			return true;
		}
		return false;
	}
	
	public static boolean isShifter(int id) {
		if (id >= 3732 && id <= 3741) {
			return true;
		}
		return false;
	}
	
	public static boolean isSpinner(int id) {
		if (id >= 3747 && id <= 3751) {
			return true;
		}
		return false;
	}
	
	public static boolean isTorcher(int id) {
		if (id >= 3752 && id <= 3761) {
			return true;
		}
		return false;
	}
	
	public static void kill(NPC npc) {
		if (npc == null || !npc.isActive) {
			return;
		}
		npc.die();
	}

	public static void spawnPitScorpions() {
		int max = 30;
		int totalCount = 0;
		int startX = 9623;
		int startY = 10332;

		loop:
		for (int x = 0; x < 26; x++) {
			for (int y = 0; y < 19; y++) {

				x += 10;
				y += 2;

				x = x % 25;
				y = y % 18;

				totalCount++;

				newNPC(109, startX + x, startY + y, 0, 1);

				if (totalCount == max)
					break loop;
			}
		}
	}

	public static boolean loadAutoSpawn(String FileName) {
		String line = "";
		String token = "";
		String token2 = "";
		String token2_2 = "";
		String[] token3 = new String[10];
		boolean EndOfFile = false;
		// int ReadMode = 0;
		BufferedReader characterfile = null;
		try {
			characterfile = new BufferedReader(new FileReader("./" + FileName));
		} catch (FileNotFoundException fileex) {
			Misc.println(FileName + ": file not found.");
			return false;
		}
		try {
			line = characterfile.readLine();
		} catch (IOException ioexception) {
			Misc.println(FileName + ": error loading file.");
		}
		while (!EndOfFile && line != null) {
			line = line.trim();
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token2_2 = token2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.equals("spawn")) {
					newNPC(Integer.parseInt(token3[0]),
							Integer.parseInt(token3[1]),
							Integer.parseInt(token3[2]),
							Integer.parseInt(token3[3]),
							Integer.parseInt(token3[4]));
					
				}
			} else {
				if (line.equals("[ENDOFSPAWNLIST]")) {
					try {
						characterfile.close();
					} catch (IOException ioexception) {
					}
					// return true;
				}
			}
			try {
				line = characterfile.readLine();
			} catch (IOException ioexception1) {
				EndOfFile = true;
			}
		}
		try {
			characterfile.close();
		} catch (IOException ioexception) {
		}
		return false;
	}
	
	public static void loadNpcHandler() {
		// NPC loading crap
		NPCAnimations.initialize();
		NPCAttackLoader.initialize();
		NpcStats.load();
		loadNpcStats();
		loadAutoSpawn("./Data/" + Config.cfgDir() + "/spawn-config.cfg");
		spawnPitScorpions();
		NPCSpawnManager.load();
	}

	public static void loadNpcStats() {
		OSRSBoxNpcDataLoader.load();
	}

	public static boolean loadNPCList(String FileName) {
		String line = "";
		String token = "";
		String token2 = "";
		String token2_2 = "";
		String[] token3 = new String[10];
		boolean EndOfFile = false;
		// int ReadMode = 0;
		BufferedReader characterfile = null;
		try {
			characterfile = new BufferedReader(new FileReader("./" + FileName));
		} catch (FileNotFoundException fileex) {
			Misc.println(FileName + ": file not found.");
			return false;
		}
		try {
			line = characterfile.readLine();
		} catch (IOException ioexception) {
			Misc.println(FileName + ": error loading file.");
			try {
				characterfile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
		while (EndOfFile == false && line != null) {
			line = line.trim();
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token2_2 = token2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.equals("npc")) {
					newNPCList(Integer.parseInt(token3[0]), token3[1],
							Integer.parseInt(token3[2]),
							Integer.parseInt(token3[3]));
				}
			} else {
				if (line.equals("[ENDOFNPCLIST]")) {
					try {
						characterfile.close();
					} catch (IOException ioexception) {
						ioexception.printStackTrace();
					}
					// return true;
				}
			}
			try {
				line = characterfile.readLine();
			} catch (IOException ioexception1) {
				EndOfFile = true;
			}
		}
		try {
			characterfile.close();
		} catch (IOException ioexception) {
			ioexception.printStackTrace();
		}
		return false;
	}
	
	public static void loadSpell(Client c, NPC npc) {
		if (npc.isBarbAssaultNpc() && BarbarianAssault.handleNpcSpell(c, npc)) {
			return;
		}
		if (npc.npcType >= 10560 && npc.npcType <= 10603) {
			ForgottenMageSpells.forgottenMageNpc(npc, c);
			return;
		}
		if (npc.npcType >= 10791 && npc.npcType <= 10796 || npc.npcType >= 10610 && npc.npcType <= 10618 || npc.npcType >= 10212 && npc.npcType <= 10218
				|| npc.npcType >= 10831 && npc.npcType <= 10842 || npc.npcType >= 10460 && npc.npcType <= 10468 || npc.npcType >= 10670 && npc.npcType <= 10693
				|| npc.npcType >= 10375 && npc.npcType <= 10385 || npc.npcType == 10705 || npc.npcType == 10704 || npc.npcType == 10701 || npc.npcType == 10697) {
			DungeonRangeAndMageNpcs.DungeoneeringSpells(npc, c);
			return;
		}
		
		if (npc.loadedSpell(c))
			return;
		
		boolean switchAttackType = (Misc.random(7) == 1);
		switch (npc.npcType) {
			
			case 13460:
			/*if (Misc.random(10) >= 3) {
				npc.attackType = 0;
			} else if (Misc.random(10) >= 1) {
				npc.attackType = 2;
				npc.startAnimation(new Animation(15257));
				npc.projectileId = 2595;
			} else {*/
				if (Misc.random(5) == 1) {
					for (Player p : Server.getRegionManager()
							.getLocalPlayers(npc)) {
						if (p == null) {
							continue;
						}
						if (p.goodDistance(npc, 10)) {
							final Client c2 = (Client) p;
							
							CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
								
								int timer = 7;
								
								@Override
								public void execute(CycleEventContainer container) {
									
									if (c.disconnected || c == null) {
										container.stop();
										return;
									}
									
									if (timer == 4) {
										c2.setFreezeTimer(20);
										c2.startGraphic(Graphic.create(2550));
										c2.sendMessage("You have been frozen");
									}
									
									if (timer == 2) {
										c2.startGraphic(Graphic.create(2548));
									}
									
									if (timer == 1) {
										//c2.dealDamage(new Hit(30 + Misc.random(30), 0, -1));
									}
									
									timer--;
									if (timer == 0) {
										container.stop();
									}
								}
								
								@Override
								public void stop() {
								}
							}, 1);
						}
					}
					npc.attackType = 2;
					npc.startAnimation(new Animation(15257));
					npc.projectileId = 2595;
					npc.startGraphic(Graphic.create(-1));
				} else {
					npc.attackType = 0;
					npc.startGraphic(Graphic.create(2594, GraphicType.LOW));
					npc.projectileId = -1;
				}
				break;
			
			case 2499:
			case 2501:
			case 2503:
				if (Misc.random(2) == 1) {
					c.getSkills().drainLevel(Skills.ATTACK, Misc.randomNoPlus(4), 1);
					c.getSkills().drainLevel(Skills.DEFENSE, Misc.randomNoPlus(4), 1);
					c.getSkills().drainLevel(Skills.STRENGTH, Misc.randomNoPlus(4), 1);
					c.getSkills().drainLevel(Skills.RANGED, Misc.randomNoPlus(4), 1);
					c.getSkills().drainLevel(Skills.MAGIC, Misc.randomNoPlus(4), 1);
					npc.startAnimation(711);
					npc.startGraphic(Graphic.create(177, GraphicType.HIGH));
					npc.projectileId = 178;
					npc.attackType = 2;
					npc.maxHit = 5;
					npc.endGfx = 180;
					npc.projectiletStartHeight = 43;
					c.sendMessage("You feel incredibly weakened");
				} else {
					npc.attackType = 5;
				}
				break;
			
			case 8596: //avatar of destruction
			case 8597: //avatar of creation
				if (Misc.random(15) == 1) {
					npc.attackType = 0;
					npc.projectileId = -1;
					c.getPlayerLevel()[5] -= (c.getPlayerLevel()[5] * .15);
					c.sendMessage("Your prayer was drained!");
					c.refreshSkill(5);
					break;
				}
				break;
			
			case 7151:
				if (Misc.random(30) == 7) { // special attack
					BoulderFallingFromSkyAndJuliusShittingHisBallss.tossBoulder(
							c.getX(), c.getY(), c.getZ(),
							c.getSkills().getMaximumLifepoints() / 3);
				} else { // everything else
					if (npc.currentAttackStyle == -1) {
						// if not set yet.
						npc.currentAttackStyle = Misc.random(2);
					}
					
					switch (npc.currentAttackStyle) {
						case 0: // melee
							npc.attackType = CombatStatics.MELEE_HIT;
							npc.projectileId = -1;
							npc.endGfx = -1;
							break;
						case 1: // range
							npc.attackType = CombatStatics.RANGE_HIT;
							npc.projectileId = 1302 + Config.OSRS_GFX_OFFSET;
							npc.endGfx = 855;
							break;
						case 2: // magic
							npc.attackType = CombatStatics.MAGIC_HIT;
							// npc.gfx0(999);
							npc.projectileId = 1000;
							npc.endGfx = 1001;
							break;
					}
				}
				
				break;
			
			case 6090:
				int minion = Misc.random(8);
				if (minion == 5) {
					npc.forceChat("Baby WildyWyrm, I choose you!!");
					NPC babywyrm = spawnNpc2(6089,
							WildyWyrm.getWormLocationData().getSpawnPos().getX() + 2,
							WildyWyrm.getWormLocationData().getSpawnPos().getY(),
							0, 0);
					LoadSpell.explodeAndSmite(babywyrm, 30, 30, 20, 35);
				} else {
					npc.attackType = 2;
					npc.projectileId = 1617;
				}
				break;
			
			case 4540:
				if (npc.bandosAvatar == 0) {
					if (npc.getSkills().getLifepoints() < 400) {
						npc.forceChat("Kill them all!");
						NPC orkstatue1 = spawnNpc2(7084, 2905, 4768, 0, 0);
						NPC orkstatue2 = spawnNpc2(7084, 2903, 4770, 0, 0);
						NPC ogrestatue1 = spawnNpc2(7085, 2901, 4768, 0, 0);
						NPC ogrestatue2 = spawnNpc2(7085, 2904, 4766, 0, 0);
						LoadSpell.explodeAndSmite(orkstatue1, 30, 30, 20, 35);
						LoadSpell.explodeAndSmite(orkstatue2, 30, 30, 20, 35);
						LoadSpell.explodeAndSmite(ogrestatue1, 30, 40, 20, 60);
						LoadSpell.explodeAndSmite(ogrestatue2, 30, 40, 20, 60);
						npc.bandosAvatar++;
					}
				} else if (npc.bandosAvatar == 1) {
					if (npc.getSkills().getLifepoints() < 200) {
						npc.forceChat("Kill them all!");
						NPC ourgstatue1 = spawnNpc2(7086, 2905, 4768, 0, 0);
						NPC ourgstatue2 = spawnNpc2(7086, 2903, 4770, 0, 0);
						LoadSpell.explodeAndSmite(ourgstatue1, 30, 50, 45, 55);
						LoadSpell.explodeAndSmite(ourgstatue2, 30, 50, 45, 55);
						npc.bandosAvatar++;
					}
				}
				if (Misc.random(1) == 1) {
					npc.attackType = 1;
					npc.projectileId = 2706;
					npc.endGfx = -1;
				} else if (Misc.random(3) == 1) {
					npc.attackType = 2;
					npc.endGfx = 369;
					npc.projectileId = -1;
					if (c.applyFreeze(19)) {
						c.sendMessage("You have been Frozen!");
					}
				} else {
					npc.attackType = 0;
					npc.endGfx = -1;
					npc.projectileId = -1;
				}
				break;
			
			case 3340:
				if (Misc.random(6) == 1) {
					NPC mole = npc;
					// generates random coordinate for the mole to teleport to
					int[][] coords = NPC.giantMoleTele;
					int[] coord = Misc.random(coords);
					int x = coord[0];
					int y = coord[1];
					int npcX = npc.getX();
					int npcY = npc.getY();
					
					// sets their spawn x/y so that they won't walk home
					mole.makeX = x;
					mole.makeY = y;
					
					mole.setKillerId(0);
					npc.resetFace();
					
					Server.schedule(2, () -> {
						// start of the mole digging
						if (mole != null) {
							mole.isDigging = true;
							mole.startAnimation(3314);
							mole.startGraphic(Graphic.create(572));
							
							Server.getStillGraphicsManager().createGfx(npcX + 2,
									npcY + 2, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX + 1,
									npcY + 2, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX + 2,
									npcY + 1, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX,
									npcY, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX,
									npcY + 1, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX,
									npcY + 2, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX + 1,
									npcY, mole.getHeightLevel(), 571, 0, 0);
							Server.getStillGraphicsManager().createGfx(npcX + 2,
									npcY, mole.getHeightLevel(), 571, 0, 0);
						}
						
						Server.schedule(4, () -> {
							// teleporting
							if (mole != null) {
								mole.setLastLocation(Location.create(mole.getX(), mole.getY(), mole.getZ()));
								mole.absX = x;
								mole.absY = y;
								mole.heightLevel = 0;
								mole.didTeleport = true;
								mole.setLocation(x, y);
								
								npc.attackType = -1;
								npc.resetFace();
							}
							
							// delay the digging up anim by 1 tick
							Server.schedule(() -> {
								if (mole != null) {
									mole.startGraphic(Graphic.create(573));
									mole.startAnimation(3315);
									mole.isDigging = false;
								}
							});
						});
					});
				} else {
					npc.attackType = 0;
				}
				break;
			case 8528:
				if (npc.nomadTele == 1) {
					npc.attackType = 2;
					npc.endGfx = 2282;
					npc.projectileId = 2283;
					npc.nomadTele = 0;
				} else if (npc.nomadTele == 2) {
					npc.attackType = 0;
					npc.endGfx = -1;
					npc.projectileId = -1;
					npc.nomadTele = 0;
				} else if (npc.nomad == 0 || npc.nomad == 3) {
					if (npc.getSkills().getLifepoints() < 450) {
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.startAnimation(12699);
						if (Misc.random(1) == 1) {
							npc.forceChat(
									"You cannot hide from my explosion!");
							NPC barrel1 = spawnNpc2(9443, 3367, 9813, 0, 0);
							LoadSpell.explodeAndSmite(barrel1, 16, 200, 50, 13);
							npc.nomad++;
						} else if (Misc.random(1) == 0) {
							npc.forceChat(
									"You'll never beat me this time!");
							NPC barrel2 = spawnNpc2(9443, 3384, 9813, 0, 0);
							LoadSpell.explodeAndSmite(barrel2, 16, 200, 50, 13);
							npc.nomad++;
						}
					} else if (Misc.random(1) == 1) {
						npc.attackType = 1;
						npc.projectileId = 2268;
						npc.endGfx = -1;
					} else if (Misc.random(3) == 1) {
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
					} else if (c.absX > 3370 && c.absX < 3380 && c.absY > 9808
							&& c.absY < 9815 && Misc.random(4) == 1) {
						npc.teleportToPlayer(c, -1);
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.nomadTele = 2;
					} else {
						npc.teleport(3369 + Misc.random(9), 9809 + Misc.random(6), 2, -1);
						npc.nomadTele++;
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
					}
				} else if (npc.nomad == 1 || npc.nomad == 4) {
					if (npc.getSkills().getLifepoints() < 200) {
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.startAnimation(12699);
						if (Misc.random(2) == 1) {
							npc.forceChat(
									"You cannot hide from my explosion!");
							NPC barrel = spawnNpc2(9443, 3367, 9813, 0, 0);
							LoadSpell.explodeAndSmite(barrel, 16, 50, 50, 13);
							npc.nomad++;
						} else if (Misc.random(2) == 2) {
							npc.forceChat(
									"You'll never beat me this time!");
							NPC barrel = spawnNpc2(9443, 3384, 9813, 0, 0);
							LoadSpell.explodeAndSmite(barrel, 16, 50, 50, 13);
							npc.nomad++;
						} else {
							npc.forceChat("You don't stand a chance.");
							NPC barrel = spawnNpc2(9443, 3375, 9806, 0, 0);
							LoadSpell.explodeAndSmite(barrel, 10, 50, 50, 13);
							npc.nomad++;
						}
					} else if (Misc.random(1) == 1) {
						npc.attackType = 1;
						npc.projectileId = 2268;
						npc.endGfx = -1;
					} else if (Misc.random(2) == 1) {
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
					} else if (c.absX > 3370 && c.absX < 3380 && c.absY > 9808
							&& c.absY < 9815 && Misc.random(4) == 1) {
						npc.teleportToPlayer(c, -1);
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.nomadTele = 2;
					} else {
						npc.teleport(3369 + Misc.random(9), 9809 + Misc.random(6), 2, -1);
						npc.nomadTele++;
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
					}
				} else if (npc.nomad == 2 || npc.nomad == 5) {
					if (npc.getSkills().getLifepoints() < 100) {
						npc.getSkills().heal(400);
						npc.forceChat(
								"You are tougher than I thought. Time to even things up.");
						npc.startAnimation(12700);
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.nomad++;
					} else if (Misc.random(1) == 1) {
						npc.attackType = 1;
						npc.projectileId = 2268;
						npc.endGfx = -1;
					} else if (Misc.random(2) == 1) {
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
					} else if (c.absX > 3370 && c.absX < 3380 && c.absY > 9808
							&& c.absY < 9815 && Misc.random(4) == 1) {
						npc.teleportToPlayer(c, -1);
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.nomadTele = 2;
					} else {
						npc.teleport(3369 + Misc.random(9), 9809 + Misc.random(6), 2, -1);
						npc.nomadTele++;
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
					}
				} else if (npc.nomad == 6) {
					if (npc.getSkills().getLifepoints() < 450) {
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.forceChat("Let's make things interesting.");
						npc.startAnimation(12701);
						npc.startGraphic(Graphic.create(2280));
						NPC clone = spawnNpc2(8529, 3374, 9814, 0, 0);
						LoadSpell.explodeAndSmite(clone, 0, 0, 0, 300);
						npc.nomad++;
					} else if (Misc.random(1) == 1) {
						npc.attackType = 1;
						npc.projectileId = 2268;
						npc.endGfx = -1;
					} else if (Misc.random(2) == 1) {
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
					} else if (c.absX > 3370 && c.absX < 3380 && c.absY > 9808
							&& c.absY < 9815 && Misc.random(4) == 1) {
						npc.teleportToPlayer(c, -1);
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.nomadTele = 2;
					} else {
						npc.teleport(3369 + Misc.random(9), 9809 + Misc.random(6), 2, -1);
						npc.nomadTele++;
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
					}
				} else if (npc.nomad == 7) {
					if (npc.getSkills().getLifepoints() < 150) {
						npc.forceChat("Enough! This... ends... NOW!");
						npc.teleport(3380, 9817, 2, -1);
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
						npc.nomadTele++;
						npc.nomad++;
					} else if (Misc.random(1) == 1) {
						npc.attackType = 1;
						npc.projectileId = 2268;
						npc.endGfx = -1;
					} else if (Misc.random(2) == 1) {
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
					} else if (c.absX > 3370 && c.absX < 3380 && c.absY > 9808
							&& c.absY < 9815 && Misc.random(4) == 1) {
						npc.teleportToPlayer(c, -1);
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
						npc.nomadTele = 2;
					} else {
						npc.teleport(3369 + Misc.random(9), 9809 + Misc.random(6), 2, -1);
						npc.nomadTele++;
						npc.attackType = -1;
						npc.endGfx = -1;
						npc.projectileId = -1;
					}
				} else if (npc.nomad == 8) {
					if (npc.getSkills().getLifepoints() < 150) {
						npc.teleport(3369, 9812, 2, -1);
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
						npc.nomadTele++;
						npc.nomad++;
					}
				} else if (npc.nomad == 9) {
					if (npc.getSkills().getLifepoints() < 150) {
						npc.teleport(3380, 9812, 2, -1);
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
						npc.nomadTele++;
						npc.nomad++;
					}
				} else if (npc.nomad == 10) {
					if (npc.getSkills().getLifepoints() < 150) {
						npc.teleport(3372, 9816, 2, -1);
						npc.attackType = 2;
						npc.endGfx = 2282;
						npc.projectileId = 2283;
						npc.nomadTele++;
						npc.nomad++;
					}
				} else if (Misc.random(1) == 1) {
					npc.attackType = 1;
					npc.projectileId = 2268;
					npc.endGfx = -1;
				} else if (Misc.random(2) == 1) {
					npc.attackType = 2;
					npc.endGfx = 2282;
					npc.projectileId = 2283;
				} else if (c.absX > 3370 && c.absX < 3380 && c.absY > 9808
						&& c.absY < 9815 && Misc.random(4) == 1) {
					npc.teleportToPlayer(c, -1);
					npc.attackType = -1;
					npc.endGfx = -1;
					npc.projectileId = -1;
					npc.nomadTele = 2;
				} else {
					npc.teleport(3369 + Misc.random(9), 9809 + Misc.random(6), 2, -1);
					npc.nomadTele++;
					npc.attackType = -1;
					npc.endGfx = -1;
					npc.projectileId = -1;
				}
				break;
			
			case 8529:
				if (Misc.random(1) == 1) {
					npc.attackType = 1;
					npc.projectileId = 2268;
					npc.endGfx = -1;
				} else {
					npc.attackType = 2;
					npc.endGfx = 2282;
					npc.projectileId = 2283;
				}
				break;
			
			case 6089:
				if (Misc.random(2) == 1) {
					npc.attackType = 0;
				} else {
					npc.attackType = 2;
					npc.projectileId = 1617;
				}
				break;
			case 9176:
				int random4 = Misc.random(8);
				if (random4 == 1) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
					npc.specialAttackType = 1;
					
					for (Player p : Server.getRegionManager()
							.getLocalPlayers(npc)) {
						if (p == null) {
							continue;
						}
						if (p.goodDistance(npc, 10)) {
							final Client c2 = (Client) p;
							c2.getCombat().resetPrayers();
							c2.sendMessage(
									"The skeleton horror slams your prayer off!");
						}
					}
				}
				break;
			
			case 8349: // tormented demons
			case 8350: // tormented demons
			case 8351: // tormented demons
				int ran = 0;
				if (goodDistance(c, npc, 2)) {
					if (switchAttackType) {
						ran = Misc.random(2);
					}
				} else {
					ran = Misc.random(1);
				}
				if (ran == 0) {
					npc.attackType = 2;
					npc.startGraphic(Graphic.create(1885, GraphicType.HIGH));
					npc.projectileId = 1884;
				} else if (ran == 1) {
					npc.attackType = 1;
					npc.projectileId = 1889;
				} else if (ran == 2) {
					npc.attackType = 0;
					npc.startGraphic(Graphic.create(1886, GraphicType.HIGH));
					npc.projectileId = -1;
				} else if (Misc.random(5) == 1) {
					npc.attackType = 0;
					npc.startGraphic(Graphic.create(1885, GraphicType.HIGH));
					npc.startAnimation(10917);
					npc.forceChat("Aaaaaaaaargh!! Now you will die!");
					NpcHitPlayer.dealMagicDamage(npc, c, 10, 1, null);
					npc.projectileId = -1;
				}
				break;
			
			case 13469:
			case 13481:
			case 13471:
			case 13465:
			case 13475:
			case 13476:
			case 13477:
			case 13478:
			case 13480:
			case 13479:
			case 13472:
			case 13474:
			case 13473:
				int random2 = (switchAttackType
						? Misc.random(2)
						: npc.attackType);
				if (random2 == 0) {
					npc.projectileId = 1276;
					npc.attackType = 2;
				} else if (random2 == 1) {
					npc.projectileId = 1278;
					npc.attackType = 1;
				} else if (random2 == 2) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
			case 1382:
				int random1 = (switchAttackType
						? Misc.random(2)
						: npc.attackType);
				if (random1 == 0) {
					npc.projectileId = 280;
					npc.attackType = 2;
				} else if (random1 == 1) {
					npc.projectileId = 962;
					npc.attackType = 1;
				} else if (random1 == 2) {
					npc.projectileId = -1; // melee
					npc.endGfx = 899;
					npc.attackType = 0;
				}
				break;
			case 14301:
				int random3 = (switchAttackType
						? Misc.random(2)
						: npc.attackType);
				if (random3 == 0) {
					npc.projectileId = 280;
					npc.attackType = 2;
				} else if (random3 == 1) {
					npc.projectileId = 962;
					npc.attackType = 1;
				} else if (random3 == 2) {
					npc.projectileId = -1; // melee
					npc.endGfx = 899;
					npc.attackType = 0;
				}
				break;
			case 2892:
				npc.projectileId = 94;
				npc.attackType = 2;
				npc.endGfx = 95;
				break;
			case 2894:
				npc.projectileId = 298;
				npc.attackType = 1;
				break;
			case 51:
			case 10770:
			case 10771:
			case 10772:
			case 10773:
			case 10774:
			case 10775:
				int r5 = 0;
				if (goodDistance(c, npc, 2)) {
					r5 = (switchAttackType
							? Misc.random(5)
							: npc.attackType);
				} else {
					r5 = Misc.random(3);
				}
				if (r5 == 0) {
					npc.projectileId = 393; // red
					npc.attackType = 3;
				} else if (r5 == 1) {
					npc.projectileId = 394; // green
					npc.attackType = 3;
				} else if (r5 == 2) {
					npc.projectileId = 395; // white
					npc.attackType = 3;
				} else if (r5 == 3) {
					npc.projectileId = 396; // blue
					npc.attackType = 3;
				} else if (r5 == 4) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				} else if (r5 == 5) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
			case 50:
				int random = (switchAttackType
						? Misc.random(4)
						: npc.attackType);
				if (random == 0) {
					npc.projectileId = 393; // red
					npc.endGfx = 430;
					npc.attackType = 3;
				} else if (random == 1) {
					npc.projectileId = 394; // green
					npc.endGfx = 429;
					npc.attackType = 3;
				} else if (random == 2) {
					npc.projectileId = 395; // white
					npc.endGfx = 431;
					npc.attackType = 3;
				} else if (random == 3) {
					npc.projectileId = 396; // blue
					npc.endGfx = 428;
					npc.attackType = 3;
				} else if (random == 4) {
					npc.projectileId = -1; // melee
					// npc.endGfx = -1;
					npc.attackType = 0;
				}
				break;
			
			case 10815:
			case 10816:
			case 10817:
			case 10818:
			case 10819:
			case 10820:
			case 53:
			case 10219:
			case 10220:
			case 10221:
			case 10222:
			case 10223:
			case 10224:
			case 54:
			case 55:
			case 10604:
			case 10605:
			case 10606:
			case 10607:
			case 10608:
			case 10609:
			case 941:
			case 4677:
			case 4678:
			case 4679:
			case 4680:
			case 5362:
			case 1590:
			case 10776:
			case 10777:
			case 10778:
			case 10779:
			case 10780:
			case 10781:
			case 1591:
			case 1592:
			case 2011:
			case 3590:
				int r8 = 0;
				if (goodDistance(c, npc, 2)) {
					r8 = (switchAttackType
							? Misc.random(2)
							: npc.attackType);
				} else {
					r8 = Misc.random(1);
				}
				if (r8 == 0) {
					npc.projectileId = 393; // red
					npc.attackType = 3;
				} else if (r8 == 1) {
					npc.projectileId = 393; // red
					npc.attackType = 3;
				} else if (r8 == 2) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
			case 5363:
				int r7 = 0;
				if (goodDistance(c, npc, 2)) {
					r7 = (switchAttackType
							? Misc.random(2)
							: npc.attackType);
				} else {
					r7 = Misc.random(1);
				}
				if (r7 == 0) {
					npc.projectileId = 396; // blue
					npc.attackType = 3;
				} else if (r7 == 1) {
					npc.projectileId = 396; // blue
					npc.attackType = 2;
				} else if (r7 == 2) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
			case 3068:
				if (Misc.random(1) == 1) {
					npc.startGraphic(Graphic.create(501));
					npc.endGfx = 502;
					npc.attackType = 3;
				} else if (Misc.random(6) == 3) {
					npc.projectileId = 500;
					npc.attackType = 1;
				} else {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
			// zamorak npcs
			case 6206:
				npc.attackType = 1;
				npc.projectileId = 1209;
				break;
			case 6208:
				npc.attackType = 2;
				npc.projectileId = 1213;
				break;
			case 6256:
				npc.attackType = 1;
				npc.projectileId = 16;
				break;
			case 6220:
				npc.attackType = 1;
				npc.projectileId = 17;
				break;
			case 6203:
				if (Misc.random(20) == 7) {
					npc.attackType = 2;
					npc.projectileId = 1211;
				} else {
					npc.attackType = 0;
					npc.projectileId = -1;
				}
				break;
			// arma npcs
			case 6239:
				npc.attackType = 1;
				npc.projectileId = 1191;
				break;
			case 6232:
				npc.attackType = 1;
				npc.projectileId = 1191;
				break;
			case 6276:
				npc.attackType = 1;
				npc.projectileId = 1195;
				break;
			case 6227:
				npc.attackType = 0;
				break;
			case 6225:
			case 6233:
			case 6230:
				npc.attackType = 1;
				npc.projectileId = 1190;
				break;
			case 6229:
				npc.attackType = 1;
				npc.projectileId = 1191;
				break;
			case 6223:
				npc.attackType = 2;
				npc.projectileId = 1199;
				break;
			case 6257:// saradomin strike
				npc.attackType = 2;
				npc.endGfx = 76;
				break;
			case 6221:// zamorak strike
				npc.attackType = 2;
				npc.endGfx = 78;
				break;
			case 6231:// arma
				npc.attackType = 2;
				npc.projectileId = 1199;
				break;
			case 6222:
				if (Misc.random(20) == 7) {
					npc.attackType = 2;
					npc.projectileId = 1198;
				} else {
					npc.attackType = 1;
					npc.projectileId = 1197;
				}
				break;
			// sara npcs
			case 6247: // sara
				if (Misc.random(20) == 7) {
					npc.attackType = 2;
					npc.endGfx = 2115;// 1224;
					npc.projectileId = -1;
				} else {
					npc.attackType = 0;
				}
				break;
			case 6248: // star
				npc.attackType = 0;
				break;
			case 6250: // growler
				npc.attackType = 2;
				npc.projectileId = 1185;
				npc.endGfx = 1186;
				break;
			case 6252: // bree
				npc.attackType = 1;
				npc.projectileId = 9;
				break;
			// bandos npcs
			case 6260:
				if (Misc.random(12) == 7) {
					npc.attackType = 1;
					npc.endGfx = 1211;
					npc.projectileId = 288;
				} else {
					npc.attackType = 0;
				}
				break;
			
			// callisto wildy boss
			case 4174:
				if (Misc.random(5) == 2) {
					npc.attackType = 2;
					npc.projectileId = 1453;
				} else if (Misc.random(14) == 1) {
					c.stunEntity(8, Graphic.create(254, GraphicType.HIGH));
					c.getCombat().resetPlayerAttack();
					npc.attackType = 1;
					npc.projectileId = 1278;
				} else {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
			
			case 6261:// strongstack
				npc.attackType = 0;
				break;
			case 6263:// steelwill
				npc.attackType = 2;
				npc.projectileId = 1203;
				break;
			case 6265:// grimspike
				npc.attackType = 1;
				npc.projectileId = 1206;
				break;
			case 8777:// chaos dwarf hand cannoneer
				npc.attackType = 1;
				npc.projectileId = 2143;
				npc.startGraphic(Graphic.create(2141));
				break;
			
			case 2881:// supreme
				npc.attackType = 1;
				npc.projectileId = 475;
				break;
			
			case 2882:// prime
				npc.attackType = 2;
				npc.projectileId = 2707;
				npc.endGfx = 2712;
				break;
			
			case 2028:
				npc.attackType = 1;
				npc.projectileId = 27;
				break;
			
			case 10080:
				if (Misc.random(8) == 1) {
					npc.attackType = 1;
					npc.projectileId = 2139;
					npc.endGfx = 2149;
				} else if (Misc.random(5) == 2) {
					if (!c.getDotManager().isPoisonActive()) {
						c.getDotManager().applyPoison(2, npc);
						c.sendMessage("You have been poisend by Bulwark Beast.");
					}

					npc.attackType = 2;
					npc.endGfx = 792;
					npc.projectileId = 794;
				} else {
					npc.projectileId = 0;
					npc.attackType = 0;
				}
				break;
			
			case 9771:
				if (Misc.random(4) == 1) {
					npc.attackType = 1;
					npc.projectileId = 0;
					npc.endGfx = 0;
				} else {
					npc.projectileId = 0;
					npc.attackType = 0;
					if (Misc.random(1) == 1) {
						c.getCombat().resetPrayers();
						c.sendMessage("BOOM!, slams off your prayer!!");
					}
				}
				break;
			
			case 12805:
				if (Misc.random(6) == 1) {
					npc.projectileId = 0;
					npc.attackType = 2;
					npc.getSkills().heal(20);
					c.getCombat().resetPrayers();
					c.sendMessage(
							"Kal'Ger the Warmonger drains your health and shuts down your prayer.");
				} else {
					npc.projectileId = 0;
					npc.attackType = 0;
				}
				break;
			
			case 9463:
				random = (switchAttackType
						? Misc.random(2)
						: npc.attackType);
				if (random == 0 || random == 1) {
					npc.attackType = 0;
				} else {
					npc.attackType = 2;
					if (c.applyFreeze(20))
						c.sendMessage("The Strykewyrm Used His Ice Bite And Froze You!");
				}
				break;
			case 9467:
				random = (switchAttackType
						? Misc.random(2)
						: npc.attackType);
				if (random == 0 || random == 1) {
					npc.attackType = 0;
				} else {
					if (!c.getDotManager().isPoisonActive()) {
						c.getDotManager().applyPoison(12, npc);
						npc.attackType = 2;
						c.sendMessage("The Strykewyrm Used His Poison Bite, And Poisend You!");
					}
				}
			case 9465:
				random = (switchAttackType
						? Misc.random(2)
						: npc.attackType);
				if (random == 0 || random == 1) {
					npc.attackType = 0;
				} else {
					c.getPlayerLevel()[5] -= (c.getPlayerLevel()[5] * .22);
					npc.attackType = 2;
					c.sendMessage("The Strykewyrm Drained Your Prayer Points!");
					c.refreshSkill(5);
				}
				break;
			case 2745:
				int r3 = 0;
				Player player = PlayerHandler.players[npc.spawnedBy];
				if (player == null) {
					player = c;
				}
				
				if (goodDistance(player,
						npc, 1)) {
					r3 = (switchAttackType
							? Misc.random(2)
							: npc.attackType);
				} else {
					r3 = Misc.random(1);
				}
				if (r3 == 0) {
					npc.attackType = 2;
					npc.attackStartGfx = Graphic.create(1626);
					npc.projectileId = 1627;
				} else if (r3 == 1) {
					npc.attackType = 1;
					npc.endGfx = 451;
					npc.attackStartGfx = Graphic.create(1834);
					npc.projectileId = -1;
				} else if (r3 == 2) {
					npc.attackType = 0;
					npc.projectileId = -1;
				}
				break;
			
			case 12743:
				if (Misc.random(4) == 1) {
					npc.attackType = 2;
					npc.endGfx = 540;
					npc.projectileId = 541;
				} else if (Misc.random(5) == 1) {
					npc.attackType = 1;
					npc.endGfx = 2884;
					npc.projectileId = 2883;
					if (c.applyFreeze(15))
						c.sendMessage("The snot makes you stuck.");
				} else {
					npc.attackType = 0;
					npc.endGfx = 2878;
					npc.projectileId = -1;
				}
				break;
			
			case 2743:
				npc.attackType = 2;
				npc.projectileId = 445;
				npc.endGfx = 446;
				break;
			
			case 2631:
				npc.attackType = 1;
				npc.projectileId = 443;
				break;
			
		}
	}
	
	/**
	 * load spell
	 **/
	public static void loadSpell2(int i) {
		npcs[i].attackType = 3;
		int random = Misc.random(3);
		if (random == 0) {
			npcs[i].projectileId = 393; // red
			npcs[i].endGfx = 430;
		} else if (random == 1) {
			npcs[i].projectileId = 394; // green
			npcs[i].endGfx = 429;
		} else if (random == 2) {
			npcs[i].projectileId = 395; // white
			npcs[i].endGfx = 431;
		} else if (random == 3) {
			npcs[i].projectileId = 396; // blue
			npcs[i].endGfx = 428;
		}
	}
	
	public static void multiAttackDamage(int i) {
		int max = getMaxHit(npcs[i].getNpcIndex());
		for (Player p : Server.getRegionManager().getLocalPlayers(npcs[i])) {
			final Client c = (Client) p;
			if (c.isDead() || c.hidePlayer) {
				continue;
			}
			if (c.goodDistance(NPCHandler.npcs[i], 15)) {
				if (RegionClip.rayTraceBlocked(npcs[i].getXtoPlayerLoc(c.getX()), npcs[i].getYtoPlayerLoc(c.getY()),
						c.getX(), c.getY(), npcs[i].getHeightLevel(),
						npcs[i].attackType != 0, c.getDynamicRegionClip())) {
					// c.sendMessage("Path blocked2");
					continue; // return;
				}
				if (npcs[i].attackType == 2) { // mage mutli attacks
					if (c.playerPrayerBook == PrayerBook.NORMAL
							? !c.prayerActive[Prayers.PROTECT_FROM_MAGIC
							.ordinal()]
							: !c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
						if (MagicAccuracy.successfulHit(npcs[i], c, null).isSuccessfulHit()) {
							int dam = Misc.random(max);
							c.getCombat().appendHit(c, dam, 0, 2, 2, false);
						} else {
							c.getCombat().appendHit(c, 0, 0, 2, 2, false);
						}
					} else {
						if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
							c.curses().deflectNPC(npcs[i], 0, 2);
						}
						c.getCombat().appendHit(c, 0, 0, 2, 2, false);
					}
				} else if (npcs[i].attackType == 1) { // range multi attacks
					if (c.playerPrayerBook == PrayerBook.NORMAL
							? !c.prayerActive[Prayers.PROTECT_FROM_MISSILES
							.ordinal()]
							: !c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
						if (RangeAccuracy.isAccurateImpact(npcs[i], c).isSuccessfulHit()) {
							int dam = Misc.random(max);
							c.getCombat().appendHit(c, dam, 0, 1, 1, false);
						} else {
							c.getCombat().appendHit(c, 0, 0, 2, 2, false);
						}
					} else {
						if (c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
							c.curses().deflectNPC(npcs[i], 0, 1);
						}
						c.getCombat().appendHit(c, 0, 0, 2, 2, false);
					}
				} else if (npcs[i].attackType == 0) { // melee multi attacks
					if (c.playerPrayerBook == PrayerBook.NORMAL
							? !c.prayerActive[Prayers.PROTECT_FROM_MELEE
							.ordinal()]
							: !c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
						if (MeleeAccuracy.successfulHit(npcs[i], c).isSuccessfulHit()) {
							int dam = Misc.random(max);
							c.getCombat().appendHit(c, dam, 0, 0, 0, false);
						} else {
							c.getCombat().appendHit(c, 0, 0, 2, 2, false);
						}
					} else {
						if (c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
							c.curses().deflectNPC(npcs[i], 0, 0);
						}
						c.getCombat().appendHit(c, 0, 0, 2, 2, false);
					}
				}
				if (npcs[i].endGfx > 0) {
					c.startGraphic(Graphic.create(npcs[i].endGfx, 0, npcs[i].endGraphicType));
				}
			}
		}
	}
	
	public static void multiAttackDamageNew(NPC npc, Player player) {
		
		//Nullify damage if player teleported
		if (player.teleTimer > 0) {
			return;
		}
		
		int damage = Misc.random(npc.maxHit);
		
		if (npc.attackType == 0) { // melee
			if (!MeleeAccuracy.successfulHit(npc, player).isSuccessfulHit()) {
				damage = 0;
			}

			if (damage > 0 && (player.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]
					|| player.curseActive[Curses.DEFLECT_MELEE.ordinal()])) {
				if (player.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
					player.curses().deflectNPC(npc, damage, 0);
				}

				damage = 0;
			}
		} else if (npc.attackType == 1) { // range hits
			if (!RangeAccuracy.isAccurateImpact(npc, player).isSuccessfulHit()) {
				damage = 0;
			}

			if (damage > 0 && (player.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]
					|| player.curseActive[Curses.DEFLECT_MISSILES.ordinal()])) {
				if (player.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
					player.curses().deflectNPC(npc, damage, 1);
				}

				damage = 0;
			}
			if (npc.endGfx > 0) {
				player.startGraphic(Graphic.create(npc.endGfx, npc.endGfxDelay, npc.endGraphicType));
			}
		} else if (npc.attackType == 2) { // mage hits
			boolean magicFailed = false;
			if (!MagicAccuracy.successfulHit(npc, player, null).isSuccessfulHit()) {
				damage = 0;
				magicFailed = true;
			}

			if (damage > 0 && (player.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
					|| player.curseActive[Curses.DEFLECT_MAGIC.ordinal()])) {
				if (player.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
					player.curses().deflectNPC(npc, damage, 2);
				}
				damage = 0;
				magicFailed = true;
			}
			
			if (npc.endGfx > 0 && !magicFailed) {
				player.startGraphic(Graphic.create(npc.endGfx, npc.endGfxDelay, npc.endGraphicType));
			} else {
				player.startGraphic(Graphic.create(85, npc.endGfxDelay, GraphicType.HIGH));
			}
		}
		
		player.getCombat().appendHit((Client) player, damage, 0, npc.attackType,
				npc.attackType, false);
		npc.onDamageApplied((Client) player, damage);
	}
	
	public static void multiAttackDamage(NPC attacker, Client c) {
		
		//Nullify damage if player teleported
		if (c.teleTimer > 0) {
			return;
		}
		
		int max = getMaxHit(attacker.getNpcIndex());
		
		int damage = Misc.random(max);
		
		if (attacker.attackType == 0) { // melee
			if (!MeleeAccuracy.successfulHit(attacker, c).isSuccessfulHit()) {
				damage = 0;
			}

			if (damage > 0 && (c.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]
					|| c.curseActive[Curses.DEFLECT_MELEE.ordinal()])) {
				if (c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
					c.curses().deflectNPC(attacker, damage, 0);
				}
				damage = 0;
			}
		} else if (attacker.attackType == 1) { // range hits
			if (!RangeAccuracy.isAccurateImpact(attacker, c).isSuccessfulHit()) {
				damage = 0;
			}

			if (damage > 0 && (c.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]
					|| c.curseActive[Curses.DEFLECT_MISSILES.ordinal()])) {
				if (c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
					c.curses().deflectNPC(attacker, damage, 1);
				}
				damage = 0;
			}
		} else if (attacker.attackType == 2) { // mage hits
			if (!MagicAccuracy.successfulHit(attacker, c, null).isSuccessfulHit()) {
				damage = 0;
			}

			if (damage > 0 && (c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
					|| c.curseActive[Curses.DEFLECT_MAGIC.ordinal()])) {
				if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
					c.curses().deflectNPC(attacker, damage, 2);
				}
				damage = 0;
			}
		}
		
		c.getCombat().appendHit(c, damage, 0, attacker.attackType,
				attacker.attackType, false);
		attacker.onDamageApplied(c, damage);
		
		if (attacker.endGfx > 0) {
			c.startGraphic(Graphic.create(attacker.endGfx, 0, attacker.endGraphicType));
		}
	}
	
	public static void multiAttackGfx(NPC attacker, Player defender, int gfx) {
		if (attacker.projectileId < 0) {
			return;
		}
		
		Projectile proj = new Projectile(attacker.projectileId,
				Location.create(attacker.getX() + offset(attacker, defender.getCurrentLocation(), true),
						attacker.getY() + offset(attacker, defender.getCurrentLocation(), false),
						attacker.getHeightLevel()),
				Location.create(defender.getX(), defender.getY(), defender.getZ()));
		proj.setStartHeight(43);
		proj.setEndHeight(31);
		proj.setStartDelay(65);
		proj.setLockon(defender.getProjectileLockon());
		proj.setEndDelay(getProjectileSpeed(attacker.npcIndex));
		GlobalPackets.createProjectile(proj);
	}
	
	public static void dungMultiAttackGfx(NPC attacker, Player defender) {
		if (attacker.projectileId < 0) {
			return;
		}
		
		Projectile proj = new Projectile(attacker.projectileId,
				Location.create(attacker.getX() + offset(attacker, defender.getCurrentLocation(), true),
						attacker.getY() + offset(attacker, defender.getCurrentLocation(), false),
						attacker.getHeightLevel()),
				Location.create(defender.getX(), defender.getY(), defender.getZ()));
		proj.setStartHeight(attacker.projectiletStartHeight);
		proj.setEndHeight(attacker.projectiletEndHeight);
		proj.setStartDelay(attacker.projectileDelay);
		proj.setSlope(attacker.projectiletSlope);
		proj.setLockon(defender.getProjectileLockon());
		proj.setEndDelay(getProjectileSpeed(attacker.npcIndex));
		GlobalPackets.createProjectile(proj);
	}
	
	public static void multiAttackGfx(Player attacker, Player defender, int gfx) {
		if (gfx < 0) {
			return;
		}
		
		Projectile proj = new Projectile(gfx,
				Location.create(attacker.getX(),
						attacker.getY(),
						attacker.getHeightLevel()),
				Location.create(defender.getX(), defender.getY(), defender.getZ()));
		proj.setStartHeight(43);
		proj.setEndHeight(31);
		proj.setStartDelay(65);
		proj.setLockon(defender.getProjectileLockon());
		proj.setEndDelay(85);
		GlobalPackets.createProjectile(proj);
	}
	
	public static boolean multiAttacks(int i) {
		switch (npcs[i].npcType) {
			
			case 6090:
				if (npcs[i].attackType == 0 || npcs[i].attackType == 2) {
					return true;
				}
			
			case 10705://soulgazer
				return true;
			
			case 5247:
				if (npcs[i].attackType == 1) {
					return true;
				}
			
			case 8528:
			case 8529:
				if (npcs[i].attackType == 1 || npcs[i].attackType == 2) {
					return true;
				}
			
			case 9176:
				if (npcs[i].attackType == 0) {
					return true;
				}
			
			case 13447:
				if (npcs[i].attackType == 1) {
					if (npcs[i].attackType == 2) {
						return true;
					}
				}
			case 6222:
				return true;
			
			case 6247:
				if (npcs[i].attackType == 2) {
					return true;
				}
			
			case 6260:
				if (npcs[i].attackType == 1) {
					return true;
				}
			
			case 4540:
				if (npcs[i].attackType == 1 || npcs[i].attackType == 2) {
					return true;
				}
			
			case 4174:
				if (npcs[i].attackType == 1 || npcs[i].attackType == 2) {
					return true;
				}
			
			case 10080:
				if (npcs[i].attackType == 1 || npcs[i].attackType == 2) {
					return true;
				}
			
			case 9771:
				if (npcs[i].attackType == 1) {
					return true;
				}
			
			case 12743:
				if (npcs[i].attackType == 1 || npcs[i].attackType == 2
						|| npcs[i].attackType == 3) {
					return true;
				}
			
			case 8133: // Corporeal beast
				if (npcs[i].attackType == 1) {
					if (npcs[i].attackType == 2) {
						return true;
					}
				}
			default:
				return false;
		}
		
	}

	private static AbstractNpc getAbstractNpc(int index, int npcType) {
		Class<? extends AbstractNpc> clazz = NpcPlugins.get(npcType);

		if (clazz != null) {
			try {
				Constructor<?> con = clazz.getConstructor(new Class[]{int.class, int.class});
				return (AbstractNpc) con.newInstance(index, npcType);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public static NPC newNPC(int npcType,
	                         int x, int y, int heightLevel,
	                         int walkingType) {
		int slot = Misc.findFreeNpcSlot();
		
		if (slot == -1) {
			return null;
		}
		
		AbstractNpc absNpc = getAbstractNpc(slot, npcType);
		
		NPC newNPC = absNpc != null ? absNpc : new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.walkingType = walkingType;
		newNPC.alwaysRespawn = true;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);

		switch (newNPC.walkingType) { // npc face update - alk update
			case 5: // west
				newNPC.turnNpc(newNPC.absX - 2, newNPC.absY);
				break;
			case 4: // east
				newNPC.turnNpc(newNPC.absX + 2, newNPC.absY);
				break;
			case 3: // south
				newNPC.turnNpc(newNPC.absX, newNPC.absY - 2);
				break;
			case 2: // north
				newNPC.turnNpc(newNPC.absX, newNPC.absY + 2);
				break;
		}
		
		addNpcToNPCSArray(newNPC, slot);
		
		newNPC.applyBoss(false);
		return newNPC;
	}
	
	public static void newNPCList(int npcType, String npcName, int combat,
	                              int HP) {
		if (combat == 0 && HP == 0) {
			return;
		}

		if (NPCDefinitions.getDefinitions()[npcType] != null) {
			return;
		}
		
		npcName = npcName.replaceAll("_", " ");

		if (!npcName.equalsIgnoreCase(EntityDef.forID(npcType).name)) {
			System.out.println(npcType + "npc doesnt match from cfg to def. name="+npcName+", def="+EntityDef.forID(npcType).name);
			return;
		}
		
		NPCDefinitions newNPCList = new NPCDefinitions(npcType);
		newNPCList.setNpcName(npcName);
		newNPCList.setNpcCombat(combat);
		newNPCList.setNpcHealth(HP);
		NPCDefinitions.getDefinitions()[npcType] = newNPCList;
	}
	
	public static boolean NpcVersusNpc(int NPCID, int killingId, Player c) {
		if (NPCID < 0) {
			return false;
		}
		if (NPCID == killingId) {
			return false;
		}
		if (SoulWars.getTeam(c) == Team.BLUE && RSConstants.inSoulWarsBlueBoss(c.absX, c.absY) || SoulWars.getTeam(c) == Team.RED && RSConstants.inSoulWarsRedBoss(c.absX, c.absY)) {
			return true;
		}
		if (npcs[NPCID] != null) {
			if (npcs[killingId] != null) {
				final int EnemyX = npcs[killingId].absX;
				final int EnemyY = npcs[killingId].absY;
				// int EnemyHP = npcs[killingId].getSkills().getLifepoints();
				// int hitDiff = Misc.random(npcs[NPCID].getSkills().getStaticLevel(Skills.HITPOINTS) / 8);
				// NPC n = npcs[killingId];
				FollowNpcCB(NPCID, killingId);
				// npcs[NPCID].turnNpc(EnemyX, EnemyY);
				if (npcs[NPCID].actionTimer == 0) {
					if (goodDistance(EnemyX, EnemyY, npcs[NPCID].absX,
							npcs[NPCID].absY, 2)) {
						if (NPCHandler.npcs[killingId].isDead() == true) {
							ResetAttackNPC(NPCID);
						} else {
							Animation attackAnim = NPCAnimations.getCombatEmote(npcs[NPCID].npcType, npcs[NPCID].attackType);
							if (attackAnim != null)
								npcs[NPCID].startAnimation(attackAnim);
							if (npcs[NPCID].isSummoningSkillNpc || npcs[NPCID].isPokemon) {
								c.getSA().attackNpc(NPCID, killingId);
							}
							npcs[NPCID].actionTimer = 7;
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public static int offset(NPC n, Location other, boolean checkX) {
		switch (n.getSize()) {
			case 7:
				return 3;
			case 6:
			case 5:
				return 2;
			case 4:
			case 3:
				return 1;
			default:
				return 0;
		}

//		if (n.getSize() == 1)
//			return 0;
//		int x = n.getX();
//		int y = n.getY();
//		int size = n.getSize();
//		if (n.getSize() == 5) {
//			size = 3;
//			x++;
//			y++;
//		}
//		if (checkX) {
//			if (other.getX() <= x)
//				return 0;
//			return Math.min(other.getX()-x, size-1);
//		} else { // check y
//			if (other.getY() <= y)
//				return 0;
//			return Math.min(other.getY()-y, size-1);
//		}
	}
	
	public static boolean pathBlocked(NPC attacker, Mob victim) {
		
		final int victimX = victim.getCurrentLocation().getX();
		final int victimY = victim.getCurrentLocation().getY();
		
		final int z = attacker.getCurrentLocation().getZ();
		
		final int attackerX = attacker.getXtoPlayerLoc(victim.getCurrentLocation().getX());
		final int attackerY = attacker.getYtoPlayerLoc(victim.getCurrentLocation().getY());
		
		double offsetX = Math.abs(attackerX - victimX);
		double offsetY = Math.abs(attackerY - victimY);
		
		int distance = TileControl.calculateDistance(attacker, victim);
		
		if (distance == 0) {
			return true;
		}
		
		offsetX = offsetX > 0 ? offsetX / distance : 0;
		offsetY = offsetY > 0 ? offsetY / distance : 0;
		
		int[][] path = new int[distance][5];
		
		int curX = attackerX;
		int curY = attackerY;
		int next = 0;
		int nextMoveX = 0;
		int nextMoveY = 0;
		
		double currentTileXCount = 0.0;
		double currentTileYCount = 0.0;
		
		while (distance > 0) {
			distance--;
			nextMoveX = 0;
			nextMoveY = 0;
			if (curX > victimX) {
				currentTileXCount += offsetX;
				if (currentTileXCount >= 1.0) {
					nextMoveX--;
					curX--;
					currentTileXCount -= offsetX;
				}
			} else if (curX < victimX) {
				currentTileXCount += offsetX;
				if (currentTileXCount >= 1.0) {
					nextMoveX++;
					curX++;
					currentTileXCount -= offsetX;
				}
			}
			if (curY > victimY) {
				currentTileYCount += offsetY;
				if (currentTileYCount >= 1.0) {
					nextMoveY--;
					curY--;
					currentTileYCount -= offsetY;
				}
			} else if (curY < victimY) {
				currentTileYCount += offsetY;
				if (currentTileYCount >= 1.0) {
					nextMoveY++;
					curY++;
					currentTileYCount -= offsetY;
				}
			}
			path[next][0] = curX;
			path[next][1] = curY;
			path[next][2] = z;
			path[next][3] = nextMoveX;
			path[next][4] = nextMoveY;
			next++;
		}
		for (int i = 0; i < path.length; i++) {
			if (!RegionClip.getClippingDirection(path[i][0],
					path[i][1], path[i][2], path[i][3],
					path[i][4], attacker.getDynamicRegionClip())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean pestControlNpc(int id) {
		if (NPCHandler.isTorcher(id)) {
			return true;
		}
		if (NPCHandler.isDefiler(id)) {
			return true;
		}
		if (NPCHandler.isSpinner(id)) {
			return true;
		}
		if (NPCHandler.isShifter(id)) {
			return true;
		}
		if (id >= 3732 && id <= 3782) {
			return true;
		}
		if (id >= 7367 && id <= 7368) {
			return true;
		}
		if (id == 11532) {
			return true;
		}
		if (id == 12323) {
			return true;
		}
		if (id == 12330) {
			return true;
		}
		if (id >= 6142 && id <= 6145) {
			return true;
		}
		return false;
	}
	
	public static boolean pestControlPortal(int id) {
		return (id >= 6142 && id <= 6145);
	}
	
	public static void process() {
		for (int i = 0; i < maxNPCs; i++) {
			NPC npc = npcs[i];
			if (npc == null) {
				continue;
			}
			
			npc.clearUpdateFlags();
		}
		
		for (int i = 0; i < maxNPCs; i++) {
			NPC npc = npcs[i];
			if (npc == null) {
				continue;
			}
			
			npc.process();
			
			// npc dies stops walking
			if (npc.getSkills().getStaticLevel(Skills.HITPOINTS) > 0 && npc.getSkills().getLifepoints() <= 0 && !npc.isDead()) {
				npc.setDead(true);
				npc.setDropsItemOnDeath(true);
				npc.moveX = npc.moveY = 0; // cheap way to reset npc
				// walking
			}
			
			if (npc.teleportDelay > 0) {
				npc.teleportDelay--;
			} else if (npc.teleportDelay == 0) {
				trueTeleport(npc, npc.teleX, npc.teleY,
						npc.teleHeight);
			}
			
			if (npc.getFreezeTimer() > -6) {
				npc.setFreezeTimer(npc.getFreezeTimer() - 1);
			}
			
			if (!npc.isDead() && !npc.isLockActions() && !npc.stopFollow && npc.summonedFor > 0) {
				Client owner = (Client) PlayerHandler.players[npc.summonedFor];
				if (owner == null || !owner.isActive) {
					npc.npcHandlerProcessDestroy();
					npcs[i] = null;
					continue;
				}
				if (npc.isPokemon) {
					PokemonFollowing.processFollow(npc);
				} else if (npc.isPetNpc() && !npc.isPokemon) {
					if (npc.useSmartFollow)
						TempPathFinder.followSmartPath(npc, owner);
					else
						followPlayer(npc, owner);
				} else { // wtf is this shit on the bottom...
					if (npc.getKillerId() <= 0
							&& !owner.underAttackByPlayer()
							&& npc.killNpc <= 0) {
						summoningFollow(npc, owner);                //follows owner
					} else if (owner.isAttackingPlayer()
							&& owner.inMulti()) {
						followEnemyPlayer(npc, owner.playerIndex, owner); //follow enemy player
					} else if (owner.underAttackByPlayer()
							&& owner.inMulti()) {
						followEnemyPlayer(npc, owner.underAttackBy, owner); // follow enemy player that is attacking owner
					}
				}
			}
			
			if (npc.actionTimer > 0) {
				npc.actionTimer--;
			}
			
			if (npc.actionTimer2 > 0) {
				npc.actionTimer2--;
			}
			
			if (npc.freezeTimer > 0) {
				npc.stuckCount = 0;
				npc.freezeTimer--;
			}
			
			if (npc.getAttackTimer() > 0) {
				npc.setAttackTimer(npc.getAttackTimer() - 1);
			}
			
			Boss boss = npc.setOrGetBoss();
			
			if (boss != null) {
				boss.process(npc);
			}

			if (npc.spawnedBy > 0) { // delete summons npc
				Player player = npc.getSpawnedByPlayer();
				if (player == null || player.heightLevel != npc.heightLevel || player.respawnTimer > 0
						|| !player.goodDistance(npc, 60)) {

					if (player != null && !player.disconnected) {
						player.getPacketSender().createArrow(1, -1);
					}

					npc.npcHandlerProcessDestroy();
					npcs[i] = null;
					continue;
				}
			}
			
			if (npc.lastX != npc.getX()
					|| npc.lastY != npc.getY()) {
				npc.lastX = npc.getX();
				npc.lastY = npc.getY();
			}
			
			/**
			 * Attacking player
			 **/
			if (!npc.isDead()) {
				if (npc.getAttackTimer() == 0 && !npc.isLockActions() && npc.getRetargetTick() < 30) {
					npc.setRetargetTick(npc.getRetargetTick() + 1);
				}
				if (!npc.isLockActions() && isAggressive(npc) && npc.getRetargetTick() > 17
						&& npc.underAttackBy < 1 && npc.underAttackBy2 < 1 && npc.killNpc == -1
						&& !(npc.getKillerId() > 0)) { // does not switches
					// attackers aggro npcs
					npc.setRetargetTick(14);
					
					int randomPlayerIndex = getCloseRandomPlayer(i, canNoClipAttacks(npc), true);
					if (randomPlayerIndex > 0 && PlayerHandler.players[randomPlayerIndex] != null) {
						npc.setKillerId(randomPlayerIndex);
						npc.setRetargetTick(0);
					}
					if (!npc.randomWalk && npc.getKillerId() <= 0) {
						npc.randomWalk = true;
						if (npc.walksHome())
							npc.setWalkingHome(true);
						npc.resetFace();
					}
				}
			}
			
			// alivetick and kill at 0
			if (npc.getAliveTick() > 0) {
				npc.setAliveTick(npc.getAliveTick() - 1);
			}
			if (npc.getAliveTick() == 0) {
				npc.setDead(true);
				npc.setAliveTick(-1);
			}
			
			if (System.currentTimeMillis() - npc.lastDamageTaken > 5000) {
				npc.underAttackBy = 0;
				npc.underAttackBy2 = 0;
			}
			
			if ((npc.getKillerId() > 0)
					&& !npc.isWalkingHome() && !npc.isLockActions() && npc.isRetaliates()) {
				
				if (!npc.isDead() && !npc.isSummoningSkillNpc && !npc.isPetNpc()) {
					final Client c = (Client) PlayerHandler.players[npc.getKillerId()];
					if (c != null) {
						if (c.summonedPokemon != null && c.summoned != null && npc.underAttackBy2 == c.summoned.getId()) {
							npc.killNpc = c.summoned.getId();
							npc.setKillerId(0);
						} else {
							
							if (npc.getKillerId() > 0 && PlayerHandler.players[npc.getKillerId()] != null
									&& !PlayerHandler.players[npc.getKillerId()].isDead()) {
								
								if (npc.getAttackTimer() == 0
										&& !npc.isSummoningSkillNpc) {
									attackPlayer(c, npc, boss);
								}
							}
							
							if (npc.getKillerId() > 0)
								followPlayer(npc, c);
						}
					}
				}
			} else {
				
				/**
				 * Random walking and walking home
				 **/
				
				if (!npc.isDead() && !npc.isLockActions()) {
					if (npc.isWalkingHome() && npc.absX == npc.makeX && npc.absY == npc.makeY) {
						npc.setWalkingHome(false);
						npc.setRetargetTick(15);
					} else if (npc.isWalkingHome()) {
						
						npc.getSkills().heal(3);
						
						npc.moveX = GetMove(npc.absX, npc.makeX);
						npc.moveY = GetMove(npc.absY, npc.makeY);
						
						// pathfinding to home
						//if (!npc.getWalkingQueue().hasPath())
						//PathFinder.findRoute(npc, npc.makeX, npc.makeY, true, 0, 0);
						
						npc.stuckCount = 0;
					}
					
					if (!npc.isWalkingHome()) {
						if (npc.spawnedBy == 0 && npc.alwaysRespawn) {
							if ((npc.absX > npc.makeX
									+ Config.NPC_RANDOM_WALK_DISTANCE
									+ npc.getWalkDistance())
									|| (npc.absX < npc.makeX
									- Config.NPC_RANDOM_WALK_DISTANCE
									- npc.getWalkDistance())
									|| (npc.absY > npc.makeY
									+ Config.NPC_RANDOM_WALK_DISTANCE
									+ npc.getWalkDistance())
									|| (npc.absY < npc.makeY
									- Config.NPC_RANDOM_WALK_DISTANCE
									- npc.getWalkDistance())) {
								npc.randomWalk = true;
								if (npc.walksHome())
									npc.setWalkingHome(true);
								npc.setKillerId(0);
								npc.resetFace();
								// npc.forceChat("walking home");
							}
						}
						if (npc.walkingType == 1 && npc.getKillerId() <= 0 && npc.underAttackBy < 1 && npc.underAttackBy2 < 1 && npc.canWalk() && !npc.getWalkingQueue().hasPath()) {
							if (Misc.random(9) < 3) {
								
								int MoveX = 0;
								int MoveY = 0;
								int Rnd = Misc.random(9);
								if (Rnd == 1) {
									MoveX = 1;
									MoveY = 1;
								} else if (Rnd == 2) {
									MoveX = -1;
								} else if (Rnd == 3) {
									MoveY = -1;
								} else if (Rnd == 4) {
									MoveX = 1;
								} else if (Rnd == 5) {
									MoveY = 1;
								} else if (Rnd == 6) {
									MoveX = -1;
									MoveY = -1;
								} else if (Rnd == 7) {
									MoveX = -1;
									MoveY = 1;
								} else if (Rnd == 8) {
									MoveX = 1;
									MoveY = -1;
								}
								
								if (MoveX == 1) {
									if (npc.absX + MoveX < npc.makeX
											+ npc.getWalkDistance()) {
										npc.moveX = MoveX;
									} else {
										npc.moveX = 0;
									}
								}
								
								if (MoveX == -1) {
									if (npc.absX - MoveX > npc.makeX
											- npc.getWalkDistance()) {
										npc.moveX = MoveX;
									} else {
										npc.moveX = 0;
									}
								}
								
								if (MoveY == 1) {
									if (npc.absY + MoveY < npc.makeY
											+ npc.getWalkDistance()) {
										npc.moveY = MoveY;
									} else {
										npc.moveY = 0;
									}
								}
								
								if (MoveY == -1) {
									if (npc.absY - MoveY > npc.makeY
											- npc.getWalkDistance()) {
										npc.moveY = MoveY;
									} else {
										npc.moveY = 0;
									}
								}
								
								//if (npc.isRevenantNpc()
								//		&& npc.absY + npc.moveY < 3544) { // wildy level 4
								//	npc.moveY = 0;
								//}
								
							}
						}
					}
				}
			}
			/***********************************************
			 * allantois addition
			 ***************************************************/
			
			if (npc.getKillerId() != 0) {
				npc.killNpc = -1;
			}
			
			if (npc.killNpc != -1 && !npc.isLockActions()) {
				followNpc(i, npc.killNpc);
				if (npc.getAttackTimer() == 0) {
					attackNpc(i);
				}
				
			}
			if (npc.isDead() && (npc.isSummoningSkillNpc || npc.isPetNpc())) {
				final Client o = (Client) PlayerHandler.players[npc.spawnedBy];
				if (o != null) {
					if (npc.isPokemon && o.summoned != null) {
						PokemonButtons.pickupPet(o, true);
					} else if (npc.isSummoningSkillNpc) {
						o.getSummoning().dismissFamiliar(true);
					}
				}
				
				if (npc != null) {
					npc.npcHandlerProcessDestroy();
					npcs[i] = null;
					continue;
				}
			}
			/***********************************************
			 * end of allantois addition
			 ********************************************/
			
			if (npc.isDead() && !npc.isSummoningSkillNpc) {
				if (npc.actionTimer == 0 && !npc.applyDead
						&& !npc.needRespawn) {
					final int killerMID = npc.getKillerBySQLIndex(); //npc.killedbyNameBase37 = npc.getKillerBase37();
					
					npc.actionTimer = 4; // delete time
					
					Boss boss1 = npc.setOrGetBoss();
					
					if (npc.npcType != -1 && !npc.isPetNpc()) {
						
						int deathAnim = NPCAnimations.getDeath(npc);
						
						if (boss1 != null && boss1.getDeathAnimation() != -1) {
							deathAnim = boss1.getDeathAnimation();
						}
						
						npc.startAnimation(deathAnim);
					}
					npc.removeNpcClip();
					
					npc.getWalkingQueue().reset();
					npc.resetFace();
					npc.freezeTimer = 0;
					npc.applyDead = true;
					resetPlayersInCombat(i);
					
					final Client c = (Client) PlayerHandler
							.getPlayerByMID(killerMID);
					
					if (npc.isBarbAssaultNpc() && !npc.barbAssaultRunning) {
						npc.npcHandlerProcessDestroy();
						npcs[i] = null;
						continue;
					}
					
					if (c != null && c.isActive) {
						if (npc.isBarbAssaultNpc() && BarbarianAssault.onNpcDeath(c, npc)) {
							continue;
						}

						handleBossDeath(c, npc);
						Slayer.appendKill(c, npc);
						appendNpcKill(c, npc);
						dropItems(c, npc);
						if (npc.npcType == 13447 || npc.npcType == 2012) { // nex & zulrah drops two items at once
							dropItems(c, npc);
						}
					}
					
					if (boss1 != null) {
						boss1.onDeath(npc, c);
					}
					
					npc.onDeath();
					
					handleDungeoneeringDeath(npc, c);
					npc.getInflictors().clear();
					if (npc.npcType >= 1532 && npc.npcType <= 1535) {
						Barricades.handleBarricadeObjectRemoval(npc.makeX, npc.makeY, npc.heightLevel);
						
						if (RSConstants.inCastleWarsArena(npc.makeX, npc.makeY))
							Barricades.removeFromArray(i, true);
						else// if (RSConstants.inSoulWars(n.makeX, n.makeY))
							Barricades.removeFromArray(i, false);
					}
					
				} else if (npc.actionTimer == 0 && npc.applyDead == true
						&& npc.needRespawn == false) {
					npc.needRespawn = true;
					npc.actionTimer = getRespawnTime(i); // respawn time
					npc.absX = npc.makeX;
					npc.absY = npc.makeY;
					npc.setLastLocation(Location.create(npc.getX(), npc.getY(), npc.getZ()));
					npc.setLocation(npc.getX(), npc.getY());
					npc.getSkills().healToFull();
					npc.startAnimation(0x328);

					onNpcDeath(npc);

					if (npc.isFightCavesNPC()) {
						FightCaves.killedTzhaar(npc);
					}
					npc.afterNpcDeath();
				} else if (npc.actionTimer == 0
						&& npc.needRespawn == true) {
					if (npc.spawnedBy > 0) {
						if (PlayerHandler.players[npc.spawnedBy] != null
								&& !PlayerHandler.players[npc.spawnedBy].disconnected) {
							PlayerHandler.players[npc.spawnedBy]
									.getPacketSender().createArrow(1, -1);
						}
						npc.npcHandlerProcessDestroy();
						npcs[i] = null;
						continue;
					}
					if (!npc.alwaysRespawn) {
						npc.npcHandlerProcessDestroy();
						npcs[i] = null;
						continue;
					}
					int old1 = npc.npcType;
					int old2 = npc.makeX;
					int old3 = npc.makeY;
					int old4 = npc.heightLevel;
					int old5 = npc.walkingType;
					int homeOwnerId = npc.getHomeOwnerId();
					
					npc.npcHandlerProcessDestroy();
					npcs[i] = null;
					NPC newNPC = newNPC(old1, old2, old3, old4, old5);
					
					if (newNPC != null) {
						newNPC.setHomeOwnerId(homeOwnerId);

						if (npc.insideInstance()) {
							npc.getInstanceManager().initNpcDefaults(newNPC);
						}
					}

					continue;
				}
			}

			if (npc.despawnNextTick) {
				npc.npcHandlerProcessDestroy();
				npcs[i] = null;
				continue;
			}

			//old pi movement stuff
			//if (npc.isPetNpc()) // lock movement to pets only for debug/test
			if (!npc.isDead() && (npc.moveX != 0 || npc.moveY != 0) && !npc.getWalkingQueue().hasPath()) {
				if (!npc.isWalkingHome() && !npc.noClip)
					handleClipping(i);
				if (npc.moveX != 0 || npc.moveY != 0) {
					npc.getWalkingQueue().reset(); // since this is single step movement here, we just reset it and then take one step. One step at a time makes it impossible to run.
					npc.getWalkingQueue().getWalkTo().setX(npc.getX() + npc.moveX);
					npc.getWalkingQueue().getWalkTo().setY(npc.getY() + npc.moveY);
					npc.getWalkingQueue().getWalkTo().setZ(npc.getHeightLevel());
					npc.getWalkingQueue().addPath(npc.getWalkingQueue().getWalkTo());
				}
			}
			
			if (npc.moveNpcThisTick) {
				npc.moveNpcThisTick = false;
			}
			
			//new walking process
			//if (npc.isPetNpc()) // lock movement to pets only for debug/test
			if (!npc.isTeleporting())
				npc.getWalkingQueue().update();
			
			if (!npc.isSafeSpotNPC() && !npc.isDead() && !npc.isLockActions() && (npc.underAttackBy2 > 0 || npc.underAttackBy > 0)
					&& (npc.moveX == 0 && npc.moveY == 0)
					&& !(npc.freezeTimer > 0)
				/*&& !npc.isPestControlNPC()*/) {
				npc.stuckCount++;
				if (npc.stuckCount > 8) {
					npc.forceChat("Forget it, darn safe spotter!");
					npc.resetFace();
					npc.randomWalk = true;
					if (npc.walksHome())
						npc.setWalkingHome(true);
					npc.setKillerId(0);
				}
			}
			
			if (npc.isShoved())
				npc.setShoved(false);
			
		}
		
	}
	
	private static void handleDungeoneeringDeath(NPC npc, Client c) {
		if (npc.dungeoneeringType != DungeonConstants.BOSS_NPC && npc.dungeoneeringType != DungeonConstants.GUARDIAN_NPC) {
			return;
		}
		
		DungeonManager manager = npc.dungeonManager;
		if (manager == null && c != null && c.insideDungeoneering()) {//if somehow its null try to find it from player
			manager = c.dungParty.dungManager;
		}
		
		if (manager == null) {
			return;
		}
		
		RoomReference reference = npc.reference;
		if (reference == null) {//lets try to create it?
			reference = manager.getCurrentRoomReference(npc.getCurrentLocation());
		}
		
		if (reference == null) {
			return;
		}
		
		if (npc.dungeoneeringType == DungeonConstants.BOSS_NPC) {
			manager.openStairs(reference);
		} else if (npc.dungeoneeringType == DungeonConstants.GUARDIAN_NPC) {
			manager.totalGuardiansKilled++;
		}
	}
	
	public static boolean ResetAttackNPC(int NPCID) {
		npcs[NPCID].IsUnderAttackNpc = false;
		npcs[NPCID].IsAttackingNPC = false;
		npcs[NPCID].resetFace();
		return true;
	}
	
	/**
	 * Resets players in combat
	 */
	
	public static void resetPlayersInCombat(int i) {
		for (Player p : Server.getRegionManager().getLocalPlayers(npcs[i])) {
			if (p != null && p.underAttackBy2 == i) {
				p.underAttackBy2 = 0;
			}
		}
	}

	/**
	 * Summon npc, barrows, etc
	 **/
	public static NPC spawnNpc(final Player c, int npcType, int x, int y,
	                           int heightLevel, int walkingType, boolean attackPlayer, boolean hintIcon) {
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		if (slot == -1) {
			// Misc.println("No Free Slot");
			return null; // no free slot found
		}

		AbstractNpc absNpc = getAbstractNpc(slot, npcType);
		NPC newNPC = absNpc !=  null ? absNpc : new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = walkingType;
		newNPC.spawnedBy = c.getId();
		newNPC.resetsAggro = false; // spawned npcs for players should never reset their aggro
		if (hintIcon) {
			c.getPacketSender().createArrow(1, slot);
		}

		newNPC.applyBoss(false);

		if (attackPlayer) {
			newNPC.setKillerId(c.getId());
		}
		
		EntityDef def = EntityDef.forID(npcType);
		if (def != null) {
			if (def.combatLevel > 0) {

//				newNPC.walkingType = 1;
			}
		} else {
			System.out
					.println("ERROR REPORT TO JULIUS. EntityDef NULL on NPCTYPE:"
							+ npcType);
		}
		
		addNpcToNPCSArray(newNPC, slot);
		
		return newNPC;
	}

	/**
	 * MUST ALREADY CONTAIN PROPER NPC INDEX
	 */
	public static NPC spawnNpc(NPC newNPC, int x, int y, int heightLevel,
	                           int walkingType) {
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = walkingType;

		EntityDef def = EntityDef.forID(newNPC.npcType);
		if (def != null) {
			if (def.combatLevel > 0) {
				
				newNPC.walkingType = 0;
			}
		} else {
			System.out
					.println("ERROR REPORT TO JULIUS. EntityDef NULL on NPCTYPE:"
							+ newNPC.npcType);
		}
		
		addNpcToNPCSArray(newNPC, newNPC.getId());
		return newNPC;
	}
	
	public static NPC spawnNpc(Player p, int npcType, int x, int y, int heightLevel,
	                           int walkingType, int HP) {
		
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		if (slot == -1) {
			// Misc.println("No Free Slot");
			return null; // no free slot found
		}
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = walkingType;
		newNPC.getSkills().setStaticLevel(Skills.HITPOINTS, HP);
		newNPC.spawnedBy = p.getId();
		
		EntityDef def = EntityDef.forID(npcType);
		if (def != null) {
			if (def.combatLevel > 0) {
				
				newNPC.walkingType = 0;
			}
		} else {
			System.out
					.println("ERROR REPORT TO JULIUS. EntityDef NULL on NPCTYPE:"
							+ npcType);
		}
		
		addNpcToNPCSArray(newNPC, slot);
		return newNPC;
	}
	
	public static NPC spawnNpc2(int npcType, int x, int y, int heightLevel,
	                            int walkingType) {
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		if (slot == -1) {
			// Misc.println("No Free Slot");
			return null; // no free slot found
		}
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = walkingType;
		
		EntityDef def = EntityDef.forID(npcType);
		if (def != null) {
			if (def.combatLevel > 0) {
				newNPC.walkingType = 0;
			}
		} else {
			System.out
					.println("ERROR REPORT TO JULIUS. EntityDef NULL on NPCTYPE:"
							+ npcType);
		}
		
		addNpcToNPCSArray(newNPC, slot);
		
		if (pestControlNpc(npcType) || NPCHandler.isShifter(npcType)
				|| NPCHandler.isSpinner(npcType)
				|| NPCHandler.isDefiler(npcType)
				|| NPCHandler.isTorcher(npcType)) {
			PestControl.addNpc(newNPC);
			newNPC.setPestControlNPC(true);
		}
		return newNPC;
	}
	
	public static NPC spawnPenguin(int npcType, int x, int y, int heightLevel,
	                               int WalkingType) {
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}

		if (slot == -1) {
			// Misc.println("No Free Slot");
			return null; // no free slot found
		}
		
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = WalkingType;
		newNPC.applyBoss(false);
		addNpcToNPCSArray(newNPC, slot);
		
		return newNPC;
	}
	
	public static NPC spawnDungeoneering(int npcType, int x, int y, int heightLevel,
	                                     int WalkingType, DungeonManager manager, RoomReference reference) {
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		if (slot == -1) {
			// Misc.println("No Free Slot");
			return null; // no free slot found
		}
		
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.walkingType = WalkingType;
		NpcStatsEntry entry = NpcStats.stats.get(npcType);
		if (entry != null) {
			newNPC.maxHit = entry.getMaxHit();
			newNPC.properties = entry.getProperties();
		}

		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.dungeonManager = manager;
		newNPC.reference = reference;
		newNPC.applyBoss(false);
		addNpcToNPCSArray(newNPC, slot);
		manager.addNpc(newNPC);
		newNPC.setWalksHome(false);
		
		return newNPC;
	}
	
	public static NPC spawnRaids(int npcType, int x, int y, int heightLevel,
	                             int WalkingType, RaidsManager manager) {
		// first, search for a free slot
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		if (slot == -1) {
			// Misc.println("No Free Slot");
			return null; // no free slot found
		}
		
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.walkingType = WalkingType;
		NpcStatsEntry entry = NpcStats.stats.get(npcType);
		if (entry != null) {
			newNPC.maxHit = entry.getMaxHit();
			newNPC.properties = entry.getProperties();
		}

		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.applyBoss(false);
		addNpcToNPCSArray(newNPC, slot);
		manager.addNpc(newNPC);
		newNPC.setWalksHome(false);
		
		return newNPC;
	}

	public static NPC spawnNpc(int npcType, Location location) {
		return spawnNpc(npcType, location.getX(), location.getY(), location.getZ());
	}

	public static NPC spawnNpc(int npcType, int x, int y, int heightLevel) {
		int slot = Misc.findFreeNpcSlot();
		if (slot == -1) {
			return null;
		}

		AbstractNpc absNpc = getAbstractNpc(slot, npcType);

		NPC newNPC = absNpc !=  null ? absNpc : new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		NpcStatsEntry entry = NpcStats.stats.get(npcType);
		if (entry != null) {
			newNPC.maxHit = entry.getMaxHit();
			newNPC.properties = entry.getProperties();
		}

		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.setWalksHome(false);
		newNPC.applyBoss(false);

		addNpcToNPCSArray(newNPC, slot);

		return newNPC;
	}

	public static boolean specialCase(Client c, int i) { // responsible for npcs
		// that
		// much
		if (goodDistance(c, npcs[i], 8)
				&& !goodDistance(c, npcs[i], distanceRequired(i))) {
			return true;
		}
		return false;
	}
	
	//leaving this for retarded summoning shit
	public static void startAnimation(int animId, int i) {
		if (npcs[i] == null) {
			return;
		}
		npcs[i].startAnimation(animId);
	}
	
	public static int stepAway(NPC n) {
		if (RegionClip.getClippingDirection(n.getX() - 1, n.getY(), n.heightLevel, -1, 0, n.getDynamicRegionClip())
				&& n.walkingDir != 1) {
			n.moveX = -1;
			n.moveY = 0;
			return 0;
		} else if (RegionClip.getClippingDirection(n.getX() + 1, n.getY(), n.heightLevel,
				1, 0, n.getDynamicRegionClip()) && n.walkingDir != 0) {
			n.moveX = 1;
			n.moveY = 0;
			return 1;
		} else if (RegionClip.getClippingDirection(n.getX(), n.getY() - 1, n.heightLevel,
				0, -1, n.getDynamicRegionClip()) && n.walkingDir != 3) {
			n.moveX = 0;
			n.moveY = -1;
			return 2;
		} else if (RegionClip.getClippingDirection(n.getX(), n.getY() + 1, n.heightLevel,
				0, 1, n.getDynamicRegionClip()) && n.walkingDir != 2) {
			n.moveX = 0;
			n.moveY = 1;
			return 3;
		}
		return -1;
	}
	
	public static void stepBack(NPC n, Player p) {
		int sizeOff = n.getSize() / 2;
		int x = 0; // // n.getXtoPlayerLoc(p.getX()) - p.getX();
		int npcX = n.getX() + sizeOff;
		int y = 0; // n.getYtoPlayerLoc(p.getY()) - p.getY();
		int npcY = n.getY() + sizeOff;
		if (npcY > p.getY()) {
			if (RegionClip.getClippingDirection(n.getX(), n.getY() + 1, n.getZ(), 0, 1, n.getDynamicRegionClip())) {
				y = 1;
				x = 0;
			}
		} else {
			if (RegionClip.getClippingDirection(n.getX(), n.getY() - 1, n.getZ(), 0, -1, n.getDynamicRegionClip())) {
				y = -1;
				x = 0;
			}
		}
		if (npcX > p.getX()) {
			if (RegionClip.getClippingDirection(n.getX() + 1, n.getY(), n.getZ(), 1, 0, n.getDynamicRegionClip())) {
				x = 1;
				y = 0;
			}
		} else {
			if (RegionClip.getClippingDirection(n.getX() - 1, n.getY(), n.getZ(), -1, 0, n.getDynamicRegionClip())) {
				x = -1;
				y = 0;
			}
		}
//		System.out.println(String.format("X:%d - Y:%d", x, y));
		
		n.moveX = x;
		n.moveY = y;
	}
	
	/**
	 * Summoning follow
	 */
	public static void summoningFollow(NPC n, Client c) {
		if (!n.canWalk())
			return;
		if (!n.isSummoningSkillNpc && !n.isPokemon)
			return;
		if (c.getZ() != n.getHeightLevel()) {
			c.getSummoning().callOnTeleport();
			n.underAttackBy = 0;
			return;
		}
		if (!PlayerConstants.goodDistance(c.getCurrentLocation().getX(),
				c.getCurrentLocation().getY(), n.getX(), n.getY(), 8)) {
			c.getSummoning().callOnTeleport();
			n.underAttackBy = 0;
			return;
		}
		if (n.underAttackBy == 0) {
			
			int playerX = c.getCurrentLocation().getX();
			int playerY = c.getCurrentLocation().getY();
			n.randomWalk = false;
			if (n.heightLevel == c.getHeightLevel()
					&& goodDistance(n.getX(), n.getY(), playerX, playerY, 8)) {
				if (c != null && n != null) {
					if (playerY < n.absY) {
						n.moveX = GetMove(n.absX, playerX);
						n.moveY = GetMove(n.absY, playerY + 1);
					} else if (playerY > n.absY) {
						n.moveX = GetMove(n.absX, playerX);
						n.moveY = GetMove(n.absY, playerY - 1);
					} else if (playerX < n.absX) {
						n.moveX = GetMove(n.absX, playerX + 1);
						n.moveY = GetMove(n.absY, playerY);
					} else if (playerX > n.absX) {
						n.moveX = GetMove(n.absX, playerX - 1);
						n.moveY = GetMove(n.absY, playerY);
					} else if (playerX == n.absX || playerY == n.absY) {
						int o = Misc.random(3);
						switch (o) {
							case 0:
								n.moveX = GetMove(n.absX, playerX);
								n.moveY = GetMove(n.absY, playerY + 1);
								break;
							
							case 1:
								n.moveX = GetMove(n.absX, playerX);
								n.moveY = GetMove(n.absY, playerY - 1);
								break;
							
							case 2:
								n.moveX = GetMove(n.absX, playerX + 1);
								n.moveY = GetMove(n.absY, playerY);
								break;
							
							case 3:
								n.moveX = GetMove(n.absX, playerX - 1);
								n.moveY = GetMove(n.absY, playerY);
								break;
						}
					}
				}
			} else {
				// c.getSummoning().callFamiliar();
				// n.underAttackBy = 0;
			}
			
			if (n.IsAttackingNPC != true) {
				n.face(c);
			}
			n.randomWalk = false;
		}
	}
	
	public static NPC summonNPC(Player player, int npcType, int x, int y,
	                            int heightLevel, int walkingType, int HP) {
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = 0;//walkingType;
		newNPC.getSkills().setStaticLevel(Skills.HITPOINTS, HP);
		newNPC.spawnedBy = newNPC.summonedFor = player.getId();
		if (SummoningData.isSummonNpc(npcType)) {
			newNPC.isSummoningSkillNpc = true;
		}
		player.summoningMonsterId = slot;
		if (Pet.isPetNPC(npcType)) {
			newNPC.summoner = (Client) player;
			newNPC.setNpcAsPet(true);
		}
		addNpcToNPCSArray(newNPC, slot);
		if (newNPC.isSummoningSkillNpc) {
			newNPC.startGraphic(Graphic.create(1315));
		}
		newNPC.applyBoss(true);
		if (player.wildLevel < 1 && newNPC.npcType != 6808
				&& !Pet.isPetNPC(npcType)) {
			newNPC.transform(npcType - 1);
		}
		return newNPC;
	}
	
	/**
	 * Summon a new npc for Summoning skill
	 */
	public static NPC summonNPCforSummoning(Player c, int npcType, int x, int y,
	                                        int heightLevel, int walkingType, int HP) {
		
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.walkingType = 0;//walkingType;
		newNPC.getSkills().setStaticLevel(Skills.HITPOINTS, HP);
		newNPC.spawnedBy = newNPC.summonedFor = c.getId();
		newNPC.teleEndGfx = Summoning.TELEPORT_GFX;
		if (SummoningData.isSummonNpc(npcType)) {
			newNPC.isSummoningSkillNpc = true;
		}
		c.summoningMonsterId = slot;
		if (Pet.isPetNPC(npcType)) {
			newNPC.setNpcAsPet(true);
		}
		addNpcToNPCSArray(newNPC, slot);
		if (newNPC.isSummoningSkillNpc) {
			newNPC.startGraphic(Graphic.create(1315));
		}
		if (c.wildLevel < 1 && newNPC.npcType != 6808
				&& !Pet.isPetNPC(npcType)) {
			newNPC.transform(npcType - 1);
		}
		return newNPC;
	}
	
	public static NPC summonPokemon(Client c, int npcType, int x, int y,
	                                int heightLevel, int hp, int maxHp) {
		int slot = -1;
		for (int i = 1; i < maxNPCs; i++) {
			if (npcs[i] == null) {
				slot = i;
				break;
			}
		}
		NPC newNPC = new NPC(slot, npcType);
		newNPC.absX = x;
		newNPC.absY = y;
		newNPC.makeX = x;
		newNPC.makeY = y;
		newNPC.heightLevel = heightLevel;
		newNPC.setLastLocation(Location.create(newNPC.getX(), newNPC.getY(), newNPC.getZ()));
		newNPC.setLocation(newNPC.absX, newNPC.absY);
		newNPC.getSkills().setStaticLevel(Skills.HITPOINTS, maxHp);
		newNPC.getSkills().setLifepoints(hp);
		newNPC.spawnedBy = newNPC.summonedFor = c.getId();
		c.summoningMonsterId = slot;
		newNPC.setNpcAsPet(true);
		newNPC.isPokemon = true;
		newNPC.useSmartFollow = true;
		newNPC.getUpdateFlags().add(UpdateFlag.PET);
		addNpcToNPCSArray(newNPC, slot);
		return newNPC;
	}

	public static void trueTeleport(NPC npc, Location location) {
		trueTeleport(npc, location.getX(), location.getY(), location.getZ());
	}

	public static void trueTeleport(NPC npc, int teleX, int teleY, int teleHeight) {
		npc.didTeleport = true;
		teleportMove(npc, teleX, teleY, teleHeight);
	}

	public static void teleportMove(NPC npc, Location location) {
		teleportMove(npc, location.getX(), location.getY(), location.getZ());
	}

	public static void teleportMove(NPC npc, int teleX, int teleY, int teleHeight) {
		npc.needRespawn = false;
		if (npc.teleEndGfx != null) {
			npc.startGraphic(npc.teleEndGfx);
		}
		if (npc.teleEndAnim != null) {
			npc.startAnimation(npc.teleEndAnim);
		}
		npc.teleportDelay = -1;
		
		npc.moveNpcNow(teleX, teleY, teleHeight);
	}

	public static void addNpcToNPCSArray(NPC npc, int slot) {
		npcs[slot] = npc;
		npc.onSpawn();
		setAggroDistance(npc);
	}
	
	private static void setAggroDistance(NPC npc) {
		switch (npc.npcType) {
			
			case 1341: //dagganoths
			case 2454: //dagganoths
			case 2455: //dagganoths
			case 2456: //dagganoths
				npc.setAggroRadius(12);
				break;
			
			case 13465: //revs
			case 13469: //revs
			case 13471: //revs
			case 13472: //revs
			case 13473: //revs
			case 13474: //revs
			case 13475: //revs
			case 13476: //revs
			case 13477: //revs
			case 13478: //revs
			case 13479: //revs
			case 13480: //revs
			case 13481: //revs
			case 28://pit scorpion
			case 4403://scorpions
				npc.setAggroRadius(4);
				break;
			
			case 7151://demonic gorillas
				npc.setAggroRadius(7);
				break;
			
		}
	}
	
}