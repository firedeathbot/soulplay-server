package com.soulplay.game.model.npc.animations;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.combat.CombatStatics;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

/**
 * Loads the npc animations from a tsv (tab seperated values) file. The format
 * is id-\t-death animation-\t-block animation-\t-attack animation-\t-range
 * animation
 *
 * @author Advocatus
 * @author Jire - refactored and optimized.
 */
public final class NPCAnimations {
	
	private static Int2ObjectMap<AnimationDefinition> definitions = new Int2ObjectOpenHashMap<>();
	
	/**
	 * Grabs combat emote of an npc by npcType
	 *
	 * @param npcType - the npc id
	 * @return combat emote id
	 */
	public static Animation getCombatEmote(int npcType) {
		AnimationDefinition def = definitions.get(npcType);
		if (def != null && def.getAttack().getId() != -2) {
			return def.getAttack();
		}
		
		return new Animation(-1);
	}
	
	private static Int2ObjectMap<NpcCombatAnim> combatMap = new Int2ObjectOpenHashMap<>();
	
	public static Animation getCombatEmote(int npcType, int attackType) {
		NpcCombatAnim cmbAnim = combatMap.get(npcType);
		if (cmbAnim != null) {
			Animation anim = cmbAnim.getAnimationByAttackType(attackType);
			if (anim != null)
				return anim;
		}
		
		AnimationDefinition def = definitions.get(npcType);
		if (def != null && def.getAttack().getId() != -2) {
			return def.getAttack();
		}
		return null;
	}
	
	public static void populatePICombatAnims() {
		for (int i = 0; i < Config.MAX_NPC_ID; i++) {
			int melee = getCombatEmoteToMap(i, 0);
			int ranged = getCombatEmoteToMap(i, 1);
			int mage = getCombatEmoteToMap(i, 2);
			
			if (melee > 0 || ranged > 0 || mage > 0)
				combatMap.put(i, new NpcCombatAnim(melee, ranged, mage));
		}
	}
	
	public static int getCombatEmoteToMap(int npcType, int attackType) {
		
		switch (npcType) {
			
			case 3594: // rocnar
				return 3709;
			
			case 9929: //npcid
				if (attackType == CombatStatics.MELEE_HIT) {
					return 13775;
				} else if (attackType == CombatStatics.RANGE_HIT) {
					return 13770;
				} else if (attackType == CombatStatics.MAGIC_HIT) {
					return 13779;
				}
			case 7151: // demon gorillas
				if (attackType == CombatStatics.MELEE_HIT) {
					return Animation.getOsrsAnimId(7226);
				} else if (attackType == CombatStatics.RANGE_HIT) {
					return Animation.getOsrsAnimId(7227);
				} else if (attackType == CombatStatics.MAGIC_HIT) {
					return Animation.getOsrsAnimId(7225);
				}
				break;
			case 6090:
				if (attackType == 0) {
					return 12791;
				} else if (attackType == 2) {
					return 12790;
				}
				break;
			case 6089:
				if (attackType == 0) {
					return 12791;
				} else if (attackType == 2) {
					return 12790;
				}
				break;
			case 2745: // Jad
				if (attackType == 2) {
					return 9300;
				} else if (attackType == 1) {
					return 9276;
				} else if (attackType == 0) {
					return 9277;
				}
				break;
			case 50:// drags
				if (attackType == 0) {
					return 80;
				} else {
					return 81;
				}
			case 8133:// corp beast
				if (attackType == 2) {
					return 10053;
				} else if (attackType == 1) {
					return 10059;
				} else if (attackType == 0) {
					return 10057;
				}
				break;
			case 8349:// tormented demon
			case 8350:
			case 8351:// tormented demon
				if (attackType == 2) {
					return 10917;
				} else if (attackType == 1) {
					return 10918;
				} else if (attackType == 0) {
					return 10922;
				}
				break;
			case 1158:
			case 3835:// Kalphite Queen
				if (attackType == 0) {
					return 6241;
				} else {
					return 6240;
				}
			case 3495:// Kalphite Queen 2
				if (attackType == 0) {
					return 2075;
				} else {
					return 1979;
				}
			case 5666:// Barrelchest
				if (attackType == 0) {
					return 5894;
				} else {
					return 5895;
				}
			case 3340:// Giant Mole
				if (attackType == 7) {
					return 3311;
				} else if (attackType == 0) {
					return 3312;
				}
			case 13822: // jadinko male
			case 13821: // jadinko guard
				if (attackType == 0) {
					return 3214;
				} else {
					return 3215;
				}
				//case 8596: // avatar of destruction
				//	return 11197;
			
			case 5902: // the inadequacy
				return 6325; // 6318
			
			case 5996: // glod
				return 6501;
			
			case 13447: // nex
				return 6354;
			case 1382: // glacor
				if (attackType == 2) {
					return 9967;
				} else if (attackType == 1) {
					return 9968;
				} else if (attackType == 0) {
					return 9955;
				}
			case 8777: // chaos dwarf hand cannoner
				return 12141;
			case 1676:// Experiment
				return 1626;
			case 1677:// Experiment
				return 1616;
			case 1678:// Experiment
				return 1612;
			case 13473:// Revenant vampire
				if (attackType == 2) {
					return 7507;
				} else if (attackType == 1) {
					return 7520;
				} else if (attackType == 0) {
					return 7427;
				}
			case 13474:// Revenant werewolf
				if (attackType == 2) {
					return 7496;
				} else if (attackType == 1) {
					return 7521;
				} else if (attackType == 0) {
					return 7397;
				}
			case 13472:// Revenant hobGoblin
				if (attackType == 2) {
					return 7503;
				} else if (attackType == 1) {
					return 7516;
				} else if (attackType == 0) {
					return 7418;
				}
			case 13469:// Revenant Goblin
				if (attackType == 2) {
					return 7449;
				} else if (attackType == 1) {
					return 7513;
				} else if (attackType == 0) {
					return 7449;
				}
			case 13481:// Revenant Dragon need edit mage and range emotes
				if (attackType == 2) {
					return 14261;
				} else if (attackType == 1) {
					return 14261;
				} else if (attackType == 0) {
					return 14261;
				}
			case 13471:// Revenant pyrefiend
				if (attackType == 2) {
					return 7506;
				} else if (attackType == 1) {
					return 7519;
				} else if (attackType == 0) {
					return 7481;
				}
			case 13465:// Revenant Imp
				if (attackType == 2) {
					return 7500;
				} else if (attackType == 1) {
					return 7501;
				} else if (attackType == 0) {
					return 7407;
				}
			case 13475:// Revenant Cyclops
				if (attackType == 2) {
					return 7497;
				} else if (attackType == 1) {
					return 7510;
				} else if (attackType == 0) {
					return 7453;
				}
			case 13476:// Revenant Hellhound
				if (attackType == 2) {
					return 7515;
				} else if (attackType == 1) {
					return 7501;
				} else if (attackType == 0) {
					return 7460;
				}
			case 13477:// Revenant Demon
				if (attackType == 2) {
					return 7498;
				} else if (attackType == 1) {
					return 7512;
				} else if (attackType == 0) {
					return 7474;
				}
			case 13478:// Revenant Ork
				if (attackType == 2) {
					return 7505;
				} else if (attackType == 1) {
					return 7518;
				} else if (attackType == 0) {
					return 7411;
				}
			case 13479:// Revenant Dark Beast
				if (attackType == 2) {
					return 7515;
				} else if (attackType == 1) {
					return 7514;
				} else if (attackType == 0) {
					return 7467;
				}
			case 13480:// Revenant Knight
				if (attackType == 2) {
					return 7508;
				} else if (attackType == 1) {
					return 7522;
				} else if (attackType == 0) {
					return 7441;
				}
			case 1160:
			case 3836:
				if (attackType == 0) {
					return 6235;
				} else {
					return 6234;
				}
			case 4540:
				if (attackType == 0) {
					return 11252;
				} else if (attackType == 1) {
					return 11252;
				} else if (attackType == 2) {
					return 11245;
				}
			case 8528:
			case 8529:
				if (attackType == 0) {
					return 12696;
				} else if (attackType == 1) {
					return 12697;
				} else if (attackType == 2) {
					return 12697;
				} else if (attackType == -1) {
					return 12693;
				}
			case 6260:
				if (attackType == 0) {
					return 7060;
				} else if (attackType == 1) {
					return 7063;
				}
			case 10080:
				if (attackType == 0) {
					return 13001;
				} else if (attackType == 1) {
					return 13000;
				} else if (attackType == 2) {
					return 13000;
				}
			case 9771:
				if (attackType == 0) {
					return 13703;
				} else if (attackType == 1) {
					return 13704;
				}
			case 12743:
				if (attackType == 0) {
					return 15004;
				} else if (attackType == 1) {
					return 15007;
				} else if (attackType == 2) {
					return 15001;
				}
			case 12805:
				if (attackType == 0) {
					return 14963;
				} else if (attackType == 2) {
					return 14961;
				}
			case 5247:
				if (attackType == 0) {
					return 5411;
				} else if (attackType == 1) {
					return 5411;
				}
			case 53: // red dragon
			case 54:
			case 55:
			case 941:
			case 4677:
			case 4678:
			case 4679:
			case 4680:
			case 5362:
				if (attackType == 3) {
					return 12259;
				} else {
					return 12252;
				}
			case 5363: // mith dragon
				if (attackType == 3) {
					return 14252;
				} else if (attackType == 2) {
					return 14252;
				} else if (attackType == 0) {
					return 14247;
				}
			
			case 3590:
				if (attackType == 3) {
					return 81;
				} else if (attackType == 0) {
					if (Misc.random(1) == 0)
						return 80;
					else
						return 91;
				}
			
			case 1590:
			case 1591:
			case 1592:
				if (attackType == 3) {
					return 14252;
				} else if (attackType == 0) {
					return 14247;
				}
			case 51:
			case 10770:
			case 10771:
			case 10772:
			case 10773:
			case 10774:
			case 10775:
				if (attackType == 3) {
					return 13155;
				} else if (attackType == 0) {
					return 13151;
				}
		}
		
		return -1;
	}
	
	public static int getDeath(NPC n/* ,Client p */) {
		AnimationDefinition def = definitions.get(n.npcType);
		if (def != null) {
			return def.getDeath().getId();
		}
		return -1;
	}
	
	public static int getDefend(int npcType) {
		AnimationDefinition def = definitions.get(npcType);
		if (def != null) {
			return def.getDefend().getId();
		}
		return -1;
	}
	
	public static int getDefend(NPC n/* ,Client p */) {
		AnimationDefinition def = definitions.get(n.npcType);
		if (def != null) {
			return def.getDefend().getId();
		} else {
			Boss boss = n.setOrGetBoss();
			
			if (boss != null && boss.getBlockAnimation() != -1) {
				return boss.getBlockAnimation();
			}
		}
		return -1;
	}
	
	public static void initialize() {
		try {
			File file = new File("./Data/cfg/NPCAnimations.tsv");
			try (Scanner scanner = new Scanner(file)) {
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					if (line.startsWith("//")) {
						continue;
					}
					
					StringTokenizer token = new StringTokenizer(line, "\t");
					int id = Integer.parseInt(token.nextToken());
					int death = Integer.parseInt(token.nextToken());
					int defend = Integer.parseInt(token.nextToken());
					int attack = Integer.parseInt(token.nextToken());
					define(id, death, defend, attack);
				}
			}
		} catch (FileNotFoundException e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(NPCAnimations.class, e));
		}
		
		populatePICombatAnims();
	}
	
	public static AnimationDefinition define(int npcID, int death, int defend, int attack) {
		AnimationDefinition animationDefinition = new AnimationDefinition(death, defend, attack);
		definitions.put(npcID, animationDefinition);
		return animationDefinition;
	}
	
	private NPCAnimations() {
		throw new UnsupportedOperationException();
	}
	
}
