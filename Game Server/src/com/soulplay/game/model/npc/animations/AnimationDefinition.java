package com.soulplay.game.model.npc.animations;

import com.soulplay.game.world.entity.Animation;

public final class AnimationDefinition {
	
	private final Animation death, defend, attack;
	
	public AnimationDefinition(Animation death, Animation defend, Animation attack) {
		this.death = death;
		this.defend = defend;
		this.attack = attack;
	}
	
	public AnimationDefinition(int death, int defend, int attack) {
		this(new Animation(death), new Animation(defend), new Animation(attack));
	}
	
	public Animation getDeath() {
		return death;
	}
	
	public Animation getDefend() {
		return defend;
	}
	
	public Animation getAttack() {
		return attack;
	}
	
	@Override
	public String toString() {
		return death + ":" + defend + ":" + attack;
	}
	
}
