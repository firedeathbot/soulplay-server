package com.soulplay.game.model.npc.animations;

import com.soulplay.game.world.entity.Animation;

public final class NpcCombatAnim {
	
	private final Animation meleeAnim;
	private final Animation rangedAnim;
	private final Animation magicAnim;
	
	public NpcCombatAnim(int melee, int ranged, int magic) {
		this.meleeAnim = melee > 0 ? new Animation(melee) : null;
		this.rangedAnim = ranged > 0 ? new Animation(ranged) : null;
		this.magicAnim = magic > 0 ? new Animation(magic) : null;
	}
	
	public Animation getMeleeAnim() {
		return meleeAnim;
	}
	
	public Animation getRangedAnim() {
		return rangedAnim;
	}
	
	public Animation getMagicAnim() {
		return magicAnim;
	}
	
	public Animation getAnimationByAttackType(int attackType) {
		switch (attackType) {
			case 0:
				return meleeAnim;
			case 1:
				return rangedAnim;
			case 2:
				return magicAnim;
			default:
				return null;
		}
	}
	
}
