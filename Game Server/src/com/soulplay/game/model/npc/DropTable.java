package com.soulplay.game.model.npc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.skills.prayer.Prayer;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.summoning.Creation;
import com.soulplay.game.event.Task;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GroundItemObject;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;
import com.soulplay.util.RecentIDList;

public final class DropTable {
	
	private static final int SCALE_MIN_HP = 80;
	private static final int SCALE_MAX_HP = 1000;
	
	private static final int SCALE_MAX_RATE = 128;
	private static final int SCALE_MIN_RATE = 16;

	public static ArrayList<GroundItemObject> itemsToDrop = new ArrayList<>();

	public static ArrayList<GroundItemObject> itemsToDropTempRemove = new ArrayList<>();

	private static DropTableEntry DROP_TABLE_ENTRIES[] = new DropTableEntry[Config.MAX_NPC_ID];

	private static final ArrayList<DropTableItem> GEM_DROP_TABLE = new ArrayList<>();

	private static final ArrayList<DropTableItem> RARE_DROP_TABLE = new ArrayList<>();

	private static final ArrayList<DropTableItem> SUPER_DROP_TABLE = new ArrayList<>();

	private static final Set<Integer> EXCEPTIONS = new HashSet<>();

	private static final RecentIDList recentIDList = new RecentIDList(250, 50);

	private static final ArrayList<ItemDefinition> ALL_DROPABLE_ITEMS = new ArrayList<>();

	protected static int lineNumber = 0;

	public static boolean coinShareLoot(DropTableItem dti, Client c, int x,
			int y, int z) {

		ItemDefinition itemDef = ItemDefinition.forId(dti.getItemId());
		if (itemDef == null) {
			c.sendMessage(
					"Error itemDef is null for itemId " + dti.getItemId());
			System.out.println(
					"Error on itemDef for rare drop itemId " + dti.getItemId());
			c.sendMessage("Please report to staff.");
			return false;
		}

		int clanMembers[] = new int[c.getClan().activeMembers.size()];
		int count = 0;

		// for (Player p : PlayerHandler.players) {
		for (Player p : Server.getRegionManager().getLocalPlayers(c)) {
			if (p == null) {
				continue;
			}
			if (p.isIronMan()) {
				continue;
			}
			if (!PlayerConstants.goodDistance(p.getX(), p.getY(), c.getX(),
					c.getY(), 18)) {
				continue;
			}
			if (c.getClan().activeMembers.contains(p.getName())) {

				for (int j = 0; j < clanMembers.length; j++) {
					if (clanMembers[j] == 0) {
						clanMembers[j] = p.getId();
						count++;
						break;
					}
				}
			}
		}

		if (count > 1) {
			for (int clanMember2 : clanMembers) {
				if (clanMember2 != 0) {
					Client clanMember = (Client) PlayerHandler.players[clanMember2];
					if (clanMember == null) {
						continue;
					}
					clanMember.sendMessage("The rare item " + itemDef.getName()
							+ " has been split into Gold between the clan members.");
					itemsToDrop.add(new GroundItemObject(clanMember.getName(),
							995, x, y, z,
							Math.round(itemDef.getShopValue() / count),
							clanMember.isIronMan()));
					// ItemHandler.createGroundItem(clanMember,
					// clanMember.getName(), 995, x, y, z, (int)
					// Math.round(itemDef.getValue() / count), true,
					// clanMember.isIronMan());
				}
			}
		}

		return true;
	}

	public static ArrayList<ItemDefinition> getAllDropableItems() {
		return ALL_DROPABLE_ITEMS;
	}

	public static DropTableItem[] getDrop(int npcId, Client c,
			boolean command) {
		ArrayList<DropTableItem> drops = new ArrayList<>();
		ArrayList<DropTableItem> drops2 = new ArrayList<>();
		ArrayList<DropTableItem> alwaysDrops = new ArrayList<>();
		DropTableItem[] returnDrops;

		DropTableEntry thisEntry = getEntry(npcId);
		if (thisEntry == null) {
			return null;
		}
		double thisRoll = Misc.random(1000000000);
		// TODO Needs MASSIVe overhaul
		for (RarityGroup rarityGroup : thisEntry.getRarityGroups()) {
			if (rarityGroup == null) {
				continue;
			}
			if (rarityGroup.getRawProbability() == 1) { // Do NOT change this
														// like you did last
														// time.
				for (DropTableItem item : rarityGroup.getItems()) {
					alwaysDrops.add(item);
				}
				continue;
			}
			if (thisRoll <= (rarityGroup.getProbability(c)
					* 1000000000)) {
				if (drops.size() > 0) {
					if (drops.get(0).getProbability(c) > rarityGroup
							.getProbability(c)) {
						drops.clear();
						for (DropTableItem item : rarityGroup.getItems()) {
							drops.add(item);
						}
					} else if (drops.get(0)
							.getProbability(c) == rarityGroup
									.getProbability(c)) { // This
																		// should
																		// NEVER
																		// happen,
																		// but
																		// just
																		// in
																		// case.
						System.out.println(
								"Adding items of same rarity to drop. Shouldn't be possible. Check. (DropTable.java)");
						for (DropTableItem item : rarityGroup.getItems()) {
							drops.add(item);
						}
					}
				} else {
					for (DropTableItem item : rarityGroup.getItems()) {
						drops.add(item);
					}
				}
			}
		}
		thisRoll = Misc.random(1000000000);
		for (RarityGroup rarityGroup : thisEntry.getRarityGroups()) {
			if (rarityGroup == null) {
				continue;
			}
			if (rarityGroup.getRawProbability() == 1) { // Do NOT change this
														// like you did last
														// time.
				continue; // No need, we already have the "Always" drops.
			}
			if (thisRoll <= (rarityGroup.getProbability(c)
					* 1000000000)) {
				if (drops2.size() > 0) {
					if (drops2.get(0).getProbability(c) > rarityGroup
							.getProbability(c)) {
						drops2.clear();
						for (DropTableItem item : rarityGroup.getItems()) {
							drops2.add(item);
							// The below condition has evaluated to true (at
							// goblins, confirmed). That should NOT be possible,
							// as
							// that
							// would mean at least two groups with the same
							// probability. Not a HUGE problem, just a waste of
							// memory.
							// Note: It only appears to have been two similar
							// groups, no larger amounts of groups yet.
						}
					} else if (drops2.get(0)
							.getProbability(c) == rarityGroup
									.getProbability(c)) { // This
																		// shouldn't
																		// NEVER
																		// happen,
																		// but
																		// just
																		// in
																		// case.
						// System.out.println("Adding items of same rarity to
						// drop. Shouldn't be possible. Check.
						// (DropTable.java)");
						for (DropTableItem item : rarityGroup.getItems()) {
							drops2.add(item);
						}
					}
				} else {
					for (DropTableItem item : rarityGroup.getItems()) {
						drops2.add(item);
					}
				}
			}
		}
		returnDrops = new DropTableItem[alwaysDrops.size() + 1];
		for (int i = 0; i < alwaysDrops.size(); i++) {
			returnDrops[i] = alwaysDrops.get(i);
		}
		if (drops.size() > 0) {
			DropTableItem mainDrop = drops.get(Misc.random(drops.size() - 1));
			if (drops2.size() > 0 && (c.getItems().playerHasEquipped(2572)
					|| c.getItems().playerHasEquipped(773))) {
				DropTableItem mainDrop2 = drops2
						.get(Misc.random(drops2.size() - 1));
				boolean usedROW = mainDrop.getProbability(
						c) > mainDrop2.getProbability(c)
								? true
								: false;
				mainDrop = usedROW ? mainDrop2 : mainDrop;
				if (usedROW && !command) {
					// If SUPER_RARE drop or greater, and 1:10 rolled true
					if (Misc.random(10) == 7
							&& (mainDrop.getProbability(c) <= (1.0
									/ DropRate.SUPER_RARE.getProbability()))
							&& c.getItems().playerHasEquipped(2572)) {
						c.sendMessage(
								"<col=ff0000>Your Ring of Wealth has decayed while giving you a super rare drop.");
						c.getItems().deleteEquipmentItem(2572);
						// If RARE drop or greater, and 1:100 rolled true
					} else if (Misc.random(100) == 7
							&& (mainDrop.getProbability(c) <= (1.0
									/ DropRate.RARE.getProbability()))
							&& c.getItems().playerHasEquipped(2572)) {
						c.sendMessage(
								"<col=ff0000>Your Ring of Wealth has decayed while giving you a rare drop.");
						c.getItems().deleteEquipmentItem(2572);
					} else if (mainDrop.getProbability(c) <= (1.0
							/ DropRate.RARE.getProbability())) {
						c.sendMessage(
								"<col=00330066>Your Ring of Wealth is glowing!</col>");
					}
				}
			}
			returnDrops[returnDrops.length - 1] = mainDrop;
		} else {
			if (drops2.size() > 0 && (c.getItems().playerHasEquipped(2572)
					|| c.getItems().playerHasEquipped(773))) {
				DropTableItem mainDrop2 = drops2
						.get(Misc.random(drops2.size() - 1));
				if (!command) {
					// If SUPER_RARE drop or greater, and 1:10 rolled true
					if (Misc.random(10) == 7
							&& (mainDrop2.getProbability(c) <= (1.0
									/ DropRate.SUPER_RARE.getProbability()))
							&& c.getItems().playerHasEquipped(2572)) {
						c.sendMessage(
								"<col=ff0000>Your Ring of Wealth has decayed while giving you a super rare drop.");
						c.getItems().deleteEquipmentItem(2572);
						// If RARE drop or greater, and 1:100 rolled true
					} else if (Misc.random(100) == 7
							&& (mainDrop2.getProbability(c) <= (1.0
									/ DropRate.RARE.getProbability()))
							&& c.getItems().playerHasEquipped(2572)) {
						c.sendMessage(
								"<col=ff0000>Your Ring of Wealth has decayed while giving you a rare drop.");
						c.getItems().deleteEquipmentItem(2572);
					} else if (mainDrop2.getProbability(c) <= (1.0
							/ DropRate.RARE.getProbability())) {
						c.sendMessage(
								"<col=00330066>Your Ring of Wealth is glowing!</col>");
					}
				}
				returnDrops[returnDrops.length - 1] = mainDrop2;
			}
		}
		return returnDrops;
	}

	// private static final ArrayList<DropTableEntry> DROP_TABLE_ENTRIES = new
	// ArrayList<DropTableEntry>();

	public static DropTableEntry getEntry(int npcId) {
		// int returnId = -1;
		// returnId = recentIDList.getIndex(npcId);
		// if (returnId == -1) {
		// for (int i = 0; i < DROP_TABLE_ENTRIES.size(); i++) {
		// if (DROP_TABLE_ENTRIES.get(i).getNpcID() == npcId) {
		// recentIDList.addToIDList(npcId, i);
		// return DROP_TABLE_ENTRIES.get(i);
		// }
		// }
		// } else {
		// return DROP_TABLE_ENTRIES.get(returnId);
		// }
		// return null;
		if (npcId < 0 || npcId >= DROP_TABLE_ENTRIES.length) {
			return null;
		}

		return DROP_TABLE_ENTRIES[npcId];
	}

	// VERY IMPORTANT!!! Just for testing RDT rates. Otherwise, ignore.
	public static int getRdt(int npcId, Client c) {
		ArrayList<DropTableItem> rdtDrops = new ArrayList<>();
		boolean rdt = Misc.random(20) == 5;
		DropTableEntry thisEntry = getEntry(npcId);
		double rdtRoll;
		if (thisEntry == null) {
			return 0;
		}
		if ((c.getItems().playerHasEquipped(2572)
				|| c.getItems().playerHasEquipped(773))
				&& (rdt || Misc.random(15) == 5)) { // HAS
													// ROW
			rdtRoll = Misc.random(100);
			for (int i = 0; i < RARE_DROP_TABLE.size(); i++) {
				if (RARE_DROP_TABLE.get(i) == null) {
					System.out.println("ROW RDT Null");
					continue;
				}
				if (rdtRoll <= (RARE_DROP_TABLE.get(i)
						.getProbability(c) * 100)) {
					if (rdtDrops.size() > 0) {
						if (rdtDrops.get(0)
								.getProbability(c) > RARE_DROP_TABLE
										.get(i).getProbability(c)) {
							rdtDrops.clear();
							rdtDrops.add(RARE_DROP_TABLE.get(i));
							continue;
						} else if (rdtDrops.get(0)
								.getProbability(c) == RARE_DROP_TABLE
										.get(i).getProbability(c)) {
							rdtDrops.add(RARE_DROP_TABLE.get(i));
							continue;
						}
					}
					rdtDrops.add(RARE_DROP_TABLE.get(i));
				}
			}
			if (rdtDrops.size() > 0) {
				DropTableItem rdtDrop = rdtDrops
						.get(Misc.random(rdtDrops.size() - 1));
				if (rdtDrop.getItemId() == 65000) { // Next table
					rdtDrops.clear();
					rdtRoll = Misc.random(100); // Less precision == More likely
												// to favor.
					for (int i = 0; i < SUPER_DROP_TABLE.size(); i++) {
						if (SUPER_DROP_TABLE.get(i) == null) {
							continue;
						}
						if (rdtRoll <= (SUPER_DROP_TABLE.get(i)
								.getProbability(c) * 100)) {
							if (rdtDrops.size() > 0) {
								if (rdtDrops.get(0).getProbability(
										c) > SUPER_DROP_TABLE.get(i)
												.getProbability(c)) {
									rdtDrops.clear();
									rdtDrops.add(SUPER_DROP_TABLE.get(i));
									continue;
								} else if (rdtDrops.get(0).getProbability(
										c) == SUPER_DROP_TABLE.get(i)
												.getProbability(c)) {
									rdtDrops.add(SUPER_DROP_TABLE.get(i));
									continue;
								}
							}
							rdtDrops.add(SUPER_DROP_TABLE.get(i));
						}
					}
					if (rdtDrops.size() > 0) {
						return 3;
					}
				} else {
					return 2;
				}
			}
		} else if (thisEntry.isRdtNPC() && rdt) {
			if (NPCHandler.getNpcListHP(npcId) >= 150) { // Start on RDT
				rdtRoll = Misc.random(1000000000);
				for (int i = 0; i < RARE_DROP_TABLE.size(); i++) {
					if (RARE_DROP_TABLE.get(i) == null) {
						continue;
					}
					if (rdtRoll <= (RARE_DROP_TABLE.get(i)
							.getProbability(c) * 1000000000)) {
						rdtDrops.add(RARE_DROP_TABLE.get(i));
					}
				}
				if (rdtDrops.size() > 0) {
					DropTableItem rdtDrop = rdtDrops
							.get(Misc.random(rdtDrops.size() - 1));
					if (rdtDrop.getItemId() == 65000) { // Next table
						rdtDrops.clear();
						rdtRoll = Misc.random(1000000000); // Less precision ==
															// More likely to
															// favor.
						for (int i = 0; i < SUPER_DROP_TABLE.size(); i++) {
							if (SUPER_DROP_TABLE.get(i) == null) {
								continue;
							}
							if (rdtRoll <= (SUPER_DROP_TABLE.get(i)
									.getProbability(c)
									* 1000000000)) {
								rdtDrops.add(SUPER_DROP_TABLE.get(i));
							}
						}
						if (rdtDrops.size() > 0) {
							return 3;
						}
					} else {
						return 2;
					}
				}
			} else {
				rdtRoll = Misc.random(1000000000);
				for (int i = 0; i < GEM_DROP_TABLE.size(); i++) {
					if (GEM_DROP_TABLE.get(i) == null) {
						continue;
					}
					if (rdtRoll <= (GEM_DROP_TABLE.get(i)
							.getProbability(c) * 1000000000)) {
						rdtDrops.add(GEM_DROP_TABLE.get(i));
					}
				}
				if (rdtDrops.size() > 0) {
					DropTableItem rdtDrop = rdtDrops
							.get(Misc.random(rdtDrops.size() - 1));
					if (rdtDrop.getItemId() == 65000) {
						rdtDrops.clear();
						rdtRoll = Misc.random(1000000000);
						for (int i = 0; i < RARE_DROP_TABLE.size(); i++) {
							if (RARE_DROP_TABLE.get(i) == null) {
								continue;
							}
							if (rdtRoll <= (RARE_DROP_TABLE.get(i)
									.getProbability(c)
									* 1000000000)) {
								rdtDrops.add(RARE_DROP_TABLE.get(i));
							}
						}
						if (rdtDrops.size() > 0) {
							rdtDrop = rdtDrops
									.get(Misc.random(rdtDrops.size() - 1));
							if (rdtDrop.getItemId() == 65000) {
								rdtDrops.clear();
								rdtRoll = Misc.random(1000000000);
								for (int i = 0; i < SUPER_DROP_TABLE
										.size(); i++) {
									if (SUPER_DROP_TABLE.get(i) == null) {
										continue;
									}
									if (rdtRoll <= (SUPER_DROP_TABLE.get(i)
											.getProbability(c)
											* 1000000000)) {
										rdtDrops.add(SUPER_DROP_TABLE.get(i));
									}
								}
								if (rdtDrops.size() > 0) {
									return 3;
								}
							} else {
								return 2;
							}
						}
					} else {
						return 1;
					}
				}
			}
		}
		return 0;
	}
	
	private static boolean hasRolledRareDropTable(NPC npc) {
		
		final int combatLevel = npc.getEntityDef().getCombatLevel();
		
		if (combatLevel < 100)
			return Misc.random(128) == 7;
		else if (combatLevel < 200)
			return Misc.random(64) == 7;
		else if (combatLevel < 300) {
			return Misc.random(32) == 7;
		}
		//else monster is over lvl 300
		return Misc.random(16) == 7;
		
	}
	
	private static boolean hasRolledRareDropTableScaleHP(NPC npc) {
		
		final int result = Misc.interpolateSafe(SCALE_MAX_RATE, SCALE_MIN_RATE, SCALE_MIN_HP, SCALE_MAX_HP, npc.getSkills().getMaximumLifepoints());
		
		return Misc.newRandom(result) == 0;
	}

	public static void handleDrops(Client c, NPC npc) {
		if (c.playerIsInHouse) { // poh guards do not drop anything
			return;
		}
		
		final int npcId = npc.npcType;
		final int x;
		final int y;
		final int z;
		
		if (!RegionClip.canStandOnSpot(npc.getX(), npc.getY(), npc.getZ(), npc.getDynamicRegionClip())) {
			x = c.getX();
			y = c.getY();
			z = c.getZ();
		} else {
			x = npc.getX();
			y = npc.getY();
			z = npc.getZ();
		}
		
		DropTableEntry thisEntry = getEntry(npcId);
		if (c.getName().equalsIgnoreCase("vavbro")
				|| c.getName().equalsIgnoreCase("zaros")) {
			c.sendMessage("Drops for " + NPCHandler.getNpcName(npcId) + " : "
					+ npcId);
		}
		if (thisEntry == null && !isException(npcId)) {
			//System.err.println("No drop entry for NPC: " + npcId);
			if (c.getName().equalsIgnoreCase("vavbro")
					|| c.getName().equalsIgnoreCase("zaros")) {
				c.sendMessage("No drops for NPC.");
			}
			return;
		} else if (isException(npcId)) {
			return;
		}
		double rdtRoll;
		boolean rdt = hasRolledRareDropTableScaleHP(npc); // Has rolled RDT
		boolean ringOfWealthActivated = false;
		if (!rdt && Slayer.hasTaskAssigned(c, npcId)) { // Killing slayer tasks will be the same thing as wearing RoW
			rdt = hasRolledRareDropTableScaleHP(npc);
		} else if (!rdt && c.getItems().playerHasEquipped(2572)) { // reroll if ring of wealth is equipped
			rdt = ringOfWealthActivated = hasRolledRareDropTableScaleHP(npc);
		}
		if(!rdt && c.revenantDungeon()) { // revenant dungeon RDT rates increased by 2x rate(3x with ROW)
			rdt = hasRolledRareDropTableScaleHP(npc);
			if (!rdt && c.isSkulled && (npc.npcType >= 13465 && npc.npcType <= 13481)) {
				rdt = hasRolledRareDropTableScaleHP(npc);
			}
				
		}
		DropTableItem[] drops = getDrop(npcId, c, false);
		for (DropTableItem item : drops) {
			if (item != null) {
				if (Prayer.handleBoneCrusher(c, item.getItemId())) {
					continue;
				}

				if (item.getItemId() == Creation.GOLD_CHARM
						&& c.getItems().playerHasItem(9952)
						&& (c.getItems().freeSlots() > 0 || c.getItems().playerHasItem(Creation.GOLD_CHARM))) { // Has charming
																// imp
					c.sendMessageSpam(
							"The charming imp finds a [gold] charm and puts it in your inventory.");
					c.getItems().addItem(Creation.GOLD_CHARM, 1);
					continue;
				}
				if (item.getItemId() == Creation.GREEN_CHARM
						&& c.getItems().playerHasItem(9952)
						&& (c.getItems().freeSlots() > 0 || c.getItems().playerHasItem(Creation.GREEN_CHARM))) { // Has charming
																// imp
					c.sendMessageSpam(
							"The charming imp finds a [green] charm and puts it in your inventory.");
					c.getItems().addItem(Creation.GREEN_CHARM, 1);
					continue;
				}
				if (item.getItemId() == Creation.CRIMSON_CHARM
						&& c.getItems().playerHasItem(9952)
						&& (c.getItems().freeSlots() > 0 || c.getItems().playerHasItem(Creation.CRIMSON_CHARM))) { // Has charming
																// imp
					c.sendMessageSpam(
							"The charming imp finds a [crimson] charm and puts it in your inventory.");
					c.getItems().addItem(Creation.CRIMSON_CHARM, 1);
					continue;
				}
				if (item.getItemId() == Creation.BLUE_CHARM
						&& c.getItems().playerHasItem(9952)
						&& (c.getItems().freeSlots() > 0 || c.getItems().playerHasItem(Creation.BLUE_CHARM))) {// Has charming
																// imp
					c.sendMessageSpam(
							"The charming imp finds a [blue] charm and puts it in your inventory.");
					c.getItems().addItem(Creation.BLUE_CHARM, 1);
					continue;
				}
				
				//drop pets from drop table
				Pet pet = Pet.getPetForItem(item.getItemId());
				if (pet != null) {
					Pet.dropPetFromDropTable(c, pet);
					continue;
				}
				
				// this is where the actual item is being dropped

				ItemDefinition def = ItemDefinition.forId(item.getItemId());

				if (c.getClan() != null && c.getClan().clanShare()
						&& def != null && def.getShopValue() > 1000000) {
					coinShareLoot(item, c, x, y, z);
				} else {
					if (c.getClan() != null) {
						// ItemDefinition def =
						// ItemDefinition.forId(item.getItemId());
						if (def != null && def.getShopValue() > 1000000) {
							// c.getClan().sendClanMessage("<col=00330066>"+c.getName()+"
							// has received:
							// </col><col=669900>"+ItemDefinition.forId(item.getItemId()).getName());
							c.getClan()
									.sendAlertToLS("<col=00330066>"
											+ c.getNameSmartUp()
											+ " has received: </col><col=669900>"
											+ def.getName());
						}
					}
					DropTable.itemsToDrop.add(new GroundItemObject(c.getName(),
							item.getItemId(), x, y, z, item.getAmount(),
							c.isIronMan()));
					// ItemHandler.createGroundItem(c, c.getName(),
					// item.getItemId(), x, y, z, item.getAmount(), true,
					// c.isIronMan());
				}

				if (item.getItemId() == 11235) {
					c.getAchievement().findDarkBow();
				}
				if (item.getItemId() == 3140) {
					c.getAchievement().findDragonChain();
				}
				if (item.getItemId() == 15259) {
					c.getAchievement().findDragonPickAxe();
				}
			}
		}

		if (thisEntry.isRdtNPC() && rdt) { // Start on RDT
			
			enterRdt(c, ringOfWealthActivated, x, y, z);
			
		} else if (rdt) { // if not part of the rare drop table then drop casual items like gems
			ArrayList<DropTableItem> gemRDTList = new ArrayList<>();
			rdtRoll = Misc.random(1000000000);
			for (int i = 0; i < GEM_DROP_TABLE.size(); i++) {
				if (GEM_DROP_TABLE.get(i) == null) {
					continue;
				}
				if (rdtRoll <= (GEM_DROP_TABLE.get(i)
						.getProbability(c) * 1000000000)) {
					gemRDTList.add(GEM_DROP_TABLE.get(i));
				}
			}
			
			if (gemRDTList.size() > 0) {
				DropTableItem rdtDrop = gemRDTList
						.get(Misc.random(gemRDTList.size() - 1));

				if (rdtDrop.getItemId() != 65000) { // I'm guessing 65000 is rare chance to hit rare drop table from here?

					if (ringOfWealthActivated) {
						c.sendMessage(
								"<col=00330066>Your Ring of Wealth is glowing!</col>");
					}

					if (c.getClan() != null
							&& c.getClan().clanShare()) {

						coinShareLoot(rdtDrop, c, x, y, z);

					} else {
						itemsToDrop.add(new GroundItemObject(
								c.getName(), rdtDrop.getItemId(), x, y,
								z, rdtDrop.getAmount(),
								c.isIronMan()));
						// ItemHandler.createGroundItem(c, c.getName(),
						// rdtDrop.getItemId(), x, y, z,
						// rdtDrop.getAmount(), true, c.difficulty ==
						// 4);
					}
					gemRDTList.clear();
				} else {
					//add rare drop table roll here
					enterRdt(c, ringOfWealthActivated, x, y, z);
				}
			}
		}
		
	}
	
	public static void enterRdt(Client c, boolean ringOfWealthActivated, int x, int y, int z) {
		ArrayList<DropTableItem> rdtDrops = new ArrayList<>();
		double rdtRoll = Misc.random(1000000000);
		for (int i = 0; i < RARE_DROP_TABLE.size(); i++) {
			if (RARE_DROP_TABLE.get(i) == null) {
				continue;
			}
			if (rdtRoll <= (RARE_DROP_TABLE.get(i)
					.getProbability(c) * 1000000000)) {
				rdtDrops.add(RARE_DROP_TABLE.get(i));
			}
		}
		if (rdtDrops.size() > 0) { //rdt exist
			DropTableItem rdtDrop = rdtDrops
					.get(Misc.random(rdtDrops.size() - 1));
			if (rdtDrop.getItemId() == 65000) { // Next table(SRDT) since roll is not RDT
				rdtDrops.clear();
				rdtRoll = Misc.random(1000000000); // Less precision ==
				// More likely to
				// favor.
				for (int i = 0; i < SUPER_DROP_TABLE.size(); i++) {
					if (SUPER_DROP_TABLE.get(i) == null) {
						continue;
					}
					if (rdtRoll <= (SUPER_DROP_TABLE.get(i)
							.getProbability(c)
							* 1000000000)) {
						rdtDrops.add(SUPER_DROP_TABLE.get(i));
					}
				}
				if (rdtDrops.size() > 0) {
					rdtDrop = rdtDrops
							.get(Misc.random(rdtDrops.size() - 1));

					if (ringOfWealthActivated) {
						if (Misc.random(1) == 0) { // 50/50
							// chance
							c.sendMessage(
									"<col=ff0000>Your Ring of Wealth has decayed while giving you a super rare drop.");
							c.getItems().deleteEquipmentItem(2572);
						} else {
							c.sendMessage(
									"<col=00330066>Your Ring of Wealth is glowing!</col>");
						}
					}

					if (c.getClan() != null
							&& c.getClan().clanShare()) {

						coinShareLoot(rdtDrop, c, x, y, z);

					} else {
						c.sendMessage(
								"<col=00330066>The monster has dropped something!</col>");
						itemsToDrop.add(new GroundItemObject(
								c.getName(), rdtDrop.getItemId(), x, y,
								z, rdtDrop.getAmount(),
								c.isIronMan()));
						// ItemHandler.createGroundItem(c, c.getName(),
						// rdtDrop.getItemId(), x, y, z,
						// rdtDrop.getAmount(), true, c.difficulty ==
						// 4);
					}
					rdtDrops.clear();
				}
			} else { // else drop regular rare drop

				if (ringOfWealthActivated) {
					if (/*rdtDrop.getRawProbability() > DropRate.UNCOMMON.getRawProbability() && */Misc.random(100) == 0) { // 50/50
						// chance
						c.sendMessage(
								"<col=ff0000>Your Ring of Wealth has decayed while giving you a rare drop.");
						c.getItems().deleteEquipmentItem(2572);
					} else {
						c.sendMessage(
								"<col=00330066>Your Ring of Wealth is glowing!</col>");
					}
				}

				itemsToDrop.add(new GroundItemObject(c.getName(),
						rdtDrop.getItemId(), x, y, z,
						rdtDrop.getAmount(), c.isIronMan()));
				// ItemHandler.createGroundItem(c, c.getName(),
				// rdtDrop.getItemId(), x, y, z, rdtDrop.getAmount(),
				// true, c.isIronMan());
			}
		}
	}

	public static void initDropMaker() {

		Server.getTaskScheduler().schedule(new Task() {

			@Override
			protected void execute() {

				if (DropTable.itemsToDrop.size() > 0) {

					for (int jj = 0; jj < DropTable.itemsToDrop.size(); jj++) {
						GroundItemObject item = DropTable.itemsToDrop.get(jj);
						if (item.getItemId() == 65000) { // just in case
							System.out.println("item 65000 was still input! careful!");
							continue;
						}
						if (item.getTicks() == 0) {

							ItemHandler.createGroundItem(item.getOwnerName(),
									item.getItemId(), item.getItemX(),
									item.getItemY(), item.getItemZ(),
									item.getItemAmount(), true,
									item.isIronmanDrop());
							DropTable.itemsToDropTempRemove.add(item);
						} else {
							item.setTicks(item.getTicks() - 1);
						}
					}

					for (int jj = 0; jj < DropTable.itemsToDropTempRemove
							.size(); jj++) {
						GroundItemObject item = DropTable.itemsToDropTempRemove
								.get(jj);
						DropTable.itemsToDrop.remove(item);
					}

					DropTable.itemsToDropTempRemove.clear();
				}

			}
		});

	}

	public static boolean isException(int npcId) {
		return EXCEPTIONS.contains(npcId);
	}

	public static void loadDropTables(Client c) {
		recentIDList.clear();
		lineNumber = 0;
		File dropTables = new File(
				"./Data/" + Config.cfgDir() + "/drop-table.cfg");
		if (!dropTables.exists()) {
			System.out.println("Cannot find drop tables");
			return;
		}
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(dropTables));
			String line;
			DropTableItem item;
			boolean foundEOF = false;
			boolean sameEntry = false;
			boolean comment = false;
			DropTableEntry entry = null;
			try {
				int state = 0;
				line = reader.readLine();
				while (line != null && !foundEOF) {
					lineNumber++;
					line = line.replace("	", " ");
					while (line.startsWith(" ")) {
						line = line.substring(line.indexOf(" "));
					}
					if (line.startsWith("/*")) {
						comment = true;
					}
					if (line.endsWith("*/")) {
						comment = false;
					}
					if (line.contains("//")) {
						line = line.substring(0, line.indexOf("//"));
					}
					if (line.contains("/*")) {
						line = line.substring(0, line.indexOf("/*"));
					}
					if (line.contains("*/")) {
						line = line.substring(line.indexOf("*/"),
								line.length() - 1);
					}
					if (!line.equals("") && !line.startsWith("//")
							&& !comment) {
						switch (line.toUpperCase()) {
							case "[DROP_TABLE]":
								state = 0;
								sameEntry = false;
								break;
							case "[GEM_DROP_TABLE]":
								sameEntry = false;
								state = 1;
								break;
							case "[RARE_DROP_TABLE]":
								state = 2;
								sameEntry = false;
								break;
							case "[SUPER_DROP_TABLE]":
								state = 3;
								sameEntry = false;
								break;
							case "[REVENANTS]":
								state = 4;
								sameEntry = false;
								break;
							case "[EXCEPTIONS]":
								state = 5;
								sameEntry = false;
							case "":
								break;
							case "[EOF]":
								foundEOF = true;
								sameEntry = false;
								break;
							default:
								if (!line.startsWith("//")) {
									switch (state) {
										case 0:
											if (line.startsWith("drop = ")) {
												if (sameEntry) {
													if (entry != null) {
														// DROP_TABLE_ENTRIES.add(entry);
														DROP_TABLE_ENTRIES[entry
																.getNpcID()] = entry;
													}
												}
												sameEntry = false;
												line = line.replace("drop = ",
														"");
											}
											if (!sameEntry) {
												entry = DropTableEntry
														.parseDropsFromString(
																line.replace(
																		",", "")
																		.replace(
																				"\"",
																				"")
																		.replace(
																				" ,",
																				","),
																c);
												if (entry != null && !line
														.replace(" ", "")
														.endsWith(",")) {
													// DROP_TABLE_ENTRIES.add(entry);
													DROP_TABLE_ENTRIES[entry
															.getNpcID()] = entry;
													break;
												}
											}
											if (line.replace(" ", "")
													.endsWith(",")) {
												if (sameEntry
														&& entry != null) {
													entry.addDrop(DropTableItem
															.parseDropTableItem(
																	line.replace(
																			",",
																			"")
																			.replace(
																					"\"",
																					"")
																			.replace(
																					" ,",
																					",")));
												}
												sameEntry = true;
											} else {
												if (sameEntry) {
													if (entry != null) {
														entry.addDrop(
																DropTableItem
																		.parseDropTableItem(
																				line.replace(
																						",",
																						"")
																						.replace(
																								"\"",
																								"")
																						.replace(
																								" ,",
																								",")));
														// DROP_TABLE_ENTRIES.add(entry);
														DROP_TABLE_ENTRIES[entry
																.getNpcID()] = entry;
													}
												}
												sameEntry = false;
											}
											break;
										case 1:
											item = DropTableItem
													.parseDropTableItem(line
															.replace(",", "")
															.replace("\"", "")
															.replace(" ,",
																	","));
											if (item != null) {
												GEM_DROP_TABLE.add(item);
											}
											break;
										case 2:
											item = DropTableItem
													.parseDropTableItem(line
															.replace(",", "")
															.replace("\"", "")
															.replace(" ,",
																	","));
											if (item != null) {
												RARE_DROP_TABLE.add(item);
											}
											break;
										case 3:
											item = DropTableItem
													.parseDropTableItem(line
															.replace(",", "")
															.replace("\"", "")
															.replace(" ,",
																	","));
											if (item != null) {
												SUPER_DROP_TABLE.add(item);
											}
											break;
										case 4: //revenants
											item = DropTableItem
											.parseDropTableItem(line
													.replace(",", "")
													.replace("\"", "")
													.replace(" ,",
															","));
											int revLowestID = 13465;
											int revHighestId = 13481;
											if (item != null) {
												
												for (int i = 0, len = revHighestId-revLowestID+1; i < len; i++) {
													int revenantId = revLowestID + i;
													
													
													DropTableEntry revEntry = DROP_TABLE_ENTRIES[revenantId];
													
													if (revEntry == null) {
														revEntry = new DropTableEntry(revenantId, true);
														DROP_TABLE_ENTRIES[revenantId] = revEntry;
													}
													
													int probability = item.getRawProbability();
													
													long data = item.getData();
													
													if (probability > DropRate.ALWAYS.getRawProbability()) {
														int newProbability = (int) Math.floor(probability * scaleRarityForRevenants(revenantId));
														data = DropTableItem.setProbability(item.getData(), newProbability);
													}
													
													DropTableItem newItem = new DropTableItem(item.getItemId(), data);
													
													revEntry.addDrop(newItem);
													
												}
											}
											break;
										case 5:
											int npcId = -1;
											try {
												npcId = Integer.parseInt(line);
											} catch (NumberFormatException e) {
												npcId = NPCHandler
														.getNpcIdForCheckDrops(line);
											}
											if (npcId != -1) {
												EXCEPTIONS.add(npcId);
											}
											break;
									}
								}
						}
					}
					line = reader.readLine();
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (DropTableEntry entry : DROP_TABLE_ENTRIES) {
			// entry.sort();
			if (entry == null) {
				continue;
			}
			for (DropTableItem item : entry.getItems()) {
				ItemDefinition list = null;
				try {
					list = ItemDefinition.forId(item.getItemId());
				} catch (ArrayIndexOutOfBoundsException e) {
					System.err.println(
							"There's some item in the drop-items.cfg that probably has an ID way out of bounds: "
									+ item.getItemId());
					continue;
				}
				if (list == null)
					continue;
				if (!ALL_DROPABLE_ITEMS.contains(list)) {
					ALL_DROPABLE_ITEMS.add(list);
				}
			}
		}
		GEM_DROP_TABLE.add(new NextDropTable(DropRate.RARE));
		RARE_DROP_TABLE.add(new NextDropTable(DropRate.UNCOMMON));
		System.out.println("Finished loading drops from CFG.");
	}

	public static void reloadDropTables(Client c) {
		// DROP_TABLE_ENTRIES.clear();
		DROP_TABLE_ENTRIES = new DropTableEntry[Config.MAX_NPC_ID];
		GEM_DROP_TABLE.clear();
		RARE_DROP_TABLE.clear();
		SUPER_DROP_TABLE.clear();
		EXCEPTIONS.clear();
		loadDropTables(c);
	}
	
	private static double scaleRarityForRevenants(int npcID) {
		if (npcID >= 13465 && npcID <= 13481) {
			double npcHP = NPCHandler.getNpcListHP(npcID);
			double dragonHP = NPCHandler.getNpcListHP(13481);
			double rate = Math.floor(dragonHP/npcHP);
			
			return rate;
		}
		return 1.0;
	}

	// Consider making it not a method that sends messages to client, but
	// returns an array of npc ids and their item drop rate.
	// It would be more useful (although I'm not sure for what else)
	public static boolean whatDrops(Client c, int itemId) {
		boolean h = false;
		for (DropTableEntry entry : DROP_TABLE_ENTRIES) {
			if (entry != null && entry.dropsItem(itemId)) {
				h = true;
				if (NPCHandler.getNpcName(entry.getNpcID()).replace("_", " ")
						.equalsIgnoreCase("white knight") && itemId == 6617) {
					// System.out.println("Knight: "+entry.dropsItem(itemId));
				} else if (itemId != 6617 && NPCHandler
						.getNpcName(entry.getNpcID()).replace("_", " ")
						.equalsIgnoreCase("white knight")) {
//					System.out.println("No: " + itemId);

				}
				c.sendMessage(NPCHandler.getNpcName(entry.getNpcID())
						.replace("_", " ")
						+ " : "
						+ ((int) ((entry.getItemEntry(itemId)
								.getProbability(c)) * 1000000))
								/ 10000.0
						+ "%");
			}
		}
		return h;
	}

	private DropTable() {
		// To prevent instantiation.
	}
}
