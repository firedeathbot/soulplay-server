package com.soulplay.game.model.npc;

import java.util.ArrayList;

import com.soulplay.DBConfig;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Player;

public final class RarityGroup implements Comparable<RarityGroup> {

	public final int groupRarity; // Use RAW rarity to configure this.

	public final ArrayList<DropTableItem> items = new ArrayList<>();

	public RarityGroup(final int groupRarity) {
		this.groupRarity = groupRarity;
	}

	public RarityGroup addItem(DropTableItem item) {
		if (item.getRawProbability() != groupRarity) {
			throw new RuntimeException(
					"Item of different rarity than the group cannot be stored inside the group. (See RarityGroup.java)");
		}
		items.add(item);
		return this;
	}

	@Override
	public int compareTo(RarityGroup o) {
		return (int) (o.getProbability(null) - this.getProbability(null));
	}

	public ArrayList<DropTableItem> getItems() {
		return items;
	}

	public double getProbability(Player player) {
		double modifiers = 1.0;

		if (groupRarity >= DropRate.SUPER_RARE.getRawProbability()) {
			modifiers = DBConfig.SUPER_RARE_DROP_MODIFIER * RSConstants.getDropRatePerMode(player);
		} else if (groupRarity >= DropRate.RARE.getRawProbability()) {
			modifiers = DBConfig.RARE_DROP_MODIFIER * RSConstants.getDropRatePerMode(player);
		}

		double probability = 1.0 / groupRarity * modifiers;

		return probability > .5 && groupRarity != 1 ? 0.5D : probability; // To
																			// make
																			// sure
																			// that
																			// when
																			// you
																			// have,
																			// for
																			// example
		// 80 commons, it doesn't set the probability to 4000%.
		// This ensures that the max non-Always probability is 50%
	}

	public DropTableItem getRandomItem() {
		return items.get((int) (Math.random() * items.size()));
	}

	public double getRawProbability() {
		return groupRarity;
	}

	public int length() {
		return items.size();
	}
}
