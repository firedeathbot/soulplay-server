package com.soulplay.game.model.npc.spawns;

import java.io.File;

import com.google.gson.reflect.TypeToken;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.util.Misc;

public class NPCSpawnManager {

	public static void load() {
		loadRecursive("./Data/content/");
	}

	public static void loadRecursive(String path) {
		File directory = new File(path);
		File[] listFiles = directory.listFiles();
		if (listFiles == null) {
			return;
		}

		for (File file : listFiles) {
			if (file.isDirectory()) {
				loadRecursive(file.getAbsolutePath());
				continue;
			}

			if (!file.getName().endsWith(".spawn")) {
				continue;
			}

			try {
				String absPath = file.getAbsolutePath();
				String content = new String(Misc.readFile(absPath));
				NPCSpawnConfig[] newList = GsonSave.gsond.fromJson(content, new TypeToken<NPCSpawnConfig[]>() {}.getType());
				for (int i = 0, length = newList.length; i < length; i++) {
					NPCSpawnConfig entry = newList[i];
					if (entry == null) {
						System.out.println("Null entry? " + absPath);
						continue;
					}

					int x = entry.getX();
					int y = entry.getY();
					if (entry.isOsrs()) {
						x += 6400;
					}

					NPCHandler.newNPC(entry.getId(), x, y, entry.getZ(), entry.getWalkType());
				}
			} catch (Exception e) {
				System.err.println("File path: " + file.getAbsolutePath());
				e.printStackTrace();
			}
		}
	}

}
