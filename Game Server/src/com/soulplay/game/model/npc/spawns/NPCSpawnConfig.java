package com.soulplay.game.model.npc.spawns;

/**
 * @author Jire
 */
public final class NPCSpawnConfig {

	private final int id;
	private final int x, y, z;
	private final int walkType;
	private final boolean osrs;
	private String name;

	public NPCSpawnConfig(int id, int x, int y, int z, int walkType, boolean osrs) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.walkType = walkType;
		this.osrs = osrs;
	}

	public int getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public int getWalkType() {
		return walkType;
	}

	public boolean isOsrs() {
		return osrs;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
