package com.soulplay.game.model.npc;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.save.GsonSave;

public class NPCDefinitions {

	public static NPCDefinitions[] definitions;

	public static void init() {
		definitions = new NPCDefinitions[Config.MAX_NPC_ID + 1];
	}

	static {
		init();
	}

	/**
	 * @return the definitions
	 */
	public static NPCDefinitions[] getDefinitions() {
		return definitions;
	}

	private int npcId;

	private String npcName;

	private int npcCombat;

	private int npcHealth;
	
	private boolean aggressive;
	
	private boolean poisonous;
	
	private boolean venomous;
	
	private boolean poisonImmune;
	
	private boolean venomImmune;
	
	private CombatStats stats;
	
	private Set<NpcWeakness> weakness = new HashSet<>();
	
	private Skills skills;
	private int maxHit;

	public NPCDefinitions(int _npcId) {
		npcId = _npcId;
	}

	public NPCDefinitions(NPCDefinitions def) {
		definitions[def.getNpcId()] = def;
	}

	/**
	 * @return the npcCombat
	 */
	public int getNpcCombat() {
		return npcCombat;
	}

	/**
	 * @return the npcHealth
	 */
	public int getNpcHealth() {
		return npcHealth;
	}

	/**
	 * @return the npcId
	 */
	public int getNpcId() {
		return npcId;
	}

	/**
	 * @return the npcName
	 */
	public String getNpcName() {
		return npcName;
	}

	/**
	 * @param npcCombat
	 *            the npcCombat to set
	 */
	public void setNpcCombat(int npcCombat) {
		this.npcCombat = npcCombat;
	}

	/**
	 * @param npcHealth
	 *            the npcHealth to set
	 */
	public void setNpcHealth(int npcHealth) {
		this.npcHealth = npcHealth;
	}

	/**
	 * @param npcId
	 *            the npcId to set
	 */
	public void setNpcId(int npcId) {
		this.npcId = npcId;
	}

	/**
	 * @param npcName
	 *            the npcName to set
	 */
	public void setNpcName(String npcName) {
		this.npcName = npcName;
	}

	public CombatStats getStats() {
		return stats;
	}

	public void setStats(CombatStats stats) {
		this.stats = stats;
	}

	public Set<NpcWeakness> getWeakness() {
		return weakness;
	}

	public void setWeakness(Set<NpcWeakness> weakness) {
		this.weakness = weakness;
	}
	
	public void loadWeakness(String str) {
		this.weakness = GsonSave.gsond.fromJson(str,
				new TypeToken<HashSet<NpcWeakness>>() {
				}.getType());
	}

	public Skills getSkills() {
		return skills;
	}

	public void setSkills(Skills skills) {
		this.skills = skills;
	}

	public boolean isAggressive() {
		return aggressive;
	}

	public void setAggressive(boolean aggressive) {
		this.aggressive = aggressive;
	}

	public boolean isPoisonous() {
		return poisonous;
	}

	public void setPoisonous(boolean poisonous) {
		this.poisonous = poisonous;
	}

	public boolean isVenomous() {
		return venomous;
	}

	public void setVenomous(boolean venomous) {
		this.venomous = venomous;
	}

	public boolean isPoisonImmune() {
		return poisonImmune;
	}

	public void setPoisonImmune(boolean poisonImmune) {
		this.poisonImmune = poisonImmune;
	}

	public boolean isVenomImmune() {
		return venomImmune;
	}

	public void setVenomImmune(boolean venomImmune) {
		this.venomImmune = venomImmune;
	}

	public int getMaxHit() {
		return maxHit;
	}

	public void setMaxHit(int maxHit) {
		this.maxHit = maxHit;
	}

	@Override
	public String toString() {
		return "NPCDefinitions [npcId=" + npcId + ", npcName=" + npcName + ", npcCombat=" + npcCombat + ", npcHealth="
				+ npcHealth + ", aggressive=" + aggressive + ", poisonous=" + poisonous + ", venomous=" + venomous
				+ ", poisonImmune=" + poisonImmune + ", venomImmune=" + venomImmune + ", stats=" + stats + ", weakness="
				+ weakness + ", skills=" + skills + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NPCDefinitions other = (NPCDefinitions) obj;
		return aggressive == other.aggressive && npcCombat == other.npcCombat && npcHealth == other.npcHealth
				&& npcId == other.npcId && Objects.equals(npcName, other.npcName) && poisonImmune == other.poisonImmune
				&& poisonous == other.poisonous && Objects.equals(skills, other.skills)
				&& Objects.equals(stats, other.stats) && venomImmune == other.venomImmune && venomous == other.venomous
				&& Objects.equals(weakness, other.weakness);
	}

}
