package com.soulplay.game.model.npc;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.util.Stream;

public final class NpcUpdating {

	private static void addNewNPC(Player player, NPC npc, Stream str,
			Stream updateBlock) {

		int id = npc.npcIndex;
		player.npcInListBitmap[id >> 3] |= 1 << (id & 7);
		player.npcList[player.npcListSize++] = (short) id;

		str.writeBits(14, id);

		int deltaX = npc.absX - player.absX;
		int deltaY = npc.absY - player.absY;

		if (player.extendedNpcRendering) {
			str.writeBits(8, deltaY);
			str.writeBits(8, deltaX);
		} else {
			str.writeBits(5, deltaY);
			str.writeBits(5, deltaX);
		}

		str.writeBits(1, npc.didTeleport ? 1 : 0);
		str.writeBits(17, npc.npcType);

		if (npc.customName != null) {
			npc.getUpdateFlags().add(UpdateFlag.NAME_CHANGE);
		}
		if (npc.transformId > -1) { // alk update
			npc.getUpdateFlags().add(UpdateFlag.TRANSFORM);
		}

		if (npc.getLastFacing() > 0) {
			npc.setLastFace();
		} else if (npc.getLastFacingLocation() != null) {
			npc.faceLocation(npc.getLastFacingLocation(), false);
		}

		npc.getUpdateFlags().add(UpdateFlag.PET);
		appendNPCUpdateBlock(npc, updateBlock, player);
		str.writeBits(1, 1);
	}

	private static void appendAnimUpdate(NPC npc, Stream str) {
		Animation animation = npc.getAnimation();

		str.writeLEShort(animation.getId());
		str.writeByte(animation.getDelay());
	}

	private static void appendFaceEntity(NPC npc, Stream str) {
		str.writeShort(npc.getFacing());
	}

	private static void appendForcedText(NPC npc, Stream str) {
		str.writeString(npc.getForcedChat());
	}

	private static void appendHitUpdate(NPC npc, Stream str) {
		str.writeByte(npc.hitDiff);
		str.writeByte(npc.hitMask);
		str.writeByte(npc.hitIcon);
		str.writeShort(npc.getSkills().getLifepoints());
		str.writeShort(npc.getSkills().getMaximumLifepoints());
	}

	private static void appendHitUpdate2(NPC npc, Stream str) {
		str.writeByte(npc.hitDiff2);
		str.writeByte(npc.hitMask2);
		str.writeByte(npc.hitIcon2);
	}

	private static void appendMask80Update(NPC npc, Stream str) {		
		final Graphic graphic = npc.getGraphic();

		str.writeShort(graphic.getId());
		str.writeInt(graphic.getType().getHeight() | graphic.getDelay());
	}

	public static void appendNPCUpdateBlock(NPC npc, Stream str, Player player) {
		if (!npc.isUpdateRequired()) {
			return;
		}

		int updateMask = npc.getUpdateFlags().get();

		if (updateMask >= 0x100) {
			updateMask |= 0x1;
			str.writeByte(updateMask & 0xFF);
			str.writeByte(updateMask >> 8);
		} else {
			str.writeByte(updateMask);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.ANIMATION)) {
			appendAnimUpdate(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.HIT2)) {
			appendHitUpdate2(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.GRAPHICS)) {
			appendMask80Update(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.ENTITY_INTERACTION)) {
			appendFaceEntity(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.FORCED_CHAT)) {
			appendForcedText(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.HIT)) {
			appendHitUpdate(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.TRANSFORM)) {
			appendTransformUpdate(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.FACE_COORDINATE)) {
			appendSetFocusDestination(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.PET)) {
			appendPet(npc, str);
		}
		
		if (npc.getUpdateFlags().contains(UpdateFlag.NAME_CHANGE)) {
			appendName(npc, str);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.FORCE_MOVEMENT)) {
			appendMask400Update(npc, str, player);
		}

		if (npc.getUpdateFlags().contains(UpdateFlag.BAR_FILL)) {
			appendBarFill(npc, str);
		}
	}

	public static void appendBarFill(NPC npc, Stream str) {
		str.writeShort(npc.startFill);
		str.writeByte(npc.stayCycleAfterFill);
		str.writeByte(npc.fill);
		str.writeShort(npc.maxFill);
		str.writeByte(npc.reverse ? 1 : 0);
		str.writeInt(npc.fillColor1);
		str.writeInt(npc.fillColor2);
	}

	private static void appendMask400Update(NPC npc, Stream str, Player player) {
		int offsetX = (npc.movedX > 0 ? npc.movedX : npc.getCurrentLocation().getX()) - (player.mapRegionX << 3);
		int offsetY = (npc.movedY > 0 ? npc.movedY : npc.getCurrentLocation().getY()) - (player.mapRegionY << 3);
		str.writeByteS(offsetX + npc.x1);
		str.writeByteS(offsetY + npc.y1);
		str.writeByteS(offsetX + npc.x2);
		str.writeByteS(offsetY + npc.y2);
		str.writeLEShortA(npc.speed1);
		str.writeShortA(npc.speed2);
		str.writeByteS(npc.direction);
	}

	private static void appendName(NPC npc, Stream str) {
		String name = npc.customName;
		if (name == null) {
			name = "";
		}

		str.writeString(name);
	}

	private static void appendSetFocusDestination(NPC npc, Stream str) {
		str.writeLEShort(npc.getFacingLocation().getX()*2+1);
		str.writeLEShort(npc.getFacingLocation().getY()*2+1);
	}
	
	private static void appendPet(NPC npc, Stream str) {
		str.writeByte(npc.isPokemon ? 1 : 0);
		str.writeByte(npc.petScale);
	}

	private static void appendTransformUpdate(NPC npc, Stream str) {
		str.write3Byte(npc.transformId <= -1 ? npc.npcType : npc.transformId);
	}

	public static void updateNPC(Player plr, Stream str) {
		final Stream updateBlock = new Stream(new byte[Config.BUFFER_SIZE]);

		str.createVariableShortPacket(plr.extendedNpcRendering ? 66 : 65);
		str.initBitAccess();

		str.writeBits(8, plr.npcListSize);
		int size = plr.npcListSize;
		plr.npcListSize = 0;
		for (int i = 0; i < size; i++) {
			int index = (int) plr.npcList[i];
			NPC npc = NPCHandler.npcs[index];
			if (npc == null || npc.heightLevel != plr.heightLevel || npc.didTeleport || !withinDistance(plr, npc) || !drawNpcForPlayer(plr, npc)) {
				plr.npcInListBitmap[index >> 3] &= ~(1 << (index & 7));
				str.writeBits(1, 1);
				str.writeBits(2, 3);
				continue;
			}

			updateNPCMovement(npc, str);
			appendNPCUpdateBlock(npc, updateBlock, plr);
			plr.npcList[plr.npcListSize++] = (short) index;
		}

		int count = 0;
		for (NPC n : Server.getRegionManager().getLocalNpcsRendering(plr)) {
			if (n == null) {
				continue;
			}

			if (count >= PlayerHandler.MAX_NPC_ADD_COUNT || plr.npcListSize > PlayerConstants.maxNPCListSizeCheck) {
				break;
			}

			if (!drawNpcForPlayer(plr, n)) {
				continue;
			}

			if ((plr.npcInListBitmap[n.npcIndex >> 3] & (1 << (n.npcIndex & 7))) == 0) {
				addNewNPC(plr, n, str, updateBlock);
				count++;
			}
		}

		if (updateBlock.currentOffset > 0) {
			str.writeBits(14, 16383);
			str.finishBitAccess();
			str.writeBytes(updateBlock.buffer, updateBlock.currentOffset, 0);
		} else {
			str.finishBitAccess();
		}

		str.endFrameVarSizeWord();
	}

	private static boolean drawNpcForPlayer(Player player, NPC npc) {
		if (!npc.isActive || npc.needRespawn || npc.moveNpcThisTick || !npc.isVisible()) {
			return false;
		}

		if (npc.getHomeOwnerId() != player.getHomeOwnerId()) {
			return false;
		}

		return true;
	}

	public static void updateNPCMovement(NPC npc, Stream str) {
		if (npc.getWalkingQueue().getWalkDir() == -1) { // is not moving // if (npc.direction == -1) {
			npc.isMoving = false;
			if (npc.isUpdateRequired()) {

				str.writeBits(1, 1);
				str.writeBits(2, 0);
			} else {
				str.writeBits(1, 0);
			}
		} else if (npc.getWalkingQueue().getRunDir() == -1) { // is not running? so walking? //} else if (npc.direction2 == -1) {
			npc.isMoving = true;
			str.writeBits(1, 1);
			str.writeBits(2, 1);
			str.writeBits(3, npc.getWalkingQueue().getWalkDir()); //str.writeBits(3, Misc.xlateDirectionToClient[npc.direction]);
			if (npc.isUpdateRequired()) {
				str.writeBits(1, 1);
			} else {
				str.writeBits(1, 0);
			}
		} else { // else is running?
			npc.isMoving = true;
			str.writeBits(1, 1);
			str.writeBits(2, 2);
			str.writeBits(3, npc.getWalkingQueue().getWalkDir()); //str.writeBits(3, Misc.xlateDirectionToClient[npc.direction]);
			str.writeBits(3, npc.getWalkingQueue().getRunDir());
			if (npc.isUpdateRequired()) {
				str.writeBits(1, 1);
			} else {
				str.writeBits(1, 0);
			}
		}
	}

	private NpcUpdating() {

	}

	public static boolean withinDistance(Player player, NPC npc) {
		int deltaX = npc.getCurrentLocation().getX() - player.getCurrentLocation().getX(), deltaY = npc.getCurrentLocation().getY() - player.getCurrentLocation().getY();
		int minDistance;
		int maxDistance;
		if (player.extendedNpcRendering) {
			minDistance = 128;
			maxDistance = -129;
		} else {
			minDistance = 15;
			maxDistance = -16;
		}

		return deltaX <= minDistance && deltaX >= maxDistance && deltaY <= minDistance && deltaY >= maxDistance;
	}

}
