package com.soulplay.game.model.player;

import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Location;

public class ForceMovementMask {

	private final int dir1X;
	private final int dir1Y;
	private final int dir1Ticks;
	private int dir2X;
	private int dir2Y;
	private int dir2Ticks;
	private final Direction faceDir;
	private final int firstMovementAnim;
	private int secondMovementAnim;
	private int startAnim;
	private int endAnim;
	private boolean telePlayerAtEnd;
	private int customMovePlayerTick;
	private int movedX;
	private int movedY;
	private Location teleLoc;
	private int lockActionsTick;
	private int walkStanceId;
	private int dir1ClientTicks;
	private int dir2ClientTicks;
	
	private ForceMovementMask(int xDirection, int yDirection, int ticks, int firstMovementAnimation, Direction faceDirection) {
		this.dir1X = xDirection;
		this.dir1Y = yDirection;
		this.dir1Ticks = ticks;
		this.firstMovementAnim = firstMovementAnimation;
		this.faceDir = faceDirection;
		this.telePlayerAtEnd = true;
	}
	
	/**
	 * adds/edits a 2nd direction to the movement mask.
	 * @param xDir direction of X from current location
	 * @param yDir direction of Y from current location
	 * @param ticks the length to move from current location to x/yDir location
	 */
	public ForceMovementMask addSecondDirection(int xDir, int yDir, int ticks, int movementAnimation) {
		this.dir2X = xDir;
		this.dir2Y = yDir;
		this.dir2Ticks = ticks;
		this.secondMovementAnim = movementAnimation;
		return this;
	}
	
	/**
	 * Adds an animation to the beginning of the animation mask.
	 * @param anim
	 * @return
	 */
	public ForceMovementMask addStartAnim(int anim) {
		this.startAnim = anim;
		return this;
	}
	
	/**
	 * Adds an animation at the end of the forcemomvement mask.
	 * @param anim
	 * @return
	 */
	public ForceMovementMask addEndAnim(int anim) {
		this.endAnim = anim;
		return this;
	}
	
	public static ForceMovementMask createMask(int xDirection, int yDirection, int ticks, int movementAnimation, Direction faceDirection) {
		return new ForceMovementMask(xDirection, yDirection, ticks, movementAnimation, faceDirection);
	}

	public int getDir1X() {
		return dir1X;
	}

	public int getDir1Y() {
		return dir1Y;
	}

	public int getDir1Ticks() {
		return dir1Ticks;
	}

	public int getDir2X() {
		return dir2X;
	}

	public void setDir2X(int dir2x) {
		dir2X = dir2x;
	}

	public int getDir2Y() {
		return dir2Y;
	}

	public void setDir2Y(int dir2y) {
		dir2Y = dir2y;
	}

	public int getDir2Ticks() {
		return dir2Ticks;
	}

	public void setDir2Ticks(int dir2Ticks) {
		this.dir2Ticks = dir2Ticks;
	}

	public int getFaceDir() {
		return faceDir.toInteger();
	}

	public int getFirstMovementAnim() {
		return firstMovementAnim;
	}

	public int getStartAnim() {
		return startAnim;
	}

	public int getEndAnim() {
		return endAnim;
	}

	public ForceMovementMask setEndAnim(int endAnim) {
		this.endAnim = endAnim;
		return this;
	}

	public int getSecondMovementAnim() {
		return secondMovementAnim;
	}

	public ForceMovementMask setSecondMovementAnim(int secondMovementAnim) {
		this.secondMovementAnim = secondMovementAnim;
		return this;
	}

	public boolean isTelePlayerAtEnd() {
		return telePlayerAtEnd;
	}

	public ForceMovementMask setTelePlayerAtEnd(boolean telePlayerAtEnd) {
		this.telePlayerAtEnd = telePlayerAtEnd;
		return this;
	}

	public int getCustomMovePlayerTick() {
		return customMovePlayerTick;
	}

	public ForceMovementMask setCustomMovePlayerTick(int customMovePlayerTick) {
		this.customMovePlayerTick = customMovePlayerTick;
		return this;
	}

	public int getMovedX() {
		return movedX;
	}

	public int getMovedY() {
		return movedY;
	}

	/**
	 * Used for multiple update masks without moving player's actual location
	 * @param movedX - the absX location of where player is located after first ForceMovementMask
	 * @param movedY - the absY location of where player is located after first ForceMovementMask
	 */
	public ForceMovementMask setNewOffset(int movedX, int movedY) {
		this.movedX = movedX;
		this.movedY = movedY;
		return this;
	}
	
	public ForceMovementMask transformOffset(int x, int y) {
		this.movedX += x;
		this.movedY += y;
		return this;
	}

	public Location getTeleLoc() {
		return teleLoc;
	}

	public ForceMovementMask setTeleLoc(Location teleLoc) {
		this.teleLoc = teleLoc;
		return this;
	}

	public int getLockActionsTick() {
		return lockActionsTick;
	}

	public ForceMovementMask setLockActionsTick(int lockActionsTick) {
		this.lockActionsTick = lockActionsTick;
		return this;
	}

	public int getWalkStanceId() {
		return walkStanceId;
	}

	public ForceMovementMask setWalkStanceId(int walkStanceId) {
		this.walkStanceId = walkStanceId;
		return this;
	}
	
	public ForceMovementMask setCustomSpeed(int dir1, int dir2) {
		this.dir1ClientTicks = dir1;
		this.dir2ClientTicks = dir2;
		return this;
	}
	
	public int getCustomSpeed1() {
		return dir1ClientTicks;
	}
	
	public int getCustomSpeed2() {
		return dir2ClientTicks;
	}

}
