package com.soulplay.game.model.player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.combat.data.FightExp;
import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.event.PkingPotEvent;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.degradeable.BrawlerGloveEnum;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.minigames.gamble.GambleStage;
import com.soulplay.content.minigames.gamble.GamblingManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.npcs.impl.bosses.nex.NexEvent;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.Kill;
import com.soulplay.content.player.combat.PlayerKilling;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.Requirement;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.magic.SpellsOfPI;
import com.soulplay.content.player.combat.melee.MeleeData;
import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.combat.rules.AntiRush;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.magic.EnchantJewelry;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.content.player.skills.smithing.SmeltingData;
import com.soulplay.content.player.soulpvp.SoulpvpStarterBank;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.content.shops.ShopPrice;
import com.soulplay.content.tob.TobManager;
import com.soulplay.content.tob.rooms.TobJailLocation;
import com.soulplay.content.tob.rooms.TobRoom;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.DisplayTimerType;
import com.soulplay.game.world.Direction;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.PickedUpItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.net.Connection;
import com.soulplay.net.packet.in.Chat;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.EloRatingSystem;
import com.soulplay.util.Misc;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.util.sql.clanelo.ClanEloHandler;
import com.soulplay.util.sql.clanelo.ClanInfo;
import com.soulplay.util.sql.scoregrab.DuelScore;
import com.soulplay.util.sql.scoregrab.DuelScoreBoard;
import com.soulplay.util.sql.scoregrab.LmsScore;
import com.soulplay.util.sql.scoregrab.LmsScoreBoard;
import com.soulplay.util.sql.scoregrab.WildyEloRating;
import com.soulplay.util.sql.scoregrab.WildyEloRatingSeason;
import com.soulplay.util.sql.scoregrab.WildyRecord;

public class PlayerAssistant {

	private Client c;

	/**
	 * Show option, attack, trade, follow etc
	 **/
	public Map<Integer, String> optionCache = new HashMap<>();

	public boolean checkUpdateOption(String text, int id) {
		String t = optionCache.get(id);
		if (t != null && t.equals(text)) {
			return false;
		}

		optionCache.put(id, text);
		return true;
	}

	private static final int[] voteRewardItems = {11730, 10887, 15486, 11694, 11696, 11698,
			11700, 13738, 13740, 13742, 13744, 11283, 11726, 11724, 11718,
			11720, 11722, 14484, 11730, 10887, 15486, 11694, 11696, 11698,
			11700, 13738, 13740, 13742, 13744, 11283, 11726, 11724, 11718,
			11720, 11722, 14484, 20135, 20139, 20143, 20147, 20151, 20155,
			20159, 20163, 20167, 20171, 19780, 30174, 30175, 21805, 30367, 30176};

	public PlayerAssistant(Client Client) {
		this.c = Client;
	}
	
	public void collectMoney(boolean usingBanker) {
		if (c.playerCollect < 0) {
			c.playerCollect = 0;
		}

		if (!usingBanker && !c.inMarket()) {
			c.sendMessage("You must be in market to use this command.");
			return;
		}

		if (c.teleTimer > 0) {
			c.sendMessage("Please wait for your teleport to finish.");
			return;
		}

		if (c.playerCollect > 0 && c.playerCollect < Integer.MAX_VALUE) {
			if ((c.getItems().getItemAmount(995) + c.playerCollect < Integer.MAX_VALUE)
					&& c.getItems().freeSlots() > 0) {
				c.sendMessage("You succesfully collected " + c.playerCollect + " coins.");
				c.getItems().addItem(995, (int) c.playerCollect);
				c.playerCollect = 0;
			} else {
				c.sendMessage("Need more gp space in your inv.");
				return;
			}

		} else if (c.playerCollect > Integer.MAX_VALUE) {
			int bagsSlot = c.getItems().getItemSlot(10835);
			if (bagsSlot == -1 && c.getItems().freeSlots() <= 0) {
				c.sendMessage("Not enough space in your inventory to collect cash.");
				return;
			}

			int checks = (int) (c.playerCollect / 2000000000);
			int bags = bagsSlot == -1 ? 0 : c.playerItemsN[bagsSlot];
			if (((long) bags + checks) < Integer.MAX_VALUE && c.getItems().addItem(10835, checks)) {
				c.playerCollect -= (2000000000 * (long) checks);
				c.sendMessage("Collected " + checks + " checks because collect amt to high.");
				c.sendMessage("You still have " + c.playerCollect + "gp to collect. Use ::collect");
			} else {
				c.sendMessage("Couldn't add all of your bags. Try banking some.");
			}
		} else {
			// c.playerCollect = 0;
			c.sendMessage("You dont have anything to collect");
		}
	}

	public void activateSOL() {
		if (c.usingSOLSpec) {
			return;
		}
//		if (c.getItems().playerHasEquippedWeapon(30011)) { // staff of the dead
//			
//		} else {
//			c.startGraphic(Graphic.create(2319, GraphicType.LOW));
//			c.startAnimation(12804);
//		}
		c.usingSOLSpec = true;
		c.sendMessage("You get increased armor against Melee.");
		Server.getTaskScheduler().schedule(new Task(true) {

			int ticks = 100;

			@Override
			protected void execute() {
				if (c == null || !c.isActive || c.forceDisconnect) {
					ticks = -1;
				}
				if (ticks > -1 && (c.isDead() || !c.usingSOLSpec
						|| !c.getPA().isSOLEquipped())) {
					if (c.isActive) {
						c.sendMessage(
								"The power of the light fades. Your resistance to melee attacks returns to normal.");
					}
					ticks = -1;
				}
				if (ticks < 0) {
					if (c != null && c.isActive) {
						c.usingSOLSpec = false;
					}
					this.stop();
				}
				ticks--;
			}
		});
	}

	public void addClueReward(int clueLevel) {
		int chanceReward = Misc.random(2);
		if (clueLevel == 0) {
			switch (chanceReward) {
				case 0:
					displayReward(
							PlayerConstants.lowLevelReward[Misc.random(16)], 1,
							PlayerConstants.lowLevelReward[Misc.random(16)], 1,
							PlayerConstants.lowLevelStacks[Misc.random(3)],
							1 + Misc.random(150));
					break;
				case 1:
					displayReward(
							PlayerConstants.lowLevelReward[Misc.random(16)], 1,
							PlayerConstants.lowLevelStacks[Misc.random(3)],
							1 + Misc.random(150), -1, 1);
					break;
				case 2:
					displayReward(
							PlayerConstants.lowLevelReward[Misc.random(16)], 1,
							PlayerConstants.lowLevelReward[Misc.random(16)], 1,
							-1, 1);
					break;
			}
		} else if (clueLevel == 1) {
			switch (chanceReward) {
				case 0:
					displayReward(
							PlayerConstants.mediemLevelReward[Misc.random(13)],
							1,
							PlayerConstants.mediemLevelReward[Misc.random(13)],
							1,
							PlayerConstants.mediumLevelStacks[Misc.random(4)],
							1 + Misc.random(200));
					break;
				case 1:
					displayReward(
							PlayerConstants.mediemLevelReward[Misc.random(13)],
							1,
							PlayerConstants.mediumLevelStacks[Misc.random(4)],
							1 + Misc.random(200), -1, 1);
					break;
				case 2:
					displayReward(
							PlayerConstants.mediemLevelReward[Misc.random(13)],
							1,
							PlayerConstants.mediemLevelReward[Misc.random(13)],
							1, -1, 1);
					break;
			}
		} else if (clueLevel == 2) {
			switch (chanceReward) {
				case 0:
					displayReward(
							PlayerConstants.highLevelReward[Misc.random(75)], 1,
							PlayerConstants.highLevelReward[Misc.random(60)], 1,
							PlayerConstants.highLevelStacks[Misc.random(5)],
							1 + Misc.random(350));
					break;
				case 1:
					displayReward(
							PlayerConstants.highLevelReward[Misc.random(52)], 1,
							PlayerConstants.highLevelStacks[Misc.random(5)],
							1 + Misc.random(350), -1, 1);
					break;
				case 2:
					displayReward(
							PlayerConstants.highLevelReward[Misc.random(75)], 1,
							PlayerConstants.highLevelReward[Misc.random(60)], 1,
							-1, 1);
					break;
			}
		}
	}

	public void addPvPStarter() {
		if (c.isBot()) {
			return;
		}

		SoulpvpStarterBank.addStarterBank(c);
	}

	@SuppressWarnings("deprecation")
	public boolean addSkillXP(int amount, int skill) {
		final boolean inLMS = c.inLMSArenaCoords() || c.getZones().isInDmmTourn();

		if ((c.getCmbExpLock() || inLMS) && (skill >= Skills.ATTACK && skill <= Skills.RANGED || skill == Skills.MAGIC)) {
			c.getPacketSender().setSkillLevel(skill, c.getSkills().getLevel(skill),
					c.getSkills().getExperience(skill) + amount);
			c.getPacketSender().sendExpDrop(skill, amount);
			return false;
		}

		if (inLMS || c.expLock || c.isInJail()) {
			c.getPacketSender().sendExpDrop(skill, amount);
			return false;
		}

		if (amount + c.getSkills().getExperience(skill) < 0 || c.getSkills().getExperience(skill) >= Skills.MAX_XP) {
//			if (c.getSkills().getExperience(skill) > 200000000) {
//				c.getSkills().increaseExperience(skill, 200000000);
//			}
			c.getPacketSender().sendExpDrop(skill, amount);
			return false;
		}
		if (c.insideDungeoneering() && (skill >= Skills.ATTACK && skill <= Skills.MAGIC || skill == Skills.RUNECRAFTING)) {
			amount *= 1;
		} else if (c.difficulty == PlayerDifficulty.NORMAL) { // normal mode
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 40;
			}
		} else if (c.difficulty == PlayerDifficulty.MEDIUM) { // medium
																// difficulty
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 20;
			}
		} else if (c.difficulty == PlayerDifficulty.HARD) { // hard difficulty
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 10;
			}
		} else if (c.isIronMan()) { // ironman
																// difficulty
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 3;
			}
		} else if (c.difficulty == PlayerDifficulty.PRESTIGE_ONE) { // prestige
																	// stage 1
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 30;
			}
		} else if (c.difficulty == PlayerDifficulty.PRESTIGE_TWO) { // prestige
																	// stage 2
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 20;
			}
		} else if (c.difficulty == PlayerDifficulty.PRESTIGE_THREE) { // prestige
																		// stage
																		// 3
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				amount *= 5;
			}
		}

		if (WorldType.equalsType(WorldType.ECONOMY) && !c.insideDungeoneering()) {
			if (c.difficulty == PlayerDifficulty.NORMAL && ((skill >= 0 && skill <= 4 || skill == 6) && c.getSkills().getExperience(skill) < 13034436 && c.wildLevel < 1)) {
				amount *= Config.EASY_MODE_COMBAT_EXP_RATE;
			}
		}
		
		if (((skill >= 0 && skill <= 4 || skill == 6)
				&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			
			amount *= c.getBrawlers().handleExperienceGain(skill);
			
		} else {
			amount *= DBConfig.SERVER_EXP_BONUS;
			if (DBConfig.DOUBLE_EXP || DBConfig.GLOBAL_EXP) {
				if (c.getDoubleExpLength() > 0) {
					amount *= 3;
				} else {
					amount *= 2;
				}
			} else if (c.getDoubleExpLength() > 0) {
				amount *= 2;
			} else { // brawler gloves will not stack with double exp
				amount *= c.getBrawlers().handleExperienceGain(skill);
			}
		}

		if (c.getClan() != null) {
			if (c.getClan().getClanPoints() > 24814
					&& c.getClan().getClanPoints() < 61512) {
				amount *= 1.01;
			} else if (c.getClan().getClanPoints() > 61511
					&& c.getClan().getClanPoints() < 449428) {
				amount *= 1.02;
			} else if (c.getClan().getClanPoints() > 449428
					&& c.getClan().getClanPoints() < 1986068) {
				amount *= 1.03;
			} else if (c.getClan().getClanPoints() > 1986067
					&& c.getClan().getClanPoints() < 8771558) {
				amount *= 1.05;
			} else if (c.getClan().getClanPoints() > 8771557
					&& c.getClan().getClanPoints() < 63555443) {
				amount *= 1.07;
			} else if (c.getClan().getClanPoints() > 63555442) {
				amount *= 1.10;
			}
		}

		if (c.getTomeExpLength() > 0) {
			amount += amount * Variables.TOME_EXTRA_XP_MOD.getValue(c) / 100;
		}

		int oldLevel = c.getSkills().getStaticLevel(skill);
		c.getSkills().addExperience(skill, amount);
		if (oldLevel < c.getSkills().getStaticLevel(skill)) {
			levelUp(skill);
			c.getSkills().updateLevel(skill, c.getSkills().getStaticLevel(skill) - oldLevel, c.getSkills().getStaticLevel(skill));
			if (oldLevel == 98) {
				c.startGraphic(LVL_MASTERY_GRAPHIC);
			} else {
				c.startGraphic(LVL_UP_GRAPHIC);
			}

			if (skill > Skills.MAGIC && skill != Skills.DUNGEONEERING && skill != Skills.SUMMONING && skill != Skills.SLAYER) {
				Titles.skiller(c);
			}
		}

		if (skill == Skills.RANGED && c.getSkills().getExperience(Skills.RANGED) >= Skills.MAX_XP) {
			Titles.maxExpRange(c);
		}

		if ((skill == Skills.ATTACK || skill == Skills.STRENGTH) && (c.getSkills().getExperience(Skills.ATTACK) >= Skills.MAX_XP && c.getSkills().getExperience(Skills.STRENGTH) >= Skills.MAX_XP)) {
			Titles.maxExpMelee(c);
		}

		if (skill == Skills.MAGIC && c.getSkills().getExperience(Skills.MAGIC) >= Skills.MAX_XP) {
			Titles.maxExpMage(c);
		}

		if (c.getSkills().getExperience(skill) >= Skills.MAX_XP) {
			Titles.maxExpStat(c);
		}
		
		if (c.difficulty == PlayerDifficulty.HC_IRONMAN_DEAD) {
			c.demoteToRegularIronMan();
			c.sendMessage("You have earned experience so you get removed from HCIM leaderboards.");
		}

		c.getPacketSender().sendExpDrop(skill, amount);
		c.refreshSkill(skill);
		return true;
	}

	public static int getMultiplier(int skill, Player c) {
		int multiplier = 1;
		if (c.difficulty == PlayerDifficulty.NORMAL) { // normal mode
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 40;
			}
		} else if (c.difficulty == PlayerDifficulty.MEDIUM) { // medium
																// difficulty
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 20;
			}
		} else if (c.difficulty == PlayerDifficulty.HARD) { // hard difficulty
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 10;
			}
		} else if (c.isIronMan()) { // ironman
																// difficulty
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 3;
			}
		} else if (c.difficulty == PlayerDifficulty.PRESTIGE_ONE) { // prestige
																	// stage 1
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 30;
			}
		} else if (c.difficulty == PlayerDifficulty.PRESTIGE_TWO) { // prestige
																	// stage 2
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 20;
			}
		} else if (c.difficulty == PlayerDifficulty.PRESTIGE_THREE) { // prestige
																		// stage
																		// 3
			if (((skill >= 0 && skill <= 4 || skill == 6)
					&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			} else {
				multiplier = 5;
			}
		}

		if (((skill >= 0 && skill <= 4 || skill == 6)
				&& (c.getSkills().getExperience(skill) > 13034436 && c.wildLevel > 0))) {
			multiplier *= c.getBrawlers().handleExperienceGain(skill);
		} else {
			multiplier *= DBConfig.SERVER_EXP_BONUS;
			if (DBConfig.DOUBLE_EXP || DBConfig.GLOBAL_EXP) {
				if (c.getDoubleExpLength() > 0) {
					multiplier *= 3;
				} else {
					multiplier *= 2;
				}
			} else if (c.getDoubleExpLength() > 0) {
				// if(System.currentTimeMillis() - c.doubleExpLength < 3600000)
				// {
				multiplier *= 2;
				// c.sendMessage("Getting double exp");
				// } else {
				// c.doubleExpLength = 0;
				// c.sendMessage("Double EXP has expired.");
				// }
			} else { // brawler gloves will not stack with double exp
				multiplier *= c.getBrawlers().handleExperienceGain(skill);
			}
		}

		return multiplier;
	}

	public static final Graphic LVL_UP_GRAPHIC = Graphic.create(199, GraphicType.HIGH);
	private static final Graphic LVL_MASTERY_GRAPHIC = Graphic.create(1634, GraphicType.HIGH);

	public void addStarter() {
		if (c.isBot()) {
			return;
		}

		if (!Connection.hasRecieved1stStarter(c.UUID)) {
			c.setDoubleExpLength(6000);
			c.sendMessage("<col=ff0000>You have received a starter one hour double EXP boost.");

			c.getItems().addItem(995, 500000);
			c.getItems().addItem(1725, 1);
			c.getItems().addItem(554, 1000);
			c.getItems().addItem(555, 1000);
			c.getItems().addItem(556, 1000);
			c.getItems().addItem(558, 1000);
			c.getItems().addItem(560, 1000);
			c.getItems().addItem(565, 1000);
			c.getItems().addItem(1323, 1);
			c.getItems().addItem(841, 1);
			c.getItems().addItem(882, 1000);
			c.getItems().addItem(10499, 1);
			c.getItems().addItem(380, 200);
			c.getItems().addItem(542, 1);
			c.getItems().addItem(544, 1);
			c.getItems().addItem(2, 30); // cannon balls
			c.getItems().addItem(6, 1); // cannon base
			c.getItems().addItem(8, 1); // cannon stand
			c.getItems().addItem(10, 1); // cannon barrel
			c.getItems().addItem(12, 1); // cannon furnace
			if (WorldType.isSeasonal()) {
				c.getItems().addItem(19705, 1);
			}

			Connection.addIpToStarter1(c.UUID);
			c.sendMessage("You have received 1 out of 2 starter packages on this IP address.");
		} else if (Connection.hasRecieved1stStarter(c.UUID)	&& !Connection.hasRecieved2ndStarter(c.UUID)) {
			c.setDoubleExpLength(6000);
			c.sendMessage("<col=ff0000>You have received a starter one hour double EXP boost.");

			c.getItems().addItem(995, 500000);
			c.getItems().addItem(1725, 1);
			c.getItems().addItem(554, 100);
			c.getItems().addItem(555, 100);
			c.getItems().addItem(556, 100);
			c.getItems().addItem(558, 100);
			c.getItems().addItem(560, 100);
			c.getItems().addItem(565, 100);
			c.getItems().addItem(1323, 1);
			c.getItems().addItem(841, 1);
			c.getItems().addItem(882, 150);
			c.getItems().addItem(10499, 1);
			c.getItems().addItem(380, 120);
			c.getItems().addItem(542, 1);
			c.getItems().addItem(544, 1);
			c.getItems().addItem(2, 30); // cannon balls
			c.getItems().addItem(6, 1); // cannon base
			c.getItems().addItem(8, 1); // cannon stand
			c.getItems().addItem(10, 1); // cannon barrel
			c.getItems().addItem(12, 1); // cannon furnace
			if (WorldType.isSeasonal()) {
				c.getItems().addItem(19705, 1);
			}

			c.sendMessage("You have received 2 out of 2 starter packages on this IP address.");
			Connection.addIpToStarter2(c.UUID);
		} else {
			c.sendMessage("You have already received 2 starters!");
		}
	}

	public boolean allowTPA() {
		if (c.isInJail() || c.underAttackBy > 0 || c.underAttackBy2 > 0
				|| c.playerIndex > 0 || c.npcIndex > 0 || c.wildLevel > 0
				|| c.inMinigame() || c.performingAction || c.noClip) {
			return false;
		}
		if (RSConstants.dzDungeon(c.getX(), c.getY())
				|| RSConstants.dzIsland(c.getX(), c.getY())) {
			return false;
		}
		if (c.inBossRoom
				|| c.inFightCaves() || c.duelFightArenas() || c.inPits
				|| c.inPcGame() || c.inPcBoat()) {
			return false;
		}
		return true;
	}

	public boolean wearingAntifireShield() {
		switch (c.playerEquipment[PlayerConstants.playerShield]) {
			case 1540:
			case 11283:
			case 11284:
			case 2890:
			case 30336:
				return true;
			case 16079:
			case 16933:
				if (c.insideDungeoneering()) {
					return true;
				}

				return false;
		}

		return false;
	}
	
	public void chargeDfs() {
		switch (c.playerEquipment[PlayerConstants.playerShield]) {

		case 11283:
		case 30336:
			if (c.dfsCharge < 50) {
				c.addDfsCharge();
				c.startGraphic(Graphic.create(1163));
				c.startAnimation(6696);
				c.sendMessageSpam(
						"You receive a dragonfire shield charge.");
				if (c.dfsCharge == 50) {
					c.sendMessage(
							"Your dragonfire shield charges are full.");
				}
			}
			break;
		}
		
	}
	
	public int handleDragonFireHit(NPC npc, int damage) {
		int anti = c.getPA().antiFire();
		if (anti == 0 && (c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
						|| c.curseActive[Curses.DEFLECT_MAGIC.ordinal()])) { // protect	
			if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
				c.curses().deflectNPC(npc, 0, 2);
			}
			anti = 1;
		}
		switch (anti) {
			case 0:
				c.sendMessageSpam(
						"You are horribly burned by the dragonfire!");
				break;
			case 1:
				c.sendMessageSpam(
						"You manage to resist some of the dragonfire.");
				damage = Misc.random(20);
				break;
			case 2:
				c.sendMessageSpam(
						"You manage to resist some of the dragonfire.");
				damage = Misc.random(5);
				break;
			case 3:
				c.sendMessageSpam(
						"You are protected from the heat of the dragon's breath.");
				damage = 0;
				break;
		}
		return damage;
	}

	public int antiFire() {
		int anti = 0;
		if (wearingAntifireShield()) {
			anti = 2;
		}

		if (c.antiFirePot != null) {
			anti++;
		}

		return anti;
	}

	public void antiFirePotion() {
		if (c.antiFirePot != null) {
			c.antiFirePot.stop();
			c.antiFirePot = null;
		}

		if (c.antiFirePotExpireMessage != null) {
			c.antiFirePotExpireMessage.stop();
			c.antiFirePotExpireMessage = null;
		}

		int ticks = 600; // 6 minutes

		c.antiFirePotExpireMessage = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.antiFirePotExpireMessage = null;
				c.sendMessage("<col=BF3EFF>our antifire potion is about to expire.");
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, ticks - 25); // 15 seconds before expire

		c.antiFirePot = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.antiFirePot = null;
				c.sendMessage("<col=BF3EFF>Your antifire potion has expired.");
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, ticks);
	}

	public void applyBleed(int totalDmg, int dmgPerCycle, int dmgDelay) {
		if (totalDmg < 1) {
			return;
		}
		int roundDmg = Math.round(totalDmg / dmgPerCycle);
		if (roundDmg < 1) {
			roundDmg = 1;
		}
		if (roundDmg * dmgPerCycle > totalDmg) {
			dmgPerCycle = totalDmg;
		}

		// c.sendMessage("damage: "+roundDmg);

		final int dmgPerCycleFinal = roundDmg/* dmgPerCycle */;
		CycleEventHandler.getSingleton().addEvent(c, true, new CycleEvent() {

			int cycles = dmgPerCycleFinal;

			@Override
			public void execute(CycleEventContainer e) {
				if (c.isDead()
						|| c.getSkills().getLifepoints() < 1
						|| cycles <= 0) {
					e.stop();
					return;
				}

				if (dmgPerCycleFinal > c.getSkills().getLifepoints()) {
					// c.playerLevel[3] = 0;
				} else {
					c.dealDamage(new Hit(dmgPerCycleFinal, 0, -1));
					// c.damageToDealAfterCycle += dmgPerCycleFinal;//
					// c.playerLevel[3]
					// -=
					// dmgPerCycleFinal;
				}

				c.getPA().closeAllWindows();

				cycles--;
			}

			@Override
			public void stop() {
			}
		}, dmgDelay);
	}

	public void applyImmunity(int ticks) {
		c.sendMessage("You are immune to PJers for "
				+ Misc.ticksIntoSeconds(ticks) + " seconds.");
		c.immuneToPK = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer e) {
				if (c != null) {
					if (c.immuneToPK) {
						c.immuneToPK = false;
					}
				}
				e.stop();
			}

			@Override
			public void stop() {

			}
		}, ticks);
	}
	
	public void applyIronmanDeath(boolean safeDeath) {
		if (!c.isHCIronMan() || safeDeath || isHCIMSafeDeathZone())
			return;
		
		if (!c.inMinigame()) {
			c.sendMessage("You have lost your Hard Core Ironman Rank!");
			c.promoteToDeadHCIM();
			c.sendMessage("If you earn any more experience, you will be removed from HCIM hiscores.");
			int totalLevel = c.getSkills().getTotalLevel();
			if (totalLevel > 700) {
				PlayerHandler.messageAllPlayers(c.getNameSmartUp() + " has died as a Hard Core IronMan. Total Level:" + totalLevel);
			}
		}
	}
	
	private boolean isHCIMSafeDeathZone() {
		return RSConstants.zulrahArea(c.getX(), c.getY());
	}

	public void applyDeath() {
		if (c.hidePlayer) {
			c.setDead(false);
			return;
		}

		// setting to alive from dead to not create event, like
		// in duel, if both players died but one recovered
		// cuz he was winner
		if (!c.isDead()) {
			return;
		}
		
		if (c.applyDeath)
			return;

		c.applyDeath = true;
		c.resetSkillingEvent();

		BarbarianAssault.onDeath(c);
		
		if (Misc.random(10000) == 0)//random chance to play sound atm
			c.getPacketSender().playSound(Sounds.GOBLIN_TAKING_DAMAGE_CHANGED_TO_WTFWASTHAT, Misc.serverToClientTick(20));
		
		final boolean killedInLMS = c.isInLmsArena();
		
		final boolean killedInDmmTourn = c.getZones().isInDmmTourn();
		final boolean keepUntradeables = (killedInDmmTourn && c.hasSpawnedGear) ? false : true;

		final boolean itemsKeptOnDeath = c.getPA().keepItemsOnDeathPrayerOn();

		c.getAchievement().deathcount();
		c.curses().resetCurseBoosts();
		if (c.getSummoning().summonedFamiliar != null || c.summoned != null) {
			if (!c.summoned.isPetNpc()
					&& c.getSummoning().summonedFamiliar != null) {
				c.getSummoning().dismissFamiliar(false);
			} else if (c.summoned.isPetNpc()) {
				Pet.pickupPet(c, Pet.getPetForNPC(c.summoned.npcType));
			}
		}

		 // if player is not under attack and dies in fog, then extend timer to 60 seconds
		final long killerMID = c.getDamageMap().highestDamage();
		
		if (killedInLMS) {
			c.lmsManager.removePlayerFromLMSDamageMaps(c.mySQLIndex);
		}

		Client o = (Client) PlayerHandler.getPlayerByMID(killerMID);
		if (o == null && c.underAttackBy > 0) {
			o = (Client) PlayerHandler.players[c.underAttackBy];
		}

		if (c.getDuelingClient() != null) { // dead inside of duel
			c.getPA().rewardDuelKiller(c.getDuelingClient());
			c.sendMessage("You have lost the duel!");
		} else { // dead outside of duel
			c.sendMessage(Config.DEATH_MESSAGE);
			if (o != null) {
				c.killedByPlayerName = o.getName();
				c.getPA().rewardKiller(o);
			}
		}

		final boolean safeDeath = c.getPA().isInSafeDeathSpot();
		c.safeDeath = false;
		
		applyIronmanDeath(safeDeath);

		final boolean killedInWildy = c.wildLevel > 0;

		if (!c.getTradeAndDuel().stakedItems.isEmpty()) {
			Map<Integer, Integer> map = new HashMap<>();
			for (GameItem items : c.getTradeAndDuel().stakedItems) {
				map.put(items.getId(), items.getAmount());
			}
			// Server.getSlackApi().call(SlackMessageBuilder.buildDuperMessage(c.getName(),
			// map));
			c.getTradeAndDuel().stakedItems.clear();
		}
		if (!c.getTradeAndDuel().otherStakedItems.isEmpty()) {
			Map<Integer, Integer> map = new HashMap<>();
			for (GameItem items : c.getTradeAndDuel().otherStakedItems) {
				map.put(items.getId(), items.getAmount());
			}
			// Server.getSlackApi().call(SlackMessageBuilder.buildDuperMessage(c.getName(),
			// map));
			c.getTradeAndDuel().otherStakedItems.clear();
		}

		final Client killer = (c.getDuelingClient() != null
				? c.getDuelingClient()
				: o);

		if (c.curseActive[Curses.WRATH.ordinal()]) { // wrath active
			applyWrath();
		} else if (c.prayerActive[Prayers.RETRIBUTION.ordinal()]) { // retribution
			applyRetribution();
		}

		c.getPA().setRespawnLocation();
		
		if (killer != null && killer.getZones().isInClwRedPortalPkZone()) {
			killer.getExtraHiscores().incClwRedPortalKills();
		}
		
		if (killer != null && killer.getZones().isInDmmTourn()) {
			if (!safeDeath)
				killer.sendMessage("You have 30 seconds to pick up the loot.");
			killer.resetDeathEvent();
			killer.getSkills().healToFull();
			killer.getExtraHiscores().incPkTournamentKills();
		}
		
		if (killedInLMS) {
			Location tile = LmsManager.getLobbyRandom();
			c.respawnX = tile.getX();
			c.respawnY = tile.getY();
			c.respawnZ = tile.getZ();
			
			if (killer != null && c != killer) {
				c.getPA().rewardLmsKiller(killer);
				if (c.lmsManager.isActive) { // last man standing!
					if (c.lmsManager.playerSize == 1) {
						c.lmsManager.rewardWinner(killer);
					}
				}
			}

			if (c.lmsManager.isActive) {
				c.lmsRoundPlacement = c.lmsManager.playerSize;
			}
		}
		
		if (c.raidsManager != null) {
			c.respawnX = c.raidsManager.respawnLoc.getX();
			c.respawnY = c.raidsManager.respawnLoc.getY();
			c.respawnZ = c.raidsManager.respawnLoc.getZ();
			c.raidsManager.greatOlmManager.playerDiedOrLeft(c);
			c.raidsManager.points.handleDeath(c.mySQLIndex);
		}

		TobManager tobManager = c.tobManager;
		if (tobManager != null && tobManager.started()) {
			TobRoom tobRoom = tobManager.currentRoom;
			if (tobRoom != null) {
				tobRoom.incDeathCount();
				tobManager.incDeathCount();

				TobJailLocation jailLocation = tobRoom.getNextJailLocation();
				Location tile = tobRoom.base().transform(jailLocation.getLocalX(), jailLocation.getLocalY());
				c.respawnX = tile.getX();
				c.respawnY = tile.getY();
				c.respawnZ = tile.getZ();
			}
		}

		if (c.insideDungeoneering()) {
			DungeonManager manager = c.dungParty.dungManager;
			if (manager.hasStarted()) {
				c.getPA().walkableInterface(-1);

				if (c.dungDeaths < 16) {
					c.dungDeaths++;
				}

				if (c.dungSshDisable != null) {
					c.dungSshDisable.stop();
				}

				// Set respawn
				Location tile = manager.getHomeTile();
				c.respawnX = tile.getX();
				c.respawnY = tile.getY();
				c.respawnZ = tile.getZ();

				// Check gatestones and drop them if exist
				int index = c.getItems().getItemSlot(DungeonConstants.GROUP_GATESTONE);
				if (index != -1) {
					c.getItems().deleteItemInOneSlot(DungeonConstants.GROUP_GATESTONE, index, 1);
					manager.spawnBelow(c, new Item(DungeonConstants.GROUP_GATESTONE, 1));
					c.sendMessage("Your group gatestone drops to the floor as you die.");
				}

				index = c.getItems().getItemSlot(DungeonConstants.GATESTONE);
				if (index != -1) {
					c.getItems().deleteItemInOneSlot(DungeonConstants.GATESTONE, index, 1);
					manager.spawnBelow(c, new Item(DungeonConstants.GATESTONE, 1));
					c.sendMessage("Your gatestone drops to the floor as you die.");
				}

				// send death message to party
				for (Player partyMember : c.dungParty.partyMembers) {
					if (partyMember == c) {
						continue;
					}

					partyMember.sendMessage(c.getNameSmartUp() + " has died.");
				}
			}
		}

		final int respawnX = c.respawnX;
		final int respawnY = c.respawnY;
		final int respawnZ = c.respawnZ;
		c.respawnX = c.respawnY = c.respawnZ = 0;

		c.getPA().resetVariablesOnDeath();

		c.deathContainer = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() { // used to be a pvpTask

			int timer = 7;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.performingAction || c.isLockActions())
					return;
				if (timer == 4) {
					c.setPauseAnimReset(0);
					c.startAnimation(c.getItems().hasEquippedWeapon(7447) ? Animation.DEATH_KNIFE : Animation.DEATH_REGULAR);
					c.setPauseAnimReset(7);
				}

				if (timer == 0) {

					c.getPA().giveLife(killer, itemsKeptOnDeath, respawnX,
							respawnY, respawnZ, safeDeath, killedInWildy, killedInLMS, killedInDmmTourn, keepUntradeables);

					container.stop();
				}

				timer--;

			}

			@Override
			public void stop() {
				if (c != null && c.isActive) {
					c.applyDeath = false;
					c.deathContainer = null;
				}
			}
		}, 1); // 7 ticks instead of 8 cuz new cycle starts 1 tick late

	}

	public void applyRetribution() {
		int totalDamage = c.getSkills().getPrayerPoints() / 4;
		c.startGraphic(Graphic.create(437));
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (i == 0 && j == 0) {
					continue;
				}
				Server.getStillGraphicsManager().stillGraphics(c, c.getX() + i,
						c.getY() + j, 0, 438, 15);
			}
		}
		// for (Player p : PlayerHandler.players) {
		for (Player p : Server.getRegionManager().getLocalPlayers(c)) {
			if (p == null || p.isDead()) {
				continue;
			}
			if (MeleeRequirements.sameTeam(c, p)) {
				continue;
			}
			// if (c.wildLevel > 0) { //attacking only wildy players if this
			// player is in wild
			// p.sendMessage("wildLevel "+p.wildLevel+" inmulti:"+p.inMulti()+"
			// underattackby:"+p.underAttackBy);
			if (c.wildLevel > 0 && p.wildLevel < 1) {
				continue;
			}
			if (!p.inMulti() && !p.inFunPk() && c.underAttackBy != p.getId()) {
				continue;
			}
			if (PlayerConstants.goodDistance(c.getX(), c.getY(), p.getX(),
					p.getY(), 1)) {
				int damage = Misc.random(totalDamage);
				// final Client c2 = (Client) p;
				damage = p.dealDamage(new Hit(damage, 0, -1, c));

				p.getDamageMap().incrementTotalDamage(c.mySQLIndex,
						damage);

				p.getPA().closeAllWindows();
			}
			// }
		}
	}

	public void applyWrath() {
		int totalDamage = c.getSkills().getPrayerPoints() / 4;
		
		c.startGraphic(Graphic.create(2259, GraphicType.LOW));
		// for (Player p : PlayerHandler.players) {
		for (Player p : Server.getRegionManager().getLocalPlayers(c)) {
			if (p == null) {
				continue;
			}
			if (p.isDead()) {
				continue;
			}
			if (MeleeRequirements.sameTeam(c, p)) {
				continue;
			}
			// if (c.wildLevel > 0) { //attacking only wildy players if this
			// player is in wild
			if (c.wildLevel > 0 && p.wildLevel < 1) {
				continue;
			}
			if (!p.inMulti() && !p.inFunPk() && c.underAttackBy != p.getId()) {
				continue;
			}
			if (PlayerConstants.goodDistance(c.getX(), c.getY(), p.getX(),
					p.getY(), 2)) {
				int damage = Misc.random(totalDamage);
				// final Client c2 = (Client) p;
				p.startGraphic(Graphic.create(2260, GraphicType.LOW));
//				c.getPacketSender().createProjectile(c.getX(), c.getY(),
//						(c.getX() - p.getX()) * -1, (c.getY() - p.getY()) * -1,
//						50, 50, 2261, 25, 25, -p.getId() - 1, 0);
				Projectile proj = new Projectile(2261, c.getCurrentLocation(), p.getCurrentLocation());
				proj.setStartHeight(25);
				proj.setEndHeight(25);
				proj.setStartDelay(0);
				proj.setEndDelay(50);
				proj.setLockon(p.getProjectileLockon());
				GlobalPackets.createProjectile(proj);
				
				damage = p.dealDamage(new Hit(damage, 0, -1, c));

				p.getDamageMap().incrementTotalDamage(c.mySQLIndex,
						damage);

				p.getPA().closeAllWindows();
			}
			// }
		}
	}

	public boolean assignAutocast(int button) {
		SpellsData spellsData = SpellsData.SPELLS.get(button);
		if (spellsData == null) {
			return false;
		}

		if (c.getAutoCastSpell() != null) {
			switch (c.getAutoCastSpell()) {
				case TRIDENT_OF_THE_SEA:
				case TRIDENT_OF_THE_SWAMP:
				case DAWNBRINGER:
				case SANGUINESTI_STAFF:
					c.sendMessage("The magic of " + ItemDefinition.getName(c.playerEquipment[PlayerConstants.playerWeapon]) + " doesn't allow you to autocast any other spells.");
					c.getPacketSender().updateInterfaceEmpty(5);
					return true;
				default:
					break;
			}
		}

		if (!spellsData.canAutoCast()) {
			c.sendMessage("You can't autocast this.");
			return true;
		}

		if (c.duelRule[DuelRules.NO_MAGIC]) {
			c.sendMessage("Magic has been disabled in this duel!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			if (c.getAutoCastSpell() != null) {
				c.getPA().resetAutocast();
			}

			return true;
		}

		if (spellsData.getSpellBook() != SpellBook.ALL && spellsData.getSpellBook() != c.playerMagicBook) {
			c.sendMessage("Wrong spellbook.");
			return true;
		}

		c.getPacketSender().setConfig(43, -1);
		c.setAutoCastSpell(spellsData);
		return true;
	}

	public int bankingCount() {
		int total = 0;
		for (int bankingItem : c.bankingItems) {
			if (bankingItem > 0) {
				total++;
			}
		}
		return total;
	}

	public int blueWildyKey() {
		return PlayerConstants.blueWildyKey[(int) (Math.random()
				* PlayerConstants.blueWildyKey.length)];
	}

	public void boostStat(int skillID, boolean sup) {
		c.getSkills().updateLevel(skillID, getBoostStat(skillID, sup));
	}

	public void boostStat1(int skillID, boolean sup) {
		c.getSkills().updateLevel(skillID, getBoostStat1(skillID, sup));
	}

	public int calculateTotalBankItems() {
		int count = 0;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {

			if (c.bankItems0[i] != 0) {
				count++;
			}

			if (c.bankItems1[i] != 0) {
				count++;
			}

			if (c.bankItems2[i] != 0) {
				count++;
			}

			if (c.bankItems3[i] != 0) {
				count++;
			}

			if (c.bankItems4[i] != 0) {
				count++;
			}

			if (c.bankItems5[i] != 0) {
				count++;
			}

			if (c.bankItems6[i] != 0) {
				count++;
			}

			if (c.bankItems7[i] != 0) {
				count++;
			}

			if (c.bankItems8[i] != 0) {
				count++;
			}

		}
		return count;
	}

	public void calculateWeight() {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		c.getOutStream().createPacket(26);
		c.getOutStream().writeByte(1);
		c.flushOutStream();
	}

	public boolean canTeleport(TeleportType teleportType) {
		if (Config.isOwner(c)) {
			return true;
		}

		if (c.teleTimer > 0) {
			return false;
		}
		
		if (!c.specAndTeleTimer.complete()) {
			c.sendMessage("You must wait 10 seconds to teleport after using a special attack.");
			return false;
		}
		
		if (c.isTeleBlocked()) {
			c.sendMessage("You are teleblocked and can't teleport.");
			c.getPA().closeAllWindows();
			return false;
		}
		
		if (teleportType == TeleportType.OBELISK) // i think only rule for obelisk is to not be teleblocked.
			return true;
		
		if (c.inLMSArenaCoords() || c.inLMSWaitingRoom()) {
			c.sendMessage("You cannot teleport out of Last Man Standing.");
			return false;
		}

		if (teleportType != TeleportType.DUNG_GATESTONE && c.insideDungeoneering()) {
			c.sendMessage("You cannot teleport out of Dungeoneering.");
			return false;
		}

		if (c.tobManager != null && c.tobManager.started()) {
			c.sendMessage("You can't teleport out of the Theatre once the show has started.");
			return false;
		}

		if (c.insideInstance()) {
			return c.getInstanceManager().canTeleport(c);
		}

		if (!BarbarianAssault.canTeleport(c)) {
			c.sendMessage("You can't teleport here.");
			return false;
		}
		
		if (SoulWars.cantTeleport(c)) {
			c.sendMessage("You can't teleport from Soulwars.");
			return false;
		}

		if (c.teleportToX > 0 || c.teleportToY > 0) {
			return false;
		}

		if (c.inClanWars()) {
			c.sendMessage("You cannot teleport from here.");
			c.getPA().closeAllWindows();
			return false;
		}
		
		if (SoulWars.cantTeleport(c)) {
			c.sendMessage("You can't teleport from Soulwars.");
			return false;
		}

		if (c.isTeleCooldown()) {
			c.sendMessage(
					"Your teleport is on cooldown after teleporting from Wilderness.");
			c.getPA().closeAllWindows();
			return false;
		}

		if (c.inEasterEvent()) {
			c.sendMessage("You cannot tele from Easter Event!");
			c.getPA().closeAllWindows();
			return false;
		}

		if (c.performingAction) {
			c.sendMessage("Can't teleport yet.");
			return false;
		}

		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			return false;
		}

		if (c.inCw() || c.inCwWait()) {
			c.sendMessage("You cannot teleport here.");
			c.getPA().closeAllWindows();
			return false;
		}

		if (c.getAgility().doingAgility) {
			c.sendMessage("You can't teleporty while doing agility.");
			return false;
		}
		if (c.inTrade) {
			c.sendMessage("You can't teleport while in trade.");
			return false;
		}
		if (c.duelStatus == 5) {
			c.sendMessage("You can't teleport during a duel!");
			return false;
		}
		if (c.isInJail()) {
			c.sendMessage("You cannot teleport out of jail.");
			return false;
		}
		if (c.getItems().playerHasEquippedWeapon(MagicalCrate.MAGICAL_CRATE_ITEM_ID) && c.inWild()) {
			c.sendMessage("You are not able to teleport from wilderness while carrying this crate.");
			return false;
		}
//		if (c.revenantDungeon()) {
//			c.sendMessage("A magical force stops you from teleporting outside the revenant caves!");
//			return false;
//		}

		if (c.inWild() && c.wildLevel > Config.NO_TELEPORT_WILD_LEVEL
				&& teleportType != TeleportType.GLORY && teleportType != TeleportType.ROYAL_SEED_POD) {
			c.sendMessage("You can't teleport above level "
					+ Config.NO_TELEPORT_WILD_LEVEL + " in the wilderness.");
			c.getPA().closeAllWindows();
			return false;
		}

		if ((teleportType == TeleportType.GLORY || teleportType == TeleportType.ROYAL_SEED_POD) && c.wildLevel > 30) {
			c.sendMessage("You can't teleport above level 30 in the wilderness with this.");
			c.getPA().closeAllWindows();
			return false;
		}
		
		if (c.getZones().isInDmmTourn() || c.getZones().isInDmmLobby() || FunPkTournament.inFunPKTournament(c.getName())) {
			c.sendMessage("You are not allowed to teleport out of the Tournament.");
			c.getPA().closeAllWindows();
			return false;
		}

		if (c.raidsManager != null) {
			c.sendMessage("You are not allowed to teleport out of the raids.");
			c.getPA().closeAllWindows();
			return false;
		}

		return true;
	}

	public boolean canWalkDir(int moveX, int moveY) {
		if (RegionClip.getClippingDirection(c.getX() + moveX, c.getY() + moveY,
				c.heightLevel, moveX, moveY, c.getDynamicRegionClip())) {
			return true;
		}
		return false;
	}

	public static final Animation VENG_ANIM = new Animation(4410);
	public static final Graphic VENG_GFX = Graphic.create(726, GraphicType.HIGH);
	private static final Requirement VENG_REQ = new Requirement(new GameItem(9075, 4), new GameItem(560, 2), new GameItem(557, 10), null, 94);

	/*
	 * Vengeance
	 */
	public void castVeng(boolean usingOrb) {
		if (c.getSkills().getLevel(Skills.DEFENSE) < 40) {
			c.sendMessage("You need a defence level of 40 to cast this spell.");
			return;
		}

		if (c.playerMagicBook != SpellBook.LUNARS && !usingOrb) {
			return;
		}

		/*if (c.vengOn) {
			c.sendMessage("You already have vengeance casted.");
			return;
		}*/

		if (c.duelRule[DuelRules.NO_MAGIC] || c.clanWarRule[CWRules.NO_MAGIC]) {
			c.sendMessage(
					"You can't cast this spell because magic has been disabled.");
			return;
		}

		if (usingOrb) {
			if (!c.vengDelay.complete()) {
				c.sendMessage("You can only cast vengeance every 30 seconds.");
				return;
			}

			if (!c.getItems().deleteItem2(6950, 1)) {
				return;
			}
		} else {

			if (!c.vengDelay.complete()) {
				c.sendMessage("You can only cast vengeance every 30 seconds.");
				return;
			}
			
			if (!MagicRequirements.checkMagicReqsNew(c, VENG_REQ, SpellsData.VENG, true)) {
				return;
			}
		}

		c.vengDelay.startTimer(50);
		c.startAnimation(VENG_ANIM);
		c.startGraphic(VENG_GFX);
		addSkillXP(112, Skills.MAGIC);
		c.getPacketSender().sendWidget(DisplayTimerType.VENGEANCE, 50);
		c.vengOn = true;
	}

	//not safe, if player walks out on perfect timing outside of the 16x16 area then returns, object can be incorrect position
	public void changeObjectForNearby(int objectId, int objectX, int objectY,
			int objectZ, int face, int objectType) {
		for (Player p : Server.getRegionManager()
				.getLocalPlayersByLocation(objectX, objectY, objectZ)) {
			if (p == null || p.disconnected || !p.isActive) {
				continue;
			}
			p.getPacketSender().addObjectPacket(objectId, objectX, objectY, objectType, face);
		}
	}

	public void changeSpellBookToAncient() {
		changeSpellBook(SpellBook.ANCIENT);
	}

	public void changeSpellBookToLunar() {
		changeSpellBook(SpellBook.LUNARS);
	}
	
	public void changeSpellBook(SpellBook book) {
		if (c.playerMagicBook == book)
			return;
		
		c.setSpellbook(book);
		if (c.playerMagicBook == SpellBook.LUNARS)
			c.sendMessage("You switch spellbook to lunar magic.");
		else if (c.playerMagicBook == SpellBook.ANCIENT)
			c.sendMessage("You switch spellbook to Ancient magic.");
		else if (c.playerMagicBook == SpellBook.NORMAL)
			c.sendMessage("You switch spellbook to Normal magic.");
		c.getPA().resetAutocast();
		c.dialogueAction = 0;
		c.getPA().closeAllWindows();
	}

	public int CheckBankSlots(int tab) {
		int totalItemsAfter = 0;
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			if (tab == 0) {
				if (c.bankItems0[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 1) {
				if (c.bankItems1[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 2) {
				if (c.bankItems2[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 3) {
				if (c.bankItems3[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 4) {
				if (c.bankItems4[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 5) {
				if (c.bankItems5[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 6) {
				if (c.bankItems6[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 7) {
				if (c.bankItems7[i] != 0) {
					totalItemsAfter++;
				}
			} else if (tab == 8) {
				if (c.bankItems8[i] != 0) {
					totalItemsAfter++;
				}
			}
		}
		return totalItemsAfter;
	}

	public boolean checkEmpty(int[] array) {
		for (int element : array) {
			if (element != 0) {
				return false;
			}
		}
		return true;
	}

	public boolean checkForFlags() {
		int[][] itemsToCheck = {{995, 100000000}, {35, 5}, {667, 5}, {2402, 5},
				{746, 5}, {4151, 150}, {565, 100000}, {560, 100000},
				{555, 300000}, {11235, 10}};
		for (int[] element : itemsToCheck) {
			if (element[1] < c.getItems().getTotalCount(element[0])) {
				return true;
			}
		}
		return false;
	}

	public boolean checkForMax(GameItem item) {
		long addamount = item.getAmount();
		long invamount = c.getItems().getItemAmount(item.getId());
		if ((invamount + addamount) > Integer.MAX_VALUE) {
			long difference = ((invamount + addamount) - Integer.MAX_VALUE);
			GameItem dropItem = new GameItem(item.getId(), (int) difference);
			c.sendMessage(
					"Your itemstack for " + ItemProjectInsanity.getItemName(dropItem.getId())
							+ " exceeded maximum capacity.");
			c.sendMessage("The remaining " + dropItem.getAmount()
					+ " have been dropped on the floor.");
			c.getItems().addItem(item.getId(), (int) addamount);
			if (c.teleportToX > 0) {
				ItemHandler.createGroundItem(c, dropItem.getId(), c.teleportToX,
						c.teleportToY, c.teleportToZ, dropItem.getAmount());
			} else {
				ItemHandler.createGroundItem(c, dropItem.getId(), c.getX(),
						c.getY(), c.getHeightLevel(), dropItem.getAmount());
			}
			return true;
		}
		return false;
	}

	public boolean checkForMax(GroundItem item, boolean controller) {
		long addamount = item.getItemAmount();
		long invamount = c.getItems().getItemAmount(item.getItemId());
		if ((invamount + addamount) > Integer.MAX_VALUE) {
			long difference = (invamount + addamount) - Integer.MAX_VALUE;
			final GameItem dropItem = new GameItem(item.getItemId(),
					(int) difference);
			c.sendMessage(
					"Your itemstack for " + ItemProjectInsanity.getItemName(dropItem.getId())
							+ " exceeded maximum capacity.");
			c.sendMessage("The remaining " + dropItem.getAmount()
					+ " have been dropped on the floor.");
			c.getItems().addItem(item.getItemId(), (int) addamount);
			// TradeLog.pickedupItem(c, Item.getItemName(item.getItemId()),
			// (int) addamount, item.getOwnerName());// logging drops
			if (controller) {
				ItemHandler.removeControllersItem(item, c, item.getItemId(),
						item.getItemX(), item.getItemY(), (int) difference);
			} else {
				ItemHandler.removeControllersItem(item, c, item.getItemId(),
						item.getItemX(), item.getItemY(), (int) difference);
			}
			Server.getTaskScheduler().schedule(new Task(false) {

				@Override
				protected void execute() {
					this.stop();
					if (c.teleportToX > 0) {
						ItemHandler.createGroundItem(c, dropItem.getId(), c.teleportToX,
								c.teleportToY, c.teleportToZ, dropItem.getAmount());
					} else {
						ItemHandler.createGroundItem(c, dropItem.getId(), c.getX(),
								c.getY(), c.getHeightLevel(), dropItem.getAmount());
					}
				}
			});
			
			if (DBConfig.LOG_PICKUP) {
				if (!item.getOwnerName().equals("server") && !item.isInMinigame()) {
					Server.getLogWriter().addToPickedUpList(new PickedUpItem(c,
							new GroundItem(item.getItemId(), item.getItemX(),
									item.getItemY(), item.getItemZ(),
									item.getItemAmount(), item.getItemController(),
									item.hideTicks, item.getOwnerName(),
									item.getOriginalOwnerMID(),
									item.isIronManModeItem())));
				}
			}

			return true;
		}
		return false;
	}

	public boolean checkForPlayer(int x, int y) {
		// for (Player p : PlayerHandler.players) {
		for (Player p : c.getRegion().getPlayers()) {
			if (p != null) {
				if (p.getX() == x && p.getY() == y) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkIfCanHit() {
		if (c.playerIndex <= 0) {
			return false;
		}
		if (PlayerHandler.players[c.playerIndex] == null
				|| PlayerHandler.players[c.playerIndex].forceDisconnect
				|| PlayerHandler.players[c.playerIndex].isDead()) {
			c.getCombat().resetPlayerAttack();
			return false;
		}

		if (PlayerHandler.players[c.playerIndex].getZ() != c.getZ()) {
			c.getCombat().resetPlayerAttack();
			return false;
		}

		if (PlayerHandler.players[c.playerIndex].distanceToPoint(c.getX(),
				c.getY()) > 20) {
			c.getCombat().resetPlayerAttack();
			return false;
		}

		final int combatDistance = CombatFormulas.getRangeDistReq(c); // melee is 1,
																	// rest is
																	// range/magic

		if (combatDistance == 1 && !c.getMovement().canMove()
				&& (c.getX() != PlayerHandler.players[c.playerIndex].getX()
						&& c.getY() != PlayerHandler.players[c.playerIndex]
								.getY())) { // stop diagonal hits
			c.blockedShot = true;
			// c.sendMess("Diagonal Hit!"+c.isMoving());
			return false;
		}

		if (RegionClip.rayTraceBlocked(c.getX(), c.getY(),
				PlayerHandler.players[c.playerIndex].getX(),
				PlayerHandler.players[c.playerIndex].getY(), c.getHeightLevel(),
				combatDistance > 1, c.getDynamicRegionClip())) {
			c.blockedShot = true;
			return false;
		}

		return true;

	}
	
	public boolean checkIfCanHitNpc() {
		int npcIndex = 0;
		if (c.npcIndex > 0)
			npcIndex = c.npcIndex;
		else if (c.followId2 > 0)
			npcIndex = c.followId2;
		if (npcIndex <= 0) {
			return false;
		}
		if (NPCHandler.npcs[npcIndex] == null
				|| NPCHandler.npcs[npcIndex].isDead() || NPCHandler.npcs[npcIndex].getSkills().getLifepoints() <= 0) {
			c.getCombat().resetPlayerAttack();
			if (c.followId2 > 0)
				c.getPA().resetFollow();
			return false;
		}
		
		NPC npc = NPCHandler.npcs[npcIndex];

		if (npc.getHeightLevel() != c.getZ()) {
			c.getCombat().resetPlayerAttack();
			return false;
		}

		if (c.distanceToPoint(npc.getX(),
				npc.getY()) > 20) {
			c.getCombat().resetPlayerAttack();
			return false;
		}

		final int combatDistance = CombatFormulas.getRangeDistReq(c); // melee is 1,
																	// rest is
																	// range/magic

//		if (combatDistance == 1) {
//			NPC n = npc;
//			if (!c.getMovement().isMovingOrHasWalkQueue() && !((c.getX() >= n.getX() - n.getSize() && c.getX() <= n.getX() + n.getSize()) || (c.getY() >= n.getY() - n.getSize() && c.getY() <= n.getY() + n.getSize()))) { // diagonal
//				c.blockedShot = true;
////				c.sendMess("Diagonal Hit!"+c.isMoving());
//				return false;
//			}
//		}

		if (RegionClip.rayTraceBlocked(c.getX(), c.getY(),
				npc.getXtoPlayerLoc(c.getX()),
				npc.getYtoPlayerLoc(c.getY()), c.getHeightLevel(),
				combatDistance > 1, c.getDynamicRegionClip())) {
			c.blockedShot = true;
			return false;
		}

		return true;

	}

	/*
	 * public void sendCrashFrame() { // used for crashing cheat clients
	 * synchronized (c) { if (c.getOutStream() != null && c != null &&
	 * c.isActive) { c.getOutStream().createPacket(123); c.flushOutStream(); } }
	 * }
	 */

	public void checkPouch(int i) {
		if (i < 0) {
			return;
		}
		c.sendMessage("This pouch has " + c.pouches[i] + " rune ess in it.");
	}

	public void clearClanChat() {
		c.clanId = -1;
		c.getPacketSender().sendString("Talking in: ", 18139);
		c.getPacketSender().sendString("Owner: ", 18140);
		for (int j = 18144; j < 18244; j++) {
			c.getPacketSender().sendString("", j);
		}
	}

	public void clearNpcDamage() {
		for (NPC n : Server.getRegionManager().getLocalNpcs(c)) {
			if (n == null || n.isDead()) {
				continue;
			}
			n.removeInflictor(c.mySQLIndex);
		}
	}

	public void closeChatInterface() {
		c.getPacketSender().sendInputDialogState(0);
	}
	
	/**
	 * Close all windows and reset everything like duels/trades/etc
	 */
	public void closeAllWindows() {
		if (c.isBot()) {
			return;
		}

		if (c.insideDungeoneering() && !c.dungParty.dungManager.loaded) {
			return;
		}

		c.getPacketSender().closeInterfaces();
			if (c.isBanking) {
				c.refreshBank = true;
				c.isBanking = false;
				c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
			}
			if (c.openBankPinInterface) {
				c.openBankPinInterface = false;
			}
			if (c.getChatInterfaceId() > 0)
				c.setChatInterfaceOpen(0);
			
			if (c.bankPinOpensBank)
				c.bankPinOpensBank = false;
			if (c.isBobOpen) {
				c.isBobOpen = false;
			}
			if (c.tradeStatus >= 1 || c.inTrade) {
				c.getTradeAndDuel().declineTrade();
			}
			if (c.duelStatus >= 1 && c.duelStatus <= 4) {
				c.getTradeAndDuel().declineDuel();
			}
			if (c.myShopClient != null) {
				c.myShopClient = null;
				c.getPacketSender().sendConfig(ConfigCodes.CHANGE_BUY_1, 0);
			}

			if (c.getDuelingClanWarClient() != null) {
				ClanDuel.declineStake(c, c.getDuelingClanWarClient());
			}

			if (c.firstPinEnter) {
				c.getBankPin().falseButtons();
			}
			if (c.takeAsNote) {
				c.getPA().toggleNote(false);
			}
			if (c.dialogueAction > 0) {
				c.dialogueAction = 0;
			}
			if (c.teleAction > 0) {
				c.teleAction = 0;
			}
			if (c.interactingObject != null) {
				c.interactingObject = null;
			}

			if (c.chalAccepted) {
				c.chalAccepted = false;
			}

			if (c.gambleStage != GambleStage.NONE) {
				GamblingManager.hardClose(c);
			}

			if (c.runRecoverBoost != 0) {
				c.runRecoverBoost = 0;
			}
			
			if (c.getPOH().currentHotspot.isPresent()) {
			    c.getPOH().currentHotspot = Optional.empty();
			}

			if (!c.getMarketOffer().getFilteredBuyOffers().isEmpty()) {
				c.getMarketOffer().getFilteredBuyOffers().clear();
				c.getMarketOffer().setBuyOfferItemId(0);
			}
			
			if (c.getClaimItems().isActive())
				c.getClaimItems().close();
			
			if (c.getMarketOffer().isActive())
				c.getMarketOffer().close();
			
			if (c.getPartyOffer().isOpen())
				c.getPartyOffer().close();
			
			if (c.getStorageObject().isOpen())
				c.getStorageObject().close();
			
			resetInterfaceInfo();
	}

	public void crabSpecBonus(int stat, boolean sup) {
		boostStat1(stat, sup);
	}

	// creates gfx for everyone
	public void createPlayersStillGfx(int id, int x, int y, int height,
			int time) {
		// synchronized(c) {
		/*
		 * for (int i = 0; i < Config.MAX_PLAYERS; i++) { Player p =
		 * PlayerHandler.players[i]; if (p != null) { Client person = (Client)
		 * p;
		 */

		// for (Player player : Server.getRegionManager().getLocalPlayers(c)) {
		// final Client person = (Client) player;
		//
		// if (person != null) {
		// if (person.getOutStream() != null && person.isActive) {
		// // if (person.distanceToPoint(x, y) <= 25) {
		// person.getPA().stillGfx(id, x, y, height, time);
		// // }
		// }
		// }
		// }

		c.getPA().stillGfx(id, x, y, height, time);

		// }
	}

	public void destroyItem(int itemId, int amount, Runnable runnable, Runnable postClick, String header, String... info) {
		if (info.length <= 0 || info.length > 2 || itemId >= ItemDefinition.getDefinitions().size() || itemId < 0 || amount < 0 || amount > Integer.MAX_VALUE) {
			return;
		}

		c.getPacketSender().displayItemOnInterface(14171, itemId, 0, amount);
		c.getPacketSender()
				.sendString(header, 14174);
		c.getPacketSender().sendString("Yes.", 14175);
		c.getPacketSender().sendString("No.", 14176);
		if (info.length > 1) {
			c.getPacketSender().sendString("", 14177);
			c.getPacketSender().sendString(info[0], 14182);
			c.getPacketSender().sendString(info[1], 14183);
		} else {
			c.getPacketSender().sendString(info[0], 14177);
			c.getPacketSender().sendString("", 14182);
			c.getPacketSender().sendString("", 14183);
		}
		c.getPacketSender().sendString(ItemProjectInsanity.getItemName(itemId), 14184);
		c.getPacketSender().sendChatInterface(14170);
		c.itemDestroyCode = runnable;
		c.itemDestroyPostClick = postClick;
	}

	public void displayReward(int item, int amount, int item2, int amount2,
			int item3, int amount3) {
		int[] items = {item, item2, item3};
		int[] amounts = {amount, amount2, amount3};
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6963);
		c.getOutStream().writeShort(items.length);
		for (int i = 0; i < items.length; i++) {
			if (c.playerItemsN[i] > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(amounts[i]);
			} else {
				c.getOutStream().writeByte(amounts[i]);
			}
			if (items[i] > 0) {
				c.getOutStream().write3Byte(items[i] + 1);
			} else {
				c.getOutStream().write3Byte(0);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		c.getItems().addItem(item, amount);
		c.getItems().addItem(item2, amount2);
		c.getItems().addItem(item3, amount3);
		c.getPacketSender().showInterface(6960);
	}

	public int donatorRitem() {
		return PlayerConstants.donatorRitem[(int) (Math.random()
				* PlayerConstants.donatorRitem.length)];
	}

	public void emptyPouch(int i) {
		if (i < 0) {
			return;
		}
		int toAdd = c.pouches[i];
		if (toAdd > c.getItems().freeSlots()) {
			toAdd = c.getItems().freeSlots();
		}
		if (toAdd > 0) {
			c.getItems().addItem(1436, toAdd);
			c.pouches[i] -= toAdd;
		}
	}

	public int equipAmount() {
		int count = 0;
		for (int element : c.playerEquipment) {
			if (element > 0) {
				count++;
			}
		}
		return count;
	}

	public void fillPouch(int i) {
		if (i < 0) {
			return;
		}
		int toAdd = c.POUCH_SIZE[i] - c.pouches[i];
		if (toAdd > c.getItems().getItemAmount(1436)) {
			toAdd = c.getItems().getItemAmount(1436);
		}
		if (toAdd > c.POUCH_SIZE[i] - c.pouches[i]) {
			toAdd = c.POUCH_SIZE[i] - c.pouches[i];
		}
		if (toAdd > 0) {
			c.getItems().deleteItemInOneSlot(1436, toAdd);
			c.pouches[i] += toAdd;
		}
	}

	public void FindItemKeptInfo(Client c) {
		if (c.isSkulled && (c.playerPrayerBook == PrayerBook.NORMAL
				? c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
				: c.curseActive[Curses.PROTECT_ITEM.ordinal()])) {
			ItemKeptInfo(1);
		} else if (!c.isSkulled && (c.playerPrayerBook == PrayerBook.NORMAL
				? !c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
				: !c.curseActive[Curses.PROTECT_ITEM.ordinal()])) {
			ItemKeptInfo(3);
		} else if (!c.isSkulled && (c.playerPrayerBook == PrayerBook.NORMAL
				? c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
				: c.curseActive[Curses.PROTECT_ITEM.ordinal()])) {
			ItemKeptInfo(4);
		}
	}

	public void findRandomPker() {
		if (c.hidePlayer || c.underAttackBy > 0) {
			return;
		}
		/*if (equipAmount() < 5) {
			return;
		}*/
		ArrayList<Integer> players = new ArrayList<>();
		for (Player p : PlayerHandler.players) {
			if (p == null) {
				continue;
			}
			if (p.disconnected) {
				continue;
			}
			if (p.hidePlayer) {
				continue;
			}
			if (p.wildLevel < 1) {
				continue;
			}
			if (p.huntPlayerIndex > 0) {
				continue;
			}
			if (p.getId() == c.getId()) {
				continue;
			}
			// if (p.underAttackBy > 0)
			// continue;
			if (p.playerIndex > 0 && p.playerIndex != c.getId()) {
				continue;
			}
			if (c.UUID.equals(p.UUID)) {
				continue;
			}
			int combatDif1 = c.getCombat().getCombatDifference(c.combatLevel,
					p.combatLevel);
			if ((combatDif1 > c.wildLevel || combatDif1 > p.wildLevel)) {
				continue;
			}
			if (!PlayerConstants.goodDistance(p.getX(), p.getY(), c.getX(),
					c.getY(), 500)) {
				if (!p.getZones().isInRevDung())
					continue;
			}
			final Client o = (Client) p;
			if (/* !o.isBot() && */PlayerKilling.hostOnList(o, c.UUID)
					|| PlayerKilling.hostOnList(c, o.UUID)) {
				continue;
			}
			if (c.getItems().getTotalBonuses() < 250
					|| p.getItems().getTotalBonuses() < 250) {
				continue;
			}
			if (c.wildLevel > 0 && c.wildLevel <= 10) {
			    if (o.isBot() && (c.getCombat().getBonusDifference(o) < -600
				     || c.getCombat().getBonusDifference(o) > 600)) {
				     continue;
			    }
			    if (c.isBot() && (c.getCombat().getBonusDifference(o) < -600
					 || c.getCombat().getBonusDifference(o) > 600)) {
				     continue;
			    }
			}
			if (c.isBot() && !o.isBot()) {
				continue;
			}
			if (o.isBot() && !c.isBot()) {
				continue;
			}
			players.add(o.getId());
			// c.sendMessage("Added "+o.getName());
		}
		if (players.size() > 0) {
			int index = players.get(Misc.random(players.size() - 1));
			players.clear();

			final Client o = (Client) PlayerHandler.players[index];
			if (o != null && o.wildLevel > 0 && c.wildLevel > 0) {
				c.huntedBy.add(o.getId());
				// if (Player.goodDistance(c.getX(), c.getY(), o.getX(),
				// o.getY(), 15))
				// o.getPA().createPlayerHints(10, c.getId());
				// else
				// o.getPA().createObjectHints(c.getX(), c.getY(),
				// c.heightLevel, 120);
				// o.requestUpdates();
				o.sendMessage("<col=ff0000>The hunt is on!");
				o.sendMessage("Your next hit is " + c.getNameSmartUp() + " in level " + c.wildLevel + " Wilderness");
				o.sendMessage("You have 7 minutes to find and kill you target.");
				o.huntPlayerIndex = c.getId();
				o.setWildernessTick(0);
				// sendFrame126("Target: "+c.getName(), 29174);
				if (WorldType.equalsType(WorldType.SPAWN)) {
					o.getPacketSender().sendFrame126(
							"<col=ff7000>Current Target: <col=ffb000>" + c.getNameSmartUp(), 16040);
				} else {
					o.getPacketSender().sendFrame126(
							"<col=ff7000>Current Target: <col=ffb000>" + c.getNameSmartUp(), 16046);
				}

				// add the target to the player too if he doesn't have target
				if (c.huntPlayerIndex < 1) {
					o.huntedBy.add(c.getId());
					// if (Player.goodDistance(c.getX(), c.getY(), o.getX(),
					// o.getY(), 15))
					// o.getPA().createPlayerHints(10, c.getId());
					// else
					// o.getPA().createObjectHints(c.getX(), c.getY(),
					// c.heightLevel, 120);
					// o.requestUpdates();
					c.sendMessage("<col=ff0000>The hunt is on!");
					c.sendMessage("Your next hit is " + o.getNameSmartUp() + " in level " + o.wildLevel + " Wilderness");
					c.sendMessage(
							"You have 7 minutes to find and kill you target.");
					c.huntPlayerIndex = o.getId();
					c.setWildernessTick(0);
					// sendFrame126("Target: "+o.getName(), 29174);
					if (WorldType.equalsType(WorldType.SPAWN)) {
						c.getPacketSender().sendFrame126(
								"<col=ff7000>Current Target: <col=ffb000>" + o.getNameSmartUp(),
								16040);
					} else {
						c.getPacketSender().sendFrame126(
								"<col=ff7000>Current Target: <col=ffb000>" + o.getNameSmartUp(),
								16046);
					}
				}
			}
		}
	}
	
	public boolean withinDistanceToNpc(NPC n, int distance) {
		return c.withinDistance(n.getXYtoPlayerXY(c.getX(), n.getX()), n.getXYtoPlayerXY(c.getY(), n.getY()), c.getZ(), distance);
	}

	public void followNpc() {
		if (NPCHandler.npcs[c.followId2] == null
				|| NPCHandler.npcs[c.followId2].isDead()) {
			resetFollow();
			return;
		}
		NPC npc = NPCHandler.npcs[c.followId2];
		if (c.getFreezeTimer() > 0) {
			return;
		}
		if (c.isDead() || c.getPlayerLevel()[3] <= 0) {
			resetFollow();
			return;
		}

		int otherX = npc.getX();
		int otherY = npc.getY();

		if (npc.getFacingLocation() != null) {
			otherX = (npc.getFacingLocation().getX() - 1) / 2;
			otherY = (npc.getFacingLocation().getY() - 1) / 2;
		}

		if (!PlayerConstants.goodDistance(otherX, otherY, c.getX(), c.getY(),
				25)) {
			c.getPA().resetFollow();
			return;
		}

		boolean attackingFollow = c.npcIndex > 0;


		// my new code
		if (attackingFollow) {
			if (/*distanceReq > 1 && */!c.blockedShot
					&& c.withinDistanceToStopWalking) {
				if (npc.getXtoPlayerLoc(c.getX()) == c.absX
						&& npc.getYtoPlayerLoc(c.getY()) == c.absY) {
					c.getPA().stepAway();
				} else {
					c.getMovement().hardResetWalkingQueue();
				}
				return;
			}
		}

		// c.faceUpdate(c.followId2);
		if (!attackingFollow)
			if (otherX == c.absX && otherY == c.absY)
				c.getPA().stepAway();

		PathFinder.findRoute(c, otherX, otherY, true, 1, 1, c.entityClickSizeX, c.entityClickSizeY, c.entityWalkingFlag, 0, 0, c.clickReachDistance);
		
	}

	/**
	 * Following
	 **/

	public void followPlayer() {
		
		Client other = (Client) PlayerHandler.players[c.followId];
		
		if (other == null || other.isDead()) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}
		
		if (c.getFreezeTimer() > 0) {
			c.followId = 0;
			return;
		}

		if (c.duelRule[DuelRules.NO_MOVEMENT] && c.duelStatus == 5) {
			c.followId = 0;
			return;
		}

		if (c.isStunned()) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.performingAction) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (inPitsWait()) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.getHeightLevel() != other.getHeightLevel()) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.isDead() || c.getSkills().getLifepoints() <= 0) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (other.inWild() && !c.inWild()) {
			c.sendMessage(
					"Watch out, the other player is entering the wilderness!");
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}
		
		//don't run this if other player didn't move.
//		if (Location.equals(c.followLoc, other.getCurrentLocation())) {
//			return;
//		}
//		c.followLoc.copyLocation(other.getCurrentLocation());
		
		int otherX = other.getX();
		int otherY = other.getY();

		if (!PlayerConstants.goodDistance(otherX, otherY, c.getX(), c.getY(),
				25)) {
			resetFollow();
			c.getMovement().resetWalkingQueue();
			return;
		}

		boolean attackingPlayer = false;

		if (c.playerIndex > 0) {
			attackingPlayer = true;
		}

		if (other.getX() == other.getLastX() && other.getY() == other.getLastY()) {
			attackingPlayer = true; // if x is same as last x then to use combat
									// follow instead to not bug follow
		}

		if (!attackingPlayer) { // not attacking player? will cause follow
								// dancing XD
			/*if (otherX == c.absX && otherY == c.absY) {
				c.getPA().stepAway();
				c.face(other);
				return;
			}*/

			if (Misc.distanceToPoint(other.getX(), other.getY(), other.getLastX(), other.getLastY()) > 5) {
				otherX = other.getX();
				otherY = other.getY();
			} else {
				otherX = other.getLastX();
				otherY = other.getLastY();
			}

			PathFinder.findRoute(c, otherX, otherY, true, 1, 1);
			c.face(other);
			return;
		}

		boolean sameSpot = (c.absX == otherX && c.absY == otherY);

		c.face(other);

		int distanceReq = 1;

		if (attackingPlayer) {
			distanceReq = CombatFormulas.getRangeDistReq(c);
		}
		
		if (c.playerIndex > 0) {
			// range follow here
			if (distanceReq > 1 && !c.blockedShot && c.withinDistanceToStopWalking) {
				if (otherX == c.absX && otherY == c.absY) {
					c.getPA().stepAway();
				} else {
					c.getMovement().resetWalkingQueue();
				}
				return;
			}
		}

		if (c.getCombat().usingHally() && PlayerConstants.goodDistance(otherX, otherY,
				c.getX(), c.getY(), 2) && !sameSpot) {
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (otherX == c.absX && otherY == c.absY) {
			c.getPA().stepAway();
		} else if (attackingPlayer /* && dist < 5 *//* && !otherMoving */) {
			PathFinder.findRoute(c, otherX, otherY, true, 1, 1, 1, 1, 0, 0, 0, 1);
		} else if (c.isRunning) {
			if (otherY > c.getY() && otherX == c.getX()) {
				playerWalk(otherX, otherY - 1);
			} else if (otherY < c.getY() && otherX == c.getX()) {
				playerWalk(otherX, otherY + 1);
			} else if (otherX > c.getX() && otherY == c.getY()) {
				playerWalk(otherX - 1, otherY);
			} else if (otherX < c.getX() && otherY == c.getY()) {
				playerWalk(otherX + 1, otherY);
			} else if (otherX < c.getX() && otherY < c.getY()) {
				playerWalk(otherX + 1, otherY + 1);
			} else if (otherX > c.getX() && otherY > c.getY()) {
				playerWalk(otherX - 1, otherY - 1);
			} else if (otherX < c.getX() && otherY > c.getY()) {
				playerWalk(otherX + 1, otherY - 1);
			} else if (otherX > c.getX() && otherY < c.getY()) {
				playerWalk(otherX + 1, otherY - 1);
			}
		} else {
			if (otherY > c.getY() && otherX == c.getX()) {
				playerWalk(otherX, otherY - 1);
			} else if (otherY < c.getY() && otherX == c.getX()) {
				playerWalk(otherX, otherY + 1);
			} else if (otherX > c.getX() && otherY == c.getY()) {
				playerWalk(otherX - 1, otherY);
			} else if (otherX < c.getX() && otherY == c.getY()) {
				playerWalk(otherX + 1, otherY);
			} else if (otherX < c.getX() && otherY < c.getY()) {
				playerWalk(otherX + 1, otherY + 1);
			} else if (otherX > c.getX() && otherY > c.getY()) {
				playerWalk(otherX - 1, otherY - 1);
			} else if (otherX < c.getX() && otherY > c.getY()) {
				playerWalk(otherX + 1, otherY - 1);
			} else if (otherX > c.getX() && otherY < c.getY()) {
				playerWalk(otherX - 1, otherY + 1);
			}
		}

		c.face(other);
	}

	/**
	 * Reseting animations for everyone nearby
	 **/

	public void frame1() {
		// synchronized(c) {
		/*
		 * for (int i = 0; i < Config.MAX_PLAYERS; i++) { if
		 * (PlayerHandler.players[i] != null) { Client person = (Client)
		 * PlayerHandler.players[i];
		 */

		if (c.isBot()) {
			return;
		}
		if (c != null && c.getOutStream() != null && c.isActive) {
			c.getOutStream().createPacket(1);
			c.flushOutStream();
		}

		// for (Player player : Server.getRegionManager().getLocalPlayers(c)) {
		// final Client person = (Client) player;
		//
		// if (person != null) {
		// if (person.getOutStream() != null && !person.disconnected) {
		// if (person.isBot())
		// continue;
		// if (c.distanceToPoint(person.getX(), person.getY()) <= 25) {
		// person.getOutStream().createPacket(1);
		// person.flushOutStream();
		// person.requestUpdates();
		// }
		// }
		// }
		//
		// }
		// }
	}

	public int freeSlots() {
		int freeS = 0;
		for (int playerItem : c.playerItems) {
			if (playerItem <= 0) {
				freeS++;
			}
		}
		return freeS;
	}

	public boolean fullGuthans() {
		return c.playerEquipment[PlayerConstants.playerHat] == 4724
				&& c.playerEquipment[PlayerConstants.playerChest] == 4728
				&& c.playerEquipment[PlayerConstants.playerLegs] == 4730
				&& c.playerEquipment[PlayerConstants.playerWeapon] == 4726;
	}

	public boolean fullVeracsEffect() {
		return c.playerEquipment[PlayerConstants.playerHat] == 4753
				&& c.playerEquipment[PlayerConstants.playerChest] == 4757
				&& c.playerEquipment[PlayerConstants.playerLegs] == 4759
				&& c.playerEquipment[PlayerConstants.playerWeapon] == 4755
				&& (Misc.random(99) < 25);
	}

	public void gamesNeck() {
		c.getDH().sendOption4("Pest Control", "Castle Wars", "Warriors Guild",
				"Fight Cave");
		c.usingGamesNeck = true;
	}

	public int getBankItems(int tab) {
		int ta = 0, tb = 0, tc = 0, td = 0, te = 0, tf = 0, tg = 0, th = 0,
				ti = 0;
		for (int element : c.bankItems0) {
			if (element > 0) {
				ta++;
			}
		}
		for (int element : c.bankItems1) {
			if (element > 0) {
				tb++;
			}
		}
		for (int element : c.bankItems2) {
			if (element > 0) {
				tc++;
			}
		}
		for (int element : c.bankItems3) {
			if (element > 0) {
				td++;
			}
		}
		for (int element : c.bankItems4) {
			if (element > 0) {
				te++;
			}
		}
		for (int element : c.bankItems5) {
			if (element > 0) {
				tf++;
			}
		}
		for (int element : c.bankItems6) {
			if (element > 0) {
				tg++;
			}
		}
		for (int element : c.bankItems7) {
			if (element > 0) {
				th++;
			}
		}
		for (int element : c.bankItems8) {
			if (element > 0) {
				ti++;
			}
		}
		if (tab == 0) {
			return ta;
		}
		if (tab == 1) {
			return tb;
		}
		if (tab == 2) {
			return tc;
		}
		if (tab == 3) {
			return td;
		}
		if (tab == 4) {
			return te;
		}
		if (tab == 5) {
			return tf;
		}
		if (tab == 6) {
			return tg;
		}
		if (tab == 7) {
			return th;
		}
		if (tab == 8) {
			return ti;
		}
		return ta + tb + tc + td + te + tf + tg + th + ti;// return total

	}

	public int getBoostStat(int skill, boolean sup) {
		int increaseBy = 0;
		if (sup) {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .20);
		} else {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .06);
		}
		if (c.getSkills().getLevel(skill)
				+ increaseBy > c.getSkills().getStaticLevel(skill)
						+ increaseBy) {
			return c.getSkills().getStaticLevel(skill) + increaseBy
					- c.getSkills().getLevel(skill);
		}
		return increaseBy;
	}

	public int getBoostStat1(int skill, boolean sup) {
		int increaseBy = 0;
		if (sup) {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .10);
		} else {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .06);
		}
		if (c.getSkills().getLevel(skill)
				+ increaseBy > c.getSkills().getStaticLevel(skill)
						+ increaseBy) {
			return c.getSkills().getStaticLevel(skill) + increaseBy
					- c.getSkills().getLevel(skill);
		}
		return increaseBy;
	}

	public int[] getClosestXY(int x, int y) {
		int[] closest = {x, y};

		int[][] sides = {{x + 1, y}, // right
				{x - 1, y}, // left
				{x, y + 1}, // top
				{x, y - 1}};// bottom

		// int[] otherRight = {otherX+1, otherY};
		// int[] otherLeft = {otherX-1, otherY};
		// int[] otherTop = {otherX, otherY+1};
		// int[] otherBottom = {otherX, otherY-1};

		int lastClosest = 20;
		int closestIndex = 0;
		for (int l = 0; l < 4; l++) { // find closest spot
			int distance = Misc.distanceToPoint(c.getX(), c.getY(), sides[l][0],
					sides[l][1]);
			if (distance < lastClosest) {
				closestIndex = l;
				lastClosest = distance;
			}
		}

		closest[0] = sides[closestIndex][0]; // x
		closest[1] = sides[closestIndex][1]; // y

		return closest;
	}

	public int getHunterReward() {
		if (DBConfig.BETTER_HUNT_REWARD_CHANCE
				? Misc.random(100) <= 89
				: Misc.random(100) < 80) {
			if (DBConfig.BETTER_HUNT_REWARD_CHANCE
					? Misc.random(15) == 5
					: Misc.random(25) == 1) {
				return randomPvpGear();
			} else {
				if (DBConfig.BETTER_HUNT_REWARD_CHANCE
						? Misc.random(8/* 7 */) == 2
						/* 7 */ : Misc.random(15/* 15 */) == 0) {
					return randomPkGear();
				} else {
					if (Misc.newRandom(20) == 1)
						return BrawlerGloveEnum.getRandomGlove();
					return randomArtifact();
				}
			}
		}
		return -1;
	}

	public int getHunterRewardFromKillingBot() {
		if (Misc.random(100) < 50) {
			if (Misc.random(16) == 1) {
				return randomPvpGear();
			} else {
				if (Misc.random(16) == 0) {
					return randomPkGear();
				} else {
					return randomArtifact();
				}
			}
		}
		return -1;
	}

	public int getInterfaceModel(int slot, int[] array, int[] arrayN) {
		int model = array[slot] - 1;
		if (model == 995) {
			if (arrayN[slot] > 9999) {
				model = 1004;
			} else if (arrayN[slot] > 999) {
				model = 1003;
			} else if (arrayN[slot] > 249) {
				model = 1002;
			} else if (arrayN[slot] > 99) {
				model = 1001;
			} else if (arrayN[slot] > 24) {
				model = 1000;
			} else if (arrayN[slot] > 4) {
				model = 999;
			} else if (arrayN[slot] > 3) {
				model = 998;
			} else if (arrayN[slot] > 2) {
				model = 997;
			} else if (arrayN[slot] > 1) {
				model = 996;
			}
		}
		return model;
	}

	public int getItemSlot(int itemID) {
		for (int i = 0; i < c.playerItems.length; i++) {
			if ((c.playerItems[i] - 1) == itemID) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Private Messaging
	 **/
	// public void logIntoPM() {
	// setPrivateMessaging(2);
	//
	// //update other players who have this guy added to friends
	//
	// for (int i1 = 0; i1 < Config.MAX_PLAYERS; i1++) {
	// Player p = PlayerHandler.players[i1];
	// if (p != null && p.isActive) {
	// final Client o = (Client) p;
	// if (o != null) {
	// o.getPA().updatePM(c.getId(), 1);
	// }
	// }
	// }
	//
	//
	//
	//
	// //update your own friends list
	//
	// boolean pmLoaded = false;
	//
	// for (int i = 0; i < c.friends.length; i++) {
	// if (c.friends[i] != 0) {
	// for (int i2 = 1; i2 < Config.MAX_PLAYERS; i2++) {
	// Player p = PlayerHandler.players[i2];
	// if (p != null && p.isActive && Mi.getName()meToInt64(p.getName()) ==
	// c.friends[i]) {
	// final Client o = (Client) p;
	// if (o != null) {
	// if (c.playerRights >= 2 || p.privateChat == 0 || (p.privateChat == 1 &&
	// o.getPA().isInPM(Misc.playerNameToInt64(c.getName())))) {
	// loadPM(c.friends[i], 1);
	// pmLoaded = true;
	// }
	// break;
	// }
	// }
	// }
	// if (!pmLoaded) {
	// loadPM(c.friends[i], 0);
	// }
	// pmLoaded = false;
	//
	//// for (int i1 = 1; i1 < Config.MAX_PLAYERS; i1++) {
	//// Player p = PlayerHandler.players[i1];
	//// if (p != null && p.isActive) {
	//// final Client o = (Client) p;
	//// if (o != null) {
	//// o.getPA().updatePM(c.getId(), 1);
	//// }
	//// }
	//// }
	// }
	// }
	// }
	//
	// public void updatePM(int pID, int world) { // used for private chat
	// updates
	// Player p = PlayerHandler.players[pID];
	// if (p == null || p.getName() == null || p.getName().equals("null")) {
	// return;
	// }
	// final Client o = (Client) p;
	// // if (o == null) {
	// // return;
	// // }
	// long l = Misc.playerNameToInt64(PlayerHandler.players[pID].getName());
	//
	// if (p.privateChat == 0) {
	// for (int i = 0; i < c.friends.length; i++) {
	// if (c.friends[i] != 0) {
	// if (l == c.friends[i]) {
	// loadPM(l, world);
	// return;
	// }
	// }
	// }
	// } else if (p.privateChat == 1) {
	// for (int i = 0; i < c.friends.length; i++) {
	// if (c.friends[i] != 0) {
	// if (l == c.friends[i]) {
	// if (o.getPA().isInPM(
	// Misc.playerNameToInt64(c.getName()))) {
	// loadPM(l, world);
	// return;
	// } else {
	// loadPM(l, 0);
	// return;
	// }
	// }
	// }
	// }
	// } else if (p.privateChat == 2) {
	// for (int i = 0; i < c.friends.length; i++) {
	// if (c.friends[i] != 0) {
	// if (l == c.friends[i] && c.playerRights < 2) {
	// loadPM(l, 0);
	// return;
	// }
	// }
	// }
	// }
	// }
	//
	// public boolean isInPM(long l) {
	// for (int i = 0; i < c.friends.length; i++) {
	// if (c.friends[i] != 0) {
	// if (l == c.friends[i]) {
	// return true;
	// }
	// }
	// }
	// return false;
	// }

	/**
	 * Drink AntiPosion Potions
	 *
	 * @param itemId
	 *            The itemId
	 * @param itemSlot
	 *            The itemSlot
	 * @param newItemId
	 *            The new item After Drinking
	 * @param healType
	 *            The type of poison it heals
	 */
	// public void potionPoisonHeal(int itemId, int itemSlot, int newItemId,
	// int healType) {
	// c.attackTimer = c.getCombat().getAttackDelay(
	// ItemAssistant.getItemName(c.playerEquipment[Player.playerWeapon])
	// .toLowerCase());
	// if (c.duelRule[DuelRules.NO_DRINKS]) {
	// c.sendMessage("Potions has been disabled in this duel!");
	// return;
	// }
	// if (!c.isDead && System.currentTimeMillis() - c.foodDelay > 2000) {
	// if (c.getItems().playerHasItem(itemId, 1, itemSlot)) {
	// c.sendMessage("You drink the " +
	// ItemAssistant.getItemName(itemId).toLowerCase() + ".");
	// c.foodDelay = System.currentTimeMillis();
	// // Actions
	// if (healType == 1) {
	// // Cures The Poison
	// } else if (healType == 2) {
	// // Cures The Poison + protects from getting poison again
	// }
	// c.startAnimation(0x33D);
	// c.getItems().deleteItem(itemId, itemSlot, 1);
	// c.getItems().addItem(newItemId, 1);
	// requestUpdates();
	// }
	// }
	// }

	public int getMove(int place1, int place2) {
		if (System.currentTimeMillis() - c.lastSpear < 4000) {
			return 0;
		}
		if ((place1 - place2) == 0) {
			return 0;
		} else if ((place1 - place2) < 0) {
			return 1;
		} else if ((place1 - place2) > 0) {
			return -1;
		}
		return 0;
	}

	public void getRangeDistance() {
		if (c.playerIndex <= 0) {
			return;
		}
		if (PlayerHandler.players[c.playerIndex] == null
				|| !PlayerHandler.players[c.playerIndex].isActive
				|| PlayerHandler.players[c.playerIndex].isDead()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		
		Player defender = PlayerHandler.players[c.playerIndex];

		int distRequired = CombatFormulas.getRangeDistReq(c);
		
		if (c.inMarket())
			distRequired = 1;

		if (distRequired > 1) { // range distance
			if (c.getFightXp() == FightExp.RANGED_DEFENSE) { // long distance shots
				distRequired += 2;
			}
			
			distRequired = Math.min(10, distRequired);
			
			if (c.getSeasonalData().containsPowerUp(PowerUpData.RANGER) && (c.getFightXp() == FightExp.RANGED || c.getFightXp() == FightExp.RANGED_DEFENSE)) {
				distRequired += 2;
			}

			if (c.withinDistance(defender,
					distRequired)) {
				c.withinRangeToHit = true;
				c.rangeRequiredToHit = distRequired;
				c.withinDistanceToStopWalking = true;
			}
			
		} else { // melee distance
			
			if (PathFinder.reachedEntity(1, defender.getX(), defender.getY(),
					c.getX(), c.getY(), 1, 1, 0,
					RegionClip.getClipping(c.getX(), c.getY(), c.getZ(), c.getDynamicRegionClip()))) {
				c.withinRangeToHit = true;
				c.rangeRequiredToHit = distRequired;
			}
		}

	}
	
	public void getRangeDistanceToNpc() {
		if (c.npcIndex <= 0) {
			return;
		}
		if (NPCHandler.npcs[c.npcIndex] == null
				|| NPCHandler.npcs[c.npcIndex].isDead()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (NPCHandler.npcs[c.npcIndex].getHeightLevel() != c.getZ()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		
		final NPC npc = NPCHandler.npcs[c.npcIndex];

		int distRequired = CombatFormulas.getRangeDistReq(c);
		
		if (distRequired > 1) { // range distance
			if (c.getFightXp() == FightExp.RANGED_DEFENSE) { // long distance shots
				distRequired += 2;
			}

			if (c.getSeasonalData().containsPowerUp(PowerUpData.RANGER) && (c.getFightXp() == FightExp.RANGED || c.getFightXp() == FightExp.RANGED_DEFENSE)) {
				distRequired += 2;
			}

			if (withinDistanceToNpc(npc, distRequired)) {
				c.withinDistanceToStopWalking = true;
				c.withinRangeToHit = true;
				c.rangeRequiredToHit = distRequired;
			}
			
			
		} else { // melee distance
			if (PathFinder.reachedEntity(1, npc.getX(), npc.getY(),
					c.getX(), c.getY(), c.entityClickSizeX, c.entityClickSizeY, c.entityWalkingFlag,
					RegionClip.getClipping(c.getX(), c.getY(), c.getZ(), c.getDynamicRegionClip()))) {
				c.withinDistanceToStopWalking = true;
				c.withinRangeToHit = true;
				c.rangeRequiredToHit = distRequired;
			}

		}

	}

	public int getRunningMove(int i, int j) {
		if (j - i > 2) {
			return 2;
		} else if (j - i < -2) {
			return -2;
		} else {
			return j - i;
		}
	}

	public void getShoved(int attackerX, int attackerY) {
		int x = c.absX - attackerX;
		int y = c.absY - attackerY;
		if (x > 0) {
			if (RegionClip.getClippingDirection(c.getX() + 1, c.getY(), c.heightLevel, 1,
					0, c.getDynamicRegionClip())) {
				x = 1;
			}
		} else if (x < 0) {
			if (RegionClip.getClippingDirection(c.getX() - 1, c.getY(), c.heightLevel,
					-1, 0, c.getDynamicRegionClip())) {
				x = -1;
			}
		}
		if (y > 0) {
			if (RegionClip.getClippingDirection(c.getX(), c.getY() + 1, c.heightLevel, 0,
					1, c.getDynamicRegionClip())) {
				y = 1;
			}
		} else if (y < 0) {
			if (RegionClip.getClippingDirection(c.getX(), c.getY() - 1, c.heightLevel, 0,
					-1, c.getDynamicRegionClip())) {
				y = -1;
			}
		}
		c.getPA().moveCheck(x, y);
	}

	public int getTabCount() {
		// count tabs
		int tabs = 0;
		if (!checkEmpty(c.bankItems1)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems2)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems3)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems4)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems5)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems6)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems7)) {
			tabs++;
		}
		if (!checkEmpty(c.bankItems8)) {
			tabs++;
		}
		return tabs;
	}

	public boolean isMaxed() {
		for (int i = 0, length = c.getPlayerLevel().length; i < length; i++) {
			if (c.getSkills().getStaticLevel(i) < 99) {
				return false;
			}
		}

		return true;
	}

	public int getTotalLevel() {
		int totallevel = 0;
		for (int i = 0, length = c.getPlayerLevel().length; i < length; i++) {
			totallevel += c.getSkills().getStaticLevel(i);
		}

		return totallevel;
	}

	public int getWearingAmount() {
		int count = 0;
		for (int element : c.playerEquipment) {
			if (element > 0) {
				count++;
			}
		}
		return count;
	}

	public void giveLife(final Client killer, boolean keepItemOnDeath,
			int respawnX, int respawnY, int respawnZ, boolean safeDeath,
			boolean killedInWildy, boolean killedInLMS, boolean killedInDmmTourn, boolean keepUntradeables) {
		try {
		c.setDead(false);
		c.safeTimer = 0;
		// long delay9 = System.currentTimeMillis();

		if (c.insideInstance()) {
			c.getInstanceManager().playerDeath(c);
		}

		if (c.hidePlayer) {
			safeDeath = true;
		}

		if (Config.RUN_ON_DEDI && c.isOwner()) {
			safeDeath = true;
		}
		
		if (c.getZones().isInDmmTourn()) {
			c.isSkulled = true;
		}
		
		if (c.inSoulWarsLobby() && respawnX > 0) { // TODO: we need to do something similar to this with every minigame ending.
			respawnX = 1890;
			respawnY = 3177;
			respawnZ = 0;
		} else if (c.newRespawnX > 0) { // back up in case movePlayer() is happening during death
			respawnX = c.newRespawnX;
			respawnY = c.newRespawnY;
			respawnZ = c.newRespawnZ;
		}
		c.newRespawnX = c.newRespawnY = c.newRespawnZ = 0;
		
		MagicalCrate.dropMagicalCrateInWild(c);

		if (!safeDeath || killedInLMS) { // if we are not in a duel we must be in wildy so
							// remove items
			
			RunePouch.removeRunePouchWildy(c);

			c.getItems().resetKeepItems();
			if (keepUntradeables)
				c.getItems().grabUntradeables();

			if (!c.isSkulled && !killedInLMS) { // what items to keep
				c.getItems().keepItem(0, true);
				c.getItems().keepItem(1, true);
				c.getItems().keepItem(2, true);
			}
			if (keepItemOnDeath) {
				c.getItems().keepItem(3, true);
			}
			
			ChargedItems.unchargeItemsOnDeath(c, killer);

			if (killedInWildy) {
				PlayerSaveSql.updatePkedTimestamp(c.mySQLIndex);
				
				if (killer != null) {
					PlayerSaveSql.updatePkedTimestamp(killer.mySQLIndex);

					killer.increaseProgress(TaskType.OPPONENT_DEATH, 0);
				}
			}
			
			c.getItems().dropAllItems(killer, killedInWildy, killedInLMS); // drop all items
			c.getItems().deleteAllItems(); // delete all items

			// }
			if (!killedInLMS && !c.isSkulled) { // add the kept items once we finish deleting
								// and dropping them
				for (int i1 = 0; i1 < 3; i1++) {
					if (c.itemKeptId[i1] > 0) {
						c.getItems().addItem(c.itemKeptId[i1], 1);
					}
				}
			}
			if (!killedInLMS && keepItemOnDeath) { // if (c.playerPrayerBook == 0 ?
									// c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									// :
									// c.curseActive[Curses.PROTECT_ITEM.ordinal()])
									// {
				if (c.itemKeptId[3] > 0) {
					c.getItems().addItem(c.itemKeptId[3], 1);
				}
			}
			c.getItems().giveBackUntradeables(killer, killedInWildy);
			c.getItems().resetKeepItems();
		}
		SoulWars.onDeath(c, killer);
		c.getCombat().resetPrayers();

		if (c.getJailTime() > 0 || c.isInJail()) {
			c.sendMessage("Nice try");
			c.getPA().movePlayer(2095, 4428, c.heightLevel);
		} else if (respawnX > 0) {
			movePlayer(respawnX, respawnY, respawnZ);
		} else if (c.pitsStatus == 1) {
			movePlayer(2399, 5173, 0);
		} else if (c.inFunPk()) {
			movePlayer(3303, 3123, 0);
		} else if (c.inFightCaves()) {
			c.getPA().resetTzhaar();
			c.getPA().movePlayer(2438, 5168, 0);
		} else { // if we are not in a duel repawn to wildy
			movePlayer(DBConfig.RESPAWN_X, DBConfig.RESPAWN_Y, 0);
		}

		TobManager tobManager = c.tobManager;
		if (tobManager != null && tobManager.started()) {
			tobManager.checkWiped();
			tobManager.wipeDawnbringer(c);
			c.tobDead = true;
		}

		// System.out.println("delay "+(System.currentTimeMillis() - delay9));
		if (c.inCwGame) {
			c.getPA().setCastleWarsWaitingTimer(
					Misc.secondsToTicks(CastleWars.SECONDS_TO_EXIT_SPAWN));
		}

		if (c.inClanWars()) {
			ClanWarHandler.handleDeath(c);
		}
		
		closeAllWindows();
		
		if (c.isInLmsArena()) {
			c.lmsManager.removePlayerFromArena(c, false);
			c.getItems().deleteAllItems(); // double delete just in case
		}

		if (c.insideDungeoneering() && c.walkableInterface == DungeonManager.PROGRESS_INTERFACE_ID) {
			//Remove the progress bar interface on death
			c.getPA().walkableInterface(-1);
		}

		c.setPauseAnimReset(0);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				c.startAnimation(Animation.RESET);
			}
		}, 1);
		c.getCombat().resetPlayerAttack();
		resetTb();
		c.attackedPlayers.clear();
		c.setSkull(false);
		c.setSkullTimer(-1);
		c.vengOn = false;
		c.animatedArmorSpawned = null;
		c.vengDelay.reset();
		c.resetFace();
		c.setFreezeTimer(1);
		c.getMovement().restoreRunEnergy(100);
		MeleeData.getPlayerAnimIndex(c, c.playerEquipment[PlayerConstants.playerWeapon]);
		c.getItems().addSpecialBar(true);

		int prayerRenwalCheaphax = c.prayerRenew;
		c.getPA().restorePlayer(true);
		c.getPA().prayerRenewal(prayerRenwalCheaphax);//cheaphax to continue prayer renewal after death XD
		//temp PI resetting stupid spec info
		c.usingSpecial = false;
		c.queueGmaulSpec = false;
		//end of old PI shit
		c.performingAction = false;
		c.playerIndex = 0;
		c.npcIndex = 0;
		c.canOffer = false;
		c.killedByPlayerName = "n";
		c.wildLevel = 0;
		resetSkilling();
		c.getDamageMap().reset();
		c.setImbuedHeartTimer(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		c.startReviveTick();
	}

	public int greenWildyKey() {
		return PlayerConstants.greenWildyKey[(int) (Math.random()
				* PlayerConstants.greenWildyKey.length)];
	}

	public void handleGamesNeck() {
		if (c.getItems().playerHasItem(3853, 1)) {
			c.getItems().deleteItemInOneSlot(3853, 1);
			c.getItems().addItem(3855, 1);
			c.sendMessage("You have 7 Charges left.");
		} else if (c.getItems().playerHasItem(3855, 1)) {
			c.getItems().deleteItemInOneSlot(3855, 1);
			c.getItems().addItem(3857, 1);
			c.sendMessage("You have 6 Charges left.");
		} else if (c.getItems().playerHasItem(3857, 1)) {
			c.getItems().deleteItemInOneSlot(3857, 1);
			c.getItems().addItem(3859, 1);
			c.sendMessage("You have 5 Charges left.");
		} else if (c.getItems().playerHasItem(3859, 1)) {
			c.getItems().deleteItemInOneSlot(3859, 1);
			c.getItems().addItem(3861, 1);
			c.sendMessage("You have 4 Charges left.");
		} else if (c.getItems().playerHasItem(3861, 1)) {
			c.getItems().deleteItemInOneSlot(3861, 1);
			c.getItems().addItem(3863, 1);
			c.sendMessage("You have 3 Charges left.");
		} else if (c.getItems().playerHasItem(3863, 1)) {
			c.getItems().deleteItemInOneSlot(3863, 1);
			c.getItems().addItem(3865, 1);
			c.sendMessage("You have 2 Charges left.");
		} else if (c.getItems().playerHasItem(3865, 1)) {
			c.getItems().deleteItemInOneSlot(3865, 1);
			c.getItems().addItem(3867, 1);
			c.sendMessage("You have 1 Charge left.");
		} else if (c.getItems().playerHasItem(3867, 1)) {
			c.getItems().deleteItemInOneSlot(3867, 1);
			c.sendMessage("You have used all games necklace charges.");
		}
	}

	public void handleGlory() {
		c.getDialogueBuilder().sendOption("Edgeville", () -> {
			c.getPA().startTeleport(LocationConstants.EDGEVILLE_X, LocationConstants.EDGEVILLE_Y, 0, TeleportType.GLORY);
		} , "Al Kharid", () -> {
			c.getPA().startTeleport(LocationConstants.AL_KHARID_X, LocationConstants.AL_KHARID_Y, 0, TeleportType.GLORY);
		} , "Karamja", () -> {
			c.getPA().startTeleport(LocationConstants.KARAMJA_X, LocationConstants.KARAMJA_Y, 0, TeleportType.GLORY);
		} , "Mage Bank", () -> {
			c.getPA().startTeleport(LocationConstants.MAGEBANK_X, LocationConstants.MAGEBANK_Y, 0, TeleportType.GLORY);
		}).execute();
	}

	public void handleLoginText() {
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int timer = 4;

			@Override
			public void execute(CycleEventContainer container) {

				if (timer == 4) {
					c.getPA().setTeleportNames();
				}
				if (timer == 3) {
					loadQuests();
				}
				if (timer == 2) {
					c.getAchievement().updateAchievementTab();
					c.getAchievement().achivementsCompleted();
				}
				if (timer == 1) {
					Titles.unlockTitles(c);
				}

				timer--;
				if (timer <= 0) {
					container.stop();
				}

			}

			@Override
			public void stop() {

			}
		}, 1);

	}

	public void handleSkillsNeck() {
		if (c.getItems().playerHasItem(11105, 1)) {
			c.getItems().deleteItemInOneSlot(11105, 1);
			c.getItems().addItem(11107, 1);
			c.sendMessage("You have 3 Charges left.");
		} else if (c.getItems().playerHasItem(11107, 1)) {
			c.getItems().deleteItemInOneSlot(11107, 1);
			c.getItems().addItem(11109, 1);
			c.sendMessage("You have 2 Charges left.");
		} else if (c.getItems().playerHasItem(11109, 1)) {
			c.getItems().deleteItemInOneSlot(11109, 1);
			c.getItems().addItem(11111, 1);
			c.sendMessage("You have 1 Charge left.");
		} else if (c.getItems().playerHasItem(11111, 1)) {
			c.getItems().deleteItemInOneSlot(11111, 1);
			c.getItems().addItem(11113, 1);
			c.sendMessage("You have used all skills necklace charges.");
		}
	}

	public void increaseAS() {
		if (c.usingWhiteWhipSpec) {
			return;
		}
		
		c.startGraphic(Graphic.create(247));
		c.usingWhiteWhipSpec = true;
		c.sendMessage("You get increased Attack Speed!");
		Server.getTaskScheduler().schedule(new Task(true) {

			int ticks = 5;

			@Override
			protected void execute() {
				if (c.forceDisconnect) {
					this.stop();
					return;
				}
				if (ticks < 0) {
					c.usingWhiteWhipSpec = false;
					this.stop();
				}
				ticks--;
			}
		});
	}

	public void increaseASByINSANE() {
		if (c.usingWhiteWhipSpec) {
			return;
		}
		c.startGraphic(Graphic.create(247, GraphicType.LOW));
		c.usingWhiteWhipSpec = true;
		c.sendMessage("You get increased Attack Speed!");
		Server.getTaskScheduler().schedule(new Task(true) {

			int ticks = 7;

			@Override
			protected void execute() {
				if (c.forceDisconnect) {
					this.stop();
					return;
				}
				if (ticks < 0) {
					c.usingWhiteWhipSpec = false;
					this.stop();
				}
				ticks--;
			}
		});
	}

	public boolean inPitsWait() {
		return c.getX() <= 2404 && c.getX() >= 2394 && c.getY() <= 5175
				&& c.getY() >= 5169;
	}

	public boolean isFilledVial(final int item) {
		switch (item) {
			case 6685:
			case 6687:
			case 6689:
			case 6691:
			case 2436:
			case 145:
			case 147:
			case 149:
			case 2440:
			case 157:
			case 159:
			case 161:
			case 2444:
			case 169:
			case 171:
			case 173:
			case 2432:
			case 133:
			case 135:
			case 137:
			case 113:
			case 115:
			case 117:
			case 119:
			case 2428:
			case 121:
			case 123:
			case 125:
			case 2442:
			case 163:
			case 165:
			case 167:
			case 3024:
			case 3026:
			case 3028:
			case 3030:
			case 12140:
			case 12142:
			case 12144:
			case 12146:
			case 10925:
			case 10927:
			case 10929:
			case 10931:
			case 2434:
			case 139:
			case 141:
			case 143:
			case 2446:
			case 175:
			case 177:
			case 179:
			case 2448:
			case 181:
			case 183:
			case 185:
			case 15312:
			case 15313:
			case 15314:
			case 15315:
			case 15308:
			case 15309:
			case 15310:
			case 15311:
			case 15316:
			case 15317:
			case 15318:
			case 15319:
			case 15324:
			case 15325:
			case 15326:
			case 15327:
			case 15320:
			case 15321:
			case 15322:
			case 15323:
			case 15328:
			case 15329:
			case 15330:
			case 15331:
			case 15300:
			case 15301:
			case 15302:
			case 15303:
			case 15332:
			case 15333:
			case 15334:
			case 15335:
				return true;
		}
		return false;
	}

	public boolean isInSafeDeathSpot() {
		if (c.safeDeath) {
			return true;
		}
		if (c.getZones().isInDmmTourn() && c.tournSettings != null && c.tournSettings.isSafeDeath()) {
			return true;
		}
		if (c.raidsManager != null || c.tobManager != null) {
			return true;
		}
		if (c.insideInstance()) {
			return c.getInstanceManager().safeDeath();
		}
		if (c.infernoManager != null) {
			return true;
		}
		if (c.playerIsInHouse)
			return true;
		
		if (c.insideDungeoneering()) {
			return true;
		}
		if (PlayerHandler.disconnectedFromLoginServerCount > 20) {
			return true;
		}
		if (c.clanWarRule[CWRules.ITEMS_LOST_ON_DEATH] && c.inClanWars()) {
			return false;
		}
		if (NexEvent.hasStarted() && NexEvent.playerInsideNex(c)) {
			return true;
		}
		return RSConstants.fightPitsRoom(c.getX(), c.getY()) || c.inFightCaves()
				|| c.inCwGame || c.inPits || c.inDuel || c.inFunPk()
				|| c.inClanWarsFunPk() || c.whitePortalSafe() || c.inClanWars()
				|| c.inPcGame() || c.inDuelArena() || c.inCw()
				|| RSConstants.inBarbarianAssaultArena(c.absX, c.absY)
				|| c.inSoulWars() 
				|| c.inSoulWarsLobby();
	}

	public boolean isItemInBag(int itemID) {
		for (int playerItem : c.playerItems) {
			if ((playerItem - 1) == itemID) {
				return true;
			}
		}
		return false;
	}

	public boolean isSOLEquipped() {
		return c.getItems().hasEquippedWeapon(ItemConstants.STAFF_OF_LIGHT_IDS);
	}

	public void ItemKeptInfo(int Lose) {
		for (int i = 17109; i < 17131; i++) {
			c.getPacketSender().sendFrame126("", i);
		}
		c.getPacketSender().sendFrame126("Items you will keep on death:",
				17104);
		c.getPacketSender().sendFrame126("Items you will lose on death:",
				17105);
		c.getPacketSender().sendFrame126("Max items kept on death:", 17107);
		c.getPacketSender().sendFrame126("~ " + Lose + " ~", 17108);
		c.getPacketSender().sendFrame126("The normal amount of", 17111);
		c.getPacketSender().sendFrame126("items kept is three.", 17112);

		switch (Lose) {
			case 0:
			default:
				c.getPacketSender()
						.sendFrame126("Items you will keep on death:", 17104);
				c.getPacketSender()
						.sendFrame126("Items you will lose on death:", 17105);
				c.getPacketSender().sendFrame126("You're marked with a", 17111);
				c.getPacketSender().sendFrame126(
						"<col=ff0000>skull. <col=ff9040>This reduces the", 17112);
				c.getPacketSender().sendFrame126("items you keep from", 17113);
				c.getPacketSender().sendFrame126("three to zero!", 17114);
				break;
			case 1:
				c.getPacketSender()
						.sendFrame126("Items you will keep on death:", 17104);
				c.getPacketSender()
						.sendFrame126("Items you will lose on death:", 17105);
				c.getPacketSender().sendFrame126("You're marked with a", 17111);
				c.getPacketSender().sendFrame126(
						"<col=ff0000>skull. <col=ff9040>This reduces the", 17112);
				c.getPacketSender().sendFrame126("items you keep from", 17113);
				c.getPacketSender().sendFrame126("three to zero!", 17114);
				c.getPacketSender().sendFrame126("However, you also have",
						17115);
				c.getPacketSender().sendFrame126(
						"the <col=ff0000>Protect <col=ff9040>Items prayer", 17116);
				c.getPacketSender().sendFrame126("active, which saves you",
						17117);
				c.getPacketSender().sendFrame126("one extra item!", 17118);
				break;
			case 3:
				c.getPacketSender().sendFrame126(
						"Items you will keep on death(if not skulled):", 17104);
				c.getPacketSender().sendFrame126(
						"Items you will lose on death(if not skulled):", 17105);
				c.getPacketSender().sendFrame126("You have no factors", 17111);
				c.getPacketSender().sendFrame126("affecting the items you",
						17112);
				c.getPacketSender().sendFrame126("keep.", 17113);
				break;
			case 4:
				c.getPacketSender().sendFrame126(
						"Items you will keep on death(if not skulled):", 17104);
				c.getPacketSender().sendFrame126(
						"Items you will lose on death(if not skulled):", 17105);
				c.getPacketSender().sendFrame126("You have the <col=ff0000>Protect",
						17111);
				c.getPacketSender()
						.sendFrame126("<col=ff0000>Item <col=ff9040>prayer active,", 17112);
				c.getPacketSender().sendFrame126("which saves you one", 17113);
				c.getPacketSender().sendFrame126("extra item!", 17114);
				break;
		}
	}

	/**
	 * keeps the hunter tick for 2 minutes while player is banking and stuff
	 */
	public void keepHunterTickStart() {
		if (c.freshOnTheHunt) {
			return;
		}
		c.setWildernessTick(1);
		c.freshOnTheHunt = true;
		final Client c2 = (Client) PlayerHandler.players[c.huntPlayerIndex];
		c.sendMessage(
				"<col=ff0000>You have two minutes to gear up for the hunt or your target will reset.");
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int count = 100; // 2 minutes since cycle is 1.2 second

			@Override
			public void execute(CycleEventContainer container) {
				count--;
				if (c == null) {
					container.stop();
					return;
				}
				if (c2 == null || c2.isDead() || c2.forceDisconnect
						|| c2.wildLevel < 1) {
					c.freshOnTheHunt = false;
					if (c.wildLevel < 1) {
						c.getPA().resetHuntId();
					}
					container.stop();
				}
				if (count < 1) {
					c.freshOnTheHunt = false;
					if (c.wildLevel < 1) {
						c.getPA().resetHuntId();
					}
					container.stop();
				}
			}

			@Override
			public void stop() {

			}

		}, 2);
	}

	public boolean keepItemsOnDeathPrayerOn() {
		if (c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
				|| c.curseActive[Curses.PROTECT_ITEM.ordinal()]) {
			return true;
		}
		return false;
	}

	public void levelUp(int skill) {
		if ((skill >= Skills.ATTACK && skill <= Skills.MAGIC) || skill == Skills.SUMMONING) {
			int oldCombatLevel = c.combatLevel;
			c.calcCombat();

			if (WorldType.isSeasonal() && c.combatLevel > 9 && c.combatLevel > oldCombatLevel) {
				for (int i = oldCombatLevel; i < c.combatLevel; i++) {
					if (i % 5 == 0) {
						PowerUpInterface.rollSinglePowerUp(c);
					}
				}
			}
		}

		if (c.isBot()) {
			return;
		}

		c.updateDailyTasksState();
		int totalLevel = getTotalLevel();
		c.getPacketSender().sendFrame126("Total Lvl: " + totalLevel, 3984);

		if (c.getSkills().getStaticLevel(skill) == 99) {
			c.getAchievement().get99Skill();
			Titles.achiev99Stat(c);
			if (WorldType.isSeasonal()) {
				Variables.SEASONAL_INTERFACE_POINTS.addValue(c, 1);
			}
		}
		
		if (c.getSkills().getStaticLevel(skill) == 120) {
			Titles.achiev120Stat(c);
			if (WorldType.isSeasonal() && skill == Skills.DUNGEONEERING) {
				Variables.SEASONAL_INTERFACE_POINTS.addValue(c, 1);
			}
		}

		if (totalLevel >= 2496) {
			Titles.maximum(c);
		}

		if (totalLevel >= 2496 || (totalLevel == 1920 && c.isIronMan())) {
			Events.Prestige(c);
		}

		switch (skill) {
			case Skills.ATTACK:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced an attack level!",
						6248);
				c.getPacketSender().sendFrame126("Your attack level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6249);
				c.sendMessage(
						"Congratulations, you just advanced an attack level.");
				c.getPacketSender().sendChatInterface(6247);
				break;

			case Skills.DEFENSE:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a defence level!",
						6254);
				c.getPacketSender().sendFrame126("Your defence level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6255);
				c.sendMessage(
						"Congratulations, you just advanced a defence level.");
				c.getPacketSender().sendChatInterface(6253);
				break;

			case Skills.STRENGTH:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a strength level!",
						6207);
				c.getPacketSender().sendFrame126("Your strength level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6208);
				c.sendMessage(
						"Congratulations, you just advanced a strength level.");
				c.getPacketSender().sendChatInterface(6206);
				break;

			case Skills.HITPOINTS:
				c.getPacketSender().sendFrame126("Congratulations, you just advanced a hitpoints level!",6217);
				c.getPacketSender().sendFrame126("Your hitpoints level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6218);
				c.sendMessage("Congratulations, you just advanced a hitpoints level.");
				c.getPacketSender().sendChatInterface(6216);
				break;

			case Skills.RANGED:
				c.sendMessage(
						"Congratulations, you just advanced a ranged level.");
				break;

			case Skills.PRAYER:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a prayer level!",
						6243);
				c.getPacketSender().sendFrame126("Your prayer level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6244);
				c.sendMessage(
						"Congratulations, you just advanced a prayer level.");
				c.getPacketSender().sendChatInterface(6242);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Prayer</col>.");
				}
				break;

			case Skills.MAGIC:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a magic level!",
						6212);
				c.getPacketSender().sendFrame126("Your magic level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6213);
				c.sendMessage(
						"Congratulations, you just advanced a magic level.");
				c.getPacketSender().sendChatInterface(6211);
				break;

			case Skills.COOKING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a cooking level!",
						6227);
				c.getPacketSender().sendFrame126("Your cooking level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6228);
				c.sendMessage(
						"Congratulations, you just advanced a cooking level.");
				c.getPacketSender().sendChatInterface(6226);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Cooking</col>.");
				}
				break;

			case Skills.WOODCUTTING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a woodcutting level!",
						4273);
				c.getPacketSender()
						.sendFrame126("Your woodcutting level is now "
								+ c.getSkills().getStaticLevel(skill) + ".", 4274);
				c.sendMessage(
						"Congratulations, you just advanced a woodcutting level.");
				c.getPacketSender().sendChatInterface(4272);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Woodcutting</col>.");
				}
				break;

			case Skills.FLETCHING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a fletching level!",
						6232);
				c.getPacketSender().sendFrame126("Your fletching level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6233);
				c.sendMessage(
						"Congratulations, you just advanced a fletching level.");
				c.getPacketSender().sendChatInterface(6231);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Fletching</col>.");
				}
				break;

			case Skills.FISHING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a fishing level!",
						6259);
				c.getPacketSender().sendFrame126("Your fishing level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6260);
				c.sendMessage(
						"Congratulations, you just advanced a fishing level.");
				c.getPacketSender().sendChatInterface(6258);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Fishing</col>.");
				}
				break;

			case Skills.FIREMAKING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a firemaking level!",
						4283);
				c.getPacketSender().sendFrame126("Your firemaking level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 4284);
				c.sendMessage(
						"Congratulations, you just advanced a firemaking level.");
				c.getPacketSender().sendChatInterface(4282);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Firemaking</col>.");
				}
				break;

			case Skills.CRAFTING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a crafting level!",
						6264);
				c.getPacketSender().sendFrame126("Your crafting level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6265);
				c.sendMessage(
						"Congratulations, you just advanced a crafting level.");
				c.getPacketSender().sendChatInterface(6263);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Crafting</col>.");
				}
				break;

			case Skills.SMITHING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a smithing level!",
						6222);
				c.getPacketSender().sendFrame126("Your smithing level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6223);
				c.sendMessage(
						"Congratulations, you just advanced a smithing level.");
				c.getPacketSender().sendChatInterface(6221);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Smithing</col>.");
				}
				break;

			case Skills.MINING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a mining level!",
						4417);
				c.getPacketSender().sendFrame126("Your mining level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 4438);
				c.sendMessage(
						"Congratulations, you just advanced a mining level.");
				c.getPacketSender().sendChatInterface(4416);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Mining</col>.");
				}
				break;

			case Skills.HERBLORE:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a herblore level!",
						6238);
				c.getPacketSender().sendFrame126("Your herblore level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6239);
				c.sendMessage(
						"Congratulations, you just advanced a herblore level.");
				c.getPacketSender().sendChatInterface(6237);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Herblore</col>.");
				}
				break;

			case Skills.AGILITY:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a agility level!",
						4278);
				c.getPacketSender().sendFrame126("Your agility level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 4279);
				c.sendMessage(
						"Congratulations, you just advanced an agility level.");
				c.getPacketSender().sendChatInterface(4277);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Agility</col>.");
				}
				break;

			case Skills.THIEVING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a thieving level!",
						4263);
				c.getPacketSender().sendFrame126("Your thieving level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 4264);
				c.sendMessage(
						"Congratulations, you just advanced a thieving level.");
				c.getPacketSender().sendChatInterface(4261);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Thieving</col>.");
				}
				break;

			case Skills.SLAYER:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a slayer level!",
						12123);
				c.getPacketSender()
						.sendFrame126("Your slayer level is now "
								+ c.getSkills().getStaticLevel(skill) + ".",
								12124);
				c.sendMessage(
						"Congratulations, you just advanced a slayer level.");
				c.getPacketSender().sendChatInterface(12122);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Slayer</col>.");
				}
				break;

			case Skills.FARMING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a farming level!",
						6238);
				c.getPacketSender().sendFrame126("Your farming level is now "
						+ c.getSkills().getStaticLevel(skill) + ".", 6239);
				c.sendMessage(
						"Congratulations, you just advanced a farming level.");
				c.getPacketSender().sendChatInterface(6237);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Farming</col>.");
				}
				break;

			case Skills.RUNECRAFTING:
				c.getPacketSender().sendFrame126(
						"Congratulations, you just advanced a runecrafting level!",
						4268);
				c.getPacketSender()
						.sendFrame126("Your runecrafting level is now "
								+ c.getSkills().getStaticLevel(skill) + ".", 4269);
				c.sendMessage(
						"Congratulations, you just advanced a runecrafting level.");
				c.getPacketSender().sendChatInterface(4267);
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Runecrafting</col>.");
				}
				break;

			case Skills.DUNGEONEERING:
				c.sendMessage(
						"Congratulations, you just advanced a Dungeoneering level.");
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Dungeoneering</col>.");
				} else if (c.getSkills().getStaticLevel(skill) == 120) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 120 <col=ff>Dungeoneering</col>.");
				}

				break;


			case Skills.HUNTER:
				c.sendMessage(
						"Congratulations, you just advanced a Hunter level.");
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Hunter</col>.");
				}
				break;

			case Skills.SUMMONING: // summoning
				c.sendMessage(
						"Congratulations, you just advanced a Summoning level.");
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Summoning</col>.");
				}
				break;
				
			case Skills.CONSTRUCTION:
				c.sendMessage(
						"Congratulations, you just advanced a Construction level.");
				if (c.getSkills().getStaticLevel(skill) == 99) {
					PlayerHandler.messageAllPlayers(
							"[<col=ff0000>NEWS</col>]The player <col=ff>"
									+ c.getNameSmartUp()
									+ "</col> just advanced to 99 <col=ff>Construction</col>.");
				}
				break;

		}

		c.dialogueAction = 0;
		c.nextChat = 0;
	}

	public void loadQuests() {
		if (c.isBot()) {
			return;
		}
		if (!c.isActive || c.disconnected || !c.isActiveAtomic()) {
			return;
		}
		if (WorldType.equalsType(WorldType.SPAWN)) {
			c.getPacketSender().sendFrame126("<col=ff0000>  - <col=ffffff>Server Information",
					663);
			PlayerHandler.updatePlayerCount2Lines();

			c.getPacketSender().sendFrame126("<col=ff0000> - <col=ffffff>Personal Information", 16028);
			long hoursPlayed = c.getHoursPlayed();
			if (hoursPlayed > 0) {
				long daysPlayed = c.getDaysPlayed();
				c.getPacketSender().sendFrame126("<col=ff7000>Time Played: <col=ffb000>" + (daysPlayed == 1 ? "1 Day or " : daysPlayed + " Days or ") + hoursPlayed + " Hrs", 16029);
			} else {
				c.getPacketSender().sendFrame126("<col=ff7000>Time Played: <col=ffb000>" + c.getMinutesPlayed() + " Minutes", 16029);
			}

			c.getPacketSender().sendFrame126(
					"<col=ff7000>Donation Points: <col=ffb000>" + c.getDonPoints(), 16030);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Amount Donated: <col=ffb000>" + c.getDonatedTotalAmount(),
					16031);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Loyalty Points: <col=ffb000>" + c.getLoyaltyPoints(), 16032);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Yells left: <col=ffb000>" + c.yellPoints, 16033);
			c.getPacketSender().sendFrame126("<col=ff0000> - <col=ffffff>Wildy Information",
					16034);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>ELO Rating: <col=ffb000>" + c.getEloRating(), 16035);
			c.getPacketSender()
					.sendFrame126("<col=ff7000>Player Killcount: <col=ffb000>" + c.KC, 16036);
			c.getPacketSender().sendFrame126("<col=ff7000>kills/deaths ratio: <col=ffb000>"
					+ RSConstants.getKDR(c.KC, c.DC), 16037);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Current Killstreak: <col=ffb000>" + c.killStreak, 16038);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Top Killstreak: <col=ffb000>" + c.highestKillStreak, 16039);
			c.getPacketSender()
					.sendFrame126(
							"<col=ff7000>Current Target: <col=ffb000>"
									+ (PlayerHandler.players[c.huntPlayerIndex] != null
											? PlayerHandler.players[c.huntPlayerIndex]
													.getNameSmartUp()
											: "None"),
							16040);
			/*
			 * sendFrame126("<col=ff0000> - <col=ffffff>Player Setups", 16041);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Set Melee", 16042);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Set Hybrid", 16043);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Set Zerker", 16044);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Set Pure NH", 16045);
			 * sendFrame126("<col=ff0000> - <col=ffffff>Supplies", 16046);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Normal Runes", 16047);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Barrage Runes", 16048);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Veng Runes", 16049);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>TB Runes", 16050);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Super Set", 16051);
			 * sendFrame126("<col=ff0000>[Click Here] <col=ff7000>Food", 16052);
			 */
		} else {
			c.getPacketSender().sendFrame126("<col=ff0000>  - <col=ffffff>Server Information",
					663);
			PlayerHandler.updatePlayerCount2Lines();

			c.getPacketSender()
			.sendFrame126("<col=ff7000>World " + Config.NODE_ID + ":"
					+ " <col=ffb000>" + (PlayerHandler.getPlayerCount())
					+ " players online", 16027);

			c.getPacketSender().sendFrame126(String.format("<col=ff7000>Server Time: <col=ffb000> %s", String.format("%d:%d %s EST", Server.getCalendar().getHour(), Server.getCalendar().getMinutes(), Server.getCalendar().getAmOrPm())), 16028);

			c.getPacketSender()
					.sendFrame126("<col=ff0000> - <col=ffffff>Personal Information", 16029);
			long hoursPlayed = c.getHoursPlayed();
			if (hoursPlayed > 0) {
				long daysPlayed = c.getDaysPlayed();
				c.getPacketSender().sendFrame126("<col=ff7000>Time Played: <col=ffb000>" + (daysPlayed == 1 ? "1 Day or " : daysPlayed + " Days or ") + hoursPlayed + " Hrs", 16030);
			} else {
				c.getPacketSender().sendFrame126("<col=ff7000>Time Played: <col=ffb000>" + c.getMinutesPlayed() + " Minutes", 16030);
			}

			c.getPacketSender().sendFrame126("<col=ff7000>Membership: <col=ffb000>0 days", 16031);
			// sendFrame126("<col=ffffff>- NOOOOOB", 663);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Game Mode: <col=ffb000>" + c.difficulty, 16032);
			if (c.difficulty == PlayerDifficulty.NORMAL) {
				c.getPacketSender().sendFrame126("<col=ff7000>Game Mode: <col=ffb000> Normal",
						16032);
			} else if (c.difficulty == PlayerDifficulty.MEDIUM) {
				c.getPacketSender().sendFrame126("<col=ff7000>Game Mode: <col=ffb000> Medium",
						16032);
			} else if (c.difficulty == PlayerDifficulty.HARD) {
				c.getPacketSender().sendFrame126("<col=ff7000>Game Mode: <col=ffb000> Hard",
						16032);
			} else if (c.difficulty == PlayerDifficulty.SUPREME) {
				c.getPacketSender()
						.sendFrame126("<col=ff7000>Game Mode: <col=ffb000> Supreme", 16032);
			} else if (c.isIronMan()) {
				if (c.isHCIronMan())
					c.getPacketSender()
						.sendFrame126("<col=ff7000>Game Mode: <col=ffb000> Hardcore Ironman", 16032);
				else
					c.getPacketSender()
						.sendFrame126("<col=ff7000>Game Mode: <col=ffb000> Ironman", 16032);
			} else if (c.difficulty == PlayerDifficulty.PRESTIGE_ONE) {
				c.getPacketSender().sendFrame126(
						"<col=ff7000>Game Mode: <col=ffb000> Lord", 16032);
			} else if (c.difficulty == PlayerDifficulty.PRESTIGE_TWO) {
				c.getPacketSender().sendFrame126(
						"<col=ff7000>Game Mode: <col=ffb000> Legend", 16032);
			} else if (c.difficulty == PlayerDifficulty.PRESTIGE_THREE) {
				c.getPacketSender().sendFrame126(
						"<col=ff7000>Game Mode: <col=ffb000> Extreme", 16032);
			}

			c.getPacketSender().sendFrame126(
					"<col=ff7000>NPC Killcount: <col=ffb000>" + c.npcKills, 16033);
			c.getPacketSender().sendFrame126("<col=ff7000>Slayer Points: <col=ffb000>"
					+ c.getSlayerPoints().getTotalValue(), 16034);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Duo Points: <col=ffb000>" + c.getDuoPoints().getTotalValue(),
					16035);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Donation Points: <col=ffb000>" + c.getDonPoints(),
					16036);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Amount Donated: <col=ffb000>" + c.getDonatedTotalAmount(),
					16037);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Loyalty Points: <col=ffb000>" + c.getLoyaltyPoints(), 16038);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Yells left: <col=ffb000>" + c.yellPoints, 16039);
			c.getPacketSender().sendFrame126("<col=ff0000> - <col=ffffff>Wildy Information",
					16040);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>ELO Rating: <col=ffb000>" + c.getEloRating(), 16041);
			c.getPacketSender()
					.sendFrame126("<col=ff7000>Player Killcount: <col=ffb000>" + c.KC, 16042);
			c.getPacketSender().sendFrame126("<col=ff7000>kills/deaths ratio: <col=ffb000>"
					+ RSConstants.getKDR(c.KC, c.DC), 16043);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Current Killstreak: <col=ffb000>" + c.killStreak, 16044);
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Top Killstreak: <col=ffb000>" + c.highestKillStreak, 16045);
			c.getPacketSender().sendFrame126("<col=ff7000>PK Points <col=ffb000>" + c.pkp,
					16046);
			c.getPacketSender()
					.sendFrame126(
							"<col=ff7000>Current Target: <col=ffb000>"
									+ (PlayerHandler.players[c.huntPlayerIndex] != null
											? PlayerHandler.players[c.huntPlayerIndex]
													.getNameSmartUp()
											: "None"),
							16047);
			c.getPacketSender().sendFrame126(
				     "<col=ff7000>Seasonal ELO Rating: <col=ffb000>" + c.getSeasonWildScore().getEloRating(), 16048);
			/*c.getPacketSender().sendFrame126("<col=ff0000> - <col=ffffff> Quests", 16047);
			c.getPacketSender().sendFrame126("<col=ff7000>Quests Under Development",
					16048);
			c.getPacketSender().sendFrame126("", 16049);
			c.getPacketSender().sendFrame126("", 16050);
			c.getPacketSender().sendFrame126("", 16051);
			c.getPacketSender().sendFrame126("", 16052);*/
		}
	}

	public int localizeLocation(int coordinate, int mapRegion) {
		return coordinate - 8 * mapRegion;
	}

	/**
	 * Magic on items
	 **/

	public void magicOnItems(int slot, int itemId, int spellId) {
		SpellsData spellData = SpellsData.getSpell(c, spellId);

		if (spellData == null) {
			return;
		}

		switch(spellData) {
			case ENCHANT_SAPPHIRE:
			case ENCHANT_EMERALD:
			case ENCHANT_RUBY:
			case ENCHANT_DIAMOND:
			case ENCHANT_DRAGONSTONE:
			case ENCHANT_ONYX:
			case ENCHANT_ZENYTE:
				EnchantJewelry.enchantItem(c, itemId, spellData);
				break;
			case LOW_ALCH:
				doAlch(1000, spellData, itemId, slot, 995, ShopPrice.getItemShopValue(itemId) / 3);
				break;
			case HIGH_ALCH:
				doAlch(2000, spellData, itemId, slot, 995, (int) (ShopPrice.getItemShopValue(itemId) * .75));
				break;
			case LOW_ALCH_DUNG: {
				if (c.dungParty == null || c.dungParty.dungManager == null) {
					break;
				}

				int value = (int) (c.getShops().getItemShopValue(itemId, 1, slot) * 0.20);
				if (value == 0 || itemId < 15750 || itemId > 18329) {
					c.sendMessage("You can't alch this.");
					break;
				}

				doAlch(1000, spellData, itemId, slot, DungeonConstants.RUSTY_COINS, (int) (value * 4.0f/3.0f));
				break;
			}
			case HIGH_ALCH_DUNG: {
				if (c.dungParty == null || c.dungParty.dungManager == null) {
					break;
				}

				int value = (int) (c.getShops().getItemShopValue(itemId, 1, slot) * 0.20);
				if (value == 0 || itemId < 15750 || itemId > 18329) {
					c.sendMessage("You can't alch this.");
					break;
				}

				doAlch(2000, spellData, itemId, slot, DungeonConstants.RUSTY_COINS, value * 2);
				break;
			}
			case SUPERHEAT_ITEM: {
				SmeltingData smeltingData = null;
				if (itemId == SmeltingData.IRON.getOre1Id()) {
					if (Smelting.canSmelt(c, SmeltingData.STEEL, false)) {
						smeltingData = SmeltingData.STEEL;
					} else {
						smeltingData = SmeltingData.IRON;
					}
				} else {
					smeltingData = SmeltingData.values.get(itemId);
				}

				if (smeltingData == null) {
					c.sendMessage("Nothing interesting happens.");
					break;
				}

				if (!Smelting.canSmelt(c, smeltingData, true)) {
					break;
				}

				if ((System.currentTimeMillis() - c.alchDelay) <= 2000 || !MagicRequirements.checkMagicReqsNew(c, spellData, true)) {
					return;
				}

				c.getPacketSender().playSound(spellData.getSoundId(), 100);
				Smelting.smelt(c, smeltingData, 1, spellData.getAnim(), spellData.getStartGfx());
				c.getPacketSender().sendFrame106(6);
				addSkillXP(spellData.getExpGain(), Skills.MAGIC);
				c.alchDelay = System.currentTimeMillis();
				break;
			}
			default:
				c.sendMessage("Nothing interesting happens.");
				break;
		}
	}

	private void doAlch(int delay, SpellsData spell, int itemId, int slot, int coinId, int amount) {
		if (itemId == coinId) {
			c.sendMessage("You can't alch coins.");
			return;
		}

		if (WorldType.equalsType(WorldType.SPAWN)) {
			c.sendMessage("Alching has been disabled in SoulPvP.");
			return;
		}

		if ((System.currentTimeMillis() - c.alchDelay) <= delay || !MagicRequirements.checkMagicReqsNew(c, spell, true)) {
			return;
		}

		c.getPacketSender().playSound(spell.getSoundId(), 100);
		c.getItems().deleteItemInOneSlot(itemId, slot, 1);
		c.getItems().addItem(coinId, amount);
		c.startAnimation(spell.getAnim());
		c.startGraphic(spell.getStartGfx());
		c.alchDelay = System.currentTimeMillis();
		c.getPacketSender().sendFrame106(6);
		addSkillXP(spell.getExpGain(), Skills.MAGIC);
		if (DBConfig.LOG_ALCH) {
			Server.getLogWriter().addToAlchMap(c.mySQLIndex, itemId);
		}
	}

	public void moveCheck(int xMove, int yMove) {
		if (RegionClip.getClippingDirection(c.getX() + xMove, c.getY() + yMove,
				c.heightLevel, xMove, yMove, c.getDynamicRegionClip())) {
			c.getPA().walkTo(xMove, yMove);
		}
	}
	
	public void movePlayerInstant(int x, int y, int z) {
		c.getMovement().hardResetWalkingQueue();
		if (!Misc.isInsideRenderedArea(c, x, y)) {
			c.mapRegionX = (x >> 3) - 6;
			c.mapRegionY = (y >> 3) - 6;
		}
		c.currentX = x - 8 * c.mapRegionX;
		c.currentY = y - 8 * c.mapRegionY;
		c.absX = x;
		c.absY = y;
		c.heightLevel = z;
		c.setLocation(c.absX, c.absY, c.heightLevel);
		c.didTeleport = true;
		if (c.getLockMovement().complete())
			c.getLockMovement().startTimer(1); // lock movement for 1 tick else step on this tick will look like player tele'd
	}

	public void movePlayer(int x, int y, int h) {
		if (h < 0) {
			c.sendMessage(
					"This teleport/object is bugged, please notify Developer.");
			return;
		}
		boolean changeRegion = false;
		if (c.heightLevel != h) {
			changeRegion = true;
		}

		if (c.playerIsInHouse && (x > 96 || y > 96)) {
			Construction.leaveHouse(c, false);
		}

		if (c.teleTimer > 0) {
			c.teleTimer = 0;
			c.resetAnimations();
		}

		c.resetTeleGrabEvent();
		c.getMovement().hardResetWalkingQueue();
		c.teleportToX = x;
		c.teleportToY = y;
		c.teleportToZ/* c.heightLevel */ = h;
		
		if (c.getFreezeTimer() > 0 && c.frozenBy > 0) {
			if (Misc.distanceToPoint(c.getX(), c.getY(), x, y) > 60)
				c.frozenBy = -1;
		}
		if (changeRegion && !c.updateRegion) {
			c.updateRegion = true;
			// c.updateRequired = true;//requestUpdates();
			// c.requestUpdates();
			// c.getPA().resetSkilling();
		}

		if (c.inCwGame && c.onCwWalls) {
			c.onCwWalls = false;
		}

		if (c.spectatorMode) {
			c.hidePlayer = false;
			c.spectatorMode = false;
		}

		/*
		 * for (EntityRegion r : RegionHandler.regions) { if (r.regionX ==
		 * c.lastX/64 && r.regionY == c.lastY/64) { r.removePlayer(c); } if
		 * (r.regionX == c.absX/64 && r.regionY == c.absY/64) { r.addPlayer(c);
		 * c.currentRegion = r; break; } }
		 */
	}

	public void movePlayer(Location location) {
		movePlayer(location.getX(), location.getY(), location.getZ());
	}
	
	public void movePlayerDelayed(int x, int y, int h, int anim, int delay) {
		final Animation animation;
		if (anim > 0) {
			switch (anim) {
			case 828:
				animation = c.isMoving ? Animation.CLIMB_UP_DELAYED : Animation.CLIMB_UP;
				break;
				
			case 844:
				animation = c.isMoving ? AgilityAnimations.CRAWL_UNDER_DELAYED : AgilityAnimations.CRAWL_UNDER;
				break;
				
				default:
					animation = new Animation(anim);
					break;
			}
		} else
			animation = null;
		movePlayerDelayed(x, y, h, animation, delay);
	}

	public void movePlayerDelayed(int x, int y, int h, Animation anim, int delay) {
		if (h < 0) {
			c.sendMessage(
					"This teleport/object is bugged, please notify the Developer.");
			return;
		}
		if (anim != null)
			c.startAnimation(anim);
		if (delay < 1) {
			c.sendMessage("Somebody messed up. Report to Devs.");
			return;
		}
		final boolean onWall = c.onCwWalls;
			
//		c.performingAction = true;
		c.lockActions(2);
		c.lockMovement(2);
		Server.getTaskScheduler().schedule(new Task(delay) {

			@Override
			protected void execute() {
				this.stop();
				
				if (c.isActive) {
					//c.performingAction = false;
					c.lockActions(0);
					c.lockMovement(0);
					c.getPA().movePlayer(x, y, h);
					if (onWall)
						c.onCwWalls = true;
				}

			}
		});

	}

	/**
	 * If the player is using melee and is standing diagonal from the opponent,
	 * then move towards opponent.
	 */

	public void movePlayerDiagonal(int i) {
		final Client c2 = (Client) PlayerHandler.players[i];
		boolean hasMoved = false;
		int c2X = c2.getX();
		int c2Y = c2.getY();
		if (PlayerConstants.goodDistance(c2X, c2Y, c.getX(), c.getY(), 1)) {
			if (c.getX() != c2.getX() && c.getY() != c2.getY()) {
				if (c.getX() > c2.getX() && !hasMoved) {
					if (RegionClip.getClippingDirection(c.getX() - 1, c.getY(),
							c.heightLevel, -1, 0, c.getDynamicRegionClip())) {
						hasMoved = true;
						walkTo(-1, 0);
					}
				} else if (c.getX() < c2.getX() && !hasMoved) {
					if (RegionClip.getClippingDirection(c.getX() + 1, c.getY(),
							c.heightLevel, 1, 0, c.getDynamicRegionClip())) {
						hasMoved = true;
						walkTo(1, 0);
					}
				}

				if (c.getY() > c2.getY() && !hasMoved) {
					if (RegionClip.getClippingDirection(c.getX(), c.getY() - 1,
							c.heightLevel, 0, -1, c.getDynamicRegionClip())) {
						hasMoved = true;
						walkTo(0, -1);
					}
				} else if (c.getY() < c2.getY() && !hasMoved) {
					if (RegionClip.getClippingDirection(c.getX(), c.getY() + 1,
							c.heightLevel, 0, 1, c.getDynamicRegionClip())) {
						hasMoved = true;
						walkTo(0, 1);
					}
				}
			}
		}
		hasMoved = false;
	}

	public int nomadReward() {
		return PlayerConstants.nomadReward[(int) (Math.random()
				* PlayerConstants.nomadReward.length)];
	}

	public void openBotBank(int tab) {
		if (c.getBot().inWild() && !c.getBot().isOwner()) {
			c.sendMessage("[BOT]You cannot bank in wildy.");
			return;
		}
		if (c.getBot().inTrade || c.getBot().tradeStatus == 1) {
			final Client o = (Client) PlayerHandler.players[c
					.getBot().tradeWith];
			if (o != null) {
				o.getTradeAndDuel().declineTrade();
			}
		}
		if (c.getBot().duelStatus == 1) {
			final Client o = (Client) PlayerHandler.players[c
					.getBot().duelingWith];
			if (o != null) {
				o.getTradeAndDuel().resetDuel();
			}
		}

		if (c.getBot().inWild() && !c.getBot().isOwner()) {
			c.sendMessage("[BOT]You cannot bank in wildy.");
			return;
		}
		if (c.getBot().underAttackBy > 0 || c.getBot().underAttackBy2 > 0) {
			c.sendMessage("[BOT]You cannot bank while in combat.");
			return;
		}
		if (c.getBot().inTrade || c.getBot().tradeStatus == 1) {
			final Client o = (Client) PlayerHandler.players[c
					.getBot().tradeWith];
			if (o != null) {
				o.getTradeAndDuel().declineTrade();
			}
		}
		if (c.getBot().duelStatus > 0) {
			c.getBot().getTradeAndDuel().declineDuel();
		}

		c.getPacketSender().sendFrame126("" + calculateTotalBankItems(), 22033);

		if (c.getBot().takeAsNote) {
			c.getPacketSender().setConfig(115, 1);
		} else {
			c.getPacketSender().setConfig(115, 0);
		}

		if (c.getOutStream() != null && c != null && c.isActive) {
			c.getBot().isBanking = true;
			// System.out.println("tab_"+tab);
			c.getBot().bankingTab = tab;
			if (!c.isBot()) {
				sendBotTabs();
			}
			c.getPacketSender().sendFrame126(
					Integer.toString(Config.BANK_TAB_SIZE), 22034);// Send
																	// max
																	// Bank
																	// Items
																	// -
																	// 496

			if (c.getBot().bankingTab == 0) {
				c.getBot().bankingItems = c.getBot().bankItems0;
				c.getBot().bankingItemsN = c.getBot().bankItems0N;
			}
			if (c.getBot().bankingTab == 1) {
				c.getBot().bankingItems = c.getBot().bankItems1;
				c.getBot().bankingItemsN = c.getBot().bankItems1N;
			}
			if (c.getBot().bankingTab == 2) {
				c.getBot().bankingItems = c.getBot().bankItems2;
				c.getBot().bankingItemsN = c.getBot().bankItems2N;
			}
			if (c.getBot().bankingTab == 3) {
				c.getBot().bankingItems = c.getBot().bankItems3;
				c.getBot().bankingItemsN = c.getBot().bankItems3N;
			}
			if (c.getBot().bankingTab == 4) {
				c.getBot().bankingItems = c.getBot().bankItems4;
				c.getBot().bankingItemsN = c.getBot().bankItems4N;
			}
			if (c.getBot().bankingTab == 5) {
				c.getBot().bankingItems = c.getBot().bankItems5;
				c.getBot().bankingItemsN = c.getBot().bankItems5N;
			}
			if (c.getBot().bankingTab == 6) {
				c.getBot().bankingItems = c.getBot().bankItems6;
				c.getBot().bankingItemsN = c.getBot().bankItems6N;
			}
			if (c.getBot().bankingTab == 7) {
				c.getBot().bankingItems = c.getBot().bankItems7;
				c.getBot().bankingItemsN = c.getBot().bankItems7N;
			}
			if (c.getBot().bankingTab == 8) {
				c.getBot().bankingItems = c.getBot().bankItems8;
				c.getBot().bankingItemsN = c.getBot().bankItems8N;
			}

			if (!c.isBot()) {
				c.getPacketSender().setScrollPos(5385, 0);

				c.getItems().resetItems(5064);
			}
			c.getBot().getItems().rearrangeBank();
			c.getItems().resetBank();
			c.getItems().resetTempItems();

			if (!c.isBot()) {
				c.getPacketSender().sendFrame248(5292, 5063);
				c.getPacketSender().sendFrame126(
						"     " + c.getBot().getName() + "'s Bank", 5383);
			}
		}
	}

	public void openClanInterface() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126("Clan Board", 6400);
		c.getPacketSender().sendFrame126("                  Clan Board", 6399);
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < Clan.CLANBOARD.length) {

				c.getPacketSender().sendFrame126(Clan.CLANBOARD[count], i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}
			count++;
		}
	}

	public void openClanWarsEloScoreboard() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Top Clans",
				6399);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Top Clans",
				6400);
		// fill 6402 to 6411 and 8578 to 8617 with player names
		// TODO: fill players to the rest of ids with a loop pref
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			// if(PKBoards.records.size() == count) break;
			if (count < ClanEloHandler.getTopClanWarClans().size()) {
				ClanInfo r = ClanEloHandler.getTopClanWarClans().get(count);
				if (r == null) {
					count++;
					continue;
				}
				String name = r.getClanTitle();
				if (name.length() > 20) {
					name = name.substring(0, 20);
				}
				c.getPacketSender()
						.sendFrame126((count + 1) + ": " + name + "  -  ELO: "
								+ r.getClanElo() + " -  Wins: "
								+ r.getClanWins() + "  -  Defeats: "
								+ r.getClanLosses(), i);
				name = null;
			} else {
				c.getPacketSender().sendFrame126("", i);
			}

			count++;
		}

	}

	public void openCommandsInfo() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Commands",
				6400);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Commands",
				6399);
		// fill 6402 to 6411 and 8578 to 8617 with player names
		// TODO: fill players to the rest of ids with a loop pref
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			// if(PKBoards.records.size() == count) break;
			if (count < Config.COMMANDS.length) {

				c.getPacketSender().sendFrame126(Config.COMMANDS[count], i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}
			count++;
		}
	}

	public void openDuelScoreboard() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Duel Scores",
				6399);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Duel Scores",
				6400);
		// fill 6402 to 6411 and 8578 to 8617 with player names
		// TODO: fill players to the rest of ids with a loop pref
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			// if(PKBoards.records.size() == count) break;
			if (count < DuelScoreBoard.getDuelScores().size()) {
				DuelScore r = DuelScoreBoard.getDuelScores().get(count);
				if (r == null) {
					count++;
					continue;
				}
				c.getPacketSender()
						.sendFrame126((count + 1) + ": " + r.getPlayerName()
								+ "  -  Wins: " + r.getKills() + "  -  Losses: "
								+ r.getDeaths() + "  -  KDR: " + r.getKdr(), i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}

			count++;
		}

	}
	
	public void openLmsScoreboard() {
		c.getPacketSender().showInterface(6308);
		final String title = Config.SERVER_NAME + " LMS Scores";
		c.getPacketSender().sendFrame126(title,
				6399);
		c.getPacketSender().sendFrame126(title,
				6400);
		// fill 6402 to 6411 and 8578 to 8617 with player names
		int count = 0;
		final int size = LmsScoreBoard.getLmsScores().size();
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < size) {
				LmsScore r = LmsScoreBoard.getLmsScores().get(count);
				if (r == null) {
					count++;
					continue;
				}
				StringBuilder sb = new StringBuilder();
				sb.append(Integer.toString(count+1)); // placement
				sb.append(": "); //seperator
				sb.append(r.getDisplayName());
				sb.append("  -  Wins: ");
				sb.append(Integer.toString(r.getWins()));
				sb.append("  -  Rating: ");
				sb.append(Integer.toString(r.getWinRating()));
				sb.append("  -  Kills: ");
				sb.append(Integer.toString(r.getKills()));
				sb.append("  -  Rating: ");
				sb.append(Integer.toString(r.getKillRating()));
				
				c.getPacketSender().sendFrame126(sb.toString(), i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}

			count++;
		}

	}

	public void openGambleRules() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126("Gambling Rules", 6400);
		c.getPacketSender().sendFrame126("Gambling Rules", 6399);
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < Config.GAMBLINGRULES.length) {

				c.getPacketSender().sendFrame126(Config.GAMBLINGRULES[count],
						i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}
			count++;
		}
	}
	
	public void openDuelRules() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126("Duel Rules", 6400);
		c.getPacketSender().sendFrame126("Duel Rules", 6399);
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < Config.DUELRULES.length) {

				c.getPacketSender().sendFrame126(Config.DUELRULES[count],
						i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}
			count++;
		}
	}
	

	public void openPKEloScoreboard() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Top Pkers",
				6399);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Top Pkers",
				6400);

		final int scoreCount = WildyEloRating.getWildyScores().size();
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < scoreCount) {
				WildyRecord r = WildyEloRating.getWildyScores().get(count);
				if (r == null) {
					count++;
					continue;
				}
				c.getPacketSender()
						.sendFrame126((count + 1) + ": " + r.getPlayerName()
								+ "  -  ELO: " + r.getElo() + "  -  KDR: "
								+ r.getKdr() + "  -  Kills: " + r.getKills()
								+ "  -  Deaths: " + r.getDeaths(), i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}

			count++;
		}

	}

	// public boolean teleHomeAction() {
	// if (c.homeTeleActions == 0) {
	// c.getPA().startTeleport(2134, 23423, 0, "modern");
	// } else if (c.homeTeleActions == 1) {
	// c.getPA().startTeleport(3233, 2326, 0, "modern");
	// }
	// return false;

	// }

	public void openThreadsInterface() {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Threads", 6400);
		c.getPacketSender().sendFrame126(Config.SERVER_NAME + " Threads", 6399);
		// fill 6402 to 6411 and 8578 to 8617 with player names
		// TODO: fill players to the rest of ids with a loop pref
		int count = 0;
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			// if(PKBoards.records.size() == count) break;
			if (count < Config.THREADS.length) {

				c.getPacketSender().sendFrame126(Config.THREADS[count], i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}
			count++;
		}
	}

	// public void updateSkill(int skillId) {
	// setSkillLevel(skillId, c.playerLevel[skillId], c.playerXP[skillId]);
	// }

	// public int getNpcId(int id) {
	// for (int i = 0; i < NPCHandler.maxNPCs; i++) {
	// if (NPCHandler.npcs[i] != null) {
	// if (NPCHandler.npcs[i].npcIndex == id) {
	// return i;
	// }
	// }
	// }
	// return -1;
	// }

	public void openUpBank() {
		// c.sendMessage("banking "+c.refreshBank);
		if (!c.isBanking) {
			c.getPA().closeAllWindows();
			c.getPA().resetSkilling();
		}
		c.refreshBank = true;
		c.isBanking = true;
		openUpBank(c.lastBankTabOpen);
	}

	/**
	 * Open bank
	 **/
	public void openUpBank(int tab) {

		if (c.toggleBot) {
			openBotBank(tab);
			return;
		}

		if (Config.isPvpWorld()) {
			if (!c.inPvpWorldSafe()) {
				c.sendMessage("You can only bank inside safe zones.");
				return;
			}
		} else {
			if (c.inWild() && !c.isOwner()) {
				c.sendMessage("You cannot bank in wildy.");
				return;
			}
		}
		if (c.inTrade || c.tradeStatus == 1) {
			final Client o = (Client) PlayerHandler.players[c.tradeWith];
			if (o != null) {
				o.getTradeAndDuel().declineTrade();
			}
		}
		if (c.duelStatus >= 1 && c.duelStatus <= 4) {
			c.getTradeAndDuel().declineDuel();
			// final Client o = (Client)PlayerHandler.players[c.duelingWith];
			// if (o != null)
			// {
			// // o.getTradeAndDuel().resetDuel();
			// o.getTradeAndDuel().declineDuel();
			// }
		}
		resetSkilling();

		if (RSConstants.inCastleWarsArena(c.getX(), c.getY())) {
			c.sendMessage("You can't bank here.");
			return;
		}
		if (c.insideDungeoneering()) {
			c.sendMessage("You can't bank here.");
			return;
		}
		if (c.inWild() && !c.isOwner()) {
			c.sendMessage("You cannot bank in wildy.");
			return;
		}
		if (c.underAttackBy > 0 || c.underAttackBy2 > 0) {
			c.sendMessage("You cannot bank while in combat.");
			return;
		}
		if (c.requestPinDelete) {
			if (c.enterdBankpin) {
				c.requestPinDelete = false;
				c.sendMessage(
						"[Notice] Your PIN pending deletion has been cancelled");
			} else if (c.getBankPin().dateExpired() && c.hasBankPin) {
				c.hasBankPin = false;
				c.requestPinDelete = false;
				c.sendMessage(
						"[Notice] Your PIN has been deleted. It is recommended "
								+ "to have one.");
			}
		}
		if (c.requireBankPinToOpen()) {
			c.bankPinOpensBank = true;
			c.getBankPin().openPin();
			return;
		}
		if (c.inTrade || c.tradeStatus == 1) {
			final Client o = (Client) PlayerHandler.players[c.tradeWith];
			if (o != null) {
				o.getTradeAndDuel().declineTrade();
			}
		}
		if (c.duelStatus > 0) {
			// Client o = (Client) PlayerHandler.players[c.duelingWith];
			// if (o != null) {
			// o.getTradeAndDuel().resetDuel();
			// }
			c.getTradeAndDuel().declineDuel();
		}

		if (c.takeAsNote) {
			c.getPacketSender().setConfig(115, 1);
		} else {
			c.getPacketSender().setConfig(115, 0);
		}

		if (c.getOutStream() != null && c != null && c.isActive) {
			c.isBanking = true;
			// System.out.println("tab_"+tab);
			c.bankingTab = tab;
			if (!c.isBot() && c.refreshBank) {
				sendTabs();
			}
			c.getPacketSender().sendFrame126(
					Integer.toString(Config.BANK_TAB_SIZE-2), 22034);

			if (c.bankingTab == 0) {
				c.bankingItems = c.bankItems0;
				c.bankingItemsN = c.bankItems0N;
			}
			if (c.bankingTab == 1) {
				c.bankingItems = c.bankItems1;
				c.bankingItemsN = c.bankItems1N;
			}
			if (c.bankingTab == 2) {
				c.bankingItems = c.bankItems2;
				c.bankingItemsN = c.bankItems2N;
			}
			if (c.bankingTab == 3) {
				c.bankingItems = c.bankItems3;
				c.bankingItemsN = c.bankItems3N;
			}
			if (c.bankingTab == 4) {
				c.bankingItems = c.bankItems4;
				c.bankingItemsN = c.bankItems4N;
			}
			if (c.bankingTab == 5) {
				c.bankingItems = c.bankItems5;
				c.bankingItemsN = c.bankItems5N;
			}
			if (c.bankingTab == 6) {
				c.bankingItems = c.bankItems6;
				c.bankingItemsN = c.bankItems6N;
			}
			if (c.bankingTab == 7) {
				c.bankingItems = c.bankItems7;
				c.bankingItemsN = c.bankItems7N;
			}
			if (c.bankingTab == 8) {
				c.bankingItems = c.bankItems8;
				c.bankingItemsN = c.bankItems8N;
			}

			if (!c.isBot() && c.refreshBank) {

				// TODO count all the items in a players bank
				c.getPacketSender().sendFrame126("" + bankingCount()/*calculateTotalBankItems()*/,
						22033);

				c.getItems().resetItems(5064);
			}
			c.lastBankTabOpen = tab;
			c.getItems().rearrangeBank();
			c.getItems().resetBank();
			c.getItems().resetTempItems();

			if (!c.isBot() && c.refreshBank) {
				c.getPacketSender().sendFrame248(5292, 5063);
				if (c.getX() == 3308 && c.getY() == 3120) { // using funpk bank
					updateGearBonusesInBank();
				} else {
					c.getPacketSender().sendFrame126(
							"     " + c.getNameSmartUp() + "'s Bank", 5383);
				}
			}
		}
	}

	public void otherBank(Client c, Client o) {
		if (o == c || o == null || c == null) {
			return;
		}

		int backupItems[] = new int[Config.BANK_TAB_SIZE];
		int backupItemsN[] = new int[Config.BANK_TAB_SIZE];

		int count = 0;
		for (int i = 0; i < o.bankItems0.length; i++) {
			backupItems[i] = c.bankItems0[i];
			backupItemsN[i] = c.bankItems0N[i];
			c.bankItems0N[i] = o.bankItems0N[i];
			c.bankItems0[i] = o.bankItems0[i];
			if (c.bankItems0[i] > 0) {
				count++;
			}
		}

		try {

			if (count > c.bankItems0.length) {
				c.sendMessage("Exceeded bank limit.");
				openUpBank(0);

				c.getPacketSender()
						.sendFrame126("     " + o.getNameSmartUp() + "'s Bank", 5383);

				for (int i = 0; i < o.bankItems0.length; i++) {
					c.bankItems0N[i] = backupItemsN[i];
					c.bankItems0[i] = backupItems[i];
				}
				return;
			}
			for (int i = 0; i < o.bankItems1.length; i++) {
				if (o.bankItems1[i] > 0) {
					c.bankItems0[count] = o.bankItems1[i];
					c.bankItems0N[count] = o.bankItems1N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems2.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems2[i];
					c.bankItems0N[count] = o.bankItems2N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems3.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems3[i];
					c.bankItems0N[count] = o.bankItems3N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems4.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems4[i];
					c.bankItems0N[count] = o.bankItems4N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems5.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems5[i];
					c.bankItems0N[count] = o.bankItems5N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems6.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems6[i];
					c.bankItems0N[count] = o.bankItems6N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems7.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems7[i];
					c.bankItems0N[count] = o.bankItems7N[i];
					count++;
				}
			}
			for (int i = 0; i < o.bankItems8.length; i++) {
				if (o.bankItems1[i] > 0) {
					if (count > c.bankItems0.length) {
						break;
					}
					c.bankItems0[count] = o.bankItems8[i];
					c.bankItems0N[count] = o.bankItems8N[i];
					count++;
				}
			}

			openUpBank(0);

			c.getPacketSender().sendFrame126("     " + o.getNameSmartUp() + "'s Bank",
					5383);

			for (int i = 0; i < o.bankItems0.length; i++) {
				c.bankItems0N[i] = backupItemsN[i];
				c.bankItems0[i] = backupItems[i];
			}

		} catch (Exception ex) {
			c.sendMessage("error with bank checking. resetting all.");
			c.sendMessage("Send this to Ray:");
			c.sendMessage(ex.getMessage());
			ex.printStackTrace();
			for (int i = 0; i < o.bankItems0.length; i++) {
				c.bankItems0N[i] = backupItemsN[i];
				c.bankItems0[i] = backupItems[i];
			}
		}
	}

	public void otherBank2(Client c, Client o, int tab) {
		if (o == c || o == null || c == null) {
			return;
		}
		c.saveFile = false;

		int backupItems0[] = c.bankItems0;
		int backupItems0N[] = c.bankItems0N;

		int backupItems1[] = c.bankItems1;
		int backupItems1N[] = c.bankItems1N;

		int backupItems2[] = c.bankItems2;
		int backupItems2N[] = c.bankItems2N;

		int backupItems3[] = c.bankItems3;
		int backupItems3N[] = c.bankItems3N;

		int backupItems4[] = c.bankItems4;
		int backupItems4N[] = c.bankItems4N;

		int backupItems5[] = c.bankItems5;
		int backupItems5N[] = c.bankItems5N;

		int backupItems6[] = c.bankItems6;
		int backupItems6N[] = c.bankItems6N;

		int backupItems7[] = c.bankItems7;
		int backupItems7N[] = c.bankItems7N;

		int backupItems8[] = c.bankItems8;
		int backupItems8N[] = c.bankItems8N;

		c.bankItems0 = o.bankItems0;
		c.bankItems0N = o.bankItems0N;

		c.bankItems1 = o.bankItems1;
		c.bankItems1N = o.bankItems1N;

		c.bankItems2 = o.bankItems2;
		c.bankItems2N = o.bankItems2N;

		c.bankItems3 = o.bankItems3;
		c.bankItems3N = o.bankItems3N;

		c.bankItems4 = o.bankItems4;
		c.bankItems4N = o.bankItems4N;

		c.bankItems5 = o.bankItems5;
		c.bankItems5N = o.bankItems5N;

		c.bankItems6 = o.bankItems6;
		c.bankItems6N = o.bankItems6N;

		c.bankItems7 = o.bankItems7;
		c.bankItems7N = o.bankItems7N;

		c.bankItems8 = o.bankItems8;
		c.bankItems8N = o.bankItems8N;

		openUpBank(tab);

		c.getPacketSender().sendFrame126("     " + o.getNameSmartUp() + "'s Bank",
				5383);

		c.bankItems0 = backupItems0;
		c.bankItems0N = backupItems0N;

		c.bankItems1 = backupItems1;
		c.bankItems1N = backupItems1N;

		c.bankItems2 = backupItems2;
		c.bankItems2N = backupItems2N;

		c.bankItems3 = backupItems3;
		c.bankItems3N = backupItems3N;

		c.bankItems4 = backupItems4;
		c.bankItems4N = backupItems4N;

		c.bankItems5 = backupItems5;
		c.bankItems5N = backupItems5N;

		c.bankItems6 = backupItems6;
		c.bankItems6N = backupItems6N;

		c.bankItems7 = backupItems7;
		c.bankItems7N = backupItems7N;

		c.bankItems8 = backupItems8;
		c.bankItems8N = backupItems8N;

		c.saveFile = true;
	}

	public void otherInv(Client c, Client o) {
		if (o == c || o == null || c == null) {
			return;
		}

		int backupInvItems[] = new int[28];
		int backupInvItemsN[] = new int[28];

		for (int i = 0; i < o.playerItems.length; i++) {
			backupInvItems[i] = c.playerItems[i];
			backupInvItemsN[i] = c.playerItemsN[i];// c.playerItemsN[i] =
													// c.playerItemsN[i];
			c.playerItemsN[i] = o.playerItemsN[i];
			c.playerItems[i] = o.playerItems[i];
		}

		c.getItems().updateInventory();

		// for (int i = 0; i < c.playerItems.length; i++)
		for (int i = 0; i < backupInvItems.length; i++) {
			c.playerItemsN[i] = backupInvItemsN[i];
			c.playerItems[i] = backupInvItems[i];
		}
		// backupInvItems = new int[28];
		// backupInvItemsN = new int[28];
	}

	public void otherInv2(Client c, Client o) {
		if (o == c || o == null || c == null) {
			return;
		}
		c.saveFile = false;

		int backupInvItems[] = c.playerItems;
		int backupInvItemsN[] = c.playerItemsN;

		c.playerItemsN = o.playerItemsN;
		c.playerItems = o.playerItems;

		c.getItems().updateInventory();

		c.playerItemsN = backupInvItemsN;
		c.playerItems = backupInvItems;

		c.saveFile = true;
	}

	public void playerWalk(int x, int y) {
		if (c.isDead() || c.getHitPoints() <= 0) {
			return;
		}
		if (c.isBot()) {
			if (Misc.distanceToPoint(c.getX(), c.getY(), x, y) > 60) {
//				System.out
//						.println("Distance is too far for bot " + c.getName());
				return;
			}
		}
		PathFinder.findRoute(c, x, y, true, 1, 1);
	}

	// public void castleWarsObjects() {
	// object(-1, 2373, 3119, -3, 10);
	// object(-1, 2372, 3119, -3, 10);
	// }

	public void prayerRenewal(int heal) {
		c.prayerRenew = heal;
		if (heal <= 0) {
			c.getPacketSender().sendWidget(DisplayTimerType.PRAYER_RENEWALS, 0);
		} else {
			c.getPacketSender().sendWidget(DisplayTimerType.PRAYER_RENEWALS, heal);
		}
	}

	public void processTeleport() {
		c.teleportToX = c.teleX;
		c.teleportToY = c.teleY;
		c.teleportToZ = c.teleHeight;
		if (c.teleEndAnimation != null) {
			c.startAnimation(c.teleEndAnimation);
		}
		if (c.teleEndGfx != null) {
			c.startGraphic(c.teleEndGfx);
		}
		if (c.inBossRoom) {
			c.inBossRoom = false;
		}
		if (c.spectatorMode) {
			c.hidePlayer = false;
			c.spectatorMode = false;
		}
		c.resetTeleGrabEvent();
	}

	public int purpleWildyKey() {
		return PlayerConstants.purpleWildyKey[(int) (Math.random()
				* PlayerConstants.purpleWildyKey.length)];
	}

	public void QuestReward(String questName, int PointsGain, String Line1,
			String Line2, String Line3, String Line4, String Line5,
			String Line6, int itemID) {
		if (c.isBot()) {
			return;
		}
		c.getPacketSender()
				.sendFrame126("You have completed " + questName + "!", 12144);
		c.getPacketSender().sendQuest(Integer.toString(Variables.QUEST_POINTS.getValue(c)), 12147);
		// c.QuestPoints += PointsGain;
		c.getPacketSender().sendQuest(Line1, 12150);
		c.getPacketSender().sendQuest(Line2, 12151);
		c.getPacketSender().sendQuest(Line3, 12152);
		c.getPacketSender().sendQuest(Line4, 12153);
		c.getPacketSender().sendQuest(Line5, 12154);
		c.getPacketSender().sendQuest(Line6, 12155);
		c.getPacketSender().sendFrame246(12145, 250, itemID);
		c.getPacketSender().showInterface(12140);
		Server.getStillGraphicsManager().stillGraphics(c, c.getX(), c.getY(),
				0, 199, 0);
	}

	public int randomArtifact() {
		return RSConstants.arti[(int) (Math.random()
				* RSConstants.arti.length)];
	}

	public int randomBarrows() {
		return RSConstants.Barrows[(int) (Math.random()
				* RSConstants.Barrows.length)];
	}

	public int randomCrystal() {
		return PlayerConstants.Crystal[(int) (Math.random()
				* PlayerConstants.Crystal.length)];
	}

	public int randomPkGear() {
		return PlayerConstants.pkGear[(int) (Math.random()
				* PlayerConstants.pkGear.length)];
	}

	public int randomPvpGear() {
		return PlayerConstants.pvpGear[(int) (Math.random()
				* PlayerConstants.pvpGear.length)];
	}

	public int randomRareSkillPackage() {
		return RSConstants.rareSkillingPackage[(int) (Math.random()
				* RSConstants.rareSkillingPackage.length)];
	}

	public int randomSkillPackage() {
		return RSConstants.skillingPackage[(int) (Math.random()
				* RSConstants.skillingPackage.length)];
	}

	public int redWildyKey() {
		return PlayerConstants.redWildyKey[(int) (Math.random()
				* PlayerConstants.redWildyKey.length)];
	}

	public void removeAllItems() {
		for (int i = 0; i < c.playerItems.length; i++) {
			c.playerItems[i] = 0;
		}
		for (int i = 0; i < c.playerItemsN.length; i++) {
			c.playerItemsN[i] = 0;
		}
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
	}

	/**
	 * removes all windows without resetting anything but interface info
	 */
	public void removeAllWindows() {
		c.getPacketSender().closeInterfaces();
		resetInterfaceInfo();
	}

	public void removeTabs() {
		c.getPacketSender().sendFrame126b("0", 27001);
		c.getPacketSender().sendFrame126b("0", 27002);
		
		c.getPacketSender().displayItemOnInterface(22035, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22036, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22037, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22038, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22039, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22040, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22041, -1, 0, 1);
		c.getPacketSender().displayItemOnInterface(22042, -1, 0, 1);
		
		// tells client to update
		c.getPacketSender().sendFrame126b("1", 27000);
	}

	public void resetAutocast() {
		c.getPacketSender().setConfig(108, 0);
		c.magicDef = false;
		c.getPacketSender().updateInterfaceEmpty(5);
		c.setAutoCastSpell(null);
	}

	public void resetFollow() {
		c.followId = 0;
		c.followId2 = 0;
		c.resetFace();
	}

	public void resetHuntId() {
		c.setWildernessTick(0);
		c.huntPlayerIndex = 0;
		c.getPacketSender().createPlayerHints(10, -1);
		c.getPacketSender().createObjectHints(3333, 3333, 100, -1);
		// c.requestUpdates();
		c.sendMessage("Your target has been reset.");
		c.BountyTargetLimit = 0;
		if (WorldType.equalsType(WorldType.SPAWN)) {
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Current Target: <col=ffb000>" + c.getNameSmartUp(), 16040);
		} else {
			c.getPacketSender().sendFrame126(
					"<col=ff7000>Current Target: <col=ffb000>" + c.getNameSmartUp(), 16045);
		}
	}

	public void resetInterfaceInfo() {
		c.xRemoveSlot = 0;
		c.xInterfaceId = 0;
		c.xRemoveId = 0;
		c.interfaceIdOpenMainScreen = 0;
		c.sidebarInterfaceOpen = 0;
		c.enterAmount = 0;
		c.pollData = null;
		c.openSmithingData = null;
		c.smeltInterface = false;
		c.selectedDungReward = null;
		c.selectedLampType = null;
	}

	/**
	 * reseting animation
	 **/
	public void resetMovementAnimation() {
		// c.sendMessage("called");
		if (c.getPauseAnimReset() > 0) {
			return;
		}
		c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
		c.startAnimation(c.getMovement().getPlayerMovementAnimIds()[0]/* c.playerStandIndex */);
	}

	public void resetMyBountyHunters() {
		if (c.huntedBy.size() > 0) {
			for (int i = 0; i < c.huntedBy.size(); i++) {
				if (PlayerHandler.players[c.huntedBy.get(i)] == null) {
					continue;
				}
				final Client o = (Client) PlayerHandler.players[c.huntedBy
						.get(i)];
				if (o != null && !o.forceDisconnect) {
					o.getPA().resetHuntId();
				}
			}
			c.huntedBy.clear();
		}
	}

	public void resetSkilling() {
		for (int i = 0; i < c.playerSkilling.length; i++) {
			c.playerSkilling[i] = false;
		}
	}

	public void resetTb() {
		// c.teleBlockLength = 0;
		// c.teleBlockDelay = 0;
		c.setTeleBlockTimer(0);
	}

	/*
	 * public void fixAllBarrows() { int totalCost = 0; int cashAmount =
	 * c.getItems().getItemAmount(995); for (int j = 0; j <
	 * c.playerItems.length; j++) { boolean breakOut = false; for (int i = 0; i
	 * < c.getItems().brokenBarrows.length; i++) { if (c.playerItems[j]-1 ==
	 * c.getItems().brokenBarrows[i][1]) { if (totalCost + 80000 > cashAmount) {
	 * breakOut = true; c.sendMessage("You have run out of money."); break; }
	 * else { totalCost += 80000; } c.playerItems[j] =
	 * c.getItems().brokenBarrows[i][0]+1; } } if (breakOut) break; } if
	 * (totalCost > 0) c.getItems().deleteItem(995,
	 * c.getItems().getItemSlot(995), totalCost); }
	 */

	public void resetTzhaar() {
		c.waveId = -1;
		c.tzhaarToKill = -1;
		c.tzhaarKilled = -1;
		// c.getPA().movePlayer(2438, 5168, 0);
	}

	public void resetVariables() {
		// c.getCrafting().resetCrafting();
		c.teleAction = 0;
		c.dialogueAction = 0;
		c.nextChat = 0;
	}

	public void resetVariablesOnDeath() {
		if (c.inFightCaves()) {
			c.getPA().resetTzhaar();
		}

		c.attr().put("canAttack", false);

		c.respawnTimer = 16; // keeping this just to cheaphax dying only once in
								// process

		c.getMovement().resetWalkingQueue();
		if (!c.getTradeAndDuel().stakedItems.isEmpty() && c.duelStatus != 6) {
			c.getTradeAndDuel().stakedItems.clear();
		}
		c.getPA().clearNpcDamage();
		c.setAttackTimer(10);
		closeAllWindows();
		if (c.isBot()) {
			c.underAttackBy = 0;
		}
		c.getCombat().resetPriorityLeftClick();

	}

	public void resetXAmountInfo() {
		c.xRemoveSlot = 0;
		c.xInterfaceId = 0;
		c.xRemoveId = 0;
	}

	public void restorePlayer(boolean hardReset) {
		c.setSkull(false);
		c.setSkullTimer(-1);
		c.getSkills().restore(hardReset);
		c.lastVeng = 0;
		c.resetRoot();
		c.stunTimer.reset();
		c.vengOn = false;
		c.resetMiasmicEffect();
		c.vengDelay.reset();
		c.setFreezeTimer(1);
		c.blockRun = false;
		c.getDotManager().reset();
		c.specAmount = 10.0;
		c.usingSpecial = false;
		c.queueGmaulSpec = false;
		c.getPoisonImmuneTimer().reset();
		c.getVenomImmunityTimer().reset();
		c.getItems().addSpecialBar(true);
		c.getMovement().restoreRunEnergy(100);
		if (c.getSummoning() != null)
			c.getSummoning().setFamiliarSpecialEnergy(60);
		c.getPA().prayerRenewal(0);

		//reset autocast but dont reset autocast on powered staffs
		if (!ItemConstants.isPoweredStaff(c.playerEquipment[PlayerConstants.playerWeapon])) {
			resetAutocast();
		}
	}

	public void rewardDuelKiller(final Client rewardedKiller) {
		if (c.duelStatus == 5 && rewardedKiller.getHitPoints() <= 0) {
			// todo: draw
			c.getTradeAndDuel().sendBothDraw(rewardedKiller);
			rewardedKiller.sendMessage("It's a draw. Both get items back.");
			// DuelClient is now null after the next function,
			// declineDuelMakeDraw
			c.sendMessage("It's a draw. Both get items back.");
		} else {
			if (rewardedKiller.duelStatus == 5) {
				rewardedKiller.getTradeAndDuel().loadWinnerStake(c);
				rewardedKiller.duelStatus = 6;
				rewardedKiller.getSkills().healToFull();
				rewardedKiller.sendMessage("You have been healed");
				rewardedKiller.getPA().restorePlayer(true);
				if (rewardedKiller.isDead()) {
					rewardedKiller.setDead(false);
					rewardedKiller.respawnTimer = -6;
				}
				rewardedKiller.performingAction = true; // set this to true so
														// other player cannot
														// do anything till dead
														// player respawns
				CycleEventHandler.getSingleton().addEvent(rewardedKiller,
						new CycleEvent() {

							int timer = 6;

							@Override
							public void execute(CycleEventContainer container) {

								if (timer == 1) {
									rewardedKiller.getTradeAndDuel()
											.duelVictory(c);
								}
								if (timer == 0) {
									container.stop();
								}
								timer--;
							}

							@Override
							public void stop() {
								if (rewardedKiller != null) {
									rewardedKiller.getTradeAndDuel()
											.claimStakedItemsNew();
									rewardedKiller.performingAction = false;
								}
							}
						}, 1);
			}
		}

		c.getTradeAndDuel().resetDuelNew();
		rewardedKiller.getTradeAndDuel().resetDuelNew();
	}

	public void rewardKiller(Client o) {
		if (o == c)
			return;
		o.getCombat().resetPriorityLeftClick();
		if (o.isOwner()) {
			c.safeDeath = true;
		}
		if (o.inFunPk()) {
			c.safeDeath = true;
		}
		if (o.whitePortalSafe()) {
			c.safeDeath = true;
		}
		if (o.playerRights == 2) {
			c.safeDeath = true;
		}
		if (c.isOwner()) {
			c.safeDeath = true;
		}
		if (c.isIronMan() && !c.isHCIronMan() && !c.inWild()) {
			c.safeDeath = true;
		}

		if (DBConfig.LOG_KILL) {
			Server.getLogWriter().addToKillList(new Kill(o, c));
		}
		
		if (c != o && c.wildLevel > 0) {
			int pkingPot = PkingPotEvent.getGoldFromPot();

			if (pkingPot > 0) {
				ItemHandler.createGroundItem(o, 995, c.getX(), c.getY(), c.getZ(), pkingPot);
				o.sendMessage("You see " + pkingPot + " coins drop on the ground from the PKing pot.");
			}
		}

		if (RSConstants.inCastleWarsArena(c.getX(), c.getY()) || RSConstants.inSoulWars(c.getX(), c.getY())) {
			if (o.goldObtainedInMinigames < 15000000) {
				int receiveAmount = 300000;
				MoneyPouch.addToMoneyPouch(o, receiveAmount);
				o.goldObtainedInMinigames += receiveAmount;
				o.sendMessage("You have received 300,000 coins for killing your opponent.");
			} else {
				o.sendMessage("You have received the maximum reward for killing opponents.");
			}
		}

		if (c.wildLevel > 0) {
			c.lastLongKilledBy = o.getLongName();
			o.lastLongKilled = c.getLongName();
		}

		// System.out.println("wildLevel:"+c.wildLevel+"
		// understtackBy:"+c.underAttackBy+"
		// EquipmentAmount:"+equipAmount()+"
		// getBonuses:"+c.getItems().getTotalBonuses());

		if (!AntiRush.getRusherUUID().contains(o.UUID)) {
			if (c.wildLevel > 0 && c.getItems().getTotalBonuses() > 400) {

				if (((o.huntPlayerIndex == c.getId()) || !PlayerKilling
						.hostOnList(o, c.UUID))
						&& !c.safeDeath && !Config.isOwner(c) && !c.UUID.equals(
								o.UUID)) {
					if (!PlayerKilling.hostOnList(o,
							c.UUID/* c.connectedFrom */)) {
						PlayerKilling.addHostToList(o,
								c.UUID/* c.connectedFrom */);
					}
					
					WildyEloRatingSeason.applyKill(o, c);

					c.lastDayPkedOfYear = Server.getCalendar().getDayOfYear();
					o.lastDayPkedOfYear = Server.getCalendar().getDayOfYear();

					int prevWinnerRating = o.getEloRating();
					int prevLoserRating = c.getEloRating();
					
					o.setEloRating(
							EloRatingSystem.getNewRating(prevWinnerRating,
									prevLoserRating, EloRatingSystem.WIN),
							true);
					c.setEloRating(
							EloRatingSystem.getNewRating(prevLoserRating,
									prevWinnerRating, EloRatingSystem.LOSE),
							true);

					int newWinnerRating = o.getEloRating();
					
					c.sendMessage("Your Wilderness Elo Rating is now " + c.getEloRating());
					
					o.sendMessage("Your Wilderness Elo Rating is now " + o.getEloRating());
					
					o.increaseProgress(TaskType.EARN_ELO, 0, Math.max(0, newWinnerRating - prevWinnerRating));

					if (o.getEloRating() > 1399) {
						o.getAchievement().eloRating1400();
					}
					if (o.getEloRating() > 1599) {
						o.getAchievement().eloRating1600();
					}

					// clan elo rating
					if (o.getClan() != null && c.getClan() != null && !c.isBot() && !c.getClan().getFounder().equals("help") && !o.getClan().getFounder().equals("help")
							&& !c.getClan().getFounder().equals(o.getClan().getFounder())
							&& c.getClan().isRanked(c.getName())
							&& o.getClan().isRanked(o.getName())) {
						o.getClan()
								.setEloRating(EloRatingSystem.getNewRating(
										o.getClan().getEloRating(),
										c.getClan().getEloRating(),
										EloRatingSystem.WIN));
						c.getClan()
								.setEloRating(EloRatingSystem.getNewRating(
										c.getClan().getEloRating(),
										o.getClan().getEloRating(),
										EloRatingSystem.LOSE));
					}

					c.DC++;
					o.KC++;
					o.getAchievement().DefeatPlayer();
					o.getAchievement().defeat10Players();
					o.getAchievement().defeat60Players();
					Titles.killTenPlayers(o);
					Titles.killFiftyPlayers(o);
					Titles.killHundredPlayers(o);
					Titles.killTwoHundredPlayers(o);
					Titles.killThousendPlayers(o);
					Titles.dieOnce(c);
					Titles.dieTenTimes(c);
					Titles.dieFiftyTimes(c);
					Titles.dieHundredTimes(c);

					o.specAmount += 7.0;
					if (o.specAmount > 10.0) {
						o.specAmount = 10.0;
					}
					o.getItems().updateSpecialBar();

					o.killStreak++;
					if (o.killStreak > o.highestKillStreak) {
						o.highestKillStreak = o.killStreak;
					}

					if (o.killStreak >= 3) {
						o.getAchievement().threeKillstreak();
					}
					if (o.killStreak >= 15) {
						o.getAchievement().fifteenKillstreak();
					}
					if (o.killStreak >= 20) {
						o.getAchievement().finishedKillStreak20Kills();
					}

					if (o.killStreak > 6) {
						PlayerHandler.messageAllPlayers(
								"<shad=12632256>" + o.getNameSmartUp() + " is on a "
										+ o.killStreak + " kills kill-streak!");
					}

					if (c.killStreak > 6) {
						PlayerHandler.messageAllPlayers("<shad=12632256>"
								+ o.getNameSmartUp() + " has ended " + c.getNameSmartUp()
								+ "'s " + c.killStreak + " kill kill-streak!");
					}

					// o.sendMessage("PrevRating "+prevRating+" current
					// rating "+o.eloRating
					// +" points extra earned:"+(o.eloRating-prevRating));
					int eloBoostedPoints = (o.getEloRating() - prevWinnerRating)
							+ (int) (Math.round(
									(o.getEloRating() - prevWinnerRating) * 0.05));
					if (eloBoostedPoints < 0 || c.isBot()) {
						eloBoostedPoints = 0;
					}
					
//					int killStreakBoost = Misc.interpolateSafe(2, 150, 4, 200, c.killStreak);
					
					double killStreakBoost = Misc.interpolateDouble(1.10, 3.00, 7, 200, c.killStreak);
					
					int random = 3 * (c.wildLevel / 10) + Misc.random(30)
							+ eloBoostedPoints + c.killStreak;
					
					o.sendMessage("You gain "+ ((int)(killStreakBoost*100-100)) + "% extra PK Points for ending a kill streak.");
					
					random *= killStreakBoost;
					
					int bloodMoney = 3 * (c.wildLevel / 10) + Misc.random(20)
							+ eloBoostedPoints + c.killStreak + 180;
					
					bloodMoney *= killStreakBoost;

					if (c.isBot()) {
						random = 3 * (c.wildLevel / 2) + Misc.random(20);
					}

					if (o.huntPlayerIndex == c.getId()) {
						o.sendMessage(
								"Congratulations on killing your target!");
						o.getAchievement().defeatTarget();
						if (!WorldType.equalsType(WorldType.SPAWN)) {
							int rewardId = getHunterReward();
							if (c.isBot()) {
								rewardId = -1;
								rewardId = getHunterRewardFromKillingBot();
							}
							if (rewardId > 0) {
								ItemDefinition def = ItemDefinition
										.forId(rewardId);
								if (def != null) {
									o.sendMessage("<col=ff00>Your bounty dropped: "
											+ def.getName());
									if (def.getDropValue() > 1000000) {
										PlayerHandler.messageAllPlayers(
												"<col=800000>" + o.getNameSmartUp()
														+ " <col=00330066>got a bounty drop: </col><col=669900>"
														+ def.getName());
									}
								}
								if (o.isBot()) {
									o.getItems().addItemToBank(rewardId, 1);
								} else {
									ItemHandler.createGroundItem(o, rewardId,
											c.getX(), c.getY(),
											c.getHeightLevel(), 1);
								}
							}
							if (Misc.random(100) < 80) {
								ItemHandler.createGroundItem(o,
										o.getItems().getRandomFoodNoted(),
										c.getX(), c.getY(), c.getHeightLevel(),
										10 + Misc.random(50));
							}
						} else {
							bloodMoney *= 2;
							// ItemHandler.createGroundItem(o, 8890, c.getX(),
							// c.getY(), c.getHeightLevel(),
							// bloodMoney + Misc.random(50));
							// o.sendMessage("<col=ff00>Your bounty dropped some
							// extra blood money!");
						}
						o.getPA().resetHuntId();
						c.getPA().resetHuntId();
						c.huntedBy.clear();
						random *= 2;
					}
					c.killStreak = 0;
					o.getAchievement().earn2000PKPoints(random);
					if (!WorldType.equalsType(WorldType.SPAWN)) {
						int pkPoints = random;
						if (o.pkpBoostActive()) {
							int extraPkp = pkPoints * 50 / 100;
							if (extraPkp > 0) {
								if (extraPkp == 1) {
									o.sendMessage("The scroll has awarded you <col=ff0000>1</col> extra pk point.");
								} else {
									o.sendMessage("The scroll has awarded you <col=ff0000>" + extraPkp + "</col> extra pk points.");
								}

								pkPoints += extraPkp;
							}
						}

						o.pkp += pkPoints;
						if (RSConstants.outsideClanWarsWilderness(c.getX(), c.getY()) && Player.clanWarsWildyEvent == true) {
							ItemHandler.createGroundItem(o, 995, c.getX(), c.getY(), c.getHeightLevel(), 1300000 + Misc.random(2000000));
							o.sendMessage("Your opponent dropped some extra coins.");
						}
						if (RSConstants.edgevilleWilderness(c.getX(), c.getY()) && Player.edgeWildyEvent == true) {
							ItemHandler.createGroundItem(o, 995, c.getX(), c.getY(), c.getHeightLevel(), 1300000 + Misc.random(2000000));
							o.sendMessage("Your opponent dropped some extra coins.");
						}
						if (c.getClan() != null) {
							if (c.getClan().getClanPoints() > 37023
									&& c.getClan().getClanPoints() < 166636) {
								o.pkp += 2;
							} else if (c.getClan().getClanPoints() > 166635
									&& c.getClan().getClanPoints() < 737627) {
								o.pkp += 3;
							} else if (c.getClan().getClanPoints() > 737626
									&& c.getClan().getClanPoints() < 23611006) {
								o.pkp += 4;
							} else if (c.getClan().getClanPoints() > 23611005) {
								o.pkp += 7;
							}
						}
						if (o.getClan() != null) {
							o.getClan().addClanPoints(o, 10,
									"kill_" + c.getName());
						}
						int rewardGoldAmount = 50000 + Misc.random(100000);
						MoneyPouch.addToMoneyPouch(o, rewardGoldAmount);
						o.sendMessage("You have received <col=ff0000>" + pkPoints
								+ " </col>Pk Points after defeating <col=ff0000>"
								+ c.getNameSmartUp() + "</col>!");
						o.sendMessage("You have received a extra of: <col=ff0000>"
								+ Misc.formatNumbersWithCommas(rewardGoldAmount)
								+ "</col> coins.");
					} else {
						ItemHandler.createGroundItem(o, 8890, c.getX(),
								c.getY(), c.getHeightLevel(),
								bloodMoney + 40 + Misc.random(30));
						o.sendMessage(
								"<col=ff00>Your opponent dropped some blood money!");
					}
					c.getPA().loadQuests();
					o.getPA().loadQuests();
					// if (!c.isOwner() && !c.isBot())
					// PKBoards.addToRecord(c);
					// if (!o.isOwner() && !o.isBot())
					// PKBoards.addToRecord(o);
				} else {
					if (c.isBot()) {
						o.sendMessage(
								"You don't get a rewards this time. Try fighting someone stronger.");
					} else {
						if (c != o) {
							o.sendMessage("You have recently defeated <col=ff0000>"
									+ c.getNameSmartUp()
									+ "</col>, you didnt received pk points.");
						}
					}
				}
			}
		} else { // player in anti rush array
			o.sendMessage(
					"You have been classified as a rusher so you do not get any rewards today.");
		}

		if (o.wildLevel > 0 && o.wildLevel < 15 && !o.inMulti()) {
			o.getPA().applyImmunity(10);
		} else if (o.wildLevel >= 15 && !o.inMulti()) {
			o.getPA().applyImmunity(5);
		}
	}

	private static final GameItem BLOODY_KEY = new GameItem(30105, 1);
	private static final GameItem BLOODIER_KEY = new GameItem(30106, 1);

	private void rewardLmsKiller(final Client killer) {
		if (c.isInLmsArena() && c.lmsManager.hasMetMinReq) {
			killer.getExtraHiscores().incLmsKills();
			
			int killerRating = killer.getExtraHiscores().getLmsKillRating();
			int loserRating = c.getExtraHiscores().getLmsKillRating();
			
			
			killer.getExtraHiscores().setLmsKillRating(EloRatingSystem.getNewRating(killerRating,
					loserRating, EloRatingSystem.WIN));
			c.getExtraHiscores().setLmsKillRating(EloRatingSystem.getNewRating(loserRating,
					killerRating, EloRatingSystem.LOSE));
		}
		
		killer.lmsKillsPerGame++;
		LmsManager.updateKillsPerGameInterface(killer);
		
		if (!killer.isInLmsArena()) {
			return;
		}
		
		killer.lmsDamageMap.incrementLMSDamageDealt(c.mySQLIndex, LmsManager.POINTS_PER_KILL, true);

		if (killer.lmsManager.playerSize > 5) {
			killer.getItems().addOrDrop(BLOODY_KEY);
		} else if (killer.lmsManager.playerSize > 1) {
			killer.getItems().addOrDrop(BLOODIER_KEY);
		}
		
		killer.getPA().applyImmunity(6);
	}

	public void rewardVotingPlayer() {
		if (Misc.randomBoolean(50)) { 
			c.getItems().addOrBankItem(30380, 1);
			c.sendMessage("<col=ff0000>You have received a movement speed scroll");
		} else if (Misc.randomBoolean(5)) {
			c.getItems().addOrBankItem(30379, 1);
			c.sendMessage("<col=ff0000>You have received a movement speed scroll");
			
		}
		int random = Misc.random(1000);
		if (random == 7) {

			int itemId = Misc.random(voteRewardItems);
			int amount = 1;
			
			if (itemId == 30367)
				amount = 5_000_000;

			c.getItems().addOrDrop(new GameItem(itemId, amount));
			PlayerHandler
					.messageAllPlayers("<col=ff0000>" + c.getNameSmartUp() + " has received "
							+ Misc.formatShortPrice(amount) + " "
							+ ItemConstants.getItemName(itemId)
							+ " from voting![::Vote]");
		} else {
			c.sendMessage(
					"<col=ff0000>Sorry you did not receive a rare random reward from voting this time!.");
		}
	}

	public void runCheatClientTest() {
		if (!Config.RUN_ON_DEDI) {
			return;
		}
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (!c.passedCheatClientTest) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildCheatClientMessage(c.getName()));
				}

				container.stop();

			}

			@Override
			public void stop() {

			}
		}, 10);
	}

	/*
	 * public int npcsCountInRegion() { //int i = 0; for (EntityRegion r :
	 * RegionHandler.regions) { if (r == c.currentRegion) { for (NPC n : r.npcs)
	 * { if (n != null) { i++; } } return i; return r.npcs.size(); } } return 0;
	 * } public List<NPC> getNpcsInRegion() { for (EntityRegion r :
	 * RegionHandler.regions) { if (r == c.currentRegion) { return r.npcs; } }
	 * return null; } public List<Client> getPlayersInRegion() { List<Client>
	 * tempPlayers = new LinkedList<Client>(); for (EntityRegion r :
	 * RegionHandler.regions) { if (r == c.currentRegion) { for (Client p :
	 * r.players) { if(p != null && c.withinDistance(p) && c != p) {
	 * tempPlayers.add(p); } } return tempPlayers; } } return null; }
	 */

	public void safeDeathSecureTimer() {
		if (c.safeDeath) {
			return;
		}
		c.safeDeath = true;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (c != null) {
					c.safeDeath = false;
				}
				container.stop();
			}

			@Override
			public void stop() {

			}

		}, 12);
	}

	public void scorpSpecBonus(int stat, boolean sup) {
		boostStat(stat, sup);
	}

	public void sendBotTabs() {
		// remove empty tab
		boolean moveRest = false;
		if (checkEmpty(c.getBot().bankItems1)) { // tab 1 empty
			c.getBot().bankItems1 = Arrays.copyOf(c.getBot().bankItems2,
					c.getBot().bankingItems.length);
			c.getBot().bankItems1N = Arrays.copyOf(c.getBot().bankItems2N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems2 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems2N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.getBot().bankItems2) || moveRest) {
			c.getBot().bankItems2 = Arrays.copyOf(c.getBot().bankItems3,
					c.getBot().bankingItems.length);
			c.getBot().bankItems2N = Arrays.copyOf(c.getBot().bankItems3N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems3 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems3N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.getBot().bankItems3) || moveRest) {
			c.getBot().bankItems3 = Arrays.copyOf(c.getBot().bankItems4,
					c.getBot().bankingItems.length);
			c.getBot().bankItems3N = Arrays.copyOf(c.getBot().bankItems4N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems4 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems4N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.getBot().bankItems4) || moveRest) {
			c.getBot().bankItems4 = Arrays.copyOf(c.getBot().bankItems5,
					c.getBot().bankingItems.length);
			c.getBot().bankItems4N = Arrays.copyOf(c.getBot().bankItems5N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems5 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems5N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.getBot().bankItems5) || moveRest) {
			c.getBot().bankItems5 = Arrays.copyOf(c.getBot().bankItems6,
					c.getBot().bankingItems.length);
			c.getBot().bankItems5N = Arrays.copyOf(c.getBot().bankItems6N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems6 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems6N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.getBot().bankItems6) || moveRest) {
			c.getBot().bankItems6 = Arrays.copyOf(c.getBot().bankItems7,
					c.getBot().bankingItems.length);
			c.getBot().bankItems6N = Arrays.copyOf(c.getBot().bankItems7N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems7 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems7N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.getBot().bankItems7) || moveRest) {
			c.getBot().bankItems7 = Arrays.copyOf(c.getBot().bankItems8,
					c.getBot().bankingItems.length);
			c.getBot().bankItems7N = Arrays.copyOf(c.getBot().bankItems8N,
					c.getBot().bankingItems.length);
			c.getBot().bankItems8 = new int[Config.BANK_TAB_SIZE];
			c.getBot().bankItems8N = new int[Config.BANK_TAB_SIZE];
		}
		if (c.getBot().bankingTab > c.getBot().getPA().getTabCount()) {
			c.getBot().bankingTab = c.getBot().getPA().getTabCount();
		}
		c.getPacketSender().sendFrame126(
				Integer.toString(c.getBot().getPA().getTabCount()), 27001);
		c.getPacketSender()
				.sendFrame126(Integer.toString(c.getBot().bankingTab), 27002);
		
		c.getPacketSender().displayItemOnInterface(22035, getInterfaceModel(0, c.getBot().bankItems1, c.getBot().bankItems1N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22036, getInterfaceModel(0, c.getBot().bankItems2, c.getBot().bankItems2N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22037, getInterfaceModel(0, c.getBot().bankItems3, c.getBot().bankItems3N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22038, getInterfaceModel(0, c.getBot().bankItems4, c.getBot().bankItems4N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22039, getInterfaceModel(0, c.getBot().bankItems5, c.getBot().bankItems5N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22040, getInterfaceModel(0, c.getBot().bankItems6, c.getBot().bankItems6N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22041, getInterfaceModel(0, c.getBot().bankItems7, c.getBot().bankItems7N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22042, getInterfaceModel(0, c.getBot().bankItems8, c.getBot().bankItems8N), 0, 1);
		
		// tells client to update
		c.getPacketSender().sendFrame126b("1", 27000);
	}

	public void sendStatement(String s) {
		if (c.isBot()) {
			return;
		}
		c.getPacketSender().sendFrame126(s, 357);
		c.getPacketSender().sendFrame126("Click here to continue", 358);
		c.getPacketSender().sendChatInterface(356);
	}

	public void sendTabs() {
		// remove empty tab
		boolean moveRest = false;
		if (checkEmpty(c.bankItems1)) { // tab 1 empty
			c.bankItems1 = Arrays.copyOf(c.bankItems2, c.bankingItems.length);
			c.bankItems1N = Arrays.copyOf(c.bankItems2N, c.bankingItems.length);
			c.bankItems2 = new int[Config.BANK_TAB_SIZE];
			c.bankItems2N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.bankItems2) || moveRest) {
			c.bankItems2 = Arrays.copyOf(c.bankItems3, c.bankingItems.length);
			c.bankItems2N = Arrays.copyOf(c.bankItems3N, c.bankingItems.length);
			c.bankItems3 = new int[Config.BANK_TAB_SIZE];
			c.bankItems3N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.bankItems3) || moveRest) {
			c.bankItems3 = Arrays.copyOf(c.bankItems4, c.bankingItems.length);
			c.bankItems3N = Arrays.copyOf(c.bankItems4N, c.bankingItems.length);
			c.bankItems4 = new int[Config.BANK_TAB_SIZE];
			c.bankItems4N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.bankItems4) || moveRest) {
			c.bankItems4 = Arrays.copyOf(c.bankItems5, c.bankingItems.length);
			c.bankItems4N = Arrays.copyOf(c.bankItems5N, c.bankingItems.length);
			c.bankItems5 = new int[Config.BANK_TAB_SIZE];
			c.bankItems5N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.bankItems5) || moveRest) {
			c.bankItems5 = Arrays.copyOf(c.bankItems6, c.bankingItems.length);
			c.bankItems5N = Arrays.copyOf(c.bankItems6N, c.bankingItems.length);
			c.bankItems6 = new int[Config.BANK_TAB_SIZE];
			c.bankItems6N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.bankItems6) || moveRest) {
			c.bankItems6 = Arrays.copyOf(c.bankItems7, c.bankingItems.length);
			c.bankItems6N = Arrays.copyOf(c.bankItems7N, c.bankingItems.length);
			c.bankItems7 = new int[Config.BANK_TAB_SIZE];
			c.bankItems7N = new int[Config.BANK_TAB_SIZE];
			moveRest = true;
		}
		if (checkEmpty(c.bankItems7) || moveRest) {
			c.bankItems7 = Arrays.copyOf(c.bankItems8, c.bankingItems.length);
			c.bankItems7N = Arrays.copyOf(c.bankItems8N, c.bankingItems.length);
			c.bankItems8 = new int[Config.BANK_TAB_SIZE];
			c.bankItems8N = new int[Config.BANK_TAB_SIZE];
		}
		if (c.bankingTab > getTabCount()) {
			c.bankingTab = getTabCount();
		}
		c.getPacketSender().sendFrame126(Integer.toString(getTabCount()),
				27001);
		c.getPacketSender().sendFrame126(Integer.toString(c.bankingTab), 27002);
		c.getPacketSender().displayItemOnInterface(22035, getInterfaceModel(0, c.bankItems1, c.bankItems1N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22036, getInterfaceModel(0, c.bankItems2, c.bankItems2N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22037, getInterfaceModel(0, c.bankItems3, c.bankItems3N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22038, getInterfaceModel(0, c.bankItems4, c.bankItems4N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22039, getInterfaceModel(0, c.bankItems5, c.bankItems5N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22040, getInterfaceModel(0, c.bankItems6, c.bankItems6N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22041, getInterfaceModel(0, c.bankItems7, c.bankItems7N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22042, getInterfaceModel(0, c.bankItems8, c.bankItems8N), 0, 1);
		
		// tells client to update
		c.getPacketSender().sendFrame126b("1", 27000);
	}

	public void sendTabs(Player other) {
		c.getPacketSender().sendFrame126(Integer.toString(other.getPA().getTabCount()),27001);
		c.getPacketSender().sendFrame126(Integer.toString(other.bankingTab), 27002);
		c.getPacketSender().displayItemOnInterface(22035, getInterfaceModel(0, other.bankItems1, other.bankItems1N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22036, getInterfaceModel(0, other.bankItems2, other.bankItems2N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22037, getInterfaceModel(0, other.bankItems3, other.bankItems3N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22038, getInterfaceModel(0, other.bankItems4, other.bankItems4N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22039, getInterfaceModel(0, other.bankItems5, other.bankItems5N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22040, getInterfaceModel(0, other.bankItems6, other.bankItems6N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22041, getInterfaceModel(0, other.bankItems7, other.bankItems7N), 0, 1);
		c.getPacketSender().displayItemOnInterface(22042, getInterfaceModel(0, other.bankItems8, other.bankItems8N), 0, 1);
		// tells client to update
		c.getPacketSender().sendFrame126b("1", 27000);
	}

	public void setCastleWarsWaitingTimer(final int ticks) {
		c.sendMessage("Wait " + (Misc.ticksIntoSeconds(ticks))
				+ " seconds before you can go out to fight.");
		c.setCWWaitingRoomCountdown(ticks);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer e) {
				if (!c.inCw()) {
					e.stop();
					return;
				}
				if (c.getCWWaitingRoomCountDown() <= 0) {
					e.stop();
				}
				c.setCWWaitingRoomCountdown(c.getCWWaitingRoomCountDown() - 1);
			}

			@Override
			public void stop() {
			}

		}, 1);
	}

	public void setClanData() {
		// boolean exists = Server.clanManager.clanExists(c.getName());
		if (/* !exists || */c.getClan() == null) {
			// sendString("Join chat", 18135);
			// sendString("Talking in: Not in chat", 18139);
			// sendString("Owner: None", 18140);
			// c.getPacketSender().sendFrame126("Off", 18250);
			return;
		}
		// if (!exists) {
		// sendString("Chat Disabled", 18306);
		// String title = "";
		// for (int id = 18307; id < 18317; id += 3) {
		// if (id == 18307) {
		// title = "Anyone";
		// } else if (id == 18310) {
		// title = "Anyone";
		// } else if (id == 18313) {
		// title = "General+";
		// } else if (id == 18316) {
		// title = "Only Me";
		// }
		// sendString(title, id + 2);
		// }
		// for (int index = 0; index < 100; index++) {
		// sendString("", 18323 + index);
		// }
		// for (int index = 0; index < 100; index++) {
		// sendString("", 18424 + index);
		// }
		// return;
		// }
		// Clan clan = Server.clanManager.getClan(c.getName()); //gets clan
		// taht you are owner of
		// if(c.clan == null) return;
		Clan clan = c.getClan();

		if (clan != null) {
			c.getPacketSender().sendFrame126(
					c.getClan().clanShare() ? "On" : "Off", 18250);
		}

		c.getPacketSender().sendString(clan.getTitle(), 18306);
		String title = "";
		for (int id = 18307; id < 18317; id += 3) {
			if (id == 18307) {
				title = clan.getRankTitle(clan.whoCanJoin)
						+ (clan.whoCanJoin > Clan.Rank.ANYONE
								&& clan.whoCanJoin < Clan.Rank.OWNER
										? "+"
										: "");
			} else if (id == 18310) {
				title = clan.getRankTitle(clan.whoCanTalk)
						+ (clan.whoCanTalk > Clan.Rank.ANYONE
								&& clan.whoCanTalk < Clan.Rank.OWNER
										? "+"
										: "");
			} else if (id == 18313) {
				title = clan.getRankTitle(clan.whoCanKick)
						+ (clan.whoCanKick > Clan.Rank.ANYONE
								&& clan.whoCanKick < Clan.Rank.OWNER
										? "+"
										: "");
			} else if (id == 18316) {
				title = clan.getRankTitle(clan.whoCanBan)
						+ (clan.whoCanBan > Clan.Rank.ANYONE
								&& clan.whoCanBan < Clan.Rank.OWNER ? "+" : "");
			}

			c.getPacketSender().sendString(title, id + 2);
		}

		if (!clan.rankedMembers.isEmpty()) {
			int index = 0;
			Iterator<Map.Entry<String, Integer>> it = clan.rankedMembers
					.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Integer> map = it.next();
				if (index < clan.rankedMembers.size()) {
					c.getPacketSender().sendString(
							"<clan=" + map.getValue() + ">" + map.getKey(),
							18323 + index);
				} else {
					c.getPacketSender().sendString("", 18323 + index);
				}
				index++;
			}
			for (int index2 = index; index2 < 100; index2++) {
				if (index2 >= clan.rankedMembers.size()) {
					c.getPacketSender().sendString("", 18323 + index2);
				}
			}
		} else {
			for (int index2 = 0; index2 < 100; index2++) {
				c.getPacketSender().sendString("", 18323 + index2);
			}
		}
		if (clan.bannedMembers != null) {
			for (int index = 0; index < 100; index++) {
				if (index < clan.bannedMembers.size()) {
					c.getPacketSender().sendString(
							clan.bannedMembers.get(index), 18424 + index);
				} else {
					c.getPacketSender().sendString("", 18424 + index);
				}
			}
		}
		clan = null;
	}

	/********************************************
	 * CONSTRUCTION END?
	 **************************************************************/

	// public void restoreInv(Client c) {
	// if(c == null)
	// return;
	//
	// // for (int i = 0; i < c.playerItems.length; i++)
	// // {
	// // c.playerItems[i] = backupInvItems[i]; c.playerItemsN[i] =
	// backupInvItemsN[i];
	// // }
	//
	// c.getItems().updateInventory();
	// // backupInvItems = new int[28];
	// // backupInvItemsN = new int[28];
	// }

	/**
	 * Dieing
	 **/

	public void setRespawnLocation() {
		if (c.inPcGame()) {
			c.respawnX = 2656 + Misc.random(3);
			c.respawnY = 2609 + Misc.random(5);
		} else if (c.inPvpWorld()) {
			c.respawnX = 3182;
			c.respawnY = 3440;
		} else if (c.wildLevel > 0) {
			c.respawnX = 3086 + Misc.random(3);
			c.respawnY = 3515 + Misc.random(3);
			if (c.wildLevel > 52) {
				c.respawnX = 2537 + Misc.random(4);
				c.respawnY = 4714 + Misc.random(5);
			}
		} else if (c.inCw()) {
			if (CastleWars.hasFlag(c)) {
				CastleWars.dropFlag(c,
						c.playerEquipment[PlayerConstants.playerWeapon]);
			}
			if (CastleWars.getTeamNumber(c) == 1) { // sara team
				c.respawnX = 2424 + Misc.random(5);
				c.respawnY = 3075 + Misc.random(5);
				c.respawnZ = 1;
			} else {
				c.respawnX = 2369 + Misc.random(6);
				c.respawnY = 3128 + Misc.random(4);
				c.respawnZ = 1;
			}
		} else if (RSConstants.fightPitsRoom(c.getX(), c.getY())) {
			c.respawnX = 2399;
			c.respawnY = 5173;
			c.respawnZ = 0;
		} else if (c.pitsStatus == 1) {
			c.respawnX = 2399;
			c.respawnY = 5173;
		} else if (c.inFunPk()) {
			c.respawnX = 3303;
			c.respawnY = 3123;
		} else if (c.inClanWarsFunPk() || c.whitePortalSafe()) {
			c.respawnX = 2810;
			c.respawnY = 5510;
		} else if (c.inClanWars()) {
			c.respawnX = 0;
			c.respawnY = 0;
		} else if (c.inFightCaves()) {
			c.respawnX = 2438;
			c.respawnY = 5168;
		} else if (c.duelFightArenas()) { // we are in a duel, respawn outside
											// of arena
			c.respawnX = 1 + Config.DUELING_RESPAWN_X
					+ (Misc.random(Config.RANDOM_DUELING_RESPAWN));
			c.respawnY = 2 + Config.DUELING_RESPAWN_Y
					+ (Misc.random(Config.RANDOM_DUELING_RESPAWN - 1));
		} else if (RSConstants.inBarbarianAssaultArena(c.absX, c.absY)) {
			c.respawnX = BarbarianAssault.WAITING_ROOM.getX() + Misc.random(new int[]{-2, -1, 0, 1, 2});
			c.respawnY = BarbarianAssault.WAITING_ROOM.getY() + Misc.random(new int[]{-2, -1, 0, 1, 2});
			c.respawnZ = 0;
		} else if (c.playerIsInHouse) {
			c.respawnX = 51;
			c.respawnY = 53;
			c.respawnZ = 1;
		} else if (c.soulWarsTeam != Team.NONE) {
			int x = 0;
			int y = 0;
			int z = 0;
			if (c.soulWarsTeam == Team.RED) {
				x = 1954;
				y = 3239;
				int distance = Misc.distanceToPoint(c.absX, c.absY, x, y);
				if (SoulWars.blueGraveyard.team == Team.RED) {
					int blueDist = SoulWars.blueGraveyard.distance(c.absX, c.absY);
					if (blueDist < distance) {
						distance = blueDist;
						x = SoulWars.blueGraveyard.absX;
						y = SoulWars.blueGraveyard.absY;
					}
				}
				if (SoulWars.redGraveyard.team == Team.RED) {
					int redDist = SoulWars.redGraveyard.distance(c.absX, c.absY);
					if (redDist < distance) {
						distance = redDist;
						x = SoulWars.redGraveyard.absX;
						y = SoulWars.redGraveyard.absY;
					}
				}
			} else {
				x = 1820;
				y = 3225;
				int distance = Misc.distanceToPoint(c.absX, c.absY, x, y);
				if (SoulWars.redGraveyard.team == Team.BLUE) {
					int redDist = SoulWars.redGraveyard.distance(c.absX, c.absY);
					if (redDist < distance) {
						distance = redDist;
						x = SoulWars.redGraveyard.absX;
						y = SoulWars.redGraveyard.absY;
					}
				}
				if (SoulWars.blueGraveyard.team == Team.BLUE) {
					int blueDist = SoulWars.blueGraveyard.distance(c.absX, c.absY);
					if (blueDist < distance) {
						distance = blueDist;
						x = SoulWars.blueGraveyard.absX;
						y = SoulWars.blueGraveyard.absY;
					}
				}
			}

			c.respawnX = x;
			c.respawnY = y;
			c.respawnZ = z;
		} else {
			c.respawnX = DBConfig.RESPAWN_X;
			c.respawnY = DBConfig.RESPAWN_Y;
		}
	}

	public void setShadow(int i) {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		if (c.currentShadow == i) {
			return;
		}
		c.currentShadow = i;
		c.getOutStream().createPacket(29);
		c.getOutStream().writeByte(i);
		c.flushOutStream();
	}

	public void setTeleportNames() {
		if (WorldType.equalsType(WorldType.SPAWN)) {
			c.getPacketSender().sendFrame126("Wilderness Locations", 13037);
			c.getPacketSender().sendFrame126("Minigames", 13047);
			c.getPacketSender().sendFrame126("Wilderness Monsters", 13055);
			c.getPacketSender().sendFrame126("Monsters Locations", 13063);
			c.getPacketSender().sendFrame126("Unavailable", 13071);
			c.getPacketSender().sendFrame126("Unavailable", 13081);
			c.getPacketSender().sendFrame126("Wilderness Locations", 1300);
			c.getPacketSender().sendFrame126("Minigames", 1325);
			c.getPacketSender().sendFrame126("Wilderness Monsters", 1350);
			c.getPacketSender().sendFrame126("Monsters Locations", 1382);
			c.getPacketSender().sendFrame126("Unavailable", 1415);
			c.getPacketSender().sendFrame126("Unavailable", 1454);
			c.getPacketSender().sendFrame126("Unavailable", 7457);
			c.getPacketSender().sendFrame126("Unavailable", 7482);
			c.getPacketSender().sendFrame126("Unavailable", 13097);
			c.getPacketSender().sendFrame126("Unavailable", 13089);
		} else {
			c.getPacketSender().sendFrame126("Training Teleport", 13037);
			c.getPacketSender().sendFrame126("Minigame Teleport", 13047);
			c.getPacketSender().sendFrame126("Boss Teleport", 13055);
			c.getPacketSender().sendFrame126("Pking Teleport", 13063);
			c.getPacketSender().sendFrame126("Skilling Teleport", 13071);
			c.getPacketSender().sendFrame126("City Teleport", 13081);
			c.getPacketSender().sendFrame126("Training Teleport", 1300);
			c.getPacketSender().sendFrame126("Minigame Teleport", 1325);
			c.getPacketSender().sendFrame126("Boss Teleport", 1350);
			c.getPacketSender().sendFrame126("Pking Teleport", 1382);
			c.getPacketSender().sendFrame126("Skilling Teleport", 1415);
			c.getPacketSender().sendFrame126("City Teleport", 1454);
			c.getPacketSender().sendFrame126("Dungeon Teleport", 7457);
			c.getPacketSender().sendFrame126("Dungeon Teleport", 7482);
		}
	}

	/****************************************
	 * CONSTRUCTION BEGIN?
	 *************************************************************/

	public void showDialogue(String line) {
		c.getPacketSender().sendFrame126(line, 357);
		c.getPacketSender().sendChatInterface(356);
	}

	public void skillsNeck() {
		c.getDH().sendOption4("Woodcutting", "Fishing", "Mining",
				"skill shops");
		c.usingSkillsNeck = true;
	}

	public void snowGlobeShake(Client c) {

		if (c.wildLevel > 0 || c.inMinigame()) {
			c.sendMessage("You cannot do that here.");
			return;
		}

		if (c.performingAction) {
			return;
		}
		c.performingAction = true;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int timer = 7;

			@Override
			public void execute(CycleEventContainer container) {

				if (c.disconnected || c == null) {
					container.stop();
					return;
				}

				if (timer == 7) {
					c.startAnimation(7528);
					c.startGraphic(Graphic.create(1284, GraphicType.LOW));
					c.getMovement().resetWalkingQueue();
				}

				timer--;
				if (timer == 0) {
					c.getItems().addItem(10501, 3);
					container.stop();
				}
			}

			@Override
			public void stop() {
				c.performingAction = false;
			}
		}, 1);
	}

	public void spamWarning() {
		if (System.currentTimeMillis() - c.lastSpam < 10000) {
			if (c.spamMessage > 2) {
				c.sendMessage("<col=ff0000>Please keep down with the spam.");
				if (c.spamMessage > 3) {
					if (Chat.spammingPlayers.contains(c.UUID)) {
						if (!Connection.isUidMuted(c.UUID)) {
							Connection.addUidToMuteList(c.UUID);
							c.sendMessage(
									"You should've stopped with the spam.");
							Server.getSlackApi().call(SlackMessageBuilder
									.buildAutoMuteMessage(c.getName(), true));
						}
					} else {
						if (!Connection.isUidMuted(c.UUID)) {
							c.setTimedMute(100);
							c.sendMessage(
									"You've been automatically muted for link spamming.");
							Server.getSlackApi().call(SlackMessageBuilder
									.buildAutoMuteMessage(c.getName(), false));
							Chat.spammingPlayers.add(c.UUID);
						}
					}
				}
			}
		} else {
			c.spamMessage = 0;
		}
		c.spamMessage++;
		c.lastSpam = System.currentTimeMillis();
	}

	public void spellTeleport(Location location) {
		startTeleport(location.getX(), location.getY(), location.getZ(), TeleportType.DUNG_GATESTONE);
	}
	
	public boolean BountyTargetTeleport(Location location) {
		return startTeleport(location.getX(), location.getY(), location.getZ(), TeleportType.BOUNTY_TELEPORT);
	}

	/**
	 * Teleporting
	 **/
	public void spellTeleport(int x, int y, int height) {
		c.getPA().startTeleport(x, y, height,
				c.playerMagicBook == SpellBook.ANCIENT ? TeleportType.ANCIENT : TeleportType.MODERN); // TODO:
																// readd
																// the
																// ancient
																// teleport
																// anim/gfx
		// c.getPA().startTeleport(x, y, height, "modern");
	}

	public void StartBestItemScan() {
		if (c.isSkulled && (c.playerPrayerBook == PrayerBook.NORMAL
				? !c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
				: !c.curseActive[Curses.PROTECT_ITEM.ordinal()])) {
			ItemKeptInfo(0);
			return;
		}
		FindItemKeptInfo(c);
		c.ResetKeepItems();
		c.BestItem1();
	}

	public void startLeverTeleport(int x, int y, int height,
			String teleportType) {
		if (c.isTeleBlocked()) {
			c.sendMessage("You are teleblocked and can't teleport.");
			return;
		}

		if (!c.isDead() && c.teleTimer == 0 && c.respawnTimer == -6) {
			if (c.playerIndex > 0 || c.npcIndex > 0) {
				c.getCombat().resetPlayerAttack();
			}
			c.getMovement().resetWalkingQueue();
			c.getPA().closeAllWindows(); // removeAllWindows();
			c.teleX = x;
			c.teleY = y;
			c.npcIndex = 0;
			c.playerIndex = 0;
			c.resetFace();
			c.teleHeight = height;
			if (teleportType.equalsIgnoreCase("lever")) {
				c.startAnimation(2140);
				c.teleTimer = 8;
				c.sendMessage("You pull the lever..");
			}
		}
	}

	public void startTeleCooldown(final int cooldown) {
		if (c.wildLevel < 1) {
			return;
		}
		if (c.isTeleCooldown()) {
			return;
		}
		c.setTeleCooldown(true);
		CycleEventHandler.getSingleton().addEvent(c, true, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer e) {
				if (c.isActive) {
					c.setTeleCooldown(false);
				}
				e.stop();
			}

			@Override
			public void stop() {
			}

		}, cooldown);
	}

	public boolean startTeleport(int x, int y, int height, TeleportType teleportType) {
		if (!canTeleport(teleportType)) {
			return false;
		}

		// c.sendMessage("telex:"+c.teleportToX);
		if (c.isDead() || c.teleTimer != 0 || c.isLockActions()) {
			return false;
		}

		if (c.duelStatus >= 1 && c.duelStatus <= 4) {
			c.getTradeAndDuel().declineDuel();
		}

		if ((teleportType == TeleportType.ANCIENT || teleportType == TeleportType.MODERN) && c.inWild() && c.underAttackByPlayer()) {
			if (!MagicRequirements.checkMagicReqsNew(c, SpellsOfPI.WILD_TELE_REQ, null, true)) {
				c.sendMessage("You need 2 law runes to teleport during combat in the wilderness!");
				c.getPA().closeAllWindows();
				return false;
			}
			
		}

		if (c.playerIsSkilling) {
			c.playerIsSkilling = false;
		}
		c.getPA().resetSkilling();
		
		if (c.skillingEvent != null) {
			c.resetSkillingEvent();
		}

		if (c.playerIndex > 0 || c.npcIndex > 0) {
			c.getCombat().resetPlayerAttack();
		}
		c.getMovement().resetWalkingQueue();
		c.getPA().closeAllWindows(); // removeAllWindows();
		if (c.clickObjectType > 0) {
			c.clickObjectType = 0;
		}
		c.teleX = x;
		c.teleY = y;
		c.npcIndex = 0;
		c.playerIndex = 0;
		c.resetFace();
		c.teleHeight = height;

		if (teleportType.getStartAnimation() != null) {
			c.startAnimation(teleportType.getStartAnimation());

			if (teleportType == TeleportType.TAB) {
				c.getPacketSender().playSound(Sounds.TELETAB);
			}
		}	
		if (teleportType.getStartGfx() != null) {
			c.startGraphic(teleportType.getStartGfx());
		}
		c.teleTimer = teleportType.getTimer();
		c.endDelay = teleportType.getEndDelay();
		startTeleCooldown(teleportType.getCooldown());
		c.teleEndAnimation = teleportType.getEndAnimation();
		c.teleEndGfx = teleportType.getEndGfx();

		if (teleportType != TeleportType.DUNG_GATESTONE) {
			c.setPauseAnimReset(6);
			if (c.inMinigame()) {
				c.getPA().restorePlayer(false);
			}
		}

		if (c.getBot() != null) {
			c.getBot().getPA().spellTeleport(x, y, height);
		}

		if (c.playerIsInHouse) {
			Construction.leaveHouse(c, false);
		}
		return true;
	}

	public void startTimePlayedCounter() {
		/*
		 * final AchievementController controller = new AchievementController(
		 * new AchievementModel("Your title of task", "description of task", 1),
		 * new Achievement(100)) {
		 * @Override public void complete() {
		 * this.getAchievement().getProgress().increase(); if
		 * (this.getAchievement().getStage() == Stage.COMPLETED) { // unlock the
		 * title here c.sendMessage("working fine.."); } else if
		 * (this.getAchievement().getStage() == Stage.IN_PROGRESS) {
		 * c.sendMessage(this.getAchievement().getProgress()
		 * .getPercentageCompleted() + " complete"); } else { c.sendMessage(
		 * "Not started"); } } };
		 */
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (c == null || c.forceDisconnect) {
					container.stop();
					return;
				}

				if (c.isIdle) {
					return;
				}

				if (c.getIncubatorEggId() > 0) {
					c.setEggTimer(c.getEggTimer() + 1);
					c.setEggTimer(c.getEggTimer() + 1); // double cuz cycle event is retardedly running per 2 cycles.
				}
				
				if (c.isStaff()) {
					c.staffTimer.inc();
					c.staffTimer.inc(); // double cuz cycle event is retardedly running per 2 cycles.
				}

				c.timePlayed++;
				// c.setTitleInt(TitleData.THE_LOYAL.ordinal(),
				// c.getTitleInt(TitleData.THE_LOYAL.ordinal()) + 1);
				Titles.playedFourDays(c);
				Titles.playedTenDays(c);
				Titles.playedTwentyDays(c);
				/*
				 * if (controller.getAchievement().getStage() !=
				 * Stage.COMPLETED) controller.complete();
				 */
			}

			@Override
			public void stop() {

			}

		}, 2);

	}

	public void startTour() {
		c.tourStep = 1;

		if (WorldType.equalsType(WorldType.SPAWN)) {
			
			if (DBConfig.TOUR_VERSION.equals("edgeville")) {
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					boolean addedStarter = false;

					int timer = 0;

					int prevStep = c.tourStep;

					@Override
					public void execute(CycleEventContainer container) {
						if (c == null || c.disconnected || c.tourStep == 777) {
							container.stop();
							return;
						}

						// if (c.getX() < 3065 || c.getX() > 3265 || c.getY() <
						// 3391
						// || c.getY() > 3521) {
						// container.stop();
						// return;
						// }
						if (timer > 0) {
							timer--;
						}
						if (timer == 1) {
							if (c.tourStep == prevStep) {
								c.tourStep++;
							}
						}

						if (c.tourStep == 1) {
							c.tourStep++; // sets to tourstep 2 meaning putting
							// it on
							// idle for next tour step
							c.getDH().sendPlayerChat1(
									"We are currently standing in <col=800000>Edgeville</col>, our Home.");
							c.getPacketSender().sendFrame106(6);
							prevStep = c.tourStep;
							timer = 3;
						}

						if (c.tourStep == 3) {
							c.tourStep++;
							c.getPacketSender().sendFrame106(3);
							c.getPA().playerWalk(3088, 3504);
							c.getPacketSender().sendFrame106(15);
							c.getDH().sendPlayerChat2(
									"You can set your skills in your bottom right tab and get ",
									"some free basic items. Better items costs Blood money.");
							c.faceLocation(3088, 3505);
							prevStep = c.tourStep;
							timer = 6;

						}

						if (c.tourStep == 5) {
							c.tourStep++;
							c.getDH().sendPlayerChat1(
									"Buy items with Blood money from the shops located here.");
							c.getPA().playerWalk(3095, 3504);
							c.faceLocation(3095, 3505);
							prevStep = c.tourStep;
							timer = 5;

						}

						if (c.tourStep == 7) {
							c.tourStep++;
							c.getDH().sendPlayerChat2(
									"You can earn Blood money by killing a player.",
									"Killing your target will grant you extra blood Money.");
							prevStep = c.tourStep;
							timer = 4;

						}

						if (c.tourStep == 9) {
							c.tourStep++;
							c.getPA().playerWalk(3085, 3515);
							c.getDH().sendPlayerChat1(
									"Here is the pvp scoreboard.");
							c.faceLocation(3084, 3515);
							prevStep = c.tourStep;
							timer = 8;

						}

						if (c.tourStep == 11) {
							c.tourStep++;
							c.getPA().playerWalk(3088, 3518);
							c.getDH().sendPlayerChat2(
									"Best of all is the PKing on here.",
									"We guarantee the best Hybridding PVP here.");
							prevStep = c.tourStep;
							timer = 5;

						}

						if (c.tourStep == 13) {
							c.tourStep = 777;
							c.getDH().sendPlayerChat2(
									"You should be ready to start your journey on soulPvP.",
									"You can start by typing ::vote for some quick blood money.");
							if (!addedStarter) {
								c.getItems().addItem(8890, 100);
							}
							container.stop();
						}

					}

					@Override
					public void stop() {
						if (c != null) {
							c.tourStep = 777;
							c.sendMessage(
									"<col=800000>Your basic tour has ended. Enjoy your time here! Post suggestions on ::forums");
							c.sendMessage(
									"If you need help, ask for help in clan chat 'help'");
							c.sendMessage(
									"<col=800000>To view commands type ::commands");
						}
					}

				}, 2);
			}
			
		} else { // else not spawn server
			
			if (DBConfig.TOUR_VERSION.equals("edgeville")) {
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					boolean addedStarter = false;

					int timer = 0;

					int prevStep = c.tourStep;

					@Override
					public void execute(CycleEventContainer container) {
						if (c == null || c.disconnected || c.tourStep == 777) {
							container.stop();
							return;
						}
						// if (c.getX() < 3072 || c.getX() > 3114 || c.getY() <
						// 3475 || c.getY() > 3519) { //edgeville
						// container.stop();
						// return;
						// }
						if (c.getX() < 3065 || c.getX() > 3265
								|| c.getY() < 3391 || c.getY() > 3521) { // varrock
							// &&
							// edge
							container.stop();
							return;
						}
						if (timer > 0) {
							timer--;
						}
						if (timer == 1) {
							if (c.tourStep == prevStep) {
								c.tourStep++;
							}
						}

						// EDGEVILLE TOUR

						if (c.tourStep == 1) {
							c.tourStep++; // sets to tourstep 2 meaning putting
							// it on idle for next tour step
							c.getDH().sendPlayerChat3(
									"We are currently standing in <col=800000>Edgeville</col>, our Home.",
									"You can change your home teleport location",
									"by talking to the wise old man.");
							c.getPacketSender().sendFrame106(6);
							prevStep = c.tourStep;
							timer = 5;
						}

						if (c.tourStep == 3) {
							c.tourStep++;
							c.getPacketSender().sendFrame106(3);
							c.getPA().playerWalk(3082, 3505);
							c.getDH().sendPlayerChat1(
									"Change appearance with <col=800000>Thessalia</col>.");
							c.faceLocation(3082, 3506);
							prevStep = c.tourStep;
							timer = 6;

						}

						if (c.tourStep == 5) {
							c.tourStep++;
							c.getPA().playerWalk(3080, 3510);
							c.getDH().sendPlayerChat3(
									"General store is located here.",
									"Buy equipment for all Combat Styles",
									"around the city.");
							c.faceLocation(3080, 3511);
							prevStep = c.tourStep;
							timer = 6;

						}

						if (c.tourStep == 7) {
							c.tourStep++;
							c.getPA().playerWalk(3096, 3504);
							c.getDH().sendPlayerChat1(
									"Altars changing SpellBooks/Prayers are here.");
							c.faceLocation(3096, 3505);
							prevStep = c.tourStep;
							timer = 10;

						}

						if (c.tourStep == 9) {
							c.tourStep++;
							c.getPA().playerWalk(3105, 3499);
							c.getDH().sendPlayerChat1(
									"Slotmachine is over here, good way to make money.");
							c.faceLocation(3106, 3499);
							prevStep = c.tourStep;
							timer = 8;

						}

						if (c.tourStep == 11) {
							c.tourStep++;
							c.getPA().playerWalk(3096, 3498);
							c.getDH().sendPlayerChat1(
									"Let me show you the Vote tickets Shop.");
							c.faceLocation(3096, 3499);
							prevStep = c.tourStep;
							timer = 7;

						}

						if (c.tourStep == 13) {
							c.tourStep++;
							c.getShops().openShop(25);
							c.sendMessage(
									"<col=800000>To get Vote Tickets and random chance for a rare do ::vote");
							prevStep = c.tourStep;
							timer = 5;

						}

						if (c.tourStep == 15) {
							c.tourStep++;
							c.getPA().closeAllWindows();
							c.getPA().playerWalk(3095, 3498);
							c.getDH().sendPlayerChat1(
									"Donator Stores with Megan.");
							c.faceLocation(3095, 3499);
							prevStep = c.tourStep;
							timer = 3;

						}

						if (c.tourStep == 17) {
							c.tourStep++;
							c.getPA().playerWalk(3091, 3496);
							c.getDH().sendPlayerChat1("Our Lottery Man.");
							c.faceLocation(3090, 3496);
							prevStep = c.tourStep;
							timer = 5;
						}

						if (c.tourStep == 19) {
							c.tourStep++;
							c.getPA().playerWalk(3087, 3501);
							c.getDH().sendPlayerChat3(
									"There are 7 slayer masters to assign you tasks.",
									"Use your enchanted gem in your inventory",
									"to teleport to all 7 slayer masters.");
							prevStep = c.tourStep;
							timer = 10;
						}

						if (c.tourStep == 21) {
							c.tourStep++;
							c.getDH().sendPlayerChat3(
									"Do slayer tasks for good starter money.",
									"There are no Slayer Level Requirements",
									"needed to kill any Slayer Monsters.");
							prevStep = c.tourStep;
							timer = 6;
						}

						if (c.tourStep == 23) {
							c.tourStep++;
							c.getPA().spellTeleport(3222, 3473, 0);
							c.getDH().sendPlayerChat4(
									"You can prestige once you achiev all 99s.",
									"All skills will be reset to 1, you will get higher",
									" drop rate and more benefits, when you",
									"reach prestige 3 you will get a sir'owens.");
							prevStep = c.tourStep;
							timer = 15;
						}

						if (c.tourStep == 25) {
							c.tourStep++;
							c.getPA().playerWalk(3087, 3516);
							// c.getPA().spellTeleport(3087, 3516, 0);
							c.getDH().sendPlayerChat3(
									"Best of all is the PKing on here.",
									"We guarantee the best Hybridding PVP here.",
									"Ingame PK Boards are located here also.");
							prevStep = c.tourStep;
							timer = 12;
						}

						if (c.tourStep == 27) {
							c.tourStep = 777;
							c.getPA().spellTeleport(2678, 3718, 0);
							c.getDH().sendPlayerChat3(
									"Those are the basics! :)",
									"Now go train Combat skills at Rock Crabs",
									"or Teleport to Skilling areas for Skilling.");
							if (!addedStarter) {
								c.getItems().addItem(995, 100000);
							}
							container.stop();
						}

					}

					@Override
					public void stop() {
						if (c != null) {
							c.tourStep = 777;
							c.sendMessage(
									"<col=800000>Your basic tour has ended. Enjoy your time here! Post suggestions on ::forums");
							c.sendMessage(
									"If you need help, ask for help in clan chat 'help'");
							c.sendMessage(
									"<col=800000>To view commands type ::commands");
						}
					}

				}, 2);

			}

			if (DBConfig.TOUR_VERSION.equals("varrock")) {
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					boolean addedStarter = false;

					int timer = 0;

					int prevStep = c.tourStep;

					@Override
					public void execute(CycleEventContainer container) {
						if (c == null || c.disconnected || c.tourStep == 777) {
							container.stop();
							return;
						}
						// if (c.getX() < 3072 || c.getX() > 3114 || c.getY() < 3475
						// ||
						// c.getY() > 3519) { //edgeville
						// container.stop();
						// return;
						// }
						if (c.getX() < 3065 || c.getX() > 3265 || c.getY() < 3391
								|| c.getY() > 3521) { // varrock
							// &&
							// edge
							container.stop();
							return;
						}
						if (timer > 0) {
							timer--;
						}
						if (timer == 1) {
							if (c.tourStep == prevStep) {
								c.tourStep++;
							}
						}

						if (c.tourStep == 1) {
							c.tourStep++; // sets to tourstep 2 meaning putting it
							// on
							// idle for next tour step
							c.getDH().sendPlayerChat3(
									"We are currently standing in <col=800000>Varrock</col>, our Home.",
									"You can change your home teleport location",
									"by talking to the wise old man.");
							c.getPacketSender().sendFrame106(6);
							prevStep = c.tourStep;
							timer = 10;
						}

						if (c.tourStep == 3) {
							c.tourStep++;
							c.getPacketSender().sendFrame106(3);
							c.getPA().playerWalk(3207, 3415);
							c.getDH().sendPlayerChat1(
									"Change appearance with <col=800000>Thessalia</col>.");
							c.faceLocation(3206, 3415);
							prevStep = c.tourStep;
							timer = 6;

						}

						if (c.tourStep == 5) {
							c.tourStep++;
							c.getPA().playerWalk(3215, 3415);
							c.getDH().sendPlayerChat3(
									"General store is located here.",
									"Buy equipment for all Combat Styles",
									"around the city.");
							c.faceLocation(3216, 3415);
							prevStep = c.tourStep;
							timer = 6;

						}

						if (c.tourStep == 7) {
							c.tourStep++;
							c.getPA().playerWalk(3193, 3438);
							c.getDH().sendPlayerChat1(
									"Altars changing SpellBooks/Prayers are here.");
							c.faceLocation(3193, 3439);
							prevStep = c.tourStep;
							timer = 12;

						}

						if (c.tourStep == 9) {
							c.tourStep++;
							c.getPA().playerWalk(3194, 3413);
							c.getDH().sendPlayerChat1(
									"Slotmachine is over here, good way to make money.");
							c.faceLocation(3194, 3414);
							prevStep = c.tourStep;
							timer = 10;

						}

						if (c.tourStep == 11) {
							c.tourStep++;
							c.getPA().playerWalk(3181, 3445);
							c.getDH().sendPlayerChat1(
									"Let me show you the Vote tickets Shop.");
							c.faceLocation(3180, 3445);
							prevStep = c.tourStep;
							timer = 12;

						}

						if (c.tourStep == 13) {
							c.tourStep++;
							c.getShops().openShop(25);
							c.sendMessage(
									"<col=800000>To get Vote Tickets and random chance for a rare do ::vote");
							prevStep = c.tourStep;
							timer = 5;

						}

						if (c.tourStep == 15) {
							c.tourStep++;
							c.getPA().closeAllWindows();
							c.getPA().playerWalk(3181, 3446);
							c.getDH().sendPlayerChat1("Donator Stores with Megan.");
							c.faceLocation(3180, 3446);
							prevStep = c.tourStep;
							timer = 3;

						}

						if (c.tourStep == 17) {
							c.tourStep++;
							c.getPA().playerWalk(3181, 3437);
							c.getDH().sendPlayerChat1("Our Lottery Man.");
							c.faceLocation(3180, 3437);
							prevStep = c.tourStep;
							timer = 5;
						}

						if (c.tourStep == 19) {
							c.tourStep++;
							c.getPA().playerWalk(3181, 3417);
							c.getDH().sendPlayerChat3(
									"Click on your enchanted gem in your inventory",
									" to teleport to any slayer master. There are no Slayer Level Requirements",
									"needed to kill any Slayer Monsters.");
							prevStep = c.tourStep;
							timer = 10;
						}

						if (c.tourStep == 21) {
							c.tourStep++;
							// c.getPA().playerWalk(3087, 3516);
							c.getPA().spellTeleport(3087, 3516, 0);
							c.getDH().sendPlayerChat3(
									"Best of all is the PKing on here.",
									"We guarantee the best Hybridding PVP here.",
									"Ingame PK Boards are located here also.");
							prevStep = c.tourStep;
							timer = 10;
						}

						if (c.tourStep == 23) {
							c.tourStep = 777;
							c.getPA().spellTeleport(2678, 3718, 0);
							c.getDH().sendPlayerChat3("Those are the basics! :)",
									"Now go train Combat skills at Rock Crabs",
									"or Teleport to Skilling areas for Skilling.");
							if (!addedStarter) {
								c.getItems().addItem(995, 100000);
							}
							container.stop();
						}

					}

					@Override
					public void stop() {
						if (c != null) {
							c.tourStep = 777;
							c.sendMessage(
									"<col=800000>Your basic tour has ended. Enjoy your time here! Post suggestions on ::forums");
							c.sendMessage(
									"If you need help, ask for help in clan chat 'help'");
							c.sendMessage("<col=800000>To view commands type ::commands");
						}
					}

				}, 2);
			}

		}

	}

	public void stepAway() {
		c.getMovement().resetWalkingQueue();
		if (RegionClip.getClippingDirection(c.getX() - 1, c.getY(), c.heightLevel, -1,
				0, c.getDynamicRegionClip())) {
			c.getPA().walkTo(-1, 0);
		} else if (RegionClip.getClippingDirection(c.getX() + 1, c.getY(), c.heightLevel,
				1, 0, c.getDynamicRegionClip())) {
			c.getPA().walkTo(1, 0);
		} else if (RegionClip.getClippingDirection(c.getX(), c.getY() - 1, c.heightLevel,
				0, -1, c.getDynamicRegionClip())) {
			c.getPA().walkTo(0, -1);
		} else if (RegionClip.getClippingDirection(c.getX(), c.getY() + 1, c.heightLevel,
				0, 1, c.getDynamicRegionClip())) {
			c.getPA().walkTo(0, 1);
		}
	}
	
	public void forceStepAside() {
		
		c.getMovement().resetWalkingQueue();
		
		int x = 0, y = 0;
		if (RegionClip.getClippingDirection(c.getX() - 1, c.getY(), c.heightLevel, -1,
				0, c.getDynamicRegionClip())) {
			x = -1;
		} else if (RegionClip.getClippingDirection(c.getX() + 1, c.getY(), c.heightLevel,
				1, 0, c.getDynamicRegionClip())) {
			x = 1;
		} else if (RegionClip.getClippingDirection(c.getX(), c.getY() - 1, c.heightLevel,
				0, -1, c.getDynamicRegionClip())) {
			y = -1;
		} else if (RegionClip.getClippingDirection(c.getX(), c.getY() + 1, c.heightLevel,
				0, 1, c.getDynamicRegionClip())) {
			y = 1;
		}
		if (x == 0 && y == 0)
			return;
		
		Direction dir = Direction.getDirection(x, y);
		
		ForceMovementMask mask = ForceMovementMask.createMask(x, y, 1, 0, dir);
		c.setForceMovement(mask);
	}

	/**
	 ** GFX
	 **/
	public void stillGfx(int id, int x, int y, int graphicHeight, int time) {
		// synchronized(c) {
		Server.getStillGraphicsManager().stillGraphics(c, x, y, graphicHeight, id,
				time);
		// if (c.getOutStream() != null && c != null && c.isActive) {
		// c.getOutStream().createPacket(85);
		// c.getOutStream().writeByteC(y - (c.getMapRegionY() * 8));
		// c.getOutStream().writeByteC(x - (c.getMapRegionX() * 8));
		// c.getOutStream().createPacket(4);
		// c.getOutStream().writeByte(0);
		// c.getOutStream().writeShort(id);
		// c.getOutStream().writeByte(height);
		// c.getOutStream().writeShort(time);
		// c.flushOutStream();
		// }

	}

	public void stopDiagonal(int otherX, int otherY) {
		if (c.getFreezeTimer() > 0) {
			return;
		}
		c.getMovement().newWalkCmdSteps = 1;
		int xMove = otherX - c.getX();
		int yMove = 0;

		if (xMove < -1) {
			xMove = -1;
		}
		if (xMove > 1) {
			xMove = 1;
		}
		if (yMove < -1) {
			yMove = -1;
		}
		if (yMove > 1) {
			yMove = 1;
		}

		if (!canWalkDir(xMove, yMove)) {
			xMove = 0;
		}

		if (xMove == 0) {
			yMove = otherY - c.getY();
		}

		if (!canWalkDir(xMove, yMove)) {
			yMove = 0;
		}

		/*
		 * int xMove = otherX - c.getX(); int yMove = 0; if (xMove == 0) yMove =
		 * otherY - c.getY();
		 */
		/*
		 * if (!clipHor) { yMove = 0; } else if (!clipVer) { xMove = 0; }
		 */

		c.getMovement().addToWalkingQueue(c.getX() + xMove, c.getY() + yMove);

		// int k = c.getX() + xMove;
		// k -= c.mapRegionX * 8;
		// c.getNewWalkCmdX()[0] = c.getNewWalkCmdY()[0] = 0;
		// int l = c.getY() + yMove;
		// l -= c.mapRegionY * 8;
		//
		// for (int n = 0; n < c.newWalkCmdSteps; n++) {
		// c.getNewWalkCmdX()[n] += k;
		// c.getNewWalkCmdY()[n] += l;
		// }

	}

	public void throwSnowballOnPlayer(Client o) {
		int requiredDist = 6;
		if (o.isDead() || o.disconnected || o.getZ() != c.getZ()) {
			c.getCombat().resetPlayerAttack();
			return;
		}
		int dist = c.distanceToPoint(o.getX(), o.getY());
		if (dist > 20) {
			c.getCombat().resetPlayerAttack();
			return;
		}
		if (c.blockedShot) {
			return;
		}
		if (c.getAttackTimer() > 0) {
			if (dist <= requiredDist) {
				c.getMovement().resetWalkingQueue();
			}
			return;
		}

		if (dist > requiredDist) {
			return;
		}

		c.setAttackTimer(4);
		c.startAnimation(7530);
		c.getMovement().resetWalkingQueue();
		c.followId = c.playerIndex = 0;
		c.getItems().deleteEquipedWeaponSlot();
		
		Projectile proj = new Projectile(1281, c.getCurrentLocation(), o.getCurrentLocation());
		proj.setStartHeight(43);
		proj.setEndHeight(10);
		proj.setStartDelay(c.getCombat().getStartDelay(dist));
		proj.setEndDelay(5 * dist + 70);
		proj.setLockon(o.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		
		if (Server.getCalendar().isXmas()) {
			c.setChristmasPoints(c.getChristmasPoints()+1);
			c.sendMessage("You have " + c.getChristmasPoints() + " Christmas points.");
		}

//		int pX = c.getX();
//		int pY = c.getY();
//		int nX = o.getX();
//		int nY = o.getY();
//		int offX = (pY - nY) * -1;
//		int offY = (pX - nX) * -1;
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				(5 * dist + 70), 1281, 43, 10, -o.getId() - 1,
//				c.getCombat().getStartDelay(dist));

		CycleEventHandler.getSingleton().addEvent(o, true, new CycleEvent() {

			int timer = 3;

			@Override
			public void execute(CycleEventContainer container) {

				if (c == null || c.disconnected || o == null
						|| o.disconnected) {
					container.stop();
					return;
				}

				if (timer == 2) {
					c.resetFace();
				}

				timer--;
				if (timer == 0) {
					o.startGraphic(Graphic.create(1282, GraphicType.LOW));
					container.stop();
				}
			}

			@Override
			public void stop() {
			}
		}, 1);

	}

	public void toggleNote(boolean noted) {
		c.takeAsNote = noted;
		if (c.takeAsNote) {
			c.getPacketSender().setConfig(115, 1);
		} else {
			c.getPacketSender().setConfig(115, 0);
		}
	}

	public void toggleQuickPray() {
		if (c.playerPrayerBook == PrayerBook.NORMAL) {
			if (c.quickPrayers.isEmpty()) {
				c.sendMessage(
						"You have no quick prayers selected, type ::quickprayers with your desired prayers on");
				c.sendMessage("to set them as quick prayers.");
				c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 0);
				return;
			}
			if (c.quickPray) {
				CombatPrayer.resetPrayers(c);
				c.prayerDrainCounter = 0;
				c.quickPray = false;
				c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 0);
			} else {
				for (int i : c.quickPrayers) {
					if (!c.prayerActive[i]) {
						CombatPrayer.activatePrayer(c, i);
					}
				}

				c.quickPray = true;
				c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 1);
			}
		} else {
			if (c.quickCurses.isEmpty()) {
				c.sendMessage(
						"You have no quick curses selected, type ::quickprayers with your desired curses on");
				c.sendMessage("to set them as quick prayers.");
				c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 0);
				return;
			}
			if (c.quickPray) {
				c.curses().resetCurse();
				c.prayerDrainCounter = 0;
				c.quickPray = false;
				c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 0);
			} else {
				for (int i : c.quickCurses) {
					if (!c.curseActive[i]) {
						c.curses().activateCurse(i);
					}
				}
				c.quickPray = true;
				c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 1);
			}
		}
	}

	public void updateGearBonusesInBank() {
		if (FunPkTournament.isTournamentOpen()) {
			c.getPacketSender().sendFrame126(
					"MyGearValue:" + c.getItems().getTotalBonuses()
							+ " | Tournament:" + FunPkTournament.maxBonuses,
					5383);
		} else {
			c.getPacketSender().sendFrame126("     " + c.getNameSmartUp() + "'s Bank",
					5383);
		}
	}
	
	public void useOperate(int itemId) {
		if (SlayerHelm.wearingSlayerHelmFullOrNormal(itemId)) {
			Slayer.checkKills(c);
			return;
		}

		switch (itemId) {
			
			case 13740: // divine spirit shield operate
				if (c.getSpiritShieldHits() <= 0) {
					c.sendMessage("Your spirit shield is not disabled.");
				} else {
					c.sendMessage("Your Spirit Shield is disabled for "
							+ c.getSpiritShieldHits() + " hits.");
				}
				break;

			case 20084:
				if (Misc.random(1) == 0) {
					c.startAnimation(15150);
				} else {
					c.startAnimation(15149);
					c.startGraphic(Graphic.create(2953, GraphicType.LOW));
				}
				break;
			case 15426:
				c.startAnimation(12664);
				break;
			case 12634:
				if (c.wildLevel < 1) {
					c.startAnimation(8903);
					c.startGraphic(Graphic.create(1566, GraphicType.LOW));
					c.getMovement().resetWalkingQueue();
				} else {
					c.sendMessage("You cannot do this in wildy.");
				}
				break;
			case 1712:
			case 1710:
			case 1708:
			case 1706:
			case 1704:
				c.getPA().handleGlory();
				break;
			case 3853:
			case 3855:
			case 3857:
			case 3859:
			case 3861:
			case 3863:
			case 3865:
			case 3867:
				c.getPA().gamesNeck();
			case 11105:
			case 11107:
			case 11109:
			case 11111:
				c.getPA().skillsNeck();
				break;
			case 2552:
			case 2554:
			case 2556:
			case 2558:
			case 2560:
			case 2562:
			case 2564:
			case 2566:
				c.getPA().startTeleport(3362, 3263, 0, TeleportType.MODERN);
				break;
			case 11283:
			case 30336:
				if (!c.dfsPause || c.isDev()) {
					if (c.dfsCharge > 0 || c.getSeasonalData().containsPowerUp(PowerUpData.DRAGONBREATH)) {
						c.toggledDfs = true;
						c.sendMessage("You turned on your shield special. Now hit anyone with it.");
					} else {
						c.sendMessage("Your shield doesn't have any charges to fire.");
					}
				} else {
					c.sendMessage("Your shield is in cooldown.");
				}
				break;
			case 20800:
				c.startAnimation(new Animation(510));//broken animation should be using something else TEMP 510
			    break;
			case 20799:
				c.startAnimation(new Animation(511));//broken animation should be 512 TEMP 510
				break;
			case 20798:
				c.startAnimation(new Animation(511));
				break;
			case 20797:
				c.startAnimation(new Animation(509));
				break;
			case 20796:
				c.startAnimation(new Animation(508));
				break;
			case 20795:
				c.startAnimation(new Animation(507));
				break;
			case 20714:
				c.startAnimation(new Animation(10941));
				break;
			case 12844:
				c.startAnimation(new Animation(8990));
				break;
			case 20716:
				c.startAnimation(new Animation(10940));
				c.startGraphic(Graphic.create(721));
				break;
			case 15707:
				Party.updateSidebar(c, true);
				break;
		}
	}

	public void walkableInterface(int id) {
		if (c.walkableInterface == id) {
			return;
		}

		c.walkableInterface = id;
		c.getPacketSender().showOverlay(id);
	}

	/**
	 * add to walking queue, will noclip objects
	 *
	 * @param moveX
	 *            Direction to move X
	 * @param moveY
	 *            Direction to move Y
	 */
	public void walkTo(int moveX, int moveY) { // new walking method, more clean
		c.getMovement().hardResetWalkingQueue();
		int toX = localizeLocation(c.getX() + moveX, c.getMapRegionX());
		int toY = localizeLocation(c.getY() + moveY, c.getMapRegionY());
		// c.noClip = true;
		// c.noClipX = c.getX() + moveX;
		// c.noClipY = c.getY() + moveY;
		c.getMovement().addToWalkingQueue(toX, toY);
	}

	/**
	 * add to walking queue, will noclip objects
	 *
	 * @param moveX
	 *            Direction to move X
	 * @param moveY
	 *            Direction to move Y
	 */
	public void walkTo(int moveX, int moveY, boolean noClip) { // new walking
																// method, more
																// clean
		c.getMovement().hardResetWalkingQueue();
		int toX = localizeLocation(c.getX() + moveX, c.getMapRegionX());
		int toY = localizeLocation(c.getY() + moveY, c.getMapRegionY());
		c.noClip = noClip;
		c.noClipX = c.getX() + moveX;
		c.noClipY = c.getY() + moveY;
		c.getMovement().addToWalkingQueue(toX, toY);
	}

	public void walkTo(int moveX, int moveY, int moveX2, int moveY2) { // new
																		// walking
																		// method,
																		// more
																		// clean
		c.getMovement().hardResetWalkingQueue();
		int toX = localizeLocation(c.getX() + moveX, c.getMapRegionX());
		int toY = localizeLocation(c.getY() + moveY, c.getMapRegionY());
		c.noClip = true;
		c.noClipX = c.getX() + moveX + moveX2;
		c.noClipY = c.getY() + moveY + moveY2;
		int toX2 = localizeLocation(c.getX() + moveX + moveX2,
				c.getMapRegionX());
		int toY2 = localizeLocation(c.getY() + moveY + moveY2,
				c.getMapRegionY());
		c.getMovement().addToWalkingQueue(toX, toY);
		c.getMovement().addToWalkingQueue(toX2, toY2);
	}

	public void walkToCheck(int i, int j) {
		if (c.getFreezeTimer() > 0) {
			return;
		}
		c.getMovement().newWalkCmdSteps = 0;
		if (++c.getMovement().newWalkCmdSteps > 50) {
			c.getMovement().newWalkCmdSteps = 0;
		}
		int k = c.getX() + i;
		k -= c.mapRegionX * 8;
		c.getMovement().getNewWalkCmdX()[0] = c.getMovement().getNewWalkCmdY()[0] = 0;
		int l = c.getY() + j;
		l -= c.mapRegionY * 8;

		for (int n = 0; n < c.getMovement().newWalkCmdSteps; n++) {
			c.getMovement().getNewWalkCmdX()[n] += k;
			c.getMovement().getNewWalkCmdY()[n] += l;
		}
	}

	public int wildyKeySupply() {
		return PlayerConstants.wildyKeySupply[(int) (Math.random()
				* PlayerConstants.wildyKeySupply.length)];
	}
	
	//spellid is cheaphax for now.
	public void addExpFromCombat(int damage, BattleState state, SpellsData spell) {
		if (!state.isAppendExp()) {
			return;
		}

		if (state.getCombatStyle() == CombatType.MAGIC && spell != null) {
			if (c.magicDef) {
				c.getPA().addSkillXP(damage / 2, Skills.DEFENSE);
			}
			c.getPA().addSkillXP((spell.getExpGain()
					+ (damage * (c.magicDef ? 1 : 2))), Skills.MAGIC);

			c.getPA().addSkillXP((spell.getExpGain() + (damage / 3)), Skills.HITPOINTS);

		} else if (state.getCombatStyle() == CombatType.RANGED) {
			if (damage < 1)
				return;
			if (state.getFightExp() == FightExp.RANGED_DEFENSE) {
				c.getPA().addSkillXP((damage * 4) / 3, Skills.RANGED);
				c.getPA().addSkillXP((damage * 4) / 3, Skills.DEFENSE);
				c.getPA().addSkillXP((damage * 4) / 3, Skills.HITPOINTS);

			} else if (state.getFightExp() == FightExp.RANGED) {
				c.getPA().addSkillXP(damage * 4, Skills.RANGED);
				c.getPA().addSkillXP((damage * 4) / 3, Skills.HITPOINTS);

			} else {
				c.sendMessage("Your Combat Tab attack style is bugged. Resetting to Accurate Mode");
				c.fightMode = 0;
				c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
						ItemProjectInsanity.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
				c.getPA().addSkillXP(damage * 4, Skills.RANGED);
				c.getPA().addSkillXP((damage * 4) / 3, Skills.HITPOINTS);
			}
		} else { // melee leftover
			if (damage < 1)
				return;
			if (state.getFightExp() == FightExp.MELEE_ATTACK) {
				c.getPA().addSkillXP(damage * 4, Skills.ATTACK);
			} else if (state.getFightExp() == FightExp.MELEE_STRENGTH) {
				c.getPA().addSkillXP(damage * 4, Skills.STRENGTH);
			} else if (state.getFightExp() == FightExp.MELEE_SHARED) {
				c.getPA().addSkillXP(damage * 4 / 3, Skills.ATTACK);
				c.getPA().addSkillXP(damage * 4 / 3, Skills.STRENGTH);
				c.getPA().addSkillXP(damage * 4 / 3, Skills.DEFENSE);
			} else if (state.getFightExp() == FightExp.MELEE_DEFENSE) {
				c.getPA().addSkillXP(damage * 4, Skills.DEFENSE);
			} else {
				c.sendMessage("Error with your combat style, change attack style please and notify developers.");
				c.getPA().addSkillXP(damage * 4, Skills.ATTACK);
				c.fightMode = 0;
				c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
						ItemProjectInsanity.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			}
			c.getPA().addSkillXP((damage * 4) / 3, Skills.HITPOINTS);
		}
	}
	
	public void startHomeTeleportCommand() {
		if (c.duelStatus == 5) {
			c.sendMessage("You can't teleport in during a duel.");
		} else if (c.homeTeleActions == 1) {// varrock
			c.getPA().startTeleport(3210, 3424, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 0) {// edgeville
			c.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 2) {// lumbridge
			c.getPA().startTeleport(3222, 3218, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 3) {// falador
			c.getPA().startTeleport(2965, 3378, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 4) {// camelot
			c.getPA().startTeleport(2757, 3477, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 5) {// neitiznot
			c.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 6) {// ardougne
			c.getPA().startTeleport(2662, 3305, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 7) {// shilo village
			c.getPA().startTeleport(2852, 2960, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 8) {// yanille
			c.getPA().startTeleport(2605, 3093, 0, TeleportType.MODERN);
		} else if (c.homeTeleActions == 9) {// neitiznot
			c.getPA().startTeleport(2096, 3913, 0, TeleportType.MODERN);

			// } else {
			// c.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
		}
	}
	
	public boolean reachedNpc(NPC npc) {
		if (c.clickNpcDistance == 1)
			return PathFinder.reachedEntity(c.clickReachDistance, npc.getX(), npc.getY(), c.getX(), c.getY(), c.entityClickSizeX, c.entityClickSizeY, c.entityWalkingFlag, c.disableInteractClip ? 0 : RegionClip.getClipping(c.getX(), c.getY(), c.getZ(), c.getDynamicRegionClip()));
		else {
			if (c.disableInteractClip)
				return withinDistanceToNpc(npc, c.clickNpcDistance);
			else
				return RegionClip.rayTraceBlocked(c.getX(), c.getY(), npc.getX(), npc.getY(), c.getZ(), true, c.getDynamicRegionClip());
		}
	}

}