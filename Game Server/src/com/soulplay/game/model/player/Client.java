package com.soulplay.game.model.player;

import java.util.*;
import java.util.concurrent.TimeUnit;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.clans.ClanManager;
import com.soulplay.content.click.object.ObjectFirstClick;
import com.soulplay.content.click.object.ObjectFourthClick;
import com.soulplay.content.click.npc.NpcFirstClick;
import com.soulplay.content.click.npc.NpcFourthClick;
import com.soulplay.content.click.npc.NpcSecondClick;
import com.soulplay.content.click.npc.NpcThirdClick;
import com.soulplay.content.click.object.ObjectFifthClick;
import com.soulplay.content.click.object.ObjectSecondClick;
import com.soulplay.content.click.object.ObjectThirdClick;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.pos.ClaimItems;
import com.soulplay.content.items.search.MarketOfferInterface;
import com.soulplay.content.items.impl.RandomBoxLottery;
import com.soulplay.content.minigames.FightPitsTournament;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.barrows.Barrows;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.minigames.duel.TradeAndDuel;
import com.soulplay.content.minigames.gamble.GambleStage;
import com.soulplay.content.minigames.gamble.GamblingManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.partyroom.PartyOffer;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.minigames.soulwars.Overlay;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.npcs.impl.bosses.kraken.KrakenBoss;
import com.soulplay.content.npcs.impl.bosses.vorkath.VorkathBoss;
import com.soulplay.content.player.BankPin;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.combat.AttackNPC;
import com.soulplay.content.player.combat.CombatAssistant;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.melee.MeleeData;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.dailytasks.CategoryInterfaceHandler;
import com.soulplay.content.player.dailytasks.DailyTaskHandler;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.skills.SkillInterfaces;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.StorageObject;
import com.soulplay.content.player.skills.construction.popularhouse.PopularHouses;
import com.soulplay.content.player.skills.construction.specialobjects.WhiteBerryBushProcess;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.farming.herb.HerbFarming;
import com.soulplay.content.player.skills.farming.tree.TreeFarming;
import com.soulplay.content.player.skills.runecrafting.Runecrafting;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.content.player.skills.summoning.SummonOrbButtons;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.content.shops.ShopAssistant;
import com.soulplay.content.tob.TobManager;
import com.soulplay.content.vote.VoteInterfaceRewardsSection;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemAssistant;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.UseItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.NpcIds;
import com.soulplay.game.model.npc.NpcUpdating;
import com.soulplay.game.model.objects.RevenantCaveEntrance;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.player.save.PlayerSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.model.player.save.SoulPlayDateSave;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.map.travel.ZoneManager;
import com.soulplay.game.world.map.travel.zone.addon.DeepWildDungeon;
import com.soulplay.game.world.map.travel.zone.addon.ScorpiaZone;
import com.soulplay.game.world.map.travel.zone.addon.WildGodWarsDungeonZone;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.net.ConnectedFrom;
import com.soulplay.net.InPacket;
import com.soulplay.net.Packet;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.packet.PacketHandler;
import com.soulplay.net.packet.out.PacketSender;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.MagicOnObjectExtension;
import com.soulplay.util.ISAACCipher;
import com.soulplay.util.Misc;
import com.soulplay.util.Stream;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.soulplaydate.SoulPlayDateEnum;
import com.soulplay.util.sql.Highscore;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import plugin.button.infotab.InfoTabButtonPlugin;

public class Client extends Player {

	public static final Graphic PRAYER_RENEW_GFX = Graphic.create(1296);

	public Stream inStream = null, outStream = null;

	private Channel session;
	
	private BankPin bankPin = new BankPin(this);
	
	private Runecrafting runecrafting = new Runecrafting(this);
	
	private ItemAssistant itemAssistant = new ItemAssistant(this);
	
	private ShopAssistant shopAssistant = new ShopAssistant(this);
	
	private TradeAndDuel tradeAndDuel = new TradeAndDuel(this);
	
	private PlayerAssistant playerAssistant = new PlayerAssistant(this);
	
	private CombatAssistant combatAssistant = new CombatAssistant(this);
	
	private static final int QUEUED_PACKETS_SIZE = 100;
	
	private Deque<InPacket> newPackets = new ArrayDeque<>(QUEUED_PACKETS_SIZE);
	
	private Deque<InPacket> queuedPackets = new ArrayDeque<>(QUEUED_PACKETS_SIZE);
	
	private SkillInterfaces skillInterfaces = new SkillInterfaces(this);
	
	private Barrows barrowsClass = new Barrows(this);
	
	private final MarketOfferInterface marketOffer = new MarketOfferInterface(this);
	
	private final ClaimItems claimItems = new ClaimItems(this);
	
	private final PartyOffer partyOffer = new PartyOffer(this);
	
	private final StorageObject storageObject = new StorageObject(this);
	
	/**
	 * Skill instances
	 */
	private final PacketSender packetSender = new PacketSender(this);
	
	private PlayerDifficulty playerDifficulty = new PlayerDifficulty();
	
	public int diceItem;
	
	public int page;
	
	private boolean loadingClan = false;
	
	private long fishingCooldown = 0;
	
	private Map<Integer, String> interfaceText = new HashMap<>();
	
	public int championScrollUsed = 0;
	
	public boolean[] championRules;
	
	public int packetSize = 0, packetType = -1;
	
	private boolean tempWarn = false;
	
	private boolean isBusy = false;
	
	private final HerbFarming herbs = new HerbFarming(this);
	
	private final TreeFarming trees = new TreeFarming(this);
	
	public boolean donationSkillReset = false;
	
	public long lastSpam = System.currentTimeMillis();
	
	public int spamMessage = 0;
	
	public long lastSaved = System.currentTimeMillis();
	
	public boolean confirmUnjail = false;
	
	public Client(Channel s) {
		this.session = s;
		outStream = new Stream(new byte[Config.BUFFER_SIZE]);
		outStream.currentOffset = 0;
		inStream = new Stream(new byte[Config.BUFFER_SIZE]);
		inStream.currentOffset = 0;
	}
	
	public ClaimItems getClaimItems() {
		return claimItems;
	}
	
	public boolean canWalk() {
		return canWalk;
	}
	
	public boolean checkBusy() {
		/*
		 * if (getCombat().isFighting()) { return true; }
		 */
		if (isBusy) {
			// actionAssistant.packetSender.sendMessage("You are too busy to do
			// that.");
		}
		return isBusy;
	}
	
	public boolean checkEmpty() { // dungeoneering
		for (int element : this.playerEquipment) {
			if (element != -1) {
				return false;
			}
		}
		for (int playerItem : this.playerItems) {
			if (playerItem != 0) {
				return false;
			}
		}
		return true;
	}

	private final TickTimer serverTimeRefresh = new TickTimer();

	public boolean checkPacket126Update(String text, int id) {
		String t = interfaceText.get(id);
		if (t != null && t.equals(text)) {
			return false;
		}

		interfaceText.put(id, text);
		return true;
	}

	public void coinReward() { // coin vote box
		if (getItems().playerHasItem(3062) && (getItems().freeSlots() > 1)) {
			getItems().deleteItemInOneSlot(3062, 1);
			if (Misc.random(100) == 1) {
				getItems().addItem(995, 1000000 + Misc.random(10000000));
			} else if (Misc.random(25) == 2) {
				getItems().addItem(995, 500000 + Misc.random(1500000));
			} else {
				getItems().addItem(995, 100000 + Misc.random(100000));
			}
		} else {
			sendMessage("You need 2 free slots.");
		}
	}
	
	public void correctCoordinates() {
		if (inPcGame()) {
			getPA().movePlayer(2657, 2639, 0);
		}
		if (inFightCaves()) {
			getPA().movePlayer(LocationConstants.EDGEVILLE_X, LocationConstants.EDGEVILLE_Y, 0);
			// getPA().movePlayer(absX, absY, getId() * 4);
			// packetSender.sendMessage("Your wave will start in 10 seconds.");
			// CycleEventHandler.getSingleton().addEvent(this, new CycleEvent()
			// {
			// @Override
			// public void execute(CycleEventContainer container) {
			// Server.fightCaves.spawnNextWave((Client)
			// PlayerHandler.players[getId()]);
			// container.stop();
			// }
			//
			// @Override
			// public void stop() {
			//
			// }
			// }, 2);
			
		}
		
	}
	
	@Override
	public void destruct() {
		if (session == null) {
			return;
		}
		// if (underAttackBy > 0 || underAttackBy2 > 0)
		// return;
		// if (playerRights != 3 && playerRights != 2) {
		// Highscores.updateHighscores(this);
		// }
		
		// TODO: highscores removed temporarily
		/*
		 * if (disconnected == true && playerRights != 3 && playerRights != 2 &&
		 * !Config.SERVER_DEBUG) { if (difficulty == 0) {
		 * SimpleSql.updateHighscores(this, Config.EASY_HS_TABLE_NAME); } else if
		 * (difficulty == 1) { SimpleSql.updateHighscores(this,
		 * Config.MEDIUM_HS_TABLE_NAME); } else if (difficulty == 2) {
		 * SimpleSql.updateHighscores(this, Config.HARD_HS_TABLE_NAME); } else if
		 * (difficulty == 3) { SimpleSql.updateHighscores(this,
		 * Config.LEGENDARY_HS_TABLE_NAME); } else if (isIronMan()) {
		 * SimpleSql.updateHighscores(this, Config.IRONMAN_HS_TABLE_NAME); } }
		 */
		
		// if (duelStatus == 6) {
		// getTradeAndDuel().claimStakedItems();
		// }
		// if (duelStatus >= 1 && duelStatus <= 5) {
		// getTradeAndDuel().bothDeclineDuel();
		// saveCharacter = true;
		//// return;
		// }
		
		// if(absX >= 2847 && absX <= 2876 && absY >= 3534 && absY <= 3556 ||
		// absX >= 2838 && absX <= 2847 && absY >= 3543 && absY <= 3556 &&
		// heightLevel == 2) {
		// inCyclops = false;
		// getPA().movePlayer(2846, 3541, 2);
		// kamfreenaDone = false;
		// }
		
		// PenguinUtil.getProfileForPlayer(this).logout();
		// System.out.println(this.getName());
		// penguinProfile.logout();
		
		// if (inFunPk()) {
		// getPA().movePlayer(3303, 3119, 0);
		// }
		
		// Slayer.leaveGroup(this);
		
		// if (inCw()) {
		// CastleWars.removePlayerFromCw(this);
		// }
		// if (inCwWait()) {
		// CastleWars.leaveWaitingRoom(this);
		// }
		
		// if (disconnected == true) {
		//// if (getDuelingClient() != null) {
		//// getDuelingClient().getTradeAndDuel().duelVictory();
		//// getPA().movePlayer(3363 + Misc.random(7), 3264 + Misc.random(5),
		// 0);
		//// }
		// getTradeAndDuel().declineTrade();
		// getTradeAndDuel().declineDuel();
		// //saveCharacter = true;
		// }
		Clan clan = getClan();
		if (clan != null) {
			clan.removeMember(this);
		}

		// if (gwdCoords())
		// packGwdKc();
		
		// if(currentRegion != null)
		// currentRegion.removePlayer(this);
		Misc.println("[Logged out]: " + getName() + "");
		
		if (getSummoning().summonedFamiliar != null && summoned != null) {
			// summoned.npcTeleport(0, 0, 0);
			getSummoning().dismissFamiliar(true);
		}
		// CycleEventHandler.getSingleton().stopEvents(this);
		disconnected = true;
		forceDisconnect = true;
		session.close();
		session = null;
		inStream = null;
		outStream = null;
		isActive = false;
		super.destruct();
	}
	
	@Override
	public boolean equals(Object o) {
		
		if (o == null) {
			return false;
		}
		
		if (o instanceof Client) {
			
			Client cl = (Client) o;
			
			return cl.hashCode() == hashCode();
		}
		
		return false;
		
	}
	
	public void fixLocations(int x, int y) {
		if (playerIsInHouse || (x < 100 && y < 100)) {
			Construction.leaveHouse(this);
		}
		
		KrakenBoss.logoutInsideKrakenInstancedMap(this);

		if (RSConstants.zulrahArea(x, y)) {
			getPA().movePlayer(LocationConstants.EDGEVILLE);
		}
		if (RSConstants.vorkathFightArea(x, y)) {
			getPA().movePlayer(VorkathBoss.OUTSIDE_LOC);
		}
		
		if (RSConstants.inCastleWarsArena(x, y)) {
			CastleWars.removePlayerFromCw(this);
		} else if (RSConstants.cwSaraWaitingRoom(x, y) || RSConstants.cwZammyWaitingRoom(x, y)) {
			CastleWars.leaveWaitingRoom(this);
		}
		if (RSConstants.inSoulWars(x, y)) {
			SoulWars.removePlayerFromGame(this);
		} else if (RSConstants.inSoulWarsRedRoom(x, y) || RSConstants.inSoulWarsBlueRoom(x, y)) {
			SoulWars.removePlayerWaitingRoom(this);
		}
		if (RSConstants.inFunPk(x, y)) {
			getPA().movePlayer(3303 + Misc.random(1), 3120, 0);
		}
		
		if (inDuelArena()) {
			getPA().movePlayer(1 + Config.DUELING_RESPAWN_X + (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
					2 + Config.DUELING_RESPAWN_Y + (Misc.random(Config.RANDOM_DUELING_RESPAWN - 1)), 0);
		}
		
		if (RSConstants.inBarbarianAssaultArena(absX, absY)) {
			getBarbarianAssault().endGame(false);
		}
		
		if (RSConstants.inBarbarianAssaultArena(x, y)) {
			getBarbarianAssault().onLogin();
		}
		
		if (inClanWars()) {
			ClanWarHandler.moveToClanWarsLobby(this);
			// getPA().movePlayer(ClanWarConstants.DEATHX + Misc.random(3),
			// ClanWarConstants.DEATHY + Misc.random(3), 0);
		}

		if (inLMSWaitingRoom()) {
			LmsManager.removePlayerWaitingRoom(this);
		}
		
		if (RSConstants.inResourceArea(getX(), getY())) {
			getPA().movePlayer(3184, 3946, 0);
		}
	}
	
	// Makes the sidebar Icons flash
	// Usage: i1 = 0 through -12 inorder to work
	public void flashSideBarIcon(int i1) {
		if (getOutStream() != null && isActive) {
			getOutStream().createPacket(24);
			getOutStream().writeByteA(i1);
			flushOutStream();
		}
	}
	
	public /* synchronized */ void flushOutStream() {
		if (/* !loginPacketsSent */!isActiveAtomic() || getOutStream().currentOffset == 0 || !session.isActive()
				|| disconnected || forceDisconnect) {
			// if (getOutStream().currentOffset != 0) {
			// System.out.println("channel active "+session.isActive());
			// System.out.println("disc:"+disconnected+ "
			// fDisc:"+forceDisconnect);
			// }
			return;
		}

//    	byte[] temp = new byte[getOutStream().currentOffset];
//    	System.arraycopy(getOutStream().buffer, 0, temp, 0, temp.length);
//    	Packet packet = new Packet(-1, Type.FIXED, Unpooled.wrappedBuffer(temp));
//    	session.writeAndFlush(packet);
//    	getOutStream().currentOffset = 0;
	}
	
	public /* synchronized */ void flushOutStream2() {
		if (/* !loginPacketsSent */!isActiveAtomic() || getOutStream().currentOffset == 0 || !session.isActive()
				|| disconnected || forceDisconnect) {
			// if (getOutStream().currentOffset != 0) {
			// System.out.println("channel active "+session.isActive());
			// System.out.println("disc:"+disconnected+ "
			// fDisc:"+forceDisconnect);
			// }
			return;
		}
		
		final int packetLength = getOutStream().currentOffset;
		final ByteBuf buf = session.alloc().buffer(packetLength);
		buf.writeBytes(getOutStream().buffer, 0, packetLength);
		Packet packet = new Packet(-1, buf);
		session.writeAndFlush(packet, session.voidPromise());
		getOutStream().currentOffset = 0;
	}
	
	// private void updateFriendsList() {
	//// for (int i = 0; i < friends.length; i++) { // update my own chat
	//// if (friends[i] > 0) {
	////// packetSender.sendMessage("friend
	// "+Misc.longToPlayerName2(friends[i]));
	//// Client c2 = (Client)
	// PlayerHandler.getPlayer(Misc.longToPlayerName2(friends[i]));
	//// if (c2 != null) {//if
	// (PlayerHandler.isPlayerOnline(Misc.longToPlayerName2(friends[i]))) {
	//// getPA().loadPM(friends[i], 1);
	////// c2.getPA().loadPM(Misc.playerNameToInt64(getName()), 1);
	//// } else {
	//// getPA().loadPM(friends[i], 0);
	//// }
	//// }
	//// }
	//
	// long myLongId = getLongName();// Misc.playerNameToInt64(getName());
	//
	// // update my own chat?
	// Iterator<Map.Entry<Long, String>> it2 =
	// getFriendMap().entrySet().iterator();
	// while (it2.hasNext()) {
	// Map.Entry<Long, String> entry = it2.next();
	// long friendLongId = entry.getKey();
	// String friendName = entry.getValue();
	// Client c2 = (Client) PlayerHandler.getPlayer(friendName);
	// if (c2 != null && c2 != this) {
	// if (c2.getPrivateChatSetting() == ChatSettings.PRIVATE.ON.ordinal()) {
	// getPA().loadPM(friendLongId, 1);
	// } else if (c2.getPrivateChatSetting() ==
	// ChatSettings.PRIVATE.FRIENDS.ordinal() && c2.isInFriendMap(myLongId)) {
	// getPA().loadPM(friendLongId, 1);
	// } else {
	// getPA().loadPM(friendLongId, 0);
	// }
	// } else if (c2 == null) {
	// getPA().loadPM(friendLongId, 0);
	// }
	// }
	//
	//
	// //Update other players that are online.
	// Iterator<Map.Entry<Long, Player>> it =
	// PlayerHandler.getPlayerMap().entrySet().iterator();
	// while (it.hasNext()) {
	// Client c2 = (Client) it.next().getValue();
	// if (c2 != null && c2 != this) {
	// if (c2.isInFriendMap(myLongId)) {
	// if (getPrivateChatSetting() == ChatSettings.PRIVATE.ON.ordinal()) {
	// c2.getPA().loadPM(myLongId, 1);
	// } else if (getPrivateChatSetting() ==
	// ChatSettings.PRIVATE.FRIENDS.ordinal() &&
	// isInFriendMap(c2.getLongName())) {
	// c2.getPA().loadPM(myLongId, 1);
	// } else {
	// c2.getPA().loadPM(myLongId, 0);
	// }
	// }
	// }
	// }
	//
	// }
	
	// public void processFollow() {
	// if (followId > 0) {
	// getPA().followPlayer();
	// } else if (followId2 > 0) {
	// getPA().followNpc();
	// }
	// }
	
	public BankPin getBankPin() {
		return bankPin;
	}
	
	public Barrows getBarrows() {
		return barrowsClass;
	}
	
	@Override
	public CombatAssistant getCombat() {
		return combatAssistant;
	}
	
	public long getFishingCooldown() {
		return fishingCooldown;
	}
	
	// public PlayerKilling getKill() {
	// return playerKilling;
	// }
	
	public HerbFarming getHerbs() {
		return this.herbs;
	}
	
	public /* synchronized */ Stream getInStream() {
		return inStream;
	}
	
	// public TradeLog getTradeLog() {
	// return tradeLog;
	// }
	
	@Override
	public ItemAssistant getItems() {
		return itemAssistant;
	}
	
	// public Food getFood() {
	// return food;
	// }
	
	public /* synchronized */ Stream getOutStream() {
		if (Config.RUN_ON_DEDI && !skipThreadCheck && !firstWarning
				&& !Thread.currentThread().getName().equals("pool-1-thread-1")) {
			firstWarning = true;
			System.out.println("Wrong thread access! Thread:" + Thread.currentThread().getName());
			for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
				System.out.println(ste);
			}
		}
		return outStream;
	}
	
	@Override
	public PlayerAssistant getPA() {
		return playerAssistant;
	}
	
	@Override
	public PacketSender getPacketSender() {
		return packetSender;
	}
	
	public /* synchronized */ int getPacketSize() {
		return packetSize;
	}
	
	public /* synchronized */ int getPacketType() {
		return packetType;
	}
	
	public PlayerAssistant getPlayerAssistant() {
		return playerAssistant;
	}
	
	public PlayerDifficulty getPlayerDifficulty() {
		return playerDifficulty;
	}
	
	public Channel getSession() {
		return session;
	}
	
	@Override
	public ShopAssistant getShops() {
		return shopAssistant;
	}
	
	public SkillInterfaces getSI() {
		return skillInterfaces;
	}
	
	public TradeAndDuel getTradeAndDuel() {
		return tradeAndDuel;
	}
	
	public TreeFarming getTrees() {
		return this.trees;
	}
	
	/**
	 * Skill Constructors
	 */

	public void giveDonatorChestReward() {
		getItems().addItem(getPA().donatorRitem(), 1);// Misc.random(1));
		// c.getItems().addItem(donatorRitem2(),Misc.random(1));
		getItems().addItem(995, 1000000 + Misc.random(9000000));
	}
	
	public void giveReward() { // vote box
		if (getItems().playerHasItem(6199) && getItems().freeSlots() > 1) {
			getItems().deleteItemInOneSlot(6199, 1);
			getItems().addItem(randomlottery(), 1);
		} else {
			sendMessage("You need 2 free slots.");
		}
	}
	
	public void handleDCLogoutBeforeSave() {
		if (getDuelingClanWarClient() != null) {
			ClanDuel.declineStake(this, getDuelingClanWarClient());
		}

		if (summoned != null) {
			if (summoned.isPetNpc()) {
				Pet.pickupPet(this, Pet.getPetForNPC(summoned.npcType));
			} else if (getSummoning().summonedFamiliar != null) {
				getSummoning().dismissFamiliar(Config.SERVER_DEBUG ? false : true);
			}
		}
		
		if (getZones().isInDmmLobby()) {
			Tournament.removeFromLobby(this, false);
		}
		
		if (isInLmsArena()) {
			lmsManager.removePlayerFromArena(this, true);
		}
		
		//safety for lms skills
		if (getSpawnedSkills() != null)
			setSpawnedSkills(null);
		
		if (inBossRoom) {
			absX = LocationConstants.EDGEVILLE_X;
			absY = LocationConstants.EDGEVILLE_Y;
			heightLevel = 0;
		}
		if (inTrade) {
			getTradeAndDuel().declineTrade();
		}
		if (duelStatus == 5) {
			if (PlayerHandler.isKickAllPlayers()) {
				if (getDuelingClient() != null) {
					getTradeAndDuel().sendBothDraw(getDuelingClient());
				}
			}
			if (getDuelingClient() != null
					&& (getDuelingClient().duelStatus == 5 || getDuelingClient().duelStatus == 6)) {
				getPA().rewardDuelKiller(getDuelingClient());
			}
		} else if (duelStatus >= 1 && duelStatus <= 4) {
			if (getDuelingClient() != null) {
				getDuelingClient().getTradeAndDuel().declineDuel();
			} else {
				getTradeAndDuel().declineDuel();
			}
		}

		if (gambleStage != GambleStage.NONE) {
			GamblingManager.hardClose(this);
		}

		packGwdKc();
		
		boolean sellingShopItems = false;
		for (int playerShopItem : playerShopItems) {
			if (playerShopItem > 0) {
				sellingShopItems = true;
				break;
			}
		}
		if (sellingShopItems) {
			// for (Player p : PlayerHandler.players) {
			for (Player p : Server.getRegionManager().getLocalPlayers(this)) {
				if (p == null || p.disconnected) {
					continue;
				}
				if (p.myShopClient != null && p.myShopClient.equals(this)) {
					p.getPA().closeAllWindows();
				}
			}
		}
		Slayer.leaveGroup(this);
		if (inPcGame()) {
			PestControl.removePlayer(this);
		}
		if (inNomadsRoom()) {
			getPA().movePlayer(3087, 3499, 0);
		}
		if (getPA() != null) {
			fixLocations(getX(), getY());
		}

		if (playerMagicBook == SpellBook.DUNGEONEERING) {
			playerMagicBook = previousMagicBook;
		}

		ResourceArea.dropPackages(this);
		MagicalCrate.dropMagicalCrateInWild(this);

		if (raidsManager != null) {
			raidsManager.handleLogout(this);
		}

		if (infernoManager != null) {
			infernoManager.teleportOut();
		}

		if (insideDungeoneering()) {
			Party.handleLogout(this);
		} else {
			Party.leavePartyFast(this, true);
		}
		//Dung stuff end
		if (getX() < 200 || getY() < 200) { // inside construction map
			if (getPA() != null) {
				getPA().movePlayer(3079, 3491, 0);
			}
		}

		ZoneManager.leaveZones(this);

		for (int i = 0; i < barrows.length - 1; i++) {
			if (barrows[i] == 1) {
				barrows[i] = 0;
			}
		}
		
		//convert run energy double into int for save
		setRunEnergyForSave((int)getRunEnergy());

		getCombat().resetPlayerAttack();
		movement.resetWalkingQueue();
		CycleEventHandler.getSingleton().stopEvents(this);
		GsonSave.savePatches(this);// FarmingManager.savePatches(this);
		
		//if player is moved, process their new location so it can remove their items and shit
		getMovement().getNextPlayerMovement();
	}
	
	@Override
	public int hashCode() {
		return getId();
	}
	
	public void hasPassedNewUUIDTest() {
		PlayerSaveSql.startLoginStateChange(mySQLIndex, Config.NODE_ID);
		dialogueAction = 0;
		inVerificationState = false;
		hidePlayer = false;
		correctlyInitialized = true;
		getPA().closeAllWindows();
		getPacketSender().setBlackout(0);
		
		if (familiarPouchId > 0) {
			getSummoning().summonFamiliar(familiarPouchId, true);
		}
	}
	
	public boolean hasPet() {
		return (summoned != null && summoned.isPetNpc());
	}

	public void updateDailyTasksState() {
		if (dailyTasksLocked()) {
			getPacketSender().sendConfig(ConfigCodes.TASKS_LOCKED, 1);
		} else {
			getPacketSender().sendConfig(ConfigCodes.TASKS_LOCKED, 0);
		}
	}

	@Override
	public void onInit() {
		// first thing to send is this
		randomShitComesFirst();
		
		//load run energy into the double
		setRunEnergy(getRunEnergyForSave());
		
		// if (getPublicChatSetting() < 0 || getPublicChatSetting() >
		// ChatSettings.PUBLIC.values.length)
		// setPublicChatSettings(0);
		//
		// if (getPrivateChatSetting() < 0 || getPrivateChatSetting() >
		// ChatSettings.PRIVATE.values.length)
		// setPrivateChatSettings(0);
		//
		// if (getTradeChatSetting() < 0 || getTradeChatSetting() >
		// ChatSettings.TRADE.values.length)
		// setTradeChatSettings(0);
		//
		// if (getClanChatSetting() < 0 || getClanChatSetting() >
		// ChatSettings.CLAN.values.length)
		// setClanChatSettings(0);

		playerIsMember(1); // keep these two at the top and at this order.
		getPacketSender().camReset();
		
		getMovement().getNextPlayerMovement(); // move player to spot before doing anything else to avoid exploits with zone manager.
		
		getSaveTimer().startTimer(PlayerConstants.SAVE_TICKS);

		if (!Config.SERVER_DEBUG) {
			
			lastVeng = System.currentTimeMillis(); // cheaphax to veng then
			// relog
			if (!addStarter) {
//				if (!PKBoards.records.isEmpty()) {
//					if (PKBoards.records.get(0).playerName.equals(getName())) {
//						if (playerTitle == "" || playerTitle == null || playerTitle.isEmpty()) {
//							packetSender.sendMessage("You have gained the 'Savage' Title for being #1 PKer.");
//							playerTitle = "Savage";
//							titleColor = 2;
//						}
//					} else if (playerTitle.equalsIgnoreCase("Savage")) {
//						playerTitle = "";
//						titleColor = 0;
//						packetSender.sendMessage("You have lost the Savage Title.");
//					}
//				}
				if (Highscore.getTopPvmer().equals(getName())) { // if
					// (PlayerHandler.getTopPvM().equals(getName()))
					// {
					if (playerTitle == "" || playerTitle == null || playerTitle.isEmpty()) {
						sendMessage("You have gained the 'PvM God' Title for being #1 NPC Slayer.");
						playerTitle = "PvM God";
						titleColor = 2;
					}
				} else if (playerTitle.equalsIgnoreCase("PvM God")) {
					playerTitle = "";
					titleColor = 0;
					sendMessage("You have lost the PvM God Title.");
				}
			}
			
			// for (int i = 0; i < playerLevel.length; i++) {
			// playerLevel[i] = getPA().getLevelForXP(playerXP[i]);
			// refreshSkill(i);
			// }
			
			/*if ((getName().equals("Julius")) && !connectedFrom.equals("97.88.160.11")
					&& !connectedFrom.equals("127.0.0.1")) {
				getPacketSender().sendEnterAmountInterface();
				dialogueAction = 9999;
				inVerificationState = true;
			}*/
			
		}
		
		// temporarily set crown to match playerrights
		
		// TODO: check for any crowns that become locked here
		if (!ChatCrown.isCrownUnlocked(this, ChatCrown.REGULAR)) {
			setUnlockedCrown(ChatCrown.REGULAR.ordinal(), true);
		}

		if (isIronMan()) {
			Titles.ironMan(this);

			if (isHCIronMan()) {
				setUnlockedCrown(ChatCrown.HC_IRONMAN.ordinal(), true);
				setCrown(ChatCrown.HC_IRONMAN.ordinal());
			} else {
				setUnlockedCrown(ChatCrown.IRONMAN.ordinal(), true);
				setCrown(ChatCrown.IRONMAN.ordinal());
			}
		} else if (difficulty == PlayerDifficulty.PRESTIGE_ONE) {
			Titles.upgradeLord(this);
			if (!ChatCrown.isCrownUnlocked(this, ChatCrown.PRESTIGE_1)) {
				ChatCrownsSql.unlockChatCrown(this, ChatCrown.PRESTIGE_1);
			}
		} else if (difficulty == PlayerDifficulty.PRESTIGE_TWO) {
			Titles.upgradeLord(this);
			Titles.upgradeLegend(this);
			if (!ChatCrown.isCrownUnlocked(this, ChatCrown.PRESTIGE_1)) {
				ChatCrownsSql.unlockChatCrown(this, ChatCrown.PRESTIGE_1);
			}
			if (!ChatCrown.isCrownUnlocked(this, ChatCrown.PRESTIGE_2)) {
				ChatCrownsSql.unlockChatCrown(this, ChatCrown.PRESTIGE_2);
			}
		} else if (difficulty == PlayerDifficulty.PRESTIGE_THREE) {
			Titles.upgradeLord(this);
			Titles.upgradeLegend(this);
			Titles.upgradeExtreme(this);
			if (!ChatCrown.isCrownUnlocked(this, ChatCrown.PRESTIGE_1)) {
				ChatCrownsSql.unlockChatCrown(this, ChatCrown.PRESTIGE_1);
			}
			if (!ChatCrown.isCrownUnlocked(this, ChatCrown.PRESTIGE_2)) {
				ChatCrownsSql.unlockChatCrown(this, ChatCrown.PRESTIGE_2);
			}
			if (!ChatCrown.isCrownUnlocked(this, ChatCrown.PRESTIGE_3)) {
				ChatCrownsSql.unlockChatCrown(this, ChatCrown.PRESTIGE_3);
			}
		}

		if (getUnlockedCrowns()[getSetCrown()] == 0) {
			sendMessage("Your previous crown: '" + ChatCrown.values[getSetCrown()].toString() + "' has been locked.");
			setCrown(ChatCrown.REGULAR.ordinal());
		}

		InfoTabButtonPlugin.update(this);
		RunePouch.updateRunes(this);
		getPA().startTimePlayedCounter();

		// if (getName().equalsIgnoreCase("Aleksandr")) {
		// hidePlayer = true;
		// }
		
		QuestHandlerTemp.updateQuests(this);
		
		AnnouncementHandler.updateAnnouncement(this);
		hidePlayer = true;
		
		// Server.panel.addEntity(getName()); //Moved to place where i find out
		// if they are properly initialized.
		
		// if (splitChat == true) {
		// getPA().setConfig(502, 1);
		// getPA().setConfig(287, 1);
		// } else {
		// getPA().setConfig(502, 0);
		// getPA().setConfig(287, 0);
		// }
		//
		// if (isRunning2) {
		// getPA().setConfig(504, 1);
		// getPA().setConfig(173, 1);
		// } else {
		// getPA().setConfig(504, 0);
		// getPA().setConfig(173, 0);
		// }
		
		if (getHitPoints() <= 0) {
			getSkills().healToFull();
		}

		int bountyValue = getBountyTargetSpell();
		if (bountyValue > 0) {
			getPacketSender().sendConfig(ConfigCodes.BOUNTY_TELEPORT_UNLOCKED, bountyValue);
		}

		int soundValue = getVar(Variables.SOUND_EFFECT_VOLUME);
		if (soundValue > 0) {
			getPacketSender().sendConfig(169, soundValue);
		}

		int musicValue = Variables.SOUND_EFFECT_VOLUME.getValue(this);
		if (musicValue > 0) {
			getPacketSender().sendConfig(169, musicValue);
		}

		getPacketSender().sendFrame126(Config.SERVER_NAME_SHORT + ", always use the", 2451);
		
		// lastActive = System.currentTimeMillis();
		getPacketSender().sendRunEnergy();
		
		sendMessage(Config.WELCOME_MESSAGE);
		sendMessage("There are currently <col=ff0000>" + PlayerHandler.getPlayerCount()
				+ " </col>players online on World " + Config.NODE_ID + ".");
		// sendMessage("<col=800000>Client Commands: ::XP, ::498,
		// ::Orbs");
		sendMessage("<col=800000>Each time you claim votes, you get a chance at winning a rare item! ::vote");
		if (expLock == true) {
			sendMessage("Your experience is now locked. You will not gain experience.");
		} else {
			sendMessage("Your experience is now unlocked. You will gain experience.");
		}
		if (DBConfig.DOUBLE_DROP_RATE) {
			sendMessage("Drop rates are doubled for the current hour!");
		}
		sendMessage("For commands, type <col=ff>::commands");
		// sendMessage("Current server state is:
		// <col="+Server.getSystemMonitor().getStatusColor()+">" +
		// Server.getSystemMonitor().getStatus()+"</col>");
		calcCombat();
		
		if (getClan() != null) {
			// getClan().updateClanPoints();
			sendMessage("Your clan has " + getClan().getClanPoints() + " clan points.");
		}
		
		// getPA().setConfig(115, 0); //reset note button
		
		// for (int j = 0; j < PlayerHandler.players.length; j++) {
		// if (j == getId())
		// continue;
		// if (PlayerHandler.players[j] != null) {
		// if (PlayerHandler.players[j].getName().equalsIgnoreCase(getName()))
		// disconnected = true;
		// }
		// }
		refreshSkills();
		
		// getPA().handleWeaponStyle();
		getPA().handleLoginText();
		// accountFlagged = getPA().checkForFlags();
		
		// getPA().setChatOptions(getPublicChatSetting(),
		// getPrivateChatSetting(), getTradeChatSetting()); // reset private
		// messaging options
		
		setDefaultSidebars();
		updateDailyTasksState();
		
		// set configs
		getPOH().setBuildMode(false);
		
		// TODO save this setting
		getPOH().setRenderDoorsOpen(false);
		
		// TODO save this setting
		getPOH().setTeleportInside(true);
		
		getPacketSender().showOption(4, 0, "Follow");
		getPacketSender().showOption(5, 0, "Trade with");
		for (int i2 = 0; i2 < playerItemsN.length; i2++) { // TODO: remove after
			// all accounts are
			// fixed. just a
			// temp fix.
			if (playerItemsN[i2] < 0) {
				playerItems[i2] = 0;
				playerItemsN[i2] = 0;
			}
		}
		getItems().resetItems(3214);
		getItems().sendWeapon(playerEquipment[PlayerConstants.playerWeapon], ItemConstants.getItemName(playerEquipment[PlayerConstants.playerWeapon]));
		setUpdateEquipmentRequired(true);
		getPacketSender().itemsOnInterface2(1688, playerEquipment, playerEquipmentN);
		
		MeleeData.getPlayerAnimIndex(this, playerEquipment[PlayerConstants.playerWeapon]);
		getItems().addSpecialBar(false);
		getItems().refreshBonuses();
		PowerUpInterface.rollPowerUps(this);

		final int autocast = loadAutocastSpell();
		if (autocast > 0) {
			getPA().assignAutocast(autocast);
		} else {
			getPA().resetAutocast();
		}
		
		// saveTimer = Config.SAVE_TIMER;
		// saveCharacter = true;
		Misc.println("[Logged in]: " + getName() + "");
		// handler.updatePlayer(this, outStream);
		// flushOutStream();
		// handler.updateNPC(this, outStream);
		// flushOutStream();
		// getPA().resetFollow();
		// getPA().clearClanChat(); //made this clientside
		// getPacketSender().sendFrame126(":prayer:" + (playerPrayerBook == 1 ?
		// "curses" :
		// "prayers"), -1);
		
		if (!isOwner() && !addStarter && !lastDayLoggedIn.equals(Server.getCalendar().getDayOfWeek())) { // TODO READD
			// THIS LOL
			getDH().sendPlayerChat3("Remember to vote for us by doing ::vote it helps",
					Config.SERVER_NAME + " to grow. Voting is also one of the most rewarding",
					"things you can do on " + Config.SERVER_NAME + ".");
		}

		if (!hasBankPin) {
			sendMessage("<img=6> <col=ff0000>You have not set a bank pin code. A Bank pin will help secure your account and");
			sendMessage("<col=ff0000>prevent use of your account without this code. It will also secure your banks storage.");
		}
		
		// if (getFriendMap().size() > 200) {
		// getFriendMap().clear();
		// sendMessage("Your friends list has been reset.");
		// }
		//
		// if (getIgnoreMap().size() > 200) {
		// getIgnoreMap().clear();
		// sendMessage("Your ignore list has been reset.");
		// }
		
		setSlayerTask(SlayerTask.loadTask(taskName, masterName, assignmentAmount));

		if (addStarter) {
			setStartPack(true);
			if (!Config.SERVER_DEBUG) {
				newPlayerAct = 1;
				if (WorldType.equalsType(WorldType.SPAWN)) {
					getPA().addPvPStarter();
				} else {
					getPA().addStarter();
				}
			}
		}

		if (getMinutesPlayed() < 6) {
			setToggleRag(true);
			sendMessage("Rag command has been turned on. To toggle rag mode, type command ::rag");
		}

		if (!lastDayLoggedIn.equals(Server.getCalendar().getDayOfWeek())) { // reset
			// anything
			// daily
			// here
			lastKilledPlayers.clear();
			lastDayLoggedIn = Server.getCalendar().getDayOfWeek();
		}
		
		if (getSkullTimer() > 0) {
			setSkull(true);
		}
		
		updateFlags.add(UpdateFlag.APPEARANCE);
		
		getPacketSender().sendSummoningOrbIndex(SummonOrbButtons.values[getSummoningOrbSetting()].getButtonIndex());
		int placeholderValue = Variables.PLACEHOLDER.getValue(this);
		if (placeholderValue > 0) {
			getPacketSender().sendConfig(ConfigCodes.PLACEHOLDERS, placeholderValue);
		}
		if (autoRet > 0) {
			getPacketSender().setConfig(172, 1);
		}
		updateLawStaffCharges();
		final int brightness = Variables.BRIGHTNESS.getValue(this);
		getPacketSender().sendConfig(166, brightness == 0 ? Config.DEFAULT_BRIGHTNESS : brightness);
		if (isRunning) {
			getPacketSender().sendConfig(173, 1);
		}
		//open jiggig gate
		getPacketSender().sendConfig(455, Misc.setBit(0, 16));

//		final int musicLevel = Variables.MUSIC_LEVEL.getValue(this);
//		getPacketSender().sendConfig(168, musicLevel == 0 ? Config.DEFAULT_MUSIC_LEVEL : musicLevel);

		playerIsMember(2); // 2 for confirming the player has logged in for
		// client purposes
		
		// System.out.println("X:"+getX()+" Y:"+getY());
		
		/*
		 * if(getItems().playerHasEquipped(2572)) { getItems().deleteEquipment(2572,
		 * playerRing); //getItems().removeItem(2572, getItems().getItemSlot(2572));
		 * //getItems().deleteItem(2572, getItems().getItemAmount(2572)); }
		 * if(getItems().playerHasItem(2572)) { getItems().deleteItem(2572,
		 * getItems().getItemAmount(2572)); } if(getItems().itemInBank(2572)) {
		 * getItems().removeBankItem(2572); }
		 */
		// if(playerRights != 3) //TODO: Remove this after adding in Ring of
		// Wealth to server
		// removeItemFromPlayer(2572);
		
		fixLocations(teleportToX, teleportToY);
		
		// if(gwdCoords()) {
		// //System.out.println("POOPO");
		// //sendMessage("KillCount "+getKillCount());
		// //getPA().walkableInterface(16210);
		// //unpackGwdKc();
		// getPacketSender().sendFrame126("" + getTempVar().getInt("ArmaKC") +
		// "", 16216);
		// getPacketSender().sendFrame126("" + getTempVar().getInt("BandosKC") +
		// "",
		// 16217);
		// getPacketSender().sendFrame126("" + getTempVar().getInt("SaraKC") +
		// "", 16218);
		// getPacketSender().sendFrame126("" + getTempVar().getInt("ZammyKC") +
		// "",
		// 16219);
		// getPA().walkableInterface(16210);
		// }
		
		if (Config.isDev(getName())) {
			playerRights = 3;
		}
		
		
		// if (Config.SERVER_DEBUG) {
		// playerRights = 2;
		//// if(addStarter) {
		// for (int i = 0; i < playerLevel.length; i++) {
		// // getPA().setSkillLevel(i, playerLevel[i], playerXP[i]);
		// // refreshSkill(i);
		// playerXP[i] = getPA().getXPForLevel(99) + 5;
		// playerLevel[i] = getPA().getLevelForXP(playerXP[i]);
		// refreshSkill(i);
		//
		// }
		//// }
		// }
		
		// Profile profile = Profile.load(getName());
		// if(profile != null) {
		// profile.initialize();
		// } else {
		// new Profile(this).initialize();
		//
		// }
		
		// penguinProfile = Profile.load(getName());
		// if (penguinProfile != null)
		// penguinProfile.initialize();
		// else {
		// penguinProfile = new Profile(this);
		// penguinProfile.initialize();
		// }
		// grab profile with c.penguinProfile.w/e
		// need to Either loop through the list in profile handler or just
		// creating a profile variable in client class.
		// should only keep one method of loading profile
		Party.handleLogin(this);
		Party.updateSidebar(this, false);

		RaidsManager.handleLoginInsideRaids(this);
		TobManager.handleLoginInsideRaids(this);

		ConnectedFrom.addConnectedFrom(this, connectedFrom, UUID);

		ZoneManager.checkForAddonZones(this, this);
		ZoneManager.applyZone(this);//cheaphax to fix location
		getSeasonalData().init(this);

		if (getZones().isInDmmTourn()) {
			Tournament.onPlayerLogin(this);
		}
	}
	
	public void randomShitComesFirst() {
		
		saveCharacter = true;
		sendPinForNewUUID();
		
		// loginPacketsSent = true;
		
	}
	
	public void alertStaffOfNewPlayer(final String name, final String mac) {
		if (Config.SERVER_DEBUG) {
			return;
		}
		
		final String displayName = name;
		final String UUID = mac;
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				final boolean isUnique = !PlayerSaveSql.isUniqueUUID(UUID);
				
				Server.getTaskScheduler().schedule(()-> {
					PlayerHandler.alertStaff("New Player = ("+ displayName + ") has unique Mac Address=" + isUnique);
				});
			}
		});
	}
	
	public void initializeAfterProperConnection() {
		// getPA().logIntoPM();
		
		// getPA().setPrivateMessaging(2); // pm version 2
		// updateFriendsList(); // pm version 2
		
		if (!inVerificationState) {
			hasPassedNewUUIDTest();
		}

		getPacketSender().sendConfig(465, 4); // open up the cellar tunnel
		if (QuestIndex.COOKS_ASSISTANT.hasCompletedQuest(this)) {
			getPacketSender().sendConfig(678, getRFDProgress() < RecipeForDisaster.MAX_STAGE ? 4 : 5); //recipe for disaster portal
		}

		getSaveTimer().startTimer(PlayerConstants.SAVE_TICKS);
		
		if (isInNexRoom()) {
			inBossRoom = true;
		}
		
		if (heightLevel < 0) {
			getPA().movePlayer(DBConfig.RESPAWN_X, DBConfig.RESPAWN_Y, 0);
		}
		
		if (getEloRating() > 1000) { // elo decay
			int lengthOfDaysAway = Server.getCalendar().getDayOfYear() - lastDayPkedOfYear;
			if (lengthOfDaysAway > 7) {
				int amount = lengthOfDaysAway / 7;
				setEloRating(getEloRating() - amount * 70, true);
				if (getEloRating() < 800) {
					setEloRating(800, true);
				}
			}
		}
		
		MoneyPouch.sendMoneyPouchAmount(this);
		
		updateChatCrownsInterface();
		
		ChatCrown.checkUnlockReqs(this);
		
		if (addStarter) {
			Titles.createAccount(this);
			
			alertStaffOfNewPlayer(getNameSmartUp(), UUID);
			
			if (!Config.SERVER_DEBUG) {
				if (WorldType.equalsType(WorldType.SPAWN)) {
					difficulty = PlayerDifficulty.NORMAL;
					setMode = true;
					sendTourGuide();
				} else{
					starterDialogue();
				}
			} else {
				difficulty = PlayerDifficulty.NORMAL;
				setMode = true;
			}
			// sendTourGuide();
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				getItems().addItem(1856, 1);
				getItems().addItem(4155, 1);
			}
			specAmount = 10.0;
		} else if (lastClanChat != null && !lastClanChat.isEmpty() && !lastClanChat.equals("")) {
			if (getClan() == null) {
				// Clan localClan = ClanManager.getClan(lastClanChat);
				// if (localClan != null) {
				// localClan.addMember(this);
				// getPA().setClanData();
				// }
				
				ClanManager.joinClan(this, lastClanChat);
			}
		}

		getDotManager().checkLogin(this);
		
		if (DBConfig.BETTER_HUNT_REWARD_CHANCE) {
			sendMessage("<col=800000>It's PK Happy Hour! Get increased chances to get rewards from target kills!");
		}
		
		if (WorldType.equalsType(WorldType.SPAWN) && getTotalGold() > 500000) {
			Map<Integer, Integer> map = new HashMap<>();
			map.put(8890, (getTotalGold() > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) getTotalGold()));
			Server.getSlackApi().call(SlackMessageBuilder.buildDuperMessage(getName(), map));
		}
		
		if (getClaimItems().notifyPlayer()) {
			sendMessage("You have items ready to claim from the Trading Post.");
		}
		
		//getPA().runCheatClientTest(); //disable for now
		
		// test if player got logged into loginserver properly
		LoginServerConnection.instance().submit(() -> LoginServerConnection.instance().checkGlitchedLogin(getName()));
		
		createMillInfo();

		newDayLogin();
		DailyTaskHandler.cheaphax(this);

		CategoryInterfaceHandler.updateSidebar(this);
		VoteInterfaceRewardsSection.checkLogin(this);
	}
	
	private void newDayLogin() {
		int prevSoulplayDay = getSoulplayDate().getLastLoginDay();
		int thisSoulplayDay = SoulPlayDate.getDate();
		if (prevSoulplayDay != thisSoulplayDay) {
			SoulPlayDateSave.setNewDay(this, thisSoulplayDay, SoulPlayDateEnum.LAST_DAY_LOGIN);
			
			DailyTaskHandler.newDayReset(this, thisSoulplayDay);
		}
	}
	
	@Override
	public boolean isBusy() {
		return isBusy;
	}
	
	public boolean isLoadingClan() {
		return loadingClan;
	}
	
	public void starterDialogue() {
		getDialogueBuilder()
		.sendNpcChat(NpcIds.STARTER_GUIDE_NPC, "Welcome to SoulPlay!")
		.sendNpcChat(NpcIds.STARTER_GUIDE_NPC, "Would you like to do the Starter Tour",
				WorldType.equalsType(WorldType.SPAWN) ? "and earn <col=ff0000>100</col> Blood money?" 
						: "and earn <col=ff0000>100K</col> Gold?")
		.sendOption("Yes please. Show me around.", ()->{
			getDialogueBuilder().closeNew();
			getPA().startTour();
		}, "No thanks, I'm familiar with this game.", ()-> {
			getDialogueBuilder().closeNew();
		})
		.sendOption("Play Normal Mode.", ()-> {
			setNormalMode();
			getDialogueBuilder().jumpToStage(1);
		}, "Play Ironman Mode", ()-> {
			
		})
		.sendNpcChat(NpcIds.STARTER_GUIDE_NPC, "When you use iron man mode you will not be able to:",
				"obtain items from other players, play team ", "minigames, use lootshare, donate for items or vote",
				"for items")
		.sendNpcChat(NpcIds.STARTER_GUIDE_NPC, "Long story short, you are on your own. would you", "like to turn this option on?")
		.sendOption("Turn into Ironman Mode<col=ff0000>(Can be reversed)", ()-> {
			promoteToIronMan();
			setMode = true;
			getDialogueBuilder().jumpToStage(1);
		}, "Turn into Hardcore Ironman Mode<col=ff0000>(Can be reversed)", ()-> {
			promoteToHCIronMan();
			setMode = true;
			getDialogueBuilder().jumpToStage(1);
		}, "No <col=ff0000>(You can never turn Iron Man Mode on again)", ()-> {
			getDialogueBuilder().jumpToStage(3);
		})
		.sendNpcChat(NpcIds.STARTER_GUIDE_NPC, "Sorry about this bug.")
		.executeIfNotActive();
		getDialogueBuilder().jumpToStage(3);
	}
	
	public void setNormalMode() {
		difficulty = 0;
		setMode = true;
	}
	
	public boolean isNaked() {
		if ((playerEquipment[PlayerConstants.playerHat] == -1) && (playerEquipment[PlayerConstants.playerCape] == -1)
				&& (playerEquipment[PlayerConstants.playerAmulet] == -1)
				&& (playerEquipment[PlayerConstants.playerChest] == -1)
				&& (playerEquipment[PlayerConstants.playerShield] == -1)
				&& (playerEquipment[PlayerConstants.playerLegs] == -1)
				&& (playerEquipment[PlayerConstants.playerHands] == -1)
				&& (playerEquipment[PlayerConstants.playerFeet] == -1)
				&& (playerEquipment[PlayerConstants.playerWeapon] == -1)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void joinHelpCC() {
		if (getClan() == null) {
			// Clan localClan = /*Server.c*/ClanManager.getClan("help");
			// if (localClan != null) {
			// localClan.addMember(this);
			// }
			
			ClanManager.joinClan(this, "help");
			
		}
	}
	
	public void logout() {
		
		if (properLogout) { // already logged out
			return;
		}
		
		// if (playerRights != 3 || playerRights != 2) {
		// Highscores.save(this);
		// }
		
		if (underAttackBy > 0 || underAttackBy2 > 0) {
			sendMessage("You cannot logout while in combat.");
			return;
		}
		
		if (inClanWars() && getClan().isLockedExitPortals()) {
			sendMessage("You can't logout during a clan war fight.");
			return;
		}
		
		if (inBossRoom) {
			sendMessage("You can't log out in a boss room.");
			return;
		}
		
		if (inEasterEvent()) {
			sendMessage("You cannot logout while doing the Easter Event!");
			return;
		}
		
		if (!properLogout()) {
			sendMessage("You cannot logout right now.");
			return;
		}
		
		if (!getBarbarianAssault().canLogout() || (inLMSArenaCoords() && !isDev())) {
			sendMessage("You cannot logout here.");
			return;
		}
		
		if (getZones().isInDmmLobby()) {
			sendMessage("You can't logout here.");
			return;
		}
		
		// synchronized (this) {
		if (!isInCombatDelay()) {
			Clan clan = getClan();
			if (clan != null) {
				clan.removeMember(this);
			}

			onLogout();
			
			// if (!Config.SERVER_DEBUG) {
			// if (!isOwner())
			// PKBoards.addToRecord(this);
			// }
			// PKBoards.addToRecord(this); //TODO: REMOVE
			getPacketSender().sendLogoutPacket();
			// CycleEventHandler.getSingleton().stopEvents(this);
			properLogout = true;
		} else {
			sendMessageSpam("You must wait a few seconds from being out of combat to logout.");
		}
	}
	
	public void onLogout() {
		getBarbarianAssault().leaveGroup();
	}

	public void pkingReward() { // pking box
		if (getItems().playerHasItem(6542) && getItems().freeSlots() > 7) {
			getItems().deleteItemInOneSlot(6542, 1);
			getItems().addItem(15273, 10 + Misc.random(24));
			getItems().addItem(11090, 1);
			getItems().addItem(2570, 1);
			getItems().addItem(15243, 50);
			getItems().addItem(2491, 1);
			getItems().addItem(2497, 1);
			getItems().addItem(4131, 1);
			getItems().addItem(15301, 1);
		} else {
			sendMessage("You need 8 free slots.");
		}
	}
	
	public void playerIsMember(int i) {
		if (getOutStream() != null && isActive) {
			getOutStream().createPacket(249);
			getOutStream().writeByteA(i); // 1 for members, zero for free
			getOutStream().writeLEShortA(getId());
			flushOutStream();
		}
	}
	
	public void walkToLocation(Location destination) {
		PathFinder.findRoute(this, destination.getX(), destination.getY(), true, 1, 1);
	}
	
	public void walkToLocation(int x, int y) {
		PathFinder.findRoute(this, x, y, true, 1, 1);
	}
	
	private void huntTargetArrowDraw() {
		if (getWildernessTick() == 940) {
			if (huntPlayerIndex > 0) {
				getPA().resetHuntId();
			}
		}
		if (huntPlayerIndex <= 0)
			return;
		
		if (PlayerHandler.players[huntPlayerIndex] == null) {
			getPA().resetHuntId();
		} else {
			Player other = PlayerHandler.players[huntPlayerIndex];
			
			
			int otherX = other.getX();
			int otherY = other.getY();
			int dist = distanceToPoint(otherX, otherY);
			if (dist > 15) {
				if (objectHintPos < 0) {
					getPacketSender().createPlayerHints(10, -1);
				}
				
				boolean imInRevDung = getZones().isInRevDung();
				boolean otherInRevDung = other.getZones().isInRevDung();
				
				//get closest cave entrance
				if (imInRevDung != otherInRevDung) {
					RevenantCaveEntrance cave = RevenantCaveEntrance.SOUTH_ENTER;
					if (otherInRevDung) {
						RevenantCaveEntrance north = RevenantCaveEntrance.NORTH_ENTER;
						if (Misc.distanceToPoint(getX(), getY(), cave.getX(), cave.getY()) > Misc.distanceToPoint(getX(), getY(), north.getX(), north.getY()))
							cave = north;
					} else {
						cave = RevenantCaveEntrance.SOUTH_EXIT;
						RevenantCaveEntrance north = RevenantCaveEntrance.NORTH_EXIT;
						if (Misc.distanceToPoint(getX(), getY(), cave.getX(), cave.getY()) > Misc.distanceToPoint(getX(), getY(), north.getX(), north.getY()))
							cave = north;
					}
					
					otherX = cave.getX();
					otherY = cave.getY();
				}
				
				boolean imInWildGwdDung = getZones().containsZoneInList(WildGodWarsDungeonZone.ZONE_SAMPLE);
				boolean otherInWildGwdDung = other.getZones().containsZoneInList(WildGodWarsDungeonZone.ZONE_SAMPLE);
				
				if (imInWildGwdDung != otherInWildGwdDung) {
					if (otherInWildGwdDung) {
						otherX = WildGodWarsDungeonZone.CAVE_OUTSIDE.getX();
						otherY = WildGodWarsDungeonZone.CAVE_OUTSIDE.getY();
					} else {
						otherX = WildGodWarsDungeonZone.CAVE_INSIDE.getX();
						otherY = WildGodWarsDungeonZone.CAVE_INSIDE.getY();
					}
				}
				
				boolean imInScorpiaCave = getZones().getZoneList().contains(ScorpiaZone.ZONE_SAMPLE);
				boolean otherInScorpiaCave = other.getZones().getZoneList().contains(ScorpiaZone.ZONE_SAMPLE);
				
				if (imInScorpiaCave != otherInScorpiaCave) {
					if (otherInScorpiaCave) {
						
						int index = 0;
						int smallestDist = Integer.MAX_VALUE;
						int myBit = getCurrentLocation().toBitPacked();
						for (int i = 0; i < 2; i++) {
							int distance = Math.abs(ScorpiaZone.CAVES[i].toBitPacked() - myBit);
							if (distance < smallestDist) {
								index = i;
								smallestDist = distance;
							}
						}
						
						otherX = ScorpiaZone.CAVES[index].getX();
						otherY = ScorpiaZone.CAVES[index].getY();
						
					} else {
						int index = 0;
						int smallestDist = Integer.MAX_VALUE;
						int myBit = getCurrentLocation().toBitPacked();
						for (int i = 0; i < 3; i++) {
							int distance = Math.abs(ScorpiaZone.CREV[i].toBitPacked() - myBit);
							if (distance < smallestDist) {
								index = i;
								smallestDist = distance;
							}
						}
						
						otherX = ScorpiaZone.CREV[index].getX();
						otherY = ScorpiaZone.CREV[index].getY();
					}
					
				}
				
				boolean imInDeepDungZone = getZones().getZoneList().contains(DeepWildDungeon.ZONE_SAMPLE);
				boolean otherInDeepDungZone = other.getZones().getZoneList().contains(DeepWildDungeon.ZONE_SAMPLE);
				
				if (imInDeepDungZone != otherInDeepDungZone) {
					if (otherInDeepDungZone) {
						otherX = DeepWildDungeon.STAIRS_UPPER.getX();
						otherY = DeepWildDungeon.STAIRS_UPPER.getY();
					} else {
						otherX = DeepWildDungeon.STAIRS_UNDER.getX();
						otherY = DeepWildDungeon.STAIRS_UNDER.getY();
					}
					
				}
				
				getPacketSender().createObjectHints(otherX, otherY, 120, 2);
			} else {
				if (playerHintId < 1) {
					getPacketSender().createObjectHints(3333, 3333, 120, -1);
					getPacketSender().createPlayerHints(10, huntPlayerIndex);
				}
			}
		}
	}
	
	private boolean canAutoSave() {
		if (tradeStatus > 0 || duelStatus > 0 || disconnected || getSpawnedSkills() != null || getZones().isInDmmLobby() || getZones().isInDmmTourn())
			return false;
		return true;
	}

	@Override
	public void process() {
		super.process();

		onlineTick++;
		if (!isBot()) {
			lastActive++;
			if (lastActive > 120 && !performingAction && !isDead()) {
				reconnectPlayer = false;
				forceDisconnect = true;
				resetInCombatTimer();
				disconnected = true;
				performingAction = false;
				// logout();
			} else if (lastActive > 120 && performingAction && !isDead() && !tempWarn) {
				System.out.println("UNABLE TO KICK PLAYER BECAUSE PERFORMINGACTION == TRUE PLAYERNAME:" + getName());
				tempWarn = true;
			}
		}
		
		if (!correctlyInitialized || !saveFile || newPlayer || !saveCharacter || !setMode || inVerificationState) {
			return;
		}
		
		if (Config.RUN_ON_DEDI && getSaveTimer().checkThenStart(PlayerConstants.SAVE_TICKS)) {
			if (canAutoSave()) {
				final Client c = this;
				PlayerSaveSql.SAVE_EXECUTOR.submit(new Runnable() {
					
					@Override
					public void run() {
						PlayerSave.saveGame(c, false);
					}
				});
			}
		}
		
		if (drawDistance < 8) {
			drawDistance++;
		}

		if (serverTimeRefresh.checkThenStart(100)) {
			getPacketSender().sendFrame126(String.format("<col=ff7000>Server Time: <col=ffb000> %s", Misc.formatServerTime()), 16028);
		}
		
		BarbarianAssault.process(this);
		
		if (getDuelRuleTimer() > 0) {
			setDuelRuleTimer(getDuelRuleTimer() - 1);
			if (getDuelRuleTimer() == 0) { // change accept button back to green
				getPacketSender().sendFrame126("Accept", 6680);
			}
		}
		
		processTimers();
		
		PopularHouses.addToEntry(this);
		
		WhiteBerryBushProcess.processBush(this);
		
		if (keepWalkFlag && !isMoving) {
			keepWalkFlag = false;
			getPacketSender().resetMinimapFlagPacket();
		}
		
		if (disconnected) {
			if (!sentPlayerHome) {
				sentPlayerHome = true;
				if (isInCombatWithMob() && (!properLogout() || isInCombatDelay())) {
					sendPlayerHome();
				}
			}
		}
		
		if (getPauseAnimReset() > 0) {
			setPauseAnimReset(getPauseAnimReset() - 1);
		}
		
		if (getDoubleExpLength() > 0) {
			setDoubleExpLength(getDoubleExpLength() - 1);
		}
		if (getDoubleExpLength() == 1) {
			sendMessage("Your double exp has ran out.");
		}

		decreaseTomeExpBoost();
		decreasePetDoubleExp();
		
		if (getJailTime() > 0) {
			setJailTime(getJailTime() - 1);
			if (getJailTime() == 1) {
				sendMessage("Your jail time is over.");
				unjailPlayer();
			}
		}

		if (expireTicks > 0) {
			expireTicks--;

			if (expireTicks == 0 && onExpire != null) {
				onExpire.run();
				onExpire = null;
			}
		}

		if (bankPinWarning > 0) {
			bankPinWarning--;
		}
		
		if (huntPlayerIndex < 1) {
			if (getWildernessTick() > 60) { // changed from 5 minutes to 3
				if (underAttackBy < 1 && playerIndex < 1 && Misc.random(100) < 15) {
					getPA().findRandomPker();
				}
			}
		}
		
		if (huntPlayerIndex > 0) {
			huntTargetArrowDraw();
		}
		
		if (getRunEnergy() < 100) {
			getMovement().restoreRunEnergyProcess();
		}
		getPacketSender().sendRunEnergy();
		
		if (duelCount > 0 && Server.getTotalTicks() % 2 == 0) {
			if (duelCount != 1) {
				forceChat("" + (--duelCount));
				duelDelay = System.currentTimeMillis();
			} else {
				// damageTaken = new int[Config.MAX_PLAYERS];
				forceChat("FIGHT!");
				duelCount = 0;
			}
		}
		if (specAmount < 10 && specRestoreTimerFinished()) {
			specAmount += 1;
			if (specAmount > 10) {
				specAmount = 10;
			}
			
			if (specAmount < 10)
				startSpecRestoreTick();
			
			sendMessage(
					"<col=ff0000>Your special attack energy is at " + Double.toString(specAmount).replace(".", "") + "%");
			getItems().addSpecialBar(true);
		}
		
		if (getNpcAggroTick() < 3000) { // 30 minutes
			setNpcAggroTick(getNpcAggroTick() + 1);
		}

		
//		if (!isLockActions()) {
			if (walkingToObject && finalLocalDestX == 0 && finalLocalDestY == 0) {
				destinationX = destinationY = 0;
				PathFinder.getPathToObject(this);
			} else if (walkingToItem && finalLocalDestX == 0 && finalLocalDestY == 0) {
				destinationX = destinationY = 0;
				if (!PathFinder.findRoute(this, pItemX, pItemY, false, 0, 0, 0, 0, 0, 0, 0, 1)) {
					if (!PathFinder.findRoute(this, pItemX, pItemY, false, 0, 0, 1, 1, 0, 0, 0, 1)) {
						walkingToItem = false;
					}
				}
			} else if (destinationX != 0) { // regular walking or walking to mob
				if (walkingToMob && finalLocalDestX == 0 && finalLocalDestY == 0) { // walking to mob
					if (!PathFinder.findRoute(this, destinationX, destinationY, walkUpToPathBlockedMob, 0, 0, destOffsetX, destOffsetY, entityWalkingFlag, 0, 0, clickReachDistance)) {
						if (!walkUpToPathBlockedMob && !(npcIndex > 0 || playerIndex > 0)) {
							followId2 = followId = 0;
							sendMessage("I can't reach that.");
						}
					}
					getLastNpcLocation().setX(destinationX);
					getLastNpcLocation().setY(destinationY);
					getLastNpcLocation().setZ(getZ());
					
					//if path is blocked to unreachable npc, stop walk
					if (!walkUpToPathBlockedMob && clickNpcType > 0) {
						if (finalLocalDestX > 0 && npcClickIndex > 0) {
							NPC n = NPCHandler.npcs[npcClickIndex];
							if (n != null && n.isActive) {
								getLastNpcLocation().setX(destinationX);
								getLastNpcLocation().setY(destinationY);
								getLastNpcLocation().setZ(getZ());
							}
						} else {
							clickNpcType = 0;
							npcClickIndex = 0;
						}
					}
					walkingToMob = false;
				} else // regular walking
					PathFinder.findRoute(this, destinationX, destinationY, true, 0, 0, 0, 0, 0, 0, 0, 1);
				destinationX = destinationY = 0;
				destOffsetX = destOffsetY = 1;
				walkUpToPathBlockedMob = false;
			}
//		}
		
		// if (clickObjectType > 0)
		// sendMess("Click:"+);
			if (!isLockActions()) {
				if (getClickedObject() != null && destinationReached()) {
					if (clickObjectType == 1) {
						walkingToObject = false;
						clickObjectType = 0;
						ObjectFirstClick.execute(this, getClickedObject());
					} else if (clickObjectType == 2) {
						walkingToObject = false;
						clickObjectType = 0;
						ObjectSecondClick.execute(this, getClickedObject());
					} else if (clickObjectType == 3) {
						walkingToObject = false;
						clickObjectType = 0;
						ObjectThirdClick.execute(this, getClickedObject());
					} else if (clickObjectType == 15) {
						walkingToObject = false;
						clickObjectType = 0;
						ObjectFifthClick.execute(this, getClickedObject());
					} else if (clickObjectType == 16) {
						walkingToObject = false;
						clickObjectType = 0;
						ObjectFourthClick.execute(this, getClickedObject());
					} else if (clickObjectType == 17) {
						walkingToObject = false;
						clickObjectType = 0;
						final PluginResult<MagicOnObjectExtension> pluginResult = PluginManager.search(MagicOnObjectExtension.class, getClickedObject().getId());
						pluginResult.invoke(() -> pluginResult.extension.magicOnObject(this, spellOnObject, getClickedObject()));
						spellOnObject = 0;
					}
				}

				if (clickNpcType > 0) {
					NPC npc = NPCHandler.npcs[npcClickIndex];
					if (npc != null && npc.isActive) {
//						if (followId2 <= 0 && destinationReachedAny()) {
//							followId2 = npc.getId();
//						}
						if (npc.getCurrentLocation().getX() != getLastNpcLocation().getX() || npc.getCurrentLocation().getY() != getLastNpcLocation().getY()) {
							//reset pathfinder to mob
							walkingToMob = true;
							finalLocalDestX = 0;
							finalLocalDestY = 0;
							destinationX = npc.getCurrentLocation().getX();
							destinationY = npc.getCurrentLocation().getY();
						}
						switch (clickNpcType) {
						case 1:
							if (getPA().reachedNpc(npc)) {
								faceLocation(npc.getX(), npc.getY());
								NpcFirstClick.execute(this, npcType);
								clickNpcType = 0;
							}
							break;

						case 2:
							if (getPA().reachedNpc(npc)) {
								faceLocation(npc.getX(), npc.getY());
								NpcSecondClick.execute(this, npcType);
								clickNpcType = 0;
							}
							break;
						case 3:
							if (getPA().reachedNpc(npc)) {
								faceLocation(npc.getX(), npc.getY());
								NpcThirdClick.execute(this, npcType);
								clickNpcType = 0;
							}
							break;
						case 4:
							if (getPA().reachedNpc(npc)) {
								faceLocation(npc.getX(), npc.getY());
								NpcFourthClick.execute(this, npcType);
								clickNpcType = 0;
							}
							break;
						case 5: // item on npc
							if (destinationReachedAny() || getCurrentLocation().isNextTo(npc.getXtoPlayerLoc(getX()), npc.getYtoPlayerLoc(getY()))) {
								int sizeOffset = npc.getSize()/2;
								faceLocation(npc.getX() + sizeOffset, npc.getY() + sizeOffset);
								UseItem.ItemonNpc(this, itemOnNpcItemId, npc, itemOnNpcItemSlot);
								clickNpcType = 0;
							}
							break;
						}
					} else {
						clickNpcType = 0;
						npcClickIndex = 0;
					}
				}
			}
		
		getCombat().handlePrayerDrain();
		if (underAttackBy > 0 && !isAttacked()) {
			underAttackBy = 0;
		}
		if (underAttackBy2 > 0 && !isAttacked()) { // 4300
			underAttackBy2 = 0;
			// curses().resetCurseBoosts();
			// damageTaken = new int[Config.MAX_PLAYERS];
		}
		
		if (attackingIndex > -1 && isInteractionResetTimerDone()) { // resetting left
			// click
			// attack
			attackingIndex = -1;
			setInteractionIndex(-1);
		}
		
		if (isResetCurseBoost()) {
			curses().resetCurseBoosts();
		}
		
		if (prayerRenew > 0) {
			if (prayerRenew % 17 == 0) {
				int renew = 5; //((getSkills().getStaticLevel(Skills.PRAYER) * 4) + 120) / 500;
				getSkills().incrementPrayerPoints(renew);
				startGraphic(PRAYER_RENEW_GFX);
			}

			if (prayerRenew == 500) {
				sendMessage("<col=ff>Your prayer renewal will run out in five minutes.");
			} else if (prayerRenew == 400) {
				sendMessage("<col=ff>Your prayer renewal will run out in four minutes.");
			} else if (prayerRenew == 300) {
				sendMessage("<col=ff>Your prayer renewal will run out in three minutes.");
			} else if (prayerRenew == 200) {
				sendMessage("<col=ff>Your prayer renewal will run out in two minutes.");
			} else if (prayerRenew == 100) {
				sendMessage("<col=ff>Your prayer renewal will run out in one minute.");
			} else if (prayerRenew == 1) {
				sendMessage("<col=ff>Your prayer renewal has ended.");
			}
			prayerRenew--;
		}
		
		if (wildLevel > 0) {
			getItems().wildyProhibitedItem(0);
		}

		if (safeTimer > 0) {
			safeTimer--;
			if (Config.isPvpWorld()) {
				getPA().walkableInterface(197);
				getPacketSender().showOption(3, 0, "Attack");
				getPacketSender().sendFrame126("<col=ffb000>" + safeTimer, 199);
			}
		}
		
		if (!Config.isPvpWorld() && teleportToX == -1 && teleportToY == -1) {

			final boolean canAttack = attr().has("canAttack");

			getPacketSender().showOption(6, 0, "Null");
			if (insideDungeoneering()) {
				if (dungTeleported) {
					if (absX < 800 || absY < 800 || absX > 1200 || absY > 1200) {
						Party.leaveDungeon(this, false);
					}
				}
			} else if (inDaemonheim()) {
				getPacketSender().showOption(6, 0, "Invite");
			} else {
				if (dungParty != null) {
					Party.leaveParty(this);
					Party.updateSidebar(this, false);
				}
			}

			if (inWild() && !revenantDungeon() && !getZones().isInClwRedPortalPkZone() && !Config.isPvpWorld()/* && !safeZone() */) { // TODO:REMOVE
				// noClip
				setWildernessTick(getWildernessTick() + 1);
				
				int modY = absY > 6400 ? absY - 6400 : absY;
				
				if (wildLevel == 0) {
					// sendMessage("wildy is 0");
					// int modY = absY > 6400 ? absY - 6400 : absY;
					
					wildLevel = Math.max(5, ((modY - 3520) / 8));
					
					if (hasOverloadBoost) {
						getPotions().resetOverload();// hasOverloadBoost =
						// false;
					}
					boolean lowerSkills = false;
					if (getPlayerLevel()[0] > RSConstants.MAX_WILD_ATT) {
						getPlayerLevel()[0] = RSConstants.MAX_WILD_ATT;
						refreshSkill(0);
						lowerSkills = true;
					}
					if (getPlayerLevel()[1] > RSConstants.MAX_WILD_DEF) {
						getPlayerLevel()[1] = RSConstants.MAX_WILD_DEF;
						refreshSkill(1);
						lowerSkills = true;
					}
					if (getPlayerLevel()[2] > RSConstants.MAX_WILD_STR) {
						getPlayerLevel()[2] = RSConstants.MAX_WILD_STR;
						refreshSkill(2);
						lowerSkills = true;
					}
					if (getPlayerLevel()[4] > RSConstants.MAX_WILD_RNG) {
						getPlayerLevel()[4] = RSConstants.MAX_WILD_RNG;
						refreshSkill(4);
						lowerSkills = true;
					}
					if (getPlayerLevel()[6] > RSConstants.MAX_WILD_MAGE) {
						getPlayerLevel()[6] = RSConstants.MAX_WILD_MAGE;
						refreshSkill(6);
						lowerSkills = true;
					}
					if (lowerSkills) {
						sendMessage(
								"You combat skills were too high for wildy, they have been lowered to 118.");
					}
					
					if (summoned != null && summoned.transformId != -1 && !summoned.isPetNpc()) {
						summoned.transform(summoned.npcType);
					}
					
					if (getTransformId() != -1 && !Config.isOwner(this)) {
						transform(-1);
						movement.resetWalkingQueue();
						getPA().resetMovementAnimation();
					}
					
					calcCombat();
					
					if (!isDev())
						runTilesPerTick = 1;
				}
				
				// safeTimer = 10;
				
				
//				if (getZones().containsZoneInList(WildGodWarsDungeonZone.ZONE_SAMPLE)) {
//					wildLevel = (modY - 3520) / 8;
//				} else {
					wildLevel = Math.max(5, ((modY - 3520) / 8));
//				}
				
				getPA().walkableInterface(197);
				if (Config.SINGLE_AND_MULTI_ZONES) {
					if (inMulti()) {
						getPacketSender().sendFrame126("<col=ffff00>Level: " + wildLevel, 199);
					} else {
						getPacketSender().sendFrame126("<col=ffff00>Level: " + wildLevel, 199);
					}
				} else {
					getPacketSender().multiWay(-1);
					getPacketSender().sendFrame126("<col=ffff00>Level: " + wildLevel, 199);
				}
				getPacketSender().showOption(3, 0, "Attack");
			} else if (inFunPk() && !FunPkTournament.isTournamentOpen()) {
				if (WorldType.equalsType(WorldType.ECONOMY)) {
					getPA().walkableInterface(197);
					getPacketSender().sendFrame126("<col=ff00>FunPK", 199);
					getPacketSender().showOption(3, 0, "Attack");
				}
			} else if (raidsManager != null) {
				getPA().walkableInterface(67220);

				int ticks = 0;
				if (raidsManager.startTicks != 0) {
					ticks = Server.getTotalTicks() - raidsManager.startTicks;
				}

				getPacketSender().updateRaidsHud(raidsManager.points.getTotalPoints(), raidsManager.points.getPoints(mySQLIndex), ticks);
			} else if (getZones().isInDmmLobby() || getZones().isInClwRedPortalPkZone() || getZones().isInNightmare()) {
				/* empty */
			} else if (getZones().isInDmmTourn()) {
				getPA().walkableInterface(197);
			} else if (inClanWarsFunPk()) {
				getPacketSender().showOption(3, 0, "Attack");
			} else if (inFunPk() && FunPkTournament.isTournamentOpen() && !FunPkTournament.allowFight()) {
				getPA().walkableInterface(6673);
			} else if (inFunPk() && FunPkTournament.isTournamentOpen()) {
				getPA().walkableInterface(197);
				// getPacketSender().sendFrame126("<col=ff00>FunPK", 199);
				getPacketSender().showOption(3, 0, "Attack");
			} else if (isInBarrows() || isInBarrows2()) {
				getPA().walkableInterface(16128);
				getPacketSender().sendFrame126("" + barrows[6], 16137);
				if (barrows[3] == 2) {
					getPacketSender().sendFrame126("<col=86B404>Karils", 16135);
				}
				if (barrows[2] == 2) {
					getPacketSender().sendFrame126("<col=86B404>Guthans", 16134);
				}
				if (barrows[4] == 2) {
					getPacketSender().sendFrame126("<col=86B404>Torags", 16133);
				}
				if (barrows[0] == 2) {
					getPacketSender().sendFrame126("<col=86B404>Ahrims", 16132);
				}
				if (barrows[5] == 2) {
					getPacketSender().sendFrame126("<col=86B404>Veracs", 16131);// 0x86B404
				}
				if (barrows[1] == 2) {
					getPacketSender().sendFrame126("<col=86B404>Dharoks", 16130);
				}
			} else if (canAttack) {
				getPacketSender().showOption(3, 0, "Attack");
			} else if (safeZone()) {
				if (safeTimer > 0) {
					getPA().walkableInterface(197);
					// getPA().walkableInterface(47500);
					wildLevel = 22;
					getPacketSender().showOption(3, 0, "Attack");
					getPacketSender().sendFrame126("<col=ffb000>" + safeTimer, 199);
				} else {
					getPA().walkableInterface(197);
					// getPA().walkableInterface(47500);
					displayFriendlyFire();
					if (Config.SINGLE_AND_MULTI_ZONES) {
						if (inMulti()) {
							getPacketSender().sendFrame126("<col=ff00>SafeZone", 199);
						} else {
							getPacketSender().sendFrame126("<col=ff00>SafeZone", 199);
						}
					} else {
						getPacketSender().multiWay(-1);
						getPacketSender().sendFrame126("<col=ff00>SafeZone", 199);
					}
				}
			} else if (inPcBoat()) {
				getPA().walkableInterface(21119);
			} else if (inPcGame()) {
				getPA().walkableInterface(21100);
			} else if (gwdCoords()) {
				// System.out.println("Poop2");
				/*
				 * getPacketSender().sendFrame126("" + getTempVar().getInt("ArmaKC") + "",
				 * 16216); getPacketSender().sendFrame126("" + getTempVar().getInt("BandosKC") +
				 * "", 16217); getPacketSender().sendFrame126("" + getTempVar().getInt("SaraKC")
				 * + "", 16218); getPacketSender().sendFrame126("" +
				 * getTempVar().getInt("ZammyKC") + "", 16219);
				 */
				getPA().walkableInterface(16210);
				// } else if (!gwdCoords()) {
				// getPA().walkableInterface(-1);
			} else if (inDuelArena()) {
				getPA().walkableInterface(201);
				if (duelStatus == 5) {
					getPacketSender().showOption(3, 0, "Attack");
				} else {
					getPacketSender().showOption(3, 0, "Challenge");
				}
				
			} else if (inClanWarsLobby()) {
				getPA().walkableInterface(201);
				getPacketSender().showOption(3, 0, "Challenge");
			} else if (inClanWars()) {
				// if (getClan() != null && getClan().getClanWarId() != -1) {
				getPacketSender().showOption(3, 1, "Attack");
				// getPA().walkableInterface(3500); // walkable interface
				// changed in clanwars code
				// }
			} else if (inBarrows()) {
				// getPA().setMinimapState(2);
				getPacketSender().sendFrame126("Kill Count: " + barrows[6], 4536);
				getPA().walkableInterface(4535);
				// } else if (CastleWars.isInCw(this)) {
				// //wildLevel = 20;
				// getPA().showOption(3, 0, "Attack", 1);
			} else if (RSConstants.fightPitsRoom(getX(), getY()) && FightPitsTournament.allowFight()) {
				getPacketSender().showOption(3, 0, "Attack");
				getPA().walkableInterface(6673);
			} else if (RSConstants.fightPitsRoom(getX(), getY()) && !FightPitsTournament.allowFight()) {
				getPA().walkableInterface(6673);
			} else if (RSConstants.fightPitsWait(getX(), getY())) {
				displayFriendlyFire();
				getPA().walkableInterface(6673);
				// sendMessage("wait:"+FightPitsTournament.waitTime+"
				// game:"+FightPitsTournament.gameTime);
				getPacketSender().sendFrame126("Fight Pits Wait Room", 6570);
				getPacketSender().sendFrame126("Players Inside: " + FightPitsTournament.getPlayerCount(), 6572);
				if (FightPitsTournament.waitTime > 0) {
					getPacketSender().sendFrame126("<col=ff00>" + (FightPitsTournament.waitTime * 600 / 1000) + " seconds",
							6664);
				} else {
					getPacketSender().sendFrame126("<col=ff00>" + ((FightPitsTournament.gameTime * 600 / 1000)
							+ (FightPitsTournament.WAIT_TIME * 600 / 1000)) + " seconds", 6664);
				}
				// } else if (getPA().inPitsWait()) {
				// getPA().showOption(3, 0, "Null", 1);
				// /*} else if (noClip) {
				// int x = (absX%32);
				// int y = (absY%32);
				//
				// getPacketSender().sendFrame126(" ", 6570);
				// getPacketSender().sendFrame126("X "+x, 6572);
				// getPacketSender().sendFrame126("Y "+y, 6664);
				// getPA().walkableInterface(6673);
				//
				// //sendMessage("Poop");
				// */
			} else if (getBarbarianAssault().inArena()) {
				getBarbarianAssault().displayArenaInterface();
			} else if (getBarbarianAssault().inLobby()) {
				getBarbarianAssault().displayLobbyInterface();
			} else if (inSoulWait()) {
				Overlay.sendWaitingScreen(this);
			} else if (inSoulWars()) {
				getPacketSender().showOption(3, 0, "Attack");
				Overlay.soulWarsInterface(this);
			} else if (inCw()) {
				CastleWars.updateCastleWarsScreen(this);
				getPacketSender().showOption(3, 0, "Attack");
				getPA().walkableInterface(11146);
			} else if (inCwWait()) {
				CastleWars.sendWaitingScreen(this);
				getPA().walkableInterface(6673);
			} else if (inMarket()) {
				getPacketSender().showOption(3, 0, "View shop");
				getPA().walkableInterface(-1);
			} else if (earnedTaiBwoFavour && inTaiBwoWannai()) {
				getPA().walkableInterface(14210);
			} else if (inLMSLobby()) {
				//getPA().walkableInterface(66110);
				LmsManager.updateLobbyInterface(LmsManager.serverWideMode, this);
			} else if (inLMSArenaCoords()) {
				getPacketSender().showOption(3, 0, "Attack");
				getPA().walkableInterface(66110);
				//LmsManager.updateArenaInterface(this);
			} else if (infernoManager != null) {
				infernoManager.updateHud();
			} else if (revenantDungeon()) {
				// int modY = absY > 6400 ? absY - 6400 : absY;
				wildLevel = (((absY - 10051) / 8) + 16);
				getPA().walkableInterface(193);// 193 is actually for coordinates y 3250 and above 100 levels
				getPacketSender().showOption(3, 0, "Attack");
				if (Config.SINGLE_AND_MULTI_ZONES) {
						getPacketSender().sendFrame126("<col=ffff00>Level: " + wildLevel, 195);
				} else {
					getPacketSender().multiWay(-1);
					getPacketSender().sendFrame126("<col=ffff00>Level: " + wildLevel, 195);
				}
				getPacketSender().showOption(3, 0, "Attack");
			} else if (wildLevel < 1 && safeTimer < 1 && bankPinWarning > 0 && !hasBankPin) {
				getPA().walkableInterface(6673);
				getPacketSender().sendFrame126("<col=ff0000>WARNING!!!!", 6570);
				getPacketSender().sendFrame126("<col=ff0000>NO BANKPIN SET!", 6572);
				getPacketSender().sendFrame126("You're at risk!", 6664);
			} else if (isInJail() && (getJailTime() > 0)) {
				int seconds = Misc.ticksToSeconds(getJailTime());
				getPA().walkableInterface(6673);
				getPacketSender().sendFrame126("", 6570);
				getPacketSender()
						.sendFrame126(
								"Jail time: " + String.format("%d:%02d ", TimeUnit.SECONDS.toMinutes(seconds),
										TimeUnit.SECONDS.toSeconds(seconds)
												- TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds))),
								6572);
				getPacketSender().sendFrame126("", 6664);
			} else if (wildLevel < 1 && safeTimer < 1 && (getDoubleExpLength() > 0 || getPetDoubleExpLength() > 0 || getDropRateBoostLength() > 0)) {
				getPA().walkableInterface(6673);
				if (getDropRateBoostLength() > 0) {
					getPacketSender().sendFrame126("20% Drops: " + Misc.ticksToTime(getDropRateBoostLength(), false), 6570);
				} else {
					getPacketSender().sendFrame126("", 6570);
				}

				if (getDoubleExpLength() > 0) {
					getPacketSender().sendFrame126("Double XP: " + Misc.ticksToTime(getDoubleExpLength(), false), 6572);
				} else {
					getPacketSender().sendFrame126("", 6572);
				}

				if (getPetDoubleExpLength() > 0) {
					getPacketSender().sendFrame126("Pet 2x XP: " + Misc.ticksToTime(getPetDoubleExpLength(), false), 6664);
				} else {
					getPacketSender().sendFrame126("", 6664);
				}
			} else {
				if (safeTimer > 0) {
					getPA().walkableInterface(197);
					// getPA().walkableInterface(47500);
					wildLevel = 5;
					getPacketSender().showOption(3, 0, "Attack");
					getPacketSender().sendFrame126("<col=ffb000>" + (safeTimer / 2), 199);
				} else {
					// getPA().setMinimapState(0);
					getPA().walkableInterface(-1);
					displayFriendlyFire();
					if (wildLevel != 0) {
						if (huntedBy.size() > 0) {
							for (int i = 0; i < huntedBy.size(); i++) {
								if (PlayerHandler.players[huntedBy.get(i)] == null) {
									huntedBy.remove(i);
									continue;
								}
								final Client o = (Client) PlayerHandler.players[huntedBy.get(i)];
								if (o != null && o.isActive) {
									o.getPA().resetHuntId();
								}
							}
							huntedBy.clear();
						}
						if (huntPlayerIndex > 0) {
							getPA().resetHuntId();
						}
						if (getWildernessTick() > 0) {
							setWildernessTick(0);
						}

						if (summoned != null && summoned.npcType != 6808 && !summoned.isPetNpc()) {
							summoned.transform(summoned.npcType - 1);
						}
						wildLevel = 0;
						// if (lastY == 3521) {
						calcCombat();
						// }
					}
				}
			}
		}
		
		if (wildLevel != 0 && safeTimer == 0) {
			if (!inWild()) {
				// if (huntedBy.size() > 0) {
				// for (int i = 0; i < huntedBy.size(); i++) {
				// if (PlayerHandler.players[huntedBy.get(i)] == null) continue;
				// final Client o =
				// (Client)PlayerHandler.players[huntedBy.get(i)];
				// o.getPA().resetHuntId();
				// }
				// huntedBy.clear();
				// }
				getPA().resetMyBountyHunters();
				if (huntPlayerIndex > 0) {
					getPA().resetHuntId();
				}
				if (getWildernessTick() > 0) {
					setWildernessTick(0);
				}
				
				if (summoned != null && summoned.npcType != 6808 && !summoned.isPetNpc()) {
					summoned.transform(summoned.npcType - 1);
				}
				
				wildLevel = 0;
				calcCombat();
			}
		}
		
		if (!hasMultiSign && inMulti()) {
			hasMultiSign = true;
			getPacketSender().multiWay(1);
		}
		
		if (hasMultiSign && !inMulti()) {
			hasMultiSign = false;
			getPacketSender().multiWay(-1);
		}
		
		if (getSkullTimer() > 0) {
			setSkullTimer(getSkullTimer() - 1);
			if (getSkullTimer() == 1) {
				attackedPlayers.clear();
				setSkullTimer(-1);
				setSkull(false);
			}
		}
		
		if (getFreezeTimer() > RSConstants.FREEZE_COOLDOWN) { //this shit makes no sense
			setFreezeTimer(getFreezeTimer() - 1);
			if (frozenBy > 0) {
				Player freezeOwner = PlayerHandler.players[frozenBy];
				if (freezeOwner == null || freezeOwner.properLogout || !getCurrentLocation().isWithinDeltaDistance(freezeOwner.getCurrentLocation(), 10)) {
					frozenBy = -1;
					resetFreeze();
				}
			}
		}

		if (teleTimer > 0) {
			teleTimer--;
			if (!isDead()) {
				if (teleTimer == endDelay) {
					getPA().processTeleport();
				}
			} else {
				teleTimer = 0;
			}
		}

		// if (isMoving() && underAttackBy > 0) { // if attacker is not moving
		// towards runner then set him to move
		// if (PlayerHandler.players[underAttackBy] != null &&
		// PlayerHandler.players[underAttackBy].playerIndex == getId() &&
		// !PlayerHandler.players[underAttackBy].isMoving()) {
		// PlayerHandler.players[underAttackBy].followId = getId();
		// PlayerHandler.players[underAttackBy].getPA().checkIfCanHit();
		// PlayerHandler.players[underAttackBy].getPA().getRangeDistance();
		// PlayerHandler.players[underAttackBy].getPA().followPlayer();
		// if (PlayerHandler.players[underAttackBy].withinRangeToHit)
		// PlayerHandler.players[underAttackBy].resetWalkingQueue();
		// PlayerHandler.players[underAttackBy].withinDistanceToStopWalking =
		// false;
		// PlayerHandler.players[underAttackBy].blockedShot = false;
		// }
		// }
		
		// sendMessage("Attack timer "+attackTimer);
		
		if (playerIndex > 0) {
			getPA().checkIfCanHit();
			getPA().getRangeDistance();
		}
		if (npcIndex > 0) {
			getPA().checkIfCanHitNpc();
			getPA().getRangeDistanceToNpc();
		}
		
		/**
		 * Walking processing done here.
		 */
		// if (walkToX > 0 || walkToY > 0) {
		// PathFinder.getPathFinder().findRoute(this, walkToX, walkToY, true, 1,
		// 1);
		// }
		
//		if (getAttackTimer() == 0 && withinRangeToHit && !blockedShot) { //wtf is this???
//			movement.resetWalkingQueue();
//		}
		
		if (followId > 0) {
			getPA().followPlayer();
		} else if (followId2 > 0) {
			getPA().followNpc();
		}
		
		if (getAttackTimer() > 0) {
			setAttackTimer(getAttackTimer() - 1);
		}
		
		if (blockedShot) {
			blockedShot = false;
		}
		if (withinRangeToHit) {
			withinRangeToHit = false;
		}
		if (withinDistanceToStopWalking) {
			withinDistanceToStopWalking = false;
		}
		
		if (respawnTimer == 7) {
			respawnTimer = -6;
			// getPA().giveLife();
		} else if (respawnTimer == 12) {
			// startAnimation(0x900);
			respawnTimer--;
		}
		
		if (respawnTimer > -6) {
			respawnTimer--;
		}
		
		if (summoned != null) {
			getSummoning().familiarTick();
		}

		if (getZones().isInKaruulmSlayerDungeon() && teleTimer == 0) {
			int bootsId = playerEquipment[PlayerConstants.playerFeet];
			if (bootsId != 123037 && bootsId != 122951 && bootsId != 121643) {
				dealDamage(new Hit(4, 0, -1));
			}
		}

		if (!isBot()) {
			this.herbs.update();
			this.trees.update();
		}

		if (!isDead()) {
			getSkills().processRestore();
		}

		int regenAmt = getSeasonalData().getPowerUpAmount(PowerUpData.HEALTH_REGEN);
		if (regenAmt > 0 && Server.getTotalTicks() > getSeasonalData().regenPowerTick) {
			int maxHp = getSkills().getMaximumLifepoints();
			int hp = getSkills().getLifepoints();
			if (hp < maxHp) {
				int restoredHp = getSkills().getMaximumLifepoints() * regenAmt / 100;
				getSkills().heal(restoredHp);
			}

			getSeasonalData().tickRegen();
		}

		if (clone != null && (Server.getTotalTicks() >= clone.getServerTicks() || !getCurrentLocation().isWithinDeltaDistance(clone.getLocation(), 5))) {
			setClone(null);
		}
	}

	private void displayFriendlyFire() {
		switch (playerEquipment[PlayerConstants.playerWeapon]) {
		case 4566:
		case 14728:
			getPacketSender().showOption(3, 0, "Whack");
			break;

		case 10501:
			getPacketSender().showOption(3, 0, "Pelt");
			break;
			
		case 24145:
			getPacketSender().showOption(3, 0, "Shoot");
			break;

		default:
			getPacketSender().showOption(3, 0, "Null");
			break;
		}
	}

	private void processTimers() {
		
		if (Config.isPvpWorld()) {
			if (inPvpWorldSafe()) {
				if (onlineTick >= 1500 && onlineTick % 1500 == 0) { // 15 minutes
					decreaseEP();
				}
			} else {
				if (inHotZone) {
					if (onlineTick >= 1000 && onlineTick % 1000 == 0) { // 10 minutes
						increaseEP();
					}
				} else {
					if (onlineTick >= 1500 && onlineTick % 1500 == 0) { // 15 minutes
						increaseEP();
					}
				}
			}
		}
		
		getGodSpell().runTimer();
		
		if (getTeleBlockTimer() > 0) {
			if (wildLevel > 0)
				setTeleBlockTimer(getTeleBlockTimer() - 1);
			else
				setTeleBlockTimer(0);
		}
		
		if (getTimedMute() > 0) {
			setTimedMute(getTimedMute() - 1);
		}

		if (getDropRateBoostLength() > 0) {
			setDropRateBoostLength(getDropRateBoostLength() - 1);
		}

		if (getPkpBoostLength() > 0) {
			setPkpBoostLength(getPkpBoostLength() - 1);
		}

		if (getImbuedHeartTimer() > 0) {
			setImbuedHeartTimer(getImbuedHeartTimer() - 1);
			if (getImbuedHeartTimer() == 0) {
				sendMessage("<col=ef1020>Your imbued heart has regained its magical power.</col>");
			}
		}

		if (getMinigameBanTimer() > 0) {
			setMinigameBanTimer(getMinigameBanTimer() - 1);
		}

		if (dungBloodNeckTicks > 0) {
			dungBloodNeckTicks--;
		}

		if (dungBloodNeckTicks == 0) {
			dungBloodNeckTicks = 25;
			if ((playerEquipment[PlayerConstants.playerAmulet] == 15834 || playerEquipment[PlayerConstants.playerAmulet] == 17291) && insideDungeoneering()) {
				for (NPC npc : Server.getRegionManager().getLocalNpcsNext(this)) {
					if (!AttackNPC.attackPrereq(this, npc, false) || (npc.getEntityDef() != null && npc.getEntityDef().getCombatLevel() <= 0)) {
						continue;
					}

					int roll = Misc.random(2, 5);
					npc.dealDamage(new Hit(roll, 0, -1));
					getSkills().heal(roll);
				}
			}
		}
	}
	
	public void unjailPlayer() {
		setJailTime(0);
		getPA().movePlayer(LocationConstants.EDGEVILLE_X, LocationConstants.EDGEVILLE_Y, 0);
		sendMessage("You've been unjailed!");
		confirmUnjail = false;
	}
	
	private static final Object ADD_PACKET_MUTEX = new Object();
	
	@Override
	public boolean processQueuedPackets() {
		alertedSpam = false;
		synchronized (ADD_PACKET_MUTEX) {
			// Packet packet;
			// while ((packet = newPackets.poll()) != null) {
			// queuedPackets.add(packet);
			// }
			queuedPackets.addAll(newPackets);
			newPackets.clear();
		}
		
		if (Config.WATCH_SPAMMERS && queuedPackets.size() == QUEUED_PACKETS_SIZE && !alertedSpam) {
			alertedSpam = true;
			InPacket packet = queuedPackets.peekLast();
			System.out.println("packet spammer:"+getName()+" last packet:"+packet.getOpcode());
			System.out.println("their packets:");
			InPacket packet2 = null;
			while ((packet2 = newPackets.peek()) != null) {
				System.out.print(String.format(" %d,", packet2.getOpcode()));
			}
		}
		
		// cheaphax to processing food on main tick
//		if (!foodToEatOnMainTick.isEmpty()) {
//			Item item = null;
//			while ((item = foodToEatOnMainTick.poll()) != null) {
//				if (playerItems[item.getSlot()] - 1 != item.getId())
//					continue;
//				if (item.getId() == 3144) { //karams
//					Food.eatKaramb(this, item.getId(), item.getSlot());
//				} else {
//					FoodToEat food = Food.forId(item.getId());
//					if (food != null) {
//						Food.eat(this, food, item.getId(), item.getSlot());
//						continue;
//					}
//					Potion pot = Potion.forId(item.getId());
//					if (pot != null)
//						pot.drink(this, item);
//				}
//			}
//		}
		
		// System.out.println("DElay:"+(System.nanoTime()-delay));
		// synchronized (queuedPackets) {
		InPacket p = null;
		while ((p = queuedPackets.poll()) != null) {
			inStream.currentOffset = 0;
			packetType = p.getOpcode();
			packetSize = p.getPayload().length;
			inStream.buffer = p.getPayload();
			if (packetType >= 0) {
				// if ((packetType == 164 || packetType == 248) &&
				// lastWalkingPacket != null) {
				// if (p.equals(lastWalkingPacket)) {
				// PacketHandler.processPacket(this, packetType, packetSize);
				// lastWalkingPacket = null;
				// } else {
				// inStream.reset();
				// }
				// } else if (lastEntityInteractPacket != null &&
				// PlayerConstants.SINGLE_TASK_ENTITY_INTERACT_PACKETS.contains(packetType))
				// {
				// if (p.equals(lastEntityInteractPacket)) {
				// PacketHandler.processPacket(this, packetType, packetSize);
				// lastEntityInteractPacket = null;
				// } else {
				// inStream.reset();
				// }
				// } else {
				PacketHandler.processPacket(this, packetType, packetSize);
				// }
			}
		}
		
		// new method to prevent useless processing in equip item packet BAD:
		// causes order issues like resetting player attack
		// if (isWearingItemOnTick()) {
		// equipItems();
		// setWearingItemOnTick(false);
		// }
		
		if (isUpdateInvInterface() || isUpdateEquipmentRequired()) {
			getItems().refreshWeightAndTotalBonus();
		}
		
		if (isUpdateInvInterface()) {
			getItems().resetItems(getUpdateInvInterface());
			setUpdateInvInterface(0);
		}
		
		if (isUpdateEquipmentRequired()) {
			setUpdateEquipmentRequired(false);
			getItems().refreshBonuses();
			updateFlags.add(UpdateFlag.APPEARANCE);
			for (int i = 0, length = getUpdateEquipment().length; i < length; i++) {
				if (getUpdateEquipment()[i]) {
					setUpdateEquipment(false, i);
					getItems().updateSlot(i);
				}
			}
		}
		
		// }
		return true;
	}
	
	/**
	 * End of Skill Constructors
	 */
	private boolean alertedSpam = false;
	public void queueMessage(InPacket packet) {
		
		// if (correctlyInitialized) {
		// if (packet.getOpcode() == 122 || packet.getOpcode() == 248 ||
		// packet.getOpcode() == 164 || packet.getOpcode() == 68) { // arravs
		// instant processing for walking, testing it out
		//
		// inStream.currentOffset = 0;
		// packetType = packet.getOpcode();
		// packetSize = packet.getLength();
		// inStream.buffer = packet.getPayload().array();
		//
		// PacketHandler.processPacket(this, packetType, packetSize);
		// //PacketHandler.processPacket(this, packetType, packetSize);
		// return;
		// }
		// }
		
		// if (Connection.spammingConnections.contains(connectedFrom)) {
		// return;
		// }
		
		// if (Config.INSTANT_SWITCHES && packet.getOpcode() == 41) {
		//
		// timeOutCounter = 0;
		// final int wearId = packet.readUnsignedWord();
		// final int wearSlot = packet.readUnsignedWordA();
		// interfaceId = packet.readUnsignedWordA();
		// getItems().wearItem(wearId, wearSlot);
		// return;
		// }
		
		synchronized (ADD_PACKET_MUTEX) {
			
			// search for last walking packet
			// if (packet.getOpcode() == 164 || packet.getOpcode() == 248) {
			// lastWalkingPacket = packet;
			// }
			//
			// if
			// (PlayerConstants.SINGLE_TASK_ENTITY_INTERACT_PACKETS.contains(packet.getOpcode()))
			// {
			// lastEntityInteractPacket = packet;
			// }
			
//			if (newPackets.size() < 100) {
				newPackets.add(packet);
//			} else {
//				if (Config.WATCH_SPAMMERS && !alertedSpam) {
//					alertedSpam = true;
//					System.out.println("packet spammer:"+getName()+" packet:"+packet.getOpcode());
//					System.out.println("their packets:");
//					InPacket packet2 = null;
//					while ((packet2 = newPackets.peek()) != null) {
//						System.out.print(String.format(" %d,", packet2.getOpcode()));
//					}
//				}
//			}
			
			// //ignore the rest of shit cod underneath here
			//
			//// queuedPackets.add(packet);
			// if (timePlayed < 200 && queuedPackets.size() > 40) {
			// System.out.println("hacker "+getName()+" mac:"+UUID+"
			// IP:"+connectedFrom);
			// Server.getSlackApi().call(SlackMessageBuilder.buildSpammerMessage(getName()+"
			// mac:"+UUID+" IP:"+connectedFrom));
			// if (!Connection.spammingConnections.contains(connectedFrom)) {
			// Connection.spammingConnections.add(connectedFrom);
			// disconnected = true;
			// queuedPackets.clear();
			// }
			// }
			//
			// if (queuedPackets.size() > 200) {
			//// disconnected = true;
			// queuedPackets.clear();
			//// System.out.println("hacker2 "+getName());
			//// Server.getSlackApi().call(SlackMessageBuilder.buildSpammerMessage(getName()));
			//// if (!Connection.spammingConnections.contains(connectedFrom) &&
			// timePlayed < 50000) {
			//// Connection.spammingConnections.add(connectedFrom);
			//// disconnected = true;
			//// queuedPackets.clear();
			//// } else {
			//// if
			// (Connection.spammingConnectionsHacker2Thing.contains(getName()))
			// {
			//// System.out.println("Banned hacker2 "+getName()+"
			// IP:"+connectedFrom);
			//// Connection.spammingConnections.add(connectedFrom);
			//// disconnected = true;
			//// queuedPackets.clear();
			//// }
			//// Connection.spammingConnectionsHacker2Thing.add(getName());
			//// }
			//
			//
			//// Connection.addNameToBanList(getName());
			//// Connection.addNameToFileUsersBanned(getName());
			//// if (!Connection.isUidBanned(UUID))
			//// Connection.addUidToBanList(UUID);
			//// Connection.addIpToBanList(connectedFrom);
			//// Connection.addIpToFile(connectedFrom);
			//// disconnected = true;
			// }
		}
	}
	
	public int randomlottery() {
		return RandomBoxLottery.RANDOM_LOTTERY[(int) (Math.random() * RandomBoxLottery.RANDOM_LOTTERY.length)];
	}
	
	public void reconnectClient(Channel s, ISAACCipher outCipher, String ipAddress) {
		if (outStream != null) {
			outStream.reset();
		}
		if (inStream != null) {
			inStream.reset();
		}
		if (this.session != null && this.session.isActive()) {
			// System.out.println("closing current channel");
			this.session.close();
		}
		this.session = s;
		reconnectAttempts++;
		outStream = new Stream(new byte[Config.BUFFER_SIZE]);
		outStream.currentOffset = 0;
		inStream = new Stream(new byte[Config.BUFFER_SIZE]);
		inStream.currentOffset = 0;
		
		outStream.packetEncryption = outCipher;
		connectedFrom = ipAddress;
		outStream.reset();
		inStream.reset();
		
		// disconnected = false;
		mapRegionDidChange = true;
		// resetWalkingQueue();
	}

	@Override
	public void refreshSkill(int id) {
		getPacketSender().setSkillLevel(id, getSkills().getLevel(id), getSkills().getExperience(id));
	}

	public void removeItemFromPlayer(int itemId) {
		if (getItems().playerHasItem(itemId)) {
			getItems().deleteItemInOneSlot(itemId, getItems().getItemAmount(itemId));
		}
		// if(getItems().playerHasEquipped(itemId)) {
		// getItems().deleteEquipmentItem(itemId,
		// getItems().getEquipItemSlot(itemId));
		// getItems().removeItem(itemId, getItems().getItemSlot(itemId));
		// getItems().deleteItem(itemId, getItems().getItemAmount(itemId));
		// }
		getItems().deleteEquipmentItem(itemId);
		// if(getItems().playerHasItem(itemId)) {
		// getItems().deleteItem(itemId, getItems().getItemAmount(itemId));
		// }
		
		if (getItems().itemInBank(itemId)) {
			getItems().removeBankItem(itemId);
		}
	}
	
	public void sendEnterName() {
		if (getOutStream() != null && isActive) {
			getOutStream().createPacket(187);
			flushOutStream();
		}
	}
	
	public void sendMess(String s) {
		if (Config.NODE_ID == 1 || Config.NODE_ID == 2) {
			return;
		}
		sendMessage(s);
	}
	
	@Override
	public void sendMessage(String msg) {
		packetSender.sendMessage(ChatMessageTypes.GAME_MESSAGE, msg);
	}

	public void sendMessageSpam(String msg) {
		packetSender.sendMessage(ChatMessageTypes.GAME_MESSAGE_SPAM, msg);
	}

	public void sendPinForNewUUID() {
		if (isRequiredPinVerification()) {
			dialogueAction = 9998;
			inVerificationState = true;
			correctlyInitialized = false;
			hidePlayer = true;
			getPacketSender().showInterface(12283);
			getPacketSender().setBlackout(2);
			packetSender.sendFrame126b("New Machine Detected. Please enter your bank pin.", 12285);
			getPacketSender().sendInputDialogState(7); // new bank pin chat box thing
		} else {
			hidePlayer = false;
		}
		skipThreadCheck = false;
	}
	
	public void sendRegionChanged() {
		/*if (true) {
			getPacketSender().sendConstructMapRegionPacket(0);
			return;
		}*/

		if (infernoManager != null) {
			getPacketSender().sendConstructMapRegionPacket(infernoManager.height);
			return;
		}

		if (raidsManager != null) {
			getPacketSender().sendConstructMapRegionPacket(raidsManager.height);
			return;
		}

		if (insideDungeoneering()) {
			getPacketSender().sendConstructMapRegionPacket(dungParty.dungManager.height);
			return;
		}

		if (getOutStream() != null && isActive) {
			getOutStream().createPacket(73);
			getOutStream().writeShortA(mapRegionX + 6);
			getOutStream().writeShort(mapRegionY + 6);
			flushOutStream();
		}
	}
	
	public void sendTourGuide() {
		if (WorldType.equalsType(WorldType.SPAWN)) {
			getDH().sendNpcChat2("Would you like to do the Starter Tour", "and earn <col=ff0000>100</col> Blood money?", 949,
					"Starter Guide");
		} else {
			getDH().sendNpcChat2("Would you like to do the Starter Tour", "and earn <col=ff0000>100K</col> Gold?", 949,
					"Starter Guide");
		}
		dialogueAction = 123;
	}
	
	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}
	
	// public int getHitpoints() {
	// return playerLevel[3];
	// }
	
	public void setCanWalk(boolean canWalk) {
		this.canWalk = canWalk;
	}

	public void setDefaultSidebars() {
		getPacketSender().setSidebarInterface(1, /* 3917 */31110); // skill tab
		getPacketSender().setSidebarInterface(2, 638);
		getPacketSender().setSidebarInterface(3, 3213);
		getPacketSender().setSidebarInterface(4, 1644);
		updatePrayerBook();
		updateSpellbook();
		correctCoordinates();
		getPacketSender().setSidebarInterface(7, 18128);
		getPacketSender().setSidebarInterface(8, 5065);
		getPacketSender().setSidebarInterface(9, 67050);//5715
		getPacketSender().setSidebarInterface(10, 2449);
		getPacketSender().setSidebarInterface(11, 64000); // wrench tab
		getPacketSender().setSidebarInterface(12, 147); // run tab
		getPacketSender().setSidebarInterface(13, 15891); // music tab 6299 /spawn
		getPacketSender().setSidebarInterface(14, 29265);// achievement tab
		if (WorldType.equalsType(WorldType.SPAWN)) {
			getPacketSender().setSidebarInterface(15, 15891); // spawn tab
		} else {
			getPacketSender().setSidebarInterface(15, 15271); // edit chat crowns
		}
		getPacketSender().setSidebarInterface(16, 17011); // summoning tab
		getPacketSender().setSidebarInterface(0, 2423);
	}
	
	public void setFishingCooldown(long l) {
		fishingCooldown = l;
	}
	
	public void setInteractionIndex(int defenderIndex) { // is member packet XD
		if (isBot())
			return;
		if (getOutStream() != null && isActive) {
			getOutStream().createPacket(249);
			getOutStream().writeByteA(5); // 5 is to set interactionindex for
			// click attack
			getOutStream().writeLEShortA(defenderIndex);
			flushOutStream();
		}
	}
	
	public void setLoadingClan(boolean loading) {
		loadingClan = loading;
	}
	
	public void skillingReward() { // skilling box
		if (getItems().playerHasItem(15501) && (getItems().freeSlots() > 2)) {
			getItems().deleteItemInOneSlot(15501, 1);
			getItems().addItem(getPA().randomSkillPackage(), 5 + Misc.random(15));
			getItems().addItem(getPA().randomSkillPackage(), 5 + Misc.random(15));
			getItems().addItem(getPA().randomRareSkillPackage(), Misc.random(3));
		} else {
			sendMessage("You need 3 free slots.");
		}
	}

	public void switchDuelScreen() {
		if (duelScreenIsDuel) {
			duelScreenIsDuel = false;
			packetSender.sendFrame126b("clan", 6684);
		} else {
			duelScreenIsDuel = true;
			packetSender.sendFrame126b("duel", 6684);
		}
	}
	
	@Override
	public void update() {
		if (isBot()) {
			return;
		}
		if (getRegion() == null) {
			return;
		}
		if (mapRegionDidChange) {
			sendRegionChanged();
		}
		//flushOutStream();
		// to make sure no leftover packets are around
		/* handler */
		PlayerUpdating.updatePlayer(this, getOutStream()/* outStream */);
		if (insideDungeoneering() && getUpdateFlags().contains(UpdateFlag.CHAT)) {
			int crown;
			if (Config.SERVER_DEBUG) {
				crown = playerRights;
			} else {
				crown = ChatCrown.values[getSetCrown()].getClientIcon();
			}

			for (Client partyPlayer : dungParty.partyMembers) {
				if (partyPlayer == this)
					continue;

				boolean send = true;
				for (Player localPlayer : getLocalPlayers()) {
					if (localPlayer == partyPlayer) {
						send = false;
						break;
					}
				}

				if (send) {
					partyPlayer.getPacketSender().sendChat(getNameSmartUp(), crown, getChatText(), getChatTextSize());
				}
			}
		}

		//flushOutStream();
		/* handler */
		NpcUpdating.updateNPC(this, getOutStream()/* outStream */);
		//flushOutStream();
		if (updateRegion) {
			ObjectManager.loadObjects(this);
			ItemHandler.reloadItems(this);
			// System.out.println("Loading objects in stream");
			updateRegion = false;
		}
		flushOutStream2();
		
	}
	
	public void updateChatCrownsInterface() {
		if (getUnlockedCrowns()[0] == 0) {
			setUnlockedCrown(0, true);
		}
		
		// locks config codes
		for (int i = 0; i < ChatCrown.values.length; i++) {
			int varId = ChatCrown.values[i].getLockConfig();
			if (varId == 0 || getUnlockedCrowns()[i] == 0) {
				continue;
			}

			packetSender.sendConfig(varId, 1);
		}

		// toggle config code of the set crown
		int setCrown = ChatCrown.values[getSetCrown()].getRadioId();
		if (setCrown > 0) {
			packetSender.sendConfig(ChatCrown.RADIO_BUTTON_CONFIG, setCrown);
		}

		getPacketSender().sendConfig(ChatCrown.REGULAR.getLockConfig(), 1); // no rank lock
    }

	public void wildyWarning() {
		getPacketSender().showInterface(1908);
	}
	
	public Runecrafting getRunecrafting() {
		return runecrafting;
	}
	
	public MarketOfferInterface getMarketOffer() {
		return marketOffer;
	}
	
	public PartyOffer getPartyOffer() {
		return partyOffer;
	}
	
	public boolean isNpc() {
		return false;
	}
	
	public boolean isPlayer() {
		return true;
	}

	public StorageObject getStorageObject() {
		return storageObject;
	}
	
	public boolean logoutOnVerificationState() {
		if (inVerificationState) {
			resetInCombatTimer();
			underAttackBy = underAttackBy2 = 0;
			saveFile = false;
			disconnected = true;
			logout();
			return true;
		}
		return false;
	}
	
}
