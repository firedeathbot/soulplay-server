package com.soulplay.game.model.player.save;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.dailytasks.PlayerCategory;
import com.soulplay.content.player.dailytasks.PlayerTask;
import com.soulplay.content.player.dailytasks.task.TaskCategory;
import com.soulplay.content.player.dailytasks.task.impl.TriggerTask;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.slack.SlackMessageBuilder;

public class DailyTasksSave {
	
	private static final String SAVE_Q = "INSERT INTO `daily_task_info`(`player_id`, `tasks_completed`, `task_points`, `bonus_task_amount`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `tasks_completed` = VALUES(`tasks_completed`), `task_points` = VALUES(`task_points`), bonus_task_amount = VALUES(bonus_task_amount)";

	public static void saveInfo(Connection con, int playerId, int taskCompl, int taskPoints, int bonusAmount) {
		boolean madeNewCon = false;
		if (con == null) {
			con = Server.getConnection();
			madeNewCon = true;
		}
		try (PreparedStatement stmt = con.prepareStatement(SAVE_Q);) {
			stmt.setInt(1, playerId);
			stmt.setInt(2, taskCompl);
			stmt.setInt(3, taskPoints);
			stmt.setInt(4, bonusAmount);
			stmt.executeUpdate();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(SoulPlayDateSave.class, e));
			e.printStackTrace();
		}
		if (madeNewCon) {
			try {
				con.close();
			} catch (SQLException e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(SoulPlayDateSave.class, e));
				e.printStackTrace();
			}
		}
	}
	
	private static final String LOAD_Q = "SELECT tasks_completed, task_points, bonus_task_amount FROM `daily_task_info` WHERE `player_id` = ?";
	
	public static boolean loadDates(Player player, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(LOAD_Q);) {

			stmt.setInt(1, player.mySQLIndex);

			loaded = loadDatesResultSet(player, stmt);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadDatesResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				p.dailyTasksCompleted = rs.getInt("tasks_completed");
				p.dailyTasksPoints = rs.getInt("task_points");
				final int bonusTaskAmount = rs.getInt("bonus_task_amount");
				if (bonusTaskAmount > -1) {
					p.bonusTask = new PlayerTask(TriggerTask.VOTE_TASK);
					p.bonusTask.loadCompletedCount(bonusTaskAmount);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static final String SAVE_TASKS = "INSERT INTO `daily_tasks`(`player_id`, `cat_slot`, `task_slot`, `general_id`, `sub_id`, `amount`) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `cat_slot` = VALUES(`cat_slot`), `task_slot` = VALUES(`task_slot`), `general_id` = VALUES(`general_id`), `sub_id` = VALUES(`sub_id`), amount = VALUES(amount)";

	public static void saveTasks(Connection con, Player p) {
		if (p.categories == null)
			return;
		boolean madeNewCon = false;
		if (con == null) {
			con = Server.getConnection();
			madeNewCon = true;
		}
		try (PreparedStatement stmt = con.prepareStatement(SAVE_TASKS);) {
			
			for (int i = 0; i < PlayerCategory.CATEGORY_LENGTH; i++) {
				PlayerCategory category = p.categories[i];
				for (int j = 0; j < PlayerCategory.TASK_LENGTH; j++) {
					PlayerTask task = category.getTask(j);
					
					stmt.setInt(1, p.mySQLIndex);
					stmt.setInt(2, i); // cat slot
					stmt.setInt(3, j); // task slot
					stmt.setInt(4, category.getId());
					stmt.setInt(5, task.getId());
					stmt.setInt(6, task.getCompletedCount());
					stmt.addBatch();
				}
			}
			
			stmt.executeBatch();
			
		} catch (Exception e) {
			e.printStackTrace();
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(SoulPlayDateSave.class, e));
		}
		if (madeNewCon) {
			try {
				con.close();
			} catch (SQLException e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(SoulPlayDateSave.class, e));
				e.printStackTrace();
			}
		}
	}
	
	private static final String RESET_TASKS = "DELETE FROM `daily_tasks` WHERE `player_id` = ?";
	
	public static void resetWeeklyTasks(int playerId) {
		if (Config.SERVER_DEBUG) {
			return;
		}
		
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {
			try (java.sql.Connection connection = Server.getConnection();
					PreparedStatement query = connection.prepareStatement(RESET_TASKS);) {
				query.setInt(1, playerId);
				query.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	private static final String LOAD_TASKS_Q = "SELECT cat_slot, general_id, sub_id, amount FROM `daily_tasks` WHERE `player_id` = ?";
	
	public static boolean loadTasks(Player p, Connection con) {
			boolean loaded = false;
			if (con == null) con = Server.getConnection();

			try (PreparedStatement stmt = con.prepareStatement(LOAD_TASKS_Q);) {

				stmt.setInt(1, p.mySQLIndex);

				loaded = loadTasksResultSet(p, stmt);

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			return loaded;
		}
		
		private static boolean loadTasksResultSet(Player p, PreparedStatement ps) {
			Map<Integer, PlayerCategory> data = new HashMap<>();
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					
					final int catIndex = rs.getInt("cat_slot");
					final int catId = rs.getInt("general_id");
					final int taskId = rs.getInt("sub_id");
					final int amount = rs.getInt("amount");
					PlayerCategory cat = data.get(catIndex);
					if (cat == null) {
						cat = new PlayerCategory(TaskCategory.forId(catId), true);
						data.put(catIndex, cat);
					}
					
					cat.addTask(new PlayerTask(cat.getCategory().getTasks().get(taskId), amount));
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

			if (!data.isEmpty()) {
				PlayerCategory[] categories = new PlayerCategory[PlayerCategory.CATEGORY_LENGTH];
				int index = 0;
				for (PlayerCategory cat : data.values()) {
					categories[index++] = cat;
				}
				p.categories = categories;
			}
			return true;
		}

}
