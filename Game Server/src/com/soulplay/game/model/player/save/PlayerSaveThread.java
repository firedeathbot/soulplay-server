package com.soulplay.game.model.player.save;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class PlayerSaveThread {

	private static List<Player> playerList = new ArrayList<>();

	private static final int SAVE_INTERVAL = 600000;

	public static void addToSaveList(Player p) {
		if (!playerList.contains(p)) {
			playerList.add(p);
		}
	}

	private long lastSave = System.currentTimeMillis();

//	public PlayerSaveThread() throws InterruptedException {
//		AtomicBoolean stop = new AtomicBoolean(false);
//		Executors.newSingleThreadExecutor().execute(() -> {
//			try {
//				while (!stop.get()) {
//					if (Server.getUpdateServer()) {
//						stop.compareAndSet(false, true);
//						return;
//					}
//					if (Server.isRunningMainTask()) { // dunno if this will
//														// cause lag.
//						Thread.sleep(10);
//						continue;
//					}
//					if (System.currentTimeMillis() > lastSave + SAVE_INTERVAL) {
//						putAllPlayersInList();
//						lastSave = System.currentTimeMillis();
//					}
//					loopSaveList();
//					Thread.sleep(100000);
//				}
//			} catch (Exception e) {
//				Server.getSlackApi().call(SlackMessageBuilder
//						.buildExceptionMessage(this.getClass(), e));
//				try {
//					Thread.sleep(100000);
//				} catch (Exception e1) {
//					e1.printStackTrace();
//				}
//			}
//		});
//	}

	private static void loopSaveList() {
		List<Player> tempList = new ArrayList<>();
		synchronized (playerList) {
			tempList.addAll(playerList);
			playerList.clear();
		}
		if (tempList.isEmpty()) {
			return;
		}
		for (Player p : tempList) {
			if (p == null || !PlayerHandler.isPlayerOnline(p.getName())
					|| p.tradeStatus > 0 || p.duelStatus > 0
					|| !p.isActiveAtomic()) {
				continue;
			}
			if (p.disconnected || p.getSavedGame() || p.inVerificationState) {
				continue;
			}
			PlayerSave.saveGame(p, false);
			// p.savedGame.set(false);
		}
	}

	private static void putAllPlayersInList() {
		synchronized (playerList) {
			playerList.clear();
		}
		Map<Long, Player> playerMap = PlayerHandler.getPlayerMap();
		for (Entry<Long, Player> entry : playerMap.entrySet()) {
			Player p = entry.getValue();
			if (p != null && p.isActiveAtomic() && !p.getSavedGame() && !p.inVerificationState) {
				addToSaveList(p);
			}
		}
	}
	
	public static void runSaves() {
		putAllPlayersInList();
		loopSaveList();
	}

}
