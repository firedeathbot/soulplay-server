package com.soulplay.game.model.player.save;

import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.items.RangeWeapon;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.net.slack.SlackMessageBuilder;

public class AlterAccounts {

	public static void transferDungPoints() {
		int count = 0;
		try (Connection con = Server.getConnection();
				PreparedStatement stmt = con.prepareStatement("SELECT player_id, floors_completed FROM `players`.`dung`");
				ResultSet rs = stmt.executeQuery();) {
			while (rs.next()) {
				final long id = rs.getLong("player_id");
				final long value = rs.getLong("floors_completed");
				if (value > 0) {
					int[] intArray = AlterAccounts.loadDungFloorsCompleted(value);
					AlterAccounts.backupPoints(id, intArray);
					count++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("backed up "+ count+ " of accounts");
	}

	private static int[] loadDungFloorsCompleted(long l) {
		ByteBuffer buffer = ByteBuffer.allocate(8).putLong(l);
		int[] values = new int[2];
		values[0] = buffer.getInt(0);
		values[1] = buffer.getInt(1);
		return values;
	}

	private static String BACKUPPOINTS = "INSERT INTO `players`.`dung`(`player_id`, `floors_completed2`) VALUES (?, ?) ON DUPLICATE KEY UPDATE floors_completed2 = VALUES(floors_completed2)";

	private static void backupPoints(final long mysqlId, final int[] points) {
		String newString = GsonSave.gsond.toJson(points);
		try (Connection con = Server.getConnection();
				PreparedStatement stmt = con.prepareStatement(BACKUPPOINTS)) {
			stmt.setLong(1, mysqlId);
			stmt.setString(2, newString);
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void fixPasswords() {
	
		Connection connection = Server.getConnection();
	
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
	
			// 1000:6be3789ba227ee5f2c5b025baa14ecc6f42ae30c7660f5fd:fe969d380148e7d59d61f67962629e1d5f8e1269cb48a59c
			// // prev password hacker
			stmt = connection.createStatement();
			rs = stmt.executeQuery(
					"SELECT Password, ID FROM `accounts` WHERE Password = '1000:330555383a88c07d39e44137e4f519da186c4209556675e3:1cd48a200a77c456f1a40c82a48312ebd31dd1dbc0ddfc3a'");
			while (rs.next()) {
				int id = rs.getInt("ID");
				String pass = AlterAccounts.getOldPassword(id);
	
				if (pass != null) {
					AlterAccounts.fixPassword(id, pass);
				}
	
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
	
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
		}
		System.out.println(
				"Finished Fixing Passwords ------------------------------------------");
	}

	private static void fixPassword(int mysqlIndex, String passwordsToSet) {
	
		Connection connection = Server.getConnection();
	
		PreparedStatement stmt = null;
	
		try {
	
			stmt = connection.prepareStatement(
					"update `accounts` set Password = ? where ID = ?;");
	
			stmt.setString(1, passwordsToSet);
			stmt.setInt(2, mysqlIndex);
			stmt.executeUpdate();
	
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
	
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
		}
	}

	private static String getOldPassword(int mysqlID) {
		String pass = null;
		Connection connection = Server.getConnection();
	
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
	
			stmt = connection.createStatement();
			rs = stmt.executeQuery(
					"SELECT * FROM `passwords` WHERE ID = '" + mysqlID + "'");
			if (rs.next()) {
				pass = rs.getString("Password");
	
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
	
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
		}
		return pass;
	}

	public static void fixAccounts222() {
		Connection connection = Server.getConnection();
	
		Statement stmt = null;
		ResultSet rs = null;
	
		try {
	
			stmt = connection.createStatement();
			rs = stmt.executeQuery(
					"SELECT ID, variables FROM `accounts222`");
			while (rs.next()) {
				int id = rs.getInt("ID");
				String vars = rs.getString("variables");
	
				if (vars != null) {
					AlterAccounts.fixAccountVars(id, vars);
				}
	
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
	
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
		}
		System.out.println(
				"Finished Fixing Variables ------------------------------------------");
	}

	private static final String FIX_VARS = "update `accounts` set variables = ? where ID = ?;";

	public static void fixAccountVars(int ID, String vars) {
		try {
			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(FIX_VARS);) {
				stmt.setString(1, vars);
				stmt.setInt(2, ID);
				stmt.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void fixZulrahBlowpipeScales() {
		Connection connection = Server.getConnection();
	
		Statement stmt = null;
		ResultSet rs = null;
		
		int count = 0;
		
		int[] varsArray = new int[100];
	
		try {
	
			stmt = connection.createStatement();
			rs = stmt.executeQuery(
					"SELECT ID, variables FROM `accounts`");

			
			try (PreparedStatement stmt2 = connection.prepareStatement(PlayerSaveSql.SAVE_CHARGES_Q);) {
			
				while (rs.next()) {
					int id = rs.getInt("ID");
					String vars = rs.getString("variables");

					if (vars != null) {
						varsArray = PlayerSaveSql.getIntArray(vars, 100);
						int amount = varsArray[Variables.TOXIC_BLOWPIPE_CHARGES.getCode()];
						if (amount > 0) {
							uploadScales(id, amount, connection, stmt2);
							count++;
						}
					}

				}
				
				stmt2.executeBatch();
				

			} catch (Exception e) {
				e.printStackTrace();
			}
				
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
	
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
		}
		System.out.println(
				"Finished Uploading "+ count + " scales to blowpipes ------------------------------------------");
	}
	
	private static void uploadScales(int id, int amount, Connection con, PreparedStatement stmt) throws SQLException {
		stmt.setInt(1, id);
		stmt.setInt(2, 12926);
		stmt.setInt(3, amount);
		stmt.addBatch();
	}
	
	private static String SELECT_Q = "SELECT `ID`, `Bank`, `Bank1`, `Bank2`, `Bank3`, `Bank4`, `Bank5`, `Bank6`, `Bank7`, `Bank8`, `Inventory`, `Equipment`, `playerShopItems`, `playerShopItemsN`, `playerShopItemsPrice`, `bob_items` FROM `accounts`";
	
	public static void removeItems(int... searchItem) {
		int count = 0;
		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(SELECT_Q);) {
			
			while (rs.next()) {
				
				int bankItems0[] = new int[Config.BANK_TAB_SIZE];

				int bankItems0N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems1[] = new int[Config.BANK_TAB_SIZE];

				int bankItems1N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems2[] = new int[Config.BANK_TAB_SIZE];

				int bankItems2N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems3[] = new int[Config.BANK_TAB_SIZE];

				int bankItems3N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems4[] = new int[Config.BANK_TAB_SIZE];

				int bankItems4N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems5[] = new int[Config.BANK_TAB_SIZE];

				int bankItems5N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems6[] = new int[Config.BANK_TAB_SIZE];

				int bankItems6N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems7[] = new int[Config.BANK_TAB_SIZE];

				int bankItems7N[] = new int[Config.BANK_TAB_SIZE];

				int bankItems8[] = new int[Config.BANK_TAB_SIZE];

				int bankItems8N[] = new int[Config.BANK_TAB_SIZE];
				
				int[] playerEquipment = new int[14];
				int[] playerEquipmentN = new int[14];
				
				int playerItems[] = new int[28];
				int playerItemsN[] = new int[28];
				
				int playerShopItems[] = new int[Player.MAX_PSHOP_SLOTS];
				int playerShopItemsN[] = new int[Player.MAX_PSHOP_SLOTS];
				long playerShopItemsPrice[] = new long[Player.MAX_PSHOP_SLOTS];
				
				int[] burdenedItems = new int[30];
				
				final int sqlId = rs.getInt("ID");
				
				boolean changed = false;
				
				int index = 0;
				String temp = rs.getString("Bank");
				String[] data;
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems0[index] = itemId;
						bankItems0N[index++] = amount;
					}
				}

				index = 0;
				temp = rs.getString("Bank1");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems1[index] = itemId;
						bankItems1N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank2");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems2[index] = itemId;
						bankItems2N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank3");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems3[index] = itemId;
						bankItems3N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank4");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems4[index] = itemId;
						bankItems4N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank5");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems5[index] = itemId;
						bankItems5N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank6");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems6[index] = itemId;
						bankItems6N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank7");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems7[index] = itemId;
						bankItems7N[index++] = amount;
					}
				}
				index = 0;
				temp = rs.getString("Bank8");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						bankItems8[index] = itemId;
						bankItems8N[index++] = amount;
					}
				}
				
				temp = rs.getString("Inventory");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int Slot = Integer.parseInt(data[i++]);
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						playerItems[Slot] = itemId;
						playerItemsN[Slot] = amount;
					}
				}

				temp = rs.getString("Equipment");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int slot = Integer.parseInt(data[i++]);
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (filterItem(itemId, searchItem)) {
							changed = true;
							playerEquipment[slot] = -1;
							continue;
						}
						playerEquipment[slot] = itemId;
						playerEquipmentN[slot] = amount;
					}
				}
				
				
				String shop1 = rs.getString("playerShopItems");
				String shop2 = rs.getString("playerShopItemsN");
				String shop3 = rs.getString("playerShopItemsPrice");
				
				if (shop1 != null && shop1.contains(";")) {
					String[] array1 = new String[playerShopItems.length];
					array1 = shop1.split(";");
					
					String[] array2 = new String[playerShopItems.length];
					array2 = shop2.split(";");
					
					String[] array3 = new String[playerShopItems.length];
					array3 = shop3.split(";");
					
					for (int i = 0; i < playerShopItems.length; i++) {
						
						int itemId = Integer.parseInt(array1[i]);
						int amount = Integer.parseInt(array2[i]);
						long price = Long.parseLong(array3[i]);
						
						if (itemId == 0)
							continue;
						

						if (filterItem(itemId, searchItem)) {
							changed = true;
							continue;
						}
						
						playerShopItems[i] = itemId;
						playerShopItemsN[i] = amount;
						playerShopItemsPrice[i] = price;
					}
					
				}
				
				shop1 = rs.getString("bob_items");
				
				if (shop1 != null && shop1.contains(";")) {
					String[] array1 = new String[burdenedItems.length];
					array1 = shop1.split(";");
					
					
					for (int i = 0; i < burdenedItems.length; i++) {
						
						int itemId = Integer.parseInt(array1[i]);
						
						if (itemId == 0)
							continue;
						

						if (filterItem(itemId-1, searchItem)) {
							changed = true;
							continue;
						}
						
						burdenedItems[i] = itemId;
					}
					
				}
				
				if (changed) {
					saveFilteredAccount(sqlId, bankItems0, bankItems1, bankItems2, bankItems3, bankItems4, bankItems5, bankItems6, bankItems7, bankItems8, bankItems0N, bankItems1N, bankItems2N, bankItems3N, bankItems4N, bankItems5N, bankItems6N, bankItems7N, bankItems8N, playerItems, playerItemsN, playerEquipment, playerEquipmentN, playerShopItems, playerShopItemsN, playerShopItemsPrice, burdenedItems);
					count++;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Filtered accounts count: " + count);
	}
	
	private static String UPDATE_Q = "update `accounts` set Bank = ?, Bank1 = ?, Bank2 = ?, Bank3 = ?, Bank4 = ?, Bank5 = ?, Bank6 = ?, Bank7 = ?, Bank8 = ?, Inventory = ?, Equipment = ?, playerShopItems = ?, playerShopItemsN = ?, playerShopItemsPrice = ?, bob_items = ? Where ID = ?;";
	
	private static void saveFilteredAccount(int sqlId, int[] bankItems0, int[] bankItems1, int[] bankItems2, int[] bankItems3, int[] bankItems4, int[] bankItems5, int[] bankItems6, int[] bankItems7, int[] bankItems8, int[] bankItems0N, int[] bankItems1N, int[] bankItems2N, int[] bankItems3N, int[] bankItems4N, int[] bankItems5N, int[] bankItems6N, int[] bankItems7N, int[] bankItems8N, int[] playerItems, int[] playerItemsN, int[] playerEquipment, int[] playerEquipmentN, int[] playerShopItems, int[] playerShopItemsN, long[] playerShopItemsPrice, int[] burdenedItems) {
		try (Connection con = Server.getConnection();
				PreparedStatement stmt = con.prepareStatement(UPDATE_Q)) {
			
			StringBuilder temp = new StringBuilder();

			/* ItemID - Amount */ // bank 0
			for (int i = 0; i < bankItems0.length; i++) {
				// if (bankItems0[i] > 0)
				temp.append(bankItems0[i] + ":" + bankItems0N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(1, "");
			} else {
				stmt.setString(1,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 1
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems1.length; i++) {
				// if (bankItems1[i] > 0)
				temp.append(bankItems1[i] + ":" + bankItems1N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(2, "");
			} else {
				stmt.setString(2,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 2
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems2.length; i++) {
				// if (bankItems2[i] > 0)
				temp.append(bankItems2[i] + ":" + bankItems2N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(3, "");
			} else {
				stmt.setString(3,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 3
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems3.length; i++) {
				// if (bankItems3[i] > 0)
				temp.append(bankItems3[i] + ":" + bankItems3N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(4, "");
			} else {
				stmt.setString(4,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 4
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems4.length; i++) {
				// if (bankItems4[i] > 0)
				temp.append(bankItems4[i] + ":" + bankItems4N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(5, "");
			} else {
				stmt.setString(5,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 5
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems5.length; i++) {
				// if (bankItems5[i] > 0)
				temp.append(bankItems5[i] + ":" + bankItems5N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(6, "");
			} else {
				stmt.setString(6,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 6
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems6.length; i++) {
				// if (bankItems6[i] > 0)
				temp.append(bankItems6[i] + ":" + bankItems6N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(7, "");
			} else {
				stmt.setString(7,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 7
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems7.length; i++) {
				// if (bankItems7[i] > 0)
				temp.append(bankItems7[i] + ":" + bankItems7N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(8, "");
			} else {
				stmt.setString(8,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 8
			temp.delete(0, temp.length());
			for (int i = 0; i < bankItems8.length; i++) {
				// if (bankItems8[i] > 0)
				temp.append(bankItems8[i] + ":" + bankItems8N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(9, "");
			} else {
				stmt.setString(9,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}
			
			temp.delete(0, temp.length());
			for (int i = 0; i < playerItems.length; i++) {
				// if(playerItems[i] > 0)
				temp.append(i + ":" + playerItems[i] + ":" + playerItemsN[i]
						+ ":");
			}
			if (temp.length() == 0) {
				stmt.setString(10, "");
			} else {
				stmt.setString(10,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}
			
			temp.delete(0, temp.length());
			for (int i = 0; i < playerEquipment.length; i++) {
				temp.append(i + ":" + playerEquipment[i] + ":"
						+ playerEquipmentN[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(11, "");
			} else {
				stmt.setString(11,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}
			
			stmt.setString(12, PlayerSaveSql.createIntArrayWithSeperator(playerShopItems));
			stmt.setString(13, PlayerSaveSql.createIntArrayWithSeperator(playerShopItemsN));
			stmt.setString(14, PlayerSaveSql.createLongArrayWithSeperator(playerShopItemsPrice));
			
			stmt.setString(15, PlayerSaveSql.createIntArrayWithSeperator(burdenedItems));
			
			stmt.setInt(16, sqlId);
			
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static boolean filterItem(int realItemId, int[] searchItem) {
		for (int k = 0; k < searchItem.length; k++) {
			if (realItemId == searchItem[k])
				return true;
		}
		return false;
	}
	
	
	
	private static String SELECT_Q1 = "SELECT `ID`, `PlayerName`, `Equipment` FROM `accounts`";
	
	public static void scanStrangeEquip() {
		int count = 0;
		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(SELECT_Q1);) {
			
			while (rs.next()) {
				
				
				final int sqlId = rs.getInt("ID");
				final String displayName = rs.getString("PlayerName");
				
				String temp = rs.getString("Equipment");
				String[] data;
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int slot = Integer.parseInt(data[i++]);
						int itemId = Integer.parseInt(data[i++]);
						int amount = Integer.parseInt(data[i]);
						if (slot != PlayerConstants.playerArrows) {
							if (itemId > 0 && itemId != 10501 && amount > 1 && RangeWeapon.get(itemId) == null) {
								System.out.println("non ranged equip found on account ID:"+ sqlId + " username:"+displayName);
								count++;
							}
						}
					}
				}
				
				
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("accounts looped: " + count);
	}
	
	
	public static void moveTable() {
		Client c = null;
		int count = 0;
		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT ID, PlayerName, Password, display_name FROM `accounts`");) {
			
			while (rs.next()) {
				
				final int ID = rs.getInt("ID");
				final String username = rs.getString("PlayerName");
				final String password = rs.getString("Password");
				final String displayName = rs.getString("display_name");
				
				createDefaults(ID, username, password, displayName);
				
				c = new Client(null);
				
				c.skipPassword = true;
				
				if (PlayerSaveSql.loadGame(c, username, null, connection) == 13) {
					PlayerSaveSql.saveGame(c, false);
					count++;
				}
				
				
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(String.format("Looped %d amount of accounts.", count));
	}
	
	public static void unlockPrestigeCrowns() {
		int count = 0;
		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT ID, difficulty FROM `accounts`");) {
			
			while (rs.next()) {
				
				final int ID = rs.getInt("ID");
				final int difficulty = rs.getInt("difficulty");
				
				if (difficulty == PlayerDifficulty.PRESTIGE_ONE || difficulty == PlayerDifficulty.PRESTIGE_TWO || difficulty == PlayerDifficulty.PRESTIGE_THREE) {
					switch (difficulty) {
					case PlayerDifficulty.PRESTIGE_ONE:
						saveChatCrown(ID, ChatCrown.PRESTIGE_1, 1, connection);
						break;
					case PlayerDifficulty.PRESTIGE_TWO:
						saveChatCrown(ID, ChatCrown.PRESTIGE_2, 1, connection);
						break;
					case PlayerDifficulty.PRESTIGE_THREE:
						saveChatCrown(ID, ChatCrown.PRESTIGE_3, 1, connection);
						break;
					}
					count++;
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(String.format("Looped %d amount of accounts.", count));
	}
	
	public static void saveChatCrown(final int mysqlIndex,
			final ChatCrown chatCrown, final int unlocked, Connection connection) {
	    
	    
	    if (connection == null) connection = Server.getConnection();

	    Statement stmt = null;

	    try {
	    	stmt = connection.createStatement();
	    	stmt.executeUpdate("INSERT INTO `chat_crowns` (player_id, "
	    			+ chatCrown.getTableName() + ") values ('" + mysqlIndex
	    			+ "', '" + unlocked + "') ON DUPLICATE KEY UPDATE "
	    			+ chatCrown.getTableName() + "=" + unlocked,
	    			Statement.RETURN_GENERATED_KEYS);

	    	try {
	    		if (stmt != null) {
	    			stmt.close();
	    		}
	    	} catch (Exception e1) {
	    		e1.printStackTrace();
	    	}

	    } catch (SQLException e2) {
	    	e2.printStackTrace();
	    } finally {
	    	try {
	    		if (connection != null) {
	    			connection.close();
	    		}
	    	} catch (SQLException e3) {
	    		e3.printStackTrace();
	    	}
	    }
	}
	
	public static void createDefaults(int id, String playername, String password, String displayName) {
		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();) {
			
			stmt.executeUpdate("INSERT INTO `accounts2` (ID, PlayerName, Password, display_name) values ('" + id + "', '" + playername + "', '" + password + "', '"
											+ displayName + "')",
											Statement.RETURN_GENERATED_KEYS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void moveSkills() {
		Client p = null;
		int count = 0;
		try (Connection connection = Server.getConnection();
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT ID, Skill FROM `accounts`");) {
			
			while (rs.next()) {
				
				final int ID = rs.getInt("ID");
				String temp = rs.getString("Skill");
				
				p = new Client(null);
				
				p.mySQLIndex = ID;
				
				try {
					String data[];
					int index = 0;
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int currentLevel = Integer.parseInt(data[i++]);
							int currentLevelIndex = index;
							// p.getPlayerLevel()[index] = Integer.parseInt(data[i++]);
							p.getRealSkills().setExperienceAndLevels(index++, Integer.parseInt(data[i])); // p.playerXP[index++] = Integer.parseInt(data[i]);
							p.getRealSkills().getDynamicLevels()[currentLevelIndex] = currentLevel;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Something went wrong for ID:"+ID);
					continue;
				}
				
				PlayerSaveSql.saveSkills(p, connection);
				
				count++;
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(String.format("Looped %d amount of accounts.", count));
	}
	
}
