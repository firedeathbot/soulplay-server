package com.soulplay.game.model.player.save;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.soulplay.Server;
import com.soulplay.game.model.player.info.ExtraHiscores;
import com.soulplay.game.model.player.info.ExtraHiscoresEnum;

public class ExtraHiscoresSaving {
	
	public static boolean loadExtraHiscores(final int mysqlIndex, final ExtraHiscores h, Connection connection) {

		boolean loaded = false;
		
		try (PreparedStatement stmt = connection.prepareStatement(LOAD_QUERY);) {

			stmt.setInt(1, mysqlIndex);
			
			try (ResultSet rs = stmt.executeQuery();) {
				
				if (rs.next()) {
					for (int i = 0; i < ExtraHiscoresEnum.values.length; i++) {
						ExtraHiscoresEnum.values[i].setValue(h, rs.getInt(ExtraHiscoresEnum.values[i].getColumnName()));
					}
				}
				loaded = true;

			} catch (SQLException e) {
				e.printStackTrace();
				loaded = false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			loaded = false;
		}
		
		return loaded;
	}
	
	public static void saveExtraHiscores(final int mysqlIndex, final ExtraHiscores h, Connection connection) {

		if (connection == null) connection = Server.getConnection();

		try (Statement stmt = connection.createStatement()) {

			stmt.executeUpdate(generateSaveString(mysqlIndex, h), Statement.RETURN_GENERATED_KEYS);

		} catch (SQLException e2) {
			e2.printStackTrace();
		}
	}
	
	private static String generateSaveString(int sqlIndex, final ExtraHiscores h) {
		StringBuilder b = new StringBuilder();

		b.append("INSERT INTO `extra_hiscores` (player_id");

		for (int i = 0; i < ExtraHiscoresEnum.values.length; i++) {
			b.append(", "+ExtraHiscoresEnum.values[i].getColumnName());
		}

		b.append(") values (" + sqlIndex);

		for (int i = 0; i < ExtraHiscoresEnum.values.length; i++) {
			b.append(", "+ExtraHiscoresEnum.values[i].getValue(h));
		}

		b.append(") ON DUPLICATE KEY UPDATE ");

		for (int i = 0; i < ExtraHiscoresEnum.values.length; i++) {
			b.append(ExtraHiscoresEnum.values[i].getColumnName()+"="+ExtraHiscoresEnum.values[i].getValue(h));
			if (i < ExtraHiscoresEnum.values.length-1)
				b.append(", ");
		}
		
		return b.toString();
	}
	
	private static String LOAD_QUERY = "";
	
	static {
		StringBuilder b = new StringBuilder();
		b.append("SELECT");

		for (int i = 0; i < ExtraHiscoresEnum.values.length; i++) {
			b.append(" " + ExtraHiscoresEnum.values[i].getColumnName());
			if (i < ExtraHiscoresEnum.values.length-1)
				b.append(", ");
		}

		b.append(" FROM `extra_hiscores`");

		b.append(" where player_id = ?");

		LOAD_QUERY = b.toString();
	}
	
}
