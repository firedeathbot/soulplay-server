package com.soulplay.game.model.player.save;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class ClueProgressSave {

	public static String SAVE_Q = "INSERT INTO `clue_progress`(`player_id`, `slot`, `value`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)";
	
	private static String CLEAR_Q = "DELETE FROM `clue_progress` WHERE `player_id` = ?";
	
	public static void saveClues(Player player, Connection con) {
		
		if (con == null) con = Server.getConnection();
		
		try (PreparedStatement stmt = con.prepareStatement(CLEAR_Q);) {

			stmt.setInt(1, player.mySQLIndex);
			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try (PreparedStatement stmt = con.prepareStatement(SAVE_Q);) {
			
			for (int i = 0, l = player.getTreasureTrailManager().getCluesData().length; i < l; i++) {
				if (player.getTreasureTrailManager().getCluesData()[i] == -1) continue;
				
				stmt.setInt(1, player.mySQLIndex);
				stmt.setInt(2, i);
				stmt.setInt(3, player.getTreasureTrailManager().getCluesData()[i]);
				stmt.addBatch();
				
				stmt.setInt(1, player.mySQLIndex);
				stmt.setInt(2, Misc.setBit(i, 7));
				stmt.setInt(3, player.getTreasureTrailManager().getCluesStage()[i]);
				stmt.addBatch();
				
				stmt.setInt(1, player.mySQLIndex);
				stmt.setInt(2, Misc.setBit(i, 8));
				stmt.setInt(3, player.getTreasureTrailManager().getCluesLength()[i]);
				stmt.addBatch();
			}
			
			stmt.executeBatch();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static final String LOAD_Q = "SELECT slot, value FROM `clue_progress` WHERE `player_id` = ?";
	
	public static boolean loadClues(Player player, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(LOAD_Q);) {

			stmt.setInt(1, player.mySQLIndex);

			loaded = loadClueResultSet(player, stmt);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadClueResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				final int slot = rs.getInt("slot");
				final int value = rs.getInt("value");
				if (Misc.testBit(slot, 7)) {
					int realSlot = Misc.clearBit(slot, 7);
					p.getTreasureTrailManager().getCluesStage()[realSlot] = value;
				} else if (Misc.testBit(slot, 8)) {
					int realSlot = Misc.clearBit(slot, 8);
					p.getTreasureTrailManager().getCluesLength()[realSlot] = value;
				} else {
					p.getTreasureTrailManager().getCluesData()[slot] = value;
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
