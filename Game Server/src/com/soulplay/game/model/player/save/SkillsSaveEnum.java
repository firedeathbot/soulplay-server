package com.soulplay.game.model.player.save;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.soulplay.content.player.skills.Skills;

public enum SkillsSaveEnum {

	ATTACK(Skills.ATTACK, "attack_lvl", "attack_exp"),
	DEFENSE(Skills.DEFENSE, "defense_lvl", "defense_exp"),
	STRENGTH(Skills.STRENGTH, "str_lvl", "str_exp"),
	HITPOINTS(Skills.HITPOINTS, "hitpoints_lvl", "hitpoints_exp"),
	RANGED(Skills.RANGED, "ranged_lvl", "ranged_exp"),
	PRAYER(Skills.PRAYER, "prayer_lvl", "prayer_exp"),
	MAGIC(Skills.MAGIC, "magic_lvl", "magic_exp"),
	COOKING(Skills.COOKING, "cooking_lvl", "cooking_exp"),
	WOODCUTTING(Skills.WOODCUTTING, "wc_lvl", "wc_exp"),
	FLETCHING(Skills.FLETCHING, "fletch_lvl", "fletch_exp"),
	FISHING(Skills.FISHING, "fishing_lvl", "fishing_exp"),
	FIREMAKING(Skills.FIREMAKING, "firemaking_lvl", "firemaking_exp"),
	CRAFTING(Skills.CRAFTING, "crafting_lvl", "crafting_exp"),
	SMITHING(Skills.SMITHING, "smithing_lvl", "smithing_exp"),
	MINING(Skills.MINING, "mining_lvl", "mining_exp"),
	HERBLORE(Skills.HERBLORE, "herblore_lvl", "herblore_exp"),
	AGILITY(Skills.AGILITY, "agility_lvl", "agility_exp"),
	THIEVING(Skills.THIEVING, "thieving_lvl", "thieving_exp"),
	SLAYER(Skills.SLAYER, "slayer_lvl", "slayer_exp"),
	FARMING(Skills.FARMING, "farming_lvl", "farming_exp"),
    RUNECRAFTING(Skills.RUNECRAFTING, "runecrafting_lvl", "runecrafting_exp"),
	DUNGEONEERING(Skills.DUNGEONEERING, "dung_lvl", "dung_exp"),
	HUNTER(Skills.HUNTER, "hunter_lvl", "hunter_exp"),
	SUMMONING(Skills.SUMMONING, "summoning_lvl", "summoning_exp"),
	CONSTRUCTION(Skills.CONSTRUCTION, "construction_lvl", "construction_exp");
	
	
	private final int skillId;

	private final String lvlTable, expTable;
	
	public int getSkillId() {
		return skillId;
	}

	public String getLvlTable() {
		return lvlTable;
	}

	public String getExpTable() {
		return expTable;
	}
	
	private SkillsSaveEnum(int skillId, String lvlTable, String expTable) {
		this.skillId = skillId;
		this.lvlTable = lvlTable;
		this.expTable = expTable;
	}
	
	private static final List<SkillsSaveEnum> list = new ArrayList<>(Skills.STAT_COUNT);
	
	public static String SAVE_STRING = "INSERT INTO `skills`(`player_id`, ";
	
	public static void init() {
		for (SkillsSaveEnum skill : values()) {
			list.add(skill.skillId, skill);
		}
		Collections.sort(list, new SortBySkillId()); // just in case someone messes something up
		
		generateString();
	}
	
	static class SortBySkillId implements Comparator<SkillsSaveEnum> {

		@Override
		public int compare(SkillsSaveEnum o1, SkillsSaveEnum o2) {
			return o1.skillId - o2.skillId;
		}
		
	}
	
	
	private static void generateString() {
		StringBuilder str = new StringBuilder("INSERT INTO `skills`(`player_id`, ");
		
		for (int i = 0, len = list.size(); i < len; i ++) {
			SkillsSaveEnum skill = list.get(i);
			str.append("`");
			str.append(skill.lvlTable);
			str.append("`, `");
			str.append(skill.expTable);
			str.append("`");
			
			if (i == len-1) continue;
			str.append(", ");
		}
		
		str.append(") VALUES (?, ");
		
		for (int i = 0, len = list.size()*2; i < len; i ++) {
			str.append("?");
			
			if (i == len-1) continue;
			str.append(", ");
		}
		
		str.append(") ON DUPLICATE KEY UPDATE ");
		
		for (int i = 0, len = list.size(); i < len; i ++) {
			SkillsSaveEnum skill = list.get(i);
			str.append(skill.lvlTable);
			str.append(" = VALUES(");
			str.append(skill.lvlTable);
			str.append("), ");
			str.append(skill.expTable);
			str.append(" = VALUES(");
			str.append(skill.expTable);
			str.append(")");
			
			if (i == len-1) continue;
			str.append(", ");
		}
		
		SAVE_STRING = str.toString();
	}
	
	public static SkillsSaveEnum getSkill(int id) {
		return list.get(id);
	}
	
}
