package com.soulplay.game.model.player.save;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.chatcrown.ChatCrown;

public class ChatCrownsSql {

	public static void importAllCrowns() {

		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

			Connection connection = Server.getConnection();

			Statement stmt = null;
			ResultSet rs = null;

			try {

				stmt = connection.createStatement();
				rs = stmt.executeQuery(
						"select ID, characterrights, TotalDonatedAmount  from `accounts`");
				while (rs.next()) {
					int id = rs.getInt("ID");
					int rights = rs.getInt("characterrights");
					int donAmount = rs.getInt("TotalDonatedAmount");
					if (rights == 1) {
						saveChatCrown(id, ChatCrown.MOD_CROWN, 1);
					}
					if (rights == 2) {
						saveChatCrown(id, ChatCrown.MOD_CROWN, 1);
						saveChatCrown(id, ChatCrown.ADMIN_CROWN, 1);
					}
					if (rights == 3) {
						saveChatCrown(id, ChatCrown.MOD_CROWN, 1);
						saveChatCrown(id, ChatCrown.ADMIN_CROWN, 1);
					}
					if (donAmount >= 10) {
						saveChatCrown(id, ChatCrown.RED_DONOR, 1);
					}
					if (donAmount >= 50) {
						saveChatCrown(id, ChatCrown.BLUE_DONOR, 1);
					}
					if (donAmount >= 250) {
						saveChatCrown(id, ChatCrown.GREEN_DONOR, 1);
					}
					if (donAmount >= 1000) {
						saveChatCrown(id, ChatCrown.PURPLE_DONOR, 1);
					}
					if (donAmount >= 5000) {
						saveChatCrown(id, ChatCrown.GOLD_DONOR, 1);
					}
					if (donAmount >= 10000) {
						saveChatCrown(id, ChatCrown.INSANE, 1);
					}
					if (rights == 7) {
						saveChatCrown(id, ChatCrown.INFORMER, 1);
					}
					if (rights == 9) {
						saveChatCrown(id, ChatCrown.VETERAN, 1);
					}
					if (rights == 14) {
						saveChatCrown(id, ChatCrown.YOUTUBE_RANK, 1);
					}
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {
				if (rs != null) {
					try {
						rs.close();
						rs = null;
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}

				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e4) {
						e4.printStackTrace();
					}
				}
				System.out.println(
						"FINISHED EXTRACTING CROWNS!!!!-----------------------");
			}
		});
	}

	public static boolean loadChatCrowns(Player p, Connection connection) {

		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(
						"select * from `chat_crowns` where player_id = '"
								+ p.mySQLIndex + "' limit 1"); ) {
			if (rs.next()) {
				for (int i = 0; i < ChatCrown.values.length; i++) {
					if (ChatCrown.values[i].getTableName() == null) {
						continue;
					}
					p.setUnlockedCrown(i,
							rs.getInt(ChatCrown.values[i].getTableName()));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean lockChatCrown(Player p, ChatCrown chatCrown) {
		if (p.getUnlockedCrowns()[chatCrown.ordinal()] == 1) {
			p.setUnlockedCrown(chatCrown.ordinal(), false);
			saveChatCrown(p.mySQLIndex, chatCrown, 0);
			p.getPacketSender().sendConfig(chatCrown.getLockConfig(), 0);
			return true;
		}
		return false;
	}

	public static void saveChatCrown(final int mysqlIndex,
			final ChatCrown chatCrown, final int unlocked) {
	    
	    if (chatCrown.getTableName() == null) {
		return;
	    }
	    
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {
			Connection connection = Server.getConnection();

			Statement stmt = null;

			try {
				stmt = connection.createStatement();
				stmt.executeUpdate("INSERT INTO `chat_crowns` (player_id, "
						+ chatCrown.getTableName() + ") values ('" + mysqlIndex
						+ "', '" + unlocked + "') ON DUPLICATE KEY UPDATE "
						+ chatCrown.getTableName() + "=" + unlocked,
						Statement.RETURN_GENERATED_KEYS);

				try {
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			} catch (SQLException e2) {
				e2.printStackTrace();
			} finally {
				try {
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException e3) {
					e3.printStackTrace();
				}
			}
		});
	}

	public static boolean unlockChatCrown(Player player, ChatCrown chatCrown) {
		if (player.getUnlockedCrowns()[chatCrown.ordinal()] == 0) {
			player.setUnlockedCrown(chatCrown.ordinal(), true);
			player.getPacketSender().sendConfig(chatCrown.getLockConfig(), 1);
			saveChatCrown(player.mySQLIndex, chatCrown, 1);
			if (player.isActive) {
				player.sendMessage("You have unlocked a chat crown: " + chatCrown.getPrettyName());
			}
			return true;
		}
		return false;
	}

}
