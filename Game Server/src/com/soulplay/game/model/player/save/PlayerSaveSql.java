package com.soulplay.game.model.player.save;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.items.CompCape;
import com.soulplay.content.items.itemretrieval.ItemRetrievalInstance;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.items.presets.Preset;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.npcs.impl.bosses.BossKCHandler;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.namechange.NameChange;
import com.soulplay.content.player.pets.pokemon.PokemonSave;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.construction.Poh;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.content.player.skills.runecrafting.PouchType;
import com.soulplay.content.player.untradeables.UntradeableManager;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.ContainerType;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;
import com.soulplay.util.PasswordHash;
import com.soulplay.util.XenforoLink;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.sql.Highscore;

public class PlayerSaveSql {

	// private static ExecutorService saveSQLService =
	// Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	public static final ScheduledExecutorService SAVE_EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();
	
	public static void nameThread() {
		SAVE_EXECUTOR.submit(() -> {
			Thread.currentThread().setName("player-save-thread");
		});
	}

	/**
	 * Saving
	 **/
	private static String UpdateQuery = "update `accounts` set characterheight = ?, characterposx = ?, characterposy = ?, characterrights = ?, difficulty = ?, homeTeleActions = ?, lastClanChat = ?, timePlayed = ?, lastDayLoggedIn = ?, lastDayPkedOfYear = ?, playerTitle = ?, titleColor = ?, titles = ?, UUID = ?, connectedFrom = ?, allConnectedIps = ?, lastKilledPlayers = ?, achievements = ?, achievementsCompleted = ?, quests = ?, questPoints = ?, timers = ?, variables = ?, setPin = ?, bankPin = ?, hasBankpin = ?, pinDeleteDateRequested = ?, requestPinDelete = ?, playerMagicBook = ?, playerPrayerBook = ?, dfsCharge = ?, specAmount = ?, wildyPlayerKillCount = ?, wildyPlayerDeathCount = ?, killStreak = ?, highestKillStreak = ?, wildyEloRating = ?, xpLock = ?, chatSettings = ?, teleblocklength = ?, wildernessTick = ?, npcKills = ?, fightMode = ?, autoRet = ?, doubleExpLength = ?, muteEnd = ?, accountFlagged = ?, barrows = ?, wave = ?, tasksCompleted = ?, taskName = ?, masterName = ?, assignmentAmount = ?, slayerPoints = ?, duoPoints = ?, uuid_history = ?, dungBindedItems = ?, lms_points = ?, magePoints = ?, pkpoints = ?, penguinPoints = ?, donPoints = ?, TotalDonatedAmount = ?, yellPoints = ?, loyaltyPoints = ?, agilityPoints = ?, pcPoints = ?, gwdkc = ?, Bank = ?, Bank1 = ?, Bank2 = ?, Bank3 = ?, Bank4 = ?, Bank5 = ?, Bank6 = ?, Bank7 = ?, Bank8 = ?, Look = ?, Inventory = ?, Equipment = ?, rune_pouch = ?, playerCollect = ?, playerShopItems = ?, playerShopItemsN = ?, playerShopItemsPrice = ?, xf_id = ?, totalGold = ?, boss_kills = ?, disabled = ?, quick_prayers = ?, quick_curses = ?, judgement = ?, duel_wins = ?, duel_losses = ?, duel_streak = ?, untradeable_shop_map = ?, unlocked_crowns = ?, seasonalElo = ?, zealPoints = ?, money_pouch = ?, summon_pouch = ?, summon_time = ?, bob_items = ?, pokemon_unlocked = ?, comp_cape = ?, logged_into = ?, temp = ?, donatedRealTotalAmount = ? Where ID = ?;";

	private static String inventoryUpdateQuery = "update `accounts` set Inventory = ? Where ID = ?;";

	private static String bobInvUpdateQuery = "update `accounts` set bob_items = ? Where ID = ?;";
	
	private static String bankItemsUpdateQuery = "update `accounts` set Bank = ?, Bank1 = ?, Bank2 = ?, Bank3 = ?, Bank4 = ?, Bank5 = ?, Bank6 = ?, Bank7 = ?, Bank8 = ? Where ID = ?;";
	
	private static String UpdateQueryPVP = "update `accountspvp` set characterheight = ?, characterposx = ?, characterposy = ?, characterrights = ?, difficulty = ?, homeTeleActions = ?, lastClanChat = ?, timePlayed = ?, lastDayLoggedIn = ?, lastDayPkedOfYear = ?, playerTitle = ?, titleColor = ?, titles = ?, UUID = ?, connectedFrom = ?, allConnectedIps = ?, lastKilledPlayers = ?, achievements = ?, achievementsCompleted = ?, quests = ?, questPoints = ?, timers = ?, variables = ?, setPin = ?, bankPin = ?, hasBankpin = ?, pinDeleteDateRequested = ?, requestPinDelete = ?, playerMagicBook = ?, playerPrayerBook = ?, dfsCharge = ?, specAmount = ?, wildyPlayerKillCount = ?, wildyPlayerDeathCount = ?, killStreak = ?, highestKillStreak = ?, wildyEloRating = ?, xpLock = ?, chatSettings = ?, teleblocklength = ?, wildernessTick = ?, npcKills = ?, fightMode = ?, autoRet = ?, doubleExpLength = ?, muteEnd = ?, accountFlagged = ?, barrows = ?, wave = ?, tasksCompleted = ?, taskName = ?, masterName = ?, assignmentAmount = ?, slayerPoints = ?, duoPoints = ?, uuid_history = ?, dungBindedItems = ?, lms_points = ?, magePoints = ?, pkpoints = ?, penguinPoints = ?, donPoints = ?, TotalDonatedAmount = ?, yellPoints = ?, loyaltyPoints = ?, agilityPoints = ?, pcPoints = ?, gwdkc = ?, Bank = ?, Bank1 = ?, Bank2 = ?, Bank3 = ?, Bank4 = ?, Bank5 = ?, Bank6 = ?, Bank7 = ?, Bank8 = ?, Skill = ?, Look = ?, Inventory = ?, Equipment = ?, rune_pouch = ?, playerCollect = ?, playerShopItems = ?, playerShopItemsN = ?, playerShopItemsPrice = ?, xf_id = ?, totalGold = ?, boss_kills = ?, disabled = ?, quick_prayers = ?, quick_curses = ?, judgement = ?, untradeable_shop_map = ?, unlocked_crowns = ?, seasonalElo = ?, zealPoints = ?, money_pouch = ? Where ID = ?;";

	private static String UpdateQueryForPassBankPinETC = "update `accounts` set setPin = ?, bankPin = ?, hasBankpin = ?, pinDeleteDateRequested = ?, requestPinDelete = ?, chatSettings = ?, logged_into = ? Where PlayerName = ? limit 1;";

	public static String createIntArrayWithSeperator(int[] array) {
		StringBuilder temp = new StringBuilder();
		for (int element : array) {
			temp.append(element + ";");
		}
		if (temp.length() == 0) {
			return "";
		}

		return temp.toString();
	}
	
	public static String createIntArrayWithSeperator(short[] array) {
		StringBuilder temp = new StringBuilder();
		for (short element : array) {
			temp.append((element & 0xFFFF) + ";");
		}
		if (temp.length() == 0) {
			return "";
		}

		return temp.toString();
	}

	public static String createIntArrayWithSeperator(List<String> array) {
		StringBuilder temp = new StringBuilder();
		for (int i = 0; i < array.size(); i++) {
			temp.append(array.get(i) + ";");
		}
		if (temp.length() == 0) {
			return "";
		}

		return temp.toString();
	}

	public static String createLongArrayWithSeperator(long[] array) {
		StringBuilder temp = new StringBuilder();
		for (long element : array) {
			temp.append(element + ";");
		}
		if (temp.length() == 0) {
			return "";
		}

		return temp.toString();
	}

	public static String createStringFromIntList(List<Integer> array) {
		StringBuilder temp = new StringBuilder();
		for (int i : array) {
			temp.append(i + ";");
		}
		if (temp.length() == 0) {
			return "";
		}

		return temp.toString();
	}

	/**
	 * this is used for fields in sql that were changed to load something else
	 * instead so generate 0's for the whole array.
	 */
	public static int[] getIntArray(final String stringToSplit,
			int lengthOfLoadingArray) {
		try {
			if (stringToSplit == null) {
				int intArray[] = new int[lengthOfLoadingArray];
				return intArray;
			}
			String[] array = new String[lengthOfLoadingArray];
			array = stringToSplit.split(";");
			int intArray[] = new int[lengthOfLoadingArray];
			
			//if array is extended, then extend predefined size
			if (array.length > lengthOfLoadingArray)
				intArray = new int[array.length];
			for (int i = 0; i < lengthOfLoadingArray; i++) {
				if (i >= array.length) {
					continue; //this should never occur
				}
				intArray[i] = Integer.parseInt(array[i]);
			}
			return intArray;
		} catch (Exception ex) { // NOTE: no need to print error here. This is
									// in case we ever change fields or sizes of
									// the fields, it will generate new array
			return new int[lengthOfLoadingArray];
		}
	}

	/**
	 * this is used for arrays that increased in size in mysql db. If changing
	 * array to save for something else, then we need to create new array with
	 * all 0's
	 */
	public static int[] getIntArrayIncreasedSize(final String stringToSplit,
			int lengthOfLoadingArray) {
		try {
			if (stringToSplit == null) {
				int intArray[] = new int[lengthOfLoadingArray];
				return intArray;
			}
			String[] array = new String[lengthOfLoadingArray];
			array = stringToSplit.split(";");
			int intArray[] = new int[lengthOfLoadingArray];
			for (int i = 0; i < lengthOfLoadingArray; i++) {
				if (i >= array.length) {
					// This is outside the array so generate 0's for the rest of
					// the field, intead of generating 0's for the whole field.
					intArray[i] = 0;
					continue;
				}

				intArray[i] = Integer.parseInt(array[i]);
			}
			return intArray;
		} catch (Exception ex) { // NOTE: no need to print error here. This is
									// in case we ever change fields or sizes of
									// the fields, it will generate new array
			return new int[lengthOfLoadingArray];
		}
	}

	public static List<Integer> getIntListFromString(String s) {
		if (s == null || s.isEmpty()) {
			return new ArrayList<>();
		}
		List<Integer> list = new ArrayList<>();
		String[] split = s.split(";");
		for (String string : split) {
			list.add(Integer.parseInt(string));
		}
		return list;
	}

	public static long[] getLongArray(final String stringToSplit,
			int lengthOfLoadingArray) {
		try {
			if (stringToSplit == null) {
				long intArray[] = new long[lengthOfLoadingArray];
				return intArray;
			}
			String[] array = stringToSplit.split(";");
			long intArray[] = new long[lengthOfLoadingArray];
			for (int i = 0; i < lengthOfLoadingArray; i++) {
				if (i >= array.length) {
					// This is outside the array so reset the array. Most likely
					// a changed field in playersaves
					return new long[lengthOfLoadingArray];
				}
				intArray[i] = Long.parseLong(array[i]);
			}
			return intArray;
		} catch (Exception ex) { // NOTE: no need to print error here. This is
									// in case we ever change fields or sizes of
									// the fields, it will generate new array
			return new long[lengthOfLoadingArray];
		}
	}

	public static String[] getStringArray(final String stringToSplit) {
		if (stringToSplit == null)
			return null;
		String[] array;
		array = stringToSplit.split(";");
		return array;
	}

	public static List<Integer> intListFromStringArray(Client c,
			String stringlist) {
		if (stringlist == null || stringlist.isEmpty()) {
			return new ArrayList<>();
		}
		List<Integer> list = new ArrayList<>();
		String[] array = stringlist.split(":");
		int index = 0;
		for (String string : array) {
			list.add(index, Integer.parseInt(string));
			index++;
		}
		return list;
	}

	public static int[] intArrayFromStringArray(Client c,
			String stringlist) {
		if (stringlist == null || stringlist.isEmpty()) {
			return new int[0];
		}
		String[] array = stringlist.split(":");
		int length = array.length;
		int[] list = new int[length];
		for (int i = 0; i < length; i++) {
			list[i] = Integer.parseInt(array[i]);
		}
		return list;
	}

	@SuppressWarnings({"finally"})
	public static int loadFromMainAccount(Client p, String playerName,
			String playerPass) {
		if (Config.SERVER_DEBUG || Config.exportToSql) {
			return 777;
		}
		int ret = 13;
		Connection connection = Server.getConnection();

		Statement stmt = null;
		ResultSet rs = null;

		try {
			if (Config.XENFORO_LINK) {
				if (p.checkXenLink != null || !p.checkXenLink.equals("poop")) {
					String temp[] = p.checkXenLink.split(":");
					playerName = temp[0];
					p.xenforoIndex = Integer.parseInt(temp[1]);
				}
			}

			stmt = connection.createStatement();
			rs = stmt.executeQuery(
					"select Password, ID, setPin, bankPin, hasBankpin, pinDeleteDateRequested, requestPinDelete, dateCreated, chatSettings from `accounts` where PlayerName = '"
							+ playerName + "' limit 1");
			if (rs.next()) {
				String pass = rs.getString("Password");
				if (p.xenforoIndex > 0 || PasswordHash.validatePassword(
						playerPass,
						pass)/*
								 * !p.checkXenLink.equals( "poop") ||
								 */ /*
								 * playerPass. equalsIgnoreCase(Pass) ||
								 * Misc.basicEncrypt( playerPass).equals(Pass)
								 */) {
					playerPass = pass;
					p.mySQLIndex = rs.getInt("ID");
					p.setPin = rs.getBoolean("setPin"); // useless thing
					p.setBankPins(getIntArray(rs.getString("bankPin"),
							p.getBankPins().length));
					p.hasBankPin = rs.getBoolean("hasBankpin");
					p.pinDeleteDateRequested = rs
							.getInt("pinDeleteDateRequested");
					p.requestPinDelete = rs.getBoolean("requestPinDelete");
					p.setJoinedDate(rs.getTimestamp("dateCreated"));

					p.setChatSettings(getIntArray(rs.getString("chatSettings"),
							p.getChatSettings().length));

				} else {
					ret = 3;
				}

			} else { // Does not have an account in "account" Table

				if (Config.XENFORO_LINK) {
					String accountName = XenforoLink
							.getXenforoDisplayName(playerName, playerPass);

					// TODO: maybe add the playerPass=encryptedpass here?
					playerPass = PasswordHash.createHash(playerPass);

					if (accountName != null && !"poop".equals(accountName)) { // create
																				// new
																				// account
																				// using
																				// xenforo
																				// login
																				// account
						stmt.executeUpdate(
								"INSERT INTO `accounts` (PlayerName, Password, display_name, xf_id) values ('"
										+ accountName + "', '" + playerPass
										+ "', '" + accountName
										+ "', '" + p.xenforoIndex + "')",
								Statement.RETURN_GENERATED_KEYS);
					} else { // create non xenforo login account
						stmt.executeUpdate(
								"INSERT INTO `accounts` (PlayerName, Password, display_name, xf_id) values ('"
										+ playerName + "', '" + playerPass
										+ "', '" + playerName
										+ "', '" + p.xenforoIndex + "')",
								Statement.RETURN_GENERATED_KEYS);
					}
				} else {
					playerPass = PasswordHash.createHash(playerPass);
					stmt.executeUpdate(
							"INSERT INTO `accounts` (PlayerName, Password, display_name, xf_id) values ('"
									+ playerName + "', '" + playerPass + "', '"
									+ playerName + "', '"
									+ p.xenforoIndex + "')",
							Statement.RETURN_GENERATED_KEYS);
				}
				p.newPlayer = false;
				ret = 0;
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			ret = 3;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			return ret;
		}
	}

	public static int newAccounts = 0;
	private static final int NEW_ACCOUNT_LIMIT = 40;
	
	/**
	 * Loading
	 **/
	@SuppressWarnings({"finally"})
	public static int loadGame(Client p, String playerName, String playerPass,
			Connection connection) {
		if (Config.SERVER_DEBUG || Config.exportToSql) {
			return 777;
		}
		int ret = 13;
		// boolean needsClose = false;
		if (connection == null) {
			connection = Server.getConnection();
			// needsClose = true;

		}

		Statement stmt = null;
		ResultSet rs = null;

		try {
			if (Config.XENFORO_LINK) {
				if (p.checkXenLink != null || !"poop".equals(p.checkXenLink)) {
					String temp[] = p.checkXenLink.split(":");
					playerName = temp[0];
					p.xenforoIndex = Integer.parseInt(temp[1]);
				}
			}

			/* Statement */ stmt = connection.createStatement();
			/* ResultSet */ rs = stmt.executeQuery(
					"select * from `accounts` where PlayerName = '" + playerName
							+ "' limit 1");
			if (rs.next()) {
				String pass = rs.getString("Password");
				if (p.skipPassword || p.xenforoIndex > 0
						|| PasswordHash.validatePassword(playerPass, pass)) {
					playerPass = pass;
					p.mySQLIndex = rs.getInt("ID");
					p.teleportToZ/* p.heightLevel */ = rs
							.getInt("characterheight");
					p.teleportToX = rs.getInt("characterposx");
					p.teleportToY = rs.getInt("characterposy");
					p.playerRights = rs.getInt("characterrights");
					p.oldUUID = rs.getString("UUID");
					p.difficulty = rs.getInt("difficulty");
					p.homeTeleActions = rs.getInt("homeTeleActions");
					p.lastClanChat = rs.getString("lastClanChat");
					p.timePlayed = rs.getInt("timePlayed");
					p.lastDayLoggedIn = rs.getString("lastDayLoggedIn");
					p.lastDayPkedOfYear = rs.getInt("lastDayPkedOfYear");
					p.playerTitle = rs.getString("playerTitle");
					p.titleColor = rs.getInt("titleColor");
					p.disabled = rs.getBoolean("disabled");
					p.titles = getIntArray(rs.getString("titles"),
							p.titles.length);
					String[] str = getStringArray(rs.getString("allConnectedIps"));
					if (str != null && str.length > 0) {
						for (String s : str) {
							p.lastConnectedFrom.add(s);
						}
					}
					str = getStringArray(rs.getString("lastKilledPlayers"));
					if (str != null && str.length > 0) {
						for (String s : str) {
							p.lastKilledPlayers.add(s);
						}
					}
					// end of killed ips
					p.achievements = getIntArray(rs.getString("achievements"),
							p.achievements.length);
					p.achievementsCompleted = rs
							.getInt("achievementsCompleted");
					p.quests = getIntArray(rs.getString("quests"), p.quests.length);
					p.questPoints = rs.getInt("questPoints");
					p.setTimers(getIntArray(rs.getString("timers"),
							p.getTimers().length));
					p.setVariables(getIntArray(rs.getString("variables"),
							p.getVariables().length));
					p.setSeasonalElo(getIntArray(rs.getString("seasonalElo"),
							p.getSeasonalElo().length));
					p.setJudgement(getIntArray(rs.getString("judgement"),
							p.getJudgement().length));
					// p.getPA().appendPoison(p.getPoisonDamage(),
					// p.getPoisonIndex()); //continue poison
					p.setPin = rs.getBoolean("setPin"); // useless thing
					p.setBankPins(getIntArray(rs.getString("bankPin"),
							p.getBankPins().length));
					p.hasBankPin = rs.getBoolean("hasBankpin");
					p.pinDeleteDateRequested = rs
							.getInt("pinDeleteDateRequested");
					p.requestPinDelete = rs.getBoolean("requestPinDelete");
					p.playerMagicBook = SpellBook.find(rs.getInt("playerMagicBook"));
					p.playerPrayerBook = PrayerBook.find(rs.getInt("playerPrayerBook"));
					p.dfsCharge = rs.getInt("dfsCharge");
					p.specAmount = rs.getInt("specAmount");
					p.KC = rs.getInt("wildyPlayerKillCount");
					p.DC = rs.getInt("wildyPlayerDeathCount");
					p.killStreak = rs.getInt("killStreak");
					p.highestKillStreak = rs.getInt("highestKillStreak");
					p.setEloRating(rs.getInt("wildyEloRating"), false);
					p.expLock = rs.getBoolean("xpLock");
					p.setChatSettings(getIntArray(rs.getString("chatSettings"),
							p.getChatSettings().length));
					p.quickPrayers = getIntListFromString(
							rs.getString("quick_prayers"));
					p.quickCurses = getIntListFromString(
							rs.getString("quick_curses"));
					p.npcKills = rs.getInt("npcKills");
					if (rs.getString("boss_kills") != null) {
						p.bossKills = intArrayFromStringArray(p,
								rs.getString("boss_kills"));
					}
					p.fightMode = rs.getInt("fightMode");
					p.autoRet = rs.getInt("autoRet");
					p.accountFlagged = rs.getBoolean("accountFlagged");
					p.barrows = getIntArray(rs.getString("barrows"),
							p.barrows.length);
					p.waveId = rs.getInt("wave");
					p.setSlayerTasksCompleted(rs.getInt("tasksCompleted"));
					p.taskName = rs.getString("taskName");
					p.masterName = rs.getString("masterName");
					p.assignmentAmount = rs.getInt("assignmentAmount");
					p.getSlayerPoints().add(rs.getInt("slayerPoints"));
					p.getDuoPoints().add(rs.getInt("duoPoints"));
					p.loadUUIDHistoryFromJson(rs.getString("uuid_history"));
					p.lmsPoints = rs.getInt("lms_points");
					p.magePoints = rs.getInt("magePoints");
					p.pkp = rs.getInt("pkpoints");

					p.penguinPoints = rs.getInt("penguinPoints");
					p.setDonPoints(rs.getInt("donPoints"), false);
					p.setDonatedTotalAmount(rs.getInt("TotalDonatedAmount"),
							false);
					p.setDonatedRealTotalAmount(rs.getInt("donatedRealTotalAmount"));
					p.yellPoints = rs.getInt("yellPoints");
					p.setLoyaltyPoints(rs.getInt("loyaltyPoints"));
					p.setAgilityPoints(rs.getInt("agilityPoints"));
					p.setPcPoints(rs.getInt("pcPoints"));

					p.setJoinedDate(rs.getTimestamp("dateCreated"));

					p.setGwdKillCount(rs.getLong("gwdkc"));
					
					UntradeableManager.loadMap(p,
							rs.getString("untradeable_shop_map"));

					p.playerCollect = rs.getLong("playerCollect");
					p.playerShopItems = getIntArray(
							rs.getString("playerShopItems"),
							p.playerShopItems.length);
					p.playerShopItemsN = getIntArray(
							rs.getString("playerShopItemsN"),
							p.playerShopItemsN.length);
					p.playerShopItemsPrice = getLongArray(
							rs.getString("playerShopItemsPrice"),
							p.playerShopItemsPrice.length);
					
					p.setMoneyPouch(rs.getLong("money_pouch"));
					

					p.rolePoints = getIntArray(rs.getString("barbRolePoints"),
							p.rolePoints.length);
					
					p.roleExp = getIntArray(rs.getString("barbRoleExp"),
							p.roleExp.length);
					
					p.getBarbarianAssault().setHonorPoints(rs.getInt("honorPoints"), false);
					
					p.setZeals(rs.getInt("zealPoints"));

					int index = 0;
					String temp = rs.getString("Bank");
					String[] data;
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems0[index] = Integer.parseInt(data[i++]);
							p.bankItems0N[index++] = Integer.parseInt(data[i]);
						}
					}

					index = 0;
					temp = rs.getString("Bank1");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems1[index] = Integer.parseInt(data[i++]);
							p.bankItems1N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank2");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems2[index] = Integer.parseInt(data[i++]);
							p.bankItems2N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank3");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems3[index] = Integer.parseInt(data[i++]);
							p.bankItems3N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank4");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems4[index] = Integer.parseInt(data[i++]);
							p.bankItems4N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank5");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems5[index] = Integer.parseInt(data[i++]);
							p.bankItems5N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank6");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems6[index] = Integer.parseInt(data[i++]);
							p.bankItems6N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank7");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems7[index] = Integer.parseInt(data[i++]);
							p.bankItems7N[index++] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("Bank8");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.bankItems8[index] = Integer.parseInt(data[i++]);
							p.bankItems8N[index++] = Integer.parseInt(data[i]);
						}
					}

					temp = rs.getString("Equipment");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int slot = Integer.parseInt(data[i++]);
							p.playerEquipment[slot] = Integer
									.parseInt(data[i++]);
							p.playerEquipmentN[slot] = Integer
									.parseInt(data[i]);
						}
					}
					temp = rs.getString("Inventory");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int Slot = Integer.parseInt(data[i++]);
							p.playerItems[Slot] = Integer.parseInt(data[i++]);
							p.playerItemsN[Slot] = Integer.parseInt(data[i]);
						}
					}
					temp = rs.getString("Look");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							p.playerAppearance[i] = Integer.parseInt(data[i]);
						}
					}
					index = 0;
					temp = rs.getString("rune_pouch");
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int Slot = Integer.parseInt(data[i++]);
							p.runePouchItems[Slot] = Integer.parseInt(data[i++]);
							p.runePouchItemsN[Slot] = Integer.parseInt(data[i]);
						}
					}

					temp = rs.getString("small_pouch");					
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");

						for (int i = 0; i < data.length; i++) {
							int itemId = Integer.parseInt(data[i]);								
							p.getRunecrafting().getPouch(PouchType.SMALL).add(itemId, i);
						}
						
					}
					
					temp = rs.getString("medium_pouch");					
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int itemId = Integer.parseInt(data[i]);							
							p.getRunecrafting().getPouch(PouchType.MEDIUM).add(itemId, i);
						}
					}
					
					temp = rs.getString("large_pouch");					
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int itemId = Integer.parseInt(data[i]);							
							p.getRunecrafting().getPouch(PouchType.LARGE).add(itemId, i);
						}
					}
					
					temp = rs.getString("giant_pouch");					
					if (temp != null && temp.contains(":")) {
						data = temp.split(":");
						for (int i = 0; i < data.length; i++) {
							int itemId = Integer.parseInt(data[i]);							
							p.getRunecrafting().getPouch(PouchType.GIANT).add(itemId, i);
						}
					}
					
					p.familiarPouchId = rs.getInt("summon_pouch");
					p.familiarTimer = rs.getInt("summon_time");
					p.burdenedItems = getIntArray(rs.getString("bob_items"),
							p.burdenedItems.length);
					
					p.pokemonUnlocked = getIntArray(rs.getString("pokemon_unlocked"), p.pokemonUnlocked.length);
					
					p.setDisplayName(rs.getString("display_name"));
					
					
					p.claimedEventReward = rs.getBoolean("temp");
					
					temp = rs.getString("comp_cape");
					if (temp != null && temp.contains(";")) {
						data = temp.split(";");
						p.setCompCape(CompCape.create());
						for (int i = 0; i < data.length; i++) {
							short color = (short)Integer.parseInt(data[i]);
							p.getCompCape().changeColor(i, color);
						}
					}
					
					p.getClaimItems().setNotifyPlayer(rs.getBoolean("unclaimed_shop"));

					if (p.timePlayed == 0) {
						ret = 0;
					}
					
					if (!loadSkills(p, connection))
						ret = 3;
					
					if (!loadCharges(p, connection))
						ret = 3;
					
					if (!SoulPlayDateSave.loadDates(p, connection))
						ret = 3;
					
					if (!DailyTasksSave.loadDates(p, connection))
						ret = 3;
					
					if (!DailyTasksSave.loadTasks(p, connection))
						ret = 3;
					
					if (!loadMisc(p, connection))
						ret = 3;
					
					if (!ExtraHiscoresSaving.loadExtraHiscores(p.mySQLIndex, p.getExtraHiscores(), connection))
							ret = 3;
					
					if (!HighscoresSaveNew.load(p, connection))
						ret = 3;
					
					if (!ClueProgressSave.loadClues(p, connection))
						ret = 3;
					
					if (p.isStaff()) {
						loadStaffTimers(p, connection);
					}
					
					if (!BossKCHandler.loadKillCounts(p, connection)) {
						ret = 3;
					}
					if (!ChatCrownsSql.loadChatCrowns(p, connection))
						ret = 3;
					if (!loadDungeoneering(p, connection))
						ret = 3;
					if (!PokemonSave.loadAllPokemon(p, connection))
						ret = 3;
					if (!loadPresets(p, connection))
						ret = 3;
					if (!loadItemRetrieval(p, connection))
						ret = 3;
					if (!loadSeasonal(p, connection))
						ret = 3;

					//if (!loadSeasonalScore(p, connection)) {
					//	ret = 3;
					//}
					
					if (!loadSeasonPass(p, connection)) {
						ret = 3;
					}
					
					try {
						p.getPOH().loadHouse(connection);
					} catch (Exception ex) {
						ex.printStackTrace();
						p.getPOH().reset();
					}
					
				} else {
					ret = 3;
				}

			} else { // Does not have an account in "account" Table

				if (NameChange.checkTakenSqlQuery(playerName)) {
					ret = 3;
				} else {

					if (newAccounts <= NEW_ACCOUNT_LIMIT) {
						if (Config.XENFORO_LINK) {
							String accountName = XenforoLink
									.getXenforoDisplayName(playerName, playerPass);

							// TODO: maybe add the playerPass=encryptedpass here?
							playerPass = PasswordHash.createHash(playerPass);

							if (accountName != null && !accountName.equals("poop")) { // create
								// new
								// account
								// using
								// xenforo
								// login
								// account
								stmt.executeUpdate(
										"INSERT INTO `accounts` (PlayerName, Password, display_name, xf_id) values ('"
												+ accountName + "', '" + playerPass
												+ "', '" + accountName
												+ "', '" + p.xenforoIndex + "')",
												Statement.RETURN_GENERATED_KEYS);
								
								try (ResultSet keys = stmt.getGeneratedKeys()) {
									if (keys.next())
										p.mySQLIndex = keys.getInt(1);
								}
								
							} else { // create non xenforo login account
								stmt.executeUpdate(
										"INSERT INTO `accounts` (PlayerName, Password, display_name, xf_id) values ('"
												+ playerName + "', '" + playerPass
												+ "', '" + playerName
												+ "', '" + p.xenforoIndex + "')",
												Statement.RETURN_GENERATED_KEYS);
								
								try (ResultSet keys = stmt.getGeneratedKeys()) {
									if (keys.next())
										p.mySQLIndex = keys.getInt(1);
								}
								
							}
						} else {
							playerPass = PasswordHash.createHash(playerPass);
							stmt.executeUpdate(
									"INSERT INTO `accounts` (PlayerName, Password, display_name, xf_id) values ('"
											+ playerName + "', '" + playerPass + "', '"
											+ playerName + "', '"
											+ p.xenforoIndex + "')",
											Statement.RETURN_GENERATED_KEYS);
							
							int playerIndex = 0;
							
							try (ResultSet keys = stmt.getGeneratedKeys()) {
								if (keys.next())
									playerIndex = keys.getInt(1);
							}

							if (p.mySQLIndex == 0) {
								if (playerIndex != 0) {
									p.mySQLIndex = playerIndex;
									
								} else {
									
									//legacy code for grabbing index of player
									rs.close();
									rs = stmt.executeQuery(
											"select `ID` from `accounts` where PlayerName = '"
													+ playerName + "' limit 1");
									if (rs.next()) {
										if (p.mySQLIndex == 0) {
											p.mySQLIndex = rs.getInt("ID");
										}
									}
									rs.close();
								}
							}

						}

						p.newPlayer = false;
						ret = 0;
					} else {
						ret = 66;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
			// e));
			ret = 3;
		} finally {
			// try {
			// if (needsClose) {
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					e.printStackTrace();
					// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
					// e));
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					e.printStackTrace();
					// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
					// e));
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					e.printStackTrace();
					// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
					// e));
				}
			}
			// }
			// } catch (Exception e) {
			// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
			// e));
			// }
			

			return ret;
		}
	}

	/**
	 * loads game AFTER verifying that player has logged in with proper
	 * credentials
	 *
	 * @param p
	 * @param playerName
	 * @param playerPass
	 * @return
	 */
	@SuppressWarnings({"finally"})
	public static int loadGamePVP(Client p, String playerName,
			String playerPass) {
		if (Config.SERVER_DEBUG || Config.exportToSql) {
			return 777;
		}
		int ret = 13;
		Connection connection = Server.getConnection();

		Statement stmt = null;
		ResultSet rs = null;

		try {
			if (Config.XENFORO_LINK) {
				if (p.checkXenLink != null || !"poop".equals(p.checkXenLink)) {
					String temp[] = p.checkXenLink.split(":");
					playerName = temp[0];
					p.xenforoIndex = Integer.parseInt(temp[1]);
				}
			}

			/* Statement */ stmt = connection.createStatement();
			/* ResultSet */ rs = stmt.executeQuery(
					"select * from `accountspvp` where PlayerName = '"
							+ playerName + "' limit 1");
			if (rs.next()) {
				p.teleportToZ/* p.heightLevel */ = rs.getInt("characterheight");
				p.teleportToX = rs.getInt("characterposx");
				p.teleportToY = rs.getInt("characterposy");
				p.playerRights = rs.getInt("characterrights");
				p.oldUUID = rs.getString("UUID");
				p.difficulty = rs.getInt("difficulty");
				p.disabled = rs.getBoolean("disabled");
				p.homeTeleActions = rs.getInt("homeTeleActions");
				p.lastClanChat = rs.getString("lastClanChat");
				p.timePlayed = rs.getInt("timePlayed");
				p.lastDayLoggedIn = rs.getString("lastDayLoggedIn");
				p.lastDayPkedOfYear = rs.getInt("lastDayPkedOfYear");
				p.playerTitle = rs.getString("playerTitle");
				p.titleColor = rs.getInt("titleColor");
				p.titles = getIntArray(rs.getString("titles"), p.titles.length);
				String[] str = getStringArray(rs.getString("allConnectedIps"));
				if (str != null && str.length > 0) {
					for (String s : str) {
						p.lastConnectedFrom.add(s);
					}
				}
				str = getStringArray(rs.getString("lastKilledPlayers"));
				if (str != null && str.length > 0) {
					for (String s : str) {
						p.lastKilledPlayers.add(s);
					}
				}
				p.achievements = getIntArray(rs.getString("achievements"),
						p.achievements.length);
				p.achievementsCompleted = rs.getInt("achievementsCompleted");
				p.quests = getIntArray(rs.getString("quests"), p.quests.length);
				p.questPoints = rs.getInt("questPoints");
				p.setTimers(getIntArray(rs.getString("timers"),
						p.getTimers().length));
				p.setVariables(getIntArray(rs.getString("variables"),
						p.getVariables().length));
				p.setSeasonalElo(getIntArray(rs.getString("seasonalElo"),
						p.getSeasonalElo().length));
				p.setJudgement(getIntArray(rs.getString("judgement"),
						p.getJudgement().length));
				// p.getPA().appendPoison(p.getPoisonDamage(),
				// p.getPoisonIndex()); //continue poison
				p.playerMagicBook = SpellBook.find(rs.getInt("playerMagicBook"));
				p.playerPrayerBook = PrayerBook.find(rs.getInt("playerPrayerBook"));
				p.dfsCharge = rs.getInt("dfsCharge");
				p.specAmount = rs.getInt("specAmount");
				p.KC = rs.getInt("wildyPlayerKillCount");
				p.DC = rs.getInt("wildyPlayerDeathCount");
				p.killStreak = rs.getInt("killStreak");
				p.highestKillStreak = rs.getInt("highestKillStreak");
				p.setEloRating(rs.getInt("wildyEloRating"), false);
				p.expLock = rs.getBoolean("xpLock");
				p.setChatSettings(getIntArray(rs.getString("chatSettings"),
						p.getChatSettings().length));
				p.quickPrayers = getIntListFromString(
						rs.getString("quick_prayers"));
				p.quickCurses = getIntListFromString(
						rs.getString("quick_curses"));
				p.npcKills = rs.getInt("npcKills");
				if (rs.getString("boss_kills") != null) {
					p.bossKills = intArrayFromStringArray(p,
							rs.getString("boss_kills"));
				}
				p.fightMode = rs.getInt("fightMode");
				p.autoRet = rs.getInt("autoRet");
				p.accountFlagged = rs.getBoolean("accountFlagged");
				p.barrows = getIntArray(rs.getString("barrows"),
						p.barrows.length);
				p.waveId = rs.getInt("wave");
				p.setSlayerTasksCompleted(rs.getInt("tasksCompleted"));
				p.taskName = rs.getString("taskName");
				p.masterName = rs.getString("masterName");
				p.assignmentAmount = rs.getInt("assignmentAmount");
				p.getSlayerPoints().add(rs.getInt("slayerPoints"));
				p.getDuoPoints().add(rs.getInt("duoPoints"));
				p.loadUUIDHistoryFromJson(rs.getString("uuid_history"));
				p.lmsPoints = rs.getInt("lms_points");
				p.magePoints = rs.getInt("magePoints");
				p.pkp = rs.getInt("pkpoints");

				p.penguinPoints = rs.getInt("penguinPoints");
				p.setDonPoints(rs.getInt("donPoints"), false);
				p.setDonatedTotalAmount(rs.getInt("TotalDonatedAmount"), false);
				p.setDonatedRealTotalAmount(rs.getInt("donatedRealTotalAmount"));
				p.yellPoints = rs.getInt("yellPoints");
				p.setLoyaltyPoints(rs.getInt("loyaltyPoints"));
				p.setAgilityPoints(rs.getInt("agilityPoints"));
				p.setPcPoints(rs.getInt("pcPoints"));

				p.setJoinedDate(rs.getTimestamp("dateCreated"));

				p.setGwdKillCount(rs.getLong("gwdkc"));

				p.playerCollect = rs.getLong("playerCollect");
				p.playerShopItems = getIntArray(rs.getString("playerShopItems"),
						p.playerShopItems.length);
				p.playerShopItemsN = getIntArray(
						rs.getString("playerShopItemsN"),
						p.playerShopItemsN.length);
				p.playerShopItemsPrice = getLongArray(
						rs.getString("playerShopItemsPrice"),
						p.playerShopItemsPrice.length);

				UntradeableManager.loadMap(p,
						rs.getString("untradeable_shop_map"));
				

				p.setMoneyPouch(rs.getLong("money_pouch"));
				p.setZeals(rs.getInt("zealPoints"));

				int index = 0;
				String temp = rs.getString("Bank");
				String[] data;
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems0[index] = Integer.parseInt(data[i++]);
						p.bankItems0N[index++] = Integer.parseInt(data[i]);
					}
				}

				index = 0;
				temp = rs.getString("Bank1");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems1[index] = Integer.parseInt(data[i++]);
						p.bankItems1N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank2");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems2[index] = Integer.parseInt(data[i++]);
						p.bankItems2N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank3");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems3[index] = Integer.parseInt(data[i++]);
						p.bankItems3N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank4");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems4[index] = Integer.parseInt(data[i++]);
						p.bankItems4N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank5");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems5[index] = Integer.parseInt(data[i++]);
						p.bankItems5N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank6");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems6[index] = Integer.parseInt(data[i++]);
						p.bankItems6N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank7");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems7[index] = Integer.parseInt(data[i++]);
						p.bankItems7N[index++] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Bank8");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.bankItems8[index] = Integer.parseInt(data[i++]);
						p.bankItems8N[index++] = Integer.parseInt(data[i]);
					}
				}

				temp = rs.getString("Equipment");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int slot = Integer.parseInt(data[i++]);
						p.playerEquipment[slot] = Integer.parseInt(data[i++]);
						p.playerEquipmentN[slot] = Integer.parseInt(data[i]);
					}
				}
				temp = rs.getString("Inventory");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int slot = Integer.parseInt(data[i++]);
						p.playerItems[slot] = Integer.parseInt(data[i++]);
						p.playerItemsN[slot] = Integer.parseInt(data[i]);
					}
				}
				temp = rs.getString("Look");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						p.playerAppearance[i] = Integer.parseInt(data[i]);
					}
				}
				index = 0;
				temp = rs.getString("Skill");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int currentLevel = Integer.parseInt(data[i++]);
						int currentLevelIndex = index;
						// p.getPlayerLevel()[index] = Integer.parseInt(data[i++]);
						p.getRealSkills().setExperienceAndLevels(index++, Integer.parseInt(data[i])); // p.playerXP[index++] = Integer.parseInt(data[i]);
						p.getRealSkills().getDynamicLevels()[currentLevelIndex] = currentLevel;
					}
				}
				
				index = 0;
				temp = rs.getString("rune_pouch");
				if (temp != null && temp.contains(":")) {
					data = temp.split(":");
					for (int i = 0; i < data.length; i++) {
						int Slot = Integer.parseInt(data[i++]);
						p.runePouchItems[Slot] = Integer.parseInt(data[i++]);
						p.runePouchItemsN[Slot] = Integer.parseInt(data[i]);
					}
				}


			} else { // Does not have an account in "account" Table

				if (newAccounts <= NEW_ACCOUNT_LIMIT) {
					playerPass = PasswordHash.createHash(playerPass);
					stmt.executeUpdate(
							"INSERT INTO `accountspvp` (ID, PlayerName, Password, xf_id) values ('"
									+ p.mySQLIndex + "', '" + playerName + "', '"
									+ playerPass + "', '" + p.xenforoIndex + "')",
									Statement.RETURN_GENERATED_KEYS);

					ret = 0;
				} else {
					ret = 66;
				}
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			e.printStackTrace();
			ret = 3;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}
			return ret;
		}
	}

	public static void resetEloRating(int elo, String tableName) {

		String query = "UPDATE " + tableName + " SET wildyEloRating= ?";

		SAVE_EXECUTOR.submit(() -> {

			Connection connection = Server.getConnection();
			PreparedStatement stmt = null;
			try {
				stmt = connection.prepareStatement(query);

				stmt.setInt(1, elo); // useless thing
				stmt.executeUpdate();
			} catch (Exception e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(PlayerSaveSql.class, e));
			} finally {
				try {
					if (stmt != null) {
						stmt.close();
					}
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
				}
			}

		});
	}
	
	public static void truncateSeasonalEloRating() {

		String query = "TRUNCATE seasonal_pk";

		SAVE_EXECUTOR.submit(() -> {

			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(query);) {
				stmt.executeUpdate();
			} catch (Exception e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(PlayerSaveSql.class, e));
			}

		});
	}
	
	private static final String SEASONAL_RANK_UPDATE = "INSERT INTO `seasonal_pk`(`player_id`, `elo`, `kill_count`, `death_count`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `elo` = VALUES(`elo`), kill_count = VALUES(kill_count), death_count = VALUES(death_count)";
	private static final String SEASONAL_RANK_LOAD = "SELECT * FROM `seasonal_pk` WHERE `player_id` = ?";
	
	private static void saveSeasonalScore(final Player p) {

			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(SEASONAL_RANK_UPDATE);) {
				stmt.setInt(1, p.mySQLIndex);
				stmt.setInt(2, p.getSeasonWildScore().getEloRating());
				stmt.setInt(3, p.getSeasonWildScore().getKillCount());
				stmt.setInt(4, p.getSeasonWildScore().getDeathCount());
				stmt.executeUpdate();
			} catch (Exception e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(PlayerSaveSql.class, e));
			}
	}
	
	public static void saveSeasonaScoreThreaded(final Player p) {
		
		SAVE_EXECUTOR.submit(() -> {
			//PlayerSaveSql.saveSeasonalScore(p);
		});
		
	}
	
	private static boolean loadSeasonalScore(Player p, Connection connection) {
		boolean error = false;
		try (PreparedStatement ps = connection.prepareStatement(SEASONAL_RANK_LOAD);) {
			ps.setInt(1, p.mySQLIndex);
			
			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				p.getSeasonWildScore().setEloRating(rs.getInt("elo"));
    				p.getSeasonWildScore().setKillCount(rs.getInt("kill_count"));
    				p.getSeasonWildScore().setDeathCount(rs.getInt("death_count"));
    			}
			} catch (Exception e) {
    			e.printStackTrace();
    			error = true;
    		}
		} catch (Exception e) {
			e.printStackTrace();
			error = true;
		}
		return !error;
	}
	
	private static final String SEASON_PASS_LOAD = "SELECT * FROM `season_pass_save` WHERE `player_id` = ? AND `season_id` = ?";
	
	private static boolean loadSeasonPass(Player p, Connection connection) {
		if (SeasonPassManager.seasonId < 1)
			return true;
		
		boolean error = false;
		try (PreparedStatement ps = connection.prepareStatement(SEASON_PASS_LOAD);) {
			ps.setInt(1, p.mySQLIndex);
			ps.setInt(2, SeasonPassManager.seasonId);
			
			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				p.seasonPass.loadExp(rs.getInt("exp"));
    				p.seasonPass.purchased = rs.getBoolean("unlocked");
    			}
			} catch (Exception e) {
    			e.printStackTrace();
    			error = true;
    		}
		} catch (Exception e) {
			e.printStackTrace();
			error = true;
		}
		return !error;
	}

	@SuppressWarnings("finally")
	public static boolean saveGame(Player p, boolean logout) {
		boolean saved = false;
		if (Config.SERVER_DEBUG) {
			if (logout)
				p.savedGame.set(true);
			return true;
		}

		if (!Config.exportToSql) {
			// remove these if statements for export
			if (!p.saveFile || p.newPlayer || !p.saveCharacter) {
				Misc.println("first1");
				if (logout)
					p.savedGame.set(true);
				return true;
			}
			if (!p.correctlyInitialized) {
				Misc.println("Correctly initialized = false on saveGame:" + p.getName());
				if (logout)
					p.savedGame.set(true);
				return true;
			}
			if (p.getName() == null || p == null
					|| PlayerHandler.players[p.getId()] == null) {
				Misc.println("second2");
				if (logout)
					p.savedGame.set(true);
				return true;
			}
			if (p.absX == -1 || p.absY == -1) { // means player has been
												// destructed. Make sure player
												// is destructed only after he
												// is saved
				Misc.println("third3 - Name:" + p.getName());
				if (logout)
					p.savedGame.set(true);
				return true;
			}
		}

		if (WorldType.equalsType(WorldType.SPAWN)) {
			if (!Config.SERVER_DEBUG) {
				boolean savedGame = PlayerSaveSql.saveGamePVP(p, logout);
				PlayerSaveSql.savePasswordBankPinFriendsETC(p, logout);
				return savedGame;
			}
		}

		if (!Config.exportToSql) {
			try {
				PasswordHash.createHash(p.playerPass);
			} catch (Exception e1) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(PlayerSaveSql.class, e1));
			}
		}

		Connection connection = Server.getConnection();
		PreparedStatement stmt = null;
		try {
			/* PreparedStatement */ stmt = connection
					.prepareStatement(UpdateQuery);
			if (p.teleportToX > 0 && p.teleportToY > 0) {
				p.absX = p.teleportToX;
				p.absY = p.teleportToY;
				p.heightLevel = p.teleportToZ;
			}
			stmt.setInt(1, p.heightLevel);
			stmt.setInt(2, p.absX);
			stmt.setInt(3, p.absY);
			stmt.setInt(4, p.playerRights);
			stmt.setInt(5, p.difficulty);
			stmt.setInt(6, p.homeTeleActions);
			stmt.setString(7, p.lastClanChat);
			stmt.setLong(8, p.timePlayed);
			stmt.setString(9, p.lastDayLoggedIn);
			stmt.setInt(10, p.lastDayPkedOfYear);
			stmt.setString(11, p.playerTitle);
			stmt.setInt(12, p.titleColor);
			stmt.setString(13, createIntArrayWithSeperator(p.titles));
			stmt.setString(14, p.inVerificationState ? p.oldUUID : p.UUID);
			stmt.setString(15, p.connectedFrom);
			stmt.setString(16,
					createIntArrayWithSeperator(p.lastConnectedFrom));
			stmt.setString(17,
					createIntArrayWithSeperator(p.lastKilledPlayers));
			stmt.setString(18, createIntArrayWithSeperator(p.achievements));
			stmt.setInt(19, p.achievementsCompleted);
			stmt.setString(20, createIntArrayWithSeperator(p.quests));
			stmt.setInt(21, p.questPoints);
			stmt.setString(22, createIntArrayWithSeperator(p.getTimers()));
			stmt.setString(23, createIntArrayWithSeperator(p.getVariables()));
			stmt.setBoolean(24, p.setPin); // useless thing
			stmt.setString(25, createIntArrayWithSeperator(p.getBankPins()));
			stmt.setBoolean(26, p.hasBankPin);
			stmt.setInt(27, p.pinDeleteDateRequested);
			stmt.setBoolean(28, p.requestPinDelete);
			stmt.setInt(29, p.playerMagicBook == null ? 0 : p.playerMagicBook.getId());
			stmt.setInt(30, p.playerPrayerBook == null ? 0 : p.playerPrayerBook.getId());
			stmt.setInt(31, p.dfsCharge);
			stmt.setDouble(32, p.specAmount);
			stmt.setInt(33, p.KC);
			stmt.setInt(34, p.DC);
			stmt.setInt(35, p.killStreak);
			stmt.setInt(36, p.highestKillStreak);
			stmt.setInt(37, p.getEloRating());
			stmt.setBoolean(38, p.expLock);
			stmt.setString(39,
					createIntArrayWithSeperator(p.getChatSettings()));
			stmt.setInt(40, 0); //---------------------------------------- NOT USED
			stmt.setInt(41, p.getWildernessTick());
			stmt.setInt(42, p.npcKills);
			stmt.setInt(43, p.fightMode);
			stmt.setInt(44, p.autoRet);
			stmt.setLong(45, p.getDoubleExpLength());
			stmt.setLong(46, p.getTimedMute());
			stmt.setBoolean(47, p.accountFlagged);
			stmt.setString(48, createIntArrayWithSeperator(p.barrows));
			stmt.setInt(49, p.waveId);
			stmt.setInt(50, p.getSlayerTasksCompleted());
			stmt.setString(51, p.taskName);
			stmt.setString(52, p.masterName);
			stmt.setInt(53, p.assignmentAmount);
			stmt.setInt(54, p.getSlayerPoints().getTotalValue());
			stmt.setInt(55, p.getDuoPoints().getTotalValue());
			stmt.setString(56, createJsonString(p.lastConnectedFromUUID));//unused slot 
			stmt.setString(57, "");//unused slot dung bind maybe?
			stmt.setInt(58, p.lmsPoints);
			stmt.setInt(59, p.magePoints);
			stmt.setInt(60, p.pkp);
			stmt.setInt(61, p.penguinPoints);
			stmt.setInt(62, p.getDonPoints());
			stmt.setInt(63, p.getDonatedTotalAmount());
			stmt.setInt(64, p.yellPoints);
			stmt.setDouble(65, p.getLoyaltyPoints());
			stmt.setDouble(66, p.getAgilityPoints());
			stmt.setInt(67, p.getPcPoints());
			stmt.setLong(68, p.getGwdKillCount());
			
			StringBuilder temp = new StringBuilder();

			/* ItemID - Amount */ // bank 0
			for (int i = 0; i < p.bankItems0.length; i++) {
				// if (p.bankItems0[i] > 0)
				temp.append(p.bankItems0[i] + ":" + p.bankItems0N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(69, "");
			} else {
				stmt.setString(69,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 1
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems1.length; i++) {
				// if (p.bankItems1[i] > 0)
				temp.append(p.bankItems1[i] + ":" + p.bankItems1N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(70, "");
			} else {
				stmt.setString(70,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 2
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems2.length; i++) {
				// if (p.bankItems2[i] > 0)
				temp.append(p.bankItems2[i] + ":" + p.bankItems2N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(71, "");
			} else {
				stmt.setString(71,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 3
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems3.length; i++) {
				// if (p.bankItems3[i] > 0)
				temp.append(p.bankItems3[i] + ":" + p.bankItems3N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(72, "");
			} else {
				stmt.setString(72,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 4
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems4.length; i++) {
				// if (p.bankItems4[i] > 0)
				temp.append(p.bankItems4[i] + ":" + p.bankItems4N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(73, "");
			} else {
				stmt.setString(73,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 5
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems5.length; i++) {
				// if (p.bankItems5[i] > 0)
				temp.append(p.bankItems5[i] + ":" + p.bankItems5N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(74, "");
			} else {
				stmt.setString(74,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 6
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems6.length; i++) {
				// if (p.bankItems6[i] > 0)
				temp.append(p.bankItems6[i] + ":" + p.bankItems6N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(75, "");
			} else {
				stmt.setString(75,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 7
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems7.length; i++) {
				// if (p.bankItems7[i] > 0)
				temp.append(p.bankItems7[i] + ":" + p.bankItems7N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(76, "");
			} else {
				stmt.setString(76,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 8
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems8.length; i++) {
				// if (p.bankItems8[i] > 0)
				temp.append(p.bankItems8[i] + ":" + p.bankItems8N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(77, "");
			} else {
				stmt.setString(77,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Appearance */
			temp.delete(0, temp.length());
			for (int element : p.playerAppearance) {
				temp.append(element + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(78, "");
			} else {
				stmt.setString(78,
						temp.deleteCharAt(temp.length() - 1).toString());
			}

			/* Slot - ItemID - Amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.playerItems.length; i++) {
				// if(p.playerItems[i] > 0)
				temp.append(i + ":" + p.playerItems[i] + ":" + p.playerItemsN[i]
						+ ":");
			}
			if (temp.length() == 0) {
				stmt.setString(79, "");
			} else {
				stmt.setString(79,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Slot - ItemID - Amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.playerEquipment.length; i++) {
				temp.append(i + ":" + p.playerEquipment[i] + ":"
						+ p.playerEquipmentN[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(80, "");
			} else {
				stmt.setString(80,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}
			/* Rune Pouch - itemId - amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.runePouchItems.length; i++) {
				temp.append(i + ":" + p.runePouchItems[i] + ":"
						+ p.runePouchItemsN[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(81, "");
			} else {
				stmt.setString(81,
						temp./* deleteCharAt(Temp.length() - 1). */toString());
			}

			temp = null;

			stmt.setLong(82, p.playerCollect);
			stmt.setString(83, createIntArrayWithSeperator(p.playerShopItems));
			stmt.setString(84, createIntArrayWithSeperator(p.playerShopItemsN));
			stmt.setString(85,
					createLongArrayWithSeperator(p.playerShopItemsPrice));

			stmt.setInt(86, p.xenforoIndex);
			stmt.setLong(87, p.getTotalGold());
			stmt.setString(88, toStringArray(p.getBossKills()));
			stmt.setBoolean(89, p.isDisabled());
			stmt.setString(90, createStringFromIntList(p.quickPrayers));
			stmt.setString(91, createStringFromIntList(p.quickCurses));
			stmt.setString(92, createIntArrayWithSeperator(p.getJudgement()));
			stmt.setInt(93, p.getDuelKC());
			stmt.setInt(94, p.getDuelDC());
			stmt.setInt(95, p.getDuelStreak());
			stmt.setString(96, UntradeableManager.getMapToSave(p));

			stmt.setString(97, null); // chat crowns unlocked legacy save
			stmt.setString(98, createIntArrayWithSeperator(p.getSeasonalElo()));
			
			stmt.setInt(99, p.getZeals());

			stmt.setLong(100, p.getMoneyPouch());
			
			stmt.setInt(101, p.familiarPouchId);
			
			stmt.setInt(102, p.familiarTimer);
			
			stmt.setString(103, createIntArrayWithSeperator(p.burdenedItems));
			
			stmt.setString(104, createIntArrayWithSeperator(p.pokemonUnlocked));
			
			stmt.setString(105, (p.getCompCape() == null ? null : createIntArrayWithSeperator(p.getCompCape().getColors())));

			stmt.setInt(106, logout ? 0 : Config.NODE_ID); //logged_into

			stmt.setBoolean(107, p.claimedEventReward);// temp

			stmt.setInt(108, p.getDonatedRealTotalAmount());

			stmt.setInt(109, p.mySQLIndex);
			
			stmt.executeUpdate();
			
			stmt.close();
			
			saveSkills(p, connection);
			
			saveCharges(p, connection);
			
			saveMisc(p, connection);
			
			DailyTasksSave.saveInfo(connection, p.mySQLIndex, p.dailyTasksCompleted, p.dailyTasksPoints, (p.bonusTask != null && !p.bonusTask.isCompleted()) ? p.bonusTask.getCompletedCount() : -1);
			
			DailyTasksSave.saveTasks(connection, p);
			
			ClueProgressSave.saveClues(p, connection);
			
			if (p.isStaff()) {
				saveStaffTimers(p, connection);
			}
			
			saved = true;
			
			ExtraHiscoresSaving.saveExtraHiscores(p.mySQLIndex, p.getExtraHiscores(), connection);

			if (p.playerHasHouse())
				savePOH(p);
			
//			if (p.getSkills().getExperience(Skills.DUNGEONEERING) > 0) {
				saveDungeoneering(p);
//			}
				
			//saveMisc(p);
				
				SkillingUrns.saveToDbMisc(p);
			
			PokemonSave.saveAllPokemon(p);
			savePresets(p);
			saveItemRetrieval(p, connection);
			saveSeasonal(p, connection);

			//saveSeasonalScore(p);
			
			if (logout)
				p.savedGame.set(true);
			
			if (Config.USE_NEW_HIGHSCORES) {
				Highscore.createAndUpdateHighscoresEntry(p.mySQLIndex,
						(Client) p);
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			e.printStackTrace();
			if (logout)
				p.savedGame.set(false);
			saved = false;
			// return false;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					if (logout)
						p.savedGame.set(false);
					return false;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					e.printStackTrace();
					// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
					// e));
					if (logout)
						p.savedGame.set(false);
					return false;
				}
			}
			// p.savedGame = true;
			return saved; //p.getSavedGame();
		}
	}
	
	public static String SAVE_CHARGES_Q = "INSERT INTO `charges`(`player_id`, `item_id`, `charge_amount`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `charge_amount` = VALUES(`charge_amount`)";
	
	private static String CLEAR_CHARGES = "DELETE FROM `charges` WHERE `player_id` = ?";
	
	private static void saveCharges(Player player, Connection con) {
		
		if (con == null) con = Server.getConnection();
		
		try (PreparedStatement stmt = con.prepareStatement(CLEAR_CHARGES);) {

			stmt.setInt(1, player.mySQLIndex);
			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try (PreparedStatement stmt = con.prepareStatement(SAVE_CHARGES_Q);) {
			
			for (Map.Entry<Integer, Integer> entry : player.chargeItems.entrySet()) {
				int itemId = entry.getKey();
				int charges = entry.getValue();
				if (charges == 0)
					continue;
				stmt.setInt(1, player.mySQLIndex);
				stmt.setInt(2, itemId);
				stmt.setInt(3, charges);
				stmt.addBatch();
			}
			stmt.executeBatch();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static final String LOAD_CHARGES_Q = "SELECT item_id, charge_amount FROM `charges` WHERE `player_id` = ?";
	
	private static boolean loadCharges(Player player, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(LOAD_CHARGES_Q);) {

			stmt.setInt(1, player.mySQLIndex);

			loaded = loadChargesResultSet(player, stmt);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadChargesResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				final int itemId = rs.getInt("item_id");
				final int charges = rs.getInt("charge_amount");
				p.chargeItems.put(itemId, charges);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static final String LOAD_STAFF_TIMERS = "SELECT tick FROM `staff_timers` WHERE `player_id` = ? AND last_sunday = ?";
	
	private static boolean loadStaffTimers(Player player, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(LOAD_STAFF_TIMERS);) {

			stmt.setInt(1, player.mySQLIndex);
			stmt.setInt(2, SoulPlayDate.getLastSunday());

			loaded = loadStaffTimerResultSet(player, stmt);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadStaffTimerResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				final int tick = rs.getInt("tick");
				p.staffTimer.set(tick);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static String SAVE_STAFF_TIMER = "INSERT INTO `staff_timers`(`player_id`, `last_sunday`, `tick`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `tick` = VALUES(`tick`)";
	
	private static void saveStaffTimers(Player player, Connection con) {
		
		if (con == null) con = Server.getConnection();
		
		try (PreparedStatement stmt = con.prepareStatement(SAVE_STAFF_TIMER);) {
			
			stmt.setInt(1, player.mySQLIndex);
			stmt.setInt(2, SoulPlayDate.getLastSunday());
			stmt.setInt(3, player.staffTimer.getTimer());
			
			stmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void saveSkills(Player player) {
		try (Connection con = Server.getConnection();
				PreparedStatement stmt = con.prepareStatement(SkillsSaveEnum.SAVE_STRING);) {
			
			saveSkill(stmt, player);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void saveSkills(Player player, Connection con) {
		if (con == null) con = Server.getConnection();
	
		try (PreparedStatement stmt = con.prepareStatement(SkillsSaveEnum.SAVE_STRING);) {
			
			saveSkill(stmt, player);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void saveSkill(PreparedStatement stmt, Player player) throws Exception {
		int count = 1;
		stmt.setInt(count++, player.mySQLIndex);
		
		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			stmt.setInt(count++, player.getRealSkills().getLevel(i));
			stmt.setInt(count++, player.getRealSkills().getExperience(i));
		}
		stmt.executeUpdate();
	}
	
	private static final String SKILLS_LOAD_QUERY = "SELECT * FROM `skills` WHERE `player_id` = ?";
	
	private static boolean loadSkills(Player p, Connection connection) {
		boolean loaded = false;
		try (PreparedStatement ps = connection.prepareStatement(SKILLS_LOAD_QUERY);) {
			ps.setInt(1, p.mySQLIndex);
			
			loaded = loadSkillsResultSet(p, ps);
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadSkillsResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				for (int i = 0; i < Skills.STAT_COUNT; i++) {
					SkillsSaveEnum skill = SkillsSaveEnum.getSkill(i);
					final int lvl = rs.getInt(skill.getLvlTable());
					final int exp = rs.getInt(skill.getExpTable());
					p.getRealSkills().setExperienceAndLevels(skill.getSkillId(), exp);
					p.getRealSkills().setLevel(skill.getSkillId(), lvl);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static final String ROOM_QUERY = "INSERT INTO `poh_room`(`player_id`, `type`, `x`, `y`, `z`, `rotation`, `objects`, `items`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `type` = VALUES(`type`), rotation = VALUES(rotation), objects = VALUES(objects), items = VALUES(items)";
	public static final String POH_DETAIL_QUERY = "INSERT INTO poh_detail(player_id, house_style, locked, butler_type, servant_uses, grid_size, treasure_chest, fancy_dress_box, armor_case, magic_wardrobe, cape_rack, toy_box) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE house_style = VALUES(house_style), locked = VALUES(locked), butler_type = VALUES(butler_type), servant_uses = VALUES(servant_uses), grid_size = VALUES(grid_size), treasure_chest = VALUES(treasure_chest), fancy_dress_box = VALUES(fancy_dress_box), armor_case = VALUES(armor_case), magic_wardrobe = VALUES(magic_wardrobe), cape_rack = VALUES(cape_rack), toy_box = VALUES(toy_box)";

	public static void savePOH(Player player) {
		try (java.sql.Connection connection = Server.getConnection();
			 PreparedStatement roomStm = connection.prepareStatement(ROOM_QUERY);
			 PreparedStatement dStm = connection.prepareStatement(POH_DETAIL_QUERY);
			 ) {

			for (int z = 0; z < player.getPOH().getRooms().length; z++) {
				for (int x = 0; x < player.getPOH().getRooms()[z].length; x++) {
					for (int y = 0; y < player.getPOH().getRooms()[z][x].length; y++) {

						Room room = player.getPOH().getRoom(x,y,z);

						if (room == null || room.getType() == RoomType.EMPTY || room.getType() == RoomType.EMPTY_DUNG) {
							continue;
						}

						roomStm.setInt(1, player.mySQLIndex);
						roomStm.setString(2, room.getType().name().toUpperCase());
						roomStm.setInt(3, x);
						roomStm.setInt(4, y);
						roomStm.setInt(5, z);
						roomStm.setInt(6, room.getRotation());
						roomStm.setString(7, ConstructionUtils.buildObjectsString(room));
						roomStm.setString(8, ConstructionUtils.buildGroundItemsString(room));
						roomStm.addBatch();
					}
				}
			}
			roomStm.executeBatch();

			dStm.setInt(1, player.mySQLIndex);
			dStm.setString(2, player.getPOH().getHouseStyle().name());
			dStm.setBoolean(3, player.getPOH().isGuestsCanJoin());
			dStm.setString(4, player.getPOH().getButler().getType() == null ? "NONE" : player.getPOH().getButler().getType().name());
			dStm.setInt(5, player.getPOH().getButler().getUses());
			dStm.setInt(6, player.getPOH().getGridSize());
			dStm.setString(7, Poh.generateStorageString(player.getPOH().getTreasureChestItems()));
			dStm.setString(8, Poh.generateStorageString(player.getPOH().getFancyDressBox()));
			dStm.setString(9, Poh.generateStorageString(player.getPOH().getArmorCase()));
			dStm.setString(10, Poh.generateStorageString(player.getPOH().getMagicWardrobe()));
			dStm.setString(11, Poh.generateStorageString(player.getPOH().getCapeRack()));
			dStm.setString(12, Poh.generateStorageString(player.getPOH().getToyBox()));
			dStm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clearPOH(int mysqlIndex) {
		SAVE_EXECUTOR.submit(() -> {
			try (java.sql.Connection connection = Server.getConnection();
					PreparedStatement roomQuery = connection.prepareStatement("DELETE FROM `poh_room` WHERE `player_id` = ?");) {
				roomQuery.setInt(1, mysqlIndex); // objects must be deleted first as they are linked to rooms
				roomQuery.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	public static void deleteRoom(int mysqlIndex, int x, int y, int z) {
		SAVE_EXECUTOR.submit(() -> {
			try (java.sql.Connection connection = Server.getConnection();
					PreparedStatement roomQuery = connection.prepareStatement("DELETE FROM `poh_room` WHERE `player_id` = ? AND `x` = ? AND `y` = ? AND `z` = ?");) {
				roomQuery.setInt(1, mysqlIndex); // objects must be deleted first as they are linked to rooms
				roomQuery.setInt(2, x);
				roomQuery.setInt(3, y);
				roomQuery.setInt(4, z);
				roomQuery.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	private static final String DUNG_SAVE_QUERY = "INSERT INTO `dung`(`player_id`, `floors_unlocked`, `floors_completed`, `complexity_info`, `prev_progress`, `prayer_recharge`, `deaths`, `selected_loadout`, `ammo_id`, `ammo_count`, `bound_items`, `loadouts`, `instance_id`, `tokens`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `floors_unlocked` = VALUES(`floors_unlocked`), floors_completed = VALUES(floors_completed), complexity_info = VALUES(complexity_info), prev_progress = VALUES(prev_progress), prayer_recharge = VALUES(prayer_recharge), deaths = VALUES(deaths), selected_loadout = VALUES(selected_loadout), ammo_id = VALUES(ammo_id), ammo_count = VALUES(ammo_count), bound_items = VALUES(bound_items), loadouts = VALUES(loadouts), instance_id = VALUES(instance_id), tokens = VALUES(tokens)";
	private static final String DUNG_LOAD_QUERY = "SELECT * FROM `dung` WHERE `player_id` = ?";
	
	public static void saveDungeoneering(Player p) {
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(DUNG_SAVE_QUERY);) {
			ps.setInt(1, p.mySQLIndex);
			ps.setInt(2, p.dungFloorsUnlocked);
			ps.setString(3, p.getDungFloorsCompletedJsonString());
			ps.setInt(4, p.getComplexityInfo());
			ps.setInt(5, p.previousProgress);
			ps.setInt(6, p.dungPrayerRecharge);
			ps.setInt(7, p.dungDeaths);
			ps.setInt(8, p.getBinds().selectedLoudout);
			ps.setInt(9, p.getBinds().getAmmunitionId());
			ps.setInt(10, p.getBinds().getAmmoCount());
			ps.setString(11, p.getBinds().getBoundItemsJson());
			ps.setString(12, p.getBinds().getLoadoutsJson());
			ps.setLong(13, p.dungInstanceId);
			ps.setInt(14, p.dungTokens);
			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean loadDungeoneering(Player p, Connection connection) {
		boolean loaded = false;
		try (PreparedStatement ps = connection.prepareStatement(DUNG_LOAD_QUERY);) {
			ps.setInt(1, p.mySQLIndex);
			
			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				p.dungFloorsUnlocked = rs.getInt("floors_unlocked");
    				p.loadDungFloorsCompleted(rs.getString("floors_completed"));
    				p.setComplexityInfo(rs.getInt("complexity_info"));
    				p.previousProgress = rs.getInt("prev_progress");
    				p.dungPrayerRecharge = rs.getInt("prayer_recharge");
    				p.dungDeaths = rs.getInt("deaths");
    				p.getBinds().selectedLoudout = rs.getInt("selected_loadout");
    				p.getBinds().setAmmunitionId(rs.getInt("ammo_id"));
    				p.getBinds().setAmmoCount(rs.getInt("ammo_count"));
    				p.getBinds().loadBoundItemsFromJson(rs.getString("bound_items"));
    				p.getBinds().loadLoadoutsFromJson(rs.getString("loadouts"));
    				p.dungInstanceId = rs.getLong("instance_id");
    				p.dungTokens = rs.getInt("tokens");
    			}
    			loaded = true;
			} catch (Exception e) {
    			e.printStackTrace();
    			loaded = false;
    		}
		} catch (Exception e) {
			e.printStackTrace();
			loaded = false;
		}
		return loaded;
	}

	private static final String PRESETS_DELETE_QUERY = "DELETE FROM `presets` WHERE `player_id` = ?;";
	private static final String PRESETS_SAVE_QUERY = "INSERT INTO `presets`(`player_id`, `index`, `name`, `inventory`, `equipment`, `spellbook`) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `index` = VALUES(`index`), `name` = VALUES(`name`), `inventory` = VALUES(`inventory`), `equipment` = VALUES(`equipment`), `spellbook` = VALUES(`spellbook`)";
	private static final String PRESETS_LOAD_QUERY = "SELECT * FROM `presets` WHERE `player_id` = ?;";

	public static void savePresets(Player p) {
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(PRESETS_DELETE_QUERY)) {
			ps.setInt(1, p.mySQLIndex);
			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0, length = p.presets.size(); i < length; i++) {
			Preset preset = p.presets.get(i);
			try (Connection connection = Server.getConnection();
					PreparedStatement ps = connection.prepareStatement(PRESETS_SAVE_QUERY)) {
				ps.setInt(1, p.mySQLIndex);
				ps.setInt(2, preset.getIndex());
				ps.setString(3, preset.getName());
				ps.setString(4, preset.getInventoryJson());
				ps.setString(5, preset.getEquipmentJson());
				ps.setInt(6, preset.getSpellbook().getId());
				ps.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean loadPresets(Player p, Connection connection) {
		boolean loaded = false;
		try (PreparedStatement ps = connection.prepareStatement(PRESETS_LOAD_QUERY)) {
			ps.setInt(1, p.mySQLIndex);

			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				int index = rs.getInt("index");
    				String name = rs.getString("name");
    				List<Item> inventory = Preset.loadItemsFromJson(rs.getString("inventory"));
    				List<Item> equipment = Preset.loadItemsFromJson(rs.getString("equipment"));
    				SpellBook spellbook = SpellBook.find(rs.getInt("spellbook"));
    				Preset preset = new Preset(index, name, inventory, equipment, spellbook);
    				p.presets.add(index, preset);
    			}
    			loaded = true;
			} catch (Exception e) {
    			e.printStackTrace();
    			loaded = false;
    		}
		} catch (Exception e) {
			e.printStackTrace();
			loaded = false;
		}
		return loaded;
	}

	private static final String MISC_SAVE_QUERY = "INSERT INTO `misc`(`player_id`, `pk_t_spawned`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `pk_t_spawned`=VALUES(`pk_t_spawned`)";
	private static final String MISC_LOAD_QUERY = "SELECT * FROM `misc` WHERE `player_id` = ?";
	
	public static void saveMisc(Player p, Connection connection) {
		try (PreparedStatement ps = connection.prepareStatement(MISC_SAVE_QUERY);) {
			ps.setInt(1, p.mySQLIndex);
			ps.setBoolean(2, p.hasSpawnedGear);
			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean loadMisc(Player p, Connection con) {
		
		boolean loaded = false;
		if (con == null) con = Server.getConnection();
		
		try (PreparedStatement ps = con.prepareStatement(MISC_LOAD_QUERY);) {
			ps.setInt(1, p.mySQLIndex);
			
			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				p.getBrawlers().loadJsonString(rs.getString("brawler_gloves"));
    				p.getRecoilRings().loadJsonString(rs.getString("eye_of_the_rings"));
    				p.getGodSpell().loadJsonString(rs.getString("god_spell_req"));
    				SkillingUrns.loadUrnDataJsonString(p, rs.getString("skilling_urns"));
    				p.hasSpawnedGear = rs.getBoolean("pk_t_spawned");
    			}
			} catch (Exception e) {
    			e.printStackTrace();
    			loaded = false;
    		}
			loaded = true;
		} catch (Exception e) {
			e.printStackTrace();
			loaded = false;
		}
		return loaded;
	}

	private static final String ITEM_RETRIEVAL_SAVE_QUERY = "INSERT INTO `itemretrieval`(`player_id`, `items`, `price`, `unlocked`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `items` = VALUES(`items`), `price` = VALUES(`price`), `unlocked` = VALUES(`unlocked`)";
	private static final String ITEM_RETRIEVAL_LOAD_QUERY = "SELECT * FROM `itemretrieval` WHERE `player_id` = ?";
	
	public static void saveItemRetrieval(Player p, Connection connection) {
		try (PreparedStatement ps = connection.prepareStatement(ITEM_RETRIEVAL_SAVE_QUERY)) {
			ps.setInt(1, p.mySQLIndex);
			ItemRetrievalInstance instance = p.itemRetrieval;
			if (instance == null) {
				ps.setString(2, null);
				ps.setInt(3, -1);
				ps.setBoolean(4, false);
			} else {
				ps.setString(2, instance.getInventory().getItemsJson());
				ps.setInt(3, instance.getPrice());
				ps.setBoolean(4, instance.isUnlocked());
			}

			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean loadItemRetrieval(Player p, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();
		
		try (PreparedStatement ps = con.prepareStatement(ITEM_RETRIEVAL_LOAD_QUERY)) {
			ps.setInt(1, p.mySQLIndex);
			
			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				String value = rs.getString("items");
    				int price = rs.getInt("price");
    				boolean unlocked = rs.getBoolean("unlocked");
    				if (value != null) {
    					p.itemRetrieval = new ItemRetrievalInstance(new Inventory(ItemRetrievalManager.INVENTORY_CAPACITY, value, ContainerType.ALWAYS_STACK), unlocked, price);
    				}
    			}
			} catch (Exception e) {
    			e.printStackTrace();
    			loaded = false;
    		}
			loaded = true;
		} catch (Exception e) {
			e.printStackTrace();
			loaded = false;
		}
		return loaded;
	}
	
	private static final String SEASONAL_SAVE_QUERY = "INSERT INTO `seasonal` (`player_id`, `powers`, `boughtItems`, `powerLearnedFromCard`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `powers` = VALUES(`powers`), `boughtItems` = VALUES(`boughtItems`), `powerLearnedFromCard` = VALUES(`powerLearnedFromCard`)";
	private static final String SEASONAL_LOAD_QUERY = "SELECT * FROM `seasonal` WHERE `player_id` = ?";

	public static void saveSeasonal(Player p, Connection connection) {
		try (PreparedStatement ps = connection.prepareStatement(SEASONAL_SAVE_QUERY)) {
			ps.setInt(1, p.mySQLIndex);
			ps.setString(2, p.getSeasonalData().getPowersJson());
			ps.setString(3, p.getSeasonalData().getBoughtItemsJson());
			ps.setInt(4, p.getSeasonalData().powerLearnedFromCard != null ? p.getSeasonalData().powerLearnedFromCard.getId() : -1);
			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean loadSeasonal(Player p, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();
		
		try (PreparedStatement ps = con.prepareStatement(SEASONAL_LOAD_QUERY)) {
			ps.setInt(1, p.mySQLIndex);
			
			try (ResultSet rs = ps.executeQuery();) {
    			while (rs.next()) {
    				String powers = rs.getString("powers");
    				if (powers != null) {
    					p.getSeasonalData().initPowers(powers);
    				}

    				String items = rs.getString("boughtItems");
    				if (items != null) {
    					p.getSeasonalData().initBoughtItems(items);
    				}

    				p.getSeasonalData().powerLearnedFromCard = PowerUpData.list(rs.getInt("powerLearnedFromCard"));
    			}
			} catch (Exception e) {
    			e.printStackTrace();
    			loaded = false;
    		}
			loaded = true;
		} catch (Exception e) {
			e.printStackTrace();
			loaded = false;
		}
		return loaded;
	}

	@SuppressWarnings("finally")
	public static boolean saveGamePVP(Player p, boolean logout) {
		boolean saved = false;
		if (!Config.exportToSql) {
			// remove these if statements for export
			if (!p.saveFile || p.newPlayer || !p.saveCharacter) {
				System.out.println("first");
				if (logout)
				p.savedGame.set(true);
				return true;
			}
			if (p.getName() == null || p == null
					|| PlayerHandler.players[p.getId()] == null) {
				System.out.println("second");
				if (logout)
				p.savedGame.set(true);
				return true;
			}
			if (p.absX == -1 || p.absY == -1) {
				System.out.println("third");
				if (logout)
				p.savedGame.set(true);
				return true;
			}
		}
		if (!Config.exportToSql) {
			try {
				PasswordHash.createHash(p.playerPass);
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
			} catch (InvalidKeySpecException e1) {
				e1.printStackTrace();
			}
		}

		Connection connection = Server.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement(UpdateQueryPVP);
			if (p.teleportToX > 0 && p.teleportToY > 0) {
				p.absX = p.teleportToX;
				p.absY = p.teleportToY;
				p.heightLevel = p.teleportToZ;
			}
			stmt.setInt(1, p.heightLevel);
			stmt.setInt(2, p.absX);
			stmt.setInt(3, p.absY);
			stmt.setInt(4, p.playerRights);
			stmt.setInt(5, p.difficulty);
			stmt.setInt(6, p.homeTeleActions);
			stmt.setString(7, p.lastClanChat);
			stmt.setLong(8, p.timePlayed);
			stmt.setString(9, p.lastDayLoggedIn);
			stmt.setInt(10, p.lastDayPkedOfYear);
			stmt.setString(11, p.playerTitle);
			stmt.setInt(12, p.titleColor);
			stmt.setString(13, createIntArrayWithSeperator(p.titles));
			stmt.setString(14, p.inVerificationState ? p.oldUUID : p.UUID);
			stmt.setString(15, p.connectedFrom);
			stmt.setString(16,
					createIntArrayWithSeperator(p.lastConnectedFrom));
			stmt.setString(17,
					createIntArrayWithSeperator(p.lastKilledPlayers));
			stmt.setString(18, createIntArrayWithSeperator(p.achievements));
			stmt.setInt(19, p.achievementsCompleted);
			stmt.setString(20, createIntArrayWithSeperator(p.quests));
			stmt.setInt(21, p.questPoints);
			stmt.setString(22, createIntArrayWithSeperator(p.getTimers()));
			stmt.setString(23, createIntArrayWithSeperator(p.getVariables()));
			stmt.setBoolean(24, p.setPin); // useless thing
			stmt.setString(25, createIntArrayWithSeperator(p.getBankPins()));
			stmt.setBoolean(26, p.hasBankPin);
			stmt.setInt(27, p.pinDeleteDateRequested);
			stmt.setBoolean(28, p.requestPinDelete);
			stmt.setInt(29, p.playerMagicBook == null ? 0 : p.playerMagicBook.getId());
			stmt.setInt(30, p.playerPrayerBook == null ? 0 : p.playerPrayerBook.getId());
			stmt.setInt(31, p.dfsCharge);
			stmt.setDouble(32, p.specAmount);
			stmt.setInt(33, p.KC);
			stmt.setInt(34, p.DC);
			stmt.setInt(35, p.killStreak);
			stmt.setInt(36, p.highestKillStreak);
			stmt.setInt(37, p.getEloRating());
			stmt.setBoolean(38, p.expLock);
			stmt.setString(39,
					createIntArrayWithSeperator(p.getChatSettings()));
			stmt.setInt(40, 0); //----------------------------------------------NOT USED
			stmt.setInt(41, p.getWildernessTick());
			stmt.setInt(42, p.npcKills);
			stmt.setInt(43, p.fightMode);
			stmt.setInt(44, p.autoRet);
			stmt.setLong(45, p.getDoubleExpLength());
			stmt.setLong(46, p.getTimedMute());
			stmt.setBoolean(47, p.accountFlagged);
			stmt.setString(48, createIntArrayWithSeperator(p.barrows));
			stmt.setInt(49, p.waveId);
			stmt.setInt(50, p.getSlayerTasksCompleted());
			stmt.setString(51, p.taskName);
			stmt.setString(52, p.masterName);
			stmt.setInt(53, p.assignmentAmount);
			stmt.setInt(54, p.getSlayerPoints().getTotalValue());
			stmt.setInt(55, p.getDuoPoints().getTotalValue());
			stmt.setString(56, createJsonString(p.lastConnectedFromUUID));//unused slot
			stmt.setString(57, "");//unused slot
			stmt.setInt(58, p.lmsPoints);
			stmt.setInt(59, p.magePoints);
			stmt.setInt(60, p.pkp);
			stmt.setInt(61, p.penguinPoints);
			stmt.setInt(62, p.getDonPoints());
			stmt.setInt(63, p.getDonatedTotalAmount());
			stmt.setInt(64, p.yellPoints);
			stmt.setDouble(65, p.getLoyaltyPoints());
			stmt.setDouble(66, p.getAgilityPoints());
			stmt.setInt(67, p.getPcPoints());
			stmt.setLong(68, p.getGwdKillCount());

			StringBuilder temp = new StringBuilder();

			/* ItemID - Amount */ // bank 0
			for (int i = 0; i < p.bankItems0.length; i++) {
				// if (p.bankItems0[i] > 0)
				temp.append(p.bankItems0[i] + ":" + p.bankItems0N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(69, "");
			} else {
				stmt.setString(69,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 1
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems1.length; i++) {
				// if (p.bankItems1[i] > 0)
				temp.append(p.bankItems1[i] + ":" + p.bankItems1N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(70, "");
			} else {
				stmt.setString(70,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 2
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems2.length; i++) {
				// if (p.bankItems2[i] > 0)
				temp.append(p.bankItems2[i] + ":" + p.bankItems2N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(71, "");
			} else {
				stmt.setString(71,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 3
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems3.length; i++) {
				// if (p.bankItems3[i] > 0)
				temp.append(p.bankItems3[i] + ":" + p.bankItems3N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(72, "");
			} else {
				stmt.setString(72,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 4
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems4.length; i++) {
				// if (p.bankItems4[i] > 0)
				temp.append(p.bankItems4[i] + ":" + p.bankItems4N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(73, "");
			} else {
				stmt.setString(73,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 5
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems5.length; i++) {
				// if (p.bankItems5[i] > 0)
				temp.append(p.bankItems5[i] + ":" + p.bankItems5N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(74, "");
			} else {
				stmt.setString(74,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 6
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems6.length; i++) {
				// if (p.bankItems6[i] > 0)
				temp.append(p.bankItems6[i] + ":" + p.bankItems6N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(75, "");
			} else {
				stmt.setString(75,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 7
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems7.length; i++) {
				// if (p.bankItems7[i] > 0)
				temp.append(p.bankItems7[i] + ":" + p.bankItems7N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(76, "");
			} else {
				stmt.setString(76,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* ItemID - Amount */ // bank 8
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems8.length; i++) {
				// if (p.bankItems8[i] > 0)
				temp.append(p.bankItems8[i] + ":" + p.bankItems8N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(77, "");
			} else {
				stmt.setString(77,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Level - XP */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.getPlayerLevel().length; i++) {
				temp.append(p.getRealSkills().getLevel(i) + ":" + p.getRealSkills().getExperience(i) + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(78, "");
			} else {
				stmt.setString(78,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Appearance */
			temp.delete(0, temp.length());
			for (int element : p.playerAppearance) {
				temp.append(element + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(79, "");
			} else {
				stmt.setString(79,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Slot - ItemID - Amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.playerItems.length; i++) {
				// if(p.playerItems[i] > 0)
				temp.append(i + ":" + p.playerItems[i] + ":" + p.playerItemsN[i]
						+ ":");
			}
			if (temp.length() == 0) {
				stmt.setString(80, "");
			} else {
				stmt.setString(80,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Slot - ItemID - Amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.playerEquipment.length; i++) {
				temp.append(i + ":" + p.playerEquipment[i] + ":"
						+ p.playerEquipmentN[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(81, "");
			} else {
				stmt.setString(81,
						temp./* deleteCharAt(temp.length() - 1). */toString());
			}

			/* Rune Pouch - ItemId - Amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.runePouchItems.length; i++) {
				temp.append(i + ":" + p.runePouchItems[i] + ":"
						+ p.runePouchItemsN[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(82, "");
			} else {
				stmt.setString(82,
						temp./* deleteCharAt(Temp.length() - 1). */toString());
			}

			temp = null;

			stmt.setLong(83, p.playerCollect);
			stmt.setString(84, createIntArrayWithSeperator(p.playerShopItems));
			stmt.setString(85, createIntArrayWithSeperator(p.playerShopItemsN));
			stmt.setString(86,
					createLongArrayWithSeperator(p.playerShopItemsPrice));

			stmt.setInt(87, p.xenforoIndex);
			stmt.setLong(88, p.getTotalGold());
			stmt.setString(89, toStringArray(p.getBossKills()));
			stmt.setBoolean(90, p.isDisabled());
			stmt.setString(91, createStringFromIntList(p.quickPrayers));
			stmt.setString(92, createStringFromIntList(p.quickCurses));
			stmt.setString(93, createIntArrayWithSeperator(p.getJudgement()));
			stmt.setString(94, UntradeableManager.getMapToSave(p));
			stmt.setString(95, null); // chat_crowns legacy save
			stmt.setString(96, createIntArrayWithSeperator(p.getSeasonalElo()));
			stmt.setInt(97, p.getZeals());
			stmt.setLong(98, p.getMoneyPouch());
			stmt.setInt(99, p.mySQLIndex);
			stmt.executeUpdate();
			saved = true;
			if (logout)
			p.savedGame.set(true);
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			if (logout)
			p.savedGame.set(false);
			saved = false;
			// return false;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					if (logout)
					p.savedGame.set(false);
					return false;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					if (logout)
					p.savedGame.set(false);
					return false;
				}
			}
			// p.savedGame = true;
			return saved;//p.getSavedGame();
		}
	}

	@SuppressWarnings("finally")
	public static boolean savePasswordBankPinFriendsETC(Player p, boolean logout) {
		if (!Config.exportToSql) {
			// remove these if statements for export
			if (!p.saveFile || p.newPlayer || !p.saveCharacter) {
				System.out.println("first");
				if (logout)
				p.savedGame.set(true);
				return true;
			}
			if (p.getName() == null || p == null
					|| PlayerHandler.players[p.getId()] == null) {
				System.out.println("second");
				if (logout)
				p.savedGame.set(true);
				return true;
			}
			if (p.absX == -1 || p.absY == -1) {
				System.out.println("third");
				if (logout)
				p.savedGame.set(true);
				return true;
			}
		}
		// String passwordHash = p.playerPass;
		// if (!Config.exportToSql) {
		// try {
		// passwordHash = PasswordHash.createHash(p.playerPass);
		// } catch (Exception e) {
		// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(PlayerSaveSql.class,
		// e));
		// }
		// }

		Connection connection = Server.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement(UpdateQueryForPassBankPinETC);

			stmt.setBoolean(1, p.setPin); // useless thing
			stmt.setString(2, createIntArrayWithSeperator(p.getBankPins()));
			stmt.setBoolean(3, p.hasBankPin);
			stmt.setInt(4, p.pinDeleteDateRequested);
			stmt.setBoolean(5, p.requestPinDelete);

			stmt.setString(6, createIntArrayWithSeperator(p.getChatSettings()));

			stmt.setInt(7, logout ? 0 : Config.NODE_ID); // logged_into = ?

			stmt.setString(8, p.getName());
			stmt.executeUpdate();
			if (logout)
			p.savedGame.set(true);
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			if (logout)
			p.savedGame.set(false);
			// return false;
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					if (logout)
					p.savedGame.set(false);
					return false;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					if (logout)
					p.savedGame.set(false);
					return false;
				}
			}
			// p.savedGame = true;
			return true;//p.getSavedGame();
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean saveInventoryEconomyWorlds(Player p) {
		if (!Config.exportToSql) {
			// remove these if statements for export
			if (!p.saveFile || p.newPlayer || !p.saveCharacter || !p.isActive) {
				System.out.println("unable to save inv on new account or inactives");
				return true;
			}
			if (p.absX == -1 || p.absY == -1) {
				System.out.println("unable to save inv on account that has no location set");
				return true;
			}
		}


		Connection connection = Server.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement(inventoryUpdateQuery);
			
			StringBuilder temp = new StringBuilder();
			/* Slot - ItemID - Amount */
			temp.delete(0, temp.length());
			for (int i = 0; i < p.playerItems.length; i++) {
				temp.append(i + ":" + p.playerItems[i] + ":" + p.playerItemsN[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(1, "");
			} else {
				stmt.setString(1, temp.toString());
			}

			stmt.setInt(2, p.mySQLIndex);
			stmt.executeUpdate();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					return false;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					return false;
				}
			}
			return true;
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean saveBoBInvEconomyWorlds(Player p) {
		if (!Config.exportToSql) {
			// remove these if statements for export
			if (!p.saveFile || p.newPlayer || !p.saveCharacter || !p.isActive) {
				System.out.println("unable to save inv on new account or inactives");
				return true;
			}
			if (p.absX == -1 || p.absY == -1) {
				System.out.println("unable to save inv on account that has no location set");
				return true;
			}
		}


		Connection connection = Server.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement(bobInvUpdateQuery);
			stmt.setString(1, createIntArrayWithSeperator(p.burdenedItems));
			stmt.setInt(2, p.mySQLIndex);
			stmt.executeUpdate();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					return false;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					return false;
				}
			}
			return true;
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean saveBankEconomyWorlds(Player p) {
		if (!Config.exportToSql) {
			// remove these if statements for export
			if (!p.saveFile || p.newPlayer || !p.saveCharacter || !p.isActive) {
				System.out.println("unable to save inv on new account or inactives");
				return true;
			}
			if (p.absX == -1 || p.absY == -1) {
				System.out.println("unable to save inv on account that has no location set");
				return true;
			}
		}


		Connection connection = Server.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = connection.prepareStatement(bankItemsUpdateQuery);
			
			StringBuilder temp = new StringBuilder();
			/* ItemID - Amount */ // bank 0
			for (int i = 0; i < p.bankItems0.length; i++) {
				temp.append(p.bankItems0[i] + ":" + p.bankItems0N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(1, "");
			} else {
				stmt.setString(1, temp.toString());
			}

			/* ItemID - Amount */ // bank 1
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems1.length; i++) {
				temp.append(p.bankItems1[i] + ":" + p.bankItems1N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(2, "");
			} else {
				stmt.setString(2, temp.toString());
			}

			/* ItemID - Amount */ // bank 2
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems2.length; i++) {
				temp.append(p.bankItems2[i] + ":" + p.bankItems2N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(3, "");
			} else {
				stmt.setString(3, temp.toString());
			}

			/* ItemID - Amount */ // bank 3
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems3.length; i++) {
				temp.append(p.bankItems3[i] + ":" + p.bankItems3N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(4, "");
			} else {
				stmt.setString(4, temp.toString());
			}

			/* ItemID - Amount */ // bank 4
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems4.length; i++) {
				temp.append(p.bankItems4[i] + ":" + p.bankItems4N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(5, "");
			} else {
				stmt.setString(5, temp.toString());
			}

			/* ItemID - Amount */ // bank 5
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems5.length; i++) {
				temp.append(p.bankItems5[i] + ":" + p.bankItems5N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(6, "");
			} else {
				stmt.setString(6, temp.toString());
			}

			/* ItemID - Amount */ // bank 6
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems6.length; i++) {
				temp.append(p.bankItems6[i] + ":" + p.bankItems6N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(7, "");
			} else {
				stmt.setString(7, temp.toString());
			}

			/* ItemID - Amount */ // bank 7
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems7.length; i++) {
				temp.append(p.bankItems7[i] + ":" + p.bankItems7N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(8, "");
			} else {
				stmt.setString(8, temp.toString());
			}

			/* ItemID - Amount */ // bank 8
			temp.delete(0, temp.length());
			for (int i = 0; i < p.bankItems8.length; i++) {
				temp.append(p.bankItems8[i] + ":" + p.bankItems8N[i] + ":");
			}
			if (temp.length() == 0) {
				stmt.setString(9, "");
			} else {
				stmt.setString(9, temp.toString());
			}

			stmt.setInt(10, p.mySQLIndex);
			stmt.executeUpdate();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(PlayerSaveSql.class, e));
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					return false;
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerSaveSql.class, e));
					e.printStackTrace();
					return false;
				}
			}
			return true;
		}
	}
	
	//Put this in the saveExec single thread so i know all accounts got saved for sure.
	public static void alertSlackOfSafeExit() {
		SAVE_EXECUTOR.submit(() -> {
			try {
				Server.getSlackApi()
				.call(SlackMessageBuilder.buildStartMessage(
						"KickedPlayers World:" + Config.NODE_ID, true,
						"#developers"));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
	}

	public static void savePlayer(final Player p) {
		if (Config.SERVER_DEBUG) {
			PlayerSave.saveGame(p, true);
			return;
		}
		// AtomicBoolean stop = new AtomicBoolean(false);
		SAVE_EXECUTOR.submit(() -> {
			try {
				if (WorldType.equalsType(WorldType.SPAWN)) {
					PlayerSaveSql.saveGamePVP(p, true);
					PlayerSaveSql.savePasswordBankPinFriendsETC(p, true);
					return;
				} else {
					while (!saveGame(p, true)) {
						try {
							Thread.sleep(50);
							// saveGame(p);
							// if (p.getSavedGame()) {
							// stop.set(true);
							// }
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
	}

	public static String toStringArray(List<? extends Object> list) {
		StringBuilder sb = new StringBuilder();
		for (Object val : list) {
			if (val != null) {
				sb.append(val.toString());
			} else {
				sb.append("0");
			}
			sb.append(":");
		}
		return sb.toString();
	}

	public static String toStringArray(int[] list) {
		StringBuilder sb = new StringBuilder();
		for (int val : list) {
			sb.append(val);
			sb.append(":");
		}
		return sb.toString();
	}

	public int getMID(String playerName) {
		int mid = 0;
		Connection connection = Server.getConnection();

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = connection.createStatement();
			rs = stmt.executeQuery(
					"select ID from `accounts` where PlayerName = '"
							+ playerName + "' limit 1");
			if (rs.next()) {
				mid = rs.getInt("ID");
			} else { // Does not have an account in "account" Table so create
						// new account after login is succesful
				mid = 0;
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
			mid = 0;
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mid;
	}
	
	
	private static final String GET_USERNAME_QUERY = "SELECT PlayerName, display_name from players.accounts WHERE PlayerName = ? OR display_name = ? LIMIT 1";
	
	public static String getUserNameByDisplayName(final String displayName) {
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(GET_USERNAME_QUERY)) {
			ps.setString(1, displayName);
			ps.setString(2, displayName);
			try (ResultSet results = ps.executeQuery()) {
				if (results.next()) {
					String username = results.getString("PlayerName");
					return username;
				}
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static final String GET_UUID_QUERY = "SELECT UUID from players.accounts WHERE UUID = ? LIMIT 1";
	
	public static boolean isUniqueUUID(final String uuid) {
		boolean found = false;
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(GET_UUID_QUERY)) {
			ps.setString(1, uuid);
			try (ResultSet results = ps.executeQuery()) {
				if (results.isBeforeFirst()) {
					found = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return found;
	}
	
	public static String createJsonString(Object object) {
		return GsonSave.gsond.toJson(object);
	}
	
	public static Object loadFromJsonString(final String load, TypeToken<?> token) {
		return GsonSave.gsond.fromJson(load, token.getType());
	}

	static final String LOGIN_STATE_SQL = "update `accounts` set logged_into = ? where ID = ?;";
	public static void startLoginStateChange(int mysqlIndex, int world) {
		if (!Config.RUN_ON_DEDI)
			return;
		SAVE_EXECUTOR.submit(() -> {
			try {
				try (Connection connection = Server.getConnection();
						PreparedStatement stmt = connection.prepareStatement(LOGIN_STATE_SQL);) {
					stmt.setInt(1, world);
					stmt.setInt(2, mysqlIndex);
					stmt.executeUpdate();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
	}
	
	static final String PKED_UPDATE = "update `accounts` set last_pk_timestamp = ? where ID = ?;";
	public static void updatePkedTimestamp(int mysqlIndex) {
		if (!Config.RUN_ON_DEDI)
			return;
		SAVE_EXECUTOR.submit(() -> {
			try {
				try (Connection connection = Server.getConnection();
						PreparedStatement stmt = connection.prepareStatement(PKED_UPDATE);) {
					Timestamp ts = Misc.getESTTimestamp();
					stmt.setTimestamp(1, ts);
					stmt.setInt(2, mysqlIndex);
					stmt.executeUpdate();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
	}
	
	public static void resetEventVar() {
		System.out.println("starting of resetting temp var!!!!!!!!==============================");
		try (Connection connection = Server.getConnection();
				PreparedStatement stmt = connection.prepareStatement("update `accounts` set temp = 0 WHERE temp = 1")) {
			stmt.executeUpdate();
			System.out.println("done resetting temp var!!!!!!!!==============================");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
