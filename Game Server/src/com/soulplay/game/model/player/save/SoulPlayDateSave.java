package com.soulplay.game.model.player.save;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.soulplaydate.SoulPlayDateEnum;

public class SoulPlayDateSave {
	
	private static final String SAVE_Q = "INSERT INTO `soulplay_date`(`player_id`, `date_id`, `day`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `date_id` = VALUES(`date_id`), day = VALUES(day)";

	public static void setNewDay(Player player, int newDay, SoulPlayDateEnum date) {
		final int playerId = player.mySQLIndex;
		date.set(player, newDay);
		
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {
			save(playerId, date.getId(), newDay);
		});
	}
	
	private static void save(int playerId, int dateId, int day) {
		try (Connection connection = Server.getConnection();
				PreparedStatement stmt = connection.prepareStatement(SAVE_Q);) {
			stmt.setInt(1, playerId);
			stmt.setInt(2, dateId);
			stmt.setInt(3, day);
			stmt.executeUpdate();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(SoulPlayDateSave.class, e));
		}
	}
	
	private static final String LOAD_Q = "SELECT date_id, day FROM `soulplay_date` WHERE `player_id` = ?";
	
	public static boolean loadDates(Player player, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(LOAD_Q);) {

			stmt.setInt(1, player.mySQLIndex);

			loaded = loadResultSet(player, stmt);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				final int dateId = rs.getInt("date_id");
				final int day = rs.getInt("day");
				SoulPlayDateEnum date = SoulPlayDateEnum.forId(dateId);
				date.set(p, day);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
