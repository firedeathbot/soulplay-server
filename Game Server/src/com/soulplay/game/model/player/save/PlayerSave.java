package com.soulplay.game.model.player.save;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.soulplay.Config;
import com.soulplay.config.WorldType;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;
import com.soulplay.util.PasswordHash;

public class PlayerSave {

	/**
	 * Returns a characters friends in the form of a long array.
	 *
	 * @param name
	 * @return
	 */
	public static long[] getFriends(String name) {
		String line = "";
		String token = "";
		String token2 = "";
		String[] token3 = new String[3];
		boolean end = false;
		int readMode = 0;
		BufferedReader file = null;
		boolean file1 = false;
		long[] readFriends = new long[200];
		long[] friends = null;
		int totalFriends = 0;
		try {
			file = new BufferedReader(
					new FileReader("./Data/characters/" + name + ".txt"));
			file1 = true;
		} catch (FileNotFoundException fileex1) {
		}

		if (file1) {
			new File("./Data/characters/" + name + ".txt");
		} else {
			return null;
		}
		try {
			line = file.readLine();
		} catch (IOException ioexception) {
			return null;
		}
		while (end == false && line != null) {
			line = line.trim();
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token3 = token2.split("\t");
				switch (readMode) {
					case 0:
						if (token.equals("character-friend")) {
							readFriends[Integer.parseInt(token3[0])] = Long
									.parseLong(token3[1]);
							totalFriends++;
						}
						break;
				}
			} else {
				if (line.equals("[FRIENDS]")) {
					readMode = 0;
				} else if (line.equals("[EOF]")) {
					try {
						file.close();
					} catch (IOException ioexception) {
					}
				}
			}
			try {
				line = file.readLine();
			} catch (IOException ioexception1) {
				end = true;
			}
		}
		try {
			if (totalFriends > 0) {
				friends = new long[totalFriends];
				for (int index = 0; index < totalFriends; index++) {
					friends[index] = readFriends[index];
				}
				return friends;
			}
			file.close();
		} catch (IOException ioexception) {
		}
		return null;
	}

	/**
	 * Tells use whether or not the specified name has the friend added.
	 *
	 * @param name
	 * @param friend
	 * @return
	 */
	public static boolean isFriend(String name, String friend) {
		long nameLong = Misc.playerNameToInt64(friend);
		long[] friends = getFriends(name);
		if (friends != null && friends.length > 0) {
			for (long friend2 : friends) {
				if (friend2 == nameLong) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Loading
	 **/
	public static int loadGame(Client p, String playerName, String playerPass) {
		int sqlType = 0;

		if (WorldType.equalsType(WorldType.SPAWN)) {
			sqlType = PlayerSaveSql.loadFromMainAccount(p, playerName, playerPass); // returns 3 if wrong password
			if (sqlType != 3) {
				PlayerSaveSql.loadGamePVP(p, playerName, playerPass);
			}
		} else {
			sqlType = PlayerSaveSql.loadGame(p, playerName, playerPass, null);
		}

		// 0 = user not in sql yet so creating default field(also for new
		// account too), 13 = row in sql now but needs to be saved properly
		// still
		// System.out.println("sql Type = "+sqlType);
		if (sqlType != 777) { // if (sqlType == 13) {
			GsonSave.loadPatches(p);// FarmingManager.loadPatches(p);
			return sqlType; // return 1;
		} else {
			GsonSave.loadPatches(p);// FarmingManager.loadPatches(p);
		}
		// System.out.println("sql Type =");
		String line = "";
		String token = "";
		String token2 = "";
		String[] token3 = new String[3];
		boolean EndOfFile = false;
		int ReadMode = 0;
		BufferedReader characterfile = null;
		boolean File1 = false;
		try {
			if (Config.SERVER_DEBUG && !Config.exportToSql) {
				characterfile = new BufferedReader(new FileReader(
						"./Data/debugchars/" + playerName + ".txt"));
			} else {
				characterfile = new BufferedReader(new FileReader(
						"./Data/characters/" + playerName + ".txt"));
			}

			File1 = true;
		} catch (FileNotFoundException fileex1) {
		}
		if (playerExists(Misc.removeSpaces(playerName))
				&& !playerExists(playerName)) {
			p.disconnected = true;
			try {
				characterfile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return 3;
		}

		if (File1) {
			// new File ("./characters/"+playerName+".txt");
		} else {
			Misc.println(playerName + ": character file not found.");
			p.newPlayer = false;

			if (p.isBot()) {
				p.addStarter = true;
			}

			// try {
			// characterfile.close();
			// } catch (IOException e) {
			// e.printStackTrace();
			// }

			return 0;
		}
		try {
			line = characterfile.readLine();
		} catch (IOException ioexception) {
			Misc.println(playerName + ": error loading file.");

			try {
				characterfile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return 3;
		}
		while (EndOfFile == false && line != null) {
			line = line.trim();
			// String taskName = null;
			// String masterName = null;
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token3 = token2.split("\t");
				switch (ReadMode) {
					// case 1:
					// if (token.equals("character-password")) {
					// if (Config.XENFORO_LINK) {
					// try {
					// //if (PasswordHash.validatePassword(playerPass, token2))
					// {
					// if (!p.checkXenLink.equals("poop") || (token2.length() <
					// 14 && playerPass.equalsIgnoreCase(token2)) ||
					// Misc.basicEncrypt(playerPass).equals(token2) ||
					// (token2.length() > 14 &&
					// PasswordHash.validatePassword(playerPass, token2)) /*||
					// Misc.xenLoginSuccessful("Aleksandr", token2)*/) {
					// playerPass = token2;
					//// System.out.println("passed");
					// } else {
					// System.out.println("Invalid password3");
					// return 3;
					// }
					// } catch (NoSuchAlgorithmException e) {
					// e.printStackTrace();
					// System.out.println("Invalid password4");
					// return 3;
					// } catch (InvalidKeySpecException e) {
					// e.printStackTrace();
					// System.out.println("Invalid password5");
					// return 3;
					// }
					// } else {
					// try {
					//// System.out.println("Trying");
					// if ((token2.length() < 14 &&
					// playerPass.equalsIgnoreCase(token2)) ||
					// Misc.basicEncrypt(playerPass).equals(token2) ||
					// (/*token2.matches("-?\\d+(\\.\\d+)?") && */
					// (token2.length() > 14 &&
					// PasswordHash.validatePassword(playerPass, token2)))) {
					// playerPass = token2;
					//// System.out.println("same password");
					// } else {
					// System.out.println("Invalid password6");
					// return 3;
					// }
					// } catch (Exception e) {
					//
					// System.out.println("Invalid password9");
					// return 3;
					// //e.printStackTrace();
					// }
					// }
					// }
					// break;
					case 2:
						if (token.equals("character-height")) {
							p.heightLevel = Integer.parseInt(token2);
						} else if (token.equals("character-posx")) {
							p.teleportToX = (Integer.parseInt(token2) <= 0
									? 3210
									: Integer.parseInt(token2));
						} else if (token.equals("character-posy")) {
							p.teleportToY = (Integer.parseInt(token2) <= 0
									? 3424
									: Integer.parseInt(token2));
						} else if (token.equals("character-rights")) {
							p.playerRights = Integer.parseInt(token2);
						} else if (token.equals("UUID")) {
							p.oldUUID = token2;
						} else if (token.equals("difficulty")) {
							p.difficulty = Integer.parseInt(token2);
						} else if (token.equals("time-played")) {
							p.timePlayed = Long.parseLong(token2);
						} else if (token.equals("homeTeleActions")) {
							p.homeTeleActions = Integer.parseInt(token2);
						} else if (token.startsWith("last-clan")) {
							p.lastClanChat = token2;
						} else if (token.equals("character-title")) {
							p.playerTitle = token2;
						} else if (token.equals("character-title-color")) {
							p.titleColor = Integer.parseInt(token2);
						} else if (token.equals("titles-unlocked")) {
							String[] array = new String[p.titles.length];
							array = token2.split(";");
							for (int aa = 0; aa < p.titles.length; aa++) {
								if (aa >= array.length || array[aa].isEmpty()) { // fill
																					// zeroes
									p.titles[aa] = 0;
									continue;
								}
								p.titles[aa] = Integer.parseInt(array[aa]);
							}
							array = null;
						} else if (token.equals("achievements")) {
							String[] array = new String[p.achievements.length];
							array = token2.split(";");
							for (int aa = 0; aa < p.achievements.length; aa++) {
								p.achievements[aa] = Integer
										.parseInt(array[aa]);
							}
							array = null;
						} else if (token.equals("barrows")) {
							String[] array = new String[p.barrows.length];
							array = token2.split(";");
							for (int aa = 0; aa < p.barrows.length; aa++) {
								p.barrows[aa] = Integer.parseInt(array[aa]);
							}
							array = null;
						} else if (token.equals("killed-players")) {
							p.lastKilledPlayers.add(token2);
						} else if (token.equals("recent-ip")) {
							p.connectedFrom = token2;
						} else if (token.equals("connected-from")) {
							p.lastConnectedFrom.add(token2);
						} else if (token.equals("lastLoginDay")) {
							p.lastDayLoggedIn = token2;
						} else if (token.equals("lastDayPkedOfYear")) {
							p.lastDayPkedOfYear = Integer.parseInt(token2);
						} else if (token.equals("quest-points")) {
							p.questPoints = Integer.parseInt(token2);
						} else if (token.equals("achievements-completed")) {
							p.achievementsCompleted = Integer.parseInt(token2);
						} else if (token.equals("startPack")) {
							p.setStartPack(Boolean.parseBoolean(token2));
						} else if (token.equals("lastLoginDate")) {
							p.lastLoginDate = Integer.parseInt(token2);
						} else if (token.equals("setPin")) {
							p.setPin = Boolean.parseBoolean(token2);
						} else if (token.equals("hasBankpin")) {
							p.hasBankPin = Boolean.parseBoolean(token2);
						} else if (token.equals("pinRegisteredDeleteDay")) {
							p.pinDeleteDateRequested = Integer.parseInt(token2);
						} else if (token.equals("requestPinDelete")) {
							p.requestPinDelete = Boolean.parseBoolean(token2);
						} else if (token.equals("bankPin")) {
							String[] array = new String[p.getBankPins().length];
							array = token2.split(";");
							for (int aa = 0; aa < p
									.getBankPins().length; aa++) {
								p.setBankPin(aa, Integer.parseInt(array[aa]));
							}
							array = null;
						} else if (token.equals("skull-timer")) {
							p.setSkullTimer(Integer.parseInt(token2));
						} else if (token.equals("magic-book")) {
							p.playerMagicBook = SpellBook.find(Integer.parseInt(token2));
						} else if (token.equals("prayer-book")) {
							p.playerPrayerBook = PrayerBook.find(Integer.parseInt(token2));
						} else if (token.equals("dfs-charges")) {
							p.dfsCharge = Integer.parseInt(token2);
						} else if (token.equals("special-amount")) {
							p.specAmount = Double.parseDouble(token2);
							/* PKing */
						} else if (token.equals("pkp")) {
							p.pkp = Integer.parseInt(token2);
						} else if (token.equals("pk-killstreak")) {
							p.killStreak = Integer.parseInt(token2);
						} else if (token.equals("pk-highestkillstreak")) {
							p.highestKillStreak = Integer.parseInt(token2);
						} else if (token.equals("wildy-time")) {
							p.setWildernessTick(Integer.parseInt(token2));
						} else if (token.equals("donP")) {
							p.setDonPoints(Integer.parseInt(token2), false);
						} else if (token.equals("TotalDonAmount")) {
							p.setDonatedTotalAmount(Integer.parseInt(token2),
									false);
						} else if (token.equals("yellPoints")) {
							p.yellPoints = Integer.parseInt(token2);
						} else if (token.equals("loyaltyPoints")) {
							p.setLoyaltyPoints(Integer.parseInt(token2));
						} else if (token.equals("agilityPoints")) {
							p.setAgilityPoints(Integer.parseInt(token2));
						} else if (token.equals("xpLock")) {
							p.expLock = Boolean.parseBoolean(token2);
						} else if (line.startsWith("eloRating")) {
							p.setEloRating(Integer.parseInt(token2), false);
						} else if (line.startsWith("KC")) {
							p.KC = Integer.parseInt(token2);
						} else if (line.startsWith("DC")) {
							p.DC = Integer.parseInt(token2);
							/* END PKing by Ardi */
						} else if (token.equals("teleblock-length")) {
							p.setTeleBlockTimer(Integer.parseInt(token2));
						} else if (token.equals("pc-points")) {
							p.setPcPoints(Integer.parseInt(token2));
						} else if (token.equals("npc-kills")) {
							p.npcKills = Integer.parseInt(token2);
							// } else if (token.equals("mute-end")) {
							// p.setTimedMute(Long.parseLong(token2));
							// // } else if (token.equals("slayerTask")) {
							// // p.slayerTask = Integer.parseInt(token2);
						} else if (token.equals("tasksCompleted")) {
							p.setSlayerTasksCompleted(Integer.parseInt(token2));
						} else if (token.equals("taskName")) {
							p.taskName = token2;
						} else if (token.equals("master")) {
							p.masterName = token2;
						} else if (token.equals("assignmentAmount")) {
							p.assignmentAmount = Integer.parseInt(token2);
						} else if (token.equals("slayerPoints")) {
							p.getSlayerPoints().add(Integer.parseInt(token2));
						} else if (token.equals("duoPoints")) {
							p.getDuoPoints().add(Integer.parseInt(token2));
							// } else if (token.equals("taskAmount")) {
							// p.taskAmount = Integer.parseInt(token2);
						} else if (token.equals("magePoints")) {
							p.magePoints = Integer.parseInt(token2);
						} else if (token.equals("penguinPoints")) {
							p.penguinPoints = Integer.parseInt(token2);
						} else if (token.equals("shopcollect")) {
							p.playerCollect = Long.parseLong(token2);
						} else if (token.equals("playerShopSlots")) {
							p.playerShopSlots = Integer.parseInt(token2);
						} else if (token.equals("playerShopItems")) {
							String[] array = new String[p.playerShopItems.length];
							array = token2.split(";");
							for (int aa = 0; aa < p.playerShopItems.length; aa++) {
								p.playerShopItems[aa] = Integer
										.parseInt(array[aa]);
							}
							array = null;
						} else if (token.equals("playerShopItemsN")) {
							String[] array = new String[p.playerShopItemsN.length];
							array = token2.split(";");
							for (int aa = 0; aa < p.playerShopItemsN.length; aa++) {
								p.playerShopItemsN[aa] = Integer
										.parseInt(array[aa]);
							}
							array = null;
						} else if (token.equals("playerShopItemsPrice")) {
							String[] array = new String[p.playerShopItemsPrice.length];
							array = token2.split(";");
							for (int aa = 0; aa < p.playerShopItemsPrice.length; aa++) {
								p.playerShopItemsPrice[aa] = Integer
										.parseInt(array[aa]);
							}
							array = null;
						} else if (token.equals("autoRet")) {
							p.autoRet = Integer.parseInt(token2);
							// } else if (token.equals("barrowskillcount")) {
							// p.barrowsKillCount = Integer.parseInt(token2);
						} else if (token.equals("flagged")) {
							p.accountFlagged = Boolean.parseBoolean(token2);
						} else if (token.equals("wave")) {
							p.waveId = Integer.parseInt(token2);
						} else if (token.equals("void")) {
							for (int j = 0; j < token3.length; j++) {
								p.voidStatus[j] = Integer.parseInt(token3[j]);
							}
						} else if (token.equals("gwkc")) {
							p.setGwdKillCount(Long.parseLong(token2));
							if (p.gwdCoords()) {
								p.unpackGwdKc();
							}
						} else if (token.equals("fightMode")) {
							p.fightMode = Integer.parseInt(token2);
							// } else if (token.equals("doubleExp")) {
							// p.setDoubleExpLength(Long.parseLong(token2));
						}
						break;
					case 3:
						if (token.equals("character-equip")) {
							p.playerEquipment[Integer.parseInt(
									token3[0])] = Integer.parseInt(token3[1]);
							p.playerEquipmentN[Integer.parseInt(
									token3[0])] = Integer.parseInt(token3[2]);
						}
						break;
					case 4:
						if (token.equals("character-look")) {
							p.playerAppearance[Integer.parseInt(
									token3[0])] = Integer.parseInt(token3[1]);
						}
						break;
					case 5:
						if (token.equals("character-skill")) {
							int skillId = Integer.parseInt(token3[0]);
//							int level = Integer.parseInt(token3[1]);
							int exp = Integer.parseInt(token3[2]);
							p.getSkills().setExperienceAndLevels(skillId, exp);
						}
						break;
					case 6:
						if (token.equals("character-item")) {
							p.playerItems[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.playerItemsN[Integer.parseInt(
									token3[0])] = Integer.parseInt(token3[2]);
						}
						break;
					case 7:
						if (token.equals("character-bank")) {
							p.bankItems0[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems0N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
							p.bankingItems[Integer.parseInt(
									token3[0])] = Integer.parseInt(token3[1]);
							p.bankingItemsN[Integer.parseInt(
									token3[0])] = Integer.parseInt(token3[2]);
						} else if (token.equals("character-bank1")) {
							p.bankItems1[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems1N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank2")) {
							p.bankItems2[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems2N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank3")) {
							p.bankItems3[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems3N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank4")) {
							p.bankItems4[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems4N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank5")) {
							p.bankItems5[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems5N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank6")) {
							p.bankItems6[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems6N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank7")) {
							p.bankItems7[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems7N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						} else if (token.equals("character-bank8")) {
							p.bankItems8[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[1]);
							p.bankItems8N[Integer.parseInt(token3[0])] = Integer
									.parseInt(token3[2]);
						}
						break;
					case 8:
						if (token.equals("character-friend")) {
							p.friends[Integer.parseInt(token3[0])] = Long
									.parseLong(token3[1]);
						}
						break;
					case 9:
						/*
						 * if (token.equals("character-ignore")) {
						 * ignores[Integer.parseInt(token3[0])] =
						 * Long.parseLong(token3[1]); }
						 */
						break;
				}
			} else {
				if (line.equals("[ACCOUNT]")) {
					ReadMode = 1;
				} else if (line.equals("[CHARACTER]")) {
					ReadMode = 2;
				} else if (line.equals("[EQUIPMENT]")) {
					ReadMode = 3;
				} else if (line.equals("[LOOK]")) {
					ReadMode = 4;
				} else if (line.equals("[SKILLS]")) {
					ReadMode = 5;
				} else if (line.equals("[ITEMS]")) {
					ReadMode = 6;
				} else if (line.equals("[BANK]")) {
					ReadMode = 7;
				} else if (line.equals("[FRIENDS]")) {
					ReadMode = 8;
				} else if (line.equals("[IGNORES]")) {
					ReadMode = 9;
				} else if (line.equals("[EOF]")) {
					try {
						characterfile.close();
					} catch (IOException ioexception) {
					}
					return 1;
				}
			}
			try {
				line = characterfile.readLine();
			} catch (IOException ioexception1) {
				EndOfFile = true;
			}
		}
		try {
			characterfile.close();
		} catch (IOException ioexception) {
		}
		return 13;
	}

	/**
	 * Tells us whether or not the player exists for the specified name.
	 *
	 * @param name
	 * @return
	 */
	public static boolean playerExists(String name) {
		File file = new File("./Data/characters/" + name + ".txt");
		return file.exists();
	}

	/**
	 * Saving
	 **/
	public static boolean saveGame(Player p, boolean logout) {
		if (Config.SERVER_DEBUG) {
			// FileManager.writeFile(p);
		}
		if (!Config.saveGame) {
			if (logout)
			p.savedGame.set(true);
			return false;
		}
		if (!p.saveFile || p.newPlayer || !p.saveCharacter || !p.setMode) {
			// System.out.println("first");
			if (logout)
			p.savedGame.set(true);
			return false;
		}
		if (!p.correctlyInitialized) {
			if (logout)
			p.savedGame.set(true);
			return false;
		}
		if (p.getName() == null || PlayerHandler.players[p.getId()] == null) {
			// System.out.println("second");
			if (logout)
			p.savedGame.set(true);
			return false;
		}

		if (WorldType.equalsType(WorldType.SPAWN)) {
			if (!Config.SERVER_DEBUG) {
				boolean savedGame = PlayerSaveSql.saveGamePVP(p, logout);
				PlayerSaveSql.savePasswordBankPinFriendsETC(p, logout);
				if (logout)
					p.savedGame.set(true);
				return savedGame;
			}
		} else {

			if (!Config.SERVER_DEBUG) {
				return PlayerSaveSql.saveGame(p, logout);
			}
		}

		if (!Config.SERVER_DEBUG) {
			if (logout)
				p.savedGame.set(true);
			return true;
		}

		BufferedWriter characterfile = null;
		try {
			if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
				characterfile = new BufferedWriter(new FileWriter(
						"./Data/debugchars/" + p.getName() + ".txt"));
			} else {
				characterfile = new BufferedWriter(new FileWriter(
						"./Data/characters/" + p.getName() + ".txt"));
			}

			/* ACCOUNT */
			characterfile.write("[ACCOUNT]", 0, 9);
			characterfile.newLine();
			characterfile.write("character-username = ", 0, 21);
			characterfile.write(p.getName(), 0, p.getName().length());
			characterfile.newLine();
			characterfile.write("character-password = ", 0, 21);
			try {
				String password = p.playerPass;
				if (!Config.SERVER_DEBUG) {
					password = PasswordHash.createHash(p.playerPass);
				}
				characterfile.write(password, 0, password.length());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// } catch (NoSuchAlgorithmException e) {
			// e.printStackTrace();
			// } catch (InvalidKeySpecException e) {
			// e.printStackTrace();
			// }
			// characterfile.write(p.playerPass, 0, p.playerPass.length());
			characterfile.newLine();
			characterfile.newLine();

			/* CHARACTER */
			characterfile.write("[CHARACTER]", 0, 11);
			characterfile.newLine();
			characterfile.write("last-clan = ", 0, 12);
			characterfile.write(p.lastClanChat, 0, p.lastClanChat.length());
			characterfile.newLine();
			characterfile.write("character-height = ", 0, 19);
			characterfile.write(Integer.toString(p.heightLevel), 0,
					Integer.toString(p.heightLevel).length());
			characterfile.newLine();
			String X = (p.teleportToX > 0
					? Integer.toString(p.teleportToX)
					: Integer.toString(p.absX));
			String Y = (p.teleportToY > 0
					? Integer.toString(p.teleportToY)
					: Integer.toString(p.absY));
			characterfile.write("character-posx = ", 0, 17);
			characterfile.write(/* Integer.toString(p.absX) */X, 0,
					/* Integer.toString(p.absX) */X.length());
			characterfile.newLine();
			characterfile.write("character-posy = ", 0, 17);
			characterfile.write(/* Integer.toString(p.absY) */Y, 0,
					/* Integer.toString(p.absY) */Y.length());
			characterfile.newLine();
			characterfile.write("character-rights = ", 0, 19);
			characterfile.write(Integer.toString(p.playerRights), 0,
					Integer.toString(p.playerRights).length());
			characterfile.newLine();
			characterfile.write("difficulty = ", 0, 13);
			characterfile.write(Integer.toString(p.difficulty), 0,
					Integer.toString(p.difficulty).length());
			characterfile.newLine();
			characterfile.write("time-played = ", 0, 14);
			characterfile.write(Long.toString(p.timePlayed), 0,
					Long.toString(p.timePlayed).length());
			characterfile.newLine();
			characterfile.write("homeTeleActions = ", 0, 18);
			characterfile.write(Integer.toString(p.homeTeleActions), 0,
					Integer.toString(p.homeTeleActions).length());
			characterfile.newLine();
			characterfile.write("UUID = ", 0, 7);
			characterfile.write(p.UUID, 0, p.UUID.length());
			characterfile.newLine();
			characterfile.write("character-title = ", 0, 18);
			characterfile.write(p.playerTitle, 0, p.playerTitle.length());
			characterfile.newLine();
			characterfile.write("character-title-color = ", 0, 24);
			characterfile.write(Integer.toString(p.titleColor), 0,
					Integer.toString(p.titleColor).length());

			characterfile.newLine();
			characterfile.write("titles-unlocked = ", 0, 18);
			String builder = "0";
			for (int title : p.titles) {
				// characterfile.write(p.titles[aj] + ";");
				builder += title + ";";
			}
			characterfile.write(builder);
			builder = null;

			characterfile.newLine();
			characterfile.write("recent-ip = ", 0, 12);
			characterfile.write(p.connectedFrom, 0, p.connectedFrom.length());

			characterfile.newLine();
			for (int i = 0; i < p.lastConnectedFrom.size(); i++) {
				characterfile.write("connected-from = ", 0, 17);
				characterfile.write(p.lastConnectedFrom.get(i), 0,
						p.lastConnectedFrom.get(i).length());
				characterfile.newLine();
			}
			for (int j = 0; j < p.lastKilledPlayers.size(); j++) {
				characterfile.write("killed-players = ", 0, 17);
				characterfile.write(p.lastKilledPlayers.get(j), 0,
						p.lastKilledPlayers.get(j).length());
				characterfile.newLine();
			}
			characterfile.write("lastLoginDay = ", 0, 15);
			characterfile.write(p.lastDayLoggedIn, 0,
					p.lastDayLoggedIn.length());
			characterfile.newLine();
			characterfile.write("lastDayPkedOfYear = ", 0, 20);
			characterfile.write(Integer.toString(p.lastDayPkedOfYear), 0,
					Integer.toString(p.lastDayPkedOfYear).length());
			// start quests and achievs
			characterfile.newLine();
			characterfile.write("quest-points = ", 0, 15);
			characterfile.write(Integer.toString(p.questPoints), 0,
					Integer.toString(p.questPoints).length());
			characterfile.newLine();
			characterfile.write("achievements = ", 0, 15);
			for (int achievement : p.achievements) {
				characterfile.write(achievement + ";");
			}
			characterfile.newLine();
			characterfile.write("achievements-completed = ", 0, 25);
			characterfile.write(Integer.toString(p.achievementsCompleted), 0,
					Integer.toString(p.achievementsCompleted).length());
			characterfile.newLine();
			characterfile.write("lastLoginDate = ", 0, 16);
			characterfile.write(Integer.toString(p.lastLoginDate), 0,
					Integer.toString(p.lastLoginDate).length());
			characterfile.newLine();
			characterfile.write("startPack = ", 0, 12);
			characterfile.write(Boolean.toString(p.obtainedStartPack()), 0,
					Boolean.toString(p.obtainedStartPack()).length());
			characterfile.newLine();
			characterfile.write("setPin = ", 0, 9);
			characterfile.write(Boolean.toString(p.setPin), 0,
					Boolean.toString(p.setPin).length());
			characterfile.newLine();
			characterfile.write("bankPin = ", 0, 10);
			for (int aj = 0; aj < p.getBankPins().length; aj++) {
				characterfile.write(p.getBankPins()[aj] + ";");
			}
			characterfile.newLine();
			characterfile.write("hasBankpin = ", 0, 13);
			characterfile.write(Boolean.toString(p.hasBankPin), 0,
					Boolean.toString(p.hasBankPin).length());
			characterfile.newLine();
			characterfile.write("pinRegisteredDeleteDay = ", 0, 25);
			characterfile.write(Integer.toString(p.pinDeleteDateRequested), 0,
					Integer.toString(p.pinDeleteDateRequested).length());
			characterfile.newLine();
			characterfile.write("requestPinDelete = ", 0, 19);
			characterfile.write(Boolean.toString(p.requestPinDelete), 0,
					Boolean.toString(p.requestPinDelete).length());
			characterfile.newLine();
			characterfile.write("slayerPoints = ", 0, 15);
			characterfile.write(
					Integer.toString(p.getSlayerPoints().getTotalValue()), 0,
					Integer.toString(p.getSlayerPoints().getTotalValue())
							.length());
			characterfile.newLine();
			characterfile.write("duoPoints = ", 0, 12);
			characterfile.write(
					Integer.toString(p.getDuoPoints().getTotalValue()), 0,
					Integer.toString(p.getDuoPoints().getTotalValue())
							.length());
			characterfile.newLine();
			characterfile.write("tasksCompleted = ", 0, 17);
			characterfile.write(Integer.toString(p.getSlayerTasksCompleted()), 0,
					Integer.toString(p.getSlayerTasksCompleted()).length());
			characterfile.newLine();
			characterfile.write("taskName = ", 0, 11);
			characterfile.write(p.taskName, 0, p.taskName.length());
			characterfile.newLine();
			characterfile.write("master = ", 0, 9);
			characterfile.write(p.masterName, 0, p.masterName.length());
			characterfile.newLine();
			characterfile.write("assignmentAmount = ", 0, 19);
			characterfile.write(Integer.toString(p.assignmentAmount), 0,
					Integer.toString(p.assignmentAmount).length());
			characterfile.newLine();

			characterfile.write("skull-timer = ", 0, 14);
			characterfile.write(Integer.toString(p.getSkullTimer()), 0,
					Integer.toString(p.getSkullTimer()).length());
			characterfile.newLine();
			characterfile.write("magic-book = ", 0, 13);
			characterfile.write(Integer.toString(p.playerMagicBook.getId()), 0,
					Integer.toString(p.playerMagicBook.getId()).length());
			characterfile.newLine();
			characterfile.write("prayer-book = ", 0, 14);
			characterfile.write(Integer.toString(p.playerPrayerBook.getId()), 0,
					Integer.toString(p.playerPrayerBook.getId()).length());
			characterfile.newLine();
			characterfile.write("dfs-charges = ", 0, 14);
			characterfile.write(Integer.toString(p.dfsCharge), 0,
					Integer.toString(p.dfsCharge).length());
			characterfile.newLine();

			characterfile.write("barrows = ", 0, 10);
			for (int barrow : p.barrows) {
				characterfile.write(barrow + ";");
			}
			characterfile.newLine();
			characterfile.write("special-amount = ", 0, 17);
			characterfile.write(Double.toString(p.specAmount), 0,
					Double.toString(p.specAmount).length());
			characterfile.newLine();
			/* Elo Ratings */
			characterfile.write("eloRating = ", 0, 12);
			characterfile.write(Integer.toString(p.getEloRating()), 0,
					Integer.toString(p.getEloRating()).length());
			characterfile.newLine();
			/* PKing by Ardi */
			characterfile.write("KC = ", 0, 5);
			characterfile.write(Integer.toString(p.KC), 0,
					Integer.toString(p.KC).length());
			characterfile.newLine();
			characterfile.write("DC = ", 0, 5);
			characterfile.write(Integer.toString(p.DC), 0,
					Integer.toString(p.DC).length());
			characterfile.newLine();
			characterfile.write("pkp = ", 0, 6);
			characterfile.write(Integer.toString(p.pkp), 0,
					Integer.toString(p.pkp).length());
			characterfile.newLine();
			characterfile.write("pk-killstreak = ", 0, 16);
			characterfile.write(Integer.toString(p.killStreak), 0,
					Integer.toString(p.killStreak).length());
			characterfile.newLine();
			characterfile.write("pk-highestkillstreak = ", 0, 23);
			characterfile.write(Integer.toString(p.highestKillStreak), 0,
					Integer.toString(p.highestKillStreak).length());
			characterfile.newLine();
			characterfile.write("wildy-time = ", 0, 13);
			characterfile.write(Integer.toString(p.getWildernessTick()), 0,
					Integer.toString(p.getWildernessTick()).length());
			characterfile.newLine();
			/* END PKing */
			characterfile.write("donP = ", 0, 7);
			characterfile.write(Integer.toString(p.getDonPoints()), 0,
					Integer.toString(p.getDonPoints()).length());
			characterfile.newLine();
			characterfile.write("TotalDonAmount = ", 0, 17);
			characterfile.write(Integer.toString(p.getDonatedTotalAmount()), 0,
					Integer.toString(p.getDonatedTotalAmount()).length());
			characterfile.newLine();
			characterfile.write("yellPoints = ", 0, 13);
			characterfile.write(Integer.toString(p.yellPoints), 0,
					Integer.toString(p.yellPoints).length());
			characterfile.newLine();
			characterfile.write("loyaltyPoints = ", 0, 16);
			characterfile.write(Integer.toString(p.getLoyaltyPoints()), 0,
					Integer.toString(p.getLoyaltyPoints()).length());
			characterfile.newLine();
			characterfile.write("agilityPoints = ", 0, 16);
			characterfile.write(Integer.toString(p.getAgilityPoints()), 0,
					Integer.toString(p.getAgilityPoints()).length());
			characterfile.newLine();
			characterfile.write("xpLock = ", 0, 9);
			characterfile.write(Boolean.toString(p.expLock), 0,
					Boolean.toString(p.expLock).length());
			characterfile.newLine();
			characterfile.write("teleblock-length = ", 0, 19);
			characterfile.write(Integer.toString(p.getTeleBlockTimer()), 0,
					Integer.toString(p.getTeleBlockTimer()).length());
			characterfile.newLine();
			characterfile.write("pc-points = ", 0, 12);
			characterfile.write(Integer.toString(p.getPcPoints()), 0,
					Integer.toString(p.getPcPoints()).length());
			characterfile.newLine();
			characterfile.write("mute-end = ", 0, 11);
			characterfile.write(Long.toString(p.getTimedMute()), 0,
					Long.toString(p.getTimedMute()).length());
			// characterfile.newLine();
			// characterfile.write("slayerTask = ", 0, 13);
			// characterfile.write(Integer.toString(p.slayerTask), 0, Integer
			// .toString(p.slayerTask).length());
			// characterfile.newLine();
			// characterfile.write("taskAmount = ", 0, 13);
			// characterfile.write(Integer.toString(p.taskAmount), 0, Integer
			// .toString(p.taskAmount).length());
			characterfile.newLine();
			characterfile.write("magePoints = ", 0, 13);
			characterfile.write(Integer.toString(p.magePoints), 0,
					Integer.toString(p.magePoints).length());
			characterfile.newLine();
			characterfile.write("penguinPoints = ", 0, 16);
			characterfile.write(Integer.toString(p.penguinPoints), 0,
					Integer.toString(p.penguinPoints).length());
			characterfile.newLine();

			characterfile.write("autoRet = ", 0, 10);
			characterfile.write(Integer.toString(p.autoRet), 0,
					Integer.toString(p.autoRet).length());
			characterfile.newLine();
			characterfile.write("barrowskillcount = ", 0, 19);
			characterfile.write(Integer.toString(p.barrowsKillCount), 0,
					Integer.toString(p.barrowsKillCount).length());
			characterfile.newLine();
			characterfile.write("npc-kills = ", 0, 12);
			characterfile.write(Integer.toString(p.npcKills), 0,
					Integer.toString(p.npcKills).length());
			characterfile.newLine();
			characterfile.write("flagged = ", 0, 10);
			characterfile.write(Boolean.toString(p.accountFlagged), 0,
					Boolean.toString(p.accountFlagged).length());
			characterfile.newLine();
			characterfile.write("wave = ", 0, 7);
			characterfile.write(Integer.toString(p.waveId), 0,
					Integer.toString(p.waveId).length());
			characterfile.newLine();
			characterfile.write("gwkc = ", 0, 7);
			characterfile.write(Long.toString(p.getGwdKillCount()), 0,
					Long.toString(p.getGwdKillCount()).length());
			characterfile.newLine();
			characterfile.write("doubleExp = ", 0, 12);
			characterfile.write(Long.toString(p.getDoubleExpLength()), 0,
					Long.toString(p.getDoubleExpLength()).length());
			characterfile.newLine();
			characterfile.write("fightMode = ", 0, 12);
			characterfile.write(Integer.toString(p.fightMode), 0,
					Integer.toString(p.fightMode).length());
			characterfile.newLine();
			characterfile.write("void = ", 0, 7);
			String toWrite = p.voidStatus[0] + "\t" + p.voidStatus[1] + "\t"
					+ p.voidStatus[2] + "\t" + p.voidStatus[3] + "\t"
					+ p.voidStatus[4];
			characterfile.write(toWrite);
			characterfile.newLine();
			characterfile.write("shopcollect = ", 0, 14);
			characterfile.write(Long.toString(p.playerCollect), 0,
					Long.toString(p.playerCollect).length());
			characterfile.newLine();
			characterfile.write("playerShopSlots = ", 0, 18);
			characterfile.write(Integer.toString(p.playerShopSlots), 0,
					Integer.toString(p.playerShopSlots).length());
			characterfile.newLine();
			characterfile.write("playerShopItems = ", 0, 18);
			for (int playerShopItem : p.playerShopItems) {
				characterfile.write(playerShopItem + ";");
			}
			characterfile.newLine();
			characterfile.write("playerShopItemsN = ", 0, 19);
			for (int element : p.playerShopItemsN) {
				characterfile.write(element + ";");
			}
			characterfile.newLine();
			characterfile.write("playerShopItemsPrice = ", 0, 23);
			for (long element : p.playerShopItemsPrice) {
				characterfile.write(element + ";");
			}
			characterfile.newLine();
			characterfile.newLine();

			/* EQUIPMENT */
			characterfile.write("[EQUIPMENT]", 0, 11);
			characterfile.newLine();
			for (int i = 0; i < p.playerEquipment.length; i++) {
				characterfile.write("character-equip = ", 0, 18);
				characterfile.write(Integer.toString(i), 0,
						Integer.toString(i).length());
				characterfile.write("	", 0, 1);
				characterfile.write(Integer.toString(p.playerEquipment[i]), 0,
						Integer.toString(p.playerEquipment[i]).length());
				characterfile.write("	", 0, 1);
				characterfile.write(Integer.toString(p.playerEquipmentN[i]), 0,
						Integer.toString(p.playerEquipmentN[i]).length());
				characterfile.write("	", 0, 1);
				characterfile.newLine();
			}
			characterfile.newLine();

			/* LOOK */
			characterfile.write("[LOOK]", 0, 6);
			characterfile.newLine();
			for (int i = 0; i < p.playerAppearance.length; i++) {
				characterfile.write("character-look = ", 0, 17);
				characterfile.write(Integer.toString(i), 0,
						Integer.toString(i).length());
				characterfile.write("	", 0, 1);
				characterfile.write(Integer.toString(p.playerAppearance[i]), 0,
						Integer.toString(p.playerAppearance[i]).length());
				characterfile.newLine();
			}
			characterfile.newLine();

			/* SKILLS */
			characterfile.write("[SKILLS]", 0, 8);
			characterfile.newLine();
			for (int i = 0; i < p.getPlayerLevel().length; i++) {
				characterfile.write("character-skill = ", 0, 18);
				characterfile.write(Integer.toString(i), 0,
						Integer.toString(i).length());
				characterfile.write("	", 0, 1);
				characterfile.write(Integer.toString(p.getPlayerLevel()[i]), 0,
						Integer.toString(p.getPlayerLevel()[i]).length());
				characterfile.write("	", 0, 1);
				characterfile.write(Integer.toString(p.getSkills().getExperience(i)), 0,
						Integer.toString(p.getSkills().getExperience(i)).length());
				characterfile.newLine();
			}
			characterfile.newLine();

			/* ITEMS */
			characterfile.write("[ITEMS]", 0, 7);
			characterfile.newLine();
			for (int i = 0; i < p.playerItems.length; i++) {
				if (p.playerItems[i] > 0) {
					characterfile.write("character-item = ", 0, 17);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.playerItems[i]), 0,
							Integer.toString(p.playerItems[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.playerItemsN[i]), 0,
							Integer.toString(p.playerItemsN[i]).length());
					characterfile.newLine();
				}
			}
			characterfile.newLine();

			/* BANK */
			characterfile.write("[BANK]", 0, 6);
			characterfile.newLine();
			for (int i = 0; i < p.bankItems0.length; i++) {
				if (p.bankItems0[i] > 0) {
					characterfile.write("character-bank = ", 0, 17);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems0[i]), 0,
							Integer.toString(p.bankItems0[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems0N[i]), 0,
							Integer.toString(p.bankItems0N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems1.length; i++) {
				if (p.bankItems1[i] > 0) {
					characterfile.write("character-bank1 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems1[i]), 0,
							Integer.toString(p.bankItems1[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems1N[i]), 0,
							Integer.toString(p.bankItems1N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems2.length; i++) {
				if (p.bankItems2[i] > 0) {
					characterfile.write("character-bank2 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems2[i]), 0,
							Integer.toString(p.bankItems2[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems2N[i]), 0,
							Integer.toString(p.bankItems2N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems3.length; i++) {
				if (p.bankItems3[i] > 0) {
					characterfile.write("character-bank3 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems3[i]), 0,
							Integer.toString(p.bankItems3[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems3N[i]), 0,
							Integer.toString(p.bankItems3N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems4.length; i++) {
				if (p.bankItems4[i] > 0) {
					characterfile.write("character-bank4 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems4[i]), 0,
							Integer.toString(p.bankItems4[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems4N[i]), 0,
							Integer.toString(p.bankItems4N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems5.length; i++) {
				if (p.bankItems5[i] > 0) {
					characterfile.write("character-bank5 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems5[i]), 0,
							Integer.toString(p.bankItems5[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems5N[i]), 0,
							Integer.toString(p.bankItems5N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems6.length; i++) {
				if (p.bankItems6[i] > 0) {
					characterfile.write("character-bank6 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems6[i]), 0,
							Integer.toString(p.bankItems6[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems6N[i]), 0,
							Integer.toString(p.bankItems6N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems7.length; i++) {
				if (p.bankItems7[i] > 0) {
					characterfile.write("character-bank7 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems7[i]), 0,
							Integer.toString(p.bankItems7[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems7N[i]), 0,
							Integer.toString(p.bankItems7N[i]).length());
					characterfile.newLine();
				}
			}
			for (int i = 0; i < p.bankItems8.length; i++) {
				if (p.bankItems8[i] > 0) {
					characterfile.write("character-bank8 = ", 0, 18);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems8[i]), 0,
							Integer.toString(p.bankItems8[i]).length());
					characterfile.write("	", 0, 1);
					characterfile.write(Integer.toString(p.bankItems8N[i]), 0,
							Integer.toString(p.bankItems8N[i]).length());
					characterfile.newLine();
				}
			}

			characterfile.newLine();

			/* FRIENDS */
			characterfile.write("[FRIENDS]", 0, 9);
			characterfile.newLine();
			for (int i = 0; i < p.friends.length; i++) {
				if (p.friends[i] > 0) {
					characterfile.write("character-friend = ", 0, 19);
					characterfile.write(Integer.toString(i), 0,
							Integer.toString(i).length());
					characterfile.write("	", 0, 1);
					characterfile.write("" + p.friends[i]);
					characterfile.newLine();
				}
			}
			characterfile.newLine();

			/* IGNORES */
			/*
			 * characterfile.write("[IGNORES]", 0, 9); characterfile.newLine();
			 * for (int i = 0; i < ignores.length; i++) { if (ignores[i] > 0) {
			 * characterfile.write("character-ignore = ", 0, 19);
			 * characterfile.write(Integer.toString(i), 0,
			 * Integer.toString(i).length()); characterfile.write("	", 0, 1);
			 * characterfile.write(Long.toString(ignores[i]), 0,
			 * Long.toString(ignores[i]).length()); characterfile.newLine(); } }
			 * characterfile.newLine();
			 */
			/* EOF */
			characterfile.write("[EOF]", 0, 5);
			characterfile.newLine();
			characterfile.newLine();
			characterfile.flush();
			if (logout)
				p.savedGame.set(true);
		} catch (Exception ioexception) {
			Misc.println(p.getName() + ": error writing file.");
			if (logout)
				p.savedGame.set(false);
			if (characterfile != null) {
				try {
					characterfile.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} finally {
			if (characterfile != null) {
				try {
					characterfile.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		if (logout && !p.getSavedGame()) {
			return false;
		}
		if (logout)
			p.savedGame.set(true);
		// System.out.println("test "+p.forceDisconnect);
		// if
		// (!RS2LoginProtocol.recentlyLoggedOut.contains(p.getName().toLowerCase()))
		// {
		// RS2LoginProtocol.recentlyLoggedOut.add(p.getName().toLowerCase());
		// }
		return true;
	}

}
