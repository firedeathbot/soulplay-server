package com.soulplay.game.model.player.save;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import com.soulplay.content.player.skills.farming.herb.HerbStage;
import com.soulplay.content.player.skills.farming.patch.DefaultStage;
import com.soulplay.content.player.skills.farming.patch.Patch;
import com.soulplay.content.player.skills.farming.patch.PatchStage;
import com.soulplay.content.player.skills.farming.tree.stage.MagicTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.MapleTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.OakTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.WillowTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.YewTreeStage;
import com.soulplay.game.model.player.Client;

public class GsonSave {

	private static class StageDeserializer
			implements
				JsonDeserializer<PatchStage> {

		@Override
		public PatchStage deserialize(final JsonElement json,
				final Type typeOfT, final JsonDeserializationContext context)
				throws JsonParseException {
			try {
				final JsonObject object = json.getAsJsonObject();
				final String stage = object.get("stage").getAsString();
				String message = null;
				if (object.has("message")) {
					message = object.get("message").getAsString();
				}
				if (!stage.contains("STAGE")) {
					return DefaultStage.STAGE_ONE;
				}
				if (message == null) {
					return DefaultStage.valueOf(stage);
				} else if (message.contains("magic")) {
					return MagicTreeStage.valueOf(stage);
				} else if (message.contains("maple")) {
					return MapleTreeStage.valueOf(stage);
				} else if (message.contains("oak")) {
					return OakTreeStage.valueOf(stage);
				} else if (message.contains("willow")) {
					return WillowTreeStage.valueOf(stage);
				} else if (message.contains("yew")) {
					return YewTreeStage.valueOf(stage);
				} else {
					return HerbStage.valueOf(stage);
				}
			} catch (final Exception e) {
				return DefaultStage.STAGE_ONE;
			}
		}
	}

	private static class StageSerializer implements JsonSerializer<PatchStage> {

		@Override
		public JsonElement serialize(final PatchStage src, final Type typeOfSrc,
				final JsonSerializationContext context) {
			final JsonObject object = new JsonObject();
			object.add("stage", new JsonPrimitive(src.current().toString()));
			if (src.getMessage() != null) {
				object.add("message", new JsonPrimitive(src.getMessage()));
			}
			return object;
		}

	}

	public static Gson gson;
	public static Gson gsond;

    public static void load() {
    	/* empty */
    }

	static {
		final GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(PatchStage.class, new StageSerializer());
		builder.registerTypeAdapter(PatchStage.class, new StageDeserializer());
		builder.registerTypeAdapter(HerbStage.class, new StageSerializer());
		builder.registerTypeAdapter(HerbStage.class, new StageDeserializer());
		builder.registerTypeAdapter(DefaultStage.class, new StageSerializer());
		builder.registerTypeAdapter(DefaultStage.class,
				new StageDeserializer());
		builder.registerTypeAdapter(HerbStage.class, new StageSerializer());
		builder.registerTypeAdapter(HerbStage.class, new StageDeserializer());
		builder.registerTypeAdapter(MagicTreeStage.class,
				new StageSerializer());
		builder.registerTypeAdapter(MagicTreeStage.class,
				new StageDeserializer());
		builder.registerTypeAdapter(MapleTreeStage.class,
				new StageSerializer());
		builder.registerTypeAdapter(MapleTreeStage.class,
				new StageDeserializer());
		builder.registerTypeAdapter(OakTreeStage.class, new StageSerializer());
		builder.registerTypeAdapter(OakTreeStage.class,
				new StageDeserializer());
		builder.registerTypeAdapter(WillowTreeStage.class,
				new StageSerializer());
		builder.registerTypeAdapter(WillowTreeStage.class,
				new StageDeserializer());
		builder.registerTypeAdapter(YewTreeStage.class, new StageSerializer());
		builder.registerTypeAdapter(YewTreeStage.class,
				new StageDeserializer());
		builder.setPrettyPrinting();
		GsonSave.gson = builder.create();

		gsond = new GsonBuilder().create();
	}

	public static String FILE_LOCATION = "./Data/farming/";

	public static void loadPatches(final Client c) {
		try {
			Patch[] patches = GsonSave.gson.fromJson(
					FileUtils.readFileToString(
							new File(FILE_LOCATION + c.getName() + ".json")),
					Patch[].class);
			c.setPatches(patches);
		} catch (JsonSyntaxException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

	public static void savePatches(final Client c) {

		File dir = new File(FILE_LOCATION);

		if (!dir.exists()) {
			dir.mkdirs();
		}

		try (Writer writer = new FileWriter(
				FILE_LOCATION + c.getName() + ".json")) {
			GsonSave.gson.toJson(c.getPatches(), writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
