package com.soulplay.game.model.player.save;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.slack.SlackMessageBuilder;

public class HighscoresSaveNew {
	
	public enum HiscoreSlot {
		
		CHAMBERS_OF_XERIC(0);
		
		private final int slot;
		
		private HiscoreSlot(int slot) {
			this.slot = slot;
		}
		
		public int getSlot() {
			return slot;
		}
		
		public static final HiscoreSlot[] VALUES = HiscoreSlot.values();
	}
	
	private Map<Integer, Integer> map = new HashMap<>(HiscoreSlot.VALUES.length);
	
	private final Player player;
	
	public HighscoresSaveNew(Player p) {
		this.player = p;
	}
	
	public int getValue(HiscoreSlot hs) {
		return map.getOrDefault(hs.getSlot(), 0);
	}
	
	public void setValue(HiscoreSlot hs, int value, boolean save) {
		map.put(hs.getSlot(), value);
		if (save) {
			saveScore(player, hs);
		}
	}
	

	private static final String SAVE_Q = "INSERT INTO `players`.`hiscore_new`(`player_id`, `slot`, `value`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)";

	public static void saveScore(Player player, HiscoreSlot hs) {
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(SAVE_Q);) {
				createBatch(player, hs, stmt);
				stmt.executeBatch();
			} catch (Exception e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(SoulPlayDateSave.class, e));
			}

		});
	}
	
	private static void saveAll(Player player) {
		try (Connection connection = Server.getConnection();
				PreparedStatement stmt = connection.prepareStatement(SAVE_Q);) {
			for (int i = 0; i < HiscoreSlot.VALUES.length; i++) {
				HiscoreSlot hs = HiscoreSlot.VALUES[i];
				
				createBatch(player, hs, stmt);
			}
			stmt.executeBatch();
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(SoulPlayDateSave.class, e));
		}
	}
	
	private static void createBatch(Player player, HiscoreSlot hs, PreparedStatement stmt) throws SQLException {
		int value = player.getHiscores().getValue(hs);
		if (value > 0) {
			stmt.setInt(1, player.mySQLIndex);
			stmt.setInt(2, hs.getSlot());
			stmt.setInt(3, value);
			stmt.addBatch();
		}
	}
	
	private static final String LOAD_Q = "SELECT slot, value FROM `players`.`hiscore_new` WHERE `player_id` = ?";
	
	public static boolean load(Player player, Connection con) {
		boolean loaded = false;
		if (con == null) con = Server.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(LOAD_Q);) {

			stmt.setInt(1, player.mySQLIndex);

			loaded = loadResultSet(player, stmt);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return loaded;
	}
	
	private static boolean loadResultSet(Player p, PreparedStatement ps) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				final int slot = rs.getInt("slot");
				final int value = rs.getInt("value");
				p.getHiscores().map.put(slot, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
