package com.soulplay.game.model.player;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.save.PlayerSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.net.Connection;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;
import com.soulplay.util.NameUtil;
import com.soulplay.util.sql.Statistics;

public class PlayerHandler {

	public final static Object lock = new Object();

	public static Player players[] = new Player[Config.MAX_PLAYERS];

	public static String messageToAll = "";

	public static boolean updateAnnounced;

	public static boolean updateRunning;

	public static int updateSeconds;

	public static long updateStartTime;

	public static int scrollsUsed = 0;

	public static int scrollsBought = 0;

	private static int[] nodePlayerCount = new int[Config.MAX_WORLDS]; // 7
																		// worlds
																		// for
																		// now

	public static int playersOnline = 0;

	private static int staffOnline = 0;

	private static int playersInWildy = 0;
	
	private static int playersInCastleWars = 0;
	
	private static int playersInLMS = 0;
	
	private static int playersInSoulWars = 0;
	
	private static int playersInClanWars = 0;
	
	private static int playersInPestControl = 0;
	
	private static int playersInDuelArena = 0;
	
	private static int playersInGamblingArea = 0;

	private static boolean kickAllPlayers = false;

	private static boolean kickedAllPlayers;

	/**
	 * A {@link Map} of player usernames and the player objects.
	 */
	private static final Map<Long, Player> playerMap = new HashMap<>(Config.MAX_PLAYERS);
	
	private static final Map<Long, Player> playerMapByDisplayName = new HashMap<>(Config.MAX_PLAYERS);
	
	private static final Map<Long, Player> playerMapByMID = new HashMap<>(Config.MAX_PLAYERS);

	private static final ArrayList<String> unregisterPlayer = new ArrayList<>();

	private static ArrayList<Player> playersTemp = new ArrayList<>(Config.MAX_PLAYERS);

	public static int disconnectedFromLoginServerCount = 0;

	public static boolean switchVersion;

	public static final int MAX_NPC_ADD_COUNT = 40;

	public static final int MAX_ADD_COUNT = 10;

	public static void alertOwner(String msg) {
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null) {
				continue;
			}
			if (Config.isOwner(p)) {
				final Client c = (Client) p;
				c.sendMessage(msg);
			}
		}
	}
	
	public static void alertAdminsAndOwners(String... msgs) {
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null) {
				continue;
			}
			if (p.playerRights == 2 || Config.isOwner(p)) {
				for (String msg : msgs) {
					p.sendMessage(msg);
				}
			}
		}
	}

	public static void alertStaff(String... msgs) {
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null) {
				continue;
			}
			if (Config.isOwner(p) || p.playerRights == 1
					|| p.playerRights == 2) {
				final Client c = (Client) p;
				for (String msg : msgs) {
					c.sendMessage(msg);
				}
			}
		}
	}

	public static void alertStaffSync(String msg) {
		Server.getTaskScheduler().schedule(new Task(true) {

			@Override
			protected void execute() {
				for (int i = 0, l = players.length; i < l; i++) {
					Player p = players[i];
					if (p == null) {
						continue;
					}
					if (Config.isOwner(p) || p.playerRights == 1
							|| p.playerRights == 2) {
						final Client c = (Client) p;
						c.sendMessage(msg);
					}
				}
				this.stop();
			}
		});

	}

	public static int getEmptyPlayerSlot() {
		int slot = -1;
		for (int i = 1; i < players.length; i++) {
			if (players[i] == null) {
				slot = i;
				break;
			}
		}
		return slot;
	}

	public static int[] getNodePlayerCount() {
		return nodePlayerCount;
	}

	/**
	 * Gets the {@link Player} with the specified username. Note that this will
	 * return {@code null} if the player is offline.
	 *
	 * @param username
	 *            The username.
	 * @return The player.
	 */
	public static Player getPlayer(String username) {
		return playerMap.get(NameUtil.encodeBase37(username));
	}
	
	public static Player getPlayerSmart(String username) {
		Player p =  playerMap.get(NameUtil.encodeBase37(username));
		if (p == null)
			p = playerMapByDisplayName.get(NameUtil.encodeBase37(username));
		return p;
	}

	public static Player getPlayerByLong(long nameLong) {
		return playerMap.get(nameLong);
	}
	
	public static Player getPlayerByMID(long mysqlId) {
		return playerMapByMID.get(mysqlId);
	}

	public static int getPlayerCount() {
		if (Config.NODE_ID < 1) {
			return PlayerHandler.getNodePlayerCount()[0]; // return world 1
		}
		return PlayerHandler.getNodePlayerCount()[Config.NODE_ID - 1];// playersOnline;
	}

	public static Map<Long, Player> getPlayerMap() {
		return playerMap;
	}
	
	public static Map<Long, Player> getPlayerMapByMid() {
		return playerMapByMID;
	}
	
	public static Map<Long, Player> getPlayerMapByDisplayName() {
		return playerMapByDisplayName;
	}

	public static int getStaffCount() {
		return staffOnline;
	}

	public static int getWildyCount() {
		return playersInWildy;
	}
	
	public static int getPlayersInCastleWars() {
		return playersInCastleWars;
	}
	
	public static int getPlayersInLMS() {
		return playersInLMS;
	}

	public static int getPlayersInSoulWars() {
		return playersInSoulWars;
	}

	public static int getPlayersInClanWars() {
		return playersInClanWars;
	}

	public static int getPlayersInPestControl() {
		return playersInPestControl;
	}

	public static int getPlayersInDuelArena() {
		return playersInDuelArena;
	}

	public static int getPlayersInGamblingArea() {
		return playersInGamblingArea;
	}

	public static boolean handleYell(final Client c, String playerCommand,
			boolean command) {
		if (Config.SERVER_DEBUG && !Config.isOwner(c)) {
			return true;
		}
		if (Config.NODE_ID == 10) {
			c.sendMessage("You cannot yell on this server...");
			return true;
		}
		if (!(c.playerRights == 7 || c.isMod() || c.isAdmin() || Config.isOwner(c))) {
			if (c.getDonatedTotalAmount() > 900) {
				if (System.currentTimeMillis() - c.lastYell < 10000) {
					c.sendMessage("Sorry, But you must wait 10 seconds before yelling");
					return true;
				}
			} else if (c.getDonatedTotalAmount() > 240) {
				if (System.currentTimeMillis() - c.lastYell < 20000) {
					c.sendMessage("Sorry, But you must wait 20 seconds before yelling");
					return true;
				}
			} else {
				if (System.currentTimeMillis() - c.lastYell < 60000) {
					c.sendMessage("Sorry, But you must wait 1 minute before yelling");
					return true;
				}
			}
		}

		if (Connection.isMuted(c) || c.getTimedMute() > 0) {
			c.sendMessage("You are muted for breaking a rule.");
			return true;
		}

		if (c.isYellMuted()) {
			c.sendMessage("Your yells have been muted for repeated rule breaking.");
			return true;
		}

		if (!LoginServerConnection.instance().isActive()) {
			c.sendMessage(
					"Login server is offline at the moment. Please try again later.");
			return true;
		}

		String message = playerCommand;
		if (message.startsWith("owphat")) {
			return true;
		}

		message = Misc.escape(message);
		if (c.playerRights == 4 || c.playerRights == 5 || c.playerRights == 0 ||
				c.isIronMan() || c.playerRights == 14) {
			if (c.getDonatedTotalAmount() < 250) {
				if (c.yellPoints > 0) {
					c.yellPoints--;
					c.sendMessage("You have " + c.yellPoints + " yells left.");
				} else if (c.yellPoints < 1) {
					c.sendMessage("Sorry, But you dont have any yells left.");
					return true;
				}
			}
		}
		String rankPrefix;
		
		int rankIcon = ChatCrown.values[c.getSetCrown()].getClientIcon() - 1;
		String rankString;
		if (rankIcon >= 0) {
			rankString = "<img=" + rankIcon + ">";
		} else {
			rankString = "";
		}

		rankPrefix = "[Yell]" + rankString + " <col=ff>"+ c.getNameSmartUp() + "</col>:";

		if (Config.isDev(c.getName())) {
			rankPrefix = "[<col=ff0000>Dev</col>] <img=2>" + c.getNameSmartUp() + ":<col=800000>";
		}
		
		if (c.playerRights == 1 && !c.isSemiAdmin()) {
			rankPrefix = "[<col=000099>Moderator</col>] " + rankString +"<col=e8e8e8><shad=0>" + c.getNameSmartUp() + "</shad></col>:<col=000099>";
		}
		
		if (c.playerRights == 2 || c.isSemiAdmin()) {
			rankPrefix = "[<col=ff0000>Admin</col>] " + rankString +"<col=e8e8e8><shad=0>" + c.getNameSmartUp() + "</shad></col>:<col=ff0000>";
		}
		
		if (c.playerRights == 7) {
			rankPrefix = "[<col=008cc6>Informer</col>] " + rankString +"<col=e8e8e8><shad=0>" + c.getNameSmartUp() + "</shad></col>:<col=008cc6>";
		}
		
		if ("Ray".equalsIgnoreCase(c.getName())) {
			rankPrefix = "<shad=0><col=0>[<col=990099>Developer<col=0>] <img=7>"
					+ c.getNameSmartUp() + ":<col=ffffff>";
		}

		if (!Config.RUN_ON_DEDI && !Config.isOwner(c)) {
			PlayerHandler.messageAllPlayersFromYell(rankPrefix + message, c.getNameSmartUp());
			return true;
		}
		
		c.lastYell = System.currentTimeMillis();
		PlayerHandler.messageAllPlayersInAllWorlds(rankPrefix + message, c.getNameSmartUp());
		return true;
	}

	public static boolean isKickAllPlayers() {
		return kickAllPlayers;
	}

	/**
	 * Checks if the {@link Player} with the specified name is online.
	 *
	 * @param username
	 *            The name.
	 * @return {@code true} if the player is online, otherwise {@code false}.
	 */
	public static boolean isPlayerOnline(String username) {
		return playerMap.get(NameUtil.encodeBase37(username)) != null;
	}
	
	public static boolean isPlayerOnline(long mysqlIndex) {
		return playerMapByMID.get(mysqlIndex) != null;
	}

	public static void messageAllPlayers(String msg) {
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || !p.initialized) {
				continue;
			}
			final Client c = (Client) p;
			c.sendMessage(msg);
		}
	}
	
	public static void broadcastMessageLocalWorld(String msg) {
		msg = "<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + msg;

		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || !p.initialized) {
				continue;
			}
			final Client c = (Client) p;
			c.sendMessage(msg);
		}
	}

	public static void messageAllPlayersFromYell(String msg, String name) {
		final String message = msg.length() > Misc.MAX_MESSAGE_SIZE ? msg.substring(0, Misc.MAX_MESSAGE_SIZE) : msg;
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || !p.initialized) {
				continue;
			}

			p.getPacketSender().sendMessage(ChatMessageTypes.YELL_MESSAGE, message, name);
		}
	}

	public static void messageAllPlayersInAllWorlds(String msg, String displayName) {
		final String message = msg.length() > Misc.MAX_MESSAGE_SIZE ? msg.substring(0, Misc.MAX_MESSAGE_SIZE) : msg;
		LoginServerConnection.instance().submit(new Runnable() {
			
			@Override
			public void run() {
				LoginServerConnection.instance().sendGlobalYell1(message, displayName);
			}
		});
	}

	public static boolean newPlayerClient(Client client1) {
		
		final int slot = Misc.findFreePlayerSlot();
		if (slot < 0)
			return false;
		
		client1.setId(slot);

		players[slot] = client1;

		String username = client1.getName();
		playerMap.put(NameUtil.encodeBase37(username), client1);
		if (client1.getDisplayName() != null && !client1.getDisplayName().isEmpty())
			playerMapByDisplayName.put(NameUtil.encodeBase37(client1.getDisplayName()), client1);
		playerMapByMID.put((long)client1.mySQLIndex, client1);

		players[slot]
				.setLongName(Misc.playerNameToInt64(players[slot].getName()));

		if (players[slot].connectedFrom == null
				|| "".equals(players[slot].connectedFrom)) {
			players[slot].connectedFrom = ((InetSocketAddress) client1
					.getSession().remoteAddress()).getAddress()
							.getHostAddress();
		}
		playersOnline++;
		if ((client1.playerRights > 0 && client1.playerRights < 4)
				|| client1.playerRights == 7) {
			staffOnline++;
		}
		players[slot].saveFile = true;
		
		addToUniqueLoginList(client1.UUID, client1.connectedFrom);
		
		return true;
	}
	
	private static int nextRandom = 0;

	public static void process() {

		disconnectedFromLoginServerCount++;
		if (LoginServerConnection.instance().isActive()) {
			disconnectedFromLoginServerCount = 0;
		}

		synchronized (PlayerHandler.lock) {
			
			boolean swapped = false;

			//grab online players
			if (Server.getTotalTicks() > nextRandom) {
				switchVersion = !switchVersion;
				swapped = true;
				nextRandom = Server.getTotalTicks() + Misc.random(120, 150);
			}
			
			int index = 0;
			if (switchVersion) {
				for (int i = 0; i < players.length; i++) {
					if (players[i] != null && players[i].isActiveAtomic()) {
						playersTemp.add(players[i]);
						players[i].processTick = index;
						index++;
					}
				}
			} else {
				for (int i = players.length - 1; i >= 0; i--) {
					if (players[i] != null && players[i].isActiveAtomic()) {
						playersTemp.add(players[i]);
						players[i].processTick = index;
						index++;
					}
				}
			}

			if (isKickAllPlayers()) {
				for (Player p : playersTemp) {
					if (p != null) {
						p.kickPlayer();
						p.forceDisconnect = true;
					}
				}
			}

			if (timeToUpdatePlayerCounts()) {
				playersInWildy = playersInCastleWars = playersInLMS = playersInSoulWars = playersInClanWars = playersInPestControl = playersInDuelArena = playersInGamblingArea = 0;
			}
			
			for (int i = 0, l = playersTemp.size(); i < l; i++) {
				Player p = playersTemp.get(i);
				if (!p.initialized || p.disconnected || !p.isActiveAtomic()) {
					continue;
				}
				try {
					if (p.firstWarning) {
						p.firstWarning = false;
					}
					p.processQueuedPackets();
				} catch (Exception e) {
					e.printStackTrace();
					if (e.getMessage() != null) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildExceptionMessage(PlayerHandler.class, e));
					}
				}
			}
			
			for (int i = 0, l = playersTemp.size(); i < l; i++) {
				try {
					Player p = playersTemp.get(i);
					p.process();
					if (swapped && p.debugPID) {
						p.sendMessage("Randomizing PID.");
					}
					p.getMovement().getNextPlayerMovement();
					p.processInstaSpecs();
					p.processPreCombat();
					p.processAttackInit();
					p.processCombatEvents(); // my addition to process combat that is used in cycle events
					
					p.processDeath(); // process death watch after combat tasks
					
					p.finishedProcess = true;

					if (timeToUpdatePlayerCounts()) {
						if (p.wildLevel > 0) {
							playersInWildy++;
						} else if (p.inSoulWars() || p.inSoulWarsLobby()) {
							playersInSoulWars++;
						} else if (p.inLMSLobby() || p.inLMSArenaCoords()) {
							playersInLMS++;
						} else if (p.inClanWars() || p.inClanWarsLobby() || p.inClanWarsFunPk()) {
							playersInClanWars++;
						} else if (p.inPcBoat() || p.inPcGame()) {
							playersInPestControl++;
						} else if (p.getZones().isInGambleZone()) {
							playersInGamblingArea++;
						} else if (p.inDuelArena()) {
							playersInDuelArena++;
						} else if (p.inCwGame || p.inCwWait() || RSConstants.castleWarsLobby(p.getX(), p.getY())) {
							playersInCastleWars++;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					if (e.getMessage() != null) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildExceptionMessage(PlayerHandler.class, e));
					}
				}
			}

			for (int i = 0, l = playersTemp.size(); i < l; i++) {
				try {
					Player p = playersTemp.get(i);
					p.update();
				} catch (Exception e) {
					e.printStackTrace();
					if (e.getMessage() != null) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildExceptionMessage(PlayerHandler.class, e));
					}
				}
			}
			int playerCount = 0;
			int staffCount = 0;
			for (int i = 0, l = playersTemp.size(); i < l; i++) {
				try {
					Player p = playersTemp.get(i);
					if (p.disconnected && p.properLogout()
							&& (!p.isInCombatDelay()
									|| p.properLogout || isKickAllPlayers() || p.isSaveInProgressForDC())) {

						if (!Config.SERVER_DEBUG) {

							if (!p.isSaveInProgressForDC()) {
								try {
									final Client c = (Client) p;
									c.handleDCLogoutBeforeSave();
								} catch (Exception e) {
									e.printStackTrace();
								}

								try {
									if (isKickAllPlayers()) {
										PlayerSave.saveGame(p, true);
									} else {
										PlayerSaveSql.savePlayer(p);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								p.toggleSaveInProgress();
							}

							if (p.getSavedGame()) { // remove  player only if saved the game. else try to save game again on next tick.
								LoginServerConnection.instance().submit(new Runnable() {

									@Override
									public void run() {
										LoginServerConnection.instance().unregister(p.getName());
									}
								});
								removePlayer(p.getId());
							}
						} else {
							PlayerSave.saveGame(p, true);
							LoginServerConnection.instance().submit(new Runnable() {

								@Override
								public void run() {
									LoginServerConnection.instance().unregister(p.getName());
								}
							});
							removePlayer(p.getId());
						}

						continue;
					} else {
						playerCount++;
						if (p.isStaff()) {
							staffCount++;
						}
						p.clearUpdateFlags();
					}

				} catch (Exception e) {
					e.printStackTrace();
					if (e.getMessage() != null) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildExceptionMessage(PlayerHandler.class, e));
					}
				}
			}
			playersTemp.clear();
			playersOnline = playerCount;
			staffOnline = staffCount;
			if (updateRunning && !updateAnnounced) {
				updateAnnounced = true;
				Server.setUpdateServer(true);
			}
			if (updateRunning && (System.currentTimeMillis()
					- updateStartTime > (updateSeconds * 1000))) {
				setKickAllPlayers(true);
				if (playersOnline <= 0 && !kickedAllPlayers) {
					System.out.println("Kicked all players.");
					kickedAllPlayers = true;
					PlayerSaveSql.alertSlackOfSafeExit();
				}
			}
		}
	}
	
	private static boolean timeToUpdatePlayerCounts() {
		return Server.getTotalTicks() >= 50 && Server.getTotalTicks() % 50 == 0;
	}

	public static void processPackets300ms() {
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || !p.isActive || !p.initialized || p.disconnected || !p.isActiveAtomic()) {
				continue;
			}
			try {
				if (p.firstWarning) {
					p.firstWarning = false;
				}
				p.processQueuedPackets();
			} catch (Exception e) {
				e.printStackTrace();
				if (e.getMessage() != null) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(PlayerHandler.class, e));
				}
			}
		}
	}

	private static void removePlayer(int index) {
		playerMap.remove(NameUtil.encodeBase37(players[index].getName()));
		if (players[index].getDisplayName() != null && !players[index].getDisplayName().isEmpty())
			playerMapByDisplayName.remove(NameUtil.encodeBase37(players[index].getDisplayName()));
		playerMapByMID.remove((long)players[index].mySQLIndex);
		players[index].destruct();
		players[index] = null;
	}

	public static void sendMessageMainThread(final Player c, final String msg) {
		if (!c.isActiveAtomic()) {
			return;
		}
		Server.getTaskScheduler().schedule(new Task(true) {

			@Override
			protected void execute() {

				this.stop();

				if (c != null && !c.disconnected) {
					c.sendMessage(msg);
				}

			}
		});
	}

	public static void setKickAllPlayers(boolean kickAllPlayers) {
		PlayerHandler.kickAllPlayers = kickAllPlayers;
	}

	public static void setNodePlayerCount(int world, int nodePlayerCount) {
		if (world > PlayerHandler.nodePlayerCount.length) {
			return;
		}
		PlayerHandler.nodePlayerCount[world] = nodePlayerCount;
	}

	public static void setNodePlayerCount(int[] nodePlayerCount) {
		PlayerHandler.nodePlayerCount = nodePlayerCount;
	}

	public static void unregisterPlayers() {
		if (!unregisterPlayer.isEmpty()
				&& LoginServerConnection.instance().isActive()) {
			final ArrayList<String> temp = new ArrayList<>();
			temp.addAll(unregisterPlayer);
			unregisterPlayer.clear();
			LoginServerConnection.instance().submit(new Runnable() {
				public void run() {
					if (LoginServerConnection.instance().isActive()) {
						for (String name : temp) {
							LoginServerConnection.instance().unregister(name);
						}
					}
					temp.clear();
				}
			});
		}
	}

	public static void updatePlayerCount() {
		int total = 0;

		for (int element : nodePlayerCount) {
			if (element > 0) {
				total += element;
			}
		}

		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || p.isBot()) {
				continue;
			}
			Client c1 = (Client) p;
			c1.getPacketSender().sendFrame126("<col=ff7000>Total Online: <col=ffb000>"
					+ total, 16026);
		}
	}

	public static void updatePlayerCount2Lines() {
		int total = 0;

		for (int element : nodePlayerCount) {
			if (element > 0) {
				total += element;
			}
		}

		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || p.isBot() || !p.isActive || !p.isActiveAtomic()) {
				continue;
			}
			Client c1 = (Client) p;
			c1.getPacketSender().sendFrame126("<col=ff7000>Total Online: <col=ffb000>"
					+ total, 16026);
			c1.getPacketSender()
					.sendFrame126("<col=ff7000>World " + Config.NODE_ID + ": <col=ffb000>"
							+ (PlayerHandler.getPlayerCount())
							+ " players online", 16027);
		}
	}

	private PlayerHandler() {
	}
	
	private static Set<String> uniqueUUIDVisitorsDaily = new HashSet<>();
	private static Set<String> uniqueIPVisitorsDaily = new HashSet<>();
	
	public static void addToUniqueLoginList(String uuid, String ip) {
		if (!uniqueUUIDVisitorsDaily.contains(uuid))
			uniqueUUIDVisitorsDaily.add(uuid);
		if (!uniqueIPVisitorsDaily.contains(ip))
			uniqueIPVisitorsDaily.add(ip);
	}
	
	public static void updateDailyCountList() {
		Statistics.updateDailyUniqueCount(uniqueUUIDVisitorsDaily.size(), uniqueIPVisitorsDaily.size());
		uniqueUUIDVisitorsDaily.clear();
		uniqueIPVisitorsDaily.clear();
	}
	
	public static void executeForPlayers(Consumer<Player> consumer) {
		for (int i = 0, l = players.length; i < l; i++) {
			Player p = players[i];
			if (p == null || p.isBot() || !p.isActive || !p.isActiveAtomic()) {
				continue;
			}
			consumer.accept(p);
		}
	}
	
}
