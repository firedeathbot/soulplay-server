package com.soulplay.game.model.player.privatemessage;

import com.soulplay.Config;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.util.Misc;

/**
 * A class that handles private messaging.
 *
 * @author Ultimate1
 */

public class Friends {

	/**
	 * The maximum amount of friends.
	 */
	private static final int MAX_FRIENDS = 200;

	/**
	 * The maximum amount of ignores.
	 */
	private static final int MAX_IGNORES = 100;

	// /**
	// * An instance of the login server.
	// */
	// private static LoginServerConnection loginServer = World
	// .getLoginServerConnection();

	/**
	 * The global private message index.
	 */
	private static int privateMessageIndex = 1;

	/**
	 * Adds a friend to the friends list.
	 *
	 * @param friend
	 * @param player
	 */
	public static void addFriend(final long friend, final Player player) {
		final long[] friends = player.getFriends(); // grabs array
		if (size(friends) >= MAX_FRIENDS) {
			return;
		}
		final int i = contains(friends, friend); // if friends array contains
													// the friend long id
		if (i != -1) {
			return;
		}
		
		final int index = getSlot(friends); // gets free slot in friends array
		player.getFriends()[index] = friend; // sets the friend into free slot
		LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().addFriend(index, friend,
					player.getName());
			LoginServerConnection.instance().requestFriendStatus(friend,
					player.getName());
		});
	}

	/**
	 * Adds a name to the player's ignore list.
	 *
	 * @param l
	 * @param player
	 */
	public static void addToIgnore(long l, final Player player) {

		final long[] ignores = player.getIgnores();
		if (size(ignores) >= MAX_IGNORES) {
			return;
		}
		final int i = contains(ignores, l);
		if (i != -1) {
			player.sendMessage("Player is already on your ignore list.");
			return;
		}
		player.getIgnores()[getSlot(ignores)] = l;

		removeFriend(l, player);
		
		LoginServerConnection.instance().submit(() -> LoginServerConnection
				.instance().modifyIgnores(player.getName(), l, true));

	}

	public static void loadIgnoreList(final Player p, long[] ignores) {
		int length = ignores.length;
		System.arraycopy(ignores, 0, p.getIgnores(), 0, length);
		for (int i = length; i < p.getIgnores().length; i++) {
			p.getIgnores()[i] = 0;
		}

		p.getPacketSender().sendIgnoreList(ignores);
	}

	/**
	 * Checks if the contains the specified name.
	 *
	 * @param list
	 * @param name
	 * @return
	 */
	public static int contains(long[] list, long name) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == name) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Gets the amount of players in the specified list.
	 *
	 * @param list
	 * @return
	 */
	public static int getAmount(long[] list) {
		int count = 0;
		for (final long element : list) {
			if (element <= 0) {
				continue;
			}
			count++;
		}
		return count;
	}

	/**
	 * @return the global private message index.
	 */
	public static int getMessageIndex() {
		return privateMessageIndex++;
	}

	/**
	 * Gets a free slot in the specified list.
	 *
	 * @param list
	 * @return
	 */
	private static int getSlot(long[] list) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == 0) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Removes a friend from the friend list.
	 *
	 * @param friend
	 * @param player
	 */
	public static void removeFriend(long friend, final Player player) {
		final long[] friends = player.getFriends();
		final int index = contains(friends, friend);
		if (index == -1) {
			return;
		}
		player.getFriends()[index] = 0;
		LoginServerConnection.instance().submit(() -> LoginServerConnection
				.instance().removeFriend(index, player.getName()));
	}

	/**
	 * Remove a name from the player's ignore list.
	 *
	 * @param l
	 * @param player
	 */
	public static void removeFromIgnore(long l, final Player player) {

		final long[] ignores = player.getIgnores();
		final int i = contains(ignores, l);
		if (i == -1) {
			player.sendMessage(Misc.longToPlayerName2(l)
					+ " is not on your ignore list");
			return;
		}
		player.getIgnores()[i] = 0;

		LoginServerConnection.instance().submit(() -> LoginServerConnection
				.instance().modifyIgnores(player.getName(), l, false));
		
	}

	/**
	 * Gets the size of the specified list.
	 *
	 * @param array
	 * @return
	 */
	private static int size(long[] array) {
		int size = 0;
		for (final long element : array) {
			if (element != 0) {
				size++;
			}
		}
		return size;
	}

	/**
	 * Updates the player's friend list with the friends from login server.
	 *
	 * @param friends
	 * @param worlds
	 * @param chatMode
	 * @param player
	 */
	public static void updateFriendsList(long[] friends, byte[] worlds,
			int chatMode, Player player) {

		final int packShift = (2 | (Config.NODE_ID << 4)); // 2 must be 2 to say
															// loaded friends
															// list, the other
															// part is the world
															// we logging in
															// from
		int length = friends.length;
		System.arraycopy(friends, 0, player.getFriends(), 0, length);
		for (int i = length; i < player.getFriends().length; i++) {
			player.getFriends()[i] = 0;
		}

		player.setPrivateChatSettings(chatMode); // player.privateChatMode =
													// chatMode;
		player.getPacketSender().setPrivateMessaging(packShift/* 2 */);// player.getActionSender().sendPMStatus(2);
		player.getPacketSender().setChatOptions(0, chatMode, 0);// player.getActionSender().sendChatOptions(0,
		// chatMode, 0);
		for (int i = 0; i < player.getFriends().length; i++) {
			if (player.getFriends()[i] <= 0) {
				continue;
			}
			player.getPacketSender().loadPM(player.getFriends()[i], worlds[i]);// player.getActionSender().sendFriend(friends[i],
															// worlds[i]);
		}

	}

}
