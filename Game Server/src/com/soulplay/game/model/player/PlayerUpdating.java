package com.soulplay.game.model.player;

import java.util.Iterator;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.clans.Clan;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.entity.UpdateFlags;
import com.soulplay.util.Misc;
import com.soulplay.util.Stream;

public final class PlayerUpdating {

	public static void addNewPlayer(Player player, Player other, Stream str,
			Stream updateBlock) {
		player.getLocalPlayers().add(other);
		str.writeBits(11, other.getId());
		str.writeBit(true);
		str.writeBit(true);
		
		int dy = other.absY - player.absY;
		
		if (dy < 0) {
			dy += 32;
		}
		
		str.writeBits(5, dy);
		
		int dx = other.absX - player.absX;
		
		if (dx < 0) {
			dx += 32;
		}
		
		str.writeBits(5, dx);
	}

	private static void appendAnimationMask(Player player, Stream str) {
		Animation animation = player.getAnimation();

		str.writeLEShort(animation.getId());
		str.writeByteC(animation.getDelay());
	}

	private static void appendFaceEntityMask(Player player, Stream str) {		
		str.writeLEShort(player.getFacing());
	}
	
	private static void appendFaceLocationMask(Player player, Stream str) {		
		str.writeLEShortA(player.getFacingLocation().getX()*2+1);
		str.writeLEShort(player.getFacingLocation().getY()*2+1);
	}

	private static void appendForcedChatMask(Player player, Stream str) {		
		str.writeString(player.getForcedChat());
	}

	private static void appendHitUpdate(Player player, Stream str) {
		str.writeByte(player.getHitMask().getDamage());
		str.writeByte(player.getHitMask().getHitSplat());
		str.writeByte(player.getHitMask().getHitIcon());
		str.writeShort(player.getSkills().getLifepoints());
		str.writeShort(player.calculateMaxLifePoints());
	}

	private static void appendHitUpdate2(Player player, Stream str) {
		str.writeByte(player.getHitMask2().getDamage());
		str.writeByte(player.getHitMask2().getHitSplat());
		str.writeByte(player.getHitMask2().getHitIcon());
	}

	private static void appendMask100Update(Player player, Stream str) {	
		Graphic graphic = player.getGraphic();	
		
		str.writeLEShort(graphic.getId());
		str.writeInt(graphic.getType().getHeight() | graphic.getDelay());
	}

	private static void appendMask400Update(Player player, Player myPlayer, Stream str) {
		int offsetX = ((player.movedX > 0 ? player.movedX : player.getCurrentLocation().getX()) - (myPlayer.mapRegionX << 3));
		int offsetY = ((player.movedY > 0 ? player.movedY : player.getCurrentLocation().getY()) - (myPlayer.mapRegionY << 3));
		str.writeByteS(offsetX + player.x1);
		str.writeByteS(offsetY + player.y1);
		str.writeByteS(offsetX + player.x2);
		str.writeByteS(offsetY + player.y2);
		str.writeLEShortA(player.speed1);
		str.writeShortA(player.speed2);
		str.writeByteS(player.direction);
	}

	private static void appendPlayerAppearance(Player player, Stream str) {
		Stream playerProps = player.playerProps;

		if (playerProps.currentOffset == 0) {

		int bitPack = player.playerAppearance[0];
		if (!player.playerTitle.isEmpty())
			bitPack |= 0x2;
		if (player.getCompCape() != null)
			bitPack |= 0x4;
		if (player.getClan() != null)
			bitPack |= 0x8;

		playerProps.writeByte(bitPack);
		if ((bitPack & 0x2) != 0) {
			playerProps.writeString(player.playerTitle);
			playerProps.writeByte(player.titleColor);
		}

		if ((bitPack & 0x8) != 0) {
			Clan clan = player.getClan();
			playerProps.writeInt(clan.getClanMysqlId());
			playerProps.writeString(clan.getClanTag() != null ? clan.getClanTag() : "");
		}

		playerProps.writeByte(player.getHeadIcon());
		playerProps.writeByte(player.headIconPk);

		if (player.getTransformId() == -1) {
			if (player.playerEquipment[PlayerConstants.playerHat] > 1) {
				if (player.displayEquipment[PlayerConstants.playerCape] == 4041
						|| player.displayEquipment[PlayerConstants.playerCape] == 4042) {
					playerProps.write3Byte(0);
				} else {
					playerProps.write3Byte(0x200
							+ player.playerEquipment[PlayerConstants.playerHat]);
				}
			} else {
				playerProps.write3Byte(0);
			}

			if (player.displayEquipment[PlayerConstants.playerCape] > 1) { // just
				// display
				// diff cape
				playerProps.write3Byte(0x200
						+ player.displayEquipment[PlayerConstants.playerCape]);
			} else if (player.playerEquipment[PlayerConstants.playerCape] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerCape]);
			} else {
				playerProps.write3Byte(0);
			}

			if (player.playerEquipment[PlayerConstants.playerAmulet] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerAmulet]);
			} else {
				playerProps.write3Byte(0);
			}

			if (player.displayEquipment[PlayerConstants.playerWeapon] > 1) { // display diff weapon than what is equipped
				playerProps.write3Byte(0x200
						+ player.displayEquipment[PlayerConstants.playerWeapon]);
			} else if (player.playerEquipment[PlayerConstants.playerWeapon] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerWeapon]);
			} else {
				playerProps.write3Byte(0);
			}

			if (player.playerEquipment[PlayerConstants.playerChest] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerChest]);
			} else {
				playerProps
						.write3Byte(0x100 + player.playerAppearance[2]);
			}

			if (player.playerEquipment[PlayerConstants.playerShield] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerShield]);
			} else {
				playerProps.write3Byte(0);
			}

			if (!ItemProjectInsanity.isFullMask(
					player.playerEquipment[PlayerConstants.playerChest])) {
				playerProps
						.write3Byte(0x100 + player.playerAppearance[3]);
			} else {
				playerProps.write3Byte(0);
			}

			if (player.playerEquipment[PlayerConstants.playerLegs] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerLegs]);
			} else {
				playerProps
						.write3Byte(0x100 + player.playerAppearance[5]);
			}

			if (!ItemProjectInsanity.isFullMask(
					player.playerEquipment[PlayerConstants.playerHat])) {
				if (player.displayEquipment[PlayerConstants.playerCape] == 4041
						|| player.displayEquipment[PlayerConstants.playerCape] == 4042) {
					playerProps.write3Byte(0);
				} else {
					playerProps
							.write3Byte(0x100 + player.playerAppearance[1]);
				}
			} else {
				playerProps.write3Byte(0);
			}

			if (player.playerEquipment[PlayerConstants.playerHands] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerHands]);
			} else {
				playerProps
						.write3Byte(0x100 + player.playerAppearance[4]);
			}

			if (player.playerEquipment[PlayerConstants.playerFeet] > 1) {
				playerProps.write3Byte(0x200
						+ player.playerEquipment[PlayerConstants.playerFeet]);
			} else {
				playerProps
						.write3Byte(0x100 + player.playerAppearance[6]);
			}

			boolean showBeard = ItemProjectInsanity.showBeard(
					player.playerEquipment[PlayerConstants.playerHat]);

			// if (playerEquipment[playerHat] > 1)
			// showBeard =
			// ItemDefinition.forId(playerEquipment[playerHat]).showBeard();

			if (player.playerAppearance[0] != 1
					&& showBeard/*
								 * !Item.isFullMask(playerEquipment[playerHat])
								 */) {
				playerProps
						.write3Byte(0x100 + player.playerAppearance[7]);
			} else {
				if (player.playerAppearance[0] != 1
						&& (player.displayEquipment[PlayerConstants.playerCape] == 4041
								|| player.displayEquipment[PlayerConstants.playerCape] == 4042)) {
					playerProps
							.write3Byte(0x100 + player.playerAppearance[7]);
				} else {
					playerProps.write3Byte(0);
				}
			}
		} else {
			playerProps.write3Byte(-1);
			playerProps.write3Byte(player.getTransformId());
		}
		
		if ((bitPack & 0x4) != 0) {
			playerProps.writeShort(2);
			
			playerProps.writeShort(20769);
			playerProps.writeByte(1);
			playerProps.writeByte(player.getCompCape().getColors().length);
			for (int i = 0; i < player.getCompCape().getColors().length; i++) {
				playerProps.writeByte(i);
				playerProps.writeShort(player.getCompCape().getColors()[i]);
			}
		}

		playerProps.writeByte(player.playerAppearance[8]);
		playerProps.writeByte(player.playerAppearance[9]);
		playerProps.writeByte(player.playerAppearance[10]);
		playerProps.writeByte(player.playerAppearance[11]);
		playerProps.writeByte(player.playerAppearance[12]);
		// playerProps.writeShort(playerStandIndex); // standAnimIndex
		// playerProps.writeShort(playerTurnIndex); // standTurnAnimIndex
		// playerProps.writeShort(playerWalkIndex); // walkAnimIndex
		// playerProps.writeShort(playerTurn180Index); // turn180AnimIndex
		// playerProps.writeShort(playerTurn90CWIndex); // turn90CWAnimIndex
		// playerProps.writeShort(playerTurn90CCWIndex); // turn90CCWAnimIndex
		// playerProps.writeShort(playerRunIndex); // runAnimIndex

		for (int j = 0; j < player.getMovement().movementAnimations.length; j++) {
//			if (j == 0 && player.getLocalPlayers().size() > 200) {
//				playerProps.writeShort(6);
//				continue;
//			}
			playerProps.writeShort(player.getMovement().movementAnimations[j]);
		}

		playerProps.writeLong(
				player.getLongNameSmart()/* Misc.playerNameToInt64(playerName) */);
		playerProps.writeByte(player.combatLevel); // combat level
		playerProps.writeShort(0);
		}

		str.writeByteC(playerProps.currentOffset);
		str.writeBytes(playerProps.buffer,
				playerProps.currentOffset, 0);
	}

	private static void appendPlayerChatText(Player player, Stream str) {
		// synchronized(this) {
		str.writeLEShort(((player.getChatTextColor() & 0xFF) << 8)
				+ (player.getChatTextEffects() & 0xFF));
		if (Config.SERVER_DEBUG) {
			str.writeByte(player.playerRights);
		} else {
			str.writeByte(ChatCrown.values[player.getSetCrown()].getClientIcon());
		}
		str.writeByteC(player.getChatTextSize());
		str.writeBytes_reverse(player.getChatText(), player.getChatTextSize(),
				0);
	}

	private static void appendPlayerUpdateBlock(Player otherPlayer, Stream str,
			Player thisPlayer, boolean forceAppearance, boolean needsBlock) {

		if (!otherPlayer.isUpdateRequired() && !forceAppearance) {
			return;
		}

		UpdateFlags updateFlags = otherPlayer.getUpdateFlags();

		int mask = updateFlags.get();

		if (mask >= 0x100) {
			mask |= 0x40;
			str.writeByte(mask & 0xFF);
			str.writeByte(mask >> 8);
		} else {
			str.writeByte(mask);
		}

		if (updateFlags.contains(UpdateFlag.FORCE_MOVEMENT)) {
			appendMask400Update(otherPlayer, thisPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.GRAPHICS)) {
			appendMask100Update(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.ANIMATION)) {
			appendAnimationMask(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.FORCED_CHAT)) {
			appendForcedChatMask(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.CHAT)) {
			appendPlayerChatText(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.ENTITY_INTERACTION)) {
			appendFaceEntityMask(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.APPEARANCE)) {
			appendPlayerAppearance(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.FACE_COORDINATE)) {
			appendFaceLocationMask(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.HIT)) {
			appendHitUpdate(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.HIT2)) {
			appendHitUpdate2(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.BAR_FILL)) {
			appendBarFill(otherPlayer, str);
		}
		if (updateFlags.contains(UpdateFlag.CLONE)) {
			appendClone(otherPlayer, str);
		}
	}

	public static void appendClone(Player player, Stream buffer) {
		PlayerClone clone = player.clone;
		if (clone == null) {
			buffer.writeByte(127);
			return;
		}

		Location offset = clone.getLocation().subtract(player.getCurrentLocation());
		buffer.writeByte(offset.getX());
		buffer.writeByte(offset.getY());
		buffer.writeShort(clone.getTicks() * 30);
	}

	public static void appendBarFill(Player player, Stream buffer) {
		buffer.writeShort(player.startFill);
		buffer.writeByte(player.stayCycleAfterFill);
		buffer.writeByte(player.fill);
		buffer.writeShort(player.maxFill);
		buffer.writeByte(player.reverse ? 1 : 0);
		buffer.writeInt(player.fillColor1);
		buffer.writeInt(player.fillColor2);
	}

	public static void updatePlayer(Player player, Stream buffer) {

		if (PlayerHandler.updateRunning && !PlayerHandler.updateAnnounced) {
			buffer.createPacket(114);
			buffer.writeLEShort(PlayerHandler.updateSeconds * 50 / 30);
		}

		updateThisPlayerMovement(player, buffer);		

		final Stream updateBlock = new Stream(new byte[Config.BUFFER_SIZE]);
		
		if (player.isUpdateRequired()) {
			appendPlayerUpdateBlock(player, updateBlock, player, false, true);
		} 		

		buffer.writeBits(8, player.getLocalPlayers().size());

		int drawDist = 15;

		if (player.getRegion().getPlayers().size() > Config.MAX_PLAYERS_DISPLAY) {
			drawDist = 8;

			if (player.drawDistance < 8) {
				drawDist = player.drawDistance;
			}
		}

		if (!player.getLocalPlayers().isEmpty()) {
			for (Iterator<Player> it = player.getLocalPlayers().iterator(); it
					.hasNext();) {
				
				Player other = it.next();
				
				if (other.getZ() != player.getZ() || !drawOtherPlayers(player, other, drawDist, true) || other.didTeleport) { // removes player who went out
													// of sight
					buffer.writeBit(true);
					buffer.writeBits(2, 3);
					it.remove();
					continue;

				}

				// updates player appearance
				updateOtherPlayerMovement(other, buffer);				
				
				if (other.isUpdateRequired()) {
					appendPlayerUpdateBlock(other, updateBlock, player, false, false);
				}

			}
		}

		int playersAdded = 0;
		
		for (Player other : Server.getRegionManager().getLocalPlayers(player)) {			

			if (!drawOtherPlayers(player, other, drawDist, false)) { // already checking distance inside of RegionManager
				continue;
			}

			if (other == player || player.getLocalPlayers().contains(other)) {
				continue;
			}

			if (player.getLocalPlayers().size() > Config.MAX_PLAYERS_DISPLAY
					|| playersAdded == PlayerHandler.MAX_ADD_COUNT) {
				break;
			}

			addNewPlayer(player, other, buffer, updateBlock);
			
			other.getUpdateFlags().add(UpdateFlag.APPEARANCE);
			if (other.getLastFacing() > 0) {
				other.setLastFace();
			}
			
			appendPlayerUpdateBlock(other, updateBlock, player, true, true);

			playersAdded++;
		}

		if (updateBlock.currentOffset > 0) {
			buffer.writeBits(11, 2047);
			buffer.finishBitAccess();
			buffer.writeBytes(updateBlock.buffer, updateBlock.currentOffset, 0);
		} else {
			buffer.finishBitAccess();
		}

		buffer.endFrameVarSizeWord();
	}
	
	private static boolean drawOtherPlayers(Player myPlayer, Player otherPlayer, int drawDist, boolean distanceCheckReq) {
		if (!otherPlayer.isActive
						|| otherPlayer.isSaveInProgressForDC()
						|| otherPlayer.properLogout) {
			return false;
		}
		
		if (otherPlayer.superHide && myPlayer.playerRights != 3 && myPlayer.playerRights != 2) {
			return false;
		}
		
		if (otherPlayer.hidePlayer && myPlayer.playerRights != 3 && myPlayer.playerRights != 2 && myPlayer.playerRights != 1) {
			return false;
		}
		
		if (myPlayer.getHomeOwnerId() > -1 && myPlayer.getHomeOwnerId() != otherPlayer.getHomeOwnerId()) {
			return false;
		}

		if (!otherPlayer.correctlyInitialized || !otherPlayer.initialized) {
			return false;
		}

		if (distanceCheckReq && !myPlayer.withinDistance(otherPlayer)) {//if (myPlayer.distanceToPoint(otherPlayer.getX(), otherPlayer.getY()) > drawDist) {
			return false;
		}
		return true;
	}

	public static void updateOtherPlayerMovement(Player player, Stream str) {
		if (player.dir1 == -1) {
			if (player.isUpdateRequired()/* || isChatTextUpdateRequired() */) {

				str.writeBits(1, 1);
				str.writeBits(2, 0);
			} else {
				str.writeBits(1, 0);
			}
		} else if (player.runTiles.isEmpty()) {

			str.writeBits(1, 1);
			str.writeBits(2, 1);
			str.writeBits(3, Misc.xlateDirectionToClient[player.dir1]);
			str.writeBits(1,
					(player.isUpdateRequired()/* || isChatTextUpdateRequired() */)
							? 1
							: 0);
		} else {
			str.writeBits(1, 1);
			str.writeBits(2, 2);

			int length = player.runTiles.size();
			str.writeBits(4, length + 1);
			str.writeBits(3, Misc.xlateDirectionToClient[player.dir1]);
			for (int i = 0; i < length; i++) {
				str.writeBits(3, Misc.xlateDirectionToClient[player.runTiles.get(i)]);
			}
			str.writeBits(1,
					(player.isUpdateRequired() /* || isChatTextUpdateRequired() */)
							? 1
							: 0);
		}

	}

	public static void updateThisPlayerMovement(Player player, Stream str) {

		if (player.didTeleport) {
			str.createVariableShortPacket(81);
			str.initBitAccess();
			str.writeBits(1, 1);
			str.writeBits(2, 3);
			str.writeBits(2, player.heightLevel);
			str.writeBits(1, 1);
			str.writeBits(1, (player.isUpdateRequired()) ? 1 : 0);
			str.writeBits(7, player.currentY);
			str.writeBits(7, player.currentX);
			return;
		}

		if (player.dir1 == -1) {
			// don't have to update the character position, because we're just
			// standing
			str.createVariableShortPacket(81);
			str.initBitAccess();
			player.isMoving = false;
			if (player.isUpdateRequired()) {
				// tell client there's an update block appended at the end
				str.writeBits(1, 1);
				str.writeBits(2, 0);
			} else {
				str.writeBits(1, 0);
			}
		} else {
			str.createVariableShortPacket(81);
			str.initBitAccess();
			str.writeBits(1, 1);

			if (player.runTiles.isEmpty()) {
				player.isMoving = true;
				str.writeBits(2, 1);
				str.writeBits(3, Misc.xlateDirectionToClient[player.dir1]);
				if (player.isUpdateRequired()) {
					str.writeBits(1, 1);
				} else {
					str.writeBits(1, 0);
				}
			} else {
				player.isMoving = true;
				str.writeBits(2, 2);

				int length = player.runTiles.size();
				str.writeBits(4, length + 1);
				str.writeBits(3, Misc.xlateDirectionToClient[player.dir1]);
				for (int i = 0; i < length; i++) {
					str.writeBits(3, Misc.xlateDirectionToClient[player.runTiles.get(i)]);
				}
				if (player.isUpdateRequired()) {
					str.writeBits(1, 1);
				} else {
					str.writeBits(1, 0);
				}
			}
		}

	}

	private PlayerUpdating() {

	}

}
