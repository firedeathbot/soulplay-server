package com.soulplay.game.model.player.info;

import com.soulplay.util.EloRatingSystem;

public class SeasonalWildScore {

	private int eloRating = EloRatingSystem.DEFAULT_ELO_START_RATING;
	
	private int killCount;
	
	private int deathCount;
	
	public SeasonalWildScore() {
		
	}
	
	public int getEloRating() {
		return eloRating;
	}

	public void setEloRating(int eloRating) {
		this.eloRating = eloRating;
	}

	public int getKillCount() {
		return killCount;
	}

	public void setKillCount(int killCount) {
		this.killCount = killCount;
	}

	public int getDeathCount() {
		return deathCount;
	}

	public void setDeathCount(int deathCount) {
		this.deathCount = deathCount;
	}
	
}
