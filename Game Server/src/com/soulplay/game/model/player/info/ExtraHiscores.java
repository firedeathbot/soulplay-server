package com.soulplay.game.model.player.info;

import com.soulplay.game.model.player.Player;

public class ExtraHiscores {

    private final Player player;
    
    private int clueEasyCompleted;
    
    private int clueMediumCompleted;
    
    private int clueHardCompleted;
    
    private int clueEliteCompleted;
    
    private int clueMasterCompleted;
    
    private int crystalChestsOpened;
    
    private int pestControlWins;
    
    private int barrowsCompleted;
    
    private int castleWarsWins;
    
    private int wildyKeysUsed;
    
    private int fishingContestWins;
    
    private int barbarianAssaultWins;
    
    private int soulWarsWins;
    
    private int funPkTournamentWins;
    
    private int fightPitsWins;
    
    private int lmsKills;
    
    private int lmsWins;
    
    private int lmsKillRating;
    
    private int lmsWinRating;
    
    private int pkTournamentWins;
    
    private int pkTournamentKills;
    
    private int clwRedPortalKills;
	
    public ExtraHiscores(Player player) {
    	this.player = player;
    }

	public Player getPlayer() {
		return player;
	}

	public int getClueEasyCompleted() {
		return this.clueEasyCompleted;
	}

	public void setClueEasyCompleted(int clueEasyCompleted) {
		this.clueEasyCompleted = clueEasyCompleted;
	}
	
	public void incClueEasyCompleted() {
		this.clueEasyCompleted++;
	}

	public int getClueMediumCompleted() {
		return this.clueMediumCompleted;
	}

	public void setClueMediumCompleted(int clueMediumCompleted) {
		this.clueMediumCompleted = clueMediumCompleted;
	}
	
	public void incClueMediumCompleted() {
		this.clueMediumCompleted++;
	}

	public int getClueHardCompleted() {
		return this.clueHardCompleted;
	}

	public void setClueHardCompleted(int clueHardCompleted) {
		this.clueHardCompleted = clueHardCompleted;
	}
	
	public void incClueHardCompleted() {
		this.clueHardCompleted++;
	}

	public int getClueEliteCompleted() {
		return clueEliteCompleted;
	}

	public void setClueEliteCompleted(int clueEliteCompleted) {
		this.clueEliteCompleted = clueEliteCompleted;
	}
	
	public void incClueEliteCompleted() {
		this.clueEliteCompleted++;
	}

	public int getClueMasterCompleted() {
		return clueMasterCompleted;
	}

	public void setClueMasterCompleted(int clueMasterCompleted) {
		this.clueMasterCompleted = clueMasterCompleted;
	}
	
	public void incClueMasterCompleted() {
		this.clueMasterCompleted++;
	}

	public int getCrystalChestsOpened() {
		return crystalChestsOpened;
	}

	public void setCrystalChestsOpened(int crystalChestsOpened) {
		this.crystalChestsOpened = crystalChestsOpened;
	}
	
	public void incCrystalChestsOpened() {
		this.crystalChestsOpened++;
	}

	public int getPestControlWins() {
		return pestControlWins;
	}

	public void setPestControlWins(int pestControlWins) {
		this.pestControlWins = pestControlWins;
	}
	
	public void incPestControlWins() {
		this.pestControlWins++;
	}

	public int getBarrowsCompleted() {
		return barrowsCompleted;
	}

	public void setBarrowsCompleted(int barrowsCompleted) {
		this.barrowsCompleted = barrowsCompleted;
	}
	
	public void incBarrowsCompleted() {
		this.barrowsCompleted++;
	}

	public int getCastleWarsWins() {
		return castleWarsWins;
	}

	public void setCastleWarsWins(int castleWarsWins) {
		this.castleWarsWins = castleWarsWins;
	}
	
	public void incCastleWarsWins() {
		this.castleWarsWins++;
	}

	public int getWildyKeysUsed() {
		return wildyKeysUsed;
	}

	public void setWildyKeysUsed(int wildyKeysUsed) {
		this.wildyKeysUsed = wildyKeysUsed;
	}

	public void incWildyKeysUsed() {
		this.wildyKeysUsed++;
	}
	
	public int getFishingContestWins() {
		return fishingContestWins;
	}

	public void setFishingContestWins(int fishingContestWins) {
		this.fishingContestWins = fishingContestWins;
	}
	
	public void incFishingContestWins() {
		this.fishingContestWins++;
	}

	public int getBarbarianAssaultWins() {
		return barbarianAssaultWins;
	}

	public void setBarbarianAssaultWins(int barbarianAssaultWins) {
		this.barbarianAssaultWins = barbarianAssaultWins;
	}
	
	public void incBarbarianAssaultWins() {
		this.barbarianAssaultWins++;
	}

	public int getSoulWarsWins() {
		return soulWarsWins;
	}

	public void setSoulWarsWins(int soulWarsWins) {
		this.soulWarsWins = soulWarsWins;
	}
	
	public void incSoulWarsWins() {
		this.soulWarsWins++;
	}

	public int getFunPkTournamentWins() {
		return funPkTournamentWins;
	}

	public void setFunPkTournamentWins(int funPkTournamentWins) {
		this.funPkTournamentWins = funPkTournamentWins;
	}
	
	public void incFunPkTournamentWins() {
		this.funPkTournamentWins++;
	}

	public int getFightPitsWins() {
		return fightPitsWins;
	}

	public void setFightPitsWins(int fightPitsWins) {
		this.fightPitsWins = fightPitsWins;
	}
	
	public void incFightPitsWins() {
		this.fightPitsWins++;
	}

	public int getLmsKills() {
		return lmsKills;
	}

	public void setLmsKills(int lmsKills) {
		this.lmsKills = lmsKills;
	}
	
	public void incLmsKills() {
		this.lmsKills++;
	}

	public int getLmsWins() {
		return lmsWins;
	}
	
	public void incLmsWins() {
		this.lmsWins++;
	}

	public void setLmsWins(int lmsWins) {
		this.lmsWins = lmsWins;
	}

	public int getLmsKillRating() {
		return lmsKillRating;
	}

	public void setLmsKillRating(int lmsKillRating) {
		this.lmsKillRating = lmsKillRating;
	}

	public int getLmsWinRating() {
		return lmsWinRating;
	}

	public void setLmsWinRating(int lmsWinRating) {
		this.lmsWinRating = lmsWinRating;
	}

	public int getPkTournamentWins() {
		return pkTournamentWins;
	}

	public void setPkTournamentWins(int pkTournamentWins) {
		this.pkTournamentWins = pkTournamentWins;
	}
	
	public void incPkTournamentWins() {
		this.pkTournamentWins++;
	}

	public int getPkTournamentKills() {
		return pkTournamentKills;
	}

	public void setPkTournamentKills(int pkTournamentKills) {
		this.pkTournamentKills = pkTournamentKills;
	}
	
	public void incPkTournamentKills() {
		this.pkTournamentKills++;
	}

	public int getClwRedPortalKills() {
		return clwRedPortalKills;
	}

	public void setClwRedPortalKills(int clwRedPortalKills) {
		this.clwRedPortalKills = clwRedPortalKills;
	}
	
	public void incClwRedPortalKills() {
		this.clwRedPortalKills++;
	}
	
}
