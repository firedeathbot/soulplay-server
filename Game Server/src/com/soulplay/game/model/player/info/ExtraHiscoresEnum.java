package com.soulplay.game.model.player.info;

public enum ExtraHiscoresEnum {
	CLUE_EASY("clue_easy") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getClueEasyCompleted();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setClueEasyCompleted(value);
		}
	},
	CLUE_MED("clue_medium") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getClueMediumCompleted();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setClueMediumCompleted(value);
		}
	},
	CLUE_HARD("clue_hard") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getClueHardCompleted();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setClueHardCompleted(value);
		}
	},
	CLUE_ELITE("clue_elite") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getClueEliteCompleted();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setClueEliteCompleted(value);
		}
	},
	CLUE_MASTER("clue_master") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getClueMasterCompleted();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setClueMasterCompleted(value);
		}
	},
	CRYSTAL_CHEST("crystal_chest") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getCrystalChestsOpened();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setCrystalChestsOpened(value);
		}
	},
	PEST_CONTROL("pest_control_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getPestControlWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setPestControlWins(value);
		}
	},
	BARROWS_COMPLETED("barrows_completed") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getBarrowsCompleted();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setBarrowsCompleted(value);
		}
	},
	CASTLEWARS_WINS("castlewars_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getCastleWarsWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setCastleWarsWins(value);
		}
	},
	WILDY_KEYS("wildy_keys") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getWildyKeysUsed();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setWildyKeysUsed(value);
		}
	},
	FISH_CONTEST("fish_contest_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getFishingContestWins();
		}

		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setFightPitsWins(value);
		}
	},
	BARB_ASSAULT_WINS("barbarian_assault_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getBarbarianAssaultWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setBarbarianAssaultWins(value);
		}
	},
	SOULWARS_WINS("soulwars_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getSoulWarsWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setSoulWarsWins(value);
		}
	},
	FUNPK_TOURN_WINS("funpk_tournament_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getFunPkTournamentWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setFunPkTournamentWins(value);
		}
	},
	FIGHT_PITS_WINS("fight_pits_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getFightPitsWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setFightPitsWins(value);
		}
	},
	LMS_WINS("lms_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getLmsWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setLmsWins(value);
		}
	},
	LMS_WIN_RATING("lms_win_rating") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getLmsWinRating();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setLmsWinRating(value);
		}
	},
	LMS_KILLS("lms_kills") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getLmsKills();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setLmsKills(value);
		}
	},
	LMS_KILL_RATING("lms_kill_rating") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getLmsKillRating();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setLmsKillRating(value);
		}
	},
	PK_TOURNAMENT_WINS("pk_tournament_wins") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getPkTournamentWins();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setPkTournamentWins(value);
		}
	},
	PK_TOURNAMENT_KILLS("pk_tournament_kills") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getPkTournamentKills();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setPkTournamentKills(value);
		}
	},
	CLW_RED_PORTAL_KILLS("clw_red_portal_kills") {
		@Override
		public int getValue(ExtraHiscores h) {
			return h.getClwRedPortalKills();
		}
		@Override
		public void setValue(ExtraHiscores h, int value) {
			h.setClwRedPortalKills(value);
		}
	}
	
	;
	
	public static final ExtraHiscoresEnum[] values = ExtraHiscoresEnum.values();
	
	private String sqlString;
	
	private ExtraHiscoresEnum(String str) {
		this.sqlString = str;
	}
	
	public String getColumnName() {
		return sqlString;
	}
	
	public int getValue(ExtraHiscores h) {
		return 0;
	}
	
	public void setValue(ExtraHiscores h, int value) {
	}
}
