package com.soulplay.game.model.player.info;

public class ChatSettings {

	public enum CLAN {
		ON, FRIENDS, OFF;
		public static final CLAN[] values = values();
	}

	public enum PRIVATE {
		ON, FRIENDS, OFF;
		public static final PRIVATE[] values = values();
	}

	public enum PUBLIC {
		ON, FRIENDS, OFF, HIDE;
		public static final PUBLIC[] values = values();
	}

	public enum TRADE {
		ON, FRIENDS, OFF;
		public static final TRADE[] values = values();
	}

	public static final int PUBLIC_INDEX = 0;

	public static final int PRIVATE_INDEX = 1;

	public static final int CLAN_INDEX = 2;

	public static final int TRADE_INDEX = 3;

}
