package com.soulplay.game.model.player.info;

import com.soulplay.game.world.entity.Animation;

public class AnimationsData {

	public static final Animation STUNNED = new Animation(404);
	public static final Animation PICK_UP_FROM_GROUND = new Animation(827);
	public static final Animation CHICKEN_DANCE = new Animation(1835);
	public static final Animation CHICKEN_WHACK = new Animation(1833);
	public static final Animation SPOOKED = new Animation(2383);
	public static final Animation ELECTRICUTED = new Animation(3170);
	public static final Animation STEP_BACK_PANIC_HANDS_IN_AIR = new Animation(5206);
	public static final Animation PICK_UP_FROM_GROUND_HUNTER = new Animation(5212);
	

}
