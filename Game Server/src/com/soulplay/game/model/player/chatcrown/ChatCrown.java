package com.soulplay.game.model.player.chatcrown;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.ChatCrownsSql;

public enum ChatCrown {

	/**
	 * The ORDER of the crowns must be the SAME as the order in the client
	 * interface for chat icons.
	 */
	IRONMAN(null, 0, 13, 0, -1, "Ironman"),
	HC_IRONMAN(null, 0, 25, 0, -1, "HC Ironman"),
	REGULAR(null, 1961, 0, 74058, 0, "Player"),
	PK_BRONZE("pk_bronze", 1962, 15, 74062, 1, "Killer"),
	PK_STEEL("pk_steel", 1963, 16, 74066, 2, "Crusader"),
	PK_ADAMANT("pk_adamant", 1964, 17, 74070, 3, "Psychic"),
	PK_RUNITE("pk_runite", 1965, 18, 74074, 4, "Destroyer"),
	PK_DRAGON("pk_dragon", 1966, 19, 74078, 5, "Leader"),
	RED_DONOR("red_donor", 1967, 4, 74082, 6, "Donator"),
	BLUE_DONOR("blue_donor", 1968, 5, 74086, 7, "Super"),
	GREEN_DONOR("green_donor", 1969, 6, 74090, 8, "Supreme"),
	PURPLE_DONOR("purple_donor", 1970, 11, 74094, 9, "Legendary"),
	GOLD_DONOR("gold_donor", 1971, 12, 74098, 10, "Uber"),
	INSANE("insane", 1972, 23, 74102, 11, "Insane"),
	TRUSTED("trusted", 1973, 24, 74106, 12, "Trusted"),
	YOUTUBE_RANK("youtuber", 1974, 14, 74110, 13, "Youtuber"),
	VETERAN("veteran", 1975, 9, 74114, 14, "Veteran"),
	INFORMER("informer", 1976, 7, 74118, 15, "Helper"),
	MOD_CROWN("moderator", 1977, 1, 74122, 16, "Moderator"),
	ADMIN_CROWN("admin", 1978, 2, 74126, 17, "Admin"),
	PRESTIGE_1("prestige_1", 1979, 20, 74130, 18, "Lord"),
	PRESTIGE_2("prestige_2", 1980, 21, 74134, 19, "Legend"),
	PRESTIGE_3("prestige_3", 1981, 22, 74138, 20, "Extreme")
	;

	private final String tableName, prettyName;

	private final int toggleConfig;

	private final int clientChatIcon, buttonId, radioId;

	private ChatCrown(String tableName, int toggleConfig, int clientChatIcon, int buttonId, int radioId, String prettyName) {
		this.tableName = tableName;
		this.toggleConfig = toggleConfig;
		this.clientChatIcon = clientChatIcon;
		this.buttonId = buttonId;
		this.radioId = radioId;
		this.prettyName = prettyName;
	}

	public int getClientIcon() {
		return clientChatIcon;
	}

	public int getLockConfig() {
		return toggleConfig;
	}

	public String getTableName() {
		return tableName;
	}

	public int getButtonId() {
		return buttonId;
	}

	public int getRadioId() {
		return radioId;
	}

	public String getPrettyName() {
		return prettyName;
	}

	public static boolean isCrownUnlocked(Player c, ChatCrown crown) {
		return c.getUnlockedCrowns()[crown.ordinal()] == 1;
	}

	public static void checkUnlockReqs(Player c) {
		if (!isCrownUnlocked(c, VETERAN)) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -2);
			Date prevYear = cal.getTime();
			if (c.getDaysPlayed() >= 55 && c.getJoinedDate().before(prevYear)) {
				ChatCrownsSql.unlockChatCrown(c, VETERAN);
			}
		}
	}

	public static final ChatCrown[] values = values();
	public static int RADIO_BUTTON_CONFIG = 1960;

	private static Map<Integer, ChatCrown> map = new HashMap<>();
	
	public static void init() {
		for (ChatCrown crown : values) {
			if (crown.getButtonId() > 0) {
				map.put(crown.getButtonId(), crown);
			}
		}
	}
	
	public static ChatCrown getByButton(int buttonId) {
		return map.get(buttonId);
	}
}
