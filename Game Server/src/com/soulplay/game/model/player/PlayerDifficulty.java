package com.soulplay.game.model.player;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public class PlayerDifficulty {

	public static final int NORMAL = 0;

	public static final int MEDIUM = 1;

	public static final int HARD = 2;

	public static final int SUPREME = 3;

	public static final int IRONMAN = 4;
	
	public static final int HC_IRONMAN = 5;
	public static final int HC_IRONMAN_DEAD = 6;

	public static final int PRESTIGE_ONE = 15;

	public static final int PRESTIGE_TWO = 16;

	public static final int PRESTIGE_THREE = 17;

	public void ironmanUpgrade(Client c) {
		if (c.getItems().freeSlots() > 5) {
			c.startGraphic(Graphic.create(2321, GraphicType.HIGH));
			c.startGraphic(Graphic.create(2040));
		}
		c.getItems().addItem(20857, 1);
		c.getItems().deleteItemInOneSlot(995, 100000000);
		c.sendMessage("<img=6>Congratulation you have obtained the sir owens!");
	}

	public void prestigeOne(Client c) {
		if (WorldType.isSeasonal()) {
			c.sendMessage("You can't do this in a seasonal world.");
			return;
		}

		if (c.getItems().freeSlots() <= 5) {
			c.sendMessage("You need more than 5 free inventory slots to do this.");
			return;
		}

		c.difficulty = PlayerDifficulty.PRESTIGE_ONE;
		ChatCrownsSql.unlockChatCrown(c, ChatCrown.PRESTIGE_1);
		Titles.upgradeLord(c);
		for (int i = 0; i < c.getSkills().getDynamicLevels().length; i++) {
			c.getSkills().setStaticLevel(i, i == Skills.HITPOINTS ? 10 : 1);
		}

		c.calcCombat();
		if (c.getClan() != null) {
			c.getClan().addClanPoints(c, 2000, "prestige_one");
		}

		c.getItems().addItem(995, 200000000);
		PlayerHandler.messageAllPlayers("[<col=ff0000>NEWS</col>]The player <col=ff>" + c.getNameSmartUp()
				+ "</col> just upgraded account to <col=ff>Lord</col>.");
		c.sendMessage("<img=6>Congratulation you have upgraded your account to Lord!");
		c.updateDailyTasksState();
	}

	public void prestigeThree(Client c) {
		if (WorldType.isSeasonal()) {
			c.sendMessage("You can't do this in a seasonal world.");
			return;
		}

		if (c.getItems().freeSlots() <= 5) {
			c.sendMessage("You need more than 5 free inventory slots to do this.");
			return;
		}

		c.difficulty = PlayerDifficulty.PRESTIGE_THREE;
		ChatCrownsSql.unlockChatCrown(c, ChatCrown.PRESTIGE_3);
		Titles.upgradeLord(c);
		Titles.upgradeLegend(c);
		Titles.upgradeExtreme(c);
		for (int i = 0; i < c.getSkills().getDynamicLevels().length; i++) {
			c.getSkills().setStaticLevel(i, i == Skills.HITPOINTS ? 10 : 1);
		}

		c.calcCombat();
		c.startGraphic(Graphic.create(2321, GraphicType.HIGH));
		c.startGraphic(Graphic.create(2040));
		// c.startAnimation(1914);
		if (c.getClan() != null) {
			c.getClan().addClanPoints(c, 10000, "prestige_three");
		}

		c.getItems().addItem(20857, 1);
		c.getItems().addItem(995, 500000000);
		PlayerHandler.messageAllPlayers("[<col=ff0000>NEWS</col>]The player <col=ff>" + c.getNameSmartUp()
				+ "</col> just upgraded account to <col=ff>Extreme</col>.");
		c.sendMessage("<img=6>Congratulation you have upgraded your account to Extreme!");
		c.updateDailyTasksState();
	}

	public void prestigeTwo(Client c) {
		if (WorldType.isSeasonal()) {
			c.sendMessage("You can't do this in a seasonal world.");
			return;
		}

		if (c.getItems().freeSlots() <= 5) {
			c.sendMessage("You need more than 5 free inventory slots to do this.");
			return;
		}

		c.difficulty = PlayerDifficulty.PRESTIGE_TWO;
		ChatCrownsSql.unlockChatCrown(c, ChatCrown.PRESTIGE_2);
		Titles.upgradeLord(c);
		Titles.upgradeLegend(c);
		for (int i = 0; i < c.getPlayerLevel().length; i++) {
			c.getSkills().setStaticLevel(i, i == Skills.HITPOINTS ? 10 : 1);
		}

		c.calcCombat();
		if (c.getClan() != null) {
			c.getClan().addClanPoints(c, 5000, "prestige_two");
		}

		c.getItems().addItem(995, 300000000);
		PlayerHandler.messageAllPlayers("[<col=ff0000>NEWS</col>]The player <col=ff>" + c.getNameSmartUp()
				+ "</col> just upgraded account to <col=ff>Legend</col>.");
		c.sendMessage("<img=6>Congratulation you have upgraded your account to Legend!");
		c.updateDailyTasksState();
	}

}