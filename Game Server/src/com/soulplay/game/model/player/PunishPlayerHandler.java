package com.soulplay.game.model.player;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

public class PunishPlayerHandler {

	public static void jailPlayer(Player mod, Player toJail, final double hours, final String reason) {

		if (toJail == null || toJail.disconnected) {
		    mod.sendMessage("Player is not online");
		    return;
		}

		if (toJail.duelStatus == 5) {
		    mod.sendMessage("You cant jail a player when he is during a duel.");
		    return;
		}

		if (toJail.inMinigame()) {
		    mod.sendMessage("They are inside a minigame. Kick them first then jail.");
		    return;
		}

		if (!Config.SERVER_DEBUG && (!mod.getName().equals(toJail.getName()))) {
		    Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(mod,
			    toJail.getName(), "Jail (" + hours + " hours)", reason));
		}

		final int ticks = Misc.secondsToTicks((int) (hours * 60 * 60));

		toJail.setJailTime(ticks);
		toJail.getPA().movePlayer(2095, 4428, 0);
		toJail.getDotManager().reset();
		toJail.sendMessage("You have been jailed by " + mod.getNameSmartUp() + " for " + hours + " hours.");
		
		mod.sendMessage("Successfully Jailed " + toJail.getNameSmartUp() + " for " + hours + " hours.");
		
		toJail.getDialogueBuilder().sendStatement("Jailed by: " + mod.getNameSmartUp(), "Time remaining: " + hours,
				"Reason: " + reason).execute();
	}
}
