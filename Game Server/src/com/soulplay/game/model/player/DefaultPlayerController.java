package com.soulplay.game.model.player;

import com.soulplay.game.model.item.definition.MorphingRing;

public final class DefaultPlayerController extends PlayerController {

    public DefaultPlayerController(Player player) {
	super(player);
    }

    @Override
    public boolean canClickObject() {
	// ring of stone and easter ring
	if (MorphingRing.getRing(player) != null && player.getTransformId() != -1) {
	    return false;
	}
	
	return true;
    }

    @Override
    public boolean canClickNpc() {
	// ring of stone and easter ring
	if (MorphingRing.getRing(player) != null && player.getTransformId() != -1) {
	    return false;
	}	
	
	return true;
    }

    @Override
    public boolean canWalk() {
	// ring of stone and easter ring
	if (MorphingRing.getRing(player) != null && player.getTransformId() != -1) {
	    player.sendMessage("Unmorph to start walking again.");
	    return false;
	}
	
	return true;
    }

    @Override
    public boolean canUseCommands() {
	// ring of stone and easter ring
	if (MorphingRing.getRing(player) != null && player.getTransformId() != -1) {
	    player.sendMessage("You can't use commands while you're morphed!");
	    return false;
	}
	
	return true;
    }

	@Override
	public boolean canPickupItem() {
		// ring of stone and easter ring
		if (MorphingRing.getRing(player) != null && player.getTransformId() != -1) {
		    player.sendMessage("You can't pickup items while you're morphed!");
		    return false;
		}

		return true;
	}

}
