package com.soulplay.game.model.player.itemcharge;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.definition.RecoilRingEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;

public class RecoilRings {

	private final Player player;
	
	private static final int SIZE = RecoilRingEnum.values.length;
	
	private int[] charges = new int[SIZE];

	public int[] getCharges() {
		return charges;
	}

	public void loadJsonString(String jsonString) {
		if (jsonString == null)
			return;
		charges = Arrays.copyOf((int[])GsonSave.gsond.fromJson(jsonString, new TypeToken<int[]>(){}.getType()), SIZE);
	}
	
	public RecoilRings(Player p) {
		this.player = p;
	}
	
	public boolean spendCharge(RecoilRingEnum ring) {
		if (ring != null) {
			if (Tournament.insideArenaWithSpawnedGear(player) && ring != RecoilRingEnum.RING_OF_RECOIL)
				return true;
			if (ring.spendCharge(player)) {
				saveMisc();
				return true;
			}
		}
		return false;
	}
	
	public void checkCharges(int itemId) {
		RecoilRingEnum ring = RecoilRingEnum.get(itemId);
		
		int currentCharge = charges[ring.ordinal()];
		player.sendMessage(String.format("You have %d charges left.", currentCharge));
	}
	
	public void chargeEyeRings(Item item1, Item item2) {
		boolean firstItem = false;
		
		RecoilRingEnum ring = RecoilRingEnum.get(item1.getId());
		
		if (ring == null) {
			ring = RecoilRingEnum.get(item2.getId());
			firstItem = true;
		}
		
		final int index = ring.ordinal();
		final int maxCharges = ring.getMaxCharges();
		
		if (charges[index] >= maxCharges) {
			player.sendMessage("You cannot charge this item anymore.");
			return;
		}
		
		final Item otherItem = firstItem ? item1 : item2;
		
		if (otherItem.getAmount() > maxCharges) {
			player.sendMessage(String.format("You can only charge %d max amount.", maxCharges));
			return;
		}
		
		player.getDialogueBuilder().sendOption("Recharge ring with "+otherItem.getAmount()+" Ether?",
				"Yes", ()-> {
					confirmCharge(otherItem, index, maxCharges);
				},
				"No", ()-> {
					player.getDialogueBuilder().returnToCheckpoint(100);
				})
		.execute();
		
	}
	
	private void confirmCharge(Item ether, int index, int maxCharges) {
		int itemCount = player.getItems().getItemCount(ether.getId());
		if (player.getItems().deleteItem(ether)) {
			charges[index] = charges[index] + itemCount;
			if (charges[index] > maxCharges) {
				int giveBack = charges[index] - maxCharges;
				charges[index] = maxCharges;
				player.getItems().addItem(ether.getId(), giveBack);
			}
			saveMisc();
		}
	}
	
	public void shatterRingOfRecoil(Player p) {
		charges[RecoilRingEnum.RING_OF_RECOIL.ordinal()] = 0;
		saveMisc();
	}
	
	
	public void chargeSufferingRings(Item item1, Item item2) {
		boolean firstItemIsRecoilRing = item1.getId() == RecoilRingEnum.RING_OF_RECOIL.getItemId();
		
		RecoilRingEnum ringOfSuffering = RecoilRingEnum.get(firstItemIsRecoilRing ? item2.getId() : item1.getId());
		
		final int index = ringOfSuffering.ordinal();
		final int maxChargesSuffering = ringOfSuffering.getMaxCharges();
		
		if (charges[index] >= maxChargesSuffering) {
			player.sendMessage("You cannot charge this item anymore.");
			return;
		}
		
		final int recoilCharges = RecoilRingEnum.RING_OF_RECOIL.getMaxCharges();
		
		charges[index] = Math.min(maxChargesSuffering, charges[index]+recoilCharges);
		
		player.getItems().deleteItemInSlot(firstItemIsRecoilRing ? item1.getSlot() : item2.getSlot());
		
		saveMisc();
	}
	
	
	private static final String MISC_SAVE_QUERY = "INSERT INTO `misc`(`player_id`, `eye_of_the_rings`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `eye_of_the_rings` = VALUES(`eye_of_the_rings`)";
	
	private void saveMisc() {

		if (!Config.RUN_ON_DEDI)
			return;

		PlayerSaveSql.SAVE_EXECUTOR.execute(new Runnable() {

			@Override
			public void run() {
				
				try (Connection connection = Server.getConnection();
						PreparedStatement ps = connection.prepareStatement(MISC_SAVE_QUERY);) {
					ps.setInt(1, player.mySQLIndex);
					ps.setString(2, PlayerSaveSql.createJsonString(player.getRecoilRings().getCharges()));
					ps.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}

		});
		
	}
	
}
