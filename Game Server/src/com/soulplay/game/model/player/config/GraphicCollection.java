package com.soulplay.game.model.player.config;

import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public class GraphicCollection {

	public static final Graphic EXPLOSION = Graphic.create(Graphic.getOsrsId(157), GraphicType.HIGH);
	public static final Graphic REDEMPTION_GFX = Graphic.create(436);
	public static final Graphic HEAL_GFX = Graphic.create(7);
	public static final Graphic MAGIC_SPLASH = Graphic.create(85, GraphicType.HIGH);
	
	public static final Graphic STUN_GFX = Graphic.create(80, GraphicType.HIGH);
	public static final Graphic LVL_UP_FIREWORKS = Graphic.create(1636, GraphicType.HIGH);

}
