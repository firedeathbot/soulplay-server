package com.soulplay.game.model.player.config;

import com.soulplay.game.model.player.Player;

public enum Variables { // WARNING: DO NOT ADD INSIDE OF ANY OF THESE VARIABLES,
						// ONLY ADD ON THE VERY BOTTOM!
	STARTER_PACK(0),
	EFFIGY(1),
	DUEL_KC(2),
	DUEL_DC(3),
	DUEL_STREAK(4),
	ZAROSKC(5),
	POISON_DAMAGE(6),
	POISON_INDEX(7),
	ANTI_FIRE(8),
	LOW_CLUE(9),
	MED_CLUE(10),
	HIGH_CLUE(11),
	STRONGHOLD_LEVEL(12),
	RFD_PROGRESS(13),
	EGG_IN_INCUBATOR(14),
	EGG_INCUBATOR_TIME(15),
	UNTRADEABLE_GOLD(16),
	TOGGLE_CMB_EXP(17),
	RUN_ENERGY(18),
	SUMMONING_ORB_OPTION(19),
	JAIL_TIME(20),
	FIGHT_PITS_POINTS(21),
	SET_CROWN(22),
	SLAYER_SCROLLS(23),
	TOXIC_BLOWPIPE_CHARGES(24),
	TOXIC_BLOWPIPE_AMMO_TYPE(25),
	TOXIC_BLOWPIPE_AMMO(26),
	MINIGAME_PUNISHMENTS(27),
	BOUGHT_HOUSE(28),
	BRIGHTNESS(29),
	MUSIC_LEVEL(30),
	NAME_CHANGE_POINTS(31),
	NAME_CHANGE_DATE(32),
	LMS_COFFER(33),
	TAI_BWO_FAVOUR(34),
	TAI_BWO_CLEANUP(35),
	SEMI_ADMIN_RANK(36),
	LAST_MAN_STANDING_GAMES_PLAYED(37),
	YELL_MUTE(38),
	QUEST_POINTS(39),
	ANNOUNCEMENT_MUTE(40),
	SOUND_EFFECT_VOLUME(41, 2),
	EARNING_POTENTIAL(42),
	AUTOCAST(43),
	BREAK_VIALS_TOGGLE(44),
	STAFF_OF_LAW_CHARGES(45),
	TOURNAMENT_INSTANCE_ID(46),
	PLACEHOLDER(47),
	BOUNTY_TARGET_SPELL_UNLOCK(48),
	TIMES_VOTED(49),
	CHRISTMAS_POINTS(50),
	STAFF_OF_NATURE_CHARGES(51),
	SPIRIT_CAPE_ADDED_TO_COMP(52),
	RAID_INSTANCE_ID(53),
	PRAYER_SCROLL(54),
	INFERNO_TRIES(55),
	VOTE_POINTS(56),
	VOTE_LUCK_ITEM_DATA(57),
	VOTE_STREAK(58),
	TOB_INSTANCE_ID(59),
	INSTANCE_CHARGES(60),
	INSTANCE_INSURANCE(61),
	INSTANCE_HEALTH_REGEN(62),
	INSTANCE_PRAYER(63),
	BORK_KILL_DATE(64),
	TOME_EXTRA_XP_MOD(65),
	SEASONAL_INTERFACE_POINTS(66),
	SEASONAL_POD_UNLOCKED(67),
	;

	private final int code;
	private final int defaultValue;

	Variables(int code, int defaultValue) {
		this.code = code;
		this.defaultValue = defaultValue;
	}

	Variables(int code) {
		this.code = code;
		this.defaultValue = 0;
	}

	public static final Variables[] values = values();

	public int getValue(Player player) {
		if (code < 0 || code >= player.getVariables().length) {
			return 0;
		}

		return player.getVariables()[code];
	}

	public boolean toggled(Player player) {
		if (code < 0 || code >= player.getVariables().length) {
			return false;
		}

		return player.getVariables()[code] == 1;
	}

	public void setValue(Player p, int value) {
		p.getVariables()[code] = value;
	}

	public void addValue(Player p, int value) {
		p.getVariables()[code] += value;
	}

	public void toggle(Player p) {
		p.getVariables()[code] = 1 - p.getVariables()[code];
	}

	public int getCode() {
		return code;
	}

	public int getDefaultValue() {
		return defaultValue;
	}
}
