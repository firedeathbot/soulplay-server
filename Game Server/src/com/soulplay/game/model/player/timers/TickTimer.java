package com.soulplay.game.model.player.timers;

import com.soulplay.Server;

public class TickTimer {
	
	private int ticks;
	
	public void startTimer(int ticks) {
		this.ticks = Server.getTotalTicks() + ticks;
	}
	
	public boolean complete() {
		return Server.getTotalTicks() >= ticks;
	}
	
	public boolean checkThenStart(int ticks) {
		if (!complete()) {
			return false;
		}

		startTimer(ticks);
		return true;
	}

	public int getStartTicks() {
		return ticks;
	}

	public void reset() {
		this.ticks = Server.getTotalTicks();
	}
	
	public int getRemainingTicks() {
		return Math.max(0, ticks - Server.getTotalTicks());
	}

}
