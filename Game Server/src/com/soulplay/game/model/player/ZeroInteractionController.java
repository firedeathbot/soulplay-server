package com.soulplay.game.model.player;

public final class ZeroInteractionController extends PlayerController {

    public ZeroInteractionController(Client player) {
	super(player);
    }

    @Override
    public boolean canClickObject() {	
	return false;
    }

    @Override
    public boolean canClickNpc() {
	return false;
    }

    @Override
    public boolean canWalk() {
	return false;
    }

    @Override
    public boolean canUseCommands() {
	return false;
    }

	@Override
	public boolean canPickupItem() {
		return false;
	}

}
