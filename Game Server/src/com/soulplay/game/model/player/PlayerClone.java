package com.soulplay.game.model.player;

import com.soulplay.Server;
import com.soulplay.game.world.entity.Location;

public class PlayerClone {

	private final Location location;
	private final int ticks;
	private final int serverTicks;

	public PlayerClone(Location location, int ticks) {
		this.location = location;
		this.ticks = ticks;
		this.serverTicks = Server.getTotalTicks() + ticks;
	}

	public Location getLocation() {
		return location;
	}

	public int getTicks() {
		return ticks;
	}

	public int getServerTicks() {
		return serverTicks;
	}

}
