package com.soulplay.game.model.player;

public abstract class PlayerController {
    
    protected Player player;
    
    public PlayerController(Player player) {
	this.player = player;
    }

    public abstract boolean canPickupItem();

    public abstract boolean canClickObject();
    
    public abstract boolean canClickNpc();
    
    public abstract boolean canWalk();
    
    public abstract boolean canUseCommands();

}
