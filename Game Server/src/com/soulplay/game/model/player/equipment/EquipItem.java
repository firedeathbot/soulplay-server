package com.soulplay.game.model.player.equipment;

public class EquipItem {

	private int equipSlot;

	private int equipId;

	public EquipItem(int item, int slot) {
		setEquipId(item);
		setEquipSlot(slot);
	}

	public int getEquipId() {
		return equipId;
	}

	public int getEquipSlot() {
		return equipSlot;
	}

	public void setEquipId(int equipId) {
		this.equipId = equipId;
	}

	public void setEquipSlot(int equipSlot) {
		this.equipSlot = equipSlot;
	}

}
