package com.soulplay.game.model.player;

import java.util.LinkedList;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.ConstructionPathFinder;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;

public class PathFinder {

	// private static final PathFinder pathFinder = new PathFinder();
	//
	// public static PathFinder getPathFinder() {
	// return pathFinder;
	// }
	//
	// public PathFinder() {
	// }]
	
	public static boolean canMoveInStaticRegion(int x, int y, int height, int moveTypeX, int moveTypeY) {
		return canMove(x, y, height, moveTypeX, moveTypeY, null);
	}

	public static boolean canMove(int x, int y, int height, int moveTypeX,
			int moveTypeY, DynamicRegionClip clipMap) {
		/* if(height > 3) */height = height & 3;
		// Region r = region;//West
		// East
		if (moveTypeX == 1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280108) == 0
					&& (RegionClip.getClipping(x + 1, y, height, clipMap)
							& 0x1280180) == 0;
		} else
		// West
		if (moveTypeX == -1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280180) == 0
					&& (RegionClip.getClipping(x - 1, y, height, clipMap)
							& 0x1280108) == 0;
		} else
		// North
		if (moveTypeX == 0 && moveTypeY == 1) {
			// -1)
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280102) == 0
					&& (RegionClip.getClipping(x, y + 1, height, clipMap)
							& 0x1280120) == 0;
		} else
		// South
		if (moveTypeX == 0 && moveTypeY == -1) {
			// 1)
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280120) == 0
					&& (RegionClip.getClipping(x, y - 1, height, clipMap)
							& 0x1280102) == 0;
		} else
		// NorthEast
		if (moveTypeX == 1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280102) == 0// Check
																			// if
																			// can
																			// go
																			// north.
					&& (RegionClip.getClipping(x, y + 1, height, clipMap)
							& 0x1280120) == 0

					&& (RegionClip.getClipping(x, y, height, clipMap) & 0x1280108) == 0// Check
																				// if
																				// can
																				// go
																				// East
					&& (RegionClip.getClipping(x + 1, y, height, clipMap)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y + 1, height, clipMap)
							& 0x1280108) == 0// Check if can go East from North
					&& (RegionClip.getClipping(x + 1, y + 1, height, clipMap)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height, clipMap)
							& 0x1280102) == 0// Check if can go North from East
					&& (RegionClip.getClipping(x + 1, y + 1, height, clipMap)
							& 0x1280120) == 0;
		} else
		// NorthWest
		if (moveTypeX == -1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280102) == 0// Going
																			// North
					&& (RegionClip.getClipping(x, y + 1, height, clipMap)
							& 0x1280120) == 0

					&& (RegionClip.getClipping(x, y, height, clipMap) & 0x1280180) == 0// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height, clipMap)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y + 1, height, clipMap)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y + 1, height, clipMap)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height, clipMap)
							& 0x1280102) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y + 1, height, clipMap)
							& 0x1280120) == 0;
		} else
		// SouthEast
		if (moveTypeX == 1 && moveTypeY == -1) {
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280120) == 0// Going
																			// South
					&& (RegionClip.getClipping(x, y - 1, height, clipMap)
							& 0x1280102) == 0

					&& (RegionClip.getClipping(x, y, height, clipMap) & 0x1280108) == 0// Going
																				// East
					&& (RegionClip.getClipping(x + 1, y, height, clipMap)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y - 1, height, clipMap)
							& 0x1280108) == 0// Going East from South
					&& (RegionClip.getClipping(x + 1, y - 1, height, clipMap)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height, clipMap)
							& 0x1280120) == 0// Going East
					&& (RegionClip.getClipping(x + 1, y - 1, height, clipMap)
							& 0x1280102) == 0;
		} else
		// SouthWest
		if (moveTypeX == -1 && moveTypeY == -1) {
			return (RegionClip.getClipping(x, y, height, clipMap) & 0x1280120) == 0// Going
																			// North
					&& (RegionClip.getClipping(x, y - 1, height, clipMap)
							& 0x1280102) == 0

					&& (RegionClip.getClipping(x, y, height, clipMap) & 0x1280180) == 0// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height, clipMap)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y - 1, height, clipMap)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y - 1, height, clipMap)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height, clipMap)
							& 0x1280120) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y - 1, height, clipMap)
							& 0x1280102) == 0;
		}
		return false;
	}

	public static boolean canMoveRegionLocation(int x, int y, int height,
			int moveTypeX, int moveTypeY) {
		/* if(height > 3) */height = height & 3;
		// Region r = region;//West
		// East
		if (moveTypeX == 1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x, y, height) & 0x1280108) == 0
					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280180) == 0;
		} else
		// West
		if (moveTypeX == -1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x, y, height) & 0x1280180) == 0
					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280108) == 0;
		} else
		// North
		if (moveTypeX == 0 && moveTypeY == 1) {
			// -1)
			return (RegionClip.getClipping(x, y, height) & 0x1280102) == 0
					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280120) == 0;
		} else
		// South
		if (moveTypeX == 0 && moveTypeY == -1) {
			// 1)
			return (RegionClip.getClipping(x, y, height) & 0x1280120) == 0
					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280102) == 0;
		} else
		// NorthEast
		if (moveTypeX == 1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280102) == 0// Check
																			// if
																			// can
																			// go
																			// north.
					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280120) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280108) == 0// Check
																				// if
																				// can
																				// go
																				// East
					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280108) == 0// Check if can go East from North
					&& (RegionClip.getClipping(x + 1, y + 1, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280102) == 0// Check if can go North from East
					&& (RegionClip.getClipping(x + 1, y + 1, height)
							& 0x1280120) == 0;
		} else
		// NorthWest
		if (moveTypeX == -1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280102) == 0// Going
																			// North
					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280120) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280180) == 0// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y + 1, height)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y + 1, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280102) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y + 1, height)
							& 0x1280120) == 0;
		} else
		// SouthEast
		if (moveTypeX == 1 && moveTypeY == -1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280120) == 0// Going
																			// South
					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280102) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280108) == 0// Going
																				// East
					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280108) == 0// Going East from South
					&& (RegionClip.getClipping(x + 1, y - 1, height)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height)
							& 0x1280120) == 0// Going East
					&& (RegionClip.getClipping(x + 1, y - 1, height)
							& 0x1280102) == 0;
		} else
		// SouthWest
		if (moveTypeX == -1 && moveTypeY == -1) {
			return (RegionClip.getClipping(x, y, height) & 0x1280120) == 0// Going
																			// North
					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280102) == 0

					&& (RegionClip.getClipping(x, y, height) & 0x1280180) == 0// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y - 1, height)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y - 1, height)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height)
							& 0x1280120) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y - 1, height)
							& 0x1280102) == 0;
		}
		return false;
	}

	public static boolean canStepOffObject(int x, int y, int height,
			int moveTypeX, int moveTypeY, DynamicRegionClip dynClip) {
		/* if(height > 3) */height = height & 3;
		// Region r = region;//West
		// East
		if (moveTypeX == 1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x + 1, y, height, dynClip) & 0x1280180) == 0;
		} else
		// West
		if (moveTypeX == -1 && moveTypeY == 0) {
			// 0)
			return (RegionClip.getClipping(x - 1, y, height, dynClip) & 0x1280108) == 0;
		} else
		// North
		if (moveTypeX == 0 && moveTypeY == 1) {
			// -1)
			return (RegionClip.getClipping(x, y + 1, height, dynClip) & 0x1280120) == 0;
		} else
		// South
		if (moveTypeX == 0 && moveTypeY == -1) {
			// 1)
			return (RegionClip.getClipping(x, y - 1, height, dynClip) & 0x1280102) == 0;
		} else
		// NorthEast
		if (moveTypeX == 1 && moveTypeY == 1) {
			return (RegionClip.getClipping(x, y, height, dynClip) & 0x1280102) == 0// Check if can go north.
					&& (RegionClip.getClipping(x, y + 1, height, dynClip)
							& 0x1280120) == 0
					&& (RegionClip.getClipping(x + 1, y, height, dynClip)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y + 1, height, dynClip)
							& 0x1280108) == 0// Check if can go East from North
					&& (RegionClip.getClipping(x + 1, y + 1, height, dynClip)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height, dynClip)
							& 0x1280102) == 0// Check if can go North from East
					&& (RegionClip.getClipping(x + 1, y + 1, height, dynClip)
							& 0x1280120) == 0;
		} else
		// NorthWest
		if (moveTypeX == -1 && moveTypeY == 1) {
			return ( // Going
																			// North
					RegionClip.getClipping(x, y + 1, height, dynClip)
							& 0x1280120) == 0

					// Going
																				// West
					&& (RegionClip.getClipping(x - 1, y, height, dynClip)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y + 1, height, dynClip)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y + 1, height, dynClip)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height, dynClip)
							& 0x1280102) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y + 1, height, dynClip)
							& 0x1280120) == 0;
		} else
		// SouthEast
		if (moveTypeX == 1 && moveTypeY == -1) {
			return (// Going South
					RegionClip.getClipping(x, y - 1, height, dynClip)
							& 0x1280102) == 0

					// Going East
					&& (RegionClip.getClipping(x + 1, y, height, dynClip)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x, y - 1, height, dynClip)
							& 0x1280108) == 0// Going East from South
					&& (RegionClip.getClipping(x + 1, y - 1, height, dynClip)
							& 0x1280180) == 0

					&& (RegionClip.getClipping(x + 1, y, height, dynClip)
							& 0x1280120) == 0// Going East
					&& (RegionClip.getClipping(x + 1, y - 1, height, dynClip)
							& 0x1280102) == 0;
		} else
		// SouthWest
		if (moveTypeX == -1 && moveTypeY == -1) {
			return (// Going North
					RegionClip.getClipping(x, y - 1, height, dynClip)
							& 0x1280102) == 0

					// Going West
					&& (RegionClip.getClipping(x - 1, y, height, dynClip)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x, y - 1, height, dynClip)
							& 0x1280180) == 0// Going West from North
					&& (RegionClip.getClipping(x - 1, y - 1, height, dynClip)
							& 0x1280108) == 0

					&& (RegionClip.getClipping(x - 1, y, height, dynClip)
							& 0x1280120) == 0// Going West
					&& (RegionClip.getClipping(x - 1, y - 1, height, dynClip)
							& 0x1280102) == 0;
		}
		return false;
	}

	public static void findRangeableRoute(Client c, int destX, int destY,
			boolean moveNear, int xLength, int yLength) {
		// c.sendMessage("Walking to npc");
		if (destX == c.getX() && destY == c.getY() && !moveNear) {
			c.sendMessage("ERROR!");
			return;
		}
		// c.sendMessage("Poop");
		int destAbsX = destX;
		int destAbsY = destY;
		
		destX = destX - 8 * c.getMapRegionX();
		destY = destY - 8 * c.getMapRegionY();

		int[][] via = new int[104][104];
		int[][] cost = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		for (int xx = 0; xx < 104; xx++) {
			for (int yy = 0; yy < 104; yy++) {
				cost[xx][yy] = 99999999;
			}
		}
		int curX = c.getLocalX();
		int curY = c.getLocalY();
		via[curX][curY] = 99;
		cost[curX][curY] = 0;
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = c.getMapRegionX() * 8 + curX;
			int curAbsY = c.getMapRegionY() * 8 + curY;
			if (curX == destX && curY == destY) {
				foundPath = true;
				break;
			}

			if (c.withinRangeToHit) {
				int newDistance = Misc.distanceToPoint(curAbsX, curAbsY, destAbsX, destAbsY);
				if (newDistance <= c.rangeRequiredToHit && !RegionClip.rayTraceBlocked(curAbsX, curAbsY, destAbsX,
						destAbsY, c.getHeightLevel(), true, c.getDynamicRegionClip())) {
					// c.sendMessage("DestX "+(destAbsX));
					// c.sendMessage("DestY "+(destAbsY));

//					 c.sendMessage("curdist:"+ currentDistance +" newDist:"+ newDistance + " offset:"+ offset);
					 //c.sendMessage("path "+(tail));
					 
					foundPath = true;
					break;
				}
			}
			tail = (tail + 1) % pathLength;
			int thisCost = cost[curX][curY] + 1;
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							c.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
				cost[curX][curY - 1] = thisCost;
			}
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							c.heightLevel) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
				cost[curX - 1][curY] = thisCost;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							c.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
				cost[curX][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							c.heightLevel) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
				cost[curX + 1][curY] = thisCost;
			}

			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							c.heightLevel) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							c.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							c.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
				cost[curX - 1][curY - 1] = thisCost;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							c.heightLevel) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							c.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							c.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
				cost[curX - 1][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							c.heightLevel) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							c.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							c.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
				cost[curX + 1][curY - 1] = thisCost;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							c.heightLevel) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							c.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							c.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
				cost[curX + 1][curY + 1] = thisCost;
			}
			
			c.walkToX = c.walkToY = -1;
		}
		if (!foundPath) {
			if (moveNear) {
				int i_223_ = 1000;
				int thisCost = 100;
				int i_225_ = 10;
				for (int x = destX - i_225_; x <= destX + i_225_; x++) {
					for (int y = destY - i_225_; y <= destY + i_225_; y++) {
						if (x >= 0 && y >= 0 && x < 104 && y < 104
								&& cost[x][y] < 100) {
							int i_228_ = 0;
							if (x < destX) {
								i_228_ = destX - x;
							} else if (x > destX + xLength - 1) {
								i_228_ = x - (destX + xLength - 1);
							}
							int i_229_ = 0;
							if (y < destY) {
								i_229_ = destY - y;
							} else if (y > destY + yLength - 1) {
								i_229_ = y - (destY + yLength - 1);
							}
							int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;
							if (i_230_ < i_223_ || (i_230_ == i_223_
									&& (cost[x][y] < thisCost))) {
								i_223_ = i_230_;
								thisCost = cost[x][y];
								curX = x;
								curY = y;
							}
						}
					}
				}
				c.walkToX = c.walkToY = -1;
				if (i_223_ == 1000) {
					return;
				}
			} else {
				return;
			}
		}
		tail = 0;
		tileQueueX.set(tail, curX);
		tileQueueY.set(tail++, curY);
		int l5;
		for (int j5 = l5 = via[curX][curY]; curX != c.getLocalX()
				|| curY != c.getLocalY(); j5 = via[curX][curY]) {
			if (j5 != l5) {
				l5 = j5;
				tileQueueX.set(tail, curX);
				tileQueueY.set(tail++, curY);
			}
			if ((j5 & 2) != 0) {
				curX++;
			} else if ((j5 & 8) != 0) {
				curX--;
			}
			if ((j5 & 1) != 0) {
				curY++;
			} else if ((j5 & 4) != 0) {
				curY--;
			}
		}
		int size = tail--;
		int pathX = c.getMapRegionX() * 8 + tileQueueX.get(tail);
		int pathY = c.getMapRegionY() * 8 + tileQueueY.get(tail);

		if (c.getX() == pathX && c.getY() == pathY) {
			tileQueueX = null;
			tileQueueY = null;
			return;
		}

		c.getMovement().hardResetWalkingQueue(false);
		c.getMovement().addToWalkingQueue(localize(pathX, c.getMapRegionX()),
				localize(pathY, c.getMapRegionY()));
		for (int i = 1; i < size; i++) {
			tail--;
			pathX = c.getMapRegionX() * 8 + tileQueueX.get(tail);
			pathY = c.getMapRegionY() * 8 + tileQueueY.get(tail);
			c.getMovement().addToWalkingQueue(localize(pathX, c.getMapRegionX()),
					localize(pathY, c.getMapRegionY()));
		}
		tileQueueX = null;
		tileQueueY = null;
	}
	
	public static void findRoute(Client c, int destX, int destY, boolean moveNear, int xLength, int yLength) {
		findRoute(c, destX, destY, moveNear, xLength, yLength, 0, 0, 0, 0, 0, 1);
	}

	public static boolean findRoute(Client c, int destX, int destY, boolean moveNear, int xLength, int yLength, int offsetSizeX, int offsetSizeY, int directionMask, int objectType, int objectRotation, int reachDistance) {
		//long test = System.nanoTime();
		if (c.getDynamicRegionClip() != null/* && c.playerIsInHouse*/) {
			return ConstructionPathFinder.findRoute(c, destX, destY, moveNear, xLength, yLength, c.getDynamicRegionClip(), offsetSizeX, offsetSizeY, directionMask, objectType, objectRotation, reachDistance);
		}
		
		if (c.getX() == destX && c.getY() == destY) {
			if (!moveNear) {
				c.finalLocalDestX = c.getX();
				c.finalLocalDestY = c.getY();
			}
			return true;
		}

		int regionStartX = c.getMapRegionX() << 3;
		int regionStartY = c.getMapRegionY() << 3;
		int localX = c.getX() - regionStartX;
		int localY = c.getY() - regionStartY;
		
		final int z = c.getZ();

		if (c.toggleBot && c.getBot() != null) {
			findRoute(c.getBot(), destX, destY, moveNear, xLength, yLength, offsetSizeX, offsetSizeY, directionMask, objectType, objectRotation, reachDistance);
		}

		destX = destX - regionStartX;
		destY = destY - regionStartY;

		byte[][] via = new byte[104][104];
		int[][] cost = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		for (int xx = 0; xx < 104; xx++) {
			for (int yy = 0; yy < 104; yy++) {
				cost[xx][yy] = 99999999;
			}
		}
		int curX = localX;
		int curY = localY;
		try {
			via[curX][curY] = 99;
			cost[curX][curY] = 0;
		} catch (Exception e) {
			System.out.println("Player way out of range! Name:" + c.getName()
					+ " X:" + c.getX() + " Y:" + c.getY() + " Z:" + c.getZ());
			e.printStackTrace();
		}
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = regionStartX + curX;
			int curAbsY = regionStartY + curY;
			if (curX == destX && curY == destY) {
				foundPath = true;
				if (!moveNear) {
					c.finalLocalDestX = curAbsX;
					c.finalLocalDestY = curAbsY;
				}
				break;
			}
			if (objectType != 0) {
				if ((objectType < 5 || objectType == 10) && method219(destX, destY, curX, curY, objectRotation, objectType-1, RegionClip.getClipping(curAbsX, curAbsY, z, c.getDynamicRegionClip()))) {
					foundPath = true;
					if (!moveNear) {
						c.finalLocalDestX = curAbsX;
						c.finalLocalDestY = curAbsY;
					}
					break;
				}
				if (objectType < 10 && method220(destX, destY, curX, curY, objectType-1, objectRotation, RegionClip.getClipping(curAbsX, curAbsY, z, c.getDynamicRegionClip()))) {
					foundPath = true;
					if (!moveNear) {
						c.finalLocalDestX = curAbsX;
						c.finalLocalDestY = curAbsY;
					}
					break;
				}
			}
			if (offsetSizeX != 0 && offsetSizeY != 0 && reachedEntity(reachDistance, destX, destY, curX, curY, offsetSizeX, offsetSizeY, directionMask, c.disableInteractClip ? 0 : RegionClip.getClipping(curAbsX, curAbsY, z, c.getDynamicRegionClip()))) {
				foundPath = true;
				if (!moveNear) {
					c.finalLocalDestX = curAbsX;
					c.finalLocalDestY = curAbsY;
				}
				break;
			}
			tail = (tail + 1) % pathLength;
			int thisCost = cost[curX][curY] + 1;
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							z) & 0x1280108) == 0) {
				int offX = curX - 1;
				tileQueueX.add(offX);
				tileQueueY.add(curY);
				via[offX][curY] = 2;
				cost[offX][curY] = thisCost;
			}
			if (curX < 103 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							z) & 0x1280180) == 0) {
				int offX = curX + 1;
				tileQueueX.add(offX);
				tileQueueY.add(curY);
				via[offX][curY] = 8;
				cost[offX][curY] = thisCost;
			}
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							z) & 0x1280102) == 0) {
				int offY = curY - 1;
				tileQueueX.add(curX);
				tileQueueY.add(offY);
				via[curX][offY] = 1;
				cost[curX][offY] = thisCost;
			}
			if (curY < 103 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							z) & 0x1280120) == 0) {
				int offY = curY + 1;
				tileQueueX.add(curX);
				tileQueueY.add(offY);
				via[curX][offY] = 4;
				cost[curX][offY] = thisCost;
			}
			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							z) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							z) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							z) & 0x1280102) == 0) {
				int offX = curX - 1;
				int offY = curY - 1;
				tileQueueX.add(offX);
				tileQueueY.add(offY);
				via[offX][offY] = 3;
				cost[offX][offY] = thisCost;
			}
			if (curX < 103 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							z) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							z) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							z) & 0x1280102) == 0) {
				int offX = curX + 1;
				int offY = curY - 1;
				tileQueueX.add(offX);
				tileQueueY.add(offY);
				via[offX][offY] = 9;
				cost[offX][offY] = thisCost;
			}
			if (curX > 0 && curY < 103 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							z) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							z) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							z) & 0x1280120) == 0) {
				int offX = curX - 1;
				int offY = curY + 1;
				tileQueueX.add(offX);
				tileQueueY.add(offY);
				via[offX][offY] = 6;
				cost[offX][offY] = thisCost;
			}
			if (curX < 103 && curY < 103 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							z) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							z) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							z) & 0x1280120) == 0) {
				int offX = curX + 1;
				int offY = curY + 1;
				tileQueueX.add(offX);
				tileQueueY.add(offY);
				via[offX][offY] = 12;
				cost[offX][offY] = thisCost;
			}
		}
		c.walkToX = c.walkToY = -1;
		if (!foundPath) {
			if (!moveNear) {
				return false;
			}

			int i_223_ = 1000;
			int thisCost = 100;
			int i_225_ = 10;
			int boundsX = destX + xLength - 1;
			int boundsY = destY + yLength - 1;
			for (int x = destX - i_225_, endX = destX + i_225_; x <= endX; x++) {
				for (int y = destY - i_225_, endY = destY + i_225_; y <= endY; y++) {
					if (x >= 0 && y >= 0 && x < 104 && y < 104
							&& cost[x][y] < 100) {
						int i_228_ = 0;
						if (x < destX) {
							i_228_ = destX - x;
						} else if (x > boundsX) {
							i_228_ = x - boundsX;
						}
						int i_229_ = 0;
						if (y < destY) {
							i_229_ = destY - y;
						} else if (y > boundsY) {
							i_229_ = y - boundsY;
						}
						int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;
						if (i_230_ < i_223_ || (i_230_ == i_223_
								&& (cost[x][y] < thisCost))) {
							i_223_ = i_230_;
							thisCost = cost[x][y];
							curX = x;
							curY = y;
						}
					}
				}
			}
			c.walkToX = c.walkToY = -1;
			if (i_223_ == 1000) {
				return false;
			}
		}

		tail = 0;
		tileQueueX.set(tail, curX);
		tileQueueY.set(tail++, curY);
		int l5;
		for (int j5 = l5 = via[curX][curY]; curX != localX
				|| curY != localY; j5 = via[curX][curY]) {
			if (j5 != l5) {
				l5 = j5;
				tileQueueX.set(tail, curX);
				tileQueueY.set(tail++, curY);
			}
			if ((j5 & 2) != 0) {
				curX++;
			} else if ((j5 & 8) != 0) {
				curX--;
			}
			if ((j5 & 1) != 0) {
				curY++;
			} else if ((j5 & 4) != 0) {
				curY--;
			}
		}

		int size = tail--;
		int pathX = tileQueueX.get(tail);
		int pathY = tileQueueY.get(tail);
		c.getMovement().hardResetWalkingQueue(false);
		c.getMovement().addToWalkingQueue(pathX, pathY);
		for (int i = 1; i < size; i++) {
			tail--;
			pathX = tileQueueX.get(tail);
			pathY = tileQueueY.get(tail);
			c.getMovement().addToWalkingQueue(pathX, pathY);
		}
		c.getPacketSender().sendMiniMapFlag(pathX, pathY);
		tileQueueX = null;
		tileQueueY = null;
		//System.out.println("took " + (System.nanoTime() - test) + "ms to find path");
		return true;
	}

	public static boolean foundRoute(Client c, int destX, int destY) {
		if (destX == c.getX() && destY == c.getY()) {
			return false;
		}
		destX = destX - 8 * c.getMapRegionX();
		destY = destY - 8 * c.getMapRegionY();
		int[][] via = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		int curX = c.getLocalX();
		int curY = c.getLocalY();
		via[curX][curY] = 99;
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		// boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = c.getMapRegionX() * 8 + curX;
			int curAbsY = c.getMapRegionY() * 8 + curY;
			if (curX == destX && curY == destY) {
				return true;
			}
			tail = (tail + 1) % pathLength;
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							c.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
			}
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							c.heightLevel) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							c.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							c.heightLevel) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
			}
			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							c.heightLevel) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							c.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							c.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							c.heightLevel) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							c.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							c.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							c.heightLevel) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							c.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							c.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							c.heightLevel) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							c.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							c.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
			}
		}
		tileQueueX = null;
		tileQueueY = null;
		return false;
	}
	
	public static void findRoute(NPC npc, int destX, int destY, boolean moveNear, int xLength, int yLength) {
		findRoute(npc, destX, destY, moveNear, xLength, yLength, 1, 1, 0, 0);
	}
	
	//TODO: make this entity based
	public static void findRoute(NPC npc, int destX, int destY,
			boolean moveNear, int xLength, int yLength, int offsetSizeX, int offsetSizeY, int directionMask, int reachDistance) {
		// c.sendMessage("Walking to npc");
//		if (npc.playerIsInHouse) {
//			ConstructionPathFinder.findRoute(npc, destX, destY, moveNear, xLength, yLength);
//			return;
//		}
		if (destX == npc.getX() && destY == npc.getY() && !moveNear) {
			return;
		}

//		if (npc.toggleBot) {
//			if (npc.getBot() != null) {
//				findRoute(npc.getBot(), destX, destY, moveNear, xLength, yLength);
//			}
//		}

		destX = destX - 8 * npc.getMapRegionX();
		destY = destY - 8 * npc.getMapRegionY();
		int[][] via = new int[104][104];
		int[][] cost = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		for (int xx = 0; xx < 104; xx++) {
			for (int yy = 0; yy < 104; yy++) {
				cost[xx][yy] = 99999999;
			}
		}
		int curX = npc.getLocalX();
		int curY = npc.getLocalY();
		try {
			via[curX][curY] = 99;
			cost[curX][curY] = 0;
		} catch (Exception e) {
			System.out.println("NPC way out of range! Name:" + npc.getNpcName()
					+ " X:" + npc.getX() + " Y:" + npc.getY() + " Z:" + npc.getZ());
			e.printStackTrace();
		}
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = npc.getMapRegionX() * 8 + curX;
			int curAbsY = npc.getMapRegionY() * 8 + curY;
			if (curX == destX && curY == destY) {
				foundPath = true;
				break;
			}

			if (offsetSizeX != 0 && offsetSizeY != 0 && reachedEntity(reachDistance, destX, destY, curX, curY, offsetSizeX, offsetSizeY, directionMask, RegionClip.getClipping(curAbsX, curAbsY, npc.getZ()))) {
				foundPath = true;
				if (!moveNear) {
//					c.finalLocalDestX = curAbsX;
//					c.finalLocalDestY = curAbsY;
				}
				break;
			}
			tail = (tail + 1) % pathLength;
			int thisCost = cost[curX][curY] + 1;
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							npc.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
				cost[curX][curY - 1] = thisCost;
			}
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							npc.heightLevel) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
				cost[curX - 1][curY] = thisCost;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							npc.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
				cost[curX][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							npc.heightLevel) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
				cost[curX + 1][curY] = thisCost;
			}
			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							npc.heightLevel) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							npc.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							npc.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
				cost[curX - 1][curY - 1] = thisCost;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							npc.heightLevel) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							npc.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							npc.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
				cost[curX - 1][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							npc.heightLevel) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							npc.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							npc.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
				cost[curX + 1][curY - 1] = thisCost;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							npc.heightLevel) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							npc.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							npc.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
				cost[curX + 1][curY + 1] = thisCost;
			}
		}
//		npc.walkToX = npc.walkToY = -1;
		if (!foundPath) {
			if (moveNear) {
				int i_223_ = 1000;
				int thisCost = 100;
				int i_225_ = 10;
				for (int x = destX - i_225_; x <= destX + i_225_; x++) {
					for (int y = destY - i_225_; y <= destY + i_225_; y++) {
						if (x >= 0 && y >= 0 && x < 104 && y < 104
								&& cost[x][y] < 100) {
							int i_228_ = 0;
							if (x < destX) {
								i_228_ = destX - x;
							} else if (x > destX + xLength - 1) {
								i_228_ = x - (destX + xLength - 1);
							}
							int i_229_ = 0;
							if (y < destY) {
								i_229_ = destY - y;
							} else if (y > destY + yLength - 1) {
								i_229_ = y - (destY + yLength - 1);
							}
							int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;
							if (i_230_ < i_223_ || (i_230_ == i_223_
									&& (cost[x][y] < thisCost))) {
								i_223_ = i_230_;
								thisCost = cost[x][y];
								curX = x;
								curY = y;
							}
						}
					}
				}
//				npc.walkToX = npc.walkToY = -1;
				if (i_223_ == 1000) {
					return;
				}
			} else {
				return;
			}
		}
		tail = 0;
		tileQueueX.set(tail, curX);
		tileQueueY.set(tail++, curY);
		int l5;
		for (int j5 = l5 = via[curX][curY]; curX != npc.getLocalX()
				|| curY != npc.getLocalY(); j5 = via[curX][curY]) {
			if (j5 != l5) {
				l5 = j5;
				tileQueueX.set(tail, curX);
				tileQueueY.set(tail++, curY);
			}
			if ((j5 & 2) != 0) {
				curX++;
			} else if ((j5 & 8) != 0) {
				curX--;
			}
			if ((j5 & 1) != 0) {
				curY++;
			} else if ((j5 & 4) != 0) {
				curY--;
			}
		}
		int size = tail--;
		int pathX = npc.getMapRegionX() * 8 + tileQueueX.get(tail);
		int pathY = npc.getMapRegionY() * 8 + tileQueueY.get(tail);
		if (npc.getX() == pathX && npc.getY() == pathY) {
			tileQueueX = null;
			tileQueueY = null;
			return;
		}
		npc.getWalkingQueue().reset();
		npc.getWalkingQueue().addPath(pathX, pathY);
//		npc.getMovement().hardResetWalkingQueue();
//		npc.getMovement().addToWalkingQueue(localize(pathX, npc.getMapRegionX()), localize(pathY, npc.getMapRegionY()));
		for (int i = 1; i < size; i++) {
			tail--;
			pathX = npc.getMapRegionX() * 8 + tileQueueX.get(tail);
			pathY = npc.getMapRegionY() * 8 + tileQueueY.get(tail);
			npc.getWalkingQueue().addPath(pathX, pathY);
//			npc.getMovement().addToWalkingQueue(localize(pathX, npc.getMapRegionX()), localize(pathY, npc.getMapRegionY()));
		}
		tileQueueX = null;
		tileQueueY = null;
	}
	
	public static boolean routeTraversable(int z, int startX, int startY, int destX, int destY, DynamicRegionClip clip) {
		if (destX == startX && destY == startY) {
			return true;
		}
		
		int mapRegionX = (startX >> 3) - 6;
		int mapRegionY = (startY >> 3) - 6;
		
		destX = destX - 8 * mapRegionX;
		destY = destY - 8 * mapRegionY;
		int[][] via = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		int curX = startX - 8 * mapRegionX;
		int curY = startY - 8 * mapRegionY;
		via[curX][curY] = 99;
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = mapRegionX * 8 + curX;
			int curAbsY = mapRegionY * 8 + curY;
			if (curX == destX && curY == destY) {
				return true;
			}
			tail = (tail + 1) % pathLength;
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							z, clip) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
			}
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							z, clip) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							z, clip) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							z, clip) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
			}
			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							z, clip) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							z, clip) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							z, clip) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							z, clip) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							z, clip) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							z, clip) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							z, clip) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							z, clip) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							z, clip) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							z, clip) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							z, clip) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							z, clip) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
			}
		}
		tileQueueX = null;
		tileQueueY = null;
		return false;
	}


	public static int localize(int x, int mapRegion) {
		return x - 8 * mapRegion;
	}
	
	public static boolean reachedEntity(int reachDistance, int targetX, int targetY, int pathX, int pathY, int offsetX, int offsetY, int walkingFlag, int getClipping) {
		int objectOffsetX = (targetX + offsetX) - 1;
		int objectOffsetY = (targetY + offsetY) - 1;
		if (pathX >= targetX && pathX <= objectOffsetX && pathY >= targetY && pathY <= objectOffsetY)
			return true;
		if (pathX == targetX - reachDistance && pathY >= targetY && pathY <= objectOffsetY
				&& (getClipping & 8) == 0 // east not blocked. Path standing on west side of object
				&& (walkingFlag & 8) == 0)
			return true;
		if (pathX == objectOffsetX + reachDistance
				&& pathY >= targetY
				&& pathY <= objectOffsetY
				&& (getClipping & 0x80) == 0 // west not blocked
				&& (walkingFlag & 2) == 0)
			return true;
		return pathY == targetY - reachDistance
				&& pathX >= targetX
				&& pathX <= objectOffsetX
				&& (getClipping & 2) == 0 //north not blocked
				&& (walkingFlag & 4) == 0
				|| pathY == objectOffsetY + reachDistance
				&& pathX >= targetX
				&& pathX <= objectOffsetX
				&& (getClipping & 0x20) == 0 // south not blocked
				&& (walkingFlag & 1) == 0;
	}
	
	public static boolean method219(int objectX, int objectY, int curCheckX, int curCheckY, int objectRotation, int objectType, int getClipping) {
		if (curCheckX == objectX && curCheckY == objectY)
			return true;
		if (objectType == 0)
			if (objectRotation == 0) {
				if (curCheckX == objectX - 1 && curCheckY == objectY)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1
						&& (getClipping & 0x1280120) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1
						&& (getClipping & 0x1280102) == 0)
					return true;
			} else if (objectRotation == 1) {
				if (curCheckX == objectX && curCheckY == objectY + 1)
					return true;
				if (curCheckX == objectX - 1 && curCheckY == objectY
						&& (getClipping & 0x1280108) == 0)
					return true;
				if (curCheckX == objectX + 1 && curCheckY == objectY
						&& (getClipping & 0x1280180) == 0)
					return true;
			} else if (objectRotation == 2) {
				if (curCheckX == objectX + 1 && curCheckY == objectY)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1
						&& (getClipping & 0x1280120) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1
						&& (getClipping & 0x1280102) == 0)
					return true;
			} else if (objectRotation == 3) {
				if (curCheckX == objectX && curCheckY == objectY - 1)
					return true;
				if (curCheckX == objectX - 1 && curCheckY == objectY
						&& (getClipping & 0x1280108) == 0)
					return true;
				if (curCheckX == objectX + 1 && curCheckY == objectY
						&& (getClipping & 0x1280180) == 0)
					return true;
			}
		if (objectType == 2)
			if (objectRotation == 0) {
				if (curCheckX == objectX - 1 && curCheckY == objectY)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1)
					return true;
				if (curCheckX == objectX + 1 && curCheckY == objectY
						&& (getClipping & 0x1280180) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1
						&& (getClipping & 0x1280102) == 0)
					return true;
			} else if (objectRotation == 1) {
				if (curCheckX == objectX - 1 && curCheckY == objectY
						&& (getClipping & 0x1280108) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1)
					return true;
				if (curCheckX == objectX + 1 && curCheckY == objectY)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1
						&& (getClipping & 0x1280102) == 0)
					return true;
			} else if (objectRotation == 2) {
				if (curCheckX == objectX - 1 && curCheckY == objectY
						&& (getClipping & 0x1280108) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1
						&& (getClipping & 0x1280120) == 0)
					return true;
				if (curCheckX == objectX + 1 && curCheckY == objectY)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1)
					return true;
			} else if (objectRotation == 3) {
				if (curCheckX == objectX - 1 && curCheckY == objectY)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1
						&& (getClipping & 0x1280120) == 0)
					return true;
				if (curCheckX == objectX + 1 && curCheckY == objectY
						&& (getClipping & 0x1280180) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1)
					return true;
			}
		if (objectType == 9) {
			if (curCheckX == objectX && curCheckY == objectY + 1 && (getClipping & 0x20) == 0)
				return true;
			if (curCheckX == objectX && curCheckY == objectY - 1 && (getClipping & 2) == 0)
				return true;
			if (curCheckX == objectX - 1 && curCheckY == objectY && (getClipping & 8) == 0)
				return true;
			if (curCheckX == objectX + 1 && curCheckY == objectY && (getClipping & 0x80) == 0)
				return true;
		}
		return false;
	}

	public static boolean method220(int objectX, int objectY, int curCheckX, int curCheckY, int objectType, int objectRotation, int getClipping) {
		if (curCheckX == objectX && curCheckY == objectY)
			return true;
		if (objectType == 6 || objectType == 7) {
			if (objectType == 7)
				objectRotation = objectRotation + 2 & 3;
			if (objectRotation == 0) {
				if (curCheckX == objectX + 1 && curCheckY == objectY
						&& (getClipping & 0x80) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1
						&& (getClipping & 2) == 0)
					return true;
			} else if (objectRotation == 1) {
				if (curCheckX == objectX - 1 && curCheckY == objectY
						&& (getClipping & 8) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY - 1
						&& (getClipping & 2) == 0)
					return true;
			} else if (objectRotation == 2) {
				if (curCheckX == objectX - 1 && curCheckY == objectY
						&& (getClipping & 8) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1
						&& (getClipping & 0x20) == 0)
					return true;
			} else if (objectRotation == 3) {
				if (curCheckX == objectX + 1 && curCheckY == objectY
						&& (getClipping & 0x80) == 0)
					return true;
				if (curCheckX == objectX && curCheckY == objectY + 1
						&& (getClipping & 0x20) == 0)
					return true;
			}
		}
		if (objectType == 8) {
			if (curCheckX == objectX && curCheckY == objectY + 1
					&& (getClipping & 0x20) == 0)
				return true;
			if (curCheckX == objectX && curCheckY == objectY - 1 && (getClipping & 2) == 0)
				return true;
			if (curCheckX == objectX - 1 && curCheckY == objectY && (getClipping & 8) == 0)
				return true;
			if (curCheckX == objectX + 1 && curCheckY == objectY
					&& (getClipping & 0x80) == 0)
				return true;
		}
		return false;
	}
	
	public static void getPathToObject(Client c) {
		GameObject o = null;
		c.setClickedObject(null);
		if (c.getHomeOwnerId() > 0 && c.playerIsInHouse) {
			o = Construction.getDecoration(c, c.objectId, c.objectX, c.objectY);
			if (o == null)
				o = Construction.getHotspot(c, c.objectId, c.objectX, c.objectY, c.getZ());
		} else if (c.insideDungeoneering()) {
			o = c.dungParty.dungManager.getRealObject(c.objectId, c.objectX, c.objectY, c.getZ());
		} else if (c.dynObjectManager != null) {
			o = c.dynObjectManager.getObject(c.objectId, c.objectX, c.objectY, c.getZ());
		} else
			o = RegionClip.getGameObject(c.objectId, c.objectX, c.objectY, c.getZ()); // static object
		
		if (o != null) {
			if (o.getType() == 10 || o.getType() == 11 || o.getType() == 22) {
				ObjectDef def = o.getDef();
				if (def != null) {
					int directionMask = o.getDirectionMask();
					if (directionMask != 0)
						directionMask = ((directionMask << o.getOrientation()) & 0xF) + (directionMask >> (4 - o.getOrientation()));
					int reachDistance = def.getReachDistance() != 0 ? def.getReachDistance() : 1;
					if (PathFinder.findRoute(c, c.objectX, c.objectY, def.isMoveNear(), 0, 0, o.getSizeX(), o.getSizeY(), directionMask, 0, 0, reachDistance))
						c.setClickedObject(o);
				}
			} else {
				if (PathFinder.findRoute(c, c.objectX, c.objectY, o.getDef().isMoveNear(), 0, 0, 0, 0, 0, o.getType()+1, o.getOrientation(), 1))
					c.setClickedObject(o);
			}
		}
		if (c.getClickedObject() == null)
			c.walkingToObject = false;
	}
	
	
	public static Location findBountySpot(Player other) {
		int destX = other.getX() - 10 + Misc.random(20);
		int destY = other.getY() - 10 + Misc.random(20);
		int destZ = other.getZ();
		
		destX = destX - 8 * other.getMapRegionX();
		destY = destY - 8 * other.getMapRegionY();

		int[][] via = new int[104][104];
		int[][] cost = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		for (int xx = 0; xx < 104; xx++) {
			for (int yy = 0; yy < 104; yy++) {
				cost[xx][yy] = 99999999;
			}
		}
		int curX = other.getLocalX();
		int curY = other.getLocalY();
		via[curX][curY] = 99;
		cost[curX][curY] = 0;
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = other.getMapRegionX() * 8 + curX;
			int curAbsY = other.getMapRegionY() * 8 + curY;
			if (curX == destX && curY == destY) {
				foundPath = true;
				break;
			}

			tail = (tail + 1) % pathLength;
			int thisCost = cost[curX][curY] + 1;
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							other.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
				cost[curX][curY - 1] = thisCost;
			}
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							other.heightLevel) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
				cost[curX - 1][curY] = thisCost;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							other.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
				cost[curX][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							other.heightLevel) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
				cost[curX + 1][curY] = thisCost;
			}

			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY - 1,
							other.heightLevel) & 0x128010e) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							other.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							other.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
				cost[curX - 1][curY - 1] = thisCost;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY + 1,
							other.heightLevel) & 0x1280138) == 0
					&& (RegionClip.getClipping(curAbsX - 1, curAbsY,
							other.heightLevel) & 0x1280108) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							other.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
				cost[curX - 1][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY - 1,
							other.heightLevel) & 0x1280183) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							other.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY - 1,
							other.heightLevel) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
				cost[curX + 1][curY - 1] = thisCost;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY + 1,
							other.heightLevel) & 0x12801e0) == 0
					&& (RegionClip.getClipping(curAbsX + 1, curAbsY,
							other.heightLevel) & 0x1280180) == 0
					&& (RegionClip.getClipping(curAbsX, curAbsY + 1,
							other.heightLevel) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
				cost[curX + 1][curY + 1] = thisCost;
			}
			
		}
		if (!foundPath) {
			int i_223_ = 1000;
			int thisCost = 100;
			int i_225_ = 10;
			for (int x = destX - i_225_; x <= destX + i_225_; x++) {
				for (int y = destY - i_225_; y <= destY + i_225_; y++) {
					if (x >= 0 && y >= 0 && x < 104 && y < 104
							&& cost[x][y] < 100) {
						int i_228_ = 0;
						if (x < destX) {
							i_228_ = destX - x;
						} else if (x > destX) {
							i_228_ = x - (destX);
						}
						int i_229_ = 0;
						if (y < destY) {
							i_229_ = destY - y;
						} else if (y > destY) {
							i_229_ = y - (destY);
						}
						int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;
						if (i_230_ < i_223_ || (i_230_ == i_223_
								&& (cost[x][y] < thisCost))) {
							i_223_ = i_230_;
							thisCost = cost[x][y];
							curX = x;
							curY = y;
						}
					}
				}
			}
			if (i_223_ == 1000) {
				return Location.create(destX, destY, destZ);
			}
		}
		tail = 0;
		tileQueueX.set(tail, curX);
		tileQueueY.set(tail++, curY);
		int pathX = other.getMapRegionX() * 8 + tileQueueX.getFirst();
		int pathY = other.getMapRegionY() * 8 + tileQueueY.getFirst();

		tileQueueX = null;
		tileQueueY = null;
		
		return Location.create(pathX, pathY, destZ);
	}
	
	
}
