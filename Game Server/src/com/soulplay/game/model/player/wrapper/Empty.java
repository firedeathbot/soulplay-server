package com.soulplay.game.model.player.wrapper;

import com.soulplay.game.model.player.Player;

public class Empty {

	String player;

	int[] playerItems;

	int[] playerItemsN;

	private String connectedFrom;

	private String UUID;

	private int mysqlIndex;

	public Empty(Player player, int[] playerItems, int[] playerItemsN) {
		this.player = player.getName();
		this.playerItems = playerItems;
		this.playerItemsN = playerItemsN;
		this.connectedFrom = player.connectedFrom;
		this.UUID = player.UUID;
		this.setMysqlIndex(player.mySQLIndex);
	}

	public String getConnectedFrom() {
		return connectedFrom;
	}

	public int getMysqlIndex() {
		return mysqlIndex;
	}

	public String getPlayer() {
		return player;
	}

	public int[] getPlayerItems() {
		return playerItems;
	}

	public int[] getPlayerItemsN() {
		return playerItemsN;
	}

	public String getUUID() {
		return UUID;
	}

	public void setMysqlIndex(int mysqlIndex) {
		this.mysqlIndex = mysqlIndex;
	}
}
