package com.soulplay.game.model.player;

import java.util.Arrays;

import com.soulplay.Config;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.tob.TobManager;
import com.soulplay.content.tob.npcs.VerzikVitur;
import com.soulplay.content.tob.npcs.VerzikWeb;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.util.Misc;

public final class Movement {
	
	public short[] movementAnimations = { 0x328, 0x337, 0x333, 0x334, 0x335, 0x336, 0x338 };
	
	public short[] backupMovementAnimations = { 0x328, 0x337, 0x333, 0x334, 0x335, 0x336, 0x338 };
	
	private final Player player;	
	
	public int newWalkCmdSteps = 0;

	private boolean newWalkCmdIsRunning = false;
	
	public int oldWalkIndex = 0x333;

	public boolean animatedStep = false;
	
	public final int walkingQueueSize = 50;

	public int walkingQueueX[] = new int[walkingQueueSize], walkingQueueY[] = new int[walkingQueueSize];
	
	private int newWalkCmdX[] = new int[walkingQueueSize];

	private int newWalkCmdY[] = new int[walkingQueueSize];
	
	public int wQueueReadPtr = 0;

	public int wQueueWritePtr = 0;
	
	protected int travelBackX[] = new int[walkingQueueSize];

	protected int travelBackY[] = new int[walkingQueueSize];

	protected int numTravelBackSteps = 0;
	
	public Movement(Player c) {
		this.player = c;
	}
	
	public int[] getNewWalkCmdX() {
		return newWalkCmdX;
	}

	public int[] getNewWalkCmdY() {
		return newWalkCmdY;
	}



	public int getNextWalkingDirection() {
		if (wQueueReadPtr == wQueueWritePtr || player.isLockActions() || !player.getLockMovement().complete()) {
			return -1;
		}

		int dir;
		do {
			dir = Misc.direction(player.currentX, player.currentY, walkingQueueX[wQueueReadPtr], walkingQueueY[wQueueReadPtr]);
			if (dir == -1) {
				wQueueReadPtr = (wQueueReadPtr + 1) % walkingQueueSize;
			} else if ((dir & 1) != 0) {
				player.println_debug("Invalid waypoint in walking queue! PlayerName:" + player.getName());
				hardResetWalkingQueue();
				return -1;
			}
		} while ((dir == -1) && (wQueueReadPtr != wQueueWritePtr));
		if (dir == -1) {
			return -1;
		}
		dir >>= 1;

		// boolean prevInFunPK = false;

		if (!player.getLockMovement().complete() || player.teleTimer > 0) {
			return -1;
		}

		if (player.isRooted()) {
			hardResetWalkingQueue();
			player.getPacketSender().resetMinimapFlagPacket();
			return -1;
		}

		if (player.getFreezeTimer() > 0 && !player.performingAction && !player.isShoved()) {
			hardResetWalkingQueue();
			player.getPacketSender().resetMinimapFlagPacket();
			return -1;
		}
		
		if (player.getZones().isInClwRedPortalPkZone() && player.isRedSkulled() && !player.isInteractionResetTimerDone() && player.getY() == 5512 && dir == 4) {
			player.sendMessage("You cannot exit during combat with red skull on.");
			hardResetWalkingQueue();
			player.getPacketSender().resetMinimapFlagPacket();
			return -1;
		}

		if (!player.noClip /* && (absX != 2377 && absY != 3088) && (absX != 2378 && absY != 3084) && (absX != 2420 && absY != 3123) && (absX  != 2418 && absY != 3125) && (absX != 2419 && absY != 3124) */) {
			if (!PathFinder.canMove(player.absX, player.absY, player.heightLevel, Misc.directionDeltaX[dir], Misc.directionDeltaY[dir], player.getDynamicRegionClip())) {
				if (!PathFinder.canStepOffObject(player.absX, player.absY, player.heightLevel, Misc.directionDeltaX[dir],
						Misc.directionDeltaY[dir], player.getDynamicRegionClip())) {
					hardResetWalkingQueue();
					player.getPacketSender().resetMinimapFlagPacket();
					return -1;
				}
			}

			// if (inFunPk())
			// prevInFunPK = true;

//			if (player.inFunPk() && !RSConstants.inFunPk(player.absX + Misc.directionDeltaX[dir], player.absY + Misc.directionDeltaY[dir])) {
//				player.sendMessage("<col=ff0000>You have reached the edge of the Fun PK Tournament Arena.");
//				hardResetWalkingQueue();
//				return -1;
//			}
		}

		TobManager tobManager = player.tobManager;
		if (tobManager != null && tobManager.currentRoom instanceof VerzikRoom) {
			VerzikVitur verzik = ((VerzikRoom) tobManager.currentRoom).getVerzik();
			if (verzik != null && verzik.getPhaseId() == 3) {
				for (int i = 0, length = tobManager.npcs.size(); i < length; i++) {
					NPC npc = tobManager.npcs.get(i);
					if (npc.getNpcTypeSmart() == VerzikWeb.ID && npc.isActive) {
						VerzikWeb web = (VerzikWeb) npc;
						Location cur = npc.getCurrentLocation();
						if (cur.getX() == player.getX() && cur.getY() == player.getY() && cur.getZ() == player.getZ() && web.isWebAppeared()) {
							player.sendMessage("You're stuck in the web!");
							hardResetWalkingQueue();
							player.getPacketSender().resetMinimapFlagPacket();
							return -1;
						}
					}
				}
			}
		}

		if (!player.noClip && (player.inCwGame || player.soulWarsTeam != Team.NONE || player.insideDungeoneering())) {
			if (Misc.directionDeltaX[dir] != 0 && Misc.directionDeltaY[dir] != 0) { // to check if walking diagonal past two barricades
				if (RegionClip.isBarricadeHere(player.getX() + Misc.directionDeltaX[dir], player.getY(), player.getZ(), player.getDynamicRegionClip()) && RegionClip.isBarricadeHere(player.getX(), player.getY() + Misc.directionDeltaY[dir], player.getZ(), player.getDynamicRegionClip()))
					return -1;
				else if (RegionClip.isBarricadeHere(player.getX() + Misc.directionDeltaX[dir], player.getY() + Misc.directionDeltaY[dir], player.getZ(), player.getDynamicRegionClip()))
					return -1;
			} else {
				if (RegionClip.isBarricadeHere(player.getX() + Misc.directionDeltaX[dir], player.getY() + Misc.directionDeltaY[dir], player.getZ(), player.getDynamicRegionClip()))
					return -1;
			}
			
			// this method blocks diagonal walking
//			if (Misc.directionDeltaX[dir] != 0) {
//				if (RegionClip.isBarricadeHere(player.getX() + Misc.directionDeltaX[dir], player.getY(), player.getZ())) {
//					return -1;
//				}
//			}
//			if (Misc.directionDeltaY[dir] != 0) {
//				if (RegionClip.isBarricadeHere(player.getX(), player.getY() + Misc.directionDeltaY[dir], player.getZ())) {
//					return -1;
//				}
//			}
		}
		// sendMessage("X "+(absX+Misc.directionDeltaX[dir]));
		// if (inCwGame && checkBarricade(absX+Misc.directionDeltaX[dir],
		// absY+Misc.directionDeltaY[dir], heightLevel)) {
		// return -1;
		// }

		// if (duelStatus == 5) {
		// for (Player p : getRegion().getPlayers()) {
		// if (p == null || p.disconnected)
		// continue;
		// if (p.getHeightLevel() != getHeightLevel())
		// continue;
		// if (p.getX() == absX+Misc.directionDeltaX[dir] && p.getY() ==
		// absY+Misc.directionDeltaY[dir])
		// return -1;
		// }
		// }

		player.lastX = player.absX;
		player.lastY = player.absY;
		player.currentX += Misc.directionDeltaX[dir];
		player.currentY += Misc.directionDeltaY[dir];
		player.absX += Misc.directionDeltaX[dir];
		player.absY += Misc.directionDeltaY[dir];

		// if (prevInFunPK == true && !inFunPk()) {
		// Client c = (Client)this;
		// c.getPA().movePlayer(3302, 3110, 0);
		//
		// }

		player.setLocation(player.absX, player.absY, player.heightLevel);
		if (player.noClip) {

			if (player.noClipX == player.absX && player.noClipY == player.absY) {
				player.noClip = false;
				player.noClipX = player.noClipY = 0;
			}
			// if(noClipX > 0 || noClipY > 0) {
			// if(!goodDistance(noClipX, noClipY, absX, absY, 60)) {
			// noClipX = 0;
			// noClipY = 0;
			// noClip = false;
			// }
			// }
		}

		return dir;
	}
	
	public /* synchronized */ void postProcessing() {
		if (newWalkCmdSteps > 0) {
			int firstX = getNewWalkCmdX()[0], firstY = getNewWalkCmdY()[0];

			int lastDir = 0;
			boolean found = false;
			numTravelBackSteps = 0;
			int ptr = wQueueReadPtr;
			int dir = Misc.direction(player.currentX, player.currentY, firstX, firstY);
			if (dir != -1 && (dir & 1) != 0) {
				do {
					lastDir = dir;
					if (--ptr < 0) {
						ptr = walkingQueueSize - 1;
					}

					travelBackX[numTravelBackSteps] = walkingQueueX[ptr];
					travelBackY[numTravelBackSteps++] = walkingQueueY[ptr];
					dir = Misc.direction(walkingQueueX[ptr], walkingQueueY[ptr], firstX, firstY);
					if (lastDir != dir) {
						found = true;
						break;
					}

				} while (ptr != wQueueWritePtr);
			} else {
				found = true;
			}

			if (!found) {
				player.println_debug("Fatal: couldn't find connection vertex! Dropping packet.");
			} else {
				wQueueWritePtr = wQueueReadPtr;

				addToWalkingQueue(player.currentX, player.currentY);

				if (dir != -1 && (dir & 1) != 0) {

					for (int i = 0; i < numTravelBackSteps - 1; i++) {
						addToWalkingQueue(travelBackX[i], travelBackY[i]);
					}
					int wayPointX2 = travelBackX[numTravelBackSteps - 1],
							wayPointY2 = travelBackY[numTravelBackSteps - 1];
					int wayPointX1, wayPointY1;
					if (numTravelBackSteps == 1) {
						wayPointX1 = player.currentX;
						wayPointY1 = player.currentY;
					} else {
						wayPointX1 = travelBackX[numTravelBackSteps - 2];
						wayPointY1 = travelBackY[numTravelBackSteps - 2];
					}

					dir = Misc.direction(wayPointX1, wayPointY1, wayPointX2, wayPointY2);
					if (dir == -1 || (dir & 1) != 0) {
						player.println_debug("Fatal: The walking queue is corrupt! wp1=(" + wayPointX1 + ", " + wayPointY1
								+ "), " + "wp2=(" + wayPointX2 + ", " + wayPointY2 + ")");
					} else {
						dir >>= 1;
						found = false;
						int x = wayPointX1, y = wayPointY1;
						while (x != wayPointX2 || y != wayPointY2) {
							x += Misc.directionDeltaX[dir];
							y += Misc.directionDeltaY[dir];
							if ((Misc.direction(x, y, firstX, firstY) & 1) == 0) {
								found = true;
								break;
							}
						}
						if (!found) {
							player.println_debug("Fatal: Internal error: unable to determine connection vertex!" + "  wp1=("
									+ wayPointX1 + ", " + wayPointY1 + "), wp2=(" + wayPointX2 + ", " + wayPointY2
									+ "), " + "first=(" + firstX + ", " + firstY + ")");
						} else {
							addToWalkingQueue(wayPointX1, wayPointY1);
						}
					}
				} else {
					for (int i = 0; i < numTravelBackSteps; i++) {
						addToWalkingQueue(travelBackX[i], travelBackY[i]);
					}
				}

				for (int i = 0; i < newWalkCmdSteps; i++) {
					addToWalkingQueue(getNewWalkCmdX()[i], getNewWalkCmdY()[i]);
				}

			}

			player.isRunning = isNewWalkCmdIsRunning() || player.isRunning;
		}
	}
	
	public void preProcessing() {
		newWalkCmdSteps = 0;
	}
	
	public /* synchronized */ void getNextPlayerMovement() {
		player.mapRegionDidChange = false;
		// didTeleport = false;
		player.dir1 = -1;
		player.runTiles.clear();
		player.isRunningOnThisTick = false;

		if (player.teleportToX != -1 && player.teleportToY != -1 && player.teleportToZ != -1) {
			player.mapRegionDidChange = true;
			if (player.mapRegionX != -1 && player.mapRegionY != -1) {
				int relX = player.teleportToX - player.mapRegionX * 8, relY = player.teleportToY - player.mapRegionY * 8;
				if (relX >= 2 * 8 && relX < 11 * 8 && relY >= 2 * 8 && relY < 11 * 8) {
					player.mapRegionDidChange = false;
				}
			}
			if (player.mapRegionDidChange) {
				player.mapRegionX = (player.teleportToX >> 3) - 6;
				player.mapRegionY = (player.teleportToY >> 3) - 6;
			}
			player.currentX = player.teleportToX - 8 * player.mapRegionX;
			player.currentY = player.teleportToY - 8 *player. mapRegionY;
			player.absX = player.teleportToX;
			player.absY = player.teleportToY;
			player.heightLevel = player.teleportToZ;
			player.setLocation(player.absX, player.absY, player.heightLevel);
			hardResetWalkingQueue();

			if (animatedStep) {
				animatedStep = false;
				// playerWalkIndex = oldWalkIndex;
				getPlayerMovementAnimIds()[2] = (short) oldWalkIndex;
				player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
			}

			player.teleportToX = player.teleportToY = -1;
			player.teleportToZ = -1;
			player.didTeleport = true;
		} else {
			player.dir1 = getNextWalkingDirection();
			if (player.dir1 != -1) {
				player.keepWalkFlag = true;
			}
			if (player.dir1 == -1) {
				return;
			}
			if (player.getMovement().isRunning()) {
				for (int i = 0; i < player.runTilesPerTick; i++) {
					int direction = getNextWalkingDirection();
					if (direction == -1) {
						break;
					}

					player.runTiles.add(direction);
				}

				if (!player.runTiles.isEmpty()) {
					player.keepWalkFlag = true;
					
					player.isRunningOnThisTick = true;
					
					if (player.wildLevel > 0 || player.inMinigame() || player.getX() > 6400) {
						if (player.getRunEnergy() > 0) {
							drainRunEnergy();
						} else {
							player.isRunning = false;
							player.getPacketSender().setConfig(173, 0);
							player.isRunningOnThisTick = false;
						}
					}
					
				}
			}
			// sendMessage("dir1:"+dir1+" dir2:"+dir2);
			int deltaX = 0, deltaY = 0;
			if (player.currentX < 2 * 8) {
				deltaX = 4 * 8;
				player.mapRegionX -= 4;
				player.mapRegionDidChange = true;
			} else if (player.currentX >= 11 * 8) {
				deltaX = -4 * 8;
				player.mapRegionX += 4;
				player.mapRegionDidChange = true;
			}
			if (player.currentY < 2 * 8) {
				deltaY = 4 * 8;
				player.mapRegionY -= 4;
				player.mapRegionDidChange = true;
			} else if (player.currentY >= 11 * 8) {
				deltaY = -4 * 8;
				player.mapRegionY += 4;
				player.mapRegionDidChange = true;
			}

			if (player.mapRegionDidChange) {
				player.currentX += deltaX;
				player.currentY += deltaY;
				for (int i = 0; i < walkingQueueSize; i++) {
					walkingQueueX[i] += deltaX;
					walkingQueueY[i] += deltaY;
				}
			}
			// CoordAssistant.processCoords(this);

		}

		if (player.isShoved()) {
			player.setShoved(false);
		}
		
	}
	
	public void addToWalkingQueue(int x, int y) {
		if (player.getLocalX() == x && player.getLocalY() == y) { // no need to add to walking
													// queue if standing on same
													// spot.
			return;
		}
		// if (VirtualWorld.I(heightLevel, absX, absY, x, y, 0)) {
		int next = (wQueueWritePtr + 1) % walkingQueueSize;
		if (next == wQueueWritePtr) {
			return;
		}
		walkingQueueX[wQueueWritePtr] = x;
		walkingQueueY[wQueueWritePtr] = y;
		wQueueWritePtr = next;
		// }
	}

	public void resetDestination() {
		player.destinationX = 0;
		player.destinationY = 0;
	}

	public void hardResetWalkingQueue() {
		hardResetWalkingQueue(true);
	}

	public void hardResetWalkingQueue(boolean resetDestination) {
		wQueueReadPtr = wQueueWritePtr = 0;

		for (int i = 0; i < walkingQueueSize; i++) {
			walkingQueueX[i] = player.currentX;
			walkingQueueY[i] = player.currentY;
		}

		player.noClip = false; // resetwalkingqueue probably got people stuck so i
						// should put these resets too
		player.noClipX = player.noClipY = 0; // resetwalkingqueue probably got people stuck so
								// i should put these resets too

		if (resetDestination) {
			resetDestination();
		}
	}
	
	public boolean isNewWalkCmdIsRunning() {
		return newWalkCmdIsRunning;
	}
	
	public boolean canRun() {
		if (!canMove()) {
			return false;
		}
		if (player.getRunEnergy() < 1)
			return false;
		return true;
	}
	
	public boolean canMove() {
		if (player.getFreezeTimer() > 0)
			return false;
		if (player.isStunned())
			return false;
		if (player.isRooted())
			return false;
		return true;
	}
	
	public boolean isWalkingOrRunningAtThisPoint() {
		if (player.dir1 == -1 && player.runTiles.isEmpty()) {
			return false;
		}
		return true;
	}
	
	public boolean isMovingOrHasWalkQueue() {
		if (!canMove()) {
			return false;
		}
		if (isWalkingOrRunningAtThisPoint())
			return true;
		if (wQueueReadPtr == wQueueWritePtr) {
			return false;
		} else {
			return true;
		}
	}
	
	public void resetWalkingQueue() {
		// cheapfix to getting stuck on agility
//		if (player.performingAction) {
//			return;
//		}
		hardResetWalkingQueue();
	}
	
	public boolean isRunning() {
		return (isNewWalkCmdIsRunning() || player.isRunning) && !player.blockRun;
	}
	
	public boolean isRunningOnThisTick() {
		return !player.runTiles.isEmpty();
	}
	
	public boolean isWalkingOnThisTick() {
		return player.dir1 > -1;
	}
	
	public void setNewWalkCmdIsRunning(boolean newWalkCmdIsRunning) {
		this.newWalkCmdIsRunning = newWalkCmdIsRunning;
	}

	public void setNewWalkCmdX(int newWalkCmdX[]) {
		this.newWalkCmdX = newWalkCmdX;
	}

	public void setNewWalkCmdY(int newWalkCmdY[]) {
		this.newWalkCmdY = newWalkCmdY;
	}
	
	public void animateStep(int animId) {
		oldWalkIndex = getPlayerMovementAnimIds()[2];// playerWalkIndex;
		// playerWalkIndex = animId;
		getPlayerMovementAnimIds()[2] = (short) animId;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		animatedStep = true;
	}
	
	public int getWalkAnim() {
		return getPlayerMovementAnimIds()[2];
	}
	
	public void changeWalk(int anim) {
		getPlayerMovementAnimIds()[2] = (short) anim;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	
	public int getStandAnim() {
		return getPlayerMovementAnimIds()[0];
	}
	
	public void changeStance(int anim) {
		getPlayerMovementAnimIds()[0] = (short) anim;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	
	public void setCurrentPlayerMovementAnimIds(short[] newIds) {
		movementAnimations = newIds;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	
	public short[] getPlayerMovementAnimIds() {
		return movementAnimations;
	}
	
	public short[] copyMovementAnims() {
		return Arrays.copyOf(movementAnimations, movementAnimations.length);
	}
	
	public void setTempMovementAnim(int id) {
		backupMovementAnimations = copyMovementAnims();
		for (int i = 0; i < movementAnimations.length; i++) {
			movementAnimations[i] = (short) id;
		}
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

	public void revertMovementAnims() {
		movementAnimations = Arrays.copyOf(backupMovementAnimations, backupMovementAnimations.length);
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	
	private void drainRunEnergy() {
		if (!Config.RUN_ON_DEDI)
			return;
		double drainRate = (player.getWeight64()/100.0D + 0.64D) * (player.staminaPotRunning() ? 0.3D : 1.0D);
		if (player.getItems().playerHasEquippedWeapon(MagicalCrate.MAGICAL_CRATE_ITEM_ID))
			drainRate += 10.0D;
		else if (player.insideDungeoneering() && player.getDonatedTotalAmount() >= 50) {
			drainRate *= 0.5D;
		}

		player.setRunEnergy(player.getRunEnergy() - drainRate);
	}
	
	public void restoreRunEnergyProcess() {
		if (player.isRunningOnThisTick)
			return;
		
		double rate = (((player.getSkills().getLevel(Skills.AGILITY)/6.0D) + 8.0D) /100.0D);
		
		if (player.runRecoverBoost != 0) { // rest doubles the rate
			rate *= 4;
		}
		
		player.setRunEnergy(player.getRunEnergy() + rate);
	}
	
	public void restoreRunEnergy(double amount) {
		player.setRunEnergy(player.getRunEnergy() + amount);
	}
	
	public double drainEnergy(double amount) {
		if (player.getRunEnergy() < amount) {
			amount = player.getRunEnergy();
		}
		player.setRunEnergy(player.getRunEnergy() - amount);
		return amount;
	}
	
	public void startResting() {
		if (player.getTransformId() > -1) {
			player.sendMessage("You cannot rest while you are transformed.");
			return;
		}
		player.getCombat().resetPlayerAttack();
		player.getMovement().resetWalkingQueue();
		player.startAnimation(Misc.random(1) == 1 ? 5713 : 11786);//player.startAnimation(5713);
		player.runRecoverBoost = 1;
	}
	
	public boolean toggleRun() {
		if (player.getRunEnergy() < 1) {
			player.isRunning = false;
			player.getPacketSender().setConfig(173, 0);
			return false;
		}

		player.isRunning = !player.isRunning;
		player.getPacketSender().setConfig(173, player.isRunning ? 1 : 0);
		return true;
	}

}
