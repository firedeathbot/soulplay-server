package com.soulplay.game.model.player;

import java.util.Arrays;
import java.util.List;

import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.TileControl;

public class PlayerConstants {

	public final static int SAVE_TICKS = 500;
	
	public final static int SEARCH_STATE = 3;

	public final static int SET_SELL_PRICE_STATE = 4;

	public final static int SET_DIALOG_AMOUNT_TO_WITHDRAW = 5;
	
	public final static int SET_DIALOG_AMOUNT_TO_SELL = 8;

	public static final int[] DUEL_RULE_ID = {1, 2, 16, 32, 64, 128, 256, 512,
			1024, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288,
			2097152, 8388608, 16777216, 67108864, 134217728};

	public static final int maxNPCListSize = 255;
	public static final int maxNPCListSizeCheck = 254;

	public static final String[] SKILLNAMES = {"Attack", "Defense", "Strength",
			"Hitpoints", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting",
			"Fletching", "Fishing", "Firemaking", "Crafting", "Smithing",
			"Mining", "Herblore", "Agility", "Thieving", "Slayer", "Farming",
			"Runecrafting", "Construction", "Hunter", "Summoning",
			"Dungeoneering"};

	public static final int playerHat = 0;

	public static final int playerCape = 1;

	public static final int playerAmulet = 2;

	public static final int playerWeapon = 3;

	public static final int playerChest = 4;

	public static final int playerShield = 5;

	public static final int playerLegs = 7;

	public static final int playerHands = 9;

	public static final int playerFeet = 10;

	public static final int playerRing = 12;

	public static final int playerArrows = 13;

	public static double MELEE_ACCURACY = 1.0;

	public static double MAGIC_ACCURACY = 1.0;

	public static double RANGE_ACCURACY = 1.0;

	public static double RANGE_ACCURACY_PVM = 1.0;

	public static final int EMOTE_STAB_SWORD = 386;

	public static final int EMOTE_BLOCK_SWORD = 388;

	public static final int EMOTE_SLASH_SWORD = 390;

	public static final int EMOTE_SLASH_SCIMITAR = 12029;

	public static final int EMOTE_STAB_SCIMITAR = 12028;

	public static final int EMOTE_STAB_LONGSWORD = 13049;

	public static final int EMOTE_BLOCK_LONGSWORD = 13042;

	public static final int EMOTE_SLASH_LONGSWORD = 13048;

	public static final int EMOTE_STAB_SPEAR = 12006;

	public static final int EMOTE_BLOCK_SPEAR = 12004;

	public static final int EMOTE_SLASH_SPEAR = 12005;

	public static final int EMOTE_CRUSH_SPEAR = 12009;

	public static final int EMOTE_CHARGING_STAB = 15072;

	public static int nomadReward[] = {21472, 21473, 21474, 21475, 21476, 21467,
			21468, 21469, 21470, 21471, 21462, 21463, 21464, 21465, 21466};

	public static int wildyKeySupply[] = {18831, 537, 11212, 9244, 11091, 565,
			560, 1748};

	public static int redWildyKey[] = {4587, 4087, 3140, 1149, 1434, 7158, 1187,
			2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587, 4087,
			3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918, 6920,
			6922, 6924, 4153, 4587, 4087, 3140, 1149, 1434, 7158, 1187, 2491,
			2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587, 4087, 3140,
			1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922,
			6924, 4153, 4587, 4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497,
			2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587, 4087, 3140, 1149,
			1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924,
			4153, 4587, 4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503,
			6916, 6918, 6920, 6922, 6924, 4153, 4587, 4087, 3140, 1149, 1434,
			7158, 1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153,
			4587, 4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916,
			6918, 6920, 6922, 6924, 4153, 4587, 4087, 3140, 1149, 1434, 7158,
			1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587,
			4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918,
			6920, 6922, 6924, 4153,
			// barrows
			4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728,
			4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755,
			4757, 4759, 4151, 6585, 18333, 4708, 4710, 4712, 4714, 4716, 4718,
			4720, 4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745,
			4747, 4749, 4751, 4753, 4755, 4757, 4759, 4151, 6585, 18333, 4708,
			4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728, 4730,
			4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755, 4757,
			4759, 4151, 6585, 18333,
			// pvp items
			13884, 13890, 13896, 13902, 13887, 13893, 13899, 13905, 13870,
			13873, 13876, 13879, 13883, 13858, 13861, 13864, 13867, 18333,
			18334, 18335,
			// Corrupted pvp items
			13908, 13911, 13932, 13944, 13914, 13917, 13947, 13935, 13920,
			13938, 13929, 13923, 13941, 13926};

	public static int purpleWildyKey[] = {4587, 4087, 3140, 1149, 1434, 7158,
			1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587,
			4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918,
			6920, 6922, 6924, 4153, 4587, 4087, 3140, 1149, 1434, 7158, 1187,
			2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153,
			// barrows
			4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728,
			4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755,
			4757, 4759, 4151, 6585, 18333, 4708, 4710, 4712, 4714, 4716, 4718,
			4720, 4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745,
			4747, 4749, 4751, 4753, 4755, 4757, 4759, 4151, 6585, 18333,
			// pvp items
			13884, 13890, 13896, 13902, 13887, 13893, 13899, 13905, 13870,
			13873, 13876, 13879, 13883, 13858, 13861, 13864, 13867, 18333,
			18334, 18335,
			// Corrupted pvp items
			13908, 13911, 13932, 13944, 13914, 13917, 13947, 13935, 13920,
			13938, 13929, 13923, 13941, 13926};

	public static int blueWildyKey[] = {4587, 4087, 3140, 1149, 1434, 7158,
			1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587,
			4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918,
			6920, 6922, 6924, 4153, 4587, 4087, 3140, 1149, 1434, 7158, 1187,
			2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587, 4087,
			3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918, 6920,
			6922, 6924, 4153, 4587, 4087, 3140, 1149, 1434, 7158, 1187, 2491,
			2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153,
			// barrows
			4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728,
			4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755,
			4757, 4759, 4151, 6585, 18333};

	public static int greenWildyKey[] = {4587, 4087, 3140, 1149, 1434, 7158,
			1187, 2491, 2497, 2503, 6916, 6918, 6920, 6922, 6924, 4153, 4587,
			4087, 3140, 1149, 1434, 7158, 1187, 2491, 2497, 2503, 6916, 6918,
			6920, 6922, 6924, 4153,
			// barrows
			4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728,
			4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755,
			4757, 4759, 4151, 6585, 18333};

	public static int[] pvpGear = {
			// Normal pvp items
			13884, 13890, 13896, 13902, 13887, 13893, 13899, 13905, 13870,
			13873, 13876, 13879, 13883, 13858, 13861, 13864, 13867, 18333,
			18334, 18335,
			// Corrupted pvp items
			13908, 13911, 13932, 13944, 13914, 13917, 13947, 13935, 13920,
			13938, 13929, 13923, 13941, 13926, 13950,
			// other rares
			7447, // kitchen knife
			12002, // occult necklace
			11791, // staff of the dead
			12004, // kraken tentacle
			6542, // pk package

	};

	public static int pkGear[] = {4708, 4710, 4712, 4714, 4716, 4718, 4720,
			4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747,
			4749, 4751, 4753, 4755, 4757, 4759, 4151, 6585, 18333};

	public static int Crystal[] = {6585, 10840, 2476, 11257, 11257, 11257,
			11286, 10939, 10939, 11732, 14499, 11235, 11128, 10939, 10940,
			10941, 10862, 10858, 10828, 10828, 10825, 10802, 10796, 10772,
			10770, 10768, 10766, 10764, 10762, 10760, 10758, 10756, 10754,
			10752, 10750, 10748, 10742, 10736, 10732, 10728, 10727, 10726,
			10725, 10724, 10722, 10721, 10718, 10717, 10716, 10715, 10714,
			10713, 10712, 10710, 10708, 10707, 10705, 10703, 10702, 10700,
			10696, 10694, 10693, 10689, 10688, 10679, 10678, 10677, 10676,
			10675, 10674, 10673, 10672, 10671, 10670, 10669, 10668, 10667,
			10666, 10665, 10664, 10663, 10654, 10635, 10633, 10632, 10631,
			10630, 10629, 10628, 10627, 10626, 10625, 10624, 10623, 10622,
			10621, 10612, 10604, 10555, 10552, 10551, 10550, 10549, 10548,
			10547, 10400, 10402, 10404, 10406, 10408, 10410, 10412, 104014,
			10416, 10418, 10420, 10422, 10424, 10426, 10428, 10430, 10432,
			10434, 10436, 10438, 10344, 10346, 10077, 10079, 10081, 10083,
			10085, 10067, 10063, 10059, 10055, 10047, 10045, 10041, 10039,
			10035, 10023, 9813, 9814, 9790, 9789, 9678, 9676, 9674, 9672, 9629,
			9097, 9098, 9099, 9100, 9084, 9044, 8971, 8970, 8969, 8968, 8967,
			8966, 8965, 8964, 8963, 8962, 8961, 8960, 8959, 8958, 8957, 8956,
			8955, 8954, 8953, 8952, 8950, 8856, 8650, 8652, 8654, 8656, 8658,
			8660, 8662, 8664, 8666, 8668, 8670, 8672, 8674, 8676, 8678, 8680,
			7809, 7808, 7807, 7806, 7804, 7803, 7668, 6733, 4151, 2528, 2528,
			2528, 2528, 2528, 2528};
	/* Treasure */

	public static int lowLevelReward[] = {1077, 1089, 1107, 1125, 1131, 1129,
			1133, 1511, 1168, 1165, 1179, 1195, 1217, 1283, 1297, 1313, 1327,
			1341, 1361, 1367, 1426, 2633, 2635, 2637, 7388, 7386, 7392, 7390,
			7396, 7394, 2631, 7364, 7362, 7368, 7366, 2583, 2585, 2587, 2589,
			2591, 2593, 2595, 2597, 7332, 7338, 7350, 7356};

	public static int mediemLevelReward[] = {2599, 2601, 2603, 2605, 2607, 2609,
			2611, 2613, 7334, 7340, 7346, 7352, 7358, 7319, 7321, 7323, 7325,
			7327, 7372, 7370, 7380, 7378, 2645, 2647, 2649, 2577, 2579, 1073,
			1091, 1099, 1111, 1135, 1124, 1145, 1161, 1169, 1183, 1199, 1211,
			1245, 1271, 1287, 1301, 1317, 1332, 1357, 1371, 1430, 6916, 6918,
			6920, 6922, 6924, 10400, 10402, 10416, 10418, 10420, 10422, 10436,
			10438, 10446, 10448, 10450, 10452, 10454, 10456, 6889};

	public static int highLevelReward[] = {1079, 1093, 1113, 1127, 1147, 1163,
			1185, 1201, 1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503,
			861, 859, 2581, 2577, 2651, 1079, 1093, 1113, 1127, 1147, 1163,
			1185, 1201, 1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503,
			861, 859, 2581, 2577, 2651, 2615, 2617, 2619, 2621, 2623, 2625,
			2627, 2629, 2639, 2641, 2643, 2651, 2653, 2655, 2657, 2659, 2661,
			2663, 2665, 2667, 2669, 2671, 2673, 2675, 7342, 7348, 7454, 7460,
			7374, 7376, 7382, 7384, 7398, 7399, 7400, 3481, 3483, 3485, 3486,
			3488, 1079, 1093, 1113, 1127, 1148, 1164, 1185, 1201, 1213, 1247,
			1275, 1289, 1303, 1319, 1333, 1347, 1359, 1374, 1432, 2615, 2617,
			2619, 2621, 2623, 10330, 10332, 10334, 10336, 10344, 10368, 10376,
			10384, 10370, 10378, 10386, 10372, 10380, 10374, 10382, 10390,
			10470, 10472, 10474, 10440, 10442, 10444, 6914};

	public static int lowLevelStacks[] = {995, 380, 561, 886,};

	public static int mediumLevelStacks[] = {995, 374, 561, 563, 890,};

	public static int highLevelStacks[] = {995, 386, 561, 563, 560, 892};

	public static final List<Integer> SINGLE_TASK_ENTITY_INTERACT_PACKETS = Arrays
			.asList(new Integer[]{14, // ITEM ON PLAYER
					57, // ITEM ON NPC
					72, 131, 155, 17, 21, 18, // CLICK NPC
					/* 73, 249, */ 128, 39, 139, // INTERACT WITH PLAYER
					236, // pickup item
	});

	public static final int[] donatorRitem = {3062, 9946, 15422, 15423, 15425,
			15441, 15442, 15443, 15444, 18830, 19308, 19311, 19314, 19317,
			19320, 19370, 19323, 19372, 19368, 15018, 15019, 15020, 19354,
			19356, 19358, 19360, 15220, 15259, 13346, 13348, 13350, 13352,
			13354, 13355, 13360, 13358, /* 13655, */13362, 13366, 13370, 13340,
			13342, 13344, 20070, 20072, 4718, 4720, 4712, 4714, 4724, 4734,
			4736, 4738, 4749, 4753, 4757, 4759, 14484, 13899, 13902, 13742,
			13740, 13738, 11728, 11724, 11722, 11720, 11718, 11708, 11706,
			11704, 11702, 18333, 18334, 18335, 1050};

	public static final int[] PvpItems = {14876, 14877, 14878, 14879, 14880,
			14881, 14882, 14883, 14884, 14885, 14886, 14888, 14889, 14890,
			14891, 14892};

	public static int getDistance(int x1, int x2, int y1, int y2) {
		return (int) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	}

	public static boolean goodDistance(int x1, int y1, int x2, int y2,
			int distance) {
		return PlayerConstants.getDistance(x1, x2, y1, y2) <= distance;
	}

	// Clipping
	public static boolean pathBlocked(Client attacker, Client victim) {

		double offsetX = Math.abs(attacker.absX - victim.absX);
		double offsetY = Math.abs(attacker.absY - victim.absY);

		int distance = TileControl.calculateDistance(attacker, victim);

		if (distance == 0) {
			return true;
		}

		offsetX = offsetX > 0 ? offsetX / distance : 0;
		offsetY = offsetY > 0 ? offsetY / distance : 0;

		int[][] path = new int[distance][5];

		int curX = attacker.absX;
		int curY = attacker.absY;
		int next = 0;
		int nextMoveX = 0;
		int nextMoveY = 0;

		double currentTileXCount = 0.0;
		double currentTileYCount = 0.0;

		while (distance > 0) {
			distance--;
			nextMoveX = 0;
			nextMoveY = 0;
			if (curX > victim.absX) {
				currentTileXCount += offsetX;
				if (currentTileXCount >= 1.0) {
					nextMoveX--;
					curX--;
					currentTileXCount -= offsetX;
				}
			} else if (curX < victim.absX) {
				currentTileXCount += offsetX;
				if (currentTileXCount >= 1.0) {
					nextMoveX++;
					curX++;
					currentTileXCount -= offsetX;
				}
			}
			if (curY > victim.absY) {
				currentTileYCount += offsetY;
				if (currentTileYCount >= 1.0) {
					nextMoveY--;
					curY--;
					currentTileYCount -= offsetY;
				}
			} else if (curY < victim.absY) {
				currentTileYCount += offsetY;
				if (currentTileYCount >= 1.0) {
					nextMoveY++;
					curY++;
					currentTileYCount -= offsetY;
				}
			}
			path[next][0] = curX;
			path[next][1] = curY;
			path[next][2] = attacker.heightLevel;// getHeightLevel();
			path[next][3] = nextMoveX;
			path[next][4] = nextMoveY;
			next++;
		}
		for (int i = 0; i < path.length; i++) {
			if (!RegionClip.getClippingDirection(path[i][0], path[i][1], path[i][2],
					path[i][3], path[i][4], attacker.getDynamicRegionClip())
					&& !RegionClip.blockedShot(path[i][0], path[i][1],
							path[i][2], path[i][3], path[i][4], attacker.getDynamicRegionClip())) {
				return true;
			}
		}
		return false;
	}

	// clipping for npcs
	public static boolean pathBlocked(Client attacker, NPC victim) {
		try {

			double offsetX = Math.abs(attacker.absX - victim.absX);
			double offsetY = Math.abs(attacker.absY - victim.absY);

			int distance = TileControl.calculateDistance(attacker, victim);

			// attacker.sendMessage("distance "+distance);

			if (distance == 0) {
				return true;
			}

			offsetX = offsetX > 0 ? offsetX / distance : 0;
			offsetY = offsetY > 0 ? offsetY / distance : 0;

			int[][] path = new int[distance][5];

			int curX = attacker.absX;
			int curY = attacker.absY;
			int next = 0;
			int nextMoveX = 0;
			int nextMoveY = 0;

			double currentTileXCount = 0.0;
			double currentTileYCount = 0.0;

			while (distance > 0) {
				distance--;
				nextMoveX = 0;
				nextMoveY = 0;
				if (curX > victim.absX) {
					currentTileXCount += offsetX;
					if (currentTileXCount >= 1.0) {
						nextMoveX--;
						curX--;
						currentTileXCount -= offsetX;
					}
				} else if (curX < victim.absX) {
					currentTileXCount += offsetX;
					if (currentTileXCount >= 1.0) {
						nextMoveX++;
						curX++;
						currentTileXCount -= offsetX;
					}
				}
				if (curY > victim.absY) {
					currentTileYCount += offsetY;
					if (currentTileYCount >= 1.0) {
						nextMoveY--;
						curY--;
						currentTileYCount -= offsetY;
					}
				} else if (curY < victim.absY) {
					currentTileYCount += offsetY;
					if (currentTileYCount >= 1.0) {
						nextMoveY++;
						curY++;
						currentTileYCount -= offsetY;
					}
				}
				path[next][0] = curX;
				path[next][1] = curY;
				path[next][2] = attacker.heightLevel;
				path[next][3] = nextMoveX;
				path[next][4] = nextMoveY;
				next++;
			}
			for (int i = 0; i < path.length; i++) {
				if (!RegionClip.getClippingDirection(path[i][0], path[i][1], path[i][2],
						path[i][3], path[i][4], attacker.getDynamicRegionClip())
						&& !RegionClip.blockedShot(path[i][0], path[i][1],
								path[i][2], path[i][3], path[i][4], attacker.getDynamicRegionClip())) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
