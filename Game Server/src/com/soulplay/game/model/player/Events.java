package com.soulplay.game.model.player;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;

public class Events {

	/*
	 * Guard Event (Relates to Thieving) Randomly Called through the
	 * stealFromStall method
	 * @param c
	 */

	public static int[][] NPCs = {{3, 10, 1, 19, 1}, {11, 20, 9, 40, 2},
			{21, 40, 23, 80, 4}, {41, 60, 23, 90, 5}, {61, 90, 21, 105, 7},
			{91, 110, 21, 120, 9}, {111, 138, 21, 150, 12},};

	public static int PrestigeNpc = 648;

	public static void Guard(Client c) {
		int spawnX = c.getX(), spawnY = c.getY();
		boolean breaking = false;
		for (int x = 0; x < 2; x++) {
			if (breaking) {
				break;
			}
			for (int y = 0; y < 2; y++) {
				if (x == 0 && y == 0) {
					continue;
				}
				if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(), c.getHeightLevel(),
						x, y)) {
					spawnX = c.getX() + x;
					spawnY = c.getY() + y;
					breaking = true;
					break;
				}
			}
		}
		for (int[] Guard : NPCs) {
			if (c.combatLevel >= Guard[0] && c.combatLevel <= Guard[1]) {
				NPC n = NPCHandler.spawnNpc(c, Guard[2],
						spawnX/* c.getX() + Misc.random(1) */,
						spawnY/* c.getY() + Misc.random(1) */,
						c.heightLevel, 0, true, false);
				n.attr().put("thiev_guard", true);
				n.forceChat("Halt, Thief!");
				n.setAliveTick(200);
				
				c.sendMessage("You were caught thieving from the stall!");
				break;
			}
		}
	}

	public static void Prestige(Client c) {
		if (c.insideDungeoneering()) {
			c.sendMessage("<col=ff0000>You have received the maximum total level, please talk to king roald.");
			return;
		}

		int spawnX = c.getX(), spawnY = c.getY();
		boolean breaking = false;
		for (int x = 0; x < 2; x++) {
			if (breaking) {
				break;
			}
			for (int y = 0; y < 2; y++) {
				if (x == 0 && y == 0) {
					continue;
				}
				if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(), c.getHeightLevel(),
						x, y)) {
					spawnX = c.getX() + x;
					spawnY = c.getY() + y;
					breaking = true;
					break;
				}
			}
		}
		if (!c.hasEvent) {
			NPC n = NPCHandler.spawnNpc(c, PrestigeNpc, spawnX, spawnY, c.heightLevel,
					0, false, false);

			n.forceChat("Congratulation " + c.getNameSmartUp() + ", I would like to talk to you.");
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (c.disconnected) {
						container.stop();
						return;
					}
					container.stop();
				}

				@Override
				public void stop() {
					n.setDead(true);
					c.hasEvent = false;
				}
			}, 60);
			
			c.sendMessage(
					"<col=ff0000>You have received the maximum total level, please talk to King Roald.");
			c.hasEvent = true;
		}
	}
}
