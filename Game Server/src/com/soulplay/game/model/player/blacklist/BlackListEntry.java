package com.soulplay.game.model.player.blacklist;

public class BlackListEntry {

	private final String name;
	
	private final String UUID;
	
	public BlackListEntry(String name, String uuid) {
		this.name = name;
		this.UUID = uuid;
	}

	public String getName() {
		return name;
	}

	public String getUUID() {
		return UUID;
	}
}
