package com.soulplay.game.model.player.blacklist;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class Blacklist {

	public static void open(Client c) {
		c.getPacketSender().sendBlacklistData();
		c.getPacketSender().showInterface(67000);
	}

	public static boolean handleButton(Client c, int button, int componentId) {
		switch (button) {
			case 260453: //remove
				final int index = (componentId - 1) / 2;
				c.getDialogueBuilder().reset();
				c.getDialogueBuilder().sendStatement("Are you sure you would like to remove them from blacklist?",
						"You will have 5 minute cooldown to add", "any more names to the blacklist.")
				.sendOption("Yes, remove", ()-> {
					if (index < 0 || index >= c.getWildyBlackList().size()) {
						return;
					}

					BlackListEntry removed = c.getWildyBlackList().remove(index);
					c.sendMessage("You have removed '" + removed.getName() + "' from your list.");
					c.getPacketSender().sendBlacklistData();
					c.sendMessage("You have 5 minute timer before you can add anyone to blacklist.");
					c.blacklistTimer.startTimer(500);
				}, "No", ()-> {
					
				})
				.execute();
				return true;
			case 260447: //add
				if (!c.blacklistTimer.complete()) {
					c.sendMessage("You cannot add anyone to blacklist yet.");
					return true;
				}
				if (c.wildLevel > 0) {
					c.sendMessage("Can't add people to your list inside the wilderness.");
					return true;
				}

				if (c.getWildyBlackList().size() >= 5) {
					c.sendMessage("Your list is full.");
					return true;
				}

				c.getDialogueBuilder().sendEnterText(() -> {
					final String name = c.getDialogueBuilder().getEnterText();
					for (int i = 0, l = c.getWildyBlackList().size(); i < l; i++) {
						BlackListEntry entry = c.getWildyBlackList().get(i);
						if (name.equalsIgnoreCase(entry.getName())) {
							c.sendMessage("You already have this player blacklisted.");
							return;
						}
					}

					if (c.getWildyBlackList().size() >= 5) {
						c.sendMessage("Your list is full.");
						return;
					}

					createNewEntry(c, name);
					open(c);
				}).execute();
				return true;
		}
		return false;
	}
	
	private static void createNewEntry(Player p, String name) {
		String uuid = "";
		
		Player other = PlayerHandler.getPlayerSmart(name);
		
		if (other != null && other.isActive) {
			uuid = other.UUID;
		}
		
		BlackListEntry entry = new BlackListEntry(name, uuid);
		

		p.addToWildyBlacklist(entry);
		
	}
	
}
