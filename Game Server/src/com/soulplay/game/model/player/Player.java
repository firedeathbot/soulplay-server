package com.soulplay.game.model.player;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.JudgementHandler;
import com.soulplay.content.bot.BotClient;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.combat.data.FightExp;
import com.soulplay.content.dialogue.DialogueHandler;
import com.soulplay.content.instances.InstanceType;
import com.soulplay.content.items.CompCape;
import com.soulplay.content.items.Lamps.LampType;
import com.soulplay.content.items.Potions;
import com.soulplay.content.items.RangeWeapon;
import com.soulplay.content.items.impl.DegradableItem.Degradable;
import com.soulplay.content.items.itemdata.CombatTab;
import com.soulplay.content.items.itemdata.CombatTabButton;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.items.itemretrieval.ItemRetrievalInstance;
import com.soulplay.content.items.pos.BuyOffers;
import com.soulplay.content.items.presets.Preset;
import com.soulplay.content.items.search.MarketItem;
import com.soulplay.content.items.urn.SkillingUrn;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.ddmtournament.Room;
import com.soulplay.content.minigames.ddmtournament.Settings;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.minigames.dice.GambleLogger;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.minigames.gamble.GambleGame;
import com.soulplay.content.minigames.gamble.GambleStage;
import com.soulplay.content.minigames.gamble.flowers.FlowerPlayer;
import com.soulplay.content.minigames.gamble.flowers.FlowerRule;
import com.soulplay.content.minigames.inferno.InfernoManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.minigames.treasuretrails.TreasureTrailManager;
import com.soulplay.content.minigames.treasuretrails.TreasureTrails;
import com.soulplay.content.minigames.warriorsguild.WarriorsGuild.ArmorData;
import com.soulplay.content.npcs.impl.bosses.BossKCEnum;
import com.soulplay.content.npcs.impl.bosses.BossKCHandler;
import com.soulplay.content.npcs.impl.bosses.zulrah.Zulrah;
import com.soulplay.content.player.BrawlingGloves;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.achievement.Achievement;
import com.soulplay.content.player.combat.CombatAssistant;
import com.soulplay.content.player.combat.DamageMap;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.magic.GodSpell;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.dailytasks.PlayerCategory;
import com.soulplay.content.player.dailytasks.PlayerTask;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.misc.StaffTimer;
import com.soulplay.content.player.pets.pokemon.Pokemon;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.seasonpass.SeasonPass;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.Agility;
import com.soulplay.content.player.skills.construction.Poh;
import com.soulplay.content.player.skills.cooking.MillVars;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.player.skills.dungeoneeringv2.Binds;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.items.shop.DungeonRewardData;
import com.soulplay.content.player.skills.farming.patch.Patch;
import com.soulplay.content.player.skills.slayer.DuoPoint;
import com.soulplay.content.player.skills.slayer.PartyRequest;
import com.soulplay.content.player.skills.slayer.SlayerPoint;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.summoning.Summoning;
import com.soulplay.content.player.skills.summoning.SummoningAttacking;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.shops.ShopAssistant;
import com.soulplay.content.shops.ShopType;
import com.soulplay.content.tob.TobManager;
import com.soulplay.content.vote.VoteInterfaceStoreData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.DisplayTimerType;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.item.ComparableItem;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.item.definition.MorphingRing;
import com.soulplay.game.model.item.definition.TeleTab;
import com.soulplay.game.model.item.ItemAssistant;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.blacklist.BlackListEntry;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.config.GraphicCollection;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.dialog.DialogueBuilder;
import com.soulplay.game.model.player.dialog.impl.EnterAmountDialogue;
import com.soulplay.game.model.player.dialog.impl.EnterTextDialogue;
import com.soulplay.game.model.player.dialog.impl.OptionDialogue;
import com.soulplay.game.model.player.info.ChatSettings;
import com.soulplay.game.model.player.info.ExtraHiscores;
import com.soulplay.game.model.player.info.SeasonalWildScore;
import com.soulplay.game.model.player.info.Timers;
import com.soulplay.game.model.player.itemcharge.RecoilRings;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.player.save.HighscoresSaveNew;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.sounds.Music;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.map.DynamicObjectManager;
import com.soulplay.game.world.map.travel.ZoneManager;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.game.world.pvpworld.PvpFormula;
import com.soulplay.net.packet.out.PacketSender;
import com.soulplay.plugin.content.misc.Point;
import com.soulplay.util.*;
import com.soulplay.util.soulplaydate.PlayerDates;
import com.soulplay.util.soulplaydate.SoulPlayDate;

import plugin.item.DihnBulwarkPlugin;
import plugin.itemactions.one.TeleTabPlugin;

public abstract class Player extends Mob {

	public PlayerClone clone;
	private SeasonalData seasonalData = new SeasonalData();
	public MorphingRing morphingRing;
	public ItemRetrievalInstance itemRetrieval;
	public ArmorData animatedArmorSpawned;
	public int selectedLampSkill = -1;
	public LampType selectedLampType;
	public FlowerRule gambleNextFlowerRule;
	public TickTimer dragonBoneNecklaceTimer = new TickTimer();
	public TickTimer prayerBuryTimer = new TickTimer();
	public TickTimer gambleBlockTimer = new TickTimer();
	public Inventory gambleLastStake = new Inventory(28);
	public boolean gambleUpdateData = true;
	public GambleGame gambleGame = GambleGame.FLOWER_POKER;
	public FlowerPlayer gambleFlowerPlayer;
	public boolean gambleAccepted;
	public Inventory gambleStake = new Inventory(28);
	public Player gambleOpponent;
	public GambleStage gambleStage = GambleStage.NONE;
	public int gambleOfferTo = -1;

	public Preset selectedSharedPreset;
	public List<Preset> sharedPresets = new ArrayList<>();
	public Preset selectedPreset;
	public List<Preset> presets = new ArrayList<>();
	public Degradable degradedItem;
	private DialogueHandler dialogueHandler = new DialogueHandler(this);
	public InstanceType selectedInstanceType;
	public int donationInterfaceSelectedEntry = -1;
	public int donationInterfaceSelectedPage;
	public int donationInterfaceSelectedCategory;
	public VoteInterfaceStoreData selectedVoteReward;
	public int skillingTick = 0;
	
	public long lastWhatDropCommand = 0;

	public boolean extendedNpcRendering = false;

	private Summoning summoning = new Summoning(this);

	public Summoning getSummoning() {
		return summoning;
	}

	private SummoningAttacking sa = new SummoningAttacking(this);

	public SummoningAttacking getSA() {
		return sa;
	}
	
	private Agility agility = new Agility();
	
	public Agility getAgility() {
		return agility;
	}
	
	private HighscoresSaveNew hiscoreNew = new HighscoresSaveNew(this);
	
	public HighscoresSaveNew getHiscores() {
		return hiscoreNew;
	}

	public PokemonData selectedPokemon;
	public Pokemon summonedPokemon = null;
	public boolean pokemonTaunt;
	public boolean selectPokemonAttack = false;

	private final TreasureTrailManager treasureTrailManager = new TreasureTrailManager(this);

	private final GambleLogger gambleLogger = new GambleLogger(this);
	
	public GambleLogger getGambleLogger() {
		return this.gambleLogger;
	}
	
	public short[] urnData = new short[SkillingUrn.SIZE];

	public int getTournamentInstanceId() {
		return Variables.TOURNAMENT_INSTANCE_ID.getValue(this);
	}
	
	public void setTournamentInstanceId(int id) {
		variables[Variables.TOURNAMENT_INSTANCE_ID.getCode()] = id;
	}
	
	public boolean debugPID;
	
	public boolean hasSpawnedGear;
	
	public TickTimer specAndTeleTimer = new TickTimer();
	
	public TickTimer blacklistTimer = new TickTimer();
	
	public StaffTimer staffTimer = new StaffTimer();
	
	public TickTimer bulwarkDelay = new TickTimer();
	
	public TickTimer speedScrollTimer = new TickTimer();
	public TickTimer flashScrollTimer = new TickTimer();
	
	public int dialogueTick;

	public Tournament tournamentInstance;
	
	public Settings tournSettings;
	
	public Room tournamentRoom;
	
	private boolean redSkull;
	
	public void setRedSkull(boolean skulled) {
		this.redSkull = this.isSkulled = skulled;
		if (!redSkull) {
			headIconPk = -1;
		} else {
			headIconPk = 1;
		}
		updateFlags.add(UpdateFlag.APPEARANCE);
	}
	
	public boolean isRedSkulled() {
		return this.redSkull;
	}
	
	public CycleEventContainer deathContainer = null;

	public CycleEventContainer staffOpOut = null;
	
	public void resetDeathEvent() {
		setDead(false);
		applyDeath = false;
		if (deathContainer == null) {
			return;
		}
		startAnimation(RESET_ANIM);
		deathContainer.stop();
		deathContainer = null;
	}
	
	private TickTimer emoteTimer = new TickTimer();
	
	public TickTimer getEmoteTimer() {
		return emoteTimer;
	}
	
	private TickTimer damageImunity = new TickTimer();
	
	public boolean isImmuneToDamage() {
		return !damageImunity.complete();
	}
	
	public void startDamageImmunity() {
		damageImunity.startTimer(10);
	}
	
	private final PlayerZones zones = new PlayerZones();
	
	public PlayerZones getZones() {
		return zones;
	}
	
	private GodSpell godSpell = new GodSpell(this);

	public GodSpell getGodSpell() {
		return godSpell;
	}
	
	private RecoilRings recoilRings = new RecoilRings(this);
	
	public RecoilRings getRecoilRings() {
		return recoilRings;
	}
	
	private SeasonalWildScore seasonWildScore = new SeasonalWildScore();
	
	public SeasonalWildScore getSeasonWildScore() {
		return seasonWildScore;
	}
	
	public void resetSeasonWildScore() {
		this.seasonWildScore = new SeasonalWildScore();
	}
	
	public int arrowX, arrowY, arrowZ, arrowP;
	
	private List<BlackListEntry> wildyBlacklist = new ArrayList<>(5);

	public List<BlackListEntry> getWildyBlackList() {
		return wildyBlacklist;
	}

	public void addToWildyBlacklist(BlackListEntry entry) {
		this.wildyBlacklist.add(entry);
	}

	private final Map<Integer, BuyOffers> ownBuyOffers = new HashMap<Integer, BuyOffers>();

	public Map<Integer, BuyOffers> getOwnBuyOffers() {
		return ownBuyOffers;
	}
	
	private Timestamp joinedDate = Timestamp.from(Instant.now());
	
	public Timestamp getJoinedDate() {
		// return Timestamp.valueOf("2015-01-05 05:30:18");
		return joinedDate;
	}
	
	public void setJoinedDate(Timestamp date) {
		this.joinedDate = date;
	}
	
	private PlayerDates soulplayDate = new PlayerDates();
	
	public PlayerDates getSoulplayDate() {
		return this.soulplayDate;
	}
	
	public int gamblerGP = 1;
	
	public int dailyTasksCompleted;
	public int dailyTasksPoints;
	public PlayerCategory[] categories;
	public PlayerTask bonusTask;

	public void increaseProgress(TaskType taskType, int trigger) {
		increaseProgress(taskType, trigger, 1);
	}
	
	public void increaseProgress(TaskType taskType, int trigger, int count) {
		if (isBot() || categories == null) {
			return;
		}

		if (dailyTasksLocked()) {
			return;
		}

		for (int i = 0, length = categories.length; i < length; i++) {
			this.categories[i].increaseProgress(this, taskType, trigger, count);
		}
	}

	public boolean dailyTasksLocked() {
		return difficulty == PlayerDifficulty.NORMAL && getPA().getTotalLevel() < 750;
	}
	
	public int processTick;
	
	public boolean hasPIDover(int otherId) {
		return this.processTick > otherId;
	}
	
	public boolean finishedProcess = false;
	
	private GameObject interactionObject = null;
	
	public void setClickedObject(GameObject o) {
		this.interactionObject = o;
	}
	
	public GameObject getClickedObject() {
		return this.interactionObject;
	}
	
	public boolean isLoggedIn() {
		return isActive && !disconnected;
	}
	
	public byte[] pollData;
	
	private final Poh poh = new Poh(this);

	public Poh getPOH() {
		return poh;
	}

	private DynamicRegionClip pohClip = new DynamicRegionClip();
	
	public DynamicRegionClip getPohClipMap() {
		return pohClip;
	}
	
	public void setPohClipMap(DynamicRegionClip map) {
		this.pohClip = map;
	}

	private Binds binds = new Binds(this);
	
	public Binds getBinds() {
		return binds;
	}
	
	private CursesPrayer curses = new CursesPrayer(this);

	public CursesPrayer curses() {
		return curses;
	}
	
	private BarbarianAssault barbarianAssault = new BarbarianAssault(this);
	
	public BarbarianAssault getBarbarianAssault() {
		return barbarianAssault;
	}
	
	public Achievement achievement = new Achievement(this);
	
	public Achievement getAchievement() {
		return achievement;
	}
	
	private MillVars millVars = null;
	
	public MillVars getMillInfo() {
		return this.millVars;
	}
	
	public void createMillInfo() {
		this.millVars = new MillVars();
	}

	private CompCape compCape = null;

	protected PlayerController controller = new DefaultPlayerController(this);
    
	protected final Movement movement = new Movement(this);
	
	private DialogueBuilder dialogueBuilder = new DialogueBuilder(this);
	
	private Optional<OptionDialogue> optionDialogue = Optional.empty();
	
	private Optional<EnterAmountDialogue> enterAmountDialogue = Optional.empty();
	
	private Optional<EnterTextDialogue> enterTextDialogue = Optional.empty();
	
	private ExtraHiscores extraHiscores = new ExtraHiscores(this);

	private Potions potions = new Potions(this);
	
	private final BrawlingGloves brawlers = new BrawlingGloves(this);
	
	public BrawlingGloves getBrawlers() {
		return brawlers;
	}

    public Pokemon[] pokemon = new Pokemon[PokemonData.POKEMON_COUNT];
    public int[] pokemonUnlocked = new int[10];//10 * 31, so 310 npcs should be enough
	
	public boolean canWalk = true;

    public boolean isPokemonUnlocked(PokemonData data) {
		return isPokemonUnlocked(data.configValue - 1);
    }

    public boolean isPokemonUnlocked(int id) {
    	int arrayIndex = id / 31;
		int value = pokemonUnlocked[arrayIndex];
		int bitId = id % 31;
		return (value & 1 << bitId) != 0;
    }

	public void unlockPokemon(PokemonData data) {
		unlockPokemon(data.configValue - 1);
	}

	public void unlockPokemon(int id) {
		int arrayIndex = id / 31;
		int bitId = id % 31;
		pokemonUnlocked[arrayIndex] |= 1 << bitId;
	}

	public boolean forceClosedClient = false;

	public volatile boolean reconnectPlayer = false;

	public int makeCount;
	
	public int searchIndex = 0;

	public volatile int reconnectAttempts = 0;

	public List<Runnable> makeActions = new ArrayList<>(5);

	public boolean hasFocusOnClient = true;
	public boolean flagAsBotter;

	private BotClient playerOwnedBot = null;

	private BotClient playerOwnedShopBot = null;

	public List<MarketItem> marketSearchResults = new ArrayList<>();

	public int lastMarketSearchIndex = -1;

	public boolean debug, debugNpc;

	public Runnable itemDestroyCode;
	public Runnable itemDestroyPostClick;
	
	public long enterAmount;

	public NPC testNpc = null;

	/**
	 * Player owned shop shit
	 */
	public int playerShopSlots = 10;

	public final static int MAX_PSHOP_SLOTS = 40;

	public long playerCollect;

	public int sellingId, sellingIdOriginal; // the original shit is the item
												// that is being sold from
												// inventory, noted or unnoted
												// version, real id.

	public int sellingN;

	public int sellingS;

	public int playerShopItems[] = new int[MAX_PSHOP_SLOTS];

	public int playerShopItemsN[] = new int[MAX_PSHOP_SLOTS];

	public long playerShopItemsPrice[] = new long[MAX_PSHOP_SLOTS];

	public Client myShopClient;

	private boolean detectNull = false;

	public int drawDistance = 0;
	private Client duelingClient;

	private int duelRuleTimer = 0;

	public int reflectionCheckErrorId;

	// Untradeable shop items
	private Map<Integer, Integer> untradeableShopMap = new ConcurrentHashMap<>();

	private Map<Long, Integer> friendsUpdate = new HashMap<>();

	public String taskName = " ";

	public String masterName = " ";

	// public boolean duelPit = false;

	public int assignmentAmount;

	private Client partner;

	private SlayerTask slayerTask;

	private Point slayerPoints = new SlayerPoint();

	private Point duoPoints = new DuoPoint();

	private int slayerTasksCompleted;

	private PartyRequest pendingRequest;

	public int[] bossKills = new int[BossKCEnum.values.length];

	private DamageMap damageMap = new DamageMap(this);

	public String lastDayLoggedIn = "";

	public int votedInPoll = 0;
	public boolean votePollLoading = false;

	public int lastDayPkedOfYear;
	
	public int dominionTokens = 0;

	public int penguinPoints;

	public int clawTick = 0;
	
	public int BountyTargetLimit = 0;
	
	public boolean claimedReward = true;

	public TempVar tempVar = new TempVar();

	public String demoting = "";

	public Region currentRegion;

	public Region cannonRegion;

	public boolean inInstance = false;

	public int lastX, lastY;

	public int tpaReqId = 0;

	public String tpRequested = "";

	public boolean isIdle = false;

	public long timePlayed;

	public long getTimePlayed() {
		return timePlayed;
	}

	public long timePlayedMillis() {
		return timePlayed * (Server.CYCLE_RATE * 2);
	}

	public long getSecondsPlayed() {
		return timePlayedMillis() / 1_000L;
	}

	public long getMinutesPlayed() {
		return getSecondsPlayed() / 60L;
	}

	public long getHoursPlayed() {
		return getMinutesPlayed() / 60L;
	}

	public long getDaysPlayed() {
		return getHoursPlayed() / 24L;
	}

	public boolean cantCommunicate() {
		return getSecondsPlayed() < 20;
	}

	public int onlineTick = 0;
	
	public boolean inHotZone = false;

	public boolean hidePlayer = false, superHide = false;

	public boolean spectatorMode = false;

	private int donatedTotalAmount;
	private int donatedRealTotalAmount;

	public boolean runningTask;

	private int eloRating = EloRatingSystem.DEFAULT_ELO_START_RATING;

	public ArrayList<String> killedPlayers = new ArrayList<>();

	public ArrayList<Integer> attackedPlayers = new ArrayList<>();

	public ArrayList<String> lastKilledPlayers = new ArrayList<>();

	public ArrayList<String> lastConnectedFrom = new ArrayList<>();
	
	public Set<String> lastConnectedFromUUID = new HashSet<>();
	
	public void loadUUIDHistoryFromJson(final String load) {
		if (load == null)
			return;
		lastConnectedFromUUID = GsonSave.gsond.fromJson(load, new TypeToken<Set<String>>() { }.getType());
	}

	public Queue<Item> foodToEatOnMainTick = new LinkedList<>();

	public int objectHintX, objectHintY, objectHintPos = -1;

	public int playerHintId = -1;

	public boolean inBossRoom;

	public long gwdPrayerDelay;

	public int dfsCharge = 0;
	
	public Map<Integer, Integer> chargeItems = new HashMap<>();
	
	public int resetCharge(int itemId) {
		int amount = chargeItems.getOrDefault(itemId, 0);
		if (amount > 0)
			chargeItems.put(itemId, 0);
		return amount;
	}
	
	public int addCharge(int itemId, int toAdd, int max) {
		int leftOver = 0;
		int amount = chargeItems.getOrDefault(itemId, 0);
		amount = Misc.addOrMaxInt(amount, toAdd);
		if (amount > max) {
			leftOver = amount - max;
			amount = max;
		}
		chargeItems.put(itemId, amount);
		return leftOver;
	}
	
	public int reduceCharge(int itemId, int toRemove) {
		int newAmount = chargeItems.getOrDefault(itemId, 0);
		newAmount = Math.max(0, newAmount - toRemove);
		chargeItems.put(itemId, newAmount);
		return newAmount;
	}
	
	public int getCharges(int itemId) {
		if (Tournament.insideArenaWithSpawnedGear(this))
			return 100;
		return chargeItems.getOrDefault(itemId, 0);
	}

	public void setDfsCharge(int dfsCharge) {
		this.dfsCharge = dfsCharge;
		getItems().refreshBonuses();
	}

	public void deductDfsCharge() {
		this.dfsCharge--;
		getItems().refreshBonuses();
	}

	public void addDfsCharge() {
		this.dfsCharge++;
		getItems().refreshBonuses();
	}

	public boolean dfsPause = false;

	public boolean toggledDfs;

	public double attackMultiplier, rangedMultiplier, defenceMultiplier, leechMagicDelay, magicMultiplier,
			strengthMultiplier;

	public boolean immuneToPK = false;

	public boolean activateLeechSap = false;

	/**
	 * Clan Chat Variables
	 */
	private Clan clan;

	public String lastClanChat = "";

	/**
	 * herblore stuff
	 */
	// Vavbro - Added these two, since the new herblore uses them.
	public int herbloreItem1 = -1;

	public int herbloreItem2 = -1;

	public int doAnim, oldItem, oldItem2, gainXp, gainXp2, levelReq, levelReq2, newItem, newItem2, objectType, chance,
			skillSpeed;

	public int herbAmount, doingHerb, newHerb, newXp;

	/**
	 * Construction Stuff
	 */
	public int servantCharges, servantItemFetchId;

	public int houseServant;

	public int jesterEmotes, lastFairyDistance;

	public int combatRingType;

	public boolean resetChairAnim;

	public boolean playerIsInHouse;

	public int[] toConsCoords;

	public int buildFurnitureId, buildFurnitureX, buildFurnitureY;

	public int portalSelected;

	public Location[] dialogTeleports = new Location[5];

	public int[] dialogShopIds = new int[5];

	public int[] dialogOptionIds = new int[5];

	public boolean trollSpawned = false, zombieSpawned = false, golemSpawned = false, treeSpawned = false;

	public int currentShadow = 0;

	/**
	 * Allantois summoning additions
	 */
	public boolean summonedCanTeleport = false;

	public NPC summoned;
	
	public int familiarPouchId = 0;

	public int geyserTitanDelay = 0;

	public int geyserTitanTarget = 0;

	public boolean doneSteelTitanDelay = false, usingSummoningSpecial = false;

	public int steelTitanDelay = 0;

	public int steelTitanTarget = 0;

	public int summoningMonsterId = -1;

	public int interfaceIdOpenMainScreen = 0;

	public int sidebarInterfaceOpen = 0;
	
	private int chatInterfaceOpen = 0;
	
	public void setChatInterfaceOpen(int id) {
		this.chatInterfaceOpen = id;
	}
	
	/**
	 * Gets the ID of the open chat interface.
	 */
	public int getChatInterfaceId() {
		return chatInterfaceOpen;
	}

	public int familiarID;

	public int familiarIndex;

	public int summoningSpecialPoints;

	public int timeBetweenSpecials;

	public int specHitTimer;

	public boolean hasActivated;

	public double[] soakingBonus = new double[3];

	public int soakDamage;

	public int soakDamage2;

	public int damageType = 0;

	public String familiarName;

	public int familiarTimer;

	private int pouchData[] = { 0, 0, 0, 0 };

	public long restoreDelay;

	public boolean hasOverloadBoost;

	public boolean displayMaxHit = false;

	public boolean displayAccuracyInfo = false;

	/**
	 * Title Variables
	 */
	public String playerTitle = "";

	public int titleColor = 0;

	/**
	 * Event Variables
	 */
	public boolean hasEvent;

	public int tourStep = 0;

	public int walkableInterface = 0;

	public boolean halloweenEvent = false;
	
	public boolean claimedEventReward = false;

	public boolean safeDeath = false;

	public boolean expLock = false;
	
	public boolean superior = false;

	public double crossbowDamage;

	public int cbowSpecType;

	public int yellPoints = 0;

	// private int doubleExpLength = 0;
	public int walkToX, walkToY;

	public int bankPinWarning = 36;

	public int oI, oX, oY, oH, oF, oT; // object spawn temp variables

	public ArrayList<RSObject> objectToRemove = new ArrayList<>();

	public ArrayList<RSObject> objectToRemoveTemp = new ArrayList<>();

	public ArrayList<GroundItem> itemsToRemove = new ArrayList<>();
	public ArrayList<GroundItem> itemsToRemoveTemp = new ArrayList<>();

	public boolean performingAction;

	// Dwarf Multicannon declarations
	public int cannonScanX, cannonScanY, cannonX, cannonY, cannonBalls;

	// public boolean removeNecklace = false;
	// public boolean removeRing = false;
	public boolean cannonDecayed;

	public boolean cannonSetUp;

	public int cannonStatus = 0; // 0 = not set up, 1 = base, 2 = stand, 3 =
									// barrel, 4 = furnace

	public int cannonRangeNpcs[][] = new int[28][28];

	public int safeTimer = 0;

	private int transformNpcId = -1;

	public int uniqueActionId;

	public int pkp, KC, DC;
	
	public int guardStealCount = 0;

	public boolean[] clanWarRule = new boolean[11];

	public SmithingInterfaceData openSmithingData;

	public int[][] playerSkillProp = new int[25][15];

	public boolean[] playerSkilling = new boolean[25];

	public CycleEventContainer teleGrabEvent;
	public CycleEventContainer skillingEvent;
	public long lmsUniqueId;
	
	public CycleEventContainer overload1;
	public CycleEventContainer overload2;

	public void startAction(CycleEvent action, int cycles) {
		startAction(action, cycles, false);
	}

	public void startActionChecked(CycleEvent action, int cycles) {
		if (this.skillingEvent != null) {
			return;
		}

		startAction(action, cycles);
	}

	public void startActionChecked(CycleEvent action, int cycles, boolean instantStart) {
		if (this.skillingEvent != null) {
			return;
		}

		startAction(action, cycles, instantStart);
	}

	public void startAction(CycleEvent action, int cycles, boolean instantStart) {
		this.skillingEvent = CycleEventHandler.getSingleton().addEvent(this, action, cycles);
		if (instantStart)
			this.skillingEvent.execute();
	}

	public void resetSkillingEvent() {
		if (skillingEvent == null) {
			return;
		}
		resetAnimations();
		skillingEvent.stop();
		skillingEvent = null;
	}

	public void resetTeleGrabEvent() {
		if (teleGrabEvent == null) {
			return;
		}

		teleGrabEvent.stop();
		teleGrabEvent = null;
	}

	public boolean setPin = false;

	public String bankPin = "";

	public boolean teleporting;

	private boolean muted = false;

	public long lastReport = 0;

	public int level1 = 0, level2 = 0, level3 = 0;

	public String lastReported = "";

	public long lastButton;

	public int leatherType = -1;

	public boolean isWc;

	public int homeTele = 0;

	public int homeTeleDelay = 0;

	public int[] quests = new int[150];

	public int[] achievements = new int[150];

	public int[] titles = new int[100];

	public boolean wcing;
	public int harvestCount;

	public int treeX, treeY;

	public TickTimer miscTimer = new TickTimer();

	public long lastCast;
	
	private RangeWeapon rangeWeapon;

	public boolean openBankPinInterface, hasBankPin, enterdBankpin, firstPinEnter, requestPinDelete, secondPinEnter,
			thirdPinEnter, fourthPinEnter, bankPinOpensBank;

	public int lastLoginDate, playerBankPin, recoveryDelay = 3, attemptsRemaining = 3, lastPinSettings = -1,
			setPinDate = -1, changePinDate = -1, deletePinDate = -1, firstPin, secondPin, thirdPin,
			fourthPin, /* bankPin1, bankPin2, bankPin3, bankPin4, */
			pinDeleteDateRequested;

	private int bankPins[] = new int[4];

	private AtomicBoolean isActiveAtomicBoolean = new AtomicBoolean(false);

	public volatile boolean properLogout = false, loginPacketsSent = false;

	public boolean isBanking = false, isCooking = false, disconnected = false, forceDisconnect = false,
			ruleAgreeButton = false, initialized = false, isSkulled = false,
			friendUpdate = false, newPlayer = false, hasMultiSign = false, saveCharacter = false, mouseButton = false,
			splitChat = false, chatEffects = true, acceptAid = false, nextDialogue = false,
			usedSpecial = false, vengOn = false, addStarter = false, accountFlagged = false, setMode = true, isBobOpen = false;

	public int lastBankTabOpen = 0;

	public boolean usingSpellFromSpellBook = false;

	public boolean refreshBank = true;

	public int lastChatId = 1,
			/* privateChat, */ dialogueId, specBarId, followId,
			/* skullTimer, */ /* votingPoints, */ nextChat = 0, talkingNpc = -1, dialogueAction = 0,
			followDistance, followId2, barrageCount = 0, delayedDamage = 0, delayedDamage2 = 0, npcKills = 0,
			magePoints = 0, clanId = -1, autoRet = 0, pcDamage = 0, xInterfaceId = 0, xRemoveId = 0,
			xRemoveSlot = 0, tzhaarToKill = 0, tzhaarKilled = 0, waveId, frozenBy = 0, teleAction = 0,
			newPlayerAct = 0, bonusAttack = 0, lastNpcAttacked = 0, actionTimer,
			achievementsCompleted = 0, prayerRenew = 0;
	
	private CombatTab combatTab /*= CombatTab.UNARMED*/;
	
	private CombatTabButton combatTabButtonToggle;
	
	private Animation weaponAnimation = new Animation(390);
	
	public Animation getWeaponAnimation() {
		return weaponAnimation;
	}
	
	public void setWeaponAnimation(Animation weaponAnimation) {
		this.weaponAnimation = weaponAnimation;
	}

	public int[] rolePoints = new int[4];

	public int[] roleExp = new int[4];

	private int lastClicked;

	public long lastEffigy;

	public long lastLongKilled, lastLongKilledBy;

	public String killedByPlayerName = "n";

	private boolean toggleRag = false;

	public int capeClicked = 0;

	public boolean queueGmaulSpec = false;
	
	private int timers[] = new int[50]; // TODO:: this should probably be
										// renamed to variables, stored
										// variables like timers, etc

	private int variables[] = new int[100];

	private int judgement[] = new int[50];

	private int chatSettings[] = new int[4];

	private int unlockedCrowns[] = new int[50];

	private int seasonalElo[] = new int[100];
	
	private int loyaltyPoints = 0, donPoints = 0, pcPoints = 0, agilityPoints = 0;

	private long killCount = 0;

	public boolean inKilnFightCave = false;

	public int minigameTarget = 0;

	public int fairyCombo = 0;

	public int javaVersion;

	private int zeals = 0;

	long longName;
	long longDisplayName;
	
	public int wildyKeys = 0;

	public int[] voidStatus = new int[5];

	public int[] itemKeptId = new int[4];

	public int[] pouches = new int[4];

	public final int[] POUCH_SIZE = { 3, 6, 9, 12 };

	public boolean[] invSlot = new boolean[28], equipSlot = new boolean[14];

	public long friends[] = new long[200];

	public long ignores[] = new long[100];

	public double specAmount = 0;

	public int prayerDrainCounter;

	public boolean storing = false;

	public int attackingIndex = -1;
	
	public int getAttackingIndex() {
		return attackingIndex;
	}

	public void setAttackingIndex(int attackingIndex) {
		this.attackingIndex = attackingIndex;
	}

	public TickTimer pokemonDelayTimer = new TickTimer();
	
	public TickTimer interactionResetTimer = new TickTimer();
	
	public void startInteractionResetTimer() {
		interactionResetTimer.startTimer(8);
	}
	
	public boolean isInteractionResetTimerDone() {
		return interactionResetTimer.complete();
	}

	public int teleGrabItem, teleGrabX, teleGrabY, duelCount, underAttackBy, underAttackBy2, wildLevel, respawnTimer,
			saveTimer = 0, poisonDelay;

	public int teleTimer, endDelay;
	public TickTimer lastAction = new TickTimer();

	public long lastPlayerMove, lastSpear, lastProtItem,
			/* dfsDelay, */lastVeng, lastYell, teleGrabDelay,
			lastThieve, alchDelay, duelDelay, anyDelay2;
	
	public int lastTeleportsTab = 4;
	
	public TickTimer lockPickTimer = new TickTimer();

	private TickTimer specRestoreTick = new TickTimer();

	public void startSpecRestoreTick() {
		specRestoreTick.checkThenStart(50);
	}
	
	public boolean specRestoreTimerFinished() {
		return specRestoreTick.complete();
	}

	private int npcAggroTick = 0;

    private int foodDelayTick = 0, karamTick = 0;
	
	public boolean earnedTaiBwoFavour = false;
	
	public void setKaramTick() {
		karamTick = Server.getTotalTicks() + 2;
	}
	
	public void setFoodTick(int addTick) {
		foodDelayTick = Server.getTotalTicks() + addTick;
	}
	
	private TickTimer resetCurseBoostTimer = new TickTimer();
	
	public boolean isResetCurseBoost() {
		return resetCurseBoostTimer.complete();
	}
	
	public void startCurseBoostResetTimer() {
		resetCurseBoostTimer.startTimer(17);
	}
	
	public int lastHitByPlayerTick = 0;
	
	private TickTimer singlesCombatDelayTimer = new TickTimer();
	
	public boolean isAttacked() {
		return !singlesCombatDelayTimer.complete();
	}
	
	public void startSinglesCombatDelayTimer(int delay) {
		singlesCombatDelayTimer.startTimer(delay);
		startCurseBoostResetTimer();
	}
	
	private TickTimer reviveTick = new TickTimer();
	
	public void startReviveTick() { // i use this to not logout player till he is done being revived.
		reviveTick.startTimer(1);
	}
	
	public boolean finishedRevive() {
		return reviveTick.complete();
	}
	
	private TickTimer attackedPlayerTimer = new TickTimer();
	private int hitPlayerIndex = -1;
	
	public void startAttackedPlayerTimer(int indexOfDefender) {
		hitPlayerIndex = indexOfDefender;
		attackedPlayerTimer.startTimer(7);
	}
	
	public int getLastHitPlayerIndex() {
		if (!attackedPlayerTimer.complete()) {
			return hitPlayerIndex;
		}
		return -1;
	}
	
	private TickTimer combatLogout = new TickTimer();
	
	private TickTimer spamTick = new TickTimer();
	
	public TickTimer getSpamTick() {
		return spamTick;
	}
	
	private TickTimer moneyPouchCooldown = new TickTimer();
	
	public void startMoneyPouchCooldown() {
		moneyPouchCooldown.startTimer(100);
	}
	
	public boolean moneyPouchOnCooldown() {
		return !moneyPouchCooldown.complete();
	}
	
	private TickTimer saveTick = new TickTimer();
	
	public TickTimer getSaveTimer() {
		return saveTick;
	}
	
	private final TickTimer houseListingRefresh = new TickTimer();
	
	public TickTimer getHouseListingRefreshTick() {
		return houseListingRefresh;
	}
	
	public int getKaramTick() {
		return karamTick;
	}
	
	public int getFoodTick() {
		return foodDelayTick;
	}
	
	private int potionTick = 0;
	
	public void setPotionTick() {
		potionTick = Server.getTotalTicks() + 3;
	}
	
	public int getPotionTick() {
		return potionTick;
	}

	public boolean canChangeAppearance = false;

	public int questPoints = 0;

	public boolean applyDeath = false;

	public int respawnX, respawnY, respawnZ;
	
	public int newRespawnX, newRespawnY, newRespawnZ;
	
	public void changeRespawn(int x, int y, int z) { // temp fix to moving player during death
		if (isDead()) {
			this.newRespawnX = x;
			this.newRespawnY = y;
			this.newRespawnZ = z;
		}
	}

	public boolean randomEvent = false;

	public List<Integer> quickPrayers = new ArrayList<>();

	public List<Integer> quickCurses = new ArrayList<>();
	public int lmsPoints; //oldDungTokens

	public boolean quickPray = false;

	public int lastActive = 0;

	public RSObject interactingObject = null;

	public int finalLocalDestX, finalLocalDestY;

	public int destinationX, destinationY; // walking packet destinations, real
											// coordinates of whole map

	public boolean walkingToObject, walkingToMob;
	
	public int spellOnObject;

	public int[] keepItems = new int[4];

	public int[] keepItemsN = new int[4];

	public int WillKeepAmt1, WillKeepAmt2, WillKeepAmt3, WillKeepAmt4, WillKeepItem1, WillKeepItem2, WillKeepItem3,
			WillKeepItem4, WillKeepItem1Slot, WillKeepItem2Slot, WillKeepItem3Slot, WillKeepItem4Slot, EquipStatus;

	public boolean playerIsSkilling;

	public boolean sentPlayerHome;
	
	public boolean magicDef;

	public int teleotherType, doAmount, lastDamageDealt;

	public boolean lastHitWasSpec = false;

	public long lastSkillingAction = 0;

	/**
	 * 0-ahrim 1-dharok 2-guthan 3-karil 4-torag 5-verac 6-barrowsNpcsKillCount
	 */
	public int[] barrows = new int[7];

	public int barrowsKillCount;
	
	private TickTimer protectPrayerDisabled = new TickTimer();
	
	public TickTimer getProtectionPrayerDisabledTimer() {
		return this.protectPrayerDisabled;
	}

	public boolean usingPrayer;

	public int virusTimer = -1, virusDamage = 0;

	public boolean[] prayerActive = { false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false };

	public boolean duoPrayerEnabled = false;

	public boolean[] curseActive = new boolean[22];
	/* End of curse prayers */
	
	//Events
	public static boolean clanWarsWildyEvent = false;
	
	public static boolean edgeWildyEvent = false;

	public int duelTimer, duelTeleX, duelTeleY, duelSlot, duelSpaceReq, duelOption, duelingWith;

	public volatile int duelStatus;

	public int headIconPk = -1, headIconHints;

	public boolean duelRequested;

	public boolean[] duelRule = new boolean[22];
	
	public boolean loadPrevDuelRules;
	public boolean[] prevDuelRule = new boolean[22];

	public boolean usingSpecial, npcDroppingItems;
	public int instaSpecUUID;
	public int instaSpecActivateTick;
	
	private SpellsData autoCastingSpell;
	private SpellsData singleCastSpell;

	public int playerIndex, oldPlayerIndex, recentPlayerHitIndex,
			crystalBowArrowCount,teleHeight, teleX, teleY, killingNpcIndex,
			oldNpcIndex, fightMode, npcIndex, npcClickIndex, npcType, sendPokemonAtNpcIndex;

	public SpellBook previousMagicBook = SpellBook.NORMAL, playerMagicBook = SpellBook.NORMAL;
	public PrayerBook previousPrayerBook = PrayerBook.NORMAL, playerPrayerBook = PrayerBook.NORMAL;

	public Graphic teleEndGfx;
	public Animation teleEndAnimation;
	public boolean lastButtonWasControlledStyle;
	public boolean usingWhiteWhipSpec;

	public boolean usingSOLSpec;

	private boolean teleCooldown;

	public boolean passedCheatClientTest = false;

	private FightExp fightXp;

	/**
	 * damage types 0=melee - 1=range - 2=magic
	 */
	public int lastDamageType = 0;

	public int clickNpcType, /* clickObjectType, */ objectId, objectX, objectY, objectXOffset,
			objectYOffset, objectDistance, prevObjectId, prevObjectX, prevObjectY;
	
	public int itemOnNpcItemId, itemOnNpcItemSlot;
	
	public int clickReachDistance = 1, destOffsetX = 1, destOffsetY = 1, entityClickSizeX = 1, entityClickSizeY = 1, entityWalkingFlag = 0;
	public boolean disableInteractClip = false;
	
	// one is melee, anything above is ranged
	public int clickNpcDistance = 1;
	
	// if npc is blocked, like bankers and GE bankers, then make it find path to walk up to npc
	public boolean walkUpToPathBlockedMob = false;

	/**
	 * What type of object click action 0 = none - 1 = firstClickObject - 2 =
	 * SecondClickObject - 3 = ThirdClickObject - 4 = itemOnObject - 5 =
	 * itemOnNpc - 6 = itemOnPlayer - 7 = ChallengePlayer
	 */
	public int clickObjectType;

	public boolean clickedObject = false;

	public int pItemX, pItemY, pItemId;

	public boolean isMoving, walkingToItem;

	public boolean keepWalkFlag;

	public boolean openDuel = false;

	public boolean isShopping, updateShop;
	public ShopType shopType = ShopType.REGULAR;

	public volatile int myShopId;

	public volatile int tradeStatus;
	
	public boolean inMultiDynZone = false;
	public boolean inWildDynZone = false;

	public InfernoManager infernoManager;
	public RaidsManager raidsManager;
	public TobManager tobManager;
	public boolean tobDead;

	public int tradeWith;
	public int expireTicks;
	public Runnable onExpire;
	public int dungPartyInvite = -1;
	public Party dungParty;
	public int dungFloorsUnlocked = 1;
	private int[] dungFloorsCompleted = new int[2];//bit per floor
	public int selectedFloor = 1;
	private int complexityInfo = 32;//selected 1 complexity
	public int dungVoteStage = 0;
	public int previousProgress;
	public Location gatestoneLocation;
	public int dungDeaths = 0;
	public int dungPrayerRecharge;
	public long dungInstanceId;
	public int dungTokens;
	public DungeonRewardData selectedDungReward;
	public int dungBloodNeckTicks = 25;
	public CycleEventContainer dungSshDisable;
	public int dungPartyIndex;
	public boolean dungTeleported = false;

	public void deactivateShh() {
		if (dungSshDisable != null) {
			return;
		}

		sendMessage("Your concealment effect has been temporarily disabled.");
		dungSshDisable = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}

			@Override
			public void stop() {
				dungSshDisable = null;
				sendMessage("The spell has faded and you are once again concealed under your shadow-silk hood.");
			}

		}, 50);
	}

	public DynamicObjectManager dynObjectManager = null; // may need to move to MOB class in future if npcs require objects too
	public int lmsKillsPerGame;
	public LmsManager lmsManager = null;
	public int lmsFogTicks;
	public int lmsRoundPlacement;
	public List<Item> lmsRewardsToClaim = new ArrayList<>();
	public DamageMap lmsDamageMap = new DamageMap(this);
	public int lmsPointsPerRound = 0;

	public boolean isInLmsArena() {
		return lmsManager != null;
	}

	public void setSpellbook(SpellBook playerMagicBook) {
		this.playerMagicBook = playerMagicBook;
		updateSpellbook();
	}

	public void updateSpellbook() {
		getPacketSender().setSidebarInterface(6, playerMagicBook.getInterfaceId());
		setUpdateInvInterface(3214);
	}

	public void updatePrayerBook() {
		getPacketSender().setSidebarInterface(5, playerPrayerBook.getInterfaceId());
	}

	public void calcCombat() {
		int attack = getSkills().getStaticLevel(Skills.ATTACK);
		int defence = getSkills().getStaticLevel(Skills.DEFENSE);
		int strength = getSkills().getStaticLevel(Skills.STRENGTH);
		int hitpoints = getSkills().getStaticLevel(Skills.HITPOINTS);
		int prayer = getSkills().getStaticLevel(Skills.PRAYER);
		int mag = getSkills().getStaticLevel(Skills.MAGIC);
		int ran = getSkills().getStaticLevel(Skills.RANGED);
		int summoning = !Config.isPvpWorld() && wildLevel <= 0 ? getSkills().getStaticLevel(Skills.SUMMONING) : 0;

		int melee = attack + strength;
		int range = ran * 3 / 2;
		int mage = mag * 3 / 2;
		int max = Math.max(melee, Math.max(range, mage));
		int other = defence + hitpoints + prayer / 2 + summoning / 2;
		combatLevel = (max * 13 / 10 + other) / 4;

		getUpdateFlags().add(UpdateFlag.APPEARANCE);
		
		if (Config.isPvpWorld())
			PvpFormula.calculatePvpRange(this);
	}
	
	public void setGatestone(Location gatestoneLocation) {
		this.gatestoneLocation = gatestoneLocation;
		getPacketSender().sendConfig(ConfigCodes.HAS_GATESTONE_DROPPED, 1);
	}

	public void resetGatestone() {
		this.gatestoneLocation = null;
		getPacketSender().sendConfig(ConfigCodes.HAS_GATESTONE_DROPPED, 0);
	}

	public void increaseFloorsUnlocked() {
		if (dungFloorsUnlocked >= 60) {
			return;
		}

		dungFloorsUnlocked++;
	}

	public void unlockComplexity(int complexity) {
		if (complexity < 1 || complexity > 6) {
			return;
		}

		complexity -= 2;
		complexityInfo |= 1 << complexity;
	}

	public boolean isComplexityLocked(int complexity) {
		if (complexity < 1 || complexity > 6) {
			return false;
		}

		complexity -= 2;
		return (complexityInfo & (1 << complexity)) == 0;
	}

	public int getMaxComplexity() {
		for (int i = 6; i >= 1; i--) {
			if (!isComplexityLocked(i)) {
				return i;
			}
		}

		return 1;
	}

	public void selectComplexity(int complexity) {
		if (complexity < 1 || complexity > 6) {
			return;
		}

		int unlocked = complexityInfo & 31;
		complexityInfo = unlocked + (complexity << 5);
	}

	public int getSelectedComplexity() {
		return complexityInfo >> 5;
	}

	public int getComplexityInfo() {
		return complexityInfo;
	}
	
	public void setComplexityInfo(int i) { // for loading
		this.complexityInfo = i;
	}

	public int getDungFloorsCompleted(int i) {
		return dungFloorsCompleted[i];
	}

	public void resetFloorsCompleted() {
		for (int i = 0; i < 2; i++) {
			dungFloorsCompleted[i] = 0;
		}
	}

	public boolean isDungFloorCompleted(int i) {
		i--;

		int index = i / 31;
		int bitId = i % 31;
		return (dungFloorsCompleted[index] & 1 << bitId) != 0;
	}

	public void setDungFloorCompleted(int i) {
		i--;

		int index = i / 31;
		int bitId = i % 31;
		dungFloorsCompleted[index] |= 1 << bitId;
	}

	public void setDungFloorUnCompleted(int i) {
		i--;

		int index = i / 31;
		int bitId = i % 31;
		dungFloorsCompleted[index] &= ~(1 << bitId);
	}

	public String getDungFloorsCompletedJsonString() {
		return GsonSave.gsond.toJson(dungFloorsCompleted);
	}
	
	public void loadDungFloorsCompleted(String str) {
		dungFloorsCompleted = GsonSave.gsond.fromJson(str, new TypeToken<int[]>() { }.getType());
		if (dungFloorsCompleted == null)
			dungFloorsCompleted = new int[2];
	}

	public boolean insideDungeoneering() {
		return dungParty != null && dungParty.dungManager != null;
	}

	public boolean inDuel, tradeAccepted, goodTrade, inTrade, tradeRequested, /* tradeResetNeeded, */ tradeConfirmed,
			tradeConfirmed2, canOffer, acceptTrade, acceptedTrade;

	/*
	 * End*
	 */

	public int attackAnim;

	public boolean usedToRun = false;

	public boolean takeAsNote;

	public int combatLevel;
	
	public int pvpWorldAttackLevelRange;

	public boolean saveFile = false;

	public int playerAppearance[] = new int[13];

	public int apset;

	public int actionID;

	public int wearItemTimer, wearId, wearSlot, interfaceId;

	public int tutorial = 15;

	/* Start of combat variables */

	public boolean usingGamesNeck = false;

	public boolean usingSkillsNeck = false;

	// public int[] woodcut = new int [3];

	public String burnName;

	public int[] woodcut = new int[7];

	public int wcTimer = 0;

	/* End of combat variables */

	public int[] mining = new int[3];

	public int miningTimer = 0;

	public boolean fishing = false;

	public int fishTimer = 0;

	//TODO convert this into an id check later on.
	public boolean smeltInterface;

	public boolean patchCleared;

	public int[] farm = new int[2];

	public boolean playerIsFarming = false;

	private Patch[] patches = new Patch[16];

	private int queuedAnimation = -1;

	public CycleEventContainer antiFirePot;
	public CycleEventContainer antiFirePotExpireMessage;

	public int killStreak = 0;

	public int highestKillStreak = 0;

	// private int wildernessTick = 0;
	public boolean freshOnTheHunt = false;

	public int huntPlayerIndex = 0;

	public int lastHintId = 0;

	// public int slayerTask, taskAmount;

	// public boolean savedGame = false;
	public AtomicBoolean savedGame = new AtomicBoolean(false);

	public boolean saveGameInProcess = false;

	public ArrayList<Integer> huntedBy = new ArrayList<>();

	public HashMap<Integer, Integer> sidebarInterfaceInfo = new HashMap<>();

	public int freezeOnHit;

	public int pcParticipation = 0;

	public int capturableProgress;
	public Team soulWarsTeam = Team.NONE;
	private int soulWarsInactivity;

	public void resetInactivity() {
		soulWarsInactivity = 0;
	}

	public int getSoulWarsInactivity() {
		return soulWarsInactivity;
	}

	public void increaseInactivity(int amt) {
		soulWarsInactivity += amt;
		if (soulWarsInactivity > 1000) {
			SoulWars.removePlayerFromGame((Client) this);
		}
	}

	public void decreaseInactivity(int amt) {
		soulWarsInactivity -= amt;
		if (soulWarsInactivity < 0) {
			soulWarsInactivity = 0;
		}
	}

	// public boolean[] prayerActive = { false, false, false, false, false,
	// false,
	// false, false, false, false, false, false, false, false, false,
	// false, false, false, false, false, false, false, false, false,
	// false, false };

	/**
	 * Castle Wars
	 */
	/**
	 * 1=sara - 2=zammy
	 */
	public int castleWarsTeam;

	// public static final int[] CURSE_DRAIN_RATE = { 500, 500, 500, 500, 500,
	// 500, 500,
	// 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500 };
	// public static final int[] CURSE_LEVEL_REQUIRED = { 50, 50, 52, 54, 56,
	// 59, 62, 65,
	// 68, 71, 74, 76, 78, 80, 82, 84, 86, 89, 92, 95 };
	// public static final int[] CURSE = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
	// 12, 13,
	// 14, 15, 16, 17, 18, 19 };
	// public static final String[] CURSE_NAME = { "Protect Item", "Sap
	// Warrior",
	// "Sap Ranger", "Sap Mage", "Sap Spirit", "Berserker",
	// "Deflect Summoning", "Deflect Magic", "Deflect Missiles",
	// "Deflect Melee", "Leech Attack", "Leech Ranged", "Leech Magic",
	// "Leech Defence", "Leech Strength", "Leech Energy",
	// "Leech Special Attack", "Wrath", "Soul Split", "Turmoil" };
	// public static final int[] CURSE_GLOW = { 610, 611, 612, 613, 614, 615,
	// 616, 617,
	// 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629 };
	// public static final int[] CURSE_HEAD_ICONS = { -1, -1, -1, -1, -1, -1,
	// 12, 10, 11,
	// 9, -1, -1, -1, -1, -1, -1, -1, 16, 17, -1 };

	public boolean inCwGame;

	public boolean onCwWalls;

	public boolean inCwWait;
	
	public boolean wantsToRejoin = true;
	
	public int goldObtainedInMinigames = 0;

	private int waitRoomCountDown;

	private int waitingRoomCampTimer;

	/**
	 * nex stuff
	 */
	private boolean hasCough = false;

	/**
	 * Fight Pits
	 */
	public boolean inPits = false;

	public int pitsStatus = 0;

	public boolean blockedShot = false;

	public boolean withinRangeToHit = false;
	public int rangeRequiredToHit = 5;

	public boolean withinDistanceToStopWalking = false;

	/**
	 * Clan wars
	 */
	public boolean duelScreenIsDuel = true;

	public int clanWarOpponent;

	public boolean chalAccepted, chalInterface;

	private Client duelingClientClanWar;

	private ArrayList<GameItem> clanWarPlayerStake = new ArrayList<>();

	public boolean toggleBot = false;

	public String connectedFrom = "";

	// public static String addr = ((InetSocketAddress)
	// c.getSession().getRemoteAddress()).getAddress().getHostAddress();
	public String globalMessage = "";

	private String playerName = null;
	private String displayName = null;
	public String nameReadyToChange = null;

	public String playerPass = null;

	public String UUID = "";

	public String oldUUID = "";

	public int playerRights = 0;

	public int mySQLIndex;

	public int xenforoIndex = 0;

	public String checkXenLink = "";

	public boolean skipPassword = false;

	// public PlayerHandler handler = null;
	public boolean inVerificationState = false;

	/**
	 * id is +1 of real id, to get real id do (playerItems[i] - 1)
	 */
	public int playerItems[] = new int[28];

	public int playerItemsN[] = new int[28];
	
	public int runePouchItems[] = new int[3];

	public int runePouchItemsN[] = new int[3];

	public int bankingItems[] = new int[Config.BANK_TAB_SIZE];

	public int bankingItemsN[] = new int[Config.BANK_TAB_SIZE];

	private long moneyPouch = 0;

	private int updateInvInterface;

	public boolean droppingItem = false;

	private boolean updateEquipmentRequired;

	private boolean updateEquipment[] = new boolean[14];

	private boolean wearingItemOnTick = false;

	public List<ComparableItem> itemsToKeep = new ArrayList<>();

	public int listenPM = -1;

	public int bankingTab = 0;// -1 = bank closed

	public int lastBankItem = 0;

	public int lastBankTab = 0;

	public int firstBankTab = 0;

	/**
	 * item ids are stored +1 of real id, to get real id do (bankItems[i]-1)
	 */
	public int bankItems0[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems0N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems1[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems1N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems2[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems2N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems3[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems3N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems4[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems4N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems5[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems5N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems6[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems6N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems7[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems7N[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems8[] = new int[Config.BANK_TAB_SIZE];

	public int bankItems8N[] = new int[Config.BANK_TAB_SIZE];

	public boolean bankNotes = false;

	public final TickTimer vengDelay = new TickTimer();

	public int[] playerEquipment = new int[14];

	public int[] displayEquipment = new int[14];

	public int[] playerEquipmentN = new int[14];
	
	public int[] tournInventory = new int[playerItems.length];
	public int tournInventoryN[] = new int[playerItems.length];
	public int[] tournEquip = new int[playerEquipment.length];
	public int[] tournEquipN = new int[playerEquipment.length];
	
	public int runePouchItemsSpawned[] = new int[3];
	public int runePouchItemsSpawnedN[] = new int[3];
	
	public SeasonPass seasonPass = new SeasonPass();

	public int[] getPlayerLevel() {
		return getSkills().getDynamicLevels();
	}

	/**
	 * 0=easy - 1=medium - 2=hard - 3=legendary - 4=ironmanMode
	 */
	public int difficulty = PlayerDifficulty.NORMAL; // 0=easy - 1=medium -
														// 2=hard - 3=legendary
														// - 4=ironmanMode

	/**
	 * 0=Varrock - 1=Edgeville - 2=Lumbridge - 3=Falador - 4=Camelot -
	 * 5=Neitiznot - 6=Ardougne - 7=Shilo - 8=Yanille - 9=Lunar
	 */
	public int homeTeleActions = 0; // 1-Varrock - 0=Edgeville - 2=Lumbridge -
									// 3=Falador - 4=Camelot - 5=Neitiznot -
									// 6=Ardougne - 7=Shilo - 8=Yanille -
									// 9=Lunar

	/* Attacking styles */

	public Zulrah zulrah;

	/**
	 * The list of local players.
	 */
	private List<Player> localPlayers = new LinkedList<>();

	public short npcList[] = new short[PlayerConstants.maxNPCListSize];

	public int npcListSize = 0;

	public boolean inNpcList(int index) {
		for (int i = 0; i < npcListSize; i++) {
			if (npcList[i] == index) {
				return true;
			}
		}

		return false;
	}

	public byte npcInListBitmap[] = new byte[(NPCHandler.maxNPCs + 7) >> 3];

	public volatile boolean correctlyInitialized = false;

	private volatile boolean saveInProgressForDC = false;

	public volatile boolean skipThreadCheck = true;

	public volatile boolean firstWarning = false;

	public volatile boolean startedCheckSpins = false;

	public int mapRegionX, mapRegionY;

	public int regionCheckX, regionCheckY, regionCheckZ;
	
	private boolean loadedRegion = false;
	
	public boolean addingFriend = false;

//	public int absX, absY;

	// public int damageToDealAfterCycle = 0;
	// public ArrayList<Integer> damageQueue = new ArrayList<Integer>();

	public int currentX, currentY;

	public int noClipX, noClipY;

	public int oldX, oldY;

	public int playerSE = 0x328;

	public int playerSEW = 0x333;

	public int playerSER = 0x334;

	public int multiWay = -1;

	private boolean[] receivedPacket = new boolean[256];

	public boolean blockRun = false;
	public boolean isRunning = true;

	public int teleportToX = -1, teleportToY = -1, teleportToZ = -1;

	// private int runEnergy = 100;
	public long lastRunRecovery;

	public int runRecoverBoost = 0;

	public boolean didTeleport = false;

	public boolean mapRegionDidChange = false;

	public boolean updateRegion = false;

	public int dir1 = -1;
	public int runTilesPerTick = 1;
	public List<Integer> runTiles = new ArrayList<>(10);

	public boolean isRunningOnThisTick = false;

	public boolean createItems = false;

	public int poimiX = 0, poimiY = 0;

	protected /* static */ Stream playerProps;
	// static {
	// playerProps = new Stream(new byte[100]);
	// }

	private byte chatText[] = new byte[100];

	private byte chatTextSize = 0;

	private int chatTextColor = 0;

	private int chatTextEffects = 0;

	/**
	 * Face player or npc
	 *
	 * @param index
	 *            - if not added with 32768 then will face NPC
	 */

	private List<CycleEventContainer> combatEventsToProcess = new ArrayList<>();
	
	private List<CycleEventContainer> specQueue = new ArrayList<>();

	public boolean disabled = false;

	/**
	 * Array of the burdened items.
	 */
	public int[] burdenedItems = new int[30];

	public int getVar(Variables var) {
		if (var.getCode() < 0 || var.getCode() >= variables.length) {
			return -1;
		}
		return variables[var.getCode()];
	}

	public Player() {
		super(-1, Location.create(DBConfig.START_LOCATION_X, DBConfig.START_LOCATION_Y), true);
		playerRights = 0;

		variables = new int[Variables.values.length];
		for (int i = 0; i < variables.length; i++) {
			Variables var = Variables.values[i];
			variables[i] = var.getDefaultValue();
		}

		for (int i = 0; i < playerItems.length; i++) {
			playerItems[i] = 0;
			playerItemsN[i] = 0;
		}

		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			getPlayerLevel()[i] = 1;
		}

		getPlayerLevel()[Skills.HITPOINTS] = 10;

//		for (int i = 0; i < playerXP.length; i++) {
//			if (i == 3) {
//				playerXP[i] = 1300;
//			} else {
//				playerXP[i] = 0;
//			}
//		}
		for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
			bankItems0[i] = 0;
			bankItems0N[i] = 0;
		}

		playerAppearance[0] = 0; // gender
		playerAppearance[1] = 0; // head
		playerAppearance[2] = 18;// Torso
		playerAppearance[3] = 26; // arms
		playerAppearance[4] = 33; // hands
		playerAppearance[5] = 36; // legs
		playerAppearance[6] = 42; // feet
		playerAppearance[7] = 10; // beard
		playerAppearance[8] = 0; // hair colour
		playerAppearance[9] = 0; // torso colour
		playerAppearance[10] = 0; // legs colour
		playerAppearance[11] = 0; // feet colour
		playerAppearance[12] = 0; // skin colour

		apset = 0;
		actionID = 0;

		playerEquipment[PlayerConstants.playerHat] = -1;
		playerEquipment[PlayerConstants.playerCape] = -1;
		playerEquipment[PlayerConstants.playerAmulet] = -1;
		playerEquipment[PlayerConstants.playerChest] = -1;
		playerEquipment[PlayerConstants.playerShield] = -1;
		playerEquipment[PlayerConstants.playerLegs] = -1;
		playerEquipment[PlayerConstants.playerHands] = -1;
		playerEquipment[PlayerConstants.playerFeet] = -1;
		playerEquipment[PlayerConstants.playerRing] = -1;
		playerEquipment[PlayerConstants.playerArrows] = -1;
		playerEquipment[PlayerConstants.playerWeapon] = -1;
		playerEquipment[6] = -1;
		playerEquipment[8] = -1;
		playerEquipment[11] = -1;

		heightLevel = 0;

		teleportToX = DBConfig.START_LOCATION_X;
		teleportToY = DBConfig.START_LOCATION_Y;

		teleportToZ = 0;

		absX = absY = -1;
		mapRegionX = mapRegionY = -1;
		currentX = currentY = 0;
		movement.hardResetWalkingQueue();
		updateCenterLocation();

		patches = getPatches();

		// new addition for hpefully dc fix? 9/30/2015
		playerProps = new Stream(new byte[200]);
	}
	
	public boolean isRequiredPinVerification() {
		if (!UUID.equals(oldUUID) && hasBankPin) {
			return true;
		}
		return false;
	}
	
	public void fillPokemonData() {
		for (PokemonData data : PokemonData.data) {
			pokemon[data.ordinal()] = new Pokemon(data, data.baseStats);
		}
	}

	public void addEvent(CycleEventContainer container) {
		this.combatEventsToProcess.add(container);
	}

	public void addEvent(int id, Object owner, CycleEvent event, int cycles) {
		this.combatEventsToProcess.add(new CycleEventContainer(id, owner, event, cycles));
	}

	public void addEvent(Object owner, CycleEvent event, int cycles) {
		this.combatEventsToProcess.add(new CycleEventContainer(-1, owner, event, cycles));
	}
	
	public void addSpecQueue(Object owner, CycleEvent event) {
		this.specQueue.add(new CycleEventContainer(-1, owner, event, 1));
	}
	
	private void addHitToQueue(final Player p, Hit hit) {
		CycleEventHandler.getSingleton().addEvent(p, true, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (p.disconnected || p == null || p.isDead()
						|| p.getHitPoints() < 1 || p.duelStatus == 6) {
					container.stop();
					return;
				}				
				
				if (!p.updateFlags.contains(UpdateFlag.HIT)) {
					p.hitMask = hit;
					p.updateFlags.add(UpdateFlag.HIT);
					container.stop();
				} else if (!p.updateFlags.contains(UpdateFlag.HIT2)) {
					p.hitMask2 = hit;
					p.updateFlags.add(UpdateFlag.HIT2);
					container.stop();
				}

			}

			@Override
			public void stop() {

			}

		}, 1);
	}

	public void showHitMask(Hit hit) {		
		if (duelStatus == 6) {
			return;
		}

		if (!updateFlags.contains(UpdateFlag.HIT)) {
			this.hitMask = hit;
			updateFlags.add(UpdateFlag.HIT);
		} else if (!updateFlags.contains(UpdateFlag.HIT2)) {
			this.hitMask2 = hit;
			updateFlags.add(UpdateFlag.HIT2);
		} else {
			addHitToQueue(this, hit);
		}
	}

	public void addToHp(int toAdd) {
		if (getSkills().getLifepoints() <= 0) {
			return;
		}
		// System.out.println("maxhP"+getLevelForXP(playerXP[3]));
		int max = calculateMaxLifePoints();
		if (getSkills().getLifepoints()
				+ toAdd >= max/*
								 * PlayerConstants. getLevelForXP (playerXP[3])
								 */) {
			getSkills().setLevel(Skills.HITPOINTS, max);/*
													 * PlayerConstants.
													 * getLevelForXP(playerXP[3]
													 * )
													 */;
			return;
		}
		getSkills().heal(toAdd);
	}

	/**
	 * Adds this entity to the specified region.
	 *
	 * @param region
	 *            The region.
	 */
	public void addToRegion(Region region) {
		region.addPlayer(this);
	}





	public void applySoulSplitFromNpc(NPC npc, int damage) {
		if (npc == null) {
			return;
		}
		final int ssHeal = damage / 20;

		final int ssSmite = damage / 4;

		if (ssHeal > 0 || ssSmite > 0) {

			Projectile proj = new Projectile(2263, getCurrentLocation(),
					Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()));
			proj.setStartHeight(25);
			proj.setEndHeight(25);
			proj.setStartDelay(40);
			proj.setLockon(npc.getProjectileLockon());
			proj.setEndDelay(75);
			GlobalPackets.createProjectile(proj);

			// GlobalPackets.createPlayersProjectile(getX(), getY(), getZ(),
			// (getY() - npc.getY()) * -1, (getX() - npc.getX()) * -1, 50,
			// 75, 2263, 25, 25, npc.getNpcIndex() + 1, 40);

			startGraphic(Graphic.create(2264, GraphicType.LOW));
		}

		if (ssSmite > 0) {
			getPlayerLevel()[Skills.PRAYER] -= ssSmite;
			if (getPlayerLevel()[Skills.PRAYER] <= 0) {
				getPlayerLevel()[Skills.PRAYER] = 0;
				getCombat().resetPrayers();
			}
			refreshSkill(Skills.PRAYER);
		}

		if (ssHeal != 0) { // heal npc here
			npc.setHP(npc.getSkills().getLifepoints() + ssHeal);
		}

	}

	public boolean Area(final int x1, final int x2, final int y1, final int y2) {
		return (absX >= x1 && absX <= x2 && absY >= y1 && absY <= y2);
	}

	public int battleMageAmount() {
		int pieces = 0;
		if (playerEquipment[PlayerConstants.playerHat] == 21462) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerChest] == 21463) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerLegs] == 21464) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerHands] == 21465) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerFeet] == 21466) {
			pieces++;
		}

		return pieces;
	}

	public void BestItem1() {
		int BestValue = 0;
		int NextValue = 0;
		int ItemsContained = 0;
		WillKeepItem1 = 0;
		WillKeepItem1Slot = 0;
		// boolean foundUntradeableItem = false;
		for (int ITEM = 0; ITEM < playerItems.length; ITEM++) {
			if (playerItems[ITEM] > 0) {
				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerItems[ITEM]-1 == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerItems[ITEM] - 1) && !ItemProjectInsanity.itemIsChargeItem(playerItems[ITEM] - 1)) {
					continue;
				}

				ItemsContained += 1;
				if (ItemDefinition.forId(playerItems[ITEM] - 1) == null) {
					NextValue = 0;
				} else {
					NextValue = ItemDefinition.forId(playerItems[ITEM] - 1).getDropValue(); // (int)
																						// Math.floor(c.getShops().getItemShopValue(playerItems[ITEM]
																						// -
																						// 1));
				}
				if (NextValue > BestValue) {
					BestValue = NextValue;
					WillKeepItem1 = playerItems[ITEM] - 1;
					WillKeepItem1Slot = ITEM;
					if (playerItemsN[ITEM] > 2 && (playerPrayerBook == PrayerBook.NORMAL ? !prayerActive[Prayers.PROTECT_ITEM.ordinal()]
							: !curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt1 = 3;
					} else if (playerItemsN[ITEM] > 3
							&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt1 = 4;
					} else {
						WillKeepAmt1 = playerItemsN[ITEM];
					}
				}
			}
		}
		for (int EQUIP = 0; EQUIP < playerEquipment.length; EQUIP++) {
			if (playerEquipment[EQUIP] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerEquipment[EQUIP] == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerEquipment[EQUIP]) && !ItemProjectInsanity.itemIsChargeItem(playerEquipment[EQUIP])) {
					continue;
				}

				ItemsContained += 1;
				if (ItemDefinition.forId(playerEquipment[EQUIP]) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerEquipment[EQUIP]).getDropValue(); // (int)
																							// Math.floor(c.getShops().getItemShopValue(playerEquipment[EQUIP]));
				}
				if (NextValue > BestValue) {
					BestValue = NextValue;
					WillKeepItem1 = playerEquipment[EQUIP];
					WillKeepItem1Slot = EQUIP + 28;
					if (playerEquipmentN[EQUIP] > 2
							&& (playerPrayerBook == PrayerBook.NORMAL ? !prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: !curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt1 = 3;
					} else if (playerEquipmentN[EQUIP] > 3
							&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt1 = 4;
					} else {
						WillKeepAmt1 = playerEquipmentN[EQUIP];
					}
				}
			}
		}
		if (!isSkulled && ItemsContained > 1
				&& (WillKeepAmt1 < 3 || (prayerActive[Prayers.PROTECT_ITEM.ordinal()] && WillKeepAmt1 < 4))) {
			BestItem2(ItemsContained);
		}
	}

	public void BestItem2(int ItemsContained) {
		int BestValue = 0;
		int NextValue = 0;
		WillKeepItem2 = 0;
		WillKeepItem2Slot = 0;
		// boolean foundUntradeableItem = false;
		for (int ITEM = 0; ITEM < playerItems.length; ITEM++) {
			if (playerItems[ITEM] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerItems[ITEM]-1 == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerItems[ITEM] - 1) && !ItemProjectInsanity.itemIsChargeItem(playerItems[ITEM] - 1)) {
					continue;
				}

				if (ItemDefinition.forId(playerItems[ITEM] - 1) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerItems[ITEM] - 1).getDropValue(); // (int)
																						// Math.floor(getShops().getItemShopValue(playerItems[ITEM]
																						// -
																						// 1));
				}
				if (NextValue > BestValue && !(ITEM == WillKeepItem1Slot && playerItems[ITEM] - 1 == WillKeepItem1)) {
					BestValue = NextValue;
					WillKeepItem2 = playerItems[ITEM] - 1;
					WillKeepItem2Slot = ITEM;
					if (playerItemsN[ITEM] > 2 - WillKeepAmt1
							&& (playerPrayerBook == PrayerBook.NORMAL ? !prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: !curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt2 = 3 - WillKeepAmt1;
					} else if (playerItemsN[ITEM] > 3 - WillKeepAmt1
							&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt2 = 4 - WillKeepAmt1;
					} else {
						WillKeepAmt2 = playerItemsN[ITEM];
					}
				}
			}
		}
		for (int EQUIP = 0; EQUIP < playerEquipment.length; EQUIP++) {
			if (playerEquipment[EQUIP] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerEquipment[EQUIP] == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerEquipment[EQUIP]) && !ItemProjectInsanity.itemIsChargeItem(playerEquipment[EQUIP])) {
					continue;
				}

				if (ItemDefinition.forId(playerEquipment[EQUIP]) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerEquipment[EQUIP]).getDropValue(); // (int)
																							// Math.floor(getShops().getItemShopValue(playerEquipment[EQUIP]));
				}
				if (NextValue > BestValue
						&& !(EQUIP + 28 == WillKeepItem1Slot && playerEquipment[EQUIP] == WillKeepItem1)) {
					BestValue = NextValue;
					WillKeepItem2 = playerEquipment[EQUIP];
					WillKeepItem2Slot = EQUIP + 28;
					if (playerEquipmentN[EQUIP] > 2 - WillKeepAmt1
							&& (playerPrayerBook == PrayerBook.NORMAL ? !prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: !curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt2 = 3 - WillKeepAmt1;
					} else if (playerEquipmentN[EQUIP] > 3 - WillKeepAmt1
							&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt2 = 4 - WillKeepAmt1;
					} else {
						WillKeepAmt2 = playerEquipmentN[EQUIP];
					}
				}
			}
		}
		if (!isSkulled && ItemsContained > 2 && (WillKeepAmt1 + WillKeepAmt2 < 3
				|| (prayerActive[Prayers.PROTECT_ITEM.ordinal()] && WillKeepAmt1 + WillKeepAmt2 < 4))) {
			BestItem3(ItemsContained);
		}
	}

	public void BestItem3(int ItemsContained) {
		int BestValue = 0;
		int NextValue = 0;
		WillKeepItem3 = 0;
		WillKeepItem3Slot = 0;
		// boolean foundUntradeableItem = false;
		for (int ITEM = 0; ITEM < playerItems.length; ITEM++) {
			if (playerItems[ITEM] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerItems[ITEM]-1 == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerItems[ITEM] - 1) && !ItemProjectInsanity.itemIsChargeItem(playerItems[ITEM] - 1)) {
					continue;
				}

				if (ItemDefinition.forId(playerItems[ITEM] - 1) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerItems[ITEM] - 1).getDropValue(); // (int)
																						// Math.floor(getShops().getItemShopValue(playerItems[ITEM]
																						// -
																						// 1));
				}
				if (NextValue > BestValue && !(ITEM == WillKeepItem1Slot && playerItems[ITEM] - 1 == WillKeepItem1)
						&& !(ITEM == WillKeepItem2Slot && playerItems[ITEM] - 1 == WillKeepItem2)) {
					BestValue = NextValue;
					WillKeepItem3 = playerItems[ITEM] - 1;
					WillKeepItem3Slot = ITEM;
					if (playerItemsN[ITEM] > 2 - (WillKeepAmt1 + WillKeepAmt2)
							&& (playerPrayerBook == PrayerBook.NORMAL ? !prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: !curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt3 = 3 - (WillKeepAmt1 + WillKeepAmt2);
					} else if (playerItemsN[ITEM] > 3 - (WillKeepAmt1 + WillKeepAmt2)
							&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt3 = 4 - (WillKeepAmt1 + WillKeepAmt2);
					} else {
						WillKeepAmt3 = playerItemsN[ITEM];
					}
				}
			}
		}
		for (int EQUIP = 0; EQUIP < playerEquipment.length; EQUIP++) {
			if (playerEquipment[EQUIP] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerEquipment[EQUIP] == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerEquipment[EQUIP]) && !ItemProjectInsanity.itemIsChargeItem(playerEquipment[EQUIP])) {
					continue;
				}

				if (ItemDefinition.forId(playerEquipment[EQUIP]) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerEquipment[EQUIP]).getDropValue();// (int)
																						// Math.floor(getShops().getItemShopValue(playerEquipment[EQUIP]));
				}
				if (NextValue > BestValue
						&& !(EQUIP + 28 == WillKeepItem1Slot && playerEquipment[EQUIP] == WillKeepItem1)
						&& !(EQUIP + 28 == WillKeepItem2Slot && playerEquipment[EQUIP] == WillKeepItem2)) {
					BestValue = NextValue;
					WillKeepItem3 = playerEquipment[EQUIP];
					WillKeepItem3Slot = EQUIP + 28;
					if (playerEquipmentN[EQUIP] > 2 - (WillKeepAmt1 + WillKeepAmt2)
							&& (playerPrayerBook == PrayerBook.NORMAL ? !prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: !curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt3 = 3 - (WillKeepAmt1 + WillKeepAmt2);
					} else if (playerEquipmentN[EQUIP] > 3 - WillKeepAmt1
							&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
									: curseActive[Curses.PROTECT_ITEM.ordinal()])) {
						WillKeepAmt3 = 4 - (WillKeepAmt1 + WillKeepAmt2);
					} else {
						WillKeepAmt3 = playerEquipmentN[EQUIP];
					}
				}
			}
		}
		if (!isSkulled && ItemsContained > 3
				&& (playerPrayerBook == PrayerBook.NORMAL ? prayerActive[Prayers.PROTECT_ITEM.ordinal()]
						: curseActive[Curses.PROTECT_ITEM.ordinal()])
				&& ((WillKeepAmt1 + WillKeepAmt2 + WillKeepAmt3) < 4)) {
			BestItem4();
		}
	}

	public void BestItem4() {
		int BestValue = 0;
		int NextValue = 0;
		WillKeepItem4 = 0;
		WillKeepItem4Slot = 0;
		// boolean foundUntradeableItem = false;
		for (int ITEM = 0; ITEM < playerItems.length; ITEM++) {
			if (playerItems[ITEM] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerItems[ITEM]-1 == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerItems[ITEM] - 1) && !ItemProjectInsanity.itemIsChargeItem(playerItems[ITEM] - 1)) {
					continue;
				}

				if (ItemDefinition.forId(playerItems[ITEM] - 1) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerItems[ITEM] - 1).getDropValue();// (int)
																						// Math.floor(getShops().getItemShopValue(playerItems[ITEM]
																						// -
																						// 1));
				}
				if (NextValue > BestValue && !(ITEM == WillKeepItem1Slot && playerItems[ITEM] - 1 == WillKeepItem1)
						&& !(ITEM == WillKeepItem2Slot && playerItems[ITEM] - 1 == WillKeepItem2)
						&& !(ITEM == WillKeepItem3Slot && playerItems[ITEM] - 1 == WillKeepItem3)) {
					BestValue = NextValue;
					WillKeepItem4 = playerItems[ITEM] - 1;
					WillKeepItem4Slot = ITEM;
				}
			}
		}
		for (int EQUIP = 0; EQUIP < playerEquipment.length; EQUIP++) {
			if (playerEquipment[EQUIP] > 0) {

				// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
				// if (playerEquipment[EQUIP] == Config.ITEM_TRADEABLE[jj]) {
				// foundUntradeableItem = true;
				// continue;
				// }
				// }
				// if (foundUntradeableItem) {
				// foundUntradeableItem = false;
				// continue;
				// }

				if (!ItemProjectInsanity.itemIsTradeable(playerEquipment[EQUIP]) && !ItemProjectInsanity.itemIsChargeItem(playerEquipment[EQUIP])) {
					continue;
				}

				if (ItemDefinition.forId(playerEquipment[EQUIP]) == null) {
					NextValue = 1;
				} else {
					NextValue = ItemDefinition.forId(playerEquipment[EQUIP]).getDropValue();// (int)
																						// Math.floor(getShops().getItemShopValue(playerEquipment[EQUIP]));
				}
				if (NextValue > BestValue
						&& !(EQUIP + 28 == WillKeepItem1Slot && playerEquipment[EQUIP] == WillKeepItem1)
						&& !(EQUIP + 28 == WillKeepItem2Slot && playerEquipment[EQUIP] == WillKeepItem2)
						&& !(EQUIP + 28 == WillKeepItem3Slot && playerEquipment[EQUIP] == WillKeepItem3)) {
					BestValue = NextValue;
					WillKeepItem4 = playerEquipment[EQUIP];
					WillKeepItem4Slot = EQUIP + 28;
				}
			}
		}
	}

	public int calculateMaxLifePoints() {
		int lifePoints = getSkills().getStaticLevel(Skills.HITPOINTS);
		int healthPowerUp = getSeasonalData().getPowerUpAmount(PowerUpData.HEALTH);
		if (healthPowerUp > 0) {
			lifePoints += lifePoints * (healthPowerUp * 25) / 100;
		}

		if (isThereNexArmourEquipped(PlayerConstants.playerHat)) {
			lifePoints += getZones().isInEdgePk() ? RSConstants.NEX_ARMOR_HELM_HP_EDGE : RSConstants.NEX_ARMOR_HELM_HP;
		}
		if (isThereNexArmourEquipped(PlayerConstants.playerChest)) {
			lifePoints += getZones().isInEdgePk() ? RSConstants.NEX_ARMOR_BODY_HP_EDGE : RSConstants.NEX_ARMOR_BODY_HP;
		}
		if (isThereNexArmourEquipped(PlayerConstants.playerLegs)) {
			lifePoints += getZones().isInEdgePk() ? RSConstants.NEX_ARMOR_LEGS_HP_EDGE : RSConstants.NEX_ARMOR_LEGS_HP;
		}
		return lifePoints;
	}

	public boolean canGetHit() {
		if (!correctlyInitialized || hidePlayer || isDead() || getHitPoints() <= 0) {
			return false;
		}
		return true;
	}

	public boolean canWithDrawOrDepositMoneyPouch() {
		if (inMinigame()) {
			return false;
		}
		if (wildLevel > 0) {
			return false;
		}
		

		if (requireBankPinToOpen()) {

			((Client) this).getBankPin().openPin();
			return false;
		}

		return true;
	}

	public boolean checkFriendsUpdate(int loggedIn1Loggedout0, long friendId) {
		if (friendsUpdate.containsKey(friendId)) {
			int lastInt = friendsUpdate.get(friendId);
			if (lastInt == loggedIn1Loggedout0) {
				return false;
			}
		}
		friendsUpdate.put(friendId, loggedIn1Loggedout0);
		return true;
	}

	public void clearLists() {
		// objectToRemove.clear();
		for (RSObject rsO : objectToRemove) {
			if (!Misc.isInsideRenderedArea(this, rsO.getX(), rsO.getY())) {
				objectToRemoveTemp.add(rsO);
			}
		}
		objectToRemove.removeAll(objectToRemoveTemp);
		objectToRemoveTemp.clear();

		for (int i = 0, len = itemsToRemove.size(); i < len; i++) {
			GroundItem item = itemsToRemove.get(i);
			if (!Misc.isInsideRenderedArea(this, item.getItemX(), item.getItemY()))
				itemsToRemoveTemp.add(item);
		}
		
		itemsToRemove.removeAll(itemsToRemoveTemp);
		itemsToRemoveTemp.clear();
	}

	@Override
	public int dealDamage(Hit hit) {
		if (hit.getDamage() < 0) {
			hit.setDamage(0);
		}
		
//		if (hitSplat < 0 || hitSplat > 2) {
//			hitSplat = 0;
//		}
//		
//		if (hitIcon < -1 || hitIcon > 4) {
//			hitIcon = -1;
//		}

		if (duelStatus == 6) {
			hit.setDamage(0);
			showHitMask(new Hit(0, 0, 0));
			return 0;
		}

		if (hidePlayer || isImmuneToDamage()) {
			hit.setDamage(0);
			showHitMask(new Hit(0, 0, 0));
			return 0;
		}
		
		if (isInLmsArena() && hit.getSource() != null && hit.getSource().isPlayer()) {
			if (hit.getSource().getSkills().getLifepoints() <= 0) {
				hit.setDamage(0);
				showHitMask(new Hit(0, 0, 0));
				return 0;
			}
		}
		
		final int newDamage = getSkills().hit(hit.getDamage());

		int currentHp = getHitPoints();
		if (currentHp > 0) {
			int maxHp = getSkills().getMaximumLifepoints();
			if (playerEquipment[PlayerConstants.playerAmulet] == 11090 && currentHp < maxHp * 0.2D && !duelRule[DuelRules.NO_FOOD]) {
				getItems().deleteEquipment(11090, PlayerConstants.playerAmulet);
				getSkills().heal((int) (maxHp * 0.3D));
				sendMessage("<col=FF6600><shad=917E53>Your Phoenix necklace incinerates itself, restoring some health.");
				startGraphic(GraphicCollection.HEAL_GFX);
			} else if (prayerActive[Prayers.REDEMPTION.ordinal()] && currentHp < maxHp * 0.1D) {
				getSkills().heal((int) (getSkills().getStaticLevel(Skills.PRAYER) * 0.25D));
				getSkills().drainPrayer();
				CombatPrayer.turnOffPrayers(this);
				startGraphic(GraphicCollection.REDEMPTION_GFX);
			} else if (playerEquipment[PlayerConstants.playerRing] == 2570 && currentHp < maxHp * 0.1D && !inMinigame()) {
				getItems().deleteEquipment(11090, PlayerConstants.playerRing);
				getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sendMessage("<col=FF6600><shad=917E53>Your Ring of life incinerates itself, teleporting you away.");
			}
		}

		showHitMask(hit);
		
		return newDamage;
	}
	
	/**
	 * Deals magic damage. If player has prayer on, it will reduce damage. NOTE:
	 * if attacker is dead, then damage won't be dealt. future heads up
	 *
	 * @param damage
	 *            - damage to display/hit
	 * @param ticks:
	 *            ticks to wait till damage is dealt
	 */
	public int dealMagicDamage(int damage, final int ticks, final Graphic gfx,
			final Mob attacker) {

		if (hidePlayer || isImmuneToDamage())
			return 0;

		if (curseActive[Curses.DEFLECT_MAGIC.ordinal()] || prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]) { // deflect
																													// magic
			if (attacker != null && attacker.isNpc()) {
				damage = (int) (damage * attacker.toNpcFast().prayerReductionMagic);
			} else {
				damage = damage * 60 / 100;
			}
		}

		damage = applyDamageReduction(damage, CombatType.MAGIC);

		final int damageFinal = damage;

		getPA().closeAllWindows();

		if (ticks < 0) {
			dealMagicDamageInstant(damageFinal, gfx, attacker);
		} else {
			CycleEventHandler.getSingleton().addEvent(this, true, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (disconnected || isDead() || forceDisconnect || isDead() || getHitPoints() < 1) {
						container.stop();
						return;
					}

					if (teleTimer > 0) {
						if (container.getTick() > 1) {
							container.setTick(1);
						}
						return;
					}

					dealMagicDamageInstant(damageFinal, gfx, attacker);

					container.stop();
				}

				@Override
				public void stop() {

				}

			}, ticks);
		}
		return damageFinal;
	}
	
	private void dealMagicDamageInstant(int damage, final Graphic gfx, final Mob attacker) {
		if (gfx != null) {
			if (damage > 0) {
				startGraphic(gfx);
			} else {
				startGraphic(GraphicCollection.MAGIC_SPLASH);
			}
		}
		dealDamage(new Hit(damage, 0, 2, attacker));

		if (attacker != null) {
			if (attacker.isNpc()) {
				final NPC npc = attacker.toNpcFast();
				if (!npc.isDead()) {
					if (npc.isVenomousNpc()) {
						getDotManager().applyVenom();
					}

					getDamageMap().incrementTotalDamage(npc.getNpcIndex(), damage);
					putInCombatWithNpc(npc.npcIndex);
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							npcIndex = npc.npcIndex;
						}
					}
					if (npc.isUsingSoulSplit()) {
						applySoulSplitFromNpc(npc, damage);
					}
				}
			} else if (attacker.isPlayer()) {
				final Player c2 = attacker.toPlayerFast();
				if (!c2.isDead()) {
					getDamageMap().incrementTotalDamage(c2.mySQLIndex, damage);
					underAttackBy = c2.getId();
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							playerIndex = c2.getId();
						}
					}
				}
			}
		}
	}

	/**
	 * Deals Melee damage. If player has prayer on, it will reduce damage
	 *
	 * @param damage
	 *            - damage to display/hit
	 * @param ticks:
	 *            ticks to wait till damage is dealt
	 */
	public int dealMeleeDamage(int damage, final int ticks, final Graphic gfx,
			final Mob attacker) {
		if (hidePlayer || isImmuneToDamage())
			return 0;

		if (curseActive[Curses.DEFLECT_MELEE.ordinal()] || prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]) { // deflect
																													// melee
			if (attacker != null && attacker.isNpc()) {
				damage = (int) (damage * attacker.toNpcFast().prayerReductionMelee);
			} else {
				damage = damage * 60 / 100;
			}
		}

		damage = applyDamageReduction(damage, CombatType.MELEE);

		final int damageFinal = damage;

		getPA().closeAllWindows();

		if (ticks < 0) { // isntant hit, can only run -1 if already inside the combat cycle event
			dealMeleeDamageInstant(damageFinal, gfx, attacker);
		} else {

			CycleEventHandler.getSingleton().addEvent(this, true, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (disconnected || isDead() || forceDisconnect || isDead() || getHitPoints() < 1) {
						container.stop();
						return;
					}

					if (teleTimer > 0) {
						if (container.getTick() > 1) {
							container.setTick(1);
						}
						return;
					}

					dealMeleeDamageInstant(damageFinal, gfx, attacker);

					container.stop();
				}

				@Override
				public void stop() {

				}

			}, ticks);
		}

		return damageFinal;
	}
	
	private void dealMeleeDamageInstant(int damage, final Graphic gfx, final Mob attacker) {
		if (gfx != null) {
			startGraphic(gfx);
		}

		dealDamage(new Hit(damage, 0, 0, attacker));

		if (attacker != null) {
			if (attacker.isNpc()) {
				final NPC npc = attacker.toNpcFast();
				if (!npc.isDead()) {
					if (npc.isVenomousNpc()) {
						getDotManager().applyVenom();
					}

					getDamageMap().incrementTotalDamage(npc.getNpcIndex(), damage);
					putInCombatWithNpc(npc.npcIndex);
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							npcIndex = npc.npcIndex;
						}
					}
					if (npc.isUsingSoulSplit()) {
						applySoulSplitFromNpc(npc, damage);
					}
				}
			} else if (attacker.isPlayer()) {
				final Player c2 = attacker.toPlayerFast();
				if (!c2.isDead()) {
					getDamageMap().incrementTotalDamage(c2.mySQLIndex, damage);
					underAttackBy = c2.getId();
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							playerIndex = c2.getId();
						}
					}
				}
			}
		}
	}

	/**
	 * Deals Range damage. If player has prayer on, it will reduce damage
	 *
	 * @param damage
	 *            - damage to display/hit
	 * @param ticks:
	 *            ticks to wait till damage is dealt
	 */
	public int dealRangeDamage(int damage, final int ticks, final Graphic gfx,
			final Mob attacker) {
		if (hidePlayer || isImmuneToDamage())
			return 0;

		if (curseActive[Curses.DEFLECT_MISSILES.ordinal()] || prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]) { // deflect
																														// range
			if (attacker != null && attacker.isNpc()) {
				damage = (int) (damage * attacker.toNpcFast().prayerReductionRanged);
			} else {
				damage = damage * 60 / 100;
			}
		}

		damage = applyDamageReduction(damage, CombatType.RANGED);

		final int damageFinal = damage;

		getPA().closeAllWindows();

		if (ticks < 0) { // isntant hit, can only run -1 if already inside the combat cycle event
			dealRangeDamageInstant(damageFinal, gfx, attacker);
		} else {
			CycleEventHandler.getSingleton().addEvent(this, true, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (disconnected || isDead() || forceDisconnect || isDead() || getHitPoints() < 1) {
						container.stop();
						return;
					}

					if (teleTimer > 0) {
						if (container.getTick() > 1) {
							container.setTick(1);
						}
						return;
					}

					dealRangeDamageInstant(damageFinal, gfx, attacker);

					container.stop();
				}

				@Override
				public void stop() {

				}

			}, ticks);
		}
		return damageFinal;
	}
	
	private void dealRangeDamageInstant(int damage, final Graphic gfx, final Mob attacker) {
		if (gfx != null) {
			startGraphic(gfx);
		}

		dealDamage(new Hit(damage, 0, 1, attacker));

		if (attacker != null) {
			if (attacker.isNpc()) {
				final NPC npc = attacker.toNpcFast();
				if (!npc.isDead()) {
					if (npc.isVenomousNpc()) {
						getDotManager().applyVenom();
					}

					getDamageMap().incrementTotalDamage(npc.getNpcIndex(), damage);
					putInCombatWithNpc(npc.npcIndex);
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							npcIndex = npc.npcIndex;
						}
					}
					if (npc.isUsingSoulSplit()) {
						applySoulSplitFromNpc(npc, damage);
					}
				}

			} else if (attacker.isPlayer()) {
				final Player c2 = attacker.toPlayerFast();
				if (!c2.isDead()) {
					getDamageMap().incrementTotalDamage(c2.mySQLIndex, damage);
					underAttackBy = c2.getId();
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1) {
							playerIndex = c2.getId();
						}
					}
				}
			}
		}
	}

	public int dealTrueDamage(int damage, final int ticks, final Graphic gfx,
			final Mob attacker) {
		if (hidePlayer || isImmuneToDamage())
			return 0;

		getPA().closeAllWindows();

		if (ticks < 0) { // isntant hit, can only run -1 if already inside the combat cycle event
			dealTrueDamageInstant(damage, gfx, attacker);
		} else {
			CycleEventHandler.getSingleton().addEvent(this, true, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (disconnected || isDead() || forceDisconnect || isDead() || getHitPoints() < 1) {
						container.stop();
						return;
					}

					if (teleTimer > 0) {
						if (container.getTick() > 1) {
							container.setTick(1);
						}
						return;
					}

					dealTrueDamageInstant(damage, gfx, attacker);

					container.stop();
				}

				@Override
				public void stop() {

				}

			}, ticks);
		}
		return damage;
	}
	
	private void dealTrueDamageInstant(int damage, final Graphic gfx, final Mob attacker) {
		if (gfx != null) {
			startGraphic(gfx);
		}

		dealDamage(new Hit(damage, 0, -1, attacker));

		if (attacker != null) {
			if (attacker.isNpc()) {
				final NPC npc = attacker.toNpcFast();
				if (!npc.isDead()) {
					if (npc.isVenomousNpc()) {
						getDotManager().applyVenom();
					}

					getDamageMap().incrementTotalDamage(npc.getNpcIndex(), damage);
					putInCombatWithNpc(npc.npcIndex);
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							npcIndex = npc.npcIndex;
						}
					}
					if (npc.isUsingSoulSplit()) {
						applySoulSplitFromNpc(npc, damage);
					}
				}

			} else if (attacker.isPlayer()) {
				final Player c2 = attacker.toPlayerFast();
				if (!c2.isDead()) {
					getDamageMap().incrementTotalDamage(c2.mySQLIndex, damage);
					underAttackBy = c2.getId();
					if (playerIndex <= 0 && npcIndex <= 0) {
						if (autoRet == 1 && !walkingToItem && clickObjectType <= 0) {
							playerIndex = c2.getId();
						}
					}
				}
			}
		}
	}

	public int decreaseHP(int amount) {
		return getSkills().hit(amount);
	}

	public boolean destinationReached() {
		return getXorTeleX()/*- getMapRegionX() * 8*/ == finalLocalDestX && getYorTeleY() /*- getMapRegionY() * 8*/ == finalLocalDestY
				&& walkingToObject;
	}
	
	public boolean destinationReachedAny() {
		return absX == finalLocalDestX && absY == finalLocalDestY;
	}
	
	private Location lastNpcLocation = Location.create(0, 0, 0);
	
	public Location getLastNpcLocation() {
		return this.lastNpcLocation;
	}

	/**
	 * Destroys this entity.
	 */
	public void destroy() {
		removeFromRegion(currentRegion);
	}

	void destruct() {
		// if (underAttackBy > 0 || underAttackBy2 > 0)
		// return;
		if (currentRegion != null) {
			destroy();
		}
		if (getBot() != null) {
			getBot().disconnect();
		}
		// playerListSize = 0;
		// for (int i = 0; i < maxPlayerListSize; i++)
		// playerList[i] = null;
		absX = absY = -1;
		mapRegionX = mapRegionY = -1;
		currentX = currentY = 0;
		// if (penguinProfile != null)
		// penguinProfile.logout();
		// penguinProfile = null;
		// if (scheduler != null) {
		// scheduler.terminate();
		// }
		movement.hardResetWalkingQueue();
		// if (correctlyInitialized)
		if (Server.getPanel() != null) {
			Server.getPanel().removeEntity(playerName);
		}
		CycleEventHandler.getSingleton().stopEvents(this);

		setIsActiveAtomicBoolean(false);
	}

	public int distanceToPoint(int pointX, int pointY) {
		// return (int) Math.sqrt(Math.pow(absX - pointX, 2) + Math.pow(absY -
		// pointY, 2));

		int dx = Math.abs(pointX - getX());
		int dy = Math.abs(pointY - getY());

		int min = Math.min(dx, dy);
		int max = Math.max(dx, dy);

		int diagonalSteps = min;
		int straightSteps = max - min;

		return (int) (Math.sqrt(2) * diagonalSteps + straightSteps);
	}

	public boolean duelFightArenas() {
		if (absX > 3330 && absX < 3391 && absY > 3204 && absY < 3260) {
			return true;
		}
		return false;
	}

	public boolean fullVoidMage() {
		if (playerEquipment[PlayerConstants.playerHat] != 11663) {
			return false;
		}

		int voidCount = 1;

		switch(playerEquipment[PlayerConstants.playerLegs]) {
			case 8840:
			case 19786:
			case 19788:
			case 19790:
				voidCount++;
				break;
		}

		switch(playerEquipment[PlayerConstants.playerChest]) {
			case 8839:
			case 19785:
			case 19787:
			case 19789:
				voidCount++;
				break;
		}

		if (playerEquipment[PlayerConstants.playerHands] == 8842) {
			voidCount++;
		}

		if (playerEquipment[PlayerConstants.playerShield] == 19712) {
			voidCount++;
		}

		if (voidCount >= 4) {
			return true;
		}

		return false;
	}

	public boolean fullVoidMelee() {
		if (playerEquipment[PlayerConstants.playerHat] != 11665) {
			return false;
		}

		int voidCount = 1;

		switch(playerEquipment[PlayerConstants.playerLegs]) {
			case 8840:
			case 19786:
			case 19788:
			case 19790:
				voidCount++;
				break;
		}

		switch(playerEquipment[PlayerConstants.playerChest]) {
			case 8839:
			case 19785:
			case 19787:
			case 19789:
				voidCount++;
				break;
		}

		if (playerEquipment[PlayerConstants.playerHands] == 8842) {
			voidCount++;
		}

		if (playerEquipment[PlayerConstants.playerShield] == 19712) {
			voidCount++;
		}

		if (voidCount >= 4) {
			return true;
		}

		return false;
	}

	public boolean fullVoidRange() {
		if (playerEquipment[PlayerConstants.playerHat] != 11664) {
			return false;
		}

		int voidCount = 1;

		switch(playerEquipment[PlayerConstants.playerLegs]) {
			case 8840:
			case 19786:
			case 19788:
			case 19790:
				voidCount++;
				break;
		}

		switch(playerEquipment[PlayerConstants.playerChest]) {
			case 8839:
			case 19785:
			case 19787:
			case 19789:
				voidCount++;
				break;
		}

		if (playerEquipment[PlayerConstants.playerHands] == 8842) {
			voidCount++;
		}

		if (playerEquipment[PlayerConstants.playerShield] == 19712) {
			voidCount++;
		}

		if (voidCount >= 4) {
			return true;
		}

		return false;
	}

	public boolean getAchievBoolean(int index) {
		return achievements[index] == 1;
	}

	public int getAchievInt(int index) {
		return achievements[index];
	}

	public int getAgilityPoints() {
		return agilityPoints;
	}

	public int[] getBankPins() {
		return bankPins;
	}

	public int[] getBossKills() {
		return bossKills;
	}

	public int getBossKills(int index) {
		return bossKills[index];
	}

	public BotClient getBot() {
		if (playerOwnedBot == null) {
			// sendMessage("You haven't spawned a bot yet.");
			return null;
		}
		return playerOwnedBot;
	}

	public int getChatAbuse() {
		return judgement[JudgementHandler.CHATABUSE.ordinal()];
	}

	// public long addItemLength = 0;

	public int[] getChatSettings() {
		return chatSettings;
	}

	public byte[] getChatText() {
		return chatText;
	}

	public int getChatTextColor() {
		return chatTextColor;
	}

	public int getChatTextEffects() {
		return chatTextEffects;
	}

	public byte getChatTextSize() {
		return chatTextSize;
	}

	public Clan getClan() {
		return clan;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
		getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

	public int getClanChatSetting() {
		return chatSettings[ChatSettings.CLAN_INDEX];
	}

	public ArrayList<GameItem> getClanWarPlayerStake() {
		return clanWarPlayerStake;
	}
	
	public boolean isPlaceholderEnabled() {
		return Variables.PLACEHOLDER.getValue(this) == 1;
	}
	
	public void togglePlaceholder() {
		variables[Variables.PLACEHOLDER.getCode()] = (variables[Variables.PLACEHOLDER.getCode()] + 1) % 2;
	}

	public int getClueAmount(int type) {
		if (TreasureTrails.LOW_CLUE == type) {
			return getExtraHiscores().getClueEasyCompleted();
		}
		if (TreasureTrails.MED_CLUE == type) {
			return getExtraHiscores().getClueMediumCompleted();
		}
		if (TreasureTrails.HIGH_CLUE == type) {
			return getExtraHiscores().getClueHardCompleted();
		}
		return 0;
	}

	public void toggleAnnouncements() {
		variables[Variables.ANNOUNCEMENT_MUTE.ordinal()] = 1 - variables[Variables.ANNOUNCEMENT_MUTE.ordinal()];
	}
	
	public void toggleBreakVials() {
		variables[Variables.BREAK_VIALS_TOGGLE.ordinal()] = 1 - variables[Variables.BREAK_VIALS_TOGGLE.ordinal()];
		sendMessage("You have "+ (isBreakVials() ? "enabled" : "disabled") + " vial break.");
	}

	public void addLawStaffCharges(int amount) {
		variables[Variables.STAFF_OF_LAW_CHARGES.ordinal()] += amount;
		updateLawStaffCharges();
	}

	public void removeLawStaffCharge() {
		variables[Variables.STAFF_OF_LAW_CHARGES.ordinal()]--;
		updateLawStaffCharges();
	}

	public void emptyLawStaffCharges() {
		variables[Variables.STAFF_OF_LAW_CHARGES.ordinal()] = 0;
		updateLawStaffCharges();
	}

	public void updateLawStaffCharges() {
		getPacketSender().sendConfig(ConfigCodes.LAW_STAFF_CHARGE_COUNT, variables[Variables.STAFF_OF_LAW_CHARGES.ordinal()]);
	}

	public int getLawStaffCharges() {
		return variables[Variables.STAFF_OF_LAW_CHARGES.ordinal()];
	}

	public void addNatureStaffCharges(int amount) {
		variables[Variables.STAFF_OF_NATURE_CHARGES.ordinal()] += amount;
		updateNatureStaffCharges();
	}

	public void removeNatureStaffCharge() {
		variables[Variables.STAFF_OF_NATURE_CHARGES.ordinal()]--;
		updateNatureStaffCharges();
	}

	public void emptyNatureStaffCharges() {
		variables[Variables.STAFF_OF_NATURE_CHARGES.ordinal()] = 0;
		updateNatureStaffCharges();
	}

	public void updateNatureStaffCharges() {
		getPacketSender().sendConfig(ConfigCodes.NATURE_STAFF_CHARGE_COUNT, variables[Variables.STAFF_OF_NATURE_CHARGES.ordinal()]);
	}

	public int getNatureStaffCharges() {
		return variables[Variables.STAFF_OF_NATURE_CHARGES.ordinal()];
	}

	public boolean isBreakVials() {
		return variables[Variables.BREAK_VIALS_TOGGLE.ordinal()] == 1;
	}
	
	public int getEarningPotential() {
		return variables[Variables.EARNING_POTENTIAL.getCode()];
	}

	public void increaseEP() {
		variables[Variables.EARNING_POTENTIAL.getCode()] = Math.min(100, variables[Variables.EARNING_POTENTIAL.getCode()] + 10);
	}
	
	public void decreaseEP() {
		variables[Variables.EARNING_POTENTIAL.getCode()] = Math.max(0, variables[Variables.EARNING_POTENTIAL.getCode()] - 10);
	}
	
	public boolean getCmbExpLock() {
		return getVariables()[Variables.TOGGLE_CMB_EXP.ordinal()] == 1;
	}

	public abstract CombatAssistant getCombat();
	
	public int getCWWaitingRoomCountDown() {
		return waitRoomCountDown;
	}

	public DamageMap getDamageMap() {
		return damageMap;
	}

	public int getDonatedTotalAmount() {
		return donatedTotalAmount;
	}

	public int getDonatedRealTotalAmount() {
		return donatedRealTotalAmount;
	}

	public int getDonPoints() {
		return donPoints;
	}
	
	public int getDominionTokens() {
		return dominionTokens;
	}

	public boolean pkpBoostActive() {
		return timers[Timers.PKP_BOOST.ordinal()] > 0;
	}

	public int getPkpBoostLength() {
		return timers[Timers.PKP_BOOST.ordinal()];
	}

	public boolean dropRateBoostActive() {
		return timers[Timers.DROP_RATE_BOOST.ordinal()] > 0;
	}

	public int getDropRateBoostLength() {
		return timers[Timers.DROP_RATE_BOOST.ordinal()];
	}

	public int getDoubleExpLength() {
		return timers[Timers.DOUBLE_EXP_LENGTH.ordinal()];
	}
	
	public int getPetDoubleExpLength() {
		return timers[Timers.PET_DOUBLE_EXP_LENGTH.ordinal()];
	}

	public void setTomeExpLength(int ticks) {
		timers[Timers.TOME_EXTRA_XP_LENGTH.ordinal()] = ticks;
	}

	public int getTomeExpLength() {
		return timers[Timers.TOME_EXTRA_XP_LENGTH.ordinal()];
	}

	public void decreaseTomeExpBoost() {
		if (timers[Timers.TOME_EXTRA_XP_LENGTH.ordinal()] > 0) {
			timers[Timers.TOME_EXTRA_XP_LENGTH.ordinal()] -= 1;
			if (timers[Timers.TOME_EXTRA_XP_LENGTH.ordinal()] == 1) {
				Variables.TOME_EXTRA_XP_MOD.setValue(this, 0);
				getPacketSender().sendWidget(DisplayTimerType.TOME_OF_XP50, 0);
				getPacketSender().sendWidget(DisplayTimerType.TOME_OF_XP100, 0);
				getPacketSender().sendWidget(DisplayTimerType.TOME_OF_XP150, 0);
				if (!disconnected)
					sendMessage("Your Tome xp boost has expired.");
			}
		}
	}

	public void setPetDoubleExp(int ticks) {
		timers[Timers.PET_DOUBLE_EXP_LENGTH.ordinal()] = ticks;
	}
	
	public void decreasePetDoubleExp() {
		if (timers[Timers.PET_DOUBLE_EXP_LENGTH.ordinal()] > 0) {
			timers[Timers.PET_DOUBLE_EXP_LENGTH.ordinal()] -= 1;
			if (timers[Timers.PET_DOUBLE_EXP_LENGTH.ordinal()] == 1) {
				if (!disconnected)
					sendMessage("Your Pet Double Exp has expired.");
			}
		}
	}

	public int getDuelDC() {
		return getVariables()[Variables.DUEL_DC.getCode()];
	}

	public Client getDuelingClanWarClient() {
		return duelingClientClanWar;
	}

	public Client getDuelingClient() {
		return duelingClient;
	}

	public int getDuelKC() {
		return getVariables()[Variables.DUEL_KC.getCode()];
	}

	public int getDuelRuleTimer() {
		return duelRuleTimer;
	}

	public int getDuelStreak() {
		return getVariables()[Variables.DUEL_STREAK.getCode()];
	}

	public int getDungHeight4() {
		return getId() * 4;
	}

	public int getDungHeight5() {
		return getId() * 5;
	}

	public Point getDuoPoints() {
		return duoPoints;
	}

	public int getEffigy() {
		return getVariables()[Variables.EFFIGY.getCode()];
	}

	public int getEggTimer() {
		return getVariables()[Variables.EGG_INCUBATOR_TIME.getCode()];
	}

	public int getBlowpipeAmmoType() {
		return getVariables()[Variables.TOXIC_BLOWPIPE_AMMO_TYPE.getCode()];
	}

	public void setBlowpipeAmmoType(int itemId) {
		getVariables()[Variables.TOXIC_BLOWPIPE_AMMO_TYPE.getCode()] = itemId;
	}

	public int getBlowpipeAmmo() {
		return getVariables()[Variables.TOXIC_BLOWPIPE_AMMO.getCode()];
	}

	public void setBlowpipeAmmo(int amount) {
		getVariables()[Variables.TOXIC_BLOWPIPE_AMMO.getCode()] = amount;
	}

	public int getEloRating() {
		return eloRating;
	}

	public int getFightPitPoints() {
		return this.variables[Variables.FIGHT_PITS_POINTS.getCode()];
	}

	/**
	 * new method for friends used by login server
	 */
	public long[] getFriends() {
		return friends;
	}

	public long getGwdKillCount() {
		return killCount;
	}

	public int getHeightLevel() {
		return heightLevel;
	}

	public int getHitPoints() {
		return getSkills().getLifepoints();
	}

	public long[] getIgnores() {
		return ignores;
	}

	public int getIncubatorEggId() {
		return getVariables()[Variables.EGG_IN_INCUBATOR.getCode()];
	}

	public abstract ItemAssistant getItems();
	
	public abstract ShopAssistant getShops();

	public int getJailTime() {
		return this.variables[Variables.JAIL_TIME.getCode()];
	}
	
	public boolean isSemiAdmin() {
		return this.variables[Variables.SEMI_ADMIN_RANK.getCode()] != 0;
	}
	
	public boolean isYellMuted() {
		return this.variables[Variables.YELL_MUTE.getCode()] != 0;
	}

	public int[] getJudgement() {
		return judgement;
	}

	public int getLastClicked() {
		return lastClicked;
	}

	public int getLastX() {
		return lastX;
	}

	public int getLastY() {
		return lastY;
	}

	public int getLevelForXP2(int exp) { // USED FOR DUNGEONEERING LVL or
											// anything above 99
		int points = 0;
		int output = 0;

		for (int lvl = 1; lvl <= 99; lvl++) {
			points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
			output = (int) Math.floor(points / 4);
			if (output >= exp) {
				return lvl;
			}
		}
		return 99;
	}

	/**
	 * Gets the localPlayers.
	 *
	 * @return The localPlayers.
	 */
	public List<Player> getLocalPlayers() {
		return localPlayers;
	}

	public int getLocalX() {
		return getX() - 8 * getMapRegionX();
	}

	public int getLocalY() {
		return getY() - 8 * getMapRegionY();
	}

	public long getLongName() {
		return longName;
	}

	public long getLongDisplayName() {
		return longDisplayName;
	}

	public long getLongNameSmart() {
		if (longDisplayName > 0) {
			return longDisplayName;
		}

		return longName;
	}
	
	public int getTaiBwoFavour() {
		return getVariables()[Variables.TAI_BWO_FAVOUR.getCode()];
	}
	
	public int getTaiBwoCleanup() {
		return getVariables()[Variables.TAI_BWO_CLEANUP.getCode()];
	}
	
	public int getLmsCoffer() {
		return getVariables()[Variables.LMS_COFFER.getCode()];
	}
	
	public int getChristmasPoints() {
		return getVariables()[Variables.CHRISTMAS_POINTS.getCode()];
	}
	
	public int getExtraNameChange() {
		return getVariables()[Variables.NAME_CHANGE_POINTS.getCode()];
	}
	
	public int getNameChangeDate() {
		return getVariables()[Variables.NAME_CHANGE_DATE.getCode()];
	}
	
	public void saveNameChangedDate() {
		this.getVariables()[Variables.NAME_CHANGE_DATE.getCode()] = Server.getCalendar().getDayOfYear();
	}

	public int getBorkKillDate() {
		return getVariables()[Variables.BORK_KILL_DATE.getCode()];
	}

	public void saveBorkKillDate() {
		this.getVariables()[Variables.BORK_KILL_DATE.getCode()] = SoulPlayDate.getDate();
	}

	public int getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public int getMapRegionX() {
		return mapRegionX;
	}

	public int getMapRegionY() {
		return mapRegionY;
	}
	
	public int getRenderedMapRegionX(int absX) {
		return absX - 8 * getMapRegionX();
	}
	
	public int getRenderedMapRegionY(int absY) {
		return absY - 8 * getMapRegionY();
	}
	
	public int getMapRegionX1() {
		return (absX >> 3) - 6;
	}
	
	public int getMapRegionY1() {
		return (absY >> 3) - 6;
	}

	public long getMoneyPouch() {
		return moneyPouch;
	}

	public String getName() {
		return this.playerName;
	}
	
	public String getNameSmart() {
		if (displayName != null && !displayName.isEmpty()) {
			return displayName;
		}

		return playerName;
	}

	public String getNameSmartUp() {
		return Misc.formatName(getNameSmart());
	}

	public String getDisplayName() {
		return displayName;
	}

	// public int getHeightLevel;

	public int getNpcAggroTick() {
		return npcAggroTick;
	}

	public abstract PlayerAssistant getPA();

	public abstract PacketSender getPacketSender();

	public Client getPartner() {
		return partner;
	}

	public Patch getPatch(final int index) {
		return this.patches[index];
	}

	public Patch[] getPatches() {
		if (this.patches == null || this.patches.length < 16) {
			this.patches = new Patch[16];
			for (int i = 0; i < this.patches.length; i++) {
				this.patches[i] = new Patch();
			}
		}
		return this.patches;
	}

	public int getPcPoints() {
		return pcPoints;
	}

	public PartyRequest getPendingRequest() {
		return pendingRequest;
	}

	public boolean getPlayerFarmingState() {
		return this.playerIsFarming;
	}



	public int getZeals() {
		return zeals;
	}

	public int getPoisonDamage() {
		return getVariables()[Variables.POISON_DAMAGE.getCode()];
	}

	public int getPouchData(int i) {
		return pouchData[i];
	}

	public int getPrivateChatSetting() {
		return chatSettings[ChatSettings.PRIVATE_INDEX];
	}

	public int getPublicChatSetting() {
		return chatSettings[ChatSettings.PUBLIC_INDEX];
	}

	public int getQueuedAnimation() {
		return this.queuedAnimation;
	}

	public boolean[] getReceivedPacket() {
		return receivedPacket;
	}

	public Region getRegion() {
		return currentRegion;
	}
	
	private TickTimer staminaTick = new TickTimer();
	
	public void startStaminaTimer() {
		int ticks = Misc.minutesToTicks(2);
		staminaTick.startTimer(ticks);
		getPacketSender().sendWidget(DisplayTimerType.STAMINA, ticks);
	}

	public boolean staminaPotRunning() {
		return !staminaTick.complete();
	}
	
	private double weight = 0.0D;
	
	private int totalBonuses = 0;

	public double getWeight() {
		return weight;
	}
	
	public double getWeight64() {
		return Math.min(64.0, Math.max(0.0, weight));
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getTotalBonuses() {
		return totalBonuses;
	}

	public void setTotalBonuses(int totalBonuses) {
		this.totalBonuses = totalBonuses;
	}
	
	public boolean restoreEnergy = false;
	
	private double runEnergy = 100.0D;
	
	public double getRunEnergy() {
		return runEnergy;
	}
	
	public void setRunEnergy(double energy) {
		this.runEnergy = Math.min(100.0D, Math.max(0.0D, energy));
		this.restoreEnergy = true;
	}

	public int getRunEnergyForSave() {
		return getVariables()[Variables.RUN_ENERGY.getCode()];
	}

	public void setRunEnergyForSave(int runEnergy) {
		this.variables[Variables.RUN_ENERGY.ordinal()] = runEnergy;
	}

	public boolean getSavedGame() {
		if (savedGame.get()) {
			return true;
		} else {
			return false;
		}
	}

	public int[] getSeasonalElo() {
		return seasonalElo;
	}

	public int getSetCrown() {
		return this.variables[Variables.SET_CROWN.getCode()];
	}

	public BotClient getShopBot() {
		if (playerOwnedShopBot == null) {
			// sendMessage("You haven't spawned a bot yet.");
			return null;
		}
		return playerOwnedShopBot;
	}

	public int getSkullTimer() {
		return timers[Timers.SKULL_TIMER.ordinal()];
	}

	public Point getSlayerPoints() {
		return slayerPoints;
	}
	
	public int getBountyTargetSpell() {
		return this.variables[Variables.BOUNTY_TARGET_SPELL_UNLOCK.getCode()];
	}

	public int getSlayerScrolls() {
		return this.variables[Variables.SLAYER_SCROLLS.getCode()];
	}

	public SlayerTask getSlayerTask() {
		return slayerTask;
	}

	public String getSpellName(int id) {
		switch (id) {
		case 0:
			return "Air Strike";
		case 1:
			return "Water Strike";
		case 2:
			return "Earth Strike";
		case 3:
			return "Fire Strike";
		case 4:
			return "Air Bolt";
		case 5:
			return "Water Bolt";
		case 6:
			return "Earth Bolt";
		case 7:
			return "Fire Bolt";
		case 8:
			return "Air Blast";
		case 9:
			return "Water Blast";
		case 10:
			return "Earth Blast";
		case 11:
			return "Fire Blast";
		case 12:
			return "Air Wave";
		case 13:
			return "Water Wave";
		case 14:
			return "Earth Wave";
		case 15:
			return "Fire Wave";
		case 32:
			return "Shadow Rush";
		case 33:
			return "Smoke Rush";
		case 34:
			return "Blood Rush";
		case 35:
			return "Ice Rush";
		case 36:
			return "Shadow Burst";
		case 37:
			return "Smoke Burst";
		case 38:
			return "Blood Burst";
		case 39:
			return "Ice Burst";
		case 40:
			return "Shadow Blitz";
		case 41:
			return "Smoke Blitz";
		case 42:
			return "Blood Blitz";
		case 43:
			return "Ice Blitz";
		case 44:
			return "Shadow Barrage";
		case 45:
			return "Smoke Barrage";
		case 46:
			return "Blood Barrage";
		case 47:
			return "Ice Barrage";
		default:
			return "Select Spell";
		}
	}

	public int applyDamageReduction(int damage, CombatType combatType) {
		if (damage < 1) {
			return 0;
		}

		boolean powerUpProc = false;
		int shieldId = playerEquipment[PlayerConstants.playerShield];
		if (getSeasonalData().containsPowerUp(PowerUpData.IRONMAN) && rangeWeapon == null && ItemConstants.isShield(shieldId)) {
			damage = (int) Math.round(damage * 0.80);
			powerUpProc = true;
			sendMessageSpam("Your Ironman skin reduces incoming damage!");
			startGraphic(Graphic.create(2320));
		}

		if (!powerUpProc) {
			switch (shieldId) {
				case 13740:
					if (getSpiritShieldHits() <= 0) {
						final int prayerLevel = getSkills().getLevel(Skills.PRAYER);
						if (prayerLevel > 0) {
							final int damageRecieved = (int) Math.floor(damage * 0.70);
							final int prayerLost = (int) Math.floor(damage * 0.06);
							if (prayerLevel >= prayerLost) {
								damage = damageRecieved;
								CombatPrayer.drainPrayerByHit(this, damageRecieved); //getSkills().decrementPrayerPoints(prayerLost);
							}
						}
					}
					break;
				case 13742:
					if (Misc.random(99) <= 59) {
						damage = (int) Math.round(damage * 0.75); // damage *= 0.75;
					}
					break;
			}
		}

		switch (playerEquipment[PlayerConstants.playerWeapon]) {
			case DihnBulwarkPlugin.ITEM_ID:
				if (bulwarkDelay.complete() && getWeaponAttackType() == WeaponAttackTypes.DEFENSIVE) {
					damage = (int) Math.round(damage * 0.80);
				}
				break;
		}

		if (playerEquipment[PlayerConstants.playerHat] == 122326 && playerEquipment[PlayerConstants.playerChest] == 122327 && playerEquipment[PlayerConstants.playerLegs] == 122328 && isInCombatWithNpc()) {
			int bonus = 0;
			if (combatType != null) {
				switch (combatType) {
					case MELEE:
						bonus = Math.max(Math.max(getPlayerBonus()[EquipmentBonuses.DEFENSE_STAB.getId()], getPlayerBonus()[EquipmentBonuses.DEFENSE_SLASH.getId()]), getPlayerBonus()[EquipmentBonuses.DEFENSE_CRUSH.getId()]);
						break;
					case RANGED:
						bonus = getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()];
						break;
					case MAGIC:
						bonus = getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()];
						break;
				}
			}

			if (bonus > 0) {
				int reductionBonus = bonus * 100 / 3000;
				int percent = 100 - reductionBonus;
				damage = damage * percent / 100;
			}
		}

		return damage;
	}

	public int getSpiritShieldHits() {
		return timers[Timers.DISABLE_BLESSED_SPIRIT_SHIELD.ordinal()];
	}
	
	public int getMinigameBanTimer() {
		return timers[Timers.MINIGAME_BAN.ordinal()];
	}
	
	public void setMinigameBanTimer(int ticks) {
		timers[Timers.MINIGAME_BAN.ordinal()] = ticks;
	}

	public int getImbuedHeartTimer() {
		return timers[Timers.IMBUED_HEART.ordinal()];
	}

	public void setImbuedHeartTimer(int ticks) {
		timers[Timers.IMBUED_HEART.ordinal()] = ticks;
	}

	public int getStrongholdLevel() {
		return getVariables()[Variables.STRONGHOLD_LEVEL.getCode()];
	}
	
	public int getMinigamePunishmentPoints() {
		return getVariables()[Variables.MINIGAME_PUNISHMENTS.getCode()];
	}
	
	public void setMinigamePunishmentPoints(int points) {
		getVariables()[Variables.MINIGAME_PUNISHMENTS.getCode()] = points;
	}
	
	public boolean playerHasHouse() {
		return getVariables()[Variables.BOUGHT_HOUSE.getCode()] == 1;
	}
	
	public void setPlayerHasHouse(boolean owningHome) {
		getVariables()[Variables.BOUGHT_HOUSE.getCode()] = owningHome ? 1 : 0;
	}

	public NPC getSummonedNPC() {
		return summoned;
	}

	public int getSummoningOrbSetting() {
		return this.variables[Variables.SUMMONING_ORB_OPTION.getCode()];
	}

	public int getSlayerTasksCompleted() {
		return slayerTasksCompleted;
	}

	public int getTeleBlockTimer() {
		return timers[Timers.TELEBLOCK_TIMER.ordinal()];
	}

	public int getVenomDamage() {
		return timers[Timers.VENOM.ordinal()];
	}

	public void setVenomDamage(int damage) {
		this.timers[Timers.VENOM.ordinal()] = damage;
		getPacketSender().sendHpOrbState();
	}

	public TempVar getTempVar() {
		return tempVar;
	}

	public int getTimedMute() {
		return timers[Timers.MUTE_LENGTH.ordinal()];
	}

	public int[] getTimers() {
		return timers;
	}

	public boolean getTitleBoolean(int index) {
		return titles[index] == 1;
	}

	public int getTitleInt(int index) {
		return titles[index];
	}

	public long getTotalGold() {
		long gold = 0;
		// int money = 995;
		// int money2 = 996;

		// for (int i = 0; i < bankItems0.length; i++)
		// if ((bankItems0[i] == money) || (bankItems0[i] == money2))
		// gold += bankItems0N[i];
		// for (int i = 0; i < bankItems1.length; i++)
		// if ((bankItems1[i] == money) || (bankItems1[i] == money2))
		// gold += bankItems1N[i];
		// for (int i = 0; i < bankItems2.length; i++)
		// if ((bankItems2[i] == money) || (bankItems2[i] == money2))
		// gold += bankItems2N[i];
		// for (int i = 0; i < bankItems3.length; i++)
		// if ((bankItems3[i] == money) || (bankItems3[i] == money2))
		// gold += bankItems3N[i];
		// for (int i = 0; i < bankItems4.length; i++)
		// if ((bankItems4[i] == money) || (bankItems4[i] == money2))
		// gold += bankItems4N[i];
		// for (int i = 0; i < bankItems5.length; i++)
		// if ((bankItems5[i] == money) || (bankItems5[i] == money2))
		// gold += bankItems5N[i];
		// for (int i = 0; i < bankItems6.length; i++)
		// if ((bankItems6[i] == money) || (bankItems6[i] == money2))
		// gold += bankItems7N[i];
		// for (int i = 0; i < bankItems7.length; i++)
		// if ((bankItems7[i] == money) || (bankItems7[i] == money2))
		// gold += bankItems0N[i];
		// for (int i = 0; i < bankItems8.length; i++)
		// if ((bankItems8[i] == money) || (bankItems8[i] == money2))
		// gold += bankItems8N[i];
		// for (int i = 0; i < playerItems.length; i++)
		// if ((playerItems[i] == money) || (playerItems[i] == money2))
		// gold += playerItemsN[i];

		if (WorldType.equalsType(WorldType.SPAWN)) {
			gold += getItems().getItemAmount(8890);
			gold += getItems().getBankItemCount(8890);
		} else {
			gold += getItems().getItemAmount(995);
			gold += getItems().getBankItemCount(995);

			gold += (long) getItems().getItemAmount(10832) * 500000000; // 0.5b
																		// bag
			gold += (long) getItems().getItemAmount(10833) * 1000000000; // 1b
																			// bag
			gold += (long) getItems().getItemAmount(10834) * 1500000000; // 1.5b
																			// bag
			gold += (long) getItems().getItemAmount(10835) * 2000000000; // 2b
																			// bag
			//
			gold += (long) getItems().getBankItemCount(10832) * 500000000;
			gold += (long) getItems().getBankItemCount(10833) * 1000000000;
			gold += (long) getItems().getBankItemCount(10834) * 1500000000;
			gold += (long) getItems().getBankItemCount(10835) * 2000000000;
		}

		return gold;
	}

	public int getTradeChatSetting() {
		return chatSettings[ChatSettings.TRADE_INDEX];
	}

	public int[] getUnlockedCrowns() {
		return unlockedCrowns;
	}

	public int getUntradeableGoldToDrop() {
		return getVariables()[Variables.UNTRADEABLE_GOLD.getCode()];
	}

	public Map<Integer, Integer> getUntradeableShopMap() {
		return untradeableShopMap;
	}

	public boolean[] getUpdateEquipment() {
		return updateEquipment;
	}

	public int getUpdateInvInterface() {
		return updateInvInterface;
	}

	public int[] getVariables() {
		return variables;
	}

	public int getWaitingRoomCampTimer() {
		return waitingRoomCampTimer;
	}
	
	public void increaseQuestPoints(int amount) {
		this.variables[Variables.QUEST_POINTS.getCode()] = Variables.QUEST_POINTS.getValue(this) + amount;
	}
	
	public void increaseRfdProgress() {
		this.variables[Variables.RFD_PROGRESS.getCode()] += 1;
		if (Variables.RFD_PROGRESS.getValue(this) > RecipeForDisaster.MAX_STAGE)
			this.variables[Variables.RFD_PROGRESS.getCode()] = 0;
	}
	
	public int getRFDProgress() {
		return Variables.RFD_PROGRESS.getValue(this);
	}

	public int getWildernessTick() {
		return timers[Timers.WILDERNESS_TICK.ordinal()];
	}
	
	public int getXorTeleX() {
		if (teleportToX > 0)
			return teleportToX;
		return absX;
	}
	public int getYorTeleY() {
		if (teleportToY > 0)
			return teleportToY;
		return absY;
	}
	public int getZorTeleZ() {
		if (teleportToZ > 0)
			return teleportToZ;
		return heightLevel;
	}

	public int getX() {
		return absX;
	}

	public int getY() {
		return absY;
	}

	public int getZ() {
		return heightLevel;
	}

	public int getZarosKC() {
		return getVariables()[Variables.ZAROSKC.getCode()];
	}

	/**
	 * distance to npc
	 *
	 * @param npc
	 * @param distance
	 * @return
	 */
	public boolean goodDistance(NPC npc, int distance) {
		for (int sizeX = 0; sizeX < npc.getSize(); sizeX++) {
			for (int sizeY = 0; sizeY < npc.getSize(); sizeY++) {
				if (PlayerConstants.getDistance(npc.absX + sizeX, absX, npc.absY + sizeY, absY) <= distance) {
					return true;
				} else {
					continue;
				}
			}
		}
		return false;
	}

	public boolean gwdCoords() {
		// if (heightLevel == 2 || heightLevel == 0 || heightLevel == 1) {
		if (getX() >= 2800 && getX() <= 2950 && getY() >= 5200 && getY() <= 5400) {
			return true;
		}
		if (teleportToX >= 2800 && teleportToX <= 2950 && teleportToY >= 5200 && teleportToY <= 5400) {
			return true;
		}
		// }
		return false;
	}



	public boolean hasCough() {
		return hasCough;
	}

	public boolean hasFullAncientCeremonial() {
		if (playerEquipment[PlayerConstants.playerHat] != 20125) {
			return false;
		}
		if (playerEquipment[PlayerConstants.playerChest] != 20127) {
			return false;
		}
		if (playerEquipment[PlayerConstants.playerLegs] != 20129) {
			return false;
		}
		if (playerEquipment[PlayerConstants.playerHands] != 20131) {
			return false;
		}
		if (playerEquipment[PlayerConstants.playerFeet] != 20133) {
			return false;
		}
		return true;
	}

	public boolean hasFullTorva() {
		return playerEquipment[PlayerConstants.playerHat] == 20159
				&& playerEquipment[PlayerConstants.playerLegs] == 20167
				&& playerEquipment[PlayerConstants.playerChest] == 20163
				|| playerEquipment[PlayerConstants.playerHat] == 20147
						&& playerEquipment[PlayerConstants.playerLegs] == 20155
						&& playerEquipment[PlayerConstants.playerChest] == 20151
				|| playerEquipment[PlayerConstants.playerHat] == 20135
						&& playerEquipment[PlayerConstants.playerLegs] == 20143
						&& playerEquipment[PlayerConstants.playerChest] == 20139
				|| playerEquipment[PlayerConstants.playerHat] == 30266
						&& playerEquipment[PlayerConstants.playerLegs] == 30265
						&& playerEquipment[PlayerConstants.playerChest] == 30264;
	}

	public boolean hasHybridGear() {
		if (battleMageAmount() > 0 || tricksterAmount() > 0 || vanguardAmount() > 0) {
			return true;
		}
		for (int playerItem : playerItems) {
			if (isHybridPiece(playerItem - 1)) {
				return true;
			}
		}
		return false;
	}

	public void healHitPoints(int amount) {
		if (getSkills().getLevel(Skills.HITPOINTS) <= 0) {
			return;
		}
		// System.out.println("maxhP"+getLevelForXP(playerXP[3]));
		int max = calculateMaxLifePoints();
		if (getSkills().getLevel(Skills.HITPOINTS)
				+ amount >= max/*
								 * PlayerConstants . getLevelForXP (playerXP[3])
								 */) {
			getSkills().heal(max);/*
													 * PlayerConstants.
													 * getLevelForXP(playerXP[3]
													 * )
													 */;
			return;
		}
		getSkills().heal(amount);
	}

	public void healPrayer(int amount) {
		if (getSkills().getLevel(Skills.PRAYER) <= 0) {
			return;
		}

		getSkills().incrementPrayerPoints(amount);
	}

	/**
	 * displays hit a player and show hitmark too
	 *
	 * @param damage
	 *            - damage to display
	 * @param mask:
	 *            0 - red box 2 - green box
	 * @param icon:
	 *            -1:nothing 0 - melee 1 - range 2 - magic 3 - recoil 4 - dwarf
	 *            multicannon
	 */
	public void hitPlayer(final int damage, final int maskRed0Green2, final int icon, final int ticks) {
		CycleEventHandler.getSingleton().addEvent(this, true, new CycleEvent() { // adding
																					// a
																					// real
																					// delayed
																					// hit
																					// here

			@Override
			public void execute(CycleEventContainer container) {
				if (disconnected || isDead() || forceDisconnect || isDead() || getHitPoints() < 1) {
					container.stop();
					return;
				}

				if (teleTimer > 0) {
					if (container.getTick() > 1) {
						container.setTick(1);
					}
					return;
				}

				int damageToDeal = damage;

				damageToDeal = applyDamageReduction(damageToDeal, CombatType.list(icon));

				if (damageToDeal > getSkills().getLifepoints()) {
					damageToDeal = getSkills().getLifepoints();
				}

				damageToDeal = dealDamage(new Hit(damageToDeal, maskRed0Green2, icon));

				// damageToDealAfterCycle += damage; // let process deal the
				// damage
				// refreshSkill(3); // this is already inside dealdamage
				// function

				getPA().closeAllWindows();

				container.stop();
			}

			@Override
			public void stop() {

			}

		}, ticks);
	}

	public boolean inArea(int wX, int sY, int eX, int nY) {
		return absX >= wX && absX <= eX && absY >= sY && absY <= nY;
	}

	public boolean inBank() {
		return Area(3090, 3099, 3487, 3500) || Area(3089, 3090, 3492, 3498) || Area(3248, 3258, 3413, 3428)
				|| Area(3179, 3191, 3432, 3448) || Area(2944, 2948, 3365, 3374) || Area(2942, 2948, 3367, 3374)
				|| Area(2944, 2950, 3365, 3370) || Area(3008, 3019, 3352, 3359) || Area(3017, 3022, 3352, 3357)
				|| Area(3203, 3213, 3200, 3237) || Area(3212, 3215, 3200, 3235) || Area(3215, 3220, 3202, 3235)
				|| Area(3220, 3227, 3202, 3229) || Area(3227, 3230, 3208, 3226) || Area(3226, 3228, 3230, 3211)
				|| Area(3227, 3229, 3208, 3226);
	}
	
	public boolean inPetDungeon() {
		return RSConstants.petDungeon(getX(), getY());
	}

	public boolean inSoulWars() {
		return RSConstants.inSoulWars(getX(), getY());
	}

	public boolean inSoulWarsRedRoom() {
		return RSConstants.inSoulWarsRedRoom(getX(), getY());
	}

	public boolean inSoulWarsBlueRoom() {
		return RSConstants.inSoulWarsBlueRoom(getX(), getY());
	}

	public boolean inSoulWarsLobby() {
		return RSConstants.inSoulWarsLobby(getX(), getY());
	}
	
	public boolean inTaiBwoWannai() {
		return RSConstants.inTaiBwoWannai(getX(), getY());
	}
	
	public boolean inEdgeConstruction() {
		return RSConstants.inEdgeConstruction(getX(), getY());
	}
	
	public boolean inLMSLobby() {
		return RSConstants.inLMSLobby(getX(), getY());
	}
	
	public boolean inLMSWaitingRoom() {
		return RSConstants.inLMSWaitingRoom(getX(), getY());
	}
	
	public boolean inLMSArenaCoords() {
		return RSConstants.inLMSArena(getX(), getY());
	}

	public boolean inBarrows() {
		if (absX > 3520 && absX < 3598 && absY > 9653 && absY < 9750) {
			return true;
		}
		return false;
	}

	public boolean inClanWars() {
		return RSConstants.clanWarsFightArea(getX(), getY());
	}

	public boolean inClanWarsDead() {
		if (absX > 3318 && absX < 3322 && absY > 3762 && absY < 3789) {
			return true;
		}
		return false;
	}

	public boolean inClanWarsFunPk() {
		return RSConstants.inClanWarsFunPk(getX(), getY());
	}

	public boolean inDaemonheim() {
		return getX() >= 3434 && getX() <= 3471 && getY() >= 3716 && getY() <= 3737;
	}

	/*
	 * protected void appendHitUpdate(Stream str) { // synchronized(this) {
	 * str.writeByte(getHitDiff()); // What the perseon got 'hit' for if
	 * (poisonMask == 1) { str.writeByteA(2); } else if (getHitDiff() > 0) {
	 * str.writeByteA(1); // 0: red hitting - 1: blue hitting } else {
	 * str.writeByteA(0); // 0: red hitting - 1: blue hitting } if
	 * (playerLevel[3] <= 0) { playerLevel[3] = 0; isDead = true; }
	 * str.writeByteC(playerLevel[3]); // Their current hp, for HP bar
	 * str.writeByte(getLevelForXP(playerXP[3])); // Their max hp, for HP bar }
	 * protected void appendHitUpdate2(Stream str) { // synchronized(this) {
	 * str.writeByte(hitDiff2); // What the perseon got 'hit' for if (poisonMask
	 * == 2) { str.writeByteS(2); poisonMask = -1; } else if (hitDiff2 > 0) {
	 * str.writeByteS(1); // 0: red hitting - 1: blue hitting } else {
	 * str.writeByteS(0); // 0: red hitting - 1: blue hitting } if
	 * (playerLevel[3] <= 0) { playerLevel[3] = 0; isDead = true; }
	 * str.writeByte(playerLevel[3]); // Their current hp, for HP bar
	 * str.writeByteC(getLevelForXP(playerXP[3])); // Their max hp, for HP bar }
	 */

	public boolean inClanWarsFunPkMulti() {
		return RSConstants.inClanWarsFunPkMulti(getX(), getY());
	}

	public boolean inClanWarsLobby() {
		return RSConstants.inClanWarsLobby(getX(), getY());
	}

	public boolean inCw() {
		if (inCwGame)
			return true;
		if (teleportToX > 0 && teleportToY > 0) {
			return RSConstants.inCastleWarsArena(teleportToX, teleportToY);
		}
		return RSConstants.inCastleWarsArena(getX(), getY());
	}

	public boolean inCwWait() {
		if (RSConstants.cwSaraWaitingRoom(getX(), getY())) {
			return true;
		}
		if (RSConstants.cwZammyWaitingRoom(getX(), getY())) {
			return true;
		}
		return false;
	}

	public boolean inSoulWait() {
		if (RSConstants.inSoulWarsBlueRoom(getX(), getY())) {
			return true;
		}
		if (RSConstants.inSoulWarsRedRoom(getX(), getY())) {
			return true;
		}
		return false;
	}

	public boolean inDuelArena() {
		if ((absX > 3327 && absX < 3394 && absY > 3195 && absY < 3291)) {
			return true;
		}
		return false;
	}

	public boolean inEasterEvent() {
		return RSConstants.inEasterEvent(getX(), getY());
	}

	public boolean inEdgeville() {
		if (absX >= 3070 && absX <= 3115 && absY >= 3463 && absY <= 3521) {
			return true;
		}
		return false;
	}

	public boolean inFallyPark() {
		if (absX >= 2980 && absX <= 3032 && absY >= 3366 && absY <= 3393) {
			return true;
		}
		return false;
	}

	public boolean inFightCaves() {
		return absX >= 2360 && absX <= 2445 && absY >= 5045 && absY <= 5125;
	}

	public boolean inFishingContestIsland() {
		return RSConstants.inFishingContestIsland(getX(), getY());
	}

	public boolean inFunPk() {
		return RSConstants.inFunPk(absX, absY);
	}

	public boolean inGiantMoleDung() {
		return RSConstants.inGiantMoleDung(getX(), getY());
	}

	public boolean inMarket() {
		if (Config.isPvpWorld())
			return false;
		return RSConstants.inGrandExchange(getX(), getY());
	}

	public boolean inMinigame() {
		if (infernoManager != null) {
			return true;
		}
		if (raidsManager != null)
			return true;

		if (insideInstance()) {
			return getInstanceManager().isMinigame();
		}

		if (insideDungeoneering()) {
			return true;
		}
		if (RSConstants.inCastleWarsArena(getX(), getY())) {
			return true;
		}
		if (RSConstants.inSoulWarsBlueRoom(getX(), getY()) || RSConstants.inSoulWarsRedRoom(getX(), getY())) {
			return true;
		}
		if (RSConstants.inSoulWars(getX(), getY())) {
			return true;
		}
		if (RSConstants.inFunPk(getX(), getY())) {
			return true;
		}
		if (RSConstants.fightPitsRoom(getX(), getY())) {
			return true;
		}
		if (inDuelArena() && duelStatus > 0) {
			return true;
		}
		if (inDuel) {
			return true;
		}
		if (inClanWars() || inClanWarsDead()) {
			return true;
		}
		if (inPcGame()) {
			return true;
		}
		if (inLMSArenaCoords() || inLMSWaitingRoom()) {
			return true;
		}
		if (getZones().isInDmmLobby() || getZones().isInDmmTourn())
			return true;
		return false;
	}

	public boolean inMulti() {
		return getZones().isInMulti();
	}
	
	public boolean inMultiZoneUpdateOnly() {
		if (insideDungeoneering()) {
			return true;
		}
		
		if (revenantDungeon()) {
			//north cave entrance signles zone
			if (getX() >= 9636 && getX() <= 9645 && getY() >= 10229 && getY() <= 10236)
				return false;
			return true;
		}

		if (insideInstance()) {
			return getInstanceManager().isMulti();
		}

		if (raidsManager != null || tobManager != null) {
			return true;
		}

		if (infernoManager != null) {
			return true;
		}

		if (inMultiDynZone) {
			return true;
		}
		
		if (RSConstants.vorkathFightArea(getX(), getY())) {
			return true;
		}

		if (clanWarRule[CWRules.SINGLE_COMBAT] && inClanWars()) {
			return false;
		} else if (!clanWarRule[CWRules.SINGLE_COMBAT] && inClanWars()) {
			return true;
		}
		return RSConstants.inMulti(absX, absY);
	}

	public boolean inNomadsEntrance() {
		return RSConstants.inNomadsEntrance(getX(), getY());
	}

	public boolean inNomadsRoom() {
		return RSConstants.inNomadsRoom(getX(), getY());
	}

	public boolean inPcBoat() {
		return absX >= 2660 && absX <= 2663 && absY >= 2638 && absY <= 2643;
	}

	public boolean inPcGame() {
		return absX >= 2624 && absX <= 2690 && absY >= 2550 && absY <= 2619;
	}

	public boolean inPirateHouse() {
		return absX >= 3038 && absX <= 3044 && absY >= 3949 && absY <= 3959;
	}

	public boolean inPkBox() {
		if (absX > 3343 && absX < 3384 && absY > 9619 && absY < 9660) {
			return true;
		}
		return false;
	}

	public boolean inPvpWorld() {
		return Config.isPvpWorld();
	}

	public boolean inPvpWorldDanger() {
		if (inPvpWorld() && !inPvpWorldSafe()) {
			return true;
		}
		return false;
	}

	public boolean inPvpWorldSafe() {
		if (Config.isPvpWorld()) {
			if ((absX > 3090 && absX < 3099 && absY > 3487 && absY < 3500) // edgeville
																			// bank
					|| (absX > 3089 && absX < 3091 && absY > 3493 && absY < 3497) // edgeville
																				// bank
					|| (absX > 3179 && absX < 3191 && absY > 3432 && absY < 3448) // varrock
																				// bank
					|| (absX > 3249 && absX < 4258 && absY > 3415 && absY < 3425) // varrock
																				// bank
					|| (absX >= 2725 && absX <= 2726 && absY >= 3487 && absY <= 3489) // cammy bank
					|| (absX >= 2721 && absX <= 2730 && absY >= 3490 && absY <= 3497) // cammy bank
					
					|| (absY == 3368 && absX >= 2943 && absX <= 2949) // fally west bank
					|| (absX >= 2943 && absX <= 2947 && absY >= 3369 && absY <= 3373) // fally west bank
					|| (absX >= 3009 && absX <= 3018 && absY >= 3353 && absY <= 3358) // fally east bank
					|| (absX >= 3088 && absX <= 3097 && absY >= 3240 && absY <= 3246) // draynor bank
					|| (absX >= 2609 && absX <= 2616 && absY >= 3088 && absY <= 3097) // yanille bank
					|| (absX >= 2097 && absX <= 2104 && absY >= 3917 && absY <= 3921) // shilo bank
					
					) {
				return true;
			}
			if (RSConstants.inGrandExchangeSafePvp(getX(), getY()) || RSConstants.inNeitzBank(getX(), getY())) {
				return true;
			}
		}
		return false;
	}

	public boolean inVarrock() {
		if (absX >= 3180 && absX <= 3270 && absY >= 3381 && absY <= 3501) {
			return true;
		}
		return false;
	}

	public boolean inWild() {
		if (inClanWars()) {
			return false;
		}
		if (revenantDungeon() || getZones().isInClwRedPortalPkZone()) {
			return true;
		}
		if (inWildDynZone)
			return true;
		// if(/*safeZone() && */safeTimer == 0)
		// return false;
		// if (safeTimer > 0)
		// return true;
		if (!Config.isPvpWorld()) {
			if (absX > 2941 && absX < 3392 && absY > 3521 && absY < 3969/* 3966 */
					|| absX > 2941 && absX < 3392 && absY > 9918 && absY < 10366) {
				return true;
			}
		} else if (Config.isPvpWorld()) {
			if (inPvpWorldSafe())
				return false;
			return true;
		}

		// if(CastleWars.isInCw((Client)this))
		// return true;

		return false;
	}

	public boolean isActiveAtomic() {
		return isActiveAtomicBoolean.get();
	}

	public boolean isFriend(Player other) {
		final long name = other.getLongName();
		for (long friend : friends) {
			if (name == friend) {
				return true;
			}
		}
		return false;
	}

	public boolean isIgnored(Player other) {
		final long name = other.getLongName();
		for (long ignore : ignores) {
			if (name == ignore) {
				return true;
			}
		}

		return false;
	}

	public boolean isAdmin() {
		if (playerRights == 2) {
			return true;
		}
		if (Config.isOwner(this)) {
			return true;
		}
		return false;
	}

	public boolean isAlive() {
		return !isDead();
	}

	public void clearDung() {
		resetSkillingEvent();
		getItems().deleteAllItems();
		getItems().addItem(DungeonConstants.RING_OF_KINSHIP, 1);
		getCombat().getPlayerAnimIndex(playerEquipment[PlayerConstants.playerWeapon]);
	}

	/**
	 * Gets if the player is a bot return if player is a bot
	 */
	public boolean isBot() {
		return false;
	}

	public boolean isBusy() {
		return this.performingAction;
	}

	public boolean isDetectNull() {
		return detectNull;
	}

	public boolean isDev() {
		return Config.isDev(getName());
	}

	public boolean isDisabled() {
		return disabled;
	}

	public boolean isHybridPiece(int itemId) {
		switch (itemId) {
		case 21467: // trickster helm
		case 21468: // trickster robe
		case 21469: // trickster robe legs
		case 21470: // trickster gloves
		case 21471: // trickster boots
		case 21547: // trickster helm
		case 21472: // vanguard helm
		case 21473: // vanguard body
		case 21474: // vanguard legs
		case 21475: // vanguard gloves
		case 21476: // vanguard boots
		case 21462: // battle mage helm
		case 21463: // battle mage robe
		case 21464: // battle mage robe legs
		case 21465: // battle mage gloves
		case 21466: // battle mage boots
			return true;
		}
		return false;
	}

	public boolean isInBarrows() {
		if (absX > 3543 && absX < 3584 && absY > 3265 && absY < 3311) {
			return true;
		}
		return false;
	}

	public boolean isInBarrows2() {
		if (absX > 3529 && absX < 3581 && absY > 9673 && absY < 9722) {
			return true;
		}
		return false;
	}

	public boolean isInGlacorLair() {
		return RSConstants.isInGlacorLair(getX(), getY());
	}

	public boolean isInJail() {
		if (absX >= 2065 && absX <= 2111 && absY >= 4415 && absY <= 4455) {
			return true;
		}
		return false;
	}

	public boolean isInNexRoom() {
		return RSConstants.isInNexRoom(getX(), getY());
	}

	/**
	 * SouthWest, NorthEast, SouthWest, NorthEast
	 */

	public boolean isInTut() {
		if (absX >= 2625 && absX <= 2687 && absY >= 4670 && absY <= 4735) {
			return true;
		}
		return false;
	}

	public boolean isMod() {
		if (playerRights == 1) {
			return true;
		}
		if (isAdmin()) {
			return true;
		}
		if (Config.isOwner(this)) {
			return true;
		}
		return false;
	}



	public boolean isMuted() {
		return muted;
	}
	
	public boolean isOwner() {
		if (playerRights == 3) {
			return true;
		}
		if (Config.isOwner(this)) {
			return true;
		}
		return false;
	}

	public boolean isSaveInProgressForDC() {
		return saveInProgressForDC;
	}

	public boolean isStaff() {
		if (playerRights > 0 && playerRights < 4) {
			return true;
		}
		if (playerRights == 7) {
			return true;
		}
		return false;
	}

	public boolean isTeleBlocked() {
		if (getTeleBlockTimer() > 0) {// if (System.currentTimeMillis() -
										// teleBlockDelay < teleBlockLength) {
			return true;
		}
		return false;
	}

	public boolean isTeleCooldown() {
		return teleCooldown;
	}

	public boolean isThereNexArmourEquipped(int slot) {
		switch (slot) {
			case PlayerConstants.playerHat:
				switch (playerEquipment[slot]) {
					case 20135:
					case 20147:
					case 20159:
					case 30266:// elder chaos hood
						return true;
				}
				return false;
			case PlayerConstants.playerChest:
				switch (playerEquipment[slot]) {
					case 20163:
					case 20151:
					case 20139:
					case 30264:// elder chaos top
						return true;
				}
				return false;
			case PlayerConstants.playerLegs:
				switch (playerEquipment[slot]) {
					case 20167:
					case 20155:
					case 20143:
					case 30265:// elder chaos robe
						return true;
				}
				return false;
			default:
				return false;
		}
	}

	public boolean isToggleRag() {
		return toggleRag;
	}

	public boolean isUpdateEquipmentRequired() {
		return updateEquipmentRequired;
	}

	// private ISAACCipher inStreamDecryption = null, outStreamDecryption =
	// null;
	//
	// public void setInStreamDecryption(ISAACCipher inStreamDecryption) {
	// this.inStreamDecryption = inStreamDecryption;
	// }
	//
	// public void setOutStreamDecryption(ISAACCipher outStreamDecryption) {
	// this.outStreamDecryption = outStreamDecryption;
	// }

	// public boolean samePlayer() {
	// for (int j = 0; j < PlayerHandler.players.length; j++) {
	// if (j == getId())
	// continue;
	// if (PlayerHandler.players[j] != null) {
	// if (PlayerHandler.players[j].playerName
	// .equalsIgnoreCase(playerName)) {
	// disconnected = true;
	// return true;
	// }
	// }
	// }
	// return false;
	// }

	public boolean isUpdateInvInterface() {
		return updateInvInterface > 0;
	}

	public boolean isWearingItemOnTick() {
		return wearingItemOnTick;
	}

	public boolean kickPlayer() {
		if (isActive) {
			getPacketSender().sendLogoutPacket();
		}

		pulse(() -> {
			disconnected = true;
			forceDisconnect = true;
		});
		return true;
	}

	public boolean obtainedStartPack() {
		return Variables.STARTER_PACK.getValue(this) == 1;
	}

	public void packGwdKc() {
		if (getTempVar() == null) {
			return;
		}

		int saraKC = Math.min(getTempVar().getInt("SaraKC"), 255);
		int armaKC = Math.min(getTempVar().getInt("ArmaKC"), 255);
		int zammyKC = Math.min(getTempVar().getInt("ZammyKC"), 255);
		int bandosKC = Math.min(getTempVar().getInt("BandosKC"), 255);
		setGwdKillCount((saraKC << 24) | (armaKC << 16) | (zammyKC << 8) | (bandosKC));
	}

	public boolean playerIsBusy() {
		if (isShopping || inTrade || openDuel || isBanking || duelStatus == 1 || duelStatus == 2 || duelStatus == 3
				|| duelStatus == 4 || isBobOpen || underAttackBy > 0 || underAttackBy2 > 0) {
			return true;
		}
		return false;
	}

	// public void addHitToQueueMask2(final Player p, final int damage, final
	// int mask, final int icon) {
	//// p.sendMessage("Added a hit to queue on me");
	// CycleEventHandler.getSingleton().addEvent(p, true, new CycleEvent() {
	// @Override
	// public void execute(CycleEventContainer container) {
	// if(p.disconnected || p == null || p.isDead || p.getHitPoints() < 1) {
	// container.stop();
	// return;
	// }
	// if (!p.hitUpdateRequired2) {
	// p.hitUpdateRequired2 = true;
	// p.hitDiff2 = damage;
	// p.hitMask2 = mask;
	// p.hitIcon2 = icon;
	// p.updateRequired = true;
	// container.stop();
	// }
	//
	// }
	//
	// @Override
	// public void stop() {
	//
	// }
	//
	// }, 1);
	// }

	// public void poisonProcess() {
	// Client p = (Client) PlayerHandler.players[getId()];
	// if(virusTimer > -1) {
	// virusTimer--;
	// if (virusTimer == 0 && virusDamage > 0) {
	// forcedChat("*Cough*");
	// playerLevel[3] -= (playerLevel[3] - virusDamage < 0 ?
	// playerLevel[3]:virusDamage);
	// setHitDiff(virusDamage);
	// setHitUpdateRequired(true);
	// updateRequired = true;
	// p.refreshSkill(3);
	// virusTimer = 10;
	// virusDamage--;
	// if(virusDamage == 0) {
	// virusTimer = -1;
	// p.sendMessage("The smoke clouds around you dissipate.");
	// }
	// }
	// }
	// }

	public void println(String str) {
		System.out.println("[player-" + getId() + "]: " + str);
	}

	public void println_debug(String str) {
		System.out.println("[player-" + getId() + "]: " + str);
	}

	public void processCombatEvents() {
		// List<CycleEventContainer> eventsCopy = new
		// ArrayList<CycleEventContainer>(combatEventsToProcess);
		// List<CycleEventContainer> remove = new
		// ArrayList<CycleEventContainer>();

		// for (CycleEventContainer c : eventsCopy) {
		// if (c != null) {
		// if (c.getOwner() == null) {
		//// remove.add(c);
		// } else {
		// //sendMessage("needs execution "+c.needsExecution());
		// //if (c.needsExecution()) {
		// c.execute();
		//// sendMessage("event executed at player");
		// c.stop();
		// //}
		// if (!c.isRunning()) {
		//// remove.add(c);
		// }
		// }
		// }
		// }

		for (CycleEventContainer k : combatEventsToProcess) {
			k.execute();
			// k.stop();
		}
		// for (CycleEventContainer k : combatEventsToProcess) {
		// k.stop();
		// }
		// for (CycleEventContainer c : remove) {
		// combatEventsToProcess.remove(c);
		// }
		combatEventsToProcess.clear();
		// eventsCopy.clear();
		// remove.clear();
		// CycleEventHandler.getSingleton().stopPvpEvents(this);
	}
	
	public void processInstaSpecs() {
		List<CycleEventContainer> eventsCopy = new ArrayList<>(specQueue);
		List<CycleEventContainer> remove = new ArrayList<>();
		for (CycleEventContainer c : eventsCopy) {
			if (c != null) {
				if (c.getOwner() == null) {
					remove.add(c);
				} else {
					if (c.needsExecution()) {
						c.execute();
					}
					if (!c.isRunning()) {
						remove.add(c);
					}
				}
			}
		}
		for (CycleEventContainer c : remove) {
			specQueue.remove(c);
		}
		eventsCopy.clear();
		remove.clear();
	}
	
	public void processPreCombat() {
		if (playerIndex > 0) {
			getPA().checkIfCanHit();
		}
		if (npcIndex > 0) {
			getPA().checkIfCanHitNpc();
		}

	}
	
	public void processAttackInit() {
		if (getAttackTimer() == 1) {
			if (getSeasonalData().containsPowerUp(PowerUpData.MIRROR) && (getSingleCastSpell() != null || getAutoCastSpell() != null) && clone == null && (playerIndex > 0 || (npcIndex > 0 && clickNpcType == 0)) && Misc.randomBoolean()) {
				Mob focus;
				if (npcIndex > 0) {
					focus = NPCHandler.npcs[npcIndex];
				} else {
					focus = PlayerHandler.players[playerIndex];
				}

				SeasonalData.initClone(this, focus, 15);
			}
		}

		if (getAttackTimer() == 0) {
			if (npcIndex > 0 && clickNpcType == 0) {
				getCombat().attackNpc(npcIndex);
			} else if (playerIndex > 0) {
				getCombat().attackPlayer(playerIndex);
			}
		} else if (getAttackTimer() != 0 && queueGmaulSpec && playerIndex > 0) {
			getCombat().attackPlayer(playerIndex);
		}
		
		if (blockedShot) {
			blockedShot = false;
		}
	}
	
	public void processDeath() {
		if (!applyDeath) {
			if (isDead() || getSkills().getLifepoints() <= 0) {
				if (!isDead()) {
					setDead(true);
				}
				if (duelStatus == 6 && isDead()) {
					getSkills().healToFull();
					setDead(false);
				}
				if (!applyDeath) {
					getPA().applyDeath();
					
					int ran = Misc.random(1000);
					if (ran == 7) {
						startAnimation(Animation.FUNNY_DEATH1);
					} else if (ran == 0) {
						startAnimation(Animation.FUNNY_DEATH2);
					}
					setPauseAnimReset(7);
				}
			}
		}
	}
	
	public abstract boolean processQueuedPackets();

	public double profoundBoost() {
		double boost = 1.0;
		
		int count = 0;
		if (playerEquipment[PlayerConstants.playerChest] == 18706) {
			count++; //boost += 0.05;
		}

		if (playerEquipment[PlayerConstants.playerLegs] == 18707) {
			count++; //boost += 0.05;
		}

		if (playerEquipment[PlayerConstants.playerWeapon] == 18705) {
			count++; //boost += 0.05;
		}

		if (playerEquipment[PlayerConstants.playerShield] == 18709) {
			count++; //boost += 0.05;
		}

		if (playerEquipment[PlayerConstants.playerHat] == 18708) {
			count++; //boost += 0.05;
		}
		
		if (count == 1) {
			return 1.10;
		} else if (count == 2) {
			return 1.15;
		} else if (count > 2) {
			return 1.25;
		}

		return boost;
	}

	public boolean properLogout() {
		// if (!properLogout)
		// return false;
		if (PlayerHandler.isKickAllPlayers()) {
			return true;
		}
		if (isDead() || !finishedRevive()) {
			return false;
		}
		if (forceDisconnect) {
			return true;
		}
		if (performingAction) {
			return false;
		}
		if (teleTimer > 4) {
			return false;
			// if (doingHandCannonSpec)
			// return false;
		}

		return true;

	}
	
	public boolean isInCombatDelay() {
		return !combatLogout.complete();
	}
	
	public void startLogoutDelay() {
		combatLogout.startTimer(16);
	}

	public void putInCombatWithPlayer(int attacker) {
		underAttackBy = attacker;
		startInCombatTimers();
	}
	
	public void putInCombatWithNpc(int attacker) {
		underAttackBy2 = attacker;
		startInCombatTimers();
	}
	
	private void startInCombatTimers() {
		startLogoutDelay();
		startSinglesCombatDelayTimer(wildLevel > 10 ? 7 : 10);
	}
	
	public void resetInCombatTimer() {
		combatLogout.reset();
	}
	
	public void sendPlayerHome() {
		if (duelStatus == 5)
			return;
		boolean tabbedOut = false;
		for (int i = 0; i < playerItems.length; i++) {
			final TeleTab tab = TeleTab.forItemId(playerItems[i]-1);
			if (tab != null) {
				TeleTabPlugin.startTeleport(this, tab, i);
				tabbedOut = true;
				break;
			}
		}
		if (!tabbedOut) {
			getPA().startHomeTeleportCommand();
		}
	}

	public abstract void refreshSkill(int id);

	public void refreshSkills() {
		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			refreshSkill(i);
		}
	}

	public void resetSpawnedSkills() {
		setSpawnedSkills(null);
		refreshSkills();
		calcCombat();
	}

	/**
	 * Removes this entity from the specified region.
	 *
	 * @param region
	 *            The region.
	 */
	public void removeFromRegion(Region region) {
		region.removePlayer(this);
		Server.getRegionManager().checkEmptyRegion(region);
	}

	public void resetGwdKc() {
		getTempVar().remove("SaraKC");
		getTempVar().remove("ArmaKC");
		getTempVar().remove("ZammyKC");
		getTempVar().remove("BandosKC");
		setGwdKillCount(0);
	}

	public void ResetKeepItems() {
		WillKeepAmt1 = -1;
		WillKeepItem1 = -1;
		WillKeepAmt2 = -1;
		WillKeepItem2 = -1;
		WillKeepAmt3 = -1;
		WillKeepItem3 = -1;
		WillKeepAmt4 = -1;
		WillKeepItem4 = -1;
	}

	public void resetMyShopSellingVars() {
		sellingId = 0;
		sellingN = 0;
		sellingS = 0;
		sellingIdOriginal = 0;
	}

	public void resetSkills() {
		herbAmount = doingHerb = newHerb = newXp = -1;
	}
	
	public Client toClient() {
		return (Client) this;
	}
	
	public boolean revenantDungeon() {
		return getZones().isInRevDung();
	}

	public boolean safeAreas(int x, int y, int x1, int y1) { // clue scrolls
																// poop
		return (absX >= x && absX <= x1 && absY >= y && absY <= y1);
	}

	public boolean safeZone() {
		if (absX > 3332 && absX < 3333 && absY > 3332 && absY < 3333) {
			return true;
		}
		if (inPkBox()) {
			return true;
		}

		return false;
	}

	public abstract void sendMessage(String msg);

	public abstract void sendMessageSpam(String msg);

	public void setAchievBoolean(int index, boolean setTrue) {
		if (setTrue) {
			achievements[index] = 1;
		} else {
			achievements[index] = 0;
		}
	}

	public void setAchievInt(int index, int num) {
		achievements[index] = num;
	}

	public void setAgilityPoints(int pcAgilityPoints) {
		this.agilityPoints = pcAgilityPoints;
	}
	
	public void rewardAgilityPoints(int increment) {
		this.agilityPoints += increment;
		sendMessageSpam("You received " + increment + " Agility Tokens.");
	}

	public void setBankPin(int pinSlot, int pinNumber) {
		if (pinSlot < 0 || pinSlot > bankPins.length) {
			return;
		}
		this.bankPins[pinSlot] = pinNumber;
	}

	public void setBankPins(int[] bankPins) {
		this.bankPins = bankPins;
	}

	public void setBossKills(int index, int amount, boolean add) {
		if (!add) {
			bossKills[index] = amount;
		} else {
			int currentKills = getBossKills(index);
			bossKills[index] = currentKills + amount;
			if (!Config.SERVER_DEBUG) {
				BossKCHandler.saveKillCount(mySQLIndex, BossKCEnum.values[index], bossKills[index]);
			}
		}
	}

	public void setBot(BotClient bot) {
		playerOwnedBot = bot;
	}

	public void setChatAbuse(int amount, boolean add) {
		this.judgement[JudgementHandler.CHATABUSE.ordinal()] = (add ? getChatAbuse() : 0) + amount;
	}

	public void setChatSettings(int settings[]) {
		this.chatSettings = settings;
	}

	public void setChatText(byte chatText[]) {
		this.chatText = chatText;
	}

	public void setChatTextColor(int chatTextColor) {
		this.chatTextColor = chatTextColor;
	}

	public void setChatTextEffects(int chatTextEffects) {
		this.chatTextEffects = chatTextEffects;
	}

	public void setChatTextSize(byte chatTextSize) {
		this.chatTextSize = chatTextSize;
	}

	public void setClanChatSettings(int setting) {
		this.chatSettings[ChatSettings.CLAN_INDEX] = setting;
	}

	public void setClanWarPlayerStake(ArrayList<GameItem> clanWarPlayerStake) {
		this.clanWarPlayerStake = clanWarPlayerStake;
	}

	public void setClueAmount(int type, int amount, boolean add) {
		if (TreasureTrails.LOW_CLUE == type) {
			getExtraHiscores().setClueEasyCompleted((add ? getClueAmount(type) : 0) + amount);
		}
		if (TreasureTrails.MED_CLUE == type) {
			getExtraHiscores().setClueMediumCompleted((add ? getClueAmount(type) : 0) + amount);
		}
		if (TreasureTrails.HIGH_CLUE == type) {
			getExtraHiscores().setClueHardCompleted((add ? getClueAmount(type) : 0) + amount);
		}
	}

	public void setCmbExpLock(boolean lockExp) {
		if (lockExp) {
			getVariables()[Variables.TOGGLE_CMB_EXP.getCode()] = 1;
		} else {
			getVariables()[Variables.TOGGLE_CMB_EXP.getCode()] = 0;
		}
	}

	public void setCrown(int crown) {
		this.variables[Variables.SET_CROWN.getCode()] = crown;
	}

	public void setCWWaitingRoomCountdown(int ticks) {
		waitRoomCountDown = ticks;
	}

	public void setDetectNull(boolean checkNull) {
		detectNull = checkNull;
	}

	public void setDonatedTotalAmount(int donatedTotalAmount, boolean updateInterface) {
		this.donatedTotalAmount = donatedTotalAmount;
		if (updateInterface) {
			getPacketSender().sendFrame126("<col=ff7000>Amount Donated: <col=ffb000>" + getDonatedTotalAmount(), 16036);
		}
	}

	public void setDonatedRealTotalAmount(int donatedRealTotalAmount) {
		this.donatedRealTotalAmount = donatedRealTotalAmount;
	}

	public void addDonatedRealTotalAmount(int toAdd) {
		this.donatedRealTotalAmount += toAdd;
	}

	public void setDonPoints(int donPoints, boolean updateInterface) {
		this.donPoints = donPoints;
		if (updateInterface) {
			getPacketSender().sendFrame126("<col=ff7000>Donation Points: <col=ffb000>" + getDonPoints(), 16035);
		}
	}
	
	public void setDominionTokens(int dominionTokens) {
		this.dominionTokens = dominionTokens;
	}

	public void setDoubleExpLength(int ticks) {
		this.timers[Timers.DOUBLE_EXP_LENGTH.ordinal()] = ticks;
	}

	public void increaseDoubleExpLength(int ticks) {
		this.timers[Timers.DOUBLE_EXP_LENGTH.ordinal()] += ticks;
	}

	public void setPkpBoostLength(int ticks) {
		this.timers[Timers.PKP_BOOST.ordinal()] = ticks;
	}

	public void setDropRateBoostLength(int ticks) {
		this.timers[Timers.DROP_RATE_BOOST.ordinal()] = ticks;
	}

	public void increaseDropRateBoostLength(int ticks) {
		this.timers[Timers.DROP_RATE_BOOST.ordinal()] += ticks;
	}

	public void setDuelDC(int amount, boolean add) {
		this.variables[Variables.DUEL_DC.getCode()] = (add ? getDuelDC() : 0) + amount;
	}

	public void setDuelingClient(Client other) {
		duelingClient = other;
	}

	public void setDuelingClientClanWar(Client duelingClientClanWar) {
		this.duelingClientClanWar = duelingClientClanWar;
	}

	public void setDuelKC(int amount, boolean add) {
		this.variables[Variables.DUEL_KC.ordinal()] = (add ? getDuelKC() : 0) + amount;
	}

	public void setDuelRuleTimer(int duelRuleTimer) {
		this.duelRuleTimer = duelRuleTimer;
	}

	public void setDuelStreak(int amount, boolean add) {
		this.variables[Variables.DUEL_STREAK.ordinal()] = (add ? getDuelStreak() : 0) + amount;
	}

	public void setEffigy(int effigy) {
		this.variables[Variables.EFFIGY.ordinal()] = effigy;
	}

	public void setEggTimer(int time) {
		this.variables[Variables.EGG_INCUBATOR_TIME.ordinal()] = time;
	}

	public void setEloRating(int eloRating, boolean updateInterface) {
		this.eloRating = eloRating;
		if (updateInterface) {
			getPacketSender().sendFrame126("<col=ff7000>ELO Rating: <col=ffb000>" + getEloRating(), 16039);
		}
	}

	public void setFightPitPoints(int points) {
		this.variables[Variables.FIGHT_PITS_POINTS.ordinal()] = points;
	}

	public FightExp getFightXp() {
		return fightXp;
	}
	
	public void setFightXp(FightExp fightXp) {
		this.fightXp = fightXp;
	}

	public void setGwdKillCount(long killCount) {
		this.killCount = killCount;
	}

	public void setHasCough(boolean cough) {
		hasCough = cough;
	}

	public void setIncubatorEggId(int ID) {
		this.variables[Variables.EGG_IN_INCUBATOR.ordinal()] = ID;
	}

	public void setIsActiveAtomicBoolean(boolean isactive) {
		isActiveAtomicBoolean.set(isactive);
	}

	public void setJailTime(int ticks) {
		this.variables[Variables.JAIL_TIME.ordinal()] = ticks;
	}
	
	public void setSemiAdmin(int value) {
		this.variables[Variables.SEMI_ADMIN_RANK.ordinal()] = value;
	}
	
	public void setYellMuted(boolean value) {
		if (value) this.variables[Variables.YELL_MUTE.ordinal()] = 1;
		else this.variables[Variables.YELL_MUTE.ordinal()] = 0;
	} 

	public void setJudgement(int vars[]) {
		this.judgement = vars;
	}

	public void setLastClicked(int lastClicked) {
		this.lastClicked = lastClicked;
	}

	/**
	 * Sets the localPlayers.
	 *
	 * @param localPlayers
	 *            The localPlayers to set.
	 */
	public void setLocalPlayers(List<Player> localPlayers) {
		this.localPlayers = localPlayers;
	}

	/**
	 * Sets the current location.
	 *
	 * @param location
	 *            The current location.
	 */
	public void setLocation(int x, int y, int z) {
		// this.absX = x;
		// this.absY = y;

//		setCurrentLocation(Location.create(x, y, z));
		getCurrentLocation().setX(x);
		getCurrentLocation().setY(y);
		getCurrentLocation().setZ(z);
		updateCenterLocation();

		final Region newRegion = Server.getRegionManager().getRegionByLocation(x, y, z);
		if (newRegion != currentRegion) {
			if (currentRegion != null) {
				removeFromRegion(currentRegion);
			}
			currentRegion = newRegion;
			addToRegion(currentRegion);
			Music.playMusic(this, getCurrentLocation().getRegionId());
			// if (teleTimer < 4)
			// ItemHandler.reloadItems((Client)this);
		}
		
		ZoneManager.applyZone(this);
		
		if (raidsManager != null && getTempVar().getBoolean("acid_pool")) {
			raidsManager.greatOlmManager.buildAcidPool(getCurrentLocation().copyNew(), false);
		}
	}

	public void setLongName(long longName) {
		this.longName = longName;
	}
	
	public void setLoyaltyPoints(int loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}
	
	public void setLoyaltyPoints(int loyaltyPoints, boolean updateInterface) {
		this.loyaltyPoints = loyaltyPoints;
		if (updateInterface) {
			getPacketSender().sendFrame126("<col=ff7000>Loyalty Points: <col=ffb000>" + getLoyaltyPoints(), 16037);
		}
	}
	
	public void setExtraNameChange(int extraNameChange) {
		getVariables()[Variables.NAME_CHANGE_POINTS.getCode()] = extraNameChange;
	}
	
	public void setTaiBwoFavour(int taiBwoFavour, boolean updateInterface) {
		int favour =  Math.min(taiBwoFavour, 400);
		getVariables()[Variables.TAI_BWO_FAVOUR.getCode()] = favour;
		if (taiBwoFavour >= 400) {
			getAchievement().earnMaxFavour();
		}
		if (updateInterface && inTaiBwoWannai()) {
			earnedTaiBwoFavour = true;
			getPacketSender().sendConfig(535, favour);
		}
	}
	
	public void setTaiBwoCleanup(int taiBwoCleanup) {
		getVariables()[Variables.TAI_BWO_CLEANUP.getCode()] = taiBwoCleanup;
	}
	
	public void setLmsCoffer(int lmsCoffer) {
		getVariables()[Variables.LMS_COFFER.getCode()] = lmsCoffer;
	}
	
	public void setChristmasPoints(int christmasPoints) {
		getVariables()[Variables.CHRISTMAS_POINTS.getCode()] = christmasPoints;
	}

	public void setMoneyPouch(long moneyPouch) {
		this.moneyPouch = moneyPouch;
	}
	
	public void setMuted(boolean muted) {
		this.muted = muted;
	}
	
	public void setNpcAggroTick(int npcAggroTick) {
		this.npcAggroTick = npcAggroTick;
	}

	public void setPartner(Client partner) {
		this.partner = partner;
	}

	public void setPatch(final int index, final Patch patch) {
		this.patches[index] = patch;
	}

	public void setPatches(final Patch[] patches) {
		this.patches = patches;
	}

	public void setPcPoints(int pcPoints) {
		this.pcPoints = pcPoints;
	}

	public void setPendingRequest(PartyRequest pendingRequest) {
		this.pendingRequest = pendingRequest;
	}


	public void setPlayerFarmingState(final boolean state) {
		this.playerIsFarming = state;
	}

	public void setZeals(int zeals) {
		this.zeals = zeals;
	}

	public void setPlayerName(String name) {
		this.playerName = name;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
		if (displayName != null && !displayName.isEmpty()) {
			longDisplayName = Misc.playerNameToInt64(displayName);
		}
	}

	public void setPoisonDamage(int amount) {
		this.variables[Variables.POISON_DAMAGE.ordinal()] = amount;
		getPacketSender().sendHpOrbState();
	}

	public void setPouchData(int i, int amount) {
		pouchData[i] = amount;
	}

	public void setPrivateChatSettings(int setting) {
		this.chatSettings[ChatSettings.PRIVATE_INDEX] = setting;
	}

	public void setPublicChatSettings(int setting) {
		this.chatSettings[ChatSettings.PUBLIC_INDEX] = setting;
	}

	public Potions getPotions() {
		return potions;
	}

	public void setQueuedAnimation(final int id) {
		this.queuedAnimation = id;
	}

	public void setReceivedPacket(int index, boolean activated) {
		this.receivedPacket[index] = activated;
	}

	public void setSeasonalElo(int seasonalElo[]) {
		this.seasonalElo = seasonalElo;
	}

	public void setShopBot(BotClient bot) {
		playerOwnedShopBot = bot;
	}
	
	public void skullPlayer() {
		setSkullTimer(Config.SKULL_TIMER);
		setSkull(true);
	}

	public void setSkull(boolean setSkull) {
		if (attr().has("canAttack")) {
			return;
		}

		if (setSkull) {
			isSkulled = true;
			headIconPk = 0;
		} else {
			isSkulled = false;
			headIconPk = -1;
		}
		updateFlags.add(UpdateFlag.APPEARANCE);
	}

	public void setSkullTimer(int skullTimer) {
		this.timers[Timers.SKULL_TIMER.ordinal()] = skullTimer;
	}
	
	public void setBountyTargetSpell(int spellLocked) {
		this.variables[Variables.BOUNTY_TARGET_SPELL_UNLOCK.ordinal()] = spellLocked;
		getPacketSender().sendConfig(ConfigCodes.BOUNTY_TELEPORT_UNLOCKED, spellLocked);
	}

	public void setSlayerScrolls(int scrollType) {
		this.variables[Variables.SLAYER_SCROLLS.ordinal()] = scrollType;
	}

	public void setSlayerTask(SlayerTask task) {
		this.slayerTask = task;
	}

	public void setSpiritShieldHits(int hits) {
		this.timers[Timers.DISABLE_BLESSED_SPIRIT_SHIELD.ordinal()] = hits;
	}

	public void setStartPack(boolean startPack) {
		if (startPack) {
			variables[Variables.STARTER_PACK.ordinal()] = 1;
		} else {
			variables[Variables.STARTER_PACK.ordinal()] = 0;
		}
	}

	public void setStrongholdLevel(int amount, boolean add) {
		this.variables[Variables.STRONGHOLD_LEVEL.ordinal()] = (add ? getStrongholdLevel() : 0) + amount;
	}

	public void setSummoningOrbSetting(int settingIndex) {
		this.variables[Variables.SUMMONING_ORB_OPTION.ordinal()] = settingIndex;
	}

	public void setSlayerTasksCompleted(int tasksCompleted) {
		this.slayerTasksCompleted = tasksCompleted;
	}

	public void setTeleBlockTimer(int ticks) {
		final int time = this.timers[Timers.TELEBLOCK_TIMER.ordinal()];

		if (time <= 0 || ticks == 0) {
			getPacketSender().sendWidget(DisplayTimerType.TELEBLOCK, ticks);
		}

		this.timers[Timers.TELEBLOCK_TIMER.ordinal()] = ticks;
	}

	public void setTeleCooldown(boolean teleCooldown) {
		this.teleCooldown = teleCooldown;
	}

	public void setTimedMute(int ticks) {
		this.timers[Timers.MUTE_LENGTH.ordinal()] = ticks;
	}

	public void setTimers(int timers[]) {
		this.timers = timers;
	}

	public void setTitleBoolean(int index, boolean setTrue) {
		if (setTrue) {
			titles[index] = 1;
		} else {
			titles[index] = 0;
		}
	}

	@Override
	public void clearUpdateFlags() {
		updateFlags.clear();
		animations.clear();
		graphics.clear();
		facingLocation = null;
		poisonMask = -1;
		facing = -1;
		forcedChat = "";
		hitMask = new Hit(0, 0, 0);
		hitMask2 = new Hit(0, 0, 0);

		movedX = movedY = -1;

		didTeleport = false;
		playerProps.currentOffset = 0;

		Arrays.fill(receivedPacket, false);
		finishedProcess = false;
	}

	public void setTitleInt(int index, int num) {
		titles[index] = num;
	}

	public void setToggleRag(boolean rag) {
		toggleRag = rag;
	}

	public void setTradeChatSettings(int setting) {
		this.chatSettings[ChatSettings.TRADE_INDEX] = setting;
	}

	public void setUnlockedCrown(int index, int unlocked) {
		this.unlockedCrowns[index] = unlocked;
	}
	
	public void setUnlockedCrown(int index, boolean unlocked) {
		this.unlockedCrowns[index] = unlocked ? 1 : 0;
	}

	public void setUntradeableGold(int amount) {
		this.variables[Variables.UNTRADEABLE_GOLD.ordinal()] = amount;
	}

	public void setUntradeableShopMap(Map<Integer, Integer> untradeableMap) {
		this.untradeableShopMap = untradeableMap;
	}

	public void setUpdateEquipment(boolean updateEquipment, int slot) {
		if (updateEquipment) {
			setUpdateEquipmentRequired(true);
		}
		this.updateEquipment[slot] = updateEquipment;
	}

	public void setUpdateEquipmentRequired(boolean updateEquipmentRequired) {
		this.updateEquipmentRequired = updateEquipmentRequired;
	}

	public void setVar(Variables var, int value) {
		if (var.getCode() < 0 || var.getCode() >= variables.length) {
			return;
		}
		variables[var.getCode()] = value;
	}

	public void setUpdateInvInterface(int updateInvInterface) {
		this.updateInvInterface = updateInvInterface;
	}

	public void setVariables(int vars[]) {
		this.variables = vars;
	}
	
	public void increaseLMSGamesPlayed() {
		this.variables[Variables.LAST_MAN_STANDING_GAMES_PLAYED.getCode()] += 1;
	}
	
	public int getLmsGamesPlayed() {
		return this.variables[Variables.LAST_MAN_STANDING_GAMES_PLAYED.getCode()];
	}

	public void setWaitingRoomCampTimer(int waitingRoomCampTimer) {
		this.waitingRoomCampTimer = waitingRoomCampTimer;
	}

	public void setWearingItemOnTick(boolean wearingItemOnTick) {
		this.wearingItemOnTick = wearingItemOnTick;
	}

	public void setWildernessTick(int ticks) {
		this.timers[Timers.WILDERNESS_TICK.ordinal()] = ticks;
	}

	public void setX(int x) {
		absX = x;
	}

	public void setY(int y) {
		absY = y;
	}

	public void setZ(int z) {
		heightLevel = z;
	}

	public void setZarosKC(int amount, boolean add) {
		this.variables[Variables.ZAROSKC.ordinal()] = (add ? getZarosKC() : 0) + amount;
	}

	public boolean slayerScrollUnlocked(int scrollType) {
		if (scrollType == 0) {
			return (getSlayerScrolls() & 0x1) == 1;
		} else if (scrollType == 1) {
			return (getSlayerScrolls() >> 1 & 0x1) == 1;
		} else if (scrollType == 2) {
			return (getSlayerScrolls() >> 2 & 0x1) == 1;
		}
		return false;
	}

	public void speak(String msg) {
		String crown = "";
		int icon = ChatCrown.values[getSetCrown()].getClientIcon();
		if (icon > 0) {
			crown = "<img=" + (icon - 1) + ">";
		}

		forceChat(msg);

		String message = crown + getNameSmartUp() + ": <col=ff>" + msg;
		for (Player p : Server.getRegionManager().getLocalPlayers(this)) {
			if (p == null || p == this || p.isBot()) {
				continue;
			}

			if (!p.withinDistance(this)) {
				continue;
			}

			p.getPacketSender().sendMessage(ChatMessageTypes.ADVERT_MESSAGE, message, getNameSmartUp());
		}
	}

	public void toggleBusy() {
		this.performingAction = !this.performingAction;
	}

	public void toggleSaveInProgress() {
		saveInProgressForDC = !saveInProgressForDC;
	}

	public int tricksterAmount() {
		int pieces = 0;
		if (playerEquipment[PlayerConstants.playerHat] == 21467) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerChest] == 21468) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerLegs] == 21469) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerHands] == 21470) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerFeet] == 21471) {
			pieces++;
		}

		return pieces;
	}

	public void unlockSlayerScroll(int scrollType) {
		int scrollOne = getSlayerScrolls() & 0x1;
		int scrollTwo = getSlayerScrolls() >> 1 & 0x1;
		int scrollThree = getSlayerScrolls() >> 2 & 0x1;
		if (scrollType == 0) {
			scrollOne = 1;
		} else if (scrollType == 1) {
			scrollTwo = 1;
		} else if (scrollType == 2) {
			scrollThree = 1;
		}
		setSlayerScrolls(scrollThree << 2 | scrollTwo << 1 | scrollOne);
	}

	public void unpackGwdKc() {
		getTempVar().put("SaraKC", (int) (getGwdKillCount() >> 24));
		getTempVar().put("ArmaKC", (int) ((getGwdKillCount() >> 16) & 0xFF));
		getTempVar().put("ZammyKC", (int) ((getGwdKillCount() >> 8) & 0xFF));
		getTempVar().put("BandosKC", (int) (getGwdKillCount() & 0xFF));
	}

	public void updateshop(int i) {
		if (!disconnected) {
			final Client p = (Client) this;
			p.getShops().resetShop(i);
		}
	}

	public boolean usingPickaxe() {
		return playerEquipment[PlayerConstants.playerWeapon] >= 16142 && playerEquipment[PlayerConstants.playerWeapon] <= 16152 || 
			   playerEquipment[PlayerConstants.playerWeapon] >= 16295 && playerEquipment[PlayerConstants.playerWeapon] <= 16315;
	}

	public int vanguardAmount() {
		int pieces = 0;
		if (playerEquipment[PlayerConstants.playerHat] == 21472) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerChest] == 21473) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerLegs] == 21474) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerHands] == 21475) {
			pieces++;
		}

		if (playerEquipment[PlayerConstants.playerFeet] == 21476) {
			pieces++;
		}

		return pieces;
	}

	public boolean whitePortalSafe() {
		return RSConstants.whitePortalSafe(getX(), getY());
	}

	public boolean withinDistance(int otherX, int otherY) {
		int deltaX = this.getX() - otherX, deltaY = this.getY() - otherY;
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}

	/**
	 * NOTE: DOES NOT CHECK FOR HEIGHT
	 */
	public boolean withinDistance(Mob other) {
		int deltaX = other.getCurrentLocation().getX() - getCurrentLocation().getX(), deltaY = other.getCurrentLocation().getY() - getCurrentLocation().getY();
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}
	
	public boolean withinDistance(Mob other, int dist) {
		return withinDistance(other.getCurrentLocation().getX(), other.getCurrentLocation().getY(), other.getCurrentLocation().getZ(), dist);
	}

	public boolean withinDistance(Player otherPlr, int dist) {
		return withinDistance(otherPlr.getX(), otherPlr.getY(), otherPlr.getZ(), dist);
	}
	
	public boolean withinDistance(int otherX, int otherY, int otherZ, int dist) {
		if (heightLevel != otherZ) {
			return false;
		}
		int deltaX = otherX - absX, deltaY = otherY - absY;
		return deltaX <= dist && deltaX >= -dist && deltaY <= dist && deltaY >= -dist;
	}

	public int getProjectileLockon() {
		return -getId() - 1;
	}

	public void setDifficulty(int difficulty) {
		if (difficulty == PlayerDifficulty.NORMAL) {
			setCrown(ChatCrown.REGULAR.ordinal());
			playerRights = 0;
		} else if (isIronMan()) {
			playerRights = 13;
			ChatCrownsSql.unlockChatCrown(this, ChatCrown.IRONMAN);
			setCrown(ChatCrown.IRONMAN.ordinal());
		}
		this.difficulty = difficulty;
	}

	public void demoteToRegularRank() {
		setDifficulty(PlayerDifficulty.NORMAL);
	}

	public void promoteToIronMan() {
		setDifficulty(PlayerDifficulty.IRONMAN);
		
		Titles.ironMan(this);

		if (getSetCrown() != ChatCrown.IRONMAN.ordinal()) {
			setCrown(ChatCrown.IRONMAN.ordinal());
		}
	}
	
	public void promoteToHCIronMan() {
		setDifficulty(PlayerDifficulty.HC_IRONMAN);
		
		Titles.ironMan(this);

		if (getSetCrown() != ChatCrown.HC_IRONMAN.ordinal()) {
			setCrown(ChatCrown.HC_IRONMAN.ordinal());
		}
	}
	
	public void promoteToDeadHCIM() {
		setDifficulty(PlayerDifficulty.HC_IRONMAN_DEAD);
		
		Titles.ironMan(this);

		if (getSetCrown() != ChatCrown.IRONMAN.ordinal()) {
			setCrown(ChatCrown.IRONMAN.ordinal());
		}
	}
	
	public void demoteToRegularIronMan() {
		
//		ChatCrownsSql.lockChatCrown(this, ChatCrown.HC_IRONMAN);
		
		promoteToIronMan();
	}

	public boolean isIronMan() {
		return difficulty == PlayerDifficulty.IRONMAN || difficulty == PlayerDifficulty.HC_IRONMAN;
	}
	
	public boolean isHCIronMan() {
		return difficulty == PlayerDifficulty.HC_IRONMAN;
	}
	
	public boolean requireBankPinToOpen() {
		return !enterdBankpin && hasBankPin;
	}
	
	public Movement getMovement() {
		return movement;
	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean usingProtectPrayer(boolean usingCurses, int prayerType) {
		if (playerPrayerBook == PrayerBook.NORMAL && !usingCurses) {
			return prayerActive[prayerType];
		} else if (playerPrayerBook == PrayerBook.CURSES && usingCurses) {
			return curseActive[prayerType];
		} else {
			return false;
		}
	}
	
	public boolean usingAnyProtectPrayers() {
		if (playerPrayerBook == PrayerBook.NORMAL) {
			return usingProtectPrayer(false, Prayers.PROTECT_FROM_MELEE.ordinal())
					|| usingProtectPrayer(false, Prayers.PROTECT_FROM_MAGIC.ordinal())
					|| usingProtectPrayer(false, Prayers.PROTECT_FROM_MISSILES.ordinal());
		} else {
			return usingProtectPrayer(true, Curses.DEFLECT_MELEE.ordinal())
					|| usingProtectPrayer(true, Curses.DEFLECT_MISSILES.ordinal())
					|| usingProtectPrayer(true, Curses.DEFLECT_MAGIC.ordinal());
		}
	}
	
	@Override
	public int protectPrayerDamageReduction(int currentHit) {
		return currentHit * 60 / 100;
	}
	
	/**
	 * Transforms player into npc
	 * @param npcId - ID -1 is to turn back into npc.
	 */
	public void transform(int npcId) {
	    if (npcId < 0) {
	    	npcId = -1;
	    }

	    this.transformNpcId = npcId;
		getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

	public int getTransformId() {
		return transformNpcId;
	}

	public void resetController() {
	    this.controller = new DefaultPlayerController(this);
	}

	public void setController(PlayerController controller) {
	    this.controller = controller;
	}
	
	public PlayerController getController() {
	    return controller;
	}
	
	public boolean underAttackByPlayer() {
		return underAttackBy > 0;
	}
	
	public boolean underAttackByNpc() {
		return underAttackBy2 > 0;
	}
	
	public boolean isAttackingPlayer() {
		return playerIndex > 0;
	}
	
	public boolean isAttackingNpc() {
		return npcIndex > 0;
	}
	
	public boolean isInCombatWithPlayer() {
		return underAttackByPlayer() || isAttackingPlayer();
	}
	
	public boolean isInCombatWithNpc() {
		return underAttackByNpc() || isAttackingNpc();
	}
	
	public boolean isInCombatWithMob() {
		return isInCombatWithNpc() || isInCombatWithPlayer();
	}
	
	private TickTimer stunCooldown = new TickTimer();
	
	public void stunEntity(final int time, Graphic gfx) {
		if (isStunned()) {
			return;
		}
		getMovement().resetWalkingQueue();
		getCombat().resetPlayerAttack();
		if (gfx != null)
			startGraphic(gfx);
		getPacketSender().playSound(Sounds.PICKPOCKET_STUN);
		sendMessage("You got stunned.");
		stunTimer.startTimer(time);
		stunCooldown.startTimer(time+1);
	}
	
	private TickTimer rootTimer = new TickTimer();
	
	public boolean isRooted() {
		return !rootTimer.complete();
	}
	
	public void resetRoot() {
		rootTimer.reset();
	}
	
	public void rootEntity(final int time) {
		if (isRooted()) {
			return;
		}
		getMovement().resetWalkingQueue();
		// c.gfx100(180);
		rootTimer.startTimer(time);
		sendMessage("You got rooted.");
	}

	@Override
	public void shove(int stunTick, Location attackerLoc) {
		if (!stunCooldown.complete()) {
			return;
		}
		
		if (stunTick > 0) {
			stunEntity(stunTick, Graphic.create(254, GraphicType.HIGH));
		}

		setShoved(true);
		
		Location newLoc = Location.getShoved(attackerLoc, getCurrentLocation(), getDynamicRegionClip());
		
		getPA().moveCheck(newLoc.getX(), newLoc.getY());
	}

	public RangeWeapon getRangeWeapon() {
		return rangeWeapon;
	}

	public void setRangeWeapon(RangeWeapon rangeWeapon) {
		this.rangeWeapon = rangeWeapon;
	}

	public SpellsData getAutoCastSpell() {
		return autoCastingSpell;
	}

	public void setAutoCastSpell(SpellsData castingSpell) {
		this.autoCastingSpell = castingSpell;
		setCombatDistance(CombatFormulas.getCombatDistanceToSet(this));
		if (castingSpell != null) {
			variables[Variables.AUTOCAST.getCode()] = castingSpell.getButtonId();
		} else {
			variables[Variables.AUTOCAST.getCode()] = 0;
		}
	}
	
	public int loadAutocastSpell() {
		return variables[Variables.AUTOCAST.getCode()];
	}

	public SpellsData getSingleCastSpell() {
		return singleCastSpell;
	}

	public void setSingleCastSpell(SpellsData singleCastSpell) {
		this.singleCastSpell = singleCastSpell;
		setCombatDistance(CombatFormulas.getCombatDistanceToSet(this));
	}

	public DialogueBuilder getDialogueBuilder() {
	    return dialogueBuilder;
	}

	public void setDialogueBuilder(DialogueBuilder dialogueBuilder) {	    
	    this.dialogueBuilder = dialogueBuilder;
	}

	public Optional<OptionDialogue> getOptionDialogue() {
	    return optionDialogue;
	}

	public void setOptionDialogue(Optional<OptionDialogue> optionDialogue) {
	    this.optionDialogue = optionDialogue;
	}

	public Optional<EnterAmountDialogue> getEnterAmountDialogue() {
		return enterAmountDialogue;
	}

	public void setEnterAmountDialogue(Optional<EnterAmountDialogue> enterAmountDialogue) {
		this.enterAmountDialogue = enterAmountDialogue;
	}
	
	public Optional<EnterTextDialogue> getEnterTextDialogue() {
		return enterTextDialogue;
	}

	public void setEnterTextDialogue(Optional<EnterTextDialogue> enterTextDialogue) {
		this.enterTextDialogue = enterTextDialogue;
	}

	public CombatTab getCombatTab() {
		return combatTab;
	}

	public void setCombatTab(CombatTab combatTab) {
		this.combatTab = combatTab;
	}

	public CombatTabButton getCombatTabButtonToggle() {
		return combatTabButtonToggle;
	}

	public void setCombatTabButtonToggle(CombatTabButton combatTabButtonToggle) {
		this.combatTabButtonToggle = combatTabButtonToggle;
	}

	public ExtraHiscores getExtraHiscores() {
		return extraHiscores;
	}

	public void setExtraHiscores(ExtraHiscores extraHiscores) {
		this.extraHiscores = extraHiscores;
	}	
	

	@Override
	public String toString() {
		return "Player [poh=" + poh + ", pohClip=" + pohClip + ", controller=" + controller + ", movement=" + movement + ", dialogueBuilder=" + dialogueBuilder + ", optionDialogue=" + optionDialogue + ", enterAmountDialogue=" + enterAmountDialogue + ", extraHiscores=" + extraHiscores + ", pokemon=" + Arrays.toString(pokemon) + ", pokemonUnlocked=" + Arrays.toString(pokemonUnlocked)
				+ ", forceClosedClient=" + forceClosedClient + ", reconnectPlayer=" + reconnectPlayer + ", reconnectAttempts=" + reconnectAttempts + ", hasFocusOnClient=" + hasFocusOnClient + ", playerOwnedBot=" + playerOwnedBot + ", playerOwnedShopBot=" + playerOwnedShopBot + ", marketSearchResults=" + marketSearchResults
				+ ", lastMarketSearchIndex=" + lastMarketSearchIndex + ", debug=" + debug + ", debugNpc=" + debugNpc + ", itemSlotToDestoy=" + itemDestroyCode + ", enterAmount=" + enterAmount + ", playerShopSlots=" + playerShopSlots + ", MAX_PSHOP_SLOTS=" + MAX_PSHOP_SLOTS + ", playerCollect=" + playerCollect + ", sellingId=" + sellingId + ", sellingIdOriginal="
				+ sellingIdOriginal + ", sellingN=" + sellingN + ", sellingS=" + sellingS + ", playerShopItems=" + Arrays.toString(playerShopItems) + ", playerShopItemsN=" + Arrays.toString(playerShopItemsN) + ", playerShopItemsPrice=" + Arrays.toString(playerShopItemsPrice) + ", myShopClient=" + myShopClient + ", detectNull=" + detectNull + ", drawDistance=" + drawDistance + ", duelingClient="
				+ duelingClient + ", duelRuleTimer=" + duelRuleTimer + ", untradeableShopMap=" + untradeableShopMap + ", friendsUpdate=" + friendsUpdate + ", taskName=" + taskName + ", masterName=" + masterName + ", assignmentAmount=" + assignmentAmount
				+ ", partner=" + partner + ", slayerTask=" + slayerTask + ", slayerPoints=" + slayerPoints + ", duoPoints=" + duoPoints + ", tasksCompleted=" + slayerTasksCompleted + ", pendingRequest=" + pendingRequest + ", bossKills=" + bossKills + ", damageMap=" + damageMap + ", lastDayLoggedIn=" + lastDayLoggedIn + ", lastDayPkedOfYear=" + lastDayPkedOfYear + ", penguinPoints=" + penguinPoints
				+ ", clawTick=" + clawTick + ", claimedReward=" + claimedReward + ", tempVar=" + tempVar + ", demoting=" + demoting + ", currentRegion=" + currentRegion + ", cannonRegion=" + cannonRegion + ", inInstance=" + inInstance + ", lastX=" + lastX + ", lastY=" + lastY + ", tpaReqId=" + tpaReqId + ", tpRequested=" + tpRequested + ", isIdle=" + isIdle + ", timePlayed="
				+ timePlayed + ", hidePlayer=" + hidePlayer + ", spectatorMode=" + spectatorMode + ", donatedTotalAmount=" + donatedTotalAmount + ", runningTask=" + runningTask + ", eloRating=" + eloRating + ", killedPlayers=" + killedPlayers + ", attackedPlayers="
				+ attackedPlayers + ", lastKilledPlayers=" + lastKilledPlayers + ", lastConnectedFrom=" + lastConnectedFrom + ", foodToEatOnMainTick=" + foodToEatOnMainTick + ", objectHintX=" + objectHintX + ", objectHintY=" + objectHintY + ", objectHintPos=" + objectHintPos + ", playerHintId=" + playerHintId + ", inBossRoom="
				+ inBossRoom + ", gwdPrayerDelay=" + gwdPrayerDelay + ", dfsCharge=" + dfsCharge + ", dfsPause=" + dfsPause + ", toggledDfs=" + toggledDfs + ", attackMultiplier=" + attackMultiplier + ", rangedMultiplier=" + rangedMultiplier + ", defenceMultiplier=" + defenceMultiplier + ", leechMagicDelay=" + leechMagicDelay + ", magicMultiplier=" + magicMultiplier + ", strengthMultiplier="
				+ strengthMultiplier + ", immuneToPK=" + immuneToPK + ", activateLeechSap=" + activateLeechSap + ", clan=" + clan + ", lastClanChat=" + lastClanChat + ", herbloreItem1=" + herbloreItem1 + ", herbloreItem2=" + herbloreItem2 + ", doAnim=" + doAnim + ", oldItem=" + oldItem + ", oldItem2=" + oldItem2 + ", gainXp=" + gainXp
				+ ", gainXp2=" + gainXp2 + ", levelReq=" + levelReq + ", levelReq2=" + levelReq2 + ", newItem=" + newItem + ", newItem2=" + newItem2 + ", objectType=" + objectType + ", chance=" + chance + ", skillSpeed=" + skillSpeed + ", herbAmount=" + herbAmount + ", doingHerb=" + doingHerb + ", newHerb=" + newHerb + ", newXp=" + newXp + ", servantCharges=" + servantCharges
				+ ", servantItemFetchId=" + servantItemFetchId + ", houseServant=" + houseServant + ", jesterEmotes=" + jesterEmotes + ", lastFairyDistance=" + lastFairyDistance + ", combatRingType=" + combatRingType + ", resetChairAnim=" + resetChairAnim + ", playerIsInHouse=" + playerIsInHouse + ", toConsCoords=" + Arrays.toString(toConsCoords)
				+ ", buildFurnitureId=" + buildFurnitureId + ", buildFurnitureX=" + buildFurnitureX + ", buildFurnitureY=" + buildFurnitureY + ", portalSelected=" + portalSelected + ", dialogTeleports=" + Arrays.toString(dialogTeleports) + ", dialogShopIds=" + Arrays.toString(dialogShopIds) + ", dialogOptionIds=" + Arrays.toString(dialogOptionIds) + ", trollSpawned=" + trollSpawned
				+ ", zombieSpawned=" + zombieSpawned + ", golemSpawned=" + golemSpawned + ", treeSpawned=" + treeSpawned + ", currentShadow=" + currentShadow + ", summonedCanTeleport=" + summonedCanTeleport + ", summoned=" + summoned + ", familiarPouchId=" + familiarPouchId + ", geyserTitanDelay=" + geyserTitanDelay + ", geyserTitanTarget=" + geyserTitanTarget
				+ ", doneSteelTitanDelay=" + doneSteelTitanDelay + ", usingSummoningSpecial=" + usingSummoningSpecial + ", steelTitanDelay=" + steelTitanDelay + ", steelTitanTarget=" + steelTitanTarget + ", summoningMonsterId=" + summoningMonsterId + ", interfaceIdOpenMainScreen=" + interfaceIdOpenMainScreen + ", sidebarInterfaceOpen=" + sidebarInterfaceOpen + ", familiarID=" + familiarID + ", familiarIndex=" + familiarIndex + ", summoningSpecialPoints=" + summoningSpecialPoints + ", timeBetweenSpecials=" + timeBetweenSpecials + ", specHitTimer=" + specHitTimer + ", hasActivated=" + hasActivated + ", soakingBonus=" + Arrays.toString(soakingBonus) + ", soakDamage=" + soakDamage + ", soakDamage2=" + soakDamage2 + ", damageType="
				+ damageType + ", familiarName=" + familiarName + ", familiarTimer=" + familiarTimer + ", pouchData=" + Arrays.toString(pouchData) + ", restoreDelay=" + restoreDelay + ", hasOverloadBoost=" + hasOverloadBoost + ", displayMaxHit=" + displayMaxHit + ", displayAccuracyInfo=" + displayAccuracyInfo + ", playerTitle=" + playerTitle + ", titleColor="
				+ titleColor + ", hasEvent=" + hasEvent + ", tourStep=" + tourStep + ", walkableInterface=" + walkableInterface + ", halloweenEvent=" + halloweenEvent + ", safeDeath=" + safeDeath + ", expLock=" + expLock + ", crossbowDamage=" + crossbowDamage + ", cbowSpecType=" + cbowSpecType + ", yellPoints=" + yellPoints + ", walkToX=" + walkToX + ", walkToY=" + walkToY + ", noClip=" + noClip
				+ ", bankPinWarning=" + bankPinWarning + ", oI=" + oI + ", oX=" + oX + ", oY=" + oY + ", oH=" + oH + ", oF=" + oF + ", oT=" + oT + ", objectToRemove=" + objectToRemove + ", objectToRemoveTemp=" + objectToRemoveTemp + ", itemsToRemove=" + itemsToRemove + ", performingAction=" + performingAction
				+ ", cannonScanX=" + cannonScanX + ", cannonScanY=" + cannonScanY + ", cannonX=" + cannonX + ", cannonY=" + cannonY + ", cannonBalls=" + cannonBalls + ", cannonDecayed=" + cannonDecayed + ", cannonSetUp=" + cannonSetUp + ", cannonStatus=" + cannonStatus + ", cannonRangeNpcs=" + Arrays.toString(cannonRangeNpcs) + ", safeTimer="
				+ safeTimer + ", transformNpcId=" + transformNpcId + ", uniqueActionId=" + uniqueActionId + ", pkp=" + pkp + ", KC=" + KC + ", DC=" + DC + ", clanWarRule=" + Arrays.toString(clanWarRule) + ", antiqueSelect=" + selectedLampSkill 
				+ ", playerSkillProp=" + Arrays.toString(playerSkillProp) + ", playerSkilling=" + Arrays.toString(playerSkilling) + ", setPin=" + setPin
				+ ", bankPin=" + bankPin + ", teleporting=" + teleporting + ", muted=" + muted + ", lastReport=" + lastReport + ", level1=" + level1 + ", level2=" + level2 + ", level3=" + level3 + ", lastReported=" + lastReported + ", lastButton=" + lastButton + ", leatherType=" + leatherType + ", isWc=" + isWc + ", homeTele=" + homeTele
				+ ", homeTeleDelay=" + homeTeleDelay + ", quests=" + Arrays.toString(quests) + ", achievements=" + Arrays.toString(achievements) + ", titles=" + Arrays.toString(titles) + ", wcing=" + wcing + ", treeX=" + treeX + ", treeY=" + treeY + ", miscTimer=" + miscTimer
				+ ", lastCast=" + lastCast + ", rangeWeapon=" + rangeWeapon + ", openBankPinInterface=" + openBankPinInterface + ", hasBankPin=" + hasBankPin
				+ ", enterdBankpin=" + enterdBankpin + ", firstPinEnter=" + firstPinEnter + ", requestPinDelete=" + requestPinDelete + ", secondPinEnter=" + secondPinEnter + ", thirdPinEnter=" + thirdPinEnter + ", fourthPinEnter=" + fourthPinEnter + ", bankPinOpensBank=" + bankPinOpensBank + ", lastLoginDate=" + lastLoginDate + ", playerBankPin=" + playerBankPin + ", recoveryDelay="
				+ recoveryDelay + ", attemptsRemaining=" + attemptsRemaining + ", lastPinSettings=" + lastPinSettings + ", setPinDate=" + setPinDate + ", changePinDate=" + changePinDate + ", deletePinDate=" + deletePinDate + ", firstPin=" + firstPin + ", secondPin=" + secondPin + ", thirdPin=" + thirdPin + ", fourthPin=" + fourthPin + ", pinDeleteDateRequested=" + pinDeleteDateRequested
				+ ", bankPins=" + Arrays.toString(bankPins) + ", isActiveAtomicBoolean=" + isActiveAtomicBoolean + ", properLogout=" + properLogout + ", loginPacketsSent=" + loginPacketsSent + ", isBanking=" + isBanking + ", isCooking=" + isCooking + ", disconnected=" + disconnected + ", forceDisconnect=" + forceDisconnect + ", ruleAgreeButton=" + ruleAgreeButton
				+ "initialized=" + initialized + ", isSkulled=" + isSkulled + ", friendUpdate=" + friendUpdate + ", newPlayer=" + newPlayer + ", hasMultiSign=" + hasMultiSign + ", saveCharacter=" + saveCharacter + ", mouseButton=" + mouseButton + ", splitChat=" + splitChat + ", chatEffects=" + chatEffects + ", acceptAid=" + acceptAid + ", nextDialogue=" + nextDialogue
				+ ", usedSpecial=" + usedSpecial + ", vengOn=" + vengOn + ", addStarter=" + addStarter + ", accountFlagged=" + accountFlagged + ", setMode=" + setMode + ", isBobOpen=" + isBobOpen + ", lastBankTabOpen=" + lastBankTabOpen + ", usingSpellFromSpellBook=" + usingSpellFromSpellBook + ", refreshBank=" + refreshBank + ", lastChatId=" + lastChatId + ", dialogueId=" + dialogueId + ", specBarId=" + specBarId
				+ ", followId=" + followId + ", nextChat=" + nextChat + ", talkingNpc=" + talkingNpc + ", dialogueAction=" + dialogueAction + ", followDistance=" + followDistance + ", followId2=" + followId2 + ", barrageCount=" + barrageCount
				+ ", delayedDamage=" + delayedDamage + ", delayedDamage2=" + delayedDamage2 + ", npcKills=" + npcKills + ", magePoints=" + magePoints + ", clanId=" + clanId + ", autoRet=" + autoRet + ", pcDamage=" + pcDamage + ", xInterfaceId=" + xInterfaceId + ", xRemoveId=" + xRemoveId + ", xRemoveSlot=" + xRemoveSlot + ", tzhaarToKill=" + tzhaarToKill + ", tzhaarKilled=" + tzhaarKilled
				+ ", waveId=" + waveId + ", frozenBy=" + frozenBy + ", teleAction=" + teleAction + ", newPlayerAct=" + newPlayerAct + ", bonusAttack=" + bonusAttack + ", lastNpcAttacked=" + lastNpcAttacked + ", actionTimer=" + actionTimer + ", achievementsCompleted=" + achievementsCompleted
				+ ", prayerRenew=" + prayerRenew + ", combatTab=" + combatTab + ", combatTabButtonToggle=" + combatTabButtonToggle + ", weaponAnimation=" + weaponAnimation + ", rolePoints=" + Arrays.toString(rolePoints) + ", roleExp=" + Arrays.toString(roleExp) + ", lastClicked=" + lastClicked + ", lastEffigy=" + lastEffigy + ", lastLongKilled=" + lastLongKilled
				+ ", lastLongKilledBy=" + lastLongKilledBy + ", killedByPlayerName=" + killedByPlayerName + ", toggleRag=" + toggleRag + ", capeClicked=" + capeClicked + ", queueGmaulSpec=" + queueGmaulSpec + ", timers=" + Arrays.toString(timers) + ", variables=" + Arrays.toString(variables) + ", judgement=" + Arrays.toString(judgement)
				+ ", chatSettings=" + Arrays.toString(chatSettings) + ", unlockedCrowns=" + Arrays.toString(unlockedCrowns) + ", seasonalElo=" + Arrays.toString(seasonalElo) + ", loyaltyPoints=" + loyaltyPoints + ", donPoints=" + donPoints + ", pcPoints=" + pcPoints + ", agilityPoints=" + agilityPoints + ", killCount=" + killCount
				+ ", inKilnFightCave=" + inKilnFightCave + ", minigameTarget=" + minigameTarget + ", fairyCombo=" + fairyCombo + ", javaVersion=" + javaVersion + ", zeals=" + zeals + ", longName=" + longName + ", wildyKeys=" + wildyKeys + ", voidStatus=" + Arrays.toString(voidStatus) + ", itemKeptId=" + Arrays.toString(itemKeptId) + ", pouches="
				+ Arrays.toString(pouches) + ", POUCH_SIZE=" + Arrays.toString(POUCH_SIZE) + ", invSlot=" + Arrays.toString(invSlot) + ", equipSlot=" + Arrays.toString(equipSlot) + ", friends=" + Arrays.toString(friends) + ", ignores=" + Arrays.toString(ignores) + ", specAmount=" + specAmount + ", storing=" + storing
				+ ", attackingIndex=" + attackingIndex + ", teleGrabItem=" + teleGrabItem + ", teleGrabX=" + teleGrabX + ", teleGrabY=" + teleGrabY + ", duelCount=" + duelCount + ", underAttackBy=" + underAttackBy + ", underAttackBy2=" + underAttackBy2 + ", wildLevel=" + wildLevel + ", respawnTimer=" + respawnTimer + ", saveTimer="
				+ saveTimer + ", poisonDelay=" + poisonDelay + ", teleTimer=" + teleTimer + ", lastPlayerMove=" + lastPlayerMove + ", lastSpear=" + lastSpear + ", lastProtItem=" + lastProtItem + ", lastVeng=" + lastVeng
				+ ", lastYell=" + lastYell + ", teleGrabDelay=" + teleGrabDelay + ", lastAction=" + lastAction + ", lastThieve=" + lastThieve + ", alchDelay=" + alchDelay + ", duelDelay=" + duelDelay
				+ ", anyDelay2=" + anyDelay2 + ", npcAggroTick=" + npcAggroTick + ", foodDelayTick=" + foodDelayTick + ", karamTick=" + karamTick + ", potionTick=" + potionTick + ", canChangeAppearance=" + canChangeAppearance + ", questPoints=" + questPoints + ", applyDeath="
				+ applyDeath + ", respawnX=" + respawnX + ", respawnY=" + respawnY + ", respawnZ=" + respawnZ + ", randomEvent=" + randomEvent + ", quickPrayers=" + quickPrayers + ", quickCurses=" + quickCurses + ", quickPray=" + quickPray + ", lastActive=" + lastActive
				+ ", interactingObject=" + interactingObject + ", finalLocalDestX=" + finalLocalDestX + ", finalLocalDestY=" + finalLocalDestY + ", destinationX=" + destinationX + ", destinationY=" + destinationY + ", walkingToObject=" + walkingToObject
				+ ", keepItems=" + Arrays.toString(keepItems) + ", keepItemsN=" + Arrays.toString(keepItemsN) + ", WillKeepAmt1=" + WillKeepAmt1 + ", WillKeepAmt2=" + WillKeepAmt2 + ", WillKeepAmt3=" + WillKeepAmt3 + ", WillKeepAmt4=" + WillKeepAmt4 + ", WillKeepItem1=" + WillKeepItem1 + ", WillKeepItem2=" + WillKeepItem2 + ", WillKeepItem3=" + WillKeepItem3 + ", WillKeepItem4=" + WillKeepItem4
				+ ", WillKeepItem1Slot=" + WillKeepItem1Slot + ", WillKeepItem2Slot=" + WillKeepItem2Slot + ", WillKeepItem3Slot=" + WillKeepItem3Slot + ", WillKeepItem4Slot=" + WillKeepItem4Slot + ", EquipStatus=" + EquipStatus 
				+ ", playerIsSkilling=" + playerIsSkilling + ", magicDef=" + magicDef + ", teleotherType=" + teleotherType + ", doAmount=" + doAmount + ", lastDamageDealt=" + lastDamageDealt + ", lastHitWasSpec=" + lastHitWasSpec + ", lastSkillingAction=" + lastSkillingAction + ", barrows=" + Arrays.toString(barrows) + ", barrowsKillCount=" + barrowsKillCount 
				+ ", usingPrayer=" + usingPrayer + ", virusTimer=" + virusTimer + ", virusDamage=" + virusDamage + ", prayerActive=" + Arrays.toString(prayerActive) + ", duoPrayerEnabled="
				+ duoPrayerEnabled + ", curseActive=" + Arrays.toString(curseActive) + ", duelTimer=" + duelTimer + ", duelTeleX=" + duelTeleX + ", duelTeleY=" + duelTeleY + ", duelSlot=" + duelSlot + ", duelSpaceReq=" + duelSpaceReq + ", duelOption=" + duelOption + ", duelingWith=" + duelingWith + ", duelStatus=" + duelStatus + ", headIconPk=" + headIconPk + ", headIconHints=" + headIconHints
				+ ", duelRequested=" + duelRequested + ", duelRule=" + Arrays.toString(duelRule) + ", loadPrevDuelRules=" + loadPrevDuelRules + ", prevDuelRule=" + Arrays.toString(prevDuelRule) + ", usingSpecial=" + usingSpecial + ", npcDroppingItems=" + npcDroppingItems + ", autoCastingSpell=" + autoCastingSpell + ", singleCastSpell=" + singleCastSpell + ", playerIndex=" + playerIndex
				+ ", oldPlayerIndex=" + oldPlayerIndex + ", recentPlayerHitIndex=" + recentPlayerHitIndex + ", crystalBowArrowCount=" + crystalBowArrowCount + ", playerMagicBook=" + playerMagicBook + ", playerPrayerBook=" + playerPrayerBook + ", teleEndAnimation=" + teleEndAnimation + ", teleEndGfx=" + teleEndGfx + ", teleHeight=" + teleHeight + ", teleX=" + teleX
				+ ", teleY=" + teleY + ", killingNpcIndex=" + killingNpcIndex + ", oldNpcIndex=" + oldNpcIndex + ", fightMode=" + fightMode + ", npcIndex=" + npcIndex + ", npcClickIndex=" + npcClickIndex + ", npcType=" + npcType + ", lastButtonWasControlledStyle="
				+ lastButtonWasControlledStyle + ", isRooted()=" + isRooted() + ", usingWhiteWhipSpec=" + usingWhiteWhipSpec + ", usingSOLSpec=" + usingSOLSpec + ", teleCooldown=" + teleCooldown
				+ ", passedCheatClientTest=" + passedCheatClientTest + ", fightXp=" + fightXp + ", lastDamageType=" + lastDamageType + ", clickNpcType=" + clickNpcType + ", objectId="
				+ objectId + ", objectX=" + objectX + ", objectY=" + objectY + ", objectXOffset=" + objectXOffset + ", objectYOffset=" + objectYOffset + ", objectDistance=" + objectDistance + ", prevObjectId=" + prevObjectId + ", prevObjectX=" + prevObjectX + ", prevObjectY=" + prevObjectY + ", clickObjectType=" + clickObjectType + ", clickedObject=" + clickedObject + ", pItemX=" + pItemX
				+ ", pItemY=" + pItemY + ", pItemId=" + pItemId + ", isMoving=" + isMoving + ", walkingToItem=" + walkingToItem + ", keepWalkFlag=" + keepWalkFlag + ", openDuel=" + openDuel + ", isShopping=" + isShopping + ", updateShop=" + updateShop + ", myShopId=" + myShopId + ", tradeStatus=" + tradeStatus + ", tradeWith=" + tradeWith + ", inDuel=" + inDuel + ", tradeAccepted=" + tradeAccepted
				+ ", goodTrade=" + goodTrade + ", inTrade=" + inTrade + ", tradeRequested=" + tradeRequested + ", tradeConfirmed=" + tradeConfirmed + ", tradeConfirmed2=" + tradeConfirmed2 + ", canOffer=" + canOffer + ", acceptTrade=" + acceptTrade + ", acceptedTrade=" + acceptedTrade + ", attackAnim=" + attackAnim
				+ ", usedToRun=" + usedToRun + ", takeAsNote=" + takeAsNote + ", combatLevel=" + combatLevel + ", saveFile=" + saveFile + ", playerAppearance=" + Arrays.toString(playerAppearance) + ", apset=" + apset + ", actionID=" + actionID + ", wearItemTimer=" + wearItemTimer + ", wearId=" + wearId + ", wearSlot="
				+ wearSlot + ", interfaceId=" + interfaceId + ", tutorial=" + tutorial + ", usingGamesNeck=" + usingGamesNeck + ", usingSkillsNeck=" + usingSkillsNeck + ", burnName=" + burnName + ", woodcut=" + Arrays.toString(woodcut) + ", wcTimer=" + wcTimer + ", mining=" + Arrays.toString(mining) + ", miningTimer=" + miningTimer
				+ ", fishing=" + fishing + ", fishTimer=" + fishTimer + ", smeltInterface=" + smeltInterface + ", patchCleared=" + patchCleared + ", farm=" + Arrays.toString(farm) + ", playerIsFarming=" + playerIsFarming + ", patches=" + Arrays.toString(patches) + ", queuedAnimation=" + queuedAnimation
				+ ", antiFirePot=" + antiFirePot + ", killStreak=" + killStreak + ", highestKillStreak=" + highestKillStreak + ", freshOnTheHunt=" + freshOnTheHunt + ", huntPlayerIndex=" + huntPlayerIndex + ", lastHintId=" + lastHintId + ", savedGame=" + savedGame + ", saveGameInProcess=" + saveGameInProcess + ", huntedBy=" + huntedBy + ", sidebarInterfaceInfo=" + sidebarInterfaceInfo
				+ ", freezeOnHit=" + freezeOnHit + ", pcParticipation=" + pcParticipation + ", capturableProgress=" + capturableProgress + ", soulWarsTeam=" + soulWarsTeam + ", soulWarsInactivity=" + soulWarsInactivity + ", castleWarsTeam=" + castleWarsTeam + ", inCwGame=" + inCwGame + ", onCwWalls=" + onCwWalls + ", inCwWait=" + inCwWait + ", waitRoomCountDown=" + waitRoomCountDown
				+ ", waitingRoomCampTimer=" + waitingRoomCampTimer + ", hasCough=" + hasCough + ", inPits=" + inPits + ", pitsStatus=" + pitsStatus + ", blockedShot=" + blockedShot + ", withinRangeToHit=" + withinRangeToHit + ", withinDistanceToStopWalking=" + withinDistanceToStopWalking + ", duelScreenIsDuel=" + duelScreenIsDuel + ", clanWarOpponent=" + clanWarOpponent + ", chalAccepted="
				+ chalAccepted + ", chalInterface=" + chalInterface + ", duelingClientClanWar=" + duelingClientClanWar + ", clanWarPlayerStake=" + clanWarPlayerStake + ", toggleBot=" + toggleBot + ", connectedFrom=" + connectedFrom + ", globalMessage=" + globalMessage + ", playerName=" + playerName + ", playerPass=" + playerPass
				+ ", UUID=" + UUID + ", oldUUID=" + oldUUID + ", playerRights=" + playerRights + ", mySQLIndex=" + mySQLIndex + ", xenforoIndex=" + xenforoIndex + ", checkXenLink=" + checkXenLink + ", skipPassword=" + skipPassword + ", inVerificationState=" + inVerificationState + ", playerItems=" + Arrays.toString(playerItems) + ", playerItemsN=" + Arrays.toString(playerItemsN)
				+ ", runePouchItems=" + Arrays.toString(runePouchItems) + ", runePouchItemsN=" + Arrays.toString(runePouchItemsN) + ", bankingItems=" + Arrays.toString(bankingItems) + ", bankingItemsN=" + Arrays.toString(bankingItemsN) + ", moneyPouch=" + moneyPouch + ", updateInvInterface=" + updateInvInterface + ", droppingItem=" + droppingItem + ", updateEquipmentRequired="
				+ updateEquipmentRequired + ", updateEquipment=" + Arrays.toString(updateEquipment) + ", wearingItemOnTick=" + wearingItemOnTick + ", itemsToKeep=" + itemsToKeep + ", listenPM=" + listenPM + ", bankingTab=" + bankingTab
				+ ", lastBankItem=" + lastBankItem + ", lastBankTab=" + lastBankTab + ", firstBankTab=" + firstBankTab + ", bankItems0=" + Arrays.toString(bankItems0) + ", bankItems0N=" + Arrays.toString(bankItems0N) + ", bankItems1=" + Arrays.toString(bankItems1) + ", bankItems1N=" + Arrays.toString(bankItems1N) + ", bankItems2=" + Arrays.toString(bankItems2) + ", bankItems2N="
				+ Arrays.toString(bankItems2N) + ", bankItems3=" + Arrays.toString(bankItems3) + ", bankItems3N=" + Arrays.toString(bankItems3N) + ", bankItems4=" + Arrays.toString(bankItems4) + ", bankItems4N=" + Arrays.toString(bankItems4N) + ", bankItems5=" + Arrays.toString(bankItems5) + ", bankItems5N=" + Arrays.toString(bankItems5N) + ", bankItems6=" + Arrays.toString(bankItems6)
				+ ", bankItems6N=" + Arrays.toString(bankItems6N) + ", bankItems7=" + Arrays.toString(bankItems7) + ", bankItems7N=" + Arrays.toString(bankItems7N) + ", bankItems8=" + Arrays.toString(bankItems8) + ", bankItems8N=" + Arrays.toString(bankItems8N) + ", bankNotes=" + bankNotes + ", playerEquipment=" + Arrays.toString(playerEquipment) + ", displayEquipment=" + Arrays.toString(displayEquipment) + ", playerEquipmentN=" + Arrays.toString(playerEquipmentN)
				+ ", difficulty=" + difficulty + ", homeTeleActions=" + homeTeleActions + ", zulrah=" + zulrah + ", localPlayers=" + localPlayers + ", npcList=" + Arrays.toString(npcList) + ", npcListSize=" + npcListSize + ", npcInListBitmap=" + Arrays.toString(npcInListBitmap) + ", correctlyInitialized=" + correctlyInitialized + ", saveInProgressForDC=" + saveInProgressForDC + ", skipThreadCheck=" + skipThreadCheck + ", firstWarning="
				+ firstWarning + ", startedCheckSpins=" + startedCheckSpins + ", mapRegionX=" + mapRegionX + ", mapRegionY=" + mapRegionY + ", regionCheckX=" + regionCheckX + ", regionCheckY=" + regionCheckY + ", regionCheckZ=" + regionCheckZ + ", absX=" + absX + ", absY=" + absY + ", currentX=" + currentX + ", currentY=" + currentY + ", noClipX=" + noClipX + ", noClipY=" + noClipY + ", oldX="
				+ oldX + ", oldY=" + oldY + ", heightLevel=" + heightLevel + ", playerSE=" + playerSE + ", playerSEW=" + playerSEW + ", playerSER=" + playerSER + ", multiWay=" + multiWay + ", receivedPacket=" + Arrays.toString(receivedPacket) + ", isRunning=" + isRunning + ", teleportToX=" + teleportToX + ", teleportToY=" + teleportToY + ", teleportToZ=" + teleportToZ + ", lastRunRecovery="
				+ lastRunRecovery + ", runRecoverBoost=" + runRecoverBoost + ", didTeleport=" + didTeleport + ", mapRegionDidChange=" + mapRegionDidChange + ", updateRegion=" + updateRegion + ", dir1=" + dir1 + ", dir2=" + runTiles + ", createItems=" + createItems + ", poimiX=" + poimiX + ", poimiY=" + poimiY + ", playerProps=" + playerProps + ", chatText=" + Arrays.toString(chatText)
				+ ", chatTextSize=" + chatTextSize + ", chatTextColor=" + chatTextColor + ", chatTextEffects=" + chatTextEffects + ", x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + ", speed1=" + speed1 + ", speed2=" + speed2 + ", direction=" + direction + ", combatEventsToProcess="
				+ combatEventsToProcess + ", disabled=" + disabled + ", burdenedItems=" + Arrays.toString(burdenedItems) + "]";
	}

	public CompCape getCompCape() {
		return compCape;
	}

	public void setCompCape(CompCape compCape) {
		this.compCape = compCape;
	}

	public boolean isLoadedRegion() {
		return loadedRegion;
	}

	public void setLoadedRegion(boolean loadedRegion) {
		this.loadedRegion = loadedRegion;
	}
	
	public boolean isNotReadyForMinigame() {
		return !isActive || disconnected || teleportToX > 0;
	}
	
	public void restoreSpecialAttackFull() {
		specAmount = 10.0;
		getItems().addSpecialBar(true);
	}

	public TreasureTrailManager getTreasureTrailManager() {
		return treasureTrailManager;
	}
	
	public boolean isRigourUnlocked() {
		if (tournamentInstance != null)
			return true;
		return Misc.testBit(Variables.PRAYER_SCROLL.getValue(this), 0);
	}
	
	public boolean isAuguryUnlocked() {
		if (tournamentInstance != null)
			return true;
		return Misc.testBit(Variables.PRAYER_SCROLL.getValue(this), 1);
	}
	
	public void unlockRigour() {
		int value = Misc.setBit(Variables.PRAYER_SCROLL.getValue(this), 0);
		Variables.PRAYER_SCROLL.setValue(this, value);
	}
	
	public void unlockAugury() {
		int value = Misc.setBit(Variables.PRAYER_SCROLL.getValue(this), 1);
		Variables.PRAYER_SCROLL.setValue(this, value);
	}
	
	public void setDefaultDiscordStatus() {
		getPacketSender().sendDiscordUpdate("", "Doing SoulPlay Things", 0, 0, System.currentTimeMillis()/1000, 0);
	}
	
	public DialogueHandler getDH() {
		return dialogueHandler;
	}

	public SeasonalData getSeasonalData() {
		return seasonalData;
	}

	public void setClone(PlayerClone clone) {
		this.clone = clone;
		this.getUpdateFlags().add(UpdateFlag.CLONE);
	}

	public void resetClone() {
		this.clone = null;
		this.getUpdateFlags().add(UpdateFlag.CLONE);
	}

}
