package com.soulplay.game.model.player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.soulplay.content.minigames.gamble.GambleZone;
import com.soulplay.content.minigames.warriorsguild.CyclopesZone;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareLobbyZone;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareZone;
import com.soulplay.game.world.map.travel.zone.Zone;
import com.soulplay.game.world.map.travel.zone.impl.*;

public class PlayerZones {

	public PlayerZones() {
		for (Zone zone : defaultZones)
			zones.add(zone);
	}

	private final Zone nightmareZone = new NightmareZone();

	private final Zone nightmareLobbyZone = new NightmareLobbyZone();

	private final Zone edgePkZone = new EdgePkZone();
	
	private final Zone multiZone = new MultiZone();
	
	private final Zone dmmTourney = new DmmTournamentZone();
	
	private final Zone dmmLobby = new DmmLobbyZone();
	
	private final Zone lmsArena = new LMSArenaZone();
	
	private final Zone clwRedPortalPk = new ClwRedPortalZone();
	
	private final Zone revDung = new RevenantDungeonZone();

	private final Zone karuulmSlayerDungeon = new KaruulmSlayerDungeonZone();

	private final Zone gambleZone = new GambleZone();

	private final Zone cyclopesZone = new CyclopesZone();

	private final Set<Zone> zones = new HashSet<>();
	
	public Set<Zone> getZoneList() {
		return zones;
	}
	
	public void addDynamicZone(Zone zone) {
		this.zones.add(zone);//hopefully adds unique might need to convert list
	}
	
	public void removeDynamicZone(Zone zone) {
		removedZones.add(zone);
	}
	
	public boolean containsZoneInList(Zone zone) {
		return getZoneList().contains(zone);
	}
	
	public final List<Zone> removedZones = new ArrayList<>();
	
	private final Zone[] defaultZones = { gambleZone, edgePkZone, dmmTourney, dmmLobby, lmsArena, clwRedPortalPk, revDung, karuulmSlayerDungeon, nightmareZone, nightmareLobbyZone, cyclopesZone };
	
	public boolean isInEdgePk() {
		return edgePkZone.isInsideZone();
	}

	public Zone getEdgePkZone() {
		return edgePkZone;
	}
	
	public boolean isInMulti() {
		return multiZone.isInsideZone();
	}
	
	public Zone getMultiZone() {
		return multiZone;
	}
	
	public boolean isInDmmTourn() {
		return dmmTourney.isInsideZone();
	}
	
	public boolean isInDmmLobby() {
		return dmmLobby.isInsideZone();
	}
	
	public boolean isInClwRedPortalPkZone() {
		return clwRedPortalPk.isInsideZone();
	}
	
	public boolean isInRevDung() {
		return revDung.isInsideZone();
	}

	public boolean isInKaruulmSlayerDungeon() {
		return karuulmSlayerDungeon.isInsideZone();
	}

	public boolean isInNightmare() {
		return nightmareZone.isInsideZone();
	}

	public boolean isInGambleZone() {
		return gambleZone.isInsideZone();
	}

	public boolean isInCyclopesZone() {
		return cyclopesZone.isInsideZone();
	}

}
