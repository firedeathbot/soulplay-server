package com.soulplay.game.model.player.dialog;

import com.soulplay.game.model.player.Player;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * Represents a base dialogue in which all dialogues have some form of text and can be invoked by a player.
 *
 * @author Nshusa
 */
public abstract class Dialogue implements Consumer<Player> {

    protected String[] lines;

    public Dialogue(String[] lines) {
        this.lines = lines;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Dialogue) {
            Dialogue other = (Dialogue) o;
            return Arrays.equals(lines, other.lines);
        }
        return false;
    }

}
