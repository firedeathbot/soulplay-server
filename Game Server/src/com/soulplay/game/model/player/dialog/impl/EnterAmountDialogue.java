package com.soulplay.game.model.player.dialog.impl;

import java.util.Objects;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;

public class EnterAmountDialogue extends Dialogue {

    private Runnable action;

    public EnterAmountDialogue(Runnable action) {
        super(null);
        this.action = action;
    }

    public Runnable getAction() {
        return action;
    }

    @Override
    public void accept(Player player) {
        player.getPacketSender().sendEnterAmountInterface();
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnterAmountDialogue other = (EnterAmountDialogue) obj;
		return Objects.equals(action, other.action);
	}

}