package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;
import com.soulplay.game.model.player.dialog.Expression;

import static com.soulplay.game.model.player.dialog.DialogueUtils.validateLength;

public final class NpcDialogue extends Dialogue {

    private final Expression expression;
    private int id = -1;

    public NpcDialogue(String... lines) {
        this(Expression.DEFAULT, lines);
    }

    public NpcDialogue(Expression expression, String... lines) {
        super(lines);
        this.expression = expression;
    }

    public NpcDialogue(int id, String... lines) {
        this(id, Expression.DEFAULT, lines);
    }

    public NpcDialogue(int id, Expression expression, String... lines) {
        super(lines);
        this.id = id;
        this.expression = expression;
    }

    @Override
    public void accept(Player player) {
        validateLength(lines);

        if (id == -1) {
            id = player.npcType;
        }

        final String npcName = NPCHandler.getNpcName(id);

        switch (lines.length) {
            case 1:
                player.getPacketSender().showAnimationOnInterfacePacket(4883, expression.getId());
                player.getPacketSender().sendFrame126(npcName, 4884);
                player.getPacketSender().sendFrame126(lines[0], 4885);
                player.getPacketSender().drawNpcOnInterface(id, 4883);
                player.getPacketSender().sendChatInterface(4882, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 2:
                player.getPacketSender().showAnimationOnInterfacePacket(4888, expression.getId());
                player.getPacketSender().sendFrame126(npcName, 4889);
                player.getPacketSender().sendFrame126(lines[0], 4890);
                player.getPacketSender().sendFrame126(lines[1], 4891);
                player.getPacketSender().drawNpcOnInterface(id, 4888);
                player.getPacketSender().sendChatInterface(4887, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 3:
                player.getPacketSender().showAnimationOnInterfacePacket(4894, expression.getId());
                player.getPacketSender().sendFrame126(npcName, 4895);
                player.getPacketSender().sendFrame126(lines[0], 4896);
                player.getPacketSender().sendFrame126(lines[1], 4897);
                player.getPacketSender().sendFrame126(lines[2], 4898);
                player.getPacketSender().drawNpcOnInterface(id, 4894);
                player.getPacketSender().sendChatInterface(4893, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 4:
                player.getPacketSender().showAnimationOnInterfacePacket(4901, expression.getId());
                player.getPacketSender().sendFrame126(npcName, 4902);
                player.getPacketSender().sendFrame126(lines[0], 4903);
                player.getPacketSender().sendFrame126(lines[1], 4904);
                player.getPacketSender().sendFrame126(lines[2], 4905);
                player.getPacketSender().sendFrame126(lines[3], 4906);
                player.getPacketSender().drawNpcOnInterface(id, 4901);
                player.getPacketSender().sendChatInterface(4900, player.getDialogueBuilder().isCloseAllWindows());
                break;

            default:
                System.err.println(String.format("Invalid npc dialogue line length: %s", lines.length));
                break;
        }
    }

}