package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;

/**
 * Represents a {@link Dialogue} that executes an action instead of displaying
 * lines of text.
 *
 * @author Nshusa
 */
public class ActionDialogue extends Dialogue {

	private final Runnable action;

	public ActionDialogue(Runnable action) {
		super(new String[] {});
		this.action = action;
	}

	@Override
	public void accept(Player player) {
		action.run();
	}

	@Override
	public boolean equals(Object o) {
		if (super.equals(o) && o instanceof ActionDialogue) {
			return ((ActionDialogue) o).action == action;
		}
		return false;
	}

}
