package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;

public class DestoryItemDialogue extends Dialogue {
    
    private final int itemId;

    public DestoryItemDialogue(int itemId) {
        super(null);
        this.itemId = itemId;
    }

    @Override
    public void accept(Player player) {
        final ItemDefinition def = ItemDefinition.forId(itemId);
        if (def == null) {
        	player.sendMessage("Invalid item: " + itemId);
        	return;
        }

        final String name = def.getName();
        player.getPacketSender().displayItemOnInterface(14171, itemId, 0, 1);
        player.getPacketSender().sendFrame126("Are you sure you want to drop this item?", 14174);
        player.getPacketSender().sendFrame126("Yes", 14175);
        player.getPacketSender().sendFrame126("No", 14176);
        player.getPacketSender().sendFrame126("", 14177);
        player.getPacketSender().sendFrame126("This item is valuable, you will not", 14182);
        player.getPacketSender().sendFrame126("get it back once lost.", 14183);
        player.getPacketSender().sendFrame126(name, 14184);
        player.getPacketSender().sendChatInterface(14170, player.getDialogueBuilder().isCloseAllWindows());
        player.itemDestroyCode = () -> player.getItems().deleteItemInSlot(player.getItems().getItemSlot(itemId));
    }

}
