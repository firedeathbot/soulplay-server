package com.soulplay.game.model.player.dialog;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.impl.*;

import java.util.*;

import static com.soulplay.game.model.player.dialog.DialogueUtils.validateLength;

/**
 * A class designed to make dialogues using a fluent interface.
 *
 * @author Nshusa
 */
public final class DialogueBuilder {

    private final List<Dialogue> list = new ArrayList<>();
    private final Player player;
    private int stage = 0;
    private Map<Integer, Integer> checkpoints = new HashMap<>();
    private boolean active;
    private boolean closeAllWindows = true;
    private String enterText = "";
    private Optional<Runnable> closeAction = Optional.empty();

    public DialogueBuilder(Player player) {
        this.player = player;
    }

    public DialogueBuilder sendAction(Runnable action) {
        append(new ActionDialogue(action));
        return this;
    }

    public DialogueBuilder onReset(Runnable action) {
        closeAction = Optional.ofNullable(action);
        return this;
    }

    public DialogueBuilder nullResetAction() {
        closeAction = Optional.empty();
        return this;
    }
    
    public void reset() {
    	reset(true);
    }

    public void reset(boolean hardReset) {
    	if (hardReset)
    		closeAction.ifPresent(Runnable::run);
        closeAction = Optional.empty();
        stage = 0;
        checkpoints.clear();
        player.setOptionDialogue(Optional.empty());
        player.setEnterAmountDialogue(Optional.empty());
        player.enterAmount = 0;
        setEnterText("");
        setActive(false);
        list.clear();
        if (hardReset && closeAllWindows) {
            player.getPA().closeAllWindows();
        }
        if (hardReset)
        	closeAllWindows = true;
    }

    private DialogueBuilder append(Dialogue abstractDialogue) {
        if (list.contains(abstractDialogue)) {
            return this;
        }
        
        list.add(abstractDialogue);
        return this;
    }

    public void executeOption(int type) {
        if (!player.getOptionDialogue().isPresent()) {
            return;
        }

        OptionDialogue optionDialogue = player.getOptionDialogue().get();

        if (type >= optionDialogue.getActions().size()) {
            return;
        }

        optionDialogue.getActions().get(type).run();
        execute();
    }
    
    //NOTE: this is the same function as createCheckpointByListSize(int id)
    public DialogueBuilder createCheckpointInstant(int id) {
    	int size = list.size();
            checkpoints.put(id, size);

            if (player.debug) {
                player.sendMessage("2checkpoints:" + id + " stage:" + size);
//                System.out.println("2checkpoints:" + id + " stage:" + size);
            }
        return this;
    }

    public DialogueBuilder createCheckpoint(int id) {
        sendAction(() -> {
            checkpoints.put(id, stage);

            if (player.debug) {
                player.sendMessage("checkpoints:" + id + " stage:" + stage);
//                System.out.println("checkpoints:" + id + " stage:" + stage);
            }
        });
        return this;
    }

    //if placing right before any builder, like npcchat or statement, it will set it the checkpoint
    public DialogueBuilder createCheckpointByListSize(int id) {
    	checkpoints.put(id, list.size());
//    	System.out.println("id:"+id+" on jump:"+list.size());//useful for tracking where to jump to XD
    	return this;
    }

    public DialogueBuilder createCheckpoint() {
        return createCheckpoint(0);
    }

    public DialogueBuilder checkpointList() {
        sendAction(() -> checkpoints.put(0, list.size()));
        return this;
    }

    public DialogueBuilder executeIfNotActive() {
        if (!isActive())
            return execute();

        return this; // else just add to the list only.
    }

    public DialogueBuilder execute() {
        return execute(closeAllWindows);
    }

    public DialogueBuilder execute(boolean closeWhenFinished) {
        closeAllWindows = closeWhenFinished;

        if (stage < list.size()) {
            Dialogue entry = list.get(stage);

            if (entry instanceof OptionDialogue) {
                OptionDialogue option = (OptionDialogue) entry;
                player.setOptionDialogue(Optional.of(option));
            } else if (entry instanceof EnterAmountDialogue) {
                EnterAmountDialogue enterAmount = (EnterAmountDialogue) entry;
                player.setEnterAmountDialogue(Optional.of(enterAmount));
            } else if (entry instanceof EnterTextDialogue) {
                EnterTextDialogue enterText = (EnterTextDialogue) entry;
                player.setEnterTextDialogue(Optional.of(enterText));
            } else if (entry instanceof ActionDialogue) {
                ActionDialogue actionDialogue = (ActionDialogue) entry;
                actionDialogue.accept(player);
                setActive(true);
                stage++;
                execute(closeWhenFinished);
                return this;
            }

            setActive(true);
            entry.accept(player);
            stage++;
        } else {
            reset();
        }
        return this;
    }

    public DialogueBuilder sendPlayerChat(String... lines) {
        return append(new PlayerDialogue(lines));
    }

    public DialogueBuilder sendPlayerChat(Expression expression, String... lines) {
        return append(new PlayerDialogue(expression, lines));
    }

    //TODO: this needs npc id!
    public DialogueBuilder sendNpcChat(String... lines) {
        if (player.npcType != -1) {
            return append(new NpcDialogue(player.npcType, lines));
        }
        return append(new NpcDialogue(lines));
    }

    public DialogueBuilder sendNpcChat(Expression expression, String... lines) {
        if (player.npcType != -1) {
            return append(new NpcDialogue(player.npcType, expression, lines));
        }
        return append(new NpcDialogue(expression, lines));
    }

    public DialogueBuilder sendNpcChat(int id, String... lines) {
        return append(new NpcDialogue(id, Expression.DEFAULT, lines));
    }

    public DialogueBuilder sendNpcChat(int id, Expression expression, String... lines) {
        return append(new NpcDialogue(id, expression, lines));
    }

    public DialogueBuilder sendOption(String option1, Runnable action1, String option2, Runnable action2) {
        return append(new OptionDialogue(null, option1, action1, option2, action2));
    }

    public DialogueBuilder sendOption(String option1, Runnable action1, String option2, Runnable action2,
                                      String option3, Runnable action3) {
        return append(new OptionDialogue(null, option1, action1, option2, action2, option3, action3));
    }

    public DialogueBuilder sendOption(String option1, Runnable action1, String option2, Runnable action2,
                                      String option3, Runnable action3, String option4, Runnable action4) {
        return append(new OptionDialogue(null, option1, action1, option2, action2, option3, action3, option4, action4));
    }

    public DialogueBuilder sendOption(String option1, Runnable action1, String option2, Runnable action2,
                                      String option3, Runnable action3, String option4, Runnable action4, String option5, Runnable action5) {
        return append(new OptionDialogue(null, option1, action1, option2, action2, option3, action3, option4, action4,
                option5, action5));
    }

    public DialogueBuilder sendOption(String title, String option1, Runnable action1, String option2, Runnable action2) {
        return append(new OptionDialogue(title, option1, action1, option2, action2));
    }

    public DialogueBuilder sendOption(String title, String option1, Runnable action1, String option2, Runnable action2,
                                      String option3, Runnable action3) {
        return append(new OptionDialogue(title, option1, action1, option2, action2, option3, action3));
    }

    public DialogueBuilder sendOption(String title, String option1, Runnable action1, String option2, Runnable action2,
                                      String option3, Runnable action3, String option4, Runnable action4) {
        return append(new OptionDialogue(title, option1, action1, option2, action2, option3, action3, option4, action4));
    }

    public DialogueBuilder sendOption(String title, String option1, Runnable action1, String option2, Runnable action2,
                                      String option3, Runnable action3, String option4, Runnable action4, String option5, Runnable action5) {
        return append(new OptionDialogue(title, option1, action1, option2, action2, option3, action3, option4, action4,
                option5, action5));
    }

    public DialogueBuilder appendOption(OptionDialogue dialogue) {
        return append(dialogue);
    }

    public DialogueBuilder sendEnterAmount(Runnable action) {
        append(new EnterAmountDialogue(action));
        return this;
    }

    public void executeEnterAmount() {
        if (player.getEnterAmountDialogue().isPresent()) {
            EnterAmountDialogue enterAmount = player.getEnterAmountDialogue().get();
            enterAmount.getAction().run();
            player.enterAmount = 0;
        }
        execute();
    }

    public DialogueBuilder sendEnterText(Runnable action) {
        append(new EnterTextDialogue(action));
        return this;
    }

    public void executeEnterText() {
        if (player.getEnterTextDialogue().isPresent()) {
            EnterTextDialogue enterText = player.getEnterTextDialogue().get();
            enterText.getAction().run();
            this.setEnterText("");
        }
        execute();
    }

    public DialogueBuilder sendItemStatement(int itemId, String... lines) {
        validateLength(lines);
        append(new ItemStatementDialogue(itemId, lines));
        return this;
    }

    public DialogueBuilder sendDestoryItem(int itemId) {
        append(new DestoryItemDialogue(itemId));
        return this;
    }

    public DialogueBuilder sendStatement(String... lines) {
        validateLength(lines);
        append(new StatementDialogue(lines));
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void returnToCheckpoint() {
        returnToCheckpoint(0);
    }

    public void returnToCheckpoint(int id) {
        final int checkpoint = checkpoints.getOrDefault(id, -1);

        if (checkpoint == -1) {
            return;
        }

        this.stage = checkpoint;
        return;
    }
    
    public void returnToCheckpointFromSendAction(int id) {
        final int checkpoint = checkpoints.getOrDefault(id, -1) - 1;
        
        if (checkpoint <= -1) {
            return;
        }

        this.stage = checkpoint;
        return;
    }
    
    public DialogueBuilder jumpToStage(int stageId) {
    	this.stage = stageId;
    	return this;
    }

    public void close() {
        stage++;
        player.getPA().closeAllWindows();
    }
    
    public void getStagDebug() {
    	System.out.println("stage:"+stage);
    }
    
    public void closeNew() {
    	this.stage = 100;
    }

    public DialogueBuilder setClose(boolean close) {
        closeAllWindows = close;
        return this;
    }

    public String getEnterText() {
        return enterText;
    }

    public void setEnterText(String enterText) {
        this.enterText = enterText;
    }

    public boolean isCloseAllWindows() {
        return closeAllWindows;
    }
}
