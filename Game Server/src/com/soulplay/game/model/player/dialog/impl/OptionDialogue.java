package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.soulplay.game.model.player.dialog.DialogueUtils.validateLength;

public final class OptionDialogue extends Dialogue {

    private final List<Runnable> actions = new ArrayList<>();

    private Optional<String> title = Optional.empty();

    public OptionDialogue(String title, String option1, Runnable action1, String option2, Runnable action2) {
        super(new String[]{option1, option2});
        this.title = Optional.ofNullable(title);
        actions.addAll(Arrays.asList(action1, action2));
    }

    public OptionDialogue(String title, String option1, Runnable action1, String option2, Runnable action2,
                          String option3, Runnable action3) {
        super(new String[]{option1, option2, option3});
        this.title = Optional.ofNullable(title);
        actions.addAll(Arrays.asList(action1, action2, action3));
    }

    public OptionDialogue(String title, String option1, Runnable action1, String option2, Runnable action2,
                          String option3, Runnable action3, String option4, Runnable action4) {
        super(new String[]{option1, option2, option3, option4});
        this.title = Optional.ofNullable(title);
        actions.addAll(Arrays.asList(action1, action2, action3, action4));
    }

    public OptionDialogue(String title, String option1, Runnable action1, String option2, Runnable action2,
                          String option3, Runnable action3, String option4, Runnable action4, String option5,
                          Runnable action5) {
        super(new String[]{option1, option2, option3, option4, option5});
        this.title = Optional.ofNullable(title);
        actions.addAll(Arrays.asList(action1, action2, action3, action4, action5));
    }

    @Override
    public void accept(Player player) {
        validateLength(lines);
        switch (lines.length) {
            case 2:
                setTitle(player, title, 2460, 2465, 2468);
                player.getPacketSender().sendFrame126(lines[0], 2461);
                player.getPacketSender().sendFrame126(lines[1], 2462);
                player.getPacketSender().sendChatInterface(2459, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 3:
                setTitle(player, title, 2470, 2476, 2479);
                player.getPacketSender().sendFrame126(lines[0], 2471);
                player.getPacketSender().sendFrame126(lines[1], 2472);
                player.getPacketSender().sendFrame126(lines[2], 2473);
                player.getPacketSender().sendChatInterface(2469, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 4:
                setTitle(player, title, 2481, 2488, 2489);
                player.getPacketSender().sendFrame126(lines[0], 2482);
                player.getPacketSender().sendFrame126(lines[1], 2483);
                player.getPacketSender().sendFrame126(lines[2], 2484);
                player.getPacketSender().sendFrame126(lines[3], 2485);
                player.getPacketSender().sendChatInterface(2480, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 5:
                setTitle(player, title, 2493, 2501, 2502);
                player.getPacketSender().sendFrame126(lines[0], 2494);
                player.getPacketSender().sendFrame126(lines[1], 2495);
                player.getPacketSender().sendFrame126(lines[2], 2496);
                player.getPacketSender().sendFrame126(lines[3], 2497);
                player.getPacketSender().sendFrame126(lines[4], 2498);
                player.getPacketSender().sendChatInterface(2492, player.getDialogueBuilder().isCloseAllWindows());
                break;
        }
    }

    private static void setTitle(Player player, Optional<String> title, int id, int idSmall, int idBig) {
        if (!title.isPresent()) {
            player.getPacketSender().sendFrame126("Select an Option", id);
            player.getPacketSender().sendFrame171(0, idSmall);
            player.getPacketSender().sendFrame171(1, idBig);
        } else {
            player.getPacketSender().sendFrame171(1, idSmall);
            player.getPacketSender().sendFrame171(0, idBig);
            player.getPacketSender().sendFrame126(title.get(), id);
        }
    }

    public List<Runnable> getActions() {
        return actions;
    }
}
