package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;
import static com.soulplay.game.model.player.dialog.DialogueUtils.validateLength;

public class ItemStatementDialogue extends Dialogue {

    private final int itemId;

    public ItemStatementDialogue(int itemId, String[] lines) {
        super(lines);
        this.itemId = itemId;
    }

    @Override
    public void accept(Player player) {
        validateLength(lines);
        switch (lines.length) {

            case 1:
                player.getPacketSender().sendFrame246(307, 150, itemId);
                player.getPacketSender().sendFrame126(lines[0], 308);
                player.getPacketSender().sendChatInterface(306, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 2:
                player.getPacketSender().sendFrame246(311, 150, itemId);
                player.getPacketSender().sendFrame126(lines[0], 313);
                player.getPacketSender().sendFrame126(lines[1], 312);
                player.getPacketSender().sendChatInterface(310, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 3:
                player.getPacketSender().sendFrame246(316, 150, itemId);
                player.getPacketSender().sendFrame126(lines[0], 318);
                player.getPacketSender().sendFrame126(lines[1], 317);
                player.getPacketSender().sendFrame126(lines[2], 320);
                player.getPacketSender().sendChatInterface(315, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 4:
                player.getPacketSender().sendFrame246(322, 150, itemId);
                player.getPacketSender().sendFrame126(lines[0], 324);
                player.getPacketSender().sendFrame126(lines[1], 323);
                player.getPacketSender().sendFrame126(lines[2], 326);
                player.getPacketSender().sendFrame126(lines[3], 327);
                player.getPacketSender().sendChatInterface(321, player.getDialogueBuilder().isCloseAllWindows());
                break;

        }
    }

}
