package com.soulplay.game.model.player.dialog;

public enum Expression {
    DEFAULT_OLD(591),
    DEFAULT(9847),
    SAD(9765),
    SAD_2(9764),
    SAD_3(9768),
    REALLY_SAD(9760),
    DEPRESSED(9770),
    WORRIED(9775),
    SCARED(9780),
    MEAN_FACE(9785),
    MEAN_HEAD_BANG(9790),
    CALM_TALK(9810),
    CONFUSED(9830),
    HAPPY_DRUNK(9835),
    FAST_TALK(9845),
    HAPPY_TALK(9850),
    EXCITED_LAUGH(9851),
    SECRETLY_TALKING(9838),
    NO_EXPRESSION(9760),
    POSSESSED(9772),
    WHY(9776),
    MIDLY_ANGRY(9784),
    ANGRY(9788),
    ANGRY_YELL(9792),
    LISTENING(9804),
    PLAIN_TALK(9808),
    LOOK_DOWN(9812),
    CROOKED_HEAD(9828),
    ROLL_EYES(9832),
    SWAY_HEAD(9836),
    LISTEN_LAUGH(9840),
    TALK_SWING(9844);

    private final int id;

    Expression(int expression) {
	this.id = expression;
    }

    public int getId() {
	return id;
    }

}
