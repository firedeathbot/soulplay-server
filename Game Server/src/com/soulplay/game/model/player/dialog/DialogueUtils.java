package com.soulplay.game.model.player.dialog;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;

import java.util.Arrays;
import java.util.Objects;

public class DialogueUtils {

    private static final int MAXIMUM_LINE_LENGTH = 120;

    public static void validateLength(String... text) {
        if (Arrays.stream(text).filter(Objects::nonNull).anyMatch(s -> s.length() > MAXIMUM_LINE_LENGTH)) {
        	System.out.println("Dialogue text too long is: "+Arrays.toString(text));
            throw new IllegalStateException("Dialogue length too long, maximum line length is: " + MAXIMUM_LINE_LENGTH);
        }
    }

    public static String appendKeywords(Player player, String line) {
        if (line.contains("#username")) {
            line = line.replace("#username", player.getNameSmartUp());
        }

        if (player.npcType != -1) {

            NPC npc = NPCHandler.npcs[player.npcIndex];

            if (npc == null) {
                return line;
            }

            if (line.contains("#name")) {

                if (npc.def == null) {
                    line = line.replace("#name", "null");
                }

                line = line.replace("#name", npc.def == null ? "null" : npc.def.getName());

            }

        }
        return line;
    }

    private DialogueUtils() {

    }

}
