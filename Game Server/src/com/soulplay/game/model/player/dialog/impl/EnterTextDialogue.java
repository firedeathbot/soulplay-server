package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;

public class EnterTextDialogue extends Dialogue {

    private Runnable action;

    public EnterTextDialogue(Runnable action) {
        super(null);
        this.action = action;
    }

    public Runnable getAction() {
        return action;
    }

    public void setAction(Runnable action) {
        this.action = action;
    }

    @Override
    public void accept(Player player) {
        player.getPacketSender().sendEnterNameInterface();
    }

}