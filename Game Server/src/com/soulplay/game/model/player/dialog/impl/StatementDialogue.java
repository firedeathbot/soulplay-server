package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;
import static com.soulplay.game.model.player.dialog.DialogueUtils.validateLength;

public final class StatementDialogue extends Dialogue {

    public StatementDialogue(String... lines) {
	super(lines);
    }

    @Override
    public void accept(Player player) {
        validateLength(lines);
        switch (lines.length) {
            case 1:
                player.getPacketSender().sendFrame126(lines[0], 357);
                player.getPacketSender().sendChatInterface(356, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 2:
                player.getPacketSender().sendFrame126(lines[0], 360);
                player.getPacketSender().sendFrame126(lines[1], 361);
                player.getPacketSender().sendChatInterface(359, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 3:
                player.getPacketSender().sendFrame126(lines[0], 364);
                player.getPacketSender().sendFrame126(lines[1], 365);
                player.getPacketSender().sendFrame126(lines[2], 366);
                player.getPacketSender().sendChatInterface(363, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 4:
                player.getPacketSender().sendFrame126(lines[0], 369);
                player.getPacketSender().sendFrame126(lines[1], 370);
                player.getPacketSender().sendFrame126(lines[2], 371);
                player.getPacketSender().sendFrame126(lines[3], 372);
                player.getPacketSender().sendChatInterface(368, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 5:
                player.getPacketSender().sendFrame126(lines[0], 375);
                player.getPacketSender().sendFrame126(lines[1], 376);
                player.getPacketSender().sendFrame126(lines[2], 377);
                player.getPacketSender().sendFrame126(lines[3], 378);
                player.getPacketSender().sendFrame126(lines[4], 379);
                player.getPacketSender().sendChatInterface(374, player.getDialogueBuilder().isCloseAllWindows());
                break;

            default:
                System.err.println(String.format("Invalid statement dialogue line length: %s", lines.length));
                break;

        }
    }

  }
