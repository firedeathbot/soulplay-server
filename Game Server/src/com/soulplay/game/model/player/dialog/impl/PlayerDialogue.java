package com.soulplay.game.model.player.dialog.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Dialogue;
import com.soulplay.game.model.player.dialog.DialogueUtils;
import com.soulplay.game.model.player.dialog.Expression;

import static com.soulplay.game.model.player.dialog.DialogueUtils.validateLength;

public class PlayerDialogue extends Dialogue {

    private final Expression expression;

    public PlayerDialogue(String... lines) {
        this(Expression.DEFAULT, lines);
    }

    public PlayerDialogue(Expression expression, String... lines) {
        super(lines);
        this.expression = expression;
    }

    @Override
    public void accept(Player player) {
        validateLength(lines);
        switch (lines.length) {
            case 1:
                player.getPacketSender().showAnimationOnInterfacePacket(969, expression.getId());
                player.getPacketSender().sendFrame126(player.getNameSmartUp(), 970);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[0]), 971);
                player.getPacketSender().showPlayerHeadModelOnInterfacePacket(969);
                player.getPacketSender().sendChatInterface(968, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 2:
                player.getPacketSender().showAnimationOnInterfacePacket(974, expression.getId());
                player.getPacketSender().sendFrame126(player.getNameSmartUp(), 975);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[0]), 976);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[1]), 977);
                player.getPacketSender().showPlayerHeadModelOnInterfacePacket(974);
                player.getPacketSender().sendChatInterface(973, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 3:
                player.getPacketSender().showAnimationOnInterfacePacket(980, expression.getId());
                player.getPacketSender().sendFrame126(player.getNameSmartUp(), 981);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[0]), 982);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[1]), 983);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[2]), 984);
                player.getPacketSender().showPlayerHeadModelOnInterfacePacket(980);
                player.getPacketSender().sendChatInterface(979, player.getDialogueBuilder().isCloseAllWindows());
                break;

            case 4:
                player.getPacketSender().showAnimationOnInterfacePacket(987, expression.getId());
                player.getPacketSender().sendFrame126(player.getNameSmartUp(), 988);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[0]), 989);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[1]), 990);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[2]), 991);
                player.getPacketSender().sendFrame126(DialogueUtils.appendKeywords(player, lines[3]), 992);
                player.getPacketSender().showPlayerHeadModelOnInterfacePacket(987);
                player.getPacketSender().sendChatInterface(986, player.getDialogueBuilder().isCloseAllWindows());
                break;

            default:
                System.err.println(String.format("Invalid player dialogue line length: %s", lines.length));
                break;
        }
    }

}
