package com.soulplay.game.model;

public enum DisplayTimerType {

    ANTIFIRE(1),
    VENGEANCE(2),
    ICE_BARRAGE(3),
    TELEBLOCK(4),
    STUN(5),
    STAMINA(6),
    PRAYER_RENEWALS(7),
    TOME_OF_XP150(8),
    TOME_OF_XP100(9),
    TOME_OF_XP50(10),
    ;

    private final int id;

    DisplayTimerType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
