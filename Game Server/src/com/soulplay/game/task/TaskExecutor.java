package com.soulplay.game.task;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * A class holding methods to execute tasks.
 *
 * @author Emperor
 */
public final class TaskExecutor {

	/**
	 * The executor to use.
	 */
	private static final ScheduledExecutorService EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();

	/**
	 * The SQL task executor.
	 */
	private static final ScheduledExecutorService SQL_EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();

	private static final ScheduledExecutorService SLACK_EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();

	private static final ScheduledExecutorService HTTP_EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();

	/**
	 * Executes the task.
	 *
	 * @param task
	 *            The task to execute.
	 */
	public static void execute(Runnable task) {
		EXECUTOR.execute(task);
	}

	public static void executeHttp(Runnable task) {
		HTTP_EXECUTOR.execute(task);
	}

	/**
	 * Executes the slack task.
	 *
	 * @param task
	 *            The task to execute.
	 */
	public static void executeSlack(Runnable task) {
		SLACK_EXECUTOR.execute(task);
	}

	/**
	 * Executes an SQL handling task.
	 *
	 * @param task
	 *            The task.
	 */
	public static void executeSQL(Runnable task) {
		SQL_EXECUTOR.execute(task);
	}

	/**
	 * In case website goes down, it should not interrupt other tasks that
	 * require main dedi sql
	 *
	 * @param task
	 */
	public static void executeWebsiteSQL(Runnable task) {
		SQL_EXECUTOR.execute(task);
	}

	/**
	 * Gets the executor.
	 *
	 * @return The executor.
	 */
	public static ScheduledExecutorService getExecutor() {
		return EXECUTOR;
	}

	public static ScheduledExecutorService getSQLExecutor() {
		return SQL_EXECUTOR;
	}

	/**
	 * Constructs a new {@code TaskExecutor} {@code Object}.
	 */
	private TaskExecutor() {
		/*
		 * empty.
		 */
	}
}
