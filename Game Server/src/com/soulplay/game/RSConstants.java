package com.soulplay.game;

import com.soulplay.DBConfig;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class RSConstants {

	public static final int FREEZE_COOLDOWN = -5;
	
	public static final int[] autocastIds = {51133, 32, 51185, 33, 51091, 34,
			24018, 35, 51159, 36, 51211, 37, 51111, 38, 51069, 39, 51146, 40,
			51198, 41, 51102, 42, 51058, 43, 51172, 44, 51224, 45, 51122, 46,
			51080, 47, 7038, 0, 7039, 1, 7040, 2, 7041, 3, 7042, 4, 7043, 5,
			7044, 6, 7045, 7, 7046, 8, 7047, 9, 7048, 10, 7049, 11, 7050, 12,
			7051, 13, 7052, 14, 7053, 15, 47019, 27, 47020, 25, 47021, 12,
			47022, 13, 47023, 14, 47024, 15};

	public static final int birdNestSeeds[] = {5291, 5292, 5293, 5294, 5295, 5296,
			5297, 5298, 5291, 5292, 5293, 5294, 5295, 5296, 5297, 5298, 5299,
			5300, 5301, 5302, 5303, 5299, 5300, 5301, 5302, 5303, 5291, 5292,
			5293, 5294, 5295, 5296, 5297, 5298, 5291, 5292, 5293, 5294, 5295,
			5296, 5297, 5298, 5299, 5300, 5301, 5302, 5303, 5299, 5300, 5301,
			5302, 5303, 5304};

	public static final int birdNestRings[] = {1635, 1637, 1639, 1641, 1643};

	public static final int ATTACK = 0;

	public static final int DEFENCE = 1;

	public static final int STRENGTH = 2;

	public static final int HITPOINTS = 3;

	public static final int RANGED = 4;

	public static final int PRAYER = 5;

	public static final int MAGIC = 6;

	public static final int COOKING = 7;

	public static final int WOODCUTTING = 8;

	public static final int FLETCHING = 9;

	public static final int FISHING = 10;

	public static final int FIREMAKING = 11;

	public static final int CRAFTING = 12;

	public static final int SMITHING = 13;

	public static final int MINING = 14;

	public static final int HERBLORE = 15;

	public static final int AGILITY = 16;

	public static final int THIEVING = 17;

	public static final int SLAYER = 18;

	public static final int FARMING = 19;

	public static final int RUNECRAFTING = 20;

	public static final int CONSTRUCTION = 21;

	public static final int HUNTER = 22;

	public static final int SUMMONING = 23;

	public static final int DUNGEONEERING = 24;

	public static final int MAX_WILD_ATT = 118;

	public static final int MAX_WILD_STR = 118;

	public static final int MAX_WILD_DEF = 118;

	public static final int MAX_WILD_RNG = 112;

	public static final int MAX_WILD_MAGE = 104;

	public static final int MAX_ATT = 126;

	public static final int MAX_STR = 126;

	// public boolean inClanWars() {
	// if (absX > 2880 && absX < 2943 && absY > 5632 && absY < 5695) {
	// return true;
	// }
	// return false;
	// }
	//
	// public boolean inClanWarsLobby() {
	// if (absX > 2979 && absX < 3006 && absY > 9665 && absY < 9693) {
	// return true;
	// }
	// return false;
	// }
	//
	// public boolean inClanWarsDead() {
	// if (absX > 2926 && absX < 2933 && absY > 5663 && absY < 5670
	// || absX > 2890 && absX < 2897 && absY > 5657 && absY < 5664) {
	// return true;
	// }
	// return false;
	// }

	public static final int MAX_DEF = 126;

	public static final int MAX_RNG = 118;

	public static final int MAX_MAGE = 108;

	public static final int DARTS_DIST = 4;

	public static final int THROWNAXES_DIST = 4;

	public static final int KNIVES_DIST = 5;

	public static final int JAVELINS_DIST = 5;

	public static final int BLISTERWOOD_DIST = 5;

	public static final int DEATH_LOTUS_DART_DIST = 6;

	public static final int CROSSBOW_DIST = 7;

	public static final int SHORTBOW_DIST = 7;

	public static final int MORRIGAN_JAVELIN_DIST = 7;

	public static final int SEERCULL_DIST = 7;

	public static final int CRYSTAL_BOW_DIST = 10;

	public static final int ZARYTE_BOW_DIST = 7;

	public static final int SHIELDBOW_DIST = 8;
	
	public static final int TWISTED_BOW_DIST = 10;

	public static final int COMBAT_SPELL_DIST = 10;

	public static final int LONGBOW_DIST = 9;

	public static final int SIGHTED_SHIELDBOW_DIST = 9;

	public static final int COMPOSITE_BOW_DIST = 9;

	public static final int SAGAIE_DIST = 9;

	public static final int ROYAL_CROSSBOW_DIST = 9;

	public static final int WYVERN_CROSSBOW_DIST = 9;

	public static final int NOXIOUS_LONGBOW_DIST = 9;

	public static final int DRAGONFIRE_SHIELD = 14;

	public static int arti[] = {14876, 14877, 14878, 14879, 14880, 14881, 14881,
			14881, 14882, 14882, 14882, 14883, 14884, 14885, 14886, 14887,
			14888, 14889, 14890, 14890, 14891, 14891, 14892, 14892, 14892,
			14878, 14879, 14880, 14881, 14881, 14881, 14882, 14882, 14882,
			14883, 14884, 14885, 14886, 14887, 14888, 14889, 14890, 14890,
			14891, 14891, 14892, 14892, 14892};

	public static boolean isThereNexArmourEquipped(int id) {
		id--;
		switch(id) {
			case 20135:
			case 20147:
			case 20159:
			case 20163:
			case 20151:
			case 20139:
			case 20167:
			case 20155:
			case 20143:
			case 30264://elder chaos top
			case 30265://elder chaos robe
			case 30266://elder chaos hood
				return true;
			default:
				return false;
		}
	}

	public static int Barrows[] = {4708, 4710, 4712, 4714, 4716, 4718, 4720,
			4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747,
			4749, 4751, 4753, 4755, 4757, 4759};

	public static int skillingPackage[] = {1748, 2358, 12158, 12159, 12160,
			12163, 537, 452, 3143, 15271, 9193};

	public static int rareSkillingPackage[] = {5304, 5973, 18834, 21622, 21626,
			12539};

	public static final String welfarePkBotNames[] = {"Welfare4Life",
			"Summerl1f3", "One3rection", "Oned4y", "Sung Xiu", "Chen4",
			"Dongmei", "Wang Li", "Huifang", "Ding Jian"};

	public static final String welfarePkBotNames2[] = {"Aaron", "Sipp",
			"Madelene", "Ries", "Wilburn", "Cavalier", "Taisha", "Stem",
			"Douglass", "Pape", "Madalyn", "Forester", "Elroy", "Sitler",
			"Michiko", "Lieu", "Debera", "Zepp", "Rosio", "Defreitas", "Bibi",
			"Moriarty", "Lucinda", "Brogden", "Manda", "Knopp", "Brandee",
			"Mcmunn", "Salena", "Lebleu", "Drema", "Maudlin", "Roxann",
			"Cairns", "Katheryn", "Flaherty", "Dalton", "Selders", "Jacinta",
			"Gaither",};

	public static final String pureWelfareBotNames[] = {"Irving", "Harmon",
			"Mathew", "Palmer", "Eddie", "Wells", "Delbert", "Lindsey",
			"Edward", "Burke", "Lorenzo", "Cross", "Miguel", "Parker", "Elbert",
			"Bowen", "Gordon", "Anderson", "Mack", "Briggs", "Cameron", "Brady",
			"Damon", "Reese", "John", "Gill", "Nathan", "Ball", "Curtis",
			"Rowe", "Barry", "Harrison", "Johnny", "Wise", "Gary", "Cain",
			"Christian", "Matthews", "Elmer", "Lynch", "Joey", "Terry",
			"Lowell", "Ramirez", "Rudy", "Kennedy", "Jesse", "Paul", "Jorge",
			"Hines"};

	public static final int NEX_ARMOR_BODY_HP = 22;
	public static final int NEX_ARMOR_BODY_HP_EDGE = 0;

	public static final int NEX_ARMOR_LEGS_HP = 17;
	public static final int NEX_ARMOR_LEGS_HP_EDGE = 0;

	public static final int NEX_ARMOR_HELM_HP = 11;
	public static final int NEX_ARMOR_HELM_HP_EDGE = 0;

	public static final int DOOR_LIFE = 200;
	
	public static boolean petDungeon(int absX, int absY) {
		return absX >= 3251 && absX <= 3343 && absY >= 4280 && absY <= 4421;
	}
	
	public static boolean donatorArea(int absX, int absY) {
		return absX >= 1381 && absX <= 1432 && absY >= 4399 && absY <= 4437;
	}
	
	public static boolean zulrahArea(int absX, int absY) {
		return absX >= 2254+6400 && absX <= 2278+6400 && absY >= 3064 && absY <= 3082;
	}
	
	public static boolean vorkathFightArea(int absX, int absY) {
		return absX >= 8660 && absX <= 8685 && absY >= 4053 && absY <= 4077;
	}

	public static boolean clanWarsFightArea(int absX, int absY) {
		return absX >= 9792 && absX <= 9855 && absY >= 4672 && absY <= 4799;
	}
	
	public static boolean inSoulWars(int absX, int absY) { 
		return absX >= 1763 && absX <= 2005 && absY >= 3187 && absY <= 3303;
	}
	
	public static boolean inCatacomb(int absX, int absY) { 
		return (absX >= 7987 && absX <= 9978 && absY >= 8167 && absY <= 10113) && !(absX >= 8007 && absX <= 8039 && absY >= 10068 && absY <= 10103);
	}
	
	public static boolean inBlueGraveyard(int absX, int absY) {
		return absX >= 1837 && absX <= 1847 && absY >= 3213 && absY <= 3223;
	}
	
	public static boolean inRedGraveyard(int absX, int absY) {
		return absX >= 1928 && absX <= 1938 && absY >= 3240 && absY <= 3250;
	}
	
	public static boolean inBlueGraveyardRoom(int absX, int absY) {
		return absX >= 1816 && absX <= 1823 && absY >= 3220 && absY <= 3230;
	}
	
	public static boolean inRedGraveyardRoom(int absX, int absY) {
		return absX >= 1951 && absX <= 1958 && absY >= 3234 && absY <= 3244;
	}
	
	public static boolean inBlueGraveyardDeath(int absX, int absY) {
		return absX >= 1841 && absX <= 1843 && absY >= 3217 && absY <= 3219;
	}
	
	public static boolean inRedGraveyardRoomDeath(int absX, int absY) {
		return absX >= 1932 && absX <= 1934 && absY >= 3244 && absY <= 3246;
	}
	
	public static boolean inSoulWarsObeliskArea(int absX, int absY) {
		return absX >= 1879 && absX <= 1894 && absY >= 3224 && absY <= 3239;
	}
	
	public static boolean inSoulWarsRedRoom(int absX, int absY) {
		return absX >= 1900 && absX <= 1909 && absY >= 3157 && absY <= 3166;
	}
	
	public static boolean inSoulWarsBlueRoom(int absX, int absY) {
		return absX >= 1870 && absX <= 1879 && absY >= 3158 && absY <= 3166;
	}
	
	public static boolean inSoulWarsRedBoss(int absX, int absY) {
		return absX >= 1960 && absX <= 1974 && absY >= 3246 && absY <= 3258;
	}
	
	public static boolean inSoulWarsBlueBoss(int absX, int absY) {
		return absX >= 1799 && absX <= 1815 && absY >= 3203 && absY <= 3218;
	}
	
	public static boolean inSoulWarsLobby(int absX, int absY) {
		return absX >= 1860 && absX <= 1914 && absY >= 3149 && absY <= 3191;
	}

	public static boolean inSoulWarsRedRespawn(int absX, int absY) {
		return absX >= 1951 && absX <= 1958 && absY >= 3234 && absY <= 3244;
	}

	public static boolean inSoulWarsBlueRespawn(int absX, int absY) {
		return absX >= 1816 && absX <= 1823 && absY >= 3220 && absY <= 3230;
	}

	public static boolean inTaiBwoWannai(int absX, int absY) {
		return absX >= 2724 && absX <= 2877 && absY >= 3011 && absY <= 3130;
	}
	
	public static boolean inEdgeConstruction(int absX, int absY) {
		return absX >= 3077 && absX <= 3081 && absY >= 3489 && absY <= 3496;
	}

	public static boolean inVarrockConstruction(int absX, int absY) {
		return absX >= 3203 && absX <= 3211 && absY >= 3445 && absY <= 3454;
	}
	
	public static boolean inLMSLobby(int absX, int absY) {
		return absX >= 3397 && absX <= 3416 && absY >= 3168 && absY <= 3188;
	}
	
	public static boolean inLMSWaitingRoom(int absX, int absY) {
		return absX >= 3411 && absX <= 3416 && absY >= 3181 && absY <= 3186;
	}
	
	public static boolean inLMSArena(int absX, int absY) {
		return absX >= 1792 && absX <= 1919 && absY >= 3776 && absY <= 3903;
	}
	
	public static boolean inMageArena(int x, int y) {
		return (x >= 3093 && x <= 3117 && y >= 3921 && y <= 3946);
	}
	
	public static boolean inResourceArea(int x, int y) {
		if (x >= 3174 && x <= 3196 && y >= 3924 && y <= 3944) {
			return true;
		}
		return false;
	}

	public static boolean cwSaraBattlement(int x, int y) {
		if ((x >= 2414 && x <= 2416 && y >= 3074 && y <= 3091)
				|| (x >= 2412 && x <= 2424 && y >= 3087 && y <= 3089)
				|| (x >= 2414 && x <= 2416 && y >= 3086 && y <= 3091)
				|| (x == 2413 && y == 3086)) {
			return true;
		}
		return false;
	}

	public static boolean cwSaraSpawnRoom(int x, int y, int z) {
		if (z == 1 && x >= 2423 && x <= 2431 && y >= 3072 && y <= 3080) {
			return true;
		}
		return false;
	}

	public static boolean cwSaraWaitingRoom(int x, int y) {
		if (x >= 2370 && x <= 2392 && y >= 9482 && y <= 9497) {
			// room
			return true;
		}
		return false;
	}

	public static boolean cwZammyBattlement(int x, int y) {
		if ((x >= 2375 && x <= 2387 && y >= 3118 && y <= 3210)
				|| (x >= 2383 && x <= 2385 && y >= 3116 && y <= 3133)
				|| (x >= 2382 && x <= 2386 && y >= 3117 && y <= 3121)) {
			return true;
		}
		return false;
	}

	public static boolean cwZammySpawnRoom(int x, int y, int z) {
		if (z == 1 && x >= 2368 && x <= 2376 && y >= 3127 && y <= 3135) {
			return true;
		}
		return false;
	}

	public static boolean cwZammyWaitingRoom(int x, int y) {
		if (x >= 2410 && x <= 2430 && y >= 9514 && y <= 9533) {
			// room
			return true;
		}
		return false;
	}
	
	public static boolean castleWarsLobby(int x, int y) {
		return x >= 2434 && x <= 2447 && y >= 3080 && y <= 3099;
	}
	
	public static boolean inGamblingArea(Location loc) {
		return loc.getX() >= 3310 && loc.getX() <= 3325 && loc.getY() >= 3216 && loc.getY() <= 3251;
	}

	public static boolean dzDungeon(int x, int y) {
		if (x >= 2496 && x <= 2623 && y >= 10240 && y <= 10303) {
			return true;
		}
		return false;

	}

	public static boolean dzIsland(int x, int y) {
		if (x >= 2491 && x <= 2628 && y >= 3836 && y <= 3904) {
			return true;
		}
		return false;
	}
	
	public static boolean inChampionsGuild(int x, int y) {
		return x >= 3137 && x <= 3198 && y >= 9732 && y <= 9790;
	}
	
	public static boolean inGrandExchange(int x, int y) {
		if (x >= 3142 && x <= 3187 && y >= 3469 && y <= 3513) {
			return true;
		}
		return false;
	}
	
	public static boolean inGrandExchangeSafePvp(int x, int y) {
		if ((x == 3148 && y >= 3484 && y <= 3495)
			|| (x == 3149 && y >= 3482 && y <= 3497)
			|| (x == 3150 && y >= 3480 && y <= 3499)
			|| (x == 3151 && y >= 3478 && y <= 3501)
			|| (x == 3152 && y >= 3477 && y <= 3502)
			|| (x >= 3153 && x <= 3154 && y >= 3476 && y <= 3503)
			|| (x >= 3155 && x <= 3156 && y >= 3475 && y <= 3504)
			|| (x >= 3157 && x <= 3158 && y >= 3474 && y <= 3505)
			|| (x >= 3159 && x <= 3170 && y >= 3473 && y <= 3506)
			|| (x >= 3171 && x <= 3172 && y >= 3474 && y <= 3505)
			|| (x >= 3173 && x <= 3174 && y >= 3475 && y <= 3504)
			|| (x >= 3175 && x <= 3176 && y >= 3476 && y <= 3503)
			|| (x == 3177 && y >= 3477 && y <= 3502)
			|| (x == 3178 && y >= 3478 && y <= 3501)
			|| (x == 3179 && y >= 3480 && y <= 3499)
			|| (x == 3180 && y >= 3482 && y <= 3497)
			|| (x == 3181 && y >= 3484 && y <= 3495))
			return true;
		
		
		return false;
	}
	
	public static boolean inNeitzBank(int x, int y) {
		return (x >= 2334 && x <= 2339 && y >= 3805 && y <= 3808);
	}

	public static boolean FFA(int absX, int absY) {
		return absX >= 2756 && absX <= 2875 && absY >= 5512 && absY <= 5628;
	}

	public static boolean fightPitsRoom(int x, int y) {
		return (x >= 2375 && x <= 2420 && y >= 5129 && y <= 5167);
	}

	public static boolean fightPitsWait(int x, int y) {
		return (x >= 2394 && x <= 2404 && y >= 5169 && y <= 5175);
	}
	
	public static boolean inRFDArena(int x, int y) {
		return (x >= 1889 && x <= 1910 && y >= 5345 && y <= 5366);
	}

	public static double getDropRatePerMode(Player player) {
		double baseChance = 1.0;
		double extraChance = 0.0;

		if (player != null) {
			switch (player.difficulty) {
				case PlayerDifficulty.NORMAL:
					baseChance = DBConfig.EASY_MODE_DROPRATE;
					break;
				case PlayerDifficulty.IRONMAN:
				case PlayerDifficulty.HC_IRONMAN:
					baseChance = DBConfig.IRONMAN_MODE_DROPRATE;
					break;
				case PlayerDifficulty.PRESTIGE_ONE:
					baseChance = DBConfig.MEDIUM_MODE_DROPRATE;
					break;
				case PlayerDifficulty.PRESTIGE_TWO:
					baseChance = DBConfig.HARD_MODE_DROPRATE;
					break;
				case PlayerDifficulty.PRESTIGE_THREE:
					baseChance = DBConfig.LEGENDARY_MODE_DROPRATE;
					break;
			}

			if (player.dropRateBoostActive()) {
				extraChance += 0.2;
			}
		}

		if (DBConfig.DOUBLE_DROP_RATE) {
			baseChance += baseChance;
		}

		return baseChance + extraChance;
	}

	public static double getKDR(double kc, double dc) {
		if (dc == 0) {
			return kc;
		}
		return Misc.round(kc / dc, 1);
	}

	public static boolean inBarbarianAssaultArena(int absX, int absY) {
		return absX >= 1860 && absX <= 1910 && absY >= 5450 && absY <= 5495;
	}

	public static boolean inClanWarsFunPk(int absX, int absY) {
		return absX > 2755 && absX < 2877 && absY > 5511 && absY < 5629;
	}

	public static boolean inClanWarsFunPkMulti(int absX, int absY) {
		return absX > 2751 && absX < 2877 && absY > 5545 && absY < 5629;
	}

	public static boolean inClanWarsLobby(int absX, int absY) {
		return absX >= 3367 && absY >= 3143 && absX <= 3384 && absY <= 3160;
	}

	public static boolean inCastleWarsArena(int x, int y) {
		if (x >= 2370 && x <= 2392 && y >= 9482 && y <= 9497) {
			// room
			return false;
		}
		if (x >= 2410 && x <= 2430 && y >= 9514 && y <= 9533) {
			// room
			return false;
		}
		if (x >= 2367 && x <= 2431 && y >= 3071 && y <= 3136) {
			return true;
		}
		if (x >= 2367 && x <= 2430 && y >= 9481 && y <= 9529) {
			return true;
		}
		return false;
	}

	public static boolean inEasterEvent(int absX, int absY) {
		return absX >= 2434 && absX <= 2504 && absY >= 5250 && absY <= 5361;
	}

	public static boolean inFishingContestIsland(int absX, int absY) {
		return absX >= 2507 && absX <= 2546 && absY >= 4760 && absY <= 4793;
	}

	public static boolean inFunPk(int x, int y) {
		if (x > 3179 && x < 3352 && y > 3061 && y < 3116
				|| x > 3204 && x < 3293 && y > 3115 && y < 3135
				|| x > 3317 && x < 3361 && y > 3115 && y < 3139
				|| x > 3332 && x < 3353 && y > 3141 && y < 3159
				|| x > 3331 && x < 3355 && y > 3138 && y < 3142
				|| x > 2582 && x < 2602 && y > 3152 && y < 3171
				|| x > 2601 && x < 2607 && y > 3156 && y < 3171) {
			return true;
		}
		return false;
	}

	public static boolean inGiantMoleDung(int absX, int absY) {
		return absX >= 1727 && absX <= 1794 && absY >= 5127 && absY <= 5246;
	}

	public static boolean inLavaDragonArea(int x, int y) {
		if ((x >= 3179 && x <= 3215 && y >= 3808 && y <= 3855)
				|| (x >= 3206 && x <= 3213 && y >= 3806 && y <= 3804)
				|| (x >= 3215 && x <= 3218 && y >= 3807 && y <= 3852)
				|| (x >= 3218 && x <= 3221 && y >= 3809 && y <= 3851)
				|| (x >= 3220 && x <= 3232 && y >= 3814 && y <= 3849)
				|| (x >= 3232 && x <= 3234 && y >= 3839 && y <= 3844)
				|| (x >= 3187 && x <= 3195 && y >= 3801 && y <= 3806)
				|| (x >= 3191 && x <= 3193 && y >= 3799 && y <= 3801)
				|| (x >= 3174 && x <= 3196 && y >= 3804 && y <= 3826))
		// 3174 3826
		{
			return true;
		}
		return false;
	}

	public static boolean inMulti(int absX, int absY) {
		if ((absX >= 3136 && absX <= 3327 && absY >= 3519 && absY <= 3607)
				|| (absX >= 3192 && absX <= 3327 && absY >= 3648
						&& absY <= 3752)
				|| (absX >= 3152 && absX <= 3327 && absY >= 3752
						&& absY <= 3839)
				|| (absX >= 3200 && absX <= 3390 && absY >= 3840
						&& absY <= 3967)
				|| (absX >= 3136 && absX <= 3152 && absY >= 3840
						&& absY <= 3903)
				|| (absX >= 2992 && absX <= 3007 && absY >= 3912
						&& absY <= 3967)
				|| (absX >= 2946 && absX <= 2959 && absY >= 3816
						&& absY <= 3831)
				|| (absX >= 3009 && absX <= 3199 && absY >= 3856
						&& absY <= 3903)
				|| (absX >= 2824 && absX <= 2944 && absY >= 5258
						&& absY <= 5369)
				|| (absX >= 3008 && absX <= 3071 && absY >= 3600
						&& absY <= 3711)
				|| (absX >= 3072 && absX <= 3327 && absY >= 3608
						&& absY <= 3647)
				|| (absX >= 2624 && absX <= 2690 && absY >= 2550
						&& absY <= 2619)
				|| (absX >= 2371 && absX <= 2425 && absY >= 5062
						&& absY <= 5117)
				|| (absX >= 2896 && absX <= 2927 && absY >= 3595
						&& absY <= 3630)
				|| (absX >= 2892 && absX <= 2932 && absY >= 4435
						&& absY <= 4464)
				|| (absX >= 2256 && absX <= 2287 && absY >= 4680
						&& absY <= 4711)
				|| (absX >= 3263 && absX <= 3327 && absY >= 9602
						&& absY <= 9656) // dorgeshuun dungeon
				|| (absX >= 2691 && absX <= 2729 && absY >= 3705
						&& absY <= 3737) // rock crabs side
				|| (absX >= 2540 && absX <= 2507 && absY >= 4631
						&& absY <= 4660) // dagganoth light house
				|| (absX >= 2911 && absX <= 3003 && absY >= 4357
						&& absY <= 4404) // corp beast
				|| (absX >= 2430 && absX <= 2563 && absY >= 10113
						&& absY <= 10183) // waterbirth dung part 1
				|| (absX >= 2875 && absX <= 2927 && absY >= 4741
						&& absY <= 4791) // bandos avatar
				|| (absX >= 3359 && absX <= 3391 && absY >= 9800
						&& absY <= 9825) // nomad
				|| (absX >= 2845 && absX <= 2900 && absY >= 5189
						&& absY <= 5234) // nex minions room
				|| (absX >= 1883 && absX <= 1913 && absY >= 4395
						&& absY <= 4418) // waterbirth dung part 2
				|| (absX >= 3456 && absX <= 3520 && absY >= 9472 && absY <= 9534)//kalphite place
				|| (RSConstants.petDungeon(absX, absY))
				|| (RSConstants.inClanWarsFunPkMulti(absX, absY))
				|| (RSConstants.inCastleWarsArena(absX, absY)) || (inFunPk(absX, absY))
				|| (RSConstants.isInNexRoom(absX, absY))
				|| (RSConstants.inPenanceQueenRoom(absX, absY))
				|| (RSConstants.inGiantMoleDung(absX, absY))
				|| RSConstants.inBarbarianAssaultArena(absX, absY)
				|| RSConstants.inSoulWars(absX, absY)
				|| RSConstants.inCatacomb(absX, absY)
				|| (zulrahArea(absX, absY)) // zulrah boss area
				|| (absX >= 10262 && absY >= 9941 && absX <= 10282 && absY <= 9961)
				|| (donatorArea(absX, absY))
				//easter event multi zones for boss
//				|| (absX >= 2711 && absX <= 2808 && absY >= 3391 && absY <= 3487) // seers village
//				|| (absX >= 3020 && absX <= 3052 && absY >= 3296 && absY <= 3322) //falador farm
//				|| (absX >= 3222 && absX <= 3253 && absY >= 3163 && absY <= 3190) //lummy swamp
//				|| (absX >= 3267 && absX <= 3322 && absY >= 3240 && absY <= 3304) //al kharid mine
//				|| (absX >= 3544 && absX <= 3585 && absY >= 3273 && absY <= 3307) //barrows hills
//				|| (absX >= 3247 && absX <= 3279 && absY >= 3412 && absY <= 3442) //east varrock bank
//				|| (absX >= 3086 && absX <= 3120 && absY >= 3211 && absY <= 3246) //draynor
//				|| (absX >= 2947 && absX <= 3072 && absY >= 5543 && absY <= 5629) //clw red portal
//				|| (absX >= 3473 && absX <= 3517 && absY >= 3470 && absY <= 3512) //canifis

		) {
			return true;
		}
		return false;
	}

	public static boolean inNomadsEntrance(int absX, int absY) {
		return absX >= 3357 && absX <= 3395 && absY >= 9800 && absY <= 9858;
	}

	public static boolean inNomadsRoom(int absX, int absY) {
		return absX >= 3359 && absX <= 3391 && absY >= 9800 && absY <= 9825;
	}

	public static boolean inPenanceQueenRoom(int absX, int absY) {
		return absX >= 1860 && absX <= 1910 && absY >= 5486 && absY <= 5450;
	}

	public static boolean isInGlacorLair(int absX, int absY) {
		return absX >= 2620 && absX <= 2689 && absY >= 3973 && absY <= 4014;
	}

	public static boolean isInNexRoom(int absX, int absY) {
		return absX >= 2911 && absX <= 2940 && absY >= 5188 && absY <= 5219;
	}

	public static boolean outsideClanWarsWilderness(int x, int y) {
		return x >= 3241 && x <= 3296 && y >= 3658 && y <= 3712;
	}
	
	public static boolean edgevilleWilderness(int x, int y) {
		return x >= 3062 && x <= 3128 && y >= 3522 && y <= 3559;
	}

	public static boolean whitePortalSafe(int absX, int absY) {
		return absX > 2752 && absX < 2877 && absY > 5505 && absY < 5512;
	}
	
	public static boolean isInRevenantDungeon(int x, int y) {
		return x > 9531 && x < 9675 && y > 10051 && y < 10244;
	}

}
