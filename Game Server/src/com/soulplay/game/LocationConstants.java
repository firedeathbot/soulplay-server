package com.soulplay.game;

import com.soulplay.game.world.entity.Location;

public class LocationConstants {

	/**
	 * Teleports
	 */
	public static final Location GE = Location.create(3165, 3482);
	
	public static final Location NEITIZNOT = Location.create(2339, 3802);
	
	public static final Location SHILO_VILLAGE = Location.create(2852, 2960);
	
	public static final Location LUNAR_ISLE = Location.create(2096, 3913);
	
	public static final Location TAI_BWO_WANNAI = Location.create(2791, 3067);
	
	public static final Location KELDAGRIM = Location.create(2856, 10210);
	
	public static final Location APE_ATOLL = Location.create(2762, 2785);
	
	public static final Location TAVERLY_DUNG = Location.create(2884, 9798);
	
	public static final Location KALPHITE_LAIR = Location.create(3509, 9496, 2);
	
	public static final Location ICE_DUNG = Location.create(3007, 9550);
	
	public static final Location FORBIDDEN_DUNG = Location.create(2690, 10124);
	
	public static final Location JUNGLE_DUNG = Location.create(2850, 9478);
	
	public static final Location ANCIENT_CAVERN = Location.create(1764, 5365);
	
	public static final Location CHAOS_TUNNEL = Location.create(3184, 5469);
	
	public static final Location SMOKE_DUNG = Location.create(3206, 9379);
	
	public static final Location WATERBIRTH_DUNG = Location.create(2442, 10146);
	
	public static final Location YAKS = Location.create(2326, 3801);

	public static final Location ROCK_CRABS = Location.create(2678, 3718);

	public static final Location EXPERIEMNTS = Location.create(3577, 9927);

	public static final Location SLAYER_TOWER = Location.create(3428, 3538);

	public static final Location FREMENNIK_DUNGEON = Location.create(2808, 10002);

	public static final Location LUMBRIDGE_DUNGEON = Location.create(3168, 9572);

	public static final Location EDGEVILLE_DUNGEON = Location.create(3096, 9867);

	public static final Location VARROCK_DUNGEON = Location.create(3237, 9859);

	public static final Location PEST_CONTROL = Location.create(2658, 2649);

	public static final Location DUEL_ARENA = Location.create(3366, 3266);

	public static final Location BARROWS = Location.create(3565, 3306);

	public static final Location TZHAAR = Location.create(2438, 5168);

	public static final Location WARRIORS_GUILD = Location.create(2876, 3546);

	public static final Location CASTLE_WARS = Location.create(2440, 3089);

	public static final Location PENGUIN_HUNT = Location.create(2597, 3273);

	public static final Location FIGHT_PITS = Location.create(2399, 5178);

	public static final Location WILDERNESS_TREASURE = Location.create(2544, 4714);

	public static final Location FISHING_CONTEST = Location.create(2523, 4776);

	public static final Location KBD = Location.create(3003, 3849);

	public static final Location CHAOS_ELE = Location.create(3279, 3910);

	public static final Location BARRELCHEST = Location.create(3807, 2844);

	public static final Location GENERAL_GRAARDOR = Location.create(2860, 5354);

	public static final Location KREE_ARRA = Location.create(2839, 5293);
	
	public static final Location ZULRAH = Location.create(8599, 3056);
	
	public static final Location ALCHEMICAL_HYDRA = Location.create(7711, 10205);
	
	public static final Location NIGHTMARE_OF_ASHIHAMA = Location.create(10208, 9755, 1);
	
	public static final Location VETION = Location.create(3206, 3772);
	
	public static final Location LIZARD_SHAMAN = Location.create(7705, 9973);
	
	public static final Location VORKATH = Location.create(8677, 4035);

	public static final Location KRIL_TSUTSAROTH = Location.create(2925, 5334);

	public static final Location ZILYANA = Location.create(2912, 5266);

	public static final Location TORM = Location.create(2525, 5810);

	public static final Location CORP = Location.create(2948, 4374);

	public static final Location DAG_KINGS = Location.create(1912, 4367);

	public static final Location NEX_ENTRANCE = Location.create(2885, 5279);

	public static final Location SKELETAL_HORROR = Location.create(3305, 3658);

	public static final Location SCORPIA = Location.create(3239, 3953);

	public static final Location CALLISTO = Location.create(3312, 3845);

	public static final Location VENENANTIS = Location.create(3368, 3718);
	public static final Location CRAZY_ARCHEOLOGIST = Location.create(2953, 3699);
	public static final Location CHAOS_FANATIC = Location.create(2963, 3845);
	
	public static final Location DERANGED_ARCHEOLOGIST = Location.create(10081, 3718);

	public static final Location BANDOS_AVATAR = Location.create(2912, 4765);

	public static final Location NOMAD = Location.create(3367, 9832);

	public static final Location MAGE_BANK = Location.create(2539, 4715);
	
	public static final Location HILL_GIANTS_WILDY = Location.create(3296, 3659);

	public static final Location WEST_DRAGONS = Location.create(2979, 3597);

	public static final Location DEMONIC_GORILLAS = Location.create(8529, 5646);

	public static final Location CLAN_WARS = Location.create(3368, 3169);

	public static final Location FUNPK = Location.create(3303, 3123);
	
	public static final Location CLEANUP = Location.create(2814, 3082);
	
	public static final Location LMS = Location.create(3403, 3173);

	public static final Location GAMBLE_ZONE = Location.create(1444, 4457);

	public static final Location LAND_OF_SERENITY = Location.create(1458, 4362);
	
	public static final Location WOODCUTTING_CAMELOT = Location.create(2726, 3485);

	public static final Location WOODCUTTING_DRAYNOR = Location.create(3093, 3244);
	
	public static final Location WOODCUTTING_MAHOGANY = Location.create(2816, 3083);
	
	public static final Location WOODCUTTING_FALLY_IVY = Location.create(3007, 3390);
	
	public static final Location WOODCUTTING_GUILD = Location.create(7960, 3487);

	public static final Location FISHING_CATHERBY = Location.create(2809, 3435);

	public static final Location FISHING_GUILD = Location.create(2594, 3415);

	public static final Location FISHING_KARAMBWAN = Location.create(2788, 3065);
	
	public static final Location FISHING_PORT_PISCARILIUS = Location.create(8248, 3786);
	
	public static final Location RESOURCE_AREA = Location.create(3184, 3946);

	public static final Location MINING_NEITIZNOT = Location.create(2317, 3834);
	
	public static final Location MINING_CRAFTING_GUILD = Location.create(2933, 3290);
	
	public static final Location LIMESTONE_MINE = Location.create(3369, 3495);
	
	public static final Location GEM_MINE = Location.create(2826, 2996);
	
	public static final Location DORGESH_KAAN_MINE = Location.create(3316, 9612);

	public static final Location MINING_FALADOR = Location.create(3061, 3376);

	public static final Location RUNECRAFTING_EDGE = Location.create(3087, 3499);

	public static final Location THIEVING_VARROCK = Location.create(3224, 3439);

	public static final Location THIEVING_EDGE = Location.create(3096, 3502);

	public static final Location THIEVING_ARDOUGEN = Location.create(2662, 3305);

	public static final Location AGILITY_GNOME = Location.create(2470, 3436);

	public static final Location AGILITY_BARB = Location.create(2552, 3563);

	public static final Location AGILITY_WILDY = Location.create(2998, 3909);

	public static final Location SLAYER_TURAEL = Location.create(2929, 3536);

	public static final Location SLAYER_MAZCHNA = Location.create(3510, 3507);

	public static final Location SLAYER_VANNAKA = Location.create(3146, 9914);

	public static final Location SLAYER_CHAELDAR = Location.create(2447, 4431);
	
	public static final Location SLAYER_KURADAL = Location.create(1741, 5316);

	public static final Location SLAYER_MORVAN = Location.create(2204, 3253, 0);

	public static final Location SLAYER_SUMONA = Location.create(3360, 2993, 0);

	public static final Location SLAYER_DURADEL = Location.create(2869, 2981, 1);

	public static final Location KONAR_QUO_MATEN = Location.create(7709, 3787, 0);

	public static final Location NIEVE = Location.create(2434, 3423, 0);
	
	public static final Location KRYSTILIA = Location.create(3109, 3514);

	public static final Location DUNGEONEERING = Location.create(3225, 3253);
	
	public static final Location DAEMONHEIM = Location.create(3447, 3696);

	public static final Location CONSTRUCTION = Location.create(3079, 3493);

	public static final Location SUMMONING = Location.create(2209, 5348);

	public static final Location HUNTER = Location.create(2556, 2845);
	
	public static final Location GRAY_CHINS = Location.create(2334, 3587);
	
	public static final Location RED_CHINS = Location.create(2556, 2913);
	
	public static final Location BLACK_CHINS = Location.create(3135, 3780);

	public static final Location REVENANTS = Location.create(3129, 3832);

	public static final Location CATACOMBS_OF_KOUREND = Location.create(8066, 10050);
	
	public static final Location NIEVES_CAVE = Location.create(8829, 9825);
	
	public static final Location LITHKREN_VAULT = Location.create(9949, 10448);
	
	public static final Location JORMUNGANDS_PRISON = Location.create(8861, 10417);
	
	public static final Location KARUULM_SLAYER_DUNGEON = Location.create(7711, 10205);
	
	public static final Location CRABCLAW_CAVES = Location.create(8047, 9847);
	
	public static final Location LIZARDMAN_CAVES = Location.create(7705, 9972);
	
	public static final Location WYVERN_CAVE = Location.create(10004, 10231);
	
	public static final Location LIVING_ROCK_CAVERNS = Location.create(3651, 5122);

	public static final Location SMOKE_DEVIL_DUNGEON = Location.create(8804, 9415);
	
	public static final Location KALPHITE_CAVE = Location.create(9705, 9497);

	public static final Location CAVE_HORROR = Location.create(10148, 9374);

	public static final Location WHITE_PORTAL = Location.create(2812, 5510);

	public static final Location FARMING_ARDOUGNE = Location.create(2664, 3374);

	public static final Location FARMING_FALADOR = Location.create(3052, 3305);

	public static final Location FARMING_CANIFIS = Location.create(3599, 3523);

	public static final Location FARMING_CATHERBY = Location.create(2807, 3463);

	public static final Location VARROCK_TREE_FARM = Location.create(3230, 3456);

	public static final Location LUMBRDGE_TREE_FARM = Location.create(3196, 3231);

	public static final Location FALADOR_TREE_FARM = Location.create(3004, 3376);

	public static final Location TAVERLY_TREE_FARM = Location.create(2933, 3438);

	public static final Location DWARVEN_MINES = Location.create(3055, 9777);

	public static final Location NECHRYAEL = Location.create(3427, 3558);

	public static final Location ABYSSAL_DEMONS = Location.create(3412, 3551);

	public static final Location GLACORS = Location.create(2630, 4010);

	public static final Location KALPHITE_QUEEN = Location.create(3507, 9494);

	public static final Location DARK_BEASTS = Location.create(2907, 9702);

	public static final Location MUTATED_JADINKOS = Location.create(2723, 10143);

	public static final Location NEX = Location.create(2885, 5279);

	public static final Location BRIMHAVEN_DUNGEON = Location.create(2712, 9564);

	public static final Location LAVA_DRAGONS = Location.create(3202, 3859);
	
	public static final Location ELDER_CHAOS_DRUIDS = Location.create(3236, 3630);

	public static final Location GIANT_MOLE = Location.create(2982, 3387);
	
	public static final Location KRAKEN = Location.create(8682, 10015);
	
	public static final Location BOSS_INSTANCE = Location.create(3070, 3516);
	
	public static final Location SOULWARS = Location.create(1886, 3178);
	
	public static final Location INSIDE_PET_PORTAL = Location.create(3276, 4368, 100);
	
	public static final Location OUTSIDE_PET_PORTAL = Location.create(3092, 3510);
	
	public static final Location CHAMBER_OF_XERIC = Location.create(7654, 3570);
	public static final Location INFERNO = Location.create(8895, 5113);
	public static final Location THEATRE_OF_BLOOD = Location.create(10076, 3219);

	/**
	 * SoulPvP Teleports
	 */

	public static final Location FROST_DRAGONS = Location.create(2966, 3935);

	public static final Location EDGE_PVP = Location.create(3087, 3517);

	public static final Location MAGEBANK_PVP = Location.create(2539, 4716);

	public static final Location CLANWARS_PVP = Location.create(3273, 3686);

	public static final Location EASTDRAGONS_PVP = Location.create(3351, 3659);

	public static final Location WESTDRAGONS_PVP = Location.create(2979, 3597);
	
	public static final Location EDGE = Location.create(3087, 3500);

	/**
	 * Glory locations.
	 */
	public static final int EDGEVILLE_X = 3087;

	public static final int EDGEVILLE_Y = 3500;
	
	public static final Location EDGEVILLE = Location.create(3087, 3500);

	public static final int AL_KHARID_X = 3293;

	public static final int AL_KHARID_Y = 3174;

	public static final String AL_KHARID = "";

	public static final int KARAMJA_X = 2953;

	public static final int KARAMJA_Y = 3147;

	public static final String KARAMJA = "";

	public static final int MAGEBANK_X = 2538;

	public static final int MAGEBANK_Y = 4716;

	public static final String MAGEBANK = "";
	
	public static final Location PK_TOURNAMENT_SPECTATE = Location.create(10062, 9146, 0);
	
	public static final Location KRAKEN_ENTRANCE = Location.create(8680, 10016);
	
	public static final Location KRAKEN_ENTRANCE_INSTANCED = Location.create(10096, 5798);

	/**
	 * Teleport spells.
	 **/
	/*
	 * Modern spells
	 */
	public static final int VARROCK_X = 3210;

	public static final int VARROCK_Y = 3424;

	public static final String VARROCK = "";

	public static final int LUMBY_X = 3222;

	public static final int LUMBY_Y = 3218;

	public static final String LUMBY = "";

	public static final int FALADOR_X = 2964;

	public static final int FALADOR_Y = 3378;

	public static final String FALADOR = "";

	public static final int CAMELOT_X = 2757;

	public static final int CAMELOT_Y = 3477;

	public static final String CAMELOT = "";

	public static final int ARDOUGNE_X = 2662;

	public static final int ARDOUGNE_Y = 3305;

	public static final String ARDOUGNE = "";

	public static final int WATCHTOWER_X = 3087;

	public static final int WATCHTOWER_Y = 3500;

	public static final String WATCHTOWER = "";

	public static final int TROLLHEIM_X = 3243;

	public static final int TROLLHEIM_Y = 3513;

	public static final String TROLLHEIM = "";

	public static final int APEATOLL_X = 2801;

	public static final int APEATOLL_Y = 2704;

	public static final String APEATOLL = "";

	public static final int YANILLE_X = 2606;

	public static final int YANILLE_Y = 3093;

	/*
	 * Ancient spells
	 */
	public static final int PADDEWWA_X = 3098;

	public static final int PADDEWWA_Y = 9884;

	public static final int SENNTISTEN_X = 3322;

	public static final int SENNTISTEN_Y = 3336;

	public static final int KHARYRLL_X = 3492;

	public static final int KHARYRLL_Y = 3471;

	public static final int LASSAR_X = 3006;

	public static final int LASSAR_Y = 3471;

	public static final int DAREEYAK_X = 3161;

	public static final int DAREEYAK_Y = 3671;

	public static final int CARRALLANGAR_X = 3156;

	public static final int CARRALLANGAR_Y = 3666;

	public static final int ANNAKARL_X = 3288;

	public static final int ANNAKARL_Y = 3886;

	public static final int GHORROCK_X = 2977;

	public static final int GHORROCK_Y = 3873;

	public static final Location DRAYNOR = Location.create(3092, 3248);

	public static final Location YANILLE = Location.create(2606, 3093);

	public static final int DUELNOLOWX = 3366;

	public static final int DUELNMHIGHX = 3388;

	public static final int DUELNMLOWY = 3226;

	public static final int DUELNMHIGHY = 3239;

}
