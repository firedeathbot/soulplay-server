package com.soulplay.game.world;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.sql.SqlHandler;

public class AnnouncementManager {

	private static Map<Integer, String> ANNOUNCEMENTS = new HashMap<Integer, String>() {

		private static final long serialVersionUID = 2093373549787217610L;

		{
			put(1, "<img=6> <col=ff>Join the Clan Chat 'Help' if you need any help!");
			put(2, "<img=6> <col=ff>Visit our Forums (</col>::forums<col=ff>) for information!");
			put(3, "<img=6> <col=ff>Follow us on Twitter (</col>::twitter<col=ff>) to stay up to date!");
			put(4, "<img=6> <col=ff>Subscribe to our Youtube (</col>::youtube<col=ff>) for awesome videos!");
			//put(5, "<img=6> <col=ff>Like our Facebook (</col>::facebook<col=ff>) for giveaways and more!");
			put(5, "<img=6> <col=ff>Facebook giveaway event (</col>::thread 25540<col=ff>) a lot of rewards!!!");
			put(6, "<img=6> <col=ff>You can donate with OSRS or RS3 GP (</col>::rsdonate<col=ff>)");
			put(7, "<img=6> <col=ff>You can find guides by checking out our (</col>::youtube<col=ff>) channel.");
			put(8, "<img=6> <col=ff>Report all bugs you find to make game better! (</col>::report<col=ff>).");
		}
	};

	static int lastOrder = 0;

	private static void addUpdatesToAnnouncements() {
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			return;
		}
		String query = "SELECT title, thread_id FROM `xf_thread` where node_id = 11 ORDER BY thread_id DESC LIMIT 1";

		java.sql.Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://websitesql.soulplayps.com:3306/ssforums" + Config.MYSQL_DB_EXTENSIONS,
					"fromgame", "mgky0IFN6VQ1fmeP");
			java.sql.Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				ANNOUNCEMENTS.put(ANNOUNCEMENTS.size() + 1,
						rs.getString("title"));
				ANNOUNCEMENTS.put(ANNOUNCEMENTS.size() + 2,
						"Read more at ::thread " + rs.getInt("thread_id"));
			}
			conn.close();
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void announce() {
		List<String> sendList = new ArrayList<>();
		for (Entry<Integer, String> entry : ANNOUNCEMENTS.entrySet()) {
			if (entry.getKey() == lastOrder + 1) {
				sendList.add(entry.getValue());
				lastOrder = entry.getKey();
				if (lastOrder == ANNOUNCEMENTS.size() - 1) { // post the 2 last
																// messages
																// together
																// (update
																// thread)
					sendList.add(ANNOUNCEMENTS.get(ANNOUNCEMENTS.size() + 1));
					lastOrder = 0;
				}
				if (!sendList.isEmpty()) {
					for (String s : sendList) {
						if (s != null) {
							Server.getTaskScheduler().schedule(new Task(true) {

								@Override
								protected void execute() {
									PlayerHandler.messageAllPlayers(s);
									this.stop();
								}
							});
						}
					}
				} else {
					lastOrder = 0;
				}
				return;
			}
		}
	}

	public static void updateAnnouncements() {
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;
		String query = "SELECT * FROM announcements";

		if (!Config.RUN_ON_DEDI) {
			query = "SELECT * FROM announcements";
		}

		try {
			connection = SqlHandler.getHikariZaryteTable().getConnection();

			ps = connection.prepareStatement(query);
			ResultSet results = ps.executeQuery();
			ANNOUNCEMENTS.clear();
			while (results.next()) {
				ANNOUNCEMENTS.put(results.getInt("order"),
						results.getString("message"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		addUpdatesToAnnouncements();
	}

	public AnnouncementManager() throws InterruptedException, Exception {
		AtomicBoolean stop = new AtomicBoolean(false);
		Executors.newSingleThreadExecutor().execute(() -> {
			while (!stop.get()) {
				try {
					Thread.sleep(5 * 60 * 1000);
					if (DBConfig.UPDATE_ANNOUNCEMENTS) {
						updateAnnouncements();
					}
					if (DBConfig.ANNOUNCE) {
						announce();
					}
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(this.getClass(), e));
				}
			}
		});
	}
}
