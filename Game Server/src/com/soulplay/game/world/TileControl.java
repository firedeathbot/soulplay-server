package com.soulplay.game.world;

import com.soulplay.game.world.entity.Mob;

/**
 * @author Killamess
 * @editor Aleksandr
 */
public class TileControl {

	public static int calculateDistance(Mob client, Mob following) {

		Tile[] tiles = getTiles(client);

		int[] location = currentLocation(client);
		int[] pointer = new int[tiles.length];

		int lowestCount = 20, count = 0;

		for (Tile newTiles : tiles) {
			if (newTiles.getTile() == location) {
				pointer[count++] = 0;
			} else {
				pointer[count++] = calculateDistance(newTiles, following);
			}
		}
		for (int element : pointer) {
			if (element < lowestCount) {
				lowestCount = element;
			}
		}

		return lowestCount;
	}

	public static int calculateDistance(int[] location, int[] other) {
		int X = Math.abs(location[0] - other[0]);
		int Y = Math.abs(location[1] - other[1]);
		return X > Y ? X : Y;
	}

	public static int calculateDistance(Tile location, Mob other) {
		int X = Math.abs(location.getTile()[0] - other.getCurrentLocation().getX());
		int Y = Math.abs(location.getTile()[1] - other.getCurrentLocation().getY());
		return X > Y ? X : Y;
	}

	public static int[] currentLocation(Mob client) {
		int[] currentLocation = new int[3];
		if (client != null) {
			currentLocation[0] = client.getCurrentLocation().getX();
			currentLocation[1] = client.getCurrentLocation().getY();
			currentLocation[2] = client.getCurrentLocation().getZ();
		}
		return currentLocation;
	}

	public static int[] currentLocation(Tile tileLocation) {

		int[] currentLocation = new int[3];

		if (tileLocation != null) {
			currentLocation[0] = tileLocation.getTile()[0];
			currentLocation[1] = tileLocation.getTile()[1];
			currentLocation[2] = tileLocation.getTile()[2];
		}
		return currentLocation;
	}

	public static Tile generate(int x, int y, int z) {
		return new Tile(x, y, z);
	}

	public static Tile[] getTiles(Mob client) {

		int size = 1, tileCount = 0;

		Tile[] tiles = new Tile[size * size];

		if (tiles.length == 1) {
			tiles[0] = generate(client.getCurrentLocation().getX(), client.getCurrentLocation().getY(), client.getCurrentLocation().getZ());
		} else {
			// for (int x = 0; x < size; x++)
			// for (int y = 0; y < size; y++)
			tiles[tileCount++] = generate(client.getCurrentLocation().getX(), client.getCurrentLocation().getY(), client.getCurrentLocation().getZ());
		}
		return tiles;
	}

}
