package com.soulplay.game.world;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.soulplay.Config;
import com.soulplay.content.minigames.junkmachine.JunkMachine;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.shop.Shop;
import com.soulplay.game.model.shop.ShopItem;

/**
 * Shops
 **/

public class ShopHandler {

	public static int MaxShops = 200;

	public static int MaxShopItems = 200;

	public static int MaxShowDelay = 10;

	public static int MaxSpecShowDelay = 60;

	public static int TotalShops = 0;

	public static int[][] ShopItems = new int[MaxShops][MaxShopItems];

	public static int[][] ShopItemsN = new int[MaxShops][MaxShopItems];

	public static int[][] ShopItemsDelay = new int[MaxShops][MaxShopItems];

	public static int[][] ShopItemsSN = new int[MaxShops][MaxShopItems];

	public static int[] ShopItemsStandard = new int[MaxShops];

	public static String[] ShopName = new String[MaxShops];

	public static int[] ShopSModifier = new int[MaxShops];

	public static int[] ShopBModifier = new int[MaxShops];
	
	public static final int[] UNSELLABLE_SHOPS = {25, 20, 32, 34, 35, 45, 50, 84,
	102};

	public static final int[] OTHER_SHOPS = {8, 9, 10, 12, 13, 15, 18, 25, 33,
	34, 35, 36, 38, 41, 43, 45, 46, 49, 50, 52, 53, 55, 56, 57, 58, 60, 61, 65, 66, 67, 82, 84, 102, 105, 76, 80};

	public static final Set<Integer> OTHER_SHOPS_SET = new HashSet<>();

	public static final Set<Integer> REGULAR_SHOP_ITEMS = new HashSet<>();

	public static final int[] skillCapes = {9747, 9753, 9750, 9768, 9756, 9759, 9762, 9801,
	9807, 9783, 9798, 9804, 9780, 9795, 9792, 9774, 9771, 9777, 9786,
	9810, 9765, 18508, 9948, 12169, 9789};

	public static void DiscountItem(int ShopID, int ArrayID) {
		ShopItemsN[ShopID][ArrayID] -= 1;
		if (ShopItemsN[ShopID][ArrayID] <= 0) {
			ShopItemsN[ShopID][ArrayID] = 0;
			ResetItem(ShopID, ArrayID);
		}
	}

	public static void init() {

		for (int shopId : ShopHandler.OTHER_SHOPS) {
			ShopHandler.OTHER_SHOPS_SET.add(shopId);
		}
		
		for (int i = 0; i < MaxShops; i++) {
			for (int j = 0; j < MaxShopItems; j++) {
				ResetItem(i, j);
				ShopItemsSN[i][j] = 0;
			}
			ShopItemsStandard[i] = 0;
			ShopSModifier[i] = 0;
			ShopBModifier[i] = 0;
			ShopName[i] = "";
		}
		TotalShops = 0;
		loadShops(Paths.get("Data", "json", "shops.json"));
		//dumpShops();
	}

	private static void loadShops(Path path) {
		try(FileReader reader = new FileReader(path.toFile())) {
			final Shop[] shops = GsonSave.gsond.fromJson(reader, Shop[].class);
			Objects.requireNonNull(shops);

			TotalShops = shops.length;

			for (Shop shop : shops) {

				if (shop == null) {
					continue;
				}

				final int shopId = shop.getId();
				ShopName[shopId] = shop.getName();
				ShopSModifier[shopId] = shop.getSellType();
				ShopBModifier[shopId] = shop.getBuyType();
				
				final boolean isRegularShopItem = !ShopHandler.OTHER_SHOPS_SET.contains(shopId);

				for (int i = 0; i < shop.getItems().length; i++) {
					Item item = shop.getItems()[i];
					Objects.requireNonNull(item);
					ShopItems[shopId][i] = item.getId() + 1;
					ShopItemsN[shopId][i] = item.getAmount();
					ShopItemsSN[shopId][i] = item.getAmount();
					ShopItemsStandard[shopId]++;

					JunkMachine.addShopItem(item.getId() + 1);
					JunkMachine.addShopItem(item.getId());
					
					if (isRegularShopItem)
						ShopHandler.REGULAR_SHOP_ITEMS.add(item.getId());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void dumpShops() {
		List<Shop> shops = new ArrayList<>();

		try(FileWriter writer = new FileWriter(new File("./shops.json"))) {
			for (int i = 0; i < ShopName.length; i++) {
				String name = ShopName[i];
				int sellType = ShopSModifier[i];
				int buyType = ShopBModifier[i];

				if (name == null || name.isEmpty()) {
					continue;
				}

				List<ShopItem> shopItems = new ArrayList<>();

				for (int j = 0; j < ShopItems.length; j++) {
					int itemId = ShopItems[i][j];

					if (itemId == 0) {
						continue;
					}

					int amount = ShopItemsN[i][j];

					shopItems.add(new ShopItem(itemId - 1, amount, amount));

				}

				shops.add(new Shop(i, name, sellType, buyType, shopItems.toArray(new ShopItem[0])));

			}
			writer.write(GsonSave.gson.toJson(shops));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void process() {
		boolean DidUpdate = false;
		for (int i = 1; i < /* = TotalShops */MaxShops; i++) {
			for (int j = 0; j < MaxShopItems; j++) {
				if (ShopItems[i][j] > 0) {
					if (ShopItemsDelay[i][j] >= MaxShowDelay) {
						if (j <= ShopItemsStandard[i]
								&& ShopItemsN[i][j] <= ShopItemsSN[i][j]) {
							if (ShopItemsN[i][j] < ShopItemsSN[i][j]) {
								ShopItemsN[i][j] += 1;
								DidUpdate = true;
								ShopItemsDelay[i][j] = 1;
								ShopItemsDelay[i][j] = 0;
								DidUpdate = true;
							}
						} else if (ShopItemsDelay[i][j] >= MaxSpecShowDelay) {
							DiscountItem(i, j);
							ShopItemsDelay[i][j] = 0;
							DidUpdate = true;
						}
					}
					ShopItemsDelay[i][j]++;
				}
			}
			if (DidUpdate == true) {
				for (int k = 1; k < Config.MAX_PLAYERS; k++) {
					if (PlayerHandler.players[k] != null) {
						if (PlayerHandler.players[k].isShopping == true
								&& PlayerHandler.players[k].myShopId == i) {
							PlayerHandler.players[k].updateShop = true;
							DidUpdate = false;
							PlayerHandler.players[k].updateshop(i);
						}
					}
				}
				DidUpdate = false;
			}
		}
	}

	public static int getMaxShopItems(Client c) {
		return c.shopType.getMaxItems();
	}

	public static void ResetItem(int ShopID, int ArrayID) {
		ShopItems[ShopID][ArrayID] = 0;
		ShopItemsN[ShopID][ArrayID] = 0;
		ShopItemsDelay[ShopID][ArrayID] = 0;
	}

}
