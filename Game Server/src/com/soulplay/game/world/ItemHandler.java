package com.soulplay.game.world;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.items.degradeable.PvpDegradeableEnum;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.player.skills.herblore.Herblore;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemAssistant;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.PickedUpItem;
import com.soulplay.game.model.item.definition.RecoilRingEnum;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;
import com.soulplay.game.sounds.Sounds;

/**
 * Handles ground items
 **/

public class ItemHandler {

	public static final List<GroundItem> items = new ArrayList<>();

	public static final int HIDE_TICKS = 100;// 150;

	/**
	 * Creates the ground item
	 **/
	public static final int[][] brokenBarrows = {{4708, 4860}, {4710, 4866},
			{4712, 4872}, {4714, 4878}, {4716, 4884}, {4720, 4896},
			{4718, 4890}, {4720, 4896}, {4722, 4902}, {4732, 4932},
			{4734, 4938}, {4736, 4944}, {4738, 4950}, {4724, 4908},
			{4726, 4914}, {4728, 4920}, {4730, 4926}, {4745, 4956},
			{4747, 4926}, {4749, 4968}, {4751, 4994}, {4753, 4980},
			{4755, 4986}, {4757, 4992}, {4759, 4998}};

	/**
	 * Adds item to list
	 **/
	public static void addItem(GroundItem item) {
		items.add(item);
		Region newRegion = Server.getRegionManager()
				.getRegionByLocation(item.getItemX(), item.getItemY(), item.getItemZ());
		newRegion.addGroundItem(item);
	}

	/**
	 * Shows items for everyone who is within 60 squares
	 **/
	public static void makeItemGloballyVisible(GroundItem i) {
		// for (Player p : PlayerHandler.players) {
		for (Player p : Server.getRegionManager()
				.getPlayersForRenderedRegionsAllZ(i.getItemX(), i.getItemY(),
						i.getItemZ())) {
			if (p != null) {
				Client person = (Client) p;

				// for (Player p :
				// Server.getRegionManager().getLocalPlayersByLocation(i.getItemX(),
				// i.getItemY(), i.getItemZ())) {
				// Client person = (Client)p;

				if (person != null) {
					if (!i.getOwnerName().equals("server") && i.getOwnerName().toLowerCase()
							.equals(person.getName().toLowerCase())) {
						continue; // use this instead of itemcontollerid cuz
					}
					
					if (!i.isDisplayForOthers()) {
						// need here
						continue;
					}
					if (!ItemAssistant.tradeable(i.getItemId())) {
						continue;
					}
					if (Misc.isInsideRenderedArea(p, i.getItemX(), i.getItemY())) {
						if (p.getZ() == i.getItemZ())
							person.getItems().createGroundItemPacket(i.getItemId(),
									i.getItemX(), i.getItemY(), i.getItemAmount());
						else
							p.itemsToRemove.add(i);
					}
				}
			}
		}
	}
	
	private static boolean itemDroppedInMinigameArena(int x, int y) {
		if (RSConstants.inLMSArena(x, y) || RSConstants.inSoulWars(x, y)) {
			return true;
		}
		return false;
	}

	public static void createGroundItem(Client c, GameItem gi, Location loc) {
		createGroundItem(c, gi.getId(), loc.getX(), loc.getY(), loc.getZ(),
				gi.getAmount());
	}

	public static void createGroundItem(GroundItem item,
			boolean visibleToEveryone) {
		addItem(item);
		item.setInMinigame(itemDroppedInMinigameArena(item.getItemX(), item.getItemY()));
		item.setServerWideDrop(visibleToEveryone);

		for (Player p : Server.getRegionManager()
				.getPlayersForRenderedRegionsAllZ(item.getItemX(),
						item.getItemY(), item.getItemZ())) {

			if (!p.isActive) {
				continue;
			}


			// if the item is not visible to everyone, we need to check who can
			// see this item.
			if (!visibleToEveryone) {
				if (!item.getOwnerName()
						.equalsIgnoreCase(p.getName().toLowerCase())) {
					continue;
				}
			} else {
				if (item.getRemoveTicks() == 0) {
					item.setRemoveTicks(item.hideTicks);
					item.hideTicks = 0;
				}
			}

			// non-tradeable items should not appear
			if (!ItemAssistant.tradeable(item.getItemId())) {
				continue;
			}

			// if a player is within a certain distance, the item should appear
			// to them
			
			if (Misc.isInsideRenderedArea(p, item.getItemX(), item.getItemY())) {
				if (p.getZ() == item.getItemZ())
					p.getItems().createGroundItemPacket(item.getItemId(),
							item.getItemX(), item.getItemY(), item.getItemAmount());
				else
					p.itemsToRemove.add(item);
			}

		}

	}

	// TODO: Make sure item amount is correct everywhere used
	public static void createGroundItem(Player c, int itemId, int itemX,
			int itemY, int itemZ, int itemAmount) {
		// if (c == null)
		// return;
		if (c != null && c.isActive && c.playerRights == 2
				&& !DBConfig.ADMIN_DROP_ITEMS && Config.RUN_ON_DEDI) {
			c.sendMessage("Your items disappear because you're an admin.");
			return;
		}

		int showTicks;
		if (c.tobManager != null) {
			showTicks = 2;
		} else {
			if (itemId == 122516) {
				return;
			}

			showTicks = HIDE_TICKS;
		}

		if (itemZ < 0) { // bugged drop??? just in case

			itemX = LocationConstants.EDGEVILLE_X;
			itemY = LocationConstants.EDGEVILLE_Y;
			itemZ = 0;

			if (c != null && c.isActive) {
				c.sendMessage(
						"Your dropped items bugged. They will be at Edgeville Home.");
				System.out.println("Bugged drop!!!! tried to drop at X:" + itemX
						+ " Y:" + itemY + " Z:" + itemZ + "  ITEMID:" + itemId
						+ " Amount:" + itemAmount);
			}

		}
		
		final boolean inMinigame = itemDroppedInMinigameArena(itemX, itemY);

		String name = "julius";

		int mid = 0;

		boolean ironManMode = false;

		if (c != null && c.isActive) {
			name = c.getName();
			mid = c.mySQLIndex;
			if (c.isIronMan()) {
				ironManMode = true;
			}
			if (RSConstants.zulrahArea(c.absX, c.absY) || RSConstants.vorkathFightArea(itemX, itemY)) { // drop items right under player XD
				itemX = c.getX();
				itemY = c.getY();
			}
		}

		if (itemId > 0) {
			if (itemId >= 2412 && itemId <= 2414) {
				if (c != null && c.isActive) {
					c.sendMessage(
							"The cape vanishes as it touches the ground.");
				}
				return;
			}
			// for (int jj = 0; jj < Config.ITEM_TRADEABLE.length; jj++) {
			// if (itemId == Config.ITEM_TRADEABLE[jj]) {
			// return;
			// }
			// }
			/*
			 * if (itemId > 4705 && itemId < 4760) { for (int j = 0; j <
			 * brokenBarrows.length; j++) { if (brokenBarrows[j][0] == itemId) {
			 * itemId = brokenBarrows[j][1]; break; } } }
			 */

			if (!ItemProjectInsanity.itemStackable[itemId] && itemAmount > 0) { // nonstackable
																	// items
				for (int j = 0; j < itemAmount; j++) {
					if (c != null && c.isActive) {
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, 1);
					}
					GroundItem item = new GroundItem(itemId, itemX, itemY,
							itemZ, 1, 1234, showTicks, name, mid, ironManMode);
					if (c != null && c.isActive && c.droppingItem) {
						item.setPlayerDropped(true);
					}
					item.setInMinigame(inMinigame);
					addItem(item);
					// c.sendMessage("not stackable");
				}
			} else { // stackable items
				GroundItem stackedItem = getGroundItemOfOwner(name, itemId, itemX,
						itemY, itemZ);
				if (stackedItem != null && stackedItem.ownerName.equals(name)
						&& stackedItem.hideTicks > 0
						&& (stackedItem.getItemAmount()
								+ (long) itemAmount <= Integer.MAX_VALUE)) { // if
																				// item
																				// owned
																				// by
																				// player
																				// and
																				// still
																				// hidden
																				// item
					int newAmount = stackedItem.getItemAmount() + itemAmount;
					if (c != null && c.isActive) {
						c.getItems().removeGroundItem(stackedItem.getItemId(),
								stackedItem.getItemX(), stackedItem.getItemY(),
								stackedItem.getItemAmount());
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, newAmount);
					}
					stackedItem.setItemAmount(newAmount);
					stackedItem.hideTicks = showTicks;
				} else {
					if (c != null && c.isActive) {
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, itemAmount);
					}
					GroundItem item = new GroundItem(itemId, itemX, itemY,
							itemZ, itemAmount, 1234, showTicks, name, mid,
							ironManMode);
					item.setInMinigame(inMinigame);
					addItem(item);
					// c.sendMessage("stackable. amount:"+itemAmount);
				}
			}
		}
	}

	public static void createGroundItem(String ownerName, int itemId, int itemX,
			int itemY, int itemZ, int itemAmount, boolean npcDrop,
			boolean ironManMode) {
		if (itemId > 0) {

			final Client c = (Client) PlayerHandler.getPlayer(ownerName);

			if (itemZ < 0) { // bugged drop??? just in case

				itemX = LocationConstants.EDGEVILLE_X;
				itemY = LocationConstants.EDGEVILLE_Y;
				itemZ = 0;

				if (c != null && c.isActive) {
					c.sendMessage(
							"Your dropped items bugged. They will be at Edgeville Home.");
					System.out.println("Bugged drop!!!! tried to drop at X:"
							+ itemX + " Y:" + itemY + " Z:" + itemZ
							+ "  ITEMID:" + itemId + " Amount:" + itemAmount);
				}

			}
			
			final boolean dropInMinigame = itemDroppedInMinigameArena(itemX, itemY);
			
			int mid = 0;
			
			String name = "julius";

			if (c != null && c.isActive) {
				mid = c.mySQLIndex;
				
				name = c.getName();
				
				if (RSConstants.zulrahArea(c.getX(), c.getY()) || RSConstants.vorkathFightArea(itemX, itemY)) { // drop items right under player XD
					itemX = c.getX();
					itemY = c.getY();
				}
			}

			if (itemId >= 2412 && itemId <= 2414) {
				if (c != null && c.isActive) {
					c.sendMessage(
							"The cape vanishes as it touches the ground.");
				}
				return;
			}

			if (!ItemProjectInsanity.itemStackable[itemId] && itemAmount > 0) {
				for (int j = 0; j < itemAmount; j++) {
					if (c != null && c.isActive && c.getZ() == itemZ) {
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, 1);
					}
					GroundItem item = new GroundItem(itemId, itemX, itemY,
							itemZ, 1, 1234, HIDE_TICKS, name, mid,
							ironManMode);
					addItem(item);
					item.setInMinigame(dropInMinigame);
					// c.sendMessage("not stackable");
				}
			} else {
				GroundItem stackedItem = getGroundItemOfOwner(name, itemId, itemX,
						itemY, itemZ); // TODO: remove c variable
				if (stackedItem != null
						&& stackedItem.ownerName.equals(name)
						&& stackedItem.hideTicks > 0
						&& (stackedItem.getItemAmount()
								+ (long) itemAmount <= Integer.MAX_VALUE)) {
					int newAmount = stackedItem.getItemAmount() + itemAmount;
					if (c != null && c.isActive && c.getZ() == itemZ) {
						c.getItems().removeGroundItem(stackedItem.getItemId(),
								stackedItem.getItemX(), stackedItem.getItemY(),
								stackedItem.getItemAmount());
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, newAmount);
					}
					stackedItem.setItemAmount(newAmount);
					stackedItem.hideTicks = HIDE_TICKS;
				} else {
					if (c != null && c.isActive && c.getZ() == itemZ) {
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, itemAmount);
					}
					GroundItem item = new GroundItem(itemId, itemX, itemY,
							itemZ, itemAmount, 1234, HIDE_TICKS, name, mid,
							ironManMode);
					addItem(item);
					item.setInMinigame(dropInMinigame);
					// c.sendMessage("stackable. amount:"+itemAmount);
				}
				stackedItem = null;
			}
		}
	}

	public static void createGroundItemFromPlayerDeath(Player c,
			String spawnForPlayerName, int itemId, int itemX, int itemY,
			int itemZ, int itemAmount, Player originalOwner) {
		createGroundItemFromPlayerDeath(c, spawnForPlayerName, itemId, itemX, itemY, itemZ, itemAmount, originalOwner, null);
	}

	// TODO: make sure the item amount is correct everywhere it's used
	public static void createGroundItemFromPlayerDeath(Player c,
			String spawnForPlayerName, int itemId, int itemX, int itemY,
			int itemZ, int itemAmount, Player originalOwner, Consumer<GroundItem> consumer) {
		// if (c == null) //TODO: remove this PI shit and put it into task
		// system to secure that items spawn normally...
		// return;
		
		boolean killedInMinigame = itemDroppedInMinigameArena(itemX, itemY);
		
		if (c != null && c.isActive && c.soulWarsTeam != Team.NONE) {
			killedInMinigame = true;
		}

		if (!killedInMinigame && c != null && c.isActive && c.playerRights == 2
				&& !DBConfig.ADMIN_DROP_ITEMS && Config.RUN_ON_DEDI) {
			c.sendMessage("Your items disappear because you're an admin.");
			return;
		}

		int mID = originalOwner.mySQLIndex;

		if (itemZ < 0) { // bugged drop??? just in case

			itemX = LocationConstants.EDGEVILLE_X;
			itemY = LocationConstants.EDGEVILLE_Y;
			itemZ = 0;

			if (c != null && c.isActive) {
				c.sendMessage(
						"Your dropped items bugged. They will be at Edgeville Home.");
				System.out.println("Bugged drop!!!! tried to drop at X:" + itemX
						+ " Y:" + itemY + " Z:" + itemZ + "  ITEMID:" + itemId
						+ " Amount:" + itemAmount);
			}

		}

		String name = spawnForPlayerName;
		
		if (name == null)
			name = "julius";

		boolean ironManMode = false;
		
		boolean displayForIronman = true;

		//some ironman cheaphaxes
		if (c != null && c.isActive) {
			if (!killedInMinigame && c.isIronMan()) {
				ironManMode = true;
				if (c == originalOwner) {
					displayForIronman = true;
					name = originalOwner.getName();
				} else {
					displayForIronman = false;
					name = "server";
				}
			}
			if (killedInMinigame) {
				mID = c.mySQLIndex;
				name = c.getName();
			}
		}

		if (itemId > 0) {
			if (itemId >= 2412 && itemId <= 2414) {
				if (c != null && c.isActive) {
					c.sendMessage(
							"The cape vanishes as it touches the ground.");
				}
				return;
			}

			if (!ItemProjectInsanity.itemStackable[itemId] && itemAmount > 0) { // nonstackable
																	// items
				for (int j = 0; j < itemAmount; j++) {
					if (c != null && c.isActive && c.getZ() == itemZ && displayForIronman) {
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, 1);
					}
					GroundItem item = new GroundItem(itemId, itemX, itemY,
							itemZ, 1, 1234, HIDE_TICKS, name, mID, ironManMode);
					item.originalOwnerName = originalOwner.getName();
					item.setInMinigame(killedInMinigame);
					addItem(item);

					if (consumer != null) {
						consumer.accept(item);
					}
				}
			} else { // stackable items
				GroundItem stackedItem = getGroundItemOfOwner(name, itemId, itemX,
						itemY, itemZ);
				if (stackedItem != null && stackedItem.ownerName.equals(name)
						&& stackedItem.hideTicks > 0
						&& (stackedItem.getItemAmount()
								+ (long) itemAmount <= Integer.MAX_VALUE)) {
					//ground item exists and belongs to player and is hidden
					int newAmount = stackedItem.getItemAmount() + itemAmount;
					if (c != null && c.isActive && c.getZ() == itemZ) {
						c.getItems().removeGroundItem(stackedItem.getItemId(),
								stackedItem.getItemX(), stackedItem.getItemY(),
								stackedItem.getItemAmount());
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, newAmount);
					}
					stackedItem.setItemAmount(newAmount);
					stackedItem.hideTicks = HIDE_TICKS;
				} else {
					//ground item does not exist so create a new item
					if (c != null && c.isActive && c.getZ() == itemZ && displayForIronman) {
						c.getItems().createGroundItemPacket(itemId, itemX,
								itemY, itemAmount);
					}
					GroundItem item = new GroundItem(itemId, itemX, itemY,
							itemZ, itemAmount, 123, HIDE_TICKS, name, mID,
							ironManMode);
					item.originalOwnerName = originalOwner.getName();
					item.setInMinigame(killedInMinigame);
					addItem(item);

					if (consumer != null) {
						consumer.accept(item);
					}
				}
			}
		}
	}

	public static GroundItem getGroundItemOfOwner(String forPlayerName, int itemId,
			int itemX, int itemY, int itemZ) {
		// for (GroundItem i : items) {
		for (GroundItem i : Server.getRegionManager().getGroundItemsInOneRegion(itemX,
				itemY, itemZ)) { // c.getRegion().getGroundItems()) {
			if (i.getItemId() == itemId && i.getItemX() == itemX
					&& i.getItemY() == itemY) {
				
				//retarded debug
				if (i.getOriginalOwnerName() == null) { /*System.out.println("null item original owner name."); */continue; }
				if (forPlayerName == null) { System.out.println("forPlayerName is null"); continue; }
				
				if (!i.getOriginalOwnerName().equals(forPlayerName)) {
					return null;
				}
				return i;
			}
		}
		return null;
	}

	/*
	 * Average elapsed time: 1ms (rounded up). Don't worry, I did test it
	 * because I too was worried it would be slow.
	 */
	public static ArrayList<ItemDefinition> getItemsWithSimilarName(
			String originalName, ArrayList<ItemDefinition> list, int count) {
		class SimDataContainer {

			ItemDefinition item;

			double simIndex;

			protected SimDataContainer(ItemDefinition item, double simIndex) {
				this.item = item;
				this.simIndex = simIndex;
			}
		}
		if (count <= 0) {
			return new ArrayList<>();
		}
		ArrayList<SimDataContainer> simItems = new ArrayList<>();
		ArrayList<ItemDefinition> returnData = new ArrayList<>();
		for (int i = 0, len = list.size(); i < len; i++) {
			ItemDefinition item = list.get(i);
			if (item != null) {
				double simIndex = Misc.getSimilarityIndex(originalName,
						item.getName().replace("_", " "));
				if (simIndex < 0.50D && simIndex != 0.0D) {
					simItems.add(new SimDataContainer(item, simIndex));
				}
			}
		}
		Collections.sort(simItems, (arg0, arg1) -> ((Double) arg0.simIndex)
				.compareTo(arg1.simIndex));
		for (int i = 0; i < count && i < simItems.size(); i++) {
			returnData.add(simItems.get(i).item);
		}
		simItems.clear();
		simItems = null;
		return returnData;
	}

	public static GroundItem getPlayerOwnerGroundItem(String forPlayerName,
			int itemId, Location location) {
		return getPlayerOwnerGroundItem(forPlayerName, itemId, location.getX(), location.getY(), location.getZ());
	}

	public static GroundItem getPlayerOwnerGroundItem(String forPlayerName,
			int itemId, int itemX, int itemY, int itemZ) {
		for (GroundItem i : Server.getRegionManager().getGroundItemsInOneRegion(itemX,
				itemY, itemZ)) { // c.getRegion().getGroundItems()) {
			if (i.getItemId() == itemId && i.getItemX() == itemX
					&& i.getItemY() == itemY
					&& i.getOwnerName().equals(forPlayerName)) {
				return i;
			}
		}
		return null;
	}

	public static void init() { // ItemHandler() {
		Herblore.init();
		RecoilRingEnum.init();
		PvpDegradeableEnum.init();
	}
	
	/**
	 * Item amount
	 **/
	/*
	 * public int itemAmount(int itemId, int itemX, int itemY) { for (GroundItem
	 * i : items) { if (i.getItemId() == itemId && i.getItemX() == itemX &&
	 * i.getItemY() == itemY) { return i.getItemAmount(); } } return 0; }
	 */
	public static int itemAmount(Client c, int itemId, int itemX, int itemY,
			int itemZ) {
		for (GroundItem i : Server.getRegionManager().getGroundItemsInOneRegion(itemX,
				itemY, itemZ)) { // items) {
			if (i.hideTicks >= 1
					&& i.getOwnerName().equalsIgnoreCase(c.getName())) {
				if (i.getItemId() == itemId && i.getItemX() == itemX
						&& i.getItemY() == itemY) {
					return i.getItemAmount();
				}
			} else if (i.hideTicks < 1) {
				if (i.getItemId() == itemId && i.getItemX() == itemX
						&& i.getItemY() == itemY) {
					return i.getItemAmount();
				}
			}
		}
		return 0;
	}

	/**
	 * item exists inside the region the player is standing on
	 */
	public static boolean itemExists(Client c, int itemId, int itemX, int itemY,
			int itemZ) {
		for (GroundItem i : c.getRegion().getGroundItems()) {
			if (i.getItemId() == itemId && i.getItemX() == itemX
					&& i.getItemY() == itemY && i.getItemZ() == itemZ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Item exists
	 **/
	public static boolean itemExists(int itemId, int itemX, int itemY,
			int itemZ) {
		for (GroundItem i : Server.getRegionManager().getGroundItemsInOneRegion(itemX,
				itemY, itemZ)) { // items) {
			if (i.getItemId() == itemId && i.getItemX() == itemX
					&& i.getItemY() == itemY) {
				return true;
			}
		}
		return false;
	}

	/**
	 * item exists inside the region the player is standing on
	 */
	public static boolean itemExistsNearbyRegions(Client c, int itemId,
			int itemX, int itemY, int itemZ) {
		for (GroundItem i : Server.getRegionManager().getGroundItemsInOneRegion(itemX, itemY, itemZ)) {
			if (i.getItemId() == itemId) {
				return true;
			}
		}
		return false;
	}

	private static final List<GroundItem> toRemove = new ArrayList<>();

	private static final TickTimer stopWatch = new TickTimer();
	
	public static void process() {
		if (stopWatch.checkThenStart(HIDE_TICKS)) {
			spawnGlobalItems();
		}
		for (int j = 0, length = items.size(); j < length; j++) {
			GroundItem i = items.get(j);
			if (i == null) {
				continue;
			}

			if (i.hideTicks > 0) {
				i.hideTicks--;
			}
			if (i.hideTicks == 1) { // item can now be seen by others
				i.hideTicks = 0;
				// items on ground for others
				makeItemGloballyVisible(i);
				i.removeTicks = HIDE_TICKS;
			}

			if (i.removeTicks > 0) {
				i.removeTicks--;
			}
			if (i.removeTicks == 1) {
				i.removeTicks = 0;
				toRemove.add(i);
			}
		}

		for (int j = toRemove.size() - 1; j >= 0; j--) {
			GroundItem i = toRemove.get(j);
			removeGlobalItem(i);
		}

		toRemove.clear();
		/*
		 * for(GroundItem i : items) { if(i.hideTicks > 0) { i.hideTicks--; }
		 * if(i.hideTicks == 1) { // item can now be seen by others i.hideTicks
		 * = 0; createGlobalItem(i); i.removeTicks = HIDE_TICKS; }
		 * if(i.removeTicks > 0) { i.removeTicks--; } if(i.removeTicks == 1) {
		 * i.removeTicks = 0; removeGlobalItem(i, i.getItemId(), i.getItemX(),
		 * i.getItemY(), i.getItemAmount()); } }
		 */
	}

	/**
	 * Reloads any items if you enter a new region
	 **/
	public static void reloadItems(Client c) {
		ArrayList<GroundItem> notStackable = new ArrayList<>();
		ArrayList<GroundItem> clearItemsToRemove = new ArrayList<>();
		for (GroundItem i : c.itemsToRemove) {
			if (c.getHeightLevel() == i.getItemZ()) {
				c.getItems().removeGroundItem(i.getItemId(), i.getItemX(),
						i.getItemY(), i.getItemAmount());
				clearItemsToRemove.add(i);
			}
		}
		for (GroundItem i : clearItemsToRemove) {
			c.itemsToRemove.remove(i);
		}
		clearItemsToRemove.clear();
		clearItemsToRemove = null;
		for (GroundItem i : Server.getRegionManager().getRenderedGroundItemsByZ(c)) {
			if (!Misc.isInsideRenderedArea(c, i.getItemX(), i.getItemY()))
				continue;

			if (i.getOwnerName() == null) {
				i.removeTicks = 0;
				continue;
			}

			if (i.getOwnerName().equals(c.getName())
					/* || i.displayForOthers */ || ItemAssistant
					.tradeable(i.getItemId())) {
				if (i.hideTicks > 0
						&& i.getOwnerName().equals(c.getName())) {
					if (!ItemProjectInsanity.itemStackable[i.getItemId()]) {
						notStackable.add(i);
						c.getItems().removeGroundItem(i.getItemId(),
								i.getItemX(), i.getItemY(),
								i.getItemAmount());
					} else {
						c.getItems().removeGroundItem(i.getItemId(),
								i.getItemX(), i.getItemY(),
								i.getItemAmount());
						c.getItems().createGroundItemPacket(
								i.getItemId(), i.getItemX(),
								i.getItemY(), i.getItemAmount());
					}
				}
				if (i.hideTicks == 0) {
					if (!i.isDisplayForOthers()
							&& !i.getOwnerName().equals(c.getName())) {
						continue;
					}
					if (!ItemProjectInsanity.itemStackable[i.getItemId()]) {
						notStackable.add(i);
						c.getItems().removeGroundItem(i.getItemId(),
								i.getItemX(), i.getItemY(),
								i.getItemAmount());
					} else {
						c.getItems().removeGroundItem(i.getItemId(),
								i.getItemX(), i.getItemY(),
								i.getItemAmount());
						c.getItems().createGroundItemPacket(
								i.getItemId(), i.getItemX(),
								i.getItemY(), i.getItemAmount());
					}
				}
			}
		}
		for (GroundItem i : notStackable) { // add the none stackable items
			// after removing the ground item if
			// there were some
			c.getItems().createGroundItemPacket(i.getItemId(), i.getItemX(),
					i.getItemY(), i.getItemAmount());
		}
		notStackable.clear();
		notStackable = null;
	}

	/**
	 * Remove item for just the item controller (item not global yet)
	 **/

	public static void removeControllersItem(GroundItem i, Client c, int itemId,
			int itemX, int itemY, int itemAmount) {
		c.getItems().removeGroundItem(itemId, itemX, itemY, itemAmount);
		removeItem(i);
	}

	/**
	 * Remove item for everyone within 60 squares
	 **/

	public static void removeGlobalItem(GroundItem item) {
		// for (Player p : PlayerHandler.players) {
		for (Player p : Server.getRegionManager()
				.getPlayersForRenderedRegionsAllZ(item.getItemX(), item.getItemY(),
						item.getItemZ())) {
			if (p != null) {
				boolean distanceMatch = Misc.isInsideRenderedArea(p, item.getItemX(), item.getItemY());

				if (p.getHeightLevel() != item.getItemZ()) {
					if (distanceMatch) {
						p.itemsToRemove.add(item);
					}
					continue;
				}

				if (distanceMatch) {
					p.getItems().removeGroundItem(item.getItemId(), item.getItemX(), item.getItemY(), item.getItemAmount());
				}
			}
		}

		removeItem(item);
	}

	/**
	 * Removing the ground item
	 **/

	public static void removeGroundItem(Client c, int itemId, int itemX,
			int itemY, int itemZ, boolean add) {

		if (!BarbarianAssault.canPickupItem(c) && c.getBarbarianAssault().inArena()) {
			c.sendMessage("You need to be a collector to pickup this item.");
			return;
		}
		
		if (itemId == MagicalCrate.MAGICAL_CRATE_ITEM_ID && (c.inWild() || c.safeTimer > 0)) {
			if (c.playerEquipment[PlayerConstants.playerWeapon] > 1 || c.playerEquipment[PlayerConstants.playerShield] > 1) {
				c.sendMessage("You need to have both hands empty to carry this crate!");
				return;
			}
		}
		
		if (c.playerIsInHouse) {
			ConstructionUtils.pickupItem(c, itemId, itemX, itemY);
			return;
		}

		// for (GroundItem i : items) {
		for (GroundItem i : Server.getRegionManager().getGroundItemsInOneRegion(itemX, itemY, itemZ)) {
			if (i.getItemId() == itemId && i.getItemX() == itemX
					&& i.getItemY() == itemY
					&& i.getItemZ() == itemZ) {
				
				final boolean ownerOfItem = i.getOwnerName().toLowerCase().equals(c.getName().toLowerCase());
				
				if (!i.isDisplayForOthers() && !ownerOfItem) // filter nontradeable items
					continue;
				
				// if is ironman and not owner of item, then don't pick up
				if (!ownerOfItem && c.isIronMan() && !c.insideDungeoneering() && (!i.isInMinigame() || (i.isInMinigame() && i.isPlayerDropped())) && !i.isServerWideDrop()) {
					continue;
				}

				if (i.hideTicks > 0 && i.getOwnerName().toLowerCase()
						.equals(c.getName().toLowerCase())) {
					if (add) {
						if (!c.getPA().checkForMax(i, false)) {
							if (c.getItems().addItem(i.getItemId(),
									i.getItemAmount())) {
								c.getPacketSender().playSound(Sounds.PICKUP_ITEM);
								// String itemName = "nullItemId:"+itemId;
								// ItemDefinition item =
								// ItemDefinition.forId(itemId);
								// if (item != null)
								// itemName = item.getName();
								// TradeLog.pickedupItem(c, itemName,
								// i.getItemAmount(),
								// i.getOriginalOwnerName()); // logging
								// drops
								if (DBConfig.LOG_PICKUP) {
									if (!i.getOwnerName().equals("server"))
										Server.getLogWriter().addToPickedUpList(
												new PickedUpItem(c, i));
								}

								removeControllersItem(i, c, i.getItemId(),
										i.getItemX(), i.getItemY(),
										i.getItemAmount());
//								PresentSpawn.checkPresentRemove(i);
								break;
							}
						}
					} else {
						removeControllersItem(i, c, i.getItemId(), i.getItemX(),
								i.getItemY(), i.getItemAmount());
//						PresentSpawn.checkPresentRemove(i);
						break;
					}
				} else if (i.hideTicks <= 0) {
					if (add) {
						if (!c.getPA().checkForMax(i, false)) {
							if (c.getItems().addItem(i.getItemId(),
									i.getItemAmount())) {
								// String itemName = "nullItemId:"+itemId;
								// ItemDefinition item =
								// ItemDefinition.forId(itemId);
								// if (item != null)
								// itemName = item.getName();
								// TradeLog.pickedupItem(c, itemName,
								// i.getItemAmount(), i.getOriginalOwnerName());
								// // logging drops
								if (DBConfig.LOG_PICKUP) {
									if (!i.getOwnerName().equals("server"))
										Server.getLogWriter().addToPickedUpList(
												new PickedUpItem(c, i));
								}
								removeGlobalItem(i);
//								PresentSpawn.checkPresentRemove(i);
								break;
							}
						}
					} else {
						removeGlobalItem(i);
//						PresentSpawn.checkPresentRemove(i);
						break;
					}
				}
			}
		}
	}

	/**
	 * Removes item from list
	 **/
	public static void removeItem(GroundItem item) {
		Region newRegion = Server.getRegionManager()
				.getRegionByLocation(item.getItemX(), item.getItemY(), item.getItemZ());
		newRegion.removeGroundItem(item);

		// Server.getRegionManager().checkEmptyRegion(newRegion);

		item.setActive(false);
		items.remove(item);
	}

	/**
	 * restacks stackable ground item of item owner
	 *
	 * @param i
	 * @param c
	 * @param stackAmount
	 */
	public static void stackGroundItem(GroundItem i, Client c,
			int stackAmount) {
		if (c.isActive && c.getHeightLevel() == i.getItemZ()) {
			c.getItems().removeGroundItem(i.getItemId(), i.getItemX(),
					i.getItemY(), i.getItemAmount());
		}
		i.setItemAmount(i.getItemAmount() + stackAmount);
		if (c.isActive && c.getHeightLevel() == i.getItemZ()) {
			c.getItems().createGroundItemPacket(i.getItemId(), i.getItemX(),
					i.getItemY(), i.getItemAmount());
		}
	}
	
	public static boolean canPickupItem(Location itemLoc, Location playerLoc, boolean canMove, DynamicRegionClip dynClip) {
		if (itemLoc.getZ() == playerLoc.getZ() && Location.equals(itemLoc, playerLoc))
			return true;
		if (itemLoc.getX() != playerLoc.getX() && itemLoc.getY() != playerLoc.getY()) // diagonal check
			return false;
		if ((RegionClip.getClipping(itemLoc.getX(), itemLoc.getY(), itemLoc.getZ(), dynClip) & 0x100) != 0 || !canMove) { // if item is on top of an object
			if (!RegionClip.blockedByWall(itemLoc, Direction.getDirection(playerLoc, itemLoc), dynClip))
				return true;
		}
		return false;
	}
	
	private static void spawnGlobalItems() {
		createGroundItem(new GroundItem(1931, 3209, 3214, 0, 1, HIDE_TICKS), true); // empty pot in lumby castle
		createGroundItem(new GroundItem(1925, 3216, 9625, 0, 1, HIDE_TICKS), true); // empty bucket in lumbridge cellar
		createGroundItem(new GroundItem(1944, 3229, 3299, 0, 1, HIDE_TICKS), true); // egg at lumbridge farm by cows
		createGroundItem(new GroundItem(1735, 3192, 3272, 0, 1, HIDE_TICKS), true); // shears at lumbridge sheep farm
		
		createGroundItem(new GroundItem(1925, 2568, 3102, 0, 1, HIDE_TICKS), true); // empty bucket in yanille farming area
		
		createGroundItem(new GroundItem(1005, 3009, 3204, 0, 1, HIDE_TICKS), true); // white apron in port sarim
		createGroundItem(new GroundItem(1005, 3017, 3227, 0, 1, HIDE_TICKS), true); // white apron in port sarim
		
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3218, 3814, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons
		createGroundItem(new GroundItem(239, 3182, 3822, 0, 1, HIDE_TICKS), true); // whiteberries at lava dragons+
		createGroundItem(new GroundItem(239, 8602, 3149, 0, 1, HIDE_TICKS), true); // whiteberries at tyras camp(by zulrah tele)
	
		createGroundItem(new GroundItem(3138, 3512, 9501, 2, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3507, 9502, 2, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3515, 9496, 2, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3506, 9494, 2, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
	
		createGroundItem(new GroundItem(3138, 3466, 9494, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3468, 9497, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3469, 9492, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3470, 9490, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3471, 9501, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3469, 9496, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		createGroundItem(new GroundItem(3138, 3469, 9500, 0, 1, HIDE_TICKS), true); // potato cactus in kalphite lair
		
		createGroundItem(new GroundItem(231, 2553, 3751, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2553, 3754, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2551, 3757, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2541, 3765, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2546, 3761, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2514, 3766, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2508, 3763, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2503, 3758, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2504, 3752, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		createGroundItem(new GroundItem(231, 2509, 3753, 0, 1, HIDE_TICKS), true); // snape grass on waterbirth island
		
		createGroundItem(new GroundItem(1973, 3146, 3444, 0, 1, HIDE_TICKS), true); // chocolate bar spawning outside of waterbirth island
		createGroundItem(new GroundItem(1973, 3140, 3444, 0, 1, HIDE_TICKS), true); // chocolate bar spawning outside of waterbirth island
		createGroundItem(new GroundItem(1973, 3138, 3446, 0, 1, HIDE_TICKS), true); // chocolate bar spawning outside of waterbirth island
		createGroundItem(new GroundItem(1973, 3149, 3448, 0, 1, HIDE_TICKS), true); // chocolate bar spawning outside of waterbirth island
		createGroundItem(new GroundItem(1973, 3145, 3454, 0, 1, HIDE_TICKS), true); // chocolate bar spawning outside of waterbirth island
		
		createGroundItem(new GroundItem(2970, 3444, 3444, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3438, 3445, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3424, 3439, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3419, 3437, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3416, 3429, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3414, 3424, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3422, 3424, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3427, 3425, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3442, 3421, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3449, 3424, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3458, 3432, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		createGroundItem(new GroundItem(2970, 3449, 3433, 0, 1, HIDE_TICKS), true); // mort myre fungi spawning in morytania
		
		createGroundItem(new GroundItem(245, 2930, 3514, 0, 1, HIDE_TICKS), true); // wine of zamorak in chaos temple
		createGroundItem(new GroundItem(245, 2933, 3517, 0, 1, HIDE_TICKS), true); // wine of zamorak in chaos temple
		createGroundItem(new GroundItem(245, 2935, 3513, 0, 1, HIDE_TICKS), true); // wine of zamorak in chaos temple
		createGroundItem(new GroundItem(245, 2936, 3514, 0, 1, HIDE_TICKS), true); // wine of zamorak in chaos temple
		createGroundItem(new GroundItem(245, 2930, 3516, 0, 1, HIDE_TICKS), true); // wine of zamorak in chaos temple
		createGroundItem(new GroundItem(245, 2930, 3515, 0, 1, HIDE_TICKS), true); // wine of zamorak in chaos temple

		createGroundItem(new GroundItem(590, 2431, 3072, 0, 1, HIDE_TICKS), true); // tinderbox saradomin cw
		createGroundItem(new GroundItem(590, 2423, 3080, 1, 1, HIDE_TICKS), true); // tinderbox saradomin cw
		createGroundItem(new GroundItem(590, 2376, 3127, 1, 1, HIDE_TICKS), true); // tinderbox zamorak cw
		createGroundItem(new GroundItem(590, 2368, 3135, 0, 1, HIDE_TICKS), true); // tinderbox zamorak cw
		createGroundItem(new GroundItem(1925, 2428, 3080, 0, 1, HIDE_TICKS), true); // bucket saradomin cw
		createGroundItem(new GroundItem(1925, 2428, 3079, 0, 1, HIDE_TICKS), true); // bucket saradomin cw
		createGroundItem(new GroundItem(1925, 2371, 3127, 0, 1, HIDE_TICKS), true); // zamorak saradomin cw
		createGroundItem(new GroundItem(1925, 2371, 3128, 0, 1, HIDE_TICKS), true); // zamorak saradomin cw
	}
	
}
