package com.soulplay.game.world.packet;

import java.util.List;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;

public class GlobalPackets {
	
	public static void createProjectile(Projectile proj) {
		int startX = proj.getStartLoc().getX();
		int startY = proj.getStartLoc().getY();
		int speed = proj.getEndDelay();
		int offX = proj.getOffsetX();
		int offY = proj.getOffsetY();
		
		for (Player player : Server.getRegionManager()
				.getLocalPlayersByLocationExt(startX, startY, proj.getStartLoc().getZ())) {

			if (player.isBot()) {
				continue;
			}
			if (proj.getHomeOwnerId() > 0 && player.getHomeOwnerId() != proj.getHomeOwnerId()) {
				continue;
			}

			player.getPacketSender().createProjectile(startX, startY, offX, offY,
					speed, proj.getProjectileGFX(), proj.getStartHeight(), proj.getEndHeight(), proj.getLockon(),
					proj.getStartDelay(), proj.getSlope(), proj.getStartOffset());
		}

	}
	
	public static void createProjectileForPlayerOnly(Player player, Projectile proj) {
		int startX = proj.getStartLoc().getX();
		int startY = proj.getStartLoc().getY();
		int speed = proj.getEndDelay();
		int offX = proj.getOffsetX();
		int offY = proj.getOffsetY();


		if (player.isBot()) {
			return;
		}

		player.getPacketSender().createProjectile(startX, startY, offX, offY,
				speed, proj.getProjectileGFX(), proj.getStartHeight(), proj.getEndHeight(), proj.getLockon(),
				proj.getStartDelay(), proj.getSlope(), proj.getStartOffset());
	}
	
	public static void stillGfx(List<Player> list, Location loc, int gfxId, int gfxHeight, int ticks) {
		
		for (int i = 0, len = list.size(); i < len; i++) {
			Player p = list.get(i);
			
			if (p == null || !p.isActive || p.disconnected || p.isBot()) continue;
			
			p.getPacketSender().sendStillGraphics(gfxId, gfxHeight, loc.getY(), loc.getX(), ticks);
		}
	}
	
	public static void objectAnim(int x, int y, int animationID, int tileObjectType, int orientation, int z) {
		for (Player p : Server.getRegionManager().getLocalPlayersByLocationExt(x, y, z)) {
			if (p != null) {
				p.getPacketSender().createPlayersObjectAnim(x, y, animationID, tileObjectType, orientation);
			}
		}
	}
	
}
