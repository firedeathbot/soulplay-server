package com.soulplay.game.world.packet;

public class ConfigCodes {

	public static final int TEMP = 261;

	public static final int CHANGE_BUY_1 = 767;
	public static final int REQUEST_UUID = 768;
	public static final int CONSTRUCTION_MODE_1 = 770;
	public static final int CONSTRUCTION_MODE_2 = 771;
	public static final int CONSTRUCTION_MODE_3 = 772;
	
	public static final int CONS_LVL_REQ1 = 773;
	public static final int CONS_LVL_REQ2 = 774;
	public static final int CONS_LVL_REQ3 = 775;
	public static final int CONS_LVL_REQ4 = 776;
	public static final int CONS_LVL_REQ5 = 777;
	public static final int CONS_LVL_REQ6 = 778;
	public static final int CONS_LVL_REQ7 = 779;
	public static final int CONS_LVL_REQ8 = 780;

	public static final int INSTANCE_DEATH_SAFETY = 6557;
	public static final int INSTANCE_HEALTH_GENERATION = 6558;
	public static final int INSTANCE_PRAYER_DRAINING = 6559;

	public static final int CLEAR_INTERFACE = 798;
	public static final int OPEN_CHATBOX_INTERFACE = 799;
	public static final int DUNG_MAP_UPDATE = 8218;
	public static final int HAS_GATESTONE_DROPPED = 8225;
	public static final int AMOUNT_OF_PLAYERS = 8226;
	public static final int PLAYERS_STATES = 8227;
	public static final int DUNG_GUIDE_MODE = 8228;
	public static final int LMS_BOX = 8229;
	public static final int DUNG_PROGRESS_BAR = 8230;
	public static final int DAILY_TASK_TABS = 8231;
	public static final int PLACEHOLDERS = 8232;
	public static final int QUICK_PRAYERS_ON = 8233;
	public static final int TASKS_LOCKED = 8234;
	public static final int BOUNTY_TELEPORT_UNLOCKED = 8235;
	public static final int LAW_STAFF_CHARGE_COUNT = 8236;
	public static final int NATURE_STAFF_CHARGE_COUNT = 8237;
	public static final int GAMBLE_GAME_MODE = 8238;
	public static final int ITEM_RETRIEVAL_UNLOCKED = 8239;
	public static final int INF_RUNES = 8240;

}
