package com.soulplay.game.world.entity;

/**
 * Represents the different ways an object can be orientated.
 * 
 * @author Freyr
 */
public enum ObjectOrientation {
    
    NORTH(0),
    
    EAST(1),
    
    SOUTH(2),
    
    WEST(3);
    
    private final int value;
    
    private ObjectOrientation(int value) {
	this.value = value;
    }

    public int getValue() {
        return value;
    }
    
}
