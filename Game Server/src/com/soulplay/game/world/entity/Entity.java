package com.soulplay.game.world.entity;

public abstract class Entity {
	
	private int id;
	
	public boolean isActive;
	private boolean visible = true;
	
	public Entity(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public boolean isActive() {
		return this.isActive;
	}
	
	public void setIsActive(boolean active) {
		this.isActive = active;
	}

}
