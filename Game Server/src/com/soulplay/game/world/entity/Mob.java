package com.soulplay.game.world.entity;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Callable;

import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.items.itemdata.MeleeSwingStyle;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.minigames.gamble.GambleStage;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.DisplayTimerType;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.dot.DotManager;
import com.soulplay.game.world.map.InstanceManager;
import com.soulplay.util.Misc;

public abstract class Mob extends Entity {

	private DotManager dotManager = new DotManager(this);
	private TickTimer venomImmunity = new TickTimer();
	private TickTimer poisonImmune = new TickTimer();	

	public void pulse(Runnable runnable) {
		pulse(runnable, 1);
	}

	public void pulse(Runnable runnable, int ticks) {
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				runnable.run();
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, ticks);
	}

	private InstanceManager instanceManager;
	private final AttributeMap attributes = new AttributeMap();

	public int absX;

	public int absY;

	public int heightLevel;

	private int size = 1; // default size is one, boundDimension

	private final Location createdLocation;

	private Location currentLocation;
	private Location centerLocation;

	private Location lastLocation = Location.create(0, 0, 0); //WalkingQueue has var called footprint which is the same thing as this, but we don't use WalkingQueue on player walk yet

	protected final UpdateFlags updateFlags;

	protected final Queue<Animation> animations = new PriorityQueue<>();

	protected final Queue<Graphic> graphics = new PriorityQueue<>();
	
	private final WalkingQueue walkingQueue = new WalkingQueue(this);

	private DynamicRegionClip dynamicRegionClip;

	public DynamicRegionClip getDynamicRegionClip() {
		return dynamicRegionClip;
	}

	public void setDynamicRegionClip(DynamicRegionClip map) {
		this.dynamicRegionClip = map;
	}

	public boolean noClip = false;
	
	private double setMultiply = 1.0;

	public double getSetMultiply() {
		return setMultiply;
	}

	public void setSetMultiply(double setMultiply) {
		this.setMultiply = setMultiply;
	}

	private int[] playerBonus = new int[EquipmentBonuses.values.length];

	public int[] getPlayerBonus() {
		return playerBonus;
	}
	
	public void setPlayerBonus(int[] playerBonus) {
		this.playerBonus = playerBonus;
	}

	public void setWeaponAttackType(WeaponAttackTypes attackType) {
		this.weaponAttackType = attackType;
	}
	
	public WeaponAttackTypes getWeaponAttackType() {
		return weaponAttackType;
	}
	
	/**
	 * attackType: 0-Accurate, 1-Aggressive, 2-Controlled, 3-Defensive
	 */
	private WeaponAttackTypes weaponAttackType = WeaponAttackTypes.ACCURATE;
	
	/**
	 * Fight Mode: The order of the boxes (first is 0, second 1, third 2, fourth
	 * 3) Fight Style: 0 = stab, 1 = slash, 2 = crush Fight Xp: 0 = attack, 1 =
	 * str, 2 = def, 3 = att/def/str, 4 = range, 5 = ran/def
	 */
	private MeleeSwingStyle meleeSwingStyle = MeleeSwingStyle.STAB;

	public void setMeleeSwingStyle(MeleeSwingStyle fightStyle) {
		this.meleeSwingStyle = fightStyle;
	}

	public MeleeSwingStyle getMeleeSwingStyle() {
		return meleeSwingStyle;
	}
	
	private boolean immuneToPoison = false;
	
	public void setPoisonImmunity(boolean immune) {
		this.immuneToPoison = immune;
	}
	
	public boolean isPoisonImmune() {
		return this.immuneToPoison;
	}
	
	private boolean immuneToVenom = false;
	
	public void setVenomImmunity(boolean immune) {
		this.immuneToVenom = immune;
	}
	
	public boolean isVenomImmune() {
		return this.immuneToVenom;
	}

	protected Hit hitMask = new Hit(0, 0, 0);		
	protected Hit hitMask2 = new Hit(0, 0, 0);	
	
	protected int facing, lastFacing;

	public boolean dontFace = false;
	protected Location lastFacingLocation;
	protected Location facingLocation;
	
	private Location followLocation = Location.create(-1, -1);

	protected String forcedChat;

	protected byte poisonMask = 0;

	protected int pauseAnimReset;
	
	private int headIcon = -1;
	
	private int freezeTimer = -6;
	
	private boolean isStunned;

	private boolean isShoved;
	
	private boolean isDead = false;
	
	private CycleEventContainer miasmicEffect;

	public void startMiasmicEffect(int seconds) {
		if (miasmicEffect != null) {
			return;
		}

		int ticks = Misc.secondsToTicks(seconds);
		miasmicEffect = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				miasmicEffect = null;
				if (isPlayer()) {
					toPlayerFast().sendMessage("The miasmic spell effect has ran out.");
				}
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, ticks);

		if (isPlayer()) {
			toPlayerFast().sendMessage("You've been affected by miasmic spell.");
		}
	}

	public boolean miasmicEffectActive() {
		return miasmicEffect != null;
	}

	public void resetMiasmicEffect() {
		miasmicEffect = null;
	}

	private int attackTimer = 0;
	
	private int combatDistance = 1;
	
	private int homeOwnerId = -1;
	
	private final TickTimer stopWatch = new TickTimer();

	private final Skills skills = new Skills(this);
	
	private Skills lmsSkills = null;

	public int startFill;
	public int stayCycleAfterFill;
	public int fill;
	public int maxFill;
	public boolean reverse;
	public int fillColor1;
	public int fillColor2;

	public int x1 = -1;

	public int y1 = -1;
	
	public int movedX = -1, movedY = -1;

	public int x2 = -1;

	public int y2 = -1;

	public int speed1 = -1;

	public int speed2 = -1;

	public int direction = -1;
	
	private TickTimer lockActions = new TickTimer();
	
	private TickTimer lockMovement = new TickTimer();
	
	public Mob(int id, Location location, boolean player) {
		super(id);
		this.absX = location.getX();
		this.absY = location.getY();
		this.heightLevel = location.getZ();
		this.createdLocation = location.copyNew();
		this.currentLocation = location.copyNew();
		this.walkingQueue.reset();
		this.updateFlags = new UpdateFlags(player);
	}

	public int getLocalX() {
		return absX - 8 * getMapRegionX();
	}

	public int getLocalX(int x) {
		return x - 8 * getMapRegionX();
	}

	public int getLocalY() {
		return absY - 8 * getMapRegionY();
	}

	public int getLocalY(int y) {
		return y - 8 * getMapRegionY();
	}

	public int getMapRegionX() {
		return (absX >> 3) - 6;
	}

	public int getMapRegionY() {
		return (absY >> 3) - 6;
	}

	public void process() {
		dotManager.process();
	}

	public abstract void update();

	public void onInit() {

	}

	public abstract boolean isNpc();

	public NPC toNpc() {
		if (this instanceof NPC) {
			return toNpcFast();
		}

		throw new RuntimeException("This mob is not an npc.");
	}

	public NPC toNpcFast() {
		return (NPC) this;
	}

	public abstract boolean isPlayer();

	public Player toPlayer() {
		if (this instanceof Player) {
			return toPlayerFast();
		}

		throw new RuntimeException("This mob is not a player.");
	}

	public Player toPlayerFast() {
		return (Player) this;
	}

	public void face(Mob mob) {
		if (mob == null) {
			resetFace();
			return;
		}

		int id = mob.getId();

		lastFacing = facing = mob.isPlayer() ? (id + (Short.MAX_VALUE + 1)) : id;
		updateFlags.add(UpdateFlag.ENTITY_INTERACTION);
	}
	
	public void setLastFace() {
		facing = lastFacing;
		updateFlags.add(UpdateFlag.ENTITY_INTERACTION);
	}

	public void forceChat(String text) {
		if (text.isEmpty()) {
			return;
		}
		
		forcedChat = text.trim();
		updateFlags.add(UpdateFlag.FORCED_CHAT);
	}

	public void startGraphic(Graphic graphic) {
		graphics.add(graphic);
		updateFlags.add(UpdateFlag.GRAPHICS);
	}

	public void resetGraphic() {
		graphics.clear();
		updateFlags.add(UpdateFlag.GRAPHICS);
	}

	public void startAnimation(int animId) {
		startAnimation(new Animation(animId));
	}
	
	public void startAnimation(int animId, int delay) {
		startAnimation(new Animation(animId, delay));
	}
	
	public void startAnimation(Animation anim) {
		if (getPauseAnimReset() > 0) {
			return;
		}
		animations.add(anim);
		updateFlags.add(UpdateFlag.ANIMATION);
	}

	public void startAnimation(Animation animationId, int cycles, Callable<Boolean> callable) {

		this.startAnimation(animationId);

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			private int tick = 0;

			@Override
			public void execute(CycleEventContainer container) {

				if (tick == cycles) {
					container.stop();
				}

				tick++;
			}

			@Override
			public void stop() {
				try {
					callable.call();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}, 1);
	}

	public void setBarFill(int startFill, int stayCyclesAfterFill, int fill, int maxFill, boolean reverse, int fillColor1, int fillColor2) {
		this.startFill = startFill;
		this.stayCycleAfterFill = stayCyclesAfterFill;
		this.fill = fill;
		this.maxFill = maxFill;
		this.reverse = reverse;
		this.fillColor1 = fillColor1;
		this.fillColor2 = fillColor2;
		updateFlags.add(UpdateFlag.BAR_FILL);
	}

	public void setHeadIcon(int id) {
		if (id < -1) {
			id = -1;
		}
		
		this.headIcon = id;
		updateFlags.add(UpdateFlag.APPEARANCE);
	}
	
	public int getHeadIcon() {
		return headIcon;
	}

	public int getPauseAnimReset() {
		return pauseAnimReset;
	}

	public void setPauseAnimReset(int pauseAnimReset) {
		this.pauseAnimReset = pauseAnimReset;
	}

	public void resetFace() {
		lastFacing = facing = -1;
		updateFlags.add(UpdateFlag.ENTITY_INTERACTION);
	}

	public void resetAnimations() {	
		animations.clear();
		updateFlags.add(UpdateFlag.ANIMATION);
	}

	public void faceLocation(int pointX, int pointY) {
		faceLocation(Location.create(pointX, pointY), true);
	}
	
	public void faceLocation(Location loc) {
		faceLocation(loc, true);
	}

	public void faceDirection(Direction direction) {
		faceLocation(getCurrentLocation().transform(direction == null ? Direction.NORTH : direction, getSize()));
	}

	public void faceLocation(Location loc, boolean changeLastFace) {
		if (dontFace) {
			return;
		}

		if (changeLastFace)
			lastFacingLocation = loc.copyNew();
		facingLocation = loc.copyNew();
		updateFlags.add(UpdateFlag.FACE_COORDINATE);
	}

	public abstract void clearUpdateFlags();

	public Location getCreatedLocation() {
		return createdLocation;
	}

	public UpdateFlags getUpdateFlags() {
		return updateFlags;
	}

	public boolean isUpdateRequired() {
		return !updateFlags.isEmpty();
	}

	public Animation getAnimation() {
		Animation peek = animations.peek();
		return peek == null ? RESET_ANIM : peek;
	}

	public static final Animation RESET_ANIM = new Animation(65535);
	public static final Graphic RESET_GRAPHIC = Graphic.create(-1);

	public Graphic getGraphic() {
		Graphic peek = graphics.peek();
		return peek == null ? RESET_GRAPHIC : peek;
	}

	public int getFacing() {
		return facing;
	}
	
	public int getIdforFaceId() {
		if (isPlayer())
			return getId() + (Short.MAX_VALUE + 1);
		return getId();
	}
	
	public int getLastFacing() {
		return lastFacing;
	}

	public Location getFacingLocation() {
		return facingLocation;
	}
	
	public Location getLastFacingLocation() {
		return lastFacingLocation;
	}
	
	public String getForcedChat() {
		return forcedChat;
	}

	public Hit getHitMask() {
		return hitMask;
	}
	
	public Hit getHitMask2() {
		return hitMask2;
	}
	
	public abstract boolean usingProtectPrayer(boolean usingCurses, int prayerType);
	
	public abstract int protectPrayerDamageReduction(int currentHit);
	
	public abstract void shove(int stunTick, Location loc);
	
	public boolean isShoved() {
		return isShoved;
	}

	public void setShoved(boolean isShoved) {
		this.isShoved = isShoved;
	}
	
	public int getFreezeTimer() {
		return freezeTimer;
	}

	public void setFreezeTimer(int freezeTimer) {
		this.freezeTimer = freezeTimer;
	}

	public Location getCenterLocation() {
		return centerLocation;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation.copyLocation(currentLocation);
		updateCenterLocation();
	}

	public void updateCenterLocation() {
		int halfSize = getSize() >> 1;
		this.centerLocation = this.currentLocation.transform(halfSize, halfSize, 0);
	}

	public int dealDamage(Hit hit) {
		return 0;
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}
	
	public Skills getRealSkills() {
		return skills;
	}
	
	public Skills getSkills() {
		return lmsSkills != null ? lmsSkills : skills;
	}

	public int getAttackTimer() {
		return attackTimer;
	}
	
	public boolean isStartBlockAnim() {
		return attackTimer == 3 || attackTimer == 0;
	}

	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}
	
	public abstract int getProjectileLockon();
	
	public abstract boolean inMulti();
	
	public abstract void stunEntity(int timer, Graphic gfx);
	
	public TickTimer stunTimer = new TickTimer();

	public boolean isStunned() {
		return isStunned || !stunTimer.complete();
	}

	public void setStunned(boolean isStunned) {
		this.isStunned = isStunned;
	}
	
	public abstract void rootEntity(int timer);

	public int getCombatDistance() {
		return combatDistance;
	}

	public void setCombatDistance(int combatDistance) {
		this.combatDistance = combatDistance;
	}

	public int getHomeOwnerId() {
		return homeOwnerId;
	}
	
	public void setHomeOwnerId(int owner) {
		this.homeOwnerId = owner;
	}
	
	public boolean applyFreeze(int ticks) {
		if (getFreezeTimer() <= RSConstants.FREEZE_COOLDOWN) { //Maybe needs to be <= -3
			setFreezeTimer(ticks);

			if (isPlayer()) {
				toPlayerFast().getPacketSender().sendWidget(DisplayTimerType.ICE_BARRAGE, ticks);
			} else if (isNpc()) {
				toNpcFast().lockMovement(ticks);
				toNpcFast().resetWalkTemp();
			}
			return true;
		}
		return false;
	}
	
	public void resetFreeze() {
		setFreezeTimer(RSConstants.FREEZE_COOLDOWN);
		if (isPlayer()) {
			toPlayerFast().getPacketSender().sendWidget(DisplayTimerType.ICE_BARRAGE, 0);
		}
	}

	public TickTimer getStopWatch() {
		return stopWatch;
	}

	public WalkingQueue getWalkingQueue() {
		return walkingQueue;
	}

	public Location getFollowLocation() {
		return followLocation;
	}

	public void setFollowLocation(Location followLocation) {
		this.followLocation = followLocation;
	}

	public Location getLastLocation() {
		return lastLocation;
	}

	public void setLastLocation(Location lastLocation) {
		this.lastLocation = lastLocation;
	}

	public AttributeMap attr() {
		return attributes;
	}

	public void setForceMovement(final ForceMovementMask mask) {
		if (isPlayer()) {
			((Client) this).performingAction = true;
		}

		final int twoTicks = mask.getDir1Ticks() + mask.getDir2Ticks();

		if (mask.getLockActionsTick() > 0)
			lockActions.startTimer(mask.getLockActionsTick());
		else
			lockActions.startTimer(twoTicks);
		
		this.x1 = mask.getDir1X();
		this.y1 = mask.getDir1Y();
		this.x2 = this.x1 + mask.getDir2X();
		this.y2 = this.y1 + mask.getDir2Y();
		if (mask.getCustomSpeed1() > 0 || mask.getCustomSpeed2() > 0) {
			this.speed1 = mask.getCustomSpeed1();
			this.speed2 = mask.getCustomSpeed2() == 0 ? 0 : mask.getCustomSpeed2() + speed1;
		} else {
			this.speed1 = Misc.serverToClientTick(mask.getDir1Ticks());
			this.speed2 = mask.getDir2Ticks() == 0 ? 0 : (Misc.serverToClientTick(mask.getDir2Ticks()) + speed1);
		}
		this.direction = mask.getFaceDir();
		if (mask.getMovedX() > 0) {
			this.movedX = mask.getMovedX();
			this.movedY = mask.getMovedY();
		}
		if (mask.getStartAnim() > 0)
			this.startAnimation(mask.getStartAnim());
		final Location finalDest = mask.getTeleLoc() != null ? mask.getTeleLoc() : Location.create(absX+mask.getDir1X()+mask.getDir2X(), absY+mask.getDir1Y()+mask.getDir2Y(), heightLevel);
//		final short[] backupAnims = getMovement().copyMovementAnims();
//		for (int kk = 0; kk < getMovement().getPlayerMovementAnimIds().length; kk++) {
//			getMovement().getPlayerMovementAnimIds()[kk] = (short)mask.getSecondMovementAnim();
//		}
		
		final boolean changeStance = mask.getWalkStanceId() > 0 && isPlayer();
		final int stanceId = changeStance ? toPlayer().getMovement().getStandAnim() : 0;
		final int walkId = changeStance ? toPlayer().getMovement().getWalkAnim() : 0;
		if (changeStance) {
			toPlayer().getMovement().changeStance(mask.getWalkStanceId());
			toPlayer().getMovement().changeWalk(mask.getWalkStanceId());
		}
		
		Mob mob = this;
		updateFlags.add(UpdateFlag.FORCE_MOVEMENT);
		CycleEventHandler.getSingleton().addEvent(mob, new CycleEvent() {
			int tick = 0;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (tick < mask.getDir1Ticks()-2) {
					if (mask.getFirstMovementAnim() > 0)
						startAnimation(mask.getFirstMovementAnim());
				} else if (tick < mask.getDir2Ticks()) {
					if (mask.getSecondMovementAnim() > 0) {
						startAnimation(mask.getSecondMovementAnim());
					}
				}
				if (tick == mask.getDir1Ticks()+mask.getDir2Ticks() - 2) {
					if (mask.getEndAnim() > 0) {
						startAnimation(mask.getEndAnim());
					}
//					getMovement().movementAnimations = Arrays.copyOf(backupAnims, backupAnims.length);
//					updateFlags.add(UpdateFlag.APPEARANCE);
				}
				if (tick >= mask.getDir1Ticks()+mask.getDir2Ticks())
					container.stop();
				tick++;
			}
		}, 1);
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer e) {
				e.stop();
				if (mask.getEndAnim() > 0) {
					startAnimation(mask.getEndAnim());
				}
				
				if (changeStance) {
					toPlayer().getMovement().changeStance(stanceId);
					toPlayer().getMovement().changeWalk(walkId);
				}

				if (isPlayer()) {
					Player player = toPlayerFast();
					if (player.gambleStage == GambleStage.NONE) {
						player.performingAction = false; //TODO: rewrite to use something else like  isLockActions() in player packets and following/attacking
					}
				}

				if (mask.isTelePlayerAtEnd()) {
					if (isPlayer()) {
						((Client) mob).getPA().movePlayerInstant(finalDest.getX(), finalDest.getY(), finalDest.getZ());
					} else if (isNpc()) {
						toNpc().moveNpc(finalDest);
					}
				}
			}

			@Override
			public void stop() {
				startAnimation(RESET_ANIM);
			}
		}, mask.getCustomMovePlayerTick() > 0 ? mask.getCustomMovePlayerTick() : twoTicks);
	}
	
	public void setForceMovementRaw(final ForceMovementMask mask) {
		updateFlags.add(UpdateFlag.FORCE_MOVEMENT);
		this.x1 = mask.getDir1X();
		this.y1 = mask.getDir1Y();
		this.x2 = this.x1 + mask.getDir2X();
		this.y2 = this.y1 + mask.getDir2Y();
		if (mask.getCustomSpeed1() > 0 || mask.getCustomSpeed2() > 0) {
			this.speed1 = mask.getCustomSpeed1();
			this.speed2 = mask.getCustomSpeed2(); // mask.getCustomSpeed2() == 0 ? 0 : mask.getCustomSpeed2() + speed1;
		} else {
			this.speed1 = Misc.serverToClientTick(mask.getDir1Ticks());
			this.speed2 = mask.getDir2Ticks() == 0 ? 0 : (Misc.serverToClientTick(mask.getDir2Ticks()) + speed1);
		}
		this.direction = mask.getFaceDir();
		if (mask.getMovedX() > 0) {
			this.movedX = mask.getMovedX();
			this.movedY = mask.getMovedY();
		}
	}

	public boolean isLockActions() {
		return !lockActions.complete();
	}

	public TickTimer lockActions() {
		return this.lockActions;
	}
	
	public void lockActions(int ticks) {
		this.lockActions.startTimer(ticks);
	}

	public Skills getSpawnedSkills() {
		return lmsSkills;
	}

	public void setSpawnedSkills(Skills lmsSkills) {
		this.lmsSkills = lmsSkills;
	}

	public TickTimer getLockMovement() {
		return lockMovement;
	}

	public void lockMovement(int ticks) {
		this.lockMovement.startTimer(ticks);
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public int getSize() {
		return this.size;
	}
	
	public boolean consideredForHit(/*@Nullable */Mob target) {//TODO: add package for annotations i think
		return CombatFormulas.consideredForHit(this)
				&& (target == null || (CombatFormulas.consideredForHit(target) && getCurrentLocation().isWithinDistance(target.getCurrentLocation(), 64)));
	}

	public boolean withinDistance(int otherX, int otherY, int otherZ, int dist) {
		if (heightLevel != otherZ) {
			return false;
		}
		int deltaX = otherX - absX, deltaY = otherY - absY;
		return deltaX <= dist && deltaX >= -dist && deltaY <= dist && deltaY >= -dist;
	}

	public void setInstanceManager(InstanceManager instanceManager) {
		this.instanceManager = instanceManager;
	}

	public InstanceManager getInstanceManager() {
		return instanceManager;
	}

	public boolean insideInstance() {
		return instanceManager != null;
	}

	public Location getLocationInstance() {
		if (!insideInstance()) {
			return currentLocation;
		}

		Location base = getInstanceManager().base();
		return currentLocation.subtract(base);
	}

	public DotManager getDotManager() {
		return dotManager;
	}

	public TickTimer getPoisonImmuneTimer() {
		return poisonImmune;
	}

	public TickTimer getVenomImmunityTimer() {
		return venomImmunity;
	}

}
