package com.soulplay.game.world.entity;

public class UpdateFlags {

	private int bitPack;
	private boolean player;

	public UpdateFlags(boolean player) {
		this.player = player;
	}

	public void add(UpdateFlag updateFlag) {
		int mask = updateFlag.getMask(player);
		if (mask == -1) {
			System.out.println("Not handled mask for " + (player ? "player" : "npc") + "=" + updateFlag); 
			Thread.dumpStack();
			return;
		}

		bitPack |= mask;
	}

	public void remove(UpdateFlag updateFlag) {
		int mask = updateFlag.getMask(player);
		if (mask == -1) {
			System.out.println("Not handled mask for " + (player ? "player" : "npc") + "=" + updateFlag); 
			Thread.dumpStack();
			return;
		}

		bitPack &= ~mask;
	}

	public boolean contains(UpdateFlag updateFlag) {
		int mask = updateFlag.getMask(player);
		if (mask == -1) {
			return false;
		}

		return (bitPack & mask) != 0;
	}

	public void clear() {
		bitPack = 0;
	}

	public boolean isEmpty() {
		return bitPack == 0;
	}

	public int get() {
		return bitPack;
	}
}
