package com.soulplay.game.world.entity.dot;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

public class VenomDot extends DotType {

	public static final int TICKS = 30;

	public VenomDot(Mob mob, int damage) {
		super(mob, "Venom", damage, null);
	}

	@Override
	public void add() {
		super.add();

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			player.sendMessage("You have been envenomed!");
			player.getPacketSender().sendHpOrbState();
		}
	}

	@Override
	public boolean process() {
		if (checkImmunity()) {
			return true;
		}

		int ticks = getTicks();
		if (ticks % TICKS == 0) {
			int damage = getDamage();
			mob.dealDamage(new Hit(damage, 2, -1));
			if (damage < 20) {
				setDamage(damage + 2);
			}
		}

		return false;
	}

	@Override
	public boolean checkImmunity() {
		return mob.isVenomImmune() || !mob.getVenomImmunityTimer().complete();
	}

	@Override
	public void setDamage(int damage) {
		super.setDamage(damage);

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			player.setVenomDamage(damage);
		}
	}

	@Override
	public void remove() {
		super.remove();

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			player.getPacketSender().sendHpOrbState();
		}
	}

}
