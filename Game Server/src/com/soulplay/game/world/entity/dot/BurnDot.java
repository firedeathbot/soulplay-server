package com.soulplay.game.world.entity.dot;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.world.entity.Mob;

public class BurnDot extends DotType {

	private int burns;

	public BurnDot(Mob mob, int damage) {
		super(mob, "Burn", damage, null);
	}

	@Override
	public boolean checkImmunity() {
		return false;
	}

	@Override
	public boolean process() {
		int ticks = getTicks();
		if (ticks % 3 == 0) {
			mob.dealDamage(new Hit(1));
			burns++;
			if (burns >= 5) {
				return true;
			}
		}

		return false;
	}

}
