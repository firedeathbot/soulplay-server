package com.soulplay.game.world.entity.dot;

import com.soulplay.game.world.entity.Mob;

public abstract class DotType {

	public final Mob mob;
	private final String name;
	private int damage;
	private int ticks = -1;
	public final Mob inflicter;

	public DotType(Mob mob, String name, int damage, Mob inflicter) {
		this.mob = mob;
		this.name = name;
		this.setDamage(damage);
		this.inflicter = inflicter;
	}

	public Mob getMob() {
		return mob;
	}

	public String getName() {
		return name;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void preProcess() {
		ticks++;
	}

	public int getTicks() {
		return ticks;
	}

	public void remove() {
		/* empty */
	}

	public void add() {
		/* empty */
	}

	public abstract boolean checkImmunity();

	public abstract boolean process();

}
