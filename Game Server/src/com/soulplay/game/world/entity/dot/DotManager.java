package com.soulplay.game.world.entity.dot;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

public class DotManager {

	private static final int LENGTH = 3;
	private final Mob mob;
	private DotType[] dots = new DotType[LENGTH];

	public DotManager(Mob mob) {
		this.mob = mob;
	}

	public void applyPoison(int damage) {
		applyPoison(damage, null);
	}

	public void applyPoison(int damage, Mob inflicter) {
		DotType poison = new PoisonDot(mob, damage, inflicter);
		if (poison.checkImmunity()) {
			return;
		}

		dots[0] = poison;
		poison.add();
	}

	public void applyVenom() {
		applyVenom(6);
	}

	public void applyVenom(int damage) {
		VenomDot venom = new VenomDot(mob, damage);
		if (venom.checkImmunity()) {
			return;
		}

		dots[1] = venom;
		venom.add();
	}

	public void applyBurn(int damage) {
		BurnDot burn = new BurnDot(mob, damage);
		if (burn.checkImmunity()) {
			return;
		}

		dots[2] = burn;
		burn.add();
	}

	public boolean isActive(int index) {
		return dots[index] != null;
	}

	public boolean isPoisonActive() {
		return isActive(0);
	}

	public boolean isVenomActive() {
		return isActive(1);
	}

	public void clear(int index) {
		boolean removed = false;

		if (dots[index] != null) {
			dots[index].remove();
			dots[index] = null;
			removed = true;
		}

		if (removed && mob.isPlayer()) {
			mob.toPlayerFast().getPacketSender().sendHpOrbState();
		}
	}

	public void clearPoison() {
		clear(0);
	}

	public void clearVenom() {
		clear(1);
	}

	public void process() {
		if (!mob.isActive() || mob.isDead() || mob.getSkills().getLifepoints() <= 0) {
			return;
		}

		for (int i = 0; i < LENGTH; i++) {
			DotType dot = dots[i];
			if (dot == null) {
				continue;
			}

			dot.preProcess();
			if (dot.process()) {
				dots[i] = null;
				dot.remove();
			}
		}
	}

	public void reset() {
		for (int i = 0; i < LENGTH; i++) {
			DotType dot = dots[i];
			if (dot == null) {
				continue;
			}

			dots[i] = null;
			dot.remove();
		}
	}

	public void checkLogin(Player player) {
		int poisonDamage = player.getPoisonDamage();
		if (poisonDamage > 0) {
			applyPoison(poisonDamage);
		}

		int venomDamage = player.getVenomDamage();
		if (venomDamage > 0) {
			applyVenom(venomDamage);
		}

		// Skip all dots tick, incase we saved it and so it doesn't tick constantly on relogging
		for (int i = 0; i < LENGTH; i++) {
			DotType dot = dots[i];
			if (dot == null) {
				continue;
			}

			dot.preProcess();
		}
	}

	public DotType get(int index) {
		return dots[index];
	}

	public PoisonDot getPoison() {
		return (PoisonDot) dots[0];
	}

	public VenomDot getVenom() {
		return (VenomDot) dots[1];
	}

	public void curePoison() {
		curePoison(0);
	}

	public void curePoison(int immunityTicks) {
		clearPoison();

		if (immunityTicks > 0) {
			mob.getPoisonImmuneTimer().startTimer(immunityTicks);
		}
	}

	public void cureVenom(int immunityTicks) {
		clearVenom();

		if (immunityTicks > 0) {
			mob.getVenomImmunityTimer().startTimer(immunityTicks);
		}
	}

	public boolean convertVenomToPoison() {
		if (!isVenomActive()) {
			return false;
		}

		int venomDamage = getVenom().getDamage();
		clearVenom();
		applyPoison(venomDamage);
		return true;
	}

	public void consumeAntiPoison(int immunityTicks) {
		if (convertVenomToPoison()) {
			return;
		}

		curePoison(immunityTicks);
	}

}
