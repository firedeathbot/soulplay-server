package com.soulplay.game.world.entity.dot;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public class PoisonDot extends DotType {

	public static final int TICKS = 30;
	private int hits;
	private int hitsAmount = 5;

	public PoisonDot(Mob mob, int damage, Mob inflicter) {
		super(mob, "Poison", damage, inflicter);
	}

	@Override
	public void add() {
		super.add();

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			player.sendMessage("You have been poisoned.");
			player.getPacketSender().sendHpOrbState();
		}

		if (inflicter != null && inflicter.isPlayer()) {
			Player player = inflicter.toPlayerFast();
			int poisonAmt = player.getSeasonalData().getPowerUpAmount(PowerUpData.POISON);
			if (poisonAmt > 0) {
				setDamage(getDamage() + poisonAmt);
			}

			int poisonLastAmt = player.getSeasonalData().getPowerUpAmount(PowerUpData.POISON_LASTING);
			if (poisonLastAmt > 0) {
				hitsAmount += poisonLastAmt;
			}
		}
	}

	@Override
	public boolean process() {
		if (checkImmunity()) {
			return true;
		}

		int ticks = getTicks();
		if (ticks % TICKS == 0) {
			int damage = getDamage();
			if (inflicter != null && inflicter.isPlayer() && inflicter.toPlayerFast().getSeasonalData().containsPowerUp(PowerUpData.POISON_CRIT) && Misc.randomBoolean()) {
				damage *= 2;
			}

			mob.dealDamage(new Hit(damage, 2, -1));
			if (mob.isPlayer()) {
				mob.toPlayerFast().getPA().closeAllWindows();
			}

			hits++;
			if (hits >= hitsAmount) {
				hits = 0;
				setDamage(damage - 1);
				return getDamage() <= 0;
			}
		}

		return false;
	}

	@Override
	public boolean checkImmunity() {
		return mob.isPoisonImmune() || !mob.getPoisonImmuneTimer().complete();
	}

	@Override
	public void setDamage(int damage) {
		super.setDamage(damage);

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			player.setPoisonDamage(damage);
		}
	}

	@Override
	public void remove() {
		super.remove();

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			player.getPacketSender().sendHpOrbState();
		}
	}

}
