package com.soulplay.game.world.entity;

import com.soulplay.Config;

public class Animation implements Comparable<Animation> {

	public static final Animation FUNNY_DEATH1 = new Animation(767);
	public static final Animation FUNNY_DEATH2 = new Animation(837);
	public static final Animation PICK_UP_OSRS = new Animation(Animation.getOsrsAnimId(827));
	public static final Animation PICK_UP = new Animation(827);
	public static final Animation TABLE_PICK_UP = new Animation(833);
	public static final Animation LEVER_CONTROL = new Animation(834);
	public static final Animation LEVER_CONTROL_DELAYED = new Animation(834, 20);
	public static final Animation CLIMB_UP = new Animation(828);
	public static final Animation CLIMB_UP_DELAYED = new Animation(828, 20);
	public static final Animation DEATH_REGULAR = new Animation(2304);
	public static final Animation EFFIGY_OPEN_FAIL = new Animation(4067);
	public static final Animation EFFIGY_OPEN = new Animation(4068);
	public static final Animation EFFIGY_OPEN_FINAL = new Animation(14177);
	public static final Animation DEATH_KNIFE = new Animation(14590);
	public static final Animation DAB = new Animation(726);
	
	public static final Animation RESET = new Animation(65535);

	private final int id;

	private final int delay;

	private final Priority priority;

	public Animation(int id) {
		this(Priority.NORMAL, id, 0);
	}

	public Animation(int id, int delay) {
		this(Priority.NORMAL, id, delay);
	}

	public Animation(Priority priority, int id) {
		this(priority, id, 0);
	}

	public Animation(Priority priority, int id, int delay) {
		this.priority = priority;
		this.id = id;
		this.delay = delay;
	}

	public int getId() {
		return id;
	}

	public int getDelay() {
		return delay;
	}

	public Priority getPriority() {
		return priority;
	}
	
	public static Animation create(int id) {
		return new Animation(id);
	}

	public static Animation osrs(int id) {
		return new Animation(getOsrsAnimId(id));
	}

	@Override
	public int compareTo(Animation other) {
		if (other == null) {
			return 1;
		}

		return Integer.signum(other.priority.getPriority() - priority.getPriority());
	}

	@Override
	public String toString() {
		return String.format("ANIMATION[priority=%s, id=%s, delay=%s]", priority, id, delay);
	}

	public static int getOsrsAnimId(int anim) {
		return Config.OSRS_ANIM_OFFSET + anim;
	}

}
