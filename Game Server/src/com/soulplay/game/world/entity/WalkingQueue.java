package com.soulplay.game.world.entity;

import java.util.ArrayDeque;
import java.util.Deque;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.map.Point;

public final class WalkingQueue {

	/**
	 * The walking queue.
	 */
	private final Deque<Point> walkingQueue = new ArrayDeque<Point>(100);

	/**
	 * The entity.
	 */
	private final Mob entity;

	/**
	 * The current walking direction.
	 */
	private int walkDir = -1;

	/**
	 * The current running direction.
	 */
	private int runDir = -1;

	/**
	 * If the entity is running (set to true when holding the ctrl button +
	 * click).
	 */
	private boolean running = false;

	/**
	 * If running is disabled.
	 */
	private boolean runDisabled;

	/**
	 * The last location this entity walked on.
	 */
	private final Location footPrint = Location.create(0, 0, 0);
	
	private final Location walkTo = Location.create(0, 0, 0);

	/**
	 * Constructs a new {@code WalkingQueue} {@code Object}.
	 *
	 * @param entity
	 *            The entity.
	 */
	public WalkingQueue(Mob entity) {
		this.entity = entity;
//		this.footPrint = entity.getCreatedLocation().copyNew();
	}

	/**
	 * Gets the energy drain rate for this player.
	 *
	 * @param player
	 *            The player.
	 * @return The energy drain rate.
	 */
	// private double getEnergyDrainRate(Player player) {
	// double rate = 0.234 * 3 * Math.pow(Math.E,
	// 0.0027725887222397812376689284858327062723020005374410 *
	// player.getSettings().getWeight());
	// if (player.getDetails().getShop().hasPerk(Perks.STAMINA_BOOST) &&
	// !WildernessZone.inWilderness(player, 1)) {
	// rate -= 0.57;
	// }
	// long staminaPotUsed = player.getAttribute("stamina_potion_used", -1L);
	// if (staminaPotUsed > -1) {
	// long timeRemaining = System.currentTimeMillis() - staminaPotUsed;
	// if (timeRemaining < 120000 /* 2 minutes */) {
	// rate *= 0.3; // lowered by 70% for stamina pots
	// } else player.removeAttribute("stamina_potion_used");
	// }
	// rate = player.getStateManager().hasState(EntityState.HAMSTRING) ? rate *
	// 4 : rate;
	// return rate;
	// }
	/**
	 * Gets the energy restore amount.
	 *
	 * @param player
	 *            The player.
	 * @return The amount to restore.
	 */
	// private double getEnergyRestore(Player player) {
	// double rate = 0.13 * Math.pow(Math.E,
	// 0.0162569486104454583293005993255170468638949631744294
	// * player.getSkills().getLevel(Skills.AGILITY));
	// if (player.getDetails().getShop().hasPerk(Perks.STAMINA_BOOST) &&
	// !WildernessZone.inWilderness(player, 1)) {
	// rate += 0.42;
	// }
	// if (player.hasPetTalent(Talents.HURDLER)) {
	// rate += 0.07;
	// }
	// return rate;
	// }

	/**
	 * Walks back to the last location.
	 */
	public void walkBack() {
		// entity.getPulseManager().clear();
		reset();
		addPath(footPrint.getX(), footPrint.getY());
	}

	public void addPath(Location loc) {
		addPath(loc.getX(), loc.getY());
	}

	/**
	 * Adds a path to the walking queue.
	 *
	 * @param x
	 *            The last x-coordinate of the path.
	 * @param y
	 *            The last y-coordinate of the path.
	 */
	public void addPath(int x, int y) {
		addPath(x, y, runDisabled);
	}

	public void addPath(Location location, boolean runDisabled) {
		addPath(location.getX(), location.getY(), runDisabled);
	}

	/**
	 * Adds a path to the walking queue.
	 *
	 * @param x
	 *            The last x-coordinate of the path.
	 * @param y
	 *            The last y-coordinate of the path.
	 * @param runDisabled
	 *            If running is disabled for this walking path.
	 */
	public void addPath(int x, int y, boolean runDisabled) {
		Point point = walkingQueue.peekLast();
		if (point == null) {
			return;
		}
		int diffX = x - point.getX(), diffY = y - point.getY();
		int max = Math.max(Math.abs(diffX), Math.abs(diffY));
		for (int i = 0; i < max; i++) {
			if (diffX < 0) {
				diffX++;
			} else if (diffX > 0) {
				diffX--;
			}
			if (diffY < 0) {
				diffY++;
			} else if (diffY > 0) {
				diffY--;
			}
			addPoint(x - diffX, y - diffY, runDisabled);
		}
	}

	/**
	 * Adds a point to the walking queue.
	 *
	 * @param x
	 *            The x-coordinate of the point.
	 * @param y
	 *            The y-coordinate of the point.
	 */
	public void addPoint(int x, int y, boolean runDisabled) {
		Point point = walkingQueue.peekLast();
		if (point == null) {
			return;
		}
		int diffX = x - point.getX(), diffY = y - point.getY();
		Direction direction = Direction.getDirection(diffX, diffY);
		if (direction != null) {
			walkingQueue.add(new Point(x, y, direction, diffX, diffY, runDisabled));
		}
	}

	/**
	 * Checks if the entity is running.
	 *
	 * @return {@code True} if a ctrl + click action was performed, <br>
	 *         the player has the run option enabled or the NPC is a familiar,
	 *         <p>
	 *         {@code false} if not.
	 */
	public boolean isRunningBoth() {
		// if (entity instanceof Player && ((Player)
		// entity).getSettings().isRunToggled()) {
		// return true;
		// }
		return running;
	}

	/**
	 * Checks if the entity has a path to walk.
	 *
	 * @return {@code True} if so.
	 */
	public boolean hasPath() {
		if (!walkingQueue.isEmpty()) {
			Point p = walkingQueue.peek();
			return p.getDirection() != null;
		}
		return false;
	}

	/**
	 * Checks if the entity is moving.
	 *
	 * @return {@code True} if so.
	 */
	public boolean isMoving() {
		return walkDir != -1 || runDir != -1;
	}

	/**
	 * Resets the walking queue.
	 */
	public void reset() {
		reset(running);
	}

	/**
	 * Resets the walking queue.
	 *
	 * @param running
	 *            The running flag (ctrl + click action).
	 */
	public void reset(boolean running) {
		walkingQueue.clear();
		walkingQueue.add(new Point(entity.getCurrentLocation().getX(), entity.getCurrentLocation().getY()));
		// entity.setAttribute("check_all_paths", false);
		this.running = running;
	}

	public void resetAll() {
		reset();
		// if (entity instanceof Player) {
		// Player p = (Player) entity;
		// p.getPulseManager().clear("movement", "rest");
		// p.getPacketDispatch().clearMinimapFlag();
		// }
	}

	/**
	 * Gets the current walking direction.
	 *
	 * @return The walk direction.
	 */
	public int getWalkDir() {
		return walkDir;
	}

	/**
	 * Gets the current run direction.
	 *
	 * @return The run direction.
	 */
	public int getRunDir() {
		return runDir;
	}

	/**
	 * Sets the running flag.
	 *
	 * @param running
	 *            The running flag.
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}

	/**
	 * Checks if the player is running.
	 *
	 * @return {@code True} if so.
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * @return the footPrint
	 */
	public Location getFootPrint() {
		return footPrint;
	}

	/**
	 * @param footPrint
	 *            the footPrint to set
	 */
//	public void setFootPrint(Location footPrint) {
//		this.footPrint = footPrint;
//	}

	/**
	 * Gets the walking queue.
	 *
	 * @return The queue.
	 */
	public Deque<Point> getQueue() {
		return walkingQueue;
	}

	/**
	 * Gets the runDisabled.
	 *
	 * @return The runDisabled.
	 */
	public boolean isRunDisabled() {
		return runDisabled;
	}

	/**
	 * Sets the runDisabled.
	 *
	 * @param runDisabled
	 *            The runDisabled to set.
	 */
	public void setRunDisabled(boolean runDisabled) {
		this.runDisabled = runDisabled;
	}

	public void update() {
		if (entity.isLockActions() || !entity.getLockMovement().complete())
			return;
		boolean isPlayer = entity.isPlayer();
		this.walkDir = -1;
		this.runDir = -1;
		// if (updateTeleport()) {
		// return;
		// }
		// if (isPlayer && updateRegion(entity.getLocation(), true)) {
		// return;
		// }
		Point point = walkingQueue.poll();
		if (point == null) {
			// updateRunEnergy(false);
			// entity.forceChat("empty queue");
			return;
		}
		// if (isPlayer && ((Player) entity).getSettings().getRunEnergy() < 1.0)
		// {
		// running = false;
		// ((Player) entity).getSettings().setRunToggled(false);
		// }
		Point runPoint = null;
		if (point.getDirection() == null) {
			point = walkingQueue.poll();
		}
		int walkDirection = -1;
		int runDirection = -1;
		if (isRunningBoth() && (point == null || !point.isRunDisabled())) {
			runPoint = walkingQueue.poll();
		}
		if (point != null) {
			if (point.getDirection() == null) {
				return;
			}
			walkDirection = point.getDirection().ordinal();
		}
		if (runPoint != null) {
			runDirection = runPoint.getDirection().ordinal();
		}
		int diffX = 0;
		int diffY = 0;
		if (walkDirection != -1) {
			diffX = point.getDiffX();
			diffY = point.getDiffY();
		}
		if (runDirection != -1) {
//			footPrint = entity.getCurrentLocation().transform(diffX, diffY, 0);
			footPrint.copyLocation(entity.getCurrentLocation());
			footPrint.transformMe(diffX, diffY, 0);
			diffX += runPoint.getDiffX();
			diffY += runPoint.getDiffY();
			// updateRunEnergy(true);
		} else {
			// updateRunEnergy(false);
		}
		if (diffX != 0 || diffY != 0) {
			boolean npcWalkingHome = false;
			if (entity.isNpc()) {
				NPC n = entity.toNpc();
				npcWalkingHome = n.isWalkingHome();
				if (!n.npcCanMove()) {
					reset();
					return;
				}
			}
			
			walkTo.copyLocation(entity.getCurrentLocation()); //Location walk = entity.getCurrentLocation();
			if (point != null) {
				walkTo.transformMe(point.getDiffX(), point.getDiffY(), 0); //walk = walk.transform(point.getDiffX(), point.getDiffY(), 0);
				// check if entity can move to location
				// if
				// (!entity.getZoneMonitor().move(entity.getCurrentLocation(),
				// walk)) {
				// reset();
				// Pulse current = entity.getPulseManager().getCurrent();
				// if (current != null && current.isRunning() && current
				// instanceof MovementPulse) {
				// entity.getPulseManager().clear();
				// }
				// /*if (entity.getPulseManager().isMovingPulse()) {
				// entity.getPulseManager().clear(); // TODO: Check for bugs
				// }*/
				// return;
				// }
			}

			// Location dest = entity.getCurrentLocation().transform(diffX, diffY, 0);
			walkTo.copyLocation(entity.getCurrentLocation());
			walkTo.transformMe(diffX, diffY, 0);
			
			// TODO: do clipping check in case object is moved/added
			if (!isPlayer) {
				// if (!RegionClip.canNpcStandOnNextTile(diffX, diffY,
				// entity.toNpc().getSize(), entity.getCurrentLocation(),
				// entity.getDynamicRegionClip()))//if
				// (RegionClip.npcOnTile(dest, null))
				// return;
			}
			// if (!npcWalkingHome) {
			// Location cLoc = entity.getCurrentLocation();
			// if (!PathFinder.canMove(cLoc.getX(), cLoc.getY(), cLoc.getZ(),
			// diffX, diffY)) {
			// reset();
			// return;
			// }
			// }

			if (runPoint != null) {
				// if (!entity.getZoneMonitor().move(walk, dest)) {
				// dest = dest.transform(-runPoint.getDiffX(),
				// -runPoint.getDiffY(), 0);
				// runPoint = null;
				// runDirection = -1;
				// reset();
				// if (entity.getPulseManager().isMovingPulse()) {
				// entity.getPulseManager().clear(); // TODO: Check for bugs
				// }
				// }
			}
			// if (runPoint != null) {
			// entity.setDirection(runPoint.getDirection());
			// } else if (point != null) {
			// entity.setDirection(point.getDirection());
			// }
			
			footPrint.copyLocation(entity.getCurrentLocation()); //footPrint = entity.getCurrentLocation();
			entity.setCurrentLocation(walkTo);
			// RegionManager.move(entity); //TODO: add this for our server too,
			// instead of shitcode down here
			if (!isPlayer) {
				NPC n = entity.toNpc();
				// n.direction = -1;
				// n.moveX = diffX;
				// n.moveY = diffY;
				// n.direction = n.getNextWalkingDirection();
				n.getLastLocation().setX(n.getX());
				n.getLastLocation().setY(n.getY());
				n.getLastLocation().setZ(n.getZ());
				n.absX = walkTo.getX();
				n.absY = walkTo.getY();
				n.setLocation(n.getX(), n.getY());
				n.moveX = n.moveY = 0;
			}
		}
		this.walkDir = walkDirection;
		this.runDir = runDirection;
	}

	public Location getWalkTo() {
		return walkTo;
	}

}
