package com.soulplay.game.world.entity;

import com.soulplay.Config;
import com.soulplay.Server;

public final class Graphic implements Comparable<Graphic> {

	private final int id;

	private final int delay;

	private final GraphicType type;

	private final Priority priority;
	
	public void send(Location location) {
		send(location.getX(), location.getY(), location.getZ());
	}
	
	public void send(int x, int y, int z) {
		Server.getStillGraphicsManager().createGfx(x, y, z, getId(), 0, getDelay());
	}
	
	public enum GraphicType {
		EVEN_HIGHER(360<<16), BOWARROWPULL(106<<16), HIGH(100<<16), FIFTY(50 << 16), THIRTY(30 << 16), MEDIUM(10<<16), LOW(1<<16), PROPER_LOW(0), HYDRA(170 << 16);

		private final int height;

		GraphicType(int height) {
			this.height = height;
		}

		public int getHeight() {
			return height;
		}
		
	}
	
	public static Graphic create(int id) {
		return new Graphic(Priority.NORMAL, id, 0, GraphicType.LOW);
	}

	public static Graphic create(int id, GraphicType type) {
		return new Graphic(Priority.NORMAL, id, 0, type);
	}

	public static Graphic create(int id, int delay, GraphicType type) {
		return new Graphic(Priority.NORMAL, id, delay, type);
	}

	public static Graphic create(Priority priority, int id, GraphicType type) {
		return new Graphic(priority, id, 0, type);
	}

	public static Graphic create(Priority priority, int id, int delay, GraphicType type) {
		return new Graphic(priority, id, delay, type);
	}

	public static Graphic osrs(int id) {
		return new Graphic(Priority.NORMAL, getOsrsId(id), 0, GraphicType.LOW);
	}

	public static Graphic osrs(int id, GraphicType type) {
		return new Graphic(Priority.NORMAL, getOsrsId(id), 0, type);
	}

	public static Graphic osrs(int id, int delay, GraphicType type) {
		return new Graphic(Priority.NORMAL, getOsrsId(id), delay, type);
	}

	public static Graphic osrs(Priority priority, int id, GraphicType type) {
		return new Graphic(priority, getOsrsId(id), 0, type);
	}

	public static Graphic osrs(Priority priority, int id, int delay, GraphicType type) {
		return new Graphic(priority, getOsrsId(id), delay, type);
	}

	public Graphic(Priority priority, int id, int delay, GraphicType type) {
		this.priority = priority;
		this.id = id;
		this.delay = delay;
		this.type = type;
	}	

	public int getId() {
		return id;
	}
	
	public int getOsrsId() {
		return id + Config.OSRS_GFX_OFFSET;
	}

	public int getDelay() {
		return delay;
	}

	public GraphicType getType() {
		return type;
	}

	public Priority getPriority() {
		return priority;
	}
	
	public Graphic copy() {
		return new Graphic(this.getPriority(), this.getId(), this.getDelay(), this.getType());
	}

	@Override
	public int compareTo(Graphic other) {
		if (other == null) {
			return 1;
		}

		return Integer.signum(other.priority.getPriority() - priority.getPriority());
	}

	@Override
	public String toString() {
		return String.format("gfx[priority=%s, id=%s, delay=%s, height=%s]", priority, id, delay, type.name());
	}
	
	public static int getOsrsId(int gfxId) {
		return Config.OSRS_GFX_OFFSET + gfxId;
	}

}
