package com.soulplay.game.world.entity;

import java.util.Optional;

/**
 * The different types an in-game object can me.
 * 
 * @author Freyr
 */
public enum ObjectType {

    STRAIGHT(0),
    TRIANGULAR_CORNER(1),
    CORNER(2),
    RECTANGULAR_CORNER(3),
    STRAIGHT_INSIDE(4),
    STRAIGHT_OUTSIDE(5),
    DIAGONAL_OUTSIDE(6),
    DIAGONAL_INSIDE(7),
    DIAGONAL_IN(8),
    DIAGONAL(9),
    INTERACTABLE(10),
    GROUND_OBJECT(11),
    STRAIGHT_SLOPED(12),
    DIAGONAL_SLOPED(13),
    DIAGONAL_SLOPED_CONNECTING(14),
    STRAIGHT_SLOPED_CORNER_CONNECTING(15),
    STRAIGHT_SLOPED_CORNER(16),
    STRAIGHT_FLAT(17),
    STRAIGHT_BOTTOM_EDGE(18),
    DIAGONAL_BOTTOM_EDGE_CONNECTING(19),
    STRAIGHT_BOTTOM_EDGE_CONNECTING(20),
    STRAIGHT_BOTTOM_EDGE_CONNECTING_CORNER(21),
    GROUND_DECORATION(22);
	
	public static final ObjectType[] values = ObjectType.values();

    private final int value;

    private ObjectType(int value) {
	this.value = value;
    }
    
    public int getValue() {
	return value;
    }

    public static Optional<ObjectType> lookup(int value) {
	for (ObjectType type : ObjectType.values) {
	    if (type.getValue() == value) {
		return Optional.of(type);
	    }
	}
	return Optional.empty();
    }

}
