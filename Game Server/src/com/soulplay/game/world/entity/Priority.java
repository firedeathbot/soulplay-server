package com.soulplay.game.world.entity;

public enum Priority {

	LOW(0), NORMAL(1), HIGH(2), EXTRA_HIGH(3);

	private final int priority;

	private Priority(int priority) {
		this.priority = priority;
	}

	public int getPriority() {
		return priority;
	}

}
