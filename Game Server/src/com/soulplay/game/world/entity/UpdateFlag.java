package com.soulplay.game.world.entity;

/**
 * The bit-based flags for updating various actions for mobile entities.
 *
 * @author Freyr
 */
public enum UpdateFlag {
	APPEARANCE(0x10, -1),
	CHAT(0x80, -1),
	GRAPHICS(0x100, 0x80),
	ANIMATION(0x8, 0x10),
	FORCED_CHAT(0x4, 0x200),
	ENTITY_INTERACTION(0x1, 0x20),
	FACE_COORDINATE(0x2, 0x4),
	HIT(0x20, 0x40),
	HIT2(0x200, 0x8),
	TRANSFORM(-1, 0x2),
	FORCE_MOVEMENT(0x400, 0x800),
	PET(-1, 0x100),
	NAME_CHANGE(-1, 0x400),
	BAR_FILL(0x800, 0x1000),
	CLONE(0x1000, -1);

	private final int maskPlayer;
	private final int maskNpc;

	private UpdateFlag(int maskPlayer, int maskNpc) {
		this.maskPlayer = maskPlayer;
		this.maskNpc = maskNpc;
	}

	public int getMask(boolean player) {
		return player ? maskPlayer : maskNpc;
	}

}