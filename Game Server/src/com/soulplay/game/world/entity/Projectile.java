package com.soulplay.game.world.entity;

import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class Projectile {
	
	private int projectileGFX;
	private Location startLoc;
	private Location endLoc;
	private int endDelay;
	private int startDelay;
	private int startHeight;
	private int endHeight;
	private int lockon;
	private int offsetX;
	private int offsetY;
	private int slope;
	private int distance;
	private int startOffset;
	private int homeOwnerId; // instanced projectile
	
	public Projectile(int gfx, Mob source, Mob target) {
		this(gfx, source.getCenterLocation(), target.getCurrentLocation());
		setLockon(target.getProjectileLockon());
	}

	public Projectile(int gfx, Location start, Mob target) {
		this(gfx, start, target.getCurrentLocation());
		setLockon(target.getProjectileLockon());
	}

	public Projectile(int gfx, Location start, Location end) {
		this.setProjectileGFX(gfx);
		this.startLoc = start;
		this.endLoc = end;
		this.setOffsetX((startLoc.getY() - endLoc.getY()) * -1);
		this.setOffsetY((startLoc.getX() - endLoc.getX()) * -1);
		this.distance = Misc.distanceToPoint(start.getX(), start.getY(), end.getX(), end.getY());
		this.endDelay = 5 * distance + 55; //53 - dist;
		this.startDelay = 65;
		this.startHeight = 43;
		this.endHeight = 31;
		this.slope = 16;
		this.startOffset = 64;
	}
	
	public Projectile copy() {
		Projectile proj = new Projectile(getProjectileGFX(), getStartLoc().copyNew(), getEndLoc().copyNew());
		proj.setEndDelay(getEndDelay());
		proj.setStartDelay(getStartDelay());
		proj.setStartHeight(getStartHeight());
		proj.setEndHeight(getEndHeight());
		proj.setSlope(getSlope());
		proj.setLockon(getLockon());
		proj.setStartOffset(getStartOffset());
		return proj;
	}
	
	public Location getStartLoc() {
		return startLoc;
	}
	
	public Location getEndLoc() {
		return endLoc;
	}
	
	public Projectile setStartLoc(Location loc) {
		this.startLoc = loc;
		this.setOffsetX((startLoc.getY() - endLoc.getY()) * -1);
		this.setOffsetY((startLoc.getX() - endLoc.getX()) * -1);
		return this;
	}
	
	public Projectile setEndLoc(Location loc) {
		this.endLoc = loc;
		this.setOffsetX((startLoc.getY() - endLoc.getY()) * -1);
		this.setOffsetY((startLoc.getX() - endLoc.getX()) * -1);
		return this;
	}
	
	public int getProjectileGFX() {
		return projectileGFX;
	}
	
	public Projectile setProjectileGFX(int projectileGFX) {
		this.projectileGFX = projectileGFX;
		return this;
	}
	
	public int getEndDelay() {
		return endDelay;
	}
	
	public Projectile setEndDelay(int speed) {
		this.endDelay = speed;
		return this;
	}
	
	public int getStartDelay() {
		return startDelay;
	}
	
	/**
	 * Time is the delay in client ticks. 1 server tick is 30 client ticks.
	 *
	 * @param time
	 */
	public Projectile setStartDelay(int time) {
		this.startDelay = time;
		return this;
	}
	
	public int getStartHeight() {
		return startHeight;
	}
	
	public Projectile setStartHeight(int startHeight) {
		this.startHeight = startHeight;
		return this;
	}
	
	public int getEndHeight() {
		return endHeight;
	}
	
	public Projectile setEndHeight(int endHeight) {
		this.endHeight = endHeight;
		return this;
	}
	
	public int getLockon() {
		return lockon;
	}
	
	public Projectile setLockon(int lockon) {
		this.lockon = lockon;
		return this;
	}
	
	public int getOffsetX() {
		return offsetX;
	}
	
	public Projectile setOffsetX(int offsetX) {
		this.offsetX = offsetX;
		return this;
	}
	
	public int getOffsetY() {
		return offsetY;
	}
	
	public Projectile setOffsetY(int offsetY) {
		this.offsetY = offsetY;
		return this;
	}
	
	public int getSlope() {
		return slope;
	}
	
	public Projectile setSlope(int slope) {
		this.slope = slope;
		return this;
	}
	
	public int getDistance() {
		return distance;
	}
	
	public int getStartOffset() {
		return startOffset;
	}
	
	public Projectile setStartOffset(int startOffset) {
		this.startOffset = startOffset;
		return this;
	}
	
	public static Location getLocationOffset(Location start, Location end, int size) { // LOS
		int centerOff = size == 7 ? 3 : size == 5 ? 2 : 0;
		
		size--;
		
		int x1 = start.getX() + centerOff;
		int y1 = start.getY() + centerOff;
		
		int x2 = end.getX();
		int y2 = end.getY();
		
		int dx = Math.abs(x2 - x1);
		int dy = Math.abs(y2 - y1);
		int sx = (x1 < x2) ? 1 : -1;
		int sy = (y1 < y2) ? 1 : -1;
		int err = dx - dy;
		int n = centerOff; //1+centerOff; //TODO: maybe add offset to the parameters so we can control how far outwards we want the projectile to start
		
		int x3 = x1;
		int y3 = y1;
		
		for (; n > 0; --n) {
			
			x3 = x1; // first x
			y3 = y1; // first y
			
			if (x1 == x2 && y1 == y2) {
				break;
			}
			int e2 = 2 * err;
			if (e2 > -dy) {
				err = err - dy;
				x1 = x1 + sx;
			}
			if (e2 < dx) {
				err = err + dx;
				y1 = y1 + sy;
			}
			
		}
		return Location.create(x3, y3, start.getZ());
	}
	
	public int getHomeOwnerId() {
		return homeOwnerId;
	}
	
	public Projectile setHomeOwnerId(int homeOwnerId) {
		this.homeOwnerId = homeOwnerId;
		return this;
	}
	
	public static final int DEFAULT_START_HEIGHT = 40;
	public static final int DEFAULT_END_HEIGHT = 36;
	public static final int DEFAULT_START_DELAY = 41;
	public static final int DEFAULT_SLOPE = 5; // angle
	public static final int DEFAULT_START_OFFSET = 11; // distance
	
	public static int getDefaultSpeed(Location start, Location destination) {
		return (int) (46 + (start.getDistance(destination) * 5));
	}
	
	public static int getDefaultSpeed(Mob source, Mob target) {
		return getDefaultSpeed(source.getCurrentLocation(), target.getCurrentLocation());
	}

	public static int getDefaultSpeed(Location start, Mob target) {
		return getDefaultSpeed(start, target.getCurrentLocation());
	}

	public static Projectile create(Location start, Location destination, int projectileID) {
		return new Projectile(projectileID, start, destination)
				.setDefaults()
				.setEndDelay(getDefaultSpeed(start, destination));
	}

	public static Projectile create(Location start, Mob target, int projectileID) {
		return new Projectile(projectileID, start, target)
				.setDefaults()
				.setEndDelay(getDefaultSpeed(start, target));
	}

	public static Projectile create(Mob source, Mob target, int projectileID) {
		return new Projectile(projectileID, source, target)
				.setDefaults()
				.setEndDelay(getDefaultSpeed(source, target));
	}
	
	public Projectile setDefaults() {
		setStartHeight(DEFAULT_START_HEIGHT);
		setEndHeight(DEFAULT_END_HEIGHT);
		setStartDelay(DEFAULT_START_DELAY);
		setSlope(DEFAULT_SLOPE); // angle
		setStartOffset(DEFAULT_START_OFFSET); // distance
		return this;
	}
	
	public Projectile send() {
		GlobalPackets.createProjectile(this);
		return this;
	}
	
}
