package com.soulplay.game.world.entity;

import java.util.Objects;

import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.world.Direction;
import com.soulplay.util.Misc;

public final class Location {

	private int x;
	private int y;
	private int z;

	private Location(int x, int y) {
		this.x = x;
		this.y = y;
		this.z = 0;
	}

	private Location(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Location(Location tile, int randomize) {
		this.x = (tile.x + Misc.newRandom(randomize * 2 + 1) - randomize);
		this.y = (tile.y + Misc.newRandom(randomize * 2 + 1) - randomize);
		this.z = tile.z;
	}

	public static Location create(int x, int y) {
		return new Location(x, y);
	}

	public static Location create(int x, int y, int z) {
		return new Location(x, y, z);
	}

	public Location copyNew() {
		return new Location(x, y, z);
	}
	
	public void copyLocation(Location l) {
		this.x = l.getX();
		this.y = l.getY();
		this.z = l.getZ();
	}

	public static boolean equals(Location first, Location second) {
		return first.getX() == second.getX() && first.getY() == second.getY();
	}
	
	/**
	 * Returns the distance between you and the other.
	 *
	 * @param other The other location.
	 * @return The amount of distance between you and other.
	 */
	public double getDistance(Location other) {
		int xdiff = this.getX() - other.getX();
		int ydiff = this.getY() - other.getY();
		return Math.sqrt(xdiff * xdiff + ydiff * ydiff);
	}

	public int getDistanceInt(int x, int y) {
		int xdiff = this.getX() - x;
		int ydiff = this.getY() - y;
		return (int) Math.sqrt(xdiff * xdiff + ydiff * ydiff);
	}

	public int getDistanceInt(Location other) {
		return getDistanceInt(other.getX(), other.getY());
	}

	public int getDx(Location other) {
		return getX() - other.getX();
	}

	public int getDy(Location other) {
		return getY() - other.getY();
	}

	public boolean isWithinDistance(Location other, int distance) {
		if (this.getZ() != other.getZ()) {
			return false;
		}

		return Math.abs(other.getX() - getX()) <= distance && Math.abs(other.getY() - getY()) <= distance;
	}

	public boolean isWithinCircle(Location other, int radius) {
		if (this.getZ() != other.getZ()) {
			return false;
		}

		return Misc.isPointInCircle(other.getX(), other.getY(), x, y, radius);
	}

	public boolean isWithinDistanceNoZ(Location other, int distance) {
		return Math.abs(other.getX() - getX()) <= distance && Math.abs(other.getY() - getY()) <= distance;
	}

	public boolean isWithinViewDistance(Location other) {
		int deltaX = other.getX() - this.getX(), deltaY = other.getY() - this.getY();
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}
	
	public boolean isWithinDeltaDistance(Location other, int distance) {
		int deltaX = other.getX() - this.getX(), deltaY = other.getY() - this.getY();
		return deltaX <= distance && deltaX >= -distance && deltaY <= distance && deltaY >= -distance;
	}

	public int getZ() {
		return this.z;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public static Location getShoved(Location attacker, Location victim, DynamicRegionClip dynClip) {
		int x = victim.getX() - attacker.getX();
		int y = victim.getY() - attacker.getY();
		if (x > 0) {
			if (RegionClip.getClippingDirection(victim.getX() + 1, victim.getY(), victim.getZ(), 1, 0, dynClip)) {
				x = 1;
				y = 0;
			}
		} else if (x < 0) {
			if (RegionClip.getClippingDirection(victim.getX() - 1, victim.getY(), victim.getZ(), -1, 0, dynClip)) {
				x = -1;
				y = 0;
			}
		}
		if (y > 0) {
			if (RegionClip.getClippingDirection(victim.getX(), victim.getY() + 1, victim.getZ(), 0, 1, dynClip)) {
				y = 1;
				x = 0;
			}
		} else if (y < 0) {
			if (RegionClip.getClippingDirection(victim.getX(), victim.getY() - 1, victim.getZ(), 0, -1, dynClip)) {
				y = -1;
				x = 0;
			}
		}
		if (x < -1 || x > 1) // cheaphax for now
			x = 0;
		if (y < -1 || y > 1) // cheaphax for now
			y = 0;
		return Location.create(x, y, victim.getZ());
	}

	/**
	 * Gets the location that is close to teleto and is not blocked by path.
	 * 
	 * @param teleto
	 * @return
	 */
	public static Location getCloseLocation(Location teleto) {
		return getCloseLocation(teleto, null);
	}

	public static Location getCloseLocation(Location to, DynamicRegionClip clip) {
		if ((RegionClip.getClipping(to.getX(), to.getY() + 1, to.getZ(), clip) & 0x1280120) != 0) {
			return Location.create(to.getX(), to.getY() + 1, to.getZ());
		}
		if ((RegionClip.getClipping(to.getX() + 1, to.getY(), to.getZ(), clip) & 0x1280180) != 0) {
			return Location.create(to.getX() + 1, to.getY(), to.getZ());
		}
		if ((RegionClip.getClipping(to.getX(), to.getY() - 1, to.getZ(), clip) & 0x1280102) != 0) {
			return Location.create(to.getX(), to.getY() - 1, to.getZ());
		}
		if ((RegionClip.getClipping(to.getX() - 1, to.getY(), to.getZ(), clip) & 0x1280108) != 0) {
			return Location.create(to.getX() - 1, to.getY(), to.getZ());
		}
		return to.copyNew();
	}

	private static byte[][] closestOffset = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };
	
	public Location getClosestLocation(Location to, DynamicRegionClip dynClip) {
		return getClosestLocation(to, dynClip, null);
	}

	public Location getClosestLocation(Location to, DynamicRegionClip dynClip, Location ownerLoc) {
		int lastClosest = 20;
		int closestIndex = 0;
		boolean foundSpot = false;
		int ddOwner = -1;
		for (int l = 0; l < 4; l++) { // find closest spot
			int otherX = to.getX() + closestOffset[l][0];
			int otherY = to.getY() + closestOffset[l][1];
			int dist = Misc.distanceToPoint(getX(), getY(), otherX, otherY);
			if (dist < lastClosest) {
				if (!PathFinder.canMove(to.getX(), to.getY(), getZ(), closestOffset[l][0], closestOffset[l][1], dynClip)) {
					continue;
				}
				if (ownerLoc != null && ownerLoc.getX() == otherX && ownerLoc.getY() == otherY) {
					ddOwner = l;
					continue;
				}
				closestIndex = l;
				lastClosest = dist;
				foundSpot = true;
			}
		}
		
		if (!foundSpot && ddOwner != -1) {
			closestIndex = ddOwner;
		}
		
		return Location.create(to.getX() + closestOffset[closestIndex][0], to.getY() + closestOffset[closestIndex][1], getZ());
	}

	public static boolean canTouchNpc(Mob p, NPC n, boolean usingRanged) {
		return !RegionClip.rayTraceBlocked(p.getCurrentLocation().getX(), p.getCurrentLocation().getY(), n.getXtoPlayerLoc(p.getCurrentLocation().getX()), n.getYtoPlayerLoc(p.getCurrentLocation().getY()), p.getCurrentLocation().getZ(), usingRanged, p.getDynamicRegionClip());
	}
	
	public static boolean overlaps(int x1, int y1, int size1, int x2, int y2, int size2) {
		size2 -= 1;
		size1 -= 1;
		return x1 < x2 + size2 && x1 + size1 > x2 && y1 < y2 + size2 && y1 + size1 > y2;
	}
	
	public boolean overlaps(int size1, Location loc2, int size2) {
		size1 -= 1;
		size2 -= 1;
		
		if (getY()+size1 < loc2.getY() || getY() > loc2.getY()+size2)
			return false;
		
		if (getX()+size1 < loc2.getX() || getX() > loc2.getX()+size2)
			return false;
		
		return true;
	}

	/**
	 * Checks if this location is right next to the node (assuming the node is
	 * size 1x1).
	 *
	 * @param node
	 *            The node to check.
	 * @return {@code True} if this location is 1 tile north, west, south or
	 *         east of the node location.
	 */
	public boolean isNextTo(int x, int y) {
		if (y == getY()) {
			return x - getX() == -1 || x - getX() == 1;
		}
		if (x == getX()) {
			return y - getY() == -1 || y - getY() == 1;
		}
		return false;
	}
	
	public boolean isNextTo(Location l) {
		return isNextTo(l.getX(), l.getY());
	}

	/**
	 * Gets the region id.
	 *
	 * @return The region id.
	 */
	public int getRegionId() {
		return (getX() >> 6) << 8 | (getY() >> 6);
	}

	/**
	 * Gets the location incremented by the given coordinates.
	 *
	 * @param dir
	 *            The direction to transform this location.
	 * @return The location.
	 */
	public Location transform(Direction dir) {
		return transform(dir, 1);
	}

	/**
	 * Gets the location incremented by the given coordinates.
	 *
	 * @param dir
	 *            The direction to transform this location.
	 * @param steps
	 *            The amount of steps to move in this direction.
	 * @return The location.
	 */
	public Location transform(Direction dir, int steps) {
		return new Location(getX() + (dir.getStepX() * steps), getY() + (dir.getStepY() * steps), getZ());
	}

	/**
	 * Gets the location incremented by the given coordinates.
	 *
	 * @param diffX
	 *            The x-difference.
	 * @param diffY
	 *            The y-difference.
	 * @param z
	 *            The height difference.
	 * @return The location.
	 */
	public Location transform(Location l) {
		return new Location(getX() + l.getX(), getY() + l.getY(), getZ() + l.getZ());
	}

	/**
	 * Gets the location incremented by the given coordinates.
	 *
	 * @param diffX
	 *            The x-difference.
	 * @param diffY
	 *            The y-difference.
	 * @return The location.
	 */
	public Location transform(int diffX, int diffY) {
		return new Location(getX() + diffX, getY() + diffY, getZ());
	}

	/**
	 * Gets the location incremented by the given coordinates.
	 *
	 * @param diffX
	 *            The x-difference.
	 * @param diffY
	 *            The y-difference.
	 * @param diffZ
	 *            The height difference.
	 * @return The location.
	 */
	public Location transform(int diffX, int diffY, int diffZ) {
		return new Location(getX() + diffX, getY() + diffY, getZ() + diffZ);
	}

	public Location subtract(Location l) {
		return new Location(getX() - l.getX(), getY() - l.getY(), getZ());
	}

	public void transformMe(int diffX, int diffY, int diffZ) {
		this.setX(getX() + diffX);
		this.setY(getY() + diffY);
		this.setZ(getZ() + diffZ);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, z);
	}

	@Override
	public boolean equals(Object o) {
		Objects.requireNonNull(o);

		if (o instanceof Location) {
			Location other = (Location) o;

			return other.hashCode() == hashCode();
		}

		return false;
	}

	public boolean matches(Location other) {
		if (other == null) {
			return false;
		}

		return x == other.x && y == other.y && z == other.z;
	}

	public int toBitPacked() {//this should be changed to long if z > 3 else overflow
		return z << 30 | (y & 0x7FFF) << 15 | x & 0x7FFF;
	}

	public int getMapZoneX() {
		return (getX() >> 3);
	}

	public int getMapZoneY() {// actually z btw
		return (getY() >> 3);
	}
	
	public int getLocalX() {
		return getX() - ((getX() >> 6) << 6);
	}

	public int getLocalX(int x) {
		return x - ((getX() >> 6) << 6);
	}

	public int getLocalY() {
		return getY() - ((getY() >> 6) << 6);
	}

	public int getLocalY(int y) {
		return y - ((getY() >> 6) << 6);
	}
	
	public int getChunkOffsetX() {
		int y = getLocalX();
		return y - ((y / 8) * 8);
	}
	
	public int getChunkOffsetY() {
		int y = getLocalY();
		return y - ((y / 8) * 8);
	}
	
	/**
	 * Gets the base location for the chunk this location is in.
	 *
	 * @return The base location.
	 */
	public Location getChunkBase() {
		return create(getMapZoneX() << 3, getMapZoneY() << 3, getZ());
	}

	public Location getMapZoneBase(int z) {
		int newX = x - (x & 63);
		int newY = y - (y & 63);
		return create(newX, newY, z);
	}

	public Location getMapZoneBase() {
		return getMapZoneBase(z);
	}

	@Override
	public String toString() {
		return String.format("location[x=%d y=%d z=%d]", getX(), getY(), getZ());
	}

}
