package com.soulplay.game.world.entity;

import com.soulplay.game.world.Direction;

public class Location2D {

	private int x;
	private int y;

	private Location2D(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public static Location2D create(int x, int y) {
		return new Location2D(x, y);
	}

	public Location2D copyNew() {
		return new Location2D(x, y);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Location2D transform(Direction dir) {
		return transform(dir, 1);
	}

	public Location2D transform(Direction dir, int steps) {
		return create(getX() + (dir.getStepX() * steps), getY() + (dir.getStepY() * steps));
	}

	public Location toLocation() {
		return toLocation(0);
	}

	public Location toLocation(int z) {
		return Location.create(x, y, z);
	}

	@Override
	public String toString() {
		return String.format("location[x=%d y=%d]", getX(), getY());
	}

}
