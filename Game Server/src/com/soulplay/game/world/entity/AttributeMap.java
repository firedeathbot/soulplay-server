package com.soulplay.game.world.entity;

import java.util.HashMap;
import java.util.Map;

public final class AttributeMap {

    private final Map<String, Object> attrs = new HashMap<>();

    @SuppressWarnings("unchecked")
    public <T> T get(String name) {
        return (T) attrs.get(name);
    }

    @SuppressWarnings("unchecked")
    public <T> T getOrDefault(String name, T defaultValue) {
        return (T) attrs.getOrDefault(name, defaultValue);
    }

    public boolean has(String name) {
        final Object result = attrs.get(name);
        return result != null && result instanceof Boolean && (boolean) result;
    }

    public <T> T put(String name, T value) {
        attrs.put(name, value);
        return value;
    }
    
    public void remove(String name) {
        attrs.remove(name);
    }

    public boolean contains(String name) {
       return attrs.containsKey(name);
    }

}
