package com.soulplay.game.world;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.CustomObject;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

/**
 * @author Aleksandr Retardo
 */

public class ObjectManager {

	public static List<RSObject> objects = new ArrayList<>();

	private static List<RSObject> toRemove = new ArrayList<>();

	private static List<CustomObject> customObject = new ArrayList<>();

	public static void addCustomObject(CustomObject o) {
		customObject.add(o);
	}

	public static void addObject(RSObject o) {
		placeUpdateObject(o);
	}

	public static void clearCustomObjectList() {
		customObject.clear();
	}

	public static void clipCustomObject(CustomObject o) {
		if (o.getId() == -1) {
			GameObject gO = RegionClip.getGameObject(o.getX(), o.getY(),
					o.getHeight(), o.getType(), true); // RegionClip.pullAllObjects(o.getX(),
														// o.getY(),
														// o.getHeight());
			if (gO == null) {
				System.out.println("Null at " + o.getX() + " " + o.getY());
				return;
			}
//			RegionClip.removeObjectClipping(gO.getId(), gO.getX(), gO.getY(), gO.getZ(),
//					gO.getOrientation(), gO.getType());
			RegionClip.removeGameObject(gO);
		} else {
			RegionClip.addObject(o.getId(), o.getX(), o.getY(), o.getHeight(),
					o.getType(), o.getFace(), true);
		}
	}

	public static CustomObject getCustomObject(int x, int y, int z) {
		for (CustomObject o : customObject) {
			if (o.getX() == x && o.getY() == y && o.getHeight() == z) {
				return o;
			}
		}
		return null;
	}

	public static RSObject getObject(int x, int y, int height) {
		for (RSObject o : Server.getRegionManager().getRegionByLocation(x, y, height).getRSObjects()/*objects*/) {
			if (o.isActive() && o.getX() == x && o.getY() == y) {
				return o;
			}
		}
		return null;
	}

	public static RSObject getObjectByNewId(int id, int x, int y, int height) {
		for (RSObject o : Server.getRegionManager().getRegionByLocation(x, y, height).getRSObjects()/*objects*/) {
			if (o.isActive() && o.getNewObjectId() == id && o.getX() == x && o.getY() == y) {
				return o;
			}
		}
		return null;
	}
	
	public static RSObject getObjectByOwnerLongName(long playerLongName, int x, int y, int height) {
		for (RSObject o : Server.getRegionManager().getRegionByLocation(x, y, height).getRSObjects()/*objects*/) {
			if (o.isActive() && o.getObjectOwnerLong() == playerLongName && o.getX() == x && o.getY() == y) {
				return o;
			}
		}
		return null;
	}

	public static RSObject getObjectByOwnerLongName(long playerLongName) {
		for (RSObject o : objects) {
			if (o.isActive() && o.getObjectOwnerLong() == playerLongName) {
				return o;
			}
		}
		return null;
	}

	public static void getObjectCount() {
		System.out.println("Objects " + objects.size());
		// System.out.println("CustObjects "+customObject.size());
	}

	public static RSObject getPlacedObject(int x, int y, int height, int type) {
		for (RSObject o : objects) {
			if (o.isActive() && o.getX() == x && o.getY() == y && o.getHeight() == height && Misc.objectTypesCollide(type, o.getType())) {
				return o;
			}
		}
		return null;
	}

	public static void importCustomObjects() {
		BufferedReader in = null;
		FileReader reader = null;
		String s = "";
		int lineNum = 1;

		try {
			reader = new FileReader(
					"./Data/" + Config.cfgDir() + "/custom-objects.cfg");
			in = new BufferedReader(reader);
			while ((s = in.readLine()) != null) {
				String[] sArr = s.split("\t");
				try {
					if (s.startsWith("//") || s.equals("")) {
						lineNum++;
						continue;
					}
					int id = Integer.parseInt(sArr[0]);
					int x = Integer.parseInt(sArr[1]);
					int y = Integer.parseInt(sArr[2]);
					int height = Integer.parseInt(sArr[3]);
					int face = Integer.parseInt(sArr[4]);
					int type = Integer.parseInt(sArr[5]);
					CustomObject cO = new CustomObject(id, x, y, height, face,
							type);
					customObject.add(cO); // not clipped
					// addObject(new RSObject(id, x, y, height, face, type)); //
					// for clipped objects

					if (Config.SERVER_CLIPPING) {
						clipCustomObject(cO);
						// -1 means we are removing an object, so if no object
						// exists there shouldn't be any clipping.
						if (id == -2) {
							RegionClip.removeClip(x, y, height);
						}
					}

				} catch (Exception e) {
					System.out.println(
							"Error in loading custom object from text. ("
									+ lineNum + ")");
				}
				lineNum++;
			}
			reader.close();
			in.close();
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Err in loading custom objects from text");
		}
		
		// load this under to override any clipping made by custom objects
		if (Config.SERVER_CLIPPING) {
			RegionClip.loadCustomClips();
		}
	}

	public static void loadCustomObjects(Client c) {

		int fixedHeight = c.getZ() % 4;
		for (CustomObject o : customObject) {
			if (o.getHeight() != fixedHeight) {
				continue;
			}
			if (c.getZ() == o.getHeight() && Misc.isInsideRenderedArea(c, o.getX(), o.getY())) {
				c.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), o.getType(), o.getFace());
			}
		}

	}

	public static void loadFromText(Client c) {
		BufferedReader in;
		String s = "";
		int lineNum = 1;
		try {
			in = new BufferedReader(
					new FileReader("./Data/" + Config.cfgDir() + "/obj.cfg"));
			while ((s = in.readLine()) != null) {
				String[] sArr = s.split("\t");
				try {
					if (s.startsWith("//") || s.equals("")) {
						continue;
					}
					if (c.heightLevel == Integer.parseInt(sArr[5])) {
						c.getPacketSender().addObjectPacket(Integer.parseInt(sArr[0]),
								Integer.parseInt(sArr[1]),
								Integer.parseInt(sArr[2]),
								Integer.parseInt(sArr[4]),
								Integer.parseInt(sArr[3]));
					}
				} catch (Exception e) {
					System.out.println(
							"obj.cfg: Error in loading object from text. ("
									+ lineNum + ")");
				}
				lineNum++;
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Err in loading obj.cfg objects from text");
		}
	}

	public static void loadObjects(Client c) {
		if (c == null) {
			return;
		}
		
		if (c.insideDungeoneering()) {
			c.dungParty.dungManager.getObjectManager().loadObjects(c);
			return;
		}
		
		if (c.dynObjectManager != null) {
			c.dynObjectManager.loadObjects(c);
			return;
		}
		
		if (c.objectToRemove.size() > 0) {
			for (int i = 0, len = c.objectToRemove.size(); i < len; i++) {
				RSObject o = c.objectToRemove.get(i);
				if (o.getHeight() != c.heightLevel) {
					continue;
				}
				if (c.getZ() == o.getHeight() && Misc.isInsideRenderedArea(c, o.getX(), o.getY())) {
					c.getPacketSender().addObjectPacket(o.getOriginalId(), o.getX(), o.getY(), o.getType(), o.getFace());
					c.objectToRemoveTemp.add(o);
				}
			}
			for (int i = 0, len = c.objectToRemoveTemp.size(); i < len; i++) {
				RSObject o2 = c.objectToRemoveTemp.get(i);
				c.objectToRemove.remove(o2);
			}
			c.objectToRemoveTemp.clear();
		}
		for (RSObject o : objects) {
			// c.sendMessage("Ticks "+o.getTick());
			if (o.getHeight() != c.heightLevel) {
				continue;
			}
			if (c.getZ() == o.getHeight() && Misc.isInsideRenderedArea(c, o.getX(), o.getY())) {
				c.getPacketSender().addObjectPacket(o.getNewObjectId(), o.getX(), o.getY(), o.getType(), o.getFace());
			}
		}

		// CustomObjects.getSingleton().loadCustomSpawns(c);
		// loadFromText(c);
		// loadConstructionObjects(c);
		loadCustomObjects(c);
		// if (c.distanceToPoint(2813, 3463) <= 100) {
		// c.getFarming().updateHerbPatch();
		// }
	}

	public static boolean objectExists(int id, int x, int y, int height) {
		for (RSObject o : Server.getRegionManager().getRegionByLocation(x, y, height).getRSObjects()/*objects*/) {
			if (o.getNewObjectId() == id && o.getX() == x && o.getY() == y
					&& o.getTick() > 0) {
				return true;
			}
		}
		return false;
	}

	// for object type 10 and any others that cannot be placed in same spot
	public static boolean objectInSpotTypeTen(int x, int y, int height) {
		for (RSObject o : Server.getRegionManager().getRegionByLocation(x, y, height).getRSObjects()/*objects*/) {
			if (o.getTick() > 0 && (o.getType() == 10 || o.getType() == 11) && o.getX() == x && o.getY() == y
					&& o.getTick() > 0) {
				return true;
			}
		}
		return false;
	}

	public static void placeObject(RSObject o) {
		// if(o.getId() > 9 && o.getId() < 6) {
		if (o.getNewObjectId() == -1) {
			GameObject o2 = RegionClip.getGameObject(o.getX(), o.getY(),
					o.getHeight(), o.getType(), true);
			if (o2 != null) {
				RegionClip.removeGameObject(o2);
			}
		} else {
			RegionClip
					.addGameObject(new GameObject(o.getNewObjectId(), Location.create(o.getX(),
							o.getY(), o.getHeight()), o.getFace(), o.getType()));
		}
		
		//Display object for everyone
//			for (Player player : PlayerHandler.players) {// for (Player player : Server.getRegionManager().getLocalPlayersByLocation(o.getX(), o.getY(), o.getHeight())) {
//			if (player == null) {
//				continue;
//			}
//			if (player.heightLevel != o.getHeight()) {
//				continue;
//			}
//			Client c = (Client) player;

		for (Player c : Server.getRegionManager().getLocalPlayersByLocationExt(o.getX(), o.getY(), o.getHeight())) {
//			if (c != null) {

				if (c.getZ() == o.getHeight() && Misc.isInsideRenderedArea(c, o.getX(), o.getY())) {
					c.getPacketSender().addObjectPacket(o.getNewObjectId(), o.getX(), o.getY(), o.getType(), o.getFace());
				}
//			}
		}
	}

	public static void placeUpdateObject(RSObject o) {
		try {
			RSObject o2 = getPlacedObject(o.getX(), o.getY(), o.getHeight(), o.getType());
			if (o2 != null) { // why would this scenario occur???
				toRemove.add(o2);
				o2.setIsActive(false);
			}
			objects.add(o);
			Server.getRegionManager().getRegionByLocation(o.getX(), o.getY(), o.getHeight()).addRSObject(o);
			placeObject(o);
		} catch (Exception ex) {
			System.out.println("location of object on exception is - X:"
					+ o.getX() + " Y:" + o.getY() + " Z:" + o.getHeight());
			ex.printStackTrace();
		}
	}

	public static void process() {
		for (int i = 0, l = objects.size(); i < l; i++) {
			RSObject o = objects.get(i);
			if (o.getTick() > 0) {
				o.setTick(o.getTick() - 1);
				o.process();
			}
			if (o.getTick() == 0) {
				updateObject(o);
				toRemove.add(o);
				o.setIsActive(false);
			}
		}
		for (int i = 0, l = toRemove.size(); i < l; i++) {
			RSObject o = toRemove.get(i);
			objects.remove(o);
			Server.getRegionManager().getRegionByLocation(o.getX(), o.getY(), o.getHeight()).removeRSObject(o);
		}
		toRemove.clear();
	}

	/**
	 * 
	 * @param classId 1=castlewars 3=pest control
	 */
	public static void revertObjectsByClass(int classId) {
		for (RSObject o : objects) {
			if (o.getObjectClass() == classId) {
				o.setTick(0);
			}
		}
	}

	public static void saveCustomObjects() {

		BufferedWriter out;
		FileWriter writer;
		try {
			writer = new FileWriter(
					"./Data/" + Config.cfgDir() + "/custom-objects.cfg", false);
			out = new BufferedWriter(writer);

			for (CustomObject o : customObject) {
				out.write(o.getId() + "\t" + o.getX() + "\t" + o.getY() + "\t"
						+ o.getHeight() + "\t" + o.getFace() + "\t"
						+ o.getType());
				out.newLine();
			}

			// out.flush();

			out.close();
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * update spawned object to a different id without setting ticks to 0 to set
	 * the object to original state
	 */
	public static void updateNewObject(RSObject o) {
		if (o.getNewObjectId() == -1) {
			// o.tick = 0;
			GameObject o2 = RegionClip.getGameObject(o.getX(), o.getY(),
					o.getHeight(), o.getType(), true);
			if (o2 != null) {
				RegionClip.removeGameObject(o2);
			}
		} else {
			RegionClip.addGameObject(new GameObject(o.getOriginalId(), Location.create(o.getX(),
					o.getY(), o.getHeight()), o.getFace(), o.getType()));
		}
		for (Player p : PlayerHandler.players) {
			if (p == null) {
				continue;
			}
			final boolean withinDistance = Misc.isInsideRenderedArea(p, o.getX(), o.getY());
			if (p.getZ() == o.getHeight() && withinDistance) {
				p.getPacketSender().addObjectPacket((o.getTick() > 0)
								? o.getNewObjectId()
								: o.getOriginalId(), o.getX(), o.getY(),
								o.getType(), o.getFace());
				continue;
			} else {
				if (withinDistance && o.getTick() <= 0) {
					p.objectToRemove.add(o);
				}
			}

		}
	}

	public static void updateObject(RSObject o) {
		if (!o.isUpdateRequired()) {
			return;
		}
		// System.out.println("updated");
		if (o.getOriginalId() == -1) {
			// o.tick = 0;
			GameObject o2 = RegionClip.getGameObject(o.getX(), o.getY(),
					o.getHeight(), o.getType(), true);
			if (o2 != null) {
				RegionClip.removeGameObject(o2);
			}
		} else {
			RegionClip.addGameObject(new GameObject(o.getOriginalId(), Location.create(o.getX(),
					o.getY(), o.getHeight()), o.getFace(), o.getType()));
		}
		if (o.getTick() <= 0) { // revert the clipping to original clippping
								// before the spawned object
			RegionClip.revertClipping(o.getX(), o.getY(), o.getHeight());
		}

		for (Player p : Server.getRegionManager().getPlayersForRenderedRegionsAllZ(o.getX(), o.getY(), o.getHeight())) {
			if (p == null) {
				continue;
			}

			final boolean withinDistance = Misc.isInsideRenderedArea(p, o.getX(), o.getY());
			if (withinDistance) {
				// c.sendMessage("Updated this object!");

				if (p.getZ() == o.getHeight()) {
					p.getPacketSender().addObjectPacket((o.getTick() > 0)
							? o.getNewObjectId()
									: o.getOriginalId(), o.getX(), o.getY(),
									o.getType(), o.getFace());
					if (((o.getNewObjectId() >= 6 && o.getNewObjectId() <= 12)
							|| (o.getNewObjectId() >= 38660
							&& o.getNewObjectId() <= 38668))
							&& o.getTick() > 0) {
						o.setOriginalId(-1);
					}
				} else {
					p.objectToRemove.add(o);
				}
			}
		}
	}
	
	//changes the object ID without altering the clipping of the object
	public static void transformObjectNoClippingChange(RSObject o, int newId, boolean updateDisplay) {
		GameObject gO = RegionClip.getGameObject(o.getNewObjectId(), o.getX(), o.getY(), o.getHeight());
		if (gO != null) {
			gO.setId(newId);
		}
		o.setNewObjectId(newId);
		
		if (updateDisplay)
			displayObject(o, newId);
	}
	
	public static void displayObject(RSObject o, final int id) {
		displayObject(o.getX(), o.getY(), o.getHeight(), o.getType(), o.getFace(), id);
	}
	
	public static void displayObject(int x, int y, int z, int type, int orientation, final int id) {
		for (Player p : Server.getRegionManager().getPlayersForRenderedRegionsAllZ(x, y, z)) {
			if (!p.disconnected) {
				if (Misc.isInsideRenderedArea(p, x, y))
					p.getPacketSender().addObjectPacket(id, x, y, type, orientation);
			}
		}
	}

}
