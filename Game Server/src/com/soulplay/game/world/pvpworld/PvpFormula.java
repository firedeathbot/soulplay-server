package com.soulplay.game.world.pvpworld;

import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.game.model.player.Player;

public class PvpFormula {

	public static void calculatePvpRange(Player player) {
		int wildLevel = 0;
		if (player.getY() > 3521) {
			int modY = player.getY() > 6400 ? player.getY() - 6400 : player.getY();
			wildLevel = (((modY - 3520) / 8) + 1);
		}
		player.pvpWorldAttackLevelRange = (int)(0.1 * player.combatLevel + wildLevel + 5);
	}
	
	public static boolean withinPvpRange(Player p1, Player p2, boolean resetAttack) {
		int combatDif1 = MeleeRequirements.getCombatDifference(p1.combatLevel,
				p2.combatLevel);
		if ((combatDif1 > p1.pvpWorldAttackLevelRange && combatDif1 > p2.pvpWorldAttackLevelRange)) {
			if (resetAttack) {
				p1.sendMessage(
						"Your combat level difference is too great to attack that player here.");
				p1.getCombat().resetPlayerAttack();
				p1.getMovement().resetWalkingQueue();
			}
			return false;
		}
		return true;
	}
}
