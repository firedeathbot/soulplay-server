package com.soulplay.game.world;

import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public enum Direction {
    
	/**
	 * The north-west direction.
	 */
	NORTH_WEST(-1, 1, 7, 0x12c0108, 0x12c0120, 0x12c0138),

	/**
	 * The north direction.
	 */
	NORTH(0, 1, 0, true, 0x12c0120),

	/**
	 * The north-east direction.
	 */
	NORTH_EAST(1, 1, 4, 0x12c0180, 0x12c0120, 0x12c01e0),

	/**
	 * The west direction.
	 */
	WEST(-1, 0, 3, true, 0x12c0108),

	/**
	 * The east direction.
	 */
	EAST(1, 0, 1, true, 0x12c0180),

	/**
	 * The south-west direction.
	 */
	SOUTH_WEST(-1, -1, 6, 0x12c0108, 0x12c0102, 0x12c010e),

	/**
	 * The south direction.
	 */
	SOUTH(0, -1, 2, true, 0x12c0102),

	/**
	 * The south-east direction.
	 */
	SOUTH_EAST(1, -1, 5, 0x12c0180, 0x12c0102, 0x12c0183);

	/**
	 * The amounts of steps on the x-axis.
	 */
	private final int stepX;

	/**
	 * The amounts of steps on the y-axis.
	 */
	private final int stepY;

	/**
	 * The integer value.
	 */
	private final int value;

	/**
	 * The traversal flags.
	 */
	private final int[] traversal;
	private final boolean pole;
	
	private static final Direction[] values = values();

	public static final Direction[] valuesNoDiagonal = { NORTH, EAST, SOUTH, WEST };

	private Direction(int stepX, int stepY, int value, int... traversal) {
		this(stepX, stepY, value, false, traversal);
	}
	
	/**
	 * Constructs a new {@code Direction} {@code Object}.
	 * @param stepX The x-offset to move a step.
	 * @param stepY The y-offset to move a step.
	 * @param value The direction value.
	 * @param traversal The traversal flags.
	 */
	private Direction(int stepX, int stepY, int value, boolean pole, int... traversal) {
		this.stepX = stepX;
		this.stepY = stepY;
		this.value = value;
		this.traversal = traversal;
		this.pole = pole;
	}

	/**
	 * Gets the direction.
	 * @param rotation The int value.
	 * @return The direction.
	 */
	public static Direction get(int rotation) {
		for (Direction dir : values) {
			if (dir.value == rotation) {
				return dir;
			}
		}
		throw new IllegalArgumentException("Invalid direction value - " + rotation);
	}
	
	public static Direction getNonDiagonal(int rotation) {
		rotation = rotation & 3;
		return valuesNoDiagonal[rotation];
	}

	/**
	 * Gets the walk point for a direction. <br> The point will be the offset to
	 * the location the node is facing.
	 * @param direction The direction.
	 * @return The point.
	 */
//	public static Point getWalkPoint(Direction direction) {
//		return new Point(direction.stepX, direction.stepY);
//	}

	/**
	 * Gets the direction.
	 * @param location The start location.
	 * @param l The end location.
	 * @return The direction.
	 */
	public static Direction getDirection(Location location, Location l) {
		return getDirection(l.getX() - location.getX(), l.getY() - location.getY());
	}

	/**
	 * Gets the direction for movement.
	 * @param diffX The difference between 2 x-coordinates.
	 * @param diffY The difference between 2 y-coordinates.
	 * @return The direction.
	 */
	public static Direction getDirection(int diffX, int diffY) {
		if (diffX < 0) {
			if (diffY < 0) {
				return SOUTH_WEST;
			} else if (diffY > 0) {
				return NORTH_WEST;
			}
			return WEST;
		} else if (diffX > 0) {
			if (diffY < 0) {
				return SOUTH_EAST;
			} else if (diffY > 0) {
				return NORTH_EAST;
			}
			return EAST;
		}
		if (diffY < 0) {
			return SOUTH;
		}
		return NORTH;
	}

	/**
	 * Gets the direction for the given walking flag.
	 * @param walkingFlag The walking flag.
	 * @param rotation The rotation.
	 * @return The direction, or null if the walk flag was 0.
	 */
	public static Direction forWalkFlag(int walkingFlag, int rotation) {
		if (rotation != 0) {
			walkingFlag = (walkingFlag << rotation & 0xf) + (walkingFlag >> 4 - rotation);
		}
		if (walkingFlag > 0) {
			if ((walkingFlag & 0x8) == 0) {
				return Direction.WEST;
			}
			if ((walkingFlag & 0x2) == 0) {
				return Direction.EAST;
			}
			if ((walkingFlag & 0x4) == 0) {
				return Direction.SOUTH;
			}
			if ((walkingFlag & 0x1) == 0) {
				return Direction.NORTH;
			}
		}
		return null;
	}

	/**
	 * Gets the opposite dir.
	 * @return the direction.
	 */
	public Direction getOpposite() {
		return Direction.get(toInteger() + 2 & 3);
	}

	/**
	 * Gets the most logical direction.
	 * @param from The start location.
	 * @param to The end location.
	 * @return The most logical direction.
	 */
	public static Direction getLogicalDirection(Location from, Location to) {
		int offsetX = Math.abs(to.getX() - from.getX());
		int offsetY = Math.abs(to.getY() - from.getY());
		if (offsetX > offsetY) {
			if (to.getX() > from.getX()) {
				return Direction.EAST;
			} else {
				return Direction.WEST;
			}
		} else if (to.getY() < from.getY()) {
			return Direction.SOUTH;
		}
		return Direction.NORTH;
	}

	/**
	 * Method used to go to clue the anme.
	 * @param direction the direction.
	 * @return the name.
	 */
	public String toName(Direction direction) {
		return direction.name().toLowerCase();
	}

	/**
	 * Method used to get the direction to an integer.
	 * @return the integer.
	 */
	public int toInteger() {
		return value;
	}

	/**
	 * Gets the stepX.
	 * @return The stepX.
	 */
	public int getStepX() {
		return stepX;
	}

	/**
	 * Gets the stepY.
	 * @return The stepY.
	 */
	public int getStepY() {
		return stepY;
	}

	/**
	 * Checks if traversal is permitted for this direction.
	 * @param l The location.
	 * @return {@code True} if so.
	 */
	public boolean canMove(Location l) {
		int flag = RegionClip.getClipping(l.getX(), l.getY(), l.getZ());
		for (int f : traversal) {
			if ((flag & f) != 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Gets the traversal.
	 * @return The traversal.
	 */
	public int[] getTraversal() {
		return traversal;
	}

	public static Direction getRandom() {
		return values[Misc.randomNoPlus(values.length)];
	}

	public static Direction getRandomNoDiagonal() {
		return valuesNoDiagonal[Misc.randomNoPlus(valuesNoDiagonal.length)];
	}

	public static Direction rotate(Direction direction) {
		switch (direction) {
			case NORTH:
				return EAST;
			case EAST:
				return SOUTH;
			case SOUTH:
				return WEST;
			case WEST:
				return NORTH;
			default:
				return NORTH;
		}
	}

	public static Direction normalize(Direction direction) {
		switch (direction) {
			case NORTH_EAST:
			case NORTH_WEST:
				return NORTH;
			case SOUTH_EAST:
			case SOUTH_WEST:
				return SOUTH;
			default:
				return direction; 
		}
	}

	public static Direction getLogicalDirectionForEntity(Player p, GameObject o) {
		return getLogicalDirectionForEntity(p, o.getX(), o.getY(), o.getSizeY());
	}
	
	public static Direction getLogicalDirectionForEntity(Player c, int otherX, int otherY, int otherSizeY) {
		int faceX = 0;
		int faceY = 0;
		if (c.getY() >= otherY && c.getY() <= otherY + otherSizeY - 1) {
			faceX = otherX;
			faceY = c.getY();
		} else {
			faceY = otherY;
			faceX = c.getX();
		}

		if (faceX > 0 && faceY > 0) {
			c.faceLocation(faceX, faceY);
			return Direction.getLogicalDirection(c.getCurrentLocation(), Location.create(faceX, faceY));
		}
		return Direction.NORTH;
	}

	public boolean isPole() {
		return pole;
	}
	
	public Direction clockwise() {
		return clockwise(false);
	}
	
	public Direction clockwise(boolean polesOnly) {
		return clockwise(this, polesOnly);
	}
	
	public Direction counterClockwise() {
		return counterClockwise(false);
	}
	
	public Direction counterClockwise(boolean polesOnly) {
		return counterClockwise(this, polesOnly);
	}
	
	public static Direction clockwise(Direction direction) {
		return clockwise(direction, false);
	}
	
	public static Direction clockwise(Direction direction, boolean polesOnly) {
		if (polesOnly) {
			switch (direction) {
				default:
				case NORTH:
				case NORTH_EAST:
					return EAST;
				case EAST:
				case SOUTH_EAST:
					return SOUTH;
				case SOUTH:
				case SOUTH_WEST:
					return WEST;
				case WEST:
				case NORTH_WEST:
					return NORTH;
			}
		}
		switch (direction) {
			default:
			case NORTH:
				return NORTH_EAST;
			case NORTH_EAST:
				return EAST;
			case EAST:
				return SOUTH_EAST;
			case SOUTH_EAST:
				return SOUTH;
			case SOUTH:
				return SOUTH_WEST;
			case SOUTH_WEST:
				return WEST;
			case WEST:
				return NORTH_WEST;
			case NORTH_WEST:
				return NORTH;
		}
	}
	
	public static Direction counterClockwise(Direction direction) {
		return counterClockwise(direction, false);
	}
	
	public static Direction counterClockwise(Direction direction, boolean polesOnly) {
		if (polesOnly) {
			switch (direction) {
				default:
				case NORTH:
				case NORTH_WEST:
					return WEST;
				case WEST:
				case SOUTH_WEST:
					return SOUTH;
				case SOUTH:
				case SOUTH_EAST:
					return EAST;
				case EAST:
				case NORTH_EAST:
					return NORTH;
			}
		}
		switch (direction) {
			default:
			case NORTH:
				return NORTH_WEST;
			case NORTH_WEST:
				return WEST;
			case WEST:
				return SOUTH_WEST;
			case SOUTH_WEST:
				return SOUTH;
			case SOUTH:
				return SOUTH_EAST;
			case SOUTH_EAST:
				return EAST;
			case EAST:
				return NORTH_EAST;
			case NORTH_EAST:
				return NORTH;
		}
	}

	public static Direction combine(Direction a, Direction b) {
		for (Direction dir : values) {
			if (!dir.isPole()) {
				Direction[] composite = dir.getComposite();
				if ((a.equals(composite[1]) && b.equals(composite[2]))
						|| (a.equals(composite[2]) && b.equals(composite[1]))) {
					return dir;
				}
			}
		}
		return null;
	}

	private Direction[] composite;
	
	public Direction[] getComposite() {
		final Direction[] storedComposite = composite;
		if (storedComposite != null) {
			return storedComposite;
		}
		Direction[] composite;
		switch (this) {
			case NORTH_WEST:
				composite = new Direction[]{this, WEST, NORTH};
				break;
			case NORTH_EAST:
				composite = new Direction[]{this, EAST, NORTH};
				break;
			case SOUTH_WEST:
				composite = new Direction[]{this, WEST, SOUTH};
				break;
			case SOUTH_EAST:
				composite = new Direction[]{this, EAST, SOUTH};
				break;
			default:
				composite = new Direction[]{this};
				break;
		}
		return this.composite = composite;
	}
	
	public Direction compositeX() {
		if (isPole()) throw new UnsupportedOperationException();
		return getComposite()[1];
	}
	
	public Direction compositeY() {
		if (isPole()) throw new UnsupportedOperationException();
		return getComposite()[2];
	}
	
}