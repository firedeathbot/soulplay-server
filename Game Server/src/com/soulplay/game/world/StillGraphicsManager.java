package com.soulplay.game.world;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;

/**
 * Handles still graphics
 *
 * @author Graham
 */
public class StillGraphicsManager {
	
	/**
	 * Nothing to load, as of yet.
	 */
	public StillGraphicsManager() {
	}
	
	public void createGfx(Location location, int gfxId, int gfxHeight, int pause) {
		createGfx(location.getX(), location.getY(), location.getZ(), gfxId, gfxHeight, pause);
	}
	
	/**
	 * NOTE. THIS FUNCTION ONLY TO BE USED IN STATIC MAPS
	 */
	public void createGfx(int x, int y, int z, int gfxId, int gfxHeight,
	                      int pause) {
		for (Player p : Server.getRegionManager().getLocalPlayersByLocationExt(x,
				y, z)) {
			if (p == null || !p.isActive || p.disconnected) {
				continue;
			}
			if (p.isBot()) {
				continue;
			}
//			if (p.getHomeOwnerId() > 0 &&) {
//				continue;
//			}
			
			p.getPacketSender().sendStillGraphics(gfxId, gfxHeight, y, x,
					pause);
		}
	}
	
	public void stillGraphics(Player curPlr, int absX, int absY,
	                          int gfxHeight, int id, int pause) {
		for (Player p : Server.getRegionManager().getLocalPlayers(curPlr)) {
			if (p == null || !p.isActive || p.disconnected) {
				continue;
			}
			if (p.isBot()) {
				continue;
			}
			if (curPlr.getHomeOwnerId() > 0 && curPlr.getHomeOwnerId() != p.getHomeOwnerId()) { // do not display gfx for POH players
				continue;
			}
			if (p == curPlr
					|| p.withinDistance(absX, absY)) {
				p.getPacketSender().sendStillGraphics(id, gfxHeight, absY,
						absX, pause);
			}
		}
	}
	
	public void createGfx(Location landLocation, int gfxID, int gfxHeight) {
		createGfx(landLocation, gfxID, gfxHeight, 0);
	}
	
	public void createGfx(Location landLocation, int gfxID) {
		createGfx(landLocation, gfxID, 0);
	}
	
}
