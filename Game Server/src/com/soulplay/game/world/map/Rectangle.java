package com.soulplay.game.world.map;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Rectangle {
	
	private Location[] areas;
	
	public Rectangle(Location southWest, Location northEast) {
		this.areas = new Location[] {southWest , northEast };
	}
	
	public static Rectangle create(int x, int y, int z, int size) {
		return new Rectangle(Location.create(x-size, y-size, z), Location.create(x+size, y+size, z));
	}
	
	public boolean inside(Location location) {
		if (areas[1].getX() < location.getX() || areas[0].getX() > location.getX())
			return false;
		
		if (areas[1].getY() < location.getY() || areas[0].getY() > location.getY())
			return false;
		
		return true;
	}
	
	public List<Location> getBorder(Location location) {
		List<Location> border = new ArrayList<>();
		if (areas[1].getX() - 1 <= location.getX() || areas[0].getX() + 1 >= location.getX())
			border.add(location);
		
		if (areas[1].getY() - 1 <= location.getY() || areas[0].getY() + 1 >= location.getY())
			border.add(location);
		return border;
	}
	
	public int getNumberOfPlayers(List<Player> players) {
		int count = 0;
		for (Player player : players) {
			if (inside(player.getCurrentLocation())) {
				count++;
			}
		}
		return count;
	}
	
	public Location getNorthEast() {
		return areas[1];
	}
	
	public Location getSouthWest() {
		return areas[0];
	}
	
	public Location getRandomTile() {
		int randomX = getSouthWest().getX() + Misc.random(getNorthEast().getX() - getSouthWest().getX());
		int randomY = getSouthWest().getY() + Misc.random(getNorthEast().getY() - getSouthWest().getY());
		return Location.create(randomX, randomY);
	}
}