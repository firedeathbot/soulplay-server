package com.soulplay.game.world.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.cache.ObjectDef;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class DynamicObjectManager {
	
	//TODO: maybe add reference to player list of some sort, to display for those players only
	
	private final Object POOP = new Object();
	
	private final Map<Long, GameObject> objects = new HashMap<>();
	
	public Map<Long, GameObject> getSpawnedObjects() {
		return objects;
	}
	
	// used for loading objects into a map that are never changed, original state
	private final Map<Long, GameObject> staticObjects = new HashMap<>();
	
	public Map<Long, GameObject> getStaticObjects() {
		return staticObjects;
	}

	private CycleEventContainer container = null;
	
	private final DynamicRegionClip clip;
	
	private ObjectDefVersion defsVersion = ObjectDefVersion.DEF_474;
	
	public DynamicObjectManager(DynamicRegionClip clip) {
		this.clip = clip;
		init();
	}
	
	private void init() {
		container = CycleEventHandler.getSingleton().addEvent(POOP, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				Iterator<Map.Entry<Long, GameObject>> itr = objects.entrySet().iterator();
				while (itr.hasNext()) {
					Map.Entry<Long, GameObject> entry = itr.next();
					GameObject o = entry.getValue();
					o.setTicks(o.getTicks()-1);
					if (o.getTicks() <= 0) {
						revertObject(o);
						itr.remove();
					}
				}
			}
		}, 1);
	}
	
	public void revertObject(GameObject o) {
		o.setActive(false);
		if (clip != null) { // add/remove clipping
			if (o.getRevertId() == -1) { // to delete
				clip.deleteObject(o);
			} else { // to add
				clip.addObject(o, getDefsVersion());
			}
		}
		
		displayObject(o, o.getRevertId());
	}
	
	public void revertAllObjects() {
		Iterator<Map.Entry<Long, GameObject>> itr = objects.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry<Long, GameObject> entry = itr.next();
			GameObject o = entry.getValue();
			o.setTicks(0);
			o.setActive(false);
			revertObject(o);
			itr.remove();
		}
	}
	
	//changes the object ID without altering the clipping of the object
	public void transformObjectNoClippingChange(GameObject o, int newId) {
		o.setId(newId);
		displayObject(o, newId);
	}
	
	public void displayObject(GameObject o, final int id) {
		for (Player p : Server.getRegionManager().getPlayersForRenderedRegionsAllZ(o.getX(), o.getY(), o.getZ())) {
			if (!p.disconnected) {
				if (regionLoaded(p, o.getLocation()))
					p.getPacketSender().addObjectPacket(id, o.getX(), o.getY(), o.getType(), o.getOrientation());
			}
		}
	}
	
	public void loadObjects(Player p) {
		for (Map.Entry<Long, GameObject> entry : objects.entrySet()) {
			GameObject o = entry.getValue();
			if (o.isActive() && o.getZ() == p.getZ() && regionLoaded(p, o.getLocation())) {
				p.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), o.getType(), o.getOrientation());
			}
		}
	}
	
	private boolean regionLoaded(Player c, Location l) {
		final int x = c.getRenderedMapRegionX(l.getX());
		final int y = c.getRenderedMapRegionY(l.getY());
		return x >= 0 && x < 104 && y >= 0 && y < 104;
	}
	
	public void removeObject(GameObject o, int ticks) {
		GameObject removed = GameObject.createTickObject(-1, o.getLocation(), o.getOrientation(), o.getType(), ticks, o.getId());
		removed.setDefs(getObjectDef(o.getId() == -1 ? o.getRevertId() : o.getId()));
		addObject(removed, o.getDef());
	}
	
	public void addObject(GameObject o) {
		o.setDefs(getObjectDef(o.getId() == -1 ? o.getRevertId() : o.getId()));
		addObject(o, o.getDef());
	}
	
	public void addObject(GameObject o, ObjectDef objectDef) {
		objects.put(o.getHashLong(), o);
		if (clip != null) { // add/remove clipping
			if (o.getId() == -1) { // to delete
				clip.deleteObject(o, objectDef);
			} else { // to add
				clip.addObject(o, getDefsVersion());
			}
		}
		
		for (Player p : Server.getRegionManager().getPlayersForRenderedRegionsAllZ(o.getX(), o.getY(), o.getZ())) {
			if (!p.disconnected) {
				if (regionLoaded(p, o.getLocation()))
					if (o.getZ() == p.getZ())
						p.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), o.getType(), o.getOrientation());
//					else
//						p.objectToRemove.add(o)
			}
		}
	}
	
	public GameObject getObject(int x, int y, int z) {
		return getObject(-1, x, y, z);
	}
	
	public GameObject getObject(int id, int x, int y, int z) { // if Id doesn't match, object doesn't exist
		GameObject o = getObject(id, x, y, z, Misc.OBJECT_SLOTS[10]);
		if (o != null)
			return o;
		
		for (int i = 0; i < 4; i++) {
			if (i == 2) i++;
			o = getObject(id, x, y, z, i);
			if (o != null)
				return o;
		}
		return null;
	}
	
	public GameObject getObject(int id, int x, int y, int z, int convertedType) { // ID not used XD
		final long key = GameObject.generateKey(x, y, z, convertedType);
		GameObject o = objects.get(key);
		if (o != null && o.isActive() && (id == -1 || id == o.getId()))
			return o;
		//spawned object not found so look for static objects
		o = staticObjects.get(key);
		if (o != null && o.isActive() && (id == -1 || id == o.getId()))
			return o;
		return null;
	}

	public void cleanup() {
		container.stop();
		objects.clear();
		staticObjects.clear();
	}

	public void clear() {
		objects.clear();
		staticObjects.clear();
	}

	public void stopEvent() {
		container.stop();
	}

	public ObjectDefVersion getDefsVersion() {
		return defsVersion;
	}

	public void setDefsVersion(ObjectDefVersion defsVersion) {
		this.defsVersion = defsVersion;
	}
	
	public ObjectDef getObjectDef(int id) {
		if (id > 0) {
			switch (defsVersion) {
			case DEF_667:
				return ObjectDef.forID667(id);
			case DEF_525:
				return ObjectDef.forID525(id);
			case DEF_474:
				return ObjectDef.getObjectDef(id);
			case OSRS_DEFS:
				return ObjectDef.forIDOSRS(id);
				
				default:
					return ObjectDef.getObjectDef(id);
			}
		}
		return ObjectDef.getObjectDef(0);
	}

}
