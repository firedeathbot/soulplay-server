package com.soulplay.game.world.map;

public enum InstanceInteractIndex {

	FIRST,
	SECOND,
	THIRD,
	FOURTH,
	FIFTH;

}
