package com.soulplay.game.world.map;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.RectangleZone;

public class InstanceZone extends RectangleZone {

	private int height;
	private InstanceManager manager;

	public InstanceZone(Rectangle rec, InstanceManager manager, int height) {
		super(rec);
		this.height = height;
		this.manager = manager;
		makeDynamicZone();
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return super.isInZoneCoordinates(p) && p.heightLevel == height;
	}

	@Override
	public void onEntry(Player p) {
		super.onEntry(p);
		manager.enter(p);
	}

	@Override
	public void onExit(Player p) {
		super.onExit(p);
		manager.leave(p, false);
	}

	@Override
	public void onLogout(Player player) {
		super.onLogout(player);
		manager.leave(player, true);
	}

}
