package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class RevenantDungeonZone extends Zone {

	@Override
	public void onChange(Player p) {
		
	}

	@Override
	public void onEntry(Player p) {
	}

	@Override
	public void onExit(Player p) {
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 9536 && p.getX() <= 9663 && p.getY() >= 10048 && p.getY() <= 10239;
	}

}
