package com.soulplay.game.world.map.travel.zone.addon;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;
import com.soulplay.game.world.map.travel.zone.Zone;

public class WildGodWarsDungeonZone extends RectangleZone {
	
	public static final Rectangle RECTANGLE = new Rectangle(Location.create(9408, 10112), Location.create(9471, 10175));
	
	public static final Zone ZONE_SAMPLE = new WildGodWarsDungeonZone(RECTANGLE);
	
	public static final Location CAVE_OUTSIDE = Location.create(3017, 3740, 0);
	public static final Location CAVE_INSIDE = Location.create(9465, 10159, 3);
	
	public static final WildGodWarsDungeonZone createDynamic() {
		return new WildGodWarsDungeonZone(RECTANGLE);
	}
	

	public WildGodWarsDungeonZone(Rectangle rec) {
		super(rec);
		makeDynamicZone();
	}
	
	@Override
	public void onExit(Player p) {
		super.onExit(p);
		p.inWildDynZone = false;
		p.inMultiDynZone = false;
		
	}
	
	@Override
	public void onEntry(Player p) {
		super.onEntry(p);
		p.inWildDynZone = true;
	}
	
	@Override
	public void onChange(Player p) {
		super.onChange(p);
	}
	
	@Override
	public Zone copy() {
		return new WildGodWarsDungeonZone(RECTANGLE);
	}
	
}