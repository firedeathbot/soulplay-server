package com.soulplay.game.world.map.travel;

import java.util.Iterator;
import com.soulplay.Config;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;
import com.soulplay.game.world.map.travel.zone.addon.DeepWildDungeon;
import com.soulplay.game.world.map.travel.zone.addon.WildGodWarsDungeonZone;
import com.soulplay.game.world.map.travel.zone.addon.ScorpiaZone;
import com.soulplay.game.world.pvpworld.PvpFormula;

public class ZoneManager {

	public static void applyZone(Player player) {
		if (Config.isPvpWorld()) {
			pvpZone(player);
		} else {
			otherPICrap(player);
			loopZones(player);
		}
		
	}
	
	/**
	 * Check if needing to add any zones to list upon login.
	 */
	public static void checkForAddonZones(Player player, Player playerToCheck) {
		if (ScorpiaZone.ZONE_SAMPLE.isInZoneCoordinates(playerToCheck)) {
			player.getZones().addDynamicZone(ScorpiaZone.createDynamic());
		}
		if (DeepWildDungeon.ZONE_SAMPLE.isInZoneCoordinates(playerToCheck)) {
			player.getZones().addDynamicZone(DeepWildDungeon.createDynamic());
		}
		if (WildGodWarsDungeonZone.ZONE_SAMPLE.isInZoneCoordinates(playerToCheck)) {
			player.getZones().addDynamicZone(WildGodWarsDungeonZone.createDynamic());
		}
	}
	
	private static void pvpZone(Player player) {
		if (!player.inPvpWorldSafe()) {
			player.getPA().walkableInterface(197);
			
			PvpFormula.calculatePvpRange(player);
			
			int min = Math.max(3, player.combatLevel - player.pvpWorldAttackLevelRange);
			int max = Math.min(126, player.combatLevel + player.pvpWorldAttackLevelRange);
			
			player.getPacketSender().sendFrame126("<col=ffb000>" + (min) + "-" + (max) + "", 199);
			
			player.getPacketSender().showOption(3, 0, "Attack");
		} else {
			if (player.safeTimer == 0) {
				player.getPA().walkableInterface(-1);
				player.getPacketSender().showOption(3, 0, "Null");
			}
		}
	}

	public static void leaveZones(Player p) {
		for (Zone zone : p.getZones().getZoneList()) {
			if (!zone.isInsideZone()) {
				continue;
			}

			zone.onLogout(p);
		}
	}

	private static void loopZones(Player p) {
		
		Iterator<Zone> itr = p.getZones().getZoneList().iterator();
		
		while (itr.hasNext()) {
			Zone zone = itr.next();
			if (zone.dontProcessNextTick()) {
				zone.dontProcessNextTick(false);
				continue;
			}

			zone.setInsideZone(zone.isInZoneCoordinates(p));
			
			if (zone.zoneChanged()) {
				zone.onChange(p);
				if (!zone.isInsideZone()) {
					zone.onExit(p);
					
					if (zone.isDynamicZone()) //clear dynamic zone once we exit it
						p.getZones().removeDynamicZone(zone);
					
				}
			}
		}
		
		applyMultiZone(p, true);
		
		Iterator<Zone> itr2 = p.getZones().getZoneList().iterator();

		//run entry code after exit code.
		while (itr2.hasNext()) {
			Zone zone = itr2.next();
			if (zone.dontProcessNextTick()) {
				zone.dontProcessNextTick(false);
				continue;
			}

			if (zone.hasZoneChanged()) {
				if (zone.isInsideZone())
					zone.onEntry(p);
				zone.resetHasZoneChanged();
			}
		}

		applyMultiZone(p, false);
		
		if (!p.getZones().removedZones.isEmpty()) {
			p.getZones().getZoneList().removeAll(p.getZones().removedZones);
			p.getZones().removedZones.clear();
		}
		
	}
	
	private static void applyMultiZone(Player p, boolean onExit) {
		Zone zone = p.getZones().getMultiZone();
		
		if (onExit) {
			
			zone.setInsideZone(zone.isInZoneCoordinates(p));

			if (zone.zoneChanged()) {
				zone.onChange(p);
				if (!zone.isInsideZone()) {
					zone.onExit(p);

					if (zone.isDynamicZone()) //clear dynamic zone once we exit it
						p.getZones().removeDynamicZone(zone);

				}
			}
		} else {
			if (zone.hasZoneChanged()) {
				if (zone.isInsideZone())
					zone.onEntry(p);
				zone.resetHasZoneChanged();
			}
		}
	}
	
	private static void otherPICrap(Player p) {
		
	}
	
	private static void sendArrowByZone(Player player) {
		if (player.arrowX > 0) {
			int x = player.getRenderedMapRegionX(player.arrowX);
			int y = player.getRenderedMapRegionY(player.arrowY);
			if (x < 0 || x >= 104 || y < 0 || y >= 104) {
				x = Math.max(0, Math.min(103, x));
				y = Math.max(0, Math.min(103, y));
				
				player.getPacketSender().createArrow(player.getMapRegionX()*8+x, player.getMapRegionY()*8+y, player.arrowZ, player.arrowP);
			} else {
				player.getPacketSender().createArrow(player.arrowX, player.arrowY, player.arrowZ, player.arrowP);
			}
			
		}
	}
	
	public static void startArrow(Player player, int x, int y, int height, int pos) {
		player.arrowX = x;
		player.arrowY = y;
		player.arrowZ = height;
		player.arrowP = pos;
		ZoneManager.sendArrowByZone(player);
	}
	
	public static void onRegionChangePacket(Player player) {
		sendArrowByZone(player);
	}
	
}
