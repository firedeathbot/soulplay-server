package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;

public class ChambersOfXericZone extends RectangleZone {

	public ChambersOfXericZone(Rectangle rec) {
		super(rec);
		makeDynamicZone();
	}

	@Override
	public void onExit(Player p) {
		super.onExit(p);
		if (p.raidsManager != null) {
			p.raidsManager.leaveRaid(p, false);
		}
	}
}
