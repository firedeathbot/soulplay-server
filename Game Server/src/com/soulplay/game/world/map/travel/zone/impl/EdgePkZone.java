package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.Zone;

public class EdgePkZone extends Zone {

	public static final Rectangle RECT = new Rectangle(Location.create(3062, 3522), Location.create(3128, 3559));
	
	@Override
	public void onChange(Player p) {
		
	}

	@Override
	public void onEntry(Player p) {
		//lower nex hp boost
		int maxHP = p.getSkills().getMaximumLifepoints();
		int brewHplol = 14;
		if (p.getSkills().getLevel(Skills.HITPOINTS) > maxHP+brewHplol)
			p.getSkills().setLevel(Skills.HITPOINTS, maxHP+brewHplol);
	}

	@Override
	public void onExit(Player p) {
		
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return RECT.inside(p.getCurrentLocation()); //return RSConstants.edgevilleWilderness(p.getX(), p.getY());
	}

}
