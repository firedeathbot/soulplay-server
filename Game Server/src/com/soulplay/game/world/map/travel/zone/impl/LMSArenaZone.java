package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class LMSArenaZone extends Zone {

	@Override
	public void onChange(Player p) {
		p.getItems().removeAllItems();
	}

	@Override
	public void onEntry(Player p) {
	}

	@Override
	public void onExit(Player p) {
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return RSConstants.inLMSArena(p.getX(), p.getY());
	}

}
