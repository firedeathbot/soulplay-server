package com.soulplay.game.world.map.travel.zone.addon;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;

public class EdgevilleSpawnSkillZone extends RectangleZone {

	private static final Rectangle rec = new Rectangle(Location.create(2950, 3481), Location.create(3396, 3977));

	public EdgevilleSpawnSkillZone() {
		super(rec);
		makeDynamicZone();
	}
	
	@Override
	public void onExit(Player p) {
		p.resetSpawnedSkills();
	}
	
	public static void assignZone(Player p) {
		p.getZones().addDynamicZone(new EdgevilleSpawnSkillZone());
	}
	
	public static boolean canChangeStats(Player p) {
		return rec.inside(p.getCurrentLocation());
	}

}
