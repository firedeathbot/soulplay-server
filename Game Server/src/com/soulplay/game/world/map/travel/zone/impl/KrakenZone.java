package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;
import com.soulplay.game.world.map.travel.zone.Zone;

public class KrakenZone extends RectangleZone {
	
	private static final Rectangle instancedBossRec = new Rectangle(Location.create(10080, 5795), Location.create(10110, 5822));
	
	private static final Rectangle publicRectangle = new Rectangle(Location.create(8670, 10022), Location.create(8691, 10046));
	
	public static final Zone ZONE1 = new KrakenZone(instancedBossRec);
	public static final Zone ZONE2 = new KrakenZone(publicRectangle);

	private KrakenZone(Rectangle rec) {
		super(rec);
		makeDynamicZone();
	}
	
	public static KrakenZone createPublicZone() {
		return new KrakenZone(publicRectangle);
	}
	
	public static KrakenZone createInstancedZone() {
		return new KrakenZone(instancedBossRec);
	}

	@Override
	public void onExit(Player p) {
		super.onExit(p);
		p.inMultiDynZone = false;
	}
	
	@Override
	public void onEntry(Player p) {
		super.onEntry(p);
		p.inMultiDynZone = true;
	}
	
	@Override
	public void onChange(Player p) {
		super.onChange(p);
		p.inMultiDynZone = true;
	}
}
