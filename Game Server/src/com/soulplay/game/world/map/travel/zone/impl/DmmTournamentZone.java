package com.soulplay.game.world.map.travel.zone.impl;

import java.util.Arrays;

import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.map.travel.zone.Zone;

public class DmmTournamentZone extends Zone {

	@Override
	public void onChange(Player p) {
		p.getPA().closeAllWindows();
	}

	@Override
	public void onEntry(Player p) {
		if (!p.disconnected && p.tournSettings != null && p.tournSettings.isSpawnGear()) {
			p.playerItems = Arrays.copyOf(p.tournInventory, p.playerItems.length);
			p.playerItemsN = Arrays.copyOf(p.tournInventoryN, p.playerItems.length);
			p.playerEquipment = Arrays.copyOf(p.tournEquip, p.playerEquipment.length);
			p.playerEquipmentN = Arrays.copyOf(p.tournEquipN, p.playerEquipment.length);
			p.runePouchItems = Arrays.copyOf(p.runePouchItemsSpawned, p.runePouchItems.length);
			p.runePouchItemsN = Arrays.copyOf(p.runePouchItemsSpawnedN, p.runePouchItemsN.length);
			p.getUpdateFlags().add(UpdateFlag.APPEARANCE);
			for (int i = 0, len = p.playerEquipment.length; i < len; i++)
				p.setUpdateEquipment(true, i);
			p.getItems().refreshWeapon();
			p.setUpdateInvInterface(3214);
		}
	}

	@Override
	public void onExit(Player p) {
		if (p.tournamentRoom != null) {
			p.tournamentRoom.removePlayerFromRoomReference(p);
		}
		if (p.hasSpawnedGear) {
			p.hasSpawnedGear = false;
			Tournament.clearAllItems(p);
		}
		
		Tournament.flushPlayerFromTournament(p, false);
		
		if (p.tournSettings != null)
			p.tournSettings = null;
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 9985 && p.getY() >= 8961 && p.getX() <= 10110 && p.getY() <= 9150;
	}

}
