package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;

public class InfernoZone extends RectangleZone {

	public InfernoZone(Rectangle rec) {
		super(rec);
		makeDynamicZone();
	}

	@Override
	public void onExit(Player p) {
		super.onExit(p);
		
		p.setDefaultDiscordStatus();

		if (p.infernoManager == null) {
			return;
		}

		p.infernoManager.cleanup();
	}
}
