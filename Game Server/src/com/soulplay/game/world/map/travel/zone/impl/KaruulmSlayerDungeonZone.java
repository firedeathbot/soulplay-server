package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class KaruulmSlayerDungeonZone extends Zone {

	@Override
	public void onChange(Player p) {
		
	}

	@Override
	public void onEntry(Player p) {
		//p.sendMessage("enter");
	}

	@Override
	public void onExit(Player p) {
		//p.sendMessage("exit");
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 7616 && p.getX() <= 7807 && p.getY() >= 10112 && p.getY() <= 10303 && !(p.getX() >= 7703 && p.getY() >= 10187 && p.getX() <= 7721 && p.getY() <= 10215 && p.getZ() == 0);
	}

}
