package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class ClwRedPortalZone extends Zone {

	@Override
	public void onChange(Player p) {
	}

	@Override
	public void onEntry(Player p) {
		p.getPA().walkableInterface(197);
		p.wildLevel = 20;
		p.getPacketSender().sendFrame126("<col=ffff00>Level: " + p.wildLevel, 199);
		p.getPacketSender().showOption(3, 0, "Attack");
	}

	@Override
	public void onExit(Player p) {
		p.getPA().walkableInterface(-1);
		p.getPacketSender().showOption(3, 0, "Null");
		p.setRedSkull(false);
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 2948 && p.getX() <= 3067 && p.getY() >= 5512 && p.getY() <= 5628;
	}

}
