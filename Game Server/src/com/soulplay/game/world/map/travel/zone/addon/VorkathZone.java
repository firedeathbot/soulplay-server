package com.soulplay.game.world.map.travel.zone.addon;

import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;
import com.soulplay.game.world.map.travel.zone.Zone;

public class VorkathZone extends RectangleZone {
	
	private static final Rectangle REC = new Rectangle(Location.create(8656, 4052), Location.create(8686, 4077));
	
	public static final Zone ZONE = new VorkathZone();

	public VorkathZone() {
		super(REC);
		makeDynamicZone();
	}

}
