package com.soulplay.game.world.map.travel.zone.impl;

import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class DmmLobbyZone extends Zone {

	@Override
	public void onChange(Player p) {
		p.myShopId = 0;
		if (Tournament.pre_settings.isSpawnGear()) {
			Tournament.clearAllItems(p);
		}
	}

	@Override
	public void onEntry(Player p) {
		p.getPA().walkableInterface(6673);
		Tournament.pre_settings.getPresetType().setPreset(p);
	}

	@Override
	public void onExit(Player p) {
		if (Tournament.pre_settings.isSpawnGear()) {
			Tournament.clearAllItems(p);
		}
		p.getPA().walkableInterface(-1);
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 2689 && p.getX() <= 2750 && p.getY() >= 5505 && p.getY() <= 5630;
	}

}
