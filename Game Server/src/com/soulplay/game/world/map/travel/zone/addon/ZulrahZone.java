package com.soulplay.game.world.map.travel.zone.addon;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;
import com.soulplay.game.world.map.travel.zone.Zone;

public class ZulrahZone extends RectangleZone {

	private static final Rectangle REC = new Rectangle(Location.create(8658, 3065), Location.create(8679, 3081));
	
	public static final Zone ZONE = new ZulrahZone();
	
	public ZulrahZone() {
		super(REC);
		makeDynamicZone();
	}
	
	@Override
	public void onExit(Player p) {
		if (p.zulrah != null) {
			p.zulrah.nullZulrah();
			p.zulrah = null;
		}
		//p.getPA().restorePlayer(false);
	}

}
