package com.soulplay.game.world.map.travel.zone;

import com.soulplay.game.model.player.Player;

public abstract class Zone {
	
	boolean insideZone;
	
	boolean lastState;
	
	boolean zoneChanged;
	
	boolean isDynamicZone;

	boolean dontProcessNextTick;

	public Zone() {
		
	}
	
	/**
	 * All zones are checked once a tick if changed
	 * 
	 * @return
	 */
	public boolean zoneChanged() {
		if (insideZone != lastState) {
			lastState = insideZone;
			zoneChanged = true;
			return true;
		}
		return false;
	}
	
	public void setInsideZone(boolean inside) {
		this.insideZone = inside;
	}
	
	public boolean isInsideZone() {
		return insideZone;
	}
	
	public boolean hasZoneChanged() {
		return zoneChanged;
	}
	
	public void resetHasZoneChanged() {
		this.zoneChanged = false;
	}
	
	public boolean isDynamicZone() {
		return this.isDynamicZone;
	}
	
	public void makeDynamicZone() {
		this.isDynamicZone = true;
	}

	public void makeStaticZone() {
		this.isDynamicZone = false;
	}

	public void dontProcessNextTick(boolean dontProcessNextTick) {
		this.dontProcessNextTick = dontProcessNextTick;
	}

	public boolean dontProcessNextTick() {
		return dontProcessNextTick;
	}

	/**
	 * run functions on entry or exit, doesn't matter
	 * @param p - player exiting/entering
	 */
	public abstract void onChange(Player p);
	
	/**
	 * Run functions on entry of the zone
	 * @param p - player
	 */
	public abstract void onEntry(Player p);
	
	/**
	 * when player exits, run code
	 * @param p - player
	 */
	public abstract void onExit(Player p);

	public void onLogout(Player player) {
		/* empty */
	}

	/**
	 * Run this once just to set the boolean for if player is inside the zone.
	 * @return - if player is inside the coordinates specified inside the object
	 */
	public abstract boolean isInZoneCoordinates(Player p);

	public Zone copy() {
		return this;
	}

	public boolean storeDeathItems() {
		return false;
	}

}
