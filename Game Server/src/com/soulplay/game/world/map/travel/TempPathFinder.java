package com.soulplay.game.world.map.travel;

import com.soulplay.content.player.skills.construction.ConstructionPathFinder;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;

public class TempPathFinder {
	
	public static boolean followSmartPath(Mob follower, Mob toFollow) {
		return followSmartPath(follower, toFollow, null);
	}
	
	public static boolean followSmartPath(Mob follower, Mob toFollow, Location ownerLoc) {
		if (follower.getCurrentLocation().getZ() != toFollow.getCurrentLocation().getZ())
			return false;
		Location walkTo = follower.getCurrentLocation().getClosestLocation(toFollow.getCurrentLocation(), follower.getDynamicRegionClip(), ownerLoc);
		if (!Location.equals(walkTo, follower.getFollowLocation())) {
			follower.setFollowLocation(walkTo);
			
			if (follower.isNpc()) {
				NPC n = follower.toNpc();
				if (follower.getDynamicRegionClip() != null) {
					int toFollowSize = 1;
					if (toFollow.isNpc()) {
						toFollowSize = toFollow.toNpc().getSize();
					}
					ConstructionPathFinder.findRoute(n, walkTo.getX(), walkTo.getY(), true, 1, 1, n.getDynamicRegionClip(), toFollowSize, toFollowSize, 0, 0, 0, 1);
				} else {
					PathFinder.findRoute(n, walkTo.getX(), walkTo.getY(), true, 1, 1);
				}
			}
		}
		return true;
	}
	
	public static boolean walkToLocationSmart(Mob entity, Location walkTo) {
		if (entity.isNpc()) {
			NPC n = entity.toNpc();
			if (entity.getDynamicRegionClip() != null) {
				int toFollowSize = 1;
				ConstructionPathFinder.findRoute(n, walkTo.getX(), walkTo.getY(), true, 1, 1, n.getDynamicRegionClip(), toFollowSize, toFollowSize, 0, 0, 0, 1);
			} else {
				PathFinder.findRoute(n, walkTo.getX(), walkTo.getY(), true, 1, 1);
			}
		}
		return true;
	}
	
}
