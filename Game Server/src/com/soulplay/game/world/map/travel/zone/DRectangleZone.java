package com.soulplay.game.world.map.travel.zone;

import java.util.Objects;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;

public class DRectangleZone extends RectangleZone {

	private final Rectangle rec2;

	public DRectangleZone(Rectangle rec1, Rectangle rec2) {
		super(rec1);
		this.rec2 = rec2;
	}

	@Override
	public void onChange(Player p) {
		/* empty */
	}

	@Override
	public void onEntry(Player p) {
		/* empty */
	}

	@Override
	public void onExit(Player p) {
		/* empty */
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		Location loc = p.getCurrentLocation();
		if (rec2 != null) {
			return getRectangle().inside(loc) || rec2.inside(loc);
		} else {
			return getRectangle().inside(loc);
		}
	}

	@Override
	public Zone copy() {
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getRectangle(), rec2);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DRectangleZone other = (DRectangleZone) obj;
		return Objects.equals(getRectangle(), other.getRectangle()) && Objects.equals(rec2, other.rec2);
	}

}
