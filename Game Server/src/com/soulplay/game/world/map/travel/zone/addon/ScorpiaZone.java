package com.soulplay.game.world.map.travel.zone.addon;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;
import com.soulplay.game.world.map.travel.zone.Zone;

public class ScorpiaZone extends RectangleZone {
	
	public static final Rectangle RECTANGLE = new Rectangle(Location.create(9617, 10327), Location.create(9650, 10354));
	public static final Zone ZONE_SAMPLE = new ScorpiaZone(RECTANGLE);
	
	public static final ScorpiaZone createDynamic() {
		return new ScorpiaZone(RECTANGLE);
	}
	
	public static final Location[] CREV = { Location.create(9633, 10332), Location.create(9632, 10351), Location.create(9643, 10351) };
	
	public static final Location[] CAVES = { Location.create(3232, 3937), Location.create(3232, 3951) };

	public ScorpiaZone(Rectangle rec) {
		super(rec);
		makeDynamicZone();
	}
	
	@Override
	public void onExit(Player p) {
		super.onExit(p);
		p.inMultiDynZone = false;
		p.inWildDynZone = false;
		
	}
	
	@Override
	public void onEntry(Player p) {
		super.onEntry(p);
		p.inMultiDynZone = true;
		p.inWildDynZone = true;
	}
	
	@Override
	public void onChange(Player p) {
		super.onChange(p);
		p.inMultiDynZone = true;
		p.inWildDynZone = true;
	}
	
	@Override
	public Zone copy() {
		return new ScorpiaZone(RECTANGLE);
	}
	
}
