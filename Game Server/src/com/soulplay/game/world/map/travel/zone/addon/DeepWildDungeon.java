package com.soulplay.game.world.map.travel.zone.addon;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.RectangleZone;
import com.soulplay.game.world.map.travel.zone.Zone;

public class DeepWildDungeon extends RectangleZone {
	
	public static final Rectangle RECTANGLE = new Rectangle(Location.create(9403, 10301), Location.create(9466, 10355));
	
	public static final Zone ZONE_SAMPLE = new DeepWildDungeon(RECTANGLE);
	
	public static final DeepWildDungeon createDynamic() {
		return new DeepWildDungeon(RECTANGLE);
	}
	
	public static final Location STAIRS_UNDER = Location.create(9633, 10332);
	
	public static final Location STAIRS_UPPER = Location.create(3232, 3937);

	public DeepWildDungeon(Rectangle rec) {
		super(rec);
		makeDynamicZone();
	}
	
	@Override
	public void onExit(Player p) {
		super.onExit(p);
		p.inWildDynZone = false;
		
	}
	
	@Override
	public void onEntry(Player p) {
		super.onEntry(p);
		p.inWildDynZone = true;
	}
	
	@Override
	public void onChange(Player p) {
		super.onChange(p);
		p.inWildDynZone = true;
	}
	
	@Override
	public Zone copy() {
		return new DeepWildDungeon(RECTANGLE);
	}
	
}
