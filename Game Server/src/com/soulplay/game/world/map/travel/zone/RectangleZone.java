package com.soulplay.game.world.map.travel.zone;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.Rectangle;

public class RectangleZone extends Zone {
	
	private final Rectangle rec;
	
	public RectangleZone(Rectangle rec) {
		this.rec = rec;
	}

	@Override
	public void onChange(Player p) {
	}

	@Override
	public void onEntry(Player p) {
	}

	@Override
	public void onExit(Player p) {
	}
	
	public Rectangle getRectangle() {
		return rec;
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return rec.inside(p.getCurrentLocation());
	}
	
	@Override
	public Zone copy() {
		Zone zone = new RectangleZone(rec);
		if (isDynamicZone())
			zone.makeDynamicZone();
		
		return zone;
	}
	
	@Override
	public int hashCode() {
		return rec.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RectangleZone) {
			RectangleZone rZ = (RectangleZone) obj;
			return rZ.rec == rec;
		}
		return super.equals(obj);
	}

}
