package com.soulplay.game.world.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.MapTools;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.instances.DynSingleInstanceManager;
import com.soulplay.game.world.map.travel.zone.Zone;

public class InstanceManager {

	public static Map<String, DynSingleInstanceManager> playerInstances = new HashMap<>();
	private final String name;
	private final Player player;
	private DynamicRegionClip clipping;
	private DynamicObjectManager objectManager;
	private List<NPC> npcs;
	private Location base;
	private int height;
	private Zone zone;
	private List<GroundItem> groundItems;

	public InstanceManager(String name, Player player) {
		this.name = name;
		this.player = player;
		this.clipping = new DynamicRegionClip();
		this.objectManager = new DynamicObjectManager(clipping);
		this.npcs = new ArrayList<>();
		this.height = calculateHeight(player);
		this.groundItems = new ArrayList<>();
		if (useDefaults()) {
			initDefaults();
		}
	}

	public String getName() {
		return name;
	}

	public Player getPlayer() {
		return player;
	}

	private void initDefaults() {
		this.objectManager.setDefsVersion(ObjectDefVersion.OSRS_DEFS);
	}

	public boolean useDefaults() {
		return true;
	}

	public NPC spawnNpc(int id, int x, int y, Direction face) {
		return spawnNpc(id, x, y, 0, face);
	}

	public NPC spawnNpc(int id, int x, int y) {
		return spawnNpc(id, x, y, 0, null);
	}

	public NPC spawnNpc(int id, int x, int y, int z) {
		return spawnNpc(id, x, y, z, null);
	}

	public NPC spawnNpc(int id, int x, int y, int z, Direction face) {
		if (alertForNull(npcs)) {
			return null;
		}

		Location loc = base(x, y, z);
		if (loc == null) {
			return null;
		}

		NPC npc = NPCHandler.spawnNpc(id, loc.getX(), loc.getY(), loc.getZ());
		initNpcDefaults(npc);
		if (face != null) {
			npc.faceDirection(face);
		}

		return npc;
	}

	public void initNpcDefaults(NPC npc) {
		npc.setInstanceManager(this);
		npc.setDynamicRegionClip(clipping);
		npcs.add(npc);
	}

	public GameObject spawnObject(int id, int x, int y, int rotation, int type) {
		return spawnObject(id, x, y, 0, rotation, type);
	}

	public GameObject spawnObject(int id, int x, int y, int z, int rotation, int type) {
		return spawnObject(id, x, y, z, rotation, type, Integer.MAX_VALUE);
	}

	public GameObject spawnObject(int id, int x, int y, int z, int rotation, int type, int ticks) {
		if (alertForNull(objectManager)) {
			return null;
		}

		Location loc = base(x, y, z);
		if (loc == null) {
			return null;
		}

		GameObject object = GameObject.createTickObject(id, loc, rotation, type, ticks, -1);
		objectManager.addObject(object);
		return object;
	}

	public Location base(int x, int y, int z) {
		return base(Location.create(x, y, z));
	}

	public Location base(int x, int y) {
		return base(Location.create(x, y));
	}

	public Location base(Location location) {
		if (alertForNull(base)) {
			return null;
		}

		return base.transform(location);
	}

	public Location base() {
		return base;
	}

	public void initPlayerDefaults(Player player) {
		player.setInstanceManager(this);
		player.setDynamicRegionClip(clipping);
		player.dynObjectManager = objectManager;
		player.extendedNpcRendering = extendNpcRendering();
		player.getZones().addDynamicZone(zone);
	}

	public void revertPlayerDefaults(Player player) {
		player.setInstanceManager(null);
		player.setDynamicRegionClip(null);
		player.dynObjectManager = null;
		player.extendedNpcRendering = false;
	}

	public boolean extendNpcRendering() {
		return true;
	}

	public void destroy(boolean hard) {
		if (npcs != null) {
			int length = npcs.size();
			if (length > 0) {
				for (int i = 0; i < length; i++) {
					NPC npc = npcs.get(i);
					npc.despawnNextTick = true;
				}

				npcs.clear();
			}
		}

		if (groundItems != null) {
			int length = groundItems.size();
			if (length > 0) {
				for (int i = 0; i < length; i++) {
					GroundItem groundItem = groundItems.get(i);
					groundItem.setRemoveTicks(2);
				}
			}
	
			groundItems.clear();
		}

		if (objectManager != null) {
			objectManager.clear();
		}

		if (clipping != null) {
			clipping.cleanup();
		}

		if (hard) {
			npcs = null;
			clipping = null;
			if (objectManager != null) {
				objectManager.stopEvent();
			}

			objectManager = null;

			if (player != null) {
				revertPlayerDefaults(player);
			}
		}
	}

	public void enter(Player player) {
		/* empty */
	}

	public void leave(Player player, boolean logout) {
		if (destroyOnExit()) {
			destroy(true);
		}

		if (logout) {
			Location logoutLocation = logoutLocation();
			if (logoutLocation != null) {
				player.getPA().movePlayer(logoutLocation);
			}
		}
	}

	public Location logoutLocation() {
		return null;
	}

	public boolean canTeleport(Player player) {
		return true;
	}

	public boolean destroyOnExit() {
		return true;
	}

	public boolean safeDeath() {
		return true;
	}

	public boolean isMulti() {
		return true;
	}

	public boolean isMinigame() {
		return false;
	}

	private int calculateHeight(Player player) {
		if (player == null) {
			//TODO unique instances if created by server
			return -1;
		}

		DynSingleInstanceManager heightManager = playerInstances.get(name);
		if (heightManager == null) {
			heightManager = new DynSingleInstanceManager();
			playerInstances.put(name, heightManager);
		}

		return heightManager.getOrCreateZ(player.mySQLIndex);
	}

	public int getHeight() {
		return height;
	}

	public void createStaticRegion(Location location, int mapWidth, int mapHeight) {
		base = location.getMapZoneBase(height);

		int baseX = base.getX();
		int baseY = base.getY();
		MapTools.copyStaticData(baseX, baseY, mapWidth, mapHeight, height, objectManager.getStaticObjects(), objectManager.getSpawnedObjects(), clipping, objectManager.getDefsVersion(), 0, 1, 2, 3);
		zone = new InstanceZone(new Rectangle(Location.create(baseX, baseY), Location.create(baseX + mapWidth, baseY + mapHeight)), this, height);
	}

	public void spawnBelow(Item item, NPC npc) {
		spawnBelow(item.getId(), item.getAmount(), npc);
	}

	public void spawnBelow(int itemId, int itemAmount, NPC npc) {
		spawnItem(itemId, itemAmount, npc.absX, npc.absY, true, "server");
	}

	public void spawnBelow(Item item, Player player) {
		spawnBelow(item.getId(), item.getAmount(), player);
	}

	public void spawnBelow(int itemId, int itemAmount, Player player) {
		spawnItem(itemId, itemAmount, player.absX, player.absY, true, player.getName());
	}

	public void spawnItem(Item item, int x, int y) {
		spawnItem(item.getId(), item.getAmount(), x, y);
	}

	public void spawnItem(int itemId, int itemAmount, int x, int y) {
		Location loc = base(x, y);
		if (loc == null) {
			return;
		}

		spawnItem(itemId, itemAmount, loc.getX(), loc.getY(), true, "server");
	}

	public void spawnItem(int itemId, int itemAmount, int x, int y, boolean visibleToAll, String owner) {
		if (alertForNull(groundItems)) {
			return;
		}

		GroundItem groundItem = new GroundItem(itemId, x, y, height, itemAmount, 0);
		groundItem.ownerName = owner;
		ItemHandler.createGroundItem(groundItem, visibleToAll);
		groundItem.setInMinigame(true);//ironman can pickup if visibleToAll is false
		addGroundItem(groundItem);
	}

	public void addGroundItem(GroundItem groundItem) {
		if (alertForNull(groundItems)) {
			return;
		}

		groundItems.add(groundItem);
	}

	private boolean alertForNull(Object object) {
		if (object == null) {
			//TODO change message, to say where it coming from, add name of instance etc
			System.err.println("Instance already been destroyed.");
			return true;
		}

		return false;
	}

	public Zone getZone() {
		return zone;
	}

	public boolean interact(Player player, Object interactingWith, InstanceInteractType type, InstanceInteractIndex index) {
		return false;
	}

	public boolean buttonClick(Player player, int buttonId, int componentId) {
		return false;
	}

	public void playerDeath(Player player) {
		/* empty */
	}

	public void npcDeath(NPC npc) {
		/* empty */
	}

	public boolean storeDeathItems() {
		return false;
	}

}
