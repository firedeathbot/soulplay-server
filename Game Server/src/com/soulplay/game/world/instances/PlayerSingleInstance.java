package com.soulplay.game.world.instances;

public class PlayerSingleInstance {

	private static final DynSingleInstanceManager instance = new DynSingleInstanceManager();
	
	public static int getOrCreateZ(int mysqlIndex) {
		return instance.getOrCreateZ(mysqlIndex);
	}
}
