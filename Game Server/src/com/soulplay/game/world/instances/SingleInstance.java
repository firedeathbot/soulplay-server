package com.soulplay.game.world.instances;

public class SingleInstance {
	
	private final int playerId;
	private final int height;
	
	public SingleInstance(int pId, int z) {
		this.playerId = pId;
		this.height = z;
	}

	public int getPlayerId() {
		return playerId;
	}

	public int getHeight() {
		return height;
	}

}
