package com.soulplay.game.world.instances;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;

public class DynSingleInstanceManager {

	private Int2IntMap map;
	private int availableHeight = 4;

	public DynSingleInstanceManager() {
		this.map = new Int2IntOpenHashMap();
		this.map.defaultReturnValue(-1);
	}

	public int getOrCreateZ(int uniqueID) {
		int z = map.get(uniqueID);
		if (z == -1) {
			return createNewInstance(uniqueID);
		}

		return z;
	}

	private int createNewInstance(int uniqueID) {
		int z = availableHeight;
		map.put(uniqueID, z);
		availableHeight += 4;
		return z;
	}

}