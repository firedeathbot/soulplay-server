package com.soulplay.game.world;

import com.soulplay.cache.ObjectDef;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class GameObject {

	private /*final*/ int id;

	private /*final*/ int type;

	private Location location;

	private int orientation;
	
	private int sizeX;
	
	private int sizeY;
	
	private ObjectDef def;
	
	private boolean isActive;
	
	private int directionMask;
	
	private int ticks;
	private int revertId;// when ticks hit 0, display this as objectId
	private int ownerMID;
	private int originalDoorX, originalDoorY;

	public GameObject(int id, Location location, int orientation, int type) {
		this.id = id;
		this.type = type;
		this.location = location;
		this.orientation = orientation;
		setDefs(id);
		this.isActive = true;
	}
	
	public void setDefs(int id) {
		if (id > -1) {
			ObjectDef def;
			if (id >= ObjectDef.OSRS_START || location.getX() >= 6400 || RegionClip.isOsrsRegion(location.getRegionId())) {
				def = ObjectDef.forIDOSRS(id);
			} else
				def = ObjectDef.getObjectDef(id);

			if (def != null) {
				this.def = def;
				this.sizeX = (orientation & 0x1) == 1 ? def.sizeY : def.sizeX;
				this.sizeY = (orientation & 0x1) == 1 ? def.sizeX : def.sizeY;
			}
			this.directionMask = def.getWalkingFlag();
		}
	}
	
	public void setDefs(ObjectDef def) {
		if (def != null) {
			this.def = def;
			this.sizeX = (orientation & 0x1) == 1 ? def.sizeY : def.sizeX;
			this.sizeY = (orientation & 0x1) == 1 ? def.sizeX : def.sizeY;
			this.directionMask = def.getWalkingFlag();
		}
	}

	public int getId() {
		return id;
	}
	
	public void setId(int newId) {
		this.id = newId;
	}
	
	public int getType() {
		return type;
	}

	public Location getLocation() {
	    return location;
	}

	public void setLocation(Location location) {
	    this.location = location;
	}
	
	public int getX() {
		return this.location.getX();
	}
	
	public int getY() {
		return this.location.getY();
	}
	
	public int getZ() {
		return this.location.getZ();
	}
	
	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int orientation) {
	    this.orientation = orientation;
	    if (def != null) {
	    	this.sizeX = (orientation & 0x1) == 1 ? def.sizeY : def.sizeX;
	    	this.sizeY = (orientation & 0x1) == 1 ? def.sizeX : def.sizeY;
	    }
	}

	public int getSizeX() {
		return sizeX;
	}
	
	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	public int getSizeY() {
		return sizeY;
	}

	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	public ObjectDef getDef() {
		return def;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "GameObject [id=" + id + ", isActive=" + isActive + ", type=" + type + ", location=" + location + ", orientation=" + orientation
				+ ", sizeX=" + sizeX + ", sizeY=" + sizeY + ", def=" + def + "]";
	}

	public GameObject copy() {
		return new GameObject(this.id, this.location.copyNew(), this.orientation, this.type);
	}

	public GameObject copy(int id) {
		return new GameObject(id, this.location.copyNew(), this.orientation, this.type);
	}

	public int getHash() {
		return this.getType() << 26 | this.getLocation().getX() << 13 | this.getLocation().getY();
	}
	
	public long getHashLong() {
		return generateKey(this.getLocation().getX(), this.getLocation().getY(), this.getLocation().getZ(), Misc.OBJECT_SLOTS[this.getType()]);
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

	public int getRevertId() {
		return revertId;
	}

	private void setRevertId(int revertId) {
		this.revertId = revertId;
		if (this.id == -1) { // deleting object
			setDefs(revertId);
		}
	}
	
	public static GameObject createTickObject(int newId, Location location, int orientation, int type, int ticks, int revertId) {
		GameObject o = new GameObject(newId, location, orientation, type);
		
		o.setRevertId(revertId);
		o.setTicks(ticks);
		
		return o;
	}
	
	// convertedType is number that is grabbed from Misc.OBJECT_TYPES[type]
	public static long generateKey(int x, int y, int z, int convertedType) {
		return (long)z << 29 | (long)x << 16 | (long)y << 2 | (long)convertedType;
	}

	public int getOwnerMID() {
		return ownerMID;
	}

	public void setOwnerMID(int ownerMID) {
		this.ownerMID = ownerMID;
	}

	public int getOriginalDoorX() {
		return originalDoorX;
	}

	public void setOriginalDoorX(int originalDoorX) {
		this.originalDoorX = originalDoorX;
	}

	public int getOriginalDoorY() {
		return originalDoorY;
	}

	public void setOriginalDoorY(int originalDoorY) {
		this.originalDoorY = originalDoorY;
	}

	public int getDirectionMask() {
		return directionMask;
	}

	public void setDirectionMask(int directionMask) {
		this.directionMask = directionMask;
	}
	
	public void animate(int animId) {
		GlobalPackets.objectAnim(getX(), getY(), animId, getType(), getOrientation(), getZ());
	}

	public void animate(Animation anim) {
		GlobalPackets.objectAnim(getX(), getY(), anim.getId(), getType(), getOrientation(), getZ());
	}

}
