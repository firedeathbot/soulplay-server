package com.soulplay.game.world;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;

public final class World {
	
	private World() {
		
	}
	
	public static Optional<Client> search(String playerName) {
		return Arrays.stream(PlayerHandler.players).filter(Objects::nonNull).filter(it -> it.getNameSmart().equalsIgnoreCase(playerName)).map(Client.class::cast).findFirst();
	}

	public static Optional<Client> search(int mysqlIndex) {
		return Arrays.stream(PlayerHandler.players).filter(Objects::nonNull).filter(it -> it.mySQLIndex == mysqlIndex).map(Client.class::cast).findFirst();
	}

}

