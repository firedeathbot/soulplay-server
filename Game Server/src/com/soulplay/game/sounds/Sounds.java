package com.soulplay.game.sounds;

/**
 * A class that contains identified osrs sound effect ids
 *
 * @author nshusa
 */
public class Sounds {

    public static final int HIGH_ALCH = 97;
    public static final int LOW_ALCH = 98;
    public static final int BIND = 101;
    public static final int EARTH_BLAST = 128;
    public static final int EARTH_BOLT = 130;
    public static final int EARTH_STRIKE = 132;
    public static final int ENTANGLE = 151;
    public static final int FIRE_BLAST = 155;
    public static final int FIRE_BOLT = 157;
    public static final int FIRE_STRIKE = 160;
    public static final int ANCIENT_MAGICS_ICE_SPELL = 171;
    public static final int ANCIENT_MAGICS_SHADOW_SPELL = 178;
    public static final int ANCIENT_MAGICS_SMOKE_SPELL = 183;
    public static final int MODERN_SPELL_TELEPORT = 200;
    public static final int STRANGE_SPELL = 202;
    public static final int WATER_BLAST = 207;
    public static final int WATER_BOLT = 209;
    public static final int WATER_STRIKE = 211;
    public static final int WIND_BLAST = 216;
    public static final int WIND_BOLD = 218;
    public static final int WIND_STRIKE = 220;
    public static final int WIND_WAVE = 222;
    public static final int MAGIC_SPELL_SPLASH = 227;
    public static final int PLAYER_SMALL_DAMAGE = 519;
    public static final int GOBLIN_TAKING_DAMAGE_CHANGED_TO_WTFWASTHAT = 522;
    public static final int TELETAB = 965;
    public static final int ROCK_CAKE = 1018;
    public static final int RANDOM_EVENT_POPUP = 1930;
    public static final int CLICKING_BUTTONS = 2266;
    public static final int EAT = 2393;
    public static final int EQUIP_HATCHET = 2229;
    public static final int EQUIP_GREAT_AXE = 2232;
    public static final int EQUIP_NON_WEAPON = 2238;
    public static final int EQUIP_PLATEBODY = 2239;
    public static final int EQUIP_PLATELEGS = 2242;
    public static final int EQUIP_DH_PLATELEGS = 2243;
    public static final int EQUIP_BOW = 2244;
    public static final int EQUIP_STAFF = 2247;
    public static final int EQUIP_SWORD_OR_DAGGER = 2248;
    public static final int EQUIP_WHIP = 2249;
    public static final int DRINK_POT = 2401;
    public static final int UNKNOWN_LMS_START = 2405;
    public static final int SWORD = 2500;
    public static final int DDS_SPEC = 2537;
    public static final int DRAGON_SCIMITAR_SPEC = 2540;
    public static final int DAGGER = 2549;
    public static final int COOK_FOOD = 2577;
    public static final int PICKUP_ITEM = 2582;
    public static final int LIGHT_LOG_SUCCESS = 2596;
    public static final int LIGHT_LOG = 2597;
    public static final int FLETCH_LOG = 2605;
    public static final int PRAYER_DEACTIVATE = 2663;
    public static final int PRAYER_FAIL = 2673;
    public static final int PRAYER_ALTAR = 2674;
    public static final int PRAYER_THICK_SKIN = 2690;
    public static final int PRAYER_BURST_OF_STRENGTH = 2688;
    public static final int PRAYER_CLARITY_OF_THOUGHT = 2664;
    public static final int PRAYER_SHARP_EYE = 2685;
    public static final int PRAYER_MYSTIC_WILL = 2670;
    public static final int PRAYER_ROCK_SKIN = 2684;
    public static final int PRAYER_SUPER_HUMAN_STRENGTH = 2689;
    public static final int PRAYER_IMPROVED_REFLEXES = 2662;
    public static final int PRAYER_RAPID_RESTORE = 2679;
    public static final int PRAYER_RAPID_HEAL = 2678;
    public static final int PRAYER_PROTECT_ITEM = 1982;
    public static final int PRAYER_HAWK_EYE = 2666;
    public static final int PRAYER_MYSTIC_LORE = 2668;
    public static final int PRAYER_STEEL_SKIN = 2687;
    public static final int PRAYER_ULTIMATE_STRENGTH = 2691;
    public static final int PRAYER_INCREDIBLE_REFLEX = 2667;
    public static final int PRAYER_PROTECT_FROM_MAGIC = 2675;
    public static final int PRAYER_PROTECT_FROM_MISSLES = 2677;
    public static final int PRAYER_PROTECT_FROM_MELEE = 2676;
    public static final int PRAYER_EAGLE_EYE = 2665;
    public static final int PRAYER_MYSTIC_MIGHT = 2669;
    public static final int PRAYER_RETRIBUTION = 2682;
    public static final int PRAYER_REDEMPTION = 2680;
    public static final int PRAYER_SMITE = 2686;
    public static final int BOW = 2693;
    public static final int WHIP_SPEC = 2713;
    public static final int WHIP = 2720;
    public static final int PICKPOCKET_STUN = 2727;
    public static final int MAKE_TREE_STUMP = 2734;
    public static final int CHOP_TREE = 2735;
    public static final int BURY_BONE = 2738;
    public static final int DROP_ITEM = 2739;
    public static final int SNARE = 3003;
    public static final int TIME_RUNNING_OUT = 3120;
    public static final int MINE_ROCK = 3220;
    public static final int PIETY = 3825;
    public static final int CHIVALRY = 3826;

    private Sounds() {

    }

}
