package com.soulplay.game.sounds;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.Player;

public class Music {
	
	public static final Map<Integer, Integer> regionToMusic = new HashMap<>();

    public static void load() {
    	/* empty */
    }

	public static void playMusic(Player player, int regionId) {
		Integer music = regionToMusic.get(regionId);
		if (music == null) {
			return;
		}
		
		player.getPacketSender().sendMusic(music);
	}
	
	static {
		regionToMusic.put(12850, 76);
		regionToMusic.put(14131, 380);
		regionToMusic.put(13618, 286);
		regionToMusic.put(13362, 47);
		regionToMusic.put(13106, 36);
		regionToMusic.put(12594, 327);
		regionToMusic.put(12338, 3);
		regionToMusic.put(12082, 35);
		regionToMusic.put(12339, 151);
		regionToMusic.put(12595, 163);
		regionToMusic.put(12851, 2);
		regionToMusic.put(13107, 123);
		regionToMusic.put(13619, 154);
		regionToMusic.put(13620, 245);
		regionToMusic.put(13108, 111);
		regionToMusic.put(12852, 106);
		regionToMusic.put(12596, 116);
		regionToMusic.put(12340, 333);
		regionToMusic.put(12084, 15);
		regionToMusic.put(12083, 49);
		regionToMusic.put(11827, 127);
		regionToMusic.put(1571, 107);
		regionToMusic.put(11828, 72);
		regionToMusic.put(11572, 186);
		regionToMusic.put(11573, 18);
		regionToMusic.put(11829, 54);
		regionToMusic.put(12341, 141);
		regionToMusic.put(12597, 175);
		regionToMusic.put(12853, 125);
		regionToMusic.put(13109, 157);
		regionToMusic.put(13365, 20);
		regionToMusic.put(13621, 84);
		regionToMusic.put(13622, 48);
		regionToMusic.put(13366, 93);
		regionToMusic.put(13110, 93);
		regionToMusic.put(12854, 177);
		regionToMusic.put(12598, 56);
		regionToMusic.put(12342, 98);
		regionToMusic.put(12086, 102);
		regionToMusic.put(11574, 77);
		regionToMusic.put(11318, 87);
		regionToMusic.put(11317, 119);
		regionToMusic.put(11061, 74); //Might not be correct
		regionToMusic.put(11062, 104);
		regionToMusic.put(10806, 7);
		regionToMusic.put(10805, 184);
		regionToMusic.put(10549, 60);
		regionToMusic.put(10550, 140);
		regionToMusic.put(10294, 109);
		regionToMusic.put(10293, 193);
		regionToMusic.put(10038, 32);
		regionToMusic.put(10039, 66);
		regionToMusic.put(10037, 82);
		regionToMusic.put(10036, 328);
		regionToMusic.put(10035, 5);
		regionToMusic.put(9779, 10);
		regionToMusic.put(10291, 191);
		regionToMusic.put(10547, 99);
		regionToMusic.put(10548, 81);
		regionToMusic.put(10292, 133);
		regionToMusic.put(10551, 20);
		regionToMusic.put(10807, 21);
		regionToMusic.put(11063, 104);
		regionToMusic.put(13617, 36);
		regionToMusic.put(13105, 50);
		regionToMusic.put(13361, 50);
		regionToMusic.put(12593, 64);
		regionToMusic.put(11825, 180);
		regionToMusic.put(11569, 92);
		regionToMusic.put(11313, 172);
		regionToMusic.put(11057, 55);
		regionToMusic.put(10801, 164);
		regionToMusic.put(10802, 115);
		regionToMusic.put(11058, 6);
		regionToMusic.put(11056, 58);
		regionToMusic.put(11312, 162);
		regionToMusic.put(11568, 479);
		regionToMusic.put(11055, 117);
		regionToMusic.put(11311, 165);
		regionToMusic.put(11567, 89);
		regionToMusic.put(11054, 114);
		regionToMusic.put(11310, 90);
		regionToMusic.put(11566, 94);
		regionToMusic.put(11053, 129);
		regionToMusic.put(11309, 172);
		regionToMusic.put(10290, 152);
		regionToMusic.put(10034, 24);
		regionToMusic.put(10033, 148);
		regionToMusic.put(10032, 83);
		regionToMusic.put(10288, 185);
		regionToMusic.put(9776, 317);
		regionToMusic.put(9520, 314);
		regionToMusic.put(9523, 10);

		//Wilderness
		regionToMusic.put(11831, 34);
		regionToMusic.put(12087, 435);
		regionToMusic.put(12343, 182);
		regionToMusic.put(12599, 113);
		regionToMusic.put(12855, 165);
		regionToMusic.put(13111, 121);
		regionToMusic.put(13367, 121);
		regionToMusic.put(11832, 435);
		regionToMusic.put(12088, 160);
		regionToMusic.put(12344, 8);
		regionToMusic.put(12600, 10);
		regionToMusic.put(12856, 337);
		regionToMusic.put(13112, 337);
		regionToMusic.put(13368, 179);
		regionToMusic.put(11833, 183);
		regionToMusic.put(12089, 66);
		regionToMusic.put(12345, 176);
		regionToMusic.put(12601, 476);
		regionToMusic.put(12857, 332);
		regionToMusic.put(13113, 326);
		regionToMusic.put(13369, 326);
		regionToMusic.put(11834, 43);
		regionToMusic.put(12346, 435);
		regionToMusic.put(13114, 14);
		regionToMusic.put(13370, 14);
		regionToMusic.put(11835, 37);
		regionToMusic.put(12091, 43); //not sure if this is correct
		regionToMusic.put(12347, 43); //not sure if this is correct
		regionToMusic.put(12603, 422);
		regionToMusic.put(12859, 422);
		regionToMusic.put(13115, 182);
		regionToMusic.put(13371, 182);
		regionToMusic.put(11836, 37);
		regionToMusic.put(12092, 5);
		regionToMusic.put(12348, 5);
		regionToMusic.put(12604, 332);
		regionToMusic.put(12860, 332);
		regionToMusic.put(13116, 331);
		regionToMusic.put(13372, 331);
		regionToMusic.put(11837, 52);
		regionToMusic.put(12349, 13);
		regionToMusic.put(12605, 106);
		regionToMusic.put(12861, 127);
	}

}
