package com.soulplay.game.sounds;

import com.soulplay.Config;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;

public class SoundManager {

    public static int getWearSound(Player player, int itemId) {
        if (itemId < 0 || itemId >= Config.ITEM_LIMIT) {
            return - 1;
        }

        ItemDefinition def = ItemDefinition.forId(itemId);

        if (def == null) {
        	return -1;
        }

		if (def.getSlot() != PlayerConstants.playerWeapon) {
			if (def.getName() == null) {
				return -1;
			}

			final String name = def.getName().toLowerCase();

			if (name.endsWith("platelegs")) {
				return name.startsWith("dharok") ? Sounds.EQUIP_DH_PLATELEGS : Sounds.EQUIP_PLATELEGS;
			} else if (name.endsWith("platebody")) {
				return Sounds.EQUIP_PLATEBODY;
			}

			return Sounds.EQUIP_NON_WEAPON;
		}

        final int weaponInterfaceId = player.sidebarInterfaceInfo.getOrDefault(0, -1);

        if (weaponInterfaceId == -1) {
            return -1;
        }

        //System.out.println("weaponInterface: " + weaponInterfaceId);

        switch(weaponInterfaceId) {

            case 328: // staff
                return Sounds.EQUIP_STAFF;

            case 1698: // hatchets
                return Sounds.EQUIP_HATCHET;

            case 1764:
                return Sounds.EQUIP_BOW;

            case 12290: // whips
                return Sounds.EQUIP_WHIP;

            case 5570:
            case 4718: // dh axe
                return Sounds.EQUIP_GREAT_AXE;

            case 2243: // sword or daggers
                return Sounds.EQUIP_SWORD_OR_DAGGER;

        }

        return Sounds.EQUIP_SWORD_OR_DAGGER;
    }

    public static int getWeaponAttackSound(Player player, int itemId) {

        final int weaponInterfaceId = player.sidebarInterfaceInfo.getOrDefault(0, -1);

        if (weaponInterfaceId == -1 || itemId < 0) {
            return -1;
        }

        ItemDefinition def = ItemDefinition.forId(itemId);

        if (def == null || def.getName() == null || def.getSlot() != PlayerConstants.playerWeapon) {
            return -1;
        }

        final String name = def.getName().toLowerCase();

        if (name.endsWith("whip")) {
            return Sounds.WHIP;
        } else if (name.contains("dagger")) {
            return Sounds.DAGGER;
        } else if (name.contains("sword") || name.endsWith("scimitar")) {
            return Sounds.SWORD;
        } else if (name.endsWith("shortbow")) {
            return Sounds.BOW;
        }

        return -1;
    }

}
