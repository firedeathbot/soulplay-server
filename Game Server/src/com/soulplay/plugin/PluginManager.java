package com.soulplay.plugin;

import com.soulplay.plugin.extension.PluginExtension;
import com.soulplay.util.LoggerUtils;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles how plugins are loaded/unloaded and accessed.
 *
 * @author nshusa
 */
public final class PluginManager {

    /**
     * The logger for this class
     */
    private static final Logger logger = LoggerUtils.getLogger(PluginManager.class);

    /**
     * The mapping for the plugin arguments
     */
    private static final Map<PluginKey, Plugin> mappings = new HashMap<>();

    /**
     * The collection of plugin classpaths
     */
    private static final Set<String> plugins = new HashSet<>();

    /**
     * There should only ever be 1 instance of plugin manager so don't allow it be created.
     */
    private PluginManager() {

    }

    /**
     * Scans the classpath for classes with a specific annotation, in this case the marker annotation "Marker".
     * If the annotation is found the plugin gets created.
     */
    public static void load() {
        new FastClasspathScanner().matchSubclassesOf(Plugin.class, clazz -> {
            try {
                if (!Modifier.isAbstract(clazz.getModifiers())) {
                    final Plugin plugin = clazz.getConstructor().newInstance();
                    plugins.add(plugin.getClass().getName());
                    plugin.onInit();
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, String.format("Error loading plugin=%s", clazz.getSimpleName()), ex);
            }
        }).scan();

        logger.info(String.format("Loaded: %d plugins and %d mappings.", plugins.size(), mappings.size()));
    }

    public static void reload() {
        mappings.clear();
        plugins.clear();
        load();
    }

    @SuppressWarnings("unchecked")
    public static <E extends PluginExtension> PluginResult<E> search(Class<E> extension, Object... keys) {
        final PluginKey<E> key = new PluginKey<>(extension, keys);
        final Optional<Plugin> optional = Optional.ofNullable(mappings.get(key));
        return optional.map(plugin -> new PluginResult<>((E) plugin)).orElseGet(() -> new PluginResult<>(null));
    }

    public static Map<PluginKey, Plugin> getMappings() {
        return mappings;
    }

    /**
     * Gets the amount of individual plugins that were loaded on startup.
     */
    public static int getPluginCount() {
        return plugins.size();
    }

}

