package com.soulplay.plugin.core.event.scheduled;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

public abstract class ScheduledEvent extends TimerTask {
    protected static final long ONCE_PER_DAY = 86400000L;
    private final Timer timer = new Timer();
    private final Date date;

    public ScheduledEvent(int hour, int minutes, int day) {
        this.date = this.createDate(hour, minutes, day);
        this.timer.scheduleAtFixedRate(this, this.date, 86400000L);
    }

    protected ScheduledEvent(int hour, int minutes, int day, long cycle) {
        this.date = this.createDate(hour, minutes, day);
        this.timer.scheduleAtFixedRate(this, this.date, cycle);
    }

    public ScheduledEvent(int hour, int minutes, int day, TimeUnit time, long cycle) {
        this.date = this.createDate(hour, minutes, day);
        this.timer.scheduleAtFixedRate(this, this.date, time.getTime() * cycle);
    }

    public ScheduledEvent(TimeUnit time, int cycle) {
        this.date = new Date();
        this.timer.scheduleAtFixedRate(this, 0L, time.getTime() * (long)cycle);
    }

    public ScheduledEvent(TimeUnit time, long delay) {
        this.date = new Date();
        this.timer.schedule(this, time.getTime() * delay);
    }

    private Date createDate(int hour, int minutes, int day) {
        Calendar calendar = new GregorianCalendar();
        calendar.add(5, day);
        Calendar result = new GregorianCalendar(calendar.get(1), calendar.get(2), calendar.get(5), hour, minutes);
        return result.getTime();
    }

    public Timer getTimer() {
        return this.timer;
    }

    public Date getDate() {
        return this.date;
    }
}
