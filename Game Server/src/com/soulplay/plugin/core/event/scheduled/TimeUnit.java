package com.soulplay.plugin.core.event.scheduled;

public enum TimeUnit {
    SECOND(1000L),
    MINUTE(60000L),
    HOUR(3600000L),
    DAY(86400000L);

    private final long ms;

    private TimeUnit(long ms) {
        this.ms = ms;
    }

    public long getTime() {
        return this.ms;
    }
}
