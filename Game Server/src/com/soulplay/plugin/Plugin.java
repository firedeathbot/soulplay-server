package com.soulplay.plugin;

import com.soulplay.plugin.extension.PluginExtension;

import java.util.Arrays;

/**
 * The base class that all plugins should extend. Extending this class
 * will make the plugin discoverable by the server. This class also
 * provides methods necessary to retrieve plugins efficiently.
 *
 * @author nshusa
 */
public class Plugin {

    /**
     * This method provides a way to find a plugin.
     */
    protected <T extends PluginExtension> void map(Class<T> extension, Object... bindings) {
        final PluginKey<T> key = new PluginKey<>(extension, bindings);

        if (PluginManager.getMappings().containsKey(key)) {
            throw new IllegalArgumentException(String.format("Plugin=%s argument=%s overwritten by plugin=%s.", this.getClass().getSimpleName(), Arrays.toString(key.getArguments()), PluginManager.getMappings().get(key).getClass().getSimpleName()));
        }

        PluginManager.getMappings().put(key, this);
    }

    /**
     * The method that's called after a plugin has been instantiated.
     */
    public void onInit() {

    }

}
