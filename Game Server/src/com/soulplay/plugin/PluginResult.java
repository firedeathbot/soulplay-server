package com.soulplay.plugin;
import com.soulplay.plugin.extension.PluginExtension;
import com.soulplay.util.LoggerUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a result retrieving a plugin based on input parameters.
 * This class is not necessary if you want to provide your own exception handling.
 * The purpose of this class is to be able to invoke a plugin and catch any exceptions that may occur.
 *
 * @author nshusa
 */
public final class PluginResult<E extends PluginExtension> {

    /**
     * The logger for this class.
     */
    private final Logger logger = LoggerUtils.getLogger(PluginResult.class);

    /**
     * The extension instance that was retrieved
     */
    public final E extension;

    /**
     * Creates a new {@link PluginResult}
     *
     * @param extension
     *      The extension class that was retrieved.
     */
    PluginResult(E extension) {
        this.extension = extension;
    }

    /**
     * Invokes the {@link Runnable} and provides exception handling.
     *
     * @param runnable
     *      The code that will be invoked
     *
     * @return {@code true} If this plugin was invoked, {@code false} if this plugin was not invoked.
     */
    public boolean invoke(Runnable runnable) {
        if (extension == null) {
            return false;
        }

        try {
            runnable.run();
            return true;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, String.format("[Plugin=%s] error=%s", extension.getClass().getSimpleName(), ex.getCause()), ex);
        }
        return false;
    }

}
