package com.soulplay.plugin.extension;

import com.soulplay.game.model.player.Player;

public interface ItemOnObjectExtension extends PluginExtension {

    default void onItemOnObject(Player player, int objectID, int objectX,
                                int objectY, int itemId, int itemSlot) {}

}
