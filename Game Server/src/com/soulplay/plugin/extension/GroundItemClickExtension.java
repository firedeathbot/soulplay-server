package com.soulplay.plugin.extension;

import com.soulplay.game.model.player.Player;

public interface GroundItemClickExtension extends PluginExtension {

	default void onGroundItemOption1(Player player, int itemId, int itemX, int itemY) {
	}

	default void onGroundItemOption2(Player player, int itemId, int itemX, int itemY) {
	}

	default void onGroundItemOption3(Player player, int itemId, int itemX, int itemY) {
	}

	default void onGroundItemOption4(Player player, int itemId, int itemX, int itemY) {
	}

	default void onGroundItemOption5(Player player, int itemId, int itemX, int itemY) {
	}

}