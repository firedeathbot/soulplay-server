package com.soulplay.plugin.extension;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

public interface ItemOnItemExtension extends PluginExtension {

    default void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
    }

}
