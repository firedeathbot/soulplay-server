package com.soulplay.plugin.extension;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.in.ItemAction2;

public interface ItemClickExtension extends PluginExtension {

	default void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
	}

	default void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		ItemAction2.wearItemCentral(player, itemId, itemSlot);
	}

	default void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
	}

	default void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
	}

	default void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init(((Client)player), item);
	}
}
