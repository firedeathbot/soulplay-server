package com.soulplay.plugin.extension;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;

public interface NpcClickExtension extends PluginExtension {

    default void onNpcOption1(Player player, NPC npc) {

    }

    default void onNpcOption2(Player player, NPC npc) {

    }

    default void onNpcOption3(Player player, NPC npc) {

    }

    default void onNpcOption4(Player player, NPC npc) {

    }

}
