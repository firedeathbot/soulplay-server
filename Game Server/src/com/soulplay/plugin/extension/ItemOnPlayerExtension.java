package com.soulplay.plugin.extension;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

public interface ItemOnPlayerExtension extends PluginExtension {

    default void onItemOnPlayer(Player player, Player other, Item item) {

    }

}
