package com.soulplay.plugin.extension;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;

public interface MagicOnObjectExtension extends PluginExtension {

    default void magicOnObject(Player player, int spellId, GameObject object) {

    }

}
