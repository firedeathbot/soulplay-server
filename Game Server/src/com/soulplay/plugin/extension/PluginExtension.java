package com.soulplay.plugin.extension;

/**
 * Represents an extension point for plugins. This is also called a hook.
 * Hooks allow you to tap into classes and change their behavior. Extend (interface)
 * or implement this interface if you need to provide additional behaviors to a plugin.
 *
 * @author nshusa
 */
public interface PluginExtension {

}
