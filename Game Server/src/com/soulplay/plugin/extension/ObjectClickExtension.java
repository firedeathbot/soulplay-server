package com.soulplay.plugin.extension;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;

public interface ObjectClickExtension extends PluginExtension {

    default void onObjectOption1(Player player, GameObject object) {

    }

    default void onObjectOption2(Player player, GameObject object) {

    }

    default void onObjectOption3(Player player, GameObject object) {

    }

    default void onObjectOption4(Player player, GameObject object) {

    }
    
    default void onObjectOption5(Player player, GameObject object) {

    }

}
