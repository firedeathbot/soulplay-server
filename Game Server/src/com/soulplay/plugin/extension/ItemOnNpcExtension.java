package com.soulplay.plugin.extension;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;

public interface ItemOnNpcExtension extends PluginExtension {

    void onItemOnNpc(Player player, NPC npc, int itemId, int slot);

}
