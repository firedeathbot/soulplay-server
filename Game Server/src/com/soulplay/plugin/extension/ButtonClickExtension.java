package com.soulplay.plugin.extension;

import com.soulplay.game.model.player.Player;

public interface ButtonClickExtension extends PluginExtension {

    default void onClick(Player player, int button, int component) { }

}
