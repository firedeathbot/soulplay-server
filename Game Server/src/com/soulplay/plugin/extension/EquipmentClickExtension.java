package com.soulplay.plugin.extension;

import com.soulplay.game.model.player.Player;

public interface EquipmentClickExtension extends PluginExtension {

	default void onRemoveClick(Player player, int itemId, int itemSlot, int interfaceId) { player.getItems().removeItem(itemId, itemSlot); }

	default void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) { player.getPA().useOperate(itemId); }

}