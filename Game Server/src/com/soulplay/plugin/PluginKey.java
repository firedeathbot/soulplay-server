package com.soulplay.plugin;

import com.soulplay.plugin.extension.PluginExtension;

import java.util.Objects;

/**
 * A wrapper class that wraps a plugins arguments with its extension type.
 * The extension type and arguments combined should create a unique key.
 *
 * @author nshusa
 */
public final class PluginKey<T extends PluginExtension> {

    /**
     * The extension class that this key is bound to.
     */
    private Class<T> extension;

    /**
     * The arguments that will uniquely identify this key.
     */
    private Object[] arguments;

    /**
     * Creates a new {@link PluginKey}.
     *
     * @param extension
     *      The class that provides a {@link Plugin} specific behaviors.
     *
     * @param arguments
     *     These are values that uniquely identify this key.
     */
    public PluginKey(Class<T> extension, Object... arguments) {
        this.extension = extension;
        this.arguments = arguments;
    }

    public void setExtension(Class<T> extension) {
        this.extension = extension;
    }

    public Class<T> getExtension() {
        return extension;
    }

    public Object[] getArguments() {
        return arguments;
    }

    @Override
    public int hashCode() {
        return Objects.hash(arguments);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PluginKey) {
            PluginKey argument = (PluginKey) obj;
            return (argument.extension == extension) && (argument.hashCode() == hashCode());
        }
        return false;
    }

}
