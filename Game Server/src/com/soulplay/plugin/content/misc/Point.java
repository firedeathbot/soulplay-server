package com.soulplay.plugin.content.misc;

public class Point {
    private final String id;
    private int totalValue;

    protected Point(String id) {
        this.id = id;
        PointHandler.addPointSystem(this);
    }

    public boolean add(int value) {
        this.totalValue += value;
        return true;
    }

    public boolean remove(int value) {
        if (this.totalValue != 0 && this.totalValue - value >= 0) {
            this.totalValue -= value;
            return true;
        } else {
            return false;
        }
    }

    public int getTotalValue() {
        return this.totalValue;
    }

    public String getId() {
        return this.id;
    }
}
