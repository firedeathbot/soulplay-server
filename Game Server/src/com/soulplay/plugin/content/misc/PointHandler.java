package com.soulplay.plugin.content.misc;

import java.util.HashMap;
import java.util.Map;

public class PointHandler {
    private static Map<String, Point> points = new HashMap();

    public PointHandler() {
    }

    public static Point getPointForName(String id) {
        if (points.containsKey(id)) {
            return (Point)points.get(id);
        } else {
            throw new NullPointerException("No such id.");
        }
    }

    public static boolean addPointSystem(Point point) {
        if (points.containsKey(point.getId())) {
            throw new IllegalArgumentException("Duplicate Id.");
        } else {
            return true;
        }
    }
}
