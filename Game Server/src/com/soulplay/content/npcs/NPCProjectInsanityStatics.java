package com.soulplay.content.npcs;

import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.npcs.combat.NPCCombatData;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.summoning.SummoningData;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.attacks.NPCAttackDefinition;
import com.soulplay.game.model.npc.attacks.NPCAttackLoader;

public class NPCProjectInsanityStatics {
	
	public static boolean bossRoomNpcs(int npcType) {
		switch (npcType) {
			case 6252:
			case 6250:
			case 6248:
			case 6247:
			case 6208:
			case 6204:
			case 6203:
			case 6206:
			case 6265:
			case 6263:
			case 6261:
			case 6260:
			case 6225:
			case 6223:
			case 6227:
			case 6222:
			case 5666:
			case 3200:
			case 50:
			case 8349:
			case 8133:
				// case 13447:
			case 2881:
			case 2882:
			case 2883:
			case 4540: // b avatar
			case 8528: // nomad
				return true;
		}
		return false;
	}
	
	public static int setWalkingDistance(int npcId) {
		switch (npcId) {

			// revs wrong ids aswell
			/*case 6605:// goblin
			case 6998:// dragon
			case 13471:// pyrefiend
			case 6621:// icefiend
			case 13465:// imp
			case 6687:// cyclops
			case 13476:// hellhound
			case 6689:// demon
			case 6713:// ork
			case 13480:// knight
			case 6691:// knight
			case 6623:// vampire
			case 6625:// werewolf
			case 6608:// hobgoblin
				return 100;*/ 

		}
		return 3;
	}

	public static boolean SaraKC(int npcType) {
		switch (npcType) {
			case 6247:
			case 6248:
			case 6250:
			case 6254:
			case 6252:
			case 6257:
			case 6255:
			case 6256:
			case 6258:
				return true;
	
		}
	
		return false;
	}

	public static boolean switchesAttackers(int npcType) {
		switch (npcType) {
			case 50:
			case 8133:
			case 6261:
			case 6263:
			case 6265:
			case 6223:
			case 6225:
			case 6227:
			case 6248:
			case 6250:
			case 6252:
			case 2892:
			case 2894:
			case 8349: // tormented demons
				return true;
	
		}
	
		return false;
	}

	public static boolean ZammyKC(int npcType) {
		switch (npcType) {
			case 6203:
			case 6204:
			case 6206:
			case 6208:
			case 6219:
			case 6218:
			case 6212:
			case 3248:
			case 6220:
			case 6221:
				return true;
	
		}
	
		return false;
	}

	public static boolean ZarosKC(int npcType) {
		switch (npcType) {
			case 13458:
			case 13459:
			case 13457:
			case 13456:
			case 13447:
			case 13451:
			case 13453:
			case 13454:
				return true;
	
		}
	
		return false;
	}

	public static boolean ArmadylKC(int npcType) {
		switch (npcType) {
			case 6222:
			case 6223:
			case 6225:
			case 6230:
			case 6239: // Aviansie
			case 6227:
			case 6232:
			case 6229:
			case 6233:
			case 6231:
				return true;
	
		}
	
		return false;
	}

	public static boolean BandosKC(int npcType) {
		switch (npcType) {
			case 6260:
			case 6261:
			case 6263:
			case 6265:
			case 6277:
			case 6269:
			case 6270:
			case 3247:
			case 6276:
			case 6272:
			case 6274:
			case 6278:
				return true;
	
		}
	
		return false;
	}

	public static int followDistance(NPC npc) {
	
		if (npc.isPestControlNPC()) {
			return 100;
		}
	
		if (npc.isBarbAssaultNpc()/*BarbarianAssault.isPenanceFighter(i)
				|| BarbarianAssault.isPenanceRanger(i)*/) {
			return 200;
		}
	
		int npcId = npc.npcType;

		switch (npcId) {
		
			case 2045: // snakelings for zulrah
				return 40;
	
			case BarbarianAssault.PENANCE_HEALER:
				return 200;
	
			case 6247:
			case 6248:
				return 8;
			case 2883:
				return 4;
			case 2881:
			case 2882:
				return 1;
	
			case 8349:
			case 8350:
			case 8351: // tormented demons
				return 7;
	
			case 6260:
			case 6261:
			case 6223:
			case 6225:
			case 6227:
			case 6203:
			case 6204:
			case 6206:
			case 6208:
			case 6250:
			case 6252:
			case 6263:
			case 8133:
			case 6265:
			case 8528:
			case 8529:
				return 15;
			case 3247:
			case 6270:
			case 6219:
			case 6255:
			case 6229:
			case 6277:
			case 6233:
			case 6232:
			case 6218:
			case 6269:
			case 3248:
			case 6212:
			case 6220:
			case 6276:
			case 6256:
			case 6230:
			case 6239:
			case 6221:
			case 6231:
			case 6257:
			case 6278:
			case 6272:
			case 6274:
			case 6254:
			case 4291: // Cyclops
			case 4292: // Ice cyclops
			case 6258:
			case 172:
			case 174:
				return 7;
			case 50:
				return 18;
	
			// revs
			case 13469:// goblin
			case 13481:// dragon
			case 13471:// pyrefiend
			case 6621:// icefiend
			case 13465:// imp
			case 13475:// cyclops
			case 13476:// hellhound
			case 13477:// demon
			case 13478:// ork
			case 13480:// knight
			case 13479:// dark beast
			case 13473:// vampire
			case 13474:// werewolf
			case 13472:// hobgoblin
				return 4;
	
			// thzaar caves npcs
			case 2745:
			case 2743:
			case 2741:
			case 2630:
			case 2627:
			case 2631:
				return 65;
	
			case 5235:// penance rangers
			case 5236:
			case 5237:
				return 150;
	
			// nex
			case 13447:
				return 35;
	
		}
		if (npc.getSize() == 5)
			return 2;
		if (npc.getSize() >= 2 && npc.getSize() <= 4)
			return 1;
		return 0;
	
	}

	public static boolean setNotAggressive(int id) {
	
		if (!NPCCombatData.retaliates(id)) {
			return true;
		}
		if (SummoningData.isSummonNpc(id)) {
			return true;
		}
		switch (id) {
			// non aggresive npcs
			case 8777: // dwarf hand cannoneers
			case 1265:
			case 1267: // rock crabs
			case 8142:
			case 8143:
			case 8144:
			case 8145: // zombies halloween event
			case 89: // unicorns
			case 3348:
			case 19:
			case 3349:
			case 608:
			case 3350: // white knights
			case 5529: // yaks
			case 201: // jailer
			case 2607: // tzhaar
			case 7344: // summoning golem
			case 3374: // max
				return true;
		}
	
		return false;
	}
	
	public static boolean walkUpForHit(NPC n) {
		NPCAttackDefinition attackDef = NPCAttackLoader.get(n.npcType);
		if (attackDef != null && !attackDef.hasMeleeAttack()) {
			// Misc.println("walk up to hit is fsalse");
			return false;
		}
		
		Boss boss = n.setOrGetBoss();
		if (boss != null) {
			return boss.followDistance() <= 0;
		}
		
		if (n.npcType >= 10560 && n.npcType <= 10603) {
			return false;
		}
		//dungeoneering npcs
		if (n.npcType >= 10791 && n.npcType <= 10796 || n.npcType >= 10610 && n.npcType <= 10618 || n.npcType >= 10212 && n.npcType <= 10218 
				|| n.npcType >= 10831 && n.npcType <= 10842 || n.npcType >= 10460 && n.npcType <= 10468 || n.npcType >= 10670 && n.npcType <= 10693
				|| n.npcType >= 10375 && n.npcType <= 10385 || n.npcType == 10705 || n.npcType == 10704 || n.npcType == 10701 || n.npcType == 10697) {
			return false;
		}
		//dungeoneering bosses
		if (n.npcType >= 9965 && n.npcType <= 10021 || n.npcType >= 10073 && n.npcType <= 10106) {
			return false;
		}

		switch (n.npcType) {
			case 3200: // chaos elemental
			case 7151: // demonic gorillas
			case 1234: // any npc
			case 8133: // corp
			case 8776: // chaos dwarf
			case 8777: // chaos dwarf
			case 2743: // jad npcs? ket-zek
			case 2744: // jad npcs? ket-zek
			case 2745: // jad boss
			case 4540: // bandos avatar
			case 8528: // nomad
			case 8529: // nomad clone
			case 6090: // wildyworm does not need to walkup to hit
			case 5247: // pest queen
			case 5235: // penance rangers
			case 5236: // penance whatever
			case 5237: // penance anything
			case 2045: // snakeling in zulrah
			case 2499: // broodoo victim
			case 2501: // broodoo victim
			case 2503: // broodoo victim
			case 13460: //hati wolf
				return false;

			default:
				return true;
		}
	}
	
	public static void setRevenantStats(NPC npc) {
		switch(npc.npcType) {
		case 13465:
		case 13469:
		case 13471:
		case 13472:
		case 13473:
		case 13474:
		case 13475:
		case 13476:
		case 13477:
		case 13478:
		case 13479:
		case 13480:
		case 13481:
			//npc.prayerReductionMagic = npc.prayerReductionMelee = npc.prayerReductionRanged = 0.20;//removed cuz i cannot do damage reduction with bracelet yet properly
			break;
		}
	}
	
}
