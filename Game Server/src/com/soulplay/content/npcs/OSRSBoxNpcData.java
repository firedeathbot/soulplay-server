package com.soulplay.content.npcs;

import java.util.List;

public class OSRSBoxNpcData {

	public int id;
	public String name;
	// public boolean incomplete;
	// public boolean members;
	// public String release_date;
	public int combat_level;
	// public int size;
	public int hitpoints;
	public int max_hit;
	public List<String> attack_type;
	public int attack_speed;
	public boolean aggressive;
	public boolean poisonous;
	public boolean immune_poison;
	public boolean immune_venom;
	// public List<String> attributes;
	// public List<String> category;
	// public boolean slayer_monster;
	// public int slayer_level;
	// public double slayer_xp;
	// public List<String> slayer_masters;
	// public boolean duplicate;
	// public String examine;
	// public String icon;
	// public String wiki_name;
	// public String wiki_url;
	public int attack_level;
	public int strength_level;
	public int defence_level;
	public int magic_level;
	public int ranged_level;
	public int attack_stab;
	public int attack_slash;
	public int attack_crush;
	public int attack_magic;
	public int attack_ranged;
	public int defence_stab;
	public int defence_slash;
	public int defence_crush;
	public int defence_magic;
	public int defence_ranged;
	public int attack_accuracy;
	public int melee_strength;
	public int ranged_strength;
	public int magic_damage;

	public OSRSBoxNpcData copy() {
		OSRSBoxNpcData data = new OSRSBoxNpcData();
		data.id = id;
		data.name = name;
		//data.incomplete;
		//data.members;
		// data.release_date;
		data.combat_level = combat_level;
		// data.size;
		data.hitpoints = hitpoints;
		data.max_hit = max_hit;
		data.attack_type = attack_type;
		data.attack_speed = attack_speed;
		data.aggressive = aggressive;
		data.poisonous = poisonous;
		data.immune_poison = immune_poison;
		data.immune_venom = immune_venom;
		// public List<String> attributes;
		// public List<String> category;
		//data.slayer_monster;
		// data.slayer_level;
		// public double slayer_xp;
		// public List<String> slayer_masters;
		//data.duplicate;
		// data.examine;
		// data.icon;
		// data.wiki_name;
		// data.wiki_url;
		data.attack_level = attack_level;
		data.strength_level = strength_level;
		data.defence_level = defence_level;
		data.magic_level = magic_level;
		data.ranged_level = ranged_level;
		data.attack_stab = attack_stab;
		data.attack_slash = attack_slash;
		data.attack_crush = attack_crush;
		data.attack_magic = attack_magic;
		data.attack_ranged = attack_ranged;
		data.defence_stab = defence_stab;
		data.defence_slash = defence_slash;
		data.defence_crush = defence_crush;
		data.defence_magic = defence_magic;
		data.defence_ranged = defence_ranged;
		data.attack_accuracy = attack_accuracy;
		data.melee_strength = melee_strength;
		data.ranged_strength = ranged_strength;
		data.magic_damage = magic_damage;
		return data;
	}

	@Override
	public String toString() {
		return "OSRSBoxNpcData [id=" + id + ", name=" + name + ", combat_level=" + combat_level + ", hitpoints="
				+ hitpoints + ", max_hit=" + max_hit + ", attack_type=" + attack_type + ", attack_speed=" + attack_speed
				+ ", aggressive=" + aggressive + ", poisonous=" + poisonous + ", immune_poison=" + immune_poison
				+ ", immune_venom=" + immune_venom + ", attack_level=" + attack_level + ", strength_level="
				+ strength_level + ", defence_level=" + defence_level + ", magic_level=" + magic_level
				+ ", ranged_level=" + ranged_level + ", attack_stab=" + attack_stab + ", attack_slash=" + attack_slash
				+ ", attack_crush=" + attack_crush + ", attack_magic=" + attack_magic + ", attack_ranged="
				+ attack_ranged + ", defence_stab=" + defence_stab + ", defence_slash=" + defence_slash
				+ ", defence_crush=" + defence_crush + ", defence_magic=" + defence_magic + ", defence_ranged="
				+ defence_ranged + ", attack_accuracy=" + attack_accuracy + ", melee_strength=" + melee_strength
				+ ", ranged_strength=" + ranged_strength + ", magic_damage=" + magic_damage + "]";
	}

}
