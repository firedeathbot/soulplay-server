package com.soulplay.content.npcs;

import com.soulplay.util.Misc;

public class NpcStatsEntry {

	public static final int SSH_HIDE = 0;
	public static final int BARRICADE = 1;
	public static final int SSH_TURN_OFF = 2;
	public static final int HEX_HUNTER_EFFECT = 3;
	public static final NpcStatsEntry DEFAULT = new NpcStatsEntry(0, 0, 0, -1, 0);

	private final int attack, defence, maxHit, attackSpeed, hp;
	private final int properties;

	public NpcStatsEntry(int attack, int defence, int maxHit, int attackSpeed, int hp, int... properties) {
		this.attack = attack;
		this.defence = defence;
		this.maxHit = maxHit;
		this.attackSpeed = attackSpeed;
		this.hp = hp;
		this.properties = Misc.addBits(properties);
	}

	public NpcStatsEntry(int attack, int defence, int maxHit, int attackSpeed, int hp) {
		this.attack = attack;
		this.defence = defence;
		this.maxHit = maxHit;
		this.attackSpeed = attackSpeed;
		this.hp = hp;
		this.properties = 0;
	}

	public int getAttack() {
		return attack;
	}

	public int getDefence() {
		return defence;
	}

	public int getMaxHit() {
		return maxHit;
	}

	public int getAttackSpeed() {
		return attackSpeed;
	}

	public int getHp() {
		return hp;
	}

	public int getProperties() {
		return properties;
	}

}
