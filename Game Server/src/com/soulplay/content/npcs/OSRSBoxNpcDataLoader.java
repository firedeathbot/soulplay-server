package com.soulplay.content.npcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.cache.EntityDef;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPCDefinitions;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.configs.NpcConfigs;

public class OSRSBoxNpcDataLoader {

	private static final String OSRSBOX_FILE_NAME = "monsters-complete.json";
	private static final String GENERATED_FILE_NAME = "npcdefgenerated.json";
	private static final String CUSTOM_FILE_NAME = "npcStats.json";

	public static Map<Integer, OSRSBoxNpcData> loadToMap(String fileName) {
		String absPath = new File("./Data/cfg/" + fileName).getAbsolutePath();
		String content = new String(Misc.readFile(absPath));
		return GsonSave.gsond.fromJson(content, new TypeToken<Map<Integer, OSRSBoxNpcData>>() {}.getType());
	}

	public static void mapToDefinition(Map<Integer, OSRSBoxNpcData> dataMap, int idOffset) {
		int length = NPCDefinitions.getDefinitions().length;
		for (Entry<Integer, OSRSBoxNpcData> entry : dataMap.entrySet()) {
			OSRSBoxNpcData data = entry.getValue();
			int key = entry.getKey();
			int npcId = key + idOffset;
			if (npcId >= length) {
				//System.out.println("Couldn't load osrsbox stats for " + npcId + ", npc = " + data.name);
				continue;
			}

			NPCDefinitions def = NPCDefinitions.getDefinitions()[npcId];
			if (def == null) {
				def = NPCDefinitions.getDefinitions()[npcId] = new NPCDefinitions(npcId);
			} else {
				System.out.println("Stats for npc id=" + npcId + ", name=" + data.name + " already exists.");
			}

			def.setSkills(new Skills(null));
			def.setStats(new CombatStats());
			
			def.setNpcName(data.name);
			def.setNpcCombat(data.combat_level);
			def.setNpcHealth(data.hitpoints);
			def.setAggressive(data.aggressive);
			def.setPoisonous(data.poisonous);
			//def.setVenomous(set.getBoolean(7));
			//def.loadWeakness(set.getString(8));
			def.getSkills().setStaticLevel(Skills.ATTACK, data.attack_level);
			def.getSkills().setStaticLevel(Skills.STRENGTH, data.strength_level);
			def.getSkills().setStaticLevel(Skills.DEFENSE, data.defence_level);
			def.getSkills().setStaticLevel(Skills.MAGIC, data.magic_level);
			def.getSkills().setStaticLevel(Skills.RANGED, data.ranged_level);
			def.getStats().setStabBonus(data.attack_stab);
			def.getStats().setSlashBonus(data.attack_slash);
			def.getStats().setCrushBonus(data.attack_crush);
			def.getStats().setMagicAccBonus(data.attack_magic);
			def.getStats().setRangedAccBonus(data.attack_ranged);
			def.getStats().setStabDefBonus(data.defence_stab);
			def.getStats().setSlashDefBonus(data.defence_slash);
			def.getStats().setCrushDefBonus(data.defence_crush);
			def.getStats().setMagicDefBonus(data.defence_magic);
			def.getStats().setRangedDefBonus(data.defence_ranged);
			def.getStats().setStrengthBonus(data.melee_strength);
			def.getStats().setRangeStrengthBonus(data.ranged_strength);
			def.getStats().setMagicStrengthBonus(data.magic_damage);
			def.getStats().setAttackStrength(data.attack_accuracy);
			def.getStats().setAttackSpeed(data.attack_speed);
			def.setPoisonImmune(data.immune_poison);
			def.setVenomImmune(data.immune_venom);
			def.setMaxHit(data.max_hit);
		}
	}

	public static void load() {
		NPCDefinitions.init();

		mapToDefinition(loadToMap(OSRSBOX_FILE_NAME), EntityDef.OSRS_OFFSET);
		System.out.println("Set npc stats from OSRSBox.");
		mapToDefinition(loadToMap(GENERATED_FILE_NAME), 0);
		System.out.println("Set npc stats from generated file.");
		mapToDefinition(loadToMap(CUSTOM_FILE_NAME), 0);
		System.out.println("Set npc stats from custom file.");
	}

	private static void collectMaxHitsFromOldSpawn(Map<Integer, Integer> maxHits) {
		String FileName = "./Data/" + Config.cfgDir() + "/spawn-config.cfg";
		String line = "";
		String token = "";
		String token2 = "";
		String token2_2 = "";
		String[] token3 = new String[10];
		boolean EndOfFile = false;
		// int ReadMode = 0;
		BufferedReader characterfile = null;
		try {
			characterfile = new BufferedReader(new FileReader("./" + FileName));
		} catch (FileNotFoundException fileex) {
			Misc.println(FileName + ": file not found.");
			return;
		}
		try {
			line = characterfile.readLine();
		} catch (IOException ioexception) {
			Misc.println(FileName + ": error loading file.");
		}
		while (!EndOfFile && line != null) {
			line = line.trim();
			int spot = line.indexOf("=");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token2_2 = token2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token2_2 = token2_2.replace("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.equals("spawn")) {
					maxHits.put(Integer.parseInt(token3[0]), Integer.parseInt(token3[5]));
				}
			} else {
				if (line.equals("[ENDOFSPAWNLIST]")) {
					try {
						characterfile.close();
					} catch (IOException ioexception) {
					}
					// return true;
				}
			}
			try {
				line = characterfile.readLine();
			} catch (IOException ioexception1) {
				EndOfFile = true;
			}
		}
		try {
			characterfile.close();
		} catch (IOException ioexception) {
		}
		return;
	}

	public static void createNpcDef() {
		Map<Integer, OSRSBoxNpcData> dataMap = loadToMap(OSRSBOX_FILE_NAME);
		Map<Integer, Integer> maxHits = new HashMap<>();
		collectMaxHitsFromOldSpawn(maxHits);

		NPCDefinitions.definitions = new NPCDefinitions[Config.MAX_NPC_ID + 1];
		try {
			NpcConfigs.parse();
			NPCHandler.loadNPCList("./Data/" + Config.cfgDir() + "/npc.cfg");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Map<Integer, OSRSBoxNpcData> map = new HashMap<>();
		loop: for (int i = 0, length = NPCDefinitions.definitions.length; i < length; i++) {
			NPCDefinitions def = NPCDefinitions.definitions[i];
			if (def == null) continue;

			int combat = def.getNpcCombat();
			if (combat > 0 && (def.getStats() == null || def.getSkills() == null)) {
				for (OSRSBoxNpcData data : dataMap.values()) {
					if (combat == data.combat_level && def.getNpcName().equalsIgnoreCase(data.name)) {
						//System.out.println("Found npc matching osrs " +i + ", " + data.id + " - " + data.name);
						OSRSBoxNpcData newData = data.copy();
						newData.id = i;
						if (newData.max_hit == 0) {
							Integer oldMaxHit = maxHits.get(i);
							if (oldMaxHit != null) {
								//System.out.println("Found maxhit from spawns, npc " + data.name + ", new max " + oldMaxHit);
								newData.max_hit = oldMaxHit;
							}
						}
	        			map.put(i, newData);
						continue loop;
					}
				}
			}

			if (combat > 0) {
				for (OSRSBoxNpcData data : dataMap.values()) {
					// If npc combat and name matches, we can safely use osrs stats, because stats reflect combat lvl
					if (combat == data.combat_level && def.getNpcName().equalsIgnoreCase(data.name)) {
						//System.out.println("Found npc matching osrs " +i + ", " + data.id + " - " + data.name);
						OSRSBoxNpcData newData = data.copy();
						newData.id = i;
						if (newData.max_hit == 0) {
							Integer oldMaxHit = maxHits.get(i);
							if (oldMaxHit != null) {
								//System.out.println("Found maxhit from spawns, npc " + data.name + ", new max " + oldMaxHit);
								newData.max_hit = oldMaxHit;
							}
						}
	        			map.put(i, newData);
						continue loop;
					}
				}
			}
			
			if (combat > 0 && (def.getStats() == null || def.getSkills() == null)) {
			//	System.out.println("no stats for " + def.getNpcName());
			}

			OSRSBoxNpcData data = new OSRSBoxNpcData();
			
			data.name = def.getNpcName();
			data.combat_level = combat;
			data.hitpoints = def.getNpcHealth();
			data.aggressive = def.isAggressive();
			data.poisonous = def.isPoisonous();
			if (def.getSkills() != null) {
    			data.attack_level = def.getSkills().getStaticLevel(Skills.ATTACK);
    			data.strength_level = def.getSkills().getStaticLevel(Skills.STRENGTH);
    			data.defence_level = def.getSkills().getStaticLevel(Skills.DEFENSE);
    			data.magic_level = def.getSkills().getStaticLevel(Skills.MAGIC);
    			data.ranged_level = def.getSkills().getStaticLevel(Skills.RANGED);
			}
			if (def.getStats() != null) {
    			data.attack_stab = def.getStats().getStabBonus();
    			data.attack_slash = def.getStats().getSlashBonus();
    			data.attack_crush = def.getStats().getCrushBonus();
    			data.attack_magic = def.getStats().getMagicAccBonus();
    			data.attack_ranged = def.getStats().getRangedAccBonus();
    			data.defence_stab = def.getStats().getStabDefBonus();
    			data.defence_slash = def.getStats().getSlashDefBonus();
    			data.defence_crush = def.getStats().getCrushDefBonus();
    			data.defence_magic = def.getStats().getMagicDefBonus();
    			data.defence_ranged = def.getStats().getRangedDefBonus();
    			data.melee_strength = def.getStats().getStrengthBonus();
    			data.ranged_strength = def.getStats().getRangeStrengthBonus();
    			data.attack_accuracy = def.getStats().getAttackStr();
    			data.attack_speed = def.getStats().getAttackSpeed();
			}
			data.immune_poison = def.isPoisonImmune();
			data.immune_venom = def.isVenomImmune();

			map.put(i, data);
		}

		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		builder.registerTypeAdapter(Integer.class, new JsonSerializer<Integer>() {

			@Override
			public JsonElement serialize(Integer src, Type typeOfSrc, JsonSerializationContext context) {
				if (src != null && src != 0) {
					return new JsonPrimitive(src);
				}
				return null;
			}

		});

		builder.registerTypeAdapter(Boolean.class, new JsonSerializer<Boolean>() {

			@Override
			public JsonElement serialize(Boolean src, Type typeOfSrc, JsonSerializationContext context) {
				if (src != null && src) {
					return new JsonPrimitive(src);
				}
				return null;
			}

		});

		String txt = builder.create().toJson(map);
		try (BufferedWriter writer = new BufferedWriter(new FileWriter("./Data/cfg/" + GENERATED_FILE_NAME))) {
			writer.write(txt);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
