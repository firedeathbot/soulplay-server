package com.soulplay.content.npcs.combat;

import com.soulplay.Server;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.player.skills.summoning.SummoningData;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.attacks.Attack;
import com.soulplay.game.model.npc.attacks.NPCAttackLoader;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class NPCCombatData {

	public static Attack loadSpell(final Client player, final NPC npc) {
		if (npc == null) {
			return null;
		}
		if (player == null) {
			return null;
		}
		Attack attack = NPCAttackLoader.getNPCAttack(npc.npcType);

		if (npc.getSpecialChance() != -1) {
			Attack a = npc.doSpecial(player);
			if (a != null) {
				attack = a;
			}

		}

		if (attack != null) {
			npc.projectileId = attack.projectileId;
			npc.endGfx = attack.endGfx;
			if (npc.endGfx == 0)
				npc.endGfx = -1;
			npc.attackType = attack.attackType;
			npc.currentAttackMulti = attack.multi;
			// if (attack.startGfx > 0)
			// npc.gfx100(attack.startGfx);
			return attack;
		} else {
			npc.currentAttackMulti = false;
		}
		switch (npc.npcType) {
			case 8133:
				switch (Misc.random(2)) {
					case 0: // Melee
						npc.startAnimation(10058);
						npc.attackType = 0;
						break;
					case 1: // Magic (Single target)
						npc.startAnimation(10053);
						npc.projectileId = 1825;
						npc.endGfx = -1;
						npc.attackType = 2;
						break;
					case 2: // Magic (Splash (no intended target))
						npc.startAnimation(10053);
						npc.projectileId = 1824;
						npc.splash = true;
						npc.attackType = 5;
						final int[][] splashCoord = new int[5][2];
						splashCoord[0][0] = player.getCurrentLocation().getX();
						splashCoord[0][1] = player.getCurrentLocation().getY();
//						final int splashPlane = player.getZ();
						Server.getTaskScheduler().schedule(new Task(1) {

							int splashDelay = 8;

							@Override
							protected void execute() {
								if (player == null || npc == null || npc.isDead()
										|| player.isDead() || player.disconnected
										|| player.forceDisconnect) {
									this.stop();
									return;
								}
								if (!player.goodDistance(npc, 20)) {
									this.stop();
									return;
								}
								if (splashDelay > 0) {
									splashDelay--;
								}
								if (splashDelay == 4) {
									for (int i = 1; i < splashCoord.length; i++) {
										splashCoord[i][0] = splashCoord[0][0]
												+ (Misc.random(1) == 1
														? (-1 - Misc.random(2))
														: (1 + Misc.random(2)));
										splashCoord[i][1] = splashCoord[0][1]
												+ (Misc.random(1) == 1
														? (-1 - Misc.random(2))
														: (1 + Misc.random(2)));
//										GlobalPackets.createPlayersProjectile(
//												splashCoord[0][0],
//												splashCoord[0][1], splashPlane,
//												(splashCoord[0][1]
//														- splashCoord[i][1]),
//												(splashCoord[0][0]
//														- splashCoord[i][0]),
//												50, 100, 1824, 43, 31, 0, 65);
										
										Projectile proj = new Projectile(1824, Location.create(splashCoord[0][0],
												splashCoord[0][1], npc.getHeightLevel()), Location.create(splashCoord[i][0], splashCoord[i][1], npc.getHeightLevel()));
										proj.setStartHeight(43);
										proj.setEndHeight(31);
										proj.setStartDelay(65);
										proj.setLockon(0);
										proj.setEndDelay(100);
										GlobalPackets.createProjectile(proj);
										
									}
									for (Player p : Server.getRegionManager()
											.getLocalPlayers(npc)) {
										if (p == null) {
											continue;
										}
										Client c2 = (Client) p;
										if (c2.getCurrentLocation()
												.getX() == splashCoord[0][0]
														&& c2.getCurrentLocation()
														.getY() == splashCoord[0][1]) {
											c2.getCombat().appendHit(c2,
													5/* 50 */ + Misc
													.random(8/* 80 */),
													0, 2, false, null); // fix
										}
									}
								} else if (splashDelay == 0) {
									for (Player p : Server.getRegionManager()
											.getLocalPlayers(npc)) {
										if (p == null) {
											continue;
										}
										Client c2 = (Client) p;
										for (int coords = 1; coords < splashCoord.length; coords++) {
												if (c2.getCurrentLocation()
														.getX() == splashCoord[coords][0]
														&& c2.getCurrentLocation()
																.getY() == splashCoord[coords][1]) {
													c2.getCombat().appendHit(c2,
															5/* 50 */ + Misc
																	.random(8/* 80 */),
															0, 2, false, null); // fix
												}
												c2.getPA().stillGfx(1808,
														splashCoord[coords][0],
														splashCoord[coords][1],
														0, 0);
										}
									}
									this.stop();
								}
							}

						});
						break;
				}
				break;
			case 50:
				int r5 = 0;
				if (npc.getKillerId() > 0
						&& NPCHandler.goodDistance(npc.absX, npc.absY,
								PlayerHandler.players[npc.getKillerId()]
										.getCurrentLocation().getX(),
								PlayerHandler.players[npc.getKillerId()]
										.getCurrentLocation().getY(),
								2)) {
					r5 = Misc.random(5);
				} else {
					r5 = Misc.random(3);
				}
				if (r5 == 0) {
					npc.projectileId = 393; // red
					npc.attackType = 3;
				} else if (r5 == 1) {
					npc.projectileId = 394; // green
					npc.attackType = 2;
					// if (player.poisonTick == null) { //TODO: add poison
					// player.getPA().appendPoison(8);
					// }
				} else if (r5 == 2) {
					npc.projectileId = 395; // white
					npc.attackType = 2;
					if (player.applyFreeze(19))
						player.sendMessage("You have been Frozen!");
				} else if (r5 == 3) {
					npc.projectileId = 396; // blue
					npc.attackType = 2;
				} else if (r5 == 4) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				} else if (r5 == 5) {
					npc.projectileId = -1; // melee
					npc.attackType = 0;
				}
				break;
		}
		return null;
	}

	public static Attack loadSpell(final NPC attackNpc, final NPC npc) {
		if (npc == null) {
			return null;
		}
		if (attackNpc == null) {
			return null;
		}
		Attack attack = NPCAttackLoader.getNPCAttack(npc.npcType);

		if (attack != null) {
			npc.projectileId = attack.projectileId;
			if (attack.endGfx > 0)
				npc.endGfx = attack.endGfx;
			npc.attackType = attack.attackType;
			npc.currentAttackMulti = attack.multi;
			if (attack.startGfx > 0) {
				npc.startGraphic(Graphic.create(attack.startGfx, GraphicType.HIGH));
			}
			return attack;
		} else {
			npc.currentAttackMulti = false;
		}
		return null;
	}
	
	public static boolean setAsSafeSpotNPC(int npcId) {
		switch (npcId) {
		case 1234:
			return false;
		
		default:
			return true;
		}
	}

	public static boolean retaliates(int npcType) {
		if (npcType == 2012) { // if zulrah then don't face anyone
			return false;
		}
		if (npcType >= 2440 && npcType <= 2446) {
			return false;
		}
		if (npcType >= 6142 && npcType <= 6145) {
			return false;
		}
		if (npcType >= 1532 && npcType <= 1535) {
			return false;
		}
		// return npcType < 6142 || npcType > 6145
		// && !(npcType >= 2440 && npcType <= 2446);
		
		if (SummoningData.isSummonNpc(npcType)) {
			return false;
		}
	
		return true;
	}

	public static void setCombatDistanceRequired(NPC n) {
		if (NPCHandler.isDefiler(n.npcType)
				|| NPCHandler.isTorcher(n.npcType)) {
			n.getDistanceRules().setMagicDistance(12);
			n.getDistanceRules().setMeleeDistance(12);
			n.getDistanceRules().setRangeDistance(12);
		}
		
		if (n.getSize() >= 5) {
			n.getDistanceRules().setMeleeDistance(2);
		}
		
		switch (n.npcType) { // forget this dumb shit
	
			case BarbarianAssault.PENANCE_HEALER:
				n.getDistanceRules().setMagicDistance(1);
				n.getDistanceRules().setMeleeDistance(1);
				n.getDistanceRules().setRangeDistance(1);
				return;
				
			case 5235: // penance rangers
			case 5236: // penance whatever
			case 5237: // penance anything
				n.getDistanceRules().setMagicDistance(10);
				n.getDistanceRules().setMeleeDistance(10);
				n.getDistanceRules().setRangeDistance(10);
				return;
	
			case 53:// red dragon
			case 54:
			case 55:
			case 941:
			case 4677:
			case 4678:
			case 4679:
			case 4680:
			case 5362:
			case 5363:
			case 1590:
			case 1591:
			case 1592:
			case 3590:
			case 51:
			case 10770:
			case 10771:
			case 10772:
			case 10773:
			case 10774:
			case 10775:
			case 50:
			case 6247:
			case 13481:
			case 2011:
				n.getDistanceRules().setMeleeDistance(2);
				break;
			case 6090:
				n.getDistanceRules().setMagicDistance(10);
				n.getDistanceRules().setMeleeDistance(2);
				n.getDistanceRules().setRangeDistance(10);
				break;
				// things around dags
			case 2892:
			case 2894:
				n.getDistanceRules().setMagicDistance(10);
				n.getDistanceRules().setRangeDistance(10);
				break;
				
			case 3200: // chaos elemental hits far on all styles
				n.getDistanceRules().setMeleeDistance(10);
				n.getDistanceRules().setMagicDistance(10);
				n.getDistanceRules().setRangeDistance(10);
				break;
		}
	}
}
