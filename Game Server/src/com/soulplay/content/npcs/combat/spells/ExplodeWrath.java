package com.soulplay.content.npcs.combat.spells;

import java.util.Collection;

import com.soulplay.Server;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

public class ExplodeWrath {

	private static final Graphic EXPLODE_GFX = Graphic.create(2260, GraphicType.LOW);
	
	/**
	 * Magic damage explosion
	 *
	 * @param n
	 *            - npc that explodes
	 * @param distance
	 *            - distance from npc to damage players
	 */
	public static void explode(NPC n, int distance, int damageMinimum,
			int damageMax) {
		// gather players
		Collection<Player> tempPlayers = null;
		tempPlayers = Server.getRegionManager().getLocalPlayers(n);
		if (tempPlayers.size() > 0) {
			for (Player p : tempPlayers) {
				if (p == null || !p.canGetHit()) {
					continue;
				}
				if ((p.getX() >= n.getX() - distance)
						&& (p.getX() <= n.getX() + (n.getSize()-1) + distance)
						&& (p.getY() >= n.getY() - distance)
						&& (p.getY() <= n.getY() + (n.getSize()-1) + distance)) {
					p.startGraphic(EXPLODE_GFX);
					p.hitPlayer(damageMinimum + Misc.random(damageMax), 0, 2, 1);
				}
			}
		}
		n.setDead(true);
		tempPlayers.clear();
		tempPlayers = null;

	}

	/**
	 * Magic damage explosion
	 *
	 * @param n
	 *            - npc that explodes
	 * @param distance
	 *            - distance from npc to damage players
	 */
	public static void setExplodeTick(final NPC n, final int distance,
			final int damageMinimum, final int damageMax,
			final int ticksTillDeath) {
		CycleEventHandler.getSingleton().addEvent(n, new CycleEvent() { // adding
																		// a
																		// real
																		// delayed
																		// hit
																		// here

			@Override
			public void execute(CycleEventContainer container) {

				if (n == null || n.isDead()) {
					container.stop();
					return;
				}

				// gather players
				Collection<Player> tempPlayers = null;
				tempPlayers = Server.getRegionManager().getLocalPlayers(n);
				if (tempPlayers.size() > 0) {
					for (Player p : tempPlayers) {

						if (p == null || !p.canGetHit()) {
							continue;
						}
						if ((p.getX() >= n.getX() - distance)
								&& (p.getX() <= n.getX() + (n.getSize()-1)
										+ distance)
								&& (p.getY() >= n.getY() - distance)
								&& (p.getY() <= n.getY() + (n.getSize()-1)
										+ distance)) {
							p.dealTrueDamage(damageMinimum + Misc.random(damageMax), 1, HIT_GFX, n);
						}
					}
				}
				n.setDead(true);
				// tempPlayers.clear();
				tempPlayers = null;

				container.stop();
			}

			@Override
			public void stop() {

			}

		}, ticksTillDeath);

	}

	
	private static final Graphic HIT_GFX = Graphic.create(357, GraphicType.LOW);
}
