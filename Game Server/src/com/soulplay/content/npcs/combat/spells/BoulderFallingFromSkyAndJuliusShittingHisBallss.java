package com.soulplay.content.npcs.combat.spells;

import com.soulplay.Server;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class BoulderFallingFromSkyAndJuliusShittingHisBallss {

	public static void tossBoulder(int x, int y, final int z, int damage) {
		int yPlus = 0;
		int xPlus = 0;
		if (Misc.random(1) == 1) {
			yPlus++;
		} else {
			xPlus++;
		}
		int x2 = x + xPlus;
		int y2 = y + yPlus;
//		int offX = (y2 - y) * -1;
//		int offY = (x2 - x) * -1;
		
		Projectile proj = new Projectile(856, Location.create(x2, y2, z), Location.create(x, y, z));
		proj.setStartHeight(120);
		proj.setEndHeight(0);
		proj.setStartDelay(25);
		proj.setEndDelay(150);
		GlobalPackets.createProjectile(proj);

//		GlobalPackets.createPlayersProjectile(x2, y2, z, offX, offY, 5, 150,
//				856, 120, 0, 0, 25);

		Server.getTaskScheduler().schedule(new Task(5) {

			@Override
			protected void execute() {
				this.stop();

				for (Player p : Server.getRegionManager()
						.getLocalPlayersByLocation(x, y, z)) {
					if (p.isActive && p.getX() == x && p.getY() == y) { // take
																				// damage
						p.dealTrueDamage(damage, 1, null, null);
					}
				}

				Server.getStillGraphicsManager().createGfx(x, y, z, 266,
						0, 0);

			}
		});

	}
}
