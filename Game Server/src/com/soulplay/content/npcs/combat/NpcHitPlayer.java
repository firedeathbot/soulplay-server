package com.soulplay.content.npcs.combat;

import com.soulplay.content.player.combat.accuracy.MagicAccuracy;
import com.soulplay.content.player.combat.accuracy.MeleeAccuracy;
import com.soulplay.content.player.combat.accuracy.RangeAccuracy;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class NpcHitPlayer {
	
	private static int dealChecks(NPC n, Player c, int damageDealt) {
		n.onDamageApplied(c, damageDealt);
		
		if (c.playerIndex <= 0 && c.npcIndex <= 0) {
			if (c.autoRet == 1 && !c.walkingToItem && c.clickObjectType <= 0) {
				c.npcIndex = n.npcIndex;
			}
		}
		
		return damageDealt;
	}
	
	public static int dealTrueDamage(NPC n, Player c, int damage, int ticks, Graphic gfx) {
		return dealChecks(n, c, c.dealTrueDamage(damage, ticks, gfx, n));
	}
	
	public static int dealMagicDamage(NPC n, Player c, int damage, int ticks, Graphic gfx) {
		return dealChecks(n, c, c.dealMagicDamage(
				MagicAccuracy.successfulHit(n, c, null).isSuccessfulHit() ? damage : 0,
				ticks, gfx, n));
	}
	
	public static int dealMeleeDamage(NPC n, Player c, int damage, int ticks, Graphic gfx) {
		return dealChecks(n, c, c.dealMeleeDamage(
				MeleeAccuracy.successfulHit(n, c, 1.0).isSuccessfulHit() ? damage : 0,
				ticks, gfx, n));
	}

	public static int dealRangeDamage(NPC n, Player c, int damage, int ticks, Graphic gfx) {
		return dealChecks(n, c, c.dealRangeDamage(
				RangeAccuracy.isAccurateImpact(n, c).isSuccessfulHit() ? damage : 0,
				ticks, gfx, n));
	}
	
	public static int getHitDelay(NPC n, Player p) {
		int dist = Misc.distanceToPoint(n.getX(), n.getY(), p.getX(), p.getY());
		
		int hitDelay = 2; // adjust this to work proper delay
		if (dist > 2) {
			hitDelay += 1;
		}
		
		if (dist > 4) {
			hitDelay += 1;
		}
		if (dist > 8) {
			hitDelay += 1;
		}
		
		return hitDelay;
	}
	
}
