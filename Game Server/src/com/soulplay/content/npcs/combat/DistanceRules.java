package com.soulplay.content.npcs.combat;

import com.soulplay.game.model.npc.NPC;

public class DistanceRules {
	
	private NPC npc;
	
	private int meleeDistance;
	private int rangeDistance;
	private int magicDistance;
	private int followDistance;
	private int homeDistance;
	private int dragonBreathDist;
	
	public DistanceRules(NPC n) {
		this.setNpc(n);
		this.meleeDistance = 1;
		this.rangeDistance = 8;
		this.magicDistance = 8;
		this.followDistance = 10;
		this.homeDistance = 20;
		this.dragonBreathDist = 8;
	}

	public NPC getNpc() {
		return npc;
	}

	public void setNpc(NPC npc) {
		this.npc = npc;
	}

	public int getMeleeDistance() {
		return meleeDistance;
	}

	public void setMeleeDistance(int meleeDistance) {
		this.meleeDistance = meleeDistance;
	}

	public int getRangeDistance() {
		return rangeDistance;
	}

	public void setRangeDistance(int rangeDistance) {
		this.rangeDistance = rangeDistance;
	}

	public int getMagicDistance() {
		return magicDistance;
	}

	public void setMagicDistance(int magicDistance) {
		this.magicDistance = magicDistance;
	}

	public int getFollowDistance() {
		return followDistance;
	}

	public void setFollowDistance(int followDistance) {
		this.followDistance = followDistance;
	}

	public int getHomeDistance() {
		return homeDistance;
	}

	public void setHomeDistance(int homeDistance) {
		this.homeDistance = homeDistance;
	}
	
	public int getDistanceRequiredToHit(int hitType) {
		if (hitType == 3) { // dragon breath
			return dragonBreathDist;
		} else if (hitType == 2) { // magic
			return magicDistance;
		} else if (hitType == 1) { // ranged
			return rangeDistance;
		} else {// else melee
			return meleeDistance;
		}
	}
}
