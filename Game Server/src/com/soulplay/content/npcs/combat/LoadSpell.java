package com.soulplay.content.npcs.combat;

import com.soulplay.content.npcs.combat.spells.ExplodeWrath;
import com.soulplay.game.model.npc.NPC;

public class LoadSpell {

	public static void explodeAndSmite(NPC n, int distance, int damageMinimum,
			int damageMax, int ticksTillDeath) {
		ExplodeWrath.setExplodeTick(n, distance, damageMinimum, damageMax,
				ticksTillDeath);
	}
}
