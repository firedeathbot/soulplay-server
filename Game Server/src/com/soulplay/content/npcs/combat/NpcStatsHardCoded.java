package com.soulplay.content.npcs.combat;

public class NpcStatsHardCoded {

	public static int getMagicLevel(int npcId) {
		switch (npcId) {
		case 2012: // zulrah
			return 300;
		// Kree'arra
		case 6222:
			return 200;
		// Flockleader Geerin
		case 6225:
			return 50;
		// Flight Kilisa
		case 6227:
			return 50;
		// Wingman Skree
		case 6223:
			return 150;
		// General Graardor
		case 6260:
			return 80;
		// Sergeant Steelwill
		case 6263:
			return 150;
		// Sergeant Grimspike
		case 6265:
			return 50;
		// Sergeant Strongsta
		case 6261:
			return 50;
		// K'ril Tsutsaroth
		case 6203:
			return 200;
		// Tstanon Karlak
		case 6204:
			return 50;
		// Balfrug Kreeyath
		case 6208:
			return 150;
		// Zakl'n Gritch
		case 6206:
			return 50;
		// Commander Zilyana
		case 6247:
			return 300;
		// Bree
		case 6252:
			return 80;
		// Starlight
		case 6248:
			return 125;
		// Growler
		case 6250:
			return 150;
		// Nex
		case 13447:
			return 400;
		// Fumus
		case 13451:
			// Umbra
		case 13452:
			// Cruor
		case 13453:
			// Glacies
		case 13454:
			return 85;
		// King Black Dragon
		case 50:
			return 240;
		// Choas Elemental
		case 3200:
			return 280;
		// Tormented Demons
		case 8349:
			return 1;
		// Corporeal Beast
		case 8133:
			return 350;
		// Dagganoth Prime
		case 2882:
			return 255;
		// Dagannoth Supreme
		case 2881:
			return 255;
		// Nomad
		case 8528:
			return 75;
		// Tztok-Jad
		case 2745:
			return 480;
		// Ket-Zek
		case 2743:
			return 240;
		// Yt-Mejkot
		case 2741:
			return 120;
		// Tok-Zil
		case 2631:
			return 60;
		// Tz-Kek
		case 2630:
			return 60;
		// Kalphite Queen
		case 3835:
		case 3836:
			return 150;
		// Avatar Of Destruction
		case 8596:
			return 1;
		// Avatar Of Creation
		case 8597:
			return 1;
		// Scorpia
		case 4172:
			return 1;
		// Callisto
		case 4174:
			return 1;
		// Venenantis
		case 4173:
			return 150;
		}
		return 1;
	}

	public static int getMagicDefence(int npcId) {
		switch (npcId) {
		case 2012: // zulrah
			return -45;
		// Kree'arra
		case 6222:
			return 200;
		// Flockleader Geerin
		case 6225:
			return 0;
		// Flight Kilisa
		case 6227:
			return 0;
		// Wingman Skree
		case 6223:
			return 0;
		// General Graardor
		case 6260:
			return 298;
		// Sergeant Steelwill
		case 6263:
			return 0;
		// Sergeant Grimspike
		case 6265:
			return 0;
		// Sergeant Strongsta
		case 6261:
			return 0;
		// K'ril Tsutsaroth
		case 6203:
			return 130;
		// Tstanon Karlak
		case 6204:
			return -5;
		// Balfrug Kreeyath
		case 6208:
			return 10;
		// Zakl'n Gritch
		case 6206:
			return -5;
		// Commander Zilyana
		case 6247:
			return 100;
		// Bree
		case 6252:
			return 18;
		// Starlight
		case 6248:
			return 5;
		// Growler
		case 6250:
			return 18;
		// Nex
		case 13447:
			return 250;
		// Fumus
		case 13451:
			// Umbra
		case 13452:
			// Cruor
		case 13453:
			// Glacies
		case 13454:
			return 55;
		// King Black Dragon
		case 50:
			return 80;
		// Choas Elemental
		case 3200:
			return 80;
		// Tormented Demons
		case 8349:
			return 65;
		// Corporeal Beast
		case 8133:
			return 150;
		// Dagganoth Prime
		case 2882:
			return 255;
		// Dagannoth Supreme
		case 2881:
			return 255;
		// Nomad
		case 8528:
			return 40;
		// Tztok-Jad
		case 2745:
			return 0;
		// Ket-Zek
		case 2743:
			return 0;
		// Yt-Mejkot
		case 2741:
			return 0;
		// Tok-Zil
		case 2631:
			return 0;
		// Tz-Kek
		case 2630:
			return 0;
		// Kalphite Queen
		case 3835:
		case 3836:
			return 100;
		// Avatar Of Destruction
		case 8596:
			return 40;
		// Avatar Of Creation
		case 8597:
			return 40;
		// Scorpia
		case 4172:
			return 44;
		// Callisto
		case 4174:
			return 900;
		// Venenantis
		case 4173:
			return 850;
		}
		return 0;
	}
}
