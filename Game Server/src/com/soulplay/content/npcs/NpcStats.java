package com.soulplay.content.npcs;

import com.soulplay.cache.EntityDef;
import com.soulplay.util.Misc;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

import static com.soulplay.content.npcs.NpcStatsEntry.*;
import static com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonNpcs.*;

public class NpcStats {

	public static int decDef = 0;
	public static int decAtt = 0;
	public static int decHit = 0;
	public static final Int2ObjectMap<NpcStatsEntry> stats = new Int2ObjectOpenHashMap<>();

	public static NpcStatsEntry define(int npcID, NpcStatsEntry npcStatConfig) {
		stats.put(npcID, npcStatConfig);
		return npcStatConfig;
	}

	public static NpcStatsEntry define(int npcID,
	                                   int hitpoints, int maxHit, int attack, int defence) {
		return define(npcID, new NpcStatsEntry(attack, defence, maxHit, -1, hitpoints));
	}

	public static void load() {
		loadRangers();
		loadWarriorsFastest();
		loadWarriorsFast(FORGOTTEN_WARRIORS_RAPIER);
		loadWarriorsFast(FORGOTTEN_LONGSWORD);
		loadWarriorsAverage(FORGOTTEN_WARRIORS_WARHAMMER);
		loadWarriorsAverage(FORGOTTEN_WARRIORS_BATTLEAXE);
		loadWarriorsAverage(FORGOTTEN_WARRIORS_2H);
		loadWarriorsAverage(FORGOTTEN_WARRIORS_SPEAR);
		loadWarriorsAverage(FORGOTTEN_WARRIORS_MAUL);
		loadMagers();
		loadRats(NORMAL_GIANT_RATS);
		loadRats(WARPED_GIANT_RATS);
		loadShades();
		loadIceWarriors();
		loadIceSpiders();
		loadThrowTrolls();
		loadIceTrolls();
		loadIceGiants();
		loadIceElementals();
		loadIcefiends();
		loadHydra();
		loadFrost();
		loadEarth();
		loadDungSpiders();
		loadSkeleRapier();
		loadSkeleWarhammer();
		loadSkeleHatchet();
		loadSkeleLongsword();
		loadSkeleFlail();
		loadSkeleUnarmed();
		loadSkeleMage();
		loadSkeleRange();
		loadZombieMelee();
		loadZombieRange();
		loadGreenDrag();
		loadBats();
		loadGiantBats();
		loadHillGiants();
		loadHobgoblins();
		loadAnimPickaxes();
		loadGuardDogs(NORMAL_GUARD_DOGS);
		loadGuardDogs(WARPED_GUARD_DOGS);
		loadBrutes(NORMAL_BRUTES);
		loadBrutes(WARPED_BRUTES);
		loadIronDrag();
		loadRedDrag();
		loadFireGiants();
		loadHellhounds();
		loadLesserDemons();
		loadGreaterDemons();
		loadBlackDemons();
		loadNecromancers();
		loadGhosts();
		loadBlackDrag();
		loadRebornMages();
		loadRebornWarriors();
		loadAnkous();
		loadSoulgazer();
		loadEdimmu();
		loadNechryael();
		loadSeeker();
		loadSpiritualGuardian();
		loadCaveSlime();
		loadPyrefiend();
		loadNightSpider();
		loadJelly();
		loadCaveSlime();
		loadCaveCrawler();
		loadCrawlingHand();
		loadSkeletalMinions();
	}

	private static int getCombat(int npcId) {
		EntityDef def = EntityDef.forID(npcId);
		if (def != null) {
			return def.combatLevel;
		}

		return 10;
	}

	public static void loadRangers() {
		for (int[] data : FORGOTTEN_RANGERS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 114, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 114, combat);
			int maxHit = Misc.interpolate(2, 21 - decHit, 7, 114, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 124, 1, 114, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}

	public static void loadWarriorsFastest() {
		for (int[] data : FORGOTTEN_WARIORS_DAGGER) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 138, combat);
			int defence = Misc.interpolate(1, 95 - decDef, 1, 138, combat);
			int maxHit = Misc.interpolate(2, 21 - decHit, 7, 138, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 149, 1, 138, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadWarriorsFast(int[][] array) {
		for (int[] data : array) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 138, combat);
			int defence = Misc.interpolate(1, 95 - decDef, 1, 138, combat);
			int maxHit = Misc.interpolate(1, 27 - decHit, 7, 138, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 149, 1, 138, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadWarriorsAverage(int[][] array) {
		for (int[] data : array) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 138, combat);
			int defence = Misc.interpolate(1, 95 - decDef, 1, 138, combat);
			int maxHit = Misc.interpolate(1, 30 - decHit, 7, 138, combat);
			int speed = 6;
			int hp = Misc.interpolate(2, 149, 1, 138, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}

	public static void loadMagers() {
		for (int[] data : FORGOTTEN_MAGERS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 89 - decAtt, 1, 115, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 115, combat);
			int maxHit = Misc.interpolate(1, 27 - decHit, 5, 115, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 124, 1, 114, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_TURN_OFF, HEX_HUNTER_EFFECT));
		}
	}

	public static void loadRats(int[][] array) {
		for (int[] data : array) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 45 - decAtt, 1, 96, combat);
			int defence = Misc.interpolate(1, 35 - decDef, 1, 96, combat);
			int maxHit = Misc.interpolate(1, 9 - decHit, 2, 96, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 84, 1, 96, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadShades() {//TODO finish max hit range and mage
		for (int[] data : MYSTERIOUS_SHADES) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 120 - decAtt, 1, 133, combat);
			int defence = Misc.interpolate(1, 65 - decDef, 1, 133, combat);
			int maxHit = Misc.interpolate(1, 31 - decHit, 1, 133, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 125, 1, 133, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadIceWarriors() {
		for (int[] data : ICE_WARRIORS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 70 - decAtt, 1, 130, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 130, combat);
			int maxHit = Misc.interpolate(1, 18 - decHit, 2, 130, combat);
			int speed = 4; //rapier
			int hp = Misc.interpolate(2, 114, 1, 130, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadIceSpiders() {
		for (int[] data : ICE_SPIDERS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 33 - decAtt, 1, 79, combat);
			int defence = Misc.interpolate(1, 30 - decDef, 1, 79, combat);
			int maxHit = Misc.interpolate(1, 7 - decHit, 2, 79, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 65, 1, 79, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadThrowTrolls() {//range
		for (int[] data : THROWER_TROLLS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 75 - decAtt, 1, 132, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 132, combat);
			int maxHit = Misc.interpolate(4, 26 - decHit, 42, 132, combat);
			int speed = 6;
			int hp = Misc.interpolate(2, 129, 1, 132, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadIceTrolls() {
		for (int[] data : ICE_TROLLS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 75 - decAtt, 1, 148, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 148, combat);
			int maxHit = Misc.interpolate(5, 21 - decHit, 60, 148, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 134, 1, 148, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadIceGiants() {
		for (int[] data : ICE_GIANTS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 284, combat);
			int defence = Misc.interpolate(1, 90 - decDef, 1, 284, combat);
			int maxHit = Misc.interpolate(4, 24 - decHit, 37, 284, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 172, 1, 284, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadIceElementals() {//range
		for (int[] data : ICE_ELEMENTALS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 86 - decAtt, 1, 215, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 215, combat);
			int maxHit = Misc.interpolate(2, 18 - decHit, 20, 215, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 129, 1, 215, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadIcefiends() {//range/mage
		for (int[] data : ICEFIENDS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 84 - decAtt, 1, 160, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 160, combat);
			int maxHit = Misc.interpolate(2, 20 - decHit, 12, 160, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 124, 1, 160, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadHydra() {//mage
		for (int[] data : HYDRA) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 132, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 132, combat);
			int maxHit = Misc.interpolate(2, 25 - decHit, 11, 132, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 130, 1, 132, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}

	public static void loadFrost() {//mage/melee make frost dragon use these stats
		for (int[] data : FROST_DRAGONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 254, combat);
			int defence = Misc.interpolate(1, 120 - decDef, 1, 254, combat);
			int maxHit = Misc.interpolate(9, 22 - decHit, 84, 254, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 198, 1, 254, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}

	public static void loadEarth() {
		for (int[] data : EARTH_WARRIORS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 75 - decAtt, 1, 128, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 128, combat);
			int maxHit = Misc.interpolate(1, 18 - decHit, 4, 128, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 198, 1, 128, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadDungSpiders() {
		for (int[] data : DUNGEON_SPIDERS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 33 - decAtt, 1, 74, combat);
			int defence = Misc.interpolate(1, 30 - decDef, 1, 74, combat);
			int maxHit = Misc.interpolate(1, 7 - decHit, 2, 74, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 65, 1, 74, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadSkeleUnarmed() {
		for (int[] data : SKELETONS_UNARMED) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 29 - decAtt, 1, 58, combat);
			int defence = Misc.interpolate(1, 27 - decDef, 1, 58, combat);
			int maxHit = Misc.interpolate(1, 5 - decHit, 15, 58, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 50, 1, 58, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleFlail() {
		for (int[] data : SKELETONS_FLAIL) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 178, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 178, combat);
			int maxHit = Misc.interpolate(3, 19 - decHit, 23, 178, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 132, 1, 178, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleLongsword() {
		for (int[] data : SKELETONS_LONGSWORD) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 70 - decAtt, 1, 122, combat);
			int defence = Misc.interpolate(1, 60 - decDef, 1, 122, combat);
			int maxHit = Misc.interpolate(2, 16 - decHit, 15, 122, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 114, 1, 122, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleHatchet() {
		for (int[] data : SKELETONS_HATCHET) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 178, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 178, combat);
			int maxHit = Misc.interpolate(2, 19 - decHit, 15, 178, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 132, 1, 178, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleWarhammer() {
		for (int[] data : SKELETONS_WARHAMMER) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 178, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 178, combat);
			int maxHit = Misc.interpolate(3, 19 - decHit, 33, 178, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 132, 1, 178, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleRapier() {
		for (int[] data : SKELETONS_RAPIER) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 178, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 178, combat);
			int maxHit = Misc.interpolate(3, 20 - decHit, 33, 178, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 132, 1, 178, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleRange() {
		for (int[] data : SKELETONS_RANGE) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 136, combat);
			int defence = Misc.interpolate(1, 85 - decDef, 1, 136, combat);
			int maxHit = Misc.interpolate(1, 18 - decHit, 13, 136, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 118, 1, 136, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadSkeleMage() {
		for (int[] data : SKELETONS_MAGE) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 55 - decAtt, 1, 135, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 135, combat);
			int maxHit = Misc.interpolate(1, 25 - decHit, 11, 135, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 118, 1, 135, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadZombieMelee() {
		for (int[] data : ZOMBIES) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 152, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 152, combat);
			int maxHit = Misc.interpolate(1, 22 - decHit, 4, 152, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 128, 1, 152, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadZombieRange() {
		for (int[] data : ZOMBIES_RANGE) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 152, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 152, combat);
			int maxHit = Misc.interpolate(1, 22 - decHit, 3, 152, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 128, 1, 152, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadGreenDrag() {
		for (int[] data : GREEN_DRAGONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 90 - decAtt, 1, 240, combat);
			int defence = Misc.interpolate(1, 110 - decDef, 1, 240, combat);
			int maxHit = Misc.interpolate(7, 20 - decHit, 73, 240, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 189, 1, 240, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadBats() {
		for (int[] data : BATS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 25 - decAtt, 1, 28, combat);
			int defence = Misc.interpolate(1, 20 - decDef, 1, 28, combat);
			int maxHit = Misc.interpolate(2, 3 - decHit, 3, 28, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 29, 1, 28, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadGiantBats() {
		for (int[] data : GIANT_BATS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 75 - decAtt, 1, 128, combat);
			int defence = Misc.interpolate(1, 72 - decDef, 1, 128, combat);
			int maxHit = Misc.interpolate(3, 17 - decHit, 40, 128, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 122, 1, 128, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadHillGiants() {
		for (int[] data : HILL_GIANTS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 211, combat);
			int defence = Misc.interpolate(1, 90 - decDef, 1, 211, combat);
			int maxHit = Misc.interpolate(1, 23 - decHit, 13, 211, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 162, 1, 211, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadGiantSkele() {
		for (int[] data : GIANT_SKELETONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 157, combat);
			int defence = Misc.interpolate(1, 85 - decDef, 1, 157, combat);
			int maxHit = Misc.interpolate(2, 23 - decHit, 28, 157, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 136, 1, 157, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_HIDE));
		}
	}
	
	public static void loadHobgoblins() {
		for (int[] data : HOBGOBLINS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 141, combat);
			int defence = Misc.interpolate(1, 80 - decDef, 1, 141, combat);
			int maxHit = Misc.interpolate(1, 19 - decHit, 12, 141, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 129, 1, 141, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadAnimPickaxes() {
		for (int[] data : ANIMATED_PICKAXES) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 70 - decAtt, 1, 116, combat);
			int defence = Misc.interpolate(1, 60 - decDef, 1, 116, combat);
			int maxHit = Misc.interpolate(1, 16 - decHit, 7, 116, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 110, 1, 116, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadGuardDogs(int[][] array) {
		for (int[] data : array) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 131, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 131, combat);
			int maxHit = Misc.interpolate(1, 17 - decHit, 4, 131, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 124, 1, 131, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadBrutes(int[][] array) {
		for (int[] data : array) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 90 - decAtt, 1, 143, combat);
			int defence = Misc.interpolate(1, 90 - decDef, 1, 143, combat);
			int maxHit = Misc.interpolate(3, 29 - decHit, 21, 143, combat);
			int speed = 6;
			int hp = Misc.interpolate(2, 139, 1, 143, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadIronDrag() {
		for (int[] data : IRON_DRAGONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 263, combat);
			int defence = Misc.interpolate(1, 115 - decDef, 1, 263, combat);
			int maxHit = Misc.interpolate(9, 21 - decHit, 89, 263, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 194, 1, 263, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadRedDrag() {
		for (int[] data : RED_DRAGONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 279, combat);
			int defence = Misc.interpolate(1, 120 - decDef, 1, 279, combat);
			int maxHit = Misc.interpolate(8, 22 - decHit, 86, 279, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 204, 1, 279, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadFireGiants() {
		for (int[] data : FIRE_GIANTS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 98 - decAtt, 1, 296, combat);
			int defence = Misc.interpolate(1, 96 - decDef, 1, 296, combat);
			int maxHit = Misc.interpolate(4, 24 - decHit, 40, 296, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 184, 1, 296, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadAnimBooks() {
		for (int[] data : ANIMATED_BOOKS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 68 - decAtt, 1, 111, combat);
			int defence = Misc.interpolate(1, 60 - decDef, 1, 111, combat);
			int maxHit = Misc.interpolate(1, 15 - decHit, 3, 111, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 106, 1, 111, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadHellhounds() {
		for (int[] data : HELLHOUNDS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 192, combat);
			int defence = Misc.interpolate(1, 80 - decDef, 1, 192, combat);
			int maxHit = Misc.interpolate(5, 19 - decHit, 56, 192, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 151, 1, 192, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadLesserDemons() {
		for (int[] data : LESSER_DEMONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 122, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 122, combat);
			int maxHit = Misc.interpolate(5, 16 - decHit, 56, 122, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 118, 1, 122, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadGreaterDemons() {
		for (int[] data : GREATER_DEMONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 165, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 165, combat);
			int maxHit = Misc.interpolate(8, 19 - decHit, 92, 165, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 141, 1, 165, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadBlackDemons() {
		for (int[] data : BLACK_DEMONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 226, combat);
			int defence = Misc.interpolate(1, 90 - decDef, 1, 226, combat);
			int maxHit = Misc.interpolate(19, 20 - decHit, 181, 226, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 163, 1, 226, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadNecromancers() {
		for (int[] data : NECROMANCERS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 90 - decAtt, 1, 145, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 145, combat);
			int maxHit = Misc.interpolate(1, 25 - decHit, 12, 145, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 126, 1, 145, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_TURN_OFF, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadGhosts() {
		for (int[] data : GHOSTS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 199, combat);
			int defence = Misc.interpolate(1, 80 - decDef, 1, 199, combat);
			int maxHit = Misc.interpolate(1, 19 - decHit, 5, 199, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 152, 1, 199, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadBlackDrag() {
		for (int[] data : BLACK_DRAGONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 95 - decAtt, 1, 303, combat);
			int defence = Misc.interpolate(1, 125 - decDef, 1, 303, combat);
			int maxHit = Misc.interpolate(8, 22 - decHit, 96, 303, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 218, 1, 303, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadRebornMages() {
		for (int[] data : REBORN_MAGES) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 90 - decAtt, 1, 145, combat);
			int defence = Misc.interpolate(1, 70 - decDef, 1, 145, combat);
			int maxHit = Misc.interpolate(1, 20 - decHit, 12, 145, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 126, 1, 145, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, SSH_TURN_OFF, HEX_HUNTER_EFFECT));
		}
	}
	
	public static void loadRebornWarriors() {
		for (int[] data : REBORN_WARRIORS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 80 - decAtt, 1, 152, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 152, combat);
			int maxHit = Misc.interpolate(1, 20 - decHit, 4, 152, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 128, 1, 152, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadAnkous() {
		for (int[] data : ANKOUS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 85 - decAtt, 1, 143, combat);
			int defence = Misc.interpolate(1, 75 - decDef, 1, 143, combat);
			int maxHit = Misc.interpolate(1, 19 - decHit, 12, 143, combat);
			int speed = 4;
			int hp = Misc.interpolate(2, 129, 1, 143, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp));
		}
	}
	
	public static void loadSoulgazer() {
		int attack = 100;
		int defence = 75;
		int maxHit = 45;
		int speed = 4;
		int hp = 400;
		stats.put(10705, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadEdimmu() {
		int attack = 90;
		int defence = 70;
		int maxHit = 35;
		int speed = 4;
		int hp = 320;
		stats.put(10704, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadNechryael() {
		int attack = 90;
		int defence = 80;
		int maxHit = 32;
		int speed = 4;
		int hp = 320;
		stats.put(10702, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadSeeker() {
		int attack = 75;
		int defence = 60;
		int maxHit = 24;
		int speed = 4;
		int hp = 240;
		stats.put(10701, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadSpiritualGuardian() {
		int attack = 65;
		int defence = 95;
		int maxHit = 21;
		int speed = 4;
		int hp = 106;
		stats.put(10700, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadJelly() {
		int attack = 45;
		int defence = 45;
		int maxHit = 13;
		int speed = 4;
		int hp = 90;
		stats.put(10699, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadNightSpider() {
		int attack = 67;
		int defence = 40;
		int maxHit = 12;
		int speed = 4;
		int hp = 80;
		stats.put(10698, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadPyrefiend() {
		int attack = 41;
		int defence = 36;
		int maxHit = 7;
		int speed = 4;
		int hp = 44;
		stats.put(10697, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadCaveSlime() {
		int attack = 23;
		int defence = 31;
		int maxHit = 5;
		int speed = 4;
		int hp = 37;
		stats.put(10696, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadCaveCrawler() {
		int attack = 15;
		int defence = 19;
		int maxHit = 3;
		int speed = 4;
		int hp = 21;
		stats.put(10695, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadCrawlingHand() {
		int attack = 10;
		int defence = 8;
		int maxHit = 2;
		int speed = 4;
		int hp = 12;
		stats.put(10694, new NpcStatsEntry(attack, defence, maxHit, speed, hp));
	}
	
	public static void loadSkeletalMinions() {//mage
		for (int[] data : SKELETAL_MINIONS) {
			int combat = getCombat(data[0]);
			int attack = Misc.interpolate(1, 350 - decAtt, 1, 106, combat);
			int defence = Misc.interpolate(1, 50 - decDef, 1, 106, combat);
			int maxHit = Misc.interpolate(2, 15 - decHit, 25, 106, combat);
			int speed = 5;
			int hp = Misc.interpolate(2, 100, 1, 106, combat);
			stats.put(data[0], new NpcStatsEntry(attack, defence, maxHit, speed, hp, BARRICADE));
		}
	}
}