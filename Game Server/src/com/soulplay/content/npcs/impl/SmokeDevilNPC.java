package com.soulplay.content.npcs.impl;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;

@NpcMeta(ids = { 100498 })
public class SmokeDevilNPC extends CombatNPC {

	public SmokeDevilNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		projectileSwing(p, 1470, 20, 3, null, CombatType.RANGED);
	}

}
