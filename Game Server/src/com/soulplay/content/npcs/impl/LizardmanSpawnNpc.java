package com.soulplay.content.npcs.impl;

import com.soulplay.Server;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 106768 })
public class LizardmanSpawnNpc extends CombatNPC {

	public Player targetPlayer;
	private int ticks;
	private boolean follow = true;

	public LizardmanSpawnNpc(int slot, int npcType) {
		super(slot, npcType);

		walksOnTopOfNpcs = true;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		ticks = Server.getTotalTicks() + Misc.random(6, 10);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		startAnimation(Animation.osrs(7160));
	}

	@Override
	public void process() {
		super.process();

		if (follow && targetPlayer != null) {
			face(targetPlayer);
			NPCHandler.followPlayer(this, targetPlayer);
		}

		if (Server.getTotalTicks() == ticks) {
			follow = false;
			resetFollow();
			resetWalkTemp();

			pulse(() -> {
				final Location explodeLoc = getCurrentLocation().copyNew();
				startAnimation(Animation.osrs(7159));
				pulse(() -> {
					Graphic.osrs(1295).send(explodeLoc);
					if (targetPlayer != null && explodeLoc.getDistanceInt(targetPlayer.getCurrentLocation()) <= 1) {
						targetPlayer.dealDamage(new Hit(Misc.random(5, 10)));
						targetPlayer = null;
					}

					nullNPC();
				}, 2);
			});
		}
	}

}
