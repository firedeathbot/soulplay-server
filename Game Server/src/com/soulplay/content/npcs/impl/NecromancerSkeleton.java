package com.soulplay.content.npcs.impl;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Mob;

@NpcMeta(ids = { 5386 })
public class NecromancerSkeleton extends CombatNPC {

	public static final int ID = 5386;

	public NecromancerSkeleton(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NecromancerSkeleton(slot, npcType);
	}

	public void spawn(Player player, Mob opponent) {
		spawnedBy = player.getId();
		getSkills().setStaticLevel(Skills.ATTACK, 99999); // High attack level so he ignores defense
		startAnimation(3592);
		setRetaliates(false);
		face(opponent);
		pulse(() -> {
			attack(opponent);
		}, 1);
		pulse(() -> {
			if (isActive) {
				setDead(true);
			}
		}, 18);
	}

	public void attack(Mob opponent) {
		if (opponent.isNpc()) {
			killNpc = opponent.getId();
		} else {
			attack(opponent.toPlayerFast());
		}
	}

	@Override
	public void process() {
		super.process();

		Player spawnedBy = getSpawnedByPlayer();
		if (spawnedBy != null) {
			if (spawnedBy.isAttackingNpc()) {
				attack(NPCHandler.npcs[spawnedBy.npcIndex]);
			} else if (spawnedBy.isAttackingPlayer()) {
				attack(PlayerHandler.players[spawnedBy.playerIndex]);
			}
		}
	}

}
