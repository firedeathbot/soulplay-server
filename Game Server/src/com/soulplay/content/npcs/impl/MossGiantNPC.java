package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = {112})
public class MossGiantNPC extends CombatNPC {

	public MossGiantNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new MossGiantNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(112, 60, 6, 30, 30);
		NPCAnimations.define(112, 4659, 4657, 4658);
	}
}
