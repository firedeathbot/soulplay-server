package com.soulplay.content.npcs.impl;

import com.soulplay.Server;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4188})
public class EntNpc  extends AbstractNpc {
	
//	private static final Animation DEATH_ANIM = new Animation(Animation.getOsrsAnimId(7146));
	
//	private boolean isKilled = false;

	public EntNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new EntNpc(slot, npcType);
	}
	
	@Override
	public void process() {
		super.process();
		
//		if (getSkills().getLifepoints() <= 0 && !isKilled) {
//			final int MAX_HP = getSkills().getMaximumLifepoints();
//			getSkills().setStaticLevel(Skills.HITPOINTS, 0);
//			getSkills().setLifepoints(77);
//			isKilled = true;
//			setRetaliates(false);
//			setCanWalk(false);
//			isAggressive = false;
//			
//			setAliveTick(10);
//			
//			startAnimation(DEATH_ANIM);
//			
//			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
//				
//				@Override
//				public void stop() {
//				}
//				
//				@Override
//				public void execute(CycleEventContainer container) {
//					container.stop();
//					
//
//					transform(4189);
//					startAnimation(65535);
//					getSkills().setStaticLevel(Skills.HITPOINTS, MAX_HP);
//					setAttackable(false);
//					
//					
//				}
//			}, 4);
//		}
		
		
	}
	
	@Override
	public void onDeath() {
		super.onDeath();
		
		final int killerMID = getKillerBySQLIndex();
		
		final Client c = (Client) PlayerHandler.getPlayerByMID(killerMID);
		
		final Location loc = getCurrentLocation().copyNew();
		
		Server.getTaskScheduler().schedule(4, ()-> {
			
			if (c != null && c.isActive) { //gonna use this version of spawning ent logs cuz it looks more fun
				int npcHp = 5 + Misc.random(10); // hp is how many times logs are farmed
				NPC trunk = NPCHandler.spawnNpc(c, 4189, loc.getX(), loc.getY(), loc.getZ(), 0, false, false);
				trunk.getSkills().setMaximumLifepoints(npcHp);
				trunk.setAliveTick(100);
				trunk.setRetaliates(false);
				trunk.setAttackable(false);
			}
			
		});
		
	}
	

}
