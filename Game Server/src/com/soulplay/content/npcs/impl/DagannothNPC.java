package com.soulplay.content.npcs.impl;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = {1341, 107260, 107259, 1338})
public class DagannothNPC extends CombatNPC {
	
	private List<NPC> deathSpawns = new ArrayList<>();

	public DagannothNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new DagannothNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}
	
	@Override
	public void startAttack(Player p) {
		//TODO: for lighthouse add ranged attack 
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(1341, 120, 8, 68, 71);
		NpcStats.define(107260, 120, 8, 68, 71);
		NpcStats.define(107259, 70, 8, 68, 50);
		NpcStats.define(1338, 70, 8, 68, 50);
		NPCAnimations.define(1341, 1342, -1, 1341);
		NPCAnimations.define(107260, 1342, -1, 1341);
		NPCAnimations.define(107259, 1342, -1, 1341);
		NPCAnimations.define(1338, 1342, -1, 1341);
	}
	
}
