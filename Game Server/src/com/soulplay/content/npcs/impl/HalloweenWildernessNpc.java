package com.soulplay.content.npcs.impl;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.util.Misc;

public class HalloweenWildernessNpc {
	
	private enum MoveData {
		SPOT_1(3245, 3184), 
		SPOT_2(3087, 3551),
		SPOT_3(3137, 3952),
		SPOT_4(3246, 3680),
		SPOT_5(2984, 3586),
		SPOT_6(3317, 3666),
		SPOT_7(3049, 3647),
		SPOT_8(9643, 10135),
		SPOT_9(9656, 10193),
		SPOT_10(9602, 10058),
		SPOT_11(3204, 3871),
		SPOT_12(2983, 3923),
		SPOT_13(3052, 3933);

		public static final MoveData[] values = values();
		private final int x, y;

		MoveData(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final int NPC_ID = 6504;
	private static final int TELEPORT_TICKS = 1000;
	private static NPC thisNpc;
	private static int teleportCursor;

	public static void spawnHalloweenWildernessNpc() {
		if (thisNpc != null) {
			return;
		}

		teleportCursor = Misc.randomNoPlus(MoveData.values.length);
		MoveData data = MoveData.values[teleportCursor];
		
		int npcSlot = Misc.findFreeNpcSlot();
		
		if (npcSlot != -1) {
			thisNpc = new NPC(npcSlot, NPC_ID);
			NPCHandler.spawnNpc(thisNpc, data.x, data.y, 0, 0);
		} else {
			return;
		}
		
		thisNpc.setWalksHome(false);

		CycleEventHandler.getSingleton().addEvent(thisNpc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				teleportCursor++;
				if (teleportCursor >= MoveData.values.length) {
					teleportCursor = 0;
				}
				MoveData newData = MoveData.values[teleportCursor];

				thisNpc.moveNpc(newData.x, newData.y, 0);
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, TELEPORT_TICKS);
	}


}
