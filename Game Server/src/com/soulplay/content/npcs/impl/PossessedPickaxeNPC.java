package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

@NpcMeta(ids = {5413})
public class PossessedPickaxeNPC extends CombatNPC {

	public PossessedPickaxeNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new PossessedPickaxeNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		getSkills().setStaticLevel(Skills.ATTACK, 40);
		getSkills().setStaticLevel(Skills.STRENGTH, 55);
		getSkills().setStaticLevel(Skills.DEFENSE, 40);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(5413, 40, 6, 40, 40);
		NPCAnimations.define(5413, 5592, -1, 5591);
	}
}
