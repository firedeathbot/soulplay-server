package com.soulplay.content.npcs.impl;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.util.Misc;

public class HalloweenMinigameNpc {

	private enum MoveData {
		SPOT_1(9800, 3179), 	
		SPOT_2(3370, 3267),
		SPOT_3(3559, 3295),	
		SPOT_4(2439, 5174),
		SPOT_5(2846, 3542),
		SPOT_6(2440, 3085),
		SPOT_7(2657, 2642),
		SPOT_8(1894, 3170),
		SPOT_9(3308, 3121),
		SPOT_10(2815, 3086),
		SPOT_11(3598, 3267);

		public static final MoveData[] values = values();
		private final int x, y;

		MoveData(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final int NPC_ID = 11246;
	private static final int TELEPORT_TICKS = 1000;
	private static NPC thisNpc;
	private static int teleportCursor;

	public static void spawnHalloweenMiningameNpc() {
		if (thisNpc != null) {
			return;
		}

		teleportCursor = Misc.randomNoPlus(MoveData.values.length);
		MoveData data = MoveData.values[teleportCursor];
		
		int npcSlot = Misc.findFreeNpcSlot();
		
		if (npcSlot != -1) {
			thisNpc = new NPC(npcSlot, NPC_ID);
			NPCHandler.spawnNpc(thisNpc, data.x, data.y, 0, 0);
		} else {
			return;
		}
		
		thisNpc.setWalksHome(false);

		CycleEventHandler.getSingleton().addEvent(thisNpc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				teleportCursor++;
				if (teleportCursor >= MoveData.values.length) {
					teleportCursor = 0;
				}
				MoveData newData = MoveData.values[teleportCursor];

				thisNpc.moveNpc(newData.x, newData.y, 0);
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, TELEPORT_TICKS);
	}



}
