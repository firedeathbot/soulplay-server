package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4381})
public class AnkouNPC extends CombatNPC {

	public AnkouNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new AnkouNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		getSkills().setStaticLevel(Skills.ATTACK, 70);
		getSkills().setStaticLevel(Skills.STRENGTH, 70);
		getSkills().setStaticLevel(Skills.DEFENSE, 60);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(4381, 60, 8, 70, 60);
		NPCAnimations.define(4381, 836, 424, 422);
	}
}
