package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 100010 })
public class DeathSpawnNPC extends CombatNPC {
	
	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new DeathSpawnNPC(slot, npcType);
	}

	public DeathSpawnNPC(int slot, int npcType) {
		super(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walksOnTopOfNpcs = true;
		setCanWalk(true);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
	}
	
	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	public void defineConfigs() {
		NpcStats.define(100010, 60, 2, 67, 30);
		NPCAnimations.define(100010, Animation.getOsrsAnimId(1541), -1, Animation.getOsrsAnimId(1540));
	}

}
