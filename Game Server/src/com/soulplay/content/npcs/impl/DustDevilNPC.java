package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

@NpcMeta(ids = {1624})
public class DustDevilNPC extends CombatNPC {

	public DustDevilNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new DustDevilNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		getSkills().setStaticLevel(Skills.ATTACK, 105);
		getSkills().setStaticLevel(Skills.STRENGTH, 70);
		getSkills().setStaticLevel(Skills.DEFENSE, 40);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(1624, 105, 8, 105, 40);
		NPCAnimations.define(1624, 1558, 1555, 1557);
	}
}
