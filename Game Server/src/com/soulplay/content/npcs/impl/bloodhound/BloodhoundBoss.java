package com.soulplay.content.npcs.impl.bloodhound;

import java.util.Set;

import com.google.common.collect.Sets;
import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.map.travel.TempPathFinder;
import com.soulplay.util.Misc;

@BossMeta(ids = {7232})
public class BloodhoundBoss extends Boss {
	
	//NOTE: THESE ARE STACKABLE ITEMS ONLY, THAT'S HOW IT IS CODED AT LEAST.
	private static final Set<Integer> ITEMS_PICKUP = Sets.newHashSet(995, 12158, 12159, 12160, 12163);

	public BloodhoundBoss(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
	}
	
	private PickupAction action = null;

	@Override
	public void process(NPC npc) {
		Client owner = npc.summoner;
		
		if (owner == null) {
			return;
		}
		
		if (!owner.withinDistance(npc.getX(), npc.getY(), npc.getZ(), 15)) {
			resetActionAndContinueFollow(npc);
			
			owner.getSummoning().callOnTeleport(); //npc.teleport(owner.getCurrentLocation(), TeleportType.NONE);
			return;
		}
		
		if (action == null) {
			if (npc.getStopWatch().checkThenStart(3)) {
				final int freeSlots = owner.getItems().freeSlots();
//				if (freeSlots == 0 && !owner.getItems().playerHasItem(995)) {
//					return;
//				}
				
				//find nearest gold
				PickupAction newAction = null;
				
				GroundItem itemFound = null;
				//GroundItem fallbackItem = null;
				
				int distance = 20;
				
				for (GroundItem item : Server.getRegionManager().getGroundItemsInOneRegion(owner.getX(), owner.getY(), owner.getZ())) {
					if (!ITEMS_PICKUP.contains(item.getItemId()) || (item.hideTicks > 0 && !item.getOwnerName().toLowerCase().equals(owner.getName().toLowerCase()))) continue;
					//maybe add failed attempts to fallback into ray tracing to items.
					
					if (freeSlots == 0 && !owner.getItems().playerHasItem(item.getItemId())) {
						continue;
					}
					
					int curDist = owner.distanceToPoint(item.getItemX(), item.getItemY());
					if (curDist < distance) {
						itemFound = item;
						distance = curDist;
					}
				}
				
				if (itemFound != null 
						&& !RegionClip.rayTraceBlocked(owner.getX(), owner.getY(),
								itemFound.getItemX(), itemFound.getItemY(), itemFound.getItemZ(), false, owner.getDynamicRegionClip())) {
					Location itemLoc = Location.create(itemFound.getItemX(), itemFound.getItemY(), itemFound.getItemZ());
					newAction = new PickupAction(itemLoc, itemFound);

					npc.forceChat(Misc.random(1) == 0 ? "I'll get that for you master." : "Woof! Woof!");
					
					action = newAction;
					TempPathFinder.walkToLocationSmart(npc, itemLoc);
					return;
				}
				
			}
			return;
		}
		
		if (action.expiredAction()) {
			resetActionAndContinueFollow(npc);
			action = null;
			return;
		}
		
		if (action.reachedLocation(npc.getCurrentLocation())) {
			ItemHandler.removeGroundItem(owner, action.item.getItemId(), action.item.getItemX(), action.item.getItemY(), action.item.getItemZ(), true);
			resetActionAndContinueFollow(npc);
			action = null;
			return;
		}
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.useSmartFollow = true;
		npc.getWalkingQueue().setRunning(true);
		if (Misc.random(100) == 7)
			npc.forceChat("Hello master!");
		
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 10;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		hit.setDamage(0);
	}

	@Override
	public void onDeath(NPC npc, Client c) {
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}
	
	private static void resetActionAndContinueFollow(NPC n) {
		n.dungVariables[0] = null;
		n.stopFollow = false;
	}
	
//	private static void startAction(NPC n, PickupAction action) {
//		n.stopFollow = true;
//		n.dungVariables[0] = action;
//	}

}
