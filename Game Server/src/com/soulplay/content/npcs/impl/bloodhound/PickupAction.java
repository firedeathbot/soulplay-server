package com.soulplay.content.npcs.impl.bloodhound;

import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.world.entity.Location;

public class PickupAction {

	public TickTimer expireTick = new TickTimer();
	
	public final Location walkToLocation;
	public final GroundItem item;
	
	public PickupAction(Location walkTo, GroundItem item) {
		this.walkToLocation = walkTo;
		this.item = item;
		expireTick.startTimer(10);
	}
	
	public boolean expiredAction() {
		return expireTick.complete() || !item.isActive();
	}

	public boolean reachedLocation(Location npcLoc) {
		boolean inPlace = npcLoc.matches(walkToLocation);
		
		if (inPlace)
			expireTick.reset();
		
		return inPlace;
	}
	
}
