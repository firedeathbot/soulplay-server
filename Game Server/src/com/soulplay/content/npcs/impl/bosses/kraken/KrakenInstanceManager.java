package com.soulplay.content.npcs.impl.bosses.kraken;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.clans.Clan;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class KrakenInstanceManager {

	private static Map<Integer, Integer> zMap = new HashMap<>();
	
	private static int availableHeight = 4;
	
	public static int getOrCreateZ(int mysqlIndex) {
		Integer z = zMap.get(mysqlIndex);
		if (z == null) {
			return createNewInstance(mysqlIndex);
		}
		return z;
	}
	
	private static int createNewInstance(int mysqlIndex) {
		int z = availableHeight;
		zMap.put(mysqlIndex, z);
		availableHeight += 4;
		return z;
	}
	
	private static Map<Integer, KrakenInstance> instances = new HashMap<>(); // store each instance by clan height as KEY
	
	public static KrakenInstance getInstanceByZ(int clanZ) {
		return instances.get(clanZ);
	}
	
	public static KrakenInstance getKrakenInstance(Clan clan) {
		int clanIndex = clan.getClanMysqlId();
		int z = getOrCreateZ(clanIndex);
		
		return getInstanceByZ(z);
	}
	
	public static void enterInstance(Player p) {
		if (!p.getStopWatch().checkThenStart(1)) {
			p.sendMessage("Please slow down.");
			return;
		}
		if (p.getClan() == null) {
			p.sendMessage(PRIV_ENTER_MSG);
			return;
		}
		
		int clanIndex = p.getClan().getClanMysqlId();
		
		int z = getOrCreateZ(clanIndex);
		
		KrakenInstance existing = getInstanceByZ(z);
		
		if (existing != null) { // does not exist yet
			existing.telePlayerInside(p);
		} else {
			//create an instance
			
			int length = Misc.secondsToTicks(3600) + (Math.min(p.getDonatedTotalAmount(), 1_000) * 20); // capped it at 1k which is about 4 hours?
			String timeFormatted = Misc.ticksToTime(length, false);
			
			p.getDialogueBuilder()
			.sendPlayerChat("Would you like to create an instance for 2 Vote Tickets?",
					"Your instance length is " + timeFormatted)
			.sendOption("Pay for the instance?", "Yes", ()-> {
				
				payForInstance(p, z, length);
				
			}, "No", ()-> {
				p.getDialogueBuilder().closeNew();
			})
			.execute();
			
		}
	}
	
	private static void payForInstance(Player p, int z, int seconds) {
		int payment = 2;
		int ticketId = 2996;
		if (p.getItems().playerHasItem(ticketId, payment)) {
			if (finalizeCreate(p, z, seconds)) {
				p.getItems().deleteItem2(ticketId, payment);
			}
		} else {
			p.sendMessage("You don't have enough Vote Tickets.");
		}
	}
	
	private static boolean finalizeCreate(Player p, int z, int seconds) {
		//in case someone creates or spam clicks to create instance
		if (getInstanceByZ(z) != null) {
			return false;
		}
		
		Location entrance = Location.create(
				LocationConstants.KRAKEN_ENTRANCE_INSTANCED.getX(),
				LocationConstants.KRAKEN_ENTRANCE_INSTANCED.getY(),
				z);
		
		KrakenInstance inst = KrakenInstance.createBossInstance(z, 8732, Location.create(10094, 5810, z), entrance, seconds, 0);
		
		instances.put(z, inst);
		
		inst.telePlayerInside(p);
		
		return true;
	}
	
	public static void removeInstance(int instanceKey) {
		instances.remove(instanceKey);
	}

	
	private static String PRIV_ENTER_MSG = "You need to be in a clan to enter or create the instance.";
}
