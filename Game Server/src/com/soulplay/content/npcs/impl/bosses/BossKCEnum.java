package com.soulplay.content.npcs.impl.bosses;

import java.util.HashMap;

public enum BossKCEnum {

	JAD(2745, "jad"),
	KBD(50, "kbd"),
	CHAOS_ELEMENTAL(3200, "chaos_elemental"),
	BARRELCHEST(5666, "barrelchest"),
	BANDOS(6260, "bandos"),
	ZAMORAK(6203, "zamorak"),
	SARADOMIN(6247, "saradomin"),
	ARMADYL(6222, "armadyl"),
	TORMENTED_DEMON(8349, "tormented_demon"),
	CORPOREAL_BEAST(8133, "corporeal_beast"),
	DAGGANOTH_PRIME(2882, "d_prime"),
	DAGGANOTH_REX(2883, "d_rex"),
	DAGGANOTH_SUPREME(2881, "d_supreme"),
	NEX(13447, "nex"),
	AVATAR_OF_DESTRUCTION(9176, "avatar_of_destruction"),
	BANDOS_AVATAR(4540, "bandos_avatar"),
	SCORPIA(4172, "scorpia"),
	CALLISTO(4174, "callisto"),
	VENENATIS(4173, "venenatis"),
	KALPHITE_QUEEN(1158, "kalphite_queen"),
	WILDY_WYRM(6090, "wildy_wyrm"),
	ZULRAH(2012, "zulrah"),
	GIANT_MOLE(3340, "giant_mole"),
	NOMAD(8528, "nomad"),
	KRAKEN(8732, "kraken"),
	AVATAR_OF_CREATION(8597, "avatar_of_creation"),
	SKELETAL_HORROR(9176, "skeletal_horror"),
	VORKATH(8970, "vorkath"),
	SAGITTARE(new int[] {9753, 9754, 9755, 9756, 9757, 9758, 9759, 9760, 9761, 9762, 9763, 9764, 9765, 9766}, "sagittare", true, 75),
	ASTEA_FROSTWEB(new int[] {9965, 9966, 9967, 9968, 9969, 9970, 9971, 9972, 9973, 9974, 9975, 9976, 9977, 9978, 9979,
			9980, 9981, 9982, 9983, 9984, 9985, 9986, 9987, 9988, 9989, 9990, 9991, 9992, 9993, 9994, 9995, 9996,
			9997, 9998, 9999, 10000, 10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010, 10011,
			10012, 10013, 10014, 10015, 10016, 10017, 10018, 10019, 10020, 10021}, "astea_frostweb", true, 90),
	GLUTTONOUS_BEHEMOTH(new int[] {9948, 9949, 9950, 9951, 9952, 9953, 9954, 9955, 9956, 9957, 9958, 9959, 9960, 9961, 9962, 9963, 9964}, "gluttonous_behemoth", true, 100),
	BULWARK_BEAST(new int[] {10073, 10074, 10075, 10076, 10077, 10078, 10079, 10080, 10081, 10082, 10083, 10084, 10085, 10086, 10087, 10088, 10089, 10090, 10091, 10092, 10093, 10094, 10095, 10096, 10097, 10098, 10099, 10100, 10101, 10102, 10103, 10104, 10105, 10106}, "bulwark_beast", true, 175),
	KALGER_WARMONGER(new int[] {12752, 12753, 12754, 12755, 12756, 12757, 12758, 12759, 12760, 12761, 12762, 12763, 12764, 12765, 12766}, "kalger_warmonger", true, 200),
	NECROLORD(new int[] {11737, 11738, 11739, 11740, 11741, 11742, 11743, 11744, 11745, 11746, 11747, 11748, 11749, 11750, 11751}, "necrolord", true, 185),
	DREADNAUT(new int[] {12848, 12849, 12850, 12851, 12852, 12853, 12854, 12855, 12856, 12857, 12858, 12859, 12860, 12861, 12862}, "dreadnaut", true, 185),
	GREAT_OLM(new int[] { 9000} , "great_olm", true, 0),
	VETION(4175, "vetion"),
	TZKAL_ZUK(9041, "zuk"),
	CRAZY_ARCHEOLOGIST(4186, "crazy_archeologist"),
	CHAOS_FANATIC(4190, "chaos_fanatic"),
	
	;
	
	public static final BossKCEnum[] values = values();

	private static HashMap<Integer, BossKCEnum> killcounts = new HashMap<>();

	static {
		for (final BossKCEnum kc : values) {
			for (int npcId : kc.npcIds) {
				BossKCEnum.killcounts.put(npcId, kc);
			}
		}
	}
	
    public static void load() {
    	/* empty */
    }
 
	public static BossKCEnum forId(int id) {
		return BossKCEnum.killcounts.get(id);
	}

	private final int[] npcIds;
	private final boolean rewardAll;
	private final String mysqlName;
	private final int buyKc;

	private BossKCEnum(int npcId, String mysqlName) {
		this(new int[] { npcId }, mysqlName, false, 0);
	}

	private BossKCEnum(int[] npcIds, String mysqlName, boolean rewardAll, int buyKc) {
		this.npcIds = npcIds;
		this.mysqlName = mysqlName;
		this.rewardAll = rewardAll;
		this.buyKc = buyKc;
	}

	public String getMysqlName() {
		return mysqlName;
	}

	public boolean rewardAll() {
		return rewardAll;
	}

	public int getBuyKc() {
		return buyKc;
	}
}
