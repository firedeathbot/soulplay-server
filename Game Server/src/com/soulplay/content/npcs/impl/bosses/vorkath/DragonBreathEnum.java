package com.soulplay.content.npcs.impl.bosses.vorkath;

import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public enum DragonBreathEnum {

	NORMAL(393, 131/*1466*/),
	VENOM(1470, 1472),
	PINK(1471, 1473)
	;
	
	private final int projId;
	private final Graphic hitGraphic;
	
	public int getProjId() {
		return projId;
	}

	public Graphic getHitGraphic() {
		return hitGraphic;
	}

	private DragonBreathEnum(int projId, int hitGfxId) {
		this.projId = Graphic.getOsrsId(projId);
		this.hitGraphic = Graphic.create(Graphic.getOsrsId(hitGfxId), GraphicType.HIGH);
	}
}
