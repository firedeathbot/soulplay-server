package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItemObject;
import com.soulplay.game.model.item.RangeItem;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.util.Misc;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.ObjectManager;

import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

import static com.soulplay.content.npcs.impl.bosses.nightmare.NightmareManager.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;

@NpcMeta(ids = { 109433, 109425, 109426, 109427, 109428, 109429, 109430, 109431 })
public class NightmareNPC extends AbstractNpc {

	private static final Location ARENA_START = Location.create(10263, 9942, 3);
	private static final Location ARENA_END = Location.create(10281, 9960, 3);
	public static final int SLEEPING = 109433;
	public static final int AWAKE_SHIELD_P1 = 109425;
	public static final int AWAKE_SHIELD_P2 = 109426;
	public static final int AWAKE_SHIELD_P3 = 109427;
	public static final int AWAKE_P1 = 109428;
	public static final int AWAKE_P2 = 109429;
	public static final int AWAKE_P3 = 109430;
	public static final int EXPLODING = 109431;
	public static int playersOnStart;
	public static List<NPC> spawns = new ArrayList<>();
	public static FlowerAttackData activeQuadrant;
	public static boolean totemAttack;
	public static int absorbed;

	public NightmareNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareNPC(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		setDropsItemOnDeath(false);
		stopAttackDefault = true;
		disableAggression();
		ignoreAggroTolerence();
	}

	public void enableAggression() {
		isAggressive = true;
		setRetaliates(true);
		setRetargetTick(18);
		selectAggressionPlayer = aggressionBehavior;
	}

	private static Function<IntList, Integer> aggressionBehavior = playerList -> {
		int highestBonus = 0;
		int index = -1;

		for (int i = 0, length = playerList.size(); i < length; i++) {
			int playerIndex = playerList.getInt(i);
			Player player = PlayerHandler.players[playerIndex];
			if (player == null || !player.isActive()) {
				continue;
			}

			int[] bonuses = player.getPlayerBonus();
			int maxBonus = Math.max(bonuses[EquipmentBonuses.DEFENSE_STAB.getId()], Math.max(bonuses[EquipmentBonuses.DEFENSE_SLASH.getId()], bonuses[EquipmentBonuses.DEFENSE_CRUSH.getId()]));
			if (maxBonus > highestBonus) {
				highestBonus = maxBonus;
				index = playerIndex;
			}
		}

		return index;
	};

	public void disableAggression() {
		isAggressive = false;
		setRetaliates(false);
		resetAttack();
		face(null);
	}

	public void awaken() {
		startAnimation(Animation.osrs(8611));

		pulse(() -> {
			enableAggression();
			playersOnStart = Math.min(NightmareManager.players.size(), 80);
			getInflictors().clear();
			transform(AWAKE_SHIELD_P1);
			
			NightmareManager.forEachPlayer(player -> showHud(player));
		}, 4);
	}

	private void showHud(Player player) {
		//TODO HP Hud not the totem hud.
		//player.getPA().walkableInterface(50200);
	}

	public void death(boolean death) {
		clearSpawns();
		transform(NightmareNPC.SLEEPING);
		startAnimation(Animation.osrs(8612));
		disableAggression();
		disableTotems();

		if (!death) {
			return;
		}

		pulse(() -> {
			NightmareManager.checkWakePulse(17);
			NightmareManager.lobbyNPC.stall();

			int nighmareStaffRate = Misc.interpolate(750, 600, 1, 80, playersOnStart);
			boolean droppedStaff = Misc.random(nighmareStaffRate) == 0;

			int uniqueRate = Misc.interpolate(250, 200, 1, 80, playersOnStart);
			boolean droppedUnique = false;

			Player mvp = null;
			int mvpDamage = 0;
			int minDamage = 50;
			int maxDamage = 400;
			for (final Entry<Integer, Integer> entry : getInflictors().entrySet()) {
				final Player player = PlayerHandler.getPlayerByMID(entry.getKey());
				if (player == null || !player.isActive) {
					continue;
				}

				final int amount = entry.getValue();
				if (amount >= minDamage) {
					int percent = Misc.interpolate(1, 100, minDamage, maxDamage, Math.min(amount, maxDamage));

					RangeItem rangeItem;
					int random = Misc.random(26);

					switch (random) {
						case 0:
							rangeItem = new RangeItem(890, 32, 196);
							break;
						case 1:
							rangeItem = new RangeItem(892, 12, 271);
							break;
						case 2:
							rangeItem = new RangeItem(564, 15, 125);
							break;
						case 3:
							rangeItem = new RangeItem(561, 6, 83);
							break;
						case 4:
							rangeItem = new RangeItem(560, 24, 69);
							break;
						case 5:
							rangeItem = new RangeItem(565, 13, 40);
							break;
						case 6:
							rangeItem = new RangeItem(890, 12, 57);
							break;
						case 7:
							rangeItem = new RangeItem(7937, 420, 1414);
							break;
						case 8:
							rangeItem = new RangeItem(1622, 1, 16);
							break;
						case 9:
							rangeItem = new RangeItem(1620, 2, 13);
							break;
						case 10:
							rangeItem = new RangeItem(1516, 14, 45);
							break;
						case 11:
							rangeItem = new RangeItem(1514, 3, 30);
							break;
						case 12:
							rangeItem = new RangeItem(445, 14, 78);
							break;
						case 13:
							rangeItem = new RangeItem(454, 16, 114);
							break;
						case 14:
							rangeItem = new RangeItem(448, 15, 50);
							break;
						case 15:
							rangeItem = new RangeItem(450, 8, 33);
							break;
						case 16:
							rangeItem = new RangeItem(216, 1, 7);
							break;
						case 17:
							rangeItem = new RangeItem(220, 1, 10);
							break;
						case 18:
							rangeItem = new RangeItem(385, 1, 7);
							break;
						case 19:
							rangeItem = new RangeItem(365, 1, 16);
							break;
						case 20:
							rangeItem = new RangeItem(139, 2, 10);
							break;
						case 21:
							rangeItem = new RangeItem(2434, 3, 3);
							break;
						case 22:
							rangeItem = new RangeItem(6687, 1, 9);
							break;
						case 23:
							rangeItem = new RangeItem(2450, 1, 4);
							break;
						case 24:
							rangeItem = new RangeItem(220, 10927, 11);
							break;

						default:
							rangeItem = new RangeItem(995, 2717, 21800);
							break;
					}

					int dropAmount = Misc.interpolate(rangeItem.getMinAmount(), rangeItem.getMaxAmount(), 1, 100,
							percent);
					DropTable.itemsToDrop.add(new GroundItemObject(player, rangeItem.getId(), dropAmount));
					player.sendMessage(
							"<col=337F33>An item was dropped: " + dropAmount + " x " + rangeItem.getName() + ".</col>");
					if (!droppedUnique && Misc.random(uniqueRate) == 0) {
						droppedUnique = true;

						int uniqueId;
						int uniqueRoll = Misc.random(24);
						if (uniqueRoll >= 21) {
							uniqueId = 124417;
						} else if (uniqueRoll >= 16) {
							uniqueId = 124419;
						} else if (uniqueRoll >= 11) {
							uniqueId = 124420;
						} else if (uniqueRoll >= 6) {
							uniqueId = 124421;
						} else if (uniqueRoll >= 4) {
							uniqueId = 124511;
						} else if (uniqueRoll >= 2) {
							uniqueId = 124517;
						} else {
							uniqueId = 124514;
						}

						GameItem uniqueItem = new GameItem(uniqueId);
						DropTable.itemsToDrop
								.add(new GroundItemObject(player, uniqueItem.getId(), uniqueItem.getAmount()));
						player.sendMessage("<col=337F33>An item was dropped: " + uniqueItem.getAmount() + " x "
								+ uniqueItem.getName() + ".</col>");
					}
				}

				if (amount > mvpDamage) {
					mvpDamage = amount;
					mvp = player;
				}
			}

			if (mvp != null) {
				DropTable.itemsToDrop.add(new GroundItemObject(mvp, 532, 1));
				if (droppedStaff) {
					DropTable.itemsToDrop.add(new GroundItemObject(mvp, 124422, 1));
					mvp.sendMessage("<col=337F33>An item was dropped: 1 x Nightmare staff.</col>");
				}
			}
		}, 6);
	}

	@Override
	public void process() {
		super.process();

		if (getAttackTimer() > 1 || getKillerId() < 0) {
			return;
		}

		Player enemy = PlayerHandler.players[getKillerId()];
		if (enemy == null || !enemy.isActive) {
			return;
		}

		swing(enemy);
	}

	private void swing(Player player) {
		setAttackTimer(8);
 
		if (Misc.random(10) == 0) {
			Phase phase = getPhase();
			if (phase != null) {
				int random = Misc.random(2);
				switch (phase) {
					case FIRST:
						if (random == 0 && activeQuadrant == null) {
							flowerPowerAttack();
						} else if (random == 1) {
							husksAttack();
						} else {
							graspingClawAttack();
						}
						return;
					case SECOND:
						if (random == 0) {
							parasiteAttack();
						} else if (random == 1) {
							curseAttack();
						} else {
							graspingClawAttack();
						}
						return;
					case THIRD:
						if (random == 0) {
							sporesAttack();
						} else if (random == 1) {
							chargeAttack();
						} else {
							graspingClawAttack();
						}
						return;
				}
			}
		}

		CombatType attackStyle;
		if (Misc.randomBoolean()) {
			attackStyle = CombatType.MAGIC;
		} else if (getCenterLocation().getDistance(player.getCurrentLocation()) <= getSize() && Misc.randomBoolean()) {
			attackStyle = CombatType.MELEE;
		} else {
			attackStyle = CombatType.RANGED;
		}

		if (attackStyle != CombatType.MELEE) {
			mageRangeAttack(attackStyle);
		} else {
			meleeAttack(player);
		}
	}

	private void chargeAttack() {
		setAttackTimer(6);
		disableAggression();
		ChargeData chargeData = Misc.random(ChargeData.values);
		Location npcStart = chargeData.getNpcStart();
		Location npcEnd = chargeData.getNpcEnd();

		jump(npcStart);
		pulse(() -> {
			int offsetX = npcEnd.getX() - npcStart.getX();
			int offsetY = npcEnd.getY() - npcStart.getY();
			Direction direction = Direction.getLogicalDirection(npcStart, npcEnd);
			ForceMovementMask mask1 = ForceMovementMask.createMask(offsetX, offsetY, 2, Animation.getOsrsAnimId(8597), direction);
			mask1.addStartAnim(mask1.getFirstMovementAnim());
			mask1.setTeleLoc(npcEnd);
			setForceMovement(mask1);

			Rectangle zoneBorders = new Rectangle(chargeData.getCheckStart(), chargeData.getCheckEnd());
			NightmareManager.forEachPlayer(player -> {
				if (!zoneBorders.inside(player.getCurrentLocation())) {
					return;
				}

				player.dealDamage(new Hit(Misc.random(60)));
			});

			enableAggression();
		}, 6);
	}

	private enum ChargeData {
		FIRST(Location.create(8869, 12001, 3), Location.create(8855, 12001, 3),
				Location.create(8855, 12002, 3), Location.create(8873, 12005, 3)),
		SECOND(Location.create(8862, 12005, 3), Location.create(8862, 11989, 3),
				Location.create(8862, 11989, 3), Location.create(8866, 12008, 3)),
		THIRD(Location.create(8862, 11989, 3), Location.create(8862, 12005, 3),
				Location.create(8862, 11989, 3), Location.create(8866, 12008, 3)),
		FOURTH(Location.create(8855, 11993, 3), Location.create(8869, 11993, 3),
				Location.create(8855, 11993, 3), Location.create(8873, 11996, 3));
		
		public static ChargeData[] values = values();
		private final Location npcStart, npcEnd, checkStart, checkEnd;
		
		ChargeData(Location npcStart, Location end, Location checkStart, Location checkEnd) {
			this.npcStart = npcStart.transform(Location.create(-4992 + 6400, -2048));
			this.npcEnd = end.transform(Location.create(-4992 + 6400, -2048));
			this.checkStart = checkStart.transform(Location.create(-4992 + 6400, -2048));
			this.checkEnd = checkEnd.transform(Location.create(-4992 + 6400, -2048));
		}
		
		public Location getNpcStart() {
			return npcStart;
		}
		
		public Location getNpcEnd() {
			return npcEnd;
		}
		
		public Location getCheckStart() {
			return checkStart;
		}
		
		public Location getCheckEnd() {
			return checkEnd;
		}
		
	}

	private void sporesAttack() {
		startAnimation(Animation.osrs(8599));

		List<RSObject> sporesList = new ArrayList<>();
		int x = ARENA_END.getX() - ARENA_START.getX();
		int y = ARENA_END.getY() - ARENA_START.getY();
		IntSet coords = new IntOpenHashSet();

		for (int i = 0; i < 30; i++) {
			int randomX = Misc.random(x);
			int randomY = Misc.random(y);
			int bitPack = randomX << 16 | randomY;
			if (coords.contains(bitPack)) {
				continue;
			}

			final Location sporeLocation = ARENA_START.transform(randomX, randomY);
			if (!RegionClip.isFloorFree(RegionClip.getClipping(sporeLocation.getX(), sporeLocation.getY(), sporeLocation.getZ()))) {
				continue;
			}

			coords.add(bitPack);
			RSObject gameObject = new RSObject(137738, sporeLocation, 0, 10);
			gameObject.setTick(Integer.MAX_VALUE);
			ObjectManager.addObject(gameObject);
			sporesList.add(gameObject);
		}

		final int length = sporesList.size();
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			int ticks = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (ticks == 1) {
					for (int i = 0; i < length; i++) {
						RSObject sporesObject = sporesList.get(i);
						sporesObject.animate(Animation.getOsrsAnimId(8630));
					}

					NightmareManager.forEachPlayer(player -> player.sendMessage("<col=E00A19>The Nightmare summons some infectious spores!"));
				} else if (ticks == 4) {
					for (int i = 0; i < length; i++) {
						RSObject sporesObject = sporesList.get(i);
						sporesObject.changeObject(137739);
					}
				} else if (ticks > 4) {
					if (ticks == 5 + 24) {
						for (int i = 0; i < sporesList.size(); i++) {
							RSObject sporesObject = sporesList.get(i);
							sporesObject.animate(Animation.getOsrsAnimId(8632));

							NightmareManager.forEachPlayer(player -> {
								int distance = player.getCurrentLocation().getDistanceInt(sporesObject.getX(), sporesObject.getY());
								if (distance > 1) {
									return;
								}

								drowsyPlayer(player);
							});
						}
					} else if (ticks == 7 + 24) {
						for (int i = 0; i < sporesList.size(); i++) {
							RSObject sporesObject = sporesList.get(i);
							sporesObject.setTick(0);
							sporesObject.changeObject(-1);
						}

						sporesList.clear();
						container.stop();
						return;
					} else {
						Iterator<RSObject> iterator = sporesList.iterator();
						while (iterator.hasNext()) {
							RSObject sporesObject = iterator.next();
							AtomicBoolean popped = new AtomicBoolean(false);

							NightmareManager.forEachPlayer(player -> {
								int distance = player.getCurrentLocation().getDistanceInt(sporesObject.getX(), sporesObject.getY());
								if (distance > 1) {
									return;
								}

								if (!popped.get()) {
									sporesObject.animate(Animation.getOsrsAnimId(8632));
									pulse(() -> {
										sporesObject.setTick(0);
										sporesObject.changeObject(-1);
									}, 2);

									popped.set(true);
									iterator.remove();
								}

								drowsyPlayer(player);
							});
						}
					}
				}

				ticks++;
			}

		}, 1);
	}
	
	private void drowsyPlayer(Player player) {
		if (player.attr().has("nightmare_drowsy")) {
			return;
		}

		player.sendMessage("<col=E00A19>The Nightmare's spores have infected you, making you feel drowsy.");
		player.blockRun = true;
		player.attr().put("nightmare_drowsy", true);
		player.pulse(() -> {
			player.attr().remove("nightmare_drowsy");
			player.blockRun = false;
			player.sendMessage("<col=229628>The Nightmare's infection has wore off.");
		}, 25);
	}

	private void parasiteAttack() {
		startAnimation(Animation.osrs(8606));
		
		NightmareManager.forEachPlayer(player -> {
			Projectile proj = Projectile.create(this, player, Graphic.getOsrsId(1770));/*, 100,
					20, 65, 140, 1, 128);*/
			proj.send();
			
			player.pulse(() -> {
				player.sendMessage("<col=ff289d>The Nightmare has impregnated you with a deadly parasite!");
				player.startAnimation(player.getCombat().getBlockEmote());
				player.attr().put("nightmare_parasite", true);
				player.attr().put("nightmare_spawn_parasite", true);
				player.pulse(() -> {
					if (!player.attr().contains("nightmare_spawn_parasite")) {
						return;
					}

					player.sendMessage("<col=E00A19>The parasite bursts out of you, fully grown!");
					boolean weakened = !player.attr().contains("nightmare_parasite");
					player.attr().remove("nightmare_parasite");
					player.attr().remove("nightmare_spawn_parasite");
					Graphic graphics;
					Animation spawnAnimation;
					int id;
					int extraHp;

					if (weakened) {
						graphics = Graphic.osrs(1780);
						spawnAnimation = Animation.osrs(8560);
						id = NightmareParasiteNPC.ID_WEAKEN;
						extraHp = 0;
					} else {
						graphics = Graphic.osrs(1779);
						spawnAnimation = Animation.osrs(8561);
						id = NightmareParasiteNPC.ID;
						extraHp = Misc.random(55);
						player.dealDamage(new Hit(extraHp));
					}
					
					NightmareParasiteNPC parasite = (NightmareParasiteNPC) NPCHandler.spawnNpc(id, player.getCurrentLocation().transform(1, 0, 0));
					parasite.nightmareNPC = this;
					parasite.extraHp = extraHp;
					parasite.startAnimation(spawnAnimation);
					parasite.attack(player);
					
					//TODO animation for player? lol
					player.startGraphic(graphics);
					spawns.add(parasite);
				}, 20);
			}, 4);
		});
	}

	private void curseAttack() {
		startAnimation(Animation.osrs(8599));

		NightmareManager.forEachPlayer(player -> player.getPacketSender().nightmareFog());
		pulse(() -> NightmareManager.forEachPlayer(player -> curseAttackPlayer(player)), 2);
	}

	public static void curseAttackPlayer(Player player) {
		player.sendMessage("<col=a53fff>The Nightmare has cursed you, shuffling your prayers!");

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int ticks = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (ticks >= 35) {
					player.attr().remove("nightmare_curse");
					player.sendMessage("<col=229628>You feel the effects of the Nightmare's curse wear off.");
					for (Prayers prayerType : protectionPrayers) {
						if (player.prayerActive[prayerType.ordinal()]) {
							player.getCombat().activatePrayer(cursePrayerTypeReverse(prayerType).ordinal());
							break;
						}
					}

					for (Curses prayerType : protectionCursePrayers) {
						if (player.curseActive[prayerType.ordinal()]) {
							player.curses().activateCurse(cursePrayerCurseTypeReverse(prayerType).ordinal());
							break;
						}
					}

					container.stop();
					return;
				}
				
				ticks++;
			}
			
		}, 1);

		player.attr().put("nightmare_curse", true);

		for (Prayers prayerType : protectionPrayers) {
			if (player.prayerActive[prayerType.ordinal()]) {
				player.getCombat().activatePrayer(cursePrayerType(prayerType).ordinal());
				break;
			}
		}

		for (Curses prayerType : protectionCursePrayers) {
			if (player.curseActive[prayerType.ordinal()]) {
				player.curses().activateCurse(cursePrayerCurseType(prayerType).ordinal());
				break;
			}
		}
	}

	public static final Prayers[] protectionPrayers = { Prayers.PROTECT_FROM_MAGIC, Prayers.PROTECT_FROM_MISSILES, Prayers.PROTECT_FROM_MELEE };

	public static Prayers cursePrayerType(Prayers input) {
		switch (input) {
			case PROTECT_FROM_MAGIC:
				return Prayers.PROTECT_FROM_MISSILES;
			case PROTECT_FROM_MISSILES:
				return Prayers.PROTECT_FROM_MELEE;
			case PROTECT_FROM_MELEE:
				return Prayers.PROTECT_FROM_MAGIC;
			default:
				return input;
		}
	}

	public static Prayers cursePrayerTypeReverse(Prayers input) {
		switch (input) {
			case PROTECT_FROM_MISSILES:
				return Prayers.PROTECT_FROM_MAGIC;
			case PROTECT_FROM_MELEE:
				return Prayers.PROTECT_FROM_MISSILES;
			case PROTECT_FROM_MAGIC:
				return Prayers.PROTECT_FROM_MELEE;
			default:
				return input;
		}
	}

	public static final Curses[] protectionCursePrayers = { Curses.DEFLECT_MAGIC, Curses.DEFLECT_MISSILES, Curses.DEFLECT_MELEE };

	public static Curses cursePrayerCurseType(Curses input) {
		switch (input) {
			case DEFLECT_MAGIC:
				return Curses.DEFLECT_MISSILES;
			case DEFLECT_MISSILES:
				return Curses.DEFLECT_MELEE;
			case DEFLECT_MELEE:
				return Curses.DEFLECT_MAGIC;
			default:
				return input;
		}
	}

	public static Curses cursePrayerCurseTypeReverse(Curses input) {
		switch (input) {
			case DEFLECT_MISSILES:
				return Curses.DEFLECT_MAGIC;
			case DEFLECT_MELEE:
				return Curses.DEFLECT_MISSILES;
			case DEFLECT_MAGIC:
				return Curses.DEFLECT_MELEE;
			default:
				return input;
		}
	}

	@Override
	public int dealDamage(Hit hit) {
		int damage = hit.getDamage();
		if (damage > 0) {
			Mob source = hit.getSource();
			if (source != null && activeQuadrant != null && !activeQuadrant.getSafeArea().inside(source.getCurrentLocation())) {
				getSkills().heal(damage);
				//TODO heal hitsplat
				return 0;
			}

			if (isShieldNpc()) {
				//TODO override hitsplat to shield, if we add or something.
			} else if (isAwakeNpc()) {
				hit.setDamage(0);
			}
		}

		int rawDamage = super.dealDamage(hit);

		if (getSkills().getLifepoints() <= 0) {
			Phase phase = getPhase();
			if (phase != null) {
				enableTotems();

				if (getId() != Phase.THIRD.getNextAwakeId()) {
					transform(phase.getNextAwakeId());

					NightmareManager.forEachPlayer(player -> {
						player.getPA().walkableInterface(50200);
						player.getPacketSender().sendNightmareBars(300, 0, 300, 0, 300, 0, 300, 0, true);
					});
				}
			}
		}

		return rawDamage;
	}

	public static void enableTotems() {
		int hp = Misc.interpolate(300, 1500, 1, 80, playersOnStart);

		configureTotem(NightmareManager.totemNE, hp, NightmareTotemNE.CHARGE);
		configureTotem(NightmareManager.totemNW, hp, NightmareTotemNW.CHARGE);
		configureTotem(NightmareManager.totemSE, hp, NightmareTotemSE.CHARGE);
		configureTotem(NightmareManager.totemSW, hp, NightmareTotemSW.CHARGE);
		totemAttack = true;
	}

	public static void disableTotems() {
		configureTotem(NightmareManager.totemNE, 1, NightmareTotemNE.REGULAR);
		configureTotem(NightmareManager.totemNW, 1, NightmareTotemNW.REGULAR);
		configureTotem(NightmareManager.totemSE, 1, NightmareTotemSE.REGULAR);
		configureTotem(NightmareManager.totemSW, 1, NightmareTotemSW.REGULAR);
	}

	public static void updateTotemHud() {
		int swMax = NightmareManager.totemSW.getSkills().getMaximumLifepoints();
		int swLp = NightmareManager.totemSW.getLpFixed();
		int seMax = NightmareManager.totemSE.getSkills().getMaximumLifepoints();
		int seLp = NightmareManager.totemSE.getLpFixed();
		int nwMax = NightmareManager.totemNW.getSkills().getMaximumLifepoints();
		int nwLp = NightmareManager.totemNW.getLpFixed();
		int neMax = NightmareManager.totemNE.getSkills().getMaximumLifepoints();
		int neLp = NightmareManager.totemNE.getLpFixed();

		NightmareManager.forEachPlayer(player -> {
			player.getPacketSender().sendNightmareBars(swMax, swLp, seMax, seLp, nwMax, nwLp, neMax, neLp, false);
		});
	}

	public static void totemAttack() {
		if (!totemAttack) {
			return;
		}

		if (!NightmareManager.totemSW.isCharged() || !NightmareManager.totemSE.isCharged() || !NightmareManager.totemNW.isCharged() || !NightmareManager.totemNE.isCharged()) {
			return;
		}

		NightmareManager.forEachPlayer(player -> player.sendMessage("<col=E00A19>All four totems are fully charged."));
		totemAttack = false;
		totemProjectile(NightmareManager.totemSW);
		totemProjectile(NightmareManager.totemSE);
		totemProjectile(NightmareManager.totemNW);
		totemProjectile(NightmareManager.totemNE);

		NightmareManager.boss.pulse(() -> {
			NightmareManager.boss.setAttackTimer(30);
			NightmareManager.boss.resetAnimations();

			NightmareManager.boss.pulse(() -> {
				Phase phase = NightmareManager.boss.getPhase();
				NightmareManager.boss.startGraphic(Graphic.osrs(1769));
				NightmareManager.boss.dealDamage(new Hit(800));

				boolean dead = phase.getNextAwakeShieldId() == -1;
				if (dead) {
					NightmareManager.boss.death(true);
				} else {
					NightmareManager.boss.cosineMomsAttack(phase);
					NightmareNPC.disableTotems();
				}

				NightmareManager.forEachPlayer(player -> {
					NightmareManager.boss.showHud(player);
					player.getPacketSender().sendNightmareFadeOut();
					player.pulse(() -> {
						player.getPA().walkableInterface(-1);
					});
				});
			}, 2);
		}, 2);
	}

	private void cosineMomsAttack(Phase phase) {
		absorbed = 0;
		transform(EXPLODING);
		disableAggression();
		pulse(() -> {
			startAnimation(Animation.osrs(8608));
			pulse(() -> {
				moveNpc(Location.create(10270, 9949, 3));
				startAnimation(Animation.osrs(8610));
				
				int walkers = Misc.interpolate(1, 24, 1, 80, playersOnStart);
				
				pulse(() -> {
					NightmareManager.forEachPlayer(player -> player.sendMessage("<col=E00A19>The Nightmare begins to charge up a devastating attack."));
					
					for (int i = 0; i < walkers; i++) {
						NightmareSleepwalker sleepwalker = (NightmareSleepwalker) NPCHandler.spawnNpc(Misc.random(NightmareSleepwalker.IDS), SleepWalkerData.values[i].getLocation());
						sleepwalker.boss = this;
						sleepwalker.startAnimation(Animation.osrs(8572));
						sleepwalker.pulse(() -> sleepwalker.startFollow = true, 4);

						spawns.add(sleepwalker);
					}

					pulse(() -> {
						int maxhit;
						if (playersOnStart > 1) {
							maxhit = absorbed * 4 + 5;
						} else {
							if (absorbed > 0) {
								maxhit = Integer.MAX_VALUE;
							} else {
								maxhit = 5;
							}
						}

						startAnimation(Animation.osrs(8604));
						pulse(() -> {
							NightmareManager.forEachPlayer(player -> {
								player.startGraphic(Graphic.osrs(1782));
								player.pulse(() -> player.dealDamage(new Hit(Math.min(player.getSkills().getLifepoints(), maxhit))));
							});
							pulse(() -> {
								transform(phase.getNextAwakeShieldId());
								NightmareManager.forEachPlayer(player -> {
									player.sendMessage("<col=E00A19>The Nightmare restores her shield.");
									showHud(player);
								});
							}, 6);
						}, 3);
					}, 12);
				}, 3);
			}, 2);
		}, 3);
	}

	public enum SleepWalkerData {
		WALKER1(Location.create(8858, 12008, 3)),
		WALKER2(Location.create(8859, 12008, 3)),
		WALKER3(Location.create(8860, 12008, 3)),
		
		WALKER4(Location.create(8870, 12008, 3)),
		WALKER5(Location.create(8869, 12008, 3)),
		WALKER6(Location.create(8868, 12008, 3)),
		
		WALKER7(Location.create(8873, 12005, 3)),
		WALKER8(Location.create(8873, 12004, 3)),
		WALKER9(Location.create(8873, 12003, 3)),
		
		WALKER10(Location.create(8873, 11995, 3)),
		WALKER11(Location.create(8873, 11994, 3)),
		WALKER12(Location.create(8873, 11993, 3)),
		
		WALKER13(Location.create(8870, 11990, 3)),
		WALKER14(Location.create(8869, 11990, 3)),
		WALKER15(Location.create(8868, 11990, 3)),
		
		WALKER16(Location.create(8858, 11990, 3)),
		WALKER17(Location.create(8859, 11990, 3)),
		WALKER18(Location.create(8860, 11990, 3)),
		
		WALKER19(Location.create(8855, 11993, 3)),
		WALKER20(Location.create(8855, 11994, 3)),
		WALKER21(Location.create(8855, 11995, 3)),
		
		WALKER22(Location.create(8855, 12005, 3)),
		WALKER23(Location.create(8855, 12004, 3)),
		WALKER24(Location.create(8855, 12003, 3));
		
		public static SleepWalkerData[] values = values();
		private final Location location;
		
		SleepWalkerData(Location location) {
			this.location = location.transform(Location.create(-4992 + 6400, -2048));
		}
		
		public Location getLocation() {
			return location;
		}
		
	}

	public static void totemProjectile(NPC npc) {
		Projectile projectile = Projectile.create(npc, NightmareManager.boss, Graphic.getOsrsId(1768));//, 150, 50, 10, 100, 1, 100);
		projectile.send();
	}

	private static void configureTotem(NPC npc, int hp, int newId) {
		npc.getSkills().setMaximumLifepoints(Math.max(1, hp));
		npc.getSkills().setLifepoints(hp);
		npc.setDead(false);
		npc.transform(newId);
	}

	private void flowerPowerAttack() {
		//setAttackTimer(40);

		disableAggression();
		jumpCenter();
		
		pulse(() -> {
			startAnimation(Animation.osrs(8601));

			activeQuadrant = Misc.random(FlowerAttackData.values);
			Location[] berries = activeQuadrant.getBerries();
			Location[] blossoms = activeQuadrant.getBlossoms();
			int berriesLength = berries.length;
			int blossomsLength = blossoms.length;
			List<RSObject> berriesList = new ArrayList<>(berriesLength);
			List<RSObject> blossomsList = new ArrayList<>(blossomsLength);

			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				int ticks = 0;

				@Override
				public void execute(CycleEventContainer container) {
					if (ticks == 0) {
						for (int i = 0; i < berriesLength; i++) {
							RSObject berriesObject = new RSObject(137740, berries[i], 0, 10);
							berriesObject.setTick(Integer.MAX_VALUE);
							ObjectManager.addObject(berriesObject);
							berriesList.add(berriesObject);
						}

						for (int i = 0; i < blossomsLength; i++) {
							RSObject blossomsObject = new RSObject(137743, blossoms[i], 0, 10);
							blossomsObject.setTick(Integer.MAX_VALUE);
							ObjectManager.addObject(blossomsObject);
							blossomsList.add(blossomsObject);
						}
					} else if (ticks == 1) {
						for (int i = 0; i < berriesLength; i++) {
							RSObject berriesObject = berriesList.get(i);
							berriesObject.animate(Animation.getOsrsAnimId(8623));
						}

						for (int i = 0; i < blossomsLength; i++) {
							RSObject blossomsObject = blossomsList.get(i);
							blossomsObject.animate(Animation.getOsrsAnimId(8617));
						}
					} else if (ticks == 2) {
						for (int i = 0; i < berriesLength; i++) {
							RSObject berriesObject = berriesList.get(i);
							berriesObject.changeObject(137741);
						}

						for (int i = 0; i < blossomsLength; i++) {
							RSObject blossomsObject = blossomsList.get(i);
							blossomsObject.changeObject(137744);
						}
						
						NightmareManager.forEachPlayer(player -> player.sendMessage("<col=E00A19>The Nightmare splits the area into segments!"));
					} else if (ticks == 7) {
						for (int i = 0; i < berriesLength; i++) {
							RSObject berriesObject = berriesList.get(i);
							berriesObject.animate(Animation.getOsrsAnimId(8625));
						}
						
						for (int i = 0; i < blossomsLength; i++) {
							RSObject blossomsObject = blossomsList.get(i);
							blossomsObject.animate(Animation.getOsrsAnimId(8619));
						}
					} else if (ticks == 8) {
						for (int i = 0; i < berriesLength; i++) {
							RSObject berriesObject = berriesList.get(i);
							berriesObject.changeObject(137742);
						}
						
						for (int i = 0; i < blossomsLength; i++) {
							RSObject blossomsObject = blossomsList.get(i);
							blossomsObject.changeObject(137745);
						}
						
						enableAggression();
					} else if (ticks > 8) {
						if (ticks == 8 + 22) {
							for (int i = 0; i < berriesLength; i++) {
								RSObject berriesObject = berriesList.get(i);
								berriesObject.animate(Animation.getOsrsAnimId(8627));
							}
							
							for (int i = 0; i < blossomsLength; i++) {
								RSObject blossomsObject = blossomsList.get(i);
								blossomsObject.animate(Animation.getOsrsAnimId(8621));
							}
						} else if (ticks == 9 + 22) {
							for (int i = 0; i < berriesLength; i++) {
								RSObject berriesObject = berriesList.get(i);
								berriesObject.setTick(0);
								berriesObject.setUpdateRequired(true);
								berriesObject.changeObject(-1);
							}
							
							for (int i = 0; i < blossomsLength; i++) {
								RSObject blossomsObject = blossomsList.get(i);
								blossomsObject.setTick(0);
								blossomsObject.setUpdateRequired(true);
								blossomsObject.changeObject(-1);
							}

							berriesList.clear();
							blossomsList.clear();
							activeQuadrant = null;
							container.stop();
							return;
						} else {
							NightmareManager.forEachPlayer(player -> {
								if (activeQuadrant != null && activeQuadrant.getSafeArea().inside(player.getCurrentLocation())) {
									return;
								}

								Projectile projectile = Projectile.create(Misc.random(berries), player, Graphic.getOsrsId(1783));
								projectile.send();

								player.dealDamage(new Hit(Misc.random(4, 6)));
							});
						}
					}
					
					ticks++;
				}

			}, 1);
		}, 6);
	}

	private void husksAttack() {
		setAttackTimer(6);
		startAnimation(Animation.osrs(8605));
		
		List<Integer> players = new ArrayList<>(NightmareManager.players);
		Collections.shuffle(players);
		
		int playersSize = players.size();
		int husksMax = Misc.interpolate(1, 15, 1, 80, Math.min(playersSize, 80));
		int husksAmount = Misc.random(1, Math.min(husksMax, playersSize));
		
		for (int i = 0; i < husksAmount; i++) {
			Player player = PlayerHandler.getPlayerByMID(players.get(i));
			if (player == null || !player.isActive() || player.getFreezeTimer() > 0) {
				continue;
			}

			Projectile proj = Projectile.create(this, player, Graphic.getOsrsId(1781));
			proj.setStartHeight(100);
			proj.setEndHeight(20);
			proj.setStartDelay(65);
			proj.setEndDelay(115);
			proj.setSlope(1);
			proj.setStartOffset(128);
			proj.send();
			proj.send();
		}

		pulse(() -> {
			for (int i = 0; i < husksAmount; i++) {
				Player player = PlayerHandler.getPlayerByMID(players.get(i));
				if (player == null || !player.isActive() || player.getFreezeTimer() > 0) {
					continue;
				}
	
				NightmareHuskMagicNpc magicHusk = (NightmareHuskMagicNpc) NPCHandler.spawnNpc(NightmareHuskMagicNpc.ID, player.getCurrentLocation().transform(1, 0, 0));
				magicHusk.spawnedFor = player;
				magicHusk.startAnimation(Animation.osrs(8567));
				magicHusk.attack(player);
				
				NightmareHuskRangedNpc rangedHusk = (NightmareHuskRangedNpc) NPCHandler.spawnNpc(NightmareHuskRangedNpc.ID, player.getCurrentLocation().transform(-1, 0, 0));
				rangedHusk.spawnedFor = player;
				rangedHusk.startAnimation(Animation.osrs(8567));
				rangedHusk.attack(player);
				
				rangedHusk.oppositeHusk = magicHusk;
				magicHusk.oppositeHusk = rangedHusk;

				player.applyFreeze(100000);
				player.sendMessage("<col=ff289d>The Nightmare puts you in a strange trance, preventing you from moving!");

				spawns.add(rangedHusk);
				spawns.add(magicHusk);
			}
		}, 4);
	}

	private void graspingClawAttack() {
		setAttackTimer(10);
		startAnimation(Animation.osrs(8598));
		int x = ARENA_END.getX() - ARENA_START.getX();
		int y = ARENA_END.getY() - ARENA_START.getY();
		IntSet coords = new IntOpenHashSet();
		
		for (int i = 0; i < 40; i++) {
			int randomX = Misc.random(x);
			int randomY = Misc.random(y);
			int bitPack = randomX << 16 | randomY;
			if (coords.contains(bitPack)) {
				continue;
			}

			final Location shadowLocation = ARENA_START.transform(randomX, randomY);
			if (!RegionClip.isFloorFree(RegionClip.getClipping(shadowLocation.getX(), shadowLocation.getY(), shadowLocation.getZ()))) {
				continue;
			}

			final AtomicBoolean spotTaken = new AtomicBoolean(false);
			forEachPlayer(player -> {
				if (player.getFreezeTimer() > 0) {
					spotTaken.set(true);
				}
			});

			if (spotTaken.get()) {
				continue;
			}

			coords.add(bitPack);
			Graphic.osrs(1767).send(shadowLocation);
			pulse(() -> forEachPlayer(player -> {
				if (player.getCurrentLocation().equals(shadowLocation)) {
					player.dealDamage(new Hit(Misc.random(50)));
				}
			}), 4);
		}

		pulse(() -> forEachPlayer(player -> {
			int distance = getCenterLocation().getDistanceInt(player.getCurrentLocation());
			if (distance < 3) {
				player.dealDamage(new Hit(Misc.random(50)));
			}
		}), 4);
	}

	private void mageRangeAttack(CombatType combatStyle) {
		final int startDelay;
		final int projectileId;
		final int ticks;
		final int speed;
		final int animationId;
		if (combatStyle == CombatType.RANGED) {
			projectileId = Graphic.getOsrsId(1766);
			startDelay = 100;
			ticks = 4;
			speed = 130;
			animationId = 8595;
		} else {
			startDelay = 70;
			projectileId = Graphic.getOsrsId(1764);
			ticks = 3;
			speed = 100;
			animationId = 8596;
		}

		startAnimation(Animation.osrs(animationId));
		forEachPlayer(player -> {
			Projectile proj = Projectile.create(this, player, projectileId);
			proj.setStartHeight(100);
			proj.setEndHeight(20);
			proj.setStartDelay(startDelay);
			proj.setEndDelay(speed);
			proj.setSlope(1);
			proj.setStartOffset(128);
			proj.send();
			player.pulse(() -> hitPlayer(player, combatStyle), ticks);
		});
	}

	private void meleeAttack(Mob victim) {
		startAnimation(Animation.osrs(8594));

		Location hitLocation = victim.getCurrentLocation().copyNew();
		forEachPlayer(player -> {
			Location location = player.getCurrentLocation();
			if (!location.isWithinDeltaDistance(hitLocation, 1)) {
				return;
			}

			player.pulse(() -> hitPlayer(player, CombatType.MELEE), 2);
		});
	}

	private void hitPlayer(Player player, CombatType style) {
		int hit = Misc.random(40);
		//TODO accuracy and maxhit
		//if (style.getSwingHandler().isAccurateImpact(this, player)) {
		//	hit = RandomFunction.random(style.estimatedHit(this, player));
		//}

		if (player.summoned != null && player.summoned.isPokemon) {
			player.summoned.dealDamage(new Hit(player.summoned.getSkills().getLifepoints()));
			player.sendMessage("The Nightmare kills your follower.");
		}

		if (hit > 0) {
			if (style.getPrayerActive().test(player)) {
				hit *= 0.2;
			} else if (player.curseActive[Curses.DEFLECT_MELEE.ordinal()]  || player.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()] || player.curseActive[Curses.DEFLECT_MISSILES.ordinal()]
					|| player.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()] || player.curseActive[Curses.DEFLECT_MAGIC.ordinal()] || player.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]) {
				hit *= 1.2;
			}
		}

		player.dealDamage(new Hit(hit, 0, style.getId()));
		player.startAnimation(player.getCombat().getBlockEmote());
		if (style == CombatType.MAGIC) {
			player.startGraphic(Graphic.osrs(1765, GraphicType.HIGH));
		}
	}

	public boolean isShieldNpc() {
		switch (getNpcTypeSmart()) {
			case AWAKE_SHIELD_P1:
			case AWAKE_SHIELD_P2:
			case AWAKE_SHIELD_P3:
				return true;
			default:
				return false;
		}
	}
	
	public boolean isAwakeNpc() {
		switch (getNpcTypeSmart()) {
			case AWAKE_P1:
			case AWAKE_P2:
			case AWAKE_P3:
			case EXPLODING:
				return true;
			default:
				return false;
		}
	}
	
	public Phase getPhase() {
		switch (getNpcTypeSmart()) {
			case AWAKE_SHIELD_P1:
			case AWAKE_P1:
				return Phase.FIRST;
			case AWAKE_SHIELD_P2:
			case AWAKE_P2:
				return Phase.SECOND;
			case AWAKE_SHIELD_P3:
			case AWAKE_P3:
				return Phase.THIRD;
			default:
				return null;
		}
	}

	@Override
	public void transform(int Id) {
		super.transform(Id);

		if (isShieldNpc()) {
			configureShieldHp();
			enableAggression();
			
			Phase phase = getPhase();
			if (phase != null) {
				NightmareManager.lobbyNPC.transform(phase.getLobbyId());
			}
		} else {
			Phase phase = getPhase();
			if (phase == null) {
				return;
			}

			int hp = phase.getHp();
			getSkills().setLifepoints(hp);
			getSkills().setMaximumLifepoints(2400);
			enableAggression();
			NightmareManager.lobbyNPC.transform(phase.getLobbyId());
		}
	}

	public void configureShieldHp() {
		int hp = Misc.interpolate(2000, 20000, 1, 80, playersOnStart);
		getSkills().setLifepoints(hp);
		getSkills().setMaximumLifepoints(hp);
	}

	public void clearSpawns() {
		int length = spawns.size();
		if (length <= 0) {
			return;
		}

		for (int i = 0; i < length; i++) {
			NPC npc = spawns.get(i);
			npc.nullNPC();
		}

		spawns.clear();
	}

	public enum Phase {
		FIRST(2400, AWAKE_P1, AWAKE_SHIELD_P2, NightmareLobbyNPC.FIRST_PHASE),
		SECOND(1600, AWAKE_P2, AWAKE_SHIELD_P3, NightmareLobbyNPC.SECOND_PHASE),
		THIRD(800, AWAKE_P3, -1, NightmareLobbyNPC.THIRD_PHASE);
		
		private final int hp, nextAwakeId, nextAwakeShieldId, lobbyId;
		
		Phase(int hp, int nextAwakeId, int nextAwakeShieldId, int lobbyId) {
			this.hp = hp;
			this.nextAwakeId = nextAwakeId;
			this.nextAwakeShieldId = nextAwakeShieldId;
			this.lobbyId = lobbyId;
		}
		
		public int getHp() {
			return hp;
		}
		
		public int getNextAwakeId() {
			return nextAwakeId;
		}
		
		public int getNextAwakeShieldId() {
			return nextAwakeShieldId;
		}
		
		public int getLobbyId() {
			return lobbyId;
		}
		
	}

	public void jump(Location location) {
		startAnimation(Animation.osrs(8607));
		pulse(() -> {
			moveNpc(location);
			startAnimation(Animation.osrs(8609));
		}, 3);
	}

	public void jumpCenter() {
		jump(Location.create(10270, 9949, 3));
	}

	public enum FlowerAttackData {
		FIRST(new Location[] {
				Location.create(10272, 9941, 3), Location.create(10272, 9942, 3), Location.create(10272, 9943, 3), Location.create(10272, 9944, 3), Location.create(10272, 9945, 3), Location.create(10272, 9946, 3), Location.create(10272, 9947, 3), Location.create(10272, 9948, 3), Location.create(10272, 9949, 3), Location.create(10272, 9950, 3), Location.create(10272, 9951, 3), Location.create(10271, 9951, 3), Location.create(10270, 9951, 3), Location.create(10269, 9951, 3), Location.create(10268, 9951, 3), Location.create(10267, 9951, 3), Location.create(10266, 9951, 3), Location.create(10265, 9951, 3), Location.create(10264, 9951, 3), Location.create(10263, 9951, 3),
		}, new Location[]{
				Location.create(10272, 9961, 3), Location.create(10272, 9960, 3), Location.create(10272, 9959, 3), Location.create(10272, 9958, 3), Location.create(10272, 9957, 3), Location.create(10272, 9956, 3), Location.create(10272, 9955, 3), Location.create(10272, 9954, 3), Location.create(10272, 9953, 3), Location.create(10272, 9952, 3), Location.create(10281, 9951, 3), Location.create(10280, 9951, 3), Location.create(10279, 9951, 3), Location.create(10278, 9951, 3), Location.create(10277, 9951, 3), Location.create(10276, 9951, 3), Location.create(10275, 9951, 3), Location.create(10274, 9951, 3), Location.create(10273, 9951, 3),
		}, new Rectangle(Location.create(10262, 9940), Location.create(10272, 9951))),
		SECOND(new Location[] {
				Location.create(10272, 9951, 3), Location.create(10271, 9951, 3), Location.create(10270, 9951, 3), Location.create(10269, 9951, 3), Location.create(10268, 9951, 3), Location.create(10267, 9951, 3), Location.create(10266, 9951, 3), Location.create(10265, 9951, 3), Location.create(10264, 9951, 3), Location.create(10263, 9951, 3), Location.create(10272, 9961, 3), Location.create(10272, 9960, 3), Location.create(10272, 9959, 3), Location.create(10272, 9958, 3), Location.create(10272, 9957, 3), Location.create(10272, 9956, 3), Location.create(10272, 9955, 3), Location.create(10272, 9954, 3), Location.create(10272, 9953, 3), Location.create(10272, 9952, 3)
		}, new Location[] {
				Location.create(10272, 9941, 3), Location.create(10272, 9942, 3), Location.create(10272, 9943, 3), Location.create(10272, 9944, 3), Location.create(10272, 9945, 3), Location.create(10272, 9946, 3), Location.create(10272, 9947, 3), Location.create(10272, 9948, 3), Location.create(10272, 9949, 3), Location.create(10272, 9950, 3), Location.create(10281, 9951, 3), Location.create(10280, 9951, 3), Location.create(10279, 9951, 3), Location.create(10278, 9951, 3), Location.create(10277, 9951, 3), Location.create(10276, 9951, 3), Location.create(10275, 9951, 3), Location.create(10274, 9951, 3), Location.create(10273, 9951, 3),
		}, new Rectangle(Location.create(10263, 9951), Location.create(10272, 9961))),
		THIRD(new Location[] {
				Location.create(10272, 9961, 3), Location.create(10272, 9960, 3), Location.create(10272, 9959, 3), Location.create(10272, 9958, 3), Location.create(10272, 9957, 3), Location.create(10272, 9956, 3), Location.create(10272, 9955, 3), Location.create(10272, 9954, 3), Location.create(10272, 9953, 3), Location.create(10272, 9952, 3), Location.create(10272, 9951, 3), Location.create(10281, 9951, 3), Location.create(10280, 9951, 3), Location.create(10279, 9951, 3), Location.create(10278, 9951, 3), Location.create(10277, 9951, 3), Location.create(10276, 9951, 3), Location.create(10275, 9951, 3), Location.create(10274, 9951, 3), Location.create(10273, 9951, 3)
		}, new Location[] {
				Location.create(10272, 9941, 3), Location.create(10272, 9942, 3), Location.create(10272, 9943, 3), Location.create(10272, 9944, 3), Location.create(10272, 9945, 3), Location.create(10272, 9946, 3), Location.create(10272, 9947, 3), Location.create(10272, 9948, 3), Location.create(10272, 9949, 3), Location.create(10272, 9950, 3), Location.create(10271, 9951, 3), Location.create(10270, 9951, 3), Location.create(10269, 9951, 3), Location.create(10268, 9951, 3), Location.create(10267, 9951, 3), Location.create(10266, 9951, 3), Location.create(10265, 9951, 3), Location.create(10264, 9951, 3), Location.create(10263, 9951, 3),
		}, new Rectangle(Location.create(10272, 9951), Location.create(10282, 9961))),
		FOURTH(new Location[] {
				Location.create(10272, 9951, 3), Location.create(10281, 9951, 3), Location.create(10280, 9951, 3), Location.create(10279, 9951, 3), Location.create(10278, 9951, 3), Location.create(10277, 9951, 3), Location.create(10276, 9951, 3), Location.create(10275, 9951, 3), Location.create(10274, 9951, 3), Location.create(10273, 9951, 3), Location.create(10272, 9941, 3), Location.create(10272, 9942, 3), Location.create(10272, 9943, 3), Location.create(10272, 9944, 3), Location.create(10272, 9945, 3), Location.create(10272, 9946, 3), Location.create(10272, 9947, 3), Location.create(10272, 9948, 3), Location.create(10272, 9949, 3), Location.create(10272, 9950, 3)
		}, new Location[] {
				Location.create(10272, 9961, 3), Location.create(10272, 9960, 3), Location.create(10272, 9959, 3), Location.create(10272, 9958, 3), Location.create(10272, 9957, 3), Location.create(10272, 9956, 3), Location.create(10272, 9955, 3), Location.create(10272, 9954, 3), Location.create(10272, 9953, 3), Location.create(10272, 9952, 3), Location.create(10271, 9951, 3), Location.create(10270, 9951, 3), Location.create(10269, 9951, 3), Location.create(10268, 9951, 3), Location.create(10267, 9951, 3), Location.create(10266, 9951, 3), Location.create(10265, 9951, 3), Location.create(10264, 9951, 3), Location.create(10263, 9951, 3),
		}, new Rectangle(Location.create(10272, 9941), Location.create(10282, 9951)));
		
		private final Location[] blossoms, berries;
		private final Rectangle safeArea;
		public static final FlowerAttackData[] values = values();
		
		FlowerAttackData(Location[] blossoms, Location[] berries, Rectangle safeArea) {
			this.blossoms = blossoms;
			this.berries = berries;
			this.safeArea = safeArea;
		}
		
		public Location[] getBlossoms() {
			return blossoms;
		}
		
		public Location[] getBerries() {
			return berries;
		}
		
		public Rectangle getSafeArea() {
			return safeArea;
		}
		
	}

	@Override
	public void nullNPC() {
		/* empty */
	}

	@Override
	public void die() {
		/* empty */
	}

	@Override
	public void removeNpcSafe(boolean instantClipRemove) {
		/* empty */
	}

}
