package com.soulplay.content.npcs.impl.bosses.zulrah;

import com.soulplay.Config;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.ProtectionPrayer;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class ZulrahSpells {

	public static final int RANGE_PROJECTILE_GFX = 1044 + Config.OSRS_GFX_OFFSET; // 970 is also similar, just brighter
	public static final int SPAWN_CLOUDS_PROJECTILE_GFX = 1045 + Config.OSRS_GFX_OFFSET;
	public static final int MAGIC_PROJECTILE_GFX = 1046 + Config.OSRS_GFX_OFFSET; // also 1155, just brighter
	public static final int SPAWN_MINIONS_PROJECTILES_GFX = 1047 + Config.OSRS_GFX_OFFSET;
	
	private static final int MAX_HIT = 41;
	
	private static final Animation SHOOTING_SPELL_ANIM = new Animation(Animation.getOsrsAnimId(5069)); //17216;
	public static final Animation HIDE_ANIM = new Animation(Animation.getOsrsAnimId(5072)); //17217;
	public static final Animation RISE_ANIM = new Animation(Animation.getOsrsAnimId(5073)); //17218;
	private static final Animation TAIL_WHIP_LEFT_ANIM = new Animation(Animation.getOsrsAnimId(5806)); //17221;
	private static final Animation TAIL_WHIP_RIGHT_ANIM = new Animation(Animation.getOsrsAnimId(5807)); //17222;
	
	private static final Animation SNAKELING_SPAWN_ANIM = new Animation(Animation.getOsrsAnimId(2413)); //17232;
	
	private static final int PROJECTILE_TIME = 40;
	private static final int PROJECTILE_START_HEIGHT = 80;
	private static final int PROJECTILE_END_HEIGHT = 22;
	private static final int PROJECTILE_SPEED = 90;
	
	private static final int VENOM_CLOUD_OBJECT = 11700;
	
	public static final int SNAKELING_NPC_ID = 2045;
	
	public static void shootSpell(NPC n, Client c, boolean magic) {
		int projectileId = RANGE_PROJECTILE_GFX;
		if (magic)
			projectileId = MAGIC_PROJECTILE_GFX;
		
		n.face(c);
		Projectile projectile = new Projectile(projectileId,
				Location.create(n.getX()+1, n.getY()+1, n.getHeightLevel()),
				Location.create(c.getX(), c.getY(), c.getZ()));
		projectile.setLockon(c.getProjectileLockon());
		projectile.setStartDelay(PROJECTILE_TIME);
		projectile.setStartHeight(PROJECTILE_START_HEIGHT);
		projectile.setEndHeight(PROJECTILE_END_HEIGHT);
		projectile.setEndDelay(PROJECTILE_SPEED);
		GlobalPackets.createProjectile(projectile);
		
		n.startAnimation(SHOOTING_SPELL_ANIM);
		
		int damage = Misc.random(MAX_HIT);
		
		if (n.isVenomousNpc() && Misc.random(99) < 25) {
			c.getDotManager().applyVenom();
		}
		
		if (magic) {
			if (CombatPrayer.usingProtectPrayer(c, ProtectionPrayer.PROTECT_MAGIC.ordinal()))
				damage = 0;
	
			NpcHitPlayer.dealMagicDamage(n, c, damage, 3, null); // protection prayers are 60% here
		} else {
			if (CombatPrayer.usingProtectPrayer(c, ProtectionPrayer.PROTECT_RANGE.ordinal()))
				damage = 0;
	
			NpcHitPlayer.dealRangeDamage(n, c, damage, 3, null); // protection prayers are 60% here
		}
	}
	
	
	// if two clouds are sent at once, then zulrah will face the 2nd cloud
	public static void spawnToxicClouds(final Client c, final NPC n, final Location cloudLoc) {
		if (!RSConstants.zulrahArea(c.getX(), c.getY()))
			return;
		Projectile projectile = new Projectile(SPAWN_CLOUDS_PROJECTILE_GFX,
				Location.create(n.getX()+1, n.getY()+1, n.getHeightLevel()),
				Location.create(cloudLoc.getX(), cloudLoc.getY(), c.getZ()));
		projectile.setStartDelay(PROJECTILE_TIME);
		projectile.setStartHeight(PROJECTILE_START_HEIGHT);
		projectile.setEndHeight(PROJECTILE_END_HEIGHT);
		projectile.setEndDelay(PROJECTILE_SPEED);
		GlobalPackets.createProjectile(projectile);
		
		n.startAnimation(SHOOTING_SPELL_ANIM);
		n.turnNpc(cloudLoc.getX(), cloudLoc.getY());
		
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!RSConstants.zulrahArea(c.getX(), c.getY())) {
					container.stop();
					return;
				}
				if (c == null || !c.isActive || n == null || n.isDead()) {
					container.stop();
					return;
				}
				int aliveTick = 30; // about 18 seconds maybe? that was west jad duration of clouds
				VenomCloud cloud = new VenomCloud(cloudLoc, aliveTick);
				c.getPacketSender().addObjectPacket(VENOM_CLOUD_OBJECT, cloudLoc.getX(), cloudLoc.getY(), 10, 0);
				if (c.zulrah != null) {
					c.zulrah.addVenomCloud(cloud);
				}
				container.stop();
				
			}
		}, 3);
	}
	
	public static void spawnMinion(final Client c, final NPC n, final Location loc, final int ticks) {
		if (!RSConstants.zulrahArea(c.getX(), c.getY()))
			return;
		Projectile projectile = new Projectile(SPAWN_MINIONS_PROJECTILES_GFX,
				Location.create(n.getX()+2, n.getY()+2, n.getHeightLevel()),
				Location.create(loc.getX(), loc.getY(), c.getZ()));
		projectile.setStartDelay(PROJECTILE_TIME);
		projectile.setStartHeight(PROJECTILE_START_HEIGHT);
		projectile.setEndHeight(PROJECTILE_END_HEIGHT);
		projectile.setEndDelay(PROJECTILE_SPEED);
		GlobalPackets.createProjectile(projectile);
		
		n.startAnimation(SHOOTING_SPELL_ANIM);
		n.turnNpc(loc.getX(), loc.getY());
		
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!RSConstants.zulrahArea(c.getX(), c.getY())) {
					container.stop();
					return;
				}
				if (c == null || !c.isActive || n == null || n.isDead()) {
					container.stop();
					return;
				}
				
				NPC minion = NPCHandler.spawnNpc(c, SNAKELING_NPC_ID, loc.getX(), loc.getY(), c.getZ(), 1, true, false);
				minion.setAliveTick(51); // approximately 30 seconds
				minion.startAnimation(SNAKELING_SPAWN_ANIM);
				minion.setKillerId(c.getId());
				minion.setVenomousNpc(true);
				
				container.stop();
				
			}
		}, ticks);
	}
	
	public static void tailWhip(final Client c, final NPC n, boolean leftWhip) {
		Location hitLoc = Location.create(c.getX(), c.getY());
		
		n.startAnimation(leftWhip ? TAIL_WHIP_LEFT_ANIM : TAIL_WHIP_RIGHT_ANIM);
		
		if (hitLoc.getY() == 3072)
			hitLoc = Location.create(hitLoc.getX(), 3076);
		n.turnNpc(hitLoc.getX(), hitLoc.getY());
		
		final Location hitLocFinal = hitLoc.copyNew();
		
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (c == null || !c.isActive || n == null || n.isDead()) {
					container.stop();
					return;
				}
				if (c.getY() == 3072) { // safespot cheaphax. Pointless to use clipping checks on this boss
					container.stop();
					return;
				}
				if (hitLocFinal.isWithinDistance(Location.create(c.getX(), c.getY()), 1)) {
					int damage = Misc.random(30);
					NpcHitPlayer.dealMeleeDamage(n, c, damage, 0, null);
					c.shove(3, n.getCurrentLocation()); //c.getPA().shovePlayer(3, n.getX(), n.getY()); // 3 ticks seem to be the best?
				}
				container.stop();
			}
		}, 6);
	}
	
	
}
