package com.soulplay.content.npcs.impl.bosses.barrelchest;

import com.soulplay.Server;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

/**
 * @author Julius
 */
@NpcMeta(ids = { 5666 })
public final class BarrelchestNPC extends CombatNPC {

	public BarrelchestNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		walkUpToHit = false;
		getDistanceRules().setFollowDistance(10);
		getDistanceRules().setMeleeDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize()) {
			meleeSwing(p, 61, Animation.getOsrsAnimId(5894));
			p.getCombat().resetPrayers();
			p.getPlayerLevel()[5] -= (p.getPlayerLevel()[5] * .02);
			p.refreshSkill(5);
		} else {
			earthQuake();
		}
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(5666, 255, 61, 260, 140);
		NPCAnimations.define(5666, 5898, 5897, 5894);
	}

	private void earthQuake() {
		startAnimation(Animation.getOsrsAnimId(5895));
		for (Player player : Server.getRegionManager().getLocalPlayers(this)) {
			if (player == null) {
				continue;
			}
			if (player.goodDistance(this, 15)) {
				player.dealDamage(new Hit(Misc.newRandom(1, 25), 0, 0));
			}
		}
	}

}
