package com.soulplay.content.npcs.impl.bosses.zulrah;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import com.soulplay.Server;
import com.soulplay.content.items.itemdata.MeleeSwingStyle;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.NpcWeakness;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.instances.PlayerSingleInstance;
import com.soulplay.game.world.map.travel.zone.addon.ZulrahZone;
import com.soulplay.util.Misc;

public class Zulrah {
	
	public NPC zulrah;
	
	private ArrayList<VenomCloud> venomClouds = new ArrayList<VenomCloud>();
	private ArrayList<VenomCloud> venomCloudsRemove = new ArrayList<VenomCloud>();
	
	private int mainStage = 0;
			
	private int attacksLeft = 0;
	
	private boolean castingSpells = false;
	
	private int timer = 0;
	
	private static final int INIT_TIMER = 14;
	
	private static final int ATTACK_SPEED = 3;
	private static final int TAILWHIP_ATTACK_SPEED = 8;
	
	private static final int END_MINI_STAGE_TIME = 6; // 6 seems to work good timing for when ending mini stage and starting action on next mini stage.
	
	private static final int MINION_SPAWN_TIMER = 3;
	
	private static final int GREEN_ZULRAH = 2012;
	private static final int RED_ZULRAH = 2043;
	private static final int BLUE_ZULRAH = 2044;
	
	private static final Animation ZULRAH_SPAWN_ANIM = new Animation(Animation.getOsrsAnimId(5071));
	
	private boolean usingMagic = false;// used for jad stage
	
	private Stage currentStage = null;
	private Queue<Stage> queuedStages = new LinkedList<Stage>();
	
	public boolean isUsingMagic() {
		return usingMagic;
	}

	public void setUsingMagic(boolean usingMagic) {
		this.usingMagic = usingMagic;
	}

	public Zulrah(NPC n, Client c) {
		zulrah = n;
		initZulrah(c);
		timer = INIT_TIMER;
		n.startAnimation(ZULRAH_SPAWN_ANIM);
		setRandomStage();
		
		n.setMeleeSwingStyle(MeleeSwingStyle.CRUSH);
		
//		attacksLeft = 100;
	}
	
	private void setRandomStage() {
		mainStage = Misc.random(3); // random stage from 4 stages to be picked
		usingMagic = false;
		if (mainStage == 0) {
			buildRotation1();
		} else if (mainStage == 1) {
			buildRotation2();
		} else if (mainStage == 2) {
			buildRotation3();
			usingMagic = true; // jad stage starts with magic
		} else if (mainStage == 3) {
			buildRotation4();
			usingMagic = true; // jad stage starts with magic
		}
	}
	
	private void buildStageOneClouds() {
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.SOUTH_EAST));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_MIDDLE, LocationsData.SOUTH_WEST));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_NORTH, LocationsData.EAST_SOUTH));
		queuedStages.add(Stage.createClouds(LocationsData.WEST_SOUTH, LocationsData.WEST_NORTH));
	}
	
	private void buildRotation1() {
		//1
		buildStageOneClouds();
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));
		
		//2
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, BLUE_ZULRAH));
		
		//3
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 4));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, GREEN_ZULRAH));
		
		//4
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_WEST, LocationsData.SOUTH_MIDDLE));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.EAST_SOUTH));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_MID, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER*3));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));
		
		//5
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, BLUE_ZULRAH));
		
		//6
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, GREEN_ZULRAH));
		
		//7
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_EAST, LocationsData.EAST_SOUTH_SOUTH));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_MIDDLE, LocationsData.SOUTH_WEST));
		queuedStages.add(Stage.createClouds(LocationsData.WEST_SOUTH, LocationsData.WEST_NORTH));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_MID, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH, MINION_SPAWN_TIMER*3));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, BLUE_ZULRAH));
		
		buildRotation1And2Endings();
		
	}
	
	private void buildRotation2() {
		//1
		buildStageOneClouds();
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));
		
		//2
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, BLUE_ZULRAH));
		
		//3
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 4));
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, GREEN_ZULRAH));
		
		//4
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH, LocationsData.EAST_SOUTH_SOUTH));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_NORTH, LocationsData.EAST_NORTH_NORTH));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_EAST, LocationsData.SOUTH_MIDDLE));
		queuedStages.add(Stage.createMinions(LocationsData.S_SOUTH_WEST, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER*3));
		if (Misc.random(10) < 2)
			queuedStages.add(Stage.hideZulrah(LocationsData.EAST, 0));
		else
			queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, BLUE_ZULRAH));
		
		//5 - add a chance of throwing optional stage build instead
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_WEST, LocationsData.SOUTH_MIDDLE));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.EAST_SOUTH));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_MID, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER*3));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));
		
		//6
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.hideZulrah(LocationsData.EAST, GREEN_ZULRAH));
		
		//7
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, BLUE_ZULRAH));
		
		buildRotation1And2Endings();
		
	}
	
	private void buildRotation1And2Endings() {
		//8
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_MIDDLE, LocationsData.SOUTH_EAST));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.EAST_SOUTH));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER*3));
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, GREEN_ZULRAH));
		
		//9 - jad stage
		queuedStages.add(Stage.createSpell(Stage.JAD_SPELL, 10));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_WEST, LocationsData.SOUTH_MIDDLE));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_EAST, LocationsData.EAST_SOUTH_SOUTH));
		queuedStages.add(Stage.createClouds(LocationsData.WEST_SOUTH, LocationsData.WEST_NORTH));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH, LocationsData.EAST_NORTH));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));
		
		//10 red
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, GREEN_ZULRAH)); //ending
	}
	
	private void buildRotation3() {
		//1
		buildStageOneClouds();
		queuedStages.add(Stage.hideZulrah(LocationsData.EAST, GREEN_ZULRAH));
		
		//2
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));
		
		//3
		queuedStages.add(Stage.createClouds(LocationsData.EAST_NORTH_NORTH, LocationsData.EAST_NORTH));
		queuedStages.add(Stage.createMinions(Location.create(2273, 3072), MINION_SPAWN_TIMER)); // some diff location?
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.SOUTH_EAST));
		queuedStages.add(Stage.createMinions(LocationsData.S_SOUTH_WEST, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_WEST, LocationsData.WEST_SOUTH));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, BLUE_ZULRAH));
		
		//4
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, GREEN_ZULRAH));
		
		//5
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.EAST, BLUE_ZULRAH));
		
		//6
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, GREEN_ZULRAH));
		
		//7
		queuedStages.add(Stage.createClouds(LocationsData.EAST_NORTH, LocationsData.EAST_NORTH_NORTH));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.EAST_SOUTH));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_EAST, LocationsData.SOUTH_MIDDLE));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER*3)); // needs to be delayed here
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, 0)); // already green so not transforming
		
		//8
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, BLUE_ZULRAH));
		
		buildR3and4SimilarLastStages();
		
	}
	
	private void buildRotation4() {
		//1
		buildStageOneClouds();
		queuedStages.add(Stage.hideZulrah(LocationsData.EAST, BLUE_ZULRAH));
		
		//2
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_MID, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 6));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, GREEN_ZULRAH));

		//3
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 4));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_WEST, LocationsData.SOUTH_EAST));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_MIDDLE, LocationsData.EAST_SOUTH_SOUTH)); // SUPER DELAYED LAST CLOUD
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, BLUE_ZULRAH));

		//4
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_SOUTH_WEST, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_SOUTH_EAST, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 4));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, RED_ZULRAH));

		//5
		queuedStages.add(Stage.createSpell(Stage.MELEE_SPELL, 2));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_WEST, LocationsData.SOUTH_EAST));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_MIDDLE, LocationsData.EAST_SOUTH_SOUTH));
		queuedStages.add(Stage.hideZulrah(LocationsData.EAST, GREEN_ZULRAH));

		//6
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 4));
		queuedStages.add(Stage.hideZulrah(LocationsData.SOUTH, 0));

		//7
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_MID, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_NORTH, LocationsData.EAST_NORTH_NORTH));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH_SOUTH, LocationsData.EAST_SOUTH));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_EAST, LocationsData.SOUTH_MIDDLE)); // LAST CLOUD SPAWNS SUPER LATE?
		queuedStages.add(Stage.hideZulrah(LocationsData.WEST, BLUE_ZULRAH));

		//8
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_SOUTH_EAST, MINION_SPAWN_TIMER*3));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, GREEN_ZULRAH));
		
		//9
		queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, BLUE_ZULRAH));
		
		buildR3and4SimilarLastStages();
		
	}
	
	private void buildR3and4SimilarLastStages() {
		//9
		queuedStages.add(Stage.createSpell(Stage.BLUE_SPELL, 5));
		queuedStages.add(Stage.createClouds(LocationsData.EAST_SOUTH, LocationsData.EAST_NORTH));
		queuedStages.add(Stage.createClouds(LocationsData.SOUTH_EAST, LocationsData.EAST_SOUTH_SOUTH));
		if (mainStage == 2) { // 3rd rotation creates minions only
			queuedStages.add(Stage.createMinions(LocationsData.S_SOUTH_EAST, MINION_SPAWN_TIMER));
			queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH_SOUTH, MINION_SPAWN_TIMER));
			queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER*3));
		}
		queuedStages.add(Stage.hideZulrah(LocationsData.EAST, GREEN_ZULRAH));
		
		//10 jad stage - magic first
		queuedStages.add(Stage.createSpell(Stage.JAD_SPELL, 8));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, BLUE_ZULRAH));
		
		//11
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_NORTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_WEST_SOUTH, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_MID, MINION_SPAWN_TIMER));
		queuedStages.add(Stage.createMinions(LocationsData.S_EAST_NORTH, MINION_SPAWN_TIMER*3));
		queuedStages.add(Stage.hideZulrah(LocationsData.MID, GREEN_ZULRAH)); // go back to normal
	}
	
	public void initZulrah(Client c) {
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void stop() {

				nullZulrah();
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!RSConstants.zulrahArea(c.getX(), c.getY()) || c.isDead() || !c.isActive) {
					if (zulrah != null && !zulrah.isDead())
						zulrah.setDropsItemOnDeath(false);
					container.stop();
					return;
				}
				if (zulrah.isDead()) {
					clearMinions(c);
					clearVenomClouds();
					container.stop();
					return;
				} else {
					checkVenomClouds(c);
					processVenomCloudTicks(c);
				}
				
				if (zulrah != null && !zulrah.isDead())
					pollStages(c);
				
				if (timer > 0)
					timer--;
			}
		}, 1);
	}
	
	private void pollStages(Client c) {
		if (timer == 0) {
			if (!castingSpells) {
				currentStage = queuedStages.poll();
				if (currentStage == null) { //remove null check after everything is good
					System.out.println("STAGE IS NULLLLLL");
					return;
				}
				if (currentStage.getAttacksLeft() > 0) {
					castingSpells = true;
					attacksLeft = currentStage.getAttacksLeft();
				}
			}
			if (attacksLeft > 0) { // attacking stage, wait till all attacks are sent then go to next stage
				
				timer = ATTACK_SPEED;//default attack speed
				if (currentStage.getSpellType() == Stage.MELEE_SPELL) {
					timer = TAILWHIP_ATTACK_SPEED;
					ZulrahSpells.tailWhip(c, zulrah, Misc.random(1) == 0);
				} else if (currentStage.getSpellType() == Stage.RANGED_SPELL)
					ZulrahSpells.shootSpell(zulrah, c, false);
				else if (currentStage.getSpellType() == Stage.BLUE_SPELL)
					blueSpellAttack(c);
				else { //jad leftover
					ZulrahSpells.shootSpell(zulrah, c, usingMagic);
					usingMagic = !usingMagic;
				}
				attacksLeft--;
				if (attacksLeft == 0) {
					castingSpells = false;
					zulrah.resetFace();
				}
			} else { // other stages
				timer = ATTACK_SPEED;//default attack speed
				if (currentStage.getActionType() == Stage.ACTION_CLOUDS) {
					ZulrahSpells.spawnToxicClouds(c, zulrah, currentStage.getLoc1());
					ZulrahSpells.spawnToxicClouds(c, zulrah, currentStage.getLoc2()); // this might need to be delayed by 1 tick
				} else if (currentStage.getActionType() == Stage.ACTION_SNAKELINGS) {
					ZulrahSpells.spawnMinion(c, zulrah, currentStage.getMinionLoc(), currentStage.getSpawnTicks()); // does not contain delayed minion spawns
				} else if (currentStage.getActionType() == Stage.ACTION_HIDE) {
					hideZulrah(c);
					teleport(c, currentStage.getTeleLoc(), currentStage.getTransform());
					if (queuedStages.isEmpty()) { // last stage of the rotation
						queuedStages.add(Stage.createSpell(Stage.RANGED_SPELL, 5));
						setRandomStage();
					}
				}
			}
		}
	}
	
	private void processVenomCloudTicks(Client c) {
		for (VenomCloud cl : venomClouds) {
			if (cl.getTicks() > 0) {
				cl.setTicks(cl.getTicks()-1);
			} else {
				c.getPacketSender().addObjectPacket(-1, cl.getLoc().getX(), cl.getLoc().getY(), 10, 0);
				venomCloudsRemove.add(cl);
			}
		}
		if (!venomCloudsRemove.isEmpty()) {
			venomClouds.removeAll(venomCloudsRemove);
			venomCloudsRemove.clear();
		}
	}
	
	private void teleport(final Client c, final Location newLocation, int transform) {
		if (!zulrah.isVenomousNpc() && transform == BLUE_ZULRAH)
			zulrah.setVenomousNpc(true);
		zulrah.resetFace();
		ZulrahFace face = ZulrahFace.SOUTH;
		if (Location.equals(newLocation, LocationsData.SOUTH)) {
			face = ZulrahFace.NORTH;
		} else if (Location.equals(newLocation, LocationsData.WEST)) {
			face = ZulrahFace.EAST;
		} else if (Location.equals(newLocation, LocationsData.EAST)) {
			face = ZulrahFace.WEST;
		}
		final ZulrahFace faceFinal = face;
		
		zulrah.getWeakness().clear();
		
		switch (transform) {
		case GREEN_ZULRAH:
			zulrah.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] = -45;
			zulrah.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] = 50;
			zulrah.getWeakness().add(NpcWeakness.MAGIC);
			break;
		case RED_ZULRAH:
			zulrah.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] = 0;
			zulrah.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] = 300;
			zulrah.getWeakness().add(NpcWeakness.MAGIC);
			break;
		case BLUE_ZULRAH:
			zulrah.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] = 300;
			zulrah.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] = 0;
			zulrah.getWeakness().add(NpcWeakness.RANGED);
			break;
		}
		
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			int timer2 = 0;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (c == null || !c.isActive || zulrah == null || zulrah.isDead()) {
					container.stop();
					return;
				}
				if (timer == 0) {
					zulrah.face(null);
				}
				if (timer2 == 1) {
					zulrah.turnNpc(zulrah.getX()+faceFinal.getX(), zulrah.getY() + faceFinal.getY());
					if (!Location.equals(newLocation, Location.create(zulrah.getX(), zulrah.getY()))) {
						zulrah.teleport(Location.create(newLocation.getX(), newLocation.getY(), zulrah.getZ()), TeleportType.ZULRAH);
					}
				}
				if (timer2 == 2) {
//					zulrah.turnNpc(zulrah.getX()+faceFinal.getX(), zulrah.getY() + faceFinal.getY());
					
//					if (!Location.equals(newLocation, Location.create(zulrah.getX(), zulrah.getY()))) {
//						zulrah.didTeleport = true;
//						zulrah.moveNpcNow(newLocation.getX(), newLocation.getY(), zulrah.getZ());
//					}
					if (transform > 0) {
						zulrah.transform(transform);
					}
					riseZulrah(c);
					container.stop();
				}
				timer2++;
			}
		}, 1);
	}
	
	private void blueSpellAttack(Client c) {
		ZulrahSpells.shootSpell(zulrah, c, Misc.random(2) > 0);
	}
	
	private void hideZulrah(Client c) {
		zulrah.startAnimation(ZulrahSpells.HIDE_ANIM);
		zulrah.isDigging = true;
		if (c.npcIndex > 0)
			c.getCombat().resetPlayerAttack();
		timer = END_MINI_STAGE_TIME; // 6 is perfect for going into red stage for rotaion 1 and 2
//		miniStage++; // debug purposes
	}
	
	private void riseZulrah(Client c) {
		zulrah.startAnimation(ZulrahSpells.RISE_ANIM);
		zulrah.isDigging = false;
	}
	
	private void checkVenomClouds(Client c) {
//		c.sendMess("stage:"+miniStage+" Micro:"+microStage+" rotation:"+(mainStage+1));
//		System.out.println("stage:"+miniStage+" Micro:"+microStage+" rotation:"+(mainStage+1));
		if (!venomClouds.isEmpty()) {
			for (int i = 0; i < venomClouds.size(); i++) {
				VenomCloud cl = venomClouds.get(i);
				if (c.inArea(cl.getLoc().getX(), cl.getLoc().getY(), cl.getLoc().getX()+2, cl.getLoc().getY()+2)) {
					int damage = Misc.random2(4);

					c.dealDamage(new Hit(damage, 2, -1));
				}
			}
		}
	}
	
	private void clearMinions(Client c) {
		for (NPC n : Server.getRegionManager().getLocalNpcs(c)) {
			if (n != null && !n.isDead() && n.npcType == ZulrahSpells.SNAKELING_NPC_ID) {
				n.setDead(true);
			}
		}
	}
	
	public void addVenomCloud(VenomCloud cloud) {
		venomClouds.add(cloud);
	}
	
	private void clearVenomClouds() {
		if (venomClouds != null && !venomClouds.isEmpty()) {
			venomClouds.clear();
			venomClouds = null;
			venomCloudsRemove = null;
		}
	}
	
	public void nullZulrah() {
		clearVenomClouds();
		currentStage = null;
		if (queuedStages != null) {
			queuedStages.clear();
			queuedStages = null;
		}
		if (zulrah != null) {
			zulrah.nullNPC();
			zulrah = null;
		}
	}

	
	public static void sendToZulrah(Client c, boolean spawnBoss) {
		if (RSConstants.zulrahArea(c.getX(), c.getY()))
			return;
		c.getZones().addDynamicZone(new ZulrahZone());
		final int height = PlayerSingleInstance.getOrCreateZ(c.mySQLIndex);
		//c.getPA().startTeleport(2268, 3069, height, TeleportType.MODERN);
		c.getDamageMap().reset();
		c.performingAction = true;
		c.getPacketSender().setBlackout(2 + 0x80);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			int timer = 5;
			
			@Override
			public void stop() {
				c.performingAction = false;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				timer --;
				
				if (timer == 3) {
					c.getPA().movePlayer(LocationsData.PLAYER_START.getX(), LocationsData.PLAYER_START.getY(), height);
					c.performingAction = false;
					c.getPacketSender().setBlackout(0);
				}
				
				if (timer == 1) {
					container.stop();
					
					c.getPacketSender().setBlackout(0);
					
					if (spawnBoss) {
						NPC z = NPCHandler.spawnNpc2(2012, LocationsData.MID.getX(), LocationsData.MID.getY(), height, 0);
						z.getSkills().setStaticLevel(Skills.MAGIC, 300);
						z.getSkills().setStaticLevel(Skills.DEFENSE, 300);
						z.getSkills().setStaticLevel(Skills.RANGED, 300);
						z.getCombatStats().setMagicAccBonus(50);
						z.getCombatStats().setRangedAccBonus(50);
						z.getCombatStats().setMagicDefBonus(-61);
						z.getCombatStats().setRangedDefBonus(50);
						z.getCombatStats().setRangeStrengthBonus(20);
						z.getCombatStats().setPoisonImmunity(true);
						z.getCombatStats().setVenomImmunity(true);
						z.getCombatStats().setAttackSpeed(3);
						c.zulrah = new Zulrah(z, c);
					}
				}
			}
		}, 2);
	}
}
