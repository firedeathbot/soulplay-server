package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 109440, 109441, 109442 })
public class NightmareTotemNW extends NightmareTotem {

	public static final int REGULAR = 109440;
	public static final int CHARGE = 109441;
	public static final int FULL_CHARGE = 109442;

	public NightmareTotemNW(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareTotemNW(slot, npcType);
	}

	@Override
	public int getFullChargeNpcId() {
		return FULL_CHARGE;
	}

	@Override
	public int getChargeNpcId() {
		return CHARGE;
	}

	@Override
	public String getChargedMessage() {
		return "The north west totem is fully charged.";
	}

}
