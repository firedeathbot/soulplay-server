package com.soulplay.content.npcs.impl.bosses.kraken;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.entity.Animation;

public class KrakenTentacle extends NPC {

	public static final Animation WAKE_ANIM = new Animation(Animation.getOsrsAnimId(3860));
	public static final Animation WAKE_ANIM2 = new Animation(Animation.getOsrsAnimId(3729));
	public static final Animation DEATH_HIDE_ANIM = new Animation(Animation.getOsrsAnimId(3620));
	public static final Animation BLOCK_ANIM = new Animation(Animation.getOsrsAnimId(3619));
	
	public KrakenTentacle(int npcSlot, int npcId) {
		super(npcSlot, npcId);
		isAggressive = false;
		setRetaliates(false);
		setCanWalk(false);
		alwaysRespawn = false;
		inMultiZone = true;
	}
	
	@Override
	public int dealDamage(Hit hit) {
		if (!transformed()) {
			face(hit.getSource());
			transform(8931);
			isAggressive = true;
			startAnimation(WAKE_ANIM);
			setAttackTimer(2);
			setRetaliates(true);
		} else {
			if (isStartBlockAnim()) {
				startAnimation(BLOCK_ANIM);
			}
		}
		return super.dealDamage(hit);
	}

	@Override
	public void setDead(boolean isDead) {
		super.setDead(isDead);
		if (transformed()) {
			startAnimation(DEATH_HIDE_ANIM);
		}
	}
	
	public boolean transformed() {
		return transformId == 8931;
	}
	
	public static boolean isAwake(NPC npc) {
		return npc.transformId == 8931 || npc.underAttackBy > 0;
	}
}
