package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public abstract class NightmareHuskNpc extends CombatNPC {

	public NightmareHuskNpc oppositeHusk;
	public Player spawnedFor;

	public NightmareHuskNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		ignoreAggroTolerence();
		stopAttackDefault = true;
		attack(spawnedFor);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		if (spawnedFor != null && oppositeHusk != null && !consideredForHit(null) && !oppositeHusk.consideredForHit(null)) {
			spawnedFor.resetFreeze();
			spawnedFor.sendMessage("<col=229628>As the last husk dies, you feel yourself become free of the Nightmare's trance.");
		}
	}

}
