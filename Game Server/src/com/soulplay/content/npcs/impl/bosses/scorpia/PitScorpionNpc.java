package com.soulplay.content.npcs.impl.bosses.scorpia;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

@NpcMeta(ids = {109})
public class PitScorpionNpc extends AbstractNpc {

	public PitScorpionNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new PitScorpionNpc(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();
		getDistanceRules().setFollowDistance(-9);
		setAggroRadius(2);
		getCombatStats().setAttackSpeed(4);
		getDistanceRules().setRangeDistance(4);
		walkUpToHit = false;
		isAggressive = true;
		setRetaliates(true);
		inMultiZone = true;
		inWildPermanent = true;
		setWalkDistance(5);
	}
	
	@Override
	public void process() {
		super.process();
		attackType = 1;
	}
	
	@Override
	public void onDamageApplied(Player p, int damage) {
		super.onDamageApplied(p, damage);
		if (damage > 0) {
			p.getSkills().decrementPrayerPoints(Misc.random(damage));

			if (Misc.randomBoolean(3)) {
				p.getDotManager().applyPoison(6, this);
			}
		}
	}
	
}
