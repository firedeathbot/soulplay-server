package com.soulplay.content.npcs.impl.bosses.superiorslayermonsters;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;

@NpcMeta(ids = { 107396 })
public class MalevolentMageNPC extends SuperiorNPC {

	public MalevolentMageNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new MalevolentMageNPC(slot, npcType);
	}

	@Override
	public void onSpawn() {
		walkUpToHit = false;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
		stopAttackDefault = true;
	}

	@Override
	public void startAttack(Player p) {
		projectileSwing(p, 2731, 20, 4, Graphic.create(2740), CombatType.MAGIC);// TODO: missing start graphic and check
																				// graphic ids
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107396, 7185, 7198, 2791);
	}

}
