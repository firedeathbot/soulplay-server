package com.soulplay.content.npcs.impl.bosses.nightmare;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;

public class NightmareManager {

	public static final List<Integer> players = new ArrayList<>();
	public static final List<Integer> lobbyPlayers = new ArrayList<>();
	public static NightmareLobbyNPC lobbyNPC;
	public static NightmareNPC boss;
	public static NightmareTotemNE totemNE;
	public static NightmareTotemNW totemNW;
	public static NightmareTotemSE totemSE;
	public static NightmareTotemSW totemSW;
	public static CycleEventContainer wakeUpPulse;
	private static Object object = new Object();

	public static void spawnBoss() {
		if (boss == null || !boss.isActive) {
			boss = (NightmareNPC) NPCHandler.spawnNpc(NightmareNPC.SLEEPING, 10270, 9949, 3);
		}
	}

	public static void spawn() {
		//public static final int TRUE_OSRS_X_OFFSET = 4992, TRUE_OSRS_Y_OFFSET = 2048;
		lobbyNPC = (NightmareLobbyNPC) NPCHandler.spawnNpc(NightmareLobbyNPC.SLEEPING, 10206, 9757, 1);
		lobbyNPC.setCanWalk(false);

		spawnBoss();

		totemNE = (NightmareTotemNE) NPCHandler.spawnNpc(NightmareTotemNE.REGULAR, 10279, 9958, 3);
		totemNW = (NightmareTotemNW) NPCHandler.spawnNpc(NightmareTotemNW.REGULAR, 10263, 9958, 3);
		totemSE = (NightmareTotemSE) NPCHandler.spawnNpc(NightmareTotemSE.REGULAR, 10279, 9942, 3);
		totemSW = (NightmareTotemSW) NPCHandler.spawnNpc(NightmareTotemSW.REGULAR, 10263, 9942, 3);
	}

	public static void forEachLobbyPlayer(Consumer<Player> consumer) {
		for (int i = 0, size = lobbyPlayers.size(); i < size; i++) {
			int playerIndex = lobbyPlayers.get(i);
			Player player = PlayerHandler.getPlayerByMID(playerIndex);
			if (player == null || !player.isActive()) {
				continue;
			}

			consumer.accept(player);
		}
	}

	public static void enter(Player player) {
		if (ItemRetrievalManager.hasItems(player, true)) {
			return;
		}

		int lobbyNpcId = lobbyNPC.getNpcTypeSmart();
		if (lobbyNpcId == NightmareLobbyNPC.FIRST_PHASE || lobbyNpcId == NightmareLobbyNPC.SECOND_PHASE || lobbyNpcId == NightmareLobbyNPC.THIRD_PHASE) {
			player.getDialogueBuilder().sendStatement("A group is already fighting the Nightmare. You'll have to wait until", "they are done.").execute(false);
			return;
		}

		player.startAnimation(Animation.osrs(8584));
		//TODO should be unable to this this dialog, 2021
		player.getPacketSender().setBlackout(2 + 0x80);
		player.getDialogueBuilder().sendStatement("The Nightmare pulls you into her dream as you approach her.");
		player.lockActions(Integer.MAX_VALUE);
		player.pulse(() -> {
			player.getPA().movePlayer(10272, 9948, 3);
			player.pulse(() -> {
				player.getPacketSender().setBlackout(2 + 0x40);
				player.startAnimation(Animation.osrs(8583));
				player.pulse(() -> {
					player.lockActions().reset();
					player.getPacketSender().setBlackout(0);
					player.pulse(() -> {
						//TODO should be unable to this this dialog, 2021
						//player.getDialogueInterpreter().sendPlainMessage(false, "The Nightmare pulls you into her dream as you approach her.");
						checkWakePulse(50);
					});
				}, 2);
			});
		}, 5);
	}

	public static void checkWakePulse(int initialTicks) {
		if (wakeUpPulse != null) {
			return;
		}

		lobbyNPC.startAwaken();
		spawnBoss();

		wakeUpPulse = CycleEventHandler.getSingleton().addEvent(object, new CycleEvent() {

			int ticks = initialTicks;

			@Override
			public void execute(CycleEventContainer container) {
				switch (ticks) {
					case 67:
						alertPlayers("The Nightmare will awaken in 40 seconds!");
						break;
					case 50:
						alertPlayers("The Nightmare will awaken in 30 seconds!");
						break;
					case 34:
						alertPlayers("The Nightmare will awaken in 20 seconds!");
						break;
					case 17:
						alertPlayers("The Nightmare will awaken in 10 seconds!");
						break;
				}
				
				if (ticks <= 0) {
					alertPlayers("<col=E00A19>The Nightmare has awaken!</col>");
					boss.awaken();
					lobbyNPC.awaken();
					wakeUpPulse = null;
					container.stop();
					return;
				}
				
				ticks--;
			}

		}, 1);
	}

	private static void alertPlayers(String message) {
		forEachPlayer(player -> player.sendMessage(message));
		forEachLobbyPlayer(player -> player.sendMessage(message));
	}

	public static void forEachPlayer(Consumer<Player> consumer) {
		for (int i = 0, size = players.size(); i < size; i++) {
			int playerIndex = players.get(i);
			Player player = PlayerHandler.getPlayerByMID(playerIndex);
			if (player == null || !player.isActive()) {
				continue;
			}
			
			consumer.accept(player);
		}
	}

	public static String buildFirstInspectMessage() {
		int size = players.size();
		if (size <= 0) {
			return "There are no adventurers in the dream.";
		}
		
		if (size == 1) {
			return "There is 1 adventurer in the dream.";
		}
		
		return "There are " + size + " adventurers in the dream.";
	}

	public static void checkEmpty() {
		if (!players.isEmpty()) {
			return;
		}

		lobbyNPC.sleep();
		boss.death(false);
		if (wakeUpPulse != null) {
			wakeUpPulse.stop();
			wakeUpPulse = null;
		}
	}

}
