package com.soulplay.content.npcs.impl.bosses.wild;

import com.soulplay.Server;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4190})
public class ChaosFanaticNpc extends AbstractNpc {

	public ChaosFanaticNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new ChaosFanaticNpc(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		setAggroRadius(10);
		walkUpToHit = false;
		isAggressive = true;
		attackAnim = ATTACK_ANIM;
		attackType = 2;
	}

	@Override
	public void process() {
		super.process();

		if (isDead())
			return;

		if (stopAttack)
			stopAttack = false;
	}

	@Override
	public boolean loadedSpell(Player p) {

		attackType = 2;

		if (specialAttack(p)) {

		} else if (Misc.randomBoolean(10)) {
			//unequip
			stopAttack = true;
			Projectile proj = new Projectile(Graphic.getOsrsId(551), getCurrentLocation(), p.getCurrentLocation());
			proj.setLockon(p.getProjectileLockon());
			proj.setStartDelay(20);
			proj.setEndDelay(60);
			proj.setEndHeight(20);
			GlobalPackets.createProjectile(proj);

			p.getItems().forceUnequip(PlayerConstants.playerWeapon);
		}

		return true;
	}

	@Override
	public void startAttack(Player p) {
		if (Misc.randomBoolean(1))
			forceChat(Misc.random(ATTACK_MSG));

		startAnimation(ATTACK_ANIM);

		PROJ.setStartLoc(getCurrentLocation());
		PROJ.setEndLoc(p.getCurrentLocation());
		PROJ.setLockon(p.getProjectileLockon());
		PROJ.setStartDelay(45);
		PROJ.setEndDelay(80);
		PROJ.setEndHeight(20);
		GlobalPackets.createProjectile(PROJ);

		p.startGraphic(RANGED_HIT_GFX);

	}

	private static final Projectile PROJ = new Projectile(Graphic.getOsrsId(554), Location.create(0, 0), Location.create(0, 0)).setStartDelay(45);

	private static final Graphic RANGED_HIT_GFX = Graphic.create(Graphic.getOsrsId(305), 60, GraphicType.LOW);

	private static final Animation ATTACK_ANIM = new Animation(Animation.getOsrsAnimId(811));

	private static final String[] ATTACK_MSG = new String[] { "Burn!", "WEUGH!", "Devilish Oxen Roll!",
			"All your wilderness are belong to them!", "AhehHeheuhHhahueHuUEehEahAH", "I shall call him squidgy and he shall be my squidgy!" };

	private boolean specialAttack(Player p) {
		if (!Misc.randomBoolean(10))
			return false;

		stopAttack = true;

		if (Misc.randomBoolean(1))
			forceChat(Misc.random(ATTACK_MSG));

		startAnimation(ATTACK_ANIM);

		Location loc1 = p.getCurrentLocation().copyNew();

		Location loc2 = loc1.transform(-4 + Misc.random(8), -4 + Misc.random(8));

		Location loc3 = loc1.transform(-4 + Misc.random(8), -4 + Misc.random(8));

		Projectile proj = new Projectile(Graphic.getOsrsId(551), getCurrentLocation(), loc1);
		proj.setStartDelay(45);
		proj.setEndDelay(120);
		proj.setEndHeight(0);

		GlobalPackets.createProjectile(proj);

		proj.setEndLoc(loc2);
		GlobalPackets.createProjectile(proj);

		proj.setEndLoc(loc3);
		GlobalPackets.createProjectile(proj);

		Server.getTaskScheduler().schedule(4, ()-> {

			Server.getStillGraphicsManager().createGfx(loc1, Graphic.getOsrsId(157), 100, 0);
			Server.getStillGraphicsManager().createGfx(loc2, Graphic.getOsrsId(157), 100, 0);
			Server.getStillGraphicsManager().createGfx(loc3, Graphic.getOsrsId(157), 100, 0);

			for (Player others : Server.getRegionManager().getLocalPlayersOptimized(this)) {
				if (others.getCurrentLocation().isWithinDeltaDistance(loc1, 1) || others.getCurrentLocation().isWithinDeltaDistance(loc2, 1) || others.getCurrentLocation().isWithinDeltaDistance(loc3, 1)) {
					others.dealTrueDamage(Misc.random(31), 0, null, this);
				}
			}
		});

		return true;
	}
}
