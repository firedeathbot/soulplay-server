package com.soulplay.content.npcs.impl.bosses.scorpia;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { ScorpiaNpc.SCORPIA_GUARD_ID })
public class ScorpiaGuardNpc extends AbstractNpc {
	
	public static final Animation ATTACK_ANIM = new Animation(6261);
	
	public static final Graphic SPAWN_GFX = Graphic.create(110);
	
	private NPC mother;
	
	private int failHealTick = 0;
	
	public ScorpiaGuardNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new ScorpiaGuardNpc(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		setRetaliates(false);
		isAggressive = false;
		inWildPermanent = true;
		startGraphic(SPAWN_GFX);
	}
	
	@Override
	public void process() {
		super.process();

		if (isDead())
			return;

		failHealTick++;
		
		if (mother != null && !mother.isDead()) {
			//walk towards mother
			Location motherLoc = Projectile.getLocationOffset(getCurrentLocation(), mother.getCurrentLocation(), mother.getSize());
			
			int distance = Misc.distanceToPoint(getX(), getY(), mother.getX()+2, mother.getY()+2);//getDistance(getX(), motherLoc.getX(), getY(), motherLoc.getY());
			
			
			if (distance > 4) {
				moveX = NPCHandler.GetMove(getX(), mother.getX()+2);
				moveY = NPCHandler.GetMove(getY(), mother.getY()+2);
			}
			
			if (getAttackTimer() == 0 && distance <= 4) {
				setAttackTimer(3);
				
				startAnimation(ATTACK_ANIM);
				
				failHealTick = 0;
				
				Projectile proj = new Projectile(109, getCurrentLocation(), motherLoc);
				proj.setLockon(mother.getProjectileLockon());
				proj.setStartHeight(10);
				proj.setEndHeight(15);
				proj.setStartDelay(20);
				proj.setEndDelay(40);
				
				GlobalPackets.createProjectile(proj);
				
				CycleEventHandler.getSingleton().addEvent(mother, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
						mother.healNpc(4/*Misc.random(4)*/);
						
					}
				}, 2);
			}
			
			if (mother.getSkills().getLifepoints() == mother.getSkills().getMaximumLifepoints()) {
				setDead(true);
			}
			
			if (failHealTick >= 25) {
				setDead(true);
			}
		} else {
			setDead(true);
		}
	}

	public NPC getMother() {
		return mother;
	}

	public void setMother(NPC mother) {
		this.mother = mother;
		face(mother);
	}

}
