package com.soulplay.content.npcs.impl.bosses.vorkath;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.instances.PlayerSingleInstance;
import com.soulplay.game.world.map.travel.zone.addon.VorkathZone;
import com.soulplay.util.Misc;

@BossMeta(ids = {8970})
public class VorkathBoss extends Boss {
	
	private static int STAGE_INDEX = 1;
	public static final int HP_FOR_NEXT_STAGE = 2;
	public static final int TELE_POS = 3;
	public static final int FROZEN_PLAYER = 4;
	public static final int HITS_INDEX = 5;
	public static final int ACID_STAGE_INDEX = 6;
	public static final int MINION_INDEX = 7;
	public static final int SPEC_ATTACK_INDEX = 8;
	
	private static final Animation BREATH_ATTACK_ONE = new Animation(Animation.getOsrsAnimId(7952));
	public static final Animation BREATH_UPWARDS = new Animation(Animation.getOsrsAnimId(7960));
	public static final Animation MELEE_ATTACK2 = new Animation(Animation.getOsrsAnimId(7953));
	public static final Animation MELEE_ATTACK = new Animation(Animation.getOsrsAnimId(7951));
	
	public static final Location OUTSIDE_LOC = Location.create(8672, 4052, 0);

	public VorkathBoss(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		
		npc.stopAttack = true;
		
		//don't attack while player is frozen.
		if (c.getFreezeTimer() > 0 || (boolean) npc.dungVariables[ACID_STAGE_INDEX]) {
			return;
		} else {
			if ((boolean) npc.dungVariables[FROZEN_PLAYER]) {
				npc.dungVariables[FROZEN_PLAYER] = false;
				//npc.setAttackTimer(npc.getCombatStats().getAttackSpeed());
				//return;
			}
		}
		
		npc.setAttackTimer(/*npc.getCombatStats().getAttackSpeed()*/5);
		
		int hitsCount = (int) npc.dungVariables[HITS_INDEX];
		
		if (hitsCount < 6) {
			
			npc.dungVariables[HITS_INDEX] = hitsCount+1;

			boolean meleeDistance = c.getPA().withinDistanceToNpc(npc, 1);

			int random = meleeDistance ? Misc.random(6) : Misc.random(5);

			switch(random) {
			case 0: // spit upwards dragonfire
				npc.startAnimation(BREATH_UPWARDS);
				VorkathSpells.fireBomb(c, npc);
				break;
			case 1://range attack
				npc.attackType = 1;
				npc.attackAnim = BREATH_ATTACK_ONE;
				npc.projectiletStartHeight = 30;
				npc.maxHit = 35;
				npc.projectileId = Graphic.getOsrsId(1477);
				npc.projectiletSlope = VorkathSpells.SPELL_SLOPE;
				npc.endGfx = Graphic.getOsrsId(1478);
				npc.stopAttack = false;
				break;
			case 2://normal dragonfire
				VorkathSpells.dragonFire(c, npc, DragonBreathEnum.NORMAL);
				break;
			case 3://venom dragonfire
				VorkathSpells.dragonFire(c, npc, DragonBreathEnum.VENOM);
				break;
			case 4://pink dragonfire
				VorkathSpells.dragonFire(c, npc, DragonBreathEnum.PINK);
				break;
			case 5://blue magic ball
				npc.attackType = 2;
				npc.attackAnim = BREATH_ATTACK_ONE;
				npc.projectiletStartHeight = 30;
				npc.maxHit = 30;
				npc.projectileId = Graphic.getOsrsId(1479);
				npc.projectiletSlope = VorkathSpells.SPELL_SLOPE;
				npc.endGfx = Graphic.getOsrsId(1480);
				npc.stopAttack = false;
				break;
			case 6: // melee
				npc.attackType = 0;
				npc.maxHit = 40;
				npc.attackAnim = MELEE_ATTACK;
				npc.stopAttack = false;
				break;
			}

		} else {
			npc.dungVariables[HITS_INDEX] = 0;
			
			if ((boolean) npc.dungVariables[SPEC_ATTACK_INDEX]) {
				VorkathSpells.freezePlayerSpawnMinion(c, npc);
				npc.setAttackTimer(npc.getCombatStats().getAttackSpeed()*2);
			} else {
				VorkathSpells.shootPool(c, npc);
				npc.dungVariables[ACID_STAGE_INDEX] = true;
			}
			npc.dungVariables[SPEC_ATTACK_INDEX] = !(boolean)npc.dungVariables[SPEC_ATTACK_INDEX];
		}
        
	}
	
	@Override
	public void process(NPC npc) {
		if (npc.npcType == 8970 && !npc.isDead()) {
			final Player p = PlayerHandler.players[npc.spawnedBy];

			if (p != null && p.isActive && p.getY() < 4053) {
				if (p.getZ() == npc.getZ()) {
					NPC sleeper = spawnSleepingVorkath(p, npc.getZ());
					sleeper.startAnimation(DEATH_ANIM);
				}
				npc.removeNpcSafe(true);
			}
		}

	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.dungVariables[STAGE_INDEX] = new Integer(0);
		npc.dungVariables[FROZEN_PLAYER] = new Boolean(false);
		npc.dungVariables[HITS_INDEX] = new Integer(0);
		npc.dungVariables[ACID_STAGE_INDEX] = new Boolean(false);
		npc.dungVariables[SPEC_ATTACK_INDEX] = new Boolean(Misc.random(1)==0);
		npc.resetAttackType = false;
		npc.getSkills().setStaticLevel(Skills.HITPOINTS, 1000);
		npc.getSkills().setStaticLevel(Skills.ATTACK, 100);
		npc.getSkills().setStaticLevel(Skills.DEFENSE, 100);
		npc.getSkills().setStaticLevel(Skills.STRENGTH, 100);
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 20;
	}

	@Override
	public boolean canMove() {
		return false;
	}

	@Override
	public int extraAttackDistance() {
		return 24;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if ((boolean) npc.dungVariables[ACID_STAGE_INDEX]) {
			hit.setDamage(hit.getDamage()/2);
		}
		if ((boolean) npc.dungVariables[FROZEN_PLAYER]) {
			final int damage = hit.getDamage();
			hit.setDamage(0);
			npc.healNpc(damage);
		}
	}

	@Override
	public int getAttackSpeed() {
		return 5;
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (boss.dungVariables[VorkathBoss.MINION_INDEX] != null) {
			NPC n = (NPC) boss.dungVariables[VorkathBoss.MINION_INDEX];
			if (n != null && !n.isDead()) {
				n.setDead(true);
			}
		}
		
		npc.actionTimer = 5;
		
		final Player p = PlayerHandler.players[npc.spawnedBy];
		final int height = npc.getZ();

		if (p != null && p.isActive) {
			CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
				
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					if (!p.disconnected && p.getZ() == height)
						spawnSleepingVorkath(p, height);
				}
			}, 6);
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}
	
	public static final Animation DEATH_ANIM = new Animation(Animation.getOsrsAnimId(7949));
	
	@Override
	public int getDeathAnimation() {
		return DEATH_ANIM.getId();
	}
	
	private static final ForceMovementMask MASK = ForceMovementMask.createMask(0, 2, 2, 0, Direction.NORTH);
	
	public static void startVorkathInstance(Player p, int height) {
		spawnSleepingVorkath(p, height);
	}
	
	public static void enterInstance(Player p) {
		//creates instance for the boss
		final int height = PlayerSingleInstance.getOrCreateZ(p.mySQLIndex);
		
		p.getZones().addDynamicZone(VorkathZone.ZONE); // exit not needed so we are using the static ZONE
		
		if (height != p.getZ()) {
			p.getPA().movePlayer(p.getX(), p.getY(), height);
			startVorkathInstance(p, height);
		}
		
		p.performingAction = true;
				
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();

				p.startAnimation(AgilityAnimations.CRAWL_OVER_FENCE);
				p.setForceMovement(MASK);
//				p.setHomeOwnerId(p.mySQLIndex);

			}
		}, 2);
	}
	
	public static void exitInstance(Player p) {
		p.startAnimation(AgilityAnimations.CRAWL_OVER_FENCE);
		ForceMovementMask mask = ForceMovementMask.createMask(0, -2, 2, 0, Direction.SOUTH);
		mask.setTeleLoc(VorkathBoss.OUTSIDE_LOC);
		p.setForceMovement(mask);
//		p.setHomeOwnerId(-1);
	}
	
	public static void pokeBoss(Player p, NPC npc) {
		p.startAnimation(Animation.PICK_UP);
		p.faceLocation(VorkathSpells.CENTER);
		
		npc.startAnimation(27950);
		npc.faceLocation(8672, 4058);
		final int x = npc.getX(), y = npc.getY(), z = npc.getZ(), hp = 750;
		npc.removeNpcSafe(true);
		
		final NPC boss = NPCHandler.spawnNpc(p, 8970, x, y, z, 0, true, false);
		boss.startAnimation(27950);
		boss.setAttackable(false);
		boss.setAttackTimer(8);
//		boss.setHomeOwnerId(p.mySQLIndex);
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (boss.isActive)
					boss.setAttackable(true);
			}
				
		}, 7);
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
	public static NPC spawnSleepingVorkath(Player p, int height) {
		return NPCHandler.spawnNpc(p, 8971, 8669, 4062, height, 0, false, false);
	}
}

