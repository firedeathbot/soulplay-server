package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.player.Client;

public abstract class NightmareTotem extends AbstractNpc {

	public NightmareTotem(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		isAggressive = false;
		setRetaliates(false);
		stopAttackDefault = true;
	}

	@Override
	public void onDeath() {
		super.onDeath();

		getSkills().healToFull();
		transform(getFullChargeNpcId());
		NightmareManager.forEachPlayer(player -> player.sendMessage(getChargedMessage()));
		NightmareNPC.totemAttack();
		applyDead = false;
		setDead(false);
	}

	@Override
	public boolean isAttackable() {
		if (getNpcTypeSmart() == getChargeNpcId()) {
			return true;
		}

		return false;
	}

	public boolean isCharged() {
		return getNpcTypeSmart() == getFullChargeNpcId();
	}

	public int getLpFixed() {
		if (isCharged()) {
			return getSkills().getMaximumLifepoints();
		}

		return getSkills().getMaximumLifepoints() - getSkills().getLifepoints();
	}

	@Override
	public int dealDamage(Hit hit) {
		if (hit.getDamage() > 0) {
			// TODO change hitsplat?
		}

		int damage = super.dealDamage(hit);
		NightmareNPC.updateTotemHud();

		return damage;
	}

	public abstract int getFullChargeNpcId();
	public abstract int getChargeNpcId();
	public abstract String getChargedMessage();

}
