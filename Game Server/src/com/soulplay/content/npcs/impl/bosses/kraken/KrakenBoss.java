package com.soulplay.content.npcs.impl.bosses.kraken;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.map.travel.zone.impl.KrakenZone;
import com.soulplay.util.Misc;

@BossMeta(ids = {8732})
public class KrakenBoss extends Boss {
	
	private static final Animation WAKE_ANIM = new Animation(Animation.getOsrsAnimId(7135));
	private static final Animation DEATH_ANIM = new Animation(3993);
	private static final Animation ATTACK_ANIM = new Animation(3992);
	
	private KrakenTentacle tent1, tent2, tent3, tent4;
	
	private boolean spawnedTentacles;

	public KrakenBoss(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.projectileId = Graphic.getOsrsId(156);
		npc.endGfx = Graphic.getOsrsId(157);
	}

	@Override
	public void process(NPC npc) {
		
		//did not transform yet
		if (spawnedTentacles && !npc.isAttackable() && !isAwake(npc)) {
			if ((tent1.isDead() || KrakenTentacle.isAwake(tent1))
					&& (tent2.isDead() || KrakenTentacle.isAwake(tent2))
					&& (tent3.isDead() || KrakenTentacle.isAwake(tent3))
					&& (tent4.isDead() || KrakenTentacle.isAwake(tent4))) {
				npc.setAttackable(true);
				
			}
		}
		
		if (tent1 != null) {
		}
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.stopAttack = true;
		npc.attackType = 2;
		npc.resetAttackType = false;
		
		npc.attackAnim = ATTACK_ANIM;
		
		npc.setAttackable(false);
		npc.setRetaliates(false);
		
		Server.getTaskScheduler().schedule(()-> {
			tent1 = spawnTentacle(npc, -3, 0);
			tent2 = spawnTentacle(npc, -3, 3);
			tent3 = spawnTentacle(npc, 6, 3);
			tent4 = spawnTentacle(npc, 6, 0);
			spawnedTentacles = true;
		});
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 0;
	}

	@Override
	public boolean canMove() {
		return false;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (npc.alwaysRespawn) {
			clearNonInstancedMinions();
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		if (npc.isAttackable() && !isAwake(npc)) {
			npc.transform(8980);
			npc.setAttackable(true);
			npc.startAnimation(WAKE_ANIM);
			npc.isAggressive = true;
			npc.setAttackTimer(4);
			npc.setRetaliates(true);
			npc.stopAttack = false;
		}
	}
	
	@Override
	public void onNull(NPC npc) {
		super.onNull(npc);
		clear();
	}
	
	@Override
	public void onDestroy(NPC npc) {
		super.onDestroy(npc);
		clear();
	};
	
	@Override
	public int getDeathAnimation() {
		return DEATH_ANIM.getId();
	}
	
	@Override
	public boolean isMultiCombat() {
		return true;
	}
	
	private void clear() {
		if (tent1 != null)
			tent1.setDead(true);
		if (tent2 != null)
			tent2.setDead(true);
		if (tent3 != null)
			tent3.setDead(true);
		if (tent4 != null)
			tent4.setDead(true);
	}
	
	private void clearNonInstancedMinions() {
		if (tent1 != null && !tent1.isDead())
			tent1.setAliveTick(5);
		if (tent2 != null && !tent2.isDead())
			tent2.setAliveTick(5);
		if (tent3 != null && !tent3.isDead())
			tent3.setAliveTick(5);
		if (tent4 != null && !tent4.isDead())
			tent4.setAliveTick(5);
	}
	
	private KrakenTentacle spawnTentacle(NPC boss, int offX, int offY) {
		int index = Misc.findFreeNpcSlot();
		KrakenTentacle npc = new KrakenTentacle(index, 8729);
		if (index != -1)
			NPCHandler.spawnNpc(npc, boss.getX() + offX, boss.getY() + offY, boss.getZ(), 0);
		return npc;
	}
	
	private boolean isAwake(NPC npc) {
		return npc.transformId == 8980;
	}
	
	public static void addPlayerToBossZone(Player p, int z) {
		p.getZones().addDynamicZone(KrakenZone.createInstancedZone());
		p.getPA().movePlayer(10096, 5798, z);
	}

	public static void logoutInsideKrakenInstancedMap(Player p) {
		if (p.getZones().getZoneList().contains(KrakenZone.ZONE1) || p.getZones().getZoneList().contains(KrakenZone.ZONE2)) {
			p.getPA().movePlayer(LocationConstants.KRAKEN_ENTRANCE);
		}
	}
}
