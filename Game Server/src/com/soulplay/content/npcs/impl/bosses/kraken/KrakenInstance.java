package com.soulplay.content.npcs.impl.bosses.kraken;

import com.soulplay.Server;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.travel.zone.impl.KrakenZone;
import com.soulplay.util.Misc;

public class KrakenInstance {

	public KrakenInstance() {
		
	}
	
	private CycleEventContainer container;
	
	private int instanceKey;
	
	private NPC boss;
	
	private int ticksRemaining;
	
	private int nextSpawnTick = 0;
	
	private Location spawnLoc, entrance;
	
	private int npcId, walkType;
	
	private boolean applyDeath;
	
	public int getTimeRemaining() {
		return ticksRemaining;
	}
	
	public int getInstanceKey() {
		return instanceKey;
	}
	
	public Location getEntranceLoc() {
		return entrance;
	}
	
	private void startCycle(final int time) {
		container = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			
			@Override
			public void stop() {
				if (boss != null) {
					if (boss.alwaysRespawn)
						boss.alwaysRespawn = false;
					if (!boss.isDead())
						boss.nullNPC();
				}
				KrakenInstanceManager.removeInstance(instanceKey);
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				ticksRemaining--;
				if (ticksRemaining <= 0) {
					container.stop();
					return;
				}
				if (boss != null) {
					if (!applyDeath && boss.isDead()) {
						applyDeath = true;
						nextSpawnTick = Server.getTotalTicks() + 11;
					}
				}
				
				if (applyDeath && nextSpawnTick <= Server.getTotalTicks()) {
					boss = spawnBoss();
				}
			}
		}, 1);
	}
	
	private NPC spawnBoss() {
		NPC npc = NPCHandler.spawnNpc2(npcId, spawnLoc.getX(), spawnLoc.getY(), spawnLoc.getZ(), walkType);
		npc.applyBoss(false);
		applyDeath = false;
		return npc;
	}
	
	public void telePlayerInside(Player p) {
		p.getZones().addDynamicZone(KrakenZone.createInstancedZone());
		p.getPA().movePlayer(entrance);
		p.sendMessage("Time left: "+ Misc.ticksToTime(ticksRemaining, false));
	}
	
	public static KrakenInstance createBossInstance(int instanceKey, int npcId, Location respawnLoc, Location entrance, int time, int walkType) {
		KrakenInstance inst = new KrakenInstance();
		inst.instanceKey = instanceKey;
		inst.spawnLoc = respawnLoc;
		inst.entrance = entrance;
		inst.npcId = npcId;
		inst.walkType = walkType;
		inst.ticksRemaining = time;
		inst.boss = inst.spawnBoss();
		inst.startCycle(time);
		return inst;
	}
	
	
	
}
