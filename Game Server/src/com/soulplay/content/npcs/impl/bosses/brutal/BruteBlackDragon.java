package com.soulplay.content.npcs.impl.bosses.brutal;

import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 107275 })
public class BruteBlackDragon extends BrutalDragon {

	public BruteBlackDragon(int slot, int npcType) {
		super(slot, npcType, 29);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107275, 28, 26, 80);
	}

}
