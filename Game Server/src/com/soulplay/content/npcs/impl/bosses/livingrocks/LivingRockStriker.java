package com.soulplay.content.npcs.impl.bosses.livingrocks;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;

@NpcMeta(ids = { 8833 })
public final class LivingRockStriker extends LivingRockNpc {

	private static final int MELEE_ATTACK_ANIM = 12204;
	private static final int RANGE_ATTACK_ANIM = 12196;
	private static final int RANGE_PROJECTILE = 1887;
	private static final int DEATH_ANIM = 12206;
	private static final int TRANSFORM_ID = 8838;

	public LivingRockStriker(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		getSkills().setStaticLevel(Skills.HITPOINTS, 275);
		getSkills().setStaticLevel(Skills.ATTACK, 130);
		getSkills().setStaticLevel(Skills.STRENGTH, 140);
		getSkills().setStaticLevel(Skills.DEFENSE, 150);
		getSkills().setStaticLevel(Skills.MAGIC, 1);
		getSkills().setStaticLevel(Skills.RANGED, 320);

		getCombatStats().setRangeStrengthBonus(55);
		getCombatStats().setRangedAccBonus(55);
		getCombatStats().setAttackStrength(130);
		getCombatStats().setStrengthBonus(20);

		getCombatStats().setStabDefBonus(50);
		getCombatStats().setSlashDefBonus(50);
		getCombatStats().setCrushDefBonus(10);
		getCombatStats().setMagicDefBonus(150);
		getCombatStats().setRangedDefBonus(300);

		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setFollowDistance(8);
		stopAttackDefault = true;
	}

	@Override
	public void startAttack(Player p) {
		if (getCurrentLocation().getDistance(p.getCurrentLocation()) <= getSize()) {
			meleeSwing(p, 8, MELEE_ATTACK_ANIM);
		} else {
			startAnimation(RANGE_ATTACK_ANIM);
			projectileSwing(p, RANGE_PROJECTILE, 14, 3);
		}
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(8833, -1, -1, MELEE_ATTACK_ANIM);
	}

	@Override
	public int getDeathAnim() {
		return DEATH_ANIM;
	}

	@Override
	public int getTransformId() {
		return TRANSFORM_ID;
	}

}