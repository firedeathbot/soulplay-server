package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class NightmareZone extends Zone {

	@Override
	public void onChange(Player p) {
		/* empty */
	}

	@Override
	public void onEntry(Player p) {
		p.extendedNpcRendering = true;

		int index = p.mySQLIndex;
		if (!NightmareManager.players.contains(index)) {
			NightmareManager.players.add(index);
		}
	}

	@Override
	public void onExit(Player p) {
		p.extendedNpcRendering = false;
		p.blockRun = false;
		p.attr().remove("nightmare_parasite");
		p.attr().remove("nightmare_spawn_parasite");
		p.attr().remove("nightmare_curse");
		p.attr().remove("nightmare_drowsy");
		p.resetFreeze();

		NightmareManager.players.remove((Integer) p.mySQLIndex);
		NightmareManager.checkEmpty();
	}

	@Override
	public void onLogout(Player player) {
		super.onLogout(player);
		onExit(player);
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 10262 && p.getY() >= 9941 && p.getX() <= 10282 && p.getY() <= 9961;
	}

	@Override
	public boolean storeDeathItems() {
		return true;
	}

}
