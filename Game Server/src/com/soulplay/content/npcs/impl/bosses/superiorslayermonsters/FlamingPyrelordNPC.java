package com.soulplay.content.npcs.impl.bosses.superiorslayermonsters;

import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 107394 })
public class FlamingPyrelordNPC extends SuperiorNPC {

	public FlamingPyrelordNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new FlamingPyrelordNPC(slot, npcType);
	}

	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107394, Animation.getOsrsAnimId(1580), Animation.getOsrsAnimId(1581),
				Animation.getOsrsAnimId(1582));
	}

}
