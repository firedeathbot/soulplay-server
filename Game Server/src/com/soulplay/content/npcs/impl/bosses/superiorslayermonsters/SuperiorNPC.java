package com.soulplay.content.npcs.impl.bosses.superiorslayermonsters;

import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.player.Player;

public abstract class SuperiorNPC extends CombatNPC {

	public SuperiorNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		Player killer = getKiller();
		if (killer != null) {
			killer.superior = false;
		}
	}

}
