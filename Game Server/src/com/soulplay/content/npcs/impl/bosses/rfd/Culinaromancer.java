package com.soulplay.content.npcs.impl.bosses.rfd;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@BossMeta(ids = {3491})
public class Culinaromancer extends Boss {

	public Culinaromancer(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.attackType = 2;
		npc.projectileId = 592;
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.walkUpToHit = true;
		npc.getDistanceRules().setMagicDistance(1);
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 100;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		npc.actionTimer = 5;
		npc.faceLocation(npc.getCurrentLocation().transform(0, -1));
		final Location start = npc.getCurrentLocation().copyNew();

		if (c != null) {
			
			if (c != null && c.isActive)
				RecipeForDisaster.defeatedBoss(c);
			
			c.performingAction = true;
			
			
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				int tick = 0;
				@Override
				public void stop() {
					c.performingAction = false;
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					tick++;
					if (tick == 5) {
						Server.getStillGraphicsManager().stillGraphics(c, start.getX(), start.getY(), 0, 591, 30);
					}
					if (tick == 15) {
						for (int i = 0; i < 10; i++) {
							Location ranLoc = Location.create(start.getX() - 4 + Misc.random(8), start.getY() - 4 + Misc.random(8), start.getZ());
							Projectile proj = new Projectile(getRandomProjectile(), start, ranLoc);
							proj.setEndDelay(180);
							GlobalPackets.createProjectileForPlayerOnly(c, proj);
						}
					}
					if (tick > 20) {
						container.stop();
					}
				}
			}, 1);
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
	private static int getRandomProjectile() {
		return 592 + Misc.newRandom(3);
	}

}
