package com.soulplay.content.npcs.impl.bosses.wild;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4186})
public class CrazyArchaeologistNpc extends AbstractNpc {

	public CrazyArchaeologistNpc(int slot, int npcType) {
		super(slot, npcType);
	}
	
	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new CrazyArchaeologistNpc(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		setAggroRadius(10);
		walkUpToHit = false;
		isAggressive = true;
	}
	
	private static final String DEATH_MSG = "Ow!";
	
	@Override
	public void onDeath() {
		super.onDeath();
		forceChat(DEATH_MSG);
	}
	
	@Override
	public void process() {
		super.process();
		
		if (isDead())
			return;
		
		if (stopAttack)
			stopAttack = false;
	}
	
	@Override
	public boolean loadedSpell(Player p) {
		
		int distance = Misc.distanceToPoint(p.getX(), p.getY(), getX(), getY());
		
		if (!specialAttack(p)) {
			
			if (distance <= 1 && !Misc.randomBoolean(10)) { //melee
				attackType = 0;
				attackAnim = KICK;
			} else {
				attackType = 1;
				attackAnim = THROW_BOOK;
			}
			
		} else {
			attackType = 2;//spec
		}
		
		return true;
	}
	
	@Override
	public void startAttack(Player p) {
		forceChat(Misc.random(ATTACK_MSG));
		
		if (attackType == 1) {
			Projectile proj = new Projectile(Graphic.getOsrsId(1259), getCurrentLocation(), p.getCurrentLocation());
			proj.setLockon(p.getProjectileLockon());
			proj.setStartDelay(45);
			proj.setEndDelay(80);
			proj.setEndHeight(20);
			GlobalPackets.createProjectile(proj);
			
			p.startGraphic(RANGED_HIT_GFX);
			
		}
	}
	
	private static final Graphic RANGED_HIT_GFX = Graphic.create(Graphic.getOsrsId(305), 60, GraphicType.LOW);
	
	private static final Animation KICK = new Animation(423);
	private static final Animation THROW_BOOK = new Animation(Animation.getOsrsAnimId(3353));
	
	private static final String[] ATTACK_MSG = new String[] { "I'm Bellock - respect me!", "Get off my site!", "No-one messes with Bellock's dig!",
			"These ruins are mine!", "Taste my knowledge!", "You belong in a museum!" };
	
	private static final String SPEC_MSG = "Rain of knowledge!";
	
	private boolean specialAttack(Player p) {
		if (!Misc.randomBoolean(10))
			return false;
		
		stopAttack = true;
		
		forceChat(SPEC_MSG);
		
		startAnimation(THROW_BOOK);
		
		Location loc1 = p.getCurrentLocation().copyNew();
		
		Location loc2 = loc1.transform(-4 + Misc.random(8), -4 + Misc.random(8));
		
		Location loc3 = loc1.transform(-4 + Misc.random(8), -4 + Misc.random(8));
		

		Location loc1Off1 = loc1.transform(-3 + Misc.random(6), -3 + Misc.random(6));
		Location loc1Off2 = loc1.transform(-3 + Misc.random(6), -3 + Misc.random(6));
		
		Projectile proj = new Projectile(Graphic.getOsrsId(1260), getCurrentLocation(), loc1);
		proj.setStartDelay(45);
		proj.setEndDelay(120);
		proj.setEndHeight(0);
		
		GlobalPackets.createProjectile(proj);
		
		proj.setEndLoc(loc2);
		GlobalPackets.createProjectile(proj);
		
		proj.setEndLoc(loc3);
		GlobalPackets.createProjectile(proj);
		
		Server.getTaskScheduler().schedule(4, ()-> {
			
			Projectile proj2 = new Projectile(Graphic.getOsrsId(1260), loc1, loc1Off1);
			proj2.setStartDelay(10);
			proj2.setEndDelay(90);
			proj2.setEndHeight(0);
			
			GlobalPackets.createProjectile(proj2);
			
			proj2.setEndLoc(loc1Off2);
			GlobalPackets.createProjectile(proj2);
			
			Server.getStillGraphicsManager().createGfx(loc1, Graphic.getOsrsId(157), 100, 0);
			Server.getStillGraphicsManager().createGfx(loc2, Graphic.getOsrsId(157), 100, 0);
			Server.getStillGraphicsManager().createGfx(loc3, Graphic.getOsrsId(157), 100, 0);
			
			for (Player others : Server.getRegionManager().getLocalPlayersOptimized(this)) {
				if (others.getCurrentLocation().isWithinDeltaDistance(loc1, 1) || others.getCurrentLocation().isWithinDeltaDistance(loc2, 1) || others.getCurrentLocation().isWithinDeltaDistance(loc3, 1)) {
					others.dealTrueDamage(Misc.random(24), 0, null, this);
				}
			}
		});
		
		Server.getTaskScheduler().schedule(7, ()-> {
			
			Server.getStillGraphicsManager().createGfx(loc1Off1, Graphic.getOsrsId(157), 100, 0);
			Server.getStillGraphicsManager().createGfx(loc1Off2, Graphic.getOsrsId(157), 100, 0);
			
			for (Player others : Server.getRegionManager().getLocalPlayersOptimized(this)) {
				if (others.getCurrentLocation().isWithinDeltaDistance(loc1Off1, 1) || others.getCurrentLocation().isWithinDeltaDistance(loc1Off2, 1)) {
					others.dealTrueDamage(Misc.random(24), 0, null, this);
				}
			}
		});
		
		
		
		return true;
	}
}
