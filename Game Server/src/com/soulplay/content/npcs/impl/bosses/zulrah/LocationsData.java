package com.soulplay.content.npcs.impl.bosses.zulrah;

import com.soulplay.game.world.entity.Location;

public class LocationsData {
	
	private static final int off = 6400;

	// cloud locations
	public final static Location WEST_NORTH = Location.create(2262 + off, 3075);
	public final static Location WEST_SOUTH = Location.create(2262 + off, 3072);
	public final static Location SOUTH_WEST = Location.create(2262 + off, 3069);
	public final static Location SOUTH_MIDDLE = Location.create(2265 + off, 3068);
	public final static Location SOUTH_EAST = Location.create(2268 + off, 3068);
	public final static Location EAST_NORTH_NORTH = Location.create(2272 + off, 3077);
	public final static Location EAST_NORTH = Location.create(2272 + off, 3074);
	public final static Location EAST_SOUTH = Location.create(2272 + off, 3071);
	public final static Location EAST_SOUTH_SOUTH = Location.create(2271 + off, 3068);
	
	//zulrah locations
	public final static Location MID = Location.create(2266 + off, 3073);
	public final static Location SOUTH = Location.create(2266 + off, 3063);
	public final static Location EAST = Location.create(2275 + off, 3072);
	public final static Location WEST = Location.create(2256 + off, 3072);
	
	//snakelings locations
	public static final Location S_EAST_NORTH = Location.create(2273 + off, 3077);
	public static final Location S_EAST_MID = Location.create(2273 + off, 3075);
	public static final Location S_EAST_SOUTH = Location.create(2273 + off, 3071);
	public static final Location S_EAST_SOUTH_SOUTH = Location.create(2272 + off, 3069);
	public static final Location S_SOUTH_EAST = Location.create(2269 + off, 3069);
	public static final Location S_SOUTH_WEST = Location.create(2266 + off, 3069);
	public static final Location S_WEST_SOUTH_SOUTH = Location.create(2263 + off, 3070);
	public static final Location S_WEST_SOUTH = Location.create(2263 + off, 3073);
	public static final Location S_WEST_NORTH = Location.create(2263 + off, 3076);


	//Player location
	public static final Location PLAYER_START = Location.create(2268 + off, 3069);


}
