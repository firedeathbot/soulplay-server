package com.soulplay.content.npcs.impl.bosses.nex;

import java.util.ArrayList;
import java.util.Map.Entry;

import com.soulplay.Server;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.config.CycleEventIds;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 13447 })
public class Nex extends AbstractNpc {

	private static final int FUMUS_STAGE = 0;
	private static final int UMBRA_STAGE = 1;
	private static final int CRUOR_STAGE = 2;
	private static final int GLACIES_STAGE = 3;
	private static final int FINAL_STAGE = 4;
	private NPC FUMUS, UMBRA, CRUOR, GLACIES;
	private int phase;
	private boolean[] mageAttackable = new boolean[4];
	private boolean bloodSoak = false;

	public Nex(int slot, int npcType) {
		super(slot, npcType);

		startNex();
		prayerReductionMagic = 0.4;
		prayerReductionMelee = 0.4;
		prayerReductionRanged = 0.4;
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new Nex(slot, npcType);
	}

	private void bloodSpellSoakDamage() {
		bloodSoak = true;
		startAnimation(6948);
	}

	private void checkForNextStage() {

		try {

			if (FUMUS != null && FUMUS.isDead()) {
				FUMUS = null;
			}
			if (UMBRA != null && UMBRA.isDead()) {
				UMBRA = null;
			}
			if (CRUOR != null && CRUOR.isDead()) {
				CRUOR = null;
			}
			if (GLACIES != null && GLACIES.isDead()) {
				GLACIES = null;
			}

			if (phase == GLACIES_STAGE) {
				if (mageAttackable[3] && GLACIES == null) {
					phase = FINAL_STAGE;
				}
				if (getSkills().getLifepoints() <= 4000 && !mageAttackable[3]) { // make nex
																	// untargetable
																	// and
																	// minion
																	// targettable
					setAttackable(false);
					sendMessageToPlayersInRoom(
							"<col=ff0000>Glacies is now attackable! You need to defeat him to weaken Nex!");
					forceChat("Don't fail me, Glacies!");
					startAnimation(6987);
					turnNpc(2914, 5192);
					if (GLACIES != null) {
						GLACIES.setAttackable(true);
						GLACIES.isAggressive = true;
					}
					mageAttackable[3] = true;
				}
			} else if (phase == CRUOR_STAGE) {
				if (mageAttackable[2] && CRUOR == null) {
					phase = GLACIES_STAGE;
				}
				if (getSkills().getLifepoints() <= 8000 && !mageAttackable[2]) { // make nex
																	// untargetable
																	// and
																	// minion
																	// targettable
					setAttackable(false);
					sendMessageToPlayersInRoom(
							"<col=ff0000>Cruor is now attackable! You need to defeat him to weaken Nex!");
					forceChat("Don't fail me, Cruor!");
					if (CRUOR != null) {
						CRUOR.setAttackable(true);
						CRUOR.isAggressive = true;
					}
					startAnimation(6987);
					turnNpc(2914, 5192);
					mageAttackable[2] = true;
				}
			} else if (phase == UMBRA_STAGE) {
				if (mageAttackable[1] && UMBRA == null) {
					phase = CRUOR_STAGE;
				}
				if (getSkills().getLifepoints() <= 12000 && !mageAttackable[1]) { // make nex
																	// untargetable
																	// and
																	// minion
																	// targettable
					setAttackable(false);
					sendMessageToPlayersInRoom(
							"<col=ff0000>Umbra is now attackable! You need to defeat him to weaken Nex!");
					forceChat("Don't fail me, Umbra!");
					startAnimation(6987);
					turnNpc(2935, 5214);
					if (UMBRA != null) {
						UMBRA.setAttackable(true);
						UMBRA.isAggressive = true;
					}
					mageAttackable[1] = true;
				}
			} else if (phase == FUMUS_STAGE) {
				if (mageAttackable[0] && FUMUS == null) {
					phase = UMBRA_STAGE;
				}
				if (getSkills().getLifepoints() <= 16000 && !mageAttackable[0]) { // make nex
																	// untargetable
																	// and
																	// minion
																	// targettable
					setAttackable(false);
					sendMessageToPlayersInRoom(
							"<col=ff0000>Fumus is now attackable! You need to defeat her to weaken Nex!");
					forceChat("Don't fail me, Fumus!");
					spawnFumus();
					if (FUMUS != null) {
						FUMUS.setAttackable(true);
						FUMUS.isAggressive = true;
					}
					mageAttackable[0] = true;
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void cough(final Player c) {
		if (c.hasCough()) {
			return;
		}
		if (!getPlayersInRoom().contains(c)) {
			return;
		}
		c.setHasCough(true);

		c.forceChat("*Cough*");

		CycleEventHandler.getSingleton().addEvent(
				CycleEventIds.NEX_COUGH_EVENT.ordinal(), c, new CycleEvent() {

					int ticks = 0;

					@Override
					public void execute(CycleEventContainer container) {
						if (c == null || c.disconnected) {
							container.stop();
							return;
						}
						for (int i = 0; i < 7; i++) {
							if (i == 3/* || i == 5 */) {
								continue;
							}
							if (c.getPlayerLevel()[i] > 1) {
								c.getPlayerLevel()[i]--;
							}
						}
						for (Player p2 : Server.getRegionManager()
								.getLocalPlayers(c)) {
							if (p2 == null || p2 == c) {
								continue;
							}
							if (p2.distanceToPoint(c.getX(), c.getY()) == 1) {
								cough(p2);
							}
						}

						if (ticks == 5 || c.getHitPoints() <= 0) {
							container.stop();
						}

						ticks++;

					}

					@Override
					public void stop() {
						c.setHasCough(false);
					}
				}, 1);
	}

	public void destroy() {
		nullEverything();
	}

	public ArrayList<Player> getPlayersInRoom() {
		ArrayList<Player> temp = new ArrayList<>();
		for (Player p : Server.getRegionManager()
				.getRegionByLocation(2914, 5204, 0).getPlayers()) {
			if (p != null) {
				// if (!temp.contains(p))
				temp.add(p);
			}
		}
		for (Player p : Server.getRegionManager()
				.getRegionByLocation(2928, 5217, 0).getPlayers()) {
			if (p != null) {
				// if (!temp.contains(p))
				temp.add(p);
			}
		}
		return temp;
	}

	public void nullEverything() {
		mageAttackable = null;
		if (FUMUS != null) {
			FUMUS.nullNPC();
			FUMUS = null;
		}
		if (UMBRA != null) {
			UMBRA.nullNPC();
			UMBRA = null;
		}
		if (CRUOR != null) {
			CRUOR.nullNPC();
			CRUOR = null;
		}
		if (GLACIES != null) {
			GLACIES.nullNPC();
			GLACIES = null;
		}
	}

	public void randomSpell(NPC npc) {
		int ran = Misc.random(4);
		if (ran == 0) {
			NexSpells.smokeAttack(npc);
		} else if (ran == 1) {
			NexSpells.bloodBlitzMulti(npc);
		} else if (ran == 2) {
			NexSpells.iceBlitzMulti(npc);
		} else if (ran == 3) {
			NexSpells.shadowBlitzMulti(npc);
		} else if (ran == 4) {
			NexSpells.shadowBlastMulti(npc);
		}
	}

	public void sendMessageToPlayersInRoom(String msg) {
		for (Player p : Server.getRegionManager()
				.getRegionByLocation(2914, 5204, 0).getPlayers()) {
			if (p != null) {
				p.sendMessage(msg);
			}
		}
		for (Player p : Server.getRegionManager()
				.getRegionByLocation(2928, 5217, 0).getPlayers()) {
			if (p != null) {
				p.sendMessage(msg);
			}
		}
	}

	private void spawnCruor() {
//		if (CRUOR != null) {
//			CRUOR.nullNPC();
//			CRUOR = null;
//		}
//		CRUOR = NPCHandler.spawnNpc2(13453, 2935, 5192, 0, 0, 600, 16, 70, 80);
//		CRUOR.setAttackable(false);
//		CRUOR.setCanWalk(false);
	}

	private void spawnFumus() {
//		if (FUMUS != null) {
//			FUMUS.nullNPC();
//			FUMUS = null;
//		}
//		FUMUS = NPCHandler.spawnNpc2(13451, 2914, 5214, 0, 0, 600, 16, 70, 80);
//		FUMUS.setAttackable(false);
//		FUMUS.setCanWalk(false);
	}

	private void spawnGlacies() {
//		if (GLACIES != null) {
//			GLACIES.nullNPC();
//			GLACIES = null;
//		}
//		GLACIES = NPCHandler.spawnNpc2(13454, 2914, 5192, 0, 0, 600, 16, 70,
//				80);
//		GLACIES.setAttackable(false);
//		GLACIES.setCanWalk(false);
	}
	
	private void spawnUmbra() {
//		if (UMBRA != null) {
//			UMBRA.nullNPC();
//			UMBRA = null;
//		}
//		UMBRA = NPCHandler.spawnNpc2(13452, 2935, 5214, 0, 0, 600, 16, 70, 80);
//		UMBRA.setAttackable(false);
//		UMBRA.setCanWalk(false);
	}

	public static final Location CENTER = Location.create(2925, 5203);
	
	public void spawnMinions() {
		if (FUMUS != null) {
			FUMUS.nullNPC();
			FUMUS = null;
		}
		if (UMBRA != null) {
			UMBRA.nullNPC();
			UMBRA = null;
		}
		if (CRUOR != null) {
			CRUOR.nullNPC();
			CRUOR = null;
		}
		if (GLACIES != null) {
			GLACIES.nullNPC();
			GLACIES = null;
		}
		Server.getTaskScheduler().schedule(new Task(2) {

			@Override
			protected void execute() {
				FUMUS = NPCHandler.spawnNpc2(13451, 2912, 5216, 0, 0);
				FUMUS.setAttackable(false);
				FUMUS.setCanWalk(false);
				FUMUS.isAggressive = false;
				FUMUS.faceLocation(CENTER);
				UMBRA = NPCHandler.spawnNpc2(13452, 2937, 5216, 0, 0);
				UMBRA.setAttackable(false);
				UMBRA.setCanWalk(false);
				UMBRA.isAggressive = false;
				UMBRA.faceLocation(CENTER);
				CRUOR = NPCHandler.spawnNpc2(13453, 2937, 5190, 0, 0);
				CRUOR.setAttackable(false);
				CRUOR.setCanWalk(false);
				CRUOR.isAggressive = false;
				CRUOR.faceLocation(CENTER);
				GLACIES = NPCHandler.spawnNpc2(13454, 2912, 5190, 0, 0);
				GLACIES.setAttackable(false);
				GLACIES.setCanWalk(false);
				GLACIES.isAggressive = false;
				GLACIES.faceLocation(CENTER);
				this.stop();
			}
		});
	}

	/**
	 * start nex with stage 1
	 */
	public void startNex() {
		spawnMinions();

		phase = FUMUS_STAGE;
		setAttackable(true);

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			int attackTimer = 0;

			@Override
			public void execute(CycleEventContainer container) {
				// System.out.println("stage 1");

				checkForNextStage();

				if (phase == 1) { // THIS IS USED TO SWITCH TO DIFFFERENT
									// STAGES, SAYS TO STARTNEXSTAGE2 THEN
									// STOP() CURRENT EVENT FOR NEXSTAGE 1

					startNexStage2();
					container.stop();
					return;
				}

				if (isDead()) {
					container.stop();
					// defiler = null;
					destroy();
					return;
				}

				if (teleporting) {

					return;
				}

				if (attackTimer > 0) {
					attackTimer--;
				}

				if (attackTimer == 0) { // cast spell then set cooldown for next
										// spell with attackTimer
					attackTimer = 4;

					if (Misc.random(99) <= 50) { // multi target spell
						// magic attack
						NexSpells.smokeAttack(Nex.this);
					} else { // single target spells

						int randomId = NPCHandler
								.getCloseRandomPlayer(npcIndex, true);

						// System.out.println("runniing task "+randomId);
						if (randomId != -1) {
							Client p = (Client) PlayerHandler.players[randomId];
							if (p != null) {
								if (Misc.random(99) >= 80) {
									cough(p);
								} else {
									if (p.distanceToPoint(getX(),
											getY()) <= 1) {
										// melee attack
										startAnimation(6354);
										NpcHitPlayer.dealMeleeDamage(Nex.this, p,
												Misc.random(30), 2, null); // TODO:
																			// make
																			// sure
																			// delay
																			// is
																			// proper
																			// for
																			// melee
																			// hit
									} else { // if out of range then use spell
										NexSpells.smokeAttack(Nex.this);
									}
								}
							}
						}
					}

				}
			}

			@Override
			public void stop() {

			}
		}, 1); // The delay is here, for each time this goes through the loop
				// again
	}

	/**
	 * start nex with stage 2
	 */
	public void startNexStage2() {
		phase = UMBRA_STAGE;

		setAttackable(true);

		spawnUmbra();

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			int attackTimer = 0;

			@Override
			public void execute(CycleEventContainer container) {

				checkForNextStage();

				if (phase == 2) {
					startNexStage3();
					container.stop();
					return;
				}

				if (isDead()) {
					container.stop();
					// defiler = null;
					destroy();
					return;
				}

				if (teleporting) {

					return;
				}

				// if players are too close to Nex then hit them 40's per tick
				for (Player p : NexSpells.getNearbyPlayers(Nex.this)) {
					for (int x = -1; x < 4; x++) {
						for (int y = -1; y < 4; y++) {
							if (p.getX() == getX() + x
									&& p.getY() == getY() + y) {
								NpcHitPlayer.dealMagicDamage(Nex.this, p, 10, 1, null);
							}
						}
					}
				}

				if (attackTimer > 0) {
					attackTimer--;
				}

				if (attackTimer == 0) { // cast spell then set cooldown for next
										// spell with attackTimer
					attackTimer = 5;

					if (Misc.random(99) <= 50) { // multi target spell
						if (Misc.random(99) <= 50) {
							// magic attack
							NexSpells.shadowBlitzMulti(Nex.this);
						} else {
							// creates holes in ground and hits players
							NexSpells.shadowBlastMulti(Nex.this);
							attackTimer = 6;
						}

					} else { // single target spells

						int randomId = NPCHandler
								.getCloseRandomPlayer(npcIndex, true);

						if (randomId != -1) {
							Client p = (Client) PlayerHandler.players[randomId];
							if (p != null) {
								// if (Misc.random(99) <= 80 &&
								// p.distanceToPoint(getX(), getY()) <=
								// 1) {
								// //melee attack
								// startAnimation(6354);
								// NpcHitPlayer.dealMeleeDamage(nex, p,
								// Misc.random(30), 2, 0, 0); //TODO: make sure
								// delay is proper for melee hit
								// }
							}
						}
					}

				}

			}

			@Override
			public void stop() {

			}
		}, 1);
	}

	/**
	 * start nex with stage 3
	 */
	public void startNexStage3() {
		phase = CRUOR_STAGE;
		setAttackable(true);

		spawnCruor();

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			int attackTimer = 0;

			@Override
			public void execute(CycleEventContainer container) {

				checkForNextStage();

				if (phase == 3) {
					startNexStage4();
					container.stop();
					return;
				}

				if (isDead()) {
					container.stop();
					// defiler = null;
					destroy();
					return;
				}

				if (teleporting) {
					return;
				}

				// randomWalk();

				if (attackTimer > 0) {
					attackTimer--;
				}

				if (attackTimer == 0) { // cast spell then set cooldown for next
										// spell with attackTimer
					if (bloodSoak) {
						bloodSoak = false;
					}

					attackTimer = 5;

					if (Misc.random(99) <= 10) {
						bloodSpellSoakDamage();
						attackTimer = 12;
						return;
					}

					if (Misc.random(99) <= 50) { // multi target spell
						NexSpells.bloodBlitzMulti(Nex.this);

					} else { // single target spells

						int randomId = NPCHandler
								.getCloseRandomPlayer(npcIndex, true);

						if (randomId != -1) {
							Client p = (Client) PlayerHandler.players[randomId];
							if (p != null) {
								if (p.distanceToPoint(getX(),
										getY()) <= 1) {
									// melee attack
									startAnimation(6354);
									NpcHitPlayer.dealMeleeDamage(Nex.this, p,
											Misc.random(30), 2, null); // TODO:
																		// make
																		// sure
																		// delay
																		// is
																		// proper
																		// for
																		// melee
																		// hit
								} else {
									NexSpells.bloodBlitzMulti(Nex.this);
								}
							}
						}
					}

				}

			}

			@Override
			public void stop() {

			}
		}, 1);
	}

	public void startNexStage4() {
		phase = GLACIES_STAGE;
		setAttackable(true);

		spawnGlacies();

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			int attackTimer = 0;

			@Override
			public void execute(CycleEventContainer container) {

				checkForNextStage();

				if (phase == 4) {
					startNexStage5();
					container.stop();
					return;
				}

				if (isDead()) {
					container.stop();
					// defiler = null;
					destroy();
					return;
				}

				if (teleporting) {
					return;
				}

				if (attackTimer > 0) {
					attackTimer--;
				}

				if (attackTimer == 0) { // cast spell then set cooldown for next
										// spell with attackTimer
					attackTimer = 5;

					if (Misc.random(99) <= 50) { // multi target spell
						if (Misc.random(99) <= 10) {
							NexSpells.glacierPrison(Nex.this, getX(), getY(),
									5);
						} else {
							// magic attack
							NexSpells.iceBlitzMulti(Nex.this);
						}

					} else { // single target spells

						int randomId = NPCHandler
								.getCloseRandomPlayer(npcIndex, true);

						if (randomId != -1) {
							Client p = (Client) PlayerHandler.players[randomId];
							if (p != null) {
								if (Misc.random(99) <= 10) {
									NexSpells.glacierPrison(Nex.this, p.getX(),
											p.getY(), 3);
								} else if (p.distanceToPoint(getX(),
										getY()) <= 1) {
									// melee attack
									startAnimation(6354);
									NpcHitPlayer.dealMeleeDamage(Nex.this, p,
											Misc.random(30), 2, null); // TODO:
																		// make
																		// sure
																		// delay
																		// is
																		// proper
																		// for
																		// melee
																		// hit
								} else {
									NexSpells.iceBlitzMulti(Nex.this);
								}
							}
						}
					}

				}

			}

			@Override
			public void stop() {

			}
		}, 1);
	}

	public void startNexStage5() {
		phase = FINAL_STAGE;

		setAttackable(true);

		getSkills().setLevel(Skills.ATTACK, getSkills().getLevel(Skills.ATTACK) * 2);
		getSkills().setLevel(Skills.STRENGTH, getSkills().getLevel(Skills.STRENGTH) * 2);
		getSkills().setLevel(Skills.DEFENSE, (int) (getSkills().getLevel(Skills.DEFENSE) * 1.2));
		getSkills().setLevel(Skills.RANGED, getSkills().getLevel(Skills.RANGED) * 2);
		getSkills().setLevel(Skills.MAGIC, getSkills().getLevel(Skills.MAGIC) * 2);
		forceChat("NOW, THE POWER OF ZAROS!");

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			int attackTimer = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (isDead()) {
					container.stop();
					// defiler = null;
					destroy();
					return;
				}

				if (teleporting) {
					// teleportTo(2935, 5202);
					return;
				}

				if (Misc.random(5) == 0) {
					transformNex();
				}

				// int random = Misc.random(3);
				// if (random == 1) {
				// forceChat("Fill my soul with smoke!");
				// startAnimation(6987);
				// attackType = 2;
				// projectileId = 386;
				// endGfx = 390;
				// } else if(random == 0) {
				// forceChat("Let the virus flow through you!");
				// attackType = 4;
				// projectileId = -1;
				// endGfx = -1;
				// } else if(random == 3) {
				// teleporting = true;
				// startAnimation(6321);
				// }

				if (attackTimer > 0) {
					attackTimer--;
				}

				if (attackTimer == 0) { // cast spell then set cooldown for next
										// spell with attackTimer
					attackTimer = 4;

					if (Misc.random(99) <= 30) { // multi target spell
						// random spell
						randomSpell(Nex.this);
					} else { // single target spells

						int randomId = NPCHandler
								.getCloseRandomPlayer(npcIndex, true);

						if (randomId != -1) {
							Client p = (Client) PlayerHandler.players[randomId];
							if (p != null) {
								if (p.distanceToPoint(getX(),
										getY()) <= 1) {
									// melee attack
									startAnimation(6354);
									NpcHitPlayer.dealMeleeDamage(Nex.this, p,
											Misc.random(30), 2, null); // TODO:
																		// make
																		// sure
																		// delay
																		// is
																		// proper
																		// for
																		// melee
																		// hit
								} else {
									// random spell
									randomSpell(Nex.this);
								}
							}
						}
					}

				}

			}

			@Override
			public void stop() {

			}
		}, 1);
	}

	@Override
	public int dealDamage(Hit hit) {
		int combatType = hit.getHitIcon();
		if ((combatType == 0 && usingProtectPrayer(true, Curses.DEFLECT_MELEE.ordinal())) 
				|| (combatType == 1 && usingProtectPrayer(true, Curses.DEFLECT_MISSILES.ordinal())
				|| (combatType == 2 && usingProtectPrayer(true, Curses.DEFLECT_MAGIC.ordinal())))) { // nex protect prayers reduce damage
			hit.setDamage(hit.getDamage() * 60 / 100);
		}

		int damage;
		if (phase == CRUOR_STAGE) {
			if (bloodSoak) {
				setHP(getSkills().getLifepoints() + hit.getDamage());
				handleHitMask(hit.getDamage(), 2, -1);
				damage = 0;
			} else {
				damage = super.dealDamage(hit);
			}
		} else {
			damage = super.dealDamage(hit);
		}

		return damage;
	}

	public void teleportTo(int teleX, int teleY) {
		// forceChat("teleporting");
		teleporting = true;
		// gfx0(654);
		moveX = 0;
		moveY = 0;
		int spawnX = teleX, spawnY = teleY;
		boolean breaking = false;
		for (int x = 0; x < 2; x++) {
			if (breaking) {
				break;
			}
			for (int y = 0; y < 2; y++) {
				if (x == 0 && y == 0) {
					continue;
				}
				if (PathFinder.canMoveInStaticRegion(teleX, teleY, 0, x, y)) {
					spawnX = teleX + x;
					spawnY = teleY + y;
					breaking = true;
					break;
				}
			}
		}

		final int teleXFinal = spawnX;
		final int teleYFinal = spawnY;

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (isDead()) {
					container.stop();
					return;
				}

				moveX = 0;
				moveY = 0;
				setLastLocation(Location.create(getX(), getY(), getZ()));
				absX = teleXFinal;
				absY = teleYFinal;
				setLocation(getX(), getY());
				didTeleport = true;
				// gfx0(654);
				container.stop();

			}

			@Override
			public void stop() {
				teleporting = false;
				if (isDead()) {
					destroy();
				}
			}
		}, 2);
	}

	private void transformNex() {
		setUsingSoulSplit(false);
		protectPrayer = -1;
		int random = Misc.random(2);
		if (random == 0) {
			transform(13448); // soulsplit nex
			setUsingSoulSplit(true);
		} else if (random == 1) {
			transform(13449); // protect from melee nex
			protectPrayer = Prayers.PROTECT_FROM_MELEE.ordinal(); //TODO: change to deflect melee when it's coded
		} else {
			transform(13450); // protect from range nex
			protectPrayer = Prayers.PROTECT_FROM_MISSILES.ordinal(); // TODO: change to deflect missiles when it's coded
		}
	}

	@Override
	public void afterNpcDeath() {
		super.afterNpcDeath();

		if (NexEvent.hasStarted()) {
			actionTimer = 1;
		}
	}

	@Override
	public void onDeath() {
		super.onDeath();

		if (NexEvent.hasStarted()) {
			NexEvent.end();
		}
	}

}
