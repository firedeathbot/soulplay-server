package com.soulplay.content.npcs.impl.bosses.zulrah;

import com.soulplay.game.world.entity.Location;

public class Stage {
	
	public static final int RANGED_SPELL = 0;
	public static final int BLUE_SPELL = 1;
	public static final int JAD_SPELL = 2;
	public static final int MELEE_SPELL = 3;
	
	public static final int ACTION_SPELL = 0;
	public static final int ACTION_CLOUDS = 1;
	public static final int ACTION_SNAKELINGS = 2;
	public static final int ACTION_HIDE = 3;

	private int spellType = 0; // 0 ranged, 1 magic/range, 2 jad form, 3 melee
	
	private int attacksLeft = 0;
	
	private int actionType = 0; // 0 spell, 1 spawn clouds, 2 spawn minions
	
	private Location cLoc1;
	private Location cLoc2;
	private Location teleLoc;
	private Location minionLoc;
	
	private int spawnTicks = 0;
	
	private int transform = 0;
	
	private Stage(int actionType, int spell, int hits, Location loc1, Location loc2, Location teleLoc, int trans, Location minionLoc, int spawnTimer) {
		this.setSpellType(spell);
		this.setAttacksLeft(hits);
		this.setLoc1(loc1);
		this.setLoc2(loc2);
		this.setTeleLoc(teleLoc);
		this.setTransform(trans);
		this.setMinionLoc(minionLoc);
		this.setSpawnTicks(spawnTimer);
		this.setActionType(actionType);
	}
	
	public static Stage createSpell(int spell, int hits) {
		return new Stage(ACTION_SPELL, spell, hits, null, null, null, 0, null, 0);
	}
	
	public static Stage createClouds(Location loc, Location loc2) {
		return new Stage(ACTION_CLOUDS, 0, 0, loc, loc2, null, 0, null, 0);
	}
	
	public static Stage createMinions(Location loc, int ticks) {
		return new Stage(ACTION_SNAKELINGS, 0, 0, null, null, null, 0, loc, ticks);
	}
	
	/**
	 * To hide zulrah and teleport it to a different location
	 * @param loc - Location of the next teleport spot
	 * @param trans - the form it should be on next stage
	 * @return
	 */
	public static Stage hideZulrah(Location loc, int trans) {
		return new Stage(ACTION_HIDE, 10, 0, null, null, loc, trans, null, 0);
	}

	public int getSpellType() {
		return spellType;
	}

	public void setSpellType(int spellType) {
		this.spellType = spellType;
	}

	public int getAttacksLeft() {
		return attacksLeft;
	}

	public void setAttacksLeft(int attacksLeft) {
		this.attacksLeft = attacksLeft;
	}

	public int getActionType() {
		return actionType;
	}

	public void setActionType(int actionType) {
		this.actionType = actionType;
	}

	public Location getLoc1() {
		return cLoc1;
	}

	public void setLoc1(Location loc1) {
		this.cLoc1 = loc1;
	}

	public Location getLoc2() {
		return cLoc2;
	}

	public void setLoc2(Location loc2) {
		this.cLoc2 = loc2;
	}

	public Location getTeleLoc() {
		return teleLoc;
	}

	public void setTeleLoc(Location teleLoc) {
		this.teleLoc = teleLoc;
	}

	public int getTransform() {
		return transform;
	}

	public void setTransform(int transform) {
		this.transform = transform;
	}

	public Location getMinionLoc() {
		return minionLoc;
	}

	public void setMinionLoc(Location sLoc) {
		this.minionLoc = sLoc;
	}

	public int getSpawnTicks() {
		return spawnTicks;
	}

	public void setSpawnTicks(int spawnTicks) {
		this.spawnTicks = spawnTicks;
	}
	
}
