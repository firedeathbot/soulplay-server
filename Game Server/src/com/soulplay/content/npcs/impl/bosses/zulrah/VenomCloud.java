package com.soulplay.content.npcs.impl.bosses.zulrah;

import com.soulplay.game.world.entity.Location;

public class VenomCloud {

	private Location loc;
	private int ticks;
	
	public VenomCloud(Location loc, int ticks) {
		this.setLoc(loc);
		this.setTicks(ticks);
	}

	public Location getLoc() {
		return loc;
	}

	public void setLoc(Location loc) {
		this.loc = loc;
	}

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}
}
