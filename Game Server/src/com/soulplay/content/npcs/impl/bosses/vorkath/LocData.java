package com.soulplay.content.npcs.impl.bosses.vorkath;

import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class LocData {
	
	private static final Location POISON_MID_1 = Location.create(8669, 4055);//front mid y+6 - x+6
	private static final Location POISON_MID_2 = Location.create(8669, 4069);//back mid y+6 - x+6
	private static final Location POISON_MID_3 = Location.create(8676, 4062);//right mid y+6 - x+6
	private static final Location POISON_MID_4 = Location.create(8662, 4062);//left mid y+6 - x+6
	private static final Location POISON_BOT_RIGHT = Location.create(8676, 4054); // right bottom x+7 y+7
	private static final Location POISON_BOT_LEFT = Location.create(8661, 4054); // left bottom x+7 y+7
	private static final Location POISON_TOP_RIGHT = Location.create(8676, 4069); // right top x+7 y+7
	private static final Location POISON_TOP_LEFT = Location.create(8661, 4069); // left top x+7 y+7
	
	private static final int MID_OFF = 6;
	private static final int CORNER_OFF = 7;
	private static final Location[] MID = { POISON_MID_1, POISON_MID_2, POISON_MID_3, POISON_MID_4 };
	private static final Location[] CORNER = { POISON_BOT_RIGHT, POISON_BOT_LEFT, POISON_TOP_RIGHT, POISON_TOP_LEFT };
	
	public static Location generateMid(int z, int index) {
		return MID[index].transform(Misc.newRandom(MID_OFF), Misc.newRandom(MID_OFF), z);
	}
	
	public static Location generateCorner(int z, int i) {
		return CORNER[i].transform(Misc.newRandom(CORNER_OFF), Misc.newRandom(CORNER_OFF), z);
	}

}
