package com.soulplay.content.npcs.impl.bosses.superiorslayermonsters;

import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 107401 })
public class CaveAbominationNPC extends SuperiorNPC {

	public CaveAbominationNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new CaveAbominationNPC(slot, npcType);
	}

	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		stopAttackDefault = true;
	}

	@Override
	public void startAttack(Player p) {
		if (Misc.random(2) == 0) {
			meleeSwing(p, 24, Animation.getOsrsAnimId(4234), 4);
		} else if (Misc.random(2) == 1) {
			meleeSwing(p, 24, Animation.getOsrsAnimId(4235), 4);
		} else {
			meleeSwing(p, 24, Animation.getOsrsAnimId(4237), 4);
		}
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107401, Animation.getOsrsAnimId(4233), Animation.getOsrsAnimId(4232),
				Animation.getOsrsAnimId(4234));
	}

}
