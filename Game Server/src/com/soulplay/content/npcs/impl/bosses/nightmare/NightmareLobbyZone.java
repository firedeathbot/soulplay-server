package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class NightmareLobbyZone extends Zone {

	@Override
	public void onChange(Player p) {
		/* empty */
	}

	@Override
	public void onEntry(Player p) {
		int index = p.mySQLIndex;
		if (NightmareManager.lobbyPlayers.contains(index)) {
			return;
		}

		NightmareManager.lobbyPlayers.add(index);
	}

	@Override
	public void onExit(Player p) {
		NightmareManager.lobbyPlayers.remove((Integer) p.mySQLIndex);
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 10198 && p.getY() >= 9749 && p.getX() <= 10218 && p.getY() <= 9769;
	}

}
