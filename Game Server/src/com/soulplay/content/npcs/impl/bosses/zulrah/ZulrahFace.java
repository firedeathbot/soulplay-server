package com.soulplay.content.npcs.impl.bosses.zulrah;

public enum ZulrahFace {
	NORTH(0, 5),
	SOUTH(0, -5),
	EAST(5, 1),
	WEST(-5, 1);
	

	private int x;
	private int y;
	
	private ZulrahFace(int x, int y) {
		this.setX(x);
		this.setY(y);
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
}
