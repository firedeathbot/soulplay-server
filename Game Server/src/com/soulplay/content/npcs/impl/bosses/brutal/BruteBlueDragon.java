package com.soulplay.content.npcs.impl.bosses.brutal;

import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 107273 })
public class BruteBlueDragon extends BrutalDragon {

	public BruteBlueDragon(int slot, int npcType) {
		super(slot, npcType, 21);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107273, 28, 26, 80);
	}

}
