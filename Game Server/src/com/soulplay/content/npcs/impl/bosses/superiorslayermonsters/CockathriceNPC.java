package com.soulplay.content.npcs.impl.bosses.superiorslayermonsters;

import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 107393 })
public class CockathriceNPC extends SuperiorNPC {

	public CockathriceNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new CockathriceNPC(slot, npcType);
	}

	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107393, Animation.getOsrsAnimId(1563), -1, Animation.getOsrsAnimId(1562));
	}

}