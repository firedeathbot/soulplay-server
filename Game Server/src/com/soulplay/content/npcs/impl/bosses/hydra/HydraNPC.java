package com.soulplay.content.npcs.impl.bosses.hydra;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.slayer.Assignment;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GroundItemObject;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@NpcMeta(ids = {108609})
public final class HydraNPC extends CombatNPC {
	
	private boolean mageAttack;
	private int attacks = 0;
	private int specialAttack = 0;
	
	public HydraNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
		mageAttack = Misc.randomBoolean();
	}

	@Override
	public void startAttack(Player p) {
		if (attacks > 2) {
			mageAttack = !mageAttack;
			attacks = 0;
		}

		if (specialAttack > 10) {
			setAttackTimer(6);
			multiAcidAttack(p);
			return;
		}

		if (!mageAttack) {
			startAnimation(Animation.getOsrsAnimId(8261));
			projectileSwing(p, Graphic.getOsrsId(1663), 	22, 3, null, CombatType.RANGED);
			specialAttack++;
			attacks++;
			if (p.debugNpc) p.sendMessage("range attack: " + attacks + " special attack: " + specialAttack);
		} else {
			startAnimation(Animation.getOsrsAnimId(8262));
			projectileSwing(p, Graphic.getOsrsId(1662), 	22, 3, null, CombatType.MAGIC);
			specialAttack++;
			attacks++;
			if (p.debugNpc) p.sendMessage("MAGE attack: " + attacks + " special attack: " + specialAttack);
		}
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(108609, 300, 22, 210, 210);
		NPCAnimations.define(108609, Animation.getOsrsAnimId(8264), -1, -1);
	}

	private Location findLocation(Location location, Location last) {
		if (location == null || last == null) {
			return null;
		}

		for (int i = 0; i < 10; i++) {
			Location gen = location.transform(Direction.getRandom(), 1 - Misc.random(2));
			if (gen != null && !gen.matches(location) && !last.matches(gen) && RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}

		return null;
	}

	private void multiAcidAttack(Player p) {
		startAnimation(Animation.getOsrsAnimId(8263));
		Location end = p.getCurrentLocation().copyNew();
		Projectile.create(getCenterLocation(), end, Graphic.getOsrsId(1644)).send();

		final Location finalEnd = end;
		pulse(() -> {
			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				int ticks = 0;

				@Override
				public void execute(CycleEventContainer container) {
					if (p.getCurrentLocation().equals(finalEnd)) {
						p.dealDamage(new Hit(4, 2, -1));
					}

					ticks++;
					if (ticks >= 8) {
						container.stop();
					}
				}

				@Override
				public void stop() {
					
				}
				
			}, 2);

			if (finalEnd.getDistanceInt(p.getCurrentLocation()) <= 1) {
				p.dealDamage(new Hit(4, 2, -1));
			}

			Graphic.create(Graphic.getOsrsId(1655), GraphicType.LOW).send(finalEnd);
		}, 3);

		for (int i = 0; i < 2; i++) {
			end = findLocation(p.getCurrentLocation(), end);
			if (end == null) {
				continue;
			}

			Projectile.create(getCenterLocation(), end, Graphic.getOsrsId(1644)).send();

			final Location finalEnd2 = end;
			pulse(() -> {
				CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

					int ticks = 0;

					@Override
					public void execute(CycleEventContainer container) {
						if (p.getCurrentLocation().equals(finalEnd2)) {
							p.dealDamage(new Hit(4, 2, -1));
						}

						ticks++;
						if (ticks >= 8) {
							container.stop();
						}
					}

					@Override
					public void stop() {
						
					}
					
				}, 2);

				if (finalEnd.getDistanceInt(p.getCurrentLocation()) <= 1) {
					p.dealDamage(new Hit(4, 2, -1));
				}

				Graphic.create(Graphic.getOsrsId(1655), GraphicType.LOW).send(finalEnd2);
			}, 4);
		}

		specialAttack = 0;
	}

}