package com.soulplay.content.npcs.impl.bosses.alchhydra;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GroundItemObject;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108615, 108616, 108617, 108618, 108619, 108620, 108621 })
public class AlchemicalHydra extends CombatNPC {

	private boolean mageAttack;
	private int attacks = 0;
	private int specialAttack = 6;
	private boolean damageReduced = true;
	private int rangeAttacks = 2;
	private int mageAttacks = 2;
	private int thisMaxHit;
	private int rangedAnimation;
	private int magicAnimation;
	private Set<Location> fires = new HashSet<>();
	private Location walkToLocation;
	private Runnable afterWalk;
	private Location templateSpawn;

	public AlchemicalHydra(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttackDefault = true;
		walkUpToHit = false;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
		mageAttack = Misc.randomBoolean();
		thisMaxHit = maxHit;
		rangedAnimation = 8235;
		magicAnimation = 8236;
	}

	public void process() {
		super.process();

		if (damageReduced && inCombatWithPlayer()) {
			Location requiredVent = requiredVent();
			if (requiredVent != null) {
				List<Location> vents = getInstance().getVents();
				for (int i = 0, length = vents.size(); i < length; i++) {
					Location point = vents.get(i);
					int distance = point.getDistanceInt(getCenterLocation());
					if (distance <= 2) {
						if (point == requiredVent) {
							damageReduced = false;
							forceChat("Roaaaaaaaaaaar!");
							getInstance().getPlayer().sendMessage("The chemicals neutralise the Alchemical Hydra's defences!");
						} else if (thisMaxHit < 26) {
							getInstance().getPlayer().sendMessage("The chemicals are absorbed by the Alchemical Hydra; empowering it further!");
							thisMaxHit += 2;
							thisMaxHit = Math.min(thisMaxHit, 26);
						}
					}
				}
			}
		}

		if (walkToLocation != null) {
			walkToLocation(walkToLocation);

			int dist = (int) getCurrentLocation().getDistance(walkToLocation);
			if (dist <= 1) {
				walkToLocation = null;
				moveX = 0;
				moveY = 0;
				setRetaliates(true);

				pulse(() -> {
					afterWalk.run();
					afterWalk = null;
				});
			}
		}
	}

	@Override
	public void startAttack(Player p) {
		int phase = getPhaseId();
		if (specialAttack >= 9) {
			specialAttack = 0;

			switch (phase) {
				case 0:
				case 3:
					acidSpecial(p);
					break;
				case 1:
					lightningAttack(p);
					break;
				case 2:
					fireAttack(p);
					break;
			}

			return;
		}

		if (attacks > 2 || phase == 3) {
			mageAttack = !mageAttack;
			attacks = 0;
		}

		if (!mageAttack) {
			startAnimation(Animation.getOsrsAnimId(rangedAnimation));
			for (int i = 0; i < rangeAttacks; i++) {
				projectileSwing(p, Graphic.getOsrsId(1663), thisMaxHit, 3, null, CombatType.RANGED);
			}
		} else {
			startAnimation(Animation.getOsrsAnimId(magicAnimation));
			for (int i = 0; i < mageAttacks; i++) {
				projectileSwing(p, Graphic.getOsrsId(1662), thisMaxHit, 3, null, CombatType.MAGIC);
			}
		}

		specialAttack++;
		attacks++;
	}

	@Override
	public int dealDamage(Hit hit) {
		if (damageReduced) {
			hit.setDamage(hit.getDamage() * 75 / 100);
		}

		int phase = getPhaseId();
		int damage = super.dealDamage(hit);
		int percentHp = getSkills().getLifepoints()  * 100 / getSkills().getMaximumLifepoints();
		if (phase == 0 && percentHp <= 75) {
			switchPhase(108616, 108619, 8237, 8238, 8242, 8243, 3, 1, 2, true);
		} else if (phase == 1 && percentHp <= 50) {
			switchPhase(108617, 108620, 8244, 8245, 8249, 8250, 2, 1, 1, true);
		} else if (phase == 2 && percentHp <= 25) {
			switchPhase(108618, 108621, 8251, 8252, 8255, 8256, 2, 1, 1, false);
			thisMaxHit = 52;
		}

		return damage;
	}

	private void switchPhase(int tempId, int npcId, int transformAnim, int switchAnim, int rangedAnim, int magicAnim, int ticks, int rangeAtks, int mageAtks, boolean damageReduction) {
		final int transformAnimFinal = Animation.getOsrsAnimId(transformAnim);
		final int switchAnimFinal = Animation.getOsrsAnimId(switchAnim);

		rangeAttacks = rangeAtks;
		mageAttacks = mageAtks;
		rangedAnimation = rangedAnim;
		magicAnimation = magicAnim;
		takeDamage = false;
		damageReduced = damageReduction;
		specialAttack = 6;
		startAnimation(transformAnimFinal);
		transform(tempId);
		setAttackTimer(2 + ticks);
		resetFace();
		dontFace = true;
		pulse(() -> {
			startAnimation(switchAnimFinal);
			transform(npcId);
			takeDamage = true;
			dontFace = false;
			thisMaxHit = maxHit;
		}, ticks);
	}

	private void lightningAttack(Player p) {
		setAttackTimer(25);
		startAnimation(Animation.osrs(8241));

		Location center = getInstance().base(22, 27);
		Projectile projectile = Projectile.create(getCenterLocation(), center, Graphic.getOsrsId(1664));
		projectile.setStartHeight(50);
		projectile.setEndHeight(50);
		projectile.setStartDelay(15);
		projectile.setEndDelay(75);
		projectile.setSlope(5);
		projectile.setStartOffset(128);
		projectile.send();

		pulse(() -> {
			Graphic.osrs(1664, GraphicType.HYDRA).send(center);

			startLightning(p, center, getInstance().base(27, 23));
			pulse(() -> {
				startLightning(p, center, getInstance().base(27, 32));
				pulse(() -> {
					startLightning(p, center, getInstance().base(18, 32));
					pulse(() -> {
						startLightning(p, center, getInstance().base(18, 23));
					});
				});
			});
		}, 3);
	}

	private void startLightning(Player p, Location center, Location l) {
		final Location targetLocation = l.transform(Direction.getRandomNoDiagonal());
		Projectile projectile = Projectile.create(center, targetLocation, Graphic.getOsrsId(1665));
		projectile.setStartHeight(100);
		projectile.setEndHeight(1);
		projectile.setStartDelay(15);
		projectile.setEndDelay(75);
		projectile.setSlope(5);
		projectile.setStartOffset(128);
		projectile.send();

		pulse(() -> {
			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				private int left = 14;
				private Location last = targetLocation;

				@Override
				public void execute(CycleEventContainer container) {
					Graphic.osrs(1666).send(last);
					Location currentLocation = p.getCurrentLocation();
					if (currentLocation.matches(last)) {
						p.dealDamage(new Hit(thisMaxHit, 0, -1));
						container.stop();
						return;
					}

					Direction dir = Direction.getDirection(last, currentLocation);
					last = last.transform(dir);
					if (left-- <= 0) {
						container.stop();
					}
				}

			}, 1);
		}, 2);
	}

	private void fireAttack(Player p) {
		face(null);
		resetAttack();
		fires.clear();
		setRetaliates(false);
		walkToLocation = getInstance().base(20, 25);
		afterWalk = () -> {
			Direction dir1 = Direction.getDirection(getCenterLocation(), p.getCenterLocation());
			Direction dir2;
			if (dir1.isPole()) {
				dir2 = Misc.randomBoolean() ? dir1.clockwise(true) : dir1.counterClockwise(true);
			} else {
				dir2 = dir1.getComposite()[2];
				dir1 = dir1.getComposite()[1];
			}

			Direction combine = Direction.combine(dir1, dir2);
			setAttackTimer(32);

			shootFireWave(p, dir1);
			p.pulse(() -> {
				shootFireWave(p, dir2);
			}, 4);
			p.pulse(() -> {
				Location spot = null;
				switch (combine) {
					case NORTH_WEST:
						spot = getInstance().base(19, 31);
						break;
					case SOUTH_WEST:
						spot = getInstance().base(19, 24);
						break;
					case NORTH_EAST:
						spot = getInstance().base(26, 31);
						break;
					case SOUTH_EAST:
						spot = getInstance().base(26, 24);
						break;
				}

				sendFireAt(spot, 0, p, 0);
				startFireFollow(p);
				
			}, 8);
		};
	}

	private void startFireFollow(Player victim) {
		face(victim);
		CycleEventHandler.getSingleton().addEvent(victim, new CycleEvent() {

			private int left = 14;
			
			@Override
			public void execute(CycleEventContainer container) {
				Location end = victim.getCurrentLocation().copyNew();
				sendFireAt(end, 0, victim, -1);
				if (left-- <= 0) {
					container.stop();
				}
			}
			
		}, 1);
	}

	private void shootFireWave(Player victim, Direction dir) {
		faceLocation(getCenterLocation().transform(dir, 8));
		startAnimation(Animation.osrs(8248));

		int[] coords = coords(dir);
		int sx = coords[0];
		int sy = coords[1];//+3 on this shit after
		
		int ex = coords[2];
		int ey = coords[3];
		
		switch (dir) {
			case EAST: {
				int count = 0;
				for (int x = sx; x <= ex; x++) {
					for (int y = sy; y <= ey; y++) {
						int delay = Math.max(0, (count++ / 4)) * 10;
						sendFireAt(getInstance().base(x, y), delay, victim, count);
					}
				}
				break;
			}
			case WEST: {
				int count = 0;
				for (int x = ex; x >= sx; x--) {
					for (int y = ey; y >= sy; y--) {
						int delay = Math.max(0, (count++ / 4)) * 10;
						sendFireAt(getInstance().base(x, y), delay, victim, count);
					}
				}
				break;
			}
			case NORTH: {
				int count = 0;
				for (int y = sy; y <= ey; y++) {
					for (int x = sx; x <= ex; x++) {
						int delay = Math.max(0, (count++ / 4)) * 10;
						sendFireAt(getInstance().base(x, y), delay, victim, count);
					}
				}
				break;
			}
			case SOUTH: {
				int count = 0;
				for (int y = ey; y >= sy; y--) {
					for (int x = ex; x >= sx; x--) {
						int delay = Math.max(0, (count++ / 4)) * 10;
						sendFireAt(getInstance().base(x, y), delay, victim, count);
					}
				}
				break;
			}
		}
	}

	private void sendFireAt(Location l, int delay, Player victim, int count) {
		if (fires.contains(l)) {
			return;
		}

		fires.add(l);
		int ticks = Math.max(0, delay / 20);
		if (count >= 0 && count <= 4) {
			Projectile proj = Projectile.create(getCenterLocation(), l, Graphic.getOsrsId(1667));//Projectile.create(getCenterLocation(), l, 101667, 40, 36, 0, 20, 5, getSize() << 5);
			victim.pulse(() -> {
				proj.send();
			});
		}

		Graphic.osrs(1668, delay, GraphicType.LOW).send(l);
		victim.pulse(() -> {
			CycleEventHandler.getSingleton().addEvent(victim, new CycleEvent() {

				private int left = 43 - 1;//minus 1 cuz the initial delay tick
				
				@Override
				public void execute(CycleEventContainer container) {
					if (l.matches(victim.getCurrentLocation())) {
						victim.dealDamage(new Hit(thisMaxHit, 0, -1));
					}

					if (left-- <= 0) {
						fires.remove(l);
						container.stop();
					}
				}
				
			}, 1);
		}, ticks - 1);
	}

	private int[] coords(Direction dir) {
		int sx = -1, sy = -1, ex = -1, ey = -1;
		switch (dir) {
			case NORTH:
				sx = 21;
				sy = 31;
				ex = 24;
				ey = 38;
				break;
			case SOUTH:
				sx = 21;
				sy = 17;
				ex = 24;
				ey = 24;
				break;
			case EAST:
				sx = 26;
				sy = 26;
				ex = 33;
				ey = 29;
				break;
			case WEST:
				sx = 12;
				sy = 26;
				ex = 19;
				ey = 29;
				break;
		}
		return new int[]{sx, sy, ex, ey};
	}

	private void acidSpecial(Player p) {
		setAttackTimer(6);
		int attackAnim;
		switch (getPhaseId()) {
			case 3:
				if (mageAttack) {
					attackAnim = magicAnimation;
				} else {
					attackAnim = rangedAnimation;
				}
				break;
			default:
				attackAnim = 8234;
				break;
		}

		startAnimation(Animation.getOsrsAnimId(attackAnim));
		Location end = p.getCurrentLocation().copyNew();
		Projectile.create(getCenterLocation(), end, Graphic.getOsrsId(1644)).send();

		final Location finalEnd = end;
		pulse(() -> {
			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				int ticks = 0;

				@Override
				public void execute(CycleEventContainer container) {
					if (p.getCurrentLocation().matches(finalEnd)) {
						p.dealDamage(new Hit(4, 2, -1));
					}

					ticks++;
					if (ticks >= 8) {
						container.stop();
					}
				}

				@Override
				public void stop() {

				}

			}, 2);

			if (finalEnd.getDistanceInt(p.getCurrentLocation()) <= 1) {
				p.dealDamage(new Hit(Misc.random(7), 2, -1));
			}

			Graphic.create(Graphic.getOsrsId(1655), GraphicType.LOW).send(finalEnd);
		}, 3);

		for (int i = 0; i < 4; i++) {
			end = findLocation(p.getCurrentLocation(), end);
			if (end == null) {
				continue;
			}

			Projectile.create(getCenterLocation(), end, Graphic.getOsrsId(1644)).send();

			final Location finalEnd2 = end;
			pulse(() -> {
				CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

					int ticks = 0;

					@Override
					public void execute(CycleEventContainer container) {
						if (p.getCurrentLocation().matches(finalEnd2)) {
							p.dealDamage(new Hit(4, 2, -1));
						}

						ticks++;
						if (ticks >= 8) {
							container.stop();
						}
					}

					@Override
					public void stop() {

					}

				}, 2);

				if (finalEnd.getDistanceInt(p.getCurrentLocation()) <= 1) {
					p.dealDamage(new Hit(Misc.random(7), 2, -1));
				}

				Graphic.create(Graphic.getOsrsId(1655), GraphicType.LOW).send(finalEnd2);
			}, 4);
		}
	}

	private Location findLocation(Location location, Location last) {
		if (location == null || last == null) {
			return null;
		}

		for (int i = 0; i < 10; i++) {
			Location gen = location.transform(Direction.getRandom(), 1 - Misc.random(2));
			if (gen != null && !gen.matches(location) && !last.matches(gen)
					&& RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}

		return null;
	}

	@Override
	public void onDeath() {
		super.onDeath();

		getInstance().initRespawn();
		actionTimer = 3;
		templateSpawn = getCurrentLocation().copyNew();
	}

	@Override
	public void afterNpcDeath() {
		super.afterNpcDeath();

		// Npc didn't die, got cleared
		if (templateSpawn == null) {
			return;
		}

		NPC npc = NPCHandler.spawnNpc(108622, templateSpawn.getX(), templateSpawn.getY(), templateSpawn.getZ());
		npc.stopAttack = true;
		npc.dontFace = true;
		npc.setCanWalk(false);
		npc.startAnimation(Animation.osrs(8258));
		npc.pulse(() -> {
			npc.nullNPC();
		}, 3);
	}

	@Override
	public void defineConfigs() {
		int[] ids = { 108615, 108616, 108617, 108618, 108619, 108620, 108621 };
		for (int id : ids) {
			NpcStats.define(id, 1100, 17, 210, 210);
			NPCAnimations.define(id, Animation.getOsrsAnimId(8257), -1, -1);
		}
	}

	private Location requiredVent() {
		switch (getPhaseId()) {
			case 0:
				return getInstance().getRedVent();
			case 1:
				return getInstance().getGreenVent();
			case 2:
				return getInstance().getBlueVent();
			default:
				return null;
		}
	}

	public int getPhaseId() {
		switch (getNpcTypeSmart()) {
			case 108617:
			case 108619:
				return 1;
			case 108618:
			case 108620:
				return 2;
			case 108621:
				return 3;
			default:
				return 0;
		}
	}

	public AlchemicalHydraInstance getInstance() {
		return (AlchemicalHydraInstance) getInstanceManager();
	}

}
