package com.soulplay.content.npcs.impl.bosses.wyverns;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

@NpcMeta(ids = {107793})
public final class TalonedWyvern extends CombatNPC {

	public TalonedWyvern(int slot, int npcType) {
		super(slot, npcType);
	}
	
	int KICK_ATTACK = 7651;
	int TAIL_SWING_ATTACK = 7654;
	int OPEN_MOUTH = 7653;	
	
	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttack = true;
		setAggroRadius(10);
		getDistanceRules().setFollowDistance(10);
		walkUpToHit = false;
		isAggressive = true;
	}

	@Override
	public void startAttack(Player p) {
		if (getCurrentLocation()
				.getDistance(p.getCurrentLocation()) <= getSize()
				&& Misc.randomBoolean(2)) {
			meleeSwing(p, 12, Animation.getOsrsAnimId(KICK_ATTACK));
		} else if (Misc.randomBoolean(0)) {
			attackType = 1;
			startAnimation(Animation.getOsrsAnimId(TAIL_SWING_ATTACK));
			projectileSwing(p, Graphic.getOsrsId(500), 10, 3, Graphic
					.create(Graphic.getOsrsId(502), Graphic.GraphicType.HIGH),
					CombatType.RANGED);
		} else {
			attackType = 3;
			// TODO: Proper dragonfire breath method
			startAnimation(Animation.getOsrsAnimId(OPEN_MOUTH));
			projectileSwing(p, Graphic.getOsrsId(1392), 54, 3);

		}
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107793, 27652, -1, 27651);
	}
}
