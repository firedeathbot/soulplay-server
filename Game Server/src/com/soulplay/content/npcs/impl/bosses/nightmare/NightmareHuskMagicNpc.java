package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

@NpcMeta(ids = { 109454 })
public class NightmareHuskMagicNpc extends NightmareHuskNpc {

	public static final int ID = 109454;

	public NightmareHuskMagicNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void startAttack(Player p) {
		startAnimation(Animation.osrs(8565));
		projectileSwing(p, Graphic.getOsrsId(1776), 14, 2, Graphic.osrs(1777), CombatType.MAGIC);
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(ID, 20, 14, 80, 30);
		NPCAnimations.define(ID, Animation.getOsrsAnimId(8566), -1, -1);
	}

}