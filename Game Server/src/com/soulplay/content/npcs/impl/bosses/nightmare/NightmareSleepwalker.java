package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 109446, 109447, 109448, 109449, 109450, 109451 })
public class NightmareSleepwalker extends AbstractNpc {

	public static final int[] IDS = { 109446, 109447, 109448, 109449, 109450, 109451 };

	public NightmareNPC boss;
	public boolean startFollow;
	private boolean stop;

	public NightmareSleepwalker(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareSleepwalker(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		isAggressive = false;
		setRetaliates(false);
		stopAttackDefault = true;
	}

	@Override
	public void defineConfigs() {
		for (int ID : IDS) {
			NpcStats.define(ID, 10, 0, 0, 0);
			NPCAnimations.define(ID, Animation.getOsrsAnimId(8571), -1, -1);
		}
	}

	@Override
	public int dealDamage(Hit hit) {
		setDead(true);
		return super.dealDamage(hit);
	}

	@Override
	public void process() {
		super.process();

		if (stop || boss == null) {
			return;
		}

		if (startFollow) {
			NPCHandler.followNpc(getId(), boss.getId());
		}

		int distance = getCurrentLocation().getDistanceInt(boss.getCenterLocation());
		if (distance <= 3) {
			NightmareNPC.absorbed++;
			stop = true;
			startAnimation(Animation.osrs(8571));
			pulse(() -> nullNPC(), 2);
		}
	}

}
