package com.soulplay.content.npcs.impl.bosses.rfd;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {3495})
public class Karamel extends Boss {
	
	private static final int WHITE_BLAST_GFX_ID = 359;
	
	private static final Graphic WHITE_BLAST_GFX = Graphic.create(WHITE_BLAST_GFX_ID);

	private static final Animation MAGIC_SPEC_ANIM = new Animation(810);
	private static final Animation MAGIC_ANIM = new Animation(1979);
	private static final Animation RANGED_ANIM = new Animation(426);

	public Karamel(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		boolean useFreezeSpecial = Misc.random(15) == 7;
		
		npc.stopAttack = false;
		
		if (useFreezeSpecial) {
			npc.attackType = 2;
			c.applyFreeze(33);
			npc.startAnimation(MAGIC_SPEC_ANIM);
			if (c.withinDistance(npc, 1)) {
				//teleport away
				int x = 1891 + Misc.random(16);
				int y = 5347 + Misc.random(16);
				
				npc.teleport(Location.create(x, y, npc.getZ()), TeleportType.TAB);
			}
			npc.setAttackTimer(5);
			npc.stopAttack = true;
			NpcHitPlayer.dealMagicDamage(npc, c, 20, 1, WHITE_BLAST_GFX);
		} else {
			if (Misc.random(1) == 1) {
				//magic
				npc.forceChat("Semolina-Go!");
				npc.attackType = 2;
				npc.endGfx = 369;//barrage end gfx
				npc.attackAnim = MAGIC_ANIM;
				npc.endGraphicType = GraphicType.LOW;
			} else {
				//ranged
				npc.attackType = 1;
				npc.attackAnim = RANGED_ANIM;
			}
		}
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.walkUpToHit = false;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 100;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (c != null && c.isActive)
			RecipeForDisaster.defeatedBoss(c);
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
