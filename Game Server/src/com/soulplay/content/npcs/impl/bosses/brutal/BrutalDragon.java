package com.soulplay.content.npcs.impl.bosses.brutal;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

public abstract class BrutalDragon extends CombatNPC {

	public BrutalDragon(int slot, int npcType, int maxhit) {
		super(slot, npcType);
		this.maxHit = maxhit;
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize() && Misc.randomBoolean(3)) {
			meleeSwing(p, maxHit, 80);
		} else {
			switch (Misc.random(1)) {
				case 0: {
					startAnimation(Animation.osrs(6722));
					Projectile projectile = new Projectile(Graphic.getOsrsId(88), this, p);
					projectile.setStartDelay(41);
					projectile.setStartHeight(20);
					projectile.setEndHeight(20);
					projectile.setStartOffset(255);
					projectile.setEndDelay(80);
					projectileSwing(p, projectile, maxHit, 3, null, CombatType.MAGIC);
					break;
				}
				case 1: {
					startAnimation(81);
					startGraphic(Graphic.osrs(1, GraphicType.FIFTY));
					p.pulse(() -> {
						int damage = p.getPA().handleDragonFireHit(this, Misc.random(40) + 10);
						p.dealTrueDamage(damage, 0, null, this);
					}, 2);
					break;
				}
			}
		}
	}

}
