package com.soulplay.content.npcs.impl.bosses.nex;

import java.util.Collection;

import com.soulplay.Server;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;

public class NexEvent {

	private static Object object = new Object();
	private static boolean nexEvent = false;
	private static CycleEventContainer startingEvent;

	public static void announceEvent(Player player) {
		Collection<Player> playersInside = Server.getRegionManager().getLocalPlayersByLocationExt(2925, 5203, 0);
		for (Player prisonPlayers : playersInside) {
			if (prisonPlayers == null || !prisonPlayers.isActive) {
				continue;
			}

			if (playerInsideNex(prisonPlayers)) {
				player.sendMessage("Unable to announce nex event, people inside nex.");
				return;
			}
		}

		AnnouncementHandler.announce("Broadcast: The mass Nex event is starting in 15 minutes!");
		startingEvent = CycleEventHandler.getSingleton().addEvent(object, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				startNexEvent();
				container.stop();
			}

		}, 1500);
	}

	private static void startNexEvent() {
		nexEvent = true;
		startingEvent = null;

		NPC nex = null;
		for (NPC n : NPCHandler.npcs) {
			if (n == null || n.isDead()) continue;
			if (n.npcType == 13447 && n.getHeightLevel() == 0) {
				nex = n;
				break;
			}
		}

		if (nex == null) {
			AnnouncementHandler.announce("Broadcast: The mass Nex was unable to start.");
			nexEvent = false;
			return;
		}

		nex.nullNPC();
		AnnouncementHandler.announce("Broadcast: The mass Nex has started!");
	}

	public static void end() {
		nexEvent = false;
		AnnouncementHandler.announce("Broadcast: The mass Nex has ended.");
	}

	public static boolean hasStarted() {
		return nexEvent;
	}

	public static boolean isStarting() {
		return startingEvent != null;
	}

	public static boolean playerInsideNex(Player player) {
		int x = player.getX();
		int y = player.getY();
		return x >= 2911 && y >= 5189 && x <= 2939 && y <= 5218;
	}

}
