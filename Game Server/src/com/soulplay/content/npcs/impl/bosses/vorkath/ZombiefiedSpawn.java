package com.soulplay.content.npcs.impl.bosses.vorkath;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NpcWeakness;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {VorkathSpells.MINION_NPC_ID})
public class ZombiefiedSpawn extends Boss {

	public ZombiefiedSpawn(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		
	}

	@Override
	public void process(NPC npc) {
		npc.setFreezeTimer(-1);//make zombified spawn unable to be frozen.
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.noClip = true;
		npc.getWeakness().add(NpcWeakness.UNDEAD);//temp fix
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 50;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		unfreezePlayer(npc);
	}

	public static final Graphic BLUE_FIRE_GFX = Graphic.create(Graphic.getOsrsId(163), Graphic.GraphicType.HIGH);
	
	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		npc.setAttackTimer(10);
		if (!npc.isDead()) {
			Server.getStillGraphicsManager().stillGraphics(p, npc.getX(), npc.getY(), 100, BLUE_FIRE_GFX.getId(), 0);
			if (npc.getSkills().getLifepoints() > 0) {
				p.dealTrueDamage(22 + npc.getSkills().getLifepoints(), 1, null, npc);
			}
			npc.setDead(true);
			unfreezePlayer(npc);
			npc.removeNpcSafe(true);
		}
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {//shitty way of doing it XD
		if (state.getCurrentHit() > 0 && state.getSpellsData() != null && state.getSpellsData() == SpellsData.CRUMBLE_UNDEAD) {
			npc.setDead(true);
			npc.getSkills().setLifepoints(0);
			state.setCurrentHit(npc.getSkills().getLifepoints());
		}
		
	}
	
	private static void unfreezePlayer(NPC npc) {
		
		Player p = PlayerHandler.players[npc.spawnedBy];

		if (p != null && p.isActive) {
			CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {

				@Override
				public void stop() {
				}

				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					p.sendMessage("You become unfozen as the spawn dies.");
					p.resetFreeze();
				}
			}, 4);
		}
	}

}
