package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 109443, 109444, 109445 })
public class NightmareTotemNE extends NightmareTotem {

	public static final int REGULAR = 109443;
	public static final int CHARGE = 109444;
	public static final int FULL_CHARGE = 109445;

	public NightmareTotemNE(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareTotemNE(slot, npcType);
	}

	@Override
	public int getFullChargeNpcId() {
		return FULL_CHARGE;
	}

	@Override
	public int getChargeNpcId() {
		return CHARGE;
	}

	@Override
	public String getChargedMessage() {
		return "The north east totem is fully charged.";
	}

}
