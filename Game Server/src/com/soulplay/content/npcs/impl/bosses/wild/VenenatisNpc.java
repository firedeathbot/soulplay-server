package com.soulplay.content.npcs.impl.bosses.wild;

import com.soulplay.Server;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.ProtectionPrayer;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.GraphicCollection;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4173})
public class VenenatisNpc extends AbstractNpc {
	
	private static final Animation MAGIC_ATTACK_ANIM = new Animation(Animation.getOsrsAnimId(5322));
	
	private boolean usedPrayerDrain = false; // PI cheaphax shit XD
	private int prevAttackType = -1; //another cheaphax

	public VenenatisNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new VenenatisNpc(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walkUpToHit = false;
		setAggroRadius(6);
		setRetargetTick(8);
	}

	@Override
	public boolean loadedSpell(Player p) {
		//if player is using protect prayer, then walk up to hit the player to hopefully switch to melee attacks
		if (CombatPrayer.usingProtectPrayer(p,  ProtectionPrayer.PROTECT_MAGIC.ordinal())) {
			walkUpToHit = true;
		} else {
			walkUpToHit = false;
		}
		
		stopAttack = false;
		
		int distance = Misc.distanceToPoint(p.getX(), p.getY(), getXtoPlayerLoc(p.getX()), getYtoPlayerLoc(p.getY()));
		
		if (prevAttackType != -1) {
			attackType = prevAttackType;
			prevAttackType = -1;
		}
		
		if (distance <= 2) {
			if (Misc.randomBoolean(5)) {
				attackType = attackType == 2 ? 0 : 2;
			}
		} else {
			attackType = 2;
		}
		
		usedPrayerDrain = false;
		
		if (Misc.randomBoolean(10)) {
			//prayer drain spell
			usedPrayerDrain = true;
			prevAttackType = attackType;
			attackType = 2;
		} else {
			
			if (attackType == 2) {

				if (walkUpToHit && distance > 2) {
					//attackType = 0;
					return true;
				}
				
				stopAttack = true;

				attackAnim = null;

				int hitCount = 0;

				Location start = Projectile.getLocationOffset(getCurrentLocation(), p.getCurrentLocation(), getSize());

				for (Player others : Server.getRegionManager().getLocalPlayers(this)) {

					if (!RegionClip.rayTraceBlocked(getXtoPlayerLoc(others.getX()), getYtoPlayerLoc(others.getY()), others.getX(), others.getY(), getZ(), true, getDynamicRegionClip())) {
						if (others.getCurrentLocation().isWithinDistance(start, 8)) {
							Projectile proj = new Projectile(Graphic.getOsrsId(165), start, others.getCurrentLocation());
							proj.setLockon(others.getProjectileLockon());
							proj.setStartDelay(20);
							proj.setEndDelay(60);

							GlobalPackets.createProjectile(proj);
							
							NpcHitPlayer.dealMagicDamage(this, others, Misc.random(50), 3, null);

							hitCount++;
						}
					}

					if (hitCount > 0) {
						startAnimation(MAGIC_ATTACK_ANIM);
					}
				}
			}
			
		}
		
		return true;
	}
	
	@Override
	public void startAttack(Player p) {

		if (Misc.randomBoolean(16)) {
			//stun spell
			startAnimation(MAGIC_ATTACK_ANIM);
			p.lockMovement(13);
			p.startAnimation(424);
			p.startGraphic(GraphicCollection.STUN_GFX);
			p.dealTrueDamage(Misc.random(50), 3, null, this);
			p.sendMessage("Venenatis hurls her web at you, sticking you to the ground.");
		}
		
		if (usedPrayerDrain) {
			startAnimation(MAGIC_ATTACK_ANIM);
			p.startGraphic(PRAYER_HIT_GFX);
			endGfxDelay = 120;
			projectileId = 171;
			p.sendMessage("Your prayer was drained!");
			p.getSkills().decrementPrayerPoints((int)(p.getSkills().getLevel(Skills.PRAYER) * 0.1));//10% drain of total level
		}
		
	}
	
	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	public static final Graphic PRAYER_HIT_GFX = Graphic.create(Graphic.getOsrsId(172), GraphicType.HIGH);
	
	
}
