package com.soulplay.content.npcs.impl.bosses.rfd;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NpcWeakness;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {3497})
public class GelatinnothMother extends Boss {

	private static final int[] FORMS = { 3497/*air*/, 3499/*water*/, 3498/*melee*/, 3502/*earth*/, 3500/*fire*/, 3501/*ranged*/ };
	
	private static final int TRANSFORM_DELAY = 33;
	
	private static final int VAR_TRANS = 0;
	
	private static final int PROJECTILE_ID = 599;
	
	public GelatinnothMother(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.attackType = Misc.newRandom(1, 2);
		
		npc.projectileId = PROJECTILE_ID;
		
//		switch (npc.attackType) {
//		case 1: // ranged
//			
//			break;
//			
//		case 2: //magic
//			
//			break;
//		}
	}

	@Override
	public void process(NPC npc) {
		
		if (npc.actionTimer2 < 1) {
			npc.actionTimer2 = Misc.newRandom(25, TRANSFORM_DELAY); //15 to 20 seconds random
			npc.dungVariables[VAR_TRANS] = ((int)npc.dungVariables[VAR_TRANS] + 1) % FORMS.length;
			npc.transform(FORMS[(int)npc.dungVariables[VAR_TRANS]]);
			
			npc.getWeakness().clear();
			
			switch(npc.transformId) {
			case 3497: // air
				npc.getWeakness().add(NpcWeakness.AIR_SPELLS);
				break;
				
			case 3498: // melee
				npc.getWeakness().add(NpcWeakness.MELEE);
				break;
				
			case 3499: // water
				npc.getWeakness().add(NpcWeakness.WATER_SPELLS);
				break;
				
			case 3500: //fire
				npc.getWeakness().add(NpcWeakness.FIRE_SPELLS);
				break;
				
			case 3501: //ranged
				npc.getWeakness().add(NpcWeakness.RANGED);
				break;
				
			case 3502: // earth
				npc.getWeakness().add(NpcWeakness.EARTH_SPELLS);
				break;
			}
			
		}
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.actionTimer2 = TRANSFORM_DELAY;
		npc.dungVariables[VAR_TRANS] = new Integer(0);
		npc.transformId = FORMS[0];//default stage
		npc.getWeakness().add(NpcWeakness.AIR_SPELLS);//default weakness
		npc.walkUpToHit = false;
		npc.transformId = 3497;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 100;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (c != null && c.isActive)
			RecipeForDisaster.defeatedBoss(c);
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
		//cheaphaxing this for now till the new accuracy system is released :D
		switch(npc.transformId) {
		case 3497: // air
			if (state.getSpellsData() == null || state.getSpellsData().getNpcWeakness() != NpcWeakness.AIR_SPELLS)
				state.setCurrentHit(0);
			break;
			
		case 3498: // melee
			if (state.getCombatStyle() != CombatType.MELEE)
				state.setCurrentHit(0);
			break;
			
		case 3499: // water
			if (state.getSpellsData() == null || state.getSpellsData().getNpcWeakness() != NpcWeakness.WATER_SPELLS)
				state.setCurrentHit(0);
			break;
			
		case 3500: //fire
			if (state.getSpellsData() == null || state.getSpellsData().getNpcWeakness() != NpcWeakness.FIRE_SPELLS)
				state.setCurrentHit(0);
			break;
			
		case 3501: //ranged
			if (state.getCombatStyle() != CombatType.RANGED)
				state.setCurrentHit(0);
			break;
			
		case 3502: // earth
			if (state.getSpellsData() == null || state.getSpellsData().getNpcWeakness() != NpcWeakness.EARTH_SPELLS)
				state.setCurrentHit(0);
			break;
			
		}
	}

}
