package com.soulplay.content.npcs.impl.bosses.rfd;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {3496})
public class Dessourt extends Boss {
	
	//596 yellow projectile
	private static final int PROJECTILE_ID = 596;
	//private static final Animation ATTACK_ANIM = new Animation(3507);
	
	private static final String MAGIC_MSG = "Hssssssssssss";

	public Dessourt(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		if (Misc.random(1) == 1) {
			npc.attackType = 2;
			npc.projectileId = PROJECTILE_ID;
			npc.forceChat(MAGIC_MSG);
		} else {
			npc.attackType = 0;
		}
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.walkUpToHit = true;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 100;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		npc.actionTimer = 12;
		if (c != null && c.isActive)
			RecipeForDisaster.defeatedBoss(c);
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
