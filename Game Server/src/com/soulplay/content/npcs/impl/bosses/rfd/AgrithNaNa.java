package com.soulplay.content.npcs.impl.bosses.rfd;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Graphic.GraphicType;

@BossMeta(ids = {3493})
public class AgrithNaNa extends Boss {
	
	private static final Animation MELEE_ANIM = new Animation(3501);
	private static final Animation MAGIC_ANIM = new Animation(3502);

	public AgrithNaNa(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		boolean withinMeleeDistance = c.getPA().withinDistanceToNpc(npc, 1);
		
		if (withinMeleeDistance) {
			//use melee
			npc.attackType = 0;
			npc.attackAnim = MELEE_ANIM;
		} else {
			//use magic
			npc.attackType = 2;
			npc.attackAnim = MAGIC_ANIM;
			npc.projectileId = 2729;
			npc.endGfx = 2737;
			npc.endGraphicType = GraphicType.HIGH;
			npc.endGfxDelay = 30;
		}
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.walkUpToHit = false;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 100;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		npc.actionTimer = 6;
		if (c != null && c.isActive)
			RecipeForDisaster.defeatedBoss(c);
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
