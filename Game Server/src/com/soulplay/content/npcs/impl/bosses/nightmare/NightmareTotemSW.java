package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 109434, 109435, 109436 })
public class NightmareTotemSW extends NightmareTotem {

	public static final int REGULAR = 109434;
	public static final int CHARGE = 109435;
	public static final int FULL_CHARGE = 109436;

	public NightmareTotemSW(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareTotemSW(slot, npcType);
	}

	@Override
	public int getFullChargeNpcId() {
		return FULL_CHARGE;
	}

	@Override
	public int getChargeNpcId() {
		return CHARGE;
	}

	@Override
	public String getChargedMessage() {
		return "The south west totem is fully charged.";
	}

}
