package com.soulplay.content.npcs.impl.bosses.livingrocks;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;

@NpcMeta(ids = { 8834 })
public final class LivingRockPatriarch extends LivingRockNpc {

	private static final int MELEE_ATTACK_ANIM = 12205;
	private static final int DEATH_ANIM = 12206;
	private static final int TRANSFORM_ID = 8839;

	public LivingRockPatriarch(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		getSkills().setStaticLevel(Skills.HITPOINTS, 400);
		getSkills().setStaticLevel(Skills.ATTACK, 230);
		getSkills().setStaticLevel(Skills.STRENGTH, 180);
		getSkills().setStaticLevel(Skills.DEFENSE, 200);
		getSkills().setStaticLevel(Skills.MAGIC, 1);
		getSkills().setStaticLevel(Skills.RANGED, 1);

		getCombatStats().setAttackStrength(130);
		getCombatStats().setStrengthBonus(50);

		getCombatStats().setStabDefBonus(50);
		getCombatStats().setSlashDefBonus(50);
		getCombatStats().setCrushDefBonus(10);
		getCombatStats().setMagicDefBonus(150);
		getCombatStats().setRangedDefBonus(300);

		stopAttackDefault = true;
		isAggressive = false;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setFollowDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		meleeSwing(p, 16, MELEE_ATTACK_ANIM);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(8834, -1, -1, MELEE_ATTACK_ANIM);
	}

	@Override
	public int getDeathAnim() {
		return DEATH_ANIM;
	}

	@Override
	public int getTransformId() {
		return TRANSFORM_ID;
	}
}
