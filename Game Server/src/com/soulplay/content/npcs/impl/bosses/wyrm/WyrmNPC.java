package com.soulplay.content.npcs.impl.bosses.wyrm;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108611, 108610 })
public final class WyrmNPC extends CombatNPC {

	private static final int MAGIC_ATTACK_ANIM = 8271;
	private static final int MELEE_ATTACK_ANIM = 8270;

	private boolean transformed = true;

	public WyrmNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		dontFace = true;
		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = false;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setMeleeDistance(8);
	}

	@Override
	public int dealDamage(Hit hit) {
		if (transformed) {
			transform(108611);
			startAnimation(Animation.getOsrsAnimId(8268));
			setAttackTimer(2);
			transformed = false;
			dontFace = false;
		}

		return super.dealDamage(hit);
	}

	@Override
	public void startAttack(Player p) {
		if (transformed) {
			return;
		}

		if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize() && Misc.randomBoolean(1)) {
			meleeSwing(p, 13, Animation.getOsrsAnimId(MELEE_ATTACK_ANIM));
		} else {
			startAnimation(Animation.getOsrsAnimId(MAGIC_ATTACK_ANIM));
			projectileSwing(p, Graphic.getOsrsId(1634), 13, 4,
					Graphic.create(Graphic.getOsrsId(1635)), CombatType.MAGIC);
		}
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(108611, 130, 13, 85, 80);
		NPCAnimations.define(108611, 28272, -1, 28270);
		NpcStats.define(108610, 130, 13, 85, 80);
		NPCAnimations.define(108610, 28272, -1, 28270);
	}
}
