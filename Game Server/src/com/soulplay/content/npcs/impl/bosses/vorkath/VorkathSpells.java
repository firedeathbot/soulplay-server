package com.soulplay.content.npcs.impl.bosses.vorkath;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class VorkathSpells {

	public static final int MINION_NPC_ID = 8981;
	public static final int ACID_POOL_OBJECT_ID = 32000;
	public static final int ACID_POOL_PROJECTILE = Graphic.getOsrsId(1483);
	public static final int SPAWN_MINION_PROJECTILE = Graphic.getOsrsId(1484);
	private static final int PROJECTILE_START_DELAY_CLIENT = Misc.serverToClientTick(1);
	private static final int ACID_END_DELAY_SERVER = 3;
	private static final int ACID_END_DELAY_CLIENT = Misc.serverToClientTick(ACID_END_DELAY_SERVER);
	private static final int SPAWN_MINION_DELAY_SERVER = 4;
	private static final int SPAWN_MINION_DELAY_CLIENT = Misc.serverToClientTick(SPAWN_MINION_DELAY_SERVER);
	private static final int DRAGON_BREATH_HIT_DELAY = 4;
	private static final int DRAGON_BREATH_END_DELAY = Misc.serverToClientTick(DRAGON_BREATH_HIT_DELAY-1);
	private static final int DRAGON_BOMB_HIT_DELAY = SPAWN_MINION_DELAY_SERVER+2;
	private static final int FIRE_BOMB_END_DELAY_CLIENT = Misc.serverToClientTick(DRAGON_BOMB_HIT_DELAY-1);
	private static final int RAPID_FIRE_DELAY_SERVER = 1;
	private static final int RAPID_FIRE_DELAY_CLIENT = Misc.serverToClientTick(RAPID_FIRE_DELAY_SERVER);
	//FREEZES PLAYER FOR 20 seconds until little spawn is dead. https://i.imgur.com/ixBkdVa.png
	public static final Animation SHOOT_BREATH_ANIM = new Animation(Animation.getOsrsAnimId(7952));
	
	public static final Animation ONE_SHOT_UP_ANIM = new Animation(Animation.getOsrsAnimId(7960)); // SAME AS 7952
	
	public static final Animation POISON_POOL_RAPID_FIRE_ANIM = new Animation(Animation.getOsrsAnimId(7957));
	
	public static final int VENOM_PROJECTILE_ID = Graphic.getOsrsId(1472);
	
	public static final int FIRE_BOMB_ID = Graphic.getOsrsId(1481);
	
	public static final int RAPID_FIRE_GFX_ID = Graphic.getOsrsId(1482);
	
	public static final int FIRE_BLAST_END = Graphic.getOsrsId(131);
	
	public static final int FREEZE_PROJECTILE_ID = Graphic.getOsrsId(395);
	
	public static final Location CENTER = Location.create(8672, 4065);
	
	public static final int SPELL_SLOPE = 10;
	
	public static void shootPool(Player player, NPC n) {
		
		n.startAnimation(POISON_POOL_RAPID_FIRE_ANIM);
		
		final List<Location> acidList = new ArrayList<>();
		
		final Location start = n.getCurrentLocation().transform(3, 3);
		
		Location pLoc = player.getCurrentLocation().copyNew();
		
		singleAcidPool(player, start, pLoc);
		
		acidList.add(pLoc);
		
		for (int p = 0; p < 2; p++) {
			for (int i = 0; i < 4; i++) {
				for (int k = 0, len = Misc.newRandom(7, 9); k < len; k++) {

					
					final Location end = p == 0 ? LocData.generateMid(player.getZ(), i) : LocData.generateCorner(player.getZ(), i);

//					if (RegionClip.getClipping(end.getX(), end.getY(), end.getZ()) != 0) { //to stop from spawning on very corner
//						continue;
//					}
					
					singleAcidPool(player, start, end);
					acidList.add(end);
					
				}
			}
		}
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				
				startRapidFireBreath(player, n, acidList);
			}
		}, 3);
	}
	
	private static void singleAcidPool(Player player, Location start, Location end) {
		
		Projectile acid = new Projectile(ACID_POOL_PROJECTILE, start, end);
		acid.setEndHeight(0);
		acid.setSlope(45);
		acid.setStartDelay(PROJECTILE_START_DELAY_CLIENT);
		acid.setEndDelay(ACID_END_DELAY_CLIENT);
		
		GlobalPackets.createProjectileForPlayerOnly(player, acid);
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				
				player.getPacketSender().addObjectPacket(ACID_POOL_OBJECT_ID, end, 10, Misc.newRandom(3));
				
			}
		}, ACID_END_DELAY_SERVER+1);
		
	}
	
	public static void freezePlayerSpawnMinion(final Player p, final NPC n) {
		n.startAnimation(SHOOT_BREATH_ANIM);
		
		final Location start = getStartProjectileLocation(n, p);
		
		Projectile proj = new Projectile(FREEZE_PROJECTILE_ID, start, p.getCurrentLocation());
		proj.setLockon(p.getProjectileLockon());
		proj.setStartDelay(PROJECTILE_START_DELAY_CLIENT);
		proj.setEndDelay(SPAWN_MINION_DELAY_CLIENT);
		proj.setStartHeight(30);
		proj.setSlope(SPELL_SLOPE);
		
		GlobalPackets.createProjectileForPlayerOnly(p, proj);
		
		CycleEventHandler.getSingleton().addEvent(p, true, new CycleEvent() {
			int timer = 0;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (p.isDead()) {
					container.stop();
					return;
				}

				if (timer == SPAWN_MINION_DELAY_SERVER) {
					p.applyFreeze(33);
					p.startGraphic(SpellsData.ICE_BARRAGE.getEndGfx());
					n.dungVariables[VorkathBoss.FROZEN_PLAYER] = true;
				}
				
				if (p.getFreezeTimer() > 0) {
					container.stop();
					spawnMinion(p, n);
				}
				timer++;
			}
		}, 1);
	}
	
	private static void spawnMinion(Player player, NPC boss) {
		final Location minLoc = createMinionLoc(player, CENTER);
		
		boss.startAnimation(VorkathBoss.BREATH_UPWARDS);
		
		Projectile proj = new Projectile(SPAWN_MINION_PROJECTILE, Projectile.getLocationOffset(boss.getCurrentLocation(), /*minLoc*/player.getCurrentLocation(), boss.getSize()), minLoc);
		
		proj.setEndHeight(0);
		proj.setSlope(45);
		proj.setStartDelay(PROJECTILE_START_DELAY_CLIENT);
		proj.setEndDelay(SPAWN_MINION_DELAY_CLIENT);
		
		GlobalPackets.createProjectileForPlayerOnly(player, proj);
		
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				NPC minion = NPCHandler.spawnNpc(player, MINION_NPC_ID, minLoc.getX(), minLoc.getY(), minLoc.getZ(), 0, true, false);
//				minion.setHomeOwnerId(player.mySQLIndex);
				minion.setAttackable(true);
				boss.dungVariables[VorkathBoss.MINION_INDEX] = minion;
				minion.setAliveTick(30);
			}
		}, SPAWN_MINION_DELAY_SERVER);
	}
	
	private static Location createMinionLoc(Player player, Location start) {
		
		int sideDist = Misc.newRandom(6, 8);
		
		
		int x1 = player.getX();
		int y1 = player.getY();
		
		int x2 = start.getX();
		int y2 = start.getY();
		
		int x3;
		int y3;
		
		double angle = Misc.newRandom(1) == 0 ? getAngle(x1, y1, x2, y2) : getAngle(x2, y2, x1, y1);
		
		x3 = (int) (x2 + Math.sin(angle) * sideDist);
		y3 = (int) (y2 + Math.cos(angle) * sideDist);
		
		

		return Location.create(x3, y3, player.getZ());
		
	}
	
	private static double getAngle(double x1, double y1, double x2, double y2) {
		double diffX = x1 - x2;
		double diffY = y2 - y1;
		
		return Math.atan2(diffY, diffX);
	}
	
	public static void dragonFire(final Player p, final NPC n, final DragonBreathEnum spell) {
		n.startAnimation(SHOOT_BREATH_ANIM);
		
		final int damage = p.getPA().handleDragonFireHit(n, 20+Misc.newRandom(52));
		
		final Location start = getStartProjectileLocation(n, p);
		
		Projectile proj = new Projectile(spell.getProjId(), start, p.getCurrentLocation());
		proj.setLockon(p.getProjectileLockon());
		proj.setStartDelay(PROJECTILE_START_DELAY_CLIENT);
		proj.setEndDelay(DRAGON_BREATH_END_DELAY);
		proj.setSlope(SPELL_SLOPE);
		proj.setStartHeight(30);
		
//		GlobalPackets.createProjectileForPlayerOnly(p, proj);
		GlobalPackets.createProjectile(proj);
		
		CycleEventHandler.getSingleton().addEvent(p, true, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				p.startGraphic(spell.getHitGraphic());
				if (spell == DragonBreathEnum.VENOM) {
					if (Misc.newRandom(99) < 25) {
						p.getDotManager().applyVenom();
					}
				}
				if (spell == DragonBreathEnum.PINK) {
					CombatPrayer.resetPrayers(p);
					p.getCombat().appendHit(p, p.getPA().antiFire() < 2  ? damage : 0, 0, 2, false, null);
				} else {
					p.getCombat().appendHit(p, damage, 0, 2, false, null);
				}
				p.getPA().chargeDfs();
			}
		}, DRAGON_BREATH_HIT_DELAY);
	}
	
	public static void fireBomb(final Player p, final NPC n) {
		
		final Location start = getStartProjectileLocation(n, p);
		
		final Location end = p.getCurrentLocation().copyNew();
		
		final Projectile proj = new Projectile(FIRE_BOMB_ID, start, end);
		proj.setEndHeight(30);
		proj.setSlope(45);
		proj.setStartDelay(PROJECTILE_START_DELAY_CLIENT);
		proj.setEndDelay(FIRE_BOMB_END_DELAY_CLIENT);
		proj.setStartHeight(64);
		
		GlobalPackets.createProjectileForPlayerOnly(p, proj);

		Server.getStillGraphicsManager().stillGraphics(p, end.getX(), end.getY(), 100, DragonBreathEnum.NORMAL.getHitGraphic().getId(), FIRE_BOMB_END_DELAY_CLIENT);
		
		final int damage = Misc.newRandom(115, 121);
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				int distance = Misc.distanceToPoint(p.getX(), p.getY(), end.getX(), end.getY());
				
				switch (distance) {
				case 0:
					p.dealTrueDamage(damage, 0, null, n);
					break;
				case 1:
					p.dealTrueDamage(damage/2, 0, null, n);
					break;
				}
				
			}
		}, DRAGON_BOMB_HIT_DELAY);
		
	}
	
	private static void startRapidFireBreath(final Player p, final NPC n, List<Location> acidList) {
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			int tick = 0;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (p.isDead() || n.isDead() || tick > 25 || p.getZ() != n.getZ()) {
					container.stop();
					clearAcidPools(p, acidList);
					n.dungVariables[VorkathBoss.ACID_STAGE_INDEX] = false;
					n.setAttackTimer(n.getCombatStats().getAttackSpeed());
					return;
				}
				
				if (tick < 26) {
					if (standingInAcidPool(p, acidList)) {
						int damage = Misc.newRandom(5, 10);
						p.dealTrueDamage(damage, 0, null, n);
						n.healNpc(damage);
					}
				}

				if (tick > 0)
					singleRapidFireBreath(p, n);
				
				tick++;
			}
		}, 1);
	}
	
	private static boolean standingInAcidPool(final Player p, List<Location> acidList) {
		for (int i = 0, len = acidList.size(); i < len; i++) {
			Location acid = acidList.get(i);
			if (Location.equals(p.getCurrentLocation(), acid)) {
				return true;
			}
		}
		return false;
	}
	
	private static void clearAcidPools(final Player p, List<Location> acidList) {
		if (!p.disconnected) {
			for (int i = 0, len = acidList.size(); i < len; i++) {
				Location acid = acidList.get(i);
				p.getPacketSender().addObjectPacket(-1, acid, 10, 0);
			}
		}
		acidList.clear();
	}
	
	private static void singleRapidFireBreath(final Player p, final NPC n) {
					
		final Location end = p.getCurrentLocation().copyNew();

		Projectile proj = new Projectile(RAPID_FIRE_GFX_ID, getStartProjectileLocation(n, p), end);
		proj.setStartHeight(30);
		proj.setStartDelay(0);
		proj.setEndDelay(RAPID_FIRE_DELAY_CLIENT);
		proj.setSlope(0);

		GlobalPackets.createProjectileForPlayerOnly(p, proj);
		
		Server.getStillGraphicsManager().stillGraphics(p, end.getX(), end.getY(), 100, FIRE_BLAST_END, RAPID_FIRE_DELAY_CLIENT);

		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (Location.equals(p.getCurrentLocation(), end)) {
					p.dealTrueDamage(Misc.newRandom(25, 40), 0, null, n);
				}

			}
		}, RAPID_FIRE_DELAY_SERVER);
		
	}
	
	private static Location getStartProjectileLocation(final NPC n, final Player p) {
		return Projectile.getLocationOffset(n.getCurrentLocation(), p.getCurrentLocation(), n.getSize());
	}
	
}
