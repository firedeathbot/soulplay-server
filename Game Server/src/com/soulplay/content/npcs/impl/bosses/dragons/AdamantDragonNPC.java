package com.soulplay.content.npcs.impl.bosses.dragons;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108030 })
public final class AdamantDragonNPC extends CombatNPC {

	public AdamantDragonNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize() && Misc.randomBoolean(3)) {
			meleeSwing(p, 29, 80);
		} else if (Misc.randomBoolean(6)) { // special shit
			if (Misc.randomBoolean() && (int) (getSkills().getLifepoints() * 0.10) > 0 && getSkills().getLifepoints() - ((int) (getSkills().getLifepoints() * 0.10)) > 0) {
				startAnimation(81);
				Projectile projectile = new Projectile(Graphic.getOsrsId(27), this, p);
				projectile.setStartDelay(41);
				projectile.setStartHeight(38);
				projectile.setEndHeight(36);
				projectile.setStartOffset(255);
				projectile.setEndDelay(63);
				projectile.send();
				pulse(() -> {
					if (p.isDead() || isDead()) {
						return;
					}

					int victimDamage = (int) (p.getSkills().getLifepoints() * 0.20);
					int attackerDamage = (int) (getSkills().getLifepoints() * 0.10);

					if (victimDamage > p.getSkills().getLifepoints())
						victimDamage = p.getSkills().getLifepoints();

					dealDamage(new Hit(attackerDamage, 0, -1));
					p.dealTrueDamage(victimDamage, 1, null, this);
				}, 2);
			} else {
				startAnimation(81);
				Location centerLocation = p.getCenterLocation().copyNew();
				Projectile projectile = new Projectile(Graphic.getOsrsId(1486), getCenterLocation(), centerLocation);
				projectile.setStartDelay(40);
				projectile.setStartHeight(10);
				projectile.setEndHeight(31);
				projectile.setStartOffset(127);
				projectile.setEndDelay(80);
				projectile.send();
				pulse(() -> {
					Graphic.osrs(1487).send(centerLocation);
					int distance = centerLocation.getDistanceInt(p.getCenterLocation());
					if (distance < 3) {
						int maxHit = !p.getPoisonImmuneTimer().complete() ? Misc.random(4, 8) : Misc.random(14, 25);
						p.dealDamage(new Hit(maxHit, 2, -1));
					}
				}, 4);
			}
		} else {
			switch (Misc.random(2)) {
				case 0: {
					startAnimation(81);
					Projectile projectile = new Projectile(Graphic.getOsrsId(165), this, p);
					projectile.setStartDelay(41);
					projectile.setStartHeight(20);
					projectile.setEndHeight(20);
					projectile.setStartOffset(255);
					projectile.setEndDelay(80);
					projectileSwing(p, projectile, 29, 3, Graphic.osrs(166, GraphicType.HIGH), CombatType.MAGIC);
					break;
				}
				case 1: {
					startAnimation(81);
					Projectile projectile = new Projectile(13, this, p);
					projectile.setStartDelay(41);
					projectile.setStartHeight(20);
					projectile.setEndHeight(20);
					projectile.setStartOffset(255);
					projectile.setEndDelay(80);
					projectileSwing(p, projectile, 29, 3, null, CombatType.RANGED);
					break;
				}
				case 2: {
					startAnimation(81);
					startGraphic(Graphic.osrs(1, GraphicType.FIFTY));
					p.pulse(() -> {
						int damage = p.getPA().handleDragonFireHit(this, Misc.random(40) + 10);
						p.dealTrueDamage(damage, 0, null, this);
					}, 2);
					break;
				}
			}
		}
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(108030, 28, 26, 80);
	}

}
