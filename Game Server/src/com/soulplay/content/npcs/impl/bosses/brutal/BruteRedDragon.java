package com.soulplay.content.npcs.impl.bosses.brutal;

import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 107274 })
public class BruteRedDragon extends BrutalDragon {

	public BruteRedDragon(int slot, int npcType) {
		super(slot, npcType, 26);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(107274, 28, 26, 80);
	}

}
