package com.soulplay.content.npcs.impl.bosses.nex;

import java.util.ArrayList;

import com.soulplay.Server;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class NexSpells {
	
	private static Graphic BLOOD_BLITZ_END_GFX = Graphic.create(376);
	private static Graphic ICE_BLITZ_END_GFX = Graphic.create(369);
	private static Graphic SHADOW_BLITZ_END_GFX = Graphic.create(381);
	private static Graphic SMOKE_BLITZ_END_GFX = Graphic.create(385, GraphicType.HIGH);
	
	public static void bloodBlitzMulti(NPC npc) {
		npc.startAnimation(6987);


//		int nX = npc.getX() + 1;
//		int nY = npc.getY() + 1;
//		int nZ = npc.getHeightLevel();

		for (Player p : getNearbyPlayers(npc)) {
			if (p == null || p.disconnected || p.isDead() || !p.isInNexRoom()) {
				continue;
			}
			int dist = Misc.distanceToPoint(npc.getX(), npc.getY(), p.getX(),
					p.getY());
			if (dist <= 10) { // magic distance
				Client c = (Client) p;

				int hitDelay = 2; // adjust this to work proper delay
				if (dist > 2) {
					hitDelay += 1;
				}

				if (dist > 4) {
					hitDelay += 1;
				}
				if (dist > 8) {
					hitDelay += 1;
				}

				// int offX = (nY - c.getY()) * -1;
				// int offY = (nX - c.getX()) * -1;
				// c.getPA().createPlayersProjectile(nX, nY, offX, offY, 50,
				// c.getCombat().getProjectileSpeed(dist)/*getProjectileSpeed(i)*/,
				// 384/*npcs[i].projectileId*/, 35, 20, -c.getId() - 1, 50);
				npc.setHP(npc.getSkills().getLifepoints() + (NpcHitPlayer.dealMagicDamage(npc, c,
						Misc.random(30), hitDelay, BLOOD_BLITZ_END_GFX) / 2));
				
				Projectile proj = new Projectile(374, Location.create(npc.getX(),
						npc.getY(), npc.getHeightLevel()), c.getCurrentLocation());
				proj.setStartHeight(41);
				proj.setEndHeight(35);
				proj.setStartDelay(50);
				proj.setLockon(c.getProjectileLockon());
				proj.setEndDelay(5 * dist + 55);
				GlobalPackets.createProjectile(proj);

//				int pX = c.getX();
//				int pY = c.getY();
//				int offX = (nY - pY) * -1;
//				int offY = (nX - pX) * -1;
//				GlobalPackets.createPlayersProjectile(nX, nY, nZ, offX, offY,
//						50, (5 * dist + 55), 374, 41, 35, -c.getId() - 1, 50);

			}
		}
	}

	public static ArrayList<Player> getNearbyPlayers(NPC n) {
		ArrayList<Player> temp = new ArrayList<>();
		for (Player p : Server.getRegionManager().getLocalPlayers(n)) {
			if (p == null || p.disconnected || p.isDead() || !p.isInNexRoom()) {
				continue;
			}
			temp.add(p);
		}
		return temp;
	}
	
	public static final Graphic GLACIAL_PRISON_GFX = Graphic.create(363);

	public static void glacierPrison(final NPC npc, final int targetX,
			final int targetY, final int size) {

		final ArrayList<Player> temp = getNearbyPlayers(npc);

		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				int x = targetX - (size / 2);
				int y = targetY - (size / 2);

				for (int i1 = 0; i1 < 4; i1++) {
					for (int jk = 0; jk < size; jk++) {
						for (Player p : temp) {
							if (p.getX() == x && p.getY() == y) {
								p.dealTrueDamage(10 + Misc.random(15), 2, GLACIAL_PRISON_GFX, npc);
								p.applyFreeze(17);
							}
						}
						int randomObj = 52286 + Misc.random(2);
						int randomTicks = 10 + Misc.random(10);
						if (i1 == 0) {
							if (!RegionClip.canStandOnSpot(x, y + 1, 0)) {
								continue;
							}
							ObjectManager.addObject(new RSObject(randomObj, x, y++, 0, 0, 10, -1,
									randomTicks));
						} else if (i1 == 1) {
							if (!RegionClip.canStandOnSpot(x + 1, y, 0)) {
								continue;
							}
							ObjectManager.addObject(new RSObject(randomObj, x++, y, 0, 0, 10, -1,
									randomTicks));
						} else if (i1 == 2) {
							if (!RegionClip.canStandOnSpot(x, y - 1, 0)) {
								continue;
							}
							ObjectManager.addObject(new RSObject(randomObj, x, y--, 0, 0, 10, -1,
									randomTicks));
						} else if (i1 == 3) {
							if (!RegionClip.canStandOnSpot(x - 1, y, 0)) {
								continue;
							}
							ObjectManager.addObject(new RSObject(randomObj, x--, y, 0, 0, 10, -1,
									randomTicks));
						}
					}
				}

				container.stop();

			}

			@Override
			public void stop() {
				// TODO Auto-generated method stub

			}
		}, 3);

	}

	public static void iceBlitzMulti(NPC npc) {
		npc.startAnimation(6987);

//		int nX = npc.getX() + 1;
//		int nY = npc.getY() + 1;

		for (Player p : getNearbyPlayers(npc)) {
			if (p == null || p.disconnected || p.isDead() || !p.isInNexRoom()) {
				continue;
			}
			int dist = Misc.distanceToPoint(npc.getX(), npc.getY(), p.getX(),
					p.getY());
			if (dist <= 10) { // magic distance
				Client c = (Client) p;

				int hitDelay = 2; // adjust this to work proper delay
				if (dist > 2) {
					hitDelay += 1;
				}

				if (dist > 4) {
					hitDelay += 1;
				}
				if (dist > 8) {
					hitDelay += 1;
				}

				// int offX = (nY - c.getY()) * -1;
				// int offY = (nX - c.getX()) * -1;
				// c.getPA().createPlayersProjectile(nX, nY, offX, offY, 50,
				// c.getCombat().getProjectileSpeed(dist)/*getProjectileSpeed(i)*/,
				// 384/*npcs[i].projectileId*/, 35, 20, -c.getId() - 1, 50);
				NpcHitPlayer.dealMagicDamage(npc, c, Misc.random(30), hitDelay, ICE_BLITZ_END_GFX);
				
				Projectile proj = new Projectile(362, Location.create(npc.getX()+1,
						npc.getY()+1, npc.getHeightLevel()), c.getCurrentLocation());
				proj.setStartHeight(10);
				proj.setEndHeight(10);
				proj.setStartDelay(50);
				proj.setLockon(c.getProjectileLockon());
				proj.setEndDelay(5 * dist + 55);
				GlobalPackets.createProjectile(proj);

//				int pX = c.getX();
//				int pY = c.getY();
//				int offX = (nY - pY) * -1;
//				int offY = (nX - pX) * -1;
//				GlobalPackets.createPlayersProjectile(nX, nY, c.getZ(), offX,
//						offY, 50, (5 * dist + 55), 362, 10, 10, -c.getId() - 1,
//						50);

			}
		}
	}

	public static void shadowBlastMulti(final NPC npc) {
		npc.startAnimation(6984);

		for (Player p : getNearbyPlayers(npc)) {
			// int dist = Misc.distanceToPoint(npc.getX(), npc.getY(), p.getX(),
			// p.getY());
			Client c = (Client) p;

			int x = c.getX() - 2;
			int y = c.getY() - 2;
			x += Misc.random(5);
			y += Misc.random(5);

			final int finalX = x;
			final int finalY = y;

			Server.getStillGraphicsManager().createGfx(x, y,
					npc.getHeightLevel(), 382, 0, 0);

			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {

					Server.getStillGraphicsManager().createGfx(finalX, finalY,
							npc.getHeightLevel(), 383, 0, 0);
					for (Player p : getNearbyPlayers(npc)) {
						if (p.getX() == finalX && p.getY() == finalY) {
							NpcHitPlayer.dealMagicDamage(npc, c,
									Misc.random(30), 1, null);
						}
					}

					container.stop();

				}

				@Override
				public void stop() {
				}
			}, 3);

		}
	}

	public static void shadowBlitzMulti(NPC npc) {
		npc.startAnimation(6987);

//		int nX = npc.getX() + 1;
//		int nY = npc.getY() + 1;

		for (Player p : getNearbyPlayers(npc)) {
			if (p == null || p.disconnected || p.isDead() || !p.isInNexRoom()) {
				continue;
			}
			int dist = Misc.distanceToPoint(npc.getX(), npc.getY(), p.getX(),
					p.getY());
			if (dist <= 10) { // magic distance
				Client c = (Client) p;

				int hitDelay = 2; // adjust this to work proper delay
				if (dist > 2) {
					hitDelay += 1;
				}

				if (dist > 4) {
					hitDelay += 1;
				}
				if (dist > 8) {
					hitDelay += 1;
				}

				// int offX = (nY - c.getY()) * -1;
				// int offY = (nX - c.getX()) * -1;
				// c.getPA().createPlayersProjectile(nX, nY, offX, offY, 50,
				// c.getCombat().getProjectileSpeed(dist)/*getProjectileSpeed(i)*/,
				// 384/*npcs[i].projectileId*/, 35, 20, -c.getId() - 1, 50);
				NpcHitPlayer.dealMagicDamage(npc, c, Misc.random(30), hitDelay,
						SHADOW_BLITZ_END_GFX);
				
				Projectile proj = new Projectile(380, Location.create(npc.getX()+1,
						npc.getY()+1, npc.getHeightLevel()), c.getCurrentLocation());
				proj.setStartHeight(10);
				proj.setEndHeight(10);
				proj.setStartDelay(50);
				proj.setLockon(c.getProjectileLockon());
				proj.setEndDelay(5 * dist + 55);
				GlobalPackets.createProjectile(proj);

//				int pX = c.getX();
//				int pY = c.getY();
//				int offX = (nY - pY) * -1;
//				int offY = (nX - pX) * -1;
//				GlobalPackets.createPlayersProjectile(nX, nY, c.getZ(), offX,
//						offY, 50, (5 * dist + 55), 380, 10, 10, -c.getId() - 1,
//						50);

			}
		}
	}

	public static void smokeAttack(NPC npc) {
		npc.startAnimation(6986);

//		int nX = npc.getX() + 1;
//		int nY = npc.getY() + 1;

		for (Player p : getNearbyPlayers(npc)) {
			if (p == null || p.disconnected || p.isDead() || !p.isInNexRoom()) {
				continue;
			}
			int dist = Misc.distanceToPoint(npc.getX(), npc.getY(), p.getX(),
					p.getY());
			if (dist <= 10) { // magic distance
				Client c = (Client) p;

				int hitDelay = 2; // adjust this to work proper delay
				if (dist > 2) {
					hitDelay += 1;
				}

				if (dist > 4) {
					hitDelay += 1;
				}
				if (dist > 8) {
					hitDelay += 1;
				}

				// int offX = (nY - c.getY()) * -1;
				// int offY = (nX - c.getX()) * -1;
				// c.getPA().createPlayersProjectile(nX, nY, offX, offY, 50,
				// c.getCombat().getProjectileSpeed(dist)/*getProjectileSpeed(i)*/,
				// 384/*npcs[i].projectileId*/, 35, 20, -c.getId() - 1, 50);
				NpcHitPlayer.dealMagicDamage(npc, c, Misc.random(25), hitDelay, SMOKE_BLITZ_END_GFX);

				if (Misc.random(99) < 15) { // random chance of poisoning
											// players
					c.getDotManager().applyPoison(4, npc);
				}
				
				Projectile proj = new Projectile(195, Location.create(npc.getX()+1,
						npc.getY()+1, npc.getHeightLevel()), c.getCurrentLocation());
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(50);
				proj.setLockon(c.getProjectileLockon());
				proj.setEndDelay(5 * dist + 55);
				GlobalPackets.createProjectile(proj);

//				int pX = c.getX();
//				int pY = c.getY();
//				int offX = (nY - pY) * -1;
//				int offY = (nX - pX) * -1;
//				GlobalPackets.createPlayersProjectile(nX, nY, c.getZ(), offX,
//						offY, 50, (5 * dist + 55), 195/* 384 */, 43, 31,
//						-c.getId() - 1, 50);

			}
		}
	}

}
