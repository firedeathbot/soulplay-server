package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 109452, 109453 })
public class NightmareParasiteNPC extends CombatNPC {

	public static final int ID = 109452;
	public static final int ID_WEAKEN = 109453;
	public NightmareNPC nightmareNPC;
	public int extraHp;

	public NightmareParasiteNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		ignoreAggroTolerence();
		stopAttackDefault = true;

		getSkills().setStaticLevel(Skills.HITPOINTS, getSkills().getMaximumLifepoints() + extraHp);
	}

	@Override
	public void startAttack(Player p) {
		boolean heal = nightmareNPC != null && nightmareNPC.getSkills().getLifepoints() < nightmareNPC.getSkills().getMaximumLifepoints();
		Mob projectileTarget;
		Animation animation;
		int projectileId;
		final CombatType style;

		if (heal) {
			style = null;
			animation = Animation.osrs(8555);
			projectileId = Graphic.getOsrsId(1774);
			projectileTarget = nightmareNPC;
		} else if (Misc.randomBoolean()) {
			style = CombatType.MAGIC;
			animation = Animation.osrs(8555);
			projectileId = Graphic.getOsrsId(1771);
			projectileTarget = p;
		} else {
			style = CombatType.RANGED;
			animation = Animation.osrs(8554);
			projectileId = Graphic.getOsrsId(1775);
			projectileTarget = p;
		}

		face(projectileTarget);
		startAnimation(animation);
		Projectile projectile = Projectile.create(this, projectileTarget, projectileId/*, 10,
				20, 50, 80, 5, 20*/);
		projectile.send();
		pulse(() -> {
			if (heal) {
				int healMax = getId() == ID_WEAKEN ? 45 : 90;
				int healRoll = Misc.random(healMax);
				nightmareNPC.getSkills().heal(healRoll);
				// TODO hitsplat?
				// TODO update shield hud? and test
			} else {
				if (style == CombatType.RANGED) {
					p.dealRangeDamage(Misc.random(maxHit), 0, null, this);
				} else {
					p.dealMagicDamage(Misc.random(maxHit), 0, Graphic.osrs(1772, GraphicType.HIGH), this);
				}
			}
		}, 3);
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(ID, 40, 10, 60, 40);
		NPCAnimations.define(ID, Animation.getOsrsAnimId(8559), -1, -1);

		NpcStats.define(ID_WEAKEN, 40, 10, 60, 40);
		NPCAnimations.define(ID_WEAKEN, Animation.getOsrsAnimId(8559), -1, -1);
	}

}
