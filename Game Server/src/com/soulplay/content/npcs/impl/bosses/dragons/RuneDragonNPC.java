package com.soulplay.content.npcs.impl.bosses.dragons;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108031 })
public final class RuneDragonNPC extends CombatNPC {

	public RuneDragonNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize() && Misc.randomBoolean(3)) {
			meleeSwing(p, 38, 80);
		} else if (Misc.randomBoolean(6)) { // special shit
			if (Misc.randomBoolean()) {
				startAnimation(81);
				Location centerLocation = p.getCenterLocation().copyNew();
				Projectile projectile = new Projectile(Graphic.getOsrsId(1488), this, p);
				projectile.setStartDelay(40);
				projectile.setStartHeight(10);
				projectile.setEndHeight(31);
				projectile.setStartOffset(127);
				projectile.setEndDelay(80);
				projectile.send();
				pulse(() -> {
					Graphic.osrs(1488).send(centerLocation);
					sendElectric(centerLocation, p);
					Location sl = centerLocation.transform(Direction.getRandomNoDiagonal());
					Graphic.osrs(1488).send(sl);
					pulse(() -> sendElectric(sl, p));
				}, 3);
			} else {
				startAnimation(81);
				Projectile projectile = new Projectile(Graphic.getOsrsId(27), this, p);
				projectile.setStartDelay(41);
				projectile.setStartHeight(38);
				projectile.setEndHeight(36);
				projectile.setStartOffset(255);
				projectile.setEndDelay(63);
				projectile.send();
				pulse(() -> {
					if (p.isDead() || isDead()) {
						return;
					}

					int maxHit = Misc.random(38);
					p.dealDamage(new Hit(maxHit, 0, 1));
					p.startGraphic(Graphic.create(753));
					getSkills().heal(maxHit);
				}, 2);
			}
		} else {
			switch (Misc.random(2)) {
				case 0: {
					startAnimation(81);
					Projectile projectile = new Projectile(Graphic.getOsrsId(162), this, p);
					projectile.setStartDelay(41);
					projectile.setStartHeight(20);
					projectile.setEndHeight(20);
					projectile.setStartOffset(255);
					projectile.setEndDelay(80);
					projectileSwing(p, projectile, 38, 3, Graphic.osrs(163, GraphicType.HIGH), CombatType.MAGIC);
					break;
				}
				case 1: {
					startAnimation(81);
					Projectile projectile = new Projectile(13, this, p);
					projectile.setStartDelay(41);
					projectile.setStartHeight(20);
					projectile.setEndHeight(20);
					projectile.setStartOffset(255);
					projectile.setEndDelay(80);
					projectileSwing(p, projectile, 38, 3, null, CombatType.RANGED);
					break;
				}
				case 2: {
					startAnimation(81);
					startGraphic(Graphic.osrs(1, GraphicType.FIFTY));
					p.pulse(() -> {
						int damage = p.getPA().handleDragonFireHit(this, Misc.random(40) + 10);
						p.dealTrueDamage(damage, 0, null, this);
					}, 2);
					break;
				}
			}
		}
	}

	private void sendElectric(Location location, Player player) {
		Graphic.osrs(1488).send(location);

		if (player.isDead() || isDead()) {
			return;
		}

		if (player.getCenterLocation().matches(location)) {
			player.dealDamage(new Hit(Misc.random(blockElectric(player) ? 3 : 16)));
		}

		pulse(() -> {
			if (player.isDead() || isDead()) {
				return;
			}

			if (player.getCenterLocation().matches(location)) {
				player.dealDamage(new Hit(Misc.random(blockElectric(player) ? 3 : 16)));
			}
		});
	}

	private static boolean blockElectric(Player player) {
		if (player.playerEquipment[PlayerConstants.playerFeet] == 7159) {
			return true;
		}

		return false;
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(108031, 28, 26, 80);
	}

}
