package com.soulplay.content.npcs.impl.bosses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.PlayerSaveSql;

public class BossKCHandler {

	public static boolean loadKillCounts(Player p, Connection connection) {

		// cheaphax to converting from old killcount stuff to new one.
		if (p.getBossKills().length != BossKCEnum.values.length) {
			p.bossKills = new int[BossKCEnum.values.length];
		}

		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(
						"select * from `boss_hiscores` where player_id = '"
								+ p.mySQLIndex + "' limit 1"); ) {
			if (rs.next()) {
				for (int i = 0; i < BossKCEnum.values.length; i++) {
					p.setBossKills(i,
							rs.getInt(BossKCEnum.values[i].getMysqlName()),
							false);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void saveAllKillCounts(Player p) {
		Connection connection = Server.getConnection();

		Statement stmt = null;

		// "INSERT INTO `accountspvp` (ID, PlayerName, Password, xf_id) values
		// ('" + p.mySQLIndex + "', '" + playerName + "', '" + playerPass + "',
		// '" + p.xenforoIndex +"')", Statement.RETURN_GENERATED_KEYS

		StringBuilder build = new StringBuilder();

		build.append("INSERT INTO `boss_hiscores` (player_id, ");

		final int KILLCOUNT_LENGTH = BossKCEnum.values.length;
		int count = 0;
		for (BossKCEnum boss : BossKCEnum.values) {
			build.append(" " + boss.getMysqlName());
			count++;
			if (count != KILLCOUNT_LENGTH) {
				build.append(",");
			}
		}

		build.append(") values ('" + p.mySQLIndex + "', "); // add space bar
															// after building
															// that loop.

		count = 0;
		for (int i = 0; i < BossKCEnum.values.length; i++) {
			build.append(" '" + p.getBossKills(i) + "'");
			count++;
			if (count != KILLCOUNT_LENGTH) {
				build.append(",");
			}
		}

		build.append(")");

		try {

			stmt = connection.createStatement();
			stmt.executeUpdate(build.toString(),
					Statement.RETURN_GENERATED_KEYS);

			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void saveKillCount(final int mysqlIndex,
			final BossKCEnum boss, final int killCount) {
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {
			Connection connection = Server.getConnection();

			Statement stmt = null;

			try {

				stmt = connection.createStatement();
				stmt.executeUpdate("INSERT INTO `boss_hiscores` (player_id, "
						+ boss.getMysqlName() + ") values ('" + mysqlIndex
						+ "', '" + killCount + "') ON DUPLICATE KEY UPDATE "
						+ boss.getMysqlName() + "=" + killCount,
						Statement.RETURN_GENERATED_KEYS);

				try {
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			} catch (SQLException e2) {
				e2.printStackTrace();
			} finally {
				try {
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException e3) {
					e3.printStackTrace();
				}
			}
		});
	}

}
