package com.soulplay.content.npcs.impl.bosses.drake;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108612, 108613 })
public final class DrakeNPC extends CombatNPC {

	private static final int MELEE_ATTACK_ANIM = 8275;
	private static final int RANGED_ATTACK_ANIM = 8276;
	private static final int DEATH_ANIM = 8277;
	private static final int TRANSFORM_ID = 108613;
	private static final int TRANSFORMED_DEATH_ANIM = 8278;
	private int attacks = 0;

	public DrakeNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setFollowDistance(8);
		getDistanceRules().setMeleeDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (attacks > 6) {
			startAnimation(Animation.getOsrsAnimId(RANGED_ATTACK_ANIM));
			Location end = p.getCurrentLocation().copyNew();
			Projectile projectile = Projectile.create(getCenterLocation(), end, Graphic.getOsrsId(1637));
			projectile.setEndHeight(1);
			projectile.setEndDelay(130);
			projectile.setStartDelay(35);
			projectile.send();
			pulse(() -> {
				if (p.getCurrentLocation().equals(end)) {
					p.sendMessage("You're burnt by the Drake's volcanic breath!");
					p.dealDamage(new Hit(Misc.random(6, 8), 0, -1));
					CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

						int ticks = 0;

						@Override
						public void execute(CycleEventContainer container) {
							p.dealDamage(new Hit(Misc.random(6, 8), 0, -1));
							ticks++;
							if (ticks >= 3) {
								container.stop();
							}
						}

					}, 1);

				}

				Graphic.create(Graphic.getOsrsId(1638)).send(end);
			}, 5);
			attacks = 0;
		} else {
			if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize() && Misc.randomBoolean(1)) {
				meleeSwing(p, 15, Animation.getOsrsAnimId(MELEE_ATTACK_ANIM));
			} else {
				startAnimation(Animation.getOsrsAnimId(RANGED_ATTACK_ANIM));
				projectileSwing(p, Graphic.getOsrsId(1636), 15, 3, null, CombatType.RANGED);
			}
			attacks++;
		}
	}

	@Override
	public int dealDamage(Hit hit) {
		int damage = super.dealDamage(hit);

		if (getSkills().getLifepoints() <= 0) {
			getSkills().healToFull();
			takeDamage = false;
			dontFace = true;

			startAnimation(Animation.getOsrsAnimId(DEATH_ANIM));
			pulse(() -> {
				transform(TRANSFORM_ID);
				startAnimation(Animation.getOsrsAnimId(TRANSFORMED_DEATH_ANIM));
				die();
			}, 2);
		}

		return damage;
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(108612, 250, 15, 140, 120);
		NpcStats.define(108613, 250, 15, 140, 120);
		NPCAnimations.define(108612, Animation.getOsrsAnimId(8278), -1, 28270);
		NPCAnimations.define(108613, Animation.getOsrsAnimId(8278), -1, -1);
	}
}
