package com.soulplay.content.npcs.impl.bosses.alchhydra;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.skills.slayer.Assignment;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.InstanceManager;

public class AlchemicalHydraInstance extends InstanceManager {

	private AlchemicalHydra hydra;
	private CycleEventContainer respawnEvent;
	private Location blueVent;
	private Location redVent;
	private Location greenVent;
	private List<Location> vents = new ArrayList<>(3);

	public AlchemicalHydraInstance(Player player) {
		super("Alchemical hydra", player);
		createStaticRegion(Location.create(7744, 10240), 64, 64);
		teleport();
		blueVent = base(18, 32);
		redVent = base(27, 23);
		greenVent = base(27, 32);
		vents.add(blueVent);
		vents.add(redVent);
		vents.add(greenVent);
		spawnHydra();
	}

	private void spawnHydra() {
		if (hydra != null) {
			return;
		}

		hydra = (AlchemicalHydra) spawnNpc(108615, 22, 26);
		respawnEvent = null;
	}

	private void teleport() {
		Player player = getPlayer();
		initPlayerDefaults(player);
		player.pulse(() -> {
			player.getPA().movePlayer(base(8, 12));
		});
	}

	@Override
	public Location logoutLocation() {
		return Location.create(7752, 10250);
	}

	@Override
	public void destroy(boolean hard) {
		super.destroy(hard);

		if (hydra != null && hydra.isActive) {
			hydra.nullNPC();
			hydra = null;
		}

		if (respawnEvent != null) {
			respawnEvent.stop();
			respawnEvent = null;
		}
	}

	public void initRespawn() {
		if (respawnEvent != null) {
			return;
		}

		Player player = getPlayer();
		SlayerTask slayerTask = player.getSlayerTask();
		if (slayerTask == null) {
			player.sendMessage("You've have completed your task. Alchemical hydra won't respawn from now on.");
			return;
		}

		Assignment assignemnt = slayerTask.getAssignment();
		if (assignemnt == null || assignemnt != Assignment.HYDRAS_BY_KONAR) {
			player.sendMessage("You've have completed your task. Alchemical hydra won't respawn from now on.");
			return;
		}

		hydra = null;
		respawnEvent = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				spawnHydra();
				container.stop();
			}

		}, 40);
	}

	public AlchemicalHydra getHydra() {
		return hydra;
	}

	public Location getBlueVent() {
		return blueVent;
	}
	
	public Location getRedVent() {
		return redVent;
	}
	
	public Location getGreenVent() {
		return greenVent;
	}

	public List<Location> getVents() {
		return vents;
	}

}
