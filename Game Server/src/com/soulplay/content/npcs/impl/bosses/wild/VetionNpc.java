package com.soulplay.content.npcs.impl.bosses.wild;

import com.soulplay.Server;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4175})
public class VetionNpc extends AbstractNpc {

	private boolean imuneStarted = false;
	private boolean transformed = false;
	private boolean isImune = false;
	
	private int attackPattern = -1;
	private int revertTick = 0;
	
	private NPC dog1, dog2;
	
	public VetionNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return null;
	}
	
	@Override
	public void onSpawn() {
		super.onSpawn();

		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		setCanWalk(true);
		getDistanceRules().setFollowDistance(20);
		//getDistanceRules().setMeleeDistance(2);
		
	}
	
	private NPC spawnSkeleton() {
		int slot = Misc.findFreeNpcSlot();
		if (slot != -1) {
			NPC dog = new NPC(slot, 1575);
			dog.walksOnTopOfNpcs = true;
			NPCHandler.spawnNpc(dog, getX() + Misc.random(4), getY() + Misc.random(4), getZ(), 0);
			dog.forceChat("GRRRRRRRRRRRR");
			return dog;
		}
		return null;
	}
	
	@Override
	public int dealDamage(Hit hit) {
		if (isImune) hit.setDamage(0);
		
		int damage = super.dealDamage(hit);
		
		if (!imuneStarted && getSkills().getLifepoints() <= 126) {
			imuneStarted = true;
			
			dog1 = spawnSkeleton();
			dog2 = spawnSkeleton();
			
			if (hit.getSource().isPlayer()) {
				int index = hit.getSource().toPlayerFast().getId();
				dog1.setKillerId(index);
				dog2.setKillerId(index);
			}
				
			
			makeImmune(true);
			
			forceChat(transformed ? "Bahh! Go, Dogs!!" : "Kill, my pets!");
		}
		
		if (!transformed && getSkills().getLifepoints() < 1) {
			transform(4176);
			
			transformed = true;
			imuneStarted = false;
			makeImmune(false);
			
			forceChat("Do it again!!!");
			
			getSkills().healToFull();
		}
		
		return damage;
	}
	
	@Override
	public void process() {
		super.process();
		
		if (isDead())
			return;
		
		if (getAttackTimer() == 2) {
			startAnimation(SEARCHING);
		}
		
		if (transformed) {
			revertTick++;
			
			if (revertTick > 500) { // 5 minutes to revert
				transform(4175);
				transformed = false;
				getSkills().healToFull();
				revertTick = 0;
				
				makeImmune(false);
				
				if (dog1 != null)
					dog1.nullNPC();
				if (dog2 != null)
					dog2.nullNPC();
			}
		}
		
		if (imuneStarted) {
			
			if (dog1 != null && dog1.isDead())
				dog1 = null;
			if (dog2 != null && dog2.isDead())
				dog2 = null;
			
			if (dog1 == null && dog2 == null) {
				makeImmune(false);
			}
		}
		
	}
	
	@Override
	public boolean loadedSpell(Player p) {
		
		int stage = attackPattern % 4;
		
		if (Misc.randomBoolean(3))
			attackType = 2;
		
		
		if (stage == 3) { // earthquake

			//slam ground
			attackPattern++;
			startAnimation(ATTACK_EARTH_Q_ANIM);
			
			lockMovement(2);
			
			for (Player others : Server.getRegionManager().getLocalPlayers(this)) {
				if (Misc.distanceToPoint(getX()+1, getY()+1, others.getX(), others.getY()) < 8) {
					others.dealTrueDamage(Misc.random(46), 2, null, this);
				}
			}
			
			walkUpToHit = false;
			stopAttack = true;
		} else {
			if (Misc.randomBoolean(3)) {
				attackPattern++;
				
				startAnimation(ATTACK_ANIM);
				
				lockMovement(2);
				
				walkUpToHit = false;
				stopAttack = true;
				
				//magic attack
				Location start = getCurrentLocation().transform(1, 1);
				
				Location end1 = p.getCurrentLocation().copyNew();
				
				Location end2 = p.getCurrentLocation().transform(-4 + Misc.random(8), -4 + Misc.random(8));
				
				Location end3 = p.getCurrentLocation().transform(-4 + Misc.random(8), -4 + Misc.random(8));
				
				int projectilDelay = 200;
				
				Projectile proj = new Projectile(280, start, end1);
				proj.setStartDelay(60);
				proj.setEndDelay(projectilDelay);
				proj.setStartHeight(50);
				proj.setSlope(50);
				proj.setEndHeight(3);
				
				GlobalPackets.createProjectile(proj);
				proj.setEndLoc(end2);
				GlobalPackets.createProjectile(proj);
				proj.setEndLoc(end3);
				GlobalPackets.createProjectile(proj);
				
				projectilDelay += 5;
				
				Server.getStillGraphicsManager().createGfx(end1.getX(), end1.getY(), end1.getZ(), 281, 0, projectilDelay);
				Server.getStillGraphicsManager().createGfx(end2.getX(), end2.getY(), end1.getZ(), 281, 0, projectilDelay);
				Server.getStillGraphicsManager().createGfx(end3.getX(), end3.getY(), end1.getZ(), 281, 0, projectilDelay);
				
				final NPC attacker = this;
				
				Server.getTaskScheduler().schedule(7, ()-> {
					for (Player other : Server.getRegionManager().getLocalPlayersByLocation(end1)) {
						if (other.getCurrentLocation().matches(end1) || other.getCurrentLocation().matches(end2) || other.getCurrentLocation().matches(end3)) {
							NpcHitPlayer.dealMagicDamage(attacker, other, Misc.random(46), 0, null);
						}
					}
				});
				
			} else {
				//melee
				walkUpToHit = true;
				attackType = 0;
				stopAttack = false;
				attackAnim = ATTACK_ANIM;
				
				//moveX = NPCHandler.GetMove(getX(), p.getX());
				//moveY = NPCHandler.GetMove(getY(), p.getY());
				
			}
		}
		
		return true;
	}
	
	@Override
	public void startAttack(Player p) {
		if (attackType == 0) {
			attackPattern++;
		}
	}
	
	private void makeImmune(boolean makeImmune) {
		if (makeImmune) {
			setAttackable(false);
		} else {
			setAttackable(true);
		}

		isImune = makeImmune;
	}
	
	public static final Animation ATTACK_ANIM = new Animation(Animation.getOsrsAnimId(5499));
	public static final Animation ATTACK_EARTH_Q_ANIM = new Animation(Animation.getOsrsAnimId(5507));
	public static final Animation SEARCHING = new Animation(Animation.getOsrsAnimId(5505));

}
