package com.soulplay.content.npcs.impl.bosses.livingrocks;

import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;

public abstract class LivingRockNpc extends CombatNPC {

	private boolean executeTransform;

	public LivingRockNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		getSkills().healToFull();
		applyDead = false;
		setDead(false);
		setAttackable(false);
		setRetaliates(false);
		isAggressive = false;
		resetFollow();
		randomWalk = false;
		setCanWalk(false);
		final Client p = (Client) PlayerHandler.players[getKillerId()];
		if (p != null) {
			p.getCombat().resetPlayerAttack();
		}

		resetAttack();
		executeTransform = true;
	}

	@Override
	public void process() {
		super.process();

		if (executeTransform) {
			livingRockRemains();
			executeTransform = false;
		}
	}

	public void livingRockRemains() {
		startAnimation(getDeathAnim());
		pulse(() -> {
			transform(getTransformId());
			pulse(() -> {
				nullNPC();
			}, 100);
		}, 9);
	}

	public abstract int getDeathAnim();

	public abstract int getTransformId();

}
