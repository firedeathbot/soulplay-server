package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 109437, 109438, 109439 })
public class NightmareTotemSE extends NightmareTotem {

	public static final int REGULAR = 109437;
	public static final int CHARGE = 109438;
	public static final int FULL_CHARGE = 109439;

	public NightmareTotemSE(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareTotemSE(slot, npcType);
	}

	@Override
	public int getFullChargeNpcId() {
		return FULL_CHARGE;
	}

	@Override
	public int getChargeNpcId() {
		return CHARGE;
	}

	@Override
	public String getChargedMessage() {
		return "The south east totem is fully charged.";
	}

}
