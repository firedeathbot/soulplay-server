package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 109460, 109461, 109462, 109463, 109464 })
public class NightmareLobbyNPC extends AbstractNpc {

	public static final int SLEEPING = 109460;
	public static final int AWAKENING = 109461;
	public static final int FIRST_PHASE = 109462;
	public static final int SECOND_PHASE = 109463;
	public static final int THIRD_PHASE = 109464;

	private Runnable queuedTransform;

	public NightmareLobbyNPC(int slot, int npcType) {
		super(slot, npcType);

		isAggressive = false;
		setRetaliates(false);
		stopAttack = true;
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NightmareLobbyNPC(slot, npcType);
	}

	@Override
	public void process() {
		super.process();

		if (queuedTransform != null) {
			queuedTransform.run();
			queuedTransform = null;
		}
	}

	public void startAwaken() {
		queuedTransform = () -> {
			if (getNpcTypeSmart() == AWAKENING)
				return;
	
			startAnimation(Animation.osrs(8573));
			pulse(() -> transform(AWAKENING), 5);
		};
	}

	public void awaken() {
		queuedTransform = () -> {
			if (getNpcTypeSmart() == FIRST_PHASE)
				return;
	
			startAnimation(Animation.osrs(8575));
			pulse(() -> transform(FIRST_PHASE), 2);
		};
	}

	public void stall() {
		queuedTransform = () -> {
			if (getNpcTypeSmart() == AWAKENING)
				return;
	
			startAnimation(Animation.osrs(8576));
			pulse(() -> transform(AWAKENING), 2);
		};
	}

	public void sleep() {
		queuedTransform = () -> {
			if (getNpcTypeSmart() == SLEEPING)
				return;
	
			startAnimation(Animation.osrs(8581));
			pulse(() -> transform(SLEEPING), 5);
		};
	}

}
