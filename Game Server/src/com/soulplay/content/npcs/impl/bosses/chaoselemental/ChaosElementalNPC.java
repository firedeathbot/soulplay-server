package com.soulplay.content.npcs.impl.bosses.chaoselemental;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Priority;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

/**
 * @author Julius
 */
@NpcMeta(ids = {3200})
public final class ChaosElementalNPC extends CombatNPC {
	
	public int teleportAnim = 5442;
	
	public ChaosElementalNPC(int slot, int npcType) {
		super(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		walkUpToHit = false;
		getDistanceRules().setFollowDistance(10);
		getDistanceRules().setMeleeDistance(8);
	}
	
	@Override
	public void startAttack(Player p) {
		if (Misc.random(4) == 4) {
			if (Misc.random(2) > 0) {
				unequipItems(p);
			} else {
				teleportPlayer(p);
			}
		} else if (Misc.random(2) == 0) {
			startAnimation(5443);
		    projectileSwing(p, 552, 22, 3, null, CombatType.MAGIC);
		} else if (Misc.random(2) == 1) {
			startAnimation(5443);
		    projectileSwing(p, 552, 22, 3, null, CombatType.RANGED);
		} else {
			startAnimation(5443);
		    projectileSwing(p, 552, 22, 3, null, CombatType.MELEE);	
		}
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(3200, 255, 20, 260, 140);
		NPCAnimations.define(3200, 5446, 5444, 5443);
	}
	
	private void teleportPlayer(Player p) {
		Projectile.create(getCenterLocation(), p, 555).send();
		final int randomX = Misc.random(14) + getX() - 7;
		final int randomY = Misc.random(14) + getY() - 7;
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				p.getPA().movePlayer(randomX, randomY, p.getZ());
				p.startGraphic(new Graphic(Priority.HIGH, 554, 0, GraphicType.LOW));
			}
		}, 4);
	}
	
	private void unequipItems(Player p) {
		Projectile.create(getCenterLocation(), p, 558).send();
		p.getItems().forceUnequip(PlayerConstants.playerWeapon);
	}
	
}
