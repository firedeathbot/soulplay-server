package com.soulplay.content.npcs.impl.bosses.scorpia;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4172})
public class ScorpiaNpc extends AbstractNpc {
	
	public static final int SCORPIA_GUARD_ID = 13681;
	
	private boolean spawnedHealers = false;

	public ScorpiaNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new ScorpiaNpc(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		isAggressive = true;
		walksOnTopOfNpcs = true;
		inMultiZone = true;
		inWildPermanent = true;
	}
	
	@Override
	public void afterNpcDeath() {
		super.afterNpcDeath();
		actionTimer = 16; // respawn it in 10 seconds
	}
	
	@Override
	public int dealDamage(Hit hit) {
		int damage = super.dealDamage(hit);
		if (damage > 0 && !spawnedHealers && getSkills().getLifepoints() <= getSkills().getMaximumLifepoints() / 2) {
			spawnHealers();	
		}
		return damage;
	}
	
	private void spawnHealers() {
		spawnedHealers = true;
		int slot1 = Misc.findFreeNpcSlot();
		if (slot1 != -1) {
			ScorpiaGuardNpc guard1 = new ScorpiaGuardNpc(slot1, SCORPIA_GUARD_ID);
			NPCHandler.spawnNpc(guard1, getX(), getY(), getZ(), 0);
			guard1.setMother(this);
		}
		
		slot1 = Misc.findFreeNpcSlot();
		if (slot1 != -1) {
			ScorpiaGuardNpc guard1 = new ScorpiaGuardNpc(slot1, SCORPIA_GUARD_ID);
			NPCHandler.spawnNpc(guard1, getX()+2, getY()+2, getZ(), 0);
			guard1.setMother(this);
		}
	}
	
	@Override
	public void onDamageApplied(Player p, int damage) {
		super.onDamageApplied(p, damage);
		if (damage > 0 && Misc.randomBoolean(3)) {
			p.getDotManager().applyPoison(20, this);
		}
	}
}
