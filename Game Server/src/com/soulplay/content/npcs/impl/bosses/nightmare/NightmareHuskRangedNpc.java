package com.soulplay.content.npcs.impl.bosses.nightmare;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

@NpcMeta(ids = { 109455 })
public class NightmareHuskRangedNpc extends NightmareHuskNpc {

	public static final int ID = 109455;

	public NightmareHuskRangedNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void startAttack(Player p) {
		startAnimation(Animation.osrs(8564));
		projectileSwing(p, Graphic.getOsrsId(1778), 14, 3, null, CombatType.RANGED);
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(ID, 20, 14, 80, 30);
		NPCAnimations.define(ID, Animation.getOsrsAnimId(8566), -1, -1);
	}

}