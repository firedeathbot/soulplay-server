package com.soulplay.content.npcs.impl.bosses.basiliskknight;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

import static com.soulplay.Server.getStillGraphicsManager;
import static com.soulplay.Server.getTaskScheduler;

/**
 * @author Jire
 */
@NpcMeta(ids = {109293})
public final class BasiliskKnightNPC extends CombatNPC {
	
	public BasiliskKnightNPC(int slot, int npcType) {
		super(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setFollowDistance(1);
	}
	
	@Override
	public void startAttack(Player p) {
		if (!p.performingAction && Misc.randomBoolean(10)) {
			rockFreezeSpell(p);
		} else if (getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize()
				&& Misc.randomBoolean(1)) {
			meleeSwing(p, 20, Animation.getOsrsAnimId(8499), 3);
		} else {
			startAnimation(Animation.getOsrsAnimId(8500));
			projectileSwing(p, Graphic.getOsrsId(1735), 20, 3,
					Graphic.create(Graphic.getOsrsId(1736), Graphic.GraphicType.HIGH),
					CombatType.MAGIC);
		}
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(109293, 300, 20, 186, 186);
		NPCAnimations.define(109293, 28501, -1, 28499);
	}
	
	private void rockFreezeSpell(Player p) {
		startAnimation(Animation.osrs(8500));
		Location landLocation = p.getCurrentLocation().copyNew();
		Projectile.create(getCurrentLocation(), landLocation, Graphic.getOsrsId(1744))
				.setEndDelay(100)
				.setEndHeight(10)
				.send();
		pulse(() -> {
			getStillGraphicsManager().createGfx(landLocation, Graphic.getOsrsId(1738));
			if (landLocation.equals(p.getCurrentLocation())) {
				NpcHitPlayer.dealMagicDamage(this, p, Misc.random(47), 0, null);
				p.performingAction = true;
				p.startAnimation(Animation.osrs(8503));
				getTaskScheduler().schedule(new Task() {
					private int ticks = 0;
					
					@Override
					protected void execute() {
						p.startAnimation(Animation.osrs(8504));
						if (ticks++ >= 8) {
							stop();
							p.performingAction = false;
							p.npcIndex = getId();
							p.startAnimation(Animation.getOsrsAnimId(8507));
							p.startGraphic(Graphic.create(Graphic.getOsrsId(1743)));
						}
					}
				});
			}
		}, 4);
	}
	
}
