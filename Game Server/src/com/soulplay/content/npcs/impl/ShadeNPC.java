package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

@NpcMeta(ids = {14054})
public class ShadeNPC extends CombatNPC {

	public ShadeNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new ShadeNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		getSkills().setStaticLevel(Skills.ATTACK, 130);
		getSkills().setStaticLevel(Skills.STRENGTH, 136);
		getSkills().setStaticLevel(Skills.DEFENSE, 100);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(14054, 115, 15, 130, 100);
		NPCAnimations.define(14054, Animation.getOsrsAnimId(1283), Animation.getOsrsAnimId(1286), Animation.getOsrsAnimId(1285));
	}
	
}
