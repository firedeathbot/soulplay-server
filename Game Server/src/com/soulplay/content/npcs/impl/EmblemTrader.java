package com.soulplay.content.npcs.impl;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class EmblemTrader {

	private enum TeleportData {
		SPOT_1(9613, 10197), 
		SPOT_2(9655, 10196),
		SPOT_3(9528, 10136),
		SPOT_4(9591, 10145),
		SPOT_5(9587, 10116),
		SPOT_6(9619, 10142),
		SPOT_7(9651, 10141),
		SPOT_8(9640, 10216),
		SPOT_9(9645, 10119),
		SPOT_10(9611, 10219);

		public static final TeleportData[] values = values();
		private final int x, y;

		TeleportData(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final int NPC_ID = 7941;
	private static final int TELEPORT_TICKS = 350;
	private static NPC thisNpc;
	private static int teleportCursor;

	public static void spawnEmblemTrader() {
		if (thisNpc != null) {
			return;
		}

		teleportCursor = Misc.randomNoPlus(TeleportData.values.length);
		TeleportData data = TeleportData.values[teleportCursor];
		
		int npcSlot = Misc.findFreeNpcSlot();
		
		if (npcSlot != -1) {
			thisNpc = new NPC(npcSlot, NPC_ID);
			NPCHandler.spawnNpc(thisNpc, data.x, data.y, 0, 0);
		} else {
			return;
		}
		
		thisNpc.setWalksHome(false);
		thisNpc.teleEndAnim = new Animation(8941);
		thisNpc.teleEndGfx = Graphic.create(1577);

		CycleEventHandler.getSingleton().addEvent(thisNpc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				teleportCursor++;
				if (teleportCursor >= TeleportData.values.length) {
					teleportCursor = 0;
				}
				TeleportData newData = TeleportData.values[teleportCursor];

				thisNpc.teleport(newData.x, newData.y, 5, 8941);
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, TELEPORT_TICKS);
	}

}
