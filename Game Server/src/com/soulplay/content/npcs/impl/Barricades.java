package com.soulplay.content.npcs.impl;

import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;

public class Barricades {

	private static int[] SW_BARRICADE_INDEX = new int[20];
	private static int[] CW_BARRICADE_INDEX = new int[20];

	public static void setupBarricade(Client player, boolean inCw) {
		if (inCw) {
			if (!CastleWars.isGameStarted())
				return;
			if (player.soulWarsTeam == Team.NONE) {
				return;
			}
			if (CastleWars.isInDoorLocations(player.getX(), player.getY())) {
				player.sendMessage("You cannot set up barricade here.");
				return;
			}
			if (RSConstants.cwSaraSpawnRoom(player.getX(), player.getY(), player.getHeightLevel())
					|| RSConstants.cwZammySpawnRoom(player.getX(), player.getY(),
							player.getHeightLevel())) {
				player.sendMessage("You cannot set up barricade here.");
				return;
			}
			if ((player.getX() == 2422 && player.getY() == 3076)
					|| (player.getX() == 2426 && player.getY() == 3081)
					|| (player.getX() == 2377 && player.getY() == 3131)
					|| (player.getX() == 2373 && player.getY() == 3126)) {
				player.sendMessage("You cannot set up barricade here.");
				return;
			}
			if (RegionClip.getGameObject(4411, player.getX(), player.getY(), player.getZ()) != null) {
				player.sendMessage("You cannot setup a barricade here.");
				return;
			}
		} else {
			if (SoulWars.gameTimer < 7)
				return;
			if (!SoulWars.gameStarted)
				return;
			if (!SoulWars.soulWarsPlayers.contains(player))
				return;
		}
		if (RegionClip.isBarricadeHere(player.getX(), player.getY(), player.getZ())) {
			player.sendMessage("There already is a barricade here!");
			return;
		}
		if (maxBarricades(player, inCw)) {
			player.sendMessage("Your team already has too many barricades set up.");
			return;
		}
		if (ObjectManager.objectInSpotTypeTen(player.getX(), player.getY(),
				player.getZ())) {
			player.sendMessage("You cannot setup a barricade here.");
			return;
		}
		for (NPC n : player.getRegion().getNpcs()) {
			if (player.getX() == n.getX() && player.getY() == n.getY()) {
				player.sendMessage("There already is a barricade here!");
				return;
			}
		}
		Team team = player.soulWarsTeam; // this variable is used in castlewars too
		player.getItems().deleteItemInOneSlot(4053,
				player.getItems().getItemSlot(4053), 1);
//		player.startAnimation(827);
		switch (team) {
			case BLUE:
				NPC npc1 = NPCHandler.spawnNpc2(1532, player.absX, player.absY,
						player.heightLevel, 0);
				fillArray(npc1.getId(), inCw);
				break;
			case RED:
				NPC npc2 = NPCHandler.spawnNpc2(1534, player.absX, player.absY,
						player.heightLevel, 0);
				fillArray(npc2.getId(), inCw);
				break;
		default:
			break;
		}
		RegionClip.setBarricadeStatus(player.getX(), player.getY(), player.getZ(), true);
	}
	
	public static void fillArray(int index, boolean inCw) {
		if (inCw) {
			for (int i = 0; i < CW_BARRICADE_INDEX.length; i++) {
				if (CW_BARRICADE_INDEX[i] == 0) {
					CW_BARRICADE_INDEX[i] = index;
					return;
				}
			}
		} else {
			for (int i = 0; i < SW_BARRICADE_INDEX.length; i++) {
				if (SW_BARRICADE_INDEX[i] == 0) {
					SW_BARRICADE_INDEX[i] = index;
					return;
				}
			}
		}
		
	}
	
	public static void removeFromArray(int index, boolean inCw) {
		if (inCw) {
			for (int i = 0; i < CW_BARRICADE_INDEX.length; i++) {
				if (CW_BARRICADE_INDEX[i] == index) {
					CW_BARRICADE_INDEX[i] = 0;
					return;
				}
			}
		} else {
			for (int i = 0; i < SW_BARRICADE_INDEX.length; i++) {
				if (SW_BARRICADE_INDEX[i] == index) {
					SW_BARRICADE_INDEX[i] = 0;
					return;
				}
			}
		}
		
	}
	
	public static void killAllCades(boolean inCw) {
		if (inCw) {
			for (int index : CW_BARRICADE_INDEX) {
				if (index == 0) continue;
				NPC npc = NPCHandler.npcs[index];
				if (npc != null && !npc.isDead())
					npc.setDead(true);
			}
		} else {
			for (int index : SW_BARRICADE_INDEX) {
				if (index == 0) continue;
				NPC npc = NPCHandler.npcs[index];
				if (npc != null && !npc.isDead())
					npc.setDead(true);
			}
		}
	}
	
	public static boolean maxBarricades(Client player, boolean inCw) {
		Team team = player.soulWarsTeam; // this is used in castlewars too
		int sCount = 0;
		int zCount = 0;
		switch (team) {
			case BLUE:
				if (inCw) {
					for (int i = 0; i < CW_BARRICADE_INDEX.length; i++) {
						if (CW_BARRICADE_INDEX[i] == 0) {
							continue;
						}
						if (NPCHandler.npcs[CW_BARRICADE_INDEX[i]] == null) {
							CW_BARRICADE_INDEX[i] = 0;
							continue;
						}
						if (NPCHandler.npcs[CW_BARRICADE_INDEX[i]].npcType == 1532)
							sCount++;
						if (sCount == 10)
							return true;
					}	
				} else {
					for (int i = 0; i < SW_BARRICADE_INDEX.length; i++) {
						if (SW_BARRICADE_INDEX[i] == 0) {
							continue;
						}
						if (NPCHandler.npcs[SW_BARRICADE_INDEX[i]] == null) {
							SW_BARRICADE_INDEX[i] = 0;
							continue;
						}
						if (NPCHandler.npcs[SW_BARRICADE_INDEX[i]].npcType == 1532)
							sCount++;
						if (sCount == 10)
							return true;
					}
				}
				// return saraCount == 10;
				break;
			case RED :
				if (inCw) {
					for (int i = 0; i < CW_BARRICADE_INDEX.length; i++) {
						if (CW_BARRICADE_INDEX[i] == 0)
							continue;
						if (NPCHandler.npcs[CW_BARRICADE_INDEX[i]] == null) {
							CW_BARRICADE_INDEX[i] = 0;
							continue;
						}
						if (NPCHandler.npcs[CW_BARRICADE_INDEX[i]].npcType == 1534)
							zCount++;
						if (zCount == 10)
							return true;
					}
				} else {
					for (int i = 0; i < SW_BARRICADE_INDEX.length; i++) {
						if (SW_BARRICADE_INDEX[i] == 0)
							continue;
						if (NPCHandler.npcs[SW_BARRICADE_INDEX[i]] == null) {
							SW_BARRICADE_INDEX[i] = 0;
							continue;
						}
						if (NPCHandler.npcs[SW_BARRICADE_INDEX[i]].npcType == 1534)
							zCount++;
						if (zCount == 10)
							return true;
					}
				}
				// if(count == 10)
				// return true;
				break;
		default:
			return true;
		}
		// return count == 10;
		if (inCw) {
			for (int i = 0; i < CW_BARRICADE_INDEX.length; i++) {
				if (CW_BARRICADE_INDEX[i] == 0)
					return false;
			}
		} else {
			for (int i = 0; i < SW_BARRICADE_INDEX.length; i++) {
				if (SW_BARRICADE_INDEX[i] == 0)
					return false;
			}
		}
		
		return true;
	}

	public static void burnBarricade(final Client c, final NPC npc) {
	//		c.startAnimation(733);
			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
	
				int timer = 10;
	
				@Override
				public void execute(CycleEventContainer b) {
					if (npc.isDead() == true) {
						b.stop();
						return;
					}
					if (npc.transformId == npc.npcType || npc.transformId == -1) {
						b.stop();
						return;
					}
					if (timer == 0) {
						if (npc != null && !npc.isDead()) {
							npc.setDead(true);
							RegionClip.setBarricadeStatus(npc.getX(), npc.getY(), npc.getHeightLevel(), false);
						}
						b.stop();
					}
	
					if (timer > 0) {
						timer--;
					}
				}
	
				@Override
				public void stop() {
				}
			}, 1);
		}

	public static void explodeBarricade(Client c, NPC n, int itemSlot) {
		if (c.getItems().deleteItemInOneSlot(CastleWars.EXPLOSIVE_POTION, itemSlot, 1)) {
			n.setDead(true);
			int damg = n.getSkills().getLifepoints();
			n.dealDamage(new Hit(damg, 0, 2, c));
			n.startGraphic(Graphic.create(266));

			// cheaphax of instant barricade removal XD
			//			RegionClip.revertClipping(n.getX(), n.getY(), n.getHeightLevel());
			RegionClip.setBarricadeStatus(n.getX(), n.getY(), n.getHeightLevel(), false);
		}
	}
	
	public static void handleBarricadeObjectRemoval(int x, int y, int z) {
		RegionClip.setBarricadeStatus(x, y, z, false);
//		RSObject o = ObjectManager.getObjectByNewId(CastleWars.BARRICADE_OBJECT,
//				x, y, z);
//		if (o != null) {
//			o.setTick(0);
//		}
	}
	
}
