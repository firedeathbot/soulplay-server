package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

@NpcMeta(ids = {4353})
public class CaveHorrorNPC extends CombatNPC {

	public CaveHorrorNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new CaveHorrorNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		stopAttackDefault = true;
	}
	
	@Override
	public void startAttack(Player p) {
		 if (Misc.random(2) == 0) {
		    	meleeSwing(p, 24, 4234, 4);
		    } else if (Misc.random(2) == 1) {
		    	meleeSwing(p, 24, 4235, 4);
		    } else {
		    	meleeSwing(p, 24, 4237, 4);
		    }
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(4353, 55, 9, 80, 62);
		NPCAnimations.define(4353, 4233, 4232, 4234);
	}
}
