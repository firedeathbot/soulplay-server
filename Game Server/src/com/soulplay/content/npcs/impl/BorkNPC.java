package com.soulplay.content.npcs.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.soulplay.content.event.randomevent.Bork;
import com.soulplay.content.player.DonatorRank;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GroundItemObject;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;
import com.soulplay.util.soulplaydate.SoulPlayDate;

@NpcMeta(ids = { 7134 })
public class BorkNPC extends CombatNPC {

	private static final String ALWAYS_DROP_PREFIX = "bork_always_";
	private static final String PREROLL_DROP_PREFIX = "bork_preroll_";
	private static final String MAIN_DROP_PREFIX = "bork_main_";
	private static final String TERTIARY_DROP_PREFIX = "bork_tertiary_";
	public static final int ID = 7134;
	private List<NPC> legionOrcs = new ArrayList<>();

	public BorkNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new BorkNPC(slot, npcType);
	}

	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setMeleeDistance(4);
	}

	@Override
	public void startAttack(Player p) {
		if (Misc.random(10) == 1 && legionOrcs.size() < 10) {
			setAttackTimer(8);
			startAnimation(Animation.getOsrsAnimId(8757));

			Location spawnLocation = p.getCurrentLocation().copyNew();
			spawnLocation = findLocation(p.getCurrentLocation(), spawnLocation);
			if (spawnLocation == null) {
				return;
			}

			LegionOrcsNPC spawn = (LegionOrcsNPC) NPCHandler.spawnNpc(7135, spawnLocation.getX(), spawnLocation.getY(),
					spawnLocation.getZ());
			spawn.legionOrcs = legionOrcs;
			spawn.isAggressive = true;
			spawn.attack(p);
			legionOrcs.add(spawn);
		}
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	private Location findLocation(Location location, Location last) {
		if (location == null || last == null) {
			return null;
		}

		for (int i = 0; i < 10; i++) {
			Location gen = location.transform(Direction.getRandom(), 1 - Misc.random(2));
			if (gen != null && !gen.matches(location) && !last.matches(gen)
					&& RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}
		return null;
	}

	@Override
	public void onDeath() {
		super.onDeath();

		Bork.borkAlive = false;
		int length = legionOrcs.size();
		if (length <= 0) {
			return;
		}

		for (int i = 0; i < length; i++) {
			NPC npc = legionOrcs.get(i);
			npc.nullNPC();
		}
	}
}
