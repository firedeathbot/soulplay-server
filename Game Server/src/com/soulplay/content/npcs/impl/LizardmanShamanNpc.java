package com.soulplay.content.npcs.impl;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 107744, 107745 })
public class LizardmanShamanNpc extends CombatNPC {

	private List<NPC> spawns = new ArrayList<>();

	public LizardmanShamanNpc(int slot, int npcType) {
		super(slot, npcType);

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		getDistanceRules().setMeleeDistance(8);
		getDistanceRules().setFollowDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		int roll = Misc.random(8);

		if (roll == 0 && RegionClip.canStandOnSpot(this, p.absX, p.absY)) {
			setAttackTimer(8);
			startAnimation(Animation.osrs(7152));
			setRetaliates(false);

			Location jumpLocation = p.getCurrentLocation().copyNew();
			pulse(() -> {
				NPCHandler.trueTeleport(this, jumpLocation);
				startAnimation(Animation.osrs(6946));
				pulse(() -> {
					setRetaliates(true);
					attack(p);
					face(p);

					int dist = getCenterLocation().getDistanceInt(p.getCurrentLocation());
					if (dist <= 1) {
						p.dealDamage(new Hit(Misc.random(20, 25)));
					}
				});
			}, 4);
		} else if (roll == 1) {
			setAttackTimer(8);
			startAnimation(Animation.osrs(7157));

			Location spawnLocation = p.getCurrentLocation().copyNew();
			for (int i = 0; i < 3; i++) {
				spawnLocation = findLocation(p.getCurrentLocation(), spawnLocation);
				if (spawnLocation == null) {
					continue;
				}

				LizardmanSpawnNpc spawn = (LizardmanSpawnNpc) NPCHandler.spawnNpc(106768, spawnLocation.getX(), spawnLocation.getY(), spawnLocation.getZ());
				spawn.targetPlayer = p;
				spawns.add(spawn);
			}
		} else if (roll == 2 || roll == 3) {
			setAttackTimer(8);
			startAnimation(Animation.osrs(7193));
			pulse(() -> {
				Location startLocation = getCenterLocation();
				Location victimCenter = p.getCenterLocation().copyNew();
				Projectile projectile = Projectile.create(startLocation, victimCenter, Graphic.getOsrsId(1293));
				projectile.setStartDelay(30);
				projectile.setStartHeight(110);
				projectile.setStartOffset(128);
				pulse(() -> {
					Graphic.osrs(1294).send(victimCenter);
					if (p.getCenterLocation().matches(victimCenter)) {
						p.pulse(() -> {
							int damage = Misc.random(30);
							p.dealDamage(new Hit(damage, 2, -1));
							p.getDotManager().applyPoison(damage, this);
						});
					}
				}, 2);
			}, 2);
		} else if (roll == 4 && getCenterLocation().getDistance(p.getCurrentLocation()) <= getSize()) {
			setAttackTimer(6);
			startAnimation(Animation.osrs(7192));
			meleeSwing(p, 31, Animation.getOsrsAnimId(8499));
		} else {
			setAttackTimer(6);
			startAnimation(Animation.osrs(7193));
			pulse(() -> {
				Projectile projectile = Projectile.create(this, p, Graphic.getOsrsId(1291));
				projectile.setStartDelay(30);
				projectile.setStartHeight(120);
				projectile.setStartOffset(128);

				int damage = projectileSwing(p, projectile, 21, 2, null, CombatType.RANGED);
				p.getDotManager().applyPoison(damage, this);
			}, 2);
		}
	}

	@Override
	public void defineConfigs() {
		int[] ids = { 107744, 107745 };
		for (int id : ids) {
			NpcStats.define(id, 150, 31, 186, 186);
			NPCAnimations.define(id, Animation.getOsrsAnimId(7196), Animation.getOsrsAnimId(7194), -1);
		}
	}

	private Location findLocation(Location location, Location last) {
		if (location == null || last == null) {
			return null;
		}

		for (int i = 0; i < 10; i++) {
			Location gen = location.transform(Direction.getRandom(), 1 - Misc.random(2));
			if (gen != null && !gen.matches(location) && !last.matches(gen) && RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}

		return null;
	}

	@Override
	public void onDeath() {
		super.onDeath();

		int length = spawns.size();
		if (length <= 0) {
			return;
		}

		for (int i = 0; i < length; i++) {
			NPC npc = spawns.get(i);
			npc.nullNPC();
		}
	}

}
