package com.soulplay.content.npcs.impl;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.util.Misc;

public class HalloweenDungeonNpc {
	
	private enum MoveData {
		SPOT_1(2876, 9805), 
		SPOT_2(2891, 9786),
		SPOT_3(2696, 9561),
		SPOT_4(2776, 10005),
		SPOT_5(3171, 9570),
		SPOT_6(3132, 9913),
		SPOT_7(3243, 9881),
		SPOT_8(3507, 9493),
		SPOT_9(2994, 9555),
		SPOT_10(3054, 9777),
		SPOT_11(1752, 5352),
		SPOT_12(3221, 9378),
		SPOT_14(3179, 5480);

		public static final MoveData[] values = values();
		private final int x, y;

		MoveData(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final int NPC_ID = 5572;
	private static final int TELEPORT_TICKS = 1000;
	private static NPC thisNpc;
	private static int teleportCursor;

	public static void spawnHalloweenDungeonNpc() {
		if (thisNpc != null) {
			return;
		}

		teleportCursor = Misc.randomNoPlus(MoveData.values.length);
		MoveData data = MoveData.values[teleportCursor];
		
		int npcSlot = Misc.findFreeNpcSlot();
		
		if (npcSlot != -1) {
			thisNpc = new NPC(npcSlot, NPC_ID);
			NPCHandler.spawnNpc(thisNpc, data.x, data.y, 0, 0);
		} else {
			return;
		}
		
		thisNpc.setWalksHome(false);

		CycleEventHandler.getSingleton().addEvent(thisNpc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				teleportCursor++;
				if (teleportCursor >= MoveData.values.length) {
					teleportCursor = 0;
				}
				MoveData newData = MoveData.values[teleportCursor];

				thisNpc.moveNpc(newData.x, newData.y, 0);
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, TELEPORT_TICKS);
	}

}
