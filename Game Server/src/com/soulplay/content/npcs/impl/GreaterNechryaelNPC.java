package com.soulplay.content.npcs.impl;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = {107278})
public class GreaterNechryaelNPC extends CombatNPC {
	
	private List<NPC> deathSpawns = new ArrayList<>();

	public GreaterNechryaelNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new GreaterNechryaelNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}
	
	@Override
	public void startAttack(Player p) {
		if (Misc.random(5) == 1 && deathSpawns.size() < 2) {
			setAttackTimer(8);
			startAnimation(Animation.getOsrsAnimId(1529));

			Location spawnLocation = p.getCurrentLocation().copyNew();
			spawnLocation = findLocation(p.getCurrentLocation(), spawnLocation);
			if (spawnLocation == null) {
				return;
			}

			DeathSpawnNPC spawn = (DeathSpawnNPC) NPCHandler.spawnNpc(100010, spawnLocation.getX(), spawnLocation.getY(), spawnLocation.getZ());
			spawn.startAttack(p);
			spawn.isAggressive = true;
			deathSpawns.add(spawn);
		}	
	}
	
	private Location findLocation(Location location, Location last) {
		if (location == null || last == null) {
			return null;
		}

		for (int i = 0; i < 10; i++) {
			Location gen = location.transform(Direction.getRandom(), 1 - Misc.random(2));
			if (gen != null && !gen.matches(location) && !last.matches(gen) && RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}
		return null;
	}



	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(107278, 205, 21, 197, 85);
		NPCAnimations.define(107278, Animation.getOsrsAnimId(1530), -1, Animation.getOsrsAnimId(1528));
	}
	
	@Override
	public void onDeath() {
		super.onDeath();

		int length = deathSpawns.size();
		if (length <= 0) {
			return;
		}

		for (int i = 0; i < length; i++) {
			NPC npc = deathSpawns.get(i);
			npc.nullNPC();
		}
	}
	
}
