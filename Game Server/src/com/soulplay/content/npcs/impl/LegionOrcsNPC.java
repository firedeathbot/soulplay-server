package com.soulplay.content.npcs.impl;

import java.util.List;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = { 7135 })
public class LegionOrcsNPC extends CombatNPC {
	
	public List<NPC> legionOrcs;
	
	
	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new LegionOrcsNPC(slot, npcType);
	}

	public LegionOrcsNPC(int slot, int npcType) {
		super(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		walksOnTopOfNpcs = true;
		setCanWalk(true);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
	}
	
	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}

	@Override
	public void onDeath() {
		super.onDeath();

		if (legionOrcs == null) {
			return;
		}
		
		legionOrcs.remove(this);
	}
	
}
