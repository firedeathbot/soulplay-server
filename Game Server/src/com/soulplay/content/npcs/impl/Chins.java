package com.soulplay.content.npcs.impl;

import com.soulplay.content.player.skills.hunting.trap.BoxTrap;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Chins {

	private final NPC npc;
	
	private Location walkTo;
	
	private int tick;
	
	public Chins(NPC n) {
		this.npc = n;
	}
	
	public static final int TRAP_OPEN = 1234;
	public static final int TRAP_CLOSED_EMPTY = 5432;
	
	public void processChinchompas() {
		if (walkTo == null) { // grab nearby traps
			if (Misc.random(7) <= 3) {
				for (RSObject o : npc.getCurrentRegion().getRSObjects()) {
					if (o.getHeight() == npc.getHeightLevel() && o.getTick() > 1 && o.getNewObjectId() == TRAP_OPEN && Misc.random(1) == 0) { //TODO: add trap id
						walkTo = Location.create(o.getX(), o.getY(), o.getHeight());
					}
				}
			}
		} else { // process the walking
			if (!npc.getCurrentLocation().isWithinDistance(walkTo, 1)) {
				npc.walkToLocation(walkTo);
			} else { // in location so try to capture
				tick++;
				if (tick == 2) {
					npc.startAnimation(123); //TODO: add animation of chins attacking trap
				} else if (tick == 4) {
					if (Misc.random(3) > 0) {
						
					} else { // failed catching
						RSObject trap = getTargettedTrap();
						if (trap != null) {
							trap.setTick(0);
							ObjectManager.addObject(new RSObject(BoxTrap.SETTING.getFailId(), trap.getX(), trap.getY(), trap.getHeight(), trap.getFace(), trap.getType(), -1, 40+Misc.random(20)));
						}
					}
				}
			}
		}
	}
	
	
	private RSObject getTargettedTrap() {
		for (RSObject o : npc.getCurrentRegion().getRSObjects()) {
			if (o.getHeight() == walkTo.getZ() && o.getNewObjectId() == TRAP_OPEN && o.getX() == walkTo.getX() && o.getY() == walkTo.getY() && o.getTick() > 1)
				return o;
		}
		return null;
	}
	
}
