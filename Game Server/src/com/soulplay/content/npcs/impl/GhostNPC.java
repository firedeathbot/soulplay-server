package com.soulplay.content.npcs.impl;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

@NpcMeta(ids = {107263, 107264})
public class GhostNPC extends CombatNPC {

	public GhostNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new GhostNPC(slot, npcType);
	}
	
	@Override
	public void onSpawn() {
		getSkills().setStaticLevel(Skills.ATTACK, 13);
		getSkills().setStaticLevel(Skills.STRENGTH, 13);
		getSkills().setStaticLevel(Skills.DEFENSE, 18);
		walkUpToHit = true;
		setAggroRadius(8);
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void setWalkingHome(boolean walkingHome) {
		super.setWalkingHome(walkingHome);
		if (walkingHome)
			walkUpToHit = false;
	}
	
	@Override
	public void defineConfigs() {
		NpcStats.define(107263, 25, 2, 13, 18);
		NPCAnimations.define(107263, 5534, 5533, 5532);
		NpcStats.define(107264, 25, 2, 13, 18);
		NPCAnimations.define(107264, 5534, 5533, 5532);
	}
}
