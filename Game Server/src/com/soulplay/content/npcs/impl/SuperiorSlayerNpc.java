package com.soulplay.content.npcs.impl;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.skills.slayer.Assignment;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.util.Misc;

public class SuperiorSlayerNpc {
	
	private enum SuperiorType {

		  CRUSHING_HAND(107388, new int[] { 1648 }),   //DONE
		  CHASM_CRAWLER(107389, new int[] { 1600 }),  //DONE
		  SCREAMING_BANSHEE(107390, new int[] { 1612 }),   //DONE
		  SCREAMING_TWISTED_BANSHEE(107391, new int[] { 107272 }),
		  GIANT_ROCKSLUG(107392, new int[] { 1631, 1632 }),   //DONE
		  COCKATHRICE(107393, new int[] { 1620 }),   //DONE
		  FLAMING_PYRELORD(107394, new int[] { 7377, 7378, 9477 }), //DONE
		  //INFERNAL_PYRELORD(107395, new int[] { 1648 }),
		  MONSTROUS_BASILISK(107395, new int[] { 1616, 1617 }),  //DONE  
		  MALEVOLENT_MAGE(107396, new int[] { 1643, 1644, 1645, 1646, 1647 }),  //DONE
		  INSATIABLE_BLOODVELD(107397, new int[] { 1618, 1619 }),   //DONE
		  INSATIABLE_MUTATED_BLOODVELD(107398, new int[] { 107276, 7643, 7642 }), //DONE
		  VITREOUS_JELLY(107399, new int[] { 1637, 1638, 1639, 1640, 1641, 1642, 7459, 7460, 8599, 8617 }), //DONE
		  VITREOUS_WARPED_JELLY(107400, new int[] { 107277 }), //DONE
		  //SPIKED_TUROTH(107388, new int[] { 1648 }),
		  CAVE_ABOMINATION(107401, new int[] { 4353, 4354, 4355, 4356, 4357 }), //DONE
		  ABHORRENT_SPECTRE(107402, new int[] { 1604, 1605, 1606, 1607, 7801, 7802, 7803, 7804}),
		  REPUGNANT_SPECTRE(107403, new int[] { 107279 }),
		  BASILISK_SENTINEL(109258, new int[] { 1616, 1617 }),
		  //SHADOW_WYRM(107388, new int[] { 1648 }),
		  CHOKE_DEVIL(107404, new int[] { 100498 }),
		  KING_KURASK(107405, new int[] { 1608, 1609 }),
		  MARBLE_GARGOYLE(107407, new int[] { 1610, 1827, 9087 }),
		  NECHRYARCH(107411, new int[] { 1613, 10702, 100008, 100011, 107278 }),
		  //GUARDIAN_DRAKE(107388, new int[] { 1648 }),
		  GREATER_ABYSSAL_DEMON(107410, new int[] { 1615, 9086, 100124, 100415, 100416, 107241 }),
		  NIGHT_BEAST(107409, new int[] { 2783, 104005, 107250 }),
		  NUCLEAR_SMOKE_DEVIL(107406, new int[] { 100499 }),
		  //COLOSSAL_HYDRA(107388, new int[] { 1648 }),
		  ;
	 
	        public static final Map<Integer, SuperiorType> values = new HashMap<>();
	        private final int superiorId;
	        private final int[] npcIds;
	 
	        private SuperiorType(int superiorId, int[] npcIds) {
	            this.superiorId = superiorId;
	            this.npcIds = npcIds;
	        }

	        static {
	            for (SuperiorType value : values()) {
	                for (int npcId : value.npcIds) {
	                    values.put(npcId, value);
	                }
	            }
	        }
	        
	    public int getSuperiorId() {
	        return superiorId;
	    }
	}

	public static void handleSuperior(Client p, NPC n) {
		 SuperiorType superior = SuperiorType.values.get(n.npcType);
		 if (superior == null) {
			 return;
		 }

		 SlayerTask slayerTask = p.getSlayerTask();
		 if (slayerTask == null) {
			 return;
		 }

		 Assignment assignemnt = slayerTask.getAssignment();
		 if (assignemnt == null) {
			 return;
		 }

		 if (Misc.random(0) == 0) {
			 for (int target : assignemnt.getIds()) {
			      if (n.npcType == target) {
			    	  spawnSuperior(p, n, superior);
			    	  return;
				  }
			 }
	    }
	}

	public static void spawnSuperior(Client p, NPC n, SuperiorType superiorType) {
		if (p.superior) {
			return;
		}

		int spawnX = p.getX(), spawnY = p.getY();
		loop: for (int x = 0; x < 2; x++) {
			for (int y = 0; y < 2; y++) {
				if (x == 0 && y == 0) {
					continue;
				}
				if (PathFinder.canMoveInStaticRegion(p.getX(), p.getY(), p.getHeightLevel(), x, y)) {
					spawnX += x;
					spawnY += y;
					break loop;
				}
			}
		}

		NPC superior = NPCHandler.spawnNpc(p, superiorType.getSuperiorId(), spawnX, spawnY, p.getHeightLevel(), 10, false, false);
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (p.disconnected) {
					container.stop();
					return;
				}
				container.stop();
			}

			@Override
			public void stop() {
				if (superior.isActive) {
					superior.nullNPC();
				}

				p.superior = false;
			}
		}, 600);

		p.superior = true;
		p.sendMessage("<col=ef1020>A superior foe has appeared...");
	}
    
}