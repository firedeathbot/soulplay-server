package com.soulplay.content.npcs.impl;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.util.Misc;

public class HalloweenCityNpc {
	
	private enum MoveData {
		SPOT_1(3101, 3504), 
		SPOT_2(3213, 3428),
		SPOT_3(3185, 3440),
		SPOT_4(3252, 3428),
		SPOT_5(2725, 3491),
		SPOT_6(2809, 3440),
		SPOT_7(2952, 3377),
		SPOT_8(3013, 3357),
		SPOT_9(3036, 3354),
		SPOT_10(2660, 3310),
		SPOT_11(2653, 3283),
		SPOT_12(3094, 3243),
		SPOT_13(2611, 3092),
		SPOT_14(2088, 3913),
		SPOT_15(2344, 3802);

		public static final MoveData[] values = values();
		private final int x, y;

		MoveData(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final int NPC_ID = 8713;
	private static final int TELEPORT_TICKS = 1000;
	private static NPC thisNpc;
	private static int teleportCursor;

	public static void spawnHalloweenCityNpc() {
		if (thisNpc != null) {
			return;
		}

		teleportCursor = Misc.randomNoPlus(MoveData.values.length);
		MoveData data = MoveData.values[teleportCursor];
		
		int npcSlot = Misc.findFreeNpcSlot();
		
		if (npcSlot != -1) {
			thisNpc = new NPC(npcSlot, NPC_ID);
			NPCHandler.spawnNpc(thisNpc, data.x, data.y, 0, 0);
		} else {
			return;
		}
		
		thisNpc.setWalksHome(false);

		CycleEventHandler.getSingleton().addEvent(thisNpc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				teleportCursor++;
				if (teleportCursor >= MoveData.values.length) {
					teleportCursor = 0;
				}
				MoveData newData = MoveData.values[teleportCursor];

				thisNpc.moveNpc(newData.x, newData.y, 0);
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, TELEPORT_TICKS);
	}


}
