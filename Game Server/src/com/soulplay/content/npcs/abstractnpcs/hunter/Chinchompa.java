package com.soulplay.content.npcs.abstractnpcs.hunter;

import com.soulplay.Server;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.hunterlesik.HunterPrey;
import com.soulplay.content.player.skills.hunterlesik.box.BoxTrapEnum;
import com.soulplay.content.player.skills.hunterlesik.box.BoxTrapObject;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = {3606, 5079, 5080})
public class Chinchompa extends AbstractNpc {
	
	public static final int TRAP_OBJ_ID = 19187;
	
	public static final Animation CATCH_ANIM = new Animation(5184);
	public static final Animation FAIL_CATCH_ANIM = new Animation(5185);

	private int ticks = 0;
	
	private int walkingSteps = 0;
	
	private Direction walkingDir;
	
	private RSObject trap;
	
	private HunterPrey prey;
	
	private int minCatchRate = 5;
	private int maxLevelCatchRate = 267;
	
	public Chinchompa(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();
		
		prey = HunterPrey.forId(npcType);
		
		switch (npcType) {
		case 5080:
		case 3606:
			minCatchRate = -77;
			maxLevelCatchRate = 227;
			break;
		}
		
		isAggressive = false;
		
		stopAttack = true;
		
		setWalkDistance(15);
	}
	
	@Override
	public int dealDamage(Hit hit) {
		hit.setDamage(2);
		return super.dealDamage(hit);
	}
	
	@Override
	public void onDeath() {
		super.onDeath();

		forceChat("Squeak!");
		for (Player p : Server.getRegionManager().getLocalPlayersOptimized(this)) {
			if (getCurrentLocation().isWithinDistance(p.getCurrentLocation(), 1)) {
				p.dealTrueDamage(2, 0, null, this);
			}
		}
		actionTimer = 1;
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new Chinchompa(slot, npcType);
	}
	
	@Override
	public void process() {
		super.process();
		
		if (isLockActions()) {
			moveX = moveY = 0;
			return;
		}
		
		if (isDead())
			return;
		
		ticks++;
		
		if (trap == null && ticks % 2 == 0) { // get random object nearby
			if (Misc.randomBoolean(1)) {
				trap = getRandomTrap();
				
				if (trap != null) {
					walkingSteps = 0;
					walkingDir = null;
					turnNpc(trap.getX(), trap.getY());
					PathFinder.findRoute(this, trap.getX(), trap.getY(), false, 1, 1);
				}
			}
		}
		
		if (trap == null) { //randomwalk
			if (walkingSteps == 0 && ticks % 2 == 0) {
				if (Misc.randomBoolean(4)) {
					walkingDir = null;
				} else {
					walkingDir = Direction.getRandom();
					walkingSteps = Misc.random(1, 4);
				}
				//forceChat("randomized walk"  + Misc.random(1000000));
			}
			
			if (walkingSteps > 0 && walkingDir != null) {
				if (outsideWalkRadius(getX()+walkingDir.getStepX(), getY()+walkingDir.getStepY())) {
					walkingSteps = 0;
					walkingDir = null;
				} else {
					moveX = walkingDir.getStepX();
					moveY = walkingDir.getStepY();
					walkingSteps--;
				}
			}
		} else { // walk to trap!
			if (!trap.isActive() || trap.getNewObjectId() != BoxTrapObject.OBJECT_ID || trap.getTick() < 2) {
				trap = null;
				resetFace();
			} else {
				
				//stopped walking, so check for the trap!
				if (!getWalkingQueue().isMoving()) {
					if (Misc.distanceToPoint(trap.getX(), trap.getY(), getX(), getY()) > 1) {
						trap = null;
						resetFace();
						return; // no more code underneath will be active
					}
					
					handleTrap();
				}
				
			}
		}
	}
	
	private void handleTrap() {
		BoxTrapObject castTrap = (BoxTrapObject) trap;
		
		final boolean success = successfulCatch(castTrap.getPlayerLevel());
		
		castTrap.trapFailed = !success;
		
		if (success)
			createCapturedObject(castTrap, trap.getX(), trap.getY(), trap.getHeight(), trap.getObjectOwnerLong());
		else
			createFailedObject(castTrap, trap.getX(), trap.getY(), trap.getHeight(), trap.getObjectOwnerLong());
		
		trap = null;
	}
	
	private void createCapturedObject(BoxTrapObject box, int x, int y, int z, long playerNameLong) {
		final BoxTrapEnum trapEnum = BoxTrapEnum.VALUES[npcType == 5080 ? 1 : 0];
		
		box.setPrey(prey);
		
		Location boxLocation = Location.create(x, y, z);
		
		Direction dir = Direction.getLogicalDirection(boxLocation, getCurrentLocation());
		
		int catchObjectId = trapEnum.getObjectByDir()[dir.toInteger()];
		int trappedObjectId = trapEnum.getTrappedId();
		
		final Chinchompa chinNpc = this;
		
		lockActions(3);
		
		getWalkingQueue().reset();
		
		Server.getTaskScheduler().schedule(1, () -> {
			if (!box.trapInUse) {
				
				chinNpc.startAnimation(CATCH_ANIM);
				
				chinNpc.nullNPC();
				
				box.trapInUse = true;
				
				//reset the timer on the object
				box.setTick(200);

				ObjectManager.transformObjectNoClippingChange(box, trappedObjectId, false);
				
				Server.getTaskScheduler().schedule(1, () -> {
					if (box.isActive()) {
						ObjectManager.displayObject(x, y, z, 10, 0, catchObjectId);

						Server.getTaskScheduler().schedule(2, () -> {
							if (box.isActive())
								ObjectManager.displayObject(x, y, z, 10, 0, trappedObjectId);
						});
					}

				});
				
			}
		});
		
	}
	
	private void createFailedObject(BoxTrapObject box, int x, int y, int z, long playerNameLong) {
		
		box.trapInUse = true;
		
		lockActions(2);
		
		startAnimation(FAIL_CATCH_ANIM);
		
		//reset the timer on the object
		box.setTick(200);

		ObjectManager.transformObjectNoClippingChange(box, BoxTrapObject.BROKEN_OBJECT_ID, false);

		ObjectManager.displayObject(x, y, z, 10, 0, BoxTrapObject.BROKEN_OBJECT_WITH_ANIM);

		//ObjectManager.displayObject(x, y, z, 10, 0, getID);//get object 19193+ defending on chimp and direction

		Server.getTaskScheduler().schedule(2, () -> {
			ObjectManager.displayObject(x, y, z, 10, 0, BoxTrapObject.BROKEN_OBJECT_ID);
		});
			
	}
	
	private RSObject getRandomTrap() {
		for (RSObject o : getCurrentRegion().getRSObjects()) {
			
			if (!o.isActive() || o.getTick() < 5) continue;
			
			if (o.getNewObjectId() == BoxTrapObject.OBJECT_ID) {
				
				BoxTrapObject box = (BoxTrapObject) o;
				
				if (box.getPlayerLevel() < prey.getLevelRequired() || outsideWalkRadius(o.getX(), o.getY()) || !withinScanningRadius(o.getX(), o.getY())) {
					continue;
				}
				
				if (Misc.randomBoolean(1)) {
					return o;
				}
			}
		}
		return null;
	}
	
	private boolean successfulCatch(int level) {
		int random = Skills.randomBaseChance();
		int rate = Misc.interpolate(minCatchRate, maxLevelCatchRate, 1, 99, level);
//		System.out.println("random:"+ random + " rate:"+ rate + " level:"+level);
		return random < rate;
	}
	
	private boolean outsideWalkRadius(int otherX, int otherY) {
		return getDistance(makeX, otherX, makeY, otherY) > getWalkDistance();
	}
	
	private boolean withinScanningRadius(int otherX, int otherY) {
		return getDistance(getX(), otherX, getY(), otherY) <= 3;
	}
	
}
