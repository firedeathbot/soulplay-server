package com.soulplay.content.announcement;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class AnnouncementHandler {

	public static Announcement currentAnnouncement = null;

	public static void removeAnnouncement() {
		currentAnnouncement = null;
		updateAll();
	}

	public static void announce(String message) {
		currentAnnouncement = new Announcement(message);
		updateAll();
	}

	public static void updateAll() {
		for (Player p : PlayerHandler.players) {
			if (p == null || !p.isActive) {
				continue;
			}

			updateAnnouncement(p);
		}
	}

	public static void updateAnnouncement(Player player) {
		if (currentAnnouncement == null) {
			player.getPacketSender().updateAnnouncement("");
			return;
		}

		player.getPacketSender().updateAnnouncement(currentAnnouncement.getMessage());
	}

}
