package com.soulplay.content.announcement;

public class Announcement {

	private final String message;

	public Announcement(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
