package com.soulplay.content.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public enum ArtifactData {

	ANCIENT_STATUETTE(14876, 25_000_000),
	SEREN_STATUETTE(14877, 20_000_000),
	ARMADYL_STATUETTE(14878, 5_000_000),
	ZAMORAK_STATUETTE(14879, 5_000_000),
	SARADOMIN_STATUETTE(14880, 5_000_000),
	BANDOS_STATUETTE(14881, 5_000_000),

	ARMADYL_TOTEM(14884, 1_500_000),
	ZAMORAK_MEDALION(14885, 1_500_000),
	SARADOMIN_CARVING(14886, 1_500_000),
	BANDOS_SCRIMSHAW(14887, 1_500_000),
	SARADOMIN_AMPHORA(14888, 1_500_000),

	RUBY_CHALICE(14882, 500_000),
	GUTHIXIAN_BRAZIER(14883, 750_000),
	ANCIENT_BRIDGE(14889, 900_000),
	BRONZE_DRAGON_CLAW(14890, 1_200_000),
	THIRD_AGE_CARAFE(14891, 1_000_000),
	BROKEN_STATUE_HEADDRESS(14892, 250_000);

	public static final Map<Integer, ArtifactData> values = new HashMap<>();
	private final int itemId, cost;

	ArtifactData(int itemId, int cost) {
		this.itemId = itemId;
		this.cost = cost;
	}

	public int getItemId() {
		return itemId;
	}

	public int getCost() {
		return cost;
	}

	static {
		for (ArtifactData data : values()) {
			values.put(data.getItemId(), data);
		}	
	}

	public static void load() {
		/* empty */
	}

	public static void sellArtifacts(Player player) {
		player.getPA().closeAllWindows();

		long totalCost = 0;
		List<Item> items = new ArrayList<>();

		for (int i = 0, length = player.playerItems.length; i < length; i++) {
			int itemId = player.playerItems[i] - 1;
			if (itemId == -1) {
				continue;
			}

			ArtifactData data = values.get(itemId);
			if (data == null) {
				continue;
			}

			items.add(new Item(itemId, 1, i));
			totalCost += data.cost;
		}

		if (totalCost <= 0) {
			player.sendMessage("You have no emblems to exchange.");
			return;
		}

		if (totalCost >= Integer.MAX_VALUE) {
			player.sendMessage("Your total emblem value is too big.");
			return;
		}

		if (player.getItems().addItem(995, (int) totalCost, true)) {
			for (Item item : items) {
				player.getItems().deleteItemInSlot(item.getId(), item.getSlot());
			}

			player.sendMessage("You've exchanged your emblems for " + Misc.format(totalCost) + " gp.");
			player.getAchievement().exhangeArtifact();
		}
	}

}
