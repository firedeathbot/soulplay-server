package com.soulplay.content.player;

public class ChatMessageTypes {

	public static final int GAME_MESSAGE = 0;
	public static final int PUBLIC_MESSAGE_STAFF = 1;
	public static final int PUBLIC_MESSAGE_PLAYER = 2;
	public static final int PRIVATE_MESSAGE_RECEIVED_PLAYER = 3;
	public static final int TRADE_REQUEST = 4;
	public static final int PRIVATE_MESSAGE_RECEIVED = 5;
	public static final int PRIVATE_MESSAGE_SENT = 6;
	public static final int PRIVATE_MESSAGE_RECEIVED_STAFF = 7;
	public static final int CHALLENGE_REQUEST = 8;
	public static final int DUNG_PARTY_REQUEST = 10;
	public static final int GAMBLE_REQUEST = 11;
	public static final int CLAN_MESSAGE = 13;
	public static final int ADVERT_MESSAGE = 14;
	public static final int YELL_MESSAGE = 18;
	public static final int GAME_MESSAGE_SPAM = 19;

}
