package com.soulplay.content.player.achievement;

public class AchievType {

	public static final int EASY = 0;
	
	public static final int MEDIUM = 1;
	
	public static final int HARD = 2;
	
	public static final int ELITE = 3;
	
}
