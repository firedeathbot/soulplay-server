package com.soulplay.content.player.achievement;

import com.soulplay.Config;
import com.soulplay.config.WorldType;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.interfacedata.BitShifting;

public class Achievement {

	/**
	 * Tells the achievementComplete method what the name of the achievement you
	 * completed was.
	 */
	public String achievementName;

	private Player c;

	public Achievement(Player c) {
		this.c = c;
	}

	/**
	 * Updates the AchievementTab to display users status on an achievement
	 */
	public void achivementsCompleted() {
		c.getPacketSender().sendFrame126(
				"Achievements Completed: " + c.achievementsCompleted + "/115",
				28023);
		if (c.achievementsCompleted == 105) {
			Titles.completeAchievements(c);
			// if (!c.getItems().addItem(10867, 1)) {
			// ItemHandler.createGroundItem(c, 10867, c.getX(), c.getY(),
			// c.heightLevel, 1);
			// }
			// if (!c.getItems().addItem(10868, 1)) {
			// ItemHandler.createGroundItem(c, 10868, c.getX(), c.getY(),
			// c.heightLevel, 1);
			// }
		}
	}
	
	public void updateAchievementTab() {

		// correct
		for (Achievs ach : Achievs.values) {
			if (c.getAchievInt(ach.ordinal()) == 0) {
				setNotStarted(ach.getStringId());
			} else if (c.getAchievInt(ach.ordinal()) >= ach.getCompletedAmount()) {
				setCompletedTask(ach.getStringId());
			} else {
				setInProgress(ach.getStringId());
			}
		}

	}
	
	private static final String COMPLETED_MSG = "<img=6>You have completed the achievement %s.";
	private static final String PROGRESS_MSG = "You made progress towards <col=ff0000>%s.";
	
	public void increaseProgress(Achievs achiev) {
		increaseProgress(achiev, 1);
	}
	
	public void increaseProgress(Achievs achiev, int amount) {
		if (c.getAchievInt(
				achiev.ordinal()) < achiev.getCompletedAmount()) {
			c.setAchievInt(achiev.ordinal(), Math.min(achiev.getCompletedAmount(), (c.getAchievInt(achiev.ordinal()) + amount)));
			setInProgress(achiev.getStringId());

			if (c.getAchievInt(achiev.ordinal()) == achiev.getCompletedAmount()) {
				setCompletedTask(achiev.getStringId());
				c.sendMessage(String.format(COMPLETED_MSG, achiev.toString().replace("_", " ")));
				switch (achiev.getTaskType()) {
				case AchievType.EASY:
					rewardEASY();
					break;
				case AchievType.MEDIUM:
					rewardMEDIUM();
					break;
				case AchievType.HARD:
					rewardHARD();
					break;
				case AchievType.ELITE:
					rewardELITE();
					break;
				}
			} else if (c.getAchievInt(achiev.ordinal()) < 2) {
				c.sendMessage(String.format(PROGRESS_MSG, achiev.toString().replace("_", " ")));
			}
		}
	}

	public void barbLaps() {
		if (c.getAchievInt(Achievs.BARBLAPS_10.ordinal()) < Achievs.BARBLAPS_10
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.BARBLAPS_10.ordinal(),
					c.getAchievInt(Achievs.BARBLAPS_10.ordinal()) + 1);
			setInProgress(Achievs.BARBLAPS_10.getStringId());
			if (c.getAchievInt(Achievs.BARBLAPS_10.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Complete "
								+ Achievs.BARBLAPS_10.getCompletedAmount()
								+ " Barbarian Laps");
			}
			if (c.getAchievInt(
					Achievs.BARBLAPS_10.ordinal()) == Achievs.BARBLAPS_10
							.getCompletedAmount()) {
				setCompletedTask(Achievs.BARBLAPS_10.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Complete "
								+ Achievs.BARBLAPS_10.getCompletedAmount()
								+ " Barbarian Laps");
				rewardMEDIUM();
			}
		}
	}

	public void buildDemonicThrone() {
		if (c.getAchievInt(Achievs.BUILD_DEMONIC_THRONE
				.ordinal()) < Achievs.BUILD_DEMONIC_THRONE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.BUILD_DEMONIC_THRONE.ordinal(),
					c.getAchievInt(Achievs.BUILD_DEMONIC_THRONE.ordinal()) + 1);
			setCompletedTask(Achievs.BUILD_DEMONIC_THRONE.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Build A Demonic Throne");
			rewardELITE();
		}
	}

	public void buildGildedAtlar() {
		if (c.getAchievInt(Achievs.BUILD_GILDED_ALTAR
				.ordinal()) < Achievs.BUILD_GILDED_ALTAR.getCompletedAmount()) {
			c.setAchievInt(Achievs.BUILD_GILDED_ALTAR.ordinal(),
					c.getAchievInt(Achievs.BUILD_GILDED_ALTAR.ordinal()) + 1);
			setCompletedTask(Achievs.BUILD_GILDED_ALTAR.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Build A Gilded Altar");
			rewardHARD();
		}
	}

	public void burnFish() {
		if (c.getAchievInt(Achievs.BURN_A_FISH.ordinal()) < Achievs.BURN_A_FISH
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.BURN_A_FISH.ordinal(),
					c.getAchievInt(Achievs.BURN_A_FISH.ordinal()) + 1);
			setCompletedTask(Achievs.BURN_A_FISH.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Burn A Fish");
			achivementsCompleted();
			rewardEASY();
		}
	}

	/**
	 * Handles when a user completes an achievement and updates their total
	 * points and calls updateAchievement method
	 */
	// public void achievementComplete() {
	// this.updateAchievementTab();
	// c.achievementsCompleted++;
	// c.sendMessage("<img=6>You have completed the
	// achievement "
	// + this.achievementName + "..");
	// }

	public void buryBones() {
		if (c.getAchievInt(
				Achievs.BURY_20_BONES.ordinal()) < Achievs.BURY_20_BONES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.BURY_20_BONES.ordinal(),
					c.getAchievInt(Achievs.BURY_20_BONES.ordinal()) + 1);
			setInProgress(Achievs.BURY_20_BONES.getStringId());
			if (c.getAchievInt(Achievs.BURY_20_BONES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Bury "
								+ Achievs.BURY_20_BONES.getCompletedAmount()
								+ " Bones");
			}
			if (c.getAchievInt(
					Achievs.BURY_20_BONES.ordinal()) == Achievs.BURY_20_BONES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.BURY_20_BONES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Bury "
								+ Achievs.BURY_20_BONES.getCompletedAmount()
								+ " Bones");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void buryDragonBones() {
		if (c.getAchievInt(Achievs.BURY_100_DRAG_BONES
				.ordinal()) < Achievs.BURY_100_DRAG_BONES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.BURY_100_DRAG_BONES.ordinal(),
					c.getAchievInt(Achievs.BURY_100_DRAG_BONES.ordinal()) + 1);
			setInProgress(Achievs.BURY_100_DRAG_BONES.getStringId());
			if (c.getAchievInt(Achievs.BURY_100_DRAG_BONES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Bury "
								+ Achievs.BURY_100_DRAG_BONES
										.getCompletedAmount()
								+ " Dragon Bones");
			}
			if (c.getAchievInt(Achievs.BURY_100_DRAG_BONES
					.ordinal()) == Achievs.BURY_100_DRAG_BONES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.BURY_100_DRAG_BONES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Bury "
								+ Achievs.BURY_100_DRAG_BONES
										.getCompletedAmount()
								+ " Dragon Bones");
				rewardMEDIUM();
			}
		}
	}

	// Hard Tasks
	public void buryFrostBones() {
		if (c.getAchievInt(Achievs.BURY_25_FROSTBONES
				.ordinal()) < Achievs.BURY_25_FROSTBONES.getCompletedAmount()) {
			c.setAchievInt(Achievs.BURY_25_FROSTBONES.ordinal(),
					c.getAchievInt(Achievs.BURY_25_FROSTBONES.ordinal()) + 1);
			setInProgress(Achievs.BURY_25_FROSTBONES.getStringId());
			if (c.getAchievInt(Achievs.BURY_25_FROSTBONES.ordinal()) < 2) {
				c.sendMessage(
								"You made progress towards <col=ff0000>Bury "
										+ Achievs.BURY_25_FROSTBONES
												.getCompletedAmount()
										+ " Frost Bones");
			}
			if (c.getAchievInt(Achievs.BURY_25_FROSTBONES
					.ordinal()) == Achievs.BURY_25_FROSTBONES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.BURY_25_FROSTBONES.getStringId());
				c.sendMessage(
								"<img=6>You have completed the achievement Bury "
										+ Achievs.BURY_25_FROSTBONES
												.getCompletedAmount()
										+ " Frost Bones");
				rewardHARD();
			}
		}
	}

	public void CastDFSSpecial() {
		if (c.getAchievInt(
				Achievs.CAST_DFS_SPEC.ordinal()) < Achievs.CAST_DFS_SPEC
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CAST_DFS_SPEC.ordinal(),
					c.getAchievInt(Achievs.CAST_DFS_SPEC.ordinal()) + 1);
			setCompletedTask(Achievs.CAST_DFS_SPEC.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Cast A DFS Special");
			rewardHARD();
		}
	}

	public void catch100Grenwalls() { // TODO: add after hunter
		if (c.getAchievInt(Achievs.CATCH_100_GRENWALLS
				.ordinal()) < Achievs.CATCH_100_GRENWALLS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CATCH_100_GRENWALLS.ordinal(),
					c.getAchievInt(Achievs.CATCH_100_GRENWALLS.ordinal()) + 1);
			setInProgress(Achievs.CATCH_100_GRENWALLS.getStringId());
			if (c.getAchievInt(Achievs.CATCH_100_GRENWALLS.ordinal()) < 2) {
				c.sendMessage(
								"You made progress towards <col=ff0000>Catch "
										+ Achievs.CATCH_100_GRENWALLS
												.getCompletedAmount()
										+ " Grenwalls");
			}
			if (c.getAchievInt(Achievs.CATCH_100_GRENWALLS
					.ordinal()) == Achievs.CATCH_100_GRENWALLS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CATCH_100_GRENWALLS.getStringId());
				c.sendMessage(
								"<img=6>You have completed the achievement Catch "
										+ Achievs.CATCH_100_GRENWALLS
												.getCompletedAmount()
										+ " Grenwalls");
				rewardHARD();
			}
		}
	}

	public void catch500Grenwalls() { // TODO: add after hunter
		if (c.getAchievInt(Achievs.CATCH_500_GRENWALLS
				.ordinal()) < Achievs.CATCH_500_GRENWALLS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CATCH_500_GRENWALLS.ordinal(),
					c.getAchievInt(Achievs.CATCH_500_GRENWALLS.ordinal()) + 1);
			setInProgress(Achievs.CATCH_500_GRENWALLS.getStringId());
			if (c.getAchievInt(Achievs.CATCH_500_GRENWALLS.ordinal()) < 2) {
				c.sendMessage(
								"You made progress towards <col=ff0000>Catch "
										+ Achievs.CATCH_500_GRENWALLS
												.getCompletedAmount()
										+ " Grenwalls");
			}
			if (c.getAchievInt(Achievs.CATCH_500_GRENWALLS
					.ordinal()) == Achievs.CATCH_500_GRENWALLS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CATCH_500_GRENWALLS.getStringId());
				c.sendMessage(
								"<img=6>You have completed the achievement Catch "
										+ Achievs.CATCH_500_GRENWALLS
												.getCompletedAmount()
										+ " Grenwalls");
				rewardELITE();
			}
		}
	}

	public void catchRedChins() {
		if (c.getAchievInt(
				Achievs.CATCH_75_CHINS.ordinal()) < Achievs.CATCH_75_CHINS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CATCH_75_CHINS.ordinal(),
					c.getAchievInt(Achievs.CATCH_75_CHINS.ordinal()) + 1);
			setInProgress(Achievs.CATCH_75_CHINS.getStringId());
			if (c.getAchievInt(Achievs.CATCH_75_CHINS.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Catch "
								+ Achievs.CATCH_75_CHINS.getCompletedAmount()
								+ " Red Chinchompa");
			}
			if (c.getAchievInt(
					Achievs.CATCH_75_CHINS.ordinal()) == Achievs.CATCH_75_CHINS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CATCH_75_CHINS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Catch "
								+ Achievs.SUMMON_20_GRANITE_CRABS
										.getCompletedAmount()
								+ " Red Chinchompa");
				rewardMEDIUM();
			}
		}
	}

	public void catchRocktails() {
		if (c.getAchievInt(Achievs.CATCH_500_ROCKTAILS
				.ordinal()) < Achievs.CATCH_500_ROCKTAILS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CATCH_500_ROCKTAILS.ordinal(),
					c.getAchievInt(Achievs.CATCH_500_ROCKTAILS.ordinal()) + 1);
			setInProgress(Achievs.CATCH_500_ROCKTAILS.getStringId());
			if (c.getAchievInt(Achievs.CATCH_500_ROCKTAILS.ordinal()) < 2) {
				c.sendMessage(
								"You made progress towards <col=ff0000>Catch "
										+ Achievs.CATCH_500_ROCKTAILS
												.getCompletedAmount()
										+ " Rocktails");
			}
			if (c.getAchievInt(Achievs.CATCH_500_ROCKTAILS
					.ordinal()) == Achievs.CATCH_500_ROCKTAILS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CATCH_500_ROCKTAILS.getStringId());
				c.sendMessage(
								"<img=6>You have completed the achievement Catch "
										+ Achievs.CATCH_500_ROCKTAILS
												.getCompletedAmount()
										+ " Rocktails");
				rewardELITE();
			}
		}
	}

	public void changeAppearnace() {
		if (c.getAchievInt(
				Achievs.CHANGE_APPEARANCE.ordinal()) < Achievs.CHANGE_APPEARANCE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CHANGE_APPEARANCE.ordinal(),
					c.getAchievInt(Achievs.CHANGE_APPEARANCE.ordinal()) + 1);
			setCompletedTask(Achievs.CHANGE_APPEARANCE.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Change Appearance");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void chopLogs() {
		if (c.getAchievInt(
				Achievs.CHOP_30_LOGS.ordinal()) < Achievs.CHOP_30_LOGS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CHOP_30_LOGS.ordinal(),
					c.getAchievInt(Achievs.CHOP_30_LOGS.ordinal()) + 1);
			setInProgress(Achievs.CHOP_30_LOGS.getStringId());
			if (c.getAchievInt(Achievs.CHOP_30_LOGS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Chop "
								+ Achievs.CHOP_30_LOGS.getCompletedAmount()
								+ " Logs");
			}
			if (c.getAchievInt(
					Achievs.CHOP_30_LOGS.ordinal()) == Achievs.CHOP_30_LOGS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CHOP_30_LOGS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Chop "
								+ Achievs.CHOP_30_LOGS.getCompletedAmount()
								+ " Logs");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void chopMagic() {
		if (c.getAchievInt(
				Achievs.CHOP_500_MAGIC.ordinal()) < Achievs.CHOP_500_MAGIC
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CHOP_500_MAGIC.ordinal(),
					c.getAchievInt(Achievs.CHOP_500_MAGIC.ordinal()) + 1);
			setInProgress(Achievs.CHOP_500_MAGIC.getStringId());
			if (c.getAchievInt(Achievs.CHOP_500_MAGIC.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Chop "
								+ Achievs.CHOP_500_MAGIC.getCompletedAmount()
								+ " Magic Logs");
			}
			if (c.getAchievInt(
					Achievs.CHOP_500_MAGIC.ordinal()) == Achievs.CHOP_500_MAGIC
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CHOP_500_MAGIC.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Chop "
								+ Achievs.CHOP_500_MAGIC.getCompletedAmount()
								+ " Magic Logs");
				rewardHARD();
			}
		}
	}

	public void ChopYew() {
		if (c.getAchievInt(
				Achievs.CHOP_200_YEWS.ordinal()) < Achievs.CHOP_200_YEWS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CHOP_200_YEWS.ordinal(),
					c.getAchievInt(Achievs.CHOP_200_YEWS.ordinal()) + 1);
			setInProgress(Achievs.CHOP_200_YEWS.getStringId());
			if (c.getAchievInt(Achievs.CHOP_200_YEWS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Chop "
								+ Achievs.CHOP_200_YEWS.getCompletedAmount()
								+ " Yew Logs");
			}
			if (c.getAchievInt(
					Achievs.CHOP_200_YEWS.ordinal()) == Achievs.CHOP_200_YEWS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CHOP_200_YEWS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Chop "
								+ Achievs.CHOP_200_YEWS.getCompletedAmount()
								+ " Yew Logs");
				rewardMEDIUM();
			}
		}
	}
	
	public void progress60Dungeoneering() {
		if (c.getAchievInt(Achievs.PROGRESS_60_DUNGEONEERING.ordinal()) < Achievs.PROGRESS_60_DUNGEONEERING
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.PROGRESS_60_DUNGEONEERING.ordinal(),
					c.getAchievInt(Achievs.PROGRESS_60_DUNGEONEERING.ordinal()) + 1);
			setCompletedTask(Achievs.PROGRESS_60_DUNGEONEERING.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Get progress 60 in dungeoneering.");
			rewardMEDIUM();
		}
	}

	public void completeDuoTask() {
		if (c.getAchievInt(Achievs.DUO_TASK.ordinal()) < Achievs.DUO_TASK
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.DUO_TASK.ordinal(),
					c.getAchievInt(Achievs.DUO_TASK.ordinal()) + 1);
			setCompletedTask(Achievs.DUO_TASK.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Complete A Duo Slayertask");
			rewardMEDIUM();
		}
	}

	public void CookFish() {
		if (c.getAchievInt(
				Achievs.COOK_30_FISHES.ordinal()) < Achievs.COOK_30_FISHES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.COOK_30_FISHES.ordinal(),
					c.getAchievInt(Achievs.COOK_30_FISHES.ordinal()) + 1);
			setInProgress(Achievs.COOK_30_FISHES.getStringId());
			if (c.getAchievInt(Achievs.COOK_30_FISHES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Cook "
								+ Achievs.COOK_30_FISHES.getCompletedAmount()
								+ " Fishes");
			}
			if (c.getAchievInt(
					Achievs.COOK_30_FISHES.ordinal()) == Achievs.COOK_30_FISHES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.COOK_30_FISHES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Cook "
								+ Achievs.COOK_30_FISHES.getCompletedAmount()
								+ " Fishes");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void cookManta() {
		if (c.getAchievInt(
				Achievs.COOK_100_MANTA.ordinal()) < Achievs.COOK_100_MANTA
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.COOK_100_MANTA.ordinal(),
					c.getAchievInt(Achievs.COOK_100_MANTA.ordinal()) + 1);
			setInProgress(Achievs.COOK_100_MANTA.getStringId());
			if (c.getAchievInt(Achievs.COOK_100_MANTA.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Cook "
								+ Achievs.COOK_100_MANTA.getCompletedAmount()
								+ " Manta Rays");
			}
			if (c.getAchievInt(
					Achievs.COOK_100_MANTA.ordinal()) == Achievs.COOK_100_MANTA
							.getCompletedAmount()) {
				setCompletedTask(Achievs.COOK_100_MANTA.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Cook "
								+ Achievs.COOK_100_MANTA.getCompletedAmount()
								+ " Manta Rays");
				rewardMEDIUM();
			}
		}
	}

	public void cookRocktails() {
		if (c.getAchievInt(Achievs.COOK_500_ROCKTAILS
				.ordinal()) < Achievs.COOK_500_ROCKTAILS.getCompletedAmount()) {
			c.setAchievInt(Achievs.COOK_500_ROCKTAILS.ordinal(),
					c.getAchievInt(Achievs.COOK_500_ROCKTAILS.ordinal()) + 1);
			setInProgress(Achievs.COOK_500_ROCKTAILS.getStringId());
			if (c.getAchievInt(Achievs.COOK_500_ROCKTAILS.ordinal()) < 2) {
				c.sendMessage(
								"You made progress towards <col=ff0000>Cook "
										+ Achievs.COOK_500_ROCKTAILS
												.getCompletedAmount()
										+ " Rocktails");
			}
			if (c.getAchievInt(Achievs.COOK_500_ROCKTAILS
					.ordinal()) == Achievs.COOK_500_ROCKTAILS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.COOK_500_ROCKTAILS.getStringId());
				c.sendMessage(
								"<img=6>You have completed the achievement Cook "
										+ Achievs.COOK_500_ROCKTAILS
												.getCompletedAmount()
										+ " Rocktails");
				rewardHARD();
			}
		}
	}

	public void crabsKilled() {
		if (c.getAchievInt(
				Achievs.KILL_20_CRABS.ordinal()) < Achievs.KILL_20_CRABS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_20_CRABS.ordinal(),
					c.getAchievInt(Achievs.KILL_20_CRABS.ordinal()) + 1);
			setInProgress(Achievs.KILL_20_CRABS.getStringId());
			if (c.getAchievInt(Achievs.KILL_20_CRABS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Kill "
								+ Achievs.KILL_20_CRABS.getCompletedAmount()
								+ " Rock Crabs");
			}
			if (c.getAchievInt(
					Achievs.KILL_20_CRABS.ordinal()) == Achievs.KILL_20_CRABS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILL_20_CRABS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Kill "
								+ Achievs.KILL_20_CRABS.getCompletedAmount()
								+ " Rock Crabs");
				achivementsCompleted();
				rewardEASY();
			}

		}
	}

	public void craftAir(int amount) {
		if (c.getAchievInt(
				Achievs.CRAFT_100_AIR.ordinal()) < Achievs.CRAFT_100_AIR
						.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.CRAFT_100_AIR.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_100_AIR.getCompletedAmount()
								+ " Air Runes");
			}
			c.setAchievInt(Achievs.CRAFT_100_AIR.ordinal(),
					c.getAchievInt(Achievs.CRAFT_100_AIR.ordinal()) + amount);
			setInProgress(Achievs.CRAFT_100_AIR.getStringId());
			if (c.getAchievInt(
					Achievs.CRAFT_100_AIR.ordinal()) >= Achievs.CRAFT_100_AIR
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_100_AIR.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_100_AIR.getCompletedAmount()
								+ " Air Runes");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void craftBlackHide() {
		if (c.getAchievInt(Achievs.CRAFT_500_BLACKDHIDE
				.ordinal()) < Achievs.CRAFT_500_BLACKDHIDE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CRAFT_500_BLACKDHIDE.ordinal(),
					c.getAchievInt(Achievs.CRAFT_500_BLACKDHIDE.ordinal()) + 1);
			setInProgress(Achievs.CRAFT_500_BLACKDHIDE.getStringId());
			if (c.getAchievInt(Achievs.CRAFT_500_BLACKDHIDE.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_500_BLACKDHIDE
										.getCompletedAmount()
								+ " Black D'hide Pieces");
			}
			if (c.getAchievInt(Achievs.CRAFT_500_BLACKDHIDE
					.ordinal()) == Achievs.CRAFT_500_BLACKDHIDE
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_500_BLACKDHIDE.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_500_BLACKDHIDE
										.getCompletedAmount()
								+ " Black D'hide Pieces");
				rewardELITE();
			}
		}
	}

	public void craftFurys() {
		if (c.getAchievInt(
				Achievs.CRAFT_20_FURYS.ordinal()) < Achievs.CRAFT_20_FURYS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CRAFT_20_FURYS.ordinal(),
					c.getAchievInt(Achievs.CRAFT_20_FURYS.ordinal()) + 1);
			setInProgress(Achievs.CRAFT_20_FURYS.getStringId());
			if (c.getAchievInt(Achievs.CRAFT_20_FURYS.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_20_FURYS.getCompletedAmount()
								+ " Amulets Of Fury");
			}
			if (c.getAchievInt(
					Achievs.CRAFT_20_FURYS.ordinal()) == Achievs.CRAFT_20_FURYS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_20_FURYS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_20_FURYS.getCompletedAmount()
								+ " Amulets Of Fury");
				rewardHARD();
			}
		}
	}

	public void craftLaw(int amount) {
		if (c.getAchievInt(
				Achievs.CRAFT_200_LAWS.ordinal()) < Achievs.CRAFT_200_LAWS
						.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.CRAFT_200_LAWS.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_200_LAWS.getCompletedAmount()
								+ " Law Runes");
			}
			c.setAchievInt(Achievs.CRAFT_200_LAWS.ordinal(),
					c.getAchievInt(Achievs.CRAFT_200_LAWS.ordinal()) + amount);
			setInProgress(Achievs.CRAFT_200_LAWS.getStringId());
			if (c.getAchievInt(
					Achievs.CRAFT_200_LAWS.ordinal()) >= Achievs.CRAFT_200_LAWS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_200_LAWS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_200_LAWS.getCompletedAmount()
								+ " Law Runes");
				rewardMEDIUM();
			}
		}
	}

	public void craftPhoenixNecks() {
		if (c.getAchievInt(Achievs.CRAFT_50_PHOENIX_NECK
				.ordinal()) < Achievs.CRAFT_50_PHOENIX_NECK
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CRAFT_50_PHOENIX_NECK.ordinal(),
					c.getAchievInt(Achievs.CRAFT_50_PHOENIX_NECK.ordinal())
							+ 1);
			setInProgress(Achievs.CRAFT_50_PHOENIX_NECK.getStringId());
			if (c.getAchievInt(Achievs.CRAFT_50_PHOENIX_NECK.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_50_PHOENIX_NECK
										.getCompletedAmount()
								+ " Phoenix Necks");
			}
			if (c.getAchievInt(Achievs.CRAFT_50_PHOENIX_NECK
					.ordinal()) == Achievs.CRAFT_50_PHOENIX_NECK
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_50_PHOENIX_NECK.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_50_PHOENIX_NECK
										.getCompletedAmount()
								+ " Phoenix Necks");
				rewardMEDIUM();
			}
		}
	}

	public void craftRunes(int amount) {
		if (c.getAchievInt(
				Achievs.CRAFT_10K_RUNES.ordinal()) < Achievs.CRAFT_10K_RUNES
						.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.CRAFT_10K_RUNES.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_10K_RUNES.getCompletedAmount()
								+ " Runes");
			}
			c.setAchievInt(Achievs.CRAFT_10K_RUNES.ordinal(),
					c.getAchievInt(Achievs.CRAFT_10K_RUNES.ordinal()) + amount);
			setInProgress(Achievs.CRAFT_10K_RUNES.getStringId());
			if (c.getAchievInt(Achievs.CRAFT_10K_RUNES
					.ordinal()) >= Achievs.CRAFT_10K_RUNES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_10K_RUNES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_10K_RUNES.getCompletedAmount()
								+ " Runes");
				rewardHARD();
			}
		}
	}

	public void craftStrAmulets() {
		if (c.getAchievInt(Achievs.CRAFT_20_STR_AMULETS
				.ordinal()) < Achievs.CRAFT_20_STR_AMULETS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.CRAFT_20_STR_AMULETS.ordinal(),
					c.getAchievInt(Achievs.CRAFT_20_STR_AMULETS.ordinal()) + 1);
			setInProgress(Achievs.CRAFT_20_STR_AMULETS.getStringId());
			if (c.getAchievInt(Achievs.CRAFT_20_STR_AMULETS.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Craft "
								+ Achievs.CRAFT_20_STR_AMULETS
										.getCompletedAmount()
								+ " Strength Amulets");
			}
			if (c.getAchievInt(Achievs.CRAFT_20_STR_AMULETS
					.ordinal()) == Achievs.CRAFT_20_STR_AMULETS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.CRAFT_20_STR_AMULETS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Craft "
								+ Achievs.CRAFT_20_STR_AMULETS
										.getCompletedAmount()
								+ " Strength Amulets");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void deathcount() {
		if (c.getAchievInt(Achievs.DIE_20_TIME.ordinal()) < Achievs.DIE_20_TIME
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.DIE_20_TIME.ordinal(),
					c.getAchievInt(Achievs.DIE_20_TIME.ordinal()) + 1);
			setInProgress(Achievs.DIE_20_TIME.getStringId());
			if (c.getAchievInt(Achievs.DIE_20_TIME.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Die "
								+ Achievs.DIE_20_TIME.getCompletedAmount()
								+ " Times");
			}
			if (c.getAchievInt(
					Achievs.DIE_20_TIME.ordinal()) == Achievs.DIE_20_TIME
							.getCompletedAmount()) {
				setCompletedTask(Achievs.DIE_20_TIME.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Die "
								+ Achievs.DIE_20_TIME.getCompletedAmount()
								+ " Times");
				rewardMEDIUM();
			}
		}
	}

	public void defeat10Players() {
		if (c.getAchievInt(
				Achievs.DEFEAT_10_PLAYERS.ordinal()) < Achievs.DEFEAT_10_PLAYERS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_10_PLAYERS.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_10_PLAYERS.ordinal()) + 1);
			setInProgress(Achievs.DEFEAT_10_PLAYERS.getStringId());
			if (c.getAchievInt(Achievs.DEFEAT_10_PLAYERS.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Defeat "
								+ Achievs.DEFEAT_10_PLAYERS.getCompletedAmount()
								+ " Players In Wildy");
			}
			if (c.getAchievInt(Achievs.DEFEAT_10_PLAYERS
					.ordinal()) == Achievs.DEFEAT_10_PLAYERS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.DEFEAT_10_PLAYERS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Defeat "
								+ Achievs.DEFEAT_10_PLAYERS.getCompletedAmount()
								+ " Players In Wildy");
				rewardMEDIUM();
			}
		}
	}

	public void defeat35GWDBosses() {
		if (c.getAchievInt(
				Achievs.DEFEAT_35_GWD.ordinal()) < Achievs.DEFEAT_35_GWD
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_35_GWD.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_35_GWD.ordinal()) + 1);
			setInProgress(Achievs.DEFEAT_35_GWD.getStringId());
			if (c.getAchievInt(Achievs.DEFEAT_35_GWD.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Defeat "
								+ Achievs.DEFEAT_35_GWD.getCompletedAmount()
								+ " GWD Bosses");
			}
			if (c.getAchievInt(
					Achievs.DEFEAT_35_GWD.ordinal()) == Achievs.DEFEAT_35_GWD
							.getCompletedAmount()) {
				setCompletedTask(Achievs.DEFEAT_35_GWD.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Defeat "
								+ Achievs.DEFEAT_35_GWD.getCompletedAmount()
								+ " GWD Bosses");
				rewardELITE();
			}
		}
	}

	public void defeat40DagBosses() {
		if (c.getAchievInt(Achievs.DEFEAT_DAGG_BOSSES.ordinal()) < Achievs.DEFEAT_DAGG_BOSSES.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_DAGG_BOSSES.ordinal(), c.getAchievInt(Achievs.DEFEAT_DAGG_BOSSES.ordinal()) + 1);
			setInProgress(Achievs.DEFEAT_DAGG_BOSSES.getStringId());
			if (c.getAchievInt(Achievs.DEFEAT_DAGG_BOSSES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Defeat " + Achievs.DEFEAT_DAGG_BOSSES.getCompletedAmount() + " Dagannoth Bosses");
			}
			if (c.getAchievInt(Achievs.DEFEAT_DAGG_BOSSES.ordinal()) == Achievs.DEFEAT_DAGG_BOSSES.getCompletedAmount()) {
				setCompletedTask(Achievs.DEFEAT_DAGG_BOSSES.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Defeat " + Achievs.DEFEAT_DAGG_BOSSES.getCompletedAmount() + " Dagannoth Bosses");
				rewardELITE();
			}
		}
	}

	public void defeat60Players() {
		if (c.getAchievInt(Achievs.DEFEAT_60_PLAYERS.ordinal()) < Achievs.DEFEAT_60_PLAYERS.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_60_PLAYERS.ordinal(), c.getAchievInt(Achievs.DEFEAT_60_PLAYERS.ordinal()) + 1);
			setInProgress(Achievs.DEFEAT_60_PLAYERS.getStringId());
			if (c.getAchievInt(Achievs.DEFEAT_60_PLAYERS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Defeat " + Achievs.DEFEAT_60_PLAYERS.getCompletedAmount() + " Players In Wildy");
			}
			if (c.getAchievInt(Achievs.DEFEAT_60_PLAYERS.ordinal()) == Achievs.DEFEAT_60_PLAYERS.getCompletedAmount()) {
				setCompletedTask(Achievs.DEFEAT_60_PLAYERS.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Defeat " + Achievs.DEFEAT_60_PLAYERS.getCompletedAmount() + " Players In Wildy");
				rewardELITE();
			}
		}
	}

	// Medium Tasks

	public void defeatAOD() {
		if (c.getAchievInt(Achievs.DEFEAT_AOD.ordinal()) < Achievs.DEFEAT_AOD
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_AOD.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_AOD.ordinal()) + 1);
			setCompletedTask(Achievs.DEFEAT_AOD.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Defeat Avatar Of Destruction");
			rewardELITE();
		}
	}

	public void defeatCorpBeast() {
		if (c.getAchievInt(
				Achievs.DEFEAT_CORP_20.ordinal()) < Achievs.DEFEAT_CORP_20
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_CORP_20.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_CORP_20.ordinal()) + 1);
			setInProgress(Achievs.DEFEAT_CORP_20.getStringId());
			if (c.getAchievInt(Achievs.DEFEAT_CORP_20.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Defeat Corp Beast "
								+ Achievs.DEFEAT_CORP_20.getCompletedAmount()
								+ " Times");
			}
			if (c.getAchievInt(
					Achievs.DEFEAT_CORP_20.ordinal()) == Achievs.DEFEAT_CORP_20
							.getCompletedAmount()) {
				setCompletedTask(Achievs.DEFEAT_CORP_20.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Defeat Corp Beast "
								+ Achievs.DEFEAT_CORP_20.getCompletedAmount()
								+ " Times");
				rewardELITE();
			}
		}
	}

	public void defeatJad() {
		if (c.getAchievInt(Achievs.DEFEAT_JAD.ordinal()) < Achievs.DEFEAT_JAD
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_JAD.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_JAD.ordinal()) + 1);
			setCompletedTask(Achievs.DEFEAT_JAD.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Defeat The Jad");
			rewardHARD();
		}
	}

	public void defeatNex() {
		if (c.getAchievInt(
				Achievs.DEFEAT_10_NEX.ordinal()) < Achievs.DEFEAT_10_NEX
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_10_NEX.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_10_NEX.ordinal()) + 1);
			setInProgress(Achievs.DEFEAT_10_NEX.getStringId());
			if (c.getAchievInt(Achievs.DEFEAT_10_NEX.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Defeat Nex "
								+ Achievs.DEFEAT_10_NEX.getCompletedAmount()
								+ " Times");
			}
			if (c.getAchievInt(
					Achievs.DEFEAT_10_NEX.ordinal()) == Achievs.DEFEAT_10_NEX
							.getCompletedAmount()) {
				setCompletedTask(Achievs.DEFEAT_10_NEX.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Defeat Nex "
								+ Achievs.DEFEAT_10_NEX.getCompletedAmount()
								+ " Times");
				rewardELITE();
			}
		}
	}

	// Maybe done lol
	public void DefeatPlayer() {
		if (c.getAchievInt(
				Achievs.DEFEAT_A_PLAYER.ordinal()) < Achievs.DEFEAT_A_PLAYER
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_A_PLAYER.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_A_PLAYER.ordinal()) + 1);
			setCompletedTask(Achievs.DEFEAT_A_PLAYER.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Defeat A Player In Wildy");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void defeatTarget() {
		if (c.getAchievInt(
				Achievs.DEFEAT_TARGET.ordinal()) < Achievs.DEFEAT_TARGET
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_TARGET.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_TARGET.ordinal()) + 1);
			setCompletedTask(Achievs.DEFEAT_TARGET.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Defeat Your Target In Wildy");
			rewardELITE();
		}
	}

	public void defeatWildyWyrm() {
		if (c.getAchievInt(
				Achievs.DEFEAT_WILDYWYRM.ordinal()) < Achievs.DEFEAT_WILDYWYRM
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DEFEAT_WILDYWYRM.ordinal(),
					c.getAchievInt(Achievs.DEFEAT_WILDYWYRM.ordinal()) + 1);
			setCompletedTask(Achievs.DEFEAT_WILDYWYRM.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Defeat WildyWyrm");
			rewardELITE();
		}
	}

	public void duelCounts() {
		if (c.getAchievInt(
				Achievs.DUEL_50_TIMES.ordinal()) < Achievs.DUEL_50_TIMES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.DUEL_50_TIMES.ordinal(),
					c.getAchievInt(Achievs.DUEL_50_TIMES.ordinal()) + 1);
			setInProgress(Achievs.DUEL_50_TIMES.getStringId());
			if (c.getAchievInt(Achievs.DUEL_50_TIMES.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Duel Atleast "
								+ Achievs.DUEL_50_TIMES.getCompletedAmount()
								+ " Times");
			}
			if (c.getAchievInt(
					Achievs.DUEL_50_TIMES.ordinal()) == Achievs.DUEL_50_TIMES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.DUEL_50_TIMES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Duel Atleast "
								+ Achievs.DUEL_50_TIMES.getCompletedAmount()
								+ " Times");
				rewardHARD();
			}
		}
	}

	public void earn2000PKPoints(int points) {
		if (c.getAchievInt(Achievs.EARN_2K_PKP.ordinal()) < Achievs.EARN_2K_PKP
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.EARN_2K_PKP.ordinal(),
					c.getAchievInt(Achievs.EARN_2K_PKP.ordinal()) + points);
			setInProgress(Achievs.EARN_2K_PKP.getStringId());
			if (c.getAchievInt(Achievs.EARN_2K_PKP.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Earn "
								+ Achievs.EARN_2K_PKP.getCompletedAmount()
								+ " PK Points");
			}
			if (c.getAchievInt(
					Achievs.EARN_2K_PKP.ordinal()) >= Achievs.EARN_2K_PKP
							.getCompletedAmount()) {
				setCompletedTask(Achievs.EARN_2K_PKP.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Earn "
								+ Achievs.EARN_2K_PKP.getCompletedAmount()
								+ " PK Points");
				rewardHARD();
			}
		}
	}

	public void earnDuoPoints(int amount) {
		if (c.getAchievInt(
				Achievs.EARN_20_DUOP.ordinal()) < Achievs.EARN_20_DUOP
						.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.EARN_20_DUOP.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Earn "
								+ Achievs.EARN_20_DUOP.getCompletedAmount()
								+ " Duo Points");
			}
			c.setAchievInt(Achievs.EARN_20_DUOP.ordinal(),
					c.getAchievInt(Achievs.EARN_20_DUOP.ordinal()) + amount);
			setInProgress(Achievs.EARN_20_DUOP.getStringId());
			if (c.getAchievInt(
					Achievs.EARN_20_DUOP.ordinal()) >= Achievs.EARN_20_DUOP
							.getCompletedAmount()) {
				setCompletedTask(Achievs.EARN_20_DUOP.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Earn "
								+ Achievs.EARN_20_DUOP.getCompletedAmount()
								+ " Duo Points");
				rewardELITE();
			}
		}
	}

	public void earnSlayerPoints(int amount) {
		if (c.getAchievInt(
				Achievs.EARN_200_SLAYP.ordinal()) < Achievs.EARN_200_SLAYP
						.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.EARN_200_SLAYP.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Earn "
								+ Achievs.EARN_200_SLAYP.getCompletedAmount()
								+ " Slayer Points");
			}
			c.setAchievInt(Achievs.EARN_200_SLAYP.ordinal(),
					c.getAchievInt(Achievs.EARN_200_SLAYP.ordinal()) + amount);
			setInProgress(Achievs.EARN_200_SLAYP.getStringId());
			if (c.getAchievInt(
					Achievs.EARN_200_SLAYP.ordinal()) >= Achievs.EARN_200_SLAYP
							.getCompletedAmount()) {
				setCompletedTask(Achievs.EARN_200_SLAYP.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Earn "
								+ Achievs.EARN_200_SLAYP.getCompletedAmount()
								+ " Slayer Points");
				rewardELITE();
			}
		}
	}

	public void eatLobsters() {
		if (c.getAchievInt(
				Achievs.EAT_20_LOBSTERS.ordinal()) < Achievs.EAT_20_LOBSTERS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.EAT_20_LOBSTERS.ordinal(),
					c.getAchievInt(Achievs.EAT_20_LOBSTERS.ordinal()) + 1);
			setInProgress(Achievs.EAT_20_LOBSTERS.getStringId());
			if (c.getAchievInt(Achievs.EAT_20_LOBSTERS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Eat "
								+ Achievs.EAT_20_LOBSTERS.getCompletedAmount()
								+ " Lobsters");
			}
			if (c.getAchievInt(Achievs.EAT_20_LOBSTERS
					.ordinal()) == Achievs.EAT_20_LOBSTERS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.EAT_20_LOBSTERS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Eat "
								+ Achievs.EAT_20_LOBSTERS.getCompletedAmount()
								+ " Lobsters");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void eloRating1400() {
		if (c.getAchievInt(Achievs.ELO_1400.ordinal()) < Achievs.ELO_1400
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.ELO_1400.ordinal(),
					c.getAchievInt(Achievs.ELO_1400.ordinal()) + 1400);
			setInProgress(Achievs.ELO_1400.getStringId());
			// if (c.getAchievInt(Achievs.ELO_1400.ordinal()) < 2)
			// c.sendMessage("You made progress towards
			// <col=ff0000>Get Atleast "+
			// Achievs.ELO_1400.getCompletedAmount() +" Elo Rating");
			if (c.getAchievInt(Achievs.ELO_1400.ordinal()) >= Achievs.ELO_1400
					.getCompletedAmount()) {
				setCompletedTask(Achievs.ELO_1400.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Get Atleast "
								+ Achievs.ELO_1400.getCompletedAmount()
								+ " Elo Rating");
				rewardHARD();
			}
		}
	}

	public void eloRating1600() {
		if (c.getAchievInt(Achievs.ELO_1600.ordinal()) < Achievs.ELO_1600
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.ELO_1600.ordinal(), c.getEloRating());
			setInProgress(Achievs.ELO_1600.getStringId());
			// if (c.getAchievInt(Achievs.ELO_1600.ordinal()) < 2)
			// c.sendMessage("You made progress towards
			// <col=ff0000>Get Atleast "+
			// Achievs.ELO_1600.getCompletedAmount() +" Elo Rating");
			if (c.getAchievInt(Achievs.ELO_1600.ordinal()) >= Achievs.ELO_1600
					.getCompletedAmount()) {
				setCompletedTask(Achievs.ELO_1600.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Get Atleast "
								+ Achievs.ELO_1600.getCompletedAmount()
								+ " Elo Rating");
				rewardELITE();
			}
		}
	}

	public void enterLottery() {
		if (c.getAchievInt(
				Achievs.ENTER_LOTTERY.ordinal()) < Achievs.ENTER_LOTTERY
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.ENTER_LOTTERY.ordinal(),
					c.getAchievInt(Achievs.ENTER_LOTTERY.ordinal()) + 1);
			setCompletedTask(Achievs.ENTER_LOTTERY.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Participate In The Lottery");
			rewardMEDIUM();
		}
	}

	public void exhangeArtifact() {
		if (c.getAchievInt(
				Achievs.EXHANGE_ARTIFACT.ordinal()) < Achievs.EXHANGE_ARTIFACT
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.EXHANGE_ARTIFACT.ordinal(),
					c.getAchievInt(Achievs.EXHANGE_ARTIFACT.ordinal()) + 1);
			setCompletedTask(Achievs.EXHANGE_ARTIFACT.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Exchange An Artifact");
			rewardHARD();
		}
	}

	public void farmHerb() {
		if (c.getAchievInt(Achievs.FARM_A_HERB.ordinal()) < Achievs.FARM_A_HERB
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.FARM_A_HERB.ordinal(),
					c.getAchievInt(Achievs.FARM_A_HERB.ordinal()) + 1);
			setCompletedTask(Achievs.FARM_A_HERB.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Farm A Herb");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void fifteenKillstreak() {
		if (c.getAchievInt(
				Achievs.KILLSTREAK_15.ordinal()) < Achievs.KILLSTREAK_15
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILLSTREAK_15.ordinal(),
					Achievs.KILLSTREAK_15.getCompletedAmount());
			setInProgress(Achievs.KILLSTREAK_15.getStringId());
			// if (c.getAchievInt(Achievs.KILLSTREAK_15.ordinal()) < 2)
			// c.sendMessage("You made progress towards
			// <col=ff0000>Defeat "+
			// Achievs.KILLSTREAK_15.getCompletedAmount() +" Without Dieing");
			if (c.getAchievInt(
					Achievs.KILLSTREAK_15.ordinal()) == Achievs.KILLSTREAK_15
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILLSTREAK_15.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Defeat "
								+ Achievs.KILLSTREAK_15.getCompletedAmount()
								+ " Without Dieing");
				rewardHARD();
			}
		}
	}

	public void buildMagicCage() {
		if (c.getAchievInt(Achievs.BUILD_MAGICAL_CAGE.ordinal()) < Achievs.BUILD_MAGICAL_CAGE.getCompletedAmount()) {
			c.setAchievInt(Achievs.BUILD_MAGICAL_CAGE.ordinal(),
			c.getAchievInt(Achievs.BUILD_MAGICAL_CAGE.ordinal()) + 1);
			setCompletedTask(Achievs.BUILD_MAGICAL_CAGE.getStringId());
			c.sendMessage("<img=6>You have completed the achievement Build A Greater Magical Cage.");
			rewardELITE();
		}
	}

	public void findDarkBow() {
		if (c.getAchievInt(
				Achievs.FIND_A_DARKBOW.ordinal()) < Achievs.FIND_A_DARKBOW
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FIND_A_DARKBOW.ordinal(),
					c.getAchievInt(Achievs.FIND_A_DARKBOW.ordinal()) + 1);
			setCompletedTask(Achievs.FIND_A_DARKBOW.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Find A Dark Bow");
			rewardHARD();
		}
	}

	public void findDragonChain() {
		if (c.getAchievInt(
				Achievs.FIND_DRAGON_CHAIN.ordinal()) < Achievs.FIND_DRAGON_CHAIN
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FIND_DRAGON_CHAIN.ordinal(),
					c.getAchievInt(Achievs.FIND_DRAGON_CHAIN.ordinal()) + 1);
			setCompletedTask(Achievs.FIND_DRAGON_CHAIN.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Find A Dragon Chainbody");
			rewardHARD();
		}
	}

	public void findDragonPickAxe() {
		if (c.getAchievInt(
				Achievs.FIND_DRAGON_PICK.ordinal()) < Achievs.FIND_DRAGON_PICK
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FIND_DRAGON_PICK.ordinal(),
					c.getAchievInt(Achievs.FIND_DRAGON_PICK.ordinal()) + 1);
			setCompletedTask(Achievs.FIND_DRAGON_PICK.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Find A Dragon Pickaxe");
			rewardELITE();
		}
	}

	public void finishedKillStreak20Kills() {
		if (c.getAchievInt(
				Achievs.KILLSTREAK_20.ordinal()) < Achievs.KILLSTREAK_20
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILLSTREAK_20.ordinal(),
					Achievs.KILLSTREAK_20.getCompletedAmount());
			setInProgress(Achievs.KILLSTREAK_20.getStringId());
			// if (c.getAchievInt(Achievs.KILLSTREAK_20.ordinal()) < 2)
			// c.sendMessage("You made progress towards
			// <col=ff0000>Get Atleast "+
			// Achievs.KILLSTREAK_20.getCompletedAmount() +" Killstreaks");
			if (c.getAchievInt(
					Achievs.KILLSTREAK_20.ordinal()) == Achievs.KILLSTREAK_20
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILLSTREAK_20.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Get Atleast "
								+ Achievs.KILLSTREAK_20.getCompletedAmount()
								+ " Killstreaks");
				rewardELITE();
			}
		}
	}

	public void fireCannonBalls() {
		if (c.getAchievInt(
				Achievs.FIRE_5K_CBALLS.ordinal()) < Achievs.FIRE_5K_CBALLS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FIRE_5K_CBALLS.ordinal(),
					c.getAchievInt(Achievs.FIRE_5K_CBALLS.ordinal()) + 1);
			setInProgress(Achievs.FIRE_5K_CBALLS.getStringId());
			if (c.getAchievInt(Achievs.FIRE_5K_CBALLS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Fire "
								+ Achievs.FIRE_5K_CBALLS.getCompletedAmount()
								+ " Cannon Balls");
			}
			if (c.getAchievInt(
					Achievs.FIRE_5K_CBALLS.ordinal()) == Achievs.FIRE_5K_CBALLS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.FIRE_5K_CBALLS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Fire "
								+ Achievs.FIRE_5K_CBALLS.getCompletedAmount()
								+ " Cannon Balls");
				rewardELITE();
			}
		}
	}

	public void fishFishes() {
		if (c.getAchievInt(
				Achievs.FISH_30_FISHES.ordinal()) < Achievs.FISH_30_FISHES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FISH_30_FISHES.ordinal(),
					c.getAchievInt(Achievs.FISH_30_FISHES.ordinal()) + 1);
			setInProgress(Achievs.FISH_30_FISHES.getStringId());
			if (c.getAchievInt(Achievs.FISH_30_FISHES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Fish "
								+ Achievs.FISH_30_FISHES.getCompletedAmount()
								+ " Fishes");
			}
			if (c.getAchievInt(
					Achievs.FISH_30_FISHES.ordinal()) == Achievs.FISH_30_FISHES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.FISH_30_FISHES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Fish "
								+ Achievs.FISH_30_FISHES.getCompletedAmount()
								+ " Fishes");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void fishManta() {
		if (c.getAchievInt(
				Achievs.FISH_500_MANTAS.ordinal()) < Achievs.FISH_500_MANTAS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FISH_500_MANTAS.ordinal(),
					c.getAchievInt(Achievs.FISH_500_MANTAS.ordinal()) + 1);
			setInProgress(Achievs.FISH_500_MANTAS.getStringId());
			if (c.getAchievInt(Achievs.FISH_500_MANTAS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Fish "
								+ Achievs.FISH_500_MANTAS.getCompletedAmount()
								+ " Manta Rays");
			}
			if (c.getAchievInt(Achievs.FISH_500_MANTAS
					.ordinal()) == Achievs.FISH_500_MANTAS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.FISH_500_MANTAS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Fish "
								+ Achievs.FISH_500_MANTAS.getCompletedAmount()
								+ " Manta Rays");
				rewardHARD();
			}
		}
	}

	public void fishSharks() {
		if (c.getAchievInt(
				Achievs.FISH_200_SHARKS.ordinal()) < Achievs.FISH_200_SHARKS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.FISH_200_SHARKS.ordinal(),
					c.getAchievInt(Achievs.FISH_200_SHARKS.ordinal()) + 1);
			setInProgress(Achievs.FISH_200_SHARKS.getStringId());
			if (c.getAchievInt(Achievs.FISH_200_SHARKS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Fish "
								+ Achievs.FISH_200_SHARKS.getCompletedAmount()
								+ " Sharks");
			}
			if (c.getAchievInt(Achievs.FISH_200_SHARKS
					.ordinal()) == Achievs.FISH_200_SHARKS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.FISH_200_SHARKS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Fish "
								+ Achievs.FISH_200_SHARKS.getCompletedAmount()
								+ " Sharks");
				rewardMEDIUM();
			}
		}
	}

	public void completeAllFrozen() {
		if (c.getAchievInt(
				Achievs.COMPLETE_FROZEN_FLOORS.ordinal()) < Achievs.COMPLETE_FROZEN_FLOORS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.COMPLETE_FROZEN_FLOORS.ordinal(),
					c.getAchievInt(Achievs.COMPLETE_FROZEN_FLOORS.ordinal()) + 1);
			setCompletedTask(Achievs.COMPLETE_FROZEN_FLOORS.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Complete All Frozen Floors.");
			achivementsCompleted();
			c.getItems().addItemToBank(995, 25000);
			c.achievementsCompleted++;
			// rewardEASY();
		}
	}
	
	public void complete50Warped() {
		if (c.getAchievInt(
				Achievs.COMPLETE_50_WARPED_FLOORS.ordinal()) < Achievs.COMPLETE_50_WARPED_FLOORS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.COMPLETE_50_WARPED_FLOORS.ordinal(),
					c.getAchievInt(Achievs.COMPLETE_50_WARPED_FLOORS.ordinal()) + 1);
			setInProgress(Achievs.COMPLETE_50_WARPED_FLOORS.getStringId());
			if (c.getAchievInt(Achievs.COMPLETE_50_WARPED_FLOORS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Complete "
								+ Achievs.COMPLETE_50_WARPED_FLOORS.getCompletedAmount()
								+ " Warped Floors.");
			}
			if (c.getAchievInt(Achievs.COMPLETE_50_WARPED_FLOORS
					.ordinal()) == Achievs.COMPLETE_50_WARPED_FLOORS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.COMPLETE_50_WARPED_FLOORS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Complete "
								+ Achievs.COMPLETE_50_WARPED_FLOORS.getCompletedAmount()
								+ " Warped Floors.");
				rewardMEDIUM();
			}
		}
	}

	public void completeAllFurnished() {
		if (c.getAchievInt(Achievs.COMPLETE_ALL_FURNISHED.ordinal()) < Achievs.COMPLETE_ALL_FURNISHED.getCompletedAmount()) {
			c.setAchievInt(Achievs.COMPLETE_ALL_FURNISHED.ordinal(), c.getAchievInt(Achievs.COMPLETE_ALL_FURNISHED.ordinal()) + 1);
			setCompletedTask(Achievs.COMPLETE_ALL_FURNISHED.getStringId());
			c.sendMessage("<img=6>You have completed the achievement Complete All Furnished Floors");
			c.getItems().addItemToBank(995, 300000);
			c.achievementsCompleted++;
			// rewardHARD();
		}
	}

	public void get99Skill() {
		if (c.getAchievInt(
				Achievs.GET_99_SKILL.ordinal()) < Achievs.GET_99_SKILL
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.GET_99_SKILL.ordinal(),
					c.getAchievInt(Achievs.GET_99_SKILL.ordinal()) + 1);
			setCompletedTask(Achievs.GET_99_SKILL.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Get A Level 99 Skill");
			rewardMEDIUM();
		}
	}

	public void gnomeLaps() {
		if (c.getAchievInt(
				Achievs.GNOMELAPS_10.ordinal()) < Achievs.GNOMELAPS_10
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.GNOMELAPS_10.ordinal(),
					c.getAchievInt(Achievs.GNOMELAPS_10.ordinal()) + 1);
			setInProgress(Achievs.GNOMELAPS_10.getStringId());
			if (c.getAchievInt(Achievs.GNOMELAPS_10.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Complete "
								+ Achievs.GNOMELAPS_10.getCompletedAmount()
								+ " Gnome Laps");
			}
			if (c.getAchievInt(
					Achievs.GNOMELAPS_10.ordinal()) == Achievs.GNOMELAPS_10
							.getCompletedAmount()) {
				setCompletedTask(Achievs.GNOMELAPS_10.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Complete "
								+ Achievs.GNOMELAPS_10.getCompletedAmount()
								+ " Gnome Laps");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void heal50000HP(int amount) {
		if (c.getAchievInt(Achievs.HEAL_50K_HP.ordinal()) < Achievs.HEAL_50K_HP
				.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.HEAL_50K_HP.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Heal "
								+ Achievs.HEAL_50K_HP.getCompletedAmount()
								+ "HP");
			}
			c.setAchievInt(Achievs.HEAL_50K_HP.ordinal(),
					c.getAchievInt(Achievs.HEAL_50K_HP.ordinal()) + amount);
			setInProgress(Achievs.HEAL_50K_HP.getStringId());
			if (c.getAchievInt(
					Achievs.HEAL_50K_HP.ordinal()) >= Achievs.HEAL_50K_HP
							.getCompletedAmount()) {
				setCompletedTask(Achievs.HEAL_50K_HP.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Heal "
								+ Achievs.HEAL_50K_HP.getCompletedAmount()
								+ " HP");
				rewardELITE();
			}
		}
	}

	public void hit70Magic() {
		if (c.getAchievInt(
				Achievs.HIT_70_MAGIC.ordinal()) < Achievs.HIT_70_MAGIC
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.HIT_70_MAGIC.ordinal(),
					c.getAchievInt(Achievs.HIT_70_MAGIC.ordinal()) + 1);
			setCompletedTask(Achievs.HIT_70_MAGIC.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Hit 70+ With Magic");
			rewardHARD();
		}
	}

	public void hit90Melee() {
		if (c.getAchievInt(
				Achievs.HIT_90_MELEE.ordinal()) < Achievs.HIT_90_MELEE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.HIT_90_MELEE.ordinal(),
					c.getAchievInt(Achievs.HIT_90_MELEE.ordinal()) + 1);
			setCompletedTask(Achievs.HIT_90_MELEE.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Hit 90+ With Melee");
			rewardELITE();
		}
	}

	public void kill10000Monsters() {
		if (c.getAchievInt(
				Achievs.KILL_10K_MONSTERS.ordinal()) < Achievs.KILL_10K_MONSTERS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_10K_MONSTERS.ordinal(),
					c.getAchievInt(Achievs.KILL_10K_MONSTERS.ordinal()) + 1);
			setInProgress(Achievs.KILL_10K_MONSTERS.getStringId());
			if (c.getAchievInt(Achievs.KILL_10K_MONSTERS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Kill "
								+ Achievs.KILL_10K_MONSTERS.getCompletedAmount()
								+ " Monsters");
			}
			if (c.getAchievInt(Achievs.KILL_10K_MONSTERS
					.ordinal()) == Achievs.KILL_10K_MONSTERS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILL_10K_MONSTERS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Kill "
								+ Achievs.KILL_10K_MONSTERS.getCompletedAmount()
								+ " Monsters");

				achivementsCompleted();
				rewardELITE();
			}
		}
	}

	public void kill150Torms() {
		if (c.getAchievInt(
				Achievs.KILL_150_TORMS.ordinal()) < Achievs.KILL_150_TORMS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_150_TORMS.ordinal(),
					c.getAchievInt(Achievs.KILL_150_TORMS.ordinal()) + 1);
			setInProgress(Achievs.KILL_150_TORMS.getStringId());
			if (c.getAchievInt(Achievs.KILL_150_TORMS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Kill "
								+ Achievs.KILL_150_TORMS.getCompletedAmount()
								+ " Tormented Demons");
			}
			if (c.getAchievInt(
					Achievs.KILL_150_TORMS.ordinal()) == Achievs.KILL_150_TORMS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILL_150_TORMS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Kill "
								+ Achievs.KILL_150_TORMS.getCompletedAmount()
								+ " Tormented Demons");
				rewardELITE();
			}
		}
	}

	public void kill500Monsters() {
		if (c.getAchievInt(
				Achievs.KILL_500_MONSTERS.ordinal()) < Achievs.KILL_500_MONSTERS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_500_MONSTERS.ordinal(),
					c.getAchievInt(Achievs.KILL_500_MONSTERS.ordinal()) + 1);
			setInProgress(Achievs.KILL_500_MONSTERS.getStringId());
			if (c.getAchievInt(Achievs.KILL_500_MONSTERS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Kill "
								+ Achievs.KILL_500_MONSTERS.getCompletedAmount()
								+ " Monsters");
			}
			if (c.getAchievInt(Achievs.KILL_500_MONSTERS
					.ordinal()) == Achievs.KILL_500_MONSTERS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILL_500_MONSTERS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Kill "
								+ Achievs.KILL_500_MONSTERS.getCompletedAmount()
								+ " Monsters");
				achivementsCompleted();
				rewardMEDIUM();
			}
		}
	}

	public void kill5Torms() {
		if (c.getAchievInt(
				Achievs.KILL_5_TORMS.ordinal()) < Achievs.KILL_5_TORMS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_5_TORMS.ordinal(),
					c.getAchievInt(Achievs.KILL_5_TORMS.ordinal()) + 1);
			setInProgress(Achievs.KILL_5_TORMS.getStringId());
			if (c.getAchievInt(Achievs.KILL_5_TORMS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Kill "
								+ Achievs.KILL_5_TORMS.getCompletedAmount()
								+ " Tormented Demons");
			}
			if (c.getAchievInt(
					Achievs.KILL_5_TORMS.ordinal()) == Achievs.KILL_5_TORMS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILL_5_TORMS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Kill "
								+ Achievs.KILL_5_TORMS.getCompletedAmount()
								+ " Tormented Demons");
				rewardHARD();
			}
		}
	}

	public void killChaosEle() {
		if (c.getAchievInt(
				Achievs.KILL_CHAOS_ELE.ordinal()) < Achievs.KILL_CHAOS_ELE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_CHAOS_ELE.ordinal(),
					c.getAchievInt(Achievs.KILL_CHAOS_ELE.ordinal()) + 1);
			setCompletedTask(Achievs.KILL_CHAOS_ELE.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Kill The Chaos Elemental");
			rewardHARD();
		}
	}

	public void killDragons() {
		if (c.getAchievInt(
				Achievs.KILL_20_DRAGONS.ordinal()) < Achievs.KILL_20_DRAGONS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_20_DRAGONS.ordinal(),
					c.getAchievInt(Achievs.KILL_20_DRAGONS.ordinal()) + 1);
			setInProgress(Achievs.KILL_20_DRAGONS.getStringId());
			if (c.getAchievInt(Achievs.KILL_20_DRAGONS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Kill "
								+ Achievs.KILL_20_DRAGONS.getCompletedAmount()
								+ " Dragons");
			}
			if (c.getAchievInt(Achievs.KILL_20_DRAGONS
					.ordinal()) == Achievs.KILL_20_DRAGONS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILL_20_DRAGONS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Kill "
								+ Achievs.KILL_20_DRAGONS.getCompletedAmount()
								+ " Dragons");
				rewardMEDIUM();
			}
		}
	}

	
	public void completeFloor60C6Large() {
		if (c.getAchievInt(Achievs.COMPLETE_FLOOR60_C6_LARGE.ordinal()) < Achievs.COMPLETE_FLOOR60_C6_LARGE.getCompletedAmount()) {
			c.setAchievInt(Achievs.COMPLETE_FLOOR60_C6_LARGE.ordinal(),
			c.getAchievInt(Achievs.COMPLETE_FLOOR60_C6_LARGE.ordinal()) + 1);
			setCompletedTask(Achievs.COMPLETE_FLOOR60_C6_LARGE.getStringId());
			c.sendMessage("<img=6>You have completed the achievement Complete Floor 60 C6 Large.");
			rewardELITE();
		}
	}


	public void killFrostDragon() {
		if (c.getAchievInt(
				Achievs.KILL_FROSTDRAGON.ordinal()) < Achievs.KILL_FROSTDRAGON
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_FROSTDRAGON.ordinal(),
					c.getAchievInt(Achievs.KILL_FROSTDRAGON.ordinal()) + 1);
			setCompletedTask(Achievs.KILL_FROSTDRAGON.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Kill A Frost Dragon");
			rewardMEDIUM();
		}
	}

	public void killKBD() {
		if (c.getAchievInt(Achievs.KILL_KBD.ordinal()) < Achievs.KILL_KBD
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_KBD.ordinal(),
					c.getAchievInt(Achievs.KILL_KBD.ordinal()) + 1);
			setCompletedTask(Achievs.KILL_KBD.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Kill King Black Dragon");
			rewardHARD();
		}
	}

	public void killRev() {
		if (c.getAchievInt(
				Achievs.KILL_A_REVENANT.ordinal()) < Achievs.KILL_A_REVENANT
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_A_REVENANT.ordinal(),
					c.getAchievInt(Achievs.KILL_A_REVENANT.ordinal()) + 1);
			setCompletedTask(Achievs.KILL_A_REVENANT.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Kill A Revenant");
			rewardMEDIUM();
		}
	}

	public void kuradalTask() {
		if (c.getAchievInt(Achievs.KURADAL_SLAYERTASK
				.ordinal()) < Achievs.KURADAL_SLAYERTASK.getCompletedAmount()) {
			c.setAchievInt(Achievs.KURADAL_SLAYERTASK.ordinal(),
					c.getAchievInt(Achievs.KURADAL_SLAYERTASK.ordinal()) + 1);
			setCompletedTask(Achievs.KURADAL_SLAYERTASK.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Complete A Task From Kuradal");
			rewardHARD();
		}
	}

	public void lightFires() {
		if (c.getAchievInt(
				Achievs.LIGHT_20_FIRES.ordinal()) < Achievs.LIGHT_20_FIRES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.LIGHT_20_FIRES.ordinal(),
					c.getAchievInt(Achievs.LIGHT_20_FIRES.ordinal()) + 1);
			setInProgress(Achievs.LIGHT_20_FIRES.getStringId());
			if (c.getAchievInt(Achievs.LIGHT_20_FIRES.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Light "
								+ Achievs.LIGHT_20_FIRES.getCompletedAmount()
								+ " Fires");
			}
			if (c.getAchievInt(
					Achievs.LIGHT_20_FIRES.ordinal()) == Achievs.LIGHT_20_FIRES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.LIGHT_20_FIRES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Light "
								+ Achievs.LIGHT_20_FIRES.getCompletedAmount()
								+ " Fires");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void lightMagic() {
		if (c.getAchievInt(
				Achievs.LIGHT_200_MAGIC.ordinal()) < Achievs.LIGHT_200_MAGIC
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.LIGHT_200_MAGIC.ordinal(),
					c.getAchievInt(Achievs.LIGHT_200_MAGIC.ordinal()) + 1);
			setInProgress(Achievs.LIGHT_200_MAGIC.getStringId());
			if (c.getAchievInt(Achievs.LIGHT_200_MAGIC.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Light "
								+ Achievs.LIGHT_200_MAGIC.getCompletedAmount()
								+ " Magic Logs");
			}
			if (c.getAchievInt(Achievs.LIGHT_200_MAGIC
					.ordinal()) == Achievs.LIGHT_200_MAGIC
							.getCompletedAmount()) {
				setCompletedTask(Achievs.LIGHT_200_MAGIC.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Light "
								+ Achievs.LIGHT_200_MAGIC.getCompletedAmount()
								+ " Magic Logs");
				rewardHARD();
			}
		}
	}

	public void makeOverloads() {
		if (c.getAchievInt(Achievs.MAKE_10_OVL.ordinal()) < Achievs.MAKE_10_OVL
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_10_OVL.ordinal(),
					c.getAchievInt(Achievs.MAKE_10_OVL.ordinal()) + 1);
			setInProgress(Achievs.MAKE_10_OVL.getStringId());
			if (c.getAchievInt(Achievs.MAKE_10_OVL.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make "
								+ Achievs.MAKE_10_OVL.getCompletedAmount()
								+ " Overloads");
			}
			if (c.getAchievInt(
					Achievs.MAKE_10_OVL.ordinal()) == Achievs.MAKE_10_OVL
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_10_OVL.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Make "
								+ Achievs.MAKE_10_OVL.getCompletedAmount()
								+ " Overloads");
				rewardHARD();
			}
		}
	}

	public void makeSteelTitanPouches() {
		if (c.getAchievInt(Achievs.MAKE_600_STEEL_TITAN
				.ordinal()) < Achievs.MAKE_600_STEEL_TITAN
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_600_STEEL_TITAN.ordinal(),
					c.getAchievInt(Achievs.MAKE_600_STEEL_TITAN.ordinal()) + 1);
			setInProgress(Achievs.MAKE_600_STEEL_TITAN.getStringId());
			if (c.getAchievInt(Achievs.MAKE_600_STEEL_TITAN.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make "
								+ Achievs.MAKE_600_STEEL_TITAN
										.getCompletedAmount()
								+ " Steel Titan Pouches");
			}
			if (c.getAchievInt(Achievs.MAKE_600_STEEL_TITAN
					.ordinal()) == Achievs.MAKE_600_STEEL_TITAN
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_600_STEEL_TITAN.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Make "
								+ Achievs.MAKE_600_STEEL_TITAN
										.getCompletedAmount()
								+ " Steel Titan Pouches");
				rewardELITE();
			}
		}
	}

	public void Mine100Rune() {
		if (c.getAchievInt(
				Achievs.MINE_100_RUNE.ordinal()) < Achievs.MINE_100_RUNE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.MINE_100_RUNE.ordinal(),
					c.getAchievInt(Achievs.MINE_100_RUNE.ordinal()) + 1);
			setInProgress(Achievs.MINE_100_RUNE.getStringId());
			if (c.getAchievInt(Achievs.MINE_100_RUNE.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Mine "
								+ Achievs.MINE_100_RUNE.getCompletedAmount()
								+ " Rune Ores");
			}
			if (c.getAchievInt(
					Achievs.MINE_100_RUNE.ordinal()) == Achievs.MINE_100_RUNE
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MINE_100_RUNE.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Mine "
								+ Achievs.MINE_100_RUNE.getCompletedAmount()
								+ " Rune Ores");
				rewardHARD();
			}
		}
	}

	public void Mine300Rune() {
		if (c.getAchievInt(
				Achievs.MINE_300_RUNE.ordinal()) < Achievs.MINE_300_RUNE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.MINE_300_RUNE.ordinal(),
					c.getAchievInt(Achievs.MINE_300_RUNE.ordinal()) + 1);
			setInProgress(Achievs.MINE_300_RUNE.getStringId());
			if (c.getAchievInt(Achievs.MINE_300_RUNE.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Mine "
								+ Achievs.MINE_300_RUNE.getCompletedAmount()
								+ " Rune Ores");
			}
			if (c.getAchievInt(
					Achievs.MINE_300_RUNE.ordinal()) == Achievs.MINE_300_RUNE
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MINE_300_RUNE.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Mine "
								+ Achievs.MINE_300_RUNE.getCompletedAmount()
								+ " Rune Ores");
				rewardELITE();
			}
		}
	}

	public void mineAddy() {
		if (c.getAchievInt(
				Achievs.MINE_200_ADDYS.ordinal()) < Achievs.MINE_200_ADDYS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.MINE_200_ADDYS.ordinal(),
					c.getAchievInt(Achievs.MINE_200_ADDYS.ordinal()) + 1);
			setInProgress(Achievs.MINE_200_ADDYS.getStringId());
			if (c.getAchievInt(Achievs.MINE_200_ADDYS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Mine "
								+ Achievs.MINE_200_ADDYS.getCompletedAmount()
								+ " Adamant Ores");
			}
			if (c.getAchievInt(
					Achievs.MINE_200_ADDYS.ordinal()) == Achievs.MINE_200_ADDYS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MINE_200_ADDYS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Mine "
								+ Achievs.MINE_200_ADDYS.getCompletedAmount()
								+ " Adamant Ores");
				rewardMEDIUM();
			}
		}
	}

	public void mineOres() {
		if (c.getAchievInt(
				Achievs.MINE_30_ORES.ordinal()) < Achievs.MINE_30_ORES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.MINE_30_ORES.ordinal(),
					c.getAchievInt(Achievs.MINE_30_ORES.ordinal()) + 1);
			setInProgress(Achievs.MINE_30_ORES.getStringId());
			if (c.getAchievInt(Achievs.MINE_30_ORES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Mine "
								+ Achievs.MINE_30_ORES.getCompletedAmount()
								+ " Ores");
			}
			if (c.getAchievInt(
					Achievs.MINE_30_ORES.ordinal()) == Achievs.MINE_30_ORES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MINE_30_ORES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Mine "
								+ Achievs.MINE_30_ORES.getCompletedAmount()
								+ " Ores");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void obtainTokhaar() {
		if (c.getAchievInt(
				Achievs.OBTAIN_TOKHAAR.ordinal()) < Achievs.OBTAIN_TOKHAAR
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.OBTAIN_TOKHAAR.ordinal(),
					c.getAchievInt(Achievs.OBTAIN_TOKHAAR.ordinal()) + 1);
			setCompletedTask(Achievs.OBTAIN_TOKHAAR.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Obtain A Tokhaar-Kal");
			rewardELITE();
		}
	}

	// Elite Tasks

	public void openCrystalChests() {
		if (c.getAchievInt(
				Achievs.OPEN_50_CHESTS.ordinal()) < Achievs.OPEN_50_CHESTS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.OPEN_50_CHESTS.ordinal(),
					c.getAchievInt(Achievs.OPEN_50_CHESTS.ordinal()) + 1);
			setInProgress(Achievs.OPEN_50_CHESTS.getStringId());
			if (c.getAchievInt(Achievs.OPEN_50_CHESTS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Open "
								+ Achievs.OPEN_50_CHESTS.getCompletedAmount()
								+ " Crystal Chests");
			}
			if (c.getAchievInt(
					Achievs.OPEN_50_CHESTS.ordinal()) == Achievs.OPEN_50_CHESTS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.OPEN_50_CHESTS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Open "
								+ Achievs.OPEN_50_CHESTS.getCompletedAmount()
								+ " Crystal Chests");
				rewardELITE();
			}
		}
	}

	public void pickFlax() {
		if (c.getAchievInt(Achievs.PICK_30_FLAX.ordinal()) < Achievs.PICK_30_FLAX.getCompletedAmount()) {
			c.setAchievInt(Achievs.PICK_30_FLAX.ordinal(), c.getAchievInt(Achievs.PICK_30_FLAX.ordinal()) + 1);
			setInProgress(Achievs.PICK_30_FLAX.getStringId());
			if (c.getAchievInt(Achievs.PICK_30_FLAX.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Pick "
						+ Achievs.PICK_30_FLAX.getCompletedAmount() + " Flax");
			}
			if (c.getAchievInt(Achievs.PICK_30_FLAX.ordinal()) == Achievs.PICK_30_FLAX.getCompletedAmount()) {
				setCompletedTask(Achievs.PICK_30_FLAX.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Pick "
						+ Achievs.PICK_30_FLAX.getCompletedAmount() + " Flax");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	private static final int[][] itemsToAddEasy = {{995, 25000}};
	/**
	 * Reward Options - Easy, Medium, Hard If the player receiving the reward
	 * does not have room for the reward it is sent to their bank.
	 */
	public void rewardEASY() {
		if (!WorldType.equalsType(WorldType.SPAWN)) {
			c.achievementsCompleted++;
			achivementsCompleted();
			if (c.getItems().freeSlots() > 1 && (c.dungParty != null && c.dungParty.dungManager == null)) {
				c.getItems().addItem(995, 25000);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 1, "easy_achievement");
				}
				c.sendMessage(
						"<col=ff>You have been rewarded 25k for this achievement!");
			} else {
				for (int[] element : itemsToAddEasy) {
					c.getItems().addItemToBank(element[0], element[1]);
					if (c.getClan() != null) {
						c.getClan().addClanPoints(c, 1, "easy_achievement");
					}
					c.sendMessage(
							"<col=ff>You have been rewarded 25k for this achievement!");
				}
			}
			if (WorldType.isSeasonal()) {
				c.getItems().addItemToBank(11180, 25);
			}
		}
	}
	
	private static final int[][] itemsToAddElite = {{995, 500000}};

	public void rewardELITE() {
		if (!WorldType.equalsType(WorldType.SPAWN)) {
			c.achievementsCompleted++;
			achivementsCompleted();
			if (c.getItems().freeSlots() > 1 && (c.dungParty != null && c.dungParty.dungManager == null)) {
				c.getItems().addItem(995, 500000);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "elite_achievement");
				}
				c.sendMessage(
						"<col=ff>You have been rewarded 500k for this achievement!");
			} else {
				for (int[] element : itemsToAddElite) {
					c.getItems().addItemToBank(element[0], element[1]);
					if (c.getClan() != null) {
						c.getClan().addClanPoints(c, 3, "elite_achievement");
					}
					c.sendMessage(
							"<col=ff>You have been rewarded 500k for this achievement!");
				}
			}
			if (WorldType.isSeasonal()) {
				c.getItems().addItemToBank(11180, 200);
			}
		}
	}
	
	private static final int[][] itemsToAddHard = {{995, 300000}};

	public void rewardHARD() {
		if (!WorldType.equalsType(WorldType.SPAWN)) {
			c.achievementsCompleted++;
			achivementsCompleted();
			if (c.getItems().freeSlots() > 1 && (c.dungParty != null && c.dungParty.dungManager == null)) {
				c.getItems().addItem(995, 300000);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 3, "hard_achievement");
				}
				c.sendMessage(
						"<col=ff>You have been rewarded 300k for this achievement!");
			} else {
				for (int[] element : itemsToAddHard) {
					c.getItems().addItemToBank(element[0], element[1]);
					if (c.getClan() != null) {
						c.getClan().addClanPoints(c, 3, "hard_achievement");
					}
					c.sendMessage(
							"<col=ff>You have been rewarded 300k for this achievement!");
				}
			}
			if (WorldType.isSeasonal()) {
				c.getItems().addItemToBank(11180, 100);
			}
		}
	}
	
	private static final int[][] itemsToAddMedium = {{995, 100000}};

	public void rewardMEDIUM() {
		if (!WorldType.equalsType(WorldType.SPAWN)) {
			c.achievementsCompleted++;
			achivementsCompleted();
			if (c.getItems().freeSlots() > 1 && (c.dungParty != null && c.dungParty.dungManager == null)) {
				c.getItems().addItem(995, 100000);
				if (c.getClan() != null) {
					c.getClan().addClanPoints(c, 2, "medium_achievement");
				}
				c.sendMessage(
						"<col=ff>You have been rewarded 100k for this achievement!");
			} else {
				for (int[] element : itemsToAddMedium) {
					c.getItems().addItemToBank(element[0], element[1]);
					if (c.getClan() != null) {
						c.getClan().addClanPoints(c, 2, "medium_achievement");
					}
					c.sendMessage(
							"<col=ff>You have been rewarded 100k for this achievement!");
				}
			}
			if (WorldType.isSeasonal()) {
				c.getItems().addItemToBank(11180, 50);
			}
		}
	}

	/**
	 * Achievements Progress
	 */
	// Easy Tasks

	public void setBankPin() {
		if (c.getAchievInt(Achievs.SET_BANK_PIN.ordinal()) == 0) {
			c.setAchievInt(Achievs.SET_BANK_PIN.ordinal(),
					c.getAchievInt(Achievs.SET_BANK_PIN.ordinal()) + 1);
			setCompletedTask(Achievs.SET_BANK_PIN.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Set A Bank Pin");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void setCompletedTask(int stringId) {
		c.getPacketSender().setTextColor(stringId,
				BitShifting.compressHexColor(0x00f000));
	}

	public void setInProgress(int stringId) {
		c.getPacketSender().setTextColor(stringId,
				BitShifting.compressHexColor(0xf8f800));
	}

	public void setNotStarted(int stringId) {
		c.getPacketSender().setTextColor(stringId,
				BitShifting.compressHexColor(0xf80000));
	}

	public void setupCannon() {
		if (c.getAchievInt(
				Achievs.SETUP_CANNON.ordinal()) < Achievs.SETUP_CANNON
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SETUP_CANNON.ordinal(),
					c.getAchievInt(Achievs.SETUP_CANNON.ordinal()) + 1);
			setCompletedTask(Achievs.SETUP_CANNON.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Set A Up Cannon");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void slayerTask() {
		if (c.getAchievInt(Achievs.SLAYERTASK.ordinal()) < Achievs.SLAYERTASK
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.SLAYERTASK.ordinal(),
					c.getAchievInt(Achievs.SLAYERTASK.ordinal()) + 1);
			setCompletedTask(Achievs.SLAYERTASK.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Complete A Slayertask");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void smeltBronze() {
		if (c.getAchievInt(Achievs.SMELT_10_BRONZEBRAS
				.ordinal()) < Achievs.SMELT_10_BRONZEBRAS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SMELT_10_BRONZEBRAS.ordinal(),
					c.getAchievInt(Achievs.SMELT_10_BRONZEBRAS.ordinal()) + 1);
			setInProgress(Achievs.SMELT_10_BRONZEBRAS.getStringId());
			if (c.getAchievInt(Achievs.SMELT_10_BRONZEBRAS.ordinal()) < 2) {
				c.sendMessage(
								"You made progress towards <col=ff0000>Smelt "
										+ Achievs.SMELT_10_BRONZEBRAS
												.getCompletedAmount()
										+ " Bronze Bars");
			}
			if (c.getAchievInt(Achievs.SMELT_10_BRONZEBRAS
					.ordinal()) == Achievs.SMELT_10_BRONZEBRAS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.SMELT_10_BRONZEBRAS.getStringId());
				c.sendMessage(
								"<img=6>You have completed the achievement Smelt "
										+ Achievs.SMELT_10_BRONZEBRAS
												.getCompletedAmount()
										+ " Bronze Bars");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void smeltRune() {
		if (c.getAchievInt(
				Achievs.SMELT_50_RUNE.ordinal()) < Achievs.SMELT_50_RUNE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SMELT_50_RUNE.ordinal(),
					c.getAchievInt(Achievs.SMELT_50_RUNE.ordinal()) + 1);
			setInProgress(Achievs.SMELT_50_RUNE.getStringId());
			if (c.getAchievInt(Achievs.SMELT_50_RUNE.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Smelt "
								+ Achievs.SMELT_50_RUNE.getCompletedAmount()
								+ " Rune Ores");
			}
			if (c.getAchievInt(
					Achievs.SMELT_50_RUNE.ordinal()) == Achievs.SMELT_50_RUNE
							.getCompletedAmount()) {
				setCompletedTask(Achievs.SMELT_50_RUNE.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Smelt "
								+ Achievs.SMELT_50_RUNE.getCompletedAmount()
								+ " Rune Ores");
				rewardHARD();
			}
		}
	}

	public void smithRunePlate() {
		if (c.getAchievInt(
				Achievs.SMITH_RUNE_PLATE.ordinal()) < Achievs.SMITH_RUNE_PLATE
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SMITH_RUNE_PLATE.ordinal(),
					c.getAchievInt(Achievs.SMITH_RUNE_PLATE.ordinal()) + 1);
			setCompletedTask(Achievs.SMITH_RUNE_PLATE.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Smith A Rune Platebody");
			rewardHARD();
		}
	}

	public void specialAttack() {
		if (c.getAchievInt(
				Achievs.SPECIALATTACK.ordinal()) < Achievs.SPECIALATTACK
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SPECIALATTACK.ordinal(),
					c.getAchievInt(Achievs.SPECIALATTACK.ordinal()) + 1);
			setCompletedTask(Achievs.SPECIALATTACK.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Perform A Special Attack");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void spiritSpider() {
		if (c.getAchievInt(Achievs.SUMMON_SPIRIT_SPIDER
				.ordinal()) < Achievs.SUMMON_SPIRIT_SPIDER
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SUMMON_SPIRIT_SPIDER.ordinal(),
					c.getAchievInt(Achievs.SUMMON_SPIRIT_SPIDER.ordinal()) + 1);
			setCompletedTask(Achievs.SUMMON_SPIRIT_SPIDER.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Summon A Spirit Spider");
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void spiritWolfPouches() {
		if (c.getAchievInt(Achievs.MAKE_15_SPIRITWOLF
				.ordinal()) < Achievs.MAKE_15_SPIRITWOLF.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_15_SPIRITWOLF.ordinal(),
					c.getAchievInt(Achievs.MAKE_15_SPIRITWOLF.ordinal()) + 1);
			setInProgress(Achievs.MAKE_15_SPIRITWOLF.getStringId());
			if (c.getAchievInt(Achievs.MAKE_15_SPIRITWOLF.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make "
								+ Achievs.MAKE_15_SPIRITWOLF
										.getCompletedAmount()
								+ " Spirit Wolf Pouches");
			}
			if (c.getAchievInt(Achievs.MAKE_15_SPIRITWOLF
					.ordinal()) == Achievs.MAKE_15_SPIRITWOLF
							.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_15_SPIRITWOLF.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Make "
								+ Achievs.MAKE_15_SPIRITWOLF
										.getCompletedAmount()
								+ " Spirit Wolf Pouches");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void steal() {
		if (c.getAchievInt(
				Achievs.STEAL_50_TIMES.ordinal()) < Achievs.STEAL_50_TIMES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.STEAL_50_TIMES.ordinal(),
					c.getAchievInt(Achievs.STEAL_50_TIMES.ordinal()) + 1);
			setInProgress(Achievs.STEAL_50_TIMES.getStringId());
			if (c.getAchievInt(Achievs.STEAL_50_TIMES.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Steal "
								+ Achievs.STEAL_50_TIMES.getCompletedAmount()
								+ " Times");
			}
			if (c.getAchievInt(
					Achievs.STEAL_50_TIMES.ordinal()) == Achievs.STEAL_50_TIMES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.STEAL_50_TIMES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Steal "
								+ Achievs.STEAL_50_TIMES.getCompletedAmount()
								+ " Times");
				achivementsCompleted();
				rewardEASY();

			}
		}
	}

	public void summonGraniteCrabs() {
		if (c.getAchievInt(Achievs.SUMMON_20_GRANITE_CRABS
				.ordinal()) < Achievs.SUMMON_20_GRANITE_CRABS
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SUMMON_20_GRANITE_CRABS.ordinal(),
					c.getAchievInt(Achievs.SUMMON_20_GRANITE_CRABS.ordinal())
							+ 1);
			setInProgress(Achievs.SUMMON_20_GRANITE_CRABS.getStringId());
			if (c.getAchievInt(Achievs.SUMMON_20_GRANITE_CRABS.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Summon "
								+ Achievs.SUMMON_20_GRANITE_CRABS
										.getCompletedAmount()
								+ " Granite Crabs");
			}
			if (c.getAchievInt(Achievs.SUMMON_20_GRANITE_CRABS
					.ordinal()) == Achievs.SUMMON_20_GRANITE_CRABS
							.getCompletedAmount()) {
				setCompletedTask(Achievs.SUMMON_20_GRANITE_CRABS.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Summon "
								+ Achievs.SUMMON_20_GRANITE_CRABS
										.getCompletedAmount()
								+ " Granite Crabs");
				rewardMEDIUM();
			}
		}
	}

	public void sumonaTask() {
		if (c.getAchievInt(
				Achievs.SUMONA_SLAYERTASK.ordinal()) < Achievs.SUMONA_SLAYERTASK
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.SUMONA_SLAYERTASK.ordinal(),
					c.getAchievInt(Achievs.SUMONA_SLAYERTASK.ordinal()) + 1);
			setCompletedTask(Achievs.SUMONA_SLAYERTASK.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Complete A Task From Sumona");
			rewardMEDIUM();
		}
	}

	public void thiev10M(int amount) {
		if (c.getAchievInt(Achievs.THEIV_10M.ordinal()) < Achievs.THEIV_10M
				.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.THEIV_10M.ordinal()) < 2) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Thieve "
								+ Achievs.THEIV_10M.getCompletedAmount()
								+ " Coins");
			}
			c.setAchievInt(Achievs.THEIV_10M.ordinal(),
					c.getAchievInt(Achievs.THEIV_10M.ordinal()) + amount);
			setInProgress(Achievs.THEIV_10M.getStringId());
			if (c.getAchievInt(Achievs.THEIV_10M.ordinal()) >= Achievs.THEIV_10M
					.getCompletedAmount()) {
				setCompletedTask(Achievs.THEIV_10M.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Thieve "
								+ Achievs.THEIV_10M.getCompletedAmount()
								+ " Coins");
				rewardHARD();
			}
		}
	}

	public void Thiev300K(int amount) {
		if (c.getAchievInt(Achievs.THEIV_300K.ordinal()) < Achievs.THEIV_300K
				.getCompletedAmount()) {
			if (c.getAchievInt(Achievs.THEIV_300K.ordinal()) == 0) {
				c.sendMessage(
						"You made progress towards <col=ff0000>Thieve "
								+ Achievs.THEIV_300K.getCompletedAmount()
								+ " Coins");
			}
			c.setAchievInt(Achievs.THEIV_300K.ordinal(),
					c.getAchievInt(Achievs.THEIV_300K.ordinal()) + amount);
			setInProgress(Achievs.THEIV_300K.getStringId());
			if (c.getAchievInt(
					Achievs.THEIV_300K.ordinal()) >= Achievs.THEIV_300K
							.getCompletedAmount()) {
				setCompletedTask(Achievs.THEIV_300K.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Thieve "
								+ Achievs.THEIV_300K.getCompletedAmount()
								+ " Coins");
				rewardMEDIUM();
			}
		}
	}

	public void threeKillstreak() {
		if (c.getAchievInt(
				Achievs.KILLSTREAK_3.ordinal()) < Achievs.KILLSTREAK_3
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILLSTREAK_3.ordinal(),
					Achievs.KILLSTREAK_3.getCompletedAmount());
			setInProgress(Achievs.KILLSTREAK_3.getStringId());
			// if (c.getAchievInt(Achievs.KILLSTREAK_3.ordinal()) < 2)
			// c.sendMessage("You made progress towards
			// <col=ff0000>Defeat "+
			// Achievs.KILLSTREAK_3.getCompletedAmount() +" Players Without
			// Dying");
			if (c.getAchievInt(
					Achievs.KILLSTREAK_3.ordinal()) == Achievs.KILLSTREAK_3
							.getCompletedAmount()) {
				setCompletedTask(Achievs.KILLSTREAK_3.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Defeat "
								+ Achievs.KILLSTREAK_3.getCompletedAmount()
								+ " Players Without Dying");
				rewardMEDIUM();
			}
		}
	}
	
	public void vengeanceKill() {
		if (c.getAchievInt(
				Achievs.KILL_WITH_VENG.ordinal()) < Achievs.KILL_WITH_VENG
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.KILL_WITH_VENG.ordinal(),
					c.getAchievInt(Achievs.KILL_WITH_VENG.ordinal()) + 1);
			setCompletedTask(Achievs.KILL_WITH_VENG.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Kill A Player With Vengeance");
			rewardELITE();
		}
	}

	public void vote10Times() {
		if (c.getAchievInt(
				Achievs.VOTE_10_TIMES.ordinal()) < Achievs.VOTE_10_TIMES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.VOTE_10_TIMES.ordinal(),
					c.getAchievInt(Achievs.VOTE_10_TIMES.ordinal()) + 1);
			setInProgress(Achievs.VOTE_10_TIMES.getStringId());
			if (c.getAchievInt(Achievs.VOTE_10_TIMES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Vote "
								+ Achievs.VOTE_10_TIMES.getCompletedAmount()
								+ " Times For " + Config.SERVER_NAME);
			}
			if (c.getAchievInt(
					Achievs.VOTE_10_TIMES.ordinal()) == Achievs.VOTE_10_TIMES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.VOTE_10_TIMES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Vote "
								+ Achievs.VOTE_10_TIMES.getCompletedAmount()
								+ " Times For " + Config.SERVER_NAME);
				rewardMEDIUM();
			}
		}
	}

	public void vote250Times() {
		if (c.getAchievInt(
				Achievs.VOTE_250_TIMES.ordinal()) < Achievs.VOTE_250_TIMES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.VOTE_250_TIMES.ordinal(),
					c.getAchievInt(Achievs.VOTE_250_TIMES.ordinal()) + 1);
			setInProgress(Achievs.VOTE_250_TIMES.getStringId());
			if (c.getAchievInt(Achievs.VOTE_250_TIMES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Vote "
								+ Achievs.VOTE_250_TIMES.getCompletedAmount()
								+ " Times For " + Config.SERVER_NAME);
			}
			if (c.getAchievInt(
					Achievs.VOTE_250_TIMES.ordinal()) == Achievs.VOTE_250_TIMES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.VOTE_250_TIMES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Vote "
								+ Achievs.VOTE_250_TIMES.getCompletedAmount()
								+ " Times For " + Config.SERVER_NAME);
				rewardELITE();
			}
		}
	}

	public void vote50Times() {
		if (c.getAchievInt(
				Achievs.VOTE_50_TIMES.ordinal()) < Achievs.VOTE_50_TIMES
						.getCompletedAmount()) {
			c.setAchievInt(Achievs.VOTE_50_TIMES.ordinal(),
					c.getAchievInt(Achievs.VOTE_50_TIMES.ordinal()) + 1);
			setInProgress(Achievs.VOTE_50_TIMES.getStringId());
			if (c.getAchievInt(Achievs.VOTE_50_TIMES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Vote "
								+ Achievs.VOTE_50_TIMES.getCompletedAmount()
								+ " Times For " + Config.SERVER_NAME);
			}
			if (c.getAchievInt(
					Achievs.VOTE_50_TIMES.ordinal()) == Achievs.VOTE_50_TIMES
							.getCompletedAmount()) {
				setCompletedTask(Achievs.VOTE_50_TIMES.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Vote "
								+ Achievs.VOTE_50_TIMES.getCompletedAmount()
								+ " Times For " + Config.SERVER_NAME);
				rewardHARD();
			}
		}
	}

	public void VoteOnce() {
		if (c.getAchievInt(Achievs.VOTE_FOR_FP.ordinal()) < Achievs.VOTE_FOR_FP
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.VOTE_FOR_FP.ordinal(),
					c.getAchievInt(Achievs.VOTE_FOR_FP.ordinal()) + 1);
			setCompletedTask(Achievs.VOTE_FOR_FP.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Vote For "
							+ Config.SERVER_NAME);
			achivementsCompleted();
			rewardEASY();
		}
	}

	public void win10PestControl() {
		if (c.getAchievInt(Achievs.WIN_10_PC.ordinal()) < Achievs.WIN_10_PC
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.WIN_10_PC.ordinal(),
					c.getAchievInt(Achievs.WIN_10_PC.ordinal()) + 1);
			setInProgress(Achievs.WIN_10_PC.getStringId());
			if (c.getAchievInt(Achievs.WIN_10_PC.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Win "
								+ Achievs.WIN_10_PC.getCompletedAmount()
								+ " Games Of Pest Control");
			}
			if (c.getAchievInt(Achievs.WIN_10_PC.ordinal()) == Achievs.WIN_10_PC
					.getCompletedAmount()) {
				setCompletedTask(Achievs.WIN_10_PC.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Win "
								+ Achievs.WIN_10_PC.getCompletedAmount()
								+ " Games Of Pest Control");
				rewardHARD();
			}
		}
	}

	public void win50PestControl() {
		if (c.getAchievInt(Achievs.WIN_50_PC.ordinal()) < Achievs.WIN_50_PC
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.WIN_50_PC.ordinal(),
					c.getAchievInt(Achievs.WIN_50_PC.ordinal()) + 1);
			setInProgress(Achievs.WIN_50_PC.getStringId());
			if (c.getAchievInt(Achievs.WIN_50_PC.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Win "
								+ Achievs.WIN_50_PC.getCompletedAmount()
								+ " Games Of Pest Control");
			}
			if (c.getAchievInt(Achievs.WIN_50_PC.ordinal()) == Achievs.WIN_50_PC
					.getCompletedAmount()) {
				setCompletedTask(Achievs.WIN_50_PC.getStringId());
				c.sendMessage(
						"<img=6>You have completed the achievement Win "
								+ Achievs.WIN_50_PC.getCompletedAmount()
								+ " Games Of Pest Control");
				rewardELITE();
			}
		}
	}

	public void winDuel() {
		if (c.getAchievInt(Achievs.WIN_A_DUEL.ordinal()) < Achievs.WIN_A_DUEL
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.WIN_A_DUEL.ordinal(),
					c.getAchievInt(Achievs.WIN_A_DUEL.ordinal()) + 1);
			setCompletedTask(Achievs.WIN_A_DUEL.getStringId());
			c.sendMessage(
					"<img=6>You have completed the achievement Win A Fight In Duel Arena");
			rewardMEDIUM();
		}
	}

	public void winFunPKTourny() {
//		if (c.getAchievInt(
//				Achievs.WIN_IN_LMS.ordinal()) < Achievs.WIN_IN_LMS
//						.getCompletedAmount()) {
//			c.setAchievInt(Achievs.WIN_IN_LMS.ordinal(),
//					c.getAchievInt(Achievs.WIN_IN_LMS.ordinal()) + 1);
//			setCompletedTask(Achievs.WIN_IN_LMS.getStringId());
//			c.sendMessage(
//					"<img=6>You have completed the achievement Win A FunPK Tourny");
//			rewardELITE();
//		}
	}
	
	public void chopMahogany() {
		if (c.getAchievInt(
				Achievs.CHOP_500_MAHOGANY.ordinal()) < Achievs.CHOP_500_MAHOGANY.getCompletedAmount()) {
			c.setAchievInt(Achievs.CHOP_500_MAHOGANY.ordinal(),
			c.getAchievInt(Achievs.CHOP_500_MAHOGANY.ordinal()) + 1);
			setInProgress(Achievs.CHOP_500_MAHOGANY.getStringId());
			if (c.getAchievInt(Achievs.CHOP_500_MAHOGANY.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Chop " + Achievs.CHOP_500_MAHOGANY.getCompletedAmount() + " Mahogany Logs");
			}
			if (c.getAchievInt(Achievs.CHOP_500_MAHOGANY.ordinal()) == Achievs.CHOP_500_MAHOGANY.getCompletedAmount()) {
				setCompletedTask(Achievs.CHOP_500_MAHOGANY.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Chop " + Achievs.CHOP_500_MAHOGANY.getCompletedAmount() + " Mahogany Logs");
				rewardHARD();
			}
		}
	}
	
	public void earnMaxFavour() {
		if (c.getAchievInt(Achievs.EARN_MAX_FAVOUR.ordinal()) < Achievs.EARN_MAX_FAVOUR.getCompletedAmount()) {
			c.setAchievInt(Achievs.EARN_MAX_FAVOUR.ordinal(),
			c.getAchievInt(Achievs.EARN_MAX_FAVOUR.ordinal()) + 1);
			setCompletedTask(Achievs.EARN_MAX_FAVOUR.getStringId());
			c.sendMessage("<img=6>You have completed the achievement Earn 100% Tai Bwo Favour.");
			rewardELITE();
		}
	}
	
	public void complete20LargeFloors() {
		if (c.getAchievInt(
				Achievs.COMPLETE_20_LARGE_FLOORS.ordinal()) < Achievs.COMPLETE_20_LARGE_FLOORS.getCompletedAmount()) {
			c.setAchievInt(Achievs.COMPLETE_20_LARGE_FLOORS.ordinal(),
			c.getAchievInt(Achievs.COMPLETE_20_LARGE_FLOORS.ordinal()) + 1);
			setInProgress(Achievs.COMPLETE_20_LARGE_FLOORS.getStringId());
			if (c.getAchievInt(Achievs.COMPLETE_20_LARGE_FLOORS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Complete " + Achievs.COMPLETE_20_LARGE_FLOORS.getCompletedAmount() + " Large Floors");
			}
			if (c.getAchievInt(Achievs.COMPLETE_20_LARGE_FLOORS.ordinal()) == Achievs.COMPLETE_20_LARGE_FLOORS.getCompletedAmount()) {
				setCompletedTask(Achievs.COMPLETE_20_LARGE_FLOORS.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Complete " + Achievs.COMPLETE_20_LARGE_FLOORS.getCompletedAmount() + " Large Floors");
				rewardHARD();
			}
		}
	}
	
	public void complete100Puzzles() {
		if (c.getAchievInt(
				Achievs.COMPLETE_100_PUZZLES.ordinal()) < Achievs.COMPLETE_100_PUZZLES.getCompletedAmount()) {
			c.setAchievInt(Achievs.COMPLETE_100_PUZZLES.ordinal(),
			c.getAchievInt(Achievs.COMPLETE_100_PUZZLES.ordinal()) + 1);
			setInProgress(Achievs.COMPLETE_100_PUZZLES.getStringId());
			if (c.getAchievInt(Achievs.COMPLETE_100_PUZZLES.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Complete " + Achievs.COMPLETE_100_PUZZLES.getCompletedAmount() + " Dungeoneering puzzles");
			}
			if (c.getAchievInt(Achievs.COMPLETE_100_PUZZLES.ordinal()) == Achievs.COMPLETE_100_PUZZLES.getCompletedAmount()) {
				setCompletedTask(Achievs.COMPLETE_100_PUZZLES.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Complete " + Achievs.COMPLETE_100_PUZZLES.getCompletedAmount() + " Dungeoneering puzzles");
				rewardHARD();
			}
		}
	}
	
	public void mine500Prom() {
		if (c.getAchievInt(
				Achievs.MINE_500_PROM.ordinal()) < Achievs.MINE_500_PROM.getCompletedAmount()) {
			c.setAchievInt(Achievs.MINE_500_PROM.ordinal(),
			c.getAchievInt(Achievs.MINE_500_PROM.ordinal()) + 1);
			setInProgress(Achievs.MINE_500_PROM.getStringId());
			if (c.getAchievInt(Achievs.MINE_500_PROM.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Mine " + Achievs.MINE_500_PROM.getCompletedAmount() + " promethium ores");
			}
			if (c.getAchievInt(Achievs.MINE_500_PROM.ordinal()) == Achievs.MINE_500_PROM.getCompletedAmount()) {
				setCompletedTask(Achievs.MINE_500_PROM.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Mine " + Achievs.MINE_500_PROM.getCompletedAmount() + " promethium ores");
				rewardHARD();
			}
		}
	}
	
	public void fish500CaveMoray() {
		if (c.getAchievInt(
				Achievs.FISH_500_CAVE_MORAY.ordinal()) < Achievs.FISH_500_CAVE_MORAY.getCompletedAmount()) {
			c.setAchievInt(Achievs.FISH_500_CAVE_MORAY.ordinal(),
			c.getAchievInt(Achievs.FISH_500_CAVE_MORAY.ordinal()) + 1);
			setInProgress(Achievs.FISH_500_CAVE_MORAY.getStringId());
			if (c.getAchievInt(Achievs.FISH_500_CAVE_MORAY.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Fish " + Achievs.FISH_500_CAVE_MORAY.getCompletedAmount() + " Raw Cave Moray");
			}
			if (c.getAchievInt(Achievs.FISH_500_CAVE_MORAY.ordinal()) == Achievs.FISH_500_CAVE_MORAY.getCompletedAmount()) {
				setCompletedTask(Achievs.FISH_500_CAVE_MORAY.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Fish " + Achievs.FISH_500_CAVE_MORAY.getCompletedAmount() + " Raw Cave Moray");
				rewardHARD();
			}
		}
	}
	
	public void chop500GraveCreeper() {
		if (c.getAchievInt(
				Achievs.CHOP_500_GRAVE_CREEPER.ordinal()) < Achievs.CHOP_500_GRAVE_CREEPER.getCompletedAmount()) {
			c.setAchievInt(Achievs.CHOP_500_GRAVE_CREEPER.ordinal(),
			c.getAchievInt(Achievs.CHOP_500_GRAVE_CREEPER.ordinal()) + 1);
			setInProgress(Achievs.CHOP_500_GRAVE_CREEPER.getStringId());
			if (c.getAchievInt(Achievs.CHOP_500_GRAVE_CREEPER.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Chop " + Achievs.CHOP_500_GRAVE_CREEPER.getCompletedAmount() + " Grave Creeper Branches");
			}
			if (c.getAchievInt(Achievs.CHOP_500_GRAVE_CREEPER.ordinal()) == Achievs.CHOP_500_GRAVE_CREEPER.getCompletedAmount()) {
				setCompletedTask(Achievs.CHOP_500_GRAVE_CREEPER.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Chop " + Achievs.CHOP_500_GRAVE_CREEPER.getCompletedAmount() + " Grave Creeper Branches");
				rewardHARD();
			}
		}
	}
	
	public void Pick300SpiritBlooms() {
		if (c.getAchievInt(
				Achievs.PICK_300_SPIRITBLOOM.ordinal()) < Achievs.PICK_300_SPIRITBLOOM.getCompletedAmount()) {
			c.setAchievInt(Achievs.PICK_300_SPIRITBLOOM.ordinal(),
			c.getAchievInt(Achievs.PICK_300_SPIRITBLOOM.ordinal()) + 1);
			setInProgress(Achievs.PICK_300_SPIRITBLOOM.getStringId());
			if (c.getAchievInt(Achievs.PICK_300_SPIRITBLOOM.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Pick " + Achievs.PICK_300_SPIRITBLOOM.getCompletedAmount() + " spiritblooms");
			}
			if (c.getAchievInt(Achievs.PICK_300_SPIRITBLOOM.ordinal()) == Achievs.PICK_300_SPIRITBLOOM.getCompletedAmount()) {
				setCompletedTask(Achievs.PICK_300_SPIRITBLOOM.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Pick " + Achievs.PICK_300_SPIRITBLOOM.getCompletedAmount() + " spiritblooms");
				rewardHARD();
			}
		}
	}
	
	public void make50StrongGatherer() {
		if (c.getAchievInt(
				Achievs.MAKE_50_STRONG_GATHERER.ordinal()) < Achievs.MAKE_50_STRONG_GATHERER.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_50_STRONG_GATHERER.ordinal(),
			c.getAchievInt(Achievs.MAKE_50_STRONG_GATHERER.ordinal()) + 1);
			setInProgress(Achievs.MAKE_50_STRONG_GATHERER.getStringId());
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_GATHERER.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make " + Achievs.MAKE_50_STRONG_GATHERER.getCompletedAmount() + " strong gatherer potions");
			}
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_GATHERER.ordinal()) == Achievs.MAKE_50_STRONG_GATHERER.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_50_STRONG_GATHERER.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Make " + Achievs.MAKE_50_STRONG_GATHERER.getCompletedAmount() + " strong gatherer potions");
				rewardHARD();
			}
		}
	}
	
	public void make50StrongArtisan() {
		if (c.getAchievInt(
				Achievs.MAKE_50_STRONG_ARTISAN.ordinal()) < Achievs.MAKE_50_STRONG_ARTISAN.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_50_STRONG_ARTISAN.ordinal(),
			c.getAchievInt(Achievs.MAKE_50_STRONG_ARTISAN.ordinal()) + 1);
			setInProgress(Achievs.MAKE_50_STRONG_ARTISAN.getStringId());
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_ARTISAN.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make " + Achievs.MAKE_50_STRONG_ARTISAN.getCompletedAmount() + " strong artisan potions");
			}
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_ARTISAN.ordinal()) == Achievs.MAKE_50_STRONG_ARTISAN.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_50_STRONG_ARTISAN.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Make " + Achievs.MAKE_50_STRONG_ARTISAN.getCompletedAmount() + " strong artisan potions");
				rewardHARD();
			}
		}
	}
	
	public void make50StrongNatural() {
		if (c.getAchievInt(
				Achievs.MAKE_50_STRONG_NATURAL.ordinal()) < Achievs.MAKE_50_STRONG_NATURAL.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_50_STRONG_NATURAL.ordinal(),
			c.getAchievInt(Achievs.MAKE_50_STRONG_NATURAL.ordinal()) + 1);
			setInProgress(Achievs.MAKE_50_STRONG_NATURAL.getStringId());
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_NATURAL.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make " + Achievs.MAKE_50_STRONG_NATURAL.getCompletedAmount() + " strong natural potions");
			}
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_NATURAL.ordinal()) == Achievs.MAKE_50_STRONG_NATURAL.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_50_STRONG_NATURAL.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Make " + Achievs.MAKE_50_STRONG_NATURAL.getCompletedAmount() + " strong natural potions");
				rewardHARD();
			}
		}
	}
	
	public void make50StrongSurvival() {
		if (c.getAchievInt(
				Achievs.MAKE_50_STRONG_SURVIVAL.ordinal()) < Achievs.MAKE_50_STRONG_SURVIVAL.getCompletedAmount()) {
			c.setAchievInt(Achievs.MAKE_50_STRONG_SURVIVAL.ordinal(),
			c.getAchievInt(Achievs.MAKE_50_STRONG_SURVIVAL.ordinal()) + 1);
			setInProgress(Achievs.MAKE_50_STRONG_SURVIVAL.getStringId());
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_SURVIVAL.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Make " + Achievs.MAKE_50_STRONG_SURVIVAL.getCompletedAmount() + " strong survival potions");
			}
			if (c.getAchievInt(Achievs.MAKE_50_STRONG_SURVIVAL.ordinal()) == Achievs.MAKE_50_STRONG_SURVIVAL.getCompletedAmount()) {
				setCompletedTask(Achievs.MAKE_50_STRONG_SURVIVAL.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Make " + Achievs.MAKE_50_STRONG_SURVIVAL.getCompletedAmount() + " strong survival potions");
				rewardHARD();
			}
		}
	}
	
	public void capture5Pets() {
		if (c.getAchievInt(
				Achievs.CAPTURE_5_PETS.ordinal()) < Achievs.CAPTURE_5_PETS.getCompletedAmount()) {
			c.setAchievInt(Achievs.CAPTURE_5_PETS.ordinal(),
			c.getAchievInt(Achievs.CAPTURE_5_PETS.ordinal()) + 1);
			setInProgress(Achievs.CAPTURE_5_PETS.getStringId());
			if (c.getAchievInt(Achievs.CAPTURE_5_PETS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Capture at least " + Achievs.CAPTURE_5_PETS.getCompletedAmount() + " pets");
			}
			if (c.getAchievInt(Achievs.CAPTURE_5_PETS.ordinal()) == Achievs.CAPTURE_5_PETS.getCompletedAmount()) {
				setCompletedTask(Achievs.CAPTURE_5_PETS.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Capture at least " + Achievs.CAPTURE_5_PETS.getCompletedAmount() + " pets");
				rewardHARD();
			}
		}
	}
	
	public void LevelPetTo10() {
		if (c.getAchievInt(Achievs.LEVEL_PET_TO_10.ordinal()) < Achievs.LEVEL_PET_TO_10
				.getCompletedAmount()) {
			c.setAchievInt(Achievs.LEVEL_PET_TO_10.ordinal(),
					c.getAchievInt(Achievs.LEVEL_PET_TO_10.ordinal()) + 1);
			setCompletedTask(Achievs.LEVEL_PET_TO_10.getStringId());
			c.sendMessage("<img=6>You have completed the achievement Level a pet to level 10.");
			achivementsCompleted();
			rewardHARD();
		}
	}
	
	public void play30LMS() {
		if (c.getAchievInt(
				Achievs.PLAY_30_LMS.ordinal()) < Achievs.PLAY_30_LMS.getCompletedAmount()) {
			c.setAchievInt(Achievs.PLAY_30_LMS.ordinal(),
			c.getAchievInt(Achievs.PLAY_30_LMS.ordinal()) + 1);
			setInProgress(Achievs.PLAY_30_LMS.getStringId());
			if (c.getAchievInt(Achievs.PLAY_30_LMS.ordinal()) < 2) {
				c.sendMessage("You made progress towards <col=ff0000>Participate in " + Achievs.PLAY_30_LMS.getCompletedAmount() + " LMS games");
			}
			if (c.getAchievInt(Achievs.PLAY_30_LMS.ordinal()) == Achievs.PLAY_30_LMS.getCompletedAmount()) {
				setCompletedTask(Achievs.PLAY_30_LMS.getStringId());
				c.sendMessage("<img=6>You have completed the achievement Participate in " + Achievs.PLAY_30_LMS.getCompletedAmount() + " LMS games.");
				rewardHARD();
			}
		}
	}

}
