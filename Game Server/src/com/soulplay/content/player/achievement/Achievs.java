package com.soulplay.content.player.achievement;

import java.util.HashMap;
import java.util.Map;

// public class Achievs {

public enum Achievs {
	// EASY TAKS
	SET_BANK_PIN(28030, 1, AchievType.EASY),
	KILL_20_CRABS(28031, 20, AchievType.EASY),
	BURY_20_BONES(28032, 20, AchievType.EASY),
	SETUP_CANNON(28033, 1, AchievType.EASY),
	PICK_30_FLAX(28034, 30, AchievType.EASY),
	LIGHT_20_FIRES(28035, 20, AchievType.EASY),
	FISH_30_FISHES(28036, 30, AchievType.EASY),
	SLAYERTASK(28037, 1, AchievType.EASY),
	STEAL_50_TIMES(28038, 50, AchievType.EASY),
	CHOP_30_LOGS(28039, 30, AchievType.EASY),
	MINE_30_ORES(28040, 30, AchievType.EASY),
	COOK_30_FISHES(28041, 30, AchievType.EASY),
	BURN_A_FISH(28042, 1, AchievType.EASY),
	CRAFT_20_STR_AMULETS(28043, 20, AchievType.EASY),
	FARM_A_HERB(28044, 1, AchievType.EASY),
	EAT_20_LOBSTERS(28045, 20, AchievType.EASY),
	SPECIALATTACK(28046, 1, AchievType.EASY),
	SUMMON_SPIRIT_SPIDER(28047, 1, AchievType.EASY),
	MAKE_15_SPIRITWOLF(28048, 15, AchievType.EASY),
	SMELT_10_BRONZEBRAS(28049, 10, AchievType.EASY),
	COMPLETE_FROZEN_FLOORS(28050, 1, AchievType.EASY),
	CRAFT_100_AIR(28051, 100, AchievType.EASY),
	GNOMELAPS_10(28052, 10, AchievType.EASY),
	DEFEAT_A_PLAYER(28053, 1, AchievType.EASY),
	CHANGE_APPEARANCE(28054, 1, AchievType.EASY),
	VOTE_FOR_FP(28055, 1, AchievType.EASY),

	// MEDIUM TASKS
	KILL_500_MONSTERS(28059, 500, AchievType.MEDIUM),
	KILL_A_REVENANT(28060, 1, AchievType.MEDIUM),
	WIN_A_DUEL(28061, 1, AchievType.MEDIUM),
	SUMMON_20_GRANITE_CRABS(28062, 20, AchievType.MEDIUM),
	CATCH_75_CHINS(28063, 75, AchievType.MEDIUM),
	THEIV_300K(28064, 300000, AchievType.MEDIUM),
	KILL_FROSTDRAGON(28065, 1, AchievType.MEDIUM),
	KILL_20_DRAGONS(28066, 20, AchievType.MEDIUM),
	BURY_100_DRAG_BONES(28067, 100, AchievType.MEDIUM),
	GET_99_SKILL(28068, 1, AchievType.MEDIUM),
	FISH_200_SHARKS(28069, 200, AchievType.MEDIUM),
	COOK_100_MANTA(28070, 100, AchievType.MEDIUM),
	MINE_200_ADDYS(28071, 200, AchievType.MEDIUM),
	CHOP_200_YEWS(28072, 200, AchievType.MEDIUM),
	CRAFT_50_PHOENIX_NECK(28073, 50, AchievType.MEDIUM),
	CRAFT_200_LAWS(28074, 200, AchievType.MEDIUM),
	DUO_TASK(28075, 1, AchievType.MEDIUM),
	SUMONA_SLAYERTASK(28076, 1, AchievType.MEDIUM),
	DEFEAT_10_PLAYERS(28077, 10, AchievType.MEDIUM),
	KILLSTREAK_3(28078, 3, AchievType.MEDIUM),
	DIE_20_TIME(28079, 20, AchievType.MEDIUM),
	ENTER_LOTTERY(28080, 1, AchievType.MEDIUM),
	BARBLAPS_10(28081, 10, AchievType.MEDIUM),
	VOTE_10_TIMES(28082, 10, AchievType.MEDIUM),

	// HARD TASKS
	BURY_25_FROSTBONES(28086, 25, AchievType.HARD),
	FISH_500_MANTAS(28087, 500, AchievType.HARD),
	COOK_500_ROCKTAILS(28088, 500, AchievType.HARD),
	MINE_100_RUNE(28089, 100, AchievType.HARD),
	SMELT_50_RUNE(28090, 50, AchievType.HARD),
	CHOP_500_MAGIC(28091, 500, AchievType.HARD),
	LIGHT_200_MAGIC(28092, 200, AchievType.HARD),
	THEIV_10M(28093, 10000000, AchievType.HARD),
	CRAFT_20_FURYS(28094, 20, AchievType.HARD),
	CRAFT_10K_RUNES(28095, 10000, AchievType.HARD),
	CATCH_100_GRENWALLS(28096, 100, AchievType.HARD),
	BUILD_GILDED_ALTAR(28097, 1, AchievType.HARD),
	WIN_10_PC(28098, 10, AchievType.HARD),
	MAKE_10_OVL(28099, 10, AchievType.HARD),
	DEFEAT_JAD(28100, 1, AchievType.HARD),
	KILL_CHAOS_ELE(28101, 1, AchievType.HARD),
	FIND_A_DARKBOW(28102, 1, AchievType.HARD),
	SMITH_RUNE_PLATE(28103, 1, AchievType.HARD),
	EXHANGE_ARTIFACT(28104, 1, AchievType.HARD),
	FIND_DRAGON_CHAIN(28105, 1, AchievType.HARD),
	KURADAL_SLAYERTASK(28106, 1, AchievType.HARD),
	COMPLETE_ALL_FURNISHED(28107, 1, AchievType.HARD),
	HIT_70_MAGIC(28108, 1, AchievType.HARD),
	CAST_DFS_SPEC(28109, 1, AchievType.HARD),
	EARN_2K_PKP(28110, 2000, AchievType.HARD),
	KILLSTREAK_15(28111, 15, AchievType.HARD),
	ELO_1400(28112, 1400, AchievType.HARD),
	DUEL_50_TIMES(28113, 50, AchievType.HARD),
	KILL_KBD(28114, 1, AchievType.HARD),
	KILL_5_TORMS(28115, 5, AchievType.HARD),
	VOTE_50_TIMES(28116, 50, AchievType.HARD),

	// ELITE TASKS
	KILL_10K_MONSTERS(28120, 10000, AchievType.ELITE),
	MINE_300_RUNE(28121, 300, AchievType.ELITE),
	CATCH_500_GRENWALLS(28122, 500, AchievType.ELITE),
	CATCH_500_ROCKTAILS(28123, 500, AchievType.ELITE),
	CRAFT_500_BLACKDHIDE(28124, 500, AchievType.ELITE),
	WIN_50_PC(28125, 50, AchievType.ELITE),
	DEFEAT_60_PLAYERS(28126, 60, AchievType.ELITE),
	KILL_WITH_VENG(28127, 1, AchievType.ELITE),
	DEFEAT_TARGET(28128, 1, AchievType.ELITE),
	KILLSTREAK_20(28129, 20, AchievType.ELITE),
	ELO_1600(28130, 1600, AchievType.ELITE),
	WIN_1_GAME_OF_LMS(28131, 1, AchievType.ELITE),
	OPEN_50_CHESTS(28132, 50, AchievType.ELITE),
	DEFEAT_10_NEX(28133, 10, AchievType.ELITE),
	DEFEAT_CORP_20(28134, 20, AchievType.ELITE),
	DEFEAT_35_GWD(28135, 35, AchievType.ELITE),
	KILL_150_TORMS(28136, 150, AchievType.ELITE),
	COMPLETE_FLOOR60_C6_LARGE(28137, 1, AchievType.ELITE),
	DEFEAT_DAGG_BOSSES(28138, 40, AchievType.ELITE),
	DEFEAT_AOD(28139, 1, AchievType.ELITE),
	DEFEAT_WILDYWYRM(28140, 1, AchievType.ELITE),
	PROGRESS_60_DUNGEONEERING(28141, 1, AchievType.ELITE),
	COMPLETE_50_WARPED_FLOORS(28142, 50, AchievType.ELITE),
	EARN_200_SLAYP(28143, 200, AchievType.ELITE),
	EARN_20_DUOP(28144, 20, AchievType.ELITE),
	FIRE_5K_CBALLS(28145, 5000, AchievType.ELITE),
	FIND_DRAGON_PICK(28146, 1, AchievType.ELITE),
	BUILD_MAGICAL_CAGE(28147, 1, AchievType.ELITE),
	OBTAIN_TOKHAAR(28148, 1, AchievType.ELITE),
	BUILD_DEMONIC_THRONE(28149, 1, AchievType.ELITE),
	MAKE_600_STEEL_TITAN(28150, 600, AchievType.ELITE),
	HEAL_50K_HP(28151, 50000, AchievType.ELITE),
	HIT_90_MELEE(28152, 1, AchievType.ELITE),
	VOTE_250_TIMES(28153, 250, AchievType.ELITE),
	CHOP_500_MAHOGANY(28154, 500, AchievType.ELITE),
	EARN_MAX_FAVOUR(28155, 1, AchievType.ELITE),
	COMPLETE_20_LARGE_FLOORS(28156, 20, AchievType.ELITE),
	COMPLETE_100_PUZZLES(28157, 100, AchievType.ELITE),
	MINE_500_PROM(28158, 500, AchievType.ELITE),
	FISH_500_CAVE_MORAY(28159, 500, AchievType.ELITE),
	CHOP_500_GRAVE_CREEPER(28160, 500, AchievType.ELITE),
    PICK_300_SPIRITBLOOM(28161, 300, AchievType.ELITE),
    MAKE_50_STRONG_GATHERER(28162, 50, AchievType.ELITE),
    MAKE_50_STRONG_ARTISAN(28163, 50, AchievType.ELITE),
    MAKE_50_STRONG_NATURAL(28164, 50, AchievType.ELITE),
    MAKE_50_STRONG_SURVIVAL(28165, 50, AchievType.ELITE),
    CAPTURE_5_PETS(28166, 5, AchievType.ELITE),
    LEVEL_PET_TO_10(28167, 1, AchievType.ELITE),
    PLAY_30_LMS(28168, 30, AchievType.ELITE),
	;

	private final int stringId, completedAmount, taskType;

	private Achievs(int stringId, int completedAmount, int type) {
		this.stringId = stringId;
		this.completedAmount = completedAmount;
		this.taskType = type;
	}

	public int getCompletedAmount() {
		return completedAmount;
	}

	public int getStringId() {
		return stringId;
	}

	public int getTaskType() {
		return taskType;
	}
	
	public static final Achievs[] values = values();
	
	public static final Map<Integer, Integer> buttonIds = new HashMap<>();
	
	private static final int FIRST_BUTTON = 109126;
	private static final int LAST_BUTTON = 110008;
	public static void fillButtons() {
		int index = 0;
		for (int i = 0, len = LAST_BUTTON - FIRST_BUTTON; i <= len; i++) {
			int buttonId = i + FIRST_BUTTON;
			if (buttonId == 109151 || buttonId == 109178 || buttonId == 109212) {
				i += 3;
			}
			if (buttonId == 109255) {
				i += 744;
			}
			buttonIds.put(buttonId, index);
			index++;
		}
		//System.out.println("buttons added:"+ buttonIds.size() + " achievs size:"+values.length);
	}

}
