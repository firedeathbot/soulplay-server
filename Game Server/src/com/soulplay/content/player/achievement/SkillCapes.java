package com.soulplay.content.player.achievement;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;
import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;

public class SkillCapes {

	public static enum SkillCapesConfig {
		ATK("Attack", 9747, 9748, 4959, 823, Skills.ATTACK),
		DEF("Defence", 9753, 9754, 4961, 824, Skills.DEFENSE),
		STR("Strength", 9750, 9751, 4981, 828, Skills.STRENGTH),
		HP("Hitpoints", 9768, 9769, 14242, 2745, Skills.HITPOINTS),
		RNGE("Range", 9756, 9757, 4973, 832, Skills.RANGED),
		PRAY("Prayer", 9759, 9760, 4979, 829, Skills.PRAYER),
		MGE("Magic", 9762, 9763, 4939, 813, Skills.MAGIC),
		COOK("Cooking", 9801, 9802, 4955, 821, Skills.COOKING),
		WC("Woodcutting", 9807, 9808, 4957, 822, Skills.WOODCUTTING),
		FLTCH("Fletching", 9783, 9784, 4937, 812, Skills.FLETCHING),
		FISH("Fishing", 9798, 9799, 4951, 819, Skills.FISHING),
		FM("Firemaking", 9804, 9805, 4975, 831, Skills.FIREMAKING),
		CRFT("Crafting", 9780, 9781, 4949, 818, Skills.CRAFTING),
		SMTH("Smithing", 9795, 9796, 4943, 815, Skills.SMITHING),
		MINNG("Mining", 9792, 9793, 4941, 814, Skills.MINING),
		HRB("Herblore", 9774, 9775, 4969, 835, Skills.HERBLORE),
		AGL("Agility", 9771, 9772, 4977, 830, Skills.AGILITY),
		THVE("Thieving", 9777, 9778, 4965, 826, Skills.THIEVING),
		SLYER("Slayer", 9786, 9787, 4967, 1656, Skills.SLAYER),
		FRMING("Farming", 9810, 9811, 4963, 825, Skills.FARMING),
		RNECRFT("Runecrafting", 9765, 9766, 4947, 817, Skills.RUNECRAFTING),
		HNTR("Hunter", 9948, 9949, 5158, 907, Skills.HUNTER),
		CNSTR("Construction", 9789, 9790, 4953, 820, Skills.CONSTRUCTION),
		SMMING("Summoning", 12169, 12170, 8525, 1515, Skills.SUMMONING),
		QST("Quest", 9813, -1, 4945, 816, -1);

		public final String skill;

		public final int skillCape, skillCapeTrimmed, skillId;
		private final Animation anim;
		private final Graphic gfx;

		SkillCapesConfig(String skill, int skillCape, int skillCapeTrimmed, int anim, int gfx, int skillId) {
			this.skill = skill;
			this.skillCape = skillCape;
			this.skillCapeTrimmed = skillCapeTrimmed;
			if (anim != -1) {
				this.anim = new Animation(anim);
			} else {
				this.anim = null;
			}
			if (gfx != -1) {
				this.gfx = Graphic.create(gfx);
			} else {
				this.gfx = null;
			}
			this.skillId = skillId;
		}
	}

	public static final Map<Integer, SkillCapesConfig> map = new HashMap<>();
	
	static {
		for (SkillCapesConfig skc : SkillCapesConfig.values()) {
			map.put(skc.skillCape, skc);
			map.put(skc.skillCapeTrimmed, skc);
		}
	}
	
    public static void load() {
    	/* empty */
    }
   
	public static void emote(final Client c) {
		if (c.getTransformId() >= 0) {
			c.sendMessage("You can't do a skillcape emote while you're transformed into npc.");
			return;
		}

		int capeId = c.playerEquipment[PlayerConstants.playerCape];
		if (capeId == -1) {
			return;
		}

		if (capeId == 18508 || capeId == 18509) {
			startDungCapeEmote(c);
			return;
		}

		if (capeId == 20769 || (capeId >= 14000 && capeId < 14025)) {
			beginCompCapeEmote(c);
			return;
		}

		SkillCapesConfig skc = map.get(capeId);

		if (skc == null) {
			return;
		}

		if (c.getSkills().getStaticLevel(skc.skillId) < 99) {
			c.sendMessage("You need a level of 99 " + skc.skill + " to perform the " + skc.skill + " emote!");
			return;
		}

		if (skc.anim != null) {
			c.startAnimation(skc.anim);
		}
		if (skc.gfx != null) {
			c.startGraphic(skc.gfx);
		}
	}

	private static final Graphic SMOKE_GFX = Graphic.create(2442);
	private static final Animation MORPH_ANIM = new Animation(13190);

	private static void startDungCapeEmote(Client c) {
		if (c.performingAction) {
			return;
		}

		if (c.getSkills().getStaticLevel(Skills.DUNGEONEERING) < 99) {
			c.sendMessage("You need a level of 99 Dungeoneering to perform the emote!");
			return;
		}

		int npc;
		
		int meleeBonus = Math.max(c.getPlayerBonus()[EquipmentBonuses.ATTACK_STAB.getId()], Math.max(c.getPlayerBonus()[EquipmentBonuses.ATTACK_SLASH.getId()], c.getPlayerBonus()[EquipmentBonuses.ATTACK_CRUSH.getId()]));
		int mageBonus = c.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()];
		int rangeBonus = c.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()];
		if (meleeBonus > mageBonus && meleeBonus > rangeBonus) {
			npc = 0;
		} else if (mageBonus > meleeBonus && mageBonus > rangeBonus) {
			npc = 1;
		} else if (rangeBonus > meleeBonus && rangeBonus > mageBonus) {
			npc = 2;
		} else {
			npc = Misc.random(2);
		}

		int startAnim = 13192 + npc;
		int startNpc = 11227 + npc;
		c.performingAction = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int timer = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (timer == 0) {
					c.startGraphic(SMOKE_GFX);
					c.startAnimation(MORPH_ANIM);
				} else if (timer == 1) {
					c.transform(startNpc);
					c.startAnimation(startAnim);
				} else if (timer == 6) {
					c.transform(-1);
					c.performingAction = false;
					container.stop();
				}

				timer++;
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 1);
	}

	public static void beginCompCapeEmote(Client c) {
		if (c.performingAction) {
			return;
		}

		c.performingAction = true;
		Server.getTaskScheduler().schedule(new Task() {

			int timer = 0;

			@Override
			protected void execute() {
				if (timer == 0) {
					c.startGraphic(Graphic.create(307, GraphicType.HIGH));
					c.startAnimation(356);
				} else if (timer == 4) {
					c.transform(1830);
					c.startAnimation(1174);
					c.startGraphic(Graphic.create(1443, GraphicType.LOW));
				} else if (timer == 16) {
					c.transform(-1);
					c.startAnimation(1175);
					c.performingAction = false;
					this.stop();
				}
				timer++;

			}
		});
	}
}
