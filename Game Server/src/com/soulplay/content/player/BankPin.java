package com.soulplay.content.player;

import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

/**
 * Handles the BankPin on Bank's
 *
 * @author Michael
 * @author Ian / Core
 * @author Linus
 * @author Genesis
 */

public class BankPin {

	public int recovery_Delay = 3;

	private Client client;

	public int allowTimer = 2000000;

	private int bankPins[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	private static final int pinSendFrames2[] = {15075, 15080, 15110, 15171, 15076, 15176, 15104,
			15082, 15079};

	private static final int pinSendFrames[] = {14913, 14914, 14915, 14916};

	private static final int stringIds[] = {14883, 14884, 14885, 14886, 14887, 14888, 14889,
			14890, 14891, 14892};

	private static final int actionButtons[] = {58025, 58026, 58027, 58028, 58029, 58030,
			58031, 58032, 58033, 58034};

	private int firstPin;

	private int secondPin;

	private int thirdPin;

	private int fourthPin;

	public BankPin(Client client) {
		this.client = client;
	}

	public void bankPinEnter(int button) {
		if (!checkTimer())
			return;
		sendPins();
		if (!client.firstPinEnter) {
			handleButtonOne(button);
		} else if (!client.secondPinEnter) {
			handleButtonTwo(button);
		} else if (!client.thirdPinEnter) {
			handleButtonThree(button);
		} else if (!client.fourthPinEnter) {
			handleButtonFour(button);
		}
	}
	
	public void bankPinEnterThroughText(boolean match) {
		if (!client.hasBankPin) {
			client.sendMessage("You must create a Bank Pin only by Clicking the Red Buttons!");
			client.getPA().closeAllWindows();
			return;
		}
		if (!checkTimer())
			return;
		
		enteredAllPins(match);
	}
	
	private boolean checkTimer() {
		if (allowTimer > 0 && allowTimer <= 300000) {
			int time = allowTimer / 6000;
			if (time >= 2) {
				client.sendMessage("Please wait " + time
						+ " minutes before attempting your bank pin again.");
			} else if (time == 1) {
				client.sendMessage("Please wait " + time
						+ " minute before attempting your bank pin again.");
			} else if (time <= 0) {
				client.sendMessage("Please wait less "
						+ "than a minute before attempting your bank pin again.");
			}
			return false;
		}
		return true;
	}

	public void bankPinSettings() {
		pinSettingFrames();
		client.getPacketSender().showInterface(14924);
	}

	public void closeBankPin() {
		firstPin = secondPin = thirdPin = fourthPin = client.playerBankPin = 0;
		falseButtons();
		client.getPA().closeAllWindows(); // client.getPA().removeAllWindows();
	}

	/**
	 * pin deletion request has passed the 3 days
	 *
	 * @return
	 */
	public boolean dateExpired() {
		return Server.getCalendar().getDayOfYear()
				- client.pinDeleteDateRequested >= 3;
	}

	public boolean falseButtons() {
		return client.fourthPinEnter = client.thirdPinEnter = client.secondPinEnter = client.firstPinEnter = false;
	}

	private int[] getBankPins() {
		return bankPins;
	}

	private void handleButtonFour(int button) {
		if (client.hasBankPin && client.enterdBankpin) {
//			client.getPA().openUpBank();
			return;
		}
		client.getPacketSender().sendFrame126("*", 14916);
		for (int i = 0; i < actionButtons.length; i++) {
			if (actionButtons[i] == button) {
				fourthPin = getBankPins()[i];
			}
		}
		client.fourthPinEnter = true;
		if (!client.hasBankPin) {
			client.firstPin = firstPin;
			client.setBankPin(0, firstPin);
			client.secondPin = secondPin;
			client.setBankPin(1, secondPin);
			client.thirdPin = thirdPin;
			client.setBankPin(2, thirdPin);
			client.fourthPin = fourthPin;
			client.setBankPin(3, fourthPin);
			client.hasBankPin = client.enterdBankpin = true;
			client.sendMessage("You have just created a bank pin.");
			client.sendMessage("Your new Bank PIN is: "
					+ client.getBankPins()[0] + " - " + client.getBankPins()[1]
					+ " - " + client.getBankPins()[2] + " - "
					+ client.getBankPins()[3]);
			client.getAchievement().setBankPin();
		}

		boolean match = false;
		
		if (client.getBankPins()[0] == firstPin && client.getBankPins()[1] == secondPin
				&& client.getBankPins()[2] == thirdPin
				&& client.getBankPins()[3] == fourthPin)
			match = true;
		
		enteredAllPins(match);
	}
	
	private void enteredAllPins(boolean match) {
		if (match) {
			falseButtons();
			boolean openBank = client.bankPinOpensBank;
			client.getPA().closeAllWindows(); // client.getPA().removeAllWindows();
			client.enterdBankpin = true;
			client.playerBankPin = 15000;
			if (openBank)
				client.getPA().openUpBank();

		} else {
			client.attemptsRemaining--;
			if (client.attemptsRemaining <= 0) {
				allowTimer = 30000;
			}
			if (client.attemptsRemaining == -1) {
				client.attemptsRemaining = 3;
				allowTimer = 2000000;
			}
			if (client.attemptsRemaining > 1) {
				client.sendMessage("Invalid pin. You have "
						+ client.attemptsRemaining + " attempts remaining.");
			} else if (client.attemptsRemaining == 1) {
				client.sendMessage("Invalid pin. You have "
						+ client.attemptsRemaining + " attempt remaining.");
			} else if (client.attemptsRemaining <= 0) {
				client.sendMessage(
						"Invalid pin. You must wait 5 minutes before attempting again.");
			}
			client.getPA().closeAllWindows(); // client.getPA().removeAllWindows();
			falseButtons();
		}
	}
	
	private void handleButtonOne(int button) {
		client.getPacketSender().sendFrame126("Now click the SECOND digit",
				15313);
		client.getPacketSender().sendFrame126("*", 14913);
		for (int i = 0; i < actionButtons.length; i++) {
			if (actionButtons[i] == button) {
				firstPin = getBankPins()[i];
			}
		}
		client.firstPinEnter = true;
		randomizeNumbers();
	}

	private void handleButtonThree(int button) {
		client.getPacketSender().sendFrame126("Now click the LAST digit",
				15313);
		client.getPacketSender().sendFrame126("*", 14915);
		for (int i = 0; i < actionButtons.length; i++) {
			if (actionButtons[i] == button) {
				thirdPin = getBankPins()[i];
			}
		}
		client.thirdPinEnter = true;
		randomizeNumbers();
	}

	private void handleButtonTwo(int button) {
		client.getPacketSender().sendFrame126("Now click the THIRD digit",
				15313);
		client.getPacketSender().sendFrame126("*", 14914);
		for (int i = 0; i < actionButtons.length; i++) {
			if (actionButtons[i] == button) {
				secondPin = getBankPins()[i];
			}
		}
		client.secondPinEnter = true;
		randomizeNumbers();
	}

	public void openPin() {
		client.openBankPinInterface = true;
		if (client.hasBankPin && client.enterdBankpin) {
//			client.getPA().openUpBank();
			return;
		}
		randomizeNumbers();
		client.getPacketSender().sendFrame126("First click the FIRST digit",
				15313);
		client.getPacketSender().sendFrame126("", 14923);
		for (int j = 0; j < 4; j++) {
			client.getPacketSender().sendFrame126("?", pinSendFrames[j]);
		}
		client.getPacketSender().showInterface(7424);
		sendPins();
	}

	public void pinSettingFrames() {
		for (int j = 0; j < 9; j++) {
			client.getPacketSender().sendFrame126("", pinSendFrames2[j]);
		}
		client.getPacketSender().sendFrame126("Welcome to our bank", 15038);
		client.getPacketSender().sendFrame126("recovery system.", 15039);
		client.getPacketSender().sendFrame126("Remember, it's important",
				15040);
		client.getPacketSender().sendFrame126("to change your recovery", 15041);
		client.getPacketSender().sendFrame126("pin and password", 15042);
		client.getPacketSender().sendFrame126("every 1-3 months", 15043);
		if (!client.hasBankPin) {
			client.getPacketSender().sendFrame126("Set a Bank Pin", 15078);
			client.getPacketSender().sendFrame126("No PIN Set", 15105);
		} else {
			client.getPacketSender().sendFrame126("Delete your PIN", 15078);
			if (client.requestPinDelete) {
				client.getPacketSender().sendFrame126("Pending delete", 15105);
			} else {
				client.getPacketSender().sendFrame126("Has Bank PIN", 15105);
			}
		}
		client.getPacketSender().sendFrame126(recovery_Delay + " days", 15107);
	}

	private void randomizeNumbers() {
		int i = Misc.random(4);
		if (i == client.lastPinSettings) {
			i = (client.lastPinSettings == 0
					? client.lastPinSettings
					: client.lastPinSettings);
		}
		switch (i) {
			case 0:
				bankPins[0] = 1;
				bankPins[1] = 7;
				bankPins[2] = 0;
				bankPins[3] = 8;
				bankPins[4] = 4;
				bankPins[5] = 6;
				bankPins[6] = 5;
				bankPins[7] = 9;
				bankPins[8] = 3;
				bankPins[9] = 2;
				break;

			case 1:
				bankPins[0] = 5;
				bankPins[1] = 4;
				bankPins[2] = 3;
				bankPins[3] = 7;
				bankPins[4] = 8;
				bankPins[5] = 6;
				bankPins[6] = 9;
				bankPins[7] = 2;
				bankPins[8] = 1;
				bankPins[9] = 0;
				break;

			case 2:
				bankPins[0] = 4;
				bankPins[1] = 7;
				bankPins[2] = 6;
				bankPins[3] = 5;
				bankPins[4] = 2;
				bankPins[5] = 3;
				bankPins[6] = 1;
				bankPins[7] = 8;
				bankPins[8] = 9;
				bankPins[9] = 0;
				break;

			case 3:
				bankPins[0] = 9;
				bankPins[1] = 4;
				bankPins[2] = 2;
				bankPins[3] = 7;
				bankPins[4] = 8;
				bankPins[5] = 6;
				bankPins[6] = 0;
				bankPins[7] = 3;
				bankPins[8] = 1;
				bankPins[9] = 5;
				break;
		}
		client.lastPinSettings = i;
		sendPins();
	}

	private int resetBankNumbers() {
		return firstPin = secondPin = thirdPin = fourthPin = client.playerBankPin = client.firstPin = client.secondPin = client.thirdPin = client.fourthPin = -1;
	}

	public boolean resetBankPin() {
		resetBankNumbers();
		falseButtons();
		client.getPA().closeAllWindows();
		return client.hasBankPin = false;
	}

	private void sendPins() {
		if (client.hasBankPin && client.enterdBankpin) {
//			client.getPA().openUpBank();
			return;
		}
		for (int i = 0; i < getBankPins().length; i++) {
			client.getPacketSender().sendFrame126("" + getBankPins()[i],
					stringIds[i]);
		}
		
		client.getPacketSender().sendInputDialogState(7);
	}

	public void setDateRequestedForPinDeletion() {
		client.pinDeleteDateRequested = Server.getCalendar().getDayOfYear();
	}

	@Override
	public String toString() {
		return "BankPin{" + "recovery_Delay=" + recovery_Delay + ", allowTimer=" + allowTimer + ", bankPins="
				+ bankPins + ", firstPin=" + firstPin + ", secondPin="
				+ secondPin + ", thirdPin=" + thirdPin + ", fourthPin="
				+ fourthPin + '}';
	}
}
