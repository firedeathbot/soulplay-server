package com.soulplay.content.player;

import com.soulplay.content.npcs.impl.bosses.zulrah.ZulrahSpells;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

public enum TeleportType {
	
	ANCIENT(7, 19, 3, 1979, -1, 1681, -1),
	FAIRY(10, 20, 5, -1, -1, 569, 569),
	TAB(10, 20, 5, 4071, -1, 678, -1),
	OBELISK(11, 0, 5, 8939, -1, -1, -1),
	MODERN(11, 21, 5, 8939, 8941, 1576, 1577),
	GLORY(11, 21, 5, 8939, 8941, 1576, 1577),
	ROYAL_SEED_POD(-1, -1, -1, -1, -1, -1, -1),
	DUNG_GATESTONE(5, 21, 2, 13288, 13285, 2516, 2517),
	DUNG(15, 21, 2, 13652, 8941, 2602, 2603),
	BOUNTY_TELEPORT(7, 8, 0, 8939, -1, 1576, -1),
	ZULRAH(1, 1, 1, ZulrahSpells.HIDE_ANIM.getId(), ZulrahSpells.RISE_ANIM.getId(), -1, -1),
	NONE(1, 1, 1, -1, -1, -1, -1);

	private final int timer, cooldown, endDelay;
	private final Animation startAnimation, endAnimation;
	private final Graphic startGfx, endGfx;

	private TeleportType(int timer, int cooldown, int endDelay, int startAnimation, int endAnimation, int startGfx,
			int endGfx) {
		this.timer = timer;
		this.cooldown = cooldown;
		this.endDelay = endDelay;
		if (startAnimation != -1) {
			this.startAnimation = new Animation(startAnimation);
		} else {
			this.startAnimation = null;
		}
		if (endAnimation != -1) {
			this.endAnimation = new Animation(endAnimation);
		} else {
			this.endAnimation = null;
		}
		if (startGfx != -1) {
			this.startGfx = Graphic.create(startGfx);
		} else {
			this.startGfx = null;
		}
		if (endGfx != -1) {
			this.endGfx = Graphic.create(endGfx);
		} else {
			this.endGfx = null;
		}
	}

	public int getTimer() {
		return timer;
	}

	public int getCooldown() {
		return cooldown;
	}

	public Animation getStartAnimation() {
		return startAnimation;
	}

	public Animation getEndAnimation() {
		return endAnimation;
	}

	public Graphic getStartGfx() {
		return startGfx;
	}

	public Graphic getEndGfx() {
		return endGfx;
	}

	public int getEndDelay() {
		return endDelay;
	}
	

}
