package com.soulplay.content.player.pets;

import java.util.HashMap;

import com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl.Warmonger;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public enum Pet {

	// dogs and cats
	OVERGROWN_HELLCAT(3503, 7581),
	HELLCAT(3504, 7582),
	HELL_KITTEN(3505, 7583),
	LAZY_HELLCAT(3506, 7584),
	WILY_HELLCAT(3507, 7585),
	BULLDOG(6968, 12523),
	BULLDOG_PUPPY(6969, 12522),
	SHEEPDOG(6967, 12521),
	SHEEPDOG_PUPPY(6966, 12520),
	DALMATIER(6965, 12519),
	DALMATIER_PUPPY(6964, 12518),
	LABRADOR(6963, 12517),
	LABRADOR_PUPPY(6962, 12516),
	TERRIER(6959, 12513),
	TERRIER_PUPPY(6958, 12512),
	GREYHOUND(6961, 12515),
	GREYHOUND_PUPPY(6960, 12514),

	// monkeys
	BABY_MONKEY1(7210, 12496),
	MONKEY1(7211, 12497),
	BABY_MONKEY2(7212, 12682),
	MONKEY2(7213, 12683),
	BABY_MONKEY3(7214, 12684),
	MONKEY3(7215, 12685),
	BABY_MONKEY4(7216, 12686),
	MONKEY4(7217, 12687),
	BABY_MONKEY5(7218, 12688),
	MONKEY5(7219, 12689),
	BABY_MONKEY6(7220, 12690),
	MONKEY6(7221, 12691),
	BABY_MONKEY7(7222, 12692),
	MONKEY7(7223, 12693),
	BABY_MONKEY8(7224, 12696),
	MONKEY8(7224, 12697),
	BABY_MONKEY9(7226, 12698),
	MONKEY9(7227, 12699),
	// BABY_MONKEY10(7227, 12700),
	// MONKEY10(7228, 12701),

	// monsters from their own egg
	PENGUIN(6909, 12481), // in penguin hunt
	BABY_PENGUIN(6908, 12482),
	PHOENIX(8577, 14626),

	GUTHIX_RAPTOR(6957, 12187),
	SARADOMIN_OWL(6951, 12185),
	ZAMORAK_HAWK(6954, 12186),

	VULTURE_CHICK_BROWN(6945, 12498),
	VULTURE_BROWN(6946, 12499),
	VULTURE_CHICK_BROWN2(7319, 12766),
	VULTURE_BROWN2(7320, 12767),
	VULTURE_CHICK_YELLOW(7321, 12768),
	VULTURE_YELLOW(7322, 12769),
	VULTURE_CHICK_YELLOW2(7323, 12770),
	VULTURE_YELLOW2(7324, 12771),
	VULTURE_CHICK_WHITE(7325, 12772),
	VULTURE_WHITE(7326, 12773),
	VULTURE_CHICK_WHITE2(7327, 12774),
	VULTURE_WHITE2(7328, 12775),

	BABY_CHAMELEON_GREEN(6922, 12492),
	CHAMELEON_GREEN(6923, 12493),
	/*
	 * BABY_CHAMELEON_GREY(6924, 12498), CHAMELEON_GREY(6925, 12499),
	 * BABY_CHAMELEON_GREEN2(6926, 12498), CHAMELEON_GREEN2(6927, 12499),
	 * BABY_CHAMELEON_WHITE(6928, 12498), CHAMELEON_WHITE(6929, 12499),
	 * BABY_CHAMELEON_BROWN(6930, 12498), CHAMELEON_BROWN(6931, 12499),
	 * BABY_CHAMELEON_WHITE2(6932, 12498), CHAMELEON_WHITE2(6933, 12499),
	 * BABY_CHAMELEON_BLACK(6934, 12498), CHAMELEON_BLACK(6935, 12499),
	 * BABY_CHAMELEON_BLUE(6936, 12498), CHAMELEON_BLUE(6937, 12499),
	 * BABY_CHAMELEON_YELLOW(6938, 12498), CHAMELEON_YELLOW(6939, 12499),
	 * BABY_CHAMELEON_ORANGE(6940, 12498), CHAMELEON_ORANGE(6941, 12499),
	 */

	RAVEN_CHICK_BLACK(6911, 12484), // comes from its own egg
	RAVEN_BLACK(6912, 12485),
	RAVEN_CHICK_black2(7261, 12724),
	RAVEN_black2(7262, 12725),
	RAVEN_CHICK_GRYE(7263, 12726),
	RAVEN_GREY(7264, 12727),
	RAVEN_CHICK_GREY2(7265, 12728),
	RAVEN_GREY2(7266, 12729),
	RAVEN_CHICK_BROWN(7267, 12730),
	RAVEN_BROWN(7268, 12731),
	RAVEN_CHICK_BROWN2(7269, 12732),
	RAVEN_BROWN2(7270, 12733),

	BABY_DRAGON_RED(6901, 12470), // comes from eggs
	BABY_DRAGON_BLUE(6903, 12472),
	BABY_DRAGON_GREEN(6905, 12474),
	BABY_DRAGON_BLACK(6907, 12476),

	// monsters from monster egg
	CREEPING_HAND(8619, 14652),
	MINITRICE(8620, 14653),
	BABY_KURASK(8622, 14655),
	ABYSSA_MINION(8624, 14651),
	BROAV(8491, 14533), // not in use yet

	EX_EX_PARROT(7844, 13335), // not in use yet

	SNEAKERPEEPER_SPAWN(13089, 19894), // use this in dungeoneering shop
	SNEAKERPEEPER(13090, 19895),

	BABY_BASILISK(8621, 14654),

	// animals not from egg

	PLATYPUS_BROWN(7015, 12548),
	BABY_PLATYPUS_BROWN(7018, 12551),
	PLATYPUS_LIGHTBROWN(7016, 12549),
	BABY_PLATYPUS_LIGHTBROWN(7019, 12552),
	PLATYPUS_GREY(7017, 12550),
	BABY_PLATYPUS_GREY(70120, 12553),

	BABY_GECKO_YELLOW(7277, 12738),
	GECKO_YELLOW(7281, 12742),
	BABY_GECKO_GREEN(7278, 12739),
	GECKO_GREEN(7282, 12743),
	BABY_GECKO_RED(7279, 12740),
	GECKO_RED(7283, 12744),
	BABY_GECKO_BLUE(7280, 12741),
	GECKO_BLUE(7284, 12745),

	GIANT_CRAB_ORANGE(6948, 12501), // orange
	BABY_GIANT_CRAB_ORANGE(6947, 12500), // orange
	GIANT_CRAB_WHITE(7294, 12747), // white
	BABY_GIANT_CRAB_WHITE(7293, 12746), // white
	GIANT_CRAB_GREY(7296, 12749), // grey
	BABY_GIANT_CRAB_GREY(7295, 12748), // grey
	GIANT_CRAB_BROWN(7298, 12751), // brown
	BABY_GIANT_CRAB_BROWN(7297, 12750), // brown
	GIANT_CRAB_YELLOW(7300, 12753), // yellow
	BABY_GIANT_CRAB_YELLOW(7299, 12752), // yellow

	BABY_RACCOON_GREY(6913, 12486),
	RACCOON_GREY(6914, 12487),
	BABY_RACCOON_YELLOW(7271, 12734),
	RACCOON_YELLOW(7272, 12735),
	BABY_RACCOON_BROWN(7273, 12736),
	RACCOON_BROWN(7274, 12737),

	BABY_SQUIRREL_SILVER(6919, 12490),
	SQUIRREL_SILVER(6920, 12491),
	BABY_SQUIRREL_YELLOW(7301, 12754),
	SQUIRREL_YELLOW(7302, 12755),
	BABY_SQUIRREL_WHITE(7303, 12756),
	SQUIRREL_WHITE(7304, 12757),
	BABY_SQUIRREL_GREY(7305, 12758),
	SQUIRREL_GREY(7306, 12759),
	BABY_SQUIRREL_BROWN(7307, 12760),
	SQUIRREL_BROWN(7308, 12761),

	// new monsters
	SUPREME(4008, 4006),
	PRIME(4009, 4007),
	REX(4010, 4008),
	BABY_MOLE(3997, 3998), // missing
	PRINCE_BLACK_DRAGON(4000, 4011),
	KREE_ARRA(4005, 3999),
	ZILYANA(4007, 4005),
	KALPHITE_PRINCESS(3998, 4010), // RED
	KALPHITE_PRINCESS1(3999, 3997), // GREEN
	CHAOS_ELEMENTAL(4003, 4002),
	TZREK_JAD(4002, 21512),
	GENERAL_GRAARDOR(4001, 4001),
	KRIL_TSUTSAROTH(4006, 4004) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.equals("jump")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(Warmonger.JUMP_SLAM_ANIM);
					n.startGraphic(Warmonger.JUMP_SLAM_GFX);
				}
			}
		}
	},

	// juliu's pet
	JULIUS_PET(3996, 424),
	DRUNK_MAN(3222, 3711) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("drinking")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.forceChat("I am the liquor!");
				}
			}
		}
	},
	VETION_JR1(4177, 30370),
	VETION_JR2(4178, 30371),
	SCORPIAS_OFFSPRING(4179, 30374),
	VENENATIS(4180, 30372),
	CALLISTO_CUB(4181, 30373),
	KRAKEN(4182, 30375),
	OLMLET(4183, 30376),
	OLMLET2(4184, 30376),
	ZUK(4185, 30377),
	
	RABBIT(4447, 9975),
	PHOENIX_FLYING(8575, 14627),
	BLOODHOUND(7232, 30210);
	
	public static final HashMap<Integer, Pet> npcIdMap = new HashMap<>();
	public static final HashMap<Integer, Pet> itemIdMap = new HashMap<>();

    public static void load() {
    	/* empty */
    }

	static {
		for (Pet pet : Pet.values()) {
			itemIdMap.put(pet.item, pet);
			npcIdMap.put(pet.npcId, pet);
		}
	}

	public static Pet getPetForItem(int itemId) {
		return itemIdMap.get(itemId);
	}

	public static Pet getPetForNPC(int npcId) {
		return npcIdMap.get(npcId);
	}

	public static boolean isPetItem(int id) {
		return itemIdMap.containsKey(id);
	}

	public static boolean isPetNPC(int id) {
		return npcIdMap.containsKey(id);
	}

	public static void pickupPet(Player c, Pet pet) {
		if (pet == null || c.summoned == null) {
			return;
		}

		if (c.getItems().freeSlots() < 1) {
			c.sendMessage("Your pet was sent to bank because your inventory was full.");
		}

		if (c.getItems().addOrBankItem(pet.item, 1)) {
			c.summoned.nullNPC();
			c.startAnimation(827);
			c.summoned = null;
			c.getPacketSender().drawNpcOnInterface(4000, 17027);
		} else {
			c.sendMessage("You're unable to pickup your pet, because your inventory and bank is full.");
		}
	}

	public static void spawnPet(Player c, Pet pet, int itemId, int slot) {
		if (c.summoned != null) {
			c.sendMessage(
					"You can't have a pet with a familiar following you.");
			return;
		}
		if (c.infernoManager != null || c.raidsManager != null) {
			c.sendMessage("You can't do this here.");
			return;
		}
		if (!c.pokemonDelayTimer.complete()) {
			c.sendMessage("Please slow down.");
			return;
		}
		if (c.getItems().deleteItemInOneSlot(itemId, slot, 1)) {
			c.summoned = NPCHandler.summonNPC(c, pet.npcId,
					c.getCurrentLocation().getX(), c.getCurrentLocation().getY(),
					c.getHeightLevel(), 0, 0);
			c.startAnimation(827);
			c.getPacketSender().drawNpcOnInterface(pet.npcId, 17027);
		}
	}

	public static void petRoll(Player c, Pet pet) {
		if (Misc.random(2999) == 1) {
			dropPetFromDropTable(c, pet);
		}
	}
	
	public static void dropPetFromDropTable(Player c, Pet pet) {
		if (c.summoned == null) {
			c.summoned = NPCHandler.summonNPC(c, pet.npcId, c.getCurrentLocation().getX(), c.getCurrentLocation().getY(), c.getHeightLevel(), 1, 0);
			c.sendMessage("You have a funny feeling like you're being followed.");
		} else if (c.getItems().freeSlots() > 0) {
			c.getItems().addItem(pet.item, 1);
			c.sendMessage("You have a funny feeling like you have something in your inventory.");
		} else {
			c.getItems().addOrBankItem(pet.item, 1);
			c.sendMessage("You received a pet in your bank.");
		}
	}

	public int npcId;

	public int item;

	Pet(int npc, int item) {
		this.npcId = npc;
		this.item = item;
	}
	
	public int getItemId() {
		return item;
	}
	
	public void textCommand(NPC n, String msg) {
		
	}
}
