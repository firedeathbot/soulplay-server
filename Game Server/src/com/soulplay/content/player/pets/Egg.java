package com.soulplay.content.player.pets;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class Egg {

	public static enum EggData {

		MONSTER_EGG(15412, Pet.SUPREME, Pet.PRIME, Pet.REX, Pet.BABY_MOLE, Pet.PRINCE_BLACK_DRAGON, Pet.KREE_ARRA, Pet.ZILYANA, Pet.KALPHITE_PRINCESS, Pet.KALPHITE_PRINCESS, Pet.CHAOS_ELEMENTAL, Pet.TZREK_JAD, Pet.GENERAL_GRAARDOR, Pet.KRIL_TSUTSAROTH),
		RED_DRAGON_EGG(12477, Pet.BABY_DRAGON_RED),
		BLUE_DRAGON_EGG(12478, Pet.BABY_DRAGON_BLUE),
		GREEN_DRAGON_EGG(12479, Pet.BABY_DRAGON_GREEN),
		BLACK_DRAGON_EGG(12480, Pet.BABY_DRAGON_BLACK),
		RED_EGG(5076, Pet.ZAMORAK_HAWK),
		BLUE_EGG(5077, Pet.SARADOMIN_OWL),
		GREEN_EGG(5078, Pet.GUTHIX_RAPTOR),
		VULTURE_EGG(11965, Pet.VULTURE_BROWN, Pet.VULTURE_BROWN2, Pet.VULTURE_YELLOW, Pet.VULTURE_YELLOW2, Pet.VULTURE_WHITE, Pet.VULTURE_WHITE2),
		RAVEN_EGG(11964, Pet.RAVEN_BLACK, Pet.RAVEN_black2, Pet.RAVEN_GREY, Pet.RAVEN_GREY2, Pet.RAVEN_BROWN, Pet.RAVEN_BROWN2),
		CHAMELEON_EGG(12494, Pet.CHAMELEON_GREEN),
		PENGUIN(12483, Pet.PENGUIN),
		;

		private final int eggId;
		private final Pet[] pets;
		public static final Map<Integer, EggData> values = new HashMap<>();

		private EggData(int eggId, Pet... pets) {
			this.eggId = eggId;
			this.pets = pets;
		}

		public int getEggId() {
			return eggId;
		}

		public Pet[] getPets() {
			return pets;
		}

		public static void load() {
			for (EggData value : values()) {
				values.put(value.getEggId(), value);
			}
		}

	}

	public static void incubatorEnd(Client c) {
		if (c.getPA().freeSlots() <= 0) {
			c.getDH().sendNpcChat1("You need a free space in your inventory.", c.npcType, "Pet Shop Owner");
			return;
		}

		EggData eggData = EggData.values.get(c.getIncubatorEggId());
		if (eggData == null) {
			c.getDH().sendNpcChat1("There's no such egg. Report " + c.getIncubatorEggId(), c.npcType, "Pet Shop Owner");
			return;
		}

		c.getItems().addItem(eggData.getPets()[Misc.randomNoPlus(eggData.getPets().length)].item, 1);
		c.getDH().sendNpcChat1("Here is your pet!", c.npcType, "Pet Shop Owner");
		c.setEggTimer(0);
		c.setIncubatorEggId(0);
	}

	public static void incubatorStart(Client c, EggData eggData) {
		if (c.getIncubatorEggId() >= 1) {
			c.sendMessage("You already have an egg inside the incubator.");
			return;
		}

		if (!c.getItems().playerHasItem(eggData.eggId)) {
			return;
		}

		c.getItems().deleteItemInOneSlot(eggData.eggId, 1);
		c.setIncubatorEggId(eggData.eggId);
		c.sendMessage("You place your egg in the incubator.");
		c.startAnimation(897);
	}

}
