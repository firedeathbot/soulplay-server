package com.soulplay.content.player.pets.pokemon;

import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.map.travel.TempPathFinder;
import com.soulplay.util.Misc;

public class PokemonFollowing {
	
	public static void processFollow(NPC npc) {
		final Client owner = (Client) PlayerHandler.players[npc.summonedFor];
		if (owner != null) {
			if (npc.getZ() != owner.getZ() || Misc.distanceToPoint(npc.getX(), npc.getY(), owner.getX(), owner.getY()) > 8) {
					NPCHandler.trueTeleport(npc, owner.getX(), owner.getY(), owner.getZ());
					if (npc.killNpc > 0) {
						npc.resetAttack();
						npc.face(owner);
					owner.sendPokemonAtNpcIndex = 0;
				}
			} else
			if ((owner.npcIndex > 0 && NPCHandler.npcs[owner.npcIndex] != null && !NPCHandler.npcs[owner.npcIndex].isDead())) { // if attacking npc
				final NPC enemyNPC = NPCHandler.npcs[owner.npcIndex];
				int distanceToPoint = Misc.distanceToPoint(npc.getX(), npc.getY(), enemyNPC.getX(), enemyNPC.getY());
				if (distanceToPoint >= NPCHandler.distanceRequired(npc.getId())) {
					TempPathFinder.followSmartPath(npc, enemyNPC, owner.getCurrentLocation());
				}
				owner.sendPokemonAtNpcIndex = 0;
			} else if (owner.sendPokemonAtNpcIndex > 0 && NPCHandler.npcs[owner.sendPokemonAtNpcIndex] != null && !NPCHandler.npcs[owner.sendPokemonAtNpcIndex].isDead()) { // target npc with bolt pouch
				final NPC enemyNPC = NPCHandler.npcs[owner.sendPokemonAtNpcIndex];
				int distanceToPoint = Misc.distanceToPoint(npc.getX(), npc.getY(), enemyNPC.getX(), enemyNPC.getY());
				if (distanceToPoint >= NPCHandler.distanceRequired(npc.getId())) {
					TempPathFinder.followSmartPath(npc, enemyNPC, owner.getCurrentLocation());
				}
			} else { // else just follow owner
				if (npc.killNpc > 0 || npc.getKillerId() > 0) {
					if (npc.killNpc > 0) {
						NPC attacker = NPCHandler.npcs[npc.killNpc];
						
						// if npc is attacking me while i walk away then switch to player
						if (attacker != null && attacker.killNpc == npc.getId()) {
							attacker.resetAttack();
							attacker.setKillerId(owner.getId());
						}
							
					}
					npc.resetAttack();
					npc.face(owner);
					owner.sendPokemonAtNpcIndex = 0;
				}
				TempPathFinder.followSmartPath(npc, owner);
			}
		}
	}

}
