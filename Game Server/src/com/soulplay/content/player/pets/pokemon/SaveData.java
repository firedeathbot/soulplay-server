package com.soulplay.content.player.pets.pokemon;

public class SaveData {

	private final long ID;
	private final int playerMID;
	private final int npcId;
	private final int exp;
	private final int currentHp;
	private final int statPoints;
	private final int level;
	private final int hpLvl;
	private final int strLvl;
	private final int defLvl;
	private final int magicLvl;
	private final int rangeLvl;

	public SaveData(int playerMID, int npcId, int xp, int curHp, int statPoints, int level, int hpLvl, int strLvl, int defLvl, int magicLvl, int rangeLvl) {
		this.ID = generateID(playerMID, npcId);
		this.playerMID = playerMID;
		this.npcId = npcId;
		this.exp = xp;
		this.currentHp = curHp;
		this.statPoints = statPoints;
		this.level = level;
		this.hpLvl = hpLvl;
		this.strLvl = strLvl;
		this.defLvl = defLvl;
		this.magicLvl = magicLvl;
		this.rangeLvl = rangeLvl;
	}
	
	private long generateID(int p, int n) {
		return (p << 15) + n; // npc id cannot reach over 32767 so this might be safe for now
	}
	
	public long getID() {
		return ID;
	}

	public int getExp() {
		return exp;
	}

	public int getCurrentHp() {
		return currentHp;
	}

	public int getStatPoints() {
		return statPoints;
	}

	public int getLevel() {
		return level;
	}

	public int getHpLvl() {
		return hpLvl;
	}

	public int getStrLvl() {
		return strLvl;
	}

	public int getDefLvl() {
		return defLvl;
	}

	public int getMagicLvl() {
		return magicLvl;
	}

	public int getRangeLvl() {
		return rangeLvl;
	}

	public int getPlayerMID() {
		return playerMID;
	}

	public int getNpcId() {
		return npcId;
	}
}
