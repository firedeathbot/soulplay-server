package com.soulplay.content.player.pets.pokemon;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.attacks.NPCAttackDefinition;
import com.soulplay.game.model.npc.attacks.NPCAttackLoader;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public class PokemonButtons {

	private static final int FURY_SHARK = 20429;
	private static final int RAW_ROCKTAIL = 15270;
	private static final int REVIVE_BUTTON_ID = 195050;//CHANGE IF U ADD MORE POKEMONS

	public static void feedPet(Client c) {
		if (c.summonedPokemon == null) {
			c.sendMessage("You need to summon the pet to heal it.");
			return;
		}
		if (c.summoned.getSkills().getLifepoints() == c.summonedPokemon.stats.hp) {
			c.sendMessage("Pet has full hp.");
			return;
		}
		//TODO maybe add a delay for this lol
		
		if (!c.getItems().playerHasItem(FURY_SHARK) && !c.getItems().playerHasItem(RAW_ROCKTAIL)) {
			c.sendMessage("You need a raw rocktail or a fury shark to feed the pet with.");
			return;
		}

		if (c.getItems().deleteItemInOneSlot(RAW_ROCKTAIL, 1)) {
		    c.summoned.getSkills().heal(25);
		    c.sendMessage("You feed tiger shark to your pet and it heals 25 hp.");
		} else if (c.getItems().deleteItemInOneSlot(FURY_SHARK, 1)) {
			c.summoned.getSkills().heal(150);
		    c.sendMessage("You feed fury shark to your pet and it heals 150 hp.");
		}
		refreshData(c);
	}

	public static void deSpawn(Client c) {
		if (c.summoned != null) {
			c.summoned.nullNPC();
			c.summoned = null;
		}

		c.summonedPokemon = null;
	}

	public static void pickupPet(Client c, boolean deathPickup) {
		if (c.summoned == null) {
			c.sendMessage("You don't have npc summoned.");
			return;
		}

		if ((c.underAttackBy2 > 0 || c.summoned.killNpc > 0) && !deathPickup) {
			c.sendMessage("You can't pick up the pet while in combat.");
			return;
		}
		
		boolean found = false;
		for (int i = PokemonData.data.length - 1; i >= 0; i--) {
			if (c.summoned.npcType == PokemonData.data[i].npcId) {
				found = true;
				break;
			}
		}

		if (!found) {
			c.sendMessage("Please pick-up your pet before doing this.");
			return;
		}

		c.startAnimation(827);
		c.summonedPokemon.currentHp = c.summoned.getSkills().getLifepoints();
		deSpawn(c);
		refreshData(c);
		c.pokemonDelayTimer.startTimer(2);
	}
	
	public static void refreshData(Player c) {
		//unlocking
		for (int i = 0, length = c.pokemonUnlocked.length; i < length; i++) {
			c.getPacketSender().sendConfig(8013 + i, c.pokemonUnlocked[i]);
		}

		//Don't count the tutorial goblin
		c.getPacketSender().sendString("Pets " + countUnlocked(c) + "/" + (PokemonData.POKEMON_COUNT - 1), 49273);

		PokemonData pokemonData = c.selectedPokemon;
		if (pokemonData != null) {
			c.getPacketSender().sendConfig(8005, pokemonData.configValue);
		} else {
			c.getPacketSender().sendConfig(8005, 0);
		}

		if (c.summonedPokemon == null) {
			c.getPacketSender().sendConfig(8007, -1);
			c.getPacketSender().sendString("None", 40278);
			c.getPacketSender().sendString("Exp: 0/0", 40289);
			c.getPacketSender().sendString("HP: 0/0", 40293);
			c.getPacketSender().sendString("Level: 0", 40294);
			c.getPacketSender().sendString("Hitpoints: 0", 40295);
			c.getPacketSender().sendString("Strength: 0", 40296);
			c.getPacketSender().sendString("Magic: 0", 40297);
			c.getPacketSender().sendString("Range: 0", 40298);
			c.getPacketSender().sendString("Stat points: 0", 40299);
			c.getPacketSender().sendConfig(8009, 0);//current hp
			c.getPacketSender().sendConfig(8011, 0);//current xp
			return;
		}

		Pokemon pokemon = c.summonedPokemon;
		pokemon.currentHp = c.summoned.getSkills().getLifepoints();
		c.getPacketSender().sendConfig(8007, pokemon.data.npcId);
		c.getPacketSender().sendConfig(8006, c.summoned.getEntityDef().standAnim);
		c.getPacketSender().sendConfig(8008, pokemon.data.displayZoom);
		c.getPacketSender().sendConfig(8009, pokemon.currentHp);//current hp
		c.getPacketSender().sendConfig(8010, pokemon.stats.hp);//max hp
		c.getPacketSender().sendConfig(8011, pokemon.exp);//current xp
		int xp = 0;
		if (pokemon.level < PokemonData.POKEMON_XP_TABLE.length) {
			xp = PokemonData.POKEMON_XP_TABLE[pokemon.level];
		}

		c.getPacketSender().sendConfig(8012, xp);//xp to next level
		c.getPacketSender().sendString(c.summoned.def.name, 40278);
		c.getPacketSender().sendString("Exp: " + pokemon.exp + "/" + xp, 40289);
		c.getPacketSender().sendString("HP: " + pokemon.currentHp + "/" + pokemon.stats.hp, 40293);
		c.getPacketSender().sendString("Level: " + pokemon.level, 40294);
		c.getPacketSender().sendString("Hitpoints: " + pokemon.stats.hp, 40295);
		c.getPacketSender().sendString("Strength: " + pokemon.stats.strength, 40296);
		c.getPacketSender().sendString("Magic: " + pokemon.stats.magic, 40297);
		c.getPacketSender().sendString("Range: " + pokemon.stats.range, 40298);
		c.getPacketSender().sendString("Stat points: " + (pokemon.statPoints / 10) + "." + ((pokemon.statPoints % 10)), 40299);
	}

	public static int countUnlocked(Player c) {
		int count = 0;

		for (PokemonData data : PokemonData.data) {
			if (data == PokemonData.TUTORIAL_GOBLIN || !c.isPokemonUnlocked(data)) {
				continue;
			}

			count++;
		}

		return count;
	}

	private static void advanceFast(Client c, int index) {
		if (c.summonedPokemon == null) {
			c.sendMessage("You need to summon the pet to advance its stats.");
			return;
		}

		if (c.summonedPokemon.statPoints < 10) {
			c.sendMessage("You don't have enough stat points.");
			return;
		}

		if (c.summoned.powerUp != null) {
			c.sendMessage("You must wait for the boost to end.");
			return;
		}

		int advanceBy = 1;
		if (index == Pokemon.HP) {
			NPCAttackDefinition attack = NPCAttackLoader.getPokemon(c.summoned.npcType);
			if (attack != null) {
				int styles = attack.countAttacks();
				if (styles == 1) {
					advanceBy = 3;
				} else if (styles == 2) {
					advanceBy = 2;
				}
			}

			c.summoned.getSkills().setStaticLevelClean(Skills.HITPOINTS, c.summoned.getSkills().getStaticLevel(Skills.HITPOINTS) + advanceBy);
		}

		c.summonedPokemon.advanceStat(index, advanceBy);
		c.summonedPokemon.updatePokemonStats(c);
		refreshData(c);
	}
	
	private static void updateTaunt(Client c) {
		c.getPacketSender().sendConfig(8100, c.pokemonTaunt ? 1 : 0);
	}
	
	public static boolean handle(Client c, int buttonId) {
		switch(buttonId) {
			case 157075:
				c.getPacketSender().showInterface(49262);
				return true;
			case 192115:
				c.getPacketSender().showInterface(40262);
				updateTaunt(c);
				return true;
			case 157121://Taunt i think it was idk, u change
				c.pokemonTaunt = !c.pokemonTaunt;
				updateTaunt(c);
				return true;
			case 157108:
				advanceFast(c, Pokemon.HP);
				return true;
			case 157111:
				advanceFast(c, Pokemon.STR);
				return true;
			case 157114:
				advanceFast(c, Pokemon.MAGE);
				return true;
			case 157117:
				advanceFast(c, Pokemon.RANGE);
				return true;
			case 157090:
				feedPet(c);
				return true;
			case 192125:
				pickupPet(c, false);
				return true;
			case REVIVE_BUTTON_ID: {
				if (c.selectedPokemon == null) {
					c.sendMessage("Select a npc to revive.");
					return true;
				}

				int index = c.selectedPokemon.configValue - 1;
				
				if (!c.isPokemonUnlocked(index)) {
					c.sendMessage("This npc is locked and you can't summon it.");
					return true;
				}
				
				if (c.pokemon[index].currentHp > 0) {
					c.sendMessage("This pet is alive, no need to revive it.");
					return true;
				}

				if (!c.getItems().takeCoins(10_000_000)) {
					c.sendMessage("You need 10 million coins to revive your pet.");
					return true;
				}

				c.pokemon[index].currentHp = c.pokemon[index].stats.hp;
				c.sendMessage("You pay 10 million coins and revive your pet.");
				refreshData(c);
				return true;
			}
			case 192122:
				if (c.summoned != null) {
					c.sendMessage("You already have npc summoned.");
					return true;
				}

				if (c.infernoManager != null || c.raidsManager != null) {
					c.sendMessage("You can't do this here.");
					return true;
				}
				
				if (!c.pokemonDelayTimer.complete()) {
					c.sendMessage("Please slow down.");
					return true;
				}

				spawnPokemon(c, c.selectedPokemon, true);
				return true;
			default:
				PokemonData pokemonData = PokemonData.buttonToEnum.get(buttonId);
				if (pokemonData != null) {
					c.selectedPokemon = pokemonData;
					c.getPacketSender().sendConfig(8005, pokemonData.configValue);
					return true;
				}
		}
		
		return false;
	}

	public static void spawnPokemon(Client c, PokemonData toSpawn, boolean showInterface) {
		if (toSpawn == null) {
			c.sendMessage("Select a npc to spawn.");
			return;
		}

		int index = toSpawn.configValue - 1;
		
		if (!c.isPokemonUnlocked(index)) {
			c.sendMessage("This npc is locked and you can't summon it.");
			return;
		}
		
		if (c.pokemon[index].currentHp <= 0) {
			c.sendMessage("This pet is dead.");
			return;
		}

		c.summoned = NPCHandler.summonPokemon(c, toSpawn.npcId,
				c.getCurrentLocation().getX(), c.getCurrentLocation().getY(),
				c.getHeightLevel(), c.pokemon[index].currentHp, c.pokemon[index].stats.hp);
		c.summoned.applyBoss(true);
		c.summoned.summoner = c;
		c.summonedPokemon = c.pokemon[index];
		c.summonedPokemon.updateScale(c);
		c.summonedPokemon.updateDefence(c);
		refreshData(c);
		updateTaunt(c);
		if (showInterface) {
			c.getPacketSender().showInterface(40262);
		}
		c.summoned.face(c);
	}
}
