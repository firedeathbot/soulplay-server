package com.soulplay.content.player.pets.pokemon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;

public class PokemonSave {
	
	private static final String SAVE_Q = "INSERT INTO `pokemon` (ID, player_id, npc_id, exp, current_hp, stat_points, level, hp_lvl, str_lvl, def_lvl, magic_lvl, range_lvl) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE "
			+ "exp=VALUES(exp), current_hp=VALUES(current_hp), stat_points=VALUES(stat_points), level=VALUES(level), hp_lvl=VALUES(hp_lvl), str_lvl=VALUES(str_lvl), def_lvl=VALUES(def_lvl), magic_lvl=VALUES(magic_lvl), range_lvl=VALUES(range_lvl)";
	
	private static void savePet(SaveData data, PreparedStatement stmt) throws SQLException {
		
		stmt.setInt(1, (int)(data.getID()));
		stmt.setInt(2, data.getPlayerMID());
		stmt.setInt(3, data.getNpcId());
		stmt.setInt(4, data.getExp());
		stmt.setInt(5, data.getCurrentHp());
		stmt.setInt(6, data.getStatPoints());
		stmt.setInt(7, data.getLevel());
		stmt.setInt(8, data.getHpLvl());
		stmt.setInt(9, data.getStrLvl());
		stmt.setInt(10, data.getDefLvl());
		stmt.setInt(11, data.getMagicLvl());
		stmt.setInt(12, data.getRangeLvl());
		
		stmt.addBatch();
	}
	
	public static boolean saveAllPokemon(Player c) {

		try (Connection connection = Server.getConnection();
				PreparedStatement stmt = connection.prepareStatement(SAVE_Q);) {

			for (int i = 0; i < c.pokemon.length; i++) {
				Pokemon p = c.pokemon[i];
				if (p == null || (p.level == 1 && p.exp <= 0)) {
					continue;
				}

				final SaveData data = new SaveData(c.mySQLIndex, p.data.npcId, p.exp, p.currentHp, p.statPoints, p.level, p.stats.hp, p.stats.strength, 0, p.stats.magic, p.stats.range);
				savePet(data, stmt);
			}
			
			stmt.executeBatch();

			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;

	}
	
	public static boolean loadAllPokemon(Player c, Connection con) {
		boolean loaded = false;
		c.fillPokemonData();
		try (PreparedStatement ps = con.prepareStatement("SELECT * from `pokemon` WHERE player_id = "+ c.mySQLIndex);
				ResultSet rs = ps.executeQuery();) {
			
			while (rs.next()) {
				int npcId = rs.getInt("npc_id");
				PokemonData data = PokemonData.npcIdToEnum.get(npcId);
				if (data == null) continue; // just in case
				
				c.pokemon[data.ordinal()].exp = rs.getInt("exp");
				c.pokemon[data.ordinal()].currentHp = rs.getInt("current_hp");
				c.pokemon[data.ordinal()].statPoints = rs.getInt("stat_points");
				c.pokemon[data.ordinal()].level = rs.getInt("level");
				c.pokemon[data.ordinal()].stats.hp = rs.getInt("hp_lvl");
				c.pokemon[data.ordinal()].stats.strength = rs.getInt("str_lvl");
				rs.getInt("def_lvl");
				c.pokemon[data.ordinal()].stats.magic = rs.getInt("magic_lvl");
				c.pokemon[data.ordinal()].stats.range = rs.getInt("range_lvl");
			}
			loaded = true;
		} catch (SQLException ex) {
			ex.printStackTrace();
			loaded = false;
		}
		return loaded;
	}

}
