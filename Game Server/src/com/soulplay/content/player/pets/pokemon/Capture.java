package com.soulplay.content.player.pets.pokemon;

import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class Capture {

	public static final int strongTameRope = 19668;
	public static final int tameRope = 19667;
	public static final int questTameRope = 19665;

	public static void capturePet(Client c, NPC n, PokemonData data) {

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int timer = 6;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.disconnected || c == null || n == null || n.isDead()) {
					container.stop();
					return;
				}

				if (timer == 6) {
					c.face(n);
					n.stopAttack = true;
					n.setAttackable(false);
				}

				if (timer == 5) {
					c.startAnimation(2910);
					c.getMovement().resetWalkingQueue();
				}

				if (timer == 2) {
					c.startAnimation(-1);
					c.getMovement().resetWalkingQueue();
				}

				timer--;
				if (timer == 0) {
					container.stop();

					int catchRate = Misc.interpolate(1, 100, 1, 1001, n.getEntityDef().getCombatLevel());
					if (c.getItems().playerHasItem(strongTameRope)) {
						catchRate = Misc.interpolate(1, 50, 1, 1001, n.getEntityDef().getCombatLevel());
					}
					//System.out.println(catchRate);

					if (Misc.random(catchRate) == 1 || n.npcType == TamingQuestHandler.TAMING_GOBLIN) {
						if (n.npcType == TamingQuestHandler.TAMING_GOBLIN) {
							QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
							c.getItems().deleteItemInOneSlot(questTameRope, 1);
						}
						c.unlockPokemon(data);
						c.getAchievement().capture5Pets();
						PlayerHandler.messageAllPlayers("<img=9> " + c.getNameSmartUp() + " has Captured the monster: <col=ff0000>" + NPCHandler.getNpcName(n.npcType));
						n.die();
					} else {
						if (Misc.random(5) == 1) {
							c.sendMessage("<col=ff0000>You failed to tame this creature and the rope torn apart.");
							if (c.getItems().playerHasItem(strongTameRope)) {
							    c.getItems().deleteItemInOneSlot(strongTameRope, 1);
							} else {
							    c.getItems().deleteItemInOneSlot(tameRope, 1);
							}
						} else
							c.sendMessage("<col=ff0000>You failed to tame this creature.");
					}
				}
			}

			@Override
			public void stop() {
				n.stopAttack = false;
				n.setAttackable(true);
				n.setCanThrowPokeball(false);
				c.performingAction = false;
			}
		}, 1);
	}
}
