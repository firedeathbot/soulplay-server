package com.soulplay.content.player.pets.pokemon;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl.Sagittare;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl.Warmonger;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

//CHANGE THE REVIVE ID IF U ADD MORE POKEMONS
public enum PokemonData {
	
	CHICKEN(192131, 1, 1017, 1200, new PokemonStats(10, 1, 1, 1), 60, 128, 225, 1), //128 default size
	GIANT_RAT(192134, 2, 86, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 1),
	COW(192137, 3, 81, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 1),
	GOBLIN(192140, 4, 4261, 1200, new PokemonStats(10, 1, 1, 1), 60, 128, 225, 1),
	GIANT_SPIDER(192143, 5, 59, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 1),
	CRAWLING_HAND(192146, 6, 1648, 1200, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 2),
	MINOTAUR(192149, 7, 4404, 1200, new PokemonStats(10, 1, 1, 1), 60, 128, 225, 2),
	ROCK_CRAB(192152, 8, 1265, 1200, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 3), 
	GIANT_MOSQUITO(192155, 9, 4347, 1200, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 2),
	SCORPION(192158, 10, 107, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 2),
	UNICORN(192161, 11, 89, 1400, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 2),
	GHOST(192164, 12, 103, 1200, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 3),
	GRIZZLY_BEAR(192167, 13, 105, 1200, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 3),
	YAK(192170, 14, 5529, 1200, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 3),
	BANSHEE(192173, 15, 1612, 1500, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 3), 
	CAVE_CRAWLER(192176, 16, 1600, 1200, new PokemonStats(10, 1, 1, 1), 45, 128, 225, 3),
	CAVE_SLIME(192179, 17, 1831, 1200, new PokemonStats(10, 1, 1, 1), 45, 128, 225, 3),
	ZOMBIE(192182, 18, 75, 1400, new PokemonStats(10, 1, 1, 1), 50, 110, 225, 3),
	WHITE_WOLF(192185, 19, 96, 1200, new PokemonStats(10, 1, 1, 1), 45, 128, 225, 3),
	SKELETON(192188, 20, 92, 1400, new PokemonStats(10, 1, 1, 1), 50, 110, 225, 3),
	GIANT_BAT(192191, 21, 78, 1200, new PokemonStats(10, 1, 1, 1), 45, 120, 225, 3),
	
	HILL_GIANT(192194, 22, 117, 1800, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 4), 
	KALPHITE_WORKER(192197, 23, 1153, 1200, new PokemonStats(10, 1, 1, 1), 35, 100, 225, 4),
	ROCKSLUG(192200, 24, 1631, 800, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 4),
	RED_SPIDER(192203, 25, 63, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 4),
	FIRE_ELEMENTAL(192206, 26, 1019, 1200, new PokemonStats(10, 1, 1, 1), 45, 120, 225, 4),
	COCKATRICE(192209, 27, 1620, 1400, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 4),
	JUNGLE_SPIDER(192212, 28, 62, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 4),
	MOSS_GIANT(192215, 29, 112, 1800, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 4), 
	HOBGOBLIN(192218, 30, 2688, 1200, new PokemonStats(10, 1, 1, 1), 60, 128, 225, 4),
	PYREFIEND(192221, 31, 1633, 1400, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 4),
	BABY_BLUE_DRAG(192224, 32, 52, 1300, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 5),
	BABY_RED_DRAG(192227, 33, 1589, 1300, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 5),
	BLOODWORM(192230, 34, 2031, 1000, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 5),
	ICE_GIANT(192233, 35, 111, 1800, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 5),
	ICE_WARRIOR(192236, 36, 125, 1500, new PokemonStats(10, 1, 1, 1), 45, 105, 225, 5), 
	GIANT_ANT(192239, 37, 6380, 1200, new PokemonStats(10, 1, 1, 1), 35, 100, 225, 5),
	BASILISK(192242, 38, 1616, 1200, new PokemonStats(10, 1, 1, 1), 35, 128, 225, 5),
	POISON_SPIDER(192245, 39, 134, 1200, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 5),
	INFERNAL_MAGE(192248, 40, 1643, 1500, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 5),
	GIANT_WASP(192251, 41, 6381, 1200, new PokemonStats(10, 1, 1, 1), 32, 40, 128, 5),
	JUNGLE_HORROR(192254, 42, 4350, 1400, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 5),
	
	BLOODVELD(193001, 43, 1618, 1200, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 6), 
	JELLY(193004, 44, 1637, 1100, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 6),
	GREEN_DRAGON(193007, 45, 941, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 6)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("sleep")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(12254));
				}
			}
		}
	},
	AVIANSIE(193010, 46, 6233, 2100, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 6),
	BABY_BLACK_DRAG(193013, 47, 3376, 1300, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 6),
	KALPHITE_SOLDIER(193016, 48, 1154, 1200, new PokemonStats(10, 1, 1, 1), 35, 100, 225, 6),
	FIRE_GIANT(193019, 49, 110, 1300, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 6),
	DIRE_WOLF(193022, 50, 1198, 1000, new PokemonStats(10, 1, 1, 1), 40, 105, 225, 6), 
	TUROTH(193025, 51, 1623, 1200, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 6),
	GREATER_DEMON(193028, 52, 83, 2100, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 6),
	DAGANNOTH(193031, 53, 1343, 1700, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 6),
	WEREWOLF(193034, 54, 6212, 1200, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 6),
	ABERRANT_SPECTRE(193037, 55, 1604, 1500, new PokemonStats(10, 1, 1, 1), 45, 95, 225, 7),
	CAVE_BUG(193040, 56, 5750, 1200, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 7),
	WALLASALKI(193043, 57, 2884, 1200, new PokemonStats(10, 1, 1, 1), 45, 110, 225, 7), 
	MUMMY(193046, 58, 1961, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 7),
	KURASK(193049, 59, 1608, 1400, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 7),
	ORK(193052, 60, 6274, 1200, new PokemonStats(10, 1, 1, 1), 45, 115, 225, 7),
	JUNGLE_WORM(193055, 61, 9467, 1800, new PokemonStats(10, 1, 1, 1), 35, 80, 225, 7),
	GARGOYLE(193058, 62, 1610, 1800, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 7),
	BLUE_DRAGON(193061, 63, 55, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 8)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("sleep")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(12254));
				}
			}
		}
	},
	AQUANITE(193064, 64, 9172, 1300, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 8), 
	NECHRYAEL(193067, 65, 1613, 1500, new PokemonStats(10, 1, 1, 1), 45, 100, 225, 8),
	WATERFIEND(193070, 66, 5361, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 8),
	REV_IMP(193073, 67, 13465, 1100, new PokemonStats(10, 1, 1, 1), 60, 128, 225, 3),
	REV_GOBLIN(193076, 68, 13469, 1200, new PokemonStats(10, 1, 1, 1), 55, 115, 225, 3),
	REV_VAMPYRE(193079, 69, 13473, 1200, new PokemonStats(10, 1, 1, 1), 55, 115, 225, 4),
	REV_PYREFIEND(193082, 70, 13471, 1200, new PokemonStats(10, 1, 1, 1), 55, 120, 225, 4),
	REV_HOBGOBLIN(193085, 71, 13472, 1100, new PokemonStats(10, 1, 1, 1), 55, 110, 225, 5), 
	REV_WEREWOLF(193088, 72, 13474, 1300, new PokemonStats(10, 1, 1, 1), 55, 110, 225, 5),
	REV_CYCLOP(193091, 73, 13475, 1900, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 6),
	REV_DEMON(193094, 74, 13477, 1900, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 6),
	REV_ORK(193097, 75, 13478, 1400, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 7),
	REV_KNIGHT(193100, 76, 13480, 1200, new PokemonStats(10, 1, 1, 1), 55, 115, 225, 8),
	REV_BEAST(193103, 77, 13479, 1200, new PokemonStats(10, 1, 1, 1), 40, 64, 225, 8),
	REV_HELLHOUND(193106, 78, 13476, 1200, new PokemonStats(10, 1, 1, 1), 55, 110, 225, 8), 
	REV_DRAGON(193109, 79, 13481, 1100, new PokemonStats(10, 1, 1, 1), 45, 120, 225, 10),
	AHRIM(193112, 80, 2025, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 9),
	KARIL(193115, 81, 2028, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 9),
	GUTHAN(193118, 82, 2027, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 9),
	TORAG(193121, 83, 2029, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 9),
	DHAROK(193124, 84, 2026, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 9),

	VERAC(193127, 85, 2030, 1200, new PokemonStats(10, 1, 1, 1), 50, 115, 225, 9), 
	ABYSSA_DEMON(193130, 86, 1615, 2000, new PokemonStats(10, 1, 1, 1), 50, 120, 225, 9),
	ROCK_LOBSTER(193133, 87, 2889, 1200, new PokemonStats(10, 1, 1, 1), 45, 110, 225, 9),
	DESERT_WYRM(193136, 88, 9465, 1800, new PokemonStats(10, 1, 1, 1), 35, 80, 225, 9),
	BRONZE_DRAGON(193139, 89, 1590, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 9)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("scream")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(14246));
				}
			}
		}
	},
	TZHAAR_XIL(193142, 90, 2605, 1200, new PokemonStats(10, 1, 1, 1), 50, 128, 225, 9),
	TOK_XIL(193145, 91, 2631, 2300, new PokemonStats(10, 1, 1, 1), 30, 70, 225, 9),
	YT_MEJKOT(193148, 92, 2741, 3000, new PokemonStats(10, 1, 1, 1), 30, 60, 225, 12), 
	KET_ZEK(193151, 93, 2743, 1800, new PokemonStats(10, 1, 1, 1), 30, 75, 225, 14),
	SKELETAL_WYVERN(193154, 94, 3068, 1300, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 10),
	KALPHITE_GUARD(193157, 95, 1155, 1900, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 9),
	STRONGSTACK(193160, 96, 6261, 1100, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 10),
	GRIMSPIKE(193163, 97, 6265, 1100, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 10),
	STEELWILL(193166, 98, 6263, 1100, new PokemonStats(10, 1, 1, 1), 40, 128, 225, 10),
	GROWLER(193169, 99, 6250, 1000, new PokemonStats(10, 1, 1, 1), 45, 128, 225, 10), 
	STARLIGHT(193172, 100, 6248, 1300, new PokemonStats(10, 1, 1, 1), 45, 128, 225, 10),
	BREE(193175, 101, 6252, 1600, new PokemonStats(10, 1, 1, 1), 40, 105, 225, 10),
	KREEYATH(193178, 102, 6208, 2100, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 10),
	KARLAK(193181, 103, 6204, 2100, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 10),
	GRITCH(193184, 104, 6206, 2100, new PokemonStats(10, 1, 1, 1), 45, 100, 225, 10),
	GEERIN(193187, 105, 6225, 2000, new PokemonStats(10, 1, 1, 1), 45, 115, 225, 10),
	
	SKREE(193190, 106, 6223, 2000, new PokemonStats(10, 1, 1, 1), 45, 115, 225, 10), 
	KILISA(193193, 107, 6227, 2000, new PokemonStats(10, 1, 1, 1), 45, 115, 225, 10),
	JADINK_GUARD(193196, 108, 13821, 1200, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 10),
	GORAK(193199, 109, 6218, 1500, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 8),
	RED_DRAGON(193202, 110, 53, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 10)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("sleep")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(12254));
				}
			}
		}
	},
	FROST_DRAGON(193205, 111, 51, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 12)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("scream")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(13152));
				}
			}
		}
	},
	BARRELCHEST(193208, 112, 5666, 1500, new PokemonStats(10, 1, 1, 1), 40, 95, 225, 12)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("slam")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(5895));
				}
			}
		}
	},
	BLACK_DEMON(193211, 113, 84, 2100, new PokemonStats(10, 1, 1, 1), 40, 80, 225, 11), 
	DARK_BEAST(193214, 114, 2783, 1300, new PokemonStats(10, 1, 1, 1), 40, 100, 225, 11),
	IRON_DRAGON(193217, 115, 1591, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 11)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("scream")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(14246));
				}
			}
		}
	},
	JADINKO_MALE(193220, 116, 13822, 1200, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 11),
	ICE_STRYKEWYRM(193223, 117, 9463, 1900, new PokemonStats(10, 1, 1, 1), 35, 80, 225, 11),
	BRUTAL_DRAGON(193226, 118, 5362, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 11)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("sleep")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(12254));
				}
			}
		}
	},
	GIANT_MOLE(193229, 119, 3340, 1100, new PokemonStats(10, 1, 1, 1), 35, 85, 225, 11),
	STEEL_DRAGON(193232, 120, 1592, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 11)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("scream")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(14246));
				}
			}
		}
	},
	LAVA_DRAGON(193235, 121, 2011, 1500, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 11),
	DEMONIC_GORILLA(193238, 122, 7151, 2000, new PokemonStats(10, 1, 1, 1), 40, 74, 225, 13),
	BANDOS_AVATAR(193241, 123, 4540, 1600, new PokemonStats(10, 1, 1, 1), 40, 85, 225, 13),
	KBD(193244, 124, 50, 1500, new PokemonStats(10, 1, 1, 1), 35, 75, 225, 13),
	CHAOS_ELE(193247, 125, 3200, 1700, new PokemonStats(10, 1, 1, 1), 40, 90, 225, 13),
	DAG_SUPREME(193250, 126, 2881, 1250, new PokemonStats(10, 1, 1, 1), 35, 90, 225, 13),
	DAG_PRIME(193253, 127, 2882, 1250, new PokemonStats(10, 1, 1, 1), 35, 90, 225, 13),
	
	DAG_REX(194000, 128, 2883, 1250, new PokemonStats(10, 1, 1, 1), 35, 90, 225, 13), 
	MIHRIL_DRAGON(194003, 129, 5363, 1600, new PokemonStats(10, 1, 1, 1), 35, 70, 225, 13),
	SKELETAL_HORROR(194006, 130, 9176, 2050, new PokemonStats(10, 1, 1, 1), 30, 80, 225, 14),
	TORM_DEMON(194009, 131, 8349, 2050, new PokemonStats(10, 1, 1, 1), 35, 90, 225, 14),
	SCORPIA(194012, 132, 4172, 1100, new PokemonStats(10, 1, 1, 1), 30, 75, 225, 14),
	VENENATIS(194015, 133, 4173, 1200, new PokemonStats(10, 1, 1, 1), 30, 70, 225, 14),
	CALLISTO(194018, 134, 4174, 1500, new PokemonStats(10, 1, 1, 1), 25, 60, 225, 14),
	GLACOR(194021, 135, 1382, 1200, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 14), 
	AVATAR_OF_DES(194024, 136, 8596, 3500, new PokemonStats(10, 1, 1, 1), 25, 60, 225, 17),
	AVATAR_OF_CREA(194027, 137, 8597, 3500, new PokemonStats(10, 1, 1, 1), 25, 60, 225, 17),
	KREE_ARRA(194030, 138, 6222, 3500, new PokemonStats(10, 1, 1, 1), 25, 65, 225, 16),
	ZILYANA(194033, 139, 6247, 1400, new PokemonStats(10, 1, 1, 1), 40, 110, 225, 16),
	GRAARDOR(194036, 140, 6260, 2400, new PokemonStats(10, 1, 1, 1), 30, 75, 225, 16)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("slam")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(7063));
				}
			}
		}
	},
	KRIL_TSUTSAROTH(194039, 141, 6203, 2600, new PokemonStats(10, 1, 1, 1), 25, 65, 225, 16)  {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("jump")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(Warmonger.JUMP_SLAM_ANIM);
					n.startGraphic(Warmonger.JUMP_SLAM_GFX);
				}
			}
		}
	},
	TZTOK_JAD(194042, 142, 2745, 3050, new PokemonStats(10, 1, 1, 1), 25, 55, 225, 17) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("slam")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(9276));
					n.startGraphic(Graphic.create(1834));
				}
			}
		}
	},
	ZULRAH(194045, 143, 2045, 750, new PokemonStats(10, 1, 1, 1), 50, 220, 225, 16),
	CORP_BEAST(194048, 144, 8133, 2950, new PokemonStats(10, 1, 1, 1), 21, 60, 225, 17),
	NEX(194051, 145, 13447, 1500, new PokemonStats(10, 1, 1, 1), 32, 128, 225, 18) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("hide")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(6948));
				}
			}
		}
	},
	
	GLUTTONOUS_BEHEMOTH(194054, 146, 9948, 1500, new PokemonStats(10, 1, 1, 1), 28, 80, 225, 12),
	ASTEA_FROSTWEB(194057, 147, 9965, 1000, new PokemonStats(10, 1, 1, 1), 42, 120, 225, 12),
	ICY_BONES(194060, 148, 10040, 1300, new PokemonStats(10, 1, 1, 1), 32, 128, 225, 14),
	LUMINESCENT(194063, 149, 9912, 1000, new PokemonStats(10, 1, 1, 1), 32, 128, 225, 14) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("freeze")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(13338));
				}
			}
		}
	},
	LAKHRAHNAZ(194066, 150, 9929, 1500, new PokemonStats(10, 1, 1, 1), 32, 120, 225, 14),
	TO_KASH(194069, 151, 10024, 2500, new PokemonStats(10, 1, 1, 1), 32, 105, 225, 14),
	DIVING_SKINWEAVER(194072, 152, 10058, 1000, new PokemonStats(10, 1, 1, 1), 47, 128, 225, 14) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("stealth")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(13677));
				}
			}
		}
	},
	GEOMANCER(194075, 153, 10059, 1000, new PokemonStats(10, 1, 1, 1), 45, 128, 225, 15),
	BULWARK_BEAST(194078, 154, 10073, 1500, new PokemonStats(10, 1, 1, 1), 28, 75, 225, 15),
	UNHOLY_CURSEBEARER(194081, 155, 10111, 1100, new PokemonStats(10, 1, 1, 1), 42, 120, 225, 15),
	RAMMERNAUT(194084, 156, 9767, 1200, new PokemonStats(10, 1, 1, 1), 42, 128, 225, 15),
	HAR_LAKK(194087, 157, 9898, 2500, new PokemonStats(10, 1, 1, 1), 32, 105, 225, 15),
	LEXICUS(194090, 158, 9842, 1000, new PokemonStats(10, 1, 1, 1), 47, 120, 225, 15),
	SAGITTARE(194093, 159, 9753, 1000, new PokemonStats(10, 1, 1, 1), 47, 120, 225, 15) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("arrow rain")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(Sagittare.ARROW_RAIN_ANIM);
					n.startGraphic(Sagittare.ARROW_RAIN_GFX);
				}
			}
		}
	},
	BAL_LAK(194096, 160, 10128, 2500, new PokemonStats(10, 1, 1, 1), 32, 105, 225, 15),
	RUNEBOUND(194099, 161, 11752, 1700, new PokemonStats(10, 1, 1, 1), 28, 80, 225, 15),
	GRAVECREEPER(194102, 162, 11708, 1000, new PokemonStats(10, 1, 1, 1), 47, 120, 225, 15),
	NECROLORD(194105, 163, 11737, 1000, new PokemonStats(10, 1, 1, 1), 45, 120, 225, 16),
	HAASGHENAHK(194108, 164, 11895, 2000, new PokemonStats(10, 1, 1, 1), 28, 80, 225, 16),
	YK_LAGOR(194111, 165, 11872, 2500, new PokemonStats(10, 1, 1, 1), 32, 105, 225, 16),
	BLINK(194114, 166, 12865, 1000, new PokemonStats(10, 1, 1, 1), 47, 120, 225, 16),
	WARPED_GULEGA(194117, 167, 12737, 1800, new PokemonStats(10, 1, 1, 1), 28, 90, 225, 17),
	DREADNAUT(194120, 168, 12848, 1400, new PokemonStats(10, 1, 1, 1), 36, 115, 225, 17),
	HOPE_DEVOURER(194123, 169, 12886, 1500, new PokemonStats(10, 1, 1, 1), 28, 80, 225, 17),
	SHUKARHAZH(194126, 170, 12478, 3000, new PokemonStats(10, 1, 1, 1), 22, 65, 225, 17) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("spin")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(new Animation(14893));
				}
			}
		}
	},
	KAL_GER(194129, 171, 12841, 2500, new PokemonStats(10, 1, 1, 1), 32, 105, 225, 18) {
		@Override
		public void textCommand(NPC n, String msg) {
			if (msg.contains("jump")) {
				if (n.getStopWatch().checkThenStart(7)) {
					n.startAnimation(Warmonger.JUMP_SLAM_ANIM);
					n.startGraphic(Warmonger.JUMP_SLAM_GFX);
				}
			}
		}
	},
	VORKATH(194132, 172, 8970, 2500, new PokemonStats(10, 1, 1, 1), 25, 50, 225, 16),
	
	TERROR_DOG(194135, 173, 11365, 1000, new PokemonStats(10, 1, 1, 1), 25, 125, 225, 16),
	KRUK(194138, 174, 1441, 600, new PokemonStats(10, 1, 1, 1), 45, 140, 225, 16),
	PHOENIX(194141, 175, 8548, 1100, new PokemonStats(10, 1, 1, 1), 30, 150, 225, 17),
	ANGRY_GOBLIN(194144, 176, 3648, 800, new PokemonStats(10, 1, 1, 1), 32, 150, 225, 16),
	HAROLD(194147, 177, 14181, 950, new PokemonStats(10, 1, 1, 1), 25, 125, 225, 15),
	AYUNI(194150, 178, 13220, 1100, new PokemonStats(10, 1, 1, 1), 25, 125, 225, 16),
	CHAOS_DWOGRE(194153, 179, 8771, 1150, new PokemonStats(10, 1, 1, 1), 32, 125, 225, 17),
	KEBBIT(194156, 180, 5137, 600, new PokemonStats(10, 1, 1, 1), 35, 120, 225, 15),
	A_DOUBT(194159, 181, 5906, 1000, new PokemonStats(10, 1, 1, 1), 32, 115, 225, 15),
	OURG_STATUE(194162, 182, 7086, 2350, new PokemonStats(10, 1, 1, 1), 20, 80, 225, 17),
	ELEMENTAL_CREATURE(194165, 183, 9432, 800, new PokemonStats(10, 1, 1, 1), 32, 120, 225, 17),
	ENHANCED_ICE_TITAN(194168, 184, 14257, 1450, new PokemonStats(10, 1, 1, 1), 25, 105, 225, 17),
	THE_SHAIKAHAN(194171, 185, 1172, 850, new PokemonStats(10, 1, 1, 1), 30, 125, 225, 18),
	SHARAHTEERK(194174, 186, 8165, 1300, new PokemonStats(10, 1, 1, 1), 30, 110, 225, 15),
	GIANT_LOBSTER(194177, 187, 7828, 800, new PokemonStats(10, 1, 1, 1), 30, 120, 225, 15),
	GNOEALS(194180, 188, 7392, 1450, new PokemonStats(10, 1, 1, 1), 25, 100, 225, 15),
	TOKTZ_KET_DILL(194183, 189, 7771, 1400, new PokemonStats(10, 1, 1, 1), 25, 95, 225, 16),
	TREUS_DAYTH(194186, 190, 1540, 1000, new PokemonStats(10, 1, 1, 1), 30, 100, 225, 15),
	THE_UNTOUCHABLE(194189, 191, 5904, 1000, new PokemonStats(10, 1, 1, 1), 30, 100, 225, 15),
	NAIL_BEAST(194192, 192, 1521, 1000, new PokemonStats(10, 1, 1, 1), 30, 135, 225, 18),
	MUTANT_TARN(194195, 193, 5421, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 18),
	ICE_DEMON(194198, 194, 9052, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 18),
	BLACK_DRAGON(194201, 195, 54, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	
	BRUTAL_BLUE_DRAGON(194204, 196, 107273, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	BRUTAL_RED_DRAGON(194207, 197, 107274, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	BRUTAL_BLACK_DRAGON(194210, 198, 107275, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	ADAMANT_DRAGON(194213, 199, 108030, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	RUNE_DRAGON(194216, 200, 108027, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	CAVE_HORROR(194219, 201, 4353, 1000, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 14),
	ALBINO_BAT(194222, 202, 4345, 1000, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 13),
	LIVING_ROCK_STRIKER(194225, 203, 8833, 1200, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	LIVING_ROCK_PROTECTOR(194228, 204, 8832, 1200, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	TALONED_WYVERN(194231, 205, 107793, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	LONG_TAILED_WYVERN(194234, 206, 107792, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	SPITTING_WYVERN(194237, 207, 107794, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	ANCIENT_WYVERN(194240, 208, 107795, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	LIZARD_SHAMAN(194243, 209, 107745, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 17),
	SAND_CRAB(194246, 210, 107206, 1000, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 13),
	KING_SAND_CRAB(194249, 211, 107266, 1400, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 14),
	SULPUR_LIZARD(194252, 212, 108614, 1000, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 13),
	WYRM(194255, 213, 108611, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	DRAKE(195002, 214, 108612, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	HYDRA(195005, 215, 108609, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 16),
	ALCHEMICAL_HYDRA(195008, 216, 108615, 1600, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 18),
	BASILISK_KNIGHT(195011, 217, 109293, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 17),
	MUTATED_BLOODVELD(195014, 218, 7642, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	GREATER_NECHRYAEL(195017, 219, 107278, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	SHADE(195020, 220, 105633, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	DEVIANT_SPECTRE(195023, 221, 107279, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	TWISTED_BANSHEE(195026, 222, 107272, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	ANKOU(195029, 223, 4381, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 14),
	WARPED_JELLY(195032, 224, 107277, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	DUST_DEVIL(195035, 225, 1624, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 15),
	OLMLET(195038, 226, 107519, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 18),
	LIL_ZIK(195041, 227, 108336, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 18),
	LITTLE_NIGHTMARE(195044, 228, 109398, 1300, new PokemonStats(10, 1, 1, 1), 25, 120, 225, 18),
	
	
	
	
	
	//tutorial
	TUTORIAL_GOBLIN(-1, 194, TamingQuestHandler.TAMING_GOBLIN, -1, new PokemonStats(1000, 10, 1, 1), 32, 68, 225, 20);
	
	public final int buttonId;
	public final int configValue;
	public final int npcId;
	public final int displayZoom;
	public final PokemonStats baseStats;
	public final int minScale;
	public final int maxScale;
	public final int levelCapScale;
	public final int skillPointsPerLevel;
	
	PokemonData(int buttonId, int configValue, int npcId, int displayZoom, PokemonStats baseStats, int minScale, int maxScale, int levelCapScale, int skillPointsPerLevel) {
		this.buttonId = buttonId;
		this.configValue = configValue;
		this.npcId = npcId;
		this.displayZoom = displayZoom;
		this.baseStats = baseStats;
		this.minScale = minScale;
		this.maxScale = maxScale;
		this.levelCapScale = levelCapScale;
		this.skillPointsPerLevel = skillPointsPerLevel;
	}
	
	public void textCommand(NPC n, String msg) {
	}
	
	public static final PokemonData[] data = values();
	
	public static Map<Integer, PokemonData> buttonToEnum = new HashMap<>();
	public static Map<Integer, PokemonData> npcIdToEnum = new HashMap<>();
	public static final int POKEMON_COUNT = data.length;
	public static final int[] POKEMON_XP_TABLE = new int[1500];
	
    public static void load() {
    	/* empty */
    }
   
	static {
		for (PokemonData entry : data) {
			buttonToEnum.put(entry.buttonId, entry);
			npcIdToEnum.put(entry.npcId, entry);
		}
		
		int startXp = 135;
		int addXp = 63;
		
		for (int level = 1; level < POKEMON_XP_TABLE.length; level++) {
			int multiplier = 5;
			if (level >= 210) {
				multiplier = 195;
			} else if (level >= 200) {
				multiplier = 45;
			} else if (level >= 195) {
				multiplier = 30;
			} else if (level >= 185) {
				multiplier = 24;
			} else if (level >= 150) {
				multiplier = 19;
			} else if (level >= 125) {
				multiplier = 14;
			} else if (level >= 99) {
				multiplier = 9;
			}
			POKEMON_XP_TABLE[level] = startXp + ((level - 1) * multiplier) * addXp;
			
		}
	}
	
	public static int fixId(int id) {
		switch(id) {
		
		case 4412:
		case 4411:
		case 4410:
		case 4409: 
		case 4408:
		case 4407:
		case 1769:
		case 41:
			return 1017; // real chicken id

		case 11232:
		case 11260:
			return 4261; // real goblin id

		case 4395:
		case 87:
			return 86; // real giant rat id
			
		case 4400:
		case 60:
			return 59; // real giant spider id
			
		case 4406:
			return 4404; // real minotaur id
			
		case 4402:
		case 4403:
			return 107; // real scorpion id
		
		case 4388:
		case 4387:
			return 103; // real ghost id
			
		case 1195:
			return 105; // real grizzly bear id 
			
		case 76:
		case 73:
		case 4392:
		case 4393:
		case 4394:
		case 5393:
			return 75; // real zombie id
			
		case 91:
		case 90:
		case 4386:
		case 4385:
		case 4384:
		case 5386:
			return 92; // real skeleton id
			
		case 1632:
			return 1631; // real rockslug id
			
		case 6376:
			return 62; // real jungle spider id
			
		case 1636:
			return 1633; // real pyrefiend id
			
		case 6379:
			return 6380; // real giant ant id
			
		case 6381:
			return 6383; // real giant wasp id
			
		case 4348:
		case 4351:
		case 4352:
			return 4350; // real jungle horror id
			
		case 1639:
			return 1637; // real jelly id		
			
		case 6239:
		case 6232:
			return 6233; // real avansie id
			
		case 1630:
		case 1627:
			return 1623; // real turoth id
			
		case 2454:
		case 2456:
		case 2455:
		case 1341:
			return 1343; // real dagannoth id
		
		case 1832:
			return 5750; // real cave bug id
			
		case 6272:
			return 6274; //real ork id
			
		case 4677:
		case 4678:
		case 4679:
		case 4680:
			return 491; // green dragons
			
		case 2012:
		case 2043:
		case 2044:
		    return 2045; //real zulrah id
		case 2045: // snakeling
			return -1;
			
		case 108615: //alchemical hydra
		case 108616:
		case 108617:
		case 108618:
		case 108619:
		case 108620:
		case 108621:
		case 108622:
		case 108634:
			return 108615;
			
		case 109433:
		case 109425:
		case 109426:
		case 109427:
		case 109428:
		case 109429: 
		case 109430:
		case 109431:
			return 109398;
			
		case 108369:
		case 108370:
		case 108372:
		case 108373:
		case 108374:
			return 108336;
			
		}
		return id;
	}
	
	public static int maxHit(Player c, NPC n) {
		if (c == null || n == null || c.summonedPokemon == null) {
			return 0;
		}

		Pokemon pokemon = c.summonedPokemon;

		int stat = 0;
		if (n.attackType == 0) {
			stat = pokemon.stats.strength;
		} else if (n.attackType == 1) {
			stat = pokemon.stats.range;
		} else if (n.attackType == 2) {
			stat = pokemon.stats.magic;
		}
		
		int extraDivider = stat / 100;
		 
		int maxHit = stat / (10 + extraDivider);

		if (c.summoned != null && c.summoned.powerUp != null) {
			maxHit += Pokemon.BOOST_MAX_HIT;
		}

		if (stat >= 1) {
			return maxHit + 1;
		}
		
		return 1;
	}

}
