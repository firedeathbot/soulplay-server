package com.soulplay.content.player.pets.pokemon;

import com.soulplay.content.minigames.soulwars.Avatar;
import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.attacks.NPCAttackDefinition;
import com.soulplay.game.model.npc.attacks.NPCAttackLoader;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerAssistant;
import com.soulplay.util.Misc;

public class Pokemon {
	
	public PokemonData data;
	public int exp;
	public int currentHp;
	public PokemonStats stats;
	public int statPoints;
	public int level = 1;
	public static final int HP = 0;
	public static final int STR = 1;
	public static final int DEF = 2;
	public static final int MAGE = 3;
	public static final int RANGE = 4;
	public static final int DOUBLE_EXP_SCROLL_TICKS = 6000;
	public static final float BOOST_GROWTH_SIZE = 0.5f;
	public static final int BOOST_MAX_HIT = 4;
	public static final int BOOST_DEFENCE = 50;
	public static final int BOOST_HP = 50;

	public Pokemon(PokemonData data, PokemonStats stats) {
		this.data = data;
		this.stats = new PokemonStats(stats);
		this.currentHp = stats.hp;
	}
	
	public void updateScale(Client c) {
		NPC summoned = c.summoned;

		//Safety reasons
		if (summoned == null) {
			return;
		}

		if (level >= data.levelCapScale) {
			summoned.setPetScale(data.maxScale);
			return;
		}

		int scale = Misc.interpolate(data.minScale, data.maxScale, 1, data.levelCapScale, level);
		if (summoned.powerUp != null) {
			scale = Math.min(scale + (int) (scale * BOOST_GROWTH_SIZE), data.maxScale);
		}

		summoned.setPetScale(scale);
	}

	public void updateDefence(Player player) {
		NPC summoned = player.summoned;

		if (summoned == null) {
			return;
		}

		int defence = level;
		if (summoned.powerUp != null) {
			defence += BOOST_DEFENCE;
		}

		summoned.getSkills().setStaticLevel(Skills.DEFENSE, Math.min(defence, 99));
	}

	public void updatePokemonStats(Player player) {
		NPC summoned = player.summoned;

		if (summoned == null) {
			return;
		}

		int str = stats.strength;
		int ranged = stats.range;
		int magic = stats.magic;

		summoned.getSkills().setStaticLevel(Skills.ATTACK, Math.min(str * 150 / 100, 200));
		summoned.getSkills().setStaticLevel(Skills.RANGED, Math.min(ranged * 150 / 100, 200));
		summoned.getSkills().setStaticLevel(Skills.MAGIC, Math.min(magic * 150 / 100, 200));
	}

	public void advanceXp(int xp, Client c, NPC npc) {
		if (data == PokemonData.TUTORIAL_GOBLIN) {
			if (level >= 2) { 
			    c.sendMessage("You don't need to train the goblin anymore. Head back to the Taming Master.");
				return;
			}
			exp += 250;
		} else {
			if (c.getPetDoubleExpLength() > 0) {
				xp *= 2;
			}
		}

		exp += xp;
		if (exp >= PokemonData.POKEMON_XP_TABLE[level]) {
			exp = 0;
			level++;
			if (level >= 10) {
				c.getAchievement().LevelPetTo10();
			}
			int statPoints = data.skillPointsPerLevel;
			NPCAttackDefinition attack = NPCAttackLoader.getPokemon(npc.npcType);
			if (attack != null) {
				int styles = attack.countAttacks();
				if (styles > 0) {
					statPoints *= styles;
				}
			}

			this.statPoints += statPoints;
			c.sendMessage("Your npc is now level " + level);
			npc.startGraphic(PlayerAssistant.LVL_UP_GRAPHIC);
			updateScale(c);
			updateDefence(c);

			if (data == PokemonData.TUTORIAL_GOBLIN && level >= 2) {
				c.sendMessage("Your goblin is now level 2! Head back to the Taming Master.");
				QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
			}
		}
	}

	public void advanceStat(int index, int advanceBy) {
		switch(index) {
			case HP:
				stats.hp += advanceBy;
				currentHp += advanceBy;
				break;
			case STR:
				stats.strength += advanceBy;
				break;
			case MAGE:
				stats.magic += advanceBy;
				break;
			case RANGE:
				stats.range += advanceBy;
				break;
		}

		statPoints -= 10;
	}
	
	public static void sendPetToAttack(Client c, NPC npc, int i) {
		c.selectPokemonAttack = false;
		if (c.summoned == null) {
			return;
		}

		if (!npc.isAttackable() || npc.getSkills().getLifepoints() <= 0 || npc.isPetNpc() || npc.isSummoningSkillNpc) {
			return;
		}

		if (c.summoned == null) {
			c.sendMessage("You dont have a pet to send to attack.");
			return;
		}

		if (!Avatar.canAttackAvatar(c, npc, false)) {
			return;
		}

		if (!allowedToBoltPouch(npc.npcType)) {
			c.sendMessage("Your pet cant attack that.");
			return;
		}

		if (npc.isPokemon && !RSConstants.inChampionsGuild(npc.getX(), npc.getY())) {
			c.sendMessage("You can only do this in Champion's Guild.");
			return;
		}

		if (npc.isPokemon && npc.underAttackBy2 > 0 && npc.underAttackBy2 != c.summoned.npcIndex) {
			c.sendMessage("Already under attack.");
			return;
		}

		c.faceLocation(npc.getX(), npc.getY());
		c.sendPokemonAtNpcIndex = c.summoned.killNpc = i;
		c.getMovement().resetWalkingQueue();
		c.lockMovement(1);
		c.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.getMovement().resetWalkingQueue();
			}

		}, 1, false);
	}
	
	public static boolean allowedToBoltPouch(int npcType) {
		switch (npcType) {
		case 8732:
		case 8729:
			return false;
		}
		return true;
	}

}
