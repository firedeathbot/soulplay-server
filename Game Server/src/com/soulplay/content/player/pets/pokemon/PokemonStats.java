package com.soulplay.content.player.pets.pokemon;

public class PokemonStats {
	
	public int hp;
	public int strength;
	public int magic;
	public int range;
	
	public PokemonStats(int hp, int strength, int magic, int range) {
		this.hp = hp;
		this.strength = strength;
		this.magic = magic;
		this.range = range;
	}

	public PokemonStats(PokemonStats stats) {
		this(stats.hp, stats.strength, stats.magic, stats.range);
	}

}
