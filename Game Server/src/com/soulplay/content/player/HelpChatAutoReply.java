package com.soulplay.content.player;

import com.soulplay.game.model.player.Client;

public class HelpChatAutoReply {

	public static void getAnswer(Client c, String msg) {
		if (msg.toLowerCase().contains("how much is")) {
			c.sendMessage("<col=800000>If you would like to know the price of an item, use ::webmarket");
			c.sendMessage("<col=800000>You can also lookup prices from shop at ::market using 'Julius Shop Keeper' NPC.");
		}
	}
}
