package com.soulplay.content.player.misc;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class StaffTimer {

	private int timer;
	
	public StaffTimer() {
		
	}
	
	public void inc() {
		timer++;
	}
	
	public int getTimer() {
		return timer;
	}
	
	public void reset() {
		timer = 0;
	}
	
	public void set(int tick) {
		this.timer = tick;
	}
	
	public static void resetTimers() {
		for (Player p : PlayerHandler.players) {
			if (p != null && p.isActive) {
				p.staffTimer.reset();
			}
		}
	}
}
