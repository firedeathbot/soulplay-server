package com.soulplay.content.player;

import com.soulplay.game.model.player.Client;

public class StrongholdOfSecurity {

	public static void handleObject(Client c, int id, int obX, int obY) {

		int SOSFloor1[][] = { // first floor for stronghold shit
				{1865, 5227}, {1865, 5226}, {1868, 5226}, {1868, 5227},
				{1867, 5217}, {1867, 5218}, {1870, 5217}, {1870, 5218},
				{1894, 5213}, {1894, 5212}, {1897, 5213}, {1897, 5212},
				{1904, 5203}, {1904, 5204}, {1907, 5203}, {1907, 5204},
				{1882, 5188}, {1882, 5189}, {1879, 5189}, {1879, 5188},
				{1879, 5240}, {1879, 5239}, {1876, 5240}, {1876, 5239},
				{1884, 5244}, {1884, 5243}, {1887, 5244}, {1887, 5243},
				{1889, 5235}, {1889, 5236}, {1886, 5235}, {1886, 5236},
				{1904, 5242}, {1904, 5243}, {1908, 5242}, {1908, 5243}

		};

		int SOSFloor2[][] = { // second floor for stronghold
				{2033, 5208}, {2034, 5208}, {2031, 5196}, {2032, 5196},
				{2036, 5201}, {2037, 5201}, {2045, 5195}, {2046, 5195},
				{2020, 5200}, {2021, 5200}, {2004, 5192}, {2005, 5192},
				{1994, 5194}, {1995, 5194}, {2005, 5235}, {2006, 5235},
				{2013, 5240}, {2014, 5240}, {2019, 5240}, {2020, 5240},
				{2026, 5239}, {2027, 5239}, {2031, 5225}, {2032, 5225},
				{2044, 5237}, {2045, 5240}

		};

		int SOSFloor3[][] = { // third floor for stronghold
				{2137, 5263}, {2137, 5262}, {2140, 5262}, {2140, 5263},
				{2153, 5264}, {2153, 5263}, {2156, 5264}, {2156, 5263},
				{2167, 5272}, {2167, 5271}, {2170, 5272}, {2170, 5271},
				{2137, 5295}, {2137, 5294}, {2140, 5295}, {2140, 5294},
				{2148, 5292}, {2148, 5291}, {2152, 5292}, {2152, 5291}

		};

		int SOSFloor4[][] = { // fourth floor for stronghold
				{2365, 5221}, {2366, 5221}, {2365, 5218}, {2366, 5218},
				{2355, 5221}, {2356, 5221}, {2355, 5218}, {2356, 5218},
				{2341, 5224}, {2340, 5224}, {2340, 5221}, {2341, 5221},
				{2320, 5212}, {2319, 5212}, {2320, 5215}, {2319, 5215},
				{2148, 5292}, {2148, 5291}, {2152, 5292}, {2152, 5291}

		};

		switch (id) {

			case 16154: // stronghold entrance
				c.getPA().movePlayer(1860, 5244, 0);
				break;

			case 16123:
			case 16124:
				for (int[] element : SOSFloor1) {
					if (c.absX == element[0] && c.absY == element[1]) {
						c.getPA().walkTo(-1, 0, 0, 0);
						return;
					}
				}
				if (c.absX == 1890 && c.absY == 5208
						|| c.absX == 1889 && c.absY == 5208
						|| c.absX == 1876 && c.absY == 5195
						|| c.absX == 1877 && c.absY == 5195
						|| c.absX == 1876 && c.absY == 5192
						|| c.absX == 1877 && c.absY == 5192) {
					c.getPA().walkTo(0, -1, 0, 0);
					return;
				}
				if (c.absX == obX && c.absY == obY) {
					c.getPA().walkTo(0, 1, 0, 0);
				}
				if (c.absY == obY && c.absX < obX) {
					c.getPA().walkTo(1, 0, 0, 0);
				}
				if (c.absY > obY && c.absX == obX) {
					c.getPA().walkTo(0, -1, 0, 0);
				}
				if (c.absY < obY && c.absX == obX) {
					c.getPA().walkTo(0, 1, 0, 0);
				}
				break;

			case 16065:
			case 16066:
				c.sendMessage("open door");
				for (int[] element : SOSFloor2) {
					if (c.absX == element[0] && c.absY == element[1]) {
						c.getPA().walkTo(0, -1, 0, 0);
						c.sendMessage("open door1");
						return;
					}
				}
				if (c.absX == 2034 && c.absY == 5185
						|| c.absX == 2034 && c.absY == 5186
						|| c.absX == 2037 && c.absY == 5245
						|| c.absX == 2037 && c.absY == 5244
						|| c.absX == 2040 && c.absY == 5222
						|| c.absX == 2040 && c.absY == 5223
						|| c.absX == 1997 && c.absY == 5215
						|| c.absX == 1997 && c.absY == 5216
						|| c.absX == 2006 && c.absY == 5215
						|| c.absX == 2006 && c.absY == 5216
						|| c.absX == 2016 && c.absY == 5227
						|| c.absX == 2016 && c.absY == 5228) {
					c.getPA().walkTo(-1, 0, 0, 0);
					c.sendMessage("open door2");
					return;
				}
				if (c.absX == 2042 && c.absY == 5223
						|| c.absX == 2042 && c.absY == 5222
						|| c.absX == 2039 && c.absY == 5244
						|| c.absX == 2039 && c.absY == 5245
						|| c.absX == 2041 && c.absY == 5222
						|| c.absX == 2041 && c.absY == 5223
						|| c.absX == 2036 && c.absY == 5185
						|| c.absX == 2036 && c.absY == 5186
						|| c.absX == 1999 && c.absY == 5215
						|| c.absX == 1999 && c.absY == 5216
						|| c.absX == 2008 && c.absY == 5216
						|| c.absX == 2008 && c.absY == 5216
						|| c.absX == 2018 && c.absY == 5227
						|| c.absX == 2018 && c.absY == 5228) {
					c.getPA().walkTo(1, 0, 0, 0);
					c.sendMessage("open door10");
					return;
				}
				if (c.absX == obX && c.absY == obY) {
					c.getPA().walkTo(0, 1, 0, 0);
				}
				if (c.absY == obY && c.absX < obX) {
					c.getPA().walkTo(1, 0, 0, 0);
				}
				if (c.absY > obY && c.absX == obX) {
					c.getPA().walkTo(0, -1, 0, 0);
				}
				if (c.absY < obY && c.absX == obX) {
					c.getPA().walkTo(0, 1, 0, 0);
				}
				if (c.absX > obX && c.absY == obY) {
					c.getPA().walkTo(-1, 0, 0, 0);
				}
				break;

			case 16114:
				c.getPA().movePlayer(2026, 5217, 0);
				break;

			case 16089:
			case 16090:
				for (int[] element : SOSFloor3) {
					if (c.absX == element[0] && c.absY == element[1]) {
						c.getPA().walkTo(1, 0, 0, 0);
						return;
					}
				}
				if (c.absX == 2166 && c.absY == 5259
						|| c.absX == 2167 && c.absY == 5259) {
					c.getPA().walkTo(0, 1, 0, 0);
					return;
				}
				if (c.absX == 2149 && c.absY == 5291
						|| c.absX == 2149 && c.absY == 5292
						|| c.absX == 2141 && c.absY == 5294
						|| c.absX == 2141 && c.absY == 5295
						|| c.absX == 2138 && c.absY == 5294
						|| c.absX == 2138 && c.absY == 5295
						|| c.absX == 2127 && c.absY == 5287
						|| c.absX == 2127 && c.absY == 5288
						|| c.absX == 2124 && c.absY == 5287
						|| c.absX == 2124 && c.absY == 5288) {
					c.getPA().walkTo(-1, 0, 0, 0);
					return;
				}
				if (c.absX == 2132 && c.absY == 5281
						|| c.absX == 2133 && c.absY == 5281
						|| c.absX == 2132 && c.absY == 5278
						|| c.absX == 2133 && c.absY == 5278) {
					c.getPA().walkTo(0, 1, 0, 0);
					return;
				}
				if (c.absX == obX && c.absY == obY) {
					c.getPA().walkTo(0, -1, 0, 0);
				}
				if (c.absY == obY && c.absX < obX) {
					c.getPA().walkTo(1, 0, 0, 0);
				}
				if (c.absY > obY && c.absX == obX) {
					c.getPA().walkTo(0, -1, 0, 0);
				}
				if (c.absY < obY && c.absX == obX) {
					c.getPA().walkTo(0, 1, 0, 0);
				}
				if (c.absX > obX && c.absY == obY) {
					c.getPA().walkTo(-1, 0, 0, 0);
				}
				break;

			case 16112:
				c.getPA().movePlayer(2026, 5217, 0);
				break;

			case 16115:
				if (c.getStrongholdLevel() >= 3) {
					c.getPA().movePlayer(2358, 5215, 0);
				} else {
					c.sendMessage(
							"You must open the box before going to the next floor.");
				}
				return;

			case 16048:
				c.getPA().movePlayer(2147, 5284, 0);
				break;

			case 16078:
				c.getPA().movePlayer(1902, 5221, 0);
				break;

			case 16043:
			case 16044:
				for (int[] element : SOSFloor4) {
					if (c.absX == element[0] && c.absY == element[1]) {
						c.getPA().walkTo(0, 1, 0, 0);
						return;
					}
				}
				if (c.absX == 2324 && c.absY == 5191
						|| c.absX == 2323 && c.absY == 5191
						|| c.absX == 2323 && c.absY == 5188
						|| c.absX == 2324 && c.absY == 5188
						|| c.absX == 2324 && c.absY == 5240
						|| c.absX == 2323 && c.absY == 5240
						|| c.absX == 2324 && c.absY == 5243
						|| c.absX == 2323 && c.absY == 5243
						|| c.absX == 2360 && c.absY == 5232
						|| c.absX == 2359 && c.absY == 5232
						|| c.absX == 2360 && c.absY == 5235
						|| c.absX == 2359 && c.absY == 5235) {
					c.getPA().walkTo(0, -1, 0, 0);
					c.sendMessage("open door2");
					return;
				}
				if (c.absX == obX && c.absY == obY) {
					c.getPA().walkTo(-1, 0, 0, 0);
				}
				if (c.absY == obY && c.absX < obX) {
					c.getPA().walkTo(1, 0, 0, 0);
				}
				if (c.absY > obY && c.absX == obX) {
					c.getPA().walkTo(0, -1, 0, 0);
				}
				if (c.absY < obY && c.absX == obX) {
					c.getPA().walkTo(0, 1, 0, 0);
				}
				if (c.absX > obX && c.absY == obY) {
					c.getPA().walkTo(-1, 0, 0, 0);
				}
				break;

			case 16150:
				if (c.getStrongholdLevel() >= 1) {
					c.getPA().movePlayer(1908, 5226, 0);
				} else {
					c.sendMessage(
							"You need to finish this floor before using this portal.");
				}
				break;

			case 16082:
				if (c.getStrongholdLevel() >= 2) {
					c.getPA().movePlayer(2021, 5220, 0);
				} else {
					c.sendMessage(
							"You need to finish this floor before using this portal.");
				}
				break;

			case 16116:
				if (c.getStrongholdLevel() >= 3) {
					c.getPA().movePlayer(2145, 5276, 0);
				} else {
					c.sendMessage(
							"You need to finish this floor before using this portal.");
				}
				break;

			case 16050:
				if (c.getStrongholdLevel() >= 4) {
					c.getPA().movePlayer(2344, 5210, 0);
				} else {
					c.sendMessage(
							"You need to finish this floor before using this portal.");
				}
				break;

			case 16135:
				if (!c.hasBankPin) {
					c.sendMessage(
							"You must set a bank pin before opening this chest!");
					return;
				}
				if (c.getItems().freeSlots() < 1) {
					c.sendMessage(
							"You must have at least 1 free slot in your inventory!");
					return;
				}
				if (c.getStrongholdLevel() == 0) {
					c.setStrongholdLevel(c.getStrongholdLevel() + 1, true);
					c.getItems().addItem(995, 150000);
					break;
				} else {
					c.sendMessage("You have already received this reward!");
				}
				break;

			case 16077:
				if (!c.hasBankPin) {
					c.sendMessage(
							"You must set a bank pin before opening this chest!");
					return;
				}
				if (c.getItems().freeSlots() < 1) {
					c.sendMessage(
							"You must have at least 1 free slot in your inventory!");
					return;
				}
				if (c.getStrongholdLevel() == 1) {
					c.setStrongholdLevel(c.getStrongholdLevel() + 1, true);
					c.getItems().addItem(995, 300000);
				} else {
					c.sendMessage("You have already received this reward!");
				}
				break;

			case 16118:
				if (!c.hasBankPin) {
					c.sendMessage(
							"You must set a bank pin before opening this chest!");
					return;
				}
				if (c.getItems().freeSlots() < 1) {
					c.sendMessage(
							"You must have at least 1 free slot in your inventory!");
					return;
				}
				if (c.getStrongholdLevel() == 2) {
					c.setStrongholdLevel(c.getStrongholdLevel() + 1, true);
					c.getItems().addItem(995, 500000);
				} else {
					c.sendMessage("You have already received this reward!");
				}
				break;

			case 16047:
				if (!c.hasBankPin) {
					c.sendMessage(
							"You must set a bank pin before opening this chest!");
					return;
				}
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage(
							"You must have at least 2 free slots in your inventory!");
					return;
				}
				if (c.getStrongholdLevel() == 3) {
					c.setStrongholdLevel(c.getStrongholdLevel() + 1, true);
					c.getItems().addItem(9005, 1);
					c.getItems().addItem(9006, 1);
				} else {
					c.sendMessage("You have already received this reward!");
				}
				break;
		}
	}
}
