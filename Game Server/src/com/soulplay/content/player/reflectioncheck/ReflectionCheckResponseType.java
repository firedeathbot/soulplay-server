package com.soulplay.content.player.reflectioncheck;

import java.util.HashMap;
import java.util.Map;

public enum ReflectionCheckResponseType {
	
	SUCCESS(0, "Successful."),
	INVOKE_METHOD_RETURN_INT(1, "Invoked method returned: "),
	INVOKE_METHOD_RETURN_STRING(2, "Invoked method returned: "),
	INVOKE_METHOD_RETURN_NOT_HANDLED(4, "Invoked method returned unhandled value."),
	CLASS_NOT_FOUND(-1, "Class not found server sent: "),
	SECURITY_EXCEPTION(-2, "Security exception server sent."),
	NULL_POINTER(-3, "Null pointer server sent!"),
	EXCEPTION(-4, "Exception server sent!"),
	THROWABLE(-5, "Catch it server sent!"),

	CLASS_NOT_FOUND_CLIENT(-10, "Class not found: "),
	INVALID_CLASS(-11, "Invalid class: "),
	STREAM_CORRUPTED(-12, "Stream corrupted, check serializing code xd"),
	OPTIONAL_DATA_EXCEPTION(-13, "Optional data?"),
	ILLEGAL_ACCESS(-14, "No access to the method, field bruh"),
	ILLEGAL_ARGUMENT(-15, "Wrong argument on method?"),
	INVOCATION_TARGET(-16, "No idea what this is."),
	SECURITY_EXCEPTION_CLIENT(-17, "Security exception."),
	INPUT_OUTPUT_EXCEPTION(-18, "Input/Output exception? How"),
	NULL_POINTER_CLIENT(-19, "Null pointer!"),
	EXCEPTION_CLIENT(-20, "Exception!!"),
	THROWABLE_CLIENT(-21, "Catch it from client!"),

	UNKNOWN_REFLECTION_CHECK_ENTRY(-67, "Unknown check entry, modified client or an error? Try again."),
	FAILED_CRC_CHECK(-68, "Failed crc check, try again, if fail again modified client."),
	UNKNOWN_RESPONSE(-69, "Unknown response? This even more weirder, something fishy!");

	public static final Map<Integer, ReflectionCheckResponseType> values = new HashMap<>();
	private final int type;
	private final String responseMessage;

	private ReflectionCheckResponseType(int type, String responseMessage) {
		this.type = type;
		this.responseMessage = responseMessage;
	}

	public int getType() {
		return type;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	static {
		for (ReflectionCheckResponseType value : values()) {
			values.put(value.getType(), value);
		}
	}
}
