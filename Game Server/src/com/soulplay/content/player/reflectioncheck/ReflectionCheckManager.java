package com.soulplay.content.player.reflectioncheck;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;

public class ReflectionCheckManager {

	public static final int REQUEST_FIELD = 0;
	public static final int REQUEST_METHOD_MOD = 4;

	private static Random random = new Random();
	private static Map<Integer, ReflectionCheckEntry> requests = new HashMap<>();

	public static void sendGetFieldRequest(Player sender, Player receiver, String className, String fieldName) {
		ReflectionCheckEntry entry = new ReflectionCheckEntry(sender, REQUEST_FIELD, Server.getTotalTicks(), className, fieldName);

		int uid = random.nextInt();

		requests.put(uid, entry);

		receiver.getPacketSender().reflectionCheckGetField(uid, className, fieldName);
	}

	public static void sendGetMethodRequest(Player sender, Player receiver, String className, String methodName, String returnType, String[] arguments) {
		ReflectionCheckEntry entry = new ReflectionCheckEntry(sender, REQUEST_METHOD_MOD, Server.getTotalTicks(), className, methodName);

		int uid = random.nextInt();

		requests.put(uid, entry);

		receiver.getPacketSender().reflectionCheckGetMethodMod(uid, className, methodName, returnType, arguments);
	}

	public static ReflectionCheckEntry getEntryByUid(int uid) {
		return requests.remove(uid);
	}

}
