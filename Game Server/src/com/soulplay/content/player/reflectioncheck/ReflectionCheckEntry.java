package com.soulplay.content.player.reflectioncheck;

import com.soulplay.game.model.player.Player;

public class ReflectionCheckEntry {

	private final Player sender;
	private final int type, tick;
	private final String className, entryName;

	public ReflectionCheckEntry(Player sender, int type, int tick, String className, String entryName) {
		this.sender = sender;
		this.type = type;
		this.tick = tick;
		this.className = className;
		this.entryName = entryName;
	}

	public Player getSender() {
		return sender;
	}

	public int getType() {
		return type;
	}

	public int getTick() {
		return tick;
	}

	public String getClassName() {
		return className;
	}

	public String getEntryName() {
		return entryName;
	}

}
