package com.soulplay.content.player.titles;

import java.util.Observable;

public class ProgressTracker extends Observable {

	private final int total;

	private int increment = 1;

	private int progress;

	public ProgressTracker(int total) {
		this.total = total;
	}

	public void decrease() {
		if (progress - increment >= 0) {
			this.progress -= increment;
		} else {
			this.progress = 0;
		}
		this.setChanged();
		this.notifyObservers(progress);
	}

	public int getIncrement() {
		return increment;
	}

	public Percentage getPercentageCompleted() {
		return new Percentage((progress * 100.0f) / total);
	}

	public int getProgress() {
		return progress;
	}

	public int getTotal() {
		return total;
	}

	public void increase() {
		if (progress + increment <= total) {
			this.progress += increment;
		} else {
			this.progress = total;
		}
		this.setChanged();
		this.notifyObservers(progress);
	}

	public void setIncrement(int increment) {
		this.increment = increment;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}
}
