package com.soulplay.content.player.titles;

public enum TitleData {
	THE_REAL("The Real", 42054, 1),
	THE_LOYAL("The Loyal", 42054 + 10, 345600),
	THE_DEVOTED("The Devoted", 42054 + 20, 864000),
	THE_FAMOUS("The Famous", 42054 + 30, 1728000),
	SKILLER("Skiller", 42054 + 40, 1470),
	MAXIMUM("Maximum", 42054 + 50, 2147),
	THE_COMPLETIONIST("The Completionist", 42054 + 60, 100000),
	WEAKLING("Weakling", 42054 + 70, 1),
	COWARDLY("Cowardly", 42054 + 80, 10),
	SIR_LAME("Sir Lame", 42054 + 90, 50),
	GRUMPY("Grumpy", 42054 + 100, 100),
	OVERLORD("Overlord", 42054 + 110, 10),
	PKER("PKer", 42054 + 120, 50),
	CHEERFUL("Cheerful", 42054 + 130, 100),
	WAR_CHIEF("War-Chief", 42054 + 140, 200),
	PK_MASTER("PK Master", 42054 + 150, 1000),
	THE_AVENGER("The Avenger", 42054 + 160, 1),
	DUEL_MASTER("Duel Master", 42054 + 170, 100),
	THE_EXPLORER("The Explorer", 42054 + 180, 115),
	THE_WISE("The Wise", 42054 + 190, 1),
	THE_WEALTHY("The Wealthy", 42054 + 200, 1),
	YT_HAAR("Yt'Haar", 42054 + 210, 1),
	CHAMPION("Champion", 42054 + 220, 1),
	THE_SKILFUL("The Skillful", 42054 + 230, 98),
	INSANE("Insane", 42054 + 240, 1),
	MASTER("Master", 42054 + 250, 1),
	WARRIOR("Warrior", 42054 + 260, 1),
	ARCHER("Archer", 42054 + 270, 1),
	MAGICIAN("Magician", 42054 + 280, 1),
	IRONMAN("Ironman", 42054 + 290, 1),
	THE_MIGHTY("The Mighty", 42054 + 300, 2147),
	THE_FAITHFUL("The Faithful", 42054 + 310, 1),
	THE_LUCKY("The Lucky", 42054 + 320, 1),
	GAMBLER("Gambler", 42054 + 330, 1),
	THE_BILLIONAIRE("The Billionaire", 42054 + 340, 1),
	LAZY("Lazy", 42054 + 350, 1),
	RIOTER("Rioter", 42054 + 360, 1),
	CLAN_LEADER("Clan Leader", 42054 + 370, 1),
	VOID_KNIGHT("Void Knight", 42054 + 380, 50),
	HIDENSEEKER("HideNSeeker", 42054 + 390, 7),
	WINGMAN("Wingman", 42054 + 400, 20),
	THE_BOSS("The Boss", 42054 + 410, 1),
	KING("King", 42054 + 420, 10),
	QUEEN("Queen", 42054 + 430, 10),
	PVM_MASTER("PVM Master", 42054 + 440, 2000),
	DONATOR("Donator", 42054 + 450, 1),
	SUPER("Super", 42054 + 460, 1),
	SUPREME("Supreme", 42054 + 470, 1),
	LEGENDARY("Legendary", 42054 + 480, 1),
	UBER("Uber", 42054 + 490, 1),
	NEW_DONATOR_RANK("New donator rank", 42054 + 500, 1),
	LORD("Lord", 42054 + 510, 1),
	LEGEND("Legend", 42054 + 520, 1),
	EXTREME("Extreme", 42054 + 530, 1);
	
	public static final TitleData[] values = TitleData.values();

	private int unlocked;

	private String name;

	private int stringId;

	private TitleData(String titleName, int stringId, int amountForCompleted) {
		this.name = titleName;
		this.stringId = stringId;
		this.unlocked = amountForCompleted;
	}

	public String getName() {
		return name;
	}

	public int getUnlocked() {
		return unlocked;
	}

	public int stringId() {
		return stringId;
	}

}
