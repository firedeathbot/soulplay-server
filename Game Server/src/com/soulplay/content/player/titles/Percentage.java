package com.soulplay.content.player.titles;

public class Percentage {

	private float percent;

	public Percentage(float percent) {
		this.setPercent(percent);
	}

	public float getPercent() {
		return percent;
	}

	public void setPercent(float percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		return percent + "%";
	}
}
