package com.soulplay.content.player.titles;

public enum Stage {
	NOT_STARTED, IN_PROGRESS, COMPLETED;
}
