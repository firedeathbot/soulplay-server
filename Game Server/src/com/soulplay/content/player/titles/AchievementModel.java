package com.soulplay.content.player.titles;

public class AchievementModel {

	private final int id;

	private final String name;

	private final String description;

	public AchievementModel(String name, String description, int id) {
		this.name = name;
		this.id = id;
		this.description = description;
	}

	public String congratulations() {
		return "Congratulations you've completed the achievement " + name + "!";
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
