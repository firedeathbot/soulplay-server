package com.soulplay.content.player.titles;

import java.util.Observable;
import java.util.Observer;

public class Achievement implements Observer {

	private Stage stage;

	private final ProgressTracker progress;

	public Achievement(int progress) {
		this.progress = new ProgressTracker(progress);
		this.progress.addObserver(this);
	}

	public ProgressTracker getProgress() {
		return progress;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	@Override
	public void update(Observable model, Object value) {
		if (this.getStage() != Stage.COMPLETED) {
			if (progress.getProgress() == progress.getTotal()) {
				System.out.println("Achievement completed!");
				this.setStage(Stage.COMPLETED);
			} else if (progress.getProgress() > 0) {
				this.setStage(Stage.IN_PROGRESS);
			} else {
				this.setStage(Stage.NOT_STARTED);
			}
		}
	}
}
