package com.soulplay.content.player.titles;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.UpdateFlag;

public class Titles {

	// public Titles(Client c) {
	// this.c = c;
	// }

	// private Client c;

	// public boolean[] unlocked = new boolean[48];

	public static String[] titles = {"The Real", "The Loyal", "The Devoted",
			"The Famous", "Skiller", "Maximum", "The Completionist", "Weakling",
			"Cowardly", "Sir Lame", "Grumpy", "Overlord", "PKer", "Cheerful",
			"War-Chief", "PK Master", "The Avenger", "Duel Master",
			"The Explorer", "The Wise", "The Wealthy", "Yt'Haar", "Champion",
			"The Skillful", "Insane", "Master", "Warrior", "Archer", "Magician",
			"Ironman", "The Mighty", "The Faithful", "The Lucky", "Gambler",
			"The Billionaire", "Lazy", "Rioter", "Clan Leader", "Void Knight",
			"HideNSeeker", "Wingman", "The Boss", "King", "Queen", "PVM Master",
			"Donator", "Super", "Supreme", "Legendary", "Uber", "New donator rank", "Lord", "Legend", "Extreme"};

	public static void achiev120Stat(Client c) {
		if (c.getTitleInt(TitleData.INSANE.ordinal()) < TitleData.INSANE
				.getUnlocked()) {
			c.setTitleInt(TitleData.INSANE.ordinal(),
					c.getTitleInt(TitleData.INSANE.ordinal()) + 1);
			if (c.getTitleInt(TitleData.INSANE.ordinal()) == TitleData.INSANE
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Insane",
						+TitleData.INSANE.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.INSANE.getName() + " Title ");
			}
		}
	}

	public static void achiev99Stat(Client c) {
		if (c.getTitleInt(
				TitleData.THE_SKILFUL.ordinal()) < TitleData.THE_SKILFUL
						.getUnlocked()) {
			c.setTitleInt(TitleData.THE_SKILFUL.ordinal(),
					c.getTitleInt(TitleData.THE_SKILFUL.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_SKILFUL.ordinal()) == TitleData.THE_SKILFUL
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Skillful",
						+TitleData.THE_SKILFUL.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_SKILFUL.getName() + " Title ");
			}
		}
	}

	public static void clickButtons(Client c, int buttonId) {
		Titles.unlockTitles(c);

		switch (buttonId) {

			case 164066:
				if (c.titles[TitleData.THE_REAL.ordinal()] == TitleData.THE_REAL
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_REAL.ordinal()],
							TitleData.THE_REAL.stringId());
					// c.playerTitle = TitleData.THE_REAL.getName();
					c.dialogueAction = 400;
				}
				break;

			case 164076:
				if (c.titles[TitleData.THE_LOYAL
						.ordinal()] == TitleData.THE_LOYAL.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_LOYAL.ordinal()],
							TitleData.THE_LOYAL.stringId());
					// c.playerTitle = TitleData.THE_LOYAL.getName();
					c.dialogueAction = 401;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164086:
				if (c.titles[TitleData.THE_DEVOTED
						.ordinal()] == TitleData.THE_DEVOTED.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_DEVOTED.ordinal()],
							TitleData.THE_DEVOTED.stringId());
					// c.playerTitle = TitleData.THE_DEVOTED.getName();
					c.dialogueAction = 402;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164096:
				if (c.titles[TitleData.THE_FAMOUS
						.ordinal()] == TitleData.THE_FAMOUS.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_FAMOUS.ordinal()],
							TitleData.THE_FAMOUS.stringId());
					// c.playerTitle = TitleData.THE_FAMOUS.getName();
					c.dialogueAction = 403;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164106:
				if (c.titles[TitleData.SKILLER.ordinal()] == TitleData.SKILLER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.SKILLER.ordinal()],
							TitleData.SKILLER.stringId());
					// c.playerTitle = TitleData.SKILLER.getName();
					c.dialogueAction = 404;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164116:
				if (c.titles[TitleData.MAXIMUM.ordinal()] == TitleData.MAXIMUM
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.MAXIMUM.ordinal()],
							TitleData.MAXIMUM.stringId());
					// c.playerTitle = TitleData.MAXIMUM.getName();
					c.dialogueAction = 405;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164126:
				if (c.titles[TitleData.THE_COMPLETIONIST
						.ordinal()] == TitleData.THE_COMPLETIONIST
								.getUnlocked()) {
					c.getPacketSender().sendFrame126("<col=ffffff>"
							+ titles[TitleData.THE_COMPLETIONIST.ordinal()],
							TitleData.THE_COMPLETIONIST.stringId());
					// c.playerTitle = TitleData.THE_COMPLETIONIST.getName();
					c.dialogueAction = 406;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164136:
				if (c.titles[TitleData.WEAKLING.ordinal()] == TitleData.WEAKLING
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.WEAKLING.ordinal()],
							TitleData.WEAKLING.stringId());
					// c.playerTitle = TitleData.WEAKLING.getName();
					c.dialogueAction = 407;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164146:
				if (c.titles[TitleData.COWARDLY.ordinal()] == TitleData.COWARDLY
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.COWARDLY.ordinal()],
							TitleData.COWARDLY.stringId());
					// c.playerTitle = TitleData.COWARDLY.getName();
					c.dialogueAction = 408;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164156:
				if (c.titles[TitleData.SIR_LAME.ordinal()] == TitleData.SIR_LAME
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.SIR_LAME.ordinal()],
							TitleData.SIR_LAME.stringId());
					// c.playerTitle = TitleData.SIR_LAME.getName();
					c.dialogueAction = 409;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164166:
				if (c.titles[TitleData.GRUMPY.ordinal()] == TitleData.GRUMPY
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.GRUMPY.ordinal()],
							TitleData.GRUMPY.stringId());
					// c.playerTitle = TitleData.GRUMPY.getName();
					c.dialogueAction = 410;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164176:
				if (c.titles[TitleData.OVERLORD.ordinal()] == TitleData.OVERLORD
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.OVERLORD.ordinal()],
							TitleData.OVERLORD.stringId());
					// c.playerTitle = TitleData.OVERLORD.getName();
					c.dialogueAction = 411;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164186:
				if (c.titles[TitleData.PKER.ordinal()] == TitleData.PKER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.PKER.ordinal()],
							TitleData.PKER.stringId());
					// c.playerTitle = TitleData.PKER.getName();
					c.dialogueAction = 412;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164196:
				if (c.titles[TitleData.CHEERFUL.ordinal()] == TitleData.CHEERFUL
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.CHEERFUL.ordinal()],
							TitleData.CHEERFUL.stringId());
					// c.playerTitle = TitleData.CHEERFUL.getName();
					c.dialogueAction = 413;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164206:
				if (c.titles[TitleData.WAR_CHIEF
						.ordinal()] == TitleData.WAR_CHIEF.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.WAR_CHIEF.ordinal()],
							TitleData.WAR_CHIEF.stringId());
					// c.playerTitle = TitleData.WAR_CHIEF.getName();
					c.dialogueAction = 414;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164216:
				if (c.titles[TitleData.PK_MASTER
						.ordinal()] == TitleData.PK_MASTER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.PK_MASTER.ordinal()],
							TitleData.PK_MASTER.stringId());
					// c.playerTitle = TitleData.PK_MASTER.getName();
					c.dialogueAction = 415;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164226:
				if (c.titles[TitleData.THE_AVENGER
						.ordinal()] == TitleData.THE_AVENGER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_AVENGER.ordinal()],
							TitleData.THE_AVENGER.stringId());
					// c.playerTitle = TitleData.THE_AVENGER.getName();
					c.dialogueAction = 416;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164236:
				if (c.titles[TitleData.DUEL_MASTER
						.ordinal()] == TitleData.DUEL_MASTER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.DUEL_MASTER.ordinal()],
							TitleData.DUEL_MASTER.stringId());
					// c.playerTitle = TitleData.DUEL_MASTER.getName();
					c.dialogueAction = 417;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 164246:
				if (c.titles[TitleData.THE_EXPLORER
						.ordinal()] == TitleData.THE_EXPLORER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_EXPLORER.ordinal()],
							TitleData.THE_EXPLORER.stringId());
					// c.playerTitle = TitleData.THE_EXPLORER.getName();
					c.dialogueAction = 418;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165000:
				if (c.titles[TitleData.THE_WISE.ordinal()] == TitleData.THE_WISE
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_WISE.ordinal()],
							TitleData.THE_WISE.stringId());
					// c.playerTitle = TitleData.THE_WISE.getName();
					c.dialogueAction = 419;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165010:
				if (c.titles[TitleData.THE_WEALTHY
						.ordinal()] == TitleData.THE_WEALTHY.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_WEALTHY.ordinal()],
							TitleData.THE_WEALTHY.stringId());
					// c.playerTitle = TitleData.THE_WEALTHY.getName();
					c.dialogueAction = 420;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165020:
				if (c.titles[TitleData.YT_HAAR.ordinal()] == TitleData.YT_HAAR
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.YT_HAAR.ordinal()],
							TitleData.YT_HAAR.stringId());
					// c.playerTitle = TitleData.YT_HAAR.getName();
					c.dialogueAction = 421;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165030:
				if (c.titles[TitleData.CHAMPION.ordinal()] == TitleData.CHAMPION
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.CHAMPION.ordinal()],
							TitleData.CHAMPION.stringId());
					// c.playerTitle = TitleData.CHAMPION.getName();
					c.dialogueAction = 422;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165040:
				if (c.titles[TitleData.THE_SKILFUL
						.ordinal()] == TitleData.THE_SKILFUL.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_SKILFUL.ordinal()],
							TitleData.THE_SKILFUL.stringId());
					// c.playerTitle = TitleData.THE_SKILFUL.getName();
					c.dialogueAction = 423;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165050:
				if (c.titles[TitleData.INSANE.ordinal()] == TitleData.INSANE
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.INSANE.ordinal()],
							TitleData.INSANE.stringId());
					// c.playerTitle = TitleData.INSANE.getName();
					c.dialogueAction = 424;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165060:
				if (c.titles[TitleData.MASTER.ordinal()] == TitleData.MASTER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.MASTER.ordinal()],
							TitleData.MASTER.stringId());
					// c.playerTitle = TitleData.MASTER.getName();
					c.dialogueAction = 425;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165070:
				if (c.titles[TitleData.WARRIOR.ordinal()] == TitleData.WARRIOR
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.WARRIOR.ordinal()],
							TitleData.WARRIOR.stringId());
					// c.playerTitle = TitleData.WARRIOR.getName();
					c.dialogueAction = 426;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165080:
				if (c.titles[TitleData.ARCHER.ordinal()] == TitleData.ARCHER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.ARCHER.ordinal()],
							TitleData.ARCHER.stringId());
					// c.playerTitle = TitleData.ARCHER.getName();
					c.dialogueAction = 427;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165090:
				if (c.titles[TitleData.MAGICIAN.ordinal()] == TitleData.MAGICIAN
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.MAGICIAN.ordinal()],
							TitleData.MAGICIAN.stringId());
					// c.playerTitle = TitleData.MAGICIAN.getName();
					c.dialogueAction = 428;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165100:
				if (c.titles[TitleData.IRONMAN.ordinal()] == TitleData.IRONMAN
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.IRONMAN.ordinal()],
							TitleData.IRONMAN.stringId());
					// c.playerTitle = TitleData.IRONMAN.getName();
					c.dialogueAction = 429;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165110:
				if (c.titles[TitleData.THE_MIGHTY
						.ordinal()] == TitleData.THE_MIGHTY.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_MIGHTY.ordinal()],
							TitleData.THE_MIGHTY.stringId());
					// c.playerTitle = TitleData.THE_MIGHTY.getName();
					c.dialogueAction = 430;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165120:
				if (c.titles[TitleData.THE_FAITHFUL
						.ordinal()] == TitleData.THE_FAITHFUL.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_FAITHFUL.ordinal()],
							TitleData.THE_FAITHFUL.stringId());
					// c.playerTitle = TitleData.THE_FAITHFUL.getName();
					c.dialogueAction = 431;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165130:
				if (c.titles[TitleData.THE_LUCKY
						.ordinal()] == TitleData.THE_LUCKY.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_LUCKY.ordinal()],
							TitleData.THE_LUCKY.stringId());
					// c.playerTitle = TitleData.THE_LUCKY.getName();
					c.dialogueAction = 432;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165140:
				if (c.titles[TitleData.GAMBLER.ordinal()] == TitleData.GAMBLER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.GAMBLER.ordinal()],
							TitleData.GAMBLER.stringId());
					// c.playerTitle = TitleData.GAMBLER.getName();
					c.dialogueAction = 433;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165150:
				if (c.titles[TitleData.THE_BILLIONAIRE
						.ordinal()] == TitleData.THE_BILLIONAIRE
								.getUnlocked()) {
					c.getPacketSender().sendFrame126("<col=ffffff>"
							+ titles[TitleData.THE_BILLIONAIRE.ordinal()],
							TitleData.THE_BILLIONAIRE.stringId());
					// c.playerTitle = TitleData.THE_BILLIONAIRE.getName();
					c.dialogueAction = 434;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165160:
				if (c.titles[TitleData.LAZY.ordinal()] == TitleData.LAZY
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.LAZY.ordinal()],
							TitleData.LAZY.stringId());
					// c.playerTitle = TitleData.LAZY.getName();
					c.dialogueAction = 435;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165170:
				if (c.titles[TitleData.RIOTER.ordinal()] == TitleData.RIOTER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.RIOTER.ordinal()],
							TitleData.RIOTER.stringId());
					// c.playerTitle = TitleData.RIOTER.getName();
					c.dialogueAction = 436;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165180:
				if (c.titles[TitleData.CLAN_LEADER
						.ordinal()] == TitleData.CLAN_LEADER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.CLAN_LEADER.ordinal()],
							TitleData.CLAN_LEADER.stringId());
					// c.playerTitle = TitleData.CLAN_LEADER.getName();
					c.dialogueAction = 437;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165190:
				if (c.titles[TitleData.VOID_KNIGHT
						.ordinal()] == TitleData.VOID_KNIGHT.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.VOID_KNIGHT.ordinal()],
							TitleData.VOID_KNIGHT.stringId());
					// c.playerTitle = TitleData.VOID_KNIGHT.getName();
					c.dialogueAction = 438;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165200:
				if (c.titles[TitleData.HIDENSEEKER
						.ordinal()] == TitleData.HIDENSEEKER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.HIDENSEEKER.ordinal()],
							TitleData.HIDENSEEKER.stringId());
					// c.playerTitle = TitleData.HIDENSEEKER.getName();
					c.dialogueAction = 439;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165210:
				if (c.titles[TitleData.WINGMAN.ordinal()] == TitleData.WINGMAN
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.WINGMAN.ordinal()],
							TitleData.WINGMAN.stringId());
					// c.playerTitle = TitleData.WINGMAN.getName();
					c.dialogueAction = 440;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165220:
				if (c.titles[TitleData.THE_BOSS.ordinal()] == TitleData.THE_BOSS
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.THE_BOSS.ordinal()],
							TitleData.THE_BOSS.stringId());
					// c.playerTitle = TitleData.THE_BOSS.getName();
					c.dialogueAction = 441;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165230:
				if (c.titles[TitleData.KING.ordinal()] == TitleData.KING
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.KING.ordinal()],
							TitleData.KING.stringId());
					// c.playerTitle = TitleData.KING.getName();
					c.dialogueAction = 442;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165240:
				if (c.titles[TitleData.QUEEN.ordinal()] == TitleData.QUEEN
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.QUEEN.ordinal()],
							TitleData.QUEEN.stringId());
					// c.playerTitle = TitleData.QUEEN.getName();
					c.dialogueAction = 443;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 165250:
				if (c.titles[TitleData.PVM_MASTER
						.ordinal()] == TitleData.PVM_MASTER.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.PVM_MASTER.ordinal()],
							TitleData.PVM_MASTER.stringId());
					// c.playerTitle = TitleData.PVM_MASTER.getName();
					c.dialogueAction = 444;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 166004:
				if (c.titles[TitleData.DONATOR.ordinal()] == TitleData.DONATOR
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.DONATOR.ordinal()],
							TitleData.DONATOR.stringId());
					// c.playerTitle = TitleData.DONATOR.getName();
					c.dialogueAction = 445;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 166014:
				if (c.titles[TitleData.SUPER.ordinal()] == TitleData.SUPER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.SUPER.ordinal()],
							TitleData.SUPER.stringId());
					// c.playerTitle = TitleData.SUPER.getName();
					c.dialogueAction = 446;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 166024:
				if (c.titles[TitleData.SUPREME.ordinal()] == TitleData.SUPREME
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.SUPREME.ordinal()],
							TitleData.SUPREME.stringId());
					// c.playerTitle = TitleData.SUPREME.getName();
					c.dialogueAction = 447;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 166034:
				if (c.titles[TitleData.LEGENDARY
						.ordinal()] == TitleData.LEGENDARY.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.LEGENDARY.ordinal()],
							TitleData.LEGENDARY.stringId());
					// c.playerTitle = TitleData.LEGENDARY.getName();
					c.dialogueAction = 448;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			case 166044:
				if (c.titles[TitleData.UBER.ordinal()] == TitleData.UBER
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.UBER.ordinal()],
							TitleData.UBER.stringId());
					// c.playerTitle = TitleData.UBER.getName();
					c.dialogueAction = 449;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;
				
			case 166054:
				if (c.titles[TitleData.NEW_DONATOR_RANK.ordinal()] == TitleData.NEW_DONATOR_RANK
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.NEW_DONATOR_RANK.ordinal()],
							TitleData.NEW_DONATOR_RANK.stringId());
					// c.playerTitle = TitleData.UBER.getName();
					c.dialogueAction = 450;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;
				
			case 166064:
				if (c.titles[TitleData.LORD.ordinal()] == TitleData.LORD
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.LORD.ordinal()],
							TitleData.LORD.stringId());
					c.dialogueAction = 451;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;
				
			case 166074:
				if (c.titles[TitleData.LEGEND.ordinal()] == TitleData.LEGEND
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.LEGEND.ordinal()],
							TitleData.LEGEND.stringId());
					// c.playerTitle = TitleData.UBER.getName();
					c.dialogueAction = 452;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;
				
			case 166084:
				if (c.titles[TitleData.EXTREME.ordinal()] == TitleData.EXTREME
						.getUnlocked()) {
					c.getPacketSender().sendFrame126(
							"<col=ffffff>" + titles[TitleData.EXTREME.ordinal()],
							TitleData.EXTREME.stringId());
					c.dialogueAction = 453;
				} else {
					c.sendMessage("You haven't unlocked this title yet.");
				}
				break;

			// confirm button
			case 164020: // this is the purchase button
				if (c.getLoyaltyPoints() < 500) {
					c.sendMessage(
							"You need 500 LoyaltyPoints to buy this title.");
					return;
				}
				if (c.dialogueAction == 400) {
					c.playerTitle = TitleData.THE_REAL.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_REAL.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 401) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_LOYAL.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_LOYAL.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 402) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_DEVOTED.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_DEVOTED.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 403) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_FAMOUS.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_FAMOUS.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 404) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.SKILLER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.SKILLER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 405) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.MAXIMUM.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.MAXIMUM.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 406) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_COMPLETIONIST.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_COMPLETIONIST.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 407) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.WEAKLING.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.WEAKLING.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 408) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.COWARDLY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.COWARDLY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 409) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.SIR_LAME.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.SIR_LAME.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 410) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.GRUMPY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.GRUMPY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 411) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.OVERLORD.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.OVERLORD.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 412) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.PKER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.PKER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 413) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.CHEERFUL.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.CHEERFUL.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 414) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.WAR_CHIEF.getName();
					c.titleColor = 8;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.WAR_CHIEF.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 415) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.PK_MASTER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.PK_MASTER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 416) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_AVENGER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_AVENGER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 417) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.DUEL_MASTER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.DUEL_MASTER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 418) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_EXPLORER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_EXPLORER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 419) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_WISE.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_WISE.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 420) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_WEALTHY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_WEALTHY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 421) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.YT_HAAR.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.YT_HAAR.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 422) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.CHAMPION.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.CHAMPION.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 423) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_SKILFUL.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_SKILFUL.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 424) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.INSANE.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.INSANE.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 425) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.MASTER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.MASTER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 426) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.WARRIOR.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.WARRIOR.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 427) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.ARCHER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.ARCHER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 428) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.MAGICIAN.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.MAGICIAN.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 429) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.IRONMAN.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.IRONMAN.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 430) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_MIGHTY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_MIGHTY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 431) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_FAITHFUL.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_FAITHFUL.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 432) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_LUCKY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_LUCKY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 433) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.GAMBLER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.GAMBLER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 434) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_BILLIONAIRE.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_BILLIONAIRE.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 435) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.LAZY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.LAZY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 436) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.RIOTER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.RIOTER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 437) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.CLAN_LEADER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.CLAN_LEADER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 438) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.VOID_KNIGHT.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.VOID_KNIGHT.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 439) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.HIDENSEEKER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.HIDENSEEKER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 440) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.WINGMAN.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.WINGMAN.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 441) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.THE_BOSS.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.THE_BOSS.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 442) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.KING.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.KING.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 443) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.QUEEN.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.QUEEN.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 444) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.PVM_MASTER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.PVM_MASTER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 445) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.DONATOR.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.DONATOR.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 446) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.SUPER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.SUPER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 447) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.SUPREME.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.SUPREME.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 448) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.LEGENDARY.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.LEGENDARY.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 449) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.UBER.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.UBER.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 450) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.NEW_DONATOR_RANK.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.NEW_DONATOR_RANK.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 451) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.LORD.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.LORD.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 452) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.LEGEND.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.LEGEND.getName()
							+ "</col> for 500 LoyaltyPoints.");
				} else if (c.dialogueAction == 453) {
					if (c.getLoyaltyPoints() < 500) {
						c.sendMessage(
								"You need 500 LoyaltyPoints to buy this title.");
						return;
					}
					c.playerTitle = TitleData.EXTREME.getName();
					c.titleColor = 0;
					c.setLoyaltyPoints(c.getLoyaltyPoints() - 500, true);
					c.getPA().closeAllWindows();
					c.dialogueAction = 0;
					c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					c.sendMessage("You purchased the title <col=ff>"
							+ TitleData.EXTREME.getName()
							+ "</col> for 500 LoyaltyPoints.");
				}
				break;

			// remove title button
			case 164026:
				c.playerTitle = "";
				c.titleColor = 0;
				c.sendMessage("Your title will be removed when you logout.");
				c.getPA().closeAllWindows();
				break;

			default:
				c.sendMessage("Add this button in: " + buttonId);
				break;
		}
	}

	public static void completeAchievements(Player c) {
		if (c.getTitleInt(
				TitleData.THE_EXPLORER.ordinal()) < TitleData.THE_EXPLORER
						.getUnlocked()) {
			c.setTitleInt(TitleData.THE_EXPLORER.ordinal(),
					c.getTitleInt(TitleData.THE_EXPLORER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_EXPLORER.ordinal()) == TitleData.THE_EXPLORER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Explorer",
						+TitleData.THE_EXPLORER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_EXPLORER.getName() + " Title ");
			}
		}
	}

	public static void completeQuests(Client c) {
		if (c.getTitleInt(TitleData.THE_WISE.ordinal()) < TitleData.THE_WISE
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_WISE.ordinal(),
					c.getTitleInt(TitleData.THE_WISE.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_WISE.ordinal()) == TitleData.THE_WISE
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Wise",
						+TitleData.THE_WISE.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_WISE.getName() + " Title ");
			}
		}
	}

	public static void completeTwentyDuoSlayer(Client c) {
		if (c.getTitleInt(TitleData.WINGMAN.ordinal()) < TitleData.WINGMAN
				.getUnlocked()) {
			c.setTitleInt(TitleData.WINGMAN.ordinal(),
					c.getTitleInt(TitleData.WINGMAN.ordinal()) + 1);
			if (c.getTitleInt(TitleData.WINGMAN.ordinal()) == TitleData.WINGMAN
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Wingman",
						+TitleData.WINGMAN.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.WINGMAN.getName() + " Title ");
			}
		}
	}

	public static void completionist(Client c) {
		if (c.getTitleInt(TitleData.THE_COMPLETIONIST
				.ordinal()) < TitleData.THE_COMPLETIONIST.getUnlocked()) {
			c.setTitleInt(TitleData.THE_COMPLETIONIST.ordinal(),
					c.getTitleInt(TitleData.THE_COMPLETIONIST.ordinal()) + 1);
			if (c.getTitleInt(TitleData.THE_COMPLETIONIST
					.ordinal()) == TitleData.THE_COMPLETIONIST.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Completionist",
						+TitleData.THE_COMPLETIONIST.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_COMPLETIONIST.getName() + " Title ");
			}
		}
	}

	public static void createAccount(Client c) {
		if (c.getTitleInt(TitleData.THE_REAL.ordinal()) < TitleData.THE_REAL
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_REAL.ordinal(),
					c.getTitleInt(TitleData.THE_REAL.ordinal()) + 1);
			c.getPacketSender().sendFrame126("<col=ff00>The Real",
					+TitleData.THE_REAL.stringId());
			c.sendMessage("<img=6>Congratulation you've unlocked "
					+ TitleData.THE_REAL.getName() + " Title ");
		}
	}

	public static void defeatChampions(Client c) {
		if (c.getTitleInt(TitleData.CHAMPION.ordinal()) < TitleData.CHAMPION
				.getUnlocked()) {
			c.setTitleInt(TitleData.CHAMPION.ordinal(),
					c.getTitleInt(TitleData.CHAMPION.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.CHAMPION.ordinal()) == TitleData.CHAMPION
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Champion",
						+TitleData.CHAMPION.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.CHAMPION.getName() + " Title ");
			}
		}
	}

	public static void defeatFiveKalphite(Client c) {
		if (c.getTitleInt(TitleData.QUEEN.ordinal()) < TitleData.QUEEN
				.getUnlocked()) {
			c.setTitleInt(TitleData.QUEEN.ordinal(),
					c.getTitleInt(TitleData.QUEEN.ordinal()) + 1);
			if (c.getTitleInt(TitleData.QUEEN.ordinal()) == TitleData.QUEEN
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Queen",
						+TitleData.QUEEN.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.QUEEN.getName() + " Title ");
			}
		}
	}

	public static void defeatFiveKBD(Client c) {
		if (c.getTitleInt(TitleData.KING.ordinal()) < TitleData.KING
				.getUnlocked()) {
			c.setTitleInt(TitleData.KING.ordinal(),
					c.getTitleInt(TitleData.KING.ordinal()) + 1);
			if (c.getTitleInt(TitleData.KING.ordinal()) == TitleData.KING
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>King",
						+TitleData.KING.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.KING.getName() + " Title ");
			}
		}
	}

	public static void defeatKiln(Client c) {
		if (c.getTitleInt(TitleData.YT_HAAR.ordinal()) < TitleData.YT_HAAR
				.getUnlocked()) {
			c.setTitleInt(TitleData.YT_HAAR.ordinal(),
					c.getTitleInt(TitleData.YT_HAAR.ordinal()) + 1);
			if (c.getTitleInt(TitleData.YT_HAAR.ordinal()) == TitleData.YT_HAAR
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Yt'Haar",
						+TitleData.YT_HAAR.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.YT_HAAR.getName() + " Title ");
			}
		}
	}

	public static void defeatNex(Client c) {
		if (c.getTitleInt(TitleData.THE_BOSS.ordinal()) < TitleData.THE_BOSS
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_BOSS.ordinal(),
					c.getTitleInt(TitleData.THE_BOSS.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_BOSS.ordinal()) == TitleData.THE_BOSS
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Boss",
						+TitleData.THE_BOSS.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_BOSS.getName() + " Title ");
			}
		}
	}

	public static void defeatPlayerVeng(Client c) {
		if (c.getTitleInt(
				TitleData.THE_AVENGER.ordinal()) < TitleData.THE_AVENGER
						.getUnlocked()) {
			c.setTitleInt(TitleData.THE_AVENGER.ordinal(),
					c.getTitleInt(TitleData.THE_AVENGER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_AVENGER.ordinal()) == TitleData.THE_AVENGER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Avenger",
						+TitleData.THE_AVENGER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_AVENGER.getName() + " Title ");
			}
		}
	}

	public static void diceBag(Client c) {
		if (c.getTitleInt(TitleData.GAMBLER.ordinal()) < TitleData.GAMBLER
				.getUnlocked()) {
			c.setTitleInt(TitleData.GAMBLER.ordinal(),
					c.getTitleInt(TitleData.GAMBLER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.GAMBLER.ordinal()) == TitleData.GAMBLER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Gambler",
						+TitleData.GAMBLER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.GAMBLER.getName() + " Title ");
			}
		}
	}

	public static void dieFiftyTimes(Client c) {
		if (c.getTitleInt(TitleData.SIR_LAME.ordinal()) < TitleData.SIR_LAME
				.getUnlocked()) {
			c.setTitleInt(TitleData.SIR_LAME.ordinal(),
					c.getTitleInt(TitleData.SIR_LAME.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.SIR_LAME.ordinal()) == TitleData.SIR_LAME
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Sir Lame",
						+TitleData.SIR_LAME.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.SIR_LAME.getName() + " Title ");
			}
		}
	}

	public static void dieHundredTimes(Client c) {
		if (c.getTitleInt(TitleData.GRUMPY.ordinal()) < TitleData.GRUMPY
				.getUnlocked()) {
			c.setTitleInt(TitleData.GRUMPY.ordinal(),
					c.getTitleInt(TitleData.GRUMPY.ordinal()) + 1);
			if (c.getTitleInt(TitleData.GRUMPY.ordinal()) == TitleData.GRUMPY
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Grumpy",
						+TitleData.GRUMPY.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.GRUMPY.getName() + " Title ");
			}
		}
	}

	public static void dieOnce(Client c) {
		if (c.getTitleInt(TitleData.WEAKLING.ordinal()) < TitleData.WEAKLING
				.getUnlocked()) {
			c.setTitleInt(TitleData.WEAKLING.ordinal(),
					c.getTitleInt(TitleData.WEAKLING.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.WEAKLING.ordinal()) == TitleData.WEAKLING
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Weakling",
						+TitleData.WEAKLING.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.WEAKLING.getName() + " Title ");
			}
		}
	}

	public static void dieTenTimes(Client c) {
		if (c.getTitleInt(TitleData.COWARDLY.ordinal()) < TitleData.COWARDLY
				.getUnlocked()) {
			c.setTitleInt(TitleData.COWARDLY.ordinal(),
					c.getTitleInt(TitleData.COWARDLY.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.COWARDLY.ordinal()) == TitleData.COWARDLY
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Cowardly",
						+TitleData.COWARDLY.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.COWARDLY.getName() + " Title ");
			}
		}
	}

	public static void donate10(Player c) {
		if (c.getTitleInt(TitleData.DONATOR.ordinal()) < TitleData.DONATOR
				.getUnlocked()) {
			c.setTitleInt(TitleData.DONATOR.ordinal(),
					c.getTitleInt(TitleData.DONATOR.ordinal()) + 1);
			if (c.getTitleInt(TitleData.DONATOR.ordinal()) == TitleData.DONATOR
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Donator",
						+TitleData.DONATOR.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.DONATOR.getName() + " Title ");
			}
		}
	}

	public static void donate1000(Player c) {
		if (c.getTitleInt(TitleData.LEGENDARY.ordinal()) < TitleData.LEGENDARY
				.getUnlocked()) {
			c.setTitleInt(TitleData.LEGENDARY.ordinal(),
					c.getTitleInt(TitleData.LEGENDARY.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.LEGENDARY.ordinal()) == TitleData.LEGENDARY
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Legendary",
						+TitleData.LEGENDARY.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.LEGENDARY.getName() + " Title ");
			}
		}
	}

	public static void donate250(Player c) {
		if (c.getTitleInt(TitleData.SUPREME.ordinal()) < TitleData.SUPREME
				.getUnlocked()) {
			c.setTitleInt(TitleData.SUPREME.ordinal(),
					c.getTitleInt(TitleData.SUPREME.ordinal()) + 1);
			if (c.getTitleInt(TitleData.SUPREME.ordinal()) == TitleData.SUPREME
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Supreme",
						+TitleData.SUPREME.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.SUPREME.getName() + " Title ");
			}
		}
	}

	public static void donate50(Player c) {
		if (c.getTitleInt(TitleData.SUPER.ordinal()) < TitleData.SUPER
				.getUnlocked()) {
			c.setTitleInt(TitleData.SUPER.ordinal(),
					c.getTitleInt(TitleData.SUPER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.SUPER.ordinal()) == TitleData.SUPER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Super",
						+TitleData.SUPER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.SUPER.getName() + " Title ");
			}
		}
	}

	public static void donate5000(Player c) {
		if (c.getTitleInt(TitleData.UBER.ordinal()) < TitleData.UBER
				.getUnlocked()) {
			c.setTitleInt(TitleData.UBER.ordinal(),
					c.getTitleInt(TitleData.UBER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.UBER.ordinal()) == TitleData.UBER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Uber",
						+TitleData.UBER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.UBER.getName() + " Title ");
			}
		}
	}
	
	public static void donate10000(Client c) {
		if (c.getTitleInt(TitleData.NEW_DONATOR_RANK.ordinal()) < TitleData.NEW_DONATOR_RANK
				.getUnlocked()) {
			c.setTitleInt(TitleData.NEW_DONATOR_RANK.ordinal(),
					c.getTitleInt(TitleData.NEW_DONATOR_RANK.ordinal()) + 1);
			if (c.getTitleInt(TitleData.NEW_DONATOR_RANK.ordinal()) == TitleData.NEW_DONATOR_RANK
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>New donator rank",
						+TitleData.NEW_DONATOR_RANK.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.NEW_DONATOR_RANK.getName() + " Title ");
			}
		}
	}

	public static void findAllPenguins(Client c) {
		if (c.getTitleInt(
				TitleData.HIDENSEEKER.ordinal()) < TitleData.HIDENSEEKER
						.getUnlocked()) {
			c.setTitleInt(TitleData.HIDENSEEKER.ordinal(),
					c.getTitleInt(TitleData.HIDENSEEKER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.HIDENSEEKER.ordinal()) == TitleData.HIDENSEEKER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>HideNSeeker",
						+TitleData.HIDENSEEKER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.HIDENSEEKER.getName() + " Title ");
			}
		}
	}

	public static void ironMan(Player c) {
		if (c.getTitleInt(TitleData.IRONMAN.ordinal()) < TitleData.IRONMAN
				.getUnlocked()) {
			c.setTitleInt(TitleData.IRONMAN.ordinal(),
					c.getTitleInt(TitleData.IRONMAN.ordinal()) + 1);
			if (c.getTitleInt(TitleData.IRONMAN.ordinal()) == TitleData.IRONMAN
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Ironman",
						+TitleData.IRONMAN.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.IRONMAN.getName() + " Title ");
			}
		}
	}

	public static void lockIronMan(Client c) {
		c.setTitleInt(TitleData.IRONMAN.ordinal(), 0);
		c.getPacketSender().sendFrame126("<col=ff0000>Ironman",
				+TitleData.IRONMAN.stringId());
	}

	public static void kill2000Monsters(Client c) {
		if (c.getTitleInt(TitleData.PVM_MASTER.ordinal()) < TitleData.PVM_MASTER
				.getUnlocked()) {
			c.setTitleInt(TitleData.PVM_MASTER.ordinal(),
					c.getTitleInt(TitleData.PVM_MASTER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.PVM_MASTER.ordinal()) == TitleData.PVM_MASTER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>PVM Master",
						+TitleData.PVM_MASTER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.PVM_MASTER.getName() + " Title ");
			}
		}
	}

	public static void killFiftyPlayers(Client c) {
		if (c.getTitleInt(TitleData.PKER.ordinal()) < TitleData.PKER
				.getUnlocked()) {
			c.setTitleInt(TitleData.PKER.ordinal(),
					c.getTitleInt(TitleData.PKER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.PKER.ordinal()) == TitleData.PKER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>PKer",
						+TitleData.PKER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.PKER.getName() + " Title ");
			}
		}
	}

	public static void killHundredPlayers(Client c) {
		if (c.getTitleInt(TitleData.CHEERFUL.ordinal()) < TitleData.CHEERFUL
				.getUnlocked()) {
			c.setTitleInt(TitleData.CHEERFUL.ordinal(),
					c.getTitleInt(TitleData.CHEERFUL.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.CHEERFUL.ordinal()) == TitleData.CHEERFUL
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Cheerful",
						+TitleData.CHEERFUL.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.CHEERFUL.getName() + " Title ");
			}
		}
	}

	public static void killTenPlayers(Client c) {
		if (c.getTitleInt(TitleData.OVERLORD.ordinal()) < TitleData.OVERLORD
				.getUnlocked()) {
			c.setTitleInt(TitleData.OVERLORD.ordinal(),
					c.getTitleInt(TitleData.OVERLORD.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.OVERLORD.ordinal()) == TitleData.OVERLORD
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Overlord",
						+TitleData.OVERLORD.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.OVERLORD.getName() + " Title ");
			}
		}
	}

	public static void killThousendPlayers(Client c) {
		if (c.getTitleInt(TitleData.PK_MASTER.ordinal()) < TitleData.PK_MASTER
				.getUnlocked()) {
			c.setTitleInt(TitleData.PK_MASTER.ordinal(),
					c.getTitleInt(TitleData.PK_MASTER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.PK_MASTER.ordinal()) == TitleData.PK_MASTER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>PK Master",
						+TitleData.PK_MASTER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.PK_MASTER.getName() + " Title ");
			}
		}
	}

	public static void killTwoHundredPlayers(Client c) {
		if (c.getTitleInt(TitleData.WAR_CHIEF.ordinal()) < TitleData.WAR_CHIEF
				.getUnlocked()) {
			c.setTitleInt(TitleData.WAR_CHIEF.ordinal(),
					c.getTitleInt(TitleData.WAR_CHIEF.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.WAR_CHIEF.ordinal()) == TitleData.WAR_CHIEF
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>War-Chief",
						+TitleData.WAR_CHIEF.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.WAR_CHIEF.getName() + " Title ");
			}
		}
	}

	public static void lazyPestControl(Client c) {
		if (c.getTitleInt(TitleData.LAZY.ordinal()) < TitleData.LAZY
				.getUnlocked()) {
			c.setTitleInt(TitleData.LAZY.ordinal(),
					c.getTitleInt(TitleData.LAZY.ordinal()) + 1);
			if (c.getTitleInt(TitleData.LAZY.ordinal()) == TitleData.LAZY
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Lazy",
						+TitleData.LAZY.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.LAZY.getName() + " Title ");
			}
		}
	}

	public static void leaderWinClanWars(Client c) {
		if (c.getTitleInt(
				TitleData.CLAN_LEADER.ordinal()) < TitleData.CLAN_LEADER
						.getUnlocked()) {
			c.setTitleInt(TitleData.CLAN_LEADER.ordinal(),
					c.getTitleInt(TitleData.CLAN_LEADER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.CLAN_LEADER.ordinal()) == TitleData.CLAN_LEADER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Clan Leader",
						+TitleData.CLAN_LEADER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.CLAN_LEADER.getName() + " Title ");
			}
		}
	}

	public static void maxExpMage(Client c) {
		if (c.getTitleInt(TitleData.MAGICIAN.ordinal()) < TitleData.MAGICIAN
				.getUnlocked()) {
			c.setTitleInt(TitleData.MAGICIAN.ordinal(),
					c.getTitleInt(TitleData.MAGICIAN.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.MAGICIAN.ordinal()) == TitleData.MAGICIAN
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Magician",
						+TitleData.MAGICIAN.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.MAGICIAN.getName() + " Title ");
			}
		}
	}

	public static void maxExpMelee(Client c) {
		if (c.getTitleInt(TitleData.WARRIOR.ordinal()) < TitleData.WARRIOR
				.getUnlocked()) {
			c.setTitleInt(TitleData.WARRIOR.ordinal(),
					c.getTitleInt(TitleData.WARRIOR.ordinal()) + 1);
			if (c.getTitleInt(TitleData.WARRIOR.ordinal()) == TitleData.WARRIOR
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Warrior",
						+TitleData.WARRIOR.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.WARRIOR.getName() + " Title ");
			}
		}
	}

	public static void maxExpRange(Client c) {
		if (c.getTitleInt(TitleData.ARCHER.ordinal()) < TitleData.ARCHER
				.getUnlocked()) {
			c.setTitleInt(TitleData.ARCHER.ordinal(),
					c.getTitleInt(TitleData.ARCHER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.ARCHER.ordinal()) == TitleData.ARCHER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Archer",
						+TitleData.ARCHER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.ARCHER.getName() + " Title ");
			}
		}
	}

	public static void maxExpStat(Client c) {
		if (c.getTitleInt(TitleData.MASTER.ordinal()) < TitleData.MASTER
				.getUnlocked()) {
			c.setTitleInt(TitleData.MASTER.ordinal(),
					c.getTitleInt(TitleData.MASTER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.MASTER.ordinal()) == TitleData.MASTER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Master",
						+TitleData.MASTER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.MASTER.getName() + " Title ");
			}
		}
	}

	public static void maximum(Client c) {
		if (c.getTitleInt(TitleData.MAXIMUM.ordinal()) < TitleData.MAXIMUM
				.getUnlocked()) {
			c.setTitleInt(TitleData.MAXIMUM.ordinal(),
					c.getTitleInt(TitleData.MAXIMUM.ordinal()) + 1);
			if (c.getTitleInt(TitleData.MAXIMUM.ordinal()) == TitleData.MAXIMUM
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Maximum",
						+TitleData.MAXIMUM.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.MAXIMUM.getName() + " Title ");
			}
		}
	}

	public static void maxIronMan(Client c) {
		if (c.getTitleInt(TitleData.THE_MIGHTY.ordinal()) < TitleData.THE_MIGHTY
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_MIGHTY.ordinal(),
					c.getTitleInt(TitleData.THE_MIGHTY.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_MIGHTY.ordinal()) == TitleData.THE_MIGHTY
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Mighty",
						+TitleData.THE_MIGHTY.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_MIGHTY.getName() + " Title ");
			}
		}
	}

	public static void playedFourDays(Client c) {
		if (c.getTitleInt(TitleData.THE_LOYAL.ordinal()) < TitleData.THE_LOYAL
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_LOYAL.ordinal(),
					c.getTitleInt(TitleData.THE_LOYAL.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_LOYAL.ordinal()) == TitleData.THE_LOYAL
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Loyal",
						+TitleData.THE_LOYAL.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_LOYAL.getName() + " Title ");
			}
		}
	}

	public static void playedTenDays(Client c) {
		if (c.getTitleInt(
				TitleData.THE_DEVOTED.ordinal()) < TitleData.THE_DEVOTED
						.getUnlocked()) {
			c.setTitleInt(TitleData.THE_DEVOTED.ordinal(),
					c.getTitleInt(TitleData.THE_DEVOTED.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_DEVOTED.ordinal()) == TitleData.THE_DEVOTED
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Devoted",
						+TitleData.THE_DEVOTED.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_DEVOTED.getName() + " Title ");
			}
		}
	}

	public static void playedTwentyDays(Client c) {
		if (c.getTitleInt(TitleData.THE_FAMOUS.ordinal()) < TitleData.THE_FAMOUS
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_FAMOUS.ordinal(),
					c.getTitleInt(TitleData.THE_FAMOUS.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_FAMOUS.ordinal()) == TitleData.THE_FAMOUS
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Famous",
						+TitleData.THE_FAMOUS.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_FAMOUS.getName() + " Title ");
			}
		}
	}

	public static void skiller(Client c) {
		if (c.getTitleInt(TitleData.SKILLER.ordinal()) < TitleData.SKILLER
				.getUnlocked()) {
			c.setTitleInt(TitleData.SKILLER.ordinal(),
					c.getTitleInt(TitleData.SKILLER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.SKILLER.ordinal()) == TitleData.SKILLER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Skiller",
						+TitleData.SKILLER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.SKILLER.getName() + " Title ");
			}
		}
	}

	public static void tenBulgingBags(Client c) {
		if (c.getTitleInt(TitleData.THE_BILLIONAIRE.ordinal()) < TitleData.THE_BILLIONAIRE.getUnlocked()) {
			c.setTitleInt(TitleData.THE_BILLIONAIRE.ordinal(),
					c.getTitleInt(TitleData.THE_BILLIONAIRE.ordinal()) + 1);
			if (c.getTitleInt(TitleData.THE_BILLIONAIRE
					.ordinal()) == TitleData.THE_BILLIONAIRE.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Billionaire",
						+TitleData.THE_BILLIONAIRE.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_BILLIONAIRE.getName() + " Title ");
			}
		}
	}

	public static void throwMillions(Player c) {
		if (c.getTitleInt(
				TitleData.THE_WEALTHY.ordinal()) < TitleData.THE_WEALTHY
						.getUnlocked()) {
			c.setTitleInt(TitleData.THE_WEALTHY.ordinal(),
					c.getTitleInt(TitleData.THE_WEALTHY.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_WEALTHY.ordinal()) == TitleData.THE_WEALTHY
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Wealthy",
						+TitleData.THE_WEALTHY.stringId());
				c.sendMessage("<img=6>Congratulations you've unlocked "
						+ TitleData.THE_WEALTHY.getName() + " Title ");
			}
		}
	}

	public static void unlockTitles(Client c) {
		for (int i = 0; i < TitleData.values.length; i++) {
			TitleData data = TitleData.values[i];
			c.getPacketSender().sendFrame126((c.titles[data.ordinal()] == data.getUnlocked() ? "<col=ff00>" : "<col=ff0000>") + titles[data.ordinal()], data.stringId());
		}
	}

	public static void winClanWars(Client c) {
		if (c.getTitleInt(TitleData.RIOTER.ordinal()) < TitleData.RIOTER
				.getUnlocked()) {
			c.setTitleInt(TitleData.RIOTER.ordinal(),
					c.getTitleInt(TitleData.RIOTER.ordinal()) + 1);
			if (c.getTitleInt(TitleData.RIOTER.ordinal()) == TitleData.RIOTER
					.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Rioter",
						+TitleData.RIOTER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.RIOTER.getName() + " Title ");
			}
		}
	}

	public static void winFiftyPestControl(Client c) {
		if (c.getTitleInt(
				TitleData.VOID_KNIGHT.ordinal()) < TitleData.VOID_KNIGHT
						.getUnlocked()) {
			c.setTitleInt(TitleData.VOID_KNIGHT.ordinal(),
					c.getTitleInt(TitleData.VOID_KNIGHT.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.VOID_KNIGHT.ordinal()) == TitleData.VOID_KNIGHT
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Void Knight",
						+TitleData.VOID_KNIGHT.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.VOID_KNIGHT.getName() + " Title ");
			}
		}
	}

	public static void winHundredDuels(Client c) {
		if (c.getTitleInt(
				TitleData.DUEL_MASTER.ordinal()) < TitleData.DUEL_MASTER
						.getUnlocked()) {
			c.setTitleInt(TitleData.DUEL_MASTER.ordinal(),
					c.getTitleInt(TitleData.DUEL_MASTER.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.DUEL_MASTER.ordinal()) == TitleData.DUEL_MASTER
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Duel Master",
						+TitleData.DUEL_MASTER.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.DUEL_MASTER.getName() + " Title ");
			}
		}
	}

	public static void winLottery(Client c) {
		if (c.getTitleInt(TitleData.THE_LUCKY.ordinal()) < TitleData.THE_LUCKY
				.getUnlocked()) {
			c.setTitleInt(TitleData.THE_LUCKY.ordinal(),
					c.getTitleInt(TitleData.THE_LUCKY.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.THE_LUCKY.ordinal()) == TitleData.THE_LUCKY
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>The Lucky",
						+TitleData.THE_LUCKY.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.THE_LUCKY.getName() + " Title ");
			}
		}
	}
	
	public static void upgradeLord(Client c) {
		if (c.getTitleInt(TitleData.LORD.ordinal()) < TitleData.LORD
				.getUnlocked()) {
			c.setTitleInt(TitleData.LORD.ordinal(),
					c.getTitleInt(TitleData.LORD.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.LORD.ordinal()) == TitleData.LORD
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Lord",
						+TitleData.LORD.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.LORD.getName() + " Title ");
			}
		}
	}
	
	public static void upgradeLegend(Client c) {
		if (c.getTitleInt(TitleData.LEGEND.ordinal()) < TitleData.LEGEND
				.getUnlocked()) {
			c.setTitleInt(TitleData.LEGEND.ordinal(),
					c.getTitleInt(TitleData.LEGEND.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.LEGEND.ordinal()) == TitleData.LEGEND
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Legend",
						+TitleData.LEGEND.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.LEGEND.getName() + " Title ");
			}
		}
	}
	
	public static void upgradeExtreme(Client c) {
		if (c.getTitleInt(TitleData.EXTREME.ordinal()) < TitleData.EXTREME
				.getUnlocked()) {
			c.setTitleInt(TitleData.EXTREME.ordinal(),
					c.getTitleInt(TitleData.EXTREME.ordinal()) + 1);
			if (c.getTitleInt(
					TitleData.EXTREME.ordinal()) == TitleData.EXTREME
							.getUnlocked()) {
				c.getPacketSender().sendFrame126("<col=ff00>Extreme",
						+TitleData.EXTREME.stringId());
				c.sendMessage("<img=6>Congratulation you've unlocked "
						+ TitleData.EXTREME.getName() + " Title ");
			}
		}
	}

}
