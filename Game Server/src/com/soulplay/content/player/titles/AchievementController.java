package com.soulplay.content.player.titles;

public abstract class AchievementController {

	private Achievement achievement;

	private AchievementModel model;

	public AchievementController(AchievementModel model,
			Achievement achievement) {
		this.addAchievement(achievement);
		this.addModel(model);
		this.complete();
	}

	public void addAchievement(Achievement achievement) {
		this.achievement = achievement;
	}

	public void addModel(AchievementModel model) {
		this.model = model;
	}

	public void complete() {

	}

	public Achievement getAchievement() {
		return achievement;
	}

	public AchievementModel getModel() {
		return model;
	}
}
