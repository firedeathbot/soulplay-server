package com.soulplay.content.player.quests.tamingquest;

import com.soulplay.Server;
import com.soulplay.content.player.pets.pokemon.Pokemon;
import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.entity.Location;

public class TamingQuestHandler {
	
	public static final int TAMING_MASTER = 1281;
	public static final int TAMING_GOBLIN = 12360;
	private static final int TAMING_ROPE = 19665;
	private static final int WHITE_PORTAL = 11356;
	private static final int PET_DUNGEON_STAIRS = 31417;
	
	public static final Location OUTSIDE_PET_PORTAL = Location.create(3092, 3510);
	
	public static void startTamingQuest(Client c, NPC n) {
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 6) {
			c.getDialogueBuilder().sendNpcChat(n.npcType, "I can make more taming ropes for you", "for the right price.").sendOption("Buy 5 for 2.5 Million Coins", () -> {
				if (c.getPA().freeSlots() > 5) {
					if (c.getItems().takeCoins(2_500_000)) {
						c.getItems().addItem(19667, 5);
					} else {
						c.sendMessage("You don't have enough coins.");
					}
				} else {
					c.sendMessage("Not enough space in your inventory.");
				}
			}, "Buy 1 for 750k Coins", () -> {
				if (c.getPA().freeSlots() > 1) {
					if (c.getItems().takeCoins(750_000)) {
						c.getItems().addItem(19667, 1);
					} else {
						c.sendMessage("You don't have enough coins.");
					}
				} else {
					c.sendMessage("Not enough space in your inventory.");
				}
			}, "I need another bolt pouch.", () -> {
				if (c.getPA().freeSlots() > 1) {
					c.getItems().addItem(9433, 1);
				} else {
					c.sendMessage("Not enough space in your inventory.");
				}
			},"No, thank you!", () -> {
			}).execute();
		}
		
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 5) {
			if (c.getItems().playerHasItem(15270, 10)) {
				c.getItems().deleteItem2(21520, 2);
				PokemonButtons.deSpawn(c);
				QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
				c.getItems().addItem(9433, 1);
				c.getItems().addItem(19667, 2);
				petQuestReward(c);
			} else
			    c.getDialogueBuilder().sendNpcChat(n.npcType, "I need you to bring me 10 raw rocktails").execute();
		}
		
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 4) {
			c.getDialogueBuilder().sendNpcChat(n.npcType, "You managed to train the goblin to level 2.", "These type of pets are special and will eat nothing", "else besides raw rocktails and Fury sharks.").
			sendNpcChat(n.npcType, "I need you to bring me 10 raw rocktails.").execute();
			QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
		}
	
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 3) {
			if (c.summoned == null) {
				c.getDialogueBuilder().sendNpcChat(n.npcType, "I have found the goblin for you.").execute();
				c.sendMessage("You have a funny feeling you're being followed");
				spawnGoblinPet(c);
				return;
			}
			c.getDialogueBuilder().sendNpcChat(n.npcType, "You can train the goblin anywhere, ", "the goblin receives exp when assisting you in fights.")
					.sendNpcChat(n.npcType, "If you somehow lose the goblin, you",
							"can come back and i will help you find it.").execute();
		}
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 2) {
			if (c.summoned != null) {
				c.getDialogueBuilder().sendNpcChat(n.npcType, "You can't have a pet or familiars for the next step!").execute();
				return;
			}
			spawnGoblinPet(c);
			c.sendMessage("You have a funny feeling you're being followed");
			c.getDialogueBuilder().sendNpcChat(n.npcType, "VERY GOOD you managed to catch the goblin,", " now i need you to train the goblin to level 2.").execute();
			QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
		}
		
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 1) {
			c.getDialogueBuilder().sendNpcChat(n.npcType,
					"There should be a goblin on top of the lumbridge gate,", 
					"fight him until he is weak enough to tame him", "with the taming rope.").execute();
			if (!c.getItems().playerHasItem(TAMING_ROPE)) {
				c.getItems().addItem(TAMING_ROPE, 1);
			}
		}
		
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 0) {
			c.getDialogueBuilder().sendNpcChat(n.npcType, "Would you like to learn how to catch monsters?")
					.sendOption("Yes, of course!", () -> {
						QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
						if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 1) {
							c.getDialogueBuilder().sendNpcChat(n.npcType,
									"There should be a goblin on top of the lumbridge gate,", 
									"fight him until he is weak enough to tame him", "with the taming rope.");
							if (!c.getItems().playerHasItem(TAMING_ROPE)) {
								c.getItems().addItem(TAMING_ROPE, 1);
							}
						}
					}, "No thanks.", () -> {
						c.getPA().closeAllWindows();
					}).execute();
		}
	}
	
	public static void spawnGoblinAfterMovingUpstairs(Client c, Location o) {
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 1 && ((o.getX() == 3229 && o.getY() == 3224) || (o.getX() == 3229 && o.getY() == 3213))) {
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				
				@Override
				public void stop() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();

					spawnGoblin(c);
				}
			}, 2);
		}
	}

	public static void spawnGoblin(Client c) {
		if (c.summoned != null) {
			c.sendMessage("You can't advance the quest if you have familiar with you.");
			return;
		}

		for (NPC n : Server.getRegionManager().getLocalNpcs(c)) {
			if (n.spawnedBy == c.getId()) {
				c.sendMessage("You already have a goblin spawned.");
				return;
			}
		}

		NPCHandler.spawnNpc(c, TAMING_GOBLIN, 3228, 3218, 2, 0, false, true);
	}
	
	public static void questCaptureGoblin(Client c, NPC n) {
		
		if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) == 1) {
			if (n.canThrowPokeball()) {
				if (c.getStopWatch().checkThenStart(10)) {

					CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

						int timer = 6;

						@Override
						public void execute(CycleEventContainer container) {

							if (c.disconnected || c == null || n == null || n.isDead()) {
								container.stop();
								return;
							}

							if (timer == 6) {
								c.face(n);
							}

							if (timer == 5) {
								c.startAnimation(2910);
								c.getMovement().resetWalkingQueue();
							}

							if (timer == 2) {
								c.startAnimation(-1);
								c.getMovement().resetWalkingQueue();
							}

							timer--;
							if (timer == 0) {
								container.stop();
								QuestHandlerTemp.incQuestProgress(c, QuestIndex.POKEMON_ADVENTURES);
								c.sendMessage("<col=ff0000>You succesfully captured the goblin!");
							}
						}

						@Override
						public void stop() {
							c.performingAction = false;
						}
					}, 1);
				}
			}
		} else
			c.sendMessage("You have already caught the goblin, head back to Taming master.");
	}
	
	public static boolean handleTamingQuestObjFirstClick(Client c, int objectId) {

		switch (objectId) {

		case WHITE_PORTAL:
			if (QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) >= 3 && QuestHandlerTemp.getQuestProgress(c, QuestIndex.POKEMON_ADVENTURES) <= 5) {
				if (c.summoned != null) {
					c.sendMessage("You can't have a pet or familiars with you in here.");
					return true;
				}
				c.getPA().movePlayer(3276, 4368, 12);
				spawnGoblinPet(c);
				return true;
			} else if (QuestHandlerTemp.completedQuest(c, QuestIndex.POKEMON_ADVENTURES)) {
				whitePortalDialogue(c);
				return true;
			} else
				c.getDialogueBuilder().sendNpcChat(TAMING_MASTER, "You must complete the Mastering Enhancement Quest ", "to enter this portal.").execute();
			return true;
			
		case PET_DUNGEON_STAIRS:
			c.getPA().movePlayer(OUTSIDE_PET_PORTAL);
			return true;
		}
		return false;
	}
	
	private static void whitePortalDialogue(Client c) {

		c.getDialogueBuilder().sendNpcChat(TAMING_MASTER, "Be careful in there!")
				.sendOption("Floor one", () -> {
					c.getPA().movePlayer(3276, 4368, 0);
					c.getPA().closeAllWindows();
				}, "Floor two", () -> {
					c.getPA().movePlayer(3276, 4368, 4);
					c.getPA().closeAllWindows();
				}, "Floor three", () -> {
					c.getPA().movePlayer(3276, 4368, 8);
					c.getPA().closeAllWindows();
				}).execute();
	}
	
	public static void spawnGoblinPet(Client c) {
		int hp = PokemonData.TUTORIAL_GOBLIN.baseStats.hp;
		c.summoned = NPCHandler.summonPokemon(c, TAMING_GOBLIN,
				c.getCurrentLocation().getX(), c.getCurrentLocation().getY(),
				c.getHeightLevel(), hp, hp);
		c.summoned.summoner = c;
		c.summonedPokemon = new Pokemon(PokemonData.TUTORIAL_GOBLIN, PokemonData.TUTORIAL_GOBLIN.baseStats);
				//3276, 4368, 300);
		c.summoned.face(c);
	}

//	private static final String RED = "<col=960000>";
//	private static final String BLUE = "<col=000096>";
	
	public static final String[][] INSTRUCTIONS = {
			{
				QuestHandlerTemp.BLUE + "I can start this quest by speaking to the " + QuestHandlerTemp.RED + "Taming Master",
				QuestHandlerTemp.BLUE + "In the " + QuestHandlerTemp.RED + "House " + QuestHandlerTemp.BLUE + " North of " + QuestHandlerTemp.RED + "Edgeville Bank" 
			},
			{
				QuestHandlerTemp.BLUE + "I need to find and tame the " + QuestHandlerTemp.RED + "Goblin " + QuestHandlerTemp.BLUE + "that is hiding on",
				QuestHandlerTemp.RED + "Top of the Lumbridge gate. " + QuestHandlerTemp.BLUE + "To do this I need to use a",
				QuestHandlerTemp.RED + "Training rope " + QuestHandlerTemp.BLUE + "when he is weak." },
			{
				QuestHandlerTemp.BLUE + "Head back to the " + QuestHandlerTemp.RED + "Taming Master."	
			},
			{
				QuestHandlerTemp.BLUE + "I need to find a good spot with low level monsters",
				QuestHandlerTemp.BLUE + "and train the goblin to level 2, then head back",
				QuestHandlerTemp.BLUE + "to the "+ QuestHandlerTemp.RED + "Taming Master."
			},
			{
				QuestHandlerTemp.BLUE + "Head back to the " + QuestHandlerTemp.RED + "Taming Master."
			},
			{
				QuestHandlerTemp.BLUE + "I need to get two raw " + QuestHandlerTemp.RED+ "Rocktails " + QuestHandlerTemp.BLUE + "for the " + QuestHandlerTemp.RED + "Taming Master.",
				QuestHandlerTemp.BLUE + "They can only be cought in certain " + QuestHandlerTemp.RED + "fishing " + QuestHandlerTemp.BLUE + "spots"
			}
	};
	
	public static final int QUEST_POINTS_REWARD = 0;
	
	public static void petQuestReward(Client c) {
		c.increaseQuestPoints(QUEST_POINTS_REWARD);
		c.getPacketSender().showInterface(297);
		c.getPacketSender().sendFrame126("Mastering Enhancement Quest Completed!", 301);
		c.getPacketSender().sendFrame126("Bolt Pouch", 4444);
		c.getPacketSender().sendFrame126(Integer.toString(Variables.QUEST_POINTS.getValue(c)), 304);
		
	}


}
