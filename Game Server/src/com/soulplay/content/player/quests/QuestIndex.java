package com.soulplay.content.player.quests;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.content.player.quests.cooksassistant.CooksAssistant;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.game.model.player.Player;

public enum QuestIndex {
	//enums should match clients order of quest, index can be anything, index is just for playersave progress
	COOKS_ASSISTANT(0, 0x10, "Cook's Assistant", null) {
		@Override
		public void generateInstructions(Player p) {
			CooksAssistant.generateInstruction(p);
		}
	},
	POKEMON_ADVENTURES(1, 6, "Mastering Enhancement", TamingQuestHandler.INSTRUCTIONS);

	private final int completedCount;
	private final int index;
	private final String title;
	private final String[][] msg;
	
	private QuestIndex(int index, int completed, String title, String msg[][]) {
		this.completedCount = completed;
		this.index = index;
		this.title = title;
		this.msg = msg;
	}
	
	public void generateInstructions(Player p) {
	}
	
	public int getIndex() {
		return index;
	}

	public int getCompletedCount() {
		return completedCount;
	}

	public String getTitle() {
		return title;
	}

	public String[][] getMsg() {
		return msg;
	}
	
	public int getProgress(Player p) {
		return p.quests[getIndex()];
	}
	
	public boolean hasCompletedQuest(Player p) {
		return p.quests[getIndex()] >= getCompletedCount();
	}
	
	public void setProgress(Player p, int value) {
		p.quests[getIndex()] = value;
	}
	
	public void setCompletedBit(Player p, int mask) {
		p.quests[getIndex()] = p.quests[getIndex()] | mask;
		QuestHandlerTemp.updateQuest(p, this);
	}
	
	public boolean hasCompletedSection(Player p, int mask) {
		return (p.quests[getIndex()] & mask) == mask;
	}
	
	public static final QuestIndex[] VALUES = QuestIndex.values();
	
	public static boolean checkDuplicates() {
		Set<Integer> list = new HashSet<>();
		
		for (QuestIndex quest : QuestIndex.values()) {
			if (list.contains(quest.getIndex()))
				return true;
			list.add(quest.getIndex());
		}
		list.clear();
		list = null;
		return false;
	}
	
}
