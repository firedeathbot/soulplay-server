package com.soulplay.content.player.quests.cooksassistant;

import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;

import plugin.object.MillPlugin;
import plugin.object.lumbridge.DairyCowPlugin;

public class CooksAssistant {

	private static final String BLUE = QuestHandlerTemp.BLUE;
	private static final String RED = QuestHandlerTemp.RED;
	private static final String RED_L = QuestHandlerTemp.RED_LIGHT;
	
	public static final int STARTED = 0x1;
	public static final int GATHERED_MILK = 0x2;
	public static final int GATHERED_FLOUR = 0x4;
	public static final int GATHERED_EGG = 0x8;
	public static final int COMPLETED = 0x10;
	
	public static final int EGG_ID = 1944;
	
	private static final int QUEST_POINTS_REWARD = 1;
	private static final int COOK_EXP_REWARD = 300;
	

	private static final String[] COMPLETED_STRINGS = {
			"<str>It was the Duke of Lumbridge's birthday, but his cook had",
			"<str>Iforgotten to buy the ingredients he needed to make him a",
			"<str>Icake. I brought the cook an egg, some flour and some milk",
			"<str>Iand the cook made a delicious looking cake with them.",
			"",
			"<str>IAs a reward he now lets me use his high quality range",
			"<str>Iwhich lets me burn things less whenever I wish to cook",
			"<str>Ithere.",
			"",
			RED_L+"QUEST COMPLETE!",
	};
	
	private static final String[] NOT_STARTED = { BLUE+"I can start this quest by speaking to the "+RED+"Cook"+BLUE+" in the",
			RED+"Kitchen"+BLUE+" on the ground floor of "+RED+"Lumbridge Castle" };
	
	private static final String[] IN_PROGRESS_START = {
			BLUE+"It's the "+RED+"Duke of Lumbridge's"+BLUE+" birthday and I have to help",
			BLUE+"his "+RED+"Cook"+BLUE+" make him a "+RED+"birthday cake"+BLUE+". To do this I need to",
			BLUE+"bring him the following ingredients:"
	};
	
	private static final String[] MILK_STR = {
			//not in inv
			BLUE+"I need to find a "+RED+"bucket of milk"+BLUE+".",
			//in inv
			BLUE+"I have found a "+RED+"bucket of milk"+BLUE+" to give to the cook.",
			//gave to cook
			"<str>I have given the cook a bucket of milk."
	};
	
	private static final String[] FLOUR_STR = {
			//not in inv
			BLUE+"I need to find a "+RED+"pot of flour"+BLUE+".",
			//in inv
			BLUE+"I have found a "+RED+"pot of flour"+BLUE+" to give to the cook.",
			//gave to cook
			"<str>I have given the cook a pot of flour."
	};
	
	private static final String[] EGG_STR = {
			//not in inv
			BLUE+"I need to find an "+RED+"egg"+BLUE+".",
			//in inv
			BLUE+"I have found an "+RED+"egg"+BLUE+" to give to the cook.",
			//gave to cook
			"<str>I have given the cook an egg."
	};
	
	public static void generateInstruction(Player p) {
		int currentValue = QuestIndex.COOKS_ASSISTANT.getProgress(p);
		if (currentValue == 0) {
			QuestHandlerTemp.sendQuestList(p, NOT_STARTED);
		} else if ((currentValue & COMPLETED) == COMPLETED) {
			//completed interface
			QuestHandlerTemp.sendQuestList(p, COMPLETED_STRINGS);
		} else {
			//IN PROGRESS
			boolean finishedMilk = (currentValue & GATHERED_MILK) == GATHERED_MILK;
			boolean finishedFlour = (currentValue & GATHERED_FLOUR) == GATHERED_FLOUR;
			boolean finishedEggs = (currentValue & GATHERED_EGG) == GATHERED_EGG;
			
			QuestHandlerTemp.sendQuestList(p, IN_PROGRESS_START);
			
			if (finishedMilk)
				p.getPacketSender().sendFrame126b(MILK_STR[2], 8149);
			else
				p.getPacketSender().sendFrame126b(MILK_STR[p.getItems().playerHasItem(DairyCowPlugin.BUCKET_OF_MILK) ? 1 : 0], 8149);
			
			if (finishedFlour)
				p.getPacketSender().sendFrame126b(FLOUR_STR[2], 8150);
			else
				p.getPacketSender().sendFrame126b(FLOUR_STR[p.getItems().playerHasItem(DairyCowPlugin.BUCKET_OF_MILK) ? 1 : 0], 8150);
			
			if (finishedEggs)
				p.getPacketSender().sendFrame126b(EGG_STR[2], 8151);
			else
				p.getPacketSender().sendFrame126b(EGG_STR[p.getItems().playerHasItem(DairyCowPlugin.BUCKET_OF_MILK) ? 1 : 0], 8151);
			
		}
	}
	
	public static String buildRequirementsString(Player p) {
		StringBuilder sb = new StringBuilder("");
		if (!p.getItems().playerHasItem(1927)) {
			sb.append(" A bucket of milk.");
		}
		if (!p.getItems().playerHasItem(1933)) {
			sb.append(" A pot of flour.");
		}
		if (!p.getItems().playerHasItem(1944)) {
			sb.append(" An egg.");
		}
		return sb.toString();
	}
	
	public static void checkForIngredients(Player p) {
		if (!collectedMilk(p) && p.getItems().playerHasItem(DairyCowPlugin.BUCKET_OF_MILK)) {
			p.getDialogueBuilder()
			.sendAction(()-> {
				if (p.getItems().deleteItem2(DairyCowPlugin.BUCKET_OF_MILK, 1))
					QuestIndex.COOKS_ASSISTANT.setCompletedBit(p, GATHERED_MILK);
			})
			.sendPlayerChat("Here's a bucket of milk.");
		}
		
		if (!collectedFlour(p) && p.getItems().playerHasItem(MillPlugin.POT_OF_FLOUR)) {
			p.getDialogueBuilder()
			.sendAction(()-> {
				if (p.getItems().deleteItem2(MillPlugin.POT_OF_FLOUR, 1))
					QuestIndex.COOKS_ASSISTANT.setCompletedBit(p, GATHERED_FLOUR);
			})
			.sendPlayerChat("Here's a pot of flour.");
		}
		
		if (!collectedEgg(p) && p.getItems().playerHasItem(EGG_ID)) {
			p.getDialogueBuilder()
			.sendAction(()-> {
				if (p.getItems().deleteItem2(EGG_ID, 1))
					QuestIndex.COOKS_ASSISTANT.setCompletedBit(p, GATHERED_EGG);
			})
			.sendPlayerChat("Here's an egg.");
		}
	}
	
	public static boolean completedOneIngredient(Player p) {
		return (QuestIndex.COOKS_ASSISTANT.getProgress(p) & (GATHERED_MILK + GATHERED_FLOUR + GATHERED_EGG)) != 0;
	}
	
	private static boolean collectedMilk(Player p) {
		return  (QuestIndex.COOKS_ASSISTANT.getProgress(p) & GATHERED_MILK) == GATHERED_MILK;
	}
	
	private static boolean collectedFlour(Player p) {
		return  (QuestIndex.COOKS_ASSISTANT.getProgress(p) & GATHERED_FLOUR) == GATHERED_FLOUR;
	}
	
	private static boolean collectedEgg(Player p) {
		return  (QuestIndex.COOKS_ASSISTANT.getProgress(p) & GATHERED_EGG) == GATHERED_EGG;
	}
	
	public static boolean collectedAllIngredients(Player p) {
		return collectedMilk(p) && collectedFlour(p) && collectedEgg(p);
	}
	
	public static void rewardCompletion(Player p) {
		p.sendMessage("Congratulations! Quest complete!");
		
		p.increaseQuestPoints(QUEST_POINTS_REWARD);
		p.getPA().addSkillXP(COOK_EXP_REWARD, Skills.COOKING);
		QuestIndex.COOKS_ASSISTANT.setCompletedBit(p, CooksAssistant.COMPLETED);
		
		//reward interface stuff
		QuestHandlerTemp.openQuestRewardInterface(p);
		
		//reward interface strings
		p.getPacketSender().sendFrame126b("You have completed the Cook's Assistant Quest!", 301);
		
		p.getPacketSender().showModelOnInterfacePacket(302, 2531); // gotta show that cake model, best to include model id in itemDef
		p.getPacketSender().updateModelView(302, 300, 175, 300); // make taht cake model zoom proper and rotation
		
		p.getPacketSender().sendFrame126b(Integer.toString(QUEST_POINTS_REWARD), 4444); // points to reward
		
		p.getPacketSender().sendFrame126b(Integer.toString(Variables.QUEST_POINTS.getValue(p)), 304); // is 304 config? config code is 101
	}
}
