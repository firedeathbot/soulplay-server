package com.soulplay.content.player.quests;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.interfacedata.BitShifting;

public class QuestHandlerTemp {
	
	public static int getQuestProgress(Player c, QuestIndex quest) {
		return c.quests[quest.getIndex()];
	}
	
	public static void incQuestProgress(Player c, QuestIndex quest) {
		c.quests[quest.getIndex()]++;
		updateQuest(c, quest);
	}
	
	public static void decQuestProgress(Player c, QuestIndex quest) {
		c.quests[quest.getIndex()]--;
	}
	
	public static boolean completedQuest(Player c, QuestIndex quest) {
		return c.quests[quest.getIndex()] >= quest.getCompletedCount();
	}
	
	private static final int STRING_START = 62122;
	
	public static void updateQuests(Client c) {
		
		updateQuestPointsString(c);
		
		for (int i = 0; i < QuestIndex.VALUES.length; i++) {
			QuestIndex quest = QuestIndex.VALUES[i];
			updateQuest(c, quest);
			
		}
		
	}
	
	public static void updateQuest(Player c, QuestIndex quest) {
		final int stringID = STRING_START + quest.ordinal();
		if (completedQuest(c, quest)) {
			c.getPacketSender().setTextColor(stringID,
					BitShifting.compressHexColor(0x00f000));
		} else if (getQuestProgress(c, quest) > 0) {
			c.getPacketSender().setTextColor(stringID,
					BitShifting.compressHexColor(0xf8f800));
		} else {
			c.getPacketSender().setTextColor(stringID,
					BitShifting.compressHexColor(0xf80000));
		}
	}
	
	public static void updateQuestPointsString(Player c) {
		c.getPacketSender().sendFrame126("Quest Points: "+Integer.toString(Variables.QUEST_POINTS.getValue(c)), 62101);
	}
	
	public static final String RED = "<col=800000>";
	public static final String RED_LIGHT = "<col=FF0000>";
	public static final String BLUE = "<col=000080>";
	
	
	public static final int TITLE_STRING_ID = 8144;
	private static final int START1 = 8145;
	private static final int END1 = 8196;
	private static final int START2 = 12174;
	private static final int END2 = 12224;
	
	public static void updateQuestInterface(Player c, QuestIndex quest) {

		c.getPacketSender().showInterface(8134);
		
		if (quest.getMsg() == null) {
			quest.generateInstructions(c);
			return;
		}
		
		//below is for simple instructions
		
		int id = START1;
		
		int index = getQuestProgress(c, quest);
		
		if (index >= quest.getMsg().length) {
			c.sendMessage("Quest out of bounds!");
			return;
		}
		String[] msgs = quest.getMsg()[index];

		for (int j = 0, len = msgs.length; j < len; j++) {

			if (id == END2) {
				c.sendMessage("ERROR TOO MUCH LINES!");
				break;
			}

			String msg = msgs[j];

			c.getPacketSender().sendFrame126b(msg, id);

			id++;
			if (id == 8146)//skip this i guess
				id++;
			if (id == END1)
				id = START2;
		}
	}
	
	public static void sendQuestList(final Player c, final String[] msgs) {
		int id = START1;
		for (int j = 0, len = msgs.length; j < len; j++) {

			if (id == END2) {
				c.sendMessage("ERROR TOO MUCH LINES!");
				break;
			}

			String msg = msgs[j];

			c.getPacketSender().sendFrame126b(msg, id);

			id++;
			if (id == 8146)//skip this i guess
				id++;
			if (id == END1)
				id = START2;
		}
	}
	
	public static void clearQuestInterface(Player c) {
		c.getPacketSender().sendConfig(ConfigCodes.CLEAR_INTERFACE, 4);
	}
	
	public static void openQuestRewardInterface(Player p) {
		p.getPacketSender().showInterface(297);
	}
}
