package com.soulplay.content.player.commands;

import com.soulplay.Config;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.util.Misc;

public final class LegendaryDonatorCommands implements Command {

    @Override
    public boolean execute(Client player, CommandParser parser, String realCommand) {
	switch (parser.getCommand()) {

	case "title":
	    try {
		if (parser.hasNext()) {

		    final CommandParser parser2 = CommandParser.split(parser, "-");

		    if (parser2.hasNext(2)) {

			final String title = parser2.nextString();

			if (title.length() > 12) {
			    player.sendMessage("That title is too large. 12 chars allowed.");
			    return true;
			}

			if (title.contains("<") || title.contains(">")) {
			    player.sendMessage("You have unsupported characters in your title.");
			    return true;
			}

			if (title.contains("mod") && !(player.playerRights >= 1 && player.playerRights <= 3)) {
			    player.sendMessage("You are not allowed to have this in your title.");
			    return true;			    
			}
			
			if (title.contains("admin") && !(player.playerRights >= 2 && player.playerRights <= 3)) {
			    player.sendMessage("You are not allowed to have this in your title.");
			    return true;			    
			}
			
			if ((title.contains("owner") || title.contains("developer")) && player.playerRights != 3) {
			    player.sendMessage("You are not allowed to have this in your title.");
			    return true;			    
			}

			player.playerTitle = Misc.capitalize(title);

			final String color = parser2.nextString();
			
			int titleColor = -1;

			if (color.equalsIgnoreCase("orange") || color.equalsIgnoreCase("ora")) {
			    titleColor = 0;
			} else if (color.equalsIgnoreCase("purple") || color.equalsIgnoreCase("pur")) {
			    titleColor = 1;
			} else if (color.equalsIgnoreCase("red")) {
			    titleColor = 2;
			} else if (color.equalsIgnoreCase("green") || color.equalsIgnoreCase("gre")) {
			    titleColor = 3;
			} else if (color.equalsIgnoreCase("black") || color.equalsIgnoreCase("bla")) {
			    titleColor = 4;
//			} else if (color.equalsIgnoreCase("yellow") || color.equalsIgnoreCase("yel")) {
//			    titleColor = 5;
			} else if (color.equalsIgnoreCase("cyan") || color.equalsIgnoreCase("cya")) {
			    titleColor = 6;
			} else if (color.equalsIgnoreCase("white") || color.equalsIgnoreCase("whi")) {
			    titleColor = 7;
			} else if (color.equalsIgnoreCase("blue") || color.equalsIgnoreCase("blu")) {
			    titleColor = 8;
			}
			
			if (titleColor == -1) {
			    player.sendMessage(String.format("The color %s is not supported.", color));
			    player.sendMessage("Supported colors are [orange, purple, red, green, black, cyan, white, blue]");
			    return true;
			}	
			
			player.titleColor = titleColor;
			player.getUpdateFlags().add(UpdateFlag.APPEARANCE);

			player.sendMessage("You succesfully changed your title.");
			return true;
		    }
		}
	    } catch (Exception ex) {
		player.sendMessage("Use syntax ::title your title-color");
		return true;
	    }
	    player.sendMessage("Use syntax ::title your title-color");
	    return true;

	}
	return false;
    }

    @Override
    public boolean canAccess(Client player) {
	/* Diamond donator commands */
	if (player.getDonatedTotalAmount() >= 1000 || Config.isOwner(player)) {
	    return true;
	}
	return false;
    }

}
