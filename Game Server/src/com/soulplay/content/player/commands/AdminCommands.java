package com.soulplay.content.player.commands;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.cache.ItemDef;
import com.soulplay.cache.ObjectDef;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotConfig;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.donate.DonationInterface;
import com.soulplay.content.event.PkingPotEvent;
import com.soulplay.content.event.randomevent.Bork;
import com.soulplay.content.event.randomevent.HatiTheWinterWolf;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.instances.InstanceInterface;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.items.presets.PresetsInterface;
import com.soulplay.content.minigames.FPLottery;
import com.soulplay.content.minigames.FightPitsTournament;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.minigames.dominiontower.DominionManager;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.minigames.gamble.GamblingManager;
import com.soulplay.content.minigames.inferno.InfernoManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager.Mode;
import com.soulplay.content.minigames.partyroom.DropParty;
import com.soulplay.content.minigames.penguin.Penguin;
import com.soulplay.content.minigames.penguin.PenguinHandler;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.TreasureTrailManager;
import com.soulplay.content.minigames.treasuretrails.rewards.EliteItemLoot;
import com.soulplay.content.minigames.treasuretrails.rewards.LootManager;
import com.soulplay.content.npcs.OSRSBoxNpcDataLoader;
import com.soulplay.content.npcs.impl.bosses.alchhydra.AlchemicalHydraInstance;
import com.soulplay.content.npcs.impl.bosses.kraken.KrakenBoss;
import com.soulplay.content.npcs.impl.bosses.nex.NexEvent;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareManager;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareNPC;
import com.soulplay.content.npcs.impl.bosses.zulrah.Zulrah;
import com.soulplay.content.player.FishingContest;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.achievement.SkillCapes;
import com.soulplay.content.player.bot.tasks.*;
import com.soulplay.content.player.bot.tasks.combattasks.UltimatePkTask;
import com.soulplay.content.player.bot.tasks.combattasks.WelfareGearPkTask;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.PlayerKilling;
import com.soulplay.content.player.holiday.christmas.PresentSpawn;
import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.reflectioncheck.ReflectionCheckManager;
import com.soulplay.content.player.reflectioncheck.ReflectionCheckResponseType;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.Binds;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Manager;
import com.soulplay.content.player.skills.dungeoneeringv2.drops.BossDropManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.VisibleRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.items.DungeonItems;
import com.soulplay.content.player.skills.hunterlesik.TrapCounter;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.content.player.skills.smithing.impl.Bronze;
import com.soulplay.content.player.skills.smithing.impl.Dragon;
import com.soulplay.content.poll.PollManager;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.content.tob.TobManager;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.*;
import com.soulplay.game.model.npc.*;
import com.soulplay.game.model.npc.plugins.NpcPlugins;
import com.soulplay.game.model.player.*;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.save.AlterAccounts;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.ShopHandler;
import com.soulplay.game.world.World;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.net.Connection;
import com.soulplay.net.login.LoginServerRequest;
import com.soulplay.net.login.RS2LoginProtocol;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.packet.in.PrivateMessaging;
import com.soulplay.plugin.PluginManager;
import com.soulplay.util.EloRatingSystem;
import com.soulplay.util.Misc;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.sql.SqlHandler;
import com.soulplay.util.sql.configs.CombatConfigs;
import com.soulplay.util.sql.scoregrab.WildyEloRatingSeason;
import com.soulplay.util.sql.upload.NpcDropTableSql;

import plugin.item.skillcape.MagicCapePlugin;
import plugin.object.christmas.SnowmanPlugin;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Optional;

public final class AdminCommands implements Command {

    @Override
    public boolean execute(Client player, CommandParser parser, String realCommand) {
        switch (parser.getCommand()) {
        	case "spowreset": {
        		player.getSeasonalData().powerUps.clear();
        		player.sendMessage("You've reset your powers.");
        		return true;
        	}
        	case "spowitemsgen": {
        		PowerUpData.generateItems();
        		return true;
        	}
        	case "clonetest": {
        		SeasonalData.initClone(player, null, 100);
        		return true;
        	}
        	case "sgivepoints": {
        		int points;
        		Player playerToSet;
        		if (parser.hasNext()) {
        			playerToSet = PlayerHandler.getPlayerSmart(parser.nextString());
        			if (parser.hasNext()) {
        				points = parser.nextInt();
        			} else {
        				points = 1;
        			}
        		} else {
        			points = 10;
        			playerToSet = player;
        		}

        		if (playerToSet == null) {
        			player.sendMessage("Player is not logged in or does not exist.");
        			return true;
        		}

        		Variables.SEASONAL_INTERFACE_POINTS.addValue(playerToSet, points);
        		playerToSet.sendMessage(points + " points for you.");
        		PowerUpInterface.updatePoints(playerToSet);
        		return true;
        	}
        	case "test": {
        		player.transform(-1);
        		player.getPA().resetMovementAnimation();
				player.resetAnimations();
				player.pulse(() -> {
					SeasonalData.transformBeastPower(player);
				});
        		return true;
        	}
        	case "poison": {
        		player.getDotManager().applyPoison(10);
        		return true;
        	}
        	case "punchbag": {
        		NPC npc = NPCHandler.spawnNpc(player, 1, player.absX, player.absY, player.heightLevel, 0, false, false);
        		npc.getSkills().setMaximumLifepoints(1000000);
        		npc.applyFreeze(100000);
        		player.sendMessage("You spawn a Npc.");
        		return true;
        	}
            case "manybots":
            	BotEquipmentLoading.load();

            	for (int i = 0; i < 100; i++) {
            		player.setBot(new BotClient("Test"+i, "asdf"));
            		player.getBot().connect();
            	}
                player.setBot(null);
                return true;
        	case "tome": {
        		if (parser.hasNext()) {
        			int val = parser.nextInt();
        			Variables.TOME_EXTRA_XP_MOD.setValue(player, val);
        			player.setTomeExpLength(100);
        			player.sendMessage("Tome " + val + "% exp boost.");
        		}
        		return true;
        	}
        	case "venom": {
        		player.getDotManager().applyVenom();
        		return true;
        	}
        	case "spowunl": {
        		if (!parser.hasNext()) {
        			player.sendMessage("Usage ::spowunl index");
        			return true;
        		}

                final int index = parser.nextInt();
                PowerUpData powerUp = PowerUpData.list(index);
                if (powerUp == null) {
                	player.sendMessage("Non existant power up.");
                	return true;
                }

                player.getSeasonalData().addPowerUp(player, powerUp);
                player.getPacketSender().sendPowerUpData();
        		return true;
        	}
        	case "spowroll": {
        		PowerUpInterface.rollSinglePowerUp(player);
        		return true;
        	}
        	case "spowloginroll": {
        		player.getSeasonalData().powerUps.clear();
        		PowerUpInterface.rollPowerUps(player);
        		return true;
        	}
        	case "spow": {
        		PowerUpInterface.open(player);
        		return true;
        	}
        	case "ir": {
        		ItemRetrievalManager.openInterface(player);
        		return true;
        	}
        	case "irtest": {
        		player.itemRetrieval = null;
        		ItemRetrievalManager.transfer(player);
        		return true;
        	}
        	case "map": {
        		if (player.absX >= 6400) {
        			player.getPA().movePlayer(player.absX - 6400, player.absY, player.heightLevel);
        		} else {
        			player.getPA().movePlayer(player.absX + 6400, player.absY, player.heightLevel);
        		}
        		return true;
        	}
        	case "togglevote": {
        		VoteInterface.disabled = !VoteInterface.disabled;
        		player.sendMessage("Vote disabled = " + VoteInterface.disabled);
        		return true;
        	}
        	case "nexevent": {
        		NexEvent.announceEvent(player);
        		return true;
        	}
        	case "unlockgame": {
        		RS2LoginProtocol.blockLogins = !RS2LoginProtocol.blockLogins;
        		player.sendMessage("Unlocked " + RS2LoginProtocol.blockLogins);
        		return true;
        	}
        	case "killnightmare": {
        		NightmareNPC nightmare = NightmareManager.boss;
        		if (nightmare != null && nightmare.isActive) {
        			nightmare.death(true);
        		}
        		return true;
        	}
        	case "togglegamble": {
        		GamblingManager.enabled = !GamblingManager.enabled;
        		player.sendMessage("Gambling is now " + (GamblingManager.enabled ? "enabled" : "disabled") + ".");
        		return true;
        	}
            case "spawnbork":
                Bork.spawnBork();
                player.sendMessage("You spawn Bork!");
                return true;
        	case "gamblei": {
        		GamblingInterface.openSetup(player, null);
        		return true;
        	}
        	case "preset": {
        		PresetsInterface.open(player);
        		return true;
        	}
        	case "npcdefcreate": {
        		OSRSBoxNpcDataLoader.createNpcDef();
        		return true;
        	}
			case "instancei": {
				InstanceInterface.open(player);
				return true;
			}
			case "donatei": {
				DonationInterface.open(player);
				return true;
			}
			case "votei": {
				VoteInterface.open(player);
				return true;
			}
        	case "prayers": {
				if (player.playerPrayerBook == PrayerBook.NORMAL) {
					player.playerPrayerBook = PrayerBook.CURSES;
					player.sendMessage("You sense a surge of power flow through your body!");
				} else {
					player.playerPrayerBook = PrayerBook.NORMAL;
					player.sendMessage("You sense a surge of purity flow through your body!");
				}

				player.getCombat().resetPrayers();
				player.updatePrayerBook();
        		return true;
        	}
        case "domtokens": {
        	player.setDominionTokens(4);
        	player.sendMessage("you currently have " + player.getDominionTokens());
        	return true;
        }
        	case "domtest": {
        		DominionManager dom = new DominionManager(player);
        		dom.teleport();
        		player.sendMessage("entering dominion tower");
        		return true;
        	}
        	case "domnext": {
        		if (!player.insideInstance() || !(player.getInstanceManager() instanceof DominionManager)) {
        			player.sendMessage("Instance error. " + player.getInstanceManager());
        			return true;
        		}

        		DominionManager dominionManager = (DominionManager) player.getInstanceManager();
        		dominionManager.advanceWave();
        		dominionManager.teleport();
        		return true;
        	}
        	case "hydra": {
        		new AlchemicalHydraInstance(player);
        		return true;
        	}
        case "toggletob": {
        	TobManager.tobEnabled = !TobManager.tobEnabled;
        	player.sendMessage("Tob is currently disabled = " + TobManager.tobEnabled);
        	return true;
        }
        case "tobtest": {
        	if (player.tobManager == null)
        		return true;
        	
        	Location base = player.tobManager.currentRoom.base();
        	player.sendMessage("base=" + player.getCurrentLocation().subtract(base));
        	return true;
        }
        case "tob": {
    		TobManager tobManager = new TobManager(player.getClan(), player);
			tobManager.generate();
			tobManager.teleportPlayer(player);
        	return true;
        }
        case "loaddonate": {
        	DonationInterface.load();
        	player.sendMessage("Reloaded donation interface categories.");
        	return true;
        }
        case "togglettrails": {
        	TreasureTrailManager.enabled = !TreasureTrailManager.enabled;
        	player.sendMessage("You have toggled treasure trails. Currently = " + TreasureTrailManager.enabled);
        	return true;
        }
        case "toggleraids": {
        	RaidsManager.raidsEnabled = !RaidsManager.raidsEnabled;
        	player.sendMessage("You have toggled raids. Currently = " + RaidsManager.raidsEnabled);
        	return true;
        }
        case "resetkills": {
        	PlayerKilling.reset(player);
        	player.sendMessage("Reset kills");
        	return true;
        }
        case "resetmagiccapes": {
        	player.sendMessage("Resetting magic cape uses.");
        	MagicCapePlugin.resetUses();
        	return true;
        }
        case "pkingpot2b": {
        	player.sendMessage("Adding 2b to pking pot.");
        	PkingPotEvent.addGold(2_000_000_000);
        	return true;
        }
        case "emptypkpot": {
        	player.sendMessage("Resetting pk pot.");
        	PkingPotEvent.resetPkPot();
        	return true;
        }
        case "flyback": {
        	
        	NPC ninja = NPCHandler.spawnNpc(player, 8122, player.getX(), player.getY()+1, player.getZ(), 0, false, false);
        	player.faceLocation(ninja.getCurrentLocation());
        	
        	ninja.setAliveTick(3);
        	
        	ninja.startAnimation(new Animation(2555, 45));//kick animation
        	
        	CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
				
				@Override
				public void stop() {
					
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					
					Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), ninja.getCurrentLocation());
		        	
		        	player.startAnimation(734);// fly back
		        	
		        	ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*-6, dir.getStepY()*-6, 2, 0, dir).setCustomSpeed(45, 0);
		        	
		        	player.setForceMovement(mask);
					
				}
			}, 2);
        	
        	
        	
        	
        	return true;
        }
        
        case "npcextend": {
        	player.extendedNpcRendering = !player.extendedNpcRendering;
        	player.sendMessage("Extended npc rendering enabled = " + player.extendedNpcRendering);
        	return true;
        }
        case "lmsevent2": {
        	if (LmsManager.serverWideMode == Mode.EVENT) {
        		LmsManager.serverWideMode = Mode.NORMAL;
        	} else {
        		LmsManager.startMassLMSEvent();
        	}

        	player.sendMessage("Server wide lms mode is: " + LmsManager.serverWideMode);
        	return true;
        }
        case "inferno": {
        	InfernoManager inferno = new InfernoManager(player);
        	inferno.copy();
        	inferno.teleportPlayer();
        	return true;
        }
        case "cluetest": {
        	player.getTreasureTrailManager().addClue(ClueDifficulty.EASY, true);
        	return true;
        }
        case "cluetest1": {
        	player.getTreasureTrailManager().addClue(ClueDifficulty.MEDIUM, true);
        	return true;
        }
        case "cluetest2": {
        	player.getTreasureTrailManager().addClue(ClueDifficulty.HARD, true);
        	return true;
        }
        case "cluetest3": {
        	player.getTreasureTrailManager().addClue(ClueDifficulty.ELITE, true);
        	return true;
        }
        case "cluetest4": {
        	player.getTreasureTrailManager().addClue(ClueDifficulty.MASTER, true);
        	return true;
        }
        case "clueadvanceeasy": {
        	 final String playerName = parser.nextLine();

             Optional<Client> result = World.search(playerName);

             if (result.isPresent()) {

                 Client other = result.get();
                 other.getTreasureTrailManager().nextStage(ClueDifficulty.EASY);
     		} else {
				player.sendMessage("Player not found.");
			}
        	return true;
        }
		case "clueadvancemed": {
			final String playerName = parser.nextLine();

			Optional<Client> result = World.search(playerName);

			if (result.isPresent()) {

				Client other = result.get();
				other.getTreasureTrailManager().nextStage(ClueDifficulty.MEDIUM);
			} else {
				player.sendMessage("Player not found.");
			}
			return true;
		}
		case "clueadvancehard": {
			final String playerName = parser.nextLine();

			Optional<Client> result = World.search(playerName);

			if (result.isPresent()) {

				Client other = result.get();
				other.getTreasureTrailManager().nextStage(ClueDifficulty.HARD);
			} else {
				player.sendMessage("Player not found.");
			}
			return true;
		}
		case "clueadvanceelite": {
			final String playerName = parser.nextLine();

			Optional<Client> result = World.search(playerName);

			if (result.isPresent()) {

				Client other = result.get();
				other.getTreasureTrailManager().nextStage(ClueDifficulty.ELITE);
			} else {
				player.sendMessage("Player not found.");
			}
			return true;
		}
		case "clueadvancemaster": {
			final String playerName = parser.nextLine();

			Optional<Client> result = World.search(playerName);

			if (result.isPresent()) {

				Client other = result.get();
				other.getTreasureTrailManager().nextStage(ClueDifficulty.MASTER);
			} else {
				player.sendMessage("Player not found.");
			}
			return true;
		}
        	case "spawnholiday": {
        		PresentSpawn.spawn();
        		return true;
        	}
        	case "spawnsnow": {
        		SnowmanPlugin.spawnSnow();
        		return true;
        	}
        case "unregister": {
        	if (parser.hasNext()) {
        		final String name = parser.nextLine();
				LoginServerConnection.instance().submit(new Runnable() {

					@Override
					public void run() {
						LoginServerConnection.instance().unregister(name);
					}
				});
        	}
        	return true;
        }
        case "spawnzeal": {
        	player.setZeals(Integer.MAX_VALUE);
        	player.sendMessage("Set zeals to max value.");
        	return true;
        }
        
        case "maxpkp": {
        	player.pkp = Integer.MAX_VALUE;
        	player.sendMessage("Set Pkp to max value.");
        	return true;
        }
        
        case "ffdate": {
        	if (parser.hasNext()) {
        		int days = parser.nextInt();
        		SoulPlayDate.dayOffset = days;
        		player.sendMessage("You have set the date offset by " + days + " days.");
        	}
        	return true;
        }
        case "resetdate": {
        	SoulPlayDate.dayOffset = 0;
        	player.sendMessage("You have reset the date offset.");
        	return true;
        }
        case "resettrapcount": {
        	TrapCounter.resetMap();
        	player.sendMessage("Resetting trap count for everyone.");
        	return true;
        }
        case "resetpengs": {
        	PenguinHandler.resetPenguins();
        	return true;
        }
        case "raids":
        case "raids2": {
//        	if (player.raidsManager != null) {
//        		player.raidsManager.clear();
//        	}
        	player.getPA().movePlayer(7634, 3572, 0);
        	return true;
        }
        case "kraken": {
        	KrakenBoss.addPlayerToBossZone(player, 0);
        	return true;
        }
        case "raids3": {
        	player.raidsManager = new RaidsManager(null, player);
        	player.raidsManager.generate();
        	return true;
        }
        case "refcheckfield": {
        	parser = CommandParser.split(realCommand, " ");
        	if (parser.hasNext(3)) {
        		String name = parser.nextString();
        		Optional<Client> otherPlayer = World.search(name);
        		if (!otherPlayer.isPresent()) {
        			player.sendMessage("Player " + name + " not found.");
        			return true;
        		}

        		ReflectionCheckManager.sendGetFieldRequest(player, otherPlayer.get(), parser.nextString(), parser.nextString());
        	} else {
        		player.sendMessage("Give more args. need 3");
        	}
        	return true;
        }
        case "refcheckmethod": {
        	parser = CommandParser.split(realCommand, " ");
        	if (parser.hasNext(3)) {
        		String name = parser.nextString();
        		Optional<Client> otherPlayer = World.search(name);
        		if (!otherPlayer.isPresent()) {
        			player.sendMessage("Player " + name + " not found.");
        			return true;
        		}

        		ReflectionCheckManager.sendGetMethodRequest(player, otherPlayer.get(), parser.nextString(), parser.nextString(), "void", new String[] {});
        	} else {
        		player.sendMessage("Give more args. need 3");
        	}
        	return true;
        }
        case "spamtest": {
        	player.difficulty = PlayerDifficulty.PRESTIGE_ONE;
        	player.sendMessage("set prestige");
        	return true;
        }
        case "spamtest2": {
        	player.difficulty = PlayerDifficulty.PRESTIGE_TWO;
        	player.sendMessage("set prestige");
        	return true;
        }
        case "spamtest3": {
        	player.difficulty = PlayerDifficulty.PRESTIGE_THREE;
        	player.sendMessage("set prestige");
        	return true;
        }
        case "spamtest5": {
        	player.difficulty = PlayerDifficulty.NORMAL;
        	player.sendMessage("set prestige");
        	return true;
        }
        case "dgcomplete": {
        	for(int i = 0; i <= 60; i++)
        	player.setDungFloorCompleted(i);
        	return true;
        }
        case "updatevotepolls": {
        	player.sendMessage("Updating Vote Polls.");
        	PollManager.loadPolls();
        	return true;
        }
        
        case "loadseasonpass": {
        	player.sendMessage("Updating season pass without resetting player progress.");
        	SeasonPassManager.updateSeasonPassSettings(true);
        	return true;
        }
        
        case "updateseasonpass": {
        	player.sendMessage("Updating season pass. Resetting player levels for the season pass.");
        	SeasonPassManager.updateSeasonPassSettings(false);
        	return true;
        }
        
        case "rewardseasonalpk": {
        	WildyEloRatingSeason.rewardAndReset(true);
        	return true;
        }
        
        case "updatedroptable": {
        	if (!player.getSpamTick().checkThenStart(10)) {
        		player.sendMessage("please wait");
        		return true;
        	}
        	player.sendMessage("Starting the upload of the drop table.");
        	NpcDropTableSql.uploadToDb(player);
        	return true;
        }
        
        case "dgtoggle": {
        	Party.disableDung = !Party.disableDung;
        	player.sendMessage("Dungeoneering is disabled = " + Party.disableDung);
        	return true;
        }
        case "backupdung": {
        	player.sendMessage("backing up tokens");
        	AlterAccounts.transferDungPoints();
        	if (player.isActive)
        		player.sendMessage("backup is done!");
        	return true;
        }
        case "findbots": {
        	player.sendMessage("Begin of bot search---------------------");
        	for (int i = 0; i < PlayerHandler.players.length; i++) {
        		Player p = PlayerHandler.players[i];
        		if (p == null) continue;
        		
        		if (p.reflectionCheckErrorId != 0) {
        			player.sendMessage(p.getNameSmartUp() + ", id = " + p.reflectionCheckErrorId);
        			ReflectionCheckResponseType response = ReflectionCheckResponseType.values.getOrDefault(p.reflectionCheckErrorId, ReflectionCheckResponseType.UNKNOWN_RESPONSE);
        			player.sendMessage(response.getResponseMessage());
        		}
        	}
        	player.sendMessage("End of bot search------------------------");
        	return true;
        }
        case "loginsize": {
        	player.sendMessage("Size of login queue is :"+LoginServerConnection.instance().getNameToLoginRequests().size());
        	return true;
        }
        case "clearloginqueue": {
        	final long now = System.currentTimeMillis();
        	int count = 0;
        	Iterator<Entry<String, LoginServerRequest>> itr = LoginServerConnection.instance().getNameToLoginRequests().entrySet().iterator();
        	while (itr.hasNext()) {
        		Map.Entry<String, LoginServerRequest> entry = itr.next();
        		LoginServerRequest request = entry.getValue();
        		
        		if (now - request.time > RS2LoginProtocol.LOGIN_TIMEOUT) {
        			itr.remove();
        			count++;
        		}
        	}
        	player.sendMessage(String.format("Cleared %d amount of expired logins.", count));
        	return true;
        }
        case "value": {
        	if (parser.hasNext()) {
        		int id = parser.nextInt();
        		player.sendMessage(ItemDefinition.forId(id).getDropValue()+"");
        	}
        	return true;
        }
        case "tok": {
        	QuestHandlerTemp.getQuestProgress(player, QuestIndex.POKEMON_ADVENTURES);
        	QuestHandlerTemp.incQuestProgress(player, QuestIndex.POKEMON_ADVENTURES);
        	player.sendMess("progress: " + QuestHandlerTemp.getQuestProgress(player, QuestIndex.POKEMON_ADVENTURES));
        	
        	return true;
        }
        case "drainstats": {
        	for (int i = 0; i < Skills.STAT_COUNT; i++)
        	player.getSkills().drainLevel(i, 1, 1);
        	return true;
        }
        case "dgloot": {
            if (!player.insideDungeoneering()) {
                return true;
            }
      
            BossDropManager.dropBossItem(player.dungParty.dungManager, DungeonItems.BATTLEAXES);
        	return true;
        }
        case "fogtest": {
        	player.getPacketSender().sendLmsFog(3200, 3200, 1);
        	return true;
        }
        case "fogreset": {
        	player.getPacketSender().resetFog();
        	return true;
        }
        case "lmsparticipation":
        	if (LmsManager.GAMES_TO_REWARD_AMOUNT < 1) {
        		LmsManager.GAMES_TO_REWARD_AMOUNT = 1;
        	}
    		if (parser.hasNext()) {
    			final int itemId = parser.nextInt();
    			final int itemAmount = parser.nextInt();
    			if (!player.getItems().playerHasItem(itemId, itemAmount)) {
    				player.sendMessage("You don't have that item or amount in your inventory.");
    				return true;
    			}
    			player.getItems().deleteItem2(itemId, itemAmount);
    			LmsManager.PARTICIPATION_REWARD = new Item(itemId, itemAmount);
    			final String formatAmount = Misc.formatNumbersWithCommas(itemAmount);
    			player.sendMessage("You add " + formatAmount + " " + ItemConstants.getItemName(itemId) + " as LMS participation reward!");
    			//AnnouncementHandler.announce("Announcement: " + itemAmount + " " + ItemConstants.getItemName(itemId) + " added to LMS participation reward!");
    			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + formatAmount + " " + ItemConstants.getItemName(itemId) + " added to LMS participation reward!");
    		}
    		return true;
        case "lmstest":
        	player.lmsManager.rewardWinner(player);
        	//LmsManager.startNormal();
        	/*LmsManager lms = new LmsManager();
        	lms.start();
        	System.out.println(lms.height);
        	player.setDynamicRegionClip(lms.clipping);
        	player.getPA().movePlayer(Location.create(lms.startX, lms.startY, lms.height));
        	CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

    			@Override
    			public void execute(
    					CycleEventContainer container) {
    				player.getPacketSender().sendConstructMapRegionPacket(lms.height);
    				container.stop();
			}

			@Override
			public void stop() {
			}
		}, 1);*/
        	return true;
        case "lol":
        	LmsManager.addToWaitRoom(player, Mode.NORMAL);
        	player.sendMess("players normal: " + LmsManager.getNormalPlayersWaiting());
        	player.sendMess("players hard: " + LmsManager.getCompetetivePlayersWaiting());
        	return true;
            case "tickfloor": {
                if (parser.hasNext(1)) {
                    int floor = parser.nextInt();
                    if (!player.isDungFloorCompleted(floor)) {
                        player.setDungFloorCompleted(floor);
                    } else {
                        player.setDungFloorUnCompleted(floor);
                    }

                    Party.updatePartyFloors(player);
                }
                return true;
            }
            case "re":
            	//player.getTaiBwoCleanup()
            	player.sendMess("" + player.getTaiBwoCleanup());
            	return true;
            case "blackout": {
                player.getPacketSender().setBlackout(0);
                return true;
            }
            case "dgguard":
                if (!player.insideDungeoneering()) {
                    return true;
                }

                player.sendMessage(player.dungParty.dungManager.totalGuardiansKilled + "/" + player.dungParty.dungManager.totalGuardiansSpawned);
                return true;
            case "dgbonus":
                if (!player.insideDungeoneering()) {
                    return true;
                }

                player.sendMessage(player.dungParty.dungManager.getBonusOpened() + "/" + player.dungParty.dungManager.getBonusTotal());
                return true;
            case "dgstate":
                if (parser.hasNext(2)) {
                    int players = parser.nextInt();
                    int state = parser.nextInt();
                    int val = state << (players << 1);
                    player.sendMessage("" + val);
                    player.getPacketSender().sendConfig(ConfigCodes.PLAYERS_STATES, val);
                }
                return true;

            case "dgrunes": {
                for (int i = 17780; i <= 17793; i++) {
                    player.getItems().addItem(i, 100000);
                }
                return true;
            }
            case "nrunes": {
                for (int i = 554; i <= 566; i++) {
                    player.getItems().addItem(i, 100000);
                }
                player.getItems().addItem(9075, 100000);
                return true;
            }
            case "open":
                TamingQuestHandler.petQuestReward(player);
                return true;
            case "namechange":
                player.sendMessage("Extra name change: " + player.getExtraNameChange());

                return true;
            case "announce":
                if (parser.hasNext()) {
                    String line = parser.nextLine();
                    AnnouncementHandler.announce(line);
                }
                return true;
            case "emptybank":
                player.getDialogueBuilder().sendOption("Yes empty my bank.", () -> {
                    for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
                        player.bankItems1[i] = 0;
                        player.bankItems1N[i] = 0;
                        player.bankItems2[i] = 0;
                        player.bankItems2N[i] = 0;
                        player.bankItems3[i] = 0;
                        player.bankItems3N[i] = 0;
                        player.bankItems4[i] = 0;
                        player.bankItems4N[i] = 0;
                        player.bankItems5[i] = 0;
                        player.bankItems5N[i] = 0;
                        player.bankItems6[i] = 0;
                        player.bankItems6N[i] = 0;
                        player.bankItems7[i] = 0;
                        player.bankItems7N[i] = 0;
                        player.bankItems8[i] = 0;
                        player.bankItems8N[i] = 0;
                        player.bankItems0[i] = 0;
                        player.bankItems0N[i] = 0;
                        player.bankingItems[i] = 0;
                        player.bankingItemsN[i] = 0;
                    }
                }, "No, keep my bank.", () -> {
                }).execute();
                return true;
            case "enablepoll": {
                if (PollManager.pollOpen) {
                    PollManager.pollOpen = false;
                    player.sendMessage("You have disabled the voting poll!");
                } else {
                    PollManager.pollOpen = true;
                    player.sendMessage("You have enabled the voting poll!");
                }
            }
            case "votetest": {
                PollManager.open(player);
                return true;
            }
            case "testsm": {
                Bronze.load();
                SmithingInterface.open(player, Dragon.instance);
                return true;
            }
            case "testmake": {
                MakeInterface.make(player, 4151, () -> {
                }, 11694, () -> {
                }, 10828, () -> {
                }, -1, null, -1, null);
                System.out.println(player.makeActions.size());
                return true;
            }
            case "superhide":
                if (!player.hidePlayer) {
                    player.superHide = true;
                    player.sendMessage("You hide yourself from mods.");
                    player.displayEquipment[PlayerConstants.playerHat] = 20728;
                    player.headIconPk = 1;
                    player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
                } else {
                    player.superHide = false;
                    player.sendMessage("You reveal yourself to mods.");
                    if (!player.hidePlayer)
                        player.displayEquipment[PlayerConstants.playerHat] = 0;
                    player.headIconPk = player.hidePlayer ? 3 : -1;
                    player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
                }
                return true;
            case "npcslol": {
                if (parser.hasNext()) {
                    String customName = parser.nextLine();
                    for (int i = 0; i < NPCHandler.maxNPCs; i++) {
                        NPC npc = NPCHandler.npcs[i];
                        if (npc == null) {
                            continue;
                        }

                        npc.setCustomName(customName);
                        npc.getUpdateFlags().add(UpdateFlag.NAME_CHANGE);
                    }
                }
                return true;
            }
            case "npcslolclear": {
                for (int i = 0; i < NPCHandler.maxNPCs; i++) {
                    NPC npc = NPCHandler.npcs[i];
                    if (npc == null) {
                        continue;
                    }

                    npc.setCustomName(null);
                    npc.getUpdateFlags().add(UpdateFlag.NAME_CHANGE);
                }
                return true;
            }
            case "dgmap": {
                if (!player.insideDungeoneering()) {
                    return true;
                }

                player.dungParty.dungManager.openMap(player);
                return true;
            }
            case "dgbindload": {
                if (parser.hasNext()) {
                    player.getBinds().load(parser.nextInt(), false);
                    player.getPA().closeAllWindows();
                }
                return true;
            }
            case "dgbindgive": {
                if (parser.hasNext()) {
                    player.getBinds().addBind(parser.nextInt());
                    player.getBinds().update();
                }
                return true;
            }
            case "dgbind": {
                Binds.open(player);
                return true;
            }
            case "warriors": {
                for (int i = 0; i < Config.MAX_NPC_ID; i++) {
                    EntityDef def = EntityDef.forID(i);
                    if (def != null && def.name != null && def.name.toLowerCase().contains("skeletal minion") && def.combatLevel > 0) {
                        System.out.println("{" + i + ", " + Misc.interpolate(20, 3960, 1, 138, def.combatLevel) + "}, //"+def.combatLevel);
                    }
                }
                return true;
            }
            case "dgdroptest": {
            	if (!player.insideDungeoneering()) {
            		return true;
            	}

            	BossDropManager.dropBossItem(player.dungParty.dungManager, DungeonItems.LONGBOWS);
            	return true;
            }
            case "dgend": {
                if (!player.insideDungeoneering()) {
                    return true;
                }

                player.dungParty.dungManager.voteToMoveOn();
                return true;
            }
            case "dg": {
                player.getPA().movePlayer(3454, 3724, 0);
                return true;
            }
            case "complexity": {
                if (parser.hasNext(1)) {
                    final int index = parser.nextInt();
                    player.unlockComplexity(index);
                    player.getPacketSender().sendConfig(8214, player.getComplexityInfo());
                }
                return true;
            }
            case "dgpos": {
                if (!player.insideDungeoneering()) {
                    return true;
                }

                player.sendMessage(player.dungParty.dungManager.getRoomPosX(player.getCurrentLocation()) + ":" + player.dungParty.dungManager.getRoomPosY(player.getCurrentLocation()));
                return true;
            }
            case "dgprev":
            	if (parser.hasNext()) {
            		player.previousProgress = parser.nextInt();
            	} else {
            		player.previousProgress = 0;
            	}

                player.sendMess("previous progress: " + player.previousProgress);
                return true;
            case "dgguardtest":
            	if (!player.insideDungeoneering()) {
            		return true;
            	}

            	VisibleRoom vRoom = player.dungParty.dungManager.getVisibleRoom(player.dungParty.dungManager.getCurrentRoomReference(player.getCurrentLocation()));
            	if (vRoom == null) {
            		return true;
            	}

            	player.sendMessage("Room cleared? " + vRoom.roomCleared());
            	return true;
            case "dgunl": {
            	if (parser.hasNext()) {
            		player.dungFloorsUnlocked = parser.nextInt();
            	}

            	player.sendMessage("Floors unlocked = " + player.dungFloorsUnlocked);
            	return true;
            }
            case "dgdeath":
                player.dungDeaths = 1;
                player.sendMess("deaths: " + player.dungDeaths);
                return true;
            case "dgtest": {
                if (player.insideDungeoneering()) {
                    player.clearDung();
                    player.dungParty.dungManager.destroy(true);
                }

                Party party = player.dungParty;
                if (party == null) {
                    party = new Party(player);
                }

                party.setComplexity(6);
                party.difficulty = 5;
                party.size = DungeonConstants.LARGE_DUNGEON;
                party.floor = 60;
                party.guideMode = true;
                if (parser.hasNext()) {
                    party.size = DungeonConstants.TEST_DUNGEON;
                }
		/*if (parser.hasNext(1)) {
			final int index = parser.nextInt();
			party.floor = index;			
		}*/
                party.startDungeon();

                player.dungParty = party;
                return true;
            }
            case "dgclear": {
                for (DungeonManager entry : DungeonManager.instances.values()) {
                    player.clearDung();
                    entry.destroy(true);
                }

                DungeonManager.instances.clear();
                return true;
            }
            case "test2": {
            	 for (int i = 0; i < Config.BANK_TAB_SIZE; i++) {
                     player.bankItems1[i] = 0;
                     player.bankItems1N[i] = 0;
                     player.bankItems2[i] = 0;
                     player.bankItems2N[i] = 0;
                     player.bankItems3[i] = 0;
                     player.bankItems3N[i] = 0;
                     player.bankItems4[i] = 0;
                     player.bankItems4N[i] = 0;
                     player.bankItems5[i] = 0;
                     player.bankItems5N[i] = 0;
                     player.bankItems6[i] = 0;
                     player.bankItems6N[i] = 0;
                     player.bankItems7[i] = 0;
                     player.bankItems7N[i] = 0;
                     player.bankItems8[i] = 0;
                     player.bankItems8N[i] = 0;
                     player.bankItems0[i] = 0;
                     player.bankItems0N[i] = 0;
                     player.bankingItems[i] = 0;
                     player.bankingItemsN[i] = 0;
                 }

            	long totalGp = 0;
         		List<Item> items = new ArrayList<>();
         		player.refreshBank = false;
         		for (int i = 0; i < 413_798; i++) {
         			items.clear();
	        		int defaultRolls = 5 + Misc.random(2);
	        		for (int i1 = 0; i1 < defaultRolls; i1++) {
	        			int random = Misc.randomNoPlus(16000);
	        			if (random >= 15999) {
	        				LootManager.addLoot(EliteItemLoot.RARE_UNIQUE, items);
	        			} else if (random >= 15980) {
	        				LootManager.addLoot(EliteItemLoot.UNCOMMON_UNIQUE, items);
	        			} else if (random >= 3000) {
	        				LootManager.addLoot(EliteItemLoot.COMMON, items);
	        			} else {
	        				totalGp += (long) 1_000_000 + Misc.random(3_000_000);
	        				//item.add(new GameItem(995, 1_000_000 + Misc.random(3_000_000)));
	        			}
	        		}
	        		
	        		for (int i1 = 0; i1 < items.size(); i1++) {
	        			Item item2 = items.get(i1);
	                	player.getItems().addItemToBank(item2.getId(), item2.getAmount());
	        		}
         		}
         		player.sendMessage("Gained " + Misc.format(totalGp) + " gp.");
         		player.refreshBank = true;
         		player.getItems().resetBank();
            	return true;
            }
            case "dgbosses":
                Manager.clear();

                Manager.init();
                return true;
            case "unl":
            	if (parser.hasNext(2)) {
            		final String name = parser.nextString();
            		final int index = parser.nextInt();

            		Client other = (Client) PlayerHandler.getPlayerSmart(name);
            		if (other == null) {
            			player.sendMessage("Player is offline or wrong name:" + name);
            			return true;
            		}

            		other.unlockPokemon(index);
            		PokemonButtons.refreshData(other);
            	} else if (parser.hasNext(1)) {
                    final int index = parser.nextInt();
                    player.unlockPokemon(index);
                    PokemonButtons.refreshData(player);
                }
                return true;
            case "nexpet":
                player.sendMessage("" + Misc.interpolate(1, 18 + 600 / 100, 1, 4200, 2100));
                return true;
            case "maxlvl":
                player.summonedPokemon.level = 225;
                player.summonedPokemon.stats.strength = 4200;
                player.summonedPokemon.updateScale(player);
                player.summonedPokemon.updateDefence(player);
                player.summonedPokemon.updatePokemonStats(player);
                PokemonButtons.refreshData(player);
                return true;
            case "petprog":
                player.sendMessage("" + QuestHandlerTemp.getQuestProgress(player, QuestIndex.POKEMON_ADVENTURES));
                return true;
            case "petdec":
                QuestHandlerTemp.decQuestProgress(player, QuestIndex.POKEMON_ADVENTURES);
                player.sendMessage("" + QuestHandlerTemp.getQuestProgress(player, QuestIndex.POKEMON_ADVENTURES));
                return true;
            case "petinc":
                QuestHandlerTemp.incQuestProgress(player, QuestIndex.POKEMON_ADVENTURES);
                player.sendMessage("" + QuestHandlerTemp.getQuestProgress(player, QuestIndex.POKEMON_ADVENTURES));
                return true;
            case "pokemonpnt":
                player.summonedPokemon.statPoints += 100;
                PokemonButtons.refreshData(player);
                return true;
            case "pokemonlvl":
                player.summonedPokemon.level = 1;
                PokemonButtons.refreshData(player);
                return true;
            case "pokemonxp":
            	if (player.summonedPokemon == null) {
            		return true;
            	}
        
                player.summonedPokemon.advanceXp(1000, player, player.summoned);
                PokemonButtons.refreshData(player);
                return true;
            case "pokemonxpoff":
            	if (player.summonedPokemon == null) {
            		return true;
            	}
        
                player.summonedPokemon.advanceXp(50, player, player.summoned);
                PokemonButtons.refreshData(player);
                return true;
            case "lmsshop":
                player.getShops().openShop(60);
                return true;
            case "openshop":
            	if (parser.hasNext()) {
            		player.getShops().openShop(parser.nextInt());
            	}
                return true;
            case "lmsarena":
                player.getPA().movePlayer(1831, 3800, 0);
                return true;
            case "lmslobby":
                player.getPA().movePlayer(9804, 3176, 0);
                return true;
            case "revcave":
                player.getPA().movePlayer(9644, 10145, 0);
                return true;
            case "zulrahlobby":
                player.getPA().movePlayer(8599, 3056, 0);
                return true;
            case "config":
                if (parser.hasNext(2)) {
                    final int damage = parser.nextInt();
                    final int damag2e = parser.nextInt();
                    player.getPacketSender().sendConfig(damage, damag2e);
                }
                return true;

            case "resethouse":
                if (parser.hasNext()) {
                	final String name = parser.nextLine();
                    final Player other = PlayerHandler.getPlayerSmart(name);
                    if (other != null) {
                        other.getPOH().reset();
                        player.sendMessage("You have reset the home for " + other.getNameSmartUp());
                    } else {
                    	player.sendMessage(String.format("Player %s is not online.", name));
                    }
                    return true;
                }
                player.getPOH().reset();
                player.sendMessage("You have reset your own house.");
                return true;

            case "logoutbot":
                if (player.getBot() == null) {
                    player.sendMessage("No bots currently to log out.");
                    return true;
                }
                player.getBot().disconnect();
                return true;

            case "buildbots":
                for (String botName : BotConfig.botNames) {
                    player.setBot(new BotClient(botName, "asdf"));
                    player.getBot().connect();
                }
                player.setBot(null);
                return true;

            case "buildbots1":
                for (String element : BotConfig.botNames1) {
                    player.setBot(new BotClient(element, "asdf"));
                    player.getBot().connect();
                }
                player.setBot(null);
                return true;

            case "buildpkbots2":
                for (String element : RSConstants.welfarePkBotNames2) {
                    new BotClient(element, "asdf4444", 7);
                }
                for (String welfarePkBotName : RSConstants.welfarePkBotNames) {
                    new BotClient(welfarePkBotName, "asdf4444", 7);
                }
                return true;

            case "buildpkbots":
		        for (String welfarePkBotName : RSConstants.welfarePkBotNames) {
			         new BotClient(welfarePkBotName, "asdf4444", 7);
		        }
                for (int i = 0; i < 15; i++) {
                    new BotClient("asdf" + i, "asdf", 7);
                }
                return true;

            case "buildpurebots":
                for (int i = 0; i < 10; i++) {
                    new BotClient(RSConstants.pureWelfareBotNames[i], "asdf4444pures", 12);
                }
                return true;

            case "buildpurebots2":
                for (String pureWelfareBotName : RSConstants.pureWelfareBotNames) {
                    new BotClient(pureWelfareBotName, "asdf4444pures", 12);
                }
                return true;

            case "buildpkers":
                for (int i = 0; i < 5; i++) {
                    new BotClient(RSConstants.pureWelfareBotNames[i], "asdf4444pures", 12);
                }
                for (int i = 0; i < 5; i++) {
                    new BotClient(RSConstants.welfarePkBotNames[i], "asdf4444", 7);
                }
                return true;

            case "string":
                if (parser.hasNext()) {
                    int id = parser.nextInt();
                    player.getPacketSender().sendFrame126("TESTING " + id, id);
                    return true;
                }
                return false;
			case "frame126":
				if (parser.hasNext()) {
					int loopCount = parser.nextInt();
					for (int i = 0; i < loopCount; i++) {
						player.getPacketSender().sendFrame126(Integer.toString(i), i);
					}
					return true;
				}
				for (int i = 0; i < 7000; i++) {
					player.getPacketSender().sendFrame126(Integer.toString(i), i);
				}
				return true;

            case "starttask":
                player.getBot().setAction(new MeleeEdgeManTask());
                return true;

            case "starttask2":
                player.getBot().setAction(new MeleeRockCrabsTask());
                return true;

            case "thievingtask":
                player.getBot().setAction(new ThievingTask());
                return true;

            case "fishingtask":
                player.getBot().setAction(new FishingTask());
                return true;

            case "prayertask":
                player.getBot().setAction(new PrayerTask());
                return true;

            case "rocktailstask":
                player.getBot().setAction(new RocktailsTask());
                return true;

            case "firemakingtask":
                player.getBot().setAction(new FireMakingTask());
                return true;

            case "woodcuttingtask":
                player.getBot().setAction(new WoodCuttingAtSkillingTask());
                return true;

            case "miningtask":
                player.getBot().setAction(new MiningAtNeitiznotTask());
                return true;

            case "exploringtask":
                player.getBot().setAction(new ExploringHomeTask());
                return true;

            case "slotmachinetask":
                player.getBot().setAction(new SlotMachineTask());
                return true;

            case "dumpitems":
            	//ItemDefinitionDumper.loadItemList("item742.cfg");
            	ItemDefinition.setDefaultToNull();
                ItemDefinition.dumpDefs();
                player.sendMessage("Dumped items");
                return true;

            case "zulrah":
                Zulrah.sendToZulrah(player, true);
                return true;

            case "face":
                if (parser.hasNext(2)) {

                    switch (parser.nextString()) {

                        case "player":
                            final String playerName = parser.nextLine();

                            Optional<Client> result = World.search(playerName);

                            if (result.isPresent()) {

                                Client other = result.get();

                                player.face(other);
                            }
                            return true;

                        case "position":
                        case "pos":
                        case "location":
                        case "loc":
                            if (parser.hasNext(2)) {
                                final int x = parser.nextInt();

                                final int y = parser.nextInt();

                                player.faceLocation(x, y);
                            }
                            return true;

                    }

                }
                return true;

            case "anim":
            case "animation":
                if (parser.hasNext()) {
                    int id = parser.nextInt();

                    if (id < 0) {
                        player.sendMessage("id must be positive.");
                        return true;
                    }

                    player.startAnimation(id);
                    return true;
                }
                player.sendMessage("Use syntax ::anim 233");
                return true;

            case "forcetext":
            case "forcedtext":
            case "forcechat":
            case "forcedchat":

                if (parser.hasNext()) {

                    CommandParser parser2 = CommandParser.split(parser, "-");

                    if (parser2.hasNext(2)) {

                        final String playerName = parser2.nextString();

                        final String text = parser2.nextString();

                        Optional<Client> result = World.search(playerName);

                        if (result.isPresent()) {

                            Client other = result.get();

                            other.forceChat(text);

                        }
                    }

                }
                return true;
            case "hit":
            	if (parser.hasNext()) {
            		int dmg = parser.nextInt();
            		player.dealDamage(new Hit(dmg, 0, -1));
            	}
            	return true;
            case "hitsplat":
                if (parser.hasNext(3)) {
                    try {

                        final int damage = parser.nextInt();

                        final int hitSplat = parser.nextInt();

                        if (hitSplat < 0 || hitSplat > 2) {
                            throw new RuntimeException("Range is between 0 and 2");
                        }

                        final int hitIcon = parser.nextInt();

                        if (hitIcon < -1 || hitIcon > 4) {
                            throw new RuntimeException("Range is between -1 and 4");
                        }

                        player.showHitMask(new Hit(damage, hitSplat, hitIcon));

                        return true;

                    } catch (Exception ex) {
                        player.sendMessage("Use syntax ::hitsplat (0-2) (-1-4)");
                        return true;
                    }

                }
                player.sendMessage("Use syntax ::hitsplat (0-2) (-1-4)");
                return true;

            case "dpeventitems":
                int[][] items = {
                        {995+1, 20_000_000},
                        {537+1, 500},

                };
                for (int i = 0; i < 50; i++) {
                    int[] itemToDrop = items[Misc.random(items.length - 1)];
                    Item item = new Item(itemToDrop[0], itemToDrop[1]);
                    if (!DropParty.spawnToChest(item)) {
                        player.sendMessage("You have maxed out the chest.");
                        break;
                    }
                }
                player.sendMessage("You spawn in 25M cash stacks into the chest.");
                return true;

            case "goldenchest":
                if (NPCHandler.GOLDEN_CHEST_DROP) {
                    NPCHandler.GOLDEN_CHEST_DROP = false;
                    player.sendMessage("You have disabled the golden chest drop.");
                } else {
                    NPCHandler.GOLDEN_CHEST_DROP = true;
                    player.sendMessage("You have enabled the golden chest drop.");
                }
                return true;

            case "magicalevent":
            	MagicalCrate.spawnWorldCrate();
            	return true;
            	
            case "magicalcratetele":
            	if (MagicalCrate.getLocationData() == null) {
            		player.sendMessage("The crate is no longer in that location.");
            	} else {
            		player.getPA().movePlayer(MagicalCrate.getLocationData().getSpawnPos());
            		player.sendMessage("Teleporting to the spawn location of the crate.");
            	}
            return true;
            	
            case "hatievent":
            	HatiTheWinterWolf.spawnHati();
            	return true;

            case "smithevent":
                SmithingInterface.dragonSmithEvent = true;
                player.sendMessage("You have enabled the dragon smithing event!");
                return true;

            case "nspawn":
                NPCHandler.init();
                NPCHandler.loadNpcHandler();
                for (Player other : PlayerHandler.players) {

                    if (other == null) {
                        continue;
                    }

                    other.sendMessage("[<col=ff0000>" + player.getNameSmartUp() + "</col>] " + "NPC Spawns have been reloaded.");
                }
                return true;

            case "trade":
                if (parser.hasNext()) {
                    String playerName = parser.nextLine();
                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                Client c2 = (Client) PlayerHandler.players[i];
                                player.getTradeAndDuel().requestTrade(c2);
                                c2.getTradeAndDuel().requestTrade(player);
                                return true;
                            }
                        }
                    }
                }
                return true;

            case "equipcopy":
                if (parser.hasNext()) {
                    String playerName = parser.nextLine();
                    Player p = PlayerHandler.getPlayerSmart(playerName);
                    if (p == null) {
                        player.sendMessage("Player is offline or wrong name:" + playerName);
                        return true;
                    }
                    for (int i = 0; i < p.playerEquipment.length; i++) {
                        player.getItems().setEquipment(p.playerEquipment[i], p.playerEquipmentN[i], i);
                    }
                    int weaponId = player.playerEquipment[PlayerConstants.playerWeapon];
                    player.getItems().sendWeapon(weaponId, ItemConstants.getItemName(weaponId));
                    player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
                }
                return true;

            case "loopgfx":
                if (parser.hasNext()) {

                    final int gfxId = parser.nextInt();

                    if (player.performingAction) {
                        player.sendMessage("Stopping GFX Loop.");
                        player.performingAction = false;
                        return true;
                    }
                    player.sendMessage("Starting GFX loop");

                    player.performingAction = true;

                    CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

                        int count = gfxId;

                        @Override
                        public void execute(CycleEventContainer container) {

                            player.startGraphic(Graphic.create(count, GraphicType.LOW));

                            // c.getPA().stillGfx(count, c.absX, c.absY+2, 10, 1);
                            player.sendMessage("GFX: " + count);
                            count++;

                            if (count > 3257 || !player.performingAction) {
                                container.stop();
                            }
                        }

                        @Override
                        public void stop() {
                            player.sendMessage("Stopped gfx loop at count: " + count);
                        }
                    }, 2); // delay
                }
                return true;

            case "resetpkstats":
                if (parser.hasNext()) {

                    final String playerName = parser.nextLine();

                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                Client c2 = (Client) PlayerHandler.players[i];
                                c2.KC = c2.DC = 0;
                                c2.setEloRating(EloRatingSystem.DEFAULT_ELO_START_RATING, true);
                                c2.sendMessage("Your PK stats have been reset.");

                                player.sendMessage("You have reset " + c2.getNameSmartUp() + "'s PK stats.");
                                return true;
                            }
                        }
                    }
                }
                return true;

            case "eloreset":
                if (parser.hasNext()) {
                    final String playerName = parser.nextLine();

                    Client c2 = (Client) PlayerHandler.getPlayerSmart(playerName);
                    if (c2 != null && c2.isActive) {
                        c2.setEloRating(EloRatingSystem.DEFAULT_ELO_START_RATING, true);
                        c2.sendMessage("Your Elo has been reset.");

                        player.sendMessage("You have reset " + c2.getNameSmartUp() + "'s Elo rating.");
                        return true;

                    }

                }
                return true;

            case "info":
                if (parser.hasNext()) {
                    final String playerName = parser.nextLine();

                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                player.sendMessage("ip: " + PlayerHandler.players[i].connectedFrom);
                                return true;
                            }
                        }
                    }
                }
                return true;

            case "demoted":
                if (parser.hasNext()) {
                    String playerToG = parser.nextLine();
                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null && PlayerHandler.players[i].playerRights != 3) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerToG)) {
                                PlayerHandler.players[i].playerRights = 0;
                                PlayerHandler.players[i].disconnected = true;
                                player.sendMessage("You've demoted the user:  " + PlayerHandler.players[i].getNameSmartUp()
                                        + " IP: " + PlayerHandler.players[i].connectedFrom);
                                ChatCrownsSql.lockChatCrown(PlayerHandler.players[i], ChatCrown.ADMIN_CROWN);
                                ChatCrownsSql.lockChatCrown(PlayerHandler.players[i], ChatCrown.MOD_CROWN);
                            }
                        }
                    }
                }
                return true;

            case "givevet":
                if (parser.hasNext()) {
                    final String playerName = parser.nextLine();

                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                PlayerHandler.players[i].playerRights = 9;
                                PlayerHandler.players[i].disconnected = true;
                                player.sendMessage(
                                        "You've promoted to Veteran, the user:  " + PlayerHandler.players[i].getNameSmartUp()
                                                + " IP: " + PlayerHandler.players[i].connectedFrom);
                                ChatCrownsSql.unlockChatCrown(PlayerHandler.players[i], ChatCrown.VETERAN);
                            }
                        }
                    }
                }
                return true;

            case "givemod":
                if (parser.hasNext()) {
                    final String playerName = parser.nextLine();

                    Optional<Client> result = World.search(playerName);

                    if (result.isPresent()) {

                        Client other = result.get();

                        if (other.playerRights == 1) {
                            player.sendMessage("That player is already a mod.");
                            return true;
                        }

                        other.playerRights = 1;
                        other.disconnected = true;
                        other.sendMessage(
                                String.format("You have been promoted to moderator by user %s.", player.getNameSmartUp()));
                        ChatCrownsSql.unlockChatCrown(other, ChatCrown.MOD_CROWN);
                        return true;
                    }
                }
                return true;

            case "setcrown":
                if (parser.hasNext()) {
                    try {
                        String line = parser.nextLine();

                        ChatCrown result = ChatCrown.valueOf(line.toUpperCase());

                        player.setCrown(result.ordinal());
                        player.sendMessage("You successfully your crown.");
                    } catch (Exception ex) {
                        player.sendMessage("Available crowns are...");
                        for (ChatCrown crown : ChatCrown.values) {
                            player.sendMessage(crown.name().toLowerCase());
                        }
                    }
                    return true;
                }
                player.sendMessage("Use syntax ::setcrown admin_crown");
                return true;

            case "unlockcrowns":
                for (ChatCrown crown : ChatCrown.values) {
                    ChatCrownsSql.unlockChatCrown(player, crown);
                }
                player.sendMessage("You unlocked all crowns.");
                return true;

            case "giveadmin":
                if (parser.hasNext()) {
                    final String playerName = parser.nextLine();

                    Optional<Client> result = World.search(playerName);

                    if (result.isPresent()) {

                        Client other = result.get();

                        if (other.playerRights == 2) {
                            player.sendMessage("That player is already an admin.");
                            return true;
                        }

                        other.playerRights = 2;
                        other.disconnected = true;
                        other.sendMessage(String.format("You have been promoted to admin by user %s.", player.getNameSmartUp()));
                        ChatCrownsSql.unlockChatCrown(other, ChatCrown.ADMIN_CROWN);
                        return true;
                    }
                }
                player.sendMessage("Use syntax ::giveadmin player name");
                return true;

            case "givepts":
                if (parser.hasNext()) {
                    final int points = parser.nextInt();
                    if (points < 1 || points > 100) {
                        player.sendMessage("Over 100 points is too much i think.");
                        return true;
                    }

                    final String playerName = parser.nextLine();

                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                            	Player other = PlayerHandler.players[i];
                                other.setDonPoints(other.getDonPoints() + points,
                                        true);
                                other.setDonatedTotalAmount(other.getDonatedTotalAmount() + points, true);
                                player.sendMessage("You've given " + points + " donator points to the user:  "
                                        + other.getNameSmartUp() + " IP: "
                                        + other.connectedFrom);
                                return true;
                            }
                        }
                    }
                }
                return true;

            case "givebro":
                if (parser.hasNext()) {
                    String playerName = parser.nextLine();
                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                PlayerHandler.players[i].playerRights = 4;
                                PlayerHandler.players[i].disconnected = true;
                                player.sendMessage(
                                        "You've promoted to bronze donator the user:  " + PlayerHandler.players[i].getNameSmartUp()
                                                + " IP: " + PlayerHandler.players[i].connectedFrom);
                                ChatCrownsSql.unlockChatCrown(PlayerHandler.players[i], ChatCrown.RED_DONOR);
                            }
                        }
                    }
                }
                return true;
            case "completetask": {
            	Slayer.completeTask(player);
            	return true;
            }
            case "givesil":
                if (parser.hasNext()) {

                    final String playerName = parser.nextLine();

                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                PlayerHandler.players[i].playerRights = 5;
                                PlayerHandler.players[i].disconnected = true;
                                player.sendMessage(
                                        "You've promoted to silver donator the user:  " + PlayerHandler.players[i].getNameSmartUp()
                                                + " IP: " + PlayerHandler.players[i].connectedFrom);
                                ChatCrownsSql.unlockChatCrown(PlayerHandler.players[i], ChatCrown.BLUE_DONOR);
                            }
                        }
                    }

                }
                return true;

            case "givegol":
                if (parser.hasNext()) {
                    String playerName = parser.nextLine();
                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                PlayerHandler.players[i].playerRights = 6;
                                PlayerHandler.players[i].disconnected = true;
                                player.sendMessage(
                                        "You've promoted to gold donator the user:  " + PlayerHandler.players[i].getNameSmartUp()
                                                + " IP: " + PlayerHandler.players[i].connectedFrom);
                                ChatCrownsSql.unlockChatCrown(PlayerHandler.players[i], ChatCrown.GOLD_DONOR);
                            }
                        }
                    }
                }
                return true;

            case "update":
                if (parser.hasNext()) {
                    // FPLottery.saveLists();
                    // if (!Config.SERVER_DEBUG)
                    // PKBoards.saveBoards();
                    // Server.saveLists();
                    final int seconds = parser.nextInt();
                    PlayerHandler.updateSeconds = seconds;
                    PlayerHandler.updateAnnounced = false;
                    PlayerHandler.updateRunning = true;
                    PlayerHandler.updateStartTime = System.currentTimeMillis();

                    new Thread(PenguinHandler::savePenguinNames).start();
                }

                return true;

            case "headiconpk":
                if (parser.hasNext()) {
                    final int id = parser.nextInt();

                    player.headIconPk = id;
                    player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
                }
                return true;

            case "headicon":
                if (parser.hasNext()) {
                    final int id = parser.nextInt();

                    player.setHeadIcon(id);
                }
                return true;

            case "skillcape":
                SkillCapes.emote(player);
                return true;

            case "startpc":
            	PestControl.startGame(false);
            	return true;

            case "startpc2":
            	PestControl.startGame(true);
            	return true;

            case "taskmanager":
                player.startGraphic(Graphic.create(2930, GraphicType.LOW));
                player.startAnimation(15034);
                player.forceChat("I am the taskmaster!");
                return true;

            case "meleemaxhit":
                player.sendMessage("Melee Max Hit: " + player.getCombat().calculateMeleeMaxHit(1.0) + "");
                return true;

            case "reloadshops":
                ShopHandler.init();// Server.shopHandler = new
                // server.world.ShopHandler();
                player.sendMessage("Shops have been reloaded");
                return true;

            case "reload":
                if (parser.hasNext()) {
                    String type = parser.nextString();

                    switch(type) {
	                    case "npcdef":
	                    	TaskExecutor.execute(() -> {
	                    		NPCHandler.loadNpcStats();
								Server.getTaskScheduler().schedule(() -> {
									if (player.isActive) {
										player.sendMessage("Finished reloading NPC Defs from DB.");
									}
								});
	                    	});
	                    	player.sendMessage("Started reloading npc defs.");
	                    	return true;
                    
                        case "p":
                        case "plugins":
                        	player.sendMessage("Reloading plugins.");
                            TaskExecutor.execute(() -> {
                            	PluginManager.reload();
                            	if (!player.disconnected)
                            		player.sendMessage(String.format("Reloaded: %d plugins.", PluginManager.getPluginCount()));
                            });
                            return true;

                        case "shops":
                        case "s":
                        	player.sendMessage("Reloading shops.");
                            ShopHandler.init();// Server.shopHandler = new
                            // server.world.ShopHandler();
                            player.sendMessage("Shops have been reloaded");
                            return true;

                        case "bans":
                        	player.sendMessage("Reloading bans.");
                            Connection.clearLists();
                            Connection.initialize();
                            player.sendMessage("Reloaded the ban lists.");
                            return true;
                            
                        case "drops":
                        case "drop":
                        	player.sendMessage("Reloading Drop Table.");
                            DropTable.reloadDropTables(player);
                            player.sendMessage("Drops have been reloaded from CFG");
                            return true;

                        case "items":
                        case "itemdef":
                        case "itemdefs":
                        	player.sendMessage("starting to reload item defs.");
                            ItemDefinition.reloadList();
                            FifthItemAction.loadMap();
                            CombatConfigs.reloadConfigs();
                            player.sendMessage("Reloaded the Item Defs");
                        	return true;
                        	
                        case "absnpc":
                        case "absnpcs":
                        	NpcPlugins.clear();
                        	NpcPlugins.init();
                        	player.sendMessage("reloaded abstract npcs");
                        	return true;

                    }
                }
                return true;

            case "reloaditemdef":
            	player.sendMessage("starting to reload item defs.");
                ItemDefinition.reloadList();
                FifthItemAction.loadMap();
                player.sendMessage("Reloaded the Item Defs");
                return true;

            case "reloadbans":
                Connection.clearLists();
                Connection.initialize();
                player.sendMessage("Reloaded the ban lists.");
                return true;

            case "claws":
                player.startGraphic(Graphic.create(1950, GraphicType.LOW));
                player.startAnimation(10961);
                return true;

            case "emote":
                if (parser.hasNext()) {
                    player.resetAnimations();
                    player.startAnimation(parser.nextInt());
                }
                return true;

            case "wemote":
                if (parser.hasNext()) {
                    player.resetAnimations();
                    player.getMovement().getPlayerMovementAnimIds()[2] = parser.nextShort();
                    player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
                }
                return true;

            case "forcepenguinsave":
                PenguinHandler.savePenguinNames();
                player.sendMessage("Successfully force saved penguin data.");
                return true;

            case "forcepenguinload":
                PenguinHandler.loadPenguinNames();
                player.sendMessage("Successfully force loaded penguin data.");
                return true;

            case "resetpenguins":
                for (Penguin penguin : PenguinHandler.PENGUINS) {
                    penguin.getSpottedPlayers().remove(player.mySQLIndex);
                }
                return true;

            case "giveinfo":
                if (parser.hasNext()) {

                    final String playerName = parser.nextLine();

                    Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

                    if (other != null && other.isActive) {
                        other.playerRights = 7;
                        other.disconnected = true;
                        player.sendMessage("You've promoted to informer the user:  " + other.getNameSmartUp() + " IP: "
                                + other.connectedFrom);
                        ChatCrownsSql.unlockChatCrown(other, ChatCrown.INFORMER);
                    }
                }

                return true;

            case "alltome":
                for (int j = 0; j < PlayerHandler.players.length; j++) {
                    if (PlayerHandler.players[j] != null && PlayerHandler.players[j].correctlyInitialized
                            && !PlayerHandler.players[j].inMinigame()) {
                        Client c2 = (Client) PlayerHandler.players[j];
                        if (c2 == player) {
                            continue;
                        }
                        c2.getPA().movePlayer(player.absX, player.absY, player.heightLevel);
                        c2.sendMessage("Mass teleport to: " + player.getNameSmartUp());
                        c2 = null;
                    }
                }
                player.sendMessage("Mass teleporting everyone to myself.");
                return true;

            case "wobj":
                try (BufferedWriter out = new BufferedWriter(
                        new FileWriter("./Data/" + Config.cfgDir() + "/custom-objects.cfg", true))) {
                    out.write(player.oI + "\t" + player.oX + "\t" + player.oY + "\t" + player.oH + "\t" + player.oF + "\t"
                            + player.oT);
                    out.newLine();
                    player.sendMessage("Written last spawn object with ::obj to file");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;

            case "gfx":
                try {
                    if (parser.hasNext(2)) {

                        final int id = parser.nextInt();

                        final String height = parser.nextString();

                        if (id < 0 || id > Short.MAX_VALUE) {
                            return true;
                        }

                        player.startGraphic(Graphic.create(id, GraphicType.valueOf(height.toUpperCase())));
                        return true;
                    } else if (parser.hasNext()) {
                        final int id = parser.nextInt();

                        if (id < 0 || id > Short.MAX_VALUE) {
                            return true;
                        }

                        player.startGraphic(Graphic.create(id, GraphicType.LOW));
                        return true;
                    }
                } catch (Exception ex) {
                    player.sendMessage("Use syntax ::gfx 542 (optional=low|medium|high)");
                }
                player.sendMessage("Use syntax ::gfx 542 (optional=low|medium|high)");
                return true;

            case "object":
                if (parser.hasNext()) {
                    int id = parser.nextInt();
                    int type = 10;
                    if (parser.hasNext()) {
                        type = parser.nextInt();
                    }
                    player.getPacketSender().addObjectPacket(id, player.absX, player.absY, type, 0);
                }
                return true;

            case "deleteitemfromgame":
                if (parser.hasNext()) {

                    final int itemId = parser.nextInt();

                    if ((itemId <= Config.ITEM_LIMIT) && (itemId >= 0)) {
                        for (Player other : PlayerHandler.players) {
                            Client p = (Client) other;
                            if (p == null) {
                                continue;
                            }
                            if (p != player) {
                                p.removeItemFromPlayer(itemId);
                                p.sendMessage("Your <col=ff0000>" + ItemProjectInsanity.getItemName(itemId) + "</col> has been removed.");
                            }
                        }
                    } else {
                        player.sendMessage("No such item.");
                    }
                }
                return true;

            case "obj":
                if (parser.hasNext(3)) {

                    final int objectId = parser.nextInt();

                    final int face = parser.nextInt();

                    final int type = parser.nextInt();

                    player.getPacketSender().addObjectPacket(objectId, player.absX, player.absY, type, face);

                    player.oX = player.absX;
                    player.oI = objectId;
                    player.oY = player.absY;
                    player.oH = player.heightLevel;
                    player.oF = face;
                    player.oT = type;
                } else if (parser.hasNext(2)) {
                    final int objectId = parser.nextInt();

                    final int face = parser.nextInt();

                    player.getPacketSender().addObjectPacket(objectId, player.absX, player.absY, 10, face);

                    player.oX = player.absX;
                    player.oI = objectId;
                    player.oY = player.absY;
                    player.oH = player.heightLevel;
                    player.oF = face;
                    player.oT = 10;
                } else if (parser.hasNext(1)) {
                    final int objectId = parser.nextInt();

                    player.getPacketSender().addObjectPacket(objectId, player.absX, player.absY, 10, 0);

                    player.oX = player.absX;
                    player.oI = objectId;
                    player.oY = player.absY;
                    player.oH = player.heightLevel;
                    player.oF = 0;
                    player.oT = 10;
                }
                return true;

            case "meleeacc":
                if (parser.hasNext()) {
                    final double percent = parser.nextDouble();

                    if (percent > 2.0) {
                        player.sendMessage("That percentage is too high.");
                        return true;
                    }
                    PlayerConstants.MELEE_ACCURACY = percent;
                    player.sendMessage("You set the percentage at " + percent);
                }
                return true;

            case "magicacc":
                if (parser.hasNext()) {

                    final double percent = parser.nextDouble();

                    if (percent > 2.0) {
                        player.sendMessage("That percentage is too high.");
                        return true;
                    }

                    PlayerConstants.MAGIC_ACCURACY = percent;
                    player.sendMessage("You set the percentage at " + percent);
                }
                return true;

            case "rangeacc":
                if (parser.hasNext()) {
                    final double percent = parser.nextDouble();

                    if (percent > 2.0) {
                        player.sendMessage("That percentage is too high.");
                        return true;
                    }
                    PlayerConstants.RANGE_ACCURACY = percent;
                    player.sendMessage("You set the percentage at " + percent);
                }
                return true;

            case "falem":
                if (parser.hasNext()) {
                    String text = parser.nextLine();

                    for (Player p : PlayerHandler.players) {
                        if (p == null) {
                            continue;
                        }

                        p.forceChat(text);
                    }
                }
                return true;

            case "npc":
                if (parser.hasNext()) {
                    final int npcId = parser.nextInt();

                    if (npcId < 0 || npcId > NPCDefinitions.getDefinitions().length) {
                        player.sendMessage("No such NPC.");
                        return true;
                    }

                    NPCHandler.spawnNpc(player, npcId, player.absX, player.absY, player.heightLevel, 0, false, false);
                    player.sendMessage("You spawn a Npc.");
                }
                return true;

            case "heal":
                if (parser.hasNext()) {
                    final String username = parser.nextLine();

                    Optional<Client> result = World.search(username);

                    if (result.isPresent()) {

                        Client other = result.get();

                        other.getSkills().restore();
                        other.specAmount = 10.0;
                        other.getDotManager().reset();
                        other.sendMessage("You have been healed by " + player.getNameSmartUp() + ".");
                        player.sendMessage("You have healed " + other.getNameSmartUp() + ".");
                        return true;

                    }
                }
                
                player.getSkills().restore();
                player.specAmount = 10.0;
                player.getDotManager().reset();
                player.sendMessage("You have healed yourself.");
                return true;

            case "pnpc":
                if (parser.hasNext()) {
                    final int npcId = parser.nextInt();

                    if (npcId < 0) {
                        player.transform(-1);
                        player.sendMessage("You transformed back into a player.");
                        player.getPA().resetMovementAnimation();
                        player.resetAnimations();
                        return true;
                    }

                    EntityDef def = EntityDef.forID(npcId);

                    if (def == null) {
                        player.sendMessage("That npc does not exist.");
                        return true;
                    }

                    player.transform(npcId);
                    player.sendMessage(
                            String.format("You transformed into %s %s.", Misc.aOrAn(def.getName()), def.getName()));
                    player.sendMessage("stand animation " + def.standAnim);
                    player.getPA().resetMovementAnimation();
                    player.resetAnimations();
                }
                return true;

            case "killallnpcs":
                for (NPC n : NPCHandler.npcs) {
                    if (n != null) {
                        n.setDead(true);
                        n.addInfliction(player.mySQLIndex, 10);
                        player.lastNpcAttacked = n.npcIndex;
                        // n.killedBy = c.getId();
                    }
                }

                player.sendMessage("Killed all npcs.");
                return true;

            case "killall":
                for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                    if (PlayerHandler.players[i] != null) {
                        Client c2 = (Client) PlayerHandler.players[i];
                        c2.getPA().safeDeathSecureTimer();
                        // c2.damageToDealAfterCycle = 99;
                        c2.dealDamage(new Hit(150, 0, 0));
                        c2.sendMessage("You have been killed by " + player.getNameSmartUp());
                        player.sendMessage("You have killed " + c2.getNameSmartUp());
                    }
                }
                return true;

            case "kill":
                if (parser.hasNext()) {
                    final String playerName = parser.nextLine();

                    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
                        if (PlayerHandler.players[i] != null) {
                            if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
                                Client c2 = (Client) PlayerHandler.players[i];
                                c2.setDead(true);
                                c2.getPlayerLevel()[3] = 0;
                                c2.sendMessage("You have been killed by " + player.getNameSmartUp());
                                player.sendMessage("You have killed " + c2.getNameSmartUp());
                                return true;
                            }
                        }
                    }
                }
                return true;
            
            case "getvar":
            	try {
	                if (parser.hasNext(2)) {
	                    final String playerName = parser.nextString();
	                    final String varName = parser.nextString();
	
	                    Player c2 = PlayerHandler.getPlayerSmart(playerName);
	                    if (c2 == null) {
	                    	player.sendMessage("Player " + playerName + " offline.");
	                    	return true;
	                    }

						Field field = Misc.getField(Client.class, varName);

						if (field == null) {
							player.sendMessage("Field " + varName + " doesn't exist.");
							return true;
						}
	
						field.setAccessible(true);
						Object value = field.get(c2);
						player.sendMessage("You have found name=" + c2.getNameSmartUp() + ", var=" + varName + ", value=" + value);
						return true;
	                }
            	} catch(Exception e) {
            		player.sendMessage("Error while editing player value. " + e.getMessage());
            	}
                return true;
                
            case "setvar":
            case "editvar":
            	try {
            		parser = CommandParser.split(realCommand, " ");
	                if (parser.hasNext(3)) {
	                    final String playerName = parser.nextString();
	                    final String varName = parser.nextString();
	                    final String value = parser.nextString();
	
	                    Player c2 = PlayerHandler.getPlayerSmart(playerName);
	                    if (c2 == null) {
	                    	player.sendMessage("Player " + playerName + " offline.");
	                    	return true;
	                    }

						Field field = Misc.getField(Client.class, varName);

						if (field == null) {
							player.sendMessage("Field " + varName + " doesn't exist.");
							return true;
						}
	
						field.setAccessible(true);
						if (field.getType() == Boolean.TYPE) {
							field.set(c2, Boolean.parseBoolean(value));
						} else if (field.getType() == Integer.TYPE) {
							field.set(c2, Integer.parseInt(value));
						} else if (field.getType() == Long.TYPE) {
							field.set(c2, Long.parseLong(value));
						} else if (field.getType() == String.class) {
							field.set(c2, value);
						} else if (field.getType() == AtomicBoolean.class) {
							((AtomicBoolean) field.get(c2)).set(Boolean.parseBoolean(value));
						} else {
							player.sendMessage("Type " + field.getType() + " not added.");
							return true;
						}

						player.sendMessage("You have edited name=" + c2.getNameSmartUp() + ", var=" + varName + ", value=" + value);
						c2.sendMessage("You feel something strange happening to you.");
						return true;
	                }
            	} catch(Exception e) {
            		player.sendMessage("Error while editing player value. " + e.getMessage());
            	}
                return true;

            case "fpsale":
                if (parser.hasNext()) {
                    double percent = parser.nextDouble();

                    if (percent > 0.6) {
                        player.sendMessage("That percentage is too high.");
                        return true;
                    }

                    player.sendMessage("You set the percentage at " + percent);
                    player.sendMessage("Remember that the percentage has to be 0.15 for 15%, etc");
                    Config.increasedFPPoints = percent;
                }
                return true;

            case "openfightpits":
                for (Player p : PlayerHandler.players) {
                    if (p == null) {
                        continue;
                    }
                    Client c2 = (Client) p;
                    if (RSConstants.fightPitsRoom(c2.getX(), c2.getY())) {
                        if (!c2.isDead()) {
                            c2.addToHp(99);
                            c2.getPA().movePlayer(2399, 5177, 0);
                        }
                    }
                }
                // FightPitsTournament.openTournament();
                FightPitsTournament.nextGameTournament = true;
                return true;

            case "openfunpk":
                for (Player p : PlayerHandler.players) {
                    if (p == null) {
                        continue;
                    }
                    Client c2 = (Client) p;
                    if (c2.inFunPk()) {
                        if (!c2.isDead()) {
                            c2.addToHp(99);
                            c2.getPA().movePlayer(3304, 3123, 0);
                        }
                    }
                }
                FunPkTournament.openTournament();
                return true;

            case "updateplayercount":
                // SimpleSql.updatePlayerCount();
                player.sendMessage("Updating playercount.");
                try {
                    SqlHandler.updatePlayerCount(PlayerHandler.getPlayerCount(), PlayerHandler.getStaffCount(),
                            PlayerHandler.getWildyCount(), PlayerHandler.getPlayersInSoulWars(), PlayerHandler.getPlayersInCastleWars(), PlayerHandler.getPlayersInClanWars(), PlayerHandler.getPlayersInPestControl(), PlayerHandler.getPlayersInDuelArena(), PlayerHandler.getPlayersInGamblingArea(), PlayerHandler.getPlayersInLMS());
                } catch (Exception e) {
                    e.printStackTrace();
                    return true;
                }
                return true;

            case "drops":
                if (parser.hasNext(2)) {
                    final int number = parser.nextInt();

                    final double times = parser.nextDouble();

                    int com = 0;
                    int nul = 0;
                    int uncom = 0;
                    int rare = 0;
                    int exrare = 0;
                    int rdt0 = 0;
                    int rdt1 = 0;
                    int rdt2 = 0;
                    int rdt3 = 0;

                    for (int i = 0; i < times; i++) {
                        final DropTableItem[] drop = DropTable.getDrop(number, player, true);
                        int rdtDrop = DropTable.getRdt(number, player);

                        switch (rdtDrop) {
                            case 0:
                                rdt0++;
                                break;
                            case 1:
                                rdt1++;
                                break;
                            case 2:
                                rdt2++;
                                break;
                            case 3:
                                rdt3++;
                                break;
                            default:
                                break;
                        }
                        for (DropTableItem item : drop) {
                            if (item == null) {
                                nul++;
                                continue;
                            }
                            if (item.getProbability(player) <= 1.0 / DropRate.COMMON.getProbability()
                                    && item.getProbability(player) > 1.0 / DropRate.UNCOMMON.getProbability()) {
                                com++;
                            }
                            if (item.getProbability(player) <= 1.0 / DropRate.UNCOMMON.getProbability()
                                    && item.getProbability(player) > 1.0 / DropRate.RARE.getProbability()) {
                                uncom++;
                            }
                            if (item.getProbability(player) <= 1.0 / DropRate.RARE.getProbability() && item
                                    .getProbability(player) > 1.0 / DropRate.SUPER_RARE.getProbability()) {
                                rare++;
                            }
                            if (item.getProbability(player) <= 1.0 / DropRate.SUPER_RARE.getProbability()) {
                                exrare++;
                            }
                        }
                    }
                    System.out.println("Drops for " + times + " " + NPCHandler.getNpcName(number));
                    System.out.println("[Drops] None: " + ((int) (((nul / times) * 100) * 100)) / 100.0
                            + "%, Common Range: " + ((int) (((com / times) * 100) * 100)) / 100.0 + "%;" + com
                            + ", Uncommon Range: " + ((int) (((uncom / times) * 100) * 100)) / 100.0 + "%;" + uncom
                            + ", Rare range: " + ((int) (((rare / times) * 100) * 100)) / 100.0 + "%;" + rare
                            + ", Extremeley Rare Range: " + ((int) (((exrare / times) * 100) * 100)) / 100.0 + "%;"
                            + exrare);
                    System.out.println("[RDT] None: " + ((int) (((rdt0 / times) * 100) * 100)) / 100.0 + "%, GDT: "
                            + ((int) (((rdt1 / times) * 100) * 100)) / 100.0 + "%;" + rdt1 + ", RDT: "
                            + ((int) (((rdt2 / times) * 100) * 100)) / 100.0 + "%;" + rdt2 + ", SDT: "
                            + ((int) (((rdt3 / times) * 100) * 100)) / 100.0 + "%;" + rdt3);
                    player.sendMessage("[Drops] None: <col=ff>" + ((int) (((nul / times) * 100) * 100)) / 100.0
                            + "%</col>, Common Range: <col=ff>" + ((int) (((com / times) * 100) * 100)) / 100.0 + "%;" + com
                            + "</col>, Uncommon Range: <col=ff>" + ((int) (((uncom / times) * 100) * 100)) / 100.0 + "%;"
                            + uncom + "</col>,");
                    player.sendMessage("Rare range: <col=ff>" + ((int) (((rare / times) * 100) * 100)) / 100.0 + "%;" + rare
                            + "</col>, Extremeley Rare Range: <col=ff>" + ((int) (((exrare / times) * 100) * 100)) / 100.0
                            + "%;" + exrare);
                    player.sendMessage("</col>[RDT] None: <col=ff>" + ((int) (((rdt0 / times) * 100) * 100)) / 100.0
                            + "%</col>, GDT: <col=ff>" + ((int) (((rdt1 / times) * 100) * 100)) / 100.0 + "%;" + rdt1
                            + "</col>, RDT: <col=ff>" + ((int) (((rdt2 / times) * 100) * 100)) / 100.0 + "%;" + rdt2
                            + "</col>, SDT: <col=ff>" + ((int) (((rdt3 / times) * 100) * 100)) / 100.0 + "%;" + rdt3);
                }
                return true;
            case "resetprestige":
                player.difficulty = PlayerDifficulty.NORMAL;
                player.sendMessage("Your account is now normal mode.");
                return true;
            case "killnpcs2":
                for (NPC n : NPCHandler.npcs) {
                    if (n != null && n.goodDistance(player.getX(), player.getY(), 9)) {
                        n.setDead(true);
                        // n.updateRequired = true;
                        player.sendMessage("killing npcs");
                    }
                }
                return true;
            case "killnpcs":
                for (NPC n : NPCHandler.npcs) {
                    if (n != null && n.goodDistance(player.getX(), player.getY(), 9)) {
                        n.applyDead = true;
                        n.setDead(true);
                        // n.updateRequired = true;
                        player.sendMessage("killing npcs");
                    }
                }
                return true;

            case "killcount":
                player.setGwdKillCount(4294967294L);
                player.unpackGwdKc();
                return true;

            case "sendmarket":
                if (parser.hasNext()) {
                    String playerName = parser.nextLine();
                    Optional<Client> result = World.search(playerName);

                    if (!result.isPresent()) {
                        player.sendMessage("Player is offline.");
                        return true;
                    }

                    Client other = result.get();

                    if (other.isInJail() && (!player.getNameSmart().equals(other.getNameSmart()))) {
                    	player.sendMessage("You can't teleport players in jail, use ::unjail instead");
                    	return true;
                    }

                    other.teleportToX = 3164;
                    other.teleportToY = 3474;
                    other.teleportToZ = 0;
                    player.sendMessage("You have teleported " + other.getNameSmartUp() + " to market");
                    other.sendMessage("You have been teleported to market");
                    return true;
                }

                player.sendMessage("Use syntax :sendmarket player name");
                return true;

            case "tele":
                if (parser.hasNext(3)) {
                    final int x = parser.nextInt();
                    final int y = parser.nextInt();
                    final int z = parser.nextInt();

                    player.getPA().movePlayer(x, y, z);
                } else if (parser.hasNext(2)) {
                    final int x = parser.nextInt();
                    final int y = parser.nextInt();

                    player.getPA().movePlayer(x, y, player.heightLevel);
                }

                return true;

            case "getid":
                if (parser.hasNext()) {
                    final String itemName = parser.nextLine();
                    int results = 0;
                    player.sendMessage("Searching: " + itemName);

                    for (int i = 0; i < Config.ITEM_LIMIT; i++) {
                    	ItemDef def = ItemDef.forID(i);
                    	if (def == null) continue;
                    	String name = def.getName();
                        if (name.equals("") || name.equals("No Name")) {
                            continue;
                        }

                        if (name.replace("_", " ").toLowerCase().contains(itemName)) {
                            player.sendMessage("<col=ff0000>" + name.replace("_", " ") + " - " + i);
                            results++;
                        }
                    }

                    player.sendMessage(results + " results found...");
                    return true;
                }
                return true;

            case "getnpcid":
                if (parser.hasNext()) {

                    final String npcName = parser.nextLine();

                    int results = 0;

                    player.sendMessage("Searching: " + npcName);

                    for (int i = 0; i < Config.MAX_NPC_ID; i++) {

                        EntityDef def = EntityDef.forID(i);

                        if (def == null) {
                            continue;
                        }

                        if (def.name == null) {
                            continue;
                        }

                        if (def.name.toLowerCase().contains(npcName.toLowerCase())) {
                            player.sendMessage("<col=ff0000>" + def.name + " - " + i);
                            results++;
                        }

                    }

                    player.sendMessage(results + " results found...");
                    return true;
                }
                return true;

            case "getobjectid":
                if (parser.hasNext()) {

                    final String name = parser.nextLine();

                    int results = 0;

                    player.sendMessage("Searching: " + name);

                    for (int i = 0; i < ObjectDef.objectsSize667; i++) {

                    	ObjectDef def = ObjectDef.forID667(i);

                        if (def == null) {
                            continue;
                        }

                        if (def.name == null) {
                            continue;
                        }

                        if (def.name.toLowerCase().contains(name.toLowerCase())) {
                            player.sendMessage("<col=ff0000>" + def.name + " - " + i);
                            results++;
                        }

                    }

                    player.sendMessage(results + " results found...");
                    return true;
                }
                return true;

            case "item":
                if (parser.hasNext(2)) {
                    final int itemId = parser.nextInt();

                    String input = parser.nextLine().toLowerCase();
                    int multiplier = 1;

                    int pos = input.indexOf("b");

                    if (pos != -1) {
                        input = input.substring(0, pos);
                        multiplier *= 1_000_000_000;
                    }

                    pos = input.indexOf("m");

                    if (pos != -1) {
                        input = input.substring(0, pos);
                        multiplier *= 1_000_000;
                    }

                    pos = input.indexOf("k");

                    if (pos != -1) {
                        input = input.substring(0, pos);
                        multiplier *= 1_000;
                    }

                    int amount = 1;

                    try {
                        amount = Integer.parseInt(input) * multiplier;
                    } catch (Exception ex) {
                        return true;
                    }

                    if (itemId > Config.ITEM_LIMIT || itemId < 0) {
                        player.sendMessage("No such item.");
                        return true;
                    }

                    player.getItems().addItem(itemId, amount);
                    return true;
                } else if (parser.hasNext()) {
                    final int itemId = parser.nextInt();

                    if (itemId > Config.ITEM_LIMIT || itemId < 0) {
                        player.sendMessage("No such item.");
                        return true;
                    }

                    player.getItems().addItem(itemId, 1);
                    return true;
                }
                player.sendMessage("Use syntax ::item id amount");
                return true;

            case "sidebar":
                if (parser.hasNext(2)) {
                    final int menuId = parser.nextInt();
                    final int form = parser.nextInt();

                    player.getPacketSender().setSidebarInterface(menuId, form);
                } else if (parser.hasNext()) {
                    final int form = parser.nextInt();

                    player.getPacketSender().setSidebarInterface(6, form);
                }
                return true;

            case "dh":
                player.getDH().sendDialogues(692, player.npcType);
                return true;

            case "endsw":
                player.sendMessage("end dumb minigame 10 ticks");
                SoulWars.gameTimer = 1;
                return true;

            case "sw1":
                player.sendMessage("minimum players in sw is 1");
                SoulWars.MINIMUM_PLAYERS = 1;
                return true;

            case "sw4":
                player.sendMessage("minimum players in sw is 4");
                SoulWars.MINIMUM_PLAYERS = 4;
                return true;

            case "swlvl":
                SoulWars.blueAvatar.level = 1;
                SoulWars.redAvatar.level = 1;
                return true;

            case "ccpoints":
                player.getClan().addClanPoints(player, 10, "command");
                player.sendMessage("10 points added");
                return true;

            case "bsave":
                if (parser.hasNext()) {

                    final int nodeId = parser.nextInt();

                    if (nodeId < 0) {
                        return true;
                    }

                    LoginServerConnection.instance()
                            .submit(() -> LoginServerConnection.instance().saveAccountsOnWorld(nodeId));
                    player.sendMessage("Sent save request to node ID:" + nodeId);

                }
                player.sendMessage("Use syntax ::bsave integer");
                return true;

            case "lvlids":
            case "levelids":
                player.sendMessage("Attack = 0,   Defence = 1,  Strength = 2, Hitpoints = 3,   Ranged = 4,   Prayer = 5");
                player.sendMessage("Magic = 6,   Cook = 7,   Woodcut = 8, Fletch = 9,   Fish = 10,   Firemaking = 11,");
                player.sendMessage("Craft = 12,   Smith = 13,   Mining = 14, Herb = 15,   Agil = 16,   Thiev = 17,");
                player.sendMessage("Slayer = 18,   Farm = 19,   RC = 20, Dung = 21, Hunter = 22, Summon = 23");
                player.sendMessage("Construction = 24");
                return true;

            case "endfightpits":
                player.sendMessage("Ending Fight Pits");
                FightPitsTournament.endGame();
                return true;

            case "endfunpk":
                player.sendMessage("Ending funpk");
                FunPkTournament.endGame();
                return true;

            case "funpkcenter":
                player.sendMessage("collecting funpk players to center");
                FunPkTournament.collectAllPlayersToCenter();
                return true;

            case "savelottery":
                FPLottery.saveLists();
                player.sendMessage("saving lottery");
                return true;

            case "staking":
                DBConfig.STAKING = !DBConfig.STAKING;
                player.sendMessage("Staking set to " + DBConfig.STAKING);
                return true;

            case "changeduel":
                DBConfig.DUELING = !DBConfig.DUELING;
                player.sendMessage("Dueling set to " + DBConfig.DUELING);
                return true;

            case "checktasks":
                player.sendMessage("Running tasks " + CycleEventHandler.getSingleton().getEventsCount());
                return true;

            case "chekcnpcs":
            case "npccount":
            case "ncount": {
                int count = 0;
                for (NPC npc : NPCHandler.npcs) {
                    if (npc == null) {
                        continue;
                    }
                    count++;
                }
                player.sendMessage("NPCCount is " + count);
            }
            return true;

            case "allincw":
                CastleWars.setWaitTimer(10);
                for (Player p : PlayerHandler.players) {
                    if (p == null || p.disconnected || p.inMinigame()) {
                        continue;
                    }
                    CastleWars.addToWaitRoom((Client) p, 3);
                }
                return true;

            case "decayelo":
                //PKBoards.decayElo();
                return true;

            case "endtask":
                System.out.println("ending task");
                // c.getBot().endSession();
                player.getBot().setAction(null);
                return true;

            case "dcbot":
                player.getBot().disconnect();
                return true;

            case "killbots":
                for (Player p : PlayerHandler.players) {
                    if (p == null) {
                        continue;
                    }
                    if (p.isBot()) {
                        p.disconnected = true;
                    }
                }
                return true;

            case "hello":
                player.sendMessage("followindex " + player.getBot().followId + " my id:" + player.getId());
                return true;

            case "spambots":
                for (int i = 0; i < PlayerHandler.players.length - 10; i++) {
                    player.setBot(new BotClient("rules" + Misc.random(1000), "smartbot123"));
                    player.getBot().connect();
                }
                return true;

            case "togglebot":
                if (player.getBot() == null) {
                    player.sendMessage("You have no bot on");
                    return true;
                }
                if (!player.toggleBot) {
                    player.toggleBot = true;
                    player.sendMessage("Toggling your bot");
                    player.getBot().followId = 0;
                    player.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);

                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerHat], 1,
                            PlayerConstants.playerHat);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerCape], 1,
                            PlayerConstants.playerCape);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerAmulet], 1,
                            PlayerConstants.playerAmulet);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerArrows],
                            player.getBot().playerEquipmentN[PlayerConstants.playerArrows], PlayerConstants.playerArrows);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerChest], 1,
                            PlayerConstants.playerChest);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerShield], 1,
                            PlayerConstants.playerShield);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerLegs], 1,
                            PlayerConstants.playerLegs);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerHands], 1,
                            PlayerConstants.playerHands);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerFeet], 1,
                            PlayerConstants.playerFeet);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerRing], 1,
                            PlayerConstants.playerRing);
                    player.getItems().setEquipment(player.getBot().playerEquipment[PlayerConstants.playerWeapon],
                            player.getBot().playerEquipmentN[PlayerConstants.playerWeapon], PlayerConstants.playerWeapon);

                } else {
                    player.toggleBot = false;
                    player.sendMessage("Bot unToggled");
                    player.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);

                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerHat], 1,
                            PlayerConstants.playerHat);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerCape], 1,
                            PlayerConstants.playerCape);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerAmulet], 1,
                            PlayerConstants.playerAmulet);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerArrows],
                            player.playerEquipmentN[PlayerConstants.playerArrows], PlayerConstants.playerArrows);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerChest], 1,
                            PlayerConstants.playerChest);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerShield], 1,
                            PlayerConstants.playerShield);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerLegs], 1,
                            PlayerConstants.playerLegs);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerHands], 1,
                            PlayerConstants.playerHands);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerFeet], 1,
                            PlayerConstants.playerFeet);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerRing], 1,
                            PlayerConstants.playerRing);
                    player.getItems().setEquipment(player.playerEquipment[PlayerConstants.playerWeapon],
                            player.playerEquipmentN[PlayerConstants.playerWeapon], PlayerConstants.playerWeapon);

                }
                player.flushOutStream();
                return true;

            case "pktarget":
                if (parser.hasNext()) {
                    if (player.getBot() == null) {
                        player.sendMessage("You have no bot on");
                        return true;
                    }
                    String target = parser.nextLine();
                    Client o = (Client) PlayerHandler.getPlayerSmart(target);
                    if (o == null) {
                        player.sendMessage("Player " + target + " does not exist.");
                        return true;
                    }
                    player.sendMessage(
                            "Player " + target + " has been targetted by your bot: " + player.getBot().getName() + ".");
                    player.getBot().setPlayerToKill(o);
                    BotTask task = new UltimatePkTask();
                    player.getBot().setAction(task);
                    task = null;
                    o = null;
                    return true;
                }
                return false;

            case "loginpkbot2":
                if (PlayerHandler.isPlayerOnline("Zezima1")) {
                    player.sendMessage("Zezima is already online.");
                    return true;
                }
                player.setBot(new BotClient("Zezima1a", "asdf"));
                player.getBot().setTypeOfBot(7);
                player.getBot().connect();
                player.getBot().setBotOwner(player.getName());
                player.getBot().setOwnerClient(player);
                player.getBot().getPA().movePlayer(player.getX(), player.getY(), player.getHeightLevel());
                player.getBot().followId = player.getId();
                return true;

            case "loginpkbot":
                if (PlayerHandler.isPlayerOnline("Zezima")) {
                    player.sendMessage("Zezima is already online.");
                    return true;
                }
                player.setBot(new BotClient("Zezima", "asdf"));
                player.getBot().setTypeOfBot(10);
                player.getBot().connect();
                player.getBot().setBotOwner(player.getName());
                player.getBot().setOwnerClient(player);
                player.getBot().getPA().movePlayer(player.getX(), player.getY(), player.getHeightLevel());
                player.getBot().followId = player.getId();
                return true;

            case "followme":
                if (player.getBot() == null) {
                    player.sendMessage("You have no bot on");
                    return true;
                }
                player.getBot().followId = player.getId();
                return true;

            case "hookpm":
                try {
                    boolean found = false;
                    Client c2 = (Client) PlayerHandler.getPlayerSmart(parser.nextString());
                    if (c2 != null) {
                        found = true;
                        c2.listenPM = player.getId();
                    }
                    if (!found) {
                        player.sendMessage("Player not found.");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return true;

            case "pktarget2":
                if (parser.hasNext()) {
                    if (player.getBot() == null) {
                        player.sendMessage("You have no bot on");
                        return true;
                    }
                    final String target = parser.nextLine();
                    final Client o = (Client) PlayerHandler.getPlayerSmart(target);
                    if (o == null) {
                        player.sendMessage("Player " + target + " does not exist.");
                        return true;
                    }
                    player.sendMessage(
                            "Player " + target + " has been targetted by your bot: " + player.getBot().getName() + ".");
                    player.getBot().setPlayerToKill(o);
                    BotTask task = new WelfareGearPkTask();
                    player.getBot().setAction(task);
                    task = null;
                    return true;
                }
                return false;

            case "addspin":
                try {
                    SqlHandler.buySpin(player, Config.WHEEL_TABLE_NAME);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;

            case "hide":
                if (!player.hidePlayer) {
                    player.hidePlayer = true;
                    player.sendMessage("You hide yourself.");
                } else {
                    player.hidePlayer = false;
                    player.sendMessage("You reveal yourself to everyone.");
                }
                return true;

            case "noclip":
                if (!player.noClip) {
                    player.noClip = true;
                    player.sendMessage("noClip " + player.noClip);
                } else {
                    player.noClip = false;
                    player.sendMessage("noClip " + player.noClip);
                }
                return true;

            case "unhide":
                player.hidePlayer = false;
                player.superHide = false;
                player.sendMessage("You have UnHidden yourself.");
                return true;

            case "cleanregions":
                Server.getRegionManager().cleanRegions();
                player.sendMessage("Cleaning regions.");
                return true;

            case "fishingcontest":
                if (!FishingContest.isRunning()) {
                    FishingContest.beginFishingContest();
                    player.sendMessage("You start the fishing contest");
                } else {
                    player.sendMessage("Contest is currently running");
                }
                return true;

            case "pkhour":
                DBConfig.BETTER_HUNT_REWARD_CHANCE = !DBConfig.BETTER_HUNT_REWARD_CHANCE;
                if (DBConfig.BETTER_HUNT_REWARD_CHANCE) {
                    player.sendMessage("You started pk hour!");
                    PlayerHandler.messageAllPlayers("<col=800000>PK Hour has been enabled by an Admin.");
                } else {
                    player.sendMessage("You ended pk hour!");
                    PlayerHandler.messageAllPlayers("<col=800000>PK Hour has been disabled by an Admin.");
                }
                return true;

            case "trackuuid":
                Connection.trackUUID = !Connection.trackUUID;
                player.sendMessage("Tracking UUID:" + Connection.trackUUID);
                return true;

            case "keeplogs":
                Server.toggleKeepLogs();
                player.sendMessage("Keep text logs:" + Server.getKeepLogs());
                return true;

            case "systemlogs":
                Server.toggleSystemLogs();
                player.sendMessage("Keep System logs: " + Server.getSystemLogs());
                return true;

            case "custom":
                player.getPacketSender().addObjectPacket(411, 2340, 9806, 10, 2);
                return true;

            case "spells":
                if (player.playerMagicBook == SpellBook.NORMAL) {
                    player.sendMessage("You switch to ancient magic.");
                    player.setSpellbook(SpellBook.ANCIENT);
                } else if (player.playerMagicBook == SpellBook.ANCIENT) {
                    player.sendMessage("You switch to lunar magic.");
                    player.setSpellbook(SpellBook.LUNARS);
                } else if (player.playerMagicBook == SpellBook.LUNARS) {
                    player.sendMessage("You switch to modern magic.");
                    player.setSpellbook(SpellBook.NORMAL);
                }
                return true;

            case "getip":
            	if (!Config.RUN_ON_DEDI && !player.isDev()) {
            		return true;
            	}
                if (parser.hasNext()) {

                    final String playerName = parser.nextLine();

                    Optional<Client> result = World.search(playerName);

                    if (result.isPresent()) {

                        Client other = result.get();

                        player.sendMessage(other.getNameSmartUp() + " ip is " + other.connectedFrom);
                        return true;

                    } else {
                        player.sendMessage("Player must be offline or does not exist.");
                        return true;
                    }

                }
                player.sendMessage("Use syntax ::getip player name");
                return true;

            case "snoop":
                if (parser.hasNext()) {

                    final String playerToBan = parser.nextLine();

                    Optional<Client> result = World.search(playerToBan);

                    if (result.isPresent()) {

                        Client other = result.get();

                        PrivateMessaging.trackedPlayers.add(other.getName());
                        player.sendMessage("Started snooping on " + other.getNameSmartUp());
                        return true;
                    }

                }
                return true;

            case "unsnoop":
                if (parser.hasNext()) {
                    String playerToBan = parser.nextLine();

                    Optional<Client> result = World.search(playerToBan);

                    if (result.isPresent()) {

                        Client other = result.get();

                        PrivateMessaging.trackedPlayers.remove(other.getName());
                        player.sendMessage("Stopped snooping on " + other.getNameSmartUp());
                        return true;
                    } else {
                        player.sendMessage("Player must be offline or does not exist.");
                        return true;
                    }

                }
                return true;

            case "sendhome":
                if (parser.hasNext()) {

                    String playerToBan = parser.nextLine();

                    Optional<Client> result = World.search(playerToBan);

                    if (result.isPresent()) {

                        Client other = result.get();

                        if (other.isInJail() && (!player.getNameSmart().equals(other.getNameSmart()))) {
                            player.sendMessage("You can't teleport people in jail, use ::unjail instead");
                            return true;
                        }

                        if (other.inMinigame()) {
                            player.sendMessage("They are inside a minigame. Kick them first then jail.");
                            return true;
                        }

                        other.teleportToX = 3096;
                        other.teleportToY = 3468;
                        other.teleportToZ = 0;
                        player.sendMessage("You have teleported " + other.getNameSmartUp() + " to home");
                        other.sendMessage("You have been teleported to home");
                        return true;

                    }

                }
                return true;

            case "addxp":
                if (parser.hasNext(2)) {
                    int skill = parser.nextInt();

                    if (skill < 0) {
                        skill = 0;
                    }

                    if (skill >= player.getPlayerLevel().length) {
                        skill = player.getPlayerLevel().length - 1;
                    }

                    int level = parser.nextInt();

                    player.getPA().addSkillXP(level, skill);
                    return true;
                }
                player.sendMessage("Use syntax ::addxp skill xp");
                return true;

            case "setskill":
            case "setstat":
            case "lvl":
                if (parser.hasNext(3)) {
                    int skill = parser.nextInt();

                    if (skill < 0) {
                        skill = 0;
                    }

                    int level = parser.nextInt();

                    int maxLevel = Skills.maxLevels[skill];
                    if (level > maxLevel) {
                        level = maxLevel;
                    } else if (level < 0) {
                        level = 1;
                    }

                    String playerName = parser.nextLine();

                    Player other = PlayerHandler.getPlayerSmart(playerName);

                    if (other == null) {
                        player.sendMessage("Player is not online: " + playerName);
                        return true;
                    }

                    other.getSkills().setStaticLevel(skill, level);
                    other.calcCombat();
                    return true;
                } else if (parser.hasNext(2)) {
                    int skill = parser.nextInt();

                    if (skill < 0) {
                        skill = 0;
                    }

                    if (skill >= player.getPlayerLevel().length) {
                        skill = player.getPlayerLevel().length - 1;
                    }

                    int level = parser.nextInt();

                    int maxLevel = Skills.maxLevels[skill];
                    if (level > maxLevel) {
                        level = maxLevel;
                    } else if (level < 0) {
                        level = 1;
                    }

                    player.getSkills().setStaticLevel(skill, level);
                    player.calcCombat();
                    return true;
                }
                player.sendMessage("Use syntax ::lvl skill level");
                return true;

            case "master":
                if (parser.hasNext()) {

                    String playerName = parser.nextLine();

                    Player other = PlayerHandler.getPlayerSmart(playerName);

                    if (other == null) {
                        player.sendMessage("Player is offline or does not exist.");
                        return true;
                    }

                    for (int i = 0; i < other.getPlayerLevel().length; i++) {
                        other.getSkills().setStaticLevel(i, Skills.maxLevels[i]);
                    }

                    other.calcCombat();
                    player.sendMessage("Maxed account :" + other.getNameSmartUp());
                    return true;
                }

                for (int i = 0; i < player.getPlayerLevel().length; i++) {
                    player.getSkills().setStaticLevel(i, Skills.maxLevels[i]);
                }
                player.calcCombat();
                player.sendMessage("I have maxed my own skills.");
                return true;
                
            case "noob":
                if (parser.hasNext()) {

                    String playerName = parser.nextLine();

                    Player other = PlayerHandler.getPlayerSmart(playerName);

                    if (other == null) {
                        player.sendMessage("Player is offline or does not exist.");
                        return true;
                    }

                    for (int i = 0; i < other.getPlayerLevel().length; i++) {
                        other.getSkills().setStaticLevel(i, Skills.minLevels[i]);
                    }

                    other.calcCombat();
                    player.sendMessage("Noobed account :" + other.getNameSmartUp());
                    return true;
                }

                for (int i = 0; i < player.getPlayerLevel().length; i++) {
                    player.getSkills().setStaticLevel(i, Skills.minLevels[i]);
                }
                player.calcCombat();
                player.sendMessage("I have Noobed my own skills.");
                return true;

            case "down":
                player.resetAnimations();
                player.npcIndex = 2;
                player.getPA().movePlayer(player.getX(), player.getY(), player.getHeightLevel() - 1);
                return true;

            case "up":
                player.resetAnimations();
                player.npcIndex = 2;
                player.getPA().movePlayer(player.getX(), player.getY(), player.getHeightLevel() + 1);
                return true;

            case "rdrops":
            case "reloaddrops":
                DropTable.reloadDropTables(player);
                player.sendMessage("Drops have been reloaded from CFG");
                return true;

            case "bank":
                player.getPA().openUpBank();
                return true;

            case "interface":
                if (parser.hasNext()) {
                    try {
                        player.getPacketSender().showInterface(parser.nextInt());
                        return true;
                    } catch (Exception e) {
                        player.sendMessage("::interface ####");
                    }
                }
                return true;

            case "overlay":
                if (parser.hasNext()) {
                    try {
                        player.getPA().walkableInterface(parser.nextInt());
                        return true;
                    } catch (Exception e) {
                        player.sendMessage("::overlay ####");
                    }
                }
                return true;

            case "spec":
                player.specAmount = 10000;// 1000.0;
                player.getItems().addSpecialBar(true);
                return true;

            case "dfs":
            	player.setDfsCharge(50);
                return true;

            case "checkeggtimer":
                player.sendMessage("eggtimer: " + player.getEggTimer());
                return true;

            case "loadobj":
            case "loadobjs":
            case "loadobjects":
                ObjectManager.loadObjects(player);
                return true;

            case "reloadobjs":
            case "reloadobj":
            case "reloadobjects":
                ObjectManager.clearCustomObjectList();
                ObjectManager.importCustomObjects();
                player.sendMessage("Reloaded custom objects.");
                return true;

            case "reloadweaponconfig":
                CombatConfigs.reloadConfigs();
                player.sendMessage("reloaded weapon configs from sql");
                return true;

            case "cwend":
                CastleWars.endGame();
                return true;

            case "cw1":
                CastleWars.MINIMUM_PLAYERS = 1;
                return true;

            case "cw2":
                CastleWars.MINIMUM_PLAYERS = 1;
                return true;

            case "cw3":
                CastleWars.MINIMUM_PLAYERS = 3;
                return true;

            case "cw4":
                CastleWars.MINIMUM_PLAYERS = 4;
                return true;

            case "cw5":
                CastleWars.MINIMUM_PLAYERS = 5;
                return true;

            case "cw6":
                CastleWars.MINIMUM_PLAYERS = 6;
                return true;

            case "clw3":
                ClanWarHandler.MIN_PLAYERS_FOR_RANKED = 3;
                return true;

            case "clw4":
                ClanWarHandler.MIN_PLAYERS_FOR_RANKED = 4;
                return true;

            case "clw5":
                ClanWarHandler.MIN_PLAYERS_FOR_RANKED = 5;
                return true;

            case "clw6":
                ClanWarHandler.MIN_PLAYERS_FOR_RANKED = 6;
                return true;

        }

        return false;

    }

    @Override
    public boolean canAccess(Client player) {
        return player.isAdmin() || Config.isOwner(player);
    }

}