package com.soulplay.content.player.commands;

import java.util.Optional;

import com.soulplay.content.event.randomevent.SpawningStar;
import com.soulplay.content.event.randomevent.WildyWyrm;
import com.soulplay.content.minigames.FightPitsTournament;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.World;

public final class SemiAdminCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {
		switch(parser.getCommand()) {
		
		case "giveinfo":
			if (parser.hasNext()) {
				final String playerName = parser.nextLine();
				
				final Optional<Client> result = World.search(playerName);
				
				if (result.isPresent()) {
					
					Client other = result.get();
					
					other.playerRights = 7;
					other.disconnected = true;
					player.sendMessage("You've promoted to informer the user:  " + other.getNameSmartUp() + " IP: " + other.connectedFrom);
					return true;
				} else {
					player.sendMessage("Player is either offline or does not exist.");
					return true;
				}

			}
			player.sendMessage("Use syntax ::giveinfo player name");
			return true;
             
		case "givemod":
			if (parser.hasNext()) {
				final String playerName = parser.nextLine();
				
				final Optional<Client> result = World.search(playerName);
				
				if (result.isPresent()) {
					
					Client other = result.get();
					
					other.playerRights = 1;
					other.disconnected = true;
					player.sendMessage(
							"You've promoted to moderator the user:  " + other.getNameSmartUp()
									+ " IP: " + other.connectedFrom);
					return true;					
				} else {
					player.sendMessage("Player is either offline or does not exist.");
					return true;
				}
			}
			player.sendMessage("Use syntax ::givemod player name");
			return true;
		
		case "tele":
            if (parser.hasNext(3)) {
                final int x = parser.nextInt();
                final int y = parser.nextInt();
                final int z = parser.nextInt();

                player.getPA().movePlayer(x, y, z);
            } else if (parser.hasNext(2)) {
                final int x = parser.nextInt();
                final int y = parser.nextInt();

                player.getPA().movePlayer(x, y, player.heightLevel);
            }

            return true;
            
		case "startsw":
            player.sendMessage("start sw");
            SoulWars.waitTimer = 1;
            return true;
         
        case "spawnworm":
            WildyWyrm.spawnWorm();
            player.sendMessage("You spawn the wildywyrm!");
            return true;

		case "spawnstar":
            SpawningStar.spawnStar();
            player.sendMessage("You spawn the shooting star!");
            return true;

        case "startfightpits":
            player.sendMessage("Starting Fight Pits");
            FightPitsTournament.beginGame();
            return true;

        case "startfunpk":
            player.sendMessage("Starting funpk");
            FunPkTournament.beginGame();
            return true;
            
        case "cwstart":
            CastleWars.startGame();
            return true;
            
        case "allxp":
        case "allexp":
            for (Player p : PlayerHandler.players) {

                if (p == null) {
                    continue;
                }

                p.setDoubleExpLength(p.getDoubleExpLength()+6000);
                p.sendMessage("You have received Double EXP for one hour from a developer.");
            }
            return true;
		}
		
		return false;
		
		
	}

	@Override
	public boolean canAccess(Client player) {
		return (player.isMod() && player.isSemiAdmin()) || player.isAdmin() || player.isOwner();
	}

}

