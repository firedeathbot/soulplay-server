package com.soulplay.content.player.commands;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.event.PkingPotEvent;
import com.soulplay.content.items.pos.BuyOffers;
import com.soulplay.content.items.presets.PresetsInterface;
import com.soulplay.content.minigames.FPLottery;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.penguin.PenguinUtil;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.treasuretrails.TreasureTrails;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.achievement.SkillCapes;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.DropRate;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.DropTableEntry;
import com.soulplay.game.model.npc.DropTableItem;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.RarityGroup;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.blacklist.Blacklist;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.wrapper.Empty;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.World;
import com.soulplay.game.world.entity.Location;
import com.soulplay.net.Connection;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.sql.payment.ClaimPayments;
import com.soulplay.util.sql.payment.ClaimWheelReward;
import com.soulplay.util.sql.rewards.CodeRedeem;
import com.soulplay.util.sql.scoregrab.WildyEloRatingSeason;

public final class PlayerCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {
		final String command = parser.getCommand();
		switch (command) {
			case "presets": {
				PresetsInterface.open(player);
				return true;
			}
			case "date":
			case "day": {
				int day = SoulPlayDate.getDate();
				player.sendMessage("The current SoulPlay Day is " + day);
				return true;
			}
			case "chins":
			case "chin":
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(3138 + Misc.random(2), 3783 + Misc.random(2), 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			case "lb":
				player.getPA().startTeleport(3323, 3519, 0, TeleportType.MODERN);
				return true;
			case "ardy": {
				if (player.getPA().startTeleport(9070, 3290, 0, TeleportType.MODERN)) {
					player.sendMessage("Make sure to Enable 'Render All Floors' in Settings.");
				}
				
				return true;
			}
			case "attackspeed": {
				player.sendMessage("My attack speed is : " + player.getCombat().getAttackDelay(player.getAutoCastSpell()));
				return true;
			}
			case "west":
			case "wests": {
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(2979 + Misc.random(2), 3597 + Misc.random(2), 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "east":
			case "easts": {
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(3351 + Misc.random(2), 3674 + Misc.random(2), 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "44s": {
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(2975, 3873, 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "50s": {
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(3304, 3912, 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "gdz": {
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(3288, 3886, 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "graves": {
				player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
					player.getPA().startTeleport(3147, 3666, 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "mb":
			case "magebank": {
				player.getPA().startTeleport(2538, 4716, 0, TeleportType.MODERN);
				return true;
		    }
			case "rev":
            case "revs": {
    			player.getDialogueBuilder().sendOption("Teleport to <col=ff0000>Wilderness</col>? You can be attacked there!", "Yes, I'm not afraid.", () -> {
    				player.getPA().startTeleport(3129, 3832, 0, TeleportType.MODERN);
				}, "No, too scared.", () -> {}).execute();
				return true;
			}
			case "pussylist":
			case "trihardlist":
			case "blacklist": {
				Blacklist.open(player);
				return true;
			}
			case "scammed": {
				player.sendMessage("Attempting to send report.");
				player.getGambleLogger().sendReport();
				return true;
			}
		case "vobj": {
			ObjectManager.loadObjects(player);
			player.sendMessage("reloaded spawned objects");
			return true;
		}
		
			case "dungseed": {
				if (!player.insideDungeoneering()) {
					return true;
				}

				player.sendMessage("Your dungeon seed is " + player.dungParty.dungManager.dungeon.seed);
				return true;
			}
			case "fhouse":
				if (parser.hasNext()) {
					final String username = parser.nextLine();

					Optional<Client> result = World.search(username);

					if (!result.isPresent()) {
						break;
					}

					Client owner = result.get();

					player.getPOH().enter(owner);
				}
				return true;
				
			case "tour": {
				if (player.summoned != null) {
					player.sendMessage("You cannot bring any pets/summons inside.");
					return true;
				}
				player.getPA().startTeleport(LocationConstants.PK_TOURNAMENT_SPECTATE.getX(), LocationConstants.PK_TOURNAMENT_SPECTATE.getY(), LocationConstants.PK_TOURNAMENT_SPECTATE.getZ(), TeleportType.FAIRY);
				player.sendMessage("Welcome to the PK Tournament Arena.");
				return true;
			}
			
			case "join": {
				if (player.getPA().startTeleport(3086, 3497, 0, TeleportType.MODERN)) {
					player.getDialogueBuilder().reset();
					player.getDialogueBuilder().sendStatement("Bank all your gear and talk to the Sphinx", "to join the tournament.").execute();
				}
//				Tournament.requestToJoinLobby(player);
				return true;
			}
		
		case "dp":
			player.getPA().startTeleport(2738 + Misc.random(2), 3480 + Misc.random(2), 0, TeleportType.MODERN);
			return true;
			
		case "mypos":
			int baseX = player.mapRegionX << 3;
			int baseY = player.mapRegionY << 3;
			player.sendMessage("MapRegionX: " + baseX);
			player.sendMessage("MapRegionY: " + baseY);
			player.sendMessage("X: " + player.absX+ ", " + (player.absX - baseX) + ", " + (player.absX & 0x3f));
			player.sendMessage("Y: " + player.absY +", " + (player.absY - baseY) + ", " + (player.absY & 0x3f));
			player.sendMessage("H: " + player.heightLevel + ", region: " + (((player.absX >> 6 & 0xff) << 8) + ((player.absY >> 6) & 0xff)));
			if (player.insideInstance()) {
				player.sendMessage("Instance: " + player.getLocationInstance());
			}
			return true;
		
		case "offer":
			player.getMarketOffer().openBuyOffer(0);
			return true;
			
		case "boffer":
			BuyOffers.removeAllBuyOffers(player);
			return true;

		case "hint":
			PenguinUtil.generateHint(player);
			return true;

		case "checkevents":
		case "checkevent":
			player.sendMessage(CastleWars.EXTRA_REWARD ? "Castle wars event is currently <col=ff00>ON" : "Castle wars event is currently <col=ff0000>OFF");
			player.sendMessage(SoulWars.EXTRA_REWARD ? "Soulwars event is currently <col=ff00>ON" : "Soulwars event is currently <col=ff0000>OFF");
			player.sendMessage(PestControl.EXTRA_REWARD ? "Pest control is currently <col=ff00>ON" : "Pest control is currently <col=ff0000>OFF");
			player.sendMessage(Player.edgeWildyEvent ? "Edge PK event is currently <col=ff00>ON" : "Edge PK event is currently <col=ff0000>OFF");
			player.sendMessage(Player.clanWarsWildyEvent ? "Clan Wars PK event is currently <col=ff00>ON" : "Clan Wars PK event is currently <col=ff0000>OFF");
			player.sendMessage(LmsManager.EXTRA_REWARD ? "Last Man Standing event is currently <col=ff00>ON" : "Last Man Standing event is currently <col=ff0000>OFF");
			//player.sendMessage(WildyEloRatingSeason.isTournamentOpen() ? "PKing tournament is currently <col=ff00>ON" : "PKing tournament is currently <col=ff0000>OFF");
			if (PkingPotEvent.checkPot() > 0) {
				player.sendMessage("Pking pot is currently at <col=ff00>" + Misc.formatNumbersWithCommas(PkingPotEvent.checkPot()) + " gold</col>. Kill players in wild to obtain some of it.");
			}
			return true;

		case "tpaccept":
			if (!player.getPA().allowTPA()) {
				return true;
			}
			if (player.tpaReqId == 0) {
				player.sendMessage("You have no requests.");
				return true;
			}
			if (PlayerHandler.players[player.tpaReqId] == null) {
				player.sendMessage("The other player is offline.");
				player.tpaReqId = 0;
				return true;
			} else {
				final Client o = (Client) PlayerHandler.players[player.tpaReqId];
				if (!o.getPA().allowTPA()) {
					return true;
				}
				if (o.tpRequested.equals(player.getName())) {
					if (o.inCw() || o.inCwWait() || o.inBossRoom || o.inFightCaves() || o.duelFightArenas() || o.inPits
							|| o.inPcGame() || o.inPcBoat() || o.inMinigame()) {
						player.sendMessage("The other player is currently in a minigame. Request expires.");
						player.tpaReqId = 0;
						return true;
					}
					player.sendMessage("You have teleported to " + o.getNameSmartUp());
					o.sendMessage(player.getNameSmartUp() + " has accepted your teleport request.");
					o.getPA().movePlayer(player.getX(), player.getY(), player.heightLevel);
					player.tpaReqId = 0;
				} else {
					player.sendMessage("The request has expired.");
					player.tpaReqId = 0;
				}
			}
			return true;

		case "tpa":
			if (parser.hasNext() && DBConfig.TPA) {
				if (!player.getPA().allowTPA()) {
					return true;
				}
				try {
					if (parser.arguments() < 1) {
						player.sendMessage("You must enter a player name. Ex: tpa PlayerName");
						return true;
					}
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(parser.nextLine())) {
								final Client o = (Client) PlayerHandler.players[i];
								if (!o.getPA().allowTPA()) {
									return true;
								}
								player.sendMessage("Sending " + o.getNameSmartUp() + " a teleport request.");
								player.tpRequested = o.getName();
								o.tpaReqId = player.getId();

								o.sendMessage(player.getNameSmartUp()
										+ " has sent you a teleport request. To accept request type ::tpaccept");
								return true;
							}
						}
					}
					player.sendMessage("Player must be offline.");
					return true;
				} catch (Exception ex) {
					return true;

				}
			}
			return true;

		case "maxhit":
			player.displayMaxHit = !player.displayMaxHit;
			player.sendMessage("Display Max Hit set " + player.displayMaxHit);
			return true;

		case "accuracy":
			player.displayAccuracyInfo = !player.displayAccuracyInfo;
			player.sendMessage("Display Accuracy info set to : " + player.displayAccuracyInfo);
			return true;
			
		case "yell":
			if (parser.hasNext()) {
				return PlayerHandler.handleYell(player, " " + parser.nextLine(), true);
			}
			return false;

		case "//":
			if (parser.hasNext()) {
				return PlayerHandler.handleYell(player, " " + parser.nextLine(), false);
			}
			return false;

		case "/":
			if (parser.hasNext()) {
				if (Connection.isMuted(player) || player.getTimedMute() > 0) {
					player.sendMessage("You are muted for breaking a rule.");
					return true;
				}

				if (player.getClan() != null) {
					String message = Misc.escape(parser.nextLine());
					if (Misc.isBad(message)) {
						player.getPA().spamWarning();
						if (!Connection.isMuted(player)) {
							Server.getSlackApi().call(SlackMessageBuilder.buildAdvertiserMessage(player.getName(),
									message, "Clan (" + player.getClan().founder + ")"));
						}
					}
					player.getClan().sendChat(player, message);
				} else {
					player.sendMessage("You can only do this in a clan chat..");
				}
				return true;
			}
			return false;

		case "empty":
			if (!player.isDev() && (player.inWild() || player.isInLmsArena())) {
				player.sendMessage("You can't use ::empty here.");
				return true;
			}
			if (player.insideDungeoneering() && Config.RUN_ON_DEDI) {
				player.sendMessage("Can't do this in dungeoneering.");
				return true;
			}
			player.getDialogueBuilder().sendOption("Yes, I want to empty my inventory items.", () -> {
				if (player.getItems().freeSlots() == player.playerItems.length) {
					return;
				}
				int size = player.playerItems.length;
				int[] playerItems = new int[size];
				int[] playerItemsN = new int[size];
				for (int i = 0; i < size; i++) {
					playerItems[i] = player.playerItems[i]-1;
					playerItemsN[i] = player.playerItemsN[i];
				}
				if (player.getItems().freeSlots() < 28 && DBConfig.LOG_EMPTY) {
					Server.getLogWriter().addToEmptyList(new Empty(player, playerItems, playerItemsN));
				}
				player.sendMessage("You successfully emptied your inventory.");
				player.getItems().removeAllItems();
			}, "No, I want to keep my inventory items", () -> {
				player.getPA().closeAllWindows();
			}).execute();
			/*player.getDH().sendOption2("Yes, I want to empty my inventory items.",
					"No, i want to keep my inventory items.");
			player.dialogueAction = 162;*/
			return true;
		case "age":
			long diff = Calendar.getInstance().getTimeInMillis() - player.getJoinedDate().getTime();
			player.sendMessage("Your account is " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " days old!");
			return true;
		case "rest":
		case "sit":
			player.getMovement().startResting();
			return true;

		case "npckills":
			player.sendMessage("You killed " + player.npcKills + " Npcs");
			return true;

		case "joinsw":
			player.getPA().startTeleport(1886, 3178, 0, TeleportType.MODERN);
			return true;

		case "players":
			// c.sendMessage("There are currently <col=ff>" +
			// PlayerHandler.getPlayerCount() + "</col> players online.");
			int totalCount = 0;
			for (int i = 0; i < PlayerHandler.getNodePlayerCount().length; i++) {
				totalCount += PlayerHandler.getNodePlayerCount()[i];
			}
			player.sendMessage("There are currently <col=ff>" + totalCount + "</col> players online.");
			if (PlayerHandler.getNodePlayerCount()[0] > 0) {
				player.sendMessage(
						"[World 1]: <col=ff>" + PlayerHandler.getNodePlayerCount()[0] + "</col> players online.");
			}
			if (PlayerHandler.getNodePlayerCount()[1] > 0) {
				player.sendMessage(
						"[World 2]: <col=ff>" + PlayerHandler.getNodePlayerCount()[1] + "</col> players online.");
			}
			if (PlayerHandler.getNodePlayerCount()[2] > 0) {
				player.sendMessage(
						"[World 3]: <col=ff>" + PlayerHandler.getNodePlayerCount()[2] + "</col> players online.");
			}
			if (PlayerHandler.getNodePlayerCount()[3] > 0) {
				player.sendMessage(
						"[World 4]: <col=ff>" + PlayerHandler.getNodePlayerCount()[3] + "</col> players online.");
			}
			if (PlayerHandler.getNodePlayerCount()[4] > 0) {
				player.sendMessage("[Spawn]: <col=ff>" + PlayerHandler.getNodePlayerCount()[4] + "</col> players online.");
			}
			if (PlayerHandler.getNodePlayerCount()[5] > 0) {
				player.sendMessage(
						"[World 6]: <col=ff>" + PlayerHandler.getNodePlayerCount()[5] + "</col> players online.");
			}
			if (PlayerHandler.getNodePlayerCount()[6] > 0) {
				player.sendMessage(
						"[World 7]: <col=ff>" + PlayerHandler.getNodePlayerCount()[5] + "</col> players online.");
			}
			if (PlayerHandler.getWildyCount() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getWildyCount() + " players in the Wild.");
			}
			if (PlayerHandler.getPlayersInCastleWars() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInCastleWars() + " players in CastleWars. Next Game Starts in "+CastleWars.getNextGameMinutes()+" minutes.");
			}
			if (PlayerHandler.getPlayersInSoulWars() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInSoulWars() + " players in SoulWars. Next Game Starts in "+ SoulWars.getTimeRemainingInMunites() +" Minutes.");
			}
			if (PlayerHandler.getPlayersInClanWars() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInClanWars() + " players in the Clan Wars.");
			}
			if (PlayerHandler.getPlayersInLMS() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInLMS() + " players at Last Man Standing minigame. Next Game Starts in " + LmsManager.getTimeRemainingInMinutes() + " minutes.");
			}
			if (PlayerHandler.getPlayersInPestControl() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInPestControl() + " players in Pest Control.");
			}
			if (PlayerHandler.getPlayersInDuelArena() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInDuelArena() + " players in the Duel Arena.");
			}
			if (PlayerHandler.getPlayersInGamblingArea() > 0) {
				player.sendMessage("There are currently " + PlayerHandler.getPlayersInGamblingArea() + " players in Gambling area.");
			}

			return true;

		case "toggle":
			if (!player.getCmbExpLock()) {
				player.setCmbExpLock(true);
				player.playerIsMember(4);
				player.sendMessage("Your Combat experience is now locked. You will not gain Combat experience.");
			} else {
				player.playerIsMember(3);
				player.setCmbExpLock(false);
				player.sendMessage("Your Combat experience is now unlocked. You will gain Combat experience.");
			}
			return true;

		case "resettask":
			// c.setTasksCompleted(0);
			// c.getSlayerTask().cancel(c);
			player.sendMessage("Talk to a slayer master to reset your current task.");
			// c.getPacketSender().sendFrame126("<col=ffffff>Task: <col=ff00>Empty", 7383);
			return true;

		case "invite":
			if (parser.hasNext()) {
				// for (int i = 0; i < PlayerHandler.players.length; i++) {
				// if(PlayerHandler.players[i] != null) {
				// if(PlayerHandler.players[i].getName().equalsIgnoreCase(args[1]))
				// {
				// Client p =(Client) PlayerHandler.players[i];
				Client p = (Client) PlayerHandler.getPlayerSmart(parser.nextLine());
				if (p != null) {
					Slayer.invitePlayer(player, p);
				}
				// return true;
				// }
				// }
				// }
				return true;
			}
			return false;

		case "resetcombat":
			if (!WorldType.equalsType(WorldType.ECONOMY)) {
				player.sendMessage("You can't use this command in this world.");
				return true;
			}

			if (player.inWild()) {
				player.sendMessage("You can't use this command in the Wilderness.");
				return true;
			}

			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int level = 1;
				for (int i = 0; i < 7; i++) {
					// if (i == 5) continue; // don't reset prayer?
					if (i == 3) {
						level = 10;
					}
					player.getSkills().setStaticLevel(i, level);
					level = 1;
				}
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "resetdef":

			if (WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.inWild()) {
				return true;
			}
			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int skill = 1;
				int level = 1;
				player.getSkills().setStaticLevel(skill, level);
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "resetatt":
			if (WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.inWild()) {
				return true;
			}
			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int skill = 0;
				int level = 1;
				player.getSkills().setStaticLevel(skill, level);
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "resetstr":
			if (WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.inWild()) {
				return true;
			}
			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int skill = 2;
				int level = 1;
				player.getSkills().setStaticLevel(skill, level);
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "resetpray":

			if (WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.inWild()) {
				return true;
			}
			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int skill = 5;
				int level = 1;
				player.getSkills().setStaticLevel(skill, level);
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "resetrange":

			if (WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.inWild()) {
				return true;
			}
			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int skill = 4;
				int level = 1;
				player.getSkills().setStaticLevel(skill, level);
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "resetmage":

			if (WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.inWild()) {
				return true;
			}
			for (int element : player.playerEquipment) {
				if (element > 0) {
					player.sendMessage("Please take all your armour and weapons off before using this command.");
					return true;
				}
			}
			try {
				int skill = 6;
				int level = 1;
				player.getSkills().setStaticLevel(skill, level);
				player.calcCombat();
				player.getCombat().resetPrayers();
			} catch (Exception e) {
				return true;
			}
			return true;

		case "changepassword":
			player.getPacketSender().openUrl("https://www.soulplayps.com/ucp");
			player.sendMessage("Log out, then change your password online.");
			return true;

		case "status":
			player.sendMessage("Current server state is: <col=" + Server.getSystemMonitor().getStatusColor() + ">"
					+ Server.getSystemMonitor().getStatus() + "</col>");
			return true;

		case "forums":
		case "updates":
			player.getPacketSender().openUrl(Config.FORUMS);
			return true;
			
		case "suggest":
			player.getPacketSender().openUrl(Config.URLS[16]);
			return true;

		case "paypal":
			player.getPacketSender().openUrl(Config.URLS[3]);
			return true;

		case "facebook":
			player.getPacketSender().openUrl(Config.FACEBOOK);
			return true;

		case "youtube":
			player.getPacketSender().openUrl(Config.URLS[4]);
			return true;

		case "twitter":
			player.getPacketSender().openUrl(Config.URLS[6]);
			return true;

		case "ucp":
			player.getPacketSender().openUrl(Config.URLS[11]);
			return true;

		case "vote":
			VoteInterface.openVotePage(player);
			return true;

		case "donate":
			if (WorldType.equalsType(WorldType.SPAWN)) {
				player.getPacketSender().openUrl(Config.URLS[10]);
				return true;
			}

			player.getPacketSender().openUrl(Config.URLS[3]);
			return true;

		case "rsdonate":
			player.getPacketSender().openUrl(Config.URLS[15]);
			return true;

		case "sof":
			player.getPacketSender().openUrl(Config.URLS[9]);
			return true;

		case "promo":
			player.getPacketSender().openUrl(Config.URLS[12]);
			return true;

		case "hiscore":
		case "highscores":
		case "highscore":
			player.getPacketSender().openUrl(Config.URLS[1]);
			return true;

		case "wiki":
			player.getPacketSender().openUrl(Config.URLS[8]);
			return true;

		case "slotmachine":
			if (!WorldType.equalsType(WorldType.ECONOMY)) {
				player.sendMessage("You can only use this command in economy world.");
				return true;
			}
			player.getPA().startTeleport(3195, 3411, 0, TeleportType.MODERN);
			return true;

		case "discord":
			player.getPacketSender().openUrl(Config.URLS[14]);
			return true;
			
		case "mark":
			player.getPA().startTeleport(2995, 3374, 0, TeleportType.MODERN);
			return true;

		case "market":
			player.getPA().startTeleport(3164, 3474, 0, TeleportType.MODERN);
			player.sendMessage(
					"<col=800000>Market commands - <col=ff>::collect<col=800000> - <col=ff>::myshop<col=800000> - <col=ff>::advert<col=800000> - <col=ff>::search<col=ff0000> ITEMID<col=800000> - <col=ff>::find <col=ff0000>NAME");
			player.sendMessage("Selling from your own shop costs 2% from the total price!");
			return true;

		case "dice":
		case "gamble":
			player.getPA().startTeleport(1444, 4457, 0, TeleportType.MODERN);
			//player.getPA().openGambleRules();
			return true;

		case "bavatar":
			player.getPA().startTeleport(2912, 4765, 0, TeleportType.MODERN);
			return true;
		case "announcements":
			player.toggleAnnouncements();
			AnnouncementHandler.updateAnnouncement(player);
			if (player.getVariables()[Variables.ANNOUNCEMENT_MUTE.ordinal()] == 1) {
				player.sendMessage("You wont see any announcements from now on.");
			} else {
				player.sendMessage("You'll see announcements from now on.");
			}
			return true;
		case "home":
			player.getPA().startHomeTeleportCommand();
			return true;

		case "sw":
			player.getPA().startTeleport(1886, 3178, 0, TeleportType.MODERN);
			return true;

		case "cw":
			player.getPA().startTeleport(2440, 3089, 0, TeleportType.MODERN);
			return true;

		case "clw":
			player.getPA().startTeleport(LocationConstants.CLAN_WARS.getX(), LocationConstants.CLAN_WARS.getY(), LocationConstants.CLAN_WARS.getZ(), TeleportType.MODERN);
			return true;
			
		case "lms": {
			final Location loc = LmsManager.getLobbyRandom();
			player.getPA().startTeleport(loc.getX(), loc.getY(), 0, TeleportType.MODERN);
			return true;
		}

		case "tz":
			player.getPA().startTeleport(2437, 5171, 0, TeleportType.MODERN);
			return true;

		case "funpk":
			player.getPA().startTeleport(3303, 3123, 0, TeleportType.MODERN);
			return true;

		case "skill":
			player.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
			return true;

		case "fishing":
			player.getPA().startTeleport(2604, 3414, 0, TeleportType.MODERN);
			return true;

		case "mining":
			player.getPA().startTeleport(3046, 9751, 0, TeleportType.MODERN);
			return true;

		case "crabs":
			player.getPA().startTeleport(2679, 3718, 0, TeleportType.MODERN);
			return true;

		case "event":
			player.getPA().startTeleport(2996, 3265, 0, TeleportType.MODERN);
			return true;

		case "duel":
			if (player.underAttackBy > 0 || player.underAttackBy2 > 0) {
				player.sendMessage("You cannot teleport to duel when in combat.");
				return true;
			}
			player.getPA().startTeleport(3365, 3265, 0, TeleportType.MODERN);
			player.getPA().openDuelRules();
			return true;

		case "veng":
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}
			player.getItems().addItem(9075, 4000);
			player.getItems().addItem(560, 2000);
			player.getItems().addItem(557, 10000);
			return true;

		case "runes":
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.wildLevel >= 1) {
				return true;
			}

			player.getItems().addItem(554, 1000);
			player.getItems().addItem(555, 1000);
			player.getItems().addItem(556, 1000);
			player.getItems().addItem(557, 1000);
			player.getItems().addItem(560, 1000);
			player.getItems().addItem(561, 1000);
			player.getItems().addItem(562, 1000);
			player.getItems().addItem(563, 1000);
			player.getItems().addItem(565, 1000);
			player.getItems().addItem(566, 1000);
			player.getItems().addItem(9075, 1000);
			return true;

		case "barrage":
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.wildLevel >= 1) {
				return true;
			}

			player.getItems().addItem(555, 6000);
			player.getItems().addItem(560, 4000);
			player.getItems().addItem(565, 2000);
			return true;

		case "tb":
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.wildLevel >= 1) {
				return true;
			}

			player.getItems().addItem(562, 1000);
			player.getItems().addItem(563, 1000);
			player.getItems().addItem(560, 1000);
			return true;

		case "superset":
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.wildLevel >= 1) {
				return true;
			}

			player.getItems().addItem(2440, 1);
			player.getItems().addItem(2436, 1);
			player.getItems().addItem(2442, 1);
			return true;

		case "food":
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				return true;
			}

			if (player.wildLevel >= 1) {
				return true;
			}

			player.getItems().addItem(15272, 28);
			return true;

		case "barrow":
		case "barrows":
			player.getPA().startTeleport(3565, 3304, 0, TeleportType.MODERN);
			return true;

		case "train":
			// c.getPA().startTeleport(2520, 4777, 0, TeleportType.MODERN);
			player.getPA().startTeleport(2679, 3718, 0, TeleportType.MODERN);
			return true;

		case "clans":
			player.getPacketSender().openUrl(Config.URLS[13]);
			return true;

		case "commands":
			player.getPA().openCommandsInfo();
			return true;

		case "crushvials":
		case "breakvials":
			player.toggleBreakVials();
			return true;
			
		case "threads":
			player.getPA().openThreadsInterface();
			return true;

		case "skull":
			player.skullPlayer();
			return true;
			
		case "redskull": {
			if (player.getZones().isInClwRedPortalPkZone()) {
				if (player.isRedSkulled())
					return true;
				player.setRedSkull(true);
				player.sendMessage("Red Skull turned on.");
				CombatPrayer.resetPrayers(player);
				player.sendMessage("Resetting your prayer for Red Skull.");
			} else {
				player.sendMessage("You can only use this inside Clan Wars Red Portal zone.");
			}
			return true;
		}

		case "xpoff":
		case "expoff":
			player.setDoubleExpLength(0);
			player.sendMessage("You have removed your double experience.");
			return true;

		case "toggleallxp":
		case "toggleallexp":
			if (player.expLock == false) {
				player.expLock = true;
				player.sendMessage("Your experience is now locked. You will not gain experience.");
			} else {
				player.expLock = false;
				player.sendMessage("Your experience is now unlocked. You will gain experience.");
			}
			return true;

		case "rag":
			if (player.underAttackBy > 0 || player.playerIndex > 0) {
				player.sendMessage("You cannot use this command in combat.");
				return true;
			}
			if (!player.isToggleRag() && player.wildLevel > 0) {
				player.sendMessage("You can only use this command outside of wild.");
				return true;
			}
			
			player.setToggleRag(!player.isToggleRag());
			if (player.isToggleRag()) {
				player.sendMessage("The raglord is protecting you in low wildy.");
			} else {
				player.sendMessage("The raglord is no longer protecting you in low wildy.");
			}
			return true;

		case "fd":
			player.getPA().startTeleport(3007, 9550, 0, TeleportType.MODERN);
			return true;

		case "tds":
			player.getPA().startTeleport(LocationConstants.TORM.getX(), LocationConstants.TORM.getY(), 0, TeleportType.MODERN);
			return true;
			
		case "claimcode": {
			if (!player.inEdgeville()) {
				player.sendMessage("You must be in Edgeville to claim rewards.");
				return true;
			}
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement("Sent Claim Request.").execute();
			CodeRedeem.checkForCodes(player);
			return true;
		}

		case "claimvote":
		case "claimvotes":
			VoteInterface.claimVotes(player);
			return true;

		case "claim":
			if (!player.inEdgeville()) {
				player.sendMessage("You must be in Edgeville to claim points.");
				return true;
			}
			
			if (Config.isPvpWorld()) {
				player.sendMessageSpam("You cannot use this command in PVP world.");
				return true;
			}

			if (System.currentTimeMillis() - player.lastWhatDropCommand >= 10000) {
				if (player.getItems().freeSlots() < 12) {
					player.sendMessage("You must have at least 12 slots available in your Inventory.");
					return true;
				}
				if (player.underAttackBy > 0 || player.underAttackBy2 > 0) {
					player.sendMessage("You are currently under attack. Can't claim at the moment.");
					return true;
				}

				WildyEloRatingSeason.claimReward(player);
				
				try {
					player.startLogoutDelay();
					player.sendMessage("Checking for Donations.");
					ClaimPayments.claimDonationOverWebSQL(player);
				} catch (Exception e) {
					e.printStackTrace();
					player.sendMessage("The database is unreachable at the moment. Please try again later.");
				}

				player.sendMessage("Checking unclaimed Lottery prizes.");
				FPLottery.checkUnclaimedWinners(player);

				player.lastWhatDropCommand = System.currentTimeMillis();
			} else {
				player.sendMessage("You must wait 10 seconds before using the command again.");
			}
			return true;
			
		case "npcdrop":
		case "npcdrops": {
			player.sendMessage("Opening NPC Drop table.");
			player.getPacketSender().openUrl("https://www.soulplayps.com/npc-drops/");
			return true;
		}

		case "checkdrop":
		case "checkdrops":
			try {
				if (parser.hasNext()) {

					final String npcName = parser.nextLine().trim();

					int npcId = -1;

					try {
						npcId = Integer.parseInt(npcName);
					} catch (Exception ex) {
						npcId = NPCHandler.getNpcIdForCheckDrops(npcName);
					}

					if (npcId < 0) {
						return true;
					}
					
					boolean isRev = npcId >= 13465 && npcId <= 13481;

					DropTableEntry entry = DropTable.getEntry(npcId);

					if (entry == null) {
						if (player.playerRights == 2 || Config.isOwner(player)) {
							player.sendMessage(String.format("<col=ff0000>No drops %s id=%d", npcName, npcId));
						} else {
							player.sendMessage(String.format("<col=ff0000>Could not find any drops for %s %s",
									Misc.aOrAn(npcName), npcName));
						}
						return true;
					}

					player.sendMessage("Drops for Monster: <col=ff>" + npcName);
					player.sendMessage("Group rarities (Chance to get loot in rarity): ");

					for (RarityGroup group : entry.getRarityGroups()) {
						double prob = group.getProbability(player);
						DropRate rate = DropRate.getRate(prob);
						player.sendMessage("<shad=000000>Group <col=ff0000>Raw: <col=ff>"
								+ ((int) (1.0 / group.getRawProbability() * 100000)) / 1000.0D
								+ "</col>%, Group <col=ff00>Actual: <col=ff>"
								+ ((int) (prob * 100000)) / 1000.0D
								+ (prob == 0.5 ? "[CAP]" : "")
								+ "</col>%, Group Size: " + group.length());
						player.sendMessage("Current group rarity: <shad=000000>"
								+ rate.getColorString()
								+ rate);
					}

					player.sendMessage("Be warned: uses the rarity modifiers of the person who used command.");

					for (DropTableItem item : entry.getItems()) {
						if (item != null) {
							double probD = item.getProbability(player);
							int prob = item.getRawProbabilityPerMode(player);
							if (player.isSkulled && isRev) {
								prob = prob/3;
							} else if (isRev) {
								prob = prob/2;
							}
							String col = DropRate.getRate(probD).getColorString()
									+ "<shad=000000>";
							player.sendMessage(col + "" + ItemConstants.getItemName(item.getItemId()).replace("_", "")
									+ "</col></shad> - <col=ff>1/"
									+ (prob)
									+ "</col> Chance to drop.");
						}
					}

				}
			} catch (Exception ex) {
				player.sendMessage("Use syntax ::checkdrops abyssal demon");
			}
			return true;

		case "whatdrops":
			if (parser.hasNext()) {
				if (System.currentTimeMillis() - player.lastWhatDropCommand >= 2000) {

					final String itemName = parser.nextLine();

					player.sendMessage("<shad=000000>Monsters that drop <col=ff>" + itemName + ": ");
					boolean h = false;
					for (int i = 0, len = DropTable.getAllDropableItems().size(); i < len; i++) {
						ItemDefinition def = DropTable.getAllDropableItems().get(i);
						if (def == null) {
							continue;
						}

						if (def.getName().replace("_", " ").toLowerCase().equalsIgnoreCase(itemName)) {
							if (!h) {
								h = DropTable.whatDrops(player, def.getId());
							} else {
								DropTable.whatDrops(player, def.getId());
							}
						}

					}
					if (!h) {
						player.sendMessage("<col=FF0000><shad=000000>Nothing!");
						ArrayList<ItemDefinition> simItems = ItemHandler.getItemsWithSimilarName(itemName,
								DropTable.getAllDropableItems(), 5);
						if (simItems.size() > 0) {
							player.sendMessage("<shad=000000>Did you mean:");
							for (int i = 0, len = simItems.size(); i < len; i++) {
								ItemDefinition def = simItems.get(i);
								if (!def.getName().replace("_", " ").replace(" ", "")
										.equalsIgnoreCase(itemName.replace(" ", ""))) {
									// in
									// case
									// spaces
									// were
									// messing
									// something
									// up
									player.sendMessage(def.getName().replace("_", " "));
								}
							}
						}
					}
					player.lastWhatDropCommand = System.currentTimeMillis();
				} else {
					player.sendMessage("You must wait 2 seconds before using that command again.");
				}
				return true;
			}
			player.sendMessage("Use syntax ::whatdrops abyssal whip");
			return true;

		case "checkspins":

			if (!WorldType.equalsType(WorldType.ECONOMY)) {
				player.sendMessage("You must be in economy world in order to use the command.");
				return true;
			}

			if (!player.inEdgeville()) {
				player.sendMessage("You must be in Edgeville to use ::checkspins");
				return true;
			}

			if (player.startedCheckSpins) {
				player.sendMessage("You are already checking for rewards. Walk to stop checking task.");
				return true;
			}

			if (System.currentTimeMillis() - player.lastWhatDropCommand >= 5000) {
				if (player.getItems().freeSlots() < 1) {
					player.sendMessage("You must have at least 3 slots available in your Inventory.");
					return true;
				}
				if (player.inMinigame()) {
					player.sendMessage("You cannot claim your reward here.");
					return true;
				}
				if (player.underAttackBy > 0 || player.underAttackBy2 > 0) {
					player.sendMessage("You are currently under attack. Can't claim at the moment.");
					return true;
				}

				player.sendMessage("Checking for Wheel of fortune Rewards.");
				player.startedCheckSpins = true;

				Server.getTaskScheduler().schedule(new Task() {

					@Override
					protected void execute() {
						if (player == null || player.disconnected) {
							this.stop();
							return;
						}

						if (!player.startedCheckSpins) {
							player.sendMessage("Ended checking for rewards in SOF.");
							this.stop();
							return;
						}

						if (player.getItems().freeSlots() < 1) {
							player.sendMessage("Your inventory is full.");
							this.stop();
							return;
						}

						try {

							ClaimWheelReward.claimWheel(player, true);
						} catch (Exception e) {
							e.printStackTrace();
							player.sendMessage("An error has occured. Please try again later.");
							this.stop();
							return;
						}

					}
				});

				player.lastWhatDropCommand = System.currentTimeMillis();
			} else {
				player.sendMessage("You must wait 1 second before using the command again.");
			}
			return true;
			
		case "time":
			player.sendMessage(
					"Time: " + Server.getCalendar().getMonth() + " " + Server.getCalendar().currentDayOfMonth() + " "
							+ Server.getCalendar().getYear() + " " + Server.getCalendar().getTime());
			return true;
		case "checkmute":
			Player check = player;
			if (parser.hasNext()) {
				if (player.isStaff()) {
					String target = parser.nextString();
					Client c2 = (Client) PlayerHandler.getPlayerSmart(target);
					if (c2 != null) {
						check = c2;
					}
				}
			}
			if (Connection.isMuted(check) || check.getTimedMute() > 0) {
				long seconds  = Math.round(Misc.ticksIntoSeconds(check.getTimedMute()));
				player.sendMessage("<col=ff0000>Mute timer on "+ check.getNameSmartUp() +": " + String.format("%d:%02d ", TimeUnit.SECONDS.toMinutes(seconds),
						TimeUnit.SECONDS.toSeconds(seconds)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds))));
			} else {
				player.sendMessage(check.getNameSmartUp() + " is not muted");
			}
			return true;
		case "hybridset":
			player.getItems().hybridSet();
			return true;

		case "pureset":
			player.getItems().pureSet();
			return true;

		case "meleeset":
			player.getItems().meleeSet();
			return true;

		case "rangeset":
			player.getItems().rangeSet();
			return true;

		case "magicset":
			player.getItems().magicSet();
			return true;

		case "quickprayers":
			if (player.playerPrayerBook == PrayerBook.NORMAL) {
				player.quickPrayers.clear();
				for (int i = 0; i < player.prayerActive.length; i++) {
					if (player.prayerActive[i]) {
						player.quickPrayers.add(i);
					}
				}
			} else {
				player.quickCurses.clear();
				for (int i = 0; i < player.curseActive.length; i++) {
					if (player.curseActive[i]) {
						player.quickCurses.add(i);
					}
				}
			}
			return true;

		case "nex":
			if (!Config.RUN_ON_DEDI) {
				player.getPA().spellTeleport(2904, 5204, 0);
			}
			return true;

		case "clues":
			player.sendMessage("You've completed <col=ff>" + player.getClueAmount(TreasureTrails.LOW_CLUE)
					+ "</col> easy, <col=ff>" + player.getClueAmount(TreasureTrails.MED_CLUE) + "</col> medium and <col=ff>"
					+ player.getClueAmount(TreasureTrails.HIGH_CLUE) + " </col>hard clue scrolls!");
			return true;

		case "thread":
			try {
				String thread = parser.nextString();

				player.sendMessage("Opening https://www.soulplayps.com/forums/index.php?threads/" + thread + "/");
				player.getPacketSender().openUrl("https://www.soulplayps.com/forums/index.php?threads/" + thread + "/");
				player.getPA().closeAllWindows();
				return true;
			} catch (Exception e) {
				player.sendMessage("Correct way to use the command is ::thread 12345");
				return true;
			}

		case "appeal":
			player.getPacketSender().openUrl("https://www.soulplayps.com/forums/index.php?forums/submit-an-appeal.41/");
			player.getPA().closeAllWindows();
			return true;

		case "report":
			player.getPacketSender().openUrl("https://www.soulplayps.com/forums/index.php?forums/player-report.38/");
			player.getPA().closeAllWindows();
			return true;

		case "support":
			player.getPacketSender().openUrl("https://www.soulplayps.com/forums/index.php?forums/support.74/");
			player.getPA().closeAllWindows();
			return true;

		case "collect":
			player.getPA().collectMoney(false);
			return true;

		case "myshop":
			if (player.inWild() || !player.inMarket()) {
				player.sendMessage("You must be in ::market");
				return true;
			}
			if (!DBConfig.ADMIN_CAN_SELL_ITEMS && player.playerRights == 2) {
				player.sendMessage("Can't sell items.");
				return true;
			}
			PlayerOwnedShops.openPlayerShop(player, player);
			return true;

		case "save":
//			if (Config.RUN_ON_DEDI && !Config.SERVER_DEBUG && Config.SERVER_PORT == 43599) {
//				if ((System.currentTimeMillis() - player.lastSaved) > 60000) {
//					PlayerSaveThread.addToSaveList(player);
//					player.lastSaved = System.currentTimeMillis();
//				} else {
//					player.sendMessage("Please wait a bit before saving again.");
//				}
//			}
			return true;

		case "find":
			if (!player.inMarket()) {
				player.sendMessage("You must be inside ::market");
				return true;
			}

			try {
				String target = parser.nextLine();
				// System.out.println("target:" + target);
				final Client o = (Client) PlayerHandler.getPlayerSmart(target);
				if (o == null || !o.isActive) {
					player.sendMessage("Player (" + target + ") is not online.");
					return true;
				}
				if (!o.inMarket()) {
					player.sendMessage("Other player must be inside ::market");
					return true;
				}
				player.minigameTarget = o.playerIndex;
				player.getPacketSender().createPlayerHints(10, o.getId());
				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (player == null || player.disconnected || player.forceDisconnect
								|| !player.playerIsSkilling) {
							container.stop();
						}
					}

					@Override
					public void stop() {
						player.minigameTarget = 0;
						player.getPacketSender().createPlayerHints(10, -1);
					}
				}, 500); // TODO: make this 10 or 20 again
			} catch (

			Exception e) {
			}
			return true;

		case "webmarket":
			player.getPacketSender().openUrl("https://www.soulplayps.com/market/");
			return true;

		case "reconnectloginserver":
			if (!LoginServerConnection.instance().isActive()) {
				Server.connectToLoginServer();
				player.sendMessage("Connecting to loginserver.");
			} else {
				player.sendMessage("Already connected to login server.");
			}
			return true;

		case "bonuses":
			player.sendMessage("Gear/Inventory Bonuses: " + player.getItems().getTotalBonuses());
			return true;

		case "adv":
		case "advert":
			if (!player.inMarket()) {
				player.sendMessage("You must be inside ::market");
				return true;
			}
			if (System.currentTimeMillis() - player.lastWhatDropCommand < 2000) {
				player.sendMessage("Please wait 2 seconds to search again.");
				return true;
			}
			for (Player p : player.getRegion().getPlayers()) {
				if (p != null && p != player) {
					if (p.playerIsSkilling && p.getX() == player.getX() && p.getY() == player.getY()) {
						player.sendMessage(
								"Another player is already advertising on this spot. Please find another spot.");
						return true;
					}
				}
			}
			try {
				if (player.playerIsSkilling) {
					player.playerIsSkilling = false;
					return true;
				}
				if (!parser.hasNext(1)) {
					player.sendMessage("Usage - ::advert Selling tinderbox!");
					return true;
				}
				player.playerIsSkilling = true;

				final String ad = parser.nextLine();

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					int timer = 12;

					@Override
					public void execute(CycleEventContainer container) {
						if (player == null || player.disconnected || player.forceDisconnect
								|| !player.playerIsSkilling) {
							container.stop();
							return;
						}

						if (!player.inMarket()) {
							container.stop();
							return;
						}

						if (timer == 12) {
							player.speak(ad);
						}
						timer--;
						if (timer < 0) {
							timer = 12;
						}

					}

					@Override
					public void stop() {

					}
				}, 1);

			} catch (Exception ex) {
				ex.printStackTrace();
			}

			return true;

		case "search":
			if (!player.inMarket()) {
				player.sendMessage("You must be inside ::market");
				return true;
			}
			if (System.currentTimeMillis() - player.lastWhatDropCommand < 2000) {
				player.sendMessage("Please wait 2 seconds to search again.");
				return true;
			}
			try {
				// String itemName = args[1];
				if (parser.arguments() < 2) {
					player.getPacketSender().openUrl("www.itemdb.biz");
					return true;
				}

				final String input = parser.nextString();

				final int itemId = (input.matches("[0-9]+") ? Integer.parseInt(input) : 0);

				if (itemId > 0) {
					player.lastWhatDropCommand = System.currentTimeMillis();
					for (Player p : PlayerHandler.players) {
						if (p == null || p.disconnected || !p.inMarket()) {
							continue;
						}
						boolean foundItem = false;
						long lowestprice = Long.MAX_VALUE;
						for (int i = 0; i < p.playerShopItems.length; i++) {
							if (p.playerShopItems[i] > 0 && p.playerShopItems[i] == itemId) {
								if (lowestprice > p.playerShopItemsPrice[i]) {
									lowestprice = p.playerShopItemsPrice[i];
								}
								foundItem = true;
								// break;
							}
						}
						if (foundItem) {
							player.sendMessage(
									"<col=800000>Item Found - Player Name: " + p.getNameSmartUp() + " Price: " + lowestprice);
							continue;
						}
					}
				} else {
					player.sendMessage("Command example - (::search 590) will search for itemId 590 which is Matches.");

					player.getPacketSender().openUrl("www.itemdb.biz/index.php?search=" + input.replace(" ", "+"));
					return true;
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return true;

		case "staff": {
			List<String> helpers = new ArrayList<>();
			List<String> mods = new ArrayList<>();
			List<String> admins = new ArrayList<>();
			List<String> devs = new ArrayList<>();
			for (Player p : PlayerHandler.players) {
				if (p == null || p.isIdle || p.staffOpOut != null) {
					continue;
				}

				if (p.playerRights == 7) {
					helpers.add(p.getNameSmartUp());
				}
				if (p.playerRights == 1 && !p.isSemiAdmin()) {
					mods.add(p.getNameSmartUp());
				}
				if (p.playerRights == 2 || p.isSemiAdmin()) {
					admins.add(p.getNameSmartUp());
				}
				if (p.playerRights == 3) {
					devs.add(p.getNameSmartUp());
				}
			}
			
			if (helpers.isEmpty() && mods.isEmpty() && admins.isEmpty() && devs.isEmpty())
			{
				player.sendMessage("There's currently no staff online, try the forums if you have urgent matters.");
				return true;
			}

			if (!helpers.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < helpers.size(); i++) {
					String s = helpers.get(i);
					if (i < helpers.size() - 1) {
						sb.append(s).append(", ");
					} else {
						sb.append(s);
					}
				}
				player.sendMessage(String.format("<img=6> <shad=65535>Supports</shad>: %s", sb.toString()));
			}
			if (!mods.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < mods.size(); i++) {
					String s = mods.get(i);
					if (i < mods.size() - 1) {
						sb.append(s).append(", ");
					} else {
						sb.append(s);
					}
				}
				player.sendMessage(String.format("<img=0> <shad=16777215>Mods</shad>: %s", sb.toString()));
			}
			if (!admins.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < admins.size(); i++) {
					String s = admins.get(i);
					if (i < admins.size() - 1) {
						sb.append(s).append(", ");
					} else {
						sb.append(s);
					}
				}
				player.sendMessage(String.format("<img=2> <shad=16711680>Admins</shad>: %s", sb.toString()));
			}
			if (!devs.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < devs.size(); i++) {
					String s = devs.get(i);
					if (i < devs.size() - 1) {
						sb.append(s).append(", ");
					} else {
						sb.append(s);
					}
				}
				player.sendMessage(String.format("<img=2> <shad=255>Developers</shad>: %s", sb.toString()));
			}
		}
			return true;

		case "divine":
			player.sendMessage("Divine hits left: " + player.getSpiritShieldHits());
			return true;

		case "samurai":
			if (Config.isOwner(player) || player.getName().equalsIgnoreCase("samurai")) {
				SkillCapes.beginCompCapeEmote(player);
			}
			return true;

		case "kbase":
			player.sendMessage("Opening http://www.soulplayps.com/forums/index.php?forums/knowledge-base.14/");
			player.getPacketSender().openUrl("https://www.soulplayps.com/forums/index.php?forums/knowledge-base.14/");
			player.getPA().closeAllWindows();
			return true;

		case "rules":
			player.sendMessage("Opening http://www.soulplayps.com/forums/index.php?threads/souleco-rules.7/");
			player.getPacketSender().openUrl("https://www.soulplayps.com/forums/index.php?threads/souleco-rules.7/");
			player.getPA().closeAllWindows();
			return true;

		case "fm":
			if (player.myShopClient == null || !player.myShopClient.equals(player)) {
				player.sendMessage("You can only use this command when viewing your own shop.");
				return true;
			}
			boolean fixedShop = false;
			for (int i = 0; i < player.playerShopItems.length; i++) {
				if (player.playerShopItems[i] > 0) {
					if (player.getItems().freeSlots() == 0) {
						player.sendMessage("Your inventory is full.");
						break;
					}
					if (ItemDef.forID(player.playerShopItems[i]).itemIsInNotePosition) {
						if (player.getItems().addItem(player.playerShopItems[i], player.playerShopItemsN[i])) {
							player.playerShopItems[i] = 0;
							player.playerShopItemsPrice[i] = 0;
							player.playerShopItemsN[i] = 0;
							fixedShop = true;
						}
					}
				}

			}
			if (fixedShop) {
				PlayerOwnedShops.openPlayerShop(player, player);
				PlayerOwnedShops.updatePlayerOwnedShop(player);
			}
			return true;

		case "hacks":
			player.sendMessage(
					"Opening http://www.soulplayps.com/forums/index.php?threads/recent-server-hacks.4729/");
			player.getPacketSender().openUrl("http://www.soulplayps.com/forums/index.php?threads/recent-server-hacks.4729/");
			player.getPA().closeAllWindows();
			return true;

		case "fix":
			player.sendRegionChanged();
			player.sendMessage("Attempting to fix maps.");
			return true;

		}
		return false;
	}

	@Override
	public boolean canAccess(Client c) {
		return true;
	}

}
