package com.soulplay.content.player.commands;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Location;

public class CommandLogEntry {

	private final String playerName;
	private final int playerId;
	private final String command;
	private final Location loc;

	public CommandLogEntry(Client client, String command) {
		this.playerName = client.getNameSmart();
		this.playerId = client.mySQLIndex;
		this.command = command;
		this.loc = client.getCurrentLocation().copyNew();
	}

	public String getPlayerName() {
		return playerName;
	}

	public int getPlayerId() {
		return playerId;
	}

	public String getCommand() {
		return command;
	}

	public Location getLoc() {
		return loc;
	}

}
