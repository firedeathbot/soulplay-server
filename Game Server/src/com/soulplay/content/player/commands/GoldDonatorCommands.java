package com.soulplay.content.player.commands;

import com.soulplay.content.player.TeleportType;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.player.Client;

public final class GoldDonatorCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {
		switch (parser.getCommand()) {
		
		case "ez":
			if (player.getDonatedTotalAmount() >= 250) {
				player.getPA().startTeleport(LocationConstants.LAND_OF_SERENITY.getX(), LocationConstants.LAND_OF_SERENITY.getY(), 0, TeleportType.MODERN);
			} else if (player.getDonatedTotalAmount() < 250) {
				player.getPA().closeAllWindows();
				player.sendMessage("You need to be a Supreme Donator or higher to enter this place");
			}
			return true;
			
		}
		return false;
	}

	@Override
	public boolean canAccess(Client player) {
		/* Gold donator commands */
		if (player.playerRights == 6 || player.playerRights == 11 || player.playerRights == 12 || player.isMod()
				|| player.getDonatedTotalAmount() >= 250) {
			return true;
		}

		return false;
	}

}
