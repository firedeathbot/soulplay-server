package com.soulplay.content.player.commands;

import com.soulplay.game.model.player.Client;

public final class SuperDonatorCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {
		switch(parser.getCommand()) {
		
		}
		return false;
	}

	@Override
	public boolean canAccess(Client player) {
		/* Silver donator commands */
		if (player.getDonatedTotalAmount() >= 50 || player.playerRights == 1 && !player.isOwner()) {
			return true;
		}
		return false;
	}

}
