package com.soulplay.content.player.commands;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.JudgementHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.privatemessage.Friends;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

public final class InformerCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {

		final String command = parser.getCommand();

		switch (command) {

		case "timedmute":
			if (parser.hasNext()) {
				CommandParser parser2 = CommandParser.split(parser, "-");

				if (parser2.hasNext(2)) {
					final String playerToMute = parser2.nextString();
					int muteTimer = parser2.nextInt();

					int secondsToTicks = Misc.secondsToTicks(muteTimer);

					if (muteTimer > 12000) {
						player.sendMessage("Setting mute timer to 2 hours.");
						muteTimer = 12000;
					}

					Player other = PlayerHandler.getPlayerSmart(playerToMute);

					if (other != null && other.isActive) {

						Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, other.getName(),
								"Mute (" + muteTimer + "s)", ""));
						other.sendMessage(
								"You have been muted by: " + player.getNameSmartUp() + " for " + muteTimer + " seconds");
						other.setTimedMute(secondsToTicks);
						player.sendMessage("You have muted " + other.getNameSmartUp() + " for " + muteTimer + " seconds.");
						return true;
					} else {
						player.sendMessage("Player is offline or does not exist.");
						return true;
					}
				}
			}
			player.sendMessage("Use syntax ::timedmute player name-seconds");
			return true;

		case "clearfriends":
			for (long l : player.getFriends()) {
				if (l > 0) {
					Friends.removeFriend(l, player);
				}
			}
			// c.disconnected = true;
			return true;

		case "chatabuse":
			if (parser.hasNext()) {

				CommandParser parser2 = CommandParser.split(parser, "-");

				if (parser2.hasNext(2)) {
					String playerToMute = parser2.nextString();
					String reason = parser2.nextString();

					Player target = PlayerHandler.getPlayerSmart(playerToMute);

					if (target != null && target.isActive) {

						target.setChatAbuse(1, true);
						int muteTimer = JudgementHandler.calculateTimer(target, JudgementHandler.CHATABUSE);
						target.setTimedMute(Misc.secondsToTicks(muteTimer));
						target.sendMessage("You have been muted by: " + player.getNameSmartUp() + " for " + muteTimer
								+ " seconds for Chat Abuse");
						player.sendMessage("You have muted " + target.getNameSmartUp() + " for " + muteTimer + " seconds.");
						Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, target.getName(),
								"Mute (" + muteTimer + "s)", "Chat Abuse: " + reason));
						Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("evidence"),
								SlackMessageBuilder.buildEvidenceMessage(target.getName(), "Chat Abuse",
										"Timed mute: " + muteTimer, reason, player.getName()));
						return true;
					} else {
						player.sendMessage("Player is offline or does not exist.");
						return true;
					}

				}

			}
			player.sendMessage("Use syntax ::chatabuse player name-reason");
			return true;

		}
		return false;
	}

	@Override
	public boolean canAccess(Client player) {
		if (player.playerRights == 7 || player.isMod() || player.isAdmin() || Config.isOwner(player)) {
			return true;
		}
		return false;
	}

}
