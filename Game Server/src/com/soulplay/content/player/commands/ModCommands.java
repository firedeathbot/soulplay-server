package com.soulplay.content.player.commands;

import java.util.Optional;
import java.util.regex.Pattern;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.event.easter.BunnyBoss;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.ddmtournament.Presets;
import com.soulplay.content.minigames.ddmtournament.SpawnTable;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.minigames.lastmanstanding.Lisa;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.minigames.lastmanstanding.LmsManager.Mode;
import com.soulplay.content.minigames.partyroom.DropParty;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.content.player.combat.rules.AntiRush;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.tracking.GrabUUID;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.PunishPlayerHandler;
import com.soulplay.game.world.World;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.net.Connection;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.packet.in.CommandPacket.Punishment;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

import plugin.item.RottenPotatoPlugin;

public final class ModCommands implements Command {

    @Override
    public boolean execute(Client player, CommandParser parser, String realCommand) {
	switch (parser.getCommand()) {

    case "ihp":
    case "infhp":
    case "infinitehp":
        player.getPlayerLevel()[3] = 99999999;
        player.refreshSkill(3);
        return true;
        
    case "boosthp": {
    	
    	int value = 999999;
    	
		if (parser.hasNext())
			value = parser.nextInt();
		
		player.getSkills().setLevel(Skills.HITPOINTS, value);
		
		player.sendMessage("Set HP to " + Misc.formatNumbersWithCommas(value));
		
        return true;
    }
	
	case "togglebunny": {
		BunnyBoss.ENABLED = !BunnyBoss.ENABLED;
		player.sendMessage("You have "+ (BunnyBoss.ENABLED ? "Enabled" : "Disabled") +" the Bunny Boss Hourly Spawn.");
		return true;
	}
	
	case "easterbunnyrandom": {
		int lowestHit = 5;
		int highestHit = 45;
		int maxhit = Misc.random(lowestHit, highestHit);
		
		int defense = Misc.interpolateSafe(100, 1500, lowestHit, highestHit, maxhit);

		BunnyBoss.spawnBoss(null, Misc.random(1000, 10000), maxhit, 3000, defense);
		return true;
	}
	
	case "easterbunny": {
		BunnyBoss.spawnBoss(player.getCurrentLocation().copyNew(), 10000, 40, 3000, 1000);
		return true;
	}
	
	case "easterbunnyhard": {
		BunnyBoss.spawnBoss(player.getCurrentLocation().copyNew(), 50000, 60, 3000, 1500);
		return true;
	}
	
	case "easterbunnyeasy": {
		BunnyBoss.spawnBoss(player.getCurrentLocation().copyNew(), 1000, 8, 500, 100);
		return true;
	}
	
	case "potato": {
		player.getItems().addItem(RottenPotatoPlugin.ROTTEN_POTATO, 1);
		player.sendMessage("You spawn a rotten potato.");
		return true;
	}
	
	case "toggleblacklistuuid": {
		MeleeRequirements.BLACK_LIST_UUID_ENABLED = !MeleeRequirements.BLACK_LIST_UUID_ENABLED;
		player.sendMessage("You have "+ (MeleeRequirements.BLACK_LIST_UUID_ENABLED ? "Enabled" : "Disabled") +" the Blacklist MAC check.");
		return true;
	}
	
	case "toggleblacklist": {
		MeleeRequirements.BLACK_LIST_ENABLED = !MeleeRequirements.BLACK_LIST_ENABLED;
		player.sendMessage("You have "+ (MeleeRequirements.BLACK_LIST_ENABLED ? "Enabled" : "Disabled") +" the Blacklist.");
		return true;
	}
	
	case "togglepktournament": {
		Tournament.OFFICIAL_ENABLED = !Tournament.OFFICIAL_ENABLED;
		player.sendMessage("You have "+ (Tournament.OFFICIAL_ENABLED ? "opened" : "closed") +" the Official PK Tournament autostart.");
		return true;
	}
	
	case "togglemasslms": {
		LmsManager.OFFICIAL_ENABLED = !LmsManager.OFFICIAL_ENABLED;
		player.sendMessage("You have "+ (LmsManager.OFFICIAL_ENABLED ? "opened" : "closed") +" the Mass LMS autostart.");
		return true;
	}
	
	case "opentshop": {
		SpawnTable.openTable(player);
		return true;
	}
	
	case "debugpid": {
		player.debugPID = !player.debugPID;
		player.sendMessage("Debugging PID is set: " + player.debugPID);
		return true;
	}
	
	case "boxingtable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("Set PK Tournament to boxing.");
		Presets.BOXING.setSettings();
		return true;
	}
	
	case "barrowstable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("Added barrows items to PK Tournament spawn table.");
		Presets.DH.setSettings();
		return true;
	}
	
	case "emptytable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("Set as Empty Preset.");
		Presets.EMPTY.setSettings();
		return true;
	}
	
	case "zerktable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("Added ZERKER items to PK Tournament spawn table.");
		Presets.ZERKER1.setSettings();
		return true;
	}
	
	case "nhtable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("Added NH items to PK Tournament spawn table.");
		Presets.NH_MAX.setSettings();
		return true;
	}
	
	case "purenhtable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("SET PURE NH items to PK Tournament.");
		Presets.PURE_NH.setSettings();
		return true;
	}
	
	case "f2ptable": {
		if (Tournament.lobbyOpen || !Tournament.lobbyPlayers.isEmpty()) {
			player.sendMessage("Lobby is open, must clear it out or close it first.");
			return true;
		}
		player.sendMessage("Added F2P items to PK Tournament spawn table.");
		Presets.F2P.setSettings();
		return true;
	}
	
	case "pktlobby": {
		Tournament.toggleLobby();
		player.sendMessage("You have "+ (Tournament.lobbyOpen ? "opened" : "closed") +" the PK Tournament lobby.");
		return true;
	}
	
	case "pktspawn": {
		if (Tournament.lobbyOpen) {
			player.sendMessage("You cannot toggle spawn when lobby is open.");
			return true;
		}
		Tournament.pre_settings.setSpawnGear(!Tournament.pre_settings.isSpawnGear());
		player.sendMessage("You have turned "+ (Tournament.pre_settings.isSpawnGear() ? "ON" : "OFF") +" the PK Tournament spawning.");
		return true;
	}
	
	case "pktstart": {
		Tournament.startGame();
		player.sendMessage("Attempting to start the tournament.");
		return true;
	}
	
	case "pktend": {
		player.sendMessage("Attempting to end the last tournament instance.");
		Tournament.lastInstance.endGame();
		return true;
	}
	
	case "pktkicklobby": {
		player.sendMessage("Attempting to kick all players from lobby.");
		Tournament.kickAllFromLobby();
		return true;
	}
	
	case "pktmagic": {
		if (Tournament.pre_settings.getSpellBook() == SpellBook.NORMAL)
			Tournament.pre_settings.setSpellBook(SpellBook.ANCIENT);
		else if (Tournament.pre_settings.getSpellBook() == SpellBook.ANCIENT)
			Tournament.pre_settings.setSpellBook(SpellBook.LUNARS);
		else
			Tournament.pre_settings.setSpellBook(SpellBook.NORMAL);
			
		player.sendMessage("Magic Book set to " + Tournament.pre_settings.getSpellBook().toString());
		return true;
	}
	
	case "pktprayert": {
		if (Tournament.pre_settings.getPrayerBook() == PrayerBook.NORMAL)
			Tournament.pre_settings.setPrayerBook(PrayerBook.CURSES);
		else
			Tournament.pre_settings.setPrayerBook(PrayerBook.NORMAL);
		player.sendMessage("Prayers set to " + Tournament.pre_settings.getPrayerBook().toString());
		return true;
	}
	
	case "pktprayer1": {
		Tournament.pre_settings.setDisableProtectPrayers(!Tournament.pre_settings.isDisableProtectPrayers());
		player.sendMessage("You have "+ (Tournament.pre_settings.isDisableProtectPrayers() ? "DISABLED" : "ENABLED") +" the Combat Protect Prayers.");
		return true;
	}
	
	case "pktprayer2": {
		Tournament.pre_settings.setDisablePrayers(!Tournament.pre_settings.isDisablePrayers());
		player.sendMessage("You have "+ (Tournament.pre_settings.isDisablePrayers() ? "DISABLED" : "ENABLED") +" Prayers.");
		return true;
	}
	
	case "pktsafedeath": {
		if (Tournament.lobbyOpen) {
			player.sendMessage("You cannot toggle spawn when lobby is open.");
			return true;
		}
		Tournament.pre_settings.setSafeDeath(!Tournament.pre_settings.isSafeDeath());
		player.sendMessage("You have turned "+ (Tournament.pre_settings.isSafeDeath() ? "ON" : "OFF") +" the PK T Safe death.");
		return true;
	}
	
	case "pkttimer": {
		int value = 0;
		if (parser.hasNext())
			value = parser.nextInt();
		
		Tournament.lastInstance.setTimer(value);
		return true;
	}
	
	case "startlms1": {
		if (LmsManager.serverWideMode == Mode.NORMAL) {
			player.sendMessage("Attempting to start Normals LMS");
		} else {
			player.sendMessage("Attempting to start event LMS");
		}

		LmsManager.startGame(LmsManager.serverWideMode);
		return true;
	}
	case "clearconbans": {
		player.sendMessage("reloaded connection bans.");
		Connection.spammingConnections.clear();
		return true;
	}
	  case "remannouncement":
          AnnouncementHandler.removeAnnouncement();
          return true;
	case "pohdropping":
	    if (ConstructionUtils.DISABLED_DROP) {
	    	ConstructionUtils.DISABLED_DROP = false;
			player.sendMessage("You have enabled dropping items in PoH.");
		} else {
			ConstructionUtils.DISABLED_DROP = true;
		    player.sendMessage("You have disabled dropping items in PoH.");
		}
		return true;
	  case "swevent":
          if (SoulWars.EXTRA_REWARD) {
              SoulWars.EXTRA_REWARD = false;
              player.sendMessage("You have disabled the Soulwars event.");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, false, "Soulwars"));
          } else {
              SoulWars.EXTRA_REWARD = true;
              player.sendMessage("You have enabled the Soulwars event.");
              PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + player.getNameSmartUp() + " has enabled the soulwars Event for extra zeals!");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, true, "Soulwars"));
          }
          return true;
          
	  case "lmsevent":
          if (LmsManager.EXTRA_REWARD) {
        	  LmsManager.EXTRA_REWARD = false;
              player.sendMessage("You have disabled the LMS event.");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, false, "Last Man Standing"));
          } else {
        	  LmsManager.EXTRA_REWARD = true;
              player.sendMessage("You have enabled the LMS event.");
              PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + player.getNameSmartUp() + " has enabled the Last Man Standing Event for extra points!");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, true, "Last Man Standing"));
          }
          return true;
          
	  case "lmscomp":
          if (Lisa.COMPETITIVE_ENABLED) {
        	  Lisa.COMPETITIVE_ENABLED = false;
              player.sendMessage("You have disabled the LMS Competitive mode.");
          } else {
        	  Lisa.COMPETITIVE_ENABLED = true;
              player.sendMessage("You have enabled the LMS Competitive mode.");
          }
          return true;
          
	  case "pcevent":
          if (PestControl.EXTRA_REWARD) {
              PestControl.EXTRA_REWARD = false;
              player.sendMessage("You have disabled the PC event.");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, false, "Pest Control"));
          } else {
              PestControl.EXTRA_REWARD = true;
              player.sendMessage("You have enabled the PC event.");
              PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + player.getNameSmartUp() + " has enabled the Pest control Event for extra points!");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, true, "Pest Control"));
          }
          return true;

      case "cwevent":
          if (CastleWars.EXTRA_REWARD) {
              CastleWars.EXTRA_REWARD = false;
              player.sendMessage("You have disabled the Castle wars event.");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, false, "Castle Wars"));
          } else {
              CastleWars.EXTRA_REWARD = true;
              player.sendMessage("You have enabled the Castle wars event.");
              PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + player.getNameSmartUp() + " has enabled the castle wars Event for extra tickets!");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, true, "Castle Wars"));
          }
          return true;

      case "edgepkevent":
          if (Player.edgeWildyEvent) {
              Player.edgeWildyEvent = false;
              player.sendMessage("You have disabled the edgeville pk event.");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, false, "Edgeville PK"));
          } else {
              Player.edgeWildyEvent = true;
              player.sendMessage("You have enabled the edgeville pk event.");
              PlayerHandler.messageAllPlayers("<img=9><col=4848b7> Edgeville PK event has started, Every kill drops a big stack of coins ");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, true, "Edgeville PK"));
          }
          return true;

      case "clanwarspkevent":
          if (Player.clanWarsWildyEvent) {
              Player.clanWarsWildyEvent = false;
              player.sendMessage("You have disabled the clan wars pk event.");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, false, "Clan Wars"));
          } else {
              Player.clanWarsWildyEvent = true;
              player.sendMessage("You have enabled the clan wars pk event.");
              PlayerHandler.messageAllPlayers("<img=9><col=4848b7> Clan Wars PK event has started, Every kill drops a big stack of coins ");
              Server.getSlackApi().call(SlackMessageBuilder.buildEventMessage(player, true, "Clan Wars"));
          }
          return true;

	case "modcommands":
	    player.sendMessage("::teleto ::teletome ::kick ::jail ::unjail");
	    player.sendMessage("::mute ::unmute ::muteip ::unmuteip ::mutemac ::unmutemac ::timedmute");
	    player.sendMessage("::mutyell ::unmuteyell");
	    player.sendMessage("::ban ::unban ::banip ::unbanip ::banmac ::unbanmac ::timedban");
	    player.sendMessage("::dz ::staffzone ::checkinv ::minigameban");
	    player.sendMessage("::checkbank ::checkip ::scanip ::grabuuid ::toggletradingpost");
	    return true;
	    
	case "lmsreward":
		if (player.playerRights == 1 && LmsManager.normalReward != null) {
			player.sendMessage("There is already a reward in LMS");
			return true;
		}
		if (parser.hasNext()) {
			final int itemId = parser.nextInt();
			final int itemAmount = parser.nextInt();
			if (!player.getItems().playerHasItem(itemId, itemAmount)) {
				player.sendMessage("You don't have that item or amount in your inventory.");
				return true;
			}
			player.getItems().deleteItem2(itemId, itemAmount);

			Item donation = new Item(itemId, itemAmount);

			final String itemAmountStr = Misc.formatNumbersWithCommas(itemAmount);
			final String itemName = ItemConstants.getItemName(itemId);
			
			LmsManager.createLMSNormalEvent(donation, itemAmountStr, itemName);

			if (player.isAdmin()) {
				try {
					if (parser.hasNext(3)) {
						final int gamesAmount = parser.nextInt();
						LmsManager.GAMES_TO_REWARD_AMOUNT = gamesAmount;
					}
				} catch (NumberFormatException e) {
					player.sendMessage("You messed up on the games amount to reward players!");
				}
			}
			

			player.sendMessage("You add " + itemAmount + " " + itemName + " as LMS winner reward!");
			
		}
		return true;
		
	case "lmseventamount":
		if (player.isAdmin() && parser.hasNext()) {
			try {
				final int gamesAmount = parser.nextInt();
				LmsManager.GAMES_TO_REWARD_AMOUNT = gamesAmount;
			} catch (NumberFormatException e) {
				player.sendMessage("You messed up on the games amount to reward players!");
			}
			player.sendMessage("Games to reward players is set to :"+LmsManager.GAMES_TO_REWARD_AMOUNT);
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000> LMS event has been extended by " + LmsManager.GAMES_TO_REWARD_AMOUNT + " games!");
		}
		return true;
		
	case "clearlms":
		LmsManager.normalReward = null;
		LmsManager.PARTICIPATION_REWARD = null;
		LmsManager.GAMES_TO_REWARD_AMOUNT = 0;
		player.sendMessage("You have cleared the LMS rewards.");
		return true;
	    
	case "grabuuid":
		if (parser.hasNext()) {

			final String playerName = parser.nextLine();

			Player other = PlayerHandler.getPlayerSmart(playerName);
			
			if (other != null) {
				other.getPacketSender().sendConfig(ConfigCodes.REQUEST_UUID, 1);
				GrabUUID.enterRequest(other.getName(), player.mySQLIndex);
				player.sendMessage("Sent UUID Request on player:" + other.getNameSmartUp());
				player.sendMessage("Current UUID: " + other.UUID);
			} else {
				player.sendMessage("Unable to find playername:"+playerName);
			}
			
		}
		return true;
		
	case "toggletradingpost":
		Config.TRADING_POST_ENABLED = !Config.TRADING_POST_ENABLED;
		player.sendMessage("Trading post has been turned "+ (Config.TRADING_POST_ENABLED ? "<col=ff00>ON" : "<col=ff0000>OFF"));
		return true;

	case "teletome":
		if (parser.hasNext()) {
			final String playerName = parser.nextLine();

			Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

			if (other == null) {
				player.sendMessage("Player is offline or does not exist.");
				return true;
			}

			if (!teleportRestrictions(player, other)) {
				return true;
			}

			other.getPA().movePlayer(player.getX(), player.getY(), player.getHeightLevel());

			player.sendMessage(String.format("You have teleported %s to you.", other.getNameSmartUp()));
			other.sendMessage(String.format("You have been teleported to %s.", player.getNameSmartUp()));
			return true;
		}

	    player.sendMessage("Use syntax ::teletome playername");
		return true;

	case "teleto":
	    if (parser.hasNext()) {
	    	final String playerName = parser.nextLine();

	    	Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

	    	if (other == null) {
	    		player.sendMessage("Player is offline or does not exist.");
	    		return true;
	    	}

			if (!teleportRestrictions(player, other)) {
				return true;
			}

            if (other.playerIsInHouse) {
                World.search(other.getHomeOwnerId()).ifPresent(it -> player.getPOH().enter(other));
            } else {
                player.getPA().movePlayer(other.getX(), other.getY(), other.heightLevel);
            }
	    	return true;
	    }

	    player.sendMessage("Use syntax ::teleto playername");
	    return true;
	    
	case "debugplayer":
	    if (parser.hasNext()) {

	    	final String playerName = parser.nextLine();


	    	Player other = PlayerHandler.getPlayerSmart(playerName);

	    	if (other == null) {
	    		player.sendMessage("Player is offline or does not exist.");
	    		return true;
	    	}

	    	System.out.println(other.toString());

	    	player.sendMessage("Printed out player debug info to developers.");
	    	return true;

	    }
	    player.sendMessage("Use syntax ::debugplayer player name");
	    return true;

	case "sendhome":
	    if (parser.hasNext()) {
			String playerName = parser.nextLine().toLowerCase();
			for (Player other : PlayerHandler.players) {
			    if (other == null) {
			    	continue;
			    }

			    if (other.getNameSmart().toLowerCase().equals(playerName)) {
					if (!teleportRestrictions(player, other)) {
						return true;
					}

					if (other.isInJail()) {
					    player.sendMessage("You cannot teleport players that are in jail");
					    return true;
					}

					other.getPA().movePlayer(DBConfig.RESPAWN_X, DBConfig.RESPAWN_Y, 0);

					player.sendMessage("You have teleported " + other.getNameSmartUp() + " home.");
					other.sendMessage("You have been teleported home");
					return true;
			    }
			}
	    }

	    player.sendMessage("Use syntax ::sendhome player name");
	    return true;

	case "unjail":
	    if (parser.hasNext()) {

		final String playerName = parser.nextLine();

		Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

		if (other == null) {
		    player.sendMessage("Player must be offline or doesn't exist.");
		    return true;
		}

		if (other.inWild()) {
		    player.sendMessage("This player is in the wilderness, not in jail.");
		    return true;
		}

		if (other.duelStatus == 5 || other.inDuelArena()) {
		    player.sendMessage("This player is during a duel, and not in jail.");
		    return true;
		}

		if (!other.isInJail()) {
		    player.sendMessage("Player isn't in jail");
		    return true;
		}

		if (other.confirmUnjail) {
		    player.sendMessage("The player will now be unjailed");
		    other.unjailPlayer();
		}

		if (!Config.SERVER_DEBUG && (!player.getName().equals(other.getName()))) {
		    Server.getSlackApi()
			    .call(SlackMessageBuilder.buildJudgementMesssage(player, other.getName(), "Unjail", ""));
		}

		if (other.getJailTime() > 0 && !other.confirmUnjail) {
		    player.sendMessage("This player is still jailed for " + Misc.ticksIntoSeconds(other.getJailTime())
			    + " seconds. Use ::unjail again to unjail.");
		    other.confirmUnjail = true;
		    return true;
		}
		other.sendMessage("You have been unjailed by " + player.getNameSmartUp() + ". You can now teleport.");
		player.sendMessage("Successfully unjailed " + other.getNameSmartUp() + ".");
		other.unjailPlayer();
		return true;
	    }
	    player.sendMessage("Use syntax ::unjail player name");
	    return true;

	case "jail":
		try {
			if (parser.hasNext()) {

				CommandParser parser2 = CommandParser.split(parser, "-");

				if (parser2.hasNext(3)) {

					final String playerName = parser2.nextString();

					final double hours = parser2.nextDouble();

					final String reason = parser2.nextString();

					final Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

					if (other == null || other.disconnected) {
						player.sendMessage("Player is not online:" + playerName);
						return true;
					}

					PunishPlayerHandler.jailPlayer(player, other, hours, reason);
					return true;
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		player.sendMessage("Use syntax ::jail user name-2-some reason");
		return true;
	    
	case "minigameban":
		if (parser.hasNext()) {
			final String playerName = parser.nextLine();

	    	Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

	    	if (other == null) {
	    		player.sendMessage("Player is offline or does not exist.");
	    		return true;
	    	}
	    	if (other.getMinigamePunishmentPoints() >= 3) {
	    		player.sendMessage("This player is already perma banned from minigames.");
	    		return true;
	    	}
	    	int newPoints = other.getMinigamePunishmentPoints() + 1;
	    	
	    	other.setMinigamePunishmentPoints(newPoints);
	    	other.sendMessage("You have been given a minigame punishment point by "+player.getNameSmartUp());
	    	other.sendMessage("You now have "+newPoints+" minigame punishment points.");
	    	other.sendMessage("Third point will mean you are permanently banned from PvP Minigames.");
	    	
	    	if (newPoints == 1) {
	    		other.setMinigameBanTimer(18000); // ban for 1 day for first offense
	    		other.sendMessage("You have been banned from PvP Minigames for 3 play time hours.");
	    	} else if (newPoints == 2) {
	    		other.setMinigameBanTimer(36000); // ban for 1 day for first offense
	    		other.sendMessage("You have been banned from PvP Minigames for 6 play time hours.");
	    	}
	    	Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, other.getName(),
					"Banned from PvP Minigames (" + (newPoints == 1 ? "3" : "6") + ")", "N/A"));
	    	player.sendMessage("You have given a minigame punishment point to "+other.getNameSmartUp()+".");
	    	player.sendMessage("This player now has "+newPoints+" punishment points.");
	    	
		} else {
			player.sendMessage("Use syntax ::minigameban player name");
		}
		return true;

	case "timedban":
	    if (parser.hasNext()) {
		try {
		    CommandParser parser2 = CommandParser.split(parser, "-");

		    final String punishedName = parser2.nextString();

		    if (punishedName.isEmpty()) {
			return true;
		    }

		    final long time = parser2.nextLong();

		    if (time < 1) {
			player.sendMessage("Time must be greater than 0");
			return true;
		    }

		    final String reason = parser.nextString();

		    if (reason.length() < 1) {
			player.sendMessage("You must enter a reason for your timed ban.");
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.BAN_NAME.getPunishmentType(), time * 1000 + System.currentTimeMillis(),
				reason);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.BAN_NAME.getDesc() + " (" + time + ")", reason));
		    });
		    player.sendMessage("Sent timed ban request on playername: " + punishedName + " | Seconds:" + time);
		} catch (Exception ex) {
		    player.sendMessage("Use syntax ::timedban player name-seconds-some reason");
		}
	    }
	    return true;

	case "checkinv":
	    try {
		if (parser.hasNext()) {

		    final String playerName = parser.nextLine();

		    Optional<Client> result = World.search(playerName);

		    if (result.isPresent()) {
			Client other = result.get();
			
			if (other.getName().equalsIgnoreCase(player.getName())) {
				player.getItems().updateInventory();
				player.sendMessage("Reset my own inventory.");
				return true;
			}

			player.getPA().otherInv(player, other);
			player.sendMessage("Checking inventory of player name: " + other.getNameSmartUp());
			return true;

		    } else {
			player.sendMessage(String.format("Player %s is offline or does not exist.", playerName));
			return true;
		    }

		}
		player.getItems().updateInventory();
		player.sendMessage("Reset my own inventory.");
		return true;
	    } catch (Exception e) {
		player.sendMessage("Use syntax ::checkinv player name");
	    }
	    return true;

	case "ipmute":

	    if (Config.SERVER_DEBUG) {
		return true;
	    }

	    if (parser.hasNext()) {
		try {
		    final String playerToBan = parser.nextLine();

		    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			if (PlayerHandler.players[i] != null) {
			    if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerToBan)) {
				Connection.addIpToMuteList(PlayerHandler.players[i].connectedFrom);
				if (!Config.SERVER_DEBUG) {
				    Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player,
					    playerToBan, "IP mute", ""));
				}
				player.sendMessage("You have IP Muted the user: " + PlayerHandler.players[i].getNameSmartUp());
				Client c2 = (Client) PlayerHandler.players[i];
				c2.sendMessage("You have been muted by: " + player.getNameSmartUp());
				return true;
			    }
			}
		    }

		    player.sendMessage("Player Must Be Offline.");
		} catch (Exception e) {
		    player.sendMessage("Use syntax: ::ipmute player name");
		    return true;
		}
	    }
	    return true;

	case "unipmute":
	    if (parser.hasNext()) {
		try {

		    final String playerToBan = parser.nextLine();

		    for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			if (PlayerHandler.players[i] != null) {
			    if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerToBan)) {
				Connection.unIPMuteUser(PlayerHandler.players[i].connectedFrom);
				if (!Config.SERVER_DEBUG) {
				    Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player,
					    playerToBan, "Un-IP mute", ""));
				}
				player.sendMessage(
					"You have Un Ip-Muted the user: " + PlayerHandler.players[i].getNameSmartUp());
				return true;
			    }
			}
		    }

		    player.sendMessage("Player Must Be Offline.");
		} catch (Exception e) {
		    player.sendMessage("Use syntax ::unipmute player name");
		}
	    }
	    return true;

	case "kick":
	    if (parser.hasNext()) {
		final String punishedName = parser.nextLine();

		if (punishedName.length() < 1) {
		    return true;
		}

		LoginServerConnection.instance().submit(() -> {
		    LoginServerConnection.instance().punishPlayer(player.getName(), punishedName, 254, 0, "kicking");
		    Server.getSlackApi()
			    .call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName, "Kicked", ""));
		});
		player.sendMessage("Sent kick request on playername: " + punishedName);
		return true;
	    }
	    player.sendMessage("Command Usage - ::kick playername");
	    return true;

	case "pjail": {
		if (parser.hasNext()) {

			final String playerToBan = parser.nextLine();

			Player c2 = PlayerHandler.getPlayerSmart(playerToBan);

			if (c2 == null || !c2.isActive) {
				player.sendMessage("Player Must Be Offline.");
				return true;
			}

			if (c2.isBot()) {
				return true;
			}

			if (c2.inMinigame()) {
				player.sendMessage("They are inside a minigame. Kick them first then jail.");
				return true;
			}

			if (c2.duelStatus == 5) {
				player.sendMessage("You cant jail a player when he is during a duel.");
				return true;
			}

			if (!Config.SERVER_DEBUG && (!player.getName().equals(c2.getName()))) {
				Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player,
						c2.getName(), "Perma Jail", ""));
			}

			c2.getPA().movePlayer(2095, 4428, 4);
			c2.sendMessage("You have been jailed by " + player.getNameSmartUp() + " .");
			player.sendMessage("Successfully Jailed " + c2.getNameSmartUp() + ".");
			c2.getDialogueBuilder().sendStatement("Jailed by: " 
					+ player.getNameSmartUp(),
					"Time remaining: Permanent",
					"Reason: Will follow");
		}
		return true;
	}

	case "staffzone":
	    player.getPA().startTeleport(2912, 5475, 0, TeleportType.MODERN);
	    return true;
	    
	case "cwwait": {
		if (CastleWars.getGameTimer() > 0) {
			player.sendMessage("You cannot set the wait time during an ongoing CW game.");
			return true;
		}
		
		if (parser.hasNext()) {

			final int length = parser.nextInt();
			
			if (length < 1) {
				player.sendMessageSpam("You cannot set it to number below 1.");
				return true;
			}

			int ticks = length * 100;
			
			CastleWars.setWaitTimer(ticks);
			
			player.sendMessage("You have set the CastleWars wait time to " + length + " minutes.");
		}
		return true;
	}
	
	case "swwait": {
		if (SoulWars.gameTimer > 0) {
			player.sendMessage("You cannot set the wait time during an ongoing SW game.");
			return true;
		}
		
		if (parser.hasNext()) {

			final int length = parser.nextInt();
			
			if (length < 1) {
				player.sendMessageSpam("You cannot set it to number below 1.");
				return true;
			}

			int ticks = length * 100;
			
			SoulWars.waitTimer = ticks;
			
			player.sendMessage("You have set the SoulWars wait time to " + length + " minutes.");
		}
		return true;
	}

	case "clanstaking":
	    DBConfig.CLAN_STAKING = !DBConfig.CLAN_STAKING;
	    player.sendMessage("Clan Wars Staking set to " + DBConfig.CLAN_STAKING);
	    return true;

	case "greenportal":
		if (player.inSoulWarsLobby()) {
			SoulWars.greenPortalOnly = !SoulWars.greenPortalOnly;
			player.sendMessage("SoulWars Green Portal Lock set to " + SoulWars.greenPortalOnly);
		} else {
			CastleWars.GREEN_PORTAL_ONLY = !CastleWars.GREEN_PORTAL_ONLY;
			player.sendMessage("Castle Wars Green Portal Lock set to " + CastleWars.GREEN_PORTAL_ONLY);
		}
	    return true;
	    
	case "swgearcheck": {
		SoulWars.checkGearBonus = !SoulWars.checkGearBonus;
		player.sendMessage("Gear bonus check is set to : " +SoulWars.checkGearBonus);
	}
		return true;
	    
	case "lockparty":
		DropParty.setLockedLever(!DropParty.isLockedLever());
		player.sendMessage("The Drop Party Lever has been set to: "+ DropParty.isLockedLever());
		return true;
		
	case "lockdpchest":
		DropParty.setLockedChest(!DropParty.isLockedChest());
		player.sendMessage("The Drop Party Chest has been set to: "+ DropParty.isLockedChest());
		return true;
		
	case "emptydpchest":
		DropParty.getItemstodrop().clear();
		player.sendMessage("Drop party chest has been cleared.");
		return true;
	    
	case "tmute":
	    if (parser.hasNext()) {
		try {
		    if (parser.arguments() < 2) {
			player.sendMessage("Currect usage: ::tmute minutes playername");
			return true;
		    }

		    final int minutes = parser.nextInt();

		    final String playerToMute = parser.nextLine();

		    int secondsToTicks = Misc.secondsToTicks(minutes * 60);

		    Client c2 = (Client) PlayerHandler.getPlayerSmart(playerToMute);

		    if (c2 == null) {
			player.sendMessage(String.format("player=%s is offline.", playerToMute));
			return true;
		    }

		    Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, c2.getName(),
			    "Mute (" + minutes + "m)", ""));
		    c2.sendMessage("You have been muted by: " + player.getNameSmartUp() + " for " + minutes + " minutes");
		    c2.setTimedMute(secondsToTicks);
		    player.sendMessage("You have muted " + c2.getNameSmartUp() + " for " + minutes + " minutes.");
		    return true;
		} catch (Exception e) {
		    player.sendMessage("Currect usage: ::minutes hours playername");
		}
	    }
	    return true;

	case "rush":
	    if (parser.hasNext()) {
		try {
		    String playerPunish = parser.nextString();
		    Client c2 = (Client) PlayerHandler.getPlayerSmart(playerPunish);
		    if (c2 != null) {
			if (AntiRush.getRusherUUID().contains(c2.UUID)) {
			    player.sendMessage("That player is already set to AntiRush mode.");
			    return true;
			}
			Server.getSlackApi()
				.call(SlackMessageBuilder.buildJudgementMesssage(player, c2.getName(), "AntiRush", ""));
			c2.sendMessage("You have been added to AntiRush list.");
			c2.sendMessage("You won't get any PK rewards for 1 day.");
			player.sendMessage("You have set " + c2.getNameSmartUp() + " in AntiRush mode.");
			AntiRush.getRusherUUID().add(c2.UUID);
		    } else {
			player.sendMessage("Player is not online. PlayerName:" + playerPunish);
		    }
		    return true;
		} catch (Exception e) {
		    player.sendMessage("Player is Offline.");
		    return false;
		}
	    }
	    return true;

	case "hide":
	    if (!player.hidePlayer) {
		player.hidePlayer = true;
		player.sendMessage("You hide yourself.");
		player.displayEquipment[PlayerConstants.playerHat] = 20728;
		player.headIconPk = 3;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	    } else {
		player.hidePlayer = false;
		player.sendMessage("You reveal yourself to everyone.");
		player.displayEquipment[PlayerConstants.playerHat] = 0;
		player.headIconPk = -1;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	    }
	    return true;

	case "xrush":
	    if (parser.hasNext()) {
		try {
		    String playerPunish = parser.nextLine();
		    Client c2 = (Client) PlayerHandler.getPlayerSmart(playerPunish);
		    if (c2 != null) {
			if (!AntiRush.getRusherUUID().contains(c2.UUID)) {
			    player.sendMessage("That player is not set to AntiRush mode.");
			    return true;
			}
			// Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(c,
			// c2.getName(), "AntiRush", ""));
			c2.sendMessage("You have been removed from AntiRush list.");
			player.sendMessage("You have removed " + c2.getNameSmartUp() + " from AntiRush mode.");
			AntiRush.getRusherUUID().remove(c2.UUID);
		    } else {
			player.sendMessage("Player is not online. PlayerName:" + playerPunish);
		    }
		    return true;
		} catch (Exception e) {
		    player.sendMessage("Player is probably Offline.");
		    return false;
		}
	    }
	    return true;

	case "mutemac":
	    if (parser.hasNext()) {
		try {

		    CommandParser parser2 = CommandParser.split(parser, "-");

		    String punishedName = parser2.nextString();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    String reason = parser2.nextString();

		    if (reason.length() < 1) {
			player.sendMessage("You must enter a reason for your mute.");
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.MUTE_UUID.getPunishmentType(), 0, reason);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.MUTE_UUID.getDesc(), reason));
		    });
		    player.sendMessage("Sent MAC mute request on playername: " + punishedName);

		    return true;
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::mutemac test player-give a reason");
		}

	    }
	    return true;

	case "muteip":
	    if (parser.hasNext()) {
		try {

		    CommandParser parser2 = CommandParser.split(parser, "-");

		    String punishedName = parser2.nextString();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    String reason = parser2.nextString();

		    if (reason.length() < 1) {
			player.sendMessage("You must enter a reason for your mute.");
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.MUTE_IP.getPunishmentType(), 0, reason);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.MUTE_IP.getDesc(), reason));
		    });
		    player.sendMessage("Sent IP mute request on playername: " + punishedName);
		    return true;
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::muteip player name-some reason");
		}
	    }
	    return true;

	case "mute":
	    if (parser.hasNext()) {
		try {

		    CommandParser parser2 = CommandParser.split(parser, "-");

		    if (parser2.hasNext(2)) {
			String punishedName = parser2.nextString();

			if (punishedName.isEmpty()) {
			    return true;
			}
			String reason = parser2.nextString();

			if (reason.length() < 1) {
			    player.sendMessage("You must enter a reason for your mute.");
			    return true;
			}

			LoginServerConnection.instance().submit(() -> {
			    LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				    Punishment.MUTE_NAME.getPunishmentType(), 0, reason);
			    Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				    Punishment.MUTE_NAME.getDesc(), reason));
			});
			player.sendMessage("Sent mute request on playername: " + punishedName);

			return true;
		    }

		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::mute player name-reason");
		}
	    }
	    return true;
	    
	case "muteyell":
	    if (parser.hasNext()) {
		try {

		    String punishedName = parser.nextLine();

		    if (punishedName.length() < 1) {
		    	return true;
		    }
		    
		    Player other = PlayerHandler.getPlayer(punishedName);
		    if (other == null) { 
		    	player.sendMessage("Player offline");
		    	return true;
			}
		    
		    other.setYellMuted(true);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				"Muted yell", ""));
			
		    player.sendMessage("Yell muted: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::muteyell playername");
		}
	    }
	    return true;
	    
	case "unmuteyell":
	    if (parser.hasNext()) {
		try {

		    String punishedName = parser.nextLine();

		    if (punishedName.length() < 1) {
		    	return true;
		    }
		    
		    Player other = PlayerHandler.getPlayer(punishedName);
		    if (other == null) { 
		    	player.sendMessage("Player offline");
		    	return true;
			}
		    
		    other.setYellMuted(false);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				"Unmuted yell", ""));
			
		    player.sendMessage("Yell unmuted: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unmuteyell playername");
		}
	    }
	    return true;

	case "unmutemac":
	    if (parser.hasNext()) {
		try {
		    String punishedName = parser.nextLine();

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.UNMUTE_UUID.getPunishmentType(), 0, "unmute");
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.UNMUTE_UUID.getDesc(), ""));
		    });
		    player.sendMessage("Sent MAC unmute request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unmutemac playername");
		}
		return true;
	    }
	    return true;

	case "unmuteip":
	    if (parser.hasNext()) {
		try {
		    String punishedName = parser.nextLine();

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.UNMUTED_IP.getPunishmentType(), 0, "unmute");
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.UNMUTED_IP.getDesc(), ""));
		    });
		    player.sendMessage("Sent IP unmute request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unmuteip playername");
		}
	    }
	    return true;

	case "unmute":
	    if (parser.hasNext()) {
		try {

		    final String punishedName = parser.nextLine();

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.UNMUTED_NAME.getPunishmentType(), 0, "unmute");
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.UNMUTED_NAME.getDesc(), ""));
		    });
		    player.sendMessage("Sent unmute request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unmute playername");
		}
	    }
	    return true;

	case "banmac":
	    if (parser.hasNext()) {
		try {

		    CommandParser parser2 = CommandParser.split(parser, "-");

		    final String punishedName = parser2.nextString();

		    final String reason = parser2.nextString();

		    if (reason.length() < 1) {
			player.sendMessage("You must enter a reason for your mac ban.");
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.BAN_UUID.getPunishmentType(), 0, reason);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.BAN_UUID.getDesc(), reason));
		    });
		    player.sendMessage("Sent MAC ban request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::banmac player name-some reason");
		}
	    }
	    return true;

	case "ipban":
	case "banip":
	    if (parser.hasNext()) {
		try {

		    CommandParser parser2 = CommandParser.split(parser, "-");

		    final String punishedName = parser2.nextString();

		    final String reason = parser2.nextString();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    if (reason.length() < 1) {
			player.sendMessage("You must enter a reason for your ip ban.");
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.BAN_IP.getPunishmentType(), 0, reason);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.BAN_IP.getDesc(), reason));
		    });
		    player.sendMessage("Sent IP ban request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::banip player name-some reason");
		}
	    }
	    return true;

	case "ban":
	    if (parser.hasNext()) {
		try {

		    CommandParser parser2 = CommandParser.split(parser, "-");

		    final String punishedName = parser2.nextString();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    final String reason = parser2.nextString();

		    if (reason.length() < 1) {
			player.sendMessage("You must enter a reason for your perma ban.");
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.BAN_NAME.getPunishmentType(), 0, reason);
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.BAN_NAME.getDesc(), reason));
		    });
		    player.sendMessage("Sent ban request on playername " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::ban player name-some reason");
		}
	    }
	    return true;

	case "unbanmac":
	    if (parser.hasNext()) {
		try {

		    String punishedName = parser.nextLine();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.UNBAN_UUID.getPunishmentType(), 0, "unban");
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.UNBAN_UUID.getDesc(), ""));
		    });
		    player.sendMessage("Sent MAC unban request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unbanmac playername");
		}
	    }
	    return true;

	case "unbanip":
	    if (parser.hasNext()) {
		try {

		    final String punishedName = parser.nextLine();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.UNBAN_IP.getPunishmentType(), 0, "unban");
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.UNBAN_IP.getDesc(), ""));
		    });
		    player.sendMessage("Sent IP unban request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unbanip playername");
		}
	    }
	    return true;

	case "unban":
	    if (parser.hasNext()) {
		try {
		    String punishedName = parser.nextLine();

		    if (punishedName.length() < 1) {
			return true;
		    }

		    LoginServerConnection.instance().submit(() -> {
			LoginServerConnection.instance().punishPlayer(player.getName(), punishedName,
				Punishment.UNBAN_NAME.getPunishmentType(), 0, "unban");
			Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage(player, punishedName,
				Punishment.UNBAN_NAME.getDesc(), ""));
		    });
		    player.sendMessage("Sent unban request on playername: " + punishedName);
		} catch (Exception ex) {
		    player.sendMessage("Command Usage - ::unban playername");
		}
	    }
	    return true;

	case "sethelper":
	    player.playerTitle = "Helper";
	    player.titleColor = 2;
	    player.sendMessage("You change your title to Helper.");
	    return true;

	case "scanwealth":
	    for (int i = 0; i < PlayerHandler.players.length; i++) {
		if (PlayerHandler.players[i] != null && !PlayerHandler.players[i].disconnected) {
		    Client o = (Client) PlayerHandler.players[i];
		    long value = 0;
		    // scan inv
		    for (int inv = 0; inv < o.playerItems.length; inv++) {
			if (o.playerItems[inv] > 0) {
				ItemDefinition itemDef = ItemDefinition.forId(o.playerItems[inv] - 1);
				if (itemDef != null) {
					value += (itemDef.getDropValue() * o.playerItemsN[inv]);
				}
			}
		    }
		    // scan equip
		    for (int element : o.playerEquipment) {
			if (element > 0) {
			    value += ItemDef.forID(element).getValue();
			}
		    }

		    for (int playerShopItem : o.playerShopItems) {
			if (playerShopItem > 0) {
			    value += ItemDef.forID(playerShopItem).getValue();
			}
		    }

		    // scan banks
		    for (int b0 = 0; b0 < o.bankItems0.length; b0++) {
			if (o.bankItems0[b0] > 0) {
			    value += (ItemDef.forID(o.bankItems0[b0] - 1).getValue() * o.bankItems0N[b0]);
			}
		    }
		    for (int b1 = 0; b1 < o.bankItems1.length; b1++) {
			if (o.bankItems1[b1] > 0) {
			    value += (ItemDef.forID(o.bankItems1[b1] - 1).getValue() * o.bankItems1N[b1]);
			}
		    }
		    for (int b2 = 0; b2 < o.bankItems2.length; b2++) {
			if (o.bankItems2[b2] > 0) {
			    value += (ItemDef.forID(o.bankItems2[b2] - 1).getValue() * o.bankItems2N[b2]);
			}
		    }
		    for (int b3 = 0; b3 < o.bankItems3.length; b3++) {
			if (o.bankItems3[b3] > 0) {
			    value += (ItemDef.forID(o.bankItems3[b3] - 1).getValue() * o.bankItems3N[b3]);
			}
		    }
		    for (int b4 = 0; b4 < o.bankItems4.length; b4++) {
			if (o.bankItems4[b4] > 0) {
			    value += (ItemDef.forID(o.bankItems4[b4] - 1).getValue() * o.bankItems4N[b4]);
			}
		    }
		    for (int b5 = 0; b5 < o.bankItems5.length; b5++) {
			if (o.bankItems5[b5] > 0) {
			    value += (ItemDef.forID(o.bankItems5[b5] - 1).getValue() * o.bankItems5N[b5]);
			}
		    }
		    for (int b6 = 0; b6 < o.bankItems6.length; b6++) {
			if (o.bankItems6[b6] > 0) {
			    value += (ItemDef.forID(o.bankItems6[b6] - 1).getValue() * o.bankItems6N[b6]);
			}
		    }
		    for (int b7 = 0; b7 < o.bankItems7.length; b7++) {
			if (o.bankItems7[b7] > 0) {
			    value += (ItemDef.forID(o.bankItems7[b7] - 1).getValue() * o.bankItems7N[b7]);
			}
		    }
		    for (int b8 = 0; b8 < o.bankItems8.length; b8++) {
			if (o.bankItems8[b8] > 0) {
			    value += (ItemDef.forID(o.bankItems8[b8] - 1).getValue() * o.bankItems8N[b8]);
			}
		    }

		    if (value > 1000000000) {
			player.sendMessage("PlayerName:(" + o.getNameSmartUp() + ") value is " + Misc.formatNumbersWithCommas(value));
		    }

		}
	    }
	    return true;

	case "checkjava": {
	    int java7Count = 0;
	    int java8Count = 0;
	    for (Player p : PlayerHandler.players) {
		if (p == null || !p.isActive) {
		    continue;
		}

		if (p.javaVersion == 7) {
		    java7Count++;
		} else if (p.javaVersion == 8) {
		    java8Count++;
		} else {
		    player.sendMessage("What kind of java version is :" + p.javaVersion);
		}
	    }

	    player.sendMessage("Java 7 Count:" + java7Count);
	    player.sendMessage("Java 8 Count:" + java8Count);
	}
	    return true;

	case "checkip":
	    if (parser.hasNext()) {

		final String playerName = parser.nextLine();

		Optional<Client> result = World.search(playerName);

		if (result.isPresent()) {

		    Client other = result.get();

		    if (player.isOwner()) {
			player.sendMessage(other.getNameSmartUp() + "'s IP is : " + other.connectedFrom);
		    } else {
			player.sendMessage(other.getNameSmartUp() + "'s IP is : " + other.connectedFrom.split(Pattern.quote("."))[0]
					+ ".***.***." + other.connectedFrom.split(Pattern.quote("."))[3]);
		    }

		} else {
		    player.sendMessage("Player is offline or does not exist.");
		}

	    }
	    return true;

	case "scanip":
	    if (parser.hasNext()) {
		try {

		    final String playerName = parser.nextLine();

		    if (playerName.matches("[A-Za-z0-9 ]+")) { // probably name
							       // typed in
			player.sendMessage("Beginning to scan Name:" + playerName);

			String ipFound = "";

			final Client o = (Client) PlayerHandler.getPlayerSmart(playerName);
			if (o == null) {
			    player.sendMessage("Player is offline: " + playerName);
			    return true;
			} else {
			    ipFound = o.connectedFrom;
			    player.sendMessage("Player is online: " + playerName);
			}
			if (!ipFound.equals("")) {

			    String[] ipParts = /* p.connectedFrom */ipFound.split("\\.");
			    String ip = ipParts[0] + ".***.***." + ipParts[3];

			    for (Player p : PlayerHandler.players) {
				if (p != null) {
				    if (p.connectedFrom.equals(ipFound)) {
					player.sendMessage("IP found player: " + p.getNameSmart() + " (" + ip + ")");
				    }
				}
			    }
			} else {
			    player.sendMessage("Player not found, try IP.");
			}
		    } else { // probably typed in IP
			player.sendMessage("Beginning to scan IP:" + playerName);
			for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			    if (PlayerHandler.players[i] != null) {
				if (PlayerHandler.players[i].connectedFrom != null
					&& PlayerHandler.players[i].connectedFrom.equals(playerName)) {
				    player.sendMessage(PlayerHandler.players[i].connectedFrom + " IP found player: "
					    + PlayerHandler.players[i].getNameSmartUp());
				}
			    }
			}
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	    return true;

	case "unstuck":
	    if (parser.hasNext()) {

		Optional<Client> result = World.search(parser.nextLine());

		if (result.isPresent()) {

		    Client other = result.get();

		    if (other.performingAction) {
			other.performingAction = false;
			if (other.deathContainer != null)
				other.deathContainer.stop();
			other.sendMessage("Got player " + other.getNameSmartUp() + " unstuck successfully.");
		    }

		} else {
		    player.sendMessage("Player is offline or does not exist.");
		}

	    }
	    return true;

	}
	return false;
    }

	public static boolean teleportRestrictions(Player player, Player other) {
		if (!player.isDev()) {
			if (player.duelStatus == 5 || other.duelStatus == 5) {
				player.sendMessage("You can't teleport while either one of you are in a duel.");
				return false;
			}

			if (player.getZones().isInDmmTourn() || other.getZones().isInDmmTourn()) {
				player.sendMessage("You can't teleport while either one of you are in a tournament.");
				return false;
			}

			if (player.insideDungeoneering() || other.insideDungeoneering()) {
				player.sendMessage("You can't teleport while either one of you are in Dungeoneering.");
				return false;
			}

			if (player.inNomadsEntrance() || player.inNomadsRoom() || other.inNomadsEntrance()) {
				player.sendMessage("You can't teleport while either one of you are in Nomads dungeon.");
				return false;
			}

			if (other.inCw() || player.inCw()) {
				player.sendMessage("You can't teleport while either one of you are in Castle wars.");
				return true;
			}

			if (player.playerIsInHouse) {
				player.sendMessage("You cannot teleport to a player while in a house.");
				return false;
			}
		}

		return true;
	}

    @Override
    public boolean canAccess(Client player) {
    	return player.isMod() || player.isAdmin() || Config.isOwner(player);
    }

}
