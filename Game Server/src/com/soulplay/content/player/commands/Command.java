package com.soulplay.content.player.commands;

import com.soulplay.game.model.player.Client;

public interface Command {
	
	boolean execute(Client player, CommandParser parser, String realCommand);
	
	boolean canAccess(Client player);

}

