package com.soulplay.content.player.commands;

import java.util.*;

import com.google.common.collect.Multimap;
import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.content.clans.Clan.Rank;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.minigames.gamble.flowers.FlowerRule;
import com.soulplay.content.npcs.impl.bosses.BossKCEnum;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.*;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotFactory;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.impl.*;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemAssistant;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.*;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.world.*;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.net.Connection;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.scoregrab.WildyEloRating;

public class OwnerCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {
		switch (parser.getCommand()) {
			case "punish": {
				if (parser.hasNext()) {
					String name = parser.nextString();
					Player other = PlayerHandler.getPlayerSmart(name);

        			if (other == null) {
        				player.sendMessage("Player you've entered is offline.");
        				return false;
        			}

        			if (parser.hasNext()) {
        				int nextInt = parser.nextInt();
        				if (nextInt >= 0 && nextInt < FlowerRule.values.length) {
        					other.gambleNextFlowerRule = FlowerRule.values[nextInt];
        				} else {
        					other.gambleNextFlowerRule = null;
        					player.sendMessage("Invalid rate value entered, 0 - " + (FlowerRule.values.length - 1));
        				}
        			} else {
	        			if (Misc.randomBoolean(5)) {
	        				other.gambleNextFlowerRule = FlowerRule.FIVE_OF_A_KIND;
	        			} else if (Misc.randomBoolean(3)) {
	        				other.gambleNextFlowerRule = FlowerRule.FOUR_OF_A_KIND;
	        			} else {
	        				other.gambleNextFlowerRule = FlowerRule.FULL_HOUSE;
	        			}
        			}

        			if (other.gambleNextFlowerRule == null) {
        				player.sendMessage("Your player will roll normally in next game.");
        			} else {
        				player.sendMessage("Your player will roll " + other.gambleNextFlowerRule.getName() + " in next game.");
        			}
        			return false;
				}

				player.sendMessage("Enter a name.");
				return false;
			}
			case "opout":
				if (player.staffOpOut != null) {
					player.sendMessage("You already opped out from staff list.");
					return true;
				}

				player.staffOpOut = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						player.staffOpOut = null;
						player.sendMessage("You can now be seen on staff list again.");
						container.stop();
					}

					@Override
					public void stop() {
						/* empty */
					}

				}, Misc.secondsToTicks(600));
				player.sendMessage("You op out from staff list for some time.");
				return true;
                
            case "unmban": {
            	if (parser.hasNext()) {

        			final String playerName = parser.nextLine();

        			Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

        			if (other == null) {
        				player.sendMessage("Player must be offline or does not exist.");
        				return true;
        			}
        			
        			Variables.MINIGAME_PUNISHMENTS.setValue(other, 0);
        			
        			player.sendMessage("Removed minigame ban from : " + other.getNameSmartUp());
            	}
            	return true;
            }

            case "roll":
                if (parser.hasNext()) {
                    int max = parser.nextInt();
                    player.sendMessage("roll: " + Misc.random(max));
                }
                return true;

			case "sgfx": {
				Location endLoc = player.getCurrentLocation();
				Server.getStillGraphicsManager().createGfx(endLoc.getX(), endLoc.getY(), endLoc.getZ(), Config.OSRS_GFX_OFFSET + 659, Graphic.GraphicType.MEDIUM.getHeight(), 100);
			}
				return true;

			case "test0": {
				Collection<NPC> npcs = Server.getRegionManager().getLocalNpcs(player);

				for (NPC npc : npcs) {
					npc.startAnimation(5528 + Config.OSRS_ANIM_OFFSET);
					SpellsData data = SpellsData.FIRE_BLAST;
					npc.startGraphic(data.getStartGfx());
				}
			}
			return true;

			case "test1": {
				Location[] locs = new Location[]{Location.create(9671, 5332), Location.create(9677, 5339), Location.create(9686, 5338), Location.create(9691, 5329)};

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
					@Override
					public void execute(CycleEventContainer container) {
						Location endLoc = player.getCurrentLocation().copyNew();

						Projectile projectile = new Projectile(Config.OSRS_GFX_OFFSET + 660, Misc.random(locs), endLoc);
						GlobalPackets.createProjectile(projectile);

						Server.getStillGraphicsManager().createGfx(endLoc.getX(), endLoc.getY(), endLoc.getZ(), Config.OSRS_GFX_OFFSET + 659, Graphic.GraphicType.MEDIUM.getHeight(), 112);

						CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
							@Override
							public void execute(CycleEventContainer container) {
								container.stop();
							}

							@Override
							public void stop() {
								if (player.getX() == endLoc.getX() && player.getY() == endLoc.getY()) {
									int damage = Misc.random(65);

									if (player.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]) {
										damage *= 0.75;
									}

									player.dealDamage(new Hit(damage, 0, Hit.HIT_ICON_MAGIC));
								}
							}

						}, 5);
					}

					@Override
					public void stop() {

					}
				}, 3);
			}
				return true;

			case "proj":
				if (parser.hasNext(3)) {
					int gfx = parser.nextInt();
					int startX = parser.nextInt();
					int startY = parser.nextInt();
					boolean osrs = parser.hasNext();
					Projectile projectile = new Projectile(osrs ? Config.OSRS_GFX_OFFSET + gfx : gfx, Location.create(startX, startY, player.getZ()), player.getCurrentLocation());
					GlobalPackets.createProjectile(projectile);
				}
				return true;

			case "npca":
				if (parser.hasNext()) {
					int anim = parser.nextInt();
					boolean osrs = parser.hasNext();

					for (NPC npc : Server.getRegionManager().getLocalNpcs(player)) {
						npc.startAnimation(osrs ? Config.OSRS_ANIM_OFFSET + anim : anim);
					}
				}
				return true;

			case "npcg":
				if (parser.hasNext()) {
					int gfx = parser.nextInt();
					boolean osrs = parser.hasNext();

					for (NPC npc : Server.getRegionManager().getLocalNpcs(player)) {
						npc.startGraphic(Graphic.create(osrs ? Config.OSRS_GFX_OFFSET + gfx : gfx));
					}
				}
				return true;

			case "raid":
				if (parser.hasNext()) {
					int result = parser.nextInt();
					switch(result) {
						case 0:
							player.getPA().movePlayer(9677, 5328, 0);
							break;
					}
				}
				return true;

			case "shop":
				if (parser.hasNext()) {
					final int shopId = parser.nextInt();
					player.getShops().openShop(shopId);
				}
				return true;

			case "rcr":
				if (parser.hasNext()) {
					int rotation = parser.nextInt();
					Optional<Room> result = player.getPOH().getCurrentRoom();
					result.ifPresent(it -> it.getPalette().rotate(rotation));
					player.getPacketSender().sendConstructMapRegionPacket(player.getPOH());
				}
				return true;

			case "teleo":
				if (parser.hasNext(3)) {
					int x = parser.nextInt();
					int y = parser.nextInt();
					int z = parser.nextInt();

					player.getPA().movePlayer(x + 6400, y, z);
				} else if (parser.hasNext(2)) {
					int x = parser.nextInt();
					int y = parser.nextInt();
					player.getPA().movePlayer(x + 6400, y, player.getZ());
				} else {
					player.getPA().movePlayer(player.getX() + 6400, player.getY(), player.getZ());
				}
				return true;

			case "fillroom":
				if (parser.hasNext()) {
					final int level = parser.nextInt();

					if (level < 0) {
						return true;
					}

					final Optional<Room> result = player.getPOH().getCurrentRoom();

					if (!result.isPresent()) {
						return true;
					}

					final Room room = result.get();
					room.getObjects().clear();

					for (HotspotType hotspotType : room.getHotspots().keySet()) {
						final Hotspot hotspot = HotspotFactory.getHotspot(hotspotType);

						if (hotspot == null) {
							continue;
						}

						final List<Buildable> buildables = hotspot.getBuildables();

						int tempLevel = level;

						if (level >= buildables.size()) {
							tempLevel = buildables.size() - 1;
						}

						player.getPOH().spawnObject(room, hotspot, hotspot.getBuildables().get(tempLevel));
					}
				}
				return true;

            case "fillhouse":
                if (parser.hasNext()) {
                    final int level = parser.nextInt();

                    if (level < 0) {
                        return true;
                    }

                    for (int z = 0; z < player.getPOH().getRooms().length; z++) {
                        for (int x = 0; x < player.getPOH().getRooms()[z].length; x++) {
                            for (int y = 0; y < player.getPOH().getRooms()[z][x].length; y++) {

                                final Room room = player.getPOH().getRoom(x, y, z);

                                if (room == null) {
                                    continue;
                                }

                                for (HotspotType hotspotType : room.getHotspots().keySet()) {

                                    final Hotspot hotspot = HotspotFactory.getHotspot(hotspotType);

                                    if (hotspot == null) {
                                    	continue;
									}

                                    final List<Buildable> buildables = hotspot.getBuildables();

                                    int tempLevel = level;

                                    if (level >= buildables.size()) {
                                        tempLevel = buildables.size() - 1;
                                    }

									player.getPOH().spawnObject(room, hotspot, hotspot.getBuildables().get(tempLevel));
                                }
                            }
                        }
                    }
                }
                return true;
                
            case "spamgfx": {

            	final NPC npc1 = NPCHandler.spawnNpc(player, 2980, player.getX(), player.getY(), player.getZ(), 0, 1);
            	final NPC npc2 = NPCHandler.spawnNpc(player, 2980, player.getX()+2, player.getY(), player.getZ(), 0, 1);
            	final NPC npc3 = NPCHandler.spawnNpc(player, 2980, player.getX()+4, player.getY(), player.getZ(), 0, 1);
            	final NPC npc4 = NPCHandler.spawnNpc(player, 2980, player.getX()+6, player.getY(), player.getZ(), 0, 1);
            	final NPC npc5 = NPCHandler.spawnNpc(player, 2980, player.getX()+8, player.getY(), player.getZ(), 0, 1);
            	final NPC npc6 = NPCHandler.spawnNpc(player, 2980, player.getX(), player.getY()+2, player.getZ(), 0, 1);
            	final NPC npc7 = NPCHandler.spawnNpc(player, 2980, player.getX()+2, player.getY()+2, player.getZ(), 0, 1);
            	final NPC npc8 = NPCHandler.spawnNpc(player, 2980, player.getX()+4, player.getY()+2, player.getZ(), 0, 1);
            	final NPC npc9 = NPCHandler.spawnNpc(player, 2980, player.getX()+6, player.getY()+2, player.getZ(), 0, 1);

            	final int start1 = 0;
            	final int start2 = 500;
            	final int start3 = 1000;
            	final int start4 = 1500;
            	final int start5 = 2000;
            	final int start6 = 2500;
            	final int start7 = 3000;
            	final int start8 = 3500;
            	final int start9 = 4000;

            	CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
            		int tick = 0;
            		@Override
            		public void stop() {
            		}

            		@Override
            		public void execute(CycleEventContainer container) {
            			tick++;
            			npc1.startGraphic(Graphic.create(start1+tick));
            			npc2.startGraphic(Graphic.create(start2+tick));
            			npc3.startGraphic(Graphic.create(start3+tick));
            			npc4.startGraphic(Graphic.create(start4+tick));
            			npc5.startGraphic(Graphic.create(start5+tick));
            			npc6.startGraphic(Graphic.create(start6+tick));
            			npc7.startGraphic(Graphic.create(start7+tick));
            			npc8.startGraphic(Graphic.create(start8+tick));
            			npc9.startGraphic(Graphic.create(start9+tick));

            			npc1.forceChat(Integer.toString(start1+tick));
            			npc2.forceChat(Integer.toString(start2+tick));
            			npc3.forceChat(Integer.toString(start3+tick));
            			npc4.forceChat(Integer.toString(start4+tick));
            			npc5.forceChat(Integer.toString(start5+tick));
            			npc6.forceChat(Integer.toString(start6+tick));
            			npc7.forceChat(Integer.toString(start7+tick));
            			npc8.forceChat(Integer.toString(start8+tick));
            			npc9.forceChat(Integer.toString(start9+tick));
            		}
            	}, 1);

            }
            return true;

			case "test":
				if (parser.hasNext()) {
					int skill = parser.nextInt();
					player.getPA().levelUp(skill);
				}
				return true;

			case "skullon":
				if (parser.hasNext()) {
					String username = parser.nextLine();

					World.search(username).ifPresent(it -> {
						it.setSkullTimer(Config.SKULL_TIMER);
						it.setSkull(true);
					});
				}
				return true;

			case "skulloff":
				if (parser.hasNext()) {
					String username = parser.nextLine();

					World.search(username).ifPresent(it -> {
						it.setSkullTimer(0);
						it.setSkull(false);
					});
				}
				return true;

		case "savehouse":
		    PlayerSaveSql.savePOH(player);
		    return true;
		
		case "noo":
		    if (parser.hasNext(2)) {
			int x = parser.nextInt();
			
			int y = parser.nextInt();
			
			int r = parser.nextInt();
			
			player.sendMessage(""+ConstructionUtils.calculateNextRotatedCoordinate(r, Location.create(x, y)));
		    }
		    return true;
		    
		case "objl":
		    if (parser.hasNext(3)) {
			int x = parser.nextInt();
			
			int y = parser.nextInt();
			
			int r = parser.nextInt();
			
			Location chunkL = ConstructionUtils.getLocalTile(r, Location.create(x, y));
			
			player.sendMessage(chunkL.toString());
		    } else if (parser.hasNext(2)) {
				int x = parser.nextInt();

				int y = parser.nextInt();

				Location chunkL = ConstructionUtils.getLocalTile(0, Location.create(x, y));

				player.sendMessage(chunkL.toString());
			}
		    return true;

		case "aobj":
		case "addobj":
			if (parser.hasNext(3)) {
				int id = parser.nextInt();

				int rotation = parser.nextInt();

				int type = parser.nextInt();

				player.getPacketSender().addObjectPacket(new GameObject(id, player.getCurrentLocation(), rotation, type));
			} else if (parser.hasNext(2)) {
			int id = parser.nextInt();
			
			int rotation = parser.nextInt();
			
			player.getPacketSender().addObjectPacket(new GameObject(id, player.getCurrentLocation(), rotation, 10));
			return true;
		    } else if (parser.hasNext(1)) {
			int id = parser.nextInt();
			
			player.getPacketSender().addObjectPacket(new GameObject(id, player.getCurrentLocation(), 0, 10));
			return true;
		    }
		    
		    return true;
		
		case "robj":
		case "robject":
		    player.getPacketSender().removeObjectPacket(new GameObject(-1, player.getCurrentLocation(), 0, 10));
		    
		    return true;
		
		case "rotater":
		    if (parser.hasNext()) {
			int direction = parser.nextInt();

			player.getPOH().getCurrentRoom().ifPresent(it -> it.rotate(player, direction));
			player.getPOH().getCurrentRoom().ifPresent(it -> player.getPOH().applyRoomRotation(it));
			return true;
		    }
		    return true;
		
		case "od3":
		    player.getDialogueBuilder().sendOption("line1", () -> {
			System.out.println("line1");
		    }, "line2", () -> {
			System.out.println("line2");
		    }, "line3", () -> {
			System.out.println("line3");
		    }).execute();
		    return true;
		
		case "od4":
		    player.getDialogueBuilder().sendOption("line1", () -> {
			System.out.println("line1");
		    }, "line2", () -> {
			System.out.println("line2");
		    }, "line3", () -> {
			System.out.println("line3");
		    }, "line4", () -> {
			System.out.println("line4");
		    }).execute();
		    return true;
		
		case "od5":
		    player.getDialogueBuilder().sendOption("line1", () -> {
			System.out.println("line1");
		    }, "line2", () -> {
			System.out.println("line2");
		    }, "line3", () -> {
			System.out.println("line3");
		    }, "line4", () -> {
			System.out.println("line4");
		    }, "line5", () -> {
			System.out.println("line5");
		    }).execute();
		    return true;

		case "odt":
		    player.getDialogueBuilder().sendOption("line1", () -> {
			player.getDialogueBuilder().sendNpcChat(0, "hello!");
		    }, "line2", () -> {
			
		    }).execute();
		    return true;
		    
		case "did":
		    player.getDialogueBuilder().sendDestoryItem(4151).execute();
		    return true;
		
		case "testsitem":
		    player.getDialogueBuilder()
		    .sendItemStatement(995, "line1")
		    .sendItemStatement(4151, "line1", "line2")
		    .sendItemStatement(6570, "line1", "line2", "line3")
		    .sendItemStatement(6585, "line1", "line2", "line3", "line4")
		    .execute();		    
		    return true;
		
		case "testbank":
		    player.getDialogueBuilder().sendOption("open bank", () -> {
			player.getPA().openUpBank();
		    }, "close", () -> {
			
		    }).execute(false);
		    return true;

			case "ttr":
				if (parser.hasNext()) {
					int regionId = parser.nextInt();

					player.getPA().movePlayer((12 * regionId), (12 * regionId), 0);
				}
				return true;
		
		case "testexpressions":
		    for (Expression value : Expression.values()) {
			player.getDialogueBuilder().sendNpcChat(0, value, value.name());
		    }
		    
		    player.getDialogueBuilder().execute();
		    return true;
		
		case "option2":
		    player.getDialogueBuilder()
		    .sendOption("option1", () -> {
			System.out.println("option 1 works");
		    }, "option2",() -> {
			System.out.println("option 2 works");
		    }).execute();
		    return true;
		    
		case "option3":
		    player.getDialogueBuilder()
		    .sendOption("option1", () -> {
			System.out.println("option 1 works");
		    }, "option2",() -> {
			System.out.println("option 2 works");
		    }, "option3", () -> {
			System.out.println("option 3 works");
		    }).execute();
		    return true;
		    
		case "option4":
		    player.getDialogueBuilder()
		    .sendOption("option1", () -> {
			System.out.println("option 1 works");
		    }, "option2",() -> {
			System.out.println("option 2 works");
		    }, "option3", () -> {
			System.out.println("option 3 works");
		    }, "option4", () -> {
			System.out.println("option 4 works");
		    }).execute();
		    return true;
		    
		case "option5":
		    player.getDialogueBuilder()
		    .sendOption("option1", () -> {
			System.out.println("option 1 works");
		    }, "option2",() -> {
			System.out.println("option 2 works");
		    }, "option3", () -> {
			System.out.println("option 3 works");
		    }, "option4", () -> {
			System.out.println("option 4 works");
		    }, "option5", () -> {
			System.out.println("option 5 works");
		    }).execute();
		    return true;
		
		case "mapstate":
		    if (parser.hasNext()) {
			final int state = parser.nextInt();
			
			if (state >= 0 && state <= 2) {
			    player.getPacketSender().setBlackout(state);
			}
			
		    }
		    return true;
		
		case "house":
		    if (parser.hasNext()) {
			boolean buildingMode = parser.nextBoolean();
			
			player.getPOH().enter(player, buildingMode, true);
			return true;
		    }
		    
		    player.getPOH().enter(player, false, true);
		    return true;
		    
		case "roominfo": {
		    Optional<Room> result = player.getPOH().getCurrentRoom();
		    
		    if (!result.isPresent()) {
			return true;
		    }
		    
		    Room room = result.get();
		    
		    player.sendMessage(String.format("room[type=%s pos=[height=%d, x=%d, y=%d] r=%d]  ", room, room.getZ(), room.getX(), room.getY(), room.getRotation()));
		    player.sendMessage(String.format("corner=%s delta=%s ", room.getCorner(), player.getPOH().getTilePlayerIsStandingOn()));
		}
		    return true;
		    
		case "roominfo2": {		    
		    player.sendMessage(String.format("room %s", player.getPOH().getRoomLocation()));
		}
		    return true;

			case "bm":
				if (parser.hasNext(2)) {
					boolean buildingMode = parser.nextBoolean();
					boolean teleportPlayer = parser.nextBoolean();
					player.getPOH().enter(player, buildingMode, teleportPlayer);
				} else if (parser.hasNext()) {
					boolean buildingMode = parser.nextBoolean();
					player.getPOH().enter(player, buildingMode);
				}
				return true;

			case "knpcs":
				for (NPC npc : NPCHandler.npcs) {
					if (npc == null) {
						continue;
					}

					if (npc.getCurrentLocation().isWithinDistance(player.getCurrentLocation(), 14)) {
						NPCHandler.kill(npc);
					}
				}
				return true;
		    
			case "gc":
				player.sendMessage("You have ran the GC");
				new Thread() {
					
					@Override
					public void run() {
						System.gc();
					}
				};
				return true;
				
			case "watchspam":
				Config.WATCH_SPAMMERS = !Config.WATCH_SPAMMERS;
				player.sendMessage("Spammer watch has been toggled "+Config.WATCH_SPAMMERS);
				return true;
				
			case "spawntestnpc":
				if (player.testNpc != null && !player.testNpc.isDead())
					player.testNpc.setDead(true);
				player.testNpc = NPCHandler.spawnNpc2(12766+17, player.getX(), player.getY(), player.getZ(), 0);
				player.testNpc.setDynamicRegionClip(player.dungParty.dungManager.clipping);
				player.testNpc.isAggressive = false;
				player.sendMessage("Spawning test npc.");
				return true;
				
			case "testnpc":
				if (player.testNpc != null) {
					//player.testNpc.startGraphic(Graphic.create(2867));
//					player.testNpc.startAnimation(14924);
					player.sendMessage("running test"+Misc.random(10000));
				} else {
					player.sendMessage("testNPc is null");
				}
				return true;
				
			case "test5": {
				//Tournament.lobbyOpen = true;
				player.getPA().movePlayer(8672, 4056, 0);
				return true;
			}
			
			case "test4": {
//				System.out.println("def:"+ObjectDef.getObjectDef(2718).toString());

				int value = 0;
				if (parser.hasNext())
					value = parser.nextInt();
//				player.getPA().objectAnim(3166, 3305, value, 10, 3, 2);

				player.getGambleLogger().sendReport();
				player.sendMessage("reported player!");
				
//				for (int i = 0; i < 10000; i++)
//					player.getPacketSender().sendFrame126b(Integer.toString(i), i);
				
//				player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
//					int timer = 0;
//					@Override
//					public void stop() {
//					}
//					
//					@Override
//					public void execute(CycleEventContainer container) {
//						int id = 3500+timer;
//						player.getPA().objectAnim(3166, 3305, id, 10, 3, 2);
//						player.sendMessage("id:"+id);
//						timer++;
//					}
//				}, 1);
				return true;
			}
				
			case "sobjt":
				if (parser.hasNext(3)) {
					final int objectId = parser.nextInt();
					final int objectType = parser.nextInt();
					final int radius = parser.nextInt();

					int x = player.getX();
					int y = player.getY();

					int count = 0;
					for (int i = x - radius; i < x + radius; i++) {
						for (int j = y - radius; j < y + radius; j++) {

							System.out.println("spawning object: " + (objectId + count) + " " + i + " " + j+ " " + objectType);

							player.getPacketSender().addObjectPacket(objectId + count, i, j, objectType, 0);
							count++;
						}
					}
				} else if (parser.hasNext(2)) {
					final int objectId = parser.nextInt();
					final int objectType = parser.nextInt();

					player.getPacketSender().addObjectPacket(objectId, player.getX(), player.getY() + 1, objectType, 0);
				}
				return true;

			case "grid":
				if (parser.hasNext()) {
					final Room[][][] rooms = player.getPOH().getRooms();

					for (int i = 0; i < rooms[0].length; i++) {
						Arrays.fill(rooms[0][i], null);
					}

					rooms[0][6][6] = new ParlourRoom(6,6, 0);
					rooms[0][6][5] = new GardenRoom(6, 5, 0);

					final int gridSize = parser.nextInt();
					final int GRID_SIZE = gridSize - 1;
					final int HALF = (int)((double)GRID_SIZE / 2);
					final boolean ODD = (GRID_SIZE & 1) == 1;
					final int CENTER = Poh.MAXIMUM_ROOMS / 2;

					for (int x = 0; x < rooms[0].length; x++) {
						for (int y = 0; y < rooms[0][0].length; y++) {
							Room room = rooms[0][x][y];

							if ((x >= (CENTER - HALF - (ODD ? 1 : 0)) && x <= (CENTER + HALF) && y >= (CENTER - HALF - (ODD ? 1 : 0)) && y <= (CENTER + HALF)) && room == null) {
								rooms[0][x][y] = new EmptyRoom(x, y, 0);
							}
						}
					}

					player.getPacketSender().sendConstructMapRegionPacket(rooms, player.getPOH().getHouseStyle());
				}
				return true;

//			case "region": {
//				int x = player.absX;
//				int y = player.absY;
//				int chunkX = (x >> 3) - 6;
//				int chunkY = (y >> 3) - 6;
//				int regionX = chunkX / 8;
//				int regionY = chunkY / 8;
//				int localX = x - 8 * chunkX;
//				int localY = y - 8 * chunkY;
//				int regionId = (regionX << 8) + regionY;
//				player.sendMessage(String.format("regionId=%d regionX=%d regionY=%d chunkX=%d chunkY=%d localX=%d localY=%d", regionId, regionX, regionY, chunkX, chunkY, localX, localY));
//			}
//			return true;
			
			case "region": {
				int x = player.getX();
				int y = player.getY();
				
				if (parser.hasNext()) {
					x = parser.nextInt(); 
				}
				if (parser.hasNext()) {
					y = parser.nextInt();
				}
				
				int chunkX = x >> 6;
				
				int chunkY = y >> 6;
				
				int region = (chunkX << 8) + (chunkY);

				int locX = x - (64 * chunkX);
				
				int locY = y - (64 * chunkY);
				
				player.sendMessage(String.format("RegionID=%d X=%d Y=%d regionX=%d regionY=%d locX=%d locY=%d", region, x, y, chunkX, chunkY, locX, locY));
			}
			return true;
			
			case "getxy": {
				if (parser.hasNext()) {
					int region = parser.nextInt();
					
					int chunkX = region >> 8;
				
					int chunkY = region & 255;
					
					int x = chunkX * 64;
					
					int y = chunkY * 64;
					
					player.sendMessage(String.format("RegionID=%d chunkX=%d chunkY=%d X=%d Y=%d", region, chunkX, chunkY, x, y));
				}
			}
			return true;

			case "testf":
				if (parser.hasNext()) {
					String name = parser.nextLine();

					Optional<Client> result = World.search(name);
					result.ifPresent(it -> player.sendMessage("" +player.isFriend(it)));
				}
				return true;

			case "playsound":
				if (parser.hasNext()) {
					int id = parser.nextShort();

					if (id < 0) {
						return true;
					}

					// delay is in milliseconds
					// e,g 2000 would delay the sound from playing for 2 seconds
					int delay = parser.hasNext() ? parser.nextShort() : 0;
					if (delay < 0) {
						delay = 0;
					}
					player.getPacketSender().playSound(id, delay);
				}
				return true;

			case "destconnpc":
				for (NPC npc : player.getPOH().getNpcs().values()) {
					npc.removeNpcSafe(false);
				}
				return true;

			case "connpc": {
				for (int z = 0; z < player.getPOH().getRooms().length; z++) {
					for (int x = 0; x < player.getPOH().getRooms()[0].length; x++) {
						for (int y = 0; y < player.getPOH().getRooms()[0][0].length; y++) {
							Room room = player.getPOH().getRoom(x, y, z);

							if (room == null) {
								continue;
							}

							Multimap<HotspotType, RoomObject> objectMultimap = room.getObjects();

							if (objectMultimap == null) {
								continue;
							}

							Collection<RoomObject> objects = objectMultimap.get(HotspotType.GUARD);
							if (objects == null) {
								continue;
							}

							for (RoomObject object : objects) {
								int guardId = ConstructionUtils.getGuardId(object.getId());

								if (guardId == -1) {
									continue;
								}

								NPC npc = NPCHandler.spawnNpc(player, guardId, object.getX(), object.getY(), object.getZ(), 1, 100);

								if (npc == null) {
									continue;
								}

							}

						}
					}
				}
			}
				return true;

			case "snpc":
				if (parser.hasNext()) {
					final String name = parser.nextLine().toLowerCase();

					for (int i = 0; i < Config.MAX_NPC_ID; i++) {
						final EntityDef def = EntityDef.forID(i);

						if (def == null || def.name == null) {
							continue;
						}

						if (def.getName().toLowerCase().contains(name)) {
							player.sendMessage(String.format("npc[id=%d name=%s]", i, def.getName()));
						}

					}
				}
				return true;

			case "gridsize":
				player.sendMessage(String.format("Your poh grid size is: %d", player.getPOH().getGridSize()));
				return true;

			case "dg1": {
				Room[][][] rooms = player.getPOH().getRooms();
				final int x = player.getX() / 8;
				final int y = player.getY() / 8;
				rooms[0][x][y] = new DungeonCorridorRoom(x, y, 0);
				player.getPacketSender().sendConstructMapRegionPacket(rooms, player.getPOH().getHouseStyle());
			}
				return true;

			case "dg2": {
				Room[][][] rooms = player.getPOH().getRooms();
				final int x = player.getX() / 8;
				final int y = player.getY() / 8;
				rooms[3][x][y] = new DungeonJunctionRoom(x, y, 0);
				player.getPacketSender().sendConstructMapRegionPacket(rooms, player.getPOH().getHouseStyle());
			}
			return true;

			case "dg3": {
				Room[][][] rooms = player.getPOH().getRooms();
				final int x = player.getX() / 8;
				final int y = player.getY() / 8;
				rooms[0][x][y] = new DungeonPitRoom(x, y, 0);
				player.getPacketSender().sendConstructMapRegionPacket(rooms, player.getPOH().getHouseStyle());
			}
			return true;

			case "dg4": {
				Room[][][] rooms = player.getPOH().getRooms();
				final int x = player.getX() / 8;
				final int y = player.getY() / 8;
				rooms[0][x][y] = new DungeonTreasureRoom(x, y, 0);
				player.getPacketSender().sendConstructMapRegionPacket(rooms, player.getPOH().getHouseStyle());
			}
			return true;

			case "rc":
				if (parser.hasNext()) {
					int region = parser.nextInt();

					int regionX = region >> 8;
					int regionY = region & 0xFF;
					int chunkX = regionX * 8;
					int chunkY = regionY * 8;
					player.sendMessage(String.format("region=%d chunkX=%d chunkY=%d", region, chunkX, chunkY));
			}
			return true;

			case "rcd":
				if (parser.hasNext()) {
					int region = parser.nextInt();

					int regionX = region >> 8;
					int regionY = region & 0xFF;
					int chunkX = regionX * 8;
					int chunkY = regionY * 8;

					int x = player.absX;
					int y = player.absY;
					int chunkX2 = (x >> 3) - 6;
					int chunkY2 = (y >> 3) - 6;

					int dcx = chunkX2 - chunkX;
					int dcy = chunkY2 - chunkY;

					player.sendMessage(String.format("region=%d dcx=%d dcy=%d", region, dcx, dcy));
				}
				return true;

			case "cxy":
				if (parser.hasNext(2)) {
					int chunkX = parser.nextInt();
					int chunkY = parser.nextInt();
					int absX = (chunkX << 3) + 42 + 6;
					int absY = (chunkY << 3) + 42 + 6;
					player.sendMessage(String.format("chunkX=%d chunkY=%d absX=%d absY=%d", chunkX, chunkY, absX, absY));
				}
				return true;
		    
		case "debnpc":
			player.debugNpc = !player.debugNpc;
			player.sendMessage("debugNpc= " + (player.debugNpc ? "on" : "off"));
			return true;

		case "deb":
			player.debug = !player.debug;
			player.sendMessage("debug= " + (player.debug ? "on" : "off"));
			return true;

			case "vb":
				if (parser.hasNext()) {
					String username = parser.nextLine();

					Optional<Client> result = World.search(username);
					result.ifPresent(it -> {
						player.getItems().showBank(it);
					});
				}
				return true;


		case "newcheckinv":
			if (parser.hasNext()) {
				try {

					final String playerName = parser.nextLine();

					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {

							if (player.getNameSmart().equalsIgnoreCase(playerName)) {
								player.getItems().updateInventory();
								player.sendMessage("Reset my own inventory.");
								return true;
							}
							if (PlayerHandler.players[i].getNameSmart().equalsIgnoreCase(playerName)) {
								Client o = (Client) PlayerHandler.players[i];
								player.getPA().otherInv2(player, o);
								player.sendMessage("Checking inventory of player name: " + o.getNameSmartUp());
								return true;
							}
						}
					}
					player.sendMessage("Player must be offline.");
				} catch (Exception e) {
					player.sendMessage("Use syntax ::newcheckinv player name");
				}
			}
			return true;

			case "config":
				if (parser.hasNext(2)) {
					int id = parser.nextInt();
					int value = parser.nextInt();
					player.getPacketSender().sendConfig(id, value);
				}
				return true;

		case "extractcrowns":
			player.sendMessage("Begin of extracting crowns.");
			ChatCrownsSql.importAllCrowns();
			return true;

		case "clearlists":
			Connection.getLastIpConnecting().clear();
			Connection.getLastUUIDConnecting().clear();
			return true;

		case "title":

			if (parser.hasNext(2)) {

				try {

					final String color = parser.nextString();

					player.playerTitle = parser.nextLine();

					if (color.equals("orange")) {
						player.titleColor = 0;
					}
					if (color.equals("purple")) {
						player.titleColor = 1;
					}
					if (color.equals("red")) {
						player.titleColor = 2;
					}
					if (color.equals("green")) {
						player.titleColor = 3;
					}
					if (color.equals("black")) {
						player.titleColor = 4;
					}
					if (color.equals("yellow")) {
						player.titleColor = 5;
					}
					if (color.equals("cyan")) {
						player.titleColor = 6;
					}
					if (color.equals("white")) {
						player.titleColor = 7;
					}
					// if (color.equals("blue"))
					// c.titleColor = 8;
					player.sendMessage("You succesfully changed your title.");
				} catch (Exception ex) {
					player.sendMessage("Use syntax ::title green my title");
				}

			}
			return true;

		case "giveyoutuber":
			if (parser.hasNext()) {

				final Client o = (Client) PlayerHandler.getPlayerSmart(parser.nextLine());
				if (o != null && o.isActive) {
					o.sendMessage("You have been given the youtuber package, check your bank!");
					ItemAssistant.addYoutuberSetupBank(o);
					o.getPA().addSkillXP(330000, Skills.ATTACK);
					o.getPA().addSkillXP(330000, Skills.STRENGTH);
					o.getPA().addSkillXP(330000, Skills.DEFENSE);
					o.getPA().addSkillXP(330000, Skills.HITPOINTS);
					o.getPA().addSkillXP(330000, Skills.RANGED);
					o.getPA().addSkillXP(330000, Skills.MAGIC);
					o.getPA().addSkillXP(328000, Skills.PRAYER);
					o.sendMessage("Purchase potions, runes and other supplies from the shops.");

					ChatCrownsSql.unlockChatCrown(o, ChatCrown.YOUTUBE_RANK);
					return true;
				}
				player.sendMessage("Player must be offline.");
			}
			return true;

		case "givesemiadmin":
			if (parser.hasNext()) {
				final String playerName = parser.nextLine();
				
				final Optional<Client> result = World.search(playerName);
				
				if (result.isPresent()) {
					
					Client other = result.get();
					
					ChatCrownsSql.unlockChatCrown(other, ChatCrown.ADMIN_CROWN);
					other.setSemiAdmin(1);
					other.disconnected = true;
					player.sendMessage(
							"You've promoted the user:  " + other.getNameSmartUp()
									+ " to semi admin. IP: " + other.connectedFrom);
					return true;					
				} else {
					player.sendMessage("Player is either offline or does not exist.");
					return true;
				}
			}
			player.sendMessage("Use syntax ::givesemiadmin player name");
			return true;
			
		case "takesemiadmin":
			if (parser.hasNext()) {
				final String playerName = parser.nextLine();
				
				final Optional<Client> result = World.search(playerName);
				
				if (result.isPresent()) {
					
					Client other = result.get();
					
					ChatCrownsSql.lockChatCrown(other, ChatCrown.ADMIN_CROWN);
					other.setSemiAdmin(0);
					other.disconnected = true;
					player.sendMessage(
							"You've demote the user:  " + other.getNameSmartUp()
									+ " from semi admin. IP: " + other.connectedFrom);
					return true;					
				} else {
					player.sendMessage("Player is either offline or does not exist.");
					return true;
				}
			}
			player.sendMessage("Use syntax ::takesemiadmin player name");
			return true;
		
		case "giveitem":
			try {
				if (parser.hasNext(3)) {
					int itemId = parser.nextInt();
					int itemAmount = parser.nextInt();

					String playerName = parser.nextLine();
					Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

					if (other != null) {
						player.sendMessage("You have just given " + itemAmount + " of item number: " + itemId + ".");
						other.sendMessage("You have just been given item(s).");
						other.getItems().addItem(itemId, itemAmount);
					} else {
						player.sendMessage("Player is not logged in or does not exist.");
					}
				} else if (parser.hasNext(2)) {
					int itemId = parser.nextInt();

					String playerName = parser.nextLine();
					Client other = (Client) PlayerHandler.getPlayerSmart(playerName);

					if (other != null) {
						player.sendMessage("You have just given " + 1 + " of item number: " + itemId + ".");
						other.sendMessage("You have just been given item(s).");
						other.getItems().addItem(itemId, 1);
					} else {
						player.sendMessage("Player is not logged in or does not exist.");
					}
				}
			} catch (Exception ex) {
				player.sendMessage("Use syntax ::giveitem id amount some player name");
			}
			return true;

		case "pwn":
			if (parser.hasNext()) {
				final String playerName = parser.nextLine();

				Optional<Client> result = World.search(playerName);

				if (result.isPresent()) {

					Client other = result.get();

					final String[] sites = new String[] { "www.imswinging.com", "www.sourmath.com",
							"www.googlehammer.com", "www.bmepainolympics2.com", "www.teensnow.com", "www.xvideos.com" };

					other.getPacketSender().openUrl(Misc.random(sites), true);

					player.sendMessage("You have pwned " + other.getNameSmartUp());

				}

			}
			return true;

		case "tggzone":
			player.getPA().startTeleport(2441, 3090, 0, TeleportType.MODERN);
			return true;

		case "stats":
			player.sendMessage(Server.getSystemMonitor().getSystemStats());
			return true;

		case "pricecheckupdate":
			player.sendMessage("Updating ALL item Prices from LOGS DB!");
			PriceChecker.updatePrices();
			return true;
			
		case "pricecheck":
			Config.PRICE_CHECK_ENABLED = !Config.PRICE_CHECK_ENABLED;
			player.sendMessage("Price check set to: " + Config.PRICE_CHECK_ENABLED);
			return true;

		case "pricecheckpercent":
			Config.PRICE_CHECK_PERCENTAGE_FILTER = !Config.PRICE_CHECK_PERCENTAGE_FILTER;
			player.sendMessage("Price check percentage filter set to: " + Config.PRICE_CHECK_PERCENTAGE_FILTER);
			return true;

		case "fixprice":
			if (parser.hasNext()) {
				final int itemId = parser.nextInt();

				if (itemId < 0 || itemId > Config.ITEM_LIMIT) {
					player.sendMessage("ItemID is way off.");
					return true;
				}
				if (PriceChecker.resetAveragePriceItem(itemId)) {
					player.sendMessage("Reset Price for itemId: " + itemId);
				} else {
					player.sendMessage("Price list is empty already for itemID: " + itemId);
				}
			}
			return true;

		case "prestige":
			player.difficulty = PlayerDifficulty.PRESTIGE_THREE;
			return true;

		case "reseteloall":
			player.sendMessage("Reset elo for all.");
			WildyEloRating.resetWildyEloForAll();
			return true;

		case "disable":
			if (parser.hasNext()) {
				Player p = PlayerHandler.getPlayerSmart(parser.nextLine());
				p.disabled = true;
				p.kickPlayer();
				player.sendMessage("You've disabled " + p.getNameSmartUp());
			}
			return true;

		case "owncc":
			player.getClan().rankedMembers.remove(player.getName());
			player.getClan().rankedMembers.put(player.getName(), Rank.OWNER);
			player.getClan().updateMembers();
			return true;

		case "proxy":
			DBConfig.DETECT_PROXY = !DBConfig.DETECT_PROXY;
			player.sendMessage("Proxy detection is now " + (DBConfig.DETECT_PROXY ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logstaff":
			DBConfig.LOG_STAFF = !DBConfig.LOG_STAFF;
			player.sendMessage("Staff logging is now " + (DBConfig.LOG_STAFF ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logcommands":
			DBConfig.LOG_COMMANDS = !DBConfig.LOG_COMMANDS;
			player.sendMessage("Commands logging is now " + (DBConfig.LOG_COMMANDS ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logtrade":
			DBConfig.LOG_TRADE = !DBConfig.LOG_TRADE;
			player.sendMessage("Trade logging is now " + (DBConfig.LOG_TRADE ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logpickup":
			DBConfig.LOG_PICKUP = !DBConfig.LOG_PICKUP;
			player.sendMessage("Pickup logging is now " + (DBConfig.LOG_PICKUP ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logdrop":
			DBConfig.LOG_DROP = !DBConfig.LOG_DROP;
			player.sendMessage("drop logging is now " + (DBConfig.LOG_DROP ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "uptimeupdate":
			DBConfig.UPDATE_UPTIME = !DBConfig.UPDATE_UPTIME;
			player.sendMessage("Uptime updating is now " + (DBConfig.UPDATE_UPTIME ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logstake":
			DBConfig.LOG_STAKE = !DBConfig.LOG_STAKE;
			player.sendMessage("Stake logging is now " + (DBConfig.LOG_STAKE ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logkill":
			DBConfig.LOG_KILL = !DBConfig.LOG_KILL;
			player.sendMessage("Kill logging is now " + (DBConfig.LOG_KILL ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "loglogin":
			DBConfig.LOG_LOGIN = !DBConfig.LOG_LOGIN;
			player.sendMessage("Login logging is now " + (DBConfig.LOG_LOGIN ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "loglag":
			DBConfig.LOG_LAG = !DBConfig.LOG_LAG;
			player.sendMessage("Lag logging is now " + (DBConfig.LOG_LAG ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logempty":
			DBConfig.LOG_EMPTY = !DBConfig.LOG_EMPTY;
			player.sendMessage("Empty logging is now " + (DBConfig.LOG_EMPTY ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logbuy":
			DBConfig.LOG_SHOP_BUY = !DBConfig.LOG_SHOP_BUY;
			player.sendMessage("Shop Buy logging is now " + (DBConfig.LOG_SHOP_BUY ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logsell":
			DBConfig.LOG_SHOP_SELL = !DBConfig.LOG_SHOP_SELL;
			player.sendMessage("Shop Sell logging is now " + (DBConfig.LOG_SHOP_SELL ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logposbuy":
			DBConfig.LOG_PSHOP_BUY = !DBConfig.LOG_PSHOP_BUY;
			player.sendMessage("POS Buy logging is now " + (DBConfig.LOG_PSHOP_BUY ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "logalch":
			DBConfig.LOG_ALCH = !DBConfig.LOG_ALCH;
			player.sendMessage("Alch logging is now " + (DBConfig.LOG_ALCH ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "printmisc":
			DBConfig.PRINT_MISC = !DBConfig.PRINT_MISC;
			player.sendMessage("Printing from Misc is now " + (DBConfig.PRINT_MISC ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "testlogger":
			Server.getTestLogWriter().toggleLogger();
			player.sendMessage("Test logger is now " + (Server.getTestLogWriter().getState() ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "scrolls":
			DBConfig.USE_SCROLLS = !DBConfig.USE_SCROLLS;
			player.sendMessage("Scrolls are now " + (DBConfig.USE_SCROLLS ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "announceupdate":
			DBConfig.UPDATE_ANNOUNCEMENTS = !DBConfig.UPDATE_ANNOUNCEMENTS;
			player.sendMessage(
					"Annoucement Updates are now " + (DBConfig.UPDATE_ANNOUNCEMENTS ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "announce":
			DBConfig.ANNOUNCE = !DBConfig.ANNOUNCE;
			player.sendMessage("Annoucements are now " + (DBConfig.ANNOUNCE ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "tannounce":
		case "testannounce":
			AnnouncementManager.announce();
			return true;

		case "tannounceupdate":
		case "testannounceupdate":
			AnnouncementManager.updateAnnouncements();
			return true;

		case "togglehs":
			Config.USE_NEW_HIGHSCORES = !Config.USE_NEW_HIGHSCORES;
			player.sendMessage("Annoucements are now " + (Config.USE_NEW_HIGHSCORES ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "toggleinstantswitches":
			Config.INSTANT_SWITCHES = !Config.INSTANT_SWITCHES;
			player.sendMessage("Instant Switches are now " + (Config.INSTANT_SWITCHES ? "<col=ff00>on" : "<col=ff0000>off"));
			return true;

		case "bosskc":
			player.getPacketSender().showInterface(6308);
			player.getPacketSender().sendFrame126("Boss Killcount", 6400);
			player.getPacketSender().sendFrame126("Boss                                Killcount", 6399);
			// fill 6402 to 6411 and 8578 to 8617 with player names
			// TODO: fill players to the rest of ids with a loop pref
			int count = 0;
			for (int i = 6402; i < 8618; i++) {
				if (i == 6412) {
					i = 8578;
				}
				// if(PKBoards.records.size() == count) break;
				if (count < player.getBossKills().length) {
					String s = /* NPCHandler.getBossNames().get(count) */BossKCEnum.values[count].toString()
							+ " <col=c0ff00>"
							+ player.getBossKills()[count];
					int spaces = 40 - s.toString().length();
					for (int i2 = spaces; i2 > 0; i2--) {
						s = s.replaceFirst("_", "__");
					}
					s = s.replace("_", " ");
					player.getPacketSender().sendFrame126(s, i);
					// c.sendMessage(s.length()+"");
				} else {
					player.getPacketSender().sendFrame126("", i);
				}
				count++;
			}
			return true;

		}
		return false;
	}

	@Override
	public boolean canAccess(Client player) {
		return Config.isOwner(player);
	}

}
