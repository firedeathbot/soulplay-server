package com.soulplay.content.player.commands;

import com.soulplay.Config;
import com.soulplay.content.player.DonatorRank;
import com.soulplay.content.player.TeleportType;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.player.Client;

public final class RegularDonatorCommands implements Command {

	@Override
	public boolean execute(Client player, CommandParser parser, String realCommand) {
		switch(parser.getCommand()) {
			case "dz":
				player.getPA().startTeleport(LocationConstants.LAND_OF_SERENITY.getX(), LocationConstants.LAND_OF_SERENITY.getY(), 0, TeleportType.MODERN);
				return true;
		}

		return false;
	}

	@Override
	public boolean canAccess(Client player) {
		return DonatorRank.check(player) != DonatorRank.NONE || Config.isModerator(player) || Config.isStaff(player);
	}

}
