package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class ToolStore3 extends Buildable {
    @Override
    public String getName() {
        return "Tool store 3";
    }

    @Override
    public int getItemId() {
        return 8386;
    }

    @Override
    public int getObjectId() {
        return 13701;
    }

    @Override
    public int getLevelRequired() {
        return 35;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(8385), new Item(ConstructionItems.OAK_PLANK, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 120;
    }
}
