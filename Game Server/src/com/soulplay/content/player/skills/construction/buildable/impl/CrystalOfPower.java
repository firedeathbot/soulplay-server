package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class CrystalOfPower extends Buildable {
    @Override
    public String getName() {
        return "Crystal of power";
    }

    @Override
    public int getItemId() {
        return 8353;
    }

    @Override
    public int getObjectId() {
        return 13661;
    }

    @Override
    public int getLevelRequired() {
        return 66;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 2), new Item(567), new Item(ConstructionItems.GOLD_LEAF, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 890;
    }
}
