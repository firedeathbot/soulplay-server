package com.soulplay.content.player.skills.construction.specialobjects;

import java.util.HashMap;
import java.util.Map;

public enum WhiteberryEnum {

	BUSH1(7721, 7722, 7721),
	BUSH2(7722, 7723, 7721),
	BUSH3(7723, 7724, 7722),
	BUSH4(7724, 7725, 7723),
	BUSH5(7725, 7725, 7724);
	
	private final int object;
	private final int nextObj;
	private final int previousObject;
	
	private WhiteberryEnum(int currentState, int nextState, int prevState) {
		this.object = currentState;
		this.nextObj = nextState;
		this.previousObject = prevState;
	}

	public int getObject() {
		return object;
	}

	public int getNextObj() {
		return nextObj;
	}
	
	private static final Map<Integer, WhiteberryEnum> map = new HashMap<>();
	
	static {
		for (WhiteberryEnum w : WhiteberryEnum.values()) {
			map.put(w.getObject(), w);
		}
	}
	
	public static WhiteberryEnum get(int id) {
		return map.get(id);
	}

	public int getPreviousObject() {
		return previousObject;
	}
}
