package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class DungeonCorridorRoom extends Room {

	private static final Palette palette = new Palette(6, -3);

	private static final boolean[] doors =  {true, false, true, false};

	public DungeonCorridorRoom(int x, int y, int z) {
		super(RoomType.DUNGEON_CORRIDOR, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(3, 1), 0, 4));
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(3, 6), 0, 4));
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(4, 1), 2, 4));
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(4, 6), 2, 4));
		hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(3, 4), 0, 4));
		hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(4, 3), 2, 4));
		hotspots.put(HotspotType.GUARD, new RoomObject(15323, Location.create(3, 3), 2, 10));
		hotspots.put(HotspotType.DUNGEON_TRAP, new RoomObject(15325, Location.create(3, 2), 0, 22));
		hotspots.put(HotspotType.DUNGEON_TRAP, new RoomObject(15325, Location.create(4, 2), 0, 22));
		hotspots.put(HotspotType.DUNGEON_TRAP, new RoomObject(15324, Location.create(3, 5), 0, 22));
		hotspots.put(HotspotType.DUNGEON_TRAP, new RoomObject(15324, Location.create(4, 5), 0, 22));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15329, Location.create(3, 1), 3, 0));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15328, Location.create(4, 1), 3, 0));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15327, Location.create(4, 6), 1, 0));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15326, Location.create(3, 6), 1, 0));
	}

}
