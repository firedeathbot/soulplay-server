package com.soulplay.content.player.skills.construction;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.ObjectDefVersion;
import com.soulplay.util.Misc;

public class DynamicRegionClip {

	private static final int SIZE = 64;
	
	public class RegionC {
		
		private int id;

		private int[][][] clips;
		
		public RegionC(int id) {
			this.id = id;
			this.clips = new int[4][SIZE][SIZE];
		}
		
		private void addClip(int x, int y, int z, int shift) {
			int regionAbsX = (id >> 8) * 64;
			int regionAbsY = (id & 0xff) * 64;
			z = z % 4;
			if (clips[z] == null) {
				clips[z] = new int[64][64];
			}
			clips[z][x - regionAbsX][y - regionAbsY] |= shift;
		}
		
		private void removeClip(int x, int y, int z) {
			int regionAbsX = (id >> 8) * 64;
			int regionAbsY = (id & 0xff) * 64;
			z = z % 4;
			if (clips[z] == null) {
				clips[z] = new int[64][64];
			}
			clips[z][x - regionAbsX][y - regionAbsY] = 0;
		}

		private void removeClip(int x, int y, int z, int shift) {
			int regionAbsX = (id >> 8) * 64;
			int regionAbsY = (id & 0xff) * 64;
			z = z % 4;
			if (clips[z] == null) {
				clips[z] = new int[64][64];
			}
			clips[z][x - regionAbsX][y - regionAbsY] &= (0xFFFFFFF - shift);
		}
		
		private void removeObjectClip(int x, int y, int z) {
			int regionAbsX = (id >> 8) * 64;
			int regionAbsY = (id & 0xff) * 64;
			z = z % 4;
			if (clips[z] == null) {
				clips[z] = new int[64][64];
			}
			clips[z][x - regionAbsX][y - regionAbsY] &= ~(1 << 8); // 9th is object clip for walking, or square full, cannot stand on it.
			clips[z][x - regionAbsX][y - regionAbsY] &= ~(1 << 17); // 18th also is object clip mask for projectiles
		}
		
		private int getClip(int x, int y, int z) {
			z = z % 4;
			if (clips[z] == null) {
				return 0;
			}

			int regionAbsX = (id >> 8) * 64;
			int regionAbsY = (id & 0xff) * 64;
			return clips[z][x - regionAbsX][y - regionAbsY];
		}
		
		private void setClip(int x, int y, int z, int clip) {
			z = z % 4;
			if (clips[z] == null) {
				return;
			}

			int regionAbsX = (id >> 8) * 64;
			int regionAbsY = (id & 0xff) * 64;
			clips[z][x - regionAbsX][y - regionAbsY] = clip;
		}
		
		private void rotateWallClip(int x, int y, int z, boolean clockWise) {
			int currentClip = getClip(x, y, z);
			int wallWalkClip = currentClip & 0xFF;// get the walking clip for walls only
			//rotate once, then nondiagonal will go into diagonal version(example:south->southeast) so we will rotate twice
			// 2+6 = 8 bits to rotate, ignoring the 9th which is objects
			int newWalkClip = clockWise ? (wallWalkClip >> 2) | (wallWalkClip << 6) : (wallWalkClip << 2) | (wallWalkClip >> 6);
			currentClip >>= 8; // lesik's retarded midnight cheaphax XD
			currentClip <<= 8; // lesik's retarded midnight cheaphax XD
			int newClip = currentClip | newWalkClip;
			setClip(x, y, z, newClip);
		}
	}
	
	private Map<Integer, RegionC> regions;
	
	public DynamicRegionClip() {
		regions = new HashMap<>();
	}

	private RegionC getRegionByIdOrNull(int regionId) {
		return regions.get(regionId);
	}
	
	private RegionC getOrCreateRegionById(int regionId) {
		RegionC region = regions.get(regionId);
		if (region == null) {
			region = new RegionC(regionId);
			regions.put(regionId, region);
		}

		return region;
	}
	
	private RegionC getRegionOrNull(int x, int y) {
		return getRegionByIdOrNull(getRegionId(x, y));
	}
	
	private int getRegionId(int x, int y) {
		return ((x >> 6) << 8) + (y >> 6);
	}
	
	public int getClipping(int x, int y, int z) {
		RegionC region = getRegionOrNull(x, y);
		if (region == null)
			return 0x100;
		return region.getClip(x, y, z);
	}
	
	public void addClipping(int x, int y, int z, int shift) {
		int regionId = getRegionId(x, y);
		RegionC region = getOrCreateRegionById(regionId);
		region.addClip(x, y, z, shift);
	}

	public void removeClipping(int x, int y, int z) {
		int regionId = getRegionId(x, y);
		RegionC region = getOrCreateRegionById(regionId);
		region.removeClip(x, y, z);
	}

	public void removeClipping(int x, int y, int z, int shift) {
		RegionC region = getOrCreateRegionById(getRegionId(x, y));
		region.removeClip(x, y, z, shift);
	}
	
	private void removeObjectClipping(int x, int y, int z) {
		int regionId = getRegionId(x, y);
		RegionC region = getOrCreateRegionById(regionId);
		region.removeObjectClip(x, y, z);
	}
	
	private void addClippingForSolidObject(int x, int y, int height,
			int xLength, int yLength, boolean flag) {
		int clipping = 256;
		if (flag) {
			clipping += 0x20000;
		}
		for (int i = x; i < x + xLength; i++) {
			for (int i2 = y; i2 < y + yLength; i2++) {
				addClipping(i, i2, height, clipping);
			}
		}
	}

	private void addClippingForVariableObject(Location loc,
			int type, int direction, boolean clipProjectiles) {
		int x = loc.getX();
		int y = loc.getY();
		int height = loc.getZ();
		if (type == 0) {
			if (direction == 0) {
				addClipping(x, y, height, 128);
				addClipping(x - 1, y, height, 8);
			} else if (direction == 1) {
				addClipping(x, y, height, 2);
				addClipping(x, y + 1, height, 32);
			} else if (direction == 2) {
				addClipping(x, y, height, 8);
				addClipping(x + 1, y, height, 128);
			} else if (direction == 3) {
				addClipping(x, y, height, 32);
				addClipping(x, y - 1, height, 2);
			}
		} else if (type == 1 || type == 3) {
			if (direction == 0) {
				addClipping(x, y, height, 1);
				addClipping(x - 1, y, height, 16);
			} else if (direction == 1) {
				addClipping(x, y, height, 4);
				addClipping(x + 1, y + 1, height, 64);
			} else if (direction == 2) {
				addClipping(x, y, height, 16);
				addClipping(x + 1, y - 1, height, 1);
			} else if (direction == 3) {
				addClipping(x, y, height, 64);
				addClipping(x - 1, y - 1, height, 4);
			}
		} else if (type == 2) {
			if (direction == 0) {
				addClipping(x, y, height, 130);
				addClipping(x - 1, y, height, 8);
				addClipping(x, y + 1, height, 32);
			} else if (direction == 1) {
				addClipping(x, y, height, 10);
				addClipping(x, y + 1, height, 32);
				addClipping(x + 1, y, height, 128);
			} else if (direction == 2) {
				addClipping(x, y, height, 40);
				addClipping(x + 1, y, height, 128);
				addClipping(x, y - 1, height, 2);
			} else if (direction == 3) {
				addClipping(x, y, height, 160);
				addClipping(x, y - 1, height, 2);
				addClipping(x - 1, y, height, 8);
			}
		}
		if (clipProjectiles) {
			if (type == 0) {
				if (direction == 0) {
					addClipping(x, y, height, 65536);
					addClipping(x - 1, y, height, 4096);
				} else if (direction == 1) {
					addClipping(x, y, height, 1024);
					addClipping(x, y + 1, height, 16384);
				} else if (direction == 2) {
					addClipping(x, y, height, 4096);
					addClipping(x + 1, y, height, 65536);
				} else if (direction == 3) {
					addClipping(x, y, height, 16384);
					addClipping(x, y - 1, height, 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (direction == 0) {
					addClipping(x, y, height, 512);
					addClipping(x - 1, y + 1, height, 8192);
				} else if (direction == 1) {
					addClipping(x, y, height, 2048);
					addClipping(x + 1, y + 1, height, 32768);
				} else if (direction == 2) {
					addClipping(x, y, height, 8192);
					addClipping(x + 1, y + 1, height, 512);
				} else if (direction == 3) {
					addClipping(x, y, height, 32768);
					addClipping(x - 1, y - 1, height, 2048);
				}
			} else if (type == 2) {
				if (direction == 0) {
					addClipping(x, y, height, 66560);
					addClipping(x - 1, y, height, 4096);
					addClipping(x, y + 1, height, 16384);
				} else if (direction == 1) {
					addClipping(x, y, height, 5120);
					addClipping(x, y + 1, height, 16384);
					addClipping(x + 1, y, height, 65536);
				} else if (direction == 2) {
					addClipping(x, y, height, 20480);
					addClipping(x + 1, y, height, 65536);
					addClipping(x, y - 1, height, 1024);
				} else if (direction == 3) {
					addClipping(x, y, height, 81920);
					addClipping(x, y - 1, height, 1024);
					addClipping(x - 1, y, height, 4096);
				}
			}
		}
	}
	
	public void removeClippingForSolidObject(int x, int y, int z,
			int xLength, int yLength, boolean flag) {
		int clipping = 256;
		if (flag) {
			clipping += 0x20000;
		}
		for (int i = x; i < x + xLength; i++) {
			for (int i2 = y; i2 < y + yLength; i2++) {
				removeClipping(i, i2, z, clipping);
			}
		}
	}
	
	public void removeClippingForSolidObject(Location loc,
			int xLength, int yLength, boolean flag) {
		removeClippingForSolidObject(loc.getX(), loc.getY(), loc.getZ(), xLength, yLength, flag);
	}

	public void removeClippingForVariableObject(Location loc,
			int type, int direction, boolean flag) {
		if (type == 0) {
			if (direction == 0) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 128);
				removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 8);
			} else if (direction == 1) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 2);
				removeClipping(loc.getX(), loc.getY() + 1, loc.getZ(), 32);
			} else if (direction == 2) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 8);
				removeClipping(loc.getX() + 1, loc.getY(), loc.getZ(), 128);
			} else if (direction == 3) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 32);
				removeClipping(loc.getX(), loc.getY() - 1, loc.getZ(), 2);
			}
		} else if (type == 1 || type == 3) {
			if (direction == 0) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 1);
				removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 16);
			} else if (direction == 1) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 4);
				removeClipping(loc.getX() + 1, loc.getY() + 1, loc.getZ(), 64);
			} else if (direction == 2) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 16);
				removeClipping(loc.getX() + 1, loc.getY() - 1, loc.getZ(), 1);
			} else if (direction == 3) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 64);
				removeClipping(loc.getX() - 1, loc.getY() - 1, loc.getZ(), 4);
			}
		} else if (type == 2) {
			if (direction == 0) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 130);
				removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 8);
				removeClipping(loc.getX(), loc.getY() + 1, loc.getZ(), 32);
			} else if (direction == 1) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 10);
				removeClipping(loc.getX(), loc.getY() + 1, loc.getZ(), 32);
				removeClipping(loc.getX() + 1, loc.getY(), loc.getZ(), 128);
			} else if (direction == 2) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 40);
				removeClipping(loc.getX() + 1, loc.getY(), loc.getZ(), 128);
				removeClipping(loc.getX(), loc.getY() - 1, loc.getZ(), 2);
			} else if (direction == 3) {
				removeClipping(loc.getX(), loc.getY(), loc.getZ(), 160);
				removeClipping(loc.getX(), loc.getY() - 1, loc.getZ(), 2);
				removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 8);
			}
		}
		if (flag) {
			if (type == 0) {
				if (direction == 0) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 65536);
					removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 4096);
				} else if (direction == 1) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 1024);
					removeClipping(loc.getX(), loc.getY() + 1, loc.getZ(), 16384);
				} else if (direction == 2) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 4096);
					removeClipping(loc.getX() + 1, loc.getY(), loc.getZ(), 65536);
				} else if (direction == 3) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 16384);
					removeClipping(loc.getX(), loc.getY() - 1, loc.getZ(), 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (direction == 0) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 512);
					removeClipping(loc.getX() - 1, loc.getY() + 1, loc.getZ(), 8192);
				} else if (direction == 1) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 2048);
					removeClipping(loc.getX() + 1, loc.getY() + 1, loc.getZ(), 32768);
				} else if (direction == 2) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 8192);
					removeClipping(loc.getX() + 1, loc.getY() + 1, loc.getZ(), 512);
				} else if (direction == 3) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 32768);
					removeClipping(loc.getX() - 1, loc.getY() - 1, loc.getZ(), 2048);
				}
			} else if (type == 2) {
				if (direction == 0) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 66560);
					removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 4096);
					removeClipping(loc.getX(), loc.getY() + 1, loc.getZ(), 16384);
				} else if (direction == 1) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 5120);
					removeClipping(loc.getX(), loc.getY() + 1, loc.getZ(), 16384);
					removeClipping(loc.getX() + 1, loc.getY(), loc.getZ(), 65536);
				} else if (direction == 2) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 20480);
					removeClipping(loc.getX() + 1, loc.getY(), loc.getZ(), 65536);
					removeClipping(loc.getX(), loc.getY() - 1, loc.getZ(), 1024);
				} else if (direction == 3) {
					removeClipping(loc.getX(), loc.getY(), loc.getZ(), 81920);
					removeClipping(loc.getX(), loc.getY() - 1, loc.getZ(), 1024);
					removeClipping(loc.getX() - 1, loc.getY(), loc.getZ(), 4096);
				}
			}
		}
	}
	
	public void addObject(GameObject o) {
		addObject(o, ObjectDefVersion.DEF_474);
	}
	
	public void addObject(GameObject o, ObjectDefVersion defsVersion) {
			ObjectDef def = o.getDef();
			int id = o.getId();
			if (id == -1)
				id = o.getRevertId();
			if (id > 0) {
				switch (defsVersion) {
				case DEF_667:
					def = ObjectDef.forID667(id);
					break;
				case DEF_525:
					def = ObjectDef.forID525(id);
					break;
				case DEF_474:
					def = ObjectDef.getObjectDef(id);
					break;
				case OSRS_DEFS:
					def = ObjectDef.forIDOSRS(id);
					break;
				}
			}

			if (def == null) {
				return;
			}
			int xLength;
			int yLength;
			if (o.getOrientation() != 1 && o.getOrientation() != 3) {
				xLength = def.xLength();
				yLength = def.yLength();
			} else {
				xLength = def.yLength();
				yLength = def.xLength();
			}
			if (o.getType() == 22) {
				if (def.requiresClipping() && def.hasActions()) {
					addClipping(o.getLocation().getX(), o.getLocation().getY(), o.getLocation().getZ(), 0x200000);
				}
			} else if (o.getType() >= 9) {
				if (def.requiresClipping()) {
					addClippingForSolidObject(o.getLocation().getX(), o.getLocation().getY(), o.getLocation().getZ(), xLength, yLength,
							  def.clippedProjectile());
				}
			} else if (o.getType() >= 0 && o.getType() <= 3) {
				if (def.requiresClipping()) {
					addClippingForVariableObject(o.getLocation(), o.getType(), o.getOrientation(),
							def.clippedProjectile());
				}
			}

	}
	
	public void deleteObjectClipOnly(GameObject o) { // not necessary in POH, just when opening doors i guess, but this is only full squares to delete, not wall objects
		ObjectDef def = ObjectDef.getObjectDef(o.getId());
		if (def == null) {
			return;
		}
		int xLength;
		int yLength;
		if (o.getOrientation() != 1 && o.getOrientation() != 3) {
			xLength = def.xLength();
			yLength = def.yLength();
		} else {
			xLength = def.yLength();
			yLength = def.xLength();
		}
		for (int x = 0; x < xLength; x++) {
			for (int y = 0; y < yLength; y++) {
				removeObjectClipping(o.getLocation().getX()+x, o.getLocation().getY()+y, o.getLocation().getZ());
			}
		}
	}
	
	public void deleteObject(GameObject o) {
		deleteObject(o, o.getDef());
	}
	
	public void deleteObject(GameObject o, ObjectDef def) {
		o.setActive(false);
		if (o.getType() == 22) {
			removeClipping(o.getLocation().getX(), o.getLocation().getY(), o.getLocation().getZ(), 0x200000);
		} else if (o.getType() >= 9) {
			removeClippingForSolidObject(o.getLocation(), o.getSizeX(), o.getSizeY(), def.clippedProjectile());
		} else if (o.getType() >= 0 && o.getType() <= 3) {
			removeClippingForVariableObject(o.getLocation(), o.getType(), o.getOrientation(), def.clippedProjectile());
		}
	}
	
	public void clearRoom(Location corner) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				RegionC region = getOrCreateRegionById(getRegionId(x+corner.getX(), y+corner.getY()));
				region.setClip(x+corner.getX(), y+corner.getY(), corner.getZ(), 0);
			}
		}
	}
	
	public void fillRoom(Location corner) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				RegionC region = getOrCreateRegionById(getRegionId(x+corner.getX(), y+corner.getY()));
				region.setClip(x+corner.getX(), y+corner.getY(), corner.getZ(), 0x100);
			}
		}
	}
	
	public void clearRoomObjectClips(int rX, int rY, int z) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				int nX = x+rX;
				int nY = y+rY;
				RegionC region = getOrCreateRegionById(getRegionId(nX, nY));
				region.removeObjectClip(nX, nY, z);
			}
		}
	}
	
	public void clearRoomClipping(Room room) {
		for (RoomObject o : room.getStaticRoomObjects().values()) { //for (GameObject o : room.getObjectsArray()) {
			if (o != null && (o.getId() < 4 || o.getId() >= 9)) {
				deleteObject(o);
			}
		}
	}

	public static void copyStaticToStatic(int fromX, int fromY, int fromZ, int toZ, int lengthX, int lengthY, Map<Long, GameObject> objects, Map<Long, GameObject> newObjects, DynamicRegionClip clip, ObjectDefVersion objectDefVersion) {
		for (int x = 0; x < lengthX; x++) {
			int realX = fromX + x;
			for (int y = 0; y < lengthY; y++) {
				int realY = fromY + y;
				for (int type = 0; type < 4; type++) {
					GameObject o = RegionClip.getGameObjectByType(realX, realY, fromZ, type);
					if (o == null) {
						continue;
					}

					int objectId = o.getId();
					ObjectDef def = null;
					switch (objectDefVersion) {
					case DEF_667:
						def = ObjectDef.forID667(objectId);
						break;
					case DEF_525:
						def = ObjectDef.forID525(objectId);
						break;
					case OSRS_DEFS:
						def = ObjectDef.forIDOSRS(objectId);
						break;
					default:
						def = ObjectDef.getObjectDef(objectId);
						break;
					}

					if (objectId == 1276 && Misc.random(100) < 7)
						objectId = 13;
					if (objectId == 1278 && Misc.random(100) < 7)
						objectId = 12;
					
					Location location = Location.create(realX, realY, toZ);
					GameObject gameObject = new GameObject(objectId, location, o.getOrientation(), o.getType());
					gameObject.setDefs(def);
					final long locationKey = GameObject.generateKey(location.getX(), location.getY(), location.getZ(),
							Misc.OBJECT_SLOTS[gameObject.getType()]);
					objects.put(locationKey, gameObject);
					
					if (objectId == 12 || objectId == 13) {
						gameObject.setTicks(Integer.MAX_VALUE);
						newObjects.put(locationKey, gameObject);
					}
				}

				clip.addClipping(realX, realY, toZ, RegionClip.getClipping(realX, realY, fromZ));//RegionClip.BLOCKED_TILE);
			}
		}
	}

	public static void copy(int fromX, int fromY, int fromZ, int toX, int toY, int toZ, Map<Long, GameObject> objects, int zoneRotation, DynamicRegionClip clip, ObjectDefVersion objectDefVersion) {
		for (int x = 0; x < 8; x++) {
			int realX = fromX + x;
			for (int y = 0; y < 8; y++) {
				int realY = fromY + y;
				for (int type = 0; type < 4; type++) {
					GameObject o = RegionClip.getGameObjectByType(realX, realY, fromZ, type);
					if (o == null) {
						continue;
					}

					int objectId = o.getId();
					ObjectDef def = null;
					switch (objectDefVersion) {
					case DEF_667:
						def = ObjectDef.forID667(objectId);
						break;
					case DEF_525:
						def = ObjectDef.forID525(objectId);
						break;
					case OSRS_DEFS:
						def = ObjectDef.forIDOSRS(objectId);
						break;
					default:
						def = ObjectDef.getObjectDef(objectId);
						break;
					}

					int newRot = (o.getOrientation() + zoneRotation) & 0x3;
					int realCoordsX = translateTestX(x, y, zoneRotation, def.xLength(), def.yLength(), o.getOrientation());
					int realCoordsY = translateTestY(x, y, zoneRotation, def.xLength(), def.yLength(), o.getOrientation());
					Location location = Location.create(toX + realCoordsX, toY + realCoordsY, toZ);
					GameObject gameObject = new GameObject(objectId, location, newRot, o.getType());
					gameObject.setDefs(def);
					clip.addObject(gameObject, objectDefVersion);
					if (objectDefVersion == ObjectDefVersion.DEF_667) { // dung stuff
						if (def.itemActions != null || objectId == DungeonConstants.FARMING_PATCH_CLEAN_WARPED || objectId == DungeonConstants.FARMING_PATCH_CLEAN || objectId == DungeonConstants.FISH_SPOT_OBJECT_ID || objectId == DungeonConstants.WOODCUTTING_BASE[0] || objectId == DungeonConstants.WOODCUTTING_BASE[1] || objectId == DungeonConstants.WOODCUTTING_BASE[2] || objectId == DungeonConstants.WOODCUTTING_BASE[3] || objectId == DungeonConstants.WOODCUTTING_BASE[4]) {
							objects.put(GameObject.generateKey(location.getX(), location.getY(), location.getZ(), Misc.OBJECT_SLOTS[gameObject.getType()]), gameObject);
						}
					} else {
						objects.put(GameObject.generateKey(location.getX(), location.getY(), location.getZ(), Misc.OBJECT_SLOTS[gameObject.getType()]), gameObject);
					}
				}

				if ((RegionClip.getClipping(realX, realY, fromZ) & RegionClip.BLOCKED_TILE) != 0) {
					int realCoordsX = translateTestX(x, y, zoneRotation, 1, 1, 0);
					int realCoordsY = translateTestY(x, y, zoneRotation, 1, 1, 0);
					clip.addClipping(toX + realCoordsX, toY + realCoordsY, toZ, RegionClip.BLOCKED_TILE);
				}
			}
		}
	}

	public static int translateTestX(int x, int y, int mapRotation, int sizeX, int sizeY, int objectRotation) {
		if ((objectRotation & 0x1) == 1) {
			int prevSizeX = sizeX;
			sizeX = sizeY;
			sizeY = prevSizeX;
		}

		if (mapRotation == 0) {
			return x;
		} else if (mapRotation == 1) {
			return y;
		} else if (mapRotation == 2) {
			return 7 - x - (sizeX - 1);
		} else if (mapRotation == 3) {
			return 7 - y - (sizeY - 1);
		}

		return 0;
	}
	
	public static int translateTestY(int x, int y, int mapRotation, int sizeX, int sizeY, int objectRotation) {
		if ((objectRotation & 0x1) == 1) {
			int prevSizeX = sizeX;
			sizeX = sizeY;
			sizeY = prevSizeX;
		}

		if (mapRotation == 0) {
			return y;
		} else if (mapRotation == 1) {
			return 7 - x - (sizeX - 1);
		} else if (mapRotation == 2) {
			return 7 - y - (sizeY - 1);
		} else if (mapRotation == 3) {
			return x;
		}

		return 0;
	}

	public void copyStaticRoom(final Location from, final Location to, Room room, HouseStyle style) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
//				Location newL = Location.create(to.getX()+x, to.getY()+y, to.getZ());
//				RegionC r = getRegion(newL);
//				r.setClip(newL, RegionClip.getClipping(from.getX()+x, from.getY()+y, from.getZ()));
				for (int type = 0; type < 4; type++) {
					
					GameObject o = RegionClip.getGameObjectByType(from.getX()+x, from.getY()+y, from.getZ(), type);
					if (o != null) {
						int objectId = o.getId();
						if (objectId == 13830) { // windows
							objectId = style.getWindowId();
							
							RoomObject rO = new RoomObject(13830, style.getWindowId(), Location.create(to.getX()+x, to.getY()+y, to.getZ()), o.getOrientation(), o.getType());
							room.addStaticRoomObject(rO);
							room.addBuiltObjectToMap(HotspotType.WINDOW, rO);
						} else {
							RoomObject newO = new RoomObject(o.getId(), Location.create(to.getX()+x, to.getY()+y, to.getZ()), o.getOrientation(), o.getType());
							room.addStaticRoomObject(newO);
						}
					}
				}
			}
		}
	}
	
	public void clipAllObjectsNotHotSpots(Collection<RoomObject> objects) {
		for (RoomObject o : objects) {
			if (o.getHotspotType() == null || o.getHotspotType() == HotspotType.WINDOW)
				addObject(o);
		}
	}
	
	public void clipWallObjects(Collection<RoomObject> objects) {
		for (RoomObject o : objects) {
			if (o != null)
				if (o.getType() < 4)
					addObject(o);
		}
	}
	
	public void rotateMatrix(int N, int mat[][])
    {
        // Consider all squares one by one
        for (int x = 0; x < N / 2; x++)
        {
            // Consider elements in group of 4 in 
            // current square
            for (int y = x; y < N-x-1; y++)
            {
                // store current cell in temp variable
                int temp = mat[x][y];
      
                // move values from right to top
                mat[x][y] = mat[y][N-1-x];
      
                // move values from bottom to right
                mat[y][N-1-x] = mat[N-1-x][N-1-y];
      
                // move values from left to bottom
                mat[N-1-x][N-1-y] = mat[N-1-y][x];
      
                // assign temp to left
                mat[N-1-y][x] = temp;
                
                
                System.out.println("x:"+x+" y:"+y);
            }
        }
    }
	
	public void rotateMatrixAntiClockwise(Location c) {
		for (int x = 0; x < 8 / 2; x++) {
			for (int y = x; y < 8-x-1; y++) {                
				swapClips(Location.create(c.getX()+x, c.getY()+y, c.getZ()), Location.create(c.getX()+y, c.getY()+7-x, c.getZ()), false);
				swapClips(Location.create(c.getX()+y, c.getY()+7-x, c.getZ()), Location.create(c.getX()+7-x, c.getY()+7-y, c.getZ()), false);
				swapClips(Location.create(c.getX()+7-x, c.getY()+7-y, c.getZ()), Location.create(c.getX()+7-y, c.getY()+x, c.getZ()), false);
				//swapClips(Location.create(c.getX()+7-y, c.getY()+x, c.getZ()), Location.create(c.getX()+x, c.getY()+y, c.getZ()));
			}
		}
	}
	
	public void surroundAreaWithSolidClipMask(int startX, int startY, int size, int... planes) {
		//int startX = 20, startY = 20; // if this is decreased, then increase loop count by amount decreased on here
		
		final int loopCount = size;//63;
		
		final RegionC r1 = getOrCreateRegionById(getRegionId(startX, startY));
		
		//left side
		RegionC r2 = r1;
		for (int offset = 0; offset < loopCount; offset++) {
				//if (startY == 64) {
					r2 = getOrCreateRegionById(getRegionId(startX, startY));
				//}

				for (int z = 0; z < planes.length; z++) {
					r2.setClip(startX, startY, planes[z], 0x100);
				}
				
				startY++;
		}
		
		//top side
		for (int offset = 0; offset < loopCount; offset++) {
			//if (startX == 64) {
				r2 = getOrCreateRegionById(getRegionId(startX, startY));
			//}

			for (int z = 0; z < planes.length; z++) {
				r2.setClip(startX, startY, planes[z], 0x100);
			}
			
			startX++;
		}
		
		//ride side
		for (int offset = 0; offset < loopCount; offset++) {
			//if (startY == 63) {
				r2 = getOrCreateRegionById(getRegionId(startX, startY));
			//}

			for (int z = 0; z < planes.length; z++) {
				r2.setClip(startX, startY, planes[z], 0x100);
			}

			startY--;
		}
		
		//bottom side
		for (int offset = 0; offset < loopCount; offset++) {
			//if (startX == 63) {
				r2 = getOrCreateRegionById(getRegionId(startX, startY));
			//}

			for (int z = 0; z < planes.length; z++) {
				r2.setClip(startX, startY, planes[z], 0x100);
			}

			startX--;
		}
		
		
	}
	
	private void swapClips(Location l1, Location l2, boolean clockWise) {
		RegionC r = getOrCreateRegionById(getRegionId(l1.getX(), l1.getY()));
		RegionC r2 = getOrCreateRegionById(getRegionId(l2.getX(), l2.getY()));
		int clip1 = r.getClip(l1.getX(), l1.getY(), l1.getZ());
		int clip2 = r2.getClip(l2.getX(), l2.getY(), l2.getZ());
		r.setClip(l1.getX(), l1.getY(), l1.getZ(), clip2);
		r2.setClip(l2.getX(), l2.getY(), l2.getZ(), clip1);
		if (clip1 != 0)
			r.rotateWallClip(l1.getX(), l1.getY(), l1.getZ(), clockWise);
		if (clip2 != 0)
			r2.rotateWallClip(l2.getX(), l2.getY(), l2.getZ(), clockWise);
	}
	
	public void removeNpcTile(int x, int y, int z) {
		removeClipping(x, y, z, RegionClip.NPC_TILE);
	}
	
	public void addNpcTile(int x, int y, int z) {
		addClipping(x, y, z, RegionClip.NPC_TILE);
	}
	
	public void cleanup() {
		regions.clear();
	}
	
}
