package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.DecorativeBlood;
import com.soulplay.content.player.skills.construction.buildable.impl.DecorativePipe;
import com.soulplay.content.player.skills.construction.buildable.impl.HangingSkeleton;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DungeonDecorationHotspot extends Hotspot {

    public DungeonDecorationHotspot() {
        super(HotspotType.DUNGEON_DECORATION);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new DecorativeBlood(), new DecorativePipe(), new HangingSkeleton()));
    }

}
