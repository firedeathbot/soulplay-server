package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Bluebells;
import com.soulplay.content.player.skills.construction.buildable.impl.Daffodils;
import com.soulplay.content.player.skills.construction.buildable.impl.Rosemary;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class FormalSmallPlant2 extends Hotspot {

    public FormalSmallPlant2() {
        super(HotspotType.FORMAL_SMALL_PLANT_2);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Rosemary(), new Daffodils(), new Bluebells()));
    }
}
