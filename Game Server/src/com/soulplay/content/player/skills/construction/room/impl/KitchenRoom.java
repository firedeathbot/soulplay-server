package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class KitchenRoom extends Room {

	private static final Palette palette = new Palette(4, 1);

	private static final boolean[] doors = {false, false, true, true};

	public KitchenRoom(int x, int y, int z) {
		super(RoomType.KITCHEN, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.KITCHEN_TABLE, new RoomObject(15405, Location.create(3, 3), 0, 10));
		hotspots.put(HotspotType.LARDER, new RoomObject(15403, Location.create(6, 0), 0, 10));
		hotspots.put(HotspotType.BARREL, new RoomObject(15401, Location.create(0, 6), 3, 10));
		hotspots.put(HotspotType.STOVE, new RoomObject(15398, Location.create(3, 7), 1, 10));
		hotspots.put(HotspotType.SINK, new RoomObject(15404, Location.create(7, 3), 0, 10));
		hotspots.put(HotspotType.CAT_BASKET, new RoomObject(15402, Location.create(0, 0), 0, 22));

		hotspots.put(HotspotType.SHELF, new RoomObject(15400, Location.create(1, 7), 1, 5));
		hotspots.put(HotspotType.SHELF, new RoomObject(15400, Location.create(6, 7), 1, 5));
		hotspots.put(HotspotType.SHELF, new RoomObject(15399, Location.create(7, 6), 2, 5));
	}

}
