package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.DecorativeWindow;
import com.soulplay.content.player.skills.construction.buildable.impl.ShutteredWindow;
import com.soulplay.content.player.skills.construction.buildable.impl.StainedGlass;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class WindowHotspot extends Hotspot {

    public WindowHotspot() {
        super(HotspotType.WINDOW_SPACE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new ShutteredWindow(), new DecorativeWindow(), new StainedGlass()));
    }
}
