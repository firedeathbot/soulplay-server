package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;
import com.soulplay.content.player.skills.construction.buildable.impl.DockLeaf;
import com.soulplay.content.player.skills.construction.buildable.impl.Reeds;
import com.soulplay.content.player.skills.construction.buildable.impl.Thistle;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class SmallPlantHotspot2 extends Hotspot {

    public SmallPlantHotspot2() {
	super(HotspotType.SMALL_PLANT_2);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new DockLeaf(), new Thistle(), new Reeds()));
    }

}
