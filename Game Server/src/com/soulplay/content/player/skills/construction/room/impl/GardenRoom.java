package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public final class GardenRoom extends Room {

	private static final Palette palette = new Palette(2, -5);

	private static final boolean[] doors = {true, true, true, true};

	public GardenRoom(int x, int y, int z) {
		super(RoomType.GARDEN, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.GARDEN_CENTREPIECE, new RoomObject(15361, Location.create(3, 3), 0, 10));

		hotspots.put(HotspotType.TREE, new RoomObject(15362, Location.create(1, 5), 0, 10));
		hotspots.put(HotspotType.TREE, new RoomObject(15363, Location.create(6, 6), 0, 10));

		hotspots.put(HotspotType.SMALL_PLANT_1, new RoomObject(15366, Location.create(3, 1), 0, 10));
		hotspots.put(HotspotType.SMALL_PLANT_2, new RoomObject(15367, Location.create(4, 5), 0, 10));

		hotspots.put(HotspotType.BIG_PLANT_1, new RoomObject(15364, Location.create(6, 0), 0, 10));
		hotspots.put(HotspotType.BIG_PLANT_2, new RoomObject(15365, Location.create(0, 0), 0, 10));
	}

}
