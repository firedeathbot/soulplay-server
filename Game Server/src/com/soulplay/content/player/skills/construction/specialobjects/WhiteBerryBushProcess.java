package com.soulplay.content.player.skills.construction.specialobjects;

import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class WhiteBerryBushProcess {

	private static final int TIME = 1250; // timePlayed is 1200 each cycle, so 1250 is 25 minutes.
	
	public static void processBush(Player player) {
		
		if (!player.playerHasHouse())
			return;
		
		if (player.timePlayed >= TIME && player.timePlayed % TIME == 0) {
			growBerries(player);
		}
		
	}
	
	private static void growBerries(Player player) {
		
		int roomSize = player.getPOH().getRooms()[0].length;
		
		for (int z = 0; z < 3; z++) {
			for (int x = 0; x < roomSize; x++) {
				for (int y = 0; y < roomSize; y++) {

					if (player.getPOH().getRoomCount(z) <= 0) continue;

					Room r = player.getPOH().getRooms()[z][x][y];

					if (r != null && r.getType() == RoomType.GARDEN) {

						for (RoomObject w : r.getObjects().get(HotspotType.BIG_PLANT_1)) {
							WhiteberryEnum berry = WhiteberryEnum.get(w.getId());

							if (berry != null) {
								w.setId(berry.getNextObj());
								if (Misc.isInsideRenderedArea(player, w.getX(), w.getY()))
									player.getPacketSender().addObjectPacket(w);
							}

						}

					}

				}
			}
		}
		
	}
		
}
