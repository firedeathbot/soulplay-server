package com.soulplay.content.player.skills.construction;

public class ConstructionAnimations {

    public static final int WATERING_ANIM = 2293;

    public static final int BUILD_LOW_ANIM = 3683;

    public static final int BUILD_MID_ANIM = 3676;

    public static final int BUILD_HIGH_ANIM = 3684;

}
