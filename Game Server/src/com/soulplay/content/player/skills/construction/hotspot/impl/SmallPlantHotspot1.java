package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;
import com.soulplay.content.player.skills.construction.buildable.impl.Fern;
import com.soulplay.content.player.skills.construction.buildable.impl.Plant;
import com.soulplay.content.player.skills.construction.buildable.impl.SmallFern;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class SmallPlantHotspot1 extends Hotspot {

    public SmallPlantHotspot1() {
	super(HotspotType.SMALL_PLANT_1);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new Plant(), new SmallFern(), new Fern()));	
    }

}
