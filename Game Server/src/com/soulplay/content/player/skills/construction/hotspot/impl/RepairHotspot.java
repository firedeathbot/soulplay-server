package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.ArmourStand;
import com.soulplay.content.player.skills.construction.buildable.impl.RepairBench;
import com.soulplay.content.player.skills.construction.buildable.impl.Whetstone;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class RepairHotspot extends Hotspot {

    public RepairHotspot() {
        super(HotspotType.REPAIR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new RepairBench(), new Whetstone(), new ArmourStand()));
    }
}
