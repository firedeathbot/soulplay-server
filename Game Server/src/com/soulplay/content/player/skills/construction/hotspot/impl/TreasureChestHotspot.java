package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyTreasureChest;
import com.soulplay.content.player.skills.construction.buildable.impl.OakTreasureChest;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakTreasureChest;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class TreasureChestHotspot extends Hotspot {

    public TreasureChestHotspot() {
        super(HotspotType.TREASURE_CHEST);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakTreasureChest(), new TeakTreasureChest(), new MahoganyTreasureChest()));
    }
}
