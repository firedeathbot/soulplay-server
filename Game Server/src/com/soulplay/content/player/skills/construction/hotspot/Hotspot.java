package com.soulplay.content.player.skills.construction.hotspot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;

public abstract class Hotspot implements Consumer<Player> {

    private final HotspotType type;
    
    protected final List<Buildable> buildables = new ArrayList<>();
    protected final List<HotspotType> colliders = new ArrayList<>();

    public Hotspot(HotspotType type) {
		this.type = type;
		init();
    }
    
    public abstract void init();

    @Override
    public void accept(Player player) {
	for (int i = 39552; i < 39600; i++) {
	    player.getPacketSender().sendString("", i);
	}
	
	for (int i = 0; i < 8; i++) {
	    player.getPacketSender().itemOnInterface((39600 + i), 0); // player.getPacketSender().sendFrame34a((39600 + i), -1, 0, 0);
	    player.getPacketSender().sendConfig(773 + i, 0); // reset the x marks
	}

	for (int i = 0; i < getBuildables().size(); i++) {
	    Buildable buildable = getBuildables().get(i);

	    // name
	    player.getPacketSender().sendString(buildable.getName(), (i * 6) + 39552);

	    player.getPacketSender().displayItemOnInterface((39600 + i), buildable.getItemId(), 0, 1);

	    // level required
	    player.getPacketSender().sendString("Level: " + buildable.getLevelRequired(), (i * 6) + 39557);

	    if (buildable.getMaterialDescription().isEmpty()) {

			for (int j = 0; j < buildable.getMaterialsNeeded().size(); j++) {

				Item material = buildable.getMaterialsNeeded().get(j);

				ItemDefinition def = ItemDefinition.forId(material.getId());

				if (def == null) {
					continue;
				}

				// required items
				player.getPacketSender().sendString(def.getName() + " x " + material.getAmount(), (i * 6) + 39553 + j);

			}

			if (buildable.getRequiredNails() > 0) {
				player.getPacketSender().sendString(String.format("Nails x %d", buildable.getRequiredNails()), (i * 6) + 39553 + buildable.getMaterialsNeeded().size());
			}

		} else {
			player.getPacketSender().sendString(buildable.getMaterialDescription(), (i * 6) + 39553);
		}
	    
	    final boolean canBuild = (player.getPlayerLevel()[Skills.CONSTRUCTION] + (player.getItems().playerHasItem(ConstructionItems.CRYSTAL_SAW) ? 5 : 0)) >= buildable.getLevelRequired() && buildable.hasRequiredItems(player) && (buildable.getRequiredNails() <= 0 || ConstructionUtils.playerHasNails(player, buildable.getRequiredNails()));
	    
	    player.getPacketSender().sendConfig(773 + i, canBuild ? 0 : 1);

	}

	player.getPacketSender().showInterface(Construction.FURNITURE_CHOOSER_INTERFACE);
    }

    public final List<HotspotType> getHotspotColliders() {
    	return colliders;
	}
    
    public final List<Buildable> getBuildables() {
	return buildables;
    }

    public final HotspotType getType() {
	return type;
    }

}
