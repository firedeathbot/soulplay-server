package com.soulplay.content.player.skills.construction;

public class ObjectWrapper {

    private final int id;

    private final int type;

    public ObjectWrapper(int id, int type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }
}
