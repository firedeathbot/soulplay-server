package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Runecase3 extends Buildable {
    @Override
    public String getName() {
        return "Rune case 1";
    }

    @Override
    public int getItemId() {
        return 8278;
    }

    @Override
    public int getObjectId() {
        return 13509;
    }

    @Override
    public int getLevelRequired() {
        return 41;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK, 2), new Item(ConstructionItems.MOLTEN_GLASS, 2), new Item(563), new Item(560), new Item(565), new Item(566));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 247;
    }

    public List<String> getMaterialDescriptionLines() {
        return Arrays.asList("Teak plank x2", "Molten glass x2", "law, death, blood, soul x1");
    }
}
