package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.LargeMap;
import com.soulplay.content.player.skills.construction.buildable.impl.MediumMap;
import com.soulplay.content.player.skills.construction.buildable.impl.SmallMap;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class MapHotspot extends Hotspot {

    public MapHotspot() {
        super(HotspotType.MAP);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new SmallMap(), new MediumMap(), new LargeMap()));
    }

}
