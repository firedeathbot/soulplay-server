package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.PumpAndDrain;
import com.soulplay.content.player.skills.construction.buildable.impl.PumpAndTub;
import com.soulplay.content.player.skills.construction.buildable.impl.Sink;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class SinkHotspot extends Hotspot {

    public SinkHotspot() {
        super(HotspotType.SINK);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new PumpAndDrain(), new PumpAndTub(), new Sink()));
    }

}
