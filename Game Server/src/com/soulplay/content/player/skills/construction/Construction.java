package com.soulplay.content.player.skills.construction;

import java.util.*;

import com.soulplay.content.player.combat.melee.MeleeData;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotFactory;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.popularhouse.PopularHouses;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomFactory;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.content.player.skills.construction.room.impl.EmptyDung;
import com.soulplay.content.player.skills.construction.room.impl.EmptyRoom;
import com.soulplay.content.player.skills.construction.specialobjects.WhiteberryEnum;
import com.soulplay.content.player.skills.prayer.Prayer;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Construction {

	private static final int[] pohOnlyItems = new int[] {7671, 7673, 7675, 7676, 7679};

	public static final int WAIT_INTERFACE = 38400;

	public static final int ROOM_CREATION_MENU_INTERFACE = 28643;

	public static final int FURNITURE_CHOOSER_INTERFACE = 39550;

    public static Direction calculateDirection(Player player) {
        final Location loc = Location.create(player.getX(), player.getY());

        final int x = loc.getX() / 8;
        final int y = loc.getY() / 8;

        final Location corner = Location.create(x * 8, y * 8);

        final int dx = loc.getX() - corner.getX();
        final int dy = loc.getY() - corner.getY();

        final int distance = 2;

        if (dx >= 2 && dx <= 5 && dy >= (7 - (distance)) && dy <= 7) {
            return Direction.NORTH;
        } else if (dx >= (7 - (distance)) && dx <= 7 && dy >= 2 && dy <= 5) {
            return Direction.EAST;
        } else if (dx >= 2 && dx <= 5 && dy >= 0 && dy <= (0 + (distance))) {
            return Direction.SOUTH;
        } else if (dx >= 0 && dx <= (0 + (distance)) && dy >= 2 && dy <= 5) {
            return Direction.WEST;
        } else {
            return Direction.NORTH;
        }

    }

	public static void degradeWateringCan(Player player) {
		if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_1)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_1, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_2)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_2, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_1, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_3)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_3, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_2, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_4)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_4, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_3, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_5)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_5, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_4, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_6)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_6, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_5, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_7)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_7, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_6, 1);
		} else if (player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_8)) {
			player.getItems().deleteItem2(ConstructionItems.WATERING_CAN_8, 1);
			player.getItems().addItem(ConstructionItems.WATERING_CAN_7, 1);
		}
	}
	
	private static Location DEFAULT_EXIT_LOC = Location.create(3079, 3491);
	
	public static void leaveHouse(Player c) {
		leaveHouse(c, true, true);
	}
	
	public static void leaveHouse(Player c, boolean movePlayer) {
		leaveHouse(c, movePlayer, true);
	}

	private static void deletePohItems(Player player) {
		boolean updateRequired = false;
		boolean resetMovementAnim = false;

		for (int itemId : pohOnlyItems) {
			if (player.playerEquipment[PlayerConstants.playerWeapon] == itemId) {
				player.playerEquipment[PlayerConstants.playerWeapon] = -1;
				player.playerEquipmentN[PlayerConstants.playerWeapon] = 0;
				resetMovementAnim = true;
				updateRequired = true;
			}

			if (player.playerEquipment[PlayerConstants.playerShield] == itemId) {
				player.playerEquipment[PlayerConstants.playerShield] = -1;
				player.playerEquipmentN[PlayerConstants.playerShield] = 0;
				updateRequired = true;
			}

			for (int i = 0, length = player.burdenedItems.length; i < length; i++) {
				int bobItemId = player.burdenedItems[i] - 1;
				if (bobItemId == itemId) {
					player.burdenedItems[i] = 0;
				}
			}

			player.getItems().deleteItem2(itemId, player.getItems().getItemAmount(itemId));

			if (resetMovementAnim) {
				MeleeData.getPlayerAnimIndex((Client)player, -1);
			}

			if (updateRequired) {
				player.setUpdateEquipmentRequired(true);
			}
		}
	}

	public static void leaveHouse(Player player, boolean movePlayer, boolean removeFromList) {
		leaveHouse(player, movePlayer, DEFAULT_EXIT_LOC, removeFromList);
	}
	
	public static void leaveHouse(Player c, boolean movePlayer, Location destination, boolean removeFromList) {
		if (!c.playerIsInHouse) {
			return;
		}

		c.getPA().closeAllWindows();

		deletePohItems(c);

		if (c.getHomeOwnerId() > -1 && c.mySQLIndex != c.getHomeOwnerId()) { // if i was a guest
			if (removeFromList) {
				Player p = PlayerHandler.getPlayerByMID(c.getHomeOwnerId());
				if (p != null && p.isActive) {
					p.getPOH().getGuests().remove(c);
					p.getPOH().decGuestSize();
				}
			}
		} else if (c.mySQLIndex == c.getHomeOwnerId()) {
			for (Player c2 : c.getPOH().getGuests()) {
				if (c2 != null && c2.isActive && c2.getHomeOwnerId() == c.mySQLIndex) {
					leaveHouse(c2, true, false);
				}
			}
			c.getPOH().getGuests().clear();
		}

		for (NPC npc : c.getPOH().getNpcs().values()) {
			npc.removeNpcSafe(false);
		}
		
		PopularHouses.leaveOwnHouse(c);

		c.getPOH().getNpcs().clear();
		c.getPOH().setNpcTask(null);

		c.getPacketSender().sendString(String.format("Number of rooms: %d", c.getPOH().getRoomCount()), 46005);
	    c.playerIsInHouse = false;
	    c.setDynamicRegionClip(null);
	    if (c.summoned != null) {
	    	c.summoned.setDynamicRegionClip(null);
	    }

	    c.attr().put("canAttack", false);

		if (movePlayer) {
			c.getPA().movePlayer(destination);
			c.changeRespawn(destination.getX(), destination.getY(), destination.getZ());
			c.getPA().safeDeathSecureTimer();
		}

	    CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				c.setHomeOwnerId(-1);
			}
		}, 1);
	}
	
	public static RoomObject getDecoration(Player guest, int objectId, int objectX, int objectY) {
		if (guest.getHomeOwnerId() < 0)
			return null;
		
		Player owner = PlayerHandler.getPlayerByMID(guest.getHomeOwnerId());
		
		if (owner == null || !owner.isActive) {
//			guest.sendMessage("Owner is offline.");
			return null;
		}
		
		Poh ownerPoh = owner.getPOH();
		
		Optional<Room> roomResult = ownerPoh.getCurrentRoom(Location.create(objectX, objectY, guest.getZ()));

        if (!roomResult.isPresent()) {
            return null;
        }

        final Room currentRoom = roomResult.get();
        
        for (RoomObject o : currentRoom.getObjects().values()) {
        	if (o.getId() == objectId && o.getLocation().getX() == objectX && o.getLocation().getY() == objectY) {
        		return o;
        	}
        }
        
        return null;
	}
	
	public static RoomObject getHotspot(Player guest, int objectId, int objectX, int objectY, int objectZ) {
		if (guest.getHomeOwnerId() < 0)
			return null;
		
		HotspotType type = HotspotType.lookup(objectId);
		
		if (type == null) {
			return null;
		}
		
		Player owner = PlayerHandler.getPlayerByMID(guest.getHomeOwnerId());
		
		if (owner == null || !owner.isActive) {
//			guest.sendMessage("Owner is offline.");
			return null;
		}
		
		Poh ownerPoh = owner.getPOH();
		
		Optional<Room> roomResult = ownerPoh.getCurrentRoom(Location.create(objectX, objectY, guest.getZ()));

        if (!roomResult.isPresent()) {
            return null;
        }

        final Room currentRoom = roomResult.get();
		
        for (RoomObject o : currentRoom.getStaticRoomObjects().get(type)) {
        	//if (o.getX() == objectX && o.getY() == objectY) // x/y check not necessary right now
        		return o;
        }
		
        return null;
	}

	public static void spawnNpcs(Player player) {
		if (!player.playerIsInHouse || player.getPOH().isBuildMode()) {
			return;
		}

		Player owner = player;

		if (player.getPOH().isGuest()) {
			owner = PlayerHandler.getPlayerByMID(player.getHomeOwnerId());
		}

		if (owner == null || !owner.playerIsInHouse || owner.getPOH().isBuildMode()) {
			return;
		}

		final Poh poh = owner.getPOH();

		for (int x = 0; x < poh.getRooms()[0].length; x++) {
			for (int y = 0; y < poh.getRooms()[0][0].length; y++) {
				final Room room = poh.getRoom(x, y ,0);

				if (room == null) {
					continue;
				}

				for (RoomObject object : room.getObjects().values()) {

					if (poh.getNpcs().containsKey(object)) {
						continue;
					}

					final int guardId = ConstructionUtils.getGuardId(object.getId());

					if (guardId == -1) {
						continue;
					}

					int npcSlot = Misc.findFreeNpcSlot();
					
					if (npcSlot == -1)
						continue;
					
					NPC guard = new NPC(npcSlot, guardId);
					final NPC npc = NPCHandler.spawnNpc(guard, object.getX(), object.getY(), object.getZ(), 1);

					if (npc == null) {
						continue;
					}

					npc.setHomeOwnerId(owner.mySQLIndex);
					npc.setDynamicRegionClip(owner.getPohClipMap());
					poh.getNpcs().put(object, npc);
				}

			}
		}

	}

	public static boolean handleFirstObjectClick(Client guest, int objectId, int objectX, int objectY) {
//		if (Config.RUN_ON_DEDI) {
//			return false;
//		}

		switch (objectId) {
		
		case 18805:
		{
			final Client owner = (Client)PlayerHandler.getPlayerByMID(guest.getHomeOwnerId());

			if (owner == null || !owner.isActive) {
				guest.sendMessage("Owner is offline.");
				return true;
			}
			
			guest.getStorageObject().openStorage(owner, StorageObjectEnum.TREASURE_CHEST);
		}
			return true;
		
		 // altar
		case 13179:
		case 13180:
		case 13181:
		case 13182:
		case 13183:
		case 13184:
		case 13185:
			Prayer.clickAltar(guest);
			return true;
		
		case 13497: // stairs leading upstairs
		case 13499: // teak staircase
		case 13501: // marble staircase
		case 13503: // limebrick spiral
		case 13505: // marble spiral
		{
			
			Player owner = PlayerHandler.getPlayerByMID(guest.getHomeOwnerId());
			
			if (owner == null || !owner.isActive) {
				guest.sendMessage("Owner is offline.");
				return true;
			}

			final boolean isOwner = owner.equals(guest);
			
			Poh ownerPoh = owner.getPOH();
			
			Optional<Room> roomResult = ownerPoh.getCurrentRoom(guest.getCurrentLocation());

	        if (!roomResult.isPresent()) {
	            return true;
	        }

	        final Room currentRoom = roomResult.get();
	        
	        RoomObject currentStairs = currentRoom.getStairs();
	        
	        if (currentStairs == null) {
	        	guest.sendMessage("stairs null");
	        	return true;
	        }
	        
	        final int newHeight = ((guest.getZ()+1) % 4);
	        
	        if (newHeight > Poh.SECOND_FLOOR) {
	        	guest.sendMessage("You are on the highest possible level so you cannot add a room above here."); // TODO: make this into a dialogueBuilder.statement
	        	return true;
	        }

			Room upstairs = ownerPoh.getRoom(currentRoom.getX(), currentRoom.getY(), newHeight);
			
			if (upstairs == null || upstairs.getType() == RoomType.EMPTY) {
				if (isOwner && owner.getPOH().isBuildMode()) {
					guest.getDialogueBuilder().sendStatement("These stairs do not lead anywhere.",
							"Do you want to build", " a room at the top?")
							.sendOption("Skill Hall", ()-> {
								if (RoomType.hasRequirements(guest, RoomType.SKILL_HALL_UPPER, true)) {
									buildRoomOnOtherFloor(guest, currentRoom, RoomFactory.createRoom(RoomType.SKILL_HALL_UPPER, currentRoom.getX(), currentRoom.getY(), newHeight));
								} else {
									guest.getDialogueBuilder().close();
								}
							}, "Quest Hall", ()-> {
								if (RoomType.hasRequirements(guest, RoomType.QUEST_HALL_UPPER, true)) {
									buildRoomOnOtherFloor(guest, currentRoom, RoomFactory.createRoom(RoomType.QUEST_HALL_UPPER, currentRoom.getX(), currentRoom.getY(), newHeight));
								} else {
									guest.getDialogueBuilder().close();
								}
							}, "Cancel", ()-> {
								guest.getPA().closeAllWindows();
							}).execute(false);
				} else {
					if (isOwner) {
						guest.sendMessage("You do not have a room upstairs.");
					} else {
						guest.sendMessage(String.format("%s does not have a room upstairs.", owner.getName()));
					}
				}
				return true;
			} else if (upstairs != null && !upstairs.hasStairs() && upstairs.getStairsHotspot() != null) {
				if (isOwner && owner.getPOH().isBuildMode()) {
					// room doesn't have stairs so ask to build
					guest.getDialogueBuilder().sendStatement("Would you like to link the stairs?")
							.sendOption("Link stairs above?", "Yes", ()-> {
										if (upstairs.getType() != upstairs.getUpperRoomType()) {
											Room newRoom = RoomFactory.createRoom(upstairs.getUpperRoomType(), upstairs.getX(), upstairs.getY(), upstairs.getZ());
											ConstructionUtils.convertRoom(guest, upstairs, newRoom);
											ConstructionUtils.linkStairs(guest, currentRoom, newRoom);
										} else {
											ConstructionUtils.linkStairs(guest, currentRoom, upstairs);
										}
									}, "No", ()-> {
										guest.getPA().closeAllWindows();
									}
							).onReset(()->{
						guest.getPA().closeAllWindows();
					}).execute(false);
				} else {
					if (isOwner) {
						guest.sendMessage("You do not have stairs upstairs.");
					} else {
						guest.sendMessage(String.format("%s does not have stairs upstairs.", owner.getName()));
					}
				}
				return true;
			} else if (upstairs != null && upstairs.hasStairs() && upstairs.getStairs().getId() != currentStairs.getId()+1) {
				guest.sendMessage("The stairs don't match!");
				return true;
			}

			renderPlane(guest, newHeight);

		}
			return true;
			
		case 13498: // stairs leading downstairs
		case 13500: // teak staircase
		case 13502: // marble staircase
		case 13504: // limebrick sprial
		case 13506: // marble spiral
		{

			Player owner = PlayerHandler.getPlayerByMID(guest.getHomeOwnerId());
			
			if (owner == null || !owner.isActive) {
				guest.sendMessage("Owner is offline.");
				return true;
			}

			final boolean isOwner = owner.equals(guest);
			
			Poh ownerPoh = owner.getPOH();
			
			Optional<Room> roomResult = ownerPoh.getCurrentRoom(guest.getCurrentLocation());

	        if (!roomResult.isPresent()) {
	            return true;
	        }

	        final Room currentRoom = roomResult.get();

	        GameObject currentStairs = currentRoom.getStairs();
	        
	        if (currentStairs == null) {
	        	guest.sendMessage("stairs null2");
	        	return true;
	        }
	        
	        if (guest.getZ() == 0) {
	        	guest.sendMessage("You are on the lowest possible level so you cannot add a room below here."); // TODO: make this into a dialogueBuilder.statement
	        	return true;
	        }
	        
	        final int newHeight = guest.getZ() - 1;
	        if (newHeight < 0) {
	        	return true;
	        }
	        
			Room downstairs = ownerPoh.getRoom(currentRoom.getX(), currentRoom.getY(), newHeight);
			
			if (downstairs == null || downstairs.getType() == RoomType.EMPTY_DUNG) {
				if (isOwner && owner.getPOH().isBuildMode()) {
					guest.getDialogueBuilder().sendStatement("These stairs do not lead anywhere.",
							"Do you want to"," build a room downstairs?")
							.sendOption("Skill hall", ()-> {
								if (RoomType.hasRequirements(guest, RoomType.SKILL_TROPHY_HALL, true)) {
									buildRoomOnOtherFloor(guest, currentRoom, RoomFactory.createRoom(RoomType.SKILL_TROPHY_HALL, currentRoom.getX(), currentRoom.getY(), newHeight));
								} else {
									guest.getDialogueBuilder().close();
								}
							}, "Quest hall", ()-> {
								if (RoomType.hasRequirements(guest, RoomType.QUEST_TROPHY, true)) {
									buildRoomOnOtherFloor(guest, currentRoom, RoomFactory.createRoom(RoomType.QUEST_TROPHY, currentRoom.getX(), currentRoom.getY(), newHeight));
								} else {
									guest.getDialogueBuilder().close();
								}
							}, "Dungeon Room", ()-> {
								if (RoomType.hasRequirements(guest, RoomType.DUNGEON_STAIR_ROOM, true)) {
									buildRoomOnOtherFloor(guest, currentRoom, RoomFactory.createRoom(RoomType.DUNGEON_STAIR_ROOM, currentRoom.getX(), currentRoom.getY(), newHeight));
								} else {
									guest.getDialogueBuilder().close();
								}
							}, "Cancel", ()-> {
								guest.getPA().closeAllWindows();
							}).execute(false);
				} else {
					if (isOwner) {
						guest.sendMessage("You do not have a room downstairs.");
					} else {
						guest.sendMessage(String.format("%s does not have a room downstairs.", owner.getName()));
					}
				}
				return true;
			} else if (downstairs != null && !downstairs.hasStairs() && downstairs.getStairsHotspot() != null) {
				if (isOwner && owner.getPOH().isBuildMode()) {
					// room doesn't have stairs so ask to build
					guest.getDialogueBuilder().sendStatement("Would you like to link the stairs?")
							.sendOption("Link stairs below?", "Yes", ()-> {
										if (downstairs.getType() != downstairs.getLowerRoomType()) {
											Room newRoom = RoomFactory.createRoom(downstairs.getLowerRoomType(), downstairs.getX(), downstairs.getY(), downstairs.getZ());
											ConstructionUtils.convertRoom(guest, downstairs, newRoom);
											ConstructionUtils.linkStairs(guest, currentRoom, newRoom);
										} else {
											ConstructionUtils.linkStairs(guest, currentRoom, downstairs);
										}
									}, "No", ()-> {
										guest.getPA().closeAllWindows();
									}
							).onReset(()->{
						guest.getPA().closeAllWindows();
					}).execute(false);
				} else {
					if (isOwner) {
						guest.sendMessage("You do not have stairs below.");
					} else {
						guest.sendMessage(String.format("%s does not have a room downstairs.", owner.getName()));
					}
				}
				return true;
			} else if (downstairs != null && downstairs.hasStairs() && downstairs.getStairs().getId() != currentStairs.getId()-1) {
				guest.sendMessage("The stairs don't match!");
				return true;
			}
	        if (newHeight == 0) {
				spawnNpcs(owner);
			}

			renderPlane(guest, newHeight);

		}
		return true;		

			case 13409: // dungeon entrance
			{
				
			}
				return true;

		case 15362:
			// player.getPOH().buildObject(objectId, objectX, objectY);
			return true;
		}

		return false;
	}

	public static void renderPlane(Client player, int height) {
		if (!player.playerIsInHouse) {
			return;
		}

		Player owner;

		if (player.getPOH().isGuest()) {
			owner = player.getPOH().getOwner();
		} else {
			owner = player;
		}

		Construction.openWaitInterface(player);

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int tick = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (tick == 0) {
					player.getPA().movePlayer(player.getX(), player.getY(), height);
				} else if (tick == 1) {
					player.getPacketSender().sendConstructMapRegionPacket(owner.getPOH().getRooms(), owner.getPOH().getHouseStyle());
				} else if (tick == 2) {
					player.getPOH().showAllObjects(height, owner);
				} else {
					container.stop();
				}

				tick++;
			}

			@Override
			public void stop() {
				Construction.closeWaitInterface(player);
				player.getPacketSender().setBlackout(0);
			}
		}, 1);
	}
	
	private static void buildRoomOnOtherFloor(Player player, Room currentRoom, Room newRoom) {
		Hotspot hotspot = HotspotFactory.getHotspot(currentRoom.getStairs().getHotspotType());
		ConstructionUtils.buildable = ConstructionUtils.getBuildableForHotspot(player, currentRoom, newRoom, hotspot);
		if (ConstructionUtils.buildable == null) {
			player.sendMessage("Buildable is null");
			return;
		}
		
		player.getPOH().placeRoom(newRoom);
		
		newRoom.getPalette().rotate(currentRoom.getRotation());
		player.getPOH().applyRoomRotation(newRoom);
		
		player.getPacketSender().sendConstructMapRegionPacket(player.getPOH());
		
		player.getPOH().spawnObject(newRoom, hotspot, ConstructionUtils.buildable);
	}

	private static boolean interactWithObject(Player player, GameObject o) {
		if (ConstructionUtils.isDoorHotspot(o) && !player.getPOH().isBuildMode()) {
			return true;
		}
		return false;
	}

	private static void handleDoorHotspot(Player player) {
		Location nextPaletteIndex = ConstructionUtils.calculateNextPaletteIndex(player);

		if (nextPaletteIndex.getX() == -1 || nextPaletteIndex.getY() == -1) {
			return;
		}
		
		final Room roomToBuild = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), nextPaletteIndex.getZ());
		
		if (roomToBuild == null || roomToBuild.getType() == RoomType.EMPTY || roomToBuild.getType() == RoomType.EMPTY_DUNG) {

			if (nextPaletteIndex.getX() < 3 || nextPaletteIndex.getY() < 3 || nextPaletteIndex.getX() > 9 || nextPaletteIndex.getY() > 9) {
				player.sendMessage("You can't build this direction anymore.");
				return;
			} else if (roomToBuild == null && player.getPOH().isInDungeon()) {
				player.sendMessage("You can't build in this direction.");
				return;
			}
			
			switch (nextPaletteIndex.getZ()) {
			case Poh.SECOND_FLOOR: // second floor rules
				Room downstairs = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), Poh.GROUND_FLOOR);
				if (downstairs == null || downstairs.getType() == RoomType.EMPTY || downstairs.getType() == RoomType.GARDEN || downstairs.getType() == RoomType.FORMAL_GARDEN) {
					player.getDialogueBuilder().sendStatement("You can't build a room where there is no supported room underneath.").execute();
					return;
				}
				break;
			case Poh.DUNGEON_FLOOR: // dungeon floor rules
				break;
			}

			int count = 0;
			for (RoomType type : RoomType.values()) {
				if (type.getLevelRequired() == 0) {
					continue;
				}

				if (RoomType.hasRequirements(player, type, false)) {
					player.getPacketSender().sendString(String.format("<col=ff00>%d Coins", type.getCost()), RoomType.BEGIN_INTERFACE_COST_TEXT + (count * 4));
				} else {
					player.getPacketSender().sendString(String.format("<col=ff0000>%d Coins", type.getCost()), RoomType.BEGIN_INTERFACE_COST_TEXT + (count * 4));
				}
				count++;
			}

			player.getPacketSender().showInterface(ROOM_CREATION_MENU_INTERFACE);
			return;
		}

//				final Direction playerDirection = Construction.calculateDirection(player);

		final boolean canRotateClockwise = true;//roomToBuild.canRotate(playerDirection, true);
		final boolean canRotateAntiClockwise = true;//roomToBuild.canRotate(playerDirection, false);

		if (canRotateClockwise && canRotateAntiClockwise) {
			if (!ConstructionUtils.canDeleteRoom(player, nextPaletteIndex)) {
				return;
			}
			player.getDialogueBuilder().sendOption("Delete", () -> {
				deleteRoomDialogue(player, nextPaletteIndex);
			}, "Cancel", () -> {
			}).execute();
		}
	}

	private static boolean handleHotspot(Player player, GameObject o) {
		HotspotType hotspotType1 = HotspotType.lookup(o.getId());

		if (hotspotType1 == null) {
			return false;
		}

		Hotspot hotspot = HotspotFactory.getHotspot(hotspotType1);

		if (hotspot == null) {
			player.sendMessage(String.format("Hotspot=%s is not supported.", hotspotType1.name()));
			return true;
		}

		Optional<Room> result = player.getPOH().getCurrentRoom();
		if (!result.isPresent()) {
		    return true;
        }

        final Room currentRoom = result.get();

		if (currentRoom.getType() == null || currentRoom.getType() == RoomType.EMPTY || currentRoom.getType() == RoomType.EMPTY_DUNG) {
		    return true;
        }

		hotspot.accept(player);

		player.getPOH().currentHotspot = Optional.of(hotspot);
		return true;
	}

	public static boolean handleFifthObjectClick(Player player, GameObject o) {
		if (o == null)
			return false;

		if (o.getId() == 13405 && player.getPOH().getExitPortalCount() <= 1) {
			player.sendMessage("You need at least 1 exit portal.");
			return true;
		}

		player.faceLocation(o.getLocation());
		
		
		if (removeObjectClick(player, o.getId(), o.getX(), o.getY())) { // built object is not null so it's a "remove object" option
			return true;
		}

		if (!player.getPOH().isBuildMode()) {
			return interactWithObject(player, o);
		} else if (ConstructionUtils.isDoorHotspot(o)) {
			handleDoorHotspot(player);
			return true;
		} else if (handleHotspot(player, o)) {
			return true;
		}
		return false;
	}
	
	public static boolean handleFourthClickObject(final Client player, final int objectId, final int oX, final int oY, final int oZ) {
		switch (objectId) { // remove room on stairs
			case 13497: // upstairs room to delete
			case 13499: // teak staircase
			case 13501: // marble staircase
			case 13503: // limebrick spiral
			case 13505: // marble spiral
			{
				Optional<Room> roomResult = player.getPOH().getCurrentRoom();
	
				if (!roomResult.isPresent()) {
					return true;
				}
	
				final Room room = roomResult.get();
				final int newHeight = (room.getZ()+1) % 4;
				Location upstairs = Location.create(room.getX(), room.getY(), newHeight);
				startDeleteRoomOnDiffHeight(player, upstairs);
			}
			return true;
	
			case 13498: // downstairs room to delete
			case 13500: // teak staircase
			case 13502: // marble staircase
			case 13504: // limebrick sprial
			case 13506: // marble spiral
			{
				Optional<Room> roomResult = player.getPOH().getCurrentRoom();
				
				if (!roomResult.isPresent()) {
					return true;
				}
	
				final Room room = roomResult.get();
				
				final int newHeight = player.getZ() - 1;//player.getZ() == 0 ? 3 : ((player.getZ()-1) % 4);
				Location downstairs = Location.create(room.getX(), room.getY(), newHeight);
				startDeleteRoomOnDiffHeight(player, downstairs);
			}
			return true;
		}
		
		if (removeObjectClick(player, objectId, oX, oY)) {
			return true;
		}
		return false;
	}
	
	private static void startDeleteRoomOnDiffHeight(Client player, Location roomLoc) {

		Room roomToDelete = player.getPOH().getRoom(roomLoc.getX(), roomLoc.getY(), roomLoc.getZ());
		if (roomToDelete == null || roomToDelete.getType() == RoomType.EMPTY_DUNG) {
			player.sendMessage("You don't have a room to delete.");
			return;
		}
		if (!ConstructionUtils.canDeleteRoom(player, roomLoc)) {
			return;
		}
		deleteRoomDialogue(player, roomLoc);
	}

	private static final String REMOVE_SAME_FLOOR_DIALOGUE = "Remove Room?";
	private static final String REMOVE_ROOM_ABOVE_DIALOUGE = "Remove Room Above?";
	private static final String REMOVE_ROOM_BELOW_DIALOUGE = "Remove Room Below?";
	
	private static void deleteRoomDialogue(Player player, final Location nextPaletteIndex) {
		
		player.getDialogueBuilder()
		.sendOption((player.getZ() < nextPaletteIndex.getZ() ? REMOVE_ROOM_ABOVE_DIALOUGE : (player.getZ() > nextPaletteIndex.getZ() ? REMOVE_ROOM_BELOW_DIALOUGE : REMOVE_SAME_FLOOR_DIALOGUE)), "Yes", () -> {

			Room roomToDelete = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), nextPaletteIndex.getZ());

//			for (GameObject objectToDelete : roomToDelete.getObjects().values()) {
//				player.getPacketSender().removeObjectPacket(objectToDelete);
//			}
//
//			for (GameObject tempHotspotToDelete : roomToDelete.getTempHotspots().values()) {
//				player.getPacketSender().removeObjectPacket(tempHotspotToDelete);
//			}
			player.getPacketSender().removeObjects(roomToDelete.getX(), roomToDelete.getY(), roomToDelete.getZ());
			
			//TODO: remove clipping for this room
//			player.getPohClipMap().clearRoom(Location.create(nextPaletteIndex.getX()*8, nextPaletteIndex.getY()*8, player.getZ()));
			for (GameObject o : roomToDelete.getStaticRoomObjects().values()/*roomToDelete.getObjectsArray()*/) {
				player.getPohClipMap().deleteObject(o);
			}
			
			roomToDelete.nullRoom();
			
			Construction.rebuildClipsInSurroundingRooms(player, Location.create(nextPaletteIndex.getX(), nextPaletteIndex.getY(), nextPaletteIndex.getZ()));

			if (nextPaletteIndex.getZ() == Poh.GROUND_FLOOR)
				player.getPOH().placeRoom(new EmptyRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), nextPaletteIndex.getZ()));
			else if (nextPaletteIndex.getZ() == Poh.DUNGEON_FLOOR)
				player.getPOH().placeRoom(new EmptyDung(nextPaletteIndex.getX(), nextPaletteIndex.getY(), nextPaletteIndex.getZ()));
			else
				player.getPOH().getRooms()[nextPaletteIndex.getZ()][nextPaletteIndex.getX()][nextPaletteIndex.getY()] = null;
			
			if (nextPaletteIndex.getZ() == player.getZ()) {
				player.getPacketSender().sendConstructMapRegionPacket(player.getPOH().getRooms(), player.getPOH().getHouseStyle());
			} else {
				player.getPacketSender().sendConstructMapRegionPacket(player.getPOH());
			}
			player.getPOH().decrementRoomCount(nextPaletteIndex.getZ());
			player.getPacketSender().sendString(String.format("Number of rooms: %d", player.getPOH().getRoomCount()), 46005);
			
			PlayerSaveSql.deleteRoom(player.mySQLIndex, nextPaletteIndex.getX(), nextPaletteIndex.getY(), nextPaletteIndex.getZ());

		}, "No", () -> {

		}).executeIfNotActive();
	}
	
	public static void drawObjectsInRoom(final Player c, final Room room) {
		c.performingAction = true;
		openWaitInterface(c);
		c.setLoadedRegion(false);
		c.getPA().movePlayer(c.getX(), c.getY(), room.getZ());

		final int prevHeight = c.getZ();
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (c.isLoadedRegion()) {
					room.displayBuiltObjects(c, false);
					container.stop();
				}
			}

			@Override
			public void stop() {
				c.performingAction = false;
				if (c.isActive) {
					c.getPA().movePlayer(c.getX(), c.getY(), prevHeight);
					closeWaitInterface(c);
					c.getPacketSender().setBlackout(0);
				}
			}
		}, 1);
	}
	
	//TODO: remove clipping by current objects in room then just check surrounding rooms and add their objects in to readd wall clips
	public static void rebuildClipsInSurroundingRooms(Player c, Location currentRoom) {
//		// south room
//		Room sRoom = c.getPOH().getRoom(room.getX(), room.getY()-1, c.getZ());
//		if (sRoom != null)
//			c.getPohClipMap().clipAllObjects(sRoom.getObjectsArray());
//		
//		//north room
//		Room nRoom = c.getPOH().getRoom(room.getX(), room.getY()+1, c.getZ());
//		if (nRoom != null)
//			c.getPohClipMap().clipAllObjects(nRoom.getObjectsArray());
//		
//		Room eRoom = c.getPOH().getRoom(room.getX()+1, room.getY(), c.getZ());
//		if (eRoom != null)
//			c.getPohClipMap().clipAllObjects(eRoom.getObjectsArray());
//		
//		Room wRoom = c.getPOH().getRoom(room.getX()-1, room.getY(), c.getZ());
//		if (wRoom != null)
//			c.getPohClipMap().clipAllObjects(wRoom.getObjectsArray());
		
		for (int count = 0; count < 4; count++) {
			int xOff = (count == 1 ? -1 : (count == 0 ? 1 : 0));
			int yOff = (count == 3 ? -1 : count == 2 ? 1 : 0);
			int roomX = currentRoom.getX()+ xOff;
			int roomY = currentRoom.getY()+yOff;
			if (roomX < 0 || roomX > Poh.MAXIMUM_ROOMS-1 || roomY < 0 || roomY > Poh.MAXIMUM_ROOMS-1) continue;
			Room r = c.getPOH().getRoom(roomX, roomY, currentRoom.getZ());
			if (r == null || r.getType() == RoomType.EMPTY || r.getType() == RoomType.EMPTY_DUNG)
				continue;
			if (!r.getStaticRoomObjects().isEmpty()) {
				c.getPohClipMap().clipWallObjects(r.getStaticRoomObjects().values());
			}
		}
		
//		c.getPohClipMap().cleanup();
//		for (int i = 0; i < c.getPOH().getRooms()[c.getZ()].length; i++) {
//			for (int j = 0; j < c.getPOH().getRooms()[c.getZ()][i].length; j++) {
//				Room r = c.getPOH().getRooms()[c.getZ()][i][j];
//				if (r != null) {
//					if (!r.getObjectsArray().isEmpty())
//						c.getPohClipMap().clipAllObjects(r.getObjectsArray());
//				}
//			}
//		}
	}
	
	public static void cancelBuild(Player c) {
		c.getPOH().clearCopyRooms();
		c.getDialogueBuilder().close();
		c.getPacketSender().sendConstructMapRegionPacket(c.getPOH());
		c.getPOH().showAllObjects(c.getZ(), c);
	}
	
	private static boolean removeObjectClick(final Player player, final int objectId, final int x, final int y) {
		Optional<Room> optional = player.getPOH().getCurrentRoom();

		if (!optional.isPresent()) {
			return false;
		}

		Room room = optional.get();

		RoomObject object = null;
		for (RoomObject obj : room.getObjects().values()) {
			if (obj.getId() == objectId && x == obj.getX() && obj.getY() == y) {
				object = obj;
				break;
			}
		}
		if (object == null) {
//			player.sendMessage("Construction Object is null");
			return false;
		}
//		if (!room.objectExists(objectId)) {
//			return false;
//		}
		
		final RoomObject objFinal = object;

		player.getDialogueBuilder().sendOption("Remove this?", "Yes", () -> {
			room.removeBuiltObjects(player);

			HotspotType todelete = objFinal.getHotspotType();

			for (RoomObject ro : room.getObjects().get(todelete)) {
//				room.getTempHotspots().put(room.getRotation(), new RoomObject(ro.getHotspotId(), ro.getHotspotId(), ro.getLocation(), ro.getOrientation(), ro.getType()));
				if (ro.getId() != -1) {
					player.getPohClipMap().deleteObject(ro);
				}
//				player.sendMessage("deleting obj:"+ro.getId());
			}
			
			player.getPacketSender().clearMapZone(); //THIS WILL CLEAR GROUND ITEMS INSIDE THE ROOM TOO!!! DISABLE DROPPING ITEMS IN BUILD MODE OR RELOAD THEM

			if (todelete != null) {
				if (objectId == 13405)
					player.getPOH().decrementExitPortalCount();
				room.deleteBuiltObjectsFromMap(todelete, objectId);
				
				//cheaphax to reducing whiteberry bush counts
				if (todelete == HotspotType.BIG_PLANT_1 && objectId >= WhiteberryEnum.BUSH1.getObject() && objectId <= WhiteberryEnum.BUSH5.getObject()) {
					player.getPOH().setWhiteberryBushCount(player.getPOH().getWhiteberryBushCount()-1);
				}
			}
			
			room.displayBuiltObjects(player, false);

			//room.showTemphotspots(player);

//			room.getTempHotspots().clear();

		}, "No", () -> {

		}).execute();

		return true;
	}
	
	public static void openWaitInterface(Player player) {
		player.getPacketSender().showInterface(Construction.WAIT_INTERFACE);
		player.getPacketSender().setBlackout(2 + 0x40);
	}
	
	public static void closeWaitInterface(Player player) {
		 player.getPacketSender().setBlackout(0);
         player.getPA().closeAllWindows();
	}
	
	public static void itemOnObject(Client c, int itemId, int itemSlot, int objectId, int objectX, int objectY) {
		if (c.mySQLIndex != c.getHomeOwnerId()) {
			c.sendMessage("This is not your home.");
			return;
		}

		Optional<Room> roomResult = c.getPOH().getCurrentRoom();

        if (!roomResult.isPresent()) {
            return;
        }

        final Room room = roomResult.get();
		
		
		int sizeY = 0;
		
		for (RoomObject rO : room.getObjects().values()) {
			if (rO.getLocation().getX() == objectX && rO.getLocation().getY() == objectY) {
				sizeY = rO.getSizeY();
				break;
			}
		}
		
		if (sizeY == 0) {
			c.sendMess("What?? lol");
			return;
		}
		
		Misc.faceObject(c, objectX, objectY, sizeY);
		
		switch (objectId) {
		case 13295:
			Item item = new Item(itemId, 1, itemSlot);
			if (c.getItems().deleteItem(item)) {
				
				int placeX = c.getX();
				int placeY = c.getY();
				
				if (c.getFacingLocation() != null) {
					placeX = c.getFacingLocation().getX() == c.getX() ? c.getX() : c.getFacingLocation().getX() > c.getX() ? c.getX()+1 : c.getX()-1;;
					placeY = c.getFacingLocation().getY() == c.getY() ? c.getY() : c.getFacingLocation().getY() > c.getY() ? c.getY()+1 : c.getY()-1;
				}
				
				c.startAnimation(832);
				
				ConstructionUtils.dropItemsInPoh(c, item, placeX, placeY);
			}
			
			default:
				c.sendMessage("Item on object not added yet.");
				return;
	        
		}
	}

}
