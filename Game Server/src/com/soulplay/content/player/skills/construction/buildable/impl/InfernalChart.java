package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class InfernalChart extends Buildable {
    @Override
    public String getName() {
        return "Infernal chart";
    }

    @Override
    public int getItemId() {
        return 8356;
    }

    @Override
    public int getObjectId() {
        return 13664;
    }

    @Override
    public int getLevelRequired() {
        return 83;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.CLOTH, 4));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 60;
    }
}
