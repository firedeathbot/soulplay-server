package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.BannerEasel;
import com.soulplay.content.player.skills.construction.buildable.impl.PlumingStand;
import com.soulplay.content.player.skills.construction.buildable.impl.ShieldEasel;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class HeraldryHotspot extends Hotspot {
    public HeraldryHotspot() {
        super(HotspotType.HERALDRY);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new PlumingStand(), new ShieldEasel(), new BannerEasel()));
    }
}
