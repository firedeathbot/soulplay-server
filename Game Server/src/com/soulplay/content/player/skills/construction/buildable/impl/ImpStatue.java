package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class ImpStatue extends Buildable {

    @Override
    public String getName() {
        return "Imp Statue";
    }

    @Override
    public int getItemId() {
        return 8171;
    }

    @Override
    public int getObjectId() {
        return 13408;
    }

    @Override
    public int getLevelRequired() {
        return 15;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.LIMESTONE_BRICK, 5), new Item(ConstructionItems.SOFT_CLAY, 5));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 100;
    }

}
