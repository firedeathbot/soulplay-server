package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class AltarHotspot extends Hotspot {

    public AltarHotspot() {
        super(HotspotType.ALTAR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakAltar(), new TeakAltar(), new ClothAltar(), new MahoganyAltar(), new LimestoneAltar(), new MarbleAltar(), new GildedAltar()));
    }
}
