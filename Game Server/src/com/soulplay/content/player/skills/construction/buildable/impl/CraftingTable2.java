package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class CraftingTable2 extends Buildable {
    @Override
    public String getName() {
        return "Crafting table 2";
    }

    @Override
    public int getItemId() {
        return 8381;
    }

    @Override
    public int getObjectId() {
        return 13710;
    }

    @Override
    public int getLevelRequired() {
        return 25;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(8380), new Item(ConstructionItems.MOLTEN_GLASS));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 1;
    }
}
