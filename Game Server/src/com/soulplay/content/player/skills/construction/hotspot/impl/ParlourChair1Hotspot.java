package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;

import com.soulplay.content.player.skills.construction.buildable.impl.CrudeWoodenChair;
import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyArmchair;
import com.soulplay.content.player.skills.construction.buildable.impl.OakArmchair;
import com.soulplay.content.player.skills.construction.buildable.impl.OakChair;
import com.soulplay.content.player.skills.construction.buildable.impl.RockingChair;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakArmchair;
import com.soulplay.content.player.skills.construction.buildable.impl.WoodenChair;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class ParlourChair1Hotspot extends Hotspot {

    public ParlourChair1Hotspot() {
	super(HotspotType.PARLOUR_CHAIR_1);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new CrudeWoodenChair(), new WoodenChair(), new RockingChair(), new OakChair(), new OakArmchair(), new TeakArmchair(), new MahoganyArmchair()));
    }

}
