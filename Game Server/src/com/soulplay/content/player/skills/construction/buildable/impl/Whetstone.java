package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Whetstone extends Buildable{
    @Override
    public String getName() {
        return "Whetstone";
    }

    @Override
    public int getItemId() {
        return 8390;
    }

    @Override
    public int getObjectId() {
        return 13714;
    }

    @Override
    public int getLevelRequired() {
        return 35;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 4), new Item(ConstructionItems.LIMESTONE_BRICK));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 260;
    }
}
