package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class MagicalBalance2 extends Buildable {
    @Override
    public String getName() {
        return "Magical balance 2";
    }

    @Override
    public int getItemId() {
        return 8157;
    }

    @Override
    public int getObjectId() {
        return 13396;
    }

    @Override
    public int getLevelRequired() {
        return 57;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(556, 1000), new Item(557, 1000), new Item(554, 1000), new Item(555, 1000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 252;
    }

    @Override
    protected List<String> getMaterialDescriptionLines() {
        return Arrays.asList("air, eath, fire, water rune x1000");
    }

}
