package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class SkeletonGuard extends Buildable {
    @Override
    public String getName() {
        return "Skeleton guard";
    }

    @Override
    public int getItemId() {
        return 8131;
    }

    @Override
    public int getObjectId() {
        return 13366;
    }

    @Override
    public int getLevelRequired() {
        return 70;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(995, 50_000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 223;
    }
}
