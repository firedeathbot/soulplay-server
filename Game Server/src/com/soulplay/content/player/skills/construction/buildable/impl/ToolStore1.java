package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class ToolStore1 extends Buildable {
    @Override
    public String getName() {
        return "Tool store 1";
    }

    @Override
    public int getItemId() {
        return 8384;
    }

    @Override
    public int getObjectId() {
        return 13699;
    }

    @Override
    public int getLevelRequired() {
        return 15;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 120;
    }
}
