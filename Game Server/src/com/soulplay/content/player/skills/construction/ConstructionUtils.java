package com.soulplay.content.player.skills.construction;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;

import com.soulplay.Config;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotFactory;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.sounds.Sounds;

public class ConstructionUtils {
	
	public static boolean DISABLED_DROP = false;

	private ConstructionUtils() {

	}

	public static final String[] HANGMANWORDS = new String[]{"DENNISISNOOB",
			"ABYSSAL", "ADAMANTITE", "ALKHARID", "ARDOUGNE", "ASGARNIA",
			"AVANTOE", "BASILISK", "BANSHEE", "BARROWS", "BLOODVELD",
			"BOBTHECAT", "BRIMHAVEN", "BURTHORPE", "CADANTINE", "CAMELOT",
			"CANIFIS", "CATHERBY", "CHAOSDRUID", "CHAOSDWARF", "CHOMPYBIRD",
			"COCKATRICE", "CRANDOR", "CROMADIURE", "DAGANNOTH", "DORGESHUUN",
			"DRAGON", "DRAYNOR", "DUSTDEVIL", "DWARFWEED", "EDGEVILLE",
			"ENTRANA", "FALADOR", "FELDIP", "FIREGIANT", "FREMENNIK",
			"GARGOYLE", "GOBLIN", "GRANDTREE", "GUAMLEAF", "GUTANOTH", "GUTHIX",
			"HILLGIANT", "HELLHOUND", "HIGHWAYMAN", "HOBGOBLIN", "ICEGIANT",
			"ICEQUEEN", "ICEWARRIOR", "ICEWOLF", "ICETROLL", "IRITLEAF",
			"ISAFDAR", "JOGRE", "KALPHITE", "KANDARIN", "KARAMJA", "KELDAGRIM",
			"KHAZARD", "KWUARM", "LANTADYME", "LLETYA", "LUMBRIDGE",
			"NECHRYAEL", "MARRENTILL", "MENAPHOS", "MISTHALIN", "MITHRIL",
			"MOGRE", "MORTTON", "MORYTANIA", "MOSSGIANT", "NIGHTSHADE",
			"PALADIN", "PHASMATYS", "PORTSARIM", "PRIFDDINAS", "PYREFIEND",
			"RANARRWEED", "RELLEKKA", "RIMMINGTON", "RUNESCAPE", "RUNITE",
			"SARADOMIN", "SKELETON", "SNAPDRAGON", "SNAPEGRASS", "SOPHANEM",
			"SOULLESS", "SPIRITTREE", "TARROMIN", "TAVERLEY", "TERRORBIRD",
			"TIRANNWN", "TOADFLAX", "TORSTOL", "UGTHANKI", "UNICORN", "VARROCK",
			"WHIP", "YANILLE", "ZAMORAK"};

	public static int getGuardId(int objectId) {
		switch (objectId) {
			case 13331:
				return 3580;
			case 13373:
				return 3594;

			case 13366:
				return 3581;
			case 13367:
				return 3582;
			case 13368:
				return 3583;
			case 13372:
				return 3588;
			case 13370:
				return 3585;
			case 13369:
				return 3584;
			case 2715:
				return 3586;

			case 39260:
				return 11562;
			case 39261:
				return 11563;
			case 39262:
				return 11564;
			case 39263:
				return 11565;
			case 39264:
				return 11566;
			case 39265:
				return 11567;

			case 13378:
				return 3593;
			case 13374:
				return 3589;
			case 13377:
				return 3592;
			case 13376:
				return 3591;
			case 13375:
				return 3590;
		}
		return -1;
	}

	public static boolean[] rotateDoorFlags(boolean[] flags, int rotation) {
		boolean[] copy = new boolean[flags.length];

		for (int i = 0; i < flags.length; i++) {

			int next = (i - rotation);

			if (next < 0) {
				next = flags.length - Math.abs(i - rotation);
			}

			copy[i] = flags[next];
		}

		//		System.out.println(Arrays.toString(copy));

		return copy;
	}

	public static int getDirectionOffsetX(Direction direction) {
		if (direction == Direction.WEST) {
			return -1;
		} else if (direction == Direction.EAST) {
			return 1;
		} else {
			return 0;
		}	
	}

	public static int getDirectionOffsetY(Direction direction) {
		if (direction == Direction.SOUTH) {
			return -1;
		} else if (direction == Direction.NORTH) {
			return 1;
		} else {
			return 0;
		}	
	}

	public static boolean isDoorHotspot(GameObject object) {
		return object.getId() >= 15305 && object.getId() <= 15322;
	}

	public static Location calculateNextRotatedCoordinate(int rotation, Location location) {

		// its this if you cache this it works
		final Location localL = ConstructionUtils.getLocalTile(rotation, location);

		final int[][] coords = new int[4][];

		coords[0] = new int[] { localL.getX(), localL.getY() };

		for (int i = 1; i < 4; i++) {
			coords[i] = new int[] { coords[i - 1][1], 7 - coords[i - 1][0] };
		}

		Location corner = ConstructionUtils.getCorner(location);

		return Location.create(coords[rotation][0] + corner.getX(), coords[rotation][1] + corner.getY());
	}
	public static Location getLocalTile(int rotation, Location location) {
		Location corner = ConstructionUtils.getCorner(location);

		final int dx = location.getX() - corner.getX();
		final int dy = location.getY() - corner.getY();

		return Location.create(dx, dy);
	}

	public static Location getCorner(Location location) {


		final int x = location.getX() / 8;
		final int y = location.getY() / 8;

		return Location.create((x * 8), (y * 8));
	}

	public static Location calculateNextPaletteIndex(Player owner) {
		Direction direction = Construction.calculateDirection(owner);

		Optional<Room> result = owner.getPOH().getCurrentRoom();

		if (!result.isPresent()) {
			return Location.create(-1, -1);
		}

		Room room = result.get();

		int x = room.getX();
		int y = room.getY();
		int z = room.getZ();

		if (direction == Direction.NORTH) {
			y += 1;
		} else if (direction == Direction.EAST) {
			x += 1;
		} else if (direction == Direction.SOUTH) {
			y -= 1;
		} else if (direction == Direction.WEST) {
			x -= 1;
		} else {
			return Location.create(-1, -1, z);
		}
		return Location.create(x, y, z);
	}

	public static Location getRotatedOffset(Location loc, int sizeX, int sizeY, int objectRotation, int chunkRotation) {
		if (objectRotation > -1 && (objectRotation & 0x1) == 1) { // we already do this in gameobject class, so if you don't know the object's current size then use rotation
			sizeX = sizeX ^ sizeY;
			sizeY = sizeX ^ sizeY;
			sizeX = sizeX ^ sizeY;
		}
		if (chunkRotation == 0) {
			return Location.create(loc.getX()&7, loc.getY()&7);//new int[] { x, y };
		}
		if (chunkRotation == 1) { //clockwise 90 degrees
			return Location.create(loc.getY()&7, 7-(loc.getX()&7)-(sizeY-1)); // new int[] { y, 7 - x - (sizeX - 1) };
		}
		if (chunkRotation == 2) {
			return Location.create(7-(loc.getX()&7)-(sizeX-1), 7-(loc.getY()&7)-(sizeY-1)); // new int[] { 7 - x - (sizeX - 1), 7 - y - (sizeY - 1) };
		}
		return Location.create(7-(loc.getY()&7)-(sizeX-1), loc.getX()&7); //new int[] { 7 - y - (sizeY - 1), x };
	}

	public static String buildObjectsString(Room room) {
		StringBuilder sb = new StringBuilder("");

		for (RoomObject o : room.getObjects().values()) {
			if (o.getHotspotType() == HotspotType.WINDOW)
				continue;
			long values = 0;
			values += (long)o.getOrientation(); // 2
			values += (long)o.getLocation().getX() << 2; // 10 + 2 = 12
			values += (long)o.getLocation().getY() << 12; // 10 + 12 = 22
			values += (long)o.getId() << 22; // 15 + 22 = 37
			values += (long)o.getHotspotId() << 37; // 15 + 37 = 52
			values += (long)o.getType() << 52;
			sb.append(values);
			sb.append(":");
		}
		return sb.toString();
	}

	public static String buildGroundItemsString(Room room) {
		StringBuilder sb = new StringBuilder("");

		for (GroundItem o : room.getItems()) {
			long values = 0;
			values += (long)(o.getItemX() - room.getX()*8); // 3
			values += (long)(o.getItemY() - room.getY()*8) << 3; // 3 + 3 = 6
			values += (long)o.getItemId() << 6; // 15 + 6 = 21
			values += (long)o.getItemAmount() << 21; // 21
			sb.append(values);
			sb.append(":");
		}
		return sb.toString();
	}
	
	public static void loadItemsFromLong(Room room, String s) {
		if (s == null || s.equals(""))
			return;
		String[] array = s.split(":");
		long[] l = new long[array.length];
		if (array.length > 0 && !array[0].isEmpty()) {
			try {
				for (int i = 0; i < array.length; i++) {
					if (!array[i].isEmpty())
						l[i] = Long.parseLong(array[i]);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				return;
			}
		}
		for (long value : l) {
			if (value == 0) continue;
			final int objX = (int)(value & 3);
			final int objY = (int)(value >> 3 & 3);
			final int oId = (int)(value >> 6 & 0x7fff);
			final int amount = (int)(value >> 21);
//			System.out.println(String.format("ID:%d x:%d y:%d amount:%d", oId, objX, objY, amount));
			room.getItems().add(new GroundItem(oId, objX+room.getX()*8, objY+room.getY()*8, room.getZ(), amount, 0));
		}
	}

	public static boolean exitFound(Player player, int roomX, int roomY, int roomZ, boolean rotating) {
		boolean foundExit = false;

		int[][] via = new int[Poh.MAXIMUM_ROOMS][Poh.MAXIMUM_ROOMS];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();

		int curX = roomX; // start x
		int curY = roomY; // start y
		via[curX][curY] = 81;
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		int pathLength = 81;

		final boolean findPortal = roomZ == Poh.GROUND_FLOOR; // if boolean findPortal == false then it will search for stairs.

		boolean foundPortal = false, foundStairs = false;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			if ((findPortal && foundPortal) || (!findPortal && foundStairs)) {
				foundExit = true;
				//				System.out.println("Found at X:"+(curX*8) + " Y: "+ (curY*8) + " findPortal:"+findPortal + " foundPortal:"+foundPortal+" foundStairs:"+foundStairs);
				break;
			}

			Room room1 = rotating ? player.getPOH().getCopiedRoom(curX, curY, roomZ) : player.getPOH().getRoom(curX, curY, roomZ);
			if (room1 == null) {
				foundExit = true;
				break;
			}
			if (findPortal && room1.isHasExitPortal()) {
				foundPortal = true;
			} else if (!findPortal && room1.hasStairs()) {
				foundStairs = true;
			}
			tail = (tail + 1) % pathLength;
			Room roomSouth = rotating ? player.getPOH().getCopiedRoom(curX, curY - 1, roomZ) : player.getPOH().getRoom(curX, curY - 1, roomZ);
			if (roomSouth != null && curY > 0 && via[curX][curY - 1] == 0
					&& room1.hasExit(Direction.SOUTH) && roomSouth.hasExit(Direction.NORTH)) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
				if (findPortal && roomSouth.isHasExitPortal()) {
					foundPortal = true;
					//					System.out.println("Found1 at X:"+(roomSouth.getX()*8) + " Y: "+ (roomSouth.getY()*8));
				} else if (!findPortal && roomSouth.hasStairs()) {
					foundStairs = true;
				}
			}
			Room roomWest = rotating ? player.getPOH().getCopiedRoom(curX - 1, curY, roomZ) : player.getPOH().getRoom(curX, curY - 1, roomZ);
			if (roomWest != null && curX > 0 && via[curX - 1][curY] == 0
					&& room1.hasExit(Direction.WEST) && roomWest.hasExit(Direction.EAST)) {

				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
				if (findPortal && roomWest.isHasExitPortal()) {
					foundPortal = true;
					//					System.out.println("Found2 at X:"+(roomWest.getX()*8) + " Y: "+ (roomWest.getY()*8));
				} else if (!findPortal && roomWest.hasStairs()) {
					foundStairs = true;
				}
			}
			Room roomNorth = rotating ? player.getPOH().getCopiedRoom(curX, curY + 1, roomZ) : player.getPOH().getRoom(curX, curY - 1, roomZ);
			if (roomNorth != null && curY < Poh.MAXIMUM_ROOMS - 1 && via[curX][curY + 1] == 0
					&& room1.hasExit(Direction.NORTH) && roomNorth.hasExit(Direction.SOUTH)) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
				if (findPortal && roomNorth.isHasExitPortal()) {
					foundPortal = true;
					//					System.out.println("Found3 at X:"+(roomNorth.getX()*8) + " Y: "+ (roomNorth.getY()*8));
				} else if (!findPortal && roomNorth.hasStairs()) {
					foundStairs = true;
				}
			}
			Room roomEast = rotating ? player.getPOH().getCopiedRoom(curX + 1, curY, roomZ) : player.getPOH().getRoom(curX, curY - 1, roomZ);
			if (roomEast != null && curX < Poh.MAXIMUM_ROOMS - 1 && via[curX + 1][curY] == 0
					&& room1.hasExit(Direction.EAST) && roomEast.hasExit(Direction.WEST)) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
				if (findPortal && roomEast.isHasExitPortal()) {
					foundPortal = true;
					//					System.out.println("Found4 at X:"+(roomEast.getX()*8) + " Y: "+ (roomEast.getY()*8));
				} else if (!findPortal && roomEast.hasStairs()) {
					foundStairs = true;
				}
			}
		}
		tileQueueX = null;
		tileQueueY = null;
		return foundExit;
	}

	public static boolean isStairs(int id) {
		return id >= 13497 && id <= 13506;
	}

	public static Location calculateStairsLocationMove(Player player, GameObject o, int newZ) {
		switch (o.getId()) {
		//spirals just lead down/up
		case 13503: // limebrick
		case 13504: // limebrick
		case 13505:
		case 13506:
		return Location.create(player.getX(), player.getY(), newZ);
		}
		int newX = 0, newY = 0;
		switch(o.getOrientation()) {
		case 0:
			newY += 3;
			break;

		case 1:
			newX += 3;
			break;

		case 2:
			newY -= 3;
			break;

		case 3:
			newX -= 3;
			break;
		}
		if (newZ < player.getZ()) {
			newX *= -1;
			newY *= -1;
		}
		return Location.create(player.getX()+newX, player.getY()+newY, newZ);
	}

	public static boolean canDeleteRoom(Player player, Location nextPaletteIndex) {
		if (nextPaletteIndex.getX() == 6 && nextPaletteIndex.getY() == 6 && nextPaletteIndex.getZ() == Poh.GROUND_FLOOR) {
			player.sendMessage("You cannot delete the starter room.");
			return false;
		}
		switch (nextPaletteIndex.getZ()) {
		case Poh.GROUND_FLOOR: // ground floor rules to delete rooms
		{
			// check upstairs
			Room upstairs = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), Poh.SECOND_FLOOR);
			if (upstairs != null) {
				player.getDialogueBuilder().sendStatement("You have a room upstairs that will", "have no support if you remove", "this room.").executeIfNotActive();
				return false;
			}
		}
		break;
		}
		return true;
	}
	
	public static void convertRoom(Player owner, Room currentRoom, Room newRoom) {
		Room.copyRoom(currentRoom, newRoom);
		owner.getPOH().placeRoom(newRoom);
		owner.getPacketSender().sendConstructMapRegionPacket(owner.getPOH());
	}
	
	public static Buildable getBuildableForHotspot(Player player, Room currentRoom, Room newRoom, Hotspot hotspot) {
		for (Buildable b : hotspot.getBuildables()) {
			if (player.getZ() < newRoom.getZ()) { // if building upstairs
				if (b.getObjectId() == currentRoom.getStairs().getId()) {
					return b;
				}
			} else { // else if building downstairs
				if (b.getObjectId(currentRoom.getType() == currentRoom.getUpperRoomType()) == currentRoom.getStairs().getId()) {
					return b;
				}
			}
		}
		return null;
	}
	
	public static Buildable buildable = null;
	
	public static void linkStairs(Client player, Room currentRoom, Room otherRoom) {
		Hotspot hotspot = HotspotFactory.getHotspot(currentRoom.getStairs().getHotspotType());
		buildable = getBuildableForHotspot(player, currentRoom, otherRoom, hotspot);
		player.getPOH().spawnObject(otherRoom, hotspot, buildable);
	}
	
	public static void changeWindowsInAllrooms(Player player, HouseStyle newStyle) {
		int roomSize = player.getPOH().getRooms()[0].length;
		for (int z = 0; z < 3; z++) {
			for (int x = 0; x < roomSize; x++) {
				for (int y = 0; y < roomSize; y++) {
					if (player.getPOH().getRoomCount(z) <= 0) continue;
					Room r = player.getPOH().getRooms()[z][x][y];
					if (r != null) {
						for (RoomObject w : r.getObjects().get(HotspotType.WINDOW)) {
							w.setId(newStyle.getWindowId());
						}

						for (RoomObject w : r.getObjects().get(HotspotType.WINDOW_SPACE)) {
							w.setId(swapChapelWindows(w.getId(), newStyle));
						}
					}
				}
			}
		}
	}

	public static double calculateConstructionXpModifier(Player player) {
		double modifier = 1.0;

		if (player.getItems().playerHasItem(ConstructionItems.CRYSTAL_SAW)) {
			modifier += 0.10;
		}

		if (player.getItems().playerHasEquipped(ConstructionItems.CONSTRUCTORS_HAT, PlayerConstants.playerHat)) {
			modifier += 0.04;
		}

		if (player.getItems().playerHasEquipped(ConstructionItems.CONSTRUCTORS_CHEST, PlayerConstants.playerChest)) {
			modifier += 0.04;
		}

		if (player.getItems().playerHasEquipped(ConstructionItems.CONSTRUCTORS_LEGS, PlayerConstants.playerLegs)) {
			modifier += 0.04;
		}

		if (player.getItems().playerHasEquipped(ConstructionItems.CONSTRUCTORS_BOOTS, PlayerConstants.playerFeet)) {
			modifier += 0.04;
		}

		if (player.getItems().playerHasEquipped(ConstructionItems.CONSTRUCTORS_GLOVES, PlayerConstants.playerHands)) {
			modifier += 0.04;
		}
		return modifier;
	}

	public static boolean playerHasNails(Player player, int amount) {
		return player.getItems().playerHasItem(ConstructionItems.RUNE_NAILS, amount) || player.getItems().playerHasItem(ConstructionItems.ADAM_NAILS, amount) || player.getItems().playerHasItem(ConstructionItems.MITH_NAILS, amount) || player.getItems().playerHasItem(ConstructionItems.BLACK_NAILS, amount) || player.getItems().playerHasItem(ConstructionItems.STEEL_NAILS, amount) || player.getItems().playerHasItem(ConstructionItems.IRON_NAILS, amount) || player.getItems().playerHasItem(ConstructionItems.BRONZE_NAILS, amount);
	}

	private static int swapChapelWindows(int currentId, HouseStyle style) {
		if (currentId >= 13217 && currentId <= 13225) { // tropical
			switch(style) {
				case TROPICAL_WOOD:
					return currentId;

				case BASIC_STONE:
					return currentId + 9;

				case WHITEWASHED_STONE:
					return currentId + (9 * 2);

				case FREMENNIK_STYLE_WOOD:
					return currentId + (9 * 3);

				case BASIC_WOOD:
					return currentId + (9 * 4);

				case FANCY_STONE:
					return currentId + (9 * 5);
			}
		} else if (currentId >= 13226 && currentId <= 13234) { // stone
			switch(style) {
				case TROPICAL_WOOD:
					return currentId - 9;

				case BASIC_STONE:
					return currentId;

				case WHITEWASHED_STONE:
					return currentId + 9;

				case FREMENNIK_STYLE_WOOD:
					return currentId + (9 * 2);

				case BASIC_WOOD:
					return currentId + (9 * 3);

				case FANCY_STONE:
					return currentId + (9 * 4);
			}
		} else if (currentId >= 13235 && currentId <= 13243) { // desert
			switch(style) {
				case TROPICAL_WOOD:
					return currentId - (9 * 2);

				case BASIC_STONE:
					return currentId - 9;

				case WHITEWASHED_STONE:
					return currentId;

				case FREMENNIK_STYLE_WOOD:
					return currentId + 9 ;

				case BASIC_WOOD:
					return currentId + (9 * 2);

				case FANCY_STONE:
					return currentId + (9 * 3);
			}
		} else if (currentId >= 13244 && currentId <= 13252) { // fremmy
			switch(style) {
				case TROPICAL_WOOD:
					return currentId - (9 * 3);

				case BASIC_STONE:
					return currentId - (9 * 2);

				case WHITEWASHED_STONE:
					return currentId - 9;

				case FREMENNIK_STYLE_WOOD:
					return currentId;

				case BASIC_WOOD:
					return currentId + 9;

				case FANCY_STONE:
					return currentId + (9 * 2);
			}
		} else if (currentId >= 13253 && currentId <= 13261) { // basic wood
			switch(style) {
				case TROPICAL_WOOD:
					return currentId - (9 * 4);

				case BASIC_STONE:
					return currentId - (9 * 3);

				case WHITEWASHED_STONE:
					return currentId - (9 * 2);

				case FREMENNIK_STYLE_WOOD:
					return currentId - 9;

				case BASIC_WOOD:
					return currentId;

				case FANCY_STONE:
					return currentId + 9;
			}
		} else if (currentId >= 13262 && currentId <= 13270) { // fancy stone
			switch(style) {
				case TROPICAL_WOOD:
					return currentId - (9 * 5);

				case BASIC_STONE:
					return currentId - (9 * 4);

				case WHITEWASHED_STONE:
					return currentId - (9 * 3);

				case FREMENNIK_STYLE_WOOD:
					return currentId - (9 * 2);

				case BASIC_WOOD:
					return currentId - 9;

				case FANCY_STONE:
					return currentId;
			}
		}

		return currentId;
	}
	
	public static void dropItemsInPoh(final Player c, final Item item, final int dropX, final int dropY) {
		if (DISABLED_DROP) {
			c.sendMessage("Dropping items in PoH is currently disabled");
			return;
		}
		
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			c.sendMessage("Wrong.");
			return;
		}
		
		if (c.getHomeOwnerId() != c.mySQLIndex) {
			c.sendMessage("You cannot drop items here.");
			return;
		}
		Optional<Room> roomResult = c.getPOH().getCurrentRoom();

        if (!roomResult.isPresent()) {
        	c.sendMessage("You cannot drop here.");
            return;
        }

        final Room currentRoom = roomResult.get();
        
        if (currentRoom.getType() == RoomType.EMPTY || currentRoom.getType() == RoomType.EMPTY_DUNG) {
        	c.sendMessage("You cannot drop here.");
        	return;
        }
        
        final int z = currentRoom.getZ();
        
		if (c.getItems().deleteItem(item)) {
			final int itemId = item.getId();
			final int amount = item.getAmount();
			currentRoom.getItems().add(new GroundItem(itemId, dropX, dropY, z, amount, 0));
			c.getPacketSender().playSound(Sounds.DROP_ITEM);
			c.getItems().createGroundItemPacket(itemId, dropX, dropY, amount);
		}
		
	}
	
	public static void pickupItem(final Client c, final int itemId, final int itemX, final int itemY) {
		if (c.getHomeOwnerId() != c.mySQLIndex) {
			c.sendMessage("This is not your item.");
			return;
		}
		Optional<Room> roomResult = c.getPOH().getCurrentRoom();

        if (!roomResult.isPresent()) {
            return;
        }

        final Room currentRoom = roomResult.get();
        
        Iterator<GroundItem> itr = currentRoom.getItems().iterator();
        
        while (itr.hasNext()) {
        	GroundItem item = itr.next();
        	
        	if (item.getItemId() == itemId && item.getItemX() == itemX && item.getItemY() == itemY && item.getItemZ() == c.getZ()) {
        		if (c.getItems().addItem(item.getItemId(), item.getItemAmount())) {
        			itr.remove();
        			c.getPacketSender().playSound(Sounds.PICKUP_ITEM);
        			c.getItems().removeGroundItem(itemId, itemX, itemY, 1);
        		}
        		break;
        	}
        }
		
	}
	
	public static int getWateringCan(Player p) {
		for (int i = 0; i < p.playerItems.length; i++) {
			int itemId = p.playerItems[i]-1;
			if (itemId == ConstructionItems.WATERING_CAN || itemId == ConstructionItems.WATERING_CAN_1
					|| itemId == ConstructionItems.WATERING_CAN_2 || itemId == ConstructionItems.WATERING_CAN_3
					|| itemId == ConstructionItems.WATERING_CAN_4 || itemId == ConstructionItems.WATERING_CAN_5
					|| itemId == ConstructionItems.WATERING_CAN_6 || itemId == ConstructionItems.WATERING_CAN_7
					|| itemId == ConstructionItems.WATERING_CAN_8) {
				return i;
			}
		}
		return -1;
	}

}
