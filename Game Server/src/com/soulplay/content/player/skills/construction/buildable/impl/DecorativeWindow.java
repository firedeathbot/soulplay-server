package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

import java.util.Arrays;
import java.util.List;

public class DecorativeWindow extends Buildable {

    int objectId = 13221;

    @Override
    public void onBuild(Player player, Room room) {
        switch (player.getPOH().getHouseStyle()) {

            case BASIC_WOOD:
                objectId = 13253 + 1 + (2 * Misc.random(3));
                break;

            case BASIC_STONE:
                objectId = 13226 + 1 + (2 * Misc.random( 3));
                break;

            case FANCY_STONE:
                objectId = 13262 + 1 + (2 * Misc.random(3));
                break;

            case TROPICAL_WOOD:
                objectId = 13217 + 1 + (2 * Misc.random(3));
                break;

            case WHITEWASHED_STONE:
                objectId = 13235 + 1 + (2 * Misc.random(3));
                break;

            case FREMENNIK_STYLE_WOOD:
                objectId = 13244 + 1 + (2 * Misc.random(3));
                break;

            default:
                objectId = objectId + 1;
                break;
        }
    }

    @Override
    public String getName() {
        return "Decorative window";
    }

    @Override
    public int getItemId() {
        return 8077;
    }

    @Override
    public int getObjectId() {
        return objectId;
    }

    @Override
    public int getLevelRequired() {
        return 69;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MOLTEN_GLASS, 8));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 4;
    }
}
