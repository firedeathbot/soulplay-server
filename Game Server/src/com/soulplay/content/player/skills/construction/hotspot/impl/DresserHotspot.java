package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DresserHotspot extends Hotspot {

    public DresserHotspot() {
        super(HotspotType.DRESSER);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new ShavingStand(), new OakShavingStand(), new OakDresser(), new TeakDresser(), new FancyTeakDresser(), new MahoganyDresser(), new GildedDresser()));
    }
}
