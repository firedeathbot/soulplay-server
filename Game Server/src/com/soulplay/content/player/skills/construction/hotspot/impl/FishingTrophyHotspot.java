package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MountedBass;
import com.soulplay.content.player.skills.construction.buildable.impl.MountedShark;
import com.soulplay.content.player.skills.construction.buildable.impl.MountedSwordfish;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class FishingTrophyHotspot extends Hotspot {

    public FishingTrophyHotspot() {
        super(HotspotType.FISHING_TROPHY);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new MountedBass(), new MountedSwordfish(), new MountedShark()));
    }

}
