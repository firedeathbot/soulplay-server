package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.ObjectWrapper;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class FencingRing extends Buildable {

    @Override
    protected void init() {
        // outside corners
        objectMap.put(15278, new ObjectWrapper(13133, 3));
        objectMap.put(15279, new ObjectWrapper(13133,3 ));
        objectMap.put(15281, new ObjectWrapper(13133, 3));
        objectMap.put(15282, new ObjectWrapper(13133, 3));

        // outside side
        objectMap.put(15277, new ObjectWrapper(13133, 0));
        objectMap.put(15286, new ObjectWrapper(13133, 0));
        objectMap.put(15287, new ObjectWrapper(13133, 0));
        objectMap.put(15280, new ObjectWrapper(13133, 0));

        // center center
        objectMap.put(15292, new ObjectWrapper(13136, 22));

        // center corner
        objectMap.put(15294, new ObjectWrapper(13135, 22));
        objectMap.put(15290, new ObjectWrapper(13135, 22));
        objectMap.put(15295, new ObjectWrapper(13135, 22));
        objectMap.put(15289, new ObjectWrapper(13135, 22));

        // center side
        objectMap.put(15293, new ObjectWrapper(13134, 22));
        objectMap.put(15288, new ObjectWrapper(13134, 22));
        objectMap.put(15291, new ObjectWrapper(13134, 22));
    }

    @Override
    public String getName() {
        return "Fencing ring";
    }

    @Override
    public int getItemId() {
        return 8024;
    }

    @Override
    public int getObjectId() {
        return -1;
    }

    @Override
    public int getLevelRequired() {
        return 41;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 8), new Item(ConstructionItems.CLOTH, 6));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 570;
    }
}
