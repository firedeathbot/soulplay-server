package com.soulplay.content.player.skills.construction.buildable;

public enum BuildType {

    RECURSIVE,
    NON_RECURSIVE

}
