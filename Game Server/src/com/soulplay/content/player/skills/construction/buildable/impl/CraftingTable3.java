package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class CraftingTable3 extends Buildable {
    @Override
    public String getName() {
        return "Crafting table 3";
    }

    @Override
    public int getItemId() {
        return 8382;
    }

    @Override
    public int getObjectId() {
        return 13711;
    }

    @Override
    public int getLevelRequired() {
        return 34;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(8381), new Item(ConstructionItems.MOLTEN_GLASS, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 2;
    }
}
