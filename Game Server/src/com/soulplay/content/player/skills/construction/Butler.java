package com.soulplay.content.player.skills.construction;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Butler {

    private ButlerType type;
    private NPC npc;
    private int counter;
    private boolean faceFlag;
    private Optional<Item> usedOn = Optional.empty();
    private List<Item> items = new ArrayList<>();
    private ButlerState state = ButlerState.WALK_TO_PLAYER;
    private int uses;
    private boolean greeting;

    public boolean hasToPayWage() {
        return uses > 8;
    }

    public ButlerType getType() {
        return type;
    }

    public void setType(ButlerType type) {
        this.type = type;
    }

    public enum ButlerState {
        IDLE,
        GREET,
        BANKING,
        SAWMILL,
        DISMISS,
        SPEAK_TO_PLAYER,
        WALK_TO_PLAYER
    }

    public void setUses(int uses) {
        this.uses = uses;
    }

    public boolean isGreeting() {
        return greeting;
    }

    public void setGreeting(boolean greeting) {
        this.greeting = greeting;
    }

    public void incrementUses() {
        uses++;
    }

    public int getUses() {
        return uses;
    }

    public NPC getNpc() {
        return npc;
    }

    public void setNpc(NPC npc) {
        this.npc = npc;
    }

    public ButlerState getState() {
        return state;
    }

    public void setState(ButlerState state) {
        this.state = state;
    }

    public List<Item> getItems() {
        return items;
    }

    public Optional<Item> getUsedOn() {
        return usedOn;
    }

    public void setUsedOn(Optional<Item> usedOn) {
        this.usedOn = usedOn;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean isFaceFlag() {
        return faceFlag;
    }

    public void setFaceFlag(boolean faceFlag) {
        this.faceFlag = faceFlag;
    }

}
