package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

import java.util.Arrays;
import java.util.List;

public class StainedGlass extends Buildable {

    int objectId = 13221;

    @Override
    public void onBuild(Player player, Room room) {
        switch (player.getPOH().getHouseStyle()) {

            case BASIC_WOOD:
                objectId = 13253 + 2 * Misc.random(1, 4);
                break;

            case BASIC_STONE:
                objectId = 13226 + 2 * Misc.random(1, 4);
                break;

            case FANCY_STONE:
                objectId = 13262 + 2 * Misc.random(1, 4);
                break;

            case TROPICAL_WOOD:
                objectId = 13217 + 2 * Misc.random(1, 4);
                break;

            case WHITEWASHED_STONE:
                objectId = 13235 + 2 * Misc.random(1, 4);
                break;

            case FREMENNIK_STYLE_WOOD:
                objectId = 13244 + 2 * Misc.random(1, 4);
                break;

            default:
                objectId = objectId + 2;
                break;
        }
    }

    @Override
    public String getName() {
        return "Stained glass";
    }

    @Override
    public int getItemId() {
        return 8078;
    }

    @Override
    public int getObjectId() {
        return objectId;
    }

    @Override
    public int getLevelRequired() {
        return 89;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MOLTEN_GLASS, 16));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 5;
    }
}
