package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyPortal;
import com.soulplay.content.player.skills.construction.buildable.impl.MarblePortal;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakPortal;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class Portal3Hotspot extends Hotspot {

    public Portal3Hotspot() {
        super(HotspotType.PORTAL_3);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new TeakPortal(), new MahoganyPortal(), new MarblePortal()));
    }
}