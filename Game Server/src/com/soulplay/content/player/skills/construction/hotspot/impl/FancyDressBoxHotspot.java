package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyFancyDressBox;
import com.soulplay.content.player.skills.construction.buildable.impl.OakFancyDressBox;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakFancyDressBox;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class FancyDressBoxHotspot extends Hotspot {

    public FancyDressBoxHotspot() {
        super(HotspotType.FANCY_DRESS_BOX);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakFancyDressBox(), new TeakFancyDressBox(), new MahoganyFancyDressBox()));
    }

}
