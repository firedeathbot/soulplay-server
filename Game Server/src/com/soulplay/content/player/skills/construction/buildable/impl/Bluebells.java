package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

import java.util.Arrays;
import java.util.List;

public class Bluebells extends Buildable {
    @Override
    public String getName() {
        return "Bluebells";
    }

    @Override
    public int getItemId() {
        return 8212;
    }

    @Override
    public int getObjectId() {
        return 13439;
    }

    @Override
    public int getLevelRequired() {
        return 71;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(8455));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public boolean hasRequiredItems(Player player, boolean showMessage) {
        if ((player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_8) || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_7)
                || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_6)
                || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_5)
                || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_4)
                || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_3)
                || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_2)
                || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_1)) && player.getItems().playerHasItem(8455)) {
            return true;
        }

        if (showMessage) {
            player.sendMessage("You need a watering can.");
        }

        return false;
    }

    @Override
    public void onDeleteMaterials(Player player) {
        Construction.degradeWateringCan(player);

        player.getItems().deleteItem2(8455, 1);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.WATERING_ANIM;
    }

    @Override
    public int getExperience() {
        return 122;
    }
}
