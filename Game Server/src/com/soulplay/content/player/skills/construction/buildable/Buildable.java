package com.soulplay.content.player.skills.construction.buildable;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.ObjectWrapper;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Buildable {

    protected static final Item HAMMER = new Item(ConstructionItems.HAMMER);
    protected final Map<Integer, ObjectWrapper> objectMap = new HashMap<>();

    public Buildable() {
        init();
    }

    protected void init() {

    }

    public void onBuild(Player player, Room room) {

    }

    public BuildType getBuildType() {
        return BuildType.RECURSIVE;
    }

    public Location positionOverride() {
        return Location.create(-1, -1);
    }

    public abstract String getName();

    public abstract int getItemId();

    public abstract int getObjectId();

    public abstract int getLevelRequired();

    public abstract List<Item> getMaterialsNeeded();

    public abstract List<Item> getRequiredItems();

    public abstract int getAnimation();

    public abstract int getExperience();

    public int getRequiredNails() {
        return 0;
    }

    public void onAchievement(Player player) {

    }

    public int getObjectId(boolean upperRoom) {
        return getObjectId();
    }

    public final boolean hasRequiredItems(Player player) {
        return hasRequiredItems(player, false);
    }

    public boolean hasRequiredItems(Player player, boolean showMessage) {
    	
    	if (player.isDev()) {
    		return true;
    	}

        final List<Item> materials = getMaterialsNeeded();

        for (Item material : materials) {
            if (!player.getItems().playerHasItem(material.getId(), material.getAmount())) {
                if (showMessage) {
                    player.sendMessage("You do not have the required materials to build this!");
                }
                return false;
            }
        }

        final List<Item> requiredItems = getRequiredItems();
        for (Item item : requiredItems) {
            if (!player.getItems().playerHasItem(item.getId(), item.getAmount())) {
                if (showMessage) {
                    final ItemDef def = ItemDef.forID(item.getId());
                    if (def == null || def.name == null) {
                        player.sendMessage("You do not have the required items to build this.");
                    } else {
                        if (item.getAmount() == 1) {
                            player.sendMessage(String.format("You need %s %s to build this.", Misc.aOrAn(def.name), def.name));
                        } else {
                            player.sendMessage(String.format("You need x%d %s to build this.", item.getAmount(), def.name));
                        }
                    }
                }
                return false;
            }
        }

        return true;
    }

    public void onDeleteMaterials(Player player) {
        List<Item> materials = getMaterialsNeeded();

        for (Item material : materials) {
            player.getItems().deleteItem2(material.getId(), material.getAmount());
        }
    }

    protected List<String> getMaterialDescriptionLines() {
        return Arrays.asList();
    }

    public final String getMaterialDescription() {
        final List<String> lines = getMaterialDescriptionLines();

        final StringBuilder builder = new StringBuilder();

        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);

            if (i < lines.size() - 1) {
                builder.append(line).append("\\n");
            } else {
                builder.append(line);
            }
        }

        return builder.toString();
    }

    public Map<Integer, ObjectWrapper> getObjectMap() {
        return objectMap;
    }
}
