package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

import java.util.Arrays;
import java.util.List;

public class GildedAltar extends Buildable {
    @Override
    public String getName() {
        return "Gilded altar";
    }

    @Override
    public int getItemId() {
        return 8068;
    }

    @Override
    public int getObjectId() {
        return 13185;
    }

    @Override
    public void onAchievement(Player player) {
        player.getAchievement().buildGildedAtlar();
    }

    @Override
    public int getLevelRequired() {
        return 75;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MARBLE_BLOCK, 2), new Item(ConstructionItems.CLOTH, 2), new Item(ConstructionItems.GOLD_LEAF, 4));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 2230;
    }
}
