package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DiningTableHotspot extends Hotspot {
    public DiningTableHotspot() {
        super(HotspotType.DINING_TABLE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodDiningTable(), new OakDiningTable(), new CarvedOakTable(), new TeakDiningTable(), new MahoganyTable(), new OpulentTable()));
    }
}
