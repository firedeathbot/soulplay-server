package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class FirepitPot extends Buildable {

    @Override
    public String getName() {
        return "Firepit with pot";
    }

    @Override
    public int getItemId() {
        return 8218;
    }

    @Override
    public int getObjectId() {
        return 13531;
    }

    @Override
    public int getLevelRequired() {
        return 17;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.STEEL_BAR, 3), new Item(ConstructionItems.SOFT_CLAY, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 80;
    }

}
