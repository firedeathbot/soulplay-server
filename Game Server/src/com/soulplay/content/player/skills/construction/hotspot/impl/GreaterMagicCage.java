package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

import java.util.Arrays;
import java.util.List;

public class GreaterMagicCage extends Buildable {
    @Override
    public String getName() {
        return "Greater magic cage";
    }

    @Override
    public int getItemId() {
        return 8374;
    }

    @Override
    public int getObjectId() {
        return 13688;
    }
    
    @Override
    public void onAchievement(Player player) {
        player.getAchievement().buildMagicCage();
    }

    @Override
    public int getLevelRequired() {
        return 89;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 5), new Item(8788, 4));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 4700;
    }
}
