package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.ArcheryTarget;
import com.soulplay.content.player.skills.construction.buildable.impl.Dartboard;
import com.soulplay.content.player.skills.construction.buildable.impl.HoopAndStick;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class RangingGameHotspot extends Hotspot {

    public RangingGameHotspot() {
        super(HotspotType.RANGING_GAME);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new HoopAndStick(), new Dartboard(), new ArcheryTarget()));
    }

}
