package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class BedRoom extends Room {

	private static final Palette palette = new Palette(8, 1);

	private static final boolean[] doors = {false, false, true, true};

	public BedRoom(int x, int y, int z) {
		super(RoomType.BED, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

    public static void load() {
    	/* empty */
    }

	static {
		hotspots.put(HotspotType.CLOCK, new RoomObject(15268, Location.create(7, 0), 1, 11));
		hotspots.put(HotspotType.BED, new RoomObject(15260, Location.create(3, 6), 0, 10));
		hotspots.put(HotspotType.WARDROBE, new RoomObject(15261, Location.create(6, 7), 0, 10));
		hotspots.put(HotspotType.FIREPLACE, new RoomObject(15267, Location.create(7, 3), 2, 10));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(2, 7), 1, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(5, 7), 1, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(0, 5), 0, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(0, 2), 0, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(2, 0), 3, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(5, 0), 3, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(7, 2), 2, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15263, Location.create(7, 5), 2, 5));
		hotspots.put(HotspotType.DRESSER, new RoomObject(15262, Location.create(0, 7),0, 10));

		// corners
		hotspots.put(HotspotType.RUG, new RoomObject(15266, Location.create(1, 4), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15266, Location.create(6, 4), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15266, Location.create(6, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15266, Location.create(1, 1), 3, 22));

		// outer
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(2, 4), 0, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(3, 4), 0, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(4, 4), 0, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(5, 4), 0, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(6, 3), 1, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(6, 2), 1, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(2, 1), 2, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(3, 1), 2, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(4, 1), 2, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(5, 1), 2, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(1, 2), 3, 22).setOffset(1));
		hotspots.put(HotspotType.RUG, new RoomObject(15265, Location.create(1, 3), 3, 22).setOffset(1));

		// inner
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(2, 3), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(3, 3), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(4, 3), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(5, 3), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(2, 2), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(3, 2), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(4, 2), 0, 22).setOffset(2));
		hotspots.put(HotspotType.RUG, new RoomObject(15264, Location.create(5, 2), 0, 22).setOffset(2));
	}

}
