package com.soulplay.content.player.skills.construction;

public enum HouseStyle {

    BASIC_WOOD(1, 5000, 7247, 0, 13100, 13101, 13098, 13099),
    BASIC_STONE(10, 5000, 7247, 1, 13094, 13096, 1902, 13091),
    WHITEWASHED_STONE(20, 7500, 7247, 2, 13006, 13007, 1415, 13005),
    FREMENNIK_STYLE_WOOD(30, 10000, 7247, 3, 13109, 13107, 13111, 13112),
    TROPICAL_WOOD(40, 15000, 7503, 0, 13016, 13015, 13011, 10816),
    FANCY_STONE(50, 25000, 7503, 1, 13119, 13118, 13116, 13117);

    private final int level;
    private final int cost;
    private final int regionId;
    private final int plane;
    private final int doorId;
    private final int secondDoorId;
    private final int wallId;
    private final int windowId;

    HouseStyle(int level, int cost, int regionId, int plane, int doorId, int secondDoorId, int wallId, int windowId) {
        this.level = level;
        this.cost = cost;
        this.regionId = regionId;
        this.plane = plane;
        this.doorId = doorId;
        this.secondDoorId = secondDoorId;
        this.wallId = wallId;
        this.windowId = windowId;
    }

    public int getLevel() {
        return level;
    }

    public int getCost() {
        return cost;
    }

    public int getRegionId() {
        return regionId;
    }

    public int getPlane() {
        return plane;
    }

    public int getDoorId() {
        return doorId;
    }

    public int getWallId() {
        return wallId;
    }

    public int getSecondDoorId() {
        return secondDoorId;
    }

	public int getWindowId() {
		return windowId;
	}
}
