package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class OakDrawers extends Buildable {
    @Override
    public String getName() {
        return "Oak drawers";
    }

    @Override
    public int getItemId() {
        return 8039;
    }

    @Override
    public int getObjectId() {
        return 13156;
    }

    @Override
    public int getLevelRequired() {
        return 27;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new  Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 120;
    }
}
