package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class BrownRug extends Buildable {

    @Override
    public String getName() {
        return "Brown rug";
    }

    @Override
    public int getItemId() {
        return 8316;
    }

    @Override
    public int getObjectId() {
        return 13588;
    }

    @Override
    public int getLevelRequired() {
        return 2;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.CLOTH, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 30;
    }

}
