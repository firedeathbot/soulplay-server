package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;

import com.soulplay.content.player.skills.construction.buildable.impl.Curtains;
import com.soulplay.content.player.skills.construction.buildable.impl.OpulentCurtains;
import com.soulplay.content.player.skills.construction.buildable.impl.TornCurtains;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class CurtainHotspot extends Hotspot {

    public CurtainHotspot() {
	super(HotspotType.CURTAIN);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new TornCurtains(), new Curtains(), new OpulentCurtains()));
    }

}
