package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ThroneHotspot extends Hotspot {

    public ThroneHotspot() {
        super(HotspotType.THRONE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakThrone(), new TeakThrone(), new MahoganyThrone(), new GildedThrone(), new SkeletonThrone(), new CrystalThrone(), new DemonicThrone()));
    }

}
