package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.AdamantArmor;
import com.soulplay.content.player.skills.construction.buildable.impl.MithrilArmor;
import com.soulplay.content.player.skills.construction.buildable.impl.RuneArmor;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ArmorHotspot extends Hotspot {

    public ArmorHotspot() {
        super(HotspotType.ARMOR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new MithrilArmor(), new AdamantArmor(), new RuneArmor()));
    }
}
