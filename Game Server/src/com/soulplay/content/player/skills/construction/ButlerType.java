package com.soulplay.content.player.skills.construction;

import java.util.Arrays;
import java.util.Optional;

public enum ButlerType {
    JACK(4235, 11303, 20, 500, 6, 100),
    MAID(4237, 11304, 25, 1000, 10, 50),
    COOK(4239, 11305, 30, 3000, 16, 28),
    BUTLER(4241, 11306, 40, 5000, 20, 20),
    DEMON_BUTLER(4243, 11307, 50, 10000, 26, 12);
	
	public static final ButlerType[] values = ButlerType.values();

    private final int npcId;
    private final int purchaseNpcId;
    private final int lvlRequired;
    private final int purchaseCost;
    private final int capacity;
    private final int tripTicks;

    ButlerType(int npcId, int purchaseNpcId, int lvlRequired, int purchaseCost, int capacity,
                    int tripTicks) {
        this.npcId = npcId;
        this.purchaseNpcId = purchaseNpcId;
        this.lvlRequired = lvlRequired;
        this.purchaseCost = purchaseCost;
        this.capacity = capacity;
        this.tripTicks = tripTicks;
    }

    public static Optional<ButlerType> lookup(int npcId) {
        return Arrays.stream(ButlerType.values).filter(it -> it.npcId == npcId || it.purchaseNpcId == npcId).findFirst();
    }

    public int getNpcId() {
        return npcId;
    }

    public int getPurchaseNpcId() {
        return purchaseNpcId;
    }

    public int getLvlRequired() {
        return lvlRequired;
    }

    public int getPurchaseCost() {
        return purchaseCost;
    }

    public int getCapacity() {
        return capacity;
    }

    public double getTripTicks() {
        return tripTicks;
    }

}
