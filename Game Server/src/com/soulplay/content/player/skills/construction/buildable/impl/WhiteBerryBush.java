package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerDifficulty;

public class WhiteBerryBush extends Buildable {

	@Override
	public String getName() {
		return "Whiteberry Bush";
	}

	@Override
	public int getItemId() {
		return 239;
	}

	@Override
	public int getObjectId() {
		return 7721;
	}

	@Override
	public int getLevelRequired() {
		return 65;
	}

	@Override
	public List<Item> getMaterialsNeeded() {
		return Arrays.asList(new Item(ConstructionItems.WHITE_BERRIES));
	}

	@Override
	public List<Item> getRequiredItems() {
		return Arrays.asList();
	}

	@Override
	public int getExperience() {
		return 70;
	}

	@Override
	public boolean hasRequiredItems(Player player, boolean showMessage) {
		
		boolean hasCan = ConstructionUtils.getWateringCan(player) != -1;
		if (hasCan && player.getItems().playerHasItem(ConstructionItems.WHITE_BERRIES)) {
			return true;
		}

		if (showMessage) {
			if (!hasCan)
				player.sendMessage("You need a watering can.");
			else
				player.sendMessage("You are missing the required materials.");
		}

		return false;
	}

	@Override
	public void onDeleteMaterials(Player player) {
		Construction.degradeWateringCan(player);

		player.getItems().deleteItem2(ConstructionItems.WHITE_BERRIES, 1);
		
		player.getPOH().setWhiteberryBushCount(player.getPOH().getWhiteberryBushCount()+1);
	}

	@Override
	public int getAnimation() {
		return ConstructionAnimations.WATERING_ANIM;
	}

}
