package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

public class YewTree extends Buildable {

    @Override
    public String getName() {
	return "Yew Tree";
    }

    @Override
    public int getItemId() {
	return ConstructionItems.YEW_TREE;
    }

    @Override
    public int getObjectId() {
	return 13416;
    }

    @Override
    public int getLevelRequired() {
	return 60;
    }    
    
    @Override
    public List<Item> getMaterialsNeeded() {
	return Arrays.asList(new Item(ConstructionItems.BAGGED_YEW_TREE));
    }
    
    @Override
    public List<Item> getRequiredItems() {
	return Arrays.asList();
    }
    
    @Override
    public boolean hasRequiredItems(Player player, boolean showMessage) {
	if ((player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_8) || player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_7)
		|| player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_6)
			|| player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_5)
			|| player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_4)
			|| player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_3)
			|| player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_2)
			|| player.getItems().playerHasItem(ConstructionItems.WATERING_CAN_1)) && player.getItems().playerHasItem(ConstructionItems.BAGGED_YEW_TREE)) {
	    return true;
	}

        if (showMessage) {
            player.sendMessage("You need a watering can.");
        }
	
	return false;
    }

    @Override
    public void onDeleteMaterials(Player player) {
	Construction.degradeWateringCan(player);
	
	player.getItems().deleteItem2(ConstructionItems.BAGGED_YEW_TREE, 1);
    }

    @Override
    public int getExperience() {
	return 141;
    }
    
    @Override
    public int getAnimation() {
	return ConstructionAnimations.WATERING_ANIM;
    }

}
