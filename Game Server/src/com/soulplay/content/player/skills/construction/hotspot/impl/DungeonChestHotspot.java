package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DungeonChestHotspot extends Hotspot {

    public DungeonChestHotspot() {
        super(HotspotType.DUNGEON_CHEST);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenCrate(), new OakChest(), new TeakChest(), new MahoganyChest(), new MagicChest()));
    }
}
