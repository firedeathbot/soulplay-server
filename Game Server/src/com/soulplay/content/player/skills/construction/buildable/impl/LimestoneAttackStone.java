package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class LimestoneAttackStone extends Buildable {

    @Override
    public String getName() {
        return "Limestone attack stone";
    }

    @Override
    public int getItemId() {
        return 8154;
    }

    @Override
    public int getObjectId() {
        return 13393;
    }

    @Override
    public int getLevelRequired() {
        return 59;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.LIMESTONE_BRICK, 10));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 200;
    }
}
