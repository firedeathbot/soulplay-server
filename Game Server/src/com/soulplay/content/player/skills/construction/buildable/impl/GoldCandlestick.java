package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class GoldCandlestick extends Buildable {
    @Override
    public String getName() {
        return "Incense burners";
    }

    @Override
    public int getItemId() {
        return 8072;
    }

    @Override
    public int getObjectId() {
        return 13206;
    }

    @Override
    public int getLevelRequired() {
        return 57;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(2357, 6), new Item(36, 6));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 46;
    }
}
