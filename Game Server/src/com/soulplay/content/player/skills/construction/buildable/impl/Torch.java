package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Torch extends Buildable {

    @Override
    public String getName() {
        return "Torch";
    }

    @Override
    public int getItemId() {
        return 8129;
    }

    @Override
    public int getObjectId() {
        return 13343;
    }

    @Override
    public int getLevelRequired() {
        return 84;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 4), new Item(ConstructionItems.LIT_CANDLE, 4));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 244;
    }

}
