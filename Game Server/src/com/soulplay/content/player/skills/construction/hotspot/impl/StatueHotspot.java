package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.LargeStatue;
import com.soulplay.content.player.skills.construction.buildable.impl.MediumStatue;
import com.soulplay.content.player.skills.construction.buildable.impl.SmallStatue;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class StatueHotspot extends Hotspot {

    public StatueHotspot() {
        super(HotspotType.STATUE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new SmallStatue(), new MediumStatue(), new LargeStatue()));
    }
}
