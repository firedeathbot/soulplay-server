package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class PortalRoom extends Room {

	private static final Palette palette = new Palette(3, -2);

	private static final boolean[] doors = {false, false, true, false};

	public PortalRoom(int x, int y, int z) {
		super(RoomType.PORTAL, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.PORTAL_1, new RoomObject(15406, Location.create(0, 3), 1, 10));
		hotspots.put(HotspotType.PORTAL_2, new RoomObject(15407, Location.create(3, 7), 2, 10));
		hotspots.put(HotspotType.PORTAL_3, new RoomObject(15408, Location.create(7, 3), 3, 10));
		hotspots.put(HotspotType.PORTAL_CENTERPIECE, new RoomObject(15409, Location.create(3, 3), 1, 10));
	}

}
