package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class MagicWardrobeHotspot extends Hotspot {

    public MagicWardrobeHotspot() {
        super(HotspotType.MAGIC_WARDROBE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakMagicWardrobe(), new CarvedOakMagicWardrobe(), new TeakMagicWardrobe(), new CarvedTeakMagicWardrobe(), new MahoganyMagicWardrobe(), new GildedMagicWardrobe(), new MarbleMagicWardrobe()));
    }

}
