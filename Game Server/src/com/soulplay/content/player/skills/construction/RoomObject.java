package com.soulplay.content.player.skills.construction;

import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class RoomObject extends GameObject {
    
    private final int hotspotId;

    private int offset;

    private boolean symetric;
    
    private final HotspotType hotspotType;

    public RoomObject(int id, Location location, int orientation, int type) {
        this(id, id, location, orientation, type);
    }

    public RoomObject(int hotspotId, int id, Location location, int orientation, int type) {
        super(id, location, orientation, type);
        this.hotspotId = hotspotId;
        this.hotspotType = HotspotType.lookup(hotspotId);
    }

    public RoomObject setOffset(int offset) {
        this.offset = offset;
        return this;
    }

    public void setSymetric(boolean symatric) {
        this.symetric = symatric;
    }

    public boolean isSymetric() {
        return this.symetric;
    }

    public int getOffset() {
        return offset;
    }

    public int getHotspotId() {
	return hotspotId;
    }

	public HotspotType getHotspotType() {
		return hotspotType;
	}

}
