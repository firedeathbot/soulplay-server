package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.OakLarder;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakLarder;
import com.soulplay.content.player.skills.construction.buildable.impl.WoodenLarder;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LarderHotspot extends Hotspot {

    public LarderHotspot() {
        super(HotspotType.LARDER);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenLarder(), new OakLarder(), new TeakLarder()));
    }

}
