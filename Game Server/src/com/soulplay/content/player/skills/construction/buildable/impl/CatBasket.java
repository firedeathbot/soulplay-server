package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class CatBasket extends Buildable {

    @Override
    public String getName() {
        return "Cat basket";
    }

    @Override
    public int getItemId() {
        return 8237;
    }

    @Override
    public int getObjectId() {
        return 13575;
    }

    @Override
    public int getLevelRequired() {
        return 19;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.PLANK, 2));
    }

    @Override
    public int getRequiredNails() {
        return 2;
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 58;
    }

}
