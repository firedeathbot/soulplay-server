package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.buildable.impl.SteelCage;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class PrisonSpaceHotspot extends Hotspot {

    public PrisonSpaceHotspot() {
        super(HotspotType.PRISON_SPACE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakCage(), new OakAndSteelCage(), new SteelCage(), new SpikedCage(), new BoneCage()));
    }
}
