package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class WoodenShelves3 extends Buildable {
    @Override
    public String getName() {
        return "Wooden shelves 3";
    }

    @Override
    public int getItemId() {
        return 8225;
    }

    @Override
    public int getObjectId() {
        return 13547;
    }

    @Override
    public int getLevelRequired() {
        return 23;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.PLANK, 3), new Item(ConstructionItems.SOFT_CLAY, 6));
    }

    @Override
    public int getRequiredNails() {
        return 3;
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 147;
    }
}
