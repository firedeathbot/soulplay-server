package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyBookcase;
import com.soulplay.content.player.skills.construction.buildable.impl.OakBookcase;
import com.soulplay.content.player.skills.construction.buildable.impl.WoodenBookcase;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class BookcaseHotspot extends Hotspot {

    public BookcaseHotspot() {
	super(HotspotType.BOOKCASE);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new WoodenBookcase(), new OakBookcase(), new MahoganyBookcase()));
    }

}
