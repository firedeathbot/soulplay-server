package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Runecase2 extends Buildable {
    @Override
    public String getName() {
        return "Rune case 1";
    }

    @Override
    public int getItemId() {
        return 8277;
    }

    @Override
    public int getObjectId() {
        return 13508;
    }

    @Override
    public int getLevelRequired() {
        return 41;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK, 2), new Item(ConstructionItems.MOLTEN_GLASS, 2), new Item(559), new Item(564), new Item(562), new Item(561));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 190;
    }

    public List<String> getMaterialDescriptionLines() {
        return Arrays.asList("Teak plank x2", "Molten glass x2", "body, cosmic, chaos, nature x1");
    }
}
