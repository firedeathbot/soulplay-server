package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class OakDresser extends Buildable {
    @Override
    public String getName() {
        return "Oak dresser";
    }

    @Override
    public int getItemId() {
        return 8047;
    }

    @Override
    public int getObjectId() {
        return 13164;
    }

    @Override
    public int getLevelRequired() {
        return 37;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 2), new Item(ConstructionItems.MOLTEN_GLASS));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 121;
    }
}
