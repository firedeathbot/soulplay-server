package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class BoundaryStones extends Buildable {
    @Override
    public String getName() {
        return "Boundary stones";
    }

    @Override
    public int getItemId() {
        return 8196;
    }

    @Override
    public int getObjectId() {
        return 13449;
    }

    @Override
    public int getLevelRequired() {
        return 55;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(1761, 10));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 100;
    }
}
