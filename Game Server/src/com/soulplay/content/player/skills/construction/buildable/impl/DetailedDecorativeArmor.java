package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class DetailedDecorativeArmor extends Buildable {
    @Override
    public String getName() {
        return "Detailed decorative armor";
    }

    @Override
    public int getItemId() {
        return 8274;
    }

    @Override
    public int getObjectId() {
        return 13496;
    }

    @Override
    public int getLevelRequired() {
        return 28;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 2), new Item(4506), new Item(4507), new Item(4504));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 35;
    }
}
