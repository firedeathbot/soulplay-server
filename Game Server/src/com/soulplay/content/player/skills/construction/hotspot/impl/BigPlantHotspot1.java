package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;
import com.soulplay.content.player.skills.construction.buildable.impl.BigFern;
import com.soulplay.content.player.skills.construction.buildable.impl.Bush;
import com.soulplay.content.player.skills.construction.buildable.impl.TallPlant;
import com.soulplay.content.player.skills.construction.buildable.impl.WhiteBerryBush;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class BigPlantHotspot1 extends Hotspot {

    public BigPlantHotspot1() {
	super(HotspotType.BIG_PLANT_1);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new BigFern(), new Bush(), new TallPlant(), new WhiteBerryBush()));
    }

}