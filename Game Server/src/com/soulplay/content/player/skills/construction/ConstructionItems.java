package com.soulplay.content.player.skills.construction;

public class ConstructionItems {
	
	public static final int WHITE_BERRIES = 239;

    public static final int MAGIC_STONE = 8788;
    
    public static final int WATERING_CAN = 5331;
    
    public static final int WATERING_CAN_1 = 5333;
    
    public static final int WATERING_CAN_2 = 5334;
    
    public static final int WATERING_CAN_3 = 5335;
    
    public static final int WATERING_CAN_4 = 5336;
    
    public static final int WATERING_CAN_5 = 5337;
    
    public static final int WATERING_CAN_6 = 5338;
    
    public static final int WATERING_CAN_7 = 5339;
    
    public static final int WATERING_CAN_8 = 5340;
    
    public static final int BAGGED_DEAD_TREE = 8417;
    
    public static final int BAGGED_NICE_TREE = 8419;
    
    public static final int BAGGED_OAK_TREE = 8421;
    
    public static final int BAGGED_WILLOW_TREE = 8423;
    
    public static final int BAGGED_MAPLE_TREE = 8425;
    
    public static final int BAGGED_YEW_TREE = 8427;
    
    public static final int BAGGED_MAGIC_TREE = 8429;
    
    public static final int TREE = 8173;
    
    public static final int NICE_TREE = 8174;
    
    public static final int OAK_TREE = 8175;
    
    public static final int WILLOW_TREE = 8176;
    
    public static final int MAPLE_TREE = 8177;
    
    public static final int YEW_TREE = 8178;
    
    public static final int MAGIC_TREE = 8179;
    
    public static final int PLANT = 8180;
    
    public static final int BAGGED_PLANT_1 = 8431;
    
    public static final int BAGGED_PLANT_2 = 8433;
    
    public static final int BAGGED_PLANT_3 = 8435;
    
    public static final int PLANK = 960;

    public static final int BRONZE_NAILS = 4819;

    public static final int IRON_NAILS = 4820;

    public static final int STEEL_NAILS = 1539;

    public static final int BLACK_NAILS = 4821;

    public static final int MITH_NAILS = 4822;

    public static final int ADAM_NAILS = 4823;

    public static final int RUNE_NAILS = 4824;

    public static final int CONSTRUCTORS_HAT = 21446;

    public static final int CONSTRUCTORS_CHEST = 21447;

    public static final int CONSTRUCTORS_LEGS = 21448;

    public static final int CONSTRUCTORS_GLOVES = 21449;

    public static final int CONSTRUCTORS_BOOTS = 21450;
    
    public static final int HAMMER = 2347;
    
    public static final int OAK_PLANK = 8778;
    
    public static final int TEAK_PLANK = 8780;
    
    public static final int MAHOGANY_PLANK = 8782;
    
    public static final int CLOTH = 8790;
    
    public static final int SOFT_CLAY = 1761;
    
    public static final int LIMESTONE_BRICK = 3420;
    
    public static final int MARBLE_BLOCK = 8786;

    public static final int GOLD_LEAF = 8784;

    public static final int WOOL = 1737;

    public static final int STEEL_BAR = 2353;

    public static final int CIDER = 5763;

    public static final int ROPE = 954;

    public static final int MOLTEN_GLASS = 1775;

    public static final int LIT_CANDLE = 33;

    public static final int SKULLS = 964;

    public static final int TINDERBOX = 590;

    public static final int CRYSTAL_SAW = 9625;

}
