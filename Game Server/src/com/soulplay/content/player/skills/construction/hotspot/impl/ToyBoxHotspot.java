package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyToyBox;
import com.soulplay.content.player.skills.construction.buildable.impl.OakToyBox;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakToyBox;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ToyBoxHotspot extends Hotspot {

    public ToyBoxHotspot() {
        super(HotspotType.TOY_BOX);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakToyBox(), new TeakToyBox(), new MahoganyToyBox()));
    }
}
