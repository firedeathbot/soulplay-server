package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Hangman;
import com.soulplay.content.player.skills.construction.buildable.impl.Jester;
import com.soulplay.content.player.skills.construction.buildable.impl.TreasureHunt;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class GameHotspot extends Hotspot {

    public GameHotspot() {
        super(HotspotType.GAME);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Jester(), new TreasureHunt(), new Hangman()));
    }

}
