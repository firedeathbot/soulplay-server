package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class GamesRoom extends Room {

	private static final Palette palette = new Palette(7, -2);

	private static final boolean[] doors = {false, true, true, true};

	public GamesRoom(int x, int y, int z) {
		super(RoomType.GAMES, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.PRIZE_CHEST, new RoomObject(15343, Location.create(3, 7), 0, 10));
		hotspots.put(HotspotType.ELEMENTAL_BALANCE, new RoomObject(15345, Location.create(5, 4), 0, 10));
		hotspots.put(HotspotType.GAME, new RoomObject(15342, Location.create(6, 0), 1, 10));
		hotspots.put(HotspotType.STONE, new RoomObject(15344, Location.create(2, 4), 0, 10));
		hotspots.put(HotspotType.RANGING_GAME, new RoomObject(15346, Location.create(1, 0), 2, 10));
	}

}
