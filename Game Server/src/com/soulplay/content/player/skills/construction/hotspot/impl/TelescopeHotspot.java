package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyTelescope;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakTelescope;
import com.soulplay.content.player.skills.construction.buildable.impl.WoodenTelescope;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class TelescopeHotspot extends Hotspot {

    public TelescopeHotspot() {
        super(HotspotType.TELESCOPE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenTelescope(), new TeakTelescope(), new MahoganyTelescope()));
    }

}
