package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class HeadTrophyHotspot extends Hotspot {

    public HeadTrophyHotspot() {
        super(HotspotType.HEAD_TROPHY);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new CrawlingHand(), new CockatriceHead(), new BasiliskHead(), new KuraskHead(), new AbyssalDemonHead(), new KingBlackDragonHead(), new KalphiteQueenHead()));
    }

}
