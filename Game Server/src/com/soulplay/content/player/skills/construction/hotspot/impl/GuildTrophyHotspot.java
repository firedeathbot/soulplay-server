package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.AmuletOfGlory;
import com.soulplay.content.player.skills.construction.buildable.impl.AntiDragonShield;
import com.soulplay.content.player.skills.construction.buildable.impl.CapeOfLegends;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class GuildTrophyHotspot extends Hotspot {

    public GuildTrophyHotspot() {
        super(HotspotType.GUILD_TROPHY);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new AntiDragonShield(), new AmuletOfGlory(), new CapeOfLegends()));
    }

}
