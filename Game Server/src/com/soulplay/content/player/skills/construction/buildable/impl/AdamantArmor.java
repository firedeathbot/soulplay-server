package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class AdamantArmor extends Buildable {
    @Override
    public String getName() {
        return "Adamant armour";
    }

    @Override
    public int getItemId() {
        return 8271;
    }

    @Override
    public int getObjectId() {
        return 13492;
    }

    @Override
    public int getLevelRequired() {
        return 28;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 2), new Item(1161), new Item(1123), new Item(1091));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 150;
    }
}
