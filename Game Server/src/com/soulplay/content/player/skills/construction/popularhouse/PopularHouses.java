package com.soulplay.content.player.skills.construction.popularhouse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class PopularHouses {
	
	public static final List<HouseListing> list = new ArrayList<>();
	
	private static final int SIZE = 10;
	
	public static final int CHANGE_TITLE = 256469;

	public static final int REFRESH = 256467;
	
	public static final int BUTTON1 = 257258;
	
	public static void openInterface(Player c) {
		if (c.interfaceIdOpenMainScreen != 66000) {
			c.getPacketSender().showInterface(66000);
			populateInterface(c);
		}
	}

	public static void addToEntry(Player c) {
		if (!c.playerIsInHouse || !c.getPOH().isOwner() || !c.getHouseListingRefreshTick().checkThenStart(60))
			return;
		boolean moreGuests = false;
		
		//Check for existing listing, edit if exists
		for (int i = 0, len = list.size(); i < len; i++) {
			HouseListing l = list.get(i);
			
			if (l.getMid() == c.mySQLIndex) {
				if (l.getGuestSize() != c.getPOH().getGuestSize()) {
					l.setGuestSize(c.getPOH().getGuestSize());
					l.setListingTitle(c.getPOH().houseListingTitle);
					sortList();
				}
				return;
			}
			
			if (!moreGuests && l.getGuestSize() < c.getPOH().getGuestSize()) { // i have more guests so add to entry
				moreGuests = true;
			}
		}
		
		if (!moreGuests) {
			if (c.getPOH().houseListingTitle == null)
				c.getPOH().houseListingTitle = c.getNameSmartUp();
			list.add(new HouseListing(c.mySQLIndex, c.getPOH().getGuestSize(), c.getPOH().houseListingTitle));
			sortList();
			if (list.size() > SIZE) {
				list.remove(SIZE);
			}
		}
		
	}
	
	public static void clickedButton(Player c, int buttonId) {
		if (!c.getSpamTick().checkThenStart(1)) {
			c.sendMessage("Slow down please.");
			return;
		}

		if (c.interfaceIdOpenMainScreen != 66000)
			return;
		
		switch (buttonId) {
		case PopularHouses.CHANGE_TITLE:
			if (!c.playerIsInHouse && c.isOwner()) {
				c.sendMessage("You need to be inside your house to change your title.");
				return;
			}
			changeTitle(c);
			break;
			
		case PopularHouses.REFRESH:
			populateInterface(c);
			break;
			default:
				if (c.playerIsInHouse) {
					c.sendMessage("You need to leave your current house before entering another.");
					return;
				}

				if (!c.inEdgeConstruction() && !RSConstants.inVarrockConstruction(c.getX(), c.getY())) {
					c.sendMessage("You need to be close to the house portal to enter a house!");
					return;
				}

				final int index = (buttonId - BUTTON1)/5;
				getHouseListing(c, index);
				break;
		}
	}
	
	public static void getHouseListing(Player c, int index) {
		
		if (index >= list.size()) {
			c.getSpamTick().startTimer(2);
			return;
		}
		HouseListing l = list.get(index);
		
		Player owner = PlayerHandler.getPlayerByMID(l.getMid());
		
		if (owner == null || !owner.isActive) {
			c.sendMessage("Sorry that house is no longer available.");
			return;
		}
		
		c.getPOH().enter(owner);
	}
	
	public static void populateInterface(Player c) {
		int listSize = list.size();
		for (int i = 0; i < SIZE; i++) {
			
			final int amountString = HouseListingInterface.values[i].getAmountId();
			final int textStringId = HouseListingInterface.values[i].getTextId();
			
			if (i < listSize) {
				HouseListing l = list.get(i);

				c.getPacketSender().sendString(Integer.toString(l.getGuestSize() + 1), amountString);
				c.getPacketSender().sendString(l.getListingTitle(), textStringId);
			} else {
				c.getPacketSender().sendString("", amountString);
				c.getPacketSender().sendString("", textStringId);
			}
			
		}
	}
	
	public static void changeTitle(Player c) {
		c.getDialogueBuilder().sendEnterText(()-> {
			String title = c.getDialogueBuilder().getEnterText();
			if (title != null)
				c.getPOH().houseListingTitle = title;
		}).setClose(false).executeIfNotActive();
	}
	
	public static void leaveOwnHouse(Player c) {
		if (c.getHomeOwnerId() == c.mySQLIndex) {
			for (int i = 0, len = list.size(); i < len; i++) {
				HouseListing l = list.get(i);
				if (l.getMid() == c.mySQLIndex) {
					list.remove(l);
					return;
				}
			}
		}
	}
	
	public static void sortList() {
		Collections.sort(list, new Comparator<HouseListing>() {
		    @Override
		    public int compare(HouseListing object1, HouseListing object2) {
		        return object1.getGuestSize() < object2.getGuestSize() ? 1 : -1;
		    }
		});
	}
	
}
