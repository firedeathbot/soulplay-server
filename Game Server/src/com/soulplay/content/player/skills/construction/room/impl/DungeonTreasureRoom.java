package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class DungeonTreasureRoom extends Room {

	private static final Palette palette = new Palette(9, -2);

	private static final boolean[] doors = {false, false, true, false};

	public DungeonTreasureRoom(int x, int y, int z) {
		super(RoomType.DUNGEON_TREASURE_ROOM, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(1, 2), 0, 4));
		hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(6, 2), 2, 4));

		hotspots.put(HotspotType.DUNGEON_DECORATION_2, new RoomObject(15259, Location.create(3, 6), 1, 4));
		hotspots.put(HotspotType.DUNGEON_DECORATION_2, new RoomObject(15259, Location.create(4, 6), 1, 4));

		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(1, 5), 0, 4));
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(6, 5), 2, 4));
		hotspots.put(HotspotType.MONSTER, new RoomObject(15257, Location.create(3, 3), 0, 10));
		hotspots.put(HotspotType.DUNGEON_CHEST, new RoomObject(15256, Location.create(2, 6), 0, 10));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15327, Location.create(3, 1), 3, 0));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15327, Location.create(4, 1), 3, 0));
	}

}
