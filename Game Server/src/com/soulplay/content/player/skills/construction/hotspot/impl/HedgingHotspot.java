package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class HedgingHotspot extends Hotspot {

    public HedgingHotspot() {
        super(HotspotType.HEDGING);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new ThornyHedge(), new NiceHedge(), new SmallBoxHedge(), new TopiaryHedge(), new FancyHedge(), new TallFancyHedge(), new TallBoxHedge()));
    }
}
