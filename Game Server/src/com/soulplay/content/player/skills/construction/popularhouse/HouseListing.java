package com.soulplay.content.player.skills.construction.popularhouse;

public class HouseListing {

	private final int mid;
	private int guestSize;
	private String listingTitle;
	
	public HouseListing(int playerMID, int guestsSize, String title) {
		this.mid = playerMID;
		this.guestSize = guestsSize;
		this.listingTitle = title;
	}

	public int getMid() {
		return mid;
	}

	public int getGuestSize() {
		return guestSize;
	}

	public void setGuestSize(int guestSize) {
		this.guestSize = guestSize;
	}

	public String getListingTitle() {
		return listingTitle;
	}

	public void setListingTitle(String listingTitle) {
		this.listingTitle = listingTitle;
	}
}
