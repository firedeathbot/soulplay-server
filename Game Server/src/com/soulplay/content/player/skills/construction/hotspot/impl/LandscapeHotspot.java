package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LandscapeHotspot extends Hotspot {

    public LandscapeHotspot() {
        super(HotspotType.LANDSCAPE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Lumbridge(), new TheDesert(), new Morytania(), new Karamja(), new Isafdar()));
    }

}
