package com.soulplay.content.player.skills.construction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.buildable.BuildType;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomFactory;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.content.player.skills.construction.room.impl.EmptyDung;
import com.soulplay.content.player.skills.construction.room.impl.EmptyRoom;
import com.soulplay.content.player.skills.construction.room.impl.GardenRoom;
import com.soulplay.content.player.skills.construction.room.impl.ParlourRoom;
import com.soulplay.content.player.skills.construction.specialobjects.WhiteberryEnum;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Poh {

    public static final int MAXIMUM_ROOMS = 13;

    public static final int MAXIMUM_FLOORS = 4;

    public static final int MAXIMUM_GRID_SIZE = 8;
    
    public static final int DUNGEON_FLOOR = 0;
    public static final int GROUND_FLOOR = 1;
    public static final int SECOND_FLOOR = 2;

    private final Butler butler = new Butler();

    private final Room[][][] rooms = new Room[MAXIMUM_FLOORS][MAXIMUM_ROOMS][MAXIMUM_ROOMS];
    
    private final int[] roomCountArray = new int[MAXIMUM_FLOORS];

    private Room[][][] copyRooms;

    private final Map<RoomObject, NPC> npcMap = new LinkedHashMap<>();
    private CycleEventContainer npcTask;

    private final Set<Player> guests = new HashSet<>();
    private int guestSize = 0;

    private HouseStyle houseStyle = HouseStyle.BASIC_WOOD;

    private Player owner;

    private int exitPortalCount;

    private boolean buildMode, lastBuildMode = true;
    private boolean renderDoorsOpen; // TODO not added
    private boolean teleportInside; // TODO not added

    private boolean guestsCanJoin;
    
    public String houseListingTitle = null;

    public Optional<Hotspot> currentHotspot = Optional.empty();

    private int gridSize = 4;
    
    private final ArrayList<Item> treasureChestItems = new ArrayList<Item>();
    private final ArrayList<Item> fancyDressBox = new ArrayList<Item>();
    private final ArrayList<Item> armorCase = new ArrayList<Item>();
    private final ArrayList<Item> magicWardrobe = new ArrayList<Item>();
    private final ArrayList<Item> capeRack = new ArrayList<Item>();
    private final ArrayList<Item> toyBox = new ArrayList<Item>();
    
    private int whiteberryBushCount = 0;

    public Poh(Player owner) {
        this.owner = owner;
    }
    
    public boolean switchedModes() {
    	return buildMode != lastBuildMode;
    }

    public void buyHouse() {
        if (owner.playerHasHouse()) {
            owner.sendMessage("You already own a house? BUGGG, or didn't reset properly");
            return;
        }

        for (int z = 0; z < rooms.length; z++) {
            for (int j = 0; j < rooms[z].length; j++) {
                Arrays.fill(rooms[z][j], null);
            }
        }

        for (int x = 4; x < 8; x++) {
            for (int y = 5; y < 9; y++) {
                Room emptyRoom = new EmptyRoom(x, y, GROUND_FLOOR);
                emptyRoom.setLocation(Location.create(x, y, GROUND_FLOOR));
                placeRoom(emptyRoom);
            }
        }

        GardenRoom gardenRoom = new GardenRoom(6, 6, GROUND_FLOOR);
        gardenRoom.setLocation(Location.create(6, 6, GROUND_FLOOR));

        // add exit portal
        gardenRoom.addBuiltObjectToMap(HotspotType.GARDEN_CENTREPIECE, new RoomObject(15361, 13405, Location.create(51, 51, GROUND_FLOOR), 1, 10));
        incrementExitPortalCount();

        ParlourRoom parlourRoom = new ParlourRoom(6, 7, GROUND_FLOOR);
        parlourRoom.setLocation(Location.create(6, 7, GROUND_FLOOR));

        placeRoom(gardenRoom);
        placeRoom(parlourRoom);

        gridSize = 4;

        gardenRoom.getPalette().rotate(-1);
        if (canPlaceRoom(gardenRoom, 0, true)) {
            applyRoomRotation(gardenRoom);
        }

        parlourRoom.getPalette().rotate(-1);
        if (canPlaceRoom(parlourRoom, 0, true)) {
            applyRoomRotation(parlourRoom);
        }

        fillGrid();

        owner.setPlayerHasHouse(true);
    }

    public void reset() {
        PlayerSaveSql.clearPOH(owner.mySQLIndex);
        for (int z = 0; z < rooms.length; z++) {
            for (int j = 0; j < rooms[z].length; j++) {
                Arrays.fill(rooms[z][j], null);
            }
        }

        for (int x = 4; x < 8; x++) {
            for (int y = 5; y < 9; y++) {
                Room emptyRoom = new EmptyRoom(x, y, GROUND_FLOOR);
                emptyRoom.setLocation(Location.create(x, y, GROUND_FLOOR));
                placeRoom(emptyRoom);
            }
        }

        GardenRoom gardenRoom = new GardenRoom(6, 6, GROUND_FLOOR);
        gardenRoom.setLocation(Location.create(6, 6, GROUND_FLOOR));

        // add exit portal
        gardenRoom.addBuiltObjectToMap(HotspotType.GARDEN_CENTREPIECE, new RoomObject(15361, 13405, Location.create(51, 51, GROUND_FLOOR), 1, 10));
        exitPortalCount = 1;

        ParlourRoom parlourRoom = new ParlourRoom(6, 7, GROUND_FLOOR);
        parlourRoom.setLocation(Location.create(6, 7, GROUND_FLOOR));

        placeRoom(gardenRoom);
        placeRoom(parlourRoom);

        gridSize = 4;

        gardenRoom.getPalette().rotate(-1);
        if (canPlaceRoom(gardenRoom, 0, true))
        	applyRoomRotation(gardenRoom);
        parlourRoom.getPalette().rotate(-1);
        if (canPlaceRoom(parlourRoom, 0, true))
        	applyRoomRotation(parlourRoom);

        fillGrid();

        guestsCanJoin = false;
        buildMode = false;
        guests.clear();
        guestSize = 0;
        currentHotspot = Optional.empty();
        owner.setPlayerHasHouse(true);
    }
    
    private static final String SAVE_QUERY_ROOMS = "SELECT * FROM poh_room WHERE player_id = ?";
    private static final String SAVE_QUERY_DETAILS = "SELECT * FROM poh_detail WHERE player_id = ?";

    public boolean loadHouse(Connection connection) {

    	boolean error = false;
    	
    	for (int z = 0; z < rooms.length; z++) {
    		for (int j = 0; j < rooms[z].length; j++) {
    			Arrays.fill(rooms[z][j], null);
    		}
    	}

    	try (PreparedStatement rSta = connection.prepareStatement(SAVE_QUERY_ROOMS);
    			PreparedStatement dSta = connection.prepareStatement(SAVE_QUERY_DETAILS);
    			) {

    		boolean hasHouse = false;
    		
    		dSta.setInt(1, owner.mySQLIndex);

    		try (ResultSet detailSet = dSta.executeQuery();) {

    			while(detailSet.next()) {
    				try {
    					houseStyle = HouseStyle.valueOf(detailSet.getString("house_style").toUpperCase());
    				} catch (Exception ex) {
    					ex.printStackTrace();
    				}
    				
    				guestsCanJoin = detailSet.getBoolean("locked");

    				try {
    					gridSize = detailSet.getInt("grid_size");
    				} catch (Exception ex) {
    					ex.printStackTrace();
    				}

    				try {
    					String butler = detailSet.getString("butler_type").toUpperCase();

    					if (!butler.isEmpty() && !butler.equalsIgnoreCase("NONE")) {
    						getButler().setType(ButlerType.valueOf(butler));
    					}
    				} catch (Exception ex) {
    					ex.printStackTrace();
    				}
    				
    				Poh.extractStorageString(owner, detailSet.getString("treasure_chest"), owner.getPOH().getTreasureChestItems());
    				Poh.extractStorageString(owner, detailSet.getString("fancy_dress_box"), owner.getPOH().getFancyDressBox());
    				Poh.extractStorageString(owner, detailSet.getString("armor_case"), owner.getPOH().getArmorCase());
    				Poh.extractStorageString(owner, detailSet.getString("magic_wardrobe"), owner.getPOH().getMagicWardrobe());
    				Poh.extractStorageString(owner, detailSet.getString("cape_rack"), owner.getPOH().getCapeRack());
    				Poh.extractStorageString(owner, detailSet.getString("toy_box"), owner.getPOH().getToyBox());
    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
				error = true;
    		}

    		rSta.setInt(1, owner.mySQLIndex);

    		try (ResultSet resultSet = rSta.executeQuery();) {
    			while (resultSet.next()) {
    				hasHouse = true;
    				String type = resultSet.getString("type");
    				int x = resultSet.getInt("x");
    				int y = resultSet.getInt("y");
    				int z = resultSet.getInt("z");
    				int rotation = resultSet.getInt("rotation");

    				// recreate the rooms
    				Room room = RoomFactory.createRoom(RoomType.valueOf(type), x, y, z);
    				

    				// to load properly, we need to set unloaded rotation
    				room.getPalette().rotate(-1);
    				if (!canPlaceRoom(room, rotation, true)) {
    					System.out.println("unable to load the room! playerName:"+owner.getNameSmartUp());
    				} else {
    					applyRoomRotation(room);
    					placeRoom(room);
    				}


    				String[] array = resultSet.getString("objects").split(":");
    				long[] l = new long[array.length];
    				if (array.length > 0 && !array[0].isEmpty()) {
    					try {
    						for (int i = 0; i < array.length; i++) {
    							if (!array[i].isEmpty())
    								l[i] = Long.parseLong(array[i]);
    						}
    					} catch (Exception ex) {
    						ex.printStackTrace();
    					}
    				}
    				for (long value : l) {
    					if (value == 0) continue;
    					int objRotation = (int)(value & 3);
    					int objX = (int)(value >> 2 & 0x3ff);
    					int objY = (int)(value >> 12 & 0x3ff);
    					int oId = (int)(value >> 22 & 0x7fff);
    					int oHotId = (int)(value >> 37 & 0x7fff);
    					int oType = (int)(value >> 52 & 0x7fff);
    					
    					HotspotType hotspotType = HotspotType.lookup(oHotId);

    					if (oId == 13405) {
    						incrementExitPortalCount();
    					}

    					//increase whiteberry count
    			        if (room.getType() == RoomType.GARDEN) {
    			        	//cheaphax to reducing whiteberry bush counts
    						if (hotspotType == HotspotType.BIG_PLANT_1 && oId >= WhiteberryEnum.BUSH1.getObject() && oId <= WhiteberryEnum.BUSH5.getObject()) {
    							owner.getPOH().setWhiteberryBushCount(owner.getPOH().getWhiteberryBushCount()-1);
    						}
    			        }
    			        
    					//    		System.out.println(String.format("oFace:%d roomZ:%d roomX:%d roomY:%d objX:%d objY:%d oID:%d oHotId:%d", objRotation, roomZ, roomX, roomY, objX, objY, oId, oHotId));
    					room.addBuiltObjectToMap(hotspotType, new RoomObject(oHotId, oId, Location.create(objX, objY, room.getZ()), objRotation, oType));
    				}

    				//ConstructionUtils.loadObjectsFromLong(room, resultSet.getString("objects"));
    				ConstructionUtils.loadItemsFromLong(room, resultSet.getString("items"));
    				room.clipBuiltObjects(owner);

    			}
    		} catch (SQLException e) {
    			e.printStackTrace();
				error = true;
    		}
    		
    		owner.setPlayerHasHouse(hasHouse);

    	} catch (SQLException e) {
    		e.printStackTrace();
			error = true;
    	}

    	fillGrid();
    	owner.getPohClipMap().surroundAreaWithSolidClipMask(20, 20, 63, 0, 1, 2);
    	return !error;
    }

    public void fillGrid() {
        for (int x = 0; x < rooms[GROUND_FLOOR].length; x++) {
            for (int y = 0; y < rooms[GROUND_FLOOR][x].length; y++) {
                if (x < 2 || x > 10 || y < 2 || y > 10) {
                    continue;
                }

                if (rooms[GROUND_FLOOR][x][y] == null) {
                    placeRoom(new EmptyRoom(x, y, GROUND_FLOOR), false);
                }

                if (rooms[DUNGEON_FLOOR][x][y] == null) {
                    placeRoom(new EmptyDung(x, y, DUNGEON_FLOOR), false);
                }
            }
        }
    }

    public void enter(final Player owner, boolean buildingMode, boolean teleportPlayer) {

        final Player guest = this.owner;

        // check to see if the player is trying to enter is the same player
        final boolean samePlayer = guest.equals(owner);

        // a player is trying to enter building mode in another players house.
        if (guest.getPOH().isGuest() && buildingMode) {
            guest.sendMessage("You cannot use this in another players house.");
            return;
        }
        
        // a player trying to enter a locked house
        if (guest.getPOH().isGuest() && !owner.getPOH().isGuestsCanJoin()) {
        	guest.sendMessage("The home owner has locked their portal.");
        	if (!guest.isMod() && !guest.isOwner() && !guest.isAdmin())
        		return;
        }

        // player is trying to enter the same building mode
        if (samePlayer && isBuildMode() == buildingMode && !teleportPlayer) {
            return;
        }

        // player is trying to exit building mode while they are not in their house
        if (!guest.playerIsInHouse && !buildingMode && !teleportPlayer) {
            return;
        }

        // a player is trying to enter building mode when they are not in their house
        if (samePlayer && buildingMode && !guest.playerIsInHouse && !teleportPlayer) {
            guest.sendMessage("You need to be in your house to enter building mode.");
            return;
        }

        if (samePlayer && buildingMode && !guests.isEmpty()) {
            guest.sendMessage("You cannot enter building mode when guests are present.");
            return;
        }

        // trying to enter a house as a guest and the owner is not home
        if (!owner.playerIsInHouse && !samePlayer) {
            guest.sendMessage(String.format("%s is not home.", owner.getNameSmartUp()));
            guest.getPA().closeAllWindows();
            return;
        }

        // owner is trying to enter his house if he hasn't purchased a house yet
        if (!owner.playerHasHouse() && samePlayer) {
            owner.sendMessage("You have not purchased a house yet!");
            if (owner.getDialogueBuilder().isActive()) {
            	owner.getPA().closeAllWindows();
            }

            return;
        }

        if (!samePlayer && owner.getPOH().isBuildMode()) {
            guest.sendMessage(String.format("%s is currently in building mode.", owner.getNameSmartUp()));
            guest.getPA().closeAllWindows();
            return;
        }

        final Poh ownerPoh = owner.getPOH();

        // if the player trying to enter is not the same player that means the player is trying to enter another players house
        // so they become a guest.
        if (!samePlayer) {
            ownerPoh.getGuests().add(guest);
            ownerPoh.incGuestSize();
        }

        guest.setDynamicRegionClip(owner.getPohClipMap());
        guest.setHomeOwnerId(owner.mySQLIndex);

        if (guest.summoned != null) {
            guest.summoned.setHomeOwnerId(owner.mySQLIndex);
            guest.summoned.setDynamicRegionClip(owner.getPohClipMap());
        }

        setBuildMode(!isGuest() && buildingMode);

        guest.getPacketSender().sendConfig(770, isBuildMode() ? 0 : 1);

        Construction.openWaitInterface(guest);

        guest.playerIsInHouse = true;
        
        guest.performingAction = true;
        
        final Location moveToFinal = teleportPlayer ? Location.create(51, 53, GROUND_FLOOR) : Location.create(owner.getX(), owner.getY(), owner.getZ());

        if (buildingMode && !isGuest()) {
            guest.sendMessage("Building mode is now " + (buildingMode ? "on" : "off" + "."));
        }
        
        guest.getPA().movePlayer(moveToFinal.getX(), moveToFinal.getY(), GROUND_FLOOR);
        guest.attr().put("entering_poh", true);

        CycleEventHandler.getSingleton().addEvent(guest, new CycleEvent() {

            private int tick = 1;
            private int heightCheck = 0;

            @Override
            public void execute(CycleEventContainer container) {
            	if (!guest.isLoadedRegion()) {
                    return;
                }
            	
            	if (tick == 1) {
                    guest.getPacketSender().sendConstructMapRegionPacket(ownerPoh);
            	} else if (tick >= 2) {

            	    if (samePlayer && buildingMode) {
            	        removeNpcs();
                    }

                    if (owner.playerIsInHouse) {
                        guest.getPOH().showAllObjects(GROUND_FLOOR, owner);
                        guest.getPA().movePlayer(moveToFinal.getX(), moveToFinal.getY(), GROUND_FLOOR);
                        guest.getPacketSender().sendString(String.format("Number of rooms: %d", owner.getPOH().getRoomCount()), 46005);
                    } else {
                        guest.getPA().movePlayer(3079, 3491, 0);
                    }
                    container.stop();

                }

                tick++;
            }

            @Override
            public void stop() {
        		lastBuildMode = buildMode;
        		guest.performingAction = false;
        		if (!guest.disconnected) {
                    Construction.closeWaitInterface(guest);
            		guest.getPacketSender().setBlackout(0);
                    guest.attr().put("entering_poh", false);
        		}
            }

        }, 1);

    }

    public void enter(Player owner, boolean buildingMode) {
        enter(owner, buildingMode, true);
    }

    public void enter(Player owner) {
        enter(owner, false, true);
    }

    private void removeNpcs() {
        setNpcTask(null);
        Iterator<NPC> itr = getNpcs().values().iterator();
        while(itr.hasNext()) {
            NPC npc = itr.next();
            npc.setDead(true);
            npc.alwaysRespawn = false;
            itr.remove();
        }
    }

    public void expelGuests() {
        for (Player p : guests) {
        	if (!p.isActive || p.attr().getOrDefault("entering_poh", false)) {
        		continue;
        	}

        	Construction.leaveHouse(p, true, false);
        }
    }

//	public void leaveHouse() {
//	    owner.getPA().movePlayer(Location.create(3079, 3491));
//	    owner.playerIsInHouse = false;
//	    owner.setHomeOwnerId(-1);
//	}

    public boolean showAllObjects(int z, Player owner) {
    	boolean drawObjects = false;
    	final Player guest = this.owner;
        final Room[][][] rooms = owner.getPOH().getRooms();
        for (int x = 0; x < rooms[z].length; x++) {
            for (int y = 0; y < rooms[z][x].length; y++) {
                Room room = rooms[z][x][y];

                if (room == null || room.getObjects().size() <= 0) {
                    continue;
                }

                if (room.displayBuiltObjects(guest, false))
                	drawObjects = true;
                
                room.displayDroppedItems(guest);
            }
        }
        return drawObjects;
    }

    public void hidePaletteObjects(Player owner, int... hideHeight) {
    	for (int z : hideHeight) {
    		for (int x = 0, sX = owner.getPOH().getRooms()[z].length; x < sX; x++) {
    			for (int y = 0, sY = owner.getPOH().getRooms()[z][x].length; y < sY; y++) {
    				Room room = getRooms()[z][x][y];

    				if (room == null) {
    					continue;
    				}
    				this.owner.getPacketSender().removeObjects(room.getX(), room.getY(), room.getZ());
    			}
    		}
    	}
    }

    public void showObjects(Client friend, int height) {
        Poh poh = friend.getPOH();

        for (int i = 0; i < poh.rooms.length; i++) {
            for (int j = 0; j < poh.rooms[i].length; j++) {
                Room room = poh.rooms[height][i][j];

                if (room == null) {
                    continue;
                }

                for (GameObject o : room.getObjects().values()) {
                    owner.getPacketSender().addObjectPacket(o);
                }
            }
        }
    }

    public Optional<Room> getCurrentRoom() {
        return getCurrentRoom(owner.getCurrentLocation());
    }
    
    public Optional<Room> getCurrentRoom(Location loc) {

        final int x = loc.getX() / 8;
        final int y = loc.getY() / 8;

        if (x >= MAXIMUM_ROOMS || y >= MAXIMUM_ROOMS) {
            return Optional.empty();
        }

        return Optional.ofNullable(getRoom(x, y, loc.getZ() % 4));
    }

    // adds an object to the room
    public void spawnObject(int buttonId) {
        if (!owner.getPOH().isBuildMode()) {
            owner.sendMessage("You are not in building mode!");
            return;
        }

        // prevent cheat clients, we know hotspot the player is interacting with if any
        if (!owner.getPOH().currentHotspot.isPresent()) {
            return;
        }

        Optional<Room> roomResult = getCurrentRoom();

        if (!roomResult.isPresent()) {
            return;
        }

        final Room room = roomResult.get();

        final Hotspot hotspot = owner.getPOH().currentHotspot.get();

        final List<Buildable> buildables = hotspot.getBuildables();

        // 39600 is the first item on the interface, there can be 8 items so it can go up to 39607
        int index = buttonId - 39600;

        if (index < 0 || index >= buildables.size()) {
            return;
        }

        final Buildable buildable = buildables.get(index);

        if ((owner.getPlayerLevel()[Skills.CONSTRUCTION] + (owner.getItems().playerHasItem(ConstructionItems.CRYSTAL_SAW) ? 5 : 0)) < buildable.getLevelRequired()) {
            owner.sendMessage(String.format("You need at least level %d construction to build this.", buildable.getLevelRequired()));
            return;
        }

        // required items
        if (!buildable.hasRequiredItems(owner, true)) {
            return;
        }

        if (buildable.getRequiredNails() > 0 && !ConstructionUtils.playerHasNails(owner, buildable.getRequiredNails())) {
            owner.sendMessage(String.format("You need at least %d nails of any kind.", buildable.getRequiredNails()));
            return;
        }

        if (buildable.getRequiredNails() > 0) {
            owner.getPA().closeAllWindows();
            owner.startAction(new CycleEvent() {

                final int nailCost = buildable.getRequiredNails();
                int nailsUsed = 0;
                boolean buildObject = false;

                @Override
                public void execute(CycleEventContainer container) {
                    owner.startAnimation(ConstructionAnimations.BUILD_MID_ANIM);

                    final int remaining = nailCost - nailsUsed;

                    if (remaining == 0) {
                        buildObject = true;
                        container.stop();
                        return;
                    }

                    if (owner.getItems().playerHasItem(ConstructionItems.RUNE_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 5; // 5% chance
                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            owner.sendMessage("You use a nail.");
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.RUNE_NAILS, 1);
                    } else if (owner.getItems().playerHasItem(ConstructionItems.ADAM_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 15; // 15% chance

                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            owner.sendMessage("You use a nail.");
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.ADAM_NAILS, 1);
                    } else if (owner.getItems().playerHasItem(ConstructionItems.MITH_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 25; // 25% chance

                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            owner.sendMessage("You use a nail.");
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.MITH_NAILS, 1);
                    } else if (owner.getItems().playerHasItem(ConstructionItems.BLACK_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 35; // 35% chance

                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            owner.sendMessage("You use a nail.");
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.BLACK_NAILS, 1);
                    } else if (owner.getItems().playerHasItem(ConstructionItems.STEEL_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 45; // 45% chance

                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            owner.sendMessage("You use a nail.");
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.STEEL_NAILS, 1);
                    } else if (owner.getItems().playerHasItem(ConstructionItems.IRON_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 65; // 65% chance

                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.IRON_NAILS, 1);
                    } else if (owner.getItems().playerHasItem(ConstructionItems.BRONZE_NAILS, remaining)) {
                        boolean bend = Misc.random(1, 100) <= 85; // 85% chance
                        if (bend) {
                            owner.sendMessage("You accidentally bend a nail.");
                        } else {
                            owner.sendMessage("You use a nail.");
                            nailsUsed++;
                        }
                        owner.getItems().deleteItem2(ConstructionItems.BRONZE_NAILS, 1);
                    } else {
                        owner.sendMessage("You have ran out of nails.");
                        buildObject = false;
                        container.stop();
                    }

                }

                @Override
                public void stop() {
                    if (buildObject) {
                        applyBuildObject(room, hotspot, buildable);
                        buildObject = false;
                    }
                }
            }, 2);
        } else {
            if (hotspot.getType() == HotspotType.STAIR) {
                if (owner.getZ() == GROUND_FLOOR) {
                    owner.getDialogueBuilder().sendOption("Build stairs in which direction?",
                            "Up", ()-> {
                                buildRoomAbove(SECOND_FLOOR, room, hotspot, buildable);
                            }, "Down", ()-> {
                                buildRoomBelow(DUNGEON_FLOOR, room, hotspot, buildable);
                            }).execute();
                } else if (owner.getZ() == DUNGEON_FLOOR) {
                    buildRoomAbove(GROUND_FLOOR, room, hotspot, buildable);
                } else if (owner.getZ() == SECOND_FLOOR) {
                    buildRoomBelow(GROUND_FLOOR, room, hotspot, buildable);
                }
            } else {
                applyBuildObject(room, hotspot, buildable);
            }
        }

    }
    
    public void buildRoomAbove(int newZ, Room currentRoom, Hotspot hotspot, Buildable buildable) {
    	Room existsUpstairs = owner.getPOH().getRoom(currentRoom.getX(), currentRoom.getY(), newZ);
		if (existsUpstairs != null && existsUpstairs.getStairsHotspot() == null) {
			owner.getDialogueBuilder().sendStatement("You cannot connect", " stairs", "to the room above.").executeIfNotActive();
			owner.sendMessage("You cannot connect stairs to the room above.");
			return;
		}
		//TODO: rotate room into upstairs version if contains stairs hotspot
		if (currentRoom.getType() != currentRoom.getLowerRoomType()) {
			final Room upperRoom = RoomFactory.createRoom(currentRoom.getLowerRoomType(), currentRoom.getX(), currentRoom.getY(), currentRoom.getZ());
			Room.copyRoom(currentRoom, upperRoom);
			placeRoom(upperRoom);
			owner.getPacketSender().sendConstructMapRegionPacket(owner.getPOH());
			applyBuildObject(upperRoom, hotspot, buildable);
		} else {
			applyBuildObject(currentRoom, hotspot, buildable);
		}
    }
    
    public void buildRoomBelow(int newZ, Room currentRoom, Hotspot hotspot, Buildable buildable) {
    	Room existsDownstairs = owner.getPOH().getRoom(currentRoom.getX(), currentRoom.getY(), newZ);
		if (existsDownstairs != null && existsDownstairs.getType() != RoomType.EMPTY_DUNG && existsDownstairs.getStairsHotspot() == null) {
			owner.getDialogueBuilder().sendStatement("You cannot connect stairs", "to the room below.").executeIfNotActive();
			owner.sendMessage("You cannot connect stairs to the room below.");
			return;
		}
		//TODO: rotate room into downstairs version if contains stairs hotspot
		if (currentRoom.getType() != currentRoom.getUpperRoomType()) {
			final Room lowerRoom = RoomFactory.createRoom(currentRoom.getUpperRoomType(), currentRoom.getX(), currentRoom.getY(), currentRoom.getZ());
			Room.copyRoom(currentRoom, lowerRoom);
			placeRoom(lowerRoom);
			owner.getPacketSender().sendConstructMapRegionPacket(owner.getPOH());
			applyBuildObject(lowerRoom, hotspot, buildable);
		} else {
			applyBuildObject(currentRoom, hotspot, buildable);
		}
    }
    
    private void applyBuildObject(Room room, Hotspot hotspot, Buildable buildable) {

        // checks if the player already built an object that is going to collide with the one they are currently trying to build
        for (HotspotType colliderType : hotspot.getHotspotColliders()) {
            Collection<RoomObject> colliders = room.getObjects().get(colliderType);
            if (colliders == null) {
                continue;
            }

            if (!colliders.isEmpty()) {
                owner.sendMessage(String.format("%s collides with %s", hotspot.getType(), colliderType));
                return;
            }
        }

        // close interface
        owner.getPA().closeAllWindows();
        
    	if (spawnObject(room, hotspot, buildable)) {

            if (buildable.getAnimation() != 0) {
                owner.startAnimation(buildable.getAnimation());
            }

            buildable.onDeleteMaterials(owner);

            owner.getPA().addSkillXP((int)((double)buildable.getExperience() * ConstructionUtils.calculateConstructionXpModifier(owner)), Skills.CONSTRUCTION);

            buildable.onAchievement(owner);
        }

    }
    
    public boolean spawnObject(Room room, Hotspot hotspot, Buildable buildable) {
    	
    	boolean foundHotspotId = false;
    	boolean drawOnSameFloor = true;
    	
    	int firstHotspotId = 0;

    	boolean builtObject = false;
    	boolean flag = false;
    	
    	for (RoomObject realObject : room.getStaticRoomObjects().get(hotspot.getType())) {
    		int hotspotObjId = realObject.getHotspotId();
    		int objectType = realObject.getType();
    		
    		int rugOffset = 0;
    		if (!foundHotspotId) {
    			firstHotspotId = hotspotObjId;
    		} else {
    			if (firstHotspotId > hotspotObjId) {
    				rugOffset = firstHotspotId - hotspotObjId;
    			}
    		}
    		foundHotspotId = true;

    		int objectId = -1;

    		buildable.onBuild(owner, room);

    		if (buildable.getObjectMap().isEmpty()) {
    		    objectId = buildable.getObjectId(room.getType() == room.getUpperRoomType());
            } else {
    		    ObjectWrapper wrapper = buildable.getObjectMap().get(hotspotObjId);

    		    if (wrapper == null) {
    		        objectId = buildable.getObjectId(room.getType() == room.getUpperRoomType());
                } else {
                    objectId = wrapper.getId();
                    objectType = wrapper.getType();
                }

            }

    		if (hotspot.getType() == HotspotType.RUG) {
    			if (rugOffset != 0) {
    				objectId += rugOffset;
    			}
    		}

    		RoomObject object;

    		if (buildable.getBuildType() == BuildType.RECURSIVE) {
                object = new RoomObject(hotspotObjId, objectId, realObject.getLocation().copyNew(), realObject.getOrientation(), objectType);
            } else {
    		    if (!flag) {
                    object = new RoomObject(hotspotObjId, objectId, realObject.getLocation().copyNew(), realObject.getOrientation(), objectType);
                    flag = true;
                } else {
                    object = new RoomObject(-hotspotObjId, -1, realObject.getLocation().copyNew(), realObject.getOrientation(), objectType);
                }
            }

            if (object.getId() != -1) {
                owner.getPohClipMap().addObject(object); // add it to clipmap
            }

    		// add objects to the room so we can remove them
    		room.addBuiltObjectToMap(hotspot.getType(), object);
    		if (object.getId() == 13405) {
                exitPortalCount++;
            }

    		// display object on players client
    		if (object.getLocation().getZ() == owner.getZ()) {
                owner.getPacketSender().addObjectPacket(object);
                builtObject = true;
            } else {
                drawOnSameFloor = false;//Construction.drawObjectsInRoom(owner, room);
            }

    	}
    	
    	if (foundHotspotId && !drawOnSameFloor) {
    		Construction.drawObjectsInRoom(owner, room);
    		builtObject = true;
		}

        room.removeColliders(owner);
    	return builtObject;
    }

    private void removeHotspotIfPresent(Room room, int hotspotObjectId, Location location) {
        for (Iterator<Entry<Integer, RoomObject>> itr = room.getTempHotspots().entries().iterator(); itr.hasNext(); ) {

            Entry<Integer, RoomObject> entry = itr.next();

            if (entry.getKey() == room.getRotation() && entry.getValue().getHotspotId() == hotspotObjectId) {
                itr.remove();
            }

        }
    }

    public boolean canPlaceRoom(Room room, int startRotation, boolean clockwise) {
    	final boolean isRotatingRoom = room.getRotation() != -1; // if false, then player is loading/resetting/buying house
        int oldRotation = room.getRotation();
        for (int count = 0; count < 4; count++) {
            startRotation = startRotation & 3;

            room.getPalette().rotate(startRotation);

            // checking if exit is possible from current room and room we are building
            if (!isRotatingRoom || (isRotatingRoom && ConstructionUtils.exitFound(owner, room.getX(), room.getY(), room.getZ(), isRotatingRoom) && (owner.playerIsInHouse && ConstructionUtils.exitFound(owner, owner.getX()/8, owner.getY()/8, owner.getZ(), isRotatingRoom)) && room.getRotation() != oldRotation)) {
                return true;
            }
            if (clockwise)
                startRotation++;
            else {
                startRotation--;
                if (startRotation == -1)
                    startRotation = 3;
            }
        }
        room.getPalette().rotate(oldRotation);
        return false;
    }
    
    public void applyRoomRotation(Room room) {
        final Location from = room.getPalette().toLocation(houseStyle.getRegionId());
        final Location to = Location.create(room.getX() * 8, room.getY() * 8, room.getZ());
    	owner.getPohClipMap().clearRoomClipping(room);
        room.getStaticRoomObjects().clear();

        owner.getPohClipMap().copyStaticRoom(from.copyNew(), to, room, houseStyle);
        room.rotateObjectsArray(owner, room, room.getRotation());
        owner.getPohClipMap().clipAllObjectsNotHotSpots(room.getStaticRoomObjects().values());
    }
    
    public boolean canPlaceDoors(Room currentRoom) {
        if (currentRoom.getType() == RoomType.EMPTY || currentRoom.getType() == RoomType.EMPTY_DUNG)
            return true;
        for (int count = 0; count < 4; count++) {
            //count- 0=right 1=left 2=top 3=bottom
            int xOff = (count == 1 ? -1 : (count == 0 ? 1 : 0));
            int yOff = (count == 3 ? -1 : count == 2 ? 1 : 0);
            int roomX = currentRoom.getX() + xOff;
            int roomY = currentRoom.getY() + yOff;
            if (roomX < 0 || roomX > MAXIMUM_ROOMS - 1 || roomY < 0 || roomY > MAXIMUM_ROOMS - 1) continue;
            Room r = owner.getPOH().getRoom(roomX, roomY, owner.getZ());
            if (r == null || r.getType() == RoomType.EMPTY || r.getType() == RoomType.EMPTY_DUNG) { // when loading the game, gotta check for black maps
                continue;
            }
            switch (count) {
                case 0: // east
                    if (currentRoom.hasExit(Direction.EAST) && !r.hasExit(Direction.WEST))
                        return false;
                    if (r.hasExit(Direction.WEST) && !currentRoom.hasExit(Direction.EAST))
                        return false;
                    continue;
                case 1: // west
                    if (currentRoom.hasExit(Direction.WEST) && !r.hasExit(Direction.EAST))
                        return false;
                    if (r.hasExit(Direction.EAST) && !currentRoom.hasExit(Direction.WEST))
                        return false;
                    continue;
                case 2: // north
                    if (currentRoom.hasExit(Direction.NORTH) && !r.hasExit(Direction.SOUTH))
                        return false;
                    if (r.hasExit(Direction.SOUTH) && !currentRoom.hasExit(Direction.NORTH))
                        return false;
                    continue;
                case 3: // south
                    if (currentRoom.hasExit(Direction.SOUTH) && !r.hasExit(Direction.NORTH))
                        return false;
                    if (r.hasExit(Direction.NORTH) && !currentRoom.hasExit(Direction.SOUTH))
                        return false;
                    continue;
            }
        }
        return true;
    }

    public Location getRoomLocation() {
        Location loc = owner.getCurrentLocation().copyNew();

        final int x = loc.getX() / 8;
        final int y = loc.getY() / 8;

        return Location.create(x, y);
    }

    public Location getCorner() {
        Location loc = Location.create(owner.getX(), owner.getY());

        final int x = loc.getX() / 8;
        final int y = loc.getY() / 8;

        return Location.create(x * 8, y * 8);
    }

    public Location getTilePlayerIsStandingOn() {
        Location corner = getCorner();

        final int dx = owner.getX() - corner.getX();
        final int dy = owner.getY() - corner.getY();

        return Location.create(dx, dy);
    }

    public Room getRoom(int x, int y, int z) {
        return rooms[z][x][y];
    }
    
    public Room getCopiedRoom(int x, int y, int z) {
    	return copyRooms[z][x][y];
    }

    public void placeRoom(Room room) {
        placeRoom(room, room.isDungeon());
    }

    public void placeRoom(Room room, boolean dungeon) {
        if (room == null) {
            return;
        }

        final Room current = rooms[room.getZ()][room.getX()][room.getY()];

        if (room.getType() != RoomType.EMPTY && room.getType() != RoomType.EMPTY_DUNG && (current == null || current.getType() == RoomType.EMPTY || current.getType() == RoomType.EMPTY_DUNG)) {
        	roomCountArray[room.getZ()]++;
        }
        
        if (room.getType() == RoomType.EMPTY_DUNG) {
        	owner.getPohClipMap().fillRoom(room.getCorner());
        }
        if (current != null && current.getType() == RoomType.EMPTY_DUNG) {
        	owner.getPohClipMap().clearRoom(current.getCorner());
        }
        
        rooms[room.getZ()][room.getX()][room.getY()] = room;
    }

    public Room[][][] getRooms() {
        return rooms;
    }

    public boolean isBuildMode() {
        return buildMode;
    }

    public void setBuildMode(boolean buildMode) {
        this.buildMode = buildMode;
        owner.getPacketSender().sendConfig(8100, buildMode ? 1 : 2);
    }

    public Set<Player> getGuests() {
        return guests;
    }

    public boolean isGuestsCanJoin() {
        return guestsCanJoin;
    }

    public void setGuestsCanJoin(boolean guestsCanJoin) {
        this.guestsCanJoin = guestsCanJoin;
    }

    public Player getOwner() {
        return owner;
    }

    public Butler getButler() {
        return butler;
    }

    public void setHouseStyle(HouseStyle houseStyle) {
    	if (this.houseStyle != houseStyle)
    		ConstructionUtils.changeWindowsInAllrooms(owner, houseStyle);
        this.houseStyle = houseStyle;
    }

    public HouseStyle getHouseStyle() {
        return houseStyle;
    }

    public boolean isGuest() {
        return owner.getHomeOwnerId() != owner.mySQLIndex && owner.getHomeOwnerId() != -1;
    }
    
    public boolean isOwner() {
    	return owner.getHomeOwnerId() == owner.mySQLIndex;
    }

    public int getRoomCount() {
        return roomCountArray[0] + roomCountArray[1] + roomCountArray[2]/*roomCount*/;
    }
    
    public int getRoomCount(int z) {
    	if (z < 0 || z >= MAXIMUM_FLOORS) {
    		System.out.println("WRONG HEIGHT CHECK!!!");
    		return 0;
    	}
    	return roomCountArray[z];
    }

    public void decrementRoomCount(int z) {
        roomCountArray[z]--;
        if (roomCountArray[z] < 0)
        	roomCountArray[z] = 0; // incase of bugs? lol
    }

    public void setTeleportInside(boolean teleportInside) {
        this.teleportInside = teleportInside;
        owner.getPacketSender().sendConfig(8102, teleportInside ? 1 : 2);
    }

    public boolean isTeleportInside() {
        return teleportInside;
    }

    public void setRenderDoorsOpen(boolean renderDoorsOpen) {
        this.renderDoorsOpen = renderDoorsOpen;
        owner.getPacketSender().sendConfig(8101, renderDoorsOpen ? 1 : 2);
    }

    public void copyRooms(Room[][][] rooms) {
        copyRooms = new Room[MAXIMUM_FLOORS][MAXIMUM_ROOMS][MAXIMUM_ROOMS];

        for (int z = 0; z < rooms.length; z++) {
            for (int x = 0; x < rooms[z].length; x++) {
                for (int y = 0; y < rooms[z][x].length; y++) {
                    copyRooms[z][x][y] = rooms[z][x][y];
                }
            }
        }
    }

    public void clearCopyRooms() {
        copyRooms = null;
    }

    public Room[][][] getCopyRooms() {
        return copyRooms;
    }

    public boolean isInDungeon() {
        return owner.playerIsInHouse && owner.getZ() == 0;
    }

    public void incrementGridSize() {
        this.gridSize++;

        if (gridSize > MAXIMUM_GRID_SIZE) {
            gridSize = MAXIMUM_GRID_SIZE;
        }
    }

    public int getGridSize() {
        return gridSize;
    }

    public void incrementExitPortalCount() {
        this.exitPortalCount++;
    }

    public void decrementExitPortalCount() {
        if (--exitPortalCount < 0) {
            exitPortalCount = 0;
        }
    }

    public int getExitPortalCount() {
        return exitPortalCount;
    }

	public ArrayList<Item> getTreasureChestItems() {
		return treasureChestItems;
	}

	public ArrayList<Item> getFancyDressBox() {
		return fancyDressBox;
	}

	public ArrayList<Item> getArmorCase() {
		return armorCase;
	}

	public ArrayList<Item> getMagicWardrobe() {
		return magicWardrobe;
	}

	public ArrayList<Item> getCapeRack() {
		return capeRack;
	}

	public ArrayList<Item> getToyBox() {
		return toyBox;
	}
	
	public static String generateStorageString(ArrayList<Item> list) {
		StringBuilder builder = new StringBuilder();

		for (int i = 0, l = list.size(); i < l; i++) {
			Item item = list.get(i);
			builder.append(item.getId());
			builder.append(":");
			builder.append(item.getAmount());
			builder.append(";");
		}
		
		return builder.toString();
	}
	
	public static void extractStorageString(Player p, String storageString, ArrayList<Item> list) {
		if (storageString == null || storageString.equals(""))
			return;
		String[] splitString = storageString.split(";");
		for (int i = 0, len = splitString.length; i < len; i++) {
			String[] split = splitString[i].split(":");
			
			try { // try block in case someone messes with the char files
				final int id = Integer.parseInt(split[0]);
				final int amount = Integer.parseInt(split[1]);
				list.add(new Item(id, amount));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			
		}
	}

    public CycleEventContainer getNpcTask() {
        return npcTask;
    }

    public void setNpcTask(CycleEventContainer npcTask) {
        this.npcTask = npcTask;
    }

    public Map<RoomObject, NPC> getNpcs() {
        return npcMap;
    }

	public int getGuestSize() {
		return guestSize;
	}
	
	public void incGuestSize() {
		guestSize++;
	}
	
	public void decGuestSize() {
		guestSize--;
        if (guestSize < 0) {
            guestSize = 0;
        }
	}

	public int getWhiteberryBushCount() {
		return whiteberryBushCount;
	}

	public void setWhiteberryBushCount(int whiteberryBushCount) {
		this.whiteberryBushCount = whiteberryBushCount;
	}

}
