package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class MonsterHotspot extends Hotspot {

    public MonsterHotspot() {
        super(HotspotType.MONSTER);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Demon(), new KalphiteSoldier(), new TokXil(), new Dagannoth(), new SteelDragon()));
    }

}
