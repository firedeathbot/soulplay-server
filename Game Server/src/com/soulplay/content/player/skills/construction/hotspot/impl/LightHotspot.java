package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Candle;
import com.soulplay.content.player.skills.construction.buildable.impl.SkullTorch;
import com.soulplay.content.player.skills.construction.buildable.impl.Torch;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LightHotspot extends Hotspot {

    public LightHotspot() {
        super(HotspotType.LIGHT);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Candle(), new Torch(), new SkullTorch()));
    }

}
