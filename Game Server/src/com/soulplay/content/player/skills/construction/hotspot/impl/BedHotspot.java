package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class BedHotspot extends Hotspot {

    public BedHotspot() {
        super(HotspotType.BED);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenBed(), new OakBed(), new LargeOakBed(), new TeakBed(), new LargeTeakBed(), new FourPosterBed(), new GildedFourPosterBed()));
    }
}
