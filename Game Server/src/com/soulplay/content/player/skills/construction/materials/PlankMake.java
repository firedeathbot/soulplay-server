package com.soulplay.content.player.skills.construction.materials;

public enum PlankMake {

	REGULAR(960, 1511),
	OAK(8778, 1521),
	TEAK(8780, 6333),
	MAHOGANY(8782, 6332);
	
	private final int plankId, logId;
	
	private PlankMake(int plankId, int logId) {
		this.plankId = plankId;
		this.logId = logId;
	}

	public int getPlankId() {
		return plankId;
	}

	public int getLogId() {
		return logId;
	}

}