package com.soulplay.content.player.skills.construction.content;

import com.soulplay.game.LocationConstants;
import com.soulplay.game.world.entity.Location;

public enum Portals {
    PORTAL_1("Varrock", LocationConstants.VARROCK_X, LocationConstants.VARROCK_Y, 0, 25, new int[][]{{563, 100}, {554, 100/* fire */}, {556, 300 /* air */}}, new int[]{13615, 13622, 13629}),
    PORTAL_2("Lumbridge", LocationConstants.LUMBY_X, LocationConstants.LUMBY_Y, 0, 31, new int[][]{{563, 100}, {557, 100/* earth */}, {556, 300}}, new int[]{13616, 13623, 13630}),
    PORTAL_3("Falador", LocationConstants.FALADOR_X, LocationConstants.FALADOR_Y, 0, 37, new int[][]{{563, 100}, {555, 100/* water */}, {556, 300}}, new int[]{13617, 13624, 13631}),
    PORTAL_4("Camelot", LocationConstants.CAMELOT_X, LocationConstants.CAMELOT_Y, 0, 45, new int[][]{{563, 200}, {556, 500}}, new int[]{13618, 13625, 13632}),
    PORTAL_5("Ardougne", LocationConstants.ARDOUGNE_X, LocationConstants.ARDOUGNE_Y, 0, 51, new int[][]{{563, 200}, {555, 200}}, new int[]{13619, 13626, 13633}),
    PORTAL_6("Yanille", LocationConstants.YANILLE.getX(), LocationConstants.YANILLE.getY(), 0, 58, new int[][]{{563, 200}, {557, 200}}, new int[]{13620, 13627, 13634}),
    PORTAL_7("Kharyll", LocationConstants.KHARYRLL_X, LocationConstants.KHARYRLL_Y, 0, 66, new int[][]{{563, 200}, {565, 100}}, new int[]{13621, 13628, 13635});

    public static final Portals[] values = Portals.values();

    public static Portals forObjectId(int objectId) {
        for (Portals p : values) {
            for (int i : p.objects) {
                if (i == objectId) {
                    return p;
                }
            }
        }
        return null;
    }

    private final String name;
    private final Location destination;
    private int[][] requiredItems;
    private int[] objects;
    private int magicLevel;

    Portals(String name, int x, int y, int z, int magicLevel, int[][] requiredItems, int[] objects) {
        this.name = name;
        this.destination = Location.create(x, y, z);
        this.requiredItems = requiredItems;
        this.objects = objects;
        this.magicLevel = magicLevel;
    }

    public String getName() {
        return name;
    }

    public Location getDestination() {
        return destination;
    }

    public int getMagicLevel() {
        return magicLevel;
    }

    public int[][] getRequiredItems() {
        return requiredItems;
    }

    public int[] getObjects() {
        return objects;
    }

}
