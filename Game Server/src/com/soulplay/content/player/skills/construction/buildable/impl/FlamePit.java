package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class FlamePit extends Buildable {

    @Override
    public String getName() {
        return "Flame pit";
    }

    @Override
    public int getItemId() {
        return 8304;
    }

    @Override
    public int getObjectId() {
        return 13337;
    }

    @Override
    public int getLevelRequired() {
        return 77;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TINDERBOX, 20), new Item(995, 125_000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 357;
    }

}
