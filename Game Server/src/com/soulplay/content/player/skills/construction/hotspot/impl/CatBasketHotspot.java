package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.CatBasket;
import com.soulplay.content.player.skills.construction.buildable.impl.CatBlanket;
import com.soulplay.content.player.skills.construction.buildable.impl.CushionedBasket;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class CatBasketHotspot extends Hotspot {

    public CatBasketHotspot() {
        super(HotspotType.CAT_BASKET);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new CatBlanket(), new CatBasket(), new CushionedBasket()));
    }

}
