package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class BarrelHotspot extends Hotspot {

    public BarrelHotspot() {
        super(HotspotType.BARREL);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new BeerBarrel(), new CiderBarrel(), new AsgarnianAleBarrel(), new GreenmanAleBarrel(), new DragonBitterBarrel(), new ChefDelightBarrel()));
    }

}
