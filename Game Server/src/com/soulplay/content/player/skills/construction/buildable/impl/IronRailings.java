package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class IronRailings extends Buildable {
    @Override
    public String getName() {
        return "Iron railings";
    }

    @Override
    public int getItemId() {
        return 8199;
    }

    @Override
    public int getObjectId() {
        return 13452;
    }

    @Override
    public int getLevelRequired() {
        return 67;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(2351, 10), new Item(ConstructionItems.LIMESTONE_BRICK, 6));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 220;
    }
}
