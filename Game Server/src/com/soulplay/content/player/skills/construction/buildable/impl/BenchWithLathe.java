package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class BenchWithLathe extends Buildable {
    @Override
    public String getName() {
        return "Bench with a lathe";
    }

    @Override
    public int getItemId() {
        return 8379;
    }

    @Override
    public int getObjectId() {
        return 13708;
    }

    @Override
    public int getLevelRequired() {
        return 77;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(8378), new Item(ConstructionItems.OAK_PLANK, 2), new Item(ConstructionItems.STEEL_BAR));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 140;
    }
}
