package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Trapdoor extends Buildable {
    @Override
    public String getName() {
        return "Trapdoor";
    }

    @Override
    public int getItemId() {
        return 8372;
    }

    @Override
    public int getObjectId() {
        return 13686;
    }

    @Override
    public int getLevelRequired() {
        return 74;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 5), new Item(8792, 10));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 770;
    }
}
