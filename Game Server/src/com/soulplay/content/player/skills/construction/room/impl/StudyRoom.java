package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class StudyRoom extends Room {

	private static final Palette palette = new Palette(6, -1);

	private static final boolean[] doors = {false, true, true, true};

	public StudyRoom(int x, int y, int z) {
		super(RoomType.STUDY, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.WALL_CHART, new RoomObject(15423, Location.create(7, 0), 0, 5));
		hotspots.put(HotspotType.WALL_CHART, new RoomObject(15423, Location.create(7, 1), 2, 5));
		hotspots.put(HotspotType.WALL_CHART, new RoomObject(15423, Location.create(6, 7), 1, 5));
		hotspots.put(HotspotType.WALL_CHART, new RoomObject(15423, Location.create(1, 7), 1, 5));
		hotspots.put(HotspotType.LECTERN, new RoomObject(15420, Location.create(2, 2), 2, 10));
		hotspots.put(HotspotType.CRYSTAL_BALL, new RoomObject(15422, Location.create(5, 2), 0, 10));
		hotspots.put(HotspotType.GLOBE, new RoomObject(15421, Location.create(1, 4), 0, 10));
		hotspots.put(HotspotType.BOOKCASE, new RoomObject(15425, Location.create(3, 7), 1, 10));
		hotspots.put(HotspotType.BOOKCASE, new RoomObject(15425, Location.create(4, 7), 1, 10));
		hotspots.put(HotspotType.TELESCOPE, new RoomObject(15424, Location.create(5, 7), 2, 10));
	}

}
