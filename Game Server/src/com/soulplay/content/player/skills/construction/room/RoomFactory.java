package com.soulplay.content.player.skills.construction.room;

import com.soulplay.content.player.skills.construction.room.impl.*;

public class RoomFactory {
    
    public static Room createRoom(RoomType type, int x, int y, int z) {
	switch(type) {

	case GARDEN:
	    return new GardenRoom(x, y, z);
	    
	case KITCHEN:
	    return new KitchenRoom(x, y, z);
	    
	case PARLOUR:
	    return new ParlourRoom(x, y, z);
	    
	case DINING:
	    return new DiningRoom(x, y, z);
	    
	case WORKSHOP:
	    return new WorkshopRoom(x, y, z);
	    
	case BED:
	    return new BedRoom(x, y, z);
	    
	case SKILL_TROPHY_HALL:
	    return new SkillTrophyHallRoom(x, y, z);
	    
	case SKILL_HALL_UPPER:
		return new SkillTrophyHallRoomUpper(x, y, z);
	    
	case GAMES:
	    return new GamesRoom(x, y, z);
	    
	case COMBAT:
	    return new CombatRoom(x, y, z);
	    
	case QUEST_TROPHY:
	    return new QuestTrophyRoom(x, y, z);
	    
	case QUEST_HALL_UPPER:
	    return new QuestTrophyRoomUpper(x, y, z);
	    
	case MENAGERY:
	    return new MenageryRoom(x, y, z);
	    
	case STUDY:
	    return new StudyRoom(x, y, z);
	    
	case COSTUME:
	    return new CostumeRoom(x, y, z);
	    
	case CHAPEL:
	    return new ChapelRoom(x, y, z);
	    
	case PORTAL:
	    return new PortalRoom(x, y, z);
	    
	case FORMAL_GARDEN:
	    return new FormalGarden(x, y, z);
	    
	case THRONE:
	    return new ThroneRoom(x, y, z);

	case DUNGEON_STAIR_ROOM:
		return new DungeonStairsRoom(x, y, z);

		case DUNGEON_CORRIDOR:
			return new DungeonCorridorRoom(x, y, z);

		case DUNGEON_JUNCTION:
			return new DungeonJunctionRoom(x, y ,z);

		case DUNGEON_PIT:
			return new DungeonPitRoom(x, y, z);

		case DUNGEON_TREASURE_ROOM:
			return new DungeonTreasureRoom(x, y, z);

		case DUNGEON_OUBLIETTE:
			return new DungeonOublietteRoom(x, y, z);
	    
	default:
	    return new EmptyRoom(x, y, z);
	
	}
    }

}
