package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Elena;
import com.soulplay.content.player.skills.construction.buildable.impl.GiantDwarf;
import com.soulplay.content.player.skills.construction.buildable.impl.KingArthur;
import com.soulplay.content.player.skills.construction.buildable.impl.Miscellanians;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class PortraitHotspot extends Hotspot {

    public PortraitHotspot() {
        super(HotspotType.PORTRAIT);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new KingArthur(), new Elena(), new GiantDwarf(), new Miscellanians()));
    }

}
