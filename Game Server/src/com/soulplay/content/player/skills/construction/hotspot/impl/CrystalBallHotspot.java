package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.CrystalBall;
import com.soulplay.content.player.skills.construction.buildable.impl.CrystalOfPower;
import com.soulplay.content.player.skills.construction.buildable.impl.ElementalSphere;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class CrystalBallHotspot extends Hotspot {

    public CrystalBallHotspot() {
        super(HotspotType.CRYSTAL_BALL);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new CrystalBall(), new ElementalSphere(), new CrystalOfPower()));
    }
}
