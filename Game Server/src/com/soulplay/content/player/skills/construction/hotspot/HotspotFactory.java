package com.soulplay.content.player.skills.construction.hotspot;

import java.util.HashMap;
import java.util.Map;

public final class HotspotFactory {

	public static final Map<HotspotType, Hotspot> hotspots = new HashMap<>();

	private HotspotFactory() {

	}

	public static Hotspot getHotspot(HotspotType type) {	
		return hotspots.get(type);
	}

}
