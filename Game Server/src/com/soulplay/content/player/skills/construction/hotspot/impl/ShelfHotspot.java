package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ShelfHotspot extends Hotspot {

    public ShelfHotspot() {
        super(HotspotType.SHELF);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenShelves1(), new WoodenShelves2(), new WoodenShelves3(), new OakShelves1(), new OakShelves2(), new TeakShelves1(), new TeakShelves2()));
    }

}
