package com.soulplay.content.player.skills.construction;

import java.util.ArrayList;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;

public class StorageObject {
	
	public static final int MAIN_INTERFACE = 3824;
	public static final int INVENTORY_INTERFACE = 3822;
	public static final int INVENTORY_ITEMS_INTERFACE = 3823;
	public static final int MAIN_INTERFACE_ITEMS = 3900;
	public static final int MAIN_INTERFACE_TITLE = 3901;
	
	private boolean isActive;
	private final Client c;
	
	private StorageObjectEnum storageType;
	
	public ArrayList<Item> listRef;
	
	public StorageObject(Client c) {
		this.c = c;
	}
	
	public void openStorage(Client owner, StorageObjectEnum storageType) {
		c.getPacketSender().sendFrame126(storageType.getInterfaceName(), MAIN_INTERFACE_TITLE);
		c.getPacketSender().sendFrame248(MAIN_INTERFACE, INVENTORY_INTERFACE); // open interface
		c.getItems().resetItems(INVENTORY_ITEMS_INTERFACE); // reset own inventory
		
		this.storageType = storageType;
		storageType.setList(owner);
		c.getPacketSender().itemsOnInterface(MAIN_INTERFACE_ITEMS, listRef);
		isActive = true;
	}
	
	public boolean deposit(int itemId, int itemSlot, int amount) {
		if (!isActive)
			return false;
		
		if (!isHomeOwner()) {
			return true;
		}
		
		if (!c.getSpamTick().checkThenStart(1)) {
			c.sendMessage("Please slow down.");
			return true;
		}
		
		StorageObjectEnum addable = StorageObjectEnum.getStorage(itemId);
		
		if (addable == null || addable.getId() != this.storageType.getId()) {
			c.sendMessage("You cannot put that here.");
			return true;
		}
		
//		for (int i = 0, l = listRef.size(); i < l; i++) {
//			Item item = listRef.get(i);
//			if (item.getId() == itemId+1) {
//				c.sendMessage("Your storage already contains that item.");
//				return true;
//			}
//		}
		if (listRef.size() == this.storageType.getMaxSize()) {
			c.sendMessage("You have maxed out your capacity.");
			return true;
		}
		
		if (c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
			listRef.add(new Item(itemId+1));
		}
		
		openStorage(c, storageType);
		return true;
	}
	
	public boolean withdraw(final int itemId, final int slot, int amount) {
		if (!isActive)
			return false;

		if (!isHomeOwner()) {
			return true;
		}
		
		if (c.getItems().freeSlots() == 0) {
			c.sendMessage("You do not have enough inventory space.");
			return true;
		}
		
		if (slot < 0 || slot > listRef.size()) {
			return true;
		}
		
		Item listItem = listRef.get(slot);
		
		if (listItem != null && listItem.getId() == itemId+1) {
			if (c.getItems().addItem(itemId, 1)) {
				listRef.remove(slot);
				openStorage(c, storageType);
			}
		}
		return true;
	}

	public boolean isOpen() {
		return isActive;
	}

	public void close() {
		this.isActive = false;
	}
	
	public boolean isHomeOwner() {
		return c.getHomeOwnerId() == c.mySQLIndex;
	}

}
