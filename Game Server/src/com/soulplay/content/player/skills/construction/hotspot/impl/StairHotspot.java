package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class StairHotspot extends Hotspot {

    public StairHotspot() {
        super(HotspotType.STAIR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakStaircase(), new Teakstaircase(), new Spiralstaircase(), new Marblestaircase(), new MarbleSpiralStaircase()));
        colliders.add(HotspotType.RUG);
    }

}
