package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class OpulentRug extends Buildable {

    @Override
    public String getName() {
        return "Opulent rug";
    }

    @Override
    public int getItemId() {
        return 8318;
    }

    @Override
    public int getObjectId() {
        return 13594;
    }

    @Override
    public int getLevelRequired() {
        return 65;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.CLOTH, 4), new Item(ConstructionItems.GOLD_LEAF));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_LOW_ANIM;
    }

    @Override
    public int getExperience() {
        return 360;
    }

}
