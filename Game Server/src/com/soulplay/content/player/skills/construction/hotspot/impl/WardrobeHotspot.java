package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class WardrobeHotspot extends Hotspot {

    public WardrobeHotspot() {
        super(HotspotType.WARDROBE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Shoebox(), new OakDrawers(), new OakWardrobe(), new TeakDrawers(), new TeakWardrobe(), new MahoganyWardrobe(), new GildedWardrobe()));
    }
}
