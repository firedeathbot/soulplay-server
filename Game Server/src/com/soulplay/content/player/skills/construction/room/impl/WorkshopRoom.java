package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class WorkshopRoom extends Room {

	private static final Palette palette = new Palette(2, -1);

	private static final boolean[] doors = {true, false, true, false};

	public WorkshopRoom(int x, int y, int z) {
		super(RoomType.WORKSHOP, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.WORKBENCH, new RoomObject(15439, Location.create(3, 4), 0, 10));
		hotspots.put(HotspotType.REPAIR, new RoomObject(15448, Location.create(7, 3), 1, 10));
		hotspots.put(HotspotType.CLOCKMAKING, new RoomObject(15441, Location.create(0, 3), 3, 10));
		hotspots.put(HotspotType.HERALDRY, new RoomObject(15450, Location.create(7, 6), 1, 10));

		hotspots.put(HotspotType.TOOL_1, new RoomObject(15443, Location.create(1, 0), 3, 5));
		hotspots.put(HotspotType.TOOL_2, new RoomObject(15444, Location.create(6, 0), 3, 5));
		hotspots.put(HotspotType.TOOL_3, new RoomObject(15445, Location.create(0, 1), 0, 5));
		hotspots.put(HotspotType.TOOL_4, new RoomObject(15446, Location.create(7, 1), 2, 5));
		hotspots.put(HotspotType.TOOL_5, new RoomObject(15447, Location.create(0, 6), 0, 5));
	}
	
    public static void load() {
    	/* empty */
    }
}
