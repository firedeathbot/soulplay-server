package com.soulplay.content.player.skills.construction.room;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;

/**
 * The type of room that can be built.
 *
 * Do not add or rearrange the order, the order is important. Any new entries add at the bottom.
 *
 * @author nshusa
 */
public enum RoomType {
    PARLOUR(1, 1_000),
    GARDEN(1, 1_000),
    KITCHEN(5, 5_000),
    DINING(10, 5_000),
    WORKSHOP(15, 10_000),
    BED(20, 10_000),
    SKILL_TROPHY_HALL(25, 15_000),
    SKILL_HALL_UPPER(0, 0),
    GAMES(30, 25_000),
    COMBAT(32, 25_000),
    QUEST_TROPHY(35, 25_000),
    QUEST_HALL_UPPER(0, 0),
    MENAGERY(37, 30_000),
    STUDY(40, 50_000),
    COSTUME(42, 50_000),
    CHAPEL(45, 50_000),
    PORTAL(50, 100_000),
    FORMAL_GARDEN(55, 75_000),
    THRONE(60, 150_000),
    DUNGEON_OUBLIETTE(65, 150_000),
    DUNGEON_CORRIDOR(70, 7_500),
    DUNGEON_JUNCTION(70, 7_500),
    DUNGEON_STAIR_ROOM(70, 7_500),
    DUNGEON_PIT(70, 10_000),
    DUNGEON_TREASURE_ROOM(75, 250_000),
    EMPTY(0, 0),
    EMPTY_DUNG(0, 0)
    ;
	
	public static final RoomType[] values = values();

    private final int levelRequired, cost;  

    RoomType(int levelRequired, int cost) {
        this.levelRequired = levelRequired;
        this.cost = cost;
    }

    public static boolean hasRequirements(Player player, RoomType type, boolean showMessage) {
        if (player.getSkills().getLevel(Skills.CONSTRUCTION) < type.getLevelRequired()) {
            if (showMessage) {
                player.sendMessage(String.format("You need a construction level of %d to build this room.", type.getLevelRequired()));
            }
            return false;
        } else if (!player.getItems().playerHasItem(995, type.getCost())) {
            if (showMessage) {
                player.sendMessage(String.format("You need %d coins to build this room.", type.getCost()));
            }
            return false;
        }
        return true;
    }

    public static final int BEGIN_INTERFACE_COST_TEXT = 28650;

    public int getLevelRequired() {
        return levelRequired;
    }

    public int getCost() {
        return cost;
    }
    
}
