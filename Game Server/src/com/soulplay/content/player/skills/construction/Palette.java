package com.soulplay.content.player.skills.construction;

import com.soulplay.game.world.entity.Location;

public class Palette {

	private static final int DIRECTION_NORTH = 0;
	private static final int DIRECTION_EAST = 1;
	private static final int DIRECTION_SOUTH = 2;
	private static final int DIRECTION_WEST = 3;

	private final int offsetX, offsetY;
	private int height;
	private int rotation;

	public Palette(int offsetX, int offsetY) {
		this(offsetX, offsetY, 0);
	}

	public Palette(int offsetX, int offsetY, int height) {
		this(offsetX, offsetY, height, DIRECTION_NORTH);
	}

	public Palette(int offsetX, int offsetY, int height, int rotation) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.height = height;
		this.rotation = rotation;
	}
	
	public void rotate(int rotation) {
	    this.rotation = rotation;
	}

	public int getRotation() {
		return rotation % 4;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}
	
	public Palette copy() {
		return new Palette(offsetX, offsetY);
	}

	public int hash(int region) {
		int regionX = region >> 8;
		int regionY = region & 0xFF;
		int chunkX = (regionX * 8) + offsetX;
		int chunkY = (regionY * 8) + offsetY;
		int x = ((chunkX << 3) + 42 + 6) / 8;
		int y = ((chunkY << 3) + 42 + 6) / 8;
		return x << 15 | y << 3 | height << 26 | rotation << 1;
	}

	public Location toLocation(int region) {
		int regionX = region >> 8;
		int regionY = region & 0xFF;
		int chunkX = (regionX * 8) + offsetX;
		int chunkY = (regionY * 8) + offsetY;
		int x = (chunkX << 3) + 42 + 6;
		int y = (chunkY << 3) + 42 + 6;
		return Location.create(x, y, height % 4);
	}

}
