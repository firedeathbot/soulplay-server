package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyArmorCase;
import com.soulplay.content.player.skills.construction.buildable.impl.OakArmorCase;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakArmorCase;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ArmorCaseHotspot extends Hotspot {

    public ArmorCaseHotspot() {
        super(HotspotType.ARMOR_CASE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakArmorCase(), new TeakArmorCase(), new MahoganyArmorCase()));
    }
}
