package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.BellPull;
import com.soulplay.content.player.skills.construction.buildable.impl.PoshBellPull;
import com.soulplay.content.player.skills.construction.buildable.impl.RopeBellPull;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class BellPullHotspot extends Hotspot {

    public BellPullHotspot() {
        super(HotspotType.BELL_PULL);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new RopeBellPull(), new BellPull(), new PoshBellPull()));
    }
}
