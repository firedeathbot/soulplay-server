package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class CombatRoom extends Room {

	private static final Palette palette = new Palette(5, -2);

	private static final boolean[] doors = {false, true, true, true};

	public CombatRoom(int x, int y, int z) {
		super(RoomType.COMBAT, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(1, 2), 2, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(1, 3), 2, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(1, 4), 2, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(2, 1), 1, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(3, 1), 1, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(4, 1), 1, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(6, 3), 0, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15277, Location.create(6, 4), 0, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15278, Location.create(6, 1), 0, 3));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15279, Location.create(1, 1), 1, 3));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15280, Location.create(1, 5), 2, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15280, Location.create(2, 6), 3, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15281, Location.create(1, 6), 2, 3));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15282, Location.create(6, 6), 3, 3));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15286, Location.create(3, 6), 3, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15286, Location.create(4, 6), 3, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15286, Location.create(5, 6), 3, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15286, Location.create(6, 5), 0, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15287, Location.create(5, 1), 1, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15287, Location.create(6, 2), 0, 0));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15288, Location.create(3, 5), 1, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15288, Location.create(4, 5), 1, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15289, Location.create(5, 5), 2, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15290, Location.create(2, 5), 1, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15291, Location.create(2, 3), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15291, Location.create(2, 4), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15291, Location.create(5, 3), 2, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15291, Location.create(5, 4), 2, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15292, Location.create(3, 3), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15292, Location.create(3, 4), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15292, Location.create(4, 3), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15292, Location.create(4, 4), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15293, Location.create(3, 2), 3, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15293, Location.create(4, 2), 3, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15294, Location.create(2, 2), 0, 22));
		hotspots.put(HotspotType.COMBAT_RING, new RoomObject(15295, Location.create(5, 2), 3, 22));
		hotspots.put(HotspotType.STORAGE, new RoomObject(15296, Location.create(3, 7), 0, 10));
		hotspots.put(HotspotType.DECORATION, new RoomObject(15297, Location.create(1, 7), 1, 5));
		hotspots.put(HotspotType.DECORATION, new RoomObject(15297, Location.create(6, 7), 1, 5));
	}

}
