package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class CapeRackHotspot extends Hotspot {

    public CapeRackHotspot() {
        super(HotspotType.CAPE_RACK);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakCapeRack(), new TeakCapeRack(), new MahoganyCapeRack(), new GildedCapeRack(), new MarbleCapeRack(), new MagicalCapeRack()));
    }
}
