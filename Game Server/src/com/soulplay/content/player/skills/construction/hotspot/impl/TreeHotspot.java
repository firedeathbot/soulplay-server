package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;
import com.soulplay.content.player.skills.construction.buildable.impl.NiceTree;
import com.soulplay.content.player.skills.construction.buildable.impl.OakTree;
import com.soulplay.content.player.skills.construction.buildable.impl.WillowTree;
import com.soulplay.content.player.skills.construction.buildable.impl.YewTree;
import com.soulplay.content.player.skills.construction.buildable.impl.DeadTree;
import com.soulplay.content.player.skills.construction.buildable.impl.MagicTree;
import com.soulplay.content.player.skills.construction.buildable.impl.MapleTree;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class TreeHotspot extends Hotspot {

    public TreeHotspot() {
	super(HotspotType.TREE);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new DeadTree(), new NiceTree(), new OakTree(), new WillowTree(), new MapleTree(), new YewTree(),
		new MagicTree()));	
    }

}
