package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.AlchemicalChart;
import com.soulplay.content.player.skills.construction.buildable.impl.AstronomicalChart;
import com.soulplay.content.player.skills.construction.buildable.impl.InfernalChart;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class WallChartHotspot extends Hotspot {

    public WallChartHotspot() {
        super(HotspotType.WALL_CHART);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new AlchemicalChart(), new AstronomicalChart() , new InfernalChart()));
    }
}
