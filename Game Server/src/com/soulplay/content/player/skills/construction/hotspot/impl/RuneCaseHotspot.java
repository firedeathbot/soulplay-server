package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Runecase1;
import com.soulplay.content.player.skills.construction.buildable.impl.Runecase2;
import com.soulplay.content.player.skills.construction.buildable.impl.Runecase3;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class RuneCaseHotspot extends Hotspot {

    public RuneCaseHotspot() {
        super(HotspotType.RUNE_CASE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Runecase1(), new Runecase2(), new Runecase3()));
    }

}
