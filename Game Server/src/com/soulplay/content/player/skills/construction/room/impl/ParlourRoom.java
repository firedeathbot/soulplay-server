package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public final class ParlourRoom extends Room {

	private static final Palette palette = new Palette(2, 1);

	private static final boolean[] doors = {false, true, true, true};

	public ParlourRoom(int x, int y, int z) {
		super(RoomType.PARLOUR, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

    public static void load() {
    	/* empty */
    }

	static {
		hotspots.put(HotspotType.PARLOUR_CHAIR_1, new RoomObject(15410, Location.create(2, 4), 2, 11));
		hotspots.put(HotspotType.PARLOUR_CHAIR_2, new RoomObject(15411, Location.create(5, 4), 1, 11));
		hotspots.put(HotspotType.PARLOUR_CHAIR_3, new RoomObject(15412, Location.create(4, 3), 2, 10));
		hotspots.put(HotspotType.RUG, new RoomObject(15413, Location.create(3, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15413, Location.create(3, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15413, Location.create(4, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15413, Location.create(4, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(2, 3), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(2, 4), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(3, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(3, 5), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(4, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(4, 5), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(5, 3), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15414, Location.create(5, 4), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15415, Location.create(2, 2), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15415, Location.create(2, 5), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15415, Location.create(5, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15415, Location.create(5, 5), 1, 22));
		hotspots.put(HotspotType.BOOKCASE, new RoomObject(15416, Location.create(0, 1), 0, 10));
		hotspots.put(HotspotType.BOOKCASE, new RoomObject(15416, Location.create(7, 1), 2, 10));
		hotspots.put(HotspotType.FIREPLACE, new RoomObject(15418, Location.create(3, 7), 1, 10));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(0, 2), 0, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(0, 5), 0, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(2, 0), 3, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(2, 7), 1, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(5, 0), 3, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(5, 7), 1, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(7, 2), 2, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15419, Location.create(7, 5), 2, 5));
	}

}
