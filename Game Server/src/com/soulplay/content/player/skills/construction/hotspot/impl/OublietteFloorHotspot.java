package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.FlamePit;
import com.soulplay.content.player.skills.construction.buildable.impl.Rocnar;
import com.soulplay.content.player.skills.construction.buildable.impl.Spikes;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class OublietteFloorHotspot extends Hotspot {

    public OublietteFloorHotspot() {
        super(HotspotType.OUBLIETTE_FLOOR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Spikes(), new FlamePit(), new Rocnar()));
    }
}
