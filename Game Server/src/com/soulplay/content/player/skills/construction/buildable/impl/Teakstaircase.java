package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Teakstaircase extends Buildable {
    @Override
    public String getName() {
        return "Teak staircase";
    }

    @Override
    public int getItemId() {
        return 8252;
    }

    @Override
    public int getObjectId() {
        return 13499;
    }
    
    @Override
    public int getObjectId(boolean upperRoom) {
    	return !upperRoom ? getObjectId() : 13500;
    }

    @Override
    public int getLevelRequired() {
        return 48;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK, 10), new Item(ConstructionItems.STEEL_BAR, 4));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 980;
    }
}
