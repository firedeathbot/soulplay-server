package com.soulplay.content.player.skills.construction.hotspot;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public enum HotspotType {

	GARDEN_CENTREPIECE(15361),
	TREE(15362, 15363),
	SMALL_PLANT_1(15366),
	SMALL_PLANT_2(15367),
	BIG_PLANT_1(15364),
	BIG_PLANT_2(15365),
	PARLOUR_CHAIR_1(15410),
	PARLOUR_CHAIR_2(15411),
	PARLOUR_CHAIR_3(15412),
	RUG(15264, 15265, 15266, 15377, 15378, 15379, 15387, 15388, 15413, 15414, 15415, 15273, 15274),
	CURTAIN(15263, 15302, 15419),
	FIREPLACE(15267, 15301, 15418),
	BOOKCASE(15397, 15416, 15425),
	KITCHEN_TABLE(15405),
	DINING_TABLE(15298),
	LARDER(15403),
	BARREL(15401),
	STOVE(15398),
	SINK(15404),
	CAT_BASKET(15402),
	SHELF(15399, 15400),
	SEATING(15299, 15300, 15436, 15437),
	DECORATION(15297, 15303, 15434),
	BELL_PULL(15304),
	WORKBENCH(15439),
	REPAIR(15448),
	CLOCKMAKING(15441),
	HERALDRY(15450),
	TOOL_1(15443),
	TOOL_2(15444),
	TOOL_3(15445),
	TOOL_4(15446),
	TOOL_5(15447),
	WARDROBE(15261),
	BED(15260),
	CLOCK(15268),
	DRESSER(15262),
	DECORATIVE_ARMOR(15385),
	ARMOR(15384),
	STAIR(15380, 15381, 15390, 15391),
	RUNE_CASE(15386),
	HEAD_TROPHY(15382),
	GAME(15342),
	STONE(15344),
	ELEMENTAL_BALANCE(15345),
	PRIZE_CHEST(15343),
	RANGING_GAME(15346),
	STORAGE(15296),
	COMBAT_RING(15277, 15278, 15279, 15280, 15282, 15286, 15287, 15289, 15290, 15291, 15292, 15293, 15294, 15295, 15281, 15288),
	GUILD_TROPHY(15394),
	PORTRAIT(15392),
	LANDSCAPE(15393),
	SWORD(15395),
	MAP(15396),
	TELESCOPE(15424),
	GLOBE(15421),
	CRYSTAL_BALL(15422),
	LECTERN(15420),
	WALL_CHART(15423),
	TREASURE_CHEST(18813),
	FANCY_DRESS_BOX(18814),
	TOY_BOX(18812),
	CAPE_RACK(18810),
	ARMOR_CASE(18815),
	MAGIC_WARDROBE(18811),
	ALTAR(15270),
	ICON(15269),
	STATUE(15275),
	MUSICAL(15276),
	LAMP(15271),
	WINDOW_SPACE(13730, 13731, 13732, 13733),
	PORTAL_1(15406),
	PORTAL_2(15407),
	PORTAL_3(15408),
	PORTAL_CENTERPIECE(15409),
	FLOOR(15427, 15428, 15429, 15430, 15431, 15432),
	LEVER(15435),
	THRONE(15426),
	TRAPDOOR(15438),
	FORMAL_CENTREPIECE(15368),
	SMALL_PLANT(15375),
	BIG_PLANT(15373),
	FORMAL_SMALL_PLANT_2(15376),
	FORMAL_BIG_PLANT_2(15374),
	HEDGING(15370, 15371, 15372),
	FENCING(15369),
	DUNGEON_DECORATION(15331),
	DUNGEON_DECORATION_2(15259),
	GUARD(15323, 15336, 15337, 15354),
	MONSTER(15257),
	LIGHT(15330, 15355, 34138),
	DUNGEON_TRAP(15325, 15324),
	DUNGEON_CHEST(15256),
	DUNGEON_DOOR(15326, 15327, 15328, 15329),
	OUBLIETTE_FLOOR(15351, 15350, 15349, 15348, 15347),
	LADDER(15356),
	WINDOW(13830),
	PRISON_SPACE(15352),
	FISHING_TROPHY(15383),
	DOOR(15305, 15306, 15307, 15308, 15309, 15310, 15311, 15312, 15313, 15314, 15315, 15316, 15317);

	private final int[] hotspotObjectIds;

	private static final Map<Integer, HotspotType> map = new HashMap<>();
	
	private static final Multimap<Integer, Integer> objectsMap = ArrayListMultimap.create();

    public static void load() {
    	/* empty */
    }

	static {
		for (HotspotType type : HotspotType.values()) {
			int size = type.getHotspotObjectIds().length;
			for (int hotspotId : type.getHotspotObjectIds()) {
				map.put(hotspotId, type);
				for (int j = 0; j < size; j++) {
					objectsMap.put(hotspotId, type.getHotspotObjectIds()[j]);
				}
			}
		}
	}

	private HotspotType(int... hotspotObjectIds) {
		this.hotspotObjectIds = hotspotObjectIds;
	}

	public int[] getHotspotObjectIds() {
		return hotspotObjectIds;
	}
	
	public static HotspotType lookup(int value) {
		return map.get(value);
	}
	
	public static boolean isHotspotObject(int id) {
		return map.containsKey(id);
	}

	public static Collection<Integer> getObjectIds(int id) {
		return objectsMap.get(id);
	}

}
