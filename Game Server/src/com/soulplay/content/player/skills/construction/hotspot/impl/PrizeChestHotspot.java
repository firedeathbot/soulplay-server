package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyPrizeChest;
import com.soulplay.content.player.skills.construction.buildable.impl.OakPrizeChest;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakPrizeChest;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class PrizeChestHotspot extends Hotspot {

    public PrizeChestHotspot() {
        super(HotspotType.PRIZE_CHEST);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakPrizeChest(), new TeakPrizeChest(), new MahoganyPrizeChest()));
    }

}
