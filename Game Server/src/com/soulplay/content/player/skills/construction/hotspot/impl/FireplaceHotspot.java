package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;

import com.soulplay.content.player.skills.construction.buildable.impl.ClayFireplace;
import com.soulplay.content.player.skills.construction.buildable.impl.MarbleFireplace;
import com.soulplay.content.player.skills.construction.buildable.impl.StoneFireplace;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class FireplaceHotspot extends Hotspot {

    public FireplaceHotspot() {
	super(HotspotType.FIREPLACE);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new ClayFireplace(), new StoneFireplace(), new MarbleFireplace()));
    }

}
