package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class FencingHotspot extends Hotspot {

    public FencingHotspot() {
        super(HotspotType.FENCING);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new BoundaryStones(), new WoodenFence(), new StoneWall(), new IronRailings(), new PicketFence(), new GardenFence(), new MarbleWall()));
    }
}
