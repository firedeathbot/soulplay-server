package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class ThroneRoom extends Room {

	private static final Palette palette = new Palette(8, -1);

	private static final boolean[] doors = {false, false, true, false};

	public ThroneRoom(int x, int y, int z) {
		super(RoomType.THRONE, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.THRONE, new RoomObject(15426, Location.create(3, 6), 0, 10));
		hotspots.put(HotspotType.THRONE, new RoomObject(15426, Location.create(4, 6), 0, 10));
		hotspots.put(HotspotType.DECORATION, new RoomObject(15434, Location.create(3, 7), 1, 5));
		hotspots.put(HotspotType.DECORATION, new RoomObject(15434, Location.create(4, 7), 1, 5));
		hotspots.put(HotspotType.LEVER, new RoomObject(15435, Location.create(6, 6), 0, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15436, Location.create(0, 0), 3, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15436, Location.create(0, 1), 3, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15436, Location.create(0, 2), 3, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15436, Location.create(0, 3), 3, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15436, Location.create(0, 4), 3, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15436, Location.create(0, 5), 3, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15437, Location.create(7, 0), 1, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15437, Location.create(7, 1), 1, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15437, Location.create(7, 2), 1, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15437, Location.create(7, 3), 1, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15437, Location.create(7, 4), 1, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15437, Location.create(7, 5), 1, 10));
		hotspots.put(HotspotType.TRAPDOOR, new RoomObject(15438, Location.create(1, 6), 0, 22));

		hotspots.put(HotspotType.FLOOR, new RoomObject(15432, Location.create(3, 4), 0, 22));
		hotspots.put(HotspotType.FLOOR, new RoomObject(15432, Location.create(4, 4), 0, 22));
		hotspots.put(HotspotType.FLOOR, new RoomObject(15432, Location.create(3, 3), 0, 22));
		hotspots.put(HotspotType.FLOOR, new RoomObject(15432, Location.create(4, 3), 0, 22));
	}

}
