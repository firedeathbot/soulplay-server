package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class GlobeHotspot extends Hotspot {

    public GlobeHotspot() {
        super(HotspotType.GLOBE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Globe(), new OrnamentalGlobe(), new LunarGlobe(), new CelestialGlobe(), new ArmillarySphere(), new SmallOrrery(), new LargeOrrery()));
    }
}
