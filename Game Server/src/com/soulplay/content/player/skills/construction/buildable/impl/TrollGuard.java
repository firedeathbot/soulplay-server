package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class TrollGuard extends Buildable {
    @Override
    public String getName() {
        return "Troll guard";
    }

    @Override
    public int getItemId() {
        return 8136;
    }

    @Override
    public int getObjectId() {
        return 13369;
    }

    @Override
    public int getLevelRequired() {
        return 90;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(995, 1_000_000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 1000;
    }
}
