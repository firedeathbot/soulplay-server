package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class MagicalBalance3 extends Buildable {
    @Override
    public String getName() {
        return "Magical balance 3";
    }

    @Override
    public int getItemId() {
        return 8158;
    }

    @Override
    public int getObjectId() {
        return 13397;
    }

    @Override
    public int getLevelRequired() {
        return 77;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(556, 2000), new Item(557, 2000), new Item(554, 2000), new Item(555, 2000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 356;
    }

    @Override
    protected List<String> getMaterialDescriptionLines() {
        return Arrays.asList("air, eath, fire, water rune x2000");
    }

}
