package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

public class WoodenBookcase extends Buildable {

    @Override
    public String getName() {
	return "Wooden Bookcase";
    }

    @Override
    public int getItemId() {
	return 8319;
    }

    @Override
    public int getObjectId() {
	return 13597;
    }

    @Override
    public int getLevelRequired() {
	return 4;
    }
    
    @Override
    public List<Item> getMaterialsNeeded() {
	return Arrays.asList(new Item(ConstructionItems.PLANK, 4));
    }

    @Override
    public int getRequiredNails() {
        return 4;
    }
    
    @Override
    public List<Item> getRequiredItems() {
	return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
	return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
	return 115;
    }

}
