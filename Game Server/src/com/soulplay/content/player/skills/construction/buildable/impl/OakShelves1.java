package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class OakShelves1 extends Buildable {
    @Override
    public String getName() {
        return "Oak shelves 1";
    }

    @Override
    public int getItemId() {
        return 8226;
    }

    @Override
    public int getObjectId() {
        return 13548;
    }

    @Override
    public int getLevelRequired() {
        return 34;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 3), new Item(ConstructionItems.SOFT_CLAY, 6));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 240;
    }
}
