package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

public class MahoganyBookcase extends Buildable {

    @Override
    public String getName() {
	return "MahoganyBookcase";
    }

    @Override
    public int getItemId() {
	return 8321;
    }

    @Override
    public int getObjectId() {
	return 13599;
    }

    @Override
    public int getLevelRequired() {
	return 40;
    }
    
    @Override
    public List<Item> getMaterialsNeeded() {
	return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 3));
    }
    
    @Override
    public List<Item> getRequiredItems() {
	return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
	return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
	return 420;
    }

}
