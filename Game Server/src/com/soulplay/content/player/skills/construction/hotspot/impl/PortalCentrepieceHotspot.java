package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.GreaterTeleportFocus;
import com.soulplay.content.player.skills.construction.buildable.impl.ScryingPool;
import com.soulplay.content.player.skills.construction.buildable.impl.TeleportFocus;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class PortalCentrepieceHotspot extends Hotspot {

    public PortalCentrepieceHotspot() {
        super(HotspotType.PORTAL_CENTERPIECE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new TeleportFocus(), new GreaterTeleportFocus(), new ScryingPool()));
    }
}
