package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyLever;
import com.soulplay.content.player.skills.construction.buildable.impl.OakLever;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakLever;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LeverHotspot extends Hotspot {

    public LeverHotspot() {
        super(HotspotType.LEVER);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakLever(), new TeakLever(), new MahoganyLever()));
    }

}
