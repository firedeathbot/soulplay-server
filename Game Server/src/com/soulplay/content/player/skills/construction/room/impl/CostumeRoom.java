package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class CostumeRoom extends Room {

	private static final Palette palette = new Palette(8, -5);

	private static final boolean[] doors = {false, false, true, false};

	public CostumeRoom(int x, int y, int z) {
		super(RoomType.COSTUME, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.MAGIC_WARDROBE, new RoomObject(18811, Location.create(3, 7), 1, 10));
		hotspots.put(HotspotType.ARMOR_CASE, new RoomObject(18815, Location.create(2, 7), 1, 10));
		hotspots.put(HotspotType.CAPE_RACK, new RoomObject(18810, Location.create(6, 6), 0, 10));
		hotspots.put(HotspotType.TOY_BOX, new RoomObject(18812, Location.create(7, 3), 1, 10));
		hotspots.put(HotspotType.FANCY_DRESS_BOX, new RoomObject(18814, Location.create(3, 3), 0, 10));
		hotspots.put(HotspotType.TREASURE_CHEST, new RoomObject(18813, Location.create(0, 3), 3, 10));
	}

}
