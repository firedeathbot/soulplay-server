package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DungeonTrapHotspot extends Hotspot {

    public DungeonTrapHotspot() {
        super(HotspotType.DUNGEON_TRAP);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new SpikeTrap(), new ManTrap(), new TangleVine(), new MarbleTrap(), new TeleportTrap()));
    }

}
