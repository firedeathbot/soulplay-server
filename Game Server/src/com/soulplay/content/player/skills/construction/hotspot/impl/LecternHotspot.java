package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LecternHotspot extends Hotspot {

    public LecternHotspot() {
        super(HotspotType.LECTERN);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakLectern(), new EagleLectern(), new DemonLectern(), new TeakEagleLectern(), new TeakDemonLectern(), new MahoganyEagleLectern(), new MahoganyDemonLectern()));
    }

}
