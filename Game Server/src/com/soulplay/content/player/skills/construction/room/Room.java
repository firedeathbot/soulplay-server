package com.soulplay.content.player.skills.construction.room;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotFactory;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.objects.InteractionMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public abstract class Room {
    
    private int x, y, z;
    
    private final Multimap<Integer, RoomObject> tempHotspots = ArrayListMultimap.create();    
    
    private final Multimap<HotspotType, RoomObject> objects = ArrayListMultimap.create();
    
    private final List<GroundItem> items = new ArrayList<GroundItem>();
    
    // Stores location of every single object from original including hotspots. They are also rotated when we place room down.
    private final Multimap<HotspotType, RoomObject> staticRoomObjects = ArrayListMultimap.create();
    
	private final RoomType type;
    
    private Location location;
    
    private final Palette palette;
    
    private boolean hasExitPortal;
    
    private RoomObject stairs;
    
    private RoomObject stairsHotspot;
    
    public Room(RoomType type, int x, int y, int z) {
	this.type = type;
	this.x = x;
	this.y = y;
	this.z = z;
	this.location = Location.create(x, y, z);
	this.palette = getStaticPalette().copy();
    }
    
    // NORTH EAST SOUTH WEST
    public abstract boolean[] hasDoors();
    
    public abstract Multimap<HotspotType, RoomObject> getHotspots();
    
    public abstract Palette getStaticPalette();
    
    public Palette getPalette() {
    	return palette;
    }

	public boolean canRotate(Direction playerDirection, boolean clockwise) {
		final int currR = this.getRotation();

		if (playerDirection == Direction.NORTH) {
			if (currR == 0) {
				return clockwise ? hasDoors()[1] : hasDoors()[3];
			} else if (currR == 1) {
				return clockwise ? hasDoors()[0] : hasDoors()[2];
			} else if (currR == 2) {
				return clockwise ? hasDoors()[3] : hasDoors()[1];
			} else if (currR == 3) {
				return clockwise ? hasDoors()[2] : hasDoors()[0];
			}
		} else if (playerDirection == Direction.EAST) {
			if (currR == 0) {
				return clockwise ? hasDoors()[2] : hasDoors()[0];
			} else if (currR == 1) {
				return clockwise ? hasDoors()[1] : hasDoors()[3];
			} else if (currR == 2) {
				return clockwise ? hasDoors()[0] : hasDoors()[2];
			} else if (currR == 3) {
				return clockwise ? hasDoors()[3] : hasDoors()[1];
			}
		} else if (playerDirection == Direction.SOUTH) {
			if (currR == 0) {
				return clockwise ? hasDoors()[0] : hasDoors()[2];
			} else if (currR == 1) {
				return clockwise ? hasDoors()[2] : hasDoors()[0];
			} else if (currR == 2) {
				return clockwise ? hasDoors()[1] : hasDoors()[3];
			} else if (currR == 3) {
				return clockwise ? hasDoors()[1] : hasDoors()[3];
			}
		} else if (playerDirection == Direction.WEST) {
			if (currR == 0) {
				return clockwise ? hasDoors()[1] : hasDoors()[3];
			} else if (currR == 1) {
				return clockwise ? hasDoors()[3] : hasDoors()[1];
			} else if (currR == 2) {
				return clockwise ? hasDoors()[2] : hasDoors()[0];
			} else if (currR == 3) {
				return clockwise ? hasDoors()[1] : hasDoors()[3];
			}
		}
			return false;
	}

	public boolean hasExit(Direction directionToCheck) {
		if (type == RoomType.EMPTY)
			return true;
		else if (type == RoomType.EMPTY_DUNG)
			return false;
		int rotation = 0; // default is north
		if (directionToCheck == Direction.EAST) {
			rotation = 1;
		} else if (directionToCheck == Direction.SOUTH) {
			rotation = 2;
		} else if (directionToCheck == Direction.WEST) {
			rotation = 3;
		}
		return hasDoors()[(rotation - getRotation())&3];
	}
	
	public void rotate(Player c, int direction) {
		
		removeBuiltObjects(c);
		
		if (!c.getPOH().canPlaceRoom(this, (getRotation()+direction)&3, direction == -1 ? false : true)) {
			c.sendMessage("The room cannot be rotated anymore.");
			return;
		}
		
		
		Construction.rebuildClipsInSurroundingRooms(c, getLocation()); //c.getPohClipMap().clipAllObjects(getObjectsArray());
	
		
		c.getPacketSender().sendConstructMapRegionPacket(c.getPOH().getCopyRooms(), c.getPOH().getHouseStyle());
		
		displayBuiltObjects(c, true);
	}
    
    public Multimap<HotspotType, RoomObject> getObjects() {
	return objects;
    }   
    
    public Multimap<Integer, RoomObject> getTempHotspots() {
	return tempHotspots;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getRotation() {
    	return getPalette().getRotation();
    }
    
    public RoomType getType() {
        return type;
    }
    
    public boolean objectExists(int objectId) {
	for (GameObject o : objects.values()) {
	    if (o.getId() == objectId) {
		return true;
	    }
	}
	return false;
    }
    
    public int getX() {
	return x;
    }
    
    public int getY() {
	return y;
    }

	public int getZ() {
		return z;
	}

	public Multimap<HotspotType, RoomObject> getStaticRoomObjects() {
		return staticRoomObjects;
	}
	
	public void rotateObjectsArray(Player owner, Room room, int rotation) {
		for (GameObject o : staticRoomObjects.values()) {
			o.setOrientation((o.getOrientation() + rotation) & 3);
			Location locOff = ConstructionUtils.getRotatedOffset(o.getLocation(), o.getSizeX(), o.getSizeY(), -1, rotation);
			Location newLoc = Location.create(room.getX()*8+locOff.getX(), room.getY()*8+locOff.getY(), o.getLocation().getZ());
			o.setLocation(newLoc);
		}
	}

	public void addStaticRoomObject(RoomObject o) {
		staticRoomObjects.put(o.getHotspotType(), o);
		if (o.getHotspotType() == HotspotType.STAIR)
			setStairsHotspot(o);
	}

    @Override
    public String toString() {
	return type.name().toLowerCase();
    }

	public boolean displayBuiltObjects(Player guest, boolean rotate) {
		boolean drawObjects = false;
		for (RoomObject o : getObjects().values()) {
			removeColliders(guest, o);
			guest.getPacketSender().addObjectPacket(o);
			drawObjects = true;
		}
		return drawObjects;
	}
	
	public void displayDroppedItems(Player guest) {
		for (GroundItem i : getItems()) {
			guest.getItems().createGroundItemPacket(i.getItemId(), i.getItemX(), i.getItemY(), i.getItemAmount());
		}
	}

	private void addColliders(Player player, RoomObject roomObject) {
        if (!player.getPOH().isBuildMode()) {
            return;
        }

        HotspotType hotspotType = roomObject.getHotspotType();
        Preconditions.checkNotNull(hotspotType);

        Hotspot hotspot = HotspotFactory.getHotspot(hotspotType);
        Preconditions.checkNotNull(hotspot);

        for (HotspotType colliderType : hotspot.getHotspotColliders()) {
            Collection<RoomObject> colliders = getHotspots().get(colliderType);

            if (colliders == null) {
                continue;
            }

            for (RoomObject collider : colliders) {
                Preconditions.checkNotNull(collider);

                Location corner = getCorner();

                RoomObject temp = new RoomObject(collider.getHotspotId(), Location.create(corner.getX() + collider.getLocation().getX(), corner.getY() + collider.getLocation().getY(), location.getZ()), colliderType == HotspotType.RUG ?  collider.getOrientation() : getRotation(), collider.getType());
                player.getPacketSender().addObjectPacket(temp);
            }
        }
    }

    public void removeColliders(Player player) {
        if (!player.getPOH().isBuildMode()) {
            return;
        }

        for (RoomObject roomObject : getObjects().values()) {
            HotspotType hotspotType = roomObject.getHotspotType();

            Hotspot hotspot = HotspotFactory.getHotspot(hotspotType);
            if (hotspot == null) {
            	return;
			}

            for (HotspotType colliderType : hotspot.getHotspotColliders()) {

                Collection<RoomObject> colliders = getHotspots().get(colliderType);

                if (colliders == null) {
                    continue;
                }

                for (RoomObject collider : colliders) {
                	if (collider == null) {
                		continue;
					}

                    Location corner = getCorner();
                    RoomObject temp = new RoomObject(-1, Location.create(corner.getX() + collider.getLocation().getX(), corner.getY() + collider.getLocation().getY(), location.getZ()), collider.getOrientation(), collider.getType());
                    player.getPacketSender().addObjectPacket(temp);
                }

            }
        }
    }

	private void removeColliders(Player player, RoomObject roomObject) {
    		if (!player.getPOH().isBuildMode()) {
    			return;
			}

			HotspotType hotspotType = roomObject.getHotspotType();
			Hotspot hotspot = HotspotFactory.getHotspot(hotspotType);

			if (hotspot == null) {
				return;
			}

			for (HotspotType colliderType : hotspot.getHotspotColliders()) {

				Collection<RoomObject> colliders = getHotspots().get(colliderType);

				if (colliders == null) {
					continue;
				}

				for (RoomObject collider : colliders) {
					Preconditions.checkNotNull(collider);

					Location corner = getCorner();
					RoomObject temp = new RoomObject(-1, Location.create(corner.getX() + collider.getLocation().getX(), corner.getY() + collider.getLocation().getY(), location.getZ()), collider.getOrientation(), collider.getType());
					player.getPacketSender().addObjectPacket(temp);
				}

			}
	}
	
	public void clipBuiltObjects(Player owner) {
		for (RoomObject o : getObjects().values()) {
			owner.getPohClipMap().addObject(o);
		}
	}
	
	public void addBuiltObjectToMap(HotspotType type, RoomObject o) {
		getObjects().put(type, o);
        if (ConstructionUtils.isStairs(o.getId()))
        	setStairs(o);
        else if (o.getId() == 13405) {
			setHasExitPortal(true);
		}
        if (this.type == RoomType.THRONE && type == HotspotType.SEATING) { // throne room benches need direction masks reversed
        	o.setDirectionMask(InteractionMask.getOppositeMask(o.getDirectionMask()));
        }
	}

	public Location getCorner() {
    	return Location.create(location.getX() * 8, location.getY() * 8);
	}
	
	public void deleteBuiltObjectsFromMap(HotspotType type, int toDelete) {
		getObjects().asMap().remove(type);
		if (ConstructionUtils.isStairs(toDelete))
        	setStairs(null);
        else if (toDelete == 13405)
        	setHasExitPortal(false);
	}

	public void showTemphotspots(Player player) {
        for (RoomObject tempObject : getTempHotspots().values()) {
            addColliders(player, tempObject);
            player.getPacketSender().addObjectPacket(tempObject);
        }
    }
	
	public void removeBuiltObjects(Player owner) {
//		int count = 0;
//		for (RoomObject o : getObjects().values()) {
//			for (GameObject o2 : getObjectMap().get(o.getHotspotId())) {
//				owner.getPacketSender().removeObjectPacket(o2);
//				count++;
//			}
//		}
//		owner.sendMessage(String.format("removed %d amount of objects", count));
	}

	public boolean isDungeon() {
		return type == RoomType.DUNGEON_JUNCTION || type == RoomType.DUNGEON_CORRIDOR || type == RoomType.DUNGEON_STAIR_ROOM || type == RoomType.DUNGEON_OUBLIETTE || type == RoomType.DUNGEON_PIT || type == RoomType.DUNGEON_TREASURE_ROOM;
	}

	public boolean isHasExitPortal() {
		return hasExitPortal;
	}

	public void setHasExitPortal(boolean hasExitPortal) {
		this.hasExitPortal = hasExitPortal;
	}

	public static void copyRoom(Room roomToCopy, Room newRoom) {
    	newRoom.getPalette().rotate(roomToCopy.getRotation()); // add
		newRoom.tempHotspots.putAll(roomToCopy.tempHotspots);
		newRoom.objects.putAll(roomToCopy.objects);
		newRoom.staticRoomObjects.putAll(roomToCopy.staticRoomObjects);
		newRoom.hasExitPortal = roomToCopy.hasExitPortal;
		newRoom.stairs = roomToCopy.stairs;
		newRoom.stairsHotspot = roomToCopy.stairsHotspot;
		roomToCopy.nullRoom();
	}
	
	public RoomType getUpperRoomType() {
		return this.type;
	}
	
	public RoomType getLowerRoomType() {
		return this.type;
	}
	
	public RoomObject getStairs() {
		return stairs;
	}

	public void setStairs(RoomObject stairs) {
		this.stairs = stairs;
	}
	
	public boolean hasStairs() {
		return stairs != null;
	}
	
	public RoomObject getStairsHotspot() {
		return stairsHotspot;
	}

	public void setStairsHotspot(RoomObject stairsHotspot) {
		this.stairsHotspot = stairsHotspot;
	}
	
	public void nullRoom() {
		tempHotspots.clear();
		objects.clear();
		staticRoomObjects.clear();
		hasExitPortal = false;
		stairs = null;
		stairsHotspot = null;
	}

	public List<GroundItem> getItems() {
		return items;
	}

}
