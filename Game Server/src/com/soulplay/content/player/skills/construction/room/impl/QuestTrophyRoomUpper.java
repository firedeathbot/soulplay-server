package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class QuestTrophyRoomUpper extends Room {

	private static final Palette palette = new Palette(9, 0);

	private static final boolean[] doors = {true, true, true, true};

	public QuestTrophyRoomUpper(int x, int y, int z) {
		super(RoomType.QUEST_HALL_UPPER, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(2, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(2, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(2, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(2, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(3, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(3, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(4, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(4, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(5, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(5, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(5, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15387, Location.create(5, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(1, 2), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(1, 3), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(1, 4), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(1, 5), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(2, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(2, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(3, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(3, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(4, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(4, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(5, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(5, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(6, 2), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(6, 3), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(6, 4), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15388, Location.create(6, 5), 1, 22));
		hotspots.put(HotspotType.STAIR, new RoomObject(15391, Location.create(3, 3), 0, 10));
		hotspots.put(HotspotType.PORTRAIT, new RoomObject(15392, Location.create(1, 7), 1, 5));
		hotspots.put(HotspotType.LANDSCAPE, new RoomObject(15393, Location.create(6, 7), 1, 5));
		hotspots.put(HotspotType.GUILD_TROPHY, new RoomObject(15394, Location.create(0, 6), 0, 5));
		hotspots.put(HotspotType.SWORD, new RoomObject(15395, Location.create(7, 6), 2, 5));
		hotspots.put(HotspotType.MAP, new RoomObject(15396, Location.create(7, 1), 2, 5));
		hotspots.put(HotspotType.BOOKCASE, new RoomObject(15397, Location.create(0, 1), 0, 10));
	}
	
	@Override
	public RoomType getUpperRoomType() {
		return getType();
	}
	
	@Override
	public RoomType getLowerRoomType() {
		return RoomType.QUEST_TROPHY;
	}

}
