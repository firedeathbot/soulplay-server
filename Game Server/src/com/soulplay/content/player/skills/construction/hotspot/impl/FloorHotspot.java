package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class FloorHotspot extends Hotspot {

    public FloorHotspot() {
        super(HotspotType.FLOOR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new FloorDecoration(), new SteelCage(), new Trapdoor(), new LesserMagicCage(), new GreaterMagicCage()));
    }
}
