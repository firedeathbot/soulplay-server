package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ParlourChair3Hotspot extends Hotspot {

    public ParlourChair3Hotspot() {
        super(HotspotType.PARLOUR_CHAIR_3);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new CrudeWoodenChair(), new WoodenChair(), new RockingChair(), new OakChair(), new OakArmchair(), new TeakArmchair(), new MahoganyArmchair()));
    }

}