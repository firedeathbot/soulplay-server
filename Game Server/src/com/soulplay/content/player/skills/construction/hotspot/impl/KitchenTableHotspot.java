package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.KitchenTable;
import com.soulplay.content.player.skills.construction.buildable.impl.OakKitchenTable;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakKitchenTable;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class KitchenTableHotspot extends Hotspot {

    public KitchenTableHotspot() {
        super(HotspotType.KITCHEN_TABLE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new KitchenTable(), new OakKitchenTable(), new TeakKitchenTable()));
    }

}
