package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Marblestaircase extends Buildable {
    @Override
    public String getName() {
        return "Marble staircase";
    }

    @Override
    public int getItemId() {
        return 8255;
    }

    @Override
    public int getObjectId() {
        return 13501;
    }
    
    @Override
    public int getObjectId(boolean upperRoom) {
    	return !upperRoom ? getObjectId() : 13502;
    }

    @Override
    public int getLevelRequired() {
        return 82;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 5), new Item(ConstructionItems.MARBLE_BLOCK, 5));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 3200;
    }
}
