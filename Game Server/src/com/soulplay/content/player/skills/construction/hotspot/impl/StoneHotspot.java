package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.ClayAttackStone;
import com.soulplay.content.player.skills.construction.buildable.impl.LimestoneAttackStone;
import com.soulplay.content.player.skills.construction.buildable.impl.MarbleAttackStone;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class StoneHotspot extends Hotspot {

    public StoneHotspot() {
        super(HotspotType.STONE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new ClayAttackStone(), new LimestoneAttackStone(), new MarbleAttackStone()));
    }

}
