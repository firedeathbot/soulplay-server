package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class GuardHotspot extends Hotspot {

    public GuardHotspot() {
        super(HotspotType.GUARD);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new SkeletonGuard(), new GuardDog(), new Hobgoblin(), new BabyRedDragon(), new HugeSpider(), new TrollGuard(), new Hellhound()));
    }

}
