package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.KiteShield;
import com.soulplay.content.player.skills.construction.buildable.impl.RoundShield;
import com.soulplay.content.player.skills.construction.buildable.impl.SquareShield;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DungeonDecoration2Hotspot extends Hotspot {

    public DungeonDecoration2Hotspot() {
        super(HotspotType.DUNGEON_DECORATION_2);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new RoundShield(), new SquareShield(), new KiteShield()));
    }

}
