package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MarbleDoor;
import com.soulplay.content.player.skills.construction.buildable.impl.OakDoor;
import com.soulplay.content.player.skills.construction.buildable.impl.SteelPlatedDoor;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DungeonDoorHotspot extends Hotspot {

    public DungeonDoorHotspot() {
        super(HotspotType.DUNGEON_DOOR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakDoor(), new SteelPlatedDoor(), new MarbleDoor()));
    }

}
