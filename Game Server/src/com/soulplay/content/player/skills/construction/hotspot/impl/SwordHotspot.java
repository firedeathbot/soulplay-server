package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Darklight;
import com.soulplay.content.player.skills.construction.buildable.impl.Excalibur;
import com.soulplay.content.player.skills.construction.buildable.impl.Silverlight;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class SwordHotspot extends Hotspot {

    public SwordHotspot() {
        super(HotspotType.SWORD);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Silverlight(), new Excalibur(), new Darklight()));
    }

}
