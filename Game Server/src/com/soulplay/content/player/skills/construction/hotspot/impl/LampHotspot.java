package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LampHotspot extends Hotspot {

    public LampHotspot() {
        super(HotspotType.LAMP);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new SteelTorch(), new WoodenTorch(), new SteelCandlestick(), new GoldCandlestick(), new IncenseBurner(), new MahoganyBurner(), new MarbleBurner()));
    }
}
