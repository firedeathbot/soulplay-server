package com.soulplay.content.player.skills.construction;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.Client;

public enum StorageObjectEnum {

	TREASURE_CHEST(0, "Treasure Chest", 14484) {
		
		@Override
		public void setList(Client c) {
			c.getStorageObject().listRef = c.getPOH().getTreasureChestItems();
		}
	},
	FANCY_DRESS_BOX(1, "Fancy Dress Box", 6950) {
		
		@Override
		public void setList(Client c) {
			c.getStorageObject().listRef = c.getPOH().getFancyDressBox();
		}
	},
	ARMOR_CASE(2, "Armor Case", 20135) {
		
		@Override
		public void setList(Client c) {
			c.getStorageObject().listRef = c.getPOH().getArmorCase();
		}
	},
	MAGIC_WARDROBE(3, "Magic Wardrobe", 13864) {
		
		@Override
		public void setList(Client c) {
			c.getStorageObject().listRef = c.getPOH().getMagicWardrobe();
		}
	},
	CAPE_RACK(4, "Cape Rack", 6570) {
		
		@Override
		public void setList(Client c) {
			c.getStorageObject().listRef = c.getPOH().getCapeRack();
		}
	},
	TOY_BOX(5, "Toy Box", 2520, 2522, 2524, 2526) {
		
		@Override
		public void setList(Client c) {
			c.getStorageObject().listRef = c.getPOH().getToyBox();
		}
	}
	;
	
	private final int id;
	private final String interfaceName;
	private final int[] allowedItems;
	
	private StorageObjectEnum(int id, String interfaceName, int... allowedItems) {
		this.id = id;
		this.interfaceName = interfaceName;
		this.allowedItems = allowedItems;
	}
	
	public void setList(Client c) {
	}
	
	public int getMaxSize() {
		return allowedItems.length;
	}
	
	private static final Map<Integer, StorageObjectEnum> map = new HashMap<>();
	
	static {
		for (StorageObjectEnum obj : StorageObjectEnum.values()) {
			for (int itemId : obj.allowedItems) {
				map.put(itemId, obj);
			}
		}
	}
	
    public static void load() {
    	/* empty */
    }
    
	public static StorageObjectEnum getStorage(int itemId) {
		return map.get(itemId);
	}

	public int getId() {
		return id;
	}

	public String getInterfaceName() {
		return interfaceName;
	}
}
