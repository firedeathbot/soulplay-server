package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class MagicalBalance1 extends Buildable {
    @Override
    public String getName() {
        return "Magical balance 1";
    }

    @Override
    public int getItemId() {
        return 8156;
    }

    @Override
    public int getObjectId() {
        return 13395;
    }

    @Override
    public int getLevelRequired() {
        return 37;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(556, 500), new Item(557, 500), new Item(554, 500), new Item(555, 500));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 176;
    }

    @Override
    protected List<String> getMaterialDescriptionLines() {
        return Arrays.asList("air, eath, fire, water rune x500");
    }

}
