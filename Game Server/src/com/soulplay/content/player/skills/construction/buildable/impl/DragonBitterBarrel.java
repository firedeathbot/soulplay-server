package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class DragonBitterBarrel extends Buildable {
    @Override
    public String getName() {
        return "Dragon Bitter barrel";
    }

    @Override
    public int getItemId() {
        return 8243;
    }

    @Override
    public int getObjectId() {
        return 13572;
    }

    @Override
    public int getLevelRequired() {
        return 36;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 3), new Item(1911, 8), new Item(ConstructionItems.STEEL_BAR, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 224;
    }
}
