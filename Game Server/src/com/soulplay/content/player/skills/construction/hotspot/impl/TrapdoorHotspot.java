package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyTrapdoor;
import com.soulplay.content.player.skills.construction.buildable.impl.OakTrapdoor;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakTrapdoor;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class TrapdoorHotspot extends Hotspot {

    public TrapdoorHotspot() {
        super(HotspotType.TRAPDOOR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakTrapdoor(), new TeakTrapdoor(), new MahoganyTrapdoor()));
    }
}
