package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class DungeonStairsRoom extends Room {

	private static final Palette palette = new Palette(4, -3);

	private static final boolean[] doors = {true, true, true, true};

	public DungeonStairsRoom(int x, int y, int z) {
		super(RoomType.DUNGEON_STAIR_ROOM, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.GUARD, new RoomObject(15336, Location.create(5, 5), 0, 10));
		hotspots.put(HotspotType.GUARD, new RoomObject(15337, Location.create(1, 1), 0, 10));

		hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(1, 6), 1, 5));
		hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(6, 1), 3, 5));

		hotspots.put(HotspotType.STAIR, new RoomObject(15380, Location.create(3, 3), 0, 10));

		// south
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15328, Location.create(4, 1), 0, 3));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15329, Location.create(3, 1), 0, 3));
		// north
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15329, Location.create(4, 6), 1, 0));
		hotspots.put(HotspotType.DUNGEON_DOOR, new RoomObject(15326, Location.create(3, 6), 1, 0));

		hotspots.put(HotspotType.LIGHT, new RoomObject(34138, Location.create(1, 5), 0, 5)); // nw w
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(2, 6), 1, 5)); // nw n
		hotspots.put(HotspotType.LIGHT, new RoomObject(34138, Location.create(5, 6), 1, 5)); // ne n
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(6, 5), 2, 5)); // ne e
		hotspots.put(HotspotType.LIGHT, new RoomObject(34138, Location.create(6, 5), 2, 5)); // se e
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(5, 1), 3, 5)); // se s
		hotspots.put(HotspotType.LIGHT, new RoomObject(34138, Location.create(5, 1), 3, 5)); // sw s
		hotspots.put(HotspotType.LIGHT, new RoomObject(15330, Location.create(1, 2), 0, 5)); // sw w

		// corners
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(2, 5), 0, 22)); // nw
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(5, 5), 1, 22)); // ne
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(5, 2), 2, 22)); // se
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(2, 2), 3, 22)); // sw

		// outside
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(3, 5), 0, 22)); // north
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(4, 5), 0, 22)); // north
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(2, 3), 3, 22)); // west
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(2, 4), 3, 22)); // west
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(5, 3), 1, 22)); // east
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(5, 4), 1, 22)); // east
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(3, 2), 2, 22)); // south
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(4, 2), 2, 22)); // south

		// inside
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(3, 4), 2, 22)); // nw
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(4, 4), 2, 22)); // ne
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(4, 3), 2, 22)); // se
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(3, 3), 2, 22)); // sw
	}
	
	@Override
	public RoomType getUpperRoomType() {
		return RoomType.SKILL_HALL_UPPER;
	}

}
