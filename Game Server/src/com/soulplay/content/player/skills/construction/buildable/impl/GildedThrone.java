package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class GildedThrone extends Buildable {
    @Override
    public String getName() {
        return "Gilded throne";
    }

    @Override
    public int getItemId() {
        return 8360;
    }

    @Override
    public int getObjectId() {
        return 13668;
    }

    @Override
    public int getLevelRequired() {
        return 81;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 5), new Item(ConstructionItems.MARBLE_BLOCK, 2), new Item(ConstructionItems.GOLD_LEAF));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 2200;
    }
}
