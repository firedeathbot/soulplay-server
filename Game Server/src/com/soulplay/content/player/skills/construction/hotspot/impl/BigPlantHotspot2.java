package com.soulplay.content.player.skills.construction.hotspot.impl;

import java.util.Arrays;
import com.soulplay.content.player.skills.construction.buildable.impl.HugePlant;
import com.soulplay.content.player.skills.construction.buildable.impl.LargeLeafBush;
import com.soulplay.content.player.skills.construction.buildable.impl.ShortPlant;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

public class BigPlantHotspot2 extends Hotspot {

    public BigPlantHotspot2() {
	super(HotspotType.BIG_PLANT_2);
    }

    @Override
    public void init() {
	buildables.addAll(Arrays.asList(new ShortPlant(), new LargeLeafBush(), new HugePlant()));	
    }

}
