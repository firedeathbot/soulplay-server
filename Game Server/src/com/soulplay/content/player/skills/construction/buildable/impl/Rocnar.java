package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.buildable.BuildType;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.world.entity.Location;

import java.util.Arrays;
import java.util.List;

public class Rocnar extends Buildable {

    @Override
    public BuildType getBuildType() {
        return BuildType.NON_RECURSIVE;
    }

    @Override
    public Location positionOverride() {
        return Location.create(3, 4);
    }

    @Override
    public String getName() {
        return "Rocnar";
    }

    @Override
    public int getItemId() {
        return 8305;
    }

    @Override
    public int getObjectId() {
        return 13373;
    }

    @Override
    public int getLevelRequired() {
        return 83;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(995, 150_000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return 0;
    }

    @Override
    public int getExperience() {
        return 387;
    }

}
