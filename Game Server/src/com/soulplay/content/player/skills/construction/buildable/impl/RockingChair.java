package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

public class RockingChair extends Buildable {

    @Override
    public String getName() {
	return "Rocking chair";
    }

    @Override
    public int getItemId() {
	return 8311;
    }

    @Override
    public int getObjectId() {
	return 13583;
    }

    @Override
    public int getLevelRequired() {
	return 14;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
	return Arrays.asList(new Item(ConstructionItems.PLANK, 3));
    }

    @Override
    public int getRequiredNails() {
        return 3;
    }
    
    @Override
    public List<Item> getRequiredItems() {
	return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
	return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
	return 87;
    }

}
