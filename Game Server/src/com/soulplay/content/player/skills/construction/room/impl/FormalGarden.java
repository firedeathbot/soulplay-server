package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class FormalGarden extends Room {

	private static final Palette palette = new Palette(4, -5);

	private static final boolean[] doors = {true, true, true, true};

	public FormalGarden(int x, int y, int z) {
		super(RoomType.FORMAL_GARDEN, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.FORMAL_CENTREPIECE, new RoomObject(15368, Location.create(3, 3), 0, 10));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(0, 0), 3, 2));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(0, 1), 0, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(0, 2), 0, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(0, 5), 0, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(0, 6), 0, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(0, 7), 0, 2));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(1, 0), 3, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(1, 7), 1, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(2, 0), 3, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(2, 7), 1, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(5, 0), 3, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(5, 7), 1, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(6, 0), 3, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(6, 7), 1, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(7, 0), 2, 2));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(7, 1), 2, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(7, 2), 2, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(7, 5), 2, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(7, 6), 2, 0));
		hotspots.put(HotspotType.FENCING, new RoomObject(15369, Location.create(7, 7), 1, 2));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(0, 2), 3, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(0, 5), 1, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(2, 0), 0, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(2, 7), 0, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(5, 0), 2, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(5, 7), 2, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(7, 2), 3, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15370, Location.create(7, 5), 1, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(0, 1), 3, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(0, 6), 3, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(1, 0), 2, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(1, 7), 0, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(6, 0), 2, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(6, 7), 0, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(7, 1), 1, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15371, Location.create(7, 6), 1, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15372, Location.create(0, 0), 3, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15372, Location.create(0, 7), 0, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15372, Location.create(7, 0), 2, 10));
		hotspots.put(HotspotType.HEDGING, new RoomObject(15372, Location.create(7, 7), 1, 10));
		hotspots.put(HotspotType.BIG_PLANT, new RoomObject(15373, Location.create(1, 6), 0, 10));
		hotspots.put(HotspotType.BIG_PLANT, new RoomObject(15373, Location.create(6, 1), 2, 10));
		hotspots.put(HotspotType.FORMAL_BIG_PLANT_2, new RoomObject(15374, Location.create(1, 1), 3, 10));
		hotspots.put(HotspotType.FORMAL_BIG_PLANT_2, new RoomObject(15374, Location.create(6, 6), 1, 10));
		hotspots.put(HotspotType.SMALL_PLANT, new RoomObject(15375, Location.create(1, 5), 0, 10));
		hotspots.put(HotspotType.SMALL_PLANT, new RoomObject(15375, Location.create(2, 6), 0, 10));
		hotspots.put(HotspotType.SMALL_PLANT, new RoomObject(15375, Location.create(5, 1), 2, 10));
		hotspots.put(HotspotType.SMALL_PLANT, new RoomObject(15375, Location.create(6, 2), 2, 10));
		hotspots.put(HotspotType.FORMAL_SMALL_PLANT_2, new RoomObject(15376, Location.create(1, 2), 3, 10));
		hotspots.put(HotspotType.FORMAL_SMALL_PLANT_2, new RoomObject(15376, Location.create(2, 1), 3, 10));
		hotspots.put(HotspotType.FORMAL_SMALL_PLANT_2, new RoomObject(15376, Location.create(5, 6), 1, 10));
		hotspots.put(HotspotType.FORMAL_SMALL_PLANT_2, new RoomObject(15376, Location.create(6, 5), 1, 10));
	}

}
