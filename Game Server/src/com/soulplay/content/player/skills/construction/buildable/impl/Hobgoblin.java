package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Hobgoblin extends Buildable {
    @Override
    public String getName() {
        return "Hobgoblin";
    }

    @Override
    public int getItemId() {
        return 8133;
    }

    @Override
    public int getObjectId() {
        return 13368;
    }

    @Override
    public int getLevelRequired() {
        return 78;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(995, 100_000));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 316;
    }
}
