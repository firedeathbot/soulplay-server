package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

public class MarbleFireplace extends Buildable {

    @Override
    public String getName() {
	return "Marble Fireplace";
    }

    @Override
    public int getItemId() {
	return 8327;
    }

    @Override
    public int getObjectId() {
	return 13613;
    }

    @Override
    public int getLevelRequired() {
	return 63;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
	return Arrays.asList(new Item(ConstructionItems.MARBLE_BLOCK));
    }

    @Override
    public List<Item> getRequiredItems() {
	return Arrays.asList();
    }

    @Override
    public int getAnimation() {
	return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
	return 500;
    }

}