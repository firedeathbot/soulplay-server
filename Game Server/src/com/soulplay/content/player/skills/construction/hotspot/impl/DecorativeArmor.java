package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.BasicDecorativeArmor;
import com.soulplay.content.player.skills.construction.buildable.impl.DetailedDecorativeArmor;
import com.soulplay.content.player.skills.construction.buildable.impl.IntricateDecorativeArmor;
import com.soulplay.content.player.skills.construction.buildable.impl.ProfoundDecorativeArmor;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class DecorativeArmor extends Hotspot {

    public DecorativeArmor() {
        super(HotspotType.DECORATIVE_ARMOR);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new BasicDecorativeArmor(), new DetailedDecorativeArmor(), new IntricateDecorativeArmor(), new ProfoundDecorativeArmor()));
    }
}
