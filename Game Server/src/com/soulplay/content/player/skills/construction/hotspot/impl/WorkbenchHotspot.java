package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class WorkbenchHotspot extends Hotspot {

    public WorkbenchHotspot() {
        super(HotspotType.WORKBENCH);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenWorkbench(), new OakWorkbench(), new SteelFramedWorkbench(), new BenchWithVice(), new BenchWithLathe()));
    }
}
