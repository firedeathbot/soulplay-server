package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class BenchWithVice extends Buildable {
    @Override
    public String getName() {
        return "Bench with a vice";
    }

    @Override
    public int getItemId() {
        return 8378;
    }

    @Override
    public int getObjectId() {
        return 13707;
    }

    @Override
    public int getLevelRequired() {
        return 62;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(8377), new Item(ConstructionItems.OAK_PLANK, 2), new Item(ConstructionItems.STEEL_BAR));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 140;
    }
}
