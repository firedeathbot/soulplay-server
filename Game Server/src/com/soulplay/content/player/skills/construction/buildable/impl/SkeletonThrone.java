package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class SkeletonThrone extends Buildable {
    @Override
    public String getName() {
        return "Skeleton throne";
    }

    @Override
    public int getItemId() {
        return 8361;
    }

    @Override
    public int getObjectId() {
        return 13669;
    }

    @Override
    public int getLevelRequired() {
        return 88;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAGIC_STONE, 5), new Item(ConstructionItems.MARBLE_BLOCK, 4), new Item(526, 5), new Item(964, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 7003;
    }
}
