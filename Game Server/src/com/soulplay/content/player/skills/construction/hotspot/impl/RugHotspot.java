package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.BrownRug;
import com.soulplay.content.player.skills.construction.buildable.impl.OpulentRug;
import com.soulplay.content.player.skills.construction.buildable.impl.Rug;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class RugHotspot extends Hotspot {

    public RugHotspot() {
        super(HotspotType.RUG);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new BrownRug(), new Rug(), new OpulentRug()));
        colliders.add(HotspotType.STAIR);
    }

}
