package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class ShavingStand extends Buildable {
    @Override
    public String getName() {
        return "Shaving stand";
    }

    @Override
    public int getItemId() {
        return 8045;
    }

    @Override
    public int getObjectId() {
        return 13162;
    }

    @Override
    public int getLevelRequired() {
        return 21;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.PLANK), new Item(ConstructionItems.MOLTEN_GLASS));
    }

    @Override
    public int getRequiredNails() {
        return 1;
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 30;
    }
}
