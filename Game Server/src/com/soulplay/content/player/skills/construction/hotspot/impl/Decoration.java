package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.GildedDecoration;
import com.soulplay.content.player.skills.construction.buildable.impl.OakDecoration;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakDecoration;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class Decoration extends Hotspot {

    public Decoration() {
        super(HotspotType.DECORATION);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakDecoration(), new TeakDecoration(), new GildedDecoration()));
    }
}
