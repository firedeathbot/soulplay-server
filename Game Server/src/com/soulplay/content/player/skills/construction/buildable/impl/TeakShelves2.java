package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class TeakShelves2 extends Buildable {
    @Override
    public String getName() {
        return "Teak shelves 2";
    }

    @Override
    public int getItemId() {
        return 8229;
    }

    @Override
    public int getObjectId() {
        return 13551;
    }

    @Override
    public int getLevelRequired() {
        return 67;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK, 3), new Item(ConstructionItems.SOFT_CLAY, 6), new Item(ConstructionItems.GOLD_LEAF, 2));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 930;
    }
}
