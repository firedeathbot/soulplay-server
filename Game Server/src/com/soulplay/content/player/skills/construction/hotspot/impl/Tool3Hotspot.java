package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class Tool3Hotspot extends Hotspot {
    public Tool3Hotspot() {
        super(HotspotType.TOOL_3);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new ToolStore1(), new ToolStore2(), new ToolStore3(), new ToolStore4(), new ToolStore5()));
    }
}
