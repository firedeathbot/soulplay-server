package com.soulplay.content.player.skills.construction.buildable.impl;

import java.util.Arrays;
import java.util.List;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

public class ClayFireplace extends Buildable {

    @Override
    public String getName() {
	return "Clay Fireplace";
    }

    @Override
    public int getItemId() {
	return 8325;
    }

    @Override
    public int getObjectId() {
	return 13609;
    }

    @Override
    public int getLevelRequired() {
	return 3;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
	return Arrays.asList(new Item(ConstructionItems.SOFT_CLAY, 3));
    }

    @Override
    public List<Item> getRequiredItems() {
	return Arrays.asList();
    }

    @Override
    public int getAnimation() {
	return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
	return 30;
    }

}
