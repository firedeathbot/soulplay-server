package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.ExtraWeaponRack;
import com.soulplay.content.player.skills.construction.buildable.impl.GloveRack;
import com.soulplay.content.player.skills.construction.buildable.impl.WeaponRack;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class StorageHotspot extends Hotspot {

    public StorageHotspot() {
        super(HotspotType.STORAGE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new GloveRack(), new WeaponRack(), new ExtraWeaponRack()));
    }

}
