package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class HangingSkeleton extends Buildable {
    @Override
    public String getName() {
        return "Hanging skeleton";
    }

    @Override
    public int getItemId() {
        return 8127;
    }

    @Override
    public int getObjectId() {
        return 13310;
    }

    @Override
    public int getLevelRequired() {
        return 94;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.SKULLS, 2), new Item(526, 6));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList();
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 3;
    }
}
