package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class ChapelRoom extends Room {

	private static final Palette palette = new Palette(4, -1);

	private static final boolean[] doors = {false, false, true, true};

	public ChapelRoom(int x, int y, int z) {
		super(RoomType.CHAPEL, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {

		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.WINDOW_SPACE, new RoomObject(13733, Location.create(7, 2), 2, 0));
		hotspots.put(HotspotType.WINDOW_SPACE, new RoomObject(13733, Location.create(7, 5), 2, 0));
		hotspots.put(HotspotType.WINDOW_SPACE, new RoomObject(13733, Location.create(5, 7), 1, 0));
		hotspots.put(HotspotType.WINDOW_SPACE, new RoomObject(13733, Location.create(2, 7), 1, 0));
		hotspots.put(HotspotType.WINDOW_SPACE, new RoomObject(13733, Location.create(0, 5), 0, 0));
		hotspots.put(HotspotType.WINDOW_SPACE, new RoomObject(13733, Location.create(0, 2), 0, 0));
		hotspots.put(HotspotType.ICON, new RoomObject(15269, Location.create(3, 7), 0, 10));
		hotspots.put(HotspotType.ALTAR, new RoomObject(15270, Location.create(3, 5), 0, 10));
		hotspots.put(HotspotType.LAMP, new RoomObject(15271, Location.create(1, 5), 0, 10));
		hotspots.put(HotspotType.LAMP, new RoomObject(15271, Location.create(6, 5), 0, 10));
		hotspots.put(HotspotType.RUG, new RoomObject(15273, Location.create(3, 1), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15273, Location.create(3, 2), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15273, Location.create(3, 3), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15273, Location.create(4, 1), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15273, Location.create(4, 2), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15273, Location.create(4, 3), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15274, Location.create(3, 0), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15274, Location.create(3, 4), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15274, Location.create(4, 0), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15274, Location.create(4, 4), 1, 22));
		hotspots.put(HotspotType.STATUE, new RoomObject(15275, Location.create(0, 0), 2, 11));
		hotspots.put(HotspotType.STATUE, new RoomObject(15275, Location.create(7, 0), 1, 11));
		hotspots.put(HotspotType.MUSICAL, new RoomObject(15276, Location.create(7, 3), 1, 10));
	}

}
