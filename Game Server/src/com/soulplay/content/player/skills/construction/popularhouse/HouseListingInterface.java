package com.soulplay.content.player.skills.construction.popularhouse;

public enum HouseListingInterface {
	BUTTON1(66054, 66055),
	BUTTON2(66059, 66060),
	BUTTON3(66064, 66065),
	BUTTON4(66069, 66070),
	BUTTON5(66074, 66075),
	BUTTON6(66079, 66080),
	BUTTON7(66084, 66085),
	BUTTON8(66089, 66090),
	BUTTON9(66094, 66095),
	BUTTON10(66099, 66100);
	
	private final int amountId;
	private final int textId;
	
	private HouseListingInterface(int amount, int text) {
		this.amountId = amount;
		this.textId = text;
	}

	public int getAmountId() {
		return amountId;
	}

	public int getTextId() {
		return textId;
	}

	public static final HouseListingInterface[] values = HouseListingInterface.values();
}
