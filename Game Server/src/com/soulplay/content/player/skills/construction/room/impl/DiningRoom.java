package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class DiningRoom extends Room {

	private static final Palette palette = new Palette(6, 1);

	private static final boolean[] doors = {false, true, true, true};

	public DiningRoom(int x, int y, int z) {
		super(RoomType.DINING, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {	
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.DINING_TABLE, new RoomObject(15298, Location.create(2, 3), 0, 10));

		hotspots.put(HotspotType.SEATING, new RoomObject(15299, Location.create(2, 5), 0, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15299, Location.create(3, 5), 0, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15299, Location.create(4, 5), 0, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15299, Location.create(5, 5), 0, 10));

		hotspots.put(HotspotType.SEATING, new RoomObject(15300, Location.create(2, 2), 2, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15300, Location.create(3, 2), 2, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15300, Location.create(4, 2), 2, 10));
		hotspots.put(HotspotType.SEATING, new RoomObject(15300, Location.create(5, 2), 2, 10));

		hotspots.put(HotspotType.BELL_PULL, new RoomObject(15304, Location.create(0, 0), 2, 10));

		hotspots.put(HotspotType.DECORATION, new RoomObject(15303, Location.create(2, 7), 1, 5));
		hotspots.put(HotspotType.DECORATION, new RoomObject(15303, Location.create(5, 7), 1, 5));

		hotspots.put(HotspotType.CURTAIN, new RoomObject(15302, Location.create(0, 2), 0, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15302, Location.create(0, 5), 0, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15302, Location.create(7, 2), 2, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15302, Location.create(7, 5), 2, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15302, Location.create(2, 0), 3, 5));
		hotspots.put(HotspotType.CURTAIN, new RoomObject(15302, Location.create(5, 0), 3, 5));

		hotspots.put(HotspotType.FIREPLACE, new RoomObject(15301, Location.create(3, 7), 1, 10));
	}

}
