package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Bells;
import com.soulplay.content.player.skills.construction.buildable.impl.Organ;
import com.soulplay.content.player.skills.construction.buildable.impl.Windchimes;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class MusicalHotspot extends Hotspot {

    public MusicalHotspot() {
        super(HotspotType.MUSICAL);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Windchimes(), new Bells(), new Organ()));
    }
}
