package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.Marigolds;
import com.soulplay.content.player.skills.construction.buildable.impl.Roses;
import com.soulplay.content.player.skills.construction.buildable.impl.Sunflowers;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class BigPlantHotspot extends Hotspot {

    public BigPlantHotspot() {
        super(HotspotType.BIG_PLANT);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new Sunflowers(), new Marigolds(), new Roses()));
    }

}
