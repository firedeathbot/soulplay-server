package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class SkillTrophyHallRoom extends Room {

	private static final Palette palette = new Palette(3, 0);

	private static final boolean[] doors = {true, true, true, true};

	public SkillTrophyHallRoom(int x, int y, int z) {
		super(RoomType.SKILL_TROPHY_HALL, x, y, z);
	}

	@Override
	public boolean[] hasDoors() {
		return doors;
	}

	@Override
	public Multimap<HotspotType, RoomObject> getHotspots() {
		return hotspots;
	}

	@Override
	public Palette getStaticPalette() {
		return palette;
	}

	private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

	static {
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(2, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(2, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(2, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(2, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(3, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(3, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(3, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(3, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(4, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(4, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(4, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(4, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(5, 2), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(5, 3), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(5, 4), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15377, Location.create(5, 5), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(1, 2), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(1, 3), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(1, 4), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(1, 5), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(2, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(2, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(3, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(3, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(4, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(4, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(5, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(5, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(6, 2), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(6, 3), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(6, 4), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15378, Location.create(6, 5), 1, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(1, 1), 3, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(1, 6), 0, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(6, 1), 2, 22));
		hotspots.put(HotspotType.RUG, new RoomObject(15379, Location.create(6, 6), 1, 22));
		hotspots.put(HotspotType.STAIR, new RoomObject(15380, Location.create(3, 3), 0, 10));
		hotspots.put(HotspotType.HEAD_TROPHY, new RoomObject(15382, Location.create(6, 7), 0, 10));
		hotspots.put(HotspotType.FISHING_TROPHY, new RoomObject(15383, Location.create(1, 7), 0, 10));
		hotspots.put(HotspotType.ARMOR, new RoomObject(15384, Location.create(5, 3), 0, 10));
		hotspots.put(HotspotType.DECORATIVE_ARMOR, new RoomObject(15385, Location.create(2, 3), 0, 10));
		hotspots.put(HotspotType.RUNE_CASE, new RoomObject(15386, Location.create(0, 6), 0, 10));
	}
	
	@Override
	public RoomType getUpperRoomType() {
		return RoomType.SKILL_HALL_UPPER;
	}
	
	@Override
	public RoomType getLowerRoomType() {
		return getType();
	}

}
