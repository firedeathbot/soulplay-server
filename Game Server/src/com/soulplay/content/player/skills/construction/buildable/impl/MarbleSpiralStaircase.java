package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class MarbleSpiralStaircase extends Buildable {
    @Override
    public String getName() {
        return "Marble spiral staircase";
    }

    @Override
    public int getItemId() {
        return 8259;
    }

    @Override
    public int getObjectId() {
        return 13505;
    }
    
    @Override
    public int getObjectId(boolean upperRoom) {
    	return !upperRoom ? getObjectId() : 13506;
    }

    @Override
    public int getLevelRequired() {
        return 97;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK, 10), new Item(ConstructionItems.MARBLE_BLOCK, 7));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 4400;
    }
}
