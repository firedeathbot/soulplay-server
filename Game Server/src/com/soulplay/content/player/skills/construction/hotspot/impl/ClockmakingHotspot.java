package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.CraftingTable1;
import com.soulplay.content.player.skills.construction.buildable.impl.CraftingTable2;
import com.soulplay.content.player.skills.construction.buildable.impl.CraftingTable3;
import com.soulplay.content.player.skills.construction.buildable.impl.CraftingTable4;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ClockmakingHotspot extends Hotspot {
    public ClockmakingHotspot() {
        super(HotspotType.CLOCKMAKING);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new CraftingTable1(), new CraftingTable2(), new CraftingTable3(), new CraftingTable4()));
    }
}
