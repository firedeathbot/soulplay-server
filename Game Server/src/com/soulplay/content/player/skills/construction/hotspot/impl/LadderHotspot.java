package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MahoganyLadder;
import com.soulplay.content.player.skills.construction.buildable.impl.OakLadder;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakLadder;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class LadderHotspot extends Hotspot {

    public LadderHotspot() {
        super(HotspotType.LADDER);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakLadder(), new TeakLadder(), new MahoganyLadder()));
    }

}
