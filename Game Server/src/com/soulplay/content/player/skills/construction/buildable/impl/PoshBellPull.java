package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class PoshBellPull extends Buildable {
    @Override
    public String getName() {
        return "Posh bell-pull";
    }

    @Override
    public int getItemId() {
        return 8101;
    }

    @Override
    public int getObjectId() {
        return 13309;
    }

    @Override
    public int getLevelRequired() {
        return 60;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK), new Item(ConstructionItems.CLOTH, 2), new Item(ConstructionItems.GOLD_LEAF));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 420;
    }
}
