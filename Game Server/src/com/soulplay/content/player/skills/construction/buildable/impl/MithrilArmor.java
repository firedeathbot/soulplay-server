package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class MithrilArmor extends Buildable {
    @Override
    public String getName() {
        return "Mithril armour";
    }

    @Override
    public int getItemId() {
        return 8270;
    }

    @Override
    public int getObjectId() {
        return 13491;
    }

    @Override
    public int getLevelRequired() {
        return 28;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 2), new Item(1159), new Item(1121), new Item(1085));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 135;
    }
}
