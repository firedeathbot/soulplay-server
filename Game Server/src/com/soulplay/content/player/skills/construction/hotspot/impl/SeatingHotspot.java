package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class SeatingHotspot extends Hotspot {

    public SeatingHotspot() {
        super(HotspotType.SEATING);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new WoodenBench(), new OakBench(), new CarvedOakBench(), new TeakDiningBench(), new CarvedTeakBench(), new MahoganyBench(), new GildedBench()));
    }
}
