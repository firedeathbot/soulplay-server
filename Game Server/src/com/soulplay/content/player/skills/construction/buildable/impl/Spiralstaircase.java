package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class Spiralstaircase extends Buildable {
    @Override
    public String getName() {
        return "Spiral staircase";
    }

    @Override
    public int getItemId() {
        return 8258;
    }

    @Override
    public int getObjectId() {
        return 13503;
    }
    
    @Override
    public int getObjectId(boolean upperRoom) {
    	return !upperRoom ? getObjectId() : 13504;
    }

    @Override
    public int getLevelRequired() {
        return 67;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.TEAK_PLANK, 10), new Item(ConstructionItems.LIMESTONE_BRICK, 7));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(HAMMER);
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 1040;
    }
}
