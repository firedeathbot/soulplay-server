package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class MediumMap extends Buildable {

    @Override
    public String getName() {
        return "Medium map";
    }

    @Override
    public int getItemId() {
        return 8295;
    }

    @Override
    public int getObjectId() {
        return 13526;
    }

    @Override
    public int getLevelRequired() {
        return 58;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAHOGANY_PLANK, 3), new Item(8005));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_HIGH_ANIM;
    }

    @Override
    public int getExperience() {
        return 451;
    }

}
