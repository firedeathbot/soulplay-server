package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;

import java.util.Arrays;
import java.util.List;

public class SteelFramedWorkbench extends Buildable {
    @Override
    public String getName() {
        return "Steel framed workbench";
    }

    @Override
    public int getItemId() {
        return 8377;
    }

    @Override
    public int getObjectId() {
        return 13706;
    }

    @Override
    public int getLevelRequired() {
        return 46;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.OAK_PLANK, 6), new Item(ConstructionItems.STEEL_BAR, 4));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 440;
    }
}
