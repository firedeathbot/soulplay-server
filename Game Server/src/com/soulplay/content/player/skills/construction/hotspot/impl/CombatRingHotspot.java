package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.BoxingRing;
import com.soulplay.content.player.skills.construction.buildable.impl.CombatRing;
import com.soulplay.content.player.skills.construction.buildable.impl.FencingRing;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class CombatRingHotspot extends Hotspot {

    public CombatRingHotspot() {
        super(HotspotType.COMBAT_RING);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new BoxingRing(), new FencingRing(), new CombatRing()));
    }

}
