package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.*;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class GardenCentrepieceHotspot extends Hotspot {

    public GardenCentrepieceHotspot() {
        super(HotspotType.GARDEN_CENTREPIECE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new ExitPortal(), new DecorativeRock(), new Pond(), new ImpStatue(), new DungeonEntrance()));
    }

}
