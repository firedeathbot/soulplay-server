package com.soulplay.content.player.skills.construction;

import java.util.LinkedList;

import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;

public class ConstructionPathFinder {
	
	public static void findRoute(Mob e, Location l, boolean moveNear, int xLength, int yLength, DynamicRegionClip clipMap) {
		findRoute(e, l.getX(), l.getY(), moveNear, xLength, yLength, clipMap, 0, 0, 0, 0, 0, 1);
	}

	public static boolean findRoute(Mob e, int destX, int destY, boolean moveNear, int xLength, int yLength, DynamicRegionClip clipMap, int offsetSizeX, int offsetSizeY, int directionMask, int objectType, int objectRotation, int reachDistance) {
		if (clipMap == null) {
			return false;
		}
		int absX = e.getCurrentLocation().getX();
		int absY = e.getCurrentLocation().getY();
		int z = e.getCurrentLocation().getZ();
		if (destX == absX && destY == absY && !moveNear) {
			if (!moveNear && e.isPlayer()) {
				e.toPlayer().finalLocalDestX = absX;
				e.toPlayer().finalLocalDestY = absY;
			}
			return true;
		}
		
		int mapRegionX = e.getMapRegionX();
		int mapRegionY = e.getMapRegionY();

		destX = destX - 8 * mapRegionX;
		destY = destY - 8 * mapRegionY;
		
		int[][] via = new int[104][104];
		int[][] cost = new int[104][104];
		LinkedList<Integer> tileQueueX = new LinkedList<>();
		LinkedList<Integer> tileQueueY = new LinkedList<>();
		for (int xx = 0; xx < 104; xx++) {
			for (int yy = 0; yy < 104; yy++) {
				cost[xx][yy] = 99999999;
			}
		}
		int curX = e.getLocalX();
		int curY = e.getLocalY();
		try {
			via[curX][curY] = 99;
			cost[curX][curY] = 0;
		} catch (Exception ex) {
			System.out.println("way out of range! Name:" + (e.isPlayer() ? e.toPlayer().getName() : ("NPCID:"+e.getId())) + " X:" + absX + " Y:" + absY + " Z:" + z);
			ex.printStackTrace();
		}
		int tail = 0;
		tileQueueX.add(curX);
		tileQueueY.add(curY);
		boolean foundPath = false;
		int pathLength = 4000;
		while (tail != tileQueueX.size() && tileQueueX.size() < pathLength) {
			curX = tileQueueX.get(tail);
			curY = tileQueueY.get(tail);
			int curAbsX = mapRegionX * 8 + curX;
			int curAbsY = mapRegionY * 8 + curY;
			if (curX == destX && curY == destY) {
				foundPath = true;
				if (!moveNear && e.isPlayer()) {
					e.toPlayer().finalLocalDestX = curAbsX;
					e.toPlayer().finalLocalDestY = curAbsY;
				}
				break;
			}
			if (objectType != 0) {
				if ((objectType < 5 || objectType == 10) && PathFinder.method219(destX, destY, curX, curY, objectRotation, objectType-1, RegionClip.getClipping(curAbsX, curAbsY, z, clipMap))) {
					foundPath = true;
					if (!moveNear && e.isPlayer()) {
						e.toPlayer().finalLocalDestX = curAbsX;
						e.toPlayer().finalLocalDestY = curAbsY;
					}
					break;
				}
				if (objectType < 10 && PathFinder.method220(destX, destY, curX, curY, objectType-1, objectRotation, RegionClip.getClipping(curAbsX, curAbsY, z, clipMap))) {
					foundPath = true;
					if (!moveNear && e.isPlayer()) {
						e.toPlayer().finalLocalDestX = curAbsX;
						e.toPlayer().finalLocalDestY = curAbsY;
					}
					break;
				}
			}
			if (offsetSizeX != 0 && offsetSizeY != 0 && PathFinder.reachedEntity(reachDistance, destX, destY, curX, curY, offsetSizeX, offsetSizeY, directionMask, RegionClip.getClipping(curAbsX, curAbsY, z, clipMap))) {
				foundPath = true;
				if (!moveNear && e.isPlayer()) {
					e.toPlayer().finalLocalDestX = curAbsX;
					e.toPlayer().finalLocalDestY = curAbsY;
				}
				break;
			}
			tail = (tail + 1) % pathLength;
			int thisCost = cost[curX][curY] + 1;
			if (curX > 0 && via[curX - 1][curY] == 0
					&& (clipMap.getClipping(curAbsX - 1, curAbsY,
							z) & 0x1280108) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY);
				via[curX - 1][curY] = 2;
				cost[curX - 1][curY] = thisCost;
			}
			if (curX < 104 - 1 && via[curX + 1][curY] == 0
					&& (clipMap.getClipping(curAbsX + 1, curAbsY,
							z) & 0x1280180) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY);
				via[curX + 1][curY] = 8;
				cost[curX + 1][curY] = thisCost;
			}
			if (curY > 0 && via[curX][curY - 1] == 0
					&& (clipMap.getClipping(curAbsX, curAbsY - 1,
							z) & 0x1280102) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY - 1);
				via[curX][curY - 1] = 1;
				cost[curX][curY - 1] = thisCost;
			}
			if (curY < 104 - 1 && via[curX][curY + 1] == 0
					&& (clipMap.getClipping(curAbsX, curAbsY + 1,
							z) & 0x1280120) == 0) {
				tileQueueX.add(curX);
				tileQueueY.add(curY + 1);
				via[curX][curY + 1] = 4;
				cost[curX][curY + 1] = thisCost;
			}
			if (curX > 0 && curY > 0 && via[curX - 1][curY - 1] == 0
					&& (clipMap.getClipping(curAbsX - 1, curAbsY - 1,
							z) & 0x128010e) == 0
					&& (clipMap.getClipping(curAbsX - 1, curAbsY,
							z) & 0x1280108) == 0
					&& (clipMap.getClipping(curAbsX, curAbsY - 1,
							z) & 0x1280102) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY - 1);
				via[curX - 1][curY - 1] = 3;
				cost[curX - 1][curY - 1] = thisCost;
			}
			if (curX < 104 - 1 && curY > 0 && via[curX + 1][curY - 1] == 0
					&& (clipMap.getClipping(curAbsX + 1, curAbsY - 1,
							z) & 0x1280183) == 0
					&& (clipMap.getClipping(curAbsX + 1, curAbsY,
							z) & 0x1280180) == 0
					&& (clipMap.getClipping(curAbsX, curAbsY - 1,
							z) & 0x1280102) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY - 1);
				via[curX + 1][curY - 1] = 9;
				cost[curX + 1][curY - 1] = thisCost;
			}
			if (curX > 0 && curY < 104 - 1 && via[curX - 1][curY + 1] == 0
					&& (clipMap.getClipping(curAbsX - 1, curAbsY + 1,
							z) & 0x1280138) == 0
					&& (clipMap.getClipping(curAbsX - 1, curAbsY,
							z) & 0x1280108) == 0
					&& (clipMap.getClipping(curAbsX, curAbsY + 1,
							z) & 0x1280120) == 0) {
				tileQueueX.add(curX - 1);
				tileQueueY.add(curY + 1);
				via[curX - 1][curY + 1] = 6;
				cost[curX - 1][curY + 1] = thisCost;
			}
			if (curX < 104 - 1 && curY < 104 - 1 && via[curX + 1][curY + 1] == 0
					&& (clipMap.getClipping(curAbsX + 1, curAbsY + 1,
							z) & 0x12801e0) == 0
					&& (clipMap.getClipping(curAbsX + 1, curAbsY,
							z) & 0x1280180) == 0
					&& (clipMap.getClipping(curAbsX, curAbsY + 1,
							z) & 0x1280120) == 0) {
				tileQueueX.add(curX + 1);
				tileQueueY.add(curY + 1);
				via[curX + 1][curY + 1] = 12;
				cost[curX + 1][curY + 1] = thisCost;
			}
		}
		if (!foundPath) {
			if (moveNear) {
				int i_223_ = 1000;
				int thisCost = 100;
				int i_225_ = 10;
				for (int x = destX - i_225_; x <= destX + i_225_; x++) {
					for (int y = destY - i_225_; y <= destY + i_225_; y++) {
						if (x >= 0 && y >= 0 && x < 104 && y < 104
								&& cost[x][y] < 100) {
							int i_228_ = 0;
							if (x < destX) {
								i_228_ = destX - x;
							} else if (x > destX + xLength - 1) {
								i_228_ = x - (destX + xLength - 1);
							}
							int i_229_ = 0;
							if (y < destY) {
								i_229_ = destY - y;
							} else if (y > destY + yLength - 1) {
								i_229_ = y - (destY + yLength - 1);
							}
							int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;
							if (i_230_ < i_223_ || (i_230_ == i_223_
									&& (cost[x][y] < thisCost))) {
								i_223_ = i_230_;
								thisCost = cost[x][y];
								curX = x;
								curY = y;
							}
						}
					}
				}
				if (i_223_ == 1000) {
					return false;
				}
			} else {
				return false;
			}
		}
		tail = 0;
		tileQueueX.set(tail, curX);
		tileQueueY.set(tail++, curY);
		int l5;
		for (int j5 = l5 = via[curX][curY]; curX != e.getLocalX() || curY != e.getLocalY(); j5 = via[curX][curY]) {
			if (j5 != l5) {
				l5 = j5;
				tileQueueX.set(tail, curX);
				tileQueueY.set(tail++, curY);
			}
			if ((j5 & 2) != 0) {
				curX++;
			} else if ((j5 & 8) != 0) {
				curX--;
			}
			if ((j5 & 1) != 0) {
				curY++;
			} else if ((j5 & 4) != 0) {
				curY--;
			}
		}
		int size = tail--;
		int pathX = mapRegionX * 8 + tileQueueX.get(tail);
		int pathY = mapRegionY * 8 + tileQueueY.get(tail);
		if (absX == pathX && absY == pathY) {
			tileQueueX = null;
			tileQueueY = null;
			return true;
		}
		if (e.isPlayer()) {
			Client c = (Client) e.toPlayer();
			c.getMovement().hardResetWalkingQueue();
			c.getMovement().addToWalkingQueue(PathFinder.localize(pathX, mapRegionX), PathFinder.localize(pathY, mapRegionY));
			for (int i = 1; i < size; i++) {
				tail--;
				pathX = mapRegionX * 8 + tileQueueX.get(tail);
				pathY = mapRegionY * 8 + tileQueueY.get(tail);
				c.getMovement().addToWalkingQueue(PathFinder.localize(pathX, mapRegionX), PathFinder.localize(pathY, mapRegionY));
			}
		} else if (e.isNpc()) {
			NPC npc = e.toNpc();
			npc.getWalkingQueue().reset();
			npc.getWalkingQueue().addPath(pathX, pathY);
			for (int i = 1; i < size; i++) {
				tail--;
				pathX = npc.getMapRegionX() * 8 + tileQueueX.get(tail);
				pathY = npc.getMapRegionY() * 8 + tileQueueY.get(tail);
				npc.getWalkingQueue().addPath(pathX, pathY);
			}
		}
		tileQueueX = null;
		tileQueueY = null;
		return true;
	}
	
}
