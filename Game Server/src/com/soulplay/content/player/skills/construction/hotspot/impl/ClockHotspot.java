package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.GildedClock;
import com.soulplay.content.player.skills.construction.buildable.impl.OakClock;
import com.soulplay.content.player.skills.construction.buildable.impl.TeakClock;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ClockHotspot extends Hotspot {

    public ClockHotspot() {
        super(HotspotType.CLOCK);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new OakClock(), new TeakClock(), new GildedClock()));
    }

}
