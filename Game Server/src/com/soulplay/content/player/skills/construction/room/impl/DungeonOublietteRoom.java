package com.soulplay.content.player.skills.construction.room.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.world.entity.Location;

public class DungeonOublietteRoom extends Room {

    private static final Palette palette = new Palette(8, -3);

    private static final Multimap<HotspotType, RoomObject> hotspots = ArrayListMultimap.create();

    private static final boolean[] doors =  {true, true, true, true};

    public DungeonOublietteRoom(int x, int y, int z) {
        super(RoomType.DUNGEON_OUBLIETTE, x, y, z);
    }

    @Override
    public boolean[] hasDoors() {
        return doors;
    }

    @Override
    public Multimap<HotspotType, RoomObject> getHotspots() {
        return hotspots;
    }

    @Override
    public Palette getStaticPalette() {
        return palette;
    }

    static {

        // top
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(2, 5), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(3, 5), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(4, 5), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(5, 5), 0, 10));

        // middle top
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(2, 4), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(3, 4), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(4, 4), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(5, 4), 0, 10));

        // middle bot
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(2, 3), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(3, 3), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(4, 3), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(5, 3), 0, 10));

        // bot
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(2, 2), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(3, 2), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(4, 2), 0, 10));
        hotspots.put(HotspotType.OUBLIETTE_FLOOR, new RoomObject(15350, Location.create(5, 2), 0, 10));

        // top
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(2, 5), 0, 2)); // nw corner
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15353, Location.create(3, 5), 1, 0)); // door
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(4, 5), 1, 0));
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(5, 5), 1, 2)); // ne corner

        // west
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(2, 4), 0, 0));
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(2, 3), 0, 0));
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(2, 2), 3, 2)); // sw corner

        // south
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(3, 2), 3, 0));
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(3, 2), 3, 0));
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(3, 2), 2, 2)); // se corner

        // east
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(5, 4), 2, 0));
        hotspots.put(HotspotType.PRISON_SPACE, new RoomObject(15352, Location.create(5, 3), 2, 0));

        hotspots.put(HotspotType.GUARD, new RoomObject(15354, Location.create(0,0), 3, 10));
        hotspots.put(HotspotType.LADDER, new RoomObject(15356, Location.create(1,6), 0, 10));
        hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(0,2), 0, 4));
        hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(2,7), 1, 4));
        hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(7,5), 2, 4));
        hotspots.put(HotspotType.DUNGEON_DECORATION, new RoomObject(15331, Location.create(5,0), 3, 4));

        hotspots.put(HotspotType.LIGHT, new RoomObject(15355, Location.create(0,5), 0, 4));
        hotspots.put(HotspotType.LIGHT, new RoomObject(15355, Location.create(5,7), 1, 4));
        hotspots.put(HotspotType.LIGHT, new RoomObject(15355, Location.create(7,2), 2, 4));
        hotspots.put(HotspotType.LIGHT, new RoomObject(15355, Location.create(2,0), 3, 4));
    }

}
