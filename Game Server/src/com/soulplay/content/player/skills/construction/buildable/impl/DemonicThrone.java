package com.soulplay.content.player.skills.construction.buildable.impl;

import com.soulplay.content.player.skills.construction.ConstructionAnimations;
import com.soulplay.content.player.skills.construction.ConstructionItems;
import com.soulplay.content.player.skills.construction.buildable.Buildable;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

import java.util.Arrays;
import java.util.List;

public class DemonicThrone extends Buildable {
    @Override
    public String getName() {
        return "Demonic throne";
    }

    @Override
    public int getItemId() {
        return 8363;
    }

    @Override
    public int getObjectId() {
        return 13671;
    }

    @Override
    public void onAchievement(Player player) {
        player.getAchievement().buildDemonicThrone();
    }

    @Override
    public int getLevelRequired() {
        return 99;
    }

    @Override
    public List<Item> getMaterialsNeeded() {
        return Arrays.asList(new Item(ConstructionItems.MAGIC_STONE, 25));
    }

    @Override
    public List<Item> getRequiredItems() {
        return Arrays.asList(new Item(ConstructionItems.HAMMER));
    }

    @Override
    public int getAnimation() {
        return ConstructionAnimations.BUILD_MID_ANIM;
    }

    @Override
    public int getExperience() {
        return 25000;
    }
}
