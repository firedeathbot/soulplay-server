package com.soulplay.content.player.skills.construction.hotspot.impl;

import com.soulplay.content.player.skills.construction.buildable.impl.MagicalBalance1;
import com.soulplay.content.player.skills.construction.buildable.impl.MagicalBalance2;
import com.soulplay.content.player.skills.construction.buildable.impl.MagicalBalance3;
import com.soulplay.content.player.skills.construction.hotspot.Hotspot;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;

import java.util.Arrays;

public class ElementalBalanceHotspot extends Hotspot {

    public ElementalBalanceHotspot() {
        super(HotspotType.ELEMENTAL_BALANCE);
    }

    @Override
    public void init() {
        buildables.addAll(Arrays.asList(new MagicalBalance1(), new MagicalBalance2(), new MagicalBalance3()));
    }

}
