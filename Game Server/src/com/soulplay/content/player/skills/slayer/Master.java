package com.soulplay.content.player.skills.slayer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum Master {
	TURAEL(8461, 2, 0, 0, 0, Assignment.BANSHEES_BY_TURAEL,
			Assignment.BATS_BY_TURAEL, Assignment.BEARS_BY_TURAEL,
			Assignment.CAVE_BUGS_BY_TURAEL, Assignment.CAVE_SLIMES_BY_TURAEL,
			Assignment.COWS_BY_TURAEL, Assignment.CRAWLING_HANDS_BY_TURAEL,
			Assignment.DOGS_BY_TURAEL, Assignment.DWARVES_BY_TURAEL,
			Assignment.GHOSTS_BY_TURAEL, Assignment.GOBLINS_BY_TURAEL,
			Assignment.ICEFIENDS_BY_TURAEL, Assignment.SCORPIONS_BY_TURAEL,
			Assignment.SKELETONS_BY_TURAEL, Assignment.SPIDERS_BY_TURAEL,
			Assignment.WOLVES_BY_TURAEL, Assignment.ZOMBIES_BY_TURAEL, Assignment.CAVE_CRAWLERS_BY_TURAEL),
	/* Assignment.MINOTAURS_BY_TURAEL, */ /* Assignment.MONKEYS_BY_TURAEL, */ /*
																				 * Assignment
																				 * .
																				 * TROLLS_BY_TURAEL,
																				 */
	/* Assignment.DESERT_LIZARDS_BY_TURAEL, */ /* Assignment.BIRDS_BY_TURAEL, */

	MAZCHNA(8464, 4, 30, 20, 0, Assignment.BANSHEES_BY_MAZCHNA,
			Assignment.BATS_BY_MAZCHNA, Assignment.BEARS_BY_MAZCHNA,
			Assignment.CAVE_CRAWLERS_BY_MAZCHNA,
			Assignment.CAVE_SLIMES_BY_MAZCHNA,
			Assignment.COCKATRICES_BY_MAZCHNA,
			Assignment.CRAWLING_HANDS_BY_MAZCHNA,
			Assignment.CYCLOPES_BY_MAZCHNA, Assignment.DOGS_BY_MAZCHNA,
			Assignment.HILL_GIANTS_BY_MAZCHNA,
			Assignment.HOB_GOBLINS_BY_MAZCHNA,
			Assignment.ICE_WARRIORS_BY_MAZCHNA, Assignment.KALPHITES_BY_MAZCHNA,
			Assignment.PYREFIENDS_BY_MAZCHNA, Assignment.ROCK_SLUGS_BY_MAZCHNA,
			Assignment.SKELETONS_BY_MAZCHNA, Assignment.WOLVES_BY_MAZCHNA,
			Assignment.ZOMBIES_BY_MAZCHNA
	/* Assignment.WALL_BEASTS_BY_MAZCHNA */ /*
											 * Assignment.CATABLEPON_BY_MAZCHNA,
											 */
	/* Assignment.VAMPYRES_BY_MAZCHNA, */ /*
											 * Assignment.
											 * FLESH_CRAWLERS_BY_MAZCHNA,
											 */ /* Assignment.GHOULS_BY_MAZCHNA, */
	/* Assignment.MOGRES_BY_MAZCHNA, */ /*
										 * Assignment.DESERT_LIZARDS_BY_MAZCHNA,
										 */),

	VANNAKA(1597, 6, 40, 40, 0, Assignment.BANSHEES_BY_VANNAKA,
			Assignment.BASILISK_BY_VANNAKA, Assignment.BLOODVELD_BY_VANNAKA,
			Assignment.COCKATRICE_BY_VANNAKA, Assignment.CYCLOPES_BY_VANNAKA,
			Assignment.DUST_DEVIL_BY_VANNAKA,
			Assignment.EARTH_WARRIOR_BY_VANNAKA,
			Assignment.GREEN_DRAGON_BY_VANNAKA,
			Assignment.HILL_GIANT_BY_VANNAKA, Assignment.ICE_GIANT_BY_VANNAKA,
			Assignment.ICE_WARRIOR_BY_VANNAKA,
			Assignment.INFERNAL_MAGE_BY_VANNAKA, Assignment.JELLY_BY_VANNAKA,
			Assignment.JUNGLE_HORROR_BY_VANNAKA,
			Assignment.LESSER_DEMON_BY_VANNAKA, Assignment.TUROTH_BY_VANNAKA,
			Assignment.CAVE_HORROR_BY_VANNAKA
	/* Assignment.ABERRANT_SPECTRE_BY_VANNAKA, */ /*
													 * Assignment.
													 * ANKOU_BY_VANNAKA,
													 */
	/* Assignment.KILLERWATT_BY_VANNAKA, */ /*
											 * Assignment.
											 * HARPIE_BUG_SWARM_BY_VANNAKA,
											 */
	/* Assignment.GHOUL_BY_VANNAKA, */ /* Assignment.CROCODILE_BY_VANNAKA, */ /*
																				 * Assignment
																				 * .
																				 * BRINE_RAT_BY_VANNAKA,
																				 */
	/* Assignment.MOGRE_BY_VANNAKA, */),

	CHAELDAR(1598, 10, 60, 75, 0, Assignment.BANSHEES_BY_CHAELDAR,
			Assignment.BASILISK_BY_CHAELDAR, Assignment.BLOODVELD_BY_CHAELDAR,
			Assignment.BLUE_DRAGON_BY_CHAELDAR,
			Assignment.BRONZE_DRAGON_BY_CHAELDAR,
			Assignment.CAVE_CRAWLER_BY_CHAELDAR,
			Assignment.CRAWLING_HAND_BY_CHAELDAR,
			Assignment.DAGANNOTH_BY_CHAELDAR, Assignment.DUST_DEVIL_BY_CHAELDAR,
			Assignment.FIRE_GIANT_BY_CHAELDAR, Assignment.GARGOYLE_BY_CHAELDAR,
			Assignment.JUNGLE_STRYKEWYRM_BY_CHAELDAR,
			Assignment.INFERNAL_MAGE_BY_CHAELDAR, Assignment.JELLY_BY_CHAELDAR,
			Assignment.JUNGLE_HORROR_BY_CHAELDAR,
			Assignment.KALPHITE_BY_CHAELDAR, Assignment.KURASK_BY_CHAELDAR,
			Assignment.LESSER_DEMON_BY_CHAELDAR, Assignment.TUROTH_BY_CHAELDAR,
			Assignment.CAVE_HORROR_BY_CHAELDAR 
	/* Assignment.VYREWATCH_BY_CHAELDAR, */ /*
											 * Assignment.
											 * WARPED_TORTOISE_BY_CHAELDAR
											 */
	/* Assignment.MUTATED_ZYGOMITE_BY_CHAELDAR, */ /*
													 * Assignment.
													 * SHADOW_WARRIOR_BY_CHAELDAR,
													 */
	/* Assignment.TROLL_BY_CHAELDAR, */ /* Assignment.VYREWATCH_BY_CHAELDAR, */ /*
																				 * Assignment
																				 * .
																				 * WARPED_TORTOISE_BY_CHAELDAR
																				 */
	/* Assignment.GRIFOLAPINE_BY_CHAELDAR, */ /*
												 * Assignment.
												 * GRIFOLAROO_BY_CHAELDAR,
												 */
	/* Assignment.FUNGAL_MAGI_BY_CHAELDAR, */ /*
												 * Assignment.
												 * HARPIE_BUG_SWARM_BY_CHAELDAR,
												 */
	/* Assignment.FUNGAL_MAGI_BY_CHAELDAR, */ /* Assignment.ELVE_BY_CHAELDAR, */ /*
																					 * Assignment
																					 * .
																					 * FEVER_SPIDER_BY_CHAELDAR,
																					 */
	 /*
												 * Assignment.
												 * BRINE_RAT_BY_CHAELDAR,
												 */
	/* Assignment.ABERRANT_SPECTRE_BY_CHAELDAR, */),

	KONAR_QUO_MATEN(108623, 18, 80, 75, 0, Assignment.ABERRANT_SPECTRE_BY_KONAR,
			Assignment.ABYSSAL_DEMON_BY_KONAR, Assignment.ADAMANT_DRAGON_BY_KONAR,
			Assignment.ANKOU_BY_KONAR, Assignment.AVIANSIE_BY_KONAR, Assignment.BASILISK_BY_KONAR,
			Assignment.BLACK_DEMON_BY_KONAR, Assignment.BLACK_DRAGON_BY_KONAR,
			Assignment.BLOODVELD_BY_KONAR, Assignment.BLUE_DRAGON_BY_KONAR,
			Assignment.BRONZE_DRAGON_BY_KONAR,
			Assignment.DAGANNOTH_BY_KONAR, Assignment.DARK_BEAST_BY_KONAR,
			Assignment.DRAKES_BY_KONAR, Assignment.DUST_DEVIL_BY_KONAR,
			Assignment.FIRE_GIANT_BY_KONAR,
			Assignment.GARGOYLE_BY_KONAR, Assignment.GREATER_DEMON_BY_KONAR,
			Assignment.HELLHOUND_BY_KONAR,
			Assignment.HYDRAS_BY_KONAR, Assignment.JELLY_BY_KONAR, Assignment.KALPHITES_BY_KONAR,
			Assignment.KURASK_BY_KONAR,
			Assignment.MITHRIL_DRAGON_BY_KONAR, Assignment.NECHRYAEL_BY_KONAR,
	        Assignment.RED_DRAGON_BY_KONAR,
	        Assignment.RUNE_DRAGON_BY_KONAR, Assignment.SKELETAL_WYVERN_BY_KONAR,
	        Assignment.STEEL_DRAGON_BY_KONAR,
	        Assignment.TUROTH_BY_KONAR, Assignment.WATERFIEND_BY_KONAR,
	        Assignment.WYRMS_BY_KONAR, Assignment.SMOKE_DEVIL_BY_KONAR),
	
	NIEVE(106797, 12, 70, 85, 0, Assignment.ABERRANT_SPECTRE_BY_NIEVE,
			Assignment.ABYSSAL_DEMON_BY_NIEVE, Assignment.ADAMANT_DRAGON_BY_NIEVE,
			Assignment.ANKOU_BY_NIEVE, Assignment.AVIANSIE_BY_NIEVE, Assignment.BASILISK_BY_NIEVE,
			Assignment.BLACK_DEMON_BY_NIEVE, Assignment.BLACK_DRAGON_BY_NIEVE,
			Assignment.BLOODVELD_BY_NIEVE, Assignment.BLUE_DRAGON_BY_NIEVE,
			Assignment.DAGANNOTH_BY_NIEVE,
			Assignment.DARK_BEAST_BY_NIEVE, Assignment.DRAKES_BY_NIEVE,
			Assignment.DUST_DEVIL_BY_NIEVE, Assignment.FIRE_GIANT_BY_NIEVE,
			Assignment.GARGOYLE_BY_NIEVE,
			Assignment.GREATER_DEMON_BY_NIEVE, Assignment.HELLHOUND_BY_NIEVE,
			Assignment.IRON_DRAGON_BY_NIEVE,
			Assignment.KALPHITES_BY_NIEVE, Assignment.KURASK_BY_NIEVE, Assignment.MITHRIL_DRAGON_BY_NIEVE,
			Assignment.NECHRYAEL_BY_NIEVE,
			Assignment.RED_DRAGON_BY_NIEVE, Assignment.RUNE_DRAGON_BY_NIEVE,
	        Assignment.SKELETAL_WYVERN_BY_NIEVE,
	        Assignment.STEEL_DRAGON_BY_NIEVE, Assignment.TUROTH_BY_NIEVE,
	        Assignment.TZHAAR_BY_NIEVE,
	        Assignment.WYRMS_BY_NIEVE, Assignment.SMOKE_DEVIL_BY_NIEVE,
	        Assignment.CAVE_HORROR_BY_SUMONA),
			
	SUMONA(7780, 14, 70, 90, 35, Assignment.ABYSSAL_DEMON_BY_SUMONA,
			Assignment.AQUANITE_BY_SUMONA, Assignment.BANSHEES_BY_SUMONA,
			Assignment.BASILISK_BY_SUMONA, Assignment.BLACK_DEMON_BY_SUMONA,
			Assignment.BLOODVELD_BY_SUMONA, Assignment.BLUE_DRAGON_BY_SUMONA,
			Assignment.CAVE_CRAWLER_BY_SUMONA, Assignment.DAGANNOTH_BY_SUMONA,
			Assignment.DESERT_STRYKEWYRM_BY_SUMONA,
			Assignment.DUST_DEVIL_BY_SUMONA, Assignment.FIRE_GIANT_BY_SUMONA,
			Assignment.GARGOYLE_BY_SUMONA, Assignment.GREATER_DEMON_BY_SUMONA,
			Assignment.HELLHOUND_BY_SUMONA, Assignment.IRON_DRAGON_BY_SUMONA,
			Assignment.JUNGLE_STRYKEWYRM_BY_SUMONA,
			Assignment.KALPHITE_BY_SUMONA, Assignment.KURASK_BY_SUMONA,
			Assignment.MUTATED_JADINKO_BY_SUMONA,
			Assignment.NECHRYAEL_BY_SUMONA, Assignment.RED_DRAGON_BY_SUMONA,
			Assignment.SPIRITUAL_MAGE_BY_SUMONA,
			Assignment.SPIRITUAL_WARRIOR_BY_SUMONA, Assignment.TUROTH_BY_SUMONA,
			Assignment.CAVE_HORROR_BY_SUMONA
	/* Assignment.VYREWATCH_BY_SUMONA, */ /* Assignment.TROLL_BY_SUMONA, */ /*
																			 * Assignment
																			 * .
																			 * TERROR_DOG_BY_SUMONA,
																			 */
	/* Assignment.WARPED_TORTOISE_BY_SUMONA */ /*
												 * Assignment.
												 * GRIFOLAPINE_BY_SUMONA,
												 */ /*
													 * Assignment.
													 * GRIFOLAROO_BY_SUMONA,
													 */
	/* Assignment.FUNGAL_MAGE_BY_SUMONA, */ /* Assignment.ELVE_BY_SUMONA, */ 
	/* Assignment.ABERRANT_SPECTRE_BY_SUMONA, */ /*
													 * Assignment.
													 * AUTOMATON_BY_SUMONA,
													 */ /*
													 * Assignment.
													 * ELVE_BY_SUMONA,
													 */),

	DURADEL(8466, 16, 80, 100, 50, Assignment.ABYSSAL_DEMON_BY_DURADEL,
			Assignment.AQUANITE_BY_DURADEL, Assignment.AVIANSIE_BY_DURADEL,
			Assignment.BLACK_DEMON_BY_DURADEL,
			Assignment.BLACK_DRAGON_BY_DURADEL, Assignment.BLOODVELD_BY_DURADEL,
			Assignment.DAGANNOTH_BY_DURADEL, Assignment.DARK_BEAST_BY_DURADEL,
			Assignment.DESERT_STRYKEWYRM_BY_DURADEL,
			Assignment.DUST_DEVIL_BY_DURADEL, Assignment.FIRE_GIANT_BY_DURADEL,
			Assignment.GARGOYLE_BY_DURADEL, Assignment.GORAK_BY_DURADEL,
			Assignment.GREATER_DEMON_BY_DURADEL,
			Assignment.HELLHOUND_BY_DURADEL,
			Assignment.ICE_STRYKEWYRM_BY_DURADEL,
			Assignment.IRON_DRAGON_BY_DURADEL,
			Assignment.JUNGLE_STRYKEWYRM_BY_DURADEL,
			Assignment.KALPHITE_BY_DURADEL,
			Assignment.MITHRIL_DRAGON_BY_DURADEL,
			Assignment.MUTATED_JADINKO_BY_DURADEL,
			Assignment.NECHRYAEL_BY_DURADEL,
			Assignment.SKELETAL_WYVERN_BY_DURADEL,
			Assignment.SPIRITUAL_MAGE_BY_DURADEL,
			Assignment.STEEL_DRAGON_BY_DURADEL, Assignment.WATERFIEND_BY_DURADEL,
			Assignment.SMOKE_DEVIL_BY_DURADEL
	/* Assignment.WARPED_TERRORBIRD_BY_DURADEL, */ /*
													 * Assignment.
													 * SUQAH_BY_DURADEL,
													 */
	/* Assignment.VYREWATCH_BY_DURADEL, */ /*
											 * Assignment.
											 * GRIFOLAPINE_BY_DURADEL,
											 */ /*
												 * Assignment.
												 * GRIFOLAROO_BY_DURADEL,
												 */
	/* Assignment.FUNGAL_MAGE_BY_DURADEL, */ /*
												 * Assignment.
												 * GANODERMIC_CREATURE_BY_DURADEL,
												 */
	/* Assignment.CELESTIAL_DRAGON_BY_DURADEL, */ /*
													 * Assignment.
													 * ASCENSION_MEMBER_BY_DURADEL,
													 */
	/* Assignment.AUTOMATON_BY_DURADEL, */ /* Assignment.AIRUT_BY_DURADEL, */ /*
																				 * Assignment
																				 * .
																				 * ABERRANT_SPECTRE_BY_DURADEL,
																				 */ ),

	KURADAL(9085, 18, 90, 110, 75, Assignment.ABYSSAL_DEMON_BY_KURADAL,
			Assignment.AVIANSIE_BY_KURADAL, Assignment.AQUANITE_BY_KURADAL,
			Assignment.BLACK_DEMON_BY_KURADAL,
			Assignment.BLACK_DRAGON_BY_KURADAL, Assignment.BLOODVELD_BY_KURADAL,
			Assignment.BLUE_DRAGON_BY_KURADAL, Assignment.DAGANNOTH_BY_KURADAL,
			Assignment.DARK_BEAST_BY_KURADAL,
			Assignment.DESERT_STRYKEWYRM_BY_KURADAL,
			Assignment.DUST_DEVIL_BY_KURADAL, Assignment.FIRE_GIANT_BY_KURADAL,
			Assignment.GARGOYLE_BY_KURADAL, Assignment.GLACOR_BY_KURADAL,
			Assignment.GREATER_DEMON_BY_KURADAL,
			Assignment.HELLHOUND_BY_KURADAL,
			Assignment.ICE_STRYKEWYRM_BY_KURADAL,
			Assignment.IRON_DRAGON_BY_KURADAL,
			Assignment.JUNGLE_STRYKEWYRM_BY_KURADAL,
			Assignment.KALPHITE_BY_KURADAL,
			Assignment.MITHRIL_DRAGON_BY_KURADAL,
			Assignment.MUTATED_JADINKO_BY_KURADAL,
			Assignment.NECHRYAEL_BY_KURADAL,
			Assignment.SKELETAL_WYVERN_BY_KURADAL,
			Assignment.SPIRITUAL_MAGE_BY_KURADAL,
			Assignment.STEEL_DRAGON_BY_KURADAL,
			Assignment.TZHAAR_BY_KURADAL,
			Assignment.TORMENTED_DEMON_BY_KURADAL,
			Assignment.WATERFIEND_BY_KURADAL,
			Assignment.LIVING_ROCK_GOLEMS_BY_KURADAL
	/* Assignment.WARPED_TORTOISE_BY_KURADAL, */ /*
													 * Assignment.
													 * VYREWATCH_BY_KURADAL,
													 */
	/* Assignment.SUQAH_BY_KURADAL, */ /* Assignment.TERROR_DOG_BY_KURADAL, */ /*
																				 * Assignment
																				 * .
																				 * MUSPAH_BY_KURADAL,
																				 */
	/* Assignment.NIHIL_BY_KURADAL, */ /*
										 * Assignment.
										 * KALGERION_DEMON_BY_KURADAL,
										 */ /*
											 * Assignment.
											 * GRIFOLAPINE_BY_KURADAL,
											 */
	/* Assignment.GRIFOLAROO_BY_KURADAL, */ /*
											 * Assignment.
											 * GANODERMIC_CREATURE_BY_KURADAL,
											 */
	/* Assignment.ELVE_BY_KURADAL, */ /*
										 * Assignment.
										 * CELESTIAL_DRAGON_BY_KURADAL,
										 */
	/* Assignment.ASCENSION_MEMBER_BY_KURADAL, */ /*
													 * Assignment.
													 * AUTOMATON_BY_KURADAL,
													 */
	/* Assignment.ABERRANT_SPECTRE_BY_KURADAL, */ /*
													 * Assignment.
													 * AIRUT_BY_KURADAL,
													 */),

	MORVRAN(1188, 20, 100, 120, 85, Assignment.ABYSSAL_DEMON_BY_MORVRAN,
			//Assignment.AVIANSIE_BY_MORVRAN,
			//Assignment.AQUANITE_BY_MORVRAN,
			Assignment.BLACK_DEMON_BY_MORVRAN,
			Assignment.BLACK_DRAGON_BY_MORVRAN, Assignment.DAGANNOTH_BY_MORVRAN,
			Assignment.DARK_BEAST_BY_MORVRAN,
			//Assignment.DESERT_STRYKEWYRM_BY_MORVRAN,
			Assignment.GARGOYLE_BY_MORVRAN, Assignment.GLACOR_BY_MORVRAN,
			//Assignment.GREATER_DEMON_BY_MORVRAN,
			//Assignment.ICE_STRYKEWYRM_BY_MORVRAN,
			//Assignment.IRON_DRAGON_BY_MORVRAN, Assignment.KALPHITE_BY_MORVRAN,
			Assignment.MITHRIL_DRAGON_BY_MORVRAN,
			Assignment.MUTATED_JADINKO_BY_MORVRAN,
			Assignment.NECHRYAEL_BY_MORVRAN,
			//Assignment.STEEL_DRAGON_BY_MORVRAN,
			Assignment.TZHAAR_BY_MORVRAN, Assignment.TORMENTED_DEMON_BY_MORVRAN,
			//Assignment.WATERFIEND_BY_MORVRAN,
			Assignment.KRAKEN_BY_MORVRAN,
			Assignment.GENERAL_GRAARDOR_BY_MORVRAN,
			Assignment.KRIL_TSUTSAROTH_BY_MORVRAN,
			Assignment.KREE_ARRA_BY_MORVRAN,
			Assignment.COMMANDER_ZILYANA_BY_MORVRAN,
			Assignment.KING_BLACK_DRAGON_BY_MORVRAN,
			Assignment.CHAOS_ELEMENTAL_BY_MORVRAN,
			Assignment.CORPOREAL_BEAST_BY_MORVRAN,
			Assignment.DAGANNOTH_KINGS_BY_MORVRAN,
			Assignment.AVATAR_OF_DESTRUCTION_BY_MORVRAN,
			Assignment.SCORPIA_BY_MORVRAN, Assignment.CALLISTO_BY_MORVRAN, Assignment.VENENANTIS_BY_MORVRAN),
	/* Assignment.NIHIL_BY_MORVRAN, */
	/* Assignment.MUSPAH_BY_MORVRAN, */ /*
										 * Assignment.
										 * KALGERION_DEMON_BY_MORVRAN,
										 */
	/* Assignment.GANODERMIC_CREATURE_BY_MORVRAN, */ /*
														 * Assignment.
														 * ELVE_BY_MORVRAN,
														 */ /*
														 * Assignment.
														 * EDIMMUS_BY_MORVRAN,
														 */
	/* Assignment.CELESTIAL_DRAGON_BY_MORVRAN, */ /*
													 * Assignment.
													 * ASCENSION_MEMBER_BY_MORVRAN,
													 */
	/* Assignment.AUTOMATON_BY_MORVRAN, */ /* Assignment.AIRUT_BY_MORVRAN, */ /*
																				 * Assignment
																				 * .
																				 * VYREWATCH_BY_MORVRAN
																				 * ,
																				 * )
																				 * ;
																				 */
	
	KRYSTILIA(7663, 25, 110, 80, 1, 
			Assignment.GREEN_DRAGON_BY_KRYSTILIA,
			Assignment.MAGIC_AXES_BY_KRYSTILIA,
			Assignment.SKELETONS_BY_KRYSTILIA,
			Assignment.GIANT_SKELETONS_BY_KRYSTILIA,
			Assignment.HILL_GIANTS_BY_KRYSTILIA,
			Assignment.ZOMBIES_BY_KRYSTILIA,
			Assignment.HELLHOUNDS_BY_KRYSTILIA,
			Assignment.DEMONIC_GORILLAS_BY_KRYSTILIA,
			Assignment.ENTS_BY_KRYSTILIA,
			Assignment.SPIDERS_BY_KRYSTILIA,
			Assignment.BLACK_DEMON_BY_KRYSTILIA,
			Assignment.REVENANTS_BY_KRYSTILIA,
			Assignment.BLACK_DRAGON_BY_KRYSTILIA,
			Assignment.CHAOS_DRUIDS_BY_KRYSTILIA,
			Assignment.ANKOU_BY_KRYSTILIA,
			Assignment.LAVA_DRAGONS_BY_KRYSTILIA,
			Assignment.SKELETAL_HORROR_BY_KRYSTILIA,
			Assignment.CALLISTO_BY_KRYSTILIA,
			Assignment.CHAOS_ELE_BY_KRYSTILIA,
			Assignment.SCORPIA_BY_KRYSTILIA,
			Assignment.VENENATIS_BY_KRYSTILIA,
			Assignment.CRAZY_ARCHEOLOGIST_BY_KRYSTILIA,
			Assignment.CHAOS_FANATIC_BY_KRYSTILIA,
			Assignment.VETION_BY_KRYSTILIA,
			Assignment.BATTLE_MAGES_BY_KRYSTILIA,
			Assignment.FROST_DRAGONS_BY_KRYSTILIA,
			Assignment.ICE_WARRIORS_BY_KRYSTILIA,
			Assignment.LESSER_DEMONS_BY_KRYSTILIA,
			Assignment.ELDER_CHAOS_DRUIDS_BY_KRYSTILIA
			);
	
	private static final Map<Integer, Master> map = new HashMap<>();

	static {
		for (Master value : values()) {
			map.put(value.getId(), value);
		}
	}

	private final int id, points, duoExtraPts, combatLevel, slayerLevel;
	private final Assignment[] task;

	Master(int id, int points, int duoExtraPts, int combatLevel, int slayerLevel,
			Assignment... task) {
		this.id = id;
		this.points = points;
		this.duoExtraPts = duoExtraPts;
		this.combatLevel = combatLevel;
		this.slayerLevel = slayerLevel;
		this.task = task;
	}

	public static Optional<Master> lookup(int npcId) {
		return Optional.ofNullable(map.get(npcId));
	}

	public int getCombatLevel() {
		return combatLevel;
	}

	public int getId() {
		return id;
	}

	public int getPoints() {
		return points;
	}

	public int getDuoExtraPoints() {
		return duoExtraPts;
	}

	public int getSlayerLevel() {
		return slayerLevel;
	}

	public Assignment[] getTask() {
		return task;
	}

	public String getName() {
		String name = name().toLowerCase();
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

}
