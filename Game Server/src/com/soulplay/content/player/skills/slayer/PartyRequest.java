package com.soulplay.content.player.skills.slayer;

import com.soulplay.plugin.core.event.scheduled.ScheduledEvent;
import com.soulplay.plugin.core.event.scheduled.TimeUnit;
import com.soulplay.Server;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;

public class PartyRequest extends ScheduledEvent {

	private final Client sender;

	private final Client recipient;

	public PartyRequest(Client sender, Client recipient) {
		super(TimeUnit.MINUTE, (long) 1);
		this.sender = sender;
		this.recipient = recipient;
	}

	@Override
	public boolean cancel() {
		if (recipient.getPendingRequest() != null
				&& sender.getPendingRequest() != null) {
			Server.getTaskScheduler().schedule(new Task() {

				@Override
				protected void execute() {
					if (recipient != null && recipient.isActive) {
						recipient.getPA().closeAllWindows();
						recipient.sendMessage("Your invitation has expired.");
						recipient.setPendingRequest(null);
					}
					if (sender != null && sender.isActive) {
						sender.sendMessage("Your invitation has expired.");
						sender.setPendingRequest(null);
					}
					this.stop();
				}
			});
			return true;
		}
		return false;
	}

	public Client getRecipient() {
		return recipient;
	}

	public Client getSender() {
		return sender;
	}

	@Override
	public void run() {
		cancel();
	}

}
