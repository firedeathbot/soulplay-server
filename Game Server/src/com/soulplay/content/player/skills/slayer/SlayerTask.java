package com.soulplay.content.player.skills.slayer;

import java.util.Random;

import com.soulplay.game.model.player.Client;

public class SlayerTask {

	public static SlayerTask loadTask(String assignment, String master,
			int amount) {
		Object assigned = null;
		Object slayerMaster = null;
		Class<?> m = Master.class;
		Class<?> a = Assignment.class;
		if (assignment != null && master != null) {
			for (int i = 0; i < m.getEnumConstants().length; i++) {
				if (master
						.equalsIgnoreCase(m.getEnumConstants()[i].toString())) {
					slayerMaster = m.getEnumConstants()[i];
				}
			}
			for (int i = 0; i < a.getEnumConstants().length; i++) {
				if (assignment
						.equalsIgnoreCase(a.getEnumConstants()[i].toString())) {
					assigned = a.getEnumConstants()[i];
				}
			}
			// System.out.println(assignment);
			// System.out.println(assigned);
			// System.out.println(master);
			// System.out.println(slayerMaster);
			return new SlayerTask((Assignment) assigned, (Master) slayerMaster,
					amount);
		}
		return null;
	}

	private Assignment assignment;

	private int amount;

	// private Client partner;
	private Master slayerMaster;

	public SlayerTask(Assignment assignment, Master slayerMaster) {
		this.assignment = assignment;
		this.slayerMaster = slayerMaster;
		// this.amount = getCalculatedAmount(assignment);
		// new amount, reduced by 30% at least
		int newAmount = getCalculatedAmount(assignment);
		newAmount = newAmount - (newAmount / 3);
		this.amount = newAmount;
	}

	public SlayerTask(Assignment assignment, Master slayerMaster, int amount) {
		this.assignment = assignment;
		this.slayerMaster = slayerMaster;
		this.amount = amount;
	}

	public boolean cancel(Client player) {
		if (assignment != null) {
			assignment = null;
			amount = 0;
			player.taskName = " ";

			player.assignmentAmount = 0;
			return true;
		}
		return false;
	}

	public void deductKill(Client player) {
		amount--;
		player.assignmentAmount--;
	}

	public int getAmount() {
		return amount;
	}

	// public Client getPartner() {
	// return partner;
	// }
	//
	// public void setPartner(Client partner) {
	// this.partner = partner;
	// }

	public Assignment getAssignment() {
		return assignment;
	}

	private int getCalculatedAmount(Assignment assignment) {
		final Random random = new Random();
		return random.nextInt(assignment.getMax() - assignment.getMin())
				+ assignment.getMin();
	}

	public Master getSlayerMaster() {
		return slayerMaster;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setAssignment(Assignment assignment) {
		this.assignment = assignment;
	}

	public void setSlayerMaster(Master slayerMaster) {
		this.slayerMaster = slayerMaster;
	}
}
