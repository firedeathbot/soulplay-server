package com.soulplay.content.player.skills.slayer;

import java.util.LinkedList;
import java.util.List;

import com.soulplay.config.WorldType;
import com.soulplay.content.items.LarransKey;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;

public class Slayer {

	private static final int SLAYER_XP_PERCENTAGE = 2;

	private static final int DUO_POINTS_PER_TASK = 20;
	
	public static boolean hasTaskAssigned(Player player, int npcIdToCheck) {
		if (player.getSlayerTask() != null && player.getSlayerTask().getAssignment() != null) {
			return player.getSlayerTask().getAssignment().matchesAssignment(npcIdToCheck);
		}
		return false;
	}

	public static void appendKill(Client player, NPC npc) {
		SlayerTask slayerTask = player.getSlayerTask();
		if (slayerTask == null) {
			return;
		}

		Master slayerMaster = slayerTask.getSlayerMaster();
		Assignment assignment = slayerTask.getAssignment();
		if (assignment == null) {
			return;
		}

		for (int target : assignment.getIds()) {
			if (npc.npcType == target) {
				if (slayerMaster == Master.KRYSTILIA && player.wildLevel <= 0 && npc.npcType != 50) {
					player.sendMessage("The monster you were assigned can only be killed in the Wilderness.");
					return;
				}

				player.getSlayerTask().deductKill(player);
				player.getPA().addSkillXP(npc.getSkills().getMaximumLifepoints() / SLAYER_XP_PERCENTAGE, Skills.SLAYER);

				if (!WorldType.equalsType(WorldType.SPAWN)) {
					if (npc.getSkills().getMaximumLifepoints() < 51 && Misc.random(7) == 1) {
						ItemHandler.createGroundItem(player.getName(), 995,
								npc.absX, npc.absY, npc.heightLevel,
								350000 + Misc.random(250000), true,
								player.isIronMan());
						player.sendMessage(
								"Your slayer monster dropped a big stack of coins!");
					} else if (npc.getSkills().getMaximumLifepoints() >= 51 && npc.getSkills().getMaximumLifepoints() <= 101
							&& Misc.random(9) == 1) {
						ItemHandler.createGroundItem(player.getName(), 995,
								npc.absX, npc.absY, npc.heightLevel,
								500000 + Misc.random(500000), true,
								player.isIronMan());
						player.sendMessage(
								"Your slayer monster dropped a big stack of coins!");
					} else if (npc.getSkills().getMaximumLifepoints() >= 102 && Misc.random(11) == 1) {
						ItemHandler.createGroundItem(player.getName(), 995,
								npc.absX, npc.absY, npc.heightLevel,
								1000000 + Misc.random(650000), true,
								player.isIronMan());
						player.sendMessage(
								"Your slayer monster dropped a big stack of coins!");
					}
				}

				if (Misc.randomBoolean(10)) {
					ItemHandler.createGroundItem(player.getName(), 120718,
							npc.absX, npc.absY, npc.heightLevel,
							Misc.random(5, 10), true,
							player.isIronMan());
				}

				switch (slayerMaster) {
					case KONAR_QUO_MATEN:
						int combatLevel = npc.def.getCombatLevel();
						if (combatLevel <= 0) {
							break;
						}

						int rate;
						if (combatLevel >= 100) {
							rate = Misc.interpolate(100, 50, 100, 350, Math.min(combatLevel, 350));
						} else {
							rate = (int) (0.2f * Math.pow((combatLevel - 100), 2) + 100.0);
						}

						if (Misc.randomBoolean(rate)) {
							ItemHandler.createGroundItem(player.getName(), 123083,
									npc.absX, npc.absY, npc.heightLevel,
									1, true,
									player.isIronMan());
							player.sendMessage("<col=ff0000>Untradable drop: Brimstone key");
						}
						break;
					case KRYSTILIA:
						LarransKey.onSlayerMobKilled(player, npc);

						int maxLp = npc.getSkills().getMaximumLifepoints();
						if (maxLp <= 0) {
							break;
						}

						boolean statueDrop = Misc.random((1000 / maxLp) + 5) == 1;
						if (statueDrop) {
							int emblemId = -1;
							int roll = Misc.random(1000);
							if (roll > 500) {
								emblemId = 30181; // 3 mill
							} else if (roll > 250) {
								emblemId = 30182; // 5 mill
							} else  if (roll > 150) {
								emblemId = 30183; // 10 mill
							} else  if (roll > 75) {
								emblemId = 30184; // 50m
							} else  if (roll > 25) {
								emblemId = 30185; // 115m
							} else {
								emblemId = 30186; // 200m
							}

							ItemHandler.createGroundItem(player.getName(), emblemId,
									npc.absX, npc.absY, npc.heightLevel,
									1, true,
									player.isIronMan());

							if (emblemId == 30185 || emblemId == 30186) {
								PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + player.getNameSmartUp() + " just got a " + ItemConstants.getItemName(emblemId) +" from a Wilderness slayer task!");
							}
						}
						break;
					default:
						break;
				}

				if (player.getSlayerTask().getAmount() <= 0) {
					completeTask(player);
					if (hasPartner(player)) {
						completeTask(player.getPartner());
					}
				}

				break;
			}
		}
	}

	public static boolean assignTask(Client player, Master master) {
		if (player.combatLevel < master.getCombatLevel()) {
			player.sendMessage("You need a combat level of " + master.getCombatLevel() + " to use this slayer master.");
			return false;
		}

		if (player.getSkills().getStaticLevel(Skills.SLAYER) < master.getSlayerLevel()) {
			player.sendMessage("You need a Slayer level of "+ master.getSlayerLevel() + " to use this slayer master.");
			return false;
		}

		if (player.getSlayerTask() != null && player.getSlayerTask().getAssignment() != null) {
			player.getDialogueBuilder().sendNpcChat(master.getId(), "You haven't completed your current task!").executeIfNotActive();
			return false;
		}

		List<Assignment> availableTasks = new LinkedList<>();
		for (int i = 0, length = master.getTask().length; i < length; i++) {
			Assignment assignment = master.getTask()[i];
			if (player.getSkills().getStaticLevel(Skills.SLAYER) >= assignment.getSlayLevel()) {
				availableTasks.add(assignment);
			}
		}

		int size = availableTasks.size();
		if (size <= 0) {
			player.sendMessage("Error, no available tasks.");
			return false;
		}

		SlayerTask task = new SlayerTask(availableTasks.get(Misc.randomNoPlus(size)), master);

		final String taskName = task.getAssignment().toString().toLowerCase().replace("_", " ");
		final String text = "You have been assigned to kill " + task.getAmount() + " " + taskName + ".";

		if (hasPartner(player)) {
			player.setSlayerTask(task);
			player.getPartner().setSlayerTask(task);
			player.getPartner().getDialogueBuilder().sendNpcChat(task.getSlayerMaster().getId(), text).execute();
		}

		player.taskName = task.getAssignment().toString();
		player.masterName = master.toString();
		player.assignmentAmount = task.getAmount();
		player.setSlayerTask(task);
		player.sendMessage(text);
		player.getDialogueBuilder().sendNpcChat(text).executeIfNotActive();
		return true;
	}

	private static int calculatedPoints(Client player) {
		if (player.getSlayerTasksCompleted() % 50 == 0) {
			return player.getSlayerTask().getSlayerMaster().getPoints() * 15;
		} else if (player.getSlayerTasksCompleted() % 10 == 0) {
			return player.getSlayerTask().getSlayerMaster().getPoints() * 5;
		} else {
			return player.getSlayerTask().getSlayerMaster().getPoints();
		}
	}

	public static void checkKills(Client player) {
		// System.out.println(player.getTask().getAssignment());
		if (player.getSlayerTask() != null && player.getSlayerTask().getAssignment() != null) {
			player.sendMessage(String.format("You have %d %s remaining.", player.getSlayerTask().getAmount(), player.getSlayerTask().getAssignment().toString().toLowerCase().replace("_", " ")));
			//task.getAssignment().toString().toLowerCase().replace("_", " ")
		} else {
			player.sendMessage("You have completed your slayer assignment.");
		}
	}

	public static void completeTask(Client player) {
		player.setSlayerTasksCompleted(player.getSlayerTasksCompleted() + 1);
		player.getAchievement().slayerTask();
		if (player.getSlayerTask().getSlayerMaster() == Master.KURADAL) {
			player.getAchievement().kuradalTask();
		}

		if (player.getSlayerTask().getSlayerMaster() == Master.SUMONA) {
			player.getAchievement().sumonaTask();
		}

		int slayerPoints;
		if (hasPartner(player)) {
			slayerPoints = calculatedPoints(player) / 2;

			int duoPoints = DUO_POINTS_PER_TASK + player.getSlayerTask().getSlayerMaster().getDuoExtraPoints();
			player.getDuoPoints().add(duoPoints);
			player.sendMessage("You've been awarded " + duoPoints + " Co-op points.");

			player.getAchievement().completeDuoTask();
			player.getAchievement().earnDuoPoints(slayerPoints);
			if (player.getClan() != null) {
				player.getClan().addClanPoints(player, calculatedPoints(player) / 4, "complete_slayertask");
			}
		} else {
			slayerPoints = calculatedPoints(player);
			if (player.getClan() != null) {
				player.getClan().addClanPoints(player, calculatedPoints(player) / 2, "complete_slayer_task");
			}
		}

		player.getAchievement().earnSlayerPoints(slayerPoints);
		player.getSlayerPoints().add(slayerPoints);
		player.sendMessage("You have completed your slayer assignment,");
		player.sendMessage("You've earned " + slayerPoints + " Slayer points. You now have " + player.getSlayerPoints().getTotalValue() + ".");
		player.getPacketSender().sendFrame126("<col=ff7000>Slayer Points: <col=ffb000>" + player.getSlayerPoints().getTotalValue(), 16033);

		player.getSlayerTask().setAssignment(null);
		player.taskName = " ";
	}

	public static void decline(Client player) {
		player.getPendingRequest().getSender().sendMessage(player.getNameSmartUp() + " has declined your invitation.");
		player.getPendingRequest().getSender().setPendingRequest(null);
		player.setPendingRequest(null);
	}

	private static boolean hasPartner(Client player) {
		if (player.getPartner() != null) {
			return true;
		}

		return false;
	}

	public static boolean invitePlayer(Client player, Client target) {
		if (player.getSlayerTask() == null || player.getSlayerTask().getAssignment() == null) {player.sendMessage(
				"You do not have a slayer task.");
			return false;
		}
		if (target.getSlayerTask() == null || target.getSlayerTask().getAssignment() == null) {
			player.sendMessage("The other player does not have a slayer task/assignment.");
			return false;
		}
		if (player.getSlayerTask().getAssignment() != target.getSlayerTask()
				.getAssignment()) {
			player.sendMessage(
					"That player does not have the same task as you.");
			return false;
		} else if (target.getPartner() != null) {
			player.sendMessage("That player is already in a slayer group.");
			return false;
		} else if (player.getPartner() != null) {
			player.sendMessage("You've already got a partner.");
			return false;
		} else if (player.equals(target)) {
			player.sendMessage("You can't invite yourself.");
			return false;
		} else if (player.getPendingRequest() != null) {
			player.sendMessage("You've already got an invitation pending.");
			return false;
		} else if (target.getPendingRequest() != null) {
			player.sendMessage("That player already has a pending request.");
			return false;
		}
		player.sendMessage("Your request has been sent.");
		target.sendMessage(
				player.getNameSmartUp() + " has invited you to their slayer group.");
		target.getDH().sendOption2("Yes i would like to do a slayer duo with "
				+ player.getNameSmartUp() + ".", "No thanks.");
		target.dialogueAction = 4155;
		PartyRequest request = new PartyRequest(player, target);
		target.setPendingRequest(request);
		player.setPendingRequest(request);
		return true;
	}

	public static boolean joinGroup(Client recipient, Client sender) {
		if (recipient != null && sender != null) {
			if (sender.getPartner() == null) {
				sender.sendMessage(
						recipient.getNameSmartUp() + " has joined your duo group.");
				sender.setPartner(recipient);
				recipient.setPartner(sender);
				recipient.setPendingRequest(null);
				sender.setPendingRequest(null);
				return true;
			}
		}
		return false;
	}

	public static void leaveGroup(Client player) {
		if (hasPartner(player)) {
			player.getPartner()
					.sendMessage(player.getNameSmartUp() + " has left the group.");
			player.getSlayerTask().cancel(player);
			player.getSlayerTask().cancel(player.getPartner());
			player.sendMessage("Slayer Task has been reset.");
			player.getPartner().sendMessage("Slayer Task has been reset.");
			player.getPartner().setPartner(null);
			player.setPartner(null);
		}
	}

	// private static int calculateAmount(Master master) {
	// if(master.ordinal() > 1) {
	// return master.ordinal() * 10;
	// }
	// return master.ordinal();
	// }

}
