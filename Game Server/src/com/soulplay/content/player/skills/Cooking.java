package com.soulplay.content.player.skills;

import com.soulplay.content.items.Food;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.GameObject;
import com.soulplay.util.Misc;

/**
 * Class Cooking Handles Cooking
 *
 * @author 2012 START: 20:13 25/10/2010 FINISH: 20:21 25/10/2010
 */

public class Cooking extends SkillHandler {
	
	private static void cook(Client c, int itemId, int xpReceived,
			int levelRequired, int burntId, int cookedId, int objectId) {
		cook(c, itemId, -1, -1, xpReceived, levelRequired, burntId, cookedId, objectId);
	}

	private static void cook(Client c, int itemId, int itemId2, int xpReceived,
			int levelRequired, int burntId, int cookedId, int objectId) {
		cook(c, itemId, itemId2, -1, xpReceived, levelRequired, burntId, cookedId, objectId);
	}

	private static void cook(Client c, int itemId, int itemId2, int itemId3, int xpReceived,
			int levelRequired, int burntId, int cookedId, int objectId) {
		if (!hasRequiredLevel(c, 7, levelRequired, "cooking", "cook this")) {
			return;
		}
		int chance = (c.getPlayerLevel()[7] + (int) (c.getPlayerLevel()[7] * .15));
		if (c.playerEquipment[PlayerConstants.playerHands] == 775) {
			chance = c.getPlayerLevel()[7] + 8;
		}
		if (chance <= 0) {
			chance = Misc.random(5);
		}
		if (objectId == 114) {
			chance += 5;
		}
		c.playerSkillProp[7][0] = itemId;
		c.playerSkillProp[7][1] = xpReceived;
		c.playerSkillProp[7][2] = levelRequired;
		c.playerSkillProp[7][3] = burntId;
		c.playerSkillProp[7][4] = cookedId;
		c.playerSkillProp[7][5] = objectId;
		c.playerSkillProp[7][6] = chance;
		c.playerSkillProp[7][7] = itemId2;
		c.playerSkillProp[7][8] = itemId3;
		c.playerIsSkilling = false;
		int item = c.getItems().getItemAmount(c.playerSkillProp[7][0]);
		if (item == 1) {
			c.doAmount = 1;
			cook(c);
			return;
		}
		if (!c.isBot()) {
			viewCookInterface(c, itemId);
		} else {
			c.doAmount = 28;
			cook(c);
		}
	}

	public static void cook(final Client c) {
		c.getPA().closeAllWindows();

		final int itemId = c.playerSkillProp[7][0];
		final int xpReceived = c.playerSkillProp[7][1];
		final int levelRequired = c.playerSkillProp[7][2];
		final int burntId = c.playerSkillProp[7][3];
		final int cookedId = c.playerSkillProp[7][4];
		final int objectId = c.playerSkillProp[7][5];
		final int chance = c.playerSkillProp[7][6];
		// final int itemId2 = c.playerSkillProp[7][7];
		// final int itemId3 = c.playerSkillProp[7][8];

		c.startActionChecked(new CycleEvent() {

			int tick = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (tick % 2 == 0) {
					if (!completeCookTask(c, itemId, chance, levelRequired, cookedId, xpReceived, burntId, objectId)) {
						container.stop();
					}
				}

				if (tick % 8 == 0) {
					c.getPacketSender().playSound(Sounds.COOK_FOOD);
				}

				tick++;
			}

			@Override
			public void stop() {
			}
		}, 1, true);
	}
	
	private static void cookAnimation(Player p, int objectId) {
		if (p.getPauseAnimReset() > 0)
			return;
		p.startAnimation(objectId == 2732 ? 897 : 896);
		p.setPauseAnimReset(4);
	}
	
	private static boolean completeCookTask(Player c, int itemId, int chance, int levelRequired, int cookedId, int xpReceived, int burntId, int objectId) {
		if (!c.getItems().deleteItemInOneSlot(itemId,
				c.getItems().getItemSlot(itemId), 1)) {
			return false;
		}
		cookAnimation(c, objectId);
		if (c.getPlayerLevel()[7] >= fishStopsBurning(
				itemId)
				|| Misc.random(chance) > Misc
				.random(levelRequired)) {
			if (cookedId == 391) {
				c.getAchievement().cookManta();
			} else if (cookedId == 15272) {
				c.getAchievement().cookRocktails();
			}
			c.sendMessageSpam(
					"You successfully cook the " + ItemConstants
							.getItemName(itemId)
							.toLowerCase() + ".");
			if (xpReceived > 0)
				c.getPA().addSkillXP(xpReceived, 7);
			c.getItems().addItem(cookedId, 1);
			c.getAchievement().CookFish();
			c.increaseProgress(TaskType.COOKING, itemId);
			SkillingUrns.handleUrn(c, Skills.COOKING, levelRequired, xpReceived);
		} else if (!c.isBot()) {
			c.sendMessageSpam(
					"Oops! You accidentally burnt the " + ItemConstants
							.getItemName(itemId)
							.toLowerCase() + "!");
			c.getItems().addItem(burntId, 1);
			c.getAchievement().burnFish();
		}
		deleteTime(c);
		if (!c.getItems().playerHasItem(itemId, 1)
				|| c.doAmount <= 0) {
			return false;
		}
		return true;
	}

	public static void cookThisFood(Client player, int itemId, int objectId, GameObject gameObject) {
		switch (itemId) {
			case 317:
				cook(player, itemId, 30, 1, 323, 315, objectId);
				break;
			case 2307:
				cook(player, itemId, 1, 1, 2309, 2309, objectId);
				break;
			case 321:
				cook(player, itemId, 30, 1, 323, 319, objectId);
				break;
			case 327:
				cook(player, itemId, 40, 1, 367, 325, objectId);
				break;
			case 345:
				cook(player, itemId, 50, 5, 357, 347, objectId);
				break;
			case 353:
				cook(player, itemId, 60, 10, 357, 355, objectId);
				break;
			case 335:
				cook(player, itemId, 70, 15, 343, 333, objectId);
				break;
			case 341:
				cook(player, itemId, 75, 18, 343, 339, objectId);
				break;
			case 349:
				cook(player, itemId, 80, 20, 343, 351, objectId);
				break;
			case 331:
				cook(player, itemId, 90, 25, 343, 329, objectId);
				break;
			case 359:
				cook(player, itemId, 100, 30, 367, 361, objectId);
				break;
			case 363:
				cook(player, itemId, 100, 30, 367, 365, objectId);
				break;
			case 2138:
				cook(player, itemId, 30, 1, 2144, 2140, objectId);
				break;
			case 2132:
				if (gameObject != null && gameObject.getDef().getName().toLowerCase().contains("range")) {
					player.getDialogueBuilder().sendOption("Dry the meat into sinew.", () -> {
						cook(player, itemId, 30, 1, 2146, 9436, objectId);
					}, "Cook the meat.", () -> {
						cook(player, itemId, 30, 1, 2146, 2142, objectId);
					}).execute(false);
				} else {
					cook(player, itemId, 30, 1, 2146, 2142, objectId);
				}
				break;
			case 377:
				cook(player, itemId, 120, 40, itemId + 4, itemId + 2, objectId);
				break;
			case 371:
				cook(player, itemId, 140, 45, itemId + 4, itemId + 2, objectId);
				break;
			case 3142:
				cook(player, itemId, 190, 30, itemId + 4, itemId + 2, objectId);
				break;
			case 383:
				cook(player, itemId, 210, 80, itemId + 4, itemId + 2, objectId);
				break;
			case 395:
				cook(player, itemId, 212, 82, itemId + 4, itemId + 2, objectId);
				break;
			case 389:
				cook(player, itemId, 216, 91, itemId + 4, itemId + 2, objectId);
				break;
			case 7944:
				cook(player, itemId, 150, 62, itemId + 4, itemId + 2, objectId);
				break;
			case 15270:
				cook(player, itemId, 225, 93, itemId + 4, itemId + 2, objectId);
				break;
			case 113439:
				cook(player, itemId, 230, 84, itemId + 4, itemId + 2, objectId);
				break;
			//TODO potato stuff
			case 17817://raw cave potato
				cook(player, itemId, 9, 1, 6699, 18093, objectId);
				break;
			case 17819://gissel mushroom
				cook(player, itemId, 17817, 12, 1, 6699, 18095, objectId);
				break;
			case 17821://edicap mushroom
				cook(player, itemId, 17817, 16, 1, 6699, 18097, objectId);
				break;
			case 17797://raw heim crab
				cook(player, itemId, 22, 1, 18179, 18159, objectId);
				break;
			case 17799://raw red eye
				cook(player, itemId, 41, 10, 18181, 18161, objectId);
				break;
			case 17801://raw dusk eel
				cook(player, itemId, 61, 20, 18183, 18163, objectId);
				break;
			case 17803://raw flatfish
				cook(player, itemId, 82, 30, 18185, 18165, objectId);
				break;
			case 17805://raw finned eel
				cook(player, itemId, 103, 40, 18187, 18167, objectId);
				break;
			case 17807://raw web snipper
				cook(player, itemId, 124, 50, 18189, 18169, objectId);
				break;
			case 17809://raw bouldabass
				cook(player, itemId, 146, 60, 18191, 18171, objectId);
				break;
			case 17811://raw salve eel
				cook(player, itemId, 168, 70, 18193, 18173, objectId);
				break;
			case 17813://raw blue crab
				cook(player, itemId, 191, 80, 18195, 18175, objectId);
				break;
			case 17815://raw cave moray
				cook(player, itemId, 215, 90, 18197, 18177, objectId);
				break;
				
				//soda ash
			case 401://seaweed
			case 14830: // giant seaweed?
			case 10978: // swamp weed
				cook(player, itemId, 0, 1, 0, 1781, objectId);
				break;
				
				
			default:
				if (Food.isFood(itemId)) {
					ItemDefinition food = ItemDefinition.forId(itemId);
					ItemDefinition burnedFood = ItemDefinition.forId(itemId + 2);
					if (burnedFood != null && food != null) {
						if (burnedFood.getName().toLowerCase()
								.contains("burnt ")
								&& burnedFood.getName().toLowerCase().contains(
										food.getName().toLowerCase())) {
							player.startAnimation(objectId == 2732 ? 897 : 896);
							player.sendMessage(
									"You burnt your " + food.getName() + "!");
							player.getAchievement().burnFish();
							player.getItems().deleteItemInOneSlot(itemId, 1);
							player.getItems().addItem(burnedFood.getId(), 1);
							break;
						}
					}
				} else {
					player.sendMessage("You can't cook this!");
				}
				break;
		}
	}

	private static int fishStopsBurning(int i) {
		switch (i) {
			case 317:
			case 17797:
			case 2307:
			case 321:
			case 2138:
			case 2132:
				return 34;
			case 327:
				return 38;
			case 345:
				return 37;
			case 353:
			case 17799:
				return 45;
			case 335:
				return 50;
			case 341:
				return 39;
			case 349:
			case 17801:
			    return 53;
			case 331:
				return 58;
			case 359:
			case 17803:
				return 65;
			case 377:
			case 17805:
				return 74;
			case 363:
			case 17807:
				return 80;
			case 371:
				return 86;
			case 7944:
			case 17809:
				return 90;
			case 383:
			case 17811:
				return 94;
			case 17813:
				return 97;
				
			case 401://seaweed
			case 14830: // giant seaweed?
			case 10978: // swamp weed
				return 1;
				
			default:
				return 99;
		}
	}

	public static void getAmount(Client c, int amount) {
		if (c.duelStatus > 0) {
			c.sendMessage("You Cannot cook while dueling.");
			return;
		}
		if (c.playerSkillProp[7][0] < 1) {
			return;
		}
		int item = c.getItems().getItemAmount(c.playerSkillProp[7][0]);
		if (amount > item) {
			amount = item;
		}
		c.doAmount = amount;
		cook(c);
	}

	public static void resetCooking(Client c) {
		c.playerSkilling[7] = false;
		c.playerIsSkilling = false;
		c.isCooking = false;
		for (int i = 0; i < 6; i++) {
			c.playerSkillProp[7][i] = -1;
		}
	}

	private static void viewCookInterface(Client c, int item) {
		c.getPacketSender().sendChatInterface(1743);
		c.getPacketSender().sendFrame246(13716, 190, item);
		c.getPacketSender().sendFrame126(
				"\\n\\n\\n\\n\\n" + ItemConstants.getItemName(item) + "",
				13717);
	}
}
