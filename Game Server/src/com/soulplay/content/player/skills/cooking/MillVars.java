package com.soulplay.content.player.skills.cooking;

public class MillVars {

	private boolean hopperFull;
	
	private int flourCollect;
	
	public MillVars() {
	}
	
	public boolean isHopperFull() {
		return hopperFull;
	}
	public void setHopperFull(boolean hopperFull) {
		this.hopperFull = hopperFull;
	}

	public int getFlourCollectAmount() {
		return flourCollect;
	}

	public void increaseFlour() {
		this.flourCollect++;
	}
	
	public void decreaseFlour() {
		if (this.flourCollect > 0)
			this.flourCollect--;
	}
	
	public void emptyFlourBin() {
		this.flourCollect = 0;
	}
}
