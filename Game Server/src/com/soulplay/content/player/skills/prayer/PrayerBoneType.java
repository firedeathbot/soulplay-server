package com.soulplay.content.player.skills.prayer;

import java.util.function.Consumer;

import com.soulplay.game.model.player.Player;

public enum PrayerBoneType {

	BONES(526, 5, 1),
	WOLF_BONES(2859, 5, 1),
	BIG_BONES(532, 15, 2),
	BABY_DRAGON_BONES(534, 15, 3),
	WYVERN_BONES(6812, 50, 3),
	INFERNAL_ASHES(22312, 63, 0),
	DRAGON_BONES(536, 72, 4, player -> player.getAchievement().buryDragonBones()),
	DAGANNOTH_BONES(6729, 125, 4),
	OURG_BONES(4834, 140, 0),
	FROST_DRAGON_BONES(18830, 180, 5, player -> player.getAchievement().buryFrostBones()),
	LAVA_DRAGON_BONES(17676, 85, 0),
	SUPERIOR_DRAGON_BONES(30333, 150, 5),
	HYDRA_BONES(122786, 110, 4),
	DRAKE_BONES(122783, 80, 4),
	WYRM_BONES(122780, 50, 4),;

	public static final PrayerBoneType[] values = values();
	private final int id, exp, prayerRestore;
	private final Consumer<Player> onBury;

	PrayerBoneType(int id, int exp, int prayerRestore) {
		this(id, exp, prayerRestore, null);
	}

	PrayerBoneType(int id, int exp, int prayerRestore, Consumer<Player> onBury) {
		this.id = id;
		this.exp = exp;
		this.prayerRestore = prayerRestore;
		this.onBury = onBury;
	}

	public int getId() {
		return id;
	}

	public int getExp() {
		return exp;
	}

	public int getPrayerRestore() {
		return prayerRestore;
	}

	public Consumer<Player> getOnBury() {
		return onBury;
	}

	public static PrayerBoneType list(int id) {
		for (int i = 0, length = values.length; i < length; i++) {
			PrayerBoneType bone = values[i];
			if (bone.getId() == id) {
				return bone;
			}
		}

		return null;
	}

}
