package com.soulplay.content.player.skills.prayer;

import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerAssistant;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;
import com.soulplay.game.sounds.Sounds;

public class Prayer {

	public static void bonesOnAltar2(Player player, final int itemId) {
		if (player.skillingEvent != null) {
			return;
		}

		PrayerBoneType bone = PrayerBoneType.list(itemId);
		if (bone == null) {
			return;
		}

		int amount = player.getItems().getItemAmount(itemId);
		if (amount <= 0) {
			return;
		}

		if (!player.attr().getOrDefault("usingAltar", false)) {
			if (player.inWild() && Misc.random(1) == 0) {
				player.sendMessage("The chaos altar prevented you from consuming your bone.");
		    } else {
				player.getItems().deleteItemInOneSlot(itemId, player.getItems().getItemSlot(itemId),1);	
				amount--;
			}
			player.sendMessageSpam("The gods are pleased with your offering.");
			player.getPA().addSkillXP(bone.getExp() * 2, Skills.PRAYER);
			player.startAnimation(3705);
			player.getPA().stillGfx(624, player.prevObjectX, player.prevObjectY, player.getHeightLevel(),10);
			player.attr().put("usingAltar", true);
			player.increaseProgress(TaskType.PRAYER_ALTAR, itemId);
		}

		if (amount <= 0) {
			player.attr().put("usingAltar", false);
			return;
		}

		final int finalAmount = amount;

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int executes = 0;

			@Override
			public void execute(CycleEventContainer altar) {
				if (executes < finalAmount && player.getItems().playerHasItem(itemId, 1) && player.attr().getOrDefault("usingAltar", false)) {
					if (player.inWild() && Misc.random(1) == 0) {
					    player.sendMessage("The chaos altar prevented you from consuming your bone.");
					} else {
					    player.getItems().deleteItemInOneSlot(itemId, player.getItems().getItemSlot(itemId),1);	
				    }   
					player.sendMessageSpam("The gods are pleased with your offering.");
					player.getPA().addSkillXP(bone.getExp() * 2, Skills.PRAYER);
					player.startAnimation(3705);
					player.getPA().stillGfx(624, player.prevObjectX, player.prevObjectY, player.getHeightLevel(), 10);
					player.increaseProgress(TaskType.PRAYER_ALTAR, itemId);
					executes++;
				} else {
					altar.stop();
				}
			}

			@Override
			public void stop() {
				player.attr().put("usingAltar", false);
			}
		}, 6);
	}

	public static boolean buryBone(Player player, int id, int slot) {
		PrayerBoneType bone = PrayerBoneType.list(id);
		if (bone == null) {
			return false;
		}

		if (!player.prayerBuryTimer.complete()) {
			return true;
		}

		player.prayerBuryTimer.startTimer(1);
		if (bone.getOnBury() != null) {
			bone.getOnBury().accept(player);
		}

		player.getItems().deleteItemInOneSlot(id, slot, 1);
		player.sendMessageSpam("You bury the bones.");
		int multipler = 1;
		if (bone == PrayerBoneType.LAVA_DRAGON_BONES && RSConstants.inLavaDragonArea(player.getX(), player.getY())) {
			multipler = 5;
		}

		player.getPA().addSkillXP(bone.getExp() * multipler, Skills.PRAYER);
		player.startAnimation(827);
		player.getPacketSender().playSound(Sounds.BURY_BONE);
		player.getAchievement().buryBones();
		player.increaseProgress(TaskType.PRAYER_BURY, id);
		executeBuryEffects(player, bone);

		if (player.soulWarsTeam != Team.NONE) {
			if (Misc.random(1) == 0 && id == 526) {
				Team team = player.soulWarsTeam;
				if (team == Team.BLUE) {
					if (RSConstants.inBlueGraveyardRoom(player.absX, player.absY)
							|| (SoulWars.blueGraveyard.team == Team.BLUE && RSConstants.inBlueGraveyard(player.absX, player.absY))
							|| (SoulWars.redGraveyard.team == Team.BLUE && RSConstants.inRedGraveyard(player.absX, player.absY))) {
						SoulWars.blueAvatar.increaseLevel();
						player.decreaseInactivity(75);
					}
				} else if (team == Team.RED) {
					if (RSConstants.inRedGraveyardRoom(player.absX, player.absY)
							|| (SoulWars.blueGraveyard.team == Team.RED && RSConstants.inBlueGraveyard(player.absX, player.absY))
							|| (SoulWars.redGraveyard.team == Team.RED && RSConstants.inRedGraveyard(player.absX, player.absY))) {
						SoulWars.redAvatar.increaseLevel();
						player.decreaseInactivity(75);
					}
				}
	
				player.decreaseInactivity(1);
				player.healPrayer(1);
			}
		}

		return true;
	}

	public static boolean handleBoneCrusher(Player player, int itemId) {
		PrayerBoneType bone = PrayerBoneType.list(itemId);
		if (bone == null) {
			return false;
		}

		boolean hasBoneCrusher = false;
		if (player.getItems().playerHasItem(18337) || player.playerEquipment[PlayerConstants.playerAmulet] == 122986) {
			hasBoneCrusher = true;
		}

		if (!hasBoneCrusher) {
			return false;
		}

		int experience = bone.getExp();
		player.sendMessageSpam("Your bonecrusher grinds the bones to dust, and grants you " + (PlayerAssistant.getMultiplier(Skills.PRAYER, player) * experience) + " Prayer experience.");
		player.getPA().addSkillXP(experience, Skills.PRAYER);
		executeBuryEffects(player, bone);
		return true;
	}

	public static void executeBuryEffects(Player player, PrayerBoneType bone) {
		int necklaceId = player.playerEquipment[PlayerConstants.playerAmulet];
		switch (necklaceId) {
			case 30332:
			case 122986:
				int prayerRestore = bone.getPrayerRestore();
				if (prayerRestore <= 0 || player.getSkills().prayerPointsFull()) {
					return;
				}

				if (!player.dragonBoneNecklaceTimer.complete()) {
					return;
				}

				player.getSkills().incrementPrayerPoints(prayerRestore);
				player.sendMessageSpam("Your necklace restores some of your prayer points.");
				return;
		}
	}

	public static final Animation PRAY_ANIM = new Animation(645);

	public static void clickAltar(Player player) {
		if (player.wildLevel > 0) {
			player.sendMessage("You cannot use this yet. Wait for timer to reach 0.");
			return;
		}

		player.prayerDrainCounter = 0;
		if (player.getSkills().prayerPointsFull()) {
			player.sendMessage("You already have full prayer points.");
			return;
		}

		player.startAnimation(PRAY_ANIM);
		player.getPacketSender().playSound(Sounds.PRAYER_ALTAR);
		player.getSkills().setPrayerPointsToMax();
		player.sendMessage("You recharge your prayer points.");
	}

}
