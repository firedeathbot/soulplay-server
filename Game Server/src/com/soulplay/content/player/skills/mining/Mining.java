package com.soulplay.content.player.skills.mining;

import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.SkillHandler;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

public class Mining extends SkillHandler {
	
	private static final int prospectorHelmet = 30318;
	private static final int prospectorLegs = 30320;
	private static final int prospectorJacket = 30319;
	private static final int prospectorBoots = 30321;

	private static final int[] UNCUT_GEMS = {1617, 1619, 1621, 1623, 1625, 1627, 1629};

	public static boolean attemptData(final Client c, final GameObject o) {
		if (c.skillingEvent != null) {
			return true;
		}

		Rocks data = Rocks.values.get(o.getId());
		if (data == null) {
			return false;
		}

		Pickaxes pickaxe = getPickaxe(c);
		if (pickaxe == null) {
			return true;
		}

		if (!hasInventorySpace(c, "mining")) {
			resetMining(c);
			return true;
		}
		if (!hasRequiredLevel(c, Skills.MINING, data.getLevel(), "mining",
				"mine here")) {
			return true;
		}

		c.sendMessageSpam("You swing your pick at the rock.");
		c.startAnimation(pickaxe.getAnimation());

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (!hasInventorySpace(c, "mining")) {
					resetMining(c);
					container.stop();
					return;
				}
				if (ObjectManager.objectExists(451, o.getX(), o.getY(), o.getZ())) {
					container.stop();
					return;
				}

				if (data == Rocks.ADAMANT) {
					c.getAchievement().mineAddy();
				} else if (data == Rocks.RUNE) {
					c.getAchievement().Mine100Rune();
					c.getAchievement().Mine300Rune();
				}

				int produce = 0;
				if (data == Rocks.GEM) {
					int random = Misc.random(1000);
					int index = 0;
					if (random >= 650) {
						index = 0;
					} else if (random >= 350) {
						index = 1;
					} else if (random >= 100) {
						index = 2;
					} else if (random >= 10) {
						index = 3;
					} else if (random >= 0) {
						index = 4;
					}

					produce = data.getProduce()[index];
				} else {
					produce = data.getProduce()[0];
				}

				c.increaseProgress(TaskType.MINING, produce);
				c.getItems().addItem(produce, 1);
				c.sendMessageSpam("You manage to mine some " + ItemConstants.getItemName(produce).toLowerCase() + ".");
				c.getAchievement().mineOres();
				int xp = data.getXp();
				if (c.inWild()) {
					xp *= 2;
				}

				c.getPA().addSkillXP(applyMiningXpBoosts(c, xp), Skills.MINING);
				SkillingUrns.handleUrn(c, Skills.MINING, data.getLevel(), xp);
				if (data.getGeodeRate() > 0)
					c.getTreasureTrailManager().rollClueThingy(data.getLevel(), data.getGeodeRate(), Skills.MINING, "You mine a clue geode!");
				if (Misc.random(16) == 7) {
					ObjectManager.addObject(new RSObject(451, o.getX(), o.getY(), o.getZ(),
							o.getOrientation(), o.getType(), o.getId(),
							4 + Misc.random(7)));
					container.stop();
				}

				if (Misc.random(100) == 7) {
					if (c.getItems().freeSlots() > 1) {
						c.getItems().addItem(getRandomGem(), 1);
						c.sendMessage("You get a gem!");
					}
				} else if (Misc.random(1000) == 7) {
					if (c.getItems().freeSlots() > 1) {
						if (Misc.random(2) <= 1) {
							c.getItems().addItem(1631, 1);
						} else {
							c.getItems().addItem(6571, 1);
						}
						c.sendMessage("You get a rare gem!");
					}
				}
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, getTimer(c, data, pickaxe.getTime()));

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (c.skillingEvent == null) {
					container.stop();
					return;
				}

				c.startAnimation(pickaxe.getAnimation());
			}

			@Override
			public void stop() {

			}
		}, 4);
		return true;
	}

	public static int getRandomGem() {
		return UNCUT_GEMS[Misc.randomNoPlus(UNCUT_GEMS.length)];
	}

	private static int getTimer(Client c, Rocks rocks, int pickTime) {
		return (rocks.getMineTime() + pickTime + playerMiningLevel(c));
	}

	public static void mineEss(final Client c, final int object) {
		if (c.skillingEvent != null) {
			return;
		}

		if (!hasInventorySpace(c, "mining")) {
			resetMining(c);
			return;
		}

		Pickaxes pickaxe = getPickaxe(c);
		if (pickaxe == null) {
			return;
		}

		c.startAnimation(pickaxe.getAnimation());

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().addItem(1436, 1);
				if (Misc.random(100) == 7) {
					if (c.getItems().freeSlots() > 1) {
						c.getItems().addItem(getRandomGem(), 1);
						c.sendMessage("You get a gem!");
					}
				} else if (Misc.random(1000) == 7) {
					if (c.getItems().freeSlots() > 1) {
						if (Misc.random(2) <= 1) {
							c.getItems().addItem(1631, 1);
						} else {
							c.getItems().addItem(6571, 1);
						}
						c.sendMessage("You get a rare gem!");
					}
				}

				c.sendMessageSpam("You manage to mine some " + ItemConstants.getItemName(1436).toLowerCase() + ".");
				c.getPA().addSkillXP(applyMiningXpBoosts(c, 15), Skills.MINING);
				c.startAnimation(pickaxe.getAnimation());

				if (!hasInventorySpace(c, "mining")) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
				resetMining(c);
			}
		}, 2);
	}

	private static int playerMiningLevel(Client c) {
		return (10 - (int) Math.floor(c.getPlayerLevel()[Skills.MINING] / 10));
	}

	public static void resetMining(Player c) {
		c.playerSkilling[14] = false;
		c.playerIsSkilling = false;
		c.getPA().resetVariables();
		for (int i = 0; i < 2; i++) {
			c.playerSkillProp[14][i] = -1;
		}
		c.resetAnimations();
	}

	public static Pickaxes getPickaxe(Player c) {
		Pickaxes toReturn = null;
		Pickaxes compare = null;
		int myLevel = c.getSkills().getLevel(Skills.MINING);
		boolean lvlReqMet = false;
		// search equipment
		if (c.playerEquipment[3] > 0) {
			toReturn = Pickaxes.values.get(c.playerEquipment[3]);

			if (toReturn != null) {
				if (toReturn.getLevel() > myLevel) {
					toReturn = null;
				} else {
					lvlReqMet = true;
				}
			}
		}
		// search inv
		for (int i = 0, length = c.playerItems.length; i < length; i++) {
			int itemId = c.playerItems[i] - 1;
			if (itemId > 0)
				compare = Pickaxes.values.get(itemId);

			if (compare != null && compare.getLevel() <= myLevel) {
				lvlReqMet = true;
				if (toReturn == null) {
					toReturn = compare;
				} else if (toReturn != null && toReturn.getLevel() < compare.getLevel()) {
					toReturn = compare;
				}
			}
		}
		if (!lvlReqMet) {
			c.sendMessage("You do not have a pickaxe of your level.");
		} else if (lvlReqMet && toReturn == null) {
			c.sendMessage("You need a pickaxe to mine.");
		}
		return toReturn;
	}

	public static int applyMiningXpBoosts(Player player, int xp) {
		return (int) ((double) xp * calculateProspectorOutfitXpModifier(player));
	}

	public static double calculateProspectorOutfitXpModifier(Player player) {
		double modifier = 1.0;

		if (player.getItems().playerHasEquipped(prospectorHelmet, PlayerConstants.playerHat)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(prospectorJacket, PlayerConstants.playerChest)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(prospectorLegs, PlayerConstants.playerLegs)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(prospectorBoots, PlayerConstants.playerFeet)) {
			modifier += 0.05;
		}
		return modifier;
	}
}
