package com.soulplay.content.player.skills.mining;

import java.util.HashMap;
import java.util.Map;

public enum Rocks {

	CLAY(new int[] { 9711, 9713, 15504, 15503, 15505, 2108, 2109, 11189, 11191, 11190 }, new short[] { 434 }, 1, 18, 1, 741600),
	COPPER(new int[] { 2090, 2091, 11960, 11961, 11962, 21284, 21285, 21286 }, new short[] { 436 }, 1, 18, 1, 741600),
	TIN(new int[] { 2094, 2095, 9714, 9716, 11957, 11958, 11959, 21293, 21294, 21295, 11186, 11188, 11187 }, new short[] { 438 }, 1, 18, 1, 741600),
	IRON(new int[] { 2092, 2093, 11954, 11955, 11956, 14856, 14857, 14858, 21281, 21282, 21283, 6943, 6944, 107455, 107488, 111364 }, new short[] { 440 }, 15, 35, 2, 741600),
	SILVER(new int[] { 2101, 6946, 6945 }, new short[] { 442 }, 20, 40, 2, 741600),
	COAL(new int[] { 2096, 2097, 9717, 9718, 9719, 14850, 14851, 14852, 21287, 21288, 21289, 107489, 107456, 111366 }, new short[] { 453 }, 30, 50, 3, 296640),
	GOLD(new int[] { 2098, 2099, 9720, 9722, 21290, 21291, 11185, 11184, 11183, 107491, 111370 }, new short[] { 444 }, 40, 65, 3, 296640),
	MITHRIL(new int[] { 2102, 2103, 14853, 14854, 14855, 21278, 21279, 21280, 107459, 107492, 111372 }, new short[] { 447 }, 55, 80, 5, 148320),
	ADAMANT(new int[] { 2105, 14862, 14863, 14864, 21275, 21276, 21277, 107460, 107493, 111374 }, new short[] { 449 }, 70, 95, 7, 59328),
	RUNE(new int[] { 2107, 14859, 14860, 14861, 7494, 7461, 107494, 107461 }, new short[] { 451 }, 85, 125, 40, 42377),
	LIMESTONE(new int[] { 4027 }, new short[] { 3211 }, 40, 75, 5, 741600),
	GEM(new int[] { 2111 }, new short[] { 1623, 1621, 1619, 1617, 1631 }, 40, 75, 5, 211886);

	public static final Map<Integer, Rocks> values = new HashMap<>();
	private final int[] ids;
	private final short[] produce;
	private final int level, xp, mineTime, geodeRate;

	private Rocks(int[] ids, short[] produce, int level, int xp, int mineTime, int geodeRate) {
		this.ids = ids;
		this.produce = produce;
		this.level = level;
		this.xp = xp;
		this.mineTime = mineTime;
		this.geodeRate = geodeRate;
	}

	public int[] getIds() {
		return ids;
	}

	public short[] getProduce() {
		return produce;
	}

	public int getLevel() {
		return level;
	}

	public int getXp() {
		return xp;
	}

	public int getMineTime() {
		return mineTime;
	}

	public int getGeodeRate() {
		return geodeRate;
	}

	static {
		for (Rocks data : values()) {
			for (int id : data.getIds()) {
				values.put(id, data);
			}
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
