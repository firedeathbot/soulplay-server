package com.soulplay.content.player.skills.mining;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

public enum Pickaxes {

	BRONZE(1265, 1, 6747, 9),
	IRON(1267, 1, 6748, 8),
	STEEL(1269, 6, 6749, 7),
	MITHRIL(1273, 21, 6751, 6),
	ADAMANT(1271, 31, 6750, 5),
	RUNE(1275, 41, 6746, 4),
	DRAGON(15259, 61, 12188, 3),
	INFERNO(13661, 41, 10222, 4),
	GILDED(20786, 61, 272, 2),
	DRAGON_OR(30054, 61, Animation.getOsrsAnimId(7139), 3);

	public static final Map<Integer, Pickaxes> values = new HashMap<>();
	private final int itemId, level, time;
	private final Animation animation;

	private Pickaxes(int itemId, int level, int animation, int time) {
		this.itemId = itemId;
		this.level = level;
		this.animation = new Animation(animation);
		this.time = time;
	}

	public int getItemId() {
		return itemId;
	}

	public int getLevel() {
		return level;
	}

	public Animation getAnimation() {
		return animation;
	}

	public int getTime() {
		return time;
	}

	static {
		for (Pickaxes data : values()) {
			values.put(data.getItemId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
