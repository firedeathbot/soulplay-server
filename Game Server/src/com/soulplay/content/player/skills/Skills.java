package com.soulplay.content.player.skills;

import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.game.model.item.definition.MorphingRing;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public final class Skills {

	public static final int MAX_TOTAL_LEVEL = 2496;
	public static final int MAX_XP = 200_000_000;
	public static final int STAT_COUNT = 25;

	public static final int ATTACK = 0;
	public static final int DEFENSE = 1;
	public static final int STRENGTH = 2;
	public static final int HITPOINTS = 3;
	public static final int RANGED = 4;
	public static final int PRAYER = 5;
	public static final int MAGIC = 6;
	public static final int COOKING = 7;
	public static final int WOODCUTTING = 8;
	public static final int FLETCHING = 9;
	public static final int FISHING = 10;
	public static final int FIREMAKING = 11;
	public static final int CRAFTING = 12;
	public static final int SMITHING = 13;
	public static final int MINING = 14;
	public static final int HERBLORE = 15;
	public static final int AGILITY = 16;
	public static final int THIEVING = 17;
	public static final int SLAYER = 18;
	public static final int FARMING = 19;
    public static final int RUNECRAFTING = 20;
	public static final int DUNGEONEERING = 21;
	public static final int HUNTER = 22;
	public static final int SUMMONING = 23;
	public static final int CONSTRUCTION = 24;
	public static final int[] maxLevels = {99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 120, 99, 99, 99};
	public static final int[] minLevels = {1, 1, 1, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	public static final String[] SKILL_NAME = { "Attack", "Defence", "Strength", "Hitpoints", "Ranged", "Prayer",
			"Magic", "Cooking", "Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining",
			"Herblore", "Agility", "Thieving", "Slayer", "Farming", "Runecrafting", "Dungeoneering", "Hunter",
			"Summoning", "Construction" };

	public static int getXPForLevel(int level) {
		return Skills.expArray[level >= Skills.expArray.length ? Skills.expArray.length - 1 : level - 1];
	}

	public static int getLevelForXP(int exp) {
		return getLevelForXP(exp, 99);
	}

	public static int getLevelForXP(int exp, int maxLevel) {
		for (int j = maxLevel - 1; j != -1; j--) {
			if (Skills.expArray[j] <= exp) {
				return j + 1;
			}
		}
		return 0;
	}

	public static final int expArray[] = {0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431, 14391160, 15889109, 17542976, 19368992, 21385073, 23611006, 26068632, 28782069, 31777943, 35085654, 38737661, 42769801, 47221641, 52136869, 57563718, 63555443, 70170840, 77474828, 85539082, 94442737, 104273167, 115126838};

	private final Mob entity;
    
	public Skills(Mob entity) {
		this.entity = entity;
		this.experience = new int[STAT_COUNT];
		this.staticLevels = new int[STAT_COUNT];
		this.dynamicLevels = new int[STAT_COUNT];
		this.restoration = new SkillRestoration[STAT_COUNT];
		for (int i = 0; i < STAT_COUNT; i++) {
			this.staticLevels[i] = 1;
			this.dynamicLevels[i] = 1;
			this.restoration[i] = new SkillRestoration(i);
		}

		this.experience[HITPOINTS] = 1154;
		this.dynamicLevels[HITPOINTS] = 10;
		this.staticLevels[HITPOINTS] = 10;
		this.restoration[PRAYER] = null;
		this.restoration[SUMMONING] = null;
    }

	public Mob getEntity() {
		return entity;
	}
	
	private final int[] experience;

	private final int[] dynamicLevels;
	
	private final int[] staticLevels;
	
	private final SkillRestoration[] restoration;
	
    public int[] getDynamicLevels() {
		return dynamicLevels;
	}
    
    public int getLifepoints() {
		return dynamicLevels[HITPOINTS];
	}

    public int getMaximumLifepoints() {
    	if (entity.isPlayer()) {
    		return entity.toPlayerFast().calculateMaxLifePoints();
    	}

		return staticLevels[HITPOINTS];
	}

    public void setMaximumLifepoints(int lifepoints) {
		setStaticLevel(Skills.HITPOINTS, lifepoints);
	}

    public int getLevel(int slot) {
		return dynamicLevels[slot];
	}
    
    public void setLevel(int slot, int level) {
		dynamicLevels[slot] = level;
		if (restoration[slot] != null) {
			restoration[slot].restart();
		}
		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(slot);
		}
	}
    
    public int getStaticLevel(int slot) {
		return staticLevels[slot];
	}
    
    public int getExperience(int slot) {
		return experience[slot];
	}
    
    public int[] getExperienceArray() {
    	return experience;
    }
    
    public void setExperienceAndLevels(int slot, int xp) {
		this.experience[slot] = xp;
		int level = getLevelForXP(xp, maxLevels[slot]);
		this.dynamicLevels[slot] = level;
		this.staticLevels[slot] = level;
	}
    
    @Deprecated
    public void addExperience(int slot, int amount) {
    	amount = Math.max(0, amount);

    	long newExperience = experience[slot] + amount;
    	if (newExperience > MAX_XP) {
    		newExperience = MAX_XP;
    	}

    	this.experience[slot] = (int) newExperience;

    	int newLevel = Skills.getLevelForXP(this.experience[slot], maxLevels[slot]);
    	if (newLevel != this.staticLevels[slot]) {
    		this.staticLevels[slot] = newLevel;
    	}
    }
    
	public int getMaxSkillLevel() {
		int level = 0;
		for (int skill = 0; skill < STAT_COUNT; skill++) {
			if (skill >= Skills.ATTACK && skill <= Skills.MAGIC || skill == Skills.DUNGEONEERING || skill == Skills.SUMMONING) {
				continue;
			}

			int lvl = staticLevels[skill];
			if (lvl > level) {
				level = lvl;
			}
		}

		return level;
	}
	
	public int getTotalLevel() {
		int total = 0;
		for (int skill = 0; skill < STAT_COUNT; skill++) {
			total += staticLevels[skill];
		}
		return total;
	}
    
    /**
	 * Updates the current skill level (by incrementing the current amount with
	 * the given amount, up to the given maximum).
	 * @param skill The skill id.
	 * @param amount The amount to increment.
	 * @param maximum The maximum amount the skill can be.
	 * @return The amount of "overflow".
	 */
	public int updateLevel(int skill, int amount, int maximum) {
		if (amount > 0 && dynamicLevels[skill] > maximum) {
			return -amount;
		}
		int left = (dynamicLevels[skill] + amount) - maximum;
		int level = dynamicLevels[skill] += amount;
		if (level < 0) {
			dynamicLevels[skill] = 0;
		} else if (amount < 0 && level < maximum) {
			dynamicLevels[skill] = maximum;
		} else if (amount > 0 && level > maximum) {
			dynamicLevels[skill] = maximum;
		}
		if (restoration[skill] != null) {
			restoration[skill].restart();
		}
		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(skill);
		}
		return left;
	}

	/**
	 * Updates a level.
	 * @param skill the skill.
	 * @param amount the amount.
	 * @return the left.
	 */
	public int updateLevel(int skill, int amount) {
		return updateLevel(skill, amount, amount >= 0 ? getStaticLevel(skill) + amount : getStaticLevel(skill) - amount);
	}
	
	/**
	 * Sets the static level.
	 * @param skill The skill id.
	 * @param level The level to set.
	 */
	public void setStaticLevel(int skill, int level) {
		staticLevels[skill] = dynamicLevels[skill] = level;
		if (entity != null && entity.isPlayer()) {
			experience[skill] = getXPForLevel(level);
			entity.toPlayer().refreshSkill(skill);
		}
	}
	
	/**
	 * Sets the static level.
	 * @param skill The skill id.
	 * @param level The level to set.
	 */
	public void setStaticLevelClean(int skill, int level) {
		staticLevels[skill] = level;
		if (entity.isPlayer()) {
			experience[skill] = getXPForLevel(level);
			entity.toPlayer().refreshSkill(skill);
		}
	}

	/**
	 * Drains a certain percentage of a level.
	 * @param skill The skill.
	 * @param drainPercentage The drain percentage (0.05 indicates 5% drain).
	 * @param maximumDrainPercentage The maximum drain percentage (0.05
	 * indicates 5%).
	 */
	public void drainLevel(int skill, double drainPercentage, double maximumDrainPercentage) {
		int drain = (int) (dynamicLevels[skill] * drainPercentage);
		int minimum = (int) (staticLevels[skill] * (1.0 - maximumDrainPercentage));
		updateLevel(skill, -drain, minimum);
	}

	public void drainLevel(int skill, int drain, int minimumLevel) {
		updateLevel(skill, -drain, minimumLevel);
	}

	public void decrementPrayerPoints(int amount) {
		dynamicLevels[PRAYER] -= amount;
		if (dynamicLevels[PRAYER] < 0) {
			dynamicLevels[PRAYER] = 0;
		}
		// if (prayerPoints > staticLevels[PRAYER]) {
		// prayerPoints = staticLevels[PRAYER];
		// }
		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(PRAYER);
		}
		
		if (dynamicLevels[PRAYER] == 0 && entity.isPlayer())
			CombatPrayer.resetPrayers(entity.toPlayerFast());
	}

	/**
	 * Updates the current amount of prayer points (by incrementing)
	 * @param amount The amount to decrement with.
	 */
	public void incrementPrayerPoints(int amount) {
		dynamicLevels[PRAYER] += amount;
		if (dynamicLevels[PRAYER] < 0) {
			dynamicLevels[PRAYER] = 0;
		}
		if (dynamicLevels[PRAYER] > staticLevels[PRAYER]) {
			dynamicLevels[PRAYER] = staticLevels[PRAYER];
		}
		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(PRAYER);
		}
	}

	public void drainPrayer() {
		dynamicLevels[PRAYER] = 0;

		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(PRAYER);
		}
	}

	public void setPrayerPointsToMax() {
		dynamicLevels[PRAYER] = staticLevels[PRAYER];

		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(PRAYER);
		}
	}
	
	public int getPrayerPoints() {
		return dynamicLevels[PRAYER];
	}

	public boolean prayerPointsFull() {
		return dynamicLevels[Skills.PRAYER] >= staticLevels[Skills.PRAYER];
	}

	public void setLifepoints(int lifepoints) {
		this.dynamicLevels[HITPOINTS] = lifepoints;
		if (this.dynamicLevels[HITPOINTS] < 0) {
			this.dynamicLevels[HITPOINTS] = 0;
		}
		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(HITPOINTS);
		}
	}
	
	public void healToFull() {
		setLifepoints(getMaximumLifepoints());
	}

	public int heal(int health) {
		return heal(health, getMaximumLifepoints());
	}

	public int heal(int health, int maxHp) {
		dynamicLevels[HITPOINTS] += health;

		int left = 0;
		if (dynamicLevels[HITPOINTS] > maxHp) {
			left = dynamicLevels[HITPOINTS] - maxHp;
			dynamicLevels[HITPOINTS] = maxHp;
		}

		if (entity.isPlayer()) {
			entity.toPlayer().refreshSkill(HITPOINTS);
		}
		return left;
	}
	
	public int hit(int damage) {
		if (dynamicLevels[HITPOINTS] - damage < 0) {
			damage = dynamicLevels[HITPOINTS];
		}

		dynamicLevels[HITPOINTS] -= damage;
		if (entity.isPlayer()) {
			Player player = entity.toPlayerFast();
			player.refreshSkill(HITPOINTS);

			MorphingRing morphingRing = MorphingRing.getRing(player);
			if (morphingRing != null) {
				morphingRing.unmorph(player);
			}
		}

		return damage;
	}

	public void processRestore() {
		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			SkillRestoration entry = restoration[i];
			if (entry == null) {
				continue;
			}

			entry.restore(entity);
		}
	}

	public SkillRestoration[] getRestoration() {
		return restoration;
	}

	public void restore() {
		restore(true);
	}
	
	public void restore(boolean hardReset) {
		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			int staticLevel;
			if (i == 3 && entity.isPlayer()) {
				staticLevel = entity.toPlayerFast().calculateMaxLifePoints();
			} else {
				staticLevel = getStaticLevel(i);
			}

			if (hardReset || (!hardReset && getLevel(i) < staticLevel))
				setLevel(i, staticLevel);
		}
	}

	public void restoreLevel(int skillId) {
		int staticLevel = getStaticLevel(skillId);
		setLevel(skillId, staticLevel);
	}

	public int getMasteredSkills() {
		int count = 0;
		for (int i = 0; i < 23; i++) {
			if (getStaticLevel(i) >= maxLevels[i]) {
				count++;
			}
		}
		return count;
	}

	public int getSkillBoostable(int skill) {
		switch (skill) {
			case PRAYER:
			case SUMMONING:
				return getStaticLevel(skill);
			default:
				return getLevel(skill);
		}
	}
	
	public void copySkills(Skills copyFrom) {
		for (int i = 0, length = staticLevels.length; i < length; i++) {
			setStaticLevel(i, copyFrom.getStaticLevel(i));
		}
	}

	public static int randomBaseChance() {
		return Misc.random(254);
	}
	
	public static boolean successRoll(int low, int high, int currentRate) {
		int chance = Misc.interpolate(low, high, 1, 99, currentRate);
//		System.out.println("rate:"+ chance + "  chance:"+ (chance/255.d));
		return randomBaseChance() < chance;
	}
	
}
