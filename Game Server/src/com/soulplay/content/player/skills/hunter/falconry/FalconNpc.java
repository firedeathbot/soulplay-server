package com.soulplay.content.player.skills.hunter.falconry;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = {5094})
public class FalconNpc extends AbstractNpc {

	public int ownerId;
	public FalconCatch falconCatch;
	
	public FalconNpc(int slot) {
		super(slot, 5094);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new FalconNpc(slot);
	}
	
	private static final String MSG = "Your falcon has left its prey. You see it heading back toward the falconer.";

	@Override
	public void process() {
		super.process();

		if (!isDead() && getAliveTick() == 1) {
			Player p = PlayerHandler.getPlayerByMID(ownerId);
			
			NPC matt = NPCHandler.findNPC(5092);
			
			if (p != null && !p.disconnected) {
				p.sendMessage(MSG);
				p.getPacketSender().resetArrow();
			}
			
			if (matt != null) {
				Projectile proj = new Projectile(922, getCurrentLocation(), matt.getCurrentLocation());
				proj.setLockon(matt.getProjectileLockon());
				proj.setStartHeight(1);
				proj.setEndHeight(26);
				proj.setStartDelay(0);
				proj.setEndDelay((int)(getCurrentLocation().getDistance(matt.getCurrentLocation())*8));
				GlobalPackets.createProjectile(proj);
			}
			nullNPC();
		}
	}
	
	@Override
	public void onSpawn() {
		super.onSpawn();
		setAliveTick(100);
		faceLocation(getX()+Misc.random(1), getY()+Misc.random(1));
	}
	
	public boolean isOwner(Player other) {
		return other.mySQLIndex == ownerId;
	}
}
