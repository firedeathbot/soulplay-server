package com.soulplay.content.player.skills.hunter.falconry;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;

/**
 * Represents a falcon catch.
 * @author Vexia
 */
public enum FalconCatch {
	SPOTTED_KEBBIT(5098, 43, 104, new GameItem(10125, 1)), DARK_KEBBIT(5099, 57, 132, new GameItem(10115, 1)), DASHING_KEBBIT(5100, 69, 156, new GameItem(10127, 1));

	/**
	 * Represents the npc.
	 */
	private final int npc;

	/**
	 * Represents the level.
	 */
	private final int level;

	/**
	 * Represents the experience gained.
	 */
	private final int experience;

	/**
	 * Represents the item reward.
	 */
	private final GameItem item;

	/**
	 * Constructs a new {@code FalconCatch} {@code Object}.
	 * @param npc the npc.
	 * @param level the level.
	 * @param experience the experience.
	 * @param item the item.
	 */
	FalconCatch(int npc, int level, int experience, GameItem item) {
		this.npc = npc;
		this.level = level;
		this.experience = experience;
		this.item = item;
	}

	/**
	 * Gets the falcon catch.
	 * @param item the item.
	 * @return the falcon catch.
	 */
	public static FalconCatch forItem(final GameItem item) {
		for (FalconCatch falconCatch : FalconCatch.values) {
			if (item.getId() == falconCatch.getItem().getId()) {
				return falconCatch;
			}
		}
		return null;
	}

	/**
	 * Gets the falcon catch.
	 * @param npc the npc.
	 * @return the falcon catch.
	 */
	public static FalconCatch forNPC(final NPC npc) {
		for (FalconCatch falconCatch : FalconCatch.values) {
			if (npc.npcType == falconCatch.getNpc()) {
				return falconCatch;
			}
		}
		return null;
	}

	/**
	 * Gets the npc.
	 * @return The npc.
	 */
	public int getNpc() {
		return npc;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the experience.
	 * @return The experience.
	 */
	public int getExperience() {
		return experience;
	}

	/**
	 * Gets the item.
	 * @return The item.
	 */
	public GameItem getItem() {
		return item;
	}

	public static final FalconCatch[] values = values();
	
}
