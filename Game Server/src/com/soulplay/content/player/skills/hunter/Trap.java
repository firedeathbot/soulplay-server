package com.soulplay.content.player.skills.hunter;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;

/**
 * @author Rene
 */
public class Trap {

	/**
	 * The possible states a trap can be in
	 */
	public static enum TrapState {

		SET, CAUGHT;
	}

	/**
	 * The WorldObject linked to this HunterObject
	 */
	private GameObject gameObject;

	/**
	 * The amount of ticks this object should stay for
	 */
	private int ticks;

	/**
	 * This trap's state
	 */
	private TrapState trapState;

	private Player player;

	/**
	 * Reconstructs a new Trap
	 *
	 * @param object
	 * @param state
	 */
	public Trap(GameObject object, TrapState state, int ticks, Player owner) {
		gameObject = object;
		trapState = state;
		this.ticks = ticks;
		this.player = owner;
	}

	/**
	 * Gets the GameObject
	 */
	public GameObject getGameObject() {
		return gameObject;
	}

	public Player getOwner() {
		return player;
	}

	/**
	 * @return the ticks
	 */
	public int getTicks() {
		return ticks;
	}

	/**
	 * Gets a trap's state
	 */
	public TrapState getTrapState() {
		return trapState;
	}

	/**
	 * Sets the GameObject
	 *
	 * @param gameObject
	 */
	public void setGameObject(GameObject gameObject) {
		this.gameObject = gameObject;
	}

	public void setOwner(Player player) {
		this.player = player;
	}

	/**
	 * @param ticks
	 *            the ticks to set
	 */
	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

	/**
	 * Sets a trap's state
	 *
	 * @param state
	 */
	public void setTrapState(TrapState state) {
		trapState = state;
	}
}
