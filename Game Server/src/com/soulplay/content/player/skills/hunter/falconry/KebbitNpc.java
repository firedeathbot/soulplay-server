package com.soulplay.content.player.skills.hunter.falconry;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;

@NpcMeta(ids = {5098,5099, 5100})
public class KebbitNpc extends AbstractNpc {

	public KebbitNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new KebbitNpc(slot, npcType);
	}

	private boolean isBusy = false;

	public boolean isBusy() {
		return isBusy;
	}

	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}
	
}
