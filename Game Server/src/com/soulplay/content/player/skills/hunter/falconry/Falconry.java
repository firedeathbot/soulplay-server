package com.soulplay.content.player.skills.hunter.falconry;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.info.AnimationsData;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class Falconry {
	
	public static final int GLOVE_ONLY = 10023;
	public static final int GLOVE_FALCON = 10024;
	
	private static final String CATCH_MSG[] = { "The falcon successfully swoops down and captures the kebbit.", "The falcon swoops down on the kebbit, but just misses catching it."};

	public static void catchKebbit(Player p, KebbitNpc n) {
		FalconCatch kebbit = FalconCatch.forNPC(n);
		
		p.getMovement().hardResetWalkingQueue();
		p.getPA().resetFollow();
		p.faceLocation(n.getCurrentLocation());
		
		if (kebbit == null) {
			return;
		}
		
		if (!checkRequirements(p, n.getCurrentLocation(), kebbit)) {
			return;
		}
		
		int dist = (int) p.getCurrentLocation().getDistance(n.getCurrentLocation());
		
		final boolean kebbitInUse = n.isBusy();
		
		final boolean successfulCatch = kebbitInUse || success(p, kebbit);
		
		p.performingAction = true;
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				switchGlove(p, false);
			}
		}, 0);
		
		Projectile proj = new Projectile(922, p.getCurrentLocation(), n.getCurrentLocation());
		proj.setStartHeight(26);
		proj.setEndHeight(1);
		proj.setStartDelay(30);
		proj.setEndDelay(30 + dist * 7);
		GlobalPackets.createProjectile(proj);
		
		if (!kebbitInUse)
			n.setBusy(true);
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (successfulCatch) {
					if (n.isActive && !n.isDead()) {
						n.nullNPC();

						int index = Misc.findFreeNpcSlot();
						if (index != -1) {
							FalconNpc falcon = new FalconNpc(index);
							falcon.ownerId = p.mySQLIndex;
							falcon.falconCatch = kebbit;
							p.getPacketSender().createArrow(1, index);
							NPCHandler.spawnNpc(falcon, n.getX(), n.getY(), n.getZ(), 0);

							p.sendMessage(CATCH_MSG[0]);
						}

					}
				} else {
					p.sendMessage(CATCH_MSG[1]);
				}
			}
		}, (proj.getEndDelay()/30));
		
		if (successfulCatch) {
			
		} else {
			//display return gfx
			proj.setStartLoc(n.getCurrentLocation());
			proj.setEndLoc(p.getCurrentLocation());
			proj.setStartDelay(proj.getEndDelay());
			proj.setStartHeight(1);
			proj.setEndHeight(26);
			proj.setEndDelay(proj.getStartDelay() + dist * 10);
			GlobalPackets.createProjectile(proj);

		}
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				p.performingAction = false;
				p.getMovement().hardResetWalkingQueue();//cheaphax, todo find out why it's still walking sometimes.
				if (n.isActive && !n.isDead())
					n.setBusy(false);
				
				if (!successfulCatch)
					switchGlove(p, true);
			}
		}, (proj.getEndDelay()/30 + 1));
		
		
		p.lockMovement(proj.getEndDelay()/30 + 2);
		
	}
	
	public static boolean checkRequirements(Player player, Location kebLoc, FalconCatch falconCatch) {
		if (kebLoc.getDistance(player.getCurrentLocation()) > 15) {
			player.sendMessage("You can't catch a kebbit that far away.");
			return false;
		}
		if (player.getSkills().getLevel(Skills.HUNTER) < falconCatch.getLevel()) {
			player.sendMessage("You need a Hunter level of at least " + falconCatch.getLevel() + " to catch this kebbit.");
			return false;
		}
		if (player.playerEquipment[PlayerConstants.playerHands] > 0 || player.playerEquipment[PlayerConstants.playerShield] > 0) {
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement("Sorry, free your hands, weapon, and shield slot first.").execute();
			return false;
		}
		if (player.displayEquipment[PlayerConstants.playerWeapon] > 0 || player.playerEquipment[PlayerConstants.playerWeapon] != GLOVE_FALCON) {
			player.sendMessage("You need a falcon to catch a kebbit.");
			return false;
		}
		return true;
	}
	
	public static boolean success(Player player, FalconCatch falconCatch) {
//		if (originalLocation != node.getLocation()) {
//			return RandomFunction.random(1, 3) == 2;
//		}
		return ((Misc.random(3) * player.getSkills().getLevel(Skills.HUNTER)) / 3) > (falconCatch.getLevel() / 2);
	}
	
	private static final String RET_MSG = "You retrieve the falcon as well as the fur of the dead kebbit.";
	private static final GameItem BONES = new GameItem(526, 1);
	
	public static void pickupFalcon(Player player, FalconNpc npc) {
		if (!npc.isOwner(player)) {
			player.sendMessage("That is not your falcon.");
			return;
		}
		
		if (npc.isDead()) {
			return;
		}
		
		if (player.displayEquipment[PlayerConstants.playerWeapon] != GLOVE_ONLY) {
			player.sendMessage("You need a falcon glove to pickup the falcon.");
			return;
		}
		
		if (player.getItems().freeSlots() == 0) {
			player.sendMessage("You don't have enough inventory space.");
			return;
		}
		
		final FalconCatch falCatch = npc.falconCatch;
		
		player.resetFace();
		
		npc.nullNPC();
		
		player.getPacketSender().resetArrow();
		player.getPA().addSkillXP(falCatch.getExperience(), Skills.HUNTER);
		player.sendMessage(RET_MSG);
		player.getItems().addOrDrop(falCatch.getItem());
		player.getItems().addOrDrop(BONES);
		player.startAnimation(AnimationsData.PICK_UP_FROM_GROUND);
		switchGlove(player, true);
	}
	
	private static void switchGlove(Player p, boolean drawFalcon) {
		p.displayEquipment[PlayerConstants.playerWeapon] = drawFalcon ? -1 : GLOVE_ONLY;
		p.getItems().refreshWeapon();
	}
	
	private static final String LVL_REQ_MSG = "You need hunter level 43 to use the falcon.";
	private static final String ALREADY_HAVE_MSG[] = {"You already have a falcon glove.", "Check your inventory or bank."};
	private static final String NO_ROOM_MSG = "Your inventory is full.";
	private static final String GIVE_GLOVE_MSG = "Here is a glove for you. Enjoy!";
	
	public static void getGloveFromMatt(Player p) {
		
		if (p.getSkills().getLevel(Skills.HUNTER) < 43) {
			p.getDialogueBuilder().reset();
			p.getDialogueBuilder().sendNpcChat(5092, LVL_REQ_MSG).execute();
			return;
		}
		
		//First we check if he is already equipped a glove, to reset the visual
		if (p.displayEquipment[PlayerConstants.playerWeapon] == GLOVE_ONLY) {
			switchGlove(p, true);
			return;
		}
		
		//check inventory and bank for glove
		boolean hasEquipped = p.getItems().playerHasEquipped(GLOVE_FALCON, PlayerConstants.playerWeapon);
		boolean hasInInv = hasEquipped ? true : p.getItems().playerHasItem(GLOVE_FALCON);
		boolean bankHasItem = hasInInv ? true : p.getItems().bankHasItemAmount(GLOVE_FALCON, 1);
		if (bankHasItem || hasInInv || hasEquipped) {
			p.getDialogueBuilder().reset();
			p.getDialogueBuilder().sendNpcChat(5092, ALREADY_HAVE_MSG).execute();
			return;
		}
		
		if (p.getItems().freeSlots() == 0) {
			p.getDialogueBuilder().reset();
			p.getDialogueBuilder().sendNpcChat(5092, NO_ROOM_MSG).execute();
			return;
		}
		
		p.getItems().addItem(10024, 1);

		p.getDialogueBuilder().reset();
		p.getDialogueBuilder().sendNpcChat(5092, GIVE_GLOVE_MSG).execute();
	}
	
	public static void openFurTradingDialogue(Player p, int npcType) {
		p.getDialogueBuilder().reset(false);
		p.getDialogueBuilder()
		.sendPlayerChat("placeholder...")//gotta use placeholder cuz of the way dialogue option shit works...
		.sendNpcChat(npcType, "I will accept 2 pieces of fur and 1M gold")
		.sendOption("Select the fur",
		"Spotted kebbit fur", ()-> {
			tradeFur(p, 10125, 10069, npcType);
		}, "Dashing kebbit fur", ()-> {
			tradeFur(p, 10127, 10071, npcType);
		}, "Dark kebbit fur", ()-> {
			tradeFur(p, 10115, 10075, npcType);
		})
		.sendNpcChat(npcType, "You don't have enough fur to trade.")
		.sendAction(()-> {
			p.getDialogueBuilder().closeNew();
		})
		.sendNpcChat(npcType, "You don't have enough gold with you.");
		
		p.getDialogueBuilder().execute(true);
	}
	
	private static void tradeFur(Player p, int furId, int rewardId, int npcType) {
		boolean hasFur = p.getItems().playerHasItem(furId, 2);
		if (hasFur && p.getItems().takeCoins(1_000_000)) {
			p.getItems().deleteItem2(furId, 2);
			p.getItems().addItem(rewardId, 1);
			p.getDialogueBuilder().closeNew();
		} else {
			if (!hasFur) {
				p.sendMessage("You don't have enough fur to trade.");
				p.getDialogueBuilder().jumpToStage(3);
			} else { // not enough gold i guess?
				p.getDialogueBuilder().jumpToStage(5);
				p.sendMessage("You don't have enough gold with you.");
			}
		}
	}
}
