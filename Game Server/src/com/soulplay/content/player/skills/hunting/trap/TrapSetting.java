package com.soulplay.content.player.skills.hunting.trap;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class TrapSetting {

	/**
	 * The node ids to create the trap.
	 */
	private final int[] nodeIds;

	/**
	 * The items required.
	 */
	private final Item[] items;

	/**
	 * The create option.
	 */
	private final String option;

	/**
	 * The level required to set the trap.
	 */
	private final int level;

	/**
	 * The setup animation of the trap.
	 */
	private final Animation setupAnimation;

	/**
	 * The animation
	 */
	private final Animation dismantleAnimation;

	/**
	 * The fail object id.
	 */
	private final int failId;

	/**
	 * The corresponding object ids.
	 */
	private final int[] objectIds;

	/**
	 * The bait ids.
	 */
	private final int[] baitIds;

	/**
	 * If it's an object trap.
	 */
	private final boolean objectTrap;

	/**
	 * Constructs a new {@code TrapSettingTemp} {@code Object}.
	 * @param nodeIds the node ids.
	 * @param items the items.
	 * @param level the level.
	 * @param option the option.
	 * @param animation the animation.
	 * @param objectTrap if an object trap.
	 */
	public TrapSetting(int[] nodeIds, Item[] items, int[] objectIds, final int[] baitIds, final String option, int level, final int failId, final Animation setupAnimation, final Animation dismantleAnimation, boolean objectTrap) {
		this.nodeIds = nodeIds;
		this.items = items;
		this.objectIds = objectIds;
		this.baitIds = baitIds;
		this.level = level;
		this.option = option;
		this.objectTrap = objectTrap;
		this.failId = failId;
		this.setupAnimation = setupAnimation;
		this.dismantleAnimation = dismantleAnimation;
	}

	/***
	 * Constructs a new {@code TrapSettingTemp} {@code Object}.
	 * @param nodeId the node id.
	 * @param items the items.
	 * @param option the option.
	 * @param level the level.
	 * @param objectTrap if an object trap.
	 */
	public TrapSetting(int nodeId, Item[] items, int[] objectIds, final int[] baitIds, final String option, int level, final int failId, final Animation setupAnimation, final Animation dismantleAnimation, boolean objectTrap) {
		this(new int[] { nodeId }, items, objectIds, baitIds, option, level, failId, setupAnimation, dismantleAnimation, objectTrap);
	}

	/**
	 * Constructs a new {@code TrapSettingTemp} {@code Object}.
	 * @param nodeId the node id.
	 * @param option the option.
	 * @param objectids the ids.
	 * @param level the level.
	 */
	public TrapSetting(int nodeId, int[] objectIds, final int[] baitIds, String option, final int failId, final Animation setupAnimation, final Animation dismantleAnimation, int level) {
		this(new int[] { nodeId }, objectIds, baitIds, option, level, failId, setupAnimation, dismantleAnimation, false);
	}

	/**
	 * Constructs a new {@code TrapSettingTemp} {@code Object}.
	 * @param nodeIds the node ids.
	 * @param option the option.
	 * @param level the level.
	 * @param objectTrap the object trap.
	 */
	public TrapSetting(int[] nodeIds, int[] objectIds, final int[] baitIds, String option, int level, final int failId, final Animation setupAnimation, final Animation dismantleAnimation, boolean objectTrap) {
		this(nodeIds, new Item[] { new Item(nodeIds[0]) }, objectIds, baitIds, option, level, failId, setupAnimation, dismantleAnimation, objectTrap);
	}

	/**
	 * Clears the trap.
	 * @param wrapper the wrapper.
	 * @param type the clear type (0=groundItems, 1=inventory)
	 * @return {@code True} if cleared.
	 */
	public boolean clear(TrapWrapper wrapper, int type) {
		RSObject object = wrapper.getObject();
		returnItems(object, wrapper, type);
		removeObject(wrapper);
		return true;
	}

	/**
	 * Returns the items to the ground or player.
	 * @param object the object.
	 * @param wrapper the wrapper.
	 * @param type the type.
	 */
	public void returnItems(RSObject object, TrapWrapper wrapper, int type) {
		boolean ground = type == 0;
		Player player = wrapper.getPlayer();
		if (!isObjectTrap()) {
			if (ground) {
				ItemHandler.createGroundItem((Client)player, items[0].getId(), object.getX(), object.getY(), object.getHeight(), items[0].getAmount());
				return;
			}
			addTool(player, wrapper, type);
			player.sendMessage("What???188118"); //player.getInventory().add(wrapper.getItems().toArray(new Item[] {}));
		} else {
			if (isObjectTrap() && !ground) {
				player.sendMessage("What283848"); //player.getInventory().add(wrapper.getItems().toArray(new Item[] {}));
				return;
			}
		}
	}

	/**
	 * Adds the tool back to the inventory.
	 * @param player the player.
	 * @param wrapper the wrapper.
	 * @param type the type.
	 */
	public void addTool(Player player, TrapWrapper wrapper, int type) {
		player.getItems().addItem(items[0].getId(), items[0].getAmount());
	}

	/**
	 * Checks if the player has the required items.
	 * @param player the player.
	 * @return {@code True} if so.
	 */
	public boolean hasItems(Player player) {
		for (Item item : items) {
			if (!player.getItems().playerHasItem(item.getId())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Creates a ground item.
	 * @param item the item.
	 * @param location the location.
	 * @param player the player.
	 */
	public void createGroundItem(Item item, Location location, Player player) {
		ItemHandler.createGroundItem((Client)player, item.getId(), location.getX(), location.getY(), location.getZ(), item.getAmount());
	}

	/**
	 * Removes the object from a wrapper.
	 * @param object the object.
	 */
	public boolean removeObject(TrapWrapper wrapper) {
		return removeObject(wrapper.getObject());
	}

	/**
	 * Removes the object from the region.
	 * @param object the object.
	 * @return {@code True} if so.
	 */
	public boolean removeObject(RSObject object) {
		if (object.getTick() > 0) {
			object.setTick(0);
			return true;
		}
		return false;
	}

	/**
	 * Builds a game object.
	 * @param node the node.
	 * @return the object.
	 */
	public RSObject buildObject(Player player) {
		final RSObject rso = new RSObject(objectIds[0], player.getX(), player.getY(), player.getZ(), 0, 10, -1, 100+Misc.random(100));
		ObjectManager.addObject(rso);
		return rso;
	}

	/**
	 * Handles the catch on a pulse.
	 * @param counter the counter.
	 * @param wrapper the wrapper.
	 * @param node the node.
	 * @param npc the npc.
	 * @param success the success.
	 */
	public void handleCatch(int counter, TrapWrapper wrapper, BoxTrap node, NPC npc, boolean success) {
	}

	/**
	 * Checks if the catch was successfull.
	 * @param player the player.
	 * @param node the node.
	 * @return {@code True} if so.
	 */
	public boolean isSuccess(Player player, final BoxTrap node) {
		double level = player.getSkills().getLevel(Skills.HUNTER);
		double req = node.getLevel();
		double successChance = Math.ceil((level * 50 - req * 17) / req / 3 * 4);
		int roll = Misc.random(99);
		if (successChance >= roll) {
			return true;
		}
		return false;
	}

	/**
	 * Calles when an object is setup.
	 * @param player the player.
	 * @param node the node.
	 * @param wrapper the wrapper.
	 */
	public void reward(Player player, RSObject node, TrapWrapper wrapper) {

	}

	/**
	 * A global method to check if a catch can be made.
	 * @param wrapper the wrapper.
	 * @param npc the npc.
	 * @return {@code True} if so.
	 */
	public boolean canCatch(TrapWrapper wrapper, NPC npc) {
		return true;
	}

	/**
	 * Checks if the bait corresponds.
	 * @param bait the bait.
	 * @return {@code True} if so.
	 */
	public boolean hasBait(Item bait) {
		for (int id : getBaitIds()) {
			if (id == bait.getId()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the node for the object.
	 * @param object the object.
	 * @return the node id.
	 */
	public int getNodeForObjectId(int objectId) {
		return getNodeForObject(getObjectIndex(objectId));
	}

	/**
	 * Gets the node for the object.
	 * @param index the index.
	 * @return the id.
	 */
	public int getNodeForObject(int index) {
		return nodeIds[index];
	}

	/**
	 * Gets the object id for the node.
	 * @param index the index.
	 * @return the id.
	 */
	public int getObjectForNode(int index) {
		return objectIds[index];
	}

	/**
	 * Gets the object index.
	 * @param object the object.
	 * @return the index.
	 */
	public int getObjectIndex(int objectId) {
		for (int i = 0; i < objectIds.length; i++) {
			if (objectId == objectIds[i]) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Gets the transform id.
	 * @param node the node.
	 * @param object the object.
	 * @return the id.
	 */
	public int getTransformId(TrapWrapper wrapper, BoxTrap node) {
		return node.getTransformId();
	}

	/**
	 * Gets the final id.
	 * @param wrapper the wrapper.
	 * @param node the node.
	 * @return the id.
	 */
	public int getFinalId(TrapWrapper wrapper, BoxTrap node) {
		return node.getFinalId();
	}

	/**
	 * Gets the failId.
	 * @param wrapper the wrapper.
	 * @param node the node.
	 * @return The failId.
	 */
	public int getFailId(TrapWrapper wrapper, BoxTrap node) {
		return failId;
	}

	/**
	 * Gets the limit message.
	 * @param player the player.
	 * @return the message.
	 */
	public String getLimitMessage(Player player) {
		return "You don't have a high enough Hunter level to set up more than " + 1/*player.getHunterManager().getMaximumTraps()*/ + " trap" + (1/*player.getHunterManager().getMaximumTraps() == 1 ? "." : "s."*/);
	}

	/**
	 * Gets the name of the trap.
	 * @return the name.
	 */
	public String getName() {
		if (isObjectTrap()) {
			return ObjectDef.getObjectDef(nodeIds[0]).getName().toLowerCase();
		}
		return ItemDefinition.getName(nodeIds[0]).toLowerCase();
	}

	/**
	 * Gets the time up message.
	 * @return the message.
	 */
	public String getTimeUpMessage() {
		return "The " + getName() + " " + (isObjectTrap() ? "trap that you constructed has collapsed." : "that you laid has fallen over.");
	}

	/**
	 * Checks if adding this trap will exceed the limit.
	 * @param player the player.
	 * @return {@code True} if so.
	 */
	public boolean exceedsLimit(Player player) {
		return false;
	}

	/**
	 * Gets the objectTrap.
	 * @return The objectTrap.
	 */
	public boolean isObjectTrap() {
		return objectTrap;
	}

	/**
	 * Gets the nodeIds.
	 * @return The nodeIds.
	 */
	public int[] getNodeIds() {
		return nodeIds;
	}

	/**
	 * Gets the items.
	 * @return The items.
	 */
	public Item[] getItems() {
		return items;
	}

	/**
	 * Gets the option.
	 * @return The option.
	 */
	public String getOption() {
		return option;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the setupAnimation.
	 * @return The setupAnimation.
	 */
	public Animation getSetupAnimation() {
		return setupAnimation;
	}

	/**
	 * Gets the objectIds.
	 * @return The objectIds.
	 */
	public int[] getObjectIds() {
		return objectIds;
	}

	/**
	 * Gets the dismantleAnimation.
	 * @return The dismantleAnimation.
	 */
	public Animation getDismantleAnimation() {
		return dismantleAnimation;
	}

	/**
	 * The fail id.
	 * @return the id.
	 */
	public int getFailId() {
		return failId;
	}

	/**
	 * Gets the baitIds.
	 * @return The baitIds.
	 */
	public int[] getBaitIds() {
		return baitIds;
	}

}