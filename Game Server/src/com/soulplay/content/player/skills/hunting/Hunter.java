package com.soulplay.content.player.skills.hunting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

public class Hunter {

	public static enum HunterData {
		BABY("Baby Impling", 11238, 7, 1, 6055, 1), // xp 15
		YOUNG("Young Impling", 11240, 9, 22, 6056, 1), // xp 20
		GOURMET("Gourmet Impling", 11242, 12, 28, 6057, 1), // xp 22
		EARTH("Earth Impling", 11244, 16, 36, 6058, 1), // xp 25
		ESSENCE("Essence Impling", 11246, 20, 42, 6059, 1), // xp 27
		ELECTIC("Electic Impling", 11248, 27, 50, 6060, 1), // xp 32
		NATURE("Nature Impling", 11250, 42, 58, 6061, 1), // xp 34
		MAGPIE("Magpie Impling", 11252, 56, 65, 6062, 1), // xp 44
		NINJA("Ninja Impling", 11254, 74, 74, 6063, 1), // xp 57
		DRAGON("Dragon Impling", 11256, 83, 83, 6064, 1), // xp 65
		KINGLY("Kingly Impling", 15517, 98, 91, 7903, 1), // xp 71

		RUBY("Ruby harvest", 10020, 20, 15, 5085, 2),
		SAPPHIRE("Sapphire glacialis", 10018, 23, 25, 5084, 2),
		SNOWY("Snowy knight", 10016, 26, 35, 5083, 2),
		BLACK("Black warlock", 10014, 30, 45, 5082, 2);
		
		public static final HunterData[] values = HunterData.values();

		private int jar, xp, level, npcId, type;

		private String name;

		HunterData(String name, int jar, int xp, int level, int npcId,
				int type) {
			this.name = name;
			this.jar = jar;
			this.xp = xp;
			this.level = level;
			this.npcId = npcId;
			this.type = type;
		}

		public int getJar() {
			return jar;
		}

		public int getLevel() {
			return level;
		}

		public String getName() {
			return name;
		}

		public int getNpcId() {
			return npcId;
		}

		public int getType() {
			return type;
		}

		public int getXP() {
			return xp;
		}

	}

	private static final Animation HUNTER_ANIM = Animation.create(393);

	private static final int BUTTERFLY_NET = 10010;

	private static final int IMPLING_JAR = 11260;

	private static final int BUTTERFLY_JAR = 10012;

	private static void catchButterfly(Player c, NPC npc, HunterData h) {
		if (c.getStopWatch().checkThenStart(9)) {
			c.startAnimation(HUNTER_ANIM);
			if (Misc.random(2) == 1 && !hasKyatt(c)
					|| Misc.random(2) == 1 && !hasLarupia(c)) {
				c.sendMessageSpam(
						"You failed to catch the butterfly.");
			} else if (Misc.random(10) == 1 && hasKyatt(c)) {
				c.sendMessageSpam(
						"You failed to catch the butterfly.");
			} else if (Misc.random(12) == 1 && hasLarupia(c)) {
				c.sendMessageSpam(
						"You failed to catch the butterfly.");
			} else {
				if (npc != null && !npc.isDead()
						&& npc.goodDistance(c.getX(), c.getY(), 1)) {
					npc.setDead(true);
				}
				if (c.inWild()) {
					c.getPA().addSkillXP(h.getXP() * 2,
							Skills.HUNTER);
				} else {
					c.getPA().addSkillXP(h.getXP(),
							Skills.HUNTER);
				}
				c.sendMessageSpam(
						"You catch a " + h.getName() + ".");
				c.getItems().deleteItemInOneSlot(BUTTERFLY_JAR, 1);
				c.getItems().addItem(h.getJar(), 1);
			}
		}
	}

	private static void catchImp(Player c, NPC npc, HunterData h) {
		if (c.getStopWatch().checkThenStart(9)) {
			c.startAnimation(HUNTER_ANIM);
			if (Misc.random(2) == 1 && !hasKyatt(c)
					|| Misc.random(2) == 1 && !hasLarupia(c)) {
				c.sendMessageSpam(
						"You failed to catch the impling.");
			} else if (Misc.random(10) == 1 && hasKyatt(c)) {
				c.sendMessageSpam(
						"You failed to catch the impling.");
			} else if (Misc.random(12) == 1 && hasLarupia(c)) {
				c.sendMessageSpam(
						"You failed to catch the impling.");
			} else {
				if (npc != null && !npc.isDead()
						&& npc.goodDistance(c.getX(), c.getY(), 1)) {
					npc.setDead(true);
				}
				if (c.inWild()) {
					c.getPA().addSkillXP(h.getXP() * 2,
							Skills.HUNTER);
				} else {
					c.getPA().addSkillXP(h.getXP(),
							Skills.HUNTER);
				}
				c.sendMessageSpam(
						"You catch a " + h.getName() + ".");
				c.getItems().deleteItemInOneSlot(IMPLING_JAR, 1);
				c.getItems().addItem(h.getJar(), 1);
			}
		}
	}

	public static void checkReq(Player c, NPC npc, HunterData h) {
		if (c.playerEquipment[PlayerConstants.playerWeapon] == BUTTERFLY_NET) {
			if (c.getPlayerLevel()[22] >= h.getLevel()) {
				if (c.getItems().playerHasItem(IMPLING_JAR)
						&& h.getType() == 1) {
					catchImp(c, npc, h);
					return;
				} else if (c.getItems().playerHasItem(BUTTERFLY_JAR)
						&& h.getType() == 2) {
					catchButterfly(c, npc, h);
					return;
				} else {
					c.sendMessage("You need a jar to catch this.");
					return;
				}
			} else {
				c.sendMessage("You need a hunter level of "
						+ h.getLevel() + " to catch this.");
				return;
			}
		} else {
			c.sendMessage(
					"You need to wear a butterfly net to catch this.");
			return;
		}
	}

	private static boolean hasKyatt(Player c) {
		if (c.playerEquipment[PlayerConstants.playerHat] == 10039
				&& c.playerEquipment[PlayerConstants.playerLegs] == 10037
				&& c.playerEquipment[PlayerConstants.playerChest] == 10035) {
			return true;
		}
		return false;
	}

	private static boolean hasLarupia(Player c) {
		if (c.playerEquipment[PlayerConstants.playerHat] == 10045
				&& c.playerEquipment[PlayerConstants.playerLegs] == 10041
				&& c.playerEquipment[PlayerConstants.playerChest] == 10043) {
			return true;
		}
		return false;
	}

	public static boolean isHunterNpc(int npcId) {
		for (HunterData h : HunterData.values) {
			if (h.getNpcId() == npcId) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean handlingHunterNpc(Player p, final NPC npc) {
		for (HunterData h : HunterData.values) {
			if (h.getNpcId() == npc.npcType) {
				Hunter.checkReq(p, npc, h);
				return true;
			}
		}
		return false;
	}

}
