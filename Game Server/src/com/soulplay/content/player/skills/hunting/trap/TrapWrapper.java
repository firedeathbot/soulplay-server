package com.soulplay.content.player.skills.hunting.trap;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public final class TrapWrapper {

	/**
	 * The items to be received on trap clearing.
	 */
	private final List<Item> items = new ArrayList<>();

	/**
	 * The player instance.
	 */
	private final Player player;

	/**
	 * The type of trap.
	 */
	private final BoxTrap type;

	/**
	 * The original object id.
	 */
	private final int originalId;

	/**
	 * The object of the trap.
	 */
	private RSObject object;

	/**
	 * The secondary game object.
	 */
	private RSObject secondary;

	/**
	 * The reward of the trap.
	 */
	private BoxTrap reward;

	/**
	 * If the trap has been smoked.
	 */
	private boolean smoked;

	/**
	 * If the trap has been baited.
	 */
	private boolean baited;

	/**
	 * If the trap has failed.
	 */
	private boolean failed;

	/**
	 * If the trap is currently in an action.
	 */
	private int busyTicks;

	/**
	 * The tick when the life is up.
	 */
	private int ticks;

	/**
	 * Constructs a new {@code TrapWrapper} {@code Object}.
	 * @param player the player.
	 * @param type the type.
	 * @param object the object.
	 */
	public TrapWrapper(final Player player, BoxTrap type, RSObject object) {
		this.player = player;
		this.type = type;
		this.object = object;
		this.originalId = object.getNewObjectId();
	}

	/**
	 * Sets the new object of the wrapper.
	 * @param id the id.
	 */
	public void setObject(final int id) {
		object.setTick(0);
		ObjectManager.addObject(new RSObject(id, object.getX(), object.getY(), object.getHeight(), object.getFace(), object.getType(), -1, 60+Misc.random(40)));
	}

	/**
	 * Smokes the trap.
	 */
	public void smoke() {
		if (smoked) {
			player.sendMessage("This trap has already been smoked.");
			return;
		}
		if (player.getSkills().getLevel(Skills.HUNTER) < 39) {
			player.sendMessage("You need a Hunter level of at least 39 to be able to smoke traps.");
			return;
		}
		smoked = true;
//		player.lock(4);
		player.startAnimation(5208);
		player.startGraphic(Graphic.create(931));
		player.sendMessage("You use the smoke from the torch to remove your scent from the trap.");
	}

	/**
	 * Baits the trap.
	 * @param item the item.
	 */
	public void bait(Item bait) {
		if (baited) {
			player.sendMessage("This trap has already been baited.");
			return;
		}
		baited = true;
		player.getItems().deleteItem2(bait.getId(), 1);
	}

	/**
	 * Gets the chance ratio of luring.
	 * @return the chance rate.
	 */
	public double getChanceRate() {
		double chance = 0.0;
		if (baited) {
			chance += 1.0;
		}
		if (smoked) {
			chance += 1.0;
		}
		//chance += HunterGear.getChanceRate(player);
		return chance;
	}

	/**
	 * Adds items to the received list.
	 * @param items the items.
	 */
	public void addItem(Item... items) {
		for (Item item : items) {
			addItem(item);
		}
	}

	/**
	 * Adds an item to the received list.
	 * @param item the item.
	 */
	public void addItem(Item item) {
		items.add(item);
	}

	/**
	 * Gets the type.
	 * @return The type.
	 */
	public BoxTrap getType() {
		return type;
	}

	/**
	 * Gets the object.
	 * @return The object.
	 */
	public RSObject getObject() {
		return object;
	}

	/**
	 * The original id.
	 * @return the id.
	 */
	public int getOriginalId() {
		return originalId;
	}

	/**
	 * Gets the player.
	 * @return The player.
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets the ticks.
	 * @return The ticks.
	 */
	public int getTicks() {
		return ticks;
	}

	/**
	 * Sets the ticks.
	 * @param ticks The ticks to set.
	 */
	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

	/**
	 * Gets the smoked.
	 * @return The smoked.
	 */
	public boolean isSmoked() {
		return smoked;
	}

	/**
	 * Sets the smoked.
	 * @param smoked The smoked to set.
	 */
	public void setSmoked(boolean smoked) {
		this.smoked = smoked;
	}

	/**
	 * Gets the baited.
	 * @return The baited.
	 */
	public boolean isBaited() {
		return baited;
	}

	/**
	 * Sets the baited.
	 * @param baited The baited to set.
	 */
	public void setBaited(boolean baited) {
		this.baited = baited;
	}

	/**
	 * Checks if the trap has been caught.
	 * @return {@code True} if so.
	 */
	public boolean isCaught() {
		return getReward() != null;
	}

	/**
	 * Gets the reward.
	 * @return The reward.
	 */
	public BoxTrap getReward() {
		return reward;
	}

	/**
	 * Sets the reward.
	 * @param reward The reward to set.
	 */
	public void setReward(BoxTrap reward) {
		this.reward = reward;
		this.addItem(reward.getRewards());
	}

	/**
	 * Checks if the trap is busy.
	 * @return {@code True} if so.
	 */
	public boolean isBusy() {
		return getBusyTicks() > Server.getTotalTicks();
	}

	/**
	 * Gets the busyTicks.
	 * @return The busyTicks.
	 */
	public int getBusyTicks() {
		return busyTicks;
	}

	/**
	 * Sets the busyTicks.
	 * @param busyTicks The busyTicks to set.
	 */
	public void setBusyTicks(int busyTicks) {
		this.busyTicks = Server.getTotalTicks() + busyTicks;
	}

	/**
	 * Gets the items.
	 * @return The items.
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * Gets the secondary.
	 * @return The secondary.
	 */
	public RSObject getSecondary() {
		return secondary;
	}

	/**
	 * Sets the secondary.
	 * @param secondary The secondary to set.
	 */
	public void setSecondary(RSObject secondary) {
		this.secondary = secondary;
	}

	/**
	 * Sets the object.
	 * @param object The object to set.
	 */
	public void setObject(RSObject object) {
		this.object = object;
	}

	/**
	 * Gets the failed.
	 * @return The failed.
	 */
	public boolean isFailed() {
		return failed;
	}

	/**
	 * Sets the failed.
	 * @param failed The failed to set.
	 */
	public void setFailed(boolean failed) {
		this.failed = failed;
	}

}
