package com.soulplay.content.player.skills.hunting.trap;

import java.util.HashMap;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;

public enum BoxTrap {

	FERRET(new int[] { 5081 }, 27, 100, new Item[] { new Item(10092) }, 1),
	GECKO(new int[] { 6918, 7289, 7290, 7291, 7292 }, 27, 100, new Item[] { new Item(12488) }, 10),
	RAIN(new int[] { 1487 }, 27, 100, new Item[] { new Item(4033, 1) }, 95),
	PLATYPUS(new int[] { 7021, 7022, 7023 }, 48, 150, new Item[] { new Item(12551, 1) }, 1),
	GRAY_CHIN(new int[] { 5079 }, 53, 198, new Item[] { new Item(10033, 1) }, 1),
	PENGUIN(new int[] { 5428, 5430, 5449, 5450, 5451 }, 56, 150, new Item[] { new Item(12188) }, 1),
	RED_CHIN(new int[] { 5080 }, 63, 265, new Item[] { new Item(10034, 1) }, 1),
	MAN_SITTINGONCHAIR(new int[] { 8634 }, 73, 315, new Item[] { new Item(14993, 1) }, 1),
	BABY_GECKO(new int[] { 6915, 6917, 7277, 7278, 7279, 7280, 7285, 7286 }, 27, 20, new Item[] { new Item(12488, 1) }, 1),
	BABY_MONKEY(new int[] { 6944, 6942, 7210, 7212, 7214, 7216, 7218, 7220, 7222, 7224, 7226, 7227, 7228, 7229, 7330, 7231, 7232, 7233, 7234, 7235, 7236 }, 27, 15, new Item[] { new Item(12496, 1) }, 1),
	PAWYA(new int[] { 7012, 7014 }, 66, 400, new Item[] { new Item(12535) }, 1),
	GRENWALL(new int[] { 7010, 7011 }, 77, 1100, new Item[] { new Item(12539, 1) }, 1);


	/**
	 * The npc ids related to the node.
	 */
	private final int[] npcIds;

	/**
	 * The level required.
	 */
	private final int level;

	/**
	 * The experience received.
	 */
	private final double experience;

	/**
	 * The object ids for this node.
	 */
	private final int[] objectIds;

	/**
	 * The rewards received.
	 */
	private final Item[] rewards;
	
	private final int summoningLevel;

	/**
	 * Constructs a new {@code TrapNode} {@code Object}.
	 * @param npcIds the npc ids.
	 * @param level the level.
	 * @param experience the experience.
	 * @param rewards the rewards.
	 */
	private BoxTrap(int[] npcIds, int level, int experience, Item[] rewards, int summonLevel) {
		this.npcIds = npcIds;
		this.level = level;
		this.experience = experience;
		this.objectIds = new int[] { 19188, 19189 };
		this.rewards = rewards;
		this.summoningLevel = summonLevel;
	}

	/**
	 * Has the requirements to catch the node.
	 * @param wrapper the wrapper.
	 * @param npc the npc.
	 * @return {@code True} if so.
	 * @note Override for quests.
	 */
	public boolean canCatch(TrapWrapper wrapper, final NPC npc) {
		final Player player = wrapper.getPlayer();
		if (wrapper.isCaught() || wrapper.isBusy() || wrapper.isFailed()) {
			return false;
		}
		return player.getSkills().getLevel(Skills.HUNTER) >= level && !npc.isDead();
	}

	/**
	 * Gets the transform id.
	 * @return the id.
	 */
	public int getTransformId() {
		return objectIds[0];
	}

	/**
	 * Gets the final id.
	 * @return the id.
	 */
	public int getFinalId() {
		return objectIds[1];
	}

	/**
	 * Gets the npcIds.
	 * @return The npcIds.
	 */
	public int[] getNpcIds() {
		return npcIds;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the experience.
	 * @return The experience.
	 */
	public double getExperience() {
		return experience;
	}

	/**
	 * Gets the rewards.
	 * @return The rewards.
	 */
	public Item[] getRewards() {
		return rewards;
	}

	/**
	 * Gets the objectIds.
	 * @return The objectIds.
	 */
	public int[] getObjectIds() {
		return objectIds;
	}

	public int getSummoningLevel() {
		return summoningLevel;
	}

	public static int getMaximumTraps(Client c) {
		final int level = c.getSkills().getLevel(Skills.HUNTER);
		int original = level >= 80 ? 5 : level >= 60 ? 4 : level >= 40 ? 3 : level >= 20 ? 2 : 1;
		int mod = 0; //player.getDetails().getShop().hasPerk(Perks.MASTER_HUNTSMAN) ? 1 : 0;
		return original + mod;
	}
	
	public static BoxTrap forNpcId(int npcId) {
		return map.get(npcId);
	}
	
	public final static TrapSetting SETTING = new TrapSetting(10008, new int[] { 19187 }, new int[] { 1963, 12579, 1869, 9996, 5972, 12535 }, "lay", 19192, new Animation(5208), new Animation(9726), 27);
	
	private static HashMap<Integer, BoxTrap> map = new HashMap<Integer, BoxTrap>();
	
	static {
		for (BoxTrap trap : BoxTrap.values()) {
			for (int id : trap.npcIds) {
				map.put(id, trap);
			}
		}
	}
	
    public static void load() {
    	/* empty */
    }
	
}
