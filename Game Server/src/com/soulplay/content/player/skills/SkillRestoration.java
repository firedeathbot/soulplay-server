package com.soulplay.content.player.skills;

import com.soulplay.Server;
import com.soulplay.game.world.entity.Mob;

/**
 * Handles the skill restoration data.
 */
public final class SkillRestoration {

	public static final int RESTORE_TICKS = 100;

	/**
	 * The skill index.
	 */
	private final int skillId;

	/**
	 * The current tick.
	 */
	private int tick;
	private int restoreTicks;

	/**
	 * Constructs a new {@code SkillRestoration} {@code Object}.
	 * @param skillId The skill id.
	 */
	public SkillRestoration(int skillId) {
		this.skillId = skillId;
		this.restoreTicks = RESTORE_TICKS;
		this.tick = Server.getTotalTicks() + restoreTicks;
	}

	/**
	 * Restores the skill.
	 * @param entity The entity.
	 */
	public void restore(Mob entity) {
		if (tick > Server.getTotalTicks()) {
			return;
		}

		Skills skills = entity.getSkills();
		if (skillId == Skills.HITPOINTS) {
			int hp = skills.getLifepoints();
			int maxHp = skills.getMaximumLifepoints();
			if (hp < maxHp)
				skills.heal(1, maxHp);
			else if (hp > maxHp) {
				skills.hit(1);
			}
		} else {
			int stat = skills.getLevel(skillId);
			int base = skills.getStaticLevel(skillId);
			if (stat < base) {
				skills.updateLevel(skillId, 1, base);
			} else if (stat > base) {
				skills.updateLevel(skillId, -1, base);
			}
		}

		restart();
	}

	/**
	 * Gets the tick.
	 * @return The tick.
	 */
	public int getTick() {
		return tick;
	}

	/**
	 * Gets the skillId.
	 * @return The skillId.
	 */
	public int getSkillId() {
		return skillId;
	}

	/**
	 * Restarts the restoration.
	 */
	public void restart() {
		this.tick = Server.getTotalTicks() + restoreTicks;
	}

	public void setRestoreTicks(int restoreTicks) {
		this.restoreTicks = restoreTicks;
	}

}