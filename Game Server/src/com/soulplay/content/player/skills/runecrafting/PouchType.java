package com.soulplay.content.player.skills.runecrafting;

public enum PouchType {
    
	SMALL(5509, 1, 3, -1, "small_pouch"),
	MEDIUM(5510, 25, 6, 45, "medium_pouch"),
	LARGE(5512, 50, 9, 29, "large_pouch"),
	GIANT(5514, 75, 12, 10, "giant_pouch");
	
	public static final PouchType[] values = PouchType.values();
	
	private final int id;
	private final int level;
	private final int capacity;
	private final int uses;
	private final String table;
	
	private PouchType(int id, int level, int capacity, int uses, String table) {
	    this.id = id;
	    this.level = level;
	    this.capacity = capacity;
	    this.uses = uses;
	    this.table = table;
	}

	public int getId() {
	    return id;
	}

	public int getLevel() {
	    return level;
	}

	public int getCapacity() {
	    return capacity;
	}
	
	public int getUses() {
	    return uses;
	}
	
	public String getTable() {
	    return table;
	}

}
