package com.soulplay.content.player.skills.runecrafting;

import com.soulplay.content.player.achievement.Achievs;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;

public final class Runecrafting {

	public static final Location[] RANDOM_ABYS_SPAWNS = new Location[] { Location.create(3059, 4849),
			Location.create(3063, 4840), Location.create(3064, 4831), Location.create(3065, 4822),
			Location.create(3060, 4810), Location.create(3054, 4808), Location.create(3047, 4808),
			Location.create(3039, 4806), Location.create(3032, 4808), Location.create(3026, 4806),
			Location.create(3017, 4812), Location.create(3015, 4834), Location.create(3018, 4843),
			Location.create(3023, 4852), Location.create(3029, 4853), Location.create(3039, 4856),
			Location.create(3051, 4853), };

	public static final Location AIR_ALTAR = Location.create(2841, 4829);
	public static final Location MIND_ALTAR = Location.create(2793, 4828);
	public static final Location WATER_ALTAR = Location.create(2725, 4832);
	public static final Location EARTH_ALTAR = Location.create(2655, 4830);
	public static final Location FIRE_ALTAR = Location.create(2574, 4848);
	public static final Location BODY_ALTAR = Location.create(2523, 4826);
	public static final Location COSMIC_ALTAR = Location.create(2162, 4833);
	public static final Location CHAOS_ALTAR = Location.create(2281, 4837);
	public static final Location NATURE_ALTAR = Location.create(2400, 4835);
	public static final Location LAW_ALTAR = Location.create(2464, 4818);
	public static final Location DEATH_ALTAR = Location.create(2208, 4830);
	public static final Location BLOOD_ALTAR = Location.create(2468, 4889, 1);
	public static final Location ASTRAL_ALTAR = Location.create(2153, 3868);

	public static final Location RUNECRAFTING_BOT_TRAP = Location.create(2338, 4747);

	public static final int TIARA = 5525;
	public static final int RUNE_ESSENCE = 1436;
	public static final int PURE_ESSENCE = 7936;
	
	public static final int DUST_OF_ARMADYL = 21774;

	private final Client player;

	private final Pouch smallPouch;
	private final Pouch mediumPouch;
	private final Pouch largePouch;
	private final Pouch giantPouch;

	public Runecrafting(Client player) {
		this.player = player;
		smallPouch = new Pouch(player, PouchType.SMALL);
		mediumPouch = new Pouch(player, PouchType.MEDIUM);
		largePouch = new Pouch(player, PouchType.LARGE);
		giantPouch = new Pouch(player, PouchType.GIANT);
	}
	
	public void craftArmadylRunes() {
		if (!player.getItems().playerHasItem(RUNE_ESSENCE) && !player.getItems().playerHasItem(PURE_ESSENCE)) {
			player.sendMessage("You don't have any essence to craft runes.");
			return;
		}

		if (player.getPlayerLevel()[Skills.RUNECRAFTING] < RunecraftingData.ARMADYL_RUNE.getSkillLevelRequired()) {
			player.sendMessage("You need a runecrafting level of " + RunecraftingData.ARMADYL_RUNE.getSkillLevelRequired() + " to use this.");
			return;
		}
		
		final int dustAmount = player.getItems().getItemAmount(DUST_OF_ARMADYL);

		final int pureAmount = player.getItems().getItemAmount(PURE_ESSENCE);
		
		final int essAmount = player.getItems().getItemAmount(RUNE_ESSENCE);
		
		final int totalEssAmount = Math.min(dustAmount, pureAmount + essAmount);
		
		player.getItems().deleteItem2(PURE_ESSENCE, Math.min(dustAmount, pureAmount));		
		player.getItems().deleteItem2(RUNE_ESSENCE, Math.min(dustAmount, essAmount));
		player.getItems().deleteItem2(DUST_OF_ARMADYL, Math.min(dustAmount, pureAmount) + Math.min(dustAmount, essAmount));
		
		player.startGraphic(Graphic.create(186, GraphicType.HIGH));
		player.startAnimation(791);
		
		final int productAmount = RunecraftingData.AIR_RUNE.getMultipler(player) * totalEssAmount;
		
		player.getItems().addItem(RunecraftingData.ARMADYL_RUNE.getRuneId(), productAmount);
		
		player.getPA().addSkillXP((int) (RunecraftingData.ARMADYL_RUNE.getBaseExperience() * totalEssAmount), RSConstants.RUNECRAFTING);

		player.sendMessage("You bind the temple's power into " + productAmount + " " + RunecraftingData.ARMADYL_RUNE.getName() + "s.");

		player.getAchievement().increaseProgress(Achievs.CRAFT_10K_RUNES, productAmount);
		player.increaseProgress(TaskType.RUNECRAFTING, RunecraftingData.ARMADYL_RUNE.getRuneId(), productAmount);
	}

	public void craftRunes(RunecraftingData data) {
		if (!player.getItems().playerHasItem(RUNE_ESSENCE) && !player.getItems().playerHasItem(PURE_ESSENCE)) {
			player.sendMessage("You don't have any essence to craft runes.");
			return;
		}

		if (player.getPlayerLevel()[Skills.RUNECRAFTING] < data.getSkillLevelRequired()) {
			player.sendMessage("You need a runecrafting level of " + data.getSkillLevelRequired() + " to use this.");
			return;
		}

		int runeEssenceAmount = 0;

		if (player.getItems().playerHasItem(RUNE_ESSENCE)) {
			runeEssenceAmount += player.getItems().getItemAmount(RUNE_ESSENCE);
		}

		int pureEssenceAmount = 0;

		if (player.getItems().playerHasItem(PURE_ESSENCE)) {
			pureEssenceAmount += player.getItems().getItemAmount(PURE_ESSENCE);
		}

		final int totalEssenceAmount = runeEssenceAmount + pureEssenceAmount;

		player.getItems().deleteItem2(RUNE_ESSENCE, runeEssenceAmount);
		player.getItems().deleteItem2(PURE_ESSENCE, pureEssenceAmount);

		player.startGraphic(Graphic.create(186, GraphicType.HIGH));
		player.startAnimation(791);

		final int productAmount = totalEssenceAmount * data.getMultipler(player);

		player.getItems().addItem(data.getRuneId(), productAmount);

		player.getPA().addSkillXP((int) (data.getBaseExperience() * totalEssenceAmount), RSConstants.RUNECRAFTING);

		player.sendMessage("You bind the temple's power into " + productAmount + " " + data.getName() + "s.");

		player.getAchievement().increaseProgress(Achievs.CRAFT_10K_RUNES, productAmount);
		player.increaseProgress(TaskType.RUNECRAFTING, data.getRuneId(), productAmount);

		if (data == RunecraftingData.LAW_RUNE) {
			player.getAchievement().increaseProgress(Achievs.CRAFT_200_LAWS, productAmount);
		} else if (data == RunecraftingData.AIR_RUNE) {
			player.getAchievement().increaseProgress(Achievs.CRAFT_100_AIR, productAmount);
		}

	}

	public Pouch getPouch(PouchType type) {
		switch (type) {

		case SMALL:
			return smallPouch;

		case MEDIUM:
			return mediumPouch;

		case LARGE:
			return largePouch;

		case GIANT:
			return giantPouch;

		default:
			throw new UnsupportedOperationException("This pouch is not supported.");
		}
	}

}
