package com.soulplay.content.player.skills.runecrafting;

import java.util.Arrays;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;

public enum RunecraftingData {
    
    AIR_RUNE(556, 17780, 1, 5.0, 11, 22, 33, 44, 55, 66, 77, 88, 99),
    MIND_RUNE(558, 17784, 2, 5.5, 14, 28, 42, 56, 70, 84, 98),
    WATER_RUNE(555, 17781, 5, 6.0, 19, 38, 57, 76, 95),
    EARTH_RUNE(557, 17782, 9, 6.5, 26, 52, 78),
    FIRE_RUNE(554, 17783, 14, 7.0, 35, 70),
    BODY_RUNE(559, 17788, 20, 7.5, 46, 92),
    COSMIC_RUNE(564, 17789, 27, 8.0, 59),
    CHAOS_RUNE(562, 17785, 35, 8.5, 74),
    ASTRAL_RUNE(9075, 17790, 40, 8.7, 82),
    NATURE_RUNE(561, 17791, 44, 9.0, 91),
    LAW_RUNE(563, 17792, 54, 9.5, 95),
    DEATH_RUNE(560, 17786, 65, 10.0, 99),
    ARMADYL_RUNE(21773, -1, 72, 10.0, 77, 88, 99),
    BLOOD_RUNE(565, 17787, 77, 23.8),
    SOUL_RUNE(566, 17793, 90, 29.7);
    
    private final int runeId;

    private final int dungRuneId;

    private final int skillLevelRequired;
    
    private final double baseExperience;    
    
    private final int[] multipliers;
    
    private final String name;
    
    private RunecraftingData(int runeId, int dungRuneId, int skillLevelRequired, double experience, int... multipliers) {
	this.runeId = runeId;
	this.dungRuneId = dungRuneId;
	this.skillLevelRequired = skillLevelRequired;
	this.baseExperience = experience;
	this.multipliers = multipliers;
	this.name = makeName();
    }
    
    public String getName() {
    	return name;
    }
    
    public String makeName() {
	String name = name().toLowerCase().replace("_", " ");
	
	name = name.substring(0, 1).toUpperCase() + name.substring(1);
	
	return name;	
    }

    public int getDungRuneId() {
    	return dungRuneId;
    }

    public int getRuneId() {
        return runeId;
    }
    
    public int  getSkillLevelRequired() {
	return skillLevelRequired;
    }
    
    public double getBaseExperience() {
	return baseExperience;
    }

    public int[] getMultipliers() {
        return multipliers;
    }
    
    public int getMultipler(Client c) {	
	return Math.toIntExact(Arrays.stream(multipliers).filter(it -> c.getPlayerLevel()[Skills.RUNECRAFTING] >= it).count()) + 1;	
    }
    
}
