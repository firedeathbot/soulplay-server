package com.soulplay.content.player.skills.runecrafting;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.util.SQLUtils;

public final class Pouch {

    private final PouchType type;

    private final int[] container;

    private final int[] uses = new int[PouchType.values.length];
    
    private int counter;
    
    private final Client player;

    public Pouch(Client player, PouchType type) {
	this.player = player;
	this.type = type;
	this.container = new int[type.getCapacity()];
    }

    public void fill() {
	if (player.getPlayerLevel()[Skills.RUNECRAFTING] < type.getLevel()) {
	    player.sendMessage(
		    String.format("You need a runecrafting level of %d to use this pouch.", type.getLevel()));
	    return;
	}

	if (!player.getItems().playerHasItem(Runecrafting.RUNE_ESSENCE)
		&& !player.getItems().playerHasItem(Runecrafting.PURE_ESSENCE)) {
	    player.sendMessage("You don't have any essence.");
	    return;
	}
	
	if (getFreeSlots() == 0) {
	    player.sendMessage("This pouch is full!");
	    return;
	}

	int runeEssenceAmount = player.getItems().getItemAmount(Runecrafting.RUNE_ESSENCE);

	int freeSlots = getFreeSlots();

	if (runeEssenceAmount > freeSlots) {
	    runeEssenceAmount = freeSlots;
	}

	for (int i = 0; i < runeEssenceAmount; i++) {

	    if (i == freeSlots) {
		break;
	    }

	    if (counter == container.length) {
		return;
	    }

	    if (player.getItems().deleteItem2(Runecrafting.RUNE_ESSENCE, 1)) {
		container[counter] = Runecrafting.RUNE_ESSENCE;
		counter++;
		
		if (counter > container.length) {
		    counter = container.length;
		}
	    }

	}

	int pureEssenceAmount = player.getItems().getItemAmount(Runecrafting.PURE_ESSENCE);

	freeSlots = getFreeSlots();

	for (int i = 0; i < pureEssenceAmount; i++) {

	    if (i == freeSlots) {
		break;
	    }

	    if (counter == container.length) {
		return;
	    }

	    if (player.getItems().deleteItem2(Runecrafting.PURE_ESSENCE, 1)) {
		container[counter] = Runecrafting.PURE_ESSENCE;
		counter++;
		
		if (counter > container.length) {
		    counter = container.length;
		}
	    }

	}
	
	PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

		final String sql = "UPDATE `accounts` SET " + type.getTable() + " = ? WHERE ID = ?;";

		try (java.sql.Connection connection = Server.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, SQLUtils.createStringFromIntArray(container));
			statement.setInt(2, player.mySQLIndex);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	});

    }

    public void empty() {
	
	if (this.getFreeSlots() == container.length) {
	    player.sendMessage("This pouch is empty!");
	    return;
	}	
	
	for (int i = 0; i < container.length; i++) {
	    
	    int essence = container[i];
	    
	    if (essence == 0) {
		continue;
	    }
	    
	    if (player.getItems().freeSlots() <= 0) {
		break;
	    }
	    
	    if (player.getItems().addItem(essence, 1)) {
		container[i] = 0;
		counter--;
		
		if (counter < 0) {
		    counter = 0;
		}
	    }
	    
	}
	
	PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

		final String sql = "UPDATE `accounts` SET " + type.getTable() + " = ? WHERE ID = ?;";

		try (java.sql.Connection connection = Server.getConnection();
				PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, SQLUtils.createStringFromIntArray(container));
			statement.setInt(2, player.mySQLIndex);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	});
	
    }
    
    public void check() {
	player.sendMessage(String.format("You currently have %d essence in this pouch.", counter));
    }
    
    public void add(int itemId, int slot) {
	if (slot < 0 || slot >= container.length) {
	    return;
	}
	
	if (itemId != Runecrafting.RUNE_ESSENCE && itemId != Runecrafting.PURE_ESSENCE) {
	    return;
	}
	
	container[slot] = itemId;
	counter++;
    }

    public int getCount() {
	return counter;
    }

    public int getFreeSlots() {
	return container.length - counter;
    }

    public PouchType getType() {
	return type;
    }

    public int getRemainingUses() {
	return uses[type.ordinal()];
    }

}
