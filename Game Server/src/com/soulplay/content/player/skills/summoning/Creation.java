package com.soulplay.content.player.skills.summoning;

import java.util.HashMap;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

public class Creation {

	public static enum Data {
		SPIRIT_WOLF(155031, 12047, 12425, new int[] { GOLD_CHARM, 2859 }, 7, 1, 5),
		SPIRIT_DREADFOWL(155034, 12043, 12445, new int[] { GOLD_CHARM, 2138 }, 8, 8, 9),
		SPIRIT_SPIDER(155037, 12059, 12428, new int[] { GOLD_CHARM, 6291 }, 8, 8, 10),
		THORNY_SNAIL(155040, 12019, 12459, new int[] { GOLD_CHARM, 3363 }, 9, 13, 12),
		GRANITE_CRAB(155043, 12009, 12533, new int[] { GOLD_CHARM, 440 }, 7, 16, 22),
		SPIRIT_MOSQUITO(155046, 12778, 12838, new int[] { GOLD_CHARM, 6319 }, 1, 17, 47),
		DESERT_WYRM(155049, 12049, 12460, new int[] { GREEN_CHARM, 1783 }, 45, 18, 31),
		SPIRIT_SCORPION(155052, 12055, 12432, new int[] { CRIMSON_CHARM, 3095 }, 57, 19, 83),
		SPIRIT_TZ_KIH(155055, 12808, 12839, new int[] { CRIMSON_CHARM, OBSIDIAN_CHARM }, 64, 22, 97),
		ALBINO_RAT(155058, 12067, 12430, new int[] { BLUE_CHARM, 2134 }, 75, 23, 202),
		SPIRIT_KALPHITE(155061, 12063, 12446, new int[] { BLUE_CHARM, 3138 }, 51, 25, 220),
		COMPOST_MOUND(155064, 12091, 12440, new int[] { GREEN_CHARM, 6032 }, 47, 28, 50),
		GIANT_CHINCHOMPA(155067, 12800, 12834, new int[] { BLUE_CHARM, 9976 }, 84, 29, 255),
		VAMPIRE_BAT(155070, 12053, 12447, new int[] { CRIMSON_CHARM, 3325 }, 81, 31, 136),
		HONEY_BADGER(155073, 12065, 12433, new int[] { CRIMSON_CHARM, 12156 }, 84, 32, 141),
		BEAVER(155076, 12021, 12429, new int[] { GREEN_CHARM, 1519 }, 72, 33, 58),
		VOID_RAVAGER(155079, 12818, 12443, new int[] { GREEN_CHARM, RAVAGER_CHARM }, 74, 34, 60),
		VOID_SHIFTER(155082, 12814, 12443, new int[] { BLUE_CHARM, SHIFTER_CHARM }, 74, 34, 60),
		VOID_SPINNER(155085, 12780, 12443, new int[] { BLUE_CHARM, SPINNER_CHARM }, 74, 34, 60),
		VOID_TORCHER(155088, 12798, 12443, new int[] { BLUE_CHARM, TORCHER_CHARM }, 74, 34, 60),
		BRONZE_MINOTAUR(155091, 12073, 12461, new int[] { BLUE_CHARM, 2349 }, 102, 36, 317),
		BULL_ANT(155094, 12087, 12431, new int[] { GOLD_CHARM, 6010 }, 11, 40, 53),
		MACAW(155097, 12071, 12422, new int[] { GREEN_CHARM, 249 }, 78, 41, 72),
		EVIL_TURNIP(155100, 12051, 12442, new int[] { CRIMSON_CHARM, 12153 }, 104, 42, 185),
		SPIRIT_COCKATRICE(155103, 12095, 12458, new int[] { GREEN_CHARM, 12109 }, 88, 43, (int) 75.2),
		SPIRIT_GUTHATRICE(155106, 12097, 12458, new int[] { GREEN_CHARM, 12111 }, 88, 43, (int) 75.2),
		SPIRIT_SARATRICE(155109, 12099, 12458, new int[] { GREEN_CHARM, 12113 }, 88, 43, (int) 75.2),
		SPIRIT_ZAMATRICE(155112, 12101, 12458, new int[] { GREEN_CHARM, 12115 }, 88, 43, (int) 75.2),
		SPIRIT_PENGATRICE(155115, 12103, 12458, new int[] { GREEN_CHARM, 12117 }, 88, 43, (int) 75.2),
		SPIRIT_CORAXATRICE(155118, 12105, 12458, new int[] { GREEN_CHARM, 12119 }, 88, 43, (int) 75.2),
		SPIRIT_VULATRICE(155121, 12107, 12458, new int[] { GREEN_CHARM, 12121 }, 88, 43, (int) 75.2),
		IRON_MINOTAUR(155124, 12075, 12462, new int[] { BLUE_CHARM, 2351 }, 125, 46, 405),
		PYRELORD(155127, 12816, 12829, new int[] { CRIMSON_CHARM, 590 }, 111, 46, 202),
		MAGPIE(155130, 12041, 12426, new int[] { GREEN_CHARM, 1635 }, 88, 47, 83),
		BLOATED_LEECH(155133, 12061, 12444, new int[] { CRIMSON_CHARM, 2132 }, 117, 49, 215),
		SPIRIT_TERRORBIRD(155136, 12007, 12441, new int[] { GOLD_CHARM, 9978 }, 12, 52, 68),
		ABYSSAL_PARASITE(155139, 12035, 12454, new int[] { GREEN_CHARM, ABYSSAL_CHARM }, 106, 54, 95),
		SPIRIT_JELLY(155142, 12027, 12453, new int[] { BLUE_CHARM, 1937 }, 151, 55, 484),
		IBIS(155145, 12531, 12424, new int[] { GREEN_CHARM, 311 }, 109, 56, 99),
		STEEL_MINOTAUR(155148, 12077, 12463, new int[] { BLUE_CHARM, 2353 }, 141, 56, 493),
		SPIRIT_GRAAHK(155151, 12810, 12835, new int[] { BLUE_CHARM, 10099 }, 154, 57, 502),
		SPIRIT_KYATT(155154, 12812, 12836, new int[] { BLUE_CHARM, 10103 }, 153, 57, 502),
		SPIRIT_LARUPIA(155157, 12784, 12840, new int[] { BLUE_CHARM, 10095 }, 155, 57, 502),
		KHARAMTHULHU_OVERLORD(155160, 12023, 12455, new int[] { BLUE_CHARM, 6667 }, 144, 58, 510),
		SMOKE_DEVIL(155163, 12085, 12468, new int[] { CRIMSON_CHARM, 9736 }, 141, 61, 268),
		ABYSSAL_LURKER(155166, 12037, 12427, new int[] { GREEN_CHARM, ABYSSAL_CHARM }, 119, 62, 110),
		SPIRIT_COBRA(155169, 12015, 12436, new int[] { CRIMSON_CHARM, 6287 }, 116, 63, 269),
		STRANGER_PLANT(155172, 12045, 12467, new int[] { CRIMSON_CHARM, 8431 }, 128, 64, 282),
		MITHRIL_MINOTAUR(155175, 12079, 12464, new int[] { BLUE_CHARM, 2359 }, 152, 66, 581),
		BARKER_TOAD(155178, 12123, 12452, new int[] { GOLD_CHARM, 2150 }, 11, 66, 87),
		WAR_TORTOISE(155181, 12031, 12439, new int[] { GOLD_CHARM, 7939 }, 1, 67, 59),
		BUNYIP(155184, 12029, 12438, new int[] { GREEN_CHARM, 383 }, 110, 68, 120),
		FRUIT_BAT(155187, 12033, 12423, new int[] { GREEN_CHARM, 1963 }, 130, 69, 121),
		RAVENOUS_LOCUST(155190, 12820, 12830, new int[] { CRIMSON_CHARM, 1933 }, 79, 70, 132),
		ARCTIC_BEAR(155193, 12057, 12451, new int[] { GOLD_CHARM, 10117 }, 14, 71, 93),
		PHOENIX(155196, 14623, 14622, new int[] { CRIMSON_CHARM, 14616 }, 165, 72, 301),
		OBSIDIAN_GOLEM(155199, 12792, 12826, new int[] { BLUE_CHARM, OBSIDIAN_CHARM }, 195, 73, 642),
		GRANITE_LOBSTER(155202, 12069, 12449, new int[] { CRIMSON_CHARM, 6979 }, 166, 74, 326),
		PRAYING_MANTIS(155205, 12011, 12450, new int[] { CRIMSON_CHARM, 2460 }, 168, 75, 330),
		FORGE_REGENT(155208, 12782, 12841, new int[] { GREEN_CHARM, 10020 }, 141, 76, 134),
		ADAMANT_MINOTAUR(155211, 12081, 12465, new int[] { BLUE_CHARM, 2361 }, 144, 76, 669),
		TALON_BEAST(155214, 12794, 12831, new int[] { CRIMSON_CHARM, TALON_BEAST_CHARM }, 174, 77, 1015),
		GIANT_ENT(155217, 12013, 12457, new int[] { GREEN_CHARM, 5933 }, 124, 78, 137),
		FIRE_TITAN(155220, 12802, 12824, new int[] { BLUE_CHARM, 1442 }, 198, 79, 695),
		MOSS_TITAN(155223, 12804, 12824, new int[] { BLUE_CHARM, 1440 }, 202, 79, 695),
		ICE_TITAN(155226, 12806, 12824, new int[] { BLUE_CHARM, 1444 }, 198, 79, 695),
		HYDRA(155229, 12025, 12442, new int[] { GREEN_CHARM, 571 }, 128, 80, 141),
		SPIRIT_DAGGANOTH(155232, 12017, 12456, new int[] { CRIMSON_CHARM, 6155 }, 1, 83, 365),
		LAVA_TITAN(155235, 12788, 12837, new int[] { BLUE_CHARM, OBSIDIAN_CHARM }, 219, 83, 730),
		SWAMP_TITAN(155238, 12776, 12832, new int[] { CRIMSON_CHARM, 10149 }, 150, 85, 374),
		RUNE_MINOTAUR(155241, 12083, 12466, new int[] { BLUE_CHARM, 2363 }, 1, 86, 757),
		UNICORN_STALLION(155244, 12039, 12434, new int[] { GREEN_CHARM, 237 }, 140, 88, 154),
		GEYSER_TITAN(155247, 12786, 12833, new int[] { BLUE_CHARM, 1444 }, 222, 89, 783),
		WOLPERTINGER(155250, 12089, 12437, new int[] { CRIMSON_CHARM, 3226 }, 203, 92, 405),
		ABYSSAL_TITAN(155253, 12796, 12827, new int[] { GREEN_CHARM, ABYSSAL_CHARM }, 113, 93, 163),
		IRON_TITAN(156000, 12822, 12828, new int[] { CRIMSON_CHARM, 1115 }, 198, 95, 418),
		PACK_YAK(156003, 12093, 12435, new int[] { CRIMSON_CHARM, 10818 }, 211, 96, 422),
		STEEL_TITAN(156006, 12790, 12825, new int[] { CRIMSON_CHARM, 1119 }, 178, 99, 435);
		
		public static final Data[] values = Data.values();

		private static int scroll_button = 22769;
		
		private static HashMap<Integer, Data> buttonMap = new HashMap<Integer, Data>();
		private static HashMap<Integer, Data> pouchMap = new HashMap<Integer, Data>();

		public static Data forButtonId(int buttonId) {
//			for (Data d : Data.values) {
//				if (d.getButtonId() == buttonId) {
//					return d;
//				}
//			}
//			return null;
			return buttonMap.get(buttonId);
		}

		public static Data forPouchId(int pouchId) {
//			for (Data d : Data.values) {
//				if (d.pouchId == pouchId) {
//					return d;
//				}
//			}
//			return null;
			return pouchMap.get(pouchId);
		}
		
		static {
			for (Data d : Data.values) {
				buttonMap.put(d.getButtonId(), d);
				pouchMap.put(d.pouchId, d);
			}
		}
		
	    public static void load() {
	    	/* empty */
	    }

		public static Data forScrollButtonId(int buttonId) {
			int index = (buttonId - 151055) / 3;
			if (buttonId >= 152000) {
				index = 67 + ((buttonId - 152000) / 3);
			}
			try {
				Data data = values[index];
				return data;
			} catch (Throwable t) {
			}
//			for (Data d : Data.values) {
//				if (d.getScrollButton() == buttonId - 22769) {
//					return d;
//				}
//			}
//			return null;
			return buttonMap.get(buttonId-22769);
		}

		private int buttonId, pouchId, scrollId, shardsRequired, levelRequired,
				receivedExp, scrollButton;

		private int[] requiredItems;

		private Data(int buttonId, int pouchId, int scrollId,
				int[] requiredItems, int shardsRequired, int levelRequired,
				int receivedExp) {
			this.buttonId = buttonId;
			this.pouchId = pouchId;
			this.scrollId = scrollId;
			this.requiredItems = requiredItems;
			this.shardsRequired = shardsRequired;
			this.levelRequired = levelRequired;
			this.receivedExp = receivedExp;
			setScr();
		}

		public int getButtonId() {
			return buttonId;
		}

		public int getLevelRequired() {
			return levelRequired;
		}

		public int getPouchId() {
			return pouchId;
		}

		public int getReceivedExp() {
			return receivedExp;
		}

		public int[] getRequiredItems() {
			return requiredItems;
		}

		public int getScrollButton() {
			return scrollButton;
		}

		public int getScrollId() {
			return scrollId;
		}

		public int getShardsRequired() {
			return shardsRequired;
		}

		private void setScr() {
			this.scrollButton = scroll_button;
			scroll_button += 9;
		}
	}

	public final static int GOLD_CHARM = 12158, GREEN_CHARM = 12159,
			CRIMSON_CHARM = 12160, ABYSSAL_CHARM = 12161,
			TALON_BEAST_CHARM = 12162, BLUE_CHARM = 12163,
			RAVAGER_CHARM = 12164, SHIFTER_CHARM = 12165, SPINNER_CHARM = 12166,
			TORCHER_CHARM = 12167, OBSIDIAN_CHARM = 12168;

	public static final int SHARD_ID = 18016;

	public static final int POUCH_ID = 12155;

	public static boolean handleButtons(Client player, int buttonId) {
		Data data = Data.forButtonId(buttonId);
		if (data != null) {
			handleCraft(data, player, buttonId, false);
			return true;
		}
		if (data == null) {
			data = Data.forScrollButtonId(buttonId);
		}
		if (data != null) {
			handleCraft(data, player, buttonId, true);
			return true;
		}
		return false;
	}

	private static final Animation MAKE_ANIM = new Animation(725);

	public static void handleCraft(final Data data, final Client player,
			final int buttonId, final boolean scroll) {
		if (!isAble(data, player, scroll)) {
			return;
		}

		player.getPA().closeAllWindows();
		player.startActionChecked(new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				boolean able = isAble(data, player, scroll);
				if (!able) {
					container.stop();
					return;
				}

				if (!scroll) {
					if (data == Data.SPIRIT_WOLF) {
						player.getAchievement().spiritWolfPouches();
					} else if (data == Data.STEEL_TITAN) {
						player.getAchievement().makeSteelTitanPouches();
					}

					player.getItems().deleteItemInOneSlot(POUCH_ID, 1);
					player.getItems().deleteItemInOneSlot(SHARD_ID, data.getShardsRequired());
					player.getItems().deleteItemInOneSlot(data.getRequiredItems()[0], 1);
					player.getItems().deleteItemInOneSlot(data.getRequiredItems()[1], 1);
					player.getPA().addSkillXP(data.getReceivedExp(), Skills.SUMMONING);
					player.getItems().addItem(data.getPouchId(), 1);
				} else {
					player.getItems().deleteItemInOneSlot(data.getPouchId(), 1);
					player.getItems().addItem(data.getScrollId(), 10);
					player.getPA().addSkillXP(data.getReceivedExp() / 10, Skills.SUMMONING);
				}
				player.startAnimation(MAKE_ANIM);
			}

			@Override
			public void stop() {
				player.resetAnimations();
			}
		}, 3, false);
	}

	private static boolean isAble(final Data data, final Client player, final boolean scroll) {
		if (player.getPlayerLevel()[RSConstants.SUMMONING] < data.getLevelRequired()) {
			player.sendMessage("You need a Summoning level of " + data.getLevelRequired() + " to create this item.");
			return false;
		}

		if (scroll) {
			if (!player.getItems().playerHasItem(data.getPouchId())) {
				player.sendMessage("You need a " + ItemConstants.getItemName(data.getPouchId()) + " to create scrolls.");
				return false;
			}

			return true;
		}

		if (!player.getItems().playerHasItem(POUCH_ID)) {
			player.sendMessage("You don't have a pouch to create this item.");
			return false;
		}

		if (!player.getItems().playerHasItem(SHARD_ID, data.getShardsRequired())) {
			player.sendMessage("You need at least " + data.getShardsRequired() + " shards to create this pouch.");
			return false;
		}

		if (!player.getItems().playerHasItem(data.getRequiredItems()[0])) {
			player.sendMessage("You need a " + ItemConstants.getItemName(data.getRequiredItems()[0]) + " to make this pouch.");
			return false;
		}

		if (!player.getItems().playerHasItem(data.getRequiredItems()[1])) {
			player.sendMessage("You need " + ItemConstants.getItemName(data.getRequiredItems()[1]) + " to create this pouch.");
			return false;
		}

		return true;
	}

}
