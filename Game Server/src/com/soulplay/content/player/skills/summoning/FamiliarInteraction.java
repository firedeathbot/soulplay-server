package com.soulplay.content.player.skills.summoning;

import com.soulplay.game.model.player.Client;

public class FamiliarInteraction {

	public static boolean interactWithFamiliar(Client p, int interactId) {
		if (p.getSummoning().summonedFamiliar == null) {
			return false;
		}

		final int npcId = p.getSummoning().summonedFamiliar.npcId;
		if (npcId != interactId) {
			return false;
		}

		switch (npcId - 1) {
			case 6829: // Spirit wolf
				p.getDialogueBuilder().sendNpcChat(npcId, "Woof!", "Woof, woof?", "Ummm...Wewf?", "Hi...I mean...Woof!")
						.execute();
				break;
	
			case 6826: // Dreadfowl
				p.getDialogueBuilder().sendNpcChat(npcId, "Bawk!", "Bawk?!", "*Chirp*", "BAWK!!").execute();
				break;
	
			case 6842: // Spirit spider
				p.getDialogueBuilder().sendNpcChat(npcId, "*Cricri*", "*Crunch*", "Please stop looking at me.").execute();
				break;
	
			case 6807: // Thorny snail-NOT
				p.getDialogueBuilder().sendNpcChat(npcId, "...", "???").execute();
				break;
	
			case 6797: // Granite crab-NOT
				p.getDialogueBuilder().sendNpcChat(npcId, "*Cruuunch, crunch*", "*Tssk!*", "I bet I can eat you whole.")
						.execute();
				break;
	
			case 7332: // Spirit mosquito
				p.getDialogueBuilder().sendNpcChat(npcId, "*Buzz*", "*Buzzzzzz!*", "I'm Buzzzy!").execute();
				break;
	
			case 6832: // Desert wyrm
				p.getDialogueBuilder().sendNpcChat(npcId, "*Ssssss*", "*Thhhhhssss*", "What are you *Ssss* looking at?")
						.execute();
				break;
	
			case 6838: // Spirit scorpion
				p.getDialogueBuilder().sendNpcChat(npcId, "*Tssk, tssk*", "*Ku, ku*").execute();
				break;
	
			case 7362: // Spirit tz-kih
				p.getDialogueBuilder().sendNpcChat(npcId, "*Ssssss...").execute();
				break;
	
			case 6848: // Albino rat
				p.getDialogueBuilder()
						.sendNpcChat(npcId, "Got any cheese?", "Your face is very *cheesy*, hahahaha", "*Nibble, nibble*")
						.execute();
				break;
	
			case 6995: // Spirit kalphite
				p.getDialogueBuilder().sendNpcChat(npcId, "Take me to my queen, now!", "I demand to talk to your leader.",
						"When my queen finds out about this world...").execute();
				break;
	
			case 6872: // Compost mound
				p.getDialogueBuilder().sendNpcChat(npcId, "*Brrragghh*", "*Hmmf*, *Hmmmmf*", "Gah!").execute();
				break;
	
			case 7354: // Giant chinchompa
				p.getDialogueBuilder().sendNpcChat(npcId, "*Sniffle, sniffle*",
						"I have an....*exploding*, personality! Mwuahaha.", "KABOOM!...Gotcha!").execute();
				break;
	
			case 6795: // Spirit terrorbird
				p.getDialogueBuilder().sendNpcChat(npcId, "Macaw!", "Caw, caw!",
						"Don't you get *tired* of using my ability all the time?").execute();
				break;
	
			case 6873: // Pack yak
				p.getDialogueBuilder().sendNpcChat(npcId, "Barruw", "Barrooo!", "Barroo").execute();
				break;
		}

		return true;
	}

}