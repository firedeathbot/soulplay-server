package com.soulplay.content.player.skills.summoning;

import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

/*
 * if (10 + Misc.random(c.getCombat().calculateRangeDefence()) >
 * Misc.random(NPCHandler.n.attack)) { damage = 0; } if (10 +
 * Misc.random(c.getCombat().mageDef()) > Misc.random(NPCHandler.n.attack)) {
 * damage = 0; magicFailed = true; } if (10 +
 * Misc.random(c.getCombat().calculateMeleeDefence()) >
 * Misc.random(NPCHandler.n.attack)) { damage = 0; }
 */

/**
 * Horrible code, needs redone
 */
@SuppressWarnings("all")
public class SummoningAttacking {

	private Player c;

	public SummoningAttacking(Player c) {
		this.c = c;
	}

	public void attackNpc(int i, int z) {
		if (i == z) {
			return;
		}
		final NPC n = NPCHandler.npcs[i];
		if (n == null) {
			return;
		}
		final Client o = (Client) PlayerHandler.players[n.spawnedBy];
		if (o == null) {
			return;
		}
		final NPC d = NPCHandler.npcs[z];
		if (d == null) {
			return;
		}
		d.underAttackBy = o.getId();
		d.setWalkingHome(false);
		int damage = n.isPokemon ? PokemonData.maxHit(c, n) : SummoningData.getSummonDamage(n.npcType, false);
		if (d.getSkills().getLifepoints() - damage < 0) {
			damage = d.getSkills().getLifepoints();
		}
		boolean usingSpecial = false;
		usingSpecial = false;
		if (!n.isPokemon && damage >= SummoningData.getSummonMaxHit(n.npcType)) {
			damage = SummoningData.getSummonMaxHit(n.npcType);
		}
		if (!o.usingSummoningSpecial) {
			c.getCombat().appendHit(d, damage, 0, 0, 1);
		} else {
			usingSpecial = true;
		}
		if (n.isUsingSummonigSpecial) {
			n.isUsingSummonigSpecial = false;
		}
		if (n.npcType != 7340 && usingSpecial == true
				&& NPCHandler.goodDistance(n.getX(), n.getY(), d.absX, d.absY,
						NPCHandler.distanceRequired(i))) {
			int dmg1 = 0;
			int dmg2 = 0;
			dmg1 = SummoningData.getSummonDamage(n.npcType, false);
			dmg2 = SummoningData.getSummonDamage(n.npcType, false);
			if (dmg1 >= 244) {
				dmg1 = 244;
			}
			if (dmg2 >= 244) {
				dmg2 = 244;
			}
			int random1 = 0;
			random1 = Misc.random(12 + 1);
			if (random1 <= 2) {
				dmg1 = 0;
			}
			int random2 = 0;
			random2 = Misc.random(12 + 1);
			if (random2 <= 2) {
				dmg2 = 0;
			}
			c.getCombat().appendHit(d, dmg1, 0, 0, 1);
			c.getCombat().appendHit(d, dmg2, 0, 0, 1);
			c.steelTitanTarget = c.getId();
			c.steelTitanDelay = 2;
			c.doneSteelTitanDelay = true;
			o.usingSummoningSpecial = false;
			usingSpecial = false;
		} else if (n.npcType != 7340 && usingSpecial == true
				&& !NPCHandler.goodDistance(n.getX(), n.getY(), d.absX, d.absY,
						NPCHandler.distanceRequired(i))) {
			int dmg1 = 0;
			int dmg2 = 0;
			dmg1 = SummoningData.getSummonDamage(n.npcType, false);
			dmg2 = SummoningData.getSummonDamage(n.npcType, false);
			if (dmg1 >= 244) {
				dmg1 = 244;
			}
			if (dmg2 >= 244) {
				dmg2 = 244;
			}
			int random1 = 0;
			random1 = Misc.random(12 + 1);
			if (random1 <= 2) {
				dmg1 = 0;
			}
			int random2 = 0;
			random2 = Misc.random(12 + 1);
			if (random2 <= 2) {
				dmg2 = 0;
			}
			c.getCombat().appendHit(d, dmg1, 0, 0, 1);
			c.getCombat().appendHit(d, dmg2, 0, 0, 1);
			c.steelTitanTarget = c.getId();
			c.steelTitanDelay = 2;
			c.doneSteelTitanDelay = true;
			o.usingSummoningSpecial = false;
			usingSpecial = false;
		}
	}

	public void attackPlayer(int i) {
		int damage = 0;
		final NPC n = NPCHandler.npcs[i];
		if (n == null) {
			return;
		}
		final Client sumNpcOwner = (Client) PlayerHandler.players[n.spawnedBy];
		if (sumNpcOwner == null) {
			return;
		}

		if (c.teleTimer > 0) {
			return;
		}
		
		c.underAttackBy = sumNpcOwner.getId();
		damage = SummoningData.getSummonDamage(n.npcType, false);
		if (c.getPlayerLevel()[3] - damage < 0) {
			damage = c.getPlayerLevel()[3];
		}
		boolean usingSpecial = false;
		usingSpecial = false;
		if (damage >= SummoningData.getSummonMaxHit(n.npcType)) {
			damage = SummoningData.getSummonMaxHit(n.npcType);
		}
		c.damageType = 0;
		if (n.npcType == 7344 && !NPCHandler.goodDistance(n.getX(), n.getY(),
				c.getCurrentLocation().getX(), c.getCurrentLocation().getY(), 3)) {
			c.damageType = 1;
		}	
		
		if (!sumNpcOwner.usingSummoningSpecial) {

			damage = checkPrayersDmg(sumNpcOwner, damage, c.damageType);

			c.getCombat().appendHit(c, damage, 0, c.damageType, false, 0);
		} else {
			usingSpecial = true;
		}
		if (n.npcType != 7340 && usingSpecial == true
				&& NPCHandler.goodDistance(n.getX(), n.getY(),
						c.getCurrentLocation().getX(), c.getCurrentLocation().getY(),
						NPCHandler.distanceRequired(i))) { // melee hit
			int dmg1 = 0;
			int dmg2 = 0;
			dmg1 = SummoningData.getSummonDamage(n.npcType, false);
			dmg2 = SummoningData.getSummonDamage(n.npcType, false);
			if (dmg1 >= 244) {
				dmg1 = 244;
			}
			if (dmg2 >= 244) {
				dmg2 = 244;
			}
			int random1 = 0;
			random1 = Misc.random(12 + 1);
			if (random1 <= 2) {
				dmg1 = 0;
			}
			int random2 = 0;
			random2 = Misc.random(12 + 1);
			if (random2 <= 2) {
				dmg2 = 0;
			}
			// dmg1 = checkPrayersDmg(sumNpcOwner, dmg1, 0);
			// dmg2 = checkPrayersDmg(sumNpcOwner, dmg2, 0);
			// c.getCombat().appendHit(c, dmg1, 0, 0, true, 0);
			// c.getCombat().appendHit(c, dmg2, 0, 0, true, 0);
			c.steelTitanTarget = c.getId();
			c.steelTitanDelay = 2;
			c.doneSteelTitanDelay = true;
			sumNpcOwner.usingSummoningSpecial = false;
			usingSpecial = false;
			createEvent(sumNpcOwner, n);
		} else if (n.npcType != 7340 && usingSpecial == true
				&& !NPCHandler.goodDistance(n.getX(), n.getY(),
						c.getCurrentLocation().getX(), c.getCurrentLocation().getY(),
						NPCHandler.distanceRequired(i))) { // range hit
			int dmg1 = 0;
			int dmg2 = 0;
			dmg1 = SummoningData.getSummonDamage(n.npcType, false);
			dmg2 = SummoningData.getSummonDamage(n.npcType, false);
			if (dmg1 >= 244) {
				dmg1 = 244;
			}
			if (dmg2 >= 244) {
				dmg2 = 244;
			}
			int random1 = 0;
			random1 = Misc.random(12 + 1);
			if (random1 <= 2) {
				dmg1 = 0;
			}
			int random2 = 0;
			random2 = Misc.random(12 + 1);
			if (random2 <= 2) {
				dmg2 = 0;
			}
			// dmg1 = checkPrayersDmg(sumNpcOwner, dmg1, 1);
			// dmg2 = checkPrayersDmg(sumNpcOwner, dmg2, 1);
			// c.getCombat().appendHit(c, dmg1, 1, 0, true, 0);
			// c.getCombat().appendHit(c, dmg2, 1, 0, true, 0);
			c.steelTitanTarget = c.getId();
			c.steelTitanDelay = 2;
			c.doneSteelTitanDelay = true;
			sumNpcOwner.usingSummoningSpecial = false;
			usingSpecial = false;
			createEvent(sumNpcOwner, n);
		}
		if (n.npcType == 7340 && usingSpecial == true) {
			sumNpcOwner.usingSummoningSpecial = false;
			c.geyserTitanTarget = c.getId();
			c.geyserTitanDelay = 2;
			usingSpecial = false;
			createEvent(sumNpcOwner, n);
			// c.getCombat().appendHit(c, dmg, 2, 0, true, 0);
		}
	}

	public int checkPrayersDmg(final Client sumNpcOwner, int damage,
			int hitType) {
		if (hitType == 0
				&& c.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]) {
			return damage / 2;
		}
		if (hitType == 1
				&& c.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]) {
			return damage / 2;
		}
		if (hitType == 2
				&& c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]) {
			return damage / 2;
		}

		if (damage > 0) {
			c.curses();
			if (hitType == 0 && c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
				if (damage > 0) {
					c.curses().deflect(sumNpcOwner, damage, 0);
				}
				return damage / 2;
			} else {
				sumNpcOwner.curses();
				if (hitType == 1 && c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
					if (damage > 0) {
						c.curses().deflect(sumNpcOwner, damage, 1);
					}
					return damage / 2;
				} else {
					sumNpcOwner.curses();
					if (hitType == 2
							&& c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
						if (damage > 0) {
							c.curses().deflect(sumNpcOwner, damage, 2);
						}
						return damage / 2;
					} else {
						c.curses();
						if (hitType == 5
								&& c.curseActive[Curses.DEFLECT_SUMMONING.ordinal()]) { // summoning
																					// special
																					// attack
																					// scroll
							if (damage > 0) {
								c.curses().deflect(sumNpcOwner, damage,
										c.damageType);
							}
							return damage / 2;
						}
					}
				}
			}
		}
		return damage;
	}

	public void createEvent(final Client sumNpcOwner, final NPC summoningNpc) {
		summoningNpc.isUsingSummonigSpecial = true;
		final int damageType = c.damageType;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (c.disconnected || c.teleTimer > 0) {
					container.stop();
					return;
				}
				if (c.steelTitanDelay != 0) {
					c.steelTitanDelay--;
				}
				if (c.steelTitanDelay == 0 && c.doneSteelTitanDelay == true) {
					// final Client d = ((Client)
					// PlayerHandler.players[c.steelTitanTarget]);
					int dmg3 = SummoningData.getSummonDamage(7344, false);
					dmg3 = SummoningData.getSummonDamage(7344, false);
					int dmg4 = SummoningData.getSummonDamage(7344, false);
					dmg4 = SummoningData.getSummonDamage(7344, false);
					if (dmg3 >= 244) {
						dmg3 = 244;
					}
					if (dmg4 >= 244) {
						dmg4 = 244;
					}
					int random1 = 0;
					random1 = Misc.random(12 + 1);
					if (random1 <= 2) {
						dmg3 = 0;
					}
					int random2 = 0;
					random2 = Misc.random(12 + 1);
					if (random2 <= 2) {
						dmg4 = 0;
					}
					dmg3 = checkPrayersDmg(sumNpcOwner, dmg3, damageType);
					dmg4 = checkPrayersDmg(sumNpcOwner, dmg4, damageType);
					c.getCombat().appendHit(c, dmg3, damageType, 0, false, 0);
					c.getCombat().appendHit(c, dmg4, damageType, 0, false, 0);
					c.doneSteelTitanDelay = false;
					container.stop();
				}
				if (c.geyserTitanDelay != 1 && c.geyserTitanDelay != 0) {
					c.geyserTitanDelay--;
				}
				if (c.geyserTitanDelay == 1) {
					c.startGraphic(Graphic.create(1375));
					int dmg = SummoningData.getSummonDamage(7340, true);
					if (dmg >= 300) {
						dmg = 300;
					}
					dmg = checkPrayersDmg(sumNpcOwner, dmg, 5);
					c.getCombat().appendHit(c, dmg, 2, 0, false, 0);
					c.geyserTitanDelay = 0;
					container.stop();
				}
			}

			@Override
			public void stop() {
				// TODO Auto-generated method stub
				if (summoningNpc != null) {
					summoningNpc.isUsingSummonigSpecial = false;
				}
			}
		}, 1);
	}

}
