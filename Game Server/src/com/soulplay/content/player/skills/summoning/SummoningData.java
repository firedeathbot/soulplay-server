package com.soulplay.content.player.skills.summoning;

import com.soulplay.util.Misc;

public class SummoningData {

	private static final int[][] SUMMONING_DATA = {{6795, 76, 2330},
			{6797, 40, 390}, {6799, 145, 4280}, {6801, 150, 4760},
			{6805, 140, 5280}, {6807, 40, 280}, {6810, 160, 2760},
			{6812, 157, 4900}, {6816, 100, 3480}, {6819, 100, 2480},
			{6823, 80, 1000}, {6828, 121, 3220}, {6830, 40, 150},
			{6832, 40, 470}, {6834, 70, 1620}, {6836, 30, 1050},
			{6838, 60, 670}, {6840, 130, 3810}, {6842, 30, 180},
			{6844, 90, 2110}, {6846, 50, 1100}, {6864, 160, 5700},
			{6866, 112, 3000}, {6868, 90, 1540}, {6870, 220, 6510},
			{6872, 50, 950}, {6874, 180, 7100}, {6876, 70, 1730},
			{6878, 70, 1730}, {6880, 70, 1730}, {6882, 70, 1730},
			{6884, 70, 1730}, {6886, 70, 1730}, {6888, 70, 1730},
			{6993, 100, 255}, {7330, 160, 5560}, {7332, 42, 430},
			{7334, 50, 5900}, {7336, 140, 4410}, {7338, 107, 2680},
			{7340, 215, 6200}, {7342, 140, 5280}, {7344, 244, 7540},
			{7346, 140, 4060}, {7348, 80, 4540}, {7350, 229, 6670},
			{7352, 60, 1210}, {7356, 152, 4760}, {7358, 150, 4760},
			{7360, 152, 4760}, {7362, 50, 630}, {7364, 100, 2680},
			{7366, 107, 2680}, {7368, 60, 1210}, {7373, 134, 3750},
			{7376, 230, 6940},};

	public static int getSummonDamage(int npcId, boolean moreDamage) {
		int maxHit = getSummonMaxHit(npcId);
		int damage = Misc.random(maxHit)
				+ (int) ((maxHit/* - 10 */) * (moreDamage ? 0.15 : 0.05));
		return damage;
	}

	public static int getSummonHealth(int id) {
		for (int[] i : SUMMONING_DATA) {
			if (id == i[0]) {
				return i[2] / 10;
			}
		}
		return 0;
	}

	public static int getSummonMaxHit(int id) {
		for (int[] i : SUMMONING_DATA) {
			if (id == i[0]) {
				return i[1] / 10;
			}
		}
		return 0;
	}

	public static boolean isSummonNpc(int id) {
		for (int[] i : SUMMONING_DATA) {
			if (id == i[0]) {
				return true;
			}
		}
		return false;
	}
}
