package com.soulplay.content.player.skills.summoning;

public enum SummonOrbButtons {

	CALL_FOLLOWER(0, "Call Follower", 3250),
	DISMISS(1, "Dismiss", 1019), // unused
	TAKE_BOB(2, "Take BoB", 3252),
	RENEW_FAMILIAR(3, "Renew Familiar", 3253),
	INTERACT(4, "Interact", 1022), // unused
	ATTACK(5, "Attack", 1023), // unused
	FOLLOWER_DETAILS(6, "Follower Details", 1024), // unused
	CAST(7, "Cast", 4002);
	
	public static final SummonOrbButtons[] values = SummonOrbButtons.values();

	private int buttonIndex;

	private String buttonName;

	private int actionButtonId;

	private SummonOrbButtons(int buttonIndex, String buttonName,
			int actionButtonId) {
		this.setButtonIndex(buttonIndex);
		this.setButtonName(buttonName);
		this.actionButtonId = actionButtonId;
	}

	public int getActionButtonId() {
		return actionButtonId;
	}

	public int getButtonIndex() {
		return buttonIndex;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setActionButtonId(int actionButtonId) {
		this.actionButtonId = actionButtonId;
	}

	public void setButtonIndex(int buttonIndex) {
		this.buttonIndex = buttonIndex;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
}
