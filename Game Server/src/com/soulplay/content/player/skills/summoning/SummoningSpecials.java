
package com.soulplay.content.player.skills.summoning;

import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@SuppressWarnings("all")
public class SummoningSpecials {

	private static int itemsDrop[] = {5972, 5982, 1963};

	private static int itemsDrop1[] = {249, 251, 253, 255, 257, 259, 261, 263,
			265, 267, 269};

	private static int itemsDrop2[] = {1623, 1621, 1619, 1617, 1623, 1621, 1619,
			1623, 1621, 1623, 1631};

	private static int itemsDrop3[] = {335, 359, 377};

	private static void abby2Spec(final int i, final Client c) {
		int invSlotCount = c.getItems().freeSlots();
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 20) {
				if (invSlotCount >= 4) {
					if (c.getItems().playerHasItem(12827, 1)) {
						c.getItems().deleteItemInOneSlot(12827,
								c.getItems().getItemSlot(12827), 1);
						c.getItems().addItem(7936, Misc.random(4));
						NPCHandler.npcs[i].startAnimation(7698);
						NPCHandler.npcs[i].startGraphic(Graphic.create(1457));
						c.familiarIndex += 6;
						c.summoningSpecialPoints -= 20;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						c.startAnimation(7660);
						c.startGraphic(Graphic.create(1308, GraphicType.LOW));
						c.getPA().addSkillXP(585, Skills.SUMMONING);
					} else {
						c.sendMessage(
								"You do not have the correct scroll for this familiar.");
					}
				} else {
					c.sendMessage(
							"You need to have at least 4 free inventory slots for the familiar to use it's special.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void abbySpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (c.getItems().playerHasItem(12454, 1)) {
					c.getItems().deleteItemInOneSlot(12454,
							c.getItems().getItemSlot(12454), 1);
					typicalStuff(c);
					c.summoningSpecialPoints -= 12;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					NPCHandler.npcs[i].startAnimation(7672);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1422));
					c.getPA().addSkillXP(75, Skills.SUMMONING);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void badgerBonus(Client c) {
		c.getPA().scorpSpecBonus(0, false);
		c.getPA().scorpSpecBonus(2, false);
		c.getPA().scorpSpecBonus(4, false);
		c.getPA().scorpSpecBonus(6, false);
	}

	private static void badgerSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 4) {
				if (c.getItems().playerHasItem(12433, 1)) {
					c.getItems().deleteItemInOneSlot(12433,
							c.getItems().getItemSlot(12433), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 4;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					NPCHandler.npcs[i].startAnimation(7930);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1397));
					c.startGraphic(Graphic.create(1399, GraphicType.LOW));
					badgerBonus(c);
					c.getPA().addSkillXP(28, Skills.SUMMONING);
					massiveRefresh(c);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void beaverSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 3) {
				if (c.getItems().playerHasItem(12429, 1)) {
					if (c.isWc) {
						c.getItems().deleteItemInOneSlot(12429,
								c.getItems().getItemSlot(12429), 1);
						c.summoningSpecialPoints -= 3;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						typicalStuff(c);
						NPCHandler.npcs[i].startAnimation(7722);
						c.getPA().addSkillXP(28, Skills.SUMMONING);
						// Server.npcHandler.npcs[i].gfx0(1397);
						int maybe = Misc.random(1);
						if (maybe == 1) {
							// Woodcutting.receiveBirdsNest(c);
							c.sendMessage(
									"The beaver found a birds nest for you."); // TODO:
																				// add
																				// birds
																				// nest
						}
					} else {
						c.sendMessage(
								"You must be woodcutting to use this special.");
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void bullAntSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (c.getItems().playerHasItem(12431, 1)) {
					c.getItems().deleteItemInOneSlot(12431,
							c.getItems().getItemSlot(12431), 1);
					c.summoningSpecialPoints -= 12;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1300, GraphicType.LOW));
					NPCHandler.npcs[c.familiarIndex].setAttackTimer(5);
					c.familiarIndex += 6;
					c.getMovement().restoreRunEnergy(c.getSkills().getStaticLevel(Skills.AGILITY) / 2);
					//c.getPacketSender().sendFrame126(c.getRunEnergyForSave() + "%", 149);
					NPCHandler.npcs[i].startAnimation(7895);
					c.getPA().addSkillXP(30, Skills.SUMMONING);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1382));
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	public static void castFamiliarSpecial(Client c) {
		switch (c.familiarID) {
			case 6817:
				fruitFall(c);
				break;

			case 6841:
				redSpiderSpec(c);
				break;

			case 6796:
				graniteCrabSpec(c.familiarIndex, c);
				break;

			case 6837:
				scorpionSpec(c.familiarIndex, c);
				break;

			case 6847:
				ratSpec(c.familiarIndex, c);
				break;

			case 6871:
				compostSpec(c.familiarIndex, c);
				break;

			case 6845:
				badgerSpec(c.familiarIndex, c);
				break;

			case 6808:
				beaverSpec(c.familiarIndex, c);
				break;

			case 7370:
			case 7367:
			case 7351:
			case 7333:
				voidSpec(c.familiarIndex, c);
				break;

			case 6867:
				bullAntSpec(c.familiarIndex, c);
				break;

			case 6851:
				herbSpec(c.familiarIndex, c);
				break;

			case 7377:
				pyreSpec(c.familiarIndex, c);
				break;

			case 6824:
				magpieSpec(c.familiarIndex, c);
				break;

			case 6843:
				leechSpec(c.familiarIndex, c);
				break;

			case 6794:
				terrorSpec(c.familiarIndex, c);
				break;

			case 6818:
				abbySpec(c.familiarIndex, c);
				break;

			case 6991:
				ibisSpec(c.familiarIndex, c);
				break;

			case 7363:
				graahkSpec(c.familiarIndex, c);
				break;

			case 6820:
				lurkerSpec(c.familiarIndex, c);
				break;

			case 6802:
				cobraSpec(c.familiarIndex, c);
				break;

			case 6815:
				tortSpec(c.familiarIndex, c);
				break;

			case 6813:
				startBunyipHealing(c);
				break;

			case 7345:
				golemSpec(c.familiarIndex, c);
				break;

			case 7359:
			case 7357:
			case 7355:
				threeTitanSpec(c.familiarIndex, c);
				break;

			case 6811:
				hydraSpec(c.familiarIndex, c);
				break;

			case 6822:
				unicornSpec(c.familiarIndex, c);
				break;

			case 6869:
				magicFocus(c.familiarIndex, c);
				break;

			case 7349:
				abby2Spec(c.familiarIndex, c);
				break;

		}
		if (c.summoningSpecialPoints >= 10) {
			c.getPacketSender()
					.sendFrame126("" + c.summoningSpecialPoints + "/60", 18024);
		} else {
			c.getPacketSender().sendFrame126(
					" " + c.summoningSpecialPoints + "/60", 18024);
		}
	}

	private static void cobraSpec(final int i, final Client c) {
		int invSlotCount = c.getItems().freeSlots();
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 3) {
				if (invSlotCount >= 4) {
					if (c.getItems().playerHasItem(12436, 1)) {
						c.getItems().deleteItemInOneSlot(12436,
								c.getItems().getItemSlot(12436), 1);
						c.getItems().addItem(12109, Misc.random(4));
						NPCHandler.npcs[i].startAnimation(8159);
						NPCHandler.npcs[i].startGraphic(Graphic.create(1388));
						c.familiarIndex += 6;
						c.summoningSpecialPoints -= 3;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						c.startAnimation(7660);
						c.startGraphic(Graphic.create(1308, GraphicType.LOW));
						c.getPA().addSkillXP(181, Skills.SUMMONING);
					} else {
						c.sendMessage(
								"You do not have the correct scroll for this familiar.");
					}
				} else {
					c.sendMessage(
							"You need to have at least 4 free inventory slots for the familiar to use it's special.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void compostSpec(int i, Client c) {
		int invSlotCount = c.getItems().freeSlots();
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (invSlotCount >= 3) {
					if (c.getItems().playerHasItem(12440, 1)) {
						c.getItems().deleteItemInOneSlot(12440,
								c.getItems().getItemSlot(12440), 1);
						c.getItems().addItem(6034, Misc.random(2) + 1);
						NPCHandler.npcs[i].startAnimation(7771);
						NPCHandler.npcs[i].startGraphic(Graphic.create(1425));
						typicalStuff(c);
						c.summoningSpecialPoints -= 12;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						c.getPA().addSkillXP(88, Skills.SUMMONING);
					} else {
						c.sendMessage(
								"You do not have the correct scroll for this familiar.");
					}
				} else {
					c.sendMessage(
							"You need to have at least 3 free inventory slots for the familiar to use it's special.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void fruitFall(Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (c.getItems().playerHasItem(12423, 1)) {
					typicalStuff(c);
					c.getItems().deleteItemInOneSlot(12423,
							c.getItems().getItemSlot(12423), 1);
					c.summoningSpecialPoints -= 6;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					NPCHandler.npcs[c.familiarIndex].startAnimation(8320);
					c.getPA().addSkillXP(49, 23);
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1331, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop(),
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								1);
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void golemSpec(final int i, final Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (c.getItems().playerHasItem(12826, 1)) {
					c.getItems().deleteItemInOneSlot(12826,
							c.getItems().getItemSlot(12826), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 12;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1303, GraphicType.LOW));
					NPCHandler.npcs[i].startAnimation(8053);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1465));
					c.getPA().crabSpecBonus(Skills.STRENGTH,
							true);
					c.getPA().addSkillXP(97, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void graahkSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 3) {
				if (c.getItems().playerHasItem(12835, 1)) {
					c.getItems().deleteItemInOneSlot(12835,
							c.getItems().getItemSlot(12835), 1);
					NPCHandler.npcs[c.familiarIndex].setAttackTimer(5);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 3;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					NPCHandler.npcs[i].startAnimation(7910);
					c.getPA().addSkillXP(196, 23);
					c.getPA().startTeleport(2818, 3012, 0, TeleportType.MODERN);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void graniteCrabSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (c.getItems().playerHasItem(12533, 1)) {
					c.getItems().deleteItemInOneSlot(12533,
							c.getItems().getItemSlot(12533), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 12;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1300, GraphicType.LOW));
					NPCHandler.npcs[i].startAnimation(8109);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1326));
					c.getPA().crabSpecBonus(1, false);
					c.getPA().addSkillXP(7, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void herbSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (c.getItems().playerHasItem(12422, 1)) {
					c.getItems().deleteItemInOneSlot(12422,
							c.getItems().getItemSlot(12422), 1);
					c.summoningSpecialPoints -= 12;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					typicalStuff(c);
					NPCHandler.npcs[c.familiarIndex].setAttackTimer(5);
					NPCHandler.npcs[i].startAnimation(8013);
					c.getPA().addSkillXP(79, 23);
					if (Misc.random(1) == 0) {
						c.getPA().stillGfx(1321, c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop1(),
								c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void hydraSpec(final int i, final Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (c.getItems().playerHasItem(12442, 1)) {
					c.getItems().deleteItemInOneSlot(12442,
							c.getItems().getItemSlot(12442), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 6;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1308, GraphicType.LOW));
					NPCHandler.npcs[i].startAnimation(7945);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1487));
					c.getPA().crabSpecBonus(Skills.WOODCUTTING,
							true);
					c.getPA().crabSpecBonus(Skills.FARMING,
							true);
					c.getPA().addSkillXP(274, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void ibisSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 12) {
				if (c.getItems().playerHasItem(12424, 1)) {
					typicalStuff(c);
					c.getItems().deleteItemInOneSlot(12424,
							c.getItems().getItemSlot(12424), 1);
					c.summoningSpecialPoints -= 12;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.getPA().addSkillXP(124, 23);
					NPCHandler.npcs[i].startAnimation(8201);
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1337, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, randomDrop3(),
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								1);
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static boolean isNonCombat(final Client c) {
		switch (c.familiarID) {
			case 6817:
			case 6841:
			case 6869:
			case 6796:
			case 6837:
			case 6847:
			case 6871:
			case 6845:
			case 6808:
			case 7370:
			case 7367:
			case 7349:
			case 7351:
			case 7333:
			case 6867:
			case 6851:
			case 7377:
			case 6824:
			case 6843:
			case 6794:
			case 6818:
			case 6991:
			case 7363:
			case 6820:
			case 6811:
			case 6802:
			case 6815:
			case 6813:
			case 7345:
			case 7359:
			case 7357:
			case 7355:
			case 6822:
				return true;
		}
		return false;
	}

	private static void leechSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (c.getItems().playerHasItem(12444, 1)) {
					c.getItems().deleteItemInOneSlot(12444,
							c.getItems().getItemSlot(12444), 1);
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1416, GraphicType.LOW));
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 6;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					int adding = Misc.random(7);
					c.getSkills().heal(adding);
					NPCHandler.npcs[i].startAnimation(7712);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1418));
					c.getPA().addSkillXP(75, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void lurkerSpec(final int i, final Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 3) {
				if (c.getItems().playerHasItem(12427, 1)) {
					c.getItems().deleteItemInOneSlot(12427,
							c.getItems().getItemSlot(12427), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 3;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1307, GraphicType.LOW));
					NPCHandler.npcs[i].startAnimation(7682);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1339));
					c.getPA().crabSpecBonus(Skills.AGILITY,
							false);
					c.getPA().crabSpecBonus(Skills.THIEVING,
							false);
					c.getPA().addSkillXP(134, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void magicFocus(final int i, final Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 20) {
				if (c.getItems().playerHasItem(12437, 1)) {
					c.getItems().deleteItemInOneSlot(12437,
							c.getItems().getItemSlot(12437), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 20;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1296, GraphicType.LOW));
					NPCHandler.npcs[i].startAnimation(8308);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1464));
					c.getSkills().setLevel(Skills.MAGIC, c.getSkills().getStaticLevel(Skills.MAGIC) + 7);
					c.getPA().addSkillXP(417, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void magpieSpec(int i, Client c) {
		int invSlotCount = c.getItems().freeSlots();
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 3) {
				if (c.getItems().playerHasItem(12426, 1)) {
					if (invSlotCount > 1) {
						c.getItems().deleteItemInOneSlot(12426,
								c.getItems().getItemSlot(12426), 1);
						c.getItems().addItem(randomDrop2(), 1);
						c.startAnimation(7660);
						c.startGraphic(Graphic.create(1300, GraphicType.LOW));
						c.familiarIndex += 6;
						c.summoningSpecialPoints -= 3;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						NPCHandler.npcs[i].startAnimation(8009);
						NPCHandler.npcs[i].startGraphic(Graphic.create(1336));
						c.getPA().addSkillXP(177, 23);
					} else {
						c.sendMessage(
								"You must have at least 1 free space in your inventory");
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void massiveRefresh(Client c) {
		c.refreshSkill(0);
		c.refreshSkill(1);
		c.refreshSkill(2);
		c.refreshSkill(4);
		c.refreshSkill(6);
	}

	private static void pyreSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (c.getItems().playerHasItem(12829, 1)) {
					if (c.getItems().playerHasItem(444, 1)) {
						c.getItems().deleteItemInOneSlot(12829,
								c.getItems().getItemSlot(12829), 1);
						c.getItems().deleteItemInOneSlot(444,
								c.getItems().getItemSlot(444), 1);
						c.getItems().addItem(2357, 1);
						c.startAnimation(7660);
						c.startGraphic(Graphic.create(1463, GraphicType.LOW));
						c.familiarIndex += 6;
						c.summoningSpecialPoints -= 6;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						NPCHandler.npcs[i].startAnimation(8082);
						c.getPA().addSkillXP(75, 23);
					} else {
						c.sendMessage(
								"You must have gold ore to be able to use this special");
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	static int randomDrop() {
		return itemsDrop[(int) (Math.random() * itemsDrop.length)];
	}

	static int randomDrop1() {
		return itemsDrop1[(int) (Math.random() * itemsDrop1.length)];
	}

	static int randomDrop2() {
		return itemsDrop2[(int) (Math.random() * itemsDrop2.length)];
	}

	static int randomDrop3() {
		return itemsDrop3[(int) (Math.random() * itemsDrop3.length)];
	}

	private static void ratSpec(int i, Client c) {
		int invSlotCount = c.getItems().freeSlots();
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (invSlotCount >= 4) {
					if (c.getItems().playerHasItem(12430, 1)) {
						c.getItems().deleteItemInOneSlot(12430,
								c.getItems().getItemSlot(12430), 1);
						c.getItems().addItem(1985, Misc.random(4));
						NPCHandler.npcs[i].startAnimation(7907);
						NPCHandler.npcs[i].startGraphic(Graphic.create(1384));
						typicalStuff(c);
						c.summoningSpecialPoints -= 6;
						if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
							c.summoningSpecialPoints += 2;
						}
						c.getPA().addSkillXP(81, 23);
						c.refreshSkill(23);
					} else {
						c.sendMessage(
								"You do not have the correct scroll for this familiar.");
					}
				} else {
					c.sendMessage(
							"You need to have at least 4 free inventory slots for the familiar to use it's special.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void redSpiderSpec(Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (c.getItems().playerHasItem(12428, 1)) {
					typicalStuff(c);
					c.getItems().deleteItemInOneSlot(12428,
							c.getItems().getItemSlot(12428), 1);
					c.summoningSpecialPoints -= 6;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					NPCHandler.npcs[c.familiarIndex].startAnimation(8164);
					c.getPA().addSkillXP(7, 23);
					c.refreshSkill(23);
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY(), c.getHeightLevel(), 1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX(),
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX() + 1,
								c.getCurrentLocation().getY() - 1, c.getHeightLevel(),
								1);
					}
					if (Misc.random(2) == 0) {
						c.getPA().stillGfx(1342, c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								3);
						ItemHandler.createGroundItem(c, 223,
								c.getCurrentLocation().getX() - 1,
								c.getCurrentLocation().getY() + 1, c.getHeightLevel(),
								1);
					}
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void scorpionSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 6) {
				if (c.getItems().playerHasItem(12432, 1)) {
					c.getItems().deleteItemInOneSlot(12432,
							c.getItems().getItemSlot(12432), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 6;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1300, GraphicType.LOW));
					c.getPA().scorpSpecBonus(0, false);
					c.getPA().addSkillXP(7, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void startBunyipHealing(final Client c) {
		if (!c.getItems().playerHasItem(12438, 1)) {
			c.sendMessage(
					"You do not have the correct scroll for this familiar.");
			return;
		}
		if (c.summoningSpecialPoints < 20) {
			c.sendMessage(
					"You do not have enough special points to cast this special.");
			return;
		}
		if (c.hasActivated) {
			c.sendMessage(
					"You have already activated this special, you must wait until it deactivates.");
			return;
		}
		typicalStuff(c);
		c.summoningSpecialPoints -= 20;
		if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
			c.summoningSpecialPoints += 2;
		}
		c.sendMessage("The bunyip will slowly heal you for awhile now.");
		c.getItems().deleteItem2(12438, 1);
		c.hasActivated = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int time = 15;

			int count = 0;

			@Override
			public void execute(CycleEventContainer e) {
				if (c == null) {
					e.stop();
					return;
				}
				if (c.familiarID != 6813) {
					e.stop();
				}
				if (count == 25) {
					e.stop();
				}
				if (time > 0) {
					time--;
				}

				if (time == 0) {
					c.getPlayerLevel()[Skills.HITPOINTS] += 20;
					if (c.getPlayerLevel()[Skills.HITPOINTS] >= c.calculateMaxLifePoints()) {
						c.getPlayerLevel()[Skills.HITPOINTS] = c.calculateMaxLifePoints();
					}
					c.startGraphic(Graphic.create(1507, GraphicType.LOW));
					c.refreshSkill(Skills.HITPOINTS);
					time = 15;
					count++;

				}
			}

			@Override
			public void stop() {
				if (c != null) {
					c.sendMessage("The effect of the bunyip wore off.");
					c.hasActivated = false;
				}
			}
		}, 1);
	}

	private static void terrorSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 8) {
				if (c.getItems().playerHasItem(12441, 1)) {
					c.getItems().deleteItemInOneSlot(12441,
							c.getItems().getItemSlot(12441), 1);
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1300, GraphicType.LOW));
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 8;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.getSkills().updateLevel(Skills.AGILITY, 2);
					c.getMovement().restoreRunEnergy(c.getSkills().getStaticLevel(Skills.AGILITY) / 2);
					c.getPacketSender().sendRunEnergy();
					NPCHandler.npcs[i].startAnimation(8229);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1521));
					c.getPA().addSkillXP(75, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void threeTitanSpec(final int i, final Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 20) {
				if (c.getItems().playerHasItem(12824, 1)) {
					c.getItems().deleteItemInOneSlot(12824,
							c.getItems().getItemSlot(12824), 1);
					c.familiarIndex += 6;
					switch (c.familiarID) {
						case 7359:
							NPCHandler.npcs[i].startAnimation(7837);
							NPCHandler.npcs[i].startGraphic(Graphic.create(1512));
							break;
						case 7357:
							NPCHandler.npcs[i].startAnimation(7837);
							NPCHandler.npcs[i].startGraphic(Graphic.create(1513));
							break;
						case 7355:
							NPCHandler.npcs[i].startAnimation(7835);
							NPCHandler.npcs[i].startGraphic(Graphic.create(1514));
							break;
					}
					c.summoningSpecialPoints -= 20;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1313, GraphicType.LOW));
					c.getSkills().heal(8);
					c.getPA().crabSpecBonus(1, true);
					c.getPA().addSkillXP(497, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void tortSpec(final int i, final Client c) {
		final NPC npc = NPCHandler.npcs[i];
		int famX = NPCHandler.npcs[i].absX;
		int famY = NPCHandler.npcs[i].absY;
		int pX = PlayerHandler.players[c.getId()].getCurrentLocation().getX();
		int pY = PlayerHandler.players[c.getId()].getCurrentLocation().getY();
		int poffX = (famX - pX) * -1;
		int poffY = (famX - pY) * -1;
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 20) {
				if (c.getItems().playerHasItem(12439, 1)) {
					c.getItems().deleteItemInOneSlot(12439,
							c.getItems().getItemSlot(12439), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 20;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1302, GraphicType.LOW));

					Projectile proj = new Projectile(1415,
							Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()),
							c.getCurrentLocation());
					proj.setStartHeight(41);
					proj.setEndHeight(28);
					proj.setStartDelay(5);
					proj.setEndDelay(70);
					proj.setLockon(c.getProjectileLockon());
					GlobalPackets.createProjectile(proj);
					
					NPCHandler.npcs[i].startAnimation(8288);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1414));
					c.getPA().crabSpecBonus(Skills.DEFENSE,
							true);
					c.getPA().addSkillXP(234, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	public static void typicalStuff(Client c) { // typicalStuff(c);
		c.startAnimation(7660);
		c.startGraphic(Graphic.create(1316, GraphicType.LOW));
		NPCHandler.npcs[c.familiarIndex].setAttackTimer(5);
		c.familiarIndex += 6;
	}

	private static void unicornSpec(final int i, final Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 20) {
				if (c.getItems().playerHasItem(12434, 1)) {
					if (c.getSkills().getLifepoints() > c.getSkills().getMaximumLifepoints()) {
						c.sendMessage("You cannot heal any higher at this moment.");
						return;
					}
					c.getItems().deleteItemInOneSlot(12434,
							c.getItems().getItemSlot(12434), 1);
					c.familiarIndex += 6;
					c.summoningSpecialPoints -= 20;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.startAnimation(7660);
					c.startGraphic(Graphic.create(1298, GraphicType.LOW));
					c.getPlayerLevel()[3] *= 1.15;
					c.refreshSkill(3);
					NPCHandler.npcs[i].startAnimation(8267);
					NPCHandler.npcs[i].startGraphic(Graphic.create(1356));
					c.getPA().addSkillXP(56, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	private static void voidSpec(int i, Client c) {
		if (c.familiarIndex > 0) {
			return;
		} else {
			if (c.summoningSpecialPoints >= 3) {
				if (c.getItems().playerHasItem(12443, 1)) {
					if (c.inDuelArena()) {
						c.sendMessage("You can't teleport during a duel!");
						return;
					}
					if (c.inWild()
							&& c.wildLevel > 20/*
												 * Config.NO_TELEPORT_WILD_LEVEL
												 */) {
						c.sendMessage("You can't teleport above level "
								+ /* Constants.NO_TELEPORT_WILD_LEVEL */20
								+ " in the wilderness.");
						return;
					}
					if (c.isTeleBlocked()) {
						c.sendMessage(
								"You are teleblocked and can't teleport.");
						return;
					}
					c.familiarIndex += 6;
					c.getPA().startTeleport(2662, 2650, 0, TeleportType.MODERN);
					c.getItems().deleteItemInOneSlot(12443,
							c.getItems().getItemSlot(12443), 1);
					c.summoningSpecialPoints -= 3;
					if (c.playerEquipment[PlayerConstants.playerCape] == 19893) {
						c.summoningSpecialPoints += 2;
					}
					c.getPA().addSkillXP(28, 23);
				} else {
					c.sendMessage(
							"You do not have the correct scroll for this familiar.");
				}
			} else {
				c.sendMessage(
						"You do not have enough special points to cast this special.");
			}
		}
	}

	public int damage = 0, damage2 = -1, endGfx = -1, timer = 0, drain = -1,
			hitType = 0;

	public boolean doubleHit = false;

	private Client p;

	public SummoningSpecials(Client Client) {
		this.p = Client;
	}

//	public void castFamiliarCombatSpecial(final Client c, final NPC n) {
//		if (c.familiarIndex < 1) {
//			return;
//		}
//		if (n.isUsingSummonigSpecial) {
//			return;
//		}
//		if (c.npcIndex > 0 && !isNonCombat(c)) {
//			if (c.inMulti()) {
//				specialEffectNPC(c.familiarIndex,
//						NPCHandler.npcs[c.familiarIndex].npcType, n);
//				if (c.summoningSpecialPoints >= 10) {
//					c.getPacketSender().sendFrame126(
//							"" + c.summoningSpecialPoints + "/60", 18024);
//				} else {
//					c.getPacketSender().sendFrame126(
//							" " + c.summoningSpecialPoints + "/60", 18024);
//				}
//			} else {
//				c.sendMessage(
//						"You must be in a multiple attack area to use this special.");
//			}
//		} else if (c.playerIndex > 0 && !isNonCombat(c)) {
//			if (c.inMulti()) {
//				specialEffect(c.familiarIndex,
//						NPCHandler.npcs[c.familiarIndex].npcType, n);
//				if (c.summoningSpecialPoints >= 10) {
//					c.getPacketSender().sendFrame126(
//							"" + c.summoningSpecialPoints + "/60", 18024);
//				} else {
//					c.getPacketSender().sendFrame126(
//							" " + c.summoningSpecialPoints + "/60", 18024);
//				}
//			} else {
//				c.sendMessage(
//						"You must be in a multiple attack area to use this special.");
//			}
//		}
//	}

//	public void specialEffect(int i, int npc, NPC n) {
//		int famX = NPCHandler.npcs[i].absX;
//		int famY = NPCHandler.npcs[i].absY;
//		int pX = PlayerHandler.players[p.playerIndex].getLocation().getX();
//		int pY = PlayerHandler.players[p.playerIndex].getLocation().getY();
//		int poffX = (famX - pX) * -1;
//		int poffY = (famX - pY) * -1;
//		endGfx = -1;
//		drain = -1;
//		switch (npc) {
//			case 7337:
//				// Larupri
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12840, 1)) {
//							p.getItems().deleteItem2(12840, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1371, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(5228);
//							n.gfx0(1369);
//							drain = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							hitType = 2;
//							damage = Misc.random(10);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(200, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6829:
//				// Spirit Wolf
//			case 6830:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12425, 1)) {
//							p.getItems().deleteItem2(12425, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1333, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8293);
//							// n.gfx0(1369);
//							damage = Misc.random(2);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(4, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//			case 6825:
//				// DreadFowl
//			case 6826:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12445, 1)) {
//							p.getItems().deleteItem2(12445, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1318, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7810);
//							n.gfx0(1523);
//							damage = Misc.random(3);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(4, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6806:
//				// Thorny Snail
//			case 6807:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12459, 1)) {
//							p.getItems().deleteItem2(12459, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1386, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8148);
//							n.gfx0(1385);
//							damage = Misc.random(8);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(7, 23);
//							p.refreshSkill(23);
//							endGfx = 1387;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7331:
//				// Mosquito
//			case 7332:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12838, 1)) {
//							p.getItems().deleteItem2(12838, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// poffX,
//							// poffY, 50, 70, 1386, 41, 28, c.playerIndex + 1,
//							// 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(8039);
//							n.gfx0(1441);
//							damage = 2;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(18, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6831:
//				// Desert Wyrm
//			case 6832:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12460, 1)) {
//							p.getItems().deleteItem2(12460, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1411, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7795);
//							n.gfx0(1410);
//							damage = Misc.random(5);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(14, 23);
//							p.refreshSkill(23);
//							endGfx = 1413;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7361:
//				// TzKih
//			case 7362:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12839, 1)) {
//							p.getItems().deleteItem2(12839, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// poffX,
//							// poffY, 50, 70, 1411, 41, 28, c.playerIndex + 1,
//							// 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(8257);
//							// n.gfx0(1410);
//							damage = Misc.random(7);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(39, 23);
//							p.refreshSkill(23);
//							endGfx = 1329;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6994:
//				// Kalphite
//			case 6995:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12446, 1)) {
//							p.getItems().deleteItem2(12446, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1349, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8517);
//							// n.gfx0(1410);
//							damage = Misc.random(5);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(81, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6804:
//				// Spirit Dag
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12456, 1)) {
//							p.getItems().deleteItem2(12456, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1426, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7787);
//							// n.gfx0(1410);
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							damage = Misc.random(18);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(281, 23);
//							p.refreshSkill(23);
//							endGfx = 1428;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//			case 7353:
//				// chinchompa
//			case 7354:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12834, 1)) {
//							p.getItems().deleteItem2(12834, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// poffX,
//							// poffY, 50, 70, 1349, 41, 28, c.playerIndex + 1,
//							// 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(7758);
//							n.gfx0(1364);
//							damage = Misc.random(12);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(104, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//			case 6835:
//				// vampire bat
//			case 6836:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 4) {
//						if (p.getItems().playerHasItem(12447, 1)) {
//							p.getItems().deleteItem2(12447, 1);
//							p.summoningSpecialPoints -= 4;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// poffX,
//							// poffY, 50, 70, 1497, 41, 28, c.playerIndex + 1,
//							// 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(8277);
//							n.gfx0(1467);
//							damage = Misc.random(12);
//							p.playerLevel[3] += Misc.random(2);
//							if (p.playerLevel[3] > PlayerConstants
//									.getLevelForXP(p.playerXP[3])) {
//								p.playerLevel[3] = PlayerConstants
//										.getLevelForXP(p.playerXP[3]);
//							}
//							p.specHitTimer = 1;
//							p.getPA().addSkillXP(56, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6853:
//				// bronze minot
//			case 6854:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12461, 1)) {
//							p.getItems().deleteItem2(12461, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1497, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(4);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(126, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 8575:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 5) {
//						if (p.getItems().playerHasItem(14622, 1)) {
//							p.getItems().deleteItem2(14622, 1);
//							p.summoningSpecialPoints -= 5;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1330, 41, 28,
//									p.playerIndex, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(11093);
//							damage = Misc.random(14);
//							p.specHitTimer = 3;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6833:
//				// evil turnip
//			case 6834:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12448, 1)) {
//							p.getItems().deleteItem2(12448, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1330, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8251);
//							damage = Misc.random(6);
//							p.playerLevel[4] += damage / 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							if (p.playerLevel[4] > p.getPA()
//									.getLevelForXP(p.playerXP[4]) + 5) {
//								p.playerLevel[4] = p.getPA()
//										.getLevelForXP(p.playerXP[4]) + 5;
//							}
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(74, 23);
//							p.refreshSkill(23);
//							p.getPA()
//									.refreshSkill(PlayerConstants.playerRanged);
//							if (damage == 0) {
//								endGfx = 85;
//							} else {
//								endGfx = 1329;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6889:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12452, 1)) {
//							p.getItems().deleteItem2(12452, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// offX,
//							// offY, 50, 70, 1508, 41, 28, c.npcIndex + 1, 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(7705);
//							n.gfx0(1403);
//							damage = Misc.random(18);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(45, 23);
//							p.refreshSkill(23);
//							endGfx = 1404;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6855:
//				// iron minot
//			case 6856:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12462, 1)) {
//							p.getItems().deleteItem2(12462, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1497, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(8);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(161, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6992:
//				// spirit Jelly
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12453, 1)) {
//							p.getItems().deleteItem2(12453, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1359, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8575);
//							damage = Misc.random(8);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(193, 23);
//							p.refreshSkill(23);
//							endGfx = 1360;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6857:
//				// steel minot
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12463, 1)) {
//							p.getItems().deleteItem2(12463, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1497, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(9);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(195, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6863:
//				// rune minot
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12466, 1)) {
//							p.getItems().deleteItem2(12466, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1497, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(20);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(395, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6861:
//				// addy minot
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12465, 1)) {
//							p.getItems().deleteItem2(12465, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1497, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(16);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(349, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7339:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12833, 1)) {
//							p.getItems().deleteItem2(12833, 1);
//							p.summoningSpecialPoints -= 6;
//							doubleHit = true;
//							typicalStuff(p);
//							damage = Misc.random(15);
//							damage2 = Misc.random(15);
//							p.getPA().createPlayersProjectile2(famX + 1,
//									famY + 1, poffX, poffY, 50, 70, 1376, 50,
//									14, p.playerIndex + 1, 5, 30,
//									p.getHeightLevel());
//							PlayerConstants.createPlayersProjectile(famX + 1,
//									famY + 1, poffX, poffY, 50, 70, 1376, 50,
//									14, p.playerIndex + 1, 20,
//									p.getHeightLevel());
//							Projectile proj = new Projectile(1376, Location.create(famX + 1, famY + 1, z), o.getLocation());
//							proj.setAngle(50);
//							proj.setStartHeight(43);
//							proj.setEndHeight(31);
//							proj.setTime(c.getCombat().getStartDelay(dist));
//							proj.setSpeed(c.getCombat().getProjectileSpeed(dist));
//							proj.setLockon(o.getProjectileLockon());
//							proj.setSlope(10);
//							GlobalPackets.createProjectile(proj);
//							NPCHandler.npcs[i].startAnimation(7883);
//							NPCHandler.npcs[i].gfx100(1375);
//							endGfx = 1377;
//							p.specHitTimer = 4;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6875:
//				// the cocks
//			case 6877:
//			case 6879:
//			case 6881:
//			case 6883:
//			case 6885:
//			case 6887:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12458, 1)) {
//							p.getItems().deleteItem2(12458, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1468, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7766);
//							n.gfx0(1467);
//							damage = Misc.random(10);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(33, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							} else {
//								endGfx = 1469;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7335:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12841, 1)) {
//							p.getItems().deleteItem2(12841, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7871);
//							n.gfx0(1394);
//							damage = Misc.random(20);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(181, 23);
//							p.refreshSkill(23);
//							endGfx = 1395;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7365:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12836, 1)) {
//							p.getItems().deleteItem2(12836, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7914);
//							n.gfx0(1369);
//							damage = Misc.random(11) * 3;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(199, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7343:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 12) {
//						if (p.getItems().playerHasItem(12825, 1)) {
//							p.getItems().deleteItem2(12825, 1);
//							p.summoningSpecialPoints -= 12;
//							NPCHandler.npcs[i].spec = true;
//							NPCHandler.npcs[i].extraHit = true;
//							doubleHit = true;
//							typicalStuff(p);
//							NPCHandler.npcs[i].startAnimation(8190);
//							NPCHandler.npcs[i].gfx100(1449);
//							p.specHitTimer = 3;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6809:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12455, 1)) {
//							p.getItems().deleteItem2(12455, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1479, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7974);
//							n.gfx0(1478);
//							damage = Misc.random(16);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(206, 23);
//							p.refreshSkill(23);
//							endGfx = 1480;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6865:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12468, 1)) {
//							p.getItems().deleteItem2(12468, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7820);
//							n.gfx0(1470);
//							damage = Misc.random(6);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(223, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6827:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12467, 1)) {
//							p.getItems().deleteItem2(12467, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1508, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8214);
//							damage = Misc.random(13);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(245, 23);
//							p.refreshSkill(23);
//							endGfx = 1511;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6859:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12464, 1)) {
//							p.getItems().deleteItem2(12464, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 4;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1497, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(13);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(300, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7372:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 12) {
//						if (p.getItems().playerHasItem(12830, 1)) {
//							p.getItems().deleteItem2(12830, 1);
//							p.summoningSpecialPoints -= 12;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1347, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7995);
//							NPCHandler.npcs[i].gfx0(1346);
//							damage = Misc.random(7);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(182, 23);
//							p.refreshSkill(23);
//							endGfx = 1348;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6839:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12451, 1)) {
//							p.getItems().deleteItem2(12451, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].gfx100(1405);
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1406, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(4921);
//							damage = Misc.random(15);
//							p.specHitTimer = 3;
//							if (damage == 0) {
//								endGfx = 85;
//							} else {
//								endGfx = 1407;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6849:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12449, 1)) {
//							p.getItems().deleteItem2(12449, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1352, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8118);
//							NPCHandler.npcs[i].gfx0(1353);
//							damage = Misc.random(14);
//							damage2 = Misc.random(14);
//							doubleHit = true;
//							p.specHitTimer = 3;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6798:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12450, 1)) {
//							p.getItems().deleteItem2(12450, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(8071);
//							NPCHandler.npcs[i].gfx0(1353);
//							damage = Misc.random(17);
//							p.specHitTimer = 3;
//							endGfx = 1353;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7341:
//				// lava titan
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 4) {
//						if (p.getItems().playerHasItem(12837, 1)) {
//							p.getItems().deleteItem2(12837, 1);
//							p.summoningSpecialPoints -= 4;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1493, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7986);
//							n.gfx0(1492);
//							damage = Misc.random(20);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(241, 23);
//							p.refreshSkill(23);
//							endGfx = 1494;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7329:
//				// swamp titan
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12832, 1)) {
//							p.getItems().deleteItem2(12832, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1462, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8223);
//							damage = Misc.random(21);
//							p.getPA().crabSpecBonus(
//									PlayerConstants.playerHerblore, false);
//							p.refreshSkill(
//									PlayerConstants.playerHerblore);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(241, 23);
//							p.refreshSkill(23);
//							endGfx = 1460;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7347:
//				// talon beast
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12831, 1)) {
//							p.getItems().deleteItem2(12831, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1520, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8043);
//							n.gfx0(1519);
//							damage = Misc.random(8);
//							damage2 = Misc.random(8);
//							doubleHit = true;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(395, 23);
//							p.refreshSkill(23);
//							if (damage == 0 || damage2 == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6800:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12457, 1)) {
//							p.getItems().deleteItem2(12457, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									poffX, poffY, 50, 70, 1362, 41, 28,
//									p.playerIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7852);
//							damage = Misc.random(19);
//							p.specHitTimer = 3;
//							endGfx = 1363;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//		}
//	}

//	public void specialEffectNPC(final int i, final int npc, final NPC n) {
//		int famX = NPCHandler.npcs[i].absX;
//		int famY = NPCHandler.npcs[i].absY;
//		int npcX = NPCHandler.npcs[p.npcIndex].absX;
//		int npcY = NPCHandler.npcs[p.npcIndex].absY;
//		int offX = (famX - npcX) * -1;
//		int offY = (famY - npcY) * -1;
//		endGfx = -1;
//		drain = -1;
//		hitType = 0;
//		doubleHit = false;
//		damage2 = -1;
//		switch (p.familiarID) {
//			case 7337:
//				// NPC Larupri
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12840, 1)) {
//							p.getItems().deleteItem2(12840, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1371, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(5228);
//							n.gfx0(1369);
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							damage = Misc.random(10);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(200, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7339:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12833, 1)) {
//							p.getItems().deleteItem2(12833, 1);
//							p.summoningSpecialPoints -= 6;
//							doubleHit = true;
//							typicalStuff(p);
//							damage = Misc.random(15);
//							damage2 = Misc.random(15);
//							p.getPA().createPlayersProjectile2(famX + 1,
//									famY + 1, offX, offY, 50, 70, 1376, 50, 14,
//									p.npcIndex + 1, 5, 30, p.getHeightLevel());
//							PlayerConstants.createPlayersProjectile(famX + 1,
//									famY + 1, offX, offY, 50, 70, 1376, 50, 14,
//									p.npcIndex + 1, 20, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7883);
//							NPCHandler.npcs[i].gfx100(1375);
//							endGfx = 1377;
//							p.specHitTimer = 4;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7343:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 12) {
//						if (p.getItems().playerHasItem(12825, 1)) {
//							p.getItems().deleteItem2(12825, 1);
//							p.summoningSpecialPoints -= 12;
//							NPCHandler.npcs[i].spec = true;
//							NPCHandler.npcs[i].extraHit = true;
//							doubleHit = true;
//							typicalStuff(p);
//							NPCHandler.npcs[i].startAnimation(8190);
//							NPCHandler.npcs[i].gfx100(1449);
//							p.specHitTimer = 3;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6829:
//				// NPC Spirit Wolf
//			case 6830:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12425, 1)) {
//							p.getItems().deleteItem2(12425, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							hitType = 1;
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1333, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8293);
//							// n.gfx0(1369);
//							damage = Misc.random(2);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(4, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//			case 6825:
//				// DreadFowl
//			case 6826:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12445, 1)) {
//							p.getItems().deleteItem2(12445, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							hitType = 2;
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1318, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7810);
//							n.gfx0(1523);
//							damage = Misc.random(3);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(4, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6806:
//				// Thorny Snail
//			case 6807:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12459, 1)) {
//							p.getItems().deleteItem2(12459, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1386, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8148);
//							n.gfx0(1385);
//							damage = Misc.random(8);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(7, 23);
//							p.refreshSkill(23);
//							endGfx = 1387;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7331:
//				// Mosquito
//			case 7332:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12838, 1)) {
//							p.getItems().deleteItem2(12838, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// poffX,
//							// poffY, 50, 70, 1386, 41, 28, c.playerIndex + 1,
//							// 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(8039);
//							n.gfx0(1441);
//							damage = 2;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(18, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6831:
//				// Desert Wyrm
//			case 6832:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12460, 1)) {
//							p.getItems().deleteItem2(12460, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1411, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7795);
//							n.gfx0(1410);
//							damage = Misc.random(5);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(14, 23);
//							p.refreshSkill(23);
//							endGfx = 1413;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7361:
//				// TzKih
//			case 7362:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12839, 1)) {
//							p.getItems().deleteItem2(12839, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// poffX,
//							// poffY, 50, 70, 1411, 41, 28, c.playerIndex + 1,
//							// 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(8257);
//							// n.gfx0(1410);
//							damage = Misc.random(7);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(39, 23);
//							p.refreshSkill(23);
//							endGfx = 1329;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6994:
//				// Kalphite
//			case 6995:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12446, 1)) {
//							p.getItems().deleteItem2(12446, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1349, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8517);
//							// n.gfx0(1410);
//							damage = Misc.random(5);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(81, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7335:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12841, 1)) {
//							p.getItems().deleteItem2(12841, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7871);
//							n.gfx0(1394);
//							damage = Misc.random(20);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(181, 23);
//							p.refreshSkill(23);
//							endGfx = 1395;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7353:
//				// chinchompa
//			case 7354:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12834, 1)) {
//							p.getItems().deleteItem2(12834, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7758);
//							n.gfx0(1364);
//							damage = Misc.random(12);
//							p.specHitTimer = 2;
//							p.getPA().addSkillXP(104, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//			case 6835:
//				// vampire bat
//			case 6836:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 4) {
//						if (p.getItems().playerHasItem(12447, 1)) {
//							p.getItems().deleteItem2(12447, 1);
//							p.summoningSpecialPoints -= 4;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(8277);
//							n.gfx0(1467);
//							damage = Misc.random(12);
//							p.playerLevel[3] += Misc.random(2);
//							if (p.playerLevel[3] > PlayerConstants
//									.getLevelForXP(p.playerXP[3])) {
//								p.playerLevel[3] = PlayerConstants
//										.getLevelForXP(p.playerXP[3]);
//							}
//							p.specHitTimer = 2;
//							p.getPA().addSkillXP(56, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6853:
//				// bronze minot
//			case 6854:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12461, 1)) {
//							p.getItems().deleteItem2(12461, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 4;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1497, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(4);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(126, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6833:
//				// evil turnip
//			case 6834:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12448, 1)) {
//							p.getItems().deleteItem2(12448, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 1;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1330, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8251);
//							damage = Misc.random(6);
//							p.playerLevel[4] += damage / 2;
//							if (p.playerLevel[4] > p.getPA()
//									.getLevelForXP(p.playerXP[4]) + 6) {
//								p.playerLevel[4] = p.getPA()
//										.getLevelForXP(p.playerXP[4]) + 6;
//							}
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(74, 23);
//							p.refreshSkill(23);
//							p.getPA()
//									.refreshSkill(PlayerConstants.playerRanged);
//							if (damage == 0) {
//								endGfx = 85;
//							} else {
//								endGfx = 1329;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6855:
//				// iron minot
//			case 6856:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12462, 1)) {
//							p.getItems().deleteItem2(12462, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 4;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1497, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(8);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(161, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6800:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12457, 1)) {
//							p.getItems().deleteItem2(12457, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1362, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7852);
//							damage = Misc.random(19);
//							p.specHitTimer = 3;
//							endGfx = 1363;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6992:
//				// spirit Jelly
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12453, 1)) {
//							p.getItems().deleteItem2(12453, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1359, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8575);
//							damage = Misc.random(12);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(193, 23);
//							p.refreshSkill(23);
//							endGfx = 1360;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7347:
//				// talon beast
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12831, 1)) {
//							p.getItems().deleteItem2(12831, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1520, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8043);
//							n.gfx0(1519);
//							damage = Misc.random(8);
//							damage2 = Misc.random(8);
//							doubleHit = true;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(395, 23);
//							p.refreshSkill(23);
//							if (damage == 0 || damage2 == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6804:
//				// Spirit Dag
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12456, 1)) {
//							p.getItems().deleteItem2(12456, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1426, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7787);
//							// n.gfx0(1410);
//							damage = Misc.random(18);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(281, 23);
//							p.refreshSkill(23);
//							endGfx = 1428;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7341:
//				// lava titan
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 4) {
//						if (p.getItems().playerHasItem(12837, 1)) {
//							p.getItems().deleteItem2(12837, 1);
//							p.summoningSpecialPoints -= 4;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1493, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7986);
//							n.gfx0(1492);
//							damage = Misc.random(20);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(241, 23);
//							p.refreshSkill(23);
//							endGfx = 1494;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7375:
//				// iron Titan
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 12) {
//						if (p.getItems().playerHasItem(12828, 1)) {
//							p.getItems().deleteItem2(12828, 1);
//							p.summoningSpecialPoints -= 12;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 2;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7837);
//							n.gfx0(1450);
//							damage = Misc.random(23);
//							damage2 = Misc.random(23);
//							doubleHit = true;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(615, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6857:
//				// steel minot
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12463, 1)) {
//							p.getItems().deleteItem2(12463, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1497, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(9);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(195, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6875:
//				// the cocks
//			case 6877:
//			case 6879:
//			case 6881:
//			case 6883:
//			case 6885:
//			case 6887:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12458, 1)) {
//							p.getItems().deleteItem2(12458, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1468, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7766);
//							n.gfx0(1467);
//							damage = Misc.random(10);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(33, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							} else {
//								endGfx = 1469;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7365:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12836, 1)) {
//							p.getItems().deleteItem2(12836, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7914);
//							n.gfx0(1369);
//							damage = Misc.random(11) * 3;
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(199, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6863:
//				// rune minot
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12466, 1)) {
//							p.getItems().deleteItem2(12466, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1497, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(20);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(395, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6809:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 3) {
//						if (p.getItems().playerHasItem(12455, 1)) {
//							p.getItems().deleteItem2(12455, 1);
//							p.summoningSpecialPoints -= 3;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1479, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7974);
//							n.gfx0(1478);
//							damage = Misc.random(16);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(206, 23);
//							p.refreshSkill(23);
//							endGfx = 1480;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6865:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12468, 1)) {
//							p.getItems().deleteItem2(12468, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].startAnimation(7821);
//							n.gfx0(1470);
//							damage = Misc.random(6);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(223, 23);
//							p.refreshSkill(23);
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6827:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12467, 1)) {
//							p.getItems().deleteItem2(12467, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1508, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8214);
//							damage = Misc.random(13);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(245, 23);
//							p.refreshSkill(23);
//							endGfx = 1511;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6859:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12464, 1)) {
//							p.getItems().deleteItem2(12464, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 4;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1497, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(13);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(300, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7329:
//				// swamp titan
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12832, 1)) {
//							p.getItems().deleteItem2(12832, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1462, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8223);
//							damage = Misc.random(21);
//							p.getPA().crabSpecBonus(
//									PlayerConstants.playerHerblore, false);
//							p.refreshSkill(
//									PlayerConstants.playerHerblore);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(241, 23);
//							p.refreshSkill(23);
//							endGfx = 1460;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6889:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12452, 1)) {
//							p.getItems().deleteItem2(12452, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							// c.getPA().createPlayersProjectile(famX, famY,
//							// offX,
//							// offY, 50, 70, 1508, 41, 28, c.npcIndex + 1, 5,
//							// c.heightLevel);
//							NPCHandler.npcs[i].startAnimation(7705);
//							n.gfx0(1403);
//							damage = Misc.random(18);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(45, 23);
//							p.refreshSkill(23);
//							endGfx = 1404;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 7372:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 12) {
//						if (p.getItems().playerHasItem(12830, 1)) {
//							p.getItems().deleteItem2(12830, 1);
//							p.summoningSpecialPoints -= 12;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1347, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(7995);
//							NPCHandler.npcs[i].gfx0(1346);
//							damage = Misc.random(7);
//							p.specHitTimer = 3;
//							p.getPA().addSkillXP(182, 23);
//							p.refreshSkill(23);
//							endGfx = 1348;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6839:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12451, 1)) {
//							p.getItems().deleteItem2(12451, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							NPCHandler.npcs[i].gfx100(1405);
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1406, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(4921);
//							damage = Misc.random(15);
//							p.specHitTimer = 3;
//							if (damage == 0) {
//								endGfx = 85;
//							} else {
//								endGfx = 1407;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 8575:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 5) {
//						if (p.getItems().playerHasItem(14622, 1)) {
//							p.getItems().deleteItem2(14622, 1);
//							p.summoningSpecialPoints -= 5;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1330, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(11093);
//							damage = Misc.random(14);
//							p.specHitTimer = 3;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6849:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12449, 1)) {
//							p.getItems().deleteItem2(12449, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1352, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8118);
//							NPCHandler.npcs[i].gfx0(1353);
//							damage = Misc.random(14);
//							damage2 = Misc.random(14);
//							doubleHit = true;
//							p.specHitTimer = 3;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6798:
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12450, 1)) {
//							p.getItems().deleteItem2(12450, 1);
//							p.summoningSpecialPoints -= 6;
//							typicalStuff(p);
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							hitType = 2;
//							NPCHandler.npcs[i].startAnimation(8071);
//							NPCHandler.npcs[i].gfx0(1353);
//							damage = Misc.random(17);
//							p.specHitTimer = 3;
//							endGfx = 1353;
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//			case 6861:
//				// addy minot
//				if (p.familiarIndex > 0) {
//					return;
//				} else {
//					if (p.summoningSpecialPoints >= 6) {
//						if (p.getItems().playerHasItem(12465, 1)) {
//							p.getItems().deleteItem2(12465, 1);
//							p.summoningSpecialPoints -= 6;
//							p.startAnimation(7660);
//							p.gfx0(1316);
//							NPCHandler.npcs[p.familiarIndex].attackTimer = 3;
//							p.familiarIndex += 6;
//							hitType = 2;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							PlayerConstants.createPlayersProjectile(famX, famY,
//									offX, offY, 50, 70, 1497, 41, 28,
//									p.npcIndex + 1, 5, p.getHeightLevel());
//							NPCHandler.npcs[i].startAnimation(8026);
//							n.gfx0(1496);
//							damage = Misc.random(16);
//							p.specHitTimer = 3;
//							if (p.playerEquipment[PlayerConstants.playerCape] == 19893) {
//								p.summoningSpecialPoints += 2;
//							}
//							p.getPA().addSkillXP(349, 23);
//							p.refreshSkill(23);
//							if (damage == 0) {
//								endGfx = 85;
//							}
//						} else {
//							p.sendMessage(
//									"You do not have the correct scroll for this familiar.");
//						}
//					} else {
//						p.sendMessage(
//								"You do not have enough special points to cast this special.");
//					}
//				}
//				break;
//
//		}
//	}

}
