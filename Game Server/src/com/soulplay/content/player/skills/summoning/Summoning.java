package com.soulplay.content.player.skills.summoning;

import java.util.HashMap;

import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.DroppedItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Graphic.GraphicType;

/**
 * The Summoning Handler class.
 *
 * @Author: Mr-Nick
 */
public class Summoning {
	
	public static final Graphic TELEPORT_GFX = Graphic.create(1315);

	/**
	 * Represents different Familiar's.
	 *
	 * @Author: Mr-Nick
	 */
	public static enum Familiar {
		Spirit_Wolf(6829, 12047, 12425, 15, FamiliarType.NORMAL, 0, 40, false,
				"Howl", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 3) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 3..");
					}
				}),
		Spirit_Spider(6841, 12059, 12428, 15, FamiliarType.NORMAL, 0, 40, false,
				"Egg spawn", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 5) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 5..");
					}
				}),
		Thorny_Snail(6806, 12019, 12459, 15, FamiliarType.BOB, 3, 40, false,
				"Slime Spray", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 4) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 4..");
					}
				}),

		/**
		 * HAS IDS ANIMS ++ ABOVE
		 */

		Granite_Crab(6796, 12009, 12533, 15, FamiliarType.NORMAL, 0, 40, false, "Stony Shell", "", arguments -> {
			if (((Client) arguments[0])
					.getSummoning().getFamiliarSpecialEnergy() >= 2) {
				((Client) arguments[0]).sendMessage(
						"This special attack is currently unvailable.");
			} else {
				((Client) arguments[0]).sendMessage("Your familiar has "
						+ ((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy()
						+ " special energy left and needs 2..");
			}
		}),
		Spirit_Mosquito(7331, 12778, 12838, 15, FamiliarType.NORMAL, 0, 40,
				false, "Pester", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 5) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 5..");
					}
				}),
		Desert_Wyrm(6831, 12049, 12460, 15, FamiliarType.NORMAL, 0, 40, false,
				"Electric lash", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 6) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 6..");
					}
				}),
		Spirit_Scorpion(6837, 12055, 12432, 15, FamiliarType.NORMAL, 0, 40,
				false, "Venom shot", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 6) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 6..");
					}
				}),
		Spirit_Tz_Kih(7361, 12808, 12839, 15, FamiliarType.NORMAL, 0, 40, false,
				"Fireball assault", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Compost_Mound(6871, 12091, 12440, 15, FamiliarType.NORMAL, 0, 40, false,
				"Generate compost", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 8) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 8..");
					}
				}),
		Vampire_Bat(6835, 12053, 12447, 15, FamiliarType.NORMAL, 0, 40, false,
				"Vampyre touch", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 9) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 9..");
					}
				}),
		Honey_Badger(6845, 12065, 12433, 15, FamiliarType.NORMAL, 0, 40, false,
				"Insane ferocity", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 9) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 9..");
					}
				}),
		Beaver(6807, 12021, 12429, 15, FamiliarType.NORMAL, 0, 40, false,
				"Multichop", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 3) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 3..");
					}
				}),
		Void_Shifter(7367, 12814, 12443, 15, FamiliarType.NORMAL, 0, 40, false,
				"Call to Arms", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 5) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 5..");
					}
				}),
		Void_Torcher(7351, 12798, 12443, 15, FamiliarType.NORMAL, 0, 40, false,
				"Call to Arms", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 5) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 5..");
					}
				}),
		Void_Spinner(7333, 12780, 12443, 15, FamiliarType.NORMAL, 0, 40, false,
				"Call To Arms", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 5) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 5..");
					}
				}),
		Bull_Ant(6867, 12087, 12431, 15, FamiliarType.BOB, 9, 40, false,
				"Unburden", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 10) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Evil_Turnip(6833, 12051, 12448, 15, FamiliarType.NORMAL, 0, 40, false,
				"Evil flames", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 10) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Spirit_Cockatrice(6875, 12095, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Spirit_Guthatrice(6877, 12097, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Spirit_Saratrice(6879, 12099, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Spirit_Zamatrice(6881, 12101, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Spirit_Pengatrice(6883, 12103, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Spirit_Coraxatrice(6885, 12105, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Spirit_Vulatrice(6887, 12107, 12458, 15, FamiliarType.NORMAL, 0, 40,
				false, "Petrifying Gaze", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Bloated_Leech(6843, 12061, 12444, 15, FamiliarType.NORMAL, 0, 40, false,
				"Blood Drain", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 11) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 11..");
					}
				}),
		Abyssal_Parasite(6818, 12035, 12454, 15, FamiliarType.BOB, 3, 40, false,
				"Abyssal Drain", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 8) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 8..");
					}
				}),
		Spirit_Jelly(6992, 12027, 12453, 15, FamiliarType.NORMAL, 0, 40, false,
				"Dissolve", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 4) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 4..");
					}
				}),
		Ibis(6990, 12531, 12424, 15, FamiliarType.NORMAL, 0, 40, false,
				"Fish Rain", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 3) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 3..");
					}
				}),
		Spirit_Graahk(7363, 12810, 12835, 15, FamiliarType.NORMAL, 0, 40, false,
				"Goad", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 10) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Spirit_Kyatt(7365, 12812, 12836, 15, FamiliarType.NORMAL, 0, 40, false,
				"Ambush", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 10) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Spirit_Larupia(7337, 12784, 12840, 15, FamiliarType.NORMAL, 0, 40,
				false, "Rending", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 10) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Karamthulhu_Overlord(6809, 12023, 12455, 15, FamiliarType.NORMAL, 0, 40,
				false, "Doomsphere", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Smoke_Devil(6865, 12085, 12468, 15, FamiliarType.NORMAL, 0, 40, false,
				"Dust cloud", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 10) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Stranger_Plant(6827, 12045, 12467, 15, FamiliarType.NORMAL, 0, 40,
				false, "Poisonous blast", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Fruit_Bat(6817, 12033, 12423, 15, FamiliarType.NORMAL, 0, 40, false,
				"Fruitfall", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 3) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 3..");
					}
				}),
		Ravenous_Locust(7372, 12820, 12830, 15, FamiliarType.NORMAL, 0, 40,
				false, "Famine", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 12) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 12..");
					}
				}),
		Arctic_Bear(6839, 12057, 12451, 15, FamiliarType.NORMAL, 0, 40, false,
				"Arctic Blast", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 12) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 12..");
					}
				}),
		Obsidian_Golem(7345, 12792, 12826, 15, FamiliarType.NORMAL, 0, 40,
				false, "Volcanic strength", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 13) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 13..");
					}
				}),
		Praying_Mantis(6798, 12011, 12450, 15, FamiliarType.NORMAL, 0, 40,
				false, "Mantis strike", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 6) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 6..");
					}
				}),
		Forge_Regent(7335, 12782, 12841, 15, FamiliarType.NORMAL, 0, 40, false,
				"Inferno", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 7) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 7..");
					}
				}),
		Talon_Beast(7347, 12794, 12831, 15, FamiliarType.NORMAL, 0, 40, false,
				"Deadly claw", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 8) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 8..");
					}
				}),
		Giant_Ent(6800, 12013, 12457, 15, FamiliarType.NORMAL, 0, 40, false,
				"Acorn Missile", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 11) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 11..");
					}
				}),
		Hydra(6811, 12025, 12442, 15, FamiliarType.NORMAL, 0, 40, false,
				"Regrowth", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 5) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 5..");
					}
				}),
		Spirit_Dagannoth(6804, 12017, 12456, 15, FamiliarType.NORMAL, 0, 40,
				false, "Spike Shot", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 12) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 12..");
					}
				}),
		Lava_Titan(7341, 12788, 12837, 15, FamiliarType.NORMAL, 0, 40, false,
				"Ebon Thunder", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 13) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 13..");
					}
				}),
		Swamp_Titan(7329, 12776, 12832, 15, FamiliarType.NORMAL, 0, 40, false,
				"Swamp Plague", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 13) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 13..");
					}
				}),
		Rune_Minotaur(6863, 12083, 12466, 15, FamiliarType.NORMAL, 0, 40, false,
				"Rune Bull Rush", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 12) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 12..");
					}
				}),
		Abyssal_Titan(7349, 12796, 12827, 15, FamiliarType.BOB, 3, 40, false,
				"Essence Shipment", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 14) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 14..");
					}
				}),
		Iron_Titan(7375, 12822, 12828, 15, FamiliarType.NORMAL, 0, 40, false,
				"Iron Within", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						((Client) arguments[0]).sendMessage(
								"This special attack is currently unvailable.");
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 15..");
					}
				}),

		/**
		 * HAS SPECIAL BELOW
		 */

		Spirit_Terrorbird(6794, 12007, 12441, 15, FamiliarType.BOB, 12, 0, false, "Tireless Run", "", arguments -> {
			if (((Client) arguments[0])
					.getSummoning().getFamiliarSpecialEnergy() >= 8) {
				int toRestore = Math.round(((Client) arguments[0]).getSkills().getStaticLevel(Skills.AGILITY) / 2);
				((Client) arguments[0]).setRunEnergy(((Client) arguments[0]).getRunEnergy() + toRestore);
				((Client) arguments[0]).getSkills().updateLevel(Skills.AGILITY, 2);
				((Client) arguments[0]).startGraphic(Graphic.create(1300));
				((Client) arguments[0]).startAnimation(7660);
				((Client) arguments[0]).getSummoning().specialTimer = 5;
				((Client) arguments[0]).getSummoning().familiarSpecialEnergy -= 8;
				((Client) arguments[0]).getItems().deleteItem2(((Client) arguments[0]).getSummoning().summonedFamiliar.scrollId, 1);
			} else {
				((Client) arguments[0]).sendMessage("Your familiar has "
						+ ((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy()
						+ " special energy left and needs 15..");
			}

		}),
		Wolpertinger(6869, 12089, 12437, 62, FamiliarType.NORMAL, 0, 40, false,
				"Magic Focus", "Mew!", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						NPCHandler.startAnimation(8305,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).summoned.startGraphic(Graphic.create(1410));
						((Client) arguments[0]).startAnimation(7660);
						((Client) arguments[0]).startGraphic(Graphic.create(1313, GraphicType.LOW));
						((Client) arguments[0]).getPotions().doWolperSpecial();
						((Client) arguments[0])
								.sendMessage("Your magic is boosted!");
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 15);
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 15..");
					}
				}),
		Unicorn_Stallion(6822, 12039, 12434, 15, FamiliarType.NORMAL, 0, 40,
				false, "Healing Aura", "", arguments -> {
					if (((Client) arguments[0]).getSummoning().handleSpecialMoveCost(20)) {
						int heal = 0;
						heal = ((Client) arguments[0])
								.getSkills().getMaximumLifepoints()/* maxHp() */;
						heal *= 0.15;
						((Client) arguments[0]).addToHp(heal);
						NPCHandler.startAnimation(6375,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).summoned.startGraphic(Graphic.create(1356));
						((Client) arguments[0]).startAnimation(7660);
						((Client) arguments[0]).startGraphic(Graphic.create(1313, GraphicType.LOW));
						((Client) arguments[0]).sendMessage(
								"You restored " + heal + " Health!");
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 20..");
					}
				}),
		War_Tortoise(6815, 12031, 12439, 30, FamiliarType.BOB, 18, 0, true,
				"Testudo", "", arguments -> {
					/* Special goes here */
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						NPCHandler.startAnimation(8288,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).summoned.startGraphic(Graphic.create(1414));
						((Client) arguments[0]).startAnimation(7660);
						((Client) arguments[0]).startGraphic(Graphic.create(1313, GraphicType.LOW));
						((Client) arguments[0])
								.sendMessage("Your defence has been boosted!");
						((Client) arguments[0]).getPotions()
								.doTortoiseSpecial();
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 15);
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 15..");
					}
				}),
		Steel_Titan(7343, 12790, 12825, 30, FamiliarType.COMBAT, 18, 0, false,
				"Steel of Legends", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						NPCHandler.startAnimation(8190,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).summoned.startGraphic(Graphic.create(1449));
						((Client) arguments[0]).usingSummoningSpecial = true;
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 15);
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 15..");
					}
				}),
		Geyser_Titan(7339, 12786, 12833, 30, FamiliarType.COMBAT, 18, 0, false,
				"Boil", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						NPCHandler.startAnimation(7883,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).summoned.startGraphic(Graphic.create(1373));
						((Client) arguments[0]).usingSummoningSpecial = true;
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 15);
						((Client) arguments[0]).summoned.projectileId = 1376;
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 15..");
					}
				}),
		Fire_Titan(7355, 12802, 12824, 15, FamiliarType.NORMAL, 0, 40, false,
				"Titan's Constitution", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 20) {
						NPCHandler.startAnimation(8190,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).getPlayerLevel()[3] += 14;
						if (((Client) arguments[0]).getSkills().getMaximumLifepoints()
								+ 14 <= ((Client) arguments[0]).getPlayerLevel()[3]) {
							((Client) arguments[0]).getPlayerLevel()[3] = ((Client) arguments[0])
									.getSkills().getMaximumLifepoints() + 14;
						}
						((Client) arguments[0]).startAnimation(7660);
						((Client) arguments[0]).startGraphic(Graphic.create(1313, GraphicType.LOW));
						((Client) arguments[0]).getPotions().doTitanSpecial(1);
						((Client) arguments[0]).sendMessage(
								"Your Defence and Constitution have been boosted.");
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 20);
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 20..");
					}
				}),
		Moss_Titan(7357, 12804, 12824, 15, FamiliarType.NORMAL, 0, 40, false,
				"Titan's Constitution", "", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 20) {
						NPCHandler.startAnimation(8190,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).getPlayerLevel()[3] += 14;
						if (((Client) arguments[0]).getSkills().getMaximumLifepoints()
								+ 14 <= ((Client) arguments[0]).getPlayerLevel()[3]) {
							((Client) arguments[0]).getPlayerLevel()[3] = ((Client) arguments[0])
									.getSkills().getMaximumLifepoints() + 14;
						}
						((Client) arguments[0]).startAnimation(7660);
						((Client) arguments[0]).startGraphic(Graphic.create(1313, GraphicType.LOW));
						((Client) arguments[0]).getPotions().doTitanSpecial(1);
						((Client) arguments[0]).sendMessage(
								"Your Defence and Constitution have been boosted.");
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 20);
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 20..");
					}
				}),
		Ice_Titan(7359, 12806, 12824, 30, FamiliarType.COMBAT, 18, 0, false,
				"Titan's Constitution", "I'm melting!", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 20) {
						NPCHandler.startAnimation(8190,
								((Client) arguments[0]).summoningMonsterId);
						((Client) arguments[0]).getPlayerLevel()[3] += 14;
						if (((Client) arguments[0]).getSkills().getMaximumLifepoints()
								+ 14 <= ((Client) arguments[0]).getPlayerLevel()[3]) {
							((Client) arguments[0]).getPlayerLevel()[3] = ((Client) arguments[0])
									.getSkills().getMaximumLifepoints() + 14;
						}
						((Client) arguments[0]).startAnimation(7660);
						((Client) arguments[0]).startGraphic(Graphic.create(1313, GraphicType.LOW));
						((Client) arguments[0]).getPotions().doTitanSpecial(1);
						((Client) arguments[0]).sendMessage(
								"Your Defence and Constitution have been boosted.");
						((Client) arguments[0]).getSummoning().specialTimer = 5;
						((Client) arguments[0])
								.getSummoning().setFamiliarSpecialEnergy(((Client) arguments[0])
								.getSummoning().getFamiliarSpecialEnergy() - 20);
						((Client) arguments[0]).getItems().deleteItem2(
								((Client) arguments[0])
										.getSummoning().summonedFamiliar.scrollId,
								1);
					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 20..");
					}
				}),
		Pack_Yak(6873, 12093, 12435, 58, FamiliarType.BOB, 30, 10, true,
				"Winter Storage", "Baroo baroo!", arguments -> {
					if (((Client) arguments[0])
							.getSummoning().getFamiliarSpecialEnergy() >= 15) {
						// ((Client) arguments[0]).getPA().sendString(":moi:
						// 24000", 50000); //TODO: add "Use on" interface
						((Client) arguments[0]).sendMessage(
								"You must use the scroll on an item you wish to send to the bank."); // .sendMessage("Please
																										// select
																										// the
																										// item
																										// you
																										// would
																										// like
																										// to
																										// send
																										// to
																										// your
																										// bank!");

					} else {
						((Client) arguments[0]).sendMessage("Your familiar has "
								+ ((Client) arguments[0])
										.getSummoning().getFamiliarSpecialEnergy()
								+ " special energy left and needs 10..");
					}
				}),
		Pyrelord(7377, 12816, 12829, 10, FamiliarType.COMBAT, 0, 10, false,
				"Immense heat", null, arguments -> {
				});

		public static HashMap<Integer, Familiar> familiars = new HashMap<>();

		public static Familiar forPouchId(int id) {
			return familiars.get(id);
		}
		
		public static boolean isPouchItem(int id) {
			return familiars.containsKey(id);
		}

		public static void loadFamiliars() {
			for (Familiar f : Familiar.values()) {
				familiars.put(f.pouchId, f);
			}
		}

		public int npcId;

		public int pouchId;

		public int scrollId;

		public int timeLimit;

		public FamiliarType familiarType;

		public int storeCapacity;

		public int specialEnergyConsumption;

		public boolean large;

		public String clientTooltip;

		public String speakText;

		public FamiliarSpecial familiarSpecial;

		private Familiar(int npcId, int pouchId, int scrollId, int timeLimit,
				FamiliarType familiarType, int storeCapacity,
				int specialEnergyConsumption, boolean large,
				String clientTooltip, String textChat,
				FamiliarSpecial familiarSpecial) {
			this.npcId = npcId + 1;
			this.pouchId = pouchId;
			this.scrollId = scrollId;
			this.timeLimit = timeLimit;
			this.familiarType = familiarType;
			this.storeCapacity = storeCapacity;
			this.specialEnergyConsumption = specialEnergyConsumption;
			this.large = large;
			this.clientTooltip = clientTooltip;
			this.speakText = textChat;
			this.familiarSpecial = familiarSpecial;
		}
	}

	/**
	 * Familiar special interface.
	 */
	public static interface FamiliarSpecial {

		public abstract void execute(Object... arguements);
	}

	/**
	 * Represents different FamiliarType's.
	 *
	 * @Author: Mr Nick
	 */
	public static enum FamiliarType {
		NORMAL, COMBAT, BOB;
	}

	/**
	 * Owner of the summoning instance.
	 */
	public final Player c;

	/**
	 * The owner's summoned familiar.
	 */
	public Familiar summonedFamiliar = null;

	private boolean callFamiliarPause = false;

	private int specialRestoreCycle = 0;

	private int speakTimer = 70;

	// // 2 dissmiss?
	//
	// public void dismissFamiliar() {
	// if (summonedFamiliar != null && c.summoned != null) {
	// c.summoned.isDead = true;
	// c.summoned.applyDead = true;
	// c.summoned.actionTimer = 0;
	// c.summoned.npcType = -1;
	// //c.summoned.updateRequired = true;
	// c.usingSummoningSpecial = false;
	// c.summoned.destroy();
	// //c.setSidebarInterface(15, -1);
	// //c.getPA().sendSummOrbDetails(false, "");
	// c.burdenedItems = null;
	// summonedFamiliar = null;
	// c.summoned = null;
	// }
	// }
	//
	// /**
	// * If player dies, the summoning will dismiss
	// */
	// public void dismissOnDeath() {
	// if (summonedFamiliar != null && c.summoned != null) {
	// dismissFamiliar();
	// c.summoned.npcType = -1;
	// }
	// }

	private int familiarSpecialEnergy = 60;

	private int loginCycle = -1;

	public int specialTimer = 0;

	public int renewTimer = -1;

	public int actionTimer = 0;

	public boolean attacked = false;

	/**
	 * Instances the summoning class.
	 */
	public Summoning(Player player) {
		this.c = player;
	}

	/**
	 * Returns true if the item was added to the container successfully.
	 */
	public boolean addItem(int itemId) {
		if (c.getItems().wildyProhibitedItem(itemId - 1))
			return false;
		int nextFreeSlot = getSlotForId(1);
		if (itemId <= 0) {
			return false;
		}
		if (nextFreeSlot != -1 && c.burdenedItems[nextFreeSlot] == 0) {
			c.burdenedItems[nextFreeSlot] = itemId;
			return true;
		} else {
			return false;
		}
	}

	private boolean bobWithSpec(int npcType) {
		switch (npcType) {
			case 6873:
				return true;
		}
		return false;
	}

	/**
	 * 1. 3100 3500 1 3 2. 3100 3501 2 4 3. 3099 3500 4. 3099 3501
	 */
	/**
	 * x - y x-1 - y x - y+1 x-1 - y+1
	 */
	/**
	 * Calls the familiar to the owner.
	 */
	public void callFamiliar() {
		if (summonedFamiliar != null && c.summoned != null) {
			// } else {
			//// c.getPA().sendSummOrbDetails(false, ""); //TODO: readd
			// }
			c.summonedCanTeleport = false;
			c.summoned.setKillerId(0);
			c.summoned.underAttackBy = 0;
			// c.summoned.npcTeleport(0, 0, 0);
			c.summoned.npcTeleport(c.getCurrentLocation().getX(),
					c.getCurrentLocation().getY() + (summonedFamiliar.large ? 2 : 1),
					c.getHeightLevel());

		}

	}

	public void callFamiliarButtonPress() {
		if (callFamiliarPause) {
			c.sendMessage(
					"You must wait 30 seconds before calling your familiar again.");
			return;
		}
		if (summonedFamiliar != null && c.summoned != null) {

			c.summoned.teleportToPlayer(c, 1315);
			c.summoned.setHomeOwnerId(c.getHomeOwnerId());

			callFamiliarPause = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {

					container.stop();

				}

				@Override
				public void stop() {
					if (this != null) {
						callFamiliarPause = false;
					}
				}
			}, 50);
		}

	}

	/**
	 * If player gets offscreen (most likely teleports), familiar will come to
	 * them
	 */
	public void callOnTeleport() {
		NPC summoned = c.summoned;
		if (summoned == null) {
			return;
		}
		if (summonedFamiliar != null || summoned.isPetNpc()) {
			/*
			 * c.summoned.canTeleport = true; c.summoned.npcTeleport(c.absX,
			 * c.absY, c.heightLevel);
			 */
			Location loc = Location.getCloseLocation(c.getCurrentLocation());
			NPCHandler.trueTeleport(c.summoned, loc.getX(), loc.getY(), c.getZ());
		}
		/*
		 * if (summonedFamiliar != null && c.summoned != null) {
		 * c.summoned.needRespawn = true; NPCHandler.kill(c.summoned);
		 * c.summoned = NPCHandler.summonNPC(c, summonedFamiliar.npcId,
		 * c.getLocation().getX(), c.getLocation().getY() +
		 * (summonedFamiliar.large ? 2 : 1), c.getHeightLevel(), 0,
		 * c.summoned.HP, 1, 1, 1); // callFamiliar(); } else if (c.summoned !=
		 * null && c.summoned.isPetNpc()Pet.isPetNPC(c.summoned.npcType)) {
		 * c.summoned.teleportToPlayer(c, 1315); c.summoned.needRespawn = true;
		 * int petId = c.summoned.npcType; NPCHandler.kill(c.summoned);
		 * c.summoned = NPCHandler.summonNPC(c, petId, c.getLocation().getX(),
		 * c.getLocation().getY() + 1, c.getHeightLevel(), 0, 0, 0, 0, 0); //
		 * c.getPA().sendSummOrbDetails(false, ""); //TODO: Readd }
		 */
	}

	public boolean canWalk() {
		int squares = c.summoned.getSize(); // = c.summoned.getNpcSize() *
											// c.summoned.getNpcSize();
		int x = 0;
		int y = 0;
		for (int i = 0; i < squares; i++) {
			if (i == 1) {
				x--;
			}
			if (i == 2) {
				y++;
			}
			if (i == 3) {
				x++;
			}
			if (!((RegionClip.getClipping(
					c.getCurrentLocation().getX() + x, c.getCurrentLocation().getY()
							+ (summonedFamiliar.large ? 2 : 1) + y,
					c.getHeightLevel()) & 0x1280180) == 0)) {
				c.summoned.setKillerId(0);
				c.summoned.underAttackBy = 0;
				return false;
			}
		}
		return true;
	}

	/**
	 * Deposits an item from the owner's inventory, to the familiar's inventory.
	 *
	 * @param id
	 *            - Item id
	 * @param slot
	 *            - Item slot
	 * @param amount
	 *            - Item amount
	 */
	public void depositItem(int id, int slot, int amount) {
		if (c.getZones().isInDmmLobby())
			return;
		if (summonedFamiliar != null && c.summoned != null) {
			if (summonedFamiliar.familiarType == FamiliarType.BOB) {
				if (amount > 0 && c.playerItems[slot] > 0 && slot >= 0
						&& slot < 28) {
					if (itemIsAllowed(c.playerItems[slot] - 1) && !c.getItems().isStackable(c.playerItems[slot] - 1)) {
						while (amount > 0 && slot != -1) {
							if (addItem(c.playerItems[slot])) {
								int tempVar = c.playerItems[slot] - 1;
								c.getItems().deleteItemInOneSlot(
										c.playerItems[slot] - 1, slot, 1);
								slot = c.getItems().getItemSlot(tempVar);
								amount--;
								c.startAnimation(827);
							} else {
								break;
							}
						}
					} else {
						c.sendMessage("You cannot deposit this item!");
					}
				}
				c.getItems().resetItems(5064/* 24006 */);
				c.getItems().resetItems(39502/* 24002 */, c.burdenedItems);
			}
		}
	}

	/**
	 * Dismisses/Destroy's the familiar.
	 */
	public void dismissFamiliar(boolean logout) {
		if (c.summoned != null) {
			if (c.summoned.isPetNpc()) {
				Pet.pickupPet(c, Pet.getPetForNPC(c.summoned.npcType));
				return;
			}
			if (summonedFamiliar != null) {
				c.summoned.setDead(true);
				c.summoned.applyDead = true;
				c.summoned.actionTimer = 0;
				// c.summoned.npcType = -1;
				// c.summoned.updateRequired = true;
				c.usingSummoningSpecial = false;
//				c.summoned.needRespawn = true;
				c.getPacketSender().sendString("", 17017);
				c.getPacketSender().sendString("00:00", 17021);
				// c.setSidebarInterface(15, -1);
				// c.getPA().sendSummOrbDetails(false, "");
				c.getPacketSender().drawNpcOnInterface(4000, 17027);
				if (!logout) {
					for (int burdenedItem : c.burdenedItems) {
						if (burdenedItem < 1 || burdenedItem - 1 < 1) {
							continue;
						}
						ItemHandler.createGroundItem(c, burdenedItem - 1,
								c.summoned.absX, c.summoned.absY,
								c.summoned.heightLevel, 1);

						if (DBConfig.LOG_DROP) {
							Server.getLogWriter().addToDropList(new DroppedItem(
									c, burdenedItem - 1, 1, "bob"));
						}

					}
					c.burdenedItems = new int[30];
					c.familiarPouchId = 0;
					c.familiarTimer = 0;
				}
				summonedFamiliar = null;
				c.summoned = null;
			}
		}
	}

	/**
	 * The "process" for familiars.
	 */
	public void familiarTick() {
		if (c.summoned.isPetNpc()) {
			return;
		}
		attacked = false;
		if (c.summoned != null && (summonedFamiliar != null || c.summoned.isPokemon)) {
			/*
			 * if (!c.goodDistance(c.getX(), c.getY(), c.summoned.getX(),
			 * c.summoned.getY(), 8)) { callOnTeleport(); } if
			 * (!c.goodDistance(c.getX(), c.getY(), c.summoned.getX(),
			 * c.summoned.getY(), 8)) { c.getSummoning().callFamiliar(); return;
			 * }
			 */
			if ((summonedFamiliar != null && summonedFamiliar.familiarType.equals(FamiliarType.COMBAT)) || c.summoned.isPokemon) {
				if (c.playerIndex > 0) { // if player is attacking another
											// player
					c.getSummonedNPC().isAttackingAPerson = true;
				} else if (c.playerIndex <= 0) {
					c.getSummonedNPC().isAttackingAPerson = false;
				}
				if (c.underAttackBy > 0) {
					c.getSummonedNPC().ownerUnderAttackByPlayer = true;
				} else if (c.underAttackBy <= 0) {
					c.getSummonedNPC().ownerUnderAttackByPlayer = false;
				}
				if (c.npcIndex != 0 && !c.getSummonedNPC().isAttackingAPerson) {
					c.getSummonedNPC().IsAttackingNPC = true;
					c.getSummonedNPC().resetFace();
				} else {
					if (c.getSummonedNPC().IsAttackingNPC == true) {
						callFamiliar();
					}
					c.getSummonedNPC().IsAttackingNPC = false;
				}
				if (c.summoned.getKillerId() > 0 && c.summoned.underAttackBy <= 0) {
					c.summoned.setKillerId(0);
					c.summoned.underAttackBy = 0;
					c.getSummoning().callFamiliar();
				}
				if (!attacked && c.getSummonedNPC().ownerUnderAttackByPlayer) {
						attacked = true;
						final Client o = (Client) PlayerHandler.players[c.underAttackBy];
						if (o != null && o.wildLevel > 0 && o.inMulti() && MeleeRequirements.checkCombatDifference(c, o, false)) {
								if (actionTimer == 0) {
//									NPCHandler.followPlayer(
//											c.summoningMonsterId, o.getId(),
//											c.getId());
//									c.getSummonedNPC().facePlayer(o.getId());
									NPCHandler.attackPlayer(o,
											NPCHandler.npcs[c.summoningMonsterId], null);
									c.getSummonedNPC().setKillerId(o.getId());
									actionTimer = 7;
								} else {
									actionTimer--;
								}
					}
				}

				if (!attacked && c.inMulti()
							&& c.getSummonedNPC().isAttackingAPerson) {
						attacked = true;
						final Client o = (Client) PlayerHandler.players[c.playerIndex];
						if (o != null && o.wildLevel > 0 && o.inMulti() && MeleeRequirements.checkCombatDifference(c, o, false)) {
								if (actionTimer == 0) {
//									NPCHandler.followPlayer(
//											c.summoningMonsterId, o.getId(),
//											c.getId());
//									c.getSummonedNPC().facePlayer(o.getId());
									NPCHandler.attackPlayer(o,
											NPCHandler.npcs[c.summoningMonsterId], null);
									c.getSummonedNPC().setKillerId(o.getId());
									actionTimer = 7;
								} else {
									actionTimer--;
								}
					}
				}

				if (!attacked && c.inMulti() && c.getSummonedNPC().IsAttackingNPC) {
						attacked = true;
						final NPC n = NPCHandler.npcs[c.npcIndex];
						if (n != null) {
							// if(n.inMulti()) {
							c.getSummonedNPC().IsAttackingNPC = true;
							n.IsUnderAttackNpc = true;
							n.randomWalk = false;
							// Server.npcHandler.attackNPC(c.npcIndex,
							// c.summoningMonsterId, c);
							if (!c.summoned.isPokemon) {
								NPCHandler.NpcVersusNpc(c.summoningMonsterId,
										c.npcIndex, c);
							} else {
								c.summoned.killNpc = n.npcIndex;
							}
							// }
						}
				}
			}
			
			if (summonedFamiliar != null && summonedFamiliar.speakText != null) {
				speakTimer++;
				if (speakTimer == 100) {
					c.summoned.forceChat(summonedFamiliar.speakText);
					speakTimer = 0;
				}
			}
			c.familiarTimer -= 1;
			int hours = (c.familiarTimer / 2) / 3600;
			int minutes = ((c.familiarTimer / 2) - hours * 3600) / 60;
			int seconds = ((c.familiarTimer / 2)
					- (hours * 3600 + minutes * 60));

			String timer = minutes + ":" + seconds;
			c.getPacketSender().sendString(timer, 17021);
			if (c.familiarTimer == 100) {
				c.sendMessage(
						"<col=ff0000>Your familiar will run out in approximately 1 minute.");
				c.sendMessage(
						"<col=ff0000>Warning! Item's stored in familiar will be dropped upon death!");
			} else if (c.familiarTimer == 50) {
				c.sendMessage(
						"<col=ff0000>Your familiar will run out in approximately 30 seconds.");
				c.sendMessage(
						"<col=ff0000>Warning! Item's stored in familiar will be dropped upon death!");
			} else if (c.familiarTimer == 25) {
				c.sendMessage(
						"<col=ff0000>Your familiar will run out in approximately 15 seconds.");
				c.sendMessage(
						"<col=ff> You can renew your familiar with a new pouch by clicking <col=ff0000> Renew pouch");
				c.sendMessage(
						"<col=ff0000>Warning! Item's stored in familiar will be dropped upon death!");
			} else if (c.familiarTimer <= 0) {
				dismissFamiliar(false);
			}
			if (getFamiliarSpecialEnergy() != 60) {
				specialRestoreCycle++;
				if (specialRestoreCycle == 50) {
					if (getFamiliarSpecialEnergy() != 60) {
						setFamiliarSpecialEnergy(getFamiliarSpecialEnergy() + 15);
						if (getFamiliarSpecialEnergy() >= 60) {
							setFamiliarSpecialEnergy(60);
						}
						c.sendMessage("<col=ff0000>Summon special energy at "
								+ getFamiliarSpecialEnergy());
					}
					specialRestoreCycle = 0;

				}
			}

			if (specialTimer > 0) {
				specialTimer--;
			}
		}
		if (renewTimer > 0) {
			renewTimer--;
			if (renewTimer == 0 && c.summoned == null
					&& summonedFamiliar != null) {
				c.summoned = NPCHandler.summonNPCforSummoning(c, summonedFamiliar.npcId,
						c.getCurrentLocation().getX(),
						c.getCurrentLocation().getY()
								+ (summonedFamiliar.large ? 2 : 1),
						c.getHeightLevel(), 0,
						SummoningData.getSummonHealth(summonedFamiliar.npcId));
				callFamiliar();
			}
		}
		if (loginCycle > 0) {
			loginCycle--;
			if (loginCycle == 0 && c.summoned == null
					&& summonedFamiliar != null) {
				c.summoned = NPCHandler.summonNPCforSummoning(c, summonedFamiliar.npcId,
						c.getCurrentLocation().getX(),
						c.getCurrentLocation().getY()
								+ (summonedFamiliar.large ? 2 : 1),
						c.getHeightLevel(), 0,
						SummoningData.getSummonHealth(summonedFamiliar.npcId));
				callFamiliar();
			}
		}
	}

	/**
	 * Gets the count of the specified item.
	 *
	 * @return the amount of contained items.
	 */
	public int getItemCount(int itemId) {
		if (summonedFamiliar == null) {
			return 0;
		}

		int count = 0;
		if (summonedFamiliar.familiarType == FamiliarType.BOB) {
			for (int burdenedItem : c.burdenedItems) {
				if ((burdenedItem - 1) == itemId) {
					count++;
				}
			}
		}

		return count;
	}

	/**
	 * Gets the next slot for the specified itemId.
	 *
	 * @param itemId
	 *            - The specified item id.
	 */
	public int getSlotForId(int itemId) {
		if (summonedFamiliar.familiarType == FamiliarType.BOB) {
			for (int i = 0; i < summonedFamiliar.storeCapacity; i++) {
				if ((c.burdenedItems[i] + 1) == itemId) {
					return i;
				}
			}
		}
		return -1;
	}

	private static final int[] buttons = { 61049, 154081 };
	
	public boolean handleButtonClick(int buttonId) {
		
		for (int button : buttons) {
			if (buttonId == button) {
				if (summonedFamiliar == null) {
					c.sendMessage("You don't have a familiar.");
					return true;
				}
				if (!bobWithSpec(summonedFamiliar.npcId)) {
					if (c.getSummoning().summonedFamiliar == Familiar.Pack_Yak
							|| c.getSummoning().summonedFamiliar == Familiar.Spirit_Terrorbird
							|| c.getSummoning().summonedFamiliar == Familiar.War_Tortoise
							|| c.getSummoning().summonedFamiliar == Familiar.Thorny_Snail
							|| c.getSummoning().summonedFamiliar == Familiar.Bull_Ant
							|| c.getSummoning().summonedFamiliar == Familiar.Abyssal_Parasite
							|| c.getSummoning().summonedFamiliar == Familiar.Abyssal_Titan) {
						if (buttonId == 61049
								|| buttonId == 154081/*
														 * || buttonId == 66122
														 */) {
							handleLeftClick();
							return false;
						}
					}
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Handles the left click option.
	 */
	public void handleLeftClick() {
		if (summonedFamiliar != null && c.summoned != null) {
			if (summonedFamiliar.familiarType == FamiliarType.BOB) {
				for (int burdenedItem : c.burdenedItems) {
					if (c.getItems().freeSlots() > 0) {
						withdrawItem(burdenedItem - 1, 1);
					}
				}
			} else {
				useSpecial(c);
			}
		} else {
			// c.getPA().sendSummOrbDetails(false, ""); //TODO: add orb stuff
		}
	}

	/**
	 * Checks if the specific item is allowed to be deposited.
	 *
	 * @return returns if the item is allowed.
	 */
	public boolean itemIsAllowed(int itemId) {
		switch (itemId) {
			case ResourceArea.PACKAGE_ITEM_ID:
				return false;
		}

		return true;
	}

	/**
	 * Opens the beast of burden.
	 */
	public void openBoB() {
		// c.sendMessage("Beat of burden not available yet.");
		// return;
		if (c.inDuel) {
			return;
		}
		if (c.inTrade) {
			return;
		}
		if (c.isDead()) {
			return;
		}
		if (c.isBanking) {
			return;
		}
		if (c.isBobOpen) {
			return;
		}
		if (c.underAttackBy > 0 || c.underAttackBy2 > 0 || c.playerIndex > 0
				|| c.npcIndex > 0) {
			return;
		}
		if (c.inMinigame()/* || c.wildLevel > 0 */) {
			c.sendMessage("You cannot access BOB here.");
			return;
		}
		if (summonedFamiliar != null && c.summoned != null) {
			if (summonedFamiliar.familiarType == FamiliarType.BOB) {
				c.isBobOpen = true;
				shift();
				c.getItems().resetItems(39502/* 24002 */, c.burdenedItems);
				c.getItems().resetItems(5064/* 24006 */);
				c.getItems().resetTempItems();
				c.getPacketSender().sendFrame248(39500, 5063);
			}
		}
	}

	/**
	 * Renews a familiar before death.
	 */
	public void renewFamiliar() {
		if (summonedFamiliar != null && this.summonedFamiliar != null) {
			if (c.familiarTimer < 300) {
				if (c.getItems().playerHasItem(summonedFamiliar.pouchId, 1)) {
					c.familiarTimer = (((summonedFamiliar.timeLimit * 60)
							* 1000) / 600);
					c.sendMessage(
							"You sacrifice a pouch to renew your familiar's strength.");
					c.getItems().deleteItemInOneSlot(summonedFamiliar.pouchId,
							1);
				} else {
					c.sendMessage("You need a pouch to sacrifice!");
				}
			} else {
				c.sendMessage(
						"You still have excess time left on your current familiar!");
			}
		}
	}

	/**
	 * Shifts and re-arranges the familiar's inventory.
	 */
	public void shift() {
		// int totalItems = 0;
		int highestSlot = 0;
		for (int i = 0; i < summonedFamiliar.storeCapacity; i++) {
			if (c.burdenedItems[i] != 0) {
				// totalItems++;
				if (highestSlot <= i) {
					highestSlot = i;
				}
			}
		}
		for (int i = 0; i <= highestSlot; i++) {
			if (c.burdenedItems[i] == 0) {
				boolean stop = false;
				for (int k = i; k <= highestSlot; k++) {
					if (c.burdenedItems[k] != 0 && !stop) {
						int spots = k - i;
						for (int j = k; j <= highestSlot; j++) {
							c.burdenedItems[j - spots] = c.burdenedItems[j];
							stop = true;
							c.burdenedItems[j] = 0;
						}
					}
				}
			}
		}
	}

	/**
	 * Summons a familiar based on pouch id
	 *
	 * @param itemId
	 *            - Pouch Id from ActionButton packet.
	 */
	public boolean summonFamiliar(int itemId, boolean login) {
		
		if (!c.getBarbarianAssault().canUseSummoning()) {			
			c.sendMessage("You can't use summoning here.");
			return false;
		}		
		
		final Familiar summonedFamiliar = Familiar.forPouchId(itemId);
		if (summonedFamiliar == null) {
			return false;
		}
		Creation.Data d = Creation.Data.forPouchId(itemId);
		if (c.getPlayerLevel()[Skills.SUMMONING] < d
				.getLevelRequired()) {
			c.sendMessage("You need a summoning level of at least "
					+ d.getLevelRequired() + " to summon this familiar");
			return false;
		}
		if (c.inNomadsRoom()) {
			c.sendMessage(
					"You Cannot summon any familiars inside Nomads Room.");
			return false;
		}
		if (c.clanWarRule[CWRules.NO_SUMMONING]) {
			c.sendMessage("Summoning is disabled in this war.");
			return false;
		}
		if (c.duelStatus > 0) {
			c.sendMessage("You Cannot summon any familiars inside duel arena.");
			return false;
		}
		if (c.infernoManager != null || c.raidsManager != null) {
			c.sendMessage("You can't do this here.");
			return false;
		}
		if (c.summoned != null && c.summoned.isPetNpc()) {
			c.sendMessage(
					"You can't have a familiar with a pet following you.");
			return false;
		}
		
		if (this.summonedFamiliar != null || c.summoned != null) {
			c.sendMessage("You already have a familiar!");
			return false;
		}

		// if(c.currentClanBattle != null)
		// {
		// if(!c.currentClanBattle.summoning)
		// {
		// c.sendMessage("Summoning has been disabled in this war");
		// return;
		// }
		// }
		// int summonMPoints = 7;
		c.summonedCanTeleport = true;
		if (this.summonedFamiliar == null) {
			this.summonedFamiliar = summonedFamiliar;
			// c.getPA().sendSummOrbDetails(true,
			// summonedFamiliar.clientTooltip);
			String familiarName = "" + summonedFamiliar.toString() + "";
			familiarName = familiarName.replace('_', ' ');
			c.familiarName = familiarName;
			if (!login) {
				c.burdenedItems = new int[30/*summonedFamiliar.storeCapacity*/];
				for (int i = 0; i < c.burdenedItems.length; i++) {
					c.burdenedItems[i] = 0;
				}
				c.familiarTimer = (((summonedFamiliar.timeLimit * 60) * 1000)
						/ 600);
				c.summoned = NPCHandler.summonNPCforSummoning(c, summonedFamiliar.npcId,
						c.getCurrentLocation().getX(),
						c.getCurrentLocation().getY()
								+ (summonedFamiliar.large ? 2 : 1),
						c.getHeightLevel(), 0,
						SummoningData.getSummonHealth(summonedFamiliar.npcId));
				c.familiarPouchId = itemId;
				c.summoned.isRunning = c.isRunning;
				callFamiliar();
				c.sendMessage("You summon a " + familiarName + ".");

				if (itemId == 12059) {
					c.getAchievement().spiritSpider();
				}
				if (itemId == 12009) {
					c.getAchievement().summonGraniteCrabs();
				}
				
				c.getPacketSender().sendString(Integer.toString(this.summonedFamiliar.specialEnergyConsumption), 17026); // familiar special cost requirement
				c.getPacketSender().sendString(familiarName, 17017);
				c.getPacketSender().setSidebarInterface(15, 17011);
				c.getPacketSender().drawNpcOnInterface(summonedFamiliar.npcId, 17027);
				// c.summoningPoints -= summonMPoints;
				c.refreshSkill(23);
				c.getItems().deleteItemInOneSlot(itemId, 1);
			} else {
				c.summoned = NPCHandler.summonNPCforSummoning(c, summonedFamiliar.npcId,
						c.getCurrentLocation().getX(),
						c.getCurrentLocation().getY()
								+ (summonedFamiliar.large ? 2 : 1),
						c.getHeightLevel(), 0,
						SummoningData.getSummonHealth(summonedFamiliar.npcId));
				loginCycle = 4;
			}
			return true;
		}
		return false;
	}

	/**
	 * Executes the familiar's special using the specified arguments (for
	 * compatibility)
	 */
	public void useSpecial(Object... arguments) {
		if (summonedFamiliar != null && c.summoned != null) {
			if (summonedFamiliar.familiarSpecial != null && specialTimer < 1
					&& !c.summoned.isUsingSummonigSpecial) {
				// if (!DBConfig.USE_SCROLLS) {
				// summonedFamiliar.familiarSpecial.execute(arguments);
				// c.sendMessage("Your summon has
				// "+c.getSummoning().familiarSpecialEnergy+" energy left.");
				// }
				if (c.getItems().playerHasItem(summonedFamiliar.scrollId)) {
					if (summonedFamiliar.familiarType
							.equals(FamiliarType.COMBAT)) {
						c.summoned.isUsingSummonigSpecial = true;
					}
					summonedFamiliar.familiarSpecial.execute(arguments);
					c.sendMessage("Your summon has "
							+ c.getSummoning().getFamiliarSpecialEnergy()
							+ " energy left.");
				} else {
					c.sendMessage(
							"You must have the required scroll to do this special.");
				}
			} else {
				c.sendMessage("You must wait before casting this again.");
			}
		}
	}

	public void winterStorage(int itemToBank, int itemToBankSlot) {
		if (c.inMinigame() || c.duelStatus > 0 || c.tradeStatus > 0 || c.isBanking || c.isBobOpen
				|| !c.correctlyInitialized) {
			return;
		}

		if (summonedFamiliar == null || c.summoned == null || summonedFamiliar.familiarType != FamiliarType.BOB) {
			return;
		}

		if (c.wildLevel >= 30) {
			c.sendMessage("Magical force stops you from banking this item.");
			return;
		}
		
		if (c.wildLevel > 0 && c.isInCombatDelay()) {
			c.sendMessage("You must be outside of combat in order to use Winter Storage in wild.");
			return;
		}

		switch (itemToBank) {
			case ResourceArea.PACKAGE_ITEM_ID:
				if (c.wildLevel > 0) {
					c.sendMessage("You can't do this inside wilderness.");
					return;
				}

				break;
		}

		if (handleSpecialMoveCost(15)) {
			if (!c.getItems().deleteItem2(12435, 1)) {
				return;
			}

			final int bankAmount = 1;// c.getItems().getItemAmountInSlot(itemToBankSlot);
			if (!c.getItems().deleteItemInOneSlot(itemToBank, itemToBankSlot, bankAmount)) {
				return;
			}

			if (c.getItems().addItemToBank(itemToBank, bankAmount)) {
				c.sendMessage("Your pack yack has sent the item to your bank.");
				c.startGraphic(Graphic.create(1316, GraphicType.HIGH));
				c.startAnimation(7660);
			}
		}
	}

	/**
	 * Withdraws an item from the familiar's inventory, to the owner's
	 * inventory.
	 *
	 * @param id
	 *            - Item id
	 * @param amount
	 *            - Item amount
	 */
	public void withdrawItem(int id, int amount) {
		// if (c.inWild() && c.wildLevel <= 30) {
		// c.sendMessage("You can't use This option if your below 30
		// wilderness.");// this sends a message for each item need to fix
		// return;
		// }
		if (summonedFamiliar != null && c.summoned != null) {
			if (summonedFamiliar.familiarType == FamiliarType.BOB) {
				if (amount > 0 && id > 0) {
					int slot = getSlotForId(id + 2);
					while (amount > 0 && slot != -1) {
						if (c.getItems().addItem(c.burdenedItems[slot] - 1, 1)) {
							c.burdenedItems[slot] = 0;
							slot = getSlotForId(id + 2);
							amount--;
						} else {
							break;
						}
					}
				}
				c.getItems().resetItems(5064);
				c.getItems().resetItems(39502, c.burdenedItems);
				c.startAnimation(827);// does the animation everytime even if the npc doesn't have items in it
			}
		}
	}

	public int getFamiliarSpecialEnergy() {
		return familiarSpecialEnergy;
	}

	public void setFamiliarSpecialEnergy(int familiarSpecialEnergy) {
		this.familiarSpecialEnergy = familiarSpecialEnergy;
		if (c.correctlyInitialized) {
			//TODO: add special bar visuals
		}
	}

	public boolean handleSpecialMoveCost(int required) {
		boolean reduce = false;
		switch(c.playerEquipment[PlayerConstants.playerCape]) {
			case 19893:
				reduce = true;
				break;
			case CompletionistCape.NORMAL_CAPE:
				if (Variables.SPIRIT_CAPE_ADDED_TO_COMP.getValue(c) == 1) {
					reduce = true;
				}
				break;
		}

		if (reduce) {
			//Multiply by 10 so you get more accurate results
			int newPts = required * 10;
			//Cape saves 20%
			newPts = newPts - newPts * 20 / 100;
			//Go back to small value
			required = newPts / 10;
		}

		if (familiarSpecialEnergy >= required) {
			familiarSpecialEnergy -= required;

			if (reduce) {
				c.sendMessageSpam("Your spirit cape reduces the cost of your familiar's special move.");
			}
			return true;
		}

		return false;
	}

}
