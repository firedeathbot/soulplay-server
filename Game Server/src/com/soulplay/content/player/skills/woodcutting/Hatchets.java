package com.soulplay.content.player.skills.woodcutting;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

public enum Hatchets {
	BRONZE_AXE(1351, 1, 879, 1),
	IRON_AXE(1349, 1, 877, 6),
	STEEL_AXE(1353, 6, 875, 11),
	BLACK_AXE(1361, 6, 873, 21),
	MITHRIL_AXE(1355, 21, 871, 31),
	ADAMANT_AXE(1357, 31, 869, 41),
	RUNE_AXE(1359, 41, 867, 51),
	DRAGON_AXE(6739, 61, 2846, 61),
	INFERNO_ADZE(13661, 61, 10251, 66);

	private final Animation anim, startAnim;
	private final int itemId, lvlReq, addonRate;

	private Hatchets(int item, int lvl, int anim, int addonRate) {
		this.itemId = item;
		this.lvlReq = lvl;
		this.anim = new Animation(anim);
		this.startAnim = new Animation(anim, 30);
		this.addonRate = addonRate;
	}

	public int getItemId() {
		return itemId;
	}

	public int getLvlReq() {
		return lvlReq;
	}

	public Animation getAnim() {
		return anim;
	}

	public Animation getStartAnim() {
		return startAnim;
	}

	public int getRate() {
		return addonRate;
	}

	private static final Map<Integer, Hatchets> map = new HashMap<>();

	static {
		for (Hatchets axe : values()) {
			map.put(axe.getItemId(), axe);
		}
	}
	
    public static void load() {
    	/* empty */
    }

	public static Hatchets getAxe(int itemId) {
		return map.get(itemId);
	}

}
