package com.soulplay.content.player.skills.woodcutting;

import java.util.HashMap;
import java.util.Map;

public enum Trees {

	REGULAR(1511, 1, 25, 40, 10, 317647, 0, 60, 264, new int[] { 1276, 1277, 1278, 1279, 1280, 1282, 1283, 1284, 1285, 1286,
			1289, 1290, 1291, 1318, 1319, 1315,
			1316, 1330, 1331, 1332, 1383, 1384, 2409, 3033,
			3034, 3035, 3036, 3879, 3881, 3883, 5902, 5903, 5904, 10041, 14308, 14309, 16264,
			16265, 101319 }),
	OAK(1521, 15, 38, 60, 15, 361146, 10, 0, 256, new int[] { 1281, 3037, 8462, 8463, 8464, 8465, 8466, 8467, 10083,
					13413, 13420, 10820 }),
	WILLOW(1519, 30, 68, 90, 25, 289286, 10, 0, 120, new int[] { 1308, 5551, 5552, 5553, 8481, 8482, 8483, 8484, 8485, 8486,
					8487, 8488, 8496, 8497, 8498, 8499, 8500, 8501, 13414, 13421, 10833, 10831, 10829, 10819 }),
	MAPLE(1517, 45, 100, 135, 25, 221918, 10, 0, 120, new int[] { 1307, 4674, 8435, 8436, 8437, 8438, 8439, 8440, 8441, 8442,
					8443, 8444, 8454, 8455, 8456, 8457, 8458, 8459, 8460, 8461,
					13415, 13423, 10832 }),
	YEW(1515, 60, 175, 202, 50, 145013, 10, 0, 60, new int[] { 1309, 8503, 8504, 8505, 8506, 8507, 8508, 8509, 8510, 8511,
					8512, 8513, 13416, 13422, 101753, 10822, 110822 }),
	MAGIC(1513, 75, 250, 303, 100, 72321, 10, 0, 8, new int[] { 1306, 8396, 8397, 8398, 8399, 8400, 8401, 8402, 8403, 8404,
					8405, 8406, 8407, 8408, 8409, 13417, 13424, 101761, 10834, 110834 }),
	MAHOGANY(6332, 50, 125, 157, 50, 220623, 10, 0, 100, new int[] { 9034 }),
	TEAK(6333, 35, 85, 105, 14, 264336, 7, 0, 129, new int[] { 9036 }), //rs rates with dragon axe is 60-190 rate lvl 1 - lvl 99 / fall/destroy rate is 1/8
	IVY(-1, 68, 332, -1, 50, -1, 10, 0, 100, new int[] { 46318, 46320, 46322, 46324 }),
	REDWOOD(119669, 90, 380, 415, 0, 220623, 10, 0, 100, new int[] { 29670, 29668 });

	public static final Map<Integer, Trees> map = new HashMap<>();
	private final int logId, lvlReq, exp, adzeXp, respawnTime, clueNestRate;
	private final int destroyRate, lowRate, highRate;

	private final int[] treeIds;

	private Trees(int logId, int lvl, int exp, int adzeXp, int respawnTime, int clueNestRate, int destroyRate, int lowRate, int highRate, int[] ids) {
		this.logId = logId;
		this.lvlReq = lvl;
		this.exp = exp;
		this.adzeXp = adzeXp;
		this.respawnTime = respawnTime;
		this.clueNestRate = clueNestRate;
		this.treeIds = ids;
		this.destroyRate = destroyRate;
		this.lowRate = lowRate;
		this.highRate = highRate;
	}

	public int getLogId() {
		return logId;
	}

	public int getLvlReq() {
		return lvlReq;
	}

	public int getExp() {
		return exp;
	}

	public int getAdzeXp() {
		return adzeXp;
	}

	public int getClueNestRate() {
		return clueNestRate;
	}

	public int getRespawnTime() {
		return respawnTime;
	}

	public int getDestroyRate() {
		return destroyRate;
	}
	
	public int getLowRate() {
		return lowRate;
	}

	public int getHighRate() {
		return highRate;
	}
	
	public static final Trees[] VALUES = values();

	static {
		for (Trees tree : Trees.VALUES) {
			for (int id : tree.treeIds) {
				map.put(id, tree);
			}
		}
	}

    public static void load() {
    	/* empty */
    }

	public static Trees getTree(int id) {
		return map.get(id);
	}

}
