package com.soulplay.content.player.skills.hunterlesik;

import java.util.EnumSet;
import java.util.Optional;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

public enum HunterTrapType {
	
	BIRD_SNARE(10006, 1, 9345, 9344, 9348),

	BOX_TRAP(10008, 27, 9380, 9385, 9384),

	RABBIT_SNARE(10031, 27, 19333, 19334, 19335),

	MAGIC_BOX(10025, 71, 19223, 19224, 19226),

	FALCON(-1, -1, -1, -1, -1)

	;

	/**
	 * The id
	 */
	private final int id;

	/**
	 * The level required
	 */
	private final int levelRequired;

	/**
	 * The waiting object id
	 */
	private final int waitingObject;

	/**
	 * The failed object id
	 */
	private final int failedObject;

	/**
	 * The success object id
	 */
	private final int successObject;

	/**
	 * Represents a trap
	 *
	 * @param id the id
	 * @param levelRequired the level required
	 * @param waitingObject the waiting object
	 * @param failedObject the failed object
	 * @param successObject the sucess object
	 */
	HunterTrapType(int id, int levelRequired, int waitingObject, int failedObject,
			int successObject) {
		this.id = id;
		this.levelRequired = levelRequired;
		this.waitingObject = waitingObject;
		this.failedObject = failedObject;
		this.successObject = successObject;
	}

	/**
	 * Sets the id
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the levelRequired
	 *
	 * @return the levelRequired
	 */
	public int getLevelRequired() {
		return levelRequired;
	}

	/**
	 * Sets the waitingObject
	 *
	 * @return the waitingObject
	 */
	public int getWaitingObject() {
		return waitingObject;
	}

	/**
	 * Sets the failedObject
	 *
	 * @return the failedObject
	 */
	public int getFailedObject() {
		return failedObject;
	}

	/**
	 * Sets the successObject
	 *
	 * @return the successObject
	 */
	public int getSuccessObject() {
		return successObject;
	}

	/**
	 * The values
	 */
	private static final ImmutableSet<HunterTrapType> VALUES =
			Sets.immutableEnumSet(EnumSet.allOf(HunterTrapType.class));

	/**
	 * Gets the trap
	 * 
	 * @param item the item
	 * @return the trap
	 */
	public static Optional<HunterTrapType> forId(int item) {
		return VALUES.stream().filter(source -> source.getId() == item).findAny();
	}

	/**
	 * Gets the trap
	 * 
	 * @param item the item
	 * @return the trap
	 */
	public static Optional<HunterTrapType> forDismatle(int item) {
		return VALUES
				.stream().filter(source -> source.getWaitingObject() == item
						|| source.getSuccessObject() == item || source.getFailedObject() == item)
				.findAny();
	}

	/**
	 * Gets the trap
	 * 
	 * @param item the item
	 * @return the trap
	 */
	public static Optional<HunterTrapType> forSuccess(int item) {
		return VALUES.stream().filter(source -> source.getSuccessObject() == item).findAny();
	}
}
