package com.soulplay.content.player.skills.hunterlesik.box;

public enum BoxTrapEnum {
		//north east  south   west
	GRAY(19193, 19194, 19195, 19196, BoxTrapObject.GRAY_TRAPPED_OBJECT_ID),
	RED(19197, 19198, 19199, 19200, BoxTrapObject.RED_TRAPPED_OBJECT_ID);
	
	private final int[] dir;
	
	private final int trappedId;
	
	private BoxTrapEnum(int north, int east, int south, int west, int trappedId) {
		dir = new int[] { north, east, south, west };
		this.trappedId = trappedId;
	}

	public int[] getObjectByDir() {
		return dir;
	}
	
	public int getTrappedId() {
		return trappedId;
	}

	public static final BoxTrapEnum[] VALUES = values();
}
