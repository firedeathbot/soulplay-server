package com.soulplay.content.player.skills.hunterlesik;

import java.util.HashMap;
import java.util.Map;

public class TrapCounter {

	private static Map<Integer, Integer> map = new HashMap<>();
	
	public static int getTrapCount(int mysqlIndex) {
		return map.getOrDefault(mysqlIndex, 0);
	}
	
	public static void increaseTrapCount(int mysqlIndex) {
		int count = getTrapCount(mysqlIndex);
		map.put(mysqlIndex, count + 1);
	}
	
	public static void decreaseTrapCount(int mysqlIndex) {
		int count = getTrapCount(mysqlIndex);
		count -= 1;
		if (count <= 0)
			map.remove(mysqlIndex);
		else
			map.put(mysqlIndex, count);
	}
	
	public static void resetMap() {
		map.clear();
	}
}
