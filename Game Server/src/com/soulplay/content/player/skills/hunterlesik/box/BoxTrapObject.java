package com.soulplay.content.player.skills.hunterlesik.box;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.player.achievement.Achievs;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.hunterlesik.HunterConcurrentTrap;
import com.soulplay.content.player.skills.hunterlesik.HunterPrey;
import com.soulplay.content.player.skills.hunterlesik.HunterTrapType;
import com.soulplay.content.player.skills.hunterlesik.TrapCounter;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

public class BoxTrapObject extends RSObject {

	public static final int OBJECT_ID = 19187;
	public static final int BROKEN_OBJECT_ID = 19192;
	
	public static final int BROKEN_OBJECT_WITH_ANIM = 19188;
	
	public static final int RED_TRAPPED_OBJECT_ID = 19190;
	public static final int GRAY_TRAPPED_OBJECT_ID = 19189;
	
	public static final Animation LAY_ANIM = new Animation(5208);
	public static final Animation PICKUP_ANIM = new Animation(5212);
	
	private int trapTicks = 0;
	
	private int playerLevel;
	
	private HunterPrey prey = HunterPrey.GREY_CHINCHOMPA;
	
	public boolean trapFailed = false;
	
	public boolean trapInUse = false;
	
	public int playerMID;
	
	public boolean broken() {
		return getNewObjectId() > 19187;
	}
	
	public BoxTrapObject(int x, int y, int height, long playerLongName, int playerLevel, int playerMID) {
		super(19187, x, y, height, 0, 10);
		setTick(100); // 100 - 1 minute
		setObjectOwnerLong(playerLongName);
		this.playerLevel = playerLevel;
		this.playerMID = playerMID;
	}

	@Override
	public void process() {
		super.process();
		
		trapTicks++;
		if (getTick() == 0) {
			if (!trapFailed) {
				ItemHandler.createGroundItem(Misc.longToPlayerName2(getObjectOwnerLong()), HunterTrapType.BOX_TRAP.getId(), getX(), getY(), getHeight(), 1, false, false);
			}
			TrapCounter.decreaseTrapCount(playerMID);
		}
	}
	
	public static void setupTrap(Player p, int itemId, int itemSlot) {
		
		p.getMovement().resetWalkingQueue();
		
		if (ObjectManager.objectInSpotTypeTen(p.getX(), p.getY(), p.getZ())){
			p.sendMessage("You cannot place that here.");
			return;
		}
		
		int count = TrapCounter.getTrapCount(p.mySQLIndex);
		
		if (maxTraps(p, count)) {
			p.sendMessage("You have maxed out the amount of box traps you can setup.");
			return;
		}
		
		//drop item
		if (itemSlot != -1) {
			Item item = new Item(itemId, p.playerItemsN[itemSlot], itemSlot);
			FifthItemAction.DROP_ITEM.init((Client)p, item);
		}
		
		p.startAnimation(LAY_ANIM);
		
		final int x = p.getX();
		final int y = p.getY();
		final int z = p.getZ();
		
		p.startAction(new CycleEvent() {
			int cycle = 0;
			@Override
			public void stop() {
				p.skillingEvent = null;
				//i put it into stop to give players faster time XD
				if (!ObjectManager.objectInSpotTypeTen(x, y, z)) {
					GroundItem item = ItemHandler.getGroundItemOfOwner(p.getName(), itemId, x, y, z);
					if (item != null) {
						ItemHandler.removeGlobalItem(item);
						createTrapObject(p, x, y, z);
						p.startAnimation(Animation.RESET);
					}
				}
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				cycle++;
				
				if (cycle >= 4) {
					container.stop();
				}
			}
		}, 1);
	}
	
	public static void rearmBrokenTrap(Player p, GameObject o) {
		RSObject rsO = ObjectManager.getObjectByNewId(o.getId(), o.getX(), o.getY(), o.getZ());
		if (rsO != null && rsO.isActive()) {
			if (rsO.getObjectOwnerLong() != p.getLongName()) {
				p.sendMessage("That is not your trap.");
				return;
			}
			
//			int count = TrapCounter.getTrapCount(p.mySQLIndex);
//			
//			if (maxTraps(p, count)) {
//				p.sendMessage("You have maxed out the amount of box traps you can setup.");
//				return;
//			}
			
			p.startAnimation(LAY_ANIM);
			
			p.startAction(new CycleEvent() {
				
				@Override
				public void stop() {
					p.skillingEvent = null;
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					if (canBeRemoved(rsO, o.getId())) {
						rsO.setTick(0);
						rsO.setUpdateRequired(false);
						p.sendMessageSpam("You fix the trap.");
						createTrapObject(p, o.getX(), o.getY(), o.getZ());
						TrapCounter.decreaseTrapCount(p.mySQLIndex);
					}
				}
			}, 2, false);
			
		}
	}
	
	public static void dismantleTrap(Player player, GameObject o) {
		
		RSObject rsO = ObjectManager.getObjectByNewId(o.getId(), o.getX(), o.getY(), o.getZ());
		
		if (rsO == null || !rsO.isActive())
			return;
		
		if (rsO.getObjectOwnerLong() != player.getLongName()) {
			player.sendMessage("That is not your trap.");
			return;
		}
		
		player.startAnimation(PICKUP_ANIM);
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (canBeRemoved(rsO, o.getId())) {
					rsO.setTick(0);
					rsO.setIsActive(false);
					player.getItems().addOrDrop(new GameItem(HunterTrapType.BOX_TRAP.getId(), 1));
					player.getPacketSender().addObjectPacket(-1, rsO.getX(), rsO.getY(), rsO.getType(), rsO.getFace());
					TrapCounter.decreaseTrapCount(player.mySQLIndex);
				}
			}
		}, 2, false);
	}
	
	private static void createTrapObject(Player p, int x, int y, int z) {
		BoxTrapObject box = new BoxTrapObject(x, y, z, p.getLongName(), p.getSkills().getLevel(Skills.HUNTER), p.mySQLIndex);
		ObjectManager.addObject(box);
		TrapCounter.increaseTrapCount(p.mySQLIndex);
	}
	
	public static void collectChinchompa(Player p, GameObject o) {
		RSObject rsO = ObjectManager.getObjectByNewId(o.getId(), o.getX(), o.getY(), o.getZ());
		
		if (rsO == null || !rsO.isActive())
			return;
		
		if (rsO.getObjectOwnerLong() != p.getLongName()) {
			p.sendMessage("That is not your trap.");
			return;
		}
		
		BoxTrapObject box = (BoxTrapObject) rsO;
		
		final HunterPrey prey = box.getPrey();
		
		int reward = prey.getReward()[0].getId();
		
		if (p.getItems().freeSlots() < 1 && !p.getItems().playerHasItem(reward)) {
			p.sendMessage("You don't have enough room in your inventory.");
			return;
		}
		
		p.startAnimation(PICKUP_ANIM);
		
		p.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				p.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (canBeRemoved(rsO, o.getId())) {
					rsO.setTick(0);
					rsO.setIsActive(false);
					
					p.getItems().addOrDrop(new GameItem(HunterTrapType.BOX_TRAP.getId(), 1));
					p.getItems().addItem(reward, 1);
					p.getPA().addSkillXP(prey.getExperienceReceived(), Skills.HUNTER);
					p.getPacketSender().addObjectPacket(-1, rsO.getX(), rsO.getY(), rsO.getType(), rsO.getFace());
					TrapCounter.decreaseTrapCount(p.mySQLIndex);
					
					if (prey == HunterPrey.RED_CHINCHOMPA)
						p.getAchievement().increaseProgress(Achievs.CATCH_75_CHINS);
				}
			}
		}, 2, false);
	}

	public int getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(int playerLevel) {
		this.playerLevel = playerLevel;
	}

	public HunterPrey getPrey() {
		return prey;
	}

	public void setPrey(HunterPrey prey) {
		this.prey = prey;
	}
	
	public static boolean maxTraps(Player p, int count) {
		int boost = 0;
		if (p.getDonatedTotalAmount() >= 500)
			boost = 1;
		return count >= HunterConcurrentTrap.forLevel(p.getSkills().getLevel(Skills.HUNTER)).getAmount() + boost;
	}
	
	public static boolean canBeRemoved(RSObject rsO, int clickedId) {
		return rsO.isActive() && rsO.getTick() > 0 && rsO.getNewObjectId() == clickedId;
	}
}
