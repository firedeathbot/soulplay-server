package com.soulplay.content.player.skills.hunterlesik;

import java.util.HashMap;
import java.util.Map;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.world.entity.Animation;

public enum HunterPrey {

	CRIMSON_SWIFT(5549, 1, 39, HunterTrapType.BIRD_SNARE, new Item[] {}, new Animation(5171),
			new Animation(5172), new Animation(5173)),

	GOLDEN_WARBLER(5551, 5, 47, HunterTrapType.BIRD_SNARE, new Item[] {}, new Animation(5171),
			new Animation(5172), new Animation(5173)),

	COPPER_LONGTAIL(5552, 9, 61, HunterTrapType.BIRD_SNARE, new Item[] {}, new Animation(5171),
			new Animation(5172), new Animation(5173)),

	CERULEAN_TWITCH(5550, 11, 64, HunterTrapType.BIRD_SNARE, new Item[] {}, new Animation(5171),
			new Animation(5172), new Animation(5173)),

	TROPICAL_WAGTAIL(5548, 19, 95, HunterTrapType.BIRD_SNARE, new Item[] {}, new Animation(5171),
			new Animation(5172), new Animation(5173)),

	FERRET(1505, 27, 155, HunterTrapType.BOX_TRAP, new Item[] {new Item(10092)}),

	GREY_CHINCHOMPA(5079, 53, 159, HunterTrapType.BOX_TRAP, new Item[] {new Item(10033)}),

	RED_CHINCHOMPA(5080, 63, 265, HunterTrapType.BOX_TRAP, new Item[] {new Item(10034)}),

	BLACK_CHINCHOMPA(3606, 73, 315, HunterTrapType.BOX_TRAP, new Item[] {new Item(30356)}),

	WHITE_RABIT(1852, 27, 144, HunterTrapType.RABBIT_SNARE, new Item[] {}),

	LIME_RABBIT(1853, 54, 180, HunterTrapType.RABBIT_SNARE, new Item[] {}),

	DAD_RABBIT(3420, 74, 380, HunterTrapType.RABBIT_SNARE, new Item[] {}),

	KID_RABBIT(3421, 34, 210, HunterTrapType.RABBIT_SNARE, new Item[] {}),

	MOTHER_RABBIT(3422, 64, 290, HunterTrapType.RABBIT_SNARE, new Item[] {}),

	IMP(3134, 71, 450, HunterTrapType.MAGIC_BOX, new Item[] {new Item(10027)});

	/**
	 * The id
	 */
	private final int id;

	/**
	 * The level required
	 */
	private final int levelRequired;

	/**
	 * The experience received;
	 */
	private final int experienceReceived;

	/**
	 * The trap
	 */
	private final HunterTrapType trappedBy;

	/**
	 * The reward
	 */
	private final Item[] reward;

	/**
	 * The check animation
	 */
	private final Animation check;

	/**
	 * The failed animation
	 */
	private final Animation failed;

	/**
	 * The success animation
	 */
	private final Animation success;

	/**
	 * Represents a hunter prey
	 * 
	 * @param id the id
	 * @param levelRequired the level required
	 * @param experienceReceived the experience received
	 * @param trappedBy the trap
	 * @param reward the reward
	 * @param check the check anim
	 * @param failed the failed anim
	 * @param success the success anim
	 */
	HunterPrey(int id, int levelRequired, int experienceReceived, HunterTrapType trappedBy,
			Item[] reward, Animation check, Animation failed, Animation success) {
		this.id = id;
		this.levelRequired = levelRequired;
		this.experienceReceived = experienceReceived;
		this.trappedBy = trappedBy;
		this.reward = reward;
		this.check = check;
		this.failed = failed;
		this.success = success;
	}

	/**
	 * Represents a hunter prey
	 * 
	 * @param id the id
	 * @param levelRequired the level required
	 * @param experienceReceived the experience received
	 * @param trappedBy the trap
	 * @param reward the reward
	 */
	HunterPrey(int id, int levelRequired, int experienceReceived, HunterTrapType trappedBy,
			Item[] reward) {
		this(id, levelRequired, experienceReceived, trappedBy, reward, null, null, null);
	}

	/**
	 * The values
	 */
	private static Map<Integer, HunterPrey> map = new HashMap<>();
	
	private static Map<Integer, HunterPrey> objMap = new HashMap<>();
	
	static {
		for (HunterPrey prey : HunterPrey.values()) {
			map.put(prey.getId(), prey);
			objMap.put(prey.getTrappedBy().getSuccessObject(), prey);
		}
	}

	/**
	 * Gets the prey
	 * 
	 * @param id the id
	 * @return the prey
	 */
	public static HunterPrey forId(int id) {
		HunterPrey prey = map.get(id);
		if (prey == null)
			return HunterPrey.GREY_CHINCHOMPA;
		return prey;
	}

	/**
	 * Gets the prey
	 * 
	 * @param id the id
	 * @return the prey
	 */
	public static HunterPrey forObject(int id) {
		HunterPrey prey = objMap.get(id);
		if (prey == null)
			return HunterPrey.GREY_CHINCHOMPA;
		return prey;
	}

	/**
	 * Sets the id
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the levelRequired
	 *
	 * @return the levelRequired
	 */
	public int getLevelRequired() {
		return levelRequired;
	}

	/**
	 * Sets the experienceReceived
	 *
	 * @return the experienceReceived
	 */
	public int getExperienceReceived() {
		return experienceReceived;
	}

	/**
	 * Sets the trappedBy
	 *
	 * @return the trappedBy
	 */
	public HunterTrapType getTrappedBy() {
		return trappedBy;
	}

	/**
	 * Sets the reward
	 *
	 * @return the reward
	 */
	public Item[] getReward() {
		return reward;
	}


	/**
	 * Sets the check
	 *
	 * @return the check
	 */
	public Animation getCheck() {
		return check;
	}

	/**
	 * Sets the failed
	 *
	 * @return the failed
	 */
	public Animation getFailed() {
		return failed;
	}

	/**
	 * Sets the success
	 *
	 * @return the success
	 */
	public Animation getSuccess() {
		return success;
	}

}
