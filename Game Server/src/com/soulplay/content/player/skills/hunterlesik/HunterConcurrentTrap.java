package com.soulplay.content.player.skills.hunterlesik;

import java.util.EnumSet;
import java.util.Optional;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public enum HunterConcurrentTrap {

	FIVE(80, 5),

	FOUR(60, 4),

	THREE(40, 3),

	TWO(20, 2),

	ONE(1, 1);

	/**
	 * The level
	 */
	private final int levelRequired;

	/**
	 * The amount
	 */
	private final int amount;

	/**
	 * Represets a trap
	 *
	 * @param levelRequired the levelRequired
	 * @param amount the amount
	 */
	HunterConcurrentTrap(int levelRequired, int amount) {
		this.levelRequired = levelRequired;
		this.amount = amount;
	}

	/**
	 * Sets the level
	 *
	 * @return the level
	 */
	public int getLevelRequired() {
		return levelRequired;
	}

	/**
	 * Sets the amount
	 *
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	private static final ImmutableSet<HunterConcurrentTrap> VALUES =
			Sets.immutableEnumSet(EnumSet.allOf(HunterConcurrentTrap.class));
	
	public static HunterConcurrentTrap forLevel(int lvl) {
		for (HunterConcurrentTrap trap : VALUES) {
			if (lvl >= trap.getLevelRequired())
				return trap;
		}
		return ONE;
	}
	
}