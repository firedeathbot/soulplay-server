package com.soulplay.content.player.skills;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public class SkillHandler {

	// public static final int SKILLING_XP = 40;
	// public static final int AGILITY_XP = SKILLING_XP;
	// public static final int PRAYER_XP = SKILLING_XP;
	// public static final int MINING_XP = SKILLING_XP;
	// public static final int COOKING_XP = SKILLING_XP;
	// public static final int RUNECRAFTING_XP = SKILLING_XP;
	// public static final int WOODCUTTING_XP = SKILLING_XP;
	// public static final int THIEVING_XP = SKILLING_XP;
	// public static final int HERBLORE_XP = SKILLING_XP;
	// public static final int FISHING_XP = SKILLING_XP;
	// public static final int FLETCHING_XP = SKILLING_XP;
	// public static final int SUMMONING_XP = SKILLING_XP;
	// public static final int FIREMAKING_XP = SKILLING_XP;
	// public static boolean[] isSkilling = new boolean[25];

	public static void deleteTime(Player c) {
		c.doAmount--;
	}

	public static String getLine() {
		return "\\n\\n\\n\\n\\n";
	}

	public static boolean hasRequiredLevel(final Player c, int id, int lvlReq,
			String skill, String event) {
		if (c.getSkills().getLevel(id) < lvlReq) {
			c.sendMessage("You haven't got high enough " + skill + " level to "
					+ event + "");
			c.sendMessage("You at least need the " + skill + " level of "
					+ lvlReq + ".");
			c.getPA().sendStatement("You haven't got high enough " + skill
					+ " level to " + event + "!");
			return false;
		}
		return true;
	}

	public static boolean hasInventorySpace(Player c, String skill) {
		if (c.getItems().freeSlots() == 0) {
			String message = "You don't have enough inventory space to continue " + skill + "!";
			c.sendMessage(message);
			c.getDialogueBuilder().sendStatement(message).execute();
			return false;
		}

		return true;
	}

	public static void resetPlayerSkillVariables(Client c) {
		for (int i = 0; i < c.playerSkilling.length; i++) {
			if (c.playerSkilling[i]) {
				for (int l = 0; l < 15; l++) {
					c.playerSkillProp[i][l] = -1;
				}
			}
		}
	}
}
