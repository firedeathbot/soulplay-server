package com.soulplay.content.player.skills.fishing;

public class FishingDetails {
	
	public Fish fish;
	public Fish parentFish;
	public int delay = 0;
	
	public FishingDetails(Fish f, Fish pF) {
		this.fish = f;
		this.parentFish = pF;
	}

}
