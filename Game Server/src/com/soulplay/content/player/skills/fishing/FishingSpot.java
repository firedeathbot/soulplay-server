package com.soulplay.content.player.skills.fishing;

/**
 * Represents a fishing spot.
 *
 * @author Emperor
 */
public enum FishingSpot {

	ANGLERFISH(new int[] { 1177 }, FishingOption.ANGLERFISH), //TODO: maybe add the item?
	//SACRED_EEL(new int[] { 8741 }, FishingOption.SACRED_EEL),
	DARK_CRAB(new int[] { 1176 }, FishingOption.DARK_CRAB), 
	NET_BAIT(new int[] { 952, 316, 319, 320, 323, 325, 326, 327, 330, 332, 404, 1331, 2067, 2068, 2724, 4908, 5748, 5749, 7045 }, FishingOption.SMALL_NET, FishingOption.BAIT), 
	LURE_BAIT(new int[] { 309, 310, 311, 314, 315, 317, 318, 328, 329, 331, 403, 927, 1189, 1190, 3019 }, FishingOption.LURE, FishingOption.L_BAIT), 
	CAGE_HARPOON(new int[] { 312, 321, 324, 333, 405, 1332, 1399, 3804, 5470, 7046 }, FishingOption.CAGE, FishingOption.HARPOON),
	NET_HARPOON(new int[] { 313, 322, 334, 406, 1191, 1333, 1405, 1406, 3574, 3575, 5471, 7044 }, FishingOption.BIG_NET, FishingOption.N_HARPOON),
	HARPOON_NET(new int[] { 3848, 3849 }, FishingOption.HARPOON, FishingOption.H_NET, FishingOption.BARB_HARPOON, FishingOption.D_HARPOON, FishingOption.INFERNAL_HARPOON),
	INFERNAL_EEL(new int[] {107676 }, FishingOption.INFERNAL_EEL),
	KARAMBWANJI(new int[] {1174 }, FishingOption.KARAMBWANJI_SMALL_NET),
    KARAMBWAN(new int[] {1178 }, FishingOption.KARAMBWAN_VESSEL),
	BARB_FISH(new int[] {101516 }, FishingOption.BARB_FISH_LURE),
	ROCKTAIL_SHOAL(new int[] {8842 }, FishingOption.BAIT_ROCKTAIL),
	;
	
	public static final FishingSpot[] values = values();
	
	/**
	 * Gets the FishingSpot for the given NPC id.
	 *
	 * @param npcId The npc id.
	 * @return The fishing spot.
	 */
	public static FishingSpot forId(int npcId) {
		for (FishingSpot spot : FishingSpot.values) {
			for (int id : spot.ids) {
				if (id == npcId) {
					return spot;
				}
			}
		}
		return null;
	}
	
	/**
	 * The NPC ids.
	 */
	private final int[] ids;
	
	/**
	 * The fishing options.
	 */
	private FishingOption[] options;
	
	/**
	 * Constructs a new {@code FishingSpot} {@code Object}.
	 *
	 * @param ids     The NPC ids.
	 * @param options The fishing option.
	 */
	FishingSpot(int[] ids, FishingOption... options) {
		this.ids = ids;
		this.options = options;
	}
	
	/**
	 * Gets the ids.
	 *
	 * @return The ids.
	 */
	public int[] getIds() {
		return ids;
	}
	
	/**
	 * Gets the options.
	 *
	 * @return The options.
	 */
	public FishingOption[] getOptions() {
		return options;
	}
	
}