package com.soulplay.content.player.skills.fishing;

import com.soulplay.game.model.item.Item;

/**
 * Represents a type of (raw) fish to catch.
 *
 * @author Emperor
 * @author Jire
 */
public enum Fish {
	
	BARB_SHARK(new Item(383), 96, 76, -1, 80, 8, 0, null),
	BARB_SWORDFISH(new Item(371), 70, 50, -1, 100, 10, 0, null),
	BARB_TUNA(new Item(359), 55, 35, -1, 110, 11, 0, null),
	SACRED_EEL(new Item(15151), 87, -1, -1, 105, 0, 0, null),
	ANGLERFISH(new Item(113439), 82, -1, -1, 120, 0, 0, null),
	DARK_CRAB(new Item(15015), 85, -1, -1, 130, 0, 0, null),
	SHRIMP(new Item(317), 1, -1, -1, 10, 0, 0, null),
	SARDINE(new Item(327), 5, -1, -1, 20, 0, 0, null),
	KARAMBWANJI(new Item(3150), 5, -1, -1, 5, 0, 0, null),
	HERRING(new Item(345), 10, -1, -1, 30, 0, 0, null),
	ANCHOVIE(new Item(321), 15, -1, -1, 40, 0, 0, null),
	MACKEREL(new Item(353), 16, -1, -1, 20, 0, 0, null),
	TROUT(new Item(335), 20, -1, -1, 50, 0, 0, null),
	COD(new Item(341), 23, -1, -1, 45, 0, 0, null),
	PIKE(new Item(349), 25, -1, -1, 60, 0, 0, null),
	SLIMY_EEL(new Item(3379), 28, -1, -1, 65, 0, 0, null),
	SALMON(new Item(331), 30, -1, -1, 70, 0, 0, null),
	FROG_SPAWN(new Item(5004), 33, -1, -1, 75, 0, 0, null),
	TUNA(new Item(359), 35, -1, -1, 80, 0, 0, BARB_TUNA),
	RAINBOW_FISH(new Item(10138), 38, -1, -1, 80, 0, 0, null),
	CAVE_EEL(new Item(5001), 38, -1, -1, 80, 0, 0, null),
	LOBSTER(new Item(377), 40, -1, -1, 90, 0, 0, null),
	BASS(new Item(363), 46, -1, -1, 100, 0, 0, null),
	SWORDFISH(new Item(371), 50, -1, -1, 100, 0, 0, BARB_SWORDFISH),
	LAVA_EEL(new Item(2148), 53, -1, -1, 100, 0, 0, null),
	MONKFISH(new Item(7944), 62, -1, -1, 120, 0, 0, null),
	KARAMBWAN(new Item(3142), 65, -1, -1, 105, 0, 0, null),
	SHARK(new Item(383), 76, -1, -1, 110, 0, 0, BARB_SHARK),
	SEA_TURTLE(new Item(395), 79, -1, -1, 38, 0, 0, null),
	MANTA_RAY(new Item(389), 81, -1, -1, 200, 0, 0, null),
	SEAWEED(new Item(401), 16, -1, -1, 1, 0, 0, null),
	CASKET(new Item(405), 16, -1, -1, 10, 0, 0, null),
	OYSTER(new Item(407), 16, -1, -1, 10, 0, 0, null),
	INFERNAL_EEL(new Item(121293), 80, -1, -1, 95, 0, 0, null),
	LEAPING_TROUT(new Item(11328), 48, 15, 15, 50, 5, 5, null),
	LEAPING_SALMON(new Item(11330), 58, 30, 30, 70, 6, 6, null),
	LEAPING_STURGEON(new Item(11332), 70, 45, 45, 80, 7, 7, null),
	TIGER_SHARK(new Item(21520), 95, -1, -1, 80, 0, 0, null),
	ROCKTAIL(new Item(15270), 90, -1, -1, 380, 0, 0, null),
	
	//Fishing Trawler Junk Rewards:
	BUTTONS(688, 1, -1, -1, 0, 0, 0, null),
	EDIBLE_SEAWEED(403, 16, -1, -1, 1, 0, 0, null),
	BROKEN_ARROW(687, 1, -1, -1, 0, 0, 0, null),
	BROKEN_GLASS(690, 1, -1, -1, 0, 0, 0, null),
	DAMAGED_STAFF(689, 1, -1, -1, 0, 0, 0, null),
	DAMAGED_ARMOUR(697, 1, -1, -1, 0, 0, 0, null),
	EMPTY_POT(1931, 1, -1, -1, 0, 0, 0, null),
	RUSTY_SWORD(686, 1, -1, -1, 0, 0, 0, null),
	OLD_BOOT(685, 1, -1, -1, 0, 0, 0, null);
	
	Fish(int itemID, int level, int strengthLevel, int agilityLevel, double experience, double strengthExp, double agilityExp, Fish barbFish) {
		this(new Item(itemID), level, strengthLevel, agilityLevel, experience, strengthExp, agilityExp, barbFish);
	}
	
	/**
	 * Constructs a new {@code Fish} {@code Object}.
	 *
	 * @param item       the <code>Item</code>
	 * @param level      the level.
	 * @param experience the experience.
	 * @param barbFish
	 */
	Fish(final Item item, final int level, final int strengthLevel, final int agilityLevel, final double experience, final double strengthExp, final double agilityExp, Fish barbFish) {
		this.item = item;
		this.level = level;
		this.experience = experience;
		this.strengthExp = strengthExp;
		this.agilityExp = agilityExp;
		this.strengthLevel = strengthLevel;
		this.agilityLevel = agilityLevel;
		this.barbFish = barbFish;
	}
	
	private final Fish barbFish;
	
	/**
	 * Represents the {@link Item} of this <code>Fish</code>.
	 */
	private final Item item;
	
	/**
	 * Represents the required fishing level to catch the {@link Fish}.
	 */
	private final int level;
	
	/**
	 * Represents the required strength level to catch the {@link Fish}.
	 */
	private final int strengthLevel;
	
	/**
	 * Represents the required agility level to catch the {@link Fish}.
	 */
	private final int agilityLevel;
	
	/**
	 * Represents the fishing experience gained from this fish.
	 */
	private final double experience;
	
	/**
	 * Represents the strength experience gained from this fish.
	 */
	private final double strengthExp;
	
	/**
	 * Represents the agility experience gained from this fish.
	 */
	private final double agilityExp;
	
	/**
	 * Gets the fish.
	 *
	 * @param item the item.
	 * @return the fash.
	 */
	public static Fish forItem(Item item) {
		for (Fish fish : values) {
			if (fish.getItem().getId() == item.getId()) {
				return fish;
			}
		}
		return null;
	}
	
	public Fish getBarbFish() {
		return barbFish;
	}
	
	/**
	 * @return the item.
	 */
	public Item getItem() {
		return item;
	}
	
	/**
	 * @return the fishing level.
	 */
	public int getLevel() {
		return level;
	}
	
	/**
	 * @return the strength level.
	 */
	public int getStrengthLevel() {
		return strengthLevel;
	}
	
	/**
	 * @return the agility level.
	 */
	public int getAgilityLevel() {
		return agilityLevel;
	}
	
	/**
	 * @return the fishing experience.
	 */
	public double getExperience() {
		return experience;
	}
	
	/**
	 * @return the strength experience.
	 */
	public double getStrengthExperience() {
		return strengthExp;
	}
	
	/**
	 * @return the agility experience.
	 */
	public double getAgilityExperience() {
		return agilityExp;
	}
	
	public static final Fish[] values = values();
	
}
