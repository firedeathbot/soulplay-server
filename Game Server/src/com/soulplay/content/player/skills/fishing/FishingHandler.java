package com.soulplay.content.player.skills.fishing;

import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.FishingContest;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

public class FishingHandler {

	public static void startFishing(final Player player, final NPC npc, final FishingOption option) {
		Fish f = option.getRandomFish(player);
		Fish pf = f;
		
		final FishingDetails details = new FishingDetails(f, pf);
		
		if (!checkRequirements(player, npc, option, details.fish, details.parentFish)) {
			return;
		}
		
		message(player, option, details.fish, 0);
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.resetAnimations();
				player.startAnimation(Animation.RESET);
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (details.delay == 0)
					animate(player, option, details.fish);
			
				details.delay++;
				
				if (!checkRequirements(player, npc, option, details.fish, details.parentFish)) {
					container.stop();
					return;
				}
				
				if (details.delay == 5) {
					details.delay = 1;
					animate(player, option, details.fish);
					if (!reward(player, npc, option, details)) {
						container.stop();
						return;
					}
				}
				
			}
		}, 1);
		
	}
	
	private static boolean reward(Player player, NPC npc, FishingOption option, FishingDetails details) {
		Fish fish = details.fish;
		
//		if (npc.getId() == 333 && player.getZoneMonitor().isInZone("karamja")
//				&& player.getLocation().withinDistanceOffset(Location.create(2924, 3178, 0), 10)
//				&& !player.getAchievementDiaryManager().hasCompletedTask(DiaryType.KARAMJA, 0, 6)) {
//			player.getAchievementDiaryManager().updateTask(player, DiaryType.KARAMJA, 0, 6, true);
//		}
//		if (player.getFamiliarManager().hasFamiliar() && player.getFamiliarManager().getFamiliar() instanceof Forager) {
//			final Forager forager = (Forager) player.getFamiliarManager().getFamiliar();
//			forager.handlePassiveAction();
//		}
		
		boolean clearKarambwanji = fish == Fish.KARAMBWAN && player.getItems().deleteItem2(3150, 1);
		
		if (success(player, option, fish)) {
			if (player.getItems().freeSlots() < fish.getItem().getAmount()
					|| option.getBait() == null
					|| player.getItems().deleteItem2(option.getBait().getId(), option.getBait().getAmount())) {
				if (fish == Fish.KARAMBWAN) {
					
					if (clearKarambwanji) {
					} else if (player.getItems().deleteItem2(3159, 1)) {
						player.getItems().addItem(3157, 1);
					} else {
						player.getDialogueBuilder().reset();
						player.getDialogueBuilder()
						.sendStatement("Your karambwan vessel has ran out of karambwanji.")
						.execute();
						return false;
					}
					
//					Item vessel = null;
//					for (Item item : player.getInventory().getItems()) {
//						if (item != null && item.getId() == KarambwanVesselPlugin.FILLED_VESSEL_ID) {
//							vessel = item;
//							break;
//						}
//					}
//					if (vessel == null || vessel.getCharge() < 1) {
//						start();
//						player.sendMessage("Your karambwan vessel has ran out of karambwanji.");
//						player.sendDialogue("Your karambwan vessel has ran out of karambwanji.");
//						return false;
//					}
//					int chargesUsed;
//					int fishLevel = player.getSkills().getLevel(Skills.FISHING);
//					if (fishLevel < 80) {
//						chargesUsed = RandomFunction.randomBetween(2, 3);
//					} else if (fishLevel < 90) {
//						chargesUsed = RandomFunction.randomBetween(1, 3);
//					} else if (fishLevel < 95) {
//						chargesUsed = RandomFunction.randomBetween(1, 2);
//					} else {
//						chargesUsed = RandomFunction.randomBoolean(2) ? RandomFunction.randomBetween(1, 2) : 1;
//					}
//					int newCharges = vessel.getCharge() - chargesUsed;
//					if (newCharges < 1) {
//						stop();
//						if (player.getInventory().remove(vessel)
//								&& player.getInventory().add(new Item(KarambwanVesselPlugin.VESSEL_ID, 1))) {
//							player.sendMessage("Your karambwan vessel has ran out of karambwanji.");
//							player.sendDialogue("Your karambwan vessel has ran out of karambwanji.");
//						}
//						return true;
//					}
//					vessel.setCharge(newCharges);
//					player.sendMessage("Your karambwan vessel has " + newCharges + " karambwanji remaining.");
				}
				
//				if (fish == Fish.TROUT && player.getLocation().withinDistanceOffset(Location.create(3105, 3429, 0))
//						& !player.getAchievementDiaryManager().getDiary(DiaryType.VARROCK).isComplete(0, 11)) {
//					player.getAchievementDiaryManager().getDiary(DiaryType.VARROCK).updateTask(player, 0, 11, true);
//				}
				
				boolean perk = false;//player.getDetails().getShop().hasPerk(Perks.MASTER_FISHERMAN) && RandomFunction.random(100) < 10;
				
//				if (AdminEvent.getCurrent() == AdminEvent.DOUBLE_RESOURCES)
//					perk = true;
				
				int cookedFish = 0;
				if (hasInfernalHarpoon(player, option)) {
					if (Misc.random(100) <= 33 /*&& infernalHarpoon.getCharge() > 0*/) {
						RawAndCooked f = RawAndCooked.getByRawId(fish.getItem().getId());
						if (f != null) {
							cookedFish = f.getCookedId();
							player.sendMessageSpam("Your special harpoon cooks the " + (perk ? "two fish" : "fish") + " for full Cooking experience.");
							player.getPA().addSkillXP(f.getExp(), Skills.COOKING);
//							if (infernalHarpoon.getCharge() == 0) {
//								stop();
//								infernalHarpoon.setId(16488);
//								player.animate(Animation.RESET);
//								player.getDialogueInterpreter().sendItemMessage(infernalHarpoon,
//										"Your harpoon has run out of charges and must be re-charged to work again.");
//							}
						}
					}
				}
				int amount = perk ? 2 : 1;
				if (fish == Fish.KARAMBWANJI) {
					//1+floor(level/5)
					int extra = (int) Math.floor(player.getSkills().getLevel(Skills.FISHING) / 5.0);
					if (extra > 0) {
						amount += extra;
					}
				}

				double fishXp = fish.getExperience();
				
				if (fishXp < 0)
					fishXp = 0;

				double xp = fishXp * calculateAnglerOutfitXpModifier(player);
				if (player.inWild()) {
					xp *= 2.0;
				}
				if (perk)
					if (player.inWild())
						xp *= 1.5;
					else
						xp *= 2.0;
				
				switch (fish) {
				case SHARK:
					player.getAchievement().fishSharks();
					break;
				case MANTA_RAY:
					player.getAchievement().fishManta();
					break;
				case ROCKTAIL:
					player.getAchievement().catchRocktails();
					break;
				case TIGER_SHARK:
					if (Misc.randomBoolean(150 - player.getSkills().getLevel(Skills.FISHING)))
						cookedFish = 20429;
					break;
				default:
					break;
				}

				player.getItems().addItem(cookedFish > 0 ? cookedFish : fish.getItem().getId(), amount);
				
				player.getAchievement().fishFishes();
				SkillingUrns.handleUrn(player, Skills.FISHING, fish.getLevel(), (int)xp);
				player.increaseProgress(TaskType.FISHING, fish.getItem().getId());
				
				if (option.getClueRate() > 0)
					player.getTreasureTrailManager().rollClueThingy(fish.getLevel(), option.getClueRate(), Skills.FISHING, "You catch a clue bottle!");
				
				if (xp > 0)
					player.getPA().addSkillXP((int) xp, Skills.FISHING);
				
				if (fish.getStrengthExperience() > 0)
					player.getPA().addSkillXP((int)fish.getStrengthExperience(), Skills.STRENGTH);
				if (fish.getAgilityExperience() > 0)
					player.getPA().addSkillXP((int)fish.getAgilityExperience(), Skills.AGILITY);
				
				if (Misc.random(100) == 7 && !player.inFishingContestIsland()) {
					if (!npc.isDead())
						npc.setDead(true);
				}
				
				if (player.inFishingContestIsland()) {
					if (FishingContest.isRunning()) {
						if (Misc.random(8000) == 1) {
							player.getItems().addOrBankItem(6202, 1);
							FishingContest
							.endFishingContest();
							PlayerHandler.messageAllPlayers(
									"<img=9> <shad=1><col=FF9933>"
											+ player.getNameSmartUp()
											+ " has caught the rare fish and won the fishing contest.!");
							player.getDialogueBuilder().reset();
							player.getDialogueBuilder().sendStatement("You have caught the Rare fish for the fishing contest!").execute();
						} else if (Misc.random(300) == 1
								&& player.getDonatedTotalAmount() > 9) {
							player.getItems().addItem(405, 1);
							player.sendMessage("You found a strange casket.");
						} else if (Misc.random(1000) == 1) {
							player.getItems().addItem(405, 1);
							player.sendMessage("You found a strange casket.");
						}
					} else {
						player.sendMessage("The rare fish has already been caught in this fishing contest.");
					}
				}
					
				if (cookedFish == 0) {
					if (!message(player, option, fish, perk ? 3 : 2))
						return false;
				}
				
//				SkillPetChances.award(player, Skills.FISHING, resource);
//				if (TutorialSession.getExtension(player).getStage() == 13) {
//					TutorialStage.load(player, 14, false);
//					stop();
//					return true;
//				}
				details.fish = option.getRandomFish(player);
			}
			
//			if (player.getItems().freeSlots() >= 1 && Misc.randomBoolean(1000)) {
//				player.getInventory().add(new Item(RandomFunction.random(15177, 15181), 1));
//				player.sendMessage("You pull a bottle out of the water.");
//			}
			
//			if (Misc.random(250) == 9 && fish == Fish.ANGLERFISH) {
//				if (player.getItems().freeSlots() >= 1) {
//					player.getInventory().add(new Item(9722, 1));
//					player.sendMessage("You pull a key out of the lava... it appears to still be usable.");
//				}
//			}
			
//			if (player.getZoneMonitor().isInZone("ResourceArena")) {
//				if (fish == Fish.DARK_CRAB) {
//					player.getAchievementDiaryManager().getDiary(DiaryType.WILDERNESS).updateTask(player, 2, 6, true);
//				}
//			}
		}
		return player.getItems().freeSlots() != 0;
	}
	
	private static boolean checkRequirements(final Player player, final NPC npc, final FishingOption option, Fish fish, Fish parentFish) {

		if (npc.isDead() || !npc.isActive() || npc.needRespawn) {
//			stop();
			return false;
		}
		
		boolean canBarbFish = false;
		if (fish != parentFish.getBarbFish() && !player.getItems().playerHasItem(option.getTool().getId())
				&& !hasBarbTail(player, option) && hasDragonHarpoon(player, option) == 2) {
			if (fish.getBarbFish() != null && player.getSkills().getLevel(Skills.FISHING) >= fish.getLevel()) {
				canBarbFish = true;
			} else {
				player.getDialogueBuilder().reset();
				player.getDialogueBuilder().sendStatement("You need a " + option.getTool().getName().toLowerCase() + " to catch these fish.").execute();
				//stop();//TODO stop task
				return false;
			}
		}
		
		if (canBarbFish) {
			fish = parentFish.getBarbFish();
		}
		
		if (option.getBait() != null && !player.getItems().playerHasItem(option.getBait().getId())) {
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement(
					"You don't have any " + option.getBait().getName() + " left.").execute();
//			stop();
			return false;
		}
		if (player.getSkills().getLevel(Skills.FISHING) < fish.getLevel()) {
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement("You need a fishing level of "
					+ fish.getLevel() + " to catch " + (fish == Fish.SHRIMP || fish == Fish.ANCHOVIE ? "" : "a")
					+ " " + fish.getItem().getName().toLowerCase() + ".".trim()).execute();
//			stop();
			return false;
		}
		if (player.getSkills().getLevel(Skills.STRENGTH) < fish.getStrengthLevel() || player.getSkills().getLevel(Skills.AGILITY) < fish.getAgilityLevel()) {
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement("You need a strength level of " + fish.getAgilityLevel() + " and a agility level of "
					+ fish.getAgilityLevel() + " to ", "catch " + (fish == Fish.SHRIMP || fish == Fish.ANCHOVIE ? "" : "a")
					+ " " + fish.getItem().getName().toLowerCase() + ".".trim()).execute();
//			stop();
			return false;
		}
		if (player.getItems().freeSlots() == 0) {
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement("You don't have enough space in your inventory.").execute();
//			stop();
			return false;
		}
		if (fish == Fish.INFERNAL_EEL && !player.getItems().playerHasEquipped(1580, PlayerConstants.playerHands)) {
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement(
					"You'd burn your hands if you tried fishing",
					"infernal eels without some form of protection!").execute();
			return false;
		}
		
		
		return true;
	}
	
	private static void animate(Player player, FishingOption option, Fish fish) {
		Animation anim = option.getAnimation();
		if ((option == FishingOption.HARPOON
				|| option == FishingOption.N_HARPOON)
				&& hasDragonHarpoon(player, option) != 2) {
			anim = (hasDragonHarpoon(player, option) == 0
					? FishingOption.D_HARPOON.getAnimation()
					: FishingOption.INFERNAL_HARPOON.getAnimation());
		}
		
		if (fish == Fish.BARB_SHARK) {
			anim = Animation.create(6706);
		} else if (fish == Fish.BARB_SWORDFISH || fish == Fish.BARB_TUNA) {
			anim = Animation.create(6707);
		}
		
		player.startAnimation(anim);
	}
	
	private static boolean hasInfernalHarpoon(Player player, FishingOption option) {
			return player.getItems().playerHasItem(FishingOption.INFERNAL_HARPOON.getTool().getId())
					|| player.getItems().hasEquippedWeapon(FishingOption.INFERNAL_HARPOON.getTool().getId());
	}
	
	private static int hasDragonHarpoon(Player player, FishingOption option) {
		if (option == FishingOption.HARPOON || option == FishingOption.N_HARPOON) {
			if ((player.getItems().playerHasItem(FishingOption.D_HARPOON.getTool().getId())
					|| player.getItems().hasEquippedWeapon(FishingOption.D_HARPOON.getTool().getId()))
					&& player.getSkills().getStaticLevel(Skills.FISHING) >= FishingOption.D_HARPOON.getLevel()) {
				return 0;
			}
			if ((player.getItems().playerHasItem(FishingOption.INFERNAL_HARPOON.getTool().getId())
					|| player.getItems().hasEquippedWeapon(FishingOption.INFERNAL_HARPOON.getTool().getId()))
					&& player.getSkills().getStaticLevel(Skills.FISHING) >= FishingOption.INFERNAL_HARPOON.getLevel()) {
				return 1;
			}
		}
		return 2;
	}
	
	private static boolean hasBarbTail(Player player, FishingOption option) {
		if (option == FishingOption.HARPOON || option == FishingOption.N_HARPOON) {
			return player.getItems().playerHasItem(FishingOption.BARB_HARPOON.getTool().getId())
					|| player.getItems().hasEquippedWeapon(FishingOption.BARB_HARPOON.getTool().getId());
		}
		return false;
	}
	
	private static boolean message(Player player, FishingOption option, Fish fish, int type) {
		switch (type) {
			case 0:
				player.sendMessageSpam(option.getStartMessage());
				break;
			case 2:
			case 3:
			case 4:
				if (type != 4) {
					if (fish == Fish.KARAMBWANJI) {
						player.sendMessageSpam("You catch some karambwanji.");
					} else {
						player.sendMessageSpam(fish == Fish.ANCHOVIE || fish == Fish.SHRIMP
								? "You catch " + (type == 3 ? "two groups of" : "some") + " "
								+ fish.getItem().getName().toLowerCase().replace("raw", "").trim() + "."
								: "You catch " + (type == 3 ? "two" : fish == Fish.ANGLERFISH ? "an" : "a")
								+ " " + fish.getItem().getName().toLowerCase().replace("raw", "").trim()
								+ (type == 3 ? "s" : "").trim() + ".");
					}
				} else {
					player.sendMessageSpam("Your harpoon cooks your catch.");
				}
				if (player.getItems().freeSlots() == 0) {
					player.getDialogueBuilder().reset();
					player.getDialogueBuilder().sendStatement("You don't have enough space in your inventory.").execute();
//					stop();
					return false;
				}
				break;
		}
		return true;
	}
	
	private static boolean success(Player player, FishingOption option, Fish fish) {
//		if (getDelay() == 1) {
//			return false;
//		}
		int level = 1 + player.getSkills().getLevel(Skills.FISHING)
				/*+ player.getFamiliarManager().getBoost(Skills.FISHING)*/;
		double threshold = Misc.randomDouble(0, fish.getLevel());
		double roll = Misc.randomDouble(0, (level * 1.25 - fish.getLevel()));
		if (hasDragonHarpoon(player, option) != 2) {
			roll += (roll * 0.2);
		}
		return roll > threshold;
	}
	
	private static final int anglerHat = 30308;
	private static final int anglerWaders = 30310;
	private static final int anglerTop = 30309;
	private static final int anglerBoots = 30311;
	
	private static double calculateAnglerOutfitXpModifier(Player player) {
		double modifier = 1.0;

		if (player.getItems().playerHasEquipped(anglerHat, PlayerConstants.playerHat)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(anglerTop, PlayerConstants.playerChest)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(anglerWaders, PlayerConstants.playerLegs)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(anglerBoots, PlayerConstants.playerFeet)) {
			modifier += 0.05;
		}
		return modifier;
	}
	
}
