package com.soulplay.content.player.skills.fishing;

import java.util.HashMap;
import java.util.Map;

public enum RawAndCooked {

	SHRIMP(317, 315, 323, 30),
	ANCHOVIES(321, 319, 323, 30),
	SARDINE(327, 325, 369, 40),
	HERRING(345, 347, 357, 50),
	MACKAREL(353, 355, 357, 60),
	TROUT(335, 333, 343, 70),
	COD(341, 339, 343, 75),
	PIKE(349, 351, 343, 80),
	SAlMON(331, 329, 343, 90),
	TUNA(359, 361, 367, 100),
	BASS(363, 365, 367, 100),
	LOBSTER(377, 379, 381, 120),
	SWORDFISH(371, 373, 375, 140),
	KARAMBWAN(3142, 3144, 3146, 190),
	SHARK(383, 385, 387, 210),
	SEA_TURTLE(395, 397, 399, 212),
	MANTA(389, 391, 393, 216),
	MONKFISH(7944, 7946, 7948, 150),
	ROCKTAIL(15270, 15272, 15724, 225),
	;
	
	
	private final int rawId, cookedId, burntId, exp;
	
	private RawAndCooked(int rawId, int cookedId, int burntId, int exp) {
		this.rawId = rawId;
		this.cookedId = cookedId;
		this.burntId = burntId;
		this.exp = exp;
	}

	public int getRawId() {
		return rawId;
	}

	public int getCookedId() {
		return cookedId;
	}

	public int getBurntId() {
		return burntId;
	}

	public int getExp() {
		return exp;
	}
	
	private static final Map<Integer, RawAndCooked> rawMap = new HashMap<>();
	private static final Map<Integer, RawAndCooked> cookedMap = new HashMap<>();
	
	static {
		for (RawAndCooked fish : values()) {
			rawMap.put(fish.getRawId(), fish);
			cookedMap.put(fish.getCookedId(), fish);
		}
	}

	public static RawAndCooked getByRawId(int id) {
		return rawMap.get(id);
	}

}
