package com.soulplay.content.player.skills.fishing;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

/**
 * Represents a fishing option.
 * @author Emperor
 */
public enum FishingOption {

	ANGLERFISH(new Item(307), 82, Animation.create(622), new Item(113431), "fish", 0, Fish.ANGLERFISH),
	SACRED_EEL(new Item(307), 87, Animation.create(622), new Item(313), "bait", 0, Fish.SACRED_EEL),
	DARK_CRAB(new Item(301), 85, Animation.create(Animation.getOsrsAnimId(619)), new Item(15021), "fish", 0, Fish.DARK_CRAB), 
	SMALL_NET(new Item(303), 1, Animation.create(Animation.getOsrsAnimId(621)), null, "net", 870330, Fish.SHRIMP, Fish.ANCHOVIE), 
	BAIT(new Item(307), 5, Animation.create(622), new Item(313), "bait", 1056000, Fish.SARDINE, Fish.HERRING), 
	LURE(new Item(309), 20, Animation.create(622), new Item(314), "lure", 923616, Fish.TROUT, Fish.SALMON, Fish.RAINBOW_FISH), 
	L_BAIT(new Item(307), 25, Animation.create(622), new Item(313), "bait", 305792, Fish.PIKE), 
	CAGE(new Item(301), 40, Animation.create(Animation.getOsrsAnimId(619)), null, "cage", 116129, Fish.LOBSTER), 
	HARPOON(new Item(311), 35, Animation.create(618), null, "harpoon", 257770, Fish.TUNA, Fish.SWORDFISH),
	D_HARPOON(new Item(30069), 61, Animation.create(Animation.getOsrsAnimId(7401)), null, "harpoon", 257770, Fish.TUNA, Fish.SWORDFISH, Fish.SHARK),
	INFERNAL_HARPOON(new Item(30068), 75, Animation.create(Animation.getOsrsAnimId(7402)), null, "harpoon", 257770, Fish.TUNA, Fish.SWORDFISH, Fish.SHARK),
	BARB_HARPOON(new Item(10129), 35, Animation.create(618), null, "harpoon", 257770, Fish.TUNA, Fish.SWORDFISH),
	BIG_NET(new Item(305), 16, Animation.create(Animation.getOsrsAnimId(620)), null, "net", 1147827, Fish.MACKEREL, Fish.COD, Fish.BASS),
	N_HARPOON(new Item(311), 76, Animation.create(618), null, "harpoon", 82243, Fish.SHARK, Fish.MANTA_RAY),
	H_NET(new Item(303), 1, Animation.create(Animation.getOsrsAnimId(621)), null, "net", 138583, Fish.MONKFISH),
	INFERNAL_EEL(new Item(1585), 80, Animation.create(622), new Item(313), "bait", 0, Fish.INFERNAL_EEL),
    KARAMBWANJI_SMALL_NET(new Item(303), 5, Animation.create(Animation.getOsrsAnimId(621)), null, "net", 443697, Fish.KARAMBWANJI),
    KARAMBWAN_VESSEL(new Item(3159), 65, Animation.create(Animation.getOsrsAnimId(1193)), null, "fish", 170874, Fish.KARAMBWAN),
	BARB_FISH_LURE(new Item (11323), 48, Animation.create(622), new Item(314), "lure", 257770, Fish.LEAPING_TROUT, Fish.LEAPING_SALMON, Fish.LEAPING_STURGEON),
	BAIT_ROCKTAIL(new Item(307), 90, Animation.create(622), new Item(15263), "bait", 78649, Fish.ROCKTAIL);
	/**
	 * The tool required.
	 */
	private final Item tool;

	/**
	 * The fishing level required.
	 */
	private final int level;

	/**
	 * The fishing animation.
	 */
	private final Animation animation;

	/**
	 * The bait.
	 */
	private final Item bait;

	/**
	 * The option name.
	 */
	private final String name;
	
	private final int clueRate;

	/**
	 * The fish to catch.
	 */
	private final Fish[] fish;

	/**
	 * Constructs a new {@code FishingOption} {@code Object}.
	 * @param tool The tool needed.
	 * @param level The fishing level required.
	 * @param animation The animation.
	 * @param fish The fish to catch.
	 */
	private FishingOption(Item tool, int level, Animation animation, Item bait, String name, int clueRate, Fish... fish) {
		this.tool = tool;
		this.level = level;
		this.animation = animation;
		this.bait = bait;
		this.name = name;
		this.clueRate = clueRate;
		this.fish = fish;
	}

	/**
	 * Method used to get a random {@link Fish}.
	 * @return the {@link Fish}.
	 */
	public Fish getRandomFish(final Player player) {
		if (this == BIG_NET) {
			switch (Misc.random(100)) {
			case 0:
				return Fish.OYSTER;
			case 50:
				return Fish.CASKET;
			case 90:
				return Fish.SEAWEED;
			}
		}
		if (this == LURE && player.getItems().playerHasItem(10087, 1)) {
			return Fish.RAINBOW_FISH;
		}
		Fish reward = fish[Misc.randomNoPlus(fish.length)];
		if (reward.getLevel() > player.getSkills().getLevel(Skills.FISHING) || (reward == Fish.RAINBOW_FISH && !player.getItems().playerHasItem(100887, 1))) {
			reward = fish[0];
		}
		return reward;
	}

	/**
	 * Gets the start message.
	 * @return The start message.
	 */
	public String getStartMessage() {
		if (name.equals("net")) {
			return "You cast out your net...";
		}
		return "You attempt to catch a fish.";
	}

	/**
	 * Gets the tool.
	 * @return The tool.
	 */
	public Item getTool() {
		return tool;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the animation.
	 * @return The animation.
	 */
	public Animation getAnimation() {
		return animation;
	}

	/**
	 * Gets the bait.
	 * @return The bait.
	 */
	public Item getBait() {
		return bait;
	}

	/**
	 * Gets the name.
	 * @return The name.
	 */
	public String getName() {
		return name;
	}

	public int getClueRate() {
		return clueRate;
	}
	
	/**
	 * Gets the fish.
	 * @return The fish.
	 */
	public Fish[] getFish() {
		return fish;
	}

}