package com.soulplay.content.player.skills.dungeoneeringv2.puzzles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.PuzzleRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.VisibleRoom;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Location;

public class SlidingTilesRoom extends PuzzleRoom {

	private static final int[] BASE_TILE =
	{ 12125, 12133, 12141, 12149, 12963 };

	private static final int[][] TILE_COORDS =
	{
	{ 5, 9 },
	{ 7, 9 },
	{ 9, 9 },
	{ 5, 7 },
	{ 7, 7 },
	{ 9, 7 },
	{ 5, 5 },
	{ 7, 5 },
	{ 9, 5 }, };

	private static final int[][] VALID_MOVES =
	{
	{ 1, 3 },
	{ 0, 2, 4 },
	{ 1, 5 },
	{ 0, 4, 6 },
	{ 1, 3, 5, 7 },
	{ 2, 4, 8 },
	{ 3, 7 },
	{ 4, 6, 8 },
	{ 5, 7 } };

	private NPC[] tiles;
	private int freeIndex = 8;
	private int[] shuffledNpcOrder;
	private int[] solveOrder;
	private int solveIndex;

	@Override
	public void openRoom() {
		shuffle();
		tiles = new NPC[9];
		for (int i = 0; i < 9; i++) {
			if (shuffledNpcOrder[i] != 0) {
				int coordsX = DungeonManager.translateX(TILE_COORDS[i][0], TILE_COORDS[i][1], 0, 2, 2, 0);
				int coordsY = DungeonManager.translateY(TILE_COORDS[i][0], TILE_COORDS[i][1], 0, 2, 2, 0);
				Location base = manager.getRoomBaseTile(reference);
				tiles[i] = manager.spawnNPC(shuffledNpcOrder[i], Location.create(base.getX() + coordsX, base.getY() + coordsY, manager.height), reference, DungeonConstants.PUZZLE_NPC, false);
			}
		}
	}

	public void shuffle() {
		int type = manager.party.getFloorType();
		shuffledNpcOrder = new int[9];
		solveOrder = new int[8];
		for (int i = 0; i < 8; i++) {
			shuffledNpcOrder[i] = BASE_TILE[type] + i;
		}
		List<Integer> set = new ArrayList<Integer>();
		boolean[] used = new boolean[9];
		while (true) {
			for (int i = 0; i < VALID_MOVES[freeIndex].length; i++) {
				if (!used[VALID_MOVES[freeIndex][i]]) {
					set.add(VALID_MOVES[freeIndex][i]);
				}
			}
			if (set.isEmpty()) {
				break;
			}
			Collections.shuffle(set);
			int next = set.get(0);
			set.clear();
			used[freeIndex] = true;
			solveOrder[solveIndex++] = freeIndex;
			shuffledNpcOrder[freeIndex] = shuffledNpcOrder[next];
			shuffledNpcOrder[next] = 0;
			freeIndex = next;

		}
	}

	@Override
	public boolean processNPCClick1(Client player, NPC npc) {
		if (npc.def.name.equals("Sliding block")) {
			handleSlidingBlock(player, npc);
			return false;
		}
		return true;
	}

	private void handleSlidingBlock(Client player, NPC npc) {
		DungeonManager manager = player.dungParty.dungManager;
		VisibleRoom room = manager.getVisibleRoom(manager.getCurrentRoomReference(player.getCurrentLocation()));
		if (room == null || !(room instanceof SlidingTilesRoom)) {
			return;
		}

		player.getMovement().hardResetWalkingQueue();
		final SlidingTilesRoom puzzle = (SlidingTilesRoom) room;
		for (int i = 0; i < puzzle.tiles.length; i++) {
			if (puzzle.tiles[i] == npc) {
				//player.lock(1);//TODO?
				if (i == puzzle.solveOrder[puzzle.solveIndex - 1]) {
					puzzle.solveIndex--;
					if (puzzle.solveIndex == 0) {
						puzzle.setComplete(player);
						//players can keep clicking after it's done but will take damage
						puzzle.solveIndex = 1;
						puzzle.solveOrder[0] = -1;
					}
					int coordsX = DungeonManager.translateX(TILE_COORDS[puzzle.freeIndex][0], TILE_COORDS[puzzle.freeIndex][1], 0, 2, 2, 0);
					int coordsY = DungeonManager.translateY(TILE_COORDS[puzzle.freeIndex][0], TILE_COORDS[puzzle.freeIndex][1], 0, 2, 2, 0);
					Location base = puzzle.manager.getRoomBaseTile(puzzle.reference);
					npc.getWalkingQueue().reset();
					npc.getWalkingQueue().addPath(base.getX() + coordsX, base.getY() + coordsY);

					puzzle.tiles[puzzle.freeIndex] = puzzle.tiles[i];
					puzzle.tiles[i] = null;
					puzzle.freeIndex = i;
					return;
				} else {
					player.sendMessage("You strain your powers of telekenesis, but the tile just doesn't want to go there.");
					player.dealDamage(new Hit((int) (player.getSkills().getMaximumLifepoints() * .2), 0, -1));
					return;
				}
			}
		}
	}

	@Override
	public String getCompleteMessage() {
		return "You hear a click as a nearby door unlocks.";//This is correct
	}

}
