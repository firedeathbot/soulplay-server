package com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching;

import java.util.HashMap;
import java.util.Map;

public enum FletchingData {

	TANGLE_GUM(17682, new short[] {17702, 17722, 17742, 16977, 17756}, new short[] {5, 6, 5, 9, 12}, 15, new byte[] {1, 6, 1, 8, 3}),
	SEEPING_ELM(17684, new short[] {17704, 17724, 17742, 16979, 17758}, new short[] {9, 10, 6, 16, 21}, 21, new byte[] {11, 16, 10, 18, 13}),
	BLOOD_SPINDLE(17686, new short[] {17706, 17726, 17742, 16981, 17760}, new short[] {15, 17, 7, 27, 36}, 26, new byte[] {21, 26, 20, 28, 23}),
	UTUKU(17688, new short[] {17708, 17728, 17742, 16983, 17762}, new short[] {23, 26, 9, 41, 55}, 32, new byte[] {31, 36, 30, 38, 33}),
	SPINEBEAM(17690, new short[] {17710, 17730, 17742, 16985, 17764}, new short[] {33, 37, 11, 59, 79}, 37, new byte[] {41, 46, 40, 48, 43}),
	BOVISTRANGLER(17692, new short[] {17712, 17732, 17742, 16987, 17766}, new short[] {45, 51, 12, 81, 108}, 43, new byte[] {51, 56, 50, 58, 53}),
	THIGAT(17694, new short[] {17714, 17734, 17742, 16989, 17768}, new short[] {59, 67, 14, 106, 141}, 48, new byte[] {61, 66, 60, 68, 63}),
	CORPSETHORN(17696, new short[] {17716, 17736, 17742, 16991, 17770}, new short[] {75, 86, 16, 135, 180}, 54, new byte[] {71, 76, 70, 78, 73}),
	ENTGALLOW(17698, new short[] {17718, 17738, 17742, 16993, 17772}, new short[] {93, 106, 17, 167, 223}, 59, new byte[] {81, 86, 80, 88, 83}),
	GRAVE_CREEPER(17700, new short[] {17720, 17740, 17742, 16995, 17774}, new short[] {113, 129, 19, 203, 271}, 65, new byte[] {91, 96, 90, 98, 93});

	public static final Map<Integer, FletchingData> values = new HashMap<>();
	private final int logId;
	private final short[] products, xps;
	private final byte[] levels;
	private final int shaftCount;

	private FletchingData(int logId, short[] products, short[] xps, int shaftCount, byte[] levels) {
		this.logId = logId;
		this.products = products;
		this.xps = xps;
		this.shaftCount = shaftCount;
		this.levels = levels;
	}

	public int getLogId() {
		return logId;
	}

	public short[] getProducts() {
		return products;
	}

	public short[] getXps() {
		return xps;
	}

	public int getShaftCount() {
		return shaftCount;
	}

	public byte[] getLevels() {
		return levels;
	}

	static {
		for (FletchingData data : values()) {
			values.put(data.getLogId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }
}
