package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

public final class NormalRoom extends HandledRoom {
	
	public NormalRoom(int chunkX, int chunkY, int[] keyspot, int... doorsDirections) {
		super(chunkX, chunkY, new SpawnRandomNpcsEvent(), keyspot, doorsDirections);
	}

}
