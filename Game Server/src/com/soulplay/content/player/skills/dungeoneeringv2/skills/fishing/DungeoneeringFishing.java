package com.soulplay.content.player.skills.dungeoneeringv2.skills.fishing;

import com.soulplay.content.player.skills.Fishing;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;

public class DungeoneeringFishing {

	private static final int FLY_FISHING_ROAD_EMOTE = 622;
	private static final int FLY_FISHING_ROAD_EMOTE_LOOP = 623;
	private static final int FLY_FISHING_ROAD = 17794, FEATHER = 17796;

	private static boolean canFish(Client c, DungeoneeringFishData fishData) {
		if (c.getSkills().getStaticLevel(Skills.FISHING) < fishData.getLevel()) {
			c.getDialogueBuilder().sendStatement("You need a Fishing level of " + fishData.getLevel() + " to fish here.").execute();
			return false;
		}

		if (c.getItems().getItemSlot(FLY_FISHING_ROAD) == -1) {
			c.sendMessage("You need a fly fishing rod to fish here.");
			return false;
		}

		if (c.getItems().getItemSlot(FEATHER) == -1) {
			c.sendMessage("You don't have feather to fish here.");
			return false;
		}

		if (c.getItems().freeSlots() == 0) {
			c.getDialogueBuilder().sendStatement("You don't have enough inventory space.").execute();
			return false;
		}

		return true;
	}

	public static int decreaseFish(NPC npc) {
		return npc.dungFishCount--;
	}

	public static void start(Client c, NPC npc) {
		DungeoneeringFishData fishData = npc.fishData;
		if (fishData == null || !canFish(c, fishData) || c.skillingEvent != null) {
			return;
		}

		c.sendMessage("You attempt to capture a fish...");
		c.startAnimation(FLY_FISHING_ROAD_EMOTE);

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.startAnimation(FLY_FISHING_ROAD_EMOTE_LOOP);
				c.sendMessage("You manage to catch a " + fishData.getName() + ".");
				c.getAchievement().fishFishes();
				c.getItems().addItem(fishData.getId(), 1);
				c.getPA().addSkillXP(fishData.getXp(), Skills.FISHING);
				c.getItems().deleteItemInOneSlot(FEATHER, c.getItems().getItemSlot(FEATHER), 1);

				if (fishData == DungeoneeringFishData.CAVE_MORAY) {
					c.getAchievement().fish500CaveMoray();
				}

				if (decreaseFish(npc) <= 1) {
					c.resetAnimations();
					c.sendMessage("You have depleted this resource.");
					npc.removeNpcSafe(false);
					container.stop();
					return;
				}

				if (!c.insideDungeoneering() || !canFish(c, fishData)) {
					c.resetAnimations();
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				Fishing.resetFishing(c);
			}
		}, 4);
	}

}
