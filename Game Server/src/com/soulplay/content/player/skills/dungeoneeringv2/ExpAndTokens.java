package com.soulplay.content.player.skills.dungeoneeringv2;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonUtils;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class ExpAndTokens {

	private static final int firstFrozenExp = 157;
	private static final int lastFrozenExp = 505;
	private static final int firstAbandonedExp = 542;
	private static final int lastAbandonedExp = 726;
	private static final int firstFurnishedExp = 763;
	private static final int lastFurnishedExp = 1712;
	private static final int firstAbandoned2Exp = 1837;
	private static final int lastAbandoned2Exp = 2563;
	private static final int firstOccultExp = 2730;
	private static final int lastOccultExp = 5134;
	private static final int firstWarpedExp = 5412;
	private static final int lastWarpedExp = 9782;

	public static int[] dungeoneeringBaseExp(Party party, Player player, int dungFloor) { 
		int dungLevel = player.getPlayerLevel()[Skills.DUNGEONEERING];
		int dungSize = player.dungParty.size;
		int complexity = player.dungParty.getComplexity();
		int currentProgress = Party.calcCurrentProgress(player);
		int previousProgress = player.previousProgress;
		
		int complexityExp = 0;
		int dungeonSizeExp = 0;
		int floorExp = 0;
		int extraExp = 0;
		int prestigeExp = 0;
		int currentProgressExp = 0;
		int currentComplexityExp = 0;
		int currentDungeonSizeExp = 0;
		int prevProgressExp = 0;
		int prevComplexityExp = 0;
		int prevDungeonSizeExp = 0;
		int bonusRoomDecreaser = 0;
		
		if (dungFloor >= 1 && dungFloor <= 11) {
			floorExp += Misc.interpolate(firstFrozenExp, lastFrozenExp, 1, 11, dungFloor);
		}
		if (dungFloor >= 12 && dungFloor <= 17) {
			floorExp += Misc.interpolate(firstAbandonedExp, lastAbandonedExp, 12, 17, dungFloor);
		}
		if (dungFloor >= 18 && dungFloor <= 29) {
			floorExp += Misc.interpolate(firstFurnishedExp, lastFurnishedExp, 18, 29, dungFloor);
		}
		if (dungFloor >= 30 && dungFloor <= 35) {
			floorExp += Misc.interpolate(firstAbandoned2Exp, lastAbandoned2Exp, 30, 35, dungFloor);
		}
		if (dungFloor >= 36 && dungFloor <= 47) {
			floorExp += Misc.interpolate(firstOccultExp, lastOccultExp, 36, 47, dungFloor);
		}
		if (dungFloor >= 48 && dungFloor <= 60) {
			floorExp += Misc.interpolate(firstWarpedExp, lastWarpedExp, 48, 60, dungFloor);
		}
		
		
		if (previousProgress > 47) {
			prevProgressExp += Misc.interpolate(firstWarpedExp, lastWarpedExp, 48, 60, previousProgress);
		} else if (previousProgress > 35) {
			prevProgressExp += Misc.interpolate(firstOccultExp, lastOccultExp, 36, 47, previousProgress);
		} else if (previousProgress > 29) {
			prevProgressExp += Misc.interpolate(firstAbandoned2Exp, lastAbandoned2Exp, 30, 35, previousProgress);
		} else if (previousProgress > 17) {
			prevProgressExp += Misc.interpolate(firstFurnishedExp, lastFurnishedExp, 18, 29, previousProgress);
		} else if (previousProgress > 11) {
			prevProgressExp += Misc.interpolate(firstAbandonedExp, lastAbandonedExp, 12, 17, previousProgress);
		} else {
			prevProgressExp += Misc.interpolate(firstFrozenExp, lastFrozenExp, 1, 11, previousProgress);
		}
		
		if (currentProgress > 47) {
			currentProgressExp += Misc.interpolate(firstWarpedExp, lastWarpedExp, 48, 60, currentProgress);
		} else if (currentProgress > 35) {
			currentProgressExp += Misc.interpolate(firstOccultExp, lastOccultExp, 36, 47, currentProgress);
		} else if (currentProgress > 29) {
			currentProgressExp += Misc.interpolate(firstAbandoned2Exp, lastAbandoned2Exp, 30, 35, currentProgress);
		} else if (currentProgress > 17) {
			currentProgressExp += Misc.interpolate(firstFurnishedExp, lastFurnishedExp, 18, 29, currentProgress);
		} else if (currentProgress > 11) {
			currentProgressExp += Misc.interpolate(firstAbandonedExp, lastAbandonedExp, 12, 17, currentProgress);
		} else {
			currentProgressExp += Misc.interpolate(firstFrozenExp, lastFrozenExp, 1, 11, currentProgress);
		}
		
		if (complexity > 5) {
			bonusRoomDecreaser = Misc.interpolate(27, 0, 0, player.dungParty.dungManager.getBonusTotal(), player.dungParty.dungManager.getBonusOpened());
		} else if (complexity > 4) {
			bonusRoomDecreaser = Misc.interpolate(15, 0, 0, player.dungParty.dungManager.getBonusTotal(), player.dungParty.dungManager.getBonusOpened());
		}
		
		floorExp = (int) (floorExp - (floorExp * ((float) bonusRoomDecreaser /100)));
		currentProgressExp = (int) (currentProgressExp - (currentProgressExp * ((float) bonusRoomDecreaser /100)));
		prevProgressExp = (int) (prevProgressExp - (prevProgressExp * ((float) bonusRoomDecreaser /100)));
		
		if (complexity > 5) {
			complexityExp = (int) (floorExp * 4.25 - floorExp);
			prevComplexityExp = (int) (prevProgressExp * 4.25 - prevProgressExp);
			currentComplexityExp = (int) (currentProgressExp * 4.25 - currentProgressExp);
		} else if (complexity > 4) {
			complexityExp = (int) (floorExp * 2.9 - floorExp);
			prevComplexityExp = (int) (prevProgressExp * 2.9 - prevProgressExp);
			currentComplexityExp = (int) (currentProgressExp * 2.9 - currentProgressExp);
		} else if (complexity > 3) {
			complexityExp = (int) (floorExp * 1.31 - floorExp);
			prevComplexityExp = (int) (prevProgressExp * 1.31 - prevProgressExp);
			currentComplexityExp = (int) (currentProgressExp * 1.31 - currentProgressExp);
		} else if (complexity > 2) {
			complexityExp = (int) (floorExp * 1.22 - floorExp);
			prevComplexityExp = (int) (prevProgressExp * 1.22 - prevProgressExp);
			currentComplexityExp = (int) (currentProgressExp * 1.22 - currentProgressExp);
		} else if (complexity > 1) {
			complexityExp = (int) (floorExp * 1.14 - floorExp);
			prevComplexityExp = (int) (prevProgressExp * 1.14 - prevProgressExp);
			currentComplexityExp = (int) (currentProgressExp * 1.14 - currentProgressExp);
		}
		
		

		if (dungSize == 1) {
			dungeonSizeExp = complexityExp * 2 - complexityExp;
			prevDungeonSizeExp = prevComplexityExp * 2 - prevComplexityExp;
			currentDungeonSizeExp = currentComplexityExp * 2 - currentComplexityExp;
		} else if (dungSize == 2) {
			dungeonSizeExp = complexityExp * 4 - complexityExp; 
			prevDungeonSizeExp = prevComplexityExp * 4 - prevComplexityExp;
			currentDungeonSizeExp = currentComplexityExp * 4 - currentComplexityExp;
		}
		
		if ((dungFloor < 48 && (currentProgress < 48 || previousProgress < 48)) || complexity < 5) {
		    extraExp += Misc.interpolate(53, 0, 1, 120, dungLevel);
		} else {
			extraExp += Misc.interpolate(1, 3554, 1, 2, dungSize);
		}
		
		int totalPrevProgressExp = prevProgressExp + prevComplexityExp + prevDungeonSizeExp + extraExp;
		int totalCurrentProgressExp = currentProgressExp + currentComplexityExp + currentDungeonSizeExp + extraExp;
		
		
		float prevScaledPercent = (float) (0.009 * (previousProgress - dungFloor));
		float currentScaledPercent = (float) (0.009 * (currentProgress - dungFloor));
		
        float totalExpPercent;
        
        int totalExp = floorExp + complexityExp + dungeonSizeExp + extraExp;
        
		if (currentProgress < 1 && previousProgress < 1) {
			prestigeExp = (firstFrozenExp + complexityExp + dungeonSizeExp + extraExp);
		} else if (currentProgress < previousProgress) {
			if (previousProgress > dungFloor) {
				prestigeExp = (int) (totalPrevProgressExp - (totalPrevProgressExp * prevScaledPercent));
				totalExpPercent = (float) (0.007 * (previousProgress - dungFloor));
				totalExp = (int) (totalExp - (totalExp * totalExpPercent));
			} else {
				prestigeExp = totalPrevProgressExp;
			}
		} else if (currentProgress >= previousProgress) {
			if (currentProgress >= dungFloor) {
				prestigeExp = (int) (totalCurrentProgressExp - (totalCurrentProgressExp * currentScaledPercent));
				totalExpPercent = (float) (0.007 * (currentProgress - dungFloor));
				totalExp = (int) (totalExp - (totalExp * totalExpPercent));
			} else {
				prestigeExp = totalCurrentProgressExp;
			}
		}
		
		int[] range = DungeonUtils.getFloorThemeRange(dungFloor);
		boolean allCompleted = true;
		for (int toCheckFloor = range[0]; toCheckFloor <= range[1]; toCheckFloor++) {
			if (player.dungFloorsUnlocked < toCheckFloor) {
				break;
			}
			
			if (!player.isDungFloorCompleted(toCheckFloor)) {
				allCompleted = false;
				break;
			}
		}
		
		if (allCompleted) {
			player.sendMessage("all floors were completed so you get 0 prestige exp");
			prestigeExp = 0;
		}

		

		return new int[] {totalExp, prestigeExp};
	}

}
