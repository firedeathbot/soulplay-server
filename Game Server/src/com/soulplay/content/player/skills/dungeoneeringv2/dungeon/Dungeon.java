package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import java.util.List;
import java.util.Random;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.KeyDoors;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.SkillDoors;
import com.soulplay.util.Misc;

public class Dungeon {

	public int type;
	public int size;
	public Room[][] map;
	public int complexity;
	public DungeonManager manager;
	public RoomReference startRoom;
	public int creationCount;
	public int critCount;
	public final Random random;
	public final long seed;

	public Dungeon(DungeonManager manager, int floor, int size, int complexity) {
		this.manager = manager;
		this.type = DungeonUtils.getFloorType(floor);
		this.size = size;
		this.complexity = complexity;

		//long time = System.currentTimeMillis();
		seed = System.nanoTime();
		random = new Random(seed);
		DungeonStructure structure = new DungeonStructure(size, random, complexity, true);//manager == null ? true : manager.getParty().isKeyShare());
		//map structure to matrix dungeon
		map = new Room[DungeonConstants.DUNGEON_RATIO[size][0]][DungeonConstants.DUNGEON_RATIO[size][1]];
		RoomNode base = structure.getBase();
		
		List<Room> possibilities;
		startRoom = new RoomReference(base.x, base.y);
		List<RoomNode> children = base.getChildrenR();
		children.add(base);
		long eligiblePuzzleRooms = children.stream().filter(r -> r.children.size() > 0 && r.children.stream().allMatch(c -> c.lock == -1)).count();
		double puzzleChance = complexity < 6 ? 0 : 0.1 * children.size() / eligiblePuzzleRooms;
		for (RoomNode node : children) {
			creationCount++;
			boolean puzzle = false;
			if (node == base) {
				possibilities = DungeonUtils.selectPossibleRooms(DungeonConstants.START_ROOMS, complexity, type, base.north(), base.east(), base.south(), base.west());
			}
			else if (node.isBoss) {
				possibilities = DungeonUtils.selectPossibleBossRooms(type, complexity, floor, node.north(), node.east(), node.south(), node.west(), node.rotation());
			}
			else if (node.children.size() > 0 && node.children.stream().allMatch(c -> c.lock == -1) && puzzleChance > random.nextDouble()) {
				puzzle = true;
				possibilities = DungeonUtils.selectPossibleRooms(DungeonConstants.PUZZLE_ROOMS, complexity, type, node.north(), node.east(), node.south(), node.west(), node.rotation());
			}
			else {
				possibilities = DungeonUtils.selectPossibleRooms(DungeonConstants.NORMAL_ROOMS, complexity, type, node.north(), node.east(), node.south(), node.west());
			}
			Room room = map[node.x][node.y] = possibilities.get(random.nextInt(possibilities.size()));
			if (node.isCritPath) {
				critCount++;
				room.setCritPath(true);
			}
			if (node.key != -1)
				room.setDropId(KeyDoors.values[node.key].getKeyId());
			
			int[] directions = room.getRoom().getDoorDirections();
			for (int doorDir = 0, length = directions.length; doorDir < length; doorDir++) {
				int rotation = (directions[doorDir] + room.getRotation()) & 0x3;
				RoomNode neighbor = structure.getRoom(node.x + Misc.ROTATION_DIR_X[rotation], node.y + Misc.ROTATION_DIR_Y[rotation]);
				if (neighbor != null && neighbor.parent != null && neighbor.parent == node) {
					if (puzzle) {
						room.setDoor(doorDir, new Door(DungeonConstants.CHALLENGE_DOOR));
					}
					else if (neighbor.lock != -1) {
						room.setDoor(doorDir, new Door(DungeonConstants.KEY_DOOR, neighbor.lock));
					}
					else if (complexity >= 5 && random.nextInt(3) == 0) {
						int doorIndex = random.nextInt(DungeonConstants.SkillDoors.values.length);
						SkillDoors sd = DungeonConstants.SkillDoors.values[doorIndex];
						if (sd.getClosedObject(type) == -1) //some frozen skill doors don't exist
							continue;
						int level;
						if (manager == null) {
							level = 1;
						} else {
							int maxLevel = manager.party.getMaxLevel(sd.getSkillId());
							level = neighbor.isCritPath ? (maxLevel - random.nextInt(10)) : random.nextInt(sd.getSkillId() == Skills.SUMMONING || sd.getSkillId() == Skills.PRAYER ? 100 : 106);
						}

						room.setDoor(doorDir, new Door(DungeonConstants.SKILL_DOOR, doorIndex, level < 1 ? 1 : level));
					}
					else if (random.nextInt(2) == 0) {
						room.setDoor(doorDir, new Door(DungeonConstants.GUARDIAN_DOOR));
					}

				}
			}
		}
		//System.out.println("took " + (System.currentTimeMillis() - time) + "ms to generate a room.");
	}

	public Room getRoom(RoomReference reference) {
		return getRoom(reference.getX(), reference.getY());
	}

	public Room getRoom(int x, int y) {
		if (x < 0 || y < 0 || x >= map.length || y >= map[x].length) {
			return null;
		}

		return map[x][y];
	}

	public RoomReference getStartRoomReference() {
		return startRoom;
	}

}