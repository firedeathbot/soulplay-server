package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import java.util.Collection;

import com.soulplay.Server;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class VisibleRoom {

	private boolean loaded;
	protected RoomReference reference;
	protected DungeonManager manager;
	protected int type;

	public void init(DungeonManager manager, RoomReference ref, int type, HandledRoom room) {
		this.type = type;
		this.reference = ref;
		this.manager = manager;
	}

	public boolean roomCleared() {
		Location baseTile = manager.getRoomBaseTile(reference);
		Collection<NPC> npcs = Server.getRegionManager().getLocalNpcsByLocation(baseTile.getX(), baseTile.getY(), baseTile.getZ());
		int startX = baseTile.getX() + 1;
		int startY = baseTile.getY() + 1;
		int endX = baseTile.getX() + 14;
		int endY = baseTile.getY() + 14;
		int guardians = 0;
		for (NPC npc : npcs) {
			boolean checkGuardian = npc.dungeoneeringType == DungeonConstants.GUARDIAN_NPC || npc.dungeoneeringType == DungeonConstants.FORGOTTEN_WARRIOR || npc.dungeoneeringType == DungeonConstants.SLAYER_NPC;
			if (checkGuardian && (npc.getSkills().getLifepoints() > 0 && npc.absX >= startX && npc.absY >= startY && npc.absX <= endX && npc.absY <= endY)) {
				guardians++;
			}
		}

		//System.out.println(startX+":"+startY+":"+endX+":"+endY+":"+guardians+":"+npcs.size());
		return guardians == 0;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded() {
		loaded = true;
	}

	public void destroy() {

	}

	public void openRoom() {

	}

	public boolean processObjectClick1(Client p, GameObject object) {
		return true;
	}

	public boolean processObjectClick2(Client p, GameObject object) {
		return true;
	}

	public boolean processObjectClick3(Client p, GameObject object) {
		return true;
	}

	public boolean processObjectClick4(Client p, GameObject object) {
		return true;
	}

	public boolean processObjectClick5(Client p, GameObject object) {
		return true;
	}

	public boolean handleItemOnObject(Client player, RSObject object, Item item) {
		return true;
	}

	public boolean handleItemOnNpc(Client player, NPC npc, int itemId) {
		return true;
	}

	public boolean processNPCClick1(Client player, NPC npc) {
		return true;
	}

	public boolean processNPCClick2(Client player, NPC npc) {
		return true;
	}

	public boolean canMove(Client player, Location to) {
		return true;
	}

	public boolean processNPCClick3(Client player, NPC npc) {
		return true;
	}

}