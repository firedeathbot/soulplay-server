package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

public interface RoomEvent {

	public void openRoom(DungeonManager dungeon, RoomReference reference);
}