package com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing;

import java.util.HashMap;
import java.util.Map;

public enum SmeltingDataDung {

	NOVITE(17630, 17650, 7, 1),
	BATHUS(17632, 17652, 13, 10),
	MARMAROS(17634, 17654, 19, 20),
	KRATONITE(17636, 17656, 25, 30),
	FRACTILE(17638, 17658, 32, 40),
	ZEPHYRIUM(17640, 17660, 38, 50),
	ARGONITE(17642, 17662, 44, 60),
	KATAGON(17644, 17664, 51, 70),
	GORGONITE(17646, 17666, 57, 80),
	PROMETHIUM(17648, 17668, 63, 90);

	public static final Map<Integer, SmeltingDataDung> values = new HashMap<>();
	public static final Map<Integer, SmeltingDataDung> barToData = new HashMap<>();
	private final int xp, oreId, barId, level;

	private SmeltingDataDung(int oreId, int barId, int xp, int level) {
		this.oreId = oreId;
		this.barId = barId;
		this.xp = xp;
		this.level = level;
	}

	public int getXp() {
		return xp;
	}

	public int getOreId() {
		return oreId;
	}

	public int getBarId() {
		return barId;
	}

	public int getLevel() {
		return level;
	}

	static {
		for (SmeltingDataDung data : values()) {
			values.put(data.getOreId(), data);
			barToData.put(data.getBarId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
