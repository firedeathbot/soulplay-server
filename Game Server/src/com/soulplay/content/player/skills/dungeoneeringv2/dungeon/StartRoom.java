package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

public final class StartRoom extends HandledRoom {

	public StartRoom(int chunkX, int chunkY, int... doorsDirections) {
		super(chunkX, chunkY, new RoomEvent() {
			@Override
			public void openRoom(DungeonManager dungeon, RoomReference reference) {
				dungeon.setTableItems(reference);
				dungeon.spawnNPC(reference, DungeonConstants.SMUGGLER, 8, 8, false, DungeonConstants.NORMAL_NPC, false); // smoother
				dungeon.telePartyToRoom(reference);
				dungeon.linkPartyToDungeon();
			}
		}, new int[] { 7, 7 }, doorsDirections);

	}

	@Override
	public boolean allowResources() {
		return false;
	}

}
