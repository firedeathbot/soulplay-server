package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import com.soulplay.content.shops.ShopType;
import com.soulplay.game.model.player.Client;

public class SmugglerDialogue {
	
	public static void smugglerChat(Client c, int npcType) {
		c.getDialogueBuilder().sendNpcChat(npcType, "Hail, " + c.getNameSmartUp() + ". Need something?").createCheckpoint()
		.sendOption("What can you tell me about this place?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("What can you tell me about this place?")
			.sendNpcChat(npcType, "You know all I can teach you already, friend,", "having conquered many floors yourself.");
		}, "Who are you?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Who are you?")
			.sendNpcChat(npcType, "A friend.")
			.sendPlayerChat("Okay, what are you doing here, friend?")
			.sendNpcChat(npcType, "I'm here to help out.")
			.sendPlayerChat("With what?")
		    .sendNpcChat(npcType, "Well, let's say you find yourself in need of advernturing kit,", "and you've a heavey pile of rusty coins weighing you down.", "I can help you with both of those problems. savvy?")
		    .sendPlayerChat("Ah, so you're a trader.")
		    .sendNpcChat(npcType, "Keep it down, you fool!")
		    .sendNpcChat(npcType, "Yes, I'm a trader. But I'm not supposed to be trading here.")
		    .sendNpcChat(npcType, "If you want my goods, you'll learn not to talk about me.")
		    .sendPlayerChat("Right, got you.")
		    .sendPlayerChat("Is there anything else you can do for me?")
		    .sendNpcChat(npcType, "Well, there's a job I'm supposed to be doing down here.")
		    .sendPlayerChat("Which is?")
		    .sendNpcChat(npcType, "Say you chance upon an Object that you know little about", "Show it to me, and I'll tell you what it's used for.")
		    .sendPlayerChat("That's good to know.")
		    .sendNpcChat("I can also offer hints about the behaviour of powerful", "opponents you might meet in the area, I've spent a long", "time down here, observing them.")
		    .sendPlayerChat("I'll be sure to come back if I find a particularly", "strong opponent, then.")
		    .sendNpcChat(npcType, "You'd be wise to, " + c.getNameSmartUp() + ".")
		    .sendPlayerChat("How do you know my name?")
		    .sendNpcChat(npcType, "Nothing gets in or out of Daemonheim without me", "knowing about it.")
		    .sendPlayerChat("Fair enough. Back to my other questions").sendAction(()-> {
		    	c.getDialogueBuilder().returnToCheckpoint();
		    });
		}, "Do I have any rewards to claim?", ()-> {
			c.getDialogueBuilder().sendNpcChat(npcType, "I have no rewards for you at the moment.");
		}, "I'm here to trade.", ()-> {
			smugglerOpenShop(c);
		}, "Can I change my starting items?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Can I change my starting Items?")
			.sendNpcChat(npcType, "You're currently starting with a stack of coins. I could set", "up a pre-order, if you'd like.")
			.sendNpcChat(npcType, "I'll automatically take your coins, and provide a selection",   " of feathers, rune essence and antipoison in retur")
			.sendOption("I would like to receive coins.", ()-> {
				c.getDialogueBuilder().sendNpcChat(npcType, "You are already starting with coins.");
			}, "I would like to receive feathers, rune essence and antipoison.", ()-> {
				c.getDialogueBuilder().sendNpcChat(npcType, "I'm sorry but it seems like i have ran out of this package.");
			}, "I don't want to change anything.", ()-> {
				smugglerChat(c, npcType);
			});
		}).execute();
	}
	
	public static void smugglerOpenShop(Client c) {
		if (c.dungParty.getComplexity() > 4) {
			c.getShops().openShop(58, ShopType.BIG);
		} else if (c.dungParty.getComplexity() > 3) {
			c.getShops().openShop(57, ShopType.BIG);
		} else if (c.dungParty.getComplexity() > 2) {
			c.getShops().openShop(56, ShopType.BIG);
		} else if (c.dungParty.getComplexity() > 1) {
			c.getShops().openShop(55, ShopType.BIG);
		} else
			c.sendMessage("the smuggler has nothing for sale in complexity 1.");
	}

}
