package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.npcs.NpcStatsEntry;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.util.Misc;

public class DungeonBossesStats {

	public static final void setStats(NPC npc, int[][] array) {
		if (npc == null) {
			return;
		}

		DungeonManager dungeon = npc.dungeonManager;
		
		int teamAvg = dungeon.teamAvg;
		Party dp = dungeon.party.c.dungParty;
		if (dp == null)
			dp = dungeon.party;
		int dungLevelAvg = Party.calcPartyAvgDungLvls(dp);
		int difficulty = dungeon.party.difficulty;

		int combat = 10;
		if (npc.def != null) {
			combat = npc.def.combatLevel;
		}
		
		int decDef = 0;
		int decAtt = 0;
		int decHit = 0;
		
		int minAvg = 85;
		int maxAvg = 3960;
		
		dungLevelAvg = Misc.interpolate(1, dungLevelAvg, 1, 6, dungeon.party.getComplexity());
		int extraHp = dungLevelAvg * 2;
		
//		System.out.println("maxhit: " + npc.maxHit + " defence: " + npc.defence + " attack: " + npc.attack);

		if (array == DungeonBosses.GLUTTONOUS_BEHEMOTH) { //http://runescape.wikia.com/wiki/Gluttonous_behemoth?diff=5113323&oldid=5109356
			setNpcAttack(npc, Misc.interpolate(25, 350 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(20, 150 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 28 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(5);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (350 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else if (array == DungeonBosses.ASEA_FROSTWEB) { //http://runescape.wikia.com/wiki/Astea_Frostweb?diff=6073764&oldid=6073753
			setNpcAttack(npc, Misc.interpolate(25, 350 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(20, 120 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 32 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(5);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (300 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else if (array == DungeonBosses.ICY_BONES) { //can easily hit 50 with his multi melee attacks 
			setNpcAttack(npc, Misc.interpolate(1, 165 - decAtt, 1, 474, combat));
			setNpcDefense(npc, Misc.interpolate(1, 210 - decDef, 1, 474, combat));
			npc.maxHit = Misc.interpolate(1, 20 - decHit, 1, 613, combat);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(10, 938, 5, 474, combat)); //done
		} else if (array == DungeonBosses.LUMINESCENT_ICEFIEND) { //http://runescape.wikia.com/wiki/Luminescent_icefiend?diff=7393132&oldid=7378020
			setNpcAttack(npc, Misc.interpolate(1, 195 - decAtt, 1, 456, combat));
			setNpcDefense(npc, Misc.interpolate(1, 135 - decDef, 1, 456, combat));
			npc.maxHit = Misc.interpolate(1, 20 - decHit, 1, 613, combat);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(9, 1145, 3, 456, combat));
		} else if (array == DungeonBosses.PLANE_FREEZER_LAKHRAHNAZ) { //http://runescape.wikia.com/wiki/Plane-freezer_Lakhrahnaz?diff=5132110&oldid=5118489
			setNpcAttack(npc, Misc.interpolate(1, 180 - decAtt, 1, 491, combat));
			setNpcDefense(npc, Misc.interpolate(1, 180 - decDef, 1, 491, combat));
			npc.maxHit = Misc.interpolate(1, 20 - decHit, 1, 613, combat);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(17, 1280, 7, 491, combat));
		} else if (array == DungeonBosses.TOKASH_THE_BLOODCHILLER) { //http://runescape.wikia.com/wiki/To%27Kash_the_Bloodchiller?diff=5173037&oldid=5112239
			setNpcAttack(npc, Misc.interpolate(1, 180 - decAtt, 1, 512, combat));
			setNpcDefense(npc, Misc.interpolate(1, 230 - decDef, 1, 512, combat));
			npc.maxHit = Misc.interpolate(1, 20 - decHit, 1, 613, combat);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(19, 944, 8, 512, combat));
		} else if (array == DungeonBosses.HOBGOBLIN_GEOMANCER) { //http://runescape.wikia.com/wiki/Hobgoblin_Geomancer?diff=4164456&oldid=4162670
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 170 - decAtt, 1, 621, combat));
			setNpcDefense(npc, Misc.interpolate(1, 130 - decDef, 1, 621, combat));
			npc.maxHit = Misc.interpolate(1, 20 - decHit, 1, 613, combat);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(65, 1588, 32, 621, combat));
		} else if (array == DungeonBosses.BULWARK_BEAST) { //http://runescape.wikia.com/wiki/Bulwark_beast?diff=5113016&oldid=4937778
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(20, 330 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(15, 160 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 28 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(4);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (350 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else if (array == DungeonBosses.UNHOLY_CURSEBEARER) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.RAMMERNAUT) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.STOMP) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.HARLAKK_THE_RIFTSPLITTER) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.LEXICUS_RUNEWRIGHT) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.SAGITTARE) {
			setNpcAttack(npc, Misc.interpolate(25, 420 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(20, 85 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 36 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(3);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (350 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else if (array == DungeonBosses.NIGHT_GAZER_KHIGHORAHK) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.SHADOW_FORGER_IHLAKHIZAN) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.BALLAK_THE_PUMMELLER) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.SKELETAL_TRIO) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.RUNDEBOUND_BEHEMOTH) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.GRAVECREEPER) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.NECROLORD) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(25, 350 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(20, 80 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 30 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(5);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (300 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else if (array == DungeonBosses.FLESH_SPOILER_HAASGHENAHK) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.YKLAGOR_THE_THUNDEROUS) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.WARPED_GULEGA) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.DREADNAUT) {
			setNpcAttack(npc, Misc.interpolate(25, 350 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(20, 150 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 51 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(5);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (450 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else if (array == DungeonBosses.HOPE_DEVOURER) {
			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.WORLD_GORGER_SHUKARHAZH) {
			npc.properties = Misc.setBit(0, NpcStatsEntry.HEX_HUNTER_EFFECT);

			setNpcAttack(npc, Misc.interpolate(1, 130 - decAtt, 1, 114, combat));
			setNpcDefense(npc, Misc.interpolate(1, 90 - decDef, 1, 114, combat));
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(1, 124, 1, 114, combat));
		} else if (array == DungeonBosses.KALGER_THE_WARMONGER) {
			setNpcAttack(npc, Misc.interpolate(25, 480 - decAtt, minAvg, maxAvg, teamAvg));
			setNpcDefense(npc, Misc.interpolate(20, 120 - decDef, minAvg, maxAvg, teamAvg));
			npc.maxHit = Misc.interpolate(2, 64 - decHit, minAvg, maxAvg, teamAvg);
			npc.getCombatStats().setAttackSpeed(5);
			npc.getSkills().setStaticLevel(Skills.HITPOINTS, Misc.interpolate(30 * difficulty, (500 + extraHp) * difficulty, minAvg, maxAvg, teamAvg));
		} else {
			//System.out.println("missing npc stats: " + npc.def.combatLevel);
		}
		//System.out.println("maxhit: " + npc.maxHit + " defence: " + npc.defence + " attack: " + npc.attack + "hitpoints: " + npc.getSkills().getStaticLevel(Skills.HITPOINTS));
	}

	private static void setNpcAttack(NPC npc, int attack) {
		CombatStats combatStats = npc.getCombatStats();
		combatStats.setStabBonus(attack);
		combatStats.setSlashBonus(attack);
		combatStats.setCrushBonus(attack);
		combatStats.setRangedAccBonus(attack);
		combatStats.setMagicAccBonus(attack);
	}

	private static void setNpcDefense(NPC npc, int defense) {
		CombatStats combatStats = npc.getCombatStats();
		combatStats.setStabDefBonus(defense);
		combatStats.setSlashDefBonus(defense);
		combatStats.setCrushDefBonus(defense);
		combatStats.setRangedDefBonus(defense);
		combatStats.setMagicDefBonus(defense);
	}

}
