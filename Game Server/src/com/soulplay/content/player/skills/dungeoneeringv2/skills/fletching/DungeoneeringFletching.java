package com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.Fletching;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;

public class DungeoneeringFletching {
	
	public static final int KNIFE = 17754, BOW_STRING = 17752, HEADLESS = 17747, SHAFT = 17742, FEATHER = 17796;

	public static boolean applyFeathers(Client c, int itemUsed, int usedWith, int itemUsedSlot, int usedWithSlot) {
		if (itemUsed != FEATHER && usedWith != FEATHER) {
			return false;
		}

		if (c.getItems().freeSlots() == 0 && c.getItems().getItemSlot(HEADLESS) == -1) {
			c.getDialogueBuilder().sendStatement("You don't have enough inventory space.").execute();
			return true;
		}

		int usedAmount = c.getItems().getItemAmountInSlot(itemUsedSlot);
		int usedWithAmount = c.getItems().getItemAmountInSlot(usedWithSlot);
		int toMake = Math.min(15, Math.min(usedAmount, usedWithAmount));
		c.getItems().addItem(HEADLESS, toMake);
		c.getItems().deleteItem2(usedWith, toMake);
		c.getItems().deleteItem2(itemUsed, toMake);
		return true;
	}

	public static boolean applyArrowTips(Client c, int itemUsed, int usedWith, int itemUsedSlot, int usedWithSlot) {
		ArrowData data = ArrowData.values.get(itemUsed);
		if (data == null) {
			data = ArrowData.values.get(usedWith);
			if (data == null) {
				return false;
			}
		}

		if (c.getItems().freeSlots() == 0 && c.getItems().getItemSlot(data.getProductId()) == -1) {
			c.getDialogueBuilder().sendStatement("You don't have enough inventory space.").execute();
			return true;
		}

		int usedAmount = c.getItems().getItemAmountInSlot(itemUsedSlot);
		int usedWithAmount = c.getItems().getItemAmountInSlot(usedWithSlot);
		int toMake = Math.min(15, Math.min(usedAmount, usedWithAmount));
		c.getItems().addItem(data.getProductId(), toMake);
		c.getPA().addSkillXP((int) (toMake * data.getXp()), Skills.FLETCHING);
		c.getItems().deleteItem2(usedWith, toMake);
		c.getItems().deleteItem2(itemUsed, toMake);
		return true;
	}

	private static boolean canFletch(Client c, FletchingData data, int index) {
		if (!c.getItems().playerHasItem(KNIFE)) {
			c.sendMessage("You don't have a knife to fletch with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.FLETCHING] < data.getLevels()[index]) {
			c.sendMessage("You need a fletching level of " + data.getLevels()[index] + " to fletch this.");
			return false;
		}

		if (!c.getItems().playerHasItem(data.getLogId(), 1)) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to fletch.").execute();
			return false;
		}

		return true;
	}

	public static boolean initStringing(Client c, int itemUsed, int usedWith) {
		StringingData data = StringingData.values.get(itemUsed);
		if (data == null) {
			data = StringingData.values.get(usedWith);
			if (data == null) {
				return false;
			}
		}

		final StringingData finalData = data;
		MakeInterface.make(c, data.getProduct(), () -> {startStringing(c, finalData);});
		return true;
	}

	private static void startStringing(Client c, StringingData data) {
		c.getPA().closeAllWindows();

		if (!canString(c, data) || c.skillingEvent != null) {
			return;
		}

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int cycles = c.makeCount;

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItem2(data.getInput(), 1);
				c.getItems().deleteItem2(BOW_STRING, 1);
				c.startAnimation(data.getAnim());
				c.getItems().addItem(data.getProduct(), 1);
				c.getPA().addSkillXP(data.getXp(), Skills.FLETCHING);
				c.sendMessage("You string " + ItemDef.forID(data.getProduct()).getName() + ".");

				cycles--;
				if (!c.insideDungeoneering() || cycles == 0 || !canString(c, data)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 2);
	}

	private static boolean canString(Client c, StringingData data) {
		if (!c.getItems().playerHasItem(data.getInput())) {
			c.sendMessage("You don't have a bow to string.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.FLETCHING] < data.getLevel()) {
			c.sendMessage("You need a fletching level of " + data.getLevel() + " to string this bow.");
			return false;
		}

		if (!c.getItems().playerHasItem(BOW_STRING)) {
			c.sendMessage("You don't have a bowstring to string with.");
			return false;
		}

		return true;
	}

	public static boolean initFletch(Client c, int itemUsed, int usedWith) {
		FletchingData data = FletchingData.values.get(itemUsed);
		if (data == null) {
			data = FletchingData.values.get(usedWith);
			if (data == null) {
				return false;
			}
		}

		final FletchingData finalData = data;
		Runnable trapsCode = null;
		if (c.dungParty.getComplexity() >= 5) {
			trapsCode = () -> {startFletch(c, finalData, 4);};
		}

		MakeInterface.make(c, data.getProducts()[0], () -> {
			startFletch(c, finalData, 0);
		}, data.getProducts()[1], () -> {
			startFletch(c, finalData, 1);
		}, data.getProducts()[2], () -> {
			startFletch(c, finalData, 2);
		}, data.getProducts()[3], () -> {
			startFletch(c, finalData, 3);
		}, data.getProducts()[4], trapsCode);
		return true;
	}

	private static void startFletch(Client player, FletchingData data, int index) {
		player.getPA().closeAllWindows();

		if (!canFletch(player, data, index) || player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int cycles = player.makeCount;
			
			@Override
			public void execute(CycleEventContainer container) {
				player.getItems().deleteItem2(data.getLogId(), 1);
				player.startAnimation(Fletching.FLETCH_ANIMATION);
				player.getItems().addItem(data.getProducts()[index], index == 2 ? data.getShaftCount() : 1);
				player.getPA().addSkillXP(data.getXps()[index], Skills.FLETCHING);
				player.sendMessage("You fletch " + ItemDef.forID(data.getProducts()[index]).getName() + ".");
				
				cycles--;
				if (!player.insideDungeoneering() || cycles == 0 || !canFletch(player, data, index)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 2);
	}

}
