package com.soulplay.content.player.skills.dungeoneeringv2.drops;

import java.util.LinkedList;
import java.util.List;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.skills.dungeoneeringv2.drops.impl.*;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.items.DungeonItems;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class DropTableManager {

	public static final DropTableInfo SIMPLE_TABLES = new SimpleTable();
	public static final DropTableInfo MID_TIER_TABLE = new MidTierTable();

	public static final int MAIN_TABLE_CHANCE = 100;
	public static final int COIN_DROP_RATE = 100; // 100 = 20%
	public static final int ARMOUR_WEAPONS_LOOP_CHANCE = 6;
	public static final int ARMOUR_WEAPONS_CHANCE = 5;
	public static final int ARROWS_RUNES_CHANCE = 1;

	public static int checkTier(NPC n) {
		int combat = 0;
		if (n.getEntityDef() != null) {
			combat = n.getEntityDef().combatLevel;
		}

		int tier = 1;
		for (int i = 10; i >= 0; i--) {
			if (combat >= (12 * i) + 6) {
				tier = i;
				break;
			}
		}

		return tier;
	}

	public static void doStuff(NPC n, Client p) {//Drop general table.
		DungeonManager dungeon = n.dungeonManager;
		if (dungeon == null || !n.dropStuff) {
			return;
		}

		if (n.dungeoneeringType == DungeonConstants.BOSS_NPC) {
			BossDropManager.dropBossItem(dungeon, n.dungItemData);
			return;
		}

		int	difficulty = dungeon.party.difficulty;
		int	teamAvg = dungeon.teamAvg;
		int complexity = dungeon.party.getComplexity();

		int maxDefaultRolls = (2 * difficulty) + 4;
		if (complexity < 2) {
			maxDefaultRolls = (4 * difficulty) + 6;
		}
		int defaultRolls = Misc.randomNoPlus(0, maxDefaultRolls);
		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(MAIN_TABLE_CHANCE);
			if (random >= 18) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.FOOD);
			} else if (random >= 16) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.MISC);
			} else if (random >= 14) {
				doDropping(n, MID_TIER_TABLE, DungeonItems.DAGGERS);
			} else if (random >= 12) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.RUNES);
			} else if (random >= 10 && complexity > 1) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.BRANCHES);
			} else if (random >= 8 && complexity > 2) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.ORES);
			} else if (random >= 6 && complexity > 3) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.HERBS);
			} else if (random >= 4 && complexity > 3) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.TEXTILES);
			} else if (random >= 2 && complexity > 4) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.SEEDS);
			} else if (complexity > 4) {
				doDropping(n, SIMPLE_TABLES, DungeonItems.TORN_BAGS);
			}
		}

		if (Misc.randomNoPlus(COIN_DROP_RATE) >= 80 && complexity > 1) {
			int diffMultiplier = (50 * difficulty * difficulty * difficulty);

			dungeon.spawnBelow(n, DungeonConstants.RUSTY_COINS, diffMultiplier + Misc.random(diffMultiplier + ((50 * teamAvg / (8 * difficulty)) * difficulty)));
		}

		String npcName = n.getNpcName().toLowerCase();
		
		int COMMON = 5;
		int UNCOMMON = 12;
		int RARE = 18;
		int VERY_RARE = 25;
		
		switch (npcName) {
		
		case "crawling hand":
			if (Misc.random(COMMON) == 1) {
				dungeon.spawnBelow(n, 17261, 1);
			} else if (Misc.random(RARE) == 2) {
				dungeon.spawnBelow(n, 17263, 1);
			}
			break;
			
		case "cave crawler":
			if (Misc.random(UNCOMMON) == 1) {
				dungeon.spawnBelow(n, 17265, 1);
			} else if (Misc.random(VERY_RARE) == 2) {
				dungeon.spawnBelow(n, 17267, 1);
			}
			break;
			
		case "cave slime":
			if (Misc.random(UNCOMMON) == 1) {
				dungeon.spawnBelow(n, 17269, 1);
			} else if (Misc.random(RARE) == 2) {
				dungeon.spawnBelow(n, 17271, 1);
			}
			
		case "pyrefiend":
			if (Misc.random(UNCOMMON) == 1) {
				dungeon.spawnBelow(n, 17273, 1);
			}
			break;
			
		case "night spider":
			if (Misc.random(VERY_RARE) == 1) {
				dungeon.spawnBelow(n, 17279, 1);
			}
			break;
			
		case "jelly":
			if (Misc.random(COMMON) == 1) {
				dungeon.spawnBelow(n, 17281, 1);
			} else if (Misc.random(RARE) == 2) {
				dungeon.spawnBelow(n, 17283, 1);
			}
			break;
			
		case "spiritual guardian":
			if (Misc.random(COMMON) == 1) {
				dungeon.spawnBelow(n, 17285, 1);
			} else if (Misc.random(RARE) == 2) {
				dungeon.spawnBelow(n, 17287, 1);
			}
			break;
			
		case "seeker":
			if (Misc.random(RARE) == 1) {
				dungeon.spawnBelow(n, 17289, 1);
			} 
			break;
			
		case "nechryael":
			if (Misc.random(RARE) == 1) {
				dungeon.spawnBelow(n, 17293, 1);
			}
			break;
			
		case "edimmu":
			if (Misc.random(RARE) == 1) {
				dungeon.spawnBelow(n, 17291, 1);
			}
			break;
			
        case "soulgazer":
        	if (Misc.random(RARE) == 1) {
				dungeon.spawnBelow(n, 17295, 1);
    			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + p.getNameSmartUp() +" has received a Hexhunter bow in dungeoneering!");
			}
			break; 
		
	//TODO: make warriors, rangers, magers not being able to drop same piece in same time.
		case "forgotten warrior":
			int maxWarriorRolls = 3;
			int warriorRolls = Misc.randomNoPlus(1, maxWarriorRolls);
			int[][] lastWarriorSuccessRoll = null;
			for (int i = 0; i < warriorRolls; i++) {
				//System.out.println("looping");
				if (Misc.random(ARMOUR_WEAPONS_LOOP_CHANCE) == 1) {
					//System.out.println("pick");
					int random = Misc.randomNoPlus(9);
					if (random > 8 && lastWarriorSuccessRoll != DungeonItems.FULL_HELMS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.FULL_HELMS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.FULL_HELMS);
						}
						lastWarriorSuccessRoll = DungeonItems.FULL_HELMS;
					} else if (random > 7 && lastWarriorSuccessRoll != DungeonItems.KITESHIELDS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.KITESHIELDS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.KITESHIELDS);
						}
						lastWarriorSuccessRoll = DungeonItems.KITESHIELDS;
					} else if (random > 6 && lastWarriorSuccessRoll != DungeonItems.PLATEBODYS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.PLATEBODYS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.PLATEBODYS);
						}
						lastWarriorSuccessRoll = DungeonItems.PLATEBODYS;
					} else if (random > 5 && lastWarriorSuccessRoll != DungeonItems.PLATELEGS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.PLATELEGS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.PLATELEGS);
						}
						lastWarriorSuccessRoll = DungeonItems.PLATELEGS;
					} else if (random > 4 && lastWarriorSuccessRoll != DungeonItems.PLATESKIRTS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.PLATESKIRTS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.PLATESKIRTS);
						}
						lastWarriorSuccessRoll = DungeonItems.PLATESKIRTS;
					} else if (random > 3 && lastWarriorSuccessRoll != DungeonItems.CHAINBODYS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.CHAINBODYS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.CHAINBODYS);
						}
						lastWarriorSuccessRoll = DungeonItems.CHAINBODYS;
					} else if (random > 2 && lastWarriorSuccessRoll != DungeonItems.GAUNTLETS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.GAUNTLETS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.GAUNTLETS);
						}
						lastWarriorSuccessRoll = DungeonItems.GAUNTLETS;
					} else if (random > 1 && lastWarriorSuccessRoll != DungeonItems.BOOTS) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.BOOTS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.BOOTS);
						}
						lastWarriorSuccessRoll = DungeonItems.BOOTS;
					} else {
						switch (n.dungeonWeaponIdentifier) {

						case 0:// forgotten warrior mauls
							if (lastWarriorSuccessRoll != DungeonItems.MAULS) {
								if (n.dungeonTier != -1) {
									drop(DungeonItems.MAULS[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.MAULS);
								}
								lastWarriorSuccessRoll = DungeonItems.MAULS;
							}
							break;

						case 1:// forgotten warrior spears
							if (lastWarriorSuccessRoll != DungeonItems.SPEARS) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.SPEARS[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.SPEARS);
								}
								lastWarriorSuccessRoll = DungeonItems.SPEARS;
							}
							break;

						case 2:// forgotten warrior 2h
							if (lastWarriorSuccessRoll != DungeonItems.TWO_HANDED_SWORDS) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.TWO_HANDED_SWORDS[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.TWO_HANDED_SWORDS);
								}
								lastWarriorSuccessRoll = DungeonItems.TWO_HANDED_SWORDS;
							}
							break;

						case 3:// forgotten warrior Battleaxe
							if (lastWarriorSuccessRoll != DungeonItems.BATTLEAXES) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.BATTLEAXES[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.BATTLEAXES);
								}
								lastWarriorSuccessRoll = DungeonItems.BATTLEAXES;
							}
							break;

						case 4:// forgotten warrior warhammer
							if (lastWarriorSuccessRoll != DungeonItems.WARHAMMERS) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.WARHAMMERS[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.WARHAMMERS);
								}
								lastWarriorSuccessRoll = DungeonItems.WARHAMMERS;
							}
							break;

						case 5:// forgotten warrior longsword
							if (lastWarriorSuccessRoll != DungeonItems.LONGSWORDS) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.LONGSWORDS[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.LONGSWORDS);
								}
								lastWarriorSuccessRoll = DungeonItems.LONGSWORDS;
							}
							break;

						case 6:// forgotten warrior rapier
							if (lastWarriorSuccessRoll != DungeonItems.RAPIER) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.RAPIER[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.RAPIER);
								}
								lastWarriorSuccessRoll = DungeonItems.RAPIER;
							}
							break;

						case 7:// forgotten warrior dagger
							if (lastWarriorSuccessRoll != DungeonItems.DAGGERS) {
								if (n.dungeonTier != 1) {
									drop(DungeonItems.DAGGERS[n.dungeonTier], dungeon, n);
								} else {
									doDropping(n, SIMPLE_TABLES, DungeonItems.DAGGERS);
								}
								lastWarriorSuccessRoll = DungeonItems.DAGGERS;
							}
							break;
						}
					}
				}
			}

			dungeon.spawnBelow(n, 526, 1);
			break;
			
		case "forgotten mage":
			int maxMageRolls = 3;
			int mageRolls = Misc.randomNoPlus(0, maxMageRolls);
			for (int i = 0; i < mageRolls; i++) {
				if (Misc.randomNoPlus(ARROWS_RUNES_CHANCE) == 1) {
					doDropping(n, SIMPLE_TABLES, DungeonItems.RUNES);
				}
				if (Misc.randomNoPlus(ARMOUR_WEAPONS_LOOP_CHANCE) == 1) {
					int random = Misc.randomNoPlus(6);
					if (random > 5 && (n.dungeonWeaponIdentifier == 8 || n.dungeonWeaponIdentifier == 9)) {
						switch (n.dungeonWeaponIdentifier) {
							case 8:
								dungeon.spawnBelow(n, 19868, 1);
								break;
							case 9:
								dungeon.spawnBelow(n, 19867, 1);
								break;
						}
					} else if (random > 4) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.ROBE_BOTTOMS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.ROBE_BOTTOMS);
						}
					} else if (random > 6) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.ROBE_TOPS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.ROBE_TOPS);
						}
					} else if (random > 5) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.GLOVES[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.GLOVES);
						}
					} else if (random > 4) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.SHOES[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.SHOES);
						}
					} else {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.HOODS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.HOODS);
						}
					}

				}
			}

			dungeon.spawnBelow(n, 526, 1);
			break;
			
		case "forgotten range":
			int maxRangeRolls = 3;
			int rangeRolls = Misc.randomNoPlus(0, maxRangeRolls);
			for (int i = 0; i < rangeRolls; i++) {
				if (Misc.randomNoPlus(ARROWS_RUNES_CHANCE) == 1) {
					doDropping(n, SIMPLE_TABLES, DungeonItems.ARROWS);
				}
				if (Misc.randomNoPlus(ARMOUR_WEAPONS_LOOP_CHANCE) == 1) {
					int random = Misc.randomNoPlus(7);
					if (random > 6) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.SHORTBOWS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.SHORTBOWS);
						}
					} else if (random > 5) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.LONGBOWS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.LONGBOWS);
						}
					} else if (random > 4) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.CHAPS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.CHAPS);
						}
					} else if (random > 3) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.LEATHER_BODYS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.LEATHER_BODYS);
						}
					} else if (random > 2) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.COIFS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.COIFS);
						}
					} else if (random > 1) {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.LEATHER_BOOTS[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.LEATHER_BOOTS);
						}
					} else {
						if (n.dungeonTier != -1) {
							drop(DungeonItems.VAMBRACES[n.dungeonTier], dungeon, n);
						} else {
							doDropping(n, SIMPLE_TABLES, DungeonItems.VAMBRACES);
						}
					}

				}
			}

			dungeon.spawnBelow(n, 526, 1);
			break;
			
		case "mysterious shade":
			int maxShadeRolls = 3;
			int shadeRolls = Misc.randomNoPlus(0, maxShadeRolls);
			for (int i = 0; i < shadeRolls; i++) {
				if (Misc.randomNoPlus(ARROWS_RUNES_CHANCE) == 1) {
					doDropping(n, SIMPLE_TABLES, DungeonItems.RUNES);
				}
			}
			break;

		case "ice warrior":
			if (Misc.random(ARMOUR_WEAPONS_CHANCE) == 1) {
				if (n.dungeonTier != -1) {
					drop(DungeonItems.RAPIER[n.dungeonTier], dungeon, n);
				} else {
					doDropping(n, SIMPLE_TABLES, DungeonItems.RAPIER); 
				}
			}
			break;
			
		case "icefiend":
			if (Misc.random(UNCOMMON) == 1) {
				dungeon.spawnBelow(n, 17275, 1);
			} else if (Misc.random(RARE) == 2) {
				dungeon.spawnBelow(n, 17277, 1);
			}

			dungeon.spawnBelow(n, 20264, 1);
		    break;
		    
		case "zombie":
			if (Misc.random(ARMOUR_WEAPONS_CHANCE) == 1) {
				if (n.dungeonTier != -1) {
					drop(DungeonItems.HATCHETS[n.dungeonTier], dungeon, n);
				} else {
					 doDropping(n, SIMPLE_TABLES, DungeonItems.HATCHETS);
				}
			}

			dungeon.spawnBelow(n, 526, 1);
			break;
			
		case "hobgoblin":
			if (Misc.random(ARMOUR_WEAPONS_CHANCE) == 1) {
				if (n.dungeonTier != -1) {
			        drop(DungeonItems.SPEARS[n.dungeonTier], dungeon, n);
				} else {
					doDropping(n, SIMPLE_TABLES, DungeonItems.SPEARS);
				}
			}

			dungeon.spawnBelow(n, 526, 1);
			break;
			
		case "animated pickaxe":
			if (Misc.random(ARMOUR_WEAPONS_CHANCE) == 1) {
				if (n.dungeonTier != -1) {
					drop(DungeonItems.PICKAXES[n.dungeonTier], dungeon, n);
				} else { 
					  doDropping(n, SIMPLE_TABLES, DungeonItems.PICKAXES);
				}
			}
			break;
			
		case "brute":
			if (Misc.random(ARMOUR_WEAPONS_CHANCE) == 1) {
				if (n.dungeonTier != -1) {
					drop(DungeonItems.MAULS[n.dungeonTier], dungeon, n);
				} else {
					doDropping(n, SIMPLE_TABLES, DungeonItems.MAULS);
				}
			}

			dungeon.spawnBelow(n, 526, 1);
			break;
			
		case "frost dragon":
			dungeon.spawnBelow(n, 18830, 1);
			break;
			
		case "green dragon":
		case "iron dragon":
		case "red dragon":
		case "black dragon":
			dungeon.spawnBelow(n, 536, 1);
			break;
			
		case "ice giant":
		case "hill giant":
		case "fire giant":
		case "Thrower troll":
		case "ice troll":
		case "giant skeleton":
			dungeon.spawnBelow(n, 532, 1);
			break;
			
		case "lesser demon":
			dungeon.spawnBelow(n, 20266, 1);
			break;
			
		case "reborn mage":
			dungeon.spawnBelow(n, 592, 1);
			break;
			
		case "infernal ashes":
		case "black demon":
			dungeon.spawnBelow(n, 20268, 1);
			break;
			
		case "bat":
		case "giant bat":
			dungeon.spawnBelow(n, 530, 1);
			break;
			
		case "ghost":
		case "ice spider":
		case "ice elemental":
		case "animated book":
			break;
			
			default:
				dungeon.spawnBelow(n, 526, 1);
		        break; 
		}

		if(Misc.random(ARMOUR_WEAPONS_CHANCE)==1) {
		switch (n.dungeonWeaponIdentifier) {
		case 10:// skeleton longswords
			if (n.dungeonTier != -1) {
				drop(DungeonItems.LONGSWORDS[n.dungeonTier], dungeon, n);
			} else {
				doDropping(n, SIMPLE_TABLES, DungeonItems.LONGSWORDS);
			}
			break;

		case 11:// skeleton hatchet
			if (n.dungeonTier != -1) {
				drop(DungeonItems.HATCHETS[n.dungeonTier], dungeon, n);
			} else {
				doDropping(n, SIMPLE_TABLES, DungeonItems.HATCHETS);
			}
			break;

		case 12:// skeleton warhammer
			if (n.dungeonTier != -1) {
				drop(DungeonItems.WARHAMMERS[n.dungeonTier], dungeon, n);
			} else {
				doDropping(n, SIMPLE_TABLES, DungeonItems.WARHAMMERS);
			}
			break;

		case 13:// skeleton rapier
			if (n.dungeonTier != -1) {
				drop(DungeonItems.RAPIER[n.dungeonTier], dungeon, n);
			} else {
				doDropping(n, SIMPLE_TABLES, DungeonItems.RAPIER);
			}
			break;
		}
	}

	}

	private static void doDropping(NPC n, DropTableInfo tableInfo, int[][] items) {
		int npcTier = checkTier(n);
		if (npcTier >= items.length) {
			npcTier = items.length - 1;
		}

		int random;
		if (Misc.randomNoPlus(100) >= 1) {
			if (npcTier > tableInfo.maxTier()) {
				npcTier = tableInfo.maxTier() - 1;
			}
			
			random = npcTier;
		} else {
			List<Integer> indices = new LinkedList<>();
			npcTier++;
			for (int i = 0, length = items.length; i < length; i++) {
				//System.out.println(items[i][0]+":"+tableInfo.minTier()+":"+tableInfo.maxTier()+":"+npcTier+":"+items[i][3]);
				if (items[i][3] >= tableInfo.minTier() && items[i][3] <= tableInfo.maxTier() && items[i][3] <= npcTier) {
					indices.add(i);
					//System.out.println("drop add " + ItemDef.forID(items[i][0]).name);
				}
			}
			
			if (indices.size() == 0) {
				return;
			}

			random = indices.get(Misc.randomNoPlus(indices.size()));
		}

		drop(items[random], n.dungeonManager, n);
	}

	private static void drop(int[] items, DungeonManager dungeon, NPC npc) {
		int count;
		if (items[1] == items[2]) {
			count = items[2];
		} else {
			count = Misc.random(items[1], items[2]);
		}

		if (ItemDef.forID(items[0]).isStackable()) {
			dungeon.spawnBelow(npc, items[0], count);
		} else {
			for (int i = 0; i < count; i++) {
				dungeon.spawnBelow(npc, items[0], 1);
			}
		}
	}

}
