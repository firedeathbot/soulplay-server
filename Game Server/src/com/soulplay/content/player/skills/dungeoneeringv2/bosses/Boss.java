package com.soulplay.content.player.skills.dungeoneeringv2.bosses;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

public abstract class Boss {

	protected NPC boss;

	public Boss(NPC boss) {
		this.boss = boss;
	}

	public void onInit() {

	}

	public abstract void combatLogic(NPC npc, Client c);

	public abstract void process(NPC npc);

	public abstract Prayers protectPrayer(int npcId);

	public abstract int protectPrayerDamageReduction(int damage);// -1 to use default

	public abstract void onSpawn(NPC npc);

	public abstract boolean ignoreMeleeBonus();

	public abstract boolean ignoreRangeBonus();

	public abstract boolean ignoreMageBonus();

	public abstract int followDistance();

	public abstract boolean canMove();

	public abstract int extraAttackDistance();

	public abstract void proxyDamage(NPC npc, Hit hit, Mob source);

	/**
	 * Client c might be null, so make sure to check for null on all npcs that add this to.
	 * @param npc
	 * @param c - MIGHT BE NULL SOMETIMES, SO ALWAYS CHECK FOR NULL
	 */
	public abstract void onDeath(NPC npc, Client c);

	public abstract void afterNpcAttack(NPC npc, Player p, int damage);
	
	public abstract void onHitFromAttackMob(NPC npc, BattleState state);

	public int getAttackSpeed() {
		return -1;
	}

	public int getAttackAnimation(int attackType) {
		return -1;
	}

	public int getDeathAnimation() {
		return -1;
	}

	public int getBlockAnimation() {
		return -1;
	}

	/**
	 * Lets this npc attack a player if another npc is already attacking them
	 */
	public boolean isMultiCombat() {
		return true;
	}
	
	public void onNull(NPC npc) {
	}
	
	public void onDestroy(NPC npc) {
	}

}
