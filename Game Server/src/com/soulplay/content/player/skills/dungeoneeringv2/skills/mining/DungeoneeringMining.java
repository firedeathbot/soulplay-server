package com.soulplay.content.player.skills.dungeoneeringv2.skills.mining;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.util.Misc;

public class DungeoneeringMining {

	private static boolean canMine(Client c, Pickaxes pickaxe, Rocks rock) {
		if (pickaxe == null) {
			c.sendMessage("You do not have a pickaxe or do not have the required level to use the pickaxe.");
			return false;
		}

		if (rock.getLevel() > c.getSkills().getLevel(Skills.MINING)) {
			c.sendMessage("You need a mining level of " + rock.getLevel() + " to mine this rock.");
			return false;
		}

		if (c.getItems().freeSlots() == 0) {
			c.getDialogueBuilder().sendStatement("You don't have enough inventory space.").execute();
			return false;
		}

		return true;
	}

	public static void start(Client c, Rocks rock, GameObject object, DungeonManager manager) {
		Pickaxes pickaxe = Pickaxes.getPickaxe(c);
		if (rock == null || !canMine(c, pickaxe, rock) || c.skillingEvent != null) {
			return;
		}

		c.sendMessage("You swing your pickaxe at the rock...");
		c.startAnimation(pickaxe.getAnimationId());

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (!c.insideDungeoneering() || manager.getRealObject(object.getLocation().getX(), object.getLocation().getY()).getId() != object.getId()) {
					c.resetAnimations();
					container.stop();
					return;
				}
				
				if (rock.getOreId() == 17648) {
					c.getAchievement().mine500Prom();
				}

				c.startAnimation(pickaxe.getAnimationId());
				c.sendMessage("You get some " + ItemDefinition.getName(rock.getOreId()).toLowerCase() + ".");
				c.getItems().addItem(rock.getOreId(), 1);
				c.getPA().addSkillXP(rock.getXp(), Skills.MINING);

				if (Misc.random(5) == 0) {
					manager.spawnObject(object.getId() + 1, object.getLocation().getX(), object.getLocation().getY(), object.getOrientation(), object.getType());
					c.resetAnimations();
					c.sendMessage("You have depleted this resource.");
					container.stop();
					return;
				}

				if (!canMine(c, pickaxe, rock)) {
					c.resetAnimations();
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				Mining.resetMining(c);
			}
		}, 4);
	}

}
