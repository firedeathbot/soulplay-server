package com.soulplay.content.player.skills.dungeoneeringv2.skills.mining;

public enum Rocks {
	NOVITE_ORE(1, 15, 17630, 10, 1),
	BATHUS_ORE(10, 27, 17632, 15, 1),
	MARMAROS_ORE(20, 41, 17634, 25, 1),
	KRATONIUM_ORE(30, 56, 17636, 50, 5),
	FRACTITE_ORE(40, 71, 17638, 80, 10),
	ZEPHYRIUM_ORE(50, 85, 17640, 95, 10),
	AGRONITE_ORE(60, 100, 17642, 100, 15),
	KATAGON_ORE(70, 117, 17644, 110, 20),
	GORGONITE_ORE(80, 131, 17646, 123, 22),
	PROMETHIUM_ORE(90, 148, 17648, 130, 25);

	public static int pickRock(int maxLvl) {
		for(int i = values.length - 1; i >= 0; i--) {
			Rocks rock = values[i];
			if (maxLvl >= rock.level) {
				return i;
			}
		}

		return 0;
	}

	private int level;
	private int xp;
	private int oreId;
	private int oreBaseTime;
	private int oreRandomTime;
	public static final Rocks[] values = values();

	private Rocks(int level, int xp, int oreId, int oreBaseTime, int oreRandomTime) {
		this.level = level;
		this.xp = xp;
		this.oreId = oreId;
		this.oreBaseTime = oreBaseTime;
		this.oreRandomTime = oreRandomTime;
	}

	public int getLevel() {
		return level;
	}

	public int getXp() {
		return xp;
	}

	public int getOreId() {
		return oreId;
	}

	public int getOreBaseTime() {
		return oreBaseTime;
	}

	public int getOreRandomTime() {
		return oreRandomTime;
	}

}