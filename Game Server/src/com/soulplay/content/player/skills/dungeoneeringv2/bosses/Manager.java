package com.soulplay.content.player.skills.dungeoneeringv2.bosses;

import java.lang.reflect.Modifier;
import java.util.ArrayList;

import com.soulplay.Server;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class Manager {
	
	private static final Int2ObjectMap<Class<? extends Boss>> bosses = new Int2ObjectOpenHashMap<>();

	public static void init() {
		FastClasspathScanner scanner = new FastClasspathScanner();
		scanner.matchSubclassesOf(Boss.class, clazz -> {
			if (!clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers()) && clazz.isAnnotationPresent(BossMeta.class)) {
				BossMeta meta = clazz.getAnnotation(BossMeta.class);
				for (int id : meta.ids()) {
					bosses.put(id, clazz);
				}
			}
        }).scan();
	}

	public static Class<? extends Boss> get(int id) {
		Class<? extends Boss> clazz = bosses.get(id);
		if (clazz == null) {
			return null;
		}
		return clazz;
	}

	public static ArrayList<Player> getNearbyPlayers(NPC n) {
		ArrayList<Player> temp = new ArrayList<>();
		for (Player p : Server.getRegionManager().getLocalPlayers(n)) {
			if (p == null || p.disconnected || p.isDead()) {
				continue;
			}
			temp.add(p);
		}
		return temp;
	}

	public static void clear() {
		bosses.clear();
	}

}
