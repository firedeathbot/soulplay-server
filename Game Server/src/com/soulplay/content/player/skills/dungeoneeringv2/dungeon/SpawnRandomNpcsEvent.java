package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

public class SpawnRandomNpcsEvent implements RoomEvent {

	public void openRoom(DungeonManager dungeon, RoomReference reference) {
		dungeon.spawnRandomNPCS(reference);
	}

}
