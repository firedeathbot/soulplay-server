package com.soulplay.content.player.skills.dungeoneeringv2.packets;

import com.soulplay.Config;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Cooking;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.Door;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.KeyDoors;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.SkillDoors;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonUtils;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.Room;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.PuzzleRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.RoomReference;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.VisibleRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting.DungeoneeringCrafting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.farming.DungeoneeringFarming;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.farming.Harvest;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.mining.DungeoneeringMining;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.mining.Pickaxes;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.mining.Rocks;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.runecrafting.DungeoneeringRunecrafting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.Smelting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.SmeltingDataDung;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.woodcutting.DungeoneeringWoodcutting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.woodcutting.Hatchets;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.woodcutting.Trees;
import com.soulplay.content.player.skills.herblore.VialFilling;
import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class DungeonObjects {

	public static boolean handle1(Client c, int id, int x, int y) {
		if (!c.insideDungeoneering()) {
			return false;
		}
		
		DungeonManager dungeon = c.dungParty.dungManager;

		if (!dungeon.hasStarted()) {
			return false;
		}

		GameObject object = dungeon.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());
		//System.out.println("exists " + object);

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processObjectClick1(c, object)) {
			return true;
		}

		Room room = dungeon.getRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		int floorType = DungeonUtils.getFloorType(c.dungParty.floor);
		
		for (SkillDoors s : SkillDoors.values) {
			if (s.getClosedObject(floorType) == id) {
				openSkillDoor(c, s, object, room, floorType, dungeon);
				return true;
			}
		}
		
		String name = object.getDef().name.toLowerCase();
		if (id == DungeonConstants.DUNGEON_DOORS[floorType] || id == DungeonConstants.DUNGEON_GUARDIAN_DOORS[floorType]
				|| id == DungeonConstants.DUNGEON_BOSS_DOORS[floorType] || DungeonUtils.isOpenSkillDoor(id, floorType)
				|| (id >= KeyDoors.getLowestDoorId(floorType) && id <= KeyDoors.getMaxDoorId(floorType))
				|| (name.equals("door")
						&& object.getDef().containsOption(0, "Enter"))) {
			if (id == DungeonConstants.DUNGEON_BOSS_DOORS[floorType] && (c.npcIndex > 0 || c.underAttackBy2 > 0)) {
				c.sendMessage("This door is too complex to unlock while in combat.");
				return true;
			}

			int rotation = object.getOrientation();

			Door door = room.getDoorByRotation(rotation);
			if (door == null) {
				openDoor(c, dungeon, rotation);
				return true;
			}

			if (door.getType() == DungeonConstants.GUARDIAN_DOOR) {
				if (vRoom.roomCleared()/* || c.playerRights == 3*/) {
					room.setDoor(room.getDoorIndexByRotation(rotation), null);
					openDoor(c, dungeon, rotation);
					return true;
				}

				c.sendMessage("The door won't unlock until all of the guardians in the room have been slain.");
			} else if (door.getType() == DungeonConstants.KEY_DOOR || door.getType() == DungeonConstants.SKILL_DOOR) {
				c.sendMessage("The door is locked.");
			} else if (door.getType() == DungeonConstants.CHALLENGE_DOOR) {
				c.sendMessage(((PuzzleRoom) vRoom).getLockMessage());
			}
			return true;
		} else if (id >= KeyDoors.getLowestObjectId() && id <= KeyDoors.getMaxObjectId()) {
			if (room == null) {
				c.sendMessage("Shouldn't happen but, it glitched");
				return true;
			}
			
			int index = room.getDoorIndexByRotation(object.getOrientation());
			if (index == -1) {
				return true;
			}

			Door door = room.getDoor(index);
			if (door == null || door.getType() != DungeonConstants.KEY_DOOR) {
				return true;
			}

			int keyId = KeyDoors.values[door.getId()].getKeyId();
			int keySlot = c.getItems().getItemSlot(keyId);

			if (Config.RUN_ON_DEDI && keySlot == -1) {
				c.sendMessage("You don't have the correct key.");
				return true;
			}

			c.getItems().deleteItemInOneSlot(keyId, keySlot, 1);
			//player.lock(1);
			c.sendMessage("You unlock the door.");
			c.startAnimation(13798);
			room.setDoor(index, null);
			dungeon.getSpawnedObject(object.getLocation().getX(), object.getLocation().getY(), true);
			return true;
		} else if (DungeonUtils.isLadder(object.getId(), floorType)) {
			if (!dungeon.canEnd) {
				c.sendMessage("Can only end when you've killed the boss.");
				return true;
			}

			if (c.dungVoteStage != 0 && Config.NODE_ID != 10) {
				c.sendMessage("You have already voted to move on.");
				return true;
			}

			c.getDialogueBuilder().sendOption("Are you sure you wish to proceed and take your party with you?", "Yes, continue.", () -> {
				if (c.dungVoteStage != 0 && Config.NODE_ID !=10) {
					return;
				}

				c.dungVoteStage = 1;
				dungeon.voteToMoveOn();
			}, "No, wait.", () -> {}).execute();
			return true;
		} else if (id == DungeonConstants.THIEF_CHEST_LOCKED[floorType]) {
			int type = room.getThiefChest();
			if (type == -1) {
				return true;
			}

			int level = type == 0 ? 1 : (type * 10);
			if (level > c.getSkills().getLevel(Skills.THIEVING)) {
				c.sendMessage("You need a " + Skills.SKILL_NAME[Skills.THIEVING] + " level of " + level + " to open this chest.");
				return true;
			}
			room.setThiefChest(-1);
			c.startAnimation(536);
			//player.lock(2);
			dungeon.spawnObject(DungeonConstants.THIEF_CHEST_OPEN[floorType], object.getLocation().getX(), object.getLocation().getY(), object.getOrientation(), object.getType());
			c.getItems().addOrDrop(new GameItem(DungeonConstants.RUSTY_COINS, Misc.randomNoPlus((type + 1) * 10000) + 1));
			if (Misc.randomNoPlus(2) == 0)
				c.getItems().addOrDrop(new GameItem(DungeonConstants.CHARMS[Misc.randomNoPlus(DungeonConstants.CHARMS.length)], Misc.randomNoPlus(5) + 1));
			if (Misc.randomNoPlus(3) == 0)
				c.getItems().addOrDrop(new GameItem(DungeoneeringFarming.getHerbForLevel(level), Misc.randomNoPlus(1) + 1));
			if (Misc.randomNoPlus(4) == 0)
				c.getItems().addOrDrop(new GameItem(DungeonUtils.getArrows(type + 1), Misc.randomNoPlus(100) + 1));
			if (Misc.randomNoPlus(5) == 0)
				c.getItems().addOrDrop(new GameItem(DungeonUtils.getRandomWeapon(type + 1), 1));
			c.getPA().addSkillXP(DungeonConstants.THIEF_CHEST_XP[type], Skills.THIEVING);
			return true;
		} else if (object.getId() == DungeonConstants.THIEF_CHEST_OPEN[floorType]) {
			c.sendMessage("You already looted this chest.");
			return true;
		}
		
		/*if (name.equals("door") && object.getDef().containsOption(0, "Open")) {
			if (!ObjectManager.objectExists(-1, object.getX(), object.getY(), object.getZ()))
				ObjectManager.addObject(new RSObject(-1, object.getX(), object.getY(), object.getZ(), object.getOrientation(), object.getType(), object.getId(), 60));
		}*/

		switch (name) {
		case "altar":
			c.getDialogueBuilder().sendOption("Restore your prayer.", "Yes.", () -> {
				restorePrayer(c);
			}, "No.", () -> {}).execute();
			return true;
		case "group gatestone portal": {
			Location location = DungeonButtons.groupGateTele(dungeon, c);
			if (location == null) {
				c.sendMessage("No gatestone found.");
				return true;
			}

			c.getPA().spellTeleport(location);
			c.getPA().walkableInterface(-1);
			return true;
		}
		case "dungeon exit":
			c.getDialogueBuilder().sendStatement("This ladder leads back to the surface. You will not be able to come", "back to this dungeon if you leave.")
			.sendOption("Leave the dungeon and return to the surface?", "Yes", () -> {
				Party.leaveDungeon(c, true);
			}, "No", () -> {}).execute();
			return true;
		case "anvil":
			for (int i = 0, length = c.playerItems.length; i < length; i++) {
				int itemId = c.playerItems[i] - 1;
				if (SmithingInterface.showSmithInterfaceDung(c, itemId)) {
					return true;
				}
			}

			c.sendMessage("You don't have any bars to smith.");
			return true;
		case "furnace":
			Smelting.initSmelt(c);
			return true;
		case "spinning wheel":
			DungeoneeringCrafting.initSpin(c);
			return true;
		case "salve nettles":
			DungeoneeringFarming.initHarvest(c, Harvest.SALVE_NETTLES, object, dungeon, false);
			return true;
		case "wildercress":
			DungeoneeringFarming.initHarvest(c, Harvest.WILDERCRESS, object, dungeon, false);
			return true;
		case "blightleaf":
			DungeoneeringFarming.initHarvest(c, Harvest.BLIGHTLEAF, object, dungeon, false);
			return true;
		case "roseblood":
			DungeoneeringFarming.initHarvest(c, Harvest.ROSEBLOOD, object, dungeon, false);
			return true;
		case "bryll":
			DungeoneeringFarming.initHarvest(c, Harvest.BRYLL, object, dungeon, false);
			return true;
		case "duskweed":
			DungeoneeringFarming.initHarvest(c, Harvest.DUSKWEED, object, dungeon, false);
			return true;
		case "soulbell":
			DungeoneeringFarming.initHarvest(c, Harvest.SOULBELL, object, dungeon, false);
			return true;
		case "ectograss":
			DungeoneeringFarming.initHarvest(c, Harvest.ECTOGRASS, object, dungeon, false);
			return true;
		case "runeleaf":
			DungeoneeringFarming.initHarvest(c, Harvest.RUNELEAF, object, dungeon, false);
			return true;
		case "spiritbloom":
			DungeoneeringFarming.initHarvest(c, Harvest.SPIRITBLOOM, object, dungeon, false);
			return true;
		case "runecrafting altar":
			DungeoneeringRunecrafting.giveDialog(c);
			return true;
		case "tangle gum tree":
			DungeoneeringWoodcutting.start(c, Trees.TANGLE_GUM_VINE, object, dungeon);
			return true;
		case "seeping elm tree":
			DungeoneeringWoodcutting.start(c, Trees.SEEPING_ELM_TREE, object, dungeon);
			return true;
		case "blood spindle tree":
			DungeoneeringWoodcutting.start(c, Trees.BLOOD_SPINDLE_TREE, object, dungeon);
			return true;
		case "utuku tree":
			DungeoneeringWoodcutting.start(c, Trees.UTUKU_TREE, object, dungeon);
			return true;
		case "spinebeam tree":
			DungeoneeringWoodcutting.start(c, Trees.SPINEBEAM_TREE, object, dungeon);
			return true;
		case "bovistrangler tree":
			DungeoneeringWoodcutting.start(c, Trees.BOVISTRANGLER_TREE, object, dungeon);
			return true;
		case "thigat tree":
			DungeoneeringWoodcutting.start(c, Trees.THIGAT_TREE, object, dungeon);
			return true;
		case "corpsethorn tree":
			DungeoneeringWoodcutting.start(c, Trees.CORPESTHORN_TREE, object, dungeon);
			return true;
		case "entgallow tree":
			DungeoneeringWoodcutting.start(c, Trees.ENTGALLOW_TREE, object, dungeon);
			return true;
		case "grave creeper tree":
			DungeoneeringWoodcutting.start(c, Trees.GRAVE_CREEPER_TREE, object, dungeon);
			return true;
		case "novite ore":
			DungeoneeringMining.start(c, Rocks.NOVITE_ORE, object, dungeon);
			return true;
		case "bathus ore":
			DungeoneeringMining.start(c, Rocks.BATHUS_ORE, object, dungeon);
			return true;
		case "marmaros ore":
			DungeoneeringMining.start(c, Rocks.MARMAROS_ORE, object, dungeon);
			return true;
		case "kratonite ore":
			DungeoneeringMining.start(c, Rocks.KRATONIUM_ORE, object, dungeon);
			return true;
		case "fractite ore":
			DungeoneeringMining.start(c, Rocks.FRACTITE_ORE, object, dungeon);
			return true;
		case "zephyrium ore":
			DungeoneeringMining.start(c, Rocks.ZEPHYRIUM_ORE, object, dungeon);
			return true;
		case "argonite ore":
			DungeoneeringMining.start(c, Rocks.AGRONITE_ORE, object, dungeon);
			return true;
		case "katagon ore":
			DungeoneeringMining.start(c, Rocks.KATAGON_ORE, object, dungeon);
			return true;
		case "gorgonite ore":
			DungeoneeringMining.start(c, Rocks.GORGONITE_ORE, object, dungeon);
			return true;
		case "promethium ore":
			DungeoneeringMining.start(c, Rocks.PROMETHIUM_ORE, object, dungeon);
			return true;
		case "water trough":
			VialFilling.vialFilling(c);
			return true;
		}
		return true;
	}
	

	
	private static void openSkillDoor(Client c, final SkillDoors s, final GameObject object, final Room room, final int floorType, DungeonManager dungeon) {
		final int index = room.getDoorIndexByRotation(object.getOrientation());
		if (index == -1) {
			//System.out.println("index not found on the skill door.");
			return;
		}

		final Door door = room.getDoor(index);
		if (door == null || door.getType() != DungeonConstants.SKILL_DOOR) {
			//System.out.println("door is null or isn't skill door?");
			return;
		}

		int level = c.getSkills().getSkillBoostable(s.getSkillId());

		if (door.getLevel() > level) {
			c.sendMessage("You need a " + Skills.SKILL_NAME[s.getSkillId()] + " level of " + door.getLevel() + " to remove this " + object.getDef().name.toLowerCase() + ".");
			return;
		}

		int openAnim = s.getOpenAnim();
		int failAnim = s.getFailAnim();
		switch (s.getSkillId()) {
			case Skills.CONSTRUCTION:
			case Skills.SMITHING:
				if (!c.getItems().playerHasItem(Smithing.DUNG_HAMMER)) {
					c.sendMessage("You need a hammer to do this.");
					return;
				}
				break;
			case Skills.FIREMAKING:
				if (!c.getItems().playerHasItem(DungeonConstants.TINDERBOX)) {
					c.sendMessage("You need a tinderbox to do this.");
					return;
				}
				break;
			case Skills.FARMING:
				if (!c.getItems().playerHasItem(DungeonConstants.KNIFE)) {
					c.sendMessage("You need a knife to do this.");
					return;
				}
				break;
			case Skills.MINING:
				Pickaxes pickaxe = Pickaxes.getPickaxe(c);
				if (pickaxe == null) {
					c.sendMessage("You do not have a pickaxe or do not have the required level to use the pickaxe.");
					return;
				}

				openAnim = pickaxe.getDoorOpenAnimId();
				failAnim = pickaxe.getDoorFailAnimId();
				break;
			case Skills.WOODCUTTING:
				Hatchets hatchet = Hatchets.getHatchet(c);
				if (hatchet == null) {
					c.sendMessage("You do not have a hatchet or do not have the required level to use the hatchet.");
					return;
				}

				openAnim = hatchet.getDoorOpenAnimId();
				failAnim = hatchet.getDoorFailAnimId();
				break;
		}

		final boolean fail = Misc.randomNoPlus(100) <= 10;
		//player.lock(3);
		if (openAnim != -1)
			c.startAnimation(fail && failAnim != -1 ? failAnim : openAnim);
		if (s.getOpenGfx() != -1 || s.getFailGfx() != -1)
			c.startGraphic(Graphic.create(fail ? s.getFailGfx() : s.getOpenGfx()));
		if (s.getOpenObjectAnim() != -1 && !fail) {
			object.animate(s.getOpenObjectAnim());
		}
		CycleEventHandler.getSingleton().addEvent(room, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (s.getFailAnim() == -1) {
					c.resetAnimations();
				}

				if (!fail) {
					if (room.getDoor(index) == null) {//means someone else opened at the same time
						container.stop();
						return;
					}

					c.getPA().addSkillXP(door.getLevel() * 5 + 10, s.getSkillId());//TODO adjust the xp rates
					room.setDoor(index, null);
					int openId = s.getOpenObject(floorType);
					if (openId == -1) {
						dungeon.getSpawnedObject(object.getLocation().getX(), object.getLocation().getY(), true);
					} else {
						dungeon.setDoor(dungeon.getCurrentRoomReference(object.getLocation()), -1, openId, object.getOrientation());
					}
				} else {
					c.sendMessage(s.getFailMessage());
					c.dealDamage(new Hit(door.getLevel() / 10 * 4, 0, -1));
					if (room.getDoor(index) == null) {//means someone else opened at the same time
						container.stop();
						return;
					}

					if (s.getFailObjectAnim() != -1) {
						object.animate(s.getOpenObjectAnim());
					}
				}
				container.stop();
			}

			@Override
			public void stop() {
			}
		}, 2);//TODO higher delay on fail?
	}

	private static void openDoor(Client c, DungeonManager dungeon, int rotation) {
		RoomReference roomReference = dungeon.getCurrentRoomReference(c.getCurrentLocation());
		dungeon.enterRoom(c, roomReference.getX() + Misc.ROTATION_DIR_X[rotation], roomReference.getY() + Misc.ROTATION_DIR_Y[rotation]);
		c.getPA().walkableInterface(-1);
	}
	
	public static boolean handle2(Client c, int id, int x, int y) {
		if (c.dungParty == null) {
			return false;
		}
		
		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		GameObject object = dungeon.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());
		//System.out.println("exists " + object);

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processObjectClick2(c, object)) {
			return true;
		}

		Harvest harvest = Harvest.idToPatch.get(object.getId());
		if (harvest != null) {
			DungeoneeringFarming.initHarvest(c, harvest, object, dungeon, dungeon.party.getFloorType() == DungeonConstants.WARPED_FLOORS);
			return true;
		}

		String name = object.getDef().name.toLowerCase();
		switch(name) {
			case "altar":
				restorePrayer(c);
				return true;
			case "runecrafting altar":
				DungeoneeringRunecrafting.elementalRunes(c);
				return true;
		}
		return true;
	}

	public static boolean handle3(Client c, int id, int x, int y) {
		if (c.dungParty == null) {
			return false;
		}
		
		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		GameObject object = dungeon.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());
		//System.out.println("exists " + object);

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processObjectClick3(c, object)) {
			return true;
		}

		String name = object.getDef().name.toLowerCase();
		switch(name) {
			case "runecrafting altar":
				DungeoneeringRunecrafting.combatRunes(c);
				return true;
		}
		return true;
	}

	public static boolean handle4(Client c, int id, int x, int y) {
		if (c.dungParty == null) {
			return false;
		}
		
		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		GameObject object = dungeon.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());
		//System.out.println("exists " + object);

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processObjectClick4(c, object)) {
			return true;
		}

		String name = object.getDef().name.toLowerCase();
		switch(name) {
			case "runecrafting altar":
				DungeoneeringRunecrafting.otherRunes(c);
				return true;
		}
		return true;
	}

	public static boolean handle5(Client c, int id, int x, int y) {
		if (c.dungParty == null) {
			return false;
		}
		
		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		GameObject object = dungeon.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());
		//System.out.println("exists " + object);

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processObjectClick5(c, object)) {
			return true;
		}

		String name = object.getDef().name.toLowerCase();
		switch(name) {
			case "runecrafting altar":
				DungeoneeringRunecrafting.staves(c);
				return true;
		}
		return true;
	}

	public static boolean handleItem(Client c, int id, int x, int y, int itemId, int itemSlot) {
		if (c.dungParty == null) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		GameObject object = dungeon.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());
		//System.out.println("exists " + object);

		switch (id) {
			case 49937:
				if (itemId == VialFilling.DUNG_EMPTY_VIAL || itemId == VialFilling.NORMAL_EMPTY_VIAL) {
					VialFilling.vialFilling(c);
				}
				return true;
		}
		
		String name = object.getDef().name.toLowerCase();
		switch (name) {
			case "anvil":
				SmithingInterface.showSmithInterfaceDung(c, itemId);
				return true;
			case "fire":
				Cooking.cookThisFood(c, itemId, id, null);
				return true;
			case "furnace":
				SmeltingDataDung data = SmeltingDataDung.values.get(itemId);
				if (data == null) {
					c.sendMessage("You can't smelt that.");
					return true;
				}

				Smelting.startSmelt(c, data, 28);
				return true;
			case "farming patch":
				DungeoneeringFarming.plantHarvest(itemId, itemSlot, c, object, dungeon, dungeon.party.getFloorType() == DungeonConstants.WARPED_FLOORS);
				return true;
		}

		return true;
	}

	private static void restorePrayer(Client c) {
		if (c.getSkills().getLevel(Skills.PRAYER) >= c.getSkills().getStaticLevel(Skills.PRAYER)) {
			c.sendMessage("You already have full prayer points.");
			return;
		}
		if (c.dungPrayerRecharge <= 0) {
			c.sendMessage("You already used all your prayer charges.");
			return;
		}

		int prayerRestored = c.getSkills().getStaticLevel(Skills.PRAYER) - c.getSkills().getLevel(Skills.PRAYER);
		if (prayerRestored > c.dungPrayerRecharge) {
			prayerRestored = c.dungPrayerRecharge;
		}
		c.prayerDrainCounter = 0;
		c.startAnimation(645);
		c.dungPrayerRecharge -= prayerRestored;
		c.getSkills().incrementPrayerPoints(prayerRestored);
		c.sendMessage("You recharge your prayer points.");
	}

}
