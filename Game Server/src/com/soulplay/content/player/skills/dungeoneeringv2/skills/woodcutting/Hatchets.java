package com.soulplay.content.player.skills.dungeoneeringv2.skills.woodcutting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;

public enum Hatchets {

	//Bind should be here too i think
	NOVITE(new int[] {16361, 15991}, 1, 1, 13118, 13129, 13140),
	BATHUS(new int[] {16363, 15992}, 10, 4, 13119, 13130, 13141),
	MARMAROS(new int[] {16365, 15993}, 20, 5, 13120, 13131, 13142),
	KRATONITE(new int[] {16367, 15994}, 30, 7, 13121, 13132, 13143),
	FRACTITE(new int[] {16369, 15995}, 40, 10, 13122, 13133, 13144),
	ZEPHYRIUM(new int[] {16371, 15996}, 50, 12, 13123, 13134, 13145),
	ARGONITE(new int[] {16373, 15997}, 60, 13, 13124, 13135, 13146),
	KATAGON(new int[] {16375, 15998}, 70, 15, 13125, 13136, 13147),
	GORGONITE(new int[] {16377, 15999}, 80, 17, 13126, 13137, 13148),
	PROMETHIUM(new int[] {16379, 16000}, 90, 19, 13127, 13138, 13149),
	PRIMAL(new int[] {16381, 16001}, 99, 21, 13128, 13139, 13150);

	public static final Hatchets[] values = values();
	private final int levelRequried, axeTime, emoteId, doorOpenAnimId, doorFailAnimId;
	private final int[] itemIds;

	private Hatchets(int[] itemIds, int levelRequried, int axeTime, int emoteId, int doorOpenAnimId, int doorFailAnimId) {
		this.itemIds = itemIds;
		this.levelRequried = levelRequried;
		this.axeTime = axeTime;
		this.emoteId = emoteId;
		this.doorOpenAnimId = doorOpenAnimId;
		this.doorFailAnimId = doorFailAnimId;
	}

	public int[] getItemIds() {
		return itemIds;
	}

	public int getLevelRequried() {
		return levelRequried;
	}

	public int getAxeTime() {
		return axeTime;
	}

	public int getEmoteId() {
		return emoteId;
	}

	public int getDoorOpenAnimId() {
		return doorOpenAnimId;
	}

	public int getDoorFailAnimId() {
		return doorFailAnimId;
	}

	public static Hatchets getHatchet(Client player) {
		for (int i = values.length - 1; i >= 0; i--) {// from best to worst
			Hatchets def = Hatchets.values[i];
			int[] ids = def.getItemIds();
			if (player.getItems().playerHasAnyItem(ids) || player.getItems().playerHasEquippedWeapons(ids)) {
				if (player.getSkills().getLevel(Skills.WOODCUTTING) >= def.levelRequried) {
					return def;
				}
			}
		}

		return null;
	}

}
