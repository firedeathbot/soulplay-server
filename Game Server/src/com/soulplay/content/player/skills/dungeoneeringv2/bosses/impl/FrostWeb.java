package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonNpcs;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {9965, 9966, 9967, 9968, 9969, 9970, 9971, 9972, 9973, 9974, 9975, 9976, 9977, 9978, 9979,
		9980, 9981, 9982, 9983, 9984, 9985, 9986, 9987, 9988, 9989, 9990, 9991, 9992, 9993, 9994, 9995, 9996,
		9997, 9998, 9999, 10000, 10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010, 10011,
		10012, 10013, 10014, 10015, 10016, 10017, 10018, 10019, 10020, 10021})
public class FrostWeb extends Boss {

	private static final int TRANSFORM_TICKS = 40;
	private static final int ICE_THINGY_ID = 2146;
	private static final int ICE_THINGY_TICKS = 5;
	private static final int RANDOM = 40;
	private static final int SPIDER_THRESHOLD = 35;
	private static final int ICE_THRESHOLD = 33;
	private static final int FREEZE_THRESHOLD = 25;

	public FrostWeb(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		DungeonManager dungeon = npc.dungeonManager;
		if (dungeon == null) {
			return;
		}

		if (npc.getStopWatch().complete()) {
			int npcId = npc.getNpcTypeSmart();
			int index = (npcId - 9965) % 3;

			if (index == 0) {// Melee
				if (Misc.random(1) == 0) {
					npcId += 1;
				} else {
					npcId += 2;
				}
			} else if (index == 1) {// Mage
				if (Misc.random(1) == 0) {
					npcId += 1;
				} else {
					npcId -= 1;
				}
			} else {// Range
				if (Misc.random(1) == 0) {
					npcId -= 1;
				} else {
					npcId -= 2;
				}
			}

			npc.transform(npcId);
			npc.getStopWatch().startTimer(TRANSFORM_TICKS);
		}

		npc.dungMultiAttack = false;

		int random = Misc.random(RANDOM);
		if (random >= SPIDER_THRESHOLD) {
			int limit = 5 + dungeon.party.partyMembers.size();
			List<NPC> list = ((ArrayList<NPC>) npc.dungVariables[0]);
			if (list.size() < limit) {
				NPC spider = dungeon.spawnNpc(npc.reference, DungeonNpcs.ICE_SPIDERS);
				if (spider != null) {
					spider.setKillerId(c.getId());
					spider.dropStuff = false;
					spider.dungVariables[0] = npc;
					list.add(spider);
				}
			}
		} else if (random >= ICE_THRESHOLD) {
			multiFreeze(npc, c);
			npc.dungMultiAttack = true;
		} else if (random >= FREEZE_THRESHOLD) {
			int teamAvg = dungeon.teamAvg;
			int minAvg = 85;
			int maxAvg = 3960;

			int spikeDmg = Misc.interpolate(4, 30, minAvg, maxAvg, teamAvg);

			for (int i = 0; i < dungeon.party.partyMembers.size(); i++) {
				Client partyMember = dungeon.party.partyMembers.get(i);
				if (!dungeon.getCurrentRoomReference(partyMember.getCurrentLocation()).equals(npc.reference)) {
					continue;
				}

				int spawnedX = partyMember.absX;
				int spawnedZ = partyMember.absY;
				Server.getStillGraphicsManager().createGfx(spawnedX, spawnedZ, npc.getHeightLevel(), ICE_THINGY_ID, 0, 0);

				CycleEventHandler.getSingleton().addEvent(partyMember, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (partyMember.isDead() || partyMember.teleTimer > 0 || partyMember.getSkills().getLifepoints() <= 0) {
							container.stop();
							return;
						}

						if (partyMember.absX == spawnedX && partyMember.absY == spawnedZ) {
							partyMember.dealDamage(new Hit(Misc.random(spikeDmg), 0, -1));
						}

						container.stop();
					}

					@Override
					public void stop() {
					}
				}, ICE_THINGY_TICKS);
			}
		} else {
			if (Misc.random(1) == 0 && Misc.distanceToPoint(npc.getX(), npc.getY(), c.getX(), c.getY()) == 1) {
				npc.attackType = 0;
				npc.projectileId = -1;
			} else {
				multiFreeze(npc, c);
			}
		}
	}

	public Prayers protectPrayer(int npcId) {
		int index = (npcId - 9965) % 3;
		switch (index) {
		case 0:
			return Prayers.PROTECT_FROM_MELEE;
		case 1:
			return Prayers.PROTECT_FROM_MAGIC;
		case 2:
			return Prayers.PROTECT_FROM_MISSILES;
		}

		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	public void onSpawn(NPC npc) {
		npc.getStopWatch().startTimer(TRANSFORM_TICKS);
		npc.dungVariables[0] = new ArrayList<NPC>();
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}
	
	public int followDistance() {
		return 69;
	}
	
	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}

	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		List<NPC> list = ((ArrayList<NPC>) npc.dungVariables[0]);
		for (int i = 0, length = list.size(); i < length; i++) {
			NPC spider = list.get(i);
			spider.nullNPC();
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		// TODO Auto-generated method stub
		
	}
	
	public void multiFreeze(NPC npc, Client c) {
		for (int i = 0; i < npc.dungeonManager.party.partyMembers.size(); i++) {
			Client partyMember = npc.dungeonManager.party.partyMembers.get(i);
			if (!npc.dungeonManager.getCurrentRoomReference(partyMember.getCurrentLocation()).equals(npc.reference)) {
				continue;
			}

			npc.attackType = 2;
			npc.startAnimation(new Animation(729));
			npc.projectileId = 368;
			if (c.getFreezeTimer() < 1) {
				npc.endGfx = 369;
				npc.endGraphicType = GraphicType.LOW;
				partyMember.applyFreeze(9);
				partyMember.sendMessage("You have been Frozen!");
			} else {
				npc.endGraphicType = GraphicType.HIGH;
				npc.endGfx = 1677;
			}
			if (partyMember.getPA().withinDistanceToNpc(npc, 1)) {
				NPCHandler.stepAway(npc);
			}
		}
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}

}
