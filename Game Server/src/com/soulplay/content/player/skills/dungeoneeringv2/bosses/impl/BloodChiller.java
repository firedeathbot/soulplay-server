package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {10024, 10025, 10026, 10027, 10028, 10029, 10030, 10031, 10032, 10033, 10034, 10035, 10036, 10037, 10038, 10039})
public class BloodChiller extends Boss {

	public BloodChiller(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int random = Misc.random(40);
		if ((Integer) npc.dungVariables[0] == 1) {
			npc.forceChat("DEEP FREEZE!");
			c.setStunned(true);
			if (c.applyFreeze(10)) {//TODO number of players affect duration
				c.sendMessage("You've been frozen solid!");
				c.startGraphic(Graphic.create(369, Graphic.GraphicType.LOW));
			}
			npc.dungVariables[0] = 2;
		} else if ((Integer) npc.dungVariables[0] == 2) {//His next atk cleanse freeze on current player so its instant defreeze
			c.sendMessage("The ice encasing you shatters violently.");
			c.setStunned(false);
			c.setFreezeTimer(-1);
			npc.dungVariables[0] = 0;
		} else if (random >= 20 && (Integer) npc.dungVariables[0] == 0) {
			npc.forceChat("Sleep now, in the bitter cold...");
			npc.dungVariables[0] = 1;
			npc.attackType = 5;
		} else if (random >= 10) {
			npc.attackType = 2;
			npc.projectileId = 1884;//TOOD right gfx id
		} else {
			npc.attackType = 0;
		}
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return damage;
	}

	public void onSpawn(NPC npc) {
		npc.dungVariables[0] = new Integer(0);
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}

	public int followDistance() {
		return 0;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}
	
	@Override
	public void onDeath(NPC npc, Client c) {

	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}
	
}
