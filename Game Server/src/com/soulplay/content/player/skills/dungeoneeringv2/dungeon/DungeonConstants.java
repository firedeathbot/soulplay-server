package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonBosses;
import com.soulplay.content.player.skills.dungeoneeringv2.items.DungeonItems;
import com.soulplay.content.player.skills.dungeoneeringv2.puzzles.Puzzle;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class DungeonConstants {

	public static void load() {
		/* empty */
	}

	public static final int RING_OF_KINSHIP = 15707;
	public static final int ROTATIONS_COUNT = 4;
	public static final Location OUTSIDE = Location.create(3460, 3720, 1);

	/*
	 * floor types
	 */
	public static final int FROZEN_FLOORS = 0, ABANDONED_FLOORS = 1, FURNISHED_FLOORS = 2, OCCULT_FLOORS = 3, WARPED_FLOORS = 4;

	// 4x4, 4x8 and 8x8, 2x1(just boss room for test)
	public static final int[][] DUNGEON_RATIO = new int[][] {
			new int[] { 4, 4 }, new int[] { 4, 8 },
			new int[] { 8, 8 }, new int[] { 3, 3 }
	};
	
	public static final String[] SIZES_NAME = { "Small", "Medium", "Large", "Test" };
	
	public static final int[] RUNES =
	{ 17780, 17781, 17782, 17783, 17784, 17785, 17786, 17787, 17788, 17789, 17790, 17791, 17792, 17793 };

	public static final int RUSTY_COINS = 18201, BANANA = 18199;

	public static final int RUNE_ESSENCE = 17776, FEATHER = 17796;
	
	public static final int SINGLE_DOOR_CLOSED = 51258, SINGLE_DOOR_OPEN = 51259;
	public static final int WARPED_SINGLE_DOOR_CLOSED = 56267, WARPED_SINGLE_DOOR_OPEN = 56268; //apparently 56268 is not open version?
	public static final int OCCULT_SINGLE_DOOR_CLOSED = 54769, OCCULT_SINGLE_DOOR_OPEN = 54780;
	//public static final int OCCULT_SINGLE_DOOR_CLOSED = 54769, OCCULT_SINGLE_DOOR_OPEN = 54780;
	/*
	 * objects handling
	 */
	public static final int[] DUNGEON_DOORS = { 50342, 50343, 50344, 53948, 55762 };
	public static final int[] DUNGEON_BOSS_DOORS = { 50350, 50351, 50352, 53950, 55764 };
	public static final int[] DUNGEON_GUARDIAN_DOORS = { 50346, 50347, 50348, 53949, 55763 };
	public static final int[] LADDERS = { 49696, 49698, 49700, 53748, 55484 };
	
	/*
	 * door directions
	 */
	public static final int EAST_DOOR = 2, WEST_DOOR = 0, NORTH_DOOR = 1, SOUTH_DOOR = 3;
	
	public static final int NORMAL_NPC = 0, GUARDIAN_NPC = 1, FISH_SPOT_NPC = 2, BOSS_NPC = 3, FORGOTTEN_WARRIOR = 4, SLAYER_NPC = 5, HUNTER_NPC = 6, PUZZLE_NPC = 7;
	
	/*
	 * door types
	 */
	public static final int NORMAL_DOOR = 0, GUARDIAN_DOOR = 1, SKILL_DOOR = 2, CHALLENGE_DOOR = 3, KEY_DOOR = 4;

	/*
	 * dungeon sizes
	 */
	public static final int SMALL_DUNGEON = 0, MEDIUM_DUNGEON = 1, LARGE_DUNGEON = 2, TEST_DUNGEON = 3;
	
	public static final int[][] PUZZLE_DOOR_ORDER = new int[][] {
		{ SOUTH_DOOR, WEST_DOOR },
		{ SOUTH_DOOR, EAST_DOOR },
		{ SOUTH_DOOR, NORTH_DOOR },
		{ SOUTH_DOOR, NORTH_DOOR, WEST_DOOR },
		{ SOUTH_DOOR, EAST_DOOR, WEST_DOOR },
		{ SOUTH_DOOR, EAST_DOOR, NORTH_DOOR },
		{ SOUTH_DOOR, EAST_DOOR, NORTH_DOOR, WEST_DOOR },
	};

	public static final int SMUGGLER = 11226;
	
	public static final int[] THIEF_CHEST_LOCKED =
	{ 49345, 49346, 49347, 54407, 32462 };

	public static final int[][] WALL_BASE_X_Y =
	{
	{ 14, 1, 0, 14 },
	{ 1, 1, 14, 0 },
	{ 1, 1, 0, 14 },
	{ 1, 14, 14, 0 } };

	public static final int SET_RESOURCES_MAX_TRY = 300;

	public static final int[] WOODCUTTING_BASE = {
			49702, 49703, 49704, 53750, 55485
	};

	/*
	 * novite ores
	 */
	public static final int[] MINING_RESOURCES =
	{ 49806, 49786, 49766, 53771, 55514 };

	/*
	 * Tangle gum tree
	 */
	public static final int[] WOODCUTTING_RESOURCES =
	{ 49745, 49725, 49705, 53751, 55494 };

	/*
	 * Salve nettles
	 */
	public static final int[] FARMING_RESOURCES =
	{ 49866, 49846, 49826, 53791, 55534 };

	public static int[] spot(int x, int y) {
		return new int[]
		{ x, y };
	}
	
	public static final StartRoom[] START_ROOMS = new StartRoom[]
	{
		// FROZEN_FLOORS
		new StartRoom(14, 632, EAST_DOOR, WEST_DOOR, NORTH_DOOR, SOUTH_DOOR),
		new StartRoom(14, 624, SOUTH_DOOR),
		new StartRoom(14, 626, WEST_DOOR, SOUTH_DOOR),
		new StartRoom(14, 630, WEST_DOOR, NORTH_DOOR, SOUTH_DOOR) };

	public static final NormalRoom[] NORMAL_ROOMS = new NormalRoom[]
	{
		new NormalRoom(8, 240, spot(3, 7), SOUTH_DOOR),
		new NormalRoom(8, 242, spot(3, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(8, 244, spot(3, 12), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(8, 246, spot(4, 13), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(8, 248, spot(3, 12), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(8, 256, spot(12, 12), SOUTH_DOOR),
		new NormalRoom(8, 258, spot(12, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(8, 260, spot(11, 6), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(8, 262, spot(12, 6), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR),
		new NormalRoom(8, 264, spot(12, 12), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(10, 240, spot(3, 3), SOUTH_DOOR),
		new NormalRoom(10, 242, spot(7, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(10, 244, spot(2, 4), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(10, 246, spot(2, 12), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(10, 248, spot(2, 12), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(10, 256, spot(8, 12), SOUTH_DOOR),
		new NormalRoom(10, 258, spot(12, 2), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(10, 260, spot(3, 4), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(10, 262, spot(7, 8), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR),
		new NormalRoom(10, 264, spot(5, 10), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(12, 240, spot(3, 7), SOUTH_DOOR),
		new NormalRoom(12, 242, spot(11, 7), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(12, 244, spot(3, 12), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(12, 246, spot(10, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(12, 248, spot(10, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(12, 256, spot(9, 7), SOUTH_DOOR),
		new NormalRoom(12, 258, spot(3, 6), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(12, 260, spot(4, 10), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(12, 262, spot(9, 7), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR),
		new NormalRoom(12, 264, spot(3, 6), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(14, 240, spot(7, 10), SOUTH_DOOR),

		new NormalRoom(14, 256, spot(4, 13), SOUTH_DOOR),
		new NormalRoom(14, 258, spot(12, 9), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(14, 260, spot(12, 13), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(14, 262, spot(7, 7), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR),
		new NormalRoom(14, 264, spot(11, 13), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(16, 240, spot(7, 10), SOUTH_DOOR),

		new NormalRoom(16, 256, spot(4, 12), SOUTH_DOOR),
		new NormalRoom(16, 258, spot(7, 11), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(16, 260, spot(4, 3), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(16, 262, spot(7, 8), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR),
		new NormalRoom(16, 264, spot(7, 7), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(18, 240, spot(7, 10), SOUTH_DOOR),
		new NormalRoom(18, 260, spot(5, 12), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(18, 264, spot(3, 12), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(20, 240, spot(9, 13), SOUTH_DOOR),

		new NormalRoom(20, 258, spot(11, 9), SOUTH_DOOR, WEST_DOOR),

		new NormalRoom(22, 240, spot(8, 9), SOUTH_DOOR),

		new NormalRoom(22, 256, spot(7, 10), SOUTH_DOOR),
		new NormalRoom(22, 258, spot(11, 10), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(22, 260, spot(7, 8), SOUTH_DOOR, NORTH_DOOR),
		new NormalRoom(22, 262, spot(7, 7), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR),
		new NormalRoom(22, 264, spot(7, 7), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(24, 242, spot(7, 7), SOUTH_DOOR, WEST_DOOR),

		new NormalRoom(24, 258, spot(12, 10), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(24, 264, spot(7, 7), SOUTH_DOOR, NORTH_DOOR, WEST_DOOR, EAST_DOOR),

		new NormalRoom(26, 242, spot(9, 9), SOUTH_DOOR, WEST_DOOR),

		new NormalRoom(28, 246, spot(11, 8), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(30, 244, spot(3, 6), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(32, 244, spot(3, 12), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(34, 242, spot(12, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(36, 248, spot(2, 13), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(38, 246, spot(12, 10), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(40, 244, spot(3, 12), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(42, 244, spot(7, 7), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(44, 244, spot(7, 7), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(46, 244, spot(7, 7), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(48, 244, spot(7, 7), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(50, 244, spot(7, 7), NORTH_DOOR, SOUTH_DOOR),

		new NormalRoom(52, 240, spot(7, 11), SOUTH_DOOR),
		new NormalRoom(54, 240, spot(8, 11), SOUTH_DOOR),
		new NormalRoom(56, 240, spot(3, 10), SOUTH_DOOR),
		new NormalRoom(58, 240, spot(3, 12), SOUTH_DOOR),
		new NormalRoom(60, 240, spot(9, 12), SOUTH_DOOR),
		new NormalRoom(62, 240, spot(12, 12), SOUTH_DOOR),
		new NormalRoom(64, 240, spot(2, 12), SOUTH_DOOR),
		new NormalRoom(66, 240, spot(8, 10), SOUTH_DOOR),
		new NormalRoom(68, 240, spot(3, 7), SOUTH_DOOR),
		new NormalRoom(70, 240, spot(8, 12), SOUTH_DOOR),
		new NormalRoom(72, 240, spot(12, 7), SOUTH_DOOR),
		new NormalRoom(74, 240, spot(4, 10), SOUTH_DOOR),
		new NormalRoom(76, 240, spot(8, 7), SOUTH_DOOR),
		new NormalRoom(78, 240, spot(3, 4), SOUTH_DOOR),

		new NormalRoom(62, 242, spot(13, 11), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(62, 244, spot(2, 3), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(62, 246, spot(13, 11), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(62, 248, spot(2, 3), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(64, 242, spot(7, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(64, 244, spot(13, 12), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(64, 246, spot(7, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(64, 248, spot(7, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(66, 242, spot(12, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(66, 244, spot(3, 4), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(66, 246, spot(12, 12), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(66, 248, spot(3, 3), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(68, 242, spot(12, 8), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(68, 244, spot(5, 8), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(68, 246, spot(5, 8), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(70, 242, spot(11, 10), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(70, 244, spot(8, 7), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(70, 246, spot(8, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(72, 242, spot(9, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(72, 244, spot(10, 8), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(72, 246, spot(11, 8), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(72, 248, spot(8, 5), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(74, 242, spot(3, 6), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(74, 244, spot(7, 5), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(74, 246, spot(3, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(74, 248, spot(2, 6), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(76, 242, spot(11, 9), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(76, 244, spot(7, 7), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(76, 246, spot(7, 7), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(76, 248, spot(7, 11), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		new NormalRoom(78, 242, spot(12, 12), SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(78, 244, spot(12, 12), NORTH_DOOR, SOUTH_DOOR),
		new NormalRoom(78, 246, spot(12, 3), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR),
		new NormalRoom(78, 248, spot(2, 3), NORTH_DOOR, SOUTH_DOOR, WEST_DOOR, EAST_DOOR), };
	
	public static enum MapRoomIcon {
		MAP_1(744, 729, true, SOUTH_DOOR),
		MAP_2(745, 730, true, WEST_DOOR),
		MAP_3(751, 735, true, NORTH_DOOR),
		MAP_4(746, 731, true, EAST_DOOR),
		MAP_5(747, 732, true, WEST_DOOR, SOUTH_DOOR),
		MAP_6(748, 733, true, WEST_DOOR, NORTH_DOOR),
		MAP_7(749, 734, true, NORTH_DOOR, EAST_DOOR),
		MAP_8(750, 736, true, SOUTH_DOOR, EAST_DOOR),
		MAP_9(752, 737, true, SOUTH_DOOR, WEST_DOOR, NORTH_DOOR),
		MAP_10(753, 738, true, WEST_DOOR, NORTH_DOOR, EAST_DOOR),
		MAP_11(754, 739, true, SOUTH_DOOR, NORTH_DOOR, EAST_DOOR),
		MAP_12(755, 740, true, SOUTH_DOOR, WEST_DOOR, EAST_DOOR),
		MAP_13(756, 741, true, SOUTH_DOOR, NORTH_DOOR, EAST_DOOR, WEST_DOOR),
		MAP_14(757, 742, true, SOUTH_DOOR, NORTH_DOOR),
		MAP_15(758, 743, true, WEST_DOOR, EAST_DOOR),
		MAP_16(759, 763, false, SOUTH_DOOR),
		MAP_17(760, 764, false, WEST_DOOR),
		MAP_18(761, 765, false, NORTH_DOOR),
		MAP_19(762, 766, false, EAST_DOOR);

		public static final MapRoomIcon[] values = values();
		private int[] doorsDirections;
		private int spriteId;
		private int spriteGuideId;
		private boolean open;

		private MapRoomIcon(int spriteId, int spriteGuideId, boolean open, int... doorsDirections) {
			this.doorsDirections = doorsDirections;
			this.spriteGuideId = spriteGuideId;
			this.open = open;
			this.spriteId = spriteId;
		}

		public int[] getDoorDirections() {
			return doorsDirections;
		}

		public boolean hasSouthDoor() {
			return hasDoor(SOUTH_DOOR);
		}

		public boolean hasNorthDoor() {
			return hasDoor(NORTH_DOOR);
		}

		public boolean hasWestDoor() {
			return hasDoor(WEST_DOOR);
		}

		public boolean hasEastDoor() {
			return hasDoor(EAST_DOOR);
		}

		public int getSpriteGuideId() {
			return spriteGuideId;
		}

		public boolean hasDoor(int direction) {
			for (int dir : doorsDirections)
				if (dir == direction)
					return true;
			return false;
		}

		public int getSpriteId() {
			return spriteId;
		}

		public boolean isOpen() {
			return open;
		}
	}

	public static final HandledRoom[] PUZZLE_ROOMS;
	
	static {
		int ptr = 0;
		PUZZLE_ROOMS = new HandledRoom[Puzzle.values().length * PUZZLE_DOOR_ORDER.length];
		for (Puzzle puzzle : Puzzle.values()) {
			for (int i = 0; i < PUZZLE_DOOR_ORDER.length; i++) {
				PUZZLE_ROOMS[ptr++] = new HandledPuzzleRoom(i, puzzle);
			}
		}
	}
	
	public static final BossRoom[][] BOSS_ROOMS =//TODO all this shit lol
	{
		// FROZEN_FLOORS
		{

			//To'Kash The Blood Chiller
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.TOKASH_THE_BLOODCHILLER, 5, 10);
					npc.dungItemData = DungeonItems.GLOVES;
				}
			}, 820, 9, 26, 626),*/

			//Plane-freezer Lakhrahnaz
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.PLANE_FREEZER_LAKHRAHNAZ, 7, 11);
					npc.dungItemData = DungeonItems.GAUNTLETS;
				}
			}, 819, 6, 26, 624),*/

			//Luminscent Icefiend
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.LUMINESCENT_ICEFIEND, 7, 11);
					npc.dungItemData = DungeonItems.DAGGERS;
				}
			}, 818, 3, 26, 626),*/

			//Icy Bones
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.ICY_BONES, 7, 11);
					npc.dungItemData = DungeonItems.VAMBRACES;
				}
			}, 815, 1, 24, 626),*/

			// Gluttonous behemoth
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.GLUTTONOUS_BEHEMOTH, 5, 10);
					GameObject food1 = dungeon.spawnObject(reference, 49283, 10, 3, 1, 11);
					npc.dungVariables[3] = food1;
					npc.dungVariables[8] = npc.getCurrentLocation().transform(Direction.getLogicalDirection(npc.getCurrentLocation(), food1.getLocation()));
					if (dungeon.party.partyMembers.size() >= 2) {
						GameObject food2 = dungeon.spawnObject(reference, 49283, 10, 2, 11, 12);
						npc.dungVariables[4] = food2;
						npc.dungVariables[9] = npc.getCurrentLocation().transform(Direction.getLogicalDirection(npc.getCurrentLocation(), food2.getLocation()));
					}
					npc.dungItemData = DungeonItems.BOOTS;
				}
			}, 817, 1, 28, 624),

			// Astea Frostweb
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.ASEA_FROSTWEB, 8, 12);
					npc.dungItemData = DungeonItems.SHOES;
				}
			}, 816, 1, 30, 624) },
		//ABANDONED BOSSES
		{
			//Bal'Lak The Pummeler
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.BALLAK_THE_PUMMELLER, 6, 8);
					npc.dungItemData = DungeonItems.WARHAMMERS;
			    }
			}, 786, 33, 26, 642),*/

			//Shadow-forger Ihlakhizan
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.SHADOW_FORGER_IHLAKHIZAN, 6, 6);
					npc.dungItemData = DungeonItems.KITESHIELDS;
				}
			}, 783, 30, 28, 640),*/

			//Unholy cursebearer
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.UNHOLY_CURSEBEARER, 6, 8);
					npc.dungItemData = DungeonItems.UPGRADED_STAFFS;
				}
			}, 784, 15, 26, 640),*/

			//Bulwark beast
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.BULWARK_BEAST, 5, 10);
					npc.dungItemData = DungeonItems.PICKAXES;//TODO: add hatcet aswell
				}
			}, 785, 12, 24, 642),

			//Hobgoblin Geomancer
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.HOBGOBLIN_GEOMANCER, 6, 8);
					npc.dungItemData = DungeonItems.LEATHER_BOOTS;
				}
			}, 787, 12, 24, 640),*/

			//Divine Skinweaver HORDE BOSS
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					dungeon.spawnNPC(reference, 10058, 5, 2, false, BOSS_NPC); //TODO: check this
					//npc.dungItemData = DungeonItems.FULL_HELM; 
				}
			}, 782, 12, 30, 640)*/
		},
		//FURNISHED BOSSES
		{

			//Night-gazer Khighorahk
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.NIGHT_GAZER_KHIGHORAHK, 6, 6);
					npc.dungItemData = DungeonItems.PLATESKIRTS;
				}
			}, 759, 26, 24, 656),*/

			//Sagittare
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.SAGITTARE, 1, 1);
					npc.dungItemData = DungeonItems.LONGBOWS;
				}
			}, 768, 18, 24, 658),//TODO change floor back to 23

			//Lexicus Runewright
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.LEXICUS_RUNEWRIGHT, 8, 7);
					npc.dungItemData = DungeonItems.HOODS;
				}
			}, 738, 20, 26, 658),*/

			//Lakk The Rift Splitter
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.HARLAKK_THE_RIFTSPLITTER, 6, 6);
					npc.dungItemData = DungeonItems.RAPIER;
				}
			}, 767, 18, 28, 656),*/

			//Stomp
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.STOMP, 6, 9);
					npc.dungItemData = DungeonItems.COIFS;
				}
			}, 743, 18, 30, 656),*/

			//Rammernaut
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					//this one appears at middle of arena instead
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.RAMMERNAUT, 7, 7);
					npc.dungItemData = DungeonItems.MAULS;
				}
			}, 746, 18, 26, 656)*/
		},
		{

			//Yk'lagoor the thunderous
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.YKLAGOR_THE_THUNDEROUS, 5, 5);
					npc.dungItemData = DungeonItems.BATTLEAXES;
				}

			}, 890, 45, 26, 672),*/

			//Flesh-spoiler Haasghenahk
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.FLESH_SPOILER_HAASGHENAHK, 8, 7);
					npc.dungItemData = DungeonItems.PLATELEGS;
				}

			}, 892, 42, 24, 674),*/

			//Necro Lord
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.NECROLORD, 7, 10);
					npc.dungItemData = DungeonItems.CHAPS;
				}

			}, 876, 36, 30, 672),//TODO change back to 39

			//GRAVECREEPER
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.GRAVECREEPER, 5, 5);
					npc.dungItemData = DungeonItems.CHAINBODYS;
				}

			}, 891, 36, 28, 672),*/

			//Runebound Behemoth
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					dungeon.spawnNPC(reference, 11812, 6, 8, false, BOSS_NPC);
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.RUNDEBOUND_BEHEMOTH, 6, 8);
					npc.dungItemData = DungeonItems.ROBE_BOTTOMS;
				}

			}, 877, 36, 26, 674),*/

			//Skeletal Trio
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) { //TODO: make them all spawn in same avg
					dungeon.spawnNPC(reference, 11940, 5, 6, false, BOSS_NPC, true);
					dungeon.spawnNPC(reference, 12044, 7, 9, false, BOSS_NPC, true);
					dungeon.spawnNPC(reference, 11999, 7, 6, false, BOSS_NPC, true);
					dungeon.spawnBosses(reference, DungeonBosses.PLANE_FREEZER_LAKHRAHNAZ, 7, 11);
					dungeon.spawnBosses(reference, DungeonBosses.PLANE_FREEZER_LAKHRAHNAZ, 7, 11);
					dungeon.spawnBosses(reference, DungeonBosses.PLANE_FREEZER_LAKHRAHNAZ, 7, 11);
					//npc.dungItemData = DungeonItems.SHORTBOWS;
				}

			}, 865, 36, 24, 672)*/
		},

		//201 5513 0 for blink //TODO: dafuk ? xD
		{

			//Warmonger
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.KALGER_THE_WARMONGER, 6, 9);
					npc.dungItemData = DungeonItems.TWO_HANDED_SWORDS;
				}

			}, 924, 48, 24, 690),//TODO Change back to 75

			//World Gorger
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.WORLD_GORGER_SHUKARHAZH, 5, 6);
					npc.dungItemData = DungeonItems.SPEARS;
				}

			}, 937, 54, 26, 690),*/

			//Hope Devourer
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.HOPE_DEVOURER, 8, 6);
					npc.dungItemData = DungeonItems.LEATHER_BODYS;
				}

			}, 916, 51, 30, 688),*/

			//Blink
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.BLINK, 5, 6);
					npc.dungItemData = DungeonItems.LONGSWORDS;
				}

			}, 940, 48, 24, 688),*/

			//Dreadnaut
			new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.DREADNAUT, 7, 5);
					npc.dungItemData = DungeonItems.PLATEBODYS;
				}

			}, 913, 48, 28, 688),

			//Warped Gulega
			/*new BossRoom(new RoomEvent() {
				@Override
				public void openRoom(DungeonManager dungeon, RoomReference reference) {
					NPC npc = dungeon.spawnBosses(reference, DungeonBosses.WARPED_GULEGA, 8, 8);
					npc.dungItemData = DungeonItems.ROBE_TOPS;
				}

			}, 922, 48, 26, 688)*/
		}
	};
	
	/*
	 * keydoors only require the unlock objectid and itemid
	 */
	public static enum KeyDoors {
		ORANGE_TRIANGLE(0),
		ORANGE_DIAMOND(1),
		ORANGE_RECTANGLE(2),
		ORANGE_PENTAGON(3),
		ORANGE_CORNER(4),
		ORANGE_CRESCENT(5),
		ORANGE_WEDGE(6),
		ORANGE_SHIELD(7),
		SILVER_TRIANGLE(8),
		SILVER_DIAMOND(9),
		SILVER_RECTANGLE(10),
		SILVER_PENTAGON(11),
		SILVER_CORNER(12),
		SILVER_CRESCENT(13),
		SILVER_WEDGE(14),
		SILVER_SHIELD(15),
		YELLOW_TRIANGLE(16),
		YELLOW_DIAMOND(17),
		YELLOW_RECTANGLE(18),
		YELLOW_PENTAGON(19),
		YELLOW_CORNER(20),
		YELLOW_CRESCENT(21),
		YELLOW_WEDGE(22),
		YELLOW_SHIELD(23),
		GREEN_TRIANGLE(24),
		GREEN_DIAMOND(25),
		GREEN_RECTANGLE(26),
		GREEN_PENTAGON(27),
		GREEN_CORNER(28),
		GREEN_CRESCENT(29),
		GREEN_WEDGE(30),
		GREEN_SHIELD(31),
		BLUE_TRIANGLE(32),
		BLUE_DIAMOND(33),
		BLUE_RECTANGLE(34),
		BLUE_PENTAGON(35),
		BLUE_CORNER(36),
		BLUE_CRESCENT(37),
		BLUE_WEDGE(38),
		BLUE_SHIELD(39),
		PURPLE_TRIANGLE(40),
		PURPLE_DIAMOND(41),
		PURPLE_RECTANGLE(42),
		PURPLE_PENTAGON(43),
		PURPLE_CORNER(44),
		PURPLE_CRESCENT(45),
		PURPLE_WEDGE(46),
		PURPLE_SHIELD(47),
		CRIMSON_TRIANGLE(48),
		CRIMSON_DIAMOND(49),
		CRIMSON_RECTANGLE(50),
		CRIMSON_PENTAGON(51),
		CRIMSON_CORNER(52),
		CRIMSON_CRESCENT(53),
		CRIMSON_WEDGE(54),
		CRIMSON_SHIELD(55),
		GOLD_TRIANGLE(56),
		GOLD_DIAMOND(57),
		GOLD_RECTANGLE(58),
		GOLD_PENTAGON(59),
		GOLD_CORNER(60),
		GOLD_CRESCENT(61),
		GOLD_WEDGE(62),
		GOLD_SHIELD(63);

		public static final Set<Integer> KEYS_SET = new HashSet<>();
		public static final KeyDoors[] values = values();
		private final int index;

		private KeyDoors(int index) {
			this.index = index;
		}

		public int getObjectId() {
			return getLowestObjectId() + index;
		}

		public int getDoorId(int floorType) {
			return getLowestDoorId(floorType) + index;
		}

		public static int getLowestObjectId() {
			return 50208;
		}

		public static int getLowestDoorId(int floorType) {
			return DUNGEON_KEY_DOORS[floorType];
		}

		public static int getMaxObjectId() {
			return 50208 + values.length;
		}

		public static int getMaxDoorId(int floorType) {
			return DUNGEON_KEY_DOORS[floorType] + values.length;
		}

		public int getKeyId() {
			return getLowestKeyId() + (index * 2);
		}

		public static int getLowestKeyId() {
			return 18202;
		}

		public int getIndex() {
			return index;
		}

		static {
			for (KeyDoors data : values) {
				KEYS_SET.add(data.getKeyId());
			}
		}
	}

	public static final int[] DUNGEON_KEY_DOORS =
		{ 50353, 50417, 50481, 53884, 55675 };

	/*
	 * Creatures
	 */
	public static final int[][] GUARDIAN_CREATURES =
	{
		//ALL FLOORs
		{ 10831, 10906, 10910, 10821, 10630, 10680, 10693, 10364, 10480, 10726 }
		// FROZEN
		,
		{ 10618, 10460, 10762, 10157, 10782, 10225, 10212, 10791, 10770 }
		// ABANDONED
		,
		{ 10168, 10496, 10178, 10469, 10604, 10706, 10706 }
		// Furnished
		,
		{ 10797, 10496, 10776, 10706 }
		// Occult
		,
		{ 10744, 10722, 10496, 10754, 10718, 10188, 10492, 10815 }
		// Warped
		,
		{ 10744, 10736, 10722, 10219, 10496, 10754, 10718, 10492, 12941 } };

	public static final int[][] FORGOTTEN_WARRIORS =
	{
		//warrior
		{ 10507, 10439, 10246, 10288, 10528 },
		//mage
		{ 10560, 10570, 10575, 10580, 10585, 10590, 10595, 10600 },
		//range
		{ 10320, 10325, 10330, 10335, 10340, 10345, 10350, 10355, 10360 } };

	public static final int[] SLAYER_CREATURES =
	{ 10694, 10695, 10696, 10697, 10698, 10699, 10700, 10701, 10702, 10704, 10705 };

	/*
	 * id - lvl
	 */
	public static final int[] HUNTER_CREATURES =
	{ 11086, 11087, 11088, 11089, 11090, 11091, 11092, 11093, 11094, 11095 };

	public static final int FISH_SPOT_OBJECT_ID = 49922, FISH_SPOT_NPC_ID = 2859;
	public static final int GROUP_GATESTONE = 18829, GATESTONE = 17489;
	public static final int TINDERBOX = 17678;
	public static final int KNIFE = 17754;
	public static final int[] THIEF_CHEST_XP = { 25, 57, 115, 209, 331, 485, 661, 876, 1118, 1410 };
	public static final int[] CHARMS = { 18017, 18018, 18019, 18020 };
	public static final int[] THIEF_CHEST_OPEN = { 49348, 49349, 49350, 54408, 32679 };
	public static final int FARMING_PATCH_CLEAN = 50076;
	public static final int FARMING_PATCH_CLEAN_WARPED = 55567;

	public static enum SkillDoors {
		RUNED_DOOR(Skills.RUNECRAFTING, new int[]
		{ 50278, 50279, 50280, 53953, 55741 }, 791, 186, -1, -1, -1, -1, "You imbue the door with the wrong type of rune energy, and it reacts explosively."),
		BARRED_DOOR(Skills.STRENGTH, new int[]
		{ 50272, 50273, 50274, 53951, 55739 }, new int[]
		{ 50275, 50276, 50277, 53952, 55740 }, -1, -1, -1, -1, -1, -1, "You pull a muscle while attempting to move the plank."),
		PILE_OF_ROCKS(Skills.MINING, new int[]
		{ 50305, 50306, 50307, 53962, 55750 }, -2, -1, 13559, -1, -1, 2420, "You fail to mine the obstruction, and are harmed by falling debris."),
		FLAMMABLE_DEBRIS(Skills.FIREMAKING, new int[]
		{ 50314, 50315, 50316, 53965, 55753 }, 16700, -1, 13562, 13563, -1, -1, "The pile of debris fails to ignite. The same cannot be said for your clothes."),
		MAGICAL_BARRIER(Skills.MAGIC, new int[]
		{ 50329, 50330, 50331, 53970, 55758 }, 4411, -1, 13551, 13550, -1, -1, "You fail to dispel the barrier and take a surge of magical energy to the face."),
		DARK_SPIRIT(Skills.PRAYER, new int[]
		{ 50332, 50333, 50334, 53971, 55759 }, 645, -1, 13557, 13556, -1, -1, "The gods snub your prayer, and the dark spirit attacks you."),
		WOODEN_BARRICADE(Skills.WOODCUTTING, new int[]
		{ 50317, 50318, 50319, 53966, 55754 }, new int[]
		{ 50320, 50321, 50322, 53967, 55755 }, -3, -1, 13582, -1, -1, -1, "You swing the axe against the grain and are showered with sharp splinters of wood."),
		BROKEN_KEY_DOOR(Skills.SMITHING, new int[]
		{ 50308, 50309, 50310, 53963, 55751 }, new int[]
		{ 50311, 50312, 50313, 53964, 55752 }, 13759, -1, -1, -1, -1, -1, "You hit your hand with the hammer. Needless to say, the key is still broken."),
		BROKEN_PULLEY_DOOR(Skills.CRAFTING, new int[]
		{ 50299, 50300, 50301, 53960, 55748 }, new int[]
		{ 50302, 50303, 50304, 53961, 55749 }, 13547, -1, -1, -1, -1, -1, "The rope snaps again as you attempt to fix it, and you crush your hands in the mechanism."),
		LOCKED_DOOR(Skills.AGILITY, new int[]
		{ -1, 50288, 50289, 53956, 55744 }, new int[]
		{ 50290, 50291, 50292, 53957, 55745 }, 14550, -1, -1, -1, -1, -1, "You miss the chain, and set off the trap."),
		PADLOCKED_DOOR(Skills.THIEVING, new int[]
		{ -1, 50294, 50295, 53958, 55746 }, new int[]
		{ 50296, 50297, 50298, 53959, 55747 }, 14568, -1, -1, -1, 14569, -1, "You set off a booby trap inside the lock, and fail to pick it."),
		RAMOKEE_EXILE(Skills.SUMMONING, new int[]
		{ -1, 50327, 50328, 53969, 55757 }, 725, 1207, -1, -1, -1, -1, "You fail to dismiss the rogue familiar, and it punches you in anger."),
		LIQUID_LOCK_DOOR(Skills.HERBLORE, new int[]
		{ -1, 50336, 50337, 53972, 55760 }, new int[]
		{ 50338, 50339, 50340, 53973, 55761 }, 14568, -1, -1, -1, -1, -1, "You incorrectly mix the ingredients, making it explode."),
		VINE_COVERED_DOOR(Skills.FARMING, new int[]
		{ -1, 50324, 50325, 53968, 55756 }, 2275, -1, 13572, -1, -1, -1, "You hurt your hands on the vicious thorns covering the vines."),
		COLLAPSING_DOORFRAME(Skills.CONSTRUCTION, new int[]
		{ -1, 50282, 50283, 53954, 55742 }, new int[]
		{ 50284, 50285, 50286, 53955, 55743 }, 14566, -1, -1, -1, 14567, -1, "You dislodge some debris while attempting to fix the door, and it falls on you.");

		private SkillDoors(int skillId, int[] closedThemeObjects, int openAnim, int openGfx, int openObjectAnim, int failObjectAnim, int failAnim, int failGfx, String failMessage) {
			this(skillId, closedThemeObjects, null, openAnim, openGfx, openObjectAnim, failObjectAnim, failAnim, failGfx, failMessage);
		}

		public static final SkillDoors[] values = values();
		private final int skillId, openAnim, openGfx, openObjectAnim, failObjectAnim, failAnim, failGfx;
		private final int[] closedThemeObjects, openThemeObjects;
		private final String failMessage;

		/*
		 * set openThemeObjects if u want it to disappear after open
		 */
		private SkillDoors(int skillId, int[] closedThemeObjects, int[] openThemeObjects, int openAnim, int openGfx, int openObjectAnim, int failObjectAnim, int failAnim, int failGfx, String failMessage) {
			this.skillId = skillId;
			this.closedThemeObjects = closedThemeObjects;
			this.openThemeObjects = openThemeObjects;
			this.failMessage = failMessage;
			this.openAnim = openAnim;
			this.openGfx = openGfx;
			this.openObjectAnim = openObjectAnim;
			this.failObjectAnim = failObjectAnim;
			this.failGfx = failGfx;
			this.failAnim = failAnim;
		}

		public int getSkillId() {
			return skillId;
		}

		public int getFailGfx() {
			return failGfx;
		}

		public int getFailAnim() {
			return failAnim;
		}

		public int getFailObjectAnim() {
			return failObjectAnim;
		}

		public int getOpenObjectAnim() {
			return openObjectAnim;
		}

		public int getOpenAnim() {
			return openAnim;
		}

		public int getOpenGfx() {
			return openGfx;
		}

		public int getClosedObject(int type) {
			return closedThemeObjects[type];
		}

		public int getOpenObject(int type) {
			return openThemeObjects == null ? -1 : openThemeObjects[type];
		}

		public String getFailMessage() {
			return failMessage;
		}
	}
	
}
