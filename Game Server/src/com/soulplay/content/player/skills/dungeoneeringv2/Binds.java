package com.soulplay.content.player.skills.dungeoneeringv2;

import com.google.gson.reflect.TypeToken;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.util.Misc;

public class Binds {

	private static final String[] NAMES = { "primary", "secondary", "tertiary" };
	private static final int ROWS = 3;
	public static final int ITEMS_PER_ROW = 5;
	private static final int BOUND_ITEMS = 12;
	private static final int INTERFACE_ID = 43933;
	private static final int ROW1_INTERFACE_ID = 29991;
	private static final int ROW2_INTERFACE_ID = 29975;
	private static final int ROW3_INTERFACE_ID = 29974;
	private static final int MAX_COUNT = 200;

	public Player c;

	private int[] boundItems = new int[BOUND_ITEMS];
	private int[][] loadOuts = new int[ROWS][ITEMS_PER_ROW];
	private int ammunitionId = -1;
	private int ammunitionCount = 0;
	public int selectedLoudout;

	public Binds(Player c) {
		this.c = c;
	}
	
	public String getBoundItemsJson() {
		return GsonSave.gsond.toJson(boundItems);
	}
	
	public void loadBoundItemsFromJson(String str) {
		boundItems = GsonSave.gsond.fromJson(str, new TypeToken<int[]>() { }.getType());
		if (boundItems == null)
			boundItems = new int[BOUND_ITEMS]; //backup sets to null so we need to recreate array
	}
	
	public String getLoadoutsJson() {
		return GsonSave.gsond.toJson(loadOuts);
	}
	
	public void loadLoadoutsFromJson(String str) {
		loadOuts = GsonSave.gsond.fromJson(str, new TypeToken<int[][]>() { }.getType());
		if (loadOuts == null)
			loadOuts = new int[ROWS][ITEMS_PER_ROW]; // backup sets to null
	}

	public static void open(Player c) {
		c.getBinds().update();
		c.getPacketSender().showInterface(INTERFACE_ID);
	}
	
	public static boolean button(Player c, int buttonId) {
		switch (buttonId) {
			case 171195:
				c.getBinds().load(0, false);
				return true;
			case 171196:
				c.getBinds().load(1, false);
				return true;
			case 171197:
				c.getBinds().load(2, false);
				return true;
			case 171177:
				c.getBinds().selectedLoudout = 0;
				c.getBinds().updateLoadout();
				return true;
			case 171178:
				c.getBinds().selectedLoudout = 1;
				c.getBinds().updateLoadout();
				return true;
			case 171179:
				c.getBinds().selectedLoudout = 2;
				c.getBinds().updateLoadout();
				return true;
			default:
				//System.out.println("here " + buttonId);
				return false;
		}
	}
	
	public static int getBindCount(Player c) {
		int dung = c.getSkills().getStaticLevel(Skills.DUNGEONEERING);
		if (dung >= 120 || c.difficulty == PlayerDifficulty.PRESTIGE_ONE || c.difficulty == PlayerDifficulty.PRESTIGE_TWO || c.difficulty == PlayerDifficulty.PRESTIGE_THREE) {
			return 5;
		} else if (dung >= 90) {
			return 4;
		} else if (dung >= 50) {
			return 3;
		} else if (dung >= 20) {
			return 2;
		}

		return 1;
	}

	public static int interfaceToIndex(int interfaceId) {
		switch (interfaceId) {
			case ROW1_INTERFACE_ID:
				return 0;
			case ROW2_INTERFACE_ID:
				return 1;
			case ROW3_INTERFACE_ID:
				return 2;
			default:
				return -1;
		}
	}

	public void load(int index, boolean dungStart) {
		if (index < 0 || index >= ROWS) {
			return;
		}

		if (!c.insideDungeoneering()) {
			c.sendMessage("You may only load binds inside dungeoneering.");
			return;
		}

		for (int i = 0; i < BOUND_ITEMS; i++) {
			int item = boundItems[i] - 1;
			if (item == -1) {
				continue;
			}

			c.getItems().hardDelete(item);
		}

		int[] items = loadOuts[index];
		for (int i = 0; i < ITEMS_PER_ROW; i++) {
			int id = items[i] - 1;
			if (id == -1) {
				continue;
			}

			c.getItems().addItem(id, 1);
		}

		if (!dungStart) {
			c.sendMessage("Using " + NAMES[index] + " loadout.");
		}
	}

	public void deleteBind(int bindIndex, Runnable onClick) {
		if (bindIndex < 0 || bindIndex >= BOUND_ITEMS) {
			return;
		}

		int itemId = boundItems[bindIndex];

		c.getPA().destroyItem(boundItems[bindIndex] - 1, 1, () -> {
			Misc.deleteElement(boundItems, bindIndex);
			if (boundItems[BOUND_ITEMS - 1] != 0) {
				boundItems[BOUND_ITEMS - 1] = 0;
			}

			for (int i = 0; i < ROWS; i++) {
				for (int j = 0; j < ITEMS_PER_ROW; j++) {
					if (loadOuts[i][j] == itemId) {
						removeLoudout(i, j);
					}
				}
			}

			c.getItems().hardDelete(itemId - 1);
		}, onClick, "Are you sure you want to destroy this item?", "The only way to unbind an item is to destoy it.");
	}

	public void removeLoudout(int row, int itemIndex) {
		loadOuts[row][itemIndex] = 0;
		Misc.deleteElement(loadOuts[row], itemIndex);
	}

	public void moveToLoudout(int bindIndex, int interfaceId) {
		if (bindIndex < 0 || bindIndex >= BOUND_ITEMS) {
			return;
		}

		int bindItem = boundItems[bindIndex] - 1;
		if (bindItem == -1) {
			return;
		}

		int loudoutIndex = interfaceToIndex(interfaceId);
		if (loudoutIndex == -1) {
			return;
		}
		
		int bindCount = getBindCount(c);

		int[] items = loadOuts[loudoutIndex];

		for (int i = 0; i < ITEMS_PER_ROW; i++) {
			if (items[i] - 1 == bindItem) {
				c.sendMessage("Already have this item in your loadout.");
				return;
			}
		}

		int freeSlot = -1;

		for (int i = 0; i < ITEMS_PER_ROW; i++) {
			if (items[i] - 1 == -1) {
				freeSlot = i;
				break;
			}
		}

		if (freeSlot == -1) {
			c.sendMessage("Your loudout is full.");
			return;
		}
		
		if (bindCount == freeSlot) {
			c.sendMessage("You can't have any more binds at this level.");
			return;
		}

		loadOuts[loudoutIndex][freeSlot] = bindItem + 1;
	}

	public void updateLoadout() {
		c.getPacketSender().sendConfig(8224, selectedLoudout);
	}

	public void update() {
		c.getItems().setInventory(29395, boundItems);
		c.getItems().setInventory(ROW1_INTERFACE_ID, loadOuts[0]);
		c.getItems().setInventory(ROW2_INTERFACE_ID, loadOuts[1]);
		c.getItems().setInventory(ROW3_INTERFACE_ID, loadOuts[2]);
		updateLoadout();
	}

	public int findBound(int id) {
		id++;

		for (int i = 0; i < BOUND_ITEMS; i++) {
			if (boundItems[i] == id) {
				return i;
			}
		}

		return -1;
	}

	public int getAmmunitionId() { // for sql loading
		return this.ammunitionId;
	}
	
	public void setAmmunitionId(int id) { // for sql loading
		ammunitionId = id;
	}
	
	public int getAmmoCount() {
		return this.ammunitionCount;
	}
	
	public void setAmmoCount(int count) {
		this.ammunitionCount = count;
	}
	
	public void loadAmmunition() {
		if (ammunitionId == -1) {
			return;
		}

		c.getItems().addItem(ammunitionId, ammunitionCount);
	}

	public boolean unBindAmmunition(int id) {
		if (ammunitionId != id) {
			return false;
		}

		c.getPA().destroyItem(id, 1, () -> {
			if (c.getItems().deleteItem3(ammunitionId, c.getItems().getItemAmount(ammunitionId))) {
				ammunitionId = -1;
				ammunitionCount = 0;
			}
		}, null, "Are you sure you want to destroy this item?", "The only way to unbind an item is to destoy it.");
		return true;
	}

	public void addAmmunition(int id, int slot, int count, int bountId) {
		if (ammunitionId != -1 && ammunitionId != bountId) {
			c.getDialogueBuilder().sendStatement("You already have a bound ammo type. You'll have to unbind it", "if you wish to bind a new ammo type.", "Unbinding the ammo will destroy it.").sendOption("Unbind your current bound ammo?", "Yes.", () -> {
				if (c.getItems().deleteItem3(ammunitionId, c.getItems().getItemAmount(ammunitionId))) {
					ammunitionId = -1;
					ammunitionCount = 0;

					addAmmunition(id, slot, count, bountId);
				}
			}, "No.", () -> {}).execute();
			return;
		}

		if (ammunitionId == bountId) {
			if (c.getItems().deleteItem2(id, count)) {
				int newCount = count + ammunitionCount;
				int countNorm = Math.min(newCount, MAX_COUNT);
				ammunitionCount = countNorm;
				if (newCount > MAX_COUNT) {
					c.sendMessage("You now have " + countNorm + " ammo bound. This is the most you can have bound.");
				} else {
					c.sendMessage("You now have " + countNorm + " ammo bound.");
				}

				c.getItems().addItem(bountId, count);
			}
			return;
		}

		int countNorm = Math.min(count, MAX_COUNT);
		ammunitionId = bountId;
		ammunitionCount = countNorm;
		c.getItems().replaceItemSlot(bountId, slot);
		if (count > MAX_COUNT) {
			c.sendMessage("You now have " + countNorm + " ammo bound. This is the most you can have bound.");
		} else {
			c.sendMessage("You now have " + countNorm + " ammo bound.");
		}
	}

	public boolean addBind(int id) {
		if (findBound(id) != -1) {
			c.sendMessage("Already have this item in your bindings.");
			return false;
		}

		int freeSlot = -1;

		for (int i = 0; i < BOUND_ITEMS; i++) {
			if (boundItems[i] == 0) {
				freeSlot = i;
				break;
			}
		}

		if (freeSlot == -1) {
			c.sendMessage("No free bind spot.");
			return false;
		}

		boundItems[freeSlot] = id + 1;
		return true;
	}

}
