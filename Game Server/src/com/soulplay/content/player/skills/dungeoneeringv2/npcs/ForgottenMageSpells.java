package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public class ForgottenMageSpells {
	
	private static final int WIND_STRIKE = 1;
	private static final int WATER_STRIKE = 2;
	private static final int EARTH_STRIKE = 3;
	private static final int FIRE_STRIKE = 4;
	private static final int WIND_BOLT = 5;
	private static final int WATER_BOLT = 6;
	private static final int EARTH_BOLT = 7;
	private static final int FIRE_BOLT = 8;
	private static final int WIND_BLAST = 9;
	private static final int WATER_BLAST = 10;
	private static final int EARTH_BLAST = 11;
	private static final int FIRE_BLAST = 12;
	private static final int WIND_WAVE = 13;
	private static final int WATER_WAVE = 14;
	private static final int EARTH_WAVE = 15;
	private static final int FIRE_WAVE = 16;
	
	private static final int CONFUSE = 17;
	private static final int WEAKEN = 18;
	private static final int CURSE = 19;
	private static final int VULNERABILITY = 20;
	private static final int ENFEEBLE = 21;
	private static final int BIND = 22;
	private static final int SNARE = 23;
	private static final int ENTANGLE = 24; 
	private static final int STUN =  25;
	
	public static void forgottenMageSpells(int spellId, NPC n, Client c) {
		
		//CHEAPHAX TO STOP MAGES FROM GOING WILD
		if (!NPCHandler.canNoClipAttacks(n)) {
			if (RegionClip.rayTraceBlocked(n.getXtoPlayerLoc(c.getX()), n.getYtoPlayerLoc(c.getY()),
					c.getX(), c.getY(), n.getHeightLevel(),
					true/*n.projectileId > 0 || n.attackType != 0*/, c.getDynamicRegionClip())) {
				return;
			}
		}
		
		int maxHit =  Misc.interpolate(1, 24, 5, 114, n.def.combatLevel);
		
		//c.sendMessage("Maxhit: " + Misc.interpolate(1, 50, 5, 114, n.def.combatLevel)+" combat level: "+ n.def.combatLevel);
		
		n.maxHit = maxHit;
		
		n.projectileDelay = 50;
		n.projectiletStartHeight = 23;
		
		n.getCombatStats().setAttackSpeed(6);
		
		//c.sendMess("maxhit: " + n.maxHit + " defence: " + n.defence + " attack: " + n.attack);
		
		switch (spellId) {
			
		case 1:
			n.startAnimation(14221);
			n.startGraphic(Graphic.create(-1));
			n.projectileId = 2699;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2700;
			break;
			
		case 2:
			n.startAnimation(14221);
			n.startGraphic(Graphic.create(2701));
			n.projectileId = 2703;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2708;
			break;
			
		case 3:
			n.startAnimation(14221);
			n.startGraphic(Graphic.create(2713));
			n.projectileId = 2718;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2723;
			break;
			
		case 4:
			n.startAnimation(14221);
			n.startGraphic(Graphic.create(2728));
			n.projectileId = 2729;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2737;
			break;
			
		case 5:
			n.startAnimation(14220);
			n.startGraphic(Graphic.create(-1));
			n.projectileId = 2699;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2700;
			break;
			
		case 6:
			n.startAnimation(14220);
			n.startGraphic(Graphic.create(2701));
			n.projectileId = 2704;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2709;
			break;
			
		case 7:
			n.startAnimation(14220);
			n.startGraphic(Graphic.create(2714));
			n.projectileId = 2719;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2724;
			break;
			
		case 8:
			n.startAnimation(14220);
			n.startGraphic(Graphic.create(2728));
			n.projectileId = 2731;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2738;
			break;
			
		case 9:
			n.startAnimation(14221);
			n.startGraphic(Graphic.create(-1));
			n.projectileId = 2699;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2700;
			break;
			
		case 10:
			n.startAnimation(14220);
			n.startGraphic(Graphic.create(2702));
			n.projectileId = 2705;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2710;
			break;
			
		case 11:
			n.startAnimation(14222);
			n.startGraphic(Graphic.create(2715));
			n.projectileId = 2720;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2725;
			break;
			
		case 12:
			n.startAnimation(14223);
			n.startGraphic(Graphic.create(2728));
			n.projectileId = 2733;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2739;
			break;
			
		case 13:
			n.startAnimation(14221);
			n.startGraphic(Graphic.create(-1));
			n.projectileId = 2699;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2700;
			break;
			
		case 14:
			n.startAnimation(14220);
			n.startGraphic(Graphic.create(2702));
			n.projectileId = 2707;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2712;
			break;
			
		case 15:
			n.startAnimation(14222);
			n.startGraphic(Graphic.create(2716));
			n.projectileId = 2721;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2726;
			break;
			
		case 16:
			n.startAnimation(14223);
			n.startGraphic(Graphic.create(2728));
			n.projectileId = 2735;
			n.attackType = 2;
			//n.maxhit = maxHit;
			n.endGfx = 2740;
			break;
			
		case 17: // confuse
			n.startAnimation(716);
			n.startGraphic(Graphic.create(102, GraphicType.HIGH));
			int baseLowAtt = c.getSkills().getStaticLevel(Skills.ATTACK);
			int newLowAttLevel = (int) (baseLowAtt - baseLowAtt * 0.05);
			c.getSkills().setLevel(Skills.ATTACK, newLowAttLevel);
			n.projectileId = 103;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 104;
			n.projectiletStartHeight = 43;
			break;
			
		case 18: //weaken
			n.startAnimation(716);
			n.startGraphic(Graphic.create(105, GraphicType.HIGH));
			int baseLowStr = c.getSkills().getStaticLevel(Skills.STRENGTH);
			int newLowStrLevel = (int) (baseLowStr - baseLowStr * 0.05);
			c.getSkills().setLevel(Skills.STRENGTH, newLowStrLevel);
			n.projectileId = 106;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 107;
			n.projectiletStartHeight = 43;
			break;
			
		case 19: // curse
			n.startAnimation(716);
			n.startGraphic(Graphic.create(108, GraphicType.HIGH));
			int baseLowDef = c.getSkills().getStaticLevel(Skills.DEFENSE);
			int newLowDefLevel = (int) (baseLowDef - baseLowDef * 0.05);
			c.getSkills().setLevel(Skills.DEFENSE, newLowDefLevel);
			n.projectileId = 109;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 110;
			n.projectiletStartHeight = 43;
			break;
			
		case 20: //vuln
			n.startAnimation(729);
			n.startGraphic(Graphic.create(167, GraphicType.HIGH));
			int baseHighDef = c.getSkills().getStaticLevel(Skills.DEFENSE);
			int newHighDefLevel = (int) (baseHighDef - baseHighDef * 0.1);
			c.getSkills().setLevel(Skills.DEFENSE, newHighDefLevel);
			n.projectileId = 168;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 169;
			n.projectiletStartHeight = 43;
			break;
			
		case 21: //enfeeble
			n.startAnimation(729);
			n.startGraphic(Graphic.create(170, GraphicType.HIGH));
			int baseHighStr = c.getSkills().getStaticLevel(Skills.STRENGTH);
			int newHighStrLevel = (int) (baseHighStr - baseHighStr * 0.1);
			c.getSkills().setLevel(Skills.STRENGTH, newHighStrLevel);
			n.projectileId = 171;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 172;
			n.projectiletStartHeight = 43;
			break;
			
		case 22: //bind
			n.startAnimation(711);
			n.startGraphic(Graphic.create(177, GraphicType.HIGH));
			n.projectileId = 178;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 181;
			n.projectiletStartHeight = 43;
			c.rootEntity(10);
			break;
			
		case 23: //snare
			n.startAnimation(711);
			n.startGraphic(Graphic.create(177, GraphicType.HIGH));
			n.projectileId = 178;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 180;
			n.projectiletStartHeight = 43;
			c.rootEntity(15);
			break;
			
		case 24: //entangle
			n.startAnimation(711);
			n.startGraphic(Graphic.create(177, GraphicType.HIGH));
			n.projectileId = 178;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 179;
			n.projectiletStartHeight = 43;
			c.rootEntity(20);
			break;
			
		case 25: //stun
			n.startAnimation(729);
			n.startGraphic(Graphic.create(173, GraphicType.HIGH));
			int baseHighAtt = c.getSkills().getStaticLevel(Skills.ATTACK);
			int newHighAttLevel = (int) (baseHighAtt - baseHighAtt * 0.1);
			c.getSkills().setLevel(Skills.ATTACK, newHighAttLevel);
			n.projectileId = 174;
			n.attackType = 2;
			n.maxHit = 0;
			n.endGfx = 254;
			n.projectiletStartHeight = 43;
			break;
		}
	}
	
	/**
	 * @param npc
	 */
	public static void forgottenMageNpc(NPC npc, Client c) {
		
		int roll = Misc.random(9);
			
		if (npc.npcType == 10560) {//1
			if (roll == 1) {
				forgottenMageSpells(CONFUSE, npc, c);
			} else
			    forgottenMageSpells(WIND_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10561) {//1
			if (roll == 1) {
				forgottenMageSpells(CONFUSE, npc, c);
			} else
			    forgottenMageSpells(WIND_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10562) {//1
			if (roll == 1) {
				forgottenMageSpells(CONFUSE, npc, c);
			} else
			    forgottenMageSpells(WATER_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10563) {//1
			if (roll == 1) {
				forgottenMageSpells(CONFUSE, npc, c);
			} else
			    forgottenMageSpells(WATER_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10564) {//2
			if (roll == 1) {
				forgottenMageSpells(CONFUSE, npc, c);
			} else 
			    forgottenMageSpells(WATER_STRIKE, npc, c);
		}
		if (npc.npcType == 10565) {//2
			if (roll == 1) {
			    forgottenMageSpells(CONFUSE, npc, c);
			} else
			    forgottenMageSpells(EARTH_STRIKE, npc, c);
		}
		if (npc.npcType == 10566) {//2
			if (roll == 1) {
				forgottenMageSpells(WEAKEN, npc, c);
			} else
				forgottenMageSpells(WATER_STRIKE, npc, c);
		}
		if (npc.npcType == 10567) {//2
			if (roll == 1) {
				forgottenMageSpells(WEAKEN, npc, c);
			} else
			    forgottenMageSpells(EARTH_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10568) {//3
			if (roll == 1) {
					forgottenMageSpells(CURSE, npc, c);
			} else
			    forgottenMageSpells(FIRE_STRIKE, npc, c);
		}
		if (npc.npcType == 10569) {//3
			if (roll == 1) {
					forgottenMageSpells(BIND, npc, c); 
			} else
			    forgottenMageSpells(FIRE_STRIKE, npc, c);
		}
		if (npc.npcType == 10570) {//3
			if (roll == 1) {
					forgottenMageSpells(WEAKEN, npc, c);
			} else 
				forgottenMageSpells(EARTH_STRIKE, npc, c);
		}
		if (npc.npcType == 10571) {//3
			if (Misc.random(9) == 1) {
				    forgottenMageSpells(CONFUSE, npc, c);
			} else
				forgottenMageSpells(EARTH_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10572) {//4
			if (roll == 1) {
					forgottenMageSpells(CURSE, npc, c);
			} else
			    forgottenMageSpells(WIND_BOLT, npc, c);
		}
		if (npc.npcType == 10573) {//4
			if (roll == 1) {
					forgottenMageSpells(BIND, npc, c); 
			} else
			    forgottenMageSpells(WIND_BOLT, npc, c);
		}
		if (npc.npcType == 10574) {//4
			if (roll == 1) {
					forgottenMageSpells(WEAKEN, npc, c);
			} else 
				forgottenMageSpells(FIRE_STRIKE, npc, c);
		}
		if (npc.npcType == 10575) {//4
			if (Misc.random(9) == 1) {
				    forgottenMageSpells(CONFUSE, npc, c);
			} else
				forgottenMageSpells(FIRE_STRIKE, npc, c);
		}
		
		if (npc.npcType == 10576) {//5
			if (roll == 1) {
					forgottenMageSpells(CURSE, npc, c);
			} else
			    forgottenMageSpells(WATER_BOLT, npc, c);
		}
		if (npc.npcType == 10577) {//5
			if (roll == 1) {
					forgottenMageSpells(BIND, npc, c); 
			} else
			    forgottenMageSpells(WATER_BOLT, npc, c);
		}
		if (npc.npcType == 10578) {//5
			if (roll == 1) {
					forgottenMageSpells(WEAKEN, npc, c);
			} else 
				forgottenMageSpells(WATER_BOLT, npc, c);
		}
		if (npc.npcType == 10579) {//5
			if (roll == 1) {
				    forgottenMageSpells(CONFUSE, npc, c);
			} else
				forgottenMageSpells(WIND_BOLT, npc, c);
		}
		
		if (npc.npcType == 10580) {//6
			if (roll == 1) {
					forgottenMageSpells(CURSE, npc, c);
			} else
			    forgottenMageSpells(FIRE_BOLT, npc, c);
		}
		if (npc.npcType == 10581) {//6
			if (roll == 1) {
					forgottenMageSpells(SNARE, npc, c); 
			} else
			    forgottenMageSpells(EARTH_BOLT, npc, c);
		}
		if (npc.npcType == 10582) {//6
			if (roll == 1) {
					forgottenMageSpells(WEAKEN, npc, c);
			} else 
				forgottenMageSpells(EARTH_BOLT, npc, c);
		}
		if (npc.npcType == 10583) {//6
			if (roll == 1) {
				    forgottenMageSpells(CONFUSE, npc, c);
			} else
				forgottenMageSpells(WATER_BOLT, npc, c);
		}
		
		if (npc.npcType == 10584) {//7
			if (roll == 1) {
					forgottenMageSpells(VULNERABILITY, npc, c);
			} else
			    forgottenMageSpells(WATER_BLAST, npc, c);
		}
		if (npc.npcType == 10585) {//7
			if (roll == 1) {
					forgottenMageSpells(SNARE, npc, c); 
			} else
			    forgottenMageSpells(WIND_BLAST, npc, c);
		}
		if (npc.npcType == 10586) {//7
			if (roll == 1) {
					forgottenMageSpells(WEAKEN, npc, c);
			} else 
				forgottenMageSpells(WIND_BLAST, npc, c);
		}
		if (npc.npcType == 10587) {//7
			if (roll == 1) {
				    forgottenMageSpells(CONFUSE, npc, c);
			} else
				forgottenMageSpells(FIRE_BOLT, npc, c);
		}
		
		if (npc.npcType == 10588) {//8
			if (roll == 1) {
					forgottenMageSpells(VULNERABILITY, npc, c);
			} else
			    forgottenMageSpells(EARTH_BLAST, npc, c);
		}
		if (npc.npcType == 10589) {//8
			if (roll == 1) {
					forgottenMageSpells(ENTANGLE, npc, c); 
			} else
			    forgottenMageSpells(EARTH_BLAST, npc, c);
		}
		if (npc.npcType == 10590) {//8
			if (roll == 1) {
					forgottenMageSpells(ENFEEBLE, npc, c);
			} else 
				forgottenMageSpells(EARTH_BLAST, npc, c);
		}
		if (npc.npcType == 10591) {//8
			if (roll == 1) {
				    forgottenMageSpells(CONFUSE, npc, c);
			} else
				forgottenMageSpells(WATER_BLAST, npc, c);
		}
		
		if (npc.npcType == 10592) {//9
			if (roll == 1) {
					forgottenMageSpells(VULNERABILITY, npc, c);
			} else
			    forgottenMageSpells(FIRE_BLAST, npc, c);
		}
		if (npc.npcType == 10593) {//9
			if (roll == 1) {
					forgottenMageSpells(ENTANGLE, npc, c); 
			} else
			    forgottenMageSpells(WIND_WAVE, npc, c);
		}
		if (npc.npcType == 10594) {//9
			if (roll == 1) {
					forgottenMageSpells(ENFEEBLE, npc, c);
			} else 
				forgottenMageSpells(WIND_WAVE, npc, c);
		}
		if (npc.npcType == 10595) {//9
			if (roll == 1) {
				    forgottenMageSpells(STUN, npc, c);
			} else
				forgottenMageSpells(WATER_WAVE, npc, c);
		}
		
		if (npc.npcType == 10596) {//10
			if (roll == 1) {
					forgottenMageSpells(VULNERABILITY, npc, c);
			} else
			    forgottenMageSpells(WATER_WAVE, npc, c);
		}
		if (npc.npcType == 10597) {//10
			if (roll == 1) {
					forgottenMageSpells(ENTANGLE, npc, c); 
			} else
			    forgottenMageSpells(EARTH_WAVE, npc, c);
		}
		if (npc.npcType == 10598) {//10
			if (roll == 1) {
					forgottenMageSpells(ENFEEBLE, npc, c);
			} else 
				forgottenMageSpells(EARTH_WAVE, npc, c);
		}
		if (npc.npcType == 10599) {//10
			if (roll == 1) {
				    forgottenMageSpells(STUN, npc, c);
			} else
				forgottenMageSpells(FIRE_WAVE, npc, c);
		}
		
		if (npc.npcType == 10600) {//11
			if (roll == 1) {
					forgottenMageSpells(VULNERABILITY, npc, c);
			} else
			    forgottenMageSpells(FIRE_WAVE, npc, c);
		}
		if (npc.npcType == 10601) {//11
			if (roll == 1) {
					forgottenMageSpells(ENTANGLE, npc, c); 
			} else
			    forgottenMageSpells(FIRE_WAVE, npc, c);
		}
		if (npc.npcType == 10602) {//11
			if (roll == 1) {
					forgottenMageSpells(ENFEEBLE, npc, c);
			} else 
				forgottenMageSpells(FIRE_WAVE, npc, c);
		}
		if (npc.npcType == 10603) {//11
			if (roll == 1) {
				    forgottenMageSpells(STUN, npc, c);
			} else
				forgottenMageSpells(FIRE_WAVE, npc, c);
		}	
	}
}
