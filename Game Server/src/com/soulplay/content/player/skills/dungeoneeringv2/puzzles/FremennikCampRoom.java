package com.soulplay.content.player.skills.dungeoneeringv2.puzzles;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.PuzzleRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching.DungeoneeringFletching;
import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Graphic;

public class FremennikCampRoom extends PuzzleRoom {

	public static final int FREMENNIK_SCOUT = 11001;
	private static final int[] RAW_FISH =
	{ 49522, 49523, 49524, 49524, 49524 };
	private static final int[] COOKED_FISH =
	{ 49525, 49526, 49527, 49527, 49527 };
	private static final int[] BARS =
	{ 49528, 49529, 49530, 49530, 49530 };
	private static final int[] BATTLE_AXES =
	{ 49531, 49532, 49533, 49533, 49533 };
	private static final int[] LOGS =
	{ 49534, 49535, 49536, 49536, 49536 };
	private static final int[] BOWS =
	{ 49537, 49538, 49539, 49539, 49539 };

	private int stage = 0;

	@Override
	public void openRoom() {
		manager.spawnNPC(reference, FREMENNIK_SCOUT, 8, 5, false, DungeonConstants.NORMAL_NPC);
	}

	@Override
	public boolean processObjectClick1(Client player, GameObject object) {
		if (object.getId() == RAW_FISH[type]) {
			if (!hasRequirement(player, Skills.COOKING)) {
				player.sendMessage("You need a cooking level of " + getRequirement(Skills.COOKING) + " to cook these fish.");
				return false;
			}
			giveXP(player, Skills.COOKING);
			replaceObject(object, COOKED_FISH[type]);
			advance(player);
			player.startAnimation(897);
			return false;
		} else if (object.getId() == BARS[type]) {
			if (!hasRequirement(player, Skills.SMITHING)) {
				player.sendMessage("You need a smithing level of " + getRequirement(Skills.SMITHING) + " to smith these battle axes.");
				return false;
			}

			int index = player.getItems().getItemSlot(Smithing.DUNG_HAMMER);
			if (index == -1) {
				player.sendMessage("You need a hammer to smith battle axes.");
				return false;
			}

			giveXP(player, Skills.SMITHING);
			replaceObject(object, BATTLE_AXES[type]);
			advance(player);
			player.startAnimation(898);
			player.startGraphic(Graphic.create(2123));
			return false;
		} else if (object.getId() == LOGS[type]) {
			if (!hasRequirement(player, Skills.FLETCHING)) {
				player.sendMessage("You need a fletching level of " + getRequirement(Skills.FLETCHING) + " to fletch these bows.");
				return false;
			}
			int index = player.getItems().getItemSlot(DungeoneeringFletching.KNIFE);
			if (index == -1) {
				player.sendMessage("You need a knife to fletch bows.");
				return false;
			}
			giveXP(player, Skills.FLETCHING);
			replaceObject(object, BOWS[type]);
			advance(player);
			player.startAnimation(1248);
			return false;
		}
		return true;
	}

	private static void sendDialog(Client p, PuzzleRoom room) {
		if (room.isComplete()) {
			p.getDialogueBuilder().sendNpcChat(FremennikCampRoom.FREMENNIK_SCOUT, "Wonderful! That was the last of them.", "As promised, I'll unlock the door for you.").execute();
		} else {
			p.getDialogueBuilder().sendNpcChat(FremennikCampRoom.FREMENNIK_SCOUT, "Need some tools?").sendAction(() -> {
				if (p.getItems().getItemSlot(DungeoneeringFletching.KNIFE) == -1) {
					p.getItems().addItem(DungeoneeringFletching.KNIFE, 1);
				}
				if (p.getItems().getItemSlot(Smithing.DUNG_HAMMER) == -1) {
					p.getItems().addItem(Smithing.DUNG_HAMMER, 1);
				}
			}).execute();
		}
	}

	public void advance(Client player) {
		if (++stage == 3) {
			setComplete(player);
			sendDialog(player, this);
		}
	}

	@Override
	public boolean processNPCClick1(Client player, NPC npc) {
		if (npc.npcType == FREMENNIK_SCOUT) {
			sendDialog(player, this);
			return false;
		}
		return true;
	}

	@Override
	public String getCompleteMessage() {
		return null;
	}

}
