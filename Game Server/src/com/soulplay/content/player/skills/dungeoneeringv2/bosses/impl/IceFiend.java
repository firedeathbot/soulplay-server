package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Manager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {9912, 9913, 9914, 9915, 9916, 9917, 9918, 9919, 9920, 9921, 9922, 9923, 9924, 9925, 9926, 9927, 9928})
public class IceFiend extends Boss {

	public IceFiend(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		if ((Integer) npc.dungVariables[0] == 0 && npc.getSkills().getLifepoints() <= npc.getSkills().getMaximumLifepoints() * 0.75) {
			npc.dungVariables[0] = 1;
			npc.dungVariables[1] = 0;
			specialAttack(npc);
		} else if ((Integer) npc.dungVariables[0] == 1 && npc.getSkills().getLifepoints() <= npc.getSkills().getMaximumLifepoints() * 0.5) {
			npc.dungVariables[0] = 2;
			npc.dungVariables[1] = 0;
			specialAttack(npc);
		} else if ((Integer) npc.dungVariables[0] == 2 && npc.getSkills().getLifepoints() <= npc.getSkills().getMaximumLifepoints() * 0.25) {
			npc.dungVariables[0] = 3;
			npc.dungVariables[1] = 0;
			specialAttack(npc);
		} else if ((Integer) npc.dungVariables[0] == 3 && npc.getSkills().getLifepoints() <= 1) {
			npc.dungVariables[0] = -1;
			npc.dungVariables[1] = 0;
			specialAttack(npc);
		}
	}
	
	private void specialAttack(NPC npc) {
		npc.startAnimation(13338);
		//TODO use projectile for rising
		npc.startGraphic(Graphic.create(2524));
		npc.setStunned(true);
		npc.stopAttack = true;
		npc.setAttackable(false);
		npc.resetFace();
		
		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				npc.dungVariables[1] = (Integer) npc.dungVariables[1] + 1;
				
				for (Player p : Manager.getNearbyPlayers(npc)) {
					Client c = (Client) p;
					//TODO use projectile for landing and stuff
					int startX = p.absX;
					int startY = p.absY;
					p.getPacketSender().sendStillGraphics(2525, 0, startY, startX, 0);
					CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {

						@Override
						public void execute(CycleEventContainer container) {
							if (p.absX == startX && p.absY == startY) {
								int hpCap = (int) (p.getSkills().getMaximumLifepoints() * 0.1);
								if (p.getSkills().getLifepoints() <= hpCap) {
									container.stop();
									return;
								}

								c.getAgility().agilityWalk(c, 10070, 1, 0);
								c.dealDamage(new Hit(10, 0, 0));//TODO should damage be based on npc, complexity?
							}
							container.stop();
						}

						@Override
						public void stop() {
							c.getAgility().resetAgilityWalk(c);
						}
					}, 2);
				}
				
				if ((Integer) npc.dungVariables[1] >= 9) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				npc.setStunned(false);
				npc.setAttackable(true);
				npc.stopAttack = false;
			}
		}, 1);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.attackType = 0;
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		npc.dungVariables[0] = new Integer(0);
		npc.dungVariables[1] = new Integer(0);
		npc.dungVariables[2] = new Boolean(false);
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}
	
	public int followDistance() {
		return 0;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		int state = (Integer) npc.dungVariables[0]; 
		if (state < 0) {
			return;
		}

		int toHit = hit.getDamage();
		int hp = npc.getSkills().getLifepoints() - toHit;
		int hpCap = 0;
		if (state == 0) {
			hpCap = (int) (npc.getSkills().getMaximumLifepoints() * 0.75);
		} else if (state == 1) {
			hpCap = (int) (npc.getSkills().getMaximumLifepoints() * 0.5);
		} else if (state == 2) {
			hpCap = (int) (npc.getSkills().getMaximumLifepoints() * 0.25);
		} else if (state == 3) {
			hpCap = 1;
		}

		if (hp < hpCap) {
			hit.setDamage(npc.getSkills().getLifepoints() - hpCap);
		}
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
}
