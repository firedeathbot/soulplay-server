package com.soulplay.content.player.skills.dungeoneeringv2.skills.runecrafting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

public final class ImbueStavesData {
	
	private static final Animation CRAFT_STAFF_ANIM = new Animation(791);

	public static enum ImbueStaves {

		WATER_STAFF(16997, 1, 6, 0),
		EARTH_STAFF(17001, 10, 12, 2),
		FIRE_STAFF(17005, 20, 21, 4),
		AIR_STAFF(17009, 30, 29, 6),
		CATALYTIC_STAFF(17017, 40, 40, 8),
		EMPOWERED_WATER_STAFF(16999, 50, 51, 10),
		EMPOWERED_EARTH_STAFF(17003, 60, 64, 12),
		EMPOWERED_FIRE_STAFF(17007, 70, 76, 14),
		EMPOWERED_AIR_STAFF(17011, 80, 91, 16),
		EMPOWERED_CATALYTIC_STAFF(17015, 90, 106, 18);

		private final int staffID;

		private final int levelReq;
		
		private final int experience;
		
		private final int tier;

		private ImbueStaves(int staffID, int levelReq, int experience, int tier) {
			this.staffID = staffID;
			this.levelReq = levelReq;
			this.experience = experience;
			this.tier = tier;
		}
	}
	
	private static final int TANGLE_GUM = 16977;
	private static final int SEEPING_ELM = 16979;
	private static final int BLOOD_SPINDLE = 16981;
	private static final int UTUKU = 16983;
	private static final int SPINEBEAM = 16985;
	private static final int BOVISTRANGLER = 16987;
	private static final int THIGAT = 16989;
	private static final int CORPSETHORN = 16991;
	private static final int ENTGALLOW = 16993;
	private static final int GRAVE_CREEPER = 16995;

	public static void craftStaff(Client player, ImbueStaves staff) {
		
		player.getPA().closeAllWindows();
		
		if (!(player.getItems().playerHasItemRange(TANGLE_GUM + staff.tier, GRAVE_CREEPER))) {
			player.sendMessage("You need a higher tier staff to make this staff.");
			return;
		}
		
		if (player.getPlayerLevel()[Skills.RUNECRAFTING] < staff.levelReq) {
			player.sendMessage("You need a runecrafting level of " + staff.levelReq + " to make this.");
			return;
		}

		if (player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			
			int cycles = player.makeCount;
			
			@Override
			public void execute(CycleEventContainer container) {
				for (int i = TANGLE_GUM + staff.tier; i <= GRAVE_CREEPER;) {
					if (player.getItems().playerHasItem(i)) {
						player.getItems().deleteItemInOneSlot(i, 1);
						player.getItems().addItem(staff.staffID, 1);
						player.getPA().addSkillXP(staff.experience, RSConstants.RUNECRAFTING);
						i = GRAVE_CREEPER + 1;
					} else {
					    i += 2;
					}
				}
				
				player.startAnimation(CRAFT_STAFF_ANIM);
				
				cycles--;

				if (!player.insideDungeoneering() || cycles == 0) {
					player.resetAnimations();
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 4);
	}
}
