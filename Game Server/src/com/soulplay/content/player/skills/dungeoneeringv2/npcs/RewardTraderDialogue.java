package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import com.soulplay.content.player.skills.dungeoneeringv2.items.shop.DungeonRewardShop;
import com.soulplay.game.model.player.Client;

public class RewardTraderDialogue {

	public static void rewardTraderChat(Client c) {
		c.getDialogueBuilder().sendNpcChat("Oh, hello, I didn't see...")
		.sendPlayerChat("Hey. I was wondering if you could help me?")
		.sendNpcChat("Help? Uh...I'm not sure that I can...uh...")
		.sendOption("Who are you?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Who are you?")
			.sendNpcChat("I'm...I used to be...")
			.sendPlayerChat("Yes?")
			.sendPlayerChat("I just handle the...uh...equipment.")
			.sendPlayerChat("I mean, who are you? Like, what is your name?")
			.sendNpcChat("A name? I...uh...")
			.sendOption("You are driving me crazy.", ()-> {
				drivingMeCrazy(c);
			}, "You mentioned something about equipment?", ()-> {
				mentionedEquipments(c);
			}, "I'm done with you.", ()-> {
				done(c);
			});
		}, "I'm done with you.", ()-> {
			done(c);
		}, "About the Task System...", ()-> {
			DungeoneeringTutorDialogue.taskSystemChat(c);
		}).execute();
	}
	
	private static void done(Client c) {
		c.getDialogueBuilder().sendPlayerChat("I'm done with you.")
		.sendNpcChat("Fine, fine, fine. I'll be here.");
	}
	
	private static void drivingMeCrazy(Client c) {
		c.getDialogueBuilder().sendPlayerChat("You are driving me crazy.")
		.sendNpcChat("I'm sorry. I'm...")
		.sendPlayerChat("Yes?")
		.sendNpcChat("They called me Ma...")
		.sendPlayerChat("Malcolm? Mandrake? Ma...um...Mango?")
		.sendNpcChat("Uh, no, those aren't quite right.")
		.sendOption("You mentioned something about equipment?", ()-> {
			mentionedEquipments(c);
		}, "I'm done with you.", ()-> {
			done(c);
		});
	}
	
	private static void mentionedEquipments(Client c) {
		c.getDialogueBuilder().sendPlayerChat("You mentioned something about equipment?")
		.sendNpcChat("Yes, I did, you're right. I sell things, mend things. What", "interests you?")
		.sendOption("Let's see what you sell, then.", ()-> {
			DungeonRewardShop.open(c);
		}, "What exactly can you mend?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("What exactly can you mend?")
			.sendNpcChat("Weapons, I guess. I became good at mending in...there If", "you buy something from me, I should be able to repair it if it's...uh...degraded.", "Show it to me and Ill see what can be done.")
			.sendNpcChat("I feel...like I can trust you. Let me tell you something: I", "have a friend in...there, someone who brings me tools and", "items from that place. That is how I can offer you", "services that others...can't. Don't tell anyone. Please.");
		}, "I'm done with you.", ()-> {
			done(c);
		}).execute(true);
	}
}
