package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonNpcs;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {12903, 12904, 12905, 12906, 12907, 12908, 12909, 12910, 12911, 12912, 12913})
public class RebornMage extends Boss {

	private static final int RANDOM = 10;
	private static final int SPAWN_THRESHOLD = 3;

	public RebornMage(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		boolean insideDungeoneering = npc.dungeonManager != null;
		
		List<NPC> list = null;
		if (insideDungeoneering) {
			list = ((ArrayList<NPC>) npc.dungVariables[0]);
		}
		int warriorLimit = 2;
		
		int random = Misc.random(RANDOM);
		if (random >= 5) {
			npc.startGraphic(Graphic.create(-1));//2441?
			npc.projectileId = 2872;
			npc.attackType = 2;
			npc.endGfx = -1;
			npc.projectileDelay = 40;
			npc.projectiletStartHeight = 28;
		} else if (random >= SPAWN_THRESHOLD && insideDungeoneering && list != null && list.size() < warriorLimit) {
			
			int spawnX = c.absX + 1;
			int spawnY = c.absY;
			
			final List<NPC> finalList = list;
			
			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {

				int timer = 5;
				
				@Override
				public void execute(CycleEventContainer container) {

					if (timer == 5) {
						Server.getStillGraphicsManager().createGfx(spawnX, spawnY, c.getHeightLevel(), 1991, 0, 0);
					}
					
					timer --;
					if (timer == 0) {
							NPC warrior = npc.dungeonManager.spawnNpc(npc.reference, DungeonNpcs.REBORN_WARRIORS);
							if (warrior != null) {
								warrior.dropStuff = false;
								warrior.setKillerId(c.getId());
								warrior.dungVariables[0] = npc;
								finalList.add(warrior);
								container.stop();
							}
					}

				}

				@Override
				public void stop() {
					// TODO Auto-generated method stub

				}

			}, 5);
		} else if (random >= 2) {
			int baseHighAtt = c.getSkills().getStaticLevel(Skills.ATTACK);
			int newHighAttLevel = (int) (baseHighAtt - baseHighAtt * 0.06);
			c.getSkills().setLevel(Skills.ATTACK, newHighAttLevel);
			npc.projectileId = 174;
			npc.attackType = 2;
			npc.maxHit = 0;
			npc.endGfx = 254;
			npc.projectiletStartHeight = 43;
		} else if (random >= 1) {
			int baseHighStr = c.getSkills().getStaticLevel(Skills.STRENGTH);
			int newHighStrLevel = (int) (baseHighStr - baseHighStr * 0.06);
			c.getSkills().setLevel(Skills.STRENGTH, newHighStrLevel);
			//npc.startGraphic(Graphic.create(170, GraphicType.HIGH));
			npc.projectileId = 171;
			npc.attackType = 2;
			npc.maxHit = 0;
			npc.endGfx = 172;
			npc.projectiletStartHeight = 43;
		} else if (random >= 0) {
			int baseHighDef = c.getSkills().getStaticLevel(Skills.DEFENSE);
			int newHighDefLevel = (int) (baseHighDef - baseHighDef * 0.06);
			c.getSkills().setLevel(Skills.DEFENSE, newHighDefLevel);
			//npc.startGraphic(Graphic.create(167, GraphicType.HIGH));
			npc.projectileId = 168;
			npc.attackType = 2;
			npc.maxHit = 0;
			npc.endGfx = 169;
			npc.projectiletStartHeight = 43;
		}
	}

	@Override
	public void process(NPC npc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.dungVariables[0] = new ArrayList<NPC>();
	}

	@Override
	public boolean ignoreMeleeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int followDistance() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
