package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.SkillDoors;
import com.soulplay.util.Misc;

public class DungeonUtils {

	public static List<Room> selectPossibleBossRooms(int type, int complexity, int floorId, boolean n, boolean e, boolean s, boolean w, int rotation) {
		List<Room> possiblities = new ArrayList<>();
		for (BossRoom handledRoom : DungeonConstants.BOSS_ROOMS[type]) {
			if (!handledRoom.isComplexity(complexity))
				continue;
			if (handledRoom.getMinFloor() > floorId)
				continue;
			if (handledRoom.hasNorthDoor(rotation) == n && handledRoom.hasEastDoor(rotation) == e && handledRoom.hasSouthDoor(rotation) == s && handledRoom.hasWestDoor(rotation) == w) {
				possiblities.add(new Room(handledRoom, rotation));
			}
		}
		//possiblities.add(new Room(DungeonConstants.BOSS_ROOMS[1][3], rotation));
		return possiblities;
	}

	public static List<Room> selectPossibleRooms(HandledRoom[] handledRooms, int complexity, int floorType, boolean n, boolean e, boolean s, boolean w) {
		List<Room> possiblities = new ArrayList<>();
		for (HandledRoom handledRoom : handledRooms) {
			if (!handledRoom.isAvailableOnFloorType(floorType))
				continue;
			if (!handledRoom.isComplexity(complexity))
				continue;
			for (int rotation = 0; rotation < DungeonConstants.ROTATIONS_COUNT; rotation++) {
				if (handledRoom.hasNorthDoor(rotation) == n && handledRoom.hasEastDoor(rotation) == e && handledRoom.hasSouthDoor(rotation) == s && handledRoom.hasWestDoor(rotation) == w) {
					possiblities.add(new Room(handledRoom, rotation));
				}
			}
		}
		return possiblities;
	}

	public static List<Room> selectPossibleRooms(HandledRoom[] handledRooms, int complexity, int floorType, boolean n, boolean e, boolean s, boolean w, int rotation) {
		List<Room> possiblities = new ArrayList<>();
		for (HandledRoom handledRoom : handledRooms) {
			if (!handledRoom.isAvailableOnFloorType(floorType))
				continue;
			if (!handledRoom.isComplexity(complexity))
				continue;
			if (handledRoom.hasNorthDoor(rotation) == n && handledRoom.hasEastDoor(rotation) == e && handledRoom.hasSouthDoor(rotation) == s && handledRoom.hasWestDoor(rotation) == w) {
				possiblities.add(new Room(handledRoom, rotation));
			}
		}
		return possiblities;
	}
	
	public static int getFloorType(int floorId) {
		if (floorId <= 11)
			return DungeonConstants.FROZEN_FLOORS;
		if (floorId <= 17)
			return DungeonConstants.ABANDONED_FLOORS;
		if (floorId <= 29)
			return DungeonConstants.FURNISHED_FLOORS;
		if (floorId <= 35)
			return DungeonConstants.ABANDONED_FLOORS;
		if (floorId <= 47)
			return DungeonConstants.OCCULT_FLOORS;
		return DungeonConstants.WARPED_FLOORS;
	}

	public static boolean isOpenSkillDoor(int id, int type) {
		for (SkillDoors s : SkillDoors.values) {
			int openId = s.getOpenObject(type);
			if (openId != -1 && openId == id) {
				return true;
			}
		}

		return false;
	}
	
	public static int getBattleaxe(int tier) {
		return 15753 + (tier - 1) * 2;
	}

	public static int getGauntlets(int tier) {
		return 16273 + (tier - 1) * 2;
	}

	public static int getPickaxe(int tier) {
		return 16295 + (tier - 1) * 2;
	}

	public static int getBoots(int tier) {
		return 16339 + (tier - 1) * 2;
	}

	public static int getHatchet(int tier) {
		return 16361 + (tier - 1) * 2;
	}

	public static int getLongsword(int tier) {
		return 16383 + (tier - 1) * 2;
	}

	public static int getMaul(int tier) {
		return 16405 + (tier - 1) * 2;
	}

	public static int getArrows(int tier) {
		return 16427 + (tier - 1) * 5;
	}

	public static int getPlatelegs(int tier, boolean male) {
		return (male ? 16669 : 16647) + (tier - 1) * 2;
	}

	public static int getFullHelm(int tier) {
		return 16691 + (tier - 1) * 2;
	}

	public static int getChainbody(int tier) {
		return 16713 + (tier - 1) * 2;
	}

	public static int getDagger(int tier) {
		return 16757 + (tier - 1) * 2;
	}

	public static int getTornBag(int tier) {
		return 17995 + (tier - 1) * 2;
	}

	public static int getOre(int tier) {
		return 17630 + (tier - 1) * 2;
	}

	public static int getBranche(int tier) {
		return 17682 + (tier - 1) * 2;
	}

	public static int getTextile(int tier) {
		return 17448 + (tier - 1) * 2;
	}

	public static int getHerb(int tier) {
		return 17494 + (tier - 1) * 2;
	}

	public static int getSeed(int tier) {
		return 17823 + (tier - 1);
	}

	public static int getRandomGear(int tier) {
		switch (Misc.randomNoPlus(29)) {
			case 0:
				return getLongbow(tier);
			case 1:
				return getShortbow(tier);
			case 2:
				return getChaps(tier);
			case 3:
				return getLeatherBoots(tier);
			case 4:
				return getVambrances(tier);
			case 5:
				return getLeatherBody(tier);
			case 6:
				return getCoif(tier);
			case 7:
				return getRobeTop(tier);
			case 8:
				return getElementalStaff(tier);
			case 9:
				return getBasicStaff(tier);
			case 10:
				return getGloves(tier);
			case 11:
				return getShoes(tier);
			case 12:
				return getRobeBottom(tier);
			case 13:
				return getHood(tier);
			case 14:
				return getKiteshield(tier);
			case 15:
				return getPlatebody(tier);
			case 16:
				return getSpear(tier);
			case 17:
				return getWarhammer(tier);
			case 18:
				return getRapier(tier);
			case 19:
				return get2HSword(tier);
			case 20:
				return getDagger(tier);
			case 21:
				return getChainbody(tier);
			case 22:
				return getFullHelm(tier);
			case 23:
				return getPlatelegs(tier, true);
			case 24:
				return getMaul(tier);
			case 25:
				return getLongsword(tier);
			case 26:
				return getBoots(tier);
			case 27:
				return getGauntlets(tier);
			case 28:
				return getBattleaxe(tier);
		}
		return getHatchet(tier); //shouldnt
	}

	public static int getRandomRangeGear(int tier) {
		switch (Misc.randomNoPlus(7)) {
			case 0:
				return getLongbow(tier);
			case 1:
				return getShortbow(tier);
			case 2:
				return getChaps(tier);
			case 3:
				return getLeatherBoots(tier);
			case 4:
				return getVambrances(tier);
			case 5:
				return getLeatherBody(tier);
			case 6:
				return getCoif(tier);
		}
		return getHatchet(tier); //shouldnt
	}

	public static int getRandomMagicGear(int tier) {
		switch (Misc.randomNoPlus(7)) {
			case 0:
				return getRobeTop(tier);
			case 1:
				return getElementalStaff(tier);
			case 2:
				return getBasicStaff(tier);
			case 3:
				return getGloves(tier);
			case 4:
				return getShoes(tier);
			case 5:
				return getRobeBottom(tier);
			case 6:
				return getHood(tier);
		}
		return getHatchet(tier); //shouldnt
	}

	public static int getRandomMeleeGear(int tier) {
		switch (Misc.randomNoPlus(15)) {
			case 0:
				return getKiteshield(tier);
			case 1:
				return getPlatebody(tier);
			case 2:
				return getSpear(tier);
			case 3:
				return getWarhammer(tier);
			case 4:
				return getRapier(tier);
			case 5:
				return get2HSword(tier);
			case 6:
				return getDagger(tier);
			case 7:
				return getChainbody(tier);
			case 8:
				return getFullHelm(tier);
			case 9:
				return getPlatelegs(tier, true);
			case 10:
				return getMaul(tier);
			case 11:
				return getLongsword(tier);
			case 12:
				return getBoots(tier);
			case 13:
				return getGauntlets(tier);
			case 14:
				return getBattleaxe(tier);
		}
		return getHatchet(tier); //shouldnt
	}

	public static int getRandomWeapon(int tier) {
		switch (Misc.randomNoPlus(12)) {
			case 0:
				return getLongbow(tier);
			case 1:
				return getShortbow(tier);
			case 2:
				return getElementalStaff(tier);
			case 3:
				return getBasicStaff(tier);
			case 4:
				return getSpear(tier);
			case 5:
				return getWarhammer(tier);
			case 6:
				return getRapier(tier);
			case 7:
				return get2HSword(tier);
			case 8:
				return getDagger(tier);
			case 9:
				return getMaul(tier);
			case 10:
				return getLongsword(tier);
			case 11:
				return getBattleaxe(tier);
		}
		return getHatchet(tier); //shouldnt
	}

	public static int get2HSword(int tier) {
		return 16889 + (tier - 1) * 2;
	}

	public static int getFood(int tier) {
		return 18159 + (tier - 1) * 2;
	}

	public static int getRapier(int tier) {
		return 16935 + (tier - 1) * 2;
	}

	public static int getWarhammer(int tier) {
		return 17019 + (tier - 1) * 2;
	}

	public static int getSpear(int tier) {
		return 17063 + (tier - 1) * 8;
	}

	public static int getPlatebody(int tier) {
		return 17239 + (tier - 1) * 2;
	}

	public static int getKiteshield(int tier) {
		return 17341 + (tier - 1) * 2;
	}

	public static int getHood(int tier) {
		return 16735 + (tier - 1) * 2;
	}

	public static int getRobeBottom(int tier) {
		return 16845 + (tier - 1) * 2;
	}

	public static int getShoes(int tier) {
		return 16911 + (tier - 1) * 2;
	}

	public static int getGloves(int tier) {
		return 17151 + (tier - 1) * 2;
	}

	public static int getBasicStaff(int tier) {
		return 16977 + (tier - 1) * 2;
	}

	public static int getElementalStaff(int tier) {
		if (tier == 2)
			return 17001; // earth
		if (tier == 3)
			return 17005; // fire
		if (tier == 4)
			return 17009; // air
		if (tier == 5)
			return 17013; // catalytic
		if (tier == 6)
			return 16999; // empowered water
		if (tier == 7)
			return 17003; // empowered earth
		if (tier == 8)
			return 17007; // empowered fire
		if (tier == 9)
			return 17011; // empowered air
		if (tier == 10)
			return 17015; // empowered catalytic
		if (tier == 11)
			return 17017; // celestial catalytic
		return 16997; //water
	}

	public static int getMaxTier(Party party, int skill) {
		return getTier(party.getMaxLevel(skill));
	}

	public static int getTier(int level) {
		return level == 99 ? 11 : level > 10 ? (1 + level / 10) : 1;
	}

	public static int getStartRunes(int level) {
		if (level >= 41)
			return DungeonConstants.RUNES[6]; //death
		if (level >= 17)
			return DungeonConstants.RUNES[5]; //chaos
		return DungeonConstants.RUNES[4]; //mind
	}

	public static int getRobeTop(int tier) {
		return 17217 + (tier - 1) * 2;
	}

	public static int getCoif(int tier) {
		return 17041 + (tier - 1) * 2;
	}

	public static int getLeatherBody(int tier) {
		return 17173 + (tier - 1) * 2;
	}

	public static int getVambrances(int tier) {
		return 17195 + (tier - 1) * 2;
	}

	public static int getLeatherBoots(int tier) {
		return 17297 + (tier - 1) * 2;
	}

	public static int getChaps(int tier) {
		return 17319 + (tier - 1) * 2;
	}

	public static int getShortbow(int tier) {
		return 16867 + (tier - 1) * 2;
	}

	public static int getLongbow(int tier) {
		return 16317 + (tier - 1) * 2;
	}

	public static int getGuardianCreature(int type) {
		return DungeonConstants.GUARDIAN_CREATURES[type + 1][Misc.randomNoPlus(DungeonConstants.GUARDIAN_CREATURES[type + 1].length)];
	}

	public static int getGuardianCreature() {
		return DungeonConstants.GUARDIAN_CREATURES[0][Misc.randomNoPlus(DungeonConstants.GUARDIAN_CREATURES[0].length)];
	}

	public static int getForgottenWarrior() {
		int type = Misc.randomNoPlus(DungeonConstants.FORGOTTEN_WARRIORS.length);
		return DungeonConstants.FORGOTTEN_WARRIORS[type][Misc.randomNoPlus(type)];
	}

	public static int getHunterCreature() {
		return DungeonConstants.HUNTER_CREATURES[Misc.randomNoPlus(DungeonConstants.HUNTER_CREATURES.length)];
	}

	public static int getSlayerCreature() {
		return DungeonConstants.SLAYER_CREATURES[Misc.randomNoPlus(DungeonConstants.SLAYER_CREATURES.length)];
	}

	public static int getMiningResource(int rockType, int floorType) {
		return DungeonConstants.MINING_RESOURCES[floorType] + rockType * 2;
	}

	public static int getWoodcuttingResource(int treeType, int floorType) {
		return DungeonConstants.WOODCUTTING_RESOURCES[floorType] + treeType * 2;
	}

	public static int getFarmingResource(int flowerType, int floorType) {
		return DungeonConstants.FARMING_RESOURCES[floorType] + flowerType * 2;
	}
	
	public static boolean isLadder(int id, int type) {
		return DungeonConstants.LADDERS[type] == id;
	}

	public static int getMaxFloor(int level) {
		return (level + 1) / 2;
	}

	private static final int[] FROZEN_FLOORS = { 1, 11 };
	private static final int[] ABANDONED_FLORS = { 12, 17 };
	private static final int[] FURNISHED_FLOORS = { 18, 29 };
	private static final int[] ABANDONED2_FLOORS = { 30, 35 };
	private static final int[] OCCULT_FLOORS = { 36, 47 };
	private static final int[] WARPED_FLOORS = { 48, 60 };

	public static int[] getFloorThemeRange(int floorId) {
		if (floorId <= 11) return FROZEN_FLOORS;
		if (floorId <= 17) return ABANDONED_FLORS;
		if (floorId <= 29) return FURNISHED_FLOORS;
		if (floorId <= 35) return ABANDONED2_FLOORS;
		if (floorId <= 47) return OCCULT_FLOORS;
		return WARPED_FLOORS;
	}

	public static int getTierForLevel(int level) {
		if (level >= 90) {
			return 9;
		} else if (level >= 80) {
			return 8;
		} else if (level >= 70) {
			return 7;
		} else if (level >= 60) {
			return 6;
		} else if (level >= 50) {
			return 5;
		} else if (level >= 40) {
			return 4;
		} else if (level >= 30) {
			return 3;
		} else if (level >= 20) {
			return 2;
		} else if (level >= 10) {
			return 1;
		} else {
			return 0;
		}
	}

}
