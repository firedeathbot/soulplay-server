package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.npcs.NpcStatsEntry;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {10040, 10041, 10042, 10043, 10044, 10045, 10046, 10047, 10048, 10049, 10050, 10051, 10052, 10053, 10054,
		10055, 10056, 10057})
public class IcyBones extends Boss {

	public IcyBones(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int random = Misc.random(40);
		if (random >= 35) {
			npc.attackType = 5;
			int size = 8;
			int x = npc.absX - (size / 2);
			int y = npc.absY - (size / 2);

			for (int i1 = 0; i1 < 4; i1++) {
				for (int jk = 0; jk < size; jk++) {
					int randomObj = 52285 + Misc.random(2);
					int randomTicks = 10;
					if (i1 == 0) {
						y++;
						if (!RegionClip.canStandOnSpot(x, y, 0)) {
							continue;
						}
						ObjectManager.addObject(new RSObject(randomObj, x, y, 0, 0, 10, -1,
								randomTicks));
					} else if (i1 == 1) {
						x++;
						if (!RegionClip.canStandOnSpot(x, y, 0)) {
							continue;
						}
						ObjectManager.addObject(new RSObject(randomObj, x, y, 0, 0, 10, -1,
								randomTicks));
					} else if (i1 == 2) {
						y--;
						if (!RegionClip.canStandOnSpot(x, y, 0)) {
							continue;
						}
						ObjectManager.addObject(new RSObject(randomObj, x, y, 0, 0, 10, -1,
								randomTicks));
					} else if (i1 == 3) {
						x--;
						if (!RegionClip.canStandOnSpot(x, y, 0)) {
							continue;
						}
						ObjectManager.addObject(new RSObject(randomObj, x, y, 0, 0, 10, -1,
								randomTicks));
					}
				}
			}
		} else if (random >= 30) {
			npc.attackType = 2;
			npc.endGfx = 369;
			npc.projectileId = -1;
			if (c.applyFreeze(12)) {
				c.sendMessage("You have been Frozen!");
			}
		} else if (random >= 15) {
			npc.projectileId = 1884;
			npc.attackType = 1;
		} else {
			npc.attackType = 0;
			//TODO graphic and atk other players
		}
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		npc.properties = Misc.addBits(NpcStatsEntry.BARRICADE);
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}
	
	public int followDistance() {
		return 10;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 10;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
}
