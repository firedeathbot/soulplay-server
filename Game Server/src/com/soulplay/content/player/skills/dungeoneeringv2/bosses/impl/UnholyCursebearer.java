package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Manager;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

//TODO heal(1/8 of max health) the boss when touched the unholy font, restores player stats
@BossMeta(ids = {10111, 10112, 10113, 10114, 10115, 10116, 10117, 10118, 10119, 10120, 10121, 10122, 10123,
		10124, 10125, 10126, 10127})
public class UnholyCursebearer extends Boss {
	
	private static final int SPECIAL_ATTACK_TICKS = 10;

	public UnholyCursebearer(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		if (npc.dungeonManager == null) {
			return;
		}

		if (npc.getStopWatch().checkThenStart(SPECIAL_ATTACK_TICKS)) {
			for (Player p : Manager.getNearbyPlayers(npc)) {
				for(int i = 0; i < Skills.STAT_COUNT; i++) {
					p.getSkills().drainLevel(i, 4, 0);
				}

				p.dealDamage(new Hit(Misc.random(10), 0, -1));
				//TODO Graphic
			}
		}
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int random = Misc.random(1);
		if (random == 1) {
			npc.attackType = 2;
			npc.projectileId = 1884;
		} else {
			npc.attackType = 0;
		}
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		npc.getStopWatch().startTimer(SPECIAL_ATTACK_TICKS);
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}

	public int followDistance() {
		return 0;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
}
