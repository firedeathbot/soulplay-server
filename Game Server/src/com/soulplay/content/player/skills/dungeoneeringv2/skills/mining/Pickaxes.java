package com.soulplay.content.player.skills.dungeoneeringv2.skills.mining;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;

public enum Pickaxes {

	//13074-13084 mining wall?
	NOVITE(new int[] {16295, 16142}, 1, 13107, 13085, 13096),
	BATHUS(new int[] {16297, 16143}, 10, 13108, 13086, 13097),
	MARMAROS(new int[] {16299, 16144}, 20, 13109, 13087, 13098),
	KRATONITE(new int[] {16301, 16145}, 30, 13110, 13088, 13099),
	FRACTITE(new int[] {16303, 16146}, 40, 13111, 13089, 13100),
	ZEPHYRIUM(new int[] {16305, 16147}, 50, 13112, 13090, 13101),
	ARGONITE(new int[] {16307, 16148}, 60, 13113, 13091, 13102),
	KATAGON(new int[] {16309, 16149}, 70, 13114, 13092, 13103),
	GORGONITE(new int[] {16311, 16150}, 80, 13115, 13093, 13104),
	PROMETHIUM(new int[] {16313, 16151}, 90, 13116, 13094, 13105),
	PRIMAL(new int[] {16315, 16152}, 99, 13117, 13095, 13106);

	private static final Pickaxes[] values = values();
	private final int levelRequried, animationId, doorOpenAnimId, doorFailAnimId;
	private final int[] pickAxeIds;

	private Pickaxes(int[] pickAxeIds, int levelRequried, int animationId, int doorOpenAnimId, int doorFailAnimId) {
		this.pickAxeIds = pickAxeIds;
		this.levelRequried = levelRequried;
		this.animationId = animationId;
		this.doorOpenAnimId = doorOpenAnimId;
		this.doorFailAnimId = doorFailAnimId;
	}

	public int[] getPickAxeIds() {
		return pickAxeIds;
	}

	public int getLevelRequried() {
		return levelRequried;
	}

	public int getAnimationId() {
		return animationId;
	}

	public int getDoorOpenAnimId() {
		return doorOpenAnimId;
	}

	public int getDoorFailAnimId() {
		return doorFailAnimId;
	}

	public static Pickaxes getPickaxe(Client player) {
		for (int i = values.length - 1; i >= 0; i--) {// from best to worst
			Pickaxes def = Pickaxes.values[i];
			int ids[] = def.getPickAxeIds();
			if (player.getItems().playerHasAnyItem(ids)
					|| player.getItems().playerHasEquippedWeapons(ids)) {
				if (player.getSkills().getLevel(Skills.MINING) >= def.getLevelRequried()) {
					return def;
				}
			}
		}

		return null;
	}

}
