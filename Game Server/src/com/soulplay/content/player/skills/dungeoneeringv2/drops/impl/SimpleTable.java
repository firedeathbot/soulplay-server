package com.soulplay.content.player.skills.dungeoneeringv2.drops.impl;

import com.soulplay.content.player.skills.dungeoneeringv2.drops.DropTableInfo;

public class SimpleTable implements DropTableInfo {

	@Override
	public int tableRollCount() {
		return 1;
	}

	@Override
	public boolean dropFullTable() {
		return false;
	}

	@Override
	public int minTier() {
		return 1;
	}

	@Override
	public int maxTier() {
		return 11;
	}

}