package com.soulplay.content.player.skills.dungeoneeringv2.items;

public class DungeonItems {
	
	public static void load() {
		/* empty */
	}

	//itemId, itemAmount, tier
	
	public static final int[][] TWO_HANDED_SWORDS = { 
			{16889, 1, 1, 1},
			{16891, 1, 1, 2},
			{16893, 1, 1, 3},
			{16895, 1, 1, 4},
			{16897, 1, 1, 5},
			{16899, 1, 1, 6},
			{16901, 1, 1, 7},
			{16903, 1, 1, 8},
			{16905, 1, 1, 9},
			{16907, 1, 1, 10},
			{16909, 1, 1, 11},
	};

	public static final int[][] BATTLEAXES = { 
			{ 15753, 1, 1, 1 },
			{ 15755, 1, 1, 2 }, 
			{ 15757, 1, 1, 3 }, 
			{ 15759, 1, 1, 4 },
			{ 15761, 1, 1, 5 }, 
			{ 15763, 1, 1, 6 }, 
			{ 15765, 1, 1, 7 }, 
			{ 15767, 1, 1, 8 }, 
			{ 15769, 1, 1, 9 }, 
			{ 15771, 1, 1, 10 },
			{ 15773, 1, 1, 11 },
	};
	
	public static final int[][] GAUNTLETS = { 
			{16273, 1, 1, 1},
			{16275, 1, 1, 2},
			{16277, 1, 1, 3},
			{16279, 1, 1, 4},
			{16281, 1, 1, 5},
			{16283, 1, 1, 6},
			{16285, 1, 1, 7},
			{16287, 1, 1, 8},
			{16289, 1, 1, 9},
			{16291, 1, 1, 10},
			{16293, 1, 1, 11},
	};
	
	public static final int[][] PICKAXES = { 
			{16295, 1, 1, 1},
			{16297, 1, 1, 2},
			{16299, 1, 1, 3},
			{16301, 1, 1, 4},
			{16303, 1, 1, 5},
			{16305, 1, 1, 6},
			{16307, 1, 1, 7},
			{16309, 1, 1, 8},
			{16311, 1, 1, 9},
			{16313, 1, 1, 10},
			{16315, 1, 1, 11},
	};
	
	public static final int[][] BOOTS = { 
			{16339, 1, 1, 1},
			{16341, 1, 1, 2},
			{16343, 1, 1, 3},
			{16345, 1, 1, 4},
			{16347, 1, 1, 5},
			{16349, 1, 1, 6},
			{16351, 1, 1, 7},
			{16353, 1, 1, 8},
			{16355, 1, 1, 9},
			{16357, 1, 1, 10},
			{16359, 1, 1, 11},
	};
	
	public static final int[][] HATCHETS = { 
			{16361, 1, 1, 1},
			{16363, 1, 1, 2},
			{16365, 1, 1, 3},
			{16367, 1, 1, 4},
			{16369, 1, 1, 5},
			{16371, 1, 1, 6},
			{16373, 1, 1, 7},
			{16375, 1, 1, 8},
			{16377, 1, 1, 9},
			{16379, 1, 1, 10},
			{16381, 1, 1, 11},
	};
	
	public static final int[][] LONGSWORDS = { 
			{16383, 1, 1, 1},
			{16385, 1, 1, 2},
			{16387, 1, 1, 3},
			{16389, 1, 1, 4},
			{16391, 1, 1, 5},
			{16393, 1, 1, 6},
			{16395, 1, 1, 7},
			{16397, 1, 1, 8},
			{16399, 1, 1, 9},
			{16401, 1, 1, 10},
			{16403, 1, 1, 11},
	};
	
	public static final int[][] MAULS = { 
			{16405, 1, 1, 1},
			{16407, 1, 1, 2},
			{16409, 1, 1, 3},
			{16411, 1, 1, 4},
			{16413, 1, 1, 5},
			{16415, 1, 1, 6},
			{16417, 1, 1, 7},
			{16419, 1, 1, 8},
			{16421, 1, 1, 9},
			{16423, 1, 1, 10},
			{16425, 1, 1, 11},
	};
	
	public static final int[][] ARROWS = { 
			{16427, 1, 1, 1},
			{16432, 1, 1, 2},
			{16437, 1, 1, 3},
			{16442, 1, 1, 4},
			{16447, 1, 1, 5},
			{16452, 1, 1, 6},
			{16457, 1, 1, 7},
			{16462, 1, 1, 8},
			{16467, 1, 1, 9},
			{16472, 1, 1, 10},
			{16477, 1, 1, 11},
	};
	
	public static final int[][] PLATELEGS = { 
			{16669, 1, 1, 1},
			{16671, 1, 1, 2},
			{16673, 1, 1, 3},
			{16675, 1, 1, 4},
			{16677, 1, 1, 5},
			{16679, 1, 1, 6},
			{16681, 1, 1, 7},
			{16683, 1, 1, 8},
			{16685, 1, 1, 9},
			{16687, 1, 1, 10},
			{16689, 1, 1, 11},
	};
	
	public static final int[][] PLATESKIRTS = { 
			{16647, 1, 1, 1},
			{16649, 1, 1, 2},
			{16651, 1, 1, 3},
			{16653, 1, 1, 4},
			{16655, 1, 1, 5},
			{16657, 1, 1, 6},
			{16659, 1, 1, 7},
			{16661, 1, 1, 8},
			{16663, 1, 1, 9},
			{16665, 1, 1, 10},
			{16667, 1, 1, 11},
	};
	
	public static final int[][] FULL_HELMS = { 
			{16691, 1, 1, 1},
			{16693, 1, 1, 2},
			{16695, 1, 1, 3},
			{16697, 1, 1, 4},
			{16699, 1, 1, 5},
			{16701, 1, 1, 6},
			{16703, 1, 1, 7},
			{16705, 1, 1, 8},
			{16707, 1, 1, 9},
			{16709, 1, 1, 10},
			{16711, 1, 1, 11},
	};
	
	public static final int[][] CHAINBODYS = { 
			{16713, 1, 1, 1},
			{16715, 1, 1, 2},
			{16717, 1, 1, 3},
			{16719, 1, 1, 4},
			{16721, 1, 1, 5},
			{16723, 1, 1, 6},
			{16725, 1, 1, 7},
			{16727, 1, 1, 8},
			{16729, 1, 1, 9},
			{16731, 1, 1, 10},
			{16733, 1, 1, 11},
	};
	
	public static final int[][] DAGGERS = { 
			{16757, 1, 1, 1},
			{16765, 1, 1, 2},
			{16773, 1, 1, 3},
			{16781, 1, 1, 4},
			{16789, 1, 1, 5},
			{16797, 1, 1, 6},
			{16805, 1, 1, 7},
			{16813, 1, 1, 8},
			{16821, 1, 1, 9},
			{16829, 1, 1, 10},
			{16837, 1, 1, 11},
	};
	
	public static final int[][] TORN_BAGS = { 
			{17995, 1, 1, 1},
			{17997, 1, 1, 2},
			{17999, 1, 1, 3},
			{18001, 1, 1, 4},
			{18003, 1, 1, 5},
			{18005, 1, 1, 6},
			{18007, 1, 1, 7},
			{18009, 1, 1, 8},
			{18011, 1, 1, 9},
			{18013, 1, 1, 10},
	};
	
	public static final int[][] ORES = { 
			{17630, 1, 2, 1},
			{17632, 1, 2, 2},
			{17634, 1, 2, 3},
			{17636, 1, 2, 4},
			{17638, 1, 2, 5},
			{17640, 1, 2, 6},
			{17642, 1, 2, 7},
			{17644, 1, 2, 8},
			{17646, 1, 2, 9},
			{17648, 1, 2, 10},
	};
	
	public static final int[][] BRANCHES = { 
			{17682, 1, 3, 1},
			{17684, 1, 3, 2},
			{17686, 1, 3, 3},
			{17688, 1, 3, 4},
			{17690, 1, 3, 5},
			{17692, 1, 3, 6},
			{17694, 1, 3, 7},
			{17696, 1, 3, 8},
			{17698, 1, 3, 9},
			{17700, 1, 3, 10},
	};
	
	public static final int[][] TEXTILES = { 
			{17448, 1, 2, 1},
			{17450, 1, 2, 2},
			{17452, 1, 2, 3},
			{17454, 1, 2, 4},
			{17456, 1, 2, 5},
			{17458, 1, 2, 6},
			{17460, 1, 2, 7},
			{17462, 1, 2, 8},
			{17464, 1, 2, 9},
			{17466, 1, 2, 10},
	};
	
	public static final int[][] HERBS = { 
			{17494, 1, 2, 1}, //Grimy sagewort
			{17496, 1, 2, 2}, //Grimy valerian
			{17498, 1, 2, 3}, //Grimy aloe
			{17500, 1, 2, 4}, //Grimy wormwood leaf
			{17502, 1, 2, 5}, //Grimy magebane
			{17504, 1, 2, 6}, //Grimy featherfoil
			{17506, 1, 2, 7}, //Grimy winter's grip
			{17508, 1, 2, 8}, //Grimy lycopus
			{17510, 1, 2, 9}, //Grimy buckthorn
	};
	
	public static final int[][] SEEDS = { //change the tier lvl of these
			{17823, 1, 2, 1}, //Cave potato seed
			{17824, 1, 2, 1}, //Gissel mushroom spore
			{17825, 1, 2, 1}, //Edicap mushroom spore
			{17826, 1, 2, 1}, //Sagewort seed
			{17827, 1, 2, 1}, //Valerian seed
			{17828, 1, 2, 1}, //Aloe seed
			{17829, 1, 2, 2}, //Wormwood seed
			{17830, 1, 2, 2}, //Magebane seed
			{17831, 1, 2, 2}, //Featherfoil seed
			{17832, 1, 2, 3}, //Winter's grip seed
			{17833, 1, 2, 3}, //Lycopus seed
			{17834, 1, 2, 3}, //Buckthorn seed
	};
	
	public static final int[][] FOOD = { //itemId, minAmount, maxAmount, tier
			{18159, 1, 1, 1}, //Heim crab
			{18161, 1, 1, 2}, //Red-eye
			{18163, 1, 1, 3}, //Dusk eel
			{18165, 1, 1, 4}, //Giant flatfish
			{18167, 1, 1, 5}, //Short-finned eel
			{18169, 1, 1, 6}, //Web snipper
			{18171, 1, 1, 7}, //Bouldabass
			{18173, 1, 1, 8}, //Salve eel
	};
	
	public static final int[][] RUNES = { //itemId, minAmount, maxAmount, tier
			{17780, 10, 100, 1}, //air rune
			{17784, 10, 100, 1}, //mind rune
			{17781, 10, 100, 1}, //water rune
			{17782, 10, 100, 1}, //earth rune
			{17783, 10, 100, 1}, //fire rune
			{17788, 10, 100, 1}, //body rune
			{17789, 3, 30, 1}, //cosmic rune
			{17785, 10, 100, 2}, //chaos rune			
			{17790, 100, 100, 2}, //astral rune
			{17791, 3, 50, 2}, //nature rune
			{17792, 3, 40, 2}, //law rune
			{17786, 10, 100, 3}, //death rune
			{17787, 10, 100, 3}, //blood rune
			{17793, 10, 100, 3}, //soul rune
	};
	
	public static final int[][] MISC = { //itemId, minAmount, maxAmount, tier
			{16933, 1, 1, 1}, //Anti-fireshield
			{17752, 1, 2, 1}, //Bow string
			{17490, 1, 3, 1}, //Vial
			{17447, 5, 50, 1}, //Thread
			{17796, 10, 100, 1}, //Feather
			{17776, 5, 105, 1}, //rune essence
	};
	
	
	public static final int[][] RAPIER = { 
			{16935, 1, 1, 1}, //Novite rapier
			{16937, 1, 1, 2}, //Bathus rapier
			{16939, 1, 1, 3}, //Marmaros rapier
			{16941, 1, 1, 4}, //Kratonite rapier
			{16943, 1, 1, 5}, //Fractite rapier
			{16945, 1, 1, 6}, //Zephyrium rapier
			{16947, 1, 1, 7}, //Argonite rapier
			{16949, 1, 1, 8}, //Katagon rapier
			{16951, 1, 1, 9}, //Gorgonite rapier
			{16953, 1, 1, 10}, //Promethium rapier
			{16955, 1, 1, 11}, //Primal rapier
	};
	
	public static final int[][] WARHAMMERS = { 
			{17019, 1, 1, 1}, //Novite warhammer
			{17021, 1, 1, 2}, //Bathus warhammer
			{17023, 1, 1, 3}, //Marmaros warhammer
			{17025, 1, 1, 4}, //Kratonite warhammer
			{17027, 1, 1, 5}, //Fractite warhammer
			{17029, 1, 1, 6}, //Zephyrium warhammer
			{17031, 1, 1, 7}, //Argonite warhammer
			{17033, 1, 1, 8}, //Katagon warhammer
			{17035, 1, 1, 9}, //Gorgonite warhammer
			{17037, 1, 1, 10}, //Promethium warhammer
			{17039, 1, 1, 11}, //Primal warhammer
	};
	
	public static final int[][] SPEARS = { 
			{17063, 1, 1, 1}, //Novite spear
			{17071, 1, 1, 2}, //Bathus spear
			{17079, 1, 1, 3}, //Marmaros spear
			{17087, 1, 1, 4}, //Kratonite spear
			{17095, 1, 1, 5}, //Fractite spear
			{17103, 1, 1, 6}, //Zephyrium spear
			{17111, 1, 1, 7}, //Argonite spear
			{17119, 1, 1, 8}, //Katagon spear
			{17127, 1, 1, 9}, //Gorgonite spear
			{17135, 1, 1, 10}, //Promethium spear
			{17143, 1, 1, 11}, //Primal spear
	};
	
	public static final int[][] PLATEBODYS = { 
			{17239, 1, 1, 1}, //Novite platebody
			{17241, 1, 1, 2}, //Bathus platebody
			{17243, 1, 1, 3}, //Marmaros platebody
			{17245, 1, 1, 4}, //Kratonite platebody
			{17247, 1, 1, 5}, //Fractite platebody
			{17249, 1, 1, 6}, //Zephyrium platebody
			{17251, 1, 1, 7}, //Argonite platebody
			{17253, 1, 1, 8}, //Katagon platebody
			{17255, 1, 1, 9}, //Gorgonite platebody
			{17257, 1, 1, 10}, //Promethium platebody
			{17259, 1, 1, 11}, //Primal platebody
	};
	
	public static final int[][] KITESHIELDS = { 
			{17341, 1, 1, 1}, //Novite kiteshield
			{17343, 1, 1, 2}, //Bathus kiteshield
			{17345, 1, 1, 3}, //Marmaros kiteshield
			{17347, 1, 1, 4}, //Kratonite kiteshield
			{17349, 1, 1, 5}, //Fractite kiteshield
			{17351, 1, 1, 6}, //Zephyrium kiteshield
			{17353, 1, 1, 7}, //Argonite kiteshield
			{17355, 1, 1, 8}, //Katagon kiteshield
			{17357, 1, 1, 9}, //Gorgonite kiteshield
			{17359, 1, 1, 10}, //Promethium kiteshield
			{17361, 1, 1, 11}, //Primal kiteshield
	};
	
	public static final int[][] HOODS = { 
			{16735, 1, 1, 1}, //Salve hood
			{16737, 1, 1, 2}, //Wildercress hood
			{16739, 1, 1, 3}, //Blightleaf hood
			{16741, 1, 1, 4}, //Roseblood hood
			{16743, 1, 1, 5}, //Bryll hood
			{16745, 1, 1, 6}, //Duskweed hood
			{16747, 1, 1, 7}, //Soulbell hood
			{16749, 1, 1, 8}, //Ectohood
			{16751, 1, 1, 9}, //Runic hood
			{16753, 1, 1, 10}, //Spiritbloom hood
			{16755, 1, 1, 11}, //Celestial hood
	};
	
	public static final int[][] ROBE_BOTTOMS = { 
			{16845, 1, 1, 1}, //Salve robe bottom
			{16847, 1, 1, 2}, //Wildercress robe bottom
			{16849, 1, 1, 3}, //Blightleaf robe bottom
			{16851, 1, 1, 4}, //Roseblood robe bottom
			{16853, 1, 1, 5}, //Bryll robe bottom
			{16855, 1, 1, 6}, //Duskweed robe bottom
			{16857, 1, 1, 7}, //Soulbell robe bottom
			{16859, 1, 1, 8}, //Ectorobe bottom
			{16861, 1, 1, 9}, //Runic robe bottom
			{16863, 1, 1, 10}, //Spiritbloom robe bottom
			{16865, 1, 1, 11}, //Celestial robe bottom
	};
	
	public static final int[][] SHOES = { 
			{16911, 1, 1, 1}, //Salve shoes
			{16913, 1, 1, 2}, //Wildercress shoes
			{16915, 1, 1, 3}, //Blightleaf shoes
			{16917, 1, 1, 4}, //Roseblood shoes
			{16919, 1, 1, 5}, //Bryll shoes
			{16921, 1, 1, 6}, //Duskweed shoes
			{16923, 1, 1, 7}, //Soulbell shoes
			{16925, 1, 1, 8}, //Ectoshoes
			{16927, 1, 1, 9}, //Runic shoes
			{16929, 1, 1, 10}, //Spiritbloom shoes
			{16931, 1, 1, 11}, //Celestial shoes
	};
	
	public static final int[][] GLOVES = { 
			{17151, 1, 1, 1}, //Salve gloves
			{17153, 1, 1, 2}, //Wildercress gloves
			{17155, 1, 1, 3}, //Blightleaf gloves
			{17157, 1, 1, 4}, //Roseblood gloves
			{17159, 1, 1, 5}, //Bryll gloves
			{17161, 1, 1, 6}, //Duskweed gloves
			{17163, 1, 1, 7}, //Soulbell gloves
			{17165, 1, 1, 8}, //Ectogloves
			{17167, 1, 1, 9}, //Runic gloves
			{17169, 1, 1, 10}, //Spiritbloom gloves
			{17171, 1, 1, 11}, //Celestial gloves
	};
	
	public static final int[][] NORMAL_STAFFS = { 
			{16977, 1, 1, 1}, //Tangle gum staff
			{16979, 1, 1, 2}, //Seeping elm staff
			{16981, 1, 1, 3}, //Blood spindle staff
			{16983, 1, 1, 4}, //Utuku staff
			{16985, 1, 1, 5}, //Spinebeam staff
			{16987, 1, 1, 6}, //Bovistrangler staff
			{16989, 1, 1, 7}, //Thigat staff
			{16991, 1, 1, 8}, //Corpsethorn staff
			{16993, 1, 1, 9}, //Entgallow staff
			{16995, 1, 1, 10}, //Grave creeper staff
	};
	
	public static final int[][] UPGRADED_STAFFS = { 
			{16997, 1, 1, 1}, //water staff
			{17001, 1, 1, 2}, //earth staff
			{17005, 1, 1, 3}, //fire staff
			{17009, 1, 1, 4}, //air staff
			{17013, 1, 1, 5}, //catalytic staff
			{16999, 1, 1, 6}, //empowered water staff
			{17003, 1, 1, 7}, //empowered earth staff
			{17007, 1, 1, 8}, //empowered fire staff
			{17011, 1, 1, 9}, //empowered air staff
			{17015, 1, 1, 10}, //empowered catalytic staff
			{17017, 1, 1, 11}, //celestial catalytic staff
	};
	
	public static final int[][] ROBE_TOPS = { 
			{17217, 1, 1, 1}, //Salve robe top
			{17219, 1, 1, 2}, //Wildercress robe top
			{17221, 1, 1, 3}, //Blightleaf robe top
			{17223, 1, 1, 4}, //Roseblood robe top
			{17225, 1, 1, 5}, //Bryll robe top
			{17227, 1, 1, 6}, //Duskweed robe top
			{17229, 1, 1, 7}, //Soulbell robe top
			{17231, 1, 1, 8}, //Ectorobe top
			{17233, 1, 1, 9}, //Runic robe top
			{17235, 1, 1, 10}, //Spiritbloom robe top
			{17237, 1, 1, 11}, //Celestial robe top
	};
	
	public static final int[][] COIFS = { 
			{17041, 1, 1, 1}, //Protoleather coif
			{17043, 1, 1, 2}, //Subleather coif
			{17045, 1, 1, 3}, //Paraleather coif
			{17047, 1, 1, 4}, //Archleather coif
			{17049, 1, 1, 5}, //Dromoleather coif
			{17051, 1, 1, 6}, //Spinoleather coif
			{17053, 1, 1, 7}, //Gallileather coif
			{17055, 1, 1, 8}, //Stegoleather coif
			{17057, 1, 1, 9}, //Megaleather coif
			{17059, 1, 1, 10}, //Tyrannoleather coif
			{17061, 1, 1, 11}, //Sagittarian coif
	};
	
	public static final int[][] LEATHER_BODYS = { 
			{17173, 1, 1, 1}, //Protoleather body
			{17175, 1, 1, 2}, //Subleather body
			{17177, 1, 1, 3}, //Paraleather body
			{17179, 1, 1, 4}, //Archleather body
			{17181, 1, 1, 5}, //Dromoleather body
			{17183, 1, 1, 6}, //Spinoleather body
			{17185, 1, 1, 7}, //Gallileather body
			{17187, 1, 1, 8}, //Stegoleather body
			{17189, 1, 1, 9}, //Megaleather body
			{17191, 1, 1, 10}, //Tyrannoleather body
			{17193, 1, 1, 11}, //Sagittarian body
	};
	
	public static final int[][] VAMBRACES = { 
			{17195, 1, 1, 1}, //Protoleather vambraces
			{17197, 1, 1, 2}, //Subleather vambraces
			{17199, 1, 1, 3}, //Paraleather vambraces
			{17201, 1, 1, 4}, //Archleather vambraces
			{17203, 1, 1, 5}, //Dromoleather vambraces
			{17205, 1, 1, 6}, //Spinoleather vambraces
			{17207, 1, 1, 7}, //Gallileather vambraces
			{17209, 1, 1, 8}, //Stegoleather vambraces
			{17211, 1, 1, 9}, //Megaleather vambraces
			{17213, 1, 1, 10}, //Tyrannoleather vambraces
			{17215, 1, 1, 11}, //Sagittarian vambraces
	};
	
	public static final int[][] LEATHER_BOOTS = { 
			{17297, 1, 1, 1}, //Protoleather boots
			{17299, 1, 1, 2}, //Subleather boots
			{17301, 1, 1, 3}, //Paraleather boots
			{17303, 1, 1, 4}, //Archleather boots
			{17305, 1, 1, 5}, //Dromoleather boots
			{17307, 1, 1, 6}, //Spinoleather boots
			{17309, 1, 1, 7}, //Gallileather boots
			{17311, 1, 1, 8}, //Stegoleather boots
			{17313, 1, 1, 9}, //Megaleather boots
			{17315, 1, 1, 10}, //Tyrannoleather boots
			{17317, 1, 1, 11}, //Sagittarian boots
	};
	
	public static final int[][] CHAPS = { 
			{17319, 1, 1, 1}, //Protoleather chaps
			{17321, 1, 1, 2}, //Subleather chaps
			{17323, 1, 1, 3}, //Paraleather chaps
			{17325, 1, 1, 4}, //Archleather chaps
			{17327, 1, 1, 5}, //Dromoleather chaps
			{17329, 1, 1, 6}, //Spinoleather chaps
			{17331, 1, 1, 7}, //Gallileather chaps
			{17333, 1, 1, 8}, //Stegoleather chaps
			{17335, 1, 1, 9}, //Megaleather chaps
			{17337, 1, 1, 10}, //Tyrannoleather chaps
			{17339, 1, 1, 11}, //Sagittarian chaps
	};
	
	public static final int[][] SHORTBOWS = { 
			{16867, 1, 1, 1}, //Tangle gum shortbow
			{16869, 1, 1, 2}, //Seeping elm shortbow
			{16871, 1, 1, 3}, //Blood spindle shortbow
			{16873, 1, 1, 4}, //Utuku shortbow
			{16875, 1, 1, 5}, //Spinebeam shortbow
			{16877, 1, 1, 6}, //Bovistrangler shortbow
			{16879, 1, 1, 7}, //Thigat shortbow
			{16881, 1, 1, 8}, //Corpsethorn shortbow
			{16883, 1, 1, 9}, //Entgallow shortbow
			{16885, 1, 1, 10}, //Grave creeper shortbow
			{16887, 1, 1, 11}, //Sagittarian shortbow
	};
	
	public static final int[][] LONGBOWS = { 
			{16317, 1, 1, 1}, //Tangle gum longbow
			{16319, 1, 1, 2}, //Seeping elm longbow
			{16321, 1, 1, 3}, //Blood spindle longbow
			{16323, 1, 1, 4}, //Utuku longbow
			{16325, 1, 1, 5}, //Spinebeam longbow
			{16327, 1, 1, 6}, //Bovistrangler longbow
			{16329, 1, 1, 7}, //Thigat longbow
			{16331, 1, 1, 8}, //Corpsethorn longbow
			{16333, 1, 1, 9}, //Entgallow longbow
			{16335, 1, 1, 10}, //Grave creeper longbow
			{16337, 1, 1, 11}, //Sagittarian longbow
	};
	
	
}
