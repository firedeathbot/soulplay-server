package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@BossMeta(ids = {10143, 10144, 10145, 10146, 10147, 10148, 10149, 10150, 10151, 10152, 10153, 10154, 10155, 10156})
public class ShadowForger extends Boss {

	public ShadowForger(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int random = Misc.random(40);
		if (random >= 39) {
			npc.startGraphic(Graphic.create(2370));
			npc.startAnimation(13019);
			
			Location npcLoc = npc.getCurrentLocation().transform(npc.getSize() / 2, npc.getSize() / 2);
			
			for (int i = 0; i < 10; i++) {
				Location tile = new Location(npcLoc, 6);
				if (Misc.colides(npc.getX(), npc.getY(), npc.getSize(), tile.getX(), tile.getY(), 1)) {
					continue;
				}
		
				Projectile proj = new Projectile(2371, npcLoc, tile);
				proj.setStartHeight(25);
				proj.setEndHeight(25);
				proj.setStartDelay(50);
				GlobalPackets.createProjectile(proj);
				c.getPacketSender().sendStillGraphics(2372, npc.getHeightLevel(), tile.getY(), tile.getX(), 90);
				
				CycleEventHandler.getSingleton().addEvent(proj, new CycleEvent() {

					@Override
					public void execute(
							CycleEventContainer container) {
						if (c.disconnected) {
							container.stop();
							return;
						}
						
						//Loop players
						/*if (c.getX() == tile.getX() && c.getY() == tile.getY()) {
							c.dealDamage(new Hit(Misc.random(10), 0, 1));//TODO damage stuff
						}*/
						
						boolean hit = false;
						
						for (int j = 0; j < 2; j++) {
							Location tile2 = new Location(tile, 1);
							Projectile proj = new Projectile(2373, tile, tile2);
							proj.setStartHeight(25);
							proj.setEndHeight(25);
							proj.setStartDelay(50);
							GlobalPackets.createProjectile(proj);
							c.getPacketSender().sendStillGraphics(2374, npc.getHeightLevel(), tile2.getY(), tile2.getX(), 70);
							
							if (!hit)
								hit = c.withinDistance(tile2.getX(), tile2.getY(), c.getZ(), 1);
						}
						
						if (hit) {
							c.dealDamage(new Hit(10, 0, 1));//TODO how dmg should be done
						}
						
						container.stop();
					}

					@Override
					public void stop() {
					}
				}, 2);
				
			}
			
			npc.attackType = 5;
		} else {
			npc.attackType = 0;
		}
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		//TODO spawn animation
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}

	public int followDistance() {
		return 0;
	}

	public boolean canMove() {
		return false;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
}
