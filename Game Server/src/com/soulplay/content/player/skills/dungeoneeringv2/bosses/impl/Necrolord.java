package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import java.util.ArrayList;
import java.util.List;
import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

@BossMeta(ids = {11737, 11738, 11739, 11740, 11741, 11742, 11743, 11744, 11745, 11746, 11747, 11748, 11749, 11750, 11751})
public class Necrolord extends Boss {
	
	private int resetTicks;
	public static final int SKELETAL_MINIONS = 0;
	public static final int SKELETAL_MINIONS_DESPAWN_TICKS = 50;

	public Necrolord(NPC boss) {
		super(boss);
	}

	private int damage(DungeonManager manager) {
		int teamAvg = manager.teamAvg;
		int minAvg = 85;
		int maxAvg = 3960;

		return Misc.interpolate(1, 4, minAvg, maxAvg, teamAvg);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		if (npc.dungeonManager == null)
			return;
		
		List<NPC> list = ((ArrayList<NPC>) npc.dungVariables[SKELETAL_MINIONS]);

		int attack = Misc.random(3);
        switch(attack) {
		case 0:
		case 1:
		case 2:
			npc.startAnimation(14222);
			npc.startGraphic(Graphic.create(2716));
			npc.projectileId = 2721;
			npc.attackType = 2;
			npc.endGfx = 2726;
			npc.projectileDelay = 50;
			break;
		case 3:
		case 4:
			if (attack == 3) {
			    npc.startAnimation(716);
				npc.startGraphic(Graphic.create(108, GraphicType.HIGH));
			} else {
				npc.startAnimation(711);
				npc.startGraphic(Graphic.create(177, GraphicType.HIGH));
			}
			npc.attackType = 5;
			npc.projectiletStartHeight = 43;
			int playerX = c.absX;
			int playerY = c.absY;
			
			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
				
				int timer = 0;
				
				@Override
				public void stop() {
					
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					
					timer ++;
					if (timer == 1) {
						int projectileId = attack == 3 ? 109 : 178;
						Projectile proj = new Projectile(projectileId,
								Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()),
								Location.create(playerX, playerY));
						proj.setStartHeight(43);
						proj.setEndHeight(5);
						proj.setStartDelay(65);
						proj.setEndDelay(100);
						GlobalPackets.createProjectile(proj);
						
					}
					if (timer == 4) {
						int targetDmg = damage(npc.dungeonManager);
						for (int i = 0; i < npc.dungeonManager.party.partyMembers.size(); i++) {
							Client partyMember = npc.dungeonManager.party.partyMembers.get(i);

							if (partyMember.absX == playerX && partyMember.absY == playerY) {
								partyMember.dealDamage(new Hit(targetDmg + Misc.random(targetDmg), 0, -1));
								if (attack == 3) {
									Server.getStillGraphicsManager().createGfx(c.getX(), c.getY(), c.getHeightLevel(), 110, 0, 0);
									partyMember.getMovement().drainEnergy(c.getRunEnergy() / 2);
								} else {
									c.rootEntity(10);
									Server.getStillGraphicsManager().createGfx(c.getX(), c.getY(), c.getHeightLevel(), 179, 43, 0);
								}
								partyMember.sendMessage("You feel weary.");
							}
						}
					}
				}
			}, 1);
		}
		if (Misc.random(10) == 0) {
			int size = Math.max(1, npc.dungeonManager.party.partyMembers.size());
			int maxSkeletons = 50 * size;
			if (list.size() < maxSkeletons) {
				int skeleSpawns = 2 * size;
				CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {

					int timer = 0;
					int[] offsets = new int[skeleSpawns];
					int rotation = npc.dungeonManager.getRoom(npc.reference).getRotation();

					@Override
					public void stop() {
						/* empty */
					}

					@Override
					public void execute(CycleEventContainer container) {
						timer++;

						if (timer == 2) {
							for (int i = 0; i < skeleSpawns; i++) {
								int offX = 2 + Misc.random(11);
								int offY = 2 + Misc.random(6);
								int spawnX = DungeonManager.translateX(offX, offY, rotation, 1, 1, 0);
								int spawnY = DungeonManager.translateY(offX, offY, rotation, 1, 1, 0);
								offsets[i] = Misc.toBitpack(offX, offY);
								Projectile proj = new Projectile(2591,
										Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()),
										Location.create(npc.dungeonManager.getRoomX(npc.reference) + spawnX, npc.dungeonManager.getRoomY(npc.reference) + spawnY));
								proj.setStartHeight(43);
								proj.setEndHeight(0);
								proj.setStartDelay(65);
								proj.setEndDelay(100);
								GlobalPackets.createProjectile(proj);
							}
						}

						if (timer == 5) {
							for (int i = 0; i < skeleSpawns; i++) {
								int x = Misc.getFirst(offsets[i]);
								int y = Misc.getSecond(offsets[i]);
								NPC skeleton = npc.dungeonManager.spawnNPC(npc.reference, 11722, x, y);
								skeleton.dropStuff = false;
								skeleton.dungVariables[SKELETAL_MINIONS] = npc;
								Server.getStillGraphicsManager().createGfx(x, y, 8, 1069, 43, 0);
								list.add(skeleton);
							}

							container.stop();
						}

					}
				}, 1);
			}
		}
	}
	
	@Override
	public void process(NPC npc) {
		if (npc.dungeonManager == null) {
			return;
		}

		List<NPC> skeletons = ((ArrayList<NPC>) npc.dungVariables[SKELETAL_MINIONS]);
		if (skeletons.isEmpty()) {
			return;
		}

		int size = 0;
		for (Player player : npc.dungeonManager.party.partyMembers) {
			if (npc.dungeonManager.getCurrentRoomReference(player.getCurrentLocation()).equals(npc.reference)) {
				resetTicks = 0;
				size++;
			}
		}

		if (size <= 0) {
			resetTicks++;
			if (resetTicks >= SKELETAL_MINIONS_DESPAWN_TICKS) {
				resetSkeletons(skeletons);
			}
		}
	}
	
	public static void resetSkeletons(List<NPC> npcs) {
		for (int i = 0, length = npcs.size(); i < length; i++) {
			npcs.get(i).setDead(true);
		}

		npcs.clear();
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.attackType = 2;
		npc.setWalksHome(false);
		npc.dungVariables[SKELETAL_MINIONS] = new ArrayList<NPC>();
	}

	@Override
	public boolean ignoreMeleeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int followDistance() {
		return 14;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 20;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (npc.dungeonManager == null) {
			return;
		}

		resetSkeletons(((ArrayList<NPC>) npc.dungVariables[SKELETAL_MINIONS]));
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
