package com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching;

import java.util.HashMap;
import java.util.Map;

public enum ArrowData {
	
	NOVITE(17885, 16427, 1, 1.3f),
	BATHUS(17890, 16432, 11, 2.5f),
	MARMAROS(17895, 16437, 22, 5f),
	KRATONITE(17900, 16442, 33, 7.5f),
	FRACTITE(17905, 16447, 44, 10f),
	ZEPHYRIUM(17910, 16452, 55, 12.5f),
	ARGONITE(17915, 16457, 66, 15f),
	KATAGON(17920, 16462, 77, 17.5f),
	GORGONITE(17925, 16467, 88, 20f),
	PROMETHIUM(17930, 16472, 99, 22.5f);

	public static Map<Integer, ArrowData> values = new HashMap<>();
	private final int tipsId, productId, level;
	private final float xp;

	private ArrowData(int tipsId, int productId, int level, float xp) {
		this.tipsId = tipsId;
		this.productId = productId;
		this.level = level;
		this.xp = xp;
	}

	public int getTipsId() {
		return tipsId;
	}

	public int getProductId() {
		return productId;
	}

	public int getLevel() {
		return level;
	}

	public float getXp() {
		return xp;
	}

	static {
		for (ArrowData data : values()) {
			values.put(data.getTipsId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }
}
