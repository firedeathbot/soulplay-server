package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.util.Misc;

public abstract class PuzzleRoom extends VisibleRoom {

	private boolean complete;
	private byte[] requirements = new byte[25];
	private byte[] giveXPCount = new byte[25];

	public final boolean hasRequirement(Client p, int skill) {
		return p.getSkills().getSkillBoostable(skill) >= getRequirement(skill);
	}

	public final int getRequirement(int skill) {
		setLevel(skill);
		return requirements[skill];
	}

	public final void giveXP(Client player, int skill) {
		if (giveXPCount[skill] < 4) {
			//You only gain xp for the first 4 times you do an action
			giveXPCount[skill]++;
			player.getPA().addSkillXP(getRequirement(skill) * 5 + 10, skill);//TODO Xp rates?
		}
	}

	private void setLevel(int skill) {
		if (requirements[skill] == 0) {
			int req;
			if (manager.getRoom(reference).isCritPath()) {
				req = Math.max(1, manager.party.getMaxLevel(skill) - Misc.randomNoPlus(10));
			} else {
				req = Misc.randomNoPlus(30, skill == Skills.SUMMONING || skill == Skills.PRAYER ? 100 : 106);
			}

			requirements[skill] = (byte) req;
		}
	}

	public void replaceObject(GameObject object, int newId) {
		if(object == null) {
			return;
		}

		manager.spawnObject(newId, object.getLocation().getX(), object.getLocation().getY(), object.getOrientation(), object.getType());
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(Client c) {
		this.complete = true;
		if (getCompleteMessage() != null) {
			manager.messageRoom(reference, getCompleteMessage());
		}
		
		c.getAchievement().complete100Puzzles();

		manager.getRoom(reference).removeChallengeDoors();
	}

	public String getCompleteMessage() {
		return "You hear a clunk as the doors unlock.";
	}

	public String getLockMessage() {
		return "The door is locked. You can't see any obvious keyhole or mechanism.";
	}

}
