package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Priority;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@BossMeta(ids = {9753, 9754, 9755, 9756, 9757, 9758, 9759, 9760, 9761, 9762, 9763, 9764, 9765, 9766})
public class Sagittare extends Boss {
	
	private static final int STAGE = 1;
	public static final int HP_FOR_NEXT_STAGE = 2;
	public static final int TELE_POS = 3;
	
	private static final int RANGE_PROJ = 2533;
	private static final int MAGIC_PROJ = 2535;
	private static final int RANGE_BIND_PROJ = 2540;
	private static final int MAGE_BIND_PROJ = 2537;
	private static final int BIND_GFX = 2541;
	
	public static final Graphic ARROW_RAIN_GFX = new Graphic(Priority.NORMAL, 2542, 0, GraphicType.HIGH);
	private static final Graphic START_RANGE_GFX = new Graphic(Priority.NORMAL, 2532, 5, GraphicType.HIGH);
	private static final Graphic START_MAGE_GFX = new Graphic(Priority.NORMAL, 2534, 0, GraphicType.HIGH);
	private static final Graphic START_RANGE_BIND_GFX = new Graphic(Priority.NORMAL, 2539, 0, GraphicType.HIGH);
	private static final Graphic START_MAGE_BIND_GFX = new Graphic(Priority.NORMAL, 2536, 0, GraphicType.HIGH);
	private static final Graphic START_TELE_GFX = Graphic.create(1576);
	private static final Graphic END_TELE_GFX = Graphic.create(1577);
	
	private static final Animation START_TELE_ANIM = new Animation(8939);
	private static final Animation END_TELE_ANIM = new Animation(8941);
	private static final Animation NORMAL_ATTACK_ANIM = new Animation(13271);
	public static final Animation ARROW_RAIN_ANIM = new Animation(13270);

	public Sagittare(NPC boss) {
		super(boss);
	}

	public static enum TeleportData {
		TELE_1(Location.create(3, 11), 9, 1),
		TELE_2(Location.create(14, 8), 0, 6),
		TELE_3(Location.create(6, 6), 3, 3),
		TELE_4(Location.create(6, 6), 3, 3);

		private static final TeleportData[] values = values();
		private final Location spawnPos;
		private final int randX, randY;

		private TeleportData(Location spawnPos, int randX, int randY) {
			this.spawnPos = spawnPos;
			this.randX = randX;
			this.randY = randY;
		}
		
		public Location getSpawnPos() {
			return spawnPos;
		}

		public int getRandX() {
			return Misc.random(randX);
		}

		public int getRandY() {
			return Misc.random(randY);
		}
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		
		npc.dungMultiAttack = false;
		
		int attack = Misc.random(5);
		switch (attack) {
		case 0://Normal range
		case 1://Normal magic
		case 2://Multi magic
		case 5://normal range
			if (attack == 5)
				attack = 0;
			npc.startAnimation(NORMAL_ATTACK_ANIM);
		    npc.startGraphic(attack == 0 ? START_RANGE_GFX : START_MAGE_GFX);
			npc.projectiletStartHeight = 65;
			npc.projectiletEndHeight = 60;
			npc.projectiletSlope = 3;
			npc.projectileDelay = 35;
			if (attack == 0 || attack == 1) {
				npc.attackType = attack == 0 ? 1 : 2;
				npc.projectileId = attack == 0 ? RANGE_PROJ : MAGIC_PROJ;
			} else {
				npc.attackType = 2;
				npc.dungMultiAttack = true;
				npc.projectileId = MAGIC_PROJ;
			}
			break;
		case 3://Bind attacks
		case 4:
			boolean isMagicAttack = attack == 3;
			npc.attackType = isMagicAttack ? 2 : 1;
			npc.startAnimation(NORMAL_ATTACK_ANIM);
			npc.startGraphic(isMagicAttack ? START_MAGE_BIND_GFX: START_RANGE_BIND_GFX);
			npc.dungMultiAttack = true;
			
			boolean bindTarget = false;
			
			npc.projectileId = isMagicAttack ? MAGE_BIND_PROJ : RANGE_BIND_PROJ;
			npc.projectiletStartHeight = 65;
			npc.projectiletEndHeight = 60;
			npc.projectiletSlope = 3;

			if (isMagicAttack) {
				if (!c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()])
					bindTarget = true;
			} else {
				bindTarget = Misc.random(1) == 0;
			}
			if (bindTarget) {
				if (!c.isRooted())
					c.rootEntity(8);
			}
			disableRun(c);
			npc.endGfx = BIND_GFX;
			npc.endGraphicType = GraphicType.LOW;
			break;
		}
	}
	
	private static void disableRun(Player player) {
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int timer = 0;
			
			@Override
			public void stop() {
				
			}
			
			@Override
 			public void execute(CycleEventContainer container) {
				if (player.isDead() || !player.isActive)
					container.stop();
				
				timer ++;				
				if (timer == 1) {
					player.blockRun = true;
				}
				
				if (timer >= 18) {
					container.stop();
					player.blockRun = false;
				}
			}
		}, 1);
		
	}
	
	private static void npcTeleport(NPC npc) {
		
		npc.resetAttack();
		npc.setAttackTimer(12);
		npc.getStopWatch().startTimer(12);
		npc.setAttackable(false);
		npc.setKillerId(0);
		npc.setRetargetTick(30);
		npc.forceChat("Back off!");
		
		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
			int timer = 0;
			int absX, absY; 
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (npc.isDead() || !npc.isActive)
					container.stop();
				
				timer++;
				
				if (timer == 2) {
					npc.forceChat("Arrow-rain!");
					npc.startAnimation(ARROW_RAIN_ANIM);
					npc.startGraphic(ARROW_RAIN_GFX);
					absX = npc.absX;
					absY = npc.absY;
				}
				if (timer == 6) {
					npc.startAnimation(START_TELE_ANIM);
					npc.startGraphic(START_TELE_GFX);
				}
				if (timer == 8) {
					for (int x = -1; x <= 1; x++) {
						int coordX = absX + x;
						for(int y = -1; y <= 1; y++) {
							int coordY = absY + y;
							Projectile proj = new Projectile(2533, Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()),
									Location.create(coordX, coordY, npc.getHeightLevel()));
							proj.setStartHeight(200);
							proj.setEndHeight(0);
							proj.setStartDelay(65);
							proj.setEndDelay(85);
							GlobalPackets.createProjectile(proj);
						}
					}
				}
				if (timer == 10) {
					int teamAvg = npc.dungeonManager.teamAvg;
					int minAvg = 85;
					int maxAvg = 3960;

					int rainDmg = Misc.interpolate(4, 45, minAvg, maxAvg, teamAvg);
					for (int x = -1; x <= 1; x++) {
						int coordX = absX + x;
						for(int y = -1; y <= 1; y++) {
							int coordY = absY + y;
							for (Player player : npc.dungeonManager.party.partyMembers) {
								if (player.absX == coordX && player.absY == coordY) {
									player.dealDamage(new Hit(rainDmg, 0, -1));
									player.rootEntity(8);
									player.sendMessage("You have been injured and can't move.");
									disableRun(player);
								}
							}
						}
					}
				}
				if (timer == 9) {
					npc.startAnimation(END_TELE_ANIM);
					TeleportData data = TeleportData.values[(int) npc.dungVariables[TELE_POS]];
					int rotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
					int x = data.getSpawnPos().getX() + data.getRandX();
					int y = data.getSpawnPos().getY() + data.getRandY();
					int offX = DungeonManager.translateX(x, y, rotation, 1, 1, 0);
					int offY = DungeonManager.translateY(x, y, rotation, 1, 1, 0);
					npc.moveNpc(npc.dungeonManager.getRoomX(npc.reference) + offX, npc.dungeonManager.getRoomY(npc.reference) + offY, npc.dungeonManager.height);
					npc.startGraphic(END_TELE_GFX);
					npc.dungVariables[TELE_POS] = (int) npc.dungVariables[TELE_POS] + 1;
				}
				if (timer >= 12) {
					container.stop();
					npc.setAttackable(true);
				}
			}
		}, 1);
	}

	@Override
	public void process(NPC npc) {
		if (npc.dungVariables[STAGE] == null || !npc.getStopWatch().complete()) {
			return;
		}

		switch((Integer) npc.dungVariables[STAGE]) {
			case 0:// set hp
				npc.dungVariables[STAGE] = 1;
				npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints() * 0.75));
				break;
			case 2:// teleport stage
				npc.dungVariables[STAGE] = 3;
				npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints() * 0.5));
				npc.setAttackable(false);
				npcTeleport(npc);
			break;
			case 4:// teleport stage
				npc.dungVariables[STAGE] = 5;
				npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints() * 0.25));
				npc.setAttackable(false);
				npcTeleport(npc);
			break;
			case 6:// teleport stage
				npc.dungVariables[STAGE] = 7;
				npc.dungVariables[HP_FOR_NEXT_STAGE] = 1;
				npc.setAttackable(false);
				npcTeleport(npc);
			break;
			case 8:// teleport stage
				npc.dungVariables[STAGE] = 9;
				npc.dungVariables[HP_FOR_NEXT_STAGE] = 0;
				npc.setAttackable(false);
				npcTeleport(npc);
			break;
		}
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return Prayers.PROTECT_FROM_MISSILES;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.dungVariables[STAGE] = new Integer(0);
		npc.dungVariables[HP_FOR_NEXT_STAGE] = new Integer(0);
		npc.dungVariables[TELE_POS] = new Integer(0);
		npc.resetAttackType = false;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int followDistance() {
		// TODO Auto-generated method stub
		return 20;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		// TODO Auto-generated method stub
		return 24;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if ((Integer)npc.dungVariables[STAGE] < 9) {
			int hp = npc.getSkills().getLifepoints();
			int stageDmg = hp - (Integer)npc.dungVariables[HP_FOR_NEXT_STAGE];

			if (hit.getDamage() >= stageDmg) {
				hit.setDamage(stageDmg);
			}

			final int lifepoints = npc.getSkills().getLifepoints() - hit.getDamage();
			if (lifepoints <= (Integer)npc.dungVariables[HP_FOR_NEXT_STAGE]) {
				npc.dungVariables[STAGE] = (Integer)npc.dungVariables[STAGE] + 1;
			}
		}
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		for (Player player : npc.dungeonManager.party.partyMembers) {
			player.blockRun = false;
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
}

