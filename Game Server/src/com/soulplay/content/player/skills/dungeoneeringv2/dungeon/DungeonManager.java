package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.cache.ObjectDef;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.player.skills.dungeoneeringv2.ExpAndTokens;
import com.soulplay.content.player.skills.dungeoneeringv2.ExpModifier;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.KeyDoors;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.MapRoomIcon;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.SkillDoors;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonBossesStats;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeonNpcs;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fishing.DungeoneeringFishData;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.mining.Rocks;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerAssistant;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.DynamicObjectManager;
import com.soulplay.game.world.map.ObjectDefVersion;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

import static com.soulplay.util.Misc.*;

public class DungeonManager {

	public static final int PROGRESS_INTERFACE_ID = 50100;
	public static final int NAME_BASE_ID = 50040;

	public static long uid;
	public static final Map<Long, DungeonManager> instances = new HashMap<>();
	public List<GroundItem> items = new ArrayList<>();
	public List<NPC> npcs = new ArrayList<>();
	public Dungeon dungeon;
	public VisibleRoom[][] visibleMap;
	public Party party;
	public int startX;
	public int startY;
	public DynamicRegionClip clipping = new DynamicRegionClip();
	public DynamicObjectManager objectManager = new DynamicObjectManager(clipping);
	private final Map<Long, GameObject> defaultObjects = new HashMap<>();
	public boolean loaded;
	public long instanceId;
	public int startTick;
	public Location groupGatestone;
	public int teamAvg;
	private List<int[][]> npcsToSpawn = new ArrayList<>();
	public boolean canEnd;
	public int farmEventsUid;
	public Map<Integer, CycleEventContainer> farmingEvents = new HashMap<>();
	public CycleEventContainer endTimer;
	public int endTimerTicks;
	public List<Region> regions = new ArrayList<>();
	public int endScreenStates;
	public int totalGuardiansSpawned;
	public int totalGuardiansKilled;
	public int height;
	public long createTime;
	public CycleEventContainer cleanupEvent;

	public DungeonManager(Party party) {
		this.party = party;
		this.instanceId = uid++;
		objectManager.setDefsVersion(ObjectDefVersion.DEF_667);
		height = (int) (instanceId * 4);
	}

	public void start() {
		int size = party.partyMembers.size();
		if (size <= 0) {
			destroy(true);
			return;
		}

		createTime = System.currentTimeMillis();
		instances.put(createTime, this);
		startX = 1000;
		startY = 1000;
		clipping.cleanup();
		defaultObjects.clear();
		//party.lockParty();
		visibleMap = new VisibleRoom[DungeonConstants.DUNGEON_RATIO[party.size][0]][DungeonConstants.DUNGEON_RATIO[party.size][1]];
		//clearKeyList();
		// generates dungeon structure
		dungeon = new Dungeon(this, party.floor, party.size, party.getComplexity());
		startTick = Server.getTotalTicks();
		// finds an empty map area bounds
		//boundChuncks = MapBuilder.findEmptyChunkBound(dungeon.getMapWidth() * 2, (dungeon.getMapHeight() * 2));
		// reserves all map area
		//MapBuilder.cutMap(boundChuncks[0], boundChuncks[1], dungeon.getMapWidth() * 2, (dungeon.getMapHeight() * 2), 0);
		// set dungeon
		//setDungeon();
		// loads start room
		loadRoom(dungeon.getStartRoomReference());

		for (int i = 0; i < size; i++) {
			Client player = party.partyMembers.get(i);
			player.dungDeaths = 0;
			player.dungVoteStage = 0;
			player.resetGatestone();
			initStuffOnStart(player);
			player.setStunned(false);
			player.getPacketSender().clearRegion();
			player.dungPrayerRecharge = player.getSkills().getStaticLevel(Skills.PRAYER) * 10 * (party.size + 1);
			player.getPA().restorePlayer(true);
			player.getCombat().resetPrayers();
			player.dungInstanceId = createTime;
		}

		endScreenStates = 0;
		teamAvg = Party.calcPartyAvg(party);

		npcsToSpawn.clear();
		List<int[][]> npcs = DungeonNpcs.pickNpcs(party.getFloorType());
		for (int i = 0, length = npcs.size(); i < length; i++) {
			int[][] data = npcs.get(i);
			if (data[0][1] <= teamAvg) {
				npcsToSpawn.add(data);
			}
		}
		//npcsToSpawn.add(DungeonNpcs.REBORN_MAGES);

		loaded = true;
		canEnd = false;
	}

	public void initStuffOnStart(Client player) {
		player.setDynamicRegionClip(clipping);
		player.getPacketSender().sendConfig(8223, 1);
		player.previousMagicBook = player.playerMagicBook;
		player.setSpellbook(SpellBook.DUNGEONEERING);
	}

	public void loadRoom(int x, int y) {
		loadRoom(new RoomReference(x, y));
	}

	public void loadRoom(final RoomReference reference) {
		final Room room = dungeon.getRoom(reference);
		if (room == null) {
			//System.out.println("room is null while loading");
			return;
		}

		VisibleRoom vr;
		if (room.getRoom() instanceof HandledPuzzleRoom) {
			HandledPuzzleRoom pr = (HandledPuzzleRoom) room.getRoom();
			vr = pr.createVisibleRoom();
			if (vr == null) {
				//System.out.println("visible room is null on handled puzzle, making temp one for now lol");
				vr = new VisibleRoom();
			}
		} else {
			vr = new VisibleRoom();
		}
		
		visibleMap[reference.getX()][reference.getY()] = vr;
		if (vr != null) {
			vr.init(this, reference, party.getFloorType(), room.getRoom());
			//Was wrapped with an executor
			openRoom(room, reference, vr);
		}
	}

	public RoomReference getCurrentRoomReference(Location tile) {
		return new RoomReference((tile.getMapZoneX() - (startX >> 3)) >> 1, (tile.getMapZoneY() - (startY >> 3)) >> 1);
	}

	public boolean enterRoom(Player player, int x, int y) {
		if (x < 0 || y < 0 || x >= visibleMap.length || y >= visibleMap[0].length) {
			//System.out.println("invalid room: " + x + ":"+ y + ":" + visibleMap.length + ":" + visibleMap[0].length);
			return false;
		}

		RoomReference roomReference = getCurrentRoomReference(player.getCurrentLocation());
		//player.lock(0);
		if (visibleMap[x][y] != null) {
			if (!visibleMap[x][y].isLoaded()) {
				return false;
			}

			int xOffset = x - roomReference.getX();
			int yOffset = y - roomReference.getY();
			player.getPA().movePlayer(player.getX() + xOffset * 3, player.getY() + yOffset * 3, height);
			//playMusic(player, new RoomReference(x, y));
			return true;
		}

		loadRoom(x, y);
		return false;
	}

	public void openRoom(final Room room, final RoomReference reference, final VisibleRoom visibleRoom) {
		final int toChunkX = (startX >> 3) + (reference.getX() << 1);
		final int toChunkY = (startY >> 3) + (reference.getY() << 1);
		MapTools.copy2RatioSquare(room.getChunkX(party.getComplexity()), room.getChunkY(party.getFloorType()), toChunkX, toChunkY, room.getRotation(), defaultObjects, clipping, height, this, 0, 1);

		CycleEventHandler.getSingleton().addEvent(room, new CycleEvent() {

			@Override
			public void execute(
					CycleEventContainer container) {
				if (isDestroyed()) {
					container.stop();
					return;
				}

				room.openRoom(DungeonManager.this, reference);
				visibleRoom.openRoom();
				for (int i = 0, length = room.getRoom().getDoorDirections().length; i < length; i++) {
					Door door = room.getDoor(i);
					if (door == null)
						continue;
					int rotation = (room.getRoom().getDoorDirections()[i] + room.getRotation()) & 0x3;
					if (door.getType() == DungeonConstants.KEY_DOOR) {
						KeyDoors keyDoor = KeyDoors.values[door.getId()];
						setDoor(reference, keyDoor.getObjectId(), keyDoor.getDoorId(party.getFloorType()), rotation);
					} else if (door.getType() == DungeonConstants.GUARDIAN_DOOR) {
						setDoor(reference, -1, DungeonConstants.DUNGEON_GUARDIAN_DOORS[party.getFloorType()], rotation);
					} else if (door.getType() == DungeonConstants.SKILL_DOOR) {
						SkillDoors skillDoor = SkillDoors.values[door.getId()];
						int type = party.getFloorType();
						int closedId = skillDoor.getClosedObject(type);
						int openId = skillDoor.getOpenObject(type);
						setDoor(reference, openId == -1 ? closedId : -1, openId != -1 ? closedId : -1, rotation);
					}
				}

				if (room.getRoom().allowResources()) {
					setResources(room, reference, toChunkX, toChunkY);
				}

				if (room.getDropId() != -1) {
					setKey(room, reference, toChunkX, toChunkY);
				}

				visibleRoom.setLoaded();

				for (Player player : party.partyMembers) {
					player.getPacketSender().sendConstructMapRegionPacket(height);
					player.updateRegion = true;
				}

				container.stop();
			}

			@Override
			public void stop() {
			}
		}, 1);
	}

	public GameObject getObject(RoomReference reference, int id, int x, int y) {
		final int mapRotation = dungeon.getRoom(reference).getRotation();
		ObjectDef defs = ObjectDef.forID667(id);
		int realX = getRoomX(reference) + translateX(x, y, mapRotation, defs.sizeX, defs.sizeY, 0);
		int realY = getRoomY(reference) + translateY(x, y, mapRotation, defs.sizeX, defs.sizeY, 0);
		
		if (objectManager == null) {
			return null;
		}

		GameObject o = objectManager.getObject(realX, realY, height);
		
		if (o != null && o.isActive()) {
			return o;
		}

		return null;
	}
	
	public GameObject getSpawnedObject(int x, int y, boolean remove) {
		if (objectManager == null) {
			return null;
		}
		
		GameObject o = objectManager.getObject(x, y, height);
		
		if (o != null && o.isActive()) {
			if (remove)
				objectManager.revertObject(o);
			return o;
		}

		return null;
	}

	public void setDoor(RoomReference reference, int lockObjectId, int doorObjectId, int rotation) {
		if (lockObjectId != -1) {
			spawnObject(lockObjectId, getRoomX(reference) + translateX(1, 7, rotation, 1, 2, 0), getRoomY(reference) + translateY(1, 7, rotation, 1, 2, 0), rotation, 10);
		}
		if (doorObjectId != -1) {
			spawnObject(doorObjectId, getRoomX(reference) + translateX(0, 7, rotation, 1, 2, 0), getRoomY(reference) + translateY(0, 7, rotation, 1, 2, 0), rotation, 10);
		}
	}

	public GameObject spawnObject(RoomReference reference, int id, int type, int rotation, int x, int y) {
		final int mapRotation = dungeon.getRoom(reference).getRotation();
		ObjectDef defs = ObjectDef.forID667(id);
		return spawnObject(id, getRoomX(reference) + translateX(x, y, mapRotation, defs.sizeX, defs.sizeY, rotation), getRoomY(reference) + translateY(x, y, mapRotation, defs.sizeX, defs.sizeY, rotation), (rotation + mapRotation) & 0x3, type);
	}

	public GameObject spawnObject(int id, int x, int y, int rotation, int type) {
		return spawnObject(id, x, y, rotation, type, Integer.MAX_VALUE);
	}

	public GameObject spawnObject(int id, int x, int y, int rotation, int type, int ticks) {
		if (objectManager == null) {
			return null;
		}

		GameObject object = GameObject.createTickObject(id, Location.create(x, y, height), rotation, type, ticks, -1);
		objectManager.addObject(object);
		return object;
	}

	public GameObject getRealObject(int x, int y) {
		if (objectManager == null) {
			return null;
		}
		
		GameObject o = objectManager.getObject(x, y, height);
		if (o != null && o.isActive())
			return o;
		for (int i = 0; i < 4; i++) {
			o = defaultObjects.get(GameObject.generateKey(x, y, height, i));
			if (o != null)
				return o;
		}
		return null;
	}
	
	public GameObject getRealObject(int id, int x, int y, int z) {
		if (objectManager == null) {
			return null;
		}
		
		GameObject o = objectManager.getObject(id, x, y, z);
		if (o != null && o.isActive())
			return o;
		for (int i = 0; i < 4; i++) {
			o = defaultObjects.get(GameObject.generateKey(x, y, z, i));
			if (o != null && o.getId() == id)
				return o;
		}
		return null;
	}

	public void setResources(Room room, RoomReference reference, int toChunkX, int toChunkY) {
		toChunkX = toChunkX << 3;
		toChunkY = toChunkY << 3;
		if (party.getComplexity() >= 5 && randomNoPlus(3) == 0) {//sets thieving chest
			for (int i = 0; i < DungeonConstants.SET_RESOURCES_MAX_TRY; i++) {
				int rotation = randomNoPlus(DungeonConstants.WALL_BASE_X_Y.length);
				int[] b = DungeonConstants.WALL_BASE_X_Y[rotation];
				int x = b[0] + randomNoPlus(b[2]);
				int y = b[1] + randomNoPlus(b[3]);
				if (((x >= 6 && x <= 8) && b[2] != 0) || ((y >= 6 && y <= 8) && b[3] != 0)) {
					continue;
				}

				x = toChunkX + x;
				y = toChunkY + y;
				rotation = (rotation + 3) & 0x3;
				if (!RegionClip.isFloorFree(clipping.getClipping(x, y, 0)) || !isTileFree(0, x - ROTATION_DIR_X[rotation], y - ROTATION_DIR_Y[rotation], 0, clipping)) {
					continue;
				}

				int level = party.getMaxLevel(Skills.THIEVING);
				room.setThiefChest(random(DungeonUtils.getTierForLevel(level)));
				spawnObject(DungeonConstants.THIEF_CHEST_LOCKED[party.getFloorType()], x, y, rotation, 10);
				break;
			}
		}
		if (party.getComplexity() >= 4 && randomNoPlus(3) == 0) { //sets flower
			for (int i = 0; i < DungeonConstants.SET_RESOURCES_MAX_TRY; i++) {
				int rotation = randomNoPlus(DungeonConstants.WALL_BASE_X_Y.length);
				int[] b = DungeonConstants.WALL_BASE_X_Y[rotation];
				int x = b[0] + randomNoPlus(b[2]);
				int y = b[1] + randomNoPlus(b[3]);
				if (((x >= 6 && x <= 8) && b[2] != 0) || ((y >= 6 && y <= 8) && b[3] != 0)) {
					continue;
				}

				x = toChunkX + x;
				y = toChunkY + y;
				if (!RegionClip.isFloorFree(clipping.getClipping(x, y, 0))) {
					continue;
				}

				int tier = DungeonUtils.getTierForLevel(party.getMaxLevel(Skills.FARMING));
				int roll = Misc.random(Math.max(0, tier - 1), tier);
				int id = DungeonUtils.getFarmingResource(roll, party.getFloorType());
				spawnObject(id, x, y, rotation, 10);
				break;
			}
		}
		if (party.getComplexity() >= 3 && randomNoPlus(3) == 0) { //sets rock
			for (int i = 0; i < DungeonConstants.SET_RESOURCES_MAX_TRY; i++) {
				int rotation = randomNoPlus(DungeonConstants.WALL_BASE_X_Y.length);
				int[] b = DungeonConstants.WALL_BASE_X_Y[rotation];
				int x = b[0] + randomNoPlus(b[2]);
				int y = b[1] + randomNoPlus(b[3]);
				if (((x >= 6 && x <= 8) && b[2] != 0) || ((y >= 6 && y <= 8) && b[3] != 0)) {
					continue;
				}

				x = toChunkX + x;
				y = toChunkY + y;
				if (!RegionClip.isFloorFree(clipping.getClipping(x, y, 0))) {
					continue;
				}

				int rock = Rocks.pickRock(party.getMaxLevel(Skills.MINING));
				int roll = random(Math.max(0, rock - 1), rock);
				spawnObject(DungeonUtils.getMiningResource(roll, party.getFloorType()), x, y, rotation, 10);
				break;
			}
		}
		if (party.getComplexity() >= 2) { //sets tree
			List<GameObject> treeSpots = new ArrayList<>();
			int absX = toChunkX;
			int absY = toChunkY;
			int lookForId = DungeonConstants.WOODCUTTING_BASE[party.getFloorType()];
			for (int x = 0; x < 16; x++) {
				int realX = absX + x;
				for (int y = 0; y < 16; y++) {
					GameObject o = getRealObject(realX, absY + y);
					if (o == null || o.getId() != lookForId) {
						continue;
					}

					treeSpots.add(o);
				}
			}

			if (treeSpots.isEmpty()) {
				return;
			}

			int tier = DungeonUtils.getTierForLevel(party.getMaxLevel(Skills.WOODCUTTING));
			int lowerTier = Math.max(0, tier - 1);
			int spawns = random(0, treeSpots.size());
			for (int i = 0; i < spawns; i++) {
				GameObject spot = treeSpots.get(i);
				int roll = random(lowerTier, tier);
				spawnObject(DungeonUtils.getWoodcuttingResource(roll, party.getFloorType()), spot.getLocation().getX(), spot.getLocation().getY(), spot.getOrientation(), 10);
			}
		}
		if (party.getComplexity() >= 2) { //sets fish spot
			List<Integer> fishSpots = new ArrayList<>();
			int absX = toChunkX;
			int absY = toChunkY;
			for (int x = 0; x < 16; x++) {
				int realX = absX + x;
				for (int y = 0; y < 16; y++) {
					int realY = absY + y;
					GameObject o = getRealObject(realX, realY);
					if (o == null || o.getId() != DungeonConstants.FISH_SPOT_OBJECT_ID) {
						continue;
					}

					fishSpots.add(toBitpack(realX, realY));
				}
			}

			if (fishSpots.isEmpty()) {
				return;
			}

			int spot = fishSpots.get(randomNoPlus(fishSpots.size()));
			spawnNPC(DungeonConstants.FISH_SPOT_NPC_ID, Location.create(getFirst(spot), getSecond(spot), height), reference, DungeonConstants.FISH_SPOT_NPC, false);
		}
	}

	public void setKey(Room room, RoomReference reference, int toChunkX, int toChunkY) {
		int[] loc = room.getRoom().getKeySpot();
		if (loc != null) {
			spawnItem(reference, room.getDropId(), 1, loc[0], loc[1]);
			return;
		}

		spawnItem(reference, room.getDropId(), 1, 7, 1);
	}
	
	public int getRoomX(RoomReference reference) {
		return startX + (reference.getX() << 4);
	}
	
	public int getRoomY(RoomReference reference) {
		return startY + (reference.getY() << 4);
	}

	public void spawnBelow(Client c, Deque<Item> items) {
		for (Item item : items) {
			spawnBelow(c, item);
		}
	}

	public void spawnHome(Deque<Item> items) {
		Location location = getRoomCenterTile(dungeon.getStartRoomReference());
		for (Item item : items) {
			spawnItem(item.getId(), item.getAmount(), location.getX(), location.getY(), true, "server");
		}
	}

	public void spawnBelow(Player c, Item item) {
		boolean visibleToAll = true;
		if (item.getId() == DungeonConstants.GROUP_GATESTONE) {
			groupGatestone = c.getCurrentLocation().copyNew();
		} else if (item.getId() == DungeonConstants.GATESTONE) {
			c.setGatestone(c.getCurrentLocation().copyNew());
			visibleToAll = false;
		}

		spawnItem(item.getId(), item.getAmount(), c.absX, c.absY, visibleToAll, c.getName());
	}

	public void spawnBelow(NPC npc, int id, int amount) {
		spawnItem(id, amount, npc.absX, npc.absY, true, "server");
	}

	public void spawnItem(RoomReference reference, int item, int count, int x, int y) {
		final int mapRotation = dungeon.getRoom(reference).getRotation();
		spawnItem(item, count, getRoomX(reference) + translateX(x, y, mapRotation, 1, 1, 0), getRoomY(reference) + translateY(x, y, mapRotation, 1, 1, 0), true, "server");
	}

	public void spawnItem(int item, int count, int x, int y, boolean visibleToAll, String owner) {
		if (items == null) {
			destroy(true);
			return;
		}

		GroundItem groundItem = new GroundItem(item, x, y, height, count, 0);
		groundItem.ownerName = owner;
		ItemHandler.createGroundItem(groundItem, visibleToAll);
		groundItem.setInMinigame(true);//ironman can pickup if visibleToAll is false
		items.add(groundItem);
	}

	public void sayBye(Client player, boolean move, boolean proper) {
		Deque<Item> toDrop = new ArrayDeque<>();
    	for (int i = 0, length = player.playerItems.length; i < length; i++) {
    		int itemId = player.playerItems[i] - 1;
    		if (itemId < 0) {
    			continue;
    		}

    		if (player.getBinds().getAmmunitionId() != -1 && !move && player.getBinds().getAmmunitionId() == itemId) {
    			continue;
    		}

    		if (itemId == DungeonConstants.GROUP_GATESTONE || ItemDefinition.forId(itemId) != null && ItemDefinition.forId(itemId).getShopValue() > 0 || KeyDoors.KEYS_SET.contains(itemId)) {//TODO might need more ids
    			toDrop.push(new Item(itemId, player.playerItemsN[i]));
    		}

    		player.getItems().deleteItemInSlot(i);
    	}

    	for (int i = 0, length = player.playerEquipment.length; i < length; i++) {
    		int itemId = player.playerEquipment[i];
    		if (itemId < 0) {
    			continue;
    		}

    		if (player.getBinds().getAmmunitionId() != -1 && !move && player.getBinds().getAmmunitionId() == itemId) {
    			continue;
    		}

    		ItemDefinition itemDef = ItemDefinition.forId(itemId);
    		if (itemDef != null && itemDef.getShopValue() > 0) {
    			toDrop.push(new Item(itemId, player.playerEquipmentN[i]));
    		}

    		player.getItems().deleteEquipment(itemId, i);
    	}

    	if (proper) {
    		spawnBelow(player, toDrop);
    	} else {
    		spawnHome(toDrop);
    	}

    	player.getCombat().getPlayerAnimIndex(player.playerEquipment[PlayerConstants.playerWeapon]);
		player.getItems().addItem(DungeonConstants.RING_OF_KINSHIP, 1);
		if (move) {
			player.getPA().movePlayer(DungeonConstants.OUTSIDE);
			player.dungInstanceId = 0;
			player.setDynamicRegionClip(null);
			player.dungTeleported = false;
			player.getPA().walkableInterface(-1);
		}

		resetDungVars(player);
	}

	private void resetDungVars(Player player) {
		player.resetSkillingEvent();
		player.setStunned(false);
		player.getPacketSender().setBlackout(0);
		player.setSpellbook(player.previousMagicBook);
	}

	public void destroy(boolean hard) {
		for (Client player : party.partyMembers) {
			player.setDynamicRegionClip(null);
			player.getPacketSender().sendConfig(8223, 0);
			player.dungInstanceId = 0;
			player.dungTeleported = false;
			player.getPA().walkableInterface(-1);
			resetDungVars(player);
		}

		for (int x = 0; x < visibleMap.length; x++) {
			for (int y = 0; y < visibleMap[x].length; y++) {
				if (visibleMap[x][y] != null) {
					visibleMap[x][y].destroy();
				}
			}
		}

		if (items != null) {
			for (int i = items.size() - 1; i >= 0; i--) {
				items.get(i).setRemoveTicks(2);
			}
	
			items.clear();
		}

		if (npcs != null) {
			for (int i = npcs.size() - 1; i >= 0; i--) {
				npcs.get(i).nullNPC();
			}
	
			npcs.clear();
		}
		
		if (objectManager != null && objectManager.getSpawnedObjects() != null) {
			objectManager.getSpawnedObjects().clear();
		}
		
		if (objectManager != null && objectManager.getStaticObjects() != null) {
			objectManager.getStaticObjects().clear();
		}

		if (farmingEvents != null) {
			for (CycleEventContainer event : farmingEvents.values()) {
				event.stop();
			}

			farmingEvents.clear();
		}

		if (regions != null) {
			for (int i = regions.size() - 1; i >= 0; i--) {
				Region region = regions.get(i);
				region.removeDynamicData();
			}

			regions.clear();
		}

		if (hard) {
			party.dungManager = null;
			npcs = null;
			items = null;
			farmingEvents = null;
			regions = null;
			if (objectManager != null) {
				objectManager.cleanup();
			}
			objectManager = null;
		}

		endCleanup();

		if (endTimer != null) {
			endTimer.stop();
			endTimer = null;
		}

		instances.remove(createTime);
	}

	public void setTableItems(RoomReference room) {
		addItemToTable(room, new Item(16295)); //novice pickaxe, because of boss as well so 1+
		if (party.getComplexity() >= 2) {
			addItemToTable(room, new Item(DungeonConstants.RUSTY_COINS, 5000 + random(10000)));
			addItemToTable(room, new Item(17678)); //tinderbox
			addItemToTable(room, new Item(16361)); //novice hatchet
			addItemToTable(room, new Item(17794)); //fish rods
		}

		if (party.getComplexity() >= 3) { //set weapon and gear on the table
			int rangeTier = DungeonUtils.getTier(party.getMaxLevel(Skills.RANGED));
			if (rangeTier > 8)
				rangeTier = 8;
			addItemToTable(room, new Item(DungeonUtils.getArrows(1 + randomNoPlus(rangeTier)), 90 + random(30))); //around 100 arrows, type random up to best u can, limited to tier 8
			addItemToTable(room, new Item(DungeonConstants.RUNES[0], 90 + random(30))); //around 100 air runes
			addItemToTable(room, new Item(DungeonConstants.RUNE_ESSENCE, 90 + random(30))); //around 100 essence
			addItemToTable(room, new Item(17754)); //knife
			addItemToTable(room, new Item(17883)); //hammer
			if (party.getComplexity() >= 4)
				addItemToTable(room, new Item(17446)); //needle
		}

		for (int j = 0, length = party.partyMembers.size(); j < length; j++) {
			for (int i = 0, foodLength = 7 + randomNoPlus(4); i < foodLength; i++) {
				addItemToTable(room, new Item(DungeonUtils.getFood(1 + randomNoPlus(8))));
			}

			if (party.getComplexity() >= 3) { //set weap/gear in table
				for (int i = 0, gearLength = 1 + randomNoPlus(3); i < gearLength; i++)
					//1 up to 3 pieces of gear or weap
					addItemToTable(room, new Item(DungeonUtils.getRandomGear(1 + randomNoPlus(8))));
			}
		}
	}

	public void telePartyToRoom(RoomReference reference) {
		Location tile = getRoomCenterTile(reference);
		for (Player player : party.partyMembers) {
			player.getPA().movePlayer(tile);
		}
	}

	public void linkPartyToDungeon() {
		int length = party.partyMembers.size();
		if (length <= 0) {
			return;
		}

		//Was wrapped with a task
		if (party.getComplexity() >= 5) {
			party.partyMembers.get(0).getItems().addItem(DungeonConstants.GROUP_GATESTONE, 1);
		}

		for (int i = 0; i < length; i++) {
			Client player = party.partyMembers.get(i);
			player.dungTeleported = true;
			player.getPacketSender().setBlackout(0);
			player.getPA().closeAllWindows();
			player.getBinds().load(player.getBinds().selectedLoudout, true);
			player.getBinds().loadAmmunition();
			sendStartItems(player);
			player.sendMessage("");
			player.sendMessage("- Welcome to Daemonheim -");
			player.sendMessage("Floor <col=641d9e>" + party.floor
					+ "    <col=ffffff>Complexity <col=641d9e>" + party.getComplexity() + (party.getComplexity() == 6 ? " (Full)" : ""));
			player.sendMessage("Dungeon Size: " + "<col=641d9e>" + DungeonConstants.SIZES_NAME[party.size]);
			player.sendMessage(
					"Party Size: Difficulty <col=641d9e>" + party.partyMembers.size() + ":" + party.difficulty);

			if (party.isGuideMode()) {
				player.sendMessage("<col=641d9e>Guide Mode ON");
			}
			player.sendMessage("");
			//player.lock(2);
		}

		groupGatestone = null;
	}

	public void ready(Client c) {
		int partySize = party.partyMembers.size();
		endTimerTicks -= 75 / partySize;
	}

	public void startNextTimer() {
		if (endTimer == null) {
			endTimerTicks = 75;
			endTimer = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					endTimerTicks--;

					if ((endTimerTicks % 10 == 0 || (endTimerTicks < 10 && endTimerTicks % 2 == 0)) && endTimerTicks > 0) {
						int seconds = Misc.ticksToSeconds(endTimerTicks);
						for (int i = 0, partySize = party.partyMembers.size(); i < partySize; i++) {
							Player player = party.partyMembers.get(i);
							player.sendMessage(seconds + " seconds until dungeon ends.");
						}
					}

					if (endTimerTicks <= 0) {
						container.stop();
						advanceFloor();
					}
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 1);
		//} else {
		//	System.out.println("not null");
		}
	}

	public void voteToMoveOn() {
		if (endTimer == null) {
			endTimerTicks = party.partyMembers.size() * 100;
			endTimer = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					endTimerTicks--;
					if ((endTimerTicks % 10 == 0 || (endTimerTicks < 10 && endTimerTicks % 2 == 0)) && endTimerTicks > 0) {
						for (int i = 0, partySize = party.partyMembers.size(); i < partySize; i++) {
							Player player = party.partyMembers.get(i);
							player.sendMessage(Misc.ticksToSeconds(endTimerTicks) + " seconds until dungeon ends.");
						}
					}

					if (endTimerTicks <= 0) {
						container.stop();
						end();
					}
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 1);
		}

		endTimerTicks -= 100;
	}

	public void endCleanup() {
		if (cleanupEvent == null) {
			return;
		}

		cleanupEvent.stop();
		cleanupEvent = null;
	}

	public void startCleanupEvent() {
		if (cleanupEvent != null) {
			return;
		}

		//System.out.println("start cleanup event");
		cleanupEvent = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				destroy(true);
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 100);
	}

	public void sendStartItems(Player player) {
		if (party.getComplexity() == 1) {
			player.getDialogueBuilder().sendStatement("<col=0000FF>Complexity 1", "Combat only", "Armour and weapons allocated", "No shop stock").execute();
		} else if (party.getComplexity() == 2) {
			player.getDialogueBuilder().sendStatement("<col=0000FF>Complexity 2", "+ Fishing, Woodcutting, Firemaking, Cooking", "Armour and weapons allocated", "Minimal shop stock").execute();
		} else if (party.getComplexity() == 3) {
			player.getDialogueBuilder().sendStatement("<col=0000FF>Complexity 3", "+ Mining, Smithing weapons, Fletching, Runecrafting", "Armour allocated", "Increased shop stock").execute();
		} else if (party.getComplexity() == 4) {
			player.getDialogueBuilder().sendStatement("<col=0000FF>Complexity 4", "+ Smithing armour, Hunter, Farming textiles, Crafting", "Increased shop stock").execute();
		} else if (party.getComplexity() == 5) {
			player.getDialogueBuilder().sendStatement("<col=0000FF>Complexity 5", "All skills included", "+ Farming seeds, Herblore, Thieving, Summoning", "Complete shop stock", "Challenge rooms + skill doors").execute();
		}

		if (party.getComplexity() <= 3) {
			int defenceTier = Math.min(8, DungeonUtils.getTier(player.getSkills().getStaticLevel(Skills.DEFENSE)));
			player.getItems().addItem(DungeonUtils.getPlatebody(defenceTier), 1);
			player.getItems().addItem(DungeonUtils.getPlatelegs(defenceTier, player.playerAppearance[0] == 0), 1);

			int magicTier = Math.min(8, DungeonUtils.getTier(player.getSkills().getStaticLevel(Skills.MAGIC)));
			player.getItems().addItem(DungeonUtils.getRobeTop(defenceTier < magicTier ? defenceTier : magicTier), 1);
			player.getItems().addItem(DungeonUtils.getRobeBottom(defenceTier < magicTier ? defenceTier : magicTier), 1);

			int rangeTier = Math.min(8, DungeonUtils.getTier(player.getSkills().getStaticLevel(Skills.RANGED)));
			player.getItems().addItem(DungeonUtils.getLeatherBody(defenceTier < rangeTier ? defenceTier : rangeTier), 1);
			player.getItems().addItem(DungeonUtils.getChaps(defenceTier < rangeTier ? defenceTier : rangeTier), 1);

			if (party.getComplexity() <= 2) {
				int attackTier = Math.min(8, DungeonUtils.getTier(player.getSkills().getStaticLevel(Skills.ATTACK)));
				player.getItems().addItem(DungeonUtils.getRapier(attackTier), 1);
				player.getItems().addItem(DungeonUtils.getBattleaxe(attackTier), 1);

				player.getItems().addItem(DungeonConstants.RUNES[0], 90 + randomNoPlus(30));
				player.getItems().addItem(DungeonUtils.getStartRunes(player.getSkills().getStaticLevel(Skills.MAGIC)), 90 + randomNoPlus(30));
				player.getItems().addItem(DungeonUtils.getElementalStaff(magicTier), 1);

				player.getItems().addItem(DungeonUtils.getShortbow(rangeTier), 1);
				player.getItems().addItem(DungeonUtils.getArrows(rangeTier), 90 + randomNoPlus(30));
			}
		}
	}

	public void addItemToTable(RoomReference room, Item item) {
		int slot = randomNoPlus(10); //10 spaces for items
		if (slot < 6) {
			spawnItem(room, item.getId(), item.getAmount(), 9 + randomNoPlus(3), 10 + randomNoPlus(2));
		} else if (slot < 8) {
			spawnItem(room, item.getId(), item.getAmount(), 10 + randomNoPlus(2), 14);
		} else {
			spawnItem(room, item.getId(), item.getAmount(), 14, 10 + randomNoPlus(2));
		}
	}

	public static int translateX(int x, int y, int mapRotation, int sizeX, int sizeY, int objectRotation) {
		if ((objectRotation & 0x1) == 1) {
			int prevSizeX = sizeX;
			sizeX = sizeY;
			sizeY = prevSizeX;
		}

		if (mapRotation == 0) {
			return x;
		} else if (mapRotation == 1) {
			return y;
		} else if (mapRotation == 2) {
			return 15 - x - (sizeX - 1);
		} else if (mapRotation == 3) {
			return 15 - y - (sizeY - 1);
		}

		return 0;
	}

	public static int translateY(int x, int y, int mapRotation, int sizeX, int sizeY, int objectRotation) {
		if ((objectRotation & 0x1) == 1) {
			int prevSizeX = sizeX;
			sizeX = sizeY;
			sizeY = prevSizeX;
		}

		if (mapRotation == 0) {
			return y;
		} else if (mapRotation == 1) {
			return 15 - x - (sizeX - 1);
		} else if (mapRotation == 2) {
			return 15 - y - (sizeY - 1);
		} else if (mapRotation == 3) {
			return x;
		}

		return 0;
	}

	public Room getRoom(RoomReference reference) {
		return dungeon == null ? null : dungeon.getRoom(reference);
	}

	public boolean isDestroyed() {
		return dungeon == null;
	}
	

	public DynamicObjectManager getObjectManager() {
		return objectManager;
	}

	public NPC spawnNPC(RoomReference reference, int id, int x, int y) {
		return spawnNPC(reference, id, x, y, false, DungeonConstants.NORMAL_NPC);
	}

	public NPC spawnNPC(final RoomReference reference, final int id, int x, int y, boolean check, final int type) {
		return spawnNPC(reference, id, x, y, check, type, true);
	}

	public NPC spawnNPC(final RoomReference reference, final int id, int x, int y, boolean check, final int type, boolean move) {
		final int rotation = dungeon.getRoom(reference).getRotation();
		final int size = EntityDef.forID(id).getSize();
		final Location tile = Location.create(getRoomX(reference) + translateX(x, y, rotation, size, size, 0), getRoomY(reference) + translateY(x, y, rotation, size, size, 0), height);
		if (check && !isTileFree(0, tile.getX(), tile.getY(), size, clipping)) {
			return null;
		}

		return spawnNPC(id, tile, reference, type, move);
	}
	
	public NPC spawnBosses(RoomReference reference, int[][] array, int x, int y) {		
		int minAvg = teamAvg / 5 * party.difficulty; 
		int maxAvg = minAvg + (teamAvg / 20) + 40; //Max level solo = 792 + 238
		int closestIndex = -1;
		int closestDelta = 99999;
		
		List<Integer> indices = new ArrayList<>();
		for (int i = 0, length = array.length; i < length; i++) {
			int[] data = array[i];
			int avg = data[1];
			//System.out.println(data[0]+":"+avg+":"+maxAvg+":"+minAvg);
			if (avg >= minAvg && avg <= maxAvg) {
				indices.add(i);
			}

			//if (i > 1 && i < length - 1) {
				//int deltaMin = Math.abs(minAvg - array[i - 1][0]);
				int deltaMax = Math.abs(maxAvg - avg);//array[i + 1][0]);
				int delta = deltaMax;//Math.max(deltaMin, deltaMax);
				if (delta < closestDelta) {
					//if (delta == deltaMin) {
					//	closestIndex = i - 1;
					//} else {
						closestIndex = i;// + 1;
					//}
					closestDelta = delta;
				}
			//}
		}

		int size = indices.size();
		int npc;
		if (size == 0) {
			npc = array[Math.max(0, closestIndex)][0];
			//System.out.println("closest="+npc);
		} else {
			npc = array[indices.get(Misc.randomNoPlus(size))][0];
			//System.out.println("random="+npc + ":"+indices);
		}

		NPC npcEntry = spawnNPC(reference, npc, x, y, false, DungeonConstants.BOSS_NPC);
		//TODO to move to load
		DungeonBossesStats.setStats(npcEntry, array);
		return npcEntry;
	}

	public NPC spawnNPC(int id, Location tile, RoomReference reference, int type, boolean move) {
		NPC n = NPCHandler.spawnDungeoneering(id, tile.getX(), tile.getY(), tile.getZ(), move ? 1 : 0, this, reference);
		n.getDistanceRules().setFollowDistance(20);
		n.setDynamicRegionClip(clipping);
		n.dungeoneeringType = type;
		n.reference = reference;
		switch(type) {
			case DungeonConstants.FISH_SPOT_NPC:
				n.dungFishCount = random(1, 15);
				int tier = DungeoneeringFishData.pickFish(party.getMaxLevel(Skills.FISHING));
				int roll = Misc.random(Math.max(0, tier - 1), tier);
				n.fishData = DungeoneeringFishData.values[roll];
				n.dontFace = true;
				n.setCustomName(n.fishData.getName());
				break;
			case DungeonConstants.GUARDIAN_NPC:
				n.isAggressive = true;
				n.setRetargetTick(18);
				totalGuardiansSpawned++;
				break;
		}

		return n;
	}

	public void addNpc(NPC npc) {
		if (npcs == null) {
			destroy(true);
			return;
		}

		npcs.add(npc);
	}

	public NPC spawnNpc(RoomReference reference, int[][] array) {
		int extraMod;
		int formula;
		if (teamAvg <= 200) {
			extraMod = 50 + teamAvg + (teamAvg / (50 * party.difficulty));
			formula = teamAvg / 20 * 3;
		} else {
			int div = Misc.interpolate(100, 50, 1, 6, party.getComplexity());
			extraMod = teamAvg + (teamAvg / (div * party.difficulty));
			formula = teamAvg / 14 * 3;
		}
		
		int minCheck = formula * (party.difficulty - 1) + 20;

		int low = minCheck;
		if (party.getComplexity() >= 2) {
			low += low * (0.025 * party.getComplexity());
		}
		if (party.floor >= 10) {
			low += low * (0.025 * (party.floor / 10));
		}
		low = Math.min(low, 3960);
		//TODO move to local vars

		//System.out.println(extraMod+":"+low+":"+minCheck);
		int index = 0;
		List<Integer> indices = new ArrayList<>();
		for (int i = 0, length = array.length; i < length; i++) {
			int[] data = array[i];
			int avg = Math.min(data[1], 3959);
			//System.out.println(data[0]+":"+avg+":"+extraMod+":"+minCheck+":"+low);
			if (avg <= extraMod && avg >= minCheck) {
				indices.add(i);

				if (index == 0 && low <= avg) {
					index = indices.size();
				}
			}
		}

		int size = indices.size();
		if (size == 0) {
			//System.out.println("hmm didnt pick any from "+extraMod+":"+minCheck+":"+Arrays.toString(array));
			return null;
		}

		int min = 0;
		if (random(1) == 0) {
			min = index;
		}

		int roll = randomNoPlus(min, size);
		if (roll < 0 || roll >= size) {
			//System.out.println("broken roll, heres some debug info. " + indices +", "+Arrays.toString(array)+","+teamAvg+":"+minCheck+":"+extraMod+":"+low);
			roll = 0;
		}

		int tries = 0;
		int rollIndex = indices.get(roll);
		int id = array[rollIndex][0];
		NPC npc = spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.GUARDIAN_NPC);
		while (npc == null && tries < 10) {
			npc = spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.GUARDIAN_NPC);
			tries++;
		}

		if (npc == null) {
			return null;
		}

		int arrayLength = array[rollIndex].length;
		if (arrayLength > 2) {
			npc.dungeonTier = array[rollIndex][2] - 1;
		} else {
			npc.dungeonTier = -1;
		}

		if (arrayLength > 3) {
			npc.dungeonWeaponIdentifier = array[rollIndex][3];
		} else {
			npc.dungeonWeaponIdentifier = -1;
		}
		return npc;
	}

	public void spawnRandomNPCS(RoomReference reference) {
		if (teamAvg > 0) {
			int arraysSize = npcsToSpawn.size();
			if (arraysSize == 0) {
				//System.out.println("no available npcs for " + teamAvg + " avg.");
				return;
			}

			int npcCount = 2 + party.difficulty;
			for (int b = 0; b < npcCount; b++) {
				int[][] array = npcsToSpawn.get(randomNoPlus(arraysSize));
				spawnNpc(reference, array);
			}

			if (Misc.random(1) == 1) {//Roll on slayer npc
				int roll = Misc.random(15);
				if (roll >= 1) {//Roll on general spawn table
					int slayLvl = party.getMaxLevel(Skills.SLAYER);
					int id = -1;
					roll = Misc.random(600);
					if (roll >= 500 && slayLvl >= 5) {//crawl hand
						id = 10694;
					} else if (roll >= 400 && slayLvl >= 17) {//cave slime
						id = 10696;
					} else if (roll >= 300 && slayLvl >= 30) {//pyrefiend
						id = 10697;
					} else if (roll >= 200 && slayLvl >= 41) {//nightspider
						id = 10698;
					} else if (roll >= 100 && slayLvl >= 52) {//jelly
						id = 10699;
					} else if (roll >= 0 && slayLvl >= 63) {//spirit guard
						id = 10700;
					}

					if (id != -1) {
						spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
					}
					return;
				}

				//access special floor spawn table
				switch(party.getFloorType()) {
					case DungeonConstants.FROZEN_FLOORS:
						roll = Misc.random(50);
						if (roll >= 12) {
							spawnNPC(reference, 10695, 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
						}
						break;
					case DungeonConstants.ABANDONED_FLOORS: {
						boolean second = party.getFloorType() >= 30 && party.getFloorType() <= 35;
						roll = Misc.random(1000);
						int id = -1;
						if (roll >= 950) {
							id = 10695;
						} else if (roll >= 900) {
							id = 10701;
						} else if (roll >= 800 && second) {
							id = 10702;
						} else if (roll >= 750 && second) {
							id = 10704;
						} else if (roll >= 710 && second) {
							id = 10705;
						}

						if (id != -1) {
							spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
						}
						break;
					}
					case DungeonConstants.FURNISHED_FLOORS: {
						roll = Misc.random(1000);
						int id = -1;
						if (roll >= 900) {
							id = 10695;
						} else if (roll >= 800) {
							id = 10701;
						} else if (roll >= 700) {
							id = 10702;
						} else if (roll >= 650) {
							id = 10704;
						}

						if (id != -1) {
							spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
						}
						break;
					}
					case DungeonConstants.OCCULT_FLOORS: {
						roll = Misc.random(1000);
						int id = -1;
						if (roll >= 700) {
							id = 10701;
						} else if (roll >= 580) {
							id = 10702;
						} else if (roll >= 460) {
							id = 10704;
						} else if (roll >= 390) {
							id = 10705;
						}

						if (id != -1) {
							spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
						}
						break;
					}
					case DungeonConstants.WARPED_FLOORS: {
						roll = Misc.random(1000);
						int id = -1;
						if (roll >= 700) {
							id = 10701;
						} else if (roll >= 550) {
							id = 10702;
						} else if (roll >= 420) {
							id = 10704;
						} else if (roll >= 320) {
							id = 10705;
						}

						if (id != -1) {
							spawnNPC(reference, id, 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
						}
						break;
					}
				}
			}
			return;
		}

		int floorType = party.getFloorType();
		for (int i = 0; i < 3; i++) { //all floors creatures
			spawnNPC(reference, DungeonUtils.getGuardianCreature(), 2 + random(13), 2 + random(13), true, DungeonConstants.GUARDIAN_NPC);
		}

		for (int i = 0; i < 2; i++) { //this kind of floor creatures
			spawnNPC(reference, DungeonUtils.getGuardianCreature(floorType), 2 + random(13), 2 + random(13), true, DungeonConstants.GUARDIAN_NPC);
		}
		//forgotten warrior
		if (random(2) == 0)
			spawnNPC(reference, DungeonUtils.getForgottenWarrior(), 2 + random(13), 2 + random(13), true, DungeonConstants.FORGOTTEN_WARRIOR);
		//hunter creature
		spawnNPC(reference, DungeonUtils.getHunterCreature(), 2 + random(13), 2 + random(13), true, DungeonConstants.HUNTER_NPC);
		if (random(5) == 0) //slayer creature
			spawnNPC(reference, DungeonUtils.getSlayerCreature(), 2 + random(13), 2 + random(13), true, DungeonConstants.SLAYER_NPC);
	}

	public void messageRoom(RoomReference reference, String message) {
		for (Player player : party.partyMembers) {
			if (reference.equals(getCurrentRoomReference(player.getCurrentLocation()))) {
				player.sendMessage(message);
			}
		}
	}

	public VisibleRoom getVisibleRoom(RoomReference reference) {
		if (reference.getX() >= visibleMap.length || reference.getY() >= visibleMap[0].length) {
			return null;
		}

		return visibleMap[reference.getX()][reference.getY()];
	}

	public Location getTile(RoomReference reference, int x, int y) {
		return getTile(reference, x, y, 1, 1);
	}

	public Location getTile(RoomReference reference, int x, int y, int sizeX, int sizeY) {
		Room room = dungeon.getRoom(reference);
		if(room == null) {
			return null;
		}

		int mapRotation = room.getRotation();
		return Location.create(getRoomX(reference) + translateX(x, y, mapRotation, sizeX, sizeY, 0), getRoomY(reference) + translateY(x, y, mapRotation, sizeX, sizeY, 0), height);
	}

	public boolean hasStarted() {
		return loaded;
	}

	public Location getRoomBaseTile(RoomReference reference) {
		return Location.create(getRoomX(reference), getRoomY(reference), height);
	}

	public Location getRoomCenterTile(RoomReference reference) {
		return getRoomBaseTile(reference).transform(8, 8, 0);
	}

	public int getRoomPosX(Location tile, int size) {
		RoomReference reference = getCurrentRoomReference(tile);
		Room room = getRoom(reference);
		if (room == null) {
			return 0;
		}

		int x = tile.getX() - getRoomX(reference);
		int y = tile.getY() - getRoomY(reference);
		return translateX(x, y, (4 - room.getRotation()) & 0x3, size, size, 0);
	}

	public int getRoomPosY(Location tile, int size) {
		RoomReference reference = getCurrentRoomReference(tile);
		Room room = getRoom(reference);
		if (room == null) {
			return 0;
		}

		int x = tile.getX() - getRoomX(reference);
		int y = tile.getY() - getRoomY(reference);
		return translateY(x, y, (4 - room.getRotation()) & 0x3, size, size, 0);
	}
	
	public int getRoomPosX(Location tile) {
		return getRoomPosX(tile, 1);
	}

	public int getRoomPosY(Location tile) {
		return getRoomPosY(tile, 1);
	}

	public void openStairs(RoomReference reference) {
		Room room = getRoom(reference);

		int chunkX = room.getRoom().getChunkX();
		int chunkY = room.getRoom().getChunkY();
		int rotation = 3;
		int x = 7;
		int y = 15;
		if (chunkX == 26 && chunkY == 640) {//unholy cursed
			y = 11;
		} else if (chunkX == 30 && chunkY == 656) {//stomp
			rotation = 0;
			x = 14;
			y = 3;
		} else if ((chunkX == 30 && chunkY == 672) || (chunkX == 24 && chunkY == 690)) {//necromancer
			rotation = 0;
			x = 15;
			y = 3;
		} else if (chunkX == 26 && chunkY == 690) {//world-gorger
			x = 11;
		} else if (chunkX == 24 && chunkY == 688) {//blink
			y = 14;
		}

		spawnObject(reference, DungeonConstants.LADDERS[party.getFloorType()], 10, rotation, x, y);
		canEnd = true;
	}

	public void setState(int playerIndex, int state) {
		endScreenStates |= state << (playerIndex << 1);
		updateEndScreenStates();
	}

	public void updateEndScreenStates() {
		for (int i = 0, partySize = party.partyMembers.size(); i < partySize; i++) {
			Player player = party.partyMembers.get(i);
			player.getPacketSender().sendConfig(ConfigCodes.PLAYERS_STATES, endScreenStates);
		}
	}

	public void end() {
		int endTicks = Server.getTotalTicks() - startTick;
		int partySize = party.partyMembers.size();

		for (int i = 0; i < partySize; i++) {
			Player player = party.partyMembers.get(i);
			player.dungPartyIndex = i;
			player.setStunned(true);
			int floor = party.floor;
			int complexity = party.getComplexity();
			int size = party.size;
			boolean completed = player.isDungFloorCompleted(floor);
			int newFloor = floor;

			if (completed) {
				int[] range = DungeonUtils.getFloorThemeRange(floor);
				//check down
				for (int toCheckFloor = floor; toCheckFloor >= range[0]; toCheckFloor--) {
					if (!player.isDungFloorCompleted(toCheckFloor)) {
						player.sendMessage("Since you have previously completed this floor, floor " + toCheckFloor + " was instead ticked-off.");
						newFloor = toCheckFloor;
						completed = false;
						break;
					}
				}
				//check up if all down completed
				if (completed) {
					for (int toCheckFloor = floor; toCheckFloor <= range[1]; toCheckFloor++) {
						if (player.dungFloorsUnlocked < toCheckFloor) {
							break;
						}
	
						if (!player.isDungFloorCompleted(toCheckFloor)) {
							player.sendMessage("Since you have previously completed this floor, floor " + toCheckFloor + " was instead ticked-off.");
							newFloor = toCheckFloor;
							completed = false;
							break;
						}
					}
				}
			}
			
			if (floor == 11) {
				player.getAchievement().completeAllFrozen();
			}
			
			if (floor == 29) {
				player.getAchievement().completeAllFurnished();
			}
			
			if (floor >= 48) {
				player.getAchievement().complete50Warped();
			}
			
			if  (party.size == DungeonConstants.LARGE_DUNGEON) {
				player.getAchievement().complete20LargeFloors();
				if (floor == 60 && party.getComplexity() == 6) {
					player.getAchievement().completeFloor60C6Large();
				}
			}

			player.getPacketSender().setVisibility(44243, true);

			if (floor == player.dungFloorsUnlocked && floor < DungeonUtils.getMaxFloor(player.getSkills().getStaticLevel(Skills.DUNGEONEERING))) {
				player.increaseFloorsUnlocked();
			}

			int nextComplexity = complexity + 1;
			if (nextComplexity < 7 && player.isComplexityLocked(nextComplexity)) {
				player.unlockComplexity(nextComplexity);
			}
			
			int[] modifierData = ExpModifier.dungeoneeringModifierExp(party, player, this);

			//Complexity
			int complexityBonus = modifierData[4];
			player.getPacketSender().sendString(Integer.toString(complexity), 44230);
			player.getPacketSender().sendString((complexityBonus == 0 ? "+0%" : complexityBonus + "%"), 44232);
			
			//Difficulty
			int difficultyBonus = modifierData[3];
			player.getPacketSender().sendString(partySize + " : " + party.difficulty, 44229);
			player.getPacketSender().sendString((difficultyBonus >= 0 ? "+" + difficultyBonus + "%" : "" + difficultyBonus + "%"), 44231);//TODO find calc for this

			//Dungeon size
			int dungeonSizeBonus = modifierData[1];
			player.getPacketSender().sendString("+" + dungeonSizeBonus + "%", 44233);
			
			int[] data = ExpAndTokens.dungeoneeringBaseExp(party, player, newFloor < floor ? newFloor : floor);

			player.setDungFloorCompleted(newFloor);
			
			//Floor
			int floorXp = data[0];
			if (newFloor < floor) {
				player.getPacketSender().sendString("<col=ff0000>Floor " + newFloor + ":", 44204);	
			} else {
				player.getPacketSender().sendString("Floor " + floor + ":", 44204);
			}
			player.getPacketSender().sendString(Integer.toString(floorXp), 44207);
			player.getPacketSender().sendConfig(1997, size);
			
			//Prestige
			int currentProgress = Party.updateProgress(player);

			int prestigeXp = data[1];
			player.getPacketSender().sendString(Integer.toString(prestigeXp), 44208);
			if (currentProgress < 1 && player.previousProgress < 1) {
				player.getPacketSender().sendString("Prestige 1", 44205);
			} else if (currentProgress < player.previousProgress) {
			    player.getPacketSender().sendString("Prestige " + player.previousProgress, 44205);
			} else if (currentProgress >= player.previousProgress) {
				player.getPacketSender().sendString("Prestige " + currentProgress, 44205);
			}

			if (currentProgress >= 60) {
				player.getAchievement().progress60Dungeoneering();
			}

			//Average
			int avgXp = (floorXp + prestigeXp) / 2;
			player.getPacketSender().sendString(Integer.toString(avgXp), 44209);
			
			//Bonus rooms
			int bonusRoom = modifierData[2];
			player.getPacketSender().sendString("+" + bonusRoom + "%", 44234);
			player.getPacketSender().sendConfig(8215, bonusRoom);
			
			//Deaths
			int deathPenalty = modifierData[7];
			player.getPacketSender().sendString((deathPenalty >= 0 ? "+" + deathPenalty + "%" : deathPenalty + "%"), 44237);
			player.getPacketSender().sendConfig(8216, player.dungDeaths);
			
			//Level mod
			int levelMod = modifierData[5];
			player.getPacketSender().sendString((levelMod >= 0 ? "+" + levelMod + "%" : levelMod + "%"), 44235);
			
			//Guide mode
			int guideMode = modifierData[6];
			player.getPacketSender().sendString((guideMode == 0 ? "+0%" : guideMode + "%"), 44236);
			
			//Unbalanced penalty
			int unbalancedPenalty = modifierData[8];
			if (unbalancedPenalty < -9) {
			    player.getPacketSender().sendString(unbalancedPenalty + "%", 44246);
			    player.getPacketSender().setVisibility(44243, false);
			    player.sendMessage("wut");
			}
			
			//Total modifier
			int totalModifier = modifierData[0];
			player.getPacketSender().sendConfig(8217, totalModifier);
			player.getPacketSender().sendString(totalModifier + "%", 44238);

			//Show time
			player.getPacketSender().sendString(ticksToTime(endTicks, true), 29995);

			//Show the party info
			player.getPacketSender().sendConfig(ConfigCodes.AMOUNT_OF_PLAYERS, partySize);
			player.getPacketSender().sendConfig(ConfigCodes.PLAYERS_STATES, endScreenStates);
			
			//Total XP
			float modifierPercent = (float)totalModifier / 100;
			int totalExp = (int) (avgXp * modifierPercent);
			player.getPacketSender().sendString(Integer.toString(PlayerAssistant.getMultiplier(Skills.DUNGEONEERING, player) * totalExp), 44240);
			player.getPA().addSkillXP(totalExp, Skills.DUNGEONEERING);

			//Names
			for (int j = 0; j < partySize; j++) {
				player.getPacketSender().sendString(party.partyMembers.get(j).getNameSmartUp(), NAME_BASE_ID + j);
			}

			//Tokens
			int tokens = ((int) totalExp / 10) * 3;
			player.getPacketSender().sendString(Integer.toString(tokens), 44242);
			player.dungTokens += tokens;

			player.getPacketSender().showInterface(44200);
			player.getPacketSender().setBlackout(2 + 0x80);

			player.clearDung();
		}

		loaded = false;
		endTimer = null;
		startNextTimer();
	}

	public int getBonusTotal() {
		int total = 0;
		for (int x = 0; x < visibleMap.length; x++) {
			for (int y = 0; y < visibleMap[x].length; y++) {
				Room room = dungeon.getRoom(x, y);
				if (room != null && !room.isCritPath()) {
					total++;
				}
			}
		}

		return total;
	}

	public int getBonusOpened() {
		int open = 0;
		for (int x = 0; x < visibleMap.length; x++) {
			for (int y = 0; y < visibleMap[x].length; y++) {
				if (visibleMap[x][y] != null && visibleMap[x][y].isLoaded() && !dungeon.getRoom(x, y).isCritPath()) {
					open++;
				}
			}
		}

		return open;
	}

	public void advanceFloor() {
		if (party.getMaxFloor() > party.floor) {
			party.floor++;
		}

		int complexity = party.getComplexity();
		int maxComplexity = party.getMaxComplexity();
		if (maxComplexity > complexity && maxComplexity < 6) {
			party.setComplexity(complexity + 1);
		}

		for (Client player : party.partyMembers) {
			Party.updateFloorInfo(player);
		}

		destroy(false);
		start();
	}

	public Location getHomeTile() {
		return getRoomCenterTile(dungeon.getStartRoomReference());
	}

	private void sendInfo(Player player, int x, int y, int sprite, int size, boolean isPlayer) {
		player.getPacketSender().sendConfig(8219, x);
		player.getPacketSender().sendConfig(8220, y);
		player.getPacketSender().sendConfig(8221, sprite);
		player.getPacketSender().sendConfig(8222, size);
		player.getPacketSender().sendConfig(8218, isPlayer ? 3 : 2);
	}

	public void openMap(Player player) {
		if (party.size == DungeonConstants.TEST_DUNGEON) {
			player.sendMessage("Dungeon is too big to be displayed.");
			return;
		}

		boolean guideMode = party.isGuideMode();
		player.getPacketSender().sendConfig(8218, 1);
		for (int x = 0, lengthX = visibleMap.length; x < lengthX; x++) {
			for (int y = 0, lengthY = visibleMap[x].length; y < lengthY; y++) {
				if (visibleMap[x][y] != null) { //means exists
					Room room = getRoom(new RoomReference(x, y));
					boolean highLight = guideMode && room.isCritPath();
					sendInfo(player, x, y, getMapIconSprite(room, highLight), party.size, false);
					if (room.getRoom() instanceof StartRoom) {
						sendInfo(player, x, y, 773, party.size, false);
					} else if (room.getRoom() instanceof BossRoom) {
						sendInfo(player, x, y, 775, party.size, false);
					}
					if (room.hasNorthDoor() && visibleMap[x][y + 1] == null) {
						Room unknownR = getRoom(new RoomReference(x, y + 1));
						highLight = guideMode && unknownR.isCritPath();
						sendInfo(player, x, y + 1, getMapIconSprite(DungeonConstants.SOUTH_DOOR, highLight), party.size, false);
					}
					if (room.hasSouthDoor() && visibleMap[x][y - 1] == null) {
						Room unknownR = getRoom(new RoomReference(x, y - 1));
						highLight = guideMode && unknownR.isCritPath();
						sendInfo(player, x, y - 1, getMapIconSprite(DungeonConstants.NORTH_DOOR, highLight), party.size, false);
					}
					if (room.hasEastDoor() && visibleMap[x + 1][y] == null) {
						Room unknownR = getRoom(new RoomReference(x + 1, y));
						highLight = guideMode && unknownR.isCritPath();
						sendInfo(player, x + 1, y, getMapIconSprite(DungeonConstants.WEST_DOOR, highLight), party.size, false);
					}
					if (room.hasWestDoor() && visibleMap[x - 1][y] == null) {
						Room unknownR = getRoom(new RoomReference(x - 1, y));
						highLight = guideMode && unknownR.isCritPath();
						sendInfo(player, x - 1, y, getMapIconSprite(DungeonConstants.EAST_DOOR, highLight), party.size, false);
					}
				}
			}
		}

		for (int i = 0, length = party.partyMembers.size(); i < length; i++) {
			Player p2 = party.partyMembers.get(i);
			RoomReference reference = getCurrentRoomReference(p2.getCurrentLocation());
			sendInfo(player, reference.getX(), reference.getY(), i, party.size, true);
		}

		player.getPacketSender().showInterface(43974);
	}

	public int getMapIconSprite(int direction, boolean highLight) {
		for (MapRoomIcon icon : MapRoomIcon.values) {
			if (icon.isOpen()) {
				continue;
			}

			if (icon.hasDoor(direction)) {
				return highLight ? icon.getSpriteGuideId() : icon.getSpriteId();
			}
		}

		return 759;
	}

	public int getMapIconSprite(Room room, boolean highLight) {
		for (MapRoomIcon icon : MapRoomIcon.values) {
			if (!icon.isOpen()) {
				continue;
			}

			if (icon.hasNorthDoor() == room.hasNorthDoor() && icon.hasSouthDoor() == room.hasSouthDoor() && icon.hasWestDoor() == room.hasWestDoor() && icon.hasEastDoor() == room.hasEastDoor()) {
				return highLight ? icon.getSpriteGuideId() : icon.getSpriteId();
			}
		}

		return 759;
	}

	public void showBar(RoomReference reference, String name, int percentage) {
		for (Player player : party.partyMembers) {
			if (player.walkableInterface == PROGRESS_INTERFACE_ID) {
				updateProgress(player, percentage);
				continue;
			}

			RoomReference current = getCurrentRoomReference(player.getCurrentLocation());
			if (!reference.equals(current)) {
				continue;
			}

			player.getPA().walkableInterface(PROGRESS_INTERFACE_ID);
			player.getPacketSender().sendString(name, 50103);
			updateProgress(player, percentage);
		}
	}

	public void updateProgress(Player player, int percentage) {
		player.getPacketSender().sendConfig(ConfigCodes.DUNG_PROGRESS_BAR, percentage * 198 / 100);
	}

	public void hideBar(RoomReference reference) {
		for (Player player : party.partyMembers) {
			RoomReference current = getCurrentRoomReference(player.getCurrentLocation());
			if (!reference.equals(current)) {
				continue;
			}

			player.getPA().walkableInterface(-1);
		}
	}

}