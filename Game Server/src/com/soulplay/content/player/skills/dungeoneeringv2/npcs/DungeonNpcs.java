package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;

public class DungeonNpcs {

	public static void load() {
		/* empty */
	}

	//all dungeon npcs
	//TODO: merge all warrior arrays into one when we get to the release part so the spawn rate of warriors will be normal
	public static final int[][] FORGOTTEN_WARIORS_DAGGER = { 
			{ 10398, 135, 1, 7}, // dagger - novite
			{ 10397, 163, 1, 7}, // dagger - novite
			{ 10399, 163, 1, 7}, // dagger - novite
			{ 10412, 192, 1, 7}, // dagger - novite
			{ 10408, 250, 1, 7}, // dagger - novite
			{ 10431, 508, 2, 7}, // dagger - bathus
			{ 10419, 566, 2, 7}, // dagger - bathus
			{ 10435, 566, 2, 7}, // dagger - bathus
			{ 10439, 825, 3, 7}, // dagger - marmaros
			{ 10452, 825, 3, 7}, // dagger - marmaros
			{ 10456, 825, 3, 7}, // dagger - marmaros
			{ 10256, 1170, 4, 7}, // dagger - kratonite
			{ 10263, 1170, 4, 7}, // dagger - kratonite
			{ 10287, 1429, 5, 7}, // dagger - fractite
			{ 10270, 1457, 5, 7}, // dagger - fractite
			{ 10274, 1457, 5, 7}, // dagger - fractite
			{ 10288, 1831, 6, 7}, // dagger - zeph
			{ 10298, 1831, 6, 7}, // dagger - zeph
			{ 10850, 2090, 7, 7}, // dagger - argonite
			{ 10863, 2119, 7, 7}, // dagger - argonite
			{ 10846, 2176, 7, 7}, // dagger - argonite
			{ 10874, 2435, 8, 7}, // dagger - katagon
			{ 10867, 2493, 8, 7}, // dagger - katagon
			{ 10882, 2493, 8, 7}, // dagger - katagon
			{ 10884, 2522, 8, 7}, // dagger - katagon
			{ 10902, 2780, 9, 7}, // dagger - gorgonite
			{ 10895, 2838, 9, 7}, // dagger - gorgonite
			{ 10885, 2867, 9, 7}, // dagger - gorgonite
			{ 10520, 3097, 10, 7}, // dagger - Promethium
			{ 10507, 3154, 10, 7}, // dagger - Promethium
			{ 10524, 3154, 10, 7}, // dagger - Promethium
			{ 10535, 3758, 10, 7}, // dagger - Primal
			{ 10548, 3787, 10, 7}, // dagger - Primal
			{ 10531, 3816, 10, 7}, // dagger - Primal
	};

	public static final int[][] FORGOTTEN_WARRIORS_RAPIER = { 
			{ 10414, 135, 1, 6}, // rapier - novite
			{ 10428, 480, 2, 6}, // rapier - bathus
			{ 10418, 508, 2, 6}, // rapier - bathus
			{ 10440, 825, 3, 6}, // rapier - marmaros
			{ 10453, 825, 3, 6}, // rapier - marmaros
			{ 10449, 854, 3, 6}, // rapier - marmaros
			{ 10247, 1141, 4, 6}, // rapier - kratonite
			{ 10257, 1170, 4, 6}, // rapier - kratonite
			{ 10260, 1170, 4, 6}, // rapier - kratonite
			{ 10267, 1429, 4, 6}, // rapier - kratonite
			{ 10284, 1486, 5, 6}, // rapier - fractite
			{ 10277, 1544, 5, 6}, // rapier - fractite
			{ 10295, 1774, 6, 6}, // rapier - zeph
			{ 10294, 1745, 6, 6}, // rapier - zeph
			{ 10291, 1831, 6, 6}, // rapier - zeph
			{ 10305, 1831, 6, 6}, // rapier - zeph
			{ 10843, 2090, 7, 6}, // rapier - argonite
			{ 10853, 2176, 7, 6}, // rapier - argonite
			{ 10857, 2176, 7, 6}, // rapier - argonite
			{ 10878, 2464, 8, 6}, // rapier - katagon
			{ 10870, 2522, 8, 6}, // rapier - katagon
			{ 10875, 2522, 8, 6}, // rapier - katagon
			{ 10903, 2780, 9, 6}, // rapier - gorgonite
			{ 10886, 2809, 9, 6}, // rapier - gorgonite
			{ 10898, 2838, 9, 6}, // rapier - gorgonite
			{ 10513, 3097, 10, 6}, // rapier - Promethium
			{ 10521, 3125, 10, 6}, // rapier - Promethium
			{ 10536, 3758, 10, 6}, // rapier - Primal
			{ 10545, 3758, 10, 6}, // rapier - Primal
			{ 10528, 3816, 10, 6}, // rapier - Primal
	};

	public static final int[][] FORGOTTEN_LONGSWORD = { 
			{ 10410, 135, 1, 5}, // longsword - novite
			{ 10404, 163, 1, 5}, // longsword - novite
			{ 10415, 163, 1, 5}, // longsword - novite
			{ 10403, 192, 1, 5}, // longsword - novite
			{ 10425, 451, 2, 5}, // longsword - bathus
			{ 10424, 480, 2, 5}, // longsword - bathus
			{ 10436, 480, 2, 5}, // longsword - bathus
			{ 10432, 480, 2, 5}, // longsword - bathus
			{ 10450, 796, 3, 5}, // longsword - marmaros
			{ 10454, 796, 3, 5}, // longsword - marmaros
			{ 10445, 854, 3, 5}, // longsword - marmaros
			{ 10266, 1112, 4, 5}, // longsword - kratonite
			{ 10253, 1141, 4, 5}, // longsword - kratonite
			{ 10249, 1199, 4, 5}, // longsword - kratonite
			{ 10271, 1486, 5, 5}, // longsword - fractite
			{ 10275, 1515, 5, 5}, // longsword - fractite
			{ 10281, 1544, 5, 5}, // longsword - fractite
			{ 10306, 1831, 6, 5}, // longsword - zeph
			{ 10299, 1860, 6, 5}, // longsword - zeph
			{ 10289, 1889, 6, 5}, // longsword - zeph
			{ 10851, 2119, 7, 5}, // longsword - argonite
			{ 10860, 2119, 7, 5}, // longsword - argonite
			{ 10849, 2148, 7, 5}, // longsword - argonite
			{ 10871, 2407, 8, 5}, // longsword - katagon
			{ 10868, 2493, 8, 5}, // longsword - katagon
			{ 10879, 2522, 8, 5}, // longsword - katagon
			{ 10892, 2752, 9, 5}, // longsword - gorgonite
			{ 10888, 2838, 9, 5}, // longsword - gorgonite
			{ 10905, 2838, 9, 5}, // longsword - gorgonite
			{ 10522, 3068, 10, 5}, // longsword - Promethium
			{ 10508, 3097, 10, 5}, // longsword - Promethium
			{ 10517, 3097, 10, 5}, // longsword - Promethium
			{ 10542, 3758, 10, 5}, // longsword - Primal
			{ 10529, 3787, 10, 5}, // longsword - Primal
			{ 10538, 3844, 10, 5}, // longsword - Primal
	};

	public static final int[][] FORGOTTEN_WARRIORS_WARHAMMER = { 
			{ 10417, 135, 1, 4}, // warhammer - novite
			{ 10400, 163, 1, 4}, // warhammer - novite
			{ 10405, 192, 1, 4}, // warhammer - novite
			{ 10429, 451, 2, 4}, // warhammer - bathus
			{ 10421, 508, 2, 4}, // warhammer - bathus
			{ 10446, 796, 3, 4}, // warhammer - marmaros
			{ 10442, 825, 3, 4}, // warhammer - marmaros
			{ 10459, 854, 3, 4}, // warhammer - marmaros
			{ 10250, 1141, 4, 4}, // warhammer - kratonite
			{ 10254, 1141, 4, 4}, // warhammer - kratonite
			{ 10264, 1227, 4, 4}, // warhammer - kratonite
			{ 10285, 1429, 5, 4}, // warhammer - fractite
			{ 10273, 1457, 5, 4}, // warhammer - fractite
			{ 10278, 1486, 5, 4}, // warhammer - fractite
			{ 10296, 1774, 6, 4}, // warhammer - zeph
			{ 10308, 1774, 6, 4}, // warhammer - zeph
			{ 10844, 2119, 7, 4}, // warhammer - argonite
			{ 10856, 2148, 7, 4}, // warhammer - argonite
			{ 10861, 2205, 7, 4}, // warhammer - argonite
			{ 10881, 2435, 8, 4}, // warhammer - katagon
			{ 10864, 2493, 8, 4}, // warhammer - katagon
			{ 10891, 2809, 9, 4}, // warhammer - gorgonite
			{ 10893, 2809, 9, 4}, // warhammer - gorgonite
			{ 10899, 2809, 9, 4}, // warhammer - gorgonite
			{ 10510, 3097, 10, 4}, // warhammer - Promethium
			{ 10525, 3154, 10, 4}, // warhammer - Promethium
			{ 10546, 3729, 10, 4}, // warhammer - Primal
			{ 10532, 3816, 10, 4}, // warhammer - Primal
			{ 10541, 3844, 10, 4}, // warhammer - Primal
	};

	public static final int[][] FORGOTTEN_WARRIORS_BATTLEAXE = { 
			{ 10401, 135, 1, 3}, // baxe - novite
			{ 10411, 135, 1, 3}, // baxe - novite
			{ 10407, 163, 1, 3}, // baxe - novite
			{ 10426, 451, 2, 3}, // baxe - bathus
			{ 10422, 508, 2, 3}, // baxe - bathus
			{ 10438, 508, 2, 3}, // baxe - bathus
			{ 10443, 796, 3, 3}, // baxe - maramros
			{ 10457, 796, 3, 3}, // baxe - maramros
			{ 10447, 882, 3, 3}, // baxe - marmaros
			{ 10246, 1084, 4, 3}, // baxe - kratonite
			{ 10261, 1112, 4, 3}, // baxe - kratonite
			{ 10259, 1141, 4, 3}, // baxe - kratonite
			{ 10252, 1199, 4, 3}, // baxe - kratonite
			{ 10268, 1429, 5, 3}, // baxe - fractite
			{ 10280, 1457, 5, 3}, // baxe - fractite
			{ 10282, 1457, 5, 3}, // baxe - fractite
			{ 10292, 1745, 6, 3}, // baxe - zeph
			{ 10301, 1831, 6, 3}, // baxe - zeph
			{ 10303, 1860, 6, 3}, // baxe - zeph
			{ 10847, 2119, 7, 3}, // baxe - argonite
			{ 10858, 2119, 7, 3}, // baxe - argonite
			{ 10854, 2148, 7, 3}, // baxe - argonite
			{ 10877, 2464, 8, 3}, // baxe - katagon
			{ 10865, 2550, 8, 3}, // baxe - katagon
			{ 10889, 2780, 9, 3}, // baxe - gorgonite
			{ 10896, 2780, 9, 3}, // baxe - gorgonite
			{ 10900, 2780, 9, 3}, // baxe - gorgonite
			{ 10511, 3125, 10, 3}, // Battleaxe - Promethium
			{ 10515, 3154, 10, 3}, // Battleaxe - Promethium
			{ 10527, 3183, 10, 3}, // Battleaxe - Promethium
			{ 10534, 3758, 10, 3}, // Battleaxe - Primal
			{ 10539, 3758, 10, 3}, // Battleaxe - Primal
			{ 10543, 3787, 10, 3}, // Battleaxe - Primal
	};

	public static final int[][] FORGOTTEN_WARRIORS_2H = { 
			{ 10402, 135, 1, 2}, // 2h - novite
			{ 10413, 135, 1, 2}, // 2h - novite
			{ 10430, 566, 2, 2}, // 2h - bathus
			{ 10441, 825, 3, 2}, // 2h - marmaros
			{ 10451, 854, 3, 2}, // 2h - marmaros
			{ 10258, 1141, 4, 2}, // 2h - kratonite
			{ 10248, 1170, 4, 2}, // 2h - kratonite
			{ 10283, 1544, 5, 2}, // 2h - fractite
			{ 10272, 1429, 5, 2}, // 2h - fractite
			{ 10300, 1803, 6, 2}, // 2h - zeph
			{ 10848, 2148, 7, 2}, // 2h - argonite
			{ 10852, 2148, 7, 2}, // 2h - argonite
			{ 10866, 2464, 8, 2}, // 2h - katagon
			{ 10876, 2493, 8, 2}, // 2h - katagon
			{ 10904, 2809, 9, 2}, // 2h - gorgonite
			{ 10894, 2838, 9, 2}, // 2h - gorgonite
			{ 10512, 3154, 10, 2}, // 2h - Promethium
			{ 10516, 3212, 10, 2}, // 2h - Promethium
			{ 10533, 3787, 10, 2}, // 2h - Primal
			{ 10537, 3787, 10, 2}, // 2h - Primal
	};

	public static final int[][] FORGOTTEN_WARRIORS_SPEAR = { 
			{ 10406, 163, 1, 1}, // spear - novite
			{ 10423, 451, 2, 1}, // spear - bathus
			{ 10434, 508, 2, 1}, // spear - bathus
			{ 10437, 508, 2, 1}, // spear - bathus
			{ 10458, 825, 3, 1}, // spear - marmaros
			{ 10448, 854, 3, 1}, // spear - marmaros
			{ 10262, 1170, 4, 1}, // spear - kratonite
			{ 10251, 1199, 4, 1}, // spear - kratonite
			{ 10276, 1457, 5, 1}, // spear - fractite
			{ 10286, 1486, 5, 1}, // spear - fractite
			{ 10307, 1803, 6, 1}, // spear - zeph
			{ 10290, 1831, 6, 1}, // spear - zeph
			{ 10845, 2061, 7, 1}, // spear - argonite
			{ 10862, 2176, 7, 1}, // spear - argonite
			{ 10873, 2464, 8, 1}, // spear - katagon
			{ 10883, 2464, 8, 1}, // spear - katagon
			{ 10887, 2780, 9, 1}, // spear - gorgonite
			{ 10897, 2780, 9, 1}, // spear - gorgonite
			{ 10509, 3125, 10, 1}, // spear - Promethium
			{ 10526, 3183, 10, 1}, // spear - Promethium
			{ 10540, 3816, 10, 1}, // spear - Primal
			{ 10544, 3844, 10, 1}, // spear - Primal
	};
	public static final int[][] FORGOTTEN_WARRIORS_MAUL = { 
			{ 10416, 135, 1, 0}, // maul - novite
			{ 10409, 192, 1, 0}, // maul - novite
			{ 10420, 451, 2, 0}, // maul - bathus
			{ 10427, 508, 2, 0}, // maul - bathus
			{ 10444, 825, 3, 0}, // maul - marmaros
			{ 10455, 854, 3, 0}, // maul - marmaros
			{ 10255, 1141, 4, 0}, // maul - kratonite
			{ 10265, 1141, 4, 0}, // maul - kratonite
			{ 10269, 1486, 5, 0}, // maul - fractite
			{ 10279, 1544, 5, 0}, // maul - fractite
			{ 10297, 1774, 6, 0}, // maul - zeph
			{ 10304, 1803, 6, 0}, // maul - zeph
			{ 10293, 1831, 6, 0}, // maul - zeph
			{ 10859, 2090, 7, 0}, // maul - argonite
			{ 10855, 2119, 7, 0}, // maul - argonite
			{ 10880, 2493, 8, 0}, // maul - katagon
			{ 10869, 2550, 8, 0}, // maul - katagon
			{ 10890, 2838, 9, 0}, // maul - gorgonite
			{ 10901, 2838, 9, 0}, // maul - gorgonite
			{ 10523, 3154, 10, 0}, // maul - Promethium
			{ 10519, 3212, 10, 0}, // maul - Promethium
			{ 10530, 3729, 10, 0}, // maul - Primal
			{ 10547, 3758, 10, 0}, // maul - Primal
	};

	public static final int[][] FORGOTTEN_WARRIORS = { //right now we using this
			{10398,135},
			{10401,135},
			{10402,135},
			{10410,135},
			{10411,135},
			{10413,135},
			{10414,135},
			{10416,135},
			{10417,135},
			{10397,163},
			{10399,163},
			{10400,163},
			{10404,163},
			{10406,163},
			{10407,163},
			{10415,163},
			{10403,192},
			{10405,192},
			{10409,192},
			{10412,192},
			{10408,250},
			{10420,451},
			{10423,451},
			{10425,451},
			{10426,451},
			{10429,451},
			{10424,480},
			{10428,480},
			{10432,480},
			{10433,480},
			{10436,480},
			{10418,508},
			{10421,508},
			{10422,508},
			{10427,508},
			{10431,508},
			{10434,508},
			{10437,508},
			{10438,508},
			{10419,566},
			{10430,566},
			{10435,566},
			{10443,796},
			{10446,796},
			{10450,796},
			{10454,796},
			{10457,796},
			{10439,825},
			{10440,825},
			{10441,825},
			{10442,825},
			{10444,825},
			{10452,825},
			{10453,825},
			{10456,825},
			{10458,825},
			{10445,854},
			{10448,854},
			{10449,854},
			{10451,854},
			{10455,854},
			{10459,854},
			{10447,882},
			{10246,1084},
			{10261,1112},
			{10266,1112},
			{10247,1141},
			{10250,1141},
			{10253,1141},
			{10254,1141},
			{10255,1141},
			{10258,1141},
			{10259,1141},
			{10265,1141},
			{10248,1170},
			{10256,1170},
			{10257,1170},
			{10260,1170},
			{10262,1170},
			{10263,1170},
			{10249,1199},
			{10251,1199},
			{10252,1199},
			{10264,1227},
			{10267,1429},
			{10268,1429},
			{10272,1429},
			{10285,1429},
			{10287,1429},
			{10270,1457},
			{10273,1457},
			{10274,1457},
			{10276,1457},
			{10280,1457},
			{10282,1457},
			{10269,1486},
			{10271,1486},
			{10278,1486},
			{10284,1486},
			{10286,1486},
			{10275,1515},
			{10277,1544},
			{10279,1544},
			{10281,1544},
			{10283,1544},
			{10292,1745},
			{10294,1745},
			{10295,1774},
			{10296,1774},
			{10297,1774},
			{10308,1774},
			{10300,1803},
			{10304,1803},
			{10307,1803},
			{10288,1831},
			{10290,1831},
			{10291,1831},
			{10293,1831},
			{10298,1831},
			{10301,1831},
			{10302,1831},
			{10305,1831},
			{10306,1831},
			{10299,1860},
			{10303,1860},
			{10289,1889},
			{10845,2061},
			{10843,2090},
			{10850,2090},
			{10859,2090},
			{10844,2119},
			{10847,2119},
			{10851,2119},
			{10855,2119},
			{10858,2119},
			{10860,2119},
			{10863,2119},
			{10848,2148},
			{10849,2148},
			{10852,2148},
			{10854,2148},
			{10856,2148},
			{10846,2176},
			{10853,2176},
			{10857,2176},
			{10862,2176},
			{10861,2205},
			{10871,2407},
			{10872,2435},
			{10874,2435},
			{10881,2435},
			{10866,2464},
			{10873,2464},
			{10877,2464},
			{10878,2464},
			{10883,2464},
			{10864,2493},
			{10867,2493},
			{10868,2493},
			{10876,2493},
			{10880,2493},
			{10882,2493},
			{10870,2522},
			{10875,2522},
			{10879,2522},
			{10884,2522},
			{10865,2550},
			{10869,2550},
			{10892,2752},
			{10887,2780},
			{10889,2780},
			{10896,2780},
			{10897,2780},
			{10900,2780},
			{10902,2780},
			{10903,2780},
			{10886,2809},
			{10891,2809},
			{10893,2809},
			{10899,2809},
			{10904,2809},
			{10888,2838},
			{10890,2838},
			{10894,2838},
			{10895,2838},
			{10898,2838},
			{10901,2838},
			{10905,2838},
			{10885,2867},
			{10522,3068},
			{10508,3097},
			{10510,3097},
			{10513,3097},
			{10517,3097},
			{10520,3097},
			{10509,3125},
			{10511,3125},
			{10521,3125},
			{10507,3154},
			{10512,3154},
			{10514,3154},
			{10515,3154},
			{10518,3154},
			{10523,3154},
			{10524,3154},
			{10525,3154},
			{10526,3183},
			{10527,3183},
			{10516,3212},
			{10519,3212},
			{10530,3729},
			{10546,3729},
			{10534,3758},
			{10535,3758},
			{10536,3758},
			{10539,3758},
			{10542,3758},
			{10545,3758},
			{10547,3758},
			{10529,3787},
			{10533,3787},
			{10537,3787},
			{10543,3787},
			{10548,3787},
			{10528,3816},
			{10531,3816},
			{10532,3816},
			{10540,3816},
			{10538,3844},
			{10541,3844},
			{10544,3844},
		};
	
	public static final int[][] FORGOTTEN_RANGERS = {
			{10320, 192, 1}, //7
			{10321, 192, 1}, //7
			{10322, 192, 1}, //7
			{10323, 192, 1}, //7
			{10324, 422, 2}, //15
			{10325, 422, 2}, //15
			{10326, 422, 2}, //15
			{10327, 422, 2}, //15
			{10328, 681, 3}, //24
			{10329, 681, 3}, //24
			{10330, 681, 3}, //24
			{10331, 681, 3}, //24
			{10332, 997, 4}, //35
			{10333, 997, 4}, //35
			{10334, 997, 4}, //35
			{10335, 997, 4}, //35
			{10336, 1285, 5}, //45
			{10337, 1285, 5}, //45
			{10338, 1285, 5}, //45
			{10339, 1285, 5}, //45
			{10340, 1544, 6}, //54
			{10341, 1544, 6}, //54
			{10342, 1544, 6}, //54
			{10343, 1544, 6}, //54
			{10344, 1831, 7}, //64
			{10345, 1831, 7}, //64
			{10346, 1831, 7}, //64
			{10347, 1831, 7}, //64
			{10348, 2148, 8}, //75
			{10349, 2148, 8}, //75
			{10350, 2148, 8}, //75
			{10351, 2148, 8}, //75
			{10352, 2407, 9}, //84
			{10353, 2407, 9}, //84
			{10354, 2407, 9}, //84
			{10355, 2407, 9}, //84
			{10356, 2752, 10}, //96
			{10357, 2752, 10}, //96
			{10358, 2752, 10}, //96
			{10359, 2752, 10}, //96
			{10360, 3269, 11}, //114
			{10361, 3269, 11}, //114
			{10362, 3269, 11}, //114
			{10363, 3269, 11}, //114
	};
	
	public static final int[][] FORGOTTEN_MAGERS = {
			{10560, 135, 1}, //5
			{10561, 163, 1}, //6
			{10562, 163, 1, 9}, //6
			{10563, 135, 1, 9}, //5 
			{10564, 393, 2}, //14
			{10565, 365, 2}, //13
			{10566, 393, 2, 9}, //14
			{10567, 422, 2, 9}, //15
			{10568, 710, 3}, //25
			{10569, 710, 3}, //25
			{10570, 681, 3, 9}, //24
			{10571, 681, 3, 9}, //24
			{10572, 882, 4}, //31
			{10573, 969, 4}, //34
			{10574, 940, 4, 9}, //33
			{10575, 1026, 4, 9}, //36
			{10576, 1256, 5}, //44
			{10577, 1285, 5}, //45
			{10578, 1256, 5, 9}, //44
			{10579, 1285, 5, 9}, //45
			{10580, 1572, 6}, //55
			{10581, 1544, 6}, //54
			{10582, 1515, 6, 9}, //53
			{10583, 1572, 6, 9}, //55
			{10584, 1831, 7}, //64
			{10585, 1831, 7}, //64
			{10586, 1860, 7, 8}, //65
			{10587, 1831, 7, 8}, //64
			{10588, 2119, 8}, //74
			{10589, 2090, 8}, //73
			{10590, 2148, 8, 8}, //75
			{10591, 2176, 8, 8}, //76
			{10592, 2349, 9}, //82
			{10593, 2407, 9}, //84
			{10594, 2407, 9, 8}, //84
			{10595, 2435, 9, 8}, //85
			{10596, 2694, 10}, //94
			{10597, 2723, 10}, //95
			{10598, 2723, 10, 8}, //95
			{10599, 2694, 10, 8}, //94
			{10600, 3241, 11}, //113
			{10601, 3269, 11}, //114
			{10602, 3298, 11, 8}, //115
			{10603, 3269, 11, 8}, //114
	};
	
	public static final int[][] NORMAL_GIANT_RATS = {
			{10480, 48}, //2
			{10481, 135}, //5
			{10482, 307}, //11
			{10483, 623}, //22
			{10484, 1256}, //44
			{10485, 2033}, //71
			{10486, 2752}, //96
	};
	
	public static final int[][] MYSTERIOUS_SHADES = { //attack anims and spells not done
			{10831, 20}, //1
			{10832, 48}, //2
			{10833, 106}, //4
			{10834, 278}, //10
			{10835, 480}, //17
			{10836, 710}, //25
			{10837, 1055}, //37
			{10838, 1429}, //50
			{10839, 1918}, //67
			{10840, 2579}, //90
			{10841, 3183}, //111
			{10842, 3816}, //133
	};
	
	//frozen
	public static final int[][] ICE_WARRIORS = {
			{10225, 48, 1}, //2
			{10226, 135, 1}, //5
			{10227, 508, 2}, //18
			{10228, 911, 3}, //32
			{10229, 1314, 4}, //46
			{10230, 1774, 5}, //62
			{10231, 2119, 6}, //74
			{10232, 2550, 7}, //89
			{10233, 2953, 8}, //103
			{10234, 3384, 9}, //118
			{10235, 3729, 10}, //130
	};
	
	public static final int[][] ICE_SPIDERS = {
			{10157, 48}, //2
			{10158, 77}, //3
			{10159, 192}, //7
			{10160, 336}, //12
			{10161, 480}, //17
			{10162, 623}, //22
			{10163, 825}, //29
			{10164, 1055}, //37
			{10165, 1429}, //50
			{10166, 1803}, //63
			{10167, 2263}, //79
	};
	
	public static final int[][] THROWER_TROLLS = {
			{10791, 1199}, //42
			{10792, 1515}, //53
			{10793, 2004}, //70
			{10794, 2579}, //90
			{10795, 3154}, //110
			{10796, 3787}, //132
	};
	
	public static final int[][] ICE_TROLLS = {
			{10782, 1716}, //60
			{10783, 1774}, //62
			{10784, 2033}, //71
			{10785, 2119}, //74
			{10786, 2723}, //95
			{10787, 3010}, //105
			{10788, 3499}, //122
			{10789, 3902}, //136
			{10790, 4247}, //148
	};
	
	public static final int[][] ICE_GIANTS = {
			{10762, 1055}, //37
			{10763, 1831}, //64
			{10764, 2464}, //86
			{10765, 3269}, //114
			{10766, 4276}, //149
			{10767, 5484}, //191
			{10768, 6807}, //237
			{10769, 8158}, //284
	};
	
	public static final int[][] ICE_ELEMENTALS = {	//attack anims and spells not done		
			{10460, 566}, //20
			{10461, 1026}, //36
			{10462, 1745}, //61
			{10463, 2579}, //90
			{10464, 3413}, //119
			{10465, 4017}, //140
			{10466, 4621}, //161
			{10467, 5340}, //186
			{10468, 6174}, //215
	};
	
	public static final int[][] ICEFIENDS = { //attack anims and spells not done
			{10212, 336}, //12
			{10213, 710}, //25
			{10214, 1055}, //37
			{10215, 1803}, //63
			{10216, 2522}, //88
			{10217, 3672}, //128
			{10218, 4592}, //160
	};
	
	public static final int[][] HYDRA = {
			{10610, 307}, //11
			{10611, 451}, //16
			{10612, 623}, //22
			{10613, 940}, //33
			{10614, 1285}, //45
			{10615, 1831}, //64
			{10616, 2378}, //83
			{10617, 3154}, //110
			{10618, 3787}, //132
	};
	
	public static final int[][] FROST_DRAGONS = {
		{10770, 2407}, //84
		{10771, 3097}, //108
		{10772, 3787}, //132
		{10773, 4765}, //166
		{10774, 5944}, //207
		{10775, 7296}, //254
	};
	
	//abandoned
	public static final int[][] EARTH_WARRIORS = {
		{10178, 106}, //4
		{10179, 451}, //16
		{10180, 854}, //30
		{10181, 1256}, //44
		{10182, 1688}, //59
		{10183, 2061}, //72
		{10184, 2435}, //85
		{10185, 2838}, //99
		{10186, 3269}, //114
		{10187, 3672}, //128
	};

	public static final int[][] DUNGEON_SPIDERS = {
			{10496, 48}, //2
			{10497, 163}, //6
			{10498, 250}, //9
			{10499, 422}, //15
			{10500, 537}, //19
			{10501, 710}, //25
			{10502, 997}, //35
			{10503, 1342}, //47
			{10504, 1688}, //59
			{10505, 2119}, //74
	};
	
	public static final int [][] SKELETAL_MINIONS = {
			{11722, 710}, //25
			{11723, 1026}, //36
			{11724, 1486}, //52
			{11725, 1803}, //63
			{11726, 2004}, //70
			{11727, 2176}, //76
			{11728, 2320}, //81
			{11729, 2435}, //85
			{11730, 2550}, //89
			{11731, 2637}, //92
			{11732, 2752}, //96
			{11733, 2809}, //98
			{11734, 2895}, //101
			{11735, 2953}, //103
			{11736, 3039}, //
	};
 	
	public static final int [][] SKELETONS_UNARMED = {
			{10629, 422, 2}, //15
			{10630, 422, 2}, //15
			{10633, 652, 3}, //23
			{10637, 940, 4}, //33
			{10643, 1199, 5}, //42
			{10646, 1659, 6}, //58
	};
	public static final int [][] SKELETONS_FLAIL = {
			{10634, 652, 3}, //23
			{10644, 1199, 5}, //42
			{10662, 4420, 11}, //154
			{10666, 5110, 11}, //178
	};
	public static final int [][] SKELETONS_LONGSWORD = {
			{10631, 422, 2, 10}, //15
			{10636, 652, 3, 10}, //23
			{10641, 940, 4, 10}, //33
			{10648, 1659, 6, 10}, //58
			{10650, 2148, 8, 10}, //75
			{10652, 2148, 8, 10}, //75
			{10656, 2780, 9, 10}, //97
			{10658, 3499, 10, 10}, //122
	};
	public static final int [][] SKELETONS_HATCHET = {
			{10632, 422, 2, 11}, //15
			{10635, 652, 3, 11}, //23
			{10647, 1659, 6, 11}, //58
			{10651, 2148, 8, 11}, //75
			{10655, 2780, 9, 11}, //97
			{10660, 3499, 10, 11}, //122
			{10665, 4420, 11, 11}, //154
			{10669, 5110, 11, 11}, //178
	};
	public static final int [][] SKELETONS_WARHAMMER = {
			{10638, 940, 4, 12}, //33
			{10639, 940, 4, 12}, //33
			{10649, 1659, 6, 12}, //58
			{10653, 2148, 8, 12}, //75
			{10657, 2780, 9, 12}, //97
			{10661, 3499, 10, 12}, //122
			{10664, 4420, 11, 12}, //154
			{10668, 5110, 11, 12}, //178
	};
	public static final int [][] SKELETONS_RAPIER = {
			{10640, 940, 4}, //33
			{10642, 1199, 5}, //42
			{10645, 1199, 5}, //42
			{10654, 2780, 9}, //97
			{10659, 3499, 10}, //122
			{10663, 4420, 11}, //154
			{10667, 5110, 11}, //178
	};
	public static final int[][] SKELETONS_MELEE = {
			{10629, 422}, //15
			{10630, 422}, //15
			{10631, 422}, //15
			{10632, 422}, //15
			{10635, 652}, //23
			{10633, 652}, //23
			{10636, 652}, //23
			{10634, 652}, //23
			{10638, 940}, //33
			{10639, 940}, //33
			{10640, 940}, //33
			{10637, 940}, //33
			{10641, 940}, //33
			{10644, 1199}, //42
			{10643, 1199}, //42
			{10642, 1199}, //42
			{10645, 1199}, //42
			{10646, 1659}, //58
			{10647, 1659}, //58
			{10649, 1659}, //58
			{10648, 1659}, //58
			{10650, 2148}, //75
			{10652, 2148}, //75
			{10651, 2148}, //75
			{10653, 2148}, //75
			{10656, 2780}, //97
			{10655, 2780}, //97
			{10657, 2780}, //97
			{10654, 2780}, //97
			{10658, 3499}, //122
			{10660, 3499}, //122
			{10661, 3499}, //122
			{10659, 3499}, //122
			{10665, 4420}, //154
			{10663, 4420}, //154
			{10664, 4420}, //154
			{10662, 4420}, //154
			{10669, 5110}, //178
			{10666, 5110}, //178
			{10668, 5110}, //178
			{10667, 5110}, //178
	};
	
	public static final int[][] SKELETONS_RANGE = {
			{10670, 365}, //13
			{10671, 537}, //19
			{10672, 738}, //26
			{10673, 1055}, //37
			{10674, 1371}, //48
			{10675, 1716}, //60
			{10676, 2061}, //72
			{10677, 2435}, //85
			{10678, 2838}, //99
			{10679, 3183}, //111
			{10680, 3442}, //120
			{10681, 3902}, //136
	};
	
	public static final int[][] SKELETONS_MAGE = {
			{10682, 307}, //11
			{10683, 480}, //17
			{10684, 738}, //26
			{10685, 969}, //34
			{10686, 1199}, //42
			{10687, 1457}, //51
			{10688, 1831}, //64
			{10689, 2205}, //77
			{10690, 2435}, //85
			{10691, 2752}, //96
			{10692, 3327}, //116
			{10693, 3873}, //135
	};
	
	public static final int[][] ZOMBIES = {
			{10364, 106, 1}, //4
			{10365, 250, 1}, //9
			{10366, 508, 2}, //18
			{10367, 940, 4}, //33
			{10368, 1342, 5}, //47
			{10369, 1831, 6}, //64
			{10370, 2378, 7}, //83
			{10371, 2953, 8}, //103
			{10372, 3471, 9}, //121
			{10373, 3931, 10}, //137
			{10374, 4362, 10}, //152
	};
			
	public static final int[][] ZOMBIES_RANGE = {
			{10375, 77, 1}, //3
			{10376, 192, 1}, //7
			{10377, 393, 2}, //14
			{10378, 681, 3}, //24
			{10379, 1026, 4}, //36
			{10380, 1429, 5}, //50
			{10381, 1918, 6}, //67
			{10382, 2378, 7}, //83
			{10383, 2867, 8}, //100
			{10384, 3298, 9}, //115
			{10385, 3672, 10}, //128
	};
	
	public static final int[][] GREEN_DRAGONS = {		
			{10604, 2090}, //73
			{10605, 2780}, //97
			{10606, 3442}, //120
			{10607, 4391}, //153
			{10608, 5541}, //193
			{10609, 6893}, //240
	};
	
	public static final int[][] BATS = {
			{10906, 77}, //3
			{10907, 163}, //6
			{10908, 336}, //12
			{10909, 796}, //28
	};
	
	public static final int[][] GIANT_BATS = {
			{10910, 1141}, //40
			{10911, 2205}, //77
			{10912, 2694}, //94
			{10913, 3269}, //114
			{10914, 3672}, //128
	};
	
	public static final int[][] HILL_GIANTS = {
			{10706, 365}, //13
			{10707, 623}, //22
			{10708, 796}, //28
			{10709, 1084}, //38
			{10710, 1400}, //49
			{10711, 1774}, //62
			{10712, 2119}, //74
			{10713, 2522}, //88
			{10714, 3097}, //108
			{10715, 3931}, //137
			{10716, 4822}, //168
			{10717, 6059}, //211
	};
	
	public static final int[][] GIANT_SKELETONS = {
			{10469, 796}, //28
			{10470, 1084}, //38
			{10471, 1400}, //49
			{10472, 1659}, //58
			{10473, 1975}, //69
			{10474, 2234}, //78
			{10475, 2665}, //93
			{10476, 3241}, //113
			{10477, 3586}, //125
			{10478, 4017}, //140
			{10479, 4506}, //157
	};
	
	public static final int[][] HOBGOBLINS = {
			{10236, 336, 1}, //12
			{10237, 652, 2}, //23
			{10238, 1026, 3}, //36
			{10239, 1400, 4}, //49
			{10240, 1745, 5}, //61
			{10241, 2090, 6}, //73
			{10242, 2435, 7}, //85
			{10243, 2780, 8}, //97
			{10244, 3327, 9}, //116
			{10245, 4046, 10}, //141	
	};
	
	public static final int[][] ANIMATED_PICKAXES = {
			{10168, 192, 1}, //7
			{10169, 393, 2}, //14
			{10170, 710, 3}, //25
			{10171, 1055, 4}, //37
			{10172, 1371, 5}, //48
			{10173, 1745, 6}, //61
			{10174, 2148, 7}, //75
			{10175, 2550, 8}, //89
			{10176, 2953, 9}, //103
			{10177, 3327, 10}, //116
	};
	
	//furnished
	public static final int[][] NORMAL_GUARD_DOGS = {
			{10726, 106}, //4
			{10727, 278}, //10
			{10728, 537}, //19
			{10729, 825}, //29
			{10730, 1227}, //43
			{10731, 1774}, //62
			{10732, 2349}, //82
			{10733, 2953}, //103
			{10734, 3384}, //118
			{10735, 3758}, //131
	};

	public static final int[][] NORMAL_BRUTES = { //no novite and no kratonite going straight to fractite
			{10797, 595, 2}, //21
			{10798, 595, 2}, //21  bathus
			{10799, 997, 3}, //35  marmaros
			{10800, 997, 3}, //35
			{10801, 1400, 5}, //49 fractite 
			{10802, 1400, 5}, //49
			{10803, 1803, 6}, //63  zeyphirum
			{10804, 1803, 6}, //63
			{10805, 2148, 7}, //75  argonite
			{10806, 2148, 7}, //75
			{10807, 2550, 8}, //89 katagon
			{10808, 2550, 8}, //89
			{10809, 2982, 9}, //104  gorgonite
			{10810, 2982, 9}, //104
			{10811, 3384, 10}, //118  prom
			{10812, 3384, 10}, //118
			{10813, 4103, 10}, //143  prom
			{10814, 4103, 10}, //143
			/*{11441, 595, 2}, //21 useless shit i think
			{11442, 997, 3}, //35 
			{11443, 1400, 5}, //49
			{11444, 1803, 6}, //63
			{11445, 2148, 7}, //75
			{11446, 2550, 8}, //89
			{11447, 2982, 9}, //104 
			{11448, 3384, 10}, //118
			{11449, 4103, 10}, //143*/ 
	};
	
	public static final int[][] IRON_DRAGONS = {
			{10776, 2550}, //89
			{10777, 3269}, //114
			{10778, 3960}, //138
			{10779, 4966}, //173
			{10780, 6145}, //214
			{10781, 7554}, //263
	};
	
	//occult
	
	public static final int[][] RED_DRAGONS = {
			{10815, 2464}, //86
			{10816, 3212}, //112
			{10817, 4075}, //142
			{10818, 5340}, //186
			{10819, 6634}, //231
			{10820, 8015}, //279
	};
	
	public static final int[][] FIRE_GIANTS = {
			{10754, 1141}, //40
			{10755, 1918}, //67
			{10756, 2665}, //93
			{10757, 3471}, //121
			{10758, 4477}, //156
			{10759, 5771}, //201
			{10760, 7123}, //248
			{10761, 8503}, //296	
	};
	
	public static final int[][] ANIMATED_BOOKS = {
			{10744, 77}, //3
			{10745, 192}, //7
			{10746, 393}, //14
			{10747, 738}, //26
			{10748, 1141}, //40
			{10749, 1429}, //50
			{10750, 1803}, //63
			{10751, 2205}, //77
			{10752, 2752}, //96
			{10753, 3183}, //111	
	};
	
	public static final int[][] HELLHOUNDS = {
			{10188, 1601}, //56
			{10189, 2234}, //78
			{10190, 3068}, //107
			{10191, 3614}, //126
			{10192, 4046}, //141
			{10193, 4621}, //161
			{10194, 5024}, //175
			{10195, 5512}, //192
	};
	
	public static final int[][] LESSER_DEMONS = {
			{10492, 1601}, //56
			{10493, 2263}, //79
			{10494, 2895}, //101
			{10495, 3499}, //122	
	};
	
	public static final int[][] GREATER_DEMONS = {
			{10718, 2637}, //92
			{10719, 3614}, //126
			{10720, 4046}, //141
			{10721, 4736}, //165	
	};
	
	public static final int[][] BLACK_DEMONS = {
			{10722, 5196}, //181
			{10723, 5570}, //194
			{10724, 6030}, //210
			{10725, 6490}, //226	
	};
	
	public static final int[][] NECROMANCERS = {
			{10196, 336}, //12
			{10197, 595}, //21
			{10198, 997}, //35
			{10199, 1486}, //52
			{10200, 1975}, //69
			{10201, 2378}, //83
			{10202, 2752}, //96
			{10203, 3097}, //108
			{10204, 3384}, //118
			{10205, 3758}, //131
			{10206, 4161}, //145	
	};
	
	public static final int[][] GHOSTS = {
			/*{10821, 135}, //5 used in puzzle or something?
			{10822, 393}, //14
			{10823, 681}, //24
			{10824, 1084}, //38
			{10825, 1601}, //56
			{10826, 2263}, //79
			{10827, 3068}, //107
			{10828, 3902}, //136
			{10829, 4851}, //169
			{10830, 5714}, //199*/ 
			{10981, 135}, //5
			{10982, 135}, //5
			{10983, 393}, //14
			{10984, 393}, //14
			{10985, 681}, //24
			{10986, 681}, //24
			{10987, 1026}, //36
			{10988, 1026}, //36
			{10989, 1601}, //56
			{10990, 1601}, //56
			{10991, 2263}, //79
			{10992, 2263}, //79
			{10993, 3068}, //107
			{10994, 3068}, //107
			{10995, 3902}, //136
			{10996, 3902}, //136
			{10997, 3902}, //136
			{10998, 4851}, //169
			{10999, 5714}, //199
			{11000, 5714}, //199	
	};
	
	
	//warped 
	public static final int[][] WARPED_GIANT_RATS = {
			{12914, 48}, //2
			{12915, 135}, //5
			{12916, 307}, //11
			{12917, 623}, //22
			{12918, 1256}, //44
			{12919, 2033}, //71
			{12920, 2752}, //96
	};
	
	public static final int[][] WARPED_GUARD_DOGS = {
			{12922, 106}, //4
			{12923, 278}, //10
			{12924, 537}, //19
			{12925, 825}, //29
			{12926, 1227}, //43
			{12927, 1774}, //62
			{12928, 2349}, //82
			{12929, 2953}, //103
			{12930, 3384}, //118
			{12931, 3758}, //131
	};
	
	public static final int[][] WARPED_BRUTES = {
			{12932, 595, 2}, //21
			{12933, 997, 3}, //35
			{12934, 1400, 5}, //49
			{12935, 1803, 6}, //63
			{12936, 2148, 7}, //75
			{12937, 2550, 8}, //89
			{12938, 2982, 9}, //104
			{12939, 3384, 10}, //118
			{12940, 4103, 10}, //143
	};
	
	public static final int[][] BLACK_DRAGONS = {
			{10219, 2752}, //96
			{10220, 3471}, //121
			{10221, 4506}, //157
			{10222, 5800}, //202
			{10223, 7123}, //248
			{10224, 8705}, //303	
	};
	
	public static final int[][] REBORN_MAGES = {
			{12903, 336}, //12
			{12904, 595}, //21
			{12905, 997}, //35
			{12906, 1486}, //52
			{12907, 1975}, //69
			{12908, 2378}, //83
			{12909, 2752}, //96
			{12910, 3097}, //108
			{12911, 3384}, //118
			{12912, 3758}, //131
			{12913, 4161}, //145	
	};
	
	public static final int[][] REBORN_WARRIORS = {
			{12941, 336}, //4
			{12942, 595}, //9
			{12943, 997}, //18
			{12944, 1486}, //33
			{12945, 1975}, //47
			{12946, 2378}, //64
			{12947, 2752}, //83
			{12948, 3097}, //103
			{12949, 3384}, //121
			{12950, 3758}, //137
			{12951, 4161}, //152
	};
	
	public static final int[][] ANKOUS = {
			{10736, 336}, //12
			{10737, 566}, //20
			{10738, 940}, //33
			{10739, 1342}, //47
			{10740, 1831}, //64
			{10741, 2550}, //89
			{10742, 3298}, //115
			{10743, 4103}, //143
	};
	
	public static List<int[][]> frozenNpcs = new ArrayList<>();
	
	public static List<int[][]> abandonedNpcs = new ArrayList<>();
	
	public static List<int[][]> furnishedNpcs = new ArrayList<>();
	
	public static List<int[][]> occultNpcs = new ArrayList<>();
	
	public static List<int[][]> warpedNpcs = new ArrayList<>();
	
	static {
		frozenNpcs.add(FORGOTTEN_RANGERS);
		frozenNpcs.add(FORGOTTEN_MAGERS);
		frozenNpcs.add(FORGOTTEN_WARRIORS);
		frozenNpcs.add(NORMAL_GIANT_RATS);
		frozenNpcs.add(MYSTERIOUS_SHADES);
		frozenNpcs.add(ICE_WARRIORS);
		frozenNpcs.add(ICE_SPIDERS);
		frozenNpcs.add(THROWER_TROLLS);
		frozenNpcs.add(ICE_TROLLS);
		frozenNpcs.add(ICE_GIANTS);
		frozenNpcs.add(ICE_ELEMENTALS);
		frozenNpcs.add(ICEFIENDS);
		frozenNpcs.add(HYDRA);
		frozenNpcs.add(FROST_DRAGONS);
		
		abandonedNpcs.add(FORGOTTEN_RANGERS);
		abandonedNpcs.add(FORGOTTEN_MAGERS);
		abandonedNpcs.add(FORGOTTEN_WARRIORS);
		abandonedNpcs.add(NORMAL_GIANT_RATS);
		abandonedNpcs.add(MYSTERIOUS_SHADES);
		abandonedNpcs.add(EARTH_WARRIORS);
		abandonedNpcs.add(DUNGEON_SPIDERS);
		abandonedNpcs.add(GREEN_DRAGONS);
		abandonedNpcs.add(BATS);
		abandonedNpcs.add(GIANT_BATS);
		abandonedNpcs.add(HILL_GIANTS);
		abandonedNpcs.add(GIANT_SKELETONS);
		abandonedNpcs.add(HOBGOBLINS);
		abandonedNpcs.add(ANIMATED_PICKAXES);
		abandonedNpcs.add(ZOMBIES);
		abandonedNpcs.add(ZOMBIES_RANGE);
		abandonedNpcs.add(SKELETONS_MELEE);
		abandonedNpcs.add(SKELETONS_MAGE);
		abandonedNpcs.add(SKELETONS_RANGE);
		
		furnishedNpcs.add(FORGOTTEN_RANGERS);
		furnishedNpcs.add(FORGOTTEN_MAGERS);
		furnishedNpcs.add(FORGOTTEN_WARRIORS);
		furnishedNpcs.add(NORMAL_GIANT_RATS);
		furnishedNpcs.add(MYSTERIOUS_SHADES);
		furnishedNpcs.add(NORMAL_GUARD_DOGS);
		furnishedNpcs.add(DUNGEON_SPIDERS);
		furnishedNpcs.add(NORMAL_BRUTES);
		furnishedNpcs.add(IRON_DRAGONS);
		furnishedNpcs.add(BATS);
		furnishedNpcs.add(GIANT_BATS);
		
		occultNpcs.add(FORGOTTEN_RANGERS);
		occultNpcs.add(FORGOTTEN_MAGERS);
		occultNpcs.add(FORGOTTEN_WARRIORS);
		occultNpcs.add(NORMAL_GIANT_RATS);
		occultNpcs.add(MYSTERIOUS_SHADES);
		occultNpcs.add(DUNGEON_SPIDERS);
		occultNpcs.add(SKELETONS_MELEE); 
		occultNpcs.add(SKELETONS_MAGE);
		occultNpcs.add(SKELETONS_RANGE);
		occultNpcs.add(ZOMBIES); 
		occultNpcs.add(ZOMBIES_RANGE);
		occultNpcs.add(RED_DRAGONS);
		occultNpcs.add(BATS);
		occultNpcs.add(GIANT_BATS);
		occultNpcs.add(FIRE_GIANTS);
		occultNpcs.add(ANIMATED_BOOKS);
		occultNpcs.add(HELLHOUNDS);
		occultNpcs.add(LESSER_DEMONS);
		occultNpcs.add(GREATER_DEMONS);
		occultNpcs.add(BLACK_DEMONS);
		occultNpcs.add(NECROMANCERS);
		occultNpcs.add(GHOSTS);
		
		warpedNpcs.add(FORGOTTEN_RANGERS);
		warpedNpcs.add(FORGOTTEN_MAGERS);
		warpedNpcs.add(FORGOTTEN_WARRIORS);
		warpedNpcs.add(WARPED_GIANT_RATS);
		warpedNpcs.add(MYSTERIOUS_SHADES);
		warpedNpcs.add(SKELETONS_MELEE); 
		warpedNpcs.add(SKELETONS_MAGE);
		warpedNpcs.add(SKELETONS_RANGE);
		warpedNpcs.add(ZOMBIES); 
		warpedNpcs.add(ZOMBIES_RANGE);
		warpedNpcs.add(BLACK_DRAGONS);
		warpedNpcs.add(REBORN_MAGES);
		warpedNpcs.add(ANIMATED_BOOKS);
		warpedNpcs.add(ANIMATED_PICKAXES);
		warpedNpcs.add(WARPED_GUARD_DOGS);
		warpedNpcs.add(WARPED_BRUTES);
		warpedNpcs.add(LESSER_DEMONS);
		warpedNpcs.add(GREATER_DEMONS);
		warpedNpcs.add(BLACK_DEMONS);
		warpedNpcs.add(ANKOUS);
	}

	public static List<int[][]> pickNpcs(int floorType) {
		switch(floorType) {
			case DungeonConstants.FROZEN_FLOORS:
				return frozenNpcs;
			case DungeonConstants.ABANDONED_FLOORS:
				return abandonedNpcs;
			case DungeonConstants.FURNISHED_FLOORS:
				return furnishedNpcs;
			case DungeonConstants.OCCULT_FLOORS:
				return occultNpcs;
			case DungeonConstants.WARPED_FLOORS:
				return warpedNpcs;
		}

		return null;
	}

}