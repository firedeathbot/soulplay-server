package com.soulplay.content.player.skills.dungeoneeringv2.skills.magic;

public class SpellsConstants {

	public static final int air = 17780;
	public static final int mind = 17784;
	public static final int water = 17781;
	public static final int earth = 17782;
	public static final int fire = 17783;
	public static final int body = 17788;
	public static final int cosmic = 17789;
	public static final int chaos = 17785;
	public static final int astral = 17790;
	public static final int nature = 17791;
	public static final int law = 17792;
	public static final int death = 17786;
	public static final int blood = 17787;
	public static final int soul = 17793;

}
