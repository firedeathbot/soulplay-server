package com.soulplay.content.player.skills.dungeoneeringv2.items.shop;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.npcs.impl.bosses.BossKCEnum;
import com.soulplay.content.player.pets.pokemon.PokemonData;

public enum DungeonRewardData {

	EXPERIENCE_LAMP(15389, 10_000, 181186, true),
	BONECRUSHER(18337, 34_000, 181190, true),
	HERBICIDE(19675, 34_000, 181194, false),
	CHARMING_IMP(9952, 100_000, 181198, true),
	SPIRIT_CAPE(19893, 45_000, 181202, true),
	ARCANE_PULSE_NECK(18333, 6_500, 181206, true),
	TWISTED_BIRD_SKULL_NECK(19886, 8_500, 181210, false),
	LAW_STAFF(18342, 10_000, 181214, true),
	NATURE_STAFF(18341, 12_500, 181218, true),
	GRAVITE_RAPIER(18365, 40_000, 181222, true),
	GRAVITE_LONGSWORD(18367, 40_000, 181226, true),
	GRAVITE_2H_SWORD(18369, 40_000, 181230, true),
	GRAVITE_STAFF(18371, 40_000, 181234, true),
	GRAVITE_SHORTBOW(18373, 40_000, 181238, true),
	TOME_OF_FROST(18346, 43_000, 181242, true),
	AMULET_OF_ZEALOTS(19892, 40_000, 181246, false),
	SCROLL_OF_CLEANSING(19890, 20_000, 181250, false),
	ARCANE_BLAST_NECK(18334, 15_000, 181254, true),
	SPLIT_DRAGONTOOTH_NECK(19887, 17_000, 182002, false),
	ANTI_POISON_TOTEM(18340, 44_000, 182006, true),
	RING_OF_VIGOUR(19669, 44_000, 182010, true),
	SCROLL_OF_RENEWAL(18343, 38_000, 182014, false),
	MERCENARY_GLOVES(18347, 48_500, 182018, true),
	ARCANE_STEAM_NECK(18335, 150_000, 182022, true),
	CHAOTIC_RAPIER(18349, 200_000, 182026, true),
	CHAOTIC_LONGSWORD(18351, 200_000, 182030, true),
	CHAOTIC_MAUL(18353, 200_000, 182034, true),
	CHAOTIC_STAFF(18355, 200_000, 182038, true),
	CHAOTIC_CROSSBOW(18357, 200_000, 182042, true),
	CHAOTIC_KITESHIELD(18359, 200_000, 182046, true),
	EAGLE_EYE_KITESHIELD(18361, 200_000, 182050, true),
	FARSEER_KITESHIELD(18363, 200_000, 182054, true),
	DEMON_HORN_NECK(19888, 35_000, 182058, false),
	SNEAKERPEEPER_SPAWN(19894, 85_000, 182062, true),

	GLUTTONOUS_BEHEMOTH(9948, 475_000, 182066, true, PokemonData.GLUTTONOUS_BEHEMOTH, BossKCEnum.GLUTTONOUS_BEHEMOTH),
	ASTEA_FROSTWEB(9965, 475_000, 182070, true, PokemonData.ASTEA_FROSTWEB, BossKCEnum.ASTEA_FROSTWEB),
	ICY_BONES(10040, 500_000, 182074, true, PokemonData.ICY_BONES, null),
	LUMINESCENT(9912, 500_000, 182078, true, PokemonData.LUMINESCENT, null),
	LAKHRAHNAZ(9929, 500_000, 182082, true, PokemonData.LAKHRAHNAZ, null),
	TO_KASH(10024, 600_000, 182086, true, PokemonData.TO_KASH, null),
	DIVING_SKINWEAVER(10058, 600_000, 182090, true, PokemonData.DIVING_SKINWEAVER, null),
	GEOMANCER(10059, 500_000, 182094, true, PokemonData.GEOMANCER, null),
	BULWARK_BEAST(10073, 600_000, 182098, true, PokemonData.BULWARK_BEAST, BossKCEnum.BULWARK_BEAST),
	UNHOLY_CURSEBEARER(10111, 600_000, 182102, true, PokemonData.UNHOLY_CURSEBEARER, null),
	RAMMERNAUT(9767, 600_000, 182106, true, PokemonData.RAMMERNAUT, null),
	HAR_LAKK(9898, 650_000, 182110, true, PokemonData.HAR_LAKK, null),
	LEXICUS(9842, 550_000, 182114, true, PokemonData.LEXICUS, null),
	SAGITTARE(9753, 550_000, 182118, true, PokemonData.SAGITTARE, BossKCEnum.SAGITTARE),
	BAL_LAK(10128, 700_000, 182122, true, PokemonData.BAL_LAK, null),
	RUNEBOUND(11752, 700_000, 182126, true, PokemonData.RUNEBOUND, null),
	GRAVECREEPER(11708, 600_000, 182130, true, PokemonData.GRAVECREEPER, null),
	NECROLORD(11737, 700_000, 182134, true, PokemonData.NECROLORD, BossKCEnum.NECROLORD),
	HAASGHENAHK(11895, 850_000, 182138, true, PokemonData.HAASGHENAHK, null),
	YK_LAGOR(11872, 850_000, 182142, true, PokemonData.YK_LAGOR, null),
	BLINK(12865, 850_000, 182146, true, PokemonData.BLINK, null),
	WARPED_GULEGA(12737, 900_000, 182150, true, PokemonData.WARPED_GULEGA, null),
	DREADNAUT(12848, 900_000, 182154, true, PokemonData.DREADNAUT, BossKCEnum.DREADNAUT),
	HOPE_DEVOURER(12886, 900_000, 182158, true, PokemonData.HOPE_DEVOURER, null),
	SHUKARHAZH(12478, 1_000_000, 182162, true, PokemonData.SHUKARHAZH, null),
	KAL_GER(12841, 1_000_000, 182166, true, PokemonData.KAL_GER, BossKCEnum.KALGER_WARMONGER);

	public static final Map<Integer, DungeonRewardData> values = new HashMap<>();
	private final int productId, price, buttonId;
	private final boolean canPurchase;
	private final PokemonData pokemonData;
	private final BossKCEnum kcData;

	private DungeonRewardData(int productId, int price, int buttonId, boolean canPurchase) {
		this(productId, price, buttonId, canPurchase, null, null);
	}

	private DungeonRewardData(int productId, int price, int buttonId, boolean canPurchase, PokemonData pokemonData, BossKCEnum kcData) {
		this.productId = productId;
		this.price = price;
		this.buttonId = buttonId;
		this.canPurchase = canPurchase;
		this.pokemonData = pokemonData;
		this.kcData = kcData;
	}

	public int getProductId() {
		return productId;
	}

	public int getPrice() {
		return price;
	}

	public int getButtonId() {
		return buttonId;
	}

	public boolean canPurchase() {
		return canPurchase;
	}

	public PokemonData getPokemonData() {
		return pokemonData;
	}

	public BossKCEnum getKCData() {
		return kcData;
	}

	static {
		for (DungeonRewardData data : values()) {
			values.put(data.getButtonId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
