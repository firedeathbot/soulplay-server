package com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting;

import java.util.HashMap;
import java.util.Map;

public enum Spinning {

	SALVE_NETTLES(1, 6, 17448, 17468),
	WILDERCRESS(10, 9, 17450, 17470),
	BLIGHTLEAF(20, 12, 17452, 17472),
	ROSEBLOOD(30, 17, 17454, 17474),
	BRYLL(40, 23, 17456, 17476),
	DUSKWEED(50, 31, 17458, 17478),
	SOULBELL(60, 42, 17460, 17480),
	ECTOGRASS(70, 55, 17462, 17482),
	RUNELEAF(80, 72, 17464, 17484),
	SPIRITBLOOM(90, 94, 17466, 17486);

	public static final Map<Integer, Spinning> values = new HashMap<>();
	private final int lvl, xp, plantId, productId;

	private Spinning(int lvl, int xp, int plantId, int productId) {
		this.lvl = lvl;
		this.xp = xp;
		this.plantId = plantId;
		this.productId = productId;
	}

	public int getLvl() {
		return lvl;
	}

	public int getXp() {
		return xp;
	}

	public int getPlantId() {
		return plantId;
	}

	public int getProductId() {
		return productId;
	}

	static {
		for (Spinning data : values()) {
			values.put(data.getPlantId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
