package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Priority;
import com.soulplay.util.Misc;

@BossMeta(ids = {10059, 10060, 10061, 10062, 10063, 10064, 10065, 10066, 10067, 10068, 10069, 10070, 10071, 10072})
public class Geomancer extends Boss {

	public Geomancer(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		if ((Integer) npc.dungVariables[0] == 1 && npc.getStopWatch().complete()) {
			npc.attackType = 5;
			npc.dungVariables[0] = 2;
			npc.startAnimation(12991);
			npc.startGraphic(Graphic.create(1576));//1577 end
			npc.getStopWatch().startTimer(3);
		} else if ((Integer) npc.dungVariables[0] == 2 && npc.getStopWatch().complete()) {
			boolean teleportAway = (boolean) npc.dungVariables[1];
			if (teleportAway) {
				Location location = Misc.getFreeTile(npc.getCurrentLocation(), 6);
				NPCHandler.trueTeleport(npc, location.getX(), location.getY(), location.getZ());
			} else {
				Direction random = Direction.getRandomNoDiagonal();
				NPCHandler.trueTeleport(npc, (int) npc.dungVariables[2] + random.getStepX(), (int) npc.dungVariables[3] + random.getStepY(), npc.heightLevel);
			}
			
			npc.resetAnimations();
			npc.startGraphic(Graphic.create(Priority.HIGH, 1577, GraphicType.LOW));
			npc.setStunned(false);
			npc.setCanWalk(true);
			npc.stopAttack = false;
			npc.dungVariables[0] = 0;
		}
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int random = Misc.random(40);
		if (random >= 36) {
			//TODO multi attack when we get dg in 2018
			if (CombatPrayer.isAnyPrayerActive(c)) {
				npc.getSkills().heal(1+Misc.random(10));//TODO healing
				CombatPrayer.drainPrayerByHit(c, 1+Misc.random(10)); //c.getSkills().decrementPrayerPoints(Misc.random(10));//TODO how much should we drain his prayer
			}
			c.startGraphic(Graphic.create(2147, GraphicType.HIGH));
			c.dealDamage(new Hit(Misc.random(10), 0, -1));//TODO is dmg based on npc or fixed
			
			npc.attackType = 2;
			npc.projectileId = 2368;
			npc.startGraphic(Graphic.create(2147, GraphicType.HIGH));

			CombatPrayer.resetPrayers(c);
			c.sendMessage("Your prayers have been disabled.");
			//TODO figure out if he actually disabled prayers cus couldn't find
			npc.attackType = 5;
		} else if (random >= 30) {
			if (c.applyFreeze(12)) {
				//TODO bind gfx
				c.startGraphic(Graphic.create(180, GraphicType.HIGH));
				c.sendMessage("You have been snared.");
			}
			
			npc.startAnimation(12992);
			npc.startGraphic(Graphic.create(177, GraphicType.HIGH));
			npc.attackType = 2;
			npc.projectileId = 178;
			npc.endGfx = 179;
			npc.dungVariables[0] = 1;
			npc.getStopWatch().startTimer(4);
			int distance = c.distanceToPoint(npc.absX, npc.absY);
			if (distance > 1) {
				npc.dungVariables[1] = false;
				npc.dungVariables[2] = c.absX;
				npc.dungVariables[3] = c.absY;
			} else {
				npc.dungVariables[1] = true;
			}

			npc.setStunned(true);
			npc.setCanWalk(false);
			npc.stopAttack = true;
		} else if (random >= 25) {////TODO curse spell gfx
			int random2 = Misc.random(3);
			if (random2 == 0) {//confuse
				c.getSkills().drainLevel(Skills.ATTACK, 0.05, 1);
				npc.attackType = 2;
				npc.projectileId = 1884;
			} else if (random2 == 1) {//weaken
				c.getSkills().drainLevel(Skills.STRENGTH, 0.05, 1);
				npc.attackType = 2;
				npc.projectileId = 1884;
			} else {//curse
				c.getSkills().drainLevel(Skills.DEFENSE, 0.05, 1);
				npc.attackType = 2;
				npc.projectileId = 1884;
			}
		} else {
			if (Misc.random(2) == 1 && c.getPA().withinDistanceToNpc(npc, 1)) {
				npc.attackType = 0;
			} else {
				//TODO correct gfx and multi
				npc.attackType = 2;
				npc.projectileId = 1884;
			}
		}
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		npc.dungVariables[0] = new Integer(0);
		npc.dungVariables[1] = new Boolean(false);
		npc.dungVariables[2] = new Integer(0);
		npc.dungVariables[3] = new Integer(0);
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}

	public int followDistance() {
		return 0;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}
	
}
