package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import com.soulplay.game.model.player.Client;

public class DungeoneeringTutorDialogue {
	
	public static void dungTutorChat(Client c, int npcType) {
		c.getDialogueBuilder().sendNpcChat("Greetings, adventurer.");
		
		if (!c.getItems().bankHasItemAmount(15707, 1) && !c.getItems().playerHasItem(15707)) {
			c.getDialogueBuilder()
			.sendNpcChat("Before we carry on, let me give you this.").sendAction(()-> {
				c.getItems().addItem(15707, 1);
			}).sendItemStatement(15707, "He hands you a ring.");
		}
		
		checkPoint1(c);
		
	}
	
	public static void checkPoint1(Client c) {
		c.getDialogueBuilder().createCheckpoint(0).sendOption("What is this place?", ()-> {
			whatIsThisPlace(c);
		}, "What can I do here?", ()-> {
			whatCanIDoHere(c);
		}, "What does this ring do?", ()-> {
			ringChat(c);
		}, "Show me the journals I've found.", ()-> {
			showJournal(c);
		}, "About the Task System...", ()-> {
			taskSystemChat(c);
		}).setClose(false).executeIfNotActive();
	}
	
	private static void daemonheimChat(Client c) {
		c.getDialogueBuilder().sendPlayerChat("Daemonheim?")
		.sendNpcChat("Yes. It resembles the Niflheim of our stories: The Halls of", "the Dead.")
		.sendNpcChat("The creatures within, however, are very much alive...")
		.sendNpcChat("...unlike my fallen brothers and sisters.")
		.sendAction(()-> {
			c.getDialogueBuilder().returnToCheckpoint(0);
	    });
	}
	
	private static void whatCanIDoHere(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What can I do here?")
		.sendNpcChat("Beneath these ruins you will find a multitude of dungeons,", "filled with strange creatures and resources.")
		.sendNpcChat("Unfortunately, due to the taint that permeates this place,", "we cannot risk you taking items in or out of Daemonheim.")
		.sendOption("How will I survive without kit?", ()-> {
			surviveWithoutKit(c);
		}, "Why can't I take my kit in?", ()-> {
			takeMyKitIn(c);
		}, "Daemoheim?", ()-> {
			daemonheimChat(c);
		}, "Back...", ()-> {
			c.getDialogueBuilder().returnToCheckpoint(0);
		});
	}
	
	private static void takeMyKitIn(Client c) {
		c.getDialogueBuilder().sendPlayerChat("Why can't I take my kit in?")
		.sendNpcChat("Within Daemonheim there is a taint. The metal, wood,", "even the plantlife are not of this world. We cannot risk", "exposing the surface to them.")
		.sendNpcChat("Equally, if we allowed you to take items inside, we could", "not allow you to return with them.")
		.sendNpcChat("For this reason, our seers have erected a barrier. Nothing", "in, nothing out without express permission.")
		.sendOption("How will I survive without kit?", ()-> {
			surviveWithoutKit(c);
		}, "Daemonheim?", ()-> {
			daemonheimChat(c);
		}, "Back...", ()-> {
			c.getDialogueBuilder().returnToCheckpoint(0);
		});
	}
	
	private static void surviveWithoutKit(Client c) {
		c.getDialogueBuilder().sendPlayerChat("How will I survive without kit?")
		.sendNpcChat("When we were within Daemonheim, we found a number of", "unknown resources. You can use your skills to fashion", "them into armour and weapons to keep your party alive.")
		.sendNpcChat("Our contact within the dungeon can give you guidance on", "each of the items you find. He might even give you some", "gear to start you off.")
		.sendOption("Why can't I take my kit in?", ()-> {
			takeMyKitIn(c);
		}, "Daemonheim?", ()-> {
			daemonheimChat(c);
		}, "Back...", ()-> {
			c.getDialogueBuilder().returnToCheckpoint(0);
		});
	}
	
	private static void whatIsThisPlace(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What is this place?")
		.sendNpcChat("This is a place of treasures, fierece battles and bitter", "defeats.")
		.sendNpcChat("We fought our way into the dungeons beneath this place.")
		.sendNpcChat("Those of us who made it out alive...")
		.sendNpcChat("...called this place Daemonheim.")
		.sendOption("Daemonheim?", ()-> {
			daemonheimChat(c);
		}, "What can I do here?", ()-> {
			whatCanIDoHere(c);
		}, "Show me the journals I've found.", ()-> {
			showJournal(c);
		}, "About the Task System...", ()-> {
			taskSystemChat(c);
		});
	}
	
	private static void showJournal(Client c) {
		c.getDialogueBuilder().sendPlayerChat("Show me the joirnals I've found.")
		.sendNpcChat("There is no journals within Daemonheim you fool.");
	}
	
	public static void taskSystemChat(Client c) {
		c.getDialogueBuilder().sendOption("Tell me about the Task System.", ()-> {
			c.getDialogueBuilder().sendNpcChat("Task System is not available at the moment, sorry.");
		}, "Sorry, I was just leaving.", ()-> {});
		//c.getDialogueBuilder().sendNpcChat("Very well: the Task System is a collection of deeds you", "may wish to complete while adventuring around", "Soulplay.")
		//.sendNpcChat("You can earn special rewards for completing Tasks; at", "the very least, each is worth a cast bounty from Explorer", "Jack in Lumbridge.")
		//.sendNpcChat("Some also give items that will help complete other Tasks,", "and many count as progress towards the set for the area", "they're in.")
		
	}
	
	private static void ringChat(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What does this ring do?")
		.sendNpcChat("Raiding these forsaken dungeons can be a lot more", "rewarding if you're fighting alongside friends and allies. It", "should be more fun and you'll gain experience faster.")
		.sendNpcChat("The ring shows others that you are interested in raiding a", "dungeon. It allows you to form, join and manage a raiding", "party.")
		.sendNpcChat("We've also set up rooms with the specific purpose of", "finding a party for you.")
		.sendNpcChat("Would you like me to show you? It's the fastest way into", "the dungeons of Daemonheim.")
		.sendOption("Yes, please.", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Yes, please.")
			.sendNpcChat("It's this way.")
			.sendNpcChat("Nevermind, i forgot the way, sorry!");
		}, "No thanks, not right now.", ()-> {
			c.getDialogueBuilder().sendPlayerChat("No thanks, not right now.")
			.sendNpcChat("Suit yourself.")
			.sendAction(()-> {
				c.getDialogueBuilder().returnToCheckpoint(0);
		    });
		});
	}
	

}
