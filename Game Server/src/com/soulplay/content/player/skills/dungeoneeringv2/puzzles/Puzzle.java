package com.soulplay.content.player.skills.dungeoneeringv2.puzzles;

import java.util.Arrays;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.VisibleRoom;

import static com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants.*;

public enum Puzzle {

	/*CRYSTAL_PUZZLE(12, CrystalPuzzleRoom.class, all(spot(13, 5))),
	TOXIN_MAZE(20, ToxinMaze.class, all(spot(7, 8)), false),
	FLIP_TILES(24, FlipTilesRoom.class, list(spot(1, 3), spot(1, 12), spot(1, 3), spot(14, 11), spot(14, 11), spot(1, 9), spot(1, 3))),
	FISHING_FERRET(32, FishingFerretRoom.class, all(spot(2, 1)), false),
	LEVERS(38, LeverRoom.class, list(spot(1, 5), spot(10, 14), spot(1, 5), spot(10, 14), spot(1, 5), spot(10, 14), spot(1, 5))),
	FLOWER_ROOTS(50, FlowerRootsRoom.class, list(spot(9, 10), spot(6, 7), spot(13, 12), spot(12, 12), spot(3, 6), spot(2, 11), spot(3, 11)), false),
	POLTERGEIST(52, PoltergeistRoom.class, list(spot(1, 10), spot(2, 10), spot(3, 5), spot(2, 4), spot(13, 4), spot(2, 12), spot(3, 4))),
	RETURN_THE_FLOW(66, ReturnTheFlowRoom.class, list(spot(3, 3), spot(2, 12), spot(2, 2), spot(4, 2), spot(3, 3), spot(2, 2), spot(2, 2))),
	*/
	//SLIDING_STATUES(42, SlidingStatuesRoom.class, all(spot(1, 3)), false);
	UNHAPPY_GHOST(60, UnhappyGhostRoom.class, list(spot(2, 3), spot(3, 5), spot(3, 5), spot(2, 2), spot(3, 3), spot(2, 2), spot(4, 3)), false),
	SLIDING_TILES(76, SlidingTilesRoom.class, list(spot(1, 2), spot(13, 7), spot(1, 2), spot(13, 7), spot(1, 2), spot(13, 7), spot(1, 2))),
	FREMENNIK_CAMP(26, FremennikCampRoom.class, list(spot(6, 14), spot(10, 2), spot(14, 6), spot(5, 12), spot(13, 10), spot(3, 5), spot(13, 10))),
	COLOURED_RECESS(68, ColoredRecessRoom.class, list(spot(2, 3), spot(2, 3), spot(2, 3), spot(2, 3), spot(2, 3), spot(2, 5), spot(2, 3)), false);

	private int chunkX;
	private Class<? extends VisibleRoom> clazz;
	private boolean allowResources;
	private int[][] keys;

	private Puzzle(int chunkX, Class<? extends VisibleRoom> clazz) {
		this(chunkX, clazz, null);
	}

	private Puzzle(int chunkX, Class<? extends VisibleRoom> clazz, int[][] key) {
		this(chunkX, clazz, key, true);
	}

	private Puzzle(int chunkX, Class<? extends VisibleRoom> clazz, int[][] keys, boolean allowResources) {
		this.chunkX = chunkX;
		this.clazz = clazz;
		this.keys = keys;
		this.allowResources = allowResources;
	}

	public int getChunkX() {
		return chunkX;
	}

	public VisibleRoom newInstance() {
		if (clazz == null) {
			return null;
		}

		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			System.err.println("error creating puzzle instance");
			return null;
		}
	}

	public boolean isAvailableOnFloorType(int floorType) {
		//I think constructor args are uglier because some are exclude and some are include
		/*if ((this == FLOWER_ROOTS || this == POLTERGEIST) && floorType == 0) {
			return false;
		}*/
		//if(this == ICY_PRESSURE_PAD && floorType != 0) {
		//return false;
		//}
		return true;
	}

	public boolean allowResources() {
		return allowResources;
	}

	public int[] getKeySpot(int i) {
		if (keys == null || i >= keys.length) {
			return null;
		}
		return keys[i];
	}

	private static int[][] all(int[] spot) {
		return new int[][]
		{ spot, spot, spot, spot, spot, spot, spot };
	}

	private static int[][] list(int[]... list) {
		if (list.length != 7) {
			System.err.println("Error in key list " + Arrays.toString(list));
			return null;
		}
		return list;
	}

}