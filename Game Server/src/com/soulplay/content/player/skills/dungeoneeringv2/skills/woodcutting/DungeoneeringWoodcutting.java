package com.soulplay.content.player.skills.dungeoneeringv2.skills.woodcutting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.util.Misc;

public class DungeoneeringWoodcutting {

	private static boolean canCut(Client c, Hatchets hatchet, Trees tree) {
		if (hatchet == null) {
			c.sendMessage("You dont have the required level to use that axe or you don't have a hatchet.");
			return false;
		}

		if (tree.getLevel() > c.getSkills().getLevel(Skills.WOODCUTTING)) {
			c.sendMessage("You need a woodcutting level of " + tree.getLevel() + " to chop down this tree.");
			return false;
		}

		if (c.getItems().freeSlots() == 0) {
			c.getDialogueBuilder().sendStatement("You don't have enough inventory space.").execute();
			return false;
		}

		return true;
	}

	public static void start(Client c, Trees tree, GameObject object, DungeonManager manager) {
		Hatchets hatchets = Hatchets.getHatchet(c);
		if (tree == null || !canCut(c, hatchets, tree) || c.skillingEvent != null) {
			return;
		}

		c.sendMessage("You swing your hatchet at the tree...");
		c.startAnimation(hatchets.getEmoteId());

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (!c.insideDungeoneering() || manager.getRealObject(object.getLocation().getX(), object.getLocation().getY()).getId() != object.getId()) {
					c.resetAnimations();
					container.stop();
					return;
				}

				c.startAnimation(hatchets.getEmoteId());
				c.sendMessage("You get some " + ItemDefinition.getName(tree.getLogsId()).toLowerCase() + ".");
				c.getItems().addItem(tree.getLogsId(), 1);
				c.getPA().addSkillXP(tree.getXp(), Skills.WOODCUTTING);

				if (tree == Trees.GRAVE_CREEPER_TREE) {
					c.getAchievement().chop500GraveCreeper();
				}

				if (Misc.random(tree.getRandomLifeProbability()) == 0) {
					manager.spawnObject(object.getId() + 1, object.getLocation().getX(), object.getLocation().getY(), object.getOrientation(), object.getType());
					c.resetAnimations();
					c.sendMessage("You have depleted this resource.");
					container.stop();
					return;
				}

				if (!canCut(c, hatchets, tree)) {
					c.resetAnimations();
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				if (c.isActive)
					c.resetAnimations();
			}
		}, 4);
	}

}
