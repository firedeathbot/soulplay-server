package com.soulplay.content.player.skills.dungeoneeringv2.drops;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.items.DungeonItems;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class BossDropManager {
	
	public static void dropBossItem(DungeonManager dungeon, int[][] itemData) {
		int size = dungeon.party.partyMembers.size();
		if (size <= 0) {
			return;
		}

		if (itemData == null) {
			for (Player party : dungeon.party.partyMembers) {
				party.sendMessage("null has received item: null");
			}

			return;
		}

		int	difficulty = dungeon.party.difficulty;
		int	teamAvg = dungeon.teamAvg;
		int complexity = dungeon.party.getComplexity();
		int dungSize = dungeon.party.size + 1;
		int tier = 0;
		int baseDropRate = 4000;
		int modiferDropRate = 500 * difficulty * dungSize + (complexity * 140);
		int highTierRandom = Misc.random(10000);
		int lowTierRandom = Misc.random(10000);
		
		tier = Misc.interpolate(1, 10, 85, 3960, teamAvg);
		int dropTier = -1;
		
		for (int i = 11; i > tier; i--) {
			int div = Math.max(Math.abs(tier - i) * 2, 1);
			//System.out.println(i+":"+highTierRandom+":"+(modiferDropRate/div));
			
			if (highTierRandom < modiferDropRate / div) {
				dropTier = i;
				//System.out.println("chance: " + modiferDropRate / div);
				//System.out.println("rolled1 tier " + i + ", random="+highTierRandom+", rate="+(modiferDropRate/div));
				break;
			} 
		}

		if (dropTier == -1) {
			for (int i = 1; i <= tier - 1; i++) {
				int div = Math.max(Math.abs(tier - i) * 2, 1);
				//System.out.println(i+":"+lowTierRandom+":"+(baseDropRate/div));
				
				if (lowTierRandom < baseDropRate / div) {
					//System.out.println("chance2: " + baseDropRate / div + " random : " + lowTierRandom);
					dropTier = i;
					//System.out.println("rolled2 tier " + i + ", random="+lowTierRandom+", rate="+(modiferDropRate/div));
					break;
				} 
			}
		}

		if (dropTier == -1) {
			dropTier = tier;
			//System.out.println("fk it, didnt roll shit here ugo default tier="+tier);
		}

		for (int i = 0; i < itemData.length; i++) {
			int[] data = itemData[i];
			if (dropTier != data[3]) {
				continue;
			}

			Player randomPlayer = dungeon.party.partyMembers.get(Misc.randomNoPlus(size));
			randomPlayer.getItems().addOrDrop(new GameItem(data[0], 1));
			if (itemData == DungeonItems.SHORTBOWS || itemData == DungeonItems.LONGBOWS) {
				randomPlayer.getItems().addOrDrop(new GameItem(DungeonItems.ARROWS[i][0], Misc.random(125)));
			}

			for (Player party : dungeon.party.partyMembers) {
				String name;
				if (party == randomPlayer) {
					name = "You have";
				} else {
					name = randomPlayer.getNameSmartUp() + " has";
				}
				party.sendMessage(name + " received item: " + ItemConstants.getItemName(data[0]));
			}

			break;
		}
	}
}
