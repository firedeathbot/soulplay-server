package com.soulplay.content.player.skills.dungeoneeringv2.items.shop;

import com.soulplay.content.npcs.impl.bosses.BossKCEnum;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class DungeonRewardShop {

	public static boolean buttonClick(Client c, int buttonId) {
		if (buttonId >= DungeonRewardData.EXPERIENCE_LAMP.getButtonId() && buttonId <= DungeonRewardData.KAL_GER.getButtonId()) {
			DungeonRewardData data = DungeonRewardData.values.get(buttonId);
			if (data == null) {
				return true;
			}

			c.selectedDungReward = data;
			return true;
		}

		switch(buttonId) {
			case 183168:
				DungeonRewardData data = c.selectedDungReward;
				if (data == null) {
					return true;
				}

				if (!data.canPurchase()) {
					c.sendMessage("You can't purchase this at the moment.");
					return true;
				}

				if (c.dungTokens < data.getPrice()) {
					c.sendMessage("You don't have enough tokens to buy this.");
					return true;
				}

				boolean purchased = false;
				if (data.getPokemonData() == null) {
					purchased = c.getItems().addItem(data.getProductId(), 1);
				} else {
					if (c.isPokemonUnlocked(data.getPokemonData())) {
						c.sendMessage("You have already unlocked this npc.");
						return true;
					}

					BossKCEnum kcData = data.getKCData();
					if (kcData == null) {
						c.sendMessage("You can't purchase this at the moment.");
						return true;
					}

					if (c.bossKills[kcData.ordinal()] < kcData.getBuyKc()) {
						c.sendMessage("You need" + kcData.getBuyKc() + kcData.toString() + " to buy this pet.");
						return true;
					}

					c.unlockPokemon(data.getPokemonData());
					c.sendMessage("You have just unlocked " + NPCHandler.getNpcName(data.getPokemonData().npcId) + "!");
					purchased = true;
				}

				if (purchased) {
					c.dungTokens -= data.getPrice();
					updateTokens(c);
				}
				return true;
			default:
				//c.sendMessage(buttonId+"");
				return false;
		}
	}

	public static void updateTokens(Client c) {
		c.getPacketSender().sendString(Misc.format(c.dungTokens), 46507);
	}

	public static void open(Client c) {
		updateTokens(c);
		c.getPacketSender().showInterface(46500);
	}

}
