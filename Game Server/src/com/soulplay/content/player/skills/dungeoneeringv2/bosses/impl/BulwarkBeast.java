package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@BossMeta(ids = {10073, 10074, 10075, 10076, 10077, 10078, 10079, 10080, 10081, 10082, 10083, 10084, 10085, 10086, 10087, 10088, 10089, 10090, 10091, 10092, 10093, 10094, 10095, 10096, 10097, 10098, 10099, 10100, 10101, 10102, 10103, 10104, 10105, 10106})
public class BulwarkBeast extends Boss {
	
	private static final int ARMOR = 40;
	private static final int DD_TICKS = 12;

	public BulwarkBeast(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		
	}

	private static Hit rockDmg(DungeonManager dungeon) {
		int teamAvg = dungeon.teamAvg;
		int minAvg = 85;
		int maxAvg = 3960;

		return new Hit(Misc.random(Misc.interpolate(1, 25, minAvg, maxAvg, teamAvg)), 0, -1);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		DungeonManager dungeon = npc.dungeonManager;
		if (dungeon == null) {
			return;
		}
		
		npc.setAttackTimer(4);//set attack speed of bulwark.

		if (npc.getStopWatch().checkThenStart(DD_TICKS) && Misc.distanceToPoint(npc.getX()+2, npc.getY()+2, c.getX(), c.getY()) <= 2) {
			c.dealDamage(rockDmg(dungeon));
			npc.startAnimation(13003);
			return;
		}
		
		npc.dungMultiAttack = false;
		int random = Misc.random(40);
		if (random >= 38) {
			npc.startAnimation(SPECIAL_ANIM);
			c.dealDamage(rockDmg(dungeon));
			c.startGraphic(SPECIAL_ATT_GFX);
		} else {
			//Range and mage multi targetted
			if (random >= 17) {
				npc.dungMultiAttack = true;
				npc.attackAnim = RANGED_ANIM;
				npc.projectileId = 2395;
				npc.endGfx = 2396;
				npc.attackType = 1;
			} else if (random >= 8) {
				npc.attackAnim = MAGIC_ANIM;
				npc.attackStartGfx = MAGIC_ATT_GFX;
				npc.projectileId = 2398;
				npc.endGfx = 2399;
				npc.endGraphicType = GraphicType.LOW;
				npc.attackType = 2;
			} else {
				npc.attackAnim = MELEE_ANIM;
				npc.attackType = 0;
				npc.projectileId  = -1;
				npc.endGfx = -1;
			}
		}
	}

	private static final Animation MELEE_ANIM = new Animation(13001);
	private static final Animation MAGIC_ANIM = new Animation(13004);
	private static final Animation RANGED_ANIM = new Animation(13006);
	private static final Animation SPECIAL_ANIM = new Animation(13007);
	private static final Graphic SPECIAL_ATT_GFX = Graphic.create(2400);
	private static final Graphic MAGIC_ATT_GFX = Graphic.create(2397);

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		if (npc.npcType % 2 == 1) {
			int armor = ARMOR;
			if (npc.dungeonManager != null) {
				armor *= npc.dungeonManager.party.difficulty;
			}

			npc.dungVariables[0] = new Integer(armor);
			npc.dungVariables[1] = new Integer(armor);
		}

		npc.getDistanceRules().setMeleeDistance(3);
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}

	public int followDistance() {
		return 10;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if (npc.dungVariables[0] == null || (source != null && !source.isPlayer())) {
			return;
		}

		if (hit.getHitIcon() != 2) {//MAGE, yes u can mage it
			if ((Integer) npc.dungVariables[0] <= 0) {
				return;
			}

			Client c = (Client) source.toPlayer();
			if (c.usingPickaxe()) {
				int dmg = Misc.random(4);
				npc.dungVariables[0] = (Integer) npc.dungVariables[0] - dmg;
				//System.out.println(npc.dungVariables[0]+":"+((int) npc.dungVariables[0] * 100 / (int) npc.dungVariables[1]));
				if ((Integer) npc.dungVariables[0] <= 0) {
					npc.transform(npc.npcType + 1);
					npc.dungeonManager.hideBar(npc.reference);
					//TODO spotanim
				}
			}

			if ((Integer) npc.dungVariables[0] > 0) {
				npc.dungeonManager.showBar(npc.reference, "Bulwark Beast's Armor", (Integer) npc.dungVariables[0] * 100 / (Integer) npc.dungVariables[1]);
			}

			hit.setDamage(0);
		}
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		DungeonManager dungeon = npc.dungeonManager;
		if (dungeon == null) {
			return;
		}

		dungeon.hideBar(npc.reference);
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		DungeonManager dungeon = npc.dungeonManager;
		if (dungeon == null) {
			return;
		}

		if (npc.attackType == 2) {
			int projId = npc.projectileId;
			for (Player player : dungeon.party.partyMembers) {
				if (p == player || !dungeon.getCurrentRoomReference(player.getCurrentLocation()).equals(npc.reference)) {
					continue;
				}

				NPCHandler.multiAttackGfx(p, player, projId);
				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer e) {
						if (npc == null || player == null) {
							e.stop();
							return;
						}
						if (player.isDead() || player.teleTimer > 0 || player.getSkills().getLifepoints() <= 0) {
							e.stop();
							return;
						}

						int oldStyle = npc.attackType;
						npc.attackType = 2;
						NPCHandler.multiAttackDamageNew(npc, player);
						npc.attackType = oldStyle;
						if (player.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
								|| player.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
							CombatPrayer.drainPrayerByHit(player, 1+Misc.random(5)); //player.getSkills().decrementPrayerPoints(Misc.random(5));
						}
						e.stop();
					}

					@Override
					public void stop() {
						/* empty */
					}

				}, NPCHandler.getHitDelay(npc.getId()) - 1);
			}
		}
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}

}
