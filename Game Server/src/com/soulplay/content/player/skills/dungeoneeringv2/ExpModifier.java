package com.soulplay.content.player.skills.dungeoneeringv2;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class ExpModifier {

	public static int[] dungeoneeringModifierExp(Party party, Player player, DungeonManager dungeon) {
		int totalModifier = 0;
		int bonusRooms = 0;
		int difficultyMod = 0;
		int complexityMod = 0;
		int levelMod = 0;
		int guideMode = 0;
		int deaths = 0;
		int unbalancedPenalty = 0;
		
		int partyDiff = party.difficulty;
		int partyMembers = party.partyMembers.size();
		int dungComplexity = party.getComplexity();
		int dungeonSize = party.size;
		int playerDeaths = player.dungDeaths;
		int highestAvg = Party.highestAvg(party);
		int averageAvg = Party.calcPartyAvg(party);
		int dungSizeBonus = (int) (dungeonSize * 7.5);
		
		if (partyMembers == 1) {
			difficultyMod = 0;
		} else if (partyMembers > partyDiff) {
			difficultyMod = (partyDiff - partyMembers) * 8;
		} else if (partyDiff >= partyMembers) {
			difficultyMod = partyDiff * 5 - 5;
		}
		
		complexityMod = dungComplexity == 6 ? 0 : ((dungComplexity - 6) * 5 - 25);
		
		if (party.isGuideMode()) {
		    guideMode = dungeonSize == 0 ? -1 : (dungeonSize == 1 ? -3 : -5);
		} else 
			guideMode = 0;
		
		if (dungeonSize == DungeonConstants.SMALL_DUNGEON) {
			deaths = playerDeaths * 10; 
		}
		
		switch (playerDeaths) {
		
		case 15:
			deaths = -100;
			break;
		case 14: 
			deaths = partyMembers == 1 ? -96 : (partyMembers == 2 ? -97 : (partyMembers == 3 ? -97 : (partyMembers == 4 ? -97 : -98)));
			break;
		case 13:
			deaths = partyMembers == 1 ? -92 : (partyMembers == 2 ? -94 : (partyMembers == 3 ? -94 : (partyMembers == 4 ? -94 : -96)));
			break;
		case 12:
			deaths = partyMembers == 1 ? -87 : (partyMembers == 2 ? -90 : (partyMembers == 3 ? -91 : (partyMembers == 4 ? -91 : -94)));
		    break;
		case 11: 
			deaths = partyMembers == 1 ? -82 : (partyMembers == 2 ? -86 : (partyMembers == 3 ? -87 : (partyMembers == 4 ? -88 : - 91)));
			break;
		case 10: 
			deaths = partyMembers == 1 ? -77 : (partyMembers == 2 ? -81 : (partyMembers == 3 ? -83 : (partyMembers == 4 ? -84 : -88)));
			break;
		case 9: 
			deaths = partyMembers == 1 ? -71 : (partyMembers == 2 ? -76 : (partyMembers == 3 ? -78 : (partyMembers == 4 ? -80 : -84)));
			break;
		case 8: 
			deaths = partyMembers == 1 ? -65 : (partyMembers == 2 ? -70 : (partyMembers == 3 ? -72 : (partyMembers == 4 ? -75 : -79)));
			break;
		case 7: 
			deaths = partyMembers == 1 ? -59 : (partyMembers == 2 ? -63 : (partyMembers == 3 ? -65 : (partyMembers == 4 ? -69 : -74)));
			break;
		case 6: 
			deaths = partyMembers == 1 ? -52 : (partyMembers == 2 ? -56 : (partyMembers == 3 ? -58 : (partyMembers == 4 ? -62 : -67)));
			break;
		case 5: 
			deaths = partyMembers == 1 ? -45 : (partyMembers == 2 ? -48 : (partyMembers == 3 ? -50 : (partyMembers == 4 ? -54 : -59)));
			break;
		case 4: 
			deaths = partyMembers == 1 ? -37 : (partyMembers == 2 ? -39 : (partyMembers == 3 ? -41 : (partyMembers == 4 ? -45 : -49)));
			break;
		case 3: 
			deaths = partyMembers == 1 ? -29 : (partyMembers == 2 ? -30 : (partyMembers == 3 ? -32 : (partyMembers == 4 ? -35 : -38)));
			break;
		case 2: 
			deaths = partyMembers == 1 ? -20 : (partyMembers == 2 ? -20 : (partyMembers == 3 ? -22 : (partyMembers == 4 ? -24 : -26)));
			break;
		case 1: 
			deaths = partyMembers == 1 ? -10 : (partyMembers == 2 ? -10 : (partyMembers == 3 ? -11 : (partyMembers == 4 ? -12 : -13)));
			break;
		case 0: 
			deaths = 0;
			break;
		}
		
		int totalBonusRooms = dungeon.getBonusTotal();
		int totalBonusRoomsOpened = dungeon.getBonusOpened();
		int roomNpcCount = 2 + partyDiff;
		int unOpenedBonusRooms =  dungeon.getBonusTotal() - totalBonusRoomsOpened;
		int extraNpcs = unOpenedBonusRooms * roomNpcCount + dungeon.totalGuardiansSpawned;

		if (extraNpcs > 0) {
			if (dungeonSize > 1) {
				levelMod = Misc.interpolate(-20, 20, 0, extraNpcs, dungeon.totalGuardiansKilled);
			} else if (dungeonSize > 0) {
				levelMod = Misc.interpolate(-15, 15, 0, extraNpcs, dungeon.totalGuardiansKilled);
			} else if (dungeonSize == 0) {
				levelMod = Misc.interpolate(-10, 10, 0, extraNpcs, dungeon.totalGuardiansKilled);
			}
		}

		if (totalBonusRooms > 0) {
		    bonusRooms = Misc.interpolate(0, 13, 0, totalBonusRooms, totalBonusRoomsOpened);
		}

		int avgDifference = highestAvg - averageAvg;
		if (avgDifference > 1150) {
			unbalancedPenalty = Misc.interpolate(-10, -80, 1150, 3100, avgDifference);
		}
		
		totalModifier = 100 + dungSizeBonus + bonusRooms + difficultyMod + complexityMod + levelMod + guideMode + deaths + unbalancedPenalty;
		
		if (totalModifier < 1) {
			totalModifier = 0;
		}
		
		return new int[] {totalModifier, dungSizeBonus, bonusRooms, difficultyMod, complexityMod, levelMod, guideMode, deaths, unbalancedPenalty};
	}

}
