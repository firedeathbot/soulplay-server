package com.soulplay.content.player.skills.dungeoneeringv2.skills.farming;

import java.util.HashMap;
import java.util.Map;

public enum Harvest {

	POTATO(17823, 1, 1, 17817, 50080, 55571),
	GISSEL(17824, 34, 3, 17819, 50083, 55574),
	EDICAP(17825, 68, 5, 17821, 50086, 55577),
	SAGEWORT(17826, 7, 15, 17494, 50089, 55580),
	VALERIAN(17827, 18, 18, 17496, 50092, 55583),
	ALOE(17828, 29, 21, 17498, 50095, 55586),
	WORMWOOD(17829, 40, 27, 17500, 50098, 55589),
	MAGEBANE(17830, 51, 34, 17502, 50101, 55592),
	FEATHERFOIL(17831, 62, 44, 17504, 50104, 55595),
	WINTERS_GRIP(17832, 73, 58, 17506, 50107, 55598),
	LYCOPUS(17833, 84, 75, 17508, 50110, 55601),
	BUCKTHORN(17834, 95, 98, 17510, 50113, 55604),
	SALVE_NETTLES(1, 6, 17448),
	WILDERCRESS(10, 9, 17450),
	BLIGHTLEAF(20, 12, 17452),
	ROSEBLOOD(30, 17, 17454),
	BRYLL(40, 23, 17456),
	DUSKWEED(50, 31, 17458),
	SOULBELL(60, 42, 17460),
	ECTOGRASS(70, 55, 17462),
	RUNELEAF(80, 72, 17464),
	SPIRITBLOOM(90, 94, 17466);

	public static final Harvest[] values = values();
	public static final Map<Integer, Harvest> idToPatch = new HashMap<>();
	private final int seed, lvl, product, exp, grownId, grownIdWarped;

	private Harvest(int seed, int lvl, int exp, int product, int grownId, int grownIdWarped) {
		this.seed = seed;
		this.lvl = lvl;
		this.exp = exp;
		this.product = product;
		this.grownId = grownId;
		this.grownIdWarped = grownIdWarped;
	}

	private Harvest(int lvl, int exp, int product) {
		this(-1, lvl, exp, product, -1, -1);
	}

	public int getSeed() {
		return seed;
	}

	public int getLvl() {
		return lvl;
	}

	public int getExp() {
		return exp;
	}

	public int getProduct() {
		return product;
	}

	public boolean isTextile() {
		return seed == -1;
	}

	public int getGrownId() {
		return grownId;
	}

	public int getGrownIdWarped() {
		return grownIdWarped;
	}

	public static Harvest forSeed(int id) {
		for (Harvest harvest : Harvest.values) {
			if (harvest.seed == id) {
				return harvest;
			}
		}

		return null;
	}

	static {
		for (Harvest data : values) {
			idToPatch.put(data.grownId, data);
			idToPatch.put(data.grownIdWarped, data);
		}
	}
	
    public static void load() {
    	/* empty */
    }
}