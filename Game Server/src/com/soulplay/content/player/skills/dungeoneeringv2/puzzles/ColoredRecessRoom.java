package com.soulplay.content.player.skills.dungeoneeringv2.puzzles;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.PuzzleRoom;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class ColoredRecessRoom extends PuzzleRoom {

	public static final int[] SHELVES =
	{ 35243, 35242, 35241, 35245, 35246 };

	//+1-4 for colors
	public static final int[] BASE_BLOCKS =
	{ 13024, 13029, 13034, 13039, 13044 };

	public static final int[][] LOCATIONS =
	{
	{ 5, 10 },
	{ 10, 10 },
	{ 10, 5 },
	{ 5, 5 }, };

	private NPC[] blocks;

	@Override
	public void openRoom() {
		manager.spawnRandomNPCS(reference);
		blocks = new NPC[4];
		for (int i = 0; i < blocks.length; i++) {
			while_: while (true) {
				Location tile = manager.getTile(reference, 4 + Misc.randomNoPlus(8), 4 + Misc.randomNoPlus(8));
				if (!Misc.isTileFree(manager.height, tile.getX(), tile.getY(), 1, manager.clipping)) {
					continue;
				}
				for (int j = 0; j < i; j++) {
					if (blocks[j].absX == tile.getX() && blocks[j].absY == tile.getY()) {
						continue while_;
					}
				}

				blocks[i] = manager.spawnNPC(BASE_BLOCKS[type], tile, reference, DungeonConstants.PUZZLE_NPC, false);
				break;
			}
		}

		for (int tileColor = 0; tileColor < LOCATIONS.length; tileColor++) {
			int[] location = LOCATIONS[tileColor];
			Location tile = manager.getTile(reference, location[0], location[1]);
			manager.clipping.removeClipping(tile.getX(), tile.getY(), tile.getZ());
		}
	}

	public void checkComplete(Client player) {
		if(isComplete())
			return;
		outer: for (NPC block : blocks) {
			for (int tileColor = 0; tileColor < LOCATIONS.length; tileColor++) {
				int[] location = LOCATIONS[tileColor];
				Location tile = manager.getTile(reference, location[0], location[1]);
				if (tile.getX() == block.absX && tile.getY() == block.absY) {
					int color = block.getNpcTypeSmart() - BASE_BLOCKS[type] - 1;
					if (color == tileColor) {
						continue outer;
					} else {
						return;
					}

				}
			}
			return;
		}
		setComplete(player);
	}

	public static void handle(final Client player, NPC npc, DungeonManager manager, final boolean push) {
		boolean pull = !push;

		int nPosX = manager.getRoomPosX(npc.getCurrentLocation());
		int nPosY = manager.getRoomPosY(npc.getCurrentLocation());
		int pPosX = manager.getRoomPosX(player.getCurrentLocation());
		int pPosY = manager.getRoomPosY(player.getCurrentLocation());

		final int dx = push ? npc.getX() - player.getX() : player.getX() - npc.getX();
		final int dy = push ? npc.getY() - player.getY() : player.getY() - npc.getY();
		final int ldx = push ? nPosX - pPosX : pPosX - nPosX;
		final int ldy = push ? nPosY - pPosY : pPosY - nPosY;

		if (nPosX + ldx < 4 || nPosX + ldx > 11 || nPosY + ldy < 4 || nPosY + ldy > 11) {
			player.sendMessage("You cannot push the block there.");
			return;
		}

		final Location nTarget = npc.getCurrentLocation().transform(dx, dy, 0);
		final Location pTarget = player.getCurrentLocation().transform(dx, dy, 0);

		if (!Misc.isTileFree(0, nTarget.getX(), nTarget.getY(), 1, manager.clipping)
				|| !Misc.isTileFree(0, pTarget.getX(), pTarget.getY(), 1, manager.clipping)) {
			player.sendMessage("Something is blocking the way.");
			return;
		}

		ColoredRecessRoom room = (ColoredRecessRoom) manager.getVisibleRoom(manager.getCurrentRoomReference(player.getCurrentLocation()));

		if (!room.canMove(null, nTarget)
				|| (pull && !room.canMove(null, pTarget))) {
			player.sendMessage("A block is blocking the way.");
			return;
		}

		for (Client team : manager.party.partyMembers) {
			if (team != player && team.absX == nTarget.getX() && team.absY == nTarget.getY()) {
				player.sendMessage("A party member is blocking the way.");
				return;
			}
		}

		//player.lock(2);//TODO lock?
		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {

			private boolean moved;

			@Override
			public void execute(CycleEventContainer container) {
				if (!moved) {
					moved = true;
					//System.out.println((npc.absX + dx) + ":" + (npc.absY + dy));
					npc.randomTimer.startTimer(3);
					npc.moveX = dx;
					npc.moveY = dy;
					//Location fromTile = Location.create(player.getX(), player.getY(), player.getZ());
					player.getPA().playerWalk(pTarget.getX(), pTarget.getY());
					//player.setNextForceMovement(
					//		new ForceMovement(fromTile, 0, pTarget, 1, Helper.getFaceDirection(Block.this, player)));
					player.startAnimation(push ? 3065 : 3065);
				} else {
					room.checkComplete(player);
					container.stop();
				}
			}

			@Override
			public void stop() {
			}
		}, 1);

	}

	@Override
	public boolean canMove(Client player, Location to) {
		for (NPC block : blocks) {
			if (to.getX() == block.absX && to.getY() == block.absY) {
				return false;
			}
		}
		return true;
	}
	
	private static void pick(Client c, int id) {
		if(Math.random() < 0.2) {
			c.sendMessage("The vial reacts explosively as you pick it up.");
			c.dealDamage(new Hit((int) (c.getSkills().getMaximumLifepoints() * 0.25), 0, -1));
			return;
		}

		c.getItems().addItem(id, 1);
		c.startAnimation(832);
	}

	@Override
	public boolean processObjectClick1(Client p, GameObject object) {
		if (object.getId() == SHELVES[type]) {
			p.getDialogueBuilder().sendOption("What color would you like to mix?",
			"Blue.", () -> {
				pick(p, 19869);	
			}, "Green.", () -> {
				pick(p, 19871);	
			}, "Yellow.", () -> {
				pick(p, 19873);	
			}, "Violet.", () -> {
				pick(p, 19875);	
			}).execute();
			return false;
		}
		return true;
	}

	@Override
	public boolean processNPCClick1(Client player, NPC npc) {
		if (npc.npcType >= 13024 && npc.npcType <= 13048 && npc.randomTimer.complete()) {
			handle(player, npc, player.dungParty.dungManager, true);
			return false;
		}
		return true;
	}

	@Override
	public boolean handleItemOnNpc(Client player, NPC npc, int item) {
		if (npc.npcType >= 13024 && npc.npcType <= 13048 && npc.randomTimer.complete()) {
			int color = (item - 19869) / 2;
			if (color < 0 || color > 3) {
				return true;
			}
			if (npc.npcType != BASE_BLOCKS[type]) {
				return true;
			}
			player.getItems().deleteItemInOneSlot(item, 1);
			player.startAnimation(832);
			npc.transform(npc.npcType + color + 1);
			checkComplete(player);
			return false;
		}

		return true;
	}

	@Override
	public boolean processNPCClick2(Client player, NPC npc) {
		if (npc.npcType >= 13024 && npc.npcType <= 13048 && npc.randomTimer.complete()) {
			handle(player, npc, player.dungParty.dungManager, false);
			return false;
		}

		return true;
	}

}
