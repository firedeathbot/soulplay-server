package com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting;

import java.util.HashMap;
import java.util.Map;

public enum LeatherData {

	//Mage armor
	SALVE(17468, new short[] {17217, 16845, 16735, 16911, 17151}, new byte[] {8, 6, 4, 2, 1}, new short[] {96, 52, 31, 14, 12}),
	WILDERCRESS(17470, new short[] {17219, 16847, 16737, 16913, 17153}, new byte[] {18, 16, 14, 12, 10}, new short[] {139, 78, 48, 22, 21}),
	BRIGHTLEAF(17472, new short[] {17221, 16849, 16739, 16915, 17155}, new byte[] {28, 26, 24, 22, 20}, new short[] {181, 103, 65, 31, 29}),
	ROSEBLOOD(17474, new short[] {17223, 16851, 16741, 16917, 17157}, new byte[] {38, 36, 34, 32, 30}, new short[] {224, 129, 82, 38, 36}),
	BRYLL(17476, new short[] {17225, 16853, 16743, 16919, 17159}, new byte[] {48, 46, 44, 42, 40}, new short[] {266, 154, 99, 48, 46}),
	DUSKWEED(17478, new short[] {17227, 16855, 16745, 16921, 17161}, new byte[] {58, 56, 54, 52, 50}, new short[] {309, 180, 116, 56, 55}),
	SOULBELL(17480, new short[] {17229, 16857, 16747, 16923, 17163}, new byte[] {68, 66, 64, 62, 60}, new short[] {351, 205, 133, 65, 63}),
	ECTOGRASS(17482, new short[] {17231, 16859, 16749, 16925, 17165}, new byte[] {78, 76, 74, 72, 70}, new short[] {394, 231, 150, 73, 72}),
	RUNIC(17484, new short[] {17233, 16861, 16751, 16927, 17167}, new byte[] {88, 86, 84, 82, 80}, new short[] {436, 256, 167, 82, 80}),
	SPIRITBLOOM(17486, new short[] {17235, 16863, 16753, 16929, 17169}, new byte[] {98, 96, 94, 92, 90}, new short[] {479, 282, 184, 90, 89}),
	//Ranged armor
	PROTOLEATHER(17424, new short[] {17173, 17319, 17041, 17297, 17195}, new byte[] {9, 7, 5, 3, 1}, new short[] {99, 54, 16, 14, 13}),
	SUBLEATHER(17426, new short[] {17175, 17321, 17043, 17299, 17197}, new byte[] {19, 17, 15, 13, 11}, new short[] {142, 80, 50, 23, 21}),
	PARALEATHER(17428, new short[] {17177, 17323, 17045, 17301, 17199}, new byte[] {29, 27, 25, 23, 21}, new short[] {184, 105, 67, 31, 30}),
	ARCHLEATHER(17430, new short[] {17179, 17325, 17047, 17303, 17201}, new byte[] {39, 37, 35, 33, 31}, new short[] {269, 131, 84, 40, 38}),
	DROMOLEATHER(17432, new short[] {17181, 17327, 17049, 17305, 17203}, new byte[] {49, 47, 45, 43, 41}, new short[] {296, 156, 101, 48, 47}),
	SPINOLEATHER(17434, new short[] {17183, 17329, 17051, 17307, 17205}, new byte[] {59, 57, 55, 53, 51}, new short[] {312, 182, 118, 57, 55}),
	GALLILEATHER(17436, new short[] {17185, 17331, 17053, 17309, 17207}, new byte[] {69, 67, 65, 63, 61}, new short[] {354, 207, 135, 65, 64}),
	STEGOLEATHER(17438, new short[] {17187, 17333, 17055, 17311, 17209}, new byte[] {79, 77, 75, 73, 71}, new short[] {397, 233, 152, 74, 72}),
	MEGALEATHER(17440, new short[] {17189, 17335, 17057, 17313, 17211}, new byte[] {89, 87, 85, 83, 81}, new short[] {439, 258, 169, 82, 81}),
	TYRANNOLEATHER(17442, new short[] {17191, 17337, 17059, 17315, 17213}, new byte[] {99, 97, 95, 93, 91}, new short[] {482, 284, 186, 91, 89});

	public static Map<Integer, LeatherData> values = new HashMap<>();
	private static final byte[] LEATHER_REQS = new byte[] { 5, 3, 2, 1, 1 };
	private final int itemId;
	private final short[] products;
	private final byte[] levelReq;
	private final short[] xp;

	private LeatherData(int itemId, short[] products, byte[] levelReq, short[] xp) {
		this.itemId = itemId;
		this.products = products;
		this.levelReq = levelReq;
		this.xp = xp;
	}

	public int getItemId() {
		return itemId;
	}

	public short[] getProducts() {
		return products;
	}

	public byte[] getLevelReq() {
		return levelReq;
	}

	public short[] getXp() {
		return xp;
	}

	public byte[] getLeatherReqs() {
		return LEATHER_REQS;
	}

	static {
		for (LeatherData data : values()) {
			values.put(data.getItemId(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
