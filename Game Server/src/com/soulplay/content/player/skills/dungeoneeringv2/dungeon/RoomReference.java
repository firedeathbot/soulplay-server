package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import com.soulplay.util.Misc;

public class RoomReference {

	private final int bitpack;

	public RoomReference(int x, int y) {
		this.bitpack = Misc.toBitpack(x, y);
	}

	public int getX() {
		return Misc.getFirst(bitpack);
	}

	public int getY() {
		return Misc.getSecond(bitpack);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof RoomReference) {
			RoomReference rRef = (RoomReference) object;
			return bitpack == rRef.bitpack;
		}
		return false;
	}

	@Override
	public String toString() {
		return "[RoomReference][" + getX() + "][" + getY() + "]";
	}
}
