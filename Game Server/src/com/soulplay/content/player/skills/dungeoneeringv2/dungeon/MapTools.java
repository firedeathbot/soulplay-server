package com.soulplay.content.player.skills.dungeoneeringv2.dungeon;

import java.util.Map;

import com.soulplay.Server;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.map.ObjectDefVersion;

public class MapTools {

	public final static void copy2RatioSquare(int fromRegionX, int fromRegionY, int toRegionX, int toRegionY, int rotation, Map<Long, GameObject> objects, DynamicRegionClip clip, int toRegionZ, DungeonManager manager, int... planes) {
		for (int i : planes) { //plane 1 and 2
			int newZ = i + toRegionZ;
			if (rotation == 0) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX, toRegionY, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY, i, toRegionX + 1, toRegionY, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX, fromRegionY + 1, i, toRegionX, toRegionY + 1, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY + 1, i, toRegionX + 1, toRegionY + 1, newZ, rotation, objects, clip, manager);
			} else if (rotation == 1) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX, toRegionY + 1, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY, i, toRegionX, toRegionY, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX, fromRegionY + 1, i, toRegionX + 1, toRegionY + 1, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY + 1, i, toRegionX + 1, toRegionY, newZ, rotation, objects, clip, manager);
			} else if (rotation == 2) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX + 1, toRegionY + 1, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY, i, toRegionX, toRegionY + 1, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX, fromRegionY + 1, i, toRegionX + 1, toRegionY, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY + 1, i, toRegionX, toRegionY, newZ, rotation, objects, clip, manager);
			} else if (rotation == 3) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX + 1, toRegionY, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY, i, toRegionX + 1, toRegionY + 1, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX, fromRegionY + 1, i, toRegionX, toRegionY, newZ, rotation, objects, clip, manager);
				copyChunk(fromRegionX + 1, fromRegionY + 1, i, toRegionX, toRegionY + 1, newZ, rotation, objects, clip, manager);
			}
		}
	}

	private static void copyChunk(int fromChunkX, int fromChunkY, int fromPlane, int toChunkX, int toChunkY, int toPlane, int rotation, Map<Long, GameObject> objects, DynamicRegionClip clip, DungeonManager manager) {
		Region toRegion = Server.getRegionManager().getRegionByLocation(toChunkX << 3, toChunkY << 3, toPlane);
		toRegion.makeDynamicData();
		int regionOffsetX = toChunkX - ((toChunkX >> 3) << 3);
		int regionOffsetY = toChunkY - ((toChunkY >> 3) << 3);
		toRegion.setDynamicData(fromChunkX, fromChunkY, fromPlane, regionOffsetX, regionOffsetY, toPlane, rotation);
		manager.regions.add(toRegion);

		DynamicRegionClip.copy(fromChunkX << 3, fromChunkY << 3, fromPlane, toChunkX << 3, toChunkY << 3, toPlane, objects, rotation, clip, ObjectDefVersion.DEF_667);
	}

	public static void copy1RatioSquare(int fromRegionX, int fromRegionY, int toRegionX, int toRegionY, int rotation, Map<Long, GameObject> objects, DynamicRegionClip clip, int toRegionZ, LmsManager manager, ObjectDefVersion objectDefVersion, int... planes) {
		for (int i : planes) { //plane 1 and 2
			int newZ = i + toRegionZ;
			if (rotation == 0) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX, toRegionY, newZ, rotation, objects, clip, manager, objectDefVersion);
			} else if (rotation == 1) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX, toRegionY + 1, newZ, rotation, objects, clip, manager, objectDefVersion);
			} else if (rotation == 2) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX + 1, toRegionY + 1, newZ, rotation, objects, clip, manager, objectDefVersion);
			} else if (rotation == 3) {
				copyChunk(fromRegionX, fromRegionY, i, toRegionX + 1, toRegionY, newZ, rotation, objects, clip, manager, objectDefVersion);
			}
		}
	}

	public static void copyChunk(int fromChunkX, int fromChunkY, int fromPlane, int toChunkX, int toChunkY, int toPlane, int rotation, Map<Long, GameObject> objects, DynamicRegionClip clip, LmsManager manager, ObjectDefVersion objectDefVersion) {
		Region toRegion = Server.getRegionManager().getRegionByLocation(toChunkX << 3, toChunkY << 3, toPlane);
		toRegion.makeDynamicData();
		int regionOffsetX = toChunkX - ((toChunkX >> 3) << 3);
		int regionOffsetY = toChunkY - ((toChunkY >> 3) << 3);
		toRegion.setDynamicData(fromChunkX, fromChunkY, fromPlane, regionOffsetX, regionOffsetY, toPlane, rotation);
		manager.regions.add(toRegion);

		DynamicRegionClip.copy(fromChunkX << 3, fromChunkY << 3, fromPlane, toChunkX << 3, toChunkY << 3, toPlane, objects, rotation, clip, objectDefVersion);
	}

	public static void copyStaticData(int startX, int startY, int lengthX, int lengthY, int toPlane, Map<Long, GameObject> objects, Map<Long, GameObject> newObjects, DynamicRegionClip clip, ObjectDefVersion objectDefVersion, int... planes) {
		for (int i : planes) {
			int newZ = i + toPlane;
			DynamicRegionClip.copyStaticToStatic(startX, startY, i, newZ, lengthX, lengthY, objects, newObjects, clip, objectDefVersion);
		}
	}

	public static void copyStaticData(int startX, int startY, int lengthX, int lengthY, int toPlane, Map<Long, GameObject> objects, Map<Long, GameObject> newObjects, DynamicRegionClip clip, TobManager manager, ObjectDefVersion objectDefVersion, int... planes) {
		for (int i : planes) {
			int newZ = i + toPlane;
			DynamicRegionClip.copyStaticToStatic(startX, startY, i, newZ, lengthX, lengthY, objects, newObjects, clip, objectDefVersion);
		}
	}

	public final static void copy4RatioSquare(int fromRegionX, int fromRegionY, int toRegionX, int toRegionY, int rotation, Map<Long, GameObject> objects, DynamicRegionClip clip, int toRegionZ, RaidsManager manager, int fromRegionZ) {
		if (rotation == 0) {
			copyChunk(fromRegionX + 0, fromRegionY + 0, fromRegionZ, toRegionX + 0, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 0, fromRegionY + 1, fromRegionZ, toRegionX + 0, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 0, fromRegionY + 2, fromRegionZ, toRegionX + 0, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 0, fromRegionY + 3, fromRegionZ, toRegionX + 0, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 1, fromRegionY + 0, fromRegionZ, toRegionX + 1, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 1, fromRegionZ, toRegionX + 1, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 2, fromRegionZ, toRegionX + 1, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 3, fromRegionZ, toRegionX + 1, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 2, fromRegionY + 0, fromRegionZ, toRegionX + 2, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 1, fromRegionZ, toRegionX + 2, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 2, fromRegionZ, toRegionX + 2, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 3, fromRegionZ, toRegionX + 2, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 3, fromRegionY + 0, fromRegionZ, toRegionX + 3, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 1, fromRegionZ, toRegionX + 3, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 2, fromRegionZ, toRegionX + 3, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 3, fromRegionZ, toRegionX + 3, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
		} else if (rotation == 1) {
			copyChunk(fromRegionX + 0, fromRegionY + 0, fromRegionZ, toRegionX + 0, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 0, fromRegionZ, toRegionX + 0, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 0, fromRegionZ, toRegionX + 0, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 0, fromRegionZ, toRegionX + 0, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 0, fromRegionY + 1, fromRegionZ, toRegionX + 1, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 1, fromRegionZ, toRegionX + 1, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 1, fromRegionZ, toRegionX + 1, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 1, fromRegionZ, toRegionX + 1, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 0, fromRegionY + 2, fromRegionZ, toRegionX + 2, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 2, fromRegionZ, toRegionX + 2, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 2, fromRegionZ, toRegionX + 2, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 2, fromRegionZ, toRegionX + 2, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 0, fromRegionY + 3, fromRegionZ, toRegionX + 3, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 3, fromRegionZ, toRegionX + 3, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 3, fromRegionZ, toRegionX + 3, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 3, fromRegionZ, toRegionX + 3, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
		} else if (rotation == 2) {
			copyChunk(fromRegionX + 0, fromRegionY + 0, fromRegionZ, toRegionX + 3, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 0, fromRegionY + 1, fromRegionZ, toRegionX + 3, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 0, fromRegionY + 2, fromRegionZ, toRegionX + 3, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 0, fromRegionY + 3, fromRegionZ, toRegionX + 3, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 1, fromRegionY + 0, fromRegionZ, toRegionX + 2, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 1, fromRegionZ, toRegionX + 2, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 2, fromRegionZ, toRegionX + 2, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 3, fromRegionZ, toRegionX + 2, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 2, fromRegionY + 0, fromRegionZ, toRegionX + 1, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 1, fromRegionZ, toRegionX + 1, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 2, fromRegionZ, toRegionX + 1, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 3, fromRegionZ, toRegionX + 1, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 3, fromRegionY + 0, fromRegionZ, toRegionX + 0, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 1, fromRegionZ, toRegionX + 0, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 2, fromRegionZ, toRegionX + 0, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 3, fromRegionZ, toRegionX + 0, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
		} else if (rotation == 3) {
			copyChunk(fromRegionX + 0, fromRegionY + 0, fromRegionZ, toRegionX + 3, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 0, fromRegionZ, toRegionX + 3, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 0, fromRegionZ, toRegionX + 3, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 0, fromRegionZ, toRegionX + 3, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 0, fromRegionY + 1, fromRegionZ, toRegionX + 2, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 1, fromRegionZ, toRegionX + 2, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 1, fromRegionZ, toRegionX + 2, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 1, fromRegionZ, toRegionX + 2, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 0, fromRegionY + 2, fromRegionZ, toRegionX + 1, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 2, fromRegionZ, toRegionX + 1, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 2, fromRegionZ, toRegionX + 1, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 2, fromRegionZ, toRegionX + 1, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);

			copyChunk(fromRegionX + 0, fromRegionY + 3, fromRegionZ, toRegionX + 0, toRegionY + 0, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 1, fromRegionY + 3, fromRegionZ, toRegionX + 0, toRegionY + 1, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 2, fromRegionY + 3, fromRegionZ, toRegionX + 0, toRegionY + 2, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
			copyChunk(fromRegionX + 3, fromRegionY + 3, fromRegionZ, toRegionX + 0, toRegionY + 3, toRegionZ, rotation, objects, clip, manager, ObjectDefVersion.OSRS_DEFS);
		}
	}

	public static void copyChunk(int fromChunkX, int fromChunkY, int fromPlane, int toChunkX, int toChunkY, int toPlane, int rotation, Map<Long, GameObject> objects, DynamicRegionClip clip, RaidsManager manager, ObjectDefVersion objectDefVersion) {
		Region toRegion = Server.getRegionManager().getRegionByLocation(toChunkX << 3, toChunkY << 3, toPlane);
		toRegion.makeDynamicData();
		int regionOffsetX = toChunkX - ((toChunkX >> 3) << 3);
		int regionOffsetY = toChunkY - ((toChunkY >> 3) << 3);
		toRegion.setDynamicData(fromChunkX, fromChunkY, fromPlane, regionOffsetX, regionOffsetY, toPlane, rotation);
		manager.regions.add(toRegion);

		DynamicRegionClip.copy(fromChunkX << 3, fromChunkY << 3, fromPlane, toChunkX << 3, toChunkY << 3, toPlane, objects, rotation, clip, objectDefVersion);
	}

}
