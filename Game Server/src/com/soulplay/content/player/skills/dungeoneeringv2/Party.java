package com.soulplay.content.player.skills.dungeoneeringv2;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonUtils;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

public class Party {
	
	public static boolean disableDung = false;
	public static final int INVITE_EXPIRE_TICKS = 50;
	public static final int MAX_PARTY_MEMBERS = 5;

	public Client c;
	public int floor;
	private int complexity;
	public int size = DungeonConstants.SMALL_DUNGEON;
	public DungeonManager dungManager;
	public int difficulty = 1;
	public boolean guideMode;

	public List<Client> partyMembers = new ArrayList<>(MAX_PARTY_MEMBERS);
	
	public Party(Client c) {
		this.c = c;
		partyMembers.add(c);

		complexity = c.getMaxComplexity();
		for (int i = 1; i <= 60; i++) {
			if (!c.isDungFloorCompleted(i) && c.dungFloorsUnlocked >= i) {
				floor = i;
				break;
			}
		}

		if (floor == 0) {
			floor = c.dungFloorsUnlocked;
		}
	}

	public int getFloorType() {
		return DungeonUtils.getFloorType(floor);
	}

	public void inviteToParty(Client sender, Client other) {
		if (!partyMembers.isEmpty() && partyMembers.get(0) != sender) {
			sender.sendMessage("Only party leader can invite.");
			return;
		}
		
		if (partyMembers.size() >= MAX_PARTY_MEMBERS) {
			//TODO is this right check lol, test 2018
			c.sendMessage("Party is full.");
			return;
		}
		
		if (partyMembers.contains(other)) {
			c.sendMessage("Player is already in your party.");
			return;
		}
		
		if (other.dungParty != null) {
			c.sendMessage("Player is already in a party.");
			return;
		}

		if (complexity > other.getMaxComplexity()) {
			c.sendMessage(other.getNameSmartUp() + " cannot access that complexity level.");
			return;
		}

		if (floor > other.dungFloorsUnlocked) {
			c.sendMessage(other.getNameSmartUp() + " cannot access that floor.");
			return;
		}

		c.expireTicks = INVITE_EXPIRE_TICKS;
		c.onExpire = () -> {
			if (c.dungPartyInvite != -1) {
				Client invited = (Client) PlayerHandler.players[c.dungPartyInvite];
				if (invited != null) {
					invited.sendMessage("Party invitation from " + c.getNameSmartUp() + " has expired.");
					invited.dungPartyInvite = -1;
					invited.expireTicks = 0;
				}
			}

			c.sendMessage("Your invitation has expired.");
			c.dungPartyInvite = -1;
			c.expireTicks = 0;
		};

		c.dungPartyInvite = other.getId();
		c.sendMessage("Sending party invitation to " + other.getNameSmartUp() + "...");
		other.getPacketSender().sendMessage(ChatMessageTypes.DUNG_PARTY_REQUEST, "has invited you to a dungeon party.", c.getNameSmartUp());
	}

	public static void handleLogin(Client c) {
		if (c.dungInstanceId <= 0) {
			return;
		}

		DungeonManager entry = DungeonManager.instances.get(c.dungInstanceId);
		if (entry == null) {
			//System.out.println("entry null " +c.dungInstanceId + ":" + DungeonManager.instances);
			c.clearDung();
			c.getPA().movePlayer(DungeonConstants.OUTSIDE);
			c.dungInstanceId = 0;
			return;
		}

		//TODO more checks later on?
		c.dungParty = entry.party;
		c.dungParty.partyMembers.add(c);
		updatePartyMembers(c);
		c.updateRegion = true;
		c.mapRegionDidChange = true;
		c.dungTeleported = true;
		c.dungParty.dungManager.initStuffOnStart(c);
		c.sendRegionChanged();
		//System.out.println("entry found");
		if (c.dungParty.partyMembers.size() > 0) {
			c.dungParty.dungManager.endCleanup();
			//System.out.println("end cleanup event.");
		}
	}

	public static void handleLogout(Client c) {
		if (c.dungParty == null || c.dungParty.dungManager == null) {
			return;
		}

		c.dungParty.dungManager.sayBye(c, false, true);
		if (c.dungParty.partyMembers.size() == 1) {//Only one person inside the party.
			c.dungParty.dungManager.startCleanupEvent();
		}

		leavePartyFast(c, true);
	}

	public static void leaveDungeon(Client c, boolean properLeave) {
		if (c.dungParty == null || c.dungParty.dungManager == null) {
			return;
		}

		c.teleTimer = 0;
		if (c.dungParty.partyMembers.size() == 1) {//Only one person inside the party.
			c.clearDung();
			c.dungParty.dungManager.destroy(true);
			c.getPA().movePlayer(DungeonConstants.OUTSIDE);
		} else {
			c.dungParty.dungManager.sayBye(c, true, properLeave);
		}

		leavePartyFast(c, true);
		updateSidebar(c, false);
	}

	public static void leaveParty(Client c) {
		if (c.insideDungeoneering()) {
			if (c.npcIndex > 0 || c.underAttackBy2 > 0) {
				c.sendMessage("You can't leave party in combat.");
				return;
			}

			c.getDialogueBuilder().sendStatement("Warning: If you leave the dungeon, you will not be able to return!").sendOption("Leave the dungeon for good?", "Yes.", () -> {
				leaveDungeon(c, true);
			}, "No.", () -> {}).execute();
		} else {
			leavePartyFast(c, true);
		}
	}

	public static void leavePartyFast(Client c, boolean buttonLeave) {
		if (c.dungParty == null) {
			c.sendMessage("You're not in a party to leave.");
			return;
		}

		c.sendMessage("You leave the party.");
		c.dungParty.partyMembers.remove(c);
		if (!c.dungParty.partyMembers.isEmpty() && c == c.dungParty.c) {
			c.dungParty.c = c.dungParty.partyMembers.get(0);
		}

		if (buttonLeave) {
			updatePartyMembers(c);
		}

		c.dungParty = null;
		c.getPacketSender().sendConfig(8223, 0);
	}

	public static void openInviteInterface(Client c, Party party, Client o) {
		if (c == null || party == null || party.partyMembers == null || party.partyMembers.isEmpty()) {
			return;
		}

		fillInvitePartyMembers(c, party, 44119);
		c.getPacketSender().showInterface(44100);
		c.dungPartyInvite = o.getId();
	}

	public static void updateGuide(Client c) {
		c.getPacketSender().sendConfig(ConfigCodes.DUNG_GUIDE_MODE, c.dungParty.guideMode ? 1 : 0);
	}

	public static void updateFloorInfo(Client c) {
		int floor;
		int complexity;
		if (c.dungParty != null) {
			floor = c.dungParty.floor;
			complexity = c.dungParty.getComplexity();
		} else {
			floor = 0;
			complexity = 0;
		}

		c.getPacketSender().sendString(Integer.toString(floor), 44037);
		c.getPacketSender().sendString(Integer.toString(complexity), 44038);

		updateProgress(c);
	}

	public static void updateSidebar(Client c, boolean open) {
		boolean inParty = c.dungParty != null;
		c.getPacketSender().setVisibility(44051, inParty);
		c.getPacketSender().setVisibility(44071, !inParty);

		updateFloorInfo(c);

		fillPartyMembers(c, c.dungParty, 44041);
		if (open) {
			c.getPacketSender().sendFrame106(2);
			c.getPacketSender().setSidebarInterface(2, 44026);
		}
	}
	
	public static boolean handleOptions(Client c, int id, int option) {
		if (id < 44041 || id > 44045) {
			return false;
		}
		
		if (c.dungParty == null) {
			return true;
		}

		int index = id - 44041;

		if (index >= c.dungParty.partyMembers.size()) {
			return true;
		}

		Client other = c.dungParty.partyMembers.get(index);
		if (other == null) {//Not needed but lets keep it
			return true;
		}

		switch(option) {
			case 0://Inspect
				c.sendMessage("Coming soon tm.");
				return true;
			case 1://Kick
				if (c != c.dungParty.partyMembers.get(0)) {
					c.sendMessage("Only party leader can kick.");
					return true;
				}
				if (other == c) {
					c.sendMessage("Can't kick yourself.");
					return true;
				}

				leavePartyFast(other, true);
				updateSidebar(other, false);
				return true;
			case 2://Promote
				c.sendMessage("Coming soon tm.");
				return true;
		}

		return false;
	}
	
	public static boolean handleButtons(Client c, int buttonId) {
		if (buttonId >= 95126 && buttonId <= 95185) {//Floor select
			if (c.dungParty == null || c.insideDungeoneering()) {
				return true;
			}

			if (c.dungParty.partyMembers.get(0) != c) {
				c.sendMessage("Only the party leader can change these settings.");
				return true;
			}

			int index = buttonId - 95125;

			for (int i = 0, length = c.dungParty.partyMembers.size(); i < length; i++) {
				Client partyMember = c.dungParty.partyMembers.get(i);
				if (index > partyMember.dungFloorsUnlocked) {
					c.sendMessage(partyMember.getNameSmartUp() + " is unable to access that floor. Use the bars on the floor select");
					c.sendMessage("interface to see which floors your party can access.");
					return true;
				}
			}

			c.selectedFloor = index;
			c.getPacketSender().sendString(Integer.toString(c.selectedFloor), 46047);
			return true;
		}
		
		switch(buttonId) {
		    case 172071:
		    case 172072: {//decline invitation
		    	if (c.dungParty != null) {
					c.getPA().closeAllWindows();
					return true;
				}
		    	
		    	Client other = getInviteOfferer(c);
				if (other == null || other.dungParty == null) {
					c.sendMessage("Invalid party request.");
					c.getPA().closeAllWindows();
					return true;
				}
		    	
				c.expireTicks = 0;
				other.expireTicks = 0;
				c.dungPartyInvite = -1;
				other.dungPartyInvite = -1;
				
				c.getPA().closeAllWindows();
		    	return true;
		    }
			
			case 172070: {//accept invitation
				if (c.dungParty != null) {
					c.getPA().closeAllWindows();
					return true;
				}
				
				Client o = getInviteOfferer(c);
				if (o == null || o.dungParty == null) {
					c.sendMessage("Invalid party request.");
					c.getPA().closeAllWindows();
					return true;
				}

				if (o.dungParty.getComplexity() > c.getMaxComplexity()) {
					c.sendMessage("Unable to access this party complexity.");
					c.getPA().closeAllWindows();
					return true;
				}

				if (o.dungParty.floor > c.dungFloorsUnlocked) {
					c.sendMessage("Unable to access this party floor.");
					c.getPA().closeAllWindows();
					return true;
				}

				c.dungParty = o.dungParty;
				c.dungParty.partyMembers.add(c);

				updateSidebar(c, false);
				updatePartyMembers(c);

				c.expireTicks = 0;
				o.expireTicks = 0;
				c.dungPartyInvite = -1;
				o.dungPartyInvite = -1;
				
				c.getPA().closeAllWindows();
				return true;
			}
			case 172020: { //form party
				if (c.dungParty != null) {
					updateSidebar(c, false);
					return true;
				}

				if (!c.inDaemonheim()) {
					c.sendMessage("You must be inside Daemonheim lobby to form a party.");
					return true;
				}

				c.sendMessage("You have been set as the party leader.");
				createParty(c, false);
				return true;
			}
			case 172042://invite player
				c.sendMessage("Right click and click invite to invite a player to your party.");
				return true;
			case 172040://leave party
				leaveParty(c);
				updateSidebar(c, false);
				return true;
			case 172044://change floor
				if (c.dungParty == null) {
					return true;
				}

				updatePartyFloors(c);
				c.getPacketSender().sendString(Integer.toString(c.selectedFloor), 46047);

				c.getPacketSender().showInterface(46040);
				return true;
			case 179219://confirm floor change
				if (c.dungParty == null) {
					return true;
				}
				
				if (c.insideDungeoneering()) {
					c.sendMessage("You can't select a new floor when you are already in a dungeon.");
					return true;
				}

				if (c.dungParty.partyMembers.get(0) != c) {
					c.sendMessage("Only the party leader can change these settings.");
					return true;
				}

				for (int i = 0, length = c.dungParty.partyMembers.size(); i < length; i++) {
					Client partyMember = c.dungParty.partyMembers.get(i);
					if (c.selectedFloor > partyMember.dungFloorsUnlocked) {
						c.sendMessage(partyMember.getNameSmartUp() + " is unable to access that floor. Use the bars on the floor select");
						c.sendMessage("interface to see which floors your party can access.");
						return true;
					}

					if (c.selectedFloor > DungeonUtils.getMaxFloor(partyMember.getSkills().getStaticLevel(Skills.DUNGEONEERING))) {
						c.sendMessage(partyMember.getNameSmartUp() + " is unable to access that floor. Dungeoneering level too low.");
						return true;
					}
				}

				c.dungParty.floor = c.selectedFloor;
				c.getPA().closeAllWindows();
				updatePartyMembers(c);
				return true;
			case 172045: {//Change complexity
				if (c.dungParty == null) {
					return true;
				}
				
				if (c.insideDungeoneering()) {
					c.sendMessage("You can't change your complexity when you are already in a dungeon.");
					return true;
				}

				if (c.dungParty.partyMembers.get(0) != c) {
					c.sendMessage("Only the party leader can change these settings.");
					return true;
				}

				updateComplexityInfo(c);
				c.getPacketSender().showInterface(43672);
				return true;
			}
			case 99079:
			case 99081:
			case 99083:
			case 99085:
			case 99087:
			case 99089: {//Select complexity
				if (c.dungParty == null || c.insideDungeoneering()) {
					return true;
				}
				
				if (c.dungParty.partyMembers.get(0) != c) {
					c.sendMessage("Only the party leader can change these settings.");
					return true;
				}
				
				int complexity = ((buttonId - 99077) / 2);

				if (complexity != 1) {//complexity 1 isn't locked.
					for (int i = 0, length = c.dungParty.partyMembers.size(); i < length; i++) {
						Client partyMember = c.dungParty.partyMembers.get(i);
						if (!c.isComplexityLocked(complexity) && partyMember.isComplexityLocked(complexity)) {
							c.sendMessage(partyMember.getNameSmartUp() + " is unable to access that complexity level. Use the select complexity");
							c.sendMessage("interface to see which complexity levels your party can access.");
							return true;
						}
					}

					if (c.isComplexityLocked(complexity)) {
						c.sendMessage("This complexity is locked.");
						return true;
					}
				}

				c.selectComplexity(complexity);
				updateComplexityInfo(c);
				return true;
			}
			case 170155://confirm complexity
				if (c.dungParty == null || c.insideDungeoneering()) {
					return true;
				}

				if (c.dungParty.partyMembers.get(0) != c) {
					c.sendMessage("Only the party leader can change these settings.");
					return true;
				}

				c.dungParty.setComplexity(c.getSelectedComplexity());
				c.getPA().closeAllWindows();
				updatePartyMembers(c);
				return true;
			case 171252://Reset
				c.getDialogueBuilder().sendStatement("<col=8D4938>Are you sure you want to reset your dungeon progress? Your", "<col=8D4938>previous progress will be set to the number of floors you have", "<col=8D4938>completed and all floors will be marked as incomplete.", "<col=8D4938>This cannot be undone.").sendOption("Select an Option", "Yes, reset my progress.", () -> {
					int currentProgress = calcCurrentProgress(c);
					if (currentProgress == 0) {
						c.sendMessage("You can't reset progress at this moment.");
						return;
					}

					c.previousProgress = currentProgress;
					c.resetFloorsCompleted();
					updateProgress(c, 0);
				}, "No, don't reset my progress.", () -> {}).execute();
				return true;
			case 172014://Close
				c.getPacketSender().setSidebarInterface(2, 638);
				return true;
			case 172048://Guide mode
				if (c.dungParty == null) {
					c.sendMessage("You must be in a party to do that.");
					return true;
				}

				if (c.insideDungeoneering()) {
					c.sendMessage("You cannot change the guide mode once the dungeon has started.");
					return true;
				}

				if (c.dungParty.partyMembers.get(0) != c) {
					c.sendMessage("Only the party leader can change the guide mode.");
					return true;
				}

				c.dungParty.guideMode = !c.dungParty.guideMode;
				for (int i = 0, length = c.dungParty.partyMembers.size(); i < length; i++) {
					Client partyMember = c.dungParty.partyMembers.get(i);
					if (c.dungParty.guideMode) {
						partyMember.sendMessage("Guide mode enabled. Your map will show you the critical path, but you will receive");
						partyMember.sendMessage("an xp penalty.");
					} else {
						partyMember.sendMessage("Guide mode disabled. Your map will no longer show critical path.");
					}

					updateGuide(partyMember);
				}
				return true;
			default:
//				System.out.println(buttonId);
				return false;
		}
	}

	private static void updateComplexityInfo(Client c) {
		int minComplexityUnlockedFlag = 0;
		compLoop: for (int complexity = 2; complexity <= 6; complexity++) {
			for (int i = 0; i < c.dungParty.partyMembers.size(); i++) {
				Client player = c.dungParty.partyMembers.get(i);
				if (player.isComplexityLocked(complexity)) {
					break compLoop;
				}
			}

			minComplexityUnlockedFlag |= 1 << (complexity - 2);
		}

		int complexity = c.getSelectedComplexity();
		int penalty = complexity == 6 ? 0 : ((6 - complexity) * 5 + 25);
		c.getPacketSender().sendString(penalty + "% XP Penalty", 29996);
		c.getPacketSender().sendString(Integer.toString(complexity), 43679);
		c.getPacketSender().sendConfig(8214, minComplexityUnlockedFlag + (complexity << 5));
	}

	public static void createParty(Client c, boolean open) {
		Client o = getInviteOfferer(c);
		if (o != null) {
			o.dungPartyInvite = -1;
		}

		c.dungPartyInvite = -1;
		c.dungParty = new Party(c);
		updateSidebar(c, open);
	}

	private static final int[] SKILLS = {Skills.ATTACK, Skills.STRENGTH, Skills.DEFENSE, Skills.RANGED, Skills.MAGIC, Skills.PRAYER, Skills.HITPOINTS, Skills.SUMMONING};
	private static final int MULTIPLIER = 5;

	public static int calcAvg(Client c) {
		int avg = 0;
		for (int skill : SKILLS) {
			avg += c.getSkills().getStaticLevel(skill) * MULTIPLIER;
		}

		return avg;
	}

	public static int highestAvg(Party party) {
		int highest = 0;

		int length = party.partyMembers.size();
		for (int i = 0; i < length; i++) {
			int avg = calcAvg(party.partyMembers.get(i));
			if (avg > highest) {
				highest = avg;
			}
		}

		return highest;
	}

	public static int calcPartyAvg(Party party) {
		int totalAvg = 0;

		int length = party.partyMembers.size();
		for (int i = 0; i < length; i++) {
			totalAvg += calcAvg(party.partyMembers.get(i));
		}

		totalAvg /= length;
		return totalAvg;
	}
	
	public static int calcPartyAvgDungLvls(Party party) {
		int totalDungLevel = 0;

		int length = party.partyMembers.size();
		for (int i = 0; i < length; i++) {
			totalDungLevel += party.partyMembers.get(i).getSkills().getStaticLevel(Skills.DUNGEONEERING);
		}

		totalDungLevel /= length;
		return totalDungLevel;
	}

	public void startDungeon() {
		if (disableDung) {
			int length = partyMembers.size();
			for (int i = 0; i < length; i++) {
				partyMembers.get(i).sendMessage("Dungeoneering has been disabled.");
			}
			return;
		}
		
		if (!allowStart(c)) {
			return;
		}

//		List<Client> removeMembers = new ArrayList<>(MAX_PARTY_MEMBERS);
//		
//		int length = partyMembers.size();
//		for (int i = 0; i < length; i++) {
//			Client other = partyMembers.get(i);
//			if (!other.inDaemonheim()) {
//				removeMembers.add(other);
//			}
//		}
//		
//		for (int i = 0, len = removeMembers.size(); i < len; i++) {
//			Client other = removeMembers.get(i);
//			if (other.isActive) {
//				Party.leavePartyFast(other, true);
//			}
//		}

		Client offered = getInviteOfferer(c);
		if (offered != null) {
			offered.dungPartyInvite = -1;
			offered.getPA().closeAllWindows();
		}

		c.dungPartyInvite = -1;
		dungManager = new DungeonManager(this);
		dungManager.start();
	}
	
	public static boolean allowStart(Client c) {
		if (c.dungParty == null) {
			int index = c.getItems().getItemSlot(DungeonConstants.RING_OF_KINSHIP);

			if (index == -1 && !c.getItems().playerHasEquipped(DungeonConstants.RING_OF_KINSHIP)) {
				c.sendMessage("You need Ring of Kinship to start the party.");
				return false;
			}

			c.getDialogueBuilder().sendOption("Would you like to start a party?", "Yes.", () -> {Party.createParty(c, true);}, "No.", () -> {}).execute();
			return false;
		}
		if (c.dungParty.partyMembers.get(0) != c) {
			c.sendMessage("Only the party leader can start a dungeon.");
			return false;
		}

		int index = c.getItems().getItemSlot(DungeonConstants.RING_OF_KINSHIP);
		
		if (index == -1 && !c.getItems().playerHasEquipped(DungeonConstants.RING_OF_KINSHIP)) {
			c.sendMessage("You need Ring of Kinship to enter a dungeon.");
			return false;
		}

		if (c.summoned != null) {
			c.sendMessage("You can't bring any pets into Daemonheim.");
			return false;
		}

		int size = c.dungParty.partyMembers.size();
		
		//check for items
		for (int i = 0; i < size; i++) {
			Client partyMember = c.dungParty.partyMembers.get(i);

			if (!partyMember.inDaemonheim()) {
				c.sendMessage(partyMember.getNameSmartUp() + " is outside Daemonheim lobby.");
				return false;
			}

			int kinshipIndex = partyMember.getItems().getItemSlot(DungeonConstants.RING_OF_KINSHIP);

			if (kinshipIndex == -1 && !partyMember.getItems().playerHasEquipped(DungeonConstants.RING_OF_KINSHIP)) {
				c.sendMessage(partyMember.getNameSmartUp() + " doesn't have Ring of Kinship in his inventory.");
				return false;
			}

			if (partyMember.summoned != null) {
				c.sendMessage(partyMember.getNameSmartUp() + " has a pet summoned and cannot enter Daemonheim.");
				return false;
			}

			for (int j = 0, length = partyMember.playerItems.length; j < length; j++) {
				int id = partyMember.playerItems[j] - 1;
				if (id != -1 && id != DungeonConstants.RING_OF_KINSHIP) {
					c.sendMessage(partyMember.getNameSmartUp() + " is carrying items that cannot be take into Daemonheim.");
					return false;
				}
			}

			for (int j = 0, length = partyMember.playerEquipment.length; j < length; j++) {
				int id = partyMember.playerEquipment[j];
				if (id != -1 && id != DungeonConstants.RING_OF_KINSHIP) {
					c.sendMessage(partyMember.getNameSmartUp() + " is wearing items that cannot be take into Daemonheim.");
					return false;
				}
			}
			
			//check in trade or duel
			if (partyMember.getTradeAndDuel().hasItemsInTradeOrDuel()) {
				c.sendMessage(partyMember.getNameSmartUp() + " is carrying items that cannot be take into Daemonheim.");
				return true;
			}
		}
		
		return true;
	}

	public static void updatePartyFloors(Client c) {
		if (false) {
			int partyMembers = Misc.random(5);
			System.out.println(partyMembers);
			
			for (int i = 0; i < 5; i++) {
				int val;
				if (i < partyMembers) {
					val = Misc.random(60);
				} else {
					val = 0;
				}

				c.getPacketSender().sendConfig(8200 + i, val);
			}

			for (int i = 0; i < partyMembers; i++) {
				for (int j = 0; j < 2; j++) {
					c.getPacketSender().sendConfig(8205 + i * 2 + j, Misc.random(Integer.MAX_VALUE));
				}
			}
			return;
		}

		Party party = c.dungParty;
		if (party != null) {
			int partyMembers = party.partyMembers.size();
			for (int i = 0; i < 5; i++) {
				int val;
				if (i < partyMembers) {
					Client partyMember = party.partyMembers.get(i);
					val = partyMember.dungFloorsUnlocked;
				} else {
					val = 0;
				}

				c.getPacketSender().sendConfig(8200 + i, val);
			}

			for (int i = 0; i < partyMembers; i++) {
				Client partyMember = party.partyMembers.get(i);

				for (int j = 0; j < 2; j++) {
					c.getPacketSender().sendConfig(8205 + i * 2 + j, partyMember.getDungFloorsCompleted(j));
				}
			}
		} else {
			for (int i = 0; i < 5; i++) {
				c.getPacketSender().sendConfig(8200 + i, 0);
			}
		}
	}
	
	public static Client getInviteOfferer(Client c) {
		if (c.dungPartyInvite == -1) {
			return null;
		}
		
		return (Client) PlayerHandler.players[c.dungPartyInvite];
	}

	private static void updatePartyMembers(Client c) {
		for (int i = 0, length = c.dungParty.partyMembers.size(); i < length; i++) {
			Client partyMember = c.dungParty.partyMembers.get(i);
			fillPartyMembers(partyMember, c.dungParty, 44041);

			updateFloorInfo(partyMember);
		}
	}

	public static int calcCurrentProgress(Player player) {
		int currentProgress = 0;
		for (int i = 1; i <= 60; i++) {
			if (player.isDungFloorCompleted(i)) {
				currentProgress++;
			}
		}

		return currentProgress;
	}

	public static void updateProgress(Player player, int currentProgress) {
		player.getPacketSender().sendString(Integer.toString(currentProgress), 44032);
		player.getPacketSender().sendString(Integer.toString(player.previousProgress), 44033);
	}

	public static int updateProgress(Player player) {
		int currentProgress = calcCurrentProgress(player);

		player.getPacketSender().sendString(Integer.toString(currentProgress), 44032);
		player.getPacketSender().sendString(Integer.toString(player.previousProgress), 44033);
		
		return currentProgress;
	}
	
	private static void fillInvitePartyMembers(Client c, Party party, int baseId) {
		int players = 5;
		
		int length = party.partyMembers.size();
		for (int i = 0; i < length; i++) {
			int id = i * players + baseId;
			Player player = party.partyMembers.get(i);
			c.getPacketSender().sendString(player.getNameSmartUp(), id);
			c.getPacketSender().sendString(Integer.toString(player.getPlayerLevel()[Skills.DUNGEONEERING]), id + 1);
			c.getPacketSender().sendString(Integer.toString(player.combatLevel), id + 2);
			c.getPacketSender().sendString(Integer.toString(player.getSkills().getMaxSkillLevel()), id + 3);
			c.getPacketSender().sendString(Integer.toString(player.getPA().getTotalLevel()), id + 4);
		}

		for (int i = length; i < players; i++) {
			int id = i * players + baseId;
			c.getPacketSender().sendString("", id);
			c.getPacketSender().sendString("", id + 1);
			c.getPacketSender().sendString("", id + 2);
			c.getPacketSender().sendString("", id + 3);
			c.getPacketSender().sendString("", id + 4);
		}

		c.getPacketSender().sendString(Integer.toString(party.floor), 44116);
		c.getPacketSender().sendString(Integer.toString(party.complexity), 44117);
	}
	
	private static void fillPartyMembers(Client c, Party party, int baseId) {
		if (c == null) {
			return;
		}
		
		if (party == null) {
			for (int i = 0; i < MAX_PARTY_MEMBERS; i++) {
				c.getPacketSender().sendString("", baseId + i);
			}
			return;
		}

		int length = party.partyMembers.size();
		for (int i = 0; i < length; i++) {
			c.getPacketSender().sendString(party.partyMembers.get(i).getNameSmartUp(), baseId + i);
		}

		for (int i = length; i < MAX_PARTY_MEMBERS; i++) {
			c.getPacketSender().sendString("", baseId + i);
		}
	}
	
	public int getComplexity() {
		return complexity;
	}

	public int getMaxLevel(int skill) {
		if (partyMembers.isEmpty()) {
			return 1;
		}

		int level = 0;
		for (Client player : partyMembers) {
			int lvl = player.getSkills().getStaticLevel(skill);
			if (lvl > level) {
				level = lvl;
			}
		}

		return level;
	}

	public void setComplexity(int complexity) {
		this.complexity = complexity;
	}

	public int getMaxFloor() {
		int floor = 60;

		for (Player player : partyMembers) {
			if (player.dungFloorsUnlocked < floor) {
				floor = player.dungFloorsUnlocked;
			}
		}

		return floor;
	}

	public boolean isGuideMode() {
		return guideMode || complexity < 5;
	}

	public int getMaxComplexity() {
		int complexity = 6;

		for (Player player : partyMembers) {
			int max = player.getMaxComplexity();
			if (max < complexity) {
				complexity = max;
			}
		}

		return complexity;
	}

}