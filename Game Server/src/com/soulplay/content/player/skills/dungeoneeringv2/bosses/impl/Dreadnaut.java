package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import java.util.ArrayList;

import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@BossMeta(ids = {12848, 12849, 12850, 12851, 12852, 12853, 12854, 12855, 12856, 12857, 12858, 12859, 12860, 12861, 12862})
public class Dreadnaut extends Boss {

	public Dreadnaut(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int dist = Misc.distanceToPoint(npc.getX(), npc.getY(), c.getX(), c.getY());
		
		if (Misc.random(4) == 1) {
			npc.dungVariables[1] = true;
		} else {
			npc.dungVariables[1] = false;
		}
		
		npc.dungMultiAttack = false;
		
		if (Misc.random((int) npc.dungVariables[2]) == 0) {
			npc.attackType = 5;
			npc.startAnimation(14982);
			npc.startGraphic(Graphic.create(2865));
			c.dealDamage(new Hit(Misc.random(npc.maxHit), 0, -1));
			AttackMob.disablePrayer(c, 8);
		} else if (dist < 4) {
			npc.attackType = 0;
			npc.startGraphic(Graphic.create(2856));
			if (!(Boolean) npc.dungVariables[1]) {
			   npc.projectileId = 2857;
		       npc.projectileDelay = 40;
		       npc.projectiletStartHeight = 0;
			} else {
				npc.projectileId = 2857;
			    npc.projectileDelay = 40;
			    npc.projectiletStartHeight = 0;
			    npc.dungMultiAttack = true;
			    //TODO: add the shit holes that appear after multi range randomly on the floor around players. drain prayer and deal some dmg
		    }
		}
	}

	@Override
	public void process(NPC npc) {
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.dungVariables[0] = new ArrayList<>();
		npc.dungVariables[1] = new Boolean(false);
		int rate = 10;
		if (npc.dungeonManager != null) {
			rate = Misc.interpolate(10, 4, 1, 5, npc.dungeonManager.party.partyMembers.size());
		}

		npc.dungVariables[2] = new Integer(rate);
	}

	@Override
	public boolean ignoreMeleeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int followDistance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		if (!p.insideDungeoneering()) {
			return;
		}
		if (npc.attackType > 0) {
			return;
		}
		
		final Boss boss = this;

		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer e) {
				int teamAvg = npc.dungeonManager.teamAvg;
				int minAvg = 85;
				int maxAvg = 3960;
				int oldType = npc.attackType;
				int oldMax = npc.maxHit;
				npc.attackType = 1;
				npc.endGfx = 2858;
				npc.endGraphicType = GraphicType.LOW;
				npc.maxHit = Misc.interpolate(2, 21, minAvg, maxAvg, teamAvg);
				if (!(Boolean) npc.dungVariables[1]) {
					NPCHandler.applyDamage(npc.getId(), (Client) p, false, boss);
				} else {
					//npc.projectileId = 2857;
					for (Player player : npc.dungeonManager.party.partyMembers) {
						if (!player.dungParty.dungManager.getCurrentRoomReference(player.getCurrentLocation()).equals(npc.reference)) {
							continue;
						}
	                    NPCHandler.applyDamage(npc.getId(), (Client) player, false, boss);	
					}
				}
				npc.attackType = oldType;
				npc.maxHit = oldMax;
				//npc.projectileId = -1;
				//npc.endGfx = -1;
				e.stop();
			}

			@Override
			public void stop() {

			}
		}, 1);
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}

}
