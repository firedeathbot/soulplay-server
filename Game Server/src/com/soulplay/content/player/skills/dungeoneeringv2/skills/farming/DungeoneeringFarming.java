package com.soulplay.content.player.skills.dungeoneeringv2.skills.farming;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

public class DungeoneeringFarming {

	private static final int GROWTH_TICKS = 100;
	private static final Animation HARVEST_ANIMATION = new Animation(3659);

	public static int getHerbForLevel(int level) {
		for (int i = 10; i < Harvest.values.length; i++) {
			if (Harvest.values[i].getLvl() == level) {
				return Harvest.values[i].getProduct();
			}
		}

		return 17448;
	}

	public static void initHarvest(Client player, Harvest harvest, GameObject object, DungeonManager dungeon, boolean isWarped) {
		final boolean isTextile = harvest.isTextile();

		if (!isTextile) {
			if (object.isActive() && object.getOwnerMID() != player.mySQLIndex) {
				player.sendMessage("This is not your farming patch.");
				return;
			}
		}

		int harvestCount = player.harvestCount;
		String productName = ItemDef.forID(harvest.getProduct()).getName().toLowerCase();

		if (isTextile) {
			if (player.getSkills().getLevel(Skills.FARMING) < harvest.getLvl()) {
				player.sendMessage(
						"You need a Farming level of " + harvest.getLvl() + " in order to pick " + productName + ".");
				return;
			}
		}

		if (harvestCount <= 0) {
			harvestCount = Misc.randomNoPlus(2, 5);
			player.harvestCount = harvestCount;
		}

		player.startAnimation(HARVEST_ANIMATION);
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (player.disconnected) {
					player.resetAnimations();
					container.stop();
					return;
				}

				if (harvest == Harvest.SPIRITBLOOM) {
					player.getAchievement().Pick300SpiritBlooms();
				}

				player.harvestCount--;
				player.getItems().addOrDrop(new GameItem(harvest.getProduct(), 1));
				player.sendMessage("You pick a " + productName + ".");
				player.getPA().addSkillXP(harvest.getExp(), Skills.FARMING);
				if (player.harvestCount <= 0) {
					if (isTextile) {
						player.sendMessage("You have depleted this resource.");
					}

					dungeon.spawnObject(isTextile ? object.getId() + 1 : isWarped ? DungeonConstants.FARMING_PATCH_CLEAN_WARPED : DungeonConstants.FARMING_PATCH_CLEAN, object.getLocation().getX(), object.getLocation().getY(), object.getOrientation(), object.getType());
				}
				container.stop();
			}

			@Override
			public void stop() {
				
			}
		}, 2);
	}

	public static void plantHarvest(int itemId, int itemSlot, final Client player, GameObject object, DungeonManager dungeon, boolean isWarped) {
		final Harvest harvest = Harvest.forSeed(itemId);
		if (harvest == null) {
			return;
		}

		if (player.getSkills().getLevel(Skills.FARMING) < harvest.getLvl()) {
			player.sendMessage("You need a Farming level of " + harvest.getLvl() + " in order to plant a " + ItemDef.forID(harvest.getSeed()).getName().toLowerCase() + ".");
			return;
		}

		int id;
		if (isWarped) {
			id = harvest.getGrownIdWarped();
		} else {
			id = harvest.getGrownId();
		}

		GameObject spawned = dungeon.spawnObject(id - 2, object.getLocation().getX(), object.getLocation().getY(), object.getOrientation(), object.getType());
		spawned.setOwnerMID(player.mySQLIndex);

		int uid = dungeon.farmEventsUid++;

		CycleEventContainer farmEvent = CycleEventHandler.getSingleton().addEvent(spawned, new CycleEvent() {

			int stage = 1;
			int baseId = spawned.getId();

			@Override
			public void execute(CycleEventContainer container) {
				int newId = baseId + stage;
				spawned.setId(newId);
				dungeon.getObjectManager().displayObject(spawned, newId);
				stage++;
				if (stage > 2) {
					container.stop();
					dungeon.farmingEvents.remove(uid);
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, GROWTH_TICKS);
		dungeon.farmingEvents.put(uid, farmEvent);

		player.startAnimation(HARVEST_ANIMATION);
		player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
	}

}
