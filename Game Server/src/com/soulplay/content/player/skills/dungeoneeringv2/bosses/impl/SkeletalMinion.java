package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import java.util.ArrayList;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {11722})
public class SkeletalMinion extends Boss {

	public SkeletalMinion(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.attackType = 0;
		
	}
	
	@Override
	public void process(NPC npc) {
		
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.isAggressive = true;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int followDistance() {
		return 14;
	}

	@Override
	public boolean canMove() {
		return true;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		NPC boss = (NPC) npc.dungVariables[Necrolord.SKELETAL_MINIONS];
		if (boss != null) {
			((ArrayList<NPC>) boss.dungVariables[Necrolord.SKELETAL_MINIONS]).remove(npc);
		}
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
