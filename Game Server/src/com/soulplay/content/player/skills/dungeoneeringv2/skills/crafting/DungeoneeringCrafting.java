package com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.crafting.Crafting;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

public class DungeoneeringCrafting {

	public static final int NEEDLE = 17446;
	public static final int THREAD = 17447;
	public static final Animation SPIN_ANIMATION = new Animation(1563);

	public static boolean canCraft(Client c, LeatherData data, int index) {
		if (!c.getItems().playerHasItem(NEEDLE)) {
			c.sendMessage("You don't have a needle to craft with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.CRAFTING] < data.getLevelReq()[index]) {
			c.sendMessage("You need a crafting level of " + data.getLevelReq()[index] + " to craft this.");
			return false;
		}

		if (!c.getItems().playerHasItem(data.getItemId(), data.getLeatherReqs()[index])) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to craft.").execute();
			return false;
		}

		return true;
	}

	public static boolean initCraft(Client c, int itemUsed, int usedWith) {
		LeatherData data = LeatherData.values.get(usedWith);
		if (data == null) {
			data = LeatherData.values.get(itemUsed);
			if (data == null) {
				return false;
			}
		}

		final LeatherData finalData = data;
		MakeInterface.make(c, data.getProducts()[0], ()->{
			startCraft(c, finalData, 0);
		}, data.getProducts()[1], ()->{
			startCraft(c, finalData, 1);
		}, data.getProducts()[2], ()->{
			startCraft(c, finalData, 2);
		}, data.getProducts()[3], ()->{
			startCraft(c, finalData, 3);
		}, data.getProducts()[4], ()->{
			startCraft(c, finalData, 4);
		});
		return true;
	}

	public static void startCraft(Client player, LeatherData data, int index) {
		player.getPA().closeAllWindows();

		if (!canCraft(player, data, index) || player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int cycles = player.makeCount;
			
			@Override
			public void execute(CycleEventContainer container) {
				player.getItems().deleteItem2(data.getItemId(), data.getLeatherReqs()[index]);
				player.startAnimation(Crafting.LEATHER_CRAFT_ANIM);
				player.getItems().addItem(data.getProducts()[index], 1);
				player.getPA().addSkillXP(data.getXp()[index], Skills.CRAFTING);
				player.sendMessage("You craft " + ItemDef.forID(data.getProducts()[index]).getName() + ".");
				
				cycles--;
				if (!player.insideDungeoneering() || cycles == 0 || !canCraft(player, data, index)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 2);
	}

	public static void initSpin(Client c) {
		Spinning data = null;
		for (int i = 0, length = c.playerItems.length; i < length; i++) {
			int id = c.playerItems[i] - 1;
			if (id == -1) {
				continue;
			}

			data = Spinning.values.get(id);
			if (data != null) {
				break;
			}
		}

		if (data == null) {
			c.sendMessage("You have nothing to spin.");
			return;
		}

		if (c.skillingEvent != null) {
			return;
		}

		final Spinning finalData = data;

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.startAnimation(SPIN_ANIMATION);
				c.getItems().deleteItem2(finalData.getPlantId(), 1);
				c.getItems().addItem(finalData.getProductId(), 1);
				c.getPA().addSkillXP(finalData.getXp(), Skills.CRAFTING);
				c.sendMessage("You spin " + ItemDef.forID(finalData.getPlantId()).getName() + ".");

				if (!c.insideDungeoneering() || !c.getItems().playerHasItem(finalData.getPlantId())) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				c.resetAnimations();
			}
		}, 2);
	}

}
