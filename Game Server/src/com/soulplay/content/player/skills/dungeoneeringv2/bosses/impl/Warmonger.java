package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@BossMeta(ids = {12752, 12753, 12754, 12755, 12756, 12757, 12758, 12759, 12760, 12761, 12762, 12763, 12764, 12765, 12766})
public class Warmonger extends Boss {
	
	private static final int PROGRESS = 0;
	public static final int SWORD_OBJ = 1;
	public static final int P2H_OBJ = 2;
	public static final int BOW_OBJ = 3;
	public static final int STAFF_OBJ = 4;
	public static final int MAUL_OBJ = 5;
	public static final int HP_FOR_NEXT_STAGE = 6;
	public static final int PULLING_ENABLED = 7;
	public static final int ANNOYANCE_TICK = 8;
	public static final int WARPED_SPHERE_VAR = 9;
	
	private static final Animation FLY_ANIM = new Animation(14995);
	//private static final Graphic DUST_GRAPHIC = Graphic.create(2870); // probably a particle gfx, which we don't have.
	
	private static final Animation[] GRAB_WEAPON_ANIMS = { new Animation(14924), new Animation(14925), new Animation(14927), new Animation(14926), new Animation(14928) };
	
	public static final Animation JUMP_SLAM_ANIM = new Animation(14968);
	//private static final Animation JUMP_SLAM_LAND_ANIM = new Animation(14969, Misc.serverToClientTick(3));
	public static final Graphic JUMP_SLAM_GFX = Graphic.create(2867);
	private static final Graphic STAFF_ATTACK_GFX = Graphic.create(2874);
	private static final Graphic MAGIC_ATTACK_END_GFX = Graphic.create(2873);
	private static final Graphic BOW_ATTACK_GFX = Graphic.create(2885);
	
	private static final Graphic DEATH_GFX = Graphic.create(2754);
	
	private static final Animation MELEE_ATTACK_NO_WEP_ANIM = new Animation(14392);
	private static final Animation SWORD_ATTACK_ANIM = new Animation(14416);
	private static final Animation STAFF_ATTACK_ANIM = new Animation(14996);
	private static final Animation P2H_ATTACK_ANIM = new Animation(14450);
	private static final Animation BOW_ATTACK_ANIM = new Animation(14537);
	private static final Animation MAUL_ATTACK_ANIM = new Animation(14963);
	
	private static final int MAGIC_ATTACK_PROJECTILE_ID = 2875; //2875 is original projectile that only shows particles
	
	//Warped Sphere anims
	public static final int WARPED_SPHERE_NPC_ID = 12842;
	private static final Animation WS_DEATH_ANIM = new Animation(13490);
	private static final Graphic WARPED_SPHERE_NPC_GFX = Graphic.create(2437);

	private static final Graphic WARPED_SPHERE_PLAYER_GFX = Graphic.create(2437);
	
	private static final Animation PLAYER_PULLED_ANIM = new Animation(14388);
	private static final Animation PLAYER_PRAYING_ANIM = new Animation(14419);

	public Warmonger(NPC boss) {
		super(boss);
	}

	private static void spawnWarpedSphere(NPC npc, boolean southSide) {
		NPC ws = (NPC) npc.dungVariables[WARPED_SPHERE_VAR];
		if (ws != null && !ws.isDead())
			ws.setDead(true);
		if (southSide) {
			int x = 2 + Misc.random(12);
			int y = 2 + Misc.random(5);
			npc.dungVariables[WARPED_SPHERE_VAR] = npc.dungeonManager.spawnNPC(npc.reference, WARPED_SPHERE_NPC_ID, x, y);
		} else {
			int x = 3 + Misc.random(9);
			int y = 10 + Misc.random(3);
			npc.dungVariables[WARPED_SPHERE_VAR] = npc.dungeonManager.spawnNPC(npc.reference, WARPED_SPHERE_NPC_ID, x, y);
		}
	}
	
	private static boolean checkSphereOverlap(final NPC boss, NPC sphere) {
		if (!sphere.getStopWatch().checkThenStart(4))
			return false;
		for (int i = 0, len = boss.dungeonManager.party.partyMembers.size(); i < len; i++) {
			Player p = boss.dungeonManager.party.partyMembers.get(i);
			if (p != null && p.isActive && !p.isDead()) {
				if (p.getX() == sphere.getCurrentLocation().getX() && p.getY() == sphere.getCurrentLocation().getY()) {
					return true;
				}		
			}
		}
		return false;
	}
	
	private static void telePlayers(final NPC boss, final NPC sphere) {
		
		final Location loc = sphere.getCurrentLocation().copyNew();

		NPCHandler.stepAway(sphere);

		final NPC clone = NPCHandler.spawnDungeoneering(WARPED_SPHERE_NPC_ID, loc.getX(), loc.getY(), loc.getZ(), 0, boss.dungeonManager, boss.reference);

		clone.setAliveTick(3);

		clone.startAnimation(WS_DEATH_ANIM);
		clone.startGraphic(WARPED_SPHERE_NPC_GFX);

		final int y = boss.dungeonManager.getRoomPosY(loc);

		final boolean southSide = y < 8;

		for (int i = 0, len = boss.dungeonManager.party.partyMembers.size(); i < len; i++) {
			Player p = boss.dungeonManager.party.partyMembers.get(i);
			if (p != null && p.isActive && !p.isDead() && p.getX() == loc.getX() && p.getY() == loc.getY()) {

				final Location newLoc = boss.dungeonManager.getTile(boss.reference, 4 + Misc.random(7), Misc.random(3) + (southSide ? 10 : 3));

				p.canWalk = false;

				CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
					int timer = 0;
					@Override
					public void stop() {

					}

					@Override
					public void execute(CycleEventContainer container) {
						timer++;

						if (timer == 2) {
							p.startAnimation(PLAYER_PRAYING_ANIM);
							p.startGraphic(WARPED_SPHERE_PLAYER_GFX);
						}

						if (timer > 6) {
							container.stop();

							p.getPA().movePlayer(newLoc);
							p.canWalk = true;
							p.startAnimation(Player.RESET_ANIM);
						}
					}
				}, 1);


			}
		}

	}
	
	private static int getSlamDamage(NPC npc) {
		
		int teamAvg = npc.dungeonManager.teamAvg;
		int minAvg = 85;
		int maxAvg = 3960;
		
		int stompDmg = Misc.interpolate(4, 40, minAvg, maxAvg, teamAvg);
	
		return stompDmg;
	}
	
	private static int getMagicDamage(NPC npc) {
		return Misc.random(35); //TODO: do the magic damage julius
	}
	
	private static boolean hitOverlapingPlayers(NPC npc) {
		boolean overlappingPlayers = false;
		for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
			Player p = npc.dungeonManager.party.partyMembers.get(i);
			if (p != null && p.isActive && !p.isDead() && Location.overlaps(p.getX(), p.getY(), 1, npc.getX(), npc.getY(), 5)) {
				p.dealTrueDamage(getSlamDamage(npc), 3, null, npc);
				overlappingPlayers = true;
			}
		}
		return overlappingPlayers;
	}
	
	public static boolean groundSlam(NPC npc) {
		if (!npc.isAttackable()) {
			return false;
		}
		boolean overlappingPlayers = false;
		for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
			Player p = npc.dungeonManager.party.partyMembers.get(i);
			if (p != null && p.isActive && !p.isDead() && Location.overlaps(p.getX(), p.getY(), 1, npc.getX(), npc.getY(), 5)) {
				overlappingPlayers = true;
				Location shove = Location.getShoved(npc.getCurrentLocation(), p.getCurrentLocation(), p.getDynamicRegionClip());
				p.getPA().walkTo(shove.getX(), shove.getY());
			}
		}
		
		if (overlappingPlayers) {
			npc.startAnimation(JUMP_SLAM_ANIM);
			npc.startGraphic(JUMP_SLAM_GFX);
			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
				
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					if (npc.isActive && npc.dungeonManager != null && npc.dungeonManager.party != null) {
						for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
							Player p = npc.dungeonManager.party.partyMembers.get(i);
							if (p != null && p.isActive && !p.isDead() && p.withinDistance(npc.getX()+2, npc.getY()+2, npc.getZ(), 7)) {
								p.dealTrueDamage(getSlamDamage(npc), 1, null, npc);
							}
						}
					}
					
				}
			}, 1);
			
		}
		return overlappingPlayers;
	}
	
	private static void getNextWeapon(NPC npc, final int transformId, final Direction traverseDir, final Direction faceObj, final int stepsX, final int stepsY, final Animation traverseAnim, final GameObject weapon, final int animIndex) {
		final int ticks = animIndex == 0 ? 7 : 4; // walking to sword needs more delay
		ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 2, 0, traverseDir).addSecondDirection(stepsX, stepsY, ticks, 0).setLockActionsTick(15);
		npc.setForceMovement(mask);
		npc.resetFace();
		npc.faceLocation(weapon.getLocation());
		npc.resetAttack();
		npc.setAttackTimer(18);
		npc.getStopWatch().startTimer(30); //25
		npc.setAttackable(false);
		npc.setKillerId(0);
		npc.setRetargetTick(30);
		if (traverseAnim != null)
			npc.startAnimation(traverseAnim);
		
		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
			int timer = 0;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (npc.isDead() || !npc.isActive)
					container.stop();
				
				timer++;
				
				npc.healNpc(1);
				
				if (timer == 12) {

					npc.faceLocation(npc.getX()+2+faceObj.getStepX()*6, npc.getY()+2+faceObj.getStepY()*6);
				}
				
				if (timer == 13) {
					
					npc.faceLocation(npc.getX()+2+faceObj.getStepX()*6, npc.getY()+2+faceObj.getStepY()*6);
					npc.startAnimation(GRAB_WEAPON_ANIMS[animIndex]);
					npc.transform(transformId);
					weapon.setTicks(2);
					
				}
				if (timer >= 15) {
					container.stop();
					npc.setAttackable(true);
					if (animIndex > 0) {
						boolean southSide = animIndex == 1 || animIndex == 3 ? true : false;
						spawnWarpedSphere(npc, southSide);
					}
				}
				
			}
		}, 1);
		
	}
	
	private static boolean checkForSafeSpotters(NPC npc) {
		if ((Boolean) npc.dungVariables[PULLING_ENABLED] == false) {
			return false;
		}
		
		int npcRoomPosY = npc.dungeonManager.getRoomPosY(npc.getCurrentLocation(), 5);
		
		boolean npcSouthSide = npcRoomPosY < 8;
		
		for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
			Player p = npc.dungeonManager.party.partyMembers.get(i);
			if (p == null || !p.isActive || !p.dungParty.dungManager.getCurrentRoomReference(p.getCurrentLocation()).equals(npc.reference))
				continue;
			
			//todo: add safespot location checks here
			int roomPosX = npc.dungeonManager.getRoomPosX(p.getCurrentLocation(), 1);
			int roomPosY = npc.dungeonManager.getRoomPosY(p.getCurrentLocation(), 1);
			boolean playerSouthSide = roomPosY < 8;
			
			if (roomPosX == 1 && roomPosY == 7 || npcSouthSide != playerSouthSide) {
				pullPlayers(npc, npcSouthSide);
				return true;
			}
		}
		return false;
	}
	
	//NOTE pull players who are on wrong side. if in pull stage and pull timer > 35 then grab all players over to other side if any on wrong side still
	public static void pullPlayers(NPC npc, boolean npcSouthSide) {
		npc.getStopWatch().startTimer(10);
		
		npc.setAttackTimer(10);
		
		CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				
				int npcRoomPosX = npc.dungeonManager.getRoomPosX(npc.getCurrentLocation(), 5);
				
				final Location loc = npc.dungeonManager.getTile(npc.reference, npcRoomPosX+2, npcSouthSide ? 8 : 9, 1, 1);
				
				// loop all players who are inside the room
				for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
					Player p = npc.dungeonManager.party.partyMembers.get(i);
					if (p == null || p.disconnected || p.isDead() || p.performingAction || !p.isActive || !p.dungParty.dungManager.getCurrentRoomReference(p.getCurrentLocation()).equals(npc.reference))
						continue;
					

					p.startAnimation(PLAYER_PULLED_ANIM);

					final int dirX = loc.getDx(p.getCurrentLocation());
					final int dirY = loc.getDy(p.getCurrentLocation());
					
					final Direction dir = Direction.getLogicalDirection(loc, npc.getCurrentLocation());
					ForceMovementMask mask = ForceMovementMask.createMask(dirX, dirY, 1, 0, dir).setNewOffset(p.getX(), p.getY());
					p.setForceMovement(mask);
					p.performingAction = true;
					CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
						int timer = 0;
						@Override
						public void stop() {

						}

						@Override
						public void execute(CycleEventContainer container) {
							timer++;
							if (timer == 1) {
								p.forceChat("Ow!");
							}
							if (timer >= 1) {
								p.dealDamage(new Hit(Misc.random(1) + 1, 0, -1));
							}
							if (timer >= 5) {
								p.startAnimation(PLAYER_PULLED_ANIM);
								ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 3, 0, dir)
										.addSecondDirection(dir.getStepX(), dir.getStepY(), 2, 0).setCustomMovePlayerTick(3);
								p.setForceMovement(mask);
								container.stop();
							}
						}
					}, 1);

				}
				
				npc.forceChat("YOU DARE HIDE FROM ME!");
				npc.startAnimation(STAFF_ATTACK_ANIM);
				//TODO: add gfx for lava flying up
				npc.turnNpc(loc.getX(), loc.getY());
				
			}
		}, 1);
		
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.projectileId = 0;
		if ((Integer)npc.dungVariables[PROGRESS] == 6) { // staff attack stage

			if (npc.getAttackTimer() <= 0) {
				boolean hitPlayer = false;
				for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
					Player p = npc.dungeonManager.party.partyMembers.get(i);
					if (p == null || !p.isActive || !p.dungParty.dungManager.getCurrentRoomReference(p.getCurrentLocation()).equals(npc.reference))
						continue;

					if (!hitPlayer)
						hitPlayer = true;

					final int hitDelay = NpcHitPlayer.getHitDelay(npc, p);

					NpcHitPlayer.dealMagicDamage(npc, p, getMagicDamage(npc), hitDelay, MAGIC_ATTACK_END_GFX);

					Projectile proj = new Projectile(MAGIC_ATTACK_PROJECTILE_ID, Location.create(npc.getX()+1,
							npc.getY()+1, npc.getHeightLevel()), p.getCurrentLocation());
					proj.setStartHeight(75);
					proj.setStartDelay(40);
					proj.setLockon(p.getProjectileLockon());
					GlobalPackets.createProjectile(proj);

					npc.face(p);
				}
				
				if (hitPlayer) {
					npc.setAttackTimer(4);//set delay on next attack
					npc.startGraphic(STAFF_ATTACK_GFX);
				}
			}
			
		} else if ((Integer)npc.dungVariables[PROGRESS] == 10) { // bow attack stage
			npc.projectileId = 2886;
			npc.projectiletStartHeight = 75;
			npc.projectileDelay = 50;
			npc.projectiletSlope = 0;
			npc.startGraphic(BOW_ATTACK_GFX);
			npc.attackType = 1;
		}
	}

	@Override
	public void process(NPC npc) {
		if (npc.dungVariables[PROGRESS] == null) // test npc
			return;
		if (!npc.getStopWatch().complete())
			return;
		
		NPC sphere = (NPC) npc.dungVariables[WARPED_SPHERE_VAR];
		if (sphere != null && !sphere.isDead() && checkSphereOverlap(npc, sphere)) {
			telePlayers(npc, sphere);
		}
		
		if (checkForSafeSpotters(npc))
				return;
		
		if ((Integer) npc.dungVariables[PROGRESS] > 1 && groundSlam(npc)) {
			npc.setAttackTimer(5);
			npc.getStopWatch().startTimer(7);
		}
		
		//npc.forceChat("stage "+(Integer) npc.dungVariables[PROGRESS]+" nextHP:"+((Integer)npc.dungVariables[HP_FOR_NEXT_STAGE]) + " currentHP:" + npc.getSkills().getLifepoints());
		
		switch((Integer) npc.dungVariables[PROGRESS]) {
		case 0: {
			npc.getStopWatch().startTimer(5);
			npc.dungVariables[PROGRESS] = 1;
			
			npc.walkUpToHit = true;//test
			
			npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints()*0.86));
		}
		break;
		case 1: {
			npc.getStopWatch().startTimer(10);
			npc.dungVariables[PROGRESS] = 2;
			
			npc.resetFace();
			
			final int mapRotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
			final Direction dir = Direction.get((mapRotation+2) % 4);
			
			final int xDir = dir.getStepX() * 6;
			final int yDir = dir.getStepY() * 6;
			
			final ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 2, 0, dir).addSecondDirection(xDir, yDir, 4, 0);
			npc.setForceMovement(mask);
			npc.startAnimation(FLY_ANIM);
			npc.setAttackTimer(12);
			npc.setAttackable(false);
			
			CycleEventHandler.getSingleton().addEvent(npc, new CycleEvent() {
				
				@Override
				public void stop() {
					
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					if (!npc.isActive || npc.isDead())
						return;
					
					hitOverlapingPlayers(npc);
					npc.setAttackable(true);
				}
			}, 9);
			
		}
		break;
		
		case 2: { // melee form
			
		}
		break;
		
		case 3: { // get sword
			npc.dungVariables[PROGRESS] = 4;
			
			npc.dungVariables[PULLING_ENABLED] = true;
			
			npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints()*0.71));
			
			npc.setAttackable(false);
			
//			npc.forceChat("step 3 done "+npc.dungeonManager.getRoom(npc.reference).getRotation());
			
			npc.attackAnim = SWORD_ATTACK_ANIM;
			
			final int mapRotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
			final Direction traverseDir = Direction.getNonDiagonal(mapRotation+3);
			
			final Location newLoc = npc.dungeonManager.getTile(npc.reference, 2, 1, 5, 5); // the x/y location can be stored in an enum for transformation types of the boss
			
			final int stepsX = newLoc.getDx(npc.getCurrentLocation());
			final int stepsY = newLoc.getDy(npc.getCurrentLocation());
			
			final GameObject sword = (GameObject) npc.dungVariables[SWORD_OBJ];
			
			final Direction dir = Direction.getNonDiagonal(sword.getOrientation()+1);
			
			getNextWeapon(npc, npc.npcType+15, traverseDir, dir, stepsX, stepsY, null, sword, 0);
			

			npc.attackType = 0;
		}
		break;
		
		
		case 4: { // attack with sword till hp runs low
			
			
		}
		break;
		
		case 5: { // get chaotic staff
			npc.getStopWatch().startTimer(20);
			npc.dungVariables[PROGRESS] = 6;
			
			npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints()*0.57));
			
			npc.attackAnim = STAFF_ATTACK_ANIM;
			
			npc.walkUpToHit = false;
			
			npc.dungVariables[PULLING_ENABLED] = (boolean) false;
			
			final int mapRotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
			final Direction traverseDir = Direction.getNonDiagonal(mapRotation);
			
			final Location newLoc = npc.dungeonManager.getTile(npc.reference, 8, 10, 5, 5); // the x/y location can be stored in an enum for transformation types of the boss
			
			final int stepsX = newLoc.getDx(npc.getCurrentLocation());
			final int stepsY = newLoc.getDy(npc.getCurrentLocation());
			
			final GameObject staff = (GameObject) npc.dungVariables[STAFF_OBJ];
			
			getNextWeapon(npc, npc.npcType+30, traverseDir, traverseDir, stepsX, stepsY, FLY_ANIM, staff, 1);
			

			npc.attackType = 2;
			
			npc.stopAttack = true;
		}
		break;
		
		case 6: { // staff attack
			
			
		}
		break;
		
		case 7: { // get primal 2h sword
			npc.stopAttack = false;
			npc.getStopWatch().startTimer(20);
			npc.dungVariables[PROGRESS] = 8;
			
			npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints()*0.43));
			
			npc.attackAnim = P2H_ATTACK_ANIM;
			
			npc.walkUpToHit = true;
			
			npc.dungVariables[PULLING_ENABLED] = true;
			
			final int mapRotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
			final Direction traverseDir = Direction.getNonDiagonal(mapRotation+2);
			
			final Location newLoc = npc.dungeonManager.getTile(npc.reference, 10, 1, 5, 5);
			
			final int stepsX = newLoc.getDx(npc.getCurrentLocation());
			final int stepsY = newLoc.getDy(npc.getCurrentLocation());
			
			final GameObject p2h = (GameObject) npc.dungVariables[P2H_OBJ];
			
			getNextWeapon(npc, npc.npcType+45, traverseDir, traverseDir, stepsX, stepsY, FLY_ANIM, p2h, 2);
			

			npc.attackType = 0;
			
		}
		break;
		
		case 8: { // p2h attack
			
		}
		break;
		
		case 9: { // get bow
			npc.getStopWatch().startTimer(20);
			npc.dungVariables[PROGRESS] = 10;
			
			npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints()*0.29));
			
			npc.attackAnim = BOW_ATTACK_ANIM;
			
			npc.walkUpToHit = false;
			
			npc.dungVariables[PULLING_ENABLED] = false;
			
			final int mapRotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
			final Direction traverseDir = Direction.getNonDiagonal(mapRotation);
			
			final Location newLoc = npc.dungeonManager.getTile(npc.reference, 3, 10, 5, 5);
			
			final int stepsX = newLoc.getDx(npc.getCurrentLocation());
			final int stepsY = newLoc.getDy(npc.getCurrentLocation());

			final GameObject bow = (GameObject) npc.dungVariables[BOW_OBJ];
			
			getNextWeapon(npc, npc.npcType+60, traverseDir, traverseDir, stepsX, stepsY, FLY_ANIM, bow, 3);
			
			npc.attackType = 1;
			
		}
		break;
		
		case 10: { // bow attack
			
		}
		break;
		
		case 11: { // get maul
			npc.getStopWatch().startTimer(20);
			npc.dungVariables[PROGRESS] = 12;
			
			npc.dungVariables[HP_FOR_NEXT_STAGE] = (int)(Math.floor(npc.getSkills().getMaximumLifepoints()*0.15));
			
			npc.attackAnim = MAUL_ATTACK_ANIM;
			
			npc.walkUpToHit = true;
			
			npc.dungVariables[PULLING_ENABLED] = true;
			
			npc.getCombatStats().setAttackSpeed(3); // speeds up attack speed in this form
			
			final int mapRotation = npc.dungeonManager.getRoom(npc.reference).getRotation();
			final Direction traverseDir = Direction.getNonDiagonal(mapRotation+2);
			final Direction objFace = Direction.getNonDiagonal(mapRotation+3);
			
			final Location newLoc = npc.dungeonManager.getTile(npc.reference, 1, 2, 5, 5);
			
			final int stepsX = newLoc.getDx(npc.getCurrentLocation());
			final int stepsY = newLoc.getDy(npc.getCurrentLocation());
			
			final GameObject maul = (GameObject) npc.dungVariables[MAUL_OBJ];
			
			getNextWeapon(npc, npc.npcType+75, traverseDir, objFace, stepsX, stepsY, FLY_ANIM, maul, 4);

			npc.attackType = 0;
		}
		break;
		
//		case 12: {
//			npc.getStopWatch().startTimer(10);
//			npc.dungVariables[PROGRESS] = 0;
//			
//
//			npc.setRetaliates(true);
//			npc.startAnimation(65535);
//			npc.transform(npc.npcType);
//			
//			npc.moveNpc(npc.makeX, npc.makeY, npc.heightLevel);
//		}
//		break;
		
		}
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.attackAnim = MELEE_ATTACK_NO_WEP_ANIM;
		npc.walkUpToHit = true;
		npc.randomWalk = true;
		npc.prayerReductionMelee = npc.prayerReductionMagic = npc.prayerReductionRanged = 0.1; // 90% prayer reduction
		npc.resetAttackType = false; // need this cuz i'm still using PI shit code for monsters attacking player
		npc.setWalksHome(false);
		npc.getWalkingQueue().setRunning(false);
		npc.setWalkDistance(100); // it can walk in an area langer than 10x10
		npc.getDistanceRules().setMeleeDistance(2); // set melee distance to 2 so he doesn't get too close
		npc.dungVariables[PROGRESS] = new Integer(0);
		npc.dungVariables[HP_FOR_NEXT_STAGE] = new Integer(0);
		npc.dungVariables[ANNOYANCE_TICK] = new Integer(0);
		
		final int weaponSizeX = 1, weaponSizeY = 5;
		
		final int roomOrientation = npc.dungeonManager.getRoom(npc.reference).getRotation();
		
		final int z = npc.getZ();
		
		int x = npc.dungeonManager.getRoomX(npc.reference) + DungeonManager.translateX(3, 14, roomOrientation, weaponSizeX, weaponSizeY, 3);
		int y = npc.dungeonManager.getRoomY(npc.reference) + DungeonManager.translateY(3, 14, roomOrientation, weaponSizeX, weaponSizeY, 3);
		Location bowLoc = Location.create(x, y, z);

		GameObject bow = GameObject.createTickObject(56055, bowLoc, (3 + roomOrientation) % 4, 10, Integer.MAX_VALUE, -1);
		npc.dungeonManager.getObjectManager().addObject(bow);
		
		x = npc.dungeonManager.getRoomX(npc.reference) + DungeonManager.translateX(8, 14, roomOrientation, weaponSizeX, weaponSizeY, 3);
		y = npc.dungeonManager.getRoomY(npc.reference) + DungeonManager.translateY(8, 14, roomOrientation, weaponSizeX, weaponSizeY, 3);
		Location staffLoc = Location.create(x, y, z);

		GameObject staff = GameObject.createTickObject(56054, staffLoc, (3 + roomOrientation) % 4, 10, Integer.MAX_VALUE, -1);
		npc.dungeonManager.getObjectManager().addObject(staff);
		
		x = npc.dungeonManager.getRoomX(npc.reference) + DungeonManager.translateX(2, 1, roomOrientation, weaponSizeX, weaponSizeY, 1);
		y = npc.dungeonManager.getRoomY(npc.reference) + DungeonManager.translateY(2, 1, roomOrientation, weaponSizeX, weaponSizeY, 1);
		Location swordLoc = Location.create(x, y, z);

		GameObject sword = GameObject.createTickObject(56057, swordLoc, (1 + roomOrientation) % 4, 10, Integer.MAX_VALUE, -1);
		npc.dungeonManager.getObjectManager().addObject(sword);
		
		x = npc.dungeonManager.getRoomX(npc.reference) + DungeonManager.translateX(10, 1, roomOrientation, weaponSizeX, weaponSizeY, 1);
		y = npc.dungeonManager.getRoomY(npc.reference) + DungeonManager.translateY(10, 1, roomOrientation, weaponSizeX, weaponSizeY, 1);
		Location primal2hLoc = Location.create(x, y, z);

		GameObject primal2h = GameObject.createTickObject(56056, primal2hLoc, (1 + roomOrientation) % 4, 10, Integer.MAX_VALUE, -1);
		npc.dungeonManager.getObjectManager().addObject(primal2h);
		
		x = npc.dungeonManager.getRoomX(npc.reference) + DungeonManager.translateX(1, 2, roomOrientation, weaponSizeX, weaponSizeY, 2);
		y = npc.dungeonManager.getRoomY(npc.reference) + DungeonManager.translateY(1, 2, roomOrientation, weaponSizeX, weaponSizeY, 2);
		Location maulLoc = Location.create(x, y, z);
		
		GameObject maul = GameObject.createTickObject(56053, maulLoc, (2 + roomOrientation) % 4, 10, Integer.MAX_VALUE, -1);
		npc.dungeonManager.getObjectManager().addObject(maul);
		

		npc.dungVariables[Warmonger.SWORD_OBJ] = sword;
		npc.dungVariables[Warmonger.P2H_OBJ] = primal2h;
		npc.dungVariables[Warmonger.BOW_OBJ] = bow;
		npc.dungVariables[Warmonger.STAFF_OBJ] = staff;
		npc.dungVariables[Warmonger.MAUL_OBJ] = maul;
		
		npc.dungVariables[PULLING_ENABLED] = new Boolean(false);
		
	}

	@Override
	public boolean ignoreMeleeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int followDistance() {
		return 10;
	}

	@Override
	public boolean canMove() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int extraAttackDistance() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if (hit.getDamage() > 50) {
			hit.setDamage(50);
		}
		
		if ((Integer)npc.dungVariables[PROGRESS] < 12) {
			int hp = npc.getSkills().getLifepoints();
			int stageDmg = hp - (Integer)npc.dungVariables[HP_FOR_NEXT_STAGE];

			if (hit.getDamage() >= stageDmg) {
				hit.setDamage(stageDmg);
			}
			
			final int lifepoints = npc.getSkills().getLifepoints() - hit.getDamage();
			if (lifepoints <= (Integer)npc.dungVariables[HP_FOR_NEXT_STAGE]) {
				npc.takeDamage = false;
				npc.dungVariables[PROGRESS] = (Integer)npc.dungVariables[PROGRESS] + 1;
			}
		}
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
		npc.startGraphic(DEATH_GFX);
		npc.forceChat("Impossibruuuuu.....");
		
		for (int i = 0, len = npc.dungeonManager.party.partyMembers.size(); i < len; i++) {
			Player p = npc.dungeonManager.party.partyMembers.get(i);
			if (p == null || !p.isActive || !p.dungParty.dungManager.getCurrentRoomReference(p.getCurrentLocation()).equals(npc.reference))
				continue;
			
			p.dealTrueDamage(getSlamDamage(npc), 49, null, npc);
		}
		
		NPC ws = (NPC)npc.dungVariables[WARPED_SPHERE_VAR];
		if (ws != null && ws.isActive && !ws.isDead())
			ws.setAliveTick(30);
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
		if (p.usingAnyProtectPrayers()) {
			npc.dungVariables[ANNOYANCE_TICK] = (Integer)npc.dungVariables[ANNOYANCE_TICK] + 1;
			
			int annoyMeter = (Integer) npc.dungVariables[ANNOYANCE_TICK];
			
			if (annoyMeter >= 10) {
				AttackMob.disablePrayer(p, 6);
				npc.dungVariables[ANNOYANCE_TICK] = 0;
			} else if (annoyMeter == 8) {
				npc.forceChat("GRRRR!");
			}
		} else {
			npc.dungVariables[ANNOYANCE_TICK] = Math.max(0, (Integer)npc.dungVariables[ANNOYANCE_TICK] - 1);
		}
		
		if (npc.attackType == 1 && damage > 0) { // ranged lowers defense
			p.getSkills().drainLevel(Skills.DEFENSE, Misc.newRandom(1, 2), 1);
		}
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
