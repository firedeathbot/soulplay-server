package com.soulplay.content.player.skills.dungeoneeringv2.packets;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.VisibleRoom;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.SmugglerDialogue;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fishing.DungeoneeringFishing;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;

public class DungeonNpcs {

	public static boolean handle1(Client c, NPC npc, int npcType) {
		if (c.dungParty == null) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processNPCClick1(c, npc)) {
			return true;
		}

		switch(npc.npcType) {
			case DungeonConstants.FISH_SPOT_NPC_ID:
				DungeoneeringFishing.start(c, npc);
				return true;
			case 11226:
				SmugglerDialogue.smugglerChat(c, npcType);
				return true;
		}

		return true;
	}

	public static boolean handle2(Client c, NPC npc) {
		if (c.dungParty == null) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.processNPCClick2(c, npc)) {
			return true;
		}
		
		switch (npc.npcType) {
		
		case 11226:
			SmugglerDialogue.smugglerOpenShop(c);
			break;
		}

		return true;
	}
	
	public static boolean handleItemOnNpc(Client c, NPC npc, int itemId) {
		if (c.dungParty == null) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (dungeon == null || !dungeon.hasStarted()) {
			return false;
		}

		VisibleRoom vRoom = dungeon.getVisibleRoom(dungeon.getCurrentRoomReference(c.getCurrentLocation()));
		if (vRoom == null || !vRoom.handleItemOnNpc(c, npc, itemId)) {
			return true;
		}

		return true;
	}

}
