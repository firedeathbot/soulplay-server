package com.soulplay.content.player.skills.dungeoneeringv2.packets;

import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.Requirement;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.DisplayTimerType;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerAssistant;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;

public class DungeonButtons {

	private static final Requirement GROUP_GATE_TELE_REQ = new Requirement(new GameItem(SpellsConstants.law, 3), null, null, null, 64);
	private static final Requirement CREATE_GATE_REQ = new Requirement(new GameItem(SpellsConstants.cosmic, 3), null, null, null, 32);
	private static final Requirement VENG_REQ = new Requirement(new GameItem(SpellsConstants.astral, 4), new GameItem(SpellsConstants.death, 2), new GameItem(SpellsConstants.earth, 10), null, 94);
	private static final Animation HOME_TELE = new Animation(16385);

	public static boolean handleOp(Client c, int buttonId, int op) {
		if (!c.insideDungeoneering()) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		switch (buttonId) {
			case 50025:
				clickStatus(c, dungeon, op, 0);
				return true;
			case 50028:
				clickStatus(c, dungeon, op, 1);
				return true;
			case 50031:
				clickStatus(c, dungeon, op, 2);
				return true;
			case 50034:
				clickStatus(c, dungeon, op, 3);
				return true;
			case 50037:
				clickStatus(c, dungeon, op, 4);
				return true;
			default:
				//System.out.println("buton="+buttonId);
				break;
		}

		return false;
	}

	private static void clickStatus(Client c, DungeonManager dungeon, int op, int index) {
		if (dungeon.hasStarted()) {
			c.sendMessage("You can't do this at this moment.");
			return;
		}

		if (c.dungPartyIndex != index) {
			c.sendMessage("This isn't your to click!");
			return;
		}

		if (op == 0) {
			if (c.dungVoteStage == 2) {
				c.sendMessage("You have already voted to move on.");
				return;
			}

			dungeon.setState(index, 1);
			dungeon.ready(c);
			c.dungVoteStage = 2;
		} else {
			c.getDialogueBuilder().sendOption("Leave the dungeon permanently?", "Yes.", () -> {
				if (!c.insideDungeoneering()) {
					return;
				}

				dungeon.setState(index, 2);
				Party.leaveDungeon(c, true);
				c.getPA().closeAllWindows();
			}, "No.", () -> {c.getPacketSender().sendChatInterface(-1, false);}).execute(false);
		}
	}

	public static boolean handle(Client c, int buttonId) {
		if (!c.insideDungeoneering()) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (!dungeon.hasStarted()) {
			return false;
		}

		switch (buttonId) {
			case 242073://Group gatestone teleport
				if (c.playerMagicBook != SpellBook.DUNGEONEERING || c.teleTimer > 0) {
					return true;
				}

				Location location = groupGateTele(dungeon, c);
				if (location == null) {
					c.sendMessage("No gatestone found.");
					return true;
				}

				if (MagicRequirements.checkMagicReqsNew(c, GROUP_GATE_TELE_REQ, null, true)) {
					c.getPA().spellTeleport(location);
					c.getPA().walkableInterface(-1);
				}
				return true;
			case 242063://create gatestone
				if (c.playerMagicBook != SpellBook.DUNGEONEERING) {
					return true;
				}

				if (c.getItems().playerHasItem(DungeonConstants.GATESTONE)) {
					c.sendMessage("You have a gatestone in your pack. Making another would be pointless.");
					return true;
				}

				if (!MagicRequirements.checkMagicReqsNew(c, CREATE_GATE_REQ, null, true)) {
					return true;
				}

				if (c.gatestoneLocation != null) {//TODO fix gatestone code
					GroundItem item = ItemHandler.getPlayerOwnerGroundItem(c.getName(), DungeonConstants.GATESTONE, c.gatestoneLocation);
					if (item != null) {
						item.setRemoveTicks(2);
					}
					c.resetGatestone();
				}

				c.startAnimation(SpellsData.HIGH_ALCH_DUNG.getAnim());
				c.startGraphic(SpellsData.HIGH_ALCH_DUNG.getStartGfx());
				c.getItems().addItem(DungeonConstants.GATESTONE, 1);
				return true;
			case 242064://gatestone teleport
				if (c.playerMagicBook != SpellBook.DUNGEONEERING || c.teleTimer > 0) {
					return true;
				}

				if (c.getSkills().getLevel(Skills.MAGIC) < 32) {
					c.sendMessage("You don't have high enough Magic level to cast this spell.");
					return true;
				}

				if (c.gatestoneLocation == null) {
					c.sendMessage("You don't have a gatestone to teleport to.");
					return true;
				}

				GroundItem item = ItemHandler.getPlayerOwnerGroundItem(c.getName(), DungeonConstants.GATESTONE, c.gatestoneLocation);
				if (item != null) {
					item.setRemoveTicks(2);
				}

				c.getPA().spellTeleport(c.gatestoneLocation);
				c.getPA().walkableInterface(-1);
				c.resetGatestone();
				return true;
			case 242049://home tele
				if (c.playerMagicBook != SpellBook.DUNGEONEERING || c.teleTimer > 0) {
					return true;
				}

				if (c.skillingEvent != null) {
					c.resetSkillingEvent();
				}

				c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					int ticks = 0;
					@Override
					public void execute(CycleEventContainer container) {
						if (!c.insideDungeoneering() || c.npcIndex > 0 || c.underAttackBy2 > 0) {
							container.stop();
							return;
						}

						if (ticks == 0) {
							c.startAnimation(HOME_TELE);
						} else if (ticks == 17) {
							c.getPA().movePlayer(dungeon.getHomeTile());
							c.getPA().walkableInterface(-1);
							container.stop();
						}
						ticks++;
					}

					@Override
					public void stop() {
						c.resetAnimations();
					}
				}, 1);
				return true;
			case 242092://veng
				if (c.playerMagicBook != SpellBook.DUNGEONEERING) {
					return true;
				}

				if (c.getSkills().getDynamicLevels()[Skills.DEFENSE] < 40) {
					c.sendMessage("You need a defence level of 40 to cast this spell.");
					return true;
				}

				if (!c.vengDelay.checkThenStart(50)) {
					c.sendMessage("You can only cast vengeance every 30 seconds.");
					return true;
				}

				if (!MagicRequirements.checkMagicReqsNew(c, VENG_REQ, null, true)) {
					return true;
				}

				c.startAnimation(PlayerAssistant.VENG_ANIM);
				c.startGraphic(PlayerAssistant.VENG_GFX);
				c.getPA().addSkillXP(112, Skills.MAGIC);
				c.vengOn = true;
				c.getPacketSender().sendWidget(DisplayTimerType.VENGEANCE, 50);
				return true;
			default:
				//System.out.println("buton="+buttonId);
				break;
		}
		return false;
	}

	public static Location groupGateTele(DungeonManager dungeon, Client c) {
		Location location = null;
		for (int i = 0, length = dungeon.party.partyMembers.size(); i < length; i++) {
			Client partyMember = dungeon.party.partyMembers.get(i);
			if (partyMember.getItems().playerHasItem(DungeonConstants.GROUP_GATESTONE)) {
				location = partyMember.getCurrentLocation().copyNew();
				break;
			}
		}

		if (location == null) {
			location = dungeon.groupGatestone;
		}

		return location;
	}
}
