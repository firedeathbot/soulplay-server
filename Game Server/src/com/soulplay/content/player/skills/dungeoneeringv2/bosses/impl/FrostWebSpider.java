package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import java.util.ArrayList;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {10157, 10158, 10159, 10160, 10161, 10162, 10163, 10164, 10165, 10166, 10167})
public class FrostWebSpider extends Boss {

	private static final int DESPAWN_TICKS = 50;

	public FrostWebSpider(NPC boss) {
		super(boss);
	}

	@Override
	public void process(NPC npc) {
		NPC boss = (NPC) npc.dungVariables[0];
		if (boss != null && npc.getStopWatch().complete()) {
			((ArrayList<NPC>) boss.dungVariables[0]).remove(npc);
			npc.nullNPC();
		}
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		npc.attackType = 0;
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		npc.getStopWatch().startTimer(DESPAWN_TICKS);
		//TODO spawn animation
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}

	public int followDistance() {
		return 69;
	}

	public boolean canMove() {
		return true;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		NPC boss = (NPC) npc.dungVariables[0];
		if (boss != null) {
			((ArrayList<NPC>) boss.dungVariables[0]).remove(npc);
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
	}

}
