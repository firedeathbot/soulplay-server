package com.soulplay.content.player.skills.dungeoneeringv2.skills.runecrafting;

import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.player.achievement.Achievs;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.runecrafting.ImbueStavesData.ImbueStaves;
import com.soulplay.content.player.skills.runecrafting.RunecraftingData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

public class DungeoneeringRunecrafting {

	private static final Graphic RUNECRAFT_GFX = Graphic.create(2571);
	private static final Animation RUNECRAFT_ANIM = new Animation(791);
	private static final int RUNE_ESSENCE = 17776;

	public static boolean canCraft(Client c, RunecraftingData data) {
		if (!c.getItems().playerHasItem(RUNE_ESSENCE)) {
			c.sendMessage("You don't have any essence to craft runes.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.RUNECRAFTING] < data.getSkillLevelRequired()) {
			c.sendMessage("You need a runecrafting level of " + data.getSkillLevelRequired() + " to use this.");
			return false;
		}

		if (c.getItems().freeSlots() == 0 && c.getItems().getItemSlot(data.getDungRuneId()) == -1) {
			c.getDialogueBuilder().sendStatement("You don't have enough inventory space.").execute();
			return false;
		}

		return true;
	}

	public static void elementalRunes(Client c) {
		MakeInterface.make(c, 17780, () -> {craftRunes(c, RunecraftingData.AIR_RUNE);}, 17781, () -> {craftRunes(c, RunecraftingData.WATER_RUNE);}, 17782, () -> {craftRunes(c, RunecraftingData.EARTH_RUNE);}, 17783, () -> {craftRunes(c, RunecraftingData.FIRE_RUNE);});
	}

	public static void combatRunes(Client c) {
		MakeInterface.make(c, 17784, () -> {craftRunes(c, RunecraftingData.MIND_RUNE);}, 17785, () -> {craftRunes(c, RunecraftingData.CHAOS_RUNE);}, 17786, () -> {craftRunes(c, RunecraftingData.DEATH_RUNE);}, 17787, () -> {craftRunes(c, RunecraftingData.BLOOD_RUNE);});
	}

	public static void otherRunes(Client c) {
		MakeInterface.make(c, 17788, () -> {craftRunes(c, RunecraftingData.BODY_RUNE);}, 17789, () -> {craftRunes(c, RunecraftingData.COSMIC_RUNE);}, 17790, () -> {craftRunes(c, RunecraftingData.ASTRAL_RUNE);}, 17791, () -> {craftRunes(c, RunecraftingData.NATURE_RUNE);}, 17792, () -> {craftRunes(c, RunecraftingData.LAW_RUNE);});
	}

	public static void staves(Client c) {
		c.getDialogueBuilder().sendOption("What would you like to make?",
				"Normal staves", () -> {
					MakeInterface.make(c, 16997, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.WATER_STAFF);}, 17001, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.EARTH_STAFF);}, 17005, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.FIRE_STAFF);}, 17009, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.AIR_STAFF);}, 17017, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.CATALYTIC_STAFF);});
				},
				"Imbued staves", () -> {
					MakeInterface.make(c, 16999, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.EMPOWERED_WATER_STAFF);}, 17003, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.EMPOWERED_EARTH_STAFF);}, 17007, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.EMPOWERED_FIRE_STAFF);}, 17011, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.EMPOWERED_AIR_STAFF);}, 17015, () -> {ImbueStavesData.craftStaff(c, ImbueStaves.EMPOWERED_CATALYTIC_STAFF);});
				}).executeIfNotActive();
	}

	public static void giveDialog(Client c) {
		c.getDialogueBuilder().sendOption("What would you like to make?",
			"Elemental runes.", () -> {
				elementalRunes(c);
			},
			"Combat runes.", () -> {
				combatRunes(c);
			},
			"Other runes.", () -> {
				otherRunes(c);
			},
			"Staves.", () -> {
				staves(c);
				}).execute(false);
	}

	public static void craftRunes(Client player, RunecraftingData data) {
		player.getPA().closeAllWindows();

		if (!canCraft(player, data) || player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int cycles = player.makeCount;
			
			@Override
			public void execute(CycleEventContainer container) {
				int runeEssenceAmount = 0;

				if (player.getItems().playerHasItem(RUNE_ESSENCE)) {
					runeEssenceAmount = Math.min(10, player.getItems().getItemAmount(RUNE_ESSENCE));
				}

				player.getItems().deleteItem2(RUNE_ESSENCE, runeEssenceAmount);

				player.startGraphic(RUNECRAFT_GFX);
				player.startAnimation(RUNECRAFT_ANIM);

				final int productAmount = runeEssenceAmount * data.getMultipler(player);

				player.getItems().addItem(data.getDungRuneId(), productAmount);

				player.getPA().addSkillXP((int) ((data.getBaseExperience() * 0.02) * runeEssenceAmount), RSConstants.RUNECRAFTING);

				player.sendMessage("You bind the temple's power into " + productAmount + " " + data.getName() + "s.");

				player.getAchievement().increaseProgress(Achievs.CRAFT_10K_RUNES, productAmount);

				if (data == RunecraftingData.LAW_RUNE) {
					player.getAchievement().increaseProgress(Achievs.CRAFT_200_LAWS, productAmount);
				} else if (data == RunecraftingData.AIR_RUNE) {
					player.getAchievement().increaseProgress(Achievs.CRAFT_100_AIR, productAmount);
				}
				
				cycles--;

				if (!player.insideDungeoneering() || !canCraft(player, data) || cycles == 0) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 2);
	}
	
}
