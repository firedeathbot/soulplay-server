package com.soulplay.content.player.skills.dungeoneeringv2.skills.fishing;

public enum DungeoneeringFishData {

	HEIM_CRAB(17797, 1, 9, "Heim crab"),
	RED_EYE(17799, 10, 27, "Red-eye"),
	DUSK_EEL(17801, 20, 45, "Dusk eel"),
	GIANT_FLATFISH(17803, 30, 63, "Giant flatfish"),
	SHORTFINNED_EEL(17805, 40, 81, "Short-finned eel"),
	WEB_SNIPPER(17807, 50, 99, "Web spinner"),
	BOULDABASS(17809, 60, 117, "Bouldabass"),
	SALVE_EEL(17811, 70, 135, "Salve eel"),
	BLUE_CRAB(17813, 80, 153, "Blue crab"),
	CAVE_MORAY(17815, 90, 171, "Cave moray"),
	VILE_FISH(17374, 1, 0, "Vile fish");

	public static int pickFish(int maxLvl) {
		for(int i = values.length - 2; i >= 0; i--) {// - 2 skip vile fish
			DungeoneeringFishData fish = values[i];
			if (maxLvl >= fish.level) {
				return i;
			}
		}

		return 0;
	}

	public static final DungeoneeringFishData[] values = values();
	private final int id, level;
	private final int xp;
	private final String name;

	private DungeoneeringFishData(int id, int level, int xp, String name) {
		this.id = id;
		this.level = level;
		this.xp = xp;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public int getLevel() {
		return level;
	}

	public int getXp() {
		return xp;
	}

	public String getName() {
		return name;
	}

}
