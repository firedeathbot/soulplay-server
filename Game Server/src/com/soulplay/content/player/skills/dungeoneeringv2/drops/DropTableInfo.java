package com.soulplay.content.player.skills.dungeoneeringv2.drops;

public interface DropTableInfo {

	public int tableRollCount();

	public boolean dropFullTable();
	
	public int minTier();
	
	public int maxTier();

}
