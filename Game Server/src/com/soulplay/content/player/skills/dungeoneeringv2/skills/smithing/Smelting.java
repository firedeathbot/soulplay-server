package com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing;

import java.util.Arrays;
import java.util.List;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

public class Smelting {

	private static final Animation SMELT_ANIM = new Animation(3243);
	private static final List<Item> ITEMS = Arrays.asList(new Item[] { new Item(17651, 1),
			new Item(17653, 1), new Item(17655, 1), new Item(17657, 1), new Item(17659, 1), new Item(17661, 1),
			new Item(17663, 1), new Item(17665, 1), new Item(17667, 1), new Item(17669, 1) });

	public static void initSmelt(Client c) {
		c.getPacketSender().itemsOnInterface(51001, ITEMS);
		c.getPacketSender().sendChatInterface(51000);
	}

	private static boolean canSmelt(Client c, SmeltingDataDung data) {
		if (!c.getItems().playerHasItem(data.getOreId())) {
			c.sendMessage("You don't have any ore left.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.SMITHING] < data.getLevel()) {
			c.sendMessage("You need a smithing level of " + data.getLevel() + " to smelt this.");
			return false;
		}

		return true;
	}

	public static void startSmelt(Client c, SmeltingDataDung data, int count) {
		c.getPA().closeAllWindows();

		if (data == null) {
			c.sendMessage("You don't have any available ore to smelt.");
			return;
		}

		if (!canSmelt(c, data) || c.skillingEvent != null) {
			return;
		}

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int cycles = count;

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItem2(data.getOreId(), 1);
				c.startAnimation(SMELT_ANIM);
				c.getItems().addItem(data.getBarId(), 1);
				c.getPA().addSkillXP(data.getXp(), Skills.SMITHING);
				c.sendMessage("You smelt " + ItemDef.forID(data.getOreId()).getName() + " into a "
						+ ItemDef.forID(data.getBarId()).getName() + ".");

				cycles--;
				if (!c.insideDungeoneering() || cycles == 0 || !canSmelt(c, data)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 4);
	}

}
