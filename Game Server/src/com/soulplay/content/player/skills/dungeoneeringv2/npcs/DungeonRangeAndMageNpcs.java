package com.soulplay.content.player.skills.dungeoneeringv2.npcs;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class DungeonRangeAndMageNpcs {
	
	public static void DungeoneeringSpells(NPC n, Client c) {

		if (n.npcType >= 10791 && n.npcType <= 10796) {// thrower troll
			n.projectileId = 2268; // think this is real gfx
			//n.startGraphic(Graphic.create(2267));
			n.attackType = 1;
			n.projectiletStartHeight = 18;
		}
		
		if (n.npcType >= 10610 && n.npcType <= 10618) {// hydra
			n.projectileId = 2736; //need real gfx
			n.attackType = 1;
			n.projectiletStartHeight = 18;
		}
		
		if (n.npcType >= 10831 && n.npcType <= 10842) {// Mysterious shade
			if (Misc.random(1) == 1) {
				n.attackType = 1;
				n.projectileId = 2510;
				n.startGraphic(Graphic.create(2514));
				n.endGfx = 2512;
			} else {
				n.projectileId = 2511;
				n.startGraphic(Graphic.create(2515));
				n.endGfx = 2513;
				n.attackType = 2;
			}
		}
		
		if (n.npcType >= 10460 && n.npcType <= 10468) {// ice elementals
			int dist = Misc.distanceToPoint(n.getX(), n.getY(), c.getX(), c.getY());
			
			int random = 1;
			if (dist < 2) {
				random = 5;
			} else {
				random = 1;
			}
			
			if (Misc.random(random) >= 2 && dist < 2) {
				n.attackType = 0;
				n.endGfx = -1;
				n.projectileId = -1;
			} else if (Misc.random(random) == 1) {
				n.attackType = 1;
				n.projectileId = 2143;
			} else {
				n.attackType = 2;
				n.projectileId = 2012; //1825 black version? 
				n.endGfx = 2013;
			}
		}
		
		if (n.npcType >= 10212 && n.npcType <= 10218) {// icefiends
			if (Misc.random(1) == 1) { // get proper graphics for both attacks
				n.projectileId = 2206;//2705
				n.projectiletStartHeight = 0;
				n.attackType = 1;
				n.endGfx = 2207;
			} else {
				n.projectileId = 2577;//2705
				n.projectiletStartHeight = 25;
				n.attackType = 2;
			}
		}
		
		if (n.npcType >= 10670 && n.npcType <= 10681) {// skeletons range
			n.projectileId = 2872; // need real gfx 88
			n.attackType = 1;
			n.projectiletStartHeight = 25;
		}
		
		if (n.npcType >= 10682 && n.npcType <= 10693) {// skeletons mage
			n.projectileId = 2183; // need real gfx
			n.attackType = 2;
			n.projectiletStartHeight = 25;
		}
		
		if (n.npcType >= 10375 && n.npcType <= 10385) {// zombies range
			n.startGraphic(Graphic.create(1831));
			n.projectileId = 1832;
			n.projectileDelay = 26;
			n.attackType = 1;
		}
		
		if (n.npcType == 10492) {// lesser demon lvl 56 //TODO: not done
			n.projectileId = 35; // need real gfx
			n.attackType = 1;
			n.projectiletStartHeight = 28;
		}
		
		if (n.npcType == 10705) {//soulgazer
			n.attackType = 2;
			n.projectileId = 2772;
			n.endGfx = 2774;
			n.dungMultiAttack = true;
		}
		
		if (n.npcType == 10704) {//edimmu
			int dist = Misc.distanceToPoint(n.getX(), n.getY(), c.getX(), c.getY());
			
			if (dist <= 1 && Misc.random(2) == 1) {
				n.attackType = 0;
				n.projectileId = -1;
			} else {
				n.attackType = 2;
				n.projectileId = 2615;
				n.resetAnimations();
				n.startAnimation(new Animation(13735));
			}
		}
		
		if (n.npcType == 10701) {//seeker
			n.attackType = 2;
			n.projectileId = 2772;
			n.endGfx = 2774;
			n.dungMultiAttack = true;
		}
		
		if (n.npcType == 10697) {//pyrefiend
			n.attackType = 2;
			n.projectileId = 4500; //missing correct gfx
		}
		

	}
		
}
