package com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

public enum StringingData {

	//Shortbows
	TANGLE_GUM_SHORT(13235, 17702, 16867, 5, 1),
	SEEPING_ELM_SHORT(13236, 17704, 16869, 9, 11),
	BLOOD_SPINDLE_SHORT(13237, 17706, 16871, 15, 21),
	UTUKU_SHORT(13238, 17708, 16873, 23, 31),
	SPINEBEAM_SHORT(13239, 17710, 16875, 33, 41),
	BOVISTRANGLER_SHORT(13240, 17712, 16877, 45, 51),
	THIGAT_SHORT(13241, 17714, 16879, 59, 61),
	CORPSETHORN_SHORT(13242, 17716, 16881, 75, 71),
	ENTGALLOW_SHORT(13243, 17718, 16883, 93, 81),
	GRAVE_CREEPER_SHORT(13244, 17720, 16885, 113, 91),
	//Longbows
	TANGLE_GUM_LONG(13225, 17722, 16317, 6, 6),
	SEEPING_ELM_LONG(13226, 17724, 16319, 10, 16),
	BLOOD_SPINDLE_LONG(13227, 17726, 16321, 17, 26),
	UTUKU_LONG(13228, 17728, 16323, 26, 36),
	SPINEBEAM_LONG(13229, 17730, 16325, 37, 46),
	BOVISTRANGLER_LONG(13230, 17732, 16327, 51, 56),
	THIGAT_LONG(13231, 17734, 16329, 67, 66),
	CORPSETHORN_LONG(13232, 17736, 16331, 86, 76),
	ENTGALLOW_LONG(13233, 17738, 16333, 106, 86),
	GRAVE_CREEPER_LONG(13234, 17740, 16335, 129, 96);

	public static final Map<Integer, StringingData> values = new HashMap<>();
	private final int input, product, xp, level;
	private final Animation anim;

	private StringingData(int animId, int input, int product, int xp, int level) {
		this.anim = new Animation(animId);
		this.input = input;
		this.product = product;
		this.xp = xp;
		this.level = level;
	}

	public Animation getAnim() {
		return anim;
	}

	public int getInput() {
		return input;
	}

	public int getProduct() {
		return product;
	}

	public int getXp() {
		return xp;
	}

	public int getLevel() {
		return level;
	}

	static {
		for (StringingData data : values()) {
			values.put(data.getInput(), data);
		}
	}
	
    public static void load() {
    	/* empty */
    }
}
