package com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.construction.ConstructionPathFinder;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {9948, 9949, 9950, 9951, 9952, 9953, 9954, 9955, 9956, 9957, 9958, 9959, 9960,
		9961, 9962, 9963, 9964})
public class GluttonousBehemoth extends Boss {
	
	private static final int DD_TICKS = 12;
	private static final int EATING_VAR_ID = 0;
	private static final int DONE_EATING_ONE_VAR_ID = 1;
	private static final int DONE_EATING_SECOND_VAR_ID = 2;
	private static final int HEAL_AMOUNT = 5;
	private static final Animation EAT_ANIM = new Animation(13720);

	public GluttonousBehemoth(NPC boss) {
		super(boss);
	}

	public static boolean blockedFood(GameObject food, NPC n) {
		for (Player p : n.dungeonManager.party.partyMembers) {
			int dist = Misc.distanceToPoint(n.getX()+2, n.getY()+2, p.getX(), p.getY());
			
			if (dist == 3) {
				int distToFood = Misc.distanceToPoint(food.getX()+1, food.getY()+1, p.getX(), p.getY());
				if (distToFood == 2) {
					return true;
				}
			}
		}

		return false;
	}
	
	@Override
	public void process(NPC npc) {
		if ((boolean) npc.dungVariables[6]) {
			npc.stopAttack = true;
			npc.setAttackable(false);
		} else {
			npc.stopAttack = false;
			npc.setAttackable(true);
		}

		GameObject food1 = null;
		GameObject food2 = null;
		boolean enoughPeople = false;
		if (npc.dungeonManager != null) {
			enoughPeople = npc.dungeonManager.party.partyMembers.size() > 1;
			food1 = (GameObject) npc.dungVariables[3];
			food2 = (GameObject) npc.dungVariables[4];
		}

		switch((Integer) npc.dungVariables[EATING_VAR_ID]) {
			case 0:
				if (npc.getSkills().getLifepoints() <= npc.getSkills().getMaximumLifepoints() / 2) {
					if ((Integer) npc.dungVariables[DONE_EATING_ONE_VAR_ID] == 0 && (food1 != null && !blockedFood(food1, npc))) {
						npc.dungVariables[EATING_VAR_ID] = 1;
					} else if (enoughPeople && (Integer) npc.dungVariables[DONE_EATING_SECOND_VAR_ID] == 0 && (food2 != null && !blockedFood(food2, npc))) {
						npc.dungVariables[EATING_VAR_ID] = 2;
					}
				}
				break;
			case 1:
				npc.resetFace();
				npc.setCanWalk(true);
				npc.setStunned(true);
				npc.setRetaliates(false);
				npc.dungVariables[5] = npc.getKillerId();
				npc.setKillerId(-1);
				ConstructionPathFinder.findRoute(npc, (Location) npc.dungVariables[8], true, 1, 1, npc.dungeonManager.clipping);
				npc.dungVariables[EATING_VAR_ID] = 3;
				break;
			case 2: {
				npc.resetFace();
				npc.setCanWalk(true);
				npc.setStunned(true);
				npc.setRetaliates(false);
				npc.dungVariables[5] = npc.getKillerId();
				npc.setKillerId(-1);
				ConstructionPathFinder.findRoute(npc, (Location) npc.dungVariables[9], true, 1, 1, npc.dungeonManager.clipping);
				npc.dungVariables[EATING_VAR_ID] = 5;
				break;
			}
			case 3: {
				npc.startAnimation(EAT_ANIM);
				int heal = Misc.randomNoPlus(HEAL_AMOUNT, HEAL_AMOUNT * 2);
				npc.getSkills().heal(heal);
				npc.handleHitMask(heal, 2, -1);//TODO add the heal icon and stuff
				if (npc.getSkills().getLifepoints() >= npc.getSkills().getMaximumLifepoints()) {
					npc.dungVariables[EATING_VAR_ID] = 4;
					npc.dungVariables[DONE_EATING_ONE_VAR_ID] = 1;
				}
				break;
			}
			case 4:
				npc.startAnimation(Mob.RESET_ANIM);
				npc.setStunned(false);
				npc.setRetaliates(true);
				npc.setKillerId((int) npc.dungVariables[5]);
				ConstructionPathFinder.findRoute(npc, (Location) npc.dungVariables[7], true, 1, 1, npc.dungeonManager.clipping);
				npc.dungVariables[EATING_VAR_ID] = 0;
				npc.setCanWalk(false);
				break;
			case 5: {
				npc.startAnimation(EAT_ANIM);
				int heal = Misc.randomNoPlus(HEAL_AMOUNT, HEAL_AMOUNT * 2);
				npc.getSkills().heal(heal);
				npc.handleHitMask(heal, 2, -1);//TODO add the heal icon and stuff
				if (npc.getSkills().getLifepoints() >= npc.getSkills().getMaximumLifepoints()) {
					npc.dungVariables[EATING_VAR_ID] = 6;
					npc.dungVariables[DONE_EATING_SECOND_VAR_ID] = 1;
				}
				break;
			}
			case 6:
				npc.startAnimation(Mob.RESET_ANIM);
				npc.setStunned(false);
				npc.setRetaliates(true);
				npc.setKillerId((int) npc.dungVariables[5]);
				ConstructionPathFinder.findRoute(npc, (Location) npc.dungVariables[7], true, 1, 1, npc.dungeonManager.clipping);
				npc.dungVariables[EATING_VAR_ID] = 0;
				npc.setCanWalk(false);
				break;
		}
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		int dist = Misc.distanceToPoint(npc.getX()+2, npc.getY()+2, c.getX(), c.getY());
		boolean withinNpc = dist <= 2;
		npc.dungVariables[6] = withinNpc;

		if (npc.getStopWatch().checkThenStart(DD_TICKS) && withinNpc) {
			c.dealDamage(new Hit(Misc.random(20), 0, -1));//TODO should damage be based on npc?
			npc.startAnimation(13718);
		}

		//TODO correct ids
		int random = Misc.random(2);//Misc.random(2);
		if (random == 2) {
			npc.projectileId = 2610;
			npc.endGfx = 2611;
			npc.projectiletStartHeight = 0;
			npc.attackType = 1;
		} else if (random == 1 && dist == 3) {
			npc.attackType = 0;
			npc.projectileId = -1;
		} else {
			npc.attackType = 2;
			npc.projectileId = 2612;
			npc.endGfx = 2613;
			npc.projectiletStartHeight = 0;
			npc.projectileDelay = Misc.serverToClientTick(2);
		}
	}

	public Prayers protectPrayer(int npcId) {
		return null;
	}

	public int protectPrayerDamageReduction(int damage) {
		return -1;
	}

	public void onSpawn(NPC npc) {
		npc.getStopWatch().startTimer(DD_TICKS);
		//npc.getSkills().setLifepoints(npc.getSkills().getMaximumLifepoints() / 2 - 1);
		
		npc.dungVariables[0] = new Integer(0);
		npc.dungVariables[1] = new Integer(0);
		npc.dungVariables[2] = new Integer(0);
		npc.dungVariables[5] = new Integer(-1);
		npc.dungVariables[6] = new Boolean(false);
		npc.dungVariables[7] = npc.getCurrentLocation().copyNew();
	}

	public boolean ignoreMeleeBonus() {
		return false;
	}

	public boolean ignoreRangeBonus() {
		return false;
	}

	public boolean ignoreMageBonus() {
		return false;
	}
	
	public int followDistance() {
		return 1;
	}

	public boolean canMove() {
		return false;
	}
	
	public int extraAttackDistance() {
		return 0;
	}
	
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}
	
}
