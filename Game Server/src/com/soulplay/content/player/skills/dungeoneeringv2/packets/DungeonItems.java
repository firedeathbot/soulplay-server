package com.soulplay.content.player.skills.dungeoneeringv2.packets;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.crafting.DungeoneeringCrafting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.fletching.DungeoneeringFletching;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public class DungeonItems {

	public static boolean handle1(Client c, int itemId, int itemSlot) {
		if (!c.insideDungeoneering()) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (!dungeon.hasStarted()) {
			return false;
		}

		switch (itemId) {
			case 19650:
				int slotsNeeded = 7;

				if (c.getItems().playerHasItem(17796)) {
					slotsNeeded--;
				}

				if (c.getPA().freeSlots() < slotsNeeded) {
					c.sendMessage("You don't have enough inventory space to open the toolkit.");
					return true;
				}
				c.getItems().deleteItemInOneSlot(19650, 1);
				c.getItems().addItem(17794, 1);
				c.getItems().addItem(16295, 1);
				c.getItems().addItem(16361, 1);
				c.getItems().addItem(17492, 1);
				c.getItems().addItem(17754, 1);
				c.getItems().addItem(17883, 1);
				c.getItems().addItem(17796, 50);
				c.getItems().addItem(17678, 1);
				return true;
		}

		return true;
	}

	public static boolean drop(Player c, Item item) {
		if (!c.insideDungeoneering()) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (!dungeon.hasStarted()) {
			return false;
		}

		if (c.getItems().deleteItem(item)) {
			c.droppingItem = true;
			dungeon.spawnBelow(c, item);
			c.droppingItem = false;
		}

		return true;
	}

	public static boolean handleItemOnItem(Client c, int itemUsed, int itemUsedSlot, int usedWith, int usedWithSlot) {
		if (!c.insideDungeoneering()) {
			return false;
		}

		DungeonManager dungeon = c.dungParty.dungManager;

		if (!dungeon.hasStarted()) {
			return false;
		}

		if (itemUsed == DungeoneeringCrafting.NEEDLE || usedWith == DungeoneeringCrafting.NEEDLE) {
			if (DungeoneeringCrafting.initCraft(c, itemUsed, usedWith)) {
				return true;
			}
		} else if (itemUsed == DungeoneeringFletching.KNIFE || usedWith == DungeoneeringFletching.KNIFE) {
			if (DungeoneeringFletching.initFletch(c, itemUsed, usedWith)) {
				return true;
			}
		} else if (itemUsed == DungeoneeringFletching.SHAFT || usedWith == DungeoneeringFletching.SHAFT) {
			if (DungeoneeringFletching.applyFeathers(c, itemUsed, usedWith, itemUsedSlot, usedWithSlot)) {
				return true;
			}
		} else if (itemUsed == DungeoneeringFletching.HEADLESS || usedWith == DungeoneeringFletching.HEADLESS) {
			if (DungeoneeringFletching.applyArrowTips(c, itemUsed, usedWith, itemUsedSlot, usedWithSlot)) {
				return true;
			}
		} else if (itemUsed == DungeoneeringFletching.BOW_STRING || usedWith == DungeoneeringFletching.BOW_STRING) {
			if (DungeoneeringFletching.initStringing(c, itemUsed, usedWith)) {
				return true;
			}
		}

		return true;
	}

}
