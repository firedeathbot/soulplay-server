package com.soulplay.content.player.skills.firemaking;

import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Firemaking {
	
	
	private static final int pyromancerHood = 30305;
	private static final int pyromancerBoots = 30306;
	private static final int pyromancerGarb = 30303;
	private static final int pyromancerRobe = 30304;

	public static void attemptFire(final Client c, final int itemUsed,
			final int usedWith, final int x, final int y,
			final boolean groundObject) {
		try {

			if (c.skillingEvent != null) {
				return;
			}
			
			int idToCheck = c.insideDungeoneering() ? 17678 : 590;
			if (!c.getItems().playerHasItem(idToCheck)) {
				c.sendMessage("You need a tinderbox to light a fire.");
				return;
			}
			
			if (c.inBank() || c.getZones().isInGambleZone()) {
				c.sendMessage("You cannot light a fire here.");
				return;
			}

            if (c.playerIsInHouse) {
                c.sendMessage("You cannot light a fire here");
                return;
            }

			final int z = c.getZ();
			if (c.insideDungeoneering()) {
				if (c.dungParty.dungManager.getRealObject(x, y) != null) { //TODO: pass Z
					c.sendMessage("You cannot light a fire here.");
				}
			} else {
				if (ObjectManager.getObject(x, y,
						z) != null) {
					c.sendMessage("You cannot light a fire here.");
					return;
				}
			}
			if (!RegionClip.canStandOnSpot(x, y, z, c.getDynamicRegionClip())) {
				c.sendMessage("You cannot light a fire here.");
				return;
			}
			if (c.getBarbarianAssault().inLobby()) {
				c.sendMessage("You cannot light a fire in the lobby.");
				return;
			}
			
			final int logId = usedWith == idToCheck ? itemUsed : usedWith;
			
			LogData l = LogData.getLog(logId);
			
			if (l == null)
				return;
			if (c.getPlayerLevel()[Skills.FIREMAKING] < l.getLevel()) {
				c.sendMessage("You need a firemaking level of "
						+ l.getLevel() + " to light "
						+ ItemConstants.getItemName(logId));
				return;
			}

			final boolean walk;
			if (RegionClip.getClippingDirection((c.getX() - 1), c.getY(), c.getZ(), -1, 0, c.getDynamicRegionClip())) {
				walk = true;
			} else {
				walk = false;
			}

			boolean notInstant = (System.currentTimeMillis()
					- c.lastSkillingAction) > 2500;
					int cycle = 2;
					if (notInstant) {
						c.sendMessageSpam(
								"You attempt to light a fire.");
						if (!groundObject) {
							c.getItems().deleteItemInOneSlot(logId,
									c.getItems().getItemSlot(logId), 1);

							ItemHandler.createGroundItem(c, logId, x,
									y, z, 1);
						}
						cycle = 3 + Misc.random(6);
					} else {
						if (!groundObject) {
							c.getItems().deleteItemInOneSlot(logId,
									c.getItems().getItemSlot(logId), 1);
						}
					}

					if (notInstant) {
						c.startAnimation(733);
						c.getPacketSender().playSound(Sounds.LIGHT_LOG);
					}
					// c.getPA().walkTo(walk == true ? -1 : 1, 0);
					c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

						@Override
						public void execute(CycleEventContainer container) {
							container.stop();
							ItemHandler.removeGroundItem(c, logId,
									x, y, z, false);
							if (c.insideDungeoneering()) {
								GameObject obj = GameObject.createTickObject(2732, Location.create(x, y, z), 0, 10, 60 + Misc.random(30), -1);
								c.dungParty.dungManager.getObjectManager().addObject(obj);
							} else if (logId == 7404) {
								ObjectManager.addObject(new RSObject(26186, x, y, z, 0, 10, -1, 200));
							} else if (logId == 7405) {
								ObjectManager.addObject(new RSObject(26575, x, y, z, 0, 10, -1, 200));
							} else if (logId == 7406) {
								ObjectManager.addObject(new RSObject(26576, x, y, z, 0, 10, -1, 200));
							} else if (logId == 10329) {
								ObjectManager.addObject(new RSObject(20001, x, y, z, 0, 10, -1, 200));
							} else if (logId == 10328) {
								ObjectManager.addObject(new RSObject(20000, x, y, z, 0, 10, -1, 200));
							} else {
								ObjectManager.addObject(new RSObject(26185, x, y, z, 0, 10, -1, 100 + Misc.random(30)));
							}

							c.increaseProgress(TaskType.FIREMAKING, logId);
							c.startAnimation(65535);
							c.getPacketSender().playSound(Sounds.LIGHT_LOG_SUCCESS);

							if (logId == 1513) {
								c.getAchievement().lightMagic();
							}
							c.getPA().addSkillXP((int)((double)l.getXp() * calculatePyromancerOutfitXpModifier(c)),	Skills.FIREMAKING);
							c.sendMessageSpam("The fire catches and the log beings to burn.");
							c.getPA().walkTo(walk ? -1 : 1,	0);
							c.getAchievement().lightFires();
							CycleEventHandler.getSingleton()
							.addEvent(c, new CycleEvent() {

								@Override
								public void execute(CycleEventContainer container) {
									c.faceLocation(
											walk
											? (x + 1)
													: (x - 1),
													y);
									container.stop();
								}

								@Override
								public void stop() {
								}
							}, 2);
						}

						@Override
						public void stop() {
							c.lastSkillingAction = System
									.currentTimeMillis();

						}
					}, cycle);
		} catch (Exception ex) {
			System.out.println("Player tried to crash with firemaking name:"
					+ c.getName() + " x:" + c.getX() + " y:" + c.getY() + " z:"
					+ c.getHeightLevel());
			ex.printStackTrace();
		}
	}
	
	public static double calculatePyromancerOutfitXpModifier(Player player) {
		double modifier = 1.0;

		if (player.getItems().playerHasEquipped(pyromancerHood, PlayerConstants.playerHat)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(pyromancerGarb, PlayerConstants.playerChest)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(pyromancerRobe, PlayerConstants.playerLegs)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(pyromancerBoots, PlayerConstants.playerFeet)) {
			modifier += 0.05;
		}
		return modifier;
	}

}
