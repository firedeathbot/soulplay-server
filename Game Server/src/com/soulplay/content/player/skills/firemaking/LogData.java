package com.soulplay.content.player.skills.firemaking;

import java.util.HashMap;
import java.util.Map;

public enum LogData {

	LOG(1511, 1, 40),
	RED_LOG(7404, 1, 50),
	GREEN_LOG(7405, 1, 50),
	BLUE_LOG(7406, 1, 50),
	PURPLE_LOG(10329, 1, 50),
	WHITE_LOG(10328, 1, 50),
	ACHEY(2862, 1, 40),
	OAK(1521, 15, 60),
	WILLOW(1519, 30, 90),
	TEAK(6333, 35, 105),
	ARCTIC_PINE(10810, 42, 125),
	MAPLE(1517, 45, 135),
	MAHOGANY(6332, 50, 157),
	EUCALYPTUS(12581, 58, 193),
	YEW(1515, 60, 202),
	MAGIC(1513, 75, 303),
	REDWOOD(119669, 90, 350),
	TANGLE_GUM_BRANCHES(17682, 1, 25),
	SEEPING_ELM_BRANCHES(17684, 10, 44),
	BLOOD_SPINDLE_BRANCHES(17686, 20, 65),
	UTUKU_BRANCHES(17688, 30, 88),
	SPINEBEAM_BRANCHES(17690, 40, 112),
	BOVISTRANGLER_BRANCHES(17692, 50, 138),
	THIGAT_BRANCHES(17694, 60, 166),
	CORPSETHRON_BRANCHES(17696, 70, 195),
	ENTGALLOW_BRANCHES(17698, 80, 225),
	GRAVE_CREEPER_BRANCHES(17700, 90, 258);
	
	public static final LogData[] values = LogData.values();

	private static final Map<Integer, LogData> map = new HashMap<>();

	private int logId, level;

	private int xp;

	LogData(int logId, int level, int xp) {
		this.logId = logId;
		this.level = level;
		this.xp = xp;
	}

	public int getLevel() {
		return level;
	}

	public int getLogId() {
		return logId;
	}

	public int getXp() {
		return xp;
	}
	
	static {
		for (LogData d : values) {
			map.put(d.getLogId(), d);
		}
	}
	
	public static LogData getLog(int logId) {
		return map.get(logId);
	}
	
    public static void load() {
    	/* empty */
    }
}
