package com.soulplay.content.player.skills.fletching.stringing;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

public enum CrossbowStringingData {
	BRONZE_CBOW(9454, 9174, 9, 6, 6671),
	BLURITE_CBOW(9456, 9176, 24, 16, 6672),
	IRON_CBOW(9457, 9177, 39, 22, 6673),
	STEEL_CBOW(9459, 9179, 46, 27, 6674),
	MITHIRIL_CBOW(9461, 9181, 54, 32, 6675),
	ADAMANT_CBOW(9463, 9183, 61, 41, 6676),
	RUNITE_CBOW(9465, 9185, 69, 50, 6677),
	DRAGON_CBOW(121921, 121902, 78, 70, 6677);

	public static final Map<Integer, CrossbowStringingData> values = new HashMap<>();
	private final int item;
	private final int product;
	private final int level;
	private final double experience;
	private final Animation animation;

	CrossbowStringingData(int item, int product, int level, double experience, int animation) {
		this.item = item;
		this.product = product;
		this.level = level;
		this.experience = experience;
		this.animation = new Animation(animation);
	}

	public int getItem() {
		return item;
	}

	public int getProduct() {
		return product;
	}

	public int getLevel() {
		return level;
	}

	public double getExperience() {
		return experience;
	}

	public Animation getAnimation() {
		return animation;
	}

	static {
		for (CrossbowStringingData data : values()) {
			values.put(data.getItem(), data);
		}
	}

}
