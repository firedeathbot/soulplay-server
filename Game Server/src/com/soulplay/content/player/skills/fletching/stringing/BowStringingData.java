package com.soulplay.content.player.skills.fletching.stringing;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

public enum BowStringingData {
	COMPOSITE_BOW(4825, 4827, 30, 45, 6686),

	SHORT_BOW(50, 841, 5, 5, 6678),
	LONG_BOW(48, 839, 10, 10, 6684),
	OAK_SHORT_BOW(54, 843, 20, 16, 6679),
	OAK_LONG_BOW(56, 845, 25, 25, 6685),
	WILLOW_SHORT_BOW(60, 849, 35, 33, 6680),
	WILLOW_LONG_BOW(58, 847, 40, 41, 6686),
	MAPLE_SHORT_BOW(64, 853, 50, 50, 6681),
	MAPLE_LONG_BOW(62, 851, 55, 58, 6687),
	YEW_SHORT_BOW(68, 857, 65, 68, 6682),
	YEW_LONG_BOW(66, 855, 70, 75, 6688),
	MAGIC_SHORT_BOW(72, 861, 80, 83, 6683),
	MAGIC_LONG_BOW(70, 859, 85, 91, 6689);

	public static final Map<Integer, BowStringingData> values = new HashMap<>();

	private final int unStrung, strung, level, xp;
	private final Animation animation;

	private BowStringingData(int unStrung, int strung, int level, int xp, int animation) {
		this.unStrung = unStrung;
		this.strung = strung;
		this.level = level;
		this.xp = xp;
		this.animation = new Animation(animation);
	}

	public int getUnStrung() {
		return unStrung;
	}

	public int getStrung() {
		return strung;
	}

	public int getLevel() {
		return level;
	}

	public int getXp() {
		return xp;
	}

	public Animation getAnimation() {
		return animation;
	}

	static {
		for (BowStringingData data : values()) {
			values.put(data.getUnStrung(), data);
		}
	}

}
