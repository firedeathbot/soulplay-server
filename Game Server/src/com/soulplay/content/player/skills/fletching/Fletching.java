package com.soulplay.content.player.skills.fletching;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.javs.JavelinEnum;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

public class Fletching {

	public static final Animation FLETCH_ANIMATION = new Animation(1248);
	public static final int[][] ARROWS = {
			{52, 314, 53, 15, 1},
			{53, 39, 882, 40, 1},
			{53, 40, 884, 58, 15},
			{53, 41, 886, 95, 30},
			{53, 42, 888, 132, 45},
			{53, 43, 890, 170, 60},
			{53, 44, 892, 207, 75},
			{53, 11237, 11212, 225, 90},
	};

	public static final int KNIFE = 946;

	public enum LogData {
		LOGS(1511, new int[] { 50, 48, 52, 9440, JavelinEnum.JAVELIN_SHAFT }, new short[] { 5, 10, 5, 6, 5 }, new byte[] { 5, 10, 1, 9, 3 }, new byte[] { 1, 1, 15, 1, 15 }),
		OAK(1521, new int[] { 54, 56, 52, 9442, JavelinEnum.JAVELIN_SHAFT }, new short[] { 16, 25, 5, 16, 5 }, new byte[] { 20, 25, 1, 24, 3 }, new byte[] { 1, 1, 15, 1, 15 }),
		WILLOW(1519, new int[] { 60, 58, 52, 9444, JavelinEnum.JAVELIN_SHAFT }, new short[] { 33, 41, 5, 22, 5 }, new byte[] { 35, 40, 1, 39, 3 }, new byte[] { 1, 1, 15, 1, 15 }),
		MAPLE(1517, new int[] { 64, 62, 52, 9448, JavelinEnum.JAVELIN_SHAFT }, new short[] { 50, 58, 5, 32, 5 }, new byte[] { 50, 55, 1, 54, 3 }, new byte[] { 1, 1, 15, 1, 15 }),
		YEW(1515, new int[] { 68, 66, 52, 9452, JavelinEnum.JAVELIN_SHAFT }, new short[] { 67, 75, 5, 50, 5 }, new byte[] { 65, 70, 1, 72, 3 }, new byte[] { 1, 1, 15, 1, 15 }),
		MAGIC(1513, new int[] { 72, 70, 52, 121952, JavelinEnum.JAVELIN_SHAFT }, new short[] { 83, 91, 5, 70, 5 }, new byte[] { 75, 80, 1, 78, 3 }, new byte[] { 1, 1, 15, 1, 15 });

		public static final Map<Integer, LogData> values = new HashMap<>();
		private final int logId;
		private final int[] products;
		private final short[] xp;
		private final byte[] levelReq, productCount;

		private LogData(int logId, int[] products, short[] xp, byte[] levelReq, byte[] productCount) {
			this.logId = logId;
			this.products = products;
			this.xp = xp;
			this.levelReq = levelReq;
			this.productCount = productCount;
		}

		public int getLogId() {
			return logId;
		}

		public int[] getProducts() {
			return products;
		}

		public short[] getXp() {
			return xp;
		}

		public byte[] getLevelReq() {
			return levelReq;
		}

		public byte[] getProductCout() {
			return productCount;
		}

		static {
			for (LogData data : values()) {
				values.put(data.getLogId(), data);
			}
		}
	}

	public static boolean handleLog(Client c, int itemUsedWith, int itemUsedOn) {
		LogData data = LogData.values.get(itemUsedWith);
		if (data == null) {
			data = LogData.values.get(itemUsedOn);
			if (data == null) {
				return false;
			}
		}

		final LogData finalData = data;
		if (data.getProducts().length == 5) { // lesiks code to not add in a shit ton of -1's XD
			MakeInterface.make(c, data.getProducts()[0], () -> {
				startFletch(c, finalData, 0);
			}, data.getProducts()[1], () -> {
				startFletch(c, finalData, 1);
			}, data.getProducts()[2], () -> {
				startFletch(c, finalData, 2);
			}, data.getProducts()[3], () -> {
				startFletch(c, finalData, 3);
			}, data.getProducts()[4], () -> {
				startFletch(c, finalData, 4);
			});
		} else {
			MakeInterface.make(c, data.getProducts()[0], () -> {
				startFletch(c, finalData, 0);
			}, data.getProducts()[1], () -> {
				startFletch(c, finalData, 1);
			}, data.getProducts()[2], () -> {
				startFletch(c, finalData, 2);
			}, data.getProducts()[3], data.getProducts()[3] != -1 ? () -> {
				startFletch(c, finalData, 3);
			} : null);
		}
		return true;
	}

	private static void startFletch(Client player, LogData data, int index) {
		player.getPA().closeAllWindows();

		if (!canFletch(player, data, index) || player.skillingEvent != null) {
			return;
		}

		player.startAction(new CycleEvent() {

			int cycles = player.makeCount;

			@Override
			public void execute(CycleEventContainer container) {
				int productId = data.getProducts()[index];
				player.getItems().deleteItem2(data.getLogId(), 1);
				player.startAnimation(FLETCH_ANIMATION);
				player.getItems().addItem(productId, data.getProductCout()[index]);
				player.getPA().addSkillXP(data.getXp()[index], Skills.FLETCHING);
				player.sendMessageSpam("You fletch a " + ItemDef.forID(productId).getName() + ".");

				cycles--;
				if (cycles == 0 || !canFletch(player, data, index)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 3);
	}

	private static boolean canFletch(Client c, LogData data, int index) {
		if (!c.getItems().playerHasItem(KNIFE)) {
			c.sendMessage("You don't have a knife to fletch with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.FLETCHING] < data.getLevelReq()[index]) {
			c.sendMessage("You need a fletching level of " + data.getLevelReq()[index] + " to fletch this.");
			return false;
		}

		if (!c.getItems().playerHasItem(data.getLogId(), 1)) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to fletch.").execute();
			return false;
		}

		return true;
	}

	public static void makeArrows(Client c, final int item1, final int item2) {
		int slot = -1;

		for (int i = 0; i < ARROWS.length; i++) {
			if ((item1 == ARROWS[i][0] && item2 == ARROWS[i][1]) || (item2 == ARROWS[i][0] && item1 == ARROWS[i][1])) {
				if (c.getSkills().getLevel(Skills.FLETCHING) < ARROWS[i][4]) {
					c.sendMessage("You need a fletching level of " + ARROWS[i][4] + " to fletch this.");
					return;
				}

				if (!c.getItems().playerHasItem(item1, 15) || !c.getItems().playerHasItem(item2, 15)) {
					c.sendMessage("You must have 15 of each supply to do this.");
					return;
				}

				slot = i;
				break;
			}
		}

		if (slot == -1) {
			c.sendMessage("Nothing interesting happens.");
			return;
		}

		final int slotFinal = slot;
		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			int cycles = 0;
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (c.getItems().playerHasItem(item1, 15)
						&& c.getItems().playerHasItem(item2, 15)) {

					c.getItems().deleteItemInOneSlot(item1,
							c.getItems().getItemSlot(item1), 15);
					c.getItems().deleteItemInOneSlot(item2,
							c.getItems().getItemSlot(item2), 15);
					c.getItems().addItem(ARROWS[slotFinal][2], 15);
					c.getPA().addSkillXP(ARROWS[slotFinal][3],
							Skills.FLETCHING);

				} else {
					c.sendMessage(
							"You must have 15 of each supply to do this.");
					container.stop();
				}
				cycles++;
				if (cycles == 20) {
					c.sendMessage("You have made 300 arrows.");
					container.stop();
				}
			}
		}, 1);
	}

}
