package com.soulplay.content.player.skills.fletching.stringing;

import com.soulplay.game.world.entity.Animation;

public enum LimbData {
	WOODEN_STOCK(9440, 9420, 9454, 9, 12, Animation.create(4436)),
	OAK_STOCK(9442, 9422, 9176, 24, 32, Animation.create(4437)),
	WILLOW_STOCK(9444, 9423, 9457, 39, 44, Animation.create(4438)),
	TEAK_STOCK(9446, 9425, 9459, 46, 54, Animation.create(4439)),
	MAPLE_STOCK(9448, 9427, 9461, 54, 64, Animation.create(4440)),
	MAHOGANY_STOCK(9450, 9429, 9463, 61, 82, Animation.create(4441)),
	YEW_STOCK(9452, 9431, 9465, 69, 100, Animation.create(4442)),
	MAGIC_STOCK(121952, 121918, 121921, 78, 135, Animation.create(4442));

	public static final LimbData[] values = values();
	private final int stock;
	private final int limb;
	private final int product;
	private final int level;
	private final double experience;
	private final Animation animation;

	LimbData(int stock, int limb, int product, int level, double experience, Animation animation) {
		this.stock = stock;
		this.limb = limb;
		this.product = product;
		this.level = level;
		this.experience = experience;
		this.animation = animation;
	}

	public int getStock() {
		return stock;
	}

	public int getLimb() {
		return limb;
	}

	public int getProduct() {
		return product;
	}

	public int getLevel() {
		return level;
	}

	public double getExperience() {
		return experience;
	}

	public Animation getAnimation() {
		return animation;
	}

	public static LimbData forItems(int item, int second) {
		for (LimbData data : LimbData.values) {
			if (data.getLimb() == item && data.getStock() == second
					|| data.getLimb() == second && data.getStock() == item) {
				return data;
			}
		}

		return null;
	}

}
