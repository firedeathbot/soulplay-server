package com.soulplay.content.player.skills.fletching.javs;

public enum JavelinEnum {
	
	DRAGON_JAVELIN(30040, 30025, 92, 15);
	
	private final int headId, productId, lvlReq, exp;

	private JavelinEnum(int headId, int prodId, int lvlReq, int expPerJav) {
		this.headId = headId;
		this.productId = prodId;
		this.lvlReq = lvlReq;
		this.exp = expPerJav;
	}
	
	public int getHeadId() {
		return headId;
	}

	public int getProductId() {
		return productId;
	}

	public int getLvlReq() {
		return lvlReq;
	}

	public int getExp() {
		return exp;
	}
	
	public static final JavelinEnum[] values = values();
	
	public static final int JAVELIN_SHAFT = 30035;
	
	public static JavelinEnum get(int id) {
		for (int i = 0; i < values.length; i++) {
			JavelinEnum jav = values[i];
			if (jav.getHeadId() == id) {
				return jav;
			}
		}
		return null;
	}

}
