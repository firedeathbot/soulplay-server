package com.soulplay.content.player.skills.fletching.bolts;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.world.entity.Animation;

/**
 * Represents gems to cut into bolt tips.
 * @author 'Vexia
 * @date 01/12/2013
 */
public enum GemBoltTips {
	OPAL(1609, new Item(45, 12), 11, 1, 890),
	JADE(1611, new Item(9187, 12), 26, 2, 891),
	RED_TOPAZ(1613, new Item(9188, 12), 48, 3, 892),
	SAPPHIRE(1607, new Item(9189, 12), 56, 4, 888),
	EMERALD(1605, new Item(9190, 12), 58, 5, 889),
	RUBY(1603, new Item(9191, 12), 63, 6, 887),
	DIAMOND(1601, new Item(9192, 12), 65, 7, 886),
	DRAGONSTONE(1615, new Item(9193, 12), 71, 8, 885),
	ONYX(6573, new Item(9194, 24), 73, 9, 886);

	public static final GemBoltTips[] values = GemBoltTips.values();

	/**
	 * Constructs a new {@code Gem.java} {@code Object}.
	 * @param gem the gem.
	 * @param bolt the bolt.
	 * @param level the level.
	 * @param experience the experience.
	 */
	GemBoltTips(int gem, Item bolt, int level, int experience, int anim) {
		this.gem = gem;
		this.bolt = bolt;
		this.level = level;
		this.experience = experience;
		this.anim = new Animation(anim);
	}

	/**
	 * Represents the gem.
	 */
	private final int gem;

	/**
	 * Represents the bolt.
	 */
	private final Item bolt;

	/**
	 * Represents the level required.
	 */
	private final int level;

	/**
	 * Represents the experience gained.
	 */
	private final int experience;
	
	private final Animation anim;

	/**
	 * Gets the gem.
	 * @return The gem.
	 */
	public int getGem() {
		return gem;
	}

	/**
	 * Gets the bolt.
	 * @return The bolt.
	 */
	public Item getBolt() {
		return bolt;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the experience.
	 * @return The experience.
	 */
	public int getExperience() {
		return experience;
	}
	
	public Animation getAnimation() {
		return anim;
	}

	/**
	 * Method used to get a gem for the item.
	 * @param item the item.
	 * @return the gem.
	 */
	public static GemBoltTips forItem(final int item) {
		for (GemBoltTips gem : values) {
			if (gem.getGem() == item) {
				return gem;
			}
		}

		return null;
	}

}
