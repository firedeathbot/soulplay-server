package com.soulplay.content.player.skills.fletching.stringing;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;

public class CrossbowStringing {

	public static final int CROSSBOW_STRING = 9438;

	private static boolean canString(Client c, CrossbowStringingData data) {
		if (!c.getItems().playerHasItem(CROSSBOW_STRING)) {
			c.sendMessage("You don't have a Crossbow string to string with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.FLETCHING] < data.getLevel()) {
			c.sendMessage("You need a Fletching level of " + data.getLevel() + " to string this crossbow.");
			return false;
		}

		if (!c.getItems().playerHasItem(data.getItem(), 1)) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to string.").execute();
			return false;
		}

		return true;
	}

	public static boolean string(Client c, int itemUsedWith, int itemUsedOn) {
		CrossbowStringingData data = CrossbowStringingData.values.get(itemUsedWith);
		if (data == null) {
			data = CrossbowStringingData.values.get(itemUsedOn);
			if (data == null) {
				return false;
			}
		}

		if (!canString(c, data) || c.skillingEvent != null) {
			return true;
		}

		final CrossbowStringingData finalData = data;
		c.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItemInOneSlot(finalData.getItem(), 1);
				c.getItems().deleteItemInOneSlot(CROSSBOW_STRING, 1);
				c.getItems().addItem(finalData.getProduct(), 1);
				c.getPA().addSkillXP((int) finalData.getExperience(), Skills.FLETCHING);
				c.sendMessageSpam("You add a string to the "
						+ ItemConstants.getItemName(finalData.getProduct()).toLowerCase() + ".");
				c.startAnimation(finalData.getAnimation());

				if (!canString(c, finalData)) {
					container.stop();
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 4);
		return true;
	}
}
