package com.soulplay.content.player.skills.fletching.stringing;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;

public class BowStringing {

	public static final int BOW_STRING = 1777;

	private static boolean canString(Client c, BowStringingData data) {
		if (!c.getItems().playerHasItem(BOW_STRING)) {
			c.sendMessage("You don't have a Bowstring to string with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.FLETCHING] < data.getLevel()) {
			c.sendMessage("You need a Fletching level of " + data.getLevel() + " to string this bow.");
			return false;
		}

		if (!c.getItems().playerHasItem(data.getUnStrung(), 1)) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to string.").execute();
			return false;
		}

		return true;
	}

	public static boolean string(Client c, int itemUsedWith, int itemUsedOn) {
		BowStringingData data = BowStringingData.values.get(itemUsedWith);
		if (data == null) {
			data = BowStringingData.values.get(itemUsedOn);
			if (data == null) {
				return false;
			}
		}

		if (!canString(c, data) || c.skillingEvent != null) {
			return true;
		}

		final BowStringingData finalData = data;
		c.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItemInOneSlot(finalData.getUnStrung(), 1);
				c.getItems().deleteItemInOneSlot(BOW_STRING, 1);
				c.getItems().addItem(finalData.getStrung(), 1);
				c.getPA().addSkillXP(finalData.getXp(), Skills.FLETCHING);
				c.sendMessageSpam("You add a string to the " + ItemConstants.getItemName(finalData.getStrung()).toLowerCase() + ".");
				c.startAnimation(finalData.getAnimation());

				if (!canString(c, finalData)) {
					container.stop();
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 4);
		return true;
	}
}
