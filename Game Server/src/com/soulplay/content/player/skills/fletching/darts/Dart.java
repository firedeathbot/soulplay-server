package com.soulplay.content.player.skills.fletching.darts;

import com.soulplay.game.model.item.Item;

/**
 * Represents an enum of bolts.
 * @author 'Vexia
 */
public enum Dart {
	BRONZE_DART(new Item(819), new Item(806), 1, 1.8), 
	IRON_DART(new Item(820), new Item(807), 22, 3.8), 
	STEEL_DART(new Item(821), new Item(808), 23, 7.5), 
	MITHRIL_DART(new Item(822), new Item(809), 52, 11.2), 
	ADAMANTITE_DART(new Item(823), new Item(810), 67, 15), 
	RUNITE_DART(new Item(824), new Item(811), 81, 18.8), 
	DRAGON_DART(new Item(11232), new Item(11230), 95, 25);
	
	public static final Dart[] values = Dart.values();

	/**
	 * The item required.
	 */
	private final Item item;

	/**
	 * The product received.
	 */
	private final Item product;

	/**
	 * The level required.
	 */
	private final int level;

	/**
	 * The experience gained.
	 */
	private final double experience;

	/**
	 * Constructs a new {@code Dart} {@code Object}.
	 * @param item the item.
	 * @param product the product.
	 * @param level the level.
	 * @param experience the experienece.
	 */
	Dart(Item item, Item product, int level, double experience) {
		this.item = item;
		this.product = product;
		this.level = level;
		this.experience = experience;
	}

	/**
	 * Gets the item.
	 * @return The item.
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * Gets the product.
	 * @return The product.
	 */
	public Item getProduct() {
		return product;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the experience.
	 * @return The experience.
	 */
	public double getExperience() {
		return experience;
	}

	/**
	 * Method used to get the dart for the item.
	 * @param item the item.
	 * @return the dart.
	 */
	public static Dart forItem(final Item item) {
		for (Dart bolt : Dart.values) {
			if (bolt.getItem().getId() == item.getId()) {
				return bolt;
			}
		}
		return null;
	}
}