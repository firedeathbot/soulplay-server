package com.soulplay.content.player.skills;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Events;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.config.GraphicCollection;
import com.soulplay.util.Misc;

/**
 * Class Thieving Handles Thieving
 *
 * @author PapaDoc 00:28 01/09/2010
 */

public class Thieving extends SkillHandler {

	private static int[][] pickpocket = { // npcId, lvl,
			{1, 1, 8, 1000, 3, 10, 20, 30}, {2, 1, 8, 1000, 3, 10, 20, 30},
			{3, 1, 8, 1000, 3, 10, 20, 30}, {3, 1, 8, 1000, 3, 10, 20, 30},
			{5, 1, 8, 1000, 3, 10, 20, 30}, {6, 1, 8, 1000, 3, 10, 20, 30},
			{3223, 1, 8, 1000, 3, 10, 20, 30},
			{3224, 1, 8, 1000, 3, 10, 20, 30},
			{3226, 1, 8, 1000, 3, 10, 20, 30},
			{3227, 1, 8, 1000, 3, 10, 20, 30},
			{3915, 1, 8, 1000, 3, 10, 20, 30}, {7, 10, 15, 1000, 3, 20, 30, 40},
			{9, 40, 47, 1200, 3, 50, 60, 70}, {15, 25, 26, 1500, 3, 35, 45, 55},
			{20, 70, 151, 6000, 8, 80, 90, 99},
			{2256, 70, 151, 6000, 8, 80, 90, 99},
			{21, 80, 275, 13000, 8, 90, 95, 99},
			{23, 55, 84, 1000, 7, 65, 75, 85},
			{26, 55, 84, 1000, 7, 65, 75, 85},
			{18, 25, 26, 1200, 3, 35, 45, 55},
			{3225, 10, 15, 1000, 3, 20, 30, 40},
			{2234, 38, 43, 2000, 7, 48, 58, 68},
			{2235, 38, 43, 2000, 7, 48, 58, 68},
			{32, 40, 47, 1200, 3, 50, 60, 70},
			{296, 40, 47, 22000, 3, 50, 60, 70}, // wilderness thieving
			{297, 40, 47, 22000, 3, 50, 60, 70}, // wilderness thieving
			{298, 40, 47, 22000, 3, 50, 60, 70}, // wilderness thieving
			{299, 40, 47, 22000, 3, 50, 60, 70}, // wilderness thieving
			{2699, 40, 47, 1200, 3, 50, 60, 70},
			{2701, 40, 47, 1200, 3, 50, 60, 70},
			{2702, 40, 47, 1200, 3, 50, 60, 70},
			{2703, 40, 47, 1200, 3, 50, 60, 70},
			{3228, 40, 47, 1200, 3, 50, 60, 70},
			{3229, 40, 47, 1200, 3, 50, 60, 70},
			{3230, 40, 47, 1200, 3, 50, 60, 70},
			{3231, 40, 47, 1200, 3, 50, 60, 70},
			{3232, 40, 47, 1200, 3, 50, 60, 70},
			{3233, 40, 47, 1200, 3, 50, 60, 70},
			{3241, 40, 47, 1200, 3, 50, 60, 70},
			{4307, 40, 47, 1200, 3, 50, 60, 70},
			{4308, 40, 47, 1200, 3, 50, 60, 70},
			{4309, 40, 47, 1200, 3, 50, 60, 70},
			{4310, 40, 47, 1200, 3, 50, 60, 70},
			{4311, 40, 47, 1200, 3, 50, 60, 70},
			{5919, 40, 47, 1200, 3, 50, 60, 70},
			{5920, 40, 47, 1200, 3, 50, 60, 70},};

	public static void attemptToPickpocket(final Client c, NPC npc) {

		if (c.isStunned() || npc == null || System.currentTimeMillis() - c.lastThieve < 2000) {
			return;
		}

		if (c.underAttackBy > 0 || c.underAttackBy2 > 0) {
			c.sendMessage("You can't pickpocket while in combat!");
			return;
		}
		
		boolean glovesOfSilence = c.playerEquipment[PlayerConstants.playerHands] == 10075;
		
		for (int i = 0; i < pickpocket.length; i++) {
			if (npc.npcType == pickpocket[i][0]) {
				if (!hasRequiredLevel(c, Skills.THIEVING, pickpocket[i][1], "thieving",
						"pickpocket this")) {
					return;
				}
				c.sendMessage("You attempt to pickpocket the "
						+ npc.def.name + "...");
				c.startAnimation(881);
				
				switch (pickpocket[i][0]) {
				
				case 296:// wild thieving
				case 297:
				case 298:
				case 299:
					int minRoll = glovesOfSilence ? 30 : 10;
					int maxRoll = glovesOfSilence ? 240 : 220;
					int gearBonuses = c.getItems().getTotalBonuses();
					int bonusInterp = Misc.interpolateSafe(0, 260, 0, 1000, gearBonuses);
					boolean gearSuccess = bonusInterp > Misc.random(254);
//					c.sendMessage("rate: " + bonusInterp + " / 254");
					boolean success = Skills.successRoll(minRoll, maxRoll, c.getSkills().getLevel(Skills.THIEVING));
					if (!success || !gearSuccess) {
						if (!gearSuccess) {
							c.sendMessage("The guard notices your weak gear and is careful of you.");
						}
	                    npc.turnNpc(c.getX(), c.getY());
						failThieve(c, i, npc);
					} else {
						completeThieve(c, i, npc);
					}
					break;
				
				default:
					int addition = glovesOfSilence ? 5 : 0;
					if (Misc.random(c.getSkills().getLevel(Skills.THIEVING) + 5 + addition) < Misc
							.random(pickpocket[i][1])) {
	                    npc.turnNpc(c.getX(), c.getY());
						failThieve(c, i, npc);
					} else {
						completeThieve(c, i, npc);
					}
					break;
				}
				c.increaseProgress(TaskType.PICKPOCKET, npc.npcType);
				c.lastThieve = System.currentTimeMillis();
			}
		}
	}

	private static final GameItem pkptoken = new GameItem(4278, 1);
	
	private static void completeThieve(final Client c, final int i, NPC npc) {
		final int loot = pickpocket[i][3];
		final String message = "...You successfully pickpocket the " + npc.def.name + ".";
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				
				int xp = pickpocket[i][2];
				if (c.inWild()) {
					xp *= 2;
				}

				c.getPA().addSkillXP(xp, Skills.THIEVING);
				c.sendMessage(message);
				c.getItems().addItem(995, loot);
				c.getAchievement().Thiev300K(loot);
				c.getAchievement().thiev10M(loot);
				c.increaseProgress(TaskType.PICKPOCKET_GP, c.npcType, loot);
				c.increaseProgress(TaskType.PICKPOCKET, c.npcType);
				switch (c.npcType) {
					case 296:
					case 297:
					case 298:
					case 299:
						c.increaseProgress(TaskType.PICKPOCKET_PKP, c.npcType, 1);
						c.guardStealCount += 1;
						if (c.guardStealCount == 100) {
							c.getItems().addOrDrop(pkptoken);
							c.guardStealCount = 0;
						} else
							c.sendMessage("Thieve " + (100 - c.guardStealCount) + " times to obtain 1 Pk Point Token.");
						break;
				}
			}

			@Override
			public void stop() {

			}
		}, 2);
	}

	private static void failThieve(final Client c, final int i, NPC npc) {
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				
				c.dealDamage(new Hit(pickpocket[i][4], 0, 0));

				c.stunEntity(10, GraphicCollection.STUN_GFX);

				c.lastThieve = System.currentTimeMillis() + 7500;
				c.sendMessage("...you fail to pickpocket the " + npc.def.name + "!");
				c.sendMessage("You've been stunned!");
			}

			@Override
			public void stop() {

			}
		}, 2);

	}

	public static boolean pickpocketNPC(Client c, int npc) {
		for (int[] element : pickpocket) {
			if (npc == element[0]) {
				return true;
			}
		}
		return false;
	}

	public static void stealFromStall(final Client c, GameObject o,
			final int id, int amount, int xp, int level, final int i,
			final int x, final int y, final int face) {
		if (System.currentTimeMillis() - c.lastThieve < 3000) {
			return;
		}
		if (c.underAttackBy > 0 || c.underAttackBy2 > 0) {
			c.sendMessage("You can't steal from a stall while in combat!");
			return;
		}
		if (WorldType.equalsType(WorldType.SPAWN)) {
			return;
		}
		if (Misc.random(200) == 0) {
			if (c.isBot() && Misc.random(1000) == 0) {
				Events.Guard(c);
			} else {
				Events.Guard(c);
			}

		}
		if (c.getSkills().getLevel(Skills.THIEVING) >= level) {
			if (c.getItems().addItem(id, amount)) {
				c.startMoneyPouchCooldown();
				c.startAnimation(832);
				c.getPA().addSkillXP(xp, Skills.THIEVING);
				c.lastThieve = System.currentTimeMillis();
				c.faceLocation(c.objectX, c.objectY);
				c.sendMessageSpam("You steal some coins.");
				c.getAchievement().steal();
				if (id == 995) {
					c.increaseProgress(TaskType.PICKPOCKET_GP, o.getId(), amount);
					c.getAchievement().thiev10M(amount);
					c.getAchievement().Thiev300K(amount);
				}
				c.increaseProgress(TaskType.PICKPOCKET_STALL, o.getId());
			}
		} else {
			c.sendMessage("You must have a thieving level of " + level
					+ " to thieve from this stall.");
		}
	}
}
