package com.soulplay.content.player.skills.smithing;


import java.util.HashMap;
import java.util.Map;

public enum SmeltingData {

	BRONZE(438, 436, 1, 2349, 6, 1),
	IRON(440, -1, 0, 2351, 13, 15),
	SILVER(442, -1, 0, 2355, 14, 20),
	STEEL(440, 453, 1, 2353, 18, 30),
	GOLD(444, -1, 0, 2357, 23, 40),
	MITHRIL(447, 453, 1, 2359, 30, 50),
	ADAMANT(449, 453, 1, 2361, 38, 70),
	RUNE(451, 453, 1, 2363, 50, 85);

	public static final Map<Integer, SmeltingData> values = new HashMap<>();
	private final int xp, ore1Id, ore2Id, ore2Amount, barId, level;

	private SmeltingData(int ore1Id, int ore2Id, int ore2Amount, int barId, int xp, int level) {
		this.ore1Id = ore1Id;
		this.ore2Id = ore2Id;
		this.ore2Amount = ore2Amount;
		this.barId = barId;
		this.xp = xp;
		this.level = level;
	}

	public int getXp() {
		return xp;
	}

	public int getOre1Id() {
		return ore1Id;
	}

	public int getOre2Id() {
		return ore2Id;
	}

	public int getOre2Amount() {
		return ore2Amount;
	}

	public int getBarId() {
		return barId;
	}

	public int getLevel() {
		return level;
	}

	static {
		for (SmeltingData data : values()) {
			values.put(data.getOre1Id(), data);
			if (data.getOre2Id() != -1 && data.getOre2Id() != 453) {//Don't want coal to be in lookup table
				values.put(data.getOre2Id(), data);
			}
		}
	}
	
    public static void load() {
    	/* empty */
    }

}
