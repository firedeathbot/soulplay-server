package com.soulplay.content.player.skills.smithing;

public class SmithingInterfaceData {

	public static final int ENTRY_COUNT = 5;

	private final int hammerId;
	private final int barId;
	private final float barXp;
	private final SmithingInterfaceEntry[][] entries;

	public SmithingInterfaceData(int barId, int hammerId, float barXp) {
		this.barId = barId;
		this.hammerId = hammerId;
		this.barXp = barXp;
		this.entries = new SmithingInterfaceEntry[ENTRY_COUNT][ENTRY_COUNT];
	}

	public void addEntry(int row, int column, SmithingInterfaceEntry entry) {
		entries[row][column] = entry;
	}

	public int getBarId() {
		return barId;
	}

	public int getHammerId() {
		return hammerId;
	}

	public float getBarXp() {
		return barXp;
	}

	public SmithingInterfaceEntry[][] getEntries() {
		return entries;
	}

}
