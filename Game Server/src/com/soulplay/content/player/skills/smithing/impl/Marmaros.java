package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Marmaros {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17654, Smithing.DUNG_HAMMER, 28f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(20, 16773, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(23, 16939, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(24, 16387, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(27, 17079, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(28, 16893, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(22, 16365, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(22, 16299, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(23, 17023, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(25, 15757, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(28, 16409, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(26, 16717, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(27, 16673, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(27, 16651, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(29, 17243, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(25, 16695, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(26, 17345, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(21, 16343, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(21, 16277, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(20, 17895, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
