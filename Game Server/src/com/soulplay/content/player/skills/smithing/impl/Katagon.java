package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Katagon {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17664, Smithing.DUNG_HAMMER, 73f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(70, 16813, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(73, 16949, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(74, 16397, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(77, 17119, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(78, 16903, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(72, 16375, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(72, 16309, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(73, 17033, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(75, 15767, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(78, 16419, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(76, 16727, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(77, 16683, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(77, 16661, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(79, 17253, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(75, 16705, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(76, 17353, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(71, 16353, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(71, 16287, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(70, 17920, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
