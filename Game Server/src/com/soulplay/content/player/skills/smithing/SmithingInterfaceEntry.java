package com.soulplay.content.player.skills.smithing;

import com.soulplay.game.model.item.Item;

public class SmithingInterfaceEntry {

	private final int level, barCount;
	private final String name;
	private final Item item;

	private SmithingInterfaceEntry(int level, int itemId, int itemAmount, int barCount, String name) {
		this.level = level;
		this.item = new Item(itemId + 1, itemAmount);
		this.barCount = barCount;
		this.name = name;
	}

	public static SmithingInterfaceEntry make(int level, int itemId, int itemAmount, int barCount, String name) {
		return new SmithingInterfaceEntry(level, itemId, itemAmount, barCount, name);
	}

	public int getLevel() {
		return level;
	}

	public Item getItem() {
		return item;
	}

	public int getBarCount() {
		return barCount;
	}

	public String getName() {
		return name;
	}

}
