package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Zephyrium {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17660, Smithing.DUNG_HAMMER, 55f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(50, 16797, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(53, 16945, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(54, 16393, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(57, 17103, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(58, 16899, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(52, 16371, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(52, 16305, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(53, 17029, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(55, 15763, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(58, 16415, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(56, 16723, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(57, 16679, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(57, 16657, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(59, 17249, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(55, 16701, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(56, 17351, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(51, 16349, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(51, 16283, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(50, 17910, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
