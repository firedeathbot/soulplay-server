package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Kratonite {

	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17656, Smithing.DUNG_HAMMER, 37f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(30, 16781, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(33, 16941, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(34, 16387, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(37, 17087, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(38, 16895, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(32, 16367, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(32, 16301, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(33, 17025, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(35, 15759, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(38, 16411, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(36, 16719, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(37, 16675, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(37, 16653, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(39, 17245, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(35, 16697, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(36, 17347, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(31, 16345, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(31, 16279, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(30, 17900, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}
	
}
