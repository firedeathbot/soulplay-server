package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Bathus {

	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17652, Smithing.DUNG_HAMMER, 19f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(10, 16765, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(13, 16937, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(14, 16385, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(17, 17071, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(18, 16891, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(12, 16363, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(12, 16297, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(13, 17021, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(15, 15755, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(18, 16407, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(16, 16715, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(17, 16671, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(17, 16649, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(19, 17241, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(15, 16693, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(16, 17343, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(11, 16341, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(11, 16275, 1, 1, "Gauntlets"));
		
		instance.addEntry(0, 4, SmithingInterfaceEntry.make(10, 17890, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}
	
}
