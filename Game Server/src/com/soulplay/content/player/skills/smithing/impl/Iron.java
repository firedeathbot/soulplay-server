package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Iron {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(2351, Smithing.HAMMER, 25f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(15, 1203, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(19, 1279, 1, 1, "Sword"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(20, 1323, 1, 2, "Scimitar"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(21, 1293, 1, 1, "Long sword"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(29, 1309, 1, 3, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(16, 1349, 1, 1, "Axe"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(17, 1420, 1, 1, "Mace"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(24, 1335, 1, 3, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(25, 1363, 1, 3, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(28, 3096, 1, 2, "Claws"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(26, 1101, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(31, 1067, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(31, 1081, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(33, 1115, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(18, 1137, 1, 1, "Medium helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(22, 1153, 1, 2, "Full helm"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(23, 1175, 1, 2, "Square shield"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(27, 1191, 1, 3, "Kite shield"));
		instance.addEntry(4, 3, SmithingInterfaceEntry.make(23, 9423, 1, 1, "Limbs"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(19, 820, 10, 1, "Dart tips"));
		instance.addEntry(1, 4, SmithingInterfaceEntry.make(20, 40, 15, 1, "Arrowtips"));
		instance.addEntry(2, 4, SmithingInterfaceEntry.make(18, 9377, 10, 1, "Bolts"));
		instance.addEntry(3, 4, SmithingInterfaceEntry.make(19, 4820, 15, 1, "Nails"));
		instance.addEntry(4, 4, SmithingInterfaceEntry.make(22, 863, 5, 1, "Knives"));
	}

	static {
		load();
	}
}
