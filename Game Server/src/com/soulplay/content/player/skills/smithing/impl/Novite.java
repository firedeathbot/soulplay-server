package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Novite {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17650, Smithing.DUNG_HAMMER, 10f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(1, 16757, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(3, 16935, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(4, 16383, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(7, 17063, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(8, 16889, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(2, 16361, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(2, 16295, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(3, 17019, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(5, 15753, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(8, 16405, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(6, 16713, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(7, 16669, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(7, 16647, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(9, 17239, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(5, 16691, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(6, 17341, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(1, 16339, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(1, 16273, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(1, 17885, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
