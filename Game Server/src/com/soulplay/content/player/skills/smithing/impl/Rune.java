package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Rune {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(2363, Smithing.HAMMER, 75f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(85, 1213, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(89, 1289, 1, 1, "Sword"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(90, 1333, 1, 2, "Scimitar"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(91, 1303, 1, 1, "Long sword"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(99, 1319, 1, 3, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(86, 1359, 1, 1, "Axe"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(87, 1432, 1, 1, "Mace"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(94, 1347, 1, 3, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(95, 1373, 1, 3, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(98, 3101, 1, 2, "Claws"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(96, 1113, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(99, 1079, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(99, 1093, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(99, 1127, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(88, 1147, 1, 1, "Medium helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(92, 1163, 1, 2, "Full helm"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(93, 1185, 1, 2, "Square shield"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(94, 1201, 1, 3, "Kite shield"));
		instance.addEntry(4, 3, SmithingInterfaceEntry.make(91, 9431, 1, 1, "Limbs"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(89, 824, 10, 1, "Dart tips"));
		instance.addEntry(1, 4, SmithingInterfaceEntry.make(90, 44, 15, 1, "Arrowtips"));
		instance.addEntry(2, 4, SmithingInterfaceEntry.make(88, 9381, 10, 1, "Bolts"));
		instance.addEntry(3, 4, SmithingInterfaceEntry.make(89, 4824, 15, 1, "Nails"));
		instance.addEntry(4, 4, SmithingInterfaceEntry.make(92, 868, 5, 1, "Knives"));
	}

	static {
		load();
	}

}
