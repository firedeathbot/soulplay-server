package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Dragon {

	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(0, Smithing.HAMMER, 100f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(1, 1215, 1, 0, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(4, 30088, 1, 0, "Long sword"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(5, 4587, 1, 0, "Scimitar"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(6, 1305, 1, 0, "Long sword"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(14, 7158, 1, 0, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(1, 6739, 1, 0, "Axe"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(2, 1434, 1, 0, "Mace"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(9, 30022, 1, 0, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(10, 1377, 1, 0, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(13, 14484, 1, 0, "Claws"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(11, 3140, 1, 0, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(16, 4087, 1, 0, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(16, 4585, 1, 0, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(18, 14479, 1, 0, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(3, 1149, 1, 0, "Medium helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(7, 11335, 1, 0, "Full helm"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(8, 1187, 1, 0, "Square shield"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(12, 30087, 1, 0, "Hunter crossbow"));
		instance.addEntry(4, 3, SmithingInterfaceEntry.make(6, 30069, 1, 0, "Harpoon"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(4, 11232, 10, 0, "Dart tips"));
		instance.addEntry(1, 4, SmithingInterfaceEntry.make(5, 11237, 15, 0, "Arrowtips"));
		instance.addEntry(2, 4, SmithingInterfaceEntry.make(3, 9341, 10, 0, "Bolts"));
		instance.addEntry(3, 4, SmithingInterfaceEntry.make(4, 30044, 15, 1, "Javelin head"));
		instance.addEntry(4, 4, SmithingInterfaceEntry.make(7, 30093, 5, 1, "Thrownaxe"));
	}

	static {
		load();
	}
	
}
