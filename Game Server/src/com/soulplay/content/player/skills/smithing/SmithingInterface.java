package com.soulplay.content.player.skills.smithing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.smithing.impl.*;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;

public class SmithingInterface {

	private static final Item EMPTY_ITEM = new Item(0, 0);
	private static final Map<Integer, Integer> indexToBar = new HashMap<>();
	private static final Map<Integer, Integer> indexToItem = new HashMap<>();
	public static final int COLUMN_ONE_ID = 1119;
	public static final int COLUMN_TWO_ID = 1120;
	public static final int COLUMN_THREE_ID = 1121;
	public static final int COLUMN_FOUR_ID = 1122;
	public static final int COLUMN_FIVE_ID = 1123;
	public static boolean dragonSmithEvent = false;

	public static int inventoryToColumn(int inventoryId) {
		switch (inventoryId) {
			case COLUMN_ONE_ID:
				return 0;
			case COLUMN_TWO_ID:
				return 1;
			case COLUMN_THREE_ID:
				return 2;
			case COLUMN_FOUR_ID:
				return 3;
			case COLUMN_FIVE_ID:
				return 4;
			default:
				return -1;
		}
	}

	public static void showSmithInterface(Client c, int itemId) {
		if (dragonSmithEvent == true) {
			open(c, Dragon.instance);
			dragonSmithEvent = false;
		} else if (itemId == Bronze.instance.getBarId()) {
			open(c, Bronze.instance);
		} else if (itemId == Iron.instance.getBarId()) {
			open(c, Iron.instance);
		} else if (itemId == Steel.instance.getBarId()) {
			open(c, Steel.instance);
		} else if (itemId == Mithril.instance.getBarId()) {
			open(c, Mithril.instance);
		} else if (itemId == Adamant.instance.getBarId()) {
			open(c, Adamant.instance);
		} else if (itemId == Rune.instance.getBarId()) {
			open(c, Rune.instance);
		}
	}

	public static boolean showSmithInterfaceDung(Client c, int itemId) {
		if (itemId == Novite.instance.getBarId()) {
			open(c, Novite.instance);
			return true;
		} else if (itemId == Bathus.instance.getBarId()) {
			open(c, Bathus.instance);
			return true;
		} else if (itemId == Marmaros.instance.getBarId()) {
			open(c, Marmaros.instance);
			return true;
		} else if (itemId == Kratonite.instance.getBarId()) {
			open(c, Kratonite.instance);
			return true;
		} else if (itemId == Fractite.instance.getBarId()) {
			open(c, Fractite.instance);
			return true;
		} else if (itemId == Zephyrium.instance.getBarId()) {
			open(c, Zephyrium.instance);
			return true;
		} else if (itemId == Argonite.instance.getBarId()) {
			open(c, Argonite.instance);
			return true;
		} else if (itemId == Katagon.instance.getBarId()) {
			open(c, Katagon.instance);
			return true;
		} else if (itemId == Gorgonite.instance.getBarId()) {
			open(c, Gorgonite.instance);
			return true;
		} else if (itemId == Promethium.instance.getBarId()) {
			open(c, Promethium.instance);
			return true;
		}

		return false;
	}

	public static void open(Client c, SmithingInterfaceData data) {
		c.openSmithingData = data;
		reset(c);
		applyData(c, data);
		c.getPacketSender().showInterface(994);
	}

	private static void reset(Client c) {
		c.getPacketSender().resetInvenory(COLUMN_ONE_ID);
		c.getPacketSender().resetInvenory(COLUMN_TWO_ID);
		c.getPacketSender().resetInvenory(COLUMN_THREE_ID);
		c.getPacketSender().resetInvenory(COLUMN_FOUR_ID);
		c.getPacketSender().resetInvenory(COLUMN_FIVE_ID);
	}

	private static void applyData(Client c, SmithingInterfaceData data) {
		int barCount = c.getItems().getItemAmount(data.getBarId());
		List<Item> items = new ArrayList<>();
		for (int column = 0; column < SmithingInterfaceData.ENTRY_COUNT; column++) {
			items.clear();

			for (int row = 0; row < SmithingInterfaceData.ENTRY_COUNT; row++) {
				SmithingInterfaceEntry entry = data.getEntries()[row][column];
				if (entry == null) {
					resetText(c, row, column);
					items.add(EMPTY_ITEM);
					continue;
				}

				setText(c, row, column, entry, barCount);
				items.add(entry.getItem());
			}

			c.getPacketSender().itemsOnInterface(COLUMN_ONE_ID + column, items);
		}
	}

	private static void setText(Client c, int row, int column, SmithingInterfaceEntry entry, int barCount) {
		int index = toIndex(row, column);
		StringBuilder barText = new StringBuilder();
		if (barCount >= entry.getBarCount()) {
			barText.append("<col=ff00>");
		}
		if (entry.getBarCount() == 1) {
			barText.append("1 bar");
		} else {
			barText.append(entry.getBarCount());
			barText.append(" bars");
		}

		c.getPacketSender().sendString(barText.toString(), indexToBar.get(index));

		StringBuilder itemText = new StringBuilder();
		if (c.getPlayerLevel()[Skills.SMITHING] >= entry.getLevel()) {
			itemText.append("<col=ffffff>");
		}
		itemText.append(entry.getName());
		c.getPacketSender().sendString(itemText.toString(), indexToItem.get(index));
	}

	private static void resetText(Client c, int row, int column) {
		int index = toIndex(row, column);
		c.getPacketSender().sendString("", indexToBar.get(index));
		c.getPacketSender().sendString("", indexToItem.get(index));
	}

	private static void add(int row, int column, int bar, int item) {
		int index = toIndex(row, column);
		indexToBar.put(index, bar);
		indexToItem.put(index, item);
	}

	private static int toIndex(int row, int column) {
		return row + column * SmithingInterfaceData.ENTRY_COUNT;
	}

	static {
		add(0, 0, 1125, 1094);
		add(1, 0, 1124, 1085);
		add(2, 0, 1116, 1087);
		add(3, 0, 1089, 1086);
		add(4, 0, 1090, 1088);

		add(0, 1, 1126, 1091);
		add(1, 1, 1129, 1093);
		add(2, 1, 1118, 1083);
		add(3, 1, 1095, 1092);
		add(4, 1, 8428, 8429);

		add(0, 2, 1109, 1098);
		add(1, 2, 1110, 1099);
		add(2, 2, 1111, 1100);
		add(3, 2, 1112, 1101);
		add(4, 2, 11459, 11461);

		add(0, 3, 1127, 1102);
		add(1, 3, 1113, 1103);
		add(2, 3, 1114, 1104);
		add(3, 3, 1115, 1105);
		add(4, 3, 13357, 13358);

		add(0, 4, 1128, 1107);
		add(1, 4, 1130, 1108);
		add(2, 4, 1131, 1106);
		add(3, 4, 1132, 1096);
		add(4, 4, 1135, 1134);
	}

    public static void load() {
    	/* empty */
    }
}
