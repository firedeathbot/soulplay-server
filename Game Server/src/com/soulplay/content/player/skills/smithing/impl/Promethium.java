package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Promethium {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17668, Smithing.DUNG_HAMMER, 91f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(90, 16829, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(93, 16953, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(94, 16401, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(97, 17135, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(98, 16907, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(92, 16379, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(92, 16313, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(93, 17037, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(95, 15771, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(98, 16423, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(96, 16731, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(97, 16687, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(97, 16665, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(99, 17257, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(95, 16709, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(96, 17359, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(91, 16357, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(91, 16291, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(90, 17930, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
