package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Fractite {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17658, Smithing.DUNG_HAMMER, 46f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(40, 16789, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(43, 16943, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(44, 16389, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(47, 17095, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(48, 16897, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(42, 16369, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(42, 16303, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(43, 17027, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(45, 15761, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(48, 16413, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(46, 16721, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(47, 16677, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(47, 16655, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(49, 17247, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(45, 16699, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(46, 17349, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(41, 16347, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(41, 16281, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(40, 17905, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
