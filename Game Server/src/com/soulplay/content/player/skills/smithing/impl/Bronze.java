package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Bronze {

	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(2349, Smithing.HAMMER, 12.5f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(1, 1205, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(4, 1277, 1, 1, "Sword"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(5, 1321, 1, 2, "Scimitar"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(6, 1291, 1, 1, "Long sword"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(14, 1307, 1, 3, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(1, 1351, 1, 1, "Axe"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(2, 1422, 1, 1, "Mace"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(9, 1337, 1, 3, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(10, 1375, 1, 3, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(13, 3095, 1, 2, "Claws"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(11, 1103, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(16, 1075, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(16, 1087, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(18, 1117, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(3, 1139, 1, 1, "Medium helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(7, 1155, 1, 2, "Full helm"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(8, 1173, 1, 2, "Square shield"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(12, 1189, 1, 3, "Kite shield"));
		instance.addEntry(4, 3, SmithingInterfaceEntry.make(6, 9420, 1, 1, "Limbs"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(4, 819, 10, 1, "Dart tips"));
		instance.addEntry(1, 4, SmithingInterfaceEntry.make(5, 39, 15, 1, "Arrowtips"));
		instance.addEntry(2, 4, SmithingInterfaceEntry.make(3, 9375, 10, 1, "Bolts"));
		instance.addEntry(3, 4, SmithingInterfaceEntry.make(4, 4819, 15, 1, "Nails"));
		instance.addEntry(4, 4, SmithingInterfaceEntry.make(7, 864, 5, 1, "Knives"));
	}

	static {
		load();
	}

}
