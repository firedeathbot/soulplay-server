package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Argonite {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17662, Smithing.DUNG_HAMMER, 64f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(60, 16805, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(63, 16947, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(64, 16395, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(67, 17111, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(68, 16901, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(62, 16373, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(62, 16307, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(63, 17031, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(65, 15765, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(68, 16417, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(66, 16725, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(67, 16681, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(67, 16659, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(69, 17251, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(65, 16703, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(66, 17353, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(61, 16351, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(61, 16285, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(60, 17915, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
