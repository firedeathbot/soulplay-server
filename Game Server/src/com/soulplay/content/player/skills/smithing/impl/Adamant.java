package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Adamant {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(2361, Smithing.HAMMER, 62.5f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(70, 1211, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(74, 1287, 1, 1, "Sword"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(75, 1331, 1, 2, "Scimitar"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(76, 1301, 1, 1, "Long sword"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(84, 1317, 1, 3, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(71, 1357, 1, 1, "Axe"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(72, 1430, 1, 1, "Mace"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(79, 1345, 1, 3, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(80, 1371, 1, 3, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(83, 3100, 1, 2, "Claws"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(81, 1111, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(86, 1073, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(86, 1091, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(88, 1123, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(73, 1145, 1, 1, "Medium helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(77, 1161, 1, 2, "Full helm"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(78, 1183, 1, 2, "Square shield"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(82, 1199, 1, 3, "Kite shield"));
		instance.addEntry(4, 3, SmithingInterfaceEntry.make(76, 9429, 1, 1, "Limbs"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(74, 823, 10, 1, "Dart tips"));
		instance.addEntry(1, 4, SmithingInterfaceEntry.make(75, 43, 15, 1, "Arrowtips"));
		instance.addEntry(2, 4, SmithingInterfaceEntry.make(73, 9380, 10, 1, "Bolts"));
		instance.addEntry(3, 4, SmithingInterfaceEntry.make(74, 4823, 15, 1, "Nails"));
		instance.addEntry(4, 4, SmithingInterfaceEntry.make(77, 867, 5, 1, "Knives"));
	}

	static {
		load();
	}

}
