package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Steel {

	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(2353, Smithing.HAMMER, 37.5f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(30, 1207, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(34, 1281, 1, 1, "Sword"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(35, 1325, 1, 2, "Scimitar"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(36, 1295, 1, 1, "Long sword"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(44, 1311, 1, 3, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(31, 1353, 1, 1, "Axe"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(32, 1424, 1, 1, "Mace"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(39, 1339, 1, 3, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(40, 1365, 1, 3, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(43, 3097, 1, 2, "Claws"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(41, 1105, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(46, 1069, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(46, 1083, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(48, 1119, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(33, 1141, 1, 1, "Medium helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(37, 1157, 1, 2, "Full helm"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(38, 1177, 1, 2, "Square shield"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(42, 1193, 1, 3, "Kite shield"));
		instance.addEntry(4, 3, SmithingInterfaceEntry.make(36, 9425, 1, 1, "Limbs"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(34, 821, 10, 1, "Dart tips"));
		instance.addEntry(1, 4, SmithingInterfaceEntry.make(35, 41, 15, 1, "Arrowtips"));
		instance.addEntry(2, 4, SmithingInterfaceEntry.make(33, 9378, 10, 1, "Bolts"));
		instance.addEntry(3, 4, SmithingInterfaceEntry.make(34, 1539, 15, 1, "Nails"));
		instance.addEntry(4, 4, SmithingInterfaceEntry.make(37, 865, 5, 1, "Knives"));
	}

	static {
		load();
	}
	
}
