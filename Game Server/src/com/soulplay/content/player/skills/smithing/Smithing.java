package com.soulplay.content.player.skills.smithing;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.smithing.impl.Dragon;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

public class Smithing {

	public static final Animation SMITH_ANIMATION = new Animation(898);
	public static final Graphic SMITH_GFX = Graphic.create(2123);
	public static final int HAMMER = 2347;
	public static final int DUNG_HAMMER = 17883;

	public static void startSmithing(Client c, int interfaceId, int row, int count) {
		SmithingInterfaceData data = c.openSmithingData;
		if (data == null || c.skillingEvent != null) {
			c.sendMessage("You can't smith now.");
			return;
		}

		c.getPA().closeAllWindows();

		int column = SmithingInterface.inventoryToColumn(interfaceId);
		if (column == -1 || row < 0 || row > SmithingInterfaceData.ENTRY_COUNT) {
			return;
		}

		SmithingInterfaceEntry entry = data.getEntries()[row][column];
		if (entry == null || !canSmith(c, data.getBarId(), entry, data.getHammerId())) {
			return;
		}

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int cycles = data == Dragon.instance ? 1 : count;

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItem2(data.getBarId(), entry.getBarCount());
				c.startAnimation(SMITH_ANIMATION);
				int itemId = entry.getItem().getId() - 1;
				c.getItems().addItem(itemId, entry.getItem().getAmount());
				c.getPA().addSkillXP((int) (data.getBarXp() * entry.getBarCount()), Skills.SMITHING);
				c.sendMessageSpam("You smith a " + ItemDef.forID(itemId).getName() + ".");
				c.increaseProgress(TaskType.SMITHING_MAKE, itemId);
				c.increaseProgress(TaskType.SMITHING_BAR_TYPE, data.getBarId());
				c.startGraphic(SMITH_GFX);
				if (itemId == 1127) {
					c.getAchievement().smithRunePlate();
				}

				cycles--;
				if (cycles == 0 || !canSmith(c, data.getBarId(), entry, data.getHammerId())) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, 3);
	}
	
	private static boolean canSmith(Client c, int barId, SmithingInterfaceEntry entry, int hammerId) {
		if (c.getItems().getItemAmount(barId) < entry.getBarCount()) {
			c.sendMessage("You don't have enough bars to smith this.");
			return false;
		}

		if (c.getSkills().getDynamicLevels()[Skills.SMITHING] < entry.getLevel()) {
			c.sendMessage("You need a smithing level of " + entry.getLevel() + " to smith this.");
			return false;
		}

		if (!c.getItems().playerHasItem(hammerId)) {
			c.sendMessage("You need a hammer to smith items.");
			return false;
		}

		return true;
	}

}
