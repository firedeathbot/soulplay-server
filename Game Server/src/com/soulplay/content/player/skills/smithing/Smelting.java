package com.soulplay.content.player.skills.smithing;

import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;

public class Smelting {

	public static final Animation SMELT_ANIMATION = new Animation(899);
	private static final int[] SMELT_BARS = { 2349, 2351, 2355, 2353, 2357, 2359, 2361, 2363 };
	private static final int[] SMELT_FRAME = { 2405, 2406, 2407, 2409, 2410, 2411, 2412, 2413 };

	public static boolean canSmelt(Client c, SmeltingData data, boolean sendMessage) {
		if (c.getSkills().getDynamicLevels()[Skills.SMITHING] < data.getLevel()) {
			if (sendMessage) {
				c.sendMessage("You must have a higher Smithing level to smelt this.");
			}
			return false;
		}

		if (!c.getItems().playerHasItem(data.getOre1Id())) {
			if (sendMessage) {
				c.sendMessage("You do not have the required ores to smelt this.");
			}
			return false;
		}

		if (data.getOre2Id() != -1 && !c.getItems().playerHasItem(data.getOre2Id(), data.getOre2Amount())) {
			if (sendMessage) {
				c.sendMessage("You do not have the required ores to smelt this.");
			}
			return false;
		}

		return true;
	}

	public static void sendSmelting(Client c) {
		for (int i = 0, length = SMELT_FRAME.length; i < length; i++) {
			c.getPacketSender().sendFrame246(SMELT_FRAME[i], 150, SMELT_BARS[i]);
		}

		c.getPacketSender().sendChatInterface(2400);
		c.smeltInterface = true;
	}

	public static void smelt(Client c, SmeltingData data, final int thisCount, Animation smeltAnimation, Graphic smeltGraphic) {
		c.startAction(new CycleEvent() {

			int count = thisCount;

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItem2(data.getOre1Id(), 1);
				if (data.getOre2Id() != -1) {
					c.getItems().deleteItem2(data.getOre2Id(), data.getOre2Amount());
				}

				SkillingUrns.handleUrn(c, Skills.SMITHING, data.getLevel(), data.getXp());
				c.increaseProgress(TaskType.SMITHING_SMELT, data.getBarId());
				c.getItems().addItem(data.getBarId(), 1);
				c.getPA().addSkillXP(data.getXp(), Skills.SMITHING);
				count--;
				c.startAnimation(smeltAnimation);
				if (smeltGraphic != null) {
					c.startGraphic(smeltGraphic);
				}

				if (data == SmeltingData.BRONZE) {
					c.getAchievement().smeltBronze();
				} else if (data == SmeltingData.RUNE) {
					c.getAchievement().smeltRune();
				}

				if (count == 0 || !canSmelt(c, data, true)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, 2);
	}

	public static void smeltCannonballs(Client c, final int thisCount) {
		if (c.skillingEvent != null) {
			return;
		}

		if (!c.getItems().playerHasItem(2353)) {
			c.sendMessage("You do not have the required bar to smelt cannonballs.");
			return;
		}

		c.getPA().closeAllWindows();
		c.startAnimation(SMELT_ANIMATION);

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int count = thisCount;

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItemInOneSlot(2353, c.getItems().getItemSlot(2353), 1);
				c.getItems().addItem(2, 4);
				c.getPA().addSkillXP(26, Skills.SMITHING);
				count--;
				c.startAnimation(SMELT_ANIMATION);
				c.increaseProgress(TaskType.SMITHING_MAKE, 2, 4);

				if (count == 0 || !c.getItems().playerHasItem(2353)) {
					c.sendMessage("You do not have the required bar to smelt cannonballs.");
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, 6);
	}

	public static void startSmelting(Client c, SmeltingData data, int count) {
		if (!c.smeltInterface || c.skillingEvent != null) {
			c.sendMessage("You can't smelt at this moment.");
			return;
		}

		c.getPA().closeAllWindows();
		if (!canSmelt(c, data, true)) {
			return;
		}

		smelt(c, data, count, SMELT_ANIMATION, null);
	}
}
