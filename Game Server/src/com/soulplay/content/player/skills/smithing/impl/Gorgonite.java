package com.soulplay.content.player.skills.smithing.impl;

import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceData;
import com.soulplay.content.player.skills.smithing.SmithingInterfaceEntry;

public class Gorgonite {
	
	public static SmithingInterfaceData instance;

	public static void load() {
		instance = new SmithingInterfaceData(17666, Smithing.DUNG_HAMMER, 82f);
		instance.addEntry(0, 0, SmithingInterfaceEntry.make(80, 16821, 1, 1, "Dagger"));
		instance.addEntry(1, 0, SmithingInterfaceEntry.make(83, 16951, 1, 2, "Rapier"));
		instance.addEntry(2, 0, SmithingInterfaceEntry.make(84, 16399, 1, 2, "Longsword"));
		instance.addEntry(3, 0, SmithingInterfaceEntry.make(87, 17127, 1, 4, "Spear"));
		instance.addEntry(4, 0, SmithingInterfaceEntry.make(88, 16905, 1, 4, "2-hand sword"));

		instance.addEntry(0, 1, SmithingInterfaceEntry.make(82, 16377, 1, 1, "Hatchet"));
		instance.addEntry(1, 1, SmithingInterfaceEntry.make(82, 16311, 1, 2, "Pickaxe"));
		instance.addEntry(2, 1, SmithingInterfaceEntry.make(83, 17035, 1, 2, "Warhammer"));
		instance.addEntry(3, 1, SmithingInterfaceEntry.make(85, 15769, 1, 2, "Battle axe"));
		instance.addEntry(4, 1, SmithingInterfaceEntry.make(88, 16421, 1, 4, "Maul"));

		instance.addEntry(0, 2, SmithingInterfaceEntry.make(86, 16729, 1, 3, "Chain body"));
		instance.addEntry(1, 2, SmithingInterfaceEntry.make(87, 16685, 1, 3, "Plate legs"));
		instance.addEntry(2, 2, SmithingInterfaceEntry.make(87, 16663, 1, 3, "Plate skirt"));
		instance.addEntry(3, 2, SmithingInterfaceEntry.make(89, 17255, 1, 5, "Plate body"));

		instance.addEntry(0, 3, SmithingInterfaceEntry.make(85, 16707, 1, 2, "Full helm"));
		instance.addEntry(1, 3, SmithingInterfaceEntry.make(86, 17357, 1, 3, "Kiteshield"));
		instance.addEntry(2, 3, SmithingInterfaceEntry.make(81, 16355, 1, 1, "Boots"));
		instance.addEntry(3, 3, SmithingInterfaceEntry.make(81, 16289, 1, 1, "Gauntlets"));

		instance.addEntry(0, 4, SmithingInterfaceEntry.make(80, 17925, 20, 1, "Arrowtips"));
	}

	static {
		load();
	}

}
