package com.soulplay.content.player.skills.farming;

import com.soulplay.game.model.player.Client;

public class FarmingManager {

	public static final double GROWING_SCALE = 20.0,
			COMPOST_DISEASE_CHANCE = 0.9, SUPERCOMPOST_DISEASE_CHANCE = 0.7;

	public static final int HERB_STAGE_TIME = 20, TREE_STAGE_TIME = 40,
			TREE_CONFIG = 502, HERB_CONFIG = 515, ALLOTMENT_ZERO = 504,
			ALLOTMENT_ONE = 505, CLEARING_PATCH_EXPERIENCE = 4,
			COMPOST_EXPERIENCE = 18, SUPERCOMPOST_EXPERIENCE = 26;

	public static boolean applyCompost(final Client client, final int id,
			final int x, final int y) {
		return client.getTrees().applyCompost(id, x, y)
				|| client.getHerbs().applyCompost(id, x, y);
	}

	public static boolean checkHealth(final Client client, final int x,
			final int y) {
		return client.getTrees().checkHealth(x, y);
	}

	public static boolean chopDown(final Client client, final int x,
			final int y) {
		return client.getTrees().chopDown(x, y);
	}

	public static boolean clear(final Client client, final int id, final int x,
			final int y) {
		return client.getHerbs().clear(id, x, y)
				|| client.getTrees().clear(id, x, y);
	}

	public static boolean cure(final Client client, final int id, final int x,
			final int y) {
		return client.getHerbs().cure(id, x, y)
				|| client.getTrees().cure(id, x, y);
	}

	public static boolean harvest(final Client client, final int x,
			final int y) {
		return client.getHerbs().harvest(x, y);
	}

	public static boolean inspect(final Client client, final int x,
			final int y) {
		return client.getTrees().inspect(x, y)
				|| client.getHerbs().inspect(x, y);
	}

	public static boolean plant(final Client client, final int id, final int x,
			final int y) {
		return client.getTrees().plant(id, x, y)
				|| client.getHerbs().plant(id, x, y);
	}

}
