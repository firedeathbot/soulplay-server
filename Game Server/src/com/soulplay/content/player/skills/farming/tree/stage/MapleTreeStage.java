package com.soulplay.content.player.skills.farming.tree.stage;

import com.soulplay.content.player.skills.farming.patch.PatchStage;

public enum MapleTreeStage implements PatchStage {

	STAGE_ONE("The maple sapling has only just been planted."),

	STAGE_TWO("The maple sapling grows a few small branches."),

	STAGE_THREE("The maple tree develops a small canopy."),

	STAGE_FOUR("The maple tree trunk straightens and the canopy grows larger."),

	STAGE_FIVE("The maple tree canopy grows."),

	STAGE_SIX("The maple tree canopy grows."),

	STAGE_SEVEN("The maple tree grows."),

	STAGE_EIGHT("The maple tree is ready to be harvested.");
	
	public static final MapleTreeStage[] values = MapleTreeStage.values();

	private String message;

	private MapleTreeStage(final String message) {
		this.message = message;
	}

	@Override
	public MapleTreeStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return MapleTreeStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public MapleTreeStage next() {
		final int index = this.ordinal() + 1;
		if (index > MapleTreeStage.values.length - 1) {
			return this;
		}
		return MapleTreeStage.values[index];
	}

	@Override
	public MapleTreeStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return MapleTreeStage.values[index];
	}
}
