package com.soulplay.content.player.skills.farming.tree.stage;

import com.soulplay.content.player.skills.farming.patch.PatchStage;

public enum YewTreeStage implements PatchStage {

	STAGE_ONE("The yew sapling has only just been planted."),

	STAGE_TWO("The yew sapling grows a few small branches"),

	STAGE_THREE("The yew tree develops several small canopies"),

	STAGE_FOUR(
			"The yew tree trunk texture becomes smoother and the canopies grow larger."),

	STAGE_FIVE("The yew tree grows larger."),

	STAGE_SIX(
			"The yew tree canopies become more angular and cone shaped due to the texture placement."),

	STAGE_SEVEN(
			"The yew tree gains a rougher tree bark texture and the base becomes darker."),

	STAGE_EIGHT(
			"The yew tree base becomes light again and the trunk loses its texture."),

	STAGE_NINE("The yew tree bark gains a stripy texture."),

	STAGE_TEN("The yew tree is ready to be harvested.");
	
	public static final YewTreeStage[] values = YewTreeStage.values();

	private String message;

	private YewTreeStage(final String message) {
		this.message = message;
	}

	@Override
	public YewTreeStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return YewTreeStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public YewTreeStage next() {
		final int index = this.ordinal() + 1;
		if (index > YewTreeStage.values.length - 1) {
			return this;
		}
		return YewTreeStage.values[index];
	}

	@Override
	public YewTreeStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return YewTreeStage.values[index];
	}
}
