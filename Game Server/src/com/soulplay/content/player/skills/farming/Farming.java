package com.soulplay.content.player.skills.farming;

import com.soulplay.Server;
import com.soulplay.content.player.skills.farming.patch.DefaultStage;
import com.soulplay.content.player.skills.farming.patch.Patch;
import com.soulplay.content.player.skills.farming.patch.PatchLocation;
import com.soulplay.content.player.skills.farming.patch.State;
import com.soulplay.content.player.skills.farming.tree.Tree;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public abstract class Farming {

	public enum ToolbeltItem {
		KNIFE(946),
		SHEARS(1735),
		AMULET_MOULD(1595),
		CHISEL(1755),
		HOLY_MOULD(1599),
		NECKALCE_MOULD(1597),
		NEEDLE(1733),
		RING_MOULD(1592),
		TIARA_MOULD(5523),
		CRAYFISH_CAGE(13431),
		FISHING_ROD(307),
		FLY_FISHING_ROD(309),
		HARPOON(311),
		LOBSTER_POT(301),
		SMALL_FISHING_NET(303),
		BRONZE_PICKAXE(1265),
		HAMMER(2347),
		BRONZE_HATCHET(1351),
		TINDERBOX(590),
		SAW(8794),
		AMMO_MOULD(4),
		BOLT_MOULD(9434),
		BRACELET_MOULD(11065),
		GLASSBLOWING_PIPE(1785),
		SICKLE_MOULD(2976),
		UNHOLY_MOULD(1594),
		SEED_DIBBER(5343),
		GARDENING_TROWEL(5325),
		RAKE(5341),
		SECATEURS(5329),
		PESTLE_AND_MORTAR(233),
		SPADE(952),
		BIG_FISHING_NET(305),
		MACHETE(975),
		BARBARIAN_ROD(11323),
		WATCH(2575),
		CHART(2576),
		CHAIN_LINK_MOULD(13153),
		NOOSE_WAND(10150);

		private int itemId;

		private ToolbeltItem(int itemId) {
			this.itemId = itemId;
		}

		public int getId() {
			return itemId;
		}
	}

	// item ids
	public static final int TROWEL = 5325, RAKE = 5341, SPADE = 952,
			WEED = 6055, COMPOST = 6032, SUPERCOMPOST = 6034, BUCKET = 1925,
			SECATEURS = 5329, MAGIC_SECATEURS = 7409, SEED_DIPPER = 5343,
			PLANT_CURE = 6036, VIAL = 229;

	// emotes
	public static final int PLANTING_POT = 2272, RAKING = 2273, KNEEL = 1331,
			STAND_UP = 1332, PRUNING = 2275, CHECK_HEALTH = 832,
			SEED_DIBBING = 2291, CURING = 2288, PICKING_HERB = 2282;

	public static final int SPADE_EMOTE = 830;

	public static final int CUT_WATERMELON_EMOTE = 2269;

	public static final int PULLING_SOMETHING_OUT_EMOTE = 2270;

	public static final int RIPPING_OUT_GROUND_EMOTE = 2271;

	public static final int SMALL_SPADE_EMOTE = 2272;

	public static final int RAKE_EMOTE = 2273;

	public static final int CLIP_EMOTE1 = 2274;

	public static final int CLIP_EMOTE2 = 2275;

	public static final int CLIP_EMOTE3 = 2276;

	public static final int CLIP_EMOTE4 = 2277;

	public static final int CLIP_EMOTE5 = 2278;

	public static final int CLIP_EMOTE6 = 2279;

	public static final int PICK_TREE_EMOTE = 2280;

	public static final int PICK_TREE_LOWER_EMOTE = 2281;

	public static final int PICK_VEGETABLES_EMOTE = 2282;

	public static final int COMPOST_EMOTE = 2283;

	public static final int POUR_GREEN_JAR_EMOTE = 2284;

	public static final int POUR_GLASS_EMOTE = 2285;

	public static final int TOUCH_HERB_EMOTE = 2286;

	public static final int CURE_PLANT_EMOTE = 2288;

	public static final int DRINK_GREEN_JAR_EMOTE = 2289;

	public static final int SEED_DIBBING_EMOTE = 2291;

	public static final int PUT_SOMETHING_SOMEWHERE_EMOTE = 2292;

	public static final int WATERING_EMOTE = 2293;
	
	public static final Graphic WATERING_GFX = Graphic.create(410, GraphicType.HIGH);

	public static final int TOUCHING_TREEMAYBE_EMOTE = 2295;

	public static int calculateConfigValue(final Patch patch, final int start) {
		if (patch.getSeed() < 1) {
			return patch.getStage().getStage();
		} else {
			int value = start + patch.getStage().getStage();
			switch (patch.getState()) {
				case DISEASED:
					value += 1 << 6;
					break;
				case DEAD:
					value += 2 << 6;
					break;
				case CHECK_HEALTH:
					value = Tree.getTree(patch.getSeed()).getCheck();
					break;
				case CHOP_DOWN:
					value = Tree.getTree(patch.getSeed()).getCheck() + 1;
					break;
				case STUMP:
					value = Tree.getTree(patch.getSeed()).getCheck() + 2;
					break;
				default:
					break;
			}
			return value;
		}
	}

	public static boolean calculatePatchStage(final Patch patch,
			final int time) {
		if (patch.getStage().getMessage() == null) {
			if (patch.getStage().getStage() < 4
					&& patch.getStage().getStage() > 0) {
				if (patch.reachedNextStage(time,
						4 - patch.getStage().getStage())
						&& patch.getState() == State.GROWING) {
					patch.setStage(patch.getStage().previous());
				}
			}
		}
		if (patch.getSeed() > 0) {
			if (patch.getStage().getStage() != patch.getStage().getMaxStage()) {
				if (patch.reachedNextStage(time, patch.getStage().getStage())
						&& patch.getState() != State.DEAD) {
					patch.setStage(patch.getStage().next());
					return true;
				}
			}
		}
		return false;
	}

	public static void calculatePatchState(final Patch patch,
			final int diseaseChance) {
		if (Tree.getTree(patch.getSeed()) != null) {
			if (patch.getStage().getStage() == patch.getStage().getMaxStage()
					&& patch.getState() == State.GROWING) {
				patch.setState(State.CHECK_HEALTH);
			}
		}
		/*
		 * if (patch.getState() == State.DEAD) { return; } if (patch.getState()
		 * == State.DISEASED) { patch.setState(State.DEAD); } if
		 * (patch.getState() == State.GROWING) { if (patch.getStage().getStage()
		 * < patch.getStage().getMaxStage()) { double chance = diseaseChance *
		 * patch.getDiseaseChance(); if ((Math.random() * 100) < chance) {
		 * patch.setState(State.DISEASED); } } }
		 */
	}

	public static void prepare(final Patch patch) {
		patch.setStage(DefaultStage.STAGE_FOUR);
		patch.setState(State.GROWING);
		patch.setSeed(0);
		patch.setPlantedTime(System.currentTimeMillis());
	}

	public static void reset(final Patch patch) {
		patch.setStage(DefaultStage.STAGE_ONE);
		patch.setState(State.GROWING);
		patch.setSeed(0);
		patch.setPlantedTime(0);
		patch.setHarvest(0);
		patch.setDiseaseChance(1);
	}

	protected Client client;

	public Farming(final Client client) {
		this.client = client;
	}

	public boolean applyCompost(final int id, final int x, final int y) {
		if (id != COMPOST && id != SUPERCOMPOST) {
			return false;
		}
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		if (patch.getStage().getMessage() != null && patch.getStage()
				.getStage() != patch.getStage().getMaxStage()) {
			this.client.sendMessage("This patch doesn't need compost.");
			return true;
		}
		if (patch.getState() == State.COMPOST) {
			this.client.sendMessage("This patch already has compost.");
			return true;
		}
		this.client.getItems().deleteItemInOneSlot(id, 1);
		this.client.getItems().addItem(BUCKET, 1);
		this.client.sendMessage(
				"You pour some " + (id == SUPERCOMPOST ? "super" : "")
						+ " compost onto the patch.");
		this.client.startAnimation(Farming.COMPOST_EMOTE);
		this.client.getPA().addSkillXP(
				(id == COMPOST
						? FarmingManager.COMPOST_EXPERIENCE
						: FarmingManager.SUPERCOMPOST_EXPERIENCE),
				RSConstants.FARMING);
		this.client.toggleBusy();
		final Task task = new Task(7) {

			@Override
			public void execute() {
				patch.setDiseaseChance(id == COMPOST
						? FarmingManager.COMPOST_DISEASE_CHANCE
						: FarmingManager.SUPERCOMPOST_DISEASE_CHANCE);
				patch.setState(State.COMPOST);
				this.stop();
			}

			@Override
			public void stop() {
				super.stop();
				Farming.this.client.toggleBusy();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return true;
	}

	public boolean clear(final int id, final int x, final int y) {
		int delay = 5;
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null || id != RAKE && id != SPADE) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		if (patch.getStage().getMessage() == null) {
			if (patch.getStage().getStage() == patch.getStage().getMaxStage()) {
				return true;
			}
		}
		if (id == RAKE) {
			if (!this.client.getItems().playerHasItem(RAKE, 1)) {
				this.client.sendMessage("You need a rake to do this.");
				return true;
			} else {
				this.client.setQueuedAnimation(RAKING);
			}
		} else if (id == SPADE) {
			if (!this.client.getItems().playerHasItem(SPADE, 1)) {
				this.client.sendMessage("You need a spade to do this.");
				return true;
			} else {
				this.client.setQueuedAnimation(SPADE_EMOTE);
				delay = 3;
			}
		}
		this.client.startAnimation(this.client.getQueuedAnimation());
		this.client.toggleBusy();
		final Task task = new Task(delay) {

			@Override
			public void execute() {
				if (Farming.this.client == null
						|| Farming.this.client.disconnected) {
					this.stop();
					return;
				}
				Farming.this.client.startAnimation(
						Farming.this.client.getQueuedAnimation());
				if (patch.getStage().getStage() == patch.getStage()
						.getMaxStage()) {
					this.stop();
				} else {
					patch.setStage(patch.getStage().next());
					patch.setPlantedTime(System.currentTimeMillis());
					Farming.this.client.getItems().addItem(WEED, 1);
				}
				Farming.this.client.getPA().addSkillXP(
						FarmingManager.CLEARING_PATCH_EXPERIENCE,
						RSConstants.FARMING);
				Farming.this.update();
			}

			@Override
			public void stop() {
				super.stop();
				Farming.this.client.toggleBusy();
				Farming.this.client.setQueuedAnimation(-1);
				if (Farming.this.client != null
						&& Farming.this.client.isActive) {
					Farming.this.client.sendMessage("You clear the patch.");
				}
				Farming.prepare(patch);
			}

		};
		Server.getTaskScheduler().schedule(task);
		return true;
	}

	public abstract boolean cure(int id, int x, int y);

	public boolean inspect(final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		switch (patch.getState()) {
			case DISEASED:
				if (plot.ordinal() < 4) {
					this.client.sendMessage(
							"This plant is diseased. Use a plant cure on it to cure it or clear the patch with a spade.");
				} else if (plot.ordinal() > 3 && plot.ordinal() < 8) {
					this.client.sendMessage(
							"This tree is diseased. Use secateurs to prune the area or clear the patch with a spade.");
				}
				return true;
			case DEAD:
				if (plot.ordinal() < 4) {
					this.client.sendMessage(
							"This plant is dead. You did not cure it while it was diseased. Clear the patch with a spade.");
				} else if (plot.ordinal() > 3 && plot.ordinal() < 8) {
					this.client.sendMessage(
							"This tre is dead. You did not cure it while it was diseased. Clear the patch with a spade.");
				}
				return true;
			case STUMP:
				this.client.sendMessage(
						"This is a tree stump. To remove it, use a spade on it to receive some roots and clear the patch.");
				return true;
			default:
				break;
		}
		if (patch.getStage().getMessage() == null) {
			if (patch.getStage().getStage() < patch.getStage().getMaxStage()) {
				if (plot.ordinal() < 4) {
					this.client.sendMessage(
							"This is an herb patch. The soil has not been treated. The patch needs weeding.");
				} else if (plot.ordinal() > 3 && plot.ordinal() < 8) {
					this.client.sendMessage(
							"This is a tree patch. The soil has not been treated. The patch needs weeding.");
				}
			} else {
				if (patch.getState() == State.COMPOST) {
					if (plot.ordinal() < 4) {
						this.client.sendMessage(
								"This is an herb patch. The soil has been treated. The patch is empty and weeded.");
					} else if (plot.ordinal() > 3 && plot.ordinal() < 8) {
						this.client.sendMessage(
								"This is a tree patch. The soil has been treated. The patch is empty and weeded.");
					}
				} else {
					if (plot.ordinal() < 4) {
						this.client.sendMessage(
								"This is an herb patch. The soil has not been treated. The patch is empty and weeded.");
					} else if (plot.ordinal() > 3 && plot.ordinal() < 8) {
						this.client.sendMessage(
								"This is a tree patch. The soil has not been treated. The patch is empty and weeded.");
					}
				}
			}
		} else {
			this.client.sendMessage(
					"You bend down and start to inspect the plant...");
			this.client.startAnimation(KNEEL);
			this.client.toggleBusy();
			final Task task = new Task(5) {

				@Override
				public void execute() {
					if (Farming.this.client != null
							&& Farming.this.client.isActive) {
						Farming.this.client
								.sendMessage(patch.getStage().getMessage());
					}
					this.stop();
				}

				@Override
				public void stop() {
					super.stop();
					Farming.this.client.toggleBusy();
					Farming.this.client.startAnimation(STAND_UP);
				}

			};
			Server.getTaskScheduler().schedule(task);
		}
		return true;
	}

	public abstract boolean plant(int id, int x, int y);

	public abstract void update();
}
