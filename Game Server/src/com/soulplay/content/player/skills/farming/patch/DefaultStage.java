package com.soulplay.content.player.skills.farming.patch;

public enum DefaultStage implements PatchStage {

	STAGE_ONE,

	STAGE_TWO,

	STAGE_THREE,

	STAGE_FOUR;

	public static final DefaultStage[] values = DefaultStage.values();

	@Override
	public DefaultStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return DefaultStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return null;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public DefaultStage next() {
		final int index = this.ordinal() + 1;
		if (index > DefaultStage.values.length - 1) {
			return this;
		}
		return DefaultStage.values[index];
	}

	@Override
	public DefaultStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return DefaultStage.values[index];
	}
}
