package com.soulplay.content.player.skills.farming.tree.stage;

import com.soulplay.content.player.skills.farming.patch.PatchStage;

public enum MagicTreeStage implements PatchStage {

	STAGE_ONE("The magic sapling has only just been planted."),

	STAGE_TWO("The magic sapling grows a little bit."),

	STAGE_THREE("The magic sapling grows a little bit more."),

	STAGE_FOUR("The magic sapling grows a few small branches."),

	STAGE_FIVE("The magic tree grows a small canopy."),

	STAGE_SIX(
			"The magic tree canopy becomes larger and starts producing sparkles."),

	STAGE_SEVEN("The magic tree grows and the base becomes lighter."),

	STAGE_EIGHT("The magic tree grows and the base becomes darker."),

	STAGE_NINE(
			"The magic tree's bark is more prominent and the canopy gains more sparkles."),

	STAGE_TEN("The magic tree grows taller."),

	STAGE_ELEVEN("The magic tree grows taller."),

	STAGE_TWELVE("The magic tree is ready to be harvested.");
	
	public static final MagicTreeStage[] values = MagicTreeStage.values();

	private String message;

	private MagicTreeStage(final String message) {
		this.message = message;
	}

	@Override
	public MagicTreeStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return MagicTreeStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public MagicTreeStage next() {
		final int index = this.ordinal() + 1;
		if (index > MagicTreeStage.values.length - 1) {
			return this;
		}
		return MagicTreeStage.values[index];
	}

	@Override
	public MagicTreeStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return MagicTreeStage.values[index];
	}
}
