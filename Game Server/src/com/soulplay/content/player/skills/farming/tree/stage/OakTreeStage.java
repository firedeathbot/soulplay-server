package com.soulplay.content.player.skills.farming.tree.stage;

import com.soulplay.content.player.skills.farming.patch.PatchStage;

public enum OakTreeStage implements PatchStage {

	STAGE_ONE("The acorn sapling has just been planted."),

	STAGE_TWO("The acorn sapling grows larger"),

	STAGE_THREE("The oak tree produces a small canopy."),

	STAGE_FOUR("The oak tree grows larger."),

	STAGE_FIVE("The oak tree is ready to harvest.");
	
	public static final OakTreeStage[] values = OakTreeStage.values();

	private String message;

	private OakTreeStage(final String message) {
		this.message = message;
	}

	@Override
	public OakTreeStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return OakTreeStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public OakTreeStage next() {
		final int index = this.ordinal() + 1;
		if (index > OakTreeStage.values.length - 1) {
			return this;
		}
		return OakTreeStage.values[index];
	}

	@Override
	public OakTreeStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return OakTreeStage.values[index];
	}
}
