package com.soulplay.content.player.skills.farming.patch;

public enum State {

	GROWING,

	DISEASED,

	DEAD,

	COMPOST,

	CHECK_HEALTH,

	CHOP_DOWN,

	STUMP;

}
