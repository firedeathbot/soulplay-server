package com.soulplay.content.player.skills.farming.tree;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Woodcutting;
import com.soulplay.content.player.skills.farming.Farming;
import com.soulplay.content.player.skills.farming.FarmingManager;
import com.soulplay.content.player.skills.farming.patch.DefaultStage;
import com.soulplay.content.player.skills.farming.patch.Patch;
import com.soulplay.content.player.skills.farming.patch.PatchLocation;
import com.soulplay.content.player.skills.farming.patch.State;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;

public class TreeFarming extends Farming {

	public TreeFarming(final Client client) {
		super(client);
		for (int i = 4; i < 8; i++) {
			final Patch patch = new Patch();
			patch.setStage(DefaultStage.STAGE_ONE);
			client.setPatch(i, patch);
		}
	}

	public boolean checkHealth(final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		final Tree tree = Tree.getTree(patch.getSeed());
		if (tree == null) {
			return false;
		}
		if (patch.getState() != State.CHECK_HEALTH) {
			return false;
		}
		this.client.startAnimation(Farming.CHECK_HEALTH);
		this.client.toggleBusy();
		final Task task = new Task(2) {

			@Override
			public void execute() {
				if (TreeFarming.this.client != null
						&& TreeFarming.this.client.isActive) {
					TreeFarming.this.client.sendMessage(
							"You examine the tree for signs of disease and find that it is in perfect health.");
					TreeFarming.this.client.getPA().addSkillXP(
							(int) tree.getCheckHealthExperience(),
							RSConstants.FARMING);
				}
				patch.setState(State.CHOP_DOWN);
				this.stop();
			}

			@Override
			public void stop() {
				super.stop();
				TreeFarming.this.client.toggleBusy();
				TreeFarming.this.update();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return true;
	}

	public boolean chopDown(final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		final Tree tree = Tree.getTree(patch.getSeed());
		if (tree == null) {
			return false;
		}
		if (patch.getState() != State.CHOP_DOWN) {
			return false;
		}

		Woodcutting.initFarmingWc(this.client, tree.getTreeObjectId(), x, y, patch);
		return true;
	}

	@Override
	public boolean clear(final int id, final int x, final int y) {
		if (super.clear(id, x, y) && this.client.isBusy()) {
			final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
			if (plot == null) {
				return false;
			}
			final Patch patch = this.client.getPatch(plot.ordinal());
			if (patch == null) {
				return false;
			}
			final Tree tree = Tree.getTree(patch.getSeed());
			if (patch.getState() == State.STUMP
					|| patch.getState() == State.CHOP_DOWN) {
				final Task task = new Task(3) {

					@Override
					public void execute() {
						if (TreeFarming.this.client != null
								&& TreeFarming.this.client.isActive) {
							TreeFarming.this.client.getItems()
									.addItem(tree.getRoots(), 1);
						}
						this.stop();
					}

				};
				Server.getTaskScheduler().schedule(task);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean cure(final int id, final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null || plot.ordinal() < 4 || plot.ordinal() > 7
				|| id != Farming.SECATEURS && id != Farming.MAGIC_SECATEURS) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		if (patch.getState() != State.DISEASED) {
			this.client.sendMessage("This area doesn't need to be pruned.");
			return true;
		}
		this.client.toggleBusy();
		this.client.startAnimation(Farming.PRUNING);
		patch.setState(State.GROWING);
		final Task task = new Task(15) {

			@Override
			public void execute() {
				if (TreeFarming.this.client != null
						&& TreeFarming.this.client.isActive) {
					TreeFarming.this.client.sendMessage(
							"You prune the area with your secateurs.");
				}
				this.stop();
			}

			@Override
			public void stop() {
				super.stop();
				TreeFarming.this.client.toggleBusy();
				TreeFarming.this.update();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return true;
	}

	@Override
	public boolean plant(final int id, final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		final Tree tree = Tree.getTree(id);
		if (plot == null || tree == null || plot.ordinal() < 4
				|| plot.ordinal() > 7) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		if (patch.getStage().getStage() != 3
				|| patch.getStage().getMessage() != null) {
			this.client.sendMessage("You can't plant a sapling here.");
			return true;
		}
		if (tree.getRequiredLevel() > this.client.getPlayerLevel()[RSConstants.FARMING]) {
			this.client.sendMessage("You need a farming level of "
					+ tree.getRequiredLevel() + " to plant this sapling.");
			return true;
		}
		if (!this.client.getItems().playerHasItem(Farming.TROWEL, 1)) {
			this.client
					.sendMessage("You need a trowel to plant a sapling here.");
			return true;
		}
		this.client.startAnimation(Farming.PLANTING_POT);
		this.client.getItems().deleteItemInOneSlot(tree.getSapling(), 1);
		this.client.toggleBusy();
		final Task task = new Task(3) {

			@Override
			public void execute() {
				patch.setStage(tree.getStage());
				patch.setState(State.GROWING);
				patch.setSeed(tree.getSapling());
				patch.setPlantedTime(System.currentTimeMillis());
				if (TreeFarming.this.client != null
						&& TreeFarming.this.client.isActive) {
					TreeFarming.this.client.getPA().addSkillXP(
							(int) tree.getPlantingExperience(),
							RSConstants.FARMING);
				}
				this.stop();
			}

			@Override
			public void stop() {
				super.stop();
				TreeFarming.this.update();
				TreeFarming.this.client.toggleBusy();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return false;
	}

	@Override
	public void update() {
		for (int i = 4; i < 8; i++) {
			final Patch patch = this.client.getPatch(i);
			if (patch == null) {
				continue;
			}
			if (Farming.calculatePatchStage(patch,
					FarmingManager.TREE_STAGE_TIME)) {
				if (patch.getSeed() > 0) {
					final Tree tree = Tree.getTree(patch.getSeed());
					if (tree == null) {
						continue;
					}
					Farming.calculatePatchState(patch, tree.getDiseaseChance());
				}
			}
		}
		int config = 0;
		for (int i = 4; i < 8; i++) {
			final Patch patch = this.client.getPatch(i);
			if (patch == null) {
				continue;
			}
			final Tree tree = Tree.getTree(patch.getSeed());
			config += Farming.calculateConfigValue(patch,
					tree == null ? -1 : tree.getStart()) << 8 * (i - 4);
		}
		if (this.client.isActive) {
			this.client.getPacketSender().sendConfig(FarmingManager.TREE_CONFIG,
					config);
		}
	}
}
