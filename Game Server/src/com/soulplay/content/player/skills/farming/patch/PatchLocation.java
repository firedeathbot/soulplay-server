package com.soulplay.content.player.skills.farming.patch;

public enum PatchLocation {

	FALADOR_HERB(new int[]{3058, 3312}, new int[]{3059, 3311}),

	CATHERBY_HERB(new int[]{2813, 3464}, new int[]{2814, 3463}),

	ARDOUGNE_HERB(new int[]{2670, 3375}, new int[]{2671, 3374}),

	PHASMATYS_HERB(new int[]{3605, 3530}, new int[]{3606, 3529}),

	TAVERLY_TREE(new int[]{2935, 3439}, new int[]{2937, 3437}),

	FALADOR_TREE(new int[]{3003, 3374}, new int[]{3005, 3372}),

	VARROCK_TREE(new int[]{3228, 3460}, new int[]{3230, 3458}),

	LUMBRIDGE_TREE(new int[]{3192, 3232}, new int[]{3194, 3230}),
	SOUTH_FALADOR_ALLOTMENT_ZERO(new int[]{3055, 3303}, new int[]{3059, 3308}),
	SOUTH_FALADOR_ALLOTMENT_ONE(new int[]{3050, 3307}, new int[]{3054, 3312}),
	PHASMATYS_ALLOTMENT_ZERO(new int[]{3597, 3525}, new int[]{3601, 3530}),
	PHASMATYS_ALLOTMENT_ONE(new int[]{3602, 3521}, new int[]{3606, 3526}),
	CATHEBRY_ALLOTMENT_ZERO(new int[]{2805, 3461}, new int[]{2814, 3459}),
	CATHEBRY_ALLOTMENT_ONE(new int[]{2805, 3466}, new int[]{2814, 3468}),
	ARDOUNGE_ALLOTMENT_ZERO(new int[]{2662, 3370}, new int[]{2671, 3372}),
	ARDOUNGE_ALLOTMENT_ONE(new int[]{2662, 3377}, new int[]{2671, 3379});
	
	public static final PatchLocation[] values = PatchLocation.values();

	public static PatchLocation getPatchLocation(final int x, final int y) {
		for (final PatchLocation patch : PatchLocation.values) {
			if ((x >= patch.northwest[0] && x <= patch.southeast[0])
					&& (y >= patch.southeast[1] && y <= patch.northwest[1])) {
				return patch;
			}
		}
		return null;
	}

	private int[] northwest, southeast;

	private PatchLocation(final int[] northwest, final int[] southeast) {
		this.northwest = northwest;
		this.southeast = southeast;
	}
}
