package com.soulplay.content.player.skills.farming.tree.stage;

import com.soulplay.content.player.skills.farming.patch.PatchStage;

public enum WillowTreeStage implements PatchStage {

	STAGE_ONE("The willow sapling has only just been planted."),

	STAGE_TWO("The willow sapling grows a few small branches"),

	STAGE_THREE("The willow tree develops a small canopy."),

	STAGE_FOUR(
			"The willow tree trunk becomes dark brown and the canopy grows."),

	STAGE_FIVE(
			"The willow tree trunk becomes a lighter shade of brown, and the canopy grows more"),

	STAGE_SIX("The trunk thickens and the canopy grows yet larger."),

	STAGE_SEVEN("The willow tree is fully grown.");
	
	public static final WillowTreeStage[] values = WillowTreeStage.values();

	private String message;

	private WillowTreeStage(final String message) {
		this.message = message;
	}

	@Override
	public WillowTreeStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return WillowTreeStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public WillowTreeStage next() {
		final int index = this.ordinal() + 1;
		if (index > WillowTreeStage.values.length - 1) {
			return this;
		}
		return WillowTreeStage.values[index];
	}

	@Override
	public WillowTreeStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return WillowTreeStage.values[index];
	}
}
