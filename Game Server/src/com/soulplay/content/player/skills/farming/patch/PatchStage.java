package com.soulplay.content.player.skills.farming.patch;

public interface PatchStage {

	public PatchStage current();

	public int getMaxStage();

	public String getMessage();

	public int getStage();

	public PatchStage next();

	public PatchStage previous();

}
