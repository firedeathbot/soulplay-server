package com.soulplay.content.player.skills.farming.herb;

import java.util.HashMap;

public enum Herb {

	GUAM(5291, 199, 1, 25, 11, 11.5, 0x04),

	MARRENTILL(5292, 201, 14, 25, 13.5, 15, 0x0B),

	TARROMIN(5293, 203, 19, 25, 16, 18, 0x12),

	HARRALANDER(5294, 205, 26, 25, 21.5, 24, 0x19),

	GOUT_TUBER(6311, 3261, 29, 25, 105, 45, 0xC0),

	RANARR(5295, 207, 32, 20, 27, 30.5, 0x20),

	TOADFLAX(5296, 3049, 38, 20, 34, 38.5, 0x27),

	IRIT(5297, 209, 44, 20, 43, 48.5, 0x2E),

	AVANTOE(5298, 211, 50, 20, 54.5, 61.5, 0x35),

	KUARM(5299, 213, 56, 20, 69, 78, 0x44),

	SNAPDRAGON(5300, 3051, 62, 15, 87.5, 98.5, 0x4B),

	CADANTINE(5301, 215, 67, 15, 106.5, 120, 0x52),

	LANTADYME(5302, 2485, 73, 15, 134.5, 151.5, 0x59),

	DWARF(5303, 217, 79, 15, 170.5, 192, 0x60),

	TORSOL(5304, 219, 85, 15, 199.5, 224.5, 0x67);

	private static HashMap<Integer, Herb> herbs = new HashMap<>();

	static {
		for (final Herb herb : Herb.values()) {
			Herb.herbs.put(herb.seed, herb);
		}
	}

    public static void load() {
    	/* empty */
    }
   
	public static Herb getHerb(final int seed) {
		return Herb.herbs.get(seed);
	}

	private int seed, harvest;

	private int required, diseaseChance;

	private double plantingExperience, harvestingExperience;

	private int start;

	private Herb(final int seed, final int harvest, final int required,
			final int diseaseChance, final double plantingExperience,
			final double harvestingExperience, final int start) {
		this.seed = seed;
		this.harvest = harvest;
		this.required = required;
		this.diseaseChance = diseaseChance;
		this.plantingExperience = plantingExperience;
		this.harvestingExperience = harvestingExperience;
		this.start = start;
	}

	public int getDiseaseChance() {
		return this.diseaseChance;
	}

	public int getHarvest() {
		return this.harvest;
	}

	public double getHarvestingExperience() {
		return this.harvestingExperience;
	}

	public double getPlantingExperience() {
		return this.plantingExperience;
	}

	public int getRequiredLevel() {
		return this.required;
	}

	public int getSeed() {
		return this.seed;
	}

	public int getStart() {
		return this.start;
	}
}
