package com.soulplay.content.player.skills.farming.herb;

import com.soulplay.content.player.skills.farming.patch.PatchStage;

public enum HerbStage implements PatchStage {

	STAGE_ONE("The seed had only just been planted."),

	STAGE_TWO("The plant is now ankle height."),

	STAGE_THREE("The herb is now knee height."),

	STAGE_FOUR("The herb is now mid-thigh height."),

	STAGE_FIVE("The herb is fully grown and ready to harvet.");
	
	public static final HerbStage[] values = HerbStage.values();

	private String message;

	private HerbStage(final String message) {
		this.message = message;
	}

	@Override
	public HerbStage current() {
		return this;
	}

	@Override
	public int getMaxStage() {
		return HerbStage.values.length - 1;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public int getStage() {
		return this.ordinal();
	}

	@Override
	public HerbStage next() {
		final int index = this.ordinal() + 1;
		if (index > HerbStage.values.length - 1) {
			return this;
		}
		return HerbStage.values[index];
	}

	@Override
	public HerbStage previous() {
		final int index = this.ordinal() - 1;
		if (index < 0) {
			return this;
		}
		return HerbStage.values[index];
	}
}
