package com.soulplay.content.player.skills.farming.tree;

import java.util.HashMap;

import com.soulplay.content.player.skills.farming.patch.PatchStage;
import com.soulplay.content.player.skills.farming.tree.stage.MagicTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.MapleTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.OakTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.WillowTreeStage;
import com.soulplay.content.player.skills.farming.tree.stage.YewTreeStage;

public enum Tree {

	OAK(5370, 6043, 15, new int[]{5968, 1}, 20, 14, 467.3, 0x08, 12, 1281,
			OakTreeStage.STAGE_ONE),

	WILLOW(5371, 6045, 30, new int[]{5386, 1}, 20, 25, 1456.3, 0x0F, 21, 1308,
			WillowTreeStage.STAGE_ONE),

	MAPLE(5372, 6047, 45, new int[]{5396, 1}, 25, 45, 3403.4, 0x18, 32, 1307,
			MapleTreeStage.STAGE_ONE),

	YEW(5373, 6049, 60, new int[]{6016, 10}, 25, 81, 7069.9, 0x23, 45, 1309,
			YewTreeStage.STAGE_ONE),

	MAGIC(5374, 6051, 75, new int[]{5976, 25}, 25, 145.5, 13768.3, 0x30, 60,
			1306, MagicTreeStage.STAGE_ONE);

	private static HashMap<Integer, Tree> trees = new HashMap<>();

	static {
		for (final Tree tree : Tree.values()) {
			Tree.trees.put(tree.sapling, tree);
		}
	}

    public static void load() {
    	/* empty */
    }
    
	public static Tree getTree(final int sapling) {
		return Tree.trees.get(sapling);
	}

	private int sapling, roots;

	private int required;

	private int[] payment;

	private int diseaseChance;

	private double plantingExperience, checkHealthExperience;

	private int start, check, tree;

	private PatchStage stage;

	private Tree(final int sapling, final int roots, final int required,
			final int[] payment, final int diseaseChance,
			final double plantingExperience, final double checkHealthExperience,
			final int start, final int check, final int tree,
			final PatchStage stage) {
		this.sapling = sapling;
		this.roots = roots;
		this.required = required;
		this.payment = payment;
		this.diseaseChance = diseaseChance;
		this.plantingExperience = plantingExperience;
		this.checkHealthExperience = checkHealthExperience;
		this.start = start;
		this.check = check;
		this.tree = tree;
		this.stage = stage;
	}

	public int getCheck() {
		return this.check;
	}

	public double getCheckHealthExperience() {
		return this.checkHealthExperience;
	}

	public int getDiseaseChance() {
		return this.diseaseChance;
	}

	public int[] getPayment() {
		return this.payment;
	}

	public double getPlantingExperience() {
		return this.plantingExperience;
	}

	public int getRequiredLevel() {
		return this.required;
	}

	public int getRoots() {
		return this.roots;
	}

	public int getSapling() {
		return this.sapling;
	}

	public PatchStage getStage() {
		return this.stage;
	}

	public int getStart() {
		return this.start;
	}

	public int getTreeObjectId() {
		return this.tree;
	}
}
