package com.soulplay.content.player.skills.farming.patch;

import java.util.concurrent.TimeUnit;

import com.soulplay.content.player.skills.farming.FarmingManager;

public class Patch {

	private PatchStage stage = DefaultStage.STAGE_ONE;

	private State state = State.GROWING;

	private int seed = 0;

	private long plantedTime = 0;

	private double diseaseChance = 1.0;

	private int harvest = 0;

	public Patch() {

	}

	public boolean decrementHarvest() {
		return --this.harvest == 0;
	}

	public double getDiseaseChance() {
		return this.diseaseChance;
	}

	public int getHarvest() {
		return this.harvest;
	}

	public int getSeed() {
		return this.seed;
	}

	public PatchStage getStage() {
		return this.stage;
	}

	public State getState() {
		return this.state;
	}

	public boolean reachedNextStage(final int time, final int stage) {
		return TimeUnit.MILLISECONDS
				.toMinutes(System.currentTimeMillis() - this.plantedTime) > time
						/ FarmingManager.GROWING_SCALE * stage;
	}

	public void setDiseaseChance(final double chance) {
		this.diseaseChance = chance;
	}

	public void setHarvest(final int harvest) {
		this.harvest = harvest;
	}

	public void setPlantedTime(final long time) {
		this.plantedTime = time;
	}

	public void setSeed(final int seed) {
		this.seed = seed;
	}

	public void setStage(final PatchStage stage) {
		this.stage = stage;
	}

	public void setState(final State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Patch [stage=" + this.stage + ", state=" + this.state
				+ ", seed=" + this.seed + ", plantedTime=" + this.plantedTime
				+ ", diseaseChance=" + this.diseaseChance + ", harvest="
				+ this.harvest + "]";
	}
}
