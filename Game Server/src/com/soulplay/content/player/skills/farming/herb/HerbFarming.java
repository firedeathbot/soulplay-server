package com.soulplay.content.player.skills.farming.herb;

import java.util.Random;

import com.soulplay.Server;
import com.soulplay.content.player.skills.farming.Farming;
import com.soulplay.content.player.skills.farming.FarmingManager;
import com.soulplay.content.player.skills.farming.patch.DefaultStage;
import com.soulplay.content.player.skills.farming.patch.Patch;
import com.soulplay.content.player.skills.farming.patch.PatchLocation;
import com.soulplay.content.player.skills.farming.patch.State;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;

public class HerbFarming extends Farming {

	private static int calculateHarvest(final Client client,
			final Patch patch) {
		int amount = 3 + new Random().nextInt(15);
		if (patch.getState() == State.COMPOST) {
			amount *= 1.33;
		}
		if (client.getItems().playerHasItem(Farming.MAGIC_SECATEURS, 1)) {
			amount *= 1.10;
		}
		return amount;
	}

	public HerbFarming(final Client client) {
		super(client);
		for (int i = 0; i < 4; i++) {
			final Patch patch = new Patch();
			patch.setStage(DefaultStage.STAGE_ONE);
			client.setPatch(i, patch);
		}
	}

	@Override
	public boolean cure(final int id, final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null || plot.ordinal() > 3 || id != Farming.PLANT_CURE) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		if (patch.getState() != State.DISEASED) {
			this.client.sendMessage("This plant doesn't need to be cured.");
			return true;
		}
		this.client.getItems().removeItem(Farming.PLANT_CURE, 1);
		this.client.getItems().addItem(Farming.VIAL, 1);
		this.client.startAnimation(Farming.CURING);
		this.client.toggleBusy();
		final Task task = new Task(5) {

			@Override
			public void execute() {
				if (HerbFarming.this.client != null
						&& HerbFarming.this.client.isActive) {
					HerbFarming.this.client.sendMessage(
							"You cure the plant with a plant cure.");
				}
				patch.setState(State.GROWING);
				HerbFarming.this.client.toggleBusy();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return false;
	}

	public boolean harvest(final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		if (plot == null || plot.ordinal() > 3) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		final Herb herb = Herb.getHerb(patch.getSeed());
		if (herb == null) {
			return false;
		}
		if (this.client.getPlayerFarmingState()) {
			return false;
		}
		this.client.setPlayerFarmingState(true);
		this.client.toggleBusy();
		this.client.startAnimation(Farming.PICKING_HERB);
		if (patch.getHarvest() == 0) {
			patch.setHarvest(HerbFarming.calculateHarvest(this.client, patch));
		}
		final Task task = new Task(3) {

			@Override
			public void execute() {
				if (!HerbFarming.this.client.getPlayerFarmingState()
						|| !HerbFarming.this.client.isActive || herb == null) {
					this.stop();
					return;
				}
				patch.decrementHarvest();
				HerbFarming.this.client.startAnimation(Farming.PICKING_HERB);
				HerbFarming.this.client.sendMessageSpam(
						"You harvest the crop and get some herbs.");
				HerbFarming.this.client.getItems().addItem(herb.getHarvest(),
						1);
				HerbFarming.this.client.getPA().addSkillXP(
						(int) herb.getHarvestingExperience(),
						RSConstants.FARMING);
				HerbFarming.this.client.getAchievement().farmHerb();
				if (patch.getHarvest() == 0) {
					Farming.reset(patch);
					HerbFarming.this.client.setPlayerFarmingState(false);
					this.stop();
					return;
				}
				if (HerbFarming.this.client.getItems().freeSlots() == 0) {
					HerbFarming.this.client.setPlayerFarmingState(false);
					this.stop();
					return;
				}
			}

			@Override
			public void stop() {
				if (super.isRunning()) {
					super.stop();
				}
				HerbFarming.this.update();
				HerbFarming.this.client.toggleBusy();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return true;
	}

	@Override
	public boolean plant(final int id, final int x, final int y) {
		final PatchLocation plot = PatchLocation.getPatchLocation(x, y);
		final Herb herb = Herb.getHerb(id);
		if (plot == null || herb == null || plot.ordinal() > 3) {
			return false;
		}
		final Patch patch = this.client.getPatch(plot.ordinal());
		if (patch == null) {
			return false;
		}
		if (patch.getStage().getStage() != 3
				|| patch.getStage().getMessage() != null) {
			this.client.sendMessage("You cannot plant a seed here.");
			return true;
		}
		if (herb.getRequiredLevel() > this.client.getPlayerLevel()[RSConstants.FARMING]) {
			this.client.sendMessage("You need a farming level of "
					+ herb.getRequiredLevel() + " to plant this seed.");
			return true;
		}
		if (!this.client.getItems().playerHasItem(Farming.SEED_DIPPER, 1)) {
			this.client.sendMessage(
					"You need a seed dibber to plant a seed here.");
			return true;
		}
		this.client.startAnimation(Farming.SEED_DIBBING_EMOTE);
		this.client.getItems().deleteItemInOneSlot(herb.getSeed(), 1);
		this.client.toggleBusy();
		final Task task = new Task(5) {

			@Override
			public void execute() {
				patch.setState(State.GROWING);
				patch.setStage(HerbStage.STAGE_ONE);
				patch.setSeed(herb.getSeed());
				patch.setPlantedTime(System.currentTimeMillis());
				if (HerbFarming.this.client != null
						&& HerbFarming.this.client.isActive) {
					HerbFarming.this.client.getPA().addSkillXP(
							(int) herb.getPlantingExperience(),
							RSConstants.FARMING);
					HerbFarming.this.client.toggleBusy();
				}
				HerbFarming.this.update();
				this.stop();
			}

		};
		Server.getTaskScheduler().schedule(task);
		return true;
	}

	@Override
	public void update() // TODO:: this spams the client so fix the sendconfig
							// shit to only send when needed. Will do when i
							// will store farming on server
	{
		for (int i = 0; i < 4; i++) {
			final Patch patch = this.client.getPatch(i);
			if (patch == null) {
				continue;
			}
			if (Farming.calculatePatchStage(patch,
					FarmingManager.HERB_STAGE_TIME)) {
				if (patch.getSeed() > 0) {
					final Herb herb = Herb.getHerb(patch.getSeed());
					if (herb == null) {
						continue;
					}
					Farming.calculatePatchState(patch, herb.getDiseaseChance());
				}
			}
		}
		int config = 0;
		for (int i = 0; i < 4; i++) {
			final Patch patch = this.client.getPatch(i);
			if (patch == null) {
				continue;
			}
			final Herb herb = Herb.getHerb(patch.getSeed());
			config += Farming.calculateConfigValue(patch,
					herb == null ? -1 : herb.getStart()) << 8 * i;
		}
		if (this.client.isActive) {
			this.client.getPacketSender().sendConfig(FarmingManager.HERB_CONFIG,
					config);
		}
	}
}
