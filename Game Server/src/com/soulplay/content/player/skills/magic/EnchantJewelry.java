package com.soulplay.content.player.skills.magic;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.achievement.Achievs;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;

public final class EnchantJewelry {

	public enum Enchant {

		SAPPHIRE_RING(1637, 2550, SpellsData.ENCHANT_SAPPHIRE),
		SAPPHIRE_AMULET(1694, 1727, SpellsData.ENCHANT_SAPPHIRE),
		SAPPHIRE_NECKLACE(1656, 3853, SpellsData.ENCHANT_SAPPHIRE),

		EMERALD_RING(1639, 2552, SpellsData.ENCHANT_EMERALD),
		EMERALD_AMULET(1696, 1729, SpellsData.ENCHANT_EMERALD),
		EMERALD_NECKLACE(1658, 5521, SpellsData.ENCHANT_EMERALD),

		RUBY_RING(1641, 2568, SpellsData.ENCHANT_RUBY),
		RUBY_AMULET(1698, 1725, SpellsData.ENCHANT_RUBY),
		RUBY_NECKLACE(1660, 11194, SpellsData.ENCHANT_RUBY),

		DIAMOND_RING(1643, 2570, SpellsData.ENCHANT_DIAMOND),
		DIAMOND_AMULET(1700, 1731, SpellsData.ENCHANT_DIAMOND),
		DIAMOND_NECKLACE(1662, 11090, SpellsData.ENCHANT_DIAMOND),

		DRAGONSTONE_RING(1645, 2572, SpellsData.ENCHANT_DRAGONSTONE),
		DRAGONSTONE_AMULET(1702, 1712, SpellsData.ENCHANT_DRAGONSTONE),
		DRAGONSTONE_NECKLACE(1664, 11105, SpellsData.ENCHANT_DRAGONSTONE),

		ONYX_RING(6575, 6583, SpellsData.ENCHANT_ONYX),
		ONYX_AMULET(6581, 6585, SpellsData.ENCHANT_ONYX),
		ONYX_NECKLACE(6577, 11128, SpellsData.ENCHANT_ONYX),

		ZENYTE_NECKLACE(30200, 30206, SpellsData.ENCHANT_ZENYTE),
		ZENYTE_RING(30201, 30203, SpellsData.ENCHANT_ZENYTE),
		ZENYTE_AMULET(30202, 30208, SpellsData.ENCHANT_ZENYTE),
		ZENYTE_BRACELET(30199, 30207, SpellsData.ENCHANT_ZENYTE);

		private static final Map<Integer, Enchant> enc = new HashMap<>();

		private final int unenchanted, enchanted;
		private final SpellsData spellToEnchant;

		Enchant(int unenchanted, int enchanted, SpellsData spellToEnchant) {
			this.unenchanted = unenchanted;
			this.enchanted = enchanted;
			this.spellToEnchant = spellToEnchant;
		}

		public int getEnchanted() {
			return enchanted;
		}

		public int getUnenchanted() {
			return unenchanted;
		}

		public SpellsData getSpellToEnchant() {
			return spellToEnchant;
		}

		static {
			for (Enchant en : Enchant.values()) {
				enc.put(en.getUnenchanted(), en);
			}
		}

		public static void load() {
			/* empty */
		}

		public static Enchant forId(int itemID) {
			return enc.get(itemID);
		}
	}

	public static void enchantItem(Client c, int itemID, SpellsData spellUsed) {
		Enchant enc = Enchant.forId(itemID);

		if (enc == null) {
			c.sendMessage("Nothing interesting happens.");
			return;
		}

		int index = c.getItems().getItemSlot(enc.getUnenchanted());
		if (index == -1) {
			c.sendMessage("You don't have anything to enchant.");
			return;
		}

		if (enc.getSpellToEnchant() != spellUsed) {
			c.sendMessage("You can't enchant this item with this spell.");
			return;
		}

		if (!MagicRequirements.checkMagicReqsNew(c, spellUsed, true)) {
			return;
		}

		c.getItems().replaceItemSlot(enc.getEnchanted(), index);
		c.getPA().addSkillXP(spellUsed.getExpGain(), Skills.MAGIC);
		c.startAnimation(spellUsed.getAnim());
		c.startGraphic(spellUsed.getStartGfx());

		switch (enc) {
			case DIAMOND_NECKLACE:
				c.getAchievement().increaseProgress(Achievs.CRAFT_50_PHOENIX_NECK);
				break;
			case ONYX_AMULET:
				c.getAchievement().increaseProgress(Achievs.CRAFT_20_FURYS);
				break;
			case RUBY_AMULET:
				c.getAchievement().increaseProgress(Achievs.CRAFT_20_STR_AMULETS);
				break;
			default:
				break;
		}

		c.getPacketSender().sendFrame106(6);
	}

}
