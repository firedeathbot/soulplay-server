package com.soulplay.content.player.skills.agility.courses;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.world.Direction;

public class WildyCourse {

	public static void climbRocks(final Client c, final GameObject o) {
		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}

		if (c.getY() != 3937) {
			c.sendMessage("Make sure to stand infront of the pipe!");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX();
		final int destY = o.getY() - 3;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startRockClimb = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					container.stop();
					return;
				}
				if (c.getX() == o.getX() && c.getY() == o.getY() + 1) {
					startRockClimb = true;
				}
				if (startRockClimb) {
					setWalkAnim(c, 10580);
					c.getPA().walkTo(0, -4, true);
					startRockClimb = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().wildyCourseStatus < 4) {
						c.getAgility().wildyCourseStatus = 0;
					}
					if (c.getAgility().wildyCourseStatus >= 4) {
						c.getPA().addSkillXP(499,
								Skills.AGILITY);
						c.getAgility().wildyCourseStatus = 0;
						if (!WorldType.equalsType(WorldType.SPAWN)) {
							c.getItems().addItem(995, 40000);
						}
						int tokens = 8;
						if (c.playerRights == 11 || c.playerRights == 12) {
							tokens += 3;
						}
						c.rewardAgilityPoints(tokens);
						c.increaseProgress(TaskType.AGILITY, 168);
					}
					resetAgility(c);
				}
			}
		}, 1);

	}

	public static void leaveWildyCourse(final Client c, final int x, final int y) {
		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}
		if (c.getX() != 2998) {
			c.sendMessage("Make sure to stand infront of the door!");
			return;
		}
		if (c.getY() != 3931) {
			c.sendMessage("Make sure to stand infront of the door!");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;

		c.getPA().changeObjectForNearby(-1, 2998, 3931, c.getZ(), 0, 0); // open
																			// double
																			// gate
		c.getPA().changeObjectForNearby(2308, 2998, 3930, c.getZ(), 2, 0); // open
																			// double
																			// gate

		setAgilityWalk(c);
		final int destX = x;
		final int destY = y - 15;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean leaveWildyCourse = false;

			boolean inPosition = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					c.getPA().changeObjectForNearby(/*2309*/123555, 2998, 3917, c.getZ(),
							3, 0); // close door
					container.stop();
					return;
				}
				if (c.getX() == 2998 && c.getY() == 3930) {
					c.getPA().changeObjectForNearby(/*2307*/123552, 2998, 3931, c.getZ(),
							3, 0); // close double gate
					c.getPA().changeObjectForNearby(-1, 2998, 3930, c.getZ(), 0,
							0); // close double gate
				}
				if (c.getX() == 2998 && c.getY() == 3917) {
					c.getPA().changeObjectForNearby(/*2309*/123555, 2998, 3917, c.getZ(),
							0, 0); // open door
				}
				if (c.getX() == x && !inPosition) {
					inPosition = true;
				}
				if (c.getX() == x && c.getY() == y) {
					leaveWildyCourse = true;
				}
				if (leaveWildyCourse) {
					setWalkAnim(c, 762);
					c.getPA().walkTo(0, -15, true);
					leaveWildyCourse = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					resetAgility(c);
				}
			}
		}, 1);

	}

	public static void logBalance(final Client c, final GameObject o) {
		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}

		if (c.getY() != 3945) {
			c.sendMessage("Make sure to stand infront of the log!");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX() - 7;
		final int destY = o.getY();
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startLogWalk = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					container.stop();
					return;
				}
				if (c.getX() == o.getX() + 1 && c.getY() == o.getY()) {
					startLogWalk = true;
				}
				if (startLogWalk) {
					setWalkAnim(c, 762);
					c.getPA().walkTo(-8, 0, true);
					startLogWalk = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().wildyCourseStatus == 3) {
						c.getAgility().wildyCourseStatus = 4;
					}
					c.getPA().addSkillXP(20, Skills.AGILITY);
					resetAgility(c);
				}
			}
		}, 1);

	}

	public static void obstaclePipe(final Client c, final GameObject o) {

		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}

		if (c.getX() != 3004 || c.getY() != 3937) {
			c.sendMessage("Make sure to stand infront of the pipe!");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX();
		final int destY = o.getY() + 12;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.resetFace();
				container.stop();
			}

			@Override
			public void stop() {
			}
		}, 1);

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startPipeWalk = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					container.stop();
					return;
				}
				if (c.getX() == o.getX() && c.getY() == o.getY() - 1) {
					startPipeWalk = true;
				}
				if (startPipeWalk) {
					setWalkAnim(c, 10580);
					c.noClip = true;
					c.getPA().walkTo(0, 13, true);
					startPipeWalk = false;
				}
				if (c.getY() == 3940) {
					c.getPA().movePlayer(c.getX(), 3948, c.getZ());
				} else if (c.getY() == 3948) {
					c.noClip = true;
					c.getPA().walkTo(0, 2, true);
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().wildyCourseStatus == 0) {
						c.getAgility().wildyCourseStatus = 1;
					}
					c.getPA().addSkillXP(13, Skills.AGILITY);
					resetAgility(c);
				}
			}
		}, 1);

	}

	private static void resetAgility(Client c) {
		// c.getPA().sendFrame36(173, 1);
		if (c.usedToRun) {
			c.isRunning = true;
			c.usedToRun = false;
		}
		c.noClip = false;
		c.performingAction = false;
		c.getCombat().getPlayerAnimIndexBypass();
		c.getMovement().hardResetWalkingQueue();
	}

	public static void ropeSwing(final Client c, final GameObject o) {
		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}

		if (c.getX() != 3005) {
			c.sendMessage("Make sure to stand infront of the rope swing.");
			return;
		}
		if (c.getY() != 3953) {
			c.sendMessage("Make sure to stand infront of the rope swing.");
			return;
		}

		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);

		if (c.getAgility().wildyCourseStatus == 1) {
			c.getAgility().wildyCourseStatus = 2;
		}
		c.startAnimation(AgilityAnimations.ROPE_SWING);
		c.setForceMovement(ForceMovementMask.createMask(0, 0, 1, 0, Direction.NORTH).addSecondDirection(0, 5, 2, 0));
		c.getPA().addSkillXP(20, Skills.AGILITY);
		o.animate(497);

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}

			@Override
			public void stop() {
				if (c != null) {
					resetAgility(c);
				}
			}
		}, 4);
	}

	private static void setAgilityWalk(Client c) {
		c.getMovement().hardResetWalkingQueue();
		c.noClip = true;
		if (c.isRunning) {
			c.usedToRun = true;
			c.isRunning = false;
		}
		// c.getPA().sendFrame36(173, 0);
	}

	private static void setWalkAnim(Client c, int walkAnim) {
		// c.playerWalkIndex = walkAnim;
		c.getMovement().getPlayerMovementAnimIds()[2] = (short) walkAnim;
		c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

	public static void steppingStones(final Client c, final GameObject o) {
		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}
		if (c.getY() != 3960) {
			c.sendMessage("Make sure to stand infront of the stones!");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX() - 5;
		final int destY = o.getY();
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startSteppingStones = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					container.stop();
					return;
				}
				if (c.getX() == o.getX() + 1 && c.getY() == o.getY()) {
					startSteppingStones = true;
				}
				if (startSteppingStones) {
					setWalkAnim(c, 741);
					c.getPA().walkTo(-6, 0, true);
					startSteppingStones = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().wildyCourseStatus == 2) {
						c.getAgility().wildyCourseStatus = 3;
					}
					c.getPA().addSkillXP(20, Skills.AGILITY);
					resetAgility(c);
				}
			}
		}, 1);

	}

	public static void walkToWildyCourse(final Client c, final GameObject o) {
		if (c.getFreezeTimer() > 0) {
			c.sendMessage("You are frozen.");
			return;
		}
		if (c.getX() != 2998) {
			c.sendMessage("Make sure to stand infront of the door!");
			return;
		}
		if (c.getY() != 3916) {
			c.sendMessage("Make sure to stand infront of the door!");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		c.getPA().changeObjectForNearby(/*2309*/123555, 2998, 3917, c.getZ(), 0, 0); // open
																			// door
		final int destX = o.getX();
		final int destY = o.getY() + 14;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startWildyCourse = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					c.getPA().changeObjectForNearby(/*2307*/123552, 2998, 3931, c.getZ(),
							3, 0); // close double gate
					c.getPA().changeObjectForNearby(-1, 2998, 3930, c.getZ(), 0,
							0); // close double gate
					container.stop();
					return;
				}
				if (c.getX() == o.getX() && c.getY() == o.getY() - 1) {
					startWildyCourse = true;
				}
				if (c.getX() == 2998 && c.getY() == 3917) {
					c.getPA().changeObjectForNearby(/*2309*/123555, 2998, 3917, c.getZ(),
							3, 0); // close door
				}
				if (c.getX() == 2998 && c.getY() == 3930) {
					c.getPA().changeObjectForNearby(-1, 2998, 3931, c.getZ(), 0,
							0); // open double gate
					c.getPA().changeObjectForNearby(2308, 2998, 3930, c.getZ(),
							2, 0); // open double gate
				}
				if (startWildyCourse) {
					setWalkAnim(c, 762);
					c.getPA().walkTo(0, 15, true);
					startWildyCourse = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					resetAgility(c);
				}
			}
		}, 1);

	}

}
