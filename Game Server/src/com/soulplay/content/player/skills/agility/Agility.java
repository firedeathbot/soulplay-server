
package com.soulplay.content.player.skills.agility;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.model.player.Client;

public class Agility {

	private boolean logBalance, obstacleNetUp, treeBranchUp, balanceRope,
			treeBranchDown, obstacleNetOver;

	public boolean doingAgility;

	public int barbarianCourseStatus = 0;

	public int wildyCourseStatus = 0;

	private int[] agilityObject = {2295, 2285, 2286, 2313, 2312, 2314, 2315,
			154, 4058};

	public void agilityCourse(final Client c, final int objectType,
			final GameObject gO) {
		if (doingAgility) {
			return;
		}

		switch (objectType) {
			case 2295:

				if (c.getX() != 2474 || c.getY() != 3436) {
					return;
				}

				doingAgility = true;
				c.performingAction = true;

				agilityWalk(c, 762, 0, -7);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						// c.sendMessage("x "+(startX+c.noClipX)+ " y
						// "+(startY+c.noClipY));
						if (c.getX() == 2474 && c.getY() == 3429) {
							container.stop();
						}
					}

					@Override
					public void stop() {
						if (c != null) {
							resetAgilityWalk(c);
							c.getPA().addSkillXP(8,
									Skills.AGILITY);
							logBalance = true;
							doingAgility = false;
							c.performingAction = false;
						}
					}
				}, 1);
				break;
			case 2285:
				if (c.getY() != 3426) {
					c.sendMessage(
							"Please stand in front of the net to use it.");
					return;
				}
				c.startAnimation(828);
				c.getPA().movePlayer(c.absX, 3424, 1);
				c.getPA().addSkillXP(8, Skills.AGILITY);
				obstacleNetUp = true;
				break;
			case 2313:
				c.startAnimation(828);
				c.getPA().movePlayer(2473, 3420, 2);
				c.getPA().addSkillXP(5, Skills.AGILITY);
				treeBranchUp = true;
				break;
			case 2312:
				if (c.getX() != 2477 || c.getY() != 3420) {
					return;
				}
				doingAgility = true;
				c.performingAction = true;
				c.noClipX = 6;
				agilityWalk(c, 762, c.noClipX, c.noClipY);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (c.getX() == 2483 && c.getY() == 3420) {
							container.stop();
						}
					}

					@Override
					public void stop() {
						if (c != null) {
							resetAgilityWalk(c);
							c.getPA().addSkillXP(8,
									Skills.AGILITY);
							balanceRope = true;
							doingAgility = false;
							c.performingAction = false;
						}
					}
				}, 1);
				break;
			case 2314:
			case 2315:
				c.startAnimation(828);
				c.getPA().movePlayer(c.absX, c.absY, 0);
				c.getPA().addSkillXP(5, Skills.AGILITY);
				treeBranchDown = true;
				break;
			case 2286:

				if (gO.getOrientation() == 2) {
					if (c.absY + 1 != gO.getY()) {
						// c.getPA().playerWalk(gO.getX(), gO.getY()-1);
						c.sendMessage(
								"You cannot climb the net from this side.");
						return;
					}
				}
				if (doingAgility) {
					return;
				}
				// c.sendMessage("Using object now "+doingAgility);
				doingAgility = true;
				c.performingAction = true;
				c.startAnimation(828);
				c.getPA().addSkillXP(8, Skills.AGILITY);
				c.getPA().movePlayer(c.absX, 3427, 0);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) { // much
																			// stupidity...
						if (c != null) {
							c.faceLocation(c.absX, 3426);
						}
						container.stop();
					}

					@Override
					public void stop() {
						if (c != null) {
							obstacleNetOver = true;
							doingAgility = false;
							c.performingAction = false;
						}
					}
				}, 3);
				break;
			case 154:
			case 4058:

				if ((gO.getId() == 154
						&& (c.getX() == 2484 && c.getY() == 3430))
						|| (gO.getId() == 4058
								&& (c.getX() == 2487 && c.getY() == 3430))) {

					if (c.performingAction) {
						return;
					}

					doingAgility = true;
					c.performingAction = true;

					// c.startAnimation(/*749*/10580);
					// agilityWalk(c, /*844*/10580, c.noClipX, /*c.noClipY*/2);
					agilityWalk(c, 10580, 0, 3);
					CycleEventHandler.getSingleton().addEvent(c,
							new CycleEvent() {

								@Override
								public void execute(
										CycleEventContainer container) {
									if (c.disconnected || c == null) {
										container.stop();
										return;
									}
									c.noClip = true;
									if (c.getY() == 3433) {
										c.getPA().movePlayer(c.getX(),
												c.getY() + 1, c.getZ());
									} else if (c.getY() == 3434) {
										c.getPA().walkTo(0, 3, true);
									} else if (c.getY() == 3437) {
										// c.getPA().movePlayer(c.getX(),
										// c.getY() + 5, c.heightLevel);
										doingAgility = false;
										c.performingAction = false;
										container.stop();
									}
								}

								@Override
								public void stop() {
									if (c != null) {
										// c.startAnimation(748);
										resetAgilityWalk(c);
										checkCompletedCourse(c);
									}
								}
							}, 1);

				} else {
					c.sendMessage("You must stand in front of the pipe.");
					return;
				}

				// if (gO.getFace() == 3) {
				// if (c.getY() - 2 != gO.getY()) {
				//// doingAgility = true;
				// if(c.isRunning) {
				// c.isRunning2 = true;
				// c.isRunning = false;
				// }
				// c.getPA().playerWalk(gO.getX(), gO.getY() + 2);
				//// CycleEventHandler.getSingleton().addEvent(c, new
				// CycleEvent() {
				//// @Override
				//// public void execute(CycleEventContainer container) {
				//// if (c.disconnected || c == null) {
				//// container.stop();
				//// return;
				//// }
				//// if (c.getY() - 2 == gO.getY() && c.getX() == gO.getX()) {
				//// container.stop();
				//// }
				//// }
				//// @Override
				//// public void stop() {
				//// doingAgility = false;
				//// c.getAgility().agilityCourse(c, objectType, gO);
				//// }
				//// }, 4);
				// return;
				// }
				// doingAgility = true;
				// c.performingAction = true;
				//
				// //c.startAnimation(749);
				// c.noClipY = -7;
				//// agilityWalk(c, 844, c.noClipX, c.noClipY);
				// agilityWalk(c, 10580, c.noClipX, -2);
				// CycleEventHandler.getSingleton().addEvent(c, new CycleEvent()
				// {
				// @Override
				// public void execute(CycleEventContainer container) {
				// if (c.disconnected || c == null) {
				// container.stop();
				// return;
				// }
				// if (c.absX == gO.getX() && c.absY == gO.getY() - 3) {
				//
				// c.getPA().walkTo(0, -2, true);
				// }
				// if (c.absX == gO.getX() && c.absY == gO.getY()) {
				// c.getPA().movePlayer(c.getX(), c.getY() - 3, c.heightLevel);
				// }
				//
				// if (c.absY == startY+c.noClipY) {
				// container.stop();
				// }
				// }
				// @Override
				// public void stop() {
				// //c.startAnimation(748);
				// if (c != null) {
				// doingAgility = false;
				// c.performingAction = false;
				// resetAgilityWalk(c);
				// checkCompletedCourse(c);
				// }
				//
				// }
				// }, 1);
				// }

				break;
		}
	}

	public boolean agilityObstacle(final Client c, final int objectType) {
		for (int element : agilityObject) {
			if (objectType == element) {
				return true;
			}
		}
		return false;
	}

	public void agilityWalk(final Client c, final int walkAnimation,
			final int x, final int y) {
		c.getMovement().hardResetWalkingQueue();
		if (c.isRunning) {
			c.isRunning = false;
			c.usedToRun = true;
		}
		// c.getPA().sendFrame36(173, 0);
		// c.playerWalkIndex = walkAnimation;
		c.getMovement().getPlayerMovementAnimIds()[2] = (short) walkAnimation;
		c.getUpdateFlags().add(UpdateFlag.APPEARANCE);

		c.getPA().walkTo(x, y, true);
	}

	public void checkCompletedCourse(Client c) {
		if (logBalance && obstacleNetUp && treeBranchUp && balanceRope
				&& treeBranchDown && obstacleNetOver) {
			if (!WorldType.equalsType(WorldType.SPAWN)) {
				c.getItems().addItem(995, 30000);
			}
			c.getPA().addSkillXP(39, Skills.AGILITY);
			c.sendMessage("You have completed the full gnome agility course.");
			int tokens = 3;
			if (c.playerRights == 4 || c.playerRights == 5
					|| c.playerRights == 6 || c.playerRights == 11
					|| c.playerRights == 12) {
				tokens += 1;
			}
			c.rewardAgilityPoints(tokens);
			c.getAchievement().gnomeLaps();
			c.increaseProgress(TaskType.AGILITY, 166);
		} else {
			c.getPA().addSkillXP(8, Skills.AGILITY);
		}
		logBalance = false;
		obstacleNetUp = false;
		treeBranchUp = false;
		balanceRope = false;
		treeBranchDown = false;
		obstacleNetOver = false;
		doingAgility = false;
		c.performingAction = false;
	}

	public void resetAgilityWalk(final Client c) {
		c.noClip = false;
		c.noClipX = c.noClipY = 0;
		if (c.usedToRun) {
			c.isRunning = true;
			c.usedToRun = false;
		}
		// c.getPA().sendFrame36(173, 1);
		// c.playerWalkIndex = 0x333;
		c.getCombat().getPlayerAnimIndexBypass();
		doingAgility = false;
		c.performingAction = false;
		c.getMovement().resetWalkingQueue();
	}
}
