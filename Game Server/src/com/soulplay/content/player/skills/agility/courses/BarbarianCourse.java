package com.soulplay.content.player.skills.agility.courses;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;

public class BarbarianCourse {

	public static void balanceLedge(final Client c, final GameObject o) {
		if (c.getY() != o.getY()) {
			c.sendMessage(
					"Please position yourself first before using this obstacle.");
			return;
		}
		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX() - 3;
		final int destY = o.getY();
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startLedgeWalk = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY - 1) {
					container.stop();
					return;
				}
				if (c.getX() == destX && c.getY() == destY) {
					c.startAnimation(759);
					c.getPA().walkTo(0, -1, true);
					// container.stop();
					// return;
				}
				if (c.getX() == o.getX() + 1 && c.getY() == o.getY()
						&& !startLedgeWalk) {
					startLedgeWalk = true;
					c.startAnimation(753);
					return;
				}
				if (startLedgeWalk) {
					setWalkAnim(c, 756);
					c.getPA().walkTo(-4, 0, true);
					startLedgeWalk = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().barbarianCourseStatus == 3) {
						c.getAgility().barbarianCourseStatus = 4;
					}
					c.getPA().addSkillXP(22, Skills.AGILITY);
					resetAgility(c);
				}
			}
		}, 1);

	}

	public static void climbCrumblingWall(final Client c, final com.soulplay.game.world.GameObject o) {
		if (c.performingAction) {
			return;
		}
		if (c.getY() != 3553) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX() + 1;
		final int destY = o.getY();
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					container.stop();
					return;
				}
				if (c.getY() != o.getY()) {
					c.getPA().playerWalk(o.getX() - 1, o.getY());
				} else {
					setWalkAnim(c, 839);
					c.getPA().walkTo(1, 0, true);
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().barbarianCourseStatus >= 4) {
						c.getAgility().barbarianCourseStatus++;
					}
					if (c.getAgility().barbarianCourseStatus >= 7) {
						c.getPA().addSkillXP(46, Skills.AGILITY);
						c.getAgility().barbarianCourseStatus = 0;
						if (!WorldType.equalsType(WorldType.SPAWN)) {
							c.getItems().addItem(995, 35000);
						}
						int tokens = 5;
						if (c.playerRights == 6 || c.playerRights == 11
								|| c.playerRights == 12) {
							tokens += 2;
						}
						c.rewardAgilityPoints(tokens);
						c.getAchievement().barbLaps();
						c.increaseProgress(TaskType.AGILITY, 167);
					}
					c.getPA().addSkillXP(9, Skills.AGILITY);
					resetAgility(c);
				}
			}
		}, 1);

	}

	public static void logBalance(final Client c, final GameObject o) {
		if (c.performingAction) {
			return;
		}
		if (c.getY() != 3546) {
			c.sendMessage("Make sure to stand infront of the log!");
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);
		final int destX = o.getX() - 9;
		final int destY = o.getY();
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean startLogWalk = false;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.getX() == destX && c.getY() == destY) {
					container.stop();
					return;
				}
				if (c.getX() == o.getX() + 1 && c.getY() == o.getY()) {
					startLogWalk = true;
				}
				if (startLogWalk) {
					setWalkAnim(c, 762);
					c.getPA().walkTo(-10, 0, true);
					startLogWalk = false;
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					if (c.getAgility().barbarianCourseStatus == 1) {
						c.getAgility().barbarianCourseStatus = 2;
					}
					c.getPA().addSkillXP(14, Skills.AGILITY);
					resetAgility(c);
				}
			}
		}, 1);

	}
	
	public static void pipeSqueeze(Client c, GameObject o) {
		Direction dir = Direction.SOUTH;
		if (c.getY() == 3558) {
			dir = Direction.NORTH;
		} else {
		}
		final ForceMovementMask mask = ForceMovementMask.createMask(0, dir == Direction.SOUTH ? -3 : 3, 3, 0, dir);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
				c.setForceMovement(mask);
			}
		}, 1);

		c.getPA().addSkillXP(1, Skills.AGILITY);
	}

	private static void resetAgility(Client c) {
		// c.getPA().sendFrame36(173, 1);
		c.getMovement().hardResetWalkingQueue();
		if (c.usedToRun) {
			c.isRunning = true;
			c.usedToRun = false;
		}
		c.noClip = false;
		c.performingAction = false;
		c.getCombat().getPlayerAnimIndexBypass();
	}

	public static void ropeSwing(final Client c, final GameObject o) {
		// c.getPA().objectAnim(2551, 3550, 100, o.getType(), o.getFace());
		// System.out.println("type face "+o.getType()+" "+o.getFace());
		// c.setForceMovement(0, +5, false, false, 0, 70, 2, 751, c.absX, c.absY
		// - 5, 2);

		if (c.performingAction) {
			return;
		}
		c.performingAction = true;
		setAgilityWalk(c);

		if (c.getAgility().barbarianCourseStatus == 0) {
			c.getAgility().barbarianCourseStatus = 1;
		}
		
		c.faceLocation(c.getX(), c.getY()-1);

		o.animate(509);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				c.startAnimation(AgilityAnimations.ROPE_SWING);
				c.setForceMovement(ForceMovementMask.createMask(0, 0, 1, 0, Direction.SOUTH).addSecondDirection(0, -5, 2, 0));
				c.getPA().addSkillXP(22, Skills.AGILITY);
			}

			@Override
			public void stop() {
			}
		}, 1);

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}

			@Override
			public void stop() {
				if (c != null) {
					resetAgility(c);
				}
			}
		}, 7);
	}

	private static void setAgilityWalk(Client c) {
		c.getMovement().hardResetWalkingQueue();
		c.noClip = true;
		if (c.isRunning) {
			c.isRunning = false;
			c.usedToRun = true;
		}
		// c.getPA().sendFrame36(173, 0);
	}

	private static void setWalkAnim(Client c, int walkAnim) {
		// c.playerWalkIndex = walkAnim;
		c.getMovement().getPlayerMovementAnimIds()[2] = (short) walkAnim;
		c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

}
