package com.soulplay.content.player.skills.agility;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.SkillHandler;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.UpdateFlag;

public class AgilityShortcuts {

	public static final int[][] DIRECTION = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
	
	public static final Animation MONKEY_BAR_MOUNT = new Animation(742);
	public static final Animation MONKEY_BAR_UNMOUNT = new Animation(743);
	public static final Animation MONKEY_BAR_TRAVERSE = new Animation(744);
	
	private static void startObjectInteract(Client c, int objectId, int objectX, int objectY,
			int walkToX, int walkToY, int clickType) {
		c.getPA().playerWalk(walkToX, walkToY);
		c.clickObjectType = clickType;
		c.objectId = objectId;
		c.objectX = objectX;
		c.objectY = objectY;
		c.walkingToObject = true;
		c.finalLocalDestX = walkToX /*- c.getMapRegionX() * 8*/;
		c.finalLocalDestY = walkToY /*- c.getMapRegionY() * 8*/;
	}

	public static boolean handleObject(Client c, GameObject o) {

		switch (o.getId()) {
		
		case 2332: // shilo log walk
			if (!SkillHandler.hasRequiredLevel(c,
					Skills.AGILITY, 55, "Agility",
					"use this obstacle")) {
				return true;
			}
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer con) {
					con.stop();
					int walkX = 4;
					int walkY = 0;
					Direction dir = Direction.EAST;
					if (c.getX() > o.getX()) {
						walkX = -4;
						dir = Direction.WEST;
					} else if (c.getY() > o.getY()) {
						walkX = 0;
						walkY = -4;
						dir = Direction.SOUTH;
					} else if (c.getY() < o.getY()) {
						walkX = 0;
						walkY = 4;
						dir = Direction.NORTH;
					}
					ForceMovementMask mask = ForceMovementMask.createMask(walkX, walkY, 5, 762, dir).addStartAnim(762);
					c.setForceMovement(mask);
				}
			}, 1);
			
			return true;
		
		
			case 9295:
				if (!SkillHandler.hasRequiredLevel(c,
						Skills.AGILITY, 51, "Agility",
						"use this obstacle")) {
					return true;
				}
				if (o.getOrientation() == 0 || o.getOrientation() == 2) {
					if (c.getY() != o.getY()) {
						c.sendMessage(
								"Please stand in front of the obstacle first.");
						return true;
					}
				} else {
					if (c.getX() != o.getX()) {
						c.sendMessage(
								"Please stand in front of the obstacle first.");
						return true;
					}
				}
				Direction dir = Direction.WEST;
				if (c.getX() == 3149)
					dir = Direction.EAST;
				
				c.getMovement().hardResetWalkingQueue();
				final ForceMovementMask mask = ForceMovementMask.createMask(dir == Direction.WEST ? -6 : 6, 0, 5, 0, dir);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
						c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
						c.setForceMovement(mask);
					}
				}, 1);
				return true;

			case 2320:
				if (!SkillHandler.hasRequiredLevel(c,
						Skills.AGILITY, 45, "Agility",
						"use this obstacle")) {
					return true;
				}
				if (c.getY() < 9965) { // lower part of the obstacle
					int useFromX = 3120;
					int useFromY = 9964;
					int endX = 3120;
					int endY = 9969;

					if (c.getX() != useFromX || c.getY() != useFromY) { // get
																		// in
																		// position
																		// to
																		// use
																		// obstacle
						startObjectInteract(c, o.getId(), o.getX(),
								o.getY(), useFromX, useFromY, 1); // click
																	// object
																	// again
						return true;
					}
					c.getMovement().hardResetWalkingQueue();

					final short oldStand = c.getMovement().getPlayerMovementAnimIds()[0];
					final short oldWalk = c.getMovement().getPlayerMovementAnimIds()[2];
					CycleEventHandler.getSingleton().addEvent(c,
							new CycleEvent() {

								int timer = 5;

								@Override
								public void execute(
										CycleEventContainer container) {
									if (timer == 5) {
										c.startAnimation(MONKEY_BAR_MOUNT);
										c.getMovement().getPlayerMovementAnimIds()[0] = 744;
										c.getMovement().getPlayerMovementAnimIds()[2] = 744;
										c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
									}
									if (timer == 4) {
										ForceMovementMask mask = ForceMovementMask.createMask(0, 5, 6, 0, Direction.NORTH);
										c.setForceMovement(mask);
									}
									if (timer != 0)
										c.startAnimation(MONKEY_BAR_TRAVERSE);
									if (timer == 0) {
										container.stop();
										c.startAnimation(MONKEY_BAR_UNMOUNT);
									}
									timer--;
								}

								@Override
								public void stop() {
									if (c.isActive) {
										c.getMovement().getPlayerMovementAnimIds()[0] = oldStand;
										c.getMovement().getPlayerMovementAnimIds()[2] = oldWalk;
										c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
									}
								}
							}, 1);

				} else { // top part of obstacle
					int useFromX = 3121;
					int useFromY = 9969;

					if (c.getX() != useFromX || c.getY() != useFromY) { // get
																		// in
																		// position
																		// to
																		// use
																		// obstacle
						startObjectInteract(c, o.getId(), o.getX(),
								o.getY(), useFromX, useFromY, 1); // click
																	// object
																	// again
						return true;
					}

					c.getMovement().hardResetWalkingQueue();
					c.faceLocation(c.getX(), c.getY()-1);

					// force movement
					final short oldStand = c.getMovement().getPlayerMovementAnimIds()[0];
					final short oldWalk = c.getMovement().getPlayerMovementAnimIds()[2];
					CycleEventHandler.getSingleton().addEvent(c,
							new CycleEvent() {

								int timer = 5;

								@Override
								public void execute(
										CycleEventContainer container) {
									if (timer == 5) {
										c.startAnimation(MONKEY_BAR_MOUNT);
										c.getMovement().getPlayerMovementAnimIds()[0] = 744;
										c.getMovement().getPlayerMovementAnimIds()[2] = 744;
										c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
									}
									if (timer == 4) {
										ForceMovementMask mask = ForceMovementMask.createMask(0, -5, 6, 0, Direction.SOUTH);
										c.setForceMovement(mask);
									}
									if (timer != 0)
										c.startAnimation(MONKEY_BAR_TRAVERSE);
									if (timer == 0) {
										c.startAnimation(MONKEY_BAR_UNMOUNT);
										container.stop();
									}
									timer--;
								}

								@Override
								public void stop() {
									c.getMovement().getPlayerMovementAnimIds()[0] = oldStand;
									c.getMovement().getPlayerMovementAnimIds()[2] = oldWalk;
									c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
								}
							}, 1);

				}
				return true;

		}

		return false;
	}
}
