package com.soulplay.content.player.skills.agility;

import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

public class AgilityAnimations {

	public static final Animation ROCK_CLIMB = new Animation(737);
	public static final Animation JUMP_OVER_1TICKS = new Animation(741);
	public static final Animation JUMP_OVER_1TICKS_FAST = new Animation(1604);
	public static final Animation JUMP_OVER_1TICKS_DELAY1TICK = new Animation(741, 25);
	public static final Animation JUMP_OVER_1TICKS_OSRS = new Animation(Animation.getOsrsAnimId(741));
	public static final Animation JUMP_REV_PILLAR = new Animation(Animation.getOsrsAnimId(741), 10);
	public static final Animation FALL_NORTH = new Animation(760, 0);
	public static final Animation LOG_WALK = new Animation(762);
	public static final Animation LOG_WALK2 = new Animation(9908);
	public static final Animation LOG_WALK3 = new Animation(11578);
	public static final Animation LOG_WALK4 = new Animation(12478);
	public static final Animation LOG_WALK_DELAYED = new Animation(762, 15);
	public static final Animation LOG_WALK_FALL_ANIM = new Animation(764);
	public static final Animation FALL_BOUNCE = new Animation(767);
	public static final Animation JUMP_OVER_1TICKS_OSRS_DELAYED_HALF_TICK = new Animation(Animation.getOsrsAnimId(741), 25);
	public static final Animation JUMP_OVER_CRACK_2TICKS = new Animation(807);
	public static final Animation JUMP_OVER_CRACK_3TICKS = new Animation(3067);
	public static final Animation PIPE_SQUEEZE_ANIM = new Animation(10580);
	public static final Animation PIPE_SQUEEZE_ANIM_DELAYED = new Animation(10580, 20);
	public static final Animation CRAWL_OVER_FENCE = new Animation(839);
	public static final Animation CRAWL_OVER_FENCE_DELAYED = new Animation(839, 10);
	public static final Animation CRAWL_OVER_FENCE_DELAYED_1_TICK = new Animation(839, Misc.serverToClientTick(1));
	public static final Animation CRAWL_UNDER = new Animation(844);
	public static final Animation CRAWL_UNDER_DELAYED = new Animation(844, 20);
	public static final Animation ROPE_SWING = new Animation(751);
	public static final Animation ROPE_SWING_DELAYED = new Animation(751, 30);
	public static final Animation JUMP_RAIL = new Animation(1603); //i think it's to kick the ball
	public static final Animation KICK_BALL = new Animation(1606); //i think it's to kick the ball
	public static final Animation FALL_LEFT = new Animation(2581);
	public static final Animation FALL_RIGHT = new Animation(2582);
	public static final Animation CLIMB_FROM_UNDDERGROUND = new Animation(2585);
	

	public static final Animation FAIL_WALL_CLIMB = new Animation(2556);
	
	public static final Animation ROCK_CLIMB_667 = new Animation(4435);
	
	public static final Animation ZIP_LINE = new Animation(5211);
	

	public static final Animation CLIMB_WALL_OSRS = new Animation(Animation.getOsrsAnimId(737));
	
	public static final Animation LAND_FROZEN_OSRS = new Animation(Animation.getOsrsAnimId(2583));
	public static final Animation LAND_OSRS = new Animation(Animation.getOsrsAnimId(2588));
	
}
