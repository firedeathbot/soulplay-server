package com.soulplay.content.player.skills.crafting;

import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;

public class Moulding {

	private static final int[][] RINGS = { // Ring, Gem, Level, XP
			{1635, -1, 5, 15}, {1637, 1607, 20, 40}, {1639, 1605, 27, 55},
			{1641, 1603, 34, 70}, {1643, 1601, 43, 85}, {1645, 1615, 55, 100},
			{6575, 6573, 67, 115}};

	private static final int[][] NECKLACES = {{1654, -1, 6, 20}, {1656, 1607, 22, 55},
			{1658, 1605, 29, 60}, {1660, 1603, 40, 75}, {1662, 1601, 56, 90},
			{1664, 1615, 72, 105}, {6577, 6573, 82, 120}};

	private static final int[][] AMULETS = {{1673, -1, 8, 30}, {1675, 1607, 24, 65},
			{1677, 1605, 31, 70}, {1679, 1603, 50, 85}, {1681, 1601, 70, 100},
			{1683, 1615, 80, 150}, {6579, 6573, 90, 165}};

	private static final int[][] MOULD_INTERFACE_IDS = {
			/* Rings */
			{1635, 1637, 1639, 1641, 1643, 1645, 6575},
			/* Neclece */
			{1654, 1656, 1658, 1660, 1662, 1664, 6577},
			/* amulet */
			{1673, 1675, 1677, 1679, 1681, 1683, 6579}

	};
	
	public static String getRequiredMessage(String item) {
		if (item.startsWith("A") || item.startsWith("E") || item.startsWith("I")
				|| item.startsWith("O") || item.startsWith("U")) {
			return "You need a Gold bar and an " + item + " to make this.";
		} else {
			return "You need a Gold bar and a " + item + " to make this.";
		}
	}

	public static void mouldInterface(Client c) {
		c.getPacketSender().showInterface(4161);
		/* Rings */
		if (c.getItems().playerHasItem(1592, 1)) {
			c.getPacketSender().updateInventoryPartial(4233, MOULD_INTERFACE_IDS[0]);
			c.getPacketSender().sendFrame126("", 4230);
			c.getPacketSender().sendFrame246(4229, 0, -1);
		} else {
			c.getPacketSender().sendFrame246(4229, 120, 1592);
			c.getPacketSender().resetInvenory(4233);
			c.getPacketSender().sendFrame126(
					"You need a ring mould to craft rings.", 4230);
		}
		/* Necklace */
		if (c.getItems().playerHasItem(1597, 1)) {
			c.getPacketSender().updateInventoryPartial(4239, MOULD_INTERFACE_IDS[1]);
			c.getPacketSender().sendFrame246(4235, 0, -1);
			c.getPacketSender().sendFrame126("", 4236);
		} else {
			c.getPacketSender().sendFrame246(4235, 120, 1597);
			c.getPacketSender().resetInvenory(4239);
			c.getPacketSender().sendFrame126(
					"You need a necklace mould to craft necklaces.", 4236);
		}
		/* Amulets */
		if (c.getItems().playerHasItem(1595, 1)) {
			c.getPacketSender().updateInventoryPartial(4245, MOULD_INTERFACE_IDS[2]);
			c.getPacketSender().sendFrame246(4241, 0, -1);
			c.getPacketSender().sendFrame126("", 4242);
		} else {
			c.getPacketSender().sendFrame246(4241, 120, 1595);
			c.getPacketSender().resetInvenory(4245);
			c.getPacketSender().sendFrame126("You need a amulet mould to craft amulets.", 4242);
		}
	}

	public static void mouldItem(final Client c, int item, int amount) {
		if (c.skillingEvent != null) {
			return;
		}

		// int done = 0;

		final int GOLD_BAR = 2357;

		boolean isRing = false;
		boolean isNeck = false;
		boolean isAmulet = false;
		int gem = 0;
		int itemAdd = -1;
		int xp = 0;
		int lvl = 1;
		for (int i = 0; i < 7; i++) {
			if (item == RINGS[i][0]) {
				isRing = true;
				itemAdd = RINGS[i][0];
				gem = RINGS[i][1];
				lvl = RINGS[i][2];
				xp = RINGS[i][3];
				break;
			}
			if (item == NECKLACES[i][0]) {
				isNeck = true;
				itemAdd = NECKLACES[i][0];
				gem = NECKLACES[i][1];
				lvl = NECKLACES[i][2];
				xp = NECKLACES[i][3];
				break;
			}
			if (item == AMULETS[i][0]) {
				isAmulet = true;
				itemAdd = AMULETS[i][0];
				gem = AMULETS[i][1];
				lvl = AMULETS[i][2];
				xp = AMULETS[i][3];
				break;
			}
		}
		if (!isRing && !isNeck && !isAmulet) {
			return;
		}
		if (c.getPlayerLevel()[Skills.CRAFTING] >= lvl) {
			if (gem == -1) {
				if (!c.getItems().playerHasItem(GOLD_BAR, 1)) {
					c.sendMessage("You need a Gold bar to make this.");
					return;
				}
			} else {
				if (!c.getItems().playerHasItem(gem, 1) && c.getItems().playerHasItem(GOLD_BAR, 1)) {
					c.sendMessage(getRequiredMessage(ItemConstants.getItemName(gem)));
					return;
				}
			}

			c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
			// c.startAnimation(899);

			final int xpFinal = xp;
			final int itemAddFinal = itemAdd;
			final int gemFinal = gem;

			c.startAction(new CycleEvent() {

				int amount = c.getItems().getItemAmount(gemFinal == -1 ? GOLD_BAR : gemFinal);

				@Override
				public void execute(CycleEventContainer e) {
					if (amount < 1) {
						// c.sendMessage("You finish moulding.");
						e.stop();
						return;
					}

					if (gemFinal == -1) {
						if (!c.getItems().playerHasItem(GOLD_BAR, 1)) {
							c.sendMessage("You need a Gold bar to make this.");
							e.stop();
							return;
						}
					} else {
						if (!c.getItems().playerHasItem(gemFinal, 1)
								&& c.getItems().playerHasItem(GOLD_BAR, 1)) {
							c.sendMessage(getRequiredMessage(
									ItemConstants.getItemName(gemFinal)));
							e.stop();
							return;
						}
					}

					if (!c.getItems().deleteItemInOneSlot(GOLD_BAR, 1)) {
						e.stop();
						return;
					}
					if (gemFinal != -1) {
						c.getItems().deleteItemInOneSlot(gemFinal, 1);
					}
					c.startAnimation(899);
					c.getItems().addItem(itemAddFinal, 1);
					c.getPA().addSkillXP(xpFinal,
							Skills.CRAFTING);
					c.increaseProgress(TaskType.CRAFTING_MOULD, gemFinal);

					if (gemFinal == -1) {
						c.sendMessageSpam(
								"You craft the gold to form a "
										+ ItemConstants.getItemName(itemAddFinal)
										+ ".");
					} else {
						c.sendMessageSpam(
								"You craft the gold and gem together to form a "
										+ ItemConstants.getItemName(itemAddFinal)
										+ ".");
					}
					amount--;

				}

				@Override
				public void stop() {
					/* empty */
				}
			}, 5);
		} else {
			c.sendMessage(
					"You need a Crafting level of " + lvl + " to craft this.");
			return;
		}
	}

}
