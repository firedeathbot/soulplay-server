package com.soulplay.content.player.skills.crafting;

import com.soulplay.content.items.urn.SkillingUrn;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;

public class Pottery {

	public static final int SOFT_CLAY = 1761;
	public static final Animation SPIN_ANIMATION = new Animation(883);

	public static void start(Player player) {
		player.getDialogueBuilder().sendOption("What would you like to make?",
				"Cooking urns", () -> {
					MakeInterface.make(player,
						SkillingUrn.CRACKED_COOKING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.CRACKED_COOKING);
						}, SkillingUrn.FRAGILE_COOKING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.FRAGILE_COOKING);
						}, SkillingUrn.NORMAL_COOKING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.NORMAL_COOKING);
						}, SkillingUrn.STRONG_COOKING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.STRONG_COOKING);
						}, SkillingUrn.DECORATED_COOKING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.DECORATED_COOKING);
						}
					);
				},
				"Fishing urns", () -> {
					MakeInterface.make(player, 
						SkillingUrn.CRACKED_FISHING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.CRACKED_FISHING);
						}, SkillingUrn.FRAGILE_FISHING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.FRAGILE_FISHING);
						}, SkillingUrn.NORMAL_FISHING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.NORMAL_FISHING);
						}, SkillingUrn.STRONG_FISHING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.STRONG_FISHING);
						}, SkillingUrn.DECORATED_FISHING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.DECORATED_FISHING);
						}
					);
				},
				"Mining urns", () -> {
					MakeInterface.make(player, 
						SkillingUrn.CRACKED_MINING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.CRACKED_MINING);
						}, SkillingUrn.FRAGILE_MINING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.FRAGILE_MINING);
						}, SkillingUrn.NORMAL_MINING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.NORMAL_MINING);
						}, SkillingUrn.STRONG_MINING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.STRONG_MINING);
						}, SkillingUrn.DECORATED_MINING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.DECORATED_MINING);
						}
					);
				},
				"Smelting urns", () -> {
					MakeInterface.make(player, 
						SkillingUrn.CRACKED_SMELTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.CRACKED_SMELTING);
						}, SkillingUrn.FRAGILE_SMELTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.FRAGILE_SMELTING);
						}, SkillingUrn.NORMAL_SMELTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.NORMAL_SMELTING);
						}, SkillingUrn.STRONG_SMELTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.STRONG_SMELTING);
						}
					);
				},
				"Woodcutting urns", () -> {
					MakeInterface.make(player, 
						SkillingUrn.CRACKED_WOODCUTTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.CRACKED_WOODCUTTING);
						}, SkillingUrn.FRAGILE_WOODCUTTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.FRAGILE_WOODCUTTING);
						}, SkillingUrn.NORMAL_WOODCUTTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.NORMAL_WOODCUTTING);
						}, SkillingUrn.STRONG_WOODCUTTING.getUnfired(), () -> {
							makeUrn(player, SkillingUrn.STRONG_WOODCUTTING);
						}
					);
				}
		).execute(false);
	}

	private static boolean canCraftUrns(Player player, SkillingUrn data) {
		if (!player.getItems().playerHasItem(SOFT_CLAY, 2)) {
			player.getDialogueBuilder().sendStatement("You don't have enough materials to craft.").execute();
			return false;
		}

		if (player.getPlayerLevel()[Skills.CRAFTING] < data.getMakeLevel()) {
			player.sendMessage("You need a crafting level of " + data.getMakeLevel() + " to craft this.");
			return false;
		}

		return true;
	}

	private static void makeUrn(Player player, SkillingUrn data) {
		player.getPA().closeAllWindows();

		if (!canCraftUrns(player, data) || player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int cycles = player.makeCount;
			
			@Override
			public void execute(CycleEventContainer container) {
				player.getItems().deleteItem2(SOFT_CLAY, 2);
				player.startAnimation(SPIN_ANIMATION);
				player.getItems().addItem(data.getUnfired(), 1);
				player.getPA().addSkillXP(data.getMakeXp(), Skills.CRAFTING);
				player.sendMessage("You craft " + ItemDef.forID(data.getUnfired()).getName() + ".");
				
				cycles--;
				if (cycles == 0 || !canCraftUrns(player, data)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 3);
	}

	private static boolean canFireUrn(Player player, SkillingUrn data, boolean showRequirements) {
		if (!player.getItems().playerHasItem(data.getUnfired(), 1)) {
			if (showRequirements) {
				player.getDialogueBuilder().sendStatement("You don't have enough materials to fire.").execute();
			}
			return false;
		}

		if (player.getPlayerLevel()[Skills.CRAFTING] < data.getMakeLevel()) {
			if (showRequirements) {
				player.sendMessage("You need a crafting level of " + data.getMakeLevel() + " to craft this.");
			}
			return false;
		}

		return true;
	}

	public static void fireUrn(Player player) {
		SkillingUrn urn = null;

		for (SkillingUrn entry : SkillingUrns.URNS) {
			if (player.getItems().playerHasItem(entry.getUnfired())) {
				urn = entry;
				break;
			}
		}

		if (urn == null) {
			player.sendMessage("You have nothing to fire.");
			return;
		}

		player.getPA().closeAllWindows();

		if (!canFireUrn(player, urn, true) || player.skillingEvent != null) {
			return;
		}

		final SkillingUrn finalUrn = urn;
		player.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				player.getItems().deleteItem2(finalUrn.getUnfired(), 1);
				player.startAnimation(Smelting.SMELT_ANIMATION);
				player.getItems().addItem(finalUrn.getNoRune(), 1);
				player.getPA().addSkillXP(finalUrn.getFireXp(), Skills.CRAFTING);

				if (!canFireUrn(player, finalUrn, false)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 5);
	}

	public static boolean handleUrnRuneAdding(Player player, int itemUsed, int usedOn) {
		SkillingUrn urn = SkillingUrns.NO_RUNE_URNS.get(itemUsed);
		boolean checkUsedOn = true;
		if (urn == null) {
			urn = SkillingUrns.NO_RUNE_URNS.get(usedOn);
			checkUsedOn = false;
		}

		if (urn == null) {
			return false;
		}

		int runeId = checkUsedOn ? usedOn : itemUsed;
		if (runeId != urn.getRuneId()) {
			player.sendMessage("You can only use " + ItemDef.forID(urn.getRuneId()).getName() + " on to active it.");
			return true;
		}

		int makeCount = Math.min(player.getItems().getItemAmount(runeId), player.getItems().getItemAmount(urn.getNoRune()));
		for (int i = 0; i < makeCount; i++) {
			player.getItems().deleteItem2(urn.getNoRune(), 1);
			player.getItems().addItem(urn.getRune(), 1);
		}

		player.getItems().deleteItem2(runeId, makeCount);
		player.startAnimation(urn.getChargeAnimation());
		return true;
	}
}
