package com.soulplay.content.player.skills.crafting;

public enum LeatherCraftingData {

	GLOVES(1059, 13, 1),
	BOOTS(1061, 16, 7),
	COWL(1167, 18, 9),
	VAMBRACES(1063, 22, 11),
	BODY(1129, 25, 14),
	CHAPS(1095, 27, 18),
	COIF(1169, 37, 38);

	private final int product, xp, levelReq;

	private LeatherCraftingData(int product, int xp, int levelReq) {
		this.product = product;
		this.xp = xp;
		this.levelReq = levelReq;
	}

	public int getProduct() {
		return product;
	}

	public int getXp() {
		return xp;
	}

	public int getLevelReq() {
		return levelReq;
	}

}
