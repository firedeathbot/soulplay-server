package com.soulplay.content.player.skills.crafting;

import java.util.HashMap;
import java.util.Map;

public enum MoltenGlassEnum {
	BEER_GLASS(48112, 1919, 1919, 1, 17, 48109, 48110, 48111),
	CANDLE_LANTERN(48116, 4529, 4527, 4, 19, 48113, 48114, 48115),
	OIL_LAMP(48120, 4522, 4522, 12, 25, 48117, 48118, 48119),
	VIAL(44210, 229, 229, 33, 35, 44207, 44208, 44209),
	FISHBOWL(24059, 6667, 6667, 42, 42, 24056, 24057, 24058),
	ORB(48108, 573, 567, 46, 52, 48105, 48106, 48107),
	LANTERN_LENS(48124, 4542, 4542, 49, 55, 48121, 48122, 48123),
//	EMPTY_LIGHT_ORB(57232, 10973, 10973, 87, 70)
	;
	
	private final int buttonId, displayId, productId, lvl, exp, makeX, make10, make5;

	private MoltenGlassEnum(int buttonId, int displayId, int prod, int lvl, int exp, int makeX, int make10, int make5) {
		this.buttonId = buttonId;
		this.displayId = displayId;
		this.productId = prod;
		this.lvl = lvl;
		this.exp = exp;
		this.makeX = makeX;
		this.make10 = make10;
		this.make5 = make5;
	}

	public int getButtonId() {
		return buttonId;
	}

	public int getMakeX() {
		return makeX;
	}

	public int getMake10() {
		return make10;
	}

	public int getMake5() {
		return make5;
	}

	public int getDisplayId() {
		return displayId;
	}

	public int getProductId() {
		return productId;
	}

	public int getLvl() {
		return lvl;
	}
	
	private static final Map<Integer, MoltenGlassEnum> map = new HashMap<>();
	
	public static void init() {
		for (MoltenGlassEnum glass : values()) {
			map.put(glass.getButtonId(), glass);
			map.put(glass.getMakeX(), glass);
			map.put(glass.getMake10(), glass);
			map.put(glass.getMake5(), glass);
		}
	}
	
	public static MoltenGlassEnum get(int buttonId) {
		return map.getOrDefault(buttonId, BEER_GLASS);
	}

	public int getExp() {
		return exp;
	}
	
}
