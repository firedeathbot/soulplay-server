package com.soulplay.content.player.skills.crafting;

import com.soulplay.game.model.player.Player;

public class TanHide {

	public static final int INTERFACE_ID = 14670;

	private enum Tan {
		SOFT_LEATHER(1739, 1741, 1),
		HARD_LEATHER(1739, 1743, 3),
		GREEN_DHIDE(1753, 1745, 20),
		BLUE_DHIDE(1751, 2505, 20),
		RED_DHIDE(1749, 2507, 20),
		BLACK_DHIDE(1747, 2509, 20),
		SNAKESKIN_1(6287, 6289, 15),
		SNAKESKIN_2(7801, 6289, 20);
		
		private final int hide, tannedHide, cost;

		private Tan(int hide, int tannedHide, int cost) {
			this.hide = hide;
			this.tannedHide = tannedHide;
			this.cost = cost;
		}

		public int getCost() {
			return cost;
		}

		public int getHide() {
			return hide;
		}

		public int getTannedHide() {
			return tannedHide;
		}
	}

	public static void handleActionButton(Player c, int button) {
		if (c.interfaceIdOpenMainScreen != INTERFACE_ID) {
			return;
		}

		switch (button) {
			case 57225:
				tanHide(c, Tan.SOFT_LEATHER);
				break;
			case 57226:
				tanHide(c, Tan.HARD_LEATHER);
				break;
			case 57227:
				tanHide(c, Tan.GREEN_DHIDE);
				break;
			case 57228:
				tanHide(c, Tan.BLUE_DHIDE);
				break;
			case 57229:
				tanHide(c, Tan.RED_DHIDE);
				break;
			case 57230:
				tanHide(c, Tan.BLACK_DHIDE);
				break;
			case 57231:
				tanHide(c, Tan.SNAKESKIN_1);
				break;
			case 57232:
				tanHide(c, Tan.SNAKESKIN_2);
				break;
		}
	}

	private static void tanHide(Player c, Tan t) {
		int amtOfHides = c.getItems().getItemCount(t.getHide());
		if (amtOfHides <= 0) {
			c.sendMessage("You don't have any hides you can tan.");
			return;
		}

		int cost = amtOfHides * t.getCost();
		if (!c.getItems().takeCoins(cost)) {
			c.sendMessage("You do not have enough money with you to tan all the hides at once.");
			return;
		}

		c.getItems().deleteItem2(t.getHide(), amtOfHides);
		c.getItems().addItem(t.getTannedHide(), amtOfHides);
		c.sendMessage("You tan " + amtOfHides + (amtOfHides > 1 ? " hides" : " hide") + " for " + cost + " coins.");
		updateCostText(c);
	}

	public static void openTanning(Player c) {
		c.getPacketSender().sendFrame246(14769, 300, Tan.SOFT_LEATHER.getHide());
		c.getPacketSender().sendFrame246(14770, 300, Tan.HARD_LEATHER.getHide());
		c.getPacketSender().sendFrame246(14771, 300, Tan.GREEN_DHIDE.getHide());
		c.getPacketSender().sendFrame246(14772, 300, Tan.BLUE_DHIDE.getHide());
		c.getPacketSender().sendFrame246(14773, 300, Tan.RED_DHIDE.getHide());
		c.getPacketSender().sendFrame246(14774, 300, Tan.BLACK_DHIDE.getHide());
		c.getPacketSender().sendFrame246(14775, 300, Tan.SNAKESKIN_1.getHide());
		c.getPacketSender().sendFrame246(14776, 300, Tan.SNAKESKIN_2.getHide());
		updateCostText(c);
		c.getPacketSender().showInterface(INTERFACE_ID);
	}

	private static void updateCostText(Player c) {
		boolean hasHides = c.getItems().playerHasItem(Tan.SOFT_LEATHER.getHide());
		String color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Leather", 14777);
		c.getPacketSender().sendFrame126(color + "1 Coin", 14785);

		hasHides = c.getItems().playerHasItem(Tan.HARD_LEATHER.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Hard Leather", 14778);
		c.getPacketSender().sendFrame126(color + "3 Coins", 14786);

		hasHides = c.getItems().playerHasItem(Tan.GREEN_DHIDE.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Green d'hide", 14779);
		c.getPacketSender().sendFrame126(color + "20 Coins", 14787);

		hasHides = c.getItems().playerHasItem(Tan.BLUE_DHIDE.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Blue d'hide", 14780);
		c.getPacketSender().sendFrame126(color + "20 Coins", 14788);

		hasHides = c.getItems().playerHasItem(Tan.RED_DHIDE.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Red d'hide", 14781);
		c.getPacketSender().sendFrame126(color + "20 Coins", 14789);

		hasHides = c.getItems().playerHasItem(Tan.BLACK_DHIDE.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Black d'hide", 14782);
		c.getPacketSender().sendFrame126(color + "20 Coins", 14790);

		hasHides = c.getItems().playerHasItem(Tan.SNAKESKIN_1.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Snakeskin", 14783);
		c.getPacketSender().sendFrame126(color + "15 Coins", 14791);

		hasHides = c.getItems().playerHasItem(Tan.SNAKESKIN_2.getHide());
		color = (hasHides ? "<col=ff00>" : "<col=800000>");
		c.getPacketSender().sendFrame126(color + "Snakeskin", 14784);
		c.getPacketSender().sendFrame126(color + "20 Coins", 14792);
	}

}
