package com.soulplay.content.player.skills.crafting;

import com.soulplay.game.world.entity.Animation;

public enum CutGems {
	
	SAPPHIRE(1623, 1607, 20, 888, 50),
	EMERALD(1621, 1605, 27, 889, 67),
	RUBY(1619, 1603, 34, 887, 85),
	DIAMOND(1617, 1601, 43, 886, 107),
	DRAGONSTONE(1631, 1615, 55, 885, 137),
	ONYX(6571, 6573, 67, 2717, 168),
	OPAL(1625, 1609, 1, 890, 10),
	JADE(1627, 1611, 13, 891, 20),
	RED_TOPAZ(1629, 1613, 16, 892, 25),
	ZENYTE(30196, 30195, 89, 892, 200);
	
	public static final CutGems[] values = CutGems.values();

	/**
	 * Represents the raw gem.
	 */
	private final int uncut;

	/**
	 * Represents the gem cut.
	 */
	private final int gem;

	/**
	 * Represents the level needed.
	 */
	private final int level;

	/**
	 * Represents the animation used.
	 */
	private final Animation animation;

	/**
	 * Represents the experience gained.
	 */
	private final int exp;

	/**
	 * Constructs a new {@code Gems} {@code Object}.
	 * @param uncut the uncut.
	 * @param gem the gem.
	 * @param level the level.
	 * @param animation the animation.
	 * @param exp the exp.
	 */
	private CutGems(int uncut, int gem, int level, int animation, int exp) {
		this.uncut = uncut;
		this.gem = gem;
		this.level = level;
		this.animation = new Animation(animation);
		this.exp = exp;
	}

	/**
	 * Gets the gem by the id.
	 * @param id the id.
	 * @return the gem.
	 */
	public static CutGems forId(final int item) {
		for (CutGems gem : CutGems.values) {
			if (gem.getUncut() == item) {
				return gem;
			}
		}
		return null;
	}

	/**
	 * Gets the uncut.
	 * @return The uncut.
	 */
	public int getUncut() {
		return uncut;
	}

	/**
	 * Gets the gem.
	 * @return The gem.
	 */
	public int getGem() {
		return gem;
	}

	/**
	 * Gets the level.
	 * @return The level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Gets the animation.
	 * @return The animation.
	 */
	public Animation getAnimation() {
		return animation;
	}

	/**
	 * Gets the exp.
	 * @return The exp.
	 */
	public int getExp() {
		return exp;
	}
}
