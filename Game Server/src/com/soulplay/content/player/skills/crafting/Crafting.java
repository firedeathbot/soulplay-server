package com.soulplay.content.player.skills.crafting;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.items.useitem.ItemsOnObjects.Items;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;

public class Crafting {

	public static final int NEEDLE = 1733;
	public static final Animation LEATHER_CRAFT_ANIM = new Animation(1249);
	public static final Animation SPIN_FLAX_ANIM = new Animation(896);// this is wrong one i think

	public enum DragonHideLeatherCrafting {
		HARD_LEATHER(1743, new short[] { 1131 }, new short[] { 35 }, new byte[] { 28 },
				new byte[] { 1 }),
		GREEN(1745, new short[] { 1065, 1099, 1135 }, new short[] { 62, 124, 186 }, new byte[] { 57, 60, 63 },
				new byte[] { 1, 2, 3 }),
		BLUE(2505, new short[] { 2487, 2493, 2499 }, new short[] { 70, 140, 210 }, new byte[] { 66, 68, 71 },
				new byte[] { 1, 2, 3 }),
		RED(2507, new short[] { 2489, 2495, 2501 }, new short[] { 78, 156, 234 }, new byte[] { 73, 75, 77 },
				new byte[] { 1, 2, 3 }),
		BLACK(2509, new short[] { 2491, 2497, 2503 }, new short[] { 86, 172, 258 }, new byte[] { 79, 82, 84 },
				new byte[] { 1, 2, 3 });

		public static final Map<Integer, DragonHideLeatherCrafting> values = new HashMap<>();
		private final int leatherId;
		private final short[] products, xp;
		private final byte[] levelReq, leatherReq;

		private DragonHideLeatherCrafting(int leatherId, short[] products, short[] xp, byte[] levelReq, byte[] leatherReq) {
			this.leatherId = leatherId;
			this.products = products;
			this.xp = xp;
			this.levelReq = levelReq;
			this.leatherReq = leatherReq;
		}

		public int getLeatherId() {
			return leatherId;
		}

		public short[] getProducts() {
			return products;
		}

		public short[] getXp() {
			return xp;
		}

		public byte[] getLevelReq() {
			return levelReq;
		}

		public byte[] getLeatherReq() {
			return leatherReq;
		}

		static {
			for (DragonHideLeatherCrafting data : values()) {
				values.put(data.getLeatherId(), data);
			}
		}
	}

	public static boolean handleDragonHideLeatherCrafting(Client c, int itemUsedWith, int itemUsedOn) {
		DragonHideLeatherCrafting data = DragonHideLeatherCrafting.values.get(itemUsedWith);
		if (data == null) {
			data = DragonHideLeatherCrafting.values.get(itemUsedOn);
			if (data == null) {
				return false;
			}
		}

		final DragonHideLeatherCrafting finalData = data;

		if (finalData == DragonHideLeatherCrafting.HARD_LEATHER) {
			MakeInterface.make(c, data.getProducts()[0], () -> {
				startCraft(c, finalData, 0);
			});
			return true;
		}

		MakeInterface.make(c, data.getProducts()[0], () -> {
			startCraft(c, finalData, 0);
		}, data.getProducts()[1], () -> {
			startCraft(c, finalData, 1);
		}, data.getProducts()[2], () -> {
			startCraft(c, finalData, 2);
		});
		return true;
	}

	private static boolean canCraft(Client c, DragonHideLeatherCrafting data, int index) {
		if (!c.getItems().playerHasItem(NEEDLE)) {
			c.sendMessage("You don't have a needle to craft with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.CRAFTING] < data.getLevelReq()[index]) {
			c.sendMessage("You need a crafting level of " + data.getLevelReq()[index] + " to craft this.");
			return false;
		}

		if (!c.getItems().playerHasItem(data.getLeatherId(), data.getLeatherReq()[index])) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to craft.").execute();
			return false;
		}

		return true;
	}

	private static void startCraft(Client player, DragonHideLeatherCrafting data, int index) {
		player.getPA().closeAllWindows();

		if (!canCraft(player, data, index) || player.skillingEvent != null) {
			return;
		}

		player.startAction(new CycleEvent() {

			int cycles = player.makeCount;

			@Override
			public void execute(CycleEventContainer container) {
				if (data == DragonHideLeatherCrafting.BLACK) {
					player.getAchievement().craftBlackHide();
				}

				int productId = data.getProducts()[index];
				player.getItems().deleteItem2(data.getLeatherId(), data.getLeatherReq()[index]);
				player.startAnimation(Crafting.LEATHER_CRAFT_ANIM);
				player.getItems().addItem(productId, 1);
				player.getPA().addSkillXP(data.getXp()[index], Skills.CRAFTING);
				player.increaseProgress(TaskType.CRAFTING, productId);
				player.sendMessageSpam("You craft " + ItemDef.forID(productId).getName() + ".");

				cycles--;
				if (cycles == 0 || !canCraft(player, data, index)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 3);
	}

	public static final int[] FURNACES = {26814, 2781, 21303, 124009};
	public static void craftZenyte(Client c) {
		MakeInterface.make(c, Items.ZENYTE_RING.getRewardId(), () -> {
			makeZenyte(c, Items.ZENYTE_RING);
		}, Items.ZENYTE_AMULET.getRewardId(), () -> {
			makeZenyte(c, Items.ZENYTE_AMULET);
		}, Items.ZENYTE_BRACELET.getRewardId(), () -> {
			makeZenyte(c, Items.ZENYTE_BRACELET);
		}, Items.ZENYTE_NECKLACE.getRewardId(), () -> {
			makeZenyte(c, Items.ZENYTE_NECKLACE);
		});
	}

	private static void makeZenyte(Client c, Items item) {
		c.getPA().closeAllWindows();
		
		if (c.skillingEvent != null) {
			return;
		}

		c.startAction(new CycleEvent() {

			int count = c.makeCount;
			
			@Override
			public void execute(CycleEventContainer container) {
				if (c.getPlayerLevel()[Skills.CRAFTING] < item.getCraftLvl()) {
					c.sendMessage("You need " + item.getCraftLvl() + " crafting level to craft this item.");
					container.stop();
					return;
				}

				if (( c.getItems().playerHasItem(item.getItemId1(), 1)) &&
						(c.getItems().playerHasItem(item.getItemId2(), 1)) &&
						(c.getItems().playerHasItem(item.getItemId3(), 1))) {

					c.getItems().deleteItemInOneSlot(item.getItemId2(), 1);
					c.getItems().deleteItemInOneSlot(item.getItemId3(), 1);
					c.getItems().addItem(item.getRewardId(), 1);
					c.getPA().addSkillXP(item.getCraftExp(), Skills.CRAFTING);
					c.startAnimation(Smelting.SMELT_ANIMATION);
					count--;
				} else {
					c.sendMessage("You need " + ItemDef.forID(item.getItemId1()).getName() + ", " + ItemDef.forID(item.getItemId2()).getName() + " and " + ItemDef.forID(item.getItemId3()).getName() + " to craft this item.");
					container.stop();
					return;
				}

				if (count == 0) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				
			}
			
		}, 5);
	}

	public static enum SpinningData {
		FLAX(1779, 1777),
		SINEW(9436, 9438);

		public static final SpinningData[] values = values();
		private final int material, product;

		private SpinningData(int material, int product) {
			this.material = material;
			this.product = product;
		}

		public int getMaterial() {
			return material;
		}

		public int getProduct() {
			return product;
		}

	}

	public static void spinningWheel(Player player) {
		SpinningData spinningData = null;
		for (SpinningData data : SpinningData.values) {
			if (player.getItems().playerHasItem(data.getMaterial())) {
				spinningData = data;
				break;
			}
		}

		if (spinningData == null) {
			player.sendMessage("You don't have anything left to spin.");
			return;
		}

		player.faceLocation(player.objectX, player.objectY);

		final int material = spinningData.getMaterial();
		final int product = spinningData.getProduct();
		player.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (!player.getItems().playerHasItem(material)) {
					player.sendMessage("You don't have anything left to spin.");
					container.stop();
					return;
				}

				player.startAnimation(SPIN_FLAX_ANIM);
				player.getItems().deleteItemInOneSlot(material, player.getItems().getItemSlot(material), 1);
				player.getItems().addItem(product, 1);
				player.increaseProgress(TaskType.CRAFTING, material);
				player.getPA().addSkillXP(15, Skills.CRAFTING);
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 3);
	}

}
