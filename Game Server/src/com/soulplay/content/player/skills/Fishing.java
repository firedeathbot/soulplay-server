package com.soulplay.content.player.skills;

import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.FishingContest;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.util.Misc;

/**
 * Class Fishing Handles: Fishing
 *
 * @author: PapaDoc START: 22:07 23/12/2010 FINISH: 22:28 23/12/2010
 */

public class Fishing extends SkillHandler {
	
	private static final int anglerHat = 30308;
	private static final int anglerWaders = 30310;
	private static final int anglerTop = 30309;
	private static final int anglerBoots = 30311;

	// 0retarded shit(supposed to be 0npcID, 1level, 2fishingTool, 3bait,
	// 4rawFishId, 5EXP, 6Emote, 7SecondFishId, 8SecondFishLvlReq,
	// 9SecondFishExp, 10 clue rate
	private static int[][] data = {
			{1, 1, 303, -1, 317, 10, Animation.getOsrsAnimId(621), 321, 15, 40, 870330}, // SHRIMP + ANCHOVIES
			{1174, 1, 303, -1, 317, 10, Animation.getOsrsAnimId(621), 3150, 5, 5, 443697}, // Karambwanji + shrimp
			{2, 5, 309, 313, 327, 20, 622, 345, 10, 30, 1056000}, // SARDINE + HERRING
			{3, 16, 305, -1, 353, 20, Animation.getOsrsAnimId(620), -1, -1, -1, 1147827}, // MACKEREL
			{4, 20, 309, 314, 335, 50, 622, 331, 30, 70, 923616}, // TROUT + SALMON
			{5, 23, 305, -1, 341, 45, Animation.getOsrsAnimId(619), 363, 46, 100, 1147827}, // BASS + COD
			{6, 25, 309, 313, 349, 60, 622, -1, -1, -1, 305792}, // PIKE
			{7, 35, 311, -1, 359, 80, 618, 371, 50, 100, 257770}, // TUNA + SWORDIE
			{8, 40, 301, -1, 377, 90, Animation.getOsrsAnimId(619), -1, -1, -1, 116129}, // LOBSTER
			{9, 62, 305, -1, 7944, 110, Animation.getOsrsAnimId(620), -1, -1, -1, 138583}, // MONKFISH
			{10, 76, 311, -1, 383, 120, 618, -1, -1, -1, 82243}, // SHARK
			{11, 79, 305, -1, 395, 38, Animation.getOsrsAnimId(619), -1, -1, -1, -1}, // SEA TURTLE
			{12, 81, 305, -1, 389, 200, Animation.getOsrsAnimId(621), -1, -1, -1, -1}, // MANTA RAY
			{13, 90, 307, 15263, 15270, 380, 622, -1, -1, -1, 78649}, // ROCKTAIL
			{14, 70, 307, 15270, 21520, 300, 622, -1, -1, -1, -1}, // tiger shark/fury shark
			{1178, 65, 3159, -1, 3142, 105, Animation.getOsrsAnimId(1193), -1, -1, -1, 170874}, // KARABWANS
	};

	public static void attemptdata(final Client c, int npcId) { // for bots
		if (!hasInventorySpace(c, "fishing")) {
			c.sendMessage(
					"You must have space in your inventory to start fishing.");
			return;
		}
		// resetFishing(c);
		for (int i = 0; i < data.length; i++) {
			if (npcId == data[i][0]) {
				if (c.getPlayerLevel()[Skills.FISHING] < data[i][1]) {
					c.sendMessage(
							"You haven't got high enough fishing level to fish here!");
					c.sendMessage("You at least need the fishing level of "
							+ data[i][1] + ".");
					c.getPA().sendStatement("You need the fishing level of "
							+ data[i][1] + " to fish here.");
					return;
				}
				if (data[i][3] > 0) {
					if (!c.getItems().playerHasItem(data[i][3])) {
						c.sendMessage("You haven't got any "
								+ ItemConstants.getItemName(data[i][3]) + "!");
						c.sendMessage("You need "
								+ ItemConstants.getItemName(data[i][3])
								+ " to fish here.");
						return;
					}
				}
				c.playerSkillProp[10][0] = data[i][6]; // ANIM id
				c.playerSkillProp[10][1] = data[i][4]; // FISH id
				c.playerSkillProp[10][2] = data[i][5]; // XP gain i guess
				c.playerSkillProp[10][3] = data[i][3]; // BAIT
				c.playerSkillProp[10][4] = data[i][2]; // EQUIP
				c.playerSkillProp[10][5] = data[i][7]; // sFish
				c.playerSkillProp[10][6] = data[i][8]; // sLvl
				c.playerSkillProp[10][7] = data[i][4]; // FISH
				c.playerSkillProp[10][8] = data[i][9]; // sXP
				c.playerSkillProp[10][9] = Misc.random(1) == 0 ? 7 : 5;
				c.playerSkillProp[10][10] = data[i][0]; // INDEX
				c.playerSkillProp[10][11] = data[i][1]; // Lvl
				c.playerSkillProp[10][12] = data[i][10]; // clue rate

				if (!hasFishingEquipment(c, c.playerSkillProp[10][4])) {
					return;
				}

				if (c.playerSkilling[10]) {
					return;
				}

				// c.turnPlayerTo(c.objectX, c.objectY); // fishing uses npcs
				// not objects lol objectX and objectY
				c.sendMessage("You start fishing.");
				c.startAnimation(c.playerSkillProp[10][0]);
				// c.playerIsSkilling = true;

				c.playerSkilling[10] = true;

				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (c.disconnected) {
							container.stop();
							return;
						}

						// if (!c.playerIsSkilling) {
						// container.stop();
						// return;
						// }
						if (!c.playerSkilling[10]) {
							container.stop();
							return;
						}

						if (!hasFishingEquipment(c, c.playerSkillProp[10][4])) {
							container.stop();
						}

						c.startAnimation(c.playerSkillProp[10][0]);

						if ((System.currentTimeMillis()
								- c.getFishingCooldown()) > (5000
										* Misc.random(5))) {
							if (c.playerSkillProp[10][5] > 0) {
								if (c.getPlayerLevel()[Skills.FISHING] >= c.playerSkillProp[10][6]) {
									c.playerSkillProp[10][1] = c.playerSkillProp[10][Misc.random(1) == 0 ? 7 : 5];
								}
							}
							if (c.playerSkillProp[10][1] > 0) {
								if (c.playerSkillProp[10][1] == 383) {
									c.getAchievement().fishSharks();
								} else if (c.playerSkillProp[10][1] == 389) {
									c.getAchievement().fishManta();
								} else if (c.playerSkillProp[10][1] == 15270) {
									c.getAchievement().catchRocktails();
								}
								c.sendMessage("You catch " + ItemConstants.getItemName(c.playerSkillProp[10][1]) + ".");
								c.getAchievement().fishFishes();

								c.getItems().addItem(c.playerSkillProp[10][1], 1);
								if (c.playerSkillProp[10][4] == 3159) {
									if (c.getItems().deleteItem2(3159, 1)) {
										c.getItems().addItem(3157, 1);
									} else {
										container.stop();
									}
								}
							}
							if (c.playerSkillProp[10][2] > 0) {
								if (c.inWild()) {
									c.getPA().addSkillXP((int)((double)(c.playerSkillProp[10][2] * 2) * calculateAnglerOutfitXpModifier(c)), Skills.FISHING);
								} else {
									c.getPA().addSkillXP((int)((double)c.playerSkillProp[10][2] * calculateAnglerOutfitXpModifier(c)), Skills.FISHING);
								}
							}
							if (c.playerSkillProp[10][3] > 0) {
								c.getItems().deleteItemInOneSlot(c.playerSkillProp[10][3], c.getItems().getItemSlot(c.playerSkillProp[10][3]), 1);
								if (!c.getItems().playerHasItem(c.playerSkillProp[10][3])) {
									c.sendMessage("You haven't got any " + ItemConstants.getItemName(c.playerSkillProp[10][3]) + " left!");
									c.sendMessage("You need " + ItemConstants.getItemName(c.playerSkillProp[10][3]) + " to fish here.");
									container.stop();
								}
							}
						}
						if (!hasInventorySpace(c, "fishing")) {
							container.stop();
						}
						if (Misc.random(140) == 0) {
							container.stop();
						}

					}

					@Override
					public void stop() {
						if (c != null && this != null) {
							resetFishing(c);
						}
					}
				}, getTimer(c, npcId) + 5 + playerFishingLevel(c));
			}
		}
	}

	public static void attemptdata(final Client c, int npcId, int npcIndex) {
		if (!hasInventorySpace(c, "fishing")) {
			c.sendMessage(
					"You must have space in your inventory to start fishing.");
			return;
		}
		// if (c.playerSkillProp[10][10] == npcIndex && c.lastFishingSpot[0] ==
		// c.getX() && c.lastFishingSpot[1] == c.getY()) {
		//// c.sendMess("fishing same fish");
		// return;
		// }
		final int uuid = Misc.generateRandomActionId();// final UUID uuid =
														// Misc.generateUUID();
		c.uniqueActionId = uuid;

		// resetFishing(c);
		for (int i = 0; i < data.length; i++) {
			if (npcId == data[i][0]) {
				if (c.getPlayerLevel()[Skills.FISHING] < data[i][1]) {
					c.sendMessage(
							"You haven't got high enough fishing level to fish here!");
					c.sendMessage("You at least need the fishing level of "
							+ data[i][1] + ".");
					c.getPA().sendStatement("You need the fishing level of "
							+ data[i][1] + " to fish here.");
					return;
				}
				if (data[i][3] > 0) {
					if (!c.getItems().playerHasItem(data[i][3])) {
						c.sendMessage("You haven't got any "
								+ ItemConstants.getItemName(data[i][3]) + "!");
						c.sendMessage("You need "
								+ ItemConstants.getItemName(data[i][3])
								+ " to fish here.");
						return;
					}
				}
				c.playerSkillProp[10][0] = data[i][6]; // ANIM id
				c.playerSkillProp[10][1] = data[i][4]; // FISH id
				c.playerSkillProp[10][2] = data[i][5]; // XP gain i guess
				c.playerSkillProp[10][3] = data[i][3]; // BAIT
				c.playerSkillProp[10][4] = data[i][2]; // EQUIP
				c.playerSkillProp[10][5] = data[i][7]; // sFish
				c.playerSkillProp[10][6] = data[i][8]; // sLvl
				c.playerSkillProp[10][7] = data[i][4]; // FISH
				c.playerSkillProp[10][8] = data[i][9]; // sXP
				c.playerSkillProp[10][9] = Misc.random(1) == 0 ? 7 : 5;
				c.playerSkillProp[10][10] = npcIndex;// npc Index now
														// //data[i][0]; //
														// INDEX
				c.playerSkillProp[10][11] = data[i][1]; // Lvl
				c.playerSkillProp[10][12] = data[i][10]; // clue rate

				if (!hasFishingEquipment(c, c.playerSkillProp[10][4])) {
					return;
				}

				// if (c.playerSkilling[10]) {
				// return;
				// }

				// c.turnPlayerTo(c.objectX, c.objectY); // fishing uses npcs
				// not objects lol objectX and objectY
				c.sendMessageSpam("You start fishing.");
				c.startAnimation(c.playerSkillProp[10][0]);
				c.playerIsSkilling = true;

				c.playerSkilling[10] = true;

				CycleEventHandler.getSingleton().addEvent(c,
						new CycleEvent() {

							@Override
							public void execute(CycleEventContainer container) {
								if (c.disconnected) {
									container.stop();
									return;
								}

								if (c.uniqueActionId != uuid) {
									container.stop();
									return;
								}

								if (!c.playerSkilling[10]) {
									container.stop();
									return;
								}
								c.startAnimation(c.playerSkillProp[10][0]);
							}

							@Override
							public void stop() {

							}

						}, 8);

				CycleEventHandler.getSingleton().addEvent(c,
						new CycleEvent() {

							@Override
							public void execute(CycleEventContainer container) {
								if (c.disconnected) {
									container.stop();
									return;
								}

								if (c.uniqueActionId != uuid) {
									container.stop();
									return;
								}

								// if (!c.playerIsSkilling) {
								// container.stop();
								// return;
								// }
								if (!c.playerSkilling[10]) {
									container.stop();
									return;
								}

								if (!hasFishingEquipment(c,
										c.playerSkillProp[10][4])) {
									container.stop();
									resetFishing(c);
									return;
								}

								if (NPCHandler.npcs[c.playerSkillProp[10][10]] == null
										|| NPCHandler.npcs[c.playerSkillProp[10][10]].isDead()) {
									resetFishing(c);
									container.stop();
									return;
								}

								// c.sendMess("Still fishing");

								if ((System.currentTimeMillis()
										- c.getFishingCooldown()) > (5000
												* Misc.random(5))) {
									if (c.playerSkillProp[10][5] > 0) {
										if (c.getPlayerLevel()[Skills.FISHING] >= c.playerSkillProp[10][6]
												&& Misc.random(99) < 50) {
											// c.playerSkillProp[10][1] =
											// c.playerSkillProp[10][Misc.random(1)
											// == 0 ? 7 : 5];
											c.playerSkillProp[10][1] = c.playerSkillProp[10][5];
											c.playerSkillProp[10][2] = c.playerSkillProp[10][8];
										}
									}
									if (c.playerSkillProp[10][1] > 0) {
										if (c.playerSkillProp[10][1] == 383) {
											c.getAchievement().fishSharks();
										} else if (c.playerSkillProp[10][1] == 389) {
											c.getAchievement().fishManta();
										} else if (c.playerSkillProp[10][1] == 15270) {
											c.getAchievement().catchRocktails();
										}
										c.sendMessageSpam("You catch "
														+ ItemConstants
																.getItemName(
																		c.playerSkillProp[10][1])
														+ ".");
										c.getAchievement().fishFishes();
										SkillingUrns.handleUrn(c, Skills.FISHING, c.playerSkillProp[10][11], c.playerSkillProp[10][2]);
										c.increaseProgress(TaskType.FISHING, c.playerSkillProp[10][1]);
									}
									if (c.playerSkillProp[10][1] > 0) {
										if (c.playerSkillProp[10][1] == 21520 && Misc.random(150 - c.getPlayerLevel()[Skills.FISHING]) == 1) {
											c.getItems().addItem(20429, 1);
										} else
										    c.getItems().addItem(c.playerSkillProp[10][1], 1);
										// c.startAnimation(c.playerSkillProp[10][0]);
										if (Misc.random(100) == 7 && !c
												.inFishingContestIsland()) { // random
																				// chance
																				// to
																				// making
																				// npc
																				// disappear
											if (NPCHandler.npcs[c.playerSkillProp[10][10]] != null) {
												NPCHandler.npcs[c.playerSkillProp[10][10]].setDead(true);
											}
										}
										
										//karambwan bait code
										if (c.playerSkillProp[10][4] == 3159) {
											if (c.getItems().deleteItem2(3150, 1)) {
												
											}
											else if (c.getItems().deleteItem2(3159,
													1)) {
												c.getItems().addItem(3157, 1);
											} else {
												resetFishing(c);
												container.stop();
											}
										}
									}
									if (c.playerSkillProp[10][2] > 0) {
										double xp = c.playerSkillProp[10][2] * calculateAnglerOutfitXpModifier(c);
										if (c.inWild()) {
											xp *= 2;
										}

										c.getPA().addSkillXP((int) xp, Skills.FISHING);
									}
									if (c.playerSkillProp[10][12] > 0) {
										c.getTreasureTrailManager().rollClueThingy(c.playerSkillProp[10][11], c.playerSkillProp[10][12], Skills.FISHING, "You catch a clue bottle!");
									}
									if (c.inFishingContestIsland()
											&& c.getItems().freeSlots() > 1) {
										if (FishingContest.isRunning()) {
											if (Misc.random(8000) == 1) {
												c.getItems().addOrBankItem(6202, 1);
												FishingContest
														.endFishingContest();
												PlayerHandler.messageAllPlayers(
														"<img=9> <shad=1><col=FF9933>"
																+ c.getNameSmartUp()
																+ " has caught the rare fish and won the fishing contest.!");
												c.getDH().sendStatement(
														"You have caught the Rare fish for the fishing contest!");
											} else if (Misc.random(300) == 1
													&& c.getDonatedTotalAmount() > 9) {
												c.getItems().addItem(405, 1);
												c.sendMessage(
														"You found a strange casket.");
											} else if (Misc.random(1000) == 1) {
												c.getItems().addItem(405, 1);
												c.sendMessage(
														"You found a strange casket.");
											}
										} else {
											c.sendMessage(
													"The rare fish has already been caught in this fishing contest.");
										}
									}

									if (c.playerSkillProp[10][3] > 0) {
										c.getItems().deleteItemInOneSlot(
												c.playerSkillProp[10][3],
												c.getItems().getItemSlot(
														c.playerSkillProp[10][3]),
												1);
										if (!c.getItems().playerHasItem(
												c.playerSkillProp[10][3])) {
											c.sendMessage("You haven't got any "
													+ ItemConstants.getItemName(
															c.playerSkillProp[10][3])
													+ " left!");
											c.sendMessage("You need "
													+ ItemConstants.getItemName(
															c.playerSkillProp[10][3])
													+ " to fish here.");
											container.stop();
											resetFishing(c);
										}
									}
								}
								if (!hasInventorySpace(c, "fishing")) {
									container.stop();
									resetFishing(c);
								}
								if (Misc.random(140) == 0) {
									container.stop();
									resetFishing(c);
								}

							}

							@Override
							public void stop() {
								// if (c != null && this != null)
								// resetFishing(c);
							}
						}, getTimer(c, npcId) + 5 + playerFishingLevel(c));
			}
		}
	}

	private final static int getTimer(Client c, int npcId) {
		switch (npcId) {
			case 1:
				return 2;
			case 2:
				return 3;
			case 3:
				return 4;
			case 4:
				return 4;
			case 5:
				return 4;
			case 6:
				return 5;
			case 7:
				return 5;
			case 8:
				return 5;
			case 9:
				return 5;
			case 10:
				return 5;
			case 11:
				return 9;
			case 12:
				return 9;
			case 13:
				return 10;
			default:
				return -1;
		}
	}

	private static boolean hasFishingEquipment(Client c, int equipment) {
		switch (equipment) {
			case 311:
				if (c.getPlayerLevel()[Skills.FISHING] >= 61 && (c.getItems().playerHasItem(30069) || c.getItems().playerHasEquippedWeapon(30069))) {
					return true;
				}

				if (c.getItems().playerHasItem(10129) || c.getItems().playerHasEquippedWeapon(10129)) {
					return true;
				}

				break;
		}

		if (c.getItems().playerHasItem(equipment)) {
			return true;
		}

		c.sendMessage("You need a " + ItemConstants.getItemName(equipment) + " to fish here.");
		return false;
	}

	public static boolean isFishingNPC(Client c, int clickType, int npcId,
			int npcIndex) {

		if (clickType == 1) {
			switch (npcId) {
				// FISHING
				case 319:
				case 329:
				case 323:
					// case 325:
				case 326:
				case 327:
				case 330:
				case 332:
				case 316: // NET + BAIT
					Fishing.attemptdata(c, 1, npcIndex);
					return true;
				case 325:
					Fishing.attemptdata(c, 12, npcIndex);
					return true;
				case 334:
				case 313: // NET + HARPOON
					Fishing.attemptdata(c, 3, npcIndex);
					return true;
				case 322: // NET + HARPOON
					Fishing.attemptdata(c, 5, npcIndex);
					return true;

				case 309: // LURE
				case 310:
				case 311:
				case 314:
				case 315:
				case 317:
				case 318:
				case 328:
				case 331:
					Fishing.attemptdata(c, 4, npcIndex);
					return true;

				case 312:
				case 321:
				case 324: // CAGE + HARPOON
					Fishing.attemptdata(c, 8, npcIndex);
					return true;

				case 1174: // karambwanji fishing spot
					Fishing.attemptdata(c, npcId, npcIndex);
					return true;
				case 1178: // karambwan fishing spot
					Fishing.attemptdata(c, npcId, npcIndex);
					return true;

				case 8842:// rocktail shoal
					Fishing.attemptdata(c, 13, npcIndex);
					return true;

			}

		} else if (clickType == 2) {
			switch (npcId) {

				case 326:
				case 327:
				case 330:
				case 332:
				case 316: // BAIT + NET
					Fishing.attemptdata(c, 2, npcIndex);
					return true;
				case 319:
				case 323:
				case 325: // BAIT + NET
					Fishing.attemptdata(c, 9, npcIndex);
					return true;
				case 310:
				case 311:
				case 314:
				case 315:
				case 317:
				case 318:
				case 328:
				case 329:
				case 331:
				case 309: // BAIT + LURE
					Fishing.attemptdata(c, 6, npcIndex);
					return true;
				case 312:
				case 321:
				case 324:// SWORDIES+TUNA-CAGE+HARPOON
					Fishing.attemptdata(c, 7, npcIndex);
					return true;
				case 313:
				case 322:
				case 334: // NET+HARPOON
					Fishing.attemptdata(c, 10, npcIndex);
					return true;
			}
		}

		return false;
	}

	public static int playerFishingLevel(Client c) {
		return (10 - (int) Math
				.floor(c.getPlayerLevel()[Skills.FISHING] / 10));
	}

	public static void resetFishing(Client c) {
		c.startAnimation(65535);
		c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
		c.playerIsSkilling = false;
		c.playerSkilling[10] = false;
		for (int i = 0; i < 11; i++) {
			c.playerSkillProp[10][i] = -1;
		}
		c.setFishingCooldown(System.currentTimeMillis());
	}
	
	public static double calculateAnglerOutfitXpModifier(Player player) {
		double modifier = 1.0;

		if (player.getItems().playerHasEquipped(anglerHat, PlayerConstants.playerHat)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(anglerTop, PlayerConstants.playerChest)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(anglerWaders, PlayerConstants.playerLegs)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(anglerBoots, PlayerConstants.playerFeet)) {
			modifier += 0.05;
		}
		return modifier;
	}

}
