package com.soulplay.content.player.skills.herblore;

import com.soulplay.content.player.skills.Skills;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.SkillHandler;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;

// Vavbro - Updated herblore.
public class Herblore extends SkillHandler {

	// ADD FELLSTALK
	public enum Herb {

		// HERB(herbName, new int[] { herbId, level req to clean, exp,
		// cleanHerbId, vialOfLiquid that makes unfinished, unfinished id,
		// amount to
		// make})
		SAGEWORT("Sagewort", 17494, 3, 2, 17512, 17492, 17538, 1),
		VALERIAN("Valerian", 17496, 4, 3, 17514, 17492, 17540, 1),
		ALOE("Aloe", 17498, 8, 4, 17516, 17492, 17542, 1),
		WORMWOOD("Wormwood leaf", 17500, 34, 7, 17518, 17492, 17544, 1),
		MAGEBANE("Magebane", 17502, 37, 7, 17520, 17492, 17546, 1),
		FEATHERFOIL("Featherfoil", 17504, 41, 8, 17522, 17492, 17548, 1),
		WINTERSGRIP("Winter's grip", 17506, 67, 12, 17524, 17492, 17550, 1),
		LYCOPUS("Lycopus", 17508, 70, 13, 17526, 17492, 17552, 1),
		BUCKTHORN("Buckthorn", 17510, 74, 13, 17528, 17492, 17554, 1),

		GUAM("Guam", 199, 1, 2, 249, 227, 91, 1),
		MARRENTILL("Marrentill", 201, 5, 4, 251, 227, 93, 1),
		TARROMIN("Tarromin", 203, 11, 5, 253, 227, 95, 1),
		HARRALANDER("Harralander", 205, 20, 6, 255, 227, 97, 1),
		RANARR("Ranarr", 207, 25, 7, 257, 227, 99, 1),
		TOADFLAX("Toadflax", 3049, 30, 8, 2998, 227, 3002, 1),
		SPIRIT_WEED("Sprit Weed",12174, 35, 8, 12172, 227, 12181, 1),
		IRIT("Irit", 209, 40, 9, 259, 227, 101, 1),
		WERGALI("Wergali", 14836, 41, 10, 14854, 227, 14856, 1),
		AVANTOE("Avantoe", 211, 48, 10, 261, 227, 103, 1),
		KWUARM("Kwuarm", 213, 54, 11, 263, 227, 105, 1),
		SNAPDRAGON("Snapdragon", 3051, 59, 12, 3000, 227, 3004, 1),
		CADANTINE("Cadantine", 215, 65, 12, 265, 227, 107, 1),
		LANTADYME("Lantadyme", 2485, 67, 13, 2481, 227, 2483, 1),
		DWARF_WEED("Dwarf Weed", 217, 70, 14, 267, 227, 109, 1),
		TORSTOL("Torstol", 219, 75, 15, 269, 227, 111, 1),
		ARDRIGAL("Ardrigal", 1527, 3, 2, 1528, 227, 738, 1),
		ROGUES_PURSE("Rogue's Purse",1533, 3, 2, 1534, 227, 4840, 1),
		SITO_FOIL("Sito Foil", 1529, 3, 2, 1530, -1, -1, -1),
		SNAKE_WEED("Snake Weed", 1525, 3, 2, 1526, 227, 737, 1),
		VOLENCIA_MOSS("Volencia Moss", 1531, 3, 2, 1532, -1, -1, -1),
		CACTUS_SPINE("Cactus Spine", -1, -1, -1, 6016, 5935, 5936, 1),
		CAVE_NIGHTSHADE("Cave Nightshade", -1, -1, -1, 2398, 5935, 5939, 1),
		FELLSTALK("Fellstalk", 21626, 91, 17, 21624, 227, 21628, 1);
		
		private final String name;
		private final int grimyId, levelToClean, cleanXp, cleanId, combinesWithId, makesId, amountToMake;

		private Herb(String name, int grimyId, int levelToClean, int cleanXp, int cleanId, int combinesWithId, int makesId, int amountToMake) {
			this.name = name;
			this.grimyId = grimyId;
			this.levelToClean = levelToClean;
			this.cleanXp = cleanXp;
			this.cleanId = cleanId;
			this.combinesWithId = combinesWithId;
			this.makesId = makesId;
			this.amountToMake = amountToMake;
		}

		public int getAmountToMake() {
			return amountToMake;
		}

		public int getCleanedID() {
			return cleanId;
		}

		public int getCleanExp() {
			return cleanXp;
		}

		public int getCombinesWithID() {
			return combinesWithId;
		}

		public int getHerbLevel() {
			return levelToClean;
		}

		public int getMakesID() {
			return makesId;
		}

		public String getName() {
			return name;
		}

		public int getUncleanedID() {
			return grimyId;
		}
		
		public static Herb getHerbForID(final int herbID, final boolean clean) {
			switch (herbID) {
				case 17494:
				case 17512:
					return SAGEWORT;
				case 17496:
				case 17514:
					return VALERIAN;
				case 17498:
				case 17516:
					return ALOE;
				case 17500:
				case 17518:
					return WORMWOOD;
				case 17502:
				case 17520:
					return MAGEBANE;
				case 17504:
				case 17522:
					return FEATHERFOIL;
				case 17506:
				case 17524:
					return WINTERSGRIP;
				case 17508:
				case 17526:
					return LYCOPUS;
				case 17510:
				case 17528:
					return BUCKTHORN;
				case 21626:
				case 21624:
					return FELLSTALK;
				case 249:
				case 199:
					return GUAM;
				case 251:
				case 201:
					return MARRENTILL;
				case 253:
				case 203:
					return TARROMIN;
				case 255:
				case 205:
					return HARRALANDER;
				case 257:
				case 207:
					return RANARR;
				case 2998:
				case 3049:
					return TOADFLAX;
				case 12172:
				case 12174:
					return SPIRIT_WEED;
				case 259:
				case 209:
					return IRIT;
				case 14854:
				case 14836:
					return WERGALI;
				case 261:
				case 211:
					return AVANTOE;
				case 263:
				case 213:
					return KWUARM;
				case 3000:
				case 3051:
					return SNAPDRAGON;
				case 265:
				case 215:
					return CADANTINE;
				case 2481:
				case 2485:
					return LANTADYME;
				case 267:
				case 217:
					return DWARF_WEED;
				case 269:
				case 219:
					return TORSTOL;
				case 1528:
				case 1527:
					return ARDRIGAL;
				case 1534:
				case 1533:
					return ROGUES_PURSE;
				case 1530:
				case 1529:
					return SITO_FOIL;
				case 1526:
				case 1525:
					return SNAKE_WEED;
				case 1532:
				case 1531:
					return VOLENCIA_MOSS;
				case 6016:
					return CACTUS_SPINE;
				case 2398:
					return CAVE_NIGHTSHADE;
				default:
					return null;
			}
		}
	}

	/**
	 * A list of ingredients that can be used.
	 */
	public enum Ingredient {

		// new Ingredient(ingredientId, name, new int[] { Combinable objects },
		// new int[] { levels required to combine, corresponds with objects },
		// new int[][] {{ new item, amount, exp }})
		VOID_DUST(17530, "Void dust",
				new int[] {17538, 17540, 17542, 17544, 17546, 17548, 17550, 17552, 17554},
				new int[] {3, 5, 9, 36, 38, 42, 69, 71, 75},
				new int[][] {{17556, 1, 21}, {17558, 1, 34}, {17562, 1, 41}, {17582, 1, 79}, {17584, 1, 83},
			{17588, 1, 89}, {17606, 1, 155}, {17608, 1, 160}, {17612, 1, 170}}),

		MISSHAPEN_CLAW(17532, "Misshapen claw",
				new int[] {17540, 17542, 17538, 17546, 17548, 17544, 17552, 17554, 17550},
				new int[] {7, 18, 30, 40, 48, 63, 73, 84, 96},
				new int[][] {{17560, 1, 37}, {17570, 1, 57}, {17578, 1, 72}, {17586, 1, 86}, {17594, 1, 105},
			{17602, 1, 139}, {17610, 1, 164}, {17618, 1, 189}, {17626, 1, 279}}),
		
		RED_MOSS(17534, "Red moss",
				new int[] {17542, 17538, 17540, 17548, 17544, 17546, 17554, 17550, 17552},
				new int[] {12, 24, 27, 45, 57, 60, 78, 90, 93},
				new int[][] {{17564, 1, 47}, {17574, 1, 65}, {17576, 1, 68}, {17590, 1, 93}, {17598, 1, 123},
			{17600, 1, 131}, {17614, 1, 173}, {17622, 1, 234}, {17624, 1, 253}}),
		
		FIREBREATH_WHISKEY(17536, "Firebreath whiskey",
				new int[] {17542, 17538, 17540, 17548, 17544, 17546, 17554, 17550, 17552},
				new int[] {15, 21, 33, 48, 54, 66, 81, 87, 99},
				new int[][] {{17568, 1, 53}, {17572, 1, 61}, {17580, 1, 75}, {17592, 1, 98}, {17596, 1, 114},
			{17604, 1, 147}, {17616, 1, 178}, {17620, 1, 205}, {17628, 1, 315}}),

		EYE_OF_NEWT(221, "Eye of Newt", new int[]{91, 101}, new int[]{1, 45},
				new int[][]{{121, 1, 25}, {145, 1, 100}}),

		UNICORN_HORN_DUST(235, "Unicorn Horn Dust", new int[]{93, 101},
				new int[]{5, 48}, new int[][]{{175, 1, 37}, {181, 1, 106}}),

		SNAKE_WEED(1526, "Snake Weed", new int[]{4840}, new int[]{8},
				new int[][]{{4844, 1, 40}}),

		LIMPWURT_ROOT(225, "Limpwurt Root", new int[]{95, 105},
				new int[]{12, 55}, new int[][]{{115, 1, 50}, {157, 1, 125}}),

		ASHES(592, "Ashes", new int[]{95}, new int[]{15},
				new int[][]{{3410, 1, 50}}),

		RED_SPIDERS_EGGS(223, "Red Spider's Eggs", new int[]{97, 3004, 5936},
				new int[]{22, 63, 73},
				new int[][]{{127, 1, 62}, {3026, 1, 142}, {5937, 1, 165}}),

		CHOCOLATE_DUST(1975, "Chocolate Dust", new int[]{97}, new int[]{26},
				new int[][]{{3010, 1, 67}}),

		TOADS_LEGS(2152, "Toad's Legs", new int[]{3002}, new int[]{34},
				new int[][]{{3034, 1, 80}}),

		GOAT_HORN_DUST(9736, "Goat Horn Dust", new int[]{97}, new int[]{36},
				new int[][]{{9741, 1, 84}}),

		SNAPE_GRASS(231, "Snape Grass", new int[]{99, 103}, new int[]{38, 50},
				new int[][]{{139, 1, 87}, {151, 1, 112}}),

		COCKATRICE_EGG(12109, "Cockatrice Egg", new int[]{12181}, new int[]{40},
				new int[][]{{12142, 1, 92}}),

		FROG_SPAWN(5004, "Frog Spawn", new int[]{14856}, new int[]{42},
				new int[][]{{14840, 1, 95}}),

		MORT_MYRE_FUNGUS(2970, "Mort Myre Fungus", new int[]{103},
				new int[]{52}, new int[][]{{3018, 1, 117}}),

		KEBBIT_TEETH_DUST(10111, "Kebbit Teeth Dust", new int[]{103},
				new int[]{53}, new int[][]{{10000, 1, 120}}),

		WIMPY_FEATHER(11525, "Wimpy Feather", new int[]{14856}, new int[]{58},
				new int[][]{{14848, 1, 132}}),

		DRAGON_SCALE_DUST(241, "Dragon Scale Dust", new int[]{105, 2483},
				new int[]{60, 69}, new int[][]{{187, 1, 137}, {2454, 1, 157}}),

		WHITE_BERRIES(239, "White Berries", new int[]{99, 107}, new int[]{30, 66},
				new int[][]{{133, 1, 75}, {163, 1, 150}}), 

		WINE_OF_ZAMORAK(245, "Wine of Zamorak", new int[]{109}, new int[]{72},
				new int[][]{{169, 1, 162}}),

		POTATO_CACTUS(3138, "Potato Cactus", new int[]{2483}, new int[]{76},
				new int[][]{{3042, 1, 172}}),

		JANGERBERRIES(247, "Jangerberries", new int[]{111}, new int[]{78},
				new int[][]{{189, 1, 175}}),

		CRUSHED_BIRD_NEST(6693, "Crushed Bird Nest", new int[]{3002},
				new int[]{81}, new int[][]{{6687, 1, 180}}),

		AVANTOE(261, "Avantoe", new int[]{145}, new int[]{88},
				new int[][]{{15309, 1, 220}}),

		DWARF_WEED(267, "Dwarf Weed", new int[]{157}, new int[]{89},
				new int[][]{{15313, 1, 230}}),

		LANTADYME(2481, "Lantadyme", new int[]{163}, new int[]{90},
				new int[][]{{15317, 1, 240}}),
		
		GRENWALL_SPIKES(12539, "Grenwall spikes", new int[]{169}, new int[]{92},
				new int[][]{{15325, 1, 260}}),

		GROUND_MUD_RUNES(9594, "Ground mud runes", new int[]{3042},
				new int[]{91}, new int[][]{{15321, 1, 250}}),

		MORCHELLA_MUSHROOM(21622, "Morchella mushroom", new int[]{21628},
				new int[]{94}, new int[][]{{21632, 1, 190}}),

		BONEMEAL(18834, "Bonemeal", new int[]{139}, new int[]{94},
				new int[][]{{15329, 1, 270}}),

		PAPAYA(5972, "Papaya", new int[]{3018}, new int[]{84},
				new int[][]{{15301, 1, 200}}),
		
		ZULRAH_SCALES(30005, "Zulrah's Scales", new int[]{2446, 2448}, new int[]{79, 79},
				new int[][]{{5943, 1, 180}, {5943, 1, 180}})
		;
		
		private final int id;

		private final String name;

		private final int[] combinesWith;

		private final int[] levelReqs;

		private final int[][] makes;

		private Ingredient(final int id, final String name,
				final int[] combinesWith, final int[] levelReqs,
				final int[][] makes) {
			this.id = id;
			this.name = name;
			this.combinesWith = combinesWith;
			this.levelReqs = levelReqs;
			this.makes = makes;
		}

		public boolean combinesWith(final int ITEM_ID) {
			for (int element : combinesWith) {
				if (element == ITEM_ID) {
					return true;
				}
			}
			return false;
		}

		public int getID() {
			return id;
		}

		public int getLevelForID(final int ITEM_ID) {
			for (int i = 0; i < combinesWith.length; i++) {
				if (combinesWith[i] == ITEM_ID) {
					return levelReqs[i];
				}
			}
			return 0;
		}

		public String getName() {
			return name;
		}

		public int[] getToMake(final int ITEM_ID) {
			for (int i = 0; i < combinesWith.length; i++) {
				if (combinesWith[i] == ITEM_ID) {
					return makes[i];
				}
			}
			return new int[]{-1};
		}

	}
	
	private static final Map<Integer, Ingredient> ingredients = new HashMap<>();
	
	public static Ingredient getIngredientForID(final int ingredientID) {
		return ingredients.get(ingredientID);
	}
	
	public static boolean isHerbloreIngredient(int itemId) {
		return ingredients.containsKey(itemId);
	}
	
	public static void addTime(Client c) {
		c.doAmount--;
	}

	public static boolean areOverloadComponents(int item1, int item2) {
		if (item1 == 269) {
			return item2 == 15309 || item2 == 15313 || item2 == 15317
					|| item2 == 15321 || item2 == 15325;
		} else if (item2 == 269) {
			return item1 == 15309 || item1 == 15313 || item1 == 15317
					|| item1 == 15321 || item1 == 15325;
		}
		return false;
	}

	/**
	 * Cleans herbs. 'Nuf said.
	 *
	 * @param c
	 *            client requesting clean.
	 * @param herb
	 *            index of the herb object.
	 */
	public static boolean cleanHerb(Client c, final int id, final int slot) {
		Herb herb = Herb.getHerbForID(id, true);
		if (herb == null) {
			return false;
		}

		if (c.getPlayerLevel()[Skills.HERBLORE] < herb.getHerbLevel()) {
			c.sendMessage("You need a Herblore level of " + herb.getHerbLevel() + " to clean this herb.");
			return true;
		}

		c.getItems().deleteItemInOneSlot(herb.getUncleanedID(), slot, 1);
		c.getItems().addItem(herb.getCleanedID(), 1);
		c.getPA().addSkillXP(herb.getCleanExp(), Skills.HERBLORE);
		c.sendMessageSpam("You clean the Grimy " + herb.getName() + ".");
		return true;
	}

	private static void deleteOverloadComponents(Client c) {
		if (hasOverloadComponents(c)) {
			c.getItems().deleteItemInOneSlot(15309, 1); // Extreme attack
			c.getItems().deleteItemInOneSlot(15313, 1); // Extreme strength
			c.getItems().deleteItemInOneSlot(15317, 1); // Extreme defence
			c.getItems().deleteItemInOneSlot(15321, 1); // Extreme magic
			c.getItems().deleteItemInOneSlot(15325, 1); // Extreme ranging
			c.getItems().deleteItemInOneSlot(269, 1); // Torsol
		}
	}

	/**
	 * Find an herb based on the item id and whether or not the id is of a clean
	 * or grimy herb.
	 *
	 * @param HERB_ID
	 *            id of the items that you want to find the herb of (herb's id).
	 * @return herb that meets the requirements, null if none are present.
	 */
	public static Herb getHerbForID(final int herbID) {
		Herb herb = Herb.getHerbForID(herbID, true);
		if (herb == null) {
			herb = Herb.getHerbForID(herbID, false);
		}
		return herb;
	}

	/**
	 * Handles the making of finished and unfinished potions.
	 *
	 * @param c
	 *            client requesting the check.
	 * @param item1
	 *            the first item id.
	 * @param item2
	 *            the second item id.
	 */
	public static void handlePotionMaking(Client c, final int amount,
			final int item1, final int item2) {
		if (c.getBarbarianAssault().inLobby()) {
			c.sendMessage("You cannot create potions in the lobby.");
			c.getPA().closeAllWindows();
			return;
		}

		if (isCleanHerb(item1)) {
			Herb herb = Herb.getHerbForID(item1, true);
			if (item2 == herb.getCombinesWithID()) {
				makeUnfinishedPotion(c, amount, herb);
				return;
			} else if (isIngredient(item2)) {
				if (getIngredientForID(item2).combinesWith(item1)) {
					makePotion(c, amount, item1, item2);
					return;
				}
			}
		}
		if (isCleanHerb(item2)) {
			Herb herb = Herb.getHerbForID(item2, true);
			if (item1 == herb.getCombinesWithID()) {
				makeUnfinishedPotion(c, amount, herb);
				return;
			} else if (isIngredient(item1)) {
				if (getIngredientForID(item1).combinesWith(item2)) {
					makePotion(c, amount, item2, item1);
					return;
				}
			}
		}
		if (isIngredient(item1)
				&& getIngredientForID(item1).combinesWith(item2)) {
			makePotion(c, amount, item2, item1);
			return;
		}
		if (isIngredient(item2)
				&& getIngredientForID(item2).combinesWith(item1)) {
			makePotion(c, amount, item1, item2);
			return;
		}
		if (item1 == -2 && item2 == -2) {
			makeOverloads(c, amount);
		}
	}

	public static boolean hasOverloadComponents(Client c) {
		return c.getItems().playerHasItem(15309)
				&& c.getItems().playerHasItem(15313)
				&& c.getItems().playerHasItem(15317)
				&& c.getItems().playerHasItem(15321)
				&& c.getItems().playerHasItem(15325)
				&& c.getItems().playerHasItem(269);
	}

	/**
	 * Checks to see whether the given item is a clean herb.
	 *
	 * @param ITEM_ID
	 *            the item to check's id.
	 * @return true if a clean herb shares the same id, false if not.
	 */
	public static boolean isCleanHerb(final int herbID) {
		return Herb.getHerbForID(herbID, true) != null;
	}

	public static boolean isHerb(int item) {
		return isCleanHerb(item) || isUncleanedHerb(item);
	}

	/**
	 * Checks to see whether the given item is an ingredient.
	 *
	 * @param ITEM_ID
	 *            item to check's id.
	 * @return true if an ingredient shares the same id, false if not.
	 */
	public static boolean isIngredient(final int ingredientID) {
		return getIngredientForID(ingredientID) != null;
	}

	/**
	 * Checks to see if the given id is of an unclean herb.
	 *
	 * @param ITEM_ID
	 *            item to check's id.
	 * @return true if an unclean herb shares the id, false if not.
	 */
	public static boolean isUncleanedHerb(final int herbID) {
		return Herb.getHerbForID(herbID, false) != null;
	}

	private static void makeOverloads(final Client c, final int amount) {
		final int xp = 1000;
		if (hasOverloadComponents(c)) {
			if (c.getPlayerLevel()[Skills.HERBLORE] >= 96) {
				if (!hasOverloadComponents(c) || amount <= 0) {
					return;
				}
				if (c.playerIsSkilling) {
					return;
				}
				c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
				c.playerIsSkilling = true;
				c.startAnimation(363);
				c.getPA().addSkillXP(xp, Skills.HERBLORE);
				deleteOverloadComponents(c);
				c.getItems().addItem(15333, 1); // Add OVL
				c.getItems().addItem(229, 4); // Add empty vials
				c.increaseProgress(TaskType.HERBLORE, 15333);
				c.sendMessageSpam(
						"You combine the potions to make an Overload.");
				c.getAchievement().makeOverloads();
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					int thisAmount = amount - 1;

					@Override
					public void execute(CycleEventContainer e) {
						if (!hasOverloadComponents(c) || thisAmount <= 0) {
							e.stop();
							return;
						}
						if (!c.playerIsSkilling) {
							e.stop();
							return;
						}
						if (thisAmount > 1) {
							c.startAnimation(363);
						}
						c.getPA().addSkillXP(xp,
								Skills.HERBLORE);
						deleteOverloadComponents(c);
						c.getItems().addItem(15333, 1); // Add OVL
						c.getItems().addItem(229, 4); // Add empty vials
						c.increaseProgress(TaskType.HERBLORE, 15333);
						c.sendMessageSpam(
								"You combine the potions to make an Overload.");
						c.getAchievement().makeOverloads();
						thisAmount--;
					}

					@Override
					public void stop() {
						if (c != null) {
							resetHerblore(c);
						}
					}
				}, 2);
			} else {
				c.sendMessage(
						"You need an Herblore level of 96 or greater to make this potion.");
				c.getPA().sendStatement(
						"You need an Herblore level of 96 or greater to make this potion.");
			}
		}
	}

	private static void makePotion(final Client c, final int amount,
			final int itemID, final int ingredientID) {
		final Ingredient ingredient = getIngredientForID(ingredientID);
		int[] getToMake = ingredient.getToMake(itemID);
		final int xp = getToMake[2];
		if (c.getItems().playerHasItem(itemID)
				&& c.getItems().playerHasItem(ingredientID)) {
			if (c.getPlayerLevel()[Skills.HERBLORE] >= ingredient
					.getLevelForID(itemID)) {
				if (!c.getItems().playerHasItem(itemID, 1)
						|| !c.getItems().playerHasItem(ingredientID, 1)
						|| amount <= 0) {
					return;
				}
				if (c.playerIsSkilling) {
					return;
				}
				
				if (getToMake[0] == 17626) {
					c.getAchievement().make50StrongNatural();
				} else if (getToMake[0] == 17628) {
					c.getAchievement().make50StrongSurvival();
				} else if (getToMake[0] == 17624) {
					c.getAchievement().make50StrongArtisan();
				} else if (getToMake[0] == 17622) {
					c.getAchievement().make50StrongGatherer();
				}
				c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
				c.playerIsSkilling = true;
				c.startAnimation(363);
				c.getPA().addSkillXP(xp, Skills.HERBLORE);
				c.getItems().deleteItemInOneSlot(itemID,
						c.getItems().getItemSlot(itemID), 1);
				c.getItems().deleteItemInOneSlot(ingredientID,
						c.getItems().getItemSlot(ingredientID), 1);
				c.getItems().addItem(getToMake[0],
						getToMake[1]);
				c.increaseProgress(TaskType.HERBLORE, getToMake[0]);
				c.sendMessageSpam("You mix the "
						+ ingredient.getName() + " into the potion.");
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					int thisAmount = amount - 1;

					@Override
					public void execute(CycleEventContainer e) {
						if (!c.getItems().playerHasItem(itemID, 1)
								|| !c.getItems().playerHasItem(ingredientID, 1)
								|| thisAmount <= 0) {
							e.stop();
							return;
						}
						if (!c.playerIsSkilling) {
							e.stop();
							return;
						}
						if (thisAmount > 1) {
							c.startAnimation(363);
						}
						c.getPA().addSkillXP(xp,
								Skills.HERBLORE);
						c.getItems().deleteItemInOneSlot(itemID,
								c.getItems().getItemSlot(itemID), 1);
						c.getItems().deleteItemInOneSlot(ingredientID,
								c.getItems().getItemSlot(ingredientID), 1);
						c.getItems().addItem(getToMake[0],
								getToMake[1]);
						c.increaseProgress(TaskType.HERBLORE, getToMake[0]);
						c.sendMessageSpam("You mix the "
								+ ingredient.getName() + " into the potion.");
						if (getToMake[0] == 17626) {
							c.getAchievement().make50StrongNatural();
						} else if (getToMake[0] == 17628) {
							c.getAchievement().make50StrongSurvival();
						} else if (getToMake[0] == 17624) {
							c.getAchievement().make50StrongArtisan();
						} else if (getToMake[0] == 17622) {
							c.getAchievement().make50StrongGatherer();
						}
						thisAmount--;
					}

					@Override
					public void stop() {
						if (c != null) {
							resetHerblore(c);
						}
					}
				}, 2);
			} else {
				c.sendMessage("You need an Herblore level of "
						+ ingredient.getLevelForID(itemID)
						+ " or greater to make this potion.");
				c.getPA()
						.sendStatement("You need an Herblore level of "
								+ ingredient.getLevelForID(itemID)
								+ " or greater to make this potion.");
			}
		}
	}

	/**
	 * Handles unfinished potion making.
	 *
	 * @param c
	 *            client requesting make.
	 * @param herb
	 *            herb that is being mixed.
	 */
	private static void makeUnfinishedPotion(final Client c, final int amount,
			final Herb herb) {
		if (!c.getItems().playerHasItem(herb.getCombinesWithID())
				|| !c.getItems().playerHasItem(herb.getCleanedID())
				|| amount <= 0) {
			return;
		}
		if (c.playerIsSkilling) {
			return;
		}
		c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
		c.playerIsSkilling = true;
		c.startAnimation(363);
		c.getPA().addSkillXP(1, Skills.HERBLORE);
		c.getItems().deleteItemInOneSlot(herb.getCleanedID(),
				c.getItems().getItemSlot(herb.getCleanedID()), 1);
		c.getItems().deleteItemInOneSlot(herb.getCombinesWithID(),
				c.getItems().getItemSlot(herb.getCombinesWithID()), 1);
		c.getItems().addItem(herb.getMakesID(), herb.getAmountToMake());
		c.sendMessageSpam(
				"You mix the " + herb.getName() + " into the solution.");
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int thisAmount = amount - 1;

			@Override
			public void execute(CycleEventContainer e) {
				if (!c.getItems().playerHasItem(herb.getCombinesWithID())
						|| !c.getItems().playerHasItem(herb.getCleanedID())
						|| thisAmount <= 0) {
					e.stop();
					return;
				}
				if (!c.playerIsSkilling) {
					e.stop();
					return;
				}
				if (thisAmount > 1) {
					c.startAnimation(363);
				}
				c.getPA().addSkillXP(1, Skills.HERBLORE);
				c.getItems().deleteItemInOneSlot(herb.getCleanedID(),
						c.getItems().getItemSlot(herb.getCleanedID()), 1);
				c.getItems().deleteItemInOneSlot(herb.getCombinesWithID(),
						c.getItems().getItemSlot(herb.getCombinesWithID()), 1);
				c.getItems().addItem(herb.getMakesID(), herb.getAmountToMake());
				c.sendMessageSpam("You mix the "
						+ herb.getName() + " into the solution.");
				thisAmount--;
			}

			@Override
			public void stop() {
				if (c != null) {
					resetHerblore(c);
				}
			}
		}, 2);
	}

	/**
	 * Checks to see if the two items can combine to make a potion.
	 *
	 * @param item1
	 *            The first item.
	 * @param item2
	 *            The second item.
	 */
	public static boolean potionCombo(int item1, int item2) {
		if (isIngredient(item1)) {
			return getIngredientForID(item1).combinesWith(item2);
		} else if (isIngredient(item2)) {
			return getIngredientForID(item2).combinesWith(item1);
		}
		return areOverloadComponents(item1, item2);
	}

	public static void resetHerblore(Client c) {
		c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
		c.playerIsSkilling = false;
		c.herbloreItem1 = -1;
		c.herbloreItem2 = -1;
	}

	public static void setUpPotionMaking(Client c, final int ITEM_1,
			final int ITEM_2) {
		if (isCleanHerb(ITEM_1)) {
			Herb herb = Herb.getHerbForID(ITEM_1, true);
			if (ITEM_2 == herb.getCombinesWithID()) {
				c.getPacketSender().sendChatInterface(4429);
				c.getPacketSender().sendFrame246(1746, 190, herb.getMakesID());
				c.getPacketSender().sendFrame126(getLine() + ""
						+ ItemConstants.getItemName(herb.getMakesID()) + "",
						2799); // Get line?
				c.herbloreItem1 = ITEM_1;
				c.herbloreItem2 = ITEM_2;
				return;
			} else if (isIngredient(ITEM_2)) {
				if (getIngredientForID(ITEM_2).combinesWith(ITEM_1)) {
					c.getPacketSender().sendChatInterface(4429);
					c.getPacketSender().sendFrame246(1746, 190,
							getIngredientForID(ITEM_2).getToMake(ITEM_1)[0]);
					c.getPacketSender().sendFrame126(
							getLine() + ""
									+ ItemConstants.getItemName(
											getIngredientForID(ITEM_2)
													.getToMake(ITEM_1)[0])
									+ "",
							2799);
					c.herbloreItem1 = ITEM_1;
					c.herbloreItem2 = ITEM_2;
					return;
				}
			}
		}
		if (isCleanHerb(ITEM_2)) {
			Herb herb = Herb.getHerbForID(ITEM_2, true);
			if (ITEM_1 == herb.getCombinesWithID()) {
				c.getPacketSender().sendChatInterface(4429);
				c.getPacketSender().sendFrame246(1746, 190, herb.getMakesID());
				c.getPacketSender().sendFrame126(getLine() + ""
						+ ItemConstants.getItemName(herb.getMakesID()) + "",
						2799); // Get line?
				c.herbloreItem1 = ITEM_2;
				c.herbloreItem2 = ITEM_1;
				return;
			} else if (isIngredient(ITEM_1)) {
				if (getIngredientForID(ITEM_1).combinesWith(ITEM_2)) {
					c.getPacketSender().sendChatInterface(4429);
					c.getPacketSender().sendFrame246(1746, 190,
							getIngredientForID(ITEM_1).getToMake(ITEM_2)[0]);
					c.getPacketSender().sendFrame126(
							getLine() + ""
									+ ItemConstants.getItemName(
											getIngredientForID(ITEM_1)
													.getToMake(ITEM_1)[0])
									+ "",
							2799);
					c.herbloreItem1 = ITEM_2;
					c.herbloreItem2 = ITEM_1;
					return;
				}
			}
		}
		if (isIngredient(ITEM_1)
				&& getIngredientForID(ITEM_1).combinesWith(ITEM_2)) {
			c.getPacketSender().sendChatInterface(4429);
			c.getPacketSender().sendFrame246(1746, 190,
					getIngredientForID(ITEM_1).getToMake(ITEM_2)[0]);
			c.getPacketSender()
					.sendFrame126(getLine() + "" + ItemConstants.getItemName(
							getIngredientForID(ITEM_1).getToMake(ITEM_2)[0])
							+ "", 2799);
			c.herbloreItem1 = ITEM_2;
			c.herbloreItem2 = ITEM_1;
			return;
		}
		if (isIngredient(ITEM_2)
				&& getIngredientForID(ITEM_2).combinesWith(ITEM_1)) {
			c.getPacketSender().sendChatInterface(4429);
			c.getPacketSender().sendFrame246(1746, 190,
					getIngredientForID(ITEM_2).getToMake(ITEM_1)[0]);
			c.getPacketSender()
					.sendFrame126(getLine() + "" + ItemConstants.getItemName(
							getIngredientForID(ITEM_2).getToMake(ITEM_1)[0])
							+ "", 2799);
			c.herbloreItem1 = ITEM_1;
			c.herbloreItem2 = ITEM_2;
			return;
		}
		if (areOverloadComponents(ITEM_1, ITEM_2)) {
			if (hasOverloadComponents(c)) {
				c.getPacketSender().sendChatInterface(4429);
				c.getPacketSender().sendFrame246(1746, 190, 15333);
				c.getPacketSender().sendFrame126(getLine() + "Overload (3)",
						2799);
				c.herbloreItem1 = -2; // -2 == Overload
				c.herbloreItem2 = -2; // -2 == Overload
			} else {
				c.sendMessage(
						"You need one of each extreme potion and a clean torstol to craft this.");
			}
		}
	}

	/**
	 * Checks to see if the two items can combine to make an unf. potion.
	 *
	 * @param item1
	 *            The first item.
	 * @param item2
	 *            The second item.
	 */
	public static boolean unfinishedPotCombo(int item1, int item2) {
		if (isCleanHerb(item1)) {
			return getHerbForID(item1).getCombinesWithID() == item2;
		} else if (isCleanHerb(item2)) {
			return getHerbForID(item2).getCombinesWithID() == item1;
		}
		return false;
	}

	/**
	 * Private constructor so that it can't be instantiated, and so that all
	 * users will have to use the same instance, saving memory.
	 */
	private Herblore() {

	}
	
	//contains clean and unclear herbs
	public static final Map<Integer, Herb> herbMap = new HashMap<>();
	
	public static void init() {
		for (Ingredient ing : Ingredient.values()) {
			ingredients.put(ing.getID(), ing);
		}
		for (Herb h : Herb.values()) {
			herbMap.put(h.getCleanedID(), h);
			herbMap.put(h.getUncleanedID(), h);
		}
	}

	public static Herb getAnyHerb(int itemId) {
		return herbMap.get(itemId);
	}
}
