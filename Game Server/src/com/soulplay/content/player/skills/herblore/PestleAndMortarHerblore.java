package com.soulplay.content.player.skills.herblore;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;

public class PestleAndMortarHerblore {
	
	public static final Animation PESTLE_ANIM = new Animation(364);

	public static void grindIngredient(Player p, Item horn, PestleEnum pestle) {
		if (p.getItems().playerHasItem(horn.getId(), horn.getSlot(), 1)) {
			p.startAnimation(PESTLE_ANIM);
			if (p.getItems().deleteItem2(horn.getId(), 1)) {
				p.getItems().addItem(pestle.getResult(), 1);
				p.sendMessageSpam("You grind the item.");
			}
		}
	}
}
