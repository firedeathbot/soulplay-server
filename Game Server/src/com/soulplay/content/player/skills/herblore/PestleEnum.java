package com.soulplay.content.player.skills.herblore;

import java.util.HashMap;
import java.util.Map;

public enum PestleEnum {

	UNICORN_HORN(237,235), //Unicorn horn
	CHOCOLATE(1973,1975), //
	KEBBIT_TEETH(10109, 10111), //Kebbit teeth dust
	GORAK_CLAW(9016, 9018), //Gorak claw
	NESTS(5075, 6693), //Nests
	DESERT_GOAT_HORN(9735, 9738), //Desert Goat horn
	BLUE_DRAGON_SCALE(243, 241), //Blue dragon scale
	GROUND_MUD_RUNES(4698, 9594), //Ground mud runes
	ASHES(592, 8865), //Ground ashes
	SEAWEED(401, 6683), //Seaweed
	EDIBLE_SEAWEED(403, 6683), //Seaweed
	BAT_BONES(530, 2391), //Bat bones
	CHARCOAL(973, 704), //Charcoal
	COD(341, 7528), //Cod
	KELP(7516, 7517), //Kelp
	CRABMEAT(7518, 7527), //Crabmeat
	RUNE_SHARDS(6466, 6467), //Rune shards
	GROUND_ASTRAL_RUNES(11156, 11155), //Ground astral runes
	SQUASH_TOOTH(9079, 9082), //Squah tooth
	DRIED_THISLE(3263, 3264), //Dried thisle
	GARLIC(1550, 4698), //Garlic
	BLACK_MUSHROOM(4620, 4622), //Black mushroom
	//WHITE_BERRIES(239, 299) // grinding white berries gives mithril seeds :P
	;

	private final int ing;
	private final int result;
	
	private PestleEnum(int item1, int groundedItem) {
		this.ing = item1;
		this.result = groundedItem;
	}

	public int getIngredient() {
		return ing;
	}

	public int getResult() {
		return result;
	}
	
	private static final Map<Integer, PestleEnum> map = new HashMap<>();
	
	public static void init() {
		for (PestleEnum p : PestleEnum.values()) {
			map.put(p.getIngredient(), p);
		}
	}
	
	public static PestleEnum get(int id) {
		return map.get(id);
	}
}
