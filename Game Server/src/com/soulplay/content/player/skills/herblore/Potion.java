package com.soulplay.content.player.skills.herblore;

import com.soulplay.cache.ItemDef;

public enum Potion {

	STRENGTH(113, 115, 117, 119),
	ATTACK(2428, 121, 123, 125),
	RESTORE(2430, 127, 129, 131),
	DEFENCE(2432, 133, 135, 137),
	PRAYER(2434, 139, 141, 143),
	FISHING(2438, 151, 153, 155),
	RANGING(2444, 169, 171, 173),
	ANTIFIRE(2452, 2454, 2456, 2458),
	ENERGY(3008, 3010, 3012, 3014),
	AGILITY(3032, 3034, 3036, 3038),
	MAGIC(3040, 3042, 3044, 3046),
	COMBAT(9739, 9741, 9743, 9745),
	SUMMONING(12140, 12142, 12144, 12146),
	SUPER_ATTACK(2436, 145, 147, 149),
	SUPER_STRENGTH(2440, 157, 159, 161),
	SUPER_DEFENCE(2442, 163, 165, 167),
	SUPER_ENERGY(3016, 3018, 3020, 3022),
	SUPER_RESTORE(3024, 3026, 3028, 3030),
	OVERLOAD(15332, 15333, 15334, 15335),
	SARADOMIN_BREW(6685, 6687, 6689, 6691),
	ANTIPOISON1(5943, 5945, 5947, 5949),
	ANTIPOISON2(5952, 5954, 5956, 5958),
	SUPER_ANTIPOISON(2448, 181, 183, 185),
	EXTR_RANGE(15324, 15325, 15326, 15327),
	EXTR_STR(15312, 15313, 15314, 15315),
	EXTR_MAGE(15320, 15321, 15322, 15323),
	EXTR_ATK(15308, 15309, 15310, 15311),
	EXTR_DEF(15316, 15317, 15318, 15319),
	SUPER_PRAYER(15328, 15329, 15330, 15331),
	SUPER_FIRE(15304, 15305, 15306, 15307),
	REC_SPEC(15300, 15301, 15302, 15303),
	ZAMORAK_BREW(2450, 189, 191, 193),
	ANTIPOISON(2446, 175, 177, 179),
	PRAYER_RENEWAL(21630, 21632, 21634, 21636),
	HUNTER(9998, 10000, 10002, 10004),
	COMPOST(6470, 6472, 6474, 6476),
	CRAFTING(14838, 14840, 14842, 14844),
	FLETCHING(14846, 14848, 14850, 14852);

	public static final Potion[] values = Potion.values();
	private final int dose4, dose3, dose2, dose1;
	private final int dose4Cert, dose3Cert, dose2Cert, dose1Cert;

	Potion(int dose4, int dose3, int dose2, int dose1) {
		this.dose4 = dose4;
		this.dose3 = dose3;
		this.dose2 = dose2;
		this.dose1 = dose1;
		this.dose4Cert = ItemDef.forID(dose4).noteId;
		this.dose3Cert = ItemDef.forID(dose3).noteId;
		this.dose2Cert = ItemDef.forID(dose2).noteId;
		this.dose1Cert = ItemDef.forID(dose1).noteId;
		if (this.dose4Cert == -1 || this.dose3Cert == -1 || this.dose2Cert == -1 || this.dose1Cert == -1) {
			System.out.println("NO NOTE FOR " + toString());
		}
	}

	public int getDose4() {
		return dose4;
	}

	public int getDose3() {
		return dose3;
	}

	public int getDose2() {
		return dose2;
	}

	public int getDose1() {
		return dose1;
	}

	public int getDose4Cert() {
		return dose4Cert;
	}

	public int getDose3Cert() {
		return dose3Cert;
	}

	public int getDose2Cert() {
		return dose2Cert;
	}

	public int getDose1Cert() {
		return dose1Cert;
	}

}