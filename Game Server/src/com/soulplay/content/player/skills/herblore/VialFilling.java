package com.soulplay.content.player.skills.herblore;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;

public class VialFilling {

	public static final int NORMAL_EMPTY_VIAL = 229;
	public static final int NORMAL_FILLED_VIAL = 227;
	public static final int DUNG_EMPTY_VIAL = 17490;
	public static final int DUNG_FILLED_VIAL = 17492;

	public static void vialFilling(Client player) {
		if (!(player.getItems().playerHasItem(DUNG_EMPTY_VIAL) || player.getItems().playerHasItem(NORMAL_EMPTY_VIAL))) {
			player.sendMessage("You need empty vial to fill it with water.");
			return;
		}

		if (player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				player.startAnimation(832);
				player.sendMessage("You fill your vial of water.");
				if (player.getItems().playerHasItem(NORMAL_EMPTY_VIAL)) {
					player.getItems().deleteItemInOneSlot(NORMAL_EMPTY_VIAL, 1);
					player.getItems().addItem(NORMAL_FILLED_VIAL, 1);
				} else {
					player.getItems().deleteItemInOneSlot(DUNG_EMPTY_VIAL, 1);
					player.getItems().addItem(DUNG_FILLED_VIAL, 1);
				}

				if (!(player.getItems().playerHasItem(DUNG_EMPTY_VIAL)
						|| player.getItems().playerHasItem(NORMAL_EMPTY_VIAL))) {
					player.resetAnimations();
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 2);
	}

}
