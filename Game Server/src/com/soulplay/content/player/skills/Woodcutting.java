package com.soulplay.content.player.skills;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.dailytasks.TaskConstants;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.farming.patch.Patch;
import com.soulplay.content.player.skills.farming.patch.State;
import com.soulplay.content.player.skills.woodcutting.Hatchets;
import com.soulplay.content.player.skills.woodcutting.Trees;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

public class Woodcutting extends SkillHandler {

	private static final int lumberJackHat = 10941;
	private static final int lumberJackBoots = 10933;
	private static final int lumberJackLegs = 10940;
	private static final int lumberJackTop = 10939;

	public static boolean initFarmingWc(final Client c, final int objectId, final int obX, final int obY, final Patch patch) {
		return attemptData(c, objectId, obX, obY, null, patch);
	}

	public static boolean initNormalWc(final Client c, final GameObject o) {
		return attemptData(c, o.getId(), o.getX(), o.getY(), o, null);
	}

	private static boolean attemptData(final Client c, final int objectId, final int obX, final int obY, final GameObject o, final Patch patch) {
		// filter spam clicking
		if (c.skillingEvent != null && c.prevObjectX == obX && c.prevObjectY == obY
				&& c.prevObjectId == objectId) {
			return true;
		}

		final Trees tree = Trees.getTree(objectId);
		if (tree == null) {
			return false;
		}

		ObjectDef def = null;
		if (o == null) {
			def = ObjectDef.getObjectDef(objectId);
		} else {
			def = o.getDef();
		}

		if (def == null) {
			c.sendMessage("Tree def is null");
			return true;
		}

		if (def.name == null) {
			System.out.println("Null tree? ID:" + objectId);
			return true;
		}

		if (!hasInventorySpace(c, "woodcutting")) {
			c.resetAnimations();
			return true;
		}
		
		final Hatchets axe = getHatchet(c);
		
		if (axe == null) {
			return true;
		}
		
		final int respawnTime = tree.getRespawnTime();

		final Animation anim = axe.getAnim();

		if (!hasRequiredLevel(c, 8, tree.getLvlReq(), "woodcutting",
				"cut this tree")) {
			c.resetAnimations();
			return true;
		}

		if (patch != null) {
			Misc.faceObject(c, obX, obY, def.sizeY);
		}

		if (tree == Trees.IVY) {
			c.sendMessageSpam("You swing your hatchet at the ivy.");
		} else {
			c.sendMessageSpam("You swing your hatchet at the tree.");
		}

		final int stumpId = getStump(def.name, objectId);
		
		c.startAnimation(axe.getStartAnim());

		c.startAction(new CycleEvent() {
			
			@Override
			public void execute(CycleEventContainer container) {
				c.skillingTick++;
				if (!hasInventorySpace(c, "woodcutting")) {
					c.startAnimation(Animation.RESET);
					container.stop();
					return;
				}
				if ((o != null && ObjectManager.objectInSpotTypeTen(o.getX(), o.getY(), o.getZ())) || (patch != null && patch.getState() == State.STUMP)) {
					container.stop();
					return;
				}
				if (c.skillingTick % 3 == 0) {
					
					if (successCut(tree, axe, c.getSkills().getLevel(Skills.WOODCUTTING))) {
						int experience = tree.getExp();
						if (c.inWild()) {
							experience *= 2;
						}
						c.getPA().addSkillXP((int)((double)experience * calculateLumberjackOutfitXpModifier(c)), Skills.WOODCUTTING);
						boolean adzeActivated = false;
						if (axe == Hatchets.INFERNO_ADZE && Misc.random(3) == 2) {
							adzeActivated = infernoAdze(c, tree);
						}

						int logId = tree.getLogId();
						if (!adzeActivated && logId != -1) {
							c.getItems().addItem(logId, 1);
						}

						switch (tree) {
						case YEW:
							c.getAchievement().ChopYew();
							break;
						case MAGIC:
							c.getAchievement().chopMagic();
							break;
						case MAHOGANY:
							c.getAchievement().chopMahogany();
							break;
						default:
							break;
						}

						if (tree == Trees.IVY) {
							c.increaseProgress(TaskType.WOODCUTTING, TaskConstants.WOODCUTTING_IVY);
							c.sendMessageSpam("You successfully chop away some ivy.");
						} else {
							c.increaseProgress(TaskType.WOODCUTTING, logId);
							if (!adzeActivated) {
								c.sendMessageSpam("You get some logs.");
							}
						}

						SkillingUrns.handleUrn(c, Skills.WOODCUTTING, tree.getLvlReq(), experience);
						c.getAchievement().chopLogs();
						if (tree.getClueNestRate() > 0)
							c.getTreasureTrailManager().rollClueThingy(tree.getLvlReq(), tree.getClueNestRate(), Skills.WOODCUTTING, "You find a clue nest!");
						

						if (Misc.randomBoolean(tree.getDestroyRate())) {
							if (tree == Trees.REDWOOD) {
								return;
							}
							if (patch != null) {
								patch.setState(State.STUMP);
							} else if (o != null) {
								ObjectManager.addObject(new RSObject(stumpId, o.getX(),
										o.getY(), o.getZ(), o.getOrientation(), o.getType(),
										o.getId(), respawnTime));
								c.getPacketSender().playSound(Sounds.MAKE_TREE_STUMP);
							}
							c.startAnimation(Animation.RESET);
							container.stop();
						}
					}
				}
				if (Misc.random(100000) == 0) {
					birdsNestEggs(c);
				}
				if (Misc.random(700) == 0) {
					birdsNestSeeds(c);
				}
				if (Misc.random(500) == 0) {
					birdsNestRings(c);
				}
				c.startAnimation(anim);
				c.getPacketSender().playSound(Sounds.CHOP_TREE);
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, 1);
		return true;
	}
	
	private static boolean successCut(Trees tree, Hatchets axe, int lvl) {
		return Skills.successRoll(tree.getLowRate() + axe.getRate(), tree.getHighRate() + axe.getRate(), lvl);
	}

	public static boolean infernoAdze(Client c, Trees tree) {
		if (tree.getAdzeXp() == -1) {
			return false;
		}

		c.getPA().addSkillXP(tree.getAdzeXp(), Skills.FIREMAKING);
		c.sendMessageSpam("You chop some logs. The heat of the inferno adze incinerates them.");
		Projectile projectile = Projectile.create(c.getCurrentLocation(), c.getCurrentLocation().transform(Direction.getRandom()), 1776);
		projectile.setStartHeight(30);
		projectile.setStartDelay(0);
		projectile.setEndHeight(0);
		projectile.send();
		return true;
	}

	// test for bird nest grounditem
	public static void birdsNestEggs(Client c) {
		ItemHandler.createGroundItem(c, /* 11966 */ 5070 + Misc.random(2),
				c.getX(), c.getY(), c.getHeightLevel(), 1);
		c.sendMessage("A bird's nest falls out of the tree!");
	}

	public static void birdsNestRings(Client c) {
		ItemHandler.createGroundItem(c, 5074, c.getX(), c.getY(),
				c.getHeightLevel(), 1);
		c.sendMessage("A bird's nest falls out of the tree!");
	}

	public static void birdsNestSeeds(Client c) {
		ItemHandler.createGroundItem(c, 5073, c.getX(), c.getY(),
				c.getHeightLevel(), 1);
		c.sendMessage("A bird's nest falls out of the tree!");
	}

	private static int getStump(String name, int treeId) {
		name = name.toLowerCase();
		switch (name) {
			case "dead Tree":
				return treeId + 59;
			case "oak":
				return 1356;
			case "willow":
				return 7399;
			case "maple":
				return 1343;
			case "yew":
				return 7402;
			case "magic":
				return 7401;
			case "mahogany":
				return 9035;
			case "teak":
				return 9037;
			case "ivy":
				return 46319;
			case "tree":
			default:
				return 1342;
		}
	}

	public static Hatchets getHatchet(Player c) {
		Hatchets axeToReturn = null;
		Hatchets axeCompare = null;
		int myWCLvl = c.getSkills().getLevel(Skills.WOODCUTTING);
		boolean lvlReqMet = false;
		// search equipment
		if (c.playerEquipment[3] > 0) {
			axeToReturn = Hatchets.getAxe(c.playerEquipment[3]);

			if (axeToReturn != null) {
				if (axeToReturn.getLvlReq() > myWCLvl) {
					axeToReturn = null;
				} else {
					lvlReqMet = true;
				}
			}
		}
		// search inv
		for (int i = 0; i < c.playerItems.length; i++) {
			int itemId = c.playerItems[i] - 1;
			if (itemId > 0)
				axeCompare = Hatchets.getAxe(itemId);

			if (axeCompare != null) {
				if (axeCompare.getLvlReq() <= myWCLvl) {
					lvlReqMet = true;
					if (axeToReturn == null) {
						axeToReturn = axeCompare;
					} else if (axeToReturn != null && axeToReturn.getLvlReq() < axeCompare.getLvlReq()) {
						axeToReturn = axeCompare;
					}
				}
			}
		}
		if (!lvlReqMet) {
			c.sendMessage("You do not have a hatchet of your level.");
		} else if (lvlReqMet && axeToReturn == null) {
			c.sendMessage("You need a hatchet to woodcut.");
		}
		return axeToReturn;
	}
	
	public static double calculateLumberjackOutfitXpModifier(Player player) {
		double modifier = 1.0;

		if (player.getItems().playerHasEquipped(lumberJackHat, PlayerConstants.playerHat)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(lumberJackTop, PlayerConstants.playerChest)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(lumberJackLegs, PlayerConstants.playerLegs)) {
			modifier += 0.05;
		}

		if (player.getItems().playerHasEquipped(lumberJackBoots, PlayerConstants.playerFeet)) {
			modifier += 0.05;
		}
		return modifier;
	}

}
