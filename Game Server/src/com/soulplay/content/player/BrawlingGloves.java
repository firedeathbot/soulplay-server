package com.soulplay.content.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.items.degradeable.BrawlerGloveEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.model.player.timers.TickTimer;

public class BrawlingGloves {

	private final Player player;

	public BrawlingGloves(final Player player) {
		this.player = player;
	}
	
	private static final int SIZE = BrawlerGloveEnum.values.length;
	
	private int[] gloves = new int[SIZE];
	
	private int lastGlovesUsedIndex = -1;
	
	private final TickTimer stopWatch = new TickTimer();

	public double handleExperienceGain(int skillId) {
		double mod = 1.0;
		
		final BrawlerGloveEnum glove = BrawlerGloveEnum.get(player.playerEquipment[PlayerConstants.playerHands]);
		
		if (glove == null || !glove.isCorrectSkillId(skillId))
			return mod;
		
		final int index = glove.ordinal();
		
		final int currentCharge = gloves[index];
		
		gloves[index] = currentCharge + 1;
		
		if (gloves[index] > glove.getCharges()) {
			destroyGloves(index, false);
		} else {
			saveToDbMisc();
		}
		
		mod = 1.5;
		
		double wildMod = 0.0;
		
		if (player.wildLevel > 0 && glove != BrawlerGloveEnum.MELEE && glove != BrawlerGloveEnum.MAGIC && glove != BrawlerGloveEnum.RANGED) {
			final double actualWildLevel = player.wildLevel - 4.0;
			final double highestWildLevel = Math.min(47.0, actualWildLevel);
			wildMod = actualWildLevel * 2.5 / highestWildLevel; // 1.5 so highest ratio would become 2.5+1.5 = 4.0 = 400%
			startTimer(index);
		}
		
		return mod + wildMod;
	}

	public int[] getGloves() {
		return gloves;
	}

	public void loadJsonString(String jsonString) {
		if (jsonString == null)
			return;
		gloves = Arrays.copyOf((int[])GsonSave.gsond.fromJson(jsonString, new TypeToken<int[]>(){}.getType()), SIZE);
	}
	
	private void startTimer(int gloveId) {
		this.lastGlovesUsedIndex = gloveId;
		this.stopWatch.startTimer(50);
	}
	
	public int grabGlovesToDestroy() {
		if (!this.stopWatch.complete()) {
			return this.lastGlovesUsedIndex;
		}
		return -1;
	}
	
	public void destroyGlovesInWild() {
		
		final int glovesEnumIndex = grabGlovesToDestroy();
		
		final BrawlerGloveEnum glove = BrawlerGloveEnum.get(player.playerEquipment[PlayerConstants.playerHands]);
		
		if (glove != null) {
			
			if (glovesEnumIndex != -1 && glove.ordinal() != glovesEnumIndex) { // search gloves in inventory then
				if (destroyGlovesInInventory(glovesEnumIndex))
					return;
			}
		
			final int index = glove.ordinal();

			player.getItems().replaceEquipment(PlayerConstants.playerHands, -1);
			
			destroyGloves(index, true);
			
		} else { // search inventory
			
			if (glovesEnumIndex > -1) {
				
				destroyGlovesInInventory(glovesEnumIndex);
				
			}
		}
		
	}
	
	private boolean destroyGlovesInInventory(int glovesEnumIndex) {
		BrawlerGloveEnum invGlove = BrawlerGloveEnum.values[glovesEnumIndex];
		
		if (player.getItems().deleteItem2(invGlove.getId(), 1)) {
			destroyGloves(glovesEnumIndex, true);
			return true;
		}
		return false;
	}
	
	private void destroyGloves(int index, boolean killedInWild) {
		
		if (killedInWild)
			player.sendMessage("Your gloves turned to dust in the wild.");
		else
			player.sendMessage("Your gloves have run out of charges and turned to dust.");
		
		gloves[index] = 0; // reset after glove breaks
		saveToDbMisc();
	}
	
	
	private static final String MISC_SAVE_QUERY = "INSERT INTO `misc`(`player_id`, `brawler_gloves`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `brawler_gloves` = VALUES(`brawler_gloves`)";

	private void saveToDbMisc() {
		if (!Config.RUN_ON_DEDI)
			return;
		
		PlayerSaveSql.SAVE_EXECUTOR.execute(new Runnable() {
			
			@Override
			public void run() {

				try (Connection connection = Server.getConnection();
						PreparedStatement ps = connection.prepareStatement(MISC_SAVE_QUERY);) {
					ps.setInt(1, player.mySQLIndex);
					ps.setString(2, PlayerSaveSql.createJsonString(player.getBrawlers().getGloves()));
					ps.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		});
	}

}
