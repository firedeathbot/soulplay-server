package com.soulplay.content.player.trading;

import java.sql.Timestamp;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.util.Misc;

public class TradingPostOrder {
	
	private final String buyerName;
	private final int buyerMID;
	private final Item item;
	private final long pricePerItem;
	private final String sellerName;
	private final int sellerMID;
	private final String itemName;
	private final TradingPostType tradeType; // 0 = selling item to order - 1 = buying item from order
	private final Timestamp timestamp;
	
	public TradingPostOrder(String buyerName, int buyerMID, Item item, long price, String sellerName, int sellerMID, TradingPostType type) {
		this.buyerName = buyerName;
		this.buyerMID = buyerMID;
		this.item = new Item(item.getId(), item.getAmount(), item.getSlot());
		this.sellerName = sellerName;
		this.sellerMID = sellerMID;
		this.itemName = ItemConstants.getItemName(item.getId());
		this.tradeType = type;
		this.pricePerItem = price;
		this.timestamp = Misc.getESTTimestamp();
	}

	public String getBuyerName() {
		return buyerName;
	}

	public int getBuyerMID() {
		return buyerMID;
	}

	public Item getItem() {
		return item;
	}

	public String getSellerName() {
		return sellerName;
	}

	public int getSellerMID() {
		return sellerMID;
	}

	public String getItemName() {
		return itemName;
	}

	public int getTradeType() {
		return tradeType.ordinal();
	}

	public long getPricePerItem() {
		return pricePerItem;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

}
