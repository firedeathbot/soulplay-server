package com.soulplay.content.player.trading;

public enum TradingPostType {
	BUY_COMPLETED,
	SELL_COMPLETED;
}
