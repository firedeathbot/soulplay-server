package com.soulplay.content.player.trading;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemConstants;

public class Trade {

	String giver;

	private int giverMID;

	String recipient;

	private int recipientMID;

	GameItem item;

	int amount;

	String itemName;

	public Trade(String giver, int giverMID, String recipient, int recipMID,
			GameItem item) {
		this.giver = giver;
		this.recipient = recipient;
		this.item = item;
		this.amount = item.getAmount();
		this.itemName = ItemConstants.getItemName(item.getId());
		this.setGiverMID(giverMID);
		this.setRecipientMID(recipMID);
	}

	public String getGiver() {
		return giver;
	}

	public int getGiverMID() {
		return giverMID;
	}

	public GameItem getItem() {
		return item;
	}

	public String getRecipient() {
		return recipient;
	}

	public int getRecipientMID() {
		return recipientMID;
	}

	public void setGiverMID(int giverMID) {
		this.giverMID = giverMID;
	}

	public void setRecipientMID(int recipientMID) {
		this.recipientMID = recipientMID;
	}
}
