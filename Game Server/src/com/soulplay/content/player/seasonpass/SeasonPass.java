package com.soulplay.content.player.seasonpass;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.GraphicCollection;

public class SeasonPass {
	
	private static final int EXP_PER_TASK = 5;
	private static final int EXP_PER_TASK_WITH_PASS = 5;
	private static final int BONUS_COMPLETION = 5;
	private static final int EXP_PER_VOTE_FREE = 2;
	private static final int EXP_PER_VOTE_PAID = 5;
	
	public int seasonId;
	public boolean claimingSeasonReward = false;
	public int level = 1;
	public int experience;
	public boolean purchased;
	
	public void reset() {
		level = 1;
		experience = 0;
		purchased = false;
		claimed.clear();
	}
	
	private Set<Reward> claimed = new HashSet<>();
	
	public boolean alreadyClaimed(Reward r) {
		return claimed.contains(r);
	}
	
	public void addToClaimed(Reward r) {
		claimed.add(r);
	}
	
	public void loadExp(int exp) {
		this.experience = exp;
		calculateLevel(null);
	}
	
	public void onTaskCompleted(Player p, boolean bonus) {
		if (!SeasonPassManager.seasonPassOpen())
			return;
		if (bonus) {
			experience += BONUS_COMPLETION;
			p.sendMessage("You gained " + BONUS_COMPLETION + " bonus experience for the completing the category.");
		} else {
			int expToAdd = EXP_PER_TASK;
			if (purchased) {
				expToAdd += EXP_PER_TASK_WITH_PASS;
			} else {
				p.sendMessage("Since you have not purchased the season pass, you get half the exp.");
			}
			experience += expToAdd;
			p.sendMessage("You gained " + expToAdd + " experience for the season pass.");
		}
		calculateLevel(p);
		SeasonPassManager.saveSeasonPass(p);
	}
	
	public void onVote(Player p) {
		if (!SeasonPassManager.seasonPassOpen())
			return;
		int reward = p.seasonPass.purchased ? EXP_PER_VOTE_PAID : EXP_PER_VOTE_FREE;
		
		p.sendMessage("You have earned " + reward + " exp for the season pass for voting.");
		

		experience += reward;
		
		calculateLevel(p);
		SeasonPassManager.saveSeasonPass(p);
	}
	
	public void seasonPasslevelUpScroll(Player player, int itemId, int itemSlot) {
		if (!SeasonPassManager.seasonPassOpen()) {
			player.sendMessage("Season pass is over. You cannot use this anymore.");
			return;
		}
		
		if (!player.seasonPass.purchased) {
			player.sendMessage("You can only use this when you have purchased the season pass.");
			return;
		}
		
		if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {

			experience += 200;
			
			player.sendMessage("You redeem the season pass level up scroll.");
			

			calculateLevel(player);
			SeasonPassManager.saveSeasonPass(player);
		}
	}
	
	public void calculateLevel(Player p) {
		int oldLvl = level;
		level = experience / 40 + 1;
		if (p != null && level != oldLvl) {
			p.sendMessage("You have leveled up your season pass! Your Season Pass is now Level: " + level);
			p.startGraphic(GraphicCollection.LVL_UP_FIREWORKS);
		}
	}
	
}
