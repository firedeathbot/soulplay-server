package com.soulplay.content.player.seasonpass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.soulplaydate.SoulPlayDate;

public class SeasonPassManager {

	public static int seasonId = 0, dayOpen = 0, dayClosed = 0;
	
	public static boolean active = false;
	
	public static boolean loading = false;
	
	public static List<Reward> rewards = new ArrayList<>();
	
	public static void sortListByLevel() {
		Collections.sort(rewards);
	}
	
	public static int DP_PRICE_PER_PASS = 15;
	
	public static String title = "SoulPlay Season Pass";
	
	public static boolean seasonPassOpen() {
		int soulPlayDay = SoulPlayDate.getDate();
		return soulPlayDay >= dayOpen && soulPlayDay <= dayClosed;
	}
	
	public static int getDaysLeft() {
		if (!active)
			return 0;
		return  Math.max(0, dayClosed - SoulPlayDate.getDate());
	}
	
	public static void openInterface(Player p) {
		if (!seasonPassOpen()) {
			p.sendMessage("Season Pass is not available right now.");
			return;
		}
		p.getPacketSender().showInterface(60000);
		p.getPacketSender().sendSeasonPassData(SeasonPassManager.rewards);
	}
	
	private static final String SETTINGS_QUERY = "SELECT * FROM `zaryte`.`season_pass_settings`";
	
	public static void updateSeasonPassSettings(boolean load) {
		loading = true;
		System.out.println("UPDATING SEASON PASS SETTINGS.");
		TaskExecutor.execute(()-> {
			
			final int curSoulPlayDay = SoulPlayDate.getDate();
			final int currentSeason = seasonId;
			
			boolean resettingLevels = false;

			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(SETTINGS_QUERY)) {

				try (ResultSet rs = stmt.executeQuery();) {
					while (rs.next()) {
						final int uSeasonId = rs.getInt("ID");
						final int uOpenDay = rs.getInt("open_day");
						final int uCloseDay = rs.getInt("close_day");
						final boolean active = rs.getBoolean("active");
						
						if (curSoulPlayDay >= uOpenDay && curSoulPlayDay <= uCloseDay) { // we found a pass in proper season

							SeasonPassManager.active = active;
							SeasonPassManager.seasonId = uSeasonId;
							SeasonPassManager.dayOpen = uOpenDay;
							SeasonPassManager.dayClosed = uCloseDay;
							
							if (active) { // found active season, so break out of here, no neeed to look further
								clearRewardsList();
								loadRewards(uSeasonId);
								
								if (!load && currentSeason != uSeasonId) {
									resettingLevels = true;
									SeasonPassManager.resetPlayerLevels(uSeasonId);
								}
								break;
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			if (!resettingLevels)
				SeasonPassManager.loading = false;
		});
	}
	
	private static void resetPlayerLevels(int newSeason) {
		PlayerSaveSql.SAVE_EXECUTOR.execute(()-> {
			for (Player p : PlayerHandler.players) {
				if (p == null || !p.isActive) continue;
				p.seasonPass.reset();
				p.seasonPass.seasonId = newSeason;
			}
			truncateLevels();
			SeasonPassManager.loading = false;
		});
	}
	
	private static final String TRUNC_LEVELS = "TRUNCATE `players`.`season_pass_save`";  //TODO: create players.season_pass_lvls table
	
	public static void truncateLevels() {

			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(TRUNC_LEVELS);) {
				stmt.executeUpdate();
			} catch (SQLException e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(SeasonPassManager.class, e));
			}
	}
	
	private static final String REWARDS_QUERY = "SELECT * FROM `zaryte`.`season_pass_rewards` WHERE `season_id` = ?;";
	
	public static void clearRewardsList() {
		rewards.clear();
	}
	
	public static void loadRewards(final int seasonId) {
		try (Connection connection = Server.getConnection();
				PreparedStatement stmt = connection.prepareStatement(REWARDS_QUERY)) {
			stmt.setInt(1, seasonId);
			processRewardResultSet(stmt, seasonId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (rewards.size() > 1) {
			sortListByLevel();
		}
		
	}
	
	private static void processRewardResultSet(PreparedStatement ps, final int seasonId) {
		try (ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				final int rewardIndex = rs.getInt("ID");
				final int itemId = rs.getInt("item_id");
				final int itemAmount = rs.getInt("item_amount");
				final int level = rs.getInt("level_req");
				
				GameItem item = new GameItem(itemId, itemAmount);
				
				Reward reward = new Reward(seasonId, rewardIndex, level, item);
				
				rewards.add(reward);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static final String SAVE_PLAYER_Q = "INSERT INTO `players`.`season_pass_save`(`player_id`, `season_id`, `exp`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE exp = VALUES(exp)";
	
	public static void saveSeasonPass(Player p) {
		if (seasonId < 1)
			return;

		PlayerSaveSql.SAVE_EXECUTOR.execute(()-> {
			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(SAVE_PLAYER_Q);) {
				stmt.setInt(1, p.mySQLIndex);
				stmt.setInt(2, seasonId);
				stmt.setInt(3, p.seasonPass.experience);
				stmt.executeUpdate();
			} catch (SQLException e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(SeasonPassManager.class, e));
			}
		});
	}
	
	private static final String UNLOCK_PASS_Q = "INSERT INTO `players`.`season_pass_save`(`player_id`, `season_id`, `exp`, `unlocked`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE exp = VALUES(exp), unlocked = VALUES(unlocked)";
	
	public static boolean unlockPass(Player p) {
		if (seasonId < 1)
			return false;
		
		p.seasonPass.purchased = true;
		
		PlayerSaveSql.SAVE_EXECUTOR.execute(()-> {
			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(UNLOCK_PASS_Q);) {
				stmt.setInt(1, p.mySQLIndex);
				stmt.setInt(2, seasonId);
				stmt.setInt(3, p.seasonPass.experience);
				stmt.setBoolean(4, true);
				stmt.executeUpdate();
			} catch (SQLException e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(SeasonPassManager.class, e));
			}
		});
		return true;
	}
	
	public static void openPurchasePassDialogue(Player p) {
		if (seasonId < 1 || !active) {
			p.getDialogueBuilder().sendStatement("You cannot purchase the pass at the moment.").execute();
			return;
		}
		
		p.getPA().closeAllWindows();
		
		p.getDialogueBuilder()
		.sendStatement("You can purchase the season pass for " + DP_PRICE_PER_PASS + " Donor Points.")
		.sendOption("Would you like to buy the pass?", "Yes, buy pass for 15 donor points.", ()-> {
			startPurchase(p);
		}, "No thanks.", ()-> {
			p.getDialogueBuilder().closeNew();
		}).execute();
		
	}
	
	public static void startPurchase(Player p) {
		if (p.getDonPoints() >= DP_PRICE_PER_PASS) {
			if (unlockPass(p)) {
				p.setDonPoints(p.getDonPoints() - 15, true);
				p.sendMessage("You have bought the season pass!");
			}
		} else {
			p.sendMessage("You do not have enough donor points to buy the season pass.");
		}
	}
	
}
