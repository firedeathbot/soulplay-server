package com.soulplay.content.player.seasonpass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSaveSql;

public class SeasonPassClaim {
	
	public static boolean SEASON_PASS_OPEN = true;
	
	private static final String QUERY = "INSERT INTO `zaryte`.`season_pass_claimed` (`season_id`, `reward_id`, `player_id`) VALUES (?, ?, ?);";

	public static void claimReward(Player p, Reward r) {
		
		if (!SeasonPassManager.seasonPassOpen()) {
			p.sendMessage("Season Pass is no longer available.");
			return;
		}
		
		if (!SEASON_PASS_OPEN || !SeasonPassManager.active || SeasonPassManager.loading) {
			p.sendMessage("You cannot claim right now.");
			if (p.debug) {
				p.sendMessage("SeasonPassClaim: SEASON_PASS_OPEN:"+SEASON_PASS_OPEN+" - SPM.active:"+ SeasonPassManager.active + " - SPM.loading:" + SeasonPassManager.loading);
			}
			return;
		}
		
		if (p.seasonPass.alreadyClaimed(r)) {
			p.sendMessage("You have already claimed this item.");
			return;
		}
		
		if (!p.seasonPass.purchased && r.levelRequired > 5) {
			p.sendMessage("You must purchase a season pass to claim this.");
			return;
		}
		
		if (p.seasonPass.claimingSeasonReward) {
			p.sendMessage("Please wait for your reward to process.");
			return;
		}
		
		p.sendMessage("Attempting to claim the reward.");
		
		p.seasonPass.claimingSeasonReward = true;
		
		final int playerId = p.mySQLIndex;
		
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {
			try (Connection connection = Server.getConnection();
					PreparedStatement stmt = connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS)) {
				stmt.setInt(1, r.seasonIndex);
				stmt.setInt(2, r.rewardIndex);
				stmt.setInt(3, playerId);
				stmt.executeUpdate();
				
				Integer rewardIndex = -1;
				
				try (ResultSet keys = stmt.getGeneratedKeys()) {
					if (keys.next())
						rewardIndex = keys.getInt(1);
				} catch(Exception e) {
					System.out.println("no reward");
				}
				
				if (p.isActive)
					p.seasonPass.claimingSeasonReward = false;
				
				if (rewardIndex > 0) {
					Server.getTaskScheduler().schedule(()-> {
						if (!p.disconnected) {
							p.getItems().addOrDrop(r.item);
							p.sendMessage("Claimed reward from season pass.");
							p.seasonPass.addToClaimed(r);
						} else {
							System.out.println("Player:"+ p.getName() + " -DID NOT GET THEIR SEASON REWARD!!! SeasonId:"+r.seasonIndex+" rewardId:"+r.rewardIndex+" playerId:"+playerId);
						}
					});
				} else {
					if (p.isActive) {
						PlayerHandler.sendMessageMainThread(p, "Error claiming the reward.");
					}
				}
				
			} catch (SQLException e) {
				if (p.isActive) {
					p.seasonPass.claimingSeasonReward = false;
					PlayerHandler.sendMessageMainThread(p, "You have already claimed the reward.");
					p.seasonPass.addToClaimed(r);
				}
			}
		});
		
	}
}
