package com.soulplay.content.player.seasonpass;

import com.soulplay.game.model.item.GameItem;

public class Reward implements Comparable<Reward> {

	public final int seasonIndex, rewardIndex, levelRequired;
	public final GameItem item;
	
	public Reward(int seasonId, int rewardIndex, int lvl, GameItem item) {
		this.seasonIndex = seasonId;
		this.rewardIndex = rewardIndex;
		this.levelRequired = lvl;
		this.item = item;
	}

	@Override
	public int compareTo(Reward o) {
		if (this.levelRequired > o.levelRequired)
			return 1;
		return -1;
	}
}
