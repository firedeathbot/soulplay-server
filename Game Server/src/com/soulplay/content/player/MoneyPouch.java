package com.soulplay.content.player;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

public class MoneyPouch {

	public static void addToMoneyPouch(Player c, long amount) {
		addToMoneyPouch(c, amount, true, 0);
	}
	
	public static void addToMoneyPouch(Player c, long amount, boolean updateClient, int from) {
		if (c.getZones().isInDmmLobby())
			return;
		long current = c.getMoneyPouch();
		long newGp = current;
		try {
			newGp = Math.addExact(current, amount);
		} catch (ArithmeticException e) {
			c.sendMessage("Error while adding to your pouch.");
		}

		//dupe check?
		if (from == 7) {
			long testValue = newGp - current;
			if (testValue <= -4_000_000_000l || testValue >= 4_000_000_000l) {
				Server.getSlackApi().call(SlackMessageBuilder.buildMoneyPouchDuperMessage(c.getDisplayName(), testValue));
			}
		}
		
		c.setMoneyPouch(newGp);
		if (updateClient)
			sendMoneyPouchAmount(c);
	}

	public static void removeFromMoneyPouch(Player c, long amount) {
		amount = Math.max(0, amount);

		long current = c.getMoneyPouch();
		long newGp = current;
		try {
			newGp = Math.subtractExact(current, amount);
		} catch (ArithmeticException e) {
			c.sendMessage("Error while adding to your pouch.");
		}

		c.setMoneyPouch(newGp);
		sendMoneyPouchAmount(c);
	}

	public static void sendMoneyPouchAmount(Player c) {
		String color = "<col=ffff00>";//TODO set color from client?
		if (c.getMoneyPouch() >= 1000000) {
			color = "<col=ff00>";
		}

		c.getPacketSender().sendFrame126b(color + Misc.formatShortPrice(c.getMoneyPouch()), 12501);
	}

}
