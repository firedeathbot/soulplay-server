package com.soulplay.content.player.combat.melee;

import com.soulplay.Config;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.minigames.FightPitsTournament;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.blacklist.BlackListEntry;
import com.soulplay.game.world.pvpworld.PvpFormula;
import com.soulplay.net.packet.in.ChallengePlayer;

public class MeleeRequirements {
	
	public static boolean BLACK_LIST_ENABLED = true;
	
	public static boolean BLACK_LIST_UUID_ENABLED = false;
	

	public static boolean checkReqs(Client c, Client o, boolean resetAttack) {

		if (o == null || !o.isActive) {
			if (resetAttack) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			}
			return false;
		}
		if (c.teleTimer > 0) {
			if (resetAttack) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		if (o.getId() == c.getId()) {
			if (resetAttack) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			}
			return false;
		}
		
		if (c.getZones().isInDmmTourn()) {
			if (c.tournamentRoom != null && c.tournamentRoom.fightStarted()) {
				return true;
			}
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
		case 4566:
		case 10501:
		case 14728:
		case 24145:
			if (c.wildLevel < 1)
				return true;
			break;
		}

		if (c.inSoulWars()) {
			if (!c.getStopWatch().complete() || !o.getStopWatch().complete()) { // Player is ghost?
				if (resetAttack) {
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (SoulWars.getTeam(c) == SoulWars.getTeam(o)) {
				if (resetAttack) {
					c.sendMessage("You cannot attack your own team mate.");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			return true;
		}
		
		if (c.inCw()) {
			if (o.castleWarsTeam == 0) {
				if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (RSConstants.cwSaraSpawnRoom(c.getX(), c.getY(),
					c.getHeightLevel())
					|| RSConstants.cwZammySpawnRoom(c.getX(), c.getY(),
							c.getHeightLevel())) {
				if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (RSConstants.cwSaraSpawnRoom(o.getX(), o.getY(),
					o.getHeightLevel())
					|| RSConstants.cwZammySpawnRoom(o.getX(), o.getY(),
							o.getHeightLevel())) {
				if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (CastleWars.getTeamNumber(c) == CastleWars.getTeamNumber(o)) {
				if (resetAttack) {
				c.sendMessage("You cannot attack your own team mate.");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			return true;
		}

		if (c.inClanWars() && c.getClan() != null
				&& c.getClan().getClanWarId() > -1) {
			if (c.spectatorMode) {
				return false;
			}
			if (c.inClanWarsDead() || o.inClanWarsDead()) {
				if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (!ClanWarHandler.wallDown(
					ClanWarHandler.clanWars[c.getClan().getClanWarId()])) {
				if (resetAttack) {
				c.sendMessage("Wait for the wall to go down.");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (ClanWarHandler.clanWars[c.getClan().getClanWarId()].clan1Players
					.contains(c)
					&& ClanWarHandler.clanWars[c.getClan()
							.getClanWarId()].clan1Players.contains(o)) {
				if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			if (ClanWarHandler.clanWars[c.getClan().getClanWarId()].clan2Players
					.contains(c)
					&& ClanWarHandler.clanWars[c.getClan()
							.getClanWarId()].clan2Players.contains(o)) {
				if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
		}

		if (c.inClanWars()) {
			if (c.getClan() == null) {
				return false;
			}
			return true;
		}

		if (c.inClanWarsLobby() && o.inClanWarsLobby()) {
			if (resetAttack) {
			c.getCombat().resetPlayerAttack();
			}
			if (c.clanWarOpponent > 0 && c.clanWarOpponent == o.getId()) {
				if (resetAttack) {
				ClanWarHandler.challengeClicked(c);
				}
				return false;
			}
			if (resetAttack) {
			ClanWarHandler.requestChallenge(c, o);
			}
			return false;
		}

		if (o.wildLevel > 0 && o.immuneToPK) {
			if (resetAttack) {
			c.sendMessage("This player is currently immune to PJers.");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		if (c.inClanWarsFunPk() || c.inLMSArenaCoords()) {
			if (!o.inMulti() && (c.inClanWarsFunPk() || c.inLMSArenaCoords())) {
				int oLastHitIndex = o.getLastHitPlayerIndex();
				if ((o.underAttackBy > 0 && o.underAttackBy != c.getId()) || (oLastHitIndex > 0 && oLastHitIndex != c.getId())) {
						if (resetAttack) {
							c.sendMessage("That player is already in combat.");
							c.getCombat().resetPlayerAttack();
							c.getMovement().resetWalkingQueue();
						}
						return false;
				}
				if (c.underAttackBy > 0 && (o.getId() != c.underAttackBy || c.underAttackBy2 > 0)) {
					if (resetAttack) {
						c.sendMessage("You are already in combat.");
						c.getCombat().resetPlayerAttack();
						c.getMovement().resetWalkingQueue();
					}
					return false;
				}
			}
			return true;
		}

		if (c.inFunPk()) {
			// add some rules here maybe
			if (FunPkTournament.isTournamentOpen()
					&& !FunPkTournament.allowFight()) {
				if (resetAttack) {
				c.sendMessage("You aren't allowed to fight yet.");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
			return true;

		}
		if (c.inClanWars()) {
			return true;
		}

		if (RSConstants.fightPitsRoom(c.getX(), c.getY())
				&& !FightPitsTournament.allowFight()) {
			if (resetAttack) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		final boolean isInEdgevillePk = c.getZones().isInEdgePk();
		
		if ((c.isToggleRag() || o.isToggleRag()) && isInEdgevillePk) {
			int diff = c.getItems().getTotalBonuses() - o.getItems().getTotalBonuses();
			if (diff < -600 || diff > 600) {
				if (resetAttack) {
					c.sendMessage("The raglord does not allow you to attack this player. Toggle command ::rag");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
				}
				return false;
			}
		}
		
		if (BLACK_LIST_ENABLED && (!c.getWildyBlackList().isEmpty() || !o.getWildyBlackList().isEmpty()) && isInEdgevillePk) {
			String toCheck = o.getNameSmart();
			boolean stopAttack = false;

			for (int i = 0, l = c.getWildyBlackList().size(); i < l; i++) {
				BlackListEntry entry = c.getWildyBlackList().get(i);
				if (toCheck.equalsIgnoreCase(entry.getName()) || (BLACK_LIST_UUID_ENABLED && o.UUID.equals(entry.getUUID()))) {
					stopAttack = true;
					break;
				}
			}

			if (!stopAttack) {
				toCheck = c.getNameSmart();
				for (int i = 0, l = o.getWildyBlackList().size(); i < l; i++) {
					BlackListEntry entry = o.getWildyBlackList().get(i);
					if (toCheck.equalsIgnoreCase(entry.getName()) || (BLACK_LIST_UUID_ENABLED && c.UUID.equals(entry.getUUID()))) {
						stopAttack = true;
						break;
					}
				}
			}

			if (stopAttack) {
				if (resetAttack) {
					c.sendMessage("The raglord does not allow you to attack this player.");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
				}
				return false;
			}
		}

		if (RSConstants.fightPitsRoom(c.getX(), c.getY())) {
			if (FightPitsTournament.FFA) {
				if (RSConstants.fightPitsRoom(o.getX(), o.getY())) {
					return true;
				} else {
					if (resetAttack) {
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					}
					return false;
				}
			} else if (!FightPitsTournament.FFA) {
				// TARGET STUFF
				if (o.getId() != c.minigameTarget) {
					if (resetAttack) {
					c.sendMessage("That is not your target.");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					}
					return false;
				} else if (o.getId() == c.minigameTarget) {
					return true;
				}

			}
		}

		if (c.inPits && o.inPits) {
			return true;
		}
		if (c.canOffer) {
			if (resetAttack) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			}
			return false;
		}
		
		if (c.duelStatus == 5) {
			if (c.duelCount > 0) {
				if (resetAttack) {
				c.sendMessage("The duel hasn't started yet!");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
		}
		
		if (o.inDuelArena() && c.duelStatus != 5) {
			if (c.duelFightArenas() || c.duelStatus == 5) {
				if (resetAttack)
					c.sendMessage("You can't challenge inside the arena!");
				return false;
			}

			if (resetAttack) {
				ChallengePlayer.walkToChallengePlayer(c, o);
			}
			return false;
		}

		if (c.duelStatus == 5 && (c.getDuelingClient() != null
				&& c.getDuelingClient().duelStatus == 5)) {
			if (o.getId() == c.getDuelingClient().getId()) {
				return true;
			} else {
				if (resetAttack) {
				c.sendMessage("This isn't your opponent!");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				}
				return false;
			}
		}
		if ((!o.inWild() && !(c.safeTimer > 0)) && !c.attr().has("canAttack")) {
			if (resetAttack) {
				c.sendMessage("That player is not in the wilderness.");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}
		if (c.safeTimer > 0) {
			if (!o.inWild() && o.safeTimer == 0) {
				if (resetAttack) {
					c.sendMessage("That player is not in the wilderness..");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
				}
				return false;
			}
		}
		if (!c.inWild() && !(c.safeTimer > 0) && !c.attr().has("canAttack")) {
			if (resetAttack) {
				c.sendMessage("You are not in the wilderness.");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		if (!checkCombatDifference(c, o, resetAttack)) {
			return false;
		}

		if (Config.SINGLE_AND_MULTI_ZONES) {
			if (!o.inMulti() && !o.getItems().playerHasEquipped(MagicalCrate.MAGICAL_CRATE_ITEM_ID)) { // single combat zones
				if ((o.underAttackBy > 0 && o.underAttackBy != c.getId())
						|| o.underAttackBy2 > 0) {
					if (resetAttack) {
					c.sendMessage("That player is already in combat.");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					}
					return false;
				}
				if ((c.underAttackBy > 0 && o.getId() != c.underAttackBy)
						|| c.underAttackBy2 > 0) {
					if (resetAttack) {
					c.sendMessage("You are already in combat.");
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					}
					return false;
				}
			}
		}
		return true;
	}

	public static boolean checkCombatDifference(Player c, Player o, boolean resetAttack) {
		if (Config.isPvpWorld()) {
			if (!PvpFormula.withinPvpRange(c, o, resetAttack))
				return false;
		} else {
			if (Config.COMBAT_LEVEL_DIFFERENCE && !o.getItems().playerHasEquipped(MagicalCrate.MAGICAL_CRATE_ITEM_ID)) {
				if (c.inWild()) {
					int combatDif1 = getCombatDifference(c.combatLevel,
							o.combatLevel);
					if ((combatDif1 > c.wildLevel || combatDif1 > o.wildLevel)) {
						if (resetAttack) {
							c.sendMessage(
									"Your combat level difference is too great to attack that player here.");
							c.getCombat().resetPlayerAttack();
							c.getMovement().resetWalkingQueue();
						}
						return false;
					}
				} else {
					int myCB = c.combatLevel;
					int pCB = o.combatLevel;
					if ((myCB > pCB + 15) || (myCB < pCB - 15)) {
						if (resetAttack) {
							c.sendMessage(
									"You can only fight players in your combat range!");
							c.getCombat().resetPlayerAttack();
							c.getMovement().resetWalkingQueue();
						}
						return false;
					}
				}
			}
		}

		return true;
	}

	public static int getCombatDifference(int combat1, int combat2) {
		if (combat1 > combat2) {
			return (combat1 - combat2);
		}
		if (combat2 > combat1) {
			return (combat2 - combat1);
		}
		return 0;
	}

	public static boolean sameTeam(Client c, Player p) {
		if (c.getName().equals(p.getName()))
			return true;
		if (c.soulWarsTeam != Team.NONE && p.soulWarsTeam != Team.NONE) {
			if (c.soulWarsTeam == p.soulWarsTeam)
				return true;
			else
				return false;
		} else if (c.inCw()) {
			if (!c.inCwGame || !p.inCwGame) {
				return true;
			}
			Client o2 = (Client) p;
			if (CastleWars.getTeamNumber(c) == CastleWars.getTeamNumber(o2)) {
				return true;
			}
		} else if (c.inClanWars() && c.getClan().getClanWarId() > -1) {
			if (ClanWarHandler.clanWars[c.getClan().getClanWarId()].clan1Players
					.contains(c)
					&& ClanWarHandler.clanWars[c.getClan()
							.getClanWarId()].clan1Players.contains(p)) {
				return true;
			}
			if (ClanWarHandler.clanWars[c.getClan().getClanWarId()].clan2Players
					.contains(c)
					&& ClanWarHandler.clanWars[c.getClan()
							.getClanWarId()].clan2Players.contains(p)) {
				return true;
			}
		} else if (c.inSoulWars()) {
			if (c.soulWarsTeam == p.soulWarsTeam)
				return true;
		}
		return false;
	}

}
