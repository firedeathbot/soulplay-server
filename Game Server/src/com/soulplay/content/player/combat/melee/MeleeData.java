package com.soulplay.content.player.combat.melee;

import com.soulplay.cache.EntityDef;
import com.soulplay.content.items.itemdata.MeleeSwingStyle;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;

public class MeleeData {

	// public static int getKillerId(Client c, int playerId) {
	// int oldDamage = 0;
	// int killerId = 0;
	// for (int i = 1; i < Config.MAX_PLAYERS; i++) {
	// if (PlayerHandler.players[i] != null) {
	// if (PlayerHandler.players[i].killedBy == playerId) {
	// if (PlayerHandler.players[i]
	// .withinDistance(PlayerHandler.players[playerId])) {
	// if (PlayerHandler.players[i].totalPlayerDamageDealt > oldDamage) {
	// oldDamage = PlayerHandler.players[i].totalPlayerDamageDealt;
	// killerId = i;
	// }
	// }
	// PlayerHandler.players[i].totalPlayerDamageDealt = 0;
	// PlayerHandler.players[i].killedBy = 0;
	// }
	// }
	// }
	// return killerId;
	// }
	
	public static int getAttackDelayNew(Client c, SpellsData spell) {
		if (SeasonalData.isBeast(c)) {
			return 2;
		}

		if (spell != null) {
			switch (spell) {
				case TRIDENT_OF_THE_SEA:
				case TRIDENT_OF_THE_SWAMP:
				case DAWNBRINGER:
				case SANGUINESTI_STAFF: {
					return 4;
				}
				case STORM_OF_ARMADYL: {
					if (c.playerEquipment[PlayerConstants.playerWeapon] == 21777) return 4;

					return 5;
				}
				default: {
					if (c.playerEquipment[PlayerConstants.playerWeapon] == 124423) return 4;

					return 5;
				}
			}
			
		}
		
		if (c.playerEquipment[PlayerConstants.playerWeapon] == -1)
			return 4;// unarmed

		if (c.usingWhiteWhipSpec && c.playerEquipment[PlayerConstants.playerWeapon] == 15443) // white whip spec
			return 2;
		
		ItemDefinition def = ItemDefinition.forId(c.playerEquipment[PlayerConstants.playerWeapon]);
		
		if (def == null) {
			c.sendMessage("This weapon definitions are null. Report to Staff. Error 2550");
			return 4;
		}
		if ((def.getId() == 11785 || def.getId() == 12926) && c.npcIndex > 0) // acb shoots faster on pvm
			return def.getCombatStats().getAttackSpeed()-1;
		
		return def.getCombatStats().getAttackSpeed();
	}
	
	public static int getBlockEmoteNew(Player c) {
		if (c.getTransformId() != -1) {
			return NPCAnimations.getDefend(c.getTransformId());
		}
		if (c.playerEquipment[PlayerConstants.playerShield] > 0) { // has shield emote
			ItemDefinition def2 = ItemDefinition.forId(c.playerEquipment[PlayerConstants.playerShield]);
			if (def2 != null && def2.getBlockEmote() > 0)
				return def2.getBlockEmote();
		}
		
		ItemDefinition def = ItemDefinition.forId(c.playerEquipment[PlayerConstants.playerWeapon]);

		if (def == null || def.getBlockEmote() == 0) {
			return 424;
		}
		
		return def.getBlockEmote();
	}
	
	public static int getBlockEmote(Client c) {
		if (c.getTransformId() !=  -1) {
			// return npcDefenceAnim(c.transformNpcId);
			return NPCAnimations.getDefend(c.getTransformId());
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 7447) { // kitchen
																		// knife
			return 14591;
		}
		String shield = ItemConstants
				.getItemName(c.playerEquipment[PlayerConstants.playerShield])
				.toLowerCase();
		String weapon = ItemConstants
				.getItemName(c.playerEquipment[PlayerConstants.playerWeapon])
				.toLowerCase();
		if (shield.contains("defender")) {
			return 4177;
		}
		if (shield.contains("2h")) {
			return 7050;
		}
		if (weapon.contains("light")) {
			return 12806;
		}
		if (shield.contains("book")
				|| (weapon.contains("wand") || (weapon.contains("staff")))) {
			return 420;
		}
		if (shield.contains("shield")) {
			return 1156;
		}

		if (weapon.contains("ivandis")) {
			return 9048;
		}
		if (weapon.contains("spear")) {
			return PlayerConstants.EMOTE_BLOCK_SPEAR;
		}

		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
			case 4755:
				return 12004;
			case 15241: // hand cannon
				return 12156;
			case 13899:
			case 13901: // Vesta LongSword Deg
			case 13923: // Corrupt Vesta's LongSword
			case 13925: // Corrupt Vesta's LongSword
				return 13042;
				
			case 13902:// statius warhammer
			case 13904:// statius warhammer
			case 13926:// statius warhammer
			case 13928:// statius warhammer
				return 13038;
				
			case 18355:
				return 13046;
			case 14484: // dragon claws
				return 397;
			case 11716:
				return 12008;
			case 4153:
			case 30066:
				return 1666;
			case 4151:
			case 15444:
			case 15443:
			case 15442:
			case 15441:
			case 21371:
			case 21372:
			case 21373:
			case 21374:
			case 21375:
			case 12006:
				return 11974;// 1659;
			case 15486:
				return 12806;
			case 18349:
				return 12030;
				
			case 20084:
			case 18353:
				return 13054;
			case 18351:
				return 13042;

			case 11694:
			case 11698:
			case 11700:
			case 11696:
			case 11730:
				return 7050;
			case -1:
				return 424;
			default:
				return 424;
		}
	}
	
	public static final short[] defaultAnims = {0x328, 0x337, 0x333, 0x334, 0x335, 0x336,
			0x338};

	public static void getPlayerAnimIndex(Player c, int weaponId) {

		// default animations
		// c.playerStandIndex = 0x328;
		// c.playerWalkIndex = 0x333;
		// c.playerTurn180Index = 0x334;
		// c.playerTurn90CWIndex = 0x335;
		// c.playerTurn90CCWIndex = 0x336;
		// c.playerTurnIndex = 0x337;
		// c.playerRunIndex = 0x338;

		if (c.getTransformId() != -1) {
			EntityDef npc = EntityDef.forID(c.getTransformId());
			int playerStandIndex = npc.standAnim;
			int playerWalkIndex = npc.walkAnim;
			int playerRunIndex = npc.walkAnim;
			int playerTurn180Index = npc.turn180AnimIndex;
			int playerTurn90CWIndex = npc.turn90CWAnimIndex;
			int playerTurn90CCWIndex = npc.turn90CCWAnimIndex;
			short[] anims = {(short) playerStandIndex,
					(short) 0x337, (short) playerWalkIndex,
					(short) playerTurn180Index, (short) playerTurn90CWIndex,
					(short) playerTurn90CCWIndex, (short) playerRunIndex};

			c.getMovement().setCurrentPlayerMovementAnimIds(anims);
			c.resetAnimations();
			return;
		}

		ItemDefinition itemDef = ItemDefinition.forId(weaponId);
		if (itemDef == null || itemDef.getMovementAnimIds() == null) {
			c.getMovement().setCurrentPlayerMovementAnimIds(defaultAnims.clone());
		} else {
			c.getMovement().setCurrentPlayerMovementAnimIds(itemDef.getMovementAnimIds().clone());
		}
		// if (c.correctlyInitialized && c.getRegion().getPlayers().size() >
		// Config.MAX_PLAYERS_DISPLAY) {
		// //c.playerStandIndex = 6;
		// c.getPlayerMovementAnimIds()[0] = 6;
		// c.resetAnimations();
		// return;
		// }

	}
	
	public static Animation getWepAnim(Client c) {
		if (c.getTransformId() != -1) {
			// return NPCHandler.getAttackEmote(c.transformNpcId, 0);
			return NPCAnimations.getCombatEmote(c.getTransformId());
		}
		
		if (c.getRangeWeapon() != null) {
			return c.getRangeWeapon().getAnimation();
		}

		return c.getCombatTabButtonToggle().getAttackEmote();
	}
	

	public static int getWepAnimOld(Client c, String weaponName) {
		if (c.getTransformId() != -1) {
			// return NPCHandler.getAttackEmote(c.transformNpcId, 0);
			return NPCAnimations.getCombatEmote(c.getTransformId()).getId();
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] >= 13879
				&& c.playerEquipment[PlayerConstants.playerWeapon] <= 13882) {
			return 10501;
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 13902
				|| c.playerEquipment[PlayerConstants.playerWeapon] == 13926) { // statius
																				// warhammer
			return 13035;
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926) { // toxic blowpipe
			return 25061;
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 7447) { // kitchen
																		// knife
			return 14589;
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] <= 0) {
			switch (c.fightMode) {
				case 0:
					return 422;
				case 1:
					return 423;
				case 2:
					return 422;
			}
		}

		if (weaponName.contains("ivandis")) { // 9097 is slash and crush and
												// 9048 is defensive attack
												// emote
			switch (c.fightMode) {
				case 0:
					return 9097;
				case 1:
					return 9097;
				case 2:
					return 9097;
				case 3:
					return 9097;
			}
		}

		if (weaponName.contains("dart")) {
			return c.fightMode == 1 ? 582 : 582/*6600*/;
		}

		if (weaponName.contains("thrownaxe")) {
			return 10501;// 9055;
		}

		if (weaponName.contains("knife")) {
			if (c.fightMode == 1) {
				return 9057; // rapid
			} else {
				return 9055;
			}
		}

		if (weaponName.contains("javelin")) {
			return 10501; // 9055; //806
		}
		if (weaponName.contains("karil")) {
			return 2075;
		}
		if (weaponName.contains("cross") || weaponName.contains("c'bow")) {
			return 4230;
		}
		if (weaponName.contains("halberd")) {
			return 440;
		}
		if (weaponName.startsWith("dragon dagger")) {
			return 386;// 402
		}
		if (weaponName.endsWith("dagger")) {
			return 386;// 412
		}
		if (weaponName.contains("chaotic rapier")) {
			return 386;
		}

		if (weaponName.contains("2h sword") || weaponName.contains("godsword")
				|| weaponName.contains("saradomin sword")) {
			switch (c.fightMode) {
				case 0:// chop
					return 7049;
				case 1:// slash
					return 7041;
				case 2:// smash
					return 7048;
				case 3:// block
					return 7041;
			}
			// int random = Misc.random(3);
			// switch(random) {
			// case 0:
			// return 7041;
			// case 1:
			// return 7042;
			// case 2:
			// return 7048;
			// case 3:
			// return 7049;
			/*
			 * int random = Misc.random(6); switch(random) { case 0: return
			 * 12009; case 1: return 12002; case 2: return 12003; case 3: return
			 * 12005; case 4: return 7041; case 5: return 7042; case 6: return
			 * 7048;
			 */
			// }

		}

		if (weaponName.contains("longsword")
				|| weaponName.contains("korasi's")) {
			if (c.getMeleeSwingStyle() == MeleeSwingStyle.STAB) {
				return PlayerConstants.EMOTE_STAB_LONGSWORD;
			} else if (c.getMeleeSwingStyle() == MeleeSwingStyle.SLASH) {
				return PlayerConstants.EMOTE_SLASH_LONGSWORD;
			}

			/*
			 * if (Misc.random(7) == 1) { return 10504; } if (Misc.random(7) ==
			 * 2) { return 10501; } if (Misc.random(7) == 3) { return 12031; }
			 * if (Misc.random(7) == 4) { return 386; } if (Misc.random(7) == 5)
			 * { return 15071; } if (Misc.random(7) == 0) { return 15072; } else
			 * {
			 */
			return 12029;
			// }
		}

		if (weaponName.contains("spear")) {
			if (c.getMeleeSwingStyle() == MeleeSwingStyle.STAB) {
				return PlayerConstants.EMOTE_STAB_SPEAR;
			} else if (c.getMeleeSwingStyle() == MeleeSwingStyle.SLASH) {
				return PlayerConstants.EMOTE_SLASH_SPEAR;
			} else if (c.getMeleeSwingStyle() == MeleeSwingStyle.CRUSH) {
				return PlayerConstants.EMOTE_CRUSH_SPEAR;
			}
			return PlayerConstants.EMOTE_STAB_SPEAR;
		}

		if (weaponName.contains("scimitar")) {
			if (c.getMeleeSwingStyle() == MeleeSwingStyle.STAB) {
				return PlayerConstants.EMOTE_STAB_SCIMITAR;
			} else if (c.getMeleeSwingStyle() == MeleeSwingStyle.SLASH) {
				return PlayerConstants.EMOTE_SLASH_SCIMITAR;
			}
			return 12029;
		}


		if (weaponName.contains("sword") || weaponName.contains("rapier")) {
			if (c.getMeleeSwingStyle() == MeleeSwingStyle.STAB) {
				return PlayerConstants.EMOTE_STAB_SWORD;
			} else if (c.getMeleeSwingStyle() == MeleeSwingStyle.SLASH) {
				return PlayerConstants.EMOTE_SLASH_SWORD;
			}
			return 12029;
		}

		if (weaponName.contains("dharok")) {
			switch (c.fightMode) {
				case 0:// attack
					return 2066;
				case 2:// str
					return 2067;
				case 1:// def
					return 2066;
				case 3:// crush
					return 2066;
			}
		}
		if (weaponName.contains("sword")) {
			return 390;// return 451; // old emote, doesn't work, kneeling emote
						// picking something off the floor?
		}
		if (weaponName.contains("karil")) {
			return 2075;
		}
		if ((weaponName.contains("bow") || weaponName.equals("seercull"))
				&& !weaponName.contains("'bow")) {
			return 426;
		}
		if (weaponName.contains("'bow")) {
			return 4230;
		}
		if (weaponName.contains("hasta")) {
			return 4198;
		}
		switch (c.playerEquipment[PlayerConstants.playerWeapon]) { // if you
																	// don't
																	// want to
																	// use
																	// strings
			case 13883: // morrigans axes
				return 10504;
			case 13879: // morrigans javelins
			case 13880:
			case 13881:
			case 13882:
				return 10501;
				
			case 20084:
			case 18353:
				return 2661;
			case 6522:
				return 2614;
			case 10034:
			case 10033:
				return 2779;

			case 4153: // granite maul
			case 30066:
				return 1665;
			case 4726: // guthan
				return 2080;
			case 4747: // torag
				return 0x814;
			case 4710: // ahrim
				return 406;
			case 4755: // verac
				return 2062;
			case 4734: // karil
				return 2075;
			case 4151:
			case 15444:
			case 15443:
			case 15442:
			case 15441:
			case 21371:
			case 21372:
			case 21373:
			case 21374:
			case 21375:
			case 12006:
				return 1658;
			case 6528:
				return 2661;
			case 10887:
				return 5865;
			case 14484: // dragon claws
				return 393;
			case 15241: // hand cannon
				return 12153;
			default:
				return 390; // 390 is temp fix //return 451;
		}
	}

	public static void resetPlayerAttack(Client c) {
		c.npcIndex = 0;
		c.playerIndex = 0;
		c.getPA().resetFollow();
		// if (c.isBot()) {
		// final BotClient bot = (BotClient) c;
		// bot.setPlayerToKill(null);
		// }
	}

	public static boolean usingHally(Client c) {
		if (c.isBot())
			return false;
		return c.sidebarInterfaceInfo.getOrDefault(0, -1) == 8460;
//		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
//			case 3190:
//			case 3192:
//			case 3194:
//			case 3196:
//			case 3198:
//			case 3200:
//			case 3202:
//			case 3204:
//				return true;
//
//			default:
//				return false;
//		}
	}

}
