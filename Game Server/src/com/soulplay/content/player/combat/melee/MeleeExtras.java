package com.soulplay.content.player.combat.melee;

import com.soulplay.Server;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.player.combat.AttackNPC;
import com.soulplay.content.player.combat.AttackPlayer;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.accuracy.MeleeAccuracy;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.definition.RecoilRingEnum;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class MeleeExtras {

	/**
	 * @param c
	 *            - player getting hit by veng
	 * @param o
	 *            - player that had veng on
	 * @param damage
	 */
	public static void appendVengeance(Client c, Client o, int damage) {
		if (damage <= 0) {
			return;
		}
		o.forceChat("Taste vengeance!");

		o.vengOn = false;

		damage = (int) (damage * 0.75);

		damage = c.applyDamageReduction(damage, null);

		damage = c.dealDamage(new Hit(damage, 0, 2, o));
		
		c.putInCombatWithPlayer(o.getId());

		c.getDamageMap().incrementTotalDamage(o.mySQLIndex, damage);
		if (damage >= c.getSkills().getLifepoints()) {
			o.getAchievement().vengeanceKill();
			Titles.defeatPlayerVeng(o);
		}

		if (c.getSkills().getLifepoints() > 0 && c.getSeasonalData().containsPowerUp(PowerUpData.VENGEANCE_HEAL)) {
			int healBack = damage * 80 / 100;
			if (healBack > 0) {
				c.getSkills().heal(healBack);
				c.sendMessageSpam("Your Vengeance heal power up heals you for " + healBack + " health.");
			}
		}
	}

	public static void appendVengeanceNPC(Client c, final NPC npc, int damage) {
		if (damage <= 0) {
			return;
		}
		if (c.underAttackBy2 > 0 && npc != null) {
			c.forceChat("Taste vengeance!");
			c.vengOn = false;

			if (damage > npc.getSkills().getLifepoints()) {
				damage = npc.getSkills().getLifepoints() - 1;
			}

			if ((npc.getSkills().getLifepoints() - damage) > 0) {
				damage = (int) (damage * 0.75);
				if (damage > npc.getSkills().getLifepoints()) {
					damage = npc.getSkills().getLifepoints();
				}
				npc.dealDamage(new Hit(damage, 0, 2, c));
			}
		}
	}

	public static void applyRecoilMob(Player defender, int damage, Mob attacker) {
		if (damage > 0) {
			
			RecoilRingEnum ring = RecoilRingEnum.get(defender.playerEquipment[PlayerConstants.playerRing]);
			if (ring == null)
				return;

			final boolean ringRecoils = defender.getRecoilRings().spendCharge(ring);

			if (ringRecoils) {

				int recDamage = Math.max(1, (int)Math.floor(damage * ring.getRecoilPercent()));
				
				recDamage = attacker.dealDamage(new Hit(recDamage, 0, 3, defender));
				
				if (attacker.isPlayer()) {
					Player p = attacker.toPlayerFast();
					p.putInCombatWithPlayer(defender.getId());
					p.getDamageMap().incrementTotalDamage(defender.mySQLIndex, recDamage);
				}

			}
		}
	}

	public static void applySmite(Client c, final Client c2, boolean smiteActive, int... hits) {
		if (!smiteActive || c2 == null) {
			return;
		}

		int totalDamage = 0;

		for(int hit : hits) {
			totalDamage += hit;
		}

		if (totalDamage <= 0 || c2.getSkills().getPrayerPoints() <= 0) {
			return;
		}

		CombatPrayer.drainPrayerByHit(c2, totalDamage / 4);
	}

	public static void dragon2hSpecial(Client c, BattleState state) {
		if (c.inMulti()) {
			for (Mob other : (state.getDefender().isNpc() ? Server.getRegionManager().getLocalNpcs(state.getDefender()) : Server.getRegionManager().getLocalPlayers(state.getDefender()))) {
				if (!state.getDefender().getCurrentLocation().isWithinDistanceNoZ(other.getCurrentLocation(), 1))
					continue;
				
				if (other.isNpc()) {
					if (other.isDead() || !AttackNPC.attackPrereq(state.getAttacker(), other.toNpc(), false)) {
						continue;
					}
				} else if (other.isPlayer()) {
					if (MeleeRequirements.sameTeam(state.getAttacker(), other.toPlayer()))
						continue;
					if (!MeleeRequirements.checkReqs(state.getAttacker(), (Client) other.toPlayer(), false)/*!MeleeRequirements.combatLevelCheck(state.getAttacker(), other.toPlayer())*/)
						continue;
				}
				
				BattleState state2 = state.copy(c, other);
				state2.setCurrentHit(Misc.newRandom(c.getCombat().calculateMeleeMaxHit(1.10)));
				AttackMob.applyDamage(state2);
			}
		} else { // not in multi so hit the person you were targetting
			AttackMob.applyDamage(state);
		}
	}
	
	public static boolean initGmaulPvP(Client c, Client o, SpecialAttack spec) {
		if (o == null) {
			return false;
		}
		if (o.getSkills().getLifepoints() < 1) {
			return false;
		}
		if (spec == null) {
			spec = SpecialAttack.get(4153);
		}

		int dist = 1;
//		if (c.getMovement().canMove()) {
//			dist += AttackMob.disti;//c.isRunning ? 2 : 1;
//		}
		if (c.withinDistance(o, dist)/*c.withinRangeToHit*/) {
			if (c.getCombat().checkReqs(o)) {
				BattleState state = new BattleState(c, o);
				if (spec != null && spec.canActivate(state)) {
					spec.init(state);
					int delay = 0;
					if (o.teleTimer > 0) {
						delay = 1;
					}
					
					CycleEventHandler.getSingleton().addEvent(c, true, new CycleEvent() {
						
						@Override
						public void stop() {
						}
						
						@Override
						public void execute(CycleEventContainer container) {
							if (c == null || !c.isActive || o == null || !o.isActive || o.getSkills().getLifepoints() < 1 || o.isDead()) {
								container.stop();
								return;
							}

							if (o.teleTimer > 0) {
								if (container.getTick() > 1) {
									container.setTick(1);
								}
								return;
							}

							boolean hit = MeleeAccuracy.successfulHit(c, o).isSuccessfulHit();
							int damage = 0;
							boolean deflect = false;
							if (hit) {
								damage = Misc.newRandom(
										c.getCombat().calculateMeleeMaxHit(1.0));
							}
							if (damage > 0) {
								if (o.prayerActive[Prayers.PROTECT_FROM_MELEE
										.ordinal()]) {
									damage *= .6;
								}
								if (o.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
									deflect = true;
									damage *= .6;
								}
								damage = o.applyDamageReduction(damage, CombatType.MELEE);
								if (o.usingSOLSpec) {
									o.startGraphic(Graphic.create(2320, GraphicType.LOW));
									damage = damage/2;
								}
							}
							if (o.getSkills().getLifepoints() > 0) {
								damage = o.dealDamage(new Hit(damage, 0, 0, c));

								o.getDamageMap().incrementTotalDamage(
										c.mySQLIndex, damage);
								c.setPauseAnimReset(0);
								c.resetAnimations();
								c.startAnimation(1667);
								c.startGraphic(Graphic.create(340, GraphicType.HIGH));
							}

							AttackPlayer.applySkull(c, o);

							o.putInCombatWithPlayer(c.getId());
							c.lastDamageDealt = damage;
							c.lastHitWasSpec = c.usingSpecial;
							c.usingSpecial = false;
							c.lastDamageType = 0;

							// stuff from delayed hit
							o.getPA().closeAllWindows(); // o.getPA().removeAllWindows();
							if (o.playerIndex <= 0 && o.npcIndex <= 0) {
								if (o.autoRet == 1 && !o.walkingToItem && o.clickObjectType <= 0) {
									o.playerIndex = c.getId();
								}
							}
							if (o.inTrade) {
								o.getTradeAndDuel().declineTrade();
							}
							if (damage > 89) {
								c.getAchievement().hit90Melee();
							}
							if (deflect) {
								o.curses().deflect(c, damage, 0);
							}
							if (damage > 0) {
								if (o.vengOn) {
									c.getCombat().appendVengeance(o, damage);
								}
								c.getCombat().applyRecoil(damage, o);
								c.getCombat().applySmite(o, c.prayerActive[Prayers.SMITE.ordinal()], damage);
								c.curses().soulSplit(o, damage);
							}
							if (!o.isLockActions() && (o.getAttackTimer() == 3 || o.getAttackTimer() == 0)) { // block animation
								if (!deflect) {
									o.startAnimation(o.getCombat().getBlockEmote());
								}
							}
							container.stop();
						}
					}, delay); // 0 for instant.
					
					// new code to continue to attack player if used spec
					c.playerIndex = o.getId();
					if (c.followId < 1) {
						c.followId = o.getId();
					}
					return true;
				}
			}
		}
		return false;
	}

	public static void graniteMaulSpecial(Client c, SpecialAttack spec) {
		if (spec == null) {
			spec = SpecialAttack.get(4153);
		}

		int playerHit = 0;
		if (c.recentPlayerHitIndex > 0)
			playerHit = c.recentPlayerHitIndex;
		else if (c.playerIndex > 0)
			playerHit = c.playerIndex;
		if (playerHit > 0
				&& PlayerHandler.players[playerHit] != null
				/*&& PlayerHandler.players[playerHit].underAttackBy == c.getId()*/) {
			final Client o = (Client) PlayerHandler.players[playerHit/* c.playerIndex */];
			if (c.withinDistance(o, 1)) {
				if (initGmaulPvP(c, o, spec)) {
					if (c.getAttackTimer() < 3)
						c.setAttackTimer(2);
				}
			} else if (c.withinDistance(o, 3)) {
				c.followId = c.playerIndex = playerHit;
				queueGmaul(c, spec);
			} else {
				queueGmaul(c, spec);
			}
		} else if (c.npcIndex > 0) {
			// int x = NPCHandler.npcs[c.npcIndex].absX;
			// int y = NPCHandler.npcs[c.npcIndex].absY;
			if (NPCHandler.npcs[c.npcIndex] == null) {
				return;
			}
			if (c.goodDistance(NPCHandler.npcs[c.npcIndex], 2)) {
				// System.out.println("hi0");
				// if (c.getCombat().checkReqs()) { //this is for players,
				// stupids lol
				// System.out.println("hi1");
				BattleState state = new BattleState(c, null);
				if (spec != null && spec.canActivate(state)) {

					// System.out.println("hi2");
					int damage = Misc
							.newRandom(c.getCombat().calculateMeleeMaxHit(1.0));
					if (NPCHandler.npcs[c.npcIndex].getSkills().getLifepoints() - damage < 0) {
						damage = NPCHandler.npcs[c.npcIndex].getSkills().getLifepoints();
					}
					if (NPCHandler.npcs[c.npcIndex].getSkills().getLifepoints() > 0) {
						// c.resetAnimations();

						NPCHandler.npcs[c.npcIndex].dealDamage(new Hit(damage, 0, 0, c));
						c.startAnimation(1667);
						c.startGraphic(Graphic.create(340, GraphicType.HIGH));
						c.queueGmaulSpec = false;
						c.usingSpecial = false;
						spec.init(state);
						// NPCHandler.npcs[c.npcIndex].isDead = true; //TODO:
						// remove, cuz it's just for testing
					}
				}
				// }
			}
		} else {
			//c.queueGmaulSpec = true;
			queueGmaul(c, spec);
		}
		if (c.queueGmaulSpec && c.npcIndex < 1 && playerHit > 0) {
			c.followId = c.playerIndex = playerHit;
		}
	}
	
	public static void queueGmaul(Client c, SpecialAttack spec) {
		if (c.usingSpecial && c.instaSpecActivateTick != Server.getTotalTicks()) {
			c.usingSpecial = false;
			c.getItems().updateSpecialBar();
			return;
		}
		c.usingSpecial = true;
		c.getItems().updateSpecialBar();
		final int correctWeapon = c.playerEquipment[PlayerConstants.playerWeapon];
		final int uuid = Misc.generateRandomActionId();
		c.instaSpecUUID = uuid;
		c.instaSpecActivateTick = Server.getTotalTicks();
		c.addSpecQueue(c, new CycleEvent() {
			int error = 0;
			@Override
			public void stop() {
				c.getItems().updateSpecialBar();
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (error > 1) {
					container.stop();
				}
				error++;
				int playerHit = 0;
				if (c.recentPlayerHitIndex > 0)
					playerHit = c.recentPlayerHitIndex;
				else if (c.playerIndex > 0)
					playerHit = c.playerIndex;
				if (playerHit > 0 && PlayerHandler.players[playerHit] != null) {
					final Client o = (Client) PlayerHandler.players[playerHit];
					if (c.playerEquipment[PlayerConstants.playerWeapon] == correctWeapon && initGmaulPvP(c, o, spec)) {
						if (c.getAttackTimer() < 3)
							c.setAttackTimer(2);
						container.stop();
					}
				}
			}
		});
	}

	public static void handleDragonFireShield(final Client c, final Client o) {
		switch (c.playerEquipment[PlayerConstants.playerShield]) {

		case 11283:
		case 30336:

			if (o != null) {
				if (!c.getCombat().checkReqs(o)) {
					return;
				}
				if (o.getSkills().getLifepoints() <= 0) {
					return;
				}
				if (c.dfsPause && !c.isDev()) {
					c.sendMessage("You can only use the DFS every 2 minutes.");
					return;
				}
				c.dfsPause = true;
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
					}

					@Override
					public void stop() {
						if (c != null) {
							c.dfsPause = false;
						}
					}
				}, 200);
				int damage = Misc.random(20) + 5;

				boolean deflect = false;
				if (damage > 0) {
					if (o.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
						damage = damage * 60 / 100;
						deflect = true;
					} else if (o.prayerActive[Prayers.PROTECT_FROM_MAGIC
					                          .ordinal()]) { // if prayer active reduce damage by half
						damage = damage * 60 / 100;
					}
					damage = c.applyDamageReduction(damage, CombatType.MAGIC);
				}

				final boolean usedDeflect = deflect;

				int perkCount = c.getSeasonalData().getPowerUpAmount(PowerUpData.DRAGONBREATH);
				if (perkCount > 0) {
					damage += damage * (perkCount * 8) / 100;
				} else {
					c.deductDfsCharge();
				}				

				AttackPlayer.applySkull(c, o);
				c.getAchievement().CastDFSSpecial();
				c.faceLocation(o.getX(), o.getY());
				c.startAnimation(6696);
				c.startGraphic(Graphic.create(1165));
				c.setAttackTimer(4);// += 2;//3;

				o.putInCombatWithPlayer(c.getId());
				c.lastDamageDealt = damage;
				c.lastHitWasSpec = c.usingSpecial;

				c.usingSpecial = false;
				c.lastDamageType = 0;

				final int dist = Misc.distanceToPoint(c.getX(), c.getY(), o.getX(), o.getY()); // distance between players
				int hitmarkDelay = 3;
				if (dist > 2) {
					hitmarkDelay += 1;
				}

				if (dist > 5) {
					hitmarkDelay += 1;
				}
				if (dist > 8) {
					hitmarkDelay += 1;
				}

				Location loc = c.getCurrentLocation().copyNew();
				CycleEventHandler.getSingleton().addEvent(c, true,
						new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (o == null || c == null) {
							container.stop();
							return;
						}

						if (c != null && o != null) {

							Projectile proj = new Projectile(1166, loc, o.getCurrentLocation());
							proj.setStartHeight(30);
							proj.setEndHeight(35);
							proj.setStartDelay(0);
							proj.setEndDelay(5 * dist + 10);
							proj.setLockon(o.getProjectileLockon());
							GlobalPackets.createProjectile(proj);
						}

						container.stop();
					}

					@Override
					public void stop() {

					}
				}, 3);

				o.getDamageMap().incrementTotalDamage(c.mySQLIndex, damage);

				final int damageFinal = damage;
				CycleEventHandler.getSingleton().addEvent(c, true,
						new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (o == null || o.isDead()
								|| o.getSkills().getLifepoints() <= 0
								|| c.getSkills().getLifepoints() <= 0) {
							container.stop();
							return;
						}

						int dmg = damageFinal;

						o.startGraphic(Graphic.create(1167, GraphicType.HIGH));
						// c.getCombat().appendHit(o, damage, 0, 2, 2,
						// false);

						dmg = o.dealDamage(new Hit(dmg, 0, 2, c));

						// stuff from delayed hit

						o.getPA().removeAllWindows();
						if (o.playerIndex <= 0 && o.npcIndex <= 0) {
							if (o.autoRet == 1 && !o.walkingToItem && o.clickObjectType <= 0) {
								o.playerIndex = c.getId();
							}
						}
						if (o.inTrade) {
							o.getTradeAndDuel().declineTrade();
						}

						if (usedDeflect) {
							o.curses().deflect(c, dmg, 0);
						}
						if (dmg > 0) {
							if (o.vengOn) {
								c.getCombat().appendVengeance(o, dmg);
							}
							c.getCombat().applyRecoil(dmg, o);
							c.getCombat().applySmite(o, c.prayerActive[Prayers.SMITE.ordinal()], dmg);
							c.curses().soulSplit(o, dmg);
						}
						if (!o.isLockActions() && (o.getAttackTimer() == 3 || o.getAttackTimer() == 0)) { // block animation
							if (!usedDeflect) {
								o.startAnimation(o.getCombat().getBlockEmote());
							}
						}


						container.stop();
					}

					@Override
					public void stop() {

					}
				}, hitmarkDelay);
				break;
			}
		}
	}

	public static void handleDragonFireShieldNPC(final Client c, final NPC npc) {
		if (npc.getSkills().getLifepoints() <= 0) {
			return;
		}
		// if (c.dfsPause) {
		// c.sendMessage("You can only use the DFS every 2 minutes.");
		// return;
		// }
		c.dfsPause = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}

			@Override
			public void stop() {
				if (c != null) {
					c.dfsPause = false;
				}
			}
		}, 240);
		if (npc.getSkills().getLifepoints() <= 1) {
			return;
		}
		
		int damage = Misc.random(20) + 5;
		int perkCount = c.getSeasonalData().getPowerUpAmount(PowerUpData.DRAGONBREATH);
		if (perkCount > 0) {
			damage += damage * (perkCount * 8) / 100;
		} else {
			c.deductDfsCharge();
		}

		c.getAchievement().CastDFSSpecial();
		c.startAnimation(6696);
		c.startGraphic(Graphic.create(1165));
		c.setAttackTimer(4);// += 3;
		Location loc = c.getCurrentLocation().copyNew();
		final int finalDamage = damage;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				if (npc == null || npc.isDead() || !npc.isActive) {
					container.stop();
					return;
				}

				Projectile proj = new Projectile(1166,
						loc, Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()));
				proj.setStartHeight(25);
				proj.setEndHeight(27);
				proj.setStartDelay(0);
				proj.setLockon(npc.getProjectileLockon());
				proj.setEndDelay(10);
				GlobalPackets.createProjectile(proj);

				npc.startGraphic(Graphic.create(1167, GraphicType.HIGH));
				c.getCombat().appendHit(npc, finalDamage, 0, 2, 2, false);

				container.stop();
			}

			@Override
			public void stop() {

			}
		}, 3);
	}
	
	public static void handleVitur(BattleState state) {
		
		AttackMob.applyDamage(state);
		
		Location defenderLoc;
		
		boolean defenderIsNpc = state.getDefender().isNpc();
		
		if (defenderIsNpc) {
			NPC npc = state.getDefender().toNpcFast();
			defenderLoc = npc.getCenterLocation();//Location.create(npc.getXtoPlayerLoc(state.getAttacker().getX()), npc.getYtoPlayerLoc(state.getAttacker().getY()), npc.getZ());
		} else {
			defenderLoc = state.getDefender().getCurrentLocation();
		}
		
		Direction dirToDefender = Direction.getLogicalDirection(state.getAttacker().getCurrentLocation(), defenderLoc);
		
		int gfx;
		
		switch(dirToDefender) {
		case NORTH:
			gfx = 283;
			break;
		case SOUTH:
			gfx = 282;
			break;
		case EAST:
			gfx = 284;
			break;
		case WEST:
			gfx = 285;
			break;
			
			default:
				gfx = 285;
				break;
		}
		
		Location gfxLoc = defenderLoc;//state.getAttacker().getCurrentLocation().transform(dirToDefender);
		
		Server.getStillGraphicsManager().createGfx(gfxLoc, gfx, 100, 15);
		
		if (state.getAttacker().inMulti()) {
			
			boolean grabNpcs = defenderIsNpc;
			
			Direction firstDir = Direction.rotate(dirToDefender);
			Direction secondDir = firstDir.getOpposite();
			
			Location loc2 = gfxLoc.transform(firstDir);
			
			Location loc3 = gfxLoc.transform(secondDir);
			
			for (int i = 0; i < 2; i++) {
			
				for (Mob other : Server.getRegionManager().getLocalMobs(state.getDefender(), grabNpcs, true)) {

					if (i == 0 && !loc2.overlaps(1, other.getCurrentLocation(), other.getSize())) {
						continue;
					} else if (i == 1 && !loc3.overlaps(1, other.getCurrentLocation(), other.getSize())) {
						continue;
					}

					if (other.isNpc()) {
						if (other.isDead() || !AttackNPC.attackPrereq(state.getAttacker(), other.toNpc(), false)) {
							continue;
						}
					} else if (other.isPlayer()) {
						if (MeleeRequirements.sameTeam(state.getAttacker(), other.toPlayer()))
							continue;
						if (!MeleeRequirements.checkReqs(state.getAttacker(), (Client) other.toPlayer(), false)/*!MeleeRequirements.combatLevelCheck(state.getAttacker(), other.toPlayer())*/)
							continue;
					}
					BattleState state2 = state.copy(state.getAttacker(), other);
					state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(i == 0 ? 0.5 : 0.25)));

					AttackMob.applyDamage(state2);
					
					break;
				}

			}
		}
	}
	
	private static final int BULWARK_MAX = 11;
	
	public static void handleBulwark(BattleState state) {
		
		AttackMob.applyDamage(state);
		
		
		boolean defenderIsNpc = state.getDefender().isNpc();
		
		if (state.getAttacker().inMulti()) {
			
			int count = 1;
			
			boolean grabNpcs = defenderIsNpc;
			
			
			for (Mob other : Server.getRegionManager().getLocalMobs(state.getAttacker(), grabNpcs, true)) {

				if (!state.getAttacker().getCurrentLocation().isWithinDeltaDistance(other.getCurrentLocation(), 11)) {
					continue;
				}
				
				if (other.isNpc()) {
					if (other.isDead() || !AttackNPC.attackPrereq(state.getAttacker(), other.toNpc(), false)) {
						continue;
					}
				} else if (other.isPlayer()) {
					if (MeleeRequirements.sameTeam(state.getAttacker(), other.toPlayer()))
						continue;
					if (!MeleeRequirements.checkReqs(state.getAttacker(), (Client) other.toPlayer(), false)/*!MeleeRequirements.combatLevelCheck(state.getAttacker(), other.toPlayer())*/)
						continue;
				}
				
				BattleState state2 = state.copy(state.getAttacker(), other);
				
				state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.0)));

				AttackMob.applyDamage(state2);
				
				AttackMob.putIntoCombat(state.getAttacker(), other);
				
				count++;
				
				if (count >= BULWARK_MAX)
					break;
			}

		}
	}

}
