package com.soulplay.content.player.combat.melee;

import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.items.itemdata.MeleeSwingStyle;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.player.combat.accuracy.MeleeAccuracy;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Mob;

import plugin.item.chargeitems.ViggorasChainmacePlugin;

public class MeleeMaxHit {

	public static final int[] obsidianWeapons = {746, 747, 6523, 6525, 6526,
			6527, 6528};

	public static int bestMeleeDef(Client c) {
		return Math.max(c.getPlayerBonus()[EquipmentBonuses.DEFENSE_STAB.getId()], Math.max(c.getPlayerBonus()[EquipmentBonuses.DEFENSE_SLASH.getId()], c.getPlayerBonus()[EquipmentBonuses.DEFENSE_CRUSH.getId()]));
	}
	
	public static int calculateMeleeMaxHit(Mob entity, double mod) {
		
		double a = effectiveLevel(entity);
		
		double b = equipmentBonus(entity);
		
		int maxHit = (int) Math.floor(1.3f + (a / 10.0f) + (b / 80.0f) + (a*b/640.0f));

		if (entity.isPlayer()) {
			Player c = entity.toPlayerFast();

			if (c.playerEquipment[PlayerConstants.playerWeapon] == 4718
					&& c.playerEquipment[PlayerConstants.playerHat] == 4716
					&& c.playerEquipment[PlayerConstants.playerChest] == 4720
					&& c.playerEquipment[PlayerConstants.playerLegs] == 4722) {
				maxHit *= ((Math.max(0, c.getSkills().getStaticLevel(Skills.HITPOINTS) - c.getSkills().getLifepoints())) / 100.0f) + 1.0f;
			}

			if (c.getCombatTabButtonToggle().getSwingStyle() == MeleeSwingStyle.CRUSH) {
				double inquisitorBonus = MeleeAccuracy.inquisitorArmorBonus(c);
				maxHit *= 1.0 + inquisitorBonus;
			}

			if (hasObsidianEffect(c)) {
				maxHit *= 1.2f;
			}

			if (c.npcIndex > 0) {
				switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
					case ViggorasChainmacePlugin.CHARGED_ID:
						if (c.wildLevel > 0) {
							maxHit *= 1.5f;
						}
						break;
					default:
						if (ItemConstants.isDragonBaneWeapon(c)) {
							NPC npc = NPCHandler.npcs[c.npcIndex];
							if (npc != null && npc.isDragon) {
								maxHit *= 1.2;
							}
						}
						break;
				}
			}

			if (SlayerHelm.wearingSlayerHelmFullOrNormal(c)) { // slayer helmets
				maxHit *= SlayerHelm.slayerHelmBoost(c);
			}

			int powerUpAmount = c.getSeasonalData().getPowerUpAmount(PowerUpData.MELEE_DAMAGE);
			if (powerUpAmount > 0) {
				maxHit *= 1.0 + powerUpAmount * 0.05;
			}
		}

		maxHit *= mod;

		return (int) maxHit;
	}
	
	public static double minigameBoosts(Player c) {
		int vanguard = c.vanguardAmount();
		int battlemage = c.battleMageAmount();

		float dmgBoost = Math.max(battlemage, vanguard) * 0.03f;

		if (c.inCwGame || RSConstants.fightPitsRoom(c.getX(), c.getY()) || c.inSoulWars() || c.inPcGame()) {
			
			if (c.inCwGame) {
				double profoundBoost = c.profoundBoost();
				if (profoundBoost > 1.0) {
					return profoundBoost;
				}
			}
			
			//double hybridBoost = 1.0;
			
			return 1.0f + dmgBoost;
			/*if (vanguard > trickster && vanguard > battlemage) {
				hybridBoost += vanguard * 3 / 100.0;
			} else if (trickster > vanguard && trickster > battlemage) {
				hybridBoost += trickster * 3 / 100.0;
			} else if (battlemage > trickster && battlemage > vanguard) {
				hybridBoost += battlemage * 3 / 100.0;
			}*/

			//c.sendMess("boost:" + hybridBoost);
			//if (hybridBoost > 1.0) {
			//	base += (base * hybridBoost);
			//}
		} else {
			return 1.0f - dmgBoost;
		}
	}
	
	public static double getEffectiveStr(Mob c) {
		double str = Math.round(c.getSkills().getLevel(Skills.STRENGTH) * getPrayerStr(c));
		if (c.isPlayer()) {
			Player p = c.toPlayerFast();
			str += (p.playerEquipment[PlayerConstants.playerAmulet] == 19892 ? 0.05 : 0.0) + getStyleBonus(p);
		}
		return str;
	}
	
	public static double effectiveLevel(Mob entity) {
		int str = entity.getSkills().getLevel(Skills.STRENGTH);
		
		str *= getPrayerStr(entity);
		
		if (entity.isPlayer()) {
			Player p = entity.toPlayerFast();
			
			if (p.fullVoidMelee()) {
				str *= 1.10f;
			} else {
				str *= minigameBoosts(p);
			}
			str += getStyleBonus(p);
		}
		
		return str;
	}
	
	private static double equipmentBonus(Mob entity) {
		return entity.getPlayerBonus()[EquipmentBonuses.MELEE_STR.getId()];
	}

	public static double getPrayerStr(Mob entity) {
		double prayer = 1.0;

		if (entity.isPlayer()) {
			Player c = entity.toPlayerFast();
			if (Prayers.BURST_OF_STRENGTH.isActive(c)) {
				prayer = 1.05;
			} else if (Prayers.SUPERHUMAN_STRENGTH.isActive(c)) {
				prayer = 1.1;
			} else if (Prayers.ULTIMATE_STRENGTH.isActive(c)) {
				prayer = 1.15;
			} else if (Prayers.CHIVALRY.isActive(c)) {
				prayer = 1.18;
			} else if (Prayers.PIETY.isActive(c)) {
				prayer = 1.23;
			} else if (Curses.LEECH_STRENGTH.isActive(c)) {
				prayer = 1.05;
			} else if (Curses.TURMOIL.isActive(c)) {
				prayer = CursesPrayer.getTurmoilMultiplier(c, Skills.STRENGTH);
			}

			prayer += c.strengthMultiplier;
		}

		return prayer;
	}

	public static double getEffectiveStr(Client c) {
		return Math.round(((c.getSkills().getLevel(Skills.STRENGTH)) * (getPrayerStr(c)
				+ (c.playerEquipment[PlayerConstants.playerAmulet] == 19892
						? 0.05
						: 0.0)))
				+ getStyleBonus(c));
	}

	public static int getStyleBonus(Player c) {
		if (c.getWeaponAttackType() == WeaponAttackTypes.AGGRESSIVE)
			return 3;
		if (c.getWeaponAttackType() == WeaponAttackTypes.CONTROLLED)
			return 1;
		return 0;
	}

	public static boolean hasObsidianEffect(Player c) {
		if (c.playerEquipment[2] != 11128) {
			return false;
		}

		for (int weapon : obsidianWeapons) {
			if (c.playerEquipment[3] /* c.lastWeaponUsed */ == weapon) {
				return true;
			}
		}
		return false;
	}

}
