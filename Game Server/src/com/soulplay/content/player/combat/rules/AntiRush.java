package com.soulplay.content.player.combat.rules;

import java.util.ArrayList;

public class AntiRush {

	private static ArrayList<String> rusherUUID = new ArrayList<>();

	public static ArrayList<String> getRusherUUID() {
		return rusherUUID;
	}

	public static void setRusherUUID(ArrayList<String> rusherUUID) {
		AntiRush.rusherUUID = rusherUUID;
	}

}
