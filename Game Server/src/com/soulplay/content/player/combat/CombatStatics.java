package com.soulplay.content.player.combat;

import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;

public class CombatStatics {

	public static final int MELEE_HIT = 0;

	public static final int RANGE_HIT = 1;

	public static final int MAGIC_HIT = 2;

	public static boolean canAttackSlayerMonster(Client c, NPC n,
			boolean resetAttackOnFalse, boolean sendMessage) {

		// SLAYER MONSTERS REQS
		// Abyssal demon
		if (n.npcType == 1615
				&& c.getPlayerLevel()[Skills.SLAYER] < 85) {
			if (sendMessage) {
				if (sendMessage) {
					c.sendMessage(
							"You need a Slayer Level of at least 85 to fight this.");
				}
			}
			if (resetAttackOnFalse) {
				if (resetAttackOnFalse) {
					c.getCombat().resetPlayerAttack();
				}
			}
			return false;
		}
		// Banshee
		if (n.npcType == 1612
				&& c.getPlayerLevel()[Skills.SLAYER] < 15) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 15 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Cave bug
		if (n.npcType == 1832 && c.getPlayerLevel()[Skills.SLAYER] < 7
				|| n.npcType == 5750
						&& c.getPlayerLevel()[Skills.SLAYER] < 7) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 7 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Crawling hand
		if (n.npcType == 1648
				&& c.getPlayerLevel()[Skills.SLAYER] < 5) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 5 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Cave slime
		if (n.npcType == 1831
				&& c.getPlayerLevel()[Skills.SLAYER] < 17) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 17 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Cave crawler
		if (n.npcType == 1600
				&& c.getPlayerLevel()[Skills.SLAYER] < 10) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 10 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Pyrefiend
		if (n.npcType == 1633
				&& c.getPlayerLevel()[Skills.SLAYER] < 30) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 30 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Cockatrice
		if (n.npcType == 1620
				&& c.getPlayerLevel()[Skills.SLAYER] < 25) { // abyssal
																		// demons
																		// &&
																		// c.getPA().getXPForLevel(18)
																		// < 85
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 25 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Rock slug
		if (n.npcType == 1632
				&& c.getPlayerLevel()[Skills.SLAYER] < 20) { // abyssal
																		// demons
																		// &&
																		// c.getPA().getXPForLevel(18)
																		// < 85
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 20 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Basilisk
		if (n.npcType == 1616
				&& c.getPlayerLevel()[Skills.SLAYER] < 40) { // abyssal
																		// demons
																		// &&
																		// c.getPA().getXPForLevel(18)
																		// < 85
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 40 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Dust devil
		if (n.npcType == 1624
				&& c.getPlayerLevel()[Skills.SLAYER] < 65) { // abyssal
																		// demons
																		// &&
																		// c.getPA().getXPForLevel(18)
																		// < 85
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 65 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Bloodveld
		if (n.npcType == 1618
				&& c.getPlayerLevel()[Skills.SLAYER] < 50) { // abyssal
																		// demons
																		// &&
																		// c.getPA().getXPForLevel(18)
																		// < 85
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 50 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Infernal mage
		if (n.npcType == 1643
				&& c.getPlayerLevel()[Skills.SLAYER] < 45) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 45 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Turoth
		if (n.npcType == 1627
				&& c.getPlayerLevel()[Skills.SLAYER] < 55
				|| n.npcType == 1630
						&& c.getPlayerLevel()[Skills.SLAYER] < 55) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 55 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Gargoyle
		if (n.npcType == 1610
				&& c.getPlayerLevel()[Skills.SLAYER] < 75) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 75 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Jelly
		if (n.npcType == 1637
				&& c.getPlayerLevel()[Skills.SLAYER] < 52) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 52 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Kurask
		if (n.npcType == 1608
				&& c.getPlayerLevel()[Skills.SLAYER] < 70) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 70 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Jungle strykewyrm
		if (n.npcType == 9467
				&& c.getPlayerLevel()[Skills.SLAYER] < 73) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 73 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Aquanite
		if (n.npcType == 9172
				&& c.getPlayerLevel()[Skills.SLAYER] < 78) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 78 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Desert strykewyrm
		if (n.npcType == 9465
				&& c.getPlayerLevel()[Skills.SLAYER] < 77) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 77 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Dnechryael
		if (n.npcType == 1613
				&& c.getPlayerLevel()[Skills.SLAYER] < 80) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 80 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Spiritual mage
		if (n.npcType == 6257
				&& c.getPlayerLevel()[Skills.SLAYER] < 83
				|| n.npcType == 6231
						&& c.getPlayerLevel()[Skills.SLAYER] < 83
				|| n.npcType == 6221
						&& c.getPlayerLevel()[Skills.SLAYER] < 83
				|| n.npcType == 6278
						&& c.getPlayerLevel()[Skills.SLAYER] < 83) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 83 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Spiritual warrior
		if (n.npcType == 6219
				&& c.getPlayerLevel()[Skills.SLAYER] < 68
				|| n.npcType == 6255
						&& c.getPlayerLevel()[Skills.SLAYER] < 68
				|| n.npcType == 6229
						&& c.getPlayerLevel()[Skills.SLAYER] < 68
				|| n.npcType == 6278
						&& c.getPlayerLevel()[Skills.SLAYER] < 68) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 68 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Dark beast
		if (n.npcType == 2783
				&& c.getPlayerLevel()[Skills.SLAYER] < 90) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 90 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Ice strykewyrm
		if (n.npcType == 9463
				&& c.getPlayerLevel()[Skills.SLAYER] < 93) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 93 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// Skeletal wyvern
		if (n.npcType == 3068
				&& c.getPlayerLevel()[Skills.SLAYER] < 72) {
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 72 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10694 && c.getPlayerLevel()[Skills.SLAYER] < 5) {//crawling hand
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 5 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10695 && c.getPlayerLevel()[Skills.SLAYER] < 10) {//cave crawler
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 10 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10696 && c.getPlayerLevel()[Skills.SLAYER] < 17) {//cave slime
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 17 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10697 && c.getPlayerLevel()[Skills.SLAYER] < 30) {//pyrefiend
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 30 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10698 && c.getPlayerLevel()[Skills.SLAYER] < 41) {//night spider
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 41 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10699 && c.getPlayerLevel()[Skills.SLAYER] < 52) {//jelly
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 52 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10700 && c.getPlayerLevel()[Skills.SLAYER] < 63) {//spiritual guardian
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 63 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10701 && c.getPlayerLevel()[Skills.SLAYER] < 71) {//seeker
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 71 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10702 && c.getPlayerLevel()[Skills.SLAYER] < 80) {//nechryael
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 80 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10704 && c.getPlayerLevel()[Skills.SLAYER] < 90) {//edimmu
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 90 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		if (n.npcType == 10705 && c.getPlayerLevel()[Skills.SLAYER] < 99) {//soulgazer
			if (sendMessage) {
				c.sendMessage(
						"You need a Slayer Level of at least 99 to fight this.");
			}
			if (resetAttackOnFalse) {
				c.getCombat().resetPlayerAttack();
			}
			return false;
		}
		// SLAYER REQS ENDED
		//soulwars
		if (n.npcType == 8596) {
			if (c.soulWarsTeam == Team.RED) {
				c.sendMessage("You can't atack your own team avatar.");
				return false;
			} else if (c.getPlayerLevel()[Skills.SLAYER] < SoulWars.redAvatar.level) {
				c.sendMessage(
						"You need a Slayer Level of at least " + SoulWars.redAvatar.level + " to fight this.");
				if (resetAttackOnFalse) {
					c.getCombat().resetPlayerAttack();
				}
				return false;
			} else if (!RSConstants.inSoulWarsRedBoss(c.absX, c.absY)) {
				return false;
			}
		} else if (n.npcType == 8597) {
			if (c.soulWarsTeam == Team.BLUE) {
				c.sendMessage("You can't atack your own team avatar.");
				return false;
			} else if (c.getPlayerLevel()[Skills.SLAYER] < SoulWars.blueAvatar.level) {
				c.sendMessage(
						"You need a Slayer Level of at least " + SoulWars.blueAvatar.level + " to fight this.");
				if (resetAttackOnFalse) {
					c.getCombat().resetPlayerAttack();
				}
				return false;
			} else if (!RSConstants.inSoulWarsBlueBoss(c.absX, c.absY)) {
				return false;
			}
		}

		return true;

	}

}
