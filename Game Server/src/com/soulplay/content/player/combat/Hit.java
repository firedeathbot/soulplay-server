package com.soulplay.content.player.combat;

import com.soulplay.game.world.entity.Mob;

public final class Hit {

	public static int HIT_ICON_MAGIC = 2;
	public static int HIT_ICON_RANGE = 1;
	public static int HIT_ICON_MELEE = 0;

	private int damage;
	
	private final int hitSplat;
	
	private final int hitIcon;

	private final Mob source;

	public Hit(int damage) {
		this(damage, 0, -1);
	}

	public Hit(int damage, int hitSplat) {
		this(damage, hitSplat, -1);
	}

	public Hit(int damage, int hitSplat, int hitIcon) {
		this(damage, hitSplat, hitIcon, null);
	}

	public Hit(int damage, int hitSplat, int hitIcon, Mob source) {
		if (damage < 0) {
			damage = 0;
		}
		
		if (hitSplat < 0 || hitSplat > 2) {
			hitSplat = 0;
		}
		
		if (hitIcon < -1 || hitIcon > 4) {
			hitIcon = -1;
		}
		
		this.damage = damage;
		this.hitSplat = hitSplat;
		this.hitIcon = hitIcon;
		this.source = source;
	}

	public int getDamage() {
		return damage;
	}
	
	public void setDamage(int dmg) {
		this.damage = dmg;
	}

	public int getHitSplat() {
		return hitSplat;
	}

	public int getHitIcon() {
		return hitIcon;
	}
	
	public Mob getSource() {
		return source;
	}
	
}
