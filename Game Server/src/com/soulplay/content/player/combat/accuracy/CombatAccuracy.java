package com.soulplay.content.player.combat.accuracy;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.specialattacks.SpecialEffects;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public class CombatAccuracy {

	public static AccuracyType successfulHit(BattleState state) { //TODO: pass battlestate to the formulas!
		if (state.getCombatStyle() == CombatType.MELEE) {
			return MeleeAccuracy.successfulHit(state.getAttacker(), state.getDefender(), state.getAccuracyMod());
		} else if (state.getCombatStyle() == CombatType.RANGED) {
			return RangeAccuracy.isAccurateImpact(state.getAttacker(), state.getDefender(), state.getAccuracyMod(), 1.0);
		} else if (state.getCombatStyle() == CombatType.MAGIC) {
			if (state.getDefender().isPlayer())
				return MagicAccuracy.successfulHit(state.getAttacker(), state.getDefender(), state);
			else if (state.getDefender().isNpc())
				if (state.getSpecEffect() == SpecialEffects.KORASI)
					return MeleeAccuracy.successfulHit(state.getAttacker(), state.getDefender(), state.getAccuracyMod());
				else
					return MagicAccuracy.successfulHit(state.getAttacker(), state.getDefender(), state);
		}
		return AccuracyType.ROLL_FAIL;
	}

	public static void debugAccuracy(Player player, double hit_chance, boolean npcAttacker, CombatType combatType) {
		if (!player.displayAccuracyInfo) {
			return;
		}

		double ratio = hit_chance * 100;
		double off_hit_chance = Math.floor(ratio);
		double def_block_chance = Math.floor(101 - ratio);

		player.sendMessage("acc=" + off_hit_chance + "%, opponent_def=" + def_block_chance + "%, npc_attacker=" + npcAttacker + ", combat_type="+combatType.getName());
	}

	public static boolean rollDefencePerk(Mob mob) {
		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			int percent = 0;
			int blockAmt = player.getSeasonalData().getPowerUpAmount(PowerUpData.BLOCK);
			if (blockAmt > 0) {
				percent += blockAmt * 3;
			}

			int blockMasteryAmt = player.getSeasonalData().getPowerUpAmount(PowerUpData.BLOCK_MASTERY);
			if (blockMasteryAmt > 0) {
				percent += blockMasteryAmt * 2;
			}

			if (player.getSeasonalData().containsPowerUp(PowerUpData.IRONMAN)) {
				percent += 10;
			}

			if (percent > 0) {
				int roll = Misc.random(100);
				if (roll <= percent) {
					player.sendMessageSpam("You block all incoming damage.");
					return true;
				}
			}
		}

		return false;
	}
	
	public static void applyBlockedDamage(Player player) {
		if (player.getSeasonalData().containsPowerUp(PowerUpData.BLOCK_MASTERY)) {
			player.getSkills().heal(2);
			player.sendMessageSpam("Your Block mastery perks heals you.");
			//TODO gfx or smth maybe?
		}
	}

	public static double applyDefencePowerUp(Mob opponent, double augmented_defence) {
		if (opponent.isPlayer()) {
			Player opp = opponent.toPlayerFast();
			int defPow = opp.getSeasonalData().getPowerUpAmount(PowerUpData.DEFENCE);
			if (defPow > 0) {
				boolean shield = ItemConstants.isShield(opp.playerEquipment[PlayerConstants.playerShield]);
				int shieldMod = shield ? 7 : 5;
				augmented_defence += augmented_defence * (defPow * shieldMod) / 100;
			}
		}

		return augmented_defence;
	}

	public static enum AccuracyType {
		HIT(true),
		ROLL_FAIL(false),
		BLOCKED(false);

		private final boolean successfulHit;

		AccuracyType(boolean successfulHit) {
			this.successfulHit = successfulHit;
		}

		public boolean isSuccessfulHit() {
			return successfulHit;
		}

		public static AccuracyType list(boolean test) {
			return test ? HIT : ROLL_FAIL;
		}

	}

}
