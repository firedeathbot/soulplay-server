package com.soulplay.content.player.combat.accuracy;

import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;

public class DefenceFormulas {

	public static double getPrayerDefenseBonus(Player p) {
		double bonus = 1.0;

		if (Prayers.THICK_SKIN.isActive(p)) {
			bonus = 1.05;
		} else if (Prayers.ROCK_SKIN.isActive(p)) {
			bonus = 1.1;
		} else if (Prayers.STEEL_SKIN.isActive(p)) {
			bonus = 1.15;
		} else if (Prayers.CHIVALRY.isActive(p)) {
			bonus = 1.2;
		} else if (Prayers.PIETY.isActive(p) || Prayers.RIGOUR.isActive(p) || Prayers.AUGURY.isActive(p)) {
			bonus = 1.25;
		} else if (Curses.TURMOIL.isActive(p) || Curses.ANGUISH.isActive(p) || Curses.TORMENT.isActive(p)) {
			bonus = CursesPrayer.getTurmoilMultiplier(p, Skills.DEFENSE);
		} else if (Curses.LEECH_DEFENCE.isActive(p)) {
			bonus = 1.05;
		}

		return bonus;
	}

}
