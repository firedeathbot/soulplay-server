package com.soulplay.content.player.combat.accuracy;

import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy.AccuracyType;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Mob;

import plugin.item.chargeitems.CrawsBowPlugin;

public class RangeAccuracy {

	public static double calculateAccuracy(Mob mob) {
		int baseLevel = mob.getSkills().getStaticLevel(Skills.RANGED);
		int weaponRequirement = baseLevel;
		int rangedAttackBonus;

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			int weaponId = player.playerEquipment[3];
			if (weaponId > 0) {
				ItemDefinition itemDef = ItemDefinition.forId(weaponId);
				if (itemDef != null) {
					weaponRequirement = itemDef.getRequirements()[Skills.RANGED];
				} else {
					weaponRequirement = 1;
				}
			} else {
				weaponRequirement = 1;
			}
		}

		double weaponBonus = 0.0;
		if (baseLevel > weaponRequirement) {
			weaponBonus = (baseLevel - weaponRequirement) * .3f;
		}

		int level = mob.getSkills().getLevel(Skills.RANGED);
		double prayer = 1.0;

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			prayer = getPrayerRangeBonus(player) + player.rangedMultiplier;

			if (prayer > 1.0
					&& player.playerEquipment[PlayerConstants.playerWeapon] == 11785) {
				level += 5;
			}

			rangedAttackBonus = player.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()];
		} else {
			NPC npc = mob.toNpcFast();
			CombatStats combatStats = npc.getCombatStats();
			int bonus = 0;
			if (combatStats != null) {
				bonus = combatStats.getRangedAccBonus();
			}

			rangedAttackBonus = bonus;
		}

		double additional = 1.0f; // Slayer helmet/salve/...

		int styleBonus = 0;
		if (mob.getWeaponAttackType() == WeaponAttackTypes.ACCURATE) {
			styleBonus = 3;
		}

		double effective = ((level * prayer) * additional) + styleBonus + weaponBonus;
		return ((effective + 8.0f) * (rangedAttackBonus + 64.0f)) / 10.0f;
	}

	public static double calculateDefence(Mob mob) {
		int level = mob.getSkills().getLevel(Skills.DEFENSE);
		int styleBonus = 0;
		double prayer = 1.0;
		int rangedDefenceBonus;

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();

			if (player.getWeaponAttackType() == WeaponAttackTypes.DEFENSIVE
					|| player.getWeaponAttackType() == WeaponAttackTypes.LONG_RANGED) {
				styleBonus = 3;
			} else if (player.getWeaponAttackType() == WeaponAttackTypes.CONTROLLED) {
				styleBonus = 1;
			}

			prayer = DefenceFormulas.getPrayerDefenseBonus(player);
			rangedDefenceBonus = player.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()];
		} else {
			NPC npc = mob.toNpcFast();
			CombatStats combatStats = npc.getCombatStats();
			int bonus = 0;
			if (combatStats != null) {
				bonus = combatStats.getRangedDefBonus();
			}

			rangedDefenceBonus = bonus;
		}

		double effective = (level * prayer) + styleBonus;
		return ((effective + 8.0f) * (rangedDefenceBonus + 64.0f)) / 10.0f;
	}

	public static double getPrayerRangeBonus(Player p) {
		double bonus = 1.0;

		if (Prayers.SHARP_EYE.isActive(p)) {
			bonus = 1.05;
		} else if (Prayers.HAWK_EYE.isActive(p)) {
			bonus = 1.10;
		} else if (Prayers.EAGLE_EYE.isActive(p)) {
			bonus = 1.15;
		} else if (Prayers.RIGOUR.isActive(p)) {
			bonus = 1.20;
		} else if (Curses.LEECH_RANGED.isActive(p)) {
			bonus = 1.05;
		} else if (Curses.ANGUISH.isActive(p)) {
			bonus = CursesPrayer.getTurmoilMultiplier(p, Skills.RANGED);
		}

		return bonus;
	}

	public static AccuracyType isAccurateImpact(Mob attacker, Mob opponent) {
		return isAccurateImpact(attacker, opponent, 1.0f, 1.0f);
	}

	public static AccuracyType isAccurateImpact(Mob attacker, Mob opponent,
			double accuracyMod, double defenceMod) {
		if (CombatAccuracy.rollDefencePerk(opponent)) {
			return AccuracyType.BLOCKED;
		}

		if (opponent.isNpc()) {
			//TODO do we need this?
			switch (opponent.toNpcFast().npcType) {
				case 2881:
				case 2883:
					return AccuracyType.ROLL_FAIL;
			}
		}

		double mod = PlayerConstants.RANGE_ACCURACY;

		if (attacker.isPlayer()) {
			Player player = attacker.toPlayerFast();
			switch (player.playerEquipment[PlayerConstants.playerWeapon]) {
				case 30092: // twisted bow
					accuracyMod = (140.0f + Math.floor((3.0f*opponent.getSkills().getLevel(Skills.MAGIC)-10.0f) /100.0f) - Math.floor(Math.pow(0.3f * opponent.getSkills().getLevel(Skills.MAGIC)-100.0f, 2.0f) / 100.0f)) /100.0f;
					break;
					
				case 24338: // royal crossbow
					if (opponent.isPlayer()) {
						accuracyMod = 0.5f;
						player.sendMessage("Your weapon is weakened against players.");
					}
					break;
			}
		}
		
		double attack = calculateAccuracy(attacker) * accuracyMod * mod;
		double defence = calculateDefence(opponent) * defenceMod;

		double off_mod = 1.0;
		if (attacker.isPlayer()) {
			Player player = attacker.toPlayerFast();
			if (player.fullVoidRange()) {
				off_mod += 0.10;
			}

			int rangeAcc = player.getSeasonalData().getPowerUpAmount(PowerUpData.RANGE_ACCURACY);
			if (rangeAcc > 0) {
				off_mod += 0.05 * rangeAcc;
			}

			if (opponent.isNpc()) {
				switch (player.playerEquipment[PlayerConstants.playerWeapon]) {
					case CrawsBowPlugin.CHARGED_ID:
						if (player.wildLevel > 0) {
							off_mod += 0.5;
						}
						break;
					default:
						if (ItemConstants.isDragonBaneWeapon(player)) {
							if (opponent.toNpcFast().isDragon) {
								off_mod += 0.30;
							}
						}
						break;
				}

				if (SlayerHelm.wearingSlayerHelmFull(player)) {
					off_mod += SlayerHelm.slayerHelmBoost(player, opponent.toNpcFast());
				}
			}
		}

		defence = CombatAccuracy.applyDefencePowerUp(opponent, defence);
		attack *= off_mod;

		double chance = 0.0;
		if (attack > defence) {
			chance = 1.0f - (defence + 2.0f) / (2.0f * (attack + 1.0f));
		} else {
			chance = attack / (2.0f * (defence + 1.0f));
		}

		double random = Math.random();

		if (attacker.isPlayer()) {
			CombatAccuracy.debugAccuracy(attacker.toPlayerFast(), chance, false, CombatType.RANGED);
		} else if (opponent.isPlayer()) {
			CombatAccuracy.debugAccuracy(opponent.toPlayerFast(), chance, true, CombatType.RANGED);
		}

		return AccuracyType.list(random <= chance);
	}

}
