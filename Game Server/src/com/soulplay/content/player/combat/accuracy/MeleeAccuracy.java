package com.soulplay.content.player.combat.accuracy;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.items.itemdata.MeleeSwingStyle;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault.Role;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy.AccuracyType;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Mob;

import plugin.item.chargeitems.ViggorasChainmacePlugin;

public class MeleeAccuracy {

	public static double getPrayerAttackBonus(Player p) {
		double bonus = 1.0;

		if (Prayers.CLARITY_OF_THOUGHT.isActive(p)) {
			bonus = 1.05;
		} else if (Prayers.IMPROVED_REFLEXES.isActive(p)) {
			bonus = 1.1;
		} else if (Prayers.INCREDIBLE_REFLEXES.isActive(p)) {
			bonus = 1.15;
		} else if (Prayers.CHIVALRY.isActive(p)) {
			bonus = 1.15;
		} else if (Prayers.PIETY.isActive(p)) {
			bonus = 1.2;
		} else if (Curses.LEECH_ATTACK.isActive(p)) {
			bonus = 1.05;
		} else if (Curses.TURMOIL.isActive(p)) {
			bonus = CursesPrayer.getTurmoilMultiplier(p, Skills.ATTACK);
		}

		return bonus;
	}

	public static AccuracyType successfulHit(Mob attacker, Mob opponent) {
		return successfulHit(attacker, opponent, 1.0);
	}

	public static AccuracyType successfulHit(Mob attacker, Mob opponent, double specMod) {
		if (CombatAccuracy.rollDefencePerk(opponent)) {
			return AccuracyType.BLOCKED;
		}

		double mod = PlayerConstants.MELEE_ACCURACY;

		if (attacker.isPlayer()) {
			Player player = attacker.toPlayerFast();
			if (player.getCombatTabButtonToggle().getSwingStyle() == MeleeSwingStyle.OTHER) {
				player.sendMessage("You weapon is not coded right. Notify developer.");
				player.setMeleeSwingStyle(MeleeSwingStyle.SLASH);
			}
		}

		MeleeSwingStyle swingStyle;
		if (attacker.isPlayer()) {
			swingStyle = attacker.toPlayerFast().getCombatTabButtonToggle().getSwingStyle();
		} else {
			//TODO check if npc has any other attack styles or if it says melee in the attack_styles on osrsbox
			swingStyle = MeleeSwingStyle.STAB;
		}

		int off_equipment_bonus = swingStyle.getBonusOff(attacker);
		int def_equipment_bonus = swingStyle.getBonusDef(opponent);

		double augmented_attack = (effectiveMeleeAttack(attacker) + 8.0f) * (off_equipment_bonus + 64.0f);
		double augmented_defence = (effectiveMeleeDefence(opponent) + 8.0f) * (def_equipment_bonus + 64.0f);

		double off_mod = 1.0;
		if (attacker.isPlayer()) {
			Player player = attacker.toPlayerFast();
			if (player.fullVoidMelee()) {
				off_mod += 0.10;
			}

			int meleeAcc = player.getSeasonalData().getPowerUpAmount(PowerUpData.MELEE_ACCURACY);
			if (meleeAcc > 0) {
				off_mod += 0.05 * meleeAcc;
			}

			if (swingStyle == MeleeSwingStyle.CRUSH) {
				double inquisitorBonus = inquisitorArmorBonus(player);
				off_mod += inquisitorBonus;
			}

			if (opponent.isNpc()) {
				NPC npc = opponent.toNpcFast();
				if (player.getBarbarianAssault().getRole() == Role.ATTACKER || player.getBarbarianAssault().getRole() == Role.DEFENDER) {			
					final int attackerLevel = player.getBarbarianAssault().getRoleLevel(player.getBarbarianAssault().getRoleExperience(Role.ATTACKER));	
					final int defenderLevel = player.getBarbarianAssault().getRoleLevel(player.getBarbarianAssault().getRoleExperience(Role.DEFENDER));
	
					if (attackerLevel == 2 || defenderLevel == 2) {				
						off_mod += 0.05;				
					} else if (attackerLevel == 3 || defenderLevel == 3) {
						off_mod += 0.10;
					} else if (attackerLevel == 4 || defenderLevel == 4) {
						off_mod += 0.15;
					} else if (attackerLevel == 5 || defenderLevel == 5) {
						off_mod += 0.20;
					}
				}

				switch(player.playerEquipment[PlayerConstants.playerWeapon]) {
					case 17275:
						if (player.playerEquipment[PlayerConstants.playerShield] == 17273) {
							off_mod += 0.1;
						}
						break;
					case ViggorasChainmacePlugin.CHARGED_ID:
						if (player.wildLevel > 0) {
							off_mod += 0.5;
						}
						break;
					default:
						if (ItemConstants.isDragonBaneWeapon(player) && npc.isDragon) {
							off_mod += 0.2;
						}
						break;
				}

				if (SlayerHelm.wearingSlayerHelmFullOrNormal(player)) {
					off_mod += SlayerHelm.slayerHelmBoost(player, npc);
				}
			}
		}

		augmented_defence = CombatAccuracy.applyDefencePowerUp(opponent, augmented_defence);
		augmented_attack *= off_mod * mod * specMod;

		double hit_chance = 0;

		if (augmented_attack > augmented_defence) {
			hit_chance = 1.0f - (augmented_defence + 2.0f) / (2.0f * (augmented_attack + 1.0f));
		} else {
			hit_chance = augmented_attack / (2.0f * (augmented_defence + 1.0f));
		}

		double random = Math.random();

		if (attacker.isPlayer()) {
			CombatAccuracy.debugAccuracy(attacker.toPlayerFast(), hit_chance, false, CombatType.MELEE);
		} else if (opponent.isPlayer()) {
			CombatAccuracy.debugAccuracy(opponent.toPlayerFast(), hit_chance, true, CombatType.MELEE);
		}

		return AccuracyType.list(random <= hit_chance);
	}

	//Inquisitor bonus changed from 2.5% to 10%, felt underpowered
	public static double inquisitorArmorBonus(Player player) {
		double bonus = 0.0;
		int pieces = 0;

		if (player.playerEquipment[PlayerConstants.playerHat] == 124419) {
			bonus += 0.02;
			pieces++;
		}

		if (player.playerEquipment[PlayerConstants.playerChest] == 124420) {
			bonus += 0.02;
			pieces++;
		}

		if (player.playerEquipment[PlayerConstants.playerLegs] == 124421) {
			bonus += 0.02;
			pieces++;
		}

		if (pieces == 3) {
			bonus += 0.04;
		}

		return bonus;
	}

	private static double effectiveMeleeDefence(Mob mob) {
		int def_current_defence_level = mob.getSkills().getLevel(Skills.DEFENSE);
		float def_stance_bonus = 0.0f;
		double def_defence_prayer_bonus = 1.0;

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();

			if (player.getWeaponAttackType() == WeaponAttackTypes.CONTROLLED) {
				def_stance_bonus = 1.0f;
			} else if (player.getWeaponAttackType() == WeaponAttackTypes.DEFENSIVE) {
				def_stance_bonus = 3.0f;
			}

			def_defence_prayer_bonus = DefenceFormulas.getPrayerDefenseBonus(player) + player.defenceMultiplier;
		}

		return (def_current_defence_level * def_defence_prayer_bonus) + def_stance_bonus;
	}

	private static double effectiveMeleeAttack(Mob mob) {
		int off_current_attack_level = mob.getSkills().getLevel(Skills.ATTACK);
		float off_stance_bonus = 0.0f;
		double off_attack_prayer_bonus = 1.0;

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();

			if (player.getWeaponAttackType() == WeaponAttackTypes.ACCURATE) {
				off_stance_bonus = 3.0f;
			} else if (player.getWeaponAttackType() == WeaponAttackTypes.CONTROLLED) {
				off_stance_bonus = 1.0f;
			}
	
			off_attack_prayer_bonus = getPrayerAttackBonus(player) + player.attackMultiplier;
		}

		return (off_current_attack_level * off_attack_prayer_bonus) + off_stance_bonus;
	}

}
