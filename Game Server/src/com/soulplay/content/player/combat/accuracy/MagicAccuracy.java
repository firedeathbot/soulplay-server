package com.soulplay.content.player.combat.accuracy;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.combat.specialattacks.SpecialEffects;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy.AccuracyType;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

import plugin.item.BrimstoneRingPlugin;
import plugin.item.chargeitems.ThammaronsSceptrePlugin;

public class MagicAccuracy {

	public static double getPrayerMagicBonus(Player p) {
		double bonus = 1.0;

		if (Prayers.MYSTIC_WILL.isActive(p)) {
			bonus = 1.05;
		} else if (Prayers.MYSTIC_LORE.isActive(p)) {
			bonus = 1.10;
		} else if (Prayers.MYSTIC_MIGHT.isActive(p)) {
			bonus = 1.15;
		} else if (Prayers.AUGURY.isActive(p)) {
			bonus = 1.20;
		} else if (Curses.LEECH_MAGIC.isActive(p)) {
			bonus = 1.05;
		} else if (Curses.TORMENT.isActive(p)) {
			bonus = CursesPrayer.getTurmoilMultiplier(p, Skills.MAGIC);
		}

		return bonus;
	}

	public static double effectiveMagicAttack(Mob entity, Mob opponent, BattleState state) {
		int mLvl = entity.getSkills().getLevel(Skills.MAGIC);
		double eA;
		int mAB;
		double mod = 1.0;

		if (entity.isPlayer()) {
			Player player = entity.toPlayerFast();
			int bonus = 8;

			if (ItemConstants.isPoweredStaff(player.playerEquipment[PlayerConstants.playerWeapon])) {
				if (player.getWeaponAttackType() == WeaponAttackTypes.ACCURATE) {
					bonus += 3;
				} else if (player.getWeaponAttackType() == WeaponAttackTypes.LONG_RANGED) {
					bonus += 1;
				}
			}

			double pE = getPrayerMagicBonus(player) + player.magicMultiplier;

			mAB = player.getPlayerBonus()[(state.getSpecEffect() == SpecialEffects.KORASI
					? EquipmentBonuses.MELEE_STR.getId()
					: EquipmentBonuses.ATTACK_MAGIC.getId())];
			eA = mLvl * pE + bonus;

			if (player.fullVoidMage()) {
				mod += 0.45;
			}

			if (opponent.isNpc()) {
				switch (player.playerEquipment[PlayerConstants.playerWeapon]) {
					case ThammaronsSceptrePlugin.CHARGED_ID:
						if (player.wildLevel > 0) {
							mod += 1.0;
						}
						break;
				}

				if (SlayerHelm.wearingSlayerHelmFull(player)) {
					mod += SlayerHelm.slayerHelmBoost(player, opponent.toNpcFast());
				}
			}
		} else {
			NPC npc = entity.toNpcFast();
			CombatStats combatStats = npc.getCombatStats();

			int bonus = 0;
			if (combatStats != null) {
				bonus = combatStats.getMagicAccBonus();
			}

			mAB = bonus;
			eA = mLvl + 9;
		}

		/*
		 * double off_spell_bonus = 0;
		 * 
		 * if (entity.isPlayer()) { Player player = entity.toPlayerFast(); if (state !=
		 * null) { if (state.getSpellsData() == null) { if (state.getSpecEffect() !=
		 * SpecialEffects.KORASI && state.getSpecEffect() != SpecialEffects.SARA_SWORD)
		 * player.sendMessage("You have glitched your magic attack. Notify developer.");
		 * } else { if (player.getSkills().getStaticLevel(Skills.MAGIC) >
		 * state.getSpellsData().getLevelReq()) { off_spell_bonus =
		 * (player.getSkills().getStaticLevel(Skills.MAGIC) -
		 * state.getSpellsData().getLevelReq()) * .3; } } } }
		 */

		double rA = eA * (mAB + 64);
		rA *= mod;

		return rA;
	}

	public static double effectiveMagicDefense(Mob entity) {
		double mLvl = entity.getSkills().getLevel(Skills.MAGIC);
		double eD;
		int mDB;

		if (entity.isPlayer()) {
			Player player = entity.toPlayerFast();

			mLvl *= getPrayerMagicBonus(player);
			mLvl *= 0.7;

			double dLvl = entity.getSkills().getLevel(Skills.DEFENSE) * 0.3 + 2.0; // OSRS wiki says 8, DPS calculators
																					// use 2.

			eD = mLvl + dLvl;
			mDB = player.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()];
		} else {
			NPC npc = entity.toNpcFast();
			CombatStats combatStats = npc.getCombatStats();

			int bonus = 0;
			if (combatStats != null) {
				bonus = combatStats.getMagicDefBonus();
			}

			eD = mLvl + 9;
			mDB = bonus;
		}

		double rD = eD * (mDB + 64);
		return rD;
	}

	public static AccuracyType successfulHit(Mob attacker, Mob opponent, BattleState state) {
		if (CombatAccuracy.rollDefencePerk(opponent)) {
			return AccuracyType.BLOCKED;
		}

		if (opponent.isNpc()) {// TODO do we need this? lol
			int npcType = opponent.toNpcFast().npcType;
			if (npcType == 2881 || npcType == 2882) {
				return AccuracyType.ROLL_FAIL;
			}
		}

		double mod = PlayerConstants.MAGIC_ACCURACY;

		double rA = effectiveMagicAttack(attacker, opponent, state);
		double rD = effectiveMagicDefense(opponent);

		if (attacker.isPlayer()) {
			Player player = attacker.toPlayerFast();
			if (player.playerEquipment[PlayerConstants.playerRing] == BrimstoneRingPlugin.ID
					&& Misc.randomBoolean(4)) {
				player.sendMessage("<col=E00A19>Your attack ignored 10% of your opponent's magic defence.");
				rD *= 0.9;
			}

			int magicAcc = player.getSeasonalData().getPowerUpAmount(PowerUpData.MAGIC_ACCURACY);
			if (magicAcc > 0) {
				mod += 0.05 * magicAcc;
			}
		}

		// c.sendMessage("My Magic Attack:"+augmented_attack+" Their Magic
		// Def:"+augmented_defence);

		rD = CombatAccuracy.applyDefencePowerUp(opponent, rD);
		rA *= mod; // to adjust the accuracy cheaphax way XD

		double hC = 0.0;

		if (rA > rD) {
			hC = 1.0 - (rD + 2.0) / (2.0 * (rA + 1.0));
		} else {
			hC = rA / (2.0 * (rD + 1.0));
		}

		double random = Math.random();

		if (attacker.isPlayer()) {
			CombatAccuracy.debugAccuracy(attacker.toPlayerFast(), hC, false, CombatType.MAGIC);
		} else if (opponent.isPlayer()) {
			CombatAccuracy.debugAccuracy(opponent.toPlayerFast(), hC, true, CombatType.MAGIC);
		}

		return AccuracyType.list(random <= hC);
	}

}
