package com.soulplay.content.player.combat.range;

import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.player.combat.accuracy.RangeAccuracy;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;

import plugin.item.chargeitems.CrawsBowPlugin;

public class RangeMaxHit extends RangeData {

	public static int maxHit(Client c, double modifier) {
		int lvl = c.getSkills().getLevel(Skills.RANGED);
		int rangeStr = c.getPlayerBonus()[EquipmentBonuses.RANGED_STR.getId()];
		
		double percentage = RangeAccuracy.getPrayerRangeBonus(c);
		if (percentage <= 1.00) {
			if (c.playerEquipment[PlayerConstants.playerWeapon] == 30092) {// twisted
	            percentage = 1.15;
	        }
		}

		if (percentage > 1.0
				&& c.playerEquipment[PlayerConstants.playerWeapon] == 11785) {
			lvl += 5;
		}

		if (SlayerHelm.wearingSlayerHelmFull(c)) { // slayer helmets
			percentage *= SlayerHelm.slayerHelmBoost(c);
		}

		int effectiveLevel = (int)(lvl * percentage);
		if (c.getWeaponAttackType() == WeaponAttackTypes.ACCURATE) {
			effectiveLevel = (effectiveLevel + 3);
		}
		
		effectiveLevel += 8;

		if (c.npcIndex > 0) {
			switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
				case CrawsBowPlugin.CHARGED_ID: // craw's bow boost
					if (c.wildLevel > 0) {
						effectiveLevel *= 1.5;
					}
					break;
			}
		}

		if (c.fullVoidRange()) {
			effectiveLevel = (int)(effectiveLevel * 1.20);
		} else {
			int vanguard = c.vanguardAmount();
			int trickster = c.tricksterAmount();

			float dmgBoost = Math.max(vanguard, trickster) * 0.03f;

			if (c.inCw() || RSConstants.fightPitsRoom(c.getX(), c.getY()) || c.inSoulWars() || c.inPcGame()) {

				if (c.inCwGame) {
					double profoundBoost = c.profoundBoost();
					if (profoundBoost > 1.0) {
						effectiveLevel = (int)(effectiveLevel * (profoundBoost/* + 0.05 */));
					}
				}

				//double hybridBoost = 1.0;

				effectiveLevel = (int) (effectiveLevel* (1.0f + dmgBoost));

				/*if (vanguard > trickster && vanguard > battlemage) {
					hybridBoost += vanguard * 3 / 100.0;
				} else if (trickster > vanguard && trickster > battlemage) {
					hybridBoost += trickster * 3 / 100.0;
				} else if (battlemage > trickster && battlemage > vanguard) {
					hybridBoost += battlemage * 3 / 100.0;
				}*/

				//c.sendMess("boost:"+hybridBoost);
				//if (hybridBoost > 1.0) {
				//	b *= hybridBoost;
				//}
			} else {
				effectiveLevel = (int)(effectiveLevel * (1.0f - dmgBoost));
			}
		}
		
		double max = Math.floor(0.5f + effectiveLevel * (rangeStr + 64f) / 640f);
		// c.sendMessage("Max hit is "+(int)max);

		max += Math.floor(max * c.rangedMultiplier);

		if (modifier != 1.0)
			max = Math.floor(max * modifier);
		if (c.usingWhiteWhipSpec) {
			max = max / 2;
		}
		return (int) max;
	}

	public static boolean wearingHexHunter(int weapon) {
		switch (weapon) {
			case 15836:
			case 17295:
			case 21321:
			case 21322:
			case 21323:
			case 21324:
			case 21325:
			case 21326:
			case 21327:
			case 21328:
			case 21329:
			case 21330:
			case 21331:
			case 21332:
				return true;
		}

		return false;
	}

}
