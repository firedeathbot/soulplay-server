package com.soulplay.content.player.combat.range;

import com.soulplay.Server;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.event.easter.BunnyBoss;
import com.soulplay.content.player.combat.AttackNPC;
import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class RangeExtras {
	
	public static Graphic EXPLOSION_GFX = Graphic.create(Graphic.getOsrsId(157));

	public static void applyChinchompaMulti(BattleState state) {
		int count = 1;
		int maxCount = 9;
		if (state.getAmmo().getItemId() == 30356) {
			maxCount++;
		}
		
		int dist = (int) state.getAttacker().getCurrentLocation().getDistance(state.getDefender().getCurrentLocation());
		
		//https://i.imgur.com/85K2dzM.png
		switch (state.getAttacker().getCombatTabButtonToggle().getAttackType()) {
		case ACCURATE:
			if (dist >= 7) {
				state.setAccuracyMod(0.5);
			} else if (dist >= 4) {
				state.setAccuracyMod(0.75);
			}
			break;
			
		case RAPID:
			if (dist <= 3 || dist >= 7) {
				state.setAccuracyMod(0.75);
			}
			break;
			
		case LONG_RANGED:
			if (dist <= 6) {
				state.setAccuracyMod(0.75);
			} else if (dist <= 3) {
				state.setAccuracyMod(0.5);
			}
			break;
			
		default:
			break;
		}
		
		if (dist >= 6)
			state.setHitDelay(3);
		else if (dist < 6)
			state.setHitDelay(2);
		
		AttackMob.applyDamage(state);
		
		final boolean missed = !state.isSuccessfulHit();
		
		state.getDefender().startGraphic(Graphic.create(EXPLOSION_GFX.getId(), state.getProj().getEndDelay(), GraphicType.HIGH));
		
		if (state.getAttacker().inMulti()) {
			
			boolean grabNpcs = state.getDefender().isNpc();
			
			for (Mob other : Server.getRegionManager().getLocalMobs(state.getDefender(), grabNpcs, true)) {
				if (state.getDefender() == other) // already hit this player, so skip
					continue;
				if (!state.getDefender().getCurrentLocation().isWithinDistanceNoZ(other.getCurrentLocation(), 1))
					continue;
				
				if (other.isNpc()) {
					if (other.isDead() || !AttackNPC.attackPrereq(state.getAttacker(), other.toNpc(), false)) {
						continue;
					}
				} else if (other.isPlayer()) {
					if (MeleeRequirements.sameTeam(state.getAttacker(), other.toPlayer()))
						continue;
					if (!MeleeRequirements.checkReqs(state.getAttacker(), (Client) other.toPlayer(), false)/*!MeleeRequirements.combatLevelCheck(state.getAttacker(), other.toPlayer())*/)
						continue;
				}
				BattleState state2 = state.copy(state.getAttacker(), other);
				if (!missed) {
					state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.0)));
				} else {
					state2.setSuccessfulHit(false);
					state2.setCurrentHit(0);
				}
				AttackMob.applyDamage(state2);

				count++;
				if (count >= maxCount)
					break;
			}
		}
	}
	
	private static final Graphic CHOCOLATE_HIT_GFX = Graphic.create(2858, 90, GraphicType.HIGH);
	
	public static void shootEggsterminator(Client c, Player o) {
		if (c.getRangeWeapon() == null)
			return;
		int requiredDist = c.getRangeWeapon().getCombatDistance();
		if (o.isDead() || o.disconnected || o.getZ() != c.getZ()) {
			c.getCombat().resetPlayerAttack();
			return;
		}
		int dist = c.distanceToPoint(o.getX(), o.getY());
		if (dist > 20) {
			c.getCombat().resetPlayerAttack();
			return;
		}
		if (c.blockedShot) {
			return;
		}
		if (c.getAttackTimer() > 0) {
			if (dist <= requiredDist) {
				c.sendMess("resetting walk here");
				c.getMovement().resetWalkingQueue();
			}
			return;
		}

		o.startGraphic(CHOCOLATE_HIT_GFX);

		c.setAttackTimer(4);
		c.startAnimation(c.getRangeWeapon().getAnimation());
		c.getMovement().resetWalkingQueue();
		c.getCombat().resetPlayerAttack();
		
		Projectile proj = new Projectile(BunnyBoss.CHOCOLATE_PROJECTILE, c.getCurrentLocation(), o.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(25);
		proj.setStartDelay(30);
		proj.setEndDelay(90);
		proj.setSlope(1);
		proj.setLockon(o.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		
		c.faceLocation(o.getCurrentLocation());
	}

}
