package com.soulplay.content.player.combat.range;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.specialattacks.SpecialEffects;
import com.soulplay.content.items.impl.MagicStaff;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.Audio;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

/**
 * Represents a bolt effect.
 * @author Vexia
 * @author Aero
 */
public enum BoltEffect {
	OPAL(new int[] { 9236, 121932 }, Graphic.create(749), new Audio(2918)) {

		@Override
		public void impact(BattleState state) {
			state.setCurrentHit(Misc.random(state.getAttacker().getCombat().rangeMaxHit(1.25)));
			super.impact(state);
		}
		
		@Override
		public boolean canFire(BattleState state, int chances) {
			return super.canFire(state, 5);
		}

	},
	JADE(new int[] { 9237, 121934 }, Graphic.create(755), new Audio(2916)) {

		@Override
		public void impact(BattleState state) {
			if (state.getDefender().isPlayer()) {
				state.getDefender().stunEntity(3, Graphic.create(755));
			}
			super.impact(state);
		}

		@Override
		public boolean canFire(BattleState state, int chances) {
			boolean success = false;
			if (state.getDefender().isPlayer()) {
				Player p = state.getDefender().toPlayer();
				double level = p.getSkills().getLevel(Skills.AGILITY);
				double req = 80;
				double successChance = Math.ceil((level * 50 - req * 15) / req / 3 * 4);
				int roll = Misc.random(99);
				success = successChance >= roll;
			}
			return super.canFire(state, 6) && !success;
		}

	},
	PEARL(new int[] { 9238, 121936 }, Graphic.create(750), new Audio(2920)) { // basically water damage, effective against fire monsters.

		@Override
		public void impact(BattleState state) {
			state.setCurrentHit(Misc.random(state.getAttacker().getCombat().rangeMaxHit(Misc.randomDouble(1.25, 1.30))));
			super.impact(state);
		}

		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isPlayer()) {
				if (MagicStaff.wearingStaff(state.getDefender().toPlayer(), MagicStaff.FIRE)) // has water staff equipped
					return false;
			}
			//TODO: code npcs who are fire type. they need to get increased damage.
			return super.canFire(state, 6);
		}
	},
	TOPAZ(new int[] { 9239, 121938 }, Graphic.create(757), new Audio(2914)) {
		@Override
		public void impact(BattleState state) {
			if (state.getDefender().isPlayer()) {
				Player p = state.getDefender().toPlayer();
				int level = (int) ((int) p.getSkills().getLevel(Skills.MAGIC) * 0.03);
				p.getSkills().updateLevel(Skills.MAGIC, -level, 0);
			}
			super.impact(state);
		}
		
		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isNpc())
				return super.canFire(state, 10);
			return super.canFire(state, 4);
		}
	},
	SAPPHIRE(new int[] { 9240, 121940 }, Graphic.create(759, GraphicType.HIGH), new Audio(2912)) {
		@Override
		public void impact(BattleState state) {
			if (state.getDefender() instanceof Player) {
				Player o = state.getDefender().toPlayer();
				int give = (int) (o.getSkills().getPrayerPoints() * 0.05);
				if (give > 0) {
					CombatPrayer.drainPrayerByHit(o, give); //o.getSkills().decrementPrayerPoints(give);
					if (o.getSkills().getPrayerPoints() == 0)
						CombatPrayer.resetPrayers(o);
					state.getAttacker().getSkills().incrementPrayerPoints(give);
				}
			}
			super.impact(state);
		}
		
		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isNpc())
				return super.canFire(state, 10);
			return super.canFire(state, 5);
		}
	},
	EMERALD(new int[] { 9241, 121942 }, Graphic.create(752), new Audio(2919)) {
		@Override
		public void impact(BattleState state) {
			if (state.getDefender().isPlayer()) {
				state.getDefender().toPlayerFast().getDotManager().applyPoison(5, state.getAttacker());
			}
			super.impact(state);
		}
		
		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isNpc())
				return super.canFire(state, 55);
			return super.canFire(state, 54);
		}
	},
	RUBY(new int[] { 9242, 121944 }, Graphic.create(754), new Audio(2915)) {

		@Override
		public void impact(BattleState state) {
			int victimDamage = (int) (state.getDefender().getSkills().getLifepoints() * 0.20);
			int attackerDamage = (int) (state.getAttacker().getSkills().getLifepoints() * 0.10);

			if (victimDamage > 100) {
				victimDamage = 100;
			}

			if (victimDamage > state.getDefender().getSkills().getLifepoints()) {
				victimDamage = state.getDefender().getSkills().getLifepoints();
			}

			state.getAttacker().dealDamage(new Hit(attackerDamage, 0, -1));
			if (state.getDefender().isNpc()) {
				state.getDefender().toNpc().dealDamage(new Hit(victimDamage, 0, -1, state.getAttacker()));
			} else if (state.getDefender().isPlayer()) {
				state.getDefender().toPlayer().dealTrueDamage(victimDamage, 1, null, state.getAttacker());
			}

			super.impact(state);
		}

		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isNpc()) {
				NPC n = state.getDefender().toNpc();
				if (n.npcType == 8528 || n.npcType == 8596 || n.npcType == 8597) {
					state.getAttacker().sendMessage("Ruby(e) special has no effect on this monster.");
					return false;
				}
			}

			int playerPoints = (int) (state.getAttacker().getHitPoints() * 0.10);
			if (playerPoints < 1 || state.getAttacker().getHitPoints() - playerPoints < 1) {
				return false;
			}

			return super.canFire(state, state.getDefender().isPlayer() ? 11 : 6);
		}
	},
	DIAMOND(new int[] { 9243, 121946 }, Graphic.create(758), new Audio(2913)) {
		@Override
		public void impact(BattleState state) {
			state.setCurrentHit(Misc.random(state.getAttacker().getCombat().rangeMaxHit(1.15)));
			state.setSpecEffect(SpecialEffects.IGNORE_ARMOR_ONLY);
			super.impact(state);
		}
		
		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isNpc())
				return super.canFire(state, 10);
			return super.canFire(state, 5);
		}
	},
	DRAGON(new int[] { 9244, 121948 }, Graphic.create(756), new Audio(2915)) {

		@Override
		public void impact(BattleState state) {
			int rangedLevel = state.getAttacker().getSkills().getLevel(Skills.RANGED);
			if (rangedLevel < 1)
				rangedLevel = 1;
			int maxInc = rangedLevel / 5;
			boolean displayMax = state.getAttacker().displayMaxHit;
			if (displayMax)
				state.getAttacker().displayMaxHit = false;
			int maxHit = maxInc + state.getAttacker().getCombat().rangeMaxHit(1.0);
			if (displayMax)
				state.getAttacker().displayMaxHit = true;
			if (state.getAttacker().displayMaxHit) {
				state.getAttacker().sendMessage("Range Max hit dragonStone: " + maxHit);
			}
			state.setCurrentHit(Misc.random(maxHit));
			super.impact(state);
		}

		@Override
		public boolean canFire(BattleState state, int chances) {
			if (state.getDefender().isNpc()) {
				NPC n = (NPC) state.getDefender().toNpc();
				if (n.getNpcName().toLowerCase().contains("fire") || n.getNpcName().toLowerCase().contains("dragon")) {
					return false;
				}
			}
			if (state.getDefender().isPlayer()) {
				if (state.getDefender().toPlayer().getPA().wearingAntifireShield()
						|| state.getDefender().toPlayer().antiFirePot != null) {
					return false;
				}
			}
			return super.canFire(state, 6);
		}

	},
	ONYX(new int[] { 9245, 121950 }, Graphic.create(753), new Audio(2917)) {

		@Override
		public void impact(BattleState state) {
			state.setCurrentHit(Misc.random(state.getAttacker().getCombat().rangeMaxHit(1.20)));
			state.getAttacker().healHitPoints((int)(state.getCurrentHit() * 0.25));
//			state.getAttacker().setAttribute("onyx-effect", GameWorld.getTicks() + 12);
			super.impact(state);
		}

		@Override
		public boolean canFire(BattleState state, int chances) {
//			if (state.getAttacker().getAttribute("onyx-effect", 0) > GameWorld.getTicks()) {
//				return false;
//			}
//			if (state.getDefender().isNpc()) { //TODO: add back once the undead npcs are coded
//				NPC n = (NPC) state.getDefender().toNpc();
//				if (n.getTask() != null && n.getTask().isUndead()) {
//					return false;
//				}
//			}
			if (state.getDefender().isPlayer())
				return super.canFire(state, 10);
			else
				return super.canFire(state, 11);
		}
	};
	
	public static Map<Integer, BoltEffect> map = new HashMap<>();

	
    public static void load() {
    	/* empty */
    }
    
	static {
		for (BoltEffect effect : values()) {
			for (int id : effect.getItemIds()) {
				map.put(id, effect);
			}
		}
	}
	
	/**
	 * The item id of the bolt.
	 */
	private final int[] itemIds;

	/**
	 * The graphics to send.
	 */
	private final Graphic graphics;

	/**
	 * The sound to send.
	 */
	private final Audio sound;

	/**
	 * Constructs a new {@Code BoltEffect} {@Code Object}
	 * @param itemId the item id.
	 * @param graphics the graphics.
	 * @param sound the sound.
	 */
	private BoltEffect(int[] itemIds, Graphic graphics, Audio sound) {
		this.itemIds = itemIds;
		this.graphics = graphics;
		this.sound = sound;
	}

	/**
	 * Constructs a new {@Code BoltEffect} {@Code Object}
	 * @param itemId the id.
	 * @param graphics the graphics.
	 */
	private BoltEffect(int[] itemIds, Graphic graphics) {
		this(itemIds, graphics, null);
	}

	/**
	 * Handles the impact.
	 * @param state the battle state.
	 */
	@SuppressWarnings("unused")
	public void impact(BattleState state) {
		Client entity = state.getAttacker();
		Mob victim = state.getDefender();
		if (sound != null && victim != null && victim.isPlayer() && victim.toPlayer().isActive) {
			sound.send(victim.toPlayer(), true);
		}
		if (graphics != null) {
			victim.startGraphic(graphics);
		}
	}

	/**
	 * Checks if the effect can fire.
	 * @param state the state.
	 * @return {@code True} if so.
	 */
	public boolean canFire(BattleState state, int chances) {
		return Misc.random(99) < chances;
	}

	/**
	 * Gets an effect by the id.
	 * @param id the id.
	 * @return the effect.
	 */
	public static BoltEffect forId(int id) {
		return map.get(id);
	}

	/**
	 * Gets the itemId.
	 * @return the itemId
	 */
	public int[] getItemIds() {
		return itemIds;
	}

}