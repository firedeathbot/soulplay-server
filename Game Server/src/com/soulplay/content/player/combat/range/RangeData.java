package com.soulplay.content.player.combat.range;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;

public class RangeData {

	

	public static int getProjectileEndHeight(Client c, String weaponName) {
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926 || weaponName.contains("dart")) {
			return 35;
		} else if (weaponName.contains("knife")
				|| weaponName.contains("javelin")
				|| weaponName.contains("thrownaxe")
				|| weaponName.contains("cannon")
				|| weaponName.contains("throwing")) {
			return 35;
		} else if (weaponName.contains("cross")
				|| weaponName.contains("c'bow")) {
			return 35;
		} else if ((weaponName.contains("bow")
				|| weaponName.equals("seercull"))) {
			return 35;
		}

		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {

			// case 1234:
			// return 50;

			default:
				return 35;
		}
	}

	public static int getProjectileShowDelay(Client c) {
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 13883) {
			// throwing
			// axe
			return 100;
		}
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926) // blowpipe same as darts
			return 32;

		int str = 53;
		for (int element : DARTS) {
			if (c.playerEquipment[PlayerConstants.playerWeapon] == element) {
				return 32;
			}
		}
		return str;
	}
	static int[] DARTS = {806, 807, 808, 809, 810, 811, 3093, 10033, 10034, 11230,};

	public static int getProjectileSpeed(Client c) {
		switch (c.playerEquipment[3]) {
			case 10033:
			case 10034:
				return 60;
			case 13883:
				return 60;
		}
		return 70;
	}

	public static int getProjectileStartHeight(Client c, String weaponName) {
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926 || weaponName.contains("dart")) {
			return 50;
		} else if ((weaponName.contains("bow")
				|| weaponName.equals("seercull"))) {
			return 50;
		} else if (weaponName.contains("knife")
				|| weaponName.contains("javelin")
				|| weaponName.contains("thrownaxe")
				|| weaponName.contains("cannon")
				|| weaponName.contains("throwing")) {
			return 31;
		} else if (weaponName.contains("cross")
				|| weaponName.contains("c'bow")) {
			return 40;
		}

		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {

			// case 1234:
			// return 50;

			default:
				return 31;
		}
	}

	public static int getRangeStr(int i) {
		ItemDefinition itemDef = ItemDefinition.forId(i);
		if (itemDef == null) {
			return 0;
		}

		return itemDef.getCombatStats().getRangeStrengthBonus();
	}

}
