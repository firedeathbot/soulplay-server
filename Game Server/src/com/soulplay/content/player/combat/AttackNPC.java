package com.soulplay.content.player.combat;

import com.soulplay.Config;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.pets.pokemon.Pokemon;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.NpcWeakness;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;

public class AttackNPC {

	public static void attackNpc(Client c, int i) {
		final NPC n = NPCHandler.npcs[i];

		if (n == null) {
			return;
		}
		// int playerWeapon =
		// c.playerEquipment[PlayerConstants.playerWeapon];
		// int npcId = NPCHandler.npcs[i].npcType;

		if (c.performingAction) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.hidePlayer && !Config.isOwner(c)) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.respawnTimer > 0) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.isStunned()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (!attackPrereq(c, n, true)) {
			return;
		}

		if (c.playerEquipment[PlayerConstants.playerWeapon] > 0
				&& ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]).contains("(broken)")) {
			c.sendMessage("Your weapon is broken.");
			c.getMovement().resetWalkingQueue();
			c.getCombat().resetPlayerAttack();
			return;
		}
		
		if (c.selectPokemonAttack == true) {
			c.selectPokemonAttack = false;
		    Pokemon.sendPetToAttack(c, n, n.npcIndex);
	        c.getMovement().resetWalkingQueue();
		    if (!c.underAttackByNpc()) {
			    c.getCombat().resetPlayerAttack();
		    }
		    return;
		}

		if (c.blockedShot) {
			return;
		}

		c.followId2 = i;
		c.followId = 0;
		
		
		if (c.getAttackTimer() <= 0) {

			if (n.isBarbAssaultNpc() && BarbarianAssault.handleAttackNpc(c, i)) {
				return;
			}

			c.face(n);

			// old pi code
			c.followId2 = n.npcIndex;
			c.oldNpcIndex = i;

			if (AttackMob.initHit(c, n)) {

				n.underAttackBy = c.getId();
				n.lastDamageTaken = System.currentTimeMillis();

				if (n.underAttackBy > 0 && NPCHandler.getsPulled(n.getId())) {
					n.setKillerId(c.getId());
				} else if (n.underAttackBy < 0 && !NPCHandler.getsPulled(n.getId())) {
					n.setKillerId(c.getId());
				}

				if (c.usingSpellFromSpellBook) { // if (!c.autoCasting &&
					// c.spellId <= 0) {
					c.npcIndex = 0; // TODO: wtf is this shit???
					c.followId2 = 0; // TODO: wtf is this shit???
					c.resetFace();
					c.faceLocation(n.getX(), n.getY());
					c.getPacketSender().resetMinimapFlagPacket();
				}

				c.curses().applynpcLeeches(i);
				c.startCurseBoostResetTimer();
				
				if (c.summoned != null && c.summoned.isPokemon/* && c.inPetDungeon()*/) {
					c.summoned.killNpc = n.getId();
			    }
				
				if (c.getTransformId() > -1 && !Config.isOwner(c)) {
					c.transform(-1);
					c.getMovement().resetWalkingQueue();
					c.getPA().resetMovementAnimation();
				}
				
			}
		}
	}

	public static boolean canAttackNpc(int id) {
		switch (id) {
			case 3782://Pest control knight
			case 944://Combat instructor
				return false;
		}

		return true;
	}

	public static boolean attackPrereq(Client c, NPC n, boolean resetAttack) {
		if (!canAttackNpc(n.npcType)) {
			return false;
		}

		if (n.isDead() || n.getSkills().getMaximumLifepoints() <= 0) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}



		if (c.getAutoCastSpell() == SpellsData.CRUMBLE_UNDEAD || c.getSingleCastSpell() == SpellsData.CRUMBLE_UNDEAD) {
			if (!NPCHandler.npcs[c.npcIndex].getWeakness().contains(NpcWeakness.UNDEAD)) {
				c.sendMessage(
						"You can only attack undead monsters with this spell.");
				c.getMovement().resetWalkingQueue();
				c.getMovement().resetDestination();
				c.getPacketSender().resetMinimapFlagPacket();
				return false;
			}
		}

		if (c.getZ() != n.getHeightLevel()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		if (n.isPetNpc()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}



		if (!n.isAttackable()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				c.sendMessage("This NPC is not taking any damage right now.");
			}
			return false;
		}

		if ((n.spawnedBy == c.getId())
				&& n.isSummoningSkillNpc) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				c.sendMessage("You cannot attack your summoning monster.");
			}
			return false;
		}

		if (n.isSummoningSkillNpc) {
			Player OtherPlayer = PlayerHandler.players[n.spawnedBy];
			if (!c.inWild() || !OtherPlayer.inWild()) {
				if (resetAttack) {
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					c.sendMessage("This monster is not in the wilderness.");
				}
				return false;
			}
		}

		if (n.isSummoningSkillNpc) {
			Player OtherPlayer2 = PlayerHandler.players[n.spawnedBy];
			if (!c.inMulti() || !OtherPlayer2.inMulti()) {
				if (resetAttack) {
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					c.sendMessage("This monster is not in multi.");
				}
				return false;
			}
		}

		if ((n.spawnedBy != c.getId())
				&& (n.spawnedBy > 0)
				&& !n.isSummoningSkillNpc
				&& !c.getBarbarianAssault().inArena()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				c.sendMessage("This monster was not spawned for you.");
			}
			return false;
		}

//		if (RSConstants.dzDungeon(n.getX(),
//				n.getY())) {
//			if (DailyTasks.dungeonCaveKills.containsKey(c.getName())) {
//				if (DailyTasks.dungeonCaveKills.get(c.getName())
//						.intValue() > 20) {
//					if (resetAttack) {
//						c.sendMessage(
//								"You have already reached your daily limit of 20 kills.");
//						c.getMovement().resetWalkingQueue();
//						c.getCombat().resetPlayerAttack();
//					}
//					return false;
//				}
//			}
//		}

		if (n.underAttackBy > 0
				&& n.underAttackBy != c.getId()
				&& !n.inMulti()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				c.sendMessage("This monster is already in combat.");
			}
			return false;
		}
		if ((c.underAttackBy > 0 || c.underAttackBy2 > 0)
				&& c.underAttackBy2 != n.getNpcIndex() && !c.inMulti()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				c.sendMessage("I am already under attack.");
			}
			return false;
		}
		if (n.spawnedBy != c.getId()
				&& n.spawnedBy > 0
				&& !c.getBarbarianAssault().inArena()) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				c.sendMessage("This monster was not spawned for you.");
			}
			return false;
		}

		if (PlayerConstants.getDistance(c.getX(), n.getX(),
				c.getY(), n.getY()) > 20) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		if (n.isDigging) {
			if (resetAttack) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
			}
			return false;
		}

		// TODO: add slayer reqs here
		if (!CombatStatics.canAttackSlayerMonster(c, n,
				resetAttack, resetAttack)) {
			return false;
		}

		return true;
	}

	public static boolean kalphite1(int i) {
		switch (NPCHandler.npcs[i].npcType) {
		case 1158:
			return true;
		}
		return false;
	}

	public static boolean kalphite2(int i) {
		switch (NPCHandler.npcs[i].npcType) {
		case 1160:
			return true;
		}
		return false;
	}

	public static int slayerHelmetDamage(int damage) {
		double newDamage = damage / 100d;
		newDamage = newDamage * 15 + damage;
		return (int) Math.floor(newDamage);
	}

}
