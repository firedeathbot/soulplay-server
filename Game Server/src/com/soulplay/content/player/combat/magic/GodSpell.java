package com.soulplay.content.player.combat.magic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.info.Timers;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public class GodSpell {
	
	private final Player player;
	
	public GodSpell(Player player) {
		this.player = player;
	}

	private static final int SPELL_COUNT = 3;

	private static final int REQUIREMENT_COUNT = 100;
	
	private static final String REQ_MSG = "You must cast this spell %d times in Mage Arena first.";
	
	private static final String SPELL_COUNT_MSG = "Spell count = %d";
	
	private int[] requirements = new int[SPELL_COUNT];

	public int[] getRequirements() {
		return requirements;
	}
	
	public void loadJsonString(String jsonString) {
		if (jsonString == null)
			return;
		requirements = GsonSave.gsond.fromJson(jsonString, new TypeToken<int[]>(){}.getType());
	}

	public boolean canCastGodSpell(SpellsData spell) {
		if (spell != SpellsData.CLAWS_OF_GUTHIX && spell != SpellsData.SARADOMIN_STRIKE && spell != SpellsData.FLAMES_OF_ZAMORAK) {
			return true;
		}

		final int index = spellToIndex(spell);

		if (index == -1) {
			return true;
		}

		if (requirements[index] < REQUIREMENT_COUNT && !inMageArena()) {
			player.sendMessage(String.format(REQ_MSG , REQUIREMENT_COUNT));
			return false;
		}

		return true;
	}

	public void castGodSpell(SpellsData spell) {
		final int index = spellToIndex(spell);
		if (index == -1) {
			return;
		}

		if (requirements[index] < REQUIREMENT_COUNT && inMageArena()) {
			requirements[index] += 1;
			player.sendMessageSpam(String.format(SPELL_COUNT_MSG, requirements[index]));
			saveToDbMisc();
		}
	}

	private boolean inMageArena() {
		return RSConstants.inMageArena(player.getX(), player.getY());
	}
	
	private static final String MISC_SAVE_QUERY = "INSERT INTO `misc`(`player_id`, `god_spell_req`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `god_spell_req` = VALUES(`god_spell_req`)";

	private void saveToDbMisc() {
		if (!Config.RUN_ON_DEDI)
			return;
		
		PlayerSaveSql.SAVE_EXECUTOR.execute(new Runnable() {
			
			@Override
			public void run() {

				try (Connection connection = Server.getConnection();
						PreparedStatement ps = connection.prepareStatement(MISC_SAVE_QUERY);) {
					ps.setInt(1, player.mySQLIndex);
					ps.setString(2, PlayerSaveSql.createJsonString(player.getGodSpell().getRequirements()));
					ps.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		});
	}
	
	private static final String CHARGE_EXPIRE_MSG = "Your Charge spell will run out in 1 minute.";
	
	public void runTimer() {
		int index = Timers.CHARGE_GOD_SPELLS.ordinal();
		if (player.getTimers()[index] > 0) {
			player.getTimers()[index] -= 1;
			
			hasChargeActive = true;
			
			int timeLeft = player.getTimers()[index];
			
			if (timeLeft == 100) {
				player.sendMessage(CHARGE_EXPIRE_MSG);
			}
			if (timeLeft == 0) {
				hasChargeActive = false;
			}
		}
	}
	
	private boolean hasChargeActive = false;
	
	public boolean hasChargeActive() {
		return hasChargeActive;
	}
	
	private TickTimer castDelay = new TickTimer();
	
	private static final String CHARGE_DENY_MSG = "You can only cast this spell every 30 seconds.";
	private static final String CHARGE_SUCCESS_MSG = "You feel charged with magic power.";
	
	private static final Requirement CHAREGE_SPELL_REQ = new Requirement(new GameItem(554, 3), new GameItem(565, 3), new GameItem(556, 3), null, 80);
	
	private static final Graphic CHARGE_GFX = Graphic.create(301, GraphicType.HIGH);
	
	private static final Animation GOD_SPELL_ANIM = new Animation(811);
	
	public void clickChargeButton() {
		if (castDelay.checkThenStart(175)) {
			if (!MagicRequirements.checkMagicReqsNew(player, CHAREGE_SPELL_REQ, SpellsData.CHARGE_GOD_SPELL, true)) {
				return;
			}

			player.startAnimation(GOD_SPELL_ANIM);
			player.startGraphic(CHARGE_GFX);
			player.sendMessage(CHARGE_SUCCESS_MSG);
			player.getPA().addSkillXP(180, Skills.MAGIC);
			startChargeTimer();
		} else {
			player.sendMessage(CHARGE_DENY_MSG);
		}
	}
	
	private static final int CHARGE_DURATION = 600; // 6 minutes
	
	private void startChargeTimer() {
		player.getTimers()[Timers.CHARGE_GOD_SPELLS.ordinal()] = CHARGE_DURATION;
	}

	private static int spellToIndex(SpellsData spellData) {
		switch (spellData) {
			case SARADOMIN_STRIKE:
				return 0;
			case CLAWS_OF_GUTHIX:
				return 1;
			case FLAMES_OF_ZAMORAK:
				return 2;
			default:
				return -1;
		}
	}

}
