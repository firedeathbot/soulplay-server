package com.soulplay.content.player.combat.magic;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public class MultiBarrage {

	public static void appendMultiBarrage(BattleState state) {
		final int distance = 1;
		if (state.getDefender().inMulti()) {
			for (Mob others : (state.getDefender().isNpc() ? Server.getRegionManager().getLocalNpcs(state.getDefender()) : Server.getRegionManager().getLocalPlayers(state.getDefender()))) {
				if (AttackMob.skipMultiEntity(state, others, distance))
					continue;

				BattleState state2 = state.copy(state.getAttacker(), others);
				state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().magicMaxHit(state.getSpellsData())));
				AttackMob.applyDamage(state2);
				
				AttackMob.putIntoCombat(state.getAttacker(), state.getDefender());

			}
		} else {
			AttackMob.applyDamage(state);
		}
	}
	
	public static boolean checkMultiBarrageReqs(Client c, final Client c2) {
		if (c2 == null) {
			return false;
		}
		if (c2.getId() == c.getId()) {
			return false;
		}
		if (!c2.inWild()) {
			return false;
		}
		if (Config.COMBAT_LEVEL_DIFFERENCE) {
			int combatDif1 = c.getCombat().getCombatDifference(c.combatLevel,
					c2.combatLevel);
			if (combatDif1 > c.wildLevel || combatDif1 > c2.wildLevel) {
				c.sendMessage(
						"Your combat level difference is too great to attack that player here.");
				return false;
			}
		}

		if (Config.SINGLE_AND_MULTI_ZONES) {
			if (!c2.inMulti()) { // single combat zones
				if (c2.underAttackBy != c.getId() && c2.underAttackBy != 0) {
					return false;
				}
				if (c2.getId() != c.underAttackBy && c.underAttackBy != 0) {
					c.sendMessage("You are already in combat.");
					return false;
				}
			}
		}
		return true;
	}

}
