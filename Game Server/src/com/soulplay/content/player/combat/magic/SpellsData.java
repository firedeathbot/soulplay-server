package com.soulplay.content.player.combat.magic;

import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.air;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.astral;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.blood;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.body;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.chaos;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.cosmic;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.death;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.earth;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.fire;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.mind;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.nature;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.soul;
import static com.soulplay.content.player.skills.dungeoneeringv2.skills.magic.SpellsConstants.water;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.soulplay.content.player.SpellBook;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NpcWeakness;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;

public enum SpellsData {

	//Normals
	WIND_STRIKE(1153, 1, 14221, null, 2699, Graphic.create(2699, GraphicType.HIGH), 2, 5, 556, 1, 558, 1, -1, -1, -1, -1, true, Sounds.WIND_STRIKE, 0, false, SpellBook.NORMAL, NpcWeakness.AIR_SPELLS),
	WATER_STRIKE(1155, 5, 14220, Graphic.create(2701), 2703, Graphic.create(2708, GraphicType.HIGH), 4, 7, 555, 1, 556, 1, 558, 1, -1, -1, true, Sounds.WATER_STRIKE, 0, false, SpellBook.NORMAL, NpcWeakness.WATER_SPELLS),
	EARTH_STRIKE(1157, 9, 14222, Graphic.create(2713), 2718, Graphic.create(2723, GraphicType.HIGH), 6, 9, 557, 2, 556, 1, 558, 1, -1, -1, true, Sounds.EARTH_STRIKE, 0, false, SpellBook.NORMAL, NpcWeakness.EARTH_SPELLS),
	FIRE_STRIKE(1159, 13, 14223, Graphic.create(2728), 2729, Graphic.create(2737, GraphicType.HIGH), 8, 11, 554, 3, 556, 2, 558, 1, -1, -1, true, Sounds.FIRE_STRIKE, 0, false, SpellBook.NORMAL, NpcWeakness.FIRE_SPELLS),
	WIND_BOLT(1161, 17, 14221, null, 2699, Graphic.create(2699, GraphicType.HIGH), 9, 13, 556, 2, 562, 1, -1, -1, -1, -1, true, Sounds.WIND_BOLD, 0, false, SpellBook.NORMAL, NpcWeakness.AIR_SPELLS),
	WATER_BOLT(1165, 23, 14220, Graphic.create(2701), 2704, Graphic.create(2709, GraphicType.HIGH), 10, 16, 556, 2, 555, 2, 562, 1, -1, -1, true, Sounds.WATER_BOLT, 0, false, SpellBook.NORMAL, NpcWeakness.WATER_SPELLS),
	EARTH_BOLT(1168, 29, 14222, Graphic.create(2714), 2719, Graphic.create(2724, GraphicType.HIGH), 11, 20, 556, 2, 557, 3, 562, 1, -1, -1, true, Sounds.EARTH_BOLT, 0, false, SpellBook.NORMAL, NpcWeakness.EARTH_SPELLS),
	FIRE_BOLT(1171, 35, 14223, Graphic.create(2728), 2730, Graphic.create(2738, GraphicType.HIGH), 12, 22, 556, 3, 554, 4, 562, 1, -1, -1, true, Sounds.FIRE_BOLT, 0, false, SpellBook.NORMAL, NpcWeakness.FIRE_SPELLS),
	WIND_BLAST(1175, 41, 14221, null, 2699, Graphic.create(2700, GraphicType.HIGH), 13, 25, 556, 3, 560, 1, -1, -1, -1, -1, true, Sounds.WIND_BLAST, 0, false, SpellBook.NORMAL, NpcWeakness.AIR_SPELLS),
	WATER_BLAST(1178, 47, 14220, Graphic.create(2702), 2705, Graphic.create(2710, GraphicType.HIGH), 14, 28, 556, 3, 555, 3, 560, 1, -1, -1, true, Sounds.WATER_BLAST, 0, false, SpellBook.NORMAL, NpcWeakness.WATER_SPELLS),
	EARTH_BLAST(1184, 53, 14222, Graphic.create(2715), 2720, Graphic.create(2725, GraphicType.HIGH), 15, 31, 556, 3, 557, 4, 560, 1, -1, -1, true, Sounds.EARTH_BLAST, 0, false, SpellBook.NORMAL, NpcWeakness.EARTH_SPELLS),
	FIRE_BLAST(1189, 59, 14223, Graphic.create(2728), 2733, Graphic.create(2739, GraphicType.HIGH), 16, 35, 556, 4, 554, 5, 560, 1, -1, -1, true, Sounds.FIRE_BLAST, 0, false, SpellBook.NORMAL, NpcWeakness.FIRE_SPELLS),
	WIND_WAVE(1196, 62, 14221, null, 2699, Graphic.create(2700, GraphicType.HIGH), 17, 36, 556, 5, 565, 1, -1, -1, -1, -1, true, Sounds.WIND_WAVE, 0, false, SpellBook.NORMAL, NpcWeakness.AIR_SPELLS),
	WATER_WAVE(1199, 65, 14220, Graphic.create(2702), 2706, Graphic.create(2711, GraphicType.HIGH), 18, 37, 556, 5, 555, 7, 565, 1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.WATER_SPELLS),
	EARTH_WAVE(1203, 70, 14222, Graphic.create(2716), 2721, Graphic.create(2726, GraphicType.HIGH), 19, 40, 556, 5, 557, 7, 565, 1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.EARTH_SPELLS),
	FIRE_WAVE(1206, 75, 14223, Graphic.create(2728), 2735, Graphic.create(2740, GraphicType.HIGH), 20, 42, 556, 5, 554, 7, 565, 1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.FIRE_SPELLS),
	WIND_SURGE(1211, 81, 27855, Graphic.create(5455, GraphicType.HIGH), 5456, Graphic.create(5457, GraphicType.HIGH), 22, 75, 556, 7, 565, 2, -1, -1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.AIR_SPELLS),
	WATER_SURGE(1213, 85, 27855, Graphic.create(5458, GraphicType.HIGH), 5459, Graphic.create(5460, GraphicType.HIGH), 24, 80, 555, 10, 556, 7, 565, 2, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.WATER_SPELLS),
	EARTH_SURGE(1218, 90, 27855, Graphic.create(5461, GraphicType.HIGH), 5462, Graphic.create(5463, GraphicType.HIGH), 26, 85, 557, 10, 556, 7, 565, 2, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.EARTH_SPELLS),
	FIRE_SURGE(1220, 95, 27855, Graphic.create(5464, GraphicType.HIGH), 5465, Graphic.create(5466, GraphicType.HIGH), 28, 90, 554, 10, 556, 7, 565, 2, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.FIRE_SPELLS),
	CONFUSE(1154, 3, 716, Graphic.create(102, GraphicType.HIGH), 103, Graphic.create(104, GraphicType.HIGH), 0, 13, 555, 3, 557, 2, 559, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	WEAKEN(1158, 11, 716, Graphic.create(105, GraphicType.HIGH), 106, Graphic.create(107, GraphicType.HIGH), 0, 20, 555, 3, 557, 2, 559, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	CURSE(1162, 19, 718, Graphic.create(108, GraphicType.HIGH), 109, Graphic.create(110, GraphicType.HIGH), 0, 29, 555, 2, 557, 3, 559, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	VULNERABILITY(1201, 66, 718, Graphic.create(167, GraphicType.HIGH), 168, Graphic.create(169, GraphicType.HIGH), 0, 76, 557, 5, 555, 5, 566, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENFEEBLE(1204, 73, 728, Graphic.create(170, GraphicType.HIGH), 171, Graphic.create(172, GraphicType.HIGH), 0, 83, 557, 8, 555, 8, 566, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	STUN(1209, 80, 729, Graphic.create(173, GraphicType.HIGH), 174, Graphic.create(254, GraphicType.HIGH), 0, 90, 557, 12, 555, 12, 566, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	BIND(1163, 20, 710, Graphic.create(177, GraphicType.HIGH), 178, Graphic.create(181, GraphicType.HIGH), 0, 30, 557, 3, 555, 3, 561, 2, -1, -1, false, Sounds.BIND, 10, false, SpellBook.NORMAL, NpcWeakness.NONE),
	SNARE(1181, 50, 710, Graphic.create(177, GraphicType.HIGH), 178, Graphic.create(180, GraphicType.HIGH), 2, 60, 557, 4, 555, 4, 561, 3, -1, -1, false, Sounds.SNARE, 17, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENTANGLE(1208, 79, 710, Graphic.create(177, GraphicType.HIGH), 178, Graphic.create(179, GraphicType.HIGH), 4, 90, 557, 5, 555, 5, 561, 4, -1, -1, false, Sounds.ENTANGLE, 25, false, SpellBook.NORMAL, NpcWeakness.NONE),
	CRUMBLE_UNDEAD(1173, 39, 724, Graphic.create(145, GraphicType.HIGH), 146, Graphic.create(147, GraphicType.HIGH), 15, 25, 556, 2, 557, 2, 562, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.UNDEAD),
	IBAN_BLAST(1180, 50, 708, Graphic.create(87, GraphicType.HIGH), 88, Graphic.create(89, GraphicType.HIGH), 25, 42, 554, 5, 560, 1, 1409, 1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	MAGIC_DART(1182, 50, Animation.getOsrsAnimId(1576), null, 328, Graphic.create(329), 19, 30, 560, 1, 558, 4, 4170, 1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	SARADOMIN_STRIKE(1192, 60, 811, null, -1, Graphic.create(76), 20, 60, 554, 2, 565, 2, 556, 4, 2415, 1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.LIGHT),
	CLAWS_OF_GUTHIX(1193, 60, 811, null, -1, Graphic.create(77, GraphicType.HIGH), 20, 60, 554, 1, 565, 2, 556, 4, 8843, 1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.CRUSH),
	FLAMES_OF_ZAMORAK(1194, 60, 811, null, -1, Graphic.create(Graphic.getOsrsId(78)), 20, 60, 554, 4, 565, 2, 556, 1, 2417, 1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.FIRE_SPELLS),
	TELEBLOCK(1214, 85, 10503, Graphic.create(1841), 1842, Graphic.create(1843), 0, 65, 563, 1, 562, 1, 560, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	LOW_ALCH(1164, 21, 712, Graphic.create(112), -1, null, -1, 31, 554, 3, 561, 1, -1, -1, -1, -1, false, Sounds.LOW_ALCH, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	HIGH_ALCH(1185, 55, 713, Graphic.create(113), -1, null, -1, 65, 554, 5, 561, 1, -1, -1, -1, -1, false, Sounds.HIGH_ALCH, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	TELEGRAB(1170, 33, 728, Graphic.create(142), 143, Graphic.create(144), 0, 35, 556, 1, 563, 1, -1, -1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	STORM_OF_ARMADYL(1207, 77, 10546, Graphic.create(457), -1, Graphic.create(1019), 16, 43, MagicRequirements.ARMADYL, 1, -1, -1, -1, -1, -1, -1, true, -1, 0, false, SpellBook.NORMAL, NpcWeakness.AIR_SPELLS),
	ENCHANT_SAPPHIRE(1156, 7, 719, Graphic.create(114, GraphicType.HIGH), -1, null, 0, 18, 555, 1, 564, 1, -1, -1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENCHANT_EMERALD(1167, 27, 719, Graphic.create(114, GraphicType.HIGH), -1, null, 0, 37, 556, 3, 564, 1, -1, -1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENCHANT_RUBY(1179, 47, 720, Graphic.create(115, GraphicType.HIGH), -1, null, 0, 59, 554, 5, 564, 1, -1, -1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENCHANT_DIAMOND(1187, 57, 720, Graphic.create(115, GraphicType.HIGH), -1, null, 0, 67, 557, 10, 564, 1, -1, -1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENCHANT_DRAGONSTONE(1202, 68, 721, Graphic.create(116, GraphicType.HIGH), -1, null, 0, 78, 555, 15, 557, 15, 564, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENCHANT_ONYX(1216, 87, 721, Graphic.create(452, GraphicType.HIGH), -1, null, 0, 97, 557, 20, 554, 20, 564, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	ENCHANT_ZENYTE(1219, 93, 721, Graphic.create(452, GraphicType.HIGH), -1, null, 0, 110, 565, 20, 566, 20, 564, 1, -1, -1, false, -1, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	SUPERHEAT_ITEM(1176, 43, 725, Graphic.create(148, GraphicType.HIGH), -1, null, 0, 53, 561, 1, 554, 4, -1, -1, -1, -1, false, 190, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),

	//charge spells
	CHARGE_WATER_ORB(1186, 56, 791, Graphic.create(149, GraphicType.HIGH), -1, null, 0, 66, 555, 30, 564, 3, 567, 1, -1, -1, false, 190, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	CHARGE_EARTH_ORB(1190, 60, 791, Graphic.create(152, GraphicType.HIGH), -1, null, 0, 70, 557, 30, 564, 3, 567, 1, -1, -1, false, 190, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	CHARGE_FIRE_ORB(1197, 63, 791, Graphic.create(153, GraphicType.HIGH), -1, null, 0, 73, 554, 30, 564, 3, 567, 1, -1, -1, false, 190, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	CHARGE_AIR_ORB(1200, 66, 791, Graphic.create(150, GraphicType.HIGH), -1, null, 0, 76, 556, 30, 564, 3, 567, 1, -1, -1, false, 190, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),
	CHARGE_GOD_SPELL(4186, 80, 811, Graphic.create(301, GraphicType.HIGH), -1, null, 0, 180, 554, 3, 565, 3, 556, 3, -1, -1, false, 190, 0, false, SpellBook.NORMAL, NpcWeakness.NONE),

	
	//Ancients
	SMOKE_RUSH(12857, 50, 1978, null, 384, Graphic.create(385, GraphicType.HIGH), 13, 30, 560, 2, 562, 2, 554, 1, 556, 1, true, Sounds.ANCIENT_MAGICS_SMOKE_SPELL, 0, false, SpellBook.ANCIENT, NpcWeakness.NONE),
	SHADOW_RUSH(12858, 52, 1978, null, 378, Graphic.create(379), 14, 31, 560, 2, 562, 2, 566, 1, 556, 1, true, Sounds.ANCIENT_MAGICS_SHADOW_SPELL, 0, false, SpellBook.ANCIENT, NpcWeakness.NONE),
	BLOOD_RUSH(12860, 56, 1978, null, -1, Graphic.create(373), 15, 33, 560, 2, 562, 2, 565, 1, 0, 0, true, -1, 0, false, SpellBook.ANCIENT, NpcWeakness.FIRE_SPELLS),
	ICE_RUSH(12861, 58, 1978, null, 360, Graphic.create(361), 16, 34, 560, 2, 562, 2, 555, 2, 0, 0, true, Sounds.ANCIENT_MAGICS_ICE_SPELL, 10, false, SpellBook.ANCIENT, NpcWeakness.WATER_SPELLS),
	MIASMIC_RUSH(12862, 61, 10513, Graphic.create(1845), 1846, Graphic.create(1847), 18, 36, 566, 1, 557, 1, 562, 2, 13867, 1, true, -1, 0, false, SpellBook.ANCIENT, NpcWeakness.NONE),
	SMOKE_BURST(12863, 62, 1979, null, -1, Graphic.create(389), 19, 36, 560, 2, 562, 4, 556, 2, 554, 2, true, Sounds.ANCIENT_MAGICS_SMOKE_SPELL, 0, true, SpellBook.ANCIENT, NpcWeakness.NONE),
	SHADOW_BURST(12864, 64, 1979, null, -1, Graphic.create(382), 20, 37, 560, 2, 562, 4, 556, 2, 566, 2, true, Sounds.ANCIENT_MAGICS_SHADOW_SPELL, 0, true, SpellBook.ANCIENT, NpcWeakness.NONE),
	BLOOD_BURST(12866, 68, 1979, null, -1, Graphic.create(376), 21, 39, 560, 2, 562, 4, 565, 2, 0, 0, true, -1, 0, true, SpellBook.ANCIENT, NpcWeakness.FIRE_SPELLS),
	ICE_BURST(12867, 70, 1979, null, -1, Graphic.create(363), 22, 40, 560, 2, 562, 4, 555, 4, 0, 0, true, Sounds.ANCIENT_MAGICS_ICE_SPELL, 17, true, SpellBook.ANCIENT, NpcWeakness.WATER_SPELLS),
	MIASMIC_BURST(12868, 73, 10516, Graphic.create(1848), -1, Graphic.create(1849), 24, 42, 566, 2, 557, 12, 562, 4, 13867, 1, true, -1, 0, true, SpellBook.ANCIENT, NpcWeakness.NONE),
	SMOKE_BLITZ(12869, 74, 1978, null, 386, Graphic.create(387), 23, 42, 560, 2, 554, 2, 565, 2, 556, 2, true, Sounds.ANCIENT_MAGICS_SMOKE_SPELL, 0, false, SpellBook.ANCIENT, NpcWeakness.NONE),
	SHADOW_BLITZ(12870, 76, 1978, null, 380, Graphic.create(381), 24, 43, 560, 2, 565, 2, 556, 2, 566, 2, true, Sounds.ANCIENT_MAGICS_SHADOW_SPELL, 0, false, SpellBook.ANCIENT, NpcWeakness.NONE),
	BLOOD_BLITZ(12872, 80, 1978, null, 374, Graphic.create(375), 25, 45, 560, 2, 565, 4, 0, 0, 0, 0, true, -1, 0, false, SpellBook.ANCIENT, NpcWeakness.FIRE_SPELLS),
	ICE_BLITZ(12873, 82, 1978, Graphic.create(366), -1, Graphic.create(367), 26, 46, 560, 2, 565, 2, 555, 3, 0, 0, true, Sounds.ANCIENT_MAGICS_ICE_SPELL, 25, false, SpellBook.ANCIENT, NpcWeakness.WATER_SPELLS),
	MIASMIC_BLITZ(12874, 85, 10524, Graphic.create(1850), 1852, Graphic.create(1851), 28, 48, 565, 2, 557, 3, 566, 3, 13867, 1, true, -1, 0, false, SpellBook.ANCIENT, NpcWeakness.NONE),
	SMOKE_BARRAGE(12876, 86, 1979, null, -1, Graphic.create(391, GraphicType.HIGH), 27, 48, 560, 4, 565, 2, 556, 4, 554, 4, true, Sounds.ANCIENT_MAGICS_SMOKE_SPELL, 0, true, SpellBook.ANCIENT, NpcWeakness.NONE),
	SHADOW_BARRAGE(12877, 88, 1979, null, -1, Graphic.create(383), 28, 49, 560, 4, 565, 2, 556, 4, 566, 3, true, Sounds.ANCIENT_MAGICS_SHADOW_SPELL, 0, true, SpellBook.ANCIENT, NpcWeakness.NONE),
	BLOOD_BARRAGE(12879, 92, 1979, null, -1, Graphic.create(377), 29, 51, 560, 4, 565, 4, 566, 1, 0, 0, true, -1, 0, true, SpellBook.ANCIENT, NpcWeakness.FIRE_SPELLS),
	ICE_BARRAGE(12880, 94, 1979, null, -1, Graphic.create(369), 30, 52, 560, 4, 565, 2, 555, 6, 0, 0, true, Sounds.ANCIENT_MAGICS_ICE_SPELL, 33, true, SpellBook.ANCIENT, NpcWeakness.WATER_SPELLS),
	MIASMIC_BARRAGE(12882, 97, 10518, Graphic.create(1853), -1, Graphic.create(1854), 32, 54, 565, 4, 557, 4, 566, 4, 13867, 1, true, -1, 0, true, SpellBook.ANCIENT, NpcWeakness.NONE),

	//Dungeoneering
	WIND_STRIKE_DUNG(62002, 1, 14221, null, 2699, Graphic.create(2699, GraphicType.HIGH), 2, 5, mind, 1, air, 1, -1, -1, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.AIR_SPELLS),
	CONFUSE_DUNG(62003, 3, 716, Graphic.create(102, GraphicType.HIGH), 103, Graphic.create(104, GraphicType.HIGH), 0, 13, body, 1, earth, 2, water, 3, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	WATER_STRIKE_DUNG(62004, 5, 14220, Graphic.create(2701, GraphicType.HIGH), 2703, Graphic.create(2708, GraphicType.HIGH), 4, 7, mind, 1, water, 1, air, 1, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.WATER_SPELLS),
	EARTH_STRIKE_DUNG(62005, 9, 14222, Graphic.create(2713, GraphicType.HIGH), 2718, Graphic.create(2723, GraphicType.HIGH), 6, 9, mind, 1, earth, 2, air, 1, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.EARTH_SPELLS),
	WEAKEN_DUNG(62006, 11, 716, Graphic.create(105, GraphicType.HIGH), 106, Graphic.create(107, GraphicType.HIGH), 0, 20, body, 1, earth, 2, water, 3, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	FIRE_STRIKE_DUNG(62007, 13, 14223, Graphic.create(2728, GraphicType.HIGH), 2729, Graphic.create(2737, GraphicType.HIGH), 8, 11, mind, 1, fire, 3, air, 2, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.FIRE_SPELLS),
	WIND_BOLT_DUNG(62009, 17, 14221, null, 2699, Graphic.create(2699, GraphicType.HIGH), 9, 13, chaos, 1, air, 2, -1, -1, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.AIR_SPELLS),
	CURSE_DUNG(62010, 19, 718, Graphic.create(108, GraphicType.HIGH), 109, Graphic.create(110, GraphicType.HIGH), 0, 29, body, 1, earth, 2, water, 3, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	BIND_DUNG(62011, 20, 710, Graphic.create(177, GraphicType.HIGH), 178, Graphic.create(181, GraphicType.HIGH), 0, 30, nature, 2, earth, 3, water, 2, -1, -1, false, -1, 10, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	LOW_ALCH_DUNG(62012, 21, 712, Graphic.create(112), -1, null	, 0, 0, nature, 1, fire, 2, -1, -1, -1, -1, false, Sounds.LOW_ALCH, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	WATER_BOLT_DUNG(62013, 23, 14220, Graphic.create(2701, GraphicType.HIGH), 2704, Graphic.create(2709, GraphicType.HIGH), 10, 16, chaos, 1, water, 2, air, 2, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.WATER_SPELLS),
	EARTH_BOLT_DUNG(62014, 29, 14222, Graphic.create(2714, GraphicType.HIGH), 2719, Graphic.create(2724, GraphicType.HIGH), 11, 20, chaos, 1, earth, 3, air, 2, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.EARTH_SPELLS),
	FIRE_BOLT_DUNG(62017, 35, 14223, Graphic.create(2728, GraphicType.HIGH), 2730, Graphic.create(2738, GraphicType.HIGH), 12, 22, chaos, 1, fire, 3, air, 3, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.FIRE_SPELLS),
	WIND_BLAST_DUNG(62018, 41, 14221, null, 2699, Graphic.create(2700, GraphicType.HIGH), 13, 25, death, 1, air, 3, -1, -1, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.AIR_SPELLS),
	WATER_BLAST_DUNG(62019, 47, 14220, Graphic.create(2702, GraphicType.HIGH), 2705, Graphic.create(2710, GraphicType.HIGH), 14, 28, death, 1, water, 3, air, 3, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.WATER_SPELLS),
	SNARE_DUNG(62020, 50, 710, Graphic.create(177, GraphicType.HIGH), 178, Graphic.create(180, GraphicType.HIGH), 2, 60, nature, 3, earth, 4, water, 4, -1, -1, false, -1, 17, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	EARTH_BLAST_DUNG(62021, 53, 14222, Graphic.create(2715, GraphicType.HIGH), 2720, Graphic.create(2725, GraphicType.HIGH), 15, 31, death, 1, earth, 4, air, 3, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.EARTH_SPELLS),
	HIGH_ALCH_DUNG(62022, 55, 713, Graphic.create(113), -1, null, 0, 0, nature, 1, fire, 5, -1, -1, -1, -1, false, Sounds.HIGH_ALCH, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	FIRE_BLAST_DUNG(62023, 59, 14223, Graphic.create(2728, GraphicType.HIGH), 2733, Graphic.create(2739, GraphicType.HIGH), 16, 35, death, 1, fire, 5, air, 4, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.FIRE_SPELLS),
	WIND_WAVE_DUNG(62024, 62, 14221, null, 2699, Graphic.create(2700, GraphicType.HIGH), 17, 36, blood, 1, air, 5, -1, -1, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.AIR_SPELLS),
	WATER_WAVE_DUNG(62026, 65, 14220, Graphic.create(2702, GraphicType.HIGH), 2706, Graphic.create(2711, GraphicType.HIGH), 18, 37, blood, 1, water, 7, air, 5, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.WATER_SPELLS),
	VULNURABILITY_DUNG(62027, 66, 718, Graphic.create(167, GraphicType.HIGH), 168, Graphic.create(169, GraphicType.HIGH), 0, 76, soul, 1, earth, 5, water, 5, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	MONSTER_EXAMINE_DUNG(62028, 66, -1, null, -1, null, -1, -1, cosmic, 1, astral, 1, mind, 1, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),//TODO
	EARTH_WAVE_DUNG(62031, 70, 14222, Graphic.create(2716, GraphicType.HIGH), 2721, Graphic.create(2726, GraphicType.HIGH), 19, 40, blood, 1, earth, 7, air, 5, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.EARTH_SPELLS),
	ENFEEBLE_DUNG(62033, 73, 710, Graphic.create(170, GraphicType.HIGH), 171, Graphic.create(172, GraphicType.HIGH), 0, 83, soul, 1, earth, 8, water, 8, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	FIRE_WAVE_DUNG(62035, 75, 14223, Graphic.create(2728, GraphicType.HIGH), 2735, Graphic.create(2740, GraphicType.HIGH), 20, 42, blood, 1, fire, 7, air, 5, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.FIRE_SPELLS),
	ENTANGLE_DUNG(62036, 79, 710, Graphic.create(177, GraphicType.HIGH), 178, Graphic.create(179, GraphicType.HIGH), 4, 90, nature, 4, earth, 5, water, 5, -1, -1, false, -1, 25, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	STUN_DUNG(62037, 80, 729, Graphic.create(173, GraphicType.HIGH), 174, Graphic.create(254, GraphicType.HIGH), 0, 90, soul, 1, earth, 12, water, 12, -1, -1, false, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.NONE),
	WIND_SURGE_DUNG(62038, 81, 14221, null, 2699, Graphic.create(2700, GraphicType.HIGH), 22, 75, blood, 1, death, 1, air, 7, -1, -1, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.AIR_SPELLS),
	WATER_SURGE_DUNG(62039, 85, 14220, Graphic.create(2702), 2707, Graphic.create(2712, GraphicType.HIGH), 24, 80, blood, 1, death, 1, water, 10, air, 7, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.WATER_SPELLS),
	EARTH_SURGE_DUNG(62040, 90, 14222, Graphic.create(2717), 2722, Graphic.create(2727, GraphicType.HIGH), 26, 85, blood, 1, death, 1, earth, 10, air, 7, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.EARTH_SPELLS),
	FIRE_SURGE_DUNG(62046, 95, 14223, Graphic.create(2728), 2736, Graphic.create(2740, GraphicType.HIGH), 28, 90, blood, 1, death, 1, fire, 10, air, 7, true, -1, 0, false, SpellBook.DUNGEONEERING, NpcWeakness.FIRE_SPELLS),

	//triden
	TRIDENT_OF_THE_SEA(1210, 1, Animation.getOsrsAnimId(1167), Graphic.create(Graphic.getOsrsId(1251), GraphicType.HIGH), Graphic.getOsrsId(1252), Graphic.create(Graphic.getOsrsId(1253), GraphicType.HIGH), 20, 2, -1, -1, -1, -1, -1, -1, -1, -1, true, -1, 0, false, SpellBook.ALL, NpcWeakness.WATER_SPELLS),
	TRIDENT_OF_THE_SWAMP(1212, 1, Animation.getOsrsAnimId(1167), Graphic.create(Graphic.getOsrsId(665), GraphicType.HIGH), Graphic.getOsrsId(1040), Graphic.create(Graphic.getOsrsId(1042), GraphicType.HIGH), 23, 2, -1, -1, -1, -1, -1, -1, -1, -1, true, -1, 0, false, SpellBook.ALL, NpcWeakness.WATER_SPELLS),
	DAWNBRINGER(1212, 1, Animation.getOsrsAnimId(1167), Graphic.create(Graphic.getOsrsId(1546), GraphicType.HIGH), Graphic.getOsrsId(1547), Graphic.create(Graphic.getOsrsId(1548), GraphicType.HIGH), 23, 2, -1, -1, -1, -1, -1, -1, -1, -1, true, -1, 0, false, SpellBook.ALL, NpcWeakness.WATER_SPELLS),
	SANGUINESTI_STAFF(1210, 1, Animation.getOsrsAnimId(1167), Graphic.create(Graphic.getOsrsId(1540), GraphicType.HIGH), Graphic.getOsrsId(1539), Graphic.create(Graphic.getOsrsId(1541), GraphicType.HIGH), 24, 2, -1, -1, -1, -1, -1, -1, -1, -1, true, -1, 0, false, SpellBook.ALL, NpcWeakness.WATER_SPELLS),

	VENG(117086, 94, 4410, Graphic.create(726, GraphicType.HIGH), -1, null, -1, 112, 560, 2, 9075, 4, 557, 10, -1, -1, false, -1, 0, false, SpellBook.LUNARS, NpcWeakness.NONE),

	;
	
	
	public static final Map<Integer, SpellsData> SPELLS = new HashMap<>();

	public static final Set<SpellsData> WIND_SPELLS = new HashSet<>();
	public static final Set<SpellsData> FIRE_SPELLS = new HashSet<>();
	public static final List<SpellsData> WATER_SPELLS = new ArrayList<>();

	private final int buttonId;
	private final boolean canAutoCast;
	private final Animation anim;
	private final Graphic startGfx;
	private final Graphic endGfx;
	private final Projectile projectile;
	private final int maxHit;
	private final int expGain;
	private final Requirement requirement;
	private final boolean multiSpell;
	private final int freezeTime;
	private final int soundId;
	private final SpellBook spellBook;
	private final NpcWeakness weakness;

	private SpellsData(int magicId, int levelreq, int animation, Graphic startGFX, int projectileId, Graphic endGFX, int maxhit,
			int expgained, int rune1Id, int rune1amount, int rune2Id, int rune2amount, int rune3Id, int rune3amount,
			int rune4Id, int rune4amount, boolean canAutoCast, int soundId, int freezeTime, boolean multiSpell, SpellBook spellBook, NpcWeakness weakness) {
		Projectile projectile;
		if (projectileId != -1) {
			projectile = new Projectile(projectileId, Location.create(0, 0), Location.create(0, 0));
		} else {
			projectile = null;
		}

		GameItem rune1;
		if (rune1Id != -1) {
			rune1 = new GameItem(rune1Id, rune1amount);
		} else {
			rune1 = null;
		}

		GameItem rune2;
		if (rune2Id != -1) {
			rune2 = new GameItem(rune2Id, rune2amount);
		} else {
			rune2 = null;
		}

		GameItem rune3;
		if (rune3Id != -1) {
			rune3 = new GameItem(rune3Id, rune3amount);
		} else {
			rune3 = null;
		}

		GameItem rune4;
		if (rune4Id != -1) {
			rune4 = new GameItem(rune4Id, rune4amount);
		} else {
			rune4 = null;
		}

		this.buttonId = magicId;
		this.canAutoCast = canAutoCast;
		this.anim = new Animation(animation);
		this.startGfx = startGFX;
		this.endGfx = endGFX;
		this.projectile = projectile;
		this.maxHit = maxhit;
		this.expGain = expgained;
		this.multiSpell = multiSpell;
		this.freezeTime = freezeTime;
		this.requirement = new Requirement(rune1, rune2, rune3, rune4, levelreq);
		this.soundId = soundId;
		this.spellBook = spellBook;
		this.weakness = weakness;
	}

	public int getButtonId() {
		return buttonId;
	}

	public int getLevelReq() {
		return requirement.getLevel();
	}

	public Animation getAnim() {
		return anim;
	}

	public Graphic getStartGfx() {
		return startGfx;
	}

	public Graphic getEndGfx() {
		return endGfx;
	}

	public Projectile getProjectile() {
		return projectile;
	}

	public int getMaxHit() {
		return maxHit;
	}

	public int getExpGain() {
		return expGain;
	}

	public GameItem getRunesReq1() {
		return requirement.getRunesReq1();
	}

	public GameItem getRunesReq2() {
		return requirement.getRunesReq2();
	}

	public GameItem getRunesReq3() {
		return requirement.getRunesReq3();
	}

	public GameItem getRunesReq4() {
		return requirement.getRunesReq4();
	}

	public boolean canAutoCast() {
		return canAutoCast;
	}

	public boolean isMultiSpell() {
		return multiSpell;
	}

	public int getFreezeTime() {
		return freezeTime;
	}

	public Requirement getRequirement() {
		return requirement;
	}

	public int getSoundId() {
		return soundId;
	}

	public SpellBook getSpellBook() {
		return spellBook;
	}
	
	public NpcWeakness getNpcWeakness() {
		return weakness;
	}

	public static void load() {
		for (SpellsData data : values()) {
			SPELLS.put(data.getButtonId(), data);
		}
		
		WATER_SPELLS.add(WATER_STRIKE);
		WATER_SPELLS.add(WATER_BOLT);
		WATER_SPELLS.add(WATER_BLAST);
		WATER_SPELLS.add(WATER_WAVE);
		WATER_SPELLS.add(WATER_SURGE);

		WIND_SPELLS.add(WIND_STRIKE);
		WIND_SPELLS.add(WIND_BOLT);
		WIND_SPELLS.add(WIND_BLAST);
		WIND_SPELLS.add(WIND_WAVE);
		WIND_SPELLS.add(WIND_SURGE);
		WIND_SPELLS.add(WIND_STRIKE_DUNG);
		WIND_SPELLS.add(WIND_BOLT_DUNG);
		WIND_SPELLS.add(WIND_BLAST_DUNG);
		WIND_SPELLS.add(WIND_WAVE_DUNG);
		WIND_SPELLS.add(WIND_SURGE_DUNG);

		FIRE_SPELLS.add(FIRE_STRIKE);
		FIRE_SPELLS.add(FIRE_BOLT);
		FIRE_SPELLS.add(FIRE_BLAST);
		FIRE_SPELLS.add(FIRE_WAVE);
		FIRE_SPELLS.add(FIRE_SURGE);
		FIRE_SPELLS.add(FIRE_STRIKE_DUNG);
		FIRE_SPELLS.add(FIRE_BOLT_DUNG);
		FIRE_SPELLS.add(FIRE_BLAST_DUNG);
		FIRE_SPELLS.add(FIRE_WAVE_DUNG);
		FIRE_SPELLS.add(FIRE_SURGE_DUNG);
	}

	public static SpellsData getSpell(Player p, int buttonId) {
		SpellsData spellData = SPELLS.get(buttonId);

		if (spellData == null) {
			return null;
		}

		if (spellData.getSpellBook() != SpellBook.ALL && spellData.getSpellBook() != p.playerMagicBook) {
			p.sendMessage("Wrong spellbook.");
			return null;
		}

		return spellData;
	}
	
	public static SpellsData getWaterSpell(Player p) {
		for (int i = 0, len = WATER_SPELLS.size(); i < len; i++) {
			SpellsData spell = WATER_SPELLS.get(i);
			if (MagicRequirements.checkMagicReqsNoMessage(p, spell, false))
				return spell;
		}
		return null;
	}

}
