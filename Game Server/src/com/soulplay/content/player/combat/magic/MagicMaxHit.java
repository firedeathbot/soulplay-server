package com.soulplay.content.player.combat.magic;

import com.soulplay.content.combat.data.SlayerHelm;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;

import plugin.item.TomeOfFirePlugin;
import plugin.item.chargeitems.ThammaronsSceptrePlugin;

public class MagicMaxHit {

	public static int magicMaxHit(Client c, SpellsData spellsData, int baseDamage) {
		double damage;
		if (spellsData == null) {
			damage = baseDamage;
		} else {
			damage = spellsData.getMaxHit();
		}

		double damageMultiplier = 1;

		double magicStrBonus = c.getPlayerBonus()[EquipmentBonuses.MAGIC_STR.getId()] / 100.0d;
		
		int trickster = c.tricksterAmount();
		int battlemage = c.battleMageAmount();

		float dmgBoost = Math.max(battlemage, trickster) * 0.03f;

		if (c.inCw() || RSConstants.fightPitsRoom(c.getX(), c.getY()) || c.inSoulWars() || c.inPcGame()) {
			//double hybridBoost = 0.0;
			
			damageMultiplier += dmgBoost;

			/*if (vanguard > trickster && vanguard > battlemage) {
				hybridBoost += vanguard * 3 / 100.0;
			} else if (trickster > vanguard && trickster > battlemage) {
				hybridBoost += trickster * 3 / 100.0;
			} else if (battlemage > trickster && battlemage > vanguard) {
				hybridBoost += battlemage * 3 / 100.0;
			}*/

			// c.sendMess("boost:"+hybridBoost);
			//if (hybridBoost > 0.0) {
			//	damageMultiplier += hybridBoost;
			//}
		} else {
			damageMultiplier -= dmgBoost;
		}

		// c.sendMessage("oldspellid "+c.oldSpellId);

		// int weaponBonus =
		// ItemDefinition.forId(c.playerEquipment[Player.playerWeapon]).getBonus()[3];
		//// damageMultiplier += (magicBonus - weaponBonus ) / 100.0 * 0.10;
		// damageMultiplier += (magicBonus - weaponBonus ) * 0.010;
		// damageMultiplier += weaponBonus / 100.0;
		// } else {
		// damageMultiplier += magicBonus / 100.0 * 0.10;
		// }
		
		if (spellsData != null && spellsData == SpellsData.MAGIC_DART) {
			damage += c.getSkills().getLevel(Skills.MAGIC) / 10;
		}
		
		damageMultiplier += magicStrBonus;

		if (spellsData != null) {
			switch (spellsData) {
				case FIRE_BLAST:
				case FIRE_BOLT:
				case FIRE_STRIKE:
				case FIRE_SURGE:
				case FIRE_WAVE: {
					if (c.playerEquipment[PlayerConstants.playerShield] == TomeOfFirePlugin.CHARGED_ID) {
						damageMultiplier += 0.5;
					}
					break;
				}
				default:
					break;
			}
		}

		damage = Math.round(damage * damageMultiplier);
		
		if (c.inCwGame) {
			double profoundBoost = c.profoundBoost();
			if (profoundBoost > 1.0) {
				damage = Math.round(damage * (profoundBoost/* + 0.05 */));
			}
		}
		
		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
		case ThammaronsSceptrePlugin.CHARGED_ID:
			if (c.npcIndex > 0 && c.wildLevel > 0) {
				damage *= 1.25;
			}
			break;
		}

		if (SlayerHelm.wearingSlayerHelmFull(c)) { // slayer helmets
			damage = Math.round(damage * SlayerHelm.slayerHelmBoost(c));
		}
		
		damageMultiplier = 1;
		if (c.getSkills().getLevel(Skills.MAGIC) > c.getSkills().getStaticLevel(Skills.MAGIC)) {
			damageMultiplier += .03
					* (c.getSkills().getLevel(Skills.MAGIC)
							- c.getSkills().getStaticLevel(Skills.MAGIC));
		}

		int powerUpAmount = c.getSeasonalData().getPowerUpAmount(PowerUpData.MAGIC_DAMAGE);
		if (powerUpAmount > 0) {
			damageMultiplier += powerUpAmount * 0.05;
		}

		// c.sendMessage("mult :"+damageMultiplier);
		damage = Math.round(damage * damageMultiplier);

		if (spellsData != null) {
			if (spellsData == SpellsData.STORM_OF_ARMADYL && c.playerEquipment[PlayerConstants.playerWeapon] == 21777) {
				damage += c.getSkills().getStaticLevel(Skills.MAGIC) - 77;
				damage = Math.round(damage * 1.10);
			}
			
			switch (spellsData) {
				case TRIDENT_OF_THE_SEA:
				case TRIDENT_OF_THE_SWAMP:
				case SANGUINESTI_STAFF: {
					int addon = (c.getSkills().getLevel(Skills.MAGIC) - 75) / 3;
					damage += addon;
					break;
				}
				case CLAWS_OF_GUTHIX:
				case SARADOMIN_STRIKE:
				case FLAMES_OF_ZAMORAK: {
					if (c.getGodSpell().hasChargeActive())
						damage += 10;
					break;
				}
				default:
					break;
			}
		}

		// c.sendMessage("Final damage: " + damage + " Damage Multiplier: " +
		// damageMultiplier);

		return (int) damage;
	}

}
