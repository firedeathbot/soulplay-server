package com.soulplay.content.player.combat.magic;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public class CastOnOther extends MagicRequirements {

	public static void castOtherVengeance(Client c, int i) {
		Client p = (Client) PlayerHandler.players[i];
		if (!hasRequiredLevel(c, 93)) {
			c.sendMessage(
					"You need to have a magic level of 93 to cast this spell.");
			return;
		}
		if (p.vengOn) {
			c.sendMessage("This player has already casted vengeance!");
			return;
		}
		if (System.currentTimeMillis() - p.lastVeng < 30000) {
			c.sendMessage(
					"You must wait 30 seconds before casting this again.");
			return;
		}
		if (!hasRunes(c, new int[]{ASTRAL, DEATH, EARTH},
				new int[]{3, 2, 10})) {
			return;
		}
		deleteRunes(c, new int[]{ASTRAL, DEATH, EARTH}, new int[]{3, 2, 10});
		c.face(p);
		c.startAnimation(4411);
		p.vengOn = true;
		p.lastVeng = System.currentTimeMillis();
		p.startGraphic(Graphic.create(725, GraphicType.HIGH));
		p.sendMessage("You have the power of vengeance!");
	}

	public static void healOther(Client c, int i) {
		Client p = (Client) PlayerHandler.players[i];
		double hpPercent = c.getSkills().getLifepoints() * 0.75;
		if (!hasRequiredLevel(c, 92)) {
			c.sendMessage(
					"You need to have a magic level of 92 to cast this spell.");
			return;
		}
		if (!hasRunes(c, new int[]{ASTRAL, LAW, BLOOD}, new int[]{3, 3, 1})) {
			return;
		}
		if (p.getSkills().getLifepoints() < 1) {
			return;
		}

		deleteRunes(c, new int[]{ASTRAL, LAW, BLOOD}, new int[]{3, 3, 1});

		if (p.getSkills().getLifepoints() + (int) hpPercent >= p.getSkills().getMaximumLifepoints()) {
			if (p.getSkills().getLifepoints() > (int) hpPercent) {
				hpPercent = (p.getSkills().getLifepoints() - (int) hpPercent);
			} else {
				hpPercent = ((int) hpPercent - p.getSkills().getLifepoints());
			}
		}

		if (p.getSkills().getLifepoints() >= p.getSkills().getMaximumLifepoints()) {
			c.sendMessage(p.getNameSmartUp() + " already has full hitpoints.");
			return;
		}

		c.dealDamage(new Hit((int) hpPercent, 2, -1));

		p.getSkills().heal((int) hpPercent);

		c.face(p);

		c.startAnimation(4411);
		c.startGraphic(Graphic.create(727, GraphicType.HIGH));
	}

	public static void specialEnergyTransfer(Client c, int i) {
		Client p = (Client) PlayerHandler.players[i];
		if (!hasRequiredLevel(c, 91)) {
			c.sendMessage(
					"You need to have a magic level of 91 to cast this spell.");
			return;
		}
		if (p.specAmount >= 5.0) {
			c.sendMessage("You can't transfer special energy to " + p.getNameSmartUp()
					+ "!");
			return;
		}
		if (!hasRunes(c, new int[]{ASTRAL, LAW, NATURE}, new int[]{3, 2, 1})) {
			return;
		}
		deleteRunes(c, new int[]{ASTRAL, LAW, NATURE}, new int[]{3, 2, 1});
		c.face(p);
		c.startAnimation(4411);
		p.startGraphic(Graphic.create(1069, GraphicType.LOW));
		p.specAmount += 5;
		c.specAmount -= 5;
		c.getItems().updateSpecialBar();
		p.getItems().updateSpecialBar();
		p.sendMessage("Your special energy has been restored by 50%!");
		c.sendMessage(
				"You transfer 50% of your energy to " + p.getNameSmartUp() + ".");
	}

	public static void teleOtherDistance(Client c, int type, int i) {
		Client castOn = (Client) PlayerHandler.players[i];
	
		int[][] data = {{74, SOUL, LAW, EARTH, 1, 1, 1},
				{82, SOUL, LAW, WATER, 1, 1, 1},
				{90, SOUL, LAW, -1, 2, 1, -1},};
		if (!hasRequiredLevel(c, data[type][0])) {
			c.sendMessage("You need to have a magic level of " + data[type][0]
					+ " to cast this spell.");
			return;
		}
		if (!hasRunes(c, new int[]{data[type][1], data[type][2], data[type][3]},
				new int[]{data[type][4], data[type][5], data[type][6]})) {
			return;
		}
		deleteRunes(c, new int[]{data[type][1], data[type][2], data[type][3]},
				new int[]{data[type][4], data[type][5], data[type][6]});
		String[] location = {"Lumbridge", "Falador", "Camelot",};
		c.face(castOn);
		c.startAnimation(1818);
		c.startGraphic(Graphic.create(343, GraphicType.LOW));
		if (castOn != null) {
			if (castOn.distanceToPoint(c.absX, c.absY) <= 15) {
				if (c.heightLevel == castOn.heightLevel) {
					castOn.getPacketSender().sendFrame126(location[type],
							12560);
					castOn.getPacketSender().sendFrame126(c.getNameSmartUp(), 12558);
					castOn.getPacketSender().showInterface(12468);
					castOn.teleotherType = type;
				}
			}
		}
	}

	public static void teleOtherLocation(final Client c, final int i,
			boolean decline) {
		c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
		final int[][] coords = {{3222, 3218}, // LUMBRIDGE
				{2964, 3378}, // FALADOR
				{2757, 3477}, // CAMELOT
		};
		if (!decline) {
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					c.startAnimation(715);
					c.teleportToX = coords[c.teleotherType][0];
					c.teleportToY = coords[c.teleotherType][1];
					c.teleportToZ = 0;
					c.teleotherType = -1;
					container.stop();
				}

				@Override
				public void stop() {

				}
			}, 3);
			c.startAnimation(1816);
			c.startGraphic(Graphic.create(342, GraphicType.HIGH));
		}
	}
}
