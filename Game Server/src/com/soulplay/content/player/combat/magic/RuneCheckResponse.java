package com.soulplay.content.player.combat.magic;

public class RuneCheckResponse {
	private final RuneCheckType type;
	private final int count;
	private final int[] runes;

	public RuneCheckResponse(RuneCheckType type, int count, int[] runes) {
		this.type = type;
		this.count = count;
		this.runes = runes;
	}

	public RuneCheckResponse(RuneCheckType type) {
		this(type, 99999999, null);
	}

	public RuneCheckResponse(RuneCheckType type, int count) {
		this(type, count, null);
	}

	public RuneCheckType getType() {
		return type;
	}

	public int getCount() {
		return count;
	}

	public int[] getRunes() {
		return runes;
	}

}
