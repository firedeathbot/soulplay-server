package com.soulplay.content.player.combat.magic;

public enum RuneCheckType {

	INVENTORY,
	STAFF,
	POUCH,
	LAW_STAFF,
	NATURE_STAFF,
	TOME_OF_FIRE,
	SEASONAL_POWER;

}