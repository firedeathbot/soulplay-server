package com.soulplay.content.player.combat.magic;

import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;

public class NonCombatSpells extends MagicRequirements {

	public static void attemptDate(Client c, int action) {
		switch (action) {
			case 4135:
				bonesToBase(c, 15, new int[]{EARTH, WATER, NATURE},
						new int[]{2, 2, 1}, new int[]{526, 1963});
				break;
			case 62005:
				bonesToBase(c, 60, new int[]{NATURE, WATER, EARTH},
						new int[]{2, 4, 4}, new int[]{526, 6883});
				break;
		}
	}

	private static void bonesToBase(Client c, int levelReq, int[] runes,
			int[] amount, int[] item) {
		if (!hasRequiredLevel(c, levelReq)) {
			c.sendMessage(
					"You need to have a magic level of levelReq to cast this spell.");
			return;
		}
		if (!hasRunes(c, new int[]{runes[0], runes[1], runes[2]},
				new int[]{amount[0], amount[1], amount[2]})) {
			return;
		}
		if ((!c.getItems().playerHasItem(item[0], 1))) {
			c.sendMessage("You need some " + ItemConstants.getItemName(item[0])
					+ " to cast this spell!");
			return;
		}
		c.getItems().replaceItemAll(item[0], item[1]);

		c.startGraphic(Graphic.create(141, GraphicType.HIGH));
		c.startAnimation(722);
		c.getPA().addSkillXP(100 * c.getItems().getItemAmount(item[0]), 6);
		c.sendMessage("You use your magic power to convert bones into "
				+ ItemConstants.getItemName(item[1]).toLowerCase().toLowerCase()
				+ "" + (item[1] != 1963 ? ("e") : ("")) + "s!");
		c.getCombat().resetPlayerAttack();
	}

	public static void superHeatItem(Client c, int itemID) {
		if (!hasRequiredLevel(c, 43)) {
			c.sendMessage(
					"You need to have a magic level of 43 to cast this spell.");
			return;
		}
		if (!hasRunes(c, new int[]{FIRE, NATURE}, new int[]{4, 2})) {
			return;
		}
		int[][] data = {{436, 1, 438, 1, 2349, 53}, // TIN
				{438, 1, 436, 1, 2349, 53}, // COPPER
				{440, 1, -1, -1, 2351, 53}, // IRON ORE
				{442, 1, -1, -1, 2355, 53}, // SILVER ORE
				{444, 1, -1, -1, 2357, 23}, // GOLD BAR
				{447, 1, 453, 4, 2359, 30}, // MITHRIL ORE
				{449, 1, 453, 6, 2361, 38}, // ADDY ORE
				{451, 1, 453, 8, 2363, 50}, // RUNE ORE
		};
		for (int i = 0; i < data.length; i++) {
			if (itemID == data[i][0]) {
				if (!c.getItems().playerHasItem(data[i][2], data[i][3])) {
					c.sendMessage(
							"You haven't got enough "
									+ ItemConstants.getItemName(data[i][2])
											.toLowerCase()
									+ " to cast this spell!");
					return;
				}
				c.getItems().deleteItemInOneSlot(itemID,
						c.getItems().getItemSlot(itemID), 1);
				for (int lol = 0; lol < data[i][3]; lol++) {
					c.getItems().deleteItemInOneSlot(data[i][2],
							c.getItems().getItemSlot(data[i][2]), 1);
				}
				c.getItems().addItem(data[i][4], 1);
				c.getPA().addSkillXP(data[i][5], 6);
				c.startAnimation(725);
				c.startGraphic(Graphic.create(148, GraphicType.HIGH));
				c.getPacketSender().sendFrame106(6);
				return;
			}
		}
		c.sendMessage("You can only cast superheat item on ores!");
	}
}
