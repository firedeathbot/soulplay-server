package com.soulplay.content.player.combat.magic;

import com.soulplay.game.model.player.Client;

public class MagicData {

	public static final int[] AIR_RUNES = {556, 4697, 4695, 4696};
	public static final int[] WATER_RUNES = {555, 4694, 4695, 4698};
	public static final int[] EARTH_RUNES = {557, 4696, 4699, 4698};
	public static final int[] FIRE_RUNES = {554, 4694, 4697, 4699};
	public static final int[] LAW_RUNES = {563};
	public static final int[] NATURE_RUNES = {561};

	public static final int[] DG_AIR_RUNES = {17780, 16091};
	public static final int[] DG_WATER_RUNES = {17781, 16092};
	public static final int[] DG_EARTH_RUNES = {17782, 16093};
	public static final int[] DG_FIRE_RUNES = {17783, 16094};
	public static final int[] DG_MIND_RUNES = {17784, 16095};
	public static final int[] DG_CHAOS_RUNES = {17785, 16096};
	public static final int[] DG_DEATH_RUNES = {17786, 16097};
	public static final int[] DG_BLOOD_RUNES = {17787, 16098};
	public static final int[] DG_BODY_RUNES = {17788, 16099};
	public static final int[] DG_COSMIC_RUNES = {17789, 16100};
	public static final int[] DG_ASTRAL_RUNES = {17790, 16101};
	public static final int[] DG_NATURE_RUNES = {17791, 16102};
	public static final int[] DG_LAW_RUNES = {17792, 16103};
	public static final int[] DG_SOUL_RUNES = {17793, 16104};

	public static int getEndGfxHeightDUMP(int buttonId) {
		switch (buttonId) {
			case 12987:
			case 12901:
			case 12861:
			case 12445:
			case 1192:
			case 13011:
			case 12919:
			case 12881:
			case 12999:
			case 12911:
			case 12871:
			case 13023:
			case 12929:
			case 12891:
			case 1162:
			case 1178:
				return 0;

			default:
				return 100;
		}
	}
	
	public static int getProjStartHeightDump(int buttonId) {
		switch (buttonId) {
			case 1562: // stun
				return 25;

			case 12939:// smoke rush
				return 35;

			case 12987: // shadow rush
				return 38;

			case 12861: // ice rush
				return 15;

			case 12951: // smoke blitz
				return 38;

			case 12999: // shadow blitz
				return 25;

			case 12911: // blood blitz
				return 25;

			default:
				return 43;
		}
	}
	
	public static int getProjEndHeightDump(int buttonId) {
		switch (buttonId) {
			case 1562: // stun
				return 10;

			case 12939: // smoke rush
				return 20;

			case 12987: // shadow rush
				return 28;

			case 12861: // ice rush
				return 10;

			case 12951: // smoke blitz
				return 28;

			case 12999: // shadow blitz
				return 15;

			case 12911: // blood blitz
				return 10;

			default:
				return 31;
		}
	}

	public static int getFreezeTimeDump(int buttonId) {
		switch (buttonId) {
			case 1572:
			case 12861: // ice rush
				return 10;

			case 1582:
			case 12881: // ice burst
				return 17;

			case 1592:
			case 12871: // ice blitz
				return 25;

			case 12891: // ice barrage
				return 33;

			default:
				return 0;
		}
	}

	public static int getStaffNeeded(Client c, int buttonId, boolean asdf) {
		switch (buttonId) {
			case 1539:
				return 1409;

			case 12037:
				return 4170;

			case 1190:
				return 2415;

			case 1191:
				return 2416;

			case 1192:
				return 2417;

			default:
				return 0;
		}
	}

	public static boolean godSpells(Client c, int buttonId) {
		switch (buttonId) {
			case 1190:
				return true;

			case 1191:
				return true;

			case 1192:
				return true;

			default:
				return false;
		}
	}

	public static boolean multiSpellsDump(int buttonId) {
		switch (buttonId) {
			case 12891:
			case 12881:
			case 13011:
			case 13023:
			case 12919: // blood spells
			case 12929:
			case 12963:
			case 12975:
				return true;
		}
		return false;
	}

}
