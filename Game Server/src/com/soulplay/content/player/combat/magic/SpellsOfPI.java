package com.soulplay.content.player.combat.magic;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.materials.PlankMake;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class SpellsOfPI {

	public static void init() {
		SpellsData.load();
		loadPlanks();
	}
	
	public static void castTeleGrab(final Client c, final int itemId, final int itemX, final int itemY, final SpellsData spell) {
		c.setAttackTimer(5);
		int ticksToGrab = 5;
		c.faceLocation(itemX, itemY);
		c.getMovement().resetWalkingQueue();
		c.startAnimation(spell.getAnim());
		if (spell.getStartGfx() != null)
			c.startGraphic(spell.getStartGfx());
		if (spell.getEndGfx() != null) {
			Server.getStillGraphicsManager().stillGraphics(c, itemX, itemY, 0, spell.getEndGfx().getId(), Misc.serverToClientTick(ticksToGrab));
		}
		if (spell.getProjectile() != null) {
			Projectile proj = new Projectile(spell.getProjectile().getProjectileGFX(), c.getCurrentLocation().copyNew(), Location.create(itemX, itemY, c.getZ()));
			proj.setEndDelay(100);
			proj.setEndHeight(0);
			GlobalPackets.createProjectile(proj);
		}
		
		final int itemZ = c.getZ();
		
		c.teleGrabEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (c != null)
					ItemHandler.removeGroundItem(c, itemId, itemX, itemY, itemZ, true);
				
			}
		}, ticksToGrab);
	}
	
	public static boolean lunarSpells(final Client c, final int buttonId) {
		switch (buttonId) {
		case 117078: // make planks
			makePlankSpell(c, buttonId);
			return true;
			/* VENG */
		case 117086:
			c.getPA().castVeng(false);
			return true;
		}
		return false;
	}

	private static final Map<Integer, PlankMake> PLANKS = new HashMap<Integer, PlankMake>();
	private static void loadPlanks() {
		PLANKS.put(1511, PlankMake.REGULAR); // plank
		PLANKS.put(1521, PlankMake.OAK); // oak plank
		PLANKS.put(6333, PlankMake.TEAK); // teak
		PLANKS.put(6332, PlankMake.MAHOGANY); // mahogany
	}

	private static final Graphic PLANK_GFX = Graphic.create(1063, GraphicType.HIGH);
	private static final Animation PLANK_ANIM = new Animation(6298);
	private static final Requirement PLANK_REQ = new Requirement(new GameItem(557, 15), new GameItem(561, 1), new GameItem(9075, 2), null, 86);
	
	private static void makePlankSpell(final Client c, int buttonId) {
		if (!c.getStopWatch().checkThenStart(3)) {
			c.getPA().resetFollow();
			return;
		}

		if (c.skillingEvent != null) {
			return;
		}

		c.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				boolean casted = false;
				itemLoop: for (int i = 0; i < c.playerItems.length; i++) {
					int itemId = c.playerItems[i] - 1;
					PlankMake plank = PLANKS.get(itemId);
					if (plank != null) {
						if (!MagicRequirements.checkMagicReqsNew(c, PLANK_REQ, null, true)) {
							container.stop();
							return;
						}

						c.playerItems[i] = plank.getPlankId() + 1;
						c.setUpdateInvInterface(3214);
						c.startGraphic(PLANK_GFX);
						c.startAnimation(PLANK_ANIM);
						c.getPA().addSkillXP(90, Skills.MAGIC);
						casted = true;
						break itemLoop;
					}
				}

				if (!casted) {
					c.sendMessage("You don't have anymore logs to turn into planks.");
					container.stop();
				}
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}

		}, 3);
	}
	
	public static final Requirement WILD_TELE_REQ = new Requirement(new GameItem(563, 2), null, null, null, 1);
	
}
