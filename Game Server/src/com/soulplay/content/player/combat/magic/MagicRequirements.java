package com.soulplay.content.player.combat.magic;

import com.soulplay.Config;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.degradeable.DegradeData;
import com.soulplay.content.items.impl.MagicStaff;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

import plugin.item.TomeOfFirePlugin;

public class MagicRequirements extends MagicData {

	public static final int FIRE = 554;

	public static final int WATER = 555;

	public static final int AIR = 556;

	public static final int EARTH = 557;

	public static final int MIND = 558;

	public static final int BODY = 559;

	public static final int DEATH = 560;

	public static final int NATURE = 561;

	public static final int CHAOS = 562;

	public static final int LAW = 563;

	public static final int COSMIC = 564;

	public static final int BLOOD = 565;

	public static final int SOUL = 566;

	public static final int ASTRAL = 9075;

	public static final int ARMADYL = 21773;
	
	private static final int[] GOD_SPELL_SARA_STAVES = { 2415, 15486, 22206, 22207, 22208, 22209, 22210, 22211, 22212, 22213 };
	
	private static final int[] GOD_SPELL_ZAM_STAVES = { 2417, 11791, DegradeData.TSOTD, DegradeData.TSOTD_UNCHARGED };
	
	private static final int[] GOD_SPELL_GUTHIX_STAVES = { 2416, 8841, 124144 };

	private static final int[] ZURIEL_STAFFS = { 13867, 13869, 13941, 13943};

	private static RuneCheckResponse checkItems(Player c, int id) {
		if (c.getItems().playerHasItem(RunePouch.RUNE_POUCH_ITEM)) {
			int index = RunePouch.getRuneIndex(c, id);
			if (index != -1) {
				return new RuneCheckResponse(RuneCheckType.POUCH, c.runePouchItemsN[index], new int[] {id});
			}
		}

		switch (id) {
		case AIR:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}
			if (MagicStaff.wearingStaff(c, MagicStaff.AIR)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(AIR_RUNES), AIR_RUNES);
		case WATER:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}
			if (MagicStaff.wearingStaff(c, MagicStaff.WATER)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			if (c.playerEquipment[PlayerConstants.playerShield] == 18346) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(WATER_RUNES), WATER_RUNES);
		case EARTH:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}
			if (MagicStaff.wearingStaff(c, MagicStaff.EARTH)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(EARTH_RUNES), EARTH_RUNES);
		case FIRE:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}
			if (MagicStaff.wearingStaff(c, MagicStaff.FIRE)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			if (c.playerEquipment[PlayerConstants.playerShield] == TomeOfFirePlugin.CHARGED_ID) {
				return new RuneCheckResponse(RuneCheckType.TOME_OF_FIRE, c.getCharges(TomeOfFirePlugin.CHARGED_ID));
			}
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(FIRE_RUNES), FIRE_RUNES);
		case LAW:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}
			if (MagicStaff.wearingStaff(c, MagicStaff.LAW)) {
				return new RuneCheckResponse(RuneCheckType.LAW_STAFF, c.getLawStaffCharges());
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(LAW_RUNES), LAW_RUNES);
		case NATURE:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}
			if (MagicStaff.wearingStaff(c, MagicStaff.NATURE)) {
				return new RuneCheckResponse(RuneCheckType.NATURE_STAFF, c.getNatureStaffCharges());
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(NATURE_RUNES), NATURE_RUNES);
		case 8843:
			if (c.getItems().playerHasEquippedWeapons(GOD_SPELL_GUTHIX_STAVES)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return null;
		case 2415:
			if (c.getItems().playerHasEquippedWeapons(GOD_SPELL_SARA_STAVES)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return null;
		case 2417:
			if (c.getItems().playerHasEquippedWeapons(GOD_SPELL_ZAM_STAVES)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return null;
		case 13867:
			if (c.getItems().playerHasEquippedWeapons(ZURIEL_STAFFS)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return null;
		case 1409:
		case 4170:
			if (c.getItems().playerHasEquippedWeapon(id)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}
			return null;
		case 17780:
			if (MagicStaff.wearingStaff(c, MagicStaff.AIR_DUNG)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_AIR_RUNES), DG_AIR_RUNES);
		case 17784:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_MIND_RUNES), DG_MIND_RUNES);
		case 17781:
			if (MagicStaff.wearingStaff(c, MagicStaff.WATER_DUNG)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_WATER_RUNES), DG_WATER_RUNES);
		case 17782:
			if (MagicStaff.wearingStaff(c, MagicStaff.EARTH_DUNG)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_EARTH_RUNES), DG_EARTH_RUNES);
		case 17783:
			if (MagicStaff.wearingStaff(c, MagicStaff.FIRE_DUNG)) {
				return new RuneCheckResponse(RuneCheckType.STAFF);
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_FIRE_RUNES), DG_FIRE_RUNES);
		case 17788:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_BODY_RUNES), DG_BODY_RUNES);
		case 17789:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_COSMIC_RUNES), DG_COSMIC_RUNES);
		case 17785:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_CHAOS_RUNES), DG_CHAOS_RUNES);
		case 17790:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_ASTRAL_RUNES), DG_ASTRAL_RUNES);
		case 17791:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_NATURE_RUNES),DG_NATURE_RUNES);
		case 17792:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_LAW_RUNES), DG_LAW_RUNES);
		case 17786:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_DEATH_RUNES), DG_DEATH_RUNES);
		case 17787:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_BLOOD_RUNES), DG_BLOOD_RUNES);
		case 17793:
			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(DG_SOUL_RUNES), DG_SOUL_RUNES);
		default:
			if (c.getSeasonalData().containsPowerUp(PowerUpData.MAGIC_KNOWLEDGE)) {
				return new RuneCheckResponse(RuneCheckType.SEASONAL_POWER);
			}

			return new RuneCheckResponse(RuneCheckType.INVENTORY, c.getItems().countItems(id), new int[] {id});
		}
	}

	public static boolean checkMagicReqsNew(Player c, SpellsData spell, boolean deleteRunes) {
		return checkMagicReqsNew(c, spell.getRequirement(), spell, deleteRunes);
	}

	public static boolean checkMagicReqsNew(Player c, Requirement requirement, SpellsData spell, boolean deleteRunes) {
		if (Config.MAGIC_LEVEL_REQUIRED) { // check magic level
			if (requirement.getLevel() != -1 && c.getSkills().getLevel(Skills.MAGIC) < requirement.getLevel()) {
				c.sendMessage("You need to have a magic level of "
						+ requirement.getLevel()
						+ " to cast this spell.");
				return false;
			}
		}

		if (c.duelRule[DuelRules.NO_MAGIC]) {
			c.sendMessage("Magic has been disabled in this duel!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			if (c.getAutoCastSpell() != null) {
				c.getPA().resetAutocast();
			}
			return false;
		}
		
		if (!c.getGodSpell().canCastGodSpell(spell)) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			if (c.getAutoCastSpell() != null) {
				c.getPA().resetAutocast();
			}
			return false;
		}

		if (c.playerIndex > 0 && PlayerHandler.players[c.playerIndex] != null) {
			if (spell == SpellsData.TELEBLOCK && PlayerHandler.players[c.playerIndex].isTeleBlocked()) {
				c.sendMessage("That player is already affected by this spell.");
				c.getMovement().resetWalkingQueue();
				c.getCombat().resetPlayerAttack();
				return false;
			}
		}

		RuneCheckResponse first = null;
		RuneCheckResponse second = null;
		RuneCheckResponse third = null;
		RuneCheckResponse fourth = null;
		if (Config.RUNES_REQUIRED) {
			
			if (spell != null) {
				switch (spell) {
				case TRIDENT_OF_THE_SEA:
				case TRIDENT_OF_THE_SWAMP:
				case SANGUINESTI_STAFF:
					if (c.getCharges(c.playerEquipment[PlayerConstants.playerWeapon]) <= 0) {
						c.sendMessage("Your trident has run out of charges.");
						c.getMovement().resetWalkingQueue();
						c.getCombat().resetPlayerAttack();
						return false;
					}
					break;

				default:
					break;
				}
			}
			
			if (requirement.getRunesReq1() != null) {
				first = checkRunes(c, requirement.getRunesReq1());
				if (first == null) {
					noRunes(c);
					return false;
				}
			}

			if (requirement.getRunesReq2() != null) {
				second = checkRunes(c, requirement.getRunesReq2());
				if (second == null) {
					noRunes(c);
					return false;
				}
			}

			if (requirement.getRunesReq3() != null) {
				third = checkRunes(c, requirement.getRunesReq3());
				if (third == null) {
					noRunes(c);
					return false;
				}
			}

			if (requirement.getRunesReq4() != null) {
				fourth = checkRunes(c, requirement.getRunesReq4());
				if (fourth == null) {
					noRunes(c);
					return false;
				}
			}

			if (deleteRunes) {
				if (c.getItems().playerHasEquippedWeapon(30089) && spell != null && spell.getMaxHit() > 0 && Misc.random(100) <= 15) {
					c.sendMessageSpam("Your Kodai wand saved some runes.");
					return true;
				}

				//System.out.println("All checks passed delete");
				if (first != null) {
					deleteRunes(c, first, requirement.getRunesReq1().getAmount());
				}
				if (second != null) {
					deleteRunes(c, second, requirement.getRunesReq2().getAmount());
				}
				if (third != null) {
					deleteRunes(c, third, requirement.getRunesReq3().getAmount());
				}
				if (fourth != null) {
					deleteRunes(c, fourth, requirement.getRunesReq4().getAmount());
				}
			}
		}

		return true;
	}
	
	public static boolean checkMagicReqsNoMessage(Player c, SpellsData spell, boolean deleteRunes) {
		return checkMagicReqsNoMessage(c, spell.getRequirement(), spell, deleteRunes);
	}

	public static boolean checkMagicReqsNoMessage(Player c, Requirement requirement, SpellsData spell, boolean deleteRunes) {
		if (Config.MAGIC_LEVEL_REQUIRED) { // check magic level
			if (requirement.getLevel() != -1 && c.getSkills().getLevel(Skills.MAGIC) < requirement.getLevel()) {
				return false;
			}
		}

		if (!c.getGodSpell().canCastGodSpell(spell)) {
			return false;
		}

		RuneCheckResponse first = null;
		RuneCheckResponse second = null;
		RuneCheckResponse third = null;
		RuneCheckResponse fourth = null;
		if (Config.RUNES_REQUIRED) {
			if (requirement.getRunesReq1() != null) {
				first = checkRunes(c, requirement.getRunesReq1());
				if (first == null) {
					return false;
				}
			}

			if (requirement.getRunesReq2() != null) {
				second = checkRunes(c, requirement.getRunesReq2());
				if (second == null) {
					return false;
				}
			}

			if (requirement.getRunesReq3() != null) {
				third = checkRunes(c, requirement.getRunesReq3());
				if (third == null) {
					return false;
				}
			}

			if (requirement.getRunesReq4() != null) {
				fourth = checkRunes(c, requirement.getRunesReq4());
				if (fourth == null) {
					return false;
				}
			}

			if (deleteRunes) {
				if (c.getItems().playerHasEquippedWeapon(30089) && spell != null && spell.getMaxHit() > 0 && Misc.random(100) <= 15) {
					c.sendMessageSpam("Your Kodai wand saved some runes.");
					return true;
				}

				//System.out.println("All checks passed delete");
				if (first != null) {
					deleteRunes(c, first, requirement.getRunesReq1().getAmount());
				}
				if (second != null) {
					deleteRunes(c, second, requirement.getRunesReq2().getAmount());
				}
				if (third != null) {
					deleteRunes(c, third, requirement.getRunesReq3().getAmount());
				}
				if (fourth != null) {
					deleteRunes(c, fourth, requirement.getRunesReq4().getAmount());
				}
			}
		}

		return true;
	}

	public static void deleteRunes(Player c, RuneCheckResponse entry, int amount) {
		switch(entry.getType()) {
			case LAW_STAFF: {
				boolean useCharge = Misc.random(9) == 0;
				if (useCharge) {
					c.removeLawStaffCharge();
					c.sendMessage("Your staff has used a law rune.");
				}

				break;
			}
			case NATURE_STAFF: {
				boolean useCharge = Misc.random(9) == 0;
				if (useCharge) {
					c.removeNatureStaffCharge();
					c.sendMessage("Your staff magically pays for the cost of the nature rune.");
				}

				break;
			}
			case TOME_OF_FIRE: {
				if (c.reduceCharge(TomeOfFirePlugin.CHARGED_ID, 1) == 0) {
					c.getItems().replaceEquipment(PlayerConstants.playerShield, TomeOfFirePlugin.EMPTY_ID);
					c.sendMessage("<col=FF0000>Your Tome of fire has ran out of charges!");
				}
				break;
			}
			case INVENTORY: {
				int[] runes = entry.getRunes();
				for (int rune : runes) {
					int slot = c.getItems().getItemSlot(rune);
					if (slot == -1) {
						continue;
					}

					int count = c.playerItemsN[slot];
					int deleteCount = Math.min(amount, count);
					c.getItems().deleteItemInOneSlot(rune, slot, deleteCount);
					amount -= deleteCount;
					if (amount <= 0) {
						break;
					}
				}
				//System.out.println(Arrays.toString(runes)+":"+amount);
				break;
			}
			case POUCH: {
				int[] runes = entry.getRunes();
				for (int rune : runes) {
					int slot = RunePouch.getRuneIndex(c, rune);
					if (slot == -1) {
						continue;
					}

					int count = c.runePouchItemsN[slot];
					int deleteCount = Math.min(amount, count);
					RunePouch.deleteRune(c, rune, deleteCount);
					amount -= deleteCount;
					if (amount <= 0) {
						break;
					}
				}
				break;
			}
			default:
				break;
		}
	}

	public static RuneCheckResponse checkRunes(Player c, GameItem item) {
		RuneCheckResponse entry = checkItems(c, item.getId());
		if (entry == null) {
			return null;
		}

		switch(entry.getType()) {
			case INVENTORY:
			case POUCH:
			case LAW_STAFF:
				if (entry.getCount() < item.getAmount()) {
					return null;
				}
				break;
			default:
				break;
		}

		return entry;
	}

	private static void noRunes(Player c) {
		c.sendMessage("You don't have the required runes to cast this spell.");
		if (c.getAutoCastSpell() != null) {
			c.getPA().resetAutocast();
		}
	}

	public static void deleteRunes(Player c, int[] runes, int[] amount) {
		if (c.playerRights == 0) {
			for (int i = 0; i < runes.length; i++) {
				c.getItems().deleteItemInOneSlot(runes[i],
						c.getItems().getItemSlot(runes[i]), amount[i]);
			}
		}
	}

	public static boolean hasRequiredLevel(Player c, int i) {
		return c.getSkills().getLevel(Skills.MAGIC) >= i;
	}

	public static boolean hasRunes(Player c, int[] runes, int[] amount) {
		if (c.playerRights > 0) {
			return true;
		}

		for (int i = 0; i < runes.length; i++) {
			if (c.getItems().playerHasItem(runes[i], amount[i])) {
				return true;
			}
		}

		c.sendMessage("You don't have enough required runes to cast this spell!");
		return false;
	}

}
