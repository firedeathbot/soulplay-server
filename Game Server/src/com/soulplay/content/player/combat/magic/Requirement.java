package com.soulplay.content.player.combat.magic;

import com.soulplay.game.model.item.GameItem;

public class Requirement {

	private final GameItem runesReq1;
	private final GameItem runesReq2;
	private final GameItem runesReq3;
	private final GameItem runesReq4;
	private final int level;

	public Requirement(GameItem runesReq1, GameItem runesReq2, GameItem runesReq3, GameItem runesReq4, int level) {
		this.runesReq1 = runesReq1;
		this.runesReq2 = runesReq2;
		this.runesReq3 = runesReq3;
		this.runesReq4 = runesReq4;
		this.level = level;
	}

	public GameItem getRunesReq1() {
		return runesReq1;
	}

	public GameItem getRunesReq2() {
		return runesReq2;
	}

	public GameItem getRunesReq3() {
		return runesReq3;
	}

	public GameItem getRunesReq4() {
		return runesReq4;
	}

	public int getLevel() {
		return level;
	}

}
