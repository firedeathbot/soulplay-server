package com.soulplay.content.player.combat;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.ProjectileData;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy.AccuracyType;
import com.soulplay.content.player.combat.magic.MagicData;
import com.soulplay.content.player.combat.magic.MultiBarrage;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.magic.MagicMaxHit;
import com.soulplay.content.player.combat.melee.MeleeData;
import com.soulplay.content.player.combat.melee.MeleeExtras;
import com.soulplay.content.player.combat.melee.MeleeMaxHit;
import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.range.RangeData;
import com.soulplay.content.player.combat.range.RangeMaxHit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Mob;

public class CombatAssistant {

	private Client c;

	public CombatAssistant(Client Client) {
		this.c = Client;
	}

	public void activatePrayer(int i) {
		CombatPrayer.activatePrayer(c, i);
	}
	
	public void resetPriorityLeftClick() {
		c.attackingIndex = -1;
		c.setInteractionIndex(-1);
	}

	public void appendHit(final Player c2, int damage, int mask, int icon,
			boolean playerHitting, AccuracyType accuracyType) {
		if (c2.isDead()) {
			return;
		}
		if (c2.getHitPoints() < 1) {
			return;
		}
		if (c2.teleTimer > 0) {
			return;
		}

		damage = c2.applyDamageReduction(damage, CombatType.list(icon));
		if (accuracyType != null && accuracyType == AccuracyType.BLOCKED) {
			CombatAccuracy.applyBlockedDamage(c2);
		}

		damage = c2.dealDamage(new Hit(damage, mask, icon));
		c2.getDamageMap().incrementTotalDamage(c.mySQLIndex, damage);
	}

	public void appendHit(final Player c2, int damage, int mask, int icon,
			boolean playerHitting, int soak) { // allantois addition for
												// summoning
		
		if (c2.teleTimer > 0) {
			return;
		}
		if (c2.inDuelArena()) {
			return;
		}

		damage = c2.applyDamageReduction(damage, CombatType.list(icon));

		damage = c2.dealDamage(new Hit(damage, mask, icon));

		c2.getDamageMap().incrementTotalDamage(c.mySQLIndex, damage);

	}

	public int appendHit(final Client c2, int damage, int mask, int icon,
			int damageMask, boolean maxHit) {
		if (c2 == null) {
			return 0;
		}
		if (c2.isDead()) {
			return 0;
		}
		if (c2.getHitPoints() < 1) {
			return 0;
		}

		damage = c2.applyDamageReduction(damage, CombatType.list(icon));

		damage = c2.dealDamage(new Hit(damage, mask, icon));

		c2.getDamageMap().incrementTotalDamage(c.mySQLIndex, damage);

		return damage;
	}

	public void appendHit(NPC n, int damage, int mask, int icon,
			int damageMask) {
		boolean maxHit = false;
		if (c.teleTimer > 0) {
			return;
		}
		switch (icon) {
			case 0:
				maxHit = damage >= calculateMeleeMaxHit(1.0) - 2;
				break;
			case 1:
				maxHit = damage >= rangeMaxHit(n, 1.0) - 2;
				break;
			case 2:
				// maxHit = damage == finalMagicDamage(c);
				break;
		}
		if (maxHit) {
			mask = 1;
		}
		n.dealDamage(new Hit(damage, mask, icon, c));
		n.onHitted(c, damage);
	}

	public void appendHit(NPC n, int damage, int mask, int icon, int damageMask,
			boolean maxHit) {
		if (n == null) {
			return;
		}
		if (n.getSkills().getLifepoints() < damage) {
			damage = n.getSkills().getLifepoints();
		}
		n.getSkills().setLifepoints(n.getSkills().getLifepoints() - damage);
		n.handleHitMask(damage, mask, icon);
	}

	public void appendVengeance(Client o, int damage) {
		MeleeExtras.appendVengeance(c, o, damage);
	}

	public void appendVengeanceNPC(final NPC npc, int damage) {
		MeleeExtras.appendVengeanceNPC(c, npc, damage);
	}

	public void applyRecoil(int damage, Client o) {
		MeleeExtras.applyRecoilMob(o, damage, c);
	}

	public void applySmite(final Client c2, boolean smiteActive, int... hits) {
		MeleeExtras.applySmite(c, c2, smiteActive, hits);
	}

	public void attackNpc(int i) {
		AttackNPC.attackNpc(c, i);
	}

	public void attackPlayer(int i) {
		AttackPlayer.attackPlayer(c, i);
	}

	public int calculateMeleeMaxHit(double mod) {
		// return (int)MeleeMaxHit.calculateBaseDamage(c, c.usingSpecial);
		int maxHit = MeleeMaxHit.calculateMeleeMaxHit(c, mod);
		if (c.displayMaxHit) {
			c.sendMessage("Melee Max hit: "
					+ maxHit);
		}
		return maxHit;
	}

	public boolean checkMultiBarrageReqs(final Client c2) {
		return MultiBarrage.checkMultiBarrageReqs(c, c2);
	}

	public boolean checkReqs(Client o) {
		int prevPlayerIndex = c.playerIndex;
		if (!MeleeRequirements.checkReqs(c, o, true)) {
			if (c.huntPlayerIndex > 0) {
				if (c.huntPlayerIndex == prevPlayerIndex) {
					c.getPA().resetHuntId();
				}
			}
			return false;
		}
		return true;
	}

	public int damageSoaked(int damage, int type) {
		if (damage <= 20) {
			return 0;
		}
		return (int) ((damage - 20) * soakPercentage(type));
	}

	/**
	 * Disables the effect of the spirit shield
	 *
	 * @param o
	 *            - the person who is getting hit by special, has his divine
	 *            spirit shield disabled
	 */
	public void disableSpiritShield(Player o, final int hits) {
		if (o.playerEquipment[PlayerConstants.playerShield] != 13740) {
			return;
		}
		c.sendMessage("You have disabled " + o.getNameSmartUp()
				+ "'s Spirit Shield for " + hits + " hits.");
		o.setSpiritShieldHits(hits);
		o.sendMessage(
				"Your Spirit shield has been disabled for " + hits + " hits.");

	}

	public int getAttackDelay(SpellsData spell) {
		// System.out.println("attack delay "+MeleeData.getAttackDelay(c, s));
		return MeleeData.getAttackDelayNew(c, spell);
//		return MeleeData.getAttackDelay(c, s);
	}

	public int getBlockEmote() {
		return MeleeData.getBlockEmoteNew(c);
//		return MeleeData.getBlockEmote(c);
	}

	/**
	 * Get difference in bonuses that are gained from equipment.
	 *
	 * @param o
	 *            - Other player to compare with
	 * @return positive number means other player has more bonuses
	 */
	public int getBonusDifference(Client o) {
		int otherPlayerBonuses = 0;
		int botBonuses = 0;
		for (int k = 0; k < o.getPlayerBonus().length; k++) {
			otherPlayerBonuses += o.getPlayerBonus()[k];
		}

		for (int k = 0; k < o.getPlayerBonus().length; k++) {
			botBonuses += c.getPlayerBonus()[k];
		}

		return otherPlayerBonuses - botBonuses;
	}

	public int getCombatDifference(int combat1, int combat2) {
		return MeleeRequirements.getCombatDifference(combat1, combat2);
	}

	public void getPlayerAnimIndex(int weaponId) {
		// if (c.transformNpcId <= 0 && c.getLastWeaponRefreshed() !=
		// c.playerEquipment[PlayerConstants.playerWeapon]) { // cheaphax to
		// stop PI spamming refresh animations
		MeleeData.getPlayerAnimIndex(c, weaponId);
		// }
	}

	public void getPlayerAnimIndexBypass() {
		MeleeData.getPlayerAnimIndex(c,
				c.playerEquipment[PlayerConstants.playerWeapon]);
	}

	public int getStartDelay(int dist) {
		return ProjectileData.getStartDelay(c, dist);
	}

	public int getProjectileSpeed(int dist) {
//		 c.sendMessage("projectile speed: "+ProjectileData.getProjectileSpeed(c, dist));
		return ProjectileData.getProjectileSpeed(c, dist);
	}

	public int getRangeStr(int i) {
		return RangeData.getRangeStr(i);
	}

	public Animation getWepAnim(String weaponName) {
		return MeleeData.getWepAnim(c);
	}

	public boolean godSpells(int buttonId) {
		return MagicData.godSpells(c, buttonId);
	}

	public void handleDfs(final Client c, final Client o) {
		MeleeExtras.handleDragonFireShield(c, o);
	}

	public void handleDfsNPC(final Client c, final NPC n) {
		MeleeExtras.handleDragonFireShieldNPC(c, n);
	}

	public void handleGmaulPlayer(SpecialAttack spec) {
		if (c.isDead())
			return;
		MeleeExtras.graniteMaulSpecial(c, spec);
	}

	public void handlePrayerDrain() {
		CombatPrayer.handlePrayerDrain(c);
	}

	/**
	 * instantly displays the gfx
	 *
	 * @param gfxId
	 *            - gfx id
	 */
	public void hitEntityStillGfx(int gfxId, boolean gfx100) {
		if (gfx100) {
			if (c.playerIndex > 0) {
				if (PlayerHandler.players[c.playerIndex] != null) {
					PlayerHandler.players[c.playerIndex].startGraphic(Graphic.create(gfxId, GraphicType.HIGH));
				}
			}
			if (c.npcIndex > 0) {
				if (NPCHandler.npcs[c.npcIndex] != null) {
					NPCHandler.npcs[c.npcIndex].startGraphic(Graphic.create(gfxId, GraphicType.HIGH));
				}
			}
		} else {
			if (c.playerIndex > 0) {
				if (PlayerHandler.players[c.playerIndex] != null) {
					PlayerHandler.players[c.playerIndex].startGraphic(Graphic.create(gfxId, GraphicType.LOW));
				}
			}
			if (c.npcIndex > 0) {
				if (NPCHandler.npcs[c.npcIndex] != null) {
					NPCHandler.npcs[c.npcIndex].startGraphic(Graphic.create(gfxId));
				}
			}
		}
	}

	/**
	 * displays hit a player and show hitmark too
	 *
	 * @param damage
	 *            - damage to display
	 * @param mask:
	 *            0 - red box 2 - green box
	 * @param icon:
	 *            -1:nothing 0 - melee 1 - range 2 - magic 3 - recoil 4 - dwarf
	 *            multicannon
	 */
	public void hitPlayer(int damage, int maskRed0Green2, int icon) {
		if (c.isDead()) {
			return;
		}
		if (c.getHitPoints() < 1) {
			return;
		}

		damage = c.applyDamageReduction(damage, CombatType.list(icon));

		if (damage > c.getSkills().getLifepoints()) {
			damage = c.getSkills().getLifepoints();
		}

		damage = c.dealDamage(new Hit(damage, maskRed0Green2, icon));
	}

	public boolean kalphite1(int i) {
		switch (NPCHandler.npcs[i].npcType) {
			case 1158:
				return true;
		}
		return false;
	}

	public boolean kalphite2(int i) {
		switch (NPCHandler.npcs[i].npcType) {
			case 1160:
				return true;
		}
		return false;
	}

	public int magicMaxHit(SpellsData spellData) {
		return magicMaxHit(spellData, 0);
	}

	public int magicMaxHit(int baseDamage) {
		return magicMaxHit(null, baseDamage);
	}

	private int magicMaxHit(SpellsData spellData, int baseDamage) {
		int maxHit = MagicMaxHit.magicMaxHit(c, spellData, baseDamage);
		if (c.displayMaxHit) {
			c.sendMessage("Magic Max hit: " + maxHit);
		}
		return maxHit;
	}

	public int rangeMaxHit(double mod) {
		return rangeMaxHit(null, mod);
	}

	public int rangeMaxHit(Mob defender, double mod) {
		double mod2 = 1.0;
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 30092) { // twisted bow effect
			int enemyMagicLevel = 1;
			if (defender != null) {
				enemyMagicLevel = defender.getSkills().getLevel(Skills.MAGIC);
				if (defender.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] > enemyMagicLevel)
					enemyMagicLevel = defender.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()];
			}
			mod2 = (250 + Math.floor((3*enemyMagicLevel-14)/100) - Math.floor((Math.pow(0.3*enemyMagicLevel-140, 2) /100 )))/100;
			
			if (mod2 > 3)
				mod2 -= 1;
		}

		if (defender != null && defender.isNpc()) {
			NPC npc = defender.toNpcFast();
			if (ItemConstants.isDragonBaneWeapon(c)) {
				if (npc.isDragon) {
					mod2 = 1.30;
				}
			} else if (npc.hexHunterEffect() && RangeMaxHit.wearingHexHunter(c.playerEquipment[PlayerConstants.playerWeapon])) {
				mod2 += 1;
			}
		}

		int powerUpAmount = c.getSeasonalData().getPowerUpAmount(PowerUpData.RANGE_DAMAGE);
		if (powerUpAmount > 0) {
			mod2 += powerUpAmount * 0.05;
		}

		int hit = RangeMaxHit.maxHit(c, mod*mod2);

		if (c.displayMaxHit) {
			c.sendMessage("Range Max hit: " + hit);
		}
		return hit;
	}

	public void reducePrayerLevel() {
		CombatPrayer.reducePrayerLevel(c);
	}

	public void resetPlayerAttack() {
		if (c.playerIndex > 0 || c.npcIndex > 0) {
			MeleeData.resetPlayerAttack(c);
			// c.sendMessage("Attack Reset");
		}
	}

	public void resetPrayers() {
		CombatPrayer.resetPrayers(c);
	}

	public double soakPercentage(int type) {
		double total = 0;
		if (type == 0) {
			total = c.soakingBonus[0];
		} else if (type == 1) {
			total = c.soakingBonus[2];
		} else if (type == 2) {
			total = c.soakingBonus[1];
		}

		return total;
	}

	public boolean usingChins() {
		return c.playerEquipment[PlayerConstants.playerWeapon] == 10033
				|| c.playerEquipment[PlayerConstants.playerWeapon] == 10034
				|| c.playerEquipment[PlayerConstants.playerWeapon] == 30356; // chins
	}

	public boolean usingHally() {
		return MeleeData.usingHally(c);
	}

	public boolean usingZBow() {
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 20171
				|| c.playerEquipment[PlayerConstants.playerWeapon] == 20173) {
			return true;
		}
		return false;
	}

}
