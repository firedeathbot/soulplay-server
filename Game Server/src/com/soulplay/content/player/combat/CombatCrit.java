package com.soulplay.content.player.combat;

import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public class CombatCrit {

	public static double critMultiplier(Mob mob) {
		return 2.0;
	}

	public static int critChance(Mob mob) {
		int total = 0;

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			if (player.getSeasonalData().containsPowerUp(PowerUpData.CRITTER)) {
				total += 15;
			}

			int critsAmount = player.getSeasonalData().getPowerUpAmount(PowerUpData.CRITS);
			if (critsAmount > 0) {
				total += critsAmount * 3;
			}

			int minorCritsAmount = player.getSeasonalData().getPowerUpAmount(PowerUpData.MINOR_CRIT);
			if (minorCritsAmount > 0) {
				total += minorCritsAmount * 3;
			}
		}

		return total;
	}

	public static CritRollType rolledCrit(Mob mob) {
		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			if (player.getSeasonalData().procCritSprinter) {
				player.getSeasonalData().procCritSprinter = false;
				int chance = player.getSeasonalData().getPowerUpAmount(PowerUpData.CRIT_SPRINTER) * 25;
				int roll = Misc.random(100);
				debugCrit(player, roll, chance);
				if (roll <= chance) {
					return CritRollType.CRIT_SPRINTER;
				}

				return CritRollType.NO_CRIT;
			}
		}

		int chance = critChance(mob);
		if (chance <= 0) {
			return CritRollType.NO_CRIT;
		}

		int roll = Misc.random(100);
		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			debugCrit(player, roll, chance);
		}

		if (roll <= chance) {
			return CritRollType.CRIT;
		}

		return CritRollType.NO_CRIT;
	}

	public static void debugCrit(Player player, int roll, int chance) {
		if (player.displayAccuracyInfo) {
			player.sendMessage("Crit roll=" + roll + ", chance=" + chance);
		}
	}

	public static int applyCritBoost(Mob mob, int damage) {
		return (int) (damage * critMultiplier(mob));
	}

	public static enum CritRollType {
		NO_CRIT,
		CRIT,
		CRIT_SPRINTER;
	}

}
