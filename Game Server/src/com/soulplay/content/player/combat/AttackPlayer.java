package com.soulplay.content.player.combat;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.combat.range.RangeExtras;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.info.AnimationsData;

public class AttackPlayer {

	public static void applySkull(Player c, Player o) {
		if (c.duelStatus != 5 && !c.inFunPk() && !c.inClanWars() && !c.inCw()
				&& !c.inMinigame() && !c.inClanWarsFunPk() && !c.getZones().isInClwRedPortalPkZone()) {
			if (!c.attackedPlayers.contains(o.getId())
					&& !o.attackedPlayers.contains(c.getId())) {
				c.attackedPlayers.add(o.getId());
				c.setSkullTimer(Config.SKULL_TIMER);
				c.setSkull(true);
			}
		}
	}

	public static void attackPlayer(final Client c, final int i) {

		if (!(i > 0)) {
			c.sendMessage("player does not exist.");
			return;
		}
		// c.sendMess("Delay:"+(System.currentTimeMillis()-c.foodDelay));
		// c.foodDelay = System.currentTimeMillis();

		if (c.hidePlayer && !Config.isOwner(c)) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		if (c.isStunned() || c.isDead() || c.getSkills().getLifepoints() <= 0) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		if (c.teleTimer > 0) { // pause player attack till tele is done
			return;
		}
		if (c.performingAction) {
			return;
		}
		if (i >= PlayerHandler.players.length) {
			c.sendMessage("player does not exist.");
			return;
		}
		if (PlayerHandler.players[i] == null) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		final Client o = (Client) PlayerHandler.players[i];

		if (o.properLogout || o.isSaveInProgressForDC()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (o.teleTimer > 0 || o.isDead()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.inMarket() && c.withinDistance(o)) {
			if (!c.getCurrentLocation().isWithinDistance(o.getCurrentLocation(), 1))
				return;
			PlayerOwnedShops.openPlayerShop(c, o);
			c.getCombat().resetPlayerAttack();
			return;
		}

		switch (c.playerEquipment[PlayerConstants.playerWeapon]) {
		case 4566: // rubber chicken
			c.startAnimation(AnimationsData.CHICKEN_WHACK);
			c.faceLocation(o.getCurrentLocation());
			c.getCombat().resetPlayerAttack();
			return;
		case 14728: // carrot stick
			if (c.wildLevel < 1) {
				if (!PathFinder.reachedEntity(1, o.getX(), o.getY(), c.getX(), c.getY(), 1, 1, 0, RegionClip.getClipping(c.getX(), c.getY(), c.getZ(), c.getDynamicRegionClip()))) {
					return;
				}
				SpecialAttack.EASTER_CARROT.activateDummy(c, o);
				c.getCombat().resetPlayerAttack();
				return;
			}
			break;

		case 10501:// snowballs
			c.getPA().throwSnowballOnPlayer(o);
			return;
			
		case 24145: // eggsterminator
			//TODO: add gfx stuff
			if (c.wildLevel < 1) {
				RangeExtras.shootEggsterminator(c, o);
				return;
			}
			break;

		default:
			break;
		}

		if (c.inFunPk() && !o.inFunPk()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.inClanWarsFunPk()
				&& !o.inClanWarsFunPk()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		
		if (c.inLMSArenaCoords() && !o.inLMSArenaCoords()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.inClanWars() && !o.inClanWars()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (c.playerEquipment[PlayerConstants.playerWeapon] == 12926 && c.getBlowpipeAmmo() < 1) {
			c.sendMessage("Your blowpipe has ran out of ammo.");
			c.getCombat().resetPlayerAttack();
			return;
		}

		if (c.playerEquipment[PlayerConstants.playerWeapon] > 0 && ItemConstants
				.getItemName(c.playerEquipment[PlayerConstants.playerWeapon])
				.contains("(broken)")) {
			c.sendMessage("Your weapon is broken.");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (o.isDead()) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		if (c.respawnTimer > 0
				|| o.respawnTimer > 0) {
			c.getCombat().resetPlayerAttack();
			return;
		}

		if (!c.getCombat().checkReqs(o)) {
			c.getCombat().resetPlayerAttack();
			return;
		}
		if (!PlayerConstants.goodDistance(o.getX(),
				o.getY(), c.getX(), c.getY(), 25)) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		if (o.respawnTimer > 0) {
			o.playerIndex = 0;
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}

		if (o.heightLevel != c.heightLevel) {
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}


		/*if (c.wildLevel > 0 && c.wildLevel <= 10 && c.safeTimer < 1) {
			int myBonuses = c.getItems().getTotalBonuses();
			int otherBonus = o.getItems().getTotalBonuses();
			if ((myBonuses < 200 && otherBonus > 200) || (otherBonus < 200 && myBonuses > 200)) {
				c.sendMessage(
						"You can only attack a player with equal/less value than your own.");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				return;
			}
		}*/

		if (o.wildLevel > 0 && o.immuneToPK && !o.inMulti()) {
			c.sendMessage("This player is currently immune to PJers.");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return;
		}
		
		if (c.duelRule[DuelRules.FUN_WEAPONS]) {
			boolean canUseWeapon = false;
			for (int funWeapon : DBConfig.FUN_WEAPONS) {
				if (c.playerEquipment[PlayerConstants.playerWeapon] == funWeapon) {
					canUseWeapon = true;
				}
			}
			if (!canUseWeapon) {
				c.sendMessage(
						"You can only use fun weapons in this duel!");
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				return;
			}
		}
		
		if (c.absX == o.absX
				&& c.absY == o.absY) {
			if (c.getFreezeTimer() > 0) {
				c.getCombat().resetPlayerAttack();
				c.getMovement().resetWalkingQueue();
				return;
			}
			c.followId = i;
			return;
		}
		
//		if (c.usingSpecial && c.playerEquipment[PlayerConstants.playerWeapon] == 4153) {
//			c.getCombat().handleGmaulPlayer();
//			return;
//		}
		
		c.recentPlayerHitIndex = i;
		
		c.followId = i;
		c.followId2 = 0;

		if (c.getAttackTimer() <= 0) {
			if (c.blockedShot) {
				return;
			}

			if (!c.activateLeechSap && c.curses().applyLeeches(o)) {
				c.activateLeechSap = true;
				c.setAttackTimer(1);
				return;
			}
			if (c.activateLeechSap) {
				c.activateLeechSap = false;
			}

			c.face(o);

			if (c.toggledDfs) {
				c.getCombat().handleDfs(c, o);
				if (c.toggledDfs) {
					c.toggledDfs = false;
					return;
				}
			}

			c.followId = c.playerIndex;

			if (c.oldPlayerIndex != i) {
				c.curses().resetCurseBoosts();
			}

			c.followId = c.playerIndex;

			if (AttackMob.initHit(c, o)) {
				c.startAttackedPlayerTimer(o.getId());
				c.startCurseBoostResetTimer();
				c.oldPlayerIndex = o.getId(); // used for resetting leeches
				o.putInCombatWithPlayer(c.getId());
				

				if (o.autoRet == 1 && !o.walkingToItem && o.clickObjectType <= 0) {
					o.playerIndex = c.getId();
				}
				
				if (c.inWild() && o.inWild()) {
					o.safeTimer = 16;
					c.safeTimer = 16;
				}

				if (c.wildLevel > 0 && c.immuneToPK) {
					c.immuneToPK = false;
					c.sendMessage("You are no longer Safe from PJers.");
				}
				
				if (c.getTransformId() > -1 && !Config.isOwner(c)) {
					c.transform(-1);
					c.getMovement().resetWalkingQueue();
					c.getPA().resetMovementAnimation();
				}
				
			}

		}
	}

}
