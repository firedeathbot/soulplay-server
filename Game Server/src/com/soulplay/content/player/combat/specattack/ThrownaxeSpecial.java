package com.soulplay.content.player.combat.specattack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class ThrownaxeSpecial {
	
	private static final int DISTANCE = 5;

	public static void activateSpecial(BattleState state) {
		Client c = state.getAttacker();
		
		Mob def = state.getDefender();
		
		int targets = 1;
		
//		if (c.specAmount < 2.0) {
//			return;
//		}
		
		final List<BattleState> list = new ArrayList<>(5);
		
		final Location lastLocation = def.getCurrentLocation().copyNew();
		
		if (c.inMulti() && state.getDefender().inMulti()) {
				
				for (Mob others : (state.getDefender().isNpc() ? Server.getRegionManager().getLocalNpcs(state.getDefender()) : Server.getRegionManager().getLocalPlayers(state.getDefender()))) {
					
					if (AttackMob.skipMultiEntity(state, others, DISTANCE))
						continue;
					
					if (others.getId() == def.getId()) continue;
					
					if (others.inMulti()) {
						targets++;
//						c.specAmount -= 1.0;
						
						BattleState state2 = state.copy(c, others);
						
						state2.setCurrentHit(Misc.newRandom(c.getCombat().rangeMaxHit(others, 1.0)));
						
						list.add(state2);

//						if (c.specAmount < 1.0)
//							break;
						if (targets == 5)
							break;
					}
					
				}
				
				if (!list.isEmpty()) {
					Collections.shuffle(list);
					final int totalSize = list.size();
					CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
						int index = 0;
						int timer = 0;
						
						@Override
						public void stop() {
						}
						
						@Override
						public void execute(CycleEventContainer container) {
							if (index == totalSize || timer < 0) {
								container.stop();
								return;
							}
							
							if (timer == 0) {
								try {
									BattleState state = list.get(index);
									index++;

									Projectile proj = new Projectile(258, lastLocation, state.getDefender().getCurrentLocation());

									int dist = Misc.distanceToPoint(state.getDefender().getCurrentLocation().getX(), state.getDefender().getCurrentLocation().getY(), lastLocation.getX(), lastLocation.getY());

									int hitDelay = (int) (5 + (5*dist));

									proj.setEndDelay(hitDelay);
									proj.setStartDelay(10);
									proj.setEndHeight(25);
									proj.setStartHeight(25);
									proj.setSlope(0);

									GlobalPackets.createProjectile(proj);
									
									timer = Math.max(1, hitDelay/30);

									state.setHitDelay(timer);
									AttackMob.applyDamage(state);
									
									AttackMob.putIntoCombat(state.getAttacker(), state.getDefender());
									
									lastLocation.copyLocation(state.getDefender().getCurrentLocation());
								} catch (Exception e) {
									//put into try catch just in case.
									e.printStackTrace();
								}
							
							}
							
							timer--;
						}
					}, 1);
				}
				
		}
	}
	
}
