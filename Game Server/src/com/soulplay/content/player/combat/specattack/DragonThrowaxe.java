package com.soulplay.content.player.combat.specattack;

import com.soulplay.Server;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;

public class DragonThrowaxe {

	public static void queueSpec(Client c) {
		if (c.usingSpecial && c.instaSpecActivateTick != Server.getTotalTicks()) {
			c.usingSpecial = false;
			c.getItems().updateSpecialBar();
			return;
		}
		c.usingSpecial = true;
		c.getItems().updateSpecialBar();
		final int correctWeapon = c.playerEquipment[PlayerConstants.playerWeapon];
		c.instaSpecActivateTick = Server.getTotalTicks();
		c.addSpecQueue(c, new CycleEvent() {
			int error = 0;
			@Override
			public void stop() {
				c.getItems().updateSpecialBar();
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (error > 1) {
					container.stop();
				}
				if (c.playerEquipment[PlayerConstants.playerWeapon] != correctWeapon)
					error++;
				
				if ((c.playerIndex > 0 || c.npcIndex > 0) && withinDistance(c)) {
					if (c.playerEquipment[PlayerConstants.playerWeapon] == correctWeapon) {
						c.setAttackTimer(0);
						container.stop();
					}
				}
			}
		});
	}
	
	private static boolean withinDistance(Player p) {
		if (p.playerIndex > 0 && PlayerHandler.players[p.playerIndex] != null) {
			return AttackMob.isWithinDistanceToHit(p, PlayerHandler.players[p.playerIndex], null);
		}
		if (p.npcIndex > 0 && NPCHandler.npcs[p.npcIndex] != null) {
			return AttackMob.isWithinDistanceToHit(p, NPCHandler.npcs[p.npcIndex], null);
		}
		return false;
	}
	
}
