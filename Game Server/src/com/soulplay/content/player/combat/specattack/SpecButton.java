package com.soulplay.content.player.combat.specattack;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.definition.SpecialsInterfaceInfo;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;

public class SpecButton {

	public static void clickButton(Client c) {
		int weapon = c.playerEquipment[PlayerConstants.playerWeapon];
		ItemDefinition itemDef = ItemDefinition.forId(c.playerEquipment[PlayerConstants.playerWeapon]);
		if (itemDef == null) {
			return;
		}

		SpecialsInterfaceInfo spec = itemDef.getSpecInfo();
		if (spec == null) {
			return;
		}

		SpecialAttack specAtt = SpecialAttack.get(weapon);
		if (specAtt == null) {
			return;
		}

		BattleState state = new BattleState(c, null);
		c.specBarId = spec.getSpecBarId();
		
		switch (specAtt) {
		case GRANITE_MAUL: // instant activation on gmaul
		case GRANITE_MAUL_CLAMP:
			if (specAtt.canActivate(state)) {
				c.getCombat().handleGmaulPlayer(specAtt);
			} else {
				unableToActivate(c);
			}
			break;
		case DRAGON_THROWNAXE:
			if (specAtt.canActivate(state)) {
				DragonThrowaxe.queueSpec(c);
			} else {
				unableToActivate(c);
			}
			break;
		case DRAGON_HARPOON:
		case DRAGON_BATTLEAXE:
		case STAFF_OF_LIGHT:
		case STAFF_OF_THE_DEAD:
			if (specAtt.canActivate(state))
				specAtt.init(state);
			else {
				unableToActivate(c);
			}
			break;
			
			default:
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
				break;
		}
		
		if (c.queueGmaulSpec) {
			c.usingSpecial = !c.usingSpecial;
			if (!c.usingSpecial)
				c.queueGmaulSpec = false;
			c.getItems().updateSpecialBar();
		}
	}
	
	public static void unableToActivate(Client c) {
		c.sendMessage("You don't have the required special energy to use this attack.");
		c.usingSpecial = false;
		c.getItems().updateSpecialBar();
	}

	public static void dragonHarpoon(Client c) {
		c.forceChat("Here fishy fishies!");

		c.getSkills().setLevel(Skills.FISHING, c.getSkills().getDynamicLevels()[Skills.FISHING] + 3);
	}

	public static void dragonBattleAxeShit(Client c) {
		c.forceChat("Raarrrrrgggggghhhhhhh!");

		int attack = (int) (c.getSkills().getLevel(Skills.ATTACK) * 0.10);
		c.getSkills().drainLevel(Skills.ATTACK, 0.1, 0.09);

		int def = (int) (c.getSkills().getLevel(Skills.DEFENSE) * 0.10);
		c.getSkills().drainLevel(Skills.DEFENSE, 0.1, 0.09);

		int rang = (int) (c.getSkills().getLevel(Skills.RANGED) * 0.10);
		c.getSkills().drainLevel(Skills.RANGED, 0.1, 0.09);

		int magic = (int) (c.getSkills().getLevel(Skills.MAGIC) * 0.10);
		c.getSkills().drainLevel(Skills.MAGIC, 0.1, 0.09);

		c.getSkills().setLevel(Skills.STRENGTH,
				c.getSkills().getStaticLevel(Skills.STRENGTH) + (10 + ((attack + def + rang + magic) / 4)));
	}
}
