package com.soulplay.content.player.combat;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public final class DamageMap {

	public final class TotalDamage {

		/**
		 * The total damage dealt.
		 */
		private int damage = 0;

		/**
		 * The last time the damage was increased.
		 */
		private long lastHitTime = System.currentTimeMillis();

		/**
		 * Gets the total damage dealt.
		 *
		 * @return The total damage dealt.
		 */
		public int getDamage() {
			return damage;
		}

		/**
		 * Gets the last hit time.
		 *
		 * @return The last hit time.
		 */
		public long getLastHitTime() {
			return lastHitTime;
		}

		/**
		 * Increaes the damage by the specified amount.
		 *
		 * @param amount
		 *            How much to increase the damage by.
		 */
		public void increment(int amount) {
			this.damage += amount;
			this.lastHitTime = System.currentTimeMillis();
		}
		
		public void incrementForLms(int amount, boolean kill) {
			this.damage += amount;
			if (!kill && this.damage > 99)
				this.damage = 99;
		}

	}

	/**
	 * The time after which the total hit expires (3 minutes).
	 */
	private static final long HIT_EXPIRE_TIME = 60000;

	/**
	 * The total damage map.
	 */
	private Map<Integer, TotalDamage> totalDamages = new ConcurrentHashMap<>();

	/**
	 * @return the totalDamages
	 */
	public Map<Integer, TotalDamage> getTotalDamages() {
		return totalDamages;
	}

	/**
	 * Gets the highest damage dealer to this entity.
	 *
	 * @return The highest damage dealer to this entity.
	 */
	public int highestDamage() {
		// removeInvalidEntries();
		int highestDealt = 0;
		int killer = 0;
		long now = System.currentTimeMillis();
		for (Entry<Integer, TotalDamage> mob : totalDamages.entrySet()) {
			long delta = now
					- mob.getValue().getLastHitTime();
			if (delta < HIT_EXPIRE_TIME
					&& mob.getValue().getDamage() > highestDealt) {
				highestDealt = mob.getValue().getDamage();
				killer = mob.getKey();
			}
		}
		
		//if killer is not found in LMS, then get killer with extended timer.
//		if (inLMS && killer == 0)
//			getLMSKillerExtended();
		
		return killer;
	}
	
	/**
	 * Removes entities that have been destroyed from the map, or entries where
	 * the player has not hit for 3 minutes.
	 */
	// public void removeInvalidEntries() {
	// Set<Long> mobs = new HashSet<Long>();
	// mobs.addAll(totalDamages.keySet());
	// for (long e : mobs) {
	// TotalDamage d = totalDamages.get(e);
	// long delta = System.currentTimeMillis() - d.getLastHitTime();
	// if (delta >= HIT_EXPIRE_TIME) {
	// totalDamages.remove(e);
	// }
	// }
	// }

	/**
	 * Increments the total damage dealt by an attacker.
	 *
	 * @param attacker
	 * @param amount
	 */
	public void incrementTotalDamage(int attackerMID, int amount) {
		if (amount < 1) {
			return;
		}

		TotalDamage totalDamage = totalDamages.get(attackerMID);
		if (totalDamage == null) {
			totalDamage = new TotalDamage();
			totalDamages.put(attackerMID, totalDamage);
		}

		totalDamage.increment(amount);
		if (p.isInLmsArena()) {
			Player other = PlayerHandler.getPlayerByMID(attackerMID);
			if (other != null)
				other.lmsDamageMap.incrementLMSDamageDealt(p.mySQLIndex, amount, false);
		}
	}
	
	public void incrementLMSDamageDealt(int defender, int amount, boolean kill) {
		if (amount < 1) {
			return;
		}
		if (amount > 99)
			amount = 99;
		TotalDamage totalDamage = totalDamages.get(defender);
		if (totalDamage == null) {
			totalDamage = new TotalDamage();
			totalDamages.put(defender, totalDamage);
		} else {
			totalDamage.incrementForLms(amount, kill);
		}
	}
	
	public boolean removeFromDamageMap(int attackerMID) {
		return totalDamages.remove(attackerMID) != null;
	}

	/**
	 * Resets the map (e.g. if the entity dies).
	 */
	public void reset() {
		totalDamages.clear();
	}
	
	public final Player p;
	
	public DamageMap(Player p) {
		this.p = p;
	}

}
