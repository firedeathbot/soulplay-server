package com.soulplay.content.player.combat.prayer;

import com.soulplay.game.model.player.Player;

public enum Curses {

	PROTECT_ITEM("Protect Item", 50, 2, 83, -1),
	SAP_WARRIOR("Sap Warrior", 50, 6, 84, -1),
	SAP_RANGER("Sap Ranger", 52, 6, 85, -1),
	SAP_MAGE("Sap Mage", 54, 6, 101, -1),
	SAP_SPIRIT("Sap Spirit", 56, 6, 102, -1),
	BERSERKER("Berserker", 59, 3, 86, -1),
	DEFLECT_SUMMONING("Deflect Summoning", 62, 12, 87, 12),
	DEFLECT_MAGIC("Deflect Magic", 65, 12, 88, 10),
	DEFLECT_MISSILES("Deflect Missiles", 68, 12, 89, 11),
	DEFLECT_MELEE("Deflect Melee", 71, 12, 90, 9),
	LEECH_ATTACK("Leech Attack", 74, 12, 91, -1),
	LEECH_RANGED("Leech Ranged", 76, 12, 103, -1),
	LEECH_MAGIC("Leech Magic", 78, 12, 104, -1),
	LEECH_DEFENCE("Leech Defence", 80, 12, 92, -1),
	LEECH_STRENGTH("Leech Strength", 82, 12, 93, -1),
	LEECH_ENERGY("Leech Energy", 84, 12, 94, -1),
	LEECH_SPECIAL_ATTACK("Leech Special Attack", 86, 12, 95, -1),
	WRATH("Wrath", 89, 12, 96, 16),
	SOUL_SPLIT("Soul Split", 92, 24, 97, 17),
	TURMOIL("Turmoil", 95, 24, 105, -1),
	ANGUISH("Anguish", 95, 24, 106, -1),
	TORMENT("Torment", 95, 24, 107, -1);

	public static final Curses[] values = values();
	private final String prayerName;
	private final int levelRequirement, configCode, headIcon, drainRate;

	private Curses(String prayerName, int levelRequirement, int drainRate, int configCode, int headIcon) {
		this.prayerName = prayerName;
		this.levelRequirement = levelRequirement;
		this.drainRate = drainRate;
		this.configCode = configCode;
		this.headIcon = headIcon;
	}

	public int getConfigCode() {
		return configCode;
	}

	public double getDrainRate() {
		return drainRate;
	}

	public int getHeadIcon() {
		return headIcon;
	}

	public int getLevelRequirement() {
		return levelRequirement;
	}

	public String getPrayerName() {
		return prayerName;
	}

	public boolean isActive(Player player) {
		return player.curseActive[ordinal()];
	}

}