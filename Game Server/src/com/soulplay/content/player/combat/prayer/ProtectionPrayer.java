package com.soulplay.content.player.combat.prayer;

public enum ProtectionPrayer {
	PROTECT_MELEE,
	PROTECT_RANGE,
	PROTECT_MAGIC;
}
