package com.soulplay.content.player.combat.prayer;

import java.util.Arrays;

import com.soulplay.Config;
import com.soulplay.content.instances.BossInstanceManager;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.ddmtournament.Settings;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareNPC;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

/**
 * Handles prayers drain and switching
 *
 * @author 2012
 * @author Organic
 */

public class CombatPrayer {

	public static final int[] defPray = {Prayers.THICK_SKIN.ordinal(),
			Prayers.ROCK_SKIN.ordinal(), Prayers.STEEL_SKIN.ordinal(),
			Prayers.CHIVALRY.ordinal(), Prayers.PIETY.ordinal(),
			Prayers.RIGOUR.ordinal(), Prayers.AUGURY.ordinal()};

	public static final int[] strPray = {Prayers.BURST_OF_STRENGTH.ordinal(),
			Prayers.SUPERHUMAN_STRENGTH.ordinal(),
			Prayers.ULTIMATE_STRENGTH.ordinal(), Prayers.CHIVALRY.ordinal(),
			Prayers.PIETY.ordinal()};

	public static final int[] atkPray = {Prayers.CLARITY_OF_THOUGHT.ordinal(),
			Prayers.IMPROVED_REFLEXES.ordinal(),
			Prayers.INCREDIBLE_REFLEXES.ordinal(), Prayers.CHIVALRY.ordinal(),
			Prayers.PIETY.ordinal()};

	public static final int[] rangePray = {Prayers.SHARP_EYE.ordinal(),
			Prayers.HAWK_EYE.ordinal(), Prayers.EAGLE_EYE.ordinal(),
			Prayers.RIGOUR.ordinal()};

	public static final int[] magePray = {Prayers.MYSTIC_WILL.ordinal(),
			Prayers.MYSTIC_MIGHT.ordinal(), Prayers.MYSTIC_LORE.ordinal(),
			Prayers.AUGURY.ordinal()};

	public static final int[] headIcons = {
			Prayers.PROTECT_FROM_SUMMONING.ordinal(),
			Prayers.PROTECT_FROM_MAGIC.ordinal(),
			Prayers.PROTECT_FROM_MISSILES.ordinal(),
			Prayers.PROTECT_FROM_MELEE.ordinal(), Prayers.RETRIBUTION.ordinal(),
			Prayers.REDEMPTION.ordinal(), Prayers.SMITE.ordinal()};

	public static void activatePrayer(Client c, int i) {
		if (c.isDead() || c.getSkills().getLifepoints() <= 0 || c.isLockActions()) {
			if (!c.prayerActive[i]) {
				c.getPacketSender().setConfig(Prayers.values[i].getConfigCode(), 0);
			} else {
				c.getPacketSender().setConfig(Prayers.values[i].getConfigCode(), 1);
			}
			return;
		}
		// c.sendMessage("i "+i);
		if (c.playerPrayerBook == PrayerBook.CURSES) {
			return;
		}

		if (c.attr().has("nightmare_curse")) {
			int newId = NightmareNPC.cursePrayerType(Prayers.values[i]).ordinal();
			if (newId != i) {
				i = newId;
			}
		}

		if (!Settings.checkPrayers(c, i, false))
			return;
		
		if (i == Prayers.PROTECT_ITEM.ordinal() && c.isRedSkulled()) {
			c.getPacketSender().setConfig(Prayers.values[i].getConfigCode(), 0);
			c.sendMessage("You cannot toggle Protect Item Prayer with Red Skull on.");
			return;
		}
		
		if (i == Prayers.AUGURY.ordinal() && !c.isAuguryUnlocked()) {
				c.sendMessage("You have not learned Augury yet.");
				c.getPacketSender().setConfig(Prayers.AUGURY.getConfigCode(), 0);
				return;
		}
		
		if (i == Prayers.RIGOUR.ordinal() && !c.isRigourUnlocked()) {
			c.sendMessage("You have not learned Rigour yet.");
			c.getPacketSender().setConfig(Prayers.RIGOUR.getConfigCode(), 0);
			return;
		}

		if (!c.getBarbarianAssault().canUsePrayer()) {
			c.sendMessage("You can't use prayer here.");
			resetPrayers(c);
			return;
		}
		
		if (RSConstants.inRFDArena(c.getX(), c.getY())) {
			c.getPacketSender().setConfig(Prayers.values[i].getConfigCode(), 0);
			c.sendMessage("You cannot use prayer here.");
			return;
		}

		if (c.duelRule[DuelRules.NO_PRAYER]) {
			// for (int p = 0; p < CombatPrayer.PRAYER.length; p++) { // reset
			// prayer glows
			// c.prayerActive[p] = false;
			// c.getPacketSender().setConfig(Prayers.values[p], 0);
			// }
			c.getCombat().resetPrayers();
			c.sendMessage("Prayer has been disabled in this duel!");
			return;
		}

		if (c.clanWarRule[CWRules.NO_PRAYER]) {
			c.sendMessage("You are not allowed to use prayer during this war!");
			resetPrayers(c);
			return;
		}

		if (c.getSkills().getPrayerPoints() > 0 || !Config.PRAYER_POINTS_REQUIRED) {
			if (c.getSkills().getStaticLevel(Skills.PRAYER) >= Prayers.values[i]
					.getLevelRequirement() || !Config.PRAYER_LEVEL_REQUIRED) {

				if (i == Prayers.THICK_SKIN.ordinal()
						|| i == Prayers.ROCK_SKIN.ordinal()
						|| i == Prayers.STEEL_SKIN.ordinal()) {
					if (!c.prayerActive[i]) {
						for (int j = 0; j < defPray.length; j++) {
							if (defPray[j] != i) {
								c.prayerActive[defPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[defPray[j]]
												.getConfigCode(), 0);
							}
						}
					}
				}

				if (i == Prayers.BURST_OF_STRENGTH.ordinal()
						|| i == Prayers.SUPERHUMAN_STRENGTH.ordinal()
						|| i == Prayers.ULTIMATE_STRENGTH.ordinal()) {
					if (!c.prayerActive[i]) {
						for (int j = 0; j < strPray.length; j++) {
							if (strPray[j] != i) {
								c.prayerActive[strPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[strPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < rangePray.length; j++) {
							if (rangePray[j] != i) {
								c.prayerActive[rangePray[j]] = false;
								c.getPacketSender().setConfig(
										Prayers.values[rangePray[j]]
												.getConfigCode(),
										0);
							}
						}
						for (int j = 0; j < magePray.length; j++) {
							if (magePray[j] != i) {
								c.prayerActive[magePray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[magePray[j]]
												.getConfigCode(), 0);
							}
						}
					}
				}

				if (i == Prayers.CLARITY_OF_THOUGHT.ordinal()
						|| i == Prayers.IMPROVED_REFLEXES.ordinal()
						|| i == Prayers.INCREDIBLE_REFLEXES.ordinal()) {
					if (!c.prayerActive[i]) {
						for (int j = 0; j < atkPray.length; j++) {
							if (atkPray[j] != i) {
								c.prayerActive[atkPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[atkPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < rangePray.length; j++) {
							if (rangePray[j] != i) {
								c.prayerActive[rangePray[j]] = false;
								c.getPacketSender().setConfig(
										Prayers.values[rangePray[j]]
												.getConfigCode(),
										0);
							}
						}
						for (int j = 0; j < magePray.length; j++) {
							if (magePray[j] != i) {
								c.prayerActive[magePray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[magePray[j]]
												.getConfigCode(), 0);
							}
						}
					}
				}

				if (i == Prayers.SHARP_EYE.ordinal()
						|| i == Prayers.HAWK_EYE.ordinal()
						|| i == Prayers.EAGLE_EYE.ordinal()) {
					if (!c.prayerActive[i]) {
						for (int j = 0; j < atkPray.length; j++) {
							if (atkPray[j] != i) {
								c.prayerActive[atkPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[atkPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < strPray.length; j++) {
							if (strPray[j] != i) {
								c.prayerActive[strPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[strPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < rangePray.length; j++) {
							if (rangePray[j] != i) {
								c.prayerActive[rangePray[j]] = false;
								c.getPacketSender().setConfig(
										Prayers.values[rangePray[j]]
												.getConfigCode(),
										0);
							}
						}
						for (int j = 0; j < magePray.length; j++) {
							if (magePray[j] != i) {
								c.prayerActive[magePray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[magePray[j]]
												.getConfigCode(), 0);
							}
						}
					}
				}

				if (i == Prayers.MYSTIC_WILL.ordinal()
						|| i == Prayers.MYSTIC_LORE.ordinal()
						|| i == Prayers.MYSTIC_MIGHT.ordinal()) {
					if (!c.prayerActive[i]) {
						for (int j = 0; j < atkPray.length; j++) {
							if (atkPray[j] != i) {
								c.prayerActive[atkPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[atkPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < strPray.length; j++) {
							if (strPray[j] != i) {
								c.prayerActive[strPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[strPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < rangePray.length; j++) {
							if (rangePray[j] != i) {
								c.prayerActive[rangePray[j]] = false;
								c.getPacketSender().setConfig(
										Prayers.values[rangePray[j]]
												.getConfigCode(),
										0);
							}
						}
						for (int j = 0; j < magePray.length; j++) {
							if (magePray[j] != i) {
								c.prayerActive[magePray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[magePray[j]]
												.getConfigCode(), 0);
							}
						}
					}
				}

				if (i == Prayers.PROTECT_FROM_MAGIC.ordinal()
						|| i == Prayers.PROTECT_FROM_MISSILES.ordinal()
						|| i == Prayers.PROTECT_FROM_MELEE.ordinal()) {
					if (!c.getProtectionPrayerDisabledTimer().complete()) {
						c.sendMessage(
								"You have been injured and can't use this prayer!");
						c.getPacketSender().setConfig(
								Prayers.values[Prayers.PROTECT_FROM_MAGIC
										.ordinal()].getConfigCode(),
								0);
						c.getPacketSender().setConfig(
								Prayers.values[Prayers.PROTECT_FROM_MISSILES
										.ordinal()].getConfigCode(),
								0);
						c.getPacketSender().setConfig(
								Prayers.values[Prayers.PROTECT_FROM_MELEE
										.ordinal()].getConfigCode(),
								0);
						return;
					}
				}

				if (Prayers.values[i].getHeadIcon() != -1) { // an overhead
					// if (i == Prayers.RETRIBUTION.ordinal() || i ==
					// Prayers.REDEMPTION.ordinal() || i ==
					// Prayers.SMITE.ordinal() || i ==
					// Prayers.PROTECT_FROM_MELEE.ordinal()) {
					for (int j = 0; j < headIcons.length; j++) {
						if (i == Prayers.PROTECT_FROM_MELEE.ordinal()
								&& headIcons[j] == Prayers.PROTECT_FROM_SUMMONING
										.ordinal()
								&& c.prayerActive[Prayers.PROTECT_FROM_SUMMONING
										.ordinal()]) {
							continue;
						} else if (i == Prayers.PROTECT_FROM_MAGIC.ordinal()
								&& ((c.duoPrayerEnabled
										&& headIcons[j] == Prayers.PROTECT_FROM_MISSILES
												.ordinal())
										|| headIcons[j] == Prayers.PROTECT_FROM_SUMMONING
												.ordinal())) {
							continue;
						} else if (i == Prayers.PROTECT_FROM_MISSILES.ordinal()
								&& ((c.duoPrayerEnabled
										&& headIcons[j] == Prayers.PROTECT_FROM_MAGIC
												.ordinal())
										|| headIcons[j] == Prayers.PROTECT_FROM_SUMMONING
												.ordinal())) {
							continue;
						} else if (i == Prayers.PROTECT_FROM_SUMMONING.ordinal()
								&& (headIcons[j] == Prayers.PROTECT_FROM_MISSILES
										.ordinal()
										|| headIcons[j] == Prayers.PROTECT_FROM_MAGIC
												.ordinal())) {
							continue;
						} else {
							if (headIcons[j] != i) {
								c.prayerActive[headIcons[j]] = false;
								c.getPacketSender().setConfig(
										Prayers.values[headIcons[j]]
												.getConfigCode(),
										0);
							}
						}
					}
					// } else { // the complicating shit here
					// if (i == Prayers.PROTECT_FROM_MELEE.ordinal() &&
					// !c.prayerActive[Prayers.PROTECT_FROM_SUMMONING.ordinal()])
					// {
					// c.prayerActive[i] = true;
					// }
					//
					// }
				}

				if (i == Prayers.CHIVALRY.ordinal()
						|| i == Prayers.PIETY.ordinal()
						|| i == Prayers.AUGURY.ordinal()
						|| i == Prayers.RIGOUR.ordinal()) {
					if (!c.prayerActive[i]) {
						for (int j = 0; j < atkPray.length; j++) {
							if (atkPray[j] != i) {
								c.prayerActive[atkPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[atkPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < strPray.length; j++) {
							if (strPray[j] != i) {
								c.prayerActive[strPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[strPray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < rangePray.length; j++) {
							if (rangePray[j] != i) {
								c.prayerActive[rangePray[j]] = false;
								c.getPacketSender().setConfig(
										Prayers.values[rangePray[j]]
												.getConfigCode(),
										0);
							}
						}
						for (int j = 0; j < magePray.length; j++) {
							if (magePray[j] != i) {
								c.prayerActive[magePray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[magePray[j]]
												.getConfigCode(), 0);
							}
						}
						for (int j = 0; j < defPray.length; j++) {
							if (defPray[j] != i) {
								c.prayerActive[defPray[j]] = false;
								c.getPacketSender()
										.setConfig(Prayers.values[defPray[j]]
												.getConfigCode(), 0);
							}
						}
					}
				}

				if (Prayers.values[i].getHeadIcon() == -1) { // no headicons
					if (!c.prayerActive[i]) { // activate
						if ((i == Prayers.AUGURY.ordinal()
								|| i == Prayers.RIGOUR.ordinal()
								|| i == Prayers.PIETY.ordinal())
								&& c.getSkills().getStaticLevel(
										Skills.DEFENSE) < 70) {
							c.prayerActive[i] = false;
							c.getPacketSender().setConfig(
									Prayers.values[i].getConfigCode(), 0);
							c.sendMessage(
									"You need defence level of 70 to use this prayer.");

						} else if (i == Prayers.CHIVALRY.ordinal()
								&& c.getSkills().getStaticLevel(
										Skills.DEFENSE) < 65) {
							c.prayerActive[i] = false;
							c.getPacketSender().setConfig(
									Prayers.values[i].getConfigCode(), 0);
							c.sendMessage(
									"You need defence level of 65 to use this prayer.");
						} else {
							c.prayerActive[i] = true;
							c.getPacketSender().setConfig(
									Prayers.values[i].getConfigCode(), 1);
                            if (Prayers.values[i].getSound() != -1) {
                                c.getPacketSender().playSound(Prayers.values[i].getSound());
                            }
						}
					} else {
						c.prayerActive[i] = false;
						c.getPacketSender().setConfig(
								Prayers.values[i].getConfigCode(), 0);
						c.getPacketSender().playSound(Sounds.PRAYER_DEACTIVATE);
					}
				} else { // headicons
					if (!c.prayerActive[i]) { // activate
						c.prayerActive[i] = true;

						int configCode;
						if (c.attr().has("nightmare_curse")) {
							configCode = NightmareNPC.cursePrayerTypeReverse(Prayers.values[i]).getConfigCode();
						} else {
							configCode = Prayers.values[i].getConfigCode();
						}

						c.getPacketSender().setConfig(configCode, 1);
						
						if (c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
								&& c.prayerActive[Prayers.PROTECT_FROM_MISSILES
										.ordinal()]) {
							c.setHeadIcon(6);
						} else if (c.prayerActive[Prayers.PROTECT_FROM_MELEE
								.ordinal()]
								&& c.prayerActive[Prayers.PROTECT_FROM_SUMMONING
										.ordinal()]) {
							c.setHeadIcon(13);
						} else if (c.prayerActive[Prayers.PROTECT_FROM_MISSILES
								.ordinal()]
								&& c.prayerActive[Prayers.PROTECT_FROM_SUMMONING
										.ordinal()]) {
							c.setHeadIcon(14);
						} else if (c.prayerActive[Prayers.PROTECT_FROM_MAGIC
								.ordinal()]
								&& c.prayerActive[Prayers.PROTECT_FROM_SUMMONING
										.ordinal()]) {
							c.setHeadIcon(15);
						} else {
							c.setHeadIcon(Prayers.values[i].getHeadIcon());
						}

						if (Prayers.values[i].getSound() != -1) {
                            c.getPacketSender().playSound(Prayers.values[i].getSound());
                        }

					} else { // deactivate
						c.prayerActive[i] = false;
						c.getPacketSender().setConfig(
								Prayers.values[i].getConfigCode(), 0);

                        if (Prayers.values[i].getSound() != -1) {
                            c.getPacketSender().playSound(Sounds.PRAYER_DEACTIVATE);
                        }

						if (c.prayerActive[Prayers.PROTECT_FROM_SUMMONING
								.ordinal()]) {
							c.setHeadIcon(Prayers.PROTECT_FROM_SUMMONING
									.getHeadIcon());
						}
						if (!c.prayerActive[Prayers.PROTECT_FROM_MAGIC
								.ordinal()]
								&& !c.prayerActive[Prayers.PROTECT_FROM_MISSILES
										.ordinal()]
								&& !c.prayerActive[Prayers.PROTECT_FROM_MELEE
										.ordinal()]
								&& !c.prayerActive[Prayers.PROTECT_FROM_SUMMONING
										.ordinal()]) {
							c.setHeadIcon(-1);
						}
						if (c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
								&& !c.prayerActive[Prayers.PROTECT_FROM_MISSILES
										.ordinal()]) {
							c.setHeadIcon(Prayers.PROTECT_FROM_MAGIC
									.getHeadIcon());
						} else if (!c.prayerActive[Prayers.PROTECT_FROM_MAGIC
								.ordinal()]
								&& c.prayerActive[Prayers.PROTECT_FROM_MISSILES
										.ordinal()]) {
							c.setHeadIcon(Prayers.PROTECT_FROM_MISSILES
									.getHeadIcon());
						}

					}
				}
				
				boolean allInactive = true;
				for (int i2 = 0, l = c.prayerActive.length; i2 < l; i2++) {
					if (c.prayerActive[i2]) {
						allInactive = false;
						break;
					}
				}
				
				if (allInactive)
					c.prayerDrainCounter = 0;
				
			} else {
				c.getPacketSender()
						.setConfig(Prayers.values[i].getConfigCode(), 0);
				c.getPacketSender().sendString("You need a <col=08088A>Prayer</col> level of " + Prayers.values[i].getLevelRequirement() + " to use <col=08088A>" + Prayers.values[i].getPrayerName() + "</col>.", 357);
				c.getPacketSender().playSound(Sounds.PRAYER_FAIL);
				c.getPacketSender().sendFrame126("Click here to continue", 358);
				c.getPacketSender().sendChatInterface(356);
			}
		} else {
			c.getPacketSender().setConfig(Prayers.values[i].getConfigCode(),
					0);
			c.sendMessage("You have run out of prayer points!");
			resetPrayers(c);
		}

	}

	public static boolean deactivePrayer(Player c, Prayers prayer) {
		if (c.prayerActive[prayer.ordinal()]) {
			c.prayerActive[prayer.ordinal()] = false;
			c.getPacketSender().setConfig(prayer.getConfigCode(), 0);
			return true;
		}
		return false;
	}

	public static boolean handlePrayerButton(final Client c, int actionButton) {

		if (c.playerPrayerBook == PrayerBook.CURSES) {
			return false;
		}

		switch (actionButton) {
			/** Prayers **/
			case 21233: // thick skin
				c.getCombat().activatePrayer(Prayers.THICK_SKIN.ordinal());
				return true;
			case 21234: // burst of str
				c.getCombat()
						.activatePrayer(Prayers.BURST_OF_STRENGTH.ordinal());
				return true;
			case 21235: // clarity of thought
				c.getCombat()
						.activatePrayer(Prayers.CLARITY_OF_THOUGHT.ordinal());
				return true;
			case 70080: // sharp eye
				c.getCombat().activatePrayer(Prayers.SHARP_EYE.ordinal());
				return true;
			case 70082: // mystic will
				c.getCombat().activatePrayer(Prayers.MYSTIC_WILL.ordinal());
				return true;
			case 21236: // rockskin
				c.getCombat().activatePrayer(Prayers.ROCK_SKIN.ordinal());
				return true;
			case 21237: // super human
				c.getCombat()
						.activatePrayer(Prayers.SUPERHUMAN_STRENGTH.ordinal());
				return true;
			case 21238: // improved reflexes
				c.getCombat()
						.activatePrayer(Prayers.IMPROVED_REFLEXES.ordinal());
				return true;
			case 21239: // rapid renewal
				c.getCombat().activatePrayer(Prayers.RAPID_RENEWAL.ordinal());
				return true;
			case 21240: // rapid heal
				c.getCombat().activatePrayer(Prayers.RAPID_HEAL.ordinal());
				return true;
			case 21241: // protect Item
				if (c.getZones().isInDmmTourn()) {
					c.sendMessage("You cannot Active Protect Item here.");
					c.getPacketSender().setConfig(Prayers.PROTECT_ITEM.getConfigCode(), 0);
					return true;
				}
				c.getCombat().activatePrayer(Prayers.PROTECT_ITEM.ordinal());
				return true;
			case 70084: // hawk eye
				c.getCombat().activatePrayer(Prayers.HAWK_EYE.ordinal());
				return true;
			case 70086: // mystic lore
				c.getCombat().activatePrayer(Prayers.MYSTIC_LORE.ordinal());
				return true;
			case 21242: // steel skin
				c.getCombat().activatePrayer(Prayers.STEEL_SKIN.ordinal());
				return true;
			case 21243: // ultimate str
				c.getCombat()
						.activatePrayer(Prayers.ULTIMATE_STRENGTH.ordinal());
				return true;
			case 21244: // incredible reflex
				c.getCombat()
						.activatePrayer(Prayers.INCREDIBLE_REFLEXES.ordinal());
				return true;
			case 69236: // protect from summoning lvl 35 prayer
				c.getCombat().activatePrayer(
						Prayers.PROTECT_FROM_SUMMONING.ordinal());
				return true;
			case 21245: // protect from magic
				c.getCombat()
						.activatePrayer(Prayers.PROTECT_FROM_MAGIC.ordinal());
				return true;
			case 21246: // protect from range
				c.getCombat().activatePrayer(
						Prayers.PROTECT_FROM_MISSILES.ordinal());
				return true;
			case 21247: // protect from melee
				c.getCombat()
						.activatePrayer(Prayers.PROTECT_FROM_MELEE.ordinal());
				return true;
			case 70088: // eagle eye
				c.getCombat().activatePrayer(Prayers.EAGLE_EYE.ordinal());
				return true;
			case 70090: // 45 mystic might
				c.getCombat().activatePrayer(Prayers.MYSTIC_MIGHT.ordinal());
				return true;
			case 2171: // retribution
				c.getCombat().activatePrayer(Prayers.RETRIBUTION.ordinal());
				return true;
			case 2172: // redem
				c.getCombat().activatePrayer(Prayers.REDEMPTION.ordinal());
				return true;
			case 2173: // smite
				c.getCombat().activatePrayer(Prayers.SMITE.ordinal());
				return true;
			case 70092: // chiv
				c.getCombat().activatePrayer(Prayers.CHIVALRY.ordinal());
				return true;
			case 69242: // rapid renewal
				c.getCombat().activatePrayer(Prayers.RAPID_RENEWAL.ordinal());
				return true;
			case 70094: // piety
				c.getCombat().activatePrayer(Prayers.PIETY.ordinal());
				return true;
			case 69238: // Rigour
				c.getCombat().activatePrayer(Prayers.RIGOUR.ordinal());
				return true;
			case 69240: // Augury
				c.getCombat().activatePrayer(Prayers.AUGURY.ordinal());
				return true;
		}
		return false;
	}
	
	public static final int BASE_RESISTANCE = 60;

	public static void handlePrayerDrain(Client c) {
		if (c.isDead() || c.getHitPoints() <= 0) {
			return;
		}

		final boolean prayerWasOnLastTick = c.usingPrayer;
		c.usingPrayer = false;

		int prayerRate = 0;
		for (int i = 0, length = Prayers.values.length; i < length; i++) {
			if (!c.prayerActive[i]) {
				continue;
			}

			prayerRate += Prayers.values[i].getDrainRate();
			c.usingPrayer = true;
		}

		for (int i = 0, length = Curses.values.length; i < length; i++) {
			if (!c.curseActive[i]) {
				continue;
			}

			prayerRate += Curses.values[i].getDrainRate();
			c.usingPrayer = true;
		}

		int basicResistance = BASE_RESISTANCE;

		if (c.prayerRenew > 0)
			basicResistance += c.prayerRenew / 2;

		int playerBonus = Misc.multiplyOrMaxInt(c.getPlayerBonus()[EquipmentBonuses.PRAYER.getId()], 4);//original formula is bonus * 2
		int drainResistance = basicResistance + playerBonus;

		if (c.insideInstance() && c.getInstanceManager().getName().equalsIgnoreCase(BossInstanceManager.NAME) && ((BossInstanceManager) c.getInstanceManager()).isPrayerDrainReduction()) {
			drainResistance *= 2;
		}

		int prayerDrainPow = c.getSeasonalData().getPowerUpAmount(PowerUpData.PRAYER_DRAIN);
		if (prayerDrainPow > 0) {
			drainResistance += drainResistance * (prayerDrainPow * 10) / 100;
		}

		if (c.prayerDrainCounter < drainResistance) {
			if (prayerWasOnLastTick && !c.usingPrayer)
				c.prayerDrainCounter = 0;
		}

		if (prayerRate > 0) {
			c.prayerDrainCounter += prayerRate;
		}

		while (c.prayerDrainCounter > drainResistance) {
			if (c.getSkills().getPrayerPoints() == 0)
				break;

			reducePrayerLevel(c);
			c.prayerDrainCounter -= drainResistance;
		}
	}

	public static void turnOffPrayers(Player c) {
		c.sendMessage("You have run out of prayer points!");
		resetPrayers(c);
		c.prayerDrainCounter = 0;
	}

	public static void reducePrayerLevel(Player c) {
		if (c.getSkills().getLevel(Skills.PRAYER) == 1) {
			turnOffPrayers(c);
		}

		c.getSkills().decrementPrayerPoints(1);
	}
	
	public static void drainPrayerByHit(Player p, int damage) {
		p.prayerDrainCounter += (damage * BASE_RESISTANCE);
	}

	public static void resetPrayers(Player c) {	
		for (Prayers prayer : Prayers.values) {
			c.getPacketSender().setConfig(prayer.getConfigCode(), 0);
		}

		Arrays.fill(c.prayerActive, false);
		c.curses().resetCurse();
		c.getPacketSender().sendConfig(ConfigCodes.QUICK_PRAYERS_ON, 0);
		c.quickPray = false;
	}

	public static boolean isAnyPrayerActive(Client c) {
		for (int i = 0, length = c.prayerActive.length; i < length; i++) {
			if (c.prayerActive[i]) {
				return true;
			}
		}

		for (int i = 0, length = c.curseActive.length; i < length; i++) {
			if (c.curseActive[i]) {
				return true;
			}
		}

		return false;
	}
	
	public static boolean usingProtectPrayer(Player c, int protectionType) {
		if (protectionType == ProtectionPrayer.PROTECT_MELEE.ordinal()) {
			if (c.curseActive[Curses.DEFLECT_MELEE.ordinal()]
					|| c.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]) {
				return true;
			}
		} else if (protectionType == ProtectionPrayer.PROTECT_RANGE.ordinal()) {
			if (c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]
					|| c.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]) {
				return true;
			}
		} else if (protectionType == ProtectionPrayer.PROTECT_MAGIC.ordinal()) {
			if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]
					|| c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]) {
				return true;
			}
		}
		return false;
	}
	
	public static void disableProtectPrayer(Player player) {
		disableProtectPrayer(player, null, 0);
	}
	
	public static void disableProtectPrayer(Player c, String msg, int ticks) {
		if (c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
				|| c.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]
						|| c.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]) {

			c.setHeadIcon(-1);
			CombatPrayer.deactivePrayer(c, Prayers.PROTECT_FROM_MAGIC);
			CombatPrayer.deactivePrayer(c, Prayers.PROTECT_FROM_MISSILES);
			CombatPrayer.deactivePrayer(c, Prayers.PROTECT_FROM_MELEE);
		}
		if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]
				|| c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]
						|| c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
			c.setHeadIcon(-1);
			c.curses().deactivate(Curses.DEFLECT_MAGIC.ordinal());
			c.curses().deactivate(Curses.DEFLECT_MISSILES.ordinal());
			c.curses().deactivate(Curses.DEFLECT_MELEE.ordinal());
		}
		if (msg != null)
			c.sendMessage(msg);
		if (ticks > 0)
			c.getProtectionPrayerDisabledTimer().startTimer(ticks);
	}
	
}
