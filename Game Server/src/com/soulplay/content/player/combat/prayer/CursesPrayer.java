package com.soulplay.content.player.combat.prayer;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.minigames.ddmtournament.Settings;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareNPC;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.net.packet.in.DuelPlayer;
import com.soulplay.util.Misc;

public class CursesPrayer {

	public static final int PROTECT_ITEM = 0, SAP_WARRIOR = 1, SAP_RANGER = 2,
			SAP_MAGE = 3, SAP_SPIRIT = 4, BERSERKER = 5, DEFLECT_SUMMONING = 6,
			DEFLECT_MAGIC = 7, DEFLECT_MISSILES = 8, DEFLECT_MELEE = 9;

	// private boolean appendedLeeches;

	/* All of the configuration such as activation, curse names, etc. */

	public static final int LEECH_ATTACK = 10;

	public static final int LEECH_RANGED = 11;

	public static final int LEECH_MAGIC = 12;

	public static final int LEECH_DEFENCE = 13;

	public static final int LEECH_STRENGTH = 14;

	public static final int LEECH_ENERGY = 15;

	public static final int LEECH_SPEC = 16;

	public static final int WRATH = 17;

	public static final int SOUL_SPLIT = 18;

	public static final int TURMOIL = 19;
	
	public static final int ANGUISH = 20;
	
	public static final int TORMENT = 21;

	private static final int[][] LEACH_DATA = {
			// projectile, end gfx
			{2231, 2232}, // attack
			{2236, 2238}, // ranged
			{2240, 2242}, // magic
			{2244, 2246}, // defense
			{2248, 2250}, // strength
			{2252, 2254}, // energy
			{2256, 2258}, // special
	};

	public static double getTurmoilMultiplier(Player c, int stat) {
		double turmoilMultiplier;
		switch(stat) {
			case Skills.STRENGTH:
			case Skills.RANGED:
			case Skills.MAGIC:
				turmoilMultiplier = 1.23;
				break;
			default:
				turmoilMultiplier = 1.15;
				break;
		}

		int modifier = -1;
		if (c.underAttackByPlayer()) {
			Player c2 = (Player) PlayerHandler.players[c.underAttackBy];
			if (c2 != null) {
				modifier = c2.getSkills().getLevel(stat);
			}
		} else if (c.oldNpcIndex > 0) {
			NPC npc = NPCHandler.npcs[c.oldNpcIndex];
			if (npc != null) {
				modifier = npc.def.getCombatLevel();
			}
		}

		double otherLevel = 1;
		if (modifier != -1) {
			switch(stat) {
				case Skills.ATTACK:
				case Skills.DEFENSE:
					otherLevel = modifier * 0.15;
					break;
				case Skills.STRENGTH:
				case Skills.RANGED:
				case Skills.MAGIC:
					otherLevel = modifier * 0.10;
					if (stat == Skills.STRENGTH && c.wildLevel > 0 && c.playerEquipment[PlayerConstants.playerWeapon] == 4718
							&& c.playerEquipment[PlayerConstants.playerHat] == 4716
							&& c.playerEquipment[PlayerConstants.playerChest] == 4720
							&& c.playerEquipment[PlayerConstants.playerLegs] == 4722) {
						otherLevel = otherLevel / 2;
					}
					break;
			}

			if (otherLevel > 14) {
				otherLevel = 14;
			}
		}

		turmoilMultiplier += otherLevel * .01;

		return turmoilMultiplier;
	}

	private Player c;

	public static final int[] GLOW = {83, 84, 85, 101, 102, 86, 87, 88, 89, 90, 91,
			103, 104, 92, 93, 94, 95, 96, 97, 105, 106, 107};

	public static final int[] HEADICONS = {-1, -1, -1, -1, -1, -1, 12, 10, 11, 9, -1,
			-1, -1, -1, -1, -1, -1, 16, 17, -1, -1, -1};

	public CursesPrayer(Player player) {
		this.c = player;
	}

	private static final int[] leeches = {LEECH_ATTACK, LEECH_RANGED, LEECH_MAGIC, LEECH_DEFENCE,
			LEECH_STRENGTH, LEECH_ENERGY, LEECH_SPEC};
	private static final int[] saps = {SAP_WARRIOR, SAP_RANGER, SAP_MAGE, SAP_SPIRIT};
	private static final int[] isHeadIcon = {DEFLECT_MAGIC, DEFLECT_MISSILES, DEFLECT_MELEE,
			WRATH, SOUL_SPLIT};

	public void activateCurse(int i) {
		if (c.playerPrayerBook != PrayerBook.CURSES) {
			return;
		}

		Curses curse = Curses.values[i];
		if (!c.getBarbarianAssault().canUsePrayer()) {
			c.sendMessage("You can't use prayer here.");
			c.getPacketSender().setConfig(curse.getConfigCode(), 0);
			return;
		}

		if (curse == Curses.PROTECT_ITEM && (c.getZones().isInDmmTourn() || c.getZones().isInClwRedPortalPkZone() || c.isRedSkulled())) {
			c.sendMessage("You cannot Active Protect Item here.");
			c.getPacketSender().setConfig(curse.getConfigCode(), 0);
			return;
		}

		if (c.attr().has("nightmare_curse")) {
			int newId = NightmareNPC.cursePrayerCurseType(curse).ordinal();
			if (newId != i) {
				i = newId;
			}
		}

		if (!Settings.checkPrayers(c, i, true))
			return;

		if (c.isDead() || c.getSkills().getLifepoints() <= 0) {
			if (!c.curseActive[i]) {
				c.getPacketSender().setConfig(curse.getConfigCode(), 0);
			} else {
				c.getPacketSender().setConfig(curse.getConfigCode(), 1);
			}
			return;
		}

		if (c.duelRule[DuelPlayer.RULE_PRAYER]) {
			c.getCombat().resetPrayers();
			c.sendMessage("Prayer has been disabled in this duel!");
			return;
		}

		if (c.getSkills().getLevel(Skills.DEFENSE) < 30) {
			c.getPacketSender().setConfig(curse.getConfigCode(), 0);
			c.sendMessage("You need 30 Defence to use this prayer.");
			return;
		}

		if (Config.PRAYER_POINTS_REQUIRED && c.getSkills().getPrayerPoints() <= 0) {
			c.getPacketSender().setConfig(curse.getConfigCode(), 0);
			c.sendMessage("You have run out of prayer points!");
			return;
		}

		if (Config.PRAYER_LEVEL_REQUIRED && c.getSkills().getStaticLevel(Skills.PRAYER) < curse.getLevelRequirement()) {
			deactivate(i);
			c.getPacketSender().sendString("You need a <col=08088A>Prayer</col> level of " + curse.getLevelRequirement() + " to use <col=08088A>" + curse.getPrayerName() + "</col>.", 357);
			c.getPacketSender().sendString("Click here to continue", 358);
			c.getPacketSender().sendChatInterface(356);
			c.getPacketSender().playSound(Sounds.PRAYER_FAIL);
			return;
		}

		if (!c.curseActive[i]) {
			curseEmote(curse);
		}

		boolean headIcon = false;
		switch (i) {
			case PROTECT_ITEM:
				c.lastProtItem = System.currentTimeMillis();
				break;

			case SAP_WARRIOR:
			case SAP_RANGER:
			case SAP_MAGE:
			case SAP_SPIRIT:
				if (!c.curseActive[i]) {
					for (int leeche : leeches) {
						if (leeche != i) {
							deactivate(leeche);
						}
					}
					deactivate(TURMOIL);
					deactivate(ANGUISH);
					deactivate(TORMENT);
				}
				break;

			case LEECH_ATTACK:
			case LEECH_RANGED:
			case LEECH_MAGIC:
			case LEECH_DEFENCE:
			case LEECH_STRENGTH:
			case LEECH_ENERGY:
			case LEECH_SPEC:
				if (!c.curseActive[i]) {
					for (int sap : saps) {
						if (sap != i) {
							deactivate(sap);
						}
					}
					deactivate(TURMOIL);
					deactivate(ANGUISH);
					deactivate(TORMENT);
				}
				break;

			case DEFLECT_SUMMONING:
			case DEFLECT_MAGIC:
			case DEFLECT_MISSILES:
			case DEFLECT_MELEE:
				headIcon = true;
				if (!c.getProtectionPrayerDisabledTimer().complete()) {
					c.sendMessage("You have been injured and can't use this prayer!");
					deactivate(i);
					return;
				}
				if (!c.curseActive[i]) {
					for (int element : isHeadIcon) {
						if (element != i) {
							deactivate(element);
						}
					}
				}
				break;
			case WRATH:
			case SOUL_SPLIT:
				headIcon = true;
				if (i != DEFLECT_SUMMONING) {
					deactivate(DEFLECT_SUMMONING);
				}
				if (!c.curseActive[i]) {
					for (int element : isHeadIcon) {
						if (element != i) {
							deactivate(element);
						}
					}
				}
				break;

			case TURMOIL:
			case ANGUISH:
			case TORMENT:
				if (!c.curseActive[i]) {
					for (int leeche : leeches) {
						if (leeche != i) {
							deactivate(leeche);
						}
					}
					for (int sap : saps) {
						if (sap != i) {
							deactivate(sap);
						}
					}

					deactivate(TURMOIL);
					deactivate(ANGUISH);
					deactivate(TORMENT);
				}
				break;
		}

		if (!headIcon) {
			if (!c.curseActive[i]) {
				c.curseActive[i] = true;

				int configCode;
				if (c.attr().has("nightmare_curse")) {
					configCode = GLOW[NightmareNPC.cursePrayerCurseTypeReverse(Curses.values[i]).ordinal()];
				} else {
					configCode = GLOW[i];
				}

				c.getPacketSender().setConfig(configCode, 1);
			} else {
				deactivate(i);
			}
		} else {
			if (!c.curseActive[i]) {
				c.curseActive[i] = true;

				int configCode;
				if (c.attr().has("nightmare_curse")) {
					configCode = GLOW[NightmareNPC.cursePrayerCurseTypeReverse(Curses.values[i]).ordinal()];
				} else {
					configCode = GLOW[i];
				}

				c.getPacketSender().setConfig(configCode, 1);

				c.setHeadIcon(HEADICONS[i]);

				if (c.curseActive[Curses.DEFLECT_SUMMONING.ordinal()]) {
					if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
						c.setHeadIcon(15);
					}
					if (c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
						c.setHeadIcon(14);
					}
					if (c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
						c.setHeadIcon(13);
					}
				}
			} else {
				deactivate(i);

				if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
					c.setHeadIcon(Curses.DEFLECT_MAGIC.getHeadIcon());
				} else if (c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
					c.setHeadIcon(Curses.DEFLECT_MISSILES.getHeadIcon());
				} else if (c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
					c.setHeadIcon(Curses.DEFLECT_MELEE.getHeadIcon());
				} else if (c.curseActive[Curses.DEFLECT_SUMMONING.ordinal()]) {
					c.setHeadIcon(Curses.DEFLECT_SUMMONING.getHeadIcon());
				} else {
					c.setHeadIcon(-1);
				}
			}
		}

		boolean allInactive = true;
		for (int i2 = 0, l = c.curseActive.length; i2 < l; i2++) {
			if (c.curseActive[i2]) {
				allInactive = false;
				break;
			}
		}

		if (allInactive)
			c.prayerDrainCounter = 0;
				
	}

	private void appendRandomLeech(int id, int leechType) {
		// int[] curseTypes = { Curses.LEECH_ATTACK, Curses.LEECH_RANGED,
		// Curses.LEECH_MAGIC, Curses.LEECH_DEFENCE, Curses.LEECH_STRENGTH,
		// Curses.LEECH_ENERGY, Curses.LEECH_SPEC };
		// if (!c.curseActive[curseTypes[leechType]]/* || Misc.random(3) < 3*/)
		// {
		// return;
		// }
		if (c.playerIndex > 0) {
			Player leechTarget = (Player) PlayerHandler.players[id];
			
			Projectile proj = new Projectile(LEACH_DATA[leechType][0],
					c.getCurrentLocation(), leechTarget.getCurrentLocation());
			proj.setStartHeight(25);
			proj.setEndHeight(25);
			proj.setStartDelay(0);
			proj.setLockon(leechTarget.getProjectileLockon());
//			proj.setSpeed(75);
			GlobalPackets.createProjectile(proj);
			
//			GlobalPackets.createPlayersProjectile(c.getX(), c.getY(), c.getZ(),
//					(c.getX() - leechTarget.getX()) * -1,
//					(c.getY() - leechTarget.getY()) * -1, 50, 75,
//					LEACH_DATA[leechType][0], 25, 25, -c.playerIndex - 1, 0);
		} else if (c.oldNpcIndex > 0) {
			NPC leechTargetNPC = NPCHandler.npcs[id];
			
			
			Projectile proj = new Projectile(LEACH_DATA[leechType][0],
					c.getCurrentLocation(), Location.create(leechTargetNPC.getX(), leechTargetNPC.getY(), leechTargetNPC.getHeightLevel()));
			proj.setStartHeight(25);
			proj.setEndHeight(25);
			proj.setStartDelay(0);
			proj.setLockon(leechTargetNPC.getProjectileLockon());
//			proj.setSpeed(75);
			GlobalPackets.createProjectile(proj);
			
//			GlobalPackets.createPlayersProjectile(c.getX(), c.getY(), c.getZ(),
//					(c.getX() - leechTargetNPC.getX()) * -1,
//					(c.getY() - leechTargetNPC.getY()) * -1, 50, 75,
//					LEACH_DATA[leechType][0], 25, 25, c.oldNpcIndex + 1, 0);
		}
		c.startAnimation(12575);
	}

	public boolean applyLeeches(Player o) {
		int chance = 5; // TODO: change back to 5
		if (Misc.random(chance) == 0) {
			if (leechAttack(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (leechDefence(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (leechStrength(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (leechSpecial(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (leechRanged(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (leechMagic(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (leechEnergy(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (sapAttack(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (sapRanged(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (sapMagic(o)) {
				return true;
			}
		}
		if (Misc.random(chance) == 0) {
			if (sapSpecial(o)) {
				return true;
			}
		}
		return false;
	}

	public void applynpcLeeches(int index) {
		// if (c.refreshCurseEffects == 0) {
		// c.refreshCurseEffects = 60;
		// }
		if (Misc.random(20) == 0) {
			npcleechAttack(index);
		}
		if (Misc.random(20) == 0) {
			npcleechDefence(index);
		}
		if (Misc.random(20) == 0) {
			npcleechStrength(index);
		}
		if (Misc.random(20) == 0) {
			npcleechSpecial(index);
		}
		if (Misc.random(20) == 0) {
			npcleechRanged(index);
		}
		if (Misc.random(20) == 0) {
			npcleechMagic(index);
		}
		if (Misc.random(20) == 0) {
			npcleechEnergy(index);
		}
	}

	public static final int[] buttonIds = {87231, 87233, 87235, 87237, 87239, 87241, 87243,
				87245, 87247, 87249, 87251, 87253, 87255, 88001, 88003, 88005,
				88007, 88009, 88011, 88013, 88056, 88060};
	
	public boolean curseButtons(int buttonId) {
		if (c.playerPrayerBook != PrayerBook.CURSES) {
			return false;
		}
		for (int i = 0; i < buttonIds.length; i++) {
			if (buttonIds[i] == buttonId) {
				activateCurse(i);
				return true;
			}
		}
		return false;
	}

	public void curseEmote(Curses curse) {
		switch (curse) {
			case PROTECT_ITEM:
				c.startAnimation(12567);
				c.startGraphic(Graphic.create(2213));
				break;
			case BERSERKER:
				c.startAnimation(12589);
				c.startGraphic(Graphic.create(2266));
				break;
			case TURMOIL:
			case ANGUISH:
			case TORMENT:
				c.startAnimation(12565);
				c.startGraphic(Graphic.create(2226));
				break;
			default:
				break;
		}
	}

	public void deactivate(int i) {
		c.curseActive[i] = false;
		c.getPacketSender().setConfig(GLOW[i], 0);
		if (i == Curses.TURMOIL.ordinal() && SeasonalData.isBeast(c)) {
			SeasonalData.endBeastPower(c);
		}
	}

	public void deflect(Player c2, int damage, int damageType) {
		if (damage < 1) {
			return;
		}
//		if (Misc.random(3) == 0) {
			int deflect = (damage < 10) ? 1 : (int) (damage / 10);
			c2.dealDamage(new Hit(deflect, 0, 3, c));
			c2.getDamageMap().incrementTotalDamage(c.mySQLIndex, deflect);
			c.startGraphic(Graphic.create(deflectGfx[damageType]));
			c.startAnimation(12573);
//		}
	}

	private static final int[] deflectGfx = {2230, 2229, 2228, 2228};
	
	public void deflectNPC(NPC n, int damage, int damageType) {
		if (damage < 1) {
			return;
		}
		if (Misc.random(3) == 0) {
			int deflect = (damage < 10) ? 1 : (int) (damage / 10);
			c.getCombat().appendHit(n, deflect, 0, 3, 2, false);
			c.startGraphic(Graphic.create(deflectGfx[damageType]));
			c.startAnimation(12573);
		}
	}

	/* Actual curse content */
	public boolean leechAttack(final Player c2) {
		if (!c.curseActive[10]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		// c.gfx0(2234);
		c.startAnimation(12575);
		
		Projectile proj = new Projectile(2231, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(1);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2231, 43, 31, -c.playerIndex - 1, 1);

		c2.startGraphic(Graphic.create(2232));
		if (c.attackMultiplier < 0.05) {
			c.attackMultiplier += 0.01;
			c.sendMessage("You leech your opponent's attack.");
		}
		if (c2.attackMultiplier > -0.25) {
			if (c2.attackMultiplier == 0) {
				c2.attackMultiplier -= 0.10;
			} else {
				c2.attackMultiplier -= 0.03;
			}
		}

		// c.leechAttackDelay = 2;
		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechAttackDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechAttackDelay > 0) {
		// leechAttackDelay--;
		// }
		// if (leechAttackDelay == 1) {
		// c2.gfx0(2232);
		// if (c.attackMultiplier < 0.05) {
		// c.attackMultiplier += 0.01;
		// }
		//// if (c2.attackMultiplier > 0.80) {
		//// c2.attackMultiplier -= 0.01;
		//// }
		// }
		// if (leechAttackDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public boolean leechDefence(final Player c2) {
		if (!c.curseActive[13]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		// c.gfx0(2243);
		c.startAnimation(12575);
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2244, 43, 31, -c.playerIndex - 1, 3);
		
		Projectile proj = new Projectile(2244, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(3);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

		c2.startGraphic(Graphic.create(2246));
		if (c.defenceMultiplier < 0.05) {
			c.defenceMultiplier += 0.01;
			c.sendMessage("You leech your opponent's defence.");
		}
		if (c2.defenceMultiplier > -0.25) {
			if (c2.defenceMultiplier == 0) {
				c2.defenceMultiplier -= 0.10;
			} else {
				c2.defenceMultiplier -= 0.03;
			}
		}

		// c.leechDefenceDelay = 2;
		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechDefenceDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechDefenceDelay > 0) {
		// leechDefenceDelay--;
		// }
		// if (leechDefenceDelay == 1) {
		// c2.gfx0(2246);
		// if (c.defenceMultiplier < 0.05) {
		// c.defenceMultiplier += 0.01;
		// }
		//// if (c2.defenceMultiplier > 0.80) {
		//// c2.defenceMultiplier -= 0.01;
		//// }
		// }
		// if (leechDefenceDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public boolean leechEnergy(final Player c2) {
		if (!c.curseActive[15]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		c.sendMessage("You leech your opponent's run energy.");
		// c.gfx0(2234);
		c.startAnimation(12575);
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2252, 43, 31, -c.playerIndex - 1, 5);
		
		Projectile proj = new Projectile(2252, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(5);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

		c2.startGraphic(Graphic.create(2254));
		double drained = c2.getMovement().drainEnergy(10);
		c.getMovement().restoreRunEnergy(drained);

		// c.leechEnergyDelay = 2;
		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechEnergyDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechEnergyDelay > 0) {
		// leechEnergyDelay--;
		// }
		// if (leechEnergyDelay == 1) {
		// c2.gfx0(2254);
		// }
		// if (leechEnergyDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		// return true;
		// }
		// return false;

		return true;
	}

	public boolean leechMagic(final Player c2) {
		if (!c.curseActive[12]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		// c.gfx0(2239);
		c.startAnimation(12575);
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2240, 43, 31, -c.playerIndex - 1, 2);
		Projectile proj = new Projectile(2240, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(2);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

		c2.startGraphic(Graphic.create(2242));
		if (c.magicMultiplier < 0.05) {
			c.magicMultiplier += 0.01;
			c.sendMessage("You leech your opponent's magic.");
		}
		if (c2.magicMultiplier > -0.25) {
			if (c2.magicMultiplier == 0) {
				c2.magicMultiplier -= 0.10;
			} else {
				c2.magicMultiplier -= 0.03;
			}
		}

		// c.leechMagicDelay = 2;
		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechMagicDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechMagicDelay > 0) {
		// leechMagicDelay--;
		// }
		// if (leechMagicDelay == 1) {
		// c2.gfx0(2242);
		// if (c.magicMultiplier < 0.05) {
		// c.magicMultiplier += 0.01;
		// }
		//// if (c2.magicMultiplier > 0.80) {
		//// c2.magicMultiplier -= 0.01;
		//// }
		// }
		// if (leechMagicDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public void leechPrayers(final Player leechTarget,
			final NPC leechTargetNPC) {

		final int[] curseTypes = {CursesPrayer.LEECH_ATTACK,
				CursesPrayer.LEECH_RANGED, CursesPrayer.LEECH_MAGIC,
				Curses.LEECH_DEFENCE.ordinal(), CursesPrayer.LEECH_STRENGTH,
				CursesPrayer.LEECH_ENERGY, CursesPrayer.LEECH_SPEC};
		// c.sendMessage("able to leech?");
		boolean usingLeech = false;
		for (int curseType : curseTypes) {
			if (c.curseActive[curseType]) {
				usingLeech = true;
				break;
			}
		}
		if (!usingLeech) {
			// c.sendMessage("not using leeches.");
			return;
		}

		// c.sendMessage("able to leech2?");

		Server.getTaskScheduler().schedule(new Task(1) {

			int counter = 7;

			int leechType;

			@Override
			public void execute() {
				if (c.disconnected) {
					this.stop();
					return;
				}
				if (leechTarget != null) {
					if (leechTarget.disconnected) {
						this.stop();
						return;
					}
				}
				if (counter > 0) {
					counter--;
				} else {
					this.stop();
					return;
				}
				if (counter == 5) {
					leechType = Misc.random(6);
					if (c.playerIndex > 0) {
						if (!c.curseActive[curseTypes[leechType]]
								|| Misc.random(3) < 3) {
							this.stop();
							return;
						}
						appendRandomLeech(c.playerIndex, leechType);
					} else if (c.oldNpcIndex > 0) {
						if (!c.curseActive[curseTypes[leechType]]
								|| Misc.random(3) < 3) {
							this.stop();
							return;
						}
						appendRandomLeech(c.oldNpcIndex, leechType);
					}
				} else if (counter == 3) {
					if (leechTarget != null) {
						leechTarget.startGraphic(Graphic.create(LEACH_DATA[leechType][1]));
						// leechEffect(leechType);
					} else if (leechTargetNPC != null) {
						leechTargetNPC.startGraphic(Graphic.create(LEACH_DATA[leechType][1]));
						// leechEffectNPC(leechType);
					}
				}

			}
		});

	}

	public boolean leechRanged(final Player c2) {
		if (!c.curseActive[11]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		// c.gfx0(2235);
		c.startAnimation(12575);
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2236, 43, 31, -c.playerIndex - 1, 0);
		Projectile proj = new Projectile(2236, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(0);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

		c2.startGraphic(Graphic.create(2238));
		if (c.rangedMultiplier < 0.05) {
			c.rangedMultiplier += 0.01;
			c.sendMessage("You leech your opponent's range.");
		}
		if (c2.rangedMultiplier > -0.25) {
			if (c2.rangedMultiplier == 0) {
				c2.rangedMultiplier -= 0.10;
			} else {
				c2.rangedMultiplier -= 0.03;
			}
		}

		// if (c2.rangedMultiplier > )

		// c.leechRangedDelay = 2;
		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechRangedDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechRangedDelay > 0) {
		// leechRangedDelay--;
		// }
		// if (leechRangedDelay == 1) {
		// c2.gfx0(2238);
		// if (c.rangedMultiplier < 0.05) {
		// c.rangedMultiplier += 0.01;
		// }
		//// if (c2.rangedMultiplier > 0.80) {
		//// c2.rangedMultiplier -= 0.01;
		//// }
		// }
		// if (leechRangedDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public boolean leechSpecial(final Player c2) {
		if (!c.curseActive[16]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		// c.gfx0(2253);
		c.startAnimation(12575);
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2256, 43, 31, -c.playerIndex - 1, 6);
		Projectile proj = new Projectile(2256, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(6);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

		c2.startGraphic(Graphic.create(2258));
		if (c.specAmount >= 10) {
			return true;
		}
		if (c2.specAmount <= 0) {
			return true;
		}
		c.sendMessage("You leech some special attack energy from your enemy.");
		c.specAmount += 1;
		c2.specAmount -= 1;
		c.getItems()
				.addSpecialBar(true);
		c2.getItems().addSpecialBar(true);
		c2.sendMessage("Your special attack has been drained.");

		// c.leechSpecialDelay = 2;

		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechSpecialDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechSpecialDelay > 0) {
		// leechSpecialDelay--;
		// }
		// if (leechSpecialDelay == 1) {
		// c2.gfx0(2258);
		// if (c.specAmount >= 10)
		// return;
		// if (c2.specAmount <= 0)
		// return;
		// c.specAmount += 1;
		// c2.specAmount -= 1;
		// c2.sendMessage("Your special attack has been drained.");
		// }
		// if (leechSpecialDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		// return true;

		// }
		// return false;
		return true;
	}

	public boolean leechStrength(final Player c2) {
		if (!c.curseActive[14]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		int offX = (pY - oY) * -1;
//		int offY = (pX - oX) * -1;
		// c.gfx0(2247);
		c.startAnimation(12575);
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2248, 43, 31, -c.playerIndex - 1, 4);
		Projectile proj = new Projectile(2248, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(25);
		proj.setEndHeight(0);
		proj.setStartDelay(4);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

		c2.startGraphic(Graphic.create(2250));
		if (c.strengthMultiplier < 0.05) {
			c.strengthMultiplier += 0.01;
			c.sendMessage("You leech your opponent's strength.");
			// c.playerLevel[Player.playerStrength] +=
			// (int)Math.round(c.getLevelForXP(c.playerXP[Player.playerStrength])
			// * 0.01);
			// c.refreshSkill(Player.playerStrength);
		}
		// c.sendMessage("TESTING
		// "+(Math.round(c.getLevelForXP(c.playerXP[Player.playerStrength]) *
		// 0.10)));
		// if (c2.playerLevel[Player.playerStrength] > 0 &&
		// c.playerLevel[Player.playerStrength] <
		// (c.getLevelForXP(c.playerXP[Player.playerStrength]) +
		// Math.round(c.getLevelForXP(c.playerXP[Player.playerStrength]) *
		// 0.10))) {
		// c.playerLevel[Player.playerStrength] += 1;
		// c2.playerLevel[Player.playerStrength] -= 1;
		//
		// c.refreshSkill(Player.playerStrength);
		// c2.refreshSkill(Player.playerStrength);
		//// c.sendMessage("Worked");
		// }

		if (c2.strengthMultiplier > -0.25) {
			if (c2.strengthMultiplier == 0) {
				c2.strengthMultiplier -= 0.10;
			} else {
				c2.strengthMultiplier -= 0.03;
			}
		}

		// c.leechStrengthDelay = 2;
		// Server.getTaskScheduler().schedule(new Task(1) {
		// int leechStrengthDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (leechStrengthDelay > 0) {
		// leechStrengthDelay--;
		// }
		// if (leechStrengthDelay == 1) {
		// c2.gfx0(2250);
		// if (c.strengthMultiplier < 0.05) {
		// c.strengthMultiplier += 0.01;
		// }
		//// if (c2.strengthMultiplier > 0.80) {
		//// c2.strengthMultiplier -= 0.01;
		//// }
		// }
		// if (leechStrengthDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public void npcleechAttack(int index) {
		if (!c.curseActive[10]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
				final NPC n = NPCHandler.npcs[c.oldNpcIndex];
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's attack.");
				// c.gfx0(2234);
				c.startAnimation(12575);
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2231, 43, 31, -c.oldNpcIndex - 1, 1);
				
				Projectile proj = new Projectile(2231, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(1);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);

				n.startGraphic(Graphic.create(2232));
				if (c.attackMultiplier < 0.05) {
					c.attackMultiplier += 0.01;
				}

				// c.leechAttackDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechAttackDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// this.stop();
				// return;
				// }
				// if (n == null) {
				// this.stop();
				// return;
				// }
				// if(n.isDead) {
				// this.stop();
				// return;
				// }
				// if (leechAttackDelay > 0) {
				// leechAttackDelay--;
				// }
				// if (leechAttackDelay == 1) {
				//// NPCHandler.npcs[c.oldNpcIndex].gfx0(2232);
				// n.gfx0(2232);
				// if (c.attackMultiplier < 1.10) {
				// c.attackMultiplier += 0.01;
				// }
				// }
				// if (leechAttackDelay == 0) {
				// this.stop();
				// }
				// }
				// });
			}
		}
	}

	public void npcleechDefence(int index) {
		if (!c.curseActive[13]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
				final NPC n = NPCHandler.npcs[c.oldNpcIndex];
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's defence.");
				// c.gfx0(2243);
				c.startAnimation(12575);
				
				Projectile proj = new Projectile(2244, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(1);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);
				
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2244, 43, 31, -c.oldNpcIndex - 1, 3);

				NPCHandler.npcs[c.oldNpcIndex].startGraphic(Graphic.create(2246));
				if (c.defenceMultiplier < 0.05) {
					c.defenceMultiplier += 0.01;
				}

				// c.leechDefenceDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechDefenceDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// stop();
				// return;
				// }
				// if (c.oldNpcIndex < 1) {
				// stop();
				// return;
				// }
				// if (NPCHandler.npcs[c.oldNpcIndex] == null) {
				// stop();
				// return;
				// }
				// if(NPCHandler.npcs[c.oldNpcIndex].isDead) {
				// stop();
				// return;
				// }
				// if (leechDefenceDelay > 0) {
				// leechDefenceDelay--;
				// }
				// if (leechDefenceDelay == 1) {
				// NPCHandler.npcs[c.oldNpcIndex].gfx0(2246);
				// if (c.defenceMultiplier < 1.10) {
				// c.defenceMultiplier += 0.01;
				// }
				// }
				// if (leechDefenceDelay == 0) {
				// stop();
				// }
				// }
				// });
			}
		}
	}

	public void npcleechEnergy(int index) {
		if (!c.curseActive[15]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's run energy.");
				// c.gfx0(2251);
				c.startAnimation(12575);
				
				NPC n = NPCHandler.npcs[c.oldNpcIndex];
				Projectile proj = new Projectile(2252, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(1);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);
				
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2252, 43, 31, -c.oldNpcIndex - 1, 5);

				NPCHandler.npcs[c.oldNpcIndex].startGraphic(Graphic.create(2254));
				if (c.getRunEnergy() < 100) {
					c.setRunEnergy(c.getRunEnergy() + 10);
				}

				// c.leechEnergyDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechEnergyDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// stop();
				// return;
				// }
				// if (c.oldNpcIndex < 1) {
				// stop();
				// return;
				// }
				// if (NPCHandler.npcs[c.oldNpcIndex] == null) {
				// stop();
				// return;
				// }
				// if(NPCHandler.npcs[c.oldNpcIndex].isDead) {
				// stop();
				// return;
				// }
				// if (leechEnergyDelay > 0) {
				// leechEnergyDelay--;
				// }
				// if (leechEnergyDelay == 1) {
				// NPCHandler.npcs[c.oldNpcIndex].gfx0(2254);
				// }
				// if (leechEnergyDelay == 0) {
				// stop();
				// }
				// }
				// });
			}
		}
	}

	public void npcleechMagic(int index) {
		if (!c.curseActive[12]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's magic.");
				// c.gfx0(2239);
				c.startAnimation(12575);
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2240, 43, 31, -c.oldNpcIndex - 1, 2);

				NPC n = NPCHandler.npcs[c.oldNpcIndex];
				Projectile proj = new Projectile(2240, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(2);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);
				
				NPCHandler.npcs[c.oldNpcIndex].startGraphic(Graphic.create(2242));
				if (c.magicMultiplier < 0.05) {
					c.magicMultiplier += 0.01;
				}

				// c.leechMagicDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechMagicDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// stop();
				// return;
				// }
				// if (c.oldNpcIndex < 1) {
				// stop();
				// return;
				// }
				// if (NPCHandler.npcs[c.oldNpcIndex] == null) {
				// stop();
				// return;
				// }
				// if(NPCHandler.npcs[c.oldNpcIndex].isDead) {
				// stop();
				// return;
				// }
				// if (leechMagicDelay > 0) {
				// leechMagicDelay--;
				// }
				// if (leechMagicDelay == 1) {
				// NPCHandler.npcs[c.oldNpcIndex].gfx0(2242);
				// if (c.magicMultiplier < 1.10) {
				// c.magicMultiplier += 0.01;
				// }
				// }
				// if (leechMagicDelay == 0) {
				// stop();
				// }
				// }
				// });
			}
		}
	}

	public void npcleechRanged(int index) {
		if (!c.curseActive[11]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's range.");
				// c.gfx0(2235);
				c.startAnimation(12575);
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2236, 43, 31, -c.oldNpcIndex - 1, 0);
				
				NPC n = NPCHandler.npcs[c.oldNpcIndex];
				Projectile proj = new Projectile(2236, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(0);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);

				NPCHandler.npcs[c.oldNpcIndex].startGraphic(Graphic.create(2238));
				
				if (c.rangedMultiplier < 0.05) {
					c.rangedMultiplier += 0.01;
				}

				// c.leechRangedDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechRangedDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// stop();
				// return;
				// }
				// if (c.oldNpcIndex < 1) {
				// stop();
				// return;
				// }
				// if (NPCHandler.npcs[c.oldNpcIndex] == null) {
				// stop();
				// return;
				// }
				// if(NPCHandler.npcs[c.oldNpcIndex].isDead) {
				// stop();
				// return;
				// }
				// if (leechRangedDelay > 0) {
				// leechRangedDelay--;
				// }
				// if (leechRangedDelay == 1) {
				// NPCHandler.npcs[c.oldNpcIndex].gfx0(2238);
				// if (c.rangedMultiplier < 1.10) {
				// c.rangedMultiplier += 0.01;
				// }
				// }
				// if (leechRangedDelay == 0) {
				// stop();
				// }
				// }
				// });
			}
		}
	}

	public void npcleechSpecial(int index) {
		if (!c.curseActive[16]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's special attack.");
				// c.gfx0(2255);
				c.startAnimation(12575);
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2256, 43, 31, -c.oldNpcIndex - 1, 6);
				
				NPC n = NPCHandler.npcs[c.oldNpcIndex];
				Projectile proj = new Projectile(2256, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(6);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);

				NPCHandler.npcs[c.oldNpcIndex].startGraphic(Graphic.create(2258));
				if (c.specAmount >= 10) {
					return;
				}
				c.specAmount += 1;

				// c.leechSpecialDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechSpecialDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// stop();
				// return;
				// }
				// if (c.oldNpcIndex < 1) {
				// stop();
				// return;
				// }
				// if (NPCHandler.npcs[c.oldNpcIndex] == null) {
				// stop();
				// return;
				// }
				// if(NPCHandler.npcs[c.oldNpcIndex].isDead) {
				// stop();
				// return;
				// }
				// if (leechSpecialDelay > 0) {
				// leechSpecialDelay--;
				// }
				// if (leechSpecialDelay == 1) {
				// NPCHandler.npcs[c.oldNpcIndex].gfx0(2258);
				// if (c.specAmount >= 10)
				// return;
				// c.specAmount += 1;
				// }
				// if (leechSpecialDelay == 0) {
				// stop();
				// }
				// }
				// });
			}
		}
	}

	public void npcleechStrength(int index) {
		if (!c.curseActive[14]) {
			return;
		}
		if (c.oldNpcIndex > 0) {
			if (NPCHandler.npcs[c.oldNpcIndex] != null) {
//				final int pX = c.getX();
//				final int pY = c.getY();
//				final int nX = NPCHandler.npcs[c.oldNpcIndex].getX();
//				final int nY = NPCHandler.npcs[c.oldNpcIndex].getY();
//				final int offX = (pY - nY) * -1;
//				final int offY = (pX - nX) * -1;
				c.sendMessage("You leech your opponent's strength.");
				// c.gfx0(2247);
				c.startAnimation(12575);
//				GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX,
//						offY, 50, 45, 2248, 43, 31, -c.oldNpcIndex - 1, 4);
				
				NPC n = NPCHandler.npcs[c.oldNpcIndex];
				Projectile proj = new Projectile(2248, c.getCurrentLocation(), Location.create(n.getX(), n.getY(), n.getHeightLevel()));
				proj.setStartHeight(43);
				proj.setEndHeight(31);
				proj.setStartDelay(4);
				proj.setEndDelay(45);
				proj.setLockon(n.getProjectileLockon());
				GlobalPackets.createProjectile(proj);

				NPCHandler.npcs[c.oldNpcIndex].startGraphic(Graphic.create(2250));
				if (c.strengthMultiplier < 0.05) {
					c.strengthMultiplier += 0.01;
				}

				// c.leechStrengthDelay = 2;
				// Server.getTaskScheduler().schedule(new Task(1) {
				// int leechStrengthDelay = 2;
				// public void execute() {
				// if(c.disconnected) {
				// stop();
				// return;
				// }
				// if(c.oldNpcIndex <= 0) {
				// stop();
				// return;
				// }
				// if (NPCHandler.npcs[c.oldNpcIndex] == null) {
				// stop();
				// return;
				// }
				// if(NPCHandler.npcs[c.oldNpcIndex].isDead) {
				// stop();
				// return;
				// }
				// if (leechStrengthDelay > 0) {
				// leechStrengthDelay--;
				// }
				// if (leechStrengthDelay == 1) {
				// NPCHandler.npcs[c.oldNpcIndex].gfx0(2250);
				// if (c.strengthMultiplier < 1.10) {
				// c.strengthMultiplier += 0.01;
				// }
				// }
				// if (leechStrengthDelay == 0) {
				// stop();
				// }
				// }
				// });
			}
		}
	}

	public void resetCurse() {
		for (int i = 0, length = c.curseActive.length; i < length; i++) {
			deactivate(i);
		}

		c.setHeadIcon(-1);
	}

	public void resetCurseBoosts() {
		if (c.attackMultiplier != 0 || c.rangedMultiplier != 0
				|| c.defenceMultiplier != 0 || c.magicMultiplier != 0
				|| c.strengthMultiplier != 0) {
			c.attackMultiplier = c.rangedMultiplier = c.defenceMultiplier = c.magicMultiplier = c.strengthMultiplier = 0;
		}
	}

	public boolean sapAttack(final Player c2) {
		if (!c.curseActive[1]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		final int offX = (pY - oY) * -1;
//		final int offY = (pX - oX) * -1;
		c.startAnimation(12569);
		// c.sapAttackDelay = 2;
		
		c.startGraphic(Graphic.create(2214));

		Projectile proj = new Projectile(2215, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(43);
		proj.setEndHeight(31);
		proj.setStartDelay(20);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		
//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2215, 31, 31, -c.playerIndex - 1, 20);
		Server.getStillGraphicsManager().stillGraphics(c2, c2.getX(), c2.getY(),
				0, 2216, 50);
		if (c.attackMultiplier > -0.20) {
			if (c.attackMultiplier == 0) {
				c.attackMultiplier -= 0.10;
			}
			c.attackMultiplier -= 0.02;
			c.sendMessage("You sap your opponent's attack.");
		}

		// Server.getTaskScheduler().schedule(new Task(1) {
		// int sapAttackDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// this.stop();
		// return;
		// }
		// if (sapAttackDelay > 0) {
		// sapAttackDelay--;
		// }
		// if (sapAttackDelay == 1) {
		// c.getPA().createPlayersProjectile(pX, pY, offX, offY,
		// 50, 45, 2215, 31, 31, -c.oldPlayerIndex - 1, 1);
		// c2.gfx0(2216);
		// }
		// if (sapAttackDelay == 2) {
		// if (c.attackMultiplier < 1.10) {
		// c.attackMultiplier += 0.01;
		// }
		// if (c2.attackMultiplier > 0.80) {
		// c2.attackMultiplier -= 0.01;
		// }
		// }
		// if (sapAttackDelay == 0) {
		// this.stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public boolean sapMagic(final Player c2) {
		if (!c.curseActive[3]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		final int offX = (pY - oY) * -1;
//		final int offY = (pX - oX) * -1;
		c.startAnimation(12569);
		// c.sapMagicDelay = 2;

		c.startGraphic(Graphic.create(2220));
		
		Projectile proj = new Projectile(2221, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(43);
		proj.setEndHeight(31);
		proj.setStartDelay(20);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2221, 31, 31, -c.playerIndex - 1, 20);
		Server.getStillGraphicsManager().stillGraphics(c2, c2.getX(), c2.getY(),
				0, 2222, 50);

		if (c2.magicMultiplier > -0.20) {
			if (c2.magicMultiplier == 0) {
				c2.magicMultiplier -= 0.10;
			}
			c2.magicMultiplier -= 0.02;
			c.sendMessage("You sap your opponent's magic.");
		}

		// Server.getTaskScheduler().schedule(new Task(1) {
		// int sapMagicDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// stop();
		// return;
		// }
		// if (sapMagicDelay > 0) {
		// sapMagicDelay--;
		// }
		// if (sapMagicDelay == 1) {
		// c.getPA().createPlayersProjectile(pX, pY, offX, offY,
		// 50, 45, 2221, 31, 31, -c.oldPlayerIndex - 1, 1);
		// c2.gfx0(2222);
		// }
		// if (sapMagicDelay == 2) {
		// if (c.magicMultiplier < 1.10) {
		// c.magicMultiplier += 0.01;
		// }
		// if (c2.magicMultiplier > 0.80) {
		// c2.magicMultiplier -= 0.01;
		// }
		// }
		// if (sapMagicDelay == 0) {
		// stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public boolean sapRanged(final Player c2) {
		if (!c.curseActive[2]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		final int offX = (pY - oY) * -1;
//		final int offY = (pX - oX) * -1;
		c.startAnimation(12569);
		// c.sapRangedDelay = 2;

		c.startGraphic(Graphic.create(2217));
		
		Projectile proj = new Projectile(2218, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(31);
		proj.setEndHeight(31);
		proj.setStartDelay(20);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2218, 31, 31, -c.playerIndex - 1, 20);
		Server.getStillGraphicsManager().stillGraphics(c2, c2.getX(), c2.getY(),
				0, 2219, 50);

		if (c.rangedMultiplier > -0.20) {
			if (c.rangedMultiplier == 0) {
				c.rangedMultiplier -= 0.10;
			}
			c.rangedMultiplier -= 0.02;
			c.sendMessage("You sap your opponent's range.");
		}

		// Server.getTaskScheduler().schedule(new Task(1) {
		// int sapRangedDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// stop();
		// return;
		// }
		// if (sapRangedDelay > 0) {
		// sapRangedDelay--;
		// }
		// if (sapRangedDelay == 1) {
		// c.getPA().createPlayersProjectile(pX, pY, offX, offY,
		// 50, 45, 2218, 31, 31, -c.oldPlayerIndex - 1, 1);
		// c2.gfx0(2219);
		// }
		// if (sapRangedDelay == 2) {
		// if (c.rangedMultiplier < 1.10) {
		// c.rangedMultiplier += 0.01;
		// }
		// if (c2.rangedMultiplier > 0.80) {
		// c2.rangedMultiplier -= 0.01;
		// }
		// }
		// if (sapRangedDelay == 0) {
		// stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public boolean sapSpecial(final Player c2) {
		if (!c.curseActive[4]) {
			return false;
		}
		// if (PlayerHandler.players[index] != null) {
		// final Player c2 = (Player) PlayerHandler.players[index];
//		final int pX = c.getX();
//		final int pY = c.getY();
//		final int oX = c2.getX();
//		final int oY = c2.getY();
//		final int offX = (pY - oY) * -1;
//		final int offY = (pX - oX) * -1;
		c.startAnimation(12569);
		// c.sapSpecialDelay = 2;
		c.startGraphic(Graphic.create(2223));
		
		Projectile proj = new Projectile(2224, c.getCurrentLocation(), c2.getCurrentLocation());
		proj.setStartHeight(31);
		proj.setEndHeight(31);
		proj.setStartDelay(20);
		proj.setEndDelay(45);
		proj.setLockon(c2.getProjectileLockon());
		GlobalPackets.createProjectile(proj);

//		GlobalPackets.createPlayersProjectile(pX, pY, c.getZ(), offX, offY, 50,
//				45, 2224, 31, 31, -c.playerIndex - 1, 20);
		Server.getStillGraphicsManager().stillGraphics(c2, c2.getX(), c2.getY(),
				0, 2225, 50);
		if (c2.specAmount <= 0) {
			return true;
		}
		c.sendMessage("You sap your opponent's special attack.");
		c2.specAmount -= 1;
		c2.getItems()
				.addSpecialBar(true);
		c2.sendMessage("Your special attack has been drained.");

		// Server.getTaskScheduler().schedule(new Task(1) {
		// int sapSpecialDelay = 2;
		// public void execute() {
		// if(c.disconnected || c2.disconnected) {
		// stop();
		// return;
		// }
		// if (sapSpecialDelay > 0) {
		// sapSpecialDelay--;
		// }
		// if (sapSpecialDelay == 1) {
		// c.getPA().createPlayersProjectile(pX, pY, offX, offY,
		// 50, 45, 2224, 31, 31, -c.oldPlayerIndex - 1, 1);
		// c2.gfx0(2225);
		// if (c2.specAmount <= 0) {
		// return;
		// }
		// c2.specAmount -= 1;
		// c2.getItems().addSpecialBar(
		// c.playerEquipment[Player.playerWeapon]);
		// c2.sendMessage("Your special attack has been drained.");
		// }
		// if (sapSpecialDelay == 0) {
		// stop();
		// }
		// }
		// });
		return true;
		// }
		// return false;
	}

	public void soulSplit(Player other, int damage) {
		if (c.curseActive[SOUL_SPLIT]) {
			SoulSplitOnPlayer(c, other, damage);
		}
	}

	public void soulSplit(int id, int damage) { // Changed to soulsplit on npcs
												// only
		if (c.curseActive[SOUL_SPLIT]) {
			/*
			 * if (c.oldPlayerIndex > 0) { SoulSplitOnPlayer(c, id, damage); }
			 * else
			 */if (c.oldNpcIndex > 0) {
				SoulSplitOnNpc(c, id, damage);
			}
		}
	}

	public void SoulSplitOnNpc(final Player player, int id, int damage) {

		final NPC ssTargetNPC = NPCHandler.npcs[id];;

		final int ssHeal = damage / 5;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int counter = 4;

			@Override
			public void execute(CycleEventContainer e) {
				if (c.disconnected) {
					e.stop();
					return;
				}
				if (counter > 0) {
					counter--;
				} else {
					e.stop();
					return;
				}
				if (counter == 2) {
					if (ssTargetNPC != null && ssHeal != 0) {
//						GlobalPackets.createPlayersProjectile(
//								ssTargetNPC.getX(), ssTargetNPC.getY(),
//								ssTargetNPC.getHeightLevel(),
//								(ssTargetNPC.getY() - player.getY()) * -1,
//								(ssTargetNPC.getX() - player.getX()) * -1, 50,
//								75, 2263, 25, 25, -player.getId() - 1, 40);
						
						Projectile proj = new Projectile(2263, Location.create(ssTargetNPC.getX(), ssTargetNPC.getY(), ssTargetNPC.getHeightLevel()), c.getCurrentLocation());
						proj.setStartHeight(25);
						proj.setEndHeight(25);
						proj.setStartDelay(40);
						proj.setEndDelay(75);
						proj.setLockon(c.getProjectileLockon());
						GlobalPackets.createProjectile(proj);

						ssTargetNPC.startGraphic(Graphic.create(2264));
					}
					if (ssHeal > 0)
						player.addToHp(ssHeal);
				}
			}

			@Override
			public void stop() {
			}
		}, 1);

	}

	//TODO: delete this and the soulsplitonnpc shit! using mob now!
	public void SoulSplitOnPlayer(final Player player, final Player ssTarget,
			int damage) {
		if (ssTarget == null) {
			return;
		}
		final int ssHeal = damage / 5;

		final int ssSmite = damage / 4;

		if (ssTarget != null && (ssHeal > 0 || ssSmite > 0)) {
			
			Projectile proj = new Projectile(2263, ssTarget.getCurrentLocation(), player.getCurrentLocation());
			proj.setStartHeight(25);
			proj.setEndHeight(25);
			proj.setStartDelay(40);
			proj.setEndDelay(75);
			proj.setLockon(player.getProjectileLockon());
			GlobalPackets.createProjectile(proj);
			
//			GlobalPackets.createPlayersProjectile(ssTarget.getX(),
//					ssTarget.getY(), ssTarget.getZ(),
//					(ssTarget.getY() - player.getY()) * -1,
//					(ssTarget.getX() - player.getX()) * -1, 50, 75, 2263, 25,
//					25, -player.getId() - 1, 40);
			ssTarget.startGraphic(Graphic.create(2264));
		}

		if (ssSmite > 0) {
			CombatPrayer.drainPrayerByHit(ssTarget, ssSmite); 
//			ssTarget.getSkills().decrementPrayerPoints(ssSmite);
//			if (ssTarget.getSkills().getPrayerPoints() <= 0) {
//				ssTarget.getCombat().resetPrayers();
//			}
		}

		if (player.getSkills().getLifepoints() > 0 && ssHeal > 0) {
			player.getSkills().heal(ssHeal);
		}

	}
	
	public void SoulSplitOnMob(final Mob ssTarget, boolean soulsplitActive, int... hits) {
		if (ssTarget == null || !soulsplitActive) {
			return;
		}

		int totalHeal = 0;
		int totalSmite = 0;
		for (int hit : hits) {
			totalHeal += hit / 5;
			totalSmite += hit / 4;
		}

		if (totalSmite > 0 && ssTarget.isPlayer()) {
			CombatPrayer.drainPrayerByHit(ssTarget.toPlayer(), totalSmite);
		}

		if (c.getSkills().getLifepoints() > 0 && totalHeal > 0) {
			c.getSkills().heal(totalHeal);
		}

		if (totalHeal > 0 || totalSmite > 0) {
			Projectile proj = new Projectile(2263, ssTarget.getCurrentLocation(), c.getCurrentLocation());
			proj.setStartHeight(25);
			proj.setEndHeight(25);
			proj.setStartDelay(40);
			proj.setEndDelay(75);
			proj.setLockon(c.getProjectileLockon());
			GlobalPackets.createProjectile(proj);
			ssTarget.startGraphic(Graphic.create(2264));
		}
	}

}
