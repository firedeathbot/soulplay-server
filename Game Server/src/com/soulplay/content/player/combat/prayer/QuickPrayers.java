package com.soulplay.content.player.combat.prayer;

import com.soulplay.game.model.player.Client;

public final class QuickPrayers {

	public static final int MAX_PRAYERS = 30;

	private static final int[] defPrayId = {67050, 67055, 67063, 67075, 67077,
			67078, 67082};

	private static final int[] strPrayId = {67051, 67056, 67064, 67075, 67077};

	private static final int[] atkPrayId = {67052, 67057, 67065, 67075, 67077};

	private static final int[] rangePrayId = {67053, 67061, 67070, 67078};

	private static final int[] magePrayId = {67054, 67062, 67071, 67082};

	private static final int[] headIconsId = {67066, // Summoning
			67067, // Mage
			67068, // range
			67069, // Melee
			67072, // Retribution
			67073, // Redemption
			67074 // Smite
	};

	public static int[][] data = {{67050, 630, 0}, {67051, 631, 1},
			{67052, 632, 2}, {67053, 633, 3}, {67054, 634, 4}, {67055, 635, 5},
			{67056, 636, 6}, {67057, 637, 7}, {67058, 638, 8}, {67059, 639, 9},
			{67060, 640, 10}, {67061, 641, 11}, {67062, 642, 12},
			{67063, 643, 13}, {67064, 644, 14}, {67065, 645, 15},
			{67066, 646, 16}, {67067, 647, 17}, {67068, 648, 18},
			{67069, 649, 19}, {67070, 650, 20}, {67071, 651, 21},
			{67072, 652, 22}, {67073, 653, 23}, {67074, 654, 24},
			{67075, 655, 25}, {67076, 656, 26}, {67077, 657, 27},
			{67078, 658, 28}, {67082, 662, 29}};// actionID, frameID

	public static void canBeSelected(Client c, int actionId) {
		boolean[] prayer = new boolean[MAX_PRAYERS];
		for (int j = 0; j < prayer.length; j++) {
			prayer[j] = true;
		}
		switch (actionId) {
			case 67050:
			case 67055:
			case 67063:
				for (int j = 0; j < defPrayId.length; j++) {
					if (defPrayId[j] != actionId) {
						prayer[CombatPrayer.defPray[j]] = false;
					}
				}
				break;

			case 67051:
			case 67056:
			case 67064:
				for (int j = 0; j < CombatPrayer.strPray.length; j++) {
					if (strPrayId[j] != actionId) {
						prayer[CombatPrayer.strPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.rangePray.length; j++) {
					if (rangePrayId[j] != actionId) {
						prayer[CombatPrayer.rangePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.magePray.length; j++) {
					if (magePrayId[j] != actionId) {
						prayer[CombatPrayer.magePray[j]] = false;
					}
				}
				break;

			case 67052:
			case 67057:
			case 67065:
				for (int j = 0; j < CombatPrayer.atkPray.length; j++) {
					if (atkPrayId[j] != actionId) {
						prayer[CombatPrayer.atkPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.rangePray.length; j++) {
					if (rangePrayId[j] != actionId) {
						prayer[CombatPrayer.rangePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.magePray.length; j++) {
					if (magePrayId[j] != actionId) {
						prayer[CombatPrayer.magePray[j]] = false;
					}
				}
				break;

			case 67053:
			case 67061:
			case 67070:
			case 67078:
				for (int j = 0; j < CombatPrayer.atkPray.length; j++) {
					if (atkPrayId[j] != actionId) {
						prayer[CombatPrayer.atkPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.strPray.length; j++) {
					if (strPrayId[j] != actionId) {
						prayer[CombatPrayer.strPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.rangePray.length; j++) {
					if (rangePrayId[j] != actionId) {
						prayer[CombatPrayer.rangePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.magePray.length; j++) {
					if (magePrayId[j] != actionId) {
						prayer[CombatPrayer.magePray[j]] = false;
					}
				}
				break;

			case 67054:
			case 67062:
			case 67071:
			case 67082:
				for (int j = 0; j < CombatPrayer.atkPray.length; j++) {
					if (atkPrayId[j] != actionId) {
						prayer[CombatPrayer.atkPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.strPray.length; j++) {
					if (strPrayId[j] != actionId) {
						prayer[CombatPrayer.strPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.rangePray.length; j++) {
					if (rangePrayId[j] != actionId) {
						prayer[CombatPrayer.rangePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.magePray.length; j++) {
					if (magePrayId[j] != actionId) {
						prayer[CombatPrayer.magePray[j]] = false;
					}
				}
				break;

			case 67066:
			case 67067:
			case 67068:
			case 67069:
			case 67072:
			case 67073:
			case 67074:
				for (int j = 0; j < CombatPrayer.headIcons.length; j++) {
					if (headIconsId[j] != actionId) {
						prayer[CombatPrayer.headIcons[j]] = false;
					}
				}
				break;

			case 67075:
				for (int j = 0; j < CombatPrayer.atkPray.length; j++) {
					if (atkPrayId[j] != actionId) {
						prayer[CombatPrayer.atkPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.strPray.length; j++) {
					if (strPrayId[j] != actionId) {
						prayer[CombatPrayer.strPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.rangePray.length; j++) {
					if (rangePrayId[j] != actionId) {
						prayer[CombatPrayer.rangePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.magePray.length; j++) {
					if (magePrayId[j] != actionId) {
						prayer[CombatPrayer.magePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.defPray.length; j++) {
					if (defPrayId[j] != actionId) {
						prayer[CombatPrayer.defPray[j]] = false;
					}
				}
				break;

			case 67077:
				for (int j = 0; j < CombatPrayer.atkPray.length; j++) {
					if (atkPrayId[j] != actionId) {
						prayer[CombatPrayer.atkPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.strPray.length; j++) {
					if (strPrayId[j] != actionId) {
						prayer[CombatPrayer.strPray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.rangePray.length; j++) {
					if (rangePrayId[j] != actionId) {
						prayer[CombatPrayer.rangePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.magePray.length; j++) {
					if (magePrayId[j] != actionId) {
						prayer[CombatPrayer.magePray[j]] = false;
					}
				}
				for (int j = 0; j < CombatPrayer.defPray.length; j++) {
					if (defPrayId[j] != actionId) {
						prayer[CombatPrayer.defPray[j]] = false;
					}
				}
				break;
		}
		for (int i = 0; i < MAX_PRAYERS; i++) {
			if (!prayer[i]) {
				c.quickPrayers.remove((Integer) i);
				for (int[] element : data) {
					if (i == element[2]) {
						c.getPacketSender().setConfig(element[1], 0);
					}
				}
			}
		}
	}

	public static void clickPray(Client c, int actionId) {
		canBeSelected(c, actionId);
		for (int[] element : data) {
			if (element[0] == actionId) {
				if (c.quickPrayers.contains(element[2])) {
					c.quickPrayers.remove((Integer) element[2]);
					c.getPacketSender().setConfig(element[1], 0);
				} else {
					// System.out.println("Prayer info: "+
					// Arrays.toString(data[j]));
					c.quickPrayers.add(element[2]);
					c.getPacketSender().setConfig(element[1], 1);
				}
			}
		}
	}

	public static void loadCheckMarks(Client player) {
		for (int[] element : data) {
			player.getPacketSender().setConfig(element[1],
					player.quickPrayers.contains(element[2]) ? 1 : 0);
		}
	}

}
