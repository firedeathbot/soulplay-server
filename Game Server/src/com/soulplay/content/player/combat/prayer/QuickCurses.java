package com.soulplay.content.player.combat.prayer;

import com.soulplay.content.player.PrayerBook;
import com.soulplay.game.model.player.Client;

public final class QuickCurses {

	public static final int MAX_CURSES = 20;

	private static final int[][] data = {{67050, 630, 0}, {67051, 631, 1},
			{67052, 632, 2}, {67053, 633, 3}, {67054, 634, 4}, {67055, 635, 5},
			{67056, 636, 6}, {67057, 637, 7}, {67058, 638, 8}, {67059, 639, 9},
			{67060, 640, 10}, {67061, 641, 11}, {67062, 642, 12},
			{67063, 643, 13}, {67064, 644, 14}, {67065, 645, 15},
			{67066, 646, 16}, {67067, 647, 17}, {67068, 648, 18},
			{67069, 649, 19}};// actionID, frameID, quickCurse = true

	public static void canBeSelected(Client c, int actionId) {
		boolean[] curse = new boolean[MAX_CURSES];
		for (int j = 0; j < curse.length; j++) {
			curse[j] = true;
		}
		switch (actionId) {
			case 67051:
				curse[10] = false;
				curse[19] = false;
				break;

			case 67052:
				curse[11] = false;
				curse[19] = false;
				break;

			case 67053:
				curse[12] = false;
				curse[19] = false;
				break;

			case 67054:
				curse[16] = false;
				break;

			case 67057:
				curse[8] = false;
				curse[9] = false;
				curse[17] = false;
				curse[18] = false;
				break;

			case 67058:
				curse[7] = false;
				curse[9] = false;
				curse[17] = false;
				curse[18] = false;
				break;

			case 67059:
				curse[7] = false;
				curse[8] = false;
				curse[17] = false;
				curse[18] = false;
				break;

			case 67060:
				curse[1] = false;
				curse[19] = false;
				break;

			case 67061:
				curse[2] = false;
				curse[19] = false;
				break;

			case 67062:
				curse[3] = false;
				curse[19] = false;
				break;

			case 67063:
			case 67064:
			case 67065: // Leech run-energy
				curse[19] = false;
				break;

			case 67066: // Leech special
				curse[4] = false;
				curse[19] = false;
				break;

			case 67067:
				for (int i = 7; i < 10; i++) {
					curse[i] = false;
				}
				curse[18] = false;
				break;

			case 67068:
				for (int i = 7; i < 10; i++) {
					curse[i] = false;
				}
				curse[17] = false;
				break;

			case 67069:
				for (int i = 1; i < 5; i++) {
					for (int j = 10; j < 17; j++) {
						curse[i] = false;
						curse[j] = false;
					}
				}
				break;
		}
		for (int i = 0; i < MAX_CURSES; i++) {
			if (!curse[i]) {
				c.quickCurses.remove((Integer) i);
				for (int[] element : data) {
					if (i == element[2]) {
						c.getPacketSender().setConfig(element[1], 0);
					}
				}
			}
		}
	}

	public static void clickCurse(Client c, int actionId) {
		canBeSelected(c, actionId);
		for (int[] element : data) {
			if (element[0] == actionId) {
				if (c.quickCurses.contains(element[2])) {
					c.quickCurses.remove((Integer) element[2]);
					c.getPacketSender().setConfig(element[1], 0);
				} else {
					c.quickCurses.add(element[2]);
					c.getPacketSender().setConfig(element[1], 1);
				}
			}
		}
	}

	public static void loadCheckMarks(Client player) {
		for (int[] element : data) {
			player.getPacketSender().setConfig(element[1],
					player.quickCurses.contains(element[2]) ? 1 : 0);
		}
	}

	public static void selectQuickInterface(Client c) {
		if (c.playerPrayerBook == PrayerBook.NORMAL) {
			QuickPrayers.loadCheckMarks(c);
		} else {
			loadCheckMarks(c);
		}

		c.getPacketSender().setSidebarInterface(5, c.playerPrayerBook == PrayerBook.NORMAL ? 17200 : 21000);
		c.getPacketSender().sendFrame106(5);
	}

}
