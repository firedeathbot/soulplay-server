package com.soulplay.content.player.combat.prayer;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.Sounds;

public enum Prayers {

	THICK_SKIN("Thick Skin", 1, 3, 83, -1, Sounds.PRAYER_THICK_SKIN, false), // 0
	BURST_OF_STRENGTH("Burst of Strength", 4, 3, 84, -1, Sounds.PRAYER_BURST_OF_STRENGTH, false), // 1
	CLARITY_OF_THOUGHT("Clarity of Thought", 7, 3, 85, -1, Sounds.PRAYER_CLARITY_OF_THOUGHT, false), // 2
	SHARP_EYE("Sharp Eye", 8, 3, 601, -1, Sounds.PRAYER_SHARP_EYE, false), // 3
	MYSTIC_WILL("Mystic Will", 9, 3, 602, -1, Sounds.PRAYER_MYSTIC_WILL, false), // 4
	ROCK_SKIN("Rock Skin", 10, 6, 86, -1, Sounds.PRAYER_ROCK_SKIN, false), // 5
	SUPERHUMAN_STRENGTH("Superhuman Strength", 13, 6, 87, -1, Sounds.PRAYER_SUPER_HUMAN_STRENGTH, false), // 6
	IMPROVED_REFLEXES("Improved Reflexes", 16, 6, 88, -1, Sounds.PRAYER_IMPROVED_REFLEXES, false), // 7
	RAPID_RESTORE("Rapid Restore", 19, 1, 89, -1, Sounds.PRAYER_RAPID_RESTORE, false), // 8
	RAPID_HEAL("Rapid Heal", 22, 2, 90, -1, Sounds.PRAYER_RAPID_HEAL, false), // 9
	PROTECT_ITEM("Protect Item", 25, 2, 91, -1, Sounds.PRAYER_PROTECT_ITEM, false), // 10
	HAWK_EYE("Hawk Eye", 26, 6, 603, -1, Sounds.PRAYER_HAWK_EYE, false), // 11
	MYSTIC_LORE("Mystic Lore", 27, 6, 604, -1, Sounds.PRAYER_MYSTIC_LORE, false), // 12
	STEEL_SKIN("Steel Skin", 28, 12, 92, -1, Sounds.PRAYER_STEEL_SKIN, false), // 13
	ULTIMATE_STRENGTH("Ultimate Strength", 31, 12, 93, -1, Sounds.PRAYER_ULTIMATE_STRENGTH, false), // 14
	INCREDIBLE_REFLEXES("Incredible Reflexes", 34, 12, 94, -1, Sounds.PRAYER_INCREDIBLE_REFLEX, false), // 15
	PROTECT_FROM_SUMMONING("Protect from Summoning", 35, 12, 609, 12, -1, true), // 16
	PROTECT_FROM_MAGIC("Protect from Magic", 37, 12, 95, 2, Sounds.PRAYER_PROTECT_FROM_MAGIC, false), // 17
	PROTECT_FROM_MISSILES("Protect from Missiles", 40, 12, 96, 1, Sounds.PRAYER_PROTECT_FROM_MISSLES, false), // 18
	PROTECT_FROM_MELEE("Protect from Melee", 43, 12, 97, 0, Sounds.PRAYER_PROTECT_FROM_MELEE, false), // 19
	EAGLE_EYE("Eagle Eye", 44, 12, 605, -1, Sounds.PRAYER_EAGLE_EYE, false), // 20
	MYSTIC_MIGHT("Mystic Might", 45, 12, 606, -1, Sounds.PRAYER_MYSTIC_MIGHT, false), // 21
	RETRIBUTION("Retribution", 46, 3, 98, 3, Sounds.PRAYER_RETRIBUTION, true), // 22
	REDEMPTION("Redemption", 49, 6, 99, 5, Sounds.PRAYER_REDEMPTION, true), // 23
	SMITE("Smite", 52, 18, 100, 4, Sounds.PRAYER_SMITE, true), // 24
	CHIVALRY("Chivalry", 60, 20, 607, -1, Sounds.CHIVALRY, true), // 25
	RAPID_RENEWAL("Rapid Renewal", 65, 2, 612, -1, -1, true), // 26
	PIETY("Piety", 70, 20, 608, -1, Sounds.PIETY, true), // 27
	RIGOUR("Rigour", 74, 20, 610, -1, -1, true), // 28
	AUGURY("Augury", 77, 20, 611, -1, -1, true); // 29

	public static final Prayers[] values = values();
	private final String prayerName;
	private final int levelRequirement, configCode, headIcon, drainRate, sound;
	private final boolean isMembers;

	Prayers(String prayerName, int levelRequirement, int drainRate, int configCode, int headIcon, int sound,
			boolean isMembers) {
		this.prayerName = prayerName;
		this.levelRequirement = levelRequirement;
		this.drainRate = drainRate;
		this.configCode = configCode;
		this.headIcon = headIcon;
		this.sound = sound;
		this.isMembers = isMembers;
	}

	public int getSound() {
		return sound;
	}

	public int getConfigCode() {
		return configCode;
	}

	public int getDrainRate() {
		return drainRate;
	}

	public int getHeadIcon() {
		return headIcon;
	}

	public int getLevelRequirement() {
		return levelRequirement;
	}

	public String getPrayerName() {
		return prayerName;
	}

	public boolean isMembers() {
		return isMembers;
	}

	public boolean isActive(Player player) {
		return player.prayerActive[ordinal()];
	}

}
