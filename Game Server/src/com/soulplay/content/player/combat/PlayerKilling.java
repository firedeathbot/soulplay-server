package com.soulplay.content.player.combat;

import com.soulplay.Server;
import com.soulplay.game.model.player.Client;

/**
 * @author Core Handles adding and removing hosts to the players array list.
 */
public class PlayerKilling {

	// private Client c;

	/**
	 * Constructor class
	 */

	// public PlayerKilling(Client client) {
	// this.c = client;
	// }

	public static final int KILL_WAIT_MAX = 4;

	/**
	 * Adds the host of the killed player.
	 *
	 * @param client
	 *            Player that saves the host.
	 * @param host
	 *            Host address of the killed player.
	 * @return True if the host is added to the players array.
	 */

	public static boolean addHostToList(Client client, String host) {
		if (client != null) {
			return client.lastKilledPlayers.add(host);
		}
		return false;
	}

	/**
	 * Checks if the host is already on the players array.
	 *
	 * @param client
	 *            Player that is adding the killed players host.
	 * @param host
	 *            Host address of the killed player.
	 * @return True if the host is on the players array.
	 */

	public static boolean hostOnList(Client client, String host) {
		int killAmount = KILL_WAIT_MAX;
		if (Server.getRegionManager().getLocalPlayers(client).size() > 7) {
			killAmount = 2;
		}
		if (client.lastKilledPlayers.size()
				- client.lastKilledPlayers.indexOf(host) >= killAmount) {
			// System.out.println("Size :"+client.lastKilledPlayers.size()+"
			// indexof:"+client.lastKilledPlayers.indexOf(host));
			// removeHostFromList(client, host);
			client.lastKilledPlayers.remove(host);
			return false;
		}
		return client.lastKilledPlayers.contains(host);
	}

	/*
	 * Amount of kills you have to wait before the host is deleted.
	 */

	/**
	 * Removes the host from the players array.
	 *
	 * @param client
	 *            Player that is removing the host.
	 * @param host
	 *            Host that is being removed.
	 * @return True if host is successfully removed.
	 */

	public static boolean removeHostFromList(Client client, String host) {
		if (client != null) {
			return client.lastKilledPlayers.remove(host);
		}
		return false;
	}
	
	public static void reset(Client c) {
		c.lastKilledPlayers.clear();
	}

}
