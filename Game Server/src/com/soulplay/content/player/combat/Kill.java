package com.soulplay.content.player.combat;

import com.soulplay.game.model.player.Client;

public class Kill {

	private String killerName;

	private int killerMID;

	private String victimName;

	private int victimMID;

	private String killerIP;

	private String victimIP;

	public Kill(Client killer, Client victim) {
		this.killerName = killer.getName();
		this.victimName = victim.getName();
		this.setKillerIP(killer.connectedFrom);
		this.setVictimIP(victim.connectedFrom);
		this.setKillerMID(killer.mySQLIndex);
		this.setVictimMID(victim.mySQLIndex);
	}

	public String getKillerIP() {
		return killerIP;
	}

	public int getKillerMID() {
		return killerMID;
	}

	public String getKillerName() {
		return killerName;
	}

	public String getVictimIP() {
		return victimIP;
	}

	public int getVictimMID() {
		return victimMID;
	}

	public String getVictimName() {
		return victimName;
	}

	public void setKillerIP(String killerIP) {
		this.killerIP = killerIP;
	}

	public void setKillerMID(int killerMID) {
		this.killerMID = killerMID;
	}

	public void setVictimIP(String victimIP) {
		this.victimIP = victimIP;
	}

	public void setVictimMID(int victimMID) {
		this.victimMID = victimMID;
	}
}
