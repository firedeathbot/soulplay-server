package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.click.object.ObjectFirstClick;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

/**
 * Sample bot combat task
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class MeleeEdgeManTask extends BotTask {

	private int manFocused = 0;

	@Override
	public boolean canBegin(BotClient bot) {
		return bot.getSkills().getStaticLevel(Skills.STRENGTH) > 0
				&& bot.getSkills().getStaticLevel(Skills.STRENGTH) < 50;
		// && !bot.getData().getTypeOfBot().equals("skiller");*/
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new MeleeEdgeManTask();
	}

	@Override
	public void process(BotClient bot) {
		if (bot.getCurrentHp() < 5/*
									 * ||
									 * bot.getLevelForXP(bot.playerLevel[Player.
									 * playerStrength]) >= 50
									 */) {
			if (bot.getUtils().getFoodCount() > 0) {
				bot.getUtils().eatFood();
			} else {
				stage = 6;
			}
		}
		switch (stage) {
			case 0:
				sleep(8000, 1000);
				stage++;
				break;
			case 1:
				bot.getUtils().teleHome();
				sleep(10000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3094, 3491);
				sleep(7000, 1000);
				bot.getUtils().bankAll();
				if (!bot.getUtils().getFood(20)) {
					if (!bot.getUtils().getFood(10)) {
						if (!bot.getUtils().getFood(5)) {
							if (!bot.getUtils().getFood(3)) {

							}
						}
					}
				}
				stage++;
				break;
			case 3:
				if (!(bot.getX() == 3101 && bot.getY() == 3509)) {
					bot.getPA().playerWalk(3101, 3509);
				}

				if (bot.getX() == 3101 && bot.getY() == 3509) {
					ObjectFirstClick.execute(bot, new GameObject(26910, Location.create(3101, 3509), 0, 10));
				}

				sleep(2000, 1000);
				if ((bot.getX() == 3101 && bot.getY() == 3509)
						&& bot.getPA().canWalkDir(-1, 0)) {
					stage++;
				} else {
				}
				break;
			case 4:
				if (manFocused >= 5) {
					stage = 6;
				}
				if (bot.npcIndex == 0) {
					if (bot.getSkills().getStaticLevel(Skills.ATTACK) > bot
							.getSkills().getStaticLevel(Skills.STRENGTH)) {
						bot.fightMode = 1;// 0-attack 1-str 2-def
					} else {
						bot.fightMode = 0;
					}
					if (bot.getSkills().getStaticLevel(Skills.ATTACK) > bot
							.getSkills().getStaticLevel(Skills.DEFENSE)) {
						bot.fightMode = 2;
					}
					if (bot.getSkills().getStaticLevel(Skills.ATTACK) > bot
							.getSkills().getStaticLevel(Skills.STRENGTH)
							&& (bot.getSkills().getStaticLevel(
									Skills.DEFENSE) > bot
											.getSkills().getStaticLevel(
													Skills.STRENGTH))) {
						bot.fightMode = 1;
					}
					bot.getUtils().attackNearestNPC("Man");
					manFocused++;
					sleep(2000, 1000);
				} else {
					stage++;
				}
				break;
			case 5:
				if (bot.npcIndex == 0) {
					stage--;
				}
				break;
			case 6:
				endTask(bot);
				break;
		}
	}

}
