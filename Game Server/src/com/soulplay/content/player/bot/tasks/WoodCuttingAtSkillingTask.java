package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.Woodcutting;
import com.soulplay.game.RSConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class WoodCuttingAtSkillingTask extends BotTask {

	private int woodCuttingType = 0;

	private int loopCount = 0;

	private int toolId = 0;

	private int[] treeIds = {1278, // 0 tree
			1278, // 1 normal tree
			1278, // 2 normal tree
			1308, // 3 willow tree
			1308, // 4 willow tree
			1308, // 5 willow tree
			1309, // 6 yew tree
			1309, // 7 yew tree
			1309, // 8 yew tree
			1306, // 9 magic tree
			1306, // 10 magic tree
			1306, // 11 magic tree
	};

	private int[][] treeLocations = {{3021, 3387}, // 0 tree
			{3019, 3387}, // 1 tree
			{3017, 3387}, // 2 tree
			{3014, 3389}, // 3 willow tree
			{3012, 3389}, // 4 willow tree
			{3010, 3389}, // 5 willow tree
			{3011, 3372}, // 6 yew tree
			{3011, 3372}, // 7 yew tree
			{3011, 3372}, // 8 yew tree
			{3013, 3386}, // 9 magic tree
			{3013, 3386}, // 10 magic tree
			{3013, 3386} // 11 magic tree
	};

	private int[][] walkLocations = {{3022, 3386}, // 0 tree
			{3020, 3386}, // 1 tree
			{3018, 3386}, // 2 tree
			{3015, 3388}, // 3 willow tree
			{3013, 3388}, // 4 willow tree
			{3011, 3388}, // 5 willow tree
			{3011, 3375}, // 6 yew tree
			{3011, 3371}, // 7 yew tree
			{3014, 3372}, // 8 yew tree
			{3015, 3386}, // 9 magic tree
			{3012, 3387}, // 10 magic tree
			{3013, 3385} // 11 magic tree
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new WoodCuttingAtSkillingTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("woodcutting " + stage);
		switch (stage) {

			case 0:
				sleep(10000, 1000); // 3000 is 3 seconds i believe
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3027, 3379, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				if (bot.getItems().playerHasItem(toolId)) {
					bot.getItems().deleteItemInOneSlot(toolId, 1);
				}
				bot.getPA().playerWalk(3028, 3379);
				sleep(10000, 1000);
				bot.getUtils().bankAll();
				loopCount++;
				stage++;

				if (loopCount > 2) {
					stage = 15;
				}

				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 1) {
					toolId = 1349;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 5) {
					toolId = 1353;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 21) {
					toolId = 1355;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 31) {
					toolId = 1357;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 41) {
					toolId = 1359;
				}
				// TODO: add more axe types for levels

				if (bot.getSkills().getStaticLevel(
						Skills.WOODCUTTING) >= 1) {
					woodCuttingType = 0 + Misc.random(2);
				}
				if (bot.getSkills().getStaticLevel(Skills.WOODCUTTING) >= 30
						&& bot.getSkills().getStaticLevel(RSConstants.FIREMAKING) >= 30) {
					woodCuttingType = 3 + Misc.random(2);
				}
				if (bot.getSkills().getStaticLevel(Skills.WOODCUTTING) >= 60
						&& bot.getSkills().getStaticLevel(RSConstants.FIREMAKING) >= 60) {
					woodCuttingType = 6 + Misc.random(2);
				}
				if (bot.getSkills().getStaticLevel(Skills.WOODCUTTING) >= 75
						&& bot.getSkills().getStaticLevel(RSConstants.FIREMAKING) >= 75) {
					woodCuttingType = 9 + Misc.random(2);
				}
				break;
			case 3:
				bot.getItems().addItem(toolId, 1);
				bot.getPA().playerWalk(walkLocations[woodCuttingType][0],
						walkLocations[woodCuttingType][1]);
				sleep(5000, 1000);
				stage++;
				break;
			case 4:
				if (ObjectManager.getObject(treeLocations[woodCuttingType][0],
						treeLocations[woodCuttingType][1], 0) != null) {
					return;
				}
				if (!bot.playerIsSkilling) {
					if (bot.getItems().freeSlots() < 1) {
						stage = 2;
						loopCount++;
					} else {
						// bot.turnPlayerTo(treeLocations[woodCuttingType][0],
						// treeLocations[woodCuttingType][1]);
						GameObject gO = new GameObject(treeIds[woodCuttingType],
								Location.create(treeLocations[woodCuttingType][0],
								treeLocations[woodCuttingType][1],
								bot.getHeightLevel()), 0, 10);
						Woodcutting.initNormalWc(bot, gO);
					}
				}
				sleep(2000, 1000);
				break;
			case 15:
				endTask(bot);
				break;
		}
	}

}
