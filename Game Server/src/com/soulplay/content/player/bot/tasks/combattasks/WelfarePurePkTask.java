package com.soulplay.content.player.bot.tasks.combattasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.items.Food;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class WelfarePurePkTask extends BotTask {

	private boolean setupComplete = false;

	private int maxHit = 35;

	private boolean risking = false;

	private boolean fightToDeath = true;

	private boolean retarget = true;

	private boolean activatedSpec = false;

	private long specProdDelay = 0;

	private int regularWeapon = 0;

	private int shield = 0;

	private boolean bonusesTooHigh = false;

	private boolean saidGl = false;

	private boolean previouslyUnderAttack = false;

	private int otherPlayerDidntAttackYouTimer = 0;

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public void endTask(BotClient bot) {
		if (!bot.isDead()) {
			if (regularWeapon > 0 && bot.getItems().wearItem(regularWeapon,
					bot.getItems().getItemSlot(regularWeapon))) {
			}
			if (shield > 0 && bot.getItems().wearItem(shield,
					bot.getItems().getItemSlot(shield))) {
			}
			bot.getPA().playerWalk(3095, 3515);
		}
		if (bot.isDead()) {
			bot.speak("Gf");
		}
		bot.followId = 0;
		bot.resetFace();
		bot.underAttackBy = 0;
		bot.setPlayerToKill(null);
		bot.curses().resetCurse();
		bot.setTypeOfBot(13);
		bot.setAction(null);
	}

	private int getSpecWeapon(final BotClient bot) {
		if (bot.getItems().playerHasItem(5698)) { // dragon dagger
			return 5698;
		}
		if (bot.getItems().playerHasItem(4153)) { // granite maul
			return 4153;
		}
		return -1;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new WelfarePurePkTask();
	}

	@Override
	public void process(BotClient bot) {

		if (!setupComplete) {
			if (Misc.random(3) == 1) {
				fightToDeath = false;
			}
			if (bot.getUtils().getFoodCount() < 11) {
				fightToDeath = false;
			}
			regularWeapon = bot.playerEquipment[PlayerConstants.playerWeapon];
			shield = bot.playerEquipment[PlayerConstants.playerShield];
			setupComplete = true;

		}
		if (bot.isDead() || bot.getHitPoints() < 1
				|| (bot.getX() == LocationConstants.EDGEVILLE_X
						&& bot.getY() == LocationConstants.EDGEVILLE_Y
						&& bot.getZ() == 0)) { // bot is dead
			// System.out.println("Ended task.");
			if (bot.isDead()) {
				if (Misc.random(1) == 0) {
					bot.speak("Gf");
				}
			}
			endTask(bot);
			return;
		}
		if (bot.getPlayerToKill() == null) {
			// System.out.println("Ended task2.");
			endTask(bot);
			return;
		}
		if (bot.getPlayerToKill().isDead() || bot.getPlayerToKill().wildLevel < 1
				|| bot.isDead() || bot.wildLevel < 1) {
			if (bot.isDead()) {
				if (Misc.random(1) == 0) {
					bot.speak("Gf");
				}
			}
			endTask(bot);
			return;
		}

		if (bot.playerEquipment[PlayerConstants.playerChest] < 1) {
			bot.setPlayerToKill(null);
			endTask(bot);
			return;
		}

		if (bot.getFreezeTimer() > 0 && bot.getUtils().getFoodCount() < 1) {
			if (Misc.random(5) < 4) {
				if (Misc.random(2) == 0) {
					bot.speak("...");
				}
			} else {
				if (Misc.random(2) == 0) {
					bot.speak("Homo");
				}
			}
			bot.getPA().startTeleport(LocationConstants.EDGEVILLE_X,
					LocationConstants.EDGEVILLE_Y, 0, TeleportType.TAB);
			endTask(bot);
			return;
		}

		if (bonusesTooHigh && !fightToDeath
				&& bot.getUtils().getFoodCount() < 2) {
			bot.getPA().startTeleport(LocationConstants.EDGEVILLE_X,
					LocationConstants.EDGEVILLE_Y, 0, TeleportType.TAB);
			endTask(bot);
			return;
		}

		if (bonusesTooHigh && !fightToDeath
				&& (bot.getPlayerToKill().playerIndex == 0
						|| bot.getPlayerToKill().playerIndex != bot.getId())) {
			otherPlayerDidntAttackYouTimer++;
			if (otherPlayerDidntAttackYouTimer > 10) {
				bot.getCombat().resetPlayerAttack();
				if (bot.huntPlayerIndex > 0) {
					bot.getPA().playerWalk(bot.getX(), 3502);
				}
				sleep(1000, 1000);
				if (bot.getFreezeTimer() > 0) {
					bot.getPA().startTeleport(LocationConstants.EDGEVILLE_X,
							LocationConstants.EDGEVILLE_Y, 0, TeleportType.TAB);
				}
				if (bot.wildLevel < 1) {
					endTask(bot);
				}
				if (bot.huntPlayerIndex < 0) {
					endTask(bot);
				}
				return;
			}
		}

		if (bot.getPlayerToKill().getAttackTimer() > 1
				&& bot.getPlayerToKill().playerIndex == bot.getId()) {
			otherPlayerDidntAttackYouTimer = 0;
		}

		// if (!bonusesTooHigh && fightToDeath &&
		// (bot.getPlayerToKill().getItems().playerHasEquipped(13740) ||
		// bot.getPlayerToKill().getItems().playerHasEquipped(13742))) {
		// fightToDeath = false;
		// bonusesTooHigh = true;
		// bot.speak("Off");
		// }

		if (!bonusesTooHigh && fightToDeath
				&& (bot.getCombat()
						.getBonusDifference(bot.getPlayerToKill()) < -500
						|| bot.getCombat().getBonusDifference(
								bot.getPlayerToKill()) > 500)) { // TODO: maybe
																	// ! the
																	// whole
																	// parenthesis
			fightToDeath = false;
			bonusesTooHigh = true;
			bot.speak("Off");
		}

		if (!bonusesTooHigh) {
			bot.getUtils().skull();
		}

		if ((bot.getPlayerToKill().getItems().playerHasEquipped(14484)
				|| bot.getPlayerToKill().getItems().playerHasEquipped(19784))
				&& bot.getPlayerToKill().lastHitWasSpec
				&& bot.getHitPoints() < 50) {
			bot.getUtils().eatFood();
			fightToDeath = false;
		}

		if (bot.playerPrayerBook == PrayerBook.NORMAL) { // set to curse prayers
			bot.playerPrayerBook = PrayerBook.CURSES;
		}

		// maxHit = 45;//bot.getPlayerToKill().lastDamageDealt;
		if (bot.getPlayerToKill().lastDamageDealt > maxHit) {
			maxHit = bot.getPlayerToKill().lastDamageDealt;
		}

		if (bot.underAttackBy > 0) { // prayer activation/deactivation and veng
			previouslyUnderAttack = true;
			if (!bot.vengOn
					&& System.currentTimeMillis() - bot.lastCast > 30000) {
				bot.getPA().castVeng(false);
			}
			if (bot.getPlayerLevel()[RSConstants.PRAYER] > 0) {
				if (bot.getPlayerToKill().curseActive[Curses.DEFLECT_MELEE.ordinal()]
						|| bot.getPlayerToKill().prayerActive[Prayers.PROTECT_FROM_MELEE
								.ordinal()]) {
					if (bot.getPlayerToKill().lastDamageType == 0) { // melee
																		// damage
						if (!bot.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
							bot.curses()
									.activateCurse(CursesPrayer.DEFLECT_MELEE);
						}
					} else if (bot.getPlayerToKill().lastDamageType == 1) { // range
																			// hit
																			// damage
						if (!bot.curseActive[Curses.DEFLECT_MISSILES.ordinal()]) {
							bot.curses().activateCurse(
									CursesPrayer.DEFLECT_MISSILES);
						}
					} else if (bot.getPlayerToKill().lastDamageType == 2) { // magic
																			// hit
																			// damage
						if (!bot.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
							bot.curses()
									.activateCurse(CursesPrayer.DEFLECT_MAGIC);
						}
					}
				} else if (!bot.curseActive[Curses.SOUL_SPLIT.ordinal()]) {
					bot.curses().activateCurse(CursesPrayer.SOUL_SPLIT);
				}

				if (!bot.curseActive[Curses.TURMOIL.ordinal()]) {
					bot.curses().activateCurse(CursesPrayer.TURMOIL);
				}
				if (!bot.curseActive[Curses.BERSERKER.ordinal()]) {
					bot.curses().activateCurse(CursesPrayer.BERSERKER);
				}
			} else { // no prayer so drink restore pot
				for (int i = 3030; i > 3024; i--) {
					if (bot.getItems().playerHasItem(i)) {
						bot.getPotions().handlePotion(i,
								bot.getItems().getItemSlot(i));
					}
					i--;
				}
			}
			if (bot.getPlayerLevel()[RSConstants.PRAYER] < 30) { // drink prayer
															// potions
				for (int i = 3030; i > 3022; i--) {
					if (bot.getItems().playerHasItem(i)) {
						bot.getPotions().handlePotion(i,
								bot.getItems().getItemSlot(i));
						break;
					}
					i--;
				}
			}

			// drink super potions for fight
			if (bot.getPlayerLevel()[RSConstants.STRENGTH] < 114) { // drink super
																// strength
																// potions
				for (int i = 2440; i > 156; i--) {
					if (bot.getItems().playerHasItem(i)) {
						bot.getPotions().handlePotion(i,
								bot.getItems().getItemSlot(i));
						break;
					}
					if (i == 2440) {
						i = 163;
					}
					i--;
				}
			}
			if (bot.getPlayerLevel()[RSConstants.ATTACK] < 114) { // drink super
																// attack
																// potions
				for (int i = 2436; i > 144; i--) {
					if (bot.getItems().playerHasItem(i)) {
						bot.getPotions().handlePotion(i,
								bot.getItems().getItemSlot(i));
						break;
					}
					if (i == 2436) {
						i = 151;
					}
					i--;
				}
			}
			if (bot.getPlayerLevel()[RSConstants.DEFENCE] < 114) { // drink super
																// strength
																// potions
				for (int i = 2442; i > 162; i--) {
					if (bot.getItems().playerHasItem(i)) {
						bot.getPotions().handlePotion(i,
								bot.getItems().getItemSlot(i));
						break;
					}
					if (i == 2442) {
						i = 169;
					}
					i--;
				}
			}

			if (!saidGl && !bonusesTooHigh
					&& bot.getPlayerToKill().getId() != bot.playerIndex) {
				if (Misc.random(1) == 0) {
					bot.speak("Gl");
				}
				saidGl = true;
			}

		} else {
			if (previouslyUnderAttack) {
				endTask(bot);
				return;
			}
			bot.curses().resetCurse();
		}

		if (Food.canEat(bot)
				&& (Misc.random(5) == 1 || bot.getPlayerToKill().lastHitWasSpec
						|| bonusesTooHigh
						|| bot.getPlayerToKill().lastDamageType == 2)
				&& bot.getCurrentHp() < 40 + Misc.random(5)) {
			if (bot.getUtils().getFoodCount() > 0) {
				bot.getUtils().eatFood();
			} else {
				if (!fightToDeath) {
					retarget = false;
					bot.getCombat().resetPlayerAttack();
					bot.setPlayerToKill(null);
					if (bot.wildLevel > 0
							&& (!bot.getItems().playerHasItem(563, 1)
									&& !bot.getItems().playerHasItem(556, 5))) {
						bot.getPA().playerWalk(LocationConstants.EDGEVILLE_X,
								LocationConstants.EDGEVILLE_Y);
					} else {
						bot.getPA().spellTeleport(LocationConstants.EDGEVILLE_X,
								LocationConstants.EDGEVILLE_Y, 0);
					}
					endTask(bot);
					return;
				}
			}
		}

		if (Food.canEat(bot) && !risking
				&& (Misc.random(3) == 1 || bot.getPlayerToKill().lastHitWasSpec
						|| bonusesTooHigh
						|| bot.getPlayerToKill().lastDamageType == 2)
				&& bot.getPlayerToKill().getAttackTimer() > 2
				&& bot.getCurrentHp() <= maxHit) { // eating food and running
													// away
			if (bot.getUtils().getFoodCount() > 0) {
				bot.getUtils().eatFood();
			} else {
				if (!fightToDeath) {
					retarget = false;
					bot.getCombat().resetPlayerAttack();
					bot.setPlayerToKill(null);
					if (bot.wildLevel > 0
							&& (!bot.getItems().playerHasItem(563, 1)
									&& !bot.getItems().playerHasItem(556, 5))) {
						bot.getPA().playerWalk(LocationConstants.EDGEVILLE_X,
								LocationConstants.EDGEVILLE_Y);
					} else {
						bot.getPA().spellTeleport(LocationConstants.EDGEVILLE_X,
								LocationConstants.EDGEVILLE_Y, 0);
					}
					endTask(bot);
					return;
				}
			}
		}

		// if index was reset, target the player again
		if (retarget && bot.playerIndex < 1) {
			if (bot.huntPlayerIndex > 0
					&& bot.playerIndex != bot.huntPlayerIndex
					&& PlayerHandler.players[bot.huntPlayerIndex] != null
					&& bot.underAttackBy < 1) {
				bot.setPlayerToKill(
						(Client) PlayerHandler.players[bot.huntPlayerIndex]);
				bot.playerIndex = bot.getPlayerToKill().getId();
			}
			if (bot.underAttackBy > 0 && bot.playerIndex != bot.underAttackBy
					&& PlayerHandler.players[bot.underAttackBy] != null) {
				bot.setPlayerToKill(
						(Client) PlayerHandler.players[bot.underAttackBy]);
				bot.playerIndex = bot.getPlayerToKill().getId();
			}
		}

		if (retarget && bot.playerIndex > 0) {
			if (bot.huntPlayerIndex > 0
					&& bot.playerIndex != bot.huntPlayerIndex
					&& PlayerHandler.players[bot.huntPlayerIndex] != null
					&& bot.underAttackBy < 1) {
				bot.setPlayerToKill(
						(Client) PlayerHandler.players[bot.huntPlayerIndex]);
				bot.playerIndex = bot.getPlayerToKill().getId();
			}
			if (bot.underAttackBy > 0 && bot.playerIndex != bot.underAttackBy
					&& PlayerHandler.players[bot.underAttackBy] != null) {
				bot.setPlayerToKill(
						(Client) PlayerHandler.players[bot.underAttackBy]);
				bot.playerIndex = bot.getPlayerToKill().getId();
			}
		}

		// switch back to regular weapon after spec
		if (activatedSpec && !bot.usingSpecial) {
			// bot.speak("Prod! Haha");
			if (regularWeapon > 0 && shield < 1 && bot.getItems().wearItem(
					regularWeapon, bot.getItems().getItemSlot(regularWeapon))) {
				activatedSpec = false;
			}
			if (regularWeapon > 0 && shield > 0 && bot.getItems().wearItem(
					regularWeapon, bot.getItems().getItemSlot(regularWeapon))) {
				// activatedSpec = false;
			}
			if (shield > 0 && bot.getItems().wearItem(shield,
					bot.getItems().getItemSlot(shield))) {
				activatedSpec = false;
			}
		}

		// predict the damage dealt then activate spec weapon
		SpecialAttack spec = SpecialAttack.get(getSpecWeapon(bot));
		if ((System.currentTimeMillis() - specProdDelay) > 5000
				&& bot.getAttackTimer() > 2 && getSpecWeapon(bot) > 0
				&& spec != null && spec.canActivate(new BattleState(bot, null))
				&& (bot.getPlayerToKill().getHitPoints()
						- bot.lastDamageDealt < 50
						|| bot.getPlayerToKill().getHitPoints() < 25)) {

			int specWeapon = getSpecWeapon(bot);
			if (bot.getItems().wearItem(specWeapon,
					bot.getItems().getItemSlot(specWeapon))) {
				bot.usingSpecial = true;
				activatedSpec = true;
				specProdDelay = System.currentTimeMillis();
				if (specWeapon == 4153) {
					bot.usingSpecial = false;
					bot.getCombat().handleGmaulPlayer(null);
					bot.getCombat().handleGmaulPlayer(null);
				}
				sleep(1500, 1000);
			}
		}

		if (bot.wildLevel < 5) {
			endTask(bot);
		}

	}

}
