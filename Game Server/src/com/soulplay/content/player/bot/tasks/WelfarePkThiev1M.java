package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.click.object.ObjectSecondClick;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class WelfarePkThiev1M extends BotTask {

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new WelfarePkThiev1M();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("theiving " + stage);

		if (bot.underAttackBy2 > 0) {
			// bot.getPA().playerWalk(3092 + Misc.random(5), 3482 +
			// Misc.random(4));
			// bot.getPA().startTeleport(3210, 3424, 0, TeleportType.MODERN);
			stage = 8;
		}

		// if (bot.playerName.equalsIgnoreCase("huifang")) {
		//// System.out.println("test "+stage);
		// }

		if (bot.addStarter) {
			stage = 11;
			bot.addStarter = false;
		}

		if (bot.getItems().playerHasItem(995, 1000000) && (bot.getX() != 3097
				&& bot.getY() != 3515)/* || bot.getCurrentHp() < 5 */) {
			stage = 9;
		}

		if (bot.getPlayerLevel()[RSConstants.HITPOINTS] < bot.getMaxHp()
				&& bot.getUtils().getFoodCount() > 0) {
			bot.getUtils().eatFood();
		}
		// if (bot.playerName.equalsIgnoreCase("knopp")) {
		// System.out.println("has task "+stage);
		// }

		switch (stage) {

			case 0:
				if (bot.getUtils().getFoodCount() >= 8
						&& bot.playerEquipment[PlayerConstants.playerChest] > 0) {
					stage = 11;
				} else {

					if (bot.underAttackBy > 0 || bot.isDead()) {
						if (bot.getFreezeTimer() > 0) {
							bot.getPA().startTeleport(
									LocationConstants.EDGEVILLE_X,
									LocationConstants.EDGEVILLE_Y, 0,
									TeleportType.TAB);
						} else if (!bot.isMoving && bot.wildLevel > 0) {
							bot.getPA().startTeleport(
									LocationConstants.EDGEVILLE_X,
									LocationConstants.EDGEVILLE_Y, 0,
									TeleportType.TAB);
						}
						sleep(1000, 1000);
						return;
					}
					sleep(10000, 1000);
					stage++;
				}
				break;
			case 1:
				bot.getPlayerLevel()[RSConstants.HITPOINTS] = 99;
				bot.getDotManager().reset();
				bot.getUtils().clearInvAndEquip();
				bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				// bot.getPA().playerWalk(3094, 3491);
				// sleep(7000, 1000);
				// bot.getUtils().bankAll();
				stage++;
				break;
			case 3:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 25) {
					stage = 4;
					return;
				}
				if (!(bot.getX() == 3094 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3094, 3501);
					sleep(3500, 1000);
				}

				if (bot.getX() == 3094 && bot.getY() == 3501) {			
					ObjectSecondClick.execute(bot, new GameObject(4874, Location.create(3094, 3500, bot.getZ()), 0, 10));					
				}
				sleep(3500, 1000);
				break;
			case 4:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 50) {
					stage = 5;
					return;
				}
				if (!(bot.getX() == 3095 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3095, 3501);
					sleep(3500, 1000);
				}

				if (bot.getX() == 3095 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4875, Location.create(3095, 3500, bot.getZ()), 0, 10));
					sleep(3500, 1000);
				}
				break;
			case 5:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 75) {
					stage = 6;
					return;
				}
				if (!(bot.getX() == 3096 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3096, 3501);
					sleep(3500, 1000);
				}

				if (bot.getX() == 3096 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4876, Location.create(3096, 3500, bot.getZ()), 0, 10));
					sleep(3500, 1000);
				}
				break;
			case 6:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 90) {
					stage = 7;
					return;
				}
				if (!(bot.getX() == 3097 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3097, 3501);
					sleep(3500, 1000);
				}

				if (bot.getX() == 3097 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4877, Location.create(3097, 3500, bot.getZ()), 0, 10));
					sleep(3500, 1000);
				}
				break;
			case 7:
				if (!(bot.getX() == 3098 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3098, 3501);
					sleep(3500, 1000);
				}

				if (bot.getX() == 3098 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4878, Location.create(3098, 3500, bot.getZ()), 0, 10));
					sleep(3500, 1000);
				}
				break;
			case 8:
				if (bot.getY() < 3489) {
					bot.getPA().playerWalk(3094 + Misc.random(4), 3501);
				} else {
					bot.getPA().playerWalk(3092 + Misc.random(10),
							3483 + Misc.random(4));
				}
				sleep(10000, 1000);
				stage = 1;
				break;
			case 9:
				if ((bot.getX() != 3097
						/* 3080 */ && bot.getY() != 3515/* 3509 */)) {
					bot.getPA().playerWalk(3097, 3515);
					return;
				}
				stage = 10;
				sleep(1000, 1000);
				break;
			case 10:
				bot.getUtils().clearInvAndEquip();
				bot.getUtils().welfareStarterSet();
				sleep(5000, 1000);
				stage = 11;
				break;
			case 11:
				bot.getPA().playerWalk(3068 + Misc.random(40),
						3524 + Misc.random(17));
				sleep(1500, 1000);
				if (bot.wildLevel > 0) {
					stage = 12;
				}
				break;
			case 12:
				if (bot.wildLevel < 4) {
					stage = 0;
					return;
				}
				if (bot.getUtils().getFoodCount() < 8) {
					stage = 0;
					bot.getPA().startTeleport(LocationConstants.EDGEVILLE_X,
							LocationConstants.EDGEVILLE_Y, 0, TeleportType.TAB);
					return;
				}
				if (bot.huntPlayerIndex > 0
						&& PlayerHandler.players[bot.huntPlayerIndex] != null
						&& PlayerHandler.players[bot.huntPlayerIndex].wildLevel > 0) {
					bot.setPlayerToKill(
							(Client) PlayerHandler.players[bot.huntPlayerIndex]);
					bot.setTypeOfBot(7);
					endTask(bot);
					return;
				}
				if (bot.underAttackBy > 0
						&& PlayerHandler.players[bot.underAttackBy] != null
						&& PlayerHandler.players[bot.underAttackBy].wildLevel > 0) {
					bot.setPlayerToKill(
							(Client) PlayerHandler.players[bot.underAttackBy]);
					bot.setTypeOfBot(7);
					endTask(bot);
					return;
				}
				break;
		}
	}

}
