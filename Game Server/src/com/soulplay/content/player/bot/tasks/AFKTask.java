package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.util.Misc;

public class AFKTask extends BotTask {

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new AFKTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("afk " + stage);
		switch (stage) {
			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				int random = Misc.random(10);
				if (random == 0) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 2;
				} else if (random == 1) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 3;
				} else if (random == 2) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 4;
				} else if (random == 3) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 5;
				} else if (random == 4) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 6;
				} else if (random == 5) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 7;
				} else if (random == 6) {
					bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 8;
				} else if (random == 7) {
					bot.getPA().startTeleport(3027, 3379, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 9;
				} else if (random == 8) {
					bot.getPA().startTeleport(3027, 3379, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 10;
				} else if (random == 9) {
					bot.getPA().startTeleport(3027, 3379, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 11;
				} else if (random == 10) {
					bot.getPA().startTeleport(3027, 3379, 0, TeleportType.MODERN);
					sleep(8000, 1000);
					stage = 12;
				}
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3091 + Misc.random(3),
						3489 + Misc.random(9));
				stage = 13;
				break;
			case 3:
				bot.getPA().playerWalk(3085 + Misc.random(4),
						3485 + Misc.random(34));
				stage = 13;
				break;
			case 4:
				bot.getPA().playerWalk(3085 + Misc.random(4),
						3485 + Misc.random(34));
				stage = 13;
				break;
			case 5:
				bot.getPA().playerWalk(3085 + Misc.random(4),
						3485 + Misc.random(34));
				stage = 13;
				break;
			case 6:
				bot.getPA().playerWalk(3085 + Misc.random(4),
						3485 + Misc.random(34));
				sleep(500000, 1000);
				stage = 13;
				break;
			case 7:
				bot.getPA().playerWalk(3085 + Misc.random(4),
						3485 + Misc.random(34));
				sleep(500000, 1000);
				stage = 13;
				break;
			case 8:
				bot.getPA().playerWalk(3095, 3515);
				sleep(500000, 1000);
				stage = 13;
				break;
			case 9:
				bot.getPA().playerWalk(3028, 3379);
				sleep(500000, 1000);
				stage = 13;
				break;
			case 10:
				bot.getPA().playerWalk(3028, 3382);
				sleep(500000, 1000);
				stage = 13;
				break;
			case 11:
				bot.getPA().playerWalk(3027 + Misc.random(1),
						3382 - Misc.random(7));
				sleep(500000, 1000);
				stage = 13;
				break;
			case 12:
				bot.getPA().playerWalk(3027 + Misc.random(1),
						3382 + Misc.random(7));
				sleep(500000, 1000);
				stage = 13;
				break;
			case 13:
				sleep(500000, 1000);
				stage++;
				break;
			case 14:
				endTask(bot);
				break;
		}
	}

}
