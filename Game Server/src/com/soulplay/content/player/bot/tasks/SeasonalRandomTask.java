package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.game.LocationConstants;
import com.soulplay.util.Misc;

public class SeasonalRandomTask extends BotTask {

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new SeasonalRandomTask();
	}

	@Override
	public void process(BotClient bot) {
		switch (stage) {
			case 0:
				if (!bot.inVarrock()) {
					bot.getPA().startTeleport(LocationConstants.VARROCK_X, LocationConstants.VARROCK_Y, 0, TeleportType.MODERN);
					sleep(10000);
					return;
				}

				bot.destinationX = 3177 + Misc.random(60);
				bot.destinationY = 3403 + Misc.random(50);
				sleep(20000, 10000);
				if (Misc.randomBoolean(10)) {
					stage++;
				}
				break;
			case 1:
				bot.getPA().startTeleport(0, 0, 0, TeleportType.MODERN);
				sleep(20000, 10000);
				stage++;
				break;
			case 2:
				if (!bot.inVarrock()) {
					bot.getPA().startTeleport(LocationConstants.VARROCK_X, LocationConstants.VARROCK_Y, 0, TeleportType.MODERN);
					sleep(10000);
					return;
				}
				break;
		}
	}

}
