package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.util.Misc;

public class SlotMachineTask extends BotTask {

	private int loopCount = 0;

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new SlotMachineTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("stage " + stage);

		switch (stage) {
			case 0:
				sleep(10000, 1000); // 3000 is 3 seconds i believe
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3097, 3496);
				sleep(9000, 1000);
				bot.getUtils().bankAll();
				stage++;
				break;
			case 3:
				bot.getPA().playerWalk(3106, 3498 + Misc.random(1));
				sleep(5000, 1000);
				stage++;
				break;
			case 4:
				bot.faceLocation(3107, 3499);
				bot.startAnimation(883);
				sleep(3500, 1000);
				loopCount++;
				stage++;
				break;
			case 5:
				if (loopCount == 28) {
					stage = 6;
				} else if (loopCount < 28) {
					stage = 4;
				}
				break;
			case 6:
				endTask(bot);
				break;
		}
	}

}
