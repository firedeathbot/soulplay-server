package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.click.object.ObjectSecondClick;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

/**
 * Sample bot combat task
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class ThievingTask extends BotTask {

	private int stallsFocused = 0;

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new ThievingTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("theiving " + stage);

		if (bot.underAttackBy2 != 0) {
			// bot.getPA().playerWalk(3092 + Misc.random(5), 3482 +
			// Misc.random(4));
			bot.getPA().startTeleport(3210, 3424, 0, TeleportType.MODERN);
			stage = 8;
		}

		if (stallsFocused >= 500 || bot.getCurrentHp() < 5) {
			stage = 9;
		}

		switch (stage) {

			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3094, 3491);
				sleep(7000, 1000);
				bot.getUtils().bankAll();
				stage++;
				break;
			case 3:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 25) {
					stage = 4;
					return;
				}
				if (!(bot.getX() == 3094 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3094, 3501);
				}

				if (bot.getX() == 3094 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4874, Location.create(3094, 3500, bot.getZ()), 0, 10));
				}
				stallsFocused++;
				sleep(3500, 1000);
				break;
			case 4:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 50) {
					stage = 5;
					return;
				}
				if (!(bot.getX() == 3095 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3095, 3501);
				}

				if (bot.getX() == 3095 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4875, Location.create(3095, 3500, bot.getZ()), 0, 10));
				}
				stallsFocused++;
				sleep(3500, 1000);
				break;
			case 5:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 75) {
					stage = 6;
					return;
				}
				if (!(bot.getX() == 3096 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3096, 3501);
				}

				if (bot.getX() == 3096 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4876, Location.create(3096, 3500, bot.getZ()), 0, 10));
				}
				stallsFocused++;
				sleep(3500, 1000);
				break;
			case 6:
				if (bot.getSkills().getStaticLevel(Skills.THIEVING) >= 90) {
					stage = 7;
					return;
				}
				if (!(bot.getX() == 3097 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3097, 3501);
				}

				if (bot.getX() == 3097 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4877, Location.create(3097, 3500, bot.getZ()), 0, 10));
				}
				stallsFocused++;
				sleep(3500, 1000);
				break;
			case 7:
				if (!(bot.getX() == 3098 && bot.getY() == 3501)) {
					bot.getPA().playerWalk(3098, 3501);
				}

				if (bot.getX() == 3098 && bot.getY() == 3501) {
					ObjectSecondClick.execute(bot, new GameObject(4878, Location.create(3098, 3500, bot.getZ()), 0, 10));
				}
				stallsFocused++;
				sleep(3500, 1000);
				break;
			case 8:
				sleep(10000, 1000);
				stage = 1;
				break;
			case 9:
				// System.out.println("Ended task for bot "+bot.playerName);
				endTask(bot);
				break;
		}
	}

}
