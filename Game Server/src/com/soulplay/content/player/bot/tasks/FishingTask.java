package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Cooking;
import com.soulplay.content.player.skills.Fishing;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.util.Misc;

public class FishingTask extends BotTask {

	private int fishingType = 0;

	private int loopCount = 0;

	private boolean cookingStage = false;

	private boolean startedCooking = false;

	private int[][] fishTypes = {{317, 321}, // 0 shrimp+anchovies
			{359, 371}, // 1 tuna+swordies
			{377, -1}, // 2 lobsters
			{383, -1}, // 3 shark
			{389, -1} // 4 manta
			// {15270, -1} // 5 rocktails
	};

	private int[][] fishLocations = {{3022, 3378}, // 0 shrimps+bait, firstlc
			{3017, 3383}, // 1 Tuna + Swordie / net+harpoon
			{3017, 3383}, // 2 Lobsters / cage+harpoon
			{3016, 3376}, // 3 sharks / net+harpoon
			{3014, 3381} // 4 manta / net+bait
			// {3021, 3384}, // 5 rocktails / rocktail shoal
	};

	private int[] fishingTools = {303, // 0 shrimps
			311, // 1 tuna swordie
			301, // 2 lobsters
			311, // 3 sharks
			305, // 4 manta
			// 307, // 5 rocktails
	};

	private int[] fishingNpc = { // i know its not npc id but w/e just named it
									// anything for now
			1, // 0 shrimp
			7, // 1 tuna swordie
			8, // 2 lobsters
			10, // 3 sharks
			12, // 4 manta
			// 13, // 5 rocktails
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new FishingTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("stage " + stage);
		// System.out.println("Stage 1
		// "+bot.getItems().playerHasItem(fishTypes[fishingType][1], 1));
		if (cookingStage) {
			if (startedCooking) {
				if (!bot.isCooking) {
					if (bot.getItems().playerHasItem(fishTypes[fishingType][0],
							1)) {
						Cooking.cookThisFood(bot, fishTypes[fishingType][0],
								2728, null);
					} else if (fishTypes[fishingType][1] != -1 && bot.getItems()
							.playerHasItem(fishTypes[fishingType][1], 1)) {
						Cooking.cookThisFood(bot, fishTypes[fishingType][1],
								2728, null);
					} else {
						stage = 4;
						startedCooking = false;
						cookingStage = false;
					}
				}
			}
		} else if (stage == 11) {
			if (bot.getItems().freeSlots() < 1) {
				stage = 2;
				if (loopCount > 2) {
					stage = 7;
				}
			} else if (!bot.playerSkilling[10]) {
				stage = 6;
			}
		}
		switch (stage) {
			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3027, 3379, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				if (bot.getItems().playerHasItem(fishTypes[fishingType][0], 1)
						|| bot.getItems()
								.playerHasItem(fishTypes[fishingType][1], 1)) {
					cookingStage = true;
					bot.getPA().playerWalk(3030, 3381 + Misc.random(1));
					sleep(7000, 1000);
					stage++;
				} else {

					// if (level checks here) {
					// set fishingTYpe here
					// } can u write this for lobs and tuna/swordies please?
					// greater
					stage = 4;
				}
				break;

			case 3: // TODO: rewrite to add 2nd item in the array to cook

				bot.faceLocation(bot.getX() + 1, bot.getY());

				bot.doAmount = 28;
				Cooking.cookThisFood(bot, fishTypes[fishingType][0], 2728, null);
				sleep(1000, 1000);
				stage = 12;
				startedCooking = true;
				break;

			case 4:

				if (bot.getItems().playerHasItem(fishingTools[fishingType])) {
					bot.getItems()
							.deleteItemInOneSlot(fishingTools[fishingType], 28);
				}
				bot.getPA().playerWalk(3028, 3379);
				sleep(5000, 1000);
				bot.getUtils().bankAll();
				sleep(5000, 1000);
				loopCount++;
				stage++;

				if (bot.getSkills().getStaticLevel(Skills.FISHING) >= 1) {
					fishingType = 0;
				}
				if (bot.getSkills().getStaticLevel(Skills.FISHING) >= 35
						&& bot.getSkills().getStaticLevel(
								Skills.COOKING) >= 45) {
					fishingType = 1;
				}
				if (bot.getSkills().getStaticLevel(Skills.FISHING) >= 40
						&& bot.getSkills().getStaticLevel(
								Skills.COOKING) >= 40) {
					fishingType = 2;
				}
				if (bot.getSkills().getStaticLevel(Skills.FISHING) >= 76
						&& bot.getSkills().getStaticLevel(
								Skills.COOKING) >= 80) {
					fishingType = 3;
				}
				if (bot.getSkills().getStaticLevel(Skills.FISHING) >= 81
						&& bot.getSkills().getStaticLevel(
								Skills.COOKING) >= 91) {
					fishingType = 4;
				}

				break;
			case 5:
				bot.getItems().addItem(fishingTools[fishingType], 1);
				bot.getPA().playerWalk(fishLocations[fishingType][0],
						fishLocations[fishingType][1]);
				sleep(5000, 1000);
				stage++;
				break;
			case 6:
				if (fishingType == 0 || fishingType == 1 || fishingType == 3) {
					bot.faceLocation(bot.getX(), bot.getY() + 1);
				}
				if (fishingType == 2) {
					bot.faceLocation(bot.getX() + 1, bot.getY());
				} else {
					bot.faceLocation(bot.getX(), bot.getY() - 1);
				}
				Fishing.attemptdata(bot, fishingNpc[fishingType]);
				sleep(5000, 1000);
				stage = 11;
				break;
			case 7:
				System.out.println("Ended task for bot " + bot.getName());
				// bot.getUtils().bankAll();
				// bot.getUtils().teleHome();
				endTask(bot);
				break;
		}
	}

}
