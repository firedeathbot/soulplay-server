package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.game.LocationConstants;
import com.soulplay.util.Misc;

public class SeasonalRerollTask extends BotTask {

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new SeasonalRerollTask();
	}

	@Override
	public void process(BotClient bot) {
		if (!bot.inVarrock()) {
			bot.getPA().startTeleport(LocationConstants.VARROCK_X, LocationConstants.VARROCK_Y, 0, TeleportType.MODERN);
			sleep(10000);
			return;
		}

		switch (stage) {
			case 0:
				bot.destinationX = 3208 + Misc.random(11);
				bot.destinationY = 3421 + Misc.random(13);
				stage++;
				break;
			case 1:
				bot.getSeasonalData().powerUps.clear();
				PowerUpInterface.rollPowerUps(bot);
				sleep(5000, 5000);
				if (Misc.randomBoolean(5)) {
					sleep(5000, 10000);
				}
				if (Misc.randomBoolean(20)) {
					stage = 0;
				}
				break;
		}
	}

}
