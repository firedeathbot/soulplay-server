package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.click.object.ObjectFirstClick;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.game.RSConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class MiningAtFaladorTask extends BotTask {

	private int miningType = 0;

	private int loopCount = 0;

	private int toolId = 0;

	private int[] rockIds = {2090, // 0 copper
			2094, // 1 tin
			2092, // 2 iron
			2096, // 3 coal
			2102, // 4 mithril
			2105, // 5 adamant
			14861, // 6 rune rock
			14859, // 7 rune rock
	};

	private int[][] rockLocations = {{3039, 9783}, // 0 copper
			{3054, 9781}, // 1 tin
			{3037, 9776}, // 2 iron
			{3052, 9777}, // 3 coal
			{3035, 9771}, // 4 mithril
			{3042, 9774}, // 5 adamant
			{2374, 3850}, // 6 rune rock
			{2375, 3850} // 7 rune rock
	};

	private int[][] walkLocations = {{3039, 9782}, // 0 copper
			{3053, 9781}, // 1 tin
			{3037, 9775}, // 2 iron
			{3052, 9776}, // 3 coal
			{3036, 9771}, // 4 mithril
			{3042, 9773}, // 5 adamant
			{2374, 3349}, // 6 rune rock
			{2375, 3849} // 7 rune rock
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new MiningAtFaladorTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("miningfally "+stage);

		switch (stage) {
			case 0:
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 85) {
					stage = 7;
				}
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getUtils().teleHome();
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3094, 3489);
				sleep(5000, 1000);
				stage++;
				break;
			case 3:
				if (bot.getItems().playerHasItem(toolId)) {
					bot.getItems().deleteItemInOneSlot(toolId, 1);
				}
				bot.getUtils().bankAll();
				sleep(5000, 1000);
				loopCount++;
				stage++;

				if (loopCount > 2) {
					stage = 21;
				}

				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 1) {
					toolId = 1267;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 5) {
					toolId = 1269;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 21) {
					toolId = 1273;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 31) {
					toolId = 1271;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 41) {
					toolId = 1275;
				}

				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 1) {
					miningType = 0;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 15
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 15) {
					miningType = 2;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 30
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 30) {
					miningType = 3;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 55
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 50) {
					miningType = 4;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 70
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 70) {
					miningType = 5;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 85
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 85) {
					miningType = 6 + Misc.random(1);
				}
				break;
			case 4:
				bot.getPA().startTeleport(3055, 9777, 0, TeleportType.MODERN);
				sleep(7000, 1000);
				stage++;
				break;
			case 5:
				if (!bot.getItems().playerHasItem(toolId, 1)) {
					bot.getItems().addItem(toolId, 1);
				}
				bot.getPA().playerWalk(walkLocations[miningType][0],
						walkLocations[miningType][1]);
				sleep(5000, 1000);
				stage++;
				break;
			case 6:
				if (ObjectManager.getObject(rockLocations[miningType][0],
						rockLocations[miningType][1], 0) != null) {
					return;
				}

				if (miningType == 0
						&& 2 + bot.getItems().getItemCount(436) > bot.getItems()
								.getItemCount(438)) {
					miningType = 1;
					bot.playerSkilling[14] = false;
					bot.playerIsSkilling = false;
					stage = 5;
				} else if (miningType == 1
						&& 2 + bot.getItems().getItemCount(436) < bot.getItems()
								.getItemCount(438)) {
					miningType = 0;
					bot.playerSkilling[14] = false;
					bot.playerIsSkilling = false;
					stage = 5;
				}

				if (!bot.playerIsSkilling) {
					if (bot.getItems().freeSlots() < 1) {
						if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 85) {
							stage = 7;
						} else {
							stage = 1;
						}
						loopCount++;
					} else {
						GameObject gO = new GameObject(rockIds[miningType],
								Location.create(rockLocations[miningType][0],
								rockLocations[miningType][1], bot.heightLevel), 0, 10);
						Mining.attemptData(bot, gO);
					}
				}
				sleep(2000, 1000);
				break;
			case 7:
				sleep(3000, 1000); // 3000 is 3 seconds i believe
				stage++;
				break;
			case 8:
				bot.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
				sleep(7000, 1000);
				stage++;
				break;
			case 9:
				bot.getPA().playerWalk(2338, 3807);
				sleep(5000, 1000);
				stage++;
				break;
			case 10:
				bot.getItems().deleteItemInOneSlot(toolId, 1);
				bot.faceLocation(bot.getX(), bot.getY() + 1);
				bot.getUtils().bankAll();
				sleep(4000, 1000);
				loopCount++;
				stage++;
				break;
			case 11:
				bot.getPA().startTeleport(2317, 3834, 0, TeleportType.MODERN);
				sleep(7000, 1000);
				stage++;
				break;
			case 12:
				bot.getPA().playerWalk(2332, 3833);
				sleep(6000, 1000);
				stage++;
				break;
			case 13:
				bot.getPA().playerWalk(2348, 3832);
				sleep(6000, 1000);
				stage++;
				break;
			case 14:
				bot.getPA().playerWalk(2363, 3830);
				sleep(6000, 1000);
				stage++;
				break;
			case 15:
				bot.getPA().playerWalk(2378, 3839);
				sleep(6000, 1000);
				stage++;
				break;
			case 16:
				ObjectFirstClick.execute(bot, new GameObject(21314, Location.create(2378, 3840), 0, 10));
				sleep(6000, 1000);
				stage++;
				break;
			case 17:
				stage = 5;
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 85) {
					miningType = 6 + Misc.random(1);
					toolId = 1275;
				}
				break;
			case 21:
				endTask(bot);
				break;
		}
	}

}
