package com.soulplay.content.player.bot.tasks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.util.Misc;

public class BotEquipmentLoading {

	public static List<Inventory> equipments = null;
	private static List<GameItem[]> skills = null;
	public static List<int[]> looks = null;

 	public static void load() {
 		skills = new ArrayList<>();
 
		String absPath = new File("./Data/equipments.json").getAbsolutePath();
		String content = new String(Misc.readFile(absPath));
		String[] lines = GsonSave.gsond.fromJson(content, new TypeToken<String[]>() {}.getType());
		int length = lines.length;
 		equipments = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			String temp = lines[i];
			String[] data = temp.split(":");
			Inventory inventory = new Inventory(14);
			for (int i1 = 0; i1 < data.length; i1++) {
				int slot = Integer.parseInt(data[i1++]);
				inventory.replace(new Item(Integer
						.parseInt(data[i1++]), Integer
						.parseInt(data[i1])), slot);
			}
			
			equipments.add(inventory);
		}
		
		absPath = new File("./Data/looks.json").getAbsolutePath();
		content = new String(Misc.readFile(absPath));
		lines = GsonSave.gsond.fromJson(content, new TypeToken<String[]>() {}.getType());
		length = lines.length;
		looks = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			String temp = lines[i];
			String[] data = temp.split(":");
			int[] values = new int[data.length];
			for (int i1 = 0; i1 < data.length; i1++) {
				values[i1] = Integer.parseInt(data[i1]);
			}
			
			looks.add(values);
		}

		
	}

}
