package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.util.Misc;

public class MeleeRockCrabsTask extends BotTask {

	private int crabsFocused = 0;

	public int[] weaponType = {1323, // 0 iron scimitar
			1325, // 1 steel scimitar
			1327, // 2 black scimitar
			1329, // 3 mithril scimitar
			1331, // 4 adamant scimitar
			1333, // 5 rune scimitar
			4587, // 6 dragon scimitar
	};

	public int[] helmType = {1153, // 0 iron full helm
			1157, // 1 steel full helm
			1165, // 2 black full helm
			1159, // 3 mithril full helm
			1161, // 4 adamant full helm
			1163, // 5 rune full helm
			10828, // 6 helm of neitiznot
	};

	public int[] plateBodyType = {1115, // 0 iron platebody
			1119, // 1 steel platebody
			1125, // 2 black platebody
			1121, // 3 mithril platebody
			1123, // 4 adamant platebody
			1127, // 5 rune platebody
			1127, // 6 rune platebody
	};

	public int[] kiteShieldType = {1115, // 0 iron kiteshield
			1119, // 1 steel kiteshield
			1125, // 2 black kiteshield
			1121, // 3 mithril kiteshield
			1123, // 4 adamant kiteshield
			1127, // 5 rune kiteshield
			1127, // 6 rune kiteshield
	};

	public int[] platelegsType = {1067, // 0 iron platelegs
			1069, // 1 steel platelegs
			1077, // 2 black platelegs
			1071, // 3 mithril platelegs
			1073, // 4 adamant platelegs
			1079, // 5 rune platelegs
			1079, // 6 rune platelegs
	};

	public int[] levelReq = { // level reqs for both weaponType and armourType
			1, // 0 iron
			5, // 1 steel
			10, // 2 black
			20, // 3 mithril
			30, // 4 adamant
			40, // 5 rune
			60, // 6 dragon
	};

	public int[] capeType = { // make the bot use random cape when starting this
								// task
			6568, // 0 obsidian cape
			4333, // 1 team-10 cape
			4353, // 2 team-20 cape
			4373, // 3 team-30 cape
			4393, // 4 team-40 cape
	};

	public int[] amuletType = { // make the bot use random amulet when starting
								// this task
			1725, // 0 amulet of strength
			1704, // 1 amulet of glory
			11128, // 2 berserker necklace
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return true; // !bot.getData().getTypeOfBot().equals("skiller");
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new MeleeRockCrabsTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("Stage "+stage);
		// for (int i = amuletType.length; i > 0; i--) {
		// if (bot.getItems().playerHasItem(amuletType[i])) {
		// bot.getItems().wearItem(amuletType[i],
		// bot.getItems().getItemSlot(amuletType[i]));
		// break;
		// }
		// }
		if (bot.getCurrentHp() < 5) {
			if (bot.getUtils().getFoodCount() > 0) {
				bot.getUtils().eatFood();
			} else {
				stage = 7;
			}
		}

		switch (stage) {
			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getUtils().teleHome();
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3094, 3491);
				sleep(7000, 1000);
				bot.getUtils().bankAll();
				bot.getUtils().getFood(20);
				if (!bot.getUtils().getFood(20)) {
					if (!bot.getUtils().getFood(10)) {
						if (!bot.getUtils().getFood(5)) {
							if (!bot.getUtils().getFood(3)) {

							}
						}
					}
				}
				stage++;
				break;
			case 3:
				bot.getPA().startTeleport(2678, 3718, 0, TeleportType.MODERN);
				sleep(5000, 1000);
				stage++;
				break;
			case 4:// bot should always use climbing rock boots!
				int random = Misc.random(amuletType.length - 1);
				bot.getItems().addItem(amuletType[random], 1);
				// bot.getItems().setEquipment(amuletType[random], 1,
				// bot.getItems().getItemSlot(1725));
				sleep(3000, 1000);
				stage++;
				break;
			case 5:
				if (crabsFocused >= 30) {
					stage = 7;
				}
				if (bot.npcIndex == 0) {
					if (bot.getSkills().getStaticLevel(Skills.ATTACK) > bot
							.getSkills().getStaticLevel(Skills.STRENGTH)) {
						bot.fightMode = 1;// 0-attack 2-str 1-def
					} else {
						bot.fightMode = 0;
					}
					if (bot.getSkills().getStaticLevel(Skills.ATTACK) > bot
							.getSkills().getStaticLevel(Skills.DEFENSE)) {
						bot.fightMode = 2;
					}
					if (bot.getSkills().getStaticLevel(Skills.ATTACK) > bot
							.getSkills().getStaticLevel(Skills.STRENGTH)
							&& (bot.getSkills().getStaticLevel(
									Skills.DEFENSE) < bot
											.getSkills().getStaticLevel(
													Skills.STRENGTH))) {
						bot.fightMode = 1;
					}
					bot.getUtils().attackNearestNPC("Rock_Crab");
					crabsFocused++;
					sleep(3000, 1000);
				} else {
					stage++;
				}
				break;
			case 6:
				if (bot.npcIndex == 0) {
					stage--;
				}
				break;
			case 7:
				endTask(bot);
				break;
		}
	}
}
