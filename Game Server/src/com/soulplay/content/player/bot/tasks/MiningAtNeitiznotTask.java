package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.click.object.ObjectFirstClick;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.game.RSConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class MiningAtNeitiznotTask extends BotTask {

	private int miningType = 0;

	private int loopCount = 0;

	private int toolId = 0;

	private int[] rockIds = {21284, // 0 copper
			21293, // 1 tin
			21281, // 2 iron
			21287, // 3 coal
			21278, // 4 mithril
			21276, // 5 adamant
			14861, // 6 rune rock
			14859, // 7 rune rock
	};

	private int[][] rockLocations = {{2312, 3858}, // 0 copper
			{2313, 3856}, // 1 tin
			{2315, 3854}, // 2 iron
			{2311, 3852}, // 3 coal
			{2318, 3850}, // 4 mithril
			{2319, 3861}, // 5 adamant
			{2374, 3850}, // 6 rune rock
			{2375, 3850} // 7 rune rock
	};

	private int[][] walkLocations = {{2313, 3858}, // 0 copper
			{2313, 3857}, // 1 tin
			{2315, 3853}, // 2 iron
			{2312, 3852}, // 3 coal
			{2318, 3851}, // 4 mithril
			{2319, 3860}, // 5 adamant
			{2374, 3850}, // 6 rune rock
			{2375, 3850} // 7 rune rock
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new MiningAtNeitiznotTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("miningneitz "+stage);

		switch (stage) {
			case 0:
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 85) {
					stage = 10;
				}
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getUtils().teleHome();
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3094, 3489);
				sleep(5000, 1000);
				stage++;
				break;
			case 3:
				if (bot.getItems().playerHasItem(toolId)) {
					bot.getItems().deleteItemInOneSlot(toolId, 1);
				}
				bot.getUtils().bankAll();
				sleep(5000, 1000);
				loopCount++;
				stage++;

				if (loopCount > 5) {
					stage = 21;
				}

				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 1) {
					toolId = 1267;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 5) {
					toolId = 1269;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 21) {
					toolId = 1273;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 31) {
					toolId = 1271;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 41) {
					toolId = 1275;
				}

				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 1) {
					miningType = 0;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 15
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 15) {
					miningType = 2;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 30
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 30) {
					miningType = 3;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 55
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 50) {
					miningType = 4;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 70
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 70) {
					miningType = 5;
				}
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 85
						&& bot.getSkills().getStaticLevel(RSConstants.SMITHING) >= 85) {
					miningType = 6 + Misc.random(1);
				}
				break;
			case 4:
				// bot.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
				// sleep(7000, 1000);
				stage++;
				break;
			case 5:
				bot.getPA().startTeleport(2317, 3834, 0, TeleportType.MODERN);
				sleep(7000, 1000);
				stage++;
				break;
			case 6:
				bot.getPA().playerWalk(2314, 3839);
				sleep(5000, 1000);
				stage++;
				break;
			case 7:
				ObjectFirstClick.execute(bot, new GameObject(21310, Location.create(2314, 3840, bot.getZ()), 0, 10));
				sleep(3000, 1000);
				stage++;
				break;
			case 8:
				if (!bot.getItems().playerHasItem(toolId, 1)) {
					bot.getItems().addItem(toolId, 1);
				}
				bot.getPA().playerWalk(walkLocations[miningType][0],
						walkLocations[miningType][1]);
				sleep(5000, 1000);
				stage++;
				break;
			case 9:
				if (ObjectManager.getObject(rockLocations[miningType][0],
						rockLocations[miningType][1], 0) != null) {
					return;
				}

				if (miningType == 0
						&& 2 + bot.getItems().getItemCount(436) > bot.getItems()
								.getItemCount(438)) {
					miningType = 1;
					bot.playerSkilling[14] = false;
					bot.playerIsSkilling = false;
					stage = 8;
				} else if (miningType == 1
						&& 2 + bot.getItems().getItemCount(436) < bot.getItems()
								.getItemCount(438)) {
					miningType = 0;
					bot.playerSkilling[14] = false;
					bot.playerIsSkilling = false;
					stage = 8;
				}

				if (!bot.playerIsSkilling) {
					if (bot.getItems().freeSlots() < 1) {
						if (bot.getSkills().getStaticLevel(RSConstants.MINING) >= 85) {
							stage = 10;
						} else {
							stage = 1;
						}
						loopCount++;
					} else {
						GameObject gO = new GameObject(rockIds[miningType],
								Location.create(rockLocations[miningType][0],
								rockLocations[miningType][1],
								bot.getHeightLevel()), 0, 10);
						Mining.attemptData(bot, gO);
					}
				}
				sleep(2000, 1000);
				break;
			case 10:
				sleep(3000, 1000); // 3000 is 3 seconds i believe
				stage++;
				break;
			case 11:
				bot.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
				sleep(7000, 1000);
				stage++;
				break;
			case 12:
				bot.getPA().playerWalk(2338, 3807);
				sleep(5000, 1000);
				stage++;
				break;
			case 13:
				bot.getItems().deleteItemInOneSlot(toolId, 1);
				bot.faceLocation(bot.getX(), bot.getY() + 1);
				bot.getUtils().bankAll();
				sleep(4000, 1000);
				loopCount++;
				stage++;
				break;
			case 14:
				bot.getPA().startTeleport(2317, 3834, 0, TeleportType.MODERN);
				sleep(7000, 1000);
				stage++;
				break;
			case 15:
				bot.getPA().playerWalk(2332, 3833);
				sleep(6000, 1000);
				stage++;
				break;
			case 16:
				bot.getPA().playerWalk(2348, 3832);
				sleep(6000, 1000);
				stage++;
				break;
			case 17:
				bot.getPA().playerWalk(2363, 3830);
				sleep(6000, 1000);
				stage++;
				break;
			case 18:
				bot.getPA().playerWalk(2378, 3839);
				sleep(6000, 1000);
				stage++;
				break;
			case 19:
				ObjectFirstClick.execute(bot, new GameObject(21314, Location.create(2378, 3840, bot.getZ()), 0, 10));
				sleep(6000, 1000);
				// stage++;
				stage = 23;
				break;
			case 20:
				stage = 10;
				if (bot.getSkills().getStaticLevel(Skills.MINING) >= 85) {
					miningType = 6 + Misc.random(1);
					toolId = 1275;
				}
				break;
			case 21:
				endTask(bot);
				break;

			case 23:
				bot.getPA().playerWalk(2374 + Misc.random(1), 3849);
				sleep(1500, 1000);
				stage = 20;
				loopCount++;
				break;
		}
	}

}
