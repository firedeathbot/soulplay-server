package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.util.Misc;

public class ExploringHomeTask extends BotTask {

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new ExploringHomeTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("stage" + stage) ;
		switch (stage) {
			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3094, 3491);
				sleep(9000, 1000);
				bot.getUtils().bankAll();
				stage++;
				break;
			case 3:
				bot.getPA().playerWalk(3078, 3508);
				sleep(7000, 1000);
				stage++;
				break;
			case 4:
				bot.faceLocation(3078, 3509);
				sleep(3000, 1000);
				stage++;
				break;
			case 5:
				bot.getPA().playerWalk(3083, 3510);
				sleep(1800, 1000);
				stage++;
				break;
			case 6:
				bot.faceLocation(3084, 3510);
				sleep(3000, 1000);
				stage++;
				break;
			case 7:
				bot.getPA().playerWalk(3082, 3508);
				sleep(1000, 1000);
				stage++;
				break;
			case 8:
				bot.faceLocation(3082, 3509);
				sleep(2500, 1000);
				stage++;
				break;
			case 9:
				bot.getPA().playerWalk(3085, 3514 + Misc.random(2));
				sleep(5000, 1000);
				stage++;
				break;
			case 10:
				bot.faceLocation(bot.getX() - 1, bot.getY());
				sleep(10000, 1000);
				stage++;
				break;
			case 11:
				bot.getPA().playerWalk(3087 + Misc.random(9),
						3515 + Misc.random(6));
				sleep(75000, 1000);
				stage++;
				break;
			case 12:
				bot.getPA().playerWalk(3087 + Misc.random(9),
						3515 + Misc.random(6));
				sleep(40000, 1000);
				stage++;
				break;
			case 13:
				endTask(bot);
				break;
		}
	}

}
