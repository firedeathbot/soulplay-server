package com.soulplay.content.player.bot.tasks.combattasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.game.LocationConstants;

public class UltimatePkTask extends BotTask {

	private int maxHit = 40;

	private boolean risking = false;

	private boolean fightToDeath = false;

	private boolean retarget = true;

	@Override
	public boolean canBegin(BotClient bot) {
		return true;// bot.getLevelForXP(bot.playerLevel[Player.playerStrength])
					// > 0
		// && bot.getLevelForXP(bot.playerLevel[Player.playerStrength]) < 50;
		// && !bot.getData().getTypeOfBot().equals("skiller");*/
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new UltimatePkTask();
	}

	@Override
	public void process(BotClient bot) {
		// bot.forcedChat("Shit, gotta run!");
		if (bot.getHitPoints() < 1
				|| (bot.getX() == LocationConstants.EDGEVILLE_X
						&& bot.getY() == LocationConstants.EDGEVILLE_Y
						&& bot.getZ() == 0)) { // bot is dead
			System.out.println("Ended task.");
			endTask(bot);
			return;
		}
		if (bot.getPlayerToKill() == null) {
			System.out.println("Ended task2.");
			endTask(bot);
			return;
		}
//		if (!bot.getPlayerToKill().usingMagic) { // melee damage
			maxHit = bot.getPlayerToKill().getCombat().calculateMeleeMaxHit(1.0);
//		} else if (!bot.getPlayerToKill().castingMagic
//				&& bot.getPlayerToKill().projectileStage > 0) { // range hit
//																// damage
//			maxHit = bot.getPlayerToKill().getCombat().rangeMaxHit(1.0);
//		} else if (bot.getPlayerToKill().projectileStage > 0) { // magic hit
//																// damage
//			maxHit = 15;//bot.getPlayerToKill().getCombat().magicMaxHit();
//		}

		if (!risking && bot.getCurrentHp() < maxHit) {
			// System.out.println("Food count "+bot.getUtils().getFoodCount());
			if (bot.getUtils().getFoodCount() > 0) {
				bot.getUtils().eatFood();
			} else {
				if (!fightToDeath) {
					retarget = false;
					bot.getCombat().resetPlayerAttack();
					bot.speak("Shit, gotta run!");
					if (bot.wildLevel > 0
							&& (!bot.getItems().playerHasItem(563, 1)
									&& !bot.getItems().playerHasItem(556, 5))) {
						bot.getPA().playerWalk(LocationConstants.EDGEVILLE_X,
								LocationConstants.EDGEVILLE_Y);
					} else {
						bot.getPA().spellTeleport(LocationConstants.EDGEVILLE_X,
								LocationConstants.EDGEVILLE_Y, 0);
					}
					endTask(bot);
				}
			}
		}
		if (retarget && bot.playerIndex < 1) {
			bot.playerIndex = bot.getPlayerToKill().getId();
			System.out.println("test222");
		}
		switch (stage) {

			case 6:
				System.out.println("Ended task3.");
				endTask(bot);
				break;
		}
	}

}
