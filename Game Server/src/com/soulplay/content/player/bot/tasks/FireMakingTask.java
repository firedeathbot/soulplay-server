package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.firemaking.Firemaking;
import com.soulplay.util.Misc;

public class FireMakingTask extends BotTask {

	private int fireMakingType = 0;

	private int loopCount = 0;

	public int[] logType = {1511, // 0 normal logs
			1519, // 1 willow logs
			1515, // 2 yew logs
			1513, // 3 magic logs
	};

	private int[][] fireMakingLocations = {{3102, 3502}, {3102, 3503},
			{3102, 3504}, {3102, 3505}, {3121, 3504}, {3103, 3485},
			{3103, 3484}, {3103, 3483}, {3103, 3518}, {3103, 3519},
			{3103, 3520}};

	@Override
	public boolean canBegin(BotClient bot) {
		return bot.getItems().getBankItemCount(1511) >= 26
				|| bot.getItems().getBankItemCount(1519) >= 26 && bot
						.getSkills().getStaticLevel(Skills.FIREMAKING) >= 30
				|| bot.getItems().getBankItemCount(1515) >= 26 && bot
						.getSkills().getStaticLevel(Skills.FIREMAKING) >= 60
				|| bot.getItems().getBankItemCount(1513) >= 26 && bot
						.getSkills().getStaticLevel(Skills.FIREMAKING) >= 75;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new FireMakingTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("firemaking " + stage);
		if (stage == 6) {
			if (bot.getItems().playerHasItem(logType[fireMakingType], 1)) {
				stage = 4;
			}
			if (!bot.getItems().playerHasItem(logType[fireMakingType], 1)) {
				stage = 2;
			}
		}
		if (stage == 5) {
			if (!bot.getItems().playerHasItem(logType[fireMakingType], 1)) {
				stage = 2;
			}
			if (bot.getItems().playerHasItem(logType[fireMakingType], 1)) {
				Firemaking.attemptFire(bot, 590, logType[fireMakingType],
						bot.absX, bot.absY, false);
				sleep(4000, 1000);
			}
		}

		if (loopCount > 2) {
			stage = 6;
		}
		switch (stage) {
			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				bot.getUtils().bankAll();
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3097, 3496);
				sleep(10000, 1000);
				loopCount++;
				stage++;
				if (bot.getSkills().getStaticLevel(Skills.FIREMAKING) >= 1
						&& bot.getItems().getBankItemCount(1511) >= 26) {
					fireMakingType = 0;
				}
				if (bot.getSkills().getStaticLevel(Skills.FIREMAKING) >= 30
						&& bot.getItems().getBankItemCount(1519) >= 26) {
					fireMakingType = 1;
				}
				if (bot.getSkills().getStaticLevel(Skills.FIREMAKING) >= 60
						&& bot.getItems().getBankItemCount(1515) >= 26) {
					fireMakingType = 2;
				}
				if (bot.getSkills().getStaticLevel(Skills.FIREMAKING) >= 75
						&& bot.getItems().getBankItemCount(1513) >= 26) {
					fireMakingType = 3;
				}
				if (bot.getItems().getBankItemCount(1511) < 26
						|| bot.getItems().getBankItemCount(1519) < 26
								&& bot.getSkills().getStaticLevel(
										Skills.FIREMAKING) >= 30
						|| bot.getItems().getBankItemCount(1515) < 26
								&& bot.getSkills().getStaticLevel(
										Skills.FIREMAKING) >= 60
						|| bot.getItems().getBankItemCount(1513) < 26
								&& bot.getSkills().getStaticLevel(
										Skills.FIREMAKING) >= 75) {
					stage = 6;
				}
				break;
			case 3:
				// bot.getItems().fromBank(1512,
				// bot.getItems().getBankItemSlot(1512), 26);
				bot.getItems().removeBankItem(logType[fireMakingType], 26);
				bot.getItems().addItem(logType[fireMakingType], 26);
				if (!bot.getItems().playerHasItem(590)) {
					bot.getItems().addItem(590, 1);
				}
				sleep(5000, 1000);
				stage++;
				break;
			case 4:
				int random = Misc.random(fireMakingLocations.length - 1);
				bot.getPA().playerWalk(fireMakingLocations[random][0],
						fireMakingLocations[random][1]);
				sleep(10000, 1000);
				stage++;
				break;
			case 5:
				break;
			case 6:
				if (bot.getItems().playerHasItem(590)) {
					bot.getItems().deleteItemInOneSlot(590, 1);
				}
				endTask(bot);
				break;
		}
	}

}
