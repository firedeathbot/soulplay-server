package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.prayer.Prayer;
import com.soulplay.util.Misc;

public class PrayerTask extends BotTask {

	private int loopCount = 0;

	private boolean bonesOnAltar = false;

	private int prayerType = 0;

	public int[] boneType = {526, // 0 normal bone
			532, // 1 big bone
			536, // 2 dragon bone
			18830, // 3 frost dragon bone
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return bot.getItems().getBankItemCount(526) >= 28
				|| bot.getItems().getBankItemCount(532) >= 28
				|| bot.getItems().getBankItemCount(536) >= 28
				|| bot.getItems().getBankItemCount(18830) >= 28
						&& !bot.getData().getTypeOfBot().equals("skiller");
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new PrayerTask();
	}

	@Override
	public void process(BotClient bot) {

		if (bonesOnAltar) {
			if (!bot.attr().getOrDefault("usingAltar", false)) {
				if (bot.getItems().playerHasItem(boneType[prayerType], 28)) {
					stage = 4;
				}
			} else {
				stage = 2;
				bonesOnAltar = false;
			}
		} else if (stage == 11 && bot.getItems().freeSlots() == 28) {
			stage = 2;
			if (loopCount > 2) {
				stage = 6;
			}

		}

		switch (stage) {
			case 0:
				sleep(10000, 1000);
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(3087, 3499, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				bot.getPA().playerWalk(3097, 3496);
				sleep(7000, 1000);
				bot.getUtils().bankAll();
				loopCount++;
				stage++;
				if (bot.getItems().getBankItemCount(526) >= 28) {
					prayerType = 0;
				}
				if (bot.getItems().getBankItemCount(532) >= 28) {
					prayerType = 1;
				}
				if (bot.getItems().getBankItemCount(536) >= 28) {
					prayerType = 2;
				}
				if (bot.getItems().getBankItemCount(18830) >= 28) {
					prayerType = 3;
				}
				if (bot.getItems().getBankItemCount(526) < 28
						&& bot.getItems().getBankItemCount(532) < 28
						&& bot.getItems().getBankItemCount(536) < 28
						&& bot.getItems().getBankItemCount(18830) < 28
						&& bot.getItems().freeSlots() == 28) {
					stage = 6;
				}
				break;
			case 3:
				bot.getItems().removeBankItem(boneType[prayerType], 28);
				// bot.getItems().fromBank(boneType[prayerType],
				// bot.getItems().getBankItemSlot(boneType[prayerType]), 28);
				bot.getItems().addItem(boneType[prayerType], 28);
				sleep(5000, 1000);
				stage++;
			case 4:
				bot.getPA().playerWalk(3091 + Misc.random(1), 3505);
				sleep(4000, 1000);
				stage++;
				break;
			case 5:
				bot.attr().put("usingAltar", true);
				bot.faceLocation(bot.getX(), bot.getY() + 1);
				Prayer.bonesOnAltar2(bot, boneType[prayerType]);
				stage = 11;
				break;
			case 6:
				endTask(bot);
				break;
		}
	}
}
