package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Cooking;
import com.soulplay.content.player.skills.Fishing;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.util.Misc;

public class RocktailsTask extends BotTask {

	private int loopCount = 0;

	private boolean cookingStage = false;

	private boolean startedCooking = false;

	@Override
	public boolean canBegin(BotClient bot) {
		return bot.getSkills().getStaticLevel(Skills.FISHING) >= 90
				&& bot.getSkills().getStaticLevel(Skills.COOKING) >= 93;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new RocktailsTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("stage " + stage);
		if (cookingStage) {
			if (startedCooking) {
				if (!bot.isCooking) {
					if (bot.getItems().playerHasItem(15270, 1)) {
						Cooking.cookThisFood(bot, 15270, 2728, null);
						stage = 4;
						startedCooking = false;
						cookingStage = false;
					}
				}
			}
		} else if (stage == 11 && bot.getItems().freeSlots() < 1) {
			stage = 2;
		}
		if (stage == 12 && !bot.getItems().playerHasItem(15270, 1)) {
			stage = 2;
		}
		if (loopCount > 2) {
			stage = 8;
		}
		switch (stage) {
			case 0:
				sleep(10000, 1000); // 3000 is 3 seconds i believe
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(2595, 3414, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				if (bot.getItems().playerHasItem(15270, 1)) {
					cookingStage = true;
					bot.getPA().playerWalk(2597, 3409);
					sleep(6000, 1000);
					stage++;
				} else {
					stage = 4;
				}
				break;

			case 3:
				bot.faceLocation(bot.getX() + 1, bot.getY());
				bot.doAmount = 26;
				Cooking.cookThisFood(bot, 15270, 2728, null);
				sleep(1000, 1000);
				stage = 12;
				startedCooking = true;
				break;

			case 4:
				if (bot.getItems().playerHasItem(307)
						&& bot.getItems().playerHasItem(15263)) {
					bot.getItems().deleteItemInOneSlot(307, 1);
					bot.getItems().deleteItemInOneSlot(15263, 1000);
				}
				// if (bot.getItems().playerHasItem(15274)) {
				// bot.getItems().deleteItem2(15274, 28);
				// }
				bot.getPA().playerWalk(2586, 3418 + (Misc.random(4)));
				sleep(8000, 1000);
				bot.getUtils().bankAll();
				sleep(8000, 1000);
				loopCount++;
				stage++;
				break;
			case 5:
				bot.getItems().addItem(307, 1);
				bot.getItems().addItem(15263, 1000);
				bot.getPA().playerWalk(2598, 3410);
				sleep(5000, 1000);
				stage++;
				break;
			case 6:
				bot.getPA().playerWalk(2611, 3411);
				sleep(5000, 1000);
				stage++;
			case 7:
				bot.faceLocation(bot.getX(), bot.getY() - 1);
				Fishing.attemptdata(bot, 13);
				sleep(5000, 1000);
				stage = 11;
				break;
			case 8:
				// System.out.println("Ended task for bot "+bot.playerName);
				// bot.getUtils().bankAll();
				// bot.getUtils().teleHome();
				endTask(bot);
				break;
		}
	}

}
