package com.soulplay.content.player.bot.tasks;

import com.soulplay.content.bot.BotClient;
import com.soulplay.content.bot.BotTask;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.Woodcutting;
import com.soulplay.game.RSConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class WoodCuttingAtCamelotTask extends BotTask {

	private int woodCuttingType = 0;

	private int loopCount = 0;

	private int toolId = 0;

	private int[] treeIds = {1278, // 0 tree
			1276, // 1 normal tree
			1276, // 2 normal tree
			1308, // 3 willow tree
			1308, // 4 willow tree
			1308, // 5 willow tree
			1309, // 6 yew tree
			1309, // 7 yew tree
			1309, // 8 yew tree
			1306, // 9 magic tree
			1306, // 10 magic tree
			1306, // 11 magic tree
	};

	private int[][] treeLocations = {{2714, 3437}, // 0 tree
			{2725, 3439}, // 1 tree
			{2716, 3447}, // 2 tree
			{2781, 3427}, // 3 willow tree
			{2783, 3426}, // 4 willow tree
			{2786, 3429}, // 5 willow tree
			{2760, 3431}, // 6 yew tree
			{2755, 3430}, // 7 yew tree
			{2765, 3427}, // 8 yew tree
			{2698, 3396}, // 9 magic tree
			{2705, 3396}, // 10 magic tree
			{2705, 3398} // 11 magic tree
	};

	private int[][] walkLocations = {{2715, 3439}, // 0 tree
			{2724, 3440}, // 1 tree
			{2717, 3449}, // 2 tree
			{2782, 3429}, // 3 willow tree
			{2784, 3428}, // 4 willow tree
			{2785, 3430}, // 5 willow tree
			{2759, 3431}, // 6 yew tree
			{2757, 3429}, // 7 yew tree
			{2764, 3428}, // 8 yew tree
			{2700, 3397}, // 9 magic tree
			{2704, 3397}, // 10 magic tree
			{2704, 3398} // 11 magic tree
	};

	@Override
	public boolean canBegin(BotClient bot) {
		return true;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public BotTask newInstance() {
		return new WoodCuttingAtCamelotTask();
	}

	@Override
	public void process(BotClient bot) {
		// System.out.println("woodcutting " + stage);
		switch (stage) {
			case 0:
				sleep(10000, 1000); // 3000 is 3 seconds i believe
				stage++;
				break;
			case 1:
				bot.getPA().startTeleport(2726, 3485, 0, TeleportType.MODERN);
				sleep(8000, 1000);
				stage++;
				break;
			case 2:
				int random = Misc.random(2);
				if (bot.getItems().playerHasItem(toolId)) {
					bot.getItems().deleteItemInOneSlot(toolId, 1);
				}
				if (random == 0) {
					bot.getPA().playerWalk(2721 + Misc.random(1), 3493);
				} else if (random == 1) {
					bot.getPA().playerWalk(2724, 3493);
				} else if (random == 2) {
					bot.getPA().playerWalk(2727 + Misc.random(2), 3493);
				}
				sleep(9000, 1000);
				bot.getUtils().bankAll();
				loopCount++;
				stage++;
				break;
			case 3:
				bot.getPA().playerWalk(2725, 3479);
				sleep(6000, 1000);
				stage++;
				break;
			case 4:
				bot.getPA().playerWalk(2722, 3463);
				sleep(5000, 1000);
				stage++;
				break;
			case 5:
				bot.getPA().playerWalk(2721, 3447);
				sleep(6000, 1000);
				stage++;
				break;
			case 6:
				if (loopCount > 5) {
					stage = 19;
				}

				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 1) {
					toolId = 1349;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 5) {
					toolId = 1353;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 21) {
					toolId = 1355;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 31) {
					toolId = 1357;
				}
				if (bot.getSkills().getStaticLevel(RSConstants.WOODCUTTING) >= 41) {
					toolId = 1359;
				}

				if (bot.getSkills().getStaticLevel(
						Skills.WOODCUTTING) >= 1) {
					woodCuttingType = 0 + Misc.random(2);
					stage = 17;
				}
				if (bot.getSkills().getStaticLevel(Skills.WOODCUTTING) >= 30
						&& bot.getSkills().getStaticLevel(RSConstants.FIREMAKING) >= 30) {
					woodCuttingType = 3 + Misc.random(2);
					stage++;
				}
				if (bot.getSkills().getStaticLevel(Skills.WOODCUTTING) >= 60
						&& bot.getSkills().getStaticLevel(RSConstants.FIREMAKING) >= 60) {
					woodCuttingType = 6 + Misc.random(2);
					stage = 11;
				}
				if (bot.getSkills().getStaticLevel(Skills.WOODCUTTING) >= 75
						&& bot.getSkills().getStaticLevel(RSConstants.FIREMAKING) >= 75) {
					woodCuttingType = 9 + Misc.random(2);
					stage = 13;
				}
				break;
			case 7:
				bot.getPA().playerWalk(2735, 3436);
				sleep(6000, 1000);
				stage++;
				break;
			case 8:
				bot.getPA().playerWalk(2752, 3432);
				sleep(6000, 1000);
				stage++;
				break;
			case 9:
				bot.getPA().playerWalk(2769, 3432);
				sleep(6000, 1000);
				stage++;
				break;
			case 10:
				bot.getPA().playerWalk(2786, 3432);
				sleep(6000, 1000);
				stage = 17;
				break;
			case 11:
				bot.getPA().playerWalk(2735, 3436);
				sleep(6000, 1000);
				stage++;
				break;
			case 12:
				bot.getPA().playerWalk(2752, 3432);
				sleep(6000, 1000);
				stage = 17;
				break;
			case 13:
				bot.getPA().playerWalk(2710, 3436);
				sleep(6000, 1000);
				stage++;
				break;
			case 14:
				bot.getPA().playerWalk(2696, 3426);
				sleep(6000, 1000);
				stage++;
				break;
			case 15:
				bot.getPA().playerWalk(2693, 3410);
				sleep(6000, 1000);
				stage++;
				break;
			case 16:
				bot.getPA().playerWalk(2703, 3398);
				sleep(6000, 1000);
				stage++;
				break;
			case 17:
				bot.getItems().addItem(toolId, 1);
				bot.getPA().playerWalk(walkLocations[woodCuttingType][0],
						walkLocations[woodCuttingType][1]);
				sleep(4000, 1000);
				stage++;
				break;
			case 18:
				if (ObjectManager.getObject(treeLocations[woodCuttingType][0],
						treeLocations[woodCuttingType][1], 0) != null) {
					return;
				}
				if (!bot.playerIsSkilling) {
					if (bot.getItems().freeSlots() < 1) {
						stage = 1;
						loopCount++;
					} else {
						// bot.turnPlayerTo(treeLocations[woodCuttingType][0],
						// treeLocations[woodCuttingType][1]);
						GameObject gO = new GameObject(treeIds[woodCuttingType],
								Location.create(treeLocations[woodCuttingType][0],
								treeLocations[woodCuttingType][1],
								bot.getHeightLevel()), 0, 10);
						Woodcutting.initNormalWc(bot, gO);
					}
				}
				sleep(2000, 1000);
				break;
			case 19:
				endTask(bot);
				break;
		}
	}

}
