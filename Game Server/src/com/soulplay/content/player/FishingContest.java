package com.soulplay.content.player;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class FishingContest {

	private static Task task;

	public static boolean endFishingContest = false;

	private static final int[] CONTEST_REWARD = {1025, 8969, 13299, 13297,
			13298, 9472, 9944, 9945, 9946, 13339, 13340, 13341, 13342, 13343,
			13344, 13345, 13346, 13347, 13348, 13349, 13350, 13351, 13352, 8959,
			8952, 8991, 8960, 8953, 8992, 8964, 8957, 8996, 8961, 8954, 8993,
			8965, 8958, 8997, 8962, 8955, 8994, 8963, 8956, 8995, 8966, 8967,
			8968, 8969, 8970, 8971, 13366, 7134, 7138, 13370, 13372, 13374,
			13353, 13355, 13357, 13354, 8676, 8650, 8652, 8654, 8656, 8658,
			8660, 8662, 8664, 8666, 8668, 8670, 8672, 8674, 8674, 8680, 7140,
			8950, 7110, 7116, 7122, 7126, 13364, 13368, 7128, 7132, 7122, 6541};

	public static void beginFishingContest() {

		Server.getTaskScheduler().schedule(task = new Task(true) {

			int fishSpawn = 6000;

			@Override
			public void execute() {
				if (fishSpawn < 1) {
					// this.stop();
					endFishingContest();
					return;
				}

				/*
				 * if (endFishingContest = false) { this.stop(); return; }
				 */

				if (fishSpawn == 6000) {
					PlayerHandler.messageAllPlayers(
							"<img=9> <shad=1><col=FF9933>The Rare fish has arrived at the fishing contest, Hurry!");
					NPC fishingspot11 = NPCHandler.spawnNpc2(309, 2543, 4779, 0,
							0);
					NPC fishingspot12 = NPCHandler.spawnNpc2(309, 2543, 4778, 0,
							0);
					NPC fishingspot13 = NPCHandler.spawnNpc2(309, 2543, 4777, 0,
							0);
					NPC fishingspot14 = NPCHandler.spawnNpc2(309, 2543, 4776, 0,
							0);
					NPC fishingspot15 = NPCHandler.spawnNpc2(309, 2530, 4791, 0,
							0);
					fishingspot11.setAliveTick(700); // 6000 = 1 hour 100 = 1
														// min
					fishingspot12.setAliveTick(700);
					fishingspot13.setAliveTick(700);
					fishingspot14.setAliveTick(700);
					fishingspot15.setAliveTick(700);

				}
				if (fishSpawn == 5300) {
					PlayerHandler.messageAllPlayers(
							"<img=9> <shad=1><col=FF9933>The Rare fish is starting to move around in the fishing contest!");
					NPC fishingspot21 = NPCHandler.spawnNpc2(309, 2510, 4774, 0,
							0);
					NPC fishingspot22 = NPCHandler.spawnNpc2(309, 2510, 4775, 0,
							0);
					NPC fishingspot23 = NPCHandler.spawnNpc2(309, 2510, 4776, 0,
							0);
					NPC fishingspot24 = NPCHandler.spawnNpc2(309, 2510, 4777, 0,
							0);
					NPC fishingspot25 = NPCHandler.spawnNpc2(309, 2510, 4778, 0,
							0);
					fishingspot21.setAliveTick(400); // 6000 = 1 hour 100 = 1
														// min
					fishingspot22.setAliveTick(400);
					fishingspot23.setAliveTick(400);
					fishingspot24.setAliveTick(400);
					fishingspot25.setAliveTick(400);

				}
				if (fishSpawn == 4700) {
					NPC fishingspot31 = NPCHandler.spawnNpc2(309, 2526, 4791, 0,
							0);
					NPC fishingspot32 = NPCHandler.spawnNpc2(309, 2527, 4791, 0,
							0);
					NPC fishingspot33 = NPCHandler.spawnNpc2(309, 2528, 4791, 0,
							0);
					NPC fishingspot34 = NPCHandler.spawnNpc2(309, 2524, 4764, 0,
							0);
					NPC fishingspot35 = NPCHandler.spawnNpc2(309, 2529, 4764, 0,
							0);
					fishingspot31.setAliveTick(600); // 6000 = 1 hour 100 = 1
														// min
					fishingspot32.setAliveTick(600);
					fishingspot33.setAliveTick(600);
					fishingspot34.setAliveTick(600);
					fishingspot35.setAliveTick(600);

				}
				if (fishSpawn == 4200) {
					NPC fishingspot41 = NPCHandler.spawnNpc2(309, 3333, 3333, 0,
							0);
					NPC fishingspot42 = NPCHandler.spawnNpc2(309, 3333, 3333, 0,
							0);
					NPC fishingspot43 = NPCHandler.spawnNpc2(309, 3333, 3333, 0,
							0);
					NPC fishingspot44 = NPCHandler.spawnNpc2(309, 3333, 3333, 0,
							0);
					NPC fishingspot45 = NPCHandler.spawnNpc2(309, 3333, 3333, 0,
							0);
					fishingspot41.setAliveTick(500); // 6000 = 1 hour 100 = 1
														// min
					fishingspot42.setAliveTick(500);
					fishingspot43.setAliveTick(500);
					fishingspot44.setAliveTick(500);
					fishingspot45.setAliveTick(500);
				}
				if (fishSpawn == 3700) {
					NPC fishingspot51 = NPCHandler.spawnNpc2(309, 2543, 4776, 0,
							0);
					NPC fishingspot52 = NPCHandler.spawnNpc2(309, 2543, 4777, 0,
							0);
					NPC fishingspot53 = NPCHandler.spawnNpc2(309, 2543, 4778, 0,
							0);
					NPC fishingspot54 = NPCHandler.spawnNpc2(309, 2525, 4764, 0,
							0);
					NPC fishingspot55 = NPCHandler.spawnNpc2(309, 2526, 4764, 0,
							0);
					fishingspot51.setAliveTick(800); // 6000 = 1 hour 100 = 1
														// min
					fishingspot52.setAliveTick(800);
					fishingspot53.setAliveTick(800);
					fishingspot54.setAliveTick(800);
					fishingspot55.setAliveTick(800);

				}
				if (fishSpawn == 2900) {
					PlayerHandler.messageAllPlayers(
							"<img=9> <shad=1><col=FF9933>Hurry up, catch him before the rare fish leaves the fishing contest!");
					NPC fishingspot61 = NPCHandler.spawnNpc2(309, 2526, 4791, 0,
							0);
					NPC fishingspot62 = NPCHandler.spawnNpc2(309, 2527, 4791, 0,
							0);
					NPC fishingspot63 = NPCHandler.spawnNpc2(309, 2528, 4791, 0,
							0);
					NPC fishingspot64 = NPCHandler.spawnNpc2(309, 2529, 4791, 0,
							0);
					NPC fishingspot65 = NPCHandler.spawnNpc2(309, 2530, 4791, 0,
							0);
					fishingspot61.setAliveTick(600); // 6000 = 1 hour 100 = 1
														// min
					fishingspot62.setAliveTick(600);
					fishingspot63.setAliveTick(600);
					fishingspot64.setAliveTick(600);
					fishingspot65.setAliveTick(600);
				}
				if (fishSpawn == 2300) {
					NPC fishingspot71 = NPCHandler.spawnNpc2(309, 2510, 4778, 0,
							0);
					NPC fishingspot72 = NPCHandler.spawnNpc2(309, 2510, 4777, 0,
							0);
					NPC fishingspot73 = NPCHandler.spawnNpc2(309, 2510, 4776, 0,
							0);
					NPC fishingspot74 = NPCHandler.spawnNpc2(309, 2526, 4764, 0,
							0);
					NPC fishingspot75 = NPCHandler.spawnNpc2(309, 2527, 4764, 0,
							0);
					fishingspot71.setAliveTick(650); // 6000 = 1 hour 100 = 1
														// min
					fishingspot72.setAliveTick(650);
					fishingspot73.setAliveTick(650);
					fishingspot74.setAliveTick(650);
					fishingspot75.setAliveTick(650);
				}
				if (fishSpawn == 1650) {
					NPC fishingspot81 = NPCHandler.spawnNpc2(309, 2526, 4791, 0,
							0);
					NPC fishingspot82 = NPCHandler.spawnNpc2(309, 2527, 4791, 0,
							0);
					NPC fishingspot83 = NPCHandler.spawnNpc2(309, 2528, 4791, 0,
							0);
					NPC fishingspot84 = NPCHandler.spawnNpc2(309, 2529, 4791, 0,
							0);
					NPC fishingspot85 = NPCHandler.spawnNpc2(309, 2530, 4791, 0,
							0);
					fishingspot81.setAliveTick(500); // 6000 = 1 hour 100 = 1
														// min
					fishingspot82.setAliveTick(500);
					fishingspot83.setAliveTick(500);
					fishingspot84.setAliveTick(500);
					fishingspot85.setAliveTick(500);
				}
				if (fishSpawn == 1150) {
					NPC fishingspot91 = NPCHandler.spawnNpc2(309, 2543, 4776, 0,
							0);
					NPC fishingspot92 = NPCHandler.spawnNpc2(309, 2543, 4778, 0,
							0);
					NPC fishingspot93 = NPCHandler.spawnNpc2(309, 2527, 4764, 0,
							0);
					NPC fishingspot94 = NPCHandler.spawnNpc2(309, 2510, 4775, 0,
							0);
					NPC fishingspot95 = NPCHandler.spawnNpc2(309, 2510, 4777, 0,
							0);
					fishingspot91.setAliveTick(650); // 6000 = 1 hour 100 = 1
														// min
					fishingspot92.setAliveTick(650);
					fishingspot93.setAliveTick(650);
					fishingspot94.setAliveTick(650);
					fishingspot95.setAliveTick(650);
				}
				if (fishSpawn == 500) {
					PlayerHandler.messageAllPlayers(
							"<img=9> <shad=1><col=FF9933>It seems like the rare fish is starting to swim away from the fishing contest..");
					NPC fishingspot101 = NPCHandler.spawnNpc2(309, 2524, 4764,
							0, 0);
					NPC fishingspot102 = NPCHandler.spawnNpc2(309, 2525, 4764,
							0, 0);
					NPC fishingspot103 = NPCHandler.spawnNpc2(309, 2526, 4764,
							0, 0);
					NPC fishingspot104 = NPCHandler.spawnNpc2(309, 2528, 4764,
							0, 0);
					NPC fishingspot105 = NPCHandler.spawnNpc2(309, 2529, 4764,
							0, 0);
					fishingspot101.setAliveTick(500); // 6000 = 1 hour 100 = 1
														// min
					fishingspot102.setAliveTick(500);
					fishingspot103.setAliveTick(500);
					fishingspot104.setAliveTick(500);
					fishingspot105.setAliveTick(500);
				}
				if (fishSpawn < 1) {
					PlayerHandler.messageAllPlayers(
							"<img=9> <shad=1><col=FF9933>No one was able to catch the rare fish at the fishing contest!");
					// this.stop();
					endFishingContest();
					return;
				}

				if (fishSpawn > 0) {
					fishSpawn--;
				}
			}
		});
	}

	public static void contestReward(Player c) {
		if (c.getPA().freeSlots() > 1) {
			c.getExtraHiscores().incFishingContestWins();
			c.getItems().deleteItemInOneSlot(6200, 1);
			c.getItems().addItem(995, 100000000);
			c.getPA().addSkillXP(1000, Skills.FISHING);
			c.getItems().addItem(CONTEST_REWARD[Misc.random(getLength() - 1)],
					1);
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>"
					+ c.getNameSmartUp() + " has fed Evil Bob with the rare fish and received 100M gp");
			PlayerHandler.messageAllPlayers(
					"<img=9> <shad=1><col=FF9933>some fishing exp and a rare random item.");
			if (c.getClan() != null) {
				c.getClan().addClanPoints(c, 650, "fishingcontest");
			}
		} else {
			c.sendMessage(
					"You must have a tleast 2 free slots in your inventory.");
		}
	}

	public static void endFishingContest() {
		if (task != null) {
			task.stop();
			task = null;
		}
	}

	public static int getLength() {
		return CONTEST_REWARD.length;
	}

	public static boolean isRunning() {
		return task != null;
	}

}
