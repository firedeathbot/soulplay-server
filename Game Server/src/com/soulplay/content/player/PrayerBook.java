package com.soulplay.content.player;

public enum PrayerBook {

	NORMAL(0, 5608),
	CURSES(1, 22500);

	private static final PrayerBook[] values = values();
	private final int id, interfaceId;

	private PrayerBook(int id, int interfaceId) {
		this.id = id;
		this.interfaceId = interfaceId;
	}

	public int getId() {
		return id;
	}

	public int getInterfaceId() {
		return interfaceId;
	}

	public static PrayerBook find(int id) {
		for (PrayerBook prayerBook : values) {
			if (prayerBook.getId() == id) {
				return prayerBook;
			}
		}

		return NORMAL;
	}

}