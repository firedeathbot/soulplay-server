package com.soulplay.content.player.tracking;

import java.util.HashMap;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.slack.SlackMessageBuilder;

public class GrabUUID {
	
	public static HashMap<String, UUIDRequest> requests = new HashMap<String, UUIDRequest>();
	
	public static void enterRequest(String username, int modMysqlIndex) {
		requests.put(username, new UUIDRequest(modMysqlIndex, username));
	}
	
	public static boolean hasRequest(String username, String uuid) {
		UUIDRequest req = requests.get(username);
		if (req != null) {
			Player mod = PlayerHandler.getPlayerByMID(req.getModMysqlIndex());
			if (mod != null) {
				mod.sendMessage("Player:"+username+" has UUID of "+uuid);
			}
			Server.getSlackApi().call(SlackMessageBuilder.buildGrabUUIDMessage(username, uuid));
		} else {
			return false;
		}
		requests.clear();
		return true;
	}
	
	
}
