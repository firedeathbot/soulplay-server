package com.soulplay.content.player.tracking;

public class UUIDRequest {
	private final int modMysqlIndex;
	private final String playerOfInterest;

	public UUIDRequest(int mysql, String player) {
		this.modMysqlIndex = mysql;
		this.playerOfInterest = player;
	}

	public int getModMysqlIndex() {
		return modMysqlIndex;
	}
	public String getPlayerName() {
		return this.playerOfInterest;
	}
}
