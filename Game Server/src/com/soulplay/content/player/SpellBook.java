package com.soulplay.content.player;

public enum SpellBook {

	NORMAL(0, 1151),
	ANCIENT(1, 12855),
	LUNARS(2, 29999),
	DUNGEONEERING(3, 62000),
	ALL(4, 0);

	private static final SpellBook[] values = values();
	private final int id, interfaceId;

	private SpellBook(int id, int interfaceId) {
		this.id = id;
		this.interfaceId = interfaceId;
	}

	public int getId() {
		return id;
	}

	public int getInterfaceId() {
		return interfaceId;
	}

	public static SpellBook find(int id) {
		for (SpellBook spellBook : values) {
			if (spellBook.getId() == id) {
				return spellBook;
			}
		}

		return NORMAL;
	}

}
