package com.soulplay.content.player.dailytasks;

public class TaskConstants {

	public static final int WOODCUTTING_IVY = 1337;
	public static final int CRAFTING_GOLD = -1;
	public static final int ANY = -1;
	public static final int FIGHT_WITH_ICE_BARRAGE = -2;
	public static final int KILL_REVENANTS = -2;
	public static final int KILL_WILDY_BOSS = -3;
	public static final int KILL_BATTLE_MAGE = -4;
	public static final int KILL_THIEV_GUARD = -5;

}
