package com.soulplay.content.player.dailytasks;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.dailytasks.task.Task;
import com.soulplay.content.player.dailytasks.task.TaskCategory;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.dailytasks.task.impl.TriggerTask;
import com.soulplay.game.model.player.Player;

public class PlayerCategory {

	public static final int CATEGORY_LENGTH = 5;
	public static final int TASK_LENGTH = 7;

	private final List<PlayerTask> tasks;
	private final TaskCategory category;

	public PlayerCategory(TaskCategory category) {
		List<PlayerTask> playerTasks = new ArrayList<>(TASK_LENGTH);
		List<Task> tasks = new ArrayList<>(category.getTasks());
		Collections.shuffle(tasks);

		for (int i = 0; i < TASK_LENGTH; i++) {
			playerTasks.add(new PlayerTask(tasks.get(i)));
		}

		this.tasks = playerTasks;
		this.category = category;
	}

	public PlayerCategory(TaskCategory category, boolean load) {//4now
		this.category = category;
		this.tasks = new ArrayList<>(TASK_LENGTH);
	}

	public void addTask(PlayerTask task) {
		this.tasks.add(task);
	}

	public int getId() {
		return this.category.getId();
	}

	public int getIconId() {
		return this.category.getIconId();
	}

	public String getName() {
		return this.category.getName();
	}

	public TaskCategory getCategory() {
		return category;
	}

	public void increaseProgress(Player player, TaskType taskType, int trigger, int count) {
		int dayOfTheWeek = Server.getCalendar().currentDayOfWeek();
		PlayerTask task = this.tasks.get(dayOfTheWeek - 1);
		if (task.getTaskType() != taskType) {
			return;
		}

		boolean completed = task.isCompleted();
		if (completed) {
			return;
		}

		task.increaseComplete(trigger, player, count);
		if (completed != task.isCompleted()) {
			if (player.bonusTask == null) {
				player.bonusTask = new PlayerTask(TriggerTask.VOTE_TASK);
			}

			player.dailyTasksCompleted++;
			player.dailyTasksPoints += PlayerTask.POINTS_FOR_COMPLETION;
			CategoryInterfaceHandler.updateSidebar(player);
			
			player.seasonPass.onTaskCompleted(player, false);

			if (dayOfTheWeek == Calendar.SATURDAY) {//End of the NA week
				boolean givePoints = true;
				for (int i = 0; i < TASK_LENGTH - 1; i++) {//We already completed Saturday task, no need to loop for it
					if (!this.tasks.get(i).isCompleted()) {
						givePoints = false;
						break;
					}
				}

				if (givePoints) {
					player.dailyTasksPoints += PlayerTask.POINTS_FOR_COMPLETION_ALL;
					player.seasonPass.onTaskCompleted(player, true);
				}

				boolean giveMorePoints = true;
				loop: for (int i = 0; i < CATEGORY_LENGTH; i++) {
					PlayerCategory category = player.categories[i];
					for (int k = 0; k < TASK_LENGTH; k++) {
						if (!category.tasks.get(k).isCompleted()) {
							giveMorePoints = false;
							break loop;
						}
					}
				}

				if (giveMorePoints) {
					player.dailyTasksPoints += PlayerTask.POINTS_FOR_COMPLETION_ALL_ALL;
				}
			}
		}
	}

	public PlayerTask getTask(int id) {
		return this.tasks.get(id);
	}

	public static PlayerCategory[] pickRandom5() {
		List<TaskCategory> categories = new ArrayList<>(TaskCategory.values);
		Collections.shuffle(categories);

		PlayerCategory[] playerCategories = new PlayerCategory[CATEGORY_LENGTH];
		for (int i = 0; i < CATEGORY_LENGTH; i++) {
			if (i < 2)
				playerCategories[i] = new PlayerCategory(TaskCategory.WILDERNESS);
			else
				playerCategories[i] = new PlayerCategory(categories.get(i));
		}

		return playerCategories;
	}

}
