package com.soulplay.content.player.dailytasks;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;

public class CategoryInterfaceHandler {

	public static void open(Player c) {
		if (c.dailyTasksLocked()) {
			c.sendMessage("You don't meet the requirement to access daily tasks.");
			return;
		}

		c.getPacketSender().sendTaskData();
		c.getPacketSender().sendString("Total Points: " + c.dailyTasksPoints, 66309);
		c.getPacketSender().sendString("Tasks Completed: " + c.dailyTasksCompleted, 66310);
		if (c.bonusTask == null) {
			c.getPacketSender().sendString("Bonus Task: Locked", 66328);
		} else {
			int times = c.bonusTask.getRequiredCount() - c.bonusTask.getCompletedCount();
			if (times == 0) {
				c.getPacketSender().sendString("Bonus Task: Completed", 66328);
			} else if (times == 1) {
				c.getPacketSender().sendString("Bonus Task: Claim 1 Vote.", 66328);
			} else {
				c.getPacketSender().sendString("Bonus Task: Claim " + times + " Votes.", 66328);
			}
		}

		c.getPacketSender().showInterface(66300);
	}

	public static void updateSidebar(Player c) {
		c.getPacketSender().sendSidebarData();

		int daysLeft = 7 - Server.getCalendar().currentDayOfWeek();
		String text;
		if (daysLeft == 0) {
			int hoursLeft = 24 - Server.getCalendar().get24Hour();
			if (hoursLeft == 1) {
				text = "Tasks reset in: 1 hour";
			} else {
				text = "Tasks reset in: " + hoursLeft + " hours";
			}
		} else if (daysLeft == 1) {
			text = "Tasks reset in: 1 Day";
		} else {
			text = "Tasks reset in: " + daysLeft + " Days";
		}

		c.getPacketSender().sendString(text, 66235);
		//TODO update sidebar in more places?
	}

}
