package com.soulplay.content.player.dailytasks.task.impl;

import com.soulplay.content.player.dailytasks.task.Task;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.dailytasks.task.condition.impl.WildernessCondition;

public class TriggerTask extends Task {

	public static final TriggerTask VOTE_TASK = TriggerTask.create(0, 5, "Vote 5 times", -1, TaskType.VOTE);

	private final int triggerId;

	public TriggerTask(int id, int requiredCount, String desc, int triggerId, TaskType taskType) {
		super(id, requiredCount, desc, taskType);
		this.triggerId = triggerId;
	}

	public static TriggerTask create(int id, int requiredCount, String desc, int triggerId, TaskType taskType) {
		return new TriggerTask(id, requiredCount, desc, triggerId, taskType);
	}

	public static TriggerTask agility(int id, int requiredCount, String desc, int buttonId) {
		return create(id, requiredCount, desc, buttonId, TaskType.AGILITY);
	}

	public static TriggerTask fish(int id, int requiredCount, String desc, int fishId) {
		return create(id, requiredCount, desc, fishId, TaskType.FISHING);
	}

	public static TriggerTask herb(int id, int requiredCount, String desc, int potId) {
		return create(id, requiredCount, desc, potId, TaskType.HERBLORE);
	}

	public static TriggerTask craft(int id, int requiredCount, String desc, int productId) {
		return create(id, requiredCount, desc, productId, TaskType.CRAFTING);
	}

	public static TriggerTask craftMould(int id, int requiredCount, String desc, int gemId) {
		return create(id, requiredCount, desc, gemId, TaskType.CRAFTING_MOULD);
	}

	public static TriggerTask killNpc(int id, int requiredCount, String desc, int npcId) {
		return create(id, requiredCount, desc, npcId, TaskType.NPC_KILL);
	}

	public static TriggerTask killBossNpc(int id, int requiredCount, String desc, int npcId) {
		return create(id, requiredCount, desc, npcId, TaskType.NPC_BOSS_KILL);
	}

	public static TriggerTask smelt(int id, int requiredCount, String desc, int oreId) {
		return create(id, requiredCount, desc, oreId, TaskType.SMITHING_SMELT);
	}

	public static TriggerTask smith(int id, int requiredCount, String desc, int productId) {
		return create(id, requiredCount, desc, productId, TaskType.SMITHING_MAKE);
	}

	public static TriggerTask smithBar(int id, int requiredCount, String desc, int barId) {
		return create(id, requiredCount, desc, barId, TaskType.SMITHING_BAR_TYPE);
	}

	public static TriggerTask mine(int id, int requiredCount, String desc, int oreId) {
		return create(id, requiredCount, desc, oreId, TaskType.MINING);
	}

	public static TriggerTask prayerBury(int id, int requiredCount, String desc, int boneId) {
		return create(id, requiredCount, desc, boneId, TaskType.PRAYER_BURY);
	}

	public static TriggerTask prayerAltar(int id, int requiredCount, String desc, int boneId) {
		return create(id, requiredCount, desc, boneId, TaskType.PRAYER_ALTAR);
	}
	
	public static TriggerTask firemaking(int id, int requiredCount, String desc, int logId) {
		return create(id, requiredCount, desc, logId, TaskType.FIREMAKING);
	}
	
	public static TriggerTask woodcutting(int id, int requiredCount, String desc, int logId) {
		return create(id, requiredCount, desc, logId, TaskType.WOODCUTTING);
	}
	
	public static TriggerTask runecrafting(int id, int requiredCount, String desc, int runeId) {
		return create(id, requiredCount, desc, runeId, TaskType.RUNECRAFTING);
	}
	
	public static TriggerTask cooking(int id, int requiredCount, String desc, int foodId) {
		return create(id, requiredCount, desc, foodId, TaskType.COOKING);
	}

	public static TriggerTask wildernessCreate(int id, int requiredCount, String desc, int triggerId, TaskType taskType) {
		TriggerTask task = create(id, requiredCount, desc, triggerId, taskType);
		task.setCondition(new WildernessCondition());
		return task;
	}

	public static TriggerTask killOpponent(int id, int requiredCount, String desc) {
		return wildernessCreate(id, requiredCount, desc, 0, TaskType.OPPONENT_DEATH);
	}

	public static TriggerTask fightOpponentWith(int id, int requiredCount, String desc, int weapon) {
		return wildernessCreate(id, requiredCount, desc, weapon, TaskType.FIGHT_OPPONENT);
	}

	public static TriggerTask exchangeEmblem(int id, int requiredCount, String desc) {
		return wildernessCreate(id, requiredCount, desc, 0, TaskType.EXCHANGE_EMBLEM);
	}

	public static TriggerTask earnElo(int id, int requiredCount, String desc) {
		return wildernessCreate(id, requiredCount, desc, 0, TaskType.EARN_ELO);
	}

	public static TriggerTask pickpocket(int id, int requiredCount, String desc, int triggerId, TaskType type) {
		return create(id, requiredCount, desc, triggerId, type);
	}

	@Override
	public int triggerId() {
		return triggerId;
	}

}
