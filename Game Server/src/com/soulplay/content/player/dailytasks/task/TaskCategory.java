package com.soulplay.content.player.dailytasks.task;

import com.soulplay.content.player.dailytasks.task.condition.impl.*;
import static com.soulplay.content.player.dailytasks.task.impl.TriggerTask.*;
import static com.soulplay.content.player.dailytasks.TaskConstants.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.travel.zone.impl.EdgePkZone;

public enum TaskCategory {

	//TODO Boss icon?
	BOSS(0, "Bossing", 1235,
			killBossNpc(0, 8, "Kill Zulrah 8 times", 2012),
			killBossNpc(1, 8, "Kill Kree'arra 8 times", 6222),
			killBossNpc(2, 8, "Kill General Graardor 8 times", 6260),
			killBossNpc(3, 8, "Kill Commander Zilyana 8 times", 6247),
			killBossNpc(4, 8, "Kill K'ril Tsutsaroth 8 times", 6203),
			killBossNpc(5, 8, "Kill Tormented Demon 8 times", 8349),
			killBossNpc(6, 8, "Kill Corporeal beast 8 times", 8133),
			killBossNpc(7, 8, "Kill Dagganoth Prime 8 times", 2882),
			killBossNpc(8, 8, "Kill Dagganoth Rex 8 times", 2883),
			killBossNpc(9, 8, "Kill Dagganoth Supreme 8 times", 2881),
			killBossNpc(10, 1, "Kill Nex 1 time", 13447),
			killBossNpc(11, 8, "Kill Bandos Avatar 8 times", 4540),
			killBossNpc(12, 7, "Kill Giant Mole 7 times", 3340),
			killBossNpc(13, 10, "Kill Barrelchest 10 times", 5666),
			killBossNpc(14, 8, "Kill King Black Dragon 8 times", 50),
			killBossNpc(15, 15, "Kill 15 bosses", ANY)
	),
	//TODO craft icon
	CRAFTING(1, "Crafting", 1226,
			craft(0, 250, "Spin 250 Flax into Bow strings", 1777),
			craftMould(1, 85, "Craft 85 Gold jewelleries", CRAFTING_GOLD),
			craftMould(2, 80, "Craft 80 Sapphire jewelleries", 1607),
			craftMould(3, 78, "Craft 78 Emerald jewelleries", 1605),
			craftMould(4, 75, "Craft 75 Ruby jewelleries", 1603),
			craftMould(5, 65, "Craft 65 Diamond jewelleries", 1601),
			craftMould(6, 15, "Craft 15 Dragonstone jewelleries", 1615),
			craftMould(7, 5, "Craft 5 Onyx jewelleries", 6573),
			craft(8, 100, "Craft 100 Leather vambraces", 1063),
			craft(9, 100, "Craft 100 Leather chaps", 1095),
			craft(10, 100, "Craft 100 Leather body", 1129),
			craft(11, 50, "Craft 50 Green d'hide vambraces", 1065),
			craft(12, 50, "Craft 50 Green d'hide chaps", 1099),
			craft(13, 50, "Craft 50 Green d'hide body", 1135),
			craft(14, 50, "Craft 50 Blue d'hide vambraces", 2487),
			craft(15, 50, "Craft 50 Blue d'hide chaps", 2493),
			craft(16, 50, "Craft 50 Blue d'hide body", 2499),
			craft(17, 50, "Craft 50 Red d'hide vambraces", 2489),
			craft(18, 50, "Craft 50 Red d'hide chaps", 2495),
			craft(19, 50, "Craft 50 Red d'hide body", 2501),
			craft(20, 50, "Craft 50 Black d'hide vambraces", 2491),
			craft(21, 50, "Craft 50 Black d'hide chaps", 2497),
			craft(22, 50, "Craft 50 Black d'hide body", 2503)
	),
	//TODO herb icon
	HERBLORE(2, "Herblore", 1224,
			herb(0, 100, "Make 100 Attack potions", 121),
			herb(1, 100, "Make 100 Defence potions", 133),
			herb(2, 100, "Make 100 Strength potions", 115),
			herb(3, 100, "Make 100 Magic potions", 3042),
			herb(4, 100, "Make 100 Ranging potions", 169),
			herb(5, 50, "Make 50 Prayer potions", 139),
			herb(6, 50, "Make 50 Energy potions", 3010),
			herb(7, 75, "Make 75 Super attack potions", 145),
			herb(8, 50, "Make 50 Super anti-poison potions", 181),
			herb(9, 40, "Make 40 Super energy potions", 3018),
			herb(10, 75, "Make 75 Super strength potions", 157),
			herb(11, 25, "Make 25 Weapon poison potions", 187),
			herb(12, 50, "Make 50 Super restore potions", 3026),
			herb(13, 75, "Make 75 Super defence potions", 163),
			herb(14, 50, "Make 50 Antifire potions", 2454),
			herb(15, 100, "Make 100 Saradomin brews", 6687),
			herb(16, 25, "Make 25 Extreme attack potions", 15309),
			herb(17, 25, "Make 25 Extreme strength potions", 15313),
			herb(18, 25, "Make 25 Extreme defence potions", 15317),
			herb(19, 25, "Make 25 Extreme magic potions", 15321),
			herb(20, 25, "Make 25 Extreme ranging potions", 15325),
			herb(21, 40, "Make 40 combat potions", 9741),
			herb(22, 2, "Make 2 Overloads", 15333)
	),
	FISHING(3, "Fishing", 1230,
			fish(0, 150, "Catch 150 Shrimps", 317),
			fish(1, 90, "Catch 90 Sardines", 327),
			fish(2, 300, "Catch 300 Karambwanji", 3150),
			fish(3, 80, "Catch 80 Herring", 345),
			fish(4, 80, "Catch 80 Anchovies", 321),
			fish(5, 100, "Catch 100 Mackerel", 353),
			fish(6, 100, "Catch 100 Trout", 335),
			fish(7, 145, "Catch 145 Pike", 349),
			fish(8, 80, "Catch 80 Salmon", 331),
			fish(9, 100, "Catch 100 Tuna", 359),
			fish(10, 70, "Catch 70 Swordfish", 371),
			fish(11, 125, "Catch 125 Lobster", 377),
			fish(12, 100, "Catch 100 Monkfish", 7944),
			fish(13, 100, "Catch 100 Karambwan", 3142),
			fish(14, 100, "Catch 100 Shark", 383),
			fish(15, 90, "Catch 90 Manta ray", 389),
			fish(16, 80, "Catch 80 Rocktail", 15270)
	),
	//TODO icon
	SMITHING(4, "Smithing", 1229,
			smelt(0, 75, "Smelt 75 Bronze bars", 2349),
			smelt(1, 65, "Smelt 65 Iron bars", 2351),
			smelt(2, 70, "Smelt 70 Silver bars", 2355),
			smelt(3, 55, "Smelt 55 Steel bars", 2353),
			smelt(4, 50, "Smelt 50 Gold bars", 2357),
			smelt(5, 40, "Smelt 40 Mithril bars", 2359),
			smelt(6, 35, "Smelt 35 Adamant bars", 2361),
			smelt(7, 25, "Smelt 25 Rune bars", 2363),
			smith(8, 25, "Smith 25 Bronze scimitar", 1321),
			smith(9, 22, "Smith 22 Iron scimitar", 1323),
			smith(10, 20, "Smith 20 Steel scimitar", 1325),
			smith(11, 18, "Smith 18 Mithril scimitar", 1329),
			smith(12, 16, "Smith 16 Adamant scimitar", 1331),
			smith(13, 12, "Smith 12 Rune scimitar", 1333),
			smith(14, 15, "Smith 15 Bronze platelegs", 1075),
			smith(15, 14, "Smith 14 Iron platelegs", 1067),
			smith(16, 13, "Smith 13 Steel platelegs", 1069),
			smith(17, 12, "Smith 12 Mithril platelegs", 1071),
			smith(18, 11, "Smith 11 Adamant platelegs", 1073),
			smith(19, 9, "Smith 9 Rune platelegs", 1079),
			smith(20, 5, "Smith 5 Bronze platebody", 1117),
			smith(21, 5, "Smith 5 Iron platebody", 1115),
			smith(22, 5, "Smith 5 Steel platebody", 1119),
			smith(23, 4, "Smith 4 Mithril platebody", 1121),
			smith(24, 4, "Smith 4 Adamant platebody", 1123),
			smith(25, 3, "Smith 3 Rune platebody", 1127),
			smithBar(26, 35, "Smith 35 Bronze items", 2349),
			smithBar(27, 33, "Smith 33 Iron items", 2351),
			smithBar(28, 32, "Smith 32 Steel items", 2353),
			smithBar(29, 30, "Smith 30 Mithril items", 2359),
			smithBar(30, 28, "Smith 28 Adamant items", 2361),
			smithBar(31, 25, "Smith 25 Rune items", 2363),
			smith(32, 500, "Smith 500 Cannonballs", 2)
	),
	//TODO mining icon
	MINING(5, "Mining", 1228,
			mine(0, 150, "Mine 150 Tin ore", 438),
			mine(1, 150, "Mine 150 Copper ore", 436),
			mine(2, 135, "Mine 135 Iron ore", 440),
			mine(3, 115, "Mine 115 Coal ore", 453),
			mine(4, 85, "Mine 85 Mithril ore", 447),
			mine(5, 75, "Mine 75 Adamantite ore", 449),
			mine(6, 50, "Mine 50 Runite ore", 451),
			mine(7, 150, "Mine 150 Silver ore", 442),
			mine(8, 100, "Mine 100 Gold ore", 444),
			mine(9, 150, "Mine 150 Clay", 434),
			mine(10, 100, "Mine 100 Limestone", 3211),
			mine(11, 150, "Mine 150 Ore", ANY),
			mine(12, 125, "Mine 125 Ore in Dorgeshunn", ANY).setCondition(new ZoneCondition(Location.create(3228, 9602), Location.create(3332, 9669))),
			mine(13, 125, "Mine 125 Ore in Neitiznot", ANY).setCondition(new ZoneCondition(Location.create(2309, 3849), Location.create(2323, 3864))),
			mine(14, 125, "Mine 125 Ore in Crafting Guild", ANY).setCondition(new ZoneCondition(Location.create(2924, 3274), Location.create(2945, 3294))),
			mine(15, 20, "Mine 20 Uncut ruby in Gem mine", 1619).setCondition(new ZoneCondition(Location.create(2819, 2995), Location.create(2830, 3006))),
			mine(16, 20, "Mine 20 Uncut sapphire in Gem mine", 1623).setCondition(new ZoneCondition(Location.create(2819, 2995), Location.create(2830, 3006))),
			mine(17, 20, "Mine 20 Uncut emerald in Gem mine", 1621).setCondition(new ZoneCondition(Location.create(2819, 2995), Location.create(2830, 3006)))
	),
	AGILITY(6, "Agility", 1223,
			agility(0, 25, "Complete 25 runs at gnome agility course", 166),
			agility(1, 20, "Complete 20 runs at barbarian agility course", 167),
			agility(2, 20, "Complete 20 runs at wilderness agility course", 168),
			agility(3, 25, "Complete 25 runs at barbarian agility course", 167),
			agility(4, 10, "Use the Taverly dungeon pipe shortcut 10 times", 9293),
			agility(5, 23, "Complete 23 runs at gnome agility course", 166),
			agility(6, 26, "Complete 26 runs at wilderness agility course", 168)
	),
	PRAYER(7, "Prayer", 1220,
			prayerBury(0, 150, "Bury 150 Bones", 526),
			prayerBury(1, 125, "Bury 125 Big bones", 532),
			prayerBury(2, 100, "Bury 100 Dragon bones", 536),
			prayerBury(3, 70, "Bury 70 Frost dragon bones", 18830),
			prayerBury(4, 60, "Bury 60 Dagannoth bones", 6729),
			prayerBury(5, 60, "Bury 60 Lava dragon bones", 17676),
			prayerAltar(6, 150, "Use 150 Bones on an altar", 526),
			prayerAltar(7, 125, "Use 125 Big bones on an altar", 532),
			prayerAltar(8, 100, "Use 100 Dragon bones on an altar", 536),
			prayerAltar(9, 70, "Use 70 Frost dragon bones on an altar", 18830),
			prayerAltar(10, 60, "Use 60 Dagannoth bones on an altar", 6729),
			prayerAltar(11, 60, "Use 60 Lava dragon bones on an altar", 17676),
			prayerBury(12, 200, "Bury 200 bones of any kind", ANY),
			prayerAltar(13, 200, "Use 200 bones of any kind on an altar", ANY)
	),
	FIREMAKING(8, "Firemaking", 1232,
			firemaking(0, 200, "Burn 200 normal logs", 1511),
			firemaking(1, 185, "Burn 185 oak logs", 1521),
			firemaking(2, 180, "Burn 180 willow logs", 1519),
			firemaking(3, 165, "Burn 165 maple logs", 1517),
			firemaking(4, 150, "Burn 150 yew logs", 1515),
			firemaking(5, 125, "Burn 125 magic logs", 1513),
			firemaking(6, 175, "Burn 175 normal logs above level 30 in the Wilderness", 1511).setCondition(new WildernessCondition(30)),
			firemaking(7, 165, "Burn 165 oak logs above level 30 in the Wilderness", 1521).setCondition(new WildernessCondition(30)),
			firemaking(8, 160, "Burn 160 willow logs above level 30 in the Wilderness", 1519).setCondition(new WildernessCondition(30)),
			firemaking(9, 140, "Burn 140 maple logs above level 30 in the Wilderness", 1517).setCondition(new WildernessCondition(30)),
			firemaking(10, 125, "Burn 125 yew logs above level 30 in the Wilderness", 1515).setCondition(new WildernessCondition(30)),
			firemaking(11, 100, "Burn 100 magic logs above level 30 in the Wilderness", 1513).setCondition(new WildernessCondition(30))
	),
	WOODCUTTING(9, "Woodcutting", 1233,
			woodcutting(0, 200, "Cut 200 normal logs", 1511),
			woodcutting(1, 185, "Cut 185 oak logs", 1521),
			woodcutting(2, 180, "Cut 180 willow logs", 1519),
			woodcutting(3, 165, "Cut 165 maple logs", 1517),
			woodcutting(4, 150, "Cut 150 yew logs", 1515),
			woodcutting(5, 125, "Cut 125 magic logs", 1513),
			woodcutting(6, 100, "Cut 100 mahogany logs", 6332),
			woodcutting(7, 160, "Cut 160 teak logs", 6333),
			woodcutting(8, 125, "Cut 125 yew logs in edgeville", 1515).setCondition(new ZoneCondition(Location.create(3084, 3467), Location.create(3090, 3483))),
			woodcutting(9, 125, "Cut 125 yew logs in varrock castle", 1515).setCondition(new ZoneCondition(Location.create(3199, 3440), Location.create(3225, 3506))),
			woodcutting(10, 150, "Cut 150 ivy vines", WOODCUTTING_IVY)
	),
	RUNECRAFING(10, "Runecrafting", 1234,
			runecrafting(0, 3000, "Craft 3000 air runes", 556),
			runecrafting(1, 2400, "Craft 2400 mind runes", 558),
			runecrafting(2, 1800, "Craft 1800 water runes", 555),
			runecrafting(3, 1200, "Craft 1200 earth runes", 557),
			runecrafting(4, 900, "Craft 900 fire runes", 554),
			runecrafting(5, 900, "Craft 900 body runes", 559),
			runecrafting(6, 600, "Craft 600 cosmic runes", 564),
			runecrafting(7, 600, "Craft 600 chaos runes", 562),
			runecrafting(8, 600, "Craft 600 nature runes", 561),
			runecrafting(9, 600, "Craft 600 law runes", 563),
			runecrafting(10, 600, "Craft 600 death runes", 560),
			runecrafting(11, 300, "Craft 300 blood runes", 565),
			runecrafting(12, 300, "Craft 300 soul runes", 566),
			runecrafting(13, 2000, "Craft 2000 runes", ANY),
			runecrafting(14, 10, "Craft 10 armadyl runes", 21773)
	),
	COOKING(11, "Cooking", 1231,
			cooking(0, 100, "Cook 100 raw chickens", 2138),
			cooking(1, 100, "Cook 100 raw beefs", 2132),
			cooking(2, 150, "Cook 150 raw shrimps", 317),
			cooking(3, 150, "Cook 150 raw anchovies", 321),
			cooking(4, 150, "Cook 150 raw trouts", 335),
			cooking(5, 140, "Cook 140 raw salmons", 331),
			cooking(6, 100, "Cook 100 raw karambwans", 3142),
			cooking(7, 150, "Cook 150 raw tunas", 359),
			cooking(8, 140, "Cook 140 raw swordfishes", 371),
			cooking(9, 150, "Cook 150 raw lobsters", 377),
			cooking(10, 135, "Cook 135 raw monkfishes", 7944),
			cooking(11, 125, "Cook 125 raw sharks", 383),
			cooking(12, 115, "Cook 115 raw manta rays", 389),
			cooking(13, 100, "Cook 100 raw rocktails", 15270)
	),
	WILDERNESS(12, "Wilderness", 1217,
			killOpponent(0, 4, "Kill 4 players in the Wilderness"),
			fightOpponentWith(1, 1, "Fight a player with Dragon claws in the Wilderness", 14484),
			fightOpponentWith(2, 1, "Fight a player with Dark bow in the Wilderness", 11235),
			fightOpponentWith(3, 1, "Fight a player with Dharok axe in the Wilderness", 4718),
			killOpponent(4, 1, "Kill a player in the Revenant cave").setCondition(new ZoneCondition(Location.create(9536, 10048), Location.create(9663, 10238))),
			killNpc(5, 60, "Kill 60 Revenants in the Revenant cave", KILL_REVENANTS).setCondition(new ZoneCondition(Location.create(9536, 10048), Location.create(9663, 10238))),
			killNpc(6, 40, "Kill 40 Demonic gorillas in the Revenant cave", 7151).setCondition(new ZoneCondition(Location.create(9536, 10048), Location.create(9663, 10238))),
			fightOpponentWith(7, 1, "Fight a player with Ice barrage in the Wilderness", FIGHT_WITH_ICE_BARRAGE),
			killOpponent(8, 3, "Kill 3 players in the Edgeville Wilderness").setCondition(new ZoneCondition(EdgePkZone.RECT.getSouthWest(), EdgePkZone.RECT.getNorthEast())),
			killNpc(9, 5, "Kill Venenatis 5 times", 4173).setCondition(new WildernessCondition()),
			killNpc(10, 5, "Kill Callisto 5 times", 4174).setCondition(new WildernessCondition()),
			killNpc(11, 5, "Kill Scorpia 5 times", 4172).setCondition(new WildernessCondition()),
			killNpc(12, 10, "Kill any Wilderness boss 10 times", KILL_WILDY_BOSS).setCondition(new WildernessCondition()),
			killNpc(13, 85, "Kill 85 Battle mages", KILL_BATTLE_MAGE).setCondition(new WildernessCondition()),
			killNpc(14, 35, "Kill 35 Lava dragons", 2011).setCondition(new WildernessCondition()),
			prayerBury(15, 60, "Bury 60 Lava dragon bones at Lava Dragon Isle", 17676).setCondition(new ZoneCondition(Location.create(3172, 3807), Location.create(3230, 3850))),
			killNpc(16, 85, "Kill 85 Lesser demons outside of KBD ladder", 82).setCondition(new ZoneCondition(Location.create(3008, 3840), Location.create(3022, 3855))),
			fightOpponentWith(17, 1, "Fight a player with Dragon dagger(p++) in wildy", 5698),
			killNpc(18, 100, "Kill 100 monsters in the Revenant cave", -1).setCondition(new ZoneCondition(Location.create(9536, 10048), Location.create(9663, 10238))),
			exchangeEmblem(19, 1, "Exchange 1 Ancient emblem at Emblem trader"),
			fightOpponentWith(20, 1, "Fight a player with a normal Granite maul in the Wilderness", 4153),
			earnElo(21, 25, "Earn 25 Elo rating"),
			killNpc(22, 35, "Kill 35 Frost dragons in the Wilderness", 51).setCondition(new WildernessCondition()),
			killNpc(23, 60, "Kill 60 Ice warriors in the Wilderness", 125).setCondition(new WildernessCondition()),
			killOpponent(24, 5, "Kill 5 players in the Wilderness"),
			killOpponent(25, 6, "Kill 6 players in the Wilderness"),
			killNpc(26, 5, "Kill Vet'ion 5 times", 4175).setCondition(new WildernessCondition()),
			killNpc(27, 5, "Kill Chaos Fanatic 5 times", 4190).setCondition(new WildernessCondition()),
			killNpc(27, 5, "Kill Crazy Archaeologist 5 times", 4186).setCondition(new WildernessCondition())
	),
	//TODO iconId
	THIEVING(13, "Thieving", 1225,
			pickpocket(0, 4_000_000, "Steal 4 million coins", ANY, TaskType.PICKPOCKET_GP),
			pickpocket(1, 4_000_000, "Steal 4 million coins in Edgeville", ANY, TaskType.PICKPOCKET_GP).setCondition(new ZoneCondition(Location.create(3071, 3485), Location.create(3122, 3527))),
			pickpocket(2, 1_000_000, "Steal 1 million coins from a food stall", 4875, TaskType.PICKPOCKET_GP),
			pickpocket(3, 200, "Steal 200 times from a scimitar stall", 4878, TaskType.PICKPOCKET_STALL),
			pickpocket(4, 200, "Steal 200 times from a gem stall", 2562, TaskType.PICKPOCKET_STALL),
			killNpc(5, 1, "Kill a theiving guard that appears when stealing from stalls", KILL_THIEV_GUARD),
			pickpocket(6, 300, "Steal 300 times from stalls in Ardougne", ANY, TaskType.PICKPOCKET_STALL).setCondition(new ZoneCondition(Location.create(2649, 3289), Location.create(2675, 3322))),
			pickpocket(7, 3_000_000, "Steal 3 million coins from guards in Wilderness", ANY, TaskType.PICKPOCKET_GP).setCondition(new WildernessCondition()),
			pickpocket(8, 150, "Pickpocket guards in the Wilderness 150 times", ANY, TaskType.PICKPOCKET).setCondition(new WildernessCondition()),
			pickpocket(9, 130, "Pickpocket 130 pkp from guards in Wilderness", ANY, TaskType.PICKPOCKET_PKP)
	);

	public static final List<TaskCategory> values = Arrays.asList(values());

	private final int id;
	private final String name;
	private final int iconId;
	private final List<Task> tasks;

	private TaskCategory(int id, String name, int iconId, Task... tasks) {
		if (tasks.length < 7) {
			throw new RuntimeException("Task category must have at least 7 tasks for each day of the week.");
		}

		this.id = id;
		this.name = name;
		this.iconId = iconId;
		this.tasks = Arrays.asList(tasks);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return this.name;
	}

	public int getIconId() {
		return this.iconId;
	}

	public List<Task> getTasks() {
		return this.tasks;
	}
	
	public static final Map<Integer, TaskCategory> map = new HashMap<>();
	
	public static TaskCategory forId(int id) {
		return map.getOrDefault(id, BOSS);
	}

	public static void load() {
		/* empty */
		for (TaskCategory cat : values) {
			map.put(cat.getId(), cat);
		}
	}

}
