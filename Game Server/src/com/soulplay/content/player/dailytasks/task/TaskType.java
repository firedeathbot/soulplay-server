package com.soulplay.content.player.dailytasks.task;

public enum TaskType {
	AGILITY(0),
	CRAFTING(1),
	CRAFTING_MOULD(2),
	FISHING(3),
	HERBLORE(4),
	MINING(5),
	PRAYER_BURY(6),
	PRAYER_ALTAR(7),
	FIREMAKING(8),
	WOODCUTTING(9),
	RUNECRAFTING(10),
	COOKING(11),
	NPC_BOSS_KILL(12),
	NPC_KILL(13),
	SMITHING_MAKE(14),
	SMITHING_BAR_TYPE(15),
	SMITHING_SMELT(16),
	OPPONENT_DEATH(17),
	FIGHT_OPPONENT(18),
	EARN_ELO(19),
	EXCHANGE_EMBLEM(20),
	VOTE(21),
	PICKPOCKET(22),
	PICKPOCKET_STALL(23),
	PICKPOCKET_GP(24),
	PICKPOCKET_PKP(25);

	private final int id;

	private TaskType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
