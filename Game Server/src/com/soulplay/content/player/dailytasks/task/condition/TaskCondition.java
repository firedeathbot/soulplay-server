package com.soulplay.content.player.dailytasks.task.condition;

import com.soulplay.game.model.player.Player;

public abstract class TaskCondition {

	public abstract boolean checkTrue(Player player);

}
