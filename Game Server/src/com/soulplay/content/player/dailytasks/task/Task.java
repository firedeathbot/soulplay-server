package com.soulplay.content.player.dailytasks.task;

import com.soulplay.content.player.dailytasks.task.condition.TaskCondition;

public abstract class Task {

	private final int id;
	private final int requiredCount;
	private final String desc;
	private final TaskType taskType;
	private TaskCondition condition;

	public Task(int id, int requiredCount, String desc, TaskType taskType) {
		this.id = id;
		this.requiredCount = requiredCount;
		this.desc = desc;
		this.taskType = taskType;
	}

	public int getId() {
		return id;
	}

	public int getRequiredCount() {
		return requiredCount;
	}

	public String getDesc() {
		return desc;
	}

	public TaskType getTaskType() {
		return taskType;
	}

	public Task setCondition(TaskCondition condition) {
		this.condition = condition;
		return this;
	}

	public TaskCondition getCondition() {
		return condition;
	}

	public abstract int triggerId();

}
