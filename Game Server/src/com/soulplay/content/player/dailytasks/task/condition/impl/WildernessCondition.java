package com.soulplay.content.player.dailytasks.task.condition.impl;

import com.soulplay.content.player.dailytasks.task.condition.TaskCondition;
import com.soulplay.game.model.player.Player;

public class WildernessCondition extends TaskCondition {

	private final int minWildyLevel;

	public WildernessCondition() {
		this(0);
	}

	public WildernessCondition(int minWildyLevel) {
		this.minWildyLevel = minWildyLevel;
	}

	@Override
	public boolean checkTrue(Player player) {
		return player.wildLevel > minWildyLevel;
	}

}
