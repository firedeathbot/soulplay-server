package com.soulplay.content.player.dailytasks.task.condition.impl;

import com.soulplay.content.player.dailytasks.task.condition.TaskCondition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;

public class ZoneCondition extends TaskCondition {

	private final Location bottomLeft, topRight;

	public ZoneCondition(Location bottomLeft, Location topRight) {
		this.bottomLeft = bottomLeft;
		this.topRight = topRight;

	}

	@Override
	public boolean checkTrue(Player player) {
		int x = player.absX;
		int y = player.absY;
		int z = player.heightLevel;
		return x >= bottomLeft.getX() && y >= bottomLeft.getY()
				&& x <= topRight.getX() && y <= topRight.getY();
	}

}
