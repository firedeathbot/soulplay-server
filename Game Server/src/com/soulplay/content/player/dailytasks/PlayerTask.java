package com.soulplay.content.player.dailytasks;

import com.soulplay.content.player.dailytasks.task.Task;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.game.model.player.Player;

public class PlayerTask {

	public static final int POINTS_FOR_COMPLETION_BONUS = 25;
	public static final int POINTS_FOR_COMPLETION = 50;
	public static final int POINTS_FOR_COMPLETION_ALL = 150;
	public static final int POINTS_FOR_COMPLETION_ALL_ALL = 300;

	private int completedCount;
	private final Task task;

	public PlayerTask(Task task) {
		this.task = task;
	}

	public PlayerTask(Task task, int completedCount) {
		this.task = task;
		this.completedCount = completedCount;
	}

	public void increaseComplete(int trigger, Player player, int count) {
		if (task.triggerId() != -1 && trigger != task.triggerId() || (task.getCondition() != null && !task.getCondition().checkTrue(player))) {
			return;
		}

		this.completedCount += count;
		this.completedCount = Math.min(completedCount, task.getRequiredCount());
		player.sendMessageSpam("You have made progress on '" + task.getDesc() + "' " + completedCount + "/" + task.getRequiredCount() + ".");
	}

	public boolean isCompleted() {
		return this.completedCount >= task.getRequiredCount();
	}

	public int getId() {
		return task.getId();
	}

	public TaskType getTaskType() {
		return task.getTaskType();
	}

	public int getRequiredCount() {
		return task.getRequiredCount();
	}
	
	public String getDesc() {
		return task.getDesc();
	}

	public int getCompletedCount() {
		return completedCount;
	}
	
	public void loadCompletedCount(int amount) {
		this.completedCount = amount;
	}
}
