package com.soulplay.content.player.dailytasks;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.DailyTasksSave;
import com.soulplay.game.model.player.save.SoulPlayDateSave;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.soulplaydate.SoulPlayDateEnum;

public class DailyTaskHandler {

	/**
	 * Check to reset the daily tasks on new week or not
	 */
	public static void newDayReset(Player p, int thisSoulplayDay) {
		int diff = Math.abs(thisSoulplayDay - p.getSoulplayDate().getWeeklyResetDay());
		if (diff >= 7) {
			DailyTasksSave.resetWeeklyTasks(p.mySQLIndex);
			p.categories = PlayerCategory.pickRandom5();
			int offset = (thisSoulplayDay - SoulPlayDate.SUNDAY_OFFSET) % 7;
			int lastSunday = thisSoulplayDay - offset;
			SoulPlayDateSave.setNewDay(p, lastSunday, SoulPlayDateEnum.WEEKLY_TASK_RESET);
		}

		p.bonusTask = null;
	}
	
	public static void cheaphax(Player p) {
		if (p.categories == null) { 
			p.categories = PlayerCategory.pickRandom5();
			System.out.println("Cheaphaxing daily tasks for playername: "+ p.getName());
		}
	}
}
