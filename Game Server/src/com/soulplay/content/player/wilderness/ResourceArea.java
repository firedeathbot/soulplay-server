package com.soulplay.content.player.wilderness;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;

public class ResourceArea {
	
	public static void payGate(final Player player, final GameObject object) {
		if (player.getY() < 3945) { // exit
			passGate(player, object);
			return;
		}
		player.getDialogueBuilder()
		.sendNpcChat(2463, "Would you like to enter the resource area for 200k coins?")
		.sendOption("Pay toll?", "Pay toll <col=ff0000>(200k)", () -> {
			if (!player.isDead()) {
				if (player.getItems().takeCoins(200_000)) {
					passGate(player, object);
					player.getDialogueBuilder().closeNew();
				} else {
					player.getDialogueBuilder().jumpToStage(2);
				}
			}
		}, "No.", ()-> {
			player.getDialogueBuilder().closeNew();
		})
		.sendNpcChat(2463, "Sorry you don't have that much money.")
		.executeIfNotActive();
	}
	
	private static void passGate(Player player, GameObject object) {
		
		Direction dir = player.getY() > 3944 ? Direction.SOUTH : Direction.NORTH;
		
		player.faceLocation(player.getY() > 3944 ? object.getLocation() : Location.create(3184, 3945));
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer con) {
				if (player.getX() != 3184) {
					if (!player.getMovement().isMovingOrHasWalkQueue())
						player.getPA().playerWalk(3184, player.getY());
					return;
				}
				
				con.stop();
				
				player.lockActions(2);
				player.lockMovement(2);
				ForceMovementMask mask = ForceMovementMask.createMask(0, dir.getStepY(), 1, 0, dir).setCustomSpeed(30, 0).setNewOffset(player.getX(), player.getY());
				

				
				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
					int ticks = 0;
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						
						ticks++;
						if (ticks > 5) {
							container.stop();
							return;
						}
						
						
						if (ticks == 1) {
							player.getPacketSender().addObjectPacket(-1, object.getLocation(), object.getType(), object.getOrientation());
							player.getPacketSender().addObjectPacket(object.getId(), object.getX(), object.getY()+1, object.getType(), 2);
							player.startAnimation(player.getMovement().getWalkAnim());
							player.setForceMovementRaw(mask);
						} else if (ticks == 2) {
							container.stop();
							player.getPA().movePlayerInstant(player.getX(), player.getY()+dir.getStepY(), player.getZ());
							player.getPacketSender().addObjectPacket(-1, object.getX(), object.getY()+1, object.getType(), 2);
							player.getPacketSender().addObjectPacket(object.getId(), object.getLocation(), object.getType(), object.getOrientation());
						}
						
					}
				}, 1);
				
			}
		}, 1);
		
	}

	public static final int PACKAGE_ITEM_ID = 290;

	public static void openChest(Player player, GameObject object) {
		if (!player.getItems().playerHasItem(21805)) {
			player.getDialogueBuilder().sendStatement("You don't have a key to open chest with.").executeIfNotActive();
			return;
		}

		player.getDialogueBuilder().sendStatement("You are about to open Wilderness chest. You will receive a package that", "can only be opened inside a safe zone. If you die or try to logout", "with it you will <col=ff0000>lose</col> it.").sendOption("I understand.", () -> {
			if (!player.getItems().playerHasItem(21805)) {
				return;
			}

			player.getItems().deleteItemInOneSlot(21805, 1);
			player.getItems().addItem(PACKAGE_ITEM_ID, 1);
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + player.getNameSmartUp() + " has just opened the Wilderness chest!");
		}, "I'm too scared to lose it.", () -> {}).execute();
	}
	
	public static boolean canLootChestBox(Player p) {
		if (p.wildLevel > 0 && p.combatLevel < 85) {
			p.sendMessage("Your combat level needs to be at least 85 to do this.");
			return false;
		}
		return true;
	}

	public static void dropPackages(Player player) {
		if (player.wildLevel <= 0) {
			return;
		}

		for (int i = 0, length = player.playerItems.length; i < length; i++) {
			int itemId = player.playerItems[i] - 1;
			if (itemId != PACKAGE_ITEM_ID) {
				continue;
			}

			player.getItems().deleteItemFromSlot(i, 1);
			ItemHandler.createGroundItem(new GroundItem(PACKAGE_ITEM_ID, player.absX, player.absY, player.getHeightLevel(), 1, 500), true);
		}
	}

}
