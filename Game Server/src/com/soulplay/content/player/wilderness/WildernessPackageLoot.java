package com.soulplay.content.player.wilderness;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class WildernessPackageLoot {
	// itemId, minAmount, maxAmount

	public static final int[][] COMMON = {
			{ 14484, 1, 1 }, // dragon claws
			{ 11694, 1, 1 }, // armadyl godsword
			{ 30093, 40, 50 }, // dragon thrownaxe
			{ 30095, 1, 1 }, // dexterous scroll
			{ 30096, 1, 1 }, // arcane scroll
			{ 995, 35_000_000, 150_000_000 }, // coins
			{ 995, 35_000_000, 150_000_000 }, // coins
			{ 995, 35_000_000, 150_000_000 }, // coins
	};

	public static final int[][] UNCOMMON = {
			{ 30264, 1, 1 }, // elder chaos top
			{ 30265, 1, 1 }, // elder chaos robe
			{ 30266, 1, 1 }, // elder chaos hood
			{ 30090, 1, 1 }, // elder maul
			{ 995, 150_000_000, 350_000_000 }, // coins
			{ 995, 150_000_000, 350_000_000 }, // coins
			{ 995, 150_000_000, 350_000_000 }, // coins
	};
	
	public static final int[][] RARE = {
			{ 30267, 1, 1 }, // bounty teleport scroll
			{ 995, 500_000_000, 600_000_000 }, // coins
			{ 995, 500_000_000, 600_000_000 }, // coins
			{ 22314, 1, 1 }, // Gorilla Mask
			{ 30189, 1, 1 }, // Black Demon Mask
			{ 30187, 1, 1 }, // Lesser Demon Mask
			{ 30190, 1, 1 }, // Old Demon Mask
			{ 30188, 1, 1 }, // Greater Demon Mask
	};

	public static final int[][] ULTRA_RARE = {
			{ 1419, 1, 1 }, // scythe
			{ 30327, 1, 1 }, // scythe of vitur
			{ 30092, 1, 1 }, // twisted bow
	};

	public static void loot(Player p) {
		int random = Misc.randomNoPlus(1000);
		if (random <= 1) {
			loot(p, ULTRA_RARE);
		} else if (random <= 25) {
			loot(p, RARE);
		} else if (random <= 150) {
			loot(p, UNCOMMON);
		} else if (random <= 1000) {
			loot(p, COMMON);
		}
	}

	private static void loot(Player c, int[][] items) {
		int random = Misc.randomNoPlus(items.length);

		c.getItems().addOrDrop(new GameItem(items[random][0], Misc.randomNoPlus(items[random][1], items[random][2])));

		c.sendMessage("You have received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from the Wilderness package!");
		PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + c.getNameSmartUp() + " has received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from the Wilderness package!");
	}

}
