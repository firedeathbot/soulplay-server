package com.soulplay.content.player.holiday.halloween;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class Halloween {
	
	public static final int zombieShirt = 7592;
	public static final int zombieTrouser = 7593;
	public static final int zombieGloves = 7595;
	public static final int zombieBoots = 7596;

	public static final int trickEmote = 10530;
	public static final int trickGfx = 1864;
	public static final int zombieHandEmote = 7272;
	public static final int zombieHandGfx = 1244;
	public static final int zombieWalkEmote = 3544;
	public static final int zombieDanceEmote = 3543;

	public static final int blueSweets = 4558;

	public static final int deepBlueSweets = 4559;

	public static final int whiteSweets = 4560;

	public static final int purpleSweets = 4561;

	public static final int redSweets = 4562;

	public static final int greenSweets = 4563;

	public static final int pinkSweets = 4564;

	public static final int wrappedCandy = 14084;

	public static final int popcornBall = 14082;

	public static final int chocolateDrop = 14083;

	public static final int mintCake = 9475;

	public static final int chocchipCrunchies = 2209;

	public static final int chocIce = 6794;

	public static final int premadeChocBomb = 2229;

	public static boolean halloweenAhab(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieWalkEmote);
		c.getMovement().resetWalkingQueue();
		if (!(c.getItems().playerHasItem(chocchipCrunchies))) {
			c.getItems().addItem(chocchipCrunchies, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenBetty(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(trickEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(trickGfx));
		if (!(c.getItems().playerHasItem(purpleSweets))) {
			c.getItems().addItem(purpleSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenBrian(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieHandEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(zombieHandGfx));
		if (!(c.getItems().playerHasItem(popcornBall))) {
			c.getItems().addItem(popcornBall, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenChancy(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(trickEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(trickGfx));
		if (!(c.getItems().playerHasItem(whiteSweets))) {
			c.getItems().addItem(whiteSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenDaVinci(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieHandEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(zombieHandGfx));
		if (!(c.getItems().playerHasItem(deepBlueSweets))) {
			c.getItems().addItem(deepBlueSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenGerrant(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(trickEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(trickGfx));
		if (!(c.getItems().playerHasItem(redSweets))) {
			c.getItems().addItem(redSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenGrum(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieDanceEmote);
		c.getMovement().resetWalkingQueue();
		if (!(c.getItems().playerHasItem(premadeChocBomb))) {
			c.getItems().addItem(premadeChocBomb, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenHetty(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieWalkEmote);
		c.getMovement().resetWalkingQueue();
		if (!(c.getItems().playerHasItem(chocolateDrop))) {
			c.getItems().addItem(chocolateDrop, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenHops(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieHandEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(zombieHandGfx));
		if (!(c.getItems().playerHasItem(greenSweets))) {
			c.getItems().addItem(greenSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenMonkOfEntrana(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(trickEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(trickGfx));
		if (!(c.getItems().playerHasItem(chocIce))) {
			c.getItems().addItem(chocIce, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenRommik(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(trickEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(trickGfx));
		if (!(c.getItems().playerHasItem(blueSweets))) {
			c.getItems().addItem(blueSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenShopAssistant(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieDanceEmote);
		c.getMovement().resetWalkingQueue();
		if (!(c.getItems().playerHasItem(pinkSweets))) {
			c.getItems().addItem(pinkSweets, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenShopkeeper(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(zombieHandEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(zombieHandGfx));
		if (!(c.getItems().playerHasItem(wrappedCandy))) {
			c.getItems().addItem(wrappedCandy, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}

	public static boolean halloweenWydin(Player c) {
		if (System.currentTimeMillis()
				- c.getTempVar().getLong("EMOTE") < 6000) {
			return false;
		}
		c.getTempVar().put("EMOTE", System.currentTimeMillis());
		c.startAnimation(trickEmote);
		c.getMovement().resetWalkingQueue();
		c.startGraphic(Graphic.create(trickGfx));
		if (!(c.getItems().playerHasItem(mintCake))) {
			c.getItems().addItem(mintCake, 1);
		}
		c.forceChat("Trick Or Treat!");
		return false;
	}
	
	public static void zombiePieces(Player c) {
		if (Misc.random(15) == 1) {
			if (!c.getItems().playerHasItem(zombieGloves) && !c.getItems().playerHasEquipped(zombieGloves) && !c.getItems().bankHasItemAmount(zombieGloves, 1)) {
				c.getItems().addItem(zombieGloves, 1);
			} else if (!c.getItems().playerHasItem(zombieBoots) && !c.getItems().playerHasEquipped(zombieBoots) && !c.getItems().bankHasItemAmount(zombieBoots, 1)) {
				c.getItems().addItem(zombieBoots, 1);
			} else if (!c.getItems().playerHasItem(zombieShirt) && !c.getItems().playerHasEquipped(zombieShirt) && !c.getItems().bankHasItemAmount(zombieShirt, 1)) {
				c.getItems().addItem(zombieShirt, 1);
			} else if (!c.getItems().playerHasItem(zombieTrouser) && !c.getItems().playerHasEquipped(zombieTrouser) && !c.getItems().bankHasItemAmount(zombieTrouser, 1)) {
				c.getItems().addItem(zombieTrouser, 1);
			} else
				c.sendMessage("You have received all of the zombie pieces");

		}
		
	}

}
