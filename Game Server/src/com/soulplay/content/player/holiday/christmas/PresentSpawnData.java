package com.soulplay.content.player.holiday.christmas;

import com.soulplay.game.world.entity.Location;

public enum PresentSpawnData {
	VARROCK_SPAWN(new Location[] {
			Location.create(3263, 3404),
			Location.create(3267, 3450),
			Location.create(3248, 3493),
			Location.create(3222, 3475),
			Location.create(3198, 3470),
			Location.create(3212, 3447),
			Location.create(3176, 3404),
			Location.create(3183, 3408),
			Location.create(3199, 3419),
			Location.create(3223, 3398),
			Location.create(3222, 3430),
			Location.create(3213, 3448),
			Location.create(3238, 3436),
			Location.create(3234, 3390),
			Location.create(3185, 3438),
			Location.create(3254, 3420),
	}, "Varrock"),
	FALADOR_SPAWN(new Location[] {
			Location.create(2962, 3380),
			Location.create(2990, 3384),
			Location.create(3021, 3371),
			Location.create(3039, 3356),
			Location.create(3044, 3380),
			Location.create(3021, 3337),
			Location.create(3017, 3356),
			Location.create(2981, 3363),
			Location.create(2946, 3370),
			Location.create(2952, 3390),
			Location.create(2979, 3340),
			Location.create(3002, 3326),
			Location.create(2993, 3365),
			Location.create(2979, 3384),
			Location.create(2946, 3334),
			Location.create(3049, 3342),
		}, "Falador"),
	ARDOUGNE_SPAWN(new Location[] {
			Location.create(2666, 3303),
			Location.create(2639, 3293),
			Location.create(2611, 3309),
			Location.create(2620, 3334),
			Location.create(2618, 3269),
			Location.create(2653, 3281),
			Location.create(2651, 3292),
			Location.create(2641, 3315),
			Location.create(2634, 3320),
			Location.create(2615, 3316),
			Location.create(2600, 3314),
			Location.create(2615, 3292),
			Location.create(2668, 3269),
			Location.create(2670, 3323),
			Location.create(2647, 3338),
			Location.create(2624, 3302),
		}, "Ardougne"),
	EDGEVILLE_SPAWN(new Location[] {
			Location.create(3079, 3511),
			Location.create(3096, 3512),
			Location.create(3110, 3517),
			Location.create(3104, 3501),
			Location.create(3088, 3508),
			Location.create(3086, 3490),
			Location.create(3094, 3497),
		}, "Edgeville"),
	YANILLE_SPAWN(new Location[] {
			Location.create(2612, 3095),
			Location.create(2586, 3090),
			Location.create(2567, 3085),
			Location.create(2548, 3079),
			Location.create(2552, 3100),
			Location.create(2568, 3103),
			Location.create(2617, 3097),
		}, "Yanille"),
	;

	public static final PresentSpawnData[] values = values();
	private final Location[] location;
	private final String desc;

	private PresentSpawnData(Location[] location, String desc) {
		this.location = location;
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public Location[] getLocation() {
		return location;
	}

}
