package com.soulplay.content.player.holiday.christmas;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;

public class PresentSpawn {
	
	public static final int PRESENT_ID = 15420;
	public static List<GroundItem> items = new ArrayList<>();
	public static int rotation = -1;

	public static void spawn() {
		rotation++;
		if (rotation >= PresentSpawnData.values.length) {
			rotation = 0;
		}

		PresentSpawnData data = PresentSpawnData.values[rotation];
		for (Location location : data.getLocation()) {
			GroundItem saraBucket2 = new GroundItem(PRESENT_ID, location.getX(), location.getY(), location.getZ(), 1, 0);
			saraBucket2.removeTicks = Integer.MAX_VALUE;
			ItemHandler.createGroundItem(saraBucket2, true);
			items.add(saraBucket2);
		}
	}

	public static void checkPresentRemove(GroundItem item) {
		if (item.getItemId() == PresentSpawn.PRESENT_ID) {
			PresentSpawn.items.remove(item);
		}
		
		if (!items.isEmpty()) {
			return;
		}
		
		spawn();
	}

}
