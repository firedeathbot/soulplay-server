package com.soulplay.content.player;

import com.soulplay.game.model.player.Player;

public enum DonatorRank {

	NONE("None", 0, 2, 20, 600_000),
	DONATOR("Donator", 10, 3, 20, 500_000),
	SUPER("Super", 50, 5, 15, 400_000),
	SUPREME("Supreme", 250, 10, 15, 300_000),
	LEGENDARY("Legendary", 1000, 20, 10, 200_000),
	UBER("Uber", 5000, 30, 5, 100_000),
	INSANE("Insane", 10000, 50, 1, 0);

	private final String name;
	private final int amount, maxPresets, warriorGuildTokensConsumed, itemReclaimPrice;

	private DonatorRank(String name, int amount, int maxPresets, int warriorGuildTokensConsumed, int itemReclaimPrice) {
		this.name = name;
		this.amount = amount;
		this.maxPresets = maxPresets;
		this.warriorGuildTokensConsumed = warriorGuildTokensConsumed;
		this.itemReclaimPrice = itemReclaimPrice;
	}

	public static DonatorRank check(int donatedTotal) {
		if (donatedTotal >= 10000) {
			return INSANE;
		} else if (donatedTotal >= 5000) {
			return UBER;
		} else if (donatedTotal >= 1000) {
			return LEGENDARY;
		} else if (donatedTotal >= 250) {
			return SUPREME;
		} else if (donatedTotal >= 50) {
			return SUPER;
		} else if (donatedTotal >= 10) {
			return DONATOR;
		}

		return NONE;
	}

	public static DonatorRank check(Player player) {
		return check(player.getDonatedTotalAmount());
	}

	public static DonatorRank nextRank(DonatorRank thisRank) {
		switch (thisRank) {
			case NONE:
				return DONATOR;
			case DONATOR:
				return SUPER;
			case SUPER:
				return SUPREME;
			case SUPREME:
				return LEGENDARY;
			case LEGENDARY:
				return UBER;
			case UBER:
				return INSANE;
			case INSANE:
				return INSANE;
			default:
				return NONE;
		}
	}

	public String getName() {
		return name;
	}

	public int getAmount() {
		return amount;
	}

	public int getMaxPresets() {
		return maxPresets;
	}

	public int getWarriorGuildTokensConsumed() {
		return warriorGuildTokensConsumed;
	}

	public int getItemReclaimPrice() {
		return itemReclaimPrice;
	}

}
