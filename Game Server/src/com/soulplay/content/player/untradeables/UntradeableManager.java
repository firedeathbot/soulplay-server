package com.soulplay.content.player.untradeables;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import com.soulplay.config.WorldType;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ShopHandler;

public final class UntradeableManager {

	public static final int SHOP_ID = 180;

	public static String getMapToSave(final Player p) {
		String save = "";
		for (Entry<Integer, Integer> item : p.getUntradeableShopMap()
				.entrySet()) {
			save = save + item.getKey().toString() + ":";
			save = save + item.getValue().toString() + ";";
		}
		return save;
	}

	public static void loadMap(final Player p, String array) {
		if (array == null || array.isEmpty() || array.equals("")) {
			return;
		}
		String[] s = array.split(";");
		try {
			for (String element : s) {
				if (element.isEmpty()) {
					continue;
				}
				String[] s2 = element.split(":");
				p.getUntradeableShopMap().put(Integer.parseInt(s2[0]),
						Integer.parseInt(s2[1]));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static boolean isVoidGear(int id) {
		return voidGearSet.contains(id);
	}
	
	private static final int[] voidGearInts = new int[] {
		8839, 8840, 8842, 10611, 11663, 11664, 11665, 11674, 11675, 11676, 19712, 19785, 19786, 19787, 19788, 19789, 19790, 19803, 19804
	};
	
	public static Set<Integer> voidGearSet = new HashSet<>();
	
	static {
		for (int i = 0; i < voidGearInts.length; i++) {
			voidGearSet.add(voidGearInts[i]);
		}
	}

	public static int price(int id) {
		switch (id) {
			case 18349: // chaotic rapier
			case 18351: // chaotic longsword
			case 18353: // chaotic maul
			case 18355: // chaotic staff
			case 18357: // chaotic crossbow
				return 15_000_000;
			case 21467: // trickster helm
			case 21468: // trickster robe
			case 21469: // trickster robe legs
			case 21470: // trickster gloves
			case 21471: // trickster boots
			case 21547: // trickster helm
			case 21472: // vanguard helm
			case 21473: // vanguard body
			case 21474: // vanguard legs
			case 21475: // vanguard gloves
			case 21476: // vanguard boots
			case 21462: // battle mage helm
			case 21463: // battle mage robe
			case 21464: // battle mage robe legs
			case 21465: // battle mage gloves
			case 21466: // battle mage boots
				return 12_500_000;
			case 18359: // chaotic kiteshield
			case 18363: // farseer kiteshield
			case 18361: // eagle-eye kiteshield
				return 7_500_000;
			case 20769: // Completionist cape
			case 14001: // Completionist cape
			case 14002: // Completionist cape
			case 14003: // Completionist cape
			case 14004: // Completionist cape
			case 14005: // Completionist cape
			case 14006: // Completionist cape
			case 14007: // Completionist cape
			case 14008: // Completionist cape
			case 14009: // Completionist cape
			case 14010: // Completionist cape
			case 14011: // Completionist cape
			case 14012: // Completionist cape
			case 14013: // Completionist cape
			case 14014: // Completionist cape
			case 14015: // Completionist cape
			case 14016: // Completionist cape
			case 14017: // Completionist cape
			case 14018: // Completionist cape
			case 14019: // Completionist cape
			case 14020: // Completionist cape
			case 14021: // Completionist cape
			case 14022: // Completionist cape
			case 14023: // Completionist cape
			case 14024: // Completionist cape
			case 13661: // inferno adze
				return 5_000_000;
			case 20767: // max cape
				return 3_000_000;
			case 23639: // tokhaar-kal
				return 2_500_000;
			case 19785: // elite void knight top
			case 19786: // elite void knight robe
			case 19787: // elite void knight top
			case 19788: // elite void knight robe
			case 19789: // elite void knight top
			case 19790: // elite void knight robe
			case 19803: // elite void knight top
			case 19804: // elite void knight robe
				return 3_000_000;
			case 11663: // void mage helm
			case 11664: // void ranger helm
			case 11665: // void melee helm
			case 8842: // void knight gloves
			case 8839: // void knight top
			case 8840: // void knight robe
				return 2_500_000;
			case 20795: // duelist�s cap
			case 20796: // duelist�s cap
			case 20797: // duelist�s cap
			case 20798: // duelist�s cap
			case 20799: // duelist�s cap
			case 20800: // duelist�s cap
			case 20770: // Completionist hood
				return 1_000_000;
			case 17273: // flameburst defender
				return 750_000;
			case 10548: // fighter hat
			case 20072: // dragon defender
			case 10551: // fighter torso
			case 7462: // barrows gloves
			case 6570: // fire cape
			case 20768: // max cape hood
				return 500_000;
			case 3842: // unholy book
			case 3844: // book of balance
			case 3840: // holy book
			case 8850: // rune defender
				return 300_000;
			case 8849: // adamant defender
				return 250_000;
			case 10499: // ava�s accumulator
			case 10498: // ava�s attractor
				return 30_000;
			case 8848: // mithril defender
				return 200_000;
			case 8847: // black defender
			case 9005: // fancy boots
			case 9006: // fighting boots
				return 150_000;
			case 8846: // steel defender
				return 125_000;
			case 8845: // iron defender
			case 9748: // attack cape (t)
			case 9754: // defence cape (t)
			case 9751: // strength cape (t)
			case 9769: // onstitution cape (t)
			case 9757: // ranging cape (t)
			case 9760: // prayer cape (t)
			case 9763: // magic cape (t)
			case 9802: // cooking cape (t)
			case 9808: // woodcutting cape (t)
			case 9784: // fletching cape (t)
			case 9799: // fishing cape (t)
			case 9805: // firemaking cape (t)
			case 9781: // crafting cape (t)
			case 9796: // smithing cape (t)
			case 9793: // mining cape (t)
			case 9775: // herblore cape (t)
			case 9772: // agility cape (t)
			case 9778: // theiving cape (t)
			case 9787: // slayer cape (t)
			case 9811: // farming cape (t)
			case 9766: // runecrafting cape (t)
			case 9749: // attack hood
			case 9755: // defence hood
			case 9752: // strength hood
			case 9770: // constitution hood
			case 9758: // ranging hood
			case 9761: // prayer hood
			case 9764: // magic hood
			case 9803: // cooking hood
			case 9809: // woodcutting hood
			case 9785: // fletching hood
			case 9800: // fishing hood
			case 9806: // firemaking hood
			case 9782: // crafting hood
			case 9797: // smithing hood
			case 9794: // mining hood
			case 9776: // herblore hood
			case 9773: // agility hood
			case 9779: // theiving hood
			case 9788: // slayer hood
			case 9812: // farming hood
			case 9767: // runecrafting hood
			case 9747: // attack cape
			case 9753: // defence cape
			case 9750: // strength cape
			case 9768: // constitution cape
			case 9756: // ranging cape
			case 9759: // prayer cape
			case 9762: // magic cape
			case 9801: // cooking cape
			case 9807: // woodcutting cape
			case 9783: // fletching cape
			case 9798: // fishing cape
			case 9804: // firemaking cape
			case 9780: // crafting cape
			case 9795: // smithing cape
			case 9792: // mining cape
			case 9774: // herblore cape
			case 9771: // agility cape
			case 9777: // theiving cape
			case 9786: // slayer cape
			case 9810: // farming cape
			case 9765: // runecrafting cape
			case 9948: // hunter cape
			case 9949: // hunter cape (t)
			case 12169: // summoning cape
			case 12170: // summoning cape (t)
			case 18508: // dungeoneering cape
			case 18509: // dungeoneering cape (t)
			case 18510: // dungeoneering hood
			case 9789: // construction cape
			case 9790: // construction cape (t)
			case 9791: // construction hood
				return 100_000;
			case 8844: // bronze defender
				return 50_000;
			default:
				return 0;
		}
	}

	public static void add(Client c, int itemID, int amount) {
		int currentAmount = c.getUntradeableShopMap().getOrDefault(itemID, 0);
		c.getUntradeableShopMap().put(itemID, currentAmount + amount);
	}

	public static void buy(Client c, int itemID, int amount, int slot) {
		int itemAmount = c.getUntradeableShopMap().getOrDefault(itemID, 0);
		if (itemAmount <= 0) {
			// c.sendMessage("Haxor");
			return;
		}
		if (amount > itemAmount) {
			amount = itemAmount;
		}
		if (c.getItems().freeBankSlots() < amount) {
			c.sendMessage("You need to free up some bank space before buying!");
			return;
		}

		int price = price(itemID) * amount;
		int bloodPrice = price(itemID) * amount / 20000;
		int amountToPlayer = (int) Math.ceil(price * 0.75);
		if (WorldType.equalsType(WorldType.SPAWN)) {
			if (c.getItems().playerHasItem(8890, bloodPrice)
					&& c.getItems().deleteItemInOneSlot(8890, bloodPrice)) {
				int remaining = itemAmount - amount;
				if (remaining > 0) {
					c.getUntradeableShopMap().put(itemID, remaining);
				} else {
					c.getUntradeableShopMap().remove(itemID);
				}
				if (!c.getItems().addOrBankItem(itemID, amount)) {
					c.sendMessage("Some reason couldn't add item to bank...");
				}
				c.getItems().updateInventory();

				// check for 2b max crap
				long amount2 = amountToPlayer;
				long dropAmount = c.getUntradeableGoldToDrop();
				if (amount2 + dropAmount < Integer.MAX_VALUE) {
					c.setUntradeableGold(
							c.getUntradeableGoldToDrop() + amountToPlayer); // coinsToDrop
																			// +=
																			// amountToPlayer;
				}
			} else {
				c.sendMessage("You need " + bloodPrice
						+ " blood money to recover that item.");
			}
		} else {
			if (c.getItems().takeCoins(price)) {
				int remaining = itemAmount - amount;
				if (remaining > 0) {
					c.getUntradeableShopMap().put(itemID, remaining);
				} else {
					c.getUntradeableShopMap().remove(itemID);
				}
				if (!c.getItems().addOrBankItem(itemID, amount)) {
					c.sendMessage("Some reason couldn't add item to bank...");
				}
				c.getItems().updateInventory();

				// check for 2b max crap
				long amount2 = amountToPlayer;
				long dropAmount = c.getUntradeableGoldToDrop();
				if (amount2 + dropAmount < Integer.MAX_VALUE) {
					c.setUntradeableGold(
							c.getUntradeableGoldToDrop() + amountToPlayer); // coinsToDrop
																			// +=
																			// amountToPlayer;
				}
			} else {
				c.sendMessage(
						"You need " + price + " coins to recover that item..");
			}
		}

		c.getItems().resetItems(3823);
		refresh(c);
	}

	public static void open(Client c) {
		c.getItems().resetItems(3823);
		c.isShopping = true;
		c.myShopId = 180;
		c.getPacketSender().sendFrame248(3824, 3822);
		c.getPacketSender().sendFrame126("Untradeable Recovery", 3901);

		refresh(c);
	}

	public static void refresh(Client c) {
		int amountOfItems = c.getUntradeableShopMap().size();
		if (amountOfItems > ShopHandler.MaxShopItems) {
			amountOfItems = ShopHandler.MaxShopItems;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(3900);
		c.getOutStream().writeShort(amountOfItems);
		for (Entry<Integer, Integer> item : c.getUntradeableShopMap()
				.entrySet()) {
			c.getOutStream().writeByteOrIntV2(item.getValue());
			c.getOutStream().write3Byte(item.getKey() + 1);
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

}
