package com.soulplay.content.player.namechange;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.util.Misc;
import com.soulplay.util.NameUtil;
import com.soulplay.util.log.NameChangeLog;

import plugin.button.infotab.InfoTabButtonPlugin;

public class NameChange {
	
	private static final int REDEEM_SCROLL = 253052;
	private static final int LOOKUP_NAME = 253050;
	private static final int CONFIRM_NAME = 253063;
	private static final int OPEN = 250207;
	private static final int CLOSE = 251250;
	private static final int CONFIRM_LAYER = 64830;
	private static final int STATUS_TEXT = 64823;
	
	private static final int MIN_DAYS_LIMIT = 2;
	private static final int MAX_DAYS_LIMIT = 29;

	public static void reset(Client player) {
		player.getPacketSender().sendString("", STATUS_TEXT);
		player.getPacketSender().sendString("Look up name", 64821);
		player.getPacketSender().setVisibility(CONFIRM_LAYER, true);
	}
	
	public static boolean buttonClick(Client player, int buttonId) {
		switch(buttonId) {
		
		case OPEN: {
			reset(player);
			player.getPacketSender().setSidebarInterface(11, 64800);
			
			final int currentDayOfYear = Server.getCalendar().getDayOfYear();
			int daysOff = currentDayOfYear - player.getNameChangeDate();
			if (daysOff < 0)
				daysOff += 365; // on new year, give them a free name change just because poop
			
			if (daysOff < MIN_DAYS_LIMIT) {
				final int daysLeft = MIN_DAYS_LIMIT - daysOff;
				if (daysLeft == 1) {
					player.getPacketSender().sendString("1 day", 64822);
				} else {
					player.getPacketSender().sendString(daysLeft + " days", 64822);
				}
			} else {
				player.getPacketSender().sendString("---", 64822);
			}
			player.getPacketSender().sendString(Integer.toString(player.getExtraNameChange()), 64813);
			
			if (daysOff < MAX_DAYS_LIMIT) {
				final int daysLeft = MAX_DAYS_LIMIT - daysOff;
				if (daysLeft == 1) {
					player.getPacketSender().sendString("1 day", 64811);
				} else {
					player.getPacketSender().sendString(daysLeft + " days", 64811);
				}
			} else 
				player.getPacketSender().sendString("Now!", 64811);
			return true;
		}
		case CLOSE:
			player.getPacketSender().setSidebarInterface(11, 64000);
			player.getPacketSender().sendString("---", 64822);
			return true;
			
		case CONFIRM_NAME: {
			if (Config.RUN_ON_DEDI && Config.NODE_ID != 1) {
				player.sendMessage("Name change can only be done on world 1.");
				return true;
			}
			if (player.nameReadyToChange == null) {
				return true;
			}
			player.getDialogueBuilder().sendOption("Change your name to: " + player.nameReadyToChange + " ?", "Yes, change my name.", () -> {
				confirmDisplayNameChangePrereq(player, player.nameReadyToChange);
			}, "No, I want to keep current one.", () -> {
				reset(player);
				player.nameReadyToChange = null;
			}).execute();
			return true;
		}
		
		case LOOKUP_NAME: {
			player.nameReadyToChange = null;

			player.getDialogueBuilder().sendEnterText(() -> {
				final String name = player.getDialogueBuilder().getEnterText();
				
				boolean valid = true;
				
				int size = name.length();
				if (size < 4 || size > 12)
					valid = false;
				
				if (!valid) {
					player.getPacketSender().sendString("<col=ffffff>Name rejected", STATUS_TEXT);
					return;
				}

				testNameTaken(player, name);
				
			}).execute();
			return true;
		}
		
		case REDEEM_SCROLL:
			if (!player.getItems().playerHasItem(786) && !player.getItems().playerHasItem(1505) && !player.getItems().playerHasItem(2396)) {
				player.sendMessage("You don't have any scrolls to redeem.");
				return true;
			}
			player.getDialogueBuilder().sendOption("Yes, redeem scroll into extra name change.", () -> {
				if (player.getItems().playerHasItem(786)) {
					player.getItems().deleteItemInOneSlot(786, 1);
				} else if (player.getItems().playerHasItem(1505)) {
					if (player.getPA().freeSlots() > 2) {
					    player.getItems().deleteItemInOneSlot(1505, 1);
					    player.getItems().addItem(786, 4);
					} else
						player.sendMessage("Not enough space to redeem your scoll into extra name change.");
				} else if (player.getItems().playerHasItem(2396)) {
					if (player.getPA().freeSlots() > 3) {
					    player.getItems().deleteItemInOneSlot(2396, 1);
					    player.getItems().addItem(1505, 1);
					    player.getItems().addItem(786, 4);
					} else
						player.sendMessage("Not enough space to redeem your scoll into extra name change.");
				}
				player.setExtraNameChange(player.getExtraNameChange() + 1);
				player.getPacketSender().sendString(""+ player.getExtraNameChange(), 64813);
				player.getPA().closeAllWindows();
			}, "No, don't redeem scroll into extra name change.", () -> {
				player.getPA().closeAllWindows();
			}).execute();
		    return true;
		}

		return false;
	}
	
	private static boolean canChangeName(Client player) {
		final int currentDayOfYear = Server.getCalendar().getDayOfYear();
		int daysOff = currentDayOfYear - (player.getNameChangeDate());
		if (daysOff < 0)
			daysOff += 365;
		if (daysOff >= MIN_DAYS_LIMIT && (daysOff >= MAX_DAYS_LIMIT || player.getExtraNameChange() != 0)) {
			return true;
		}

		return false;
	}
	
	public static void changeStatus(Client player, boolean taken, final String name) {
		if (taken) {
			player.getPacketSender().sendString("<col=ffffff>Name taken", STATUS_TEXT);
			player.getPacketSender().sendString("", 64832);
		} else {

			if (canChangeName(player)) {
				player.getPacketSender().sendString("", 64822);
				player.getPacketSender().setVisibility(CONFIRM_LAYER, false);
				player.nameReadyToChange = name;
			}
			
			player.getPacketSender().sendString("<col=00ff00>Not taken", STATUS_TEXT);
			player.getPacketSender().sendString(Misc.formatPlayerName(name), 64832);
			player.getPacketSender().sendString("Another name", 64821);
		}
	}
	
	private static final String QUERY = "SELECT PlayerName, display_name from players.accounts WHERE PlayerName = ? OR display_name = ? LIMIT 1";
	
	private static final String GET_NAME_QUERY = "SELECT PlayerName, display_name from players.accounts WHERE ID = ? LIMIT 1";
	
	private static final String ADD_QUERY = "UPDATE `players`.`accounts` SET display_name = ? WHERE `ID` = ?";
	
	private static void testNameTaken(final Client player, final String name) {
		if (!player.getSpamTick().checkThenStart(5)) {
			player.sendMessage("Please slow down!");
			return;
		}
		
		if (canRevertName(player, name)) {
			changeStatus(player, false, name);
			return;
		}
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				
				final boolean taken = checkTakenSqlQuery(name);
				
				Server.getTaskScheduler().schedule(()-> {
					if (player.isActive) {
						changeStatus(player, taken, name);
					}
				});
				
				
			}
		});
	}
	
	private static boolean canRevertName(final Client player, final String name) {
		return player.getName().equalsIgnoreCase(name) && !player.getNameSmart().equalsIgnoreCase(name);
	}
	
	private static void confirmDisplayNameChangePrereq(final Client player, final String name) {
		if (!player.getSpamTick().checkThenStart(10)) {
			player.sendMessage("Please slow down!");
			return;
		}
		
		if (canRevertName(player, name)) {
			changeNameFinal(player);
			return;
		}
		
		player.startLogoutDelay();
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				
				final boolean taken = checkTakenSqlQuery(name);
				
				Server.getTaskScheduler().schedule(()-> {
					if (player.isActive) {
						if (taken)
							changeStatus(player, taken, name);
						else
							changeNameFinal(player);
					}
				});
				
				
			}
		});
	}
	
	private static void changeNameFinal(final Client player) {
		final String displayName = player.nameReadyToChange;
		player.getDialogueBuilder().sendStatement("Your display name request was accepted.").setClose(true).executeIfNotActive();
		player.getPacketSender().setSidebarInterface(11, 64000);
		reset(player);
		if (player.nameReadyToChange == null) {
			return;
		}
		
		final int currentDayOfYear = Server.getCalendar().getDayOfYear();
		int daysOff = currentDayOfYear - player.getNameChangeDate();
		if (daysOff < 0)
			daysOff += 365;
		
		if (daysOff > MAX_DAYS_LIMIT) {
		} else {
			player.setExtraNameChange(player.getExtraNameChange() - 1);
			player.getPacketSender().sendString(Integer.toString(player.getExtraNameChange()), 64813);
		}
		
		if (DBConfig.LOG_NAME_CHANGE) {
			NameChangeLog log = new NameChangeLog(player.mySQLIndex, player.getName(), player.getNameSmart(), displayName);
			Server.getLogWriter().addToNameChangeList(log);
		}

		if (player.getLongDisplayName() != 0) {
			PlayerHandler.getPlayerMapByDisplayName().remove(player.getLongDisplayName());
		}

		long name37 = NameUtil.encodeBase37(displayName);
		PlayerHandler.getPlayerMapByDisplayName().put(name37, player);

		player.setDisplayName(displayName);
		player.nameReadyToChange = null;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		player.saveNameChangedDate();
		saveDisplayName(player.mySQLIndex, player.getName(), displayName);
		InfoTabButtonPlugin.update(player);
	}
	
	// returns true if name is already in use. USE ONLY INSIDE TASKEXECUTOR
	public static boolean checkTakenSqlQuery(final String name) {
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(QUERY)) {
			ps.setString(1, name);
			ps.setString(2, name);
			try (ResultSet results = ps.executeQuery()) {
				return results.next();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private static final String NO_NAME = "server";
	public static String getNameSmartFromDB(final int mysqlIndex) {
		try (Connection connection = Server.getConnection();
				PreparedStatement ps = connection.prepareStatement(GET_NAME_QUERY)) {
			ps.setInt(1, mysqlIndex);
			try (ResultSet results = ps.executeQuery()) {
				if (results.next()) {
					String username = results.getString("PlayerName");
					String displayName = results.getString("display_name");
					if (displayName != null)
						return displayName;
					else
						return username;
				}
				return NO_NAME;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return NO_NAME;
	}
	
	private static void saveDisplayName(final int mysqlId, final String username, final String displayName) {
		
		PlayerSaveSql.SAVE_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {

				try (Connection connection = Server.getConnection();
						PreparedStatement ps = connection.prepareStatement(ADD_QUERY)) {
					ps.setString(1, displayName);
					ps.setInt(2, mysqlId);
					ps.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		//TODO: add name change to Logs db
	}

}
