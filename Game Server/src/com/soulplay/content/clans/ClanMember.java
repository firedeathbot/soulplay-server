package com.soulplay.content.clans;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class ClanMember {
	
	private final String username, displayName;
	private final long mID;
	
	public ClanMember(String username, String displayName, long sqlID) {
		this.username = username;
		this.displayName = displayName;
		this.mID = sqlID;
	}

	public String getUsername() {
		return username;
	}

	public String getDisplayName() {
		return displayName;
	}

	public long getMysqlID() {
		return mID;
	}

	public Player getPlayer() {
		return PlayerHandler.getPlayerByMID(mID);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ClanMember) {
			ClanMember other = (ClanMember) obj;
			return mID == other.mID;
		}
		return false;
	}

}
