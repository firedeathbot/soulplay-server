package com.soulplay.content.clans;

import java.io.File;
import java.util.LinkedList;

import com.mysql.cj.util.StringUtils;
import com.soulplay.Server;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

@SuppressWarnings({"unchecked", "rawtypes"})
public class ClanManager {

	// private static ExecutorService clanServices = Executors
	// .newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	public static LinkedList<Clan> clans = new LinkedList();

	public static Object clanLock = new Object();

	public static final int MAXIMUM_RANKED_MEMBERS = 90;

	// public ClanManager() {
	// clans = new LinkedList();
	// }

	public static void create(Client paramClient) {
		if (paramClient.getClan() != null) {
			paramClient.sendMessage(
					"<col=ff0000>You must leave your current clan-chat before making your own.");
			return;
		}

		// clanServices.submit(() -> {
		TaskExecutor.executeSQL(() -> {

			if (!SqlHandler.clanExistsMain(paramClient.getName())) {

				Server.getTaskScheduler().schedule(new Task() {

					@Override
					protected void execute() {
						// TODO Auto-generated method stub

						Clan localClan = new Clan(paramClient);
						synchronized (clanLock) {
							clans.add(localClan);
						}
						localClan.save();
						if (paramClient != null && paramClient.isActive) {
							localClan.addMember(paramClient);
							paramClient.getPA().setClanData();
							paramClient.sendMessage(
									"<col=FF0000>You may change your clan settings by clicking the 'Clan Setup' button.");
						}

						this.stop();
					}
				});

			} else {
				PlayerHandler.sendMessageMainThread(paramClient,
						"Clan already exists.");// paramClient.sendMessage("Clan
												// already
												// exists.");
			}
		});

	}

	public static void delete(Clan paramClan) {
		if (paramClan == null) {
			return;
		}
		// File localFile = new File("Data/clan/" + paramClan.getFounder()
		// + ".cla");
		// if (localFile.delete()) {
		// Client localClient = (Client) PlayerHandler.getPlayer(paramClan
		// .getFounder());
		// if (localClient != null) {
		// localClient.sendMessage("Your clan has been deleted.");
		// }
		// clans.remove(paramClan);
		// }

		try {
			SqlHandler.deleteClanMain(paramClan.getFounder());
			synchronized (clanLock) {
				clans.remove(paramClan);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static int getActiveClans() {
		return clans.size();
	}

	/*
	 * Grabs the clan by owner name of the clan
	 */
	public static Clan getClanByOwnerName(String clanOwnerName) { // TODO:
																	// Rename to
																	// getClanByOwnerName
		if (StringUtils.isNullOrEmpty(clanOwnerName)) {
			return null;
		}
		LinkedList<Clan> temp = new LinkedList();
		synchronized (clanLock) {
			temp.addAll(clans);
		}
		for (Clan c : temp/* clans */) { // for (int i = 0; i < clans.size();
											// i++) {
			if (c != null && c.getFounder().equalsIgnoreCase(clanOwnerName)) { // if
																				// (clans.get(i).getFounder().equalsIgnoreCase(clanOwnerName))
																				// {
				return c;
			}
		}
		temp = null;

		// Clan localClan = read(paramString);
		// if (localClan != null) {
		// clans.add(localClan);
		// return localClan;
		// }
		return null;
	}

	public static int getTotalClans() {
		File localFile = new File("/Data/clan/");
		return localFile.listFiles().length;
	}

	public static void joinClan(final Client c, final String clanOwnerName) {
		// c.sendMessage("Loading clan data...");
		if (c.isLoadingClan()) {
			return;
		}

		final Clan clan = ClanManager.getClanByOwnerName(clanOwnerName);
		if (clan != null) {
			clan.addMember(c);
			c.getPA().setClanData();
			return;
		}

		c.setLoadingClan(true);

		ClanManager.loadClan(clanOwnerName);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				Clan clan = /* Server.c */ClanManager
						.getClanByOwnerName(clanOwnerName);
				if (clan != null) {
					clan.addMember(c);
				} else if (clanOwnerName.equalsIgnoreCase(c.getName())) {
					/* Server.c */ClanManager.create(c);
				} else {
					c.sendMessage(Misc.formatPlayerName(clanOwnerName)
							+ " has not created a clan yet.");
				}
				c.getPA().setClanData();

				c.setLoadingClan(false);

				container.stop();

			}

			@Override
			public void stop() {

			}
		}, 4);

	}

	public static void loadClan(String clanOwnerName) {
		if (clanOwnerName == null || clanOwnerName.isEmpty()) {
			return;
		}

		// clanServices.submit(() -> {
		TaskExecutor.executeSQL(() -> {

			try {
				Clan localClan = SqlHandler.getClanMain(clanOwnerName);
				if (localClan != null) {
					synchronized (clanLock) {
						clans.add(localClan);
					}
					// return localClan;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	public static void loadClanPointsData() {
		LinkedList<Clan> temp = new LinkedList();
		synchronized (clanLock) {
			temp.addAll(clans);
		}
		for (Clan clan : temp /* clans */ ) {
			if (clan != null) {
				try {
					final long points = SqlHandler
							.getClanPoints(clan.getFounder());
					clan.compareAndSetClanPoints(clan.getClanPoints(), points);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void loadClanTag() {
		LinkedList<Clan> temp = new LinkedList();
		synchronized (clanLock) {
			temp.addAll(clans);
		}
		for (Clan clan : temp/* clans */) {
			if (clan != null) {
				try {
					String clanTag = SqlHandler.getClanTag(clan.getFounder());
					if (clanTag != null && clanTag.isEmpty()) {
						clan.setClanTag(null);
					} else {
						clan.setClanTag(clanTag);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void removeNonActiveClan(Clan clan) {
		LinkedList<Clan> temp = new LinkedList();
		synchronized (clanLock) {
			temp.addAll(clans);
		}
		if (temp.contains(clan)) {
			try {
				SqlHandler.saveClanMain(clan);
				synchronized (clanLock) {
					clans.remove(clan);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void saveAllActiveClans() {
		LinkedList<Clan> temp = new LinkedList();
		synchronized (clanLock) {
			temp.addAll(clans);
		}
		for (Clan clan : temp/* clans */) {
			if (clan != null) {
				try {
					SqlHandler.saveClanMain(clan);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	// public static void save(Clan paramClan) {
	// if (paramClan == null) {
	// return;
	// }
	// File localFile = new File("Data/clan/" + paramClan.getFounder()
	// + ".cla");
	// try {
	// RandomAccessFile localRandomAccessFile = new RandomAccessFile(
	// localFile, "rwd");
	//
	// localRandomAccessFile.writeUTF(paramClan.getTitle());
	// localRandomAccessFile.writeByte(paramClan.whoCanJoin);
	// localRandomAccessFile.writeByte(paramClan.whoCanTalk);
	// localRandomAccessFile.writeByte(paramClan.whoCanKick);
	// localRandomAccessFile.writeByte(paramClan.whoCanBan);
	// localRandomAccessFile.writeByte(paramClan.getEloRating());
	// if ((paramClan.rankedMembers != null)
	// && (paramClan.rankedMembers.size() > 0)) {
	// localRandomAccessFile
	// .writeShort(paramClan.rankedMembers.size());
	// for (int i = 0; i < paramClan.rankedMembers.size(); i++) {
	// localRandomAccessFile.writeUTF(paramClan.rankedMembers
	// .get(i));
	// localRandomAccessFile.writeShort(paramClan.ranks.get(i)
	// .intValue());
	// }
	// } else {
	// localRandomAccessFile.writeShort(0);
	// }
	//
	// localRandomAccessFile.close();
	// } catch (IOException localIOException) {
	// localIOException.printStackTrace();
	// }
	// }

	// private static Clan read(String paramString) {
	// File localFile = new File("Data/clan/" + paramString + ".cla");
	// if (!localFile.exists()) {
	// return null;
	// }
	// try {
	// RandomAccessFile localRandomAccessFile = new RandomAccessFile(
	// localFile, "rwd");
	//
	// Clan localClan = new Clan(localRandomAccessFile.readUTF(),
	// paramString);
	// localClan.whoCanJoin = localRandomAccessFile.readByte();
	// localClan.whoCanTalk = localRandomAccessFile.readByte();
	// localClan.whoCanKick = localRandomAccessFile.readByte();
	// localClan.whoCanBan = localRandomAccessFile.readByte();
	// localClan.setEloRating(localRandomAccessFile.readByte());
	// int i = localRandomAccessFile.readShort();
	// if (i != 0) {
	// for (int j = 0; j < i; j++) {
	// localClan.rankedMembers
	// .add(localRandomAccessFile.readUTF());
	// localClan.ranks.add(Integer.valueOf(localRandomAccessFile
	// .readShort()));
	// }
	// }
	// localRandomAccessFile.close();
	//
	// return localClan;
	// } catch (IOException localIOException) {
	// localIOException.printStackTrace();
	// }
	// return null;
	// }

	// public static boolean clanExists(String paramString) {
	// File localFile = new File("Data/clan/" + paramString + ".cla");
	// return localFile.exists();
	// }

	// public LinkedList<Clan> getClans() {
	// return clans;
	// }
}
