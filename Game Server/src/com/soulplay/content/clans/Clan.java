package com.soulplay.content.clans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.content.player.HelpChatAutoReply;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.util.HttpPost;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;


public class Clan {
	
	private int clanMysqlId = -1;

	@FunctionalInterface
	public interface ClanPointsReceiver {

		void receivePoints(long points);
	}

	public static class Rank {

		public final static int ANYONE = -1;

		public final static int FRIEND = 0;

		public final static int RECRUIT = 1;

		public final static int CORPORAL = 2;

		public final static int SERGEANT = 3;

		public final static int LIEUTENANT = 4;

		public final static int CAPTAIN = 5;

		public final static int GENERAL = 6;

		public final static int OWNER = 7;
	}

	public static final String[] CLANBOARD = {
			"<col=FFFF00>Become one of the greatest clan leader on SoulPlay.",
			"<col=FFFF00>Check out the clan leaderboard at ::clans", "",
			"<col=FFFF00>Clan benefits for ranked clan members:",
			"<col=ffffff>Clan level 35 increases your xp by 1%",
			"<col=ffffff>Clan level 40 2 extra pkp for killing your opponent",
			"<col=ffffff>Clan level 45 increases your xp by 2%",
			"<col=ffffff>Clan level 50 1 extra PC point each pest control game",
			"<col=ffffff>Clan level 55 3 extra pkp for killing your opponent",
			"<col=ffffff>Clan level 60 Allows you to place a egg in the incubator",
			"<col=ffffff>Clan level 65 increases your xp by 3%",
			"<col=ffffff>Clan level 70 4 extra pkp for killing your opponent",
			"<col=ffffff>Clan level 80 increases your xp by 5%",
			"<col=ffffff>Clan level 85 2 extra PC point each pest control game",
			"<col=ffffff>Clan level 90 Allows you to enter kiln cave for free",
			"<col=ffffff>Clan level 95 increases your xp by 7%",
			"<col=ffffff>Clan level 105 7 extra pkp for killing your opponent",
			"<col=ffffff>Clan level 110 3 extra PC point each pest control game",
			"<col=ffffff>Clan level 115 increases your xp by 10%", "",
			"<col=FFFF00>How to obtain Clan rating:",
			"<col=ffffff>You can obtain clan rating by doing alot of activities.",
			"<col=ffffff>Players can do any of these activities to earn Clan Rating",
			"<col=ffffff>for their current clan.", "",
			"<col=ffffff>Castle Wars - Pest Control - Kiln - Wilderness Treasure -",
			"<col=ffffff>Barrows - Fishing Contest - FunPk Tournement", "",
			"<col=ffffff>Nex - WildyWorm - King Black Dragon - Kalphite Queen -",
			"<col=ffffff>Chaos Elemental - Tormented Demon - Corporeal Beast -",
			"<col=ffffff>Avatar Of Destruction - Dagannoth Bosses - Godwars Bosses -",
			"<col=ffffff>Nomad - Bandos Avatar", "",
			"<col=ffffff>Slayer - 99 Summoning - Dungeoneering", "",
			"<col=ffffff>Lord - Legend - Extreme", "",
			"<col=ffffff>Player Killing", "", "<col=ffffff>Achievements", "",
			"<col=ffffff>Clan rating benefits are currently in development"};

	private ArrayList<GameItem> clanWarLoot = new ArrayList<>();

	private static final int[] ccxps = {84, 174, 267, 388, 512, 650, 801, 969, 1154,
			1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018,
			5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833,
			16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224,
			41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721,
			101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254,
			224466, 247886, 273742, 302288, 333804, 368599, 407015, 449428,
			496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895,
			1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068,
			2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294,
			4385776, 4842295, 5346332, 5902831, 6517253, 7195629, 7944614,
			8771558, 9684577, 10692629, 11805606, 13034431, 14391160, 15889109,
			17542976, 19368992, 21385073, 23611006, 26068632, 28782069,
			31777943, 35085654, 38737661, 42769801, 47221641, 52136869,
			57563718, 63555443, 70170840, 77474828, 85539082, 94442737,
			104273167, 200000000};

	/**
	 * The title of the clan.
	 */
	public String title;

	/**
	 * The clan's tag
	 */
	private String clanTag;

	/**
	 * The founder of the clan.
	 */
	public String founder;

	/**
	 * The active clan members.
	 */
	public LinkedList<ClanMember> activeMembers = new LinkedList<>();

	/**
	 * The banned members.
	 */
	public LinkedList<String> bannedMembers = new LinkedList<>();

	/**
	 * The ranked clan members.
	 */
	// public LinkedList<String> rankedMembers = new LinkedList<String>();
	public HashMap<String, Integer> rankedMembers = new HashMap<>();

	/**
	 * The clan member ranks.
	 */
	public LinkedList<Integer> ranks = new LinkedList<>();

	// public void sendClanMessageSync(String message) {
	//
	// synchronized (PlayerHandler.lock) {
	// for (int index = 0; index < Config.MAX_PLAYERS; index++) {
	// final Client p = (Client) PlayerHandler.players[index];
	// if (p != null) {
	// if (activeMembers.contains(p.getName())) {
	// p.sendMessage(message);
	// }
	// }
	// }
	// }
	// }

	/**
	 * if clan war is initiated in this world, then allow players to enter the
	 * clan war id set on here.
	 */
	private int clanWarId = -1;

	private boolean[] clanWarRules = new boolean[11];

	private boolean clanWarFighting = false;

	private boolean lockedExitPortals = false;

	/**
	 * Clan chat's elo rating
	 */
	private int eloRating;

	/**
	 * The ranks privileges require (joining, talking, kicking, banning).
	 */
	public int whoCanJoin = Rank.ANYONE;

	public int whoCanTalk = Rank.ANYONE;

	public int whoCanKick = Rank.GENERAL;

	public int whoCanBan = Rank.OWNER;

	public int whoCanChangeClanShare = Rank.CAPTAIN;

	/**
	 * Clan share stuff
	 */
	private boolean clanShare = false;

	// private volatile long clanPoints = -1;
	private final AtomicLong clanPoints = new AtomicLong();
	// private volatile long lastClanUpdate = 0L;

	public RaidsManager raidsManager;
	public TobManager tobManager;

	/**
	 * Creates a new clan for the specified player.
	 *
	 * @param player
	 */
	public Clan(Client player) {
		this(player.getName() + "'s Clan", player.getName().toLowerCase());
	}

	/**
	 * Creates a new clan for the specified title and founder. Used for loading
	 * clans.
	 *
	 * @param title
	 * @param founder
	 */
	public Clan(String title, String founder) {
		setTitle(title);
		setFounder(founder);

		// Server.getTaskScheduler().schedule(new Task(500, false) {
		// @Override
		// protected void execute() {
		// updateClanPoints();
		// }
		// });
	}

	public void addClanPoints(Player o, int points, String method) {
		if (getFounder().equals("help"))
			return;
		method = method.replace(" ", "_");
		final String playerName = o.getName().replace(" ", "_");
		final String clanOwner = getFounder().replace(" ", "_");
		final String finalMethod = method;
		// HttpPost.sendLink(Config.CLANPOINTS_URL+"?name="+clanOwner+"&points="+points+"&player="+playerName+"&method="+method);
		TaskExecutor.executeHttp(
				() -> HttpPost.getHttpResponseString(Config.CLANPOINTS_URL
						+ "?name=" + clanOwner + "&points=" + points
						+ "&player=" + playerName + "&method=" + finalMethod));
		clanPoints.compareAndSet(getClanPoints(), getClanPoints() + points);
	}

	/**
	 * Adds a member to the clan.
	 *
	 * @param player
	 */
	public void addMember(Client player) {
		if (isBanned(player.getName())) {
			player.sendMessage(
					"<col=FF0000>You are currently banned from this clan chat.</col>");
			return;
		}
		if (activeMembers.size() > 199) {
			player.sendMessage(
					"<col=FF0000>Clan chat you are trying to join is full.</col>");
			return;
		}
		if (whoCanJoin > Rank.ANYONE && !isFounder(player.getName())) {
			if (getRank(player.getName()) < whoCanJoin && !player.isMod()) {
				player.sendMessage("Only " + getRankTitle(whoCanJoin) + "s+ may join this chat.");
				return;
			}
		}

		player.setClan(this);
		player.lastClanChat = getFounder();
		final ClanMember member = new ClanMember(player.getName(), player.getNameSmart(), player.mySQLIndex);
		
		if (!activeMembers.contains(member)) {
			activeMembers.add(member);
		}
		long clanPoints = getClanPoints();

		int lvl = -1;
		for (int i = 0; i < ccxps.length; i++) {
			if (clanPoints >= ccxps[i]) {
				lvl = i + 2;
			}
		}

		if (lvl > 0) {
			player.getPacketSender().sendString("Clan Chat (lvl " + lvl + ")", 18138);
		} else {
			player.getPacketSender().sendString("Clan Chat", 18138);
		}

		player.getPacketSender().sendString("Leave chat", 18135);
		player.getPacketSender().sendString(
				"Talking in: <col=FFFF64>" + getTitle() + "</col>", 18139);
		player.getPacketSender()
				.sendString("Owner: <col=FFFFFF>"
						+ Misc.formatPlayerName(getFounder()) + "</col>",
						18140);
		player.sendMessage("Now talking in clan chat <col=FFFF64><shad=0>"
				+ getTitle() + "</shad></col>.");
		player.sendMessage(
				"To talk, start each line of chat with the / symbol.");

		// Adds friends of the owner of the clan
		// if (!(getRank(player.getName()) >= Rank.FRIEND)) {
		// if (PlayerSave.isFriend(getFounder(), player.getName())) {
		// setRank(player.getName(), Rank.FRIEND);
		// }
		// }

		updateMembers();
	}
	
	public int getClanLevel() {
		int lvl = -1;
		for (int i = 0; i < ccxps.length; i++) {
			if (clanPoints.get() >= ccxps[i]) {
				lvl = i + 2;
			}
		}
		return lvl;
	}

	/**
	 * Bans the name from entering the clan chat.
	 *
	 * @param username
	 */
	public void banMember(String username) {
		username = Misc.formatPlayerName(username);
		if (bannedMembers.contains(username)) {
			return;
		}
		if (username.equalsIgnoreCase(getFounder())) {
			return;
		}
		if (isRanked(username)) {
			return;
		}
		final Client player = (Client) PlayerHandler.getPlayerSmart(username);
		if (player != null) {
			if (player.inClanWars() && isClanWarFighting()) {
				sendClanMessage(
						"Unable to kick <col=800000>'" + Misc.formatPlayerName(username)
								+ "' while he is in clan wars.", null);
				return;
			}
			player.sendMessage("You have been kicked from the clan chat.");
		}
		removeMemberByName(username);
		bannedMembers.add(username);
		save();
		sendClanMessage("<col=ff>[Attempting to kick/ban <col=800000>'"
				+ Misc.formatPlayerName(username) + "'"
				+ " <col=ff>from this friends chat]", null);
	}

	/**
	 * Can they ban?
	 *
	 * @param name
	 * @return
	 */
	public boolean canBan(String name) {
		if (isFounder(name)) {
			return true;
		}
		if (getRank(name) >= whoCanBan) {
			return true;
		}
		return false;
	}

	/**
	 * Can they kick?
	 *
	 * @param name
	 * @return
	 */
	public boolean canKick(String name) {
		if (isFounder(name)) {
			return true;
		}
		if (getRank(name) >= whoCanKick) {
			return true;
		}
		return false;
	}

	public boolean clanShare() {
		return clanShare;
	}

	public void compareAndSetClanPoints(long compare, long newLong) {
		clanPoints.compareAndSet(compare, newLong);
	}

	/**
	 * Deletes the clan.
	 */
	public void delete() {
		for (ClanMember member : activeMembers) {
			removeMember(member);
			final Player player = PlayerHandler.getPlayerByMID(member.getMysqlID());
			player.sendMessage("The clan you were in has been deleted.");
		}
		ClanManager.delete(this);
	}

	/**
	 * Demotes the specified name.
	 *
	 * @param name
	 */
	public boolean demote(String name) {
		return rankedMembers.remove(name) != null;
	}

	public long getClanPoints() {
		/*
		 * if (clanPoints == -1) updateClanPoints(); while (clanPoints == -1) {
		 * try { Thread.sleep(20); } catch (InterruptedException e) {
		 * e.printStackTrace(); break; } }
		 */
		return clanPoints.get();
	}

	public String getClanTag() {
		return clanTag;
	}

	public int getClanWarId() {
		return clanWarId;
	}

	public ArrayList<GameItem> getClanWarLoot() {
		return clanWarLoot;
	}

	public boolean[] getClanWarRules() {
		return clanWarRules;
	}

	/**
	 * Gets Clan's Elo Rating
	 */
	public int getEloRating() {
		return eloRating;
	}

	/**
	 * Gets the founder of the clan.
	 *
	 * @return
	 */
	public String getFounder() {
		return founder;
	}

	public String getMemberNameByInterfaceId(int stringId) {
		int memberId = stringId - 18323;
		if (memberId < 0 || memberId > rankedMembers.size()) {
			return null; // TODO: -1 rankedMembers.size() i think
		}
		// String member = rankedMembers.get(id - 18323);
		int loopId = 0;
		Iterator<Entry<String, Integer>> it = rankedMembers.entrySet()
				.iterator();
		while (it.hasNext()) {
			// String name = (String)it.next();
			Entry<String, Integer> entry = it.next();
			String name = entry.getKey();
			if (loopId == memberId) {
				if (name == null || name.isEmpty()) {
					return null;
				}
				return name;
			}
			loopId++;
		}
		return null;
	}

	/**
	 * Gets the rank of the specified name.
	 *
	 * @param name
	 * @return
	 */
	public int getRank(String name) {
		name = Misc.formatPlayerName(name);

		if (rankedMembers.containsKey(name)) {
			return rankedMembers.get(name);
		}
		if (isFounder(name)) {
			return Rank.OWNER;
		}

		// if (rankedMembers.contains(name)) {
		// return ranks.get(rankedMembers.indexOf(name));
		// }
		// if (isFounder(name)) {
		// return Rank.OWNER;
		// }

		// if (PlayerSave.isFriend(getFounder(), name)) {
		// return Rank.FRIEND;
		// }
		return -1;
	}

	/**
	 * Gets the rank title as a string.
	 *
	 * @param rank
	 * @return
	 */
	public String getRankTitle(int rank) {
		switch (rank) {
			case -1:
				return "Anyone";
			case 0:
				return "Friend";
			case 1:
				return "Recruit";
			case 2:
				return "Corporal";
			case 3:
				return "Sergeant";
			case 4:
				return "Lieutenant";
			case 5:
				return "Captain";
			case 6:
				return "General";
			case 7:
				return "Only Me";
		}
		return "";
	}

	/**
	 * Gets the title of the clan.
	 *
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	public int getWhoCanChangeClanShare() {
		return whoCanChangeClanShare;
	}

	/**
	 * Returns whether or not the specified name is banned.
	 *
	 * @param name
	 * @return
	 */
	public boolean isBanned(String name) {
		name = Misc.formatPlayerName(name);
		if (bannedMembers.contains(name)) {
			return true;
		}
		return false;
	}

	public boolean isClanWarFighting() {
		return clanWarFighting;
	}

	/**
	 * Returns whether or not the specified name is the founder.
	 *
	 * @param name
	 * @return
	 */
	public boolean isFounder(String name) {
		if (getFounder().equalsIgnoreCase(name)) {
			return true;
		}
		return false;
	}

	public boolean isGeneralPlusRank(String name) {
		if (getFounder().equalsIgnoreCase(name)) {
			return true;
		}
		if (getRank(name) >= Rank.GENERAL) {
			return true;
		}
		return false;
	}

	public boolean isLockedExitPortals() {
		return lockedExitPortals;
	}

	public boolean isOwnerRank(String name) {
		if (getFounder().equalsIgnoreCase(name)) {
			return true;
		}
		if (getRank(name) >= Rank.OWNER) {
			return true;
		}
		return false;
	}

	/**
	 * Returns whether or not the specified name is a ranked user.
	 *
	 * @param displayName
	 * @return
	 */
	public boolean isRanked(String displayName) {
		displayName = Misc.formatPlayerName(displayName);
		if (rankedMembers.containsKey(displayName)) {
			return true;
		}
		return false;
	}

	/**
	 * Kicks the name from the clan chat.
	 *
	 * @param name
	 */
	public void kickMember(ClanMember clanMember) {
		if (!activeMembers.contains(clanMember)) {
			return;
		}

		String name = clanMember.getUsername();
		if (name.equalsIgnoreCase(getFounder())) {
			return;
		}

		final Client player = (Client) PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
		if (player != null) {
			if (player.inClanWars() && isClanWarFighting()) {
				sendClanMessage(
						"Unable to kick <col=800000>'" + Misc.formatPlayerName(clanMember.getDisplayName())
								+ "' while he is in clan wars.", null);
				return;
			}
			player.sendMessage("You have been kicked from the clan chat.");
		}

		removeMember(clanMember);
		sendClanMessage("<col=ff>[Attempting to kick/ban <col=800000>'"
				+ Misc.formatPlayerName(clanMember.getDisplayName()) + "'"
				+ " <col=ff>from this friends chat]", null);
	}

	/**
	 * Removes the player from the clan.
	 *
	 * @param player
	 */
	public void removeMember(Client player) {
		for (int index = 0, len = activeMembers.size(); index < len; index++) {
			if (activeMembers.get(index).getMysqlID() == player.mySQLIndex) {
				player.setClan(null);
				resetInterface(player);
				activeMembers.remove(index);
				break;
			}
		}

		if (player.inClanWars() && clanWarId != -1) {
			ClanWarHandler.removePlayer(player,
					ClanWarHandler.clanWars[clanWarId]);
		}

		// player.getPA().refreshSkill(21);
		// player.getPA().refreshSkill(22);
		// player.getPA().refreshSkill(23);
		if (player.isActive && !player.disconnected) {
			updateMembers();
			player.getPacketSender().sendString("Clan Chat", 18138);
		}
	}

	/**
	 * Removes the player from the clan.
	 *
	 * @param player
	 */
	public void removeMember(ClanMember member) {
		for (int index = 0, len = activeMembers.size(); index < len; index++) {
			if (activeMembers.get(index).equals(member)) {
				Player c = PlayerHandler.getPlayerByMID(member.getMysqlID());
				if (c != null) {
					c.setClan(null);
					resetInterface(c);
					activeMembers.remove(index);
					if (c.raidsManager != null)
						c.raidsManager.leaveRaid(c, true);
					if (c.tobManager != null) {
						c.tobManager.leaveTob(c, true);
					}
				}
				break;
			}
		}
		updateMembers();
	}
	
	public void removeMemberByName(String name) {
		for (int index = 0, len = activeMembers.size(); index < len; index++) {
			if (activeMembers.get(index).getUsername().equalsIgnoreCase(name) || activeMembers.get(index).getDisplayName().equalsIgnoreCase(name)) {
				ClanMember member = activeMembers.get(index);
				Player c = PlayerHandler.getPlayerByMID(member.getMysqlID());
				if (c != null) {
					c.setClan(null);
					resetInterface(c);
					activeMembers.remove(index);
				}
				
				break;
			}
		}
	}

	/**
	 * Resets the clan interface.
	 *
	 * @param player
	 */
	public void resetInterface(Player player) {
		if (!player.isActive || player.disconnected) {
			return;
		}
		player.getPacketSender().sendString("Join chat", 18135);
		player.getPacketSender().sendString("Talking in: Not in chat", 18139);
		player.getPacketSender().sendString("Owner: None", 18140);
		for (int index = 0; index < 100; index++) {
			player.getPacketSender().sendString("", 18144 + index);
		}
	}

	/**
	 * Saves the clan.
	 */
	public void save() {
		// /*Server.c*/ClanManager.save(this);
		try {
			SqlHandler.saveClanMain(this);
			updateMembers();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendAlertToLS(String message) {
		LoginServerConnection.instance().submit(new Runnable() {

			@Override
			public void run() {
				LoginServerConnection.instance().sendGlobalYell1(":clan2:" + message, getFounder());
			}
		});
	}

	/**
	 * Sends a message to the clan.
	 *
	 * @param player
	 * @param message
	 */
	public void sendChat(Client paramClient, String paramString) {
		if (paramClient.cantCommunicate()) {
			paramClient.sendMessage("Please wait some time before chatting.");
			return;
		}

		int rank = getRank(paramClient.getName());
		if (rank < this.whoCanTalk) {
			paramClient.sendMessage("Only " + getRankTitle(this.whoCanTalk)
					+ "s+ may talk in this chat.");
			return;
		}
		if (!LoginServerConnection.instance().isActive()) {
			paramClient.sendMessage(
					"<col=800000>The login server is offline at the moment. Please try again later.");
			return;
		}
		
		if (founder.equals("help")) {
			HelpChatAutoReply.getAnswer(paramClient, paramString);
		}

		// new clan chat
		// sendMessageToLS(":clan:"+paramClient.getClan().getFounder()+":"+paramString,
		// paramClient.getName());

		paramString = Misc.formatSentence(paramString);
		// for (int j = 0; j < PlayerHandler.players.length; j++) {
		// if (PlayerHandler.players[j] != null) {
		// final Client c = (Client) PlayerHandler.players[j];
		// if ((c != null) && (this.activeMembers.contains(c.getName())))
		// c.sendMessage("</col>[<col=ff>" + getTitle()
		// + "</col>] <clan=" + getRank(paramClient.getName())
		// + ">"
		// + Misc.optimizeText(paramClient.getName())
		// + ": <col=ff0000>" + paramString + "");
		// }
		// }
		String message;
		if (rank == -1) {
			message = "</col>[<col=ff>" + getTitle() + "</col><col=0>] " + paramClient.getNameSmartUp() + ": <col=d66e00>" + paramString;
		} else {
			message = "</col>[<col=ff>" + getTitle() + "</col><col=0>] <clan=" + rank + ">" + paramClient.getNameSmartUp() + ": <col=d66e00>" + paramString;
		}

		sendMessageToLS(
				":clan:" + paramClient.getClan().getFounder() + ":" + message, paramClient.getNameSmartUp());

	}

	/**
	 * Sends a message to the clan.
	 * @param message
	 * @param name TODO
	 * @param player
	 */
	public void sendClanMessage(String msg, String name) {
		final String message = msg.length() > Misc.MAX_MESSAGE_SIZE ? msg.substring(0, Misc.MAX_MESSAGE_SIZE) : msg;
		for (int index = 0, len = activeMembers.size(); index < len; index++) {
			ClanMember member = activeMembers.get(index);
			Player player = PlayerHandler.getPlayerByMID(member.getMysqlID());
			if (player == null || !player.isActive) {
				continue;
			}

			player.getPacketSender().sendMessage(ChatMessageTypes.CLAN_MESSAGE, message, name);
		}
	}

	public void sendMessageToLS(String msg, String displayName) {
		final String message = msg.length() > Misc.MAX_MESSAGE_SIZE ? msg.substring(0, Misc.MAX_MESSAGE_SIZE) : msg;
		LoginServerConnection.instance().submit(new Runnable() {
			
			@Override
			public void run() {
				LoginServerConnection.instance().sendGlobalYell1(message, displayName);
			}
		});
	}

	public void sendPlayersClanTagUpdate() {
		Server.getTaskScheduler().schedule(new Task(true) {

			@Override
			protected void execute() {

				for (int index = 0, len = activeMembers.size(); index < len; index++) {
					ClanMember member = activeMembers.get(index);

					Player player = PlayerHandler.getPlayerByMID(member.getMysqlID());

					if (player != null && player.isActive && player.getClan() != null) {
						player.sendMessage("Clan Tag has been changed to: "
								+ player.getClan().getClanTag());
						player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
					}
				}

				this.stop();

			}
		});
	}

	public void setClanPoints(long amount) {
		clanPoints.set(amount);
	}

	public void setClanShare(boolean clanShare) {
		this.clanShare = clanShare;
	}

	/**
	 * Sends a message to the clan.
	 *
	 * @param player
	 * @param message
	 */
	public void setClanShareFrame(String message) {

		// for (int index = 0; index < Config.MAX_PLAYERS; index++) {
		// final Client p = (Client) PlayerHandler.players[index];
		// if (p != null) {
		// if (activeMembers.contains(p.getName())) {
		// p.getPacketSender().sendFrame126(message, 18250);
		// }
		// }
		// }

		for (int index = 0, len = activeMembers.size(); index < len; index++) {
			ClanMember member = activeMembers.get(index);
			Player player = PlayerHandler.getPlayerByMID(member.getMysqlID());
			if (player != null && player.isActive) {
				player.getPacketSender().sendFrame126(message, 18250);
			}
		}

	}

	public void setClanTag(String clanTag) {
		if (this.clanTag == null && clanTag == null) {
			return;
		}
		if ((clanTag == null && this.clanTag != null)
				|| (clanTag != null && this.clanTag == null)
				|| !this.clanTag.equals(clanTag)) {
			this.clanTag = clanTag;
			sendPlayersClanTagUpdate();
		}
	}

	public void setClanWarFighting(boolean clanWarFighting) {
		this.clanWarFighting = clanWarFighting;
	}

	public void setClanWarId(int clanWarId) {
		this.clanWarId = clanWarId;
	}

	// public void updateClanPoints() {
	// updateClanPoints(false);
	// }
	//
	// public void updateClanPoints(boolean force) {
	// long lastUpdate = System.currentTimeMillis() - lastClanUpdate;
	// if (clanPoints == -1 || lastUpdate > 5 * 60 * 1000) {
	// HttpPost.getClanPoints(founder, (points) -> {
	// clanPoints = points;
	// lastClanUpdate = System.currentTimeMillis();
	// });
	// }
	// }

	public void setClanWarLoot(ArrayList<GameItem> clanWarLoot) {
		this.clanWarLoot = clanWarLoot;
	}

	public void setClanWarRules(boolean[] clanWarRules) {
		this.clanWarRules = clanWarRules;
	}

	/**
	 * Sets clan elo rating
	 */
	public void setEloRating(int amount) {
		eloRating = amount;
	}

	// public void checkClanPoints(Client c) {
	// HttpPost.getHttpResponse(c, "Clan Rating : ",
	// Config.CLANPOINTS_URL+"?clan="+getFounder());
	// }

	/**
	 * Sets the founder.
	 *
	 * @param founder
	 */
	public void setFounder(String founder) {
		this.founder = founder;
	}

	public void setLockedExitPortals(boolean lockedExitPortals) {
		this.lockedExitPortals = lockedExitPortals;
	}

	/**
	 * Sets the rank for the specified name.
	 *
	 * @param name
	 * @param rank
	 */
	public boolean setRank(String name, int rank) {
		if (rankedMembers.size() >= ClanManager.MAXIMUM_RANKED_MEMBERS) {
			return false;
		}

		rankedMembers.put(name, rank);

		// if (rankedMembers.contains(name)) {
		// ranks.set(rankedMembers.indexOf(name), rank);
		// } else {
		// rankedMembers.add(name);
		// ranks.add(rank);
		// }
		save();

		return true;
	}

	/**
	 * Sets the minimum rank that can ban.
	 *
	 * @param rank
	 */
	public void setRankCanBan(int rank) {
		whoCanBan = rank;
	}

	/**
	 * Sets the minimum rank that can join.
	 *
	 * @param rank
	 */
	public void setRankCanJoin(int rank) {
		whoCanJoin = rank;
	}

	/**
	 * Sets the minimum rank that can kick.
	 *
	 * @param rank
	 */
	public void setRankCanKick(int rank) {
		whoCanKick = rank;
	}

	/**
	 * Sets the minimum rank that can talk.
	 *
	 * @param rank
	 */
	public void setRankCanTalk(int rank) {
		whoCanTalk = rank;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 * @return
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public void setWhoCanChangeClanShare(int whoCanChangeClanShare) {
		this.whoCanChangeClanShare = whoCanChangeClanShare;
	}

	/**
	 * Unbans the name from the clan chat.
	 *
	 * @param name
	 */
	public void unbanMember(String name) {
		name = Misc.formatPlayerName(name);
		if (bannedMembers.contains(name)) {
			bannedMembers.remove(name);
			save();
		}
	}

	/**
	 * Updates the members on the interface for the player.
	 *
	 * @param player
	 */
	public void updateInterface(Player player) {
		player.getPacketSender().sendString(
				"Talking in: <col=FFFF64>" + getTitle() + "</col>", 18139);
		player.getPacketSender()
				.sendString("Owner: <col=FFFFFF>"
						+ Misc.formatPlayerName(getFounder()) + "</col>",
						18140);
		int activeSize = activeMembers.size();
		for (int index = 0; index < 100; index++) {
			if (index < activeSize) {
				String clanMemberName = activeMembers.get(index).getDisplayName();
				int rank = getRank(clanMemberName);
				if (rank == -1) {
					player.getPacketSender().sendString(Misc.formatName(clanMemberName), 18144 + index);
				} else {
					player.getPacketSender().sendString("<clan=" + rank + ">" + Misc.formatName(clanMemberName), 18144 + index);
				}
			} else {
				player.getPacketSender().sendString("", 18144 + index);
			}
		}
	}
	
	private void sortByDisplayName() {
		Collections.sort(activeMembers, (m1, m2) -> {
			return m1.getDisplayName().compareTo(m2.getDisplayName());
		});
	}

	/**
	 * Updates the interface for all members.
	 */
	public void updateMembers() {

		sortByDisplayName();
		
		for (int index = 0, len = activeMembers.size(); index < len; index++) {
			ClanMember member = activeMembers.get(index);
			Player player = PlayerHandler.getPlayerByMID(member.getMysqlID());
			if (player != null && player.isActive) {
				updateInterface(player);
			}
		}
	}

	public int getClanMysqlId() {
		return clanMysqlId;
	}

	public void setClanMysqlId(int clanMysqlId) {
		this.clanMysqlId = clanMysqlId;
	}

}
