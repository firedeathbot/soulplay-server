package com.soulplay.content.seasonals.powerups;

public enum PowerUpRarity {

	COMMON(0, 0, 0x27F707, 3),
	UNCOMMON(1, 10, 0x0070DD, 3),
	RARE(2, 20, 0x8400C5, 60),
	SUPER_RARE(3, 50, 0xF17B03, 100);

	private final int id, roll, rgb, combatReq;

	PowerUpRarity(int id, int roll, int rgb, int combatReq) {
		this.id = id;
		this.roll = roll;
		this.rgb = rgb;
		this.combatReq = combatReq;
	}

	public int getId() {
		return id;
	}

	public int getRoll() {
		return roll;
	}

	public int getRgb() {
		return rgb;
	}

	public int getCombatReq() {
		return combatReq;
	}

}
