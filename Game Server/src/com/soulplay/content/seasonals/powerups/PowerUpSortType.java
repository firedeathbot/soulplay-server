package com.soulplay.content.seasonals.powerups;

public enum PowerUpSortType {

	RARITY(0),
	AVAILABLE(1),
	AGE(2);

	public static final PowerUpSortType[] values = values();
	private final int id;

	PowerUpSortType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static PowerUpSortType list(int id) {
		for (PowerUpSortType type : values) {
			if (type.id == id) {
				return type;
			}
		}

		return RARITY;
	}

}
