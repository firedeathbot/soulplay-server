package com.soulplay.content.seasonals.powerups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Consumer;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.packet.ConfigCodes;

public enum PowerUpData {

	//Common combat lvl 3 +
	HEALTH(0, "Health", "Increases your health by 25%", PowerUpRarity.COMMON, 10, 1387, 25001, player -> player.refreshSkill(Skills.HITPOINTS), player -> player.refreshSkill(Skills.HITPOINTS)),
	MELEE_DAMAGE(1, "Melee Damage", "Increases your melee damage by 5%", PowerUpRarity.COMMON, 5, 1327, 25002),
	MAGIC_DAMAGE(2, "Magic Damage", "Increases your magic damage by 5%", PowerUpRarity.COMMON, 5, 1358, 25003),
	RANGE_DAMAGE(3, "Range Damage", "Increases your range damage by 5%", PowerUpRarity.COMMON, 5, 1363, 25004),
	STRENGTH(4, "Strength", "Increases your damage by 5%", PowerUpRarity.COMMON, 5, 1332, 25005),
	PRAYER_DRAIN(5, "Prayer Drain", "Prayer drains 10% slower", PowerUpRarity.COMMON, 5, 1361, 25006),
	DEFENCE(6, "Defence", "Defence increased by 5% (7% while wearing a shield)", PowerUpRarity.COMMON, 5, 1348, 25007),
	MAGIC_KNOWLEDGE(7, "Magic knowledge", "Magic no longer requires runes", PowerUpRarity.UNCOMMON, 1, 1357, 25021, player -> player.getPacketSender().sendConfig(ConfigCodes.INF_RUNES, 1),
			player -> player.getPacketSender().sendConfig(ConfigCodes.INF_RUNES, 0)),
	BLOCK(8, "Block", "You can now block attacks (3%)", PowerUpRarity.COMMON, 3, 1342, 25008),
	SURGE(9, "Surge", "Surge spells deal 5% more damage", PowerUpRarity.COMMON, 5, 1351, 25009),
	SPIKED_SHIELD(10, "Spiked Shield", "Your shield has a 10% chance to deal 2 damage.", PowerUpRarity.COMMON, 1, 1386, 25010),
	TWO_HANDER(11, "Two Hander", "Your two handed melee weapons hits 1 tick faster", PowerUpRarity.COMMON, 1, 1380, 25011),
	ONE_HANDER(12, "One Hander", "Your one handed melee weapons deal 7% increased damage", PowerUpRarity.COMMON, 1, 1381, 25012),
	RANGER(13, "Ranger", "Your range distance is increased by 2 tiles", PowerUpRarity.COMMON, 1, 1366, 25013),
	NECROMANCER(14, "Necromancer", "All combats spells have a chance to summon a skeleton that attacks your opponent dealing 2 damage for 10 seconds every few ticks.", PowerUpRarity.UNCOMMON, 1, 1378, 25022),

	//Rare combat lvl 3 +
	HYBRID(15, "Hybrid", "Switching attack style deals 15% increased damage", PowerUpRarity.UNCOMMON, 1, 1331, 25023),
	BLOCK_MASTERY(16, "Block Mastery", "Increases ur block by 2% and triggers a heal of 2 hp each time you block", PowerUpRarity.UNCOMMON, 3, 1345, 25024),
	IGNITE(17, "Ignite", "Fire attacks have a chance to burn your target dealing 1 damage for 10 seconds (total 5 damage)", PowerUpRarity.UNCOMMON, 3, 1355, 25025),
	EARTH_ROOT(18, "Earth Root", "Earth attacks have a chance to root your opponent for 5 seconds", PowerUpRarity.UNCOMMON, 1, 1364, 25026),
	VENGEANCE_HEAL(19, "Vengeance Heal", "Vengeance now heals you back for 80% of damage dealth.", PowerUpRarity.UNCOMMON, 1, 1388, 25027),
	EATING(20, "Holy Food", "Eating now also restores your prayer", PowerUpRarity.UNCOMMON, 1, 1374, 25028),
	HEAVYWEIGHT(21, "Heavyweight", "While fighting a opponent with more than 250 health you have a chance to deal 15 extra damage", PowerUpRarity.UNCOMMON, 3, 1349, 25029),
	DRAGONBREATH(22, "Dragonbreath", "Your dragonfire breath now requires no charges and deal 8% more damage", PowerUpRarity.UNCOMMON, 2, 1356, 25030),

	//Epic combat lvl 60 +
	LIFESTEAL(23, "Lifesteal", "All your attacks have a chance to heal you for 5% of your damage", PowerUpRarity.RARE, 3, 1389, 25040),
	CASTER(24, "Caster", "Air, water, earth and fire attacks has a 20% chance to trigger same spell dealing 50% damage ", PowerUpRarity.RARE, 1, 1362, 25041),
	ADVANCED_RANGER(25, "Advanced Ranger", "Your range attacks have a 20% chance to trigger double arrow, second arrow dealing 50% damage", PowerUpRarity.RARE, 1, 1369, 25042),
	WARRIOR(26, "Warrior", "Your melee attacks have a chance to trigger an additional attack dealing 50% less damage (Does not work with shield)", PowerUpRarity.RARE, 1, 1330, 25043),
	CRITS(27, "Crits", "Your attacks now have a 3% chance to crit.", PowerUpRarity.RARE, 3, 1344, 25044),
	CRIT_SPRINTER(28, "Crit Sprinter", "Upon critting you gain increased crit chance for the next attack.", PowerUpRarity.RARE, 3, 1353, 25045),
	FROST_SHOCK(29, "Haste", "Your magic attacks now hit 1 tick faster", PowerUpRarity.SUPER_RARE, 1, 1351, 25046),
	RANGE_CRIT(30, "Range Crit", "Dealing damage with ranged has a 6% to guarantee your next spell cast to crit", PowerUpRarity.RARE, 2, 1368, 25047),
	FIRE_WIND(31, "Fire Wind", "After casting a fire spell you have a 15% chance to crit with a wind spell", PowerUpRarity.RARE, 3, 1338, 25048),
	MOVEMENT(32, "Perfected ingredients", "All consumables that heal you grant 20% more healing.", PowerUpRarity.RARE, 1, 1388, 25049),
	
	//Legendary combat lvl 100+
	BATTLEMAGE(33, "Battlemage", "Your melee attacks have a chance to trigger a Fire surge or an Earth surge.", PowerUpRarity.SUPER_RARE, 1, 1336, 25032),
	THE_BEAST(34, "The Beast", "While using the turmoil prayer you have a chance to enrage and transform into a beast dealing 10% increased damage with all attacks.", PowerUpRarity.SUPER_RARE, 1, 1347, 25033),
	CRITTER(35, "Critter", "Increase your crit chance by 15%", PowerUpRarity.SUPER_RARE, 1, 1329, 25034),
	BEAR(36, "Explosive Ranger", "Your range attacks against players have a chance to trigger explosion pushing the opponent 2 tiles away, meanwhile stunning npcs for few ticks and dealing and additional 50% damage.", PowerUpRarity.SUPER_RARE, 1, 1346, 25035),
	NINJA(37, "Ninja", "Your melee attacks now hit 1 tick faster", PowerUpRarity.SUPER_RARE, 1, 1334, 25036),
	IRONMAN(38, "Ironman", "Equipping a shield with a melee weapon reduces all incoming damage by 20% and increases your block chance by 10%.", PowerUpRarity.SUPER_RARE, 1, 1343, 25037),
	MIRROR(39, "Mirror", "20% chance to mirror yourself creating a copy of yourself that deals 70% reduced damage", PowerUpRarity.SUPER_RARE, 1, 1357, 25038),

	// Batch 2 of powers
	MINOR_CRIT(40, "Minor Crit", "Increases your crit chance by 3%", PowerUpRarity.COMMON, 5, 1390, 25014),
	HEALTH_REGEN(41, "Health Regen", "Regenerate 1% health every 5 seconds.", PowerUpRarity.COMMON, 5, 1384, 25015),
	MELEE_ACCURACY(42, "Melee Accuracy", "Increases your melee accuracy by 5%.", PowerUpRarity.COMMON, 5, 1350, 25016),
	RANGE_ACCURACY(43, "Range Accuracy", "Increases your range accuracy by 5%.", PowerUpRarity.COMMON, 5, 1370, 25017),
	MAGIC_ACCURACY(44, "Magic Accuracy", "Increases your magic accuracy by 5%.", PowerUpRarity.COMMON, 5, 1354, 25018),
	POISON(45, "Poison", "Poison damage increased by 1 damage", PowerUpRarity.COMMON, 5, 1337, 25019),
	POISON_LASTING(46, "Poison Lasting", "Poison lasts longer", PowerUpRarity.COMMON, 5, 1365, 25020),
	CRIPPLING_POISON(47, "Quick", "Your ranged attacks now hit 1 tick faster.", PowerUpRarity.SUPER_RARE, 1, 1373, 25031),
	POISON_CRIT(48, "Poison Crit", "Your poison has a 50% chance to crit", PowerUpRarity.SUPER_RARE, 1, 1375, 25039),
	;

	public static final PowerUpData[] values = values();
	public static final Map<PowerUpRarity, List<PowerUpData>> rarityToPerks = new HashMap<>();
	public static final Map<Integer, PowerUpData> itemToPerk = new HashMap<>();
	public static final Set<Integer> cardsSet = new HashSet<>();
	public static final Map<Integer, PowerUpData> idToPerk = new HashMap<>();
	private final int id;
	private final String name, description;
	private final PowerUpRarity rarity;
	private final int amount;
	private final int imageId;
	private final Consumer<Player> onAdd, onRemove;
	private final int cardId;

	private PowerUpData(int id, String name, String description, PowerUpRarity rarity, int amount, int imageId, int cardId) {
		this(id, name, description, rarity, amount, imageId, cardId, null, null);
	}

	private PowerUpData(int id, String name, String description, PowerUpRarity rarity, int amount, int imageId, int cardId, Consumer<Player> onAdd, Consumer<Player> onRemove) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.rarity = rarity;
		this.amount = amount;
		this.imageId = imageId;
		this.cardId = cardId;
		this.onAdd = onAdd;
		this.onRemove = onRemove;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public PowerUpRarity getRarity() {
		return rarity;
	}

	public int getAmount() {
		return amount;
	}

	public int getImageId() {
		return imageId;
	}

	public Consumer<Player> getOnAdd() {
		return onAdd;
	}

	public Consumer<Player> getOnRemove() {
		return onRemove;
	}

	public int getCardId() {
		return cardId;
	}

	static {
		for (PowerUpData powerUp : values) {
			List<PowerUpData> list = rarityToPerks.get(powerUp.getRarity());
			if (list == null) {
				list = new ArrayList<>();
				rarityToPerks.put(powerUp.getRarity(), list);
			}

			list.add(powerUp);

			itemToPerk.put(powerUp.getCardId(), powerUp);
			idToPerk.put(powerUp.getId(), powerUp);
			cardsSet.add(powerUp.getCardId());
		}
	}

	public static PowerUpData list(int id) {
		return idToPerk.get(id);
	}

	public static void generateItems() {
		int itemId = 25001;
		StringBuilder sb = new StringBuilder();
		for (Entry<PowerUpRarity, List<PowerUpData>> entry : rarityToPerks.entrySet()) {
			if (entry.getKey() == PowerUpRarity.COMMON) {
				List<PowerUpData> powers = entry.getValue();
				for (PowerUpData power : powers) {
					sb.append("			case " + itemId + ":\r\n"
					+ "				itemDef.name = \"Power card (" + power.getName() + ")\";\r\n"
					+ "				itemDef.inventoryOptions = new String[]{null, null, null, \"Info\", \"Drop\"};\r\n"
					+ "				itemDef.modelId = 21562;\r\n"
					+ "				itemDef.xof2d = 1;\r\n"
					+ "				itemDef.yof2d = 4;\r\n"
					+ "				itemDef.xan2d = 576;\r\n"
					+ "				itemDef.yan2d = 2047;\r\n"
					+ "				itemDef.zoom2d = 659;\r\n"
					+ "				itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};\r\n"
					+ "				itemDef.recolorModified = new int[] {22446, 22446, 22446, 22446, 22446, 22446, 22446, 10322, 10322, 10322, 10322, 10322};\r\n"
					+ "				break;");
					sb.append("\n");
					itemId++;
				}
			} else if (entry.getKey() == PowerUpRarity.UNCOMMON) {
				List<PowerUpData> powers = entry.getValue();
				for (PowerUpData power : powers) {
					sb.append("			case " + itemId + ":\r\n"
					+ "				itemDef.name = \"Power card (" + power.getName() + ")\";\r\n"
					+ "				itemDef.inventoryOptions = new String[]{null, null, null, \"Info\", \"Drop\"};\r\n"
					+ "				itemDef.modelId = 21562;\r\n"
					+ "				itemDef.xof2d = 1;\r\n"
					+ "				itemDef.yof2d = 4;\r\n"
					+ "				itemDef.xan2d = 576;\r\n"
					+ "				itemDef.yan2d = 2047;\r\n"
					+ "				itemDef.zoom2d = 659;\r\n"
					+ "				itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};\r\n"
					+ "				itemDef.recolorModified = new int[] {44959, 44959, 44959, 44959, 44959, 44959, 44959, 10322, 10322, 10322, 10322, 10322};\r\n"
					+ "				break;");
					sb.append("\n");
					itemId++;
				}
			} else if (entry.getKey() == PowerUpRarity.RARE) {
				List<PowerUpData> powers = entry.getValue();
				for (PowerUpData power : powers) {
					sb.append("			case " + itemId + ":\r\n"
					+ "				itemDef.name = \"Power card (" + power.getName() + ")\";\r\n"
					+ "				itemDef.inventoryOptions = new String[]{null, null, null, \"Info\", \"Drop\"};\r\n"
					+ "				itemDef.modelId = 21562;\r\n"
					+ "				itemDef.xof2d = 1;\r\n"
					+ "				itemDef.yof2d = 4;\r\n"
					+ "				itemDef.xan2d = 576;\r\n"
					+ "				itemDef.yan2d = 2047;\r\n"
					+ "				itemDef.zoom2d = 659;\r\n"
					+ "				itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};\r\n"
					+ "				itemDef.recolorModified = new int[] {56088, 56088, 56088, 56088, 56088, 56088, 56088, 10322, 10322, 10322, 10322, 10322};\r\n"
					+ "				break;");
					sb.append("\n");
					itemId++;
				}
			} else if (entry.getKey() == PowerUpRarity.SUPER_RARE) {
				List<PowerUpData> powers = entry.getValue();
				for (PowerUpData power : powers) {
					sb.append("			case " + itemId + ":\r\n"
					+ "				itemDef.name = \"Power card (" + power.getName() + ")\";\r\n"
					+ "				itemDef.inventoryOptions = new String[]{null, null, null, \"Info\", \"Drop\"};\r\n"
					+ "				itemDef.modelId = 21562;\r\n"
					+ "				itemDef.xof2d = 1;\r\n"
					+ "				itemDef.yof2d = 4;\r\n"
					+ "				itemDef.xan2d = 576;\r\n"
					+ "				itemDef.yan2d = 2047;\r\n"
					+ "				itemDef.zoom2d = 659;\r\n"
					+ "				itemDef.recolorOriginal = new int[] {904, 920, 10308, 10304, 10295, 10299, 10291, 12, 8, 4676, 4680, 10343};\r\n"
					+ "				itemDef.recolorModified = new int[] {5034, 5034, 5034, 5034, 5034, 5034, 5034, 10322, 10322, 10322, 10322, 10322};\r\n"
					+ "				break;");
					sb.append("\n");
					itemId++;
				}
			}
		}

		System.out.println(sb);
	}

}
