package com.soulplay.content.seasonals.powerups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.soulplay.config.WorldType;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class PowerUpInterface {

	public static final int ID = 72000;

	public static void open(Player player) {
		updatePoints(player);
		player.getPacketSender().sendPowerUpData();
		player.getPacketSender().showInterface(ID);
	}

	private static List<PowerUpData> chooseList(Player player) {
		int combatLevel = player.combatLevel;
		if (combatLevel >= PowerUpRarity.SUPER_RARE.getCombatReq()) {
			int superRareRate = PowerUpRarity.SUPER_RARE.getRoll();
			if (player.getSeasonalData().rarityToAmount(PowerUpRarity.SUPER_RARE) < 1) {
				superRareRate /= 2;
			}

			if (Misc.randomBoolean(superRareRate)) {
				return PowerUpData.rarityToPerks.get(PowerUpRarity.SUPER_RARE);
			}
		}

		if (combatLevel >= PowerUpRarity.RARE.getCombatReq()) {
			int rareRate = PowerUpRarity.RARE.getRoll();
			if (player.getSeasonalData().rarityToAmount(PowerUpRarity.RARE) < 2) {
				rareRate /= 2;
			}

			if (Misc.randomBoolean(rareRate)) {
				return PowerUpData.rarityToPerks.get(PowerUpRarity.RARE);
			}
		}

		if (Misc.randomBoolean(PowerUpRarity.UNCOMMON.getRoll())) {
			return PowerUpData.rarityToPerks.get(PowerUpRarity.UNCOMMON);
		}

		return PowerUpData.rarityToPerks.get(PowerUpRarity.COMMON);
	}

	public static void rollSinglePowerUp(Player player) {
		List<PowerUpData> displayPerks = new ArrayList<>(Arrays.asList(PowerUpData.values));

		PowerUpData newPerk = null;

		PowerUpData powerFromCard = player.getSeasonalData().powerLearnedFromCard;
		if (powerFromCard != null && player.combatLevel >= powerFromCard.getRarity().getCombatReq()) {
			newPerk = powerFromCard;
			player.getSeasonalData().powerLearnedFromCard = null;
		}

		if (newPerk == null) {
			List<PowerUpData> perksToRoll = chooseList(player);
			List<PowerUpData> perks = new ArrayList<>();

			for (int i = 0, length = perksToRoll.size(); i < length; i++) {
				PowerUpData powerUpData = perksToRoll.get(i);
				int amount = player.getSeasonalData().getPowerUpAmount(powerUpData);
				if (amount < powerUpData.getAmount()) {
					perks.add(powerUpData);
				}
			}

			if (perks.isEmpty()) {
				player.sendMessage("You're unable to learn any new power-ups. Contact admins.");
				return;
			}

			Collections.shuffle(perks);
			newPerk = perks.get(0);
		}

		Collections.shuffle(displayPerks);
		player.getSeasonalData().addPowerUp(player, newPerk);
		displayPerks.remove(newPerk);
		player.getPacketSender().sendPowerUpUnlock(displayPerks, newPerk);
		String perkName = newPerk.getName();

		player.pulse(() -> {
			player.sendMessage("You've unlocked a new power up: <col=8000>" + perkName + "</col>!");
			if (player.interfaceIdOpenMainScreen == ID) {
				player.getPacketSender().sendPowerUpData();
			}
		}, 13);
	}

	public static void rollPowerUps(Player player) {
		if (!WorldType.isSeasonal()) {
			return;
		}

		SeasonalData seasonalData = player.getSeasonalData();
		if (!seasonalData.powerUps.isEmpty()) {
			return;
		}

		int commonIndex = 0;
		List<PowerUpData> commons = new ArrayList<>(PowerUpData.rarityToPerks.get(PowerUpRarity.COMMON));
		Collections.shuffle(commons);

		int uncommonIndex = 0;
		List<PowerUpData> uncommons = new ArrayList<>(PowerUpData.rarityToPerks.get(PowerUpRarity.UNCOMMON));
		Collections.shuffle(uncommons);

		player.sendMessage("You have learned some new powers: ");
		for (int i = 0; i < 4; i++) {
			PowerUpData power;
			if (Misc.randomBoolean(3)) {
				power = uncommons.get(uncommonIndex++);
			} else {
				power = commons.get(commonIndex++);
			}

			player.sendMessage("<col=8000>" + power.getName());
			seasonalData.addPowerUp(player, power);
		}

		player.startGraphic(Graphic.create(2011));
	}

	public static void updatePoints(Player player) {
		player.getPacketSender().sendString("Points: " + Variables.SEASONAL_INTERFACE_POINTS.getValue(player), 72005);
	}
	
	public static boolean handleOps(Player player, int buttonId, int option, int componentId) {
		switch (buttonId) {
			case 72010: {
				if (componentId < 0 || componentId >= PowerUpData.values.length) {
					return true;
				}

				PowerUpData powerUp = PowerUpData.values[componentId];
				if (option == 0) {
					if (!player.isAdmin()) {
						player.getDialogueBuilder().sendStatement("You can't use learn option on seasonals.").sendAction(() -> player.getPacketSender().sendChatInterface(-1, false)).execute(false);
						return true;
					}

					player.getSeasonalData().addPowerUp(player, powerUp);
					player.getPacketSender().sendPowerUpData();
				} else {
					boolean containsPod = Variables.SEASONAL_POD_UNLOCKED.getValue(player) > 0;
					int points = Variables.SEASONAL_INTERFACE_POINTS.getValue(player);
					if (points <= 0 && !containsPod) {
						player.getDialogueBuilder().sendStatement("You don't have enough points to forget a power.").sendAction(() -> player.getPacketSender().sendChatInterface(-1, false)).execute(false);
						return true;
					}

					if (!player.getSeasonalData().removePowerUp(player, powerUp)) {
						player.getDialogueBuilder().sendStatement("You don't have anything to forget.").sendAction(() -> player.getPacketSender().sendChatInterface(-1, false)).execute(false);
						return true;
					}

					if (!containsPod) {
						Variables.SEASONAL_INTERFACE_POINTS.addValue(player, -1);
					}

					player.getPacketSender().sendPowerUpData();
					rollSinglePowerUp(player);
					updatePoints(player);
				}
				return true;
			}
			case 72008:
				player.getSeasonalData().powerUpSortType = PowerUpSortType.list(option);
				return true;
			default:
				return false;
		}
	}

}
