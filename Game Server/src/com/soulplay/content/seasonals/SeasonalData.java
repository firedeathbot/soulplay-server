package com.soulplay.content.seasonals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.seasonals.powerups.PowerUpRarity;
import com.soulplay.content.seasonals.powerups.PowerUpSortType;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerClone;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public class SeasonalData {

	public static final int TORN_ID = 106477;
	// TODO make it scale properly of combat lvls
	public static final int MAX_POWER_UPS = 30;
	public Map<PowerUpData, Integer> powerUps = new HashMap<>();
	public PowerUpSortType powerUpSortType = PowerUpSortType.RARITY;
	public NPC spawned;
	public CombatType lastAttackedWith;
	public boolean nextSpellCrit;
	public boolean nextWindSpellCrit;
	public int regenPowerTick;
	public boolean procCritSprinter;
	public Map<Integer, Integer> recentlyBoughtItems = new HashMap<>();
	public PowerUpData powerLearnedFromCard;

	public void init(Player player) {
		if (!WorldType.isSeasonal()) {
			return;
		}

		tickRegen();

		for (PowerUpData powers : powerUps.keySet()) {
			Consumer<Player> onAdd = powers.getOnAdd();
			if (onAdd != null) {
				onAdd.accept(player);
			}
		}

		player.homeTeleActions = 1;
	}

	public void tickRegen() {
		regenPowerTick = Server.getTotalTicks() + Misc.secondsToTicks(5);
	}

	public static void transformBeastPower(Player player) {
		player.performingAction = true;
		player.startAnimation(17216);
		player.pulse(() -> {
			player.startAnimation(17218);
			player.transform(TORN_ID);
			player.pulse(() -> {
				player.getPA().resetMovementAnimation();
				player.resetAnimations();
				player.performingAction = false;
				player.getItems().refreshWeapon();
				player.blockRun = true;
				player.pulse(() -> {
					if (!isBeast(player)) {
						return;
					}

					endBeastPower(player);
				}, 11);
			}, 2);
		});
	}

	public static void endBeastPower(Player player) {
		player.transform(-1);
		player.blockRun = false;
		player.getPA().resetMovementAnimation();
		player.resetAnimations();
		player.getItems().refreshWeapon();
	}

	public static boolean isBeast(Player player) {
		return player.getTransformId() == TORN_ID;
	}

	private boolean addPowerUp(PowerUpData powerUpData) {
		if (powerUps.size() >= MAX_POWER_UPS) {
			return false;
		}

		Integer value = powerUps.get(powerUpData);
		if (value == null) {
			value = 1;
		} else {
			value++;
		}

		if (value > powerUpData.getAmount()) {
			return false;
		}

		powerUps.put(powerUpData, value);
		return true;
	}

	public boolean addPowerUp(Player player, PowerUpData powerUpData) {
		if (addPowerUp(powerUpData)) {
			Consumer<Player> onAdd = powerUpData.getOnAdd();
			if (onAdd != null) {
				onAdd.accept(player);
			}
			return true;
		}

		return false;
	}

	private boolean removePowerUp(PowerUpData powerUpData) {
		int amount = getPowerUpAmount(powerUpData);
		if (amount <= 0) {
			return false;
		}
		
		amount -= 1;
		if (amount > 0) {
			powerUps.put(powerUpData, amount);
		} else {
			powerUps.remove(powerUpData);
		}
		
		return true;
	}

	public boolean removePowerUp(Player player, PowerUpData powerUpData) {
		if (removePowerUp(powerUpData)) {
			if (!containsPowerUp(powerUpData)) {
				Consumer<Player> onRemove = powerUpData.getOnRemove();
				if (onRemove != null) {
					onRemove.accept(player);
				}
			}

			return true;
		}

		return false;
	}

	public int getPowerUpAmount(PowerUpData powerUpData) {
		if (!WorldType.isSeasonal()) {
			return 0;
		}

		return powerUps.getOrDefault(powerUpData, 0);
	}

	public boolean containsPowerUp(PowerUpData powerUpData) {
		return getPowerUpAmount(powerUpData) > 0;
	}

	public int rarityToAmount(PowerUpRarity rarity) {
		int count = 0;
		for (PowerUpData power : powerUps.keySet()) {
			if (power.getRarity() == rarity) {
				count++;
			}
		}

		return count;
	}

	public String getPowersJson() {
		return GsonSave.gsond.toJson(powerUps);
	}

	public void initPowers(String json) {
		Map<PowerUpData, Integer> powers = GsonSave.gsond.fromJson(json, new TypeToken<Map<PowerUpData, Integer>>(){}.getType());
		powerUps.putAll(powers);
	}

	public static void initClone(Player player, Mob focus, int ticks) {
		List<Location> availableCoords = new ArrayList<>();
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x == 0 && y == 0) {
					continue;
				}

				Location newCoord = player.getCurrentLocation().transform(x, y);
				if (focus == null || focus != null && !RegionClip.rayTraceBlocked(newCoord.getX(), newCoord.getY(), focus.getCurrentLocation().getX(), focus.getCurrentLocation().getY(), player.getCurrentLocation().getZ(), true, player.getDynamicRegionClip()))
					availableCoords.add(newCoord);
			}
		}

		player.setClone(new PlayerClone(Misc.random(availableCoords), ticks));
	}

	public void addBought(int itemId, int amount) {
		Integer value = recentlyBoughtItems.get(itemId);
		if (value == null) {
			value = amount;
		} else {
			value += amount;
		}

		recentlyBoughtItems.put(itemId, value);
	}

	public int getBoughtAmount(int itemId) {
		return recentlyBoughtItems.getOrDefault(itemId, 0);
	}

	public String getBoughtItemsJson() {
		return GsonSave.gsond.toJson(recentlyBoughtItems);
	}

	public void initBoughtItems(String json) {
		Map<Integer, Integer> items = GsonSave.gsond.fromJson(json, new TypeToken<Map<Integer, Integer>>(){}.getType());
		recentlyBoughtItems.putAll(items);
	}

}
