package com.soulplay.content.vote;

import java.util.function.Consumer;
import java.util.function.Predicate;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.util.Misc;

public enum VoteInterfaceRewards {

	BEGINEER(player -> true, player -> {
		int minutes = Misc.secondsToTicks(600);
		player.getItems().addItem(6199, 1);
		player.getItems().addItem(4447, 1);
		player.increaseDoubleExpLength(minutes);
	}),
	INTERMEDIATE(player -> Variables.TIMES_VOTED.getValue(player) >= 50 && player.getDaysPlayed() >= 3, player -> {
		int minutes = Misc.secondsToTicks(600);
		player.getItems().addItem(6199, 1);
		player.getItems().addItem(4447, 1);
		player.increaseDoubleExpLength(minutes);
		Variables.VOTE_POINTS.addValue(player, 10);
	}),
	ADVANCED(player -> Variables.TIMES_VOTED.getValue(player) >= 200 && player.getDaysPlayed() >= 10, player -> {
		int minutes = Misc.secondsToTicks(600);
		player.getItems().addItem(6199, 1);
		player.getItems().addItem(4447, 1);
		player.increaseDoubleExpLength(minutes);
		Variables.VOTE_POINTS.addValue(player, 10);
		player.increaseDropRateBoostLength(minutes);
	});

	public static final VoteInterfaceRewards[] values = values();
	private final Predicate<Player> predicate;
	private final Consumer<Player> rewards;

	VoteInterfaceRewards(Predicate<Player> predicate, Consumer<Player> rewards) {
		this.predicate = predicate;
		this.rewards = rewards;
	}

	public Consumer<Player> getRewards() {
		return rewards;
	}

	public static void applyReward(Player player) {
		for (int i = values.length - 1; i >= 0; i--) {
			VoteInterfaceRewards voteReward = values[i];
			if (voteReward.predicate.test(player)) {
				voteReward.rewards.accept(player);
				return;
			}
		}

		BEGINEER.rewards.accept(player);
	}

}
