package com.soulplay.content.vote;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.pets.pokemon.PokemonData;

public enum VoteInterfaceStoreData {
	
	RARE_DROP_BOOSTER(607, 35, 262348, true),
	DOUBLE_EXPERIENCE(761, 30, 262351, true),
	PURE_PK_POINT_BOOST(608, 25, 262354, true),
	MAIN_PK_POINT_BOOST(2403, 25, 262357, true),
	OVERLOAD(15332, 15, 262360, true),
	FROZEN_KEY(20120, 15, 262363, true),
	FIGHTER_TORSO(10551, 65, 262366, true),
	DRAGON_DEFENDER(20072, 70, 262369, true),
	BARROWS_GLOVES(7462, 20, 262372, true),
	FIRE_CAPE(6570, 100, 262375, true),
	TOKHAAR_KAL(23639, 1000, 262378, true),
	BONECRUSHER(18337, 200, 262381, true),
	RUNE_POUCH(30047, 300, 262384, true),
	PET_STAT_RESET_SCROLL(21821, 50, 262387, true),
	PET_DOUBLE_EXPERIENCE(15360, 15, 262390, true),
	PET_BOOST(21403, 10, 262393, true),
	STRONG_TAMING_ROPE(19668, 40, 262396, true),
	BOUNTY_TELEPORT_SCROLL(30267, 400, 262399, true),
	SLAYER_HELM(13263, 80, 262402, true),
	ABYSSAL_HEAD(7979, 100, 262405, true),
	KBD_HEADS(7980, 100, 262408, true),
	KQ_HEAD(7981, 100, 262411, true),
	VORKATH_HEAD(30329, 100, 262414, true),
	BLOOD_HOUND(30210, 1500, 262417, true),
	RING_OF_COINS(30192, 1000,262420, true),
	RING_OF_NATURE(30193, 1000, 262423, true),
	VOID_TOP(8839, 50, 262426, true),
	VOID_ROBE(8840, 50, 262429, true),
	VOID_GLOVES(8842, 50, 262432, true),
	VOID_MAGE_HELM(11663, 50, 262435, true),
	VOID_RANGE_HELM(11664, 50, 262438, true),
	VOID_MELEE_HELM(11665, 50, 262441, true),
	ROYAL_SEED_POD(119564, 200, 262444, true),
	IMBUED_GUTHIX_CAPE(123603, 350, 262447, true),
	IMBUED_ZAMORAK_CAPE(123605, 350, 262450, true),
	IMBUED_SARADOMIN_CAPE(123607, 350, 262453, true),
	
	TERROR_DOG(11365, 1000, 262456, true, PokemonData.TERROR_DOG),
	ANGRY_GOBLIN(3648, 1500, 262459, true, PokemonData.ANGRY_GOBLIN),
	CHAOS_DWOGRE(8771, 2500, 262462, true, PokemonData.CHAOS_DWOGRE),
	PHOENIX(8548, 5000, 262465, true, PokemonData.PHOENIX),
	NAIL_BEAST(1521, 5000, 262468, true, PokemonData.NAIL_BEAST);

	public static final Map<Integer, VoteInterfaceStoreData> values = new HashMap<>();
	private final int productId, price, buttonId;
	private final boolean canPurchase;
	private final PokemonData pokemonData;

	private VoteInterfaceStoreData(int productId, int price, int buttonId, boolean canPurchase) {
		this(productId, price, buttonId, canPurchase, null);
	}
	
	private VoteInterfaceStoreData(int productId, int price, int buttonId, boolean canPurchase, PokemonData pokemonData) {
		this.productId = productId;
		this.price = price;
		this.buttonId = buttonId;
		this.canPurchase = canPurchase;
		this.pokemonData = pokemonData;
	}

	public int getProductId() {
		return productId;
	}

	public int getPrice() {
		return price;
	}

	public int getButtonId() {
		return buttonId;
	}

	public boolean canPurchase() {
		return canPurchase;
	}
	
	public PokemonData getPokemonData() {
		return pokemonData;
	}

	public static void load() {
		for (VoteInterfaceStoreData data : values()) {
			values.put(data.getButtonId(), data);
		}
	}

}
