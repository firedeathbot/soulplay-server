package com.soulplay.content.vote;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;

public class VoteInterfaceLuck {

	private static final int[][] ITEMS = {
			{
				4151, 4153, 4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722,
				4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747,
				4749, 4751, 4753, 4755, 4757, 4759, 11283, 11235, 15486, 21371,
				12006, 11732, 6585, 6737, 6733, 6735, 6735, 12605, 12603, 12601,
				14479, 11335, 11730, 10887
			},
			{
				11694, 11696, 11698, 11700, 13738, 13740, 13742, 13744, 11726, 11724, 11718,
				11720, 11722, 14484, 20135, 20139, 20143, 20147, 20151, 20155,
				20159, 20163, 20167, 20171, 19780, 30174, 30175, 30203, 30206, 30207, 30208, 786,
				121637, 124268
			},
			{
				1038, 1040, 1042, 1044, 1046, 1048, 1050, 15741, 4083, 30092, 30354, 30328, 30422, 30423
			}
	};

	public static boolean buttonClick(Client c, int buttonId) {
		if (buttonId >= 264316 && buttonId <= 264445) {
			if (!c.miscTimer.complete()) {
				return true;
			}

			int currentValue = Variables.VOTE_LUCK_ITEM_DATA.getValue(c);
			if (currentValue == 0) {
				if (Variables.VOTE_POINTS.getValue(c) < 10) {
					c.sendMessage("You need at least 10 vote points to try your luck.");
					return true;
				}

				if (c.getDaysPlayed() < 1) {
					c.sendMessage("You need to have a higher time played to try your luck.");
					return true;
				}

				if (Variables.TIMES_VOTED.getValue(c) < 20) {
					c.sendMessage("You must have voted at least 20 times to try your luck.");
					return true;
				}

				Variables.VOTE_POINTS.addValue(c, -10);
				VoteInterface.updateVotePoints(c);

				int index;
				if (Misc.randomBoolean(75)) {
					index = 2;
				} else {
					index = Misc.random(1);
				}

				int reward = Misc.random(ITEMS[index]);
				int slot = (buttonId - 264316) / 3;
				int value = index | slot << 2 | reward << 8;
				Variables.VOTE_LUCK_ITEM_DATA.setValue(c, value);
				c.getPacketSender().sendVoteInterfaceLuck(slot, reward, -1, -1);
				c.miscTimer.startTimer(1);
			} else {
				int newSlot = (buttonId - 264316) / 3;
				int oldSlot = currentValue >> 2 & 0x3f;
				if (newSlot == oldSlot) {
					c.sendMessage("You can't click the same slot.");
					return true;
				}

				int index = currentValue & 0x3;
				int reward = currentValue >>> 8;
				int newReward;
				if (index == 2 || Misc.random(12) != 0) {
					int[] items = ITEMS[index];
					int length = items.length;
					int randomIndex = Misc.randomNoPlus(length);
					newReward = items[randomIndex];
					if (newReward == reward) {
						newReward = items[++randomIndex % length];
					}
				} else {
					newReward = reward;

					c.sendMessage("You've received a " + ItemDefinition.getName(reward) + " for matching 2 of the same item.");
					c.sendMessage("If you have no space in inventory, your item is added to your bank.");
					if (!c.getItems().addOrBankItem(reward, 1)) {
						ItemHandler.createGroundItem(c, reward, c.getX(), c.getY(), c.getZ(), 1);
						c.sendMessage("Your reward has been dropped on ground because no space in bank or inventory.");
					}
				}

				c.getPacketSender().sendVoteInterfaceLuck(oldSlot, reward, newSlot, newReward);
				Variables.VOTE_LUCK_ITEM_DATA.setValue(c, 0);

				int ticks = Misc.secondsToTicks(3);
				c.miscTimer.startTimer(ticks);
				c.pulse(() -> {
					c.getPacketSender().sendVoteInterfaceLuck(-1, -1, -1, -1);
				}, ticks);
			}

			return true;
		}

		return false;
	}

	public static void updateLuck(Player player) {
		int currentValue = Variables.VOTE_LUCK_ITEM_DATA.getValue(player);
		if (currentValue == 0) {
			player.getPacketSender().sendVoteInterfaceLuck(-1, -1, -1, -1);
			return;
		}

		int slot = currentValue >> 2 & 0x3f;
		int reward = currentValue >>> 8;
		player.getPacketSender().sendVoteInterfaceLuck(slot, reward, -1, -1);
	}

}
