package com.soulplay.content.vote;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;

public enum VoteInterfaceStreak {

	STREAK1(25, player -> {
		addItem(player, new Item[] { new Item(995, 10_000_000) });
	}, "10M Coins"),
	STREAK2(50, player -> {
		addItem(player, new Item[] { new Item(995, 20_000_000) });
	}, "20M Coins"),
	STREAK3(75, player -> {
		addItem(player, new Item[] { new Item(30174, 1) });
	}, "Super Mystery Box"),
	STREAK4(100, player -> {
		addItem(player, new Item[] { new Item(995, 50_000_000) });
	}, "50M Coins"),
	STREAK5(150, player -> {
		addItem(player, new Item[] { new Item(30175, 1) });
	}, "Legendary Mystery Box"),
	STREAK6(200, player -> {
		addItem(player, new Item[] { new Item(995, 100_000_000) });
	}, "100M Coins"),
	STREAK7(250, player -> {
		addItem(player, new Item[] { new Item(30176, 1) });
	}, "Ultra Mystery Box"),
	STREAK8(325, player -> {
		player.unlockPokemon(PokemonData.AYUNI);
	}, "Ayuni Gorilla Pet"),
	STREAK9(400, player -> {
		addItem(player, new Item[] { new Item(786, 1) });
	}, "10$ Donator Scroll"),
	STREAK10(500, player -> {
		player.unlockPokemon(PokemonData.TOKTZ_KET_DILL);
	}, "TokTz-Ket-Dill pet");

	public static final VoteInterfaceStreak[] values = values();
	public static final Map<Integer, VoteInterfaceStreak> valueToStreak = new HashMap<>();
	private final int votesRequired;
	private final Consumer<Player> consumer;
	private final String title;

	private VoteInterfaceStreak(int votesRequired, Consumer<Player> consumer, String title) {
		this.votesRequired = votesRequired;
		this.consumer = consumer;
		this.title = title;
	}

	public int getVotesRequired() {
		return votesRequired;
	}

	public Consumer<Player> getConsumer() {
		return consumer;
	}

	public String getTitle() {
		return title;
	}

	public static void load() {
		for (VoteInterfaceStreak value : values) {
			valueToStreak.put(value.getVotesRequired(), value);
		}
	}

	private static void addItem(Player player, Item[] items) {
		boolean dropGround = false;
		for (int i = 0, length = items.length; i < length; i++) {
			Item item = items[i];
			if (item == null) {
				continue;
			}

			if (!player.getItems().addOrBankItem(item.getId(), item.getAmount())) {
				ItemHandler.createGroundItem(player, item.getId(), player.getX(), player.getY(), player.getZ(),
						item.getAmount());
				dropGround = true;
			}
		}

		player.sendMessage("Your vote streak items have been added to bank or inventory if you had enough space.");
		if (dropGround) {
			player.sendMessage("Some of your vote streak items have been dropped on the ground!");
		}
	}

}
