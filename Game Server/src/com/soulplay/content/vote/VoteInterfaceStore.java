package com.soulplay.content.vote;

import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.config.Variables;

public class VoteInterfaceStore {

	public static boolean buttonClick(Client c, int buttonId) {
		{
			VoteInterfaceStoreData data = VoteInterfaceStoreData.values.get(buttonId);
			if (data != null) {
				c.selectedVoteReward = data;
				return true;
			}
		}

		switch (buttonId) {
			case 263487:
				VoteInterfaceStoreData data = c.selectedVoteReward;
				if (data == null) {
					return true;
				}

				if (!data.canPurchase()) {
					c.sendMessage("You can't purchase this at the moment.");
					return true;
				}

				if (Variables.VOTE_POINTS.getValue(c) < data.getPrice()) {
					c.sendMessage("You don't have enough vote points to buy this.");
					return true;
				}

				PokemonData pokemonData = data.getPokemonData();
				if (pokemonData != null) {
					if (c.isPokemonUnlocked(pokemonData)) {
						c.sendMessage("You already own this pet.");
						return true;
					}

					c.unlockPokemon(pokemonData);
					Variables.VOTE_POINTS.addValue(c, -data.getPrice());
					VoteInterface.updateVotePoints(c);
					c.sendMessage("You've unlocked a new pet!");
				} else {
					if (c.getItems().addItem(data.getProductId(), 1)) {
						Variables.VOTE_POINTS.addValue(c, -data.getPrice());
						VoteInterface.updateVotePoints(c);
					}
				}

				return true;
			default:
				return false;
		}
	}

}
