package com.soulplay.content.vote;

import com.soulplay.Config;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.vote.VoteClaimSql;

public class VoteInterface {

	public static boolean disabled = false;

	public static void open(Player player) {
		if (disabled) {
			player.getDialogueBuilder().sendStatement("Vote interface is currently disabled.").execute();
			return;
		}

		player.getPacketSender().showInterface(67300);
		VoteInterfaceLuck.updateLuck(player);
		player.getPacketSender().sendVoteInterfaceStreak();
		updateVotePoints(player);
	}

	public static void updateVotePoints(Player player) {
		player.getPacketSender().sendString("Vote points: " + Misc.format(Variables.VOTE_POINTS.getValue(player)), 67315);
	}

	public static boolean buttonClick(Player player, int buttonId) {
		switch (buttonId) {
			case 67311:
				claimVotes(player);
				return true;
			case 67313:
				openVotePage(player);
				return true;
			default:
				return false;
		}
	}

	public static void openVotePage(Player player) {
		player.getPacketSender().openUrl(String.format(Config.VOTE, player.getName()));
	}

	public static void claimVotes(Player player) {
		if (!player.inEdgeville()) {
			player.sendMessage("You must be in EdgeVille to claim votes.");
			return;
		}

		if (player.getItems().freeSlots() < 5) {
			player.sendMessage("You need at least 5 free slots to claim votes.");
			return;
		}

		if (player.getSpamTick().checkThenStart(5)) {
			VoteClaimSql.claimByUsernameNames(player);
			player.sendMessage("Checking for unclaimed votes...");
		} else {
			player.sendMessage("Please slow down.");
		}
	}

}
