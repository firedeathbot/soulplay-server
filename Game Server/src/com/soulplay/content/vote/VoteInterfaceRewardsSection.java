package com.soulplay.content.vote;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.save.SoulPlayDateSave;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.soulplaydate.SoulPlayDateEnum;

public class VoteInterfaceRewardsSection {

	public static void checkLogin(Player player) {
		int prevSoulplayDay = player.getSoulplayDate().getLastVoted();
		if (prevSoulplayDay == 0) {
			return;
		}

		int thisSoulplayDay = SoulPlayDate.getDate();
		int delta = thisSoulplayDay - prevSoulplayDay;
		if (delta < 2) {
			return;
		}

		SoulPlayDateSave.setNewDay(player, 0, SoulPlayDateEnum.LAST_VOTE);
		Variables.VOTE_STREAK.setValue(player, 0);
		player.sendMessage("Your vote streak has been lost!");
	}

	public static void checkStreak(Player player) {
		int streakValue = Variables.VOTE_STREAK.getValue(player);
		VoteInterfaceStreak streak = VoteInterfaceStreak.valueToStreak.get(streakValue);
		if (streak == null) {
			player.sendMessage("Your current vote streak is " + streakValue);
			return;
		}

		streak.getConsumer().accept(player);
		player.sendMessage("Your current vote streak is " + streakValue);
	}

}
