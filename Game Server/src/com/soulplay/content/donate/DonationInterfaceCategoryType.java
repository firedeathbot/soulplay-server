package com.soulplay.content.donate;

public enum DonationInterfaceCategoryType {

	FOUR_TO_FOUR(0, 8),
	TWO_TO_TWO(1, 4),
	ONE_TO_TWO(2, 3),
	ONE_TO_ONE(3, 2),
	ONE(4, 1);

	private static DonationInterfaceCategoryType[] values = values();
	private final int id, elements;

	DonationInterfaceCategoryType(int id, int elements) {
		this.id = id;
		this.elements = elements;
	}

	public int getId() {
		return id;
	}
	
	public int getElements() {
		return elements;
	}

	public static DonationInterfaceCategoryType list(int id) {
		for (DonationInterfaceCategoryType value : values) {
			if (value.id == id) {
				return value;
			}
		}

		return FOUR_TO_FOUR;
	}

}
