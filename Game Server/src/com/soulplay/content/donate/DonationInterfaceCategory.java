package com.soulplay.content.donate;

import java.util.ArrayList;
import java.util.List;

public class DonationInterfaceCategory {

	private final String title;
	private List<DonationInterfaceCategoryEntry> entries = new ArrayList<>();
	private final int discountPercentage;
	private final DonationInterfaceCategoryType type;

	public DonationInterfaceCategory(String title, int discountPercentage, int type) {
		this.title = title;
		this.discountPercentage = discountPercentage;
		this.type = DonationInterfaceCategoryType.list(type);
	}

	public String getTitle() {
		return title;
	}

	public void addEntry(DonationInterfaceCategoryEntry entry) {
		if (discountPercentage > 0) {
			int newPrice = entry.getPrice();
			newPrice = newPrice - (newPrice * discountPercentage / 100);

			if (entry.getDiscountPrice() == -1 || newPrice < entry.getDiscountPrice()) {
				entry.setDiscountPrice(newPrice);
			}
		}

		entries.add(entry);
	}

	public List<DonationInterfaceCategoryEntry> getEntries() {
		return entries;
	}

	public DonationInterfaceCategoryType getType() {
		return type;
	}

}
