package com.soulplay.content.donate;

import java.util.function.Function;

import com.soulplay.game.model.player.Player;

public class DonationInterfaceCategoryEntry {

	private final int displayId, price;
	private int discountPrice;
	private final String title;
	private final boolean isPet;
	private final Function<Player, String> buyFunction;

	public DonationInterfaceCategoryEntry(int displayId, int price, int discountPrice, String title, boolean isPet,
			Function<Player, String> buyFunction) {
		this.displayId = displayId;
		this.price = price;
		this.discountPrice = discountPrice;
		this.title = title;
		this.isPet = isPet;
		this.buyFunction = buyFunction;
	}

	public int getDisplayId() {
		return displayId;
	}

	public int getPrice() {
		return price;
	}

	public int getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(int discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getTitle() {
		return title;
	}

	public boolean isPet() {
		return isPet;
	}

	public Function<Player, String> getBuyFunction() {
		return buyFunction;
	}

	public int getBuyPrice() {
		if (discountPrice > -1) {
			return discountPrice;
		}

		return price;
	}

}
