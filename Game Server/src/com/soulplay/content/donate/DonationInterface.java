package com.soulplay.content.donate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

public class DonationInterface {

	private static final Map<Integer, DonationInterfaceCategory> idToCategory = new HashMap<>();
	private static final List<DonationInterfaceCategory> categories = new ArrayList<>();

	public static void open(Player player) {
		player.getPacketSender().showInterface(69000);
		player.getPacketSender().sendDonationInterfaceCategoryData();
		player.getPacketSender().sendDonationInterfaceData();
		updatePoints(player);
	}

	public static void load() {
		idToCategory.clear();
		categories.clear();

		TaskExecutor.executeSQL(() -> {
			Map<String, DonationInterfaceCategory> nameToCategory = new HashMap<>();
			try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT * FROM donation_interface_category")) {

				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					int id = rs.getInt("id");
					String title = rs.getString("title");
					int discount = rs.getInt("discount_percentage");
					int type = rs.getInt("type");

					if (id < 0) {
						continue;
					}

					DonationInterfaceCategory category = new DonationInterfaceCategory(title, discount, type);
					categories.add(category);
					idToCategory.put(id, category);
					nameToCategory.put(category.getTitle().toLowerCase(), category);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

			try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection(); PreparedStatement ps = connection.prepareStatement("SELECT * FROM donation_interface_category_entry")) {

				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					int id = rs.getInt("id");
					int displayId = rs.getInt("display_id");
					String title = rs.getString("title");
					int price = rs.getInt("price");
					int discountPrice = rs.getInt("discount_price");
					String categories = rs.getString("category_id");
					String product = rs.getString("product");
					boolean isPet = rs.getBoolean("is_pet");

					if (id < 0) {
						continue;
					}

					DonationInterfaceCategoryEntry entry = new DonationInterfaceCategoryEntry(displayId, price, discountPrice, title, isPet, makeBuyFunction(product, isPet));

					String[] categoriesSplit = categories.split(",");
					for (String categoryName : categoriesSplit) {
						categoryName = categoryName.toLowerCase();

						DonationInterfaceCategory category = nameToCategory.get(categoryName);
						if (category == null) {
							System.out.println("Invalid category."); // TODO do message
							continue;
						}

						category.addEntry(entry);
					}
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
	}

	private static Function<Player, String> makeBuyFunction(String product, boolean isPet) {
		return player -> {
			if (isPet) {
				String data = product.substring(1, product.length() - 1);
				String[] args = data.split(",");
				int index = Integer.parseInt(args[0]) - 1;
				if (player.isPokemonUnlocked(index)) {
					return "Pet already unlocked.";
				}

				player.unlockPokemon(index);
				PokemonButtons.refreshData(player);
				return null;
			} else {
				String data = product;
				if (data.isEmpty()) {
					return "Entry data is empty.";
				}

				String[] split = data.split("~");
				int length = split.length;
				if (length <= 0) {
					return "Entry length is empty.";
				}

				for (int i = 0; i < length; i++) {
					String rawData = split[i].substring(1, split[i].length() - 1);
					String[] dataSplit = rawData.split(",");
					if (dataSplit.length < 2) {
						continue;
					}

					int itemId = Integer.parseInt(dataSplit[0].trim());
					int amount = Integer.parseInt(dataSplit[1].trim());

					if (!player.getItems().addOrBankItem(itemId, amount)) {
						ItemHandler.createGroundItem(player, itemId, player.getX(), player.getY(), player.getHeightLevel(), amount);
						player.sendMessage("<col=ff0000>Your item was dropped on the ground be cause your inventory and bank is full!");
					}
				}

				return null;
			}
		};
	}

	public static void updatePoints(Player player) {
		player.getPacketSender().sendString("Donation points: " + Misc.format(player.getDonPoints()), 69011);
	}

	public static List<DonationInterfaceCategory> getCategories() {
		return categories;
	}
	
}
