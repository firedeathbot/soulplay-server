package com.soulplay.content;

import com.soulplay.game.model.player.Player;

public enum JudgementHandler {
	CHATABUSE, SPAM, ADVERTISING, CCABUSE, RWT;

	public static int calculateTimer(Player c, JudgementHandler jh) {
		if (jh == CHATABUSE) {
			return c.getChatAbuse() * 600;
		}
		return 0;
	}
	
}
