package com.soulplay.content.combat;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.combat.data.FightExp;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.combat.specialattacks.SpecialEffects;
import com.soulplay.content.items.Ammunition;
import com.soulplay.content.items.RangeWeapon;
import com.soulplay.content.items.WeaponType;
import com.soulplay.content.items.degradeable.Degrade;
import com.soulplay.content.items.itemdata.WeaponAttackTypes;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.npcs.impl.NecromancerSkeleton;
import com.soulplay.content.player.combat.AttackNPC;
import com.soulplay.content.player.combat.AttackPlayer;
import com.soulplay.content.player.combat.CombatCrit;
import com.soulplay.content.player.combat.CombatCrit.CritRollType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy;
import com.soulplay.content.player.combat.accuracy.CombatAccuracy.AccuracyType;
import com.soulplay.content.player.combat.magic.MultiBarrage;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.melee.MeleeData;
import com.soulplay.content.player.combat.melee.MeleeExtras;
import com.soulplay.content.player.combat.melee.MeleeRequirements;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.combat.range.BoltEffect;
import com.soulplay.content.player.combat.range.RangeExtras;
import com.soulplay.content.player.dailytasks.TaskConstants;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.seasonals.SeasonalData;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.tob.npcs.VerzikWeb;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.sounds.SoundManager;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

import plugin.item.DawnbringerPlugin;
import plugin.item.DihnBulwarkPlugin;
import plugin.item.chargeitems.CrawsBowPlugin;

public class AttackMob {
	
	public static final Graphic VERACS_EFFECT = Graphic.create(1041 + Config.OSRS_GFX_OFFSET, GraphicType.HIGH);

	private static BattleState makeState(Client attacker, Mob defender) {
		BattleState state = new BattleState(attacker, defender);
		
		CombatType style = CombatType.MELEE;

		int dist = attacker.distanceToPoint(defender.getCurrentLocation().getX(), defender.getCurrentLocation().getY()); // commented out because i'm using withinDistanceToSize() function now

		SpellsData spellData = null;
		if (attacker.getSingleCastSpell() != null) {
			spellData = attacker.getSingleCastSpell();
		} else if (attacker.getAutoCastSpell() != null) {
			spellData = attacker.getAutoCastSpell();
		}

		if (spellData != null) {
			state.setSpellsData(spellData);
			style = CombatType.MAGIC;
			if (!MagicRequirements.checkMagicReqsNew(attacker, spellData, true)) {
				attacker.getMovement().resetWalkingQueue();
				attacker.getCombat().resetPlayerAttack();
				return null;
			}

			state.setAnimation(spellData.getAnim());
			state.setSoundId(spellData.getSoundId());
			state.setAttackerGfx(spellData.getStartGfx());
			state.setVictimGfx(spellData.getEndGfx());
			state.setCurrentHit(Misc.newRandom(attacker.getCombat().magicMaxHit(spellData)));
			
			if (spellData.getProjectile() != null) {
				Projectile projectile = spellData.getProjectile().copy();
				projectile.setStartLoc(attacker.getCurrentLocation());
				projectile.setEndLoc(defender.getCurrentLocation());
				projectile.setLockon(defender.getProjectileLockon());
				projectile.setStartDelay(53 - dist);
				projectile.setEndDelay(5 * dist + 55);
				state.setProj(projectile);
			}
		}

		RangeWeapon rangeWeapon = attacker.getRangeWeapon();
		if (style != CombatType.MAGIC) {
			//if not using magic then call this
			if (rangeWeapon != null) {
				if (rangeWeapon.getAmmunitionSlot() == PlayerConstants.playerArrows) { // using ammow slot 13, arrows and bolts mainly.
					int ammoTypeSlot13 = attacker.playerEquipment[PlayerConstants.playerArrows];
					if (ammoTypeSlot13 < 1) {
						if (rangeWeapon.getItemId() == 15241 || rangeWeapon.getItemId() == 24145)
							attacker.sendMessage("You have ran out of handcannon shots.");
						else if (rangeWeapon.getItemId() == 30023 || rangeWeapon.getItemId() == 30024)
							attacker.sendMessage("You have ran out of javelins.");
						else
							attacker.sendMessage("You have ran out of " + (rangeWeapon.getWeaponType() == WeaponType.CROSSBOW ? "bolts." : "arrows."));
						attacker.getCombat().resetPlayerAttack();
						attacker.getMovement().resetWalkingQueue();
						return null;
					} else {
						if (!rangeWeapon.getAmmunition().contains(ammoTypeSlot13)) {
							attacker.sendMessage("You cannot use <col=ff>"+ItemDefinition.getName(ammoTypeSlot13)+"</col> with <col=ff>" + ItemDefinition.getName(rangeWeapon.getItemId()) + "</col>.");
							attacker.getCombat().resetPlayerAttack();
							attacker.getMovement().resetWalkingQueue();
							return null;
						}
					}
				}
				style = CombatType.RANGED;
				state.setDropAmmo(rangeWeapon.isDropAmmo());
			}
		}

		state.setCombatStyle(style);
		SpecialAttack spec;
		if (!SeasonalData.isBeast(state.getAttacker())) {
			spec = SpecialAttack.get(attacker.playerEquipment[PlayerConstants.playerWeapon]);
		} else {
			spec = null;
		}

		//cheaphax this shit
		if (defender.isNpc() && !npcAttackableRules(state))
			return null;

		//some more req for ranged checks
		if (!checkMiniGameRulesForCombatStyles(attacker, style))
			return null;
		
		//check for distance to victim?
		if (!isWithinDistanceToHit(state, spec)) {
			if (style == CombatType.MELEE && attacker.getFreezeTimer() > 0) {
				attacker.getCombat().resetPlayerAttack();
				attacker.getMovement().resetWalkingQueue();
			}

			return null;
		}
		
		if (style != CombatType.MAGIC && attacker.toggledDfs) {
			if (attacker.withinDistance(defender, 14)) {
				if (defender.isNpc())
					attacker.getCombat().handleDfsNPC(attacker, defender.toNpc());
				else if (defender.isPlayer())
					attacker.getCombat().handleDfs(attacker, (Client)defender.toPlayer());
			} else {
				attacker.sendMessage("You are too far.");
			}
			attacker.toggledDfs = false;
			return null;
		}
		
//		int distReq = CombatFormulas.getRangeDistReq(attacker);
//		if (style == CombatType.RANGED && attacker.getFightXp() == 5) {
//			distReq += 2;
//		}
//		if (attacker.getFreezeTimer() <= 0 && attacker.getMovement().isMoving()) { // if (c.isMoving
//			// &&
//			// n.isMoving) {
//			distReq += 2;
//		}
//		if (attacker.getFreezeTimer() > 0) {
//			if (distReq < 3) {
//				distReq = CombatFormulas.getRangeDistReq(attacker);
//			}
//		}
//		
//		if (defender.isNpc())
//			distReq += defender.toNpc().getHitOffset();

		int specialAttack = attacker.usingSpecial ? attacker.playerEquipment[PlayerConstants.playerWeapon] : 0;

		//check if enough ammo
		if (specialAttack == 30056 || specialAttack == 861 || (rangeWeapon != null && rangeWeapon.getWeaponType() == WeaponType.DOUBLE_SHOT))  {
			if (attacker.playerEquipmentN[PlayerConstants.playerArrows] < 2) {
				attacker.sendMessage("You don't have enough arrows to use the special attack.");
				attacker.getCombat().resetPlayerAttack();
				attacker.getMovement().resetWalkingQueue();
				return null;
			}
		}
		
		// dwarf hand cannon explosion cheaphax
		if (specialAttack == 15241) {
			if (attacker.getSkills().getLevel(Skills.FIREMAKING) != 99
					&& Misc.newRandom(attacker.getSkills().getLevel(Skills.FIREMAKING) * 10) == 7) {
				attacker.getItems().deleteEquipment(attacker.playerEquipment[PlayerConstants.playerWeapon], PlayerConstants.playerWeapon);
				attacker.sendMessage("Your hand cannon exploded!");
				return null;
			}
		}

		//set delay of the next hit.
		int attackDelay = attacker.getCombat().getAttackDelay(spellData);
		if (attacker.miasmicEffectActive() && style != CombatType.MAGIC) {
			attackDelay += attackDelay / 2;
		}

		if (attacker.attr().contains("nightmare_drowsy")) {
			attackDelay++;
		}

		switch (style) {
			case MELEE:
				if (attacker.getSeasonalData().containsPowerUp(PowerUpData.NINJA)) {
					attackDelay--;
				}

				if (attacker.getSeasonalData().containsPowerUp(PowerUpData.TWO_HANDER) && ItemConstants.is2handed(attacker.playerEquipment[PlayerConstants.playerWeapon])) {
					attackDelay--;
				}
				break;
			case MAGIC:
				if (attacker.getSeasonalData().containsPowerUp(PowerUpData.FROST_SHOCK)) {
					attackDelay--;
				}
				break;
			case RANGED:
				if (attacker.getSeasonalData().containsPowerUp(PowerUpData.CRIPPLING_POISON)) {
					attackDelay--;
				}
				break;
		}

		attacker.setAttackTimer(attackDelay);

		if (attacker.getSkills().getLifepoints() > 0 && !attacker.isDead()
				&& defender.getSkills().getMaximumLifepoints() > 0) {
			if (style != CombatType.MAGIC) {
				state.setAnimation(attacker.getWeaponAnimation());
				state.setSoundId(SoundManager.getWeaponAttackSound(attacker, attacker.playerEquipment[PlayerConstants.playerWeapon]));
			}
		}
		
		int hitDelay = 1;
		
		attacker.face(defender);

		if (style == CombatType.RANGED) {
			if (attacker.getWeaponAttackType() == WeaponAttackTypes.RAPID)
				attacker.setAttackTimer(attacker.getAttackTimer()-1);
			if (attacker.playerEquipment[PlayerConstants.playerWeapon] != 15241)
				hitDelay = 2;
		} else if (style == CombatType.MAGIC) {
			hitDelay = 3;
		}
		
		if (style != CombatType.MELEE) {
//			if (dist > 2) {
//				hitDelay += 1;
//			}

			if (dist > 5) {
				hitDelay += 1;
			}
			if (dist > 8) {
				hitDelay += 1;
			}
		}
		
		if (style == CombatType.MELEE) {
			state.setCurrentHit(Misc.newRandom(attacker.getCombat().calculateMeleeMaxHit(1.00)));
		} else if (style == CombatType.RANGED) {
			state.setCurrentHit(Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.0)));
		}
		
		state.setRangeWeapon(rangeWeapon);
		state.setSpecialAttack(specialAttack);
		state.setHitDelay(hitDelay);
		state.setDistanceToVictim(dist);
		state.setFightExp(attacker.getFightXp());
		
		// some visuals
		if (style != CombatType.MAGIC && rangeWeapon != null) { // using range projectile, so send it! Also the start gfx
			Ammunition ammo = null;
			if (rangeWeapon.getItemId() == BlowPipeCharges.BLOWPIPE_FULL) { // toxic blowpipe cheaphax
				ammo = Ammunition.get(attacker.getBlowpipeAmmoType());
				if (ammo == null) { // no ammo in blowpipe
					attacker.getCombat().resetPlayerAttack();
					attacker.getMovement().resetWalkingQueue();
					attacker.sendMessage("Your blowpipe has ran out of ammo.");
					return null;
				}
			} else if (rangeWeapon.getAmmunitionSlot() == PlayerConstants.playerWeapon)
				ammo = Ammunition.get(attacker.playerEquipment[PlayerConstants.playerWeapon]);
			else if (rangeWeapon.getAmmunitionSlot() == PlayerConstants.playerArrows)
				ammo = Ammunition.get(attacker.playerEquipment[PlayerConstants.playerArrows]);
			
			if (ammo == null) {
				attacker.sendMessage("This weapon is not finished! Needs ammo added to database!");
				attacker.getCombat().resetPlayerAttack();
				return null;
			}
			
			state.setAmmo(ammo);
			
			Projectile projectile = ammo.getProjectile().copy();
			
			state.setProj(projectile);
			
			projectile.setSlope(projectile.getSlope() + dist);
			
			if (rangeWeapon.getWeaponType() == WeaponType.DOUBLE_SHOT) { // darkbow
				boolean descentOfDragons = false;
				Graphic dbowGraphic = Graphic.create(ammo.getDarkBowGraphics().getId(), ammo.getStartGraphics().getType());
				projectile.setLockon(defender.getProjectileLockon());
				projectile.setStartLoc(attacker.getCurrentLocation());
				projectile.setEndLoc(defender.getCurrentLocation());
				projectile.setStartDelay(attacker.getCombat().getStartDelay(dist));
				projectile.setEndDelay(attacker.getCombat().getProjectileSpeed(dist));
				projectile.setSlope(projectile.getSlope()+13);
				
				Projectile arrow2 = projectile.copy();
				arrow2.setStartDelay(projectile.getStartDelay()-10);
				arrow2.setEndDelay(projectile.getEndDelay()-10);
				arrow2.setSlope(5);

				BattleState state2 = state.copy(attacker, defender);
				
				if (state2.getDistanceToVictim() > 3) {
					state2.setHitDelay(state2.getHitDelay() + 1);
				} else if (state2.getDistanceToVictim() > 6) {
					state2.setHitDelay(state2.getHitDelay() + 2);
				}
				
				if (state.getSpecialAttack() > 0 && spec != null && spec.canActivate(state)) { //TODO: maybe move this shit to SpecialAttack class???? better there???
					descentOfDragons = ammo.getDarkBowGraphics().getId() == 1114;
					projectile.setProjectileGFX(descentOfDragons ? 1099 : 1101);
					arrow2.setProjectileGFX(descentOfDragons ? 1099 : 1101);
					Graphic hitDBowGFX = Graphic.create(descentOfDragons ? 1100 : 1103,
							Misc.serverToClientTick(hitDelay), GraphicType.HIGH);
					defender.startGraphic(hitDBowGFX);
					if (descentOfDragons) {
						state.setSpecEffect(SpecialEffects.DARK_BOW2);
						state2.setSpecEffect(SpecialEffects.DARK_BOW2);
						state.setCurrentHit(Math.max(8, Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.5))));
						state2.setCurrentHit(Math.max(8, Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.5))));
						dbowGraphic = Graphic.create(1111, GraphicType.BOWARROWPULL);
					} else {
						state.setSpecEffect(SpecialEffects.DARK_BOW1);
						state2.setSpecEffect(SpecialEffects.DARK_BOW1);
						state.setCurrentHit(Math.max(5, Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.3))));
						state2.setCurrentHit(Math.max(5, Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.3))));
					}
				} else if (state.getSpecialAttack() <= 0) {
					state.setCurrentHit(Math.max(8, Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.0))));
					state2.setCurrentHit(Math.max(8, Misc.newRandom(attacker.getCombat().rangeMaxHit(defender, 1.0))));
				}

				attacker.startGraphic(dbowGraphic);
				state.setAttackerGfx(dbowGraphic.copy());
				GlobalPackets.createProjectile(arrow2);
				if (state.getCurrentHit() > 48)
					state.setCurrentHit(48);
				
				if (state2.getCurrentHit() > 48)
					state2.setCurrentHit(48);
				AttackMob.applyDamage(state2);

			} else { // everything else other than dark bow
				projectile.setLockon(defender.getProjectileLockon());
				projectile.setStartLoc(attacker.getCurrentLocation());
				projectile.setEndLoc(defender.getCurrentLocation());
				projectile.setEndDelay(projectile.getEndDelay() + (5*dist));
				
				if (projectile.getEndDelay() >= 30) {
					state.setHitDelay(projectile.getEndDelay()/30);
				}
				
				if (rangeWeapon.getItemId() == 12926) { // toxic blowpipe cheaphax
					projectile.setStartHeight(34);
					projectile.setSlope(0+dist);
				}

//				c.sendMess("arrow1:"+arrow.getTime()+":"+arrow.getSpeed()+":"+arrow.getLockon()+" SPATT:"+specialAttack);
				
				Graphic startGraphic = Graphic.create(ammo.getStartGraphics().getId(), ammo.getStartGraphics().getType());
				
				if (startGraphic.getType() == GraphicType.BOWARROWPULL && rangeWeapon.getWeaponType() == WeaponType.THROWN) {
					startGraphic = Graphic.create(startGraphic.getId(), GraphicType.HIGH);
				}
				
				if (state.getSpecialAttack() < 1) { // not special attack, check bolts specials
					if (rangeWeapon.getWeaponType() == WeaponType.CROSSBOW && state.getAmmo().getEffect() != null) { // crossbow effect
						if (state.getAmmo().getEffect().canFire(state, 10)) {
							state.setBoltEffect(state.getAmmo().getEffect());// this is only for if i wanted to make block the hit from applyDamage function
							state.getAmmo().getEffect().impact(state);
						}
					}
				}


				if (startGraphic != null && !attacker.getItems().hasEquippedWeapon(12926))
					state.setAttackerGfx(Graphic.create(startGraphic.getId(), startGraphic.getType())); //c.startGraphic(startGraphic);
			}
			
		}
		
		//new special code
		if (attacker.usingSpecial) {
			if (spec != null) {
				if (spec.canActivate(state)) {
					if (attacker.playerEquipment[PlayerConstants.playerWeapon] == 14484) { // claws nerf version 2
						if (Server.getTotalTicks() - attacker.clawTick < 5) {
							attacker.setAttackTimer(0);
							return null;
						} else {
							attacker.clawTick = Server.getTotalTicks();
						}
					}
					spec.init(state);
					state.setSpec(spec);
					if (state.getSpecEffect() == SpecialEffects.DRAGON_SPEAR) {
						if (defender.isPlayer())
							AttackPlayer.applySkull(state.getAttacker(), defender.toPlayer());
						return state;
					} else if (state.getSpecEffect() == SpecialEffects.DRAGON_2H) {
						MeleeExtras.dragon2hSpecial(attacker, state);
						attacker.usingSpecial = false;
						attacker.getItems().updateSpecialBar();
						attacker.getAchievement().specialAttack();
						return state;
					} else if (state.getSpecEffect() == SpecialEffects.DINHS_BULWARK) {
						MeleeExtras.handleBulwark(state);
						attacker.usingSpecial = false;
						attacker.getItems().updateSpecialBar();
						attacker.getAchievement().specialAttack();
						return state;
					}
				} else {
					attacker.sendMessage("You don't have the required special energy to use this attack.");
					attacker.usingSpecial = false;
					attacker.getItems().updateSpecialBar();
					state.setSpecialAttack(0);
					specialAttack = 0; //TODO: remove this once i move the ranged special attacks into SpecialAttack.java
				}
			}
		}

		return state;
	}

	public static void executeState(BattleState state) {
		Client attacker = state.getAttacker();
		Mob defender = state.getDefender();

		if (state.isMain()) {
			if (attacker.getSeasonalData().containsPowerUp(PowerUpData.BATTLEMAGE) && state.getCombatStyle() == CombatType.MELEE && !SeasonalData.isBeast(attacker) && Misc.randomBoolean(4)) {
				attacker.pulse(() -> {
					attacker.setPauseAnimReset(0);
					final SpellsData spellData;
					final int animation;
					if (Misc.randomBoolean()) {
						spellData = SpellsData.FIRE_SURGE;
						animation = 17214;
					} else {
						spellData = SpellsData.EARTH_SURGE;
						animation = 17215;
					}

					BattleState bmPerk = new BattleState(attacker, defender);

					bmPerk.setAppendExp(false);
					bmPerk.setSpellsData(spellData);
					bmPerk.setCombatStyle(CombatType.MAGIC);
	
					bmPerk.setAnimation(Animation.create(animation));
					bmPerk.setSoundId(spellData.getSoundId());
					bmPerk.setVictimGfx(spellData.getEndGfx());
					bmPerk.setCurrentHit(Misc.newRandom(attacker.getCombat().magicMaxHit(spellData)));
					bmPerk.setHitDelay(state.getHitDelay() +1);
	
					if (spellData.getProjectile() != null) {
						Projectile projectile = spellData.getProjectile().copy();
						projectile.setStartLoc(attacker.getCurrentLocation());
						projectile.setEndLoc(defender.getCurrentLocation());
						projectile.setLockon(defender.getProjectileLockon());
						projectile.setStartDelay(20);
						projectile.setEndDelay(30);
						projectile.setStartHeight(30);
						bmPerk.setProj(projectile);
					}
	
					executeState(bmPerk);
				});
			}
	
			if (attacker.getSeasonalData().containsPowerUp(PowerUpData.SPIKED_SHIELD) && ItemConstants.isShield(attacker.playerEquipment[PlayerConstants.playerShield]) && Misc.random(10) == 1) {
				attacker.pulse(() -> {
					attacker.sendMessageSpam("Your Spiked shield perk activates!");
	
					BattleState perk = new BattleState(attacker, defender);
					perk.setAppendExp(false);
					perk.setCombatStyle(CombatType.MELEE);
					perk.setCurrentHit(2);
					perk.setHitDelay(state.getHitDelay());
	
					executeState(perk);
				});
			}

			if (attacker.getSeasonalData().containsPowerUp(PowerUpData.WARRIOR) && state.getCombatStyle() == CombatType.MELEE && !ItemConstants.isShield(attacker.playerEquipment[PlayerConstants.playerShield]) && state.getCurrentHit() > 0 && (Misc.random(100) <= 20)) {
				final int finalDamage = state.getCurrentHit() * 50 / 100;
				if (finalDamage > 0) {
					attacker.setPauseAnimReset(0);

					attacker.pulse(() -> {
						BattleState perk = state.copy(attacker, defender);
						perk.setAppendExp(false);
						perk.setCurrentHit(finalDamage);
						executeState(perk);
					});
				}
			}

			if (attacker.getSeasonalData().containsPowerUp(PowerUpData.ADVANCED_RANGER) && state.getCombatStyle() == CombatType.RANGED && state.getAmmo() != null && state.getCurrentHit() > 0 && (Misc.random(100) <= 20)) {
				int newHit = state.getCurrentHit() * 50 / 100;
				if (newHit > 0) {
					Projectile projectile = state.getProj();
					
					Projectile arrow2 = projectile.copy();
					arrow2.setStartDelay(projectile.getStartDelay()-10);
					arrow2.setEndDelay(projectile.getEndDelay()-10);
					arrow2.setSlope(5);
	
					BattleState state2 = state.copy(attacker, defender);
					state2.setAppendExp(false);
					state2.setCurrentHit(newHit);
	
					if (state2.getDistanceToVictim() > 3) {
						state2.setHitDelay(state2.getHitDelay() + 1);
					} else if (state2.getDistanceToVictim() > 6) {
						state2.setHitDelay(state2.getHitDelay() + 2);
					}
	
					GlobalPackets.createProjectile(arrow2);
					AttackMob.applyDamage(state2);
				}
			}

			if (attacker.getSeasonalData().containsPowerUp(PowerUpData.CASTER) && state.getCombatStyle() == CombatType.MAGIC && state.getSpellsData() != null && state.getCurrentHit() > 0 && (Misc.random(100) <= 20)) {
				switch (state.getSpellsData()) {
					case WIND_BLAST:
					case WIND_BLAST_DUNG:
					case WIND_BOLT:
					case WIND_BOLT_DUNG:
					case WIND_STRIKE:
					case WIND_STRIKE_DUNG:
					case WIND_SURGE:
					case WIND_SURGE_DUNG:
					case WIND_WAVE:
					case WIND_WAVE_DUNG:
					case WATER_BLAST:
					case WATER_BLAST_DUNG:
					case WATER_BOLT:
					case WATER_BOLT_DUNG:
					case WATER_STRIKE:
					case WATER_STRIKE_DUNG:
					case WATER_SURGE:
					case WATER_SURGE_DUNG:
					case WATER_WAVE:
					case WATER_WAVE_DUNG:
					case EARTH_BLAST:
					case EARTH_BLAST_DUNG:
					case EARTH_BOLT:
					case EARTH_BOLT_DUNG:
					case EARTH_STRIKE:
					case EARTH_STRIKE_DUNG:
					case EARTH_SURGE:
					case EARTH_SURGE_DUNG:
					case EARTH_WAVE:
					case EARTH_WAVE_DUNG:
					case FIRE_BLAST:
					case FIRE_BLAST_DUNG:
					case FIRE_BOLT:
					case FIRE_BOLT_DUNG:
					case FIRE_STRIKE:
					case FIRE_STRIKE_DUNG:
					case FIRE_SURGE:
					case FIRE_SURGE_DUNG:
					case FIRE_WAVE:
					case FIRE_WAVE_DUNG:
						final int finalDamage = state.getCurrentHit() * 50 / 100;
						if (finalDamage > 0) {
							attacker.pulse(() -> {
								attacker.setPauseAnimReset(0);
		
								BattleState perk = state.copy(attacker, defender);
								perk.setAppendExp(false);
								perk.setCurrentHit(finalDamage);
								executeState(perk);
							});
						}
						break;
					default:
						break;
				}
			}

			if (attacker.clone != null) {
				if (state.getCombatStyle() == CombatType.MAGIC) {
					BattleState newState = state.copy(attacker, defender);
					newState.setHitDelay(state.getHitDelay() + 1);
					newState.setAnimation(null);
					newState.setAttackerGfx(null);
					newState.setVictimGfx(null);
					newState.setSoundId(-1);
					newState.setCurrentHit(Misc.newRandom(attacker.getCombat().magicMaxHit(newState.getSpellsData()) * 70 / 100));
					if (state.getSpellsData().getProjectile() != null) {
						Projectile projectile = state.getSpellsData().getProjectile().copy();
						projectile.setStartLoc(attacker.clone.getLocation());
						projectile.setEndLoc(defender.getCurrentLocation());
						projectile.setLockon(defender.getProjectileLockon());
						projectile.setStartDelay(state.getProj().getStartDelay());
						projectile.setEndDelay(state.getProj().getEndDelay() + 30);
						newState.setProj(projectile);
					}
	
					executeState(newState);
				} else {
					attacker.resetClone();
				}
			}
		}

		if (state.getSoundId() != -1) {
			attacker.getPacketSender().playSound(state.getSoundId());
		}

		// graphics visuals, on hit.
		if (state.getAttackerGfx() != null && state.getAttackerGfx().getId() > 0)
			attacker.startGraphic(state.getAttackerGfx().copy());
		if (state.getCombatStyle() != CombatType.MAGIC && state.getVictimGfx() != null && state.getVictimGfx().getId() > 0)
			defender.startGraphic(Graphic.create(state.getVictimGfx().getId(), Misc.serverToClientTick(state.getHitDelay()), state.getVictimGfx().getType()));

		Projectile projectile = state.getProj();
		if (projectile != null)
			GlobalPackets.createProjectile(projectile);

		if (state.getAnimation() != null && state.getAnimation().getId() > 0) {
			state.getAttacker().resetAnimations(); // using this instead of priority check for now.
			attacker.startAnimation(state.getAnimation());
			attacker.setPauseAnimReset(1);
		}
		
		boolean usingVitur = state.getSpellsData() == null && (state.getAttacker().playerEquipment[PlayerConstants.playerWeapon] == 30327 || state.getAttacker().playerEquipment[PlayerConstants.playerWeapon] == 30328);

		if (state.getSpellsData() != null && state.getSpellsData().isMultiSpell()) { // pi cheaphax shit
			MultiBarrage.appendMultiBarrage(state);
		} else if (state.getRangeWeapon() != null && state.getRangeWeapon().getWeaponType() == WeaponType.CHINCHOMPA) {
			RangeExtras.applyChinchompaMulti(state);
		} else if (usingVitur) {
			MeleeExtras.handleVitur(state);
		} else {
			AttackMob.applyDamage(state);
		}
		
		//frostbite dagger effect
		if (state.getCombatStyle() == CombatType.MELEE && state.getAttacker().playerEquipment[PlayerConstants.playerWeapon] == 17275 && Misc.random(99) < 25) {
			BattleState state2 = state.copy(attacker, defender);
			state2.setCombatStyle(CombatType.MAGIC);

			if (state2.getAttacker().playerEquipment[PlayerConstants.playerShield] == 17273) { // defender sets hit to 10
				state2.setCurrentHit(10);
			} else {
				state2.setCurrentHit(5);
			}

			state2.setSpecEffect(SpecialEffects.IGNORE_ARMOR_ONLY);

			AttackMob.applyDamage(state2);
		}

		// degrading stuff here
		Degrade.degradeWeapons(attacker);
		
		if (attacker.getSingleCastSpell() != null) {
			attacker.setSingleCastSpell(null);
			if (attacker.getAutoCastSpell() == null) {
				attacker.getCombat().resetPlayerAttack();
				attacker.getMovement().resetWalkingQueue();
				attacker.faceLocation(defender.getCurrentLocation().copyNew());
			}
		}
		
		if (defender.isPlayer() && !attacker.inMulti()) {
			attacker.setAttackingIndex(defender.getId());
			attacker.startInteractionResetTimer();
			attacker.setInteractionIndex(attacker.getAttackingIndex());
		}
		
		if (state.isMain()) {
			attacker.getSeasonalData().lastAttackedWith = state.getCombatStyle();
		}
	}

	public static boolean initHit(Client attacker, Mob defender) {
		if (!canHit(attacker, defender)) {
			return false;
		}

		BattleState state = makeState(attacker, defender);
		if (state == null) {
			return false;
		}

		state.setMain(true);
		executeState(state);

		return true;
	}
	
	
	public static void applyDamage(final BattleState state) {
		
		if (state.getBoltEffect() == BoltEffect.RUBY) // ruby damage is in BoltEffect.RUBY
			return;
		
		boolean veracEffect = state.getAttacker().getPA().fullVeracsEffect();
		
		if (veracEffect) {
			state.setCurrentHit(state.getCurrentHit()+1);
			state.getDefender().startGraphic(VERACS_EFFECT);
		}
		
		boolean ignoreArmour = veracEffect;
		
		
		if (state.getSpecEffect() == SpecialEffects.IGNORE_ARMOR_ONLY) {
			ignoreArmour = true;
		}

		AccuracyType accuracyType;
		boolean successfulHit;
		boolean crit = false;
		boolean procRangerPower = false;

		if (ignoreArmour) {
			accuracyType = AccuracyType.HIT;
		} else {
			accuracyType = CombatAccuracy.successfulHit(state);
		}

		successfulHit = accuracyType.isSuccessfulHit();
		state.setSuccessfulHit(successfulHit);

		if (!successfulHit) {
			switch (state.getSpecEffect()) {
				case DARK_BOW1:
					state.setCurrentHit(5);
					break;
				case DARK_BOW2:
					state.setCurrentHit(8);
					break;
				default:
					state.setCurrentHit(0);
					break;
			}

			if (accuracyType == AccuracyType.BLOCKED) {
				CombatAccuracy.applyBlockedDamage(state.getAttacker());
			}
		} else {
			CritRollType rolledCritType = CombatCrit.rolledCrit(state.getAttacker());
			if (rolledCritType == CritRollType.NO_CRIT && state.getCombatStyle() == CombatType.MAGIC) {
				if (state.getAttacker().getSeasonalData().nextSpellCrit) {
					rolledCritType = CritRollType.CRIT;
					state.getAttacker().getSeasonalData().nextSpellCrit = false;
				} else if (state.getAttacker().getSeasonalData().nextWindSpellCrit && SpellsData.WIND_SPELLS.contains(state.getSpellsData())) {
					rolledCritType = CritRollType.CRIT;
					state.getAttacker().getSeasonalData().nextWindSpellCrit = false;
				}
			}

			if (rolledCritType != CritRollType.NO_CRIT) {
				crit = true;
				state.setCurrentHit(CombatCrit.applyCritBoost(state.getAttacker(), state.getCurrentHit()));
				if (rolledCritType == CritRollType.CRIT && state.getAttacker().getSeasonalData().containsPowerUp(PowerUpData.CRIT_SPRINTER)) {
					state.getAttacker().getSeasonalData().procCritSprinter = true;
				}
			}

			int strVal = state.getAttacker().getSeasonalData().getPowerUpAmount(PowerUpData.STRENGTH);
			if (strVal > 0) {
				state.setCurrentHit(state.getCurrentHit() + state.getCurrentHit() * (strVal * 5) / 100);
			}

			if (state.getAttacker().getSeasonalData().containsPowerUp(PowerUpData.ONE_HANDER) && state.getCombatStyle() == CombatType.MELEE && !ItemConstants.is2handed(state.getAttacker().playerEquipment[PlayerConstants.playerWeapon])) {
				state.setCurrentHit(state.getCurrentHit() + state.getCurrentHit() * 7 / 100);
			}

			if (state.getAttacker().getSeasonalData().containsPowerUp(PowerUpData.HYBRID)) {
				CombatType lastStyle = state.getAttacker().getSeasonalData().lastAttackedWith;
				if (lastStyle != null && lastStyle != state.getCombatStyle()) {
					state.setCurrentHit(state.getCurrentHit() + state.getCurrentHit() * 15 / 100);
				}
			}

			if (SeasonalData.isBeast(state.getAttacker())) {
				state.setCurrentHit(state.getCurrentHit() + state.getCurrentHit() * 10 / 100);
			}

			int heavyWeightVal = state.getAttacker().getSeasonalData().getPowerUpAmount(PowerUpData.HEAVYWEIGHT);
			if (heavyWeightVal > 0 && state.getDefender().getSkills().getLifepoints() >= 250 && Misc.randomBoolean(5)) {
				state.setCurrentHit((heavyWeightVal * 15) + state.getCurrentHit());
				state.getAttacker().sendMessageSpam("Your Heavyweight power up allows you to hit through the opponents hp.");
			}

			if (state.getAttacker().getSeasonalData().containsPowerUp(PowerUpData.BEAR) && state.getCombatStyle() == CombatType.RANGED && Misc.randomBoolean(5)) {
				state.setCurrentHit(state.getCurrentHit() + state.getCurrentHit() * 50 / 100);
				procRangerPower = true;
			}
		}

		//SPECIAL ATTACK EFFECTS ON EXP DROP
		if (state.getSpec() != null)
			state.getSpec().activateExpDropEffect(state);
		//cheaphax for now under here
		if (state.getSpecEffect() != SpecialEffects.NONE) {
			if (state.getSpecEffect() == SpecialEffects.IGNORE_ARMOR_ONLY && state.getAttacker().getItems().hasEquippedWeapon(11061)) { // ancient mace
				if (state.getCurrentHit() > 0 && state.getDefender().isPlayer()) {
					int prayerLeech = state.getCurrentHit() * 2;

					if (prayerLeech > state.getDefender().toPlayer().getSkills().getPrayerPoints()) {
						prayerLeech = state.getDefender().toPlayer().getSkills().getPrayerPoints();
					}

					state.getDefender().getSkills().decrementPrayerPoints(prayerLeech);
					state.getAttacker().getSkills().incrementPrayerPoints(prayerLeech);
				}
			}
			if (state.getSpecEffect() == SpecialEffects.ABYSSAL_TENTACLE) {
				if (successfulHit)
					state.getDefender().rootEntity(10);
			} else if (state.getSpecEffect() == SpecialEffects.TOXIC_BLOWPIPE) {
				if (state.getCurrentHit() > 0)
					state.getAttacker().getSkills().heal(state.getCurrentHit()/2);
			} else if (state.getSpecEffect() == SpecialEffects.ZAMORAK_GODSWORD) {
				if (successfulHit) {
					if (state.getDefender().applyFreeze(33)) {
						state.getAttacker().sendMessage("You freeze your enemy.");
						if (state.getDefender().isPlayer()) {
							state.getDefender().toPlayer().sendMessage("You have been frozen.");
							state.getDefender().toPlayer().getMovement().resetWalkingQueue();
							state.getDefender().toPlayer().frozenBy = state.getAttacker().getId();
						}
					}
					state.getDefender().startGraphic(Graphic.create(2104, Misc.serverToClientTick(state.getHitDelay()), GraphicType.LOW));
				} else {
					state.getDefender().startGraphic(Graphic.create(2105, Misc.serverToClientTick(state.getHitDelay()), GraphicType.LOW));
				}
				
			} else if (state.getSpecEffect() == SpecialEffects.BARRELCHEST_ANCHOR) {
				if (successfulHit) {
					int ran = Misc.newRandom(3);
					if (ran == 2) {
						ran = 4;
					}
					if (ran == 3) {
						ran = 6;
					}
					
					state.getDefender().getSkills().drainLevel(ran, 0.10, 1);

				}
			}
		}

		int ammoId = 0;
		
		if (state.getCombatStyle() != CombatType.MAGIC) {
			if (state.getAmmo() != null) { // TODO: add weapon poison ammo!!!
				int damage = state.getAmmo().getPoisonDamage();
				if (damage > 0 && Misc.newRandom(7) == 7) {
					int poisonDamage = damage / 8;
					if (poisonDamage > 0) {
						state.getDefender().getDotManager().applyPoison(poisonDamage, state.getAttacker());
					}
				}
			} else {
				int weaponId = state.getAttacker().playerEquipment[PlayerConstants.playerWeapon];
				if (weaponId > 0) {
					String itemName = ItemConstants.getItemName(weaponId);
					int damage = 0;

					if ((itemName.contains("(p++)")
							|| state.getAttacker().playerEquipment[PlayerConstants.playerWeapon] == 15444)
							&& Misc.newRandom(7) == 7) {
						damage = 6;
					} else if (weaponId == 12006 && Misc.newRandom(3) == 1) {
						damage = 4;
					} else if (itemName.contains("(p+)") && Misc.newRandom(7) == 7) {
						damage = 4;
					} else if (itemName.contains("(p)") && Misc.newRandom(7) == 7) {
						damage = 2;
					}

					if (damage > 0) {
						state.getDefender().getDotManager().applyPoison(damage, state.getAttacker());
					}
				}
			}

			//AMMO SHIT HERE
			if ((state.getAttacker().inLMSArenaCoords() || !state.getAttacker().inMinigame())) {
				if (state.getRangeWeapon() != null) {
					if (state.dropAmmo()) {
						if (state.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerWeapon) {
							ammoId = state.getAttacker().getItems().deleteEquipedWeaponSlot();
						} else if (state.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerArrows) {
							ammoId = state.getAttacker().getItems().deleteArrowSlotAmmo();
						}
					} else if (state.getRangeWeapon().getWeaponType() != WeaponType.DEGRADING) {
						if (state.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerWeapon) {
							state.getAttacker().getItems().deleteEquipedWeaponSlot();
						} else if (state.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerArrows) {
							state.getAttacker().getItems().deleteArrowSlotAmmo();
						}
					}
				}

			}

			//END OF AMMO SHIT
			if (state.getAttacker().getItems().playerHasEquippedWeapon(BlowPipeCharges.BLOWPIPE_FULL) && Misc.newRandom(99) < 25) {
				state.getDefender().getDotManager().applyVenom();
			}

			if (ammoId == 13879 && state.getAttacker().playerEquipmentN[PlayerConstants.playerWeapon] == 0 && !state.getAttacker().getItems().playerHasItem(13879)) { // q7a effect
				state.getAttacker().setAttackTimer(1); // cheaphaxing last javelin resetting attack timer
				state.getAttacker().sendMessage("You have ran out of ammo..");
			}
		}

		if (state.getSpellsData() != null && state.getAttacker().getItems().playerHasEquippedWeapon(30011) && Misc.newRandom(99) < 25) {
			state.getDefender().getDotManager().applyVenom();
		}

		//spirit shield effect
		if (!veracEffect && state.getCurrentHit() > 0 && state.getDefender().isPlayer()) {
			state.setCurrentHit(state.getDefender().toPlayer().applyDamageReduction(state.getCurrentHit(), state.getCombatStyle()));
		}
		
		//spell freezing and shit from PI shit code
		if (successfulHit && state.getSpellsData() != null && state.getCombatStyle() == CombatType.MAGIC) {
			int freezeDelay = state.getSpellsData().getFreezeTime();
			if (freezeDelay > 0 && state.getDefender().applyFreeze(freezeDelay)) {
				if (state.getDefender().isPlayer()) {
					state.getDefender().toPlayer().sendMessage("You have been frozen.");
					state.getDefender().toPlayer().getMovement().resetWalkingQueue();
					state.getDefender().toPlayer().frozenBy = state.getAttacker().getId();
				}
			}

			if (state.getSpellsData() == SpellsData.ICE_BARRAGE && state.getDefender().isPlayer()) {
				state.getAttacker().increaseProgress(TaskType.FIGHT_OPPONENT, TaskConstants.FIGHT_WITH_ICE_BARRAGE);
			}
		}

		if (state.getSpellsData() != null && state.getCombatStyle() == CombatType.MAGIC) {
			handleSpellEffects(state);
		}

		if (state.getDefender().isPlayer()) {
			Client c2 = (Client) state.getDefender().toPlayer();
			if (c2.spectatorMode) {
				return;
			}

			AttackPlayer.applySkull(state.getAttacker(), c2);

			if (c2.getSpiritShieldHits() > 0) { // reducing spirit shield hits onhit
				c2.setSpiritShieldHits(c2.getSpiritShieldHits() - 1);
			}
			
			//staff of light effect
			if (!veracEffect && state.getCombatStyle() == CombatType.MELEE && c2.usingSOLSpec && state.getCurrentHit() > 0) {
				state.getDefender().startGraphic(Graphic.create(2320, GraphicType.LOW));
				state.setCurrentHit(state.getCurrentHit() / 2);
			}

			//SPECIAL ATTACKS ON EXP DROPS THAT APPLY ONLY TO PLAYERS
			if (successfulHit && state.getSpecEffect() != SpecialEffects.NONE) {
				if (state.getSpecEffect() == SpecialEffects.ZARYTE_BOW) {
					if (state.getCurrentHit() > 0 && Misc.newRandom(10) == 7) {
						state.getAttacker().sendMessage("You smite your opponent");
						c2.prayerDrainCounter += 1000;
						c2.startGraphic(Graphic.create(437, GraphicType.LOW));
						c2.sendMessage("Your opponent drains your prayer completely.");
					} else if (state.getCurrentHit() > 0 && Misc.newRandom(10) <= 8) {
						state.getAttacker().sendMessage("You smite your opponent");
						CombatPrayer.drainPrayerByHit(c2, state.getCurrentHit()); //c2.getSkills().decrementPrayerPoints(state.getCurrentHit());
						c2.startGraphic(Graphic.create(437, GraphicType.LOW));
						c2.sendMessage("Your opponent drains your prayer.");
					} else {
						state.getAttacker().sendMessage("Your special attack failed.");
					}
				} else if (state.getSpecEffect() == SpecialEffects.BRACKISH_BLADE) {
					if (state.getCurrentHit() > 0) {
						state.getAttacker().sendMessage("The blade boosts your powers");
						double bonus = state.getCurrentHit() / 10;
						
						state.getAttacker().getSkills().updateLevel(Skills.DEFENSE, (int)bonus, state.getAttacker().wildLevel > 0 ? RSConstants.MAX_WILD_DEF : RSConstants.MAX_DEF);
						state.getAttacker().getSkills().updateLevel(Skills.ATTACK, (int)bonus, state.getAttacker().wildLevel > 0 ? RSConstants.MAX_WILD_ATT : RSConstants.MAX_ATT);
						state.getAttacker().getSkills().updateLevel(Skills.STRENGTH, (int)bonus, state.getAttacker().wildLevel > 0 ? RSConstants.MAX_WILD_STR : RSConstants.MAX_STR);
					}
				} else if (state.getSpecEffect() == SpecialEffects.DRAGON_SCIMITAR) {
					disablePrayer(c2, 8);
				}
			}

			c2.putInCombatWithPlayer(state.getAttacker().getId());

			c2.getDamageMap().incrementTotalDamage(state.getAttacker().mySQLIndex, state.getCurrentHit());
		}

		switch (state.getSpecEffect()) {
		case BANDOS_GODSWORD:
			if (state.getCurrentHit() > 0) {
				Mob opponent = state.getDefender();
				Player other = opponent.isPlayer() ? opponent.toPlayerFast() : null;
				if (other != null)
					other.sendMessage("You feel weak.");
				if (opponent.getSkills().getLevel(Skills.DEFENSE) > 1) {
					opponent.getSkills().updateLevel(Skills.DEFENSE, state.getCurrentHit(), 1);
				} else if (opponent.getSkills().getLevel(Skills.STRENGTH) > 1) {
					opponent.getSkills().updateLevel(Skills.STRENGTH, state.getCurrentHit(), 1);
				} else if (other != null && opponent.getSkills().getPrayerPoints() > 0) {
					CombatPrayer.drainPrayerByHit(other, state.getCurrentHit()); //opponent.getSkills().decrementPrayerPoints(state.getCurrentHit());
				} else if (opponent.getSkills().getLevel(Skills.ATTACK) > 1) {
					opponent.getSkills().updateLevel(Skills.ATTACK, state.getCurrentHit(), 1);
				} else if (opponent.getSkills().getLevel(Skills.MAGIC) > 1) {
					opponent.getSkills().updateLevel(Skills.MAGIC, state.getCurrentHit(), 1);
				} else if (opponent.getSkills().getLevel(Skills.RANGED) > 1) {
					opponent.getSkills().updateLevel(Skills.RANGED, state.getCurrentHit(), 1);
				}
			}
			break;
		default:
			break;
		}

		boolean guthansEffect = false;
		if (state.getAttacker().getPA().fullGuthans()) {
			if (Misc.newRandom(99) < 25) {
				guthansEffect = true;
			}
		}

		//CLAWS CHEAPHAX
		int hit2 = 0, hit3 = 0, hit4 = 0;
		if (state.getSpecEffect() == SpecialEffects.DRAGON_CLAWS) { // previous formula, kinda op
			int hit1 = state.getCurrentHit();
			hit2 = hit1 > 0 ? hit1 / 2 : CombatAccuracy.successfulHit(state).isSuccessfulHit() ? Misc.random2(state.getAttacker().getCombat().calculateMeleeMaxHit(1.0)) : 0;
			hit3 = hit2 > 0 ? hit2 / 2 : CombatAccuracy.successfulHit(state).isSuccessfulHit() ? Misc.random2(state.getAttacker().getCombat().calculateMeleeMaxHit(1.0)) : 0;
			hit4 = hit3 > 0 ? hit3 + 1 : CombatAccuracy.successfulHit(state).isSuccessfulHit() ? Misc.random2(state.getAttacker().getCombat().calculateMeleeMaxHit(1.0)) : 1;
			if (state.getDefender().isPlayer()) {
				hit2 = state.getDefender().toPlayer().applyDamageReduction(hit2, state.getCombatStyle());
				hit3 = state.getDefender().toPlayer().applyDamageReduction(hit3, state.getCombatStyle());
				hit4 = state.getDefender().toPlayer().applyDamageReduction(hit4, state.getCombatStyle());

				if (state.getDefender().toPlayer().usingSOLSpec) {
					hit2 /= 2;
					hit3 /= 2;
					hit4 /= 2;
				}
			}
		}

		boolean deflect = false;
		int retardedDeflectCheaphax = 0;

		if ((state.getCurrentHit() > 0 || hit2 > 0 || hit3 > 0 || hit4 > 0) && !ignoreArmour) { // protect prayers
			if ((state.getDefender().usingProtectPrayer(true, Curses.DEFLECT_MELEE.ordinal()) && state.getCombatStyle() == CombatType.MELEE)
					|| (state.getDefender().usingProtectPrayer(true, Curses.DEFLECT_MISSILES.ordinal()) && state.getCombatStyle() == CombatType.RANGED)
					|| (state.getDefender().usingProtectPrayer(true, Curses.DEFLECT_MAGIC.ordinal()) && state.getCombatStyle() == CombatType.MAGIC)) {
				deflect = true;
				retardedDeflectCheaphax = state.getCurrentHit();
			}
			if ((state.getCombatStyle() == CombatType.MELEE && (state.getDefender().usingProtectPrayer(true, Curses.DEFLECT_MELEE.ordinal()) 
															|| state.getDefender().usingProtectPrayer(false, Prayers.PROTECT_FROM_MELEE.ordinal())))
					|| (state.getCombatStyle() == CombatType.RANGED && (state.getDefender().usingProtectPrayer(true, Curses.DEFLECT_MISSILES.ordinal())
															|| state.getDefender().usingProtectPrayer(false, Prayers.PROTECT_FROM_MISSILES.ordinal())))
					|| (state.getCombatStyle() == CombatType.MAGIC && (state.getDefender().usingProtectPrayer(true, Curses.DEFLECT_MAGIC.ordinal())
															|| state.getDefender().usingProtectPrayer(false, Prayers.PROTECT_FROM_MAGIC.ordinal())))) {
				if (state.getCurrentHit() > 0) {
					state.setCurrentHit(state.getDefender().protectPrayerDamageReduction(state.getCurrentHit()));
				}

				if (state.getSpecialAttack() == 14484) {
					hit2 = hit2 * 60 / 100;
					hit3 = hit3 * 60 / 100;
					hit4 = hit4 * 60 / 100;
				}

			}

		}

		if (state.getCurrentHit() > 0 && guthansEffect) {
			state.getAttacker().getSkills().heal(state.getCurrentHit());
			state.getDefender().startGraphic(Graphic.create(398, GraphicType.LOW));
		}
		
		if (state.getCombatStyle() == CombatType.MAGIC && successfulHit && state.getCurrentHit() > 69) {
			state.getAttacker().getAchievement().hit70Magic();
		}
		
//		if (state.getCurrentHit() > state.getDefender().getSkills().getMaximumLifepoints()) { //if (state.getCurrentHit() > 0 && state.getDefender().getSkills().getLifepoints() - state.getCurrentHit() < 0) {
//			state.setCurrentHit(state.getDefender().getSkills().getMaximumLifepoints());
//		}

		if (!state.getAttacker().isIronMan() || state.getDefender().isNpc()) {

			state.getAttacker().getPA().addExpFromCombat(state.getCurrentHit(), state, state.getSpellsData());

			// claws cheaphax for exp
			if (hit2 > 0) {
				state.getAttacker().getPA().addExpFromCombat(hit2, state, null);
			}
			if (hit3 > 0) {
				state.getAttacker().getPA().addExpFromCombat(hit3, state, null);
			}
			if (hit4 > 0) {
				state.getAttacker().getPA().addExpFromCombat(hit4, state, null);
			}

		} else {
			//ironman exp drops
			state.getAttacker().getPacketSender().setSkillLevel(0, state.getAttacker().getSkills().getLevel(0),
					state.getAttacker().getSkills().getExperience(0) + state.getCurrentHit());
			
			if (hit2 > 0)
				state.getAttacker().getPacketSender().setSkillLevel(0, state.getAttacker().getSkills().getLevel(0),
						state.getAttacker().getSkills().getExperience(0) + hit2);
			
			if (hit3 > 0)
				state.getAttacker().getPacketSender().setSkillLevel(0, state.getAttacker().getSkills().getLevel(0),
						state.getAttacker().getSkills().getExperience(0) + hit3);
			
			if (hit4 > 0)
				state.getAttacker().getPacketSender().setSkillLevel(0, state.getAttacker().getSkills().getLevel(0),
						state.getAttacker().getSkills().getExperience(0) + hit4);
			
		}

		// used for PK bots which are not used.
		state.getAttacker().lastDamageDealt = state.getCurrentHit();
		state.getAttacker().lastHitWasSpec = state.getSpecialAttack() > 0;
		state.getAttacker().lastDamageType = 0;
		
		final int ammoFinal = ammoId;


		final boolean deflectDmg = deflect;
		final int retardedDeflectCheaphaxFinal = retardedDeflectCheaphax;

		// final int hit1C = hit1;
		final int hit2C = hit2;
		final int hit3C = hit3;
		final int hit4C = hit4;
		
		final boolean successfulHitFinal = successfulHit;
		
		if (state.getCombatStyle() == CombatType.MAGIC) {
			if (!successfulHit && state.getSpellsData() != null) { // splashing for magic
				state.getDefender().startGraphic(Graphic.create(85, Misc.serverToClientTick(state.getHitDelay()), GraphicType.HIGH));
			} else {
				if (state.getVictimGfx() != null) {
					state.getDefender().startGraphic(Graphic.create(state.getVictimGfx().getId(), Misc.serverToClientTick(state.getHitDelay()), state.getVictimGfx().getType()));
				}
			}
		}

		if (state.getAttacker().getSeasonalData().containsPowerUp(PowerUpData.THE_BEAST) && state.getCombatStyle() == CombatType.MELEE && state.getAttacker().curseActive[Curses.TURMOIL.ordinal()] && !SeasonalData.isBeast(state.getAttacker())) {
			state.getAttacker().pulse(() -> {
				state.getAttacker().resetAnimations();
				state.getAttacker().setAttackTimer(6);
				state.getAttacker().pulse(() -> {
					SeasonalData.transformBeastPower(state.getAttacker());
				});
			});
		}

		final boolean finalProcRangerPower = procRangerPower;
		final boolean finalCrit = crit;
		boolean smiteActive = state.getAttacker().prayerActive[Prayers.SMITE.ordinal()];
		boolean soulsplitActive = state.getAttacker().curseActive[Curses.SOUL_SPLIT.ordinal()];
		CycleEventHandler.getSingleton().addEvent((state.getAttacker()/*state.getDefender().isPlayer() ? (Client)state.getDefender().toPlayer() : state.getAttacker()*/), true, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				try {
					
					if (state.getDefender() == null || !state.getDefender().isActive) {
						container.stop();
						return;
					}

					if (state.getDefender().isPlayer() && state.getDefender().toPlayer().duelStatus == 6) {
						container.stop();
						return;
					}

					if (state.getDefender().isPlayer() && state.getDefender().toPlayer().teleTimer > 1) {
						//if (container.getTick() > 1) {
						//	container.setTick(1);
						//}
						container.stop();
						return;
					}

					onHitRules(state, finalProcRangerPower); // moved this up just in case we want to change damage amount

					if (!successfulHitFinal && state.getCombatStyle() == CombatType.MAGIC && state.getSpellsData() != null) {
					} else {
						state.getDefender().dealDamage(new Hit(state.getCurrentHit(), finalCrit ? 1 : 0, state.getCombatStyle().ordinal(), state.getAttacker()));
					}

					if (ammoFinal > 0)
						state.getAttacker().getItems().dropAmmo(ammoFinal, state.getDefender().getCurrentLocation());

//					if (state.getDefender().getSkills().getLifepoints() - damageNew < 0) {
//						damageNew = state.getDefender().getSkills().getLifepoints();
//					}

					if (state.getDefender().isPlayer()) {
						Client c2 = (Client)state.getDefender().toPlayer();

						if (c2.isInLmsArena() && c2.interfaceIdOpenMainScreen != 0) {
							c2.getPacketSender().closeInterfaces();
						}

						// apply special effects on pvp on hitmark
						if (successfulHitFinal) {
							 if (state.getCurrentHit() > 0 && state.getSpecEffect() == SpecialEffects.MORRIGAN_JAVELIN) {
								 c2.getPA().applyBleed(state.getCurrentHit(), 5, 2);
							 }
						}
						
						// checking if attacker is active and defender is player
						if (state.getAttacker() != null && state.getAttacker().isActive) {
							if (deflectDmg) {
								c2.curses().deflect(state.getAttacker(), retardedDeflectCheaphaxFinal, state.getCombatStyle().ordinal());
							}

							state.getAttacker().getCombat().applySmite(c2, smiteActive, state.getCurrentHit(), hit2C, hit3C, hit4C);
							if (state.getCurrentHit() > 0) {
								if (c2.vengOn) {
									state.getAttacker().getCombat().appendVengeance(c2, state.getCurrentHit());
								}

								state.getAttacker().getCombat().applyRecoil(state.getCurrentHit(), c2);
							}
						}
						Degrade.degradeArmor(c2);
					} else if (state.getDefender().isNpc()) {
						if (state.getCurrentHit() > 0 && successfulHitFinal) {
							state.getDefender().toNpc().addInfliction(state.getAttacker().mySQLIndex, state.getCurrentHit());
							state.getDefender().toNpc().lastDamageTaken = System.currentTimeMillis();
							// pest control npcs hp updating
							if (state.getDefender().toNpc().isPestControlNPC()) {
								PestControl.updateNpcHP(state.getDefender().toNpc());
								state.getAttacker().pcParticipation += state.getCurrentHit() / 3;
								state.getAttacker().getPacketSender().sendFrame126("" + state.getAttacker().pcParticipation, 21116);
							}
						}
					}

					//ANYTHING REQUIRING CHECKING OF ATTACKER BEING NOT NULL AND ACTIVE
					if (state.getAttacker() != null && state.getAttacker().isActive) {
						state.getAttacker().curses().SoulSplitOnMob(state.getDefender(), soulsplitActive, state.getCurrentHit(), hit2C, hit3C, hit4C);
						if (state.getCombatStyle() == CombatType.MELEE && state.getCurrentHit() >= 90) {
							state.getAttacker().getAchievement().hit90Melee();
						}
					}

					// CHECK IF ATTACKER IS NOT NULL AND ACTIVE FOR SPECIAL ATTACK EFFECTS ON DAMAGE TICK
					if (state.getAttacker() != null && !state.getAttacker().forceDisconnect) {

						//dragon claws
						if (state.getSpecEffect() == SpecialEffects.DRAGON_CLAWS) {
							int hit2C1 = hit2C;

							state.getDefender().dealDamage(new Hit(hit2C1, 0, 0, state.getAttacker()));
							if (hit2C1 > 0) {
								if (state.getDefender().isPlayer()) {
									Client c3 = (Client)state.getDefender().toPlayer();
									state.getAttacker().getCombat().applyRecoil(hit2C1, c3);
									if (c3.vengOn) {
										state.getAttacker().getCombat().appendVengeance(c3, hit2C1);
									}
								}
							}
							CycleEventHandler.getSingleton().addEvent(state.getAttacker(), true,
									new CycleEvent() {

								@Override
								public void execute(
										CycleEventContainer container) {
									if (state.getAttacker() == null || state.getDefender() == null
											|| state.getAttacker().forceDisconnect
											|| state.getDefender().isDead()
											|| state.getDefender().getSkills().getLifepoints() <= 0) {
										container.stop();
										return;
									}

									state.getDefender().dealDamage(new Hit(hit3C, 0, 0, state.getAttacker()));

									state.getDefender().dealDamage(new Hit(hit4C, 0, 0, state.getAttacker()));

									if ((hit4C > 0 || hit3C > 0) && state.getDefender().isPlayer()) {
										Client c3 = (Client) state.getDefender().toPlayer();
										state.getAttacker().getCombat().applyRecoil(hit4C+hit3C, c3);
										if (c3.vengOn) {
											state.getAttacker().getCombat().appendVengeance(c3, hit4C+hit3C);
										}
									}
									container.stop();
								}

								@Override
								public void stop() {

								}

							}, 1);
						}
					}

					if (successfulHitFinal && !state.getDefender().isLockActions() && (state.getDefender().getAttackTimer() == 3 || state.getDefender().getAttackTimer() == 0)) { // block animation
						if (!deflectDmg) {
							if (!state.getDefender().isPlayer() || (state.getDefender().isPlayer()))
								if (state.getDefender().isPlayer())
									state.getDefender().startAnimation(MeleeData.getBlockEmoteNew(state.getDefender().toPlayer()));
								else if (state.getDefender().isNpc()) {
									int blockEmote = NPCAnimations.getDefend(state.getDefender().toNpc());
									if (blockEmote != -1)
										state.getDefender().startAnimation(blockEmote);
								}
						}
					}

					if (state.getDefender().isPlayer() && state.getAttacker().playerEquipment[PlayerConstants.playerWeapon] != -1) {
						state.getAttacker().increaseProgress(TaskType.FIGHT_OPPONENT, state.getAttacker().playerEquipment[PlayerConstants.playerWeapon]);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}

				container.stop();
			}

			@Override
			public void stop() {

			}

		}, state.getHitDelay());

	}

	public static void disablePrayer(Player c, int ticks) {
		if (c.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
				|| c.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]
						|| c.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]) {

			c.setHeadIcon(-1);
			CombatPrayer.deactivePrayer(c, Prayers.PROTECT_FROM_MAGIC);
			CombatPrayer.deactivePrayer(c, Prayers.PROTECT_FROM_MISSILES);
			CombatPrayer.deactivePrayer(c, Prayers.PROTECT_FROM_MELEE);
		}
		if (c.curseActive[Curses.DEFLECT_MAGIC.ordinal()]
				|| c.curseActive[Curses.DEFLECT_MISSILES.ordinal()]
						|| c.curseActive[Curses.DEFLECT_MELEE.ordinal()]) {
			c.setHeadIcon(-1);
			c.curses().deactivate(Curses.DEFLECT_MAGIC.ordinal());
			c.curses().deactivate(Curses.DEFLECT_MISSILES.ordinal());
			c.curses().deactivate(Curses.DEFLECT_MELEE.ordinal());
		}
		c.sendMessage("You have been injured!");
		c.getProtectionPrayerDisabledTimer().startTimer(ticks);
	}

	private static boolean checkMiniGameRulesForCombatStyles(Client c, CombatType style) {

		if (c.clanWarRule[CWRules.NO_RANGED] && style == CombatType.RANGED) {
			c.sendMessage("You are not allowed to use ranged during this war!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return false;
		}

		if (c.clanWarRule[CWRules.NO_MELEE] && style == CombatType.MELEE) {
			c.sendMessage("You are not allowed to use melee during this war!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return false;
		}

		if (c.clanWarRule[CWRules.NO_MAGIC] && style == CombatType.MAGIC) {
			c.sendMessage("You are not allowed to use magic during this war!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return false;
		}

		if (c.duelRule[DuelRules.NO_RANGE] && style == CombatType.RANGED) {
			c.sendMessage("Range has been disabled in this duel!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return false;
		}
		if (c.duelRule[DuelRules.NO_MELEE] && style == CombatType.MELEE) {
			c.sendMessage("Melee has been disabled in this duel!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return false;
		}
		if (c.duelRule[DuelRules.NO_MAGIC] && style == CombatType.MAGIC) {
			c.sendMessage("Magic has been disabled in this duel!");
			c.getCombat().resetPlayerAttack();
			c.getMovement().resetWalkingQueue();
			return false;
		}
		
		return true;
	}

	private static void handleSpellEffects(BattleState state) {
		SpellsData spell = state.getSpellsData();
		Client c = state.getAttacker();
		int damage = state.getCurrentHit();
		boolean successfulHit = state.isSuccessfulHit();
		Mob defender = state.getDefender();
		
		if (c.getSeasonalData().containsPowerUp(PowerUpData.NECROMANCER) && Misc.randomBoolean(5) && c.getSeasonalData().spawned == null) {
			Location spawnLocation = RegionClip.findLocationAround(defender);
			if (spawnLocation != null) {
				c.pulse(() -> {
					c.setPauseAnimReset(0);
					c.startAnimation(17217);
					c.pulse(() -> {
						//TODO make him scale of magic lvl?
						NecromancerSkeleton npc = (NecromancerSkeleton) (c.getSeasonalData().spawned = NPCHandler.spawnNpc(NecromancerSkeleton.ID, spawnLocation));
						npc.spawn(c, defender);
					});
				});
			}
		}

		switch (spell) {
			case FIRE_BLAST:
			case FIRE_BLAST_DUNG:
			case FIRE_BOLT:
			case FIRE_BOLT_DUNG:
			case FIRE_STRIKE:
			case FIRE_STRIKE_DUNG:
			case FIRE_SURGE:
			case FIRE_SURGE_DUNG:
			case FIRE_WAVE:
			case FIRE_WAVE_DUNG:
				int amount = c.getSeasonalData().getPowerUpAmount(PowerUpData.IGNITE);
				if (amount > 0 && Misc.randomBoolean(5)) {
					int igniteDamage = amount * 2;
					defender.getDotManager().applyBurn(igniteDamage);
				}
				break;
			case EARTH_BLAST:
			case EARTH_BLAST_DUNG:
			case EARTH_BOLT:
			case EARTH_BOLT_DUNG:
			case EARTH_STRIKE:
			case EARTH_STRIKE_DUNG:
			case EARTH_SURGE:
			case EARTH_SURGE_DUNG:
			case EARTH_WAVE:
			case EARTH_WAVE_DUNG:
				if (c.getSeasonalData().containsPowerUp(PowerUpData.EARTH_ROOT) && Misc.randomBoolean(4)) {
					defender.applyFreeze(Misc.secondsToTicks(5));
					if (defender.isPlayer()) {
						defender.toPlayerFast().sendMessage("You have been rooted!");
					}
				}
				break;
			default:
				break;
		}

		switch (spell) {
			case WIND_SURGE:
			case WIND_SURGE_DUNG:
			case WATER_SURGE:
			case WATER_SURGE_DUNG:
			case EARTH_SURGE:
			case EARTH_SURGE_DUNG:
			case FIRE_SURGE:
			case FIRE_SURGE_DUNG:
				if (c.getSeasonalData().containsPowerUp(PowerUpData.SURGE)) {
					state.setCurrentHit(damage + damage * 5 / 100);
				}
				break;
			case BLOOD_BARRAGE:
			case BLOOD_BLITZ:
			case BLOOD_BURST:
			case BLOOD_RUSH:
				if (successfulHit) {
					int heal = damage / 4;
					c.getSkills().heal(heal);
				}
				break;
			case MIASMIC_RUSH:
				if (successfulHit) {
					defender.startMiasmicEffect(12);
				}
				break;
			case MIASMIC_BURST:
				if (successfulHit) {
					defender.startMiasmicEffect(24);
				}
				break;
			case MIASMIC_BLITZ:
				if (successfulHit) {
					defender.startMiasmicEffect(36);
				}
				break;
			case MIASMIC_BARRAGE:
				if (successfulHit) {
					defender.startMiasmicEffect(48);
				}
				break;
			case TELEBLOCK:
				if (successfulHit && defender.isPlayer()) {
					Player c2 = defender.toPlayerFast();
					if (c2.isTeleBlocked()) {
						break;
					}

					c.sendMessage("You have teleblocked your opponent.");
					c2.sendMessage("You have been teleblocked.");
					if (c2.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]
							|| c2.curseActive[Curses.DEFLECT_MAGIC.ordinal()]) {
						c2.setTeleBlockTimer(250);
					} else {
						c2.setTeleBlockTimer(500);
					}
				}
				break;
			case CLAWS_OF_GUTHIX:
				c.getGodSpell().castGodSpell(spell);
				if (defender.getSkills().getDynamicLevels()[Skills.DEFENSE] >= defender.getSkills().getStaticLevel(Skills.DEFENSE)) {
					defender.getSkills().drainLevel(Skills.DEFENSE, 0.05, 0.05);
				}
				break;
			case SARADOMIN_STRIKE:
				c.getGodSpell().castGodSpell(spell);
				if (successfulHit && defender.isPlayer()) {
					Player opponent = defender.toPlayerFast();
					opponent.getSkills().decrementPrayerPoints(1);
				}
				break;
			case FLAMES_OF_ZAMORAK:
				c.getGodSpell().castGodSpell(spell);
				defender.getSkills().drainLevel(Skills.MAGIC, 0.05, 0.05);
				break;
			case SHADOW_BARRAGE:
			case SHADOW_BLITZ:
			case SHADOW_BURST:
			case SHADOW_RUSH: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.ATTACK] < o.getSkills().getStaticLevel(Skills.ATTACK)) {
					return;
				}

				o.getSkills().drainLevel(Skills.ATTACK, 0.10, 1);
				o.sendMessage("Your attack level has been reduced!");
				break;
			}
			case CONFUSE: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.ATTACK] < o.getSkills().getStaticLevel(Skills.ATTACK)) {
					c.sendMessage("Target already affected by this spell.");
					return;
				}

				o.getSkills().drainLevel(Skills.ATTACK, 0.05, 1);
				o.sendMessage("Your attack level has been reduced!");
				break;
			}
			case WEAKEN: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.STRENGTH] < o.getSkills().getStaticLevel(Skills.STRENGTH)) {
					c.sendMessage("Target already affected by this spell.");
					return;
				}

				o.getSkills().drainLevel(Skills.STRENGTH, 0.05, 1);
				o.sendMessage("Your strength level has been reduced!");
				break;
			}
			case CURSE: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.DEFENSE] < o.getSkills().getStaticLevel(Skills.DEFENSE)) {
					c.sendMessage("Target already affected by this spell.");
					return;
				}

				o.getSkills().drainLevel(Skills.DEFENSE, 0.05, 1);
				o.sendMessage("Your defence level has been reduced!");
				break;
			}
			case VULNERABILITY: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.DEFENSE] < o.getSkills().getStaticLevel(Skills.DEFENSE)) {
					c.sendMessage("Target already affected by this spell.");
					return;
				}

				o.getSkills().drainLevel(Skills.DEFENSE, 0.10, 1);
				o.sendMessage("Your defence level has been reduced!");
				break;
			}
			case ENFEEBLE: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.STRENGTH] < o.getSkills().getStaticLevel(Skills.STRENGTH)) {
					c.sendMessage("Target already affected by this spell.");
					return;
				}

				o.getSkills().drainLevel(Skills.STRENGTH, 0.10, 1);
				o.sendMessage("Your strength level has been reduced!");
				break;
			}
			case STUN: {
				if (!defender.isPlayer() || !successfulHit) {
					break;
				}

				Player o = defender.toPlayerFast();
				if (o.getSkills().getDynamicLevels()[Skills.ATTACK] < o.getSkills().getStaticLevel(Skills.ATTACK)) {
					c.sendMessage("Target already affected by this spell.");
					return;
				}

				o.getSkills().drainLevel(Skills.ATTACK, 0.10, 1);
				o.sendMessage("Your attack level has been reduced!");
				break;
			}
			case SANGUINESTI_STAFF: {
				if (!successfulHit) {
					return;
				}

				if (Misc.randomBoolean(6)) {
					defender.startGraphic(Graphic.osrs(1542));
					c.getSkills().heal(damage);
				}
				break;
			}
			default:
				break;
		}
	}

	private static void onHitRules(BattleState state, boolean procRangerPower) {
		Player attacker = state.getAttacker();
		attacker.getSeasonalData().nextSpellCrit = false;
		attacker.getSeasonalData().nextWindSpellCrit = false;

		int lifestealAmt = attacker.getSeasonalData().getPowerUpAmount(PowerUpData.LIFESTEAL);
		if (lifestealAmt > 0) {
			int percent = lifestealAmt * 5;
			int healed = state.getCurrentHit() * percent / 100;
			if (healed > 0) {
				attacker.getSkills().heal(healed);
				//TODO add shit to it?
			}
		}

		int rangeCritAmt = attacker.getSeasonalData().getPowerUpAmount(PowerUpData.RANGE_CRIT);
		if (rangeCritAmt > 0 && state.getCurrentHit() > 0 && state.getCombatStyle() == CombatType.RANGED && (Misc.random(100) <= rangeCritAmt * 6)) {
			attacker.getSeasonalData().nextSpellCrit = true;
		}

		int fireWindAmt = attacker.getSeasonalData().getPowerUpAmount(PowerUpData.FIRE_WIND);
		if (fireWindAmt > 0 && state.getCurrentHit() > 0 && state.getCombatStyle() == CombatType.MAGIC && SpellsData.FIRE_SPELLS.contains(state.getSpellsData()) && (Misc.random(100) <= fireWindAmt * 15)) {
			attacker.getSeasonalData().nextWindSpellCrit = true;
		}

		if (procRangerPower) {
			state.getDefender().startGraphic(Graphic.create(2140));
			if (state.getDefender().isNpc()) {
				if (!state.getDefender().isStunned()) {
					state.getDefender().stunEntity(2, state.getDefender().getSize() == 1 ? Graphic.create(254, GraphicType.HIGH) : null);
					state.getAttacker().sendMessageSpam("You've stunned your opponent.");
				}
			} else {
				Direction dir = Direction.getDirection(state.getAttacker().getCurrentLocation(), state.getDefender().getCurrentLocation());
	        	Location newLocation = state.getDefender().getCurrentLocation().transform(dir, 2);
	        	if (!RegionClip.rayTraceBlocked(state.getDefender().getCurrentLocation().getX(), state.getDefender().getCurrentLocation().getY(), newLocation.getX(), newLocation.getY(), newLocation.getZ(), false, state.getDefender().getDynamicRegionClip())) {
	        		state.getDefender().startAnimation(734);
	        		Direction faceDir = Direction.getDirection(state.getDefender().getCurrentLocation(), state.getAttacker().getCurrentLocation());
	        		ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX() * 2, dir.getStepY() * 2, 2, 0, faceDir).setCustomSpeed(45, 0);
	        		state.getDefender().setForceMovement(mask);
	        	}
			}
		}

		if (state.getDefender().isNpc()) {
			NPC n = state.getDefender().toNpcFast();
			
			Boss boss = n.setOrGetBoss();
			if (boss != null) {
				boss.onHitFromAttackMob(n, state);
			}
			
			n.onHitFromAttackMob(state);
				
			if (n.npcType == 8349 && state.getCurrentHit() > 0) {
				if (Misc.random(5) == 0) {
					if (state.getCombatStyle() == CombatType.MAGIC) {
						n.protectPrayer = Prayers.PROTECT_FROM_MAGIC.ordinal();
						n.transform(8350);
					} else if (state.getCombatStyle() == CombatType.RANGED) {
						n.protectPrayer = Prayers.PROTECT_FROM_MISSILES.ordinal();
						n.transform(8351);
					} else {
						n.protectPrayer = Prayers.PROTECT_FROM_MELEE.ordinal();
						n.transform(8349);
					}
				}
			}
		}
	}
	
	private static boolean npcAttackableRules(BattleState state) {
		int npcId = state.getDefender().toNpc().npcType;
		switch (npcId) {
			case 6222:
				if (state.getCombatStyle() != CombatType.RANGED) {
					state.getAttacker().sendMessage("You can only use range on this NPC."+state.getCombatStyle().toString());
					state.getAttacker().getCombat().resetPlayerAttack();
					state.getAttacker().getMovement().resetWalkingQueue();
					return false;
				}
				break;
			case VerzikWeb.ID:
				if (state.getAttacker().getCurrentLocation().equals(state.getDefender().getCurrentLocation())) {
					state.getAttacker().sendMessage("You're stuck in the web!");
					return false;
				}
				break;
			default:
				break;
		}

		return true;
	}

	public static boolean isWithinDistanceToHit(BattleState state, SpecialAttack specialAttack) {
		return isWithinDistanceToHit(state.getAttacker(), state.getDefender(), specialAttack);
	}

	public static boolean isWithinDistanceToHit(Player attacker, Mob defender, SpecialAttack specialAttack) {
		if (defender.isDead()) {
			attacker.getCombat().resetPlayerAttack();
			attacker.getMovement().resetWalkingQueue();
			return false;
		}

		int distRequired;
		if (specialAttack != null && specialAttack.isMageDistance()) {
			distRequired = RSConstants.COMBAT_SPELL_DIST;
		} else {
			distRequired = CombatFormulas.getRangeDistReq(attacker);
		}

		if (distRequired > 1) { // range distance
			if (attacker.getFightXp() == FightExp.RANGED_DEFENSE) { // long distance shots
				distRequired += 2;
			}

			if (attacker.getSeasonalData().containsPowerUp(PowerUpData.RANGER) && (attacker.getFightXp() == FightExp.RANGED || attacker.getFightXp() == FightExp.RANGED_DEFENSE)) {
				distRequired += 2;
			}

			if (defender.isNpc()) {
				if (attacker.getPA().withinDistanceToNpc(defender.toNpcFast(), distRequired)) {
					return true;
				}
			} else {
				if (attacker.withinDistance(defender,
						distRequired)) {
					return true;
				}
			}
			
		} else { // melee distance
			
			if (defender.isNpc()) {
				if (PathFinder.reachedEntity(1,
						defender.getCurrentLocation().getX(),
						defender.getCurrentLocation().getY(),
						attacker.getX(), attacker.getY(),
						attacker.entityClickSizeX, attacker.entityClickSizeY,
						attacker.entityWalkingFlag,
						RegionClip.getClipping(attacker.getX(),
								attacker.getY(), attacker.getZ(),
								attacker.getDynamicRegionClip()))) {
					return true;
				}
			} else {
				if (attacker.withinDistance(defender,
						distRequired)) {
					return true;
				}
			}
		}
		return false;
	}

	public static void putIntoCombat(Client attacker, Mob defender) {
		if (defender.isPlayer()) {
			Player o = defender.toPlayerFast();
			
			o.putInCombatWithPlayer(attacker.getId());
			
			if (attacker.inWild() && o.inWild()) {
				o.safeTimer = 16;
				attacker.safeTimer = 16;
				o.lastHitByPlayerTick = Server.getTotalTicks();
			}
			
			if (o.autoRet == 1 && !o.walkingToItem && o.clickObjectType <= 0) {
				o.playerIndex = attacker.getId();
			}
		} else {
			NPC n = defender.toNpcFast();
			
			n.underAttackBy = attacker.getId();
			
			if (n.underAttackBy > 0 && NPCHandler.getsPulled(n.getId())) {
				n.setKillerId(attacker.getId());
			} else if (n.underAttackBy < 0 && !NPCHandler.getsPulled(n.getId())) {
				n.setKillerId(attacker.getId());
			}
		}
	}


	public static boolean skipMultiEntity(BattleState state, Mob others, int distance) {
		if (others == null || !others.isActive || others.isDead() || others.getSkills().getLifepoints() <= 0)
			return true;
		if (!state.getDefender().getCurrentLocation().isWithinDistanceNoZ(others.getCurrentLocation(), distance))
			return true;
		if (RegionClip.rayTraceBlocked(state.getAttacker().getCurrentLocation().getX(), state.getAttacker().getCurrentLocation().getY(), state.getDefender().getCurrentLocation().getX(), state.getDefender().getCurrentLocation().getY(), state.getAttacker().getCurrentLocation().getZ(), true, state.getAttacker().getDynamicRegionClip()))
			return true;
		if (others.isNpc()) {
			if (!AttackNPC.attackPrereq(state.getAttacker(), others.toNpc(), false)) {
				return true;
			}
		} else if (others.isPlayer()) {
			Player playerOther = others.toPlayerFast();

			if (playerOther.hidePlayer) {
				return true;
			}

			if (MeleeRequirements.sameTeam(state.getAttacker(), playerOther)) {
				return true;
			}
			if (RSConstants.inSoulWarsRedRespawn(playerOther.absX, playerOther.absY) || RSConstants.inSoulWarsBlueRespawn(playerOther.absX, playerOther.absY) || RSConstants.inBlueGraveyardDeath(playerOther.absX, playerOther.absY) || RSConstants.inRedGraveyardRoomDeath(playerOther.absX, playerOther.absY)) {
				return true;
			}
			if (!MeleeRequirements.checkReqs(state.getAttacker(), (Client) playerOther, false)/*!MeleeRequirements.combatLevelCheck(state.getAttacker(), playerOther)*/)
				return true;
		}
		return false;
	}
	
	public static boolean canHit(Client attacker, Mob defender) {
		switch (attacker.playerEquipment[PlayerConstants.playerWeapon]) {
			case DawnbringerPlugin.ID:
				if (attacker.tobManager == null) {
					attacker.getItems().deleteEquipmentBySlot(PlayerConstants.playerWeapon);
					System.err.println(attacker.getDisplayName() + " attacked with Dawnbringer outside tob.");
				}
				break;
		case BlowPipeCharges.BLOWPIPE_EMPTY:
			attacker.getCombat().resetPlayerAttack();
			attacker.getMovement().resetWalkingQueue();
			attacker.sendMessage("You need to charge the blowpipe with Zulrah scales first.");
			return false;
			
		case CrawsBowPlugin.UNCHARGED_ID:
			attacker.getCombat().resetPlayerAttack();
			attacker.getMovement().resetWalkingQueue();
			attacker.sendMessage("You need to charge the bow with Revenant Ether first.");
			return false;


		case BlowPipeCharges.BLOWPIPE_FULL:
			if (attacker.getCharges(BlowPipeCharges.BLOWPIPE_FULL) == 0) {
				attacker.sendMessage("Julius told me, to tell you guys, that your blow pipe needs at least one zulrah scale to fire!");
				attacker.getCombat().resetPlayerAttack();
				attacker.getMovement().resetWalkingQueue();
				return false;
			}
			break;

		case DihnBulwarkPlugin.ITEM_ID:
			boolean disruptedSpell = false;
			if (attacker.getSingleCastSpell() != null) {
				attacker.setSingleCastSpell(null);
				disruptedSpell = true;
			} else if (attacker.getAutoCastSpell() != null) {
				attacker.getPA().resetAutocast();
				disruptedSpell = true;
			}
			if (disruptedSpell) {
				attacker.sendMessage("Your bulwark gets in the way.");
				return false;
			}
			if (attacker.getWeaponAttackType() == WeaponAttackTypes.DEFENSIVE) // don't allow hitting in block position
				return false;
			break;
		}

		return true;
	}
	
}
