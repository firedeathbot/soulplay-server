package com.soulplay.content.combat;

import java.util.function.Predicate;

import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.game.model.player.Player;

public enum CombatType {
	MELEE("Melee", 0, player -> player.curseActive[Curses.DEFLECT_MELEE.ordinal()]
			|| player.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()]),
	RANGED("Ranged", 1, player -> player.curseActive[Curses.DEFLECT_MISSILES.ordinal()]
			|| player.prayerActive[Prayers.PROTECT_FROM_MISSILES.ordinal()]),
	MAGIC("Magic", 2, player -> player.curseActive[Curses.DEFLECT_MAGIC.ordinal()]
			|| player.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]);

	private final String name;
	private final int id;
	private final Predicate<Player> prayerActive;

	CombatType(String name, int id, Predicate<Player> prayerActive) {
		this.name = name;
		this.id = id;
		this.prayerActive = prayerActive;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public Predicate<Player> getPrayerActive() {
		return prayerActive;
	}

	public static CombatType list(int id) {
		switch (id) {
			case 1:
				return RANGED;
			case 2:
				return MAGIC;
			default:
				return MELEE;
		}
	}

}
