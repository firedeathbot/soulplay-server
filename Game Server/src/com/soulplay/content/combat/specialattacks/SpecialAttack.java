package com.soulplay.content.combat.specialattacks;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Config;
import com.soulplay.content.combat.AttackMob;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.items.degradeable.DegradeData;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.combat.specattack.SpecButton;
import com.soulplay.content.player.combat.specattack.ThrownaxeSpecial;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.tob.npcs.VerzikVitur;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.util.Misc;
import com.soulplay.game.sounds.Sounds;

public enum SpecialAttack {
	DAWNBRINGER(new int[] { 122516 }, 3.5, Animation.osrs(1167), Graphic.osrs(1546, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			if (state.getDefender().isNpc()) {
				NPC npc = state.getDefender().toNpcFast();
				if (npc.getNpcTypeSmart() == 108370) {
					state.setCurrentHit(Misc.random(75, 150) * VerzikVitur.DAWNBRINGER_DMG_REDUCTION);
				}
			}

			state.setCombatStyle(CombatType.MAGIC);
			super.init(state);
		}

	},
	VOLATILE_STAFF(new int[] { 124424 }, 5.5, Animation.osrs(8532), Graphic.osrs(1760), Graphic.osrs(1759), true) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.5);
			int interpolate = Misc.interpolate(
					50, 66, 75, 99,
					Math.min(state.getAttacker().getSkills().getLevel(Skills.MAGIC), 99));
			int maxHit = Misc.newRandom(state.getAttacker().getCombat().magicMaxHit(interpolate));
			int hitCap = 80;
			if (maxHit > hitCap) maxHit = hitCap;

			state.setCurrentHit(maxHit);
			state.setCombatStyle(CombatType.MAGIC);
			state.getAttacker().getMovement().resetWalkingQueue();
			super.init(state);
		}

	},
	ELDRITCH_STAFF(new int[] { 124425 }, 7.5, Animation.osrs(8532), null, Graphic.osrs(1761), true) {

		@Override
		public void init(BattleState state) {
			int interpolate = Misc.interpolate(
					39, 50, 75, 99,
					Math.min(state.getAttacker().getSkills().getLevel(Skills.MAGIC), 99));
			int maxHit = Misc.newRandom(state.getAttacker().getCombat().magicMaxHit(interpolate));
			int hitCap = 60;
			if (maxHit > hitCap) maxHit = hitCap;

			state.setCurrentHit(maxHit);
			state.setCombatStyle(CombatType.MAGIC);
			state.getAttacker().getMovement().resetWalkingQueue();
			super.init(state);
		}

		@Override
		public void activateExpDropEffect(BattleState state) {
			int hit = state.getCurrentHit();
			if (hit <= 0) {
				return;
			}

			int recoverPrayer = (int) Math.round(hit * 0.50);
			if (recoverPrayer < 1) {
				recoverPrayer = 1;
			}

			state.getAttacker().getSkills().incrementPrayerPoints(recoverPrayer);
		}

	},
	DDS(SpecWeaponIds.DDS_IDS, 2.5, new Animation(1062), Graphic.create(252, GraphicType.HIGH), Sounds.DDS_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.15)));
			BattleState state2 = state.copy(state.getAttacker(), state.getDefender());
			state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.15)));
			AttackMob.applyDamage(state2);
			super.init(state);
		}
		
	},
	DRAGON_CLAWS(new int[] { 14484, 14486, LmsManager.DRAGON_CLAWS }, 5.0, new Animation(10961), Graphic.create(1950)) {
		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.DRAGON_CLAWS);
			super.init(state);
		}
	},
	DRAGON_LONGSWORD(SpecWeaponIds.DRAGON_LONG, 2.5, new Animation(12033), Graphic.create(2117, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.15)));
			super.init(state);
		}
		
	},
	DRAGON_SCIMITAR(new int[] { 4587, 13477, 13979, 13981 }, 5.5, new Animation(12031), Graphic.create(2118, GraphicType.HIGH), Sounds.DRAGON_SCIMITAR_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.DRAGON_SCIMITAR);
			state.setAccuracyMod(1.35);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.25)));
			super.init(state);
		}
		
	},
	DRAGON_MACE(new int[] { 1434, 13479, 13985, 13987 }, 2.5, new Animation(1060), Graphic.create(251, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.10);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.45)));
			super.init(state);
		}
		
	},
	DRAGON_2H(new int[] { 7158, 13430, LmsManager.DRAGON_2H }, 6.0, new Animation(7078), Graphic.create(1225)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.DRAGON_2H);
			state.setAccuracyMod(1.15);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.10)));
			super.init(state);
		}
		
	},
	DRAGON_ZAMMY_SPEAR(new int[] { 1249, 1263, 3176, 5716, 5730, 13770, 13772, 13774, 13776, 13988, 13990, 11716, 13454, 111889 }, 2.5, new Animation(12017), Graphic.create(2116, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.DRAGON_SPEAR);
			state.getDefender().shove(5, state.getAttacker().getCurrentLocation());
			state.getAttacker().getCombat().resetPlayerAttack();
			state.getAttacker().faceLocation(state.getDefender().getCurrentLocation());
			super.init(state);
		}
		
	},
	DRAGON_HALBERD(new int[] { 3204, 13478 }, 3.0, new Animation(1203), Graphic.create(282, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.DRAGON_HALBERD);
			state.setAccuracyMod(1.10);
			super.init(state);
		}
		
	},
	DRAGON_WARHAMMER(new int[] { 30022, LmsManager.DRAGON_WARHAMMER }, 5.0, new Animation(Animation.getOsrsAnimId(1378)), Graphic.create(1292 + Config.OSRS_GFX_OFFSET, GraphicType.MEDIUM)) {
		@Override
		public void init(BattleState state) {
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.50)));
			super.init(state);
		}
		@Override
		public void activateExpDropEffect(BattleState state) {
			if (state.getCurrentHit() > 0) {
				state.getDefender().getSkills().drainLevel(Skills.DEFENSE, 0.30, 1);
			}
		}
	},
	KORASI(new int[] { 19780, 19784 }, 6.0, new Animation(14788), Graphic.create(1729), Graphic.create(2795)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.KORASI);
			state.setAccuracyMod(1.80);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.55)));
			state.setCombatStyle(CombatType.MAGIC);
			super.init(state);
		}
		
	},
	BARRELCHEST_ANCHOR(new int[] { 10887 }, 5.0, new Animation(5870), Graphic.create(1027)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.BARRELCHEST_ANCHOR);
			state.setAccuracyMod(2.0);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.20)));
			super.init(state);
		}
		
	},
	VESTA_SPEAR(new int[] { 13905, 13907, 13929, 13931 }, 5.0, new Animation(10499), Graphic.create(1835)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.VESTA_SPEAR);
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.08)));
			super.init(state);
		}
		
	},
	VESTA_LONGSWORD(new int[] { 13899, 13901, 13923, 13925 }, 2.5, new Animation(10502)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.60);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.15)));
			super.init(state);
		}
		
	},
	STATIUS_WARHAMMER(new int[] { 13902, 13904, 13926, 13928 }, 5.0, new Animation(10505), Graphic.create(1840)) {

		@Override
		public void init(BattleState state) {
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.34)));
			super.init(state);
		}

		@Override
		public void activateExpDropEffect(BattleState state) {
			if (state.getCurrentHit() > 0) {
				state.getDefender().getSkills().drainLevel(Skills.DEFENSE, 0.35, 1);
			}
		}

	},
	ARMADYL_GODSWORD(new int[] { 11694, 13450, LmsManager.ARMADYL_GODSWORD }, 5.0, new Animation(11989), Graphic.create(2113)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.37);
			if (state.getAttacker().wildLevel > 0)
				state.setCurrentHit(Math.min(85, Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.37))));
			else
				state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.37)));
			super.init(state);
		}
		
	},
	ZAMORAK_GODSWORD(new int[] { 11700, 13453 }, 5.5, new Animation(7070), Graphic.create(1221)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.ZAMORAK_GODSWORD);
			state.setAccuracyMod(1.37);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.05)));
			super.init(state);
		}
		
	},
	BANDOS_GODSWORD(new int[] { 11696, 13451 }, 6.0, new Animation(11991), Graphic.create(2114)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.BANDOS_GODSWORD);
			state.setAccuracyMod(1.37);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.30)));
			if (Misc.newRandom(99) < 25) { // a new addition to the bandos godsword
				state.getDefender().stunEntity(3, Graphic.create(254, GraphicType.HIGH));
			}
			super.init(state);
		}
		
	},
	SARADOMIN_GODSWORD(new int[] { 11698, 13452 }, 5.0, new Animation(12019), Graphic.create(2109)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			int maxHit = state.getAttacker().getCombat().calculateMeleeMaxHit(1.25);
			int lowestHit = maxHit / 6;
			state.setCurrentHit((int) Math.round(Misc.randomDouble(lowestHit, maxHit)));
			super.init(state);
		}
		
		@Override
		public void activateExpDropEffect(BattleState state) {
			if (state.getCurrentHit() > 0) {
				int healAmount = (int) Math.round(state.getCurrentHit() / 2.0);
				if (healAmount < 10) {
					healAmount = 10;
				}
				int recoverPrayer = (int) Math.round(state.getCurrentHit() * 0.25);
				if (recoverPrayer < 5) {
					recoverPrayer = 5;
				}

				state.getAttacker().getSkills().heal(healAmount);
				state.getAttacker().getSkills().incrementPrayerPoints(recoverPrayer);
			}
		}
		
	},
	SARADOMIN_SWORD(new int[] { 11730, 13461 }, 5.0, new Animation(11993), Graphic.create(2115), Graphic.create(1194)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.50);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.25)));
			
			BattleState state2 = new BattleState(state.getAttacker(), state.getDefender());
			state2.setFightExp(state.getFightExp());
			state2.setHitDelay(state.getHitDelay());
			state2.setSpecEffect(SpecialEffects.SARA_SWORD);
			state2.setCurrentHit(5 + Misc.newRandom(11));
			state2.setCombatStyle(CombatType.MAGIC);
			AttackMob.applyDamage(state2);
			
			super.init(state);
		}
		
		@Override
		public void activateExpDropEffect(BattleState state) {
			if (state.getCurrentHit() > 0) {
				int healAmount = (int) Math.round(state.getCurrentHit() / 2.0);
				if (healAmount < 10) {
					healAmount = 10;
				}
				int recoverPrayer = (int) Math.round(state.getCurrentHit() * 0.25);
				if (recoverPrayer < 5) {
					recoverPrayer = 5;
				}

				state.getAttacker().getSkills().heal(healAmount);
				state.getAttacker().getSkills().incrementPrayerPoints(recoverPrayer);
			}
		}
		
	},
	ANCIENT_MACE(new int[] { 11061 }, 5.0, new Animation(6147), Graphic.create(1052)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.IGNORE_ARMOR_ONLY);
			state.setAccuracyMod(1.05);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.10)));
			super.init(state);
		}
		
	},
	ABYSSAL_BLUDGEON(new int[] { 30018 }, 5.0, new Animation(401), null, Graphic.osrs(1284)) {//TODO wrong anim

		@Override
		public void init(BattleState state) {
			int prayersPointsMissing = state.getAttacker().getSkills().getStaticLevel(Skills.PRAYER) - state.getAttacker().getSkills().getPrayerPoints();
			double extraMaxHit = prayersPointsMissing * 0.005;
			state.setCurrentHit(Misc.newRandom((int)(state.getAttacker().getCombat().calculateMeleeMaxHit(1.00 + extraMaxHit))));
			super.init(state);
		}

	},
	NORMAL_ABYSSAL_WHIP(new int[] { 4151, 4178, 13444, 14661, LmsManager.ABYSSAL_WHIP }, 5.0, new Animation(11971), Graphic.create(2825), Graphic.create(2108, GraphicType.HIGH), Sounds.WHIP_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);

			if (state.getDefender().isPlayer()) {
				double leechEnergy = state.getDefender().toPlayer().getMovement().drainEnergy(10);
				if (leechEnergy > 0) {
					state.getAttacker().getMovement().restoreRunEnergy(leechEnergy);
				}
			}
			super.init(state);
		}
		
	},
	BLUE_GREEN_ABYSSAL_WHIP(new int[] { 15442, 15444 }, 5.0, new Animation(11971), Graphic.create(2825), Graphic.create(2108, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.10);
			super.init(state);
		}
		
	},
	WHITE_ABYSSAL_WHIP(new int[] { 15443 }, 6.0, null, Graphic.create(247), Sounds.WHIP_SPEC) {

		@Override
		public void init(BattleState state) {
			state.getAttacker().getPA().increaseAS();
			super.init(state);
		}
		
	},
	YELLOW_ABYSSAL_WHIP(new int[] { 15441 }, 5.0, new Animation(11971), Graphic.create(2825), Sounds.WHIP_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.15);
			state.getDefender().stunEntity(3, Graphic.create(254, GraphicType.HIGH));
			super.init(state);
		}
		
	},
	ABYSSAL_VINE_WHIP(new int[] { 21371, 21372, 21373, 21374, 21375 }, 6.0, new Animation(11971), Graphic.create(2825), Graphic.create(1965, GraphicType.HIGH), Sounds.WHIP_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.15);
			state.setCurrentHit(Misc.newRandom((int)(state.getAttacker().getCombat().calculateMeleeMaxHit(1.05))));
			if (state.getDefender().isPlayer())
				state.getAttacker().getCombat().disableSpiritShield(state.getDefender().toPlayer(), 50);
			super.init(state);
		}
		
	},
	ABYSSAL_TENTACLE(new int[] { 12006 }, 6.0, new Animation(11971), Graphic.create(2825), Graphic.create(180, GraphicType.HIGH), Sounds.WHIP_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.ABYSSAL_TENTACLE);
			state.setAccuracyMod(1.10);
			super.init(state);
		}
		
	},
	ABYSSAL_DAGGER(new int[] { 30020, 30021 }, 5.0, new Animation(1062), Graphic.create(Graphic.getOsrsId(1283), GraphicType.HIGH), Sounds.DDS_SPEC) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(0.95)));
			BattleState state2 = state.copy(state.getAttacker(), state.getDefender());
			state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(0.95)));
			AttackMob.applyDamage(state2);
			super.init(state);
		}
		
	},
	EASTER_CARROT(new int[] { 14728 }, 1.0, new Animation(Animation.getOsrsAnimId(1378)), Graphic.create(2856)) {
		@Override
		public void init(BattleState state) {
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.50)));
			super.init(state);
		}
		@Override
		public void activateExpDropEffect(BattleState state) {
			if (state.getCurrentHit() > 0) {
				state.getDefender().getSkills().drainLevel(Skills.DEFENSE, 0.30, 1);
			}
		}
	},
	STAFF_OF_LIGHT(new int[] { 15486, 15502, 22206, 22207, 22208, 22209, 22210, 22211, 22212, 22213, 11791, DegradeData.TSOTD,  DegradeData.TSOTD_UNCHARGED, 124144 }, 10.0, new Animation(12804), Graphic.create(2319)) {
		
		@Override
		public void init(BattleState state) {
			state.getAttacker().getPA().activateSOL();
			super.init(state);
		}
	},
	STAFF_OF_THE_DEAD(new int[] { 11791, 30011 }, 10.0, new Animation(Animation.getOsrsAnimId(7083)), Graphic.create(1228 + Config.OSRS_GFX_OFFSET, GraphicType.EVEN_HIGHER)) {
		
		@Override
		public void init(BattleState state) {
			state.getAttacker().getPA().activateSOL();
			super.init(state);
		}
	},
	DRAGON_HARPOON(new int[] { 30069 }, 10.0, new Animation(1056), Graphic.create(Graphic.getOsrsId(246), GraphicType.PROPER_LOW)) {
		@Override
		public void init(BattleState state) {
			SpecButton.dragonHarpoon(state.getAttacker());
			super.init(state);
		}
	},
	DRAGON_SWORD(new int[] { 30088 }, 4.0, new Animation(Animation.getOsrsAnimId(7515)), Graphic.create(Graphic.getOsrsId(1369), GraphicType.HIGH)) {
		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.25)));
			super.init(state);
		}
	},
	DRAGON_HASTA(new int[] { 122731 }, 0.0, new Animation(Animation.getOsrsAnimId(7515)), Graphic.create(Graphic.getOsrsId(1369), GraphicType.HIGH)) {
		@Override
		public void init(BattleState state) {
			double spec = state.getAttacker().specAmount;
			if (spec >= 10.0) {
				spec = 10.0;
			}

			int specInt = (int) spec * 10;
			int specConsumed = specInt / 5;
			double accBoost = specConsumed * 0.05;
			double dmgBoost = specConsumed * 0.025;
			state.setAccuracyMod(1.00 + accBoost);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().calculateMeleeMaxHit(1.00 + dmgBoost)));
			super.init(state);
		}
	},
	DRAGON_BATTLEAXE(new int[] { 1377 }, 10.0, new Animation(1056), Graphic.create(Graphic.getOsrsId(246), GraphicType.PROPER_LOW)) {
		@Override
		public void init(BattleState state) {
			SpecButton.dragonBattleAxeShit(state.getAttacker());
			super.init(state);
		}
	},
	GRANITE_MAUL(new int[] { 4153, 13445 }, 6.0, null) {
		
		@Override
		public void init(BattleState state) {
			super.init(state);
			state.getAttacker().setPauseAnimReset(0);
		}
	},
	GRANITE_MAUL_CLAMP(new int[] { 30066 }, 5.0, null) {
		
		@Override
		public void init(BattleState state) {
			super.init(state);
			state.getAttacker().setPauseAnimReset(0);
		}
	},
	BRACKISH_BLADE(new int[] { 12006 }, 6.0, new Animation(1060)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.BRACKISH_BLADE);
			state.setAccuracyMod(2.0);
			super.init(state);
		}
		
	},
	DINHS_BULWARK(new int[] { 30094 }, 5.0, new Animation(Animation.getOsrsAnimId(7512)), Graphic.create(Graphic.getOsrsId(1335), GraphicType.THIRTY)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.DINHS_BULWARK);
			state.setAccuracyMod(1.2);
			super.init(state);
		}
		
	},
	
	//RANGED WEAPONS
	TOXIC_BLOWPIPE(new int[] { 12926 }, 5.0, new Animation(25061)) {

		@Override
		public void init(BattleState state) {
			state.getProj().setProjectileGFX(1043 + Config.OSRS_GFX_OFFSET);
			int damage = Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.50));
			state.getAttacker().healHitPoints(Math.round(damage / 2));
			state.setCurrentHit(damage);
			super.init(state);
		}
		
	},
	DRAGON_THROWNAXE(new int[] { 30093 }, 2.5, new Animation(Animation.getOsrsAnimId(7521)), Graphic.create(Graphic.getOsrsId(1317), GraphicType.HIGH)) {
		
		@Override
		public void init(BattleState state) {
			state.getProj().setProjectileGFX(Graphic.getOsrsId(1318));
			state.setAccuracyMod(1.25);
			state.getAttacker().setPauseAnimReset(0);
			state.getProj().setSlope(10);
			state.getProj().setStartHeight(45);
			state.getProj().setStartDelay(25);
			state.getProj().setEndDelay(20+(state.getDistanceToVictim()*5));
			state.getProj().setStartOffset(0);
			state.setHitDelay(state.getDistanceToVictim() > 3 ? 2 : 1);
			state.setDropAmmo(false);
			state.getAttacker().getItems().deleteEquipedWeaponSlot();//consume the thrownaxe
			super.init(state);
		}
	},
	DRAGON_KNIFE(new int[] { 122804, 122806, 122808, 122810 }, 2.5, Animation.osrs(8291), null) {

		@Override
		public void init(BattleState state) {
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.00)));

			state.getProj().setProjectileGFX(Graphic.getOsrsId(699));
			state.getAttacker().setPauseAnimReset(0);
			state.getProj().setSlope(10);
			state.getProj().setStartHeight(45);
			state.getProj().setStartDelay(25);
			state.getProj().setEndDelay(20+(state.getDistanceToVictim()*5));
			state.getProj().setStartOffset(0);
			state.setHitDelay(state.getDistanceToVictim() > 3 ? 2 : 1);
			state.setDropAmmo(false);
			super.init(state);

			BattleState state2 = state.copy(state.getAttacker(), state.getDefender());
			state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.00)));
			AttackMob.applyDamage(state2);
		}

	},
	RUNE_THROWNAXE(new int[] { 805 }, 1.0, new Animation(1068), Graphic.create(Graphic.getOsrsId(257), GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.getProj().setProjectileGFX(258);
			state.getAttacker().setPauseAnimReset(0);
			state.getProj().setEndHeight(25);
			state.getProj().setStartHeight(25);
			state.getProj().setSlope(0);
			ThrownaxeSpecial.activateSpecial(state);
			state.setDropAmmo(false);
			state.getAttacker().getItems().deleteEquipedWeaponSlot();//consume the thrownaxe
			super.init(state);
		}
		
	},
	HAND_CANNON(new int[] { 15241, 24145 }, 6.5, new Animation(12175)) { // animation 12152 is regular shot

		@Override
		public void init(BattleState state) {
			
			int delayOfBlast = Misc.serverToClientTick(2);
			state.setHitDelay(state.getHitDelay() + 2);
			state.getProj().setStartDelay(state.getProj().getStartDelay() + delayOfBlast);
			state.getProj().setEndDelay(state.getProj().getEndDelay() + delayOfBlast);
			state.setAttackerGfx(Graphic.create(state.getAttackerGfx().getId(), delayOfBlast, GraphicType.LOW));

			state.setAccuracyMod(1.15);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.0 + (10 + Misc.newRandom(15)) / 100.0)));
//			state.getAttacker().setAttackTimer(6); // delay the next attack
			state.getAttacker().startAnimation(12175);
			state.getAttacker().setPauseAnimReset(4);
			state.getAttacker().setAttackTimer(3);
			super.init(state);
		}
		
	},
	MORRIGAN_JAVELIN(new int[] { 13879, 13880, 13881, 13882 }, 5.0, new Animation(10501), Graphic.create(1836)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.MORRIGAN_JAVELIN);
			super.init(state);
		}
		
	},
	MORRIGAN_THROWING_AXE(new int[] { 13883 }, 5.0, new Animation(10504), Graphic.create(1838)) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.20)));
			super.init(state);
		}
		
	},
	DRAGON_CROSSBOW(new int[] { 121902 }, 6.0, new Animation(4230), null, Graphic.osrs(157, GraphicType.HIGH)) {

		@Override
		public void init(BattleState state) {
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.00)) * 120 / 100);
			super.init(state);
		}

	},
	SEERCULL(new int[] { 6724, 13529 }, 5.0, new Animation(426)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.IGNORE_ARMOR_ONLY);
			state.getDefender().startGraphic(Graphic.create(474));

			if (state.getDefender().isPlayer()) {
				if (state.getCurrentHit() > 0) { // seercull
					state.getDefender().getSkills().updateLevel(Skills.MAGIC, -state.getCurrentHit(), 1);
				}
			}
			super.init(state);
		}
		
	},
	DORGESHUUN_CBOW(new int[] { 8880 }, 7.5, new Animation(4230)) {

		@Override
		public void init(BattleState state) {
			boolean facingTowardsMe = true;

			if (state.getAttacker().getIdforFaceId() != state.getDefender().getLastFacing()) { //facing me
				facingTowardsMe = false;
			} else {
				//check if face location is towards me
//				Direction myDir = Direction.getLogicalDirection(state.getAttacker().getCurrentLocation(), state.getDefender().getCurrentLocation());
//				Direction enemyDir = Direction.getLogicalDirection(state.getDefender().getCurrentLocation(), state.getDefender().getLastFacingLocation());
//				if (myDir.getOpposite() == enemyDir) {
//					facingTowardsMe = false;
//					state.getAttacker().sendMessage("facing me set = " + facingTowardsMe);
//					
//				}
			}
			
			if (!facingTowardsMe)
				state.setSpecEffect(SpecialEffects.IGNORE_ARMOR_ONLY);
			
			state.getDefender().startGraphic(Graphic.create(696));
			state.setAccuracyMod(1.5);
			state.getProj().setProjectileGFX(698);

			super.init(state);
		}
		
	},
	ARMADYL_CROSSBOW(new int[] { 11785 }, 4.0, new Animation(4230)) {

		@Override
		public void init(BattleState state) {
			state.getProj().setProjectileGFX(301 + Config.OSRS_GFX_OFFSET);
			state.setAccuracyMod(2.0);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.20)));
			super.init(state);
		}
		
	},
	ROYAL_CROSSBOW(new int[] { 24338 }, 5.0, new Animation(4230)) {

		@Override
		public void init(BattleState state) {
			
//			state.getProj().setProjectileGFX(301 + Config.OSRS_GFX_OFFSET);
			state.setAccuracyMod(2.0);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.20)));

			super.init(state);
		}
		
	},
	ZARYTE_BOW(new int[] { 20171, 20173 }, 5.0, new Animation(426), Graphic.create(250, GraphicType.BOWARROWPULL)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.ZARYTE_BOW);
			super.init(state);
		}
		
	},
	MAGIC_LONGBOW(new int[] { 859, 13527 }, 3.5, new Animation(426), Graphic.create(250, GraphicType.BOWARROWPULL)) {

		@Override
		public void init(BattleState state) {
			state.setSpecEffect(SpecialEffects.IGNORE_ARMOR_ONLY);
			super.init(state);
		}
		
	},
	MAGIC_SHORTBOW(new int[] { 861, 13528 }, 5.5, new Animation(1074), Graphic.create(256, GraphicType.BOWARROWPULL)) {
		
		@Override
		public void init(BattleState state) {
			BattleState state2 = state.copy(state.getAttacker(), state.getDefender());
			state2.setCombatStyle(CombatType.RANGED);
			state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.0)));
			state2.setHitDelay(state.getHitDelay());
			state2.setRangeWeapon(state.getRangeWeapon());
			state2.setAmmo(state.getAmmo());
			if (state2.getDistanceToVictim() > 3) {
				state2.setHitDelay(state2.getHitDelay() + 1);
			} else if (state2.getDistanceToVictim() > 6) {
				state2.setHitDelay(state2.getHitDelay() + 2);
			}
			AttackMob.applyDamage(state2);
			final Projectile arrow2 = state.getProj().copy();
			arrow2.setStartDelay(state.getProj().getStartDelay());
			arrow2.setEndDelay(state.getProj().getEndDelay()-10);
			arrow2.setProjectileGFX(249);
			state.getProj().setProjectileGFX(249);
			state.getProj().setStartDelay(state.getProj().getStartDelay()-20);
			state.getProj().setEndDelay(state.getProj().getEndDelay()-20);
			GlobalPackets.createProjectile(arrow2);
			super.init(state);
		}
		
	},
	MAGIC_SHORTBOW_IMBUED(new int[] { 30056 }, 5.0, new Animation(1074), Graphic.create(256, GraphicType.BOWARROWPULL)) {
		
		@Override
		public void init(BattleState state) {
			BattleState state2 = state.copy(state.getAttacker(), state.getDefender());
			state2.setCombatStyle(CombatType.RANGED);
			state2.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.0)));
			state2.setHitDelay(state.getHitDelay());
			state2.setRangeWeapon(state.getRangeWeapon());
			state2.setAmmo(state.getAmmo());
			AttackMob.applyDamage(state2);
			final Projectile arrow2 = state.getProj().copy();
			arrow2.setStartDelay(state.getProj().getStartDelay());
			arrow2.setEndDelay(state.getProj().getEndDelay()-10);
			arrow2.setProjectileGFX(249);
			state.getProj().setProjectileGFX(249);
			state.getProj().setStartDelay(state.getProj().getStartDelay()-20);
			state.getProj().setEndDelay(state.getProj().getEndDelay()-20);
			GlobalPackets.createProjectile(arrow2);
			super.init(state);
		}
		
	},
	DARK_BOW(new int[] { 11235, 13405, 15701, 15702, 15703, 15704, LmsManager.DARK_BOW }, 5.5, new Animation(426)) {
		@Override
		public void init(BattleState state) {
			state.setDropAmmo(false);
			super.init(state);
		}
	},
	BALLISTA(new int[] { 30023, 30024 }, 5.0, new Animation(Animation.getOsrsAnimId(7222))) {

		@Override
		public void init(BattleState state) {
			state.setAccuracyMod(1.25);
			state.setCurrentHit(Misc.newRandom(state.getAttacker().getCombat().rangeMaxHit(1.25)));
			super.init(state);
		}
		
	},
	
	;
	
	private final int[] weaponIds;
	
	private final double specAmountRequired;
	
	private final Animation anim;
	private final Graphic attackerGfx;
	
	private final Graphic victimGfx;

	private final int sound;
	private final boolean mageDistance;

	SpecialAttack(int[] weaponId, double specReq, Animation anim, Graphic startGfx, Graphic endGfx, boolean mageDistance) {
		this(weaponId, specReq, anim, startGfx, endGfx, -1, mageDistance);
	}

	SpecialAttack(int[] weaponId, double specReq, Animation anim, Graphic startGfx, Graphic endGfx) {
		this(weaponId, specReq, anim, startGfx, endGfx, -1, false);
	}

	SpecialAttack(int[] weaponId, double specReq, Animation anim, Graphic startGfx, Graphic endGfx, int sound) {
		this(weaponId, specReq, anim, startGfx, endGfx, sound, false);
	}

	SpecialAttack(int[] weaponId, double specReq, Animation anim, Graphic startGfx, Graphic endGfx, int sound, boolean mageDistance) {
		this.weaponIds = weaponId;
		this.specAmountRequired = specReq;
		this.anim = anim;
		this.attackerGfx = startGfx;
		this.victimGfx = endGfx;
		this.sound = sound;
		this.mageDistance = mageDistance;
	}

	SpecialAttack(int[] weaponId, double specReq, Animation anim, Graphic startGfx, int sound) {
		this(weaponId, specReq, anim, startGfx, null, sound, false);
	}
	
	SpecialAttack(int[] weaponId, double specReq, Animation anim, Graphic startGfx) {
		this(weaponId, specReq, anim, startGfx, null);
	}
	
	SpecialAttack(int[] weaponId, double specReq, Animation anim) {
		this(weaponId, specReq, anim, null);
	}

	public boolean isMageDistance() {
		return mageDistance;
	}
	
	public void activateDummy(Player attacker, Player defender) {
		if (victimGfx != null)
			defender.startGraphic(victimGfx);
		if (anim != null) {
			attacker.resetAnimations();
			attacker.setPauseAnimReset(0);
			attacker.startAnimation(anim);
			attacker.setPauseAnimReset(1);
		}
		if (attackerGfx != null) {
			attacker.startGraphic(attackerGfx);
		}
		
	}
	
	public void init(BattleState state) {
		if (victimGfx != null)
			state.setVictimGfx(this.victimGfx.copy());
		if (anim != null) {
			state.setAnimation(null);
			state.getAttacker().resetAnimations(); // using this instead of priority check for now.
			state.getAttacker().setPauseAnimReset(0);
			state.getAttacker().startAnimation(anim);
			if (sound != -1) {
				state.getAttacker().getPacketSender().playSound(sound);
			}
			state.getAttacker().setPauseAnimReset(1); // cheaphax to animations?
		}
		if (attackerGfx != null) {
			state.setAttackerGfx(attackerGfx.copy());
			state.getAttacker().startGraphic(attackerGfx.copy());
		}
		

		double vigourEffect = 0.0;
		if (state.getAttacker().playerEquipment[PlayerConstants.playerRing] == 19669) {
			vigourEffect = VIGOUR;
		}

		if (this == DRAGON_HASTA) {
			state.getAttacker().specAmount = 0;
		} else {
			double drainAmount = Math.max(0.5, this.specAmountRequired-vigourEffect);
			state.getAttacker().specAmount -= drainAmount;
		}

		state.getAttacker().usingSpecial = false;
		state.getAttacker().getItems().updateSpecialBar();
		state.getAttacker().getItems().addSpecialBar(true);
		state.getAttacker().getAchievement().specialAttack();

		// so spec bar doesn't refresh instantly after using special attack
		state.getAttacker().startSpecRestoreTick();
		
		if (state.getAttacker().getZones().isInEdgePk()) {
			state.getAttacker().specAndTeleTimer.startTimer(10);
			
			if (!state.getAttacker().underAttackByPlayer()) {
				state.getAttacker().specAndTeleTimer.startTimer(20);
			}
		}
	}
	
	public void activateExpDropEffect(BattleState state) {
		
	}

	public boolean canActivate(BattleState state) {
		if (state.getAttacker().clanWarRule[CWRules.NO_SPECIAL_ATK]
				&& state.getAttacker().inClanWars()) {
			state.getAttacker().sendMessage(
					"Special Attacks are disabled in this war.");
			state.getAttacker().usingSpecial = false;
			state.getAttacker().getItems().updateSpecialBar();
			return false;
		}
		if (state.getAttacker().duelRule[DuelRules.NO_SPECIAL_ATK]
				&& state.getAttacker().duelStatus == 5) {
			state.getAttacker().sendMessage(
					"Special attacks have been disabled during this duel!");
			state.getAttacker().usingSpecial = false;
			state.getAttacker().getItems().updateSpecialBar();
			return false;
		}

		switch (this) {
			case DRAGON_BATTLEAXE:
				if (state.getAttacker().inDuelArena()) {
					state.getAttacker().sendMessage("You can't use Dragon Battleaxe special attack in Duel Arena.");
					state.getAttacker().usingSpecial = false;
					state.getAttacker().getItems().updateSpecialBar();
					return false;
				}
				break;
			case DRAGON_HASTA:
				if (state.getAttacker().specAmount <= 0.1) {
					state.getAttacker().usingSpecial = false;
					state.getAttacker().getItems().updateSpecialBar();
					return false;
				}
				break;
			default:
				break;
		}

		double vigourEffect = 0.0;
		if (state.getAttacker().playerEquipment[PlayerConstants.playerRing] == 19669) {
			vigourEffect = VIGOUR;
		}
		return state.getAttacker().specAmount + vigourEffect - this.specAmountRequired >= 0.0;
	}

	public int[] getWeaponIds() {
		return weaponIds;
	}


	private static final Map<Integer, SpecialAttack> SPECIALS = new HashMap<>();

	public static Map<Integer, SpecialAttack> getSpecials() {
		return SPECIALS;
	}

	static {
		for (SpecialAttack effect : values()) {
			for (int element : effect.getWeaponIds()) {
				SpecialAttack.getSpecials().put(element, effect);
			}
		}
	}
	
    public static void load() {
    	/* empty */
    }
  
	public static SpecialAttack get(int id) {
		return SpecialAttack.getSpecials().get(id);
	}
	
	public static final double VIGOUR = 1.0;
	
}
