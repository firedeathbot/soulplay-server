package com.soulplay.content.combat;

import com.soulplay.content.combat.data.FightExp;
import com.soulplay.content.combat.specialattacks.SpecialAttack;
import com.soulplay.content.combat.specialattacks.SpecialEffects;
import com.soulplay.content.items.Ammunition;
import com.soulplay.content.items.RangeWeapon;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.range.BoltEffect;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;

public class BattleState {
	
	private Client attacker;
	
	private Mob defender;
	
	private int maxHit;
	
	private int currentHit;
	
	private Animation animation;
	
	private int hitDelay;
	
	private Projectile proj;
	
	private int specialAttack;
	
	private SpecialAttack spec;
	
	private SpecialEffects specEffect;
	
	private int distanceToVictim;
	
	private RangeWeapon rangeWeapon;
	
	private Ammunition ammo;
	
	private boolean dropAmmo;
	
	private CombatType combatStyle;
	
	private BoltEffect boltEffect;
	
	private Graphic attackerGfx;
	
	private Graphic victimGfx;
	
	private SpellsData spellsData;

	private FightExp fightExp;
	
	private double specMod = 1.0;
	
	private boolean successfulHit;

	private int soundId = -1;

	private boolean appendExp = true;
	
	private boolean main;

	public BattleState(Client attacker, Mob def) {
		this.attacker = attacker;
		this.defender = def;
		this.combatStyle = CombatType.MELEE;
		this.specEffect = SpecialEffects.NONE;
	}

	public Client getAttacker() {
		return attacker;
	}

	public Mob getDefender() {
		return defender;
	}

	public int getMaxHit() {
		return maxHit;
	}

	public void setMaxHit(int maxHit) {
		this.maxHit = maxHit;
	}

	public int getCurrentHit() {
		return currentHit;
	}

	public void setCurrentHit(int currentHit) {
		this.currentHit = currentHit;
	}

	public RangeWeapon getRangeWeapon() {
		return rangeWeapon;
	}

	public void setRangeWeapon(RangeWeapon rangeWeapon) {
		this.rangeWeapon = rangeWeapon;
	}

	public Ammunition getAmmo() {
		return ammo;
	}

	public void setAmmo(Ammunition ammo) {
		this.ammo = ammo;
	}

	public CombatType getCombatStyle() {
		return combatStyle;
	}

	public void setCombatStyle(CombatType combatStyle) {
		this.combatStyle = combatStyle;
	}

	public BoltEffect getBoltEffect() {
		return boltEffect;
	}

	public void setBoltEffect(BoltEffect boltEffect) {
		this.boltEffect = boltEffect;
	}

	public int getHitDelay() {
		return hitDelay;
	}

	public void setHitDelay(int hitDelay) {
		this.hitDelay = hitDelay;
	}

	public int getSpecialAttack() {
		return specialAttack;
	}

	public void setSpecialAttack(int specialAttack) {
		this.specialAttack = specialAttack;
	}

	public int getDistanceToVictim() {
		return distanceToVictim;
	}

	public void setDistanceToVictim(int distanceToVictim) {
		this.distanceToVictim = distanceToVictim;
	}

	public Graphic getVictimGfx() {
		return victimGfx;
	}

	public void setVictimGfx(Graphic victimGfx) {
		this.victimGfx = victimGfx;
	}

	public SpecialEffects getSpecEffect() {
		return specEffect;
	}

	public void setSpecEffect(SpecialEffects specEffect) {
		this.specEffect = specEffect;
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

	public Projectile getProj() {
		return proj;
	}

	public void setProj(Projectile proj) {
		this.proj = proj;
	}

	public Graphic getAttackerGfx() {
		return attackerGfx;
	}

	public void setAttackerGfx(Graphic attackerGfx) {
		this.attackerGfx = attackerGfx;
	}

	public void setSpellsData(SpellsData spellsData) {
		this.spellsData = spellsData;
	}

	public SpellsData getSpellsData() {
		return spellsData;
	}

	public FightExp getFightExp() {
		return fightExp;
	}

	public void setFightExp(FightExp fightExp) {
		this.fightExp = fightExp;
	}

	public SpecialAttack getSpec() {
		return spec;
	}

	public void setSpec(SpecialAttack spec) {
		this.spec = spec;
	}

	public double getAccuracyMod() {
		return specMod;
	}

	public void setAccuracyMod(double specMod) {
		this.specMod = specMod;
	}
	
	public boolean dropAmmo() {
		return dropAmmo;
	}

	public void setDropAmmo(boolean dropAmmo) {
		this.dropAmmo = dropAmmo;
	}

	public BattleState copy(Client attacker, Mob victim) {
		BattleState state = new BattleState(attacker, victim);
		state.setAmmo(getAmmo());
		state.setAnimation(getAnimation());
		state.setAttackerGfx(getAttackerGfx());
		state.setVictimGfx(getVictimGfx());
		state.setBoltEffect(getBoltEffect());
		state.setSpellsData(getSpellsData());
		state.setCombatStyle(getCombatStyle());
		state.setCurrentHit(getCurrentHit());
		state.setDistanceToVictim(getDistanceToVictim());
		state.setHitDelay(getHitDelay());
		state.setMaxHit(getMaxHit());
		state.setProj(getProj());
		state.setRangeWeapon(getRangeWeapon());
		state.setSpecEffect(getSpecEffect());
		state.setFightExp(getFightExp());
		state.setAccuracyMod(getAccuracyMod());
		state.setDropAmmo(dropAmmo());
		state.setSoundId(getSoundId());
		state.setAppendExp(isAppendExp());
		return state;
	}

	public boolean isSuccessfulHit() {
		return successfulHit;
	}

	public void setSuccessfulHit(boolean successfulHit) {
		this.successfulHit = successfulHit;
	}

	public int getSoundId() {
		return soundId;
	}

	public void setSoundId(int soundId) {
		this.soundId = soundId;
	}

	public boolean isAppendExp() {
		return appendExp;
	}

	public void setAppendExp(boolean appendExp) {
		this.appendExp = appendExp;
	}

	public boolean isMain() {
		return main;
	}
	
	public void setMain(boolean main) {
		this.main = main;
	}
	
}
