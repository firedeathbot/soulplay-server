package com.soulplay.content.combat;

import com.soulplay.game.model.item.definition.EquipBonusIds;

public class CombatStats {

	private int stabBonus = 0;
	private int slashBonus = 0;
	private int crushBonus = 0;
	private int magicAccBonus = 0;
	private int rangedAccBonus = 0;
	private int stabDefBonus = 0;
	private int slashDefBonus = 0;
	private int crushDefBonus = 0;
	private int magicDefBonus = 0;
	private int rangedDefBonus = 0;
	private int strengthBonus = 0;
	private int rangeStrengthBonus = 0;
	private int magicStrength = 0;
	private int prayerBonus = 0;
	private int summoning = 0;
	private boolean poisonImmunity = false;
	private boolean venomImmunity = false;
	private int attackStr = 0;
	private int attackSpeed = 4;

	public static CombatStats createDefaultStats() {
		return new CombatStats();
	}

	public int getBonus(int index) {
		switch (index) {
			case EquipBonusIds.ATTACK_STAB:
				return stabBonus;
			case EquipBonusIds.ATTACK_SLASH:
				return slashBonus;
			case EquipBonusIds.ATTACK_CRUSH:
				return crushBonus;
			case EquipBonusIds.ATTACK_MAGIC:
				return magicAccBonus;
			case EquipBonusIds.ATTACK_RANGED:
				return rangedAccBonus;
			case EquipBonusIds.DEF_STAB:
				return stabDefBonus;
			case EquipBonusIds.DEF_SLASH:
				return slashDefBonus;
			case EquipBonusIds.DEF_CRUSH:
				return crushDefBonus;
			case EquipBonusIds.DEF_MAGIC:
				return magicDefBonus;
			case EquipBonusIds.DEF_RANGED:
				return rangedDefBonus;
			case EquipBonusIds.MELEE_STR:
				return strengthBonus;
			case EquipBonusIds.RANGED_STR:
				return rangeStrengthBonus;
			case EquipBonusIds.PRAYER:
				return prayerBonus;
			case EquipBonusIds.MAGIC_STR:
				return magicStrength;
			case EquipBonusIds.SUMMONING:
				return summoning;
			default:
				return 0;
		}
	}

	public int getStabBonus() {
		return stabBonus;
	}

	public void setStabBonus(int stabBonus) {
		this.stabBonus = stabBonus;
	}

	public int getSlashBonus() {
		return slashBonus;
	}

	public void setSlashBonus(int slashBonus) {
		this.slashBonus = slashBonus;
	}

	public int getCrushBonus() {
		return crushBonus;
	}

	public void setCrushBonus(int crushBonus) {
		this.crushBonus = crushBonus;
	}

	public int getMagicAccBonus() {
		return magicAccBonus;
	}

	public void setMagicAccBonus(int magicAccBonus) {
		this.magicAccBonus = magicAccBonus;
	}

	public int getRangedAccBonus() {
		return rangedAccBonus;
	}

	public void setRangedAccBonus(int rangedAccBonus) {
		this.rangedAccBonus = rangedAccBonus;
	}

	public int getStabDefBonus() {
		return stabDefBonus;
	}

	public void setStabDefBonus(int stabDefBonus) {
		this.stabDefBonus = stabDefBonus;
	}

	public int getSlashDefBonus() {
		return slashDefBonus;
	}

	public void setSlashDefBonus(int slashDefBonus) {
		this.slashDefBonus = slashDefBonus;
	}

	public int getCrushDefBonus() {
		return crushDefBonus;
	}

	public void setCrushDefBonus(int crushDefBonus) {
		this.crushDefBonus = crushDefBonus;
	}

	public int getMagicDefBonus() {
		return magicDefBonus;
	}

	public void setMagicDefBonus(int magicDefBonus) {
		this.magicDefBonus = magicDefBonus;
	}

	public int getRangedDefBonus() {
		return rangedDefBonus;
	}

	public void setRangedDefBonus(int rangedDefBonus) {
		this.rangedDefBonus = rangedDefBonus;
	}

	public int getStrengthBonus() {
		return strengthBonus;
	}

	public void setStrengthBonus(int strengthBonus) {
		this.strengthBonus = strengthBonus;
	}

	public int getRangeStrengthBonus() {
		return rangeStrengthBonus;
	}

	public void setRangeStrengthBonus(int rangeStrengthBonus) {
		this.rangeStrengthBonus = rangeStrengthBonus;
	}

	public int getMagicStrengthBonus() {
		return magicStrength;
	}

	public void setMagicStrengthBonus(int attackBonus) {
		this.magicStrength = attackBonus;
	}

	public boolean isPoisonImmunity() {
		return poisonImmunity;
	}

	public void setPoisonImmunity(boolean poisonImmunity) {
		this.poisonImmunity = poisonImmunity;
	}

	public boolean isVenomImmunity() {
		return venomImmunity;
	}

	public void setVenomImmunity(boolean venomImmunity) {
		this.venomImmunity = venomImmunity;
	}

	public int getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(int attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public int getPrayerBonus() {
		return prayerBonus;
	}

	public void setPrayerBonus(int prayerBonus) {
		this.prayerBonus = prayerBonus;
	}
	
	public int getSummoning() {
		return summoning;
	}

	public void setSummoning(int summoning) {
		this.summoning = summoning;
	}

	public int getAttackStr() {
		return attackStr;
	}

	public void setAttackStrength(int attackStr) {
		this.attackStr = attackStr;
	}

	public boolean equalStats(CombatStats stat2) {
		return (this.stabBonus == stat2.getStabBonus() &&
		this.slashBonus == stat2.getSlashBonus() &&
		this.crushBonus == stat2.getCrushBonus() &&
		this.magicAccBonus == stat2.getMagicAccBonus() &&
		this.rangedAccBonus == stat2.getRangedAccBonus() &&
		this.stabDefBonus == stat2.getStabDefBonus() &&
		this.slashDefBonus == stat2.getSlashDefBonus() &&
		this.crushDefBonus == stat2.getCrushDefBonus() &&
		this.magicDefBonus == stat2.getMagicDefBonus() &&
		this.rangedDefBonus == stat2.getRangedDefBonus() &&
		this.strengthBonus == stat2.getStrengthBonus() &&
		this.rangeStrengthBonus == stat2.getRangeStrengthBonus() &&
		this.magicStrength == stat2.getMagicStrengthBonus() &&
		this.prayerBonus == stat2.getPrayerBonus());// &&
		//this.attackSpeed == stat2.getAttackSpeed());
	}
	
	public int equalStatsCount(CombatStats stat2) {
		int count = 0;
		if (this.stabBonus != stat2.getStabBonus())
			count++;
		if (this.slashBonus != stat2.getSlashBonus())
			count++;
		if (this.crushBonus != stat2.getCrushBonus())
			count++;
		if (this.magicAccBonus != stat2.getMagicAccBonus())
			count++;
		if (this.rangedAccBonus != stat2.getRangedAccBonus())
			count++;
		if (this.stabDefBonus != stat2.getStabDefBonus())
			count++;
		if (this.slashDefBonus != stat2.getSlashDefBonus())
			count++;
		if (this.crushDefBonus != stat2.getCrushDefBonus())
			count++;
		if (this.magicDefBonus != stat2.getMagicDefBonus())
			count++;
		if (this.rangedDefBonus != stat2.getRangedDefBonus())
			count++;
		if (this.strengthBonus != stat2.getStrengthBonus())
			count++;
		if (this.rangeStrengthBonus != stat2.getRangeStrengthBonus())
			count++;
		if (this.magicStrength != stat2.getMagicStrengthBonus())
			count++;
		if (this.prayerBonus != stat2.getPrayerBonus())
			count++;
		return count;
	}
	
	public void copyStats(CombatStats stat2) {
		this.stabBonus = stat2.getStabBonus();
		this.slashBonus = stat2.getSlashBonus();
		this.crushBonus = stat2.getCrushBonus();
		this.magicAccBonus = stat2.getMagicAccBonus();
		this.rangedAccBonus = stat2.getRangedAccBonus();
		this.stabDefBonus = stat2.getStabDefBonus();
		this.slashDefBonus = stat2.getSlashDefBonus();
		this.crushDefBonus = stat2.getCrushDefBonus();
		this.magicDefBonus = stat2.getMagicDefBonus();
		this.rangedDefBonus = stat2.getRangedDefBonus();
		this.strengthBonus = stat2.getStrengthBonus();
		this.rangeStrengthBonus = stat2.getRangeStrengthBonus();
		this.magicStrength = stat2.getMagicStrengthBonus();
		this.prayerBonus = stat2.getPrayerBonus();
		//this.attackSpeed = stat2.getAttackSpeed();
		this.summoning = stat2.getSummoning();
	}

	public void copy(CombatStats base) {
		this.stabBonus = base.getStabBonus();
		this.slashBonus = base.getSlashBonus();
		this.crushBonus = base.getCrushBonus();
		this.magicAccBonus = base.getMagicAccBonus();
		this.rangedAccBonus = base.getRangedAccBonus();
		this.stabDefBonus = base.getStabDefBonus();
		this.slashDefBonus = base.getSlashDefBonus();
		this.crushDefBonus = base.getCrushDefBonus();
		this.magicDefBonus = base.getMagicDefBonus();
		this.rangedDefBonus = base.getRangedDefBonus();
		this.strengthBonus = base.getStrengthBonus();
		this.rangeStrengthBonus = base.getRangeStrengthBonus();
		this.magicStrength = base.getMagicStrengthBonus();
		this.prayerBonus = base.getPrayerBonus();
		this.summoning = base.getSummoning();
		this.poisonImmunity = base.isPoisonImmunity();
		this.venomImmunity = base.isVenomImmunity();
		this.attackStr = base.getAttackStr();
		this.attackSpeed = base.getAttackSpeed();
	}

	@Override
	public String toString() {
		return "CombatStats [stabBonus=" + stabBonus + ", slashBonus=" + slashBonus + ", crushBonus=" + crushBonus + ", magicAccBonus=" + magicAccBonus + ", rangedAccBonus=" + rangedAccBonus + ", stabDefBonus=" + stabDefBonus + ", slashDefBonus=" + slashDefBonus + ", crushDefBonus=" + crushDefBonus + ", magicDefBonus=" + magicDefBonus + ", rangedDefBonus=" + rangedDefBonus + ", strengthBonus="
				+ strengthBonus + ", rangeStrengthBonus=" + rangeStrengthBonus + ", magicStrength=" + magicStrength + ", prayerBonus=" + prayerBonus + ", summoning=" + summoning + ", poisonImmunity=" + poisonImmunity + ", venomImmunity=" + venomImmunity + ", attackSpeed=" + attackSpeed + "]";
	}

}
