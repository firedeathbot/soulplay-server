package com.soulplay.content.combat.data;

public enum FightExp {
	NONE,
	MELEE_ATTACK,
	MELEE_STRENGTH,
	MELEE_DEFENSE,
	MELEE_SHARED,
	RANGED,
	RANGED_DEFENSE,
	MAGIC;
}
