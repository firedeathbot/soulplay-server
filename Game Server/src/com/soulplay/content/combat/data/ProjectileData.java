package com.soulplay.content.combat.data;

import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;

public class ProjectileData {

	public static int getStartDelay(Client c, int dist) {
	
		if (c.getCombat().usingChins() || c.playerEquipment[PlayerConstants.playerWeapon] == 6522) {
			return 17 + dist;
		}
	
		String weaponName = ItemConstants
				.getItemName(c.playerEquipment[PlayerConstants.playerWeapon])
				.toLowerCase();
		if (weaponName.contains("knife")) {
			return 18 + dist;
		} else if (weaponName.contains("dart")) {
			return 18 + dist;
		}
	
		return 53 - dist;
	}

	public static int getProjectileSpeed(Client c, int dist) {

		switch (c.playerEquipment[3]) {
			case 15241:
			case 24145: // eggsterminator	
				return 5 * dist + 31;
			
			case 30023:// ballista
			case 30024:// ballista
				return 5 * dist + 30;
			case 10033:
			case 10034:
			case 30356:
				return 60;
			case 13883:
				return 5 * dist + 80;
		}
	
		final String weaponName = ItemConstants
				.getItemName(c.playerEquipment[PlayerConstants.playerWeapon])
				.toLowerCase();
		if (weaponName.contains("knife")) {
			return 5 * dist + 25;
		} else if (weaponName.contains("dart")) {
			return 5 * dist + 25;
		}
	
		return 5 * dist + 55;
	
	}

}
