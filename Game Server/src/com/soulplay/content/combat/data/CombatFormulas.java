package com.soulplay.content.combat.data;

import com.soulplay.content.items.RangeWeapon;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Mob;

/**
 * @author Rhubarb
 */
public class CombatFormulas {
	
	public static int getCombatDistanceToSet(Player c) {
		if (!c.usingSpecial
				&& (c.getAutoCastSpell() != null || c.getSingleCastSpell() != null)) {
			
			if (c.getAutoCastSpell() != null) {
				switch(c.getAutoCastSpell()) {
				case TRIDENT_OF_THE_SEA:
				case TRIDENT_OF_THE_SWAMP:
				case DAWNBRINGER:
				case SANGUINESTI_STAFF:
					return 7;

				default:
					break;
				}
			}
			
			return RSConstants.COMBAT_SPELL_DIST;
		}
		RangeWeapon rw = RangeWeapon.get(c.playerEquipment[PlayerConstants.playerWeapon]);
		if (rw != null) {
			return rw.getCombatDistance();
		}

		ItemDefinition itemDef = ItemDefinition.forId(c.playerEquipment[PlayerConstants.playerWeapon]);
		if (itemDef != null && itemDef.getName().endsWith("halberd")) {
			return 2;
		}

		return 1;
	}
	
	public static int getRangeDistReq(Player c) {
		if (!c.usingSpecial
				&& (c.getAutoCastSpell() != null || c.getSingleCastSpell() != null)) {
			
			if (c.getAutoCastSpell() != null) {
				switch(c.getAutoCastSpell()) {
				case TRIDENT_OF_THE_SEA:
				case TRIDENT_OF_THE_SWAMP:
				case DAWNBRINGER:
				case SANGUINESTI_STAFF:
					return 7;

				default:
					break;
				}
			}
			
			return RSConstants.COMBAT_SPELL_DIST;
		}
		if (c.toggledDfs) {
			return RSConstants.DRAGONFIRE_SHIELD;
		}
		return c.getCombatDistance();
	}

	/**
	 * Is the player in the diagonal block: formula.
	 *
	 * @param attacked
	 * @param attacker
	 * @return
	 */
	public final static boolean isInDiagonalBlock(Client attacked,
			Client attacker) {
		return attacked.absX - 1 == attacker.absX
				&& attacked.absY + 1 == attacker.absY
				|| attacker.absX - 1 == attacked.absX
						&& attacker.absY + 1 == attacked.absY
				|| attacked.absX + 1 == attacker.absX
						&& attacked.absY - 1 == attacker.absY
				|| attacker.absX + 1 == attacked.absX
						&& attacker.absY - 1 == attacked.absY
				|| attacked.absX + 1 == attacker.absX
						&& attacked.absY + 1 == attacker.absY
				|| attacker.absX + 1 == attacked.absX
						&& attacker.absY + 1 == attacked.absY;
	}

	/**
	 * Stops the diagonal attack, if they're in that region.
	 *
	 * @param attacked
	 * @param attacker
	 */
	public static void stopDiagonalAttack(Client attacked, Client attacker) {
		if (attacker.getFreezeTimer() > 0) {
			attacker.sendMessage("A magical force stops you from moving.");
			attacker.getCombat().resetPlayerAttack();
		} else {
			int xMove = attacked.getX() - attacker.getX();
			int yMove = 0;
			if (xMove == 0) {
				yMove = attacked.getY() - attacker.getY();
			}
			int k = attacker.getX() + xMove;
			k -= attacker.mapRegionX * 8;
			attacker.getMovement().getNewWalkCmdX()[0] = attacker.getMovement().getNewWalkCmdY()[0] = 0;
			int l = attacker.getY() + yMove;
			l -= attacker.mapRegionY * 8;
			for (int n = 0; n < attacker.getMovement().newWalkCmdSteps; n++) {
				attacker.getMovement().getNewWalkCmdX()[n] += k;
				attacker.getMovement().getNewWalkCmdY()[n] += l;
			}
		}
	}
	
	public static boolean consideredForHit(Mob e) {
		return e != null && e.isActive() && e.isVisible() && !e.isDead() && e.getSkills().getLifepoints() > 0;
	}
}
