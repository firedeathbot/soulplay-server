package com.soulplay.content.combat.data;

import com.soulplay.content.player.skills.slayer.Assignment;
import com.soulplay.content.player.skills.slayer.Master;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;

public class SlayerHelm {

	public static boolean wearingSlayerHelmFull(Player c) {
		int helmId = c.playerEquipment[PlayerConstants.playerHat];
		return isSlayerHelmFull(helmId);
	}

	public static boolean wearingSlayerHelmFullOrNormal(Player c) {
		int helmId = c.playerEquipment[PlayerConstants.playerHat];
		return wearingSlayerHelmFullOrNormal(helmId);
	}

	public static boolean wearingSlayerHelmFullOrNormal(int helmId) {
		return isSlayerHelm(helmId) || isSlayerHelmFull(helmId);
	}

	private static boolean isSlayerHelm(int id) {
		switch (id) {
			case 13263:
			case 14636:
			case 14637:
			case 30070:
			case 30072:
			case 30074:
			case 30076:
			case 30330:
			case 124370:
			case 123073:
				return true;
		}

		return false;
	}

	private static boolean isSlayerHelmFull(int id) {
		switch (id) {
			case 15492:
			case 15496:
			case 15497:
			case 30071:
			case 30073:
			case 30075:
			case 30077:
			case 30331:
			case 124444:
			case 123075:
				return true;
		}

		return false;
	}

	public static double slayerHelmBoost(Player c) {
		if (c.npcIndex > 0 && c.getSlayerTask() != null && c.getSlayerTask().getAssignment() != null) {
			if (c.getSlayerTask().getSlayerMaster() == Master.KRYSTILIA && c.wildLevel <= 0) {
				return 1.0f;
			}

			NPC npc = NPCHandler.npcs[c.npcIndex];
			if (npc != null && npc.isActive) {
				for (int id : c.getSlayerTask().getAssignment().getIds()) {
					if (npc.npcType == id) {
						return 1.15f;
					}
				}
			}
		}

		return 1.0f;
	}

	public static float slayerHelmBoost(Player player, NPC npc) {
		SlayerTask slayerTask = player.getSlayerTask();
		if (slayerTask == null) {
			return 0.0f;
		}

		Assignment assignment = slayerTask.getAssignment();
		if (assignment == null) {
			return 0.0f;
		}

		if (slayerTask.getSlayerMaster() == Master.KRYSTILIA && player.wildLevel <= 0) {
			return 0.0f;
		}

		if (npc != null && npc.isActive) {
			for (int id : assignment.getIds()) {
				if (npc.npcType == id) {
					return 0.15f;
				}
			}
		}

		return 0.0f;
	}

}
