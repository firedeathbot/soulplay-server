package com.soulplay.content;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import com.soulplay.Config;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.npcs.impl.bosses.nex.NexEvent;
import com.soulplay.content.player.DonatorRank;
import com.soulplay.content.player.TeleportType;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public class TeleportInterface {
	
	private static class Teleport {
		private final int x;
		private final int y;
		private final int z;
		private final int type;
		private final Predicate<Player> canTeleport;

		public Teleport(int x, int y, int z, int type, Predicate<Player> canTeleport) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.type = type;
			this.canTeleport = canTeleport;
		}

		public Teleport(int x, int y, int z, int type) {
			this(x, y, z, type, null);
		}

		public Teleport(int x, int y, int z) {
			this(x, y, z, 0, null);
		}

		public Teleport(int x, int y, int z, Predicate<Player> canTeleport) {
			this(x, y, z, 0, canTeleport);
		}

	}
	
	public static final int[] layers = {25653, 25853, 26053, 26253, 26453, 26653, 26853, 27053, 27253, 27453, 27753, 28253, 28453,
			28753, 28953, 29153, 29253, 29453};
	public static final Map<Integer, Teleport> teleports = new HashMap<>();
	
	public static boolean handleButton(Client c, int buttonId) {
		Teleport teleport = teleports.get(buttonId);
		if (teleport != null) {
			if (teleport.canTeleport != null && !teleport.canTeleport.test(c)) {
				c.sendMessage("Only donators may use this teleport.");
				return true;
			}

			int x = teleport.x;
			int y = teleport.y;
			int z = teleport.z;
			if (buttonId == 102182 && (NexEvent.hasStarted() || NexEvent.isStarting())) {
				x = 2903;
				y = 5204;
				z = 0;
			}

			// donator minigames
			/*if (!Config.isDonator(c) && buttonId == 103110) {
				c.getDialogueBuilder().sendStatement("This is a donator minigame are you sure you want to continue?")
						.sendOption("Yes", () -> {
							if (teleport.type == 0) {
								c.getPA().startTeleport(teleport.x, teleport.y, teleport.z, TeleportType.MODERN);
							} else if (teleport.type == 1) {
								c.getPA().startTeleport(teleport.x, teleport.y, teleport.z, TeleportType.DUNG);
							}
						}, "No", () -> {

						}).execute();
				return true;
			}*/

			TeleportType teleportType;
			switch (teleport.type) {
				case 1:
					teleportType = TeleportType.DUNG;
					break;
				default:
					teleportType = TeleportType.MODERN;
					break;
			}

			c.getPA().startTeleport(x, y, z, teleportType);
			return true;
		}

		switch (buttonId) {
			case 99110:
				toggleLayers(c, 0);
				return true;
			case 99112:
				toggleLayers(c, 1);
				return true;
			case 99114:
				toggleLayers(c, 2);
				return true;
			case 99116:
				toggleLayers(c, 3);
				return true;
			case 99118:
				toggleLayers(c, 4);
				return true;
			case 99120:
				toggleLayers(c, 5);
				return true;
			case 99122:
				toggleLayers(c, 6);
				return true;
			case 99124:
				toggleLayers(c, 7);
				return true;
			case 99126:
				toggleLayers(c, 8);
				return true;
			case 99128:
				toggleLayers(c, 9);
				return true;
			case 99130:
				toggleLayers(c, 10);
				return true;
			case 99132:
				toggleLayers(c, 11);
				return true;
			case 99134:
				toggleLayers(c, 12);
				return true;
			case 99136:
				toggleLayers(c, 13);
				return true;
			case 99138:
				toggleLayers(c, 14);
				return true;
			case 99140:
				toggleLayers(c, 15);
				return true;
			case 99142:
				toggleLayers(c, 16);
				return true;
			case 99144:
				toggleLayers(c, 17);
				return true;
		}

		return false;
	}
	
	public static void toggleLayers(Player c, int type) {
		c.lastTeleportsTab = type;
		for (int i = 0; i < layers.length; i++) {
			if (i == type) {
				c.getPacketSender().setVisibility(layers[i], false);
			} else {
				c.getPacketSender().setVisibility(layers[i], true);
			}
		}
		
		switch (type) {
		case 6: //skilling
			c.getPacketSender().setScrollPos(25453, 149);
			break;
			
//			default:
//				c.getPacketSender().setScrollPos(25453, 0);
//				break;
		}
		
		c.getPacketSender().setVisibility(29998, true);//hide the search
	}

    public static void load() {
    	/* empty */
    }

	static {
		//Cities
		teleports.put(100054, new Teleport(LocationConstants.EDGEVILLE.getX(), LocationConstants.EDGEVILLE.getY(), 0));
		teleports.put(100058, new Teleport(LocationConstants.GE.getX(), LocationConstants.GE.getY(), 0));
		teleports.put(100062, new Teleport(LocationConstants.LUMBY_X, LocationConstants.LUMBY_Y, 0));
		teleports.put(100066, new Teleport(LocationConstants.VARROCK_X, LocationConstants.VARROCK_Y, 0));
		teleports.put(100070, new Teleport(LocationConstants.CAMELOT_X, LocationConstants.CAMELOT_Y, 0));
		teleports.put(100074, new Teleport(LocationConstants.FALADOR_X, LocationConstants.FALADOR_Y, 0));
		teleports.put(100078, new Teleport(LocationConstants.ARDOUGNE_X, LocationConstants.ARDOUGNE_Y, 0));
		teleports.put(100082, new Teleport(LocationConstants.DRAYNOR.getX(), LocationConstants.DRAYNOR.getY(), 0));
		teleports.put(100086, new Teleport(LocationConstants.NEITIZNOT.getX(), LocationConstants.NEITIZNOT.getY(), 0));
		teleports.put(100090, new Teleport(LocationConstants.SHILO_VILLAGE.getX(), LocationConstants.SHILO_VILLAGE.getY(), 0));
		teleports.put(100094, new Teleport(LocationConstants.KARAMJA_X, LocationConstants.KARAMJA_Y, 0));
		teleports.put(100098, new Teleport(LocationConstants.YANILLE.getX(), LocationConstants.YANILLE.getY(), 0));
		teleports.put(100102, new Teleport(LocationConstants.LUNAR_ISLE.getX(), LocationConstants.LUNAR_ISLE.getY(), 0));
		teleports.put(100106, new Teleport(LocationConstants.TAI_BWO_WANNAI.getX(), LocationConstants.TAI_BWO_WANNAI.getY(), 0));
		teleports.put(100110, new Teleport(LocationConstants.KELDAGRIM.getX(), LocationConstants.KELDAGRIM.getY(), 0));
		teleports.put(100114, new Teleport(LocationConstants.APE_ATOLL.getX(), LocationConstants.APE_ATOLL.getY(), 0));
		teleports.put(100118, new Teleport(LocationConstants.LAND_OF_SERENITY.getX(), LocationConstants.LAND_OF_SERENITY.getY(), 0, player -> DonatorRank.check(player) != DonatorRank.NONE || Config.isModerator(player) || Config.isStaff(player)));
		
		
		//Monsters
		teleports.put(100254, new Teleport(LocationConstants.ROCK_CRABS.getX(), LocationConstants.ROCK_CRABS.getY(), 0));
		teleports.put(101002, new Teleport(LocationConstants.YAKS.getX(), LocationConstants.YAKS.getY(), 0));
		teleports.put(101006, new Teleport(LocationConstants.EXPERIEMNTS.getX(), LocationConstants.EXPERIEMNTS.getY(), 0));
		teleports.put(101010, new Teleport(LocationConstants.SLAYER_TOWER.getX(), LocationConstants.SLAYER_TOWER.getY(), 0));
		
		//Dungeons
		teleports.put(101198, new Teleport(LocationConstants.TAVERLY_DUNG.getX(), LocationConstants.TAVERLY_DUNG.getY(), 0));
		teleports.put(101202, new Teleport(LocationConstants.BRIMHAVEN_DUNGEON.getX(), LocationConstants.BRIMHAVEN_DUNGEON.getY(), 0));
		teleports.put(101206, new Teleport(LocationConstants.FREMENNIK_DUNGEON.getX(), LocationConstants.FREMENNIK_DUNGEON.getY(), 0));
		teleports.put(101210, new Teleport(LocationConstants.LUMBRIDGE_DUNGEON.getX(), LocationConstants.LUMBRIDGE_DUNGEON.getY(), 0));
		teleports.put(101214, new Teleport(LocationConstants.EDGEVILLE_DUNGEON.getX(), LocationConstants.EDGEVILLE_DUNGEON.getY(), 0));
		teleports.put(101218, new Teleport(LocationConstants.VARROCK_DUNGEON.getX(), LocationConstants.VARROCK_DUNGEON.getY(), 0));
		teleports.put(101222, new Teleport(LocationConstants.KALPHITE_LAIR.getX(), LocationConstants.KALPHITE_LAIR.getY(), LocationConstants.KALPHITE_LAIR.getZ()));
		teleports.put(101226, new Teleport(LocationConstants.ICE_DUNG.getX(), LocationConstants.ICE_DUNG.getY(), 0));
		teleports.put(101230, new Teleport(LocationConstants.DWARVEN_MINES.getX(), LocationConstants.DWARVEN_MINES.getY(), 0));
		teleports.put(101234, new Teleport(LocationConstants.GLACORS.getX(), LocationConstants.GLACORS.getY(), 1));
		teleports.put(101238, new Teleport(LocationConstants.FORBIDDEN_DUNG.getX(), LocationConstants.FORBIDDEN_DUNG.getY(), 0));
		teleports.put(101242, new Teleport(LocationConstants.JUNGLE_DUNG.getX(), LocationConstants.JUNGLE_DUNG.getY(), 0));
		teleports.put(101246, new Teleport(LocationConstants.ANCIENT_CAVERN.getX(), LocationConstants.ANCIENT_CAVERN.getY(), 1));
		teleports.put(101250, new Teleport(LocationConstants.CHAOS_TUNNEL.getX(), LocationConstants.CHAOS_TUNNEL.getY(), 0));
		teleports.put(101254, new Teleport(LocationConstants.SMOKE_DUNG.getX(), LocationConstants.SMOKE_DUNG.getY(), 0));
		teleports.put(102002, new Teleport(LocationConstants.WATERBIRTH_DUNG.getX(), LocationConstants.WATERBIRTH_DUNG.getY(), 0));
		teleports.put(102006, new Teleport(LocationConstants.REVENANTS.getX(), LocationConstants.REVENANTS.getY(), 0));
		teleports.put(102010, new Teleport(LocationConstants.CATACOMBS_OF_KOUREND.getX(), LocationConstants.CATACOMBS_OF_KOUREND.getY(), 0));
		teleports.put(102014, new Teleport(LocationConstants.NIEVES_CAVE.getX(), LocationConstants.NIEVES_CAVE.getY(), 0));
		teleports.put(102018, new Teleport(LocationConstants.LITHKREN_VAULT.getX(), LocationConstants.LITHKREN_VAULT.getY(), 0));
		teleports.put(102022, new Teleport(LocationConstants.JORMUNGANDS_PRISON.getX(), LocationConstants.JORMUNGANDS_PRISON.getY(), 0));
		teleports.put(102026, new Teleport(LocationConstants.KARUULM_SLAYER_DUNGEON.getX(), LocationConstants.KARUULM_SLAYER_DUNGEON.getY(), 0));
		teleports.put(102030, new Teleport(LocationConstants.CRABCLAW_CAVES.getX(), LocationConstants.CRABCLAW_CAVES.getY(), 0));
		teleports.put(102034, new Teleport(LocationConstants.LIZARDMAN_CAVES.getX(), LocationConstants.LIZARDMAN_CAVES.getY(), 0));
		teleports.put(102038, new Teleport(LocationConstants.WYVERN_CAVE.getX(), LocationConstants.WYVERN_CAVE.getY(), 0));
		teleports.put(102042, new Teleport(LocationConstants.LIVING_ROCK_CAVERNS.getX(), LocationConstants.LIVING_ROCK_CAVERNS.getY(), 0));
		teleports.put(102046, new Teleport(LocationConstants.SMOKE_DEVIL_DUNGEON.getX(), LocationConstants.SMOKE_DEVIL_DUNGEON.getY(), 0));
		teleports.put(102050, new Teleport(LocationConstants.KALPHITE_CAVE.getX(), LocationConstants.KALPHITE_CAVE.getY(), 0));
		teleports.put(102054, new Teleport(LocationConstants.CAVE_HORROR.getX(), LocationConstants.CAVE_HORROR.getY(), 0));
		
		
	    //Bosses
		teleports.put(102142, new Teleport(LocationConstants.BOSS_INSTANCE.getX(), LocationConstants.BOSS_INSTANCE.getY(), 0));
		teleports.put(102146, new Teleport(LocationConstants.VORKATH.getX(), LocationConstants.VORKATH.getY(), 0));
		teleports.put(102150, new Teleport(LocationConstants.ZULRAH.getX(), LocationConstants.ZULRAH.getY(), 0));
		teleports.put(102154, new Teleport(LocationConstants.KREE_ARRA.getX(), LocationConstants.KREE_ARRA.getY(), 2));
		teleports.put(102158, new Teleport(LocationConstants.GENERAL_GRAARDOR.getX(), LocationConstants.GENERAL_GRAARDOR.getY(), 2));
		teleports.put(102162, new Teleport(LocationConstants.KRIL_TSUTSAROTH.getX(), LocationConstants.KRIL_TSUTSAROTH.getY(), 2));
		teleports.put(102166, new Teleport(LocationConstants.ZILYANA.getX(), LocationConstants.ZILYANA.getY(), 0));
		teleports.put(102170, new Teleport(LocationConstants.KRAKEN.getX(), LocationConstants.KRAKEN.getY(), 0));
		teleports.put(102174, new Teleport(LocationConstants.CORP.getX(), LocationConstants.CORP.getY(), 0));
		teleports.put(102178, new Teleport(LocationConstants.DAG_KINGS.getX(), LocationConstants.DAG_KINGS.getY(), 0));
		teleports.put(102182, new Teleport(LocationConstants.NEX.getX(), LocationConstants.NEX.getY(), 2));
		teleports.put(102186, new Teleport(LocationConstants.BANDOS_AVATAR.getX(), LocationConstants.BANDOS_AVATAR.getY(), 0));
		teleports.put(102190, new Teleport(LocationConstants.NOMAD.getX(), LocationConstants.NOMAD.getY(), 0));
		teleports.put(102194, new Teleport(LocationConstants.GIANT_MOLE.getX(), LocationConstants.GIANT_MOLE.getY(), 0));
		teleports.put(102198, new Teleport(LocationConstants.BARRELCHEST.getX(), LocationConstants.BARRELCHEST.getY(), 0));
		teleports.put(102202, new Teleport(LocationConstants.KBD.getX(), LocationConstants.KBD.getY(), 0));
		teleports.put(102206, new Teleport(LocationConstants.CHAOS_ELE.getX(), LocationConstants.CHAOS_ELE.getY(), 0));
		teleports.put(102210, new Teleport(LocationConstants.SKELETAL_HORROR.getX(), LocationConstants.SKELETAL_HORROR.getY(), 0));
		teleports.put(102214, new Teleport(LocationConstants.SCORPIA.getX(), LocationConstants.SCORPIA.getY(), 0));
		teleports.put(102218, new Teleport(LocationConstants.CALLISTO.getX(), LocationConstants.CALLISTO.getY(), 0));
		teleports.put(102222, new Teleport(LocationConstants.VENENANTIS.getX(), LocationConstants.VENENANTIS.getY(), 0));
		teleports.put(102226, new Teleport(LocationConstants.TORM.getX(), LocationConstants.TORM.getY(), 0));
		teleports.put(102230, new Teleport(LocationConstants.CHAOS_FANATIC.getX(), LocationConstants.CHAOS_FANATIC.getY(), 0));
		teleports.put(102234, new Teleport(LocationConstants.CRAZY_ARCHEOLOGIST.getX(), LocationConstants.CRAZY_ARCHEOLOGIST.getY(), 0));
		teleports.put(102238, new Teleport(LocationConstants.DERANGED_ARCHEOLOGIST.getX(), LocationConstants.DERANGED_ARCHEOLOGIST.getY(), 0));
		teleports.put(102242, new Teleport(LocationConstants.ALCHEMICAL_HYDRA.getX(), LocationConstants.ALCHEMICAL_HYDRA.getY(), 0));
		teleports.put(102246, new Teleport(LocationConstants.NIGHTMARE_OF_ASHIHAMA.getX(), LocationConstants.NIGHTMARE_OF_ASHIHAMA.getY(), LocationConstants.NIGHTMARE_OF_ASHIHAMA.getZ()));
		teleports.put(102250, new Teleport(LocationConstants.VETION.getX(), LocationConstants.VETION.getY(), 0));
		teleports.put(102254, new Teleport(LocationConstants.KALPHITE_QUEEN.getX(), LocationConstants.KALPHITE_QUEEN.getY(), LocationConstants.KALPHITE_QUEEN.getZ()));
		teleports.put(103002, new Teleport(LocationConstants.LIZARD_SHAMAN.getX(), LocationConstants.LIZARD_SHAMAN.getY(), LocationConstants.LIZARD_SHAMAN.getZ()));
		
	    //Minigames
		teleports.put(103086, new Teleport(LocationConstants.GAMBLE_ZONE.getX(), LocationConstants.GAMBLE_ZONE.getY(), 0));
		teleports.put(103090, new Teleport(LocationConstants.LMS.getX(), LocationConstants.LMS.getY(), 0));
		teleports.put(103094, new Teleport(LocationConstants.DUEL_ARENA.getX(), LocationConstants.DUEL_ARENA.getY(), 0));
		teleports.put(103098, new Teleport(LocationConstants.BARROWS.getX(), LocationConstants.BARROWS.getY(), 0));
		teleports.put(103102, new Teleport(LocationConstants.TZHAAR.getX(), LocationConstants.TZHAAR.getY(), 0));
		teleports.put(103106, new Teleport(LocationConstants.WARRIORS_GUILD.getX(), LocationConstants.WARRIORS_GUILD.getY(), 0));
		teleports.put(103110, new Teleport(LocationConstants.CASTLE_WARS.getX(), LocationConstants.CASTLE_WARS.getY(), 0));
		teleports.put(103114, new Teleport(LocationConstants.PEST_CONTROL.getX(), LocationConstants.PEST_CONTROL.getY(), 0));
		teleports.put(103118, new Teleport(LocationConstants.SOULWARS.getX(), LocationConstants.SOULWARS.getY(), 0));
		teleports.put(103122, new Teleport(LocationConstants.WILDERNESS_TREASURE.getX(), LocationConstants.WILDERNESS_TREASURE.getY(), 0));
		teleports.put(103126, new Teleport(LocationConstants.FISHING_CONTEST.getX(), LocationConstants.FISHING_CONTEST.getY(), 0));
		teleports.put(103130, new Teleport(BarbarianAssault.LOBBY.getX(), BarbarianAssault.LOBBY.getY(), 0));
		teleports.put(103134, new Teleport(LocationConstants.FIGHT_PITS.getX(), LocationConstants.FIGHT_PITS.getY(), 0));
		teleports.put(103138, new Teleport(LocationConstants.FUNPK.getX(), LocationConstants.FUNPK.getY(), 0));
		teleports.put(103142, new Teleport(LocationConstants.CLEANUP.getX(), LocationConstants.CLEANUP.getY(), 0));
		teleports.put(103146, new Teleport(LocationConstants.PENGUIN_HUNT.getX(), LocationConstants.PENGUIN_HUNT.getY(), 0));
		teleports.put(103150, new Teleport(LocationConstants.CHAMBER_OF_XERIC.getX(), LocationConstants.CHAMBER_OF_XERIC.getY(), 0));
		teleports.put(103154, new Teleport(LocationConstants.INFERNO.getX(), LocationConstants.INFERNO.getY(), 0));
		teleports.put(103158, new Teleport(LocationConstants.THEATRE_OF_BLOOD.getX(), LocationConstants.THEATRE_OF_BLOOD.getY(), 0));
		
		//Wilderness
		teleports.put(104030, new Teleport(LocationConstants.EDGE_PVP.getX(), LocationConstants.EDGE_PVP.getY(), 0));
		teleports.put(104034, new Teleport(LocationConstants.MAGE_BANK.getX(), LocationConstants.MAGE_BANK.getY(), 0));
		teleports.put(104038, new Teleport(LocationConstants.CLAN_WARS.getX(), LocationConstants.CLAN_WARS.getY(), 0));
		teleports.put(104042, new Teleport(LocationConstants.WEST_DRAGONS.getX(), LocationConstants.WEST_DRAGONS.getY(), 0));
		teleports.put(104046, new Teleport(LocationConstants.HILL_GIANTS_WILDY.getX(), LocationConstants.HILL_GIANTS_WILDY.getY(), 0));
		teleports.put(104050, new Teleport(LocationConstants.DEMONIC_GORILLAS.getX(), LocationConstants.DEMONIC_GORILLAS.getY(), 0));
		teleports.put(104054, new Teleport(LocationConstants.LAVA_DRAGONS.getX(), LocationConstants.LAVA_DRAGONS.getY(), 0));
		teleports.put(104058, new Teleport(LocationConstants.ELDER_CHAOS_DRUIDS.getX(), LocationConstants.ELDER_CHAOS_DRUIDS.getY(), 0));
		
		//Slayer
		teleports.put(104230, new Teleport(LocationConstants.SLAYER_TURAEL.getX(), LocationConstants.SLAYER_TURAEL.getY(), 0));
		teleports.put(104234, new Teleport(LocationConstants.KRYSTILIA.getX(), LocationConstants.KRYSTILIA.getY(), 0));
		teleports.put(104238, new Teleport(LocationConstants.SLAYER_MAZCHNA.getX(), LocationConstants.SLAYER_MAZCHNA.getY(), 0));
		teleports.put(104242, new Teleport(LocationConstants.SLAYER_VANNAKA.getX(), LocationConstants.SLAYER_VANNAKA.getY(), 0));
		teleports.put(104246, new Teleport(LocationConstants.SLAYER_CHAELDAR.getX(), LocationConstants.SLAYER_CHAELDAR.getY(), 0));
		teleports.put(104250, new Teleport(LocationConstants.KONAR_QUO_MATEN.getX(), LocationConstants.KONAR_QUO_MATEN.getY(), LocationConstants.KONAR_QUO_MATEN.getZ()));
		teleports.put(104254, new Teleport(LocationConstants.NIEVE.getX(), LocationConstants.NIEVE.getY(), LocationConstants.NIEVE.getZ()));
		teleports.put(105002, new Teleport(LocationConstants.SLAYER_SUMONA.getX(), LocationConstants.SLAYER_SUMONA.getY(), 0));
		teleports.put(105006, new Teleport(LocationConstants.SLAYER_DURADEL.getX(), LocationConstants.SLAYER_DURADEL.getY(), LocationConstants.SLAYER_DURADEL.getZ()));
		teleports.put(105010, new Teleport(LocationConstants.SLAYER_KURADAL.getX(), LocationConstants.SLAYER_KURADAL.getY(), 1));
		teleports.put(105014, new Teleport(LocationConstants.SLAYER_MORVAN.getX(), LocationConstants.SLAYER_MORVAN.getY(), 0));
		
	    //Fishing
		teleports.put(105174, new Teleport(LocationConstants.FISHING_CATHERBY.getX(), LocationConstants.FISHING_CATHERBY.getY(), 0));
		teleports.put(105178, new Teleport(LocationConstants.FISHING_GUILD.getX(), LocationConstants.FISHING_GUILD.getY(), 0));
		teleports.put(105182, new Teleport(LocationConstants.FISHING_KARAMBWAN.getX(), LocationConstants.FISHING_KARAMBWAN.getY(), 0));
		teleports.put(105186, new Teleport(LocationConstants.RESOURCE_AREA.getX(), LocationConstants.RESOURCE_AREA.getY(), 0));
		teleports.put(105190, new Teleport(LocationConstants.FISHING_PORT_PISCARILIUS.getX(), LocationConstants.FISHING_PORT_PISCARILIUS.getY(), 0));
		
	    //Mining
		teleports.put(106118, new Teleport(LocationConstants.DWARVEN_MINES.getX(), LocationConstants.DWARVEN_MINES.getY(), 0));
		teleports.put(106122, new Teleport(LocationConstants.MINING_NEITIZNOT.getX(), LocationConstants.MINING_NEITIZNOT.getY(), 0));
		teleports.put(106126, new Teleport(LocationConstants.MINING_CRAFTING_GUILD.getX(), LocationConstants.MINING_CRAFTING_GUILD.getY(), 0));
		teleports.put(106130, new Teleport(LocationConstants.LIMESTONE_MINE.getX(), LocationConstants.LIMESTONE_MINE.getY(), 0));
		teleports.put(106134, new Teleport(LocationConstants.GEM_MINE.getX(), LocationConstants.GEM_MINE.getY(), 0));
		teleports.put(106138, new Teleport(LocationConstants.DORGESH_KAAN_MINE.getX(), LocationConstants.DORGESH_KAAN_MINE.getY(), 0));
		
		//Woodcutting
		teleports.put(107062, new Teleport(LocationConstants.WOODCUTTING_DRAYNOR.getX(), LocationConstants.WOODCUTTING_DRAYNOR.getY(), 0));
		teleports.put(107066, new Teleport(LocationConstants.WOODCUTTING_CAMELOT.getX(), LocationConstants.WOODCUTTING_CAMELOT.getY(), 0));
		teleports.put(107070, new Teleport(LocationConstants.WOODCUTTING_MAHOGANY.getX(), LocationConstants.WOODCUTTING_MAHOGANY.getY(), 0));
		teleports.put(107074, new Teleport(LocationConstants.RESOURCE_AREA.getX(), LocationConstants.RESOURCE_AREA.getY(), 0));
		teleports.put(107078, new Teleport(LocationConstants.WOODCUTTING_FALLY_IVY.getX(), LocationConstants.WOODCUTTING_FALLY_IVY.getY(), 0));
		teleports.put(107082, new Teleport(LocationConstants.WOODCUTTING_GUILD.getX(), LocationConstants.WOODCUTTING_GUILD.getY(), 0));
	
	    //Agility
		teleports.put(108106, new Teleport(LocationConstants.AGILITY_GNOME.getX(), LocationConstants.AGILITY_GNOME.getY(), 0));	
		teleports.put(108110, new Teleport(LocationConstants.AGILITY_BARB.getX(), LocationConstants.AGILITY_BARB.getY(), 0));
		teleports.put(108114, new Teleport(LocationConstants.AGILITY_WILDY.getX(), LocationConstants.AGILITY_WILDY.getY(), 0));
		
	    //Theiving
		teleports.put(110094, new Teleport(LocationConstants.THIEVING_EDGE.getX(), LocationConstants.THIEVING_EDGE.getY(), 0));
		teleports.put(110098, new Teleport(LocationConstants.THIEVING_VARROCK.getX(), LocationConstants.THIEVING_VARROCK.getY(), 0));
		teleports.put(110102, new Teleport(LocationConstants.THIEVING_ARDOUGEN.getX(), LocationConstants.THIEVING_ARDOUGEN.getY(), 0));
		
	    //Hunter
		teleports.put(111038, new Teleport(LocationConstants.HUNTER.getX(), LocationConstants.HUNTER.getY(), 0));
		teleports.put(111042, new Teleport(LocationConstants.GRAY_CHINS.getX(), LocationConstants.GRAY_CHINS.getY(), 0));
		teleports.put(111046, new Teleport(LocationConstants.RED_CHINS.getX(), LocationConstants.RED_CHINS.getY(), 0));
		teleports.put(111050, new Teleport(LocationConstants.BLACK_CHINS.getX(), LocationConstants.BLACK_CHINS.getY(), 0));
		
		//Farming
		teleports.put(112082, new Teleport(LocationConstants.FARMING_CATHERBY.getX(), LocationConstants.FARMING_CATHERBY.getY(), 0));
		teleports.put(112086, new Teleport(LocationConstants.FARMING_FALADOR.getX(), LocationConstants.FARMING_FALADOR.getY(), 0));
		teleports.put(112090, new Teleport(LocationConstants.FARMING_CANIFIS.getX(), LocationConstants.FARMING_CANIFIS.getY(), 0));
		teleports.put(112094, new Teleport(LocationConstants.FARMING_ARDOUGNE.getX(), LocationConstants.FARMING_ARDOUGNE.getY(), 0));
		teleports.put(112098, new Teleport(LocationConstants.VARROCK_TREE_FARM.getX(), LocationConstants.VARROCK_TREE_FARM.getY(), 0));
		teleports.put(112102, new Teleport(LocationConstants.LUMBRDGE_TREE_FARM.getX(), LocationConstants.LUMBRDGE_TREE_FARM.getY(), 0));
		teleports.put(112106, new Teleport(LocationConstants.FALADOR_TREE_FARM.getX(), LocationConstants.FALADOR_TREE_FARM.getY(), 0));
		teleports.put(112110, new Teleport(LocationConstants.TAVERLY_TREE_FARM.getX(), LocationConstants.TAVERLY_TREE_FARM.getY(), 0));
		
	    //Runecrafting
		teleports.put(113026, new Teleport(LocationConstants.RUNECRAFTING_EDGE.getX(), LocationConstants.RUNECRAFTING_EDGE.getY(), 0));
	      //Summoning6
		teleports.put(113226, new Teleport(LocationConstants.SUMMONING.getX(), LocationConstants.SUMMONING.getY(), 0));
		
		//Dungeoneering
		teleports.put(114070, new Teleport(LocationConstants.DAEMONHEIM.getX(), LocationConstants.DAEMONHEIM.getY(), 0, 1));
	
	    //Construction
		teleports.put(115014, new Teleport(LocationConstants.CONSTRUCTION.getX(), LocationConstants.CONSTRUCTION.getY(), 0));
	}

}
