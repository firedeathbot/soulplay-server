package com.soulplay.content.instances;

import java.util.function.Consumer;

import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.InstanceManager;

public enum InstanceType {

	KBD("King black dragon", 0, 9033, Location.create(31, 8), instance -> {
		instance.spawnNpc(50, 32, 28);
	}),
	BANDOS("General graardor", 1, 11347, Location.create(48, 42, 2), instance -> {
		instance.spawnNpc(6260, 54, 52, 2);
		instance.spawnNpc(6261, 51, 50, 2);
		instance.spawnNpc(6263, 58, 49, 2);
		instance.spawnNpc(6265, 54, 48, 2);
	}),
	ARMADYL("Kree'arra", 2, 11346, Location.create(23, 48, 2), instance -> {
		instance.spawnNpc(6222, 14, 54, 2);
		instance.spawnNpc(6223, 16, 51, 2);
		instance.spawnNpc(6225, 17, 58, 2);
		instance.spawnNpc(6227, 18, 54, 2);
	}),
	ZAMORAK("Kril'Tsutsaroth", 3, 11603, Location.create(45, 19, 2), instance -> {
		instance.spawnNpc(6203, 49, 11, 2);
		instance.spawnNpc(6204, 51, 15, 2);
		instance.spawnNpc(6206, 43, 9, 2);
		instance.spawnNpc(6208, 47, 13, 2);
	}),
	SARADOMIN("Commander Zilyana", 4, 11602, Location.create(27, 17), instance -> {
		instance.spawnNpc(6247, 17, 16);
		instance.spawnNpc(6248, 19, 20);
		instance.spawnNpc(6250, 20, 12);
		instance.spawnNpc(6252, 21, 17);
	}),
	DAG_KINGS("Dagannoth Kings", 5, 11589, Location.create(20, 33), instance -> {
		instance.spawnNpc(2881, 31, 25);
		instance.spawnNpc(2882, 33, 38);
		instance.spawnNpc(2883, 42, 29);
	}),
	NEX("Nex", 6, 11601, Location.create(20, 20), true, instance -> {
		instance.spawnNpc(13447, 45, 19);
		instance.spawnNpc(13451, 32, 32);
		instance.spawnNpc(13452, 57, 32);
		instance.spawnNpc(13453, 57, 6);
		instance.spawnNpc(13454, 32, 6);
	}),
	BARRELCHEST("Barrelchest", 7, 15148, Location.create(31, 28), instance -> {
		instance.spawnNpc(5666, 39, 28);
	}),
	CHAOS_ELEMENTAL("Chaos Elemental", 8, 13117, Location.create(15, 1), instance -> {
		instance.spawnNpc(3200, 11, 12);
	}),
	SCORPIA("Scorpia", 9, 38561, Location.create(33, 28), instance -> {
		instance.spawnNpc(4172, 32, 41);		
		instance.spawnNpc(109, 44, 31);
		instance.spawnNpc(109, 43, 35);
		instance.spawnNpc(109, 41, 35);
		instance.spawnNpc(109, 43, 40);
		instance.spawnNpc(109, 40, 44);
		instance.spawnNpc(109, 36, 43);
		instance.spawnNpc(109, 35, 38);
		instance.spawnNpc(109, 35, 34);
		instance.spawnNpc(109, 39, 32);
		instance.spawnNpc(109, 41, 39);
		instance.spawnNpc(109, 37, 29);
		instance.spawnNpc(109, 35, 36);
		instance.spawnNpc(109, 34, 40);
		instance.spawnNpc(109, 30, 45);
		instance.spawnNpc(109, 25, 44);
		instance.spawnNpc(109, 21, 43);
		instance.spawnNpc(109, 22, 37);
		instance.spawnNpc(109, 23, 33);
		instance.spawnNpc(109, 23, 29);
		instance.spawnNpc(109, 27, 32);
		instance.spawnNpc(109, 29, 29);
		instance.spawnNpc(109, 30, 35);
		instance.spawnNpc(109, 28, 42);
		instance.spawnNpc(109, 32, 36);
		instance.spawnNpc(109, 28, 39);
		instance.spawnNpc(109, 26, 35);
		instance.spawnNpc(109, 37, 37);
	}),
	CALLISTO("Callisto", 10, 13116, Location.create(17, 11), instance -> {
		instance.spawnNpc(4174, 29, 10);
	}),
	VENENATIS("Venenatis", 11, 13370, Location.create(12, 11), instance -> {
		instance.spawnNpc(4173, 12, 28);
	}),
	TORM("Tormented Demon", 12, 10329, Location.create(34, 32), instance -> {
		instance.spawnNpc(8349, 32, 28);
		instance.spawnNpc(8349, 42, 39);
		instance.spawnNpc(8349, 43, 18);
	}),
	CHAOS_FANATIC("Chaos Fanatic", 13, 11836, Location.create(36, 17), instance -> {
		instance.spawnNpc(4190, 36, 5);
	}),
	CRAZY_ARCHEOLOGIST("Crazy Archeologist", 14, 11833, Location.create(13, 43), instance -> {
		instance.spawnNpc(4186, 23, 48);
	}),
	DERANGED_ARCHEOLOGIST("Deranged Archeologist", 15, 40249, Location.create(30, 63), instance -> {
		instance.spawnNpc(107806, 36, 57);
	}),
	KALPHITE_QUEEN("Kalphite Queen", 16, 13972, Location.create(52, 21), instance -> {
		instance.spawnNpc(1158, 24, 19);
	}),
	LIZARD_SHAMANS("Lizard Shaman", 17, 30875, Location.create(25, 53), instance -> {
		instance.spawnNpc(107745, 28, 30);
		instance.spawnNpc(107745, 24, 27);
		instance.spawnNpc(107745, 11, 38);
		instance.spawnNpc(107745, 7, 35);
		instance.spawnNpc(107745, 39, 29);
		instance.spawnNpc(107745, 44, 31);
		instance.spawnNpc(107745, 45, 49);
		instance.spawnNpc(107745, 50, 45);
	}),
	VETION("Vet'ion", 18, 12603, Location.create(37, 15), instance -> {
		instance.spawnNpc(4175, 46, 12);
	}),
	CORP("Corporeal Beast", 19, 11844, Location.create(30, 32), instance -> {
		instance.spawnNpc(8133, 43, 33);
	});

	public static final InstanceType[] values = values();
	private final String name;
	private final int index, regionId, regionWidth, regionHeight;
	private final Consumer<InstanceManager> spawns;
	private final Location spawnLocation;
	private final boolean disabled;

	private InstanceType(String name, int index, int regionId, Location spawnLocation,
			Consumer<InstanceManager> spawns) {
		this(name, index, regionId, 64, 64, spawnLocation, false, spawns);
	}

	private InstanceType(String name, int index, int regionId, Location spawnLocation, boolean disabled,
			Consumer<InstanceManager> spawns) {
		this(name, index, regionId, 64, 64, spawnLocation, disabled, spawns);
	}

	private InstanceType(String name, int index, int regionId, int regionWidth, int regionHeight,
			Location spawnLocation, boolean disabled, Consumer<InstanceManager> spawns) {
		this.name = name;
		this.index = index;
		this.regionId = regionId;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
		this.spawnLocation = spawnLocation;
		this.disabled = disabled;
		this.spawns = spawns;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}

	public int getRegionId() {
		return regionId;
	}

	public int getRegionWidth() {
		return regionWidth;
	}

	public int getRegionHeight() {
		return regionHeight;
	}

	public Location getSpawnLocation() {
		return spawnLocation;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public Consumer<InstanceManager> getSpawns() {
		return spawns;
	}

}
