package com.soulplay.content.instances;

import java.util.Optional;

import com.soulplay.content.donate.DonationInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.World;
import com.soulplay.game.world.packet.ConfigCodes;

public class InstanceInterface {

	public static void open(Player player) {
		player.getPacketSender().showInterface(70200);
		player.getPacketSender().sendInstancesUpdate();
		updateVars(player);
	}

	public static boolean handleClick(Player player, int buttonId, int componentId) {
		switch (buttonId) {
			case 70403:
				InstanceType[] values = InstanceType.values;
				int instanceIndex = componentId / 2;
				if (instanceIndex < 0 || instanceIndex >= values.length) {
					return true;
				}

				player.selectedInstanceType = values[instanceIndex];
				player.getPacketSender().sendInstancesUpdate();
				player.getPacketSender().sendString(player.selectedInstanceType.getName(), 70251);
				return true;
			case 70224:
				if (player.selectedInstanceType == null) {
					player.sendMessage("You don't have an instance selected.");
					return true;
				}

				player.getPacketSender().setVisibility(70500, false);
				return true;
			case 70226:
				player.getDialogueBuilder().sendEnterText(() -> {
					final String playerName = player.getDialogueBuilder().getEnterText();
					if (playerName.isEmpty()) {
						player.getDialogueBuilder().sendStatement("Invalid player name entered.").onReset(() -> player.getPA().closeAllWindows()).executeIfNotActive();
						return;
					}

					Optional<Client> otherPlayerOptional = World.search(playerName);
					if (!otherPlayerOptional.isPresent()) {
						player.getDialogueBuilder().sendStatement("The player isn't online or has privacy setting turned on.").onReset(() -> player.getPA().closeAllWindows()).executeIfNotActive();
						return;
					}

					Client otherPlayer = otherPlayerOptional.get();
					if (!otherPlayer.insideInstance() || !otherPlayer.getInstanceManager().getName().equalsIgnoreCase(BossInstanceManager.NAME)) {
						player.getDialogueBuilder().sendStatement("Unable to join, player isn't inside an instance.").onReset(() -> player.getPA().closeAllWindows()).executeIfNotActive();
						return;
					}

					BossInstanceManager bossInstance = (BossInstanceManager) otherPlayer.getInstanceManager();
					int instanceKey = bossInstance.getKey();
					if (instanceKey == 0) {
						player.getDialogueBuilder().sendStatement("The player isn't online or has privacy setting turned on.").onReset(() -> player.getPA().closeAllWindows()).executeIfNotActive();
						return;
					}

					player.getDialogueBuilder().sendStatement("Please enter the provided key to join the instance.").sendEnterAmount(() -> {
							final int key = (int) player.enterAmount;
							if (key != instanceKey) {
								player.getDialogueBuilder().sendStatement("Invalid key entered.").onReset(() -> player.getPA().closeAllWindows()).executeIfNotActive();
								return;
							}

							bossInstance.teleport(player);
							player.getPA().closeAllWindows();
						}).executeIfNotActive();
				}).execute(false);
				return true;
			case 70509:
				player.getPacketSender().setVisibility(70500, true);
				return true;
			case 70228:
				DonationInterface.open(player);
				return true;
			case 70252:
				Variables.INSTANCE_INSURANCE.toggle(player);
				return true;
			case 70255:
				Variables.INSTANCE_HEALTH_REGEN.toggle(player);
				return true;
			case 70258:
				Variables.INSTANCE_PRAYER.toggle(player);
				return true;
		}

		return false;
	}

	public static void updateVars(Player player) {
		player.getPacketSender().sendConfig(ConfigCodes.INSTANCE_DEATH_SAFETY, Variables.INSTANCE_INSURANCE.getValue(player));
		player.getPacketSender().sendConfig(ConfigCodes.INSTANCE_HEALTH_GENERATION, Variables.INSTANCE_HEALTH_REGEN.getValue(player));
		player.getPacketSender().sendConfig(ConfigCodes.INSTANCE_PRAYER_DRAINING, Variables.INSTANCE_PRAYER.getValue(player));
	}

	public static void createInstance(Player player, String keyString) {
		player.getPacketSender().setVisibility(70500, true);

		if (player.selectedInstanceType == null) {
			player.sendMessage("You don't have an instance selected.");
			return;
		}

		if (player.selectedInstanceType.isDisabled()) {
			player.sendMessage("This instance isn't available at the moment.");
			return;
		}

		if (Variables.INSTANCE_CHARGES.getValue(player) <= 0) {
			player.sendMessage("You don't have enough instance charges to create an instance.");
			return;
		}

		if (player.inMinigame() || player.insideInstance()) {
			player.sendMessage("Unable to create the instance at this time.");
			return;
		}

		boolean safeDeath = Variables.INSTANCE_INSURANCE.toggled(player);
		int safeDeathCost = 150_000_000;
		boolean healthRegen = Variables.INSTANCE_HEALTH_REGEN.toggled(player);
		int healthRegenCost = 50_000_000;
		boolean prayerDrainReduction = Variables.INSTANCE_PRAYER.toggled(player);
		int prayerDrainReductionCost = 50_000_000;

		int gpCost = 0;
		if (safeDeath) gpCost += safeDeathCost;
		if (healthRegen) gpCost += healthRegenCost;
		if (prayerDrainReduction) gpCost += prayerDrainReductionCost;

		if (gpCost > 0 && !player.getItems().takeCoins(gpCost)) {
			player.sendMessage("You don't have enough gp to pay for the instance perks.");
			return;
		}

		int key = 0;
		try {
			key = Integer.parseInt(keyString);
		} catch (Exception e) {
			/* empty */
		}

		Variables.INSTANCE_CHARGES.addValue(player, -1);
		new BossInstanceManager(player, player.selectedInstanceType, safeDeath, healthRegen, prayerDrainReduction, key);
		player.getPA().closeAllWindows();
	}

}
