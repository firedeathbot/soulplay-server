package com.soulplay.content.instances;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.content.player.skills.SkillRestoration;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.InstanceManager;

public class BossInstanceManager extends InstanceManager {

	public static final String NAME = "Boss instances";
	private InstanceType type;
	private boolean safeDeath, healthRegen, prayerDrainReduction;
	private int key;
	private Set<Player> players = new HashSet<>();

	public BossInstanceManager(Player player, InstanceType type, boolean safeDeath, boolean healthRegen, boolean prayerDrainReduction, int key) {
		super(NAME, player);
		this.type = type;
		this.safeDeath = safeDeath;
		this.healthRegen = healthRegen;
		this.prayerDrainReduction = prayerDrainReduction;
		this.key = key;
		generateMap();
	}

	public void generateMap() {
		int regionId = type.getRegionId();
		Location regionLocation = Location.create((regionId >> 8) << 6, (regionId & 0xff) << 6);
		createStaticRegion(regionLocation, type.getRegionWidth(), type.getRegionHeight());
		type.getSpawns().accept(this);
		teleport(getPlayer());
	}

	public void teleport(Player player) {
		player.pulse(() -> {
			player.getPA().movePlayer(base(type.getSpawnLocation()));
			initPlayerDefaults(player);
		});
	}

	@Override
	public void initPlayerDefaults(Player player) {
		super.initPlayerDefaults(player);

		if (healthRegen) {
			setHealthRestoration(player, 15);
		}
	}

	@Override
	public void revertPlayerDefaults(Player player) {
		super.revertPlayerDefaults(player);

		if (healthRegen) {
			setHealthRestoration(player, SkillRestoration.RESTORE_TICKS);
		}
	}

	private void setHealthRestoration(Player player, int ticks) {
		SkillRestoration restoration = player.getSkills().getRestoration()[Skills.HITPOINTS];
		restoration.setRestoreTicks(ticks);
		restoration.restart();
	}

	@Override
	public NPC spawnNpc(int id, int x, int y, int z, Direction face) {
		NPC npc = super.spawnNpc(id, x, y, z, face);
		npc.isAggressive = true;
		npc.setRetargetTick(18);
		npc.bossRoomNpc = false;
		npc.alwaysRespawn = true;
		npc.setWalksHome(false);
		npc.setAggroRadius(64);
		npc.ignoreAggroTolerence();
		return npc;
	}

	@Override
	public boolean isMulti() {
		return true;
	}

	public boolean isPrayerDrainReduction() {
		return prayerDrainReduction;
	}

	@Override
	public boolean safeDeath() {
		return safeDeath;
	}

	public int getKey() {
		return key;
	}

	@Override
	public void enter(Player player) {
		super.enter(player);

		players.add(player);
	}

	@Override
	public void leave(Player player, boolean logout) {
		super.leave(player, logout);

		revertPlayerDefaults(player);
		players.remove(player);
		if (players.isEmpty()) {
			destroy(true);
		}
	}

	@Override
	public boolean destroyOnExit() {
		return false;
	}

}
