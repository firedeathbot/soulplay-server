package com.soulplay.content.event.impl;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.event.randomevent.Bork;
import com.soulplay.content.event.randomevent.SpawningStar;
import com.soulplay.content.event.randomevent.WildyWyrm;
import com.soulplay.content.minigames.ddmtournament.OfficialTournamentVariables;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.minigames.lastmanstanding.LmsManager;
import com.soulplay.content.player.FishingContest;
import com.soulplay.content.player.combat.rules.AntiRush;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.Connection;
import com.soulplay.net.login.spam.LoginAttemptHandler;
import com.soulplay.util.Misc;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.sql.clanelo.ClanEloHandler;
import com.soulplay.util.sql.scoregrab.DuelScoreBoard;
import com.soulplay.util.sql.scoregrab.LmsScoreBoard;
import com.soulplay.util.sql.scoregrab.WildyEloRating;
import com.soulplay.util.sql.scoregrab.WildyEloRatingSeason;

import plugin.item.skillcape.MagicCapePlugin;

public class HourlyTasks extends Event {

	public static List<String> diced = new ArrayList<>(); // per
																		// hour,
																		// or
																		// you
																		// can
																		// store
																		// what
																		// hour
																		// they
																		// diced
																		// with
																		// hashmap
																		// and
																		// do
																		// this
																		// for 2
																		// hour
																		// limit,
																		// etc:
																		// (getHour()
																		// + 2)
																		// % 12
																		// >=
																		// diced.getValue()

	private int hour;

	@Override
	public void execute() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean executeEvent() {
		int currentHour = Server.getCalendar().get24Hour();
		if (currentHour != hour) {
			hour = currentHour;
			diced.clear();
			AnnouncementHandler.removeAnnouncement();

			if (!WorldType.equalsType(WorldType.SPAWN)) {
				if (Misc.random(99) < 98) {
					if (PlayerHandler.getPlayerCount() > 49) {
						if (Misc.random(1) == 1) {
							if (!SpawningStar.starExists()) {
								SpawningStar.spawnStar();
							} else if (!FishingContest.isRunning()) {
								FishingContest.beginFishingContest();
							} else {
								WildyWyrm.spawnWorm();
							}
						} else if (Misc.random(1) == 0) {
							WildyWyrm.spawnWorm();
						} else if (!FishingContest.isRunning()) {
							FishingContest.beginFishingContest();
						} else {
							WildyWyrm.spawnWorm();
						}
					}
				}
			}
			
			if (hour == 1 || hour == 5 || hour == 9 || hour == 13 || hour == 17 || hour == 21) {
				    if (Bork.borkAlive == false) {
					    Bork.spawnBork();
				    }
				}

			if (WorldType.equalsType(WorldType.SPAWN)) {
				if (Misc.random(2) == 0) {
					WildyWyrm.spawnWorm();
				}
			}

			if (DBConfig.DOUBLE_DROP_RATE) {
				DBConfig.DOUBLE_DROP_RATE = false;
				PlayerHandler.messageAllPlayers(
						"<col=800000>Double Drop rate event has ended.");
			}
			if (DBConfig.DOUBLE_EXP) {
				DBConfig.DOUBLE_EXP = false;
				PlayerHandler
						.messageAllPlayers("<col=800000>Double EXP hour has ended.");
			}
			if (DBConfig.BETTER_HUNT_REWARD_CHANCE) {
				DBConfig.BETTER_HUNT_REWARD_CHANCE = false;
				PlayerHandler
						.messageAllPlayers("<col=800000>PK Happy Hour has ended.");
			}
			
			if (Client.edgeWildyEvent) {
				Client.edgeWildyEvent = false;
			}
			if (Client.clanWarsWildyEvent) {
				Client.clanWarsWildyEvent = true;
			}
			if (Misc.random(300) == 7) { // a random chance of increasing drop
											// rate for 1 hour or exp
				if (Misc.random(99) > 55) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Double Drop rate is on for 1 hour!");
					DBConfig.DOUBLE_DROP_RATE = true;
				} else { // double exp
					PlayerHandler.messageAllPlayers(
							"<col=800000>Double EXP rate is on for 1 hour!");
					DBConfig.DOUBLE_EXP = true;
				}

			}

			if (Misc.random(24) == 7) { // pk happy hour
				PlayerHandler.messageAllPlayers(
						"<col=800000>It's PK Happy Hour! Get increased chances to get rewards from target kills!");
				DBConfig.BETTER_HUNT_REWARD_CHANCE = true;
			}

			// Connection.spammingConnections.clear();
			Connection.spammingConnectionsHacker2Thing.clear();
			Connection.clearBankPinAttempts();
			LoginAttemptHandler.clearPasswordFails();

			WildyEloRating.reloadWildyEloScores();
			ClanEloHandler.reloadClanEloScores();
			DuelScoreBoard.reloadDuelScores();
			LmsScoreBoard.reloadDuelScores();
			
//			BunnyBoss.spawnBoss(); //old easter event.

			if (hour == 1) {
				WildyEloRatingSeason.rewardAndReset(false);
				MagicCapePlugin.resetUses();
			}
			
			if (currentHour == 0) {
				// PKBoards.decayElo();
				SoulPlayDate.createLastSunday();
				Connection.starterRecieved1.clear();
				Connection.starterRecieved2.clear();
				AntiRush.getRusherUUID().clear();
				PlayerHandler.updateDailyCountList();
			}
			
			if (WorldType.equalsType(WorldType.ECONOMY)) {
				if (currentHour == OfficialTournamentVariables.pkTournamentStartHour) { // 4 PM EST
					Tournament.beginPkTourneyQueue();//FunPkTournament.openTournament();
				}
				
				if (currentHour == OfficialTournamentVariables.lmsStartHour) {
					if (Config.NODE_ID == 1) // only start on world 1
						LmsManager.startMassLMSEvent();
				}
			}

			return true;
		}
		return false;
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean finishEvent() {
		// TODO Auto-generated method stub
		return false;
	}

}
