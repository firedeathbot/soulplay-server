package com.soulplay.content.event.impl;

import java.util.HashMap;

import com.soulplay.Server;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.net.login.spam.LoginAttemptHandler;

public class DailyTasks extends Event {

	public static HashMap<String, Integer> dungeonCaveKills = new HashMap<>();

	// public static HashMap<String, Integer> dungeonCaveKills = new
	// HashMap<String, Integer>();
	private int day;

	@Override
	public void execute() {

	}

	@Override
	public boolean executeEvent() {
		// if (!isActive()) {
		int currentDay = Server.getCalendar().currentDayOfWeek();
		if (currentDay != day) {
			day = currentDay;
			dungeonCaveKills.clear();
			LoginAttemptHandler.clearMaxAccountMap();
			PriceChecker.updatePrices();
			return true;
		}
		// }
		return false;
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean finishEvent() {
		// TODO Auto-generated method stub
		return false;
	}

}
