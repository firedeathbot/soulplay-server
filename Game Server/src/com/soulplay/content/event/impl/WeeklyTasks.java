package com.soulplay.content.event.impl;

import com.soulplay.Server;
import com.soulplay.content.minigames.penguin.PenguinHandler;
import com.soulplay.content.player.misc.StaffTimer;

public class WeeklyTasks extends Event {

	private String day;

	@Override
	public void execute() {

	}

	@Override
	public boolean executeEvent() {
		// if (!isActive()) {
		if (!Server.getCalendar().getDayOfWeek().equalsIgnoreCase(day)) {
			day = Server.getCalendar().getDayOfWeek();

			/**
			 * ACTUAL WEEKLY CODE GOES INSIDE HERE FOR MONDAYS
			 */
			if (Server.getCalendar().getDayOfWeek()
					.equalsIgnoreCase("monday")) {
				

				StaffTimer.resetTimers();

				PenguinHandler.resetPenguins();

			}
			return true;
		}
		// }
		return false;
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean finishEvent() {
		// TODO Auto-generated method stub
		return false;
	}

}
