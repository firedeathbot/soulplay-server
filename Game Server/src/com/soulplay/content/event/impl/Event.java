package com.soulplay.content.event.impl;

public abstract class Event {

	public boolean active;

	public abstract void execute();

	public abstract boolean executeEvent();

	public abstract void finish();

	public abstract boolean finishEvent();

	public boolean isActive() {
		return this.active;
	}

	public boolean setActive(boolean active) {
		return this.active = active;
	}

}
