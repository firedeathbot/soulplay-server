package com.soulplay.content.event.impl;

import com.soulplay.Server;
import com.soulplay.content.poll.PollManager;
import com.soulplay.game.model.objects.WildernessObeliskEnum;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.net.login.spam.LoginAttemptHandler;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.util.sql.scoregrab.ClwRedPortalScoreBoard;
import com.soulplay.util.sql.scoregrab.WildyEloRatingSeason;

public class MinuteTasks extends Event {

	private int minute;

	@Override
	public void execute() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean executeEvent() {
		int currentMinute = Server.getCalendar().getMinutes();
		if (currentMinute != minute) {
			minute = currentMinute;

			LoginServerConnection.instance().submit(() -> {
				if (LoginServerConnection.instance().isActive()) {
					LoginServerConnection.instance()
							.sendCommand("req_player_count");
				}
			});
			if (!LoginServerConnection.instance().isActive()) {
				TaskExecutor.execute(new Runnable() { // LoginServerConnection.instance().submit(new Runnable() {

				 @Override
				 public void run() {
					 Server.connectToLoginServer();
				 }
				 });
			}

			if (currentMinute >= 5 && currentMinute % 5 == 0) {
				LoginAttemptHandler.clearLoginAttempts();
				PlayerSaveSql.newAccounts = 0;
				ClwRedPortalScoreBoard.reloadScores();
			}
			
			WildernessObeliskEnum.randomizeObelisks();
			
			PollManager.reloadVoteCount();
			WildyEloRatingSeason.reloadWildyEloScores();

			return true;
		}
		return false;
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean finishEvent() {
		// TODO Auto-generated method stub
		return false;
	}

}
