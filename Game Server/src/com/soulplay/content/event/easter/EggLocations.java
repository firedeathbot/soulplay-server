package com.soulplay.content.event.easter;

import com.soulplay.game.world.entity.Location;

public enum EggLocations {

	ONE(9, 9),
	TWO(5, 1),
	THREE(3, 9);
	
	final Location[] locs = new Location[4];
	
	private EggLocations(int x, int y) {
		locs[0] = Location.create(x, y);
		locs[1] = Location.create(y, x*-1);
		locs[2] = Location.create(y*-1, x);
		locs[3] = Location.create(x*-1, y*-1);
	}
	
	public static final EggLocations[] VALUES2 = values();
	
}
