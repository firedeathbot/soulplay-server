package com.soulplay.content.event.easter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.function.Consumer;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.impl.Warmonger;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = {9310})
public class BunnyBoss extends AbstractNpc {

	public BunnyBoss(int slot) {
		super(slot, 9310);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new BunnyBoss(slot);
	}
	
	@Override
	public void onSpawn() {
		stopAttack = true;
		isAggressive = false;
		setAliveTick(3000);
		setRetaliates(false);
		
		Location centerLoc = getCenterLocation();
		
		rectangle = new Rectangle(Location.create(centerLoc.getX()-15, centerLoc.getY()-15),
				Location.create(centerLoc.getX()+15, centerLoc.getY()+15));
	}
	
	private static final Animation JUMP_ANIM = new Animation(11503);
	private static final Animation JUMP_ANIM2 = new Animation(11504);
	private static final Animation SAD_ANIM = new Animation(11505);
	
	private static final Animation SIT_ANIM = new Animation(11510);
	
	private Rectangle rectangle;
	
	@Override
	public void onDeath() {
		super.onDeath();

		startAnimation(JUMP_ANIM);
		dropRandomRewards();
		Server.getTaskScheduler().schedule(()-> {
			startAnimation(SIT_ANIM);
		});
		
		if (Misc.randomBoolean(1))
			forceChat("Happy Easter Everyone!");
		else
			forceChat("Ooops! I dropped something!");
		
		actionTimer = 4;
		
		if (Misc.randomBoolean(3)) {
			forceChat("Double exp for everyone!");
			PlayerHandler.executeForPlayers(player -> {
				player.setDoubleExpLength(player.getDoubleExpLength() + 6000);
				player.sendMessage("Easter Bunny has dropped double exp!");
			});
		}
		
		//TODO: maybe on death, spawn a bunch of small rabbits and when killed, they have 50/50 chance of dropping loot
	}
	
	@Override
	public void onHitFromAttackMob(BattleState state) {
		if (state.getCurrentHit() > 0 && state.getCombatStyle() == CombatType.MELEE) {
			meleeDamageTaken += state.getCurrentHit();
		}
		if (state.getCurrentHit() > 0) {
			int prevDamage = damageMap.getOrDefault(state.getAttacker().getName(), 0);
			damageMap.put(state.getAttacker().mySQLIndex, state.getCurrentHit() + prevDamage);
		}
	}
	
	private Map<Integer, Integer> damageMap = new HashMap<>();
	
	private int meleeDamageTaken = 0;
	
	private static final Animation WAVE_ANIM = new Animation(15346, 30);
	
	private int step = -1;
	
	private int resetWavingAnim = 0;
	
	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0) {
			return;
		}
		
		if (resetWavingAnim > 0) {
			resetWavingAnim--;
			if (resetWavingAnim == 0) {
				startAnimation(Animation.RESET);
			}
		}
		
		//attacks
		if (getAttackTimer() <= 0) {
			
			lockMovement(3);
			
			setAttackTimer(6);
			
			if (meleeDamageTaken > 100 && Misc.randomBoolean(1)) {
				step = 6;
				meleeDamageTaken = 0;
			}
			
			final NPC boss = this;
			
			switch (step) {
			
			case 0:
				
				Direction dir = Direction.getRandomNoDiagonal();
				
				Location startLoc = getCurrentLocation().transform(2 + dir.getStepX(), 2 + dir.getStepY());
				
				turnNpc(getX() + 2 + dir.getStepX()*5, getY() + 2 + dir.getStepY()*5);
				
				startAnimation(WAVE_ANIM);
				resetWavingAnim = 4;
				
				nextStop = 4;
				
				setAttackTimer(10);
				
				generatePyramidTiles(dir, startLoc);
				
				Server.getTaskScheduler().schedule(new Task() {
					int ticks = 0;
					@Override
					protected void execute() {
						ticks++;
						if (ticks >= 8) {
							this.stop();
							return;
						}
						
						switch (ticks) {
						
						case 1:
							forLocalPlayers(player-> {
								player.getPacketSender().playSound(2611);
							});
							break;
						
						case 2:
							forLocalPlayers(player-> {
								player.getPacketSender().playSound(2600);
							});
							sendPyramidWave();
							break;
							
						case 3:
							sendPyramidWave();
							break;
							
						case 4:
							sendPyramidWave();
							break;
							
						case 5:
							sendPyramidWave();
							break;
							
						case 6:
							sendPyramidWave();
							break;
							
						case 7:
							sendPyramidWave();
							break;
							
						
						}
						
					}
				});
				break;
				
			case 1:
				basicAttack();
				break;
				
			case 2:
				//jump up 3 times and throw eggs at certain spots in the map to do blast radius 5x5 or 3x3, following a pattern
				
				setAttackTimer(12);
				
				forLocalPlayers(player-> {
					player.getPacketSender().playSound(2621);
				});
				
				forceChat("Yeeet!");
				
				Server.getTaskScheduler().schedule(new Task() {
					int tick = 0;
					@Override
					protected void execute() {
						tick++;
						
						if (tick > 6) {
							this.stop();
							return;
						}
						
						
						switch (tick) {
						
						case 1:
							startAnimation(JUMP_ANIM);
							throwEggsAround(0);
							break;
							
						case 3:
							startAnimation(JUMP_ANIM);
							throwEggsAround(1);
							break;
							
						case 5:
							startAnimation(JUMP_ANIM);
							throwEggsAround(2);
							break;
							
						case 2: //filler jumps
						case 4:
						case 6:
							startAnimation(JUMP_ANIM2);
							break;
						
						}
						
					}
				});
				
				break;
				
			case 3:
				basicAttack();
				break;
				
			case 4:

				//slam ground and deal melee damage
				
				startGraphic(Warmonger.JUMP_SLAM_GFX);
				
				forLocalPlayers(player -> {
					player.getPacketSender().playSound(2614);
				});
				
				Server.getTaskScheduler().schedule(new Task() {
					int tick = 0;
					@Override
					protected void execute() {
						tick++;
						if (tick >= 4) {
							this.stop();
							return;
						}
						
						switch (tick) {
						case 1:
							startAnimation(JUMP_ANIM);
							break;
							
						case 2:
							forLocalPlayers(player-> {
								if (player.getCurrentLocation().isWithinDeltaDistance(getCenterLocation(), 5)) {
									NpcHitPlayer.dealMeleeDamage(boss, player, Misc.random(5, maxHit), 0, null);
								}
							});
							break;
						}
						
					}
				});
				
				break;

			case 5:
				basicAttack();
				break;
				
			case 6:
				//face melee player, cry and shoot chocolate
				activateSadAttack();
				
				break;
				
//			case 5:
//				
//				//spawn bunch of rabbid rabbits that run around and attack anyone nearby, then die after 30 seconds.
//				
//				break;
				
				default:
					basicAttack();
					break;
				
			}

			step++;
			
			step = step % 6;
			
		} else {
			//else random walk
			
			if (Misc.randomBoolean(3)) {
				Direction ranDir = Direction.getRandom();
				if (!outsideWalkRadius(ranDir.getStepX(), ranDir.getStepY())) {
					moveX = ranDir.getStepX();
					moveY = ranDir.getStepY();
				}
			}
			
		}
		
	}
	
	private boolean outsideWalkRadius(int stepX, int stepY) {
		return getDistance(makeX, stepX+getX(), makeY, stepY+getY()) > getWalkDistance();
	}
	
	/**
	 * Chocolate Projectile special attack
	 */
	
	private List<Location> pyramidTiles = new ArrayList<>(144);
	
	private void generatePyramidTiles(Direction centerDir, Location start) {
		pyramidTiles.clear();
		pyrWaveCount = 0;
		
		Direction sideDir1 = Direction.rotate(centerDir);
		Direction sideDir2 = sideDir1.getOpposite();
		
//		Server.getStillGraphicsManager().createGfx(start, 5, 0, 0);
		
		pyramidTiles.add(start);
		
		int nextCount = 2;
		
		for (int i = 1; i < 12; i++) {
			Location nextCenter = start.transform(centerDir, i);
			pyramidTiles.add(nextCenter);
			
//			Server.getStillGraphicsManager().createGfx(nextCenter, 5, 0, 0);
			
			for (int s = 1; s < nextCount; s++) {
				
				Location side1 = nextCenter.transform(sideDir1, s);
				Location side2 = nextCenter.transform(sideDir2, s);
				
				pyramidTiles.add(side1);
				pyramidTiles.add(side2);

//				Server.getStillGraphicsManager().createGfx(side1, 5, 0, 0);
//				Server.getStillGraphicsManager().createGfx(side2, 5, 0, 0);
				
			}
			nextCount += 1;
		}
//		forceChat("tiles:"+pyramidTiles.size());
		
		int count = 0;
		
		for (int i = pyramidTiles.size()-1; i > 0; i--) {
			Location end = pyramidTiles.get(i);
			
			Projectile prj = new Projectile(CHOCOLATE_PROJECTILE, start, end);
			prj.setStartDelay(50);
			prj.setEndDelay(220);//220 or 240 looks good
			prj.setStartHeight(60);
			prj.setEndHeight(28);
			prj.setSlope(0);
			GlobalPackets.createProjectile(prj);
			
			count++;
			if (count == 23)
				break;
		}
	}
	
	private int pyrWaveCount = 0;
	
	private int nextStop = 4;

	private List<Location> hitSpots = new ArrayList<>(144);
	
	private void sendPyramidWave() {
		int pauseAt = pyrWaveCount;
		if (pyrWaveCount == 0) {
			pauseAt = 3;
		} else {
			nextStop += 8;
			pauseAt = pyrWaveCount + nextStop-1;
		}
		
		int size = pyramidTiles.size();
		for (int i = pyrWaveCount; i < size; i++) {
			Location loc = pyramidTiles.get(i);
//			Server.getStillGraphicsManager().createGfx(loc, 5/*2856*/, 0, 0);
			hitSpots.add(loc);
			if (i == pauseAt) {
				pyrWaveCount = i+1;
//				System.out.println("wavecount:"+ pyrWaveCount);
				break;
			}
		}
		

		size = hitSpots.size();

		if (size > 0) {
			for (Player p : Server.getRegionManager().getLocalPlayersOptimized(this)) {
				for (int i = 0; i < size; i++) {
					Location hitLoc = hitSpots.get(i);
					if (p.getCurrentLocation().matches(hitLoc)) {
						NpcHitPlayer.dealMagicDamage(this, p, Misc.random(1, maxHit), 0, CHOCOLATE_HIT_GFX1);
						p.lockMovement(1);
					}
				}
			}
			hitSpots.clear();
		}
		
	}
	
	private static final Graphic CHOCOLATE__START_HIT_GFX = Graphic.create(2856);
	
	public static final int CHOCOLATE_PROJECTILE = 2857;
	
	private static final Graphic CHOCOLATE_HIT_GFX1 = Graphic.create(2858);
	
	
	/**
	 * Jump and throw eggs
	 */

	private void throwEggsAround(int index) {
		
		forLocalPlayers(player-> {
			player.getPacketSender().playSound(2615);
		});
		
		Location start = getCurrentLocation().transform(2, 2);
		
		List<Location> hitLocs = new ArrayList<>();
		
		EggLocations eggLoc = EggLocations.VALUES2[index];
		
		int size = eggLoc.locs.length;
		for (int count = 0; count < size; count++) {
			Location d = eggLoc.locs[count];
			
			Location hitL = start.transform(d.getX(), d.getY());
			
			hitLocs.add(hitL);
			
			//creates shadow
			Server.getStillGraphicsManager().createGfx(hitL, Graphic.getOsrsId(1447), 0, 10);
			
			Projectile proj = new Projectile(813, start, hitL);
			proj.setSlope(50);
			proj.setStartDelay(10);
			proj.setEndDelay(140);
			proj.setEndHeight(0);
			proj.setStartHeight(100);
			GlobalPackets.createProjectile(proj);
			
			Server.getStillGraphicsManager().createGfx(hitL.getX(), hitL.getY(), hitL.getZ(), Graphic.getOsrsId(1328), 0, Misc.serverToClientTick(4));
		}
		final NPC boss = this;
		Server.getTaskScheduler().schedule(5, ()-> {
			forLocalPlayers(player -> {
				for (int i = 0, l = hitLocs.size(); i < l; i++) {
					Location hitLocation = hitLocs.get(i);
					if (hitLocation.isWithinDeltaDistance(player.getCurrentLocation(), 3)) {
						NpcHitPlayer.dealMagicDamage(this, player, Misc.random(5, maxHit), 0, null);
					}
				}
			});
		});
	}
	
	private void activateSadAttack() {
		
		startAnimation(SAD_ANIM);
		
		Location centerLocation = getCenterLocation();
		
		Location hitLoc = getCurrentLocation().transform(-1, -1);

		List<Location> hitTiles = new ArrayList<>(28);
		
		forLocalPlayers(player-> {
			player.getPacketSender().playSound(2617);
		});

		Direction direction = Direction.NORTH;
		for (int j = 0; j < 4; j++) {
			for (int i = 0; i < 7; i++) {
				if (i != 0)
					hitLoc = hitLoc.transform(direction);

				// prevent from corners getting hit twice
				if (i == 6)
					continue;
				
				hitTiles.add(hitLoc);

				Server.getStillGraphicsManager().createGfx(hitLoc, 2859, 0, 60);

				Projectile proj = new Projectile(CHOCOLATE_PROJECTILE, centerLocation, hitLoc);
				proj.setSlope(50);
				proj.setStartDelay(10);
				proj.setEndDelay(60);
				proj.setEndHeight(0);
				proj.setStartHeight(100);
				GlobalPackets.createProjectile(proj);

			}
			direction = Direction.rotate(direction);
		}

		Server.getTaskScheduler().schedule(2, ()-> {

			forLocalPlayers(player-> {
				for (int i = 0, l = hitTiles.size(); i < l; i++) {
					if (player.getCurrentLocation().matches(hitTiles.get(i))) {
						NpcHitPlayer.dealMagicDamage(this, player, Misc.random(3, maxHit), 0, null);
					}
				}
			});

		});


		//get player, get logical direction, grab 1x5 line, send gfx
	}
	
	
	private void forLocalPlayers(Consumer<Player> cons) {
		for (Player p : Server.getRegionManager().getLocalPlayersOptimized(this)) {
			cons.accept(p);
		}
	}
	
	private void dropRandomRewards() {
		
		Collections.shuffle(itemList);
		
		Set<String> UUIDs = new HashSet<>();
		
		for (Entry<Integer, Integer> damage : damageMap.entrySet()) {
			int dmg = damage.getValue();
			int mysqlIndex = damage.getKey();
			
			if (dmg > 150) {
				
				Player p = PlayerHandler.getPlayerByMID(mysqlIndex);
				
				if (p != null) {
					if (UUIDs.add(p.UUID)) {
						Item item = Misc.random(itemList);
						ItemHandler.createGroundItem(p, item.getId(), getX(), getY(), getZ(), item.getAmount());
						
						if (Misc.randomBoolean(1000)) {
							ItemHandler.createGroundItem(p, 30327, getX(), getY(), getZ(), 1);
							ItemHandler.createGroundItem(p, 30369, getX(), getY(), getZ(), 2000);
						}
					}
				}
				
			}
		}
		
		List<Location> rewardTiles = new ArrayList<>();
		
		final Location centerLoc = getCenterLocation();
		
		for (int i = 0; i < 6; i++) {
			Location randomLoc = rectangle.getRandomTile();

			rewardTiles.add(randomLoc);

			Projectile proj = new Projectile(878, centerLoc, randomLoc);
			proj.setSlope(50);
			proj.setStartDelay(10);
			proj.setEndDelay(140);
			proj.setEndHeight(0);
			proj.setStartHeight(100);
			GlobalPackets.createProjectile(proj);

		}
		
		
		Server.getTaskScheduler().schedule(5, ()-> {
			for (int i = 0, l = rewardTiles.size(); i < l; i++) {
				Location loc = rewardTiles.get(i);
				Item item = itemList.get(i);
				ItemHandler.createGroundItem(new GroundItem(item.getId(), loc.getX(), loc.getY(), loc.getZ(), item.getAmount(), 400), true);
			}
		});
		
	}

	private void basicAttack() {
		Location start = getCenterLocation();
		
		List<Player> playerList = new ArrayList<>();
		
		for (Player p : Server.getRegionManager().getLocalPlayersOptimized(this)) {
			if (p.getCurrentLocation().isWithinDeltaDistance(start, 12)) {
				playerList.add(p);
			}
		}
		
		if (playerList.isEmpty())
			return;
		
		forLocalPlayers(player-> {
			player.getPacketSender().playSound(2645, 600);
		});
		
		final Player target = Misc.random(playerList);
		
		turnNpc(target.getX(), target.getY());
		
		startAnimation(WAVE_ANIM);
		resetWavingAnim = 2;
		
		Projectile proj = new Projectile(976, start, target.getCurrentLocation());
		proj.setStartDelay(30);
		proj.setEndDelay(120);
		proj.setStartHeight(50);
		proj.setEndHeight(28);
		proj.setSlope(2);
		proj.setLockon(target.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		
		NpcHitPlayer.dealRangeDamage(this, target, Misc.random(maxHit), 5, null);
		
		
	}
	
	private static final Item[] rewards = { new Item(14728), new Item(24149),
			new Item(24150), new Item(30120), new Item(24145), new Item(4151),
			new Item(11732), new Item(537, 50), new Item(6585), new Item(4716),
			new Item(4718), new Item(4720), new Item(4722), new Item(2572),
			new Item(7927), new Item(7928, 5), new Item(7929, 5), new Item(7930, 5), new Item(7931, 5), new Item(7932, 5), new Item(7933, 5), };
	
	private static List<Item> itemList = new ArrayList<>(Arrays.asList(rewards));
	
	public static void spawnBoss(Location spawnL, int hp, int maxhit, int attack, int defense) {
		if (!ENABLED)
			return;
		
		EasterBossLocations bossLoc = null;
		
		int slot = Misc.findFreeNpcSlot();
		
		NPC boss = new BunnyBoss(slot);

		if (spawnL == null) {
			bossLoc = EasterBossLocations.generateLoc();
			spawnL = bossLoc.getLocation();
		}
		
		NPCHandler.spawnNpc(boss, spawnL.getX(), spawnL.getY(), spawnL.getZ(), 0);
		
		boss.getSkills().setStaticLevel(Skills.HITPOINTS, hp);
		boss.maxHit = maxhit;
		boss.getSkills().setStaticLevel(Skills.ATTACK, attack);
		boss.getSkills().setStaticLevel(Skills.DEFENSE, defense);

		if (bossLoc != null) {
			String msg = "Easter Bunny Boss spawned " + bossLoc.getLocationMessage();

			PlayerHandler.messageAllPlayers(msg);
			if (Config.RUN_ON_DEDI)
				PlayerHandler.messageAllPlayersInAllWorlds(msg, "");
		}
	}
	
	public static boolean ENABLED = true;

}
