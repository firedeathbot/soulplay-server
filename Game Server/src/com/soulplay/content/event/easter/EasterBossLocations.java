package com.soulplay.content.event.easter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.game.world.entity.Location;

public enum EasterBossLocations {
	
	CAMELOT_CASTLE(Location.create(2782, 3463), "outside of Camelot Castle."),
	DROP_PARTY_ROOM(Location.create(2736, 3470), "inside Drop Party Castle."),
	FLAX_FIELD(Location.create(2749, 3421), "south of the Flax Field."),
	FALADOR_FARM(Location.create(3031, 3304), "in Falador Farm."),
	LUMBRIDGE_SWAMP(Location.create(3239, 3176), "at the Lumbridge Swamp."),
	AL_KHARID_MINE(Location.create(3291, 3261), "south of Al Kharid mine."),
	BARROWS_HILLS(Location.create(3564, 3287), "at the Barrows Hills."),
	DEMONIC_RUINS(Location.create(3252, 3881), "west of Demonic Ruins."),
	GRAND_EXCHANGE(Location.create(3170, 3537), "north of Grand Exchange."),
	CASTLE_WARS(Location.create(2420, 3107), "in Castle Wars Arena."),
	CASTLE_WARS2(Location.create(2376, 3101), "in Castle Wars Arena."),
	EAST_VARROCK_BANK(Location.create(3264, 3427), "by the East Varrock bank."),
	DRAYNOR(Location.create(3099, 3226), "south of Draynor."),
	FUN_PK_ZONE(Location.create(3311, 3078), "at the Fun Pk Zone. ::funpk"),
	CLW_WHITE_PORTAL(Location.create(2830, 5562), "inside the Clan Wars White Portal."),
	CLW_RED_PORTAL_HYBRID(Location.create(3024, 5563, 0), "in CLW Red Portal for Hybrid/NH."),
	CANIFIS(Location.create(3489, 3487), "in Canifis.")
	;
	
	private final Location loc;
	private final String locString;
	
	private EasterBossLocations(Location loc, String locString) {
		this.loc = loc;
		this.locString = locString;
	}

	public String getLocationMessage() {
		return locString;
	}

	public Location getLocation() {
		return loc;
	}

	private static final List<EasterBossLocations> list = new ArrayList<>(values().length);
	
	static {
		for (EasterBossLocations b : values()) {
			list.add(b);
		}
	}
	
	public static EasterBossLocations generateLoc() {
		Collections.shuffle(list);
		return list.get(0);
	}
	

}
