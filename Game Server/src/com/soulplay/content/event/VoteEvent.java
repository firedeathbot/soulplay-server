package com.soulplay.content.event;

import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.event.randomevent.SpawningStar;
import com.soulplay.content.event.randomevent.WildyWyrm;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class VoteEvent {
	
	private static int votes = 0;
	
	public static int threshold = 200;
	
	private static String MSG = "<img=9> <shad=1><col=FF9933>" + threshold + " player votes has granted a random event.";
	
	private static final String EXP_MSG = "<img=9> <shad=1><col=0000FF>Everyone has been awarded 60 minutes of double XP by vote event.";
	
	public static void onVote() {
		votes++;
		
		if (votes == threshold) {
			votes = 0;
			
			randomEvent();
		}
	}
	
	private static void randomEvent() {
		
		PlayerHandler.messageAllPlayers(MSG);

		switch (Misc.random(4)) {
		case 0:
		case 1:
			for (Player p : PlayerHandler.players) {
				if (p == null) {
					continue;
				}

				p.setDoubleExpLength(p.getDoubleExpLength() + 6000);
			}

			PlayerHandler.messageAllPlayers(EXP_MSG);
			break;
		case 2:
			WildyWyrm.spawnWorm();
			break;
		case 3:
			if (SpawningStar.starExists())
				randomEvent();
			else
				SpawningStar.spawnStar();
			break;
		case 4:
			MagicalCrate.spawnWorldCrate();
			break;
		}
	}

}
