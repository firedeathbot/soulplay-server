package com.soulplay.content.event;

import com.soulplay.Config;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class PkingPotEvent {

	private static long goldAmount = 0;
	
	public static void addGold(int amountToAdd) {
		goldAmount += amountToAdd;
		PlayerHandler.messageAllPlayersInAllWorlds("World " + Config.NODE_ID + ": " + Misc.formatNumbersWithCommas(amountToAdd) + " gold has been added to the PKing Pot.", "");
		PlayerHandler.messageAllPlayersInAllWorlds("World " + Config.NODE_ID + ": Kill players in the wilderness on that world to obtain a split share.", "");
	}
	
	public static int getGoldFromPot() {
		if (goldAmount < 1)
			return 0;
		
		int ran = Misc.random(1_000_000, 50_000_000);
		
		if (goldAmount > 8_000_000_000l) {
			ran = Misc.random(30_000_000, 500_000_000);
		}
		
		ran = Math.min((int)goldAmount, ran);
		
		goldAmount -= ran;
		
		return ran;
	}
	
	public static long checkPot() {
		return goldAmount;
	}
	
	public static void resetPkPot() {
		goldAmount = 0;
	}
}
