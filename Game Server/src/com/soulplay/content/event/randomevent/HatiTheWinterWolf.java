package com.soulplay.content.event.randomevent;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class HatiTheWinterWolf {

	public static enum HatiLocationData {
		LOCATION_1(Location.create(2956, 3873), "near ice warriors in wilderness", "in Wilderness"),
		LOCATION_2(Location.create(2710, 3676), "in the woods East from Relekka", "in Relekka"),
		LOCATION_3(Location.create(2724, 3648), "in the woods East from Relekka", "in Relekka"),
		LOCATION_4(Location.create(2702, 3615), "in the woods East from Relekka", "in Relekka");
		
		public static final HatiLocationData[] values = values();

		private Location spawnPos;

		private String clue;

		public String playerPanelFrame;

		private HatiLocationData(Location spawnPos, String clue, String playerPanelFrame) {
			this.setSpawnPos(spawnPos);
			this.clue = clue;
			this.playerPanelFrame = playerPanelFrame;
		}

		public Location getSpawnPos() {
			return spawnPos;
		}

		public void setSpawnPos(Location spawnPos) {
			this.spawnPos = spawnPos;
		}
	}

	private static HatiLocationData LAST_LOCATION = null;

	public static HatiLocationData getRandom() {
		HatiLocationData hati = HatiLocationData.values[Misc .random(HatiLocationData.values.length - 1)];
		return hati;
	}

	public static HatiLocationData getHatiLocationData() {
		return LAST_LOCATION;
	}

	private static void spawnHatiWolf(Location loc) {
		NPC hatiWolf = NPCHandler.spawnNpc2(13460, HatiTheWinterWolf.getHatiLocationData().getSpawnPos().getX(), HatiTheWinterWolf.getHatiLocationData().getSpawnPos().getY(), 0, 0);
		hatiWolf.setAliveTick(6000); // 6000 = 1 hour
		hatiWolf.setCanWalk(false);
	}

	public static void spawnHati() {

		HatiLocationData locationData = getRandom();
		if (LAST_LOCATION != null) {
			if (locationData == LAST_LOCATION) {
				locationData = getRandom();
			}
		}
		LAST_LOCATION = locationData;
		PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>Hati the Winter Wolf has appeared "
						+ locationData.clue + "!");
		spawnHatiWolf(locationData.getSpawnPos());
	}

}
