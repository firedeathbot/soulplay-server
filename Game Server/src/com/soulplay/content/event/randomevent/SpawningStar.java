package com.soulplay.content.event.randomevent;

import com.soulplay.content.event.randomevent.ShootingStar.StarInfo;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class SpawningStar {

	public static enum LocationData {
		LOCATION_1(Location.create(2720, 3469), "South from Camelot Bank"),
		LOCATION_2(Location.create(2725, 3502), "North from Camelot Bank"),
		LOCATION_3(Location.create(3110, 3251), "East from Draynor Bank"),
		LOCATION_4(Location.create(3079, 3250), "West from Draynor Bank"),
		LOCATION_5(Location.create(2818, 3471), "North from Catherby Bank"),
		LOCATION_6(Location.create(2791, 3430), "West from Catherby Bank"),
		LOCATION_7(Location.create(2581, 3418), "Near Fishing Guild"),
		LOCATION_8(Location.create(2606, 3401), "Near Fishing Guild"),
		LOCATION_9(Location.create(2802, 3066), "East from Karambwan "),
		LOCATION_10(Location.create(2781, 3065), "West from Karambwan"),
		LOCATION_11(Location.create(2334, 3832), "East from Mining Neitznot"),
		LOCATION_12(Location.create(3055, 3352), "East from Falador Bank"),
		LOCATION_13(Location.create(3053, 3301), "South of Falador near Farming patches"),
		LOCATION_14(Location.create(3363, 3270), "near Duel Arena"),
		LOCATION_15(Location.create(2666, 2648), "in the Void knight island"),
		LOCATION_16(Location.create(3566, 3297), "near Barrows hills"),
		LOCATION_17(Location.create(2457, 3090), "near Castle Wars"),
		LOCATION_18(Location.create(2480, 3433), "at the Gnome Agility Course"),
		LOCATION_19(Location.create(3094, 3484), "South from Edgeville bank"),
		LOCATION_20(Location.create(3126, 3517), "Near Edgeville bridge"),
		LOCATION_21(Location.create(3077, 3503), "Near Edgeville Bank"),
		LOCATION_22(Location.create(2745, 3445), "in the middle of the Flax field"),
		LOCATION_23(Location.create(2986, 3599), "in the Wilderness (near the western dragons)"),
		LOCATION_24(Location.create(2995, 3911), "outside the Wilderness Agility Course"),
		LOCATION_25(Location.create(3059, 3941), "West from mage arena"),
		LOCATION_26(Location.create(3062, 3884), "North from lava maze"),
		LOCATION_27(Location.create(3110, 3564), "Near Edgeville Wildy mining"),
		LOCATION_28(Location.create(3018, 3594), "South from Dark warriors fortress in Wildy mining"),
		LOCATION_29(Location.create(3139, 3687), "West from Graveyard of Shadows in Wilderness");

		private static final LocationData[] values = values();
		private final Location spawnPos;
		private final String clue;

		private LocationData(Location spawnPos, String clue) {
			this.spawnPos = spawnPos;
			this.clue = clue;
		}

		public Location getSpawnPos() {
			return spawnPos;
		}

		public String getClue() {
			return clue;
		}
	}

	private static LocationData lastLocation = null;

	public static LocationData getLocationData() {
		return lastLocation;
	}

	public static LocationData getRandom() {
		return LocationData.values[Misc.randomNoPlus(LocationData.values.length)];
	}

	public static void spawnStar() {
		if (starExists()) {
			return;
		}

		LocationData randomLocation = getRandom();
		if (randomLocation == lastLocation) {
			randomLocation = getRandom();
		}

		lastLocation = randomLocation;
		ShootingStar.setStartDustLeft(StarInfo.STAR1.getDustAmount());
		PlayerHandler.messageAllPlayers(
				"<img=9> <shad=1><col=FF9933>A shooting star has just crashed " + randomLocation.getClue() + "!");
		spawnStarObject(randomLocation.getSpawnPos());
	}

	private static void spawnStarObject(Location loc) {
		RSObject object = new RSObject(StarInfo.STAR1.getObjectId(), loc.getX(), loc.getY(), loc.getZ(), Misc.random(3),
				10, -1, 6000);
		ObjectManager.addObject(object);// original time 12000
	}

	public static boolean starExists() {
		return lastLocation != null && ObjectManager.getObject(lastLocation.getSpawnPos().getX(),
				lastLocation.getSpawnPos().getY(), 0) != null;
	}

}
