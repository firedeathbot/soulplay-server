package com.soulplay.content.event.randomevent;

import com.soulplay.content.event.PkingPotEvent;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class WishingWell {

	private enum LuckTable {
		NONE(1_000_000_000L, 0),
		LOW(1_500_000_000L, 20),
		MEDIUM(2_000_000_000L, 40),
		HIGH(2_500_000_000L, 60),
		CRITICAL(3_000_000_000L, 80);

		private static LuckTable getLuck(long amount) {
			if (amount > CRITICAL.amount) {
				return CRITICAL;
			}
			if (amount > HIGH.amount) {
				return HIGH;
			}
			if (amount > MEDIUM.amount) {
				return MEDIUM;
			}
			if (amount > LOW.amount) {
				return LOW;
			}
			return NONE;
		}

		long amount;

		int percentage;

		private LuckTable(long amount, int percentage) {
			this.amount = amount;
			this.percentage = percentage;
		}

		private int getPercentage() {
			return percentage;
		}
	}

	public static final int WELL_ID = 26945;

	private static final int TRESHOLD = 500_000_000; // Minimum that needs to be
													// donated to trigger luck.

	private static WishingWell well = new WishingWell();

	public static WishingWell getWell() {
		return well;
	}

	private long wellFund = 0;
	private boolean pickedReward = false;

	public void addToWell(Player c, int amount) {
		c.startAnimation(385);
		if (fillWell(amount, c)) {
			c.sendMessage("You fill the well with riches, you can see " + (wellFund / 1_000_000) + "M down there!");
		}
	}

	private boolean fillWell(int amount, Player player) {
		wellFund += amount;
		if (amount < TRESHOLD) {
			return true;
		}

		LuckTable luck = LuckTable.getLuck(wellFund);
		if (Misc.random(100) < luck.getPercentage()) {
			wellFund = 0;
			pickedReward = false;
			
			PkingPotEvent.addGold(amount);
			
			if (player.getDonatedTotalAmount() >= 5000 || Misc.random(2) == 0) {

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
					@Override
					public void stop() {
					}

					@Override
					public void execute(CycleEventContainer container) {
						container.stop();

						buildDialogueForWinner(player);
					}
				}, 1);

			} else {
				randomEvent();
			}
			
			return false;
		}
		return true;
	}
	
	private void buildDialogueForWinner(Player player) {
		if (SpawningStar.starExists()) {
			player.getDialogueBuilder().sendOption("1 Hour Double Exp", ()-> {
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				for (Player p : PlayerHandler.players) {
					if (p == null) {
						continue;
					}

					p.setDoubleExpLength(p.getDoubleExpLength() + 6000);
				}

				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=0000FF>Everyone has been awarded 60 minutes of double XP by the well.");
			}, "Wildywyrm", ()-> {
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				WildyWyrm.spawnWorm();
			}, "Magical Crate", ()->{
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				MagicalCrate.spawnWorldCrate();
			}, "Random", ()-> {
				
			});
		} else {
			player.getDialogueBuilder().sendOption("1 Hour Double Exp", ()-> {
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				for (Player p : PlayerHandler.players) {
					if (p == null) {
						continue;
					}

					p.setDoubleExpLength(p.getDoubleExpLength() + 6000);
				}

				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=0000FF>Everyone has been awarded 60 minutes of double XP by the well.");
			}, "Shooting Star", ()-> {
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				SpawningStar.spawnStar();
			}, "Wildywyrm", ()-> {
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				WildyWyrm.spawnWorm();
			}, "Magical Crate", ()->{
				pickedReward = true;
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");
				MagicalCrate.spawnWorldCrate();
			}, "Random", ()-> {
				
			});
		}

		player.getDialogueBuilder().onReset(() -> {
			if (!pickedReward) {
				WishingWell.getWell().randomEvent();
			}

			pickedReward = false;
		}).execute();
	}
	
	private void randomEvent() {
		
		PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=FF9933>The wishing well lights up...");

		switch (Misc.random(4)) {
		case 0:
		case 1:
			for (Player p : PlayerHandler.players) {
				if (p == null) {
					continue;
				}

				p.setDoubleExpLength(p.getDoubleExpLength() + 6000);
			}

			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=0000FF>Everyone has been awarded 60 minutes of double XP by the well.");
			break;
		case 2:
			WildyWyrm.spawnWorm();
			break;
		case 3:
			if (SpawningStar.starExists())
				randomEvent();
			else
				SpawningStar.spawnStar();
			break;
		case 4:
			MagicalCrate.spawnWorldCrate();
			break;
		}
	}
	
	public void handleGoldDialogue(Player player) {
		player.getDialogueBuilder().
		sendOption("Throw the money down the well.", ()-> {
			int amount = player.getItems().getItemAmount(995);
			if (player.getItems().deleteItem2(995, amount)) {
				player.sendMessage("You throw the coins down the well.");
				WishingWell.getWell().addToWell(player, amount);
				if (amount > 500000000) {
					Titles.throwMillions(player);
				}
			}
		} , "Keep the money.", ()->{
			player.getDialogueBuilder().close();
			}
		).execute();
	}
	
	public void handleBulgBagDialogue(Player player, int bagId, int amount) {
		
		player.getDialogueBuilder().
		sendOption("Throw the tax bag down the well.", ()-> {
			if (player.getItems().deleteItem2(bagId, 1)) {
				player.sendMessage("You throw the bag of coins down the well.");
				WishingWell.getWell().addToWell(player, amount);
				Titles.throwMillions(player);
			}
		} , "Keep the money bag.", ()->{
			player.getDialogueBuilder().close();
			}
		).execute();
	}
	
}
