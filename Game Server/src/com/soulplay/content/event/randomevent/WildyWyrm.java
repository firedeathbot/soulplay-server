package com.soulplay.content.event.randomevent;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class WildyWyrm {

	public static enum WormLocationData {
		LOCATION_1(Location.create(3103, 3895), "South from Mage arena",
				"in Wilderness"),
		LOCATION_2(Location.create(3239, 3736), "North of Chaos Temple",
				"in Wilderness"),
		LOCATION_3(Location.create(3232, 3641), "at Chaos Temple",
				"in Wilderness"),
		LOCATION_4(Location.create(3306, 3799), "North of Venenatis",
				"in Wilderness");
		
		public static final WormLocationData[] values = values();

		private Location spawnPos;

		private String clue;

		public String playerPanelFrame;

		private WormLocationData(Location spawnPos, String clue,
				String playerPanelFrame) {
			this.setSpawnPos(spawnPos);
			this.clue = clue;
			this.playerPanelFrame = playerPanelFrame;
		}

		public Location getSpawnPos() {
			return spawnPos;
		}

		public void setSpawnPos(Location spawnPos) {
			this.spawnPos = spawnPos;
		}
	}

	private static WormLocationData LAST_LOCATION = null;

	public static int healAmount = 50;

	public static WormLocationData getRandom() {
		WormLocationData worm = WormLocationData.values[Misc
				.random(WormLocationData.values.length - 1)];
		return worm;
	}

	public static WormLocationData getWormLocationData() {
		return LAST_LOCATION;
	}

	private static void spawnWildyWorm(Location loc) {
		NPC wildywyrm = NPCHandler.spawnNpc2(6090,
				WildyWyrm.getWormLocationData().getSpawnPos().getX(),
				WildyWyrm.getWormLocationData().getSpawnPos().getY(), 0, 0);
		wildywyrm.setAliveTick(18000); // 1000 = 1 hour
		wildywyrm.setCanWalk(false);
	}

	public static void spawnWorm() {

		WormLocationData locationData = getRandom();
		if (LAST_LOCATION != null) {
			if (locationData == LAST_LOCATION) {
				locationData = getRandom();
			}
		}
		LAST_LOCATION = locationData;
		PlayerHandler.messageAllPlayers(
				"<img=9> <shad=1><col=FF9933>A wild worm has appeared "
						+ locationData.clue + "!");
		spawnWildyWorm(locationData.getSpawnPos());
	}

}
