package com.soulplay.content.event.randomevent;

import com.soulplay.content.npcs.impl.BorkNPC;
import com.soulplay.game.model.npc.NPCHandler;

public class Bork {

	public static boolean borkAlive = false;

	public static void spawnBork() {
		if (borkAlive) {
			return;
		}

		NPCHandler.spawnNpc(BorkNPC.ID, 1406, 4416, 0);
		borkAlive = true;
	}

}
