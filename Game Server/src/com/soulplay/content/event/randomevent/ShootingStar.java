package com.soulplay.content.event.randomevent;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.content.player.skills.mining.Pickaxes;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.skills.SkillHandler;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

public class ShootingStar {

	public enum StarInfo {
		STAR1(38660, 90, 210, 2924), // 1200
		STAR2(38661, 80, 145, 1724), // 700
		STAR3(38662, 70, 114, 1024), // 439
		STAR4(38663, 60, 71, 585), // 250
		STAR5(38664, 50, 47, 335), // 175
		STAR6(38665, 40, 32, 160), // 80
		STAR7(38666, 30, 29, 80), // 40
		STAR8(38667, 20, 25, 40), // 25
		STAR9(38668, 10, 14, 15); // 15

		public static final Map<Integer, StarInfo> values = new HashMap<>();
		private final int objectId, miningLevelReq, miningExp, dustAmount;

		private StarInfo(int objectId, int level, int exp, int dust) {
			this.objectId = objectId;
			this.miningLevelReq = level;
			this.miningExp = exp;
			this.dustAmount = dust;
		}

		public int getDustAmount() {
			return dustAmount;
		}

		public int getMiningExp() {
			return miningExp;
		}

		public int getMiningLevelReq() {
			return miningLevelReq;
		}

		public int getObjectId() {
			return objectId;
		}

		static {
			for (StarInfo data : values()) {
				values.put(data.getObjectId(), data);
			}
		}
		
	    public static void load() {
	    	/* empty */
	    }
	}

	private static int startDustLeft = 0;

	public static int getStartDustLeft() {
		return startDustLeft;
	}

	public static void inspectStar(Client c, GameObject gameObject) {
		if (gameObject == null) {
			c.sendMessage("Game object is null");
			return;
		}

		StarInfo starInfo = StarInfo.values.get(gameObject.getId());
		if (starInfo == null) {
			return;
		}

		int percent = (startDustLeft * 100 / StarInfo.STAR1.getDustAmount());
		c.sendMessage("This star has been mined about " + percent + " percent.");
	}

	public static void mineStar(final Client c, final int objectId,
			final int obX, final int obY) {
		if (c.skillingEvent != null) {
			return;
		}

		if (!SkillHandler.hasInventorySpace(c, "mining")) {
			Mining.resetMining(c);
			return;
		}

		final GameObject gameObject = RegionClip.getGameObject(objectId, obX, obY, c.getHeightLevel());
		if (gameObject == null) {
			c.sendMessage("Game object is null");
			return;
		}

		StarInfo starInfo = StarInfo.values.get(objectId);
		if (starInfo == null) {
			return;
		}

		if (c.getPlayerLevel()[Skills.MINING] < starInfo.getMiningLevelReq()) {
			c.sendMessage("Your mining level is not high enough to mine this star.");
			return;
		}

		final Pickaxes pickaxe = Mining.getPickaxe(c);
		if (pickaxe == null) {
			return;
		}

		c.sendMessage("You swing your pick at the star.");

		c.startAnimation(pickaxe.getAnimation());

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (SpawningStar.getLocationData() == null) {
					container.stop();
					return;
				}

				if (!SkillHandler.hasInventorySpace(c, "mining")) {
					container.stop();
					return;
				}

				if (c.getDonatedTotalAmount() > 999) {
					if (Misc.random(115) > c.getPlayerLevel()[Skills.MINING]) {
						return;
					}
				}
				if (c.getDonatedTotalAmount() < 1000) {
					if (Misc.random(130) > c.getPlayerLevel()[Skills.MINING]) {
						return;
					}
				}

				RSObject star = ObjectManager.getObject(gameObject.getX(), gameObject.getY(), gameObject.getZ());

				if (ShootingStar.getStartDustLeft() < 1 || star == null) {
					container.stop();
					return;
				}

				// can mine
				c.startAnimation(pickaxe.getAnimation());
				c.getItems().addItem(13727, 1);
				c.sendMessageSpam("You manage to mine some stardust.");
				c.getPA().addSkillXP(Mining.applyMiningXpBoosts(c, starInfo.getMiningExp()), Skills.MINING);
				ShootingStar.setStartDustLeft(ShootingStar.getStartDustLeft() - 1);

				if (ShootingStar.getStartDustLeft() == StarInfo.STAR2
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR2.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR3
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR3.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR4
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR4.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR5
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR5.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR6
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR6.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR7
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR7.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR8
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR8.getObjectId());
				} else if (ShootingStar.getStartDustLeft() == StarInfo.STAR9
						.getDustAmount()) {
					star.changeObject(StarInfo.STAR9.getObjectId());
				}

				if (ShootingStar.getStartDustLeft() == 0) {
					star.changeObject(-1);
					// spawn npc
					NPC alien = NPCHandler.spawnNpc2(8091,
							SpawningStar.getLocationData().getSpawnPos().getX(),
							SpawningStar.getLocationData().getSpawnPos().getY(),
							0, 0);
					alien.setAliveTick(200);
				}

				if (Misc.random(100) == 7) {
					if (c.getItems().freeSlots() > 1) {
						c.getItems().addItem(Mining.getRandomGem(), 1);
						c.sendMessage("You get a gem!");
					}
				} else if (Misc.random(1000) == 7) {
					if (c.getItems().freeSlots() > 1) {
						if (Misc.random(2) <= 1) {
							c.getItems().addItem(1631, 1);
						} else {
							c.getItems().addItem(6571, 1);
						}
						c.sendMessage("You get a rare gem!");
					}
				}

			}

			@Override
			public void stop() {
				c.skillingEvent = null;
				Mining.resetMining(c);
			}
		}, 4);
	}

	public static void setStartDustLeft(int startDustLeft) {
		ShootingStar.startDustLeft = startDustLeft;
	}

}
