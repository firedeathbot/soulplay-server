package com.soulplay.content.event.randomevent;

import java.util.LinkedList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.minigames.lastmanstanding.LootManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class MagicalCrate {

	public static final int CRATE_OBJECT_ID = 29081;
	private static int crateTimer = 6000;
	public static int MAGICAL_CRATE_ITEM_ID = 8871;
	private static Graphic magicCrateGfx = Graphic.create(Graphic.getOsrsId(580));

	public static final Animation PICKUP_ANIM = new Animation(4192);
	public static final Animation PICKUP_ANIM_DELAYED = new Animation(4192, Misc.serverToClientTick(1));

	public static enum LocationData {
		LOCATION_1(Location.create(9598, 10119), "in the revenant cave!"),
		LOCATION_2(Location.create(9553, 10108), "in the revenant cave!"),
		LOCATION_3(Location.create(9597, 10143), "in the revenant cave!"),
		LOCATION_4(Location.create(9623, 10143), "in the revenant cave!"),
		LOCATION_5(Location.create(9613, 10161), "in the revenant cave!"),
		LOCATION_6(Location.create(9585, 10165), "in the revenant cave!"),
		LOCATION_7(Location.create(9582, 10190), "in the revenant cave!"),
		LOCATION_8(Location.create(9589, 10217), "in the revenant cave!"),
		LOCATION_9(Location.create(9616, 10192), "in the revenant cave!"),
		LOCATION_10(Location.create(9641, 10207), "in the revenant cave!"),
		LOCATION_11(Location.create(9632, 10174), "in the revenant cave!"),
		LOCATION_12(Location.create(9652, 10161), "in the revenant cave!");

		private static final LocationData[] values = values();
		private final Location spawnPos;
		private final String clue;

		private LocationData(Location spawnPos, String clue) {
			this.spawnPos = spawnPos;
			this.clue = clue;
		}

		public Location getSpawnPos() {
			return spawnPos;
		}

		public String getClue() {
			return clue;
		}
	}

	public static enum LMSLocationData {
		LOCATION_LMS1(Location.create(1852, 3787), "rocky outcrops!"),
		LOCATION_LMS2(Location.create(1849, 3869), "mountain!"),
		LOCATION_LMS3(Location.create(1881, 3890), "Trinity Outpost!"),
		LOCATION_LMS4(Location.create(1805, 3834), "Debator hideout!"),
		LOCATION_LMS5(Location.create(1820, 3818), "Debator hideout!"),
		LOCATION_LMS6(Location.create(1858, 3805), "Moser settlement!"),
		LOCATION_LMS7(Location.create(1877, 3824), "Moser settlement!"),
		LOCATION_LMS8(Location.create(1893, 3806), "Moser settlement!");

		private static final LMSLocationData[] values = values();
		private final Location spawnLMSPos;
		private final String LMSClue;

		private LMSLocationData(Location spawnLMSPos, String LMSClue) {
			this.spawnLMSPos = spawnLMSPos;
			this.LMSClue = LMSClue;
		}

		public Location getLMSSpawnPos() {
			return spawnLMSPos;
		}

		public String getLMSClue() {
			return LMSClue;
		}
	}

	private static LocationData lastLocation = null;

	public static LocationData getLocationData() {
		return lastLocation;
	}

	public static LocationData getRandom() {
		return LocationData.values[Misc.randomNoPlus(LocationData.values.length)];
	}

	public static LMSLocationData getRandomLMS() {
		return LMSLocationData.values[Misc.randomNoPlus(LMSLocationData.values.length)];
	}

	public static void spawnWorldCrate() {

		LocationData randomLocation = getRandom();
		if (randomLocation == lastLocation) {
			randomLocation = getRandom();
		}

		lastLocation = randomLocation;
		PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>A magical loot crate has appeared " + randomLocation.getClue() + "!");
		spawnCrateObject(randomLocation.getSpawnPos());

	}

	private static void spawnCrateObject(Location loc) {
		RSObject object = new RSObject(CRATE_OBJECT_ID, loc.getX(), loc.getY(), loc.getZ(), Misc.random(3),
				10, -1, crateTimer);
		ObjectManager.addObject(object);
	}

	public static boolean dropMagicalCrateInWild(Client c) {
		if (c.wildLevel > 0 && c.getItems().playerHasEquipped(MagicalCrate.MAGICAL_CRATE_ITEM_ID)) {
			c.getItems().deleteEquipment(MagicalCrate.MAGICAL_CRATE_ITEM_ID, PlayerConstants.playerWeapon);
			GroundItem magicalCrate = new GroundItem(MagicalCrate.MAGICAL_CRATE_ITEM_ID, c.absX, c.absY, c.getHeightLevel(), 1, 500);
			ItemHandler.createGroundItem(magicalCrate, true);
			return true;
		}
		return false;
	}  

	public static void lootLmsCrate(final Client c, GameObject o) {
		if (!o.isActive())
			return;
		c.dynObjectManager.revertObject(o);
		LootManager.addLoot(c, false);
		Server.getStillGraphicsManager().createGfx(o.getX(), o.getY(), o.getZ(), magicCrateGfx.getId(), 0, 0);
	}

	public static void pickupCrate(final Client c, GameObject gameObject, RSObject rsObject) {

		if (gameObject == null) {
			c.sendMessage("Game object is null");
			return;
		}

		if (!gameObject.isActive()) {
			c.sendMess("Game object is not active.");
			return;
		}

		if (!c.getItems().playerHasEquippedWeapon(-1) || !c.getItems().playerHasEquippedShield(-1)) {
			c.sendMessage("You need to have both hands empty to carry this crate!");
			return;
		}

		if (c.isDead() || c.getSkills().getLifepoints() <= 0) {
			c.sendMess("You are dead!!!! XD");
			return;
		}
		
		if (rsObject == null || !rsObject.isActive()) {
			c.sendMess("RSObject no longer there.");
			return;
		}

		if (c.performingAction) {
			return;
		}

		rsObject.setIsActive(false);
		rsObject.setTick(3);

		gameObject.setActive(false);

		c.startAnimation(c.getMovement().isMovingOrHasWalkQueue() ? PICKUP_ANIM_DELAYED : PICKUP_ANIM);
		c.setPauseAnimReset(5);
		
		AnnouncementHandler.removeAnnouncement();

		c.performingAction = true;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int timer = 5;

			@Override
			public void execute(CycleEventContainer container) {

				if (c.disconnected || c == null) {
					container.stop();
					return;
				}

				if (timer == 5) {
				}

				if (timer == 3) {
					lastLocation = null;
					c.getItems().setEquipment(MAGICAL_CRATE_ITEM_ID, 1, PlayerConstants.playerWeapon);
					c.getItems().refreshWeapon();
					PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>"
							+ c.getNameSmartUp() + " picked up the Magical crate in revenant cave in level " + c.wildLevel
							+ " Wildy!");
				}

				timer--;
				if (timer == 0) {
					container.stop();
				}
			}

			@Override
			public void stop() {
				c.performingAction = false;
			}
		}, 1);
	}

	public static boolean pickingUpCrateInWildAsItem(Player player, int itemId) {
		if (itemId == MagicalCrate.MAGICAL_CRATE_ITEM_ID && player.wildLevel > 0) {
			if (player.getItems().hasEquippedWeapon(-1) && player.getItems().playerHasEquippedShield(-1)) {
				if (player.wildLevel > 10) {
					PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + player.getNameSmartUp() + " picked up the Magical crate in level " + player.wildLevel +" Wilderness!");
				}
				player.getItems().setEquipment(MagicalCrate.MAGICAL_CRATE_ITEM_ID, 1, PlayerConstants.playerWeapon);
				player.getPacketSender().playSound(Sounds.PICKUP_ITEM);
				player.getItems().refreshWeapon();
				return true;
			}
		}
		return false;
	}

	public static boolean hasCrateEquippedInWild(Player player) {
		return player.wildLevel > 0 && player.getItems().playerHasEquippedWeapon(MAGICAL_CRATE_ITEM_ID);
	}

	public static boolean handleOpenMagicalCrate(Client c, int itemId) {
		if (itemId != MAGICAL_CRATE_ITEM_ID) {
			return false;
		}

		c.getDialogueBuilder().sendOption("Open the crate.", ()-> {
			addReward(c);
		}, "Don't open the crate.", ()-> {}).executeIfNotActive();

		return true;
	}

	public static void addReward(Client c) {//random reward
		if (!c.getItems().playerHasItem(MAGICAL_CRATE_ITEM_ID)) {
			c.sendMessage("Stop right there! You have no crate in your inventory.");
			return;
		}

		int rewardsAmount = Misc.random(7) == 7 ? 2 : 1;//2+Misc.random(5);
		
		c.getItems().deleteItemInOneSlot(MAGICAL_CRATE_ITEM_ID, 1);

		for (int i = 0; i < rewardsAmount; i++) {
			int random = Misc.randomNoPlus(1000);
			if (random >= 500) {
				rewards(c, COMMON);
			} else if (random >= 100) {
				rewards(c, UNCOMMON);
			} else if (random >= 20) {
				rewards(c, RARE);
			} else if (random >= 5) {
				rewards(c, SUPER_RARE);
			} else {
				rewards(c, EXTREMELY_RARE);
			}
		}
	}

	private static void rewards(Client c, int[][] items) {
		int random;
		List<Integer> indices = new LinkedList<>();
		for (int i = 0, length = items.length; i < length; i++) {
			indices.add(i);
		}
		if (indices.size() == 0) {
			return;
		}

		random = indices.get(Misc.randomNoPlus(indices.size()));

		c.getItems().addOrDrop(new GameItem(items[random][0], Misc.randomNoPlus(items[random][1], items[random][2])));
		c.sendMessage("You received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from a magical crate!");
		if (items == RARE || items == SUPER_RARE || items == EXTREMELY_RARE) {
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + c.getNameSmartUp() + " received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from the magical crate!");
		}
	}

	public static final int[][] COMMON = { 
			{995, 10_000_000, 30_000_000}, // money
			{995, 10_000_000, 30_000_000}, // money
			{995, 10_000_000, 30_000_000}, // money
			{995, 10_000_000, 30_000_000}, // money
			{995, 10_000_000, 30_000_000}, // money
			{4151, 1, 1}, // whip
			{989, 1, 1}, //crystal key
			{5013, 1, 1}, //mining helmet
			{9013, 1, 1}, // skull sceptre
			{15241, 1, 1}, // hand cannon
			{8781, 100, 150}, //teak planks
			{537, 50, 150}, //dragon bones
	};

	public static final int[][] UNCOMMON = { 
			{995, 50_000_000, 120_000_000},
			{995, 50_000_000, 120_000_000},
			{995, 50_000_000, 120_000_000},
			{995, 50_000_000, 120_000_000},
			{18831, 50, 140}, //frost dragon bones 
			{8783, 100, 200}, // mahogany planks
			{19780, 1, 1},//corrupt dragon armour
			{13958, 1, 1},
			{13961, 1, 1},
			{13964, 1, 1},
			{13967, 1, 1},
			{13970, 1, 1},
			{13973, 1, 1},
			{13976, 1, 1},
			{13979, 1, 1},
			{13982, 1, 1},
			{13985, 1, 1},
			{13988, 1, 1}, // corrupt dragon armour
			{8786, 1, 1}, // marble block
			{8785, 1, 3}, // gold leaf

	};

	public static final int[][] RARE = { 
			{995, 180_000_000, 350_000_000},
			{995, 180_000_000, 350_000_000},
			{995, 180_000_000, 350_000_000},
			{8788, 1, 1}, // magic stone
			{8787, 1, 3}, //marble block
			{8785, 3, 8}, // gold leaf
			{14484, 1, 1}, //dragon claws 
			{19780, 1, 1}, //korasi
			{12602, 1, 1}, //ring of the gods (i)
			{12604, 1, 1}, //tyrannical ring (i)
			{12606, 1, 1}, //treasonous ring (i)
			{299, 30, 50}, //mithril seeds
			//{21446, 1, 1}, //constructor hat
			//{21447, 1, 1}, //constructor garb
			//{21448, 1, 1}, //constructor trousers
			//{21449, 1, 1}, //constructor gloves
			//{21450, 1, 1}, //constructor boots
	};

	public static final int[][] SUPER_RARE = { 
			{995, 350_000_000, 600_000_000},
			{995, 350_000_000, 600_000_000},
			{995, 350_000_000, 600_000_000},
			{786, 1, 1}, // 10$ scroll
			{8789, 1, 2}, // magic stone
			{13748, 1, 1}, // divine sigil
			{4273, 1, 1}, // golden key
			{30057, 1, 1}, // lava dragon mask
	};

	public static final int[][] EXTREMELY_RARE = { 
			{10835, 1, 1}, // bulging bag
			{10835, 1, 1}, // bulging bag
			{10835, 1, 1}, // bulging bag
			{10835, 1, 1}, // bulging bag
			{30022, 1, 1}, // dragon warhammer
			{30053, 1, 1}, // bucket helm(g)
			{13642, 1, 1}, // omni-talisman staff
			{30058, 1, 1}, // black h'ween
	};
}
