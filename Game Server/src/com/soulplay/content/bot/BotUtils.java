package com.soulplay.content.bot;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import com.soulplay.Config;
import com.soulplay.config.WorldType;
import com.soulplay.content.items.Food;
import com.soulplay.content.items.Food.FoodToEat;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.bot.tasks.*;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

/**
 * Contains helper methods for bot actions such as eating and moving
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotUtils {

	/**
	 * Creating a seed for password generation
	 */
	private static SecureRandom random = new SecureRandom();

	/**
	 * Generates a bot name (To be rewritten to pull names from RS later)
	 *
	 * @param usernameLength
	 *            - Characters in the username
	 * @return generated username of the designated number of characters
	 */
	public static String generateUsername(int usernameLength) {
		Random r = new Random();
		StringBuilder sb = new StringBuilder(usernameLength);
		for (int i = 0; i < usernameLength; i++) {
			char tmp = (char) ('a' + r.nextInt('z' - 'a'));
			sb.append(tmp);
		}
		return sb.toString();
	}

	/**
	 * Generates a password
	 *
	 * @return new password
	 */
	public static String nextSessionId() {
		return new BigInteger(60, random).toString(32);
	}

	/**
	 * The bot utilizing the utilities
	 */
	private BotClient bot;

	// public void speak(String msg) {
	// bot.forcedChat(msg);
	// for (Player p : PlayerHandler.players) {
	// if (p == null) continue;
	// if (p.isBot()) continue;
	// if (p.withinDistance(bot)) {
	// p.sendMessage(bot.getName()+": <col=ff>"+msg);
	// }
	// }
	// }

	/**
	 * Creates util class for a bot
	 *
	 * @param bot
	 *            - Bot receiving util class
	 */
	public BotUtils(BotClient bot) {
		this.bot = bot;
	}

	/**
	 * Attack the nearest NPC with the name given
	 *
	 * @param type
	 * @return if attack was successful
	 */
	public boolean attackNearestNPC(String name) {
		name = name.toLowerCase();

		NPC nearest = null;
		for (NPC n : NPCHandler.npcs) {
			if (n != null && Misc.distanceToPoint(bot.getX(), bot.getY(),
					n.getX(), n.getY()) > 25) {
				continue;
			}
			if (n != null && n.def.name.toLowerCase().contains(name)) {
				if (nearest == null) {
					nearest = n;
				} else {
					double distance = Misc.distanceToPoint(nearest.getX(),
							nearest.getY(), bot.getX(), bot.getY()); // Misc.distance(nearest.getX()
																		// -
																		// bot.getX(),
																		// nearest.getY()
																		// -
																		// bot.getY());
					double distance2 = Misc.distanceToPoint(n.getX(), n.getY(),
							bot.getX(), bot.getY()); // Misc.distance(n.getX() -
														// bot.getX(), n.getY()
														// - bot.getY());
					if (distance2 < distance && n.underAttackBy == 0) {
						nearest = n;
					}
				}
			}
		}
		if (nearest == null) {
			return false;
		}
		bot.getCombat().attackNpc(nearest.npcIndex);
		bot.npcIndex = nearest.npcIndex;
		return true;
	}

	public void bankAll() {
		bot.bankingTab = 0;
		for (int i = 0; i < bot.playerItems.length; i++) {
			bot.getItems().bankItem(bot.playerItems[i] - 1, i,
					bot.playerItemsN[i]);
		}
	}

	public void bankItem(int itemId, int amount) {
		bot.bankingTab = 0;
		bot.bankingTab = bot.getItems().getTabforItem(itemId);

		if (!bot.getItems().bankItem(itemId, bot.getItems().getItemSlot(itemId),
				amount)) {
			System.out.println("Unable to bank itemID:" + itemId + " for [BOT]"
					+ bot.getName());
		}
		bot.bankingTab = 0;
	}

	public void clearInvAndEquip() {
		for (int i = 0; i < bot.playerItems.length; i++) {
			bot.playerItems[i] = 0;
			bot.playerItemsN[i] = 0;
		}
		for (int j = 0; j < bot.playerEquipment.length; j++) {
			bot.playerEquipment[j] = -1;
			bot.playerEquipmentN[j] = 0;
		}
		bot.getPA().resetMovementAnimation();
		bot.getItems().resetBonus();
		bot.getItems().getBonus();
	}

	/**
	 * Eats food
	 */
	public void eatFood() {
		for (int i = 0; i < bot.playerItems.length; i++) {
			FoodToEat food = Food.forId(bot.playerItems[i] - 1);
			if (food != null) {
				Food.eat(bot, food, bot.playerItems[i] - 1, i);
			}
		}
	}

	public void fixBank() {
		for (int i = 0; i < bot.bankItems1.length; i++) {
			if (bot.bankItems1[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems2.length; i++) {
			if (bot.bankItems2[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems3.length; i++) {
			if (bot.bankItems3[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems4.length; i++) {
			if (bot.bankItems4[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems5.length; i++) {
			if (bot.bankItems5[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems6.length; i++) {
			if (bot.bankItems6[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems7.length; i++) {
			if (bot.bankItems7[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}
		for (int i = 0; i < bot.bankItems8.length; i++) {
			if (bot.bankItems8[i] > 0) {
				bot.getItems().toTab(0, i);
			}
		}

	}

	public boolean getFood(int amount) {
		for (int i = 0; i < bot.bankItems0.length; i++) {
			if (Food.isFood(bot.bankItems0[i]) && bot.bankItems0N[i] > amount) {
				withdrawItemFromBank(bot.bankItems0[i], amount);
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks how much food is in inventory
	 *
	 * @return amount of food in inventory
	 */
	public int getFoodCount() {
		int count = 0;
		for (int playerItem : bot.playerItems) {
			// System.out.println(i + " " + bot.playerItems[i].getName());
			if (Food.isFood(playerItem - 1)) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Gets percent hp remaining
	 *
	 * @return percent hp remaining
	 */
	public float getPercentHp() {
		return bot.getCurrentHp() / (float) bot.getMaxHp();
	}

	public void giveBotStarter() {

		bot.getItems().addItem(995, 500000);
		bot.getItems().addItem(1725, 1);
		bot.getItems().addItem(554, 1000);
		bot.getItems().addItem(555, 1000);
		bot.getItems().addItem(556, 1000);
		bot.getItems().addItem(558, 1000);
		bot.getItems().addItem(560, 1000);
		bot.getItems().addItem(565, 1000);
		bot.getItems().addItem(1323, 1);
		bot.getItems().addItem(841, 1);
		bot.getItems().addItem(882, 1000);
		bot.getItems().addItem(10499, 1);
		bot.getItems().addItem(380, 200);
		bot.getItems().addItem(542, 1);
		bot.getItems().addItem(544, 1);

		// bot.getItems().addItem(2, 400); //cannon balls
		// bot.getItems().addItem(6, 1); //cannon base
		// bot.getItems().addItem(8, 1); //cannon stand
		// bot.getItems().addItem(10, 1); //cannon barrel
		// bot.getItems().addItem(12, 1); //cannon furnace

	}

	public void pureMeleeWelfareSet() {
		// Top - iron platebody
		bot.getItems().addItem(1115, 1); // iron platebody
		bot.getItems().wearItem(1115, 0); // wear item
		// bottom - rune legs
		bot.getItems().addItem(1067, 1); // iron platelegs
		bot.getItems().wearItem(1067, 0); // wear item
		// the rest
		bot.getItems().addItem(3753, 1); // warrior helmet
		bot.getItems().wearItem(3753, 0); // wear item

		bot.getItems().addItem(7461, 1); // dragon gloves
		bot.getItems().wearItem(7461, 0); // wear item

		bot.getItems().addItem(6528, 1); // obby maul - regular weapon
		bot.getItems().wearItem(6528, 0); // wear item

		bot.getItems().addItem(2550, 1); // ring of recoil
		bot.getItems().wearItem(2550, 0); // wear item

		bot.getItems().addItem(11128, 1); // berserker necklace
		bot.getItems().wearItem(11128, 0); // wear item

		bot.getItems().addItem(6568, 1); // obsidian cape
		bot.getItems().wearItem(6568, 0); // wear item

		// climbing boots
		bot.getItems().addItem(3105, 1); // Climbing boots
		bot.getItems().wearItem(3105, 0); // wear item

		// dragon dagger or gmaul - spec weapon
		// if (Misc.random(99) < 75) {
		// bot.getItems().addItem(5698, 1); // dragon dagger p++
		// } else {
		bot.getItems().addItem(4153, 1); // gmaul
		// }

		// pots
		bot.getItems().addItem(3024, 2); // super restores(4) x 2
		bot.getItems().addItem(2436, 1); // super attack(4)
		bot.getItems().addItem(2440, 1); // super strength(4)
		bot.getItems().addItem(2442, 1); // super defence(4)

		bot.getItems().addItem(385, 21); // sharks

	}

	public void randomClothes() {
		bot.playerAppearance[0] = Misc.random(1); // gender
		if (bot.playerAppearance[0] == 0) { // male appearance

			bot.playerAppearance[1] = Misc.random(8); // head
			bot.playerAppearance[2] = 18 + Misc.random(7);// Torso
			bot.playerAppearance[3] = 26 + Misc.random(5); // arms
			bot.playerAppearance[4] = 33 + Misc.random(1); // hands
			bot.playerAppearance[5] = 36 + Misc.random(4); // legs
			bot.playerAppearance[6] = 42 + Misc.random(1); // feet
			bot.playerAppearance[7] = 10 + Misc.random(7); // beard
			bot.playerAppearance[8] = Misc.random(11); // hair colour
			bot.playerAppearance[9] = Misc.random(15); // torso colour
			bot.playerAppearance[10] = Misc.random(15); // legs colour
			bot.playerAppearance[11] = Misc.random(5); // feet colour
			bot.playerAppearance[12] = Misc.random(7); // skin colour

		} else {

			bot.playerAppearance[1] = 45 + Misc.random(9); // head
			bot.playerAppearance[2] = 56 + Misc.random(4);// Torso
			bot.playerAppearance[3] = 61 + Misc.random(4); // arms
			bot.playerAppearance[4] = 67 + Misc.random(1); // hands
			bot.playerAppearance[5] = 70 + Misc.random(7); // legs
			bot.playerAppearance[6] = 79 + Misc.random(1); // feet
			bot.playerAppearance[7] = -1; // beard
			bot.playerAppearance[8] = Misc.random(11); // hair colour
			bot.playerAppearance[9] = Misc.random(15); // torso colour
			bot.playerAppearance[10] = Misc.random(15); // legs colour
			bot.playerAppearance[11] = Misc.random(5); // feet colour
			bot.playerAppearance[12] = Misc.random(7); // skin colour

		}

	}

	public BotTask randomTasks() {
		if (WorldType.isSeasonal()) {
			if (Misc.randomBoolean()) {
				return new SeasonalRandomTask();
			} else {
				return new SeasonalRerollTask();
			}
		}

		int randomTask = Misc.random(1);
		switch (randomTask) {

			/*
			 * case 1: return new FishingGuildTask(); case 2: return new
			 * FishingCatherbyTask(); case 3: return new RocktailsTask(); case
			 * 4: return new ThievingTask(); case 5: return new
			 * ExploringHomeTask(); case 6: return new
			 * WoodCuttingAtCamelotTask(); case 7: return new
			 * MiningAtNeitiznotTask(); case 8: return new
			 * MiningAtFaladorTask(); case 9: return new PrayerTask(); case 10:
			 * return new FireMakingTask(); case 11: return new
			 * SlotMachineTask(); case 12: return new AFKTask();
			 */
			case 1:
				return new MeleeRockCrabsTask();
			// case 14:
			// return new MeleeEdgeManTask();

		}
		return null;
	}

	public void setCombat99() {
		for (int skill = 0; skill < 25; skill++) {
			if (skill >= 0 && skill <= 6) {
				bot.getSkills().setStaticLevel(skill, 99);
			}
			if (skill > 7) {
				return;
			}
		}
	}

	public void setPureMelee() {
		
		bot.getSkills().setStaticLevel(Skills.HITPOINTS, 99);
		bot.getSkills().setStaticLevel(Skills.ATTACK, 50);
		bot.getSkills().setStaticLevel(Skills.STRENGTH, 99);
		bot.getSkills().setStaticLevel(Skills.PRAYER, 95);

	}

	public void skull() {
		bot.setSkullTimer(Config.SKULL_TIMER);
		bot.setSkull(true);
	}

	public void targetPlayerInWild(String playerName) {
		final Client o = (Client) PlayerHandler.getPlayerSmart(playerName);
		if (o == null) {
			return;
		}
		bot.setPlayerToKill(o);
		bot.playerIndex = o.getId();
	}

	/**
	 * Teleports home
	 */
	public void teleHome() {
		// bot.getPA().startTeleport2(Config.RESPAWN_X, Config.RESPAWN_Y, 0);
		bot.getPA().startTeleport(LocationConstants.EDGEVILLE_X,
				LocationConstants.EDGEVILLE_Y, 0, TeleportType.MODERN);
	}

	/**
	 * Walk to coordinates
	 *
	 * @param loc
	 */
	public void walkTo(Location loc) {
		bot.getPA().walkTo(loc.getX(), loc.getY());
	}

	/**
	 * Walk to random location in a square area
	 *
	 * @param bottomRight
	 *            - Bottom right square
	 * @param topLeft
	 *            - Top left square
	 */
	public void walkToArea(Location bottomRight, Location topLeft) {
		int x = bottomRight.getX()
				+ Misc.random(topLeft.getX() - bottomRight.getX());
		int y = bottomRight.getY()
				+ Misc.random(topLeft.getY() - bottomRight.getY());
		bot.getPA().walkTo(x, y);
	}

	public void welfareStarterSet() {
		// Top - rune plate or fighter torso
		if (Misc.random(99) < 50) {
			bot.getItems().addItem(10551, 1); // fighter torso
			bot.getItems().wearItem(10551, 0); // wear item
		} else {
			bot.getItems().addItem(1127, 1); // rune platebody
			bot.getItems().wearItem(1127, 0); // wear item
		}
		// bottom - rune legs
		bot.getItems().addItem(1079, 1); // rune platelegs
		bot.getItems().wearItem(1079, 0); // wear item
		// the rest
		bot.getItems().addItem(10828, 1); // helm of neitiznot
		bot.getItems().wearItem(10828, 0); // wear item

		bot.getItems().addItem(7461, 1); // dragon gloves
		bot.getItems().wearItem(7461, 0); // wear item

		bot.getItems().addItem(4587, 1); // dragon scimitar - regular weapon
		bot.getItems().wearItem(4587, 0); // wear item

		bot.getItems().addItem(2550, 1); // ring of recoil
		bot.getItems().wearItem(2550, 0); // wear item

		bot.getItems().addItem(1725, 1); // str ammy
		bot.getItems().wearItem(1725, 0); // wear item

		bot.getItems().addItem(6568, 1); // obsidian cape
		bot.getItems().wearItem(6568, 0); // wear item

		// climbing boots or rune boots
		if (Misc.random(99) < 50) {
			bot.getItems().addItem(3105, 1); // Climbing boots
			bot.getItems().wearItem(3105, 0); // wear item
		} else {
			bot.getItems().addItem(4131, 1); // rune boots
			bot.getItems().wearItem(4131, 0); // wear item
		}

		// shield or defender
		if (Misc.random(99) < 50) {
			bot.getItems().addItem(1201, 1); // rune kite shield
			bot.getItems().wearItem(1201, 0); // wear item
		} else {
			bot.getItems().addItem(20072, 1); // dragon defender
			bot.getItems().wearItem(20072, 0); // wear item
		}

		// dragon dagger or gmaul - spec weapon
		if (Misc.random(99) < 75) {
			bot.getItems().addItem(5698, 1); // dragon dagger p++
		} else {
			bot.getItems().addItem(4153, 1); // gmaul
		}

		// pots
		bot.getItems().addItem(3024, 2); // super restores(4) x 2
		bot.getItems().addItem(2436, 1); // super attack(4)
		bot.getItems().addItem(2440, 1); // super strength(4)
		bot.getItems().addItem(2442, 1); // super defence(4)

		// veng runes
		bot.getItems().addItem(560, 80); // death runes
		bot.getItems().addItem(9075, 160); // astral runes
		bot.getItems().addItem(557, 400); // earth runes

		bot.getItems().addItem(385, 18); // sharks

	}

	public void withdrawItemFromBank(int itemId, int amount) {
		bot.bankingTab = 0;
		bot.bankingTab = bot.getItems().getTabforItem(itemId);

		bot.getItems().fromBank(itemId, bot.getItems().getBankItemSlot(itemId),
				amount);

		bot.bankingTab = 0;

	}

}
