package com.soulplay.content.bot;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Json bot serializer
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotSerializer implements JsonSerializer<BotData> {

	@Override
	public JsonElement serialize(BotData src, Type type,
			JsonSerializationContext context) {
		JsonObject object = new JsonObject();
		object.addProperty("username", src.getUsername().replace(" ", "_"));
		object.addProperty("password", src.getPassword());
		object.addProperty("type", src.getTypeOfBot());
		object.addProperty("chanceToPlay", src.getChanceToPlay());
		object.addProperty("beginPlaying", src.getBeginPlaying().toString());
		object.addProperty("sessionLength",
				src.getSessionDuration().toString());
		return object;
	}

}
