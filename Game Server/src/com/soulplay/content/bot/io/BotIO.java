package com.soulplay.content.bot.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.soulplay.content.bot.BotConfig;
import com.soulplay.content.bot.BotData;
import com.soulplay.content.bot.BotDeserializer;
import com.soulplay.content.bot.BotHandler;
import com.soulplay.content.bot.BotSerializer;

/**
 * Handles saving and loading json files
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotIO {

	/**
	 * Loads all the json data files, decides bot session lengths, and places
	 * them into the offlineBots array
	 */
	public static void loadDataFiles() {
		for (File f : (new File(BotConfig.DATA_PATH)).listFiles()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(f));
				Gson gson = new GsonBuilder().registerTypeAdapter(BotData.class,
						new BotDeserializer()).create();
				Type type = new TypeToken<BotData>() {
				}.getType();
				BotData bot = gson.fromJson(reader, type);
				bot.decideSessionLength();
				BotHandler.getOfflineBots().add(bot);
				reader.close();
			} catch (JsonIOException e) {
				e.printStackTrace();
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Saves a json data file for a bot
	 *
	 * @param data
	 *            bot data to save to file
	 */
	public static void save(BotData data) {
		try {
			Gson gson = new GsonBuilder()
					.registerTypeAdapter(BotData.class, new BotSerializer())
					.setPrettyPrinting().create();
			FileWriter file = new FileWriter(
					BotConfig.DATA_PATH + data.getUsername() + ".json");
			String out = gson.toJson(data);
			file.write(out);
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
