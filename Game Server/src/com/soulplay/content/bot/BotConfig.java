package com.soulplay.content.bot;

import com.soulplay.util.Time;

/**
 * Config for bots
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotConfig {

	public static final String[] botNames = {"dalros1", "joooii", "skill4life",
			"pkl33t7",
			"frags"/*
					 * , "hellokitty91", "ironman1000", "i pk u i i",
					 * "evolutionZX", "fish l1fe", "karrrl", "blank bank",
					 * "cranksh4ft", "i ate you", "pvm l0ser", "drunk driver",
					 * "bull dog33", "zShadowZ", "Legendary97", "good 4 u",
					 * "guudaahla", "indi minn", "mikael1994", "fun jon",
					 * "mygfisjunky", "instantpk666", "billskill", "toilet98",
					 * "henrient", "hundur20", "biggestloser", "froskur",
					 * "wreckuhard", "antania", "trausti", "kaaaaaaanta",
					 * "followbro", "2000model", "diplom", "proteinshake"
					 */};

	public static final String[] botNames1 = {"kanya", "tittli123", "killu",
			"2fast4u", "mixes"};

	/**
	 * Path where bot data is saved
	 */
	public final static String DATA_PATH = "./Data/bots/data/";

	/**
	 * Types of possible bots (should be used in bot task decisions)
	 */
	public final static String[] BOT_TYPES = {"pve", "pvp", "skiller"};

	/**
	 * Minimum playtime of a bot in one session
	 */
	public final static Time MIN_SESSION_LENGTH = new Time("20:00");

	/**
	 * Max playtime of a bot in one session
	 */
	public final static Time MAX_SESSION_LENGTH = new Time("40:00");

	/**
	 * Minimum time between new bots being created
	 */
	public final static Time MIN_TIME_BETWEEN_NEW_BOTS = new Time("00:01");

	/**
	 * Max time between new bots being created
	 */
	public final static Time MAX_TIME_BETWEEN_NEW_BOTS = new Time("00:02");
}
