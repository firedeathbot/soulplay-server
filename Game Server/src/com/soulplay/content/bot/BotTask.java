package com.soulplay.content.bot;

import com.soulplay.util.Misc;

/**
 * A task for a bot
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public abstract class BotTask {

	/**
	 * What stage in the action the bot currently is
	 */
	public int stage = 0;

	/**
	 * Delay until next step/action
	 */
	public long nextActionDelay = 0;

	/**
	 * Time of last action
	 */
	public long lastAction = 0;

	/**
	 * Checks whether the bot can do the task
	 *
	 * @param bot
	 *            bot to check for
	 * @return whether the bot can do the task
	 */
	public abstract boolean canBegin(BotClient bot);

	/**
	 * Process bot's task while utilizing a delay
	 */
	public void delayedProcess(BotClient bot) {
		// if (bot.isDead || bot.getCurrentHp() <= 0) {
		//// bot.setAction(BotHandler.getNextAction(bot));
		// bot.getCombat().resetPlayerAttack();
		// sleep(7000);
		// } else
		if (System.currentTimeMillis() - lastAction >= nextActionDelay) {
			process(bot);
		}
	}

	/**
	 * Ends the bot's current task
	 *
	 * @param bot
	 *            bot losing its task
	 */
	public void endTask(BotClient bot) {
		bot.setAction(null);
	}

	public abstract int getStage();

	/**
	 * Creates a copy of the task
	 *
	 * @return new copy of task
	 */
	public abstract BotTask newInstance();

	/**
	 * The process of doing the action
	 */
	public abstract void process(BotClient bot);

	/**
	 * Delay the next loop of process
	 *
	 * @param delay
	 *            length of delay in miliseconds
	 */
	public void sleep(long delay) {
		lastAction = System.currentTimeMillis();
		nextActionDelay = delay;
	}

	/**
	 * Delay the next loop of process with randomness
	 *
	 * @param delay
	 *            length of delay in miliseconds
	 * @param deviation
	 *            min and max deviation from delay
	 */
	public void sleep(long delay, long deviation) {
		long newDelay = delay - deviation + Misc.random(deviation * 2);
		sleep(newDelay >= 0 ? newDelay : 0);
	}

}
