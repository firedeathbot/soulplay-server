package com.soulplay.content.bot;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.soulplay.util.Time;

/**
 * Json bot deserializer
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotDeserializer implements JsonDeserializer<BotData> {

	@Override
	public BotData deserialize(JsonElement json, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		String user = json.getAsJsonObject().get("username").getAsString();
		String pass = json.getAsJsonObject().get("password").getAsString();
		String typeOfBot = json.getAsJsonObject().get("type").getAsString();
		float chanceToPlay = json.getAsJsonObject().get("chanceToPlay")
				.getAsFloat();
		Time beginPlaying = new Time(
				json.getAsJsonObject().get("beginPlaying").getAsString());
		Time sessionLength = new Time(
				json.getAsJsonObject().get("sessionLength").getAsString());
		return new BotData(user, pass, typeOfBot, beginPlaying, sessionLength,
				chanceToPlay);
	}

}
