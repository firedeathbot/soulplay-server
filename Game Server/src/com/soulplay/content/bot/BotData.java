package com.soulplay.content.bot;

import com.soulplay.content.bot.io.BotIO;
import com.soulplay.util.Misc;
import com.soulplay.util.Time;

/**
 * Contains all the unique data of each bot such as when and for how long it
 * will play, login data, etc
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotData {

	/**
	 * Bot username and password
	 */
	private String user, pass;

	/**
	 * Type of bot
	 */
	private String typeOfBot;

	/**
	 * Bot's chance to play a session at the scheduled begin time
	 */
	private float chanceToPlay = 0;

	/**
	 * Bot's scheduled session beginning time, max session length, and current
	 * session length
	 */
	private Time beginPlaying, sessionDuration, currentSessionDuration;

	/**
	 * Bot client that will connect to the server
	 */
	private BotClient botClient;

	/**
	 * Create new bot data file and save it
	 *
	 * @param name
	 *            - Name of bot
	 */
	public BotData(String name) {
		this.user = name;
		this.pass = BotUtils.nextSessionId();
		this.typeOfBot = BotConfig.BOT_TYPES[Misc
				.random(BotConfig.BOT_TYPES.length - 1)];
		this.beginPlaying = new Time(System.currentTimeMillis());
		this.sessionDuration = this.currentSessionDuration = Time
				.getRandomTimeBetween(BotConfig.MIN_SESSION_LENGTH,
						BotConfig.MAX_SESSION_LENGTH);
		BotIO.save(this);
	}

	/**
	 * Load bot data file
	 *
	 * @param botName
	 *            - Name of bot
	 * @param pass
	 *            - Password of bot
	 * @param typeOfBot
	 *            - Type of bot
	 * @param beginPlaying
	 *            - Time bot will begin playing
	 * @param sessionDuration
	 *            - Max bot session length
	 * @param chanceToPlay
	 *            - Chance bot will play
	 */
	public BotData(String botName, String pass, String typeOfBot,
			Time beginPlaying, Time sessionDuration, float chanceToPlay) {
		this.user = botName;
		this.pass = pass;
		this.typeOfBot = typeOfBot;
		this.beginPlaying = beginPlaying;
		this.sessionDuration = sessionDuration;
		this.chanceToPlay = chanceToPlay;
	}

	/**
	 * Sets the next session length
	 */
	public void decideSessionLength() {
		int x = sessionDuration.toMinutes() / 2;
		currentSessionDuration = new Time(0, Misc.random(x) + x);
	}

	/**
	 * Disconnects bot from server and disposes of the BotClient object
	 */
	public void destroyBot() {
		if (botClient == null) {
			return;
		}
		botClient.endSession();
		new Thread(() -> {
			while (botClient.isOnline()) {
			}
			botClient = null;
		}).start();
	}

	/**
	 * Gets time the bot will begin to play
	 *
	 * @return time bot will begin to play
	 */
	public Time getBeginPlaying() {
		return beginPlaying;
	}

	/**
	 * Gets the bot's chance to play a session
	 *
	 * @return bot's chance to play a session
	 */
	public float getChanceToPlay() {
		return chanceToPlay;
	}

	/**
	 * Get the length of the bot's current session
	 *
	 * @return length of bot's current session
	 */
	public Time getCurrentSessionDuration() {
		return currentSessionDuration;
	}

	/**
	 * Get bot password
	 *
	 * @return bot password
	 */
	public String getPassword() {
		return pass;
	}

	/**
	 * Get the length of the bot's average session
	 *
	 * @return length of bot's average session
	 */
	public Time getSessionDuration() {
		return sessionDuration;
	}

	/**
	 * Get the type of bot
	 *
	 * @return - Bot type (Source - BotConfig.BOT_TYPES)
	 */
	public String getTypeOfBot() {
		return typeOfBot;
	}

	/**
	 * Get bot username
	 *
	 * @return bot username
	 */
	public String getUsername() {
		return user;
	}

	/**
	 * Initialize the bot and connects it to the server
	 */
	public void initBot() {
		botClient = new BotClient(user, pass);
		botClient.connect();
	}

}
