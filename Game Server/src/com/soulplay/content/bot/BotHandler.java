package com.soulplay.content.bot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.content.bot.io.BotIO;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;
import com.soulplay.util.Time;

/**
 * Handles bot sessions and creation
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotHandler {

	public static Time nextBotCreatedAt;

	/**
	 * Lists of online and offline bots
	 */
	private static ArrayList<BotData> onlineBots = new ArrayList<>();

	private static ArrayList<BotData> offlineBots = new ArrayList<>();

	/**
	 * List of possible bot actions
	 */
	private static ArrayList<BotTask> botActions = new ArrayList<>();

	/**
	 * Loads the bot tasks
	 */
	static {
		try {
			for (@SuppressWarnings("rawtypes")
			Class c : Misc.getClasses("server.model.players.bot.tasks")) {
				if (c.getName().contains("$")) {
					continue;
				}
				BotTask action = (BotTask) c.newInstance();
				botActions.add(action);
			}

			nextBotCreatedAt = Time.getCurrentTime()
					.add(Time.getRandomTimeBetween(
							BotConfig.MIN_TIME_BETWEEN_NEW_BOTS,
							BotConfig.MAX_TIME_BETWEEN_NEW_BOTS));
			BotIO.loadDataFiles();
			// startThread();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates a bot with the desired name
	 *
	 * @param name
	 *            username of bot
	 */
	public static void createBot(String name) {
		offlineBots.add(new BotData(name));
		print("Created new bot " + name);
	}

	/**
	 * Gets the next action for a bot to do
	 *
	 * @param bot
	 *            - Bot needing a new action
	 * @return - The action to be done
	 */
	public static BotTask getNextAction(BotClient bot) {
		List<BotTask> actions = new ArrayList<>(botActions);
		Collections.shuffle(actions);
		for (BotTask action : actions) {
			if (action.canBegin(bot)) {
				return action.newInstance();
			}
		}
		return null;
	}

	/**
	 * Gets offline bots
	 *
	 * @return list of offline bots
	 */
	public static ArrayList<BotData> getOfflineBots() {
		return offlineBots;
	}

	/**
	 * Finds the lowest open index in the players array
	 *
	 * @return - Lowest open index in players
	 */
	public static int getOpenPlayerId() {
		for (int i = 2; i < PlayerHandler.players.length; i++) {
			if (PlayerHandler.players[i] == null) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Send message to console
	 *
	 * @param message
	 *            message to send
	 */
	public static void print(String message) {
		System.out.println("[BotHandler] - " + message);
	}

	/**
	 * Creates thread handling bot logging in and out as well as bot creation
	 */
	public static void startThread() {
		new Thread(() -> {
			while (true) {
				Time currentTime = Time.getCurrentTime();
				for (int i1 = 0; i1 < offlineBots.size(); i1++) {
					BotData bot1 = offlineBots.get(i1);
					if (currentTime.isBetween(bot1.getBeginPlaying(),
							bot1.getBeginPlaying()
									.add(bot1.getCurrentSessionDuration()))) {
						onlineBots.add(bot1);
						offlineBots.remove(bot1);
						if (bot1.getChanceToPlay() <= Math.random()) {
							print("Beginning session for "
									+ bot1.getUsername());
							bot1.initBot();
						}
						i1--;
					}
				}
				for (int i2 = 0; i2 < onlineBots.size(); i2++) {
					BotData bot2 = onlineBots.get(i2);
					if (!currentTime.isBetween(bot2.getBeginPlaying(),
							bot2.getBeginPlaying()
									.add(bot2.getCurrentSessionDuration()))) {
						print("Ending session for " + bot2.getUsername());
						offlineBots.add(bot2);
						onlineBots.remove(bot2);
						bot2.decideSessionLength();
						bot2.destroyBot();
						i2--;
					}
				}
				if (currentTime.isBetween(nextBotCreatedAt,
						nextBotCreatedAt.add(new Time("00:01")))) {
					createBot(BotUtils.generateUsername(5));
					nextBotCreatedAt = nextBotCreatedAt
							.add(Time.getRandomTimeBetween(
									BotConfig.MIN_TIME_BETWEEN_NEW_BOTS,
									BotConfig.MAX_TIME_BETWEEN_NEW_BOTS));
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
