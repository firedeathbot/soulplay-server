package com.soulplay.content.bot;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.bot.tasks.BotEquipmentLoading;
import com.soulplay.content.player.bot.tasks.ThievForPureMeleeSet;
import com.soulplay.content.player.bot.tasks.WelfarePkThiev1M;
import com.soulplay.content.player.bot.tasks.combattasks.WelfareGearPkTask;
import com.soulplay.content.player.bot.tasks.combattasks.WelfarePurePkTask;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.util.Misc;
import com.soulplay.util.NameUtil;

/**
 * A bot client class
 *
 * @author Fritz (TheRedArmy - http://www.rune-server.org/members/theredarmy/)
 */
public class BotClient extends Client {

	/**
	 * Bot utiliies
	 */
	private BotUtils util = new BotUtils(this);

	/**
	 * Current bot task
	 */
	private BotTask currentTask;

	/**
	 * Bot data
	 */
	private BotData botData;

	/**
	 * If true, the bot will disconenct from the server
	 */
	private boolean endSession;

	private int idleCount = 0;

	private int prevStage = 0;

	private String botOwnerName = "";

	private Client ownerClient;

	// the player the bot is attacking or targetting
	private Client playerToKill;

	/**
	 * 0=default - 1=skiller - 2=pvmer - 3=skill+pvm - 7=welfarepkbot -
	 * 8=welfarepkbottheiving - 10=playerownedBot - 12=puremeleewelfare -
	 * 13=puremeleewelfareFarm
	 */
	private int typeOfBot = 0;

	/**
	 * Create a bot with specified user and pass, loads if previous save file
	 *
	 * @param name
	 *            - Bot username
	 * @param pass
	 *            - Bot password
	 */
	public BotClient(String name, String pass) {
		this(name, pass, 0);
	}

	public BotClient(String name, String pass, int typeOfBot) {
		super(null);
		setPlayerName(name);
		setDisplayName(name);
		playerPass = pass;
		int load = PlayerSave.loadGame(this, getName(), playerPass);
		if (!isActive) {
			if (load == 3) {
				return;
			}
			final int slot = Misc.findFreePlayerSlot();
			if (slot < 0)
				return;
			
			setId(slot);
			setTypeOfBot(typeOfBot);
			connect();
		}
	}

	/**
	 * Load the bot into the game
	 */
	public void connect() {
		if (PlayerHandler.isPlayerOnline(getName())) {
			return;
		}
		// handler = Server.playerHandler;
		saveCharacter = true;
		saveFile = true;
		isActive = true;
		if (addStarter) {
			if (connectedFrom == null || connectedFrom.equals("")) {
				connectedFrom = "" + Misc.random(255) + "." + Misc.random(255)
						+ "." + Misc.random(255) + "." + Misc.random(255) + "";
			}
		}
		if (connectedFrom == null || connectedFrom.equals("")) {
			if (lastConnectedFrom.size() > 0) {
				connectedFrom = lastConnectedFrom.get(0);
			} else {
				connectedFrom = "" + Misc.random(255) + "." + Misc.random(255)
						+ "." + Misc.random(255) + "." + Misc.random(255) + "";
			}
		}

		if (typeOfBot == 12 || typeOfBot == 13) {
			fightMode = 2;
		}

		// playerLevel[Player.playerHitpoints] = 10;
		// getItems().getBonus();
		if (addStarter && typeOfBot == 7) { // mains pkers with welfare set
			// give bot welfare set
			getUtils().setCombat99();
			getUtils().welfareStarterSet();
			typeOfBot = 8;
		} else if (addStarter && typeOfBot == 12) {
			getUtils().setPureMelee();
			getUtils().pureMeleeWelfareSet();
			typeOfBot = 13;
		} else if (addStarter) {
			getUtils().giveBotStarter();
		}
		PlayerHandler.players[getId()] = this;

		String username = this.getName();
		PlayerHandler.getPlayerMap().put(NameUtil.encodeBase37(username), this);
		PlayerHandler.getPlayerMapByMid().put((long)this.mySQLIndex, this);
		this.setLongName(Misc.playerNameToInt64(this.getName()));

		if (WorldType.isSeasonal()) {
		      for (int i = 0; i < getPlayerLevel().length; i++) {
                  getSkills().setStaticLevel(i, Skills.maxLevels[i]);
              }

              calcCombat();
              int[] random = Misc.random(BotEquipmentLoading.looks);
              playerAppearance = random;

              Inventory equipment = Misc.random(BotEquipmentLoading.equipments);
              for (int i = 0; i < equipment.capacity(); i++) {
            	  Item item = equipment.get(i);
            	  if (item == null) {
            		  continue;
            	  }

            	  playerEquipment[i] = item.getId();
            	  playerEquipmentN[i] = item.getAmount();
              }
		}

		onInit();
		initialized = true;
		isActive = true;
		correctlyInitialized = true;
		setLoadedRegion(true);
		// loginPacketsSent = true;
		setIsActiveAtomicBoolean(true);
	}

	/**
	 * Disconnects the bot from the server
	 */
	public void disconnect() {
		PlayerSaveSql.savePlayer(this); // PlayerSave.saveGame(this);
		hidePlayer = true;
		endSession = false;
		disconnected = true;
	}

	/**
	 * Qeueus the bot to disconnect as soon as its current task is over
	 */
	public void endSession() {
		endSession = true;
	}

	public String getBotOwnerName() {
		return this.botOwnerName;
	}

	public int getCurrentHp() {
		return getPlayerLevel()[3];
	}

	/**
	 * Gets bot data
	 *
	 * @return bot data
	 */
	public BotData getData() {
		return botData;
	}

	public int getMaxHp() {
		return getSkills().getMaximumLifepoints();

	}

	public Client getOwnerClient() {
		return ownerClient;
	}

	public Client getPlayerToKill() {
		return playerToKill;
	}

	/**
	 * 0=default - 1=skiller - 2=pvmer - 3=skill+pvm - 7=welfarepkbot -
	 * 8=welfarepkbottheiving - 10=playerownedBot
	 */
	public int getTypeOfBot() {
		return typeOfBot;
	}

	/**
	 * Gets bot utilities
	 *
	 * @return bot utilities
	 */
	public BotUtils getUtils() {
		return util;
	}

	public boolean hasAction() {
		if (currentTask == null) {
			return false;
		}
		return true;
	}

	@Override
	public void onInit() {

		// fun command
		if (addStarter) {
			getUtils().randomClothes();
		}

		getItems().resetBonus();
		getItems().getBonus();
		getItems().setEquipment(playerEquipment[PlayerConstants.playerHat], 1,
				PlayerConstants.playerHat);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerCape], 1,
				PlayerConstants.playerCape);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerAmulet],
				1, PlayerConstants.playerAmulet);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerArrows],
				playerEquipmentN[PlayerConstants.playerArrows],
				PlayerConstants.playerArrows);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerChest], 1,
				PlayerConstants.playerChest);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerShield],
				1, PlayerConstants.playerShield);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerLegs], 1,
				PlayerConstants.playerLegs);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerHands], 1,
				PlayerConstants.playerHands);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerFeet], 1,
				PlayerConstants.playerFeet);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerRing], 1,
				PlayerConstants.playerRing);
		getItems().setEquipment(playerEquipment[PlayerConstants.playerWeapon],
				playerEquipmentN[PlayerConstants.playerWeapon],
				PlayerConstants.playerWeapon);
		getCombat().getPlayerAnimIndex(
				playerEquipment[PlayerConstants.playerWeapon]);

		calcCombat();
		hidePlayer = false;
	}

	@Override
	public boolean isBot() {
		return true;
	}

	/**
	 * Checks if the bot is online
	 *
	 * @return Whether bot is connected to server
	 */
	public boolean isOnline() {
		return disconnected;
	}

	/**
	 * Runs the bot's tasks
	 */
	@Override
	public void process() {
		super.process();

		if (currentTask != null) {
			currentTask.delayedProcess(this);

		} else if (endSession) {
			endSession = false;
			disconnect();
		} else {
			// currentTask = BotHandler.getNextAction(this);
		}

		if (currentTask == null) { // get bots with no tasks
			if (typeOfBot == 0) { // get default bots to do random tasks in the
									// utils randomTasks function.
				// System.out.println("type of bot "+typeOfBot);
				BotTask task = getUtils().randomTasks();
				if (task != null && task.canBegin(this)) {
					currentTask = task.newInstance();
				}
				task = null;
				// getUtils().randomTasks();
			} else if (typeOfBot == 10) { // player owned bots to follow
											// constantly when no tasks are
											// running
				if (followId == 0) {
					if (ownerClient != null && !ownerClient.toggleBot) {
						followId = ownerClient.getId();
					}
				}
			} else if (typeOfBot == 7) { // Get welfare pk bots that have no
											// tasks

				if (playerEquipment[PlayerConstants.playerChest] < 1) {
					typeOfBot = 8;
					// BotTask task2 = new WelfarePkThiev1M();
					currentTask = /* task2.newInstance(); */new WelfarePkThiev1M();
				} else {
					// BotTask task3 = new WelfareGearPkTask();
					currentTask = /* task3.newInstance(); */ new WelfareGearPkTask();
				}
			} else if (typeOfBot == 8) { // gets welfare pk bots that have to do
											// theiving

				// if (getName().equalsIgnoreCase("sipp"))
				// System.out.println("botname null task "+getName());
				// BotTask task4 = new WelfarePkThiev1M();
				// setAction(task4);
				currentTask = /* task4.newInstance(); */ new WelfarePkThiev1M();
			} else if (typeOfBot == 12) {
				if (playerEquipment[PlayerConstants.playerChest] < 1) {
					typeOfBot = 8;
					// BotTask task2 = new ThievForPureMeleeSet();
					currentTask = new ThievForPureMeleeSet();// task2.newInstance();
				} else {
					// BotTask task3 = new WelfarePurePkTask();
					currentTask = new WelfarePurePkTask();// task3.newInstance();
				}
			} else if (typeOfBot == 13) {
				// BotTask task4 = new ThievForPureMeleeSet();
				currentTask = new ThievForPureMeleeSet();// task4.newInstance();
			}
		}
		super.process();
		// System.out.println("running task?");

		if (currentTask != null && typeOfBot == 0) { // kick idlers
			if (currentTask.getStage() != prevStage) {
				idleCount = 0;
			}
			idleCount++;
			if (idleCount > 2400) {
				setAction(null);
				return;
			}
			prevStage = currentTask.getStage();
		}

		if (currentTask == null && typeOfBot != 7 && typeOfBot != 10) {
			idleCount++;
			if (idleCount >= 10) {
				disconnected = true;
				disconnect();
			}
		}

	}

	/**
	 * Sets the bot's current action
	 *
	 * @param nextAction
	 */
	public void setAction(BotTask nextAction) {
		currentTask = nextAction;
	}

	public void setBotOwner(String ownerName) {
		this.botOwnerName = ownerName;
	}

	public void setOwnerClient(Client ownerClient) {
		this.ownerClient = ownerClient;
	}

	public void setPlayerToKill(Client playerToKill) {
		this.playerToKill = playerToKill;
	}

	/**
	 * 0=default - 1=skiller - 2=pvmer - 3=skill+pvm - 7=welfarepkbot -
	 * 8=welfarepkbottheiving - 10=playerownedBot
	 */
	public void setTypeOfBot(int typeOfBot) {
		this.typeOfBot = typeOfBot;
	}

}
