package com.soulplay.content.dialogue;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.soulplay.Config;
import com.soulplay.content.minigames.junkmachine.JunkMachine;
import com.soulplay.content.player.FishingContest;
import com.soulplay.content.player.combat.specattack.Special;
import com.soulplay.content.player.holiday.halloween.Halloween;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;

public class DialogueHandler {

	private Player c;

	private static final int[] statementIds = { 357, 360, 364, 369 };

	public DialogueHandler(Player client) {
		this.c = client;
	}

	/*
	 * Information Box
	 */

	/**
	 * Handles all talking
	 *
	 * @param dialogue
	 *            The dialogue you want to use
	 * @param npcId
	 *            The npc id that the chat will focus on during the chat
	 */
	public void sendDialogues(int dialogue, int npcId) {
		c.talkingNpc = npcId;
		switch (dialogue) {
		
		case 8591:
			c.getDH().sendOption2("Open Reward Interface", "Open Shop");
			c.dialogueAction = 8591;
			break;

		case 5031:
			c.getDH().sendOption2("Yes", "No");
			c.dialogueAction = 5032;
			break;

		case 5032:
			c.getDH().sendNpcChat2("If youtube did not open visit", "<col=ff>https://www.youtube.com/watch?v=pJcsqk42sDY",
					5032, "Captain Cain");
			c.nextChat = 0;
			break;

		case 5029:
			c.getDH().sendOption2("Level Up Ur Roles", "Open Reward Shop");
			c.dialogueAction = 5029;
			break;

		case 599:
			c.getDH().sendOption2("Accept", "Decline");
			c.dialogueAction = 23;
			c.nextChat = 0;
			break;

		case 600:
			c.getDH().sendOption4("<col=ff0000>Attacker", "<col=ff>Defender", "<col=ff00>Healer", "<col=ffff00>Collector");
			c.dialogueAction = 21;
			c.nextChat = 0;
			break;

		case 8900:
			c.getDH().sendOption2("Open Shop", "I would like to exchange my set.");
			c.dialogueAction = 8900;
			break;
		case 1981:
			c.getDH().sendOption2("Yes, Change my account to regular player",
					"No, i would like to keep playing as Ironman");
			c.dialogueAction = 1981;
			break;
		case 1980:
			sendOption2("Open skillcape shop", "Change from Ironman to regular player");
			c.dialogueAction = 1980;
			break;
		case 8948:
			sendNpcChat2("Hello I sell alot of different types of armour sets",
					"and I can exchange any type of armour set for you.", c.talkingNpc, "Armour set Exchange");
			c.nextChat = 0;
			break;
		// Start of halloween event
			
		case 27254:
			c.getDH().sendOption3("Help the city Varrock", "Help the city Falador", "Help Maggie");
			c.dialogueAction = 27255;
			break;
		case 82010:
			sendNpcChat2("You can trick-or-treat people in Rimmington", "and Port Sarim thats all you need to know.",
					c.talkingNpc, "Wendy");
			c.nextChat = 0;
			break;
		case 27290:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 27291;
			break;
		case 27291:
			sendNpcChat1("Trick, please", c.talkingNpc, "Ahab");
			c.nextChat = 27292;
			break;
		case 27292:
			Halloween.halloweenMonkOfEntrana(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Ahab");
			c.nextChat = 0;
			break;
		case 26920:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 26921;
			break;
		case 26921:
			sendNpcChat1("Trick, please", c.talkingNpc, "Ahab");
			c.nextChat = 26922;
			break;
		case 26922:
			Halloween.halloweenAhab(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Ahab");
			c.nextChat = 0;
			break;
		case 5830:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5831;
			break;
		case 5831:
			sendNpcChat1("Trick, please", c.talkingNpc, "Betty");
			c.nextChat = 5832;
			break;
		case 5832:
			Halloween.halloweenBetty(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Betty");
			c.nextChat = 0;
			break;
		case 8501:
			if (c.getPartner() != null) {
				sendNpcChat3(
						"You currently have " + c.getSlayerPoints().getTotalValue() + " slayer points and "
								+ c.getDuoPoints().getTotalValue() + " Duo Points.",
						"You have completed " + c.getSlayerTasksCompleted() + " Tasks in a row.",
						"Your currently slayer partner is " + c.getPartner().getNameSmartUp(),
						8273, "Turael");
			} else {
				sendNpcChat2(
						"You currently have " + c.getSlayerPoints().getTotalValue() + " slayer points and "
								+ c.getDuoPoints().getTotalValue() + " Duo Points.",
						"You have completed " + c.getSlayerTasksCompleted() + " Tasks in a row.",
						8273, "Turael");
			}
			c.nextChat = 0;
			break;
		case 8500:
			sendNpcChat4("Duo slayer means you can do a task with another player.",
					"It's way more fun slaying together than doing it alone.",
					"You can start by using an enchanted gem on the user you",
					"would like to duo with.", 8273, "Turael");
			c.nextChat = 0;
			break;
		case 5590:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5591;
			break;
		case 5591:
			sendNpcChat1("Trick, please", c.talkingNpc, "Brian");
			c.nextChat = 5592;
			break;
		case 5592:
			Halloween.halloweenBrian(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Brian");
			c.nextChat = 0;
			break;
		case 5560:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5561;
			break;
		case 5561:
			sendNpcChat1("Trick, please", c.talkingNpc, "Grum");
			c.nextChat = 5562;
			break;
		case 5562:
			Halloween.halloweenGrum(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Grum");
			c.nextChat = 0;
			break;
		case 5570:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5571;
			break;
		case 5571:
			sendNpcChat1("Trick, please", c.talkingNpc, "Wydin");
			c.nextChat = 5572;
			break;
		case 5572:
			Halloween.halloweenWydin(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Wydin");
			c.nextChat = 0;
			break;
		case 11699:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 11700;
			break;
		case 11700:
			sendNpcChat1("Trick, please", c.talkingNpc, "Gerrant");
			c.nextChat = 11701;
			break;
		case 11701:
			Halloween.halloweenGerrant(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Gerrant");
			c.nextChat = 0;
			break;
		case 3070:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 3071;
			break;
		case 3071:
			sendNpcChat1("Trick, please", c.talkingNpc, "Hetty");
			c.nextChat = 3072;
			break;
		case 3072:
			Halloween.halloweenHetty(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Hetty");
			c.nextChat = 0;
			break;
		case 5290:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5291;
			break;
		case 5291:
			sendNpcChat1("Trick, please", c.talkingNpc, "Shop assistant");
			c.nextChat = 5292;
			break;
		case 5292:
			Halloween.halloweenShopAssistant(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Shop assistant");
			c.nextChat = 0;
			break;
		case 5260:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5261;
			break;
		case 5261:
			sendNpcChat1("Trick, please", c.talkingNpc, "Shopkeeper");
			c.nextChat = 5262;
			break;
		case 5262:
			Halloween.halloweenShopkeeper(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Shopkeeper");
			c.nextChat = 0;
			break;
		case 33800:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 33801;
			break;
		case 33801:
			sendNpcChat1("Trick, please", c.talkingNpc, "Chancy");
			c.nextChat = 33802;
			break;
		case 33802:
			Halloween.halloweenChancy(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Chancy");
			c.nextChat = 0;
			break;
		case 3400:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 3401;
			break;
		case 3401:
			sendNpcChat1("Trick, please", c.talkingNpc, "Hops");
			c.nextChat = 3402;
			break;
		case 3402:
			Halloween.halloweenHops(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Hops");
			c.nextChat = 0;
			break;
		case 3360:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 3361;
			break;
		case 3361:
			sendNpcChat1("Trick, please", c.talkingNpc, "Da vinci");
			c.nextChat = 3362;
			break;
		case 3362:
			Halloween.halloweenDaVinci(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Da vinci");
			c.nextChat = 0;
			break;
		case 5850:
			sendPlayerChat1("Trick or treat!");
			c.nextChat = 5851;
			break;
		case 5851:
			sendNpcChat1("Trick, please", c.talkingNpc, "Rommik");
			c.nextChat = 5852;
			break;
		case 5852:
			Halloween.halloweenRommik(c);
			sendNpcChat1("Okay, okay stop already! Here's your treat.", c.talkingNpc, "Rommik");
			c.nextChat = 0;
			break;
		case 80780:
			if (c.halloweenEvent == true) {
				if (c.getItems().playerHasItem(4558) && c.getItems().playerHasItem(4559)
						&& c.getItems().playerHasItem(4560) && c.getItems().playerHasItem(4561)
						&& c.getItems().playerHasItem(4562) && c.getItems().playerHasItem(4563)
						&& c.getItems().playerHasItem(4564) && c.getItems().playerHasItem(14084)
						&& c.getItems().playerHasItem(14082) && c.getItems().playerHasItem(14083)
						&& c.getItems().playerHasItem(9475) && c.getItems().playerHasItem(2209)
						&& c.getItems().playerHasItem(6794) && c.getItems().playerHasItem(2229)) {
					c.getItems().deleteItemInOneSlot(4558, 1);
					c.getItems().deleteItemInOneSlot(4559, 1);
					c.getItems().deleteItemInOneSlot(4560, 1);
					c.getItems().deleteItemInOneSlot(4561, 1);
					c.getItems().deleteItemInOneSlot(4562, 1);
					c.getItems().deleteItemInOneSlot(4563, 1);
					c.getItems().deleteItemInOneSlot(4564, 1);
					c.getItems().deleteItemInOneSlot(14084, 1);
					c.getItems().deleteItemInOneSlot(14082, 1);
					c.getItems().deleteItemInOneSlot(14083, 1);
					c.getItems().deleteItemInOneSlot(9475, 1);
					c.getItems().deleteItemInOneSlot(2209, 1);
					c.getItems().deleteItemInOneSlot(6794, 1);
					c.getItems().deleteItemInOneSlot(2229, 1);
					sendPlayerChat1("Thank you for bringing me some candy, Here take this.");
					c.getItems().addItem(15353, 1);
					c.getItems().addItem(7594, 1);
					c.nextChat = 0;
					c.halloweenEvent = true;
				} else {
					sendPlayerChat1("Happy Hallowe'en!");
					c.nextChat = 80790;
				}
			}
			break;
		case 80790:
			sendNpcChat2("Happy Hallowe'en to you as well! I must say", "absolutely favouritest of favourite holidays.",
					c.talkingNpc, "Maggie");
			c.nextChat = 80800;
			break;
		case 80800:
			sendNpcChat3("I love the decorations, the costumes, the tricks, the treats.",
					"Oh, yeah heavens, the treats!", "absolutely favouritest of favourite holidays.", c.talkingNpc,
					"Maggie");
			c.nextChat = 80810;
			break;
		case 80810:
			sendPlayerChat1("Have a bit of a sweet tooth, do you?");
			c.nextChat = 80820;
			break;
		case 80820:
			sendNpcChat3("Do i ever, cha! I keep well up-to-date on these things",
					"and heard that Southern Asgarnia was set to have the",
					"best sweeties this side of the River Salve this year.", c.talkingNpc, "Maggie");
			c.nextChat = 80830;
			break;
		case 80830:
			sendNpcChat1("The only problem is Babe over there", c.talkingNpc, "Maggie");
			c.nextChat = 80840;
			break;
		case 80840:
			sendPlayerChat1("Babe?");
			c.nextChat = 80850;
			break;
		case 80850:
			sendNpcChat3("Yes, one of my oxen. Surely you've seen her? She's",
					"come down with a little sniffle and i hate to leave", "on her own.", c.talkingNpc, "Maggie");
			c.nextChat = 80860;
			break;
		case 80860:
			sendNpcChat2("I doubt I'll be able to get out and about before the", "Hallowe'n festivites are over.",
					c.talkingNpc, "Maggie");
			c.nextChat = 80870;
			break;
		case 80870:
			sendNpcChat2("Hey, just had a thought, cha. You wouldn't happen to",
					"be trick-org-treating this year, would you?", c.talkingNpc, "Maggie");
			c.nextChat = 80880;
			break;
		case 80880:
			sendOption3("But of course i am.", "Sorry, it's not really my thing.", "How do you trick-or-treat?");
			c.dialogueAction = 80880;
			break;
		case 80890:
			sendPlayerChat1("How do you trick-or-treat?");
			c.nextChat = 80900;
			break;
		case 80900:
			sendNpcChat2("How do you trick-or-treat? My, oh my! I can hardly", "get over such a question, cha!",
					c.talkingNpc, "Maggie");
			c.nextChat = 80910;
			break;
		case 80910:
			sendNpcChat2("It's just a matter of dressing yourself up and yelling",
					"trick-or-treat at the locals, but i tell you what...", c.talkingNpc, "Maggie");
			c.nextChat = 80920;
			break;
		case 80920:
			sendNpcChat3("Have a little chat with Wendy over thataway-somewhere-",
					"yonder. She's a veritable expert on Hallowe'en. She'll", "sort you out about all the details.",
					c.talkingNpc, "Maggie");
			c.nextChat = 80930;
			break;
		case 80930:
			sendNpcChat2("But, more importantly, are you planning on groing trick-", "or-treating this year?",
					c.talkingNpc, "Maggie");
			c.nextChat = 80940;
			break;
		case 80940:
			sendOption2("But of course i am!", "Sorry it's not really my thing.");
			c.dialogueAction = 80940;
			break;
		case 80950:
			sendPlayerChat2("But of course i am! Who would pass up the chance for",
					"some free stuff - especially of the chocolate variety?");
			c.nextChat = 80960;
			break;
		case 80960:
			sendNpcChat2("Excellent, hey. Would you be willing to collect a little", "candy for me?", c.talkingNpc,
					"Maggie");
			c.nextChat = 80970;
			break;
		case 80970:
			sendOption2("Of course! What would you like?",
					"Sorry, I'm planning on collecting all the candy i can...FOR MYSELF.");
			c.dialogueAction = 80970;
			break;
		case 80980:
			sendPlayerChat1("Of course! What would you like?");
			c.nextChat = 80990;
			break;
		case 80990:
			sendNpcChat2("Oo, well, since you asked, there are several Southern",
					"Asgarnia delicacies that i'm keen to try this year.", c.talkingNpc, "Maggie");
			c.nextChat = 81000;
			break;
		case 81000:
			sendNpcChat2("I'd be most grateful if you could bring me fourteen", "different type of candies.",
					c.talkingNpc, "Maggie");
			c.nextChat = 81010;
			break;
		case 81010:
			sendPlayerChat1("Okay, i'll keep that in mind.");
			c.nextChat = 0;
			break;
		// End of halloween event

		case 3021:
			sendNpcChat1("I can take you to other farming locations.", c.talkingNpc, "Tool Leprechaun");
			c.nextChat = 3022;
			break;
		case 3022:
			sendOption5("Ardougne farming location", "Falador farming location", "Canifis farming location",
					"Catherby farming location", "Farming tree locations");
			c.dialogueAction = 3022;
			break;
		case 3023:
			sendOption5("Varrock tree location", "Lumbridge tree location", "Falador tree location",
					"Taverly tree location", "Previous");
			c.dialogueAction = 3024;
			break;
		case 1526:
			sendOption2("Armour And Weapon Shop", "Misc Shop");
			c.dialogueAction = 1526;
			break;
		case 6893:
			sendOption3("Open Pet Shop", "Incubator status", "Purchase egg growth potion.");
			c.dialogueAction = 6893;
			break;
		case 6894:
			sendNpcChat4("Do you want to buy a potion for only 6 donator points",
					"that will help you break the egg in a few seconds,",
					" but you can only use it on the incubator when", "the egg reached 1 day.", c.talkingNpc,
					"Pet Shop Owner");
			c.nextChat = 6895;
			break;
		case 6895:
			sendOption2("Buy the potion for 6 donator points", "No thanks");
			c.dialogueAction = 6895;
			break;
		case 2479:
			if (c.getItems().playerHasItem(6202)) {
				sendNpcChat2("Eeeewwww i dont eat cooked fish!", "You can uncook it with the uncooking pot.",
						c.talkingNpc, "Evil Bob");
				c.nextChat = 0;
			} else if (c.getItems().playerHasItem(6200)) {
				sendNpcChat2("Ooohhh that smells good, thank you!", "Here is some item i have found on the beach.",
						c.talkingNpc, "Evil Bob");
				FishingContest.contestReward(c);
				c.nextChat = 0;
			} else {
				sendNpcChat2("Hello, i am Evil Bob and i am veery hungry..",
						"Catch me the Rare fish and i will reward you!", c.talkingNpc, "Evil Bob");
				c.nextChat = 0;
			}
			break;
		case 8002:
			sendNpcChat2("Hello, i can take you to the boss Nex for only one",
					"vote ticket. You will skip the killcounts.", c.talkingNpc, "Jack");
			c.nextChat = 8003;
			break;
		case 8003:
			sendOption2("Take me to the Nex boss", "No thanks, maybe later.");
			c.dialogueAction = 8003;
			break;
		case 4500:
			sendNpcChat4("You can train Mining, Fishing, Woodcutting and Hunter",
					"in the Wilderness. While skilling in the Wilderness", "you will grant double experience.",
					"You can find out more by typing ::thread 8971", c.talkingNpc, "Wilderness Skilling");
			c.nextChat = 0;
			break;
		case 20795:
			sendNpcChat3("So you're a dueler aye? I got an offer you can't refuse.",
					"I got one of them fancy hats you see, the more you", "duel the fancier it gets. Whaddoyathink?",
					c.talkingNpc, "Fadli");
			c.nextChat = 20796;
			break;
		case 20796:
			sendPlayerChat1("How does this thing work?");
			c.nextChat = 20797;
			break;
		case 20797:
			sendNpcChat3("It's not that hard aye.", "Depending on the amount of duel's you've won",
					"the hat will get more detailed", c.talkingNpc, "Fadli");
			c.nextChat = 20798;
			break;
		case 20798:
			sendOption2("Accept the offer", "Refuse the offer");
			c.dialogueAction = 20798;
			break;
		case 6480:
			sendNpcChat3("Hello " + c.getNameSmartUp() + " You have reached the maximum",
					"total level. I would like to reward you for your hard work.", "Meet me in varrock castle!",
					c.talkingNpc, "King Roald");
			c.nextChat = 6481;
			break;

		case 6481:
			sendOption2("Follow the king to varrock castle.", "Go to the varrock castle later.");
			c.dialogueAction = 6481;
			break;
		// prestige 1
		case 6490:
			sendNpcChat2("You have obtained Maximum total level, so i have a ", "challenge for you.", c.talkingNpc,
					"King Roald");
			c.nextChat = 6491;
			break;

		case 6491:
			sendPlayerChat1("Alright, what challenge do you have for me?");
			c.nextChat = 6492;
			break;

		case 6492:
			sendNpcChat4("I wanna offer you to go to Lord mode, and that means",
					"Your skills will reset to 1 and your exp rate will",
					"decrease to x30 but you will have more benefits, slightly",
					"increased drop rates and some others.", c.talkingNpc, "King Roald");
			c.nextChat = 6493;
			break;

		case 6493:
			sendNpcChat2("For taking this challenge you will also", "be rewarded Lord mode capes and money.",
					c.talkingNpc, "King Roald");
			c.nextChat = 6494;
			break;

		case 6494:
			sendOption2("I would like to take the challenge.", "I do not want to take this challenge.");
			c.dialogueAction = 6494;
			break;

		// prestige 2
		case 6470:
			sendNpcChat2("You have obtained Maximum total level again!, i wanna offer you", " another challenge.",
					c.talkingNpc, "King Roald");
			c.nextChat = 6471;
			break;

		case 6471:
			sendPlayerChat1("Alright, what challenge do you have for me this time?");
			c.nextChat = 6472;
			break;

		case 6472:
			sendNpcChat4("I wanna offer you to go to Legend mode, and that means",
					"Your skills will reset to 1 and your exp rate will",
					"decrease to x20 but you will have more benefits, slightly",
					"increased drop rates and some others.", c.talkingNpc, "King Roald");
			c.nextChat = 6473;
			break;

		case 6473:
			sendNpcChat2("For taking this challenge you will also", "be rewarded Legend mode capes and money.",
					c.talkingNpc, "King Roald");
			c.nextChat = 6474;
			break;

		case 6474:
			sendOption2("I would like to take the challenge.", "I do not want to take this challenge.");
			c.dialogueAction = 6474;
			break;

		// prestige 3
		case 6460:
			sendNpcChat2("You have obtained Maximum total level once again!, would you like to ",
					"take the last and the hardest challenge?", c.talkingNpc, "King Roald");
			c.nextChat = 6461;
			break;

		case 6461:
			sendPlayerChat1("Maybe, what challenge do you have for me this time?");
			c.nextChat = 6462;
			break;

		case 6462:
			sendNpcChat4("I wanna offer you to go to Extreme mode, and that means",
					"Your skills will reset to 1 and your exp rate will",
					"decrease to x5 but you will have more benefits, slightly", "increased drop rates and some others.",
					c.talkingNpc, "King Roald");
			c.nextChat = 6463;
			break;

		case 6463:
			sendNpcChat2("For taking this challenge you will also", "be rewarded Extreme mode capes and money.",
					c.talkingNpc, "King Roald");
			c.nextChat = 6464;
			break;

		case 6464:
			sendOption2("I would like to take the challenge.", "I do not want to take this challenge.");
			c.dialogueAction = 6464;
			break;

		case 6465:
			sendOption2("I would like to upgrade to super ironman for 100M",
					"I dont want to upgrade to super ironman yet.");
			c.dialogueAction = 6465;
			break;

		case 19967:
			sendStatement("You will lose the teleporting bag when you teleport,",
					"There is no bank in the waiting room at nomad.");
			c.nextChat = 19968;
			break;
		case 19968:
			sendOption2("Teleport anyway.", "Stay here.");
			c.dialogueAction = 19968;
			break;
		/*
		 * case 12228:
		 * sendNpcChat2("Bring me three Aksel's permission (swords)",
		 * "that nomad stole from me", 12228, "Champion"); c.nextChat = 0;
		 * break; case 12229: sendNpcChat2(
		 * "Hello, Wow you have gotten the three Aksel's permission",
		 * "(swords). Now i can make my Special weapon!", 12228, "Champion");
		 * c.nextChat = 12230; break; case 12230: sendOption2("Give him",
		 * "Keep them"); c.dialogueAction = 12230; break;
		 */
		case 7410: // easter event
			sendNpcChat1("Would you like to get out of here?", 7410, "Easter Bunny Jr");
			c.nextChat = 7411;
			break;
		case 7411:
			sendOption2("Yes please.", "No thanks i will finish this.");
			c.dialogueAction = 7411;
			break;
		case 13800:
			sendNpcChat1("Grush ga ri gru! Grish!", 7201, "Chocatrice");
			c.nextChat = 13801;
			break;
		case 13801:
			sendStatement("You watch the scared, confused chocatrice dash around for a",
					"moment, you should head to Easter Bunny before going deeper", "into the warren.");
			c.nextChat = 13802;
			break;
		case 13802:
			sendStatement("Now that, you have harched a chocatrice, you dispose of the buckets",
					"you collected in the warren. They would only weigh you down.");
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.getItems().deleteItemInOneSlot(1925, 1);
			c.nextChat = 0;
			break;
		case 13803:
			sendNpcChat1("Gro, ri rant! Grul gu ragh gu.", 7201, "Chocatrice");
			c.nextChat = 13804;
			break;
		case 13804:
			sendNpcChat1("(No we won't! Ill turn you to chocolate instead.)", 7201, "Chocatrice");
			c.nextChat = 13805;
			break;
		case 13805:
			sendPlayerChat2("Oh dear the Easter Bunny didn't say anything about", "This.");
			c.nextChat = 13806;
			break;
		case 13806:
			sendNpcChat1("....", 7201, "Chocatrice");
			c.nextChat = 13807;
			break;
		case 13807:
			sendNpcChat1("(...)", 7201, "Chocatrice");
			c.nextChat = 13808;
			break;
		case 13808:
			sendPlayerChat1("Well? Get it over with then.");
			c.nextChat = 13809;
			break;
		case 13809:
			sendNpcChat1("Gru ra grough ga ghur.", 7201, "Chocatrice");
			c.nextChat = 13810;
			break;
		case 13810:
			sendNpcChat1("(I can't do it, it feels wrong.)", 7201, "Chocatrice");
			c.nextChat = 13811;
			break;
		case 13811:
			sendPlayerChat2("Ah! I guess bunny rabbits and chocatrices are on the", "same team, eh?");
			c.nextChat = 13812;
			break;
		case 13812:
			sendPlayerChat2("Ill tell you what, there's plenty of other creatures we", "can turn into chocolate.");
			c.nextChat = 13813;
			break;
		case 13813:
			sendNpcChat1("Grish ga ri gru!", 7201, "Chocatrice");
			c.nextChat = 13814;
			break;
		case 13814:
			sendNpcChat1("(I dont't think I'm angry enough to attack anything.)", 7201, "Chocatrice");
			c.nextChat = 13815;
			break;
		case 13815:
			sendPlayerChat1("Dont't worry, I'll take care of that. Lets go!");
			c.nextChat = 0;
			break;
		case 13651:
			sendNpcChat1("Well, hello there and Happy Easter!", 13651, "Easter Bunny");
			c.nextChat = 13652;
			break;
		case 13652:
			sendPlayerChat3("Happy Easter to you too. The bunny guarding the",
					"entrance said you wanted me to come down to the", "warren.");
			c.nextChat = 13653;
			break;
		case 13653:
			sendNpcChat3("You see, while i was busy decoratingi didn't notice",
					"that rodents and pests had runnelled into my chocolate",
					"store. By the time i did it was too late.", 13651, "Easter Bunny");
			c.nextChat = 13654;
			break;
		case 13654:
			sendPlayerChat2("Oh dear! All the chocolate was eaten by critters? Does",
					"that mean no chocolate eggs this year?.");
			c.nextChat = 13655;
			break;
		case 13655:
			sendNpcChat1("Don't worry, I've come up with a cunning plan.", 13651, "Easter Bunny");
			c.nextChat = 13656;
			break;
		case 13656:
			sendPlayerChat2("You're going to use the chocolate eggs i collected", "somehow?");
			c.nextChat = 13657;
			break;
		case 13657:
			sendNpcChat2("Ah, yes the fun part. Tell me have you heard of a", "creature called a 'cockatrice'?", 13651,
					"Easter Bunny");
			c.nextChat = 13658;
			break;
		case 13658:
			sendPlayerChat1("yes, i have.");
			c.nextChat = 13659;
			break;
		case 13659:
			sendNpcChat2("Wonderful, then perhaps you've heard the legend that ",
					"they can turn people to stone with their gaze.", 13651, "Easter Bunny");
			c.nextChat = 13660;
			break;
		case 13660:
			sendNpcChat2("I realised they could harness that power to turn things", "to chocolate instead.", 13651,
					"Easter Bunny");
			c.nextChat = 13661;
			break;
		case 13661:
			sendPlayerChat1("Will that work? It sounds rather strange.");
			c.nextChat = 13662;
			break;
		case 13662:
			sendNpcChat2("Well that we need here is not a cockatrice,", "but a choc-atrice!", 13651, "Easter Bunny");
			c.nextChat = 13663;
			break;
		case 13663:
			sendNpcChat1("Guess what? You're going to create this new creature.", 13651, "Easter Bunny");
			c.nextChat = 13664;
			break;
		case 13664:
			sendPlayerChat1("I wondered when this was going to involve me.");
			c.nextChat = 13665;
			break;
		case 13665:
			sendNpcChat3("I used the eggs you collected earlier to make a pool of",
					"molten chocolate. To make a chocatrice egg you need",
					"to dip a cockatrice egg into this molten chocolate.", 13651, "Easter Bunny");
			c.nextChat = 13666;
			break;
		case 13666:
			sendNpcChat2("Then, just incubate the egg and it will hatc as a", "chocatrice!", 13651, "Easter Bunny");
			c.nextChat = 13667;
			break;
		case 13667:
			sendPlayerChat2("Are you sure that will work?", "It dosen't souns very likely.");
			c.nextChat = 13668;
			break;
		case 13668:
			sendNpcChat2("Hey, you're speaking to the giant, pink, talking rabbit.",
					"That dosen't sound very likely either!", 13651, "Easter Bunny");
			c.nextChat = 13669;
			break;
		case 13669:
			sendNpcChat3("Anyway, once you've harced the cocatrice, you should",
					"guide it around the warren and turn any creatures to", "chocolate.", 13651, "Easter Bunny");
			c.nextChat = 13670;
			break;
		case 13670:
			sendPlayerChat3("So that's my mission. Make a chocatrice and use it to",
					"turn creatures in the warren to chocolate, so that you", "can make chocolate eggs?");
			c.nextChat = 13671;
			break;
		case 13671:
			sendNpcChat1("Eggs-actly! Now, any more questions?", 13651, "Easter Bunny");
			c.nextChat = 13672;
			break;
		case 13672:
			sendPlayerChat1("Nope, i will help you with the mission!");
			c.nextChat = 0;
			break;
		case 13751:
			sendPlayerChat1("It worked! I've harched a chocatrice.");
			c.transform(-1);
			c.getPA().resetMovementAnimation();
			c.nextChat = 13752;
			break;
		case 13752:
			sendNpcChat1("Yes, well done. I was watching. You were egg-cellent.", 13651, "Easter Bunny");
			c.nextChat = 13753;
			break;
		case 13753:
			sendNpcChat2("I was standing by, with a little easter magic. Now you need",
					"to go into the warren to the south.", 13651, "Easter Bunny");
			c.nextChat = 13754;
			break;
		case 13754:
			sendPlayerChat1("The warren?");
			c.nextChat = 13755;
			break;
		case 13755:
			sendNpcChat3("Oh do try to keep up. The warren is where I've been",
					"keeping the chocolate recently, wich means that is", "where the greedy little creatures are.",
					13651, "Easter Bunny");
			c.nextChat = 13756;
			break;
		case 13756:
			sendPlayerChat2("so, now I go into the warren and turn things to", "chocolate?");
			c.nextChat = 13757;
			break;
		case 13757:
			sendNpcChat3("Correct, but you won't be able to enter like that. To",
					"keep the chocatrice safely out of the way, I've sealed up", "the entrance to the warren.", 13651,
					"Easter Bunny");
			c.nextChat = 13758;
			break;
		case 13758:
			sendNpcChat2("Now, the only way in and out is through a small rabbit",
					"hole, so I'll need to change you into a bunny.", 13651, "Easter Bunny");
			c.nextChat = 13759;
			break;
		case 13759:
			sendNpcChat2("When you want to switch between human and bunny",
					"form, just come and ask me and I'll transform you.", 13651, "Easter Bunny");
			c.nextChat = 13760;
			break;
		case 13760:
			sendPlayerChat2("That sounds fun. Is there anything else i need to", "know?");
			c.nextChat = 13761;
			break;
		case 13761:
			sendNpcChat3("Yes. I need you to add the chocolate to the mixer over",
					"there. I'm going to set you a quota of 15 chocolate", "chunks", 13651, "Easter Bunny");
			c.nextChat = 13762;
			break;
		case 13762:
			sendNpcChat1("Would you like me to turn you into a bunny now?", 13651, "Easter Bunny");
			c.nextChat = 13763;
			break;
		case 13763:
			sendPlayerChat1("Yes turn me into a bunny");
			c.nextChat = 13764;
			break;
		case 13764:
			sendNpcChat1("Certainly. Here you go.", 13651, "Easter Bunny");
			c.transform(3688);
			c.getPA().resetMovementAnimation();
			c.nextChat = 13765;
			break;
		case 13765:
			sendNpcChat2("You will also need to have this ring in your inventory", "to enter the rabbit hole.", 13651,
					"Easter Bunny");
			c.getPA().movePlayer(2461, 5316, 0);
			c.nextChat = 0;
			break;
		case 13766:
			sendNpcChat2("Well done! That's certainly enough chocolate for me to", "start making chocolate eggs again.",
					13651, "Easter Bunny");
			c.nextChat = 13767;
			break;
		case 13767:
			sendPlayerChat1("Hooray!");
			c.nextChat = 13768;
			break;
		case 13768:
			sendPlayerChat1("Does that mean i can have a chocolate egg?");
			c.nextChat = 13769;
			break;
		case 13769:
			sendNpcChat1("Not right now, sorry. I've got something.", 13651, "Easter Bunny");
			c.nextChat = 13770;
			break;
		case 13770:
			sendPlayerChat1("Oh come on! I went to a lot of trouble, you know.");
			c.nextChat = 13771;
			break;
		case 13771:
			sendNpcChat2("Ahem. As i was going to say, I've got something much", "better for you", 13651,
					"Easter Bunny");
			c.nextChat = 13772;
			break;
		case 13772:
			sendNpcChat2("Chocolate goes off eventually, you know, but this is", "something you can keep forever.",
					13651, "Easter Bunny");
			c.nextChat = 13773;
			break;
		case 13773:
			sendPlayerChat1("Well, that sounds good. What is it?");
			c.nextChat = 13774;
			break;
		case 13774:
			sendNpcChat2("This cape is a memnto of your adventures with the",
					"chocatrice. If you operate it while wearing it...", 13651, "Easter Bunny");
			c.nextChat = 13775;
			break;
		case 13775:
			sendNpcChat1("...well, you'll have to wait and see.", 13651, "Easter Bunny");
			c.nextChat = 13776;
			break;
		case 13776:
			sendStatement("The Easter Bunny gives you a bunny outfit.");
			c.nextChat = 0;
			break;
		case 13777:
			sendNpcChat2("Fantastic! Go put the chocolate chunks into the", "chocolate mixer.", 13651, "Easter Bunny");
			c.nextChat = 0;
			break;
		case 23117:
			sendPlayerChat1("What a strange hole. I could almost fit inside.");
			c.nextChat = 23118;
			break;
		case 23118:
			sendStatement("You hear a mysterious voice from within the rabbit hole.");
			c.nextChat = 23119;
			break;
		case 23119:
			sendNpcChat2("Here, what's going on out there? Have you brought the", "eggs?", 7200, "Rabbit hole");
			c.nextChat = 23120;
			break;
		case 23120:
			sendPlayerChat1("OK! Are you okay down there?");
			c.nextChat = 23121;
			break;
		case 23121:
			sendNpcChat1("Never mind that, have you got the eggs?", 7200, "Rabbit hole");
			c.nextChat = 23122;
			break;
		case 23122:
			sendPlayerChat1("What eggs? This is the first time we've spoken.");
			c.nextChat = 23123;
			break;
		case 23123:
			sendNpcChat2("Really? Oh, well, perhaps you can help anyway. I need",
					"you to bring me some chocolate eggs.", 7200, "Rabbit hole");
			c.nextChat = 23124;
			break;
		case 23124:
			sendPlayerChat1("Oh! Are you the Easter Bunny?");
			c.nextChat = 23125;
			break;
		case 23125:
			sendNpcChat2("Oh no he's far too busy to guard the warren entrance.", "He's a V.I.B you know!", 7200,
					"Rabbit hole");
			c.nextChat = 23126;
			break;
		case 23126:
			sendPlayerChat1("A very important Bunny?");
			c.nextChat = 23127;
			break;
		case 23127:
			sendNpcChat2("Got it in one, kid. Anyway, i need you to go get some",
					"chocolate eggs for the Easter Bunny.", 7200, "Rabbit hole");
			c.nextChat = 23128;
			break;
		case 23128:
			sendPlayerChat2("Shouldn't the Easter Bunny be giving eggs out,", "not taking them back?");
			c.nextChat = 23129;
			break;
		case 23129:
			sendNpcChat2("Yes, but...the rodents...the eggs...", "the incubator...the...the...", 7200, "Rabbit hole");
			c.nextChat = 23130;
			break;
		case 23130:
			sendNpcChat3("Well, he has his reasons. It's a long story. All you need",
					"to know right now is that the Easter Bunny needs",
					"three chocolate eggs or the celebrations will be ruined.", 7200, "Rabbit hole");
			c.nextChat = 23131;
			break;
		case 23131:
			sendPlayerChat2("Ah - and where am i supposed to find", "these three eggs?");
			c.nextChat = 23132;
			break;
		case 23132:
			sendNpcChat4("He sent some delivery bunnies cut around this area",
					"earlier, before he discovered all the chocolate had been",
					"eaten. I'm sure that you'll be able to find some eggs", "in Yanille.", 7200, "Rabbit hole");
			c.nextChat = 23133;
			break;
		case 23133:
			sendPlayerChat1("Okay. I'll go and find you three chocolate eggs.");
			c.nextChat = 0;
			break;
		case 23136:
			sendStatement("You squeeze into the rabbit hole and are surprised to find that you", "fit inside.");
			c.getItems().deleteItemInOneSlot(12646, 1);
			c.getItems().deleteItemInOneSlot(12647, 1);
			c.getItems().deleteItemInOneSlot(12648, 1);
			c.nextChat = 0;
			break;
		case 367:
			sendNpcChat3("Hey there, I've discovered a new method of potion decanting.",
					"I can decant all potions in your inventory at a fee of 500k",
					"Would you like me to decant your potions", 367, "Chemist");
			c.nextChat = 368;
			break;
		case 368:
			c.getDH().sendOption2("Yes, please", "No thanks, I can decant them myself");
			c.dialogueAction = 368;
			break;
		case 8091:
			sendNpcChat1("Thank you for helping me out of here", c.talkingNpc, "Star sprite");
			c.nextChat = 8092;
			break;
		case 8092:
			sendNpcChat2("Here are some rewards you can choose from,", "for some stardusts.", c.talkingNpc,
					"Star sprite");
			c.nextChat = 8093;
			break;
		case 8093:
			sendOption5("1.000.000 gp for 100 stadusts.", "Xp lamp for 750 stardusts.", "Ores for 450 stardusts.",
					"Mining suite for 11.000 stardusts.", "Inferno adze for 15.000 stardusts.");
			c.dialogueAction = 8093;
			break;
		case 463:
			sendNpcChat3("Hello " + c.getNameSmartUp() + "!", "I can take you to a secret island,",
					"We will need to go diving to get there.", c.talkingNpc, "Murphy");
			c.nextChat = 464;
			break;
		case 464:
			sendOption2("Lets go diving", "No thank you");
			c.dialogueAction = 464;
			break;
		case 465:
			sendNpcChat3("You dont have any diving gear, would you like to",
					"buy a fishbowl helmet with a rubber hood inside",
					"and a Diving apparatus with some tubes for 6 millions.", c.talkingNpc, "Murphy");
			c.nextChat = 466;
			break;
		case 466:
			sendOption2("Ofcourse", "No thanks");
			c.dialogueAction = 466;
			break;
		case 1512:
			sendNpcChat3("Hello " + c.getNameSmartUp() + "!", "I am the almighty wildy adventurer", "do you need any help?",
					c.talkingNpc, "Adventurer");
			c.nextChat = 1513;
			break;
		case 1513:
			sendOption3("What are the wilderness keys?", "How do i obtain one of these keys?",
					"Where can i find the treasure chests?");
			c.dialogueAction = 1513;
			break;
		case 1514:
			sendNpcChat4("The wilderness keys are 4 keys that you can find in",
					"the wilderness and are used to open treasure chests",
					"inside wilderness. However you need one more key",
					"to enter the room where the treasure chests are.", c.talkingNpc, "Adventurer");
			c.nextChat = 1513;
			break;
		case 1515:
			sendNpcChat2("You can obtain these keys from the dangerous bosses", "around the wilderness", c.talkingNpc,
					"Adventurer");
			c.nextChat = 1513;
			break;
		case 1516:
			sendNpcChat2("You can find the chests West from mage arena,", "near the agility wilderness course.",
					c.talkingNpc, "Adventurer");
			c.nextChat = 1513;
			break;
		case 534:
			sendNpcChat4("Hello " + c.getNameSmartUp() + "!", "I am the almighty merchant of supplies,",
					"I can whip up a batch of supplys in seconds!",
					"..for a price of course.. Would you like to browse?", c.talkingNpc, "Wizard");
			c.nextChat = 535;
			break;
		case 535:
			sendOption3("Browse Magician Supplies", "Browse Miscellaneous", "Nevermind");
			c.dialogueAction = 535;
			break;
		case 536:
			c.getDH().sendOption4("1k Barrage Spells (2,482k)", "1k Vengeance Spells (2,484k)",
					"1k Blood Barrage Spells (3,469k)", "Nevermind");
			c.dialogueAction = 536;
			break;
		case 537:
			sendOption5("1k Sharks (3,500k)", "500 Super Sets (10,954k)", "500 Super Restores (5,693k)",
					"500 Saradomin Brews (8,511k)", "100 Prayer Renewals (7,900k)");
			c.dialogueAction = 537;
			break;
		case 14328:
			sendNpcChat2("Hello there, I'm Sir Tiffy Cashien", "How can i help you?", c.talkingNpc,
					"Sir Tiffy Cashien");
			c.nextChat = 14329;
			break;
		case 14329:
			sendOption5("Reset a skill for 2.5m", "Open Loyalty Titles", "Talk about Security Codes",
					"Select a home area", "Set a skill lvl");
			c.dialogueAction = 14329;
			break;
		case 14330:
			sendOption5("Attack", "Strength", "Defence", "Range", "Next Page");
			c.dialogueAction = 14330;
			break;
		case 14331:
			sendOption4("Magic", "Prayer", "Hitpoints (Disabled)", "Previous Page");
			c.dialogueAction = 14331;
			break;
		case 14332:
			sendNpcChat1("Thank you for taking some time to discuss the security code,", c.talkingNpc,
					"Sir Tiffy Cashien");
			c.nextChat = 14333;
			break;
		case 14333:
			sendPlayerChat1("Can you please tell me how the security code works?");
			c.nextChat = 14334;
			break;
		case 14334:
			sendNpcChat4("Ofcourse, a security code is a 4 number code that you can",
					" set, to set a security code simply just make a bank pin.",
					"The 4 numbers you choose for your bank pin", "will also be your security code.", c.talkingNpc,
					"Sir Tiffy Cashien");
			c.nextChat = 14335;
			break;
		case 14335:
			sendNpcChat2("A security code will protect your account from", "anyone trying access your account.",
					c.talkingNpc, "Sir Tiffy Cashien");
			c.nextChat = 14336;
			break;
		case 14336:
			sendPlayerChat1("How does that number protect my account?");
			c.nextChat = 14337;
			break;
		case 14337:
			sendNpcChat3("It will detect anyone that will try to log into your account",
					"from any other computer than yours and a dialogue will",
					"popup requesting the 4 numbers to be able to access your account.", c.talkingNpc,
					"Sir Tiffy Cashien");
			c.nextChat = 14338;
			break;
		case 14338:
			sendNpcChat2("if you type wrong numbers the account will freeze",
					"and not be able to move or do any other actions.", c.talkingNpc, "Sir Tiffy Cashien");
			c.nextChat = 0;
			break;
		case 9450:
			sendOption2("Take the Ikov tour", "No thanks");
			c.dialogueAction = 9451;
			break;
		case 7935:
			sendNpcChat1("Hello, What are you doing here?", c.talkingNpc, "Hans");
			c.nextChat = 7936;
			break;
		case 7936:
			sendOption4("I'm looking for whoever is in charge of this place.",
					"I have come to kill everyone in this castle!", "I don't know. I'm lost. Where am I?",
					"What do you do here?");
			c.dialogueAction = 7936;
			break;
		case 7937:
			sendPlayerChat1("What do you do here?");
			c.nextChat = 7938;
			break;
		case 7938:
			sendNpcChat1("I'm on patrol. i've been patrolling this castle for years!", c.talkingNpc, "Hans");
			c.nextChat = 7939;
			break;
		case 7939:
			sendPlayerChat1("You must be old then?");
			c.nextChat = 7940;
			break;
		case 7940:
			sendNpcChat2("Haha, you could say I'm quite the veteran of these",
					" lands. Yes, I've been here fair while...", c.talkingNpc, "Hans");
			long diff = Calendar.getInstance().getTimeInMillis() - c.getJoinedDate().getTime();
			if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) >= 100) {
				c.nextChat = 7941;
			} else {
				c.nextChat = 7943;
			}
			break;
		case 7941:
			sendNpcChat3("And it looks like you've been here for a decent amount",
					"of time too! I can sell you a cape to show you are a",
					"veteran too, if you'd like - only 5,000,000 coins!", c.talkingNpc, "Hans");
			c.nextChat = 7942;
			break;
		case 7942:
			sendOption2("I'll take one!", "No, thanks.");
			c.dialogueAction = 7942;
			break;
		case 7943:
			sendNpcChat4("And it looks like you've been here for a decent amount",
					"of time too! But not long enough to get veteran cape.",
					"When your account is 100 days old you can come back", "and get your veteran cape.", c.talkingNpc,
					"Hans");
			c.nextChat = 0;
			break;
		case 9060:
			sendNpcChat2("I can repair your " + ItemConstants.getItemName(c.degradedItem.brokenItem) + ",",
					"however this will cost you " + c.degradedItem.repairCost + " coins.", c.talkingNpc, "Bob");
			c.nextChat = 9061;
			break;
		case 9061:
			sendOption2("Yes please repair my item.", "No please don't repair my item");
			c.dialogueAction = 9061;
			break;
		case 5400:
			sendNpcChat2("Hi.", "I need some help.", c.talkingNpc, "Santa");
			c.nextChat = 5100;
			break;
		case 5100:
			sendNpcChat3("Cody has stolen all my Christmas Presents!",
					"Please make me five of each colored Marionette.",
					"My workshop is down the trapdoor north of draynor bank.", c.talkingNpc, "Santa");
			c.nextChat = 0;
			c.getItems().addItem(946, 1);
			// c.xMas = 1;
			// c.pete = 1;
			break;
		case 5200:
			sendNpcChat3("I am busy.", "If you need help talk to Rosie at my workshop.",
					"Go down the trapdoor north of draynor bank and bring a knife.", c.talkingNpc, "Santa");
			c.nextChat = 0;
			break;
		case 5500:
			sendNpcChat2("Why thank you! You're too late though...", "Uffinz already gave me some!", c.talkingNpc,
					"Santa");
			c.nextChat = 5600;
			break;
		case 5600:
			sendNpcChat2("I suppose I can't let your work go to waste...", "Here! Have a present!", c.talkingNpc,
					"Santa");
			c.nextChat = 5700;
			c.getItems().deleteItem2(6865, 4);
			c.getItems().deleteItem2(6867, 4);
			c.getItems().deleteItem2(6866, 4);
			// c.getItems().deleteItemInOneSlot(6865, 4);
			// c.getItems().deleteItemInOneSlot(6867, 4);
			// c.getItems().deleteItemInOneSlot(6866, 4);
			c.getItems().addItem(14664, 1);
			break;
		case 5700:
			sendNpcChat2("HoHoHo!. Have a Merry Christmas!", "Use your present on a christmas tree!", c.talkingNpc,
					"Santa");
			c.nextChat = 5800;
			// c.xMas = 3;
			// c.pete = 3;
			break;
		case 5800:
			sendNpcChat3("Having a fun time on " + Config.SERVER_NAME + "?",
					"Remember, Christmas only comes once a year!", "HoHoHo!", c.talkingNpc, "Santa");
			c.nextChat = 0;
			break;
		case 6000:
			sendNpcChat2("Talk to Santa.", "He needs help.", c.talkingNpc, "Rosie");
			c.nextChat = 0;
			break;
		case 6100:
			sendNpcChat2("Make the Marionettes.", "You may need this item, a knife, and three logs.", c.talkingNpc,
					"Rosie");
			c.nextChat = 6200;
			c.getItems().addItem(8794, 1);
			break;
		case 6200:
			sendNpcChat2("Talk to me again once", "you've made the Marionettes.", c.talkingNpc, "Rosie");
			c.nextChat = 0;
			// c.pete = 2;
			break;
		case 6300:
			sendNpcChat2("Now you need to dip the Marionettes into water.",
					"They all need to be dipped into certain ones.", c.talkingNpc, "Rosie");
			c.nextChat = 6400;
			break;
		case 6400:
			sendNpcChat2("blue needs to be dipped somewhere in Varrock.",
					"Red and Green in a town near a swamp. Good Luck!", c.talkingNpc, "Rosie");
			c.nextChat = 6500;
			// c.xMas = 2;
			break;
		case 6500:
			sendNpcChat2("I'm working off my crimes by helping Santa!", "It's actually pretty fun!", c.talkingNpc,
					"Rosie");
			c.nextChat = 0;
			break;
		case 882: // GYPSY ARIS
			sendPlayerChat1("Hello.");
			c.nextChat = 883;
			break;
		case 883:
			sendNpcChat1("Because I can tell the future.", c.talkingNpc, "Gypsy Aris");
			c.nextChat = 884;
			break;
		case 884:
			sendPlayerChat1("Um?");
			c.nextChat = 885;
			break;
		case 885:
			sendNpcChat1("That is the answer to your next question.", c.talkingNpc, "Gypsy Aris");
			c.nextChat = 886;
			break;
		case 886:
			sendPlayerChat1("But how did you know what I would ask?");
			c.nextChat = 887;
			break;
		case 887:
			sendNpcChat1("Because I can tell the future.", c.talkingNpc, "Gypsy Aris");
			c.nextChat = 888;
			break;
		case 888:
			sendPlayerChat1("Ah, that's very clever.");
			c.nextChat = 889;
			break;
		case 889:
			sendNpcChat2("Thanks. Oh and be careful in the Wilderness.", "Tonight is not your night.", c.talkingNpc,
					"Gypsy Aris");
			c.nextChat = 890;
			break;
		case 890:
			sendPlayerChat1("Cheers!");
			c.nextChat = 0;
			break;
		case 8929:
			sendNpcChat3("Hello great warrior, do you have any", "enchanted stones that you would like to",
					"use to enchant any of your rings?", c.talkingNpc, "Lord Marshal Brogan");
			c.nextChat = 8930;
			break;
		case 8930:
			sendOption2("Yes", "No, not yet");
			c.dialogueAction = 8929;
			break;
		case 8931:
			sendNpcChat2("Just choose what ring you would", "like to enchant", c.talkingNpc, "Lord Marshal Brogan");
			c.nextChat = 8932;
			break;
		case 8932:
			sendOption5("Enchant Onyx ring (Price: 86 Stones)", "Enchant Warrior ring (Price: 58 Stones)",
					"Enchant Seers ring (Price: 66 Stones)", "Enchant Archer ring (Price: 106 Stones)",
					"Enchant Berserker ring (Price: 112 Stones)");
			c.dialogueAction = 8930;
			break;
		case 820:
			sendOption2("what is spinning wheel?", "Buy spin tickets");
			c.dialogueAction = 820;
			break;
		case 821:
			sendNpcChat4("Hello, the spinning wheel is something", " everyone should try, You can buy tickets from me",
					"to try out the spinning wheel, the wheel", "has tons of rewards from common to exlusive items",
					c.talkingNpc, "Spin ticket salesman");
			c.nextChat = 820;
			break;
		case 823:
			sendNpcChat2("Would you like to buy some spin tickets", "for only 10 donation points each ticket?",
					c.talkingNpc, "Spin ticket salesman");
			c.nextChat = 824;
			break;
		case 824:
			c.getDH().sendOption3("Yes, i would like 1 ticket please", "yes, i would like 10 tickets please",
					"No thanks");
			c.dialogueAction = 824;
			break;
		case 2888:
			c.getDH().sendStatement("Congratulations, " + c.getNameSmartUp()
					+ ". You've completed the barrows challenge & your reward has been delivered.");
			c.nextChat = 0;
			break;
		case 2999:
			sendStatement("Are you ready to visit the chest room?");
			c.nextChat = 3000;
			c.dialogueAction = 2999;
			break;
		case 3000:
			sendOption2("Yes, I've killed all the other brothers!", "No, I still need to kill more brothers");
			c.nextChat = 0;
			break;

		case 3381:
			sendOption2("I'll take one!", "No Thanks");
			c.dialogueAction = 3382;
			break;

		case 14372:
			sendNpcChat3("Good day, I am scrambles, I sell PK'ing",
					"equipments. Alongside with that each set has other", "supplies like food and pots included",
					c.talkingNpc, "Scrambles");
			c.nextChat = 14373;
			break;
		case 14373:
			sendOption5("Hybrid Set", "Pure Set", "Melee Set", "Range Set", "Magic Set");
			c.dialogueAction = 14374;
			break;
		case 2247:
			sendNpcChat2("Setting any skill currently costs 200 Loyalty points",
					"Setting prayer skill costs 5 Donator Points", c.talkingNpc, "Wise Old Man");
			c.nextChat = 2249;
			break;
		case 2249:
			c.getDH().sendOption5("Attack", "Strength", "Defence", "Range", "Next Page");
			c.dialogueAction = 2250;
			break;
		case 2251:
			c.getDH().sendOption4("Magic", "Prayer", "Hitpoints (Disabled)", "Previous Page");
			c.dialogueAction = 2252;
			break;
		case 2253:
			c.getDH().sendOption4("I want to change my home location.", "I would like to lock/unlock my xp.",
					"I would like to set a skill.", "Can you take me to starter training.");
			c.dialogueAction = 2254;
			break;
		case 1210:
			c.getDH().sendOption2("Would you like to get a title?", "Open LoyaltyPoint shop");
			c.dialogueAction = 1211;
			break;
		case 662:
			c.getDH().sendOption4("Buy Weapons And Shields", "Buy Armour", "Buy Rares", "Misc");
			c.dialogueAction = 662;
			break;
		case 6673:
			sendStatement("<col=ff0000>The room beyond this point is a prison!",
					"<col=ff0000>There is no way out other than death or teleport.",
					"<col=ff0000>Only those who can endure dangerous encounters should proceed.");
			c.nextChat = 6674;
			break;
		case 6674:
			sendOption2("Climb down.", "Stay here.");
			c.dialogueAction = 6675;
			break;
		case 6675:
			sendStatement("<col=ff0000>The room on the other side is a prison!",
					"<col=ff0000>There is no way out other than death or teleport.",
					"<col=ff0000>Only those who can endure dangerous encounters should proceed.");
			c.nextChat = 6676;
			break;
		case 6676:
			sendOption2("Climb over.", "Stay here.");
			c.dialogueAction = 6676;
			break;
		case 6201:
			sendOption2("How do i get through that door.", "I have the key, can you help me unlock the door.");
			c.dialogueAction = 6202;
			break;
		case 6203:
			c.getDH().sendNpcChat1("You need a frozen key to unlock that door.", c.npcType, "Knight");
			c.nextChat = 6204;
			break;
		case 6204:
			sendPlayerChat1("Where can i find a frozen key?");
			c.nextChat = 6205;
			break;
		case 6205:
			c.getDH().sendNpcChat4("You need four key parts to create a frozen key, wich is",
					"armadyl part, bandos part, zamorak part and saradomin part",
					"You need to put those parts together in order to create a", "frozen key.", c.npcType, "Knight");
			c.nextChat = 6201;
			break;
		case 6207:
			c.getDH().sendNpcChat3("Yes, but remember you can only use", "the frozen key 1 time to unlock the door.",
					"are you sure you want me to help you unlock the door?", c.npcType, "Knight");
			c.nextChat = 6208;
			break;
		case 6208:
			sendOption2("Yes i am sure.", "No thanks");
			c.dialogueAction = 6209;
			break;
		case 6210:
			sendPlayerChat1("You don't have any frozen key.");
			c.nextChat = 6201;
			break;
		case 5497:
			sendPlayerChat1("What is this dungeon?");
			c.nextChat = 5498;
			break;
		case 5498:
			c.getDH().sendNpcChat3("This is a very dangerous dungeon",
					"with alott of dangerous creatures. Kill them and obtain their ",
					"statuettes and bring them to Gaius for rewards.", c.npcType, "miner");
			c.nextChat = 0;
			break;
		case 1111:
			c.getDH().sendNpcChat1("Hello, can i have some money please.", c.talkingNpc, "Tramp");
			c.nextChat = 1112;
			break;
		case 1112:
			sendOption2("Yes, i think i have some spare.", "No go away.");
			c.dialogueAction = 1113;
			break;
		case 1114:
			c.getDH().sendNpcChat1("You........ Greedy prick!", c.talkingNpc, "Tramp");
			c.nextChat = 0;
			break;
		case 1115:
			c.getDH().sendNpcChat1("Thank you sir.", c.talkingNpc, "Tramp");
			c.nextChat = 0;
			break;
		case 957:
			sendPlayerChat1("What is this place?");
			c.nextChat = 958;
			break;
		case 958:
			c.getDH().sendNpcChat3("This place is called duel arena.",
					"People come here to duel and stake items and coins,",
					"be carefull with staking your items or coins.", c.npcType, "Mubariz");
			c.nextChat = 0;
			break;
		case 959:
			c.getDH().sendNpcChat1("I'm a nurse, and i can restore your life points.", c.talkingNpc, "Hamid");
			c.nextChat = 0;
			break;
		case 1008:
			c.getDH().sendNpcChat1("I'm just here to watch the nurses.", c.talkingNpc, "Hamid");
			c.nextChat = 0;
			break;
		case 963:
			c.getDH().sendNpcChat1("I could have'ad him.", c.talkingNpc, "Zahwa");
			c.nextChat = 964;
			break;
		case 964:
			sendOption2("Alright?", "Good for you!");
			c.dialogueAction = 965;
			break;
		case 966:
			c.getDH().sendNpcChat1("Its just a flesh wound!", c.talkingNpc, "Zahwa");
			c.nextChat = 0;
			break;
		case 652:
		case 651:
		case 649:
			c.getDH().sendNpcChat4("We have finally found it!", "I can't tell you what or where. but we found it!",
					"I hope none of the others leak out the information...", "All of the treasure is ours!",
					c.talkingNpc, "Wizard");
			break;
		case 4250:
			c.getDH().sendNpcChat2("Would you like me to saw your logs", "into planks?", c.talkingNpc,
					"Sawmill operator");
			c.nextChat = 4251;
			break;
		case 4251:
			sendOption2("Yes", "Not thanks");
			c.dialogueAction = 4252;
			break;
		case 1856:
			sendOption4("How can i make money?", "How do i start training?", "How can i become staff?",
					"How can i vote for rewards?");
			c.dialogueAction = 1857;
			break;
		case 1858:
			c.getDH().sendNpcChat3("You can make money by skilling,", "playing minigames, player killing",
					"and of course also PVM'ing.", c.talkingNpc, "Starter Guide");
			c.nextChat = 0;
			break;
		case 1859:
			c.getDH().sendNpcChat3("You can start training by using the", "training teleports inside of your magic",
					"spellbook.", c.talkingNpc, "Starter Guide");
			c.nextChat = 0;
			break;
		case 1860:
			c.getDH().sendNpcChat3("You can become a moderator by helping other",
					"players, being active on the forums, abide the rules.", "And by inviting other members (::apply)",
					c.talkingNpc, "Starter Guide");
			c.nextChat = 0;
			break;
		case 1861:
			c.getDH().sendNpcChat3("You can vote for rewards by using ::vote,", "and complete the voting steps.",
					"To claim your rewards ingame use the command ::check.", c.talkingNpc, "Starter Guide");
			c.nextChat = 0;
			break;
		case 69:
			c.getDH().sendNpcChat1("Hello! Do you want to choose your clothes?", c.talkingNpc, "Thessalia");
			c.sendMessage("<col=ff0000>You must right-click Thessalia to change your clothes.");
			c.nextChat = 0;
			break;
		/* AL KHARID */
		case 1022:
			c.getDH().sendPlayerChat1("Can I come through this gate?");
			c.nextChat = 1023;
			break;

		case 1023:
			c.getDH().sendNpcChat1("You must pay a toll of 10 gold coins to pass.", c.talkingNpc, "Border Guard");
			c.nextChat = 1024;
			break;
		case 1024:
			c.getDH().sendOption3("Okay, I'll pay.", "Who does my money go to?", "No thanks, I'll walk around.");
			c.dialogueAction = 502;
			break;
		case 1025:
			c.getDH().sendPlayerChat1("Okay, I'll pay.");
			c.nextChat = 1026;
			break;
		case 1026:
			c.getDH().sendPlayerChat1("Who does my money go to?");
			c.nextChat = 1027;
			break;
		case 1027:
			c.getDH().sendNpcChat2("The money goes to the city of Al-Kharid.", "Will you pay the toll?", c.talkingNpc,
					"Border Guard");
			c.nextChat = 1028;
			break;
		case 1028:
			c.getDH().sendOption2("Okay, I'll pay.", "No thanks, I'll walk around.");
			c.dialogueAction = 508;
			break;
		case 1029:
			c.getDH().sendPlayerChat1("No thanks, I'll walk around.");
			c.nextChat = 0;
			break;

		case 1030:
			if (!c.getItems().playerHasItem(995, 10)) {
				c.getDH().sendPlayerChat1("I haven't got that much.");
				c.nextChat = 0;
			} else {
				c.getDH().sendNpcChat1("As you wish. Don't get too close to the scorpions.", c.talkingNpc,
						"Border Guard");
				c.getItems().deleteItem2(995, 10);
				c.sendMessage("You pass the gate.");
				Special.movePlayer(c);
				Special.openKharid(c, c.objectId);
				c.faceLocation(c.objectX, c.objectY);
				c.nextChat = 0;
			}
			break;

		case 1031:
			c.getDH().sendNpcChat1("As you wish. Don't get too close to the scopions.", c.talkingNpc, "Border Guard");
			c.getItems().deleteItem2(995, 10);
			c.sendMessage("You pass the gate.");
			Special.movePlayer(c);
			Special.openKharid(c, c.objectId);
			c.faceLocation(c.objectX, c.objectY);
			c.nextChat = 0;
			break;

		case 22:
			sendOption2("Pick the flowers", "Leave the flowers");
			c.nextChat = 0;
			c.dialogueAction = 22;
			break;
		/** Bank Settings **/
		case 1013:
			c.getDH().sendNpcChat1("Good day. How may I help you?", c.talkingNpc, "Banker");
			c.nextChat = 1014;
			break;
		case 1014:// bank open done, this place done, settings done, to do
					// delete pin
			c.getDH().sendOption3("I'd like to access my bank account, please.",
					"I'd like to check my my P I N settings.", "What is this place?");
			c.dialogueAction = 251;
			break;
		/** What is this place? **/
		case 1015:
			c.getDH().sendPlayerChat1("What is this place?");
			c.nextChat = 1016;
			break;
		case 1016:
			c.getDH().sendNpcChat2("This is the bank of " + Config.SERVER_NAME + ".",
					"We have many branches in many towns.", c.talkingNpc, "Banker");
			c.nextChat = 0;
			break;
		/**
		 * Note on P I N. In order to check your "Pin Settings. You must have
		 * enter your Bank Pin first
		 **/
		/** I don't know option for Bank Pin **/
		case 1017:
			c.getDH().sendStartInfo("Since you don't know your P I N, it will be deleted in <col=ff0000>3 days</col>. If you",
					"wish to cancel this change, you may do so by entering your P I N",
					"correctly next time you attempt to use your bank.", "", "", false);
			c.nextChat = 0;
			break;
		case 0:
			c.talkingNpc = -1;
			c.getPA().removeAllWindows();
			c.nextChat = 0;
			break;

		case 144:
			sendNpcChat1("Nice day, isn't it?", c.talkingNpc, "");
			c.nextChat = 0;
			break;

		case 2994:
			sendOption3("Lottery", "Mithril Seeds", "No Thanks.");
			c.dialogueAction = 2994;
			break;
		case 2993:
			sendOption3("10 seeds for 5<col=ff00>M</col> Coins", "100 seeds for 50<col=ff00>M</col> Coins",
					"1000 seeds for 500<col=ff00>M</col> Coins");
			c.dialogueAction = 2994;
			break;
		case 2998:
			sendNpcChat4("Hi there, i manage the Lottery, the lottery pot ends",
					"at 500,000,000 (500m), one player can enter the", "Lottery up to 5 times each pot.",
					"Participation costs 10,000,000 (10m), would you like to try?", c.talkingNpc, "Gamble");
			c.nextChat = 2997;
			break;

		case 13720:
			sendOption2("Sacrifice firecape to unlock the fight kiln", "Sacrifice Kiln cape for a chance of jad pet");
			c.dialogueAction = 13720;
			break;
		case 13721:
			sendNpcChat2("Would you like to sacrifice your firecape", "in order to unlock the fight kiln.",
					c.talkingNpc, "thzaar_mej_kah");
			c.nextChat = 13722;
			break;
		case 13722:
			sendOption2("Yes I would like to sacrifice my firecape.", "No I would like to keep my firecape.");
			c.dialogueAction = 13721;
			c.nextChat = 0;
			break;
		case 13723:
			sendNpcChat2("You enter the kiln fight cave 10 waves", "Goodluck!.", c.talkingNpc, "thzaar_mej_kah");
			c.nextChat = 0;
			break;
		case 13724:
			sendNpcChat2("Would you like to sacrifice your Kiln cape", "in order to have a chance to get jad pet.",
					c.talkingNpc, "thzaar_mej_kah");
			c.nextChat = 13725;
			break;
		case 13725:
			sendOption2("Yes I would like to sacrifice my Kiln cape.", "No I would like to keep my Kiln cape.");
			c.dialogueAction = 13725;
			c.nextChat = 0;
			break;
		case 665:
			sendNpcChat1("Hello how may i help you?", c.talkingNpc, "Boot");
			c.nextChat = 666;
			break;
		case 666:
			sendPlayerChat1("My cannon broke, could you repair it for me?");
			c.nextChat = 667;
			break;
		case 667:
			sendNpcChat2("Yes i can repair your cannon for 50.000 Coins",
					"if you bring me all the broken cannon parts.", c.talkingNpc, "Boot");
			c.nextChat = 668;
			break;
		case 668:
			sendOption2("I have all the broken cannon parts.", "I Dont have the broken cannon parts.");
			c.dialogueAction = 669;
			c.nextChat = 0;
			break;
		case 1840:
			sendNpcChat1("Hello how may i help you?", c.talkingNpc, "Dwarven Engineer");
			c.nextChat = 1841;
			break;
		case 1841:
			sendPlayerChat1("I was just wondering, what does this machine do?");
			c.nextChat = 1842;
			break;
		case 1842:
			sendNpcChat4("This machine is an slot machine.", "You can put all kind of junk items into",
					"the machine and have a chance on gaining " + JunkMachine.getRewardAmount() + " GP",
					"for the junk items. GL!", c.talkingNpc, "Dwarven Engineer");
			c.nextChat = 0;
			break;
		case 6537:
			sendOption3("Tell me my PVP statistics", "Open your PVP store", "I want to sell my artifacts");
			c.dialogueAction = 8726;
			c.nextChat = 0;
			break;
		}
	}

	public void sendNpcChat1(String s, int ChatNpc, String name) {
		c.getPacketSender().showAnimationOnInterfacePacket(4883, 9847); // used to be 591
		c.getPacketSender().sendFrame126(name, 4884);
		c.getPacketSender().sendFrame126(s, 4885);
		c.getPacketSender().drawNpcOnInterface(ChatNpc, 4883);
		c.getPacketSender().sendChatInterface(4882);
	}

	public void sendNpcChat2(String s, String s1, int ChatNpc, String name) {
		c.getPacketSender().showAnimationOnInterfacePacket(4888, 9847);
		c.getPacketSender().sendFrame126(name, 4889);
		c.getPacketSender().sendFrame126(s, 4890);
		c.getPacketSender().sendFrame126(s1, 4891);
		c.getPacketSender().drawNpcOnInterface(ChatNpc, 4888);
		c.getPacketSender().sendChatInterface(4887);
	}

	public void sendNpcChat3(String s, String s1, String s2, int ChatNpc, String name) {
		c.getPacketSender().showAnimationOnInterfacePacket(4894, 9847/* 591 */); // 591 is default
																// emote //
		// updated emotes is
		// 9847
		c.getPacketSender().sendFrame126(name, 4895);
		c.getPacketSender().sendFrame126(s, 4896);
		c.getPacketSender().sendFrame126(s1, 4897);
		c.getPacketSender().sendFrame126(s2, 4898);
		c.getPacketSender().drawNpcOnInterface(ChatNpc, 4894);
		c.getPacketSender().sendChatInterface(4893);
	}

	public void sendNpcChat4(String s, String s1, String s2, String s3, int ChatNpc, String name) {
		c.getPacketSender().showAnimationOnInterfacePacket(4901, 9847);
		c.getPacketSender().sendFrame126(name, 4902);
		c.getPacketSender().sendFrame126(s, 4903);
		c.getPacketSender().sendFrame126(s1, 4904);
		c.getPacketSender().sendFrame126(s2, 4905);
		c.getPacketSender().sendFrame126(s3, 4906);
		c.getPacketSender().drawNpcOnInterface(ChatNpc, 4901);
		c.getPacketSender().sendChatInterface(4900);
	}

	/**
	 * Options
	 */
	public void sendOption(String s) {
		c.getPacketSender().sendFrame126("Select an Option", 2470);
		c.getPacketSender().sendFrame126(s, 2471);
		c.getPacketSender().sendFrame126("Click here to continue", 2473);
		c.getPacketSender().sendChatInterface(13758);
	}

	public void sendOption2(String s, String s1) {
		c.getPacketSender().sendFrame126("Select an Option", 2460);
		c.getPacketSender().sendFrame126(s, 2461);
		c.getPacketSender().sendFrame126(s1, 2462);
		c.getPacketSender().sendChatInterface(2459);
	}

	/*
	 * Statements
	 */

	// public void sendStatement(String s) { // 1 line click here to continue
	// chat
	// box interface
	// c.getPacketSender().sendFrame126(s, 357);
	// c.getPacketSender().sendFrame126("Click here to continue", 358);
	// c.getPA().sendChatInterface(356);
	// }

	// public void sendStatement2(String s, String s1) { // 2 line click here to
	// continue chat
	// box interface
	// c.getPacketSender().sendFrame126(s, 359);
	// c.getPacketSender().sendFrame126(s1, 360);
	// c.getPacketSender().sendFrame126("Click here to continue", 361);
	// c.getPA().sendChatInterface(358);
	// }

	// public void sendStatement3(String s, String s1, String s2) { // 3 line
	// click here to continue chat
	// box interface
	// c.getPacketSender().sendFrame126(s, 364);
	// c.getPacketSender().sendFrame126(s1, 365);
	// c.getPacketSender().sendFrame126(s2, 366);
	// c.getPacketSender().sendFrame126("Click here to continue", 367);
	// c.getPA().sendChatInterface(358);
	// }

	public void sendOption3(String s, String s1, String s2) {
		c.getPacketSender().sendFrame126("Select an Option", 2470);
		c.getPacketSender().sendFrame126(s, 2471);
		c.getPacketSender().sendFrame126(s1, 2472);
		c.getPacketSender().sendFrame126(s2, 2473);
		c.getPacketSender().sendChatInterface(2469);
	}

	public void sendOption4(String s, String s1, String s2, String s3) {
		c.getPacketSender().sendFrame126("Select an Option", 2481);
		c.getPacketSender().sendFrame126(s, 2482);
		c.getPacketSender().sendFrame126(s1, 2483);
		c.getPacketSender().sendFrame126(s2, 2484);
		c.getPacketSender().sendFrame126(s3, 2485);
		c.getPacketSender().sendChatInterface(2480);
	}

	public void sendOption5(String s, String s1, String s2, String s3, String s4) {
		c.getPacketSender().sendFrame126("Select an Option", 2493);
		c.getPacketSender().sendFrame126(s, 2494);
		c.getPacketSender().sendFrame126(s1, 2495);
		c.getPacketSender().sendFrame126(s2, 2496);
		c.getPacketSender().sendFrame126(s3, 2497);
		c.getPacketSender().sendFrame126(s4, 2498);
		c.getPacketSender().sendChatInterface(2492);
	}

	public void sendPlayerChat1(String s) {
		c.getPacketSender().showAnimationOnInterfacePacket(969, 591);
		c.getPacketSender().sendFrame126(c.getNameSmartUp(), 970);
		c.getPacketSender().sendFrame126(s, 971);
		c.getPacketSender().showPlayerHeadModelOnInterfacePacket(969);
		c.getPacketSender().sendChatInterface(968);
	}

	public void sendPlayerChat2(String s, String s1) {
		c.getPacketSender().showAnimationOnInterfacePacket(974, 591);
		c.getPacketSender().sendFrame126(c.getNameSmartUp(), 975);
		c.getPacketSender().sendFrame126(s, 976);
		c.getPacketSender().sendFrame126(s1, 977);
		c.getPacketSender().showPlayerHeadModelOnInterfacePacket(974);
		c.getPacketSender().sendChatInterface(973);
	}

	public void sendPlayerChat3(String s, String s1, String s2) {
		c.getPacketSender().showAnimationOnInterfacePacket(980, 591);
		c.getPacketSender().sendFrame126(c.getNameSmartUp(), 981);
		c.getPacketSender().sendFrame126(s, 982);
		c.getPacketSender().sendFrame126(s1, 983);
		c.getPacketSender().sendFrame126(s2, 984);
		c.getPacketSender().showPlayerHeadModelOnInterfacePacket(980);
		c.getPacketSender().sendChatInterface(979);
	}

	/*
	 * Player Chating Back
	 */

	public void sendPlayerChat4(String s, String s1, String s2, String s3) {
		c.getPacketSender().showAnimationOnInterfacePacket(987, 591);
		c.getPacketSender().sendFrame126(c.getNameSmartUp(), 988);
		c.getPacketSender().sendFrame126(s, 989);
		c.getPacketSender().sendFrame126(s1, 990);
		c.getPacketSender().sendFrame126(s2, 991);
		c.getPacketSender().sendFrame126(s3, 992);
		c.getPacketSender().showPlayerHeadModelOnInterfacePacket(987);
		c.getPacketSender().sendChatInterface(986);
	}

	public void sendStartInfo(String text, String text1, String text2, String text3, String title) {
		c.getPacketSender().sendFrame126(title, 6180);
		c.getPacketSender().sendFrame126(text, 6181);
		c.getPacketSender().sendFrame126(text1, 6182);
		c.getPacketSender().sendFrame126(text2, 6183);
		c.getPacketSender().sendFrame126(text3, 6184);
		c.getPacketSender().sendChatInterface(6179);
	}

	public void sendStartInfo(String text, String text1, String text2, String text3, String title, boolean send) {
		c.getPacketSender().sendFrame126(title, 6180);
		c.getPacketSender().sendFrame126(text, 6181);
		c.getPacketSender().sendFrame126(text1, 6182);
		c.getPacketSender().sendFrame126(text2, 6183);
		c.getPacketSender().sendFrame126(text3, 6184);
		c.getPacketSender().sendChatInterface(6179);
	}

	public void sendStatement(String... lines) {
		if (lines == null || lines.length > 4) {
			return;
		}
		int id = statementIds[lines.length - 1];
		for (int i = 0; i <= lines.length; i++) {
			if (i == lines.length) {
				c.getPacketSender().sendFrame126("Click here to continue", id);
				break;
			}
			c.getPacketSender().sendFrame126(lines[i], id++);
		}
		c.getPacketSender().sendChatInterface(statementIds[lines.length - 1] - 1);
	}
	/*
	 * Npc Chatting
	 */
}
