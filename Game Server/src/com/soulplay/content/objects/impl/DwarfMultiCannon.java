package com.soulplay.content.objects.impl;

import com.soulplay.Server;
import com.soulplay.content.player.combat.CombatStatics;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

/**
 * @author Aleksandr
 */

public class DwarfMultiCannon {

	/**
	 * @author Aleksandr (CanXOffset, CanYOffset, AnimID, DirectionX,
	 *         DirectionY)
	 */
	private enum DirectionOffset {
		N(1, 2, 516, 1, 0) {

			@Override
			DirectionOffset next() {
				return NE;
			}
		},
		NE(2, 2, 517, 0, -1) {

			@Override
			DirectionOffset next() {
				return E;
			}
		},
		E(2, 1, 518, 0, -1) {

			@Override
			DirectionOffset next() {
				return SE;
			}
		},
		SE(2, 0, 519, -1, 0) {

			@Override
			DirectionOffset next() {
				return S;
			}
		},
		S(1, 0, 520, -1, 0) {

			@Override
			DirectionOffset next() {
				return SW;
			}
		},
		SW(0, 0, 521, 0, 1) {

			@Override
			DirectionOffset next() {
				return W;
			}
		},
		W(0, 1, 514, 0, 1) {

			@Override
			DirectionOffset next() {
				return NW;
			}
		},
		NW(0, 2, 515, 1, 0) {

			@Override
			DirectionOffset next() {
				return N;
			}
		};

		private int xOffset;

		private int yOffset;

		private int animId;

		private int directionX;

		private int directionY;

		// Point advance(Point point) {
		// return new Point(point.x + dx, point.y + dy);
		// }

		DirectionOffset(int xOffset, int yOffset, int animationId,
				int directionX, int directionY) {
			this.xOffset = xOffset;
			this.yOffset = yOffset;
			this.animId = animationId;
			this.directionX = directionX;
			this.directionY = directionY;
		}

		abstract DirectionOffset next();
	};

	private final static int RANGE = 12;// 3 * 5; // Note: must be a 3 in there!

	public static void attackNpc(final Client c, final RSObject o,
			final NPC npc) {
		if (npc.underAttackBy > 0 && npc.underAttackBy != c.getId()
				&& !npc.inMulti()) {
			c.sendMessage("This monster is already in combat.");
			return;
		}
		if ((c.underAttackBy > 0 || c.underAttackBy2 > 0)
				&& c.underAttackBy2 != npc.npcIndex && !c.inMulti()) {
			c.sendMessage("I'm already under attack.");
			return;
		}
		if (!CombatStatics.canAttackSlayerMonster(c, npc, false, false) || !npc.isAttackable()) {
			return;
		}
		if (npc.spawnedBy != c.getId() && npc.spawnedBy > 0) {
			c.sendMessage("This monster was not spawned for you.");
			return;
		}
		/*
		 * if (npc.summon == true) { if(npc.index != c.getId() || c.wildLevel
		 * <= 1) { c.sendMessage("You cannot attack this monster."); return; } }
		 */
		if (c.cannonBalls == 1) {
			c.sendMessage("Your cannon is out of ammo!");
		}
		c.cannonBalls--;
		int damage = Misc.random(30);
		createProjectile(c, o, npc);
		npc.underAttackBy = c.getId();
		npc.lastDamageTaken = System.currentTimeMillis();
		// c.projectileStage = 0;
		npc.setKillerId(c.getId());
		npc.addInfliction(c.mySQLIndex, damage);
		c.getPA().addSkillXP((damage) / 2, 4);
		c.getAchievement().fireCannonBalls();

		npc.dealDamage(new Hit(damage, 0, 4, c));

		// c.getCombat().delayedHit(npc.npcId);

	}

	public static boolean canSetUpCannon(Client c) {
		if (c.inEdgeville()) {
			return false;
		}
		if (c.inMarket() || c.raidsManager != null) {
			return false;
		}
		if (c.inVarrock()) {
			return false;
		}
		if (c.inPits) {
			return false;
		}
		if (c.inMinigame()) {
			return false;
		}
		if (c.inFightCaves()) {
			return false;
		}
		if (c.inDuel) {
			return false;
		}
		if (c.playerIndex > 0) {
			return false;
		}
		if (c.npcIndex > 0) {
			return false;
		}
		if (c.inDuelArena()) {
			return false;
		}
		if (c.duelStatus > 0) {
			return false;
		}
		if (c.inWild()) {
			return false;
		}
		if (c.inPcGame()) {
			return false;
		}

		if (c.getBarbarianAssault().inArena()) {
			return false;
		}

		if (c.inGiantMoleDung()) {
			return false;
		}
		if (c.isInNexRoom()) {
			return false;
		}
		if (c.isInGlacorLair()) {
			return false;
		}
		if (RSConstants.dzDungeon(c.getX(), c.getY())) {
			return false;
		}
		for (int i = -1; i < 2; i++) {
			for (int k = -1; k < 2; k++) {
				if (i == 0 && k == 0) {
					continue;
				}
				if (Math.abs(i) != Math.abs(k)) {
					continue;
				}
				if (!PathFinder.canMoveInStaticRegion(c.getX(), c.getY(), c.heightLevel, i,
						k)) {
					return false;
				}
			}
		}

		for (RSObject o : ObjectManager.objects) {
			if (o.getHeight() != c.heightLevel) {
				continue;
			}
			if (!PlayerConstants.goodDistance(o.getX(), o.getY(), c.getX(),
					c.getY(), 60)) {
				continue;
			}
			if (Misc.goodDistance(o.getX(), o.getY(), c.getX(), c.getY(), 3)) {
				return false;
			}
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					if (c.getX() + i == o.getX() && c.getY() + j == o.getY()
							&& c.heightLevel == o.getHeight()) {
						return false;
					}
				}
			}
		}

		return true;
	}

	public static void createProjectile(Client c, RSObject o, NPC npc) {
		
		Projectile proj = new Projectile(53,
				Location.create(o.getX()+1, o.getY()+1, o.getHeight()),
				Location.create(npc.getX(), npc.getY(), npc.getHeightLevel()));
		proj.setStartHeight(35);
		proj.setEndHeight(35);
		proj.setStartDelay(1);
		proj.setLockon(npc.getProjectileLockon());
		proj.setEndDelay(45);
		GlobalPackets.createProjectile(proj);
		
//		// int oX = o.getX()+currentOffset.xOffset;
//		int oX = o.getX() + 1;
//		// int oY = o.getY()+currentOffset.yOffset;
//		int oY = o.getY() + 1;
//		int offX = ((oX - npc.getX()) * -1);
//		int offY = ((oY - npc.getY()) * -1);
//		int oZ = o.getHeight();
//		if (npc.getSize() > 2) {
//			offX++;
//			offY++;
//		}
//
//		GlobalPackets.createPlayersProjectile(oX, oY, oZ, offY, offX, 50, 45,
//				53, 35, 35, -c.oldNpcIndex + 1, 1);
	}

	/**
	 * Gather Npcs near cannon into array
	 *
	 * @param c
	 *            - Player, owner of cannon
	 * @param cannonX
	 *            - Cannon's X spot, MUST BE CENTER OF CANNON(o.getX()+1)
	 * @param cannonY
	 *            - Cannon's Y spot, MUST BE CENTER OF CANNON(o.getY()+1)
	 */
	public static void gatherNpcs(Client c, int cannonX, int cannonY,
			int cannonZ) {
		if (c.cannonRegion == null) {
			return;
		}
		// for(int i = 0; i < NPCHandler.npcs.length; i++) {
		for (NPC npc : Server.getRegionManager().getLocalNpcsByLocation(cannonX,
				cannonY, cannonZ)) {// c.cannonRegion.getNpcs())
									// {
			// NPC npc = NPCHandler.npcs[i];
			if (npc == null || npc.isDead() || npc.getSkills().getMaximumLifepoints() <= 0) {
				continue;
			}
			if (npc.spawnedBy > 0 || npc.summonedFor > 0) {
				continue;
			}
			if (!c.inMulti() && npc.underAttackBy > 0
					&& npc.underAttackBy != c.getId()) {
				continue;
			}
			if (!c.inMulti() && c.underAttackBy2 > 0
					&& c.underAttackBy2 != npc.npcIndex) {
				// c.sendMessage("I'm already under attack.");
				continue;
			}
			if (!PlayerConstants.goodDistance(cannonX, cannonY, npc.getX(),
					npc.getY(), RANGE)) {
				continue;
			}

			if (!CombatStatics.canAttackSlayerMonster(c, npc, false, false)) {
				continue;
			}

			if (RegionClip.rayTraceBlocked(cannonX, cannonY, npc.absX, npc.absY,
					npc.heightLevel, true, c.getDynamicRegionClip())) {
				continue;
			}
//			if (npc.getSize() > 2) {
//				for (int xSize = 0; xSize < npc.getSize(); xSize++) {
//					for (int ySize = 0; ySize < npc.getSize(); ySize++) {
//						if (!PlayerConstants.goodDistance(cannonX, cannonY,
//								npc.getX() + xSize, npc.getY() + ySize,
//								RANGE)) {
//							// c.getPA().stillGfx(5, npc.getX()+xSize,
//							// npc.getY()+ySize, c.heightLevel, 1);
//							continue;
//						}
//						c.cannonRangeNpcs[(npc.getX() + xSize)
//								% 28][(npc.getY() + ySize) % 28] = npc.npcIndex;
//					}
//				}
//			} else {
				// c.getPA().stillGfx(5, npc.getX(), npc.getY(), c.heightLevel,
				// 1);
				c.cannonRangeNpcs[npc.getX() % 28][npc.getY()
						% 28] = npc.npcIndex;
//			}
		}
	}

	/**
	 * Scan for npcs
	 *
	 * @param x1
	 *            - x1 MUST BE objectX (NOTE: Must be center of cannon,
	 *            o.getX()+1 and o.getY()+1)
	 * @param y1
	 *            - y1 MUST BE objectY (NOTE: Must be center of cannon,
	 *            o.getX()+1 and o.getY()+1)
	 * @param x2
	 *            - npc x to scan for
	 * @param y2
	 *            - npc y to scan for
	 * @param z
	 *            - z is Height
	 * @param c
	 *            - Owner of the cannon
	 * @return NPC, else returns null
	 */
	public static NPC getNpcLOS(int x1, int y1, int x2, int y2, int z,
			Client c) { // LOS BACKUP
						// Puposes

		// int xOffset = x1 - RANGE + 2;
		// int yOffset = y1 - RANGE + 2;

		int dx = Math.abs(x2 - x1);
		int dy = Math.abs(y2 - y1);
		int sx = (x1 < x2) ? 1 : -1;
		int sy = (y1 < y2) ? 1 : -1;
		int err = dx - dy;
		int n = 1 + dx + dy;

		// int x3 = x1;
		// int y3 = y1;
		// int dirX = 0, dirY = 0;

		for (; n > 0; --n) {

			// x3 = x1; // first x
			// y3 = y1; // first y

			if (x1 == x2 && y1 == y2) {
				break;
			}
			int e2 = 2 * err;
			if (e2 > -dy) {
				err = err - dy;
				x1 = x1 + sx;
			}
			if (e2 < dx) {
				err = err + dx;
				y1 = y1 + sy;
			}
			// dirX = x1-x3;
			// dirY = y1-y3;

			// c.getPA().stillGfx(5, x1, y1, c.heightLevel, 1);
			if (c.cannonRangeNpcs[x1 % 28][y1 % 28] != 0) {
				// c.getPA().stillGfx(5, x1, y1, c.heightLevel, 1);
				return NPCHandler.npcs[c.cannonRangeNpcs[x1 % 28][y1 % 28]];
			}
		}
		return null;// NPCHandler.npcs[c.cannonRangeNpcs[x1 % 15][y1 % 15]];
	}

	public static void initializeCannon(final Client c, final RSObject o) {
		c.cannonScanX = DirectionOffset.N.xOffset + o.getX();
		c.cannonScanY = DirectionOffset.N.yOffset + o.getY() + RANGE;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			DirectionOffset currentOffset = DirectionOffset.N;

			int cannonScanX = DirectionOffset.N.xOffset + o.getX();

			int cannonScanY = DirectionOffset.N.yOffset + o.getY() + RANGE;

			@Override
			public void execute(CycleEventContainer container) {

				if (c.disconnected || c == null || o == null || o.getTick() == 0
						|| c.isDead()) {
					if (c != null && c.cannonStatus > 0) {
						if (c != null) {
							c.cannonDecayed = true;

							ItemHandler.createGroundItem(c, 20486, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
							ItemHandler.createGroundItem(c, 20477, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
							ItemHandler.createGroundItem(c, 20478, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
							ItemHandler.createGroundItem(c, 20479, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
							ItemHandler.createGroundItem(c, 20471, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
							ItemHandler.createGroundItem(c, 20468, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
							ItemHandler.createGroundItem(c, 20469, c.getX(),
									c.getY(), c.getHeightLevel(), 1);
						}
						if (o != null) {
							c.cannonStatus = 0;
							o.setTick(0);
							container.stop();
							return;
						}
					}

					if (o != null && c.disconnected) {
						o.setTick(0);
					}

					c.cannonStatus = 0;
					container.stop();
					return;
				}

				if (o.getTick() == 600 || o.getTick() == 500
						|| o.getTick() == 400 || o.getTick() == 300
						|| o.getTick() == 200 || o.getTick() == 100) {
					c.sendMessage("<col=ff0000>Your cannon is decaying.");
				}
				
				if (c.cannonBalls <= 0) {
					return;
				}

				// TODO: REMOVE THIS, not supposed to need this if it works
				// rights
				if (currentOffset == DirectionOffset.N) {
					cannonScanX = DirectionOffset.N.xOffset + o.getX();
					cannonScanY = DirectionOffset.N.yOffset + o.getY() + RANGE;
				}

				if (c.cannonBalls > 0) {
					gatherNpcs(c, o.getX() + 1, o.getY() + 1, o.getHeight());

					for (int k = 0; k < RANGE + 1; k++) {
						cannonScanX += currentOffset.directionX;
						cannonScanY += currentOffset.directionY;
						NPC npc = getNpcLOS(o.getX() + 1, o.getY() + 1,
								cannonScanX, cannonScanY, o.getHeight(), c);
						if (npc == null) {
							continue;
						}

						if (npc != null) {

							if (npc.bossRoomNpc) {
								o.setTick(0);
								c.sendMessage(
										"Your cannon has been destroyed by the boss.");
								npc = null;
								break;
							}

							c.oldNpcIndex = npc.npcIndex;
							// createProjectile(c, o, npc);
							attackNpc(c, o, npc);

							if (k != RANGE) { // 8 shots per revolution
								for (int j = k; j < RANGE; j++) {
									cannonScanX += currentOffset.directionX;
									cannonScanY += currentOffset.directionY;
								}
							}
							npc = null;
							break;
						}
					}

					c.cannonRangeNpcs = new int[28][28];
				} else {
					for (int k5 = 0; k5 < RANGE + 1; k5++) {
						cannonScanX += currentOffset.directionX;
						cannonScanY += currentOffset.directionY;
					}
				}

				GlobalPackets.objectAnim(o.getX(), o.getY(), currentOffset.animId,
						o.getType(), o.getFace(), o.getHeight());

				cannonScanX += currentOffset.directionX;
				cannonScanY += currentOffset.directionY;
				// c.getPA().movePlayer(c.cannonScanX, c.cannonScanY,
				// c.heightLevel);
				// c.getPA().createPlayersStillGfx(5, cannonScanX, cannonScanY,
				// c.heightLevel, 0);

				currentOffset = currentOffset.next();
			}

			@Override
			public void stop() {
				o.setTick(0);
				c.sendMessage("<col=ff0000>Your cannon has decayed. Boot in dwarven mines can fix it for you..");

				if (c != null || !c.disconnected) {
					c.cannonRangeNpcs = new int[28][28];
				}
			}

		}, 1);

	}

	public static void loadCannon(Client c) {
		int loadAmount = c.getItems().getItemAmount(2) > 30
				? 30
				: c.getItems().getItemAmount(2);
		int freeSpace = 30 - c.cannonBalls;
		if (c.cannonBalls > 0) {
			if (freeSpace < loadAmount) {
				loadAmount = freeSpace;
			}
		}
		c.getItems().deleteItemInOneSlot(2, loadAmount);
		c.cannonBalls += loadAmount;
		if (c.cannonBalls == 30) {
			c.sendMessage(
					"You load the cannon with " + loadAmount + " cannonballs.");
		}
		if (c.cannonBalls > 30) {
			c.sendMessage(
					"Refunding " + (c.cannonBalls - 30) + " cannonballs.");
			c.getItems().addItem(2, c.cannonBalls - 30);
		}
	}

	public static void setupCannon(final Client c) {
		if (c.performingAction) {
			return;
		}
		if (c.cannonStatus == 0 && c.clickObjectType == 0) {
			if (!canSetUpCannon(c)) {
				c.sendMessage("There isn't enough space to set up here.");
				return;
			}
			// int X = c.getX()-4, Y = c.getY()+4;
			final int cannonX = c.absX - 1;
			final int cannonY = c.absY - 1;
			final int walkToX;
			final int walkToY;

			if (stepAway(c) == null) {
				c.sendMessage("There isn't enough space to set up here.");
				return;
			} else {
				walkToX = stepAway(c)[0];
				walkToY = stepAway(c)[1];
				/*
				 * c.getPlayerAction().setAction(true);
				 * c.getPlayerAction().canWalk(false);
				 */
				// c.performingAction = true;
				// c.isRunning = false;
				c.getPA().playerWalk(walkToX, walkToY);
				c.startAction(new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (c.absX != walkToX || c.absY != walkToY) {
							return;
						}
						if (c.absX == walkToX && c.absY == walkToY) {
							c.faceLocation(cannonX, cannonY);
							c.startAnimation(827);
							final RSObject o = new RSObject(7, cannonX, cannonY, c.heightLevel, 0, 10, -1, 2400);
							o.setObjectOwnerLong(c.getLongName());
							ObjectManager.addObject(o);
							c.getItems().deleteItemInOneSlot(6, 1);
							c.sendMessage(
									"You place the cannon base on the ground.");
							c.cannonStatus = 1; // base
							/*
							 * c.getPlayerAction().setAction(false);
							 * c.getPlayerAction().canWalk(true);
							 */

							// c.isRunning = true;
							c.cannonRegion = c.currentRegion;
							container.stop();
							return;
						}
					}

					@Override
					public void stop() {
						// if (c != null)
						// c.performingAction = false;
					}
				}, 1);
				return;
			}

		}

		RSObject o = ObjectManager.getPlacedObject(c.objectX, c.objectY,
				c.heightLevel, 10);
		switch (c.cannonStatus) {
			/*
			 * case 0: new RSObject(7, c.absX, c.absY, c.heightLevel, 0, 10,
			 * 2400, c.getLongName(), true); c.getItems().deleteItem(6, 1);
			 * c.sendMessage("You place the cannon base on the ground."); //
			 * c.cannonSetUp = true; c.cannonStatus++; //base
			 * c.startAnimation(827); break;
			 */
			case 1:
				if (o == null) {
					// c.sendMessage("1 null");
					return;
				}
				if (o.getObjectOwnerLong() == c.getLongName()) {
					c.getItems().deleteItemInOneSlot(8, 1);
					o.changeObject(8);
					c.sendMessage("You add the stand.");
					c.cannonStatus = 2;
					c.startAnimation(827);
					// }
				} else {
					c.sendMessage("That isn't yours");
				}
				break;

			case 2:
				if (o == null) {
					return;
				}
				if (o.getObjectOwnerLong() == c.getLongName()) {
					c.getItems().deleteItemInOneSlot(10, 1);
					o.changeObject(9);
					c.sendMessage("You add the barrels.");
					c.cannonStatus = 3;
					c.startAnimation(827);
				} else {
					c.sendMessage("That isn't yours");
				}
				break;

			case 3:
				if (o == null) {
					return;
				}
				if (o.getObjectOwnerLong() == c.getLongName()) {
					c.getItems().deleteItemInOneSlot(12, 1);
					o.changeObject(6);
					c.sendMessage("You add the furnace.");
					c.cannonStatus = 4;
					c.startAnimation(827);
					DwarfMultiCannon.initializeCannon(c, o);
					c.getAchievement().setupCannon();
					// }
				} else {
					c.sendMessage("That isn't yours");
				}
				break;
		}
	}

	public static int[] stepAway(Client c) {
		DirectionOffset offset = DirectionOffset.NW;
		int[] route = {c.getX() - 1, c.getY() + 2};
		// int[] moveTypeDir = {
		// int Y = c.getY()+4;
		for (int k = 0; k < 4; k++) {
			for (int i = 0; i < 3; i++) {
				// if(!PathFinder.canMove(offset.xOffset+c.getX()-1,
				// offset.yOffset+c.getY()-1,
				// c.heightLevel, moveTypeX, moveTypeY))
				if (PathFinder.foundRoute(c, route[0], route[1])) {
					return route;
				}
				// c.getPA().stillGfx(5, route[0], route[1], c.heightLevel, 1);
				// // very usefull debugging
				// tool :D
				route[0] += offset.directionX;
				route[1] += offset.directionY;

			}
			offset = offset.next();
			offset = offset.next();
			route[0] += offset.directionX;
			route[1] += offset.directionY;
		}
		return null;
	}

}
