package com.soulplay.content.objects.impl;

public enum Flower {

	BLUE_PURPLE_FLOWER(2980, 2460),
	RED_FLOWER(2981, 2462),
	BLUE_FLOWERS(2982, 2464),
	YELLOW_FLOWERS(2983, 2466),
	PURPLE_FLOWERS(2984, 2468),
	ORANGE_FLOWERS(2985, 2470),
	RED_YELLOW_BLUE_FLOWERS(2986, 2472),

	BLUE_PURPLE_FLOWER2(2980, 2460),
	RED_FLOWER2(2981, 2462),
	BLUE_FLOWERS2(2982, 2464),
	YELLOW_FLOWERS2(2983, 2466),
	PURPLE_FLOWERS2(2984, 2468),
	ORANGE_FLOWERS2(2985, 2470),
	RED_YELLOW_BLUE_FLOWERS2(2986, 2472),

	WHITE_FLOWERS(2987, 2474),
	BLACK_FLOWERS(2988, 2476);
	
	public static final Flower[] values = values();

	private int objectId;

	private int itemId;

	private Flower(int objectId, int itemId) {
		this.setObjectId(objectId);
		this.setItemId(itemId);
	}

	public int getItemId() {
		return itemId;
	}

	public int getObjectId() {
		return objectId;
	}

	private void setItemId(int itemId) {
		this.itemId = itemId;
	}

	private void setObjectId(int objectId) {
		this.objectId = objectId;
	}

}
