package com.soulplay.content.raids;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.clans.ClanMember;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.raids.boss.greatolm.GreatOlmInstanceHandler;
import com.soulplay.content.raids.map.RaidsMapGen;
import com.soulplay.content.raids.map.RaidsRoom;
import com.soulplay.content.raids.map.rooms.start.StartRoom;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.instances.DynSingleInstanceManager;
import com.soulplay.game.world.map.DynamicObjectManager;
import com.soulplay.game.world.map.ObjectDefVersion;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.Zone;
import com.soulplay.game.world.map.travel.zone.impl.ChambersOfXericZone;

public class RaidsManager {

	public static boolean raidsEnabled = true;
	
	public static DynSingleInstanceManager zInstanceManager = new DynSingleInstanceManager();
	
	private static final Map<Integer, RaidsManager> instances = new HashMap<>();

	public int startX;
	public int startY;
	private Clan clan;
	private int instanceId;
	public int height;
	public int startTicks;
	public List<Region> regions = new ArrayList<>();
	public DynamicRegionClip clipping = new DynamicRegionClip();
	public DynamicObjectManager objectManager = new DynamicObjectManager(clipping);
	public RaidsMapGen mapGen;
	public List<NPC> npcs = new ArrayList<>();
	public RaidsParty party;
	public GreatOlmInstanceHandler greatOlmManager = new GreatOlmInstanceHandler(this);
	public boolean hardMode = false;
	public int startedPlayerMID;
	public Rectangle rectangle;
	public Zone raidsZone;
	public Location respawnLoc = Location.create(0, 0);
	public final RaidsPoints points = new RaidsPoints();
	public CycleEventContainer cleanupEvent;
	public CycleEventContainer endTimer;
	public Map<Integer, RaidsRewardHolder> rewardsToClaim;
	
	public boolean destroyed = false;

	public RaidsManager(Clan clan, Player startedPlayer) {
		this.startedPlayerMID = startedPlayer.mySQLIndex;
		this.clan = clan;
		this.instanceId = this.height = getInstanceId(startedPlayer.mySQLIndex);
		this.mapGen = new RaidsMapGen();
		this.objectManager.setDefsVersion(ObjectDefVersion.OSRS_DEFS); //set osrs definitions
	}
	
	public static boolean instanceExists(int leaderUniqueID) {
		return instances.get(getInstanceId(leaderUniqueID)) != null;
	}
	
	public static int getInstanceId(int uniqueId) {
		return zInstanceManager.getOrCreateZ(uniqueId);
	}

	public void generate() {
		startX = 500;
		startY = 500;

		mapGen.generate(this);
		
		instances.put(instanceId, this);
		
		rectangle = new Rectangle(Location.create(startX, startY), Location.create(startX + 128, startY + 64));
		
		raidsZone = new ChambersOfXericZone(rectangle);
		
		//cheaphax for olm respawn location
		configureStartingLocation();
	}
	
	private void configureStartingLocation() {
		if (hardMode) {
			StartRoom startRoom = mapGen.start;
			respawnLoc = Location.create(getRoomX(startRoom) + startRoom.getData().getSpawnCoordX(), getRoomY(startRoom) + startRoom.getData().getSpawnCoordY(), height);
		} else {
			respawnLoc = Location.create(startX + 28, startY + 21, height);
		}
	}

	public void startRaid() {
		if (clan == null) {
			destroy(true);
			return;
		}

		party = new RaidsParty(clan.getClanMysqlId());

		for (int i = 0, length = clan.activeMembers.size(); i < length; i++) {
			ClanMember clanMember = clan.activeMembers.get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null) {
				continue;
			}

			if (player.raidsManager != this) {
				continue;
			}
			
			Variables.RAID_INSTANCE_ID.setValue(player, instanceId);

			party.addMember(clanMember);
			
			if (rectangle != null)
				player.getZones().addDynamicZone(new ChambersOfXericZone(rectangle));
		}

		clan.raidsManager = null;
		clan = null;

		for (int i = 0, length = party.getMembers().size(); i < length; i++) {
			ClanMember clanMember = party.getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null) {
				continue;
			}

			refreshSidebar((Client) player);
		}
		
		rewardsToClaim = new HashMap<>(party.getMembers().size());

		greatOlmManager.configureOlmHP();//set HP based on party size
		startTicks = Server.getTotalTicks();
		
		if (party.getMembers().size() == 0) {
			destroy(true);
		}
	}

	public void clear() {
		if (greatOlmManager != null) {
			greatOlmManager.clear();
			greatOlmManager = null;
		}
	}

	public boolean started() {
		return party != null;
	}

	public static void leaveRaidDialog(Player player) {
		player.getDialogueBuilder().sendStatement("Are you sure you want to leave this raid?").sendOption("Yes.", () -> {
			if (player.raidsManager == null) {
				return;
			}

			player.raidsManager.leaveRaid(player, true);
		}, "No.", () -> {

		}).execute();
	}

	public static void checkRaidStart(Player player) {
		{
			RaidsManager raids = player.raidsManager;
			if (raids == null) {
				return;
			}

			if (raids.startedPlayerMID != player.mySQLIndex) {
				player.sendMessage("Only leader may start a raid.");
				return;
			}

			if (raids.started()) {
				return;
			}
		}

		player.getDialogueBuilder().sendOption("No-one may join the party after the raid begins.", "Begin the raid.", () -> {
				if (player.getClan() == null) {
					player.sendMessage("You must be in a clan to start raiding.");
					return;
				}

				RaidsManager raids = player.raidsManager;
				if (raids == null) {
					player.sendMessage("You must be in a raiding session to start raiding.");
					return;
				}

				if (raids.started()) {
					return;
				}

				raids.startRaid();
			}, "Don't begin the raid yet.", () -> {
		}).execute();
	}

	public static void refreshSidebar(Client c) {
		RaidsManager raidsManager = c.raidsManager;
		if (raidsManager == null) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(7);

		List<ClanMember> members = null;
		if (raidsManager.party != null) {
			members = raidsManager.party.getMembers();
		} else if (raidsManager.clan != null) {
			members = raidsManager.clan.activeMembers;
		}

		int length;
		if (members == null) {
			length = 0;
			c.getOutStream().writeByte(length);
		} else {
			length = members.size();
			c.getOutStream().writeByte(length);
			for (int i = 0; i < length; i++) {
				ClanMember clanMember = members.get(i);
				if (clanMember == null) {
					c.getOutStream().writeString("");
					continue;
				}

				Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (member == null) {
					c.getOutStream().writeString("");
					continue;
				}

				if (member.raidsManager == raidsManager) {
					if (member.mySQLIndex == raidsManager.startedPlayerMID) {
						c.getOutStream().writeString("<col=ffffff>" + member.getNameSmartUp());
					} else {
						c.getOutStream().writeString(member.getNameSmartUp());
					}
				} else {
					c.getOutStream().writeString("<col=757170>" + member.getNameSmartUp());
				}

				c.getOutStream().writeByte(member.combatLevel);
				c.getOutStream().writeShort(member.getSkills().getTotalLevel());
			}
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();

		if (raidsManager.startedPlayerMID == c.mySQLIndex && !raidsManager.started()) {
			c.getPacketSender().setVisibility(67209, false);
			c.getPacketSender().setVisibility(67212, true);
		} else {
			c.getPacketSender().setVisibility(67209, true);
			c.getPacketSender().setVisibility(67212, false);
		}

		if (raidsManager.started()) {
			c.getPacketSender().sendString("Your party has reached the bottom...", 67213);
		} else {
			c.getPacketSender().sendString("Waiting for your leader to begin the raid...", 67213);
		}

		c.getPacketSender().sendString("Party Size: <col=ffffff>" + length + "</col>", 67207);
	}

	public void leaveRaid(Player player, boolean movePlayer) {
		
		boolean raidStarted = started();

		greatOlmManager.playerDiedOrLeft(player);

		Variables.RAID_INSTANCE_ID.setValue(player, 0);

		if (!raidStarted && clan != null) {
			int playersInRaid = 0;

			for (int i = 0, length = clan.activeMembers.size(); i < length; i++) {
				ClanMember clanMember = clan.activeMembers.get(i);
				if (clanMember == null) {
					continue;
				}

				Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (member == null) {
					continue;
				}

				if (member.raidsManager == this) {
					playersInRaid++;
				}
			}

			if (playersInRaid <= 1) {
				clan.raidsManager = null;
				player.raidsManager = null;
				destroy(true);
			}
		}

		if (raidStarted) {
			party.removePlayer(player.mySQLIndex);

			for (int i = 0, length = party.getMembers().size(); i < length; i++) {
				ClanMember clanMember = party.getMembers().get(i);
				if (clanMember == null) {
					continue;
				}

				Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (member == null) {
					continue;
				}

				refreshSidebar((Client) member);
			}

			if (party.getMembers().isEmpty()) {
				destroy(true);
			}

			rewardsToClaim.remove(player.mySQLIndex);

		}

		player.setDynamicRegionClip(null);
		player.dynObjectManager = null;
		player.raidsManager = null;
		player.getPacketSender().setSidebarInterface(2, 638);

		if (movePlayer)
			movePlayerOutside(player);

	}

	public void teleportPlayer(Player player) {
		player.raidsManager = this;

		Location startLocation = respawnLoc;

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(
					CycleEventContainer container) {
				player.getPA().movePlayer(startLocation.getX(), startLocation.getY(), height);

				applyInstance(player);

				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 1);
	}
	
	public void applyInstance(Player player) {
		player.raidsManager = this;
		player.getPacketSender().sendConstructMapRegionPacket(height);
		player.updateRegion = true;
		player.setDynamicRegionClip(clipping);
		player.dynObjectManager = objectManager;
		player.getPacketSender().setSidebarInterface(2, 67200);
		refreshSidebar((Client) player);
		
		if (raidsZone != null)
			player.getZones().addDynamicZone(raidsZone);
	}
	
	public void addNpc(NPC npc) {
		npcs.add(npc);
	}

	public int getRoomX(RaidsRoom room) {
		return (startX >> 3 << 3) + room.getRoomAbsX();
	}

	public int getRoomY(RaidsRoom room) {
		return (startY >> 3 << 3) + room.getRoomAbsY();
	}

	public NPC spawnNPC(final int id, int x, int y, boolean move, RaidsRoom room) {
		final int size = EntityDef.forID(id).getSize();
		final Location tile = Location.create(getRoomX(room) + translateX(x, y, room.getRotation(), size, size, 0), getRoomY(room) + translateY(x, y, room.getRotation(), size, size, 0), height);
		return spawnNPC(id, tile, move);
	}

	public NPC spawnNPC(int id, Location tile, boolean move) {
		NPC n = NPCHandler.spawnRaids(id, tile.getX(), tile.getY(), tile.getZ(), move ? 1 : 0, this);
		n.getDistanceRules().setFollowDistance(20);
		n.setDynamicRegionClip(clipping);
		return n;
	}

	public GameObject getRealObject(int id, int x, int y, int z) {
		if (objectManager == null) {
			System.out.println("what? null object manager? why?");
			return null;
		}
		
		// look for spawned object first
		GameObject o = objectManager.getObject(id, x, y, z);
		if (o != null && o.isActive())
			return o;
		
		for (int i = 0; i < 4; i++) {
			o = objectManager.getStaticObjects().get(GameObject.generateKey(x, y, z, i));
			if (o != null && o.getId() == id) {
				return o;
			}
		}

		return null;
	}

	public static int translateX(int x, int y, int mapRotation, int sizeX, int sizeY, int objectRotation) {
		if ((objectRotation & 0x1) == 1) {
			int prevSizeX = sizeX;
			sizeX = sizeY;
			sizeY = prevSizeX;
		}

		if (mapRotation == 0) {
			return x;
		} else if (mapRotation == 1) {
			return y;
		} else if (mapRotation == 2) {
			return 31 - x - (sizeX - 1);
		} else if (mapRotation == 3) {
			return 31 - y - (sizeY - 1);
		}

		return 0;
	}

	public static int translateY(int x, int y, int mapRotation, int sizeX, int sizeY, int objectRotation) {
		if ((objectRotation & 0x1) == 1) {
			int prevSizeX = sizeX;
			sizeX = sizeY;
			sizeY = prevSizeX;
		}

		if (mapRotation == 0) {
			return y;
		} else if (mapRotation == 1) {
			return 31 - x - (sizeX - 1);
		} else if (mapRotation == 2) {
			return 31 - y - (sizeY - 1);
		} else if (mapRotation == 3) {
			return x;
		}

		return 0;
	}
	
	public void handleLogout(Player p) {
		
		boolean started = started();
		
		if (!started) {
			leaveRaid(p, true);
		}
		
		//count how much active users, if none, start cleanup timer
		if (started) {

			//if player is inside olm fight room, moves him to respawn location
			if (greatOlmManager != null)
				greatOlmManager.logout(p);
			
			int activeUsers = countActiveUsers();
			
			
			
			if (activeUsers == 0) {
				startCleanupEvent();
			}
		}
	}
	
	public int countActiveUsers() {
		int activeUsers = 0;
		
		for (int i = 0, length = party.getMembers().size(); i < length; i++) {
			ClanMember clanMember = party.getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (member == null) {
				continue;
			}

			if (!member.disconnected) {
				activeUsers++;
			}
		}
		
		return activeUsers;
	}
	
	public static void handleLoginInsideRaids(Player p) {
		int instanceId = Variables.RAID_INSTANCE_ID.getValue(p);
		if (instanceId == 0) {
			return;
		}
		
		
		RaidsManager raid = instances.get(instanceId);
		
		boolean inParty = raid != null && raid.party != null && raid.party.isMember(p.mySQLIndex);
		
		if (inParty) {
			if (raid != null && p.getZ() == raid.height) {
				raid.applyInstance(p);
				if (raid.countActiveUsers() > 0)
					raid.endCleanup();
				return;
			}
		}
		
		//remove from old instance
		Variables.RAID_INSTANCE_ID.setValue(p, 0);
		
		movePlayerOutside(p);
		
	}
	
	public static void movePlayerOutside(Player p) {
		p.getPA().movePlayer(7634, 3572, 0);
	}
	
	public void endCleanup() {
		if (cleanupEvent == null) {
			return;
		}

		cleanupEvent.stop();
		cleanupEvent = null;
	}

	public void startCleanupEvent() {
		if (cleanupEvent != null) {
			return;
		}
		
		if (PlayerHandler.updateRunning) {
			destroy(true);
			return;
		}

		//System.out.println("start cleanup event");
		cleanupEvent = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				destroy(true);
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 100);
	}
	
	public void destroy(boolean hard) {
		
		if (destroyed)
			return;
		
		destroyed = true;
		
		//removing players on destruct
		if (!started()) {
			
			for (int i = 0, length = clan.activeMembers.size(); i < length; i++) {
				ClanMember clanMember = clan.activeMembers.get(i);
				if (clanMember == null) {
					continue;
				}

				Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (player == null) {
					continue;
				}

				if (player.raidsManager == null || player.raidsManager != this) {
					continue;
				}

				leaveRaid(player, true);
			}
			
		} else { // started so loop party class
		
			//double loop
//			for (int i = 0, length = party.getMembers().size(); i < length; i++) {
//				ClanMember clanMember = party.getMembers().get(i);
//				if (clanMember == null) {
//					continue;
//				}
//
//				Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
//				if (player == null) {
//					continue;
//				}
//
//				if (player.raidsManager != null) {
//					leaveRaid(player, true);
//				}
//			}
			
		}
		
		if (hard) {
			
			if (greatOlmManager.penultimateCycle != null) {
				greatOlmManager.penultimateCycle.stop();
				greatOlmManager.penultimateCycle = null;
			}

			if (objectManager != null) {
				objectManager.cleanup();
			}
			objectManager = null;
			
			if (rewardsToClaim != null)
				rewardsToClaim.clear();
			
			clear();
			
		}
		
		endCleanup();

		if (endTimer != null) {
			endTimer.stop();
			endTimer = null;
		}

		instances.remove(instanceId);
		//System.out.println("destroyed instance");
	}

}
