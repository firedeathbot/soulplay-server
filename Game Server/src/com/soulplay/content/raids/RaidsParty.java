package com.soulplay.content.raids;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.soulplay.content.clans.ClanMember;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;

public class RaidsParty {

	private List<ClanMember> members = new ArrayList<>();
	
	private final int clanMID;
	
	public RaidsParty(int clanId) {
		this.clanMID = clanId;
	}

	public List<ClanMember> getMembers() {
		return members;
	}

	public int getClanMID() {
		return clanMID;
	}

	public void addMember(ClanMember clanMember) {
		members.add(clanMember);
	}

	public void removePlayer(int mySqlId) {
		for (int i = 0, length = members.size(); i < length; i++) {
			ClanMember member = members.get(i);
			if (member.getMysqlID() == mySqlId) {
				members.remove(i);
				return;
			}
		}
	}
	
	public boolean isMember(int mySqlId) {
		for (int i = 0, length = members.size(); i < length; i++) {
			ClanMember member = members.get(i);
			if (member.getMysqlID() == mySqlId) {
				return true;
			}
		}
		return false;
	}

	public void executeForMember(Consumer<Player> consumer) {
		for (int i = 0, length = getMembers().size(); i < length; i++) {
			ClanMember clanMember = getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null || !player.isActive || player.isDead()
					|| player.getSkills().getLifepoints() <= 0 || player.tobDead) {
				continue;
			}

			consumer.accept(player);
		}
	}

	public void executeForMemberNoCmb(Consumer<Player> consumer) {
		for (int i = 0, length = getMembers().size(); i < length; i++) {
			ClanMember clanMember = getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null || !player.isActive) {
				continue;
			}

			consumer.accept(player);
		}
	}

	public boolean isPartyWiped() {
		for (int i = 0, length = getMembers().size(); i < length; i++) {
			ClanMember clanMember = getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null || !player.isActive) {
				continue;
			}

			if (!player.tobDead) {
				return false;
			}
		}

		return true;
	}

	public int partySize() {
		return members.size();
	}

}
