package com.soulplay.content.raids;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.util.Misc;
import com.soulplay.cache.ItemDef;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;

public class RaidsRewards {

	private static int TOTAL_WEIGHT;
	private static List<RareRewardInfo> rewards;

	public enum CommonRewardInfo {
		REWARD1(new Item(560, 3_100)),
		REWARD2(new Item(565, 4_095)),
		REWARD3(new Item(566, 6_554), 20),
		REWARD4(new Item(892, 9_437)),
		REWARD5(new Item(11212, 926)),

		// Herbs
		REWARD6(new Item(3049, 354)),
		REWARD7(new Item(207, 164), 800),
		REWARD8(new Item(209, 668)),
		REWARD9(new Item(211, 354)),
		REWARD10(new Item(213, 323)),
		REWARD11(new Item(3051, 131)),
		REWARD12(new Item(215, 319)),
		REWARD13(new Item(2485, 446)),
		REWARD14(new Item(217, 616)),
		REWARD15(new Item(219, 153)),

		// Ores and gems
		REWARD16(new Item(442, 6_553), 20),
		REWARD17(new Item(453, 6_553), 20),
		REWARD19(new Item(444, 2_892)),
		REWARD18(new Item(447, 2_892), 32),
		REWARD20(new Item(449, 729)),
		REWARD21(new Item(451, 87)),
		REWARD22(new Item(564, 4_095)),
		REWARD23(new Item(1623, 642)),
		REWARD24(new Item(1621, 923)),
		REWARD25(new Item(1619, 524), 250),
		REWARD26(new Item(1617, 253)),

		// Other
		REWARD27(new Item(7936, 65_535), 2),
		REWARD29(new Item(8780, 1_310), 100),
		REWARD30(new Item(8782, 550)),
		REWARD31(new Item(995, 2_000_000 + Misc.random(10_000_000))),
		REWARD32(new Item(995, 2_000_000 + Misc.random(10_000_000))),
		REWARD33(new Item(995, 2_000_000 + Misc.random(10_000_000))),
		REWARD34(new Item(30369, 250), 25);

		public static final CommonRewardInfo[] values = values();
		private final Item item;
		private final int divider;

		CommonRewardInfo(Item item) {
			this(item, 1);
		}

		CommonRewardInfo(Item item, int divider) {
			this.item = item;
			this.divider = divider;
		}

		public Item getItem() {
			return item;
		}

		public int getDivider() {
			return divider;
		}
	}

	public enum RareRewardInfo {
		DEXTEROUS_PRAYER_SCROLL(new Item(30095), 20),
		ARCANE_PRAYER_SCROLL(new Item(30096), 20),
	    DRAGON_SWORD(new Item(30088), 5),
		DRAGON_HARPOON(new Item(30069), 5),
		DRAGON_THROWNAXE(new Item(30093, 100), 5),
		TWISTED_BUCKLER(new Item(30091), 4),
		DRAGON_HUNTER_CROSSBOW(new Item(30087), 4),
		DINHS_BULWARK(new Item(30094), 3),
		ANCESTRAL_HAT(new Item(30086), 3),
		ANCESTRAL_ROBE_TOP(new Item(30085), 3),
		ANCESTRAL_ROBE_BOTTOM(new Item(30084), 3),
		DRAGON_CLAWS(new Item(14484), 3),
		ELDER_MAUL(new Item(30090), 2),
		KODAI_INSIGNIA(new Item(30089), 2),
		TWISTED_BOW(new Item(30092), 2),
		OLM_PET(new Item(30376), 1),
		SCYTHE_OF_VITUR(new Item(30327), 1);

		private final Item item;
		private final int weigth;

		RareRewardInfo(Item item, int weight) {
			this.item = item;
			this.weigth = weight;
		}

		public Item getItem() {
			return item;
		}

		public int getWeigth() {
			return weigth;
		}
	}

	public static void load() {
		RareRewardInfo[] values = RareRewardInfo.values();
		rewards = new ArrayList<>(values.length);
		for (RareRewardInfo reward : values) {
			TOTAL_WEIGHT += reward.getWeigth();
			rewards.add(reward);
		}
	}

	public static boolean isJackpot(int id) {
		for (RareRewardInfo reward : rewards) {
			if (reward.getItem().getId() == id) {
				return true;
			}
		}

		return false;
	}

	public static void refreshReward(Player player) {
		if (player.raidsManager == null) {
			return;
		}

		RaidsRewardHolder rewards = player.raidsManager.rewardsToClaim.get(player.mySQLIndex);
		if (rewards == null) {
			return;
		}
		player.getPacketSender().itemsOnInterface(67234, rewards.raidsRewards, rewards.raidsRewardsN);
	}

	public static void openReward(Player player) {
		refreshReward(player);
		player.getPacketSender().showInterface(67230);
	}

	public static void genRewards(Player player, int points) {
		Item[] rewards = calcReward(player, points);

		RaidsRewardHolder playerRewards = new RaidsRewardHolder();

		if (player.raidsManager != null) {
			player.raidsManager.rewardsToClaim.put(player.mySQLIndex, playerRewards);
		}

		if (rewards != null) {
			for (int i = 0; i < rewards.length; i++) {
				Item reward = rewards[i];
				
				playerRewards.raidsRewards[i] = reward.getId() + 1;
				playerRewards.raidsRewardsN[i] = reward.getAmount();
			}

		}
	}

	private static Item[] calcReward(Player player, int points) {
		List<RareRewardInfo> yourRewards = new ArrayList<>(rewards);
		Collections.shuffle(yourRewards);

		double uniqueChance = points / 7125f / 100f;
		
		if (player.dropRateBoostActive()) {
			uniqueChance *= 1.20; // 20% boost
		}
		
		for (RareRewardInfo rewardInfo : yourRewards) {
			double itemChance = (double) rewardInfo.getWeigth() / TOTAL_WEIGHT;
			double realChance = itemChance * uniqueChance;

			double roll = Math.random();
			if (roll < realChance) {
				// "jackpot"
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + player.getNameSmartUp() + " has received " + ItemDef.forID(rewardInfo.getItem().getId()).name + " from Raids.");
				if (player.raidsManager != null)
					player.getPacketSender().addObjectPacket(30030, player.raidsManager.greatOlmManager.chestLocation, 0, 0);
				player.getPacketSender().addConstructionObjectPacket(new GameObject(30030, player.getCurrentLocation().transform(0, 1, 1), 0, 10));//dummy light on z1
				return new Item[] { rewardInfo.getItem() };
			}
		}

		Item[] rewards = new Item[2];
		for (int i = 0; i < rewards.length; i++) {
			CommonRewardInfo commonRewardInfo = Misc.random(CommonRewardInfo.values);
			Item item = commonRewardInfo.getItem();
			Item reward;
			if (commonRewardInfo.getDivider() == 1) {
				reward = new Item(item.getId(), Misc.random(1, item.getAmount()));
			} else {
				reward = new Item(item.getId(), Math.min(Math.max(1, points / commonRewardInfo.getDivider()), item.getAmount()));
			}

			rewards[i] = reward;
		}

		if (player.raidsManager != null)
			player.getPacketSender().addObjectPacket(30029, player.raidsManager.greatOlmManager.chestLocation, 0, 0);
		player.getPacketSender().addConstructionObjectPacket(new GameObject(30029, player.getCurrentLocation().transform(0, 0, 1), 0, 10));//dummy light on z1
		return rewards;
	}

}
