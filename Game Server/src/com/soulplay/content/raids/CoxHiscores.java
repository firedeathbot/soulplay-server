package com.soulplay.content.raids;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

public class CoxHiscores {

	public static void saveScore(RaidsManager raid) {
		if (!Config.RUN_ON_DEDI)
			return;
		if (raid.party.getMembers().size() == 1) {
			//save solo
			saveSolo(raid);
		} else {
			//save clan
			saveClan(raid);
		}
	}
	
	private static final String SOLO_SQL = "INSERT INTO `cox_solo_hs`(`player_id`, `ticks`, `settings`, `timestamp`) VALUES (?, ?, ?, ?)";
	
	private static void saveSolo(RaidsManager raid) {
		
		final long playerMID = raid.party.getClanMID();
		
		final int ticks = Server.getTotalTicks() - raid.startTicks;
		
		final Timestamp timestamp = Misc.getESTTimestamp();
		
		TaskExecutor.executeWebsiteSQL(()-> {
			
			try (Connection con = SqlHandler.getHikariZaryteTable().getConnection();
					PreparedStatement stmt = con.prepareStatement(SOLO_SQL)) {
				
				stmt.setLong(1, playerMID);
				stmt.setInt(2, ticks);
				stmt.setInt(3, CoxSettings.GREAT_OLM_ONLY.getSettingId()); // might use this for settings when raids is fully done
				stmt.setTimestamp(4, timestamp);
				stmt.executeUpdate();
				
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		});
	}
	
	private static final String CLAN_SQL = "INSERT INTO `cox_party_hs`(`clan_id`, `ticks`, `party_size`, `settings`, `timestamp`) VALUES (?, ?, ?, ?, ?)";
	
	private static void saveClan(RaidsManager raid) {
		
		final int clanId = raid.party.getClanMID();
		
		final int ticks = Server.getTotalTicks() - raid.startTicks;
		
		final int partySize = raid.party.getMembers().size();
		
		final Timestamp timestamp = Misc.getESTTimestamp();
		
		TaskExecutor.executeWebsiteSQL(()-> {
			
			try (Connection con = SqlHandler.getHikariZaryteTable().getConnection();
					PreparedStatement stmt = con.prepareStatement(CLAN_SQL)) {
				
				stmt.setInt(1, clanId);
				stmt.setInt(2, ticks);
				stmt.setInt(3, partySize);
				stmt.setInt(4, CoxSettings.GREAT_OLM_ONLY.getSettingId()); // might use this for settings when raids is fully done
				stmt.setTimestamp(5, timestamp);
				stmt.executeUpdate();
				
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		});
	}
	
	
}
