package com.soulplay.content.raids;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class RaidsPoints {

	public static final int POINTS_PER_DMG = 5;

	private int totalPoints = 0;
	private final Map<Integer, Integer> points = new HashMap<>();

	public int increasePoints(int mysqlIndex, int amount) {
		final int currentPoints = getPoints(mysqlIndex);
		final int newPoints = currentPoints + amount;
		points.put(mysqlIndex, newPoints);
		totalPoints += amount;
		return newPoints;
	}

	public int getPoints(int mysqlIndex) {
		Integer pointsVal = points.get(mysqlIndex);
		return pointsVal != null ? pointsVal : 0;
	}

	public int getTotalPoints() {
		return totalPoints;
	}
	
	public void handleDeath(int mysqlIndex) {
		final int currentPoints = getPoints(mysqlIndex);
		if (currentPoints < 100) {
			//reduce ponits from the whole team
			for (Entry<Integer, Integer> entry : points.entrySet()) {
				//int mId = entry.getKey();
				final int points = entry.getValue();
				final int reduction = (int) (points * 0.10);// 10% reduction
				final int newAmount = points - reduction;
				entry.setValue(newAmount);
				totalPoints -= reduction;
			}
		} else {
			final int reduction = (int) (currentPoints * 0.25);// 25% reduction
			final int newAmount = currentPoints - reduction;
			points.put(mysqlIndex, newAmount);
			totalPoints -= reduction;
		}
	}

}
