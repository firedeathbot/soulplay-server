package com.soulplay.content.raids.map;

public class RaidsLayout {

	RaidsFloor floor1;
	RaidsFloor floor2;
	
	public RaidsLayout(RaidsFloor floor1, RaidsFloor floor2) {
		this.floor1 = floor1;
		this.floor2 = floor2;
	}
	
}
