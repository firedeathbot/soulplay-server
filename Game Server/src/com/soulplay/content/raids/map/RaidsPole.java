package com.soulplay.content.raids.map;

import com.soulplay.game.world.Direction;

public enum RaidsPole {
	NORTH(Direction.NORTH),
	SOUTH(Direction.SOUTH),
	EAST(Direction.EAST),
	WEST(Direction.WEST);
	
	private final Direction direction;
	
	RaidsPole(Direction direction) {
		this.direction = direction;
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	public static final RaidsPole[] VALUES = values();
}
