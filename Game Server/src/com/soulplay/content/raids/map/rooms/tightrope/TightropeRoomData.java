package com.soulplay.content.raids.map.rooms.tightrope;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum TightropeRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 668),
	NORTH(RaidsPole.NORTH, 1212, 668),
	EAST(RaidsPole.EAST, 1216, 668);
	
	private final RaidsPole pole;
	private final int mapZoneX;
	private final int mapZoneY;
	
	TightropeRoomData(RaidsPole pole, int mapZoneX, int mapZoneY) {
		this.pole = pole;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	
	@Override
	public RaidsPole getPole() {
		return pole;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 1;
	}
}
