package com.soulplay.content.raids.map.rooms.creaturekeeper;

import com.soulplay.content.raids.map.RaidsRoom;

public class CreatureKeeperRoom extends RaidsRoom<CreatureKeeperRoomData> {
	
	public static final int ID = 1;
	
	public CreatureKeeperRoom(CreatureKeeperRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
