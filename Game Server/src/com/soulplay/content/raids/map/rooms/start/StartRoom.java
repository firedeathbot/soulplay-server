package com.soulplay.content.raids.map.rooms.start;

import com.soulplay.content.raids.map.RaidsRoom;

public class StartRoom extends RaidsRoom<StartRoomData> {
	
	public StartRoom(StartRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
	
}
