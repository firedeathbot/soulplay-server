package com.soulplay.content.raids.map.rooms.greatolm;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public class GreatOlmRoomData implements RaidsRoomData {
	
	public static final GreatOlmRoomData INSTANCE = new GreatOlmRoomData();
	
	@Override
	public RaidsPole getPole() {
		return RaidsPole.NORTH;
	}
	
	@Override
	public int getMapZoneX() {
		return 1200;
	}
	
	@Override
	public int getMapZoneY() {
		return 712;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
	
}