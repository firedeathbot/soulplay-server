package com.soulplay.content.raids.map.rooms.tekton;

import com.soulplay.content.raids.map.RaidsRoomData;
import com.soulplay.content.raids.map.RaidsPole;

public enum TektonRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 660),
	NORTH(RaidsPole.NORTH, 1212, 660),
	EAST(RaidsPole.EAST, 1216, 660);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	TektonRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 1;
	}
}
