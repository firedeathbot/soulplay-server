package com.soulplay.content.raids.map.rooms.startsecond;

import com.soulplay.content.raids.map.RaidsRoom;

public class StartSecondRoom extends RaidsRoom<StartSecondRoomData> {
	
	public StartSecondRoom(StartSecondRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
