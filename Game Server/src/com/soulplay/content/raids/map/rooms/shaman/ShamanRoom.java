package com.soulplay.content.raids.map.rooms.shaman;

import com.soulplay.content.raids.map.RaidsRoom;

public class ShamanRoom extends RaidsRoom<ShamanRoomData> {
	
	public static final int rotationID = 4;
	
	public ShamanRoom(ShamanRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}