package com.soulplay.content.raids.map.rooms.end;

import com.soulplay.content.raids.map.RaidsRoom;

public class EndRoom extends RaidsRoom<EndRoomData> {
	
	public EndRoom(EndRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
	
}
