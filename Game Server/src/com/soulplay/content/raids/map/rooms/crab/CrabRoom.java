package com.soulplay.content.raids.map.rooms.crab;

import com.soulplay.content.raids.map.RaidsRoom;

public class CrabRoom extends RaidsRoom<CrabRoomData> {
	
	public static final int ID = 0;
	
	public CrabRoom(CrabRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
