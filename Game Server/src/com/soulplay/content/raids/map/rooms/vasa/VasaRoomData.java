package com.soulplay.content.raids.map.rooms.vasa;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum VasaRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 660),
	NORTH(RaidsPole.NORTH, 1212, 660),
	EAST(RaidsPole.EAST, 1216, 660);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	VasaRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
}
