package com.soulplay.content.raids.map.rooms.start;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum StartRoomData implements RaidsRoomData {
	WEST(14, 3, RaidsPole.WEST, 1208, 648),
	NORTH(2, 4, RaidsPole.NORTH, 1212, 648),
	EAST(2, 4, RaidsPole.EAST, 1216, 648);
	
	int spawnCoordX;
	int spawnCoordY;
	RaidsPole pole;
	int mapZoneX;
	int mapZoneY;
	
	StartRoomData(int spawnCoordX, int spawnCoordY, RaidsPole pole, int mapZoneX, int mapZoneY) {
		this.spawnCoordX = spawnCoordX;
		this.spawnCoordY = spawnCoordY;
		this.pole = pole;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return pole;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}

	public int getSpawnCoordX() {
		return spawnCoordX;
	}

	public int getSpawnCoordY() {
		return spawnCoordY;
	}

}