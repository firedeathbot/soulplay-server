package com.soulplay.content.raids.map.rooms.greatolm;

import com.soulplay.Server;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

public class DouseFire {
	
	public static final int FIRE_OBJ = 32297;
	
	public static void douseFire(Player player, int obId, int obX, int obY) {
		if (player.dynObjectManager == null)
			return;
		
		if (!player.getStopWatch().checkThenStart(1)) return;
		
		GameObject fire = player.dynObjectManager.getObject(obId, obX, obY, player.getZ());
		
		if (fire == null || fire.getId() != FIRE_OBJ || !fire.isActive()) return;
		
		final SpellsData spell = SpellsData.getWaterSpell(player);
		
		if (spell == null) {
			player.sendMessage("You don't have any water spells to douse this fire.");
			resetObjectClick(player);
			player.faceLocation(fire.getX(), fire.getY());
			return;
		}
		
//		if (player.getCurrentLocation().isWithinDeltaDistance(fire.getLocation(), RSConstants.COMBAT_SPELL_DIST)) {
//			player.walkingToObject = false;
//			player.clickObjectType = 0;
//			player.destinationX = player.destinationY = 0;
//		}
		
		player.getPA().closeAllWindows();
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				
				if (!fire.isActive()) {
					container.stop();
					return;
				}
				
				boolean withinDistance = player.getCurrentLocation().isWithinDeltaDistance(fire.getLocation(), RSConstants.COMBAT_SPELL_DIST);
				//follow obj
				if (player.getMovement().isMovingOrHasWalkQueue() && withinDistance)
					player.getMovement().resetWalkingQueue();
//				if (player.getAttackTimer() > 0)
//					return;
				if (withinDistance) {
					resetObjectClick(player);
					shootWaterSpell(player, fire, spell);
					container.stop();
				}
				
			}
		}, 1);
		
	}
	
	private static void shootWaterSpell(Player c, GameObject o, SpellsData spell) {
		if (!MagicRequirements.checkMagicReqsNew(c, spell, true)) {
			return;
		}
		
		if (!o.isActive()) return;

		c.faceLocation(o.getX(), o.getY());
		c.setAttackTimer(5);
		c.getMovement().resetWalkingQueue();
		c.startAnimation(spell.getAnim());
		
		int ticksToHit = 2;
		
		if (spell.getStartGfx() != null)
			c.startGraphic(spell.getStartGfx());
		if (spell.getEndGfx() != null) {
			Server.getStillGraphicsManager().stillGraphics(c, o.getX(), o.getY(), 0, spell.getEndGfx().getId(), Misc.serverToClientTick(ticksToHit));
		}
		if (spell.getProjectile() != null) {
			Projectile proj = new Projectile(spell.getProjectile().getProjectileGFX(), c.getCurrentLocation().copyNew(), Location.create(o.getX(), o.getY(), c.getZ()));
			proj.setStartDelay(Misc.serverToClientTick(1));
			proj.setEndDelay(Misc.serverToClientTick(ticksToHit));
			proj.setEndHeight(0);
			GlobalPackets.createProjectile(proj);
		}
		
		c.teleGrabEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (c.dynObjectManager != null && o.isActive() && o.getTicks() > 0) {
					c.dynObjectManager.revertObject(o);
				}
				
			}
		}, ticksToHit);
		
	}
	
	private static void resetObjectClick(Player player) {
		player.walkingToObject = false;
		player.clickObjectType = 0;
		player.destinationX = player.destinationY = 0;
		player.finalLocalDestX = player.finalLocalDestY = 0;
	}

}
