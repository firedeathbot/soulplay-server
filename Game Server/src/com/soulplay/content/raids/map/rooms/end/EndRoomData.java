package com.soulplay.content.raids.map.rooms.end;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum EndRoomData implements RaidsRoomData {
	WEST(RaidsPole.SOUTH, 1208, 644),
	NORTH(RaidsPole.SOUTH, 1212, 644);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	EndRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
}
