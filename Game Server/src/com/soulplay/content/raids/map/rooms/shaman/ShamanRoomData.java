package com.soulplay.content.raids.map.rooms.shaman;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum ShamanRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 656),
	NORTH(RaidsPole.NORTH, 1212, 656),
	EAST(RaidsPole.EAST, 1216, 656);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	ShamanRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
}

