package com.soulplay.content.raids.map.rooms.startsecond;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum StartSecondRoomData implements RaidsRoomData {
	
	WEST(RaidsPole.WEST, 1208, 712),
	NORTH(RaidsPole.NORTH, 1212, 712),
	EAST(RaidsPole.EAST, 1216, 712);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	StartSecondRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
}
