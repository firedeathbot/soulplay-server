package com.soulplay.content.raids.map.rooms.tekton;

import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.raids.map.RaidsRoom;

public class TektonRoom extends RaidsRoom<TektonRoomData> {
	
	public static final int rotationID = 0;
	
	public TektonRoom(TektonRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}

	@Override
	public void onLoad(RaidsManager manager) {
		System.out.println("tekton spawned");
	}

}
