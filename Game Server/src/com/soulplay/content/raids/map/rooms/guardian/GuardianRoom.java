package com.soulplay.content.raids.map.rooms.guardian;

import com.soulplay.content.raids.map.RaidsRoom;

public class GuardianRoom extends RaidsRoom<GuardianRoomData> {
	
	public static final int rotationID = 2;
	
	public GuardianRoom(GuardianRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
