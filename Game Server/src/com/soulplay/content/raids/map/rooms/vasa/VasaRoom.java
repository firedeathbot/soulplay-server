package com.soulplay.content.raids.map.rooms.vasa;

import com.soulplay.content.raids.map.RaidsRoom;

public class VasaRoom extends RaidsRoom<VasaRoomData> {
	
	public static final int rotationID = 1;
	
	public VasaRoom(VasaRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
