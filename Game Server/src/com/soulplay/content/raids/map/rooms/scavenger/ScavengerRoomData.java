package com.soulplay.content.raids.map.rooms.scavenger;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum ScavengerRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 652, 0),
	NORTH(RaidsPole.NORTH, 1212, 652, 0),
	EAST(RaidsPole.EAST, 1216, 652, 0),
	WEST1(RaidsPole.WEST, 1208, 652, 1),
	NORTH1(RaidsPole.NORTH, 1212, 652, 1),
	EAST1(RaidsPole.EAST, 1216, 652, 1);
	
	RaidsPole direction;
	int mapZoneZ;
	int mapZoneX;
	int mapZoneY;
	
	ScavengerRoomData(RaidsPole direction, int mapZoneX, int mapZoneY, int mapZoneZ) {
		this.direction = direction;
		this.mapZoneZ = mapZoneZ;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return mapZoneZ;
	}
}
