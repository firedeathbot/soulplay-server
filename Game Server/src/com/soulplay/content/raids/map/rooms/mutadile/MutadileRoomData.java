package com.soulplay.content.raids.map.rooms.mutadile;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum MutadileRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 664),
	NORTH(RaidsPole.NORTH, 1212, 664),
	EAST(RaidsPole.EAST, 1216, 664);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	MutadileRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 1;
	}
}