package com.soulplay.content.raids.map.rooms.icedemon;

import com.soulplay.content.raids.map.RaidsRoom;

public class IceDemonRoom extends RaidsRoom<IceDemonRoomData> {
	
	public static final int ID = 2;
	
	public IceDemonRoom(IceDemonRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}