package com.soulplay.content.raids.map.rooms.vanguard;

import com.soulplay.content.raids.map.RaidsRoom;

public class VanguardRoom extends RaidsRoom<VanguardRoomData> {
	
	public static final int rotationID = 6;
	
	public VanguardRoom(VanguardRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
	
}
