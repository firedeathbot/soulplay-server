package com.soulplay.content.raids.map.rooms.crab;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum CrabRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 668),
	NORTH(RaidsPole.NORTH, 1212, 668),
	EAST(RaidsPole.EAST, 1216, 668);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	CrabRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 2;
	}
}
