package com.soulplay.content.raids.map.rooms.tightrope;

import com.soulplay.content.raids.map.RaidsRoom;

public class TightropeRoom extends RaidsRoom<TightropeRoomData> {
	
	public static final int ID = 3;
	
	public TightropeRoom(TightropeRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
