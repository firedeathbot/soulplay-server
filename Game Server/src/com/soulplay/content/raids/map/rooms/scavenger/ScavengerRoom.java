package com.soulplay.content.raids.map.rooms.scavenger;

import com.soulplay.content.raids.map.RaidsRoom;

public class ScavengerRoom extends RaidsRoom<ScavengerRoomData> {
	
	public ScavengerRoom(ScavengerRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
	
}
