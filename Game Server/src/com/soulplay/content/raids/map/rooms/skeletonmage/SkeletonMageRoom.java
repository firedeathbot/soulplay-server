package com.soulplay.content.raids.map.rooms.skeletonmage;

import com.soulplay.content.raids.map.RaidsRoom;

public class SkeletonMageRoom extends RaidsRoom<SkeletonMageRoomData> {

	public static final int rotationID = 3;

	public SkeletonMageRoom(SkeletonMageRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}

}
