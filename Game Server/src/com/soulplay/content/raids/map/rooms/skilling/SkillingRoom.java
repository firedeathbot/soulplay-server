package com.soulplay.content.raids.map.rooms.skilling;

import com.soulplay.content.raids.map.RaidsRoom;

public class SkillingRoom extends RaidsRoom<SkillingRoomData> {
	
	public SkillingRoom(SkillingRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
	
}
