package com.soulplay.content.raids.map.rooms.mutadile;

import com.soulplay.content.raids.map.RaidsRoom;

public class MutadileRoom extends RaidsRoom<MutadileRoomData> {
	
	public static final int rotationID = 5;
	
	public MutadileRoom(MutadileRoomData data, int x, int y, int z) {
		super(data, x, y, z);
	}
}
