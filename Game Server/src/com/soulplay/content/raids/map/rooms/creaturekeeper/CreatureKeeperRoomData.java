package com.soulplay.content.raids.map.rooms.creaturekeeper;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum CreatureKeeperRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 672),
	NORTH(RaidsPole.NORTH, 1212, 672),
	EAST(RaidsPole.EAST, 1216, 672);
	
	RaidsPole direction;
	int mapZoneX;
	int mapZoneY;
	
	CreatureKeeperRoomData(RaidsPole direction, int mapZoneX, int mapZoneY) {
		this.direction = direction;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
}
