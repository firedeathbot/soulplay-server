package com.soulplay.content.raids.map.rooms.blank;

import com.soulplay.content.raids.map.RaidsRoom;

public class BlankRoom extends RaidsRoom<BlankRoomData> {
	
	public BlankRoom(int x, int y, int z) {
		super(BlankRoomData.INSTANCE, x, y, z);
	}
	
}
