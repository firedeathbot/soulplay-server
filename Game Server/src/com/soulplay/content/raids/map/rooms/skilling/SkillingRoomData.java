package com.soulplay.content.raids.map.rooms.skilling;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public enum SkillingRoomData implements RaidsRoomData {
	WEST(RaidsPole.WEST, 1208, 680, 0),
	NORTH(RaidsPole.NORTH, 1212, 680, 0),
	EAST(RaidsPole.EAST, 1216, 680, 0),
	WEST1(RaidsPole.WEST, 1208, 680, 1),
	NORTH1(RaidsPole.NORTH, 1212, 680, 1),
	EAST1(RaidsPole.EAST, 1216, 680, 1);
	
	RaidsPole direction;
	int mapZoneZ;
	int mapZoneX;
	int mapZoneY;
	
	SkillingRoomData(RaidsPole direction, int mapZoneX, int mapZoneY, int mapZoneZ) {
		this.direction = direction;
		this.mapZoneZ = mapZoneZ;
		this.mapZoneX = mapZoneX;
		this.mapZoneY = mapZoneY;
	}
	
	@Override
	public RaidsPole getPole() {
		return direction;
	}
	
	@Override
	public int getMapZoneX() {
		return mapZoneX;
	}
	
	@Override
	public int getMapZoneY() {
		return mapZoneY;
	}
	
	@Override
	public int getMapZoneZ() {
		return mapZoneZ;
	}
}
