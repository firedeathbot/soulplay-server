package com.soulplay.content.raids.map.rooms.vespula;

import com.soulplay.content.raids.map.RaidsRoom;

public class VespulaRoom extends RaidsRoom<VespulaRoomData> {
	
	public static final int rotationID = 7;
	
	public VespulaRoom(VespulaRoomData data, int x, int y, int z) {
		super(data,x, y, z);
	}
	
}
