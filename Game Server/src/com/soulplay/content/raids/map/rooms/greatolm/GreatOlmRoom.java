package com.soulplay.content.raids.map.rooms.greatolm;

import com.soulplay.content.raids.map.RaidsRoom;

public class GreatOlmRoom extends RaidsRoom<GreatOlmRoomData> {

	public GreatOlmRoom(int x, int y, int z) {
		super(GreatOlmRoomData.INSTANCE, x, y, z);
	}

}