package com.soulplay.content.raids.map.rooms.blank;

import com.soulplay.content.raids.map.RaidsPole;
import com.soulplay.content.raids.map.RaidsRoomData;

public class BlankRoomData implements RaidsRoomData {
	
	public static final BlankRoomData INSTANCE = new BlankRoomData();
	
	@Override
	public RaidsPole getPole() {
		return RaidsPole.NORTH;
	}
	
	@Override
	public int getMapZoneX() {
		return 1216;
	}
	
	@Override
	public int getMapZoneY() {
		return 647;
	}
	
	@Override
	public int getMapZoneZ() {
		return 0;
	}
	
}