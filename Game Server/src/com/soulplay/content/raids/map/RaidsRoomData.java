package com.soulplay.content.raids.map;

public interface RaidsRoomData {

	RaidsPole getPole();

	int getMapZoneX();

	int getMapZoneY();

	int getMapZoneZ();

}
