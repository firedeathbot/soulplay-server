package com.soulplay.content.raids.map;

import com.soulplay.game.world.Direction;

public class RaidsFloor {

	public final RaidsRoomType[][] rooms;
	public final Direction[][] directions;
	
	public RaidsFloor(RaidsRoomType[][] rooms, Direction[][] directions) {
		this.rooms = rooms;
		this.directions = directions;
	}
	
}
