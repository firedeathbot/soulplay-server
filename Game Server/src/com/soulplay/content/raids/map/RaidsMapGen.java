package com.soulplay.content.raids.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.raids.map.rooms.blank.BlankRoom;
import com.soulplay.content.raids.map.rooms.crab.CrabRoom;
import com.soulplay.content.raids.map.rooms.crab.CrabRoomData;
import com.soulplay.content.raids.map.rooms.creaturekeeper.CreatureKeeperRoom;
import com.soulplay.content.raids.map.rooms.creaturekeeper.CreatureKeeperRoomData;
import com.soulplay.content.raids.map.rooms.end.EndRoom;
import com.soulplay.content.raids.map.rooms.end.EndRoomData;
import com.soulplay.content.raids.map.rooms.greatolm.GreatOlmRoom;
import com.soulplay.content.raids.map.rooms.guardian.GuardianRoom;
import com.soulplay.content.raids.map.rooms.guardian.GuardianRoomData;
import com.soulplay.content.raids.map.rooms.icedemon.IceDemonRoom;
import com.soulplay.content.raids.map.rooms.icedemon.IceDemonRoomData;
import com.soulplay.content.raids.map.rooms.mutadile.MutadileRoom;
import com.soulplay.content.raids.map.rooms.mutadile.MutadileRoomData;
import com.soulplay.content.raids.map.rooms.scavenger.ScavengerRoom;
import com.soulplay.content.raids.map.rooms.scavenger.ScavengerRoomData;
import com.soulplay.content.raids.map.rooms.shaman.ShamanRoom;
import com.soulplay.content.raids.map.rooms.shaman.ShamanRoomData;
import com.soulplay.content.raids.map.rooms.skeletonmage.SkeletonMageRoom;
import com.soulplay.content.raids.map.rooms.skeletonmage.SkeletonMageRoomData;
import com.soulplay.content.raids.map.rooms.skilling.SkillingRoom;
import com.soulplay.content.raids.map.rooms.skilling.SkillingRoomData;
import com.soulplay.content.raids.map.rooms.start.StartRoom;
import com.soulplay.content.raids.map.rooms.start.StartRoomData;
import com.soulplay.content.raids.map.rooms.startsecond.StartSecondRoom;
import com.soulplay.content.raids.map.rooms.startsecond.StartSecondRoomData;
import com.soulplay.content.raids.map.rooms.tekton.TektonRoom;
import com.soulplay.content.raids.map.rooms.tekton.TektonRoomData;
import com.soulplay.content.raids.map.rooms.tightrope.TightropeRoom;
import com.soulplay.content.raids.map.rooms.tightrope.TightropeRoomData;
import com.soulplay.content.raids.map.rooms.vanguard.VanguardRoom;
import com.soulplay.content.raids.map.rooms.vanguard.VanguardRoomData;
import com.soulplay.content.raids.map.rooms.vasa.VasaRoom;
import com.soulplay.content.raids.map.rooms.vasa.VasaRoomData;
import com.soulplay.content.raids.map.rooms.vespula.VespulaRoom;
import com.soulplay.content.raids.map.rooms.vespula.VespulaRoomData;
import com.soulplay.game.world.Direction;
import com.soulplay.util.Misc;

public class RaidsMapGen {
	
	public static final List<Integer> PUZZLES = new ArrayList<>();
	public StartRoom start;
	public List<Integer> puzzles;
	public int[] rotation;
	public int rotationPos;
	public boolean stepForward;
	
	public RaidsMapGen() {
		puzzles = new ArrayList<>(PUZZLES);
		Collections.shuffle(puzzles);
		rotation = RaidsLayoutManager.chooseRandomRotation();
		rotationPos = rotation[Misc.randomNoPlus(rotation.length)];
		stepForward = Misc.random(0) == 0;
	}

	public void generate(RaidsManager manager) {
		RaidsLayout layout = RaidsLayoutManager.chooseRandomLayout();
		RaidsRoom olmRoom = new GreatOlmRoom(0, 0, 1);
		olmRoom.loadRoom(0, 0, manager, Direction.NORTH);
		
		manager.greatOlmManager.setRoom(olmRoom);
		
		//generateFloor(manager, layout.floor1, 3);
		/*generateFloor(manager, layout.floor2, 2);
		
		//Olm
		RaidsRoom olmRoom = new GreatOlmRoom(0, 0, 1);
		olmRoom.loadRoom(manager, 0, 0, Direction.NORTH);
		
		RaidsRoom nullRoom = new BlankRoom(0, 0, 1);
		loop:
		for (int y = 0; y < 2; y++) {
			for (int x = 2; x < 4; x++) {
				nullRoom.loadRoom(manager, x, y, Direction.NORTH);
			}
		}*/
	}
	
	public void generateFloor(RaidsManager manager, RaidsFloor floor, int height) {
		int startRoomX = 0;
		int startRoomY = 0;
		Direction nextDirection = null;
		RaidsRoomType room = null;
		loop:
		for (int y = 0; y < floor.rooms.length; y++) {
			for (int x = 0; x < floor.rooms[y].length; x++) {
				if (floor.rooms[y][x] == RaidsRoomType.START) {
					room = floor.rooms[y][x];
					nextDirection = floor.directions[y][x];
					startRoomX = x;
					startRoomY = y;
					break loop;
				}
			}
		}
		
		if (nextDirection == null) {
			System.err.println("Invalid next direction. Can't make a raids layout.");
			return;
		}
		
		Direction cameFrom = null;
		while (!Thread.interrupted()) {
			//System.out.println(startRoomX+":"+startRoomY+":"+nextDirection + " - " + floor.rooms[startRoomY][startRoomX]);
			
			int normalizedY = 1 - startRoomY;
			RaidsRoom xericRoom = pickRoom(manager, room, startRoomX, normalizedY, height, nextDirection, cameFrom);
			if (room == RaidsRoomType.START && height == 3) {
				start = (StartRoom) xericRoom;
			}
			if (xericRoom != null) {
				Direction rotate = Direction.NORTH;
				if (room != RaidsRoomType.START) {//We don't rotate start room for reasons
					if (nextDirection != null) {
						Direction entrance = Direction.SOUTH;
						Direction exit = xericRoom.getData().getPole().getDirection();
						/*if (room == RaidsRoomType.SCAVENGER) {
							System.out.println(cameFrom + ":" + nextDirection + " - " + room);
							System.out.println(entrance + ":" + exit + " - " + room);
						}*/
						int i = 0;
						while (i < 3) {
							//if ((entrance == cameFrom || exit == cameFrom) && (exit == nextDirection || entrance == nextDirection)) {
							if (entrance == cameFrom && exit == nextDirection) {
								//System.out.println(cameFrom+":"+nextDirection + " - " + entrance +":"+exit+" - " + i + " - " + room);
								break;
							}
							i++;
							
							entrance = Direction.rotate(entrance);
							exit = Direction.rotate(exit);
							//if (room == RaidsRoomType.BOSS) {
							//	System.out.println(entrance+":"+exit);
							//}
						}
						rotate = Direction.get(i);
						//if (room == RaidsRoomType.BOSS) {
						//	System.out.println(entrance + ":" + exit + " - " + room);
						//	System.out.println(i);
						//}
					} else {
						Direction entrance = Direction.SOUTH;
						//System.out.println(entrance+":"+cameFrom+" - " + room);
						int i = 0;
						while (i < 3) {
							if (entrance == cameFrom) {
								break;
							}
							i++;
							entrance = Direction.rotate(entrance);
						}
						
						rotate = Direction.get(i);
						//System.out.println(entrance);
					}
				}
				
				xericRoom.loadRoom(startRoomX, normalizedY, manager, rotate);
			}
			
			if (nextDirection == Direction.EAST) {
				startRoomX++;
			} else if (nextDirection == Direction.WEST) {
				startRoomX--;
			} else if (nextDirection == Direction.SOUTH) {
				startRoomY++;
			} else if (nextDirection == Direction.NORTH) {
				startRoomY--;
			}
			
			if (room == RaidsRoomType.END) {
				break;
			}
			
			cameFrom = nextDirection.getOpposite();
			nextDirection = floor.directions[startRoomY][startRoomX];
			room = floor.rooms[startRoomY][startRoomX];
		}
		//Fill null rooms
		RaidsRoom nullRoom = new BlankRoom(0, 0, height);
		loop:
		for (int y = 0; y < floor.rooms.length; y++) {
			for (int x = 0; x < floor.rooms[y].length; x++) {
				if (floor.rooms[y][x] == null) {
					int normalizedY = 1 - y;
					//System.out.println("put null room at " + x+":"+normalizedY+":"+height);
					nullRoom.loadRoom(x, normalizedY, manager, Direction.NORTH);
				}
			}
		}
	}
	
	private RaidsRoom pickRoom(RaidsManager manager,
	                                     RaidsRoomType type,
	                                     int x, int y, int z,
	                                     Direction nextDirection,
	                                     Direction cameFrom) {
		//System.out.println(x+":"+y+":"+nextDirection+ ":"+cameFrom+" - "+ type);
		switch (type) {
			case START: {
				if (z == 2) {
					StartSecondRoomData data;
					if (nextDirection == Direction.EAST) {
						data = StartSecondRoomData.EAST;
					} else if (nextDirection == Direction.WEST) {
						data = StartSecondRoomData.WEST;
					} else {
						data = StartSecondRoomData.NORTH;
					}
					
					return new StartSecondRoom(data, x, y, z);
				} else {
					StartRoomData data;
					if (nextDirection == Direction.EAST) {
						data = StartRoomData.EAST;
					} else if (nextDirection == Direction.WEST) {
						data = StartRoomData.WEST;
					} else {
						data = StartRoomData.NORTH;
					}
					
					return new StartRoom(data, x, y, z);
				}
			}
			case SCAVENGER: {
				ScavengerRoomData data;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					if (Misc.random(0) == 0) {
						data = ScavengerRoomData.NORTH1;
					} else {
						data = ScavengerRoomData.NORTH;
					}
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						if (Misc.random(0) == 0) {
							data = ScavengerRoomData.WEST1;
						} else {
							data = ScavengerRoomData.WEST;
						}
					} else {
						if (Misc.random(0) == 0) {
							data = ScavengerRoomData.EAST;
						} else {
							data = ScavengerRoomData.EAST1;
						}
					}
				}
				return new ScavengerRoom(data, x, y, z);
			}
			case SUPPLIES: {
				SkillingRoomData data;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					if (Misc.random(0) == 0) {
						data = SkillingRoomData.NORTH1;
					} else {
						data = SkillingRoomData.NORTH;
					}
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						if (Misc.random(0) == 0) {
							data = SkillingRoomData.WEST1;
						} else {
							data = SkillingRoomData.WEST;
						}
					} else {
						if (Misc.random(0) == 0) {
							data = SkillingRoomData.EAST;
						} else {
							data = SkillingRoomData.EAST1;
						}
					}
				}
				return new SkillingRoom(data, x, y, z);
			}
			case PUZZLE: {
				Integer id = puzzles.remove(0);
				switch (id) {
					case CrabRoom.ID: {
						CrabRoomData data = CrabRoomData.EAST;
						if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
							data = CrabRoomData.NORTH;
						} else {
							if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
								data = CrabRoomData.WEST;
							} else {
								data = CrabRoomData.EAST;
							}
						}
						return new CrabRoom(data, x, y, z);
					}
					case CreatureKeeperRoom.ID: {
						CreatureKeeperRoomData data = CreatureKeeperRoomData.EAST;
						if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
							data = CreatureKeeperRoomData.NORTH;
						} else {
							if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
								data = CreatureKeeperRoomData.WEST;
							} else {
								data = CreatureKeeperRoomData.EAST;
							}
						}
						return new CreatureKeeperRoom(data, x, y, z);
					}
					case IceDemonRoom.ID: {
						IceDemonRoomData data = IceDemonRoomData.EAST;
						if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
							data = IceDemonRoomData.NORTH;
						} else {
							if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
								data = IceDemonRoomData.WEST;
							} else {
								data = IceDemonRoomData.EAST;
							}
						}
						return new IceDemonRoom(data, x, y, z);
					}
					case TightropeRoom.ID: {
						TightropeRoomData data = TightropeRoomData.EAST;
						if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
							data = TightropeRoomData.NORTH;
						} else {
							if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
								data = TightropeRoomData.WEST;
							} else {
								data = TightropeRoomData.EAST;
							}
						}
						return new TightropeRoom(data, x, y, z);
					}
					
				}
			}
			case BOSS: {
				return pickBossRoom(manager, x, y, z, nextDirection, cameFrom);
			}
			case END: {
				EndRoomData data = EndRoomData.WEST;
				if (z == 2) {
					data = EndRoomData.NORTH;
				}
				return new EndRoom(data, x, y, z);
			}
		}
		return null;
	}
	
	private RaidsRoom pickBossRoom(RaidsManager activity, int x, int y, int z, Direction nextDirection, Direction cameFrom) {
		int pickedRoration = rotation[rotationPos];
		advanceRotationPos();
		switch (pickedRoration) {
			case TektonRoom.rotationID: {
				TektonRoomData data = TektonRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = TektonRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = TektonRoomData.WEST;
					} else {
						data = TektonRoomData.EAST;
					}
				}
				return new TektonRoom(data, x, y, z);
			}
			case VasaRoom.rotationID: {
				VasaRoomData data = VasaRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = VasaRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = VasaRoomData.WEST;
					} else {
						data = VasaRoomData.EAST;
					}
				}
				return new VasaRoom(data, x, y, z);
			}
			case GuardianRoom.rotationID: {
				GuardianRoomData data = GuardianRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = GuardianRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = GuardianRoomData.WEST;
					} else {
						data = GuardianRoomData.EAST;
					}
				}
				return new GuardianRoom(data, x, y, z);
			}
			case SkeletonMageRoom.rotationID: {
				SkeletonMageRoomData data = SkeletonMageRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = SkeletonMageRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = SkeletonMageRoomData.WEST;
					} else {
						data = SkeletonMageRoomData.EAST;
					}
				}
				return new SkeletonMageRoom(data, x, y, z);
			}
			case ShamanRoom.rotationID: {
				ShamanRoomData data = ShamanRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = ShamanRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = ShamanRoomData.WEST;
					} else {
						data = ShamanRoomData.EAST;
					}
				}
				return new ShamanRoom(data, x, y, z);
			}
			case MutadileRoom.rotationID: {
				MutadileRoomData data = MutadileRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = MutadileRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = MutadileRoomData.WEST;
					} else {
						data = MutadileRoomData.EAST;
					}
				}
				return new MutadileRoom(data, x, y, z);
			}
			case VanguardRoom.rotationID: {
				VanguardRoomData data = VanguardRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = VanguardRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = VanguardRoomData.WEST;
					} else {
						data = VanguardRoomData.EAST;
					}
				}
				return new VanguardRoom(data, x, y, z);
			}
			case VespulaRoom.rotationID: {
				VespulaRoomData data = VespulaRoomData.EAST;
				if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
					data = VespulaRoomData.NORTH;
				} else {
					if (nextDirection == Direction.WEST && cameFrom == Direction.SOUTH || nextDirection == Direction.NORTH && cameFrom == Direction.WEST || nextDirection == Direction.EAST && cameFrom == Direction.NORTH || nextDirection == Direction.SOUTH && cameFrom == Direction.EAST) {
						data = VespulaRoomData.WEST;
					} else {
						data = VespulaRoomData.EAST;
					}
				}
				return new VespulaRoom(data, x, y, z);
			}
		}
		TektonRoomData data = TektonRoomData.EAST;
		if (x >= 1 && x <= 2 && (nextDirection == Direction.EAST || nextDirection == Direction.WEST) && (cameFrom != Direction.SOUTH && cameFrom != Direction.NORTH)) {
			data = TektonRoomData.NORTH;
		}
		return new TektonRoom(data, x, y, z);
	}
	
	public void advanceRotationPos() {
		if (stepForward) {
			rotationPos++;
			if (rotationPos >= rotation.length) {
				rotationPos = 0;
			}
		} else {
			rotationPos--;
			if (rotationPos < 0) {
				rotationPos = rotation.length - 1;
			}
		}
	}
	
	static {
		PUZZLES.add(CrabRoom.ID);
		PUZZLES.add(CreatureKeeperRoom.ID);
		PUZZLES.add(IceDemonRoom.ID);
		PUZZLES.add(TightropeRoom.ID);
	}
}
