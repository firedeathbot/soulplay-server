package com.soulplay.content.raids.map;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.raids.map.rooms.guardian.GuardianRoom;
import com.soulplay.content.raids.map.rooms.mutadile.MutadileRoom;
import com.soulplay.content.raids.map.rooms.shaman.ShamanRoom;
import com.soulplay.content.raids.map.rooms.skeletonmage.SkeletonMageRoom;
import com.soulplay.content.raids.map.rooms.tekton.TektonRoom;
import com.soulplay.content.raids.map.rooms.vanguard.VanguardRoom;
import com.soulplay.content.raids.map.rooms.vasa.VasaRoom;
import com.soulplay.content.raids.map.rooms.vespula.VespulaRoom;
import com.soulplay.game.world.Direction;
import com.soulplay.util.Misc;

public class RaidsLayoutManager {
	
	public static List<RaidsLayout> layouts = new ArrayList<>();
	
	public static RaidsLayout chooseRandomLayout() {
		return layouts.get(Misc.randomNoPlus(layouts.size()));
	}
	
	public static final int[] ROTATION_1 = {TektonRoom.rotationID, VasaRoom.rotationID, GuardianRoom.rotationID, SkeletonMageRoom.rotationID, ShamanRoom.rotationID, MutadileRoom.rotationID, VanguardRoom.rotationID, VespulaRoom.rotationID};
	public static final int[] ROTATION_2 = {TektonRoom.rotationID, MutadileRoom.rotationID, GuardianRoom.rotationID, VespulaRoom.rotationID, ShamanRoom.rotationID, VasaRoom.rotationID, VanguardRoom.rotationID, SkeletonMageRoom.rotationID};
	
	public static int[] chooseRandomRotation() {
		if (Misc.random(0) == 0) {
			return ROTATION_1;
		} else {
			return ROTATION_2;
		}
	}
	
	public static void addFloor1(RaidsFloor firstFloor, RaidsFloor secondFloor) {
		layouts.add(new RaidsLayout(firstFloor, secondFloor));
	}
	
	static {
		addFloor1(//1
				new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.END, RaidsRoomType.SCAVENGER, RaidsRoomType.START, RaidsRoomType.SCAVENGER}
				}, new Direction[][] {
						{null, Direction.SOUTH, Direction.WEST, Direction.WEST},
						{null, Direction.WEST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.END},
						{RaidsRoomType.START, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES}
				}, new Direction[][] {
						{null, Direction.EAST, Direction.SOUTH, null},
						{Direction.EAST, Direction.NORTH, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//2
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.START, null},
						{RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, null},
						{Direction.EAST, Direction.EAST, Direction.EAST, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, null, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS},
						{RaidsRoomType.SUPPLIES, RaidsRoomType.SCAVENGER, RaidsRoomType.PUZZLE, RaidsRoomType.START}
				}, new Direction[][] {
						{null, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, Direction.NORTH}
				}));
		addFloor1(//3
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.START, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS},
						{RaidsRoomType.SUPPLIES, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.EAST, Direction.SOUTH},
						{Direction.EAST, Direction.EAST, Direction.NORTH, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.PUZZLE, RaidsRoomType.PUZZLE, RaidsRoomType.START}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.EAST, null},
						{Direction.NORTH, Direction.WEST, Direction.WEST, Direction.WEST}
				}));
		addFloor1(//4
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, null, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER},
						{RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES, RaidsRoomType.SCAVENGER, RaidsRoomType.START}
				}, new Direction[][] {
						{null, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.START, RaidsRoomType.BOSS, RaidsRoomType.END, RaidsRoomType.SUPPLIES},
						{null, RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, null, Direction.WEST},
						{null, Direction.EAST, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//5
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, RaidsRoomType.BOSS, null},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.START, RaidsRoomType.PUZZLE, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.SOUTH, null},
						{Direction.NORTH, Direction.WEST, Direction.EAST, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, null, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS},
						{RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.START}
				}, new Direction[][] {
						{null, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, Direction.NORTH}
				}));
		addFloor1(//6
				new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.END, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.START, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{null, null, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.START, null, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.PUZZLE}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, null, null},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//7
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.END, null, RaidsRoomType.START, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, Direction.WEST},
						{null, null, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.END},
						{RaidsRoomType.START, null, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.SOUTH, null},
						{Direction.NORTH, null, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//8
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.START, null, RaidsRoomType.END, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.SOUTH},
						{Direction.NORTH, null, null, Direction.WEST}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.START, null}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.EAST, null},
						{Direction.NORTH, Direction.WEST, Direction.WEST, null}
				}));
		addFloor1(//9
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.START, RaidsRoomType.SCAVENGER},
						{RaidsRoomType.END, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.EAST, Direction.SOUTH},
						{null, Direction.NORTH, Direction.WEST, Direction.WEST}
				}), new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.START, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, Direction.EAST, Direction.SOUTH},
						{Direction.NORTH, Direction.EAST, Direction.NORTH, null}
				}));
		addFloor1(//10
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.END},
						{RaidsRoomType.START, RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, null}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, Direction.EAST, null},
						{Direction.NORTH, Direction.EAST, Direction.NORTH, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.END, RaidsRoomType.SCAVENGER, RaidsRoomType.START},
						{RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, null}
				}, new Direction[][] {
						{Direction.EAST, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, null}
				}));
		addFloor1(//11
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.END, null, RaidsRoomType.START, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, Direction.WEST},
						{null, null, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.END},
						{RaidsRoomType.START, null, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.SOUTH, null},
						{Direction.NORTH, null, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//12
				new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.END, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.START, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{null, null, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.START, null, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, null, null},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//13
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER},
						{RaidsRoomType.BOSS, RaidsRoomType.END, null, RaidsRoomType.START}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, Direction.WEST},
						{Direction.EAST, null, null, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, null},
						{RaidsRoomType.BOSS, RaidsRoomType.START, RaidsRoomType.SUPPLIES, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.SOUTH, null},
						{Direction.NORTH, Direction.WEST, Direction.EAST, null}
				}));
		addFloor1(//14
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, null, RaidsRoomType.END},
						{RaidsRoomType.START, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, null, null},
						{Direction.NORTH, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.START},
						{RaidsRoomType.END, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.SOUTH, Direction.WEST},
						{null, Direction.NORTH, Direction.WEST, null}
				}));
		addFloor1(//15
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.START, null, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, null, null},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.SCAVENGER, RaidsRoomType.PUZZLE, RaidsRoomType.START},
						{RaidsRoomType.END, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.SOUTH, Direction.WEST},
						{null, Direction.NORTH, Direction.WEST, null}
				}));
		addFloor1(//16
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.END, RaidsRoomType.SCAVENGER, RaidsRoomType.START},
						{RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, null}
				}, new Direction[][] {
						{Direction.EAST, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.START, RaidsRoomType.SUPPLIES, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.EAST, null},
						{Direction.EAST, Direction.EAST, Direction.NORTH, null}
				}));
		addFloor1(//17
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SCAVENGER, RaidsRoomType.START, RaidsRoomType.BOSS, RaidsRoomType.END},
						{RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.EAST, null},
						{Direction.EAST, Direction.EAST, Direction.NORTH, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.START},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES, RaidsRoomType.END, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, null, null}
				}));
		addFloor1(//18
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.START},
						{RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.END, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, null, null}
				}), new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER},
						{RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.START, RaidsRoomType.PUZZLE}
				}, new Direction[][] {
						{null, Direction.SOUTH, Direction.WEST, Direction.WEST},
						{null, Direction.WEST, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//19
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.START, RaidsRoomType.SCAVENGER},
						{null, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{null, Direction.WEST, Direction.EAST, Direction.SOUTH},
						{null, Direction.NORTH, Direction.WEST, Direction.WEST}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.START, RaidsRoomType.BOSS, RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES},
						{null, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, Direction.EAST, Direction.SOUTH},
						{null, Direction.EAST, Direction.NORTH, null}
				}));
		addFloor1(//20
				new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES, RaidsRoomType.END},
						{RaidsRoomType.START, RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.PUZZLE}
				}, new Direction[][] {
						{null, Direction.EAST, Direction.SOUTH, null},
						{Direction.EAST, Direction.NORTH, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.START},
						{RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES, RaidsRoomType.END, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, null, null}
				}));
		addFloor1(//21
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.START, RaidsRoomType.SCAVENGER, RaidsRoomType.END, RaidsRoomType.PUZZLE},
						{null, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, null, Direction.WEST},
						{null, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.START, RaidsRoomType.SCAVENGER},
						{null, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{null, Direction.WEST, Direction.EAST, Direction.SOUTH},
						{null, Direction.NORTH, Direction.WEST, Direction.WEST}
				}));
		addFloor1(//22
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.PUZZLE, RaidsRoomType.END},
						{RaidsRoomType.START, null, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.SOUTH, null},
						{Direction.NORTH, null, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.END, RaidsRoomType.BOSS, RaidsRoomType.START},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, null}
				}, new Direction[][] {
						{Direction.EAST, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, null}
				}));
		addFloor1(//23
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, RaidsRoomType.END},
						{RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, RaidsRoomType.START, null}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.EAST, null},
						{Direction.NORTH, Direction.WEST, Direction.WEST, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.START},
						{RaidsRoomType.END, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, null}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.SOUTH, Direction.WEST},
						{null, Direction.NORTH, Direction.WEST, null}
				}));
		addFloor1(//24
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.START, RaidsRoomType.SCAVENGER, RaidsRoomType.END, RaidsRoomType.PUZZLE},
						{null, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, null, Direction.WEST},
						{null, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.START, RaidsRoomType.BOSS},
						{null, RaidsRoomType.PUZZLE, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{null, Direction.WEST, Direction.EAST, Direction.SOUTH},
						{null, Direction.NORTH, Direction.WEST, Direction.WEST}
				}));
		addFloor1(//25
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, null},
						{RaidsRoomType.BOSS, RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.START}
				}, new Direction[][] {
						{Direction.SOUTH, Direction.WEST, Direction.WEST, null},
						{Direction.EAST, null, Direction.NORTH, Direction.WEST}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS},
						{null, RaidsRoomType.START, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER}
				}, new Direction[][] {
						{null, Direction.WEST, Direction.WEST, Direction.WEST},
						{null, Direction.EAST, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//26
				new RaidsFloor(new RaidsRoomType[][] {
						{null, RaidsRoomType.END, RaidsRoomType.BOSS, RaidsRoomType.SUPPLIES},
						{RaidsRoomType.START, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.BOSS}
				}, new Direction[][] {
						{null, null, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.START, null, RaidsRoomType.END},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES}
				}, new Direction[][] {
						{null, null, Direction.WEST, Direction.WEST},
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.NORTH}
				}));
		addFloor1(//27
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS},
						{RaidsRoomType.PUZZLE, RaidsRoomType.START, null, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.EAST, Direction.SOUTH},
						{Direction.NORTH, Direction.WEST, null, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, RaidsRoomType.PUZZLE},
						{null, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.START}
				}, new Direction[][] {
						{null, Direction.WEST, Direction.SOUTH, Direction.WEST},
						{null, Direction.NORTH, Direction.WEST, Direction.NORTH}
				}));
		addFloor1(//28
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS, RaidsRoomType.BOSS, null},
						{RaidsRoomType.SCAVENGER, RaidsRoomType.START, RaidsRoomType.PUZZLE, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, Direction.SOUTH, null},
						{Direction.NORTH, Direction.WEST, Direction.EAST, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.END, null, RaidsRoomType.SCAVENGER, RaidsRoomType.BOSS},
						{RaidsRoomType.SUPPLIES, RaidsRoomType.PUZZLE, RaidsRoomType.BOSS, RaidsRoomType.START}
				}, new Direction[][] {
						{null, null, Direction.SOUTH, Direction.WEST},
						{Direction.NORTH, Direction.WEST, Direction.WEST, Direction.NORTH}
				}));
		addFloor1(//29
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.START, RaidsRoomType.SCAVENGER, RaidsRoomType.SUPPLIES, RaidsRoomType.BOSS},
						{null, RaidsRoomType.START, RaidsRoomType.PUZZLE, RaidsRoomType.END}
				}, new Direction[][] {
						{Direction.EAST, Direction.SOUTH, Direction.EAST, Direction.SOUTH},
						{null, Direction.EAST, Direction.NORTH, null}
				}),
				new RaidsFloor(new RaidsRoomType[][] {
						{RaidsRoomType.PUZZLE, RaidsRoomType.SUPPLIES, RaidsRoomType.END, null},
						{RaidsRoomType.BOSS, RaidsRoomType.BOSS, RaidsRoomType.SCAVENGER, RaidsRoomType.START}
				}, new Direction[][] {
						{Direction.EAST, Direction.EAST, null, null},
						{Direction.NORTH, Direction.WEST, Direction.WEST, Direction.WEST}
				}));
	}
	
}
