package com.soulplay.content.raids.map;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.MapTools;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.raids.map.rooms.blank.BlankRoom;
import com.soulplay.content.raids.map.rooms.greatolm.GreatOlmRoom;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.map.ObjectDefVersion;

public abstract class RaidsRoom<DATA extends RaidsRoomData> {

	public static final int ROOM_SIZE = 32;

	private final DATA data;
	private final int roomX, roomY, roomZ;
	private int rotation;

	public RaidsRoom(DATA data, int roomX, int roomY, int roomZ) {
		this.data = data;
		this.roomX = roomX;
		this.roomY = roomY;
		this.roomZ = roomZ;
	}

	public int getRoomAbsX() {
		return roomX << 5;
	}

	public int getRoomAbsY() {
		return roomY << 5;
	}
	
	public int getRoomZ() {
		return roomZ;
	}

	public void loadRoom(int x, int y, RaidsManager manager, Direction rotation) {
		this.rotation = rotation.toInteger();

		final int toChunkX = (manager.startX >> 3) + (x << 2);
		final int toChunkY = (manager.startY >> 3) + (y << 2);

		if (this instanceof BlankRoom) {
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 0, toChunkY + 0, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 0, toChunkY + 1, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 0, toChunkY + 2, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 0, toChunkY + 3, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);

			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 1, toChunkY + 0, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 1, toChunkY + 1, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 1, toChunkY + 2, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 1, toChunkY + 3, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);

			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 2, toChunkY + 0, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 2, toChunkY + 1, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 2, toChunkY + 2, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 2, toChunkY + 3, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);

			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 3, toChunkY + 0, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 3, toChunkY + 1, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 3, toChunkY + 2, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
			MapTools.copyChunk(data.getMapZoneX(), data.getMapZoneY(), data.getMapZoneZ(), toChunkX + 3, toChunkY + 3, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
		} else if (this instanceof GreatOlmRoom) {
			for (int x1 = 0; x1 < 8; x1++) {
				for (int y1 = 0; y1 < 8; y1++) {
					MapTools.copyChunk(data.getMapZoneX() + x1, data.getMapZoneY() + y1, data.getMapZoneZ(), toChunkX + x1, toChunkY + y1, manager.height, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager, ObjectDefVersion.OSRS_DEFS);
				}
			}
		} else {
			MapTools.copy4RatioSquare(data.getMapZoneX(), data.getMapZoneY(), toChunkX, toChunkY, this.rotation, manager.objectManager.getStaticObjects(), manager.clipping, manager.height, manager, data.getMapZoneZ());
		}

		/*int baseX = data.getMapZoneX() << 3;
		int baseY = data.getMapZoneY() << 3;
		int mapSqX = baseX >> 6;
		int mapSqY = baseY >> 6;
		int regionId = (mapSqX << 8) + mapSqY;
		final Region from = RegionManager.forId(regionId);
		Region.load(from, true);

		int startX = mapSqX << 6;
		int startY = mapSqY << 6;

		int offX = baseX - startX >> 3;
		int offY = baseY - startY >> 3;
		DynamicRegion region = activity.region;
		if (roomStartX >= 8 || roomStartY >= 8) {
			if (roomStartX >= 8) {
				roomStartX -= 8;
			}
			if (roomStartY >= 8) {
				roomStartY -= 8;
			}
			region = region.linked[1];
		}

		if (this instanceof BlankRoom) {
			addChunkNull(offX, offY, roomStartX, roomStartY, from, region, rotation);
		} else if (this instanceof GreatOlmRoom) {
			addChunkOlm(offX, offY, roomStartX, roomStartY, from, region, rotation);
			activity.getRooms().add(this);
		} else {
			addChunks(offX, offY, roomStartX, roomStartY, from, region, rotation);
			activity.getRooms().add(this);
		}*/

		onLoad(manager);
	}

	public void onLoad(RaidsManager manager) {
		/* empty */
//		manager.spawnNPC(0, 0, 0, false, this);
//		System.out.println(this+":"+roomX+":"+roomY+":"+roomZ);
	}

	public int getRotation() {
		return rotation;
	}

	/*public void addChunks(int offX, int offY, int roomStartX, int roomStartY, Region from, DynamicRegion region,
			Direction rotation) {
		if (rotation == Direction.NORTH) {
			addChunk(0 + offX, 0 + offY, 0 + roomStartX, 0 + roomStartY, from, region, rotation);// a
			addChunk(0 + offX, 1 + offY, 0 + roomStartX, 1 + roomStartY, from, region, rotation);// b
			addChunk(0 + offX, 2 + offY, 0 + roomStartX, 2 + roomStartY, from, region, rotation);// c
			addChunk(0 + offX, 3 + offY, 0 + roomStartX, 3 + roomStartY, from, region, rotation);// d

			addChunk(1 + offX, 0 + offY, 1 + roomStartX, 0 + roomStartY, from, region, rotation);// e
			addChunk(1 + offX, 1 + offY, 1 + roomStartX, 1 + roomStartY, from, region, rotation);// f
			addChunk(1 + offX, 2 + offY, 1 + roomStartX, 2 + roomStartY, from, region, rotation);// g
			addChunk(1 + offX, 3 + offY, 1 + roomStartX, 3 + roomStartY, from, region, rotation);// h

			addChunk(2 + offX, 0 + offY, 2 + roomStartX, 0 + roomStartY, from, region, rotation);// i
			addChunk(2 + offX, 1 + offY, 2 + roomStartX, 1 + roomStartY, from, region, rotation);// j
			addChunk(2 + offX, 2 + offY, 2 + roomStartX, 2 + roomStartY, from, region, rotation);// k
			addChunk(2 + offX, 3 + offY, 2 + roomStartX, 3 + roomStartY, from, region, rotation);// l

			addChunk(3 + offX, 0 + offY, 3 + roomStartX, 0 + roomStartY, from, region, rotation);// m
			addChunk(3 + offX, 1 + offY, 3 + roomStartX, 1 + roomStartY, from, region, rotation);// n
			addChunk(3 + offX, 2 + offY, 3 + roomStartX, 2 + roomStartY, from, region, rotation);// o
			addChunk(3 + offX, 3 + offY, 3 + roomStartX, 3 + roomStartY, from, region, rotation);// p
		} else if (rotation == Direction.EAST) {
			addChunk(0 + offX, 0 + offY, 0 + roomStartX, 3 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 0 + offY, 0 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 0 + offY, 0 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 0 + offY, 0 + roomStartX, 0 + roomStartY, from, region, rotation);// a

			addChunk(0 + offX, 1 + offY, 1 + roomStartX, 3 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 1 + offY, 1 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 1 + offY, 1 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 1 + offY, 1 + roomStartX, 0 + roomStartY, from, region, rotation);// a

			addChunk(0 + offX, 2 + offY, 2 + roomStartX, 3 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 2 + offY, 2 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 2 + offY, 2 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 2 + offY, 2 + roomStartX, 0 + roomStartY, from, region, rotation);// a

			addChunk(0 + offX, 3 + offY, 3 + roomStartX, 3 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 3 + offY, 3 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 3 + offY, 3 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 3 + offY, 3 + roomStartX, 0 + roomStartY, from, region, rotation);// a
		} else if (rotation == Direction.SOUTH) {
			addChunk(0 + offX, 0 + offY, 3 + roomStartX, 3 + roomStartY, from, region, rotation);// a
			addChunk(0 + offX, 1 + offY, 3 + roomStartX, 2 + roomStartY, from, region, rotation);// b
			addChunk(0 + offX, 2 + offY, 3 + roomStartX, 1 + roomStartY, from, region, rotation);// c
			addChunk(0 + offX, 3 + offY, 3 + roomStartX, 0 + roomStartY, from, region, rotation);// d

			addChunk(1 + offX, 0 + offY, 2 + roomStartX, 3 + roomStartY, from, region, rotation);// e
			addChunk(1 + offX, 1 + offY, 2 + roomStartX, 2 + roomStartY, from, region, rotation);// f
			addChunk(1 + offX, 2 + offY, 2 + roomStartX, 1 + roomStartY, from, region, rotation);// g
			addChunk(1 + offX, 3 + offY, 2 + roomStartX, 0 + roomStartY, from, region, rotation);// h

			addChunk(2 + offX, 0 + offY, 1 + roomStartX, 3 + roomStartY, from, region, rotation);// i
			addChunk(2 + offX, 1 + offY, 1 + roomStartX, 2 + roomStartY, from, region, rotation);// j
			addChunk(2 + offX, 2 + offY, 1 + roomStartX, 1 + roomStartY, from, region, rotation);// k
			addChunk(2 + offX, 3 + offY, 1 + roomStartX, 0 + roomStartY, from, region, rotation);// l

			addChunk(3 + offX, 0 + offY, 0 + roomStartX, 3 + roomStartY, from, region, rotation);// m
			addChunk(3 + offX, 1 + offY, 0 + roomStartX, 2 + roomStartY, from, region, rotation);// n
			addChunk(3 + offX, 2 + offY, 0 + roomStartX, 1 + roomStartY, from, region, rotation);// o
			addChunk(3 + offX, 3 + offY, 0 + roomStartX, 0 + roomStartY, from, region, rotation);// p
		} else if (rotation == Direction.WEST) {
			addChunk(0 + offX, 0 + offY, 3 + roomStartX, 0 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 0 + offY, 3 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 0 + offY, 3 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 0 + offY, 3 + roomStartX, 3 + roomStartY, from, region, rotation);// a

			addChunk(0 + offX, 1 + offY, 2 + roomStartX, 0 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 1 + offY, 2 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 1 + offY, 2 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 1 + offY, 2 + roomStartX, 3 + roomStartY, from, region, rotation);// a

			addChunk(0 + offX, 2 + offY, 1 + roomStartX, 0 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 2 + offY, 1 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 2 + offY, 1 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 2 + offY, 1 + roomStartX, 3 + roomStartY, from, region, rotation);// a

			addChunk(0 + offX, 3 + offY, 0 + roomStartX, 0 + roomStartY, from, region, rotation);// a
			addChunk(1 + offX, 3 + offY, 0 + roomStartX, 1 + roomStartY, from, region, rotation);// a
			addChunk(2 + offX, 3 + offY, 0 + roomStartX, 2 + roomStartY, from, region, rotation);// a
			addChunk(3 + offX, 3 + offY, 0 + roomStartX, 3 + roomStartY, from, region, rotation);// a
		}
	}

	public void addChunkNull(int offX, int offY, int roomStartX, int roomStartY, Region from, DynamicRegion region,
			Direction rotation) {
		addChunk(offX, offY, 0 + roomStartX, 0 + roomStartY, from, region, rotation);// a
		addChunk(offX, offY, 0 + roomStartX, 1 + roomStartY, from, region, rotation);// b
		addChunk(offX, offY, 0 + roomStartX, 2 + roomStartY, from, region, rotation);// c
		addChunk(offX, offY, 0 + roomStartX, 3 + roomStartY, from, region, rotation);// d

		addChunk(offX, offY, 1 + roomStartX, 0 + roomStartY, from, region, rotation);// e
		addChunk(offX, offY, 1 + roomStartX, 1 + roomStartY, from, region, rotation);// f
		addChunk(offX, offY, 1 + roomStartX, 2 + roomStartY, from, region, rotation);// g
		addChunk(offX, offY, 1 + roomStartX, 3 + roomStartY, from, region, rotation);// h

		addChunk(offX, offY, 2 + roomStartX, 0 + roomStartY, from, region, rotation);// i
		addChunk(offX, offY, 2 + roomStartX, 1 + roomStartY, from, region, rotation);// j
		addChunk(offX, offY, 2 + roomStartX, 2 + roomStartY, from, region, rotation);// k
		addChunk(offX, offY, 2 + roomStartX, 3 + roomStartY, from, region, rotation);// l

		addChunk(offX, offY, 3 + roomStartX, 0 + roomStartY, from, region, rotation);// m
		addChunk(offX, offY, 3 + roomStartX, 1 + roomStartY, from, region, rotation);// n
		addChunk(offX, offY, 3 + roomStartX, 2 + roomStartY, from, region, rotation);// o
		addChunk(offX, offY, 3 + roomStartX, 3 + roomStartY, from, region, rotation);// p
	}

	public void addChunkOlm(int offX, int offY, int roomStartX, int roomStartY, Region from, DynamicRegion region,
			Direction rotation) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				addChunk(offX + x, offY + y, roomStartX + x, roomStartY + y, from, region, rotation);
			}
		}
	}*/

	public DATA getData() {
		return data;
	}

}
