package com.soulplay.content.raids.map;

public enum RaidsRoomType {
	SUPPLIES,
	PUZZLE,
	BOSS,
	START,
	END,
	SCAVENGER,
	GREAT_OLM
}
