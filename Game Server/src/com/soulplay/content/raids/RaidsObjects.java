package com.soulplay.content.raids;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.util.Misc;

public class RaidsObjects {

	public static boolean handle1(Client c, int id, int x, int y) {
		if (c.raidsManager == null) {
			return false;
		}

		RaidsManager raids = c.raidsManager;

		GameObject object = raids.getRealObject(id, x, y, c.getZ());
		if (object == null) {
			return true;
		}

		Misc.faceObject(c, x, y, object.getSizeY());

		switch(object.getId()) {
			case 29996:
				RaidsManager.leaveRaidDialog(c);
				return true;
		}
		return true;
	}

}
