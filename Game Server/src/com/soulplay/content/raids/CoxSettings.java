package com.soulplay.content.raids;

public enum CoxSettings {

	GREAT_OLM_ONLY(0)
	
	;
	
	private final int settingId;
	
	private CoxSettings(int id) {
		this.settingId = id;
	}

	public int getSettingId() {
		return settingId;
	}
}
