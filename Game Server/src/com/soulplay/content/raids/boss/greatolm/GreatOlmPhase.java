package com.soulplay.content.raids.boss.greatolm;

public enum GreatOlmPhase {

	ACID("The Great Olm rises with the power of <col=1aff1a>acid</col>."),
	
	FLAME("The Great Olm rises with the power of <col=ff1a1a>flame</col>."),
	
	CRYSTAL("The Great Olm rises with the power of <col=e600e6>crystal</col>."),
	
	OMEGA("");
	
	private final String message;
	
	private GreatOlmPhase(String str) {
		this.message = str;
	}
	
	public String getMessage() {
		return message;
	}
}
