package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.raids.RaidsPoints;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {8990})
public class GreatOlmLeftHandBoss extends Boss {

	private GreatOlmInstanceHandler greatOlm;
	public boolean flinching;
	public int flinchingTicks;
	private final NPC npc;
	public ClawAttack attack;
	
	public GreatOlmLeftHandBoss(NPC boss) {
		super(boss);
		this.npc = boss;
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		
	}

	@Override
	public void process(NPC npc) {
		if (greatOlm == null) { //spawned by admin?
			return;
		}
		if (flinchingTicks > 0 && CombatFormulas.consideredForHit(greatOlm.getRightClawNpc())) {
			flinchingTicks--;
			if (flinchingTicks == 0) {
				unflinch();
			}
		} else if (!CombatFormulas.consideredForHit(greatOlm.getRightClawNpc()) && flinching) {
			unflinch();
		}
		
		if (!npc.isDead() && npc.underAttackBy < 1)
			greatOlm.clawDamageTaken[greatOlm.isWest ? 0 : 1] = 0;
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		//NOTE: this is called twice
		npc.setRetaliates(false);
		npc.isAggressive = false;
		npc.stopAttack = true;
		
		npc.setPauseAnimReset(1000);
		
		npc.getSkills().setStaticLevel(Skills.MAGIC, 175);
		npc.getCombatStats().setMagicAccBonus(60);//using this for now temporarily
		npc.getCombatStats().setMagicDefBonus(50);//using this for now temporarily
		npc.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] = 60;
		npc.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] = 50;
		
		npc.getSkills().setStaticLevel(Skills.RANGED, 250);
		npc.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] = 60;
		npc.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] = 50;
		
		npc.getSkills().setStaticLevel(Skills.DEFENSE, 175);
		
		if (npc.dungVariables[0] != null)
			greatOlm = (GreatOlmInstanceHandler) npc.dungVariables[0];
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 0;
	}

	@Override
	public boolean canMove() {
		return false;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if (hit.getDamage() <= 0 || !source.isPlayer()) {
			return;
		}

		Player player = source.toPlayerFast();
		if (player.raidsManager == null) {
			return;
		}

		if (player.raidsManager.greatOlmManager.leftClawRespawned || greatOlm.getAttributes().getBoolean("claw_heal")) {
			return;
		}
		
		int damage = Math.min(hit.getDamage(), npc.getSkills().getLifepoints());
		
		greatOlm.clawDamageTaken[greatOlm.isWest ? 0 : 1] += damage;

		player.raidsManager.points.increasePoints(player.mySQLIndex, damage * RaidsPoints.POINTS_PER_DMG);
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (greatOlm != null) {
			
			greatOlm.clawDamageTaken[greatOlm.isWest ? 0 : 1] = 0;//TODO: replace this with underAttack in future?
			
			greatOlm.onClawKilled(npc, true);
			
			greatOlm.getLeftObj().animate(Animation.getOsrsAnimId(7370));
			greatOlm.getLeftObj().setTicks(3);
			
			if (greatOlm.penultimate && greatOlm.penultimateCycle == null) {
				greatOlm.startPenultimate(npc);
				return;
			}
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		greatOlm.leftClawHit(state, flinching);
	}
	
	public void assignSpell() {
		if (attack == null || (attack == ClawAttack.SWAP && !greatOlm.penultimate) || attack == ClawAttack.REJUVENATION) {
			attack = ClawAttack.CRYSTAL_BURST;
		} else if (attack == ClawAttack.CRYSTAL_BURST) {
			attack = ClawAttack.LIGHTNING;
		} else if (attack == ClawAttack.LIGHTNING) {
			attack = ClawAttack.SWAP;
		} else if (greatOlm.penultimate && attack == ClawAttack.SWAP) {
			attack = ClawAttack.REJUVENATION;
		}
	}

	public boolean startAttack() {
		//npc.setAttackTimer(12);
		
		if (greatOlm.isRitualising() || flinching) {
			return false;
		}
//		if (!CombatFormulas.consideredForHit(npc)) { // this check is outside the startAttack()
//			return;
//		}
		
//		if (!greatOlm.headHasPossibleTargets()) { // this check is outside the startAttack()
//			return;
//		}
		
		render(attack.getAnimId());
		attack.attack(greatOlm);
		
		//reset animation after casting attack. Rejuvenation should last as long as heal is up though.
		if (attack != ClawAttack.REJUVENATION) {
			Server.getTaskScheduler().schedule(4, () -> {
				if (!greatOlm.isActive) return;
				
				if (!CombatFormulas.consideredForHit(npc)) {
					return;
				}
				
				render(7355);
			});
		}
		return true;
	}
	
	public void flinch() {
		flinching = true;
		render(7360);
		flinchingTicks = 43;//Misc.random(50, 80);
		Server.getTaskScheduler().schedule(2, () -> {
			if (!greatOlm.isActive || !CombatFormulas.consideredForHit(greatOlm.getRightClawNpc())) {
				return;
			}
			
			render(7361);
		});
	}
	
	public void unflinch() {
		flinching = false;
		render(7362);
		Server.getTaskScheduler().schedule(2, () -> {
			if (!greatOlm.isActive) return;
			
			int id = 7355;
			
			if (!CombatFormulas.consideredForHit(greatOlm.getRightClawNpc()))
				id += 10;
			
			flinching = false;
			render(id);
		});
	}
	
	public void render(int id) {
		if (!CombatFormulas.consideredForHit(greatOlm.getRightClawNpc()) && id != 7362) {
			id += 10;
		}
		greatOlm.getLeftObj().animate(Animation.getOsrsAnimId(id));
	}
	
}
