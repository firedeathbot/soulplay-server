package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class CrystalBomb extends GameObject {
	
	private static final int OBJECT_ID = 29766;
	
	private static final int MAX_DISTANCE = 3;

	public CrystalBomb(final GreatOlmInstanceHandler olm, final Location location) {
		super(OBJECT_ID, location, Misc.random(3), 10);
		
		setTicks(8);
		
		Server.getStillGraphicsManager().createGfx(location.getX(), location.getY(), location.getZ(), Graphic.getOsrsId(1368), 0, Misc.serverToClientTick(8));
		
		Server.getTaskScheduler().schedule(8, new Runnable() {
			
			@Override
			public void run() {
				for (Player p : olm.playersInside) {
					if (p == null || !p.isActive) {
						continue;
					}
					
					int dist = (int) p.getCurrentLocation().getDistance(location);
					
					if (dist > MAX_DISTANCE) continue;
					
					int damage = Misc.interpolateSafe(60, 15, 0, MAX_DISTANCE, dist);
					
					p.dealTrueDamage(damage, 0, null, null);
				}
			}
		});
	}

}
