package com.soulplay.content.raids.boss.greatolm;

public enum AttackCycle {
	
	EMPTY,
	HEAD_ATTACK,
	LEFT_CLAW_ATTACK;
	
	public static final AttackCycle[] ATTACK_CYCLE = { HEAD_ATTACK, EMPTY, HEAD_ATTACK, LEFT_CLAW_ATTACK };
	
	public static final int LENGTH = ATTACK_CYCLE.length;
	
	public static AttackCycle getNext(int curCycle) {
		return ATTACK_CYCLE[curCycle % LENGTH];
	}

}
