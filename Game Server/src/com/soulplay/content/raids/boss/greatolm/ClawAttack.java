package com.soulplay.content.raids.boss.greatolm;


import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.util.Misc;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.info.AnimationsData;
import com.soulplay.game.world.GameObject;

public enum ClawAttack {

	CRYSTAL_BURST(7356) {
		@Override
		public void attack(GreatOlmInstanceHandler olm) {
			int ticks = 5;
			for (Player p : olm.players()) {
				if (!p.isActive() || p.raidsManager == null) {
					continue;
				}

				if (olm.getRaids().objectManager.getObject(30033, p.getX(), p.getY(), p.getZ()) != null)
					continue;
				if (p.performingAction)
					continue;
				GameObject crystal = GameObject.createTickObject(30033, p.getCurrentLocation().copyNew(), Misc.random(3), 10, ticks, -1);
				olm.getRaids().objectManager.addObject(crystal);
				
				CycleEventHandler.getSingleton().addEvent(p, true, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						
						container.stop();
						
						if (!olm.isActive) return;
						
						olm.getRaids().objectManager.transformObjectNoClippingChange(crystal, 30034);
						
						GlobalPackets.stillGfx(olm.players(), crystal.getLocation(), Graphic.getOsrsId(1337), 0, Misc.serverToClientTick(1));
						
						for (Player p2 : olm.players()) {
							if (!p2.isActive() || p2.raidsManager == null) {
								continue;
							}

							if (p2.getCurrentLocation().matches(crystal.getLocation())) {
								p2.dealTrueDamage(Misc.random(20, 35), -1, null, olm.getHeadNpc());
								p2.getPA().stepAway();
								p2.startAnimation(AnimationsData.SPOOKED);
								p2.sendMessage("The crystal beneath your feet grows rapidly and shunts you to the side.");
							}
						}
						
					}
				}, 3);
					
			}
		}
	},
	
	LIGHTNING(7358) {

		@Override
		public void attack(GreatOlmInstanceHandler olm) {
			List<Integer> flagged = new ArrayList<>();
			int initialCount = 0;
			int otherCount = 0;
			
			for (int x1 = 0; x1 < 7; x1++) {
				if (Misc.random(5) == 3 || initialCount == 0 && x1 > 3) {
					flagged.add(x1);
					initialCount++;
					throttleLightning(olm, 0, x1);
				}
			}
			
			{
				int x1 = 0;
				while (x1 < 8) {
					if (Misc.randomBoolean(10)) {
						if (flagged.contains(x1)) {
							x1 = (flagged.contains(x1 + 1) ? x1 -1 : x1 + 1);
						}
					} else {
						x1++;
						continue;
					}
					if (!flagged.contains(x1)) {
						int x = x1;
						flagged.add(x);
						otherCount++;
						throttleLightning(olm, 18, x);
					}
					x1++;
				}
			}
			
			if (otherCount == 0) {
				for (int x1 = 0; x1 < 7; x1++) {
					if (!flagged.contains(x1)) {
						throttleLightning(olm, 18, x1);
						break;
					}
				}
			}
		}
		
		public void throttleLightning(GreatOlmInstanceHandler olm, int initial, int x) {
			Server.getTaskScheduler().schedule(new Task(1) {
				
				int y = initial;
				
				@Override
				protected void execute() {
					
					if (!olm.isActive) {
						this.stop();
						return;
					}
					
					Location l = Location.create(x + olm.getRoomRectangle().getSouthWest().getX(), y + olm.getRoomRectangle().getSouthWest().getY());
					
					GlobalPackets.stillGfx(olm.players(), l, Graphic.getOsrsId(1356), 0, 0);

					for (Player p : olm.players()) {
						if (!p.isActive() || p.raidsManager == null) {
							continue;
						}

						if (p.getX() == l.getX() && p.getY() == l.getY()) {
							p.lockMovement(5);
							
							p.dealTrueDamage(Misc.random(15, 30), 0, null, olm.getHeadNpc());
							
							p.startAnimation(AnimationsData.ELECTRICUTED);
							
							p.sendMessage("<col=ff1a1a>You've been electrocuted to the spot!");
							
							CombatPrayer.resetPrayers(p);
							p.sendMessage("You've been injured and can't use protection prayers!");
						}
						
					}
					
					if (y == (initial == 0 ? 18 : 0)) {
						this.stop();
					}
					
					y += (initial == 0) ? 1 : -1;
					
				}
			});
		}
	},
	
	SWAP(7359) {

		@Override
		public void attack(GreatOlmInstanceHandler olm) {

			if (olm.players().size() == 1) {
				pair(olm, olm.players().get(0), null, olm.getProbableTilesClipped(), 0);
			} else {
				List<Player> pairs = new ArrayList<>();
				
				portal: for (int i = 0; i < PORTALS.length; i++) {
					l1: for (Player player : olm.players()) {
						if (pairs.contains(player)) {
							continue l1;
						}
						l2: for (Player p2 : olm.players()) {
							if (player == p2 || pairs.contains(p2)) {
								continue l2;
							}
							pair(olm, player, p2, null, i);
							pair(olm, p2,player, null, i);
							pairs.add(player);
							pairs.add(p2);
							continue portal;
						}
					}
				}
			}
			
		}
		
		private final Graphic[] PORTALS = { Graphic.create(Graphic.getOsrsId(1359)), Graphic.create(Graphic.getOsrsId(1360)), Graphic.create(Graphic.getOsrsId(1361)), Graphic.create(Graphic.getOsrsId(1362)) };
		
		private final Graphic SWAP_GFX = Graphic.create(Graphic.getOsrsId(1039));
		
		private void pair(GreatOlmInstanceHandler olm, Player player1, Player player2, Location loc, int gfxIndex) {
			if (loc != null) { //single player
				player1.sendMessage("The Great Olm had no one to pair you with! The magical power will enact soon...");
				Server.getTaskScheduler().schedule(new Task() {
					
					int count = 0;
					
					@Override
					protected void execute() {
						
						if (!olm.isActive || !player1.isActive() || player1.isDead() || player1.raidsManager == null) {
							this.stop();
							return;
						}
						
						
						if (count % 2 == 0) {
//							if (player1.getCurrentLocation().matches(loc)) { //This is wrong but i added to make it easier
//								this.stop();
//								player1.sendMessage("The teleport attack has no effect!");
//								return;
//							}
							
							player1.startGraphic(PORTALS[gfxIndex]);
							GlobalPackets.stillGfx(olm.players(), loc, PORTALS[gfxIndex].getId(), 10, 0);
						}
						
						if (count == 10) {
							int dist = (int)player1.getCurrentLocation().getDistance(loc);
							
							int damage = Math.min(50, 5*dist);
							
							if (dist == 0) {
								player1.sendMessage("The teleport attack has no effect!");
							} else {
								player1.sendMessage("As you had no pairing... you are taken to a random spot!");
								player1.dealTrueDamage(damage, 0, null, olm.getHeadNpc());
								player1.getPA().movePlayer(loc.getX(), loc.getY(), olm.getRaids().height);
								player1.startGraphic(SWAP_GFX);
							}
							this.stop();
						}
						
						count++;
					}
				});
			}
			if (player2 != null) { // multiple players inside
				player1.sendMessage("You have been paired with <col=ff1a1a>" +
						player2.getNameSmartUp() + "</col> The magical power will enact soon...");
				
				Server.getTaskScheduler().schedule(new Task() {
					
					int count = 0;
					
					@Override
					protected void execute() {
						
						if (!olm.isActive || !player1.isActive() || !player2.isActive() || player1.isDead() || player2.isDead() || player1.raidsManager == null || player2.raidsManager == null) {
							this.stop();
							return;
						}
						
						if (count % 2 == 0) {
							
							if (player1.getCurrentLocation().matches(player2.getCurrentLocation())) {
								this.stop();
								player1.sendMessage("The teleport attack has no effect!");
								return;
							}
							
							player1.startGraphic(PORTALS[gfxIndex]);
						}
						
						if (count == 10) {
							int dist = (int)player1.getCurrentLocation().getDistance(player2.getCurrentLocation());
							
							int damage = 5*dist;
							
							if (dist == 0) {
								player1.sendMessage("The teleport attack has no effect!");
							} else {
								player1.sendMessage("Yourself and " + player2.getNameSmartUp() + " have swapped places!");
								player1.dealTrueDamage(damage, 0, null, olm.getHeadNpc());
								player1.getPA().movePlayer(player2.getX(), player2.getY(), olm.getRaids().height);
								player1.startGraphic(SWAP_GFX);
							}
							this.stop();
						}
						
						count++;
					}
				});

			}
		}
		
	},
	
	REJUVENATION(7357) {
		@Override
		public void attack(GreatOlmInstanceHandler olm) {
			olm.getAttributes().put("claw_heal", true);
			
			Server.getTaskScheduler().schedule(15, ()-> {
				if (!olm.isActive) return;
				
				olm.getAttributes().remove("claw_heal");
				GreatOlmLeftHandBoss leftHand = (GreatOlmLeftHandBoss)olm.getLeftClawNpc().setOrGetBoss();
				leftHand.render(7355);
			});
			
		};
	}
	;
	
	
	private final int animId;
	
	public int getAnimId() {
		return animId;
	}

	private ClawAttack(int anim) {
		this.animId = anim;
	}
	
	public abstract void attack(GreatOlmInstanceHandler olm);
	
}
