package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.player.combat.prayer.ProtectionPrayer;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

public enum Sphere {
	
	MELEE(CombatType.MELEE, Graphic.getOsrsId(1345), Graphic.getOsrsId(1346), ProtectionPrayer.PROTECT_MELEE, "<col=ff0000>The Great Olm fires a sphere of aggression your way.</col>"),
	MAGE(CombatType.MAGIC, Graphic.getOsrsId(1341), Graphic.getOsrsId(1342), ProtectionPrayer.PROTECT_MAGIC, "<col=0000ff>The Great Olm fires a sphere of magical power your way.</col>"),
	RANGE(CombatType.RANGED, Graphic.getOsrsId(1343), Graphic.getOsrsId(1344), ProtectionPrayer.PROTECT_RANGE, "<col=00ff00>The Great Olm fires a sphere of accuracy and dexterity your way.</col>");
	
	public static final Sphere[] values = values();
	private final CombatType combatStyle;
	private final int startGfx;
	private final Graphic endGfx;
	private final ProtectionPrayer prayer;
	private final String message;
	
	Sphere(CombatType combatStyle, int startGfx, int endGfx, ProtectionPrayer pray, String message) {
		this.combatStyle = combatStyle;
		this.startGfx = startGfx;
		this.endGfx = Graphic.create(endGfx, GraphicType.HIGH);
		this.prayer = pray;
		this.message = message;
	}
	
	public CombatType getCombatStyle() {
		return combatStyle;
	}
	
	public int getStartGfx() {
		return startGfx;
	}
	
	public Graphic getEndGfx() {
		return endGfx;
	}
	
	public ProtectionPrayer getPrayer() {
		return prayer;
	}
	
	public String getMessage() {
		return message;
	}
	
	public static Sphere getRandom() {
		return values[Misc.random(values.length-1)];
	}
}
