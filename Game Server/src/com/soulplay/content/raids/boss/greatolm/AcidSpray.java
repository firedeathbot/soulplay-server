package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class AcidSpray extends GameObject {
	
	private int acidCycle = 0;
	private final int aliveTicksJustInCase;
	
	public AcidSpray(GreatOlmInstanceHandler olm, Location location, boolean dmgOnSpawn) {
		super(30032, location, Misc.random(3), 10);
		
		aliveTicksJustInCase = Misc.random(12, 14);
		setTicks(aliveTicksJustInCase);

		if (dmgOnSpawn) {
			for (Player player : olm.playersInside) {
				if (player.getCurrentLocation().equals(getLocation()) && Misc.random(10) > 6) {
					dealPoisonDamage(player);
				}
			}
		}
		
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!isActive() || acidCycle > aliveTicksJustInCase) {
					olm.getAcidPools().remove(location);
					container.stop();
					return;
				}
				
				//deal damage to players
				if ((!dmgOnSpawn && acidCycle > 0) || dmgOnSpawn) {
					for (Player player : olm.playersInside) {
						if (player.getCurrentLocation().equals(location)) {
							dealPoisonDamage(player);
						}
					}
				}
				
				acidCycle++;
			}
		}, 1);
	}
	
	public void dealPoisonDamage(Player p) {
		p.dealDamage(new Hit(Misc.random(3, 6), 2, -1));
	}

	
}
