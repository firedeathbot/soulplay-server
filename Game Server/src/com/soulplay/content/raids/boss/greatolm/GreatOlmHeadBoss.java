package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.raids.RaidsPoints;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {9000})
public class GreatOlmHeadBoss extends Boss {
	
	private GreatOlmInstanceHandler greatOlm;

	public GreatOlmHeadBoss(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		
	}

	@Override
	public void process(NPC npc) {
		if (greatOlm != null) {
			greatOlm.handleTickActions();
			
			if (npc.getAttackTimer() <= 0 && CombatFormulas.consideredForHit(npc)) {
				greatOlm.doSwing();
				npc.setAttackTimer(4);
			}
		}
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		npc.setRetaliates(false);
		npc.isAggressive = false;
		npc.stopAttack = true;
		//NOTE: this is called twice
		if (npc.dungVariables[0] != null)
			greatOlm = (GreatOlmInstanceHandler) npc.dungVariables[0];
		
//		npc.tempRangeDef = 0;
		
		npc.getSkills().setStaticLevel(Skills.MAGIC, 250);
		npc.getCombatStats().setMagicAccBonus(60);//using this for now temporarily
		npc.getCombatStats().setMagicDefBonus(200);//using this for now temporarily
		npc.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] = 60;
		npc.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] = 200;
		
		npc.getSkills().setStaticLevel(Skills.RANGED, 250);
		npc.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] = 60;
		npc.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] = 50;
		
		npc.getSkills().setStaticLevel(Skills.DEFENSE, 150);
		
		npc.prayerReductionMelee = npc.prayerReductionMagic = npc.prayerReductionRanged = 0.15;
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 0;
	}

	@Override
	public boolean canMove() {
		return false;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if (greatOlm == null) {
			return;
		}

		if (!greatOlm.isInOmegaStage()) {
			//prevent from one shotting the head
			if (hit.getDamage() > 70) {
				hit.setDamage(70);
			}
		} else {
			if (hit.getDamage() <= 0 || !source.isPlayer()) {
				return;
			}

			Player player = source.toPlayerFast();
			if (player.raidsManager == null) {
				return;
			}
			
			int damage = Math.min(hit.getDamage(), npc.getSkills().getLifepoints());

			player.raidsManager.points.increasePoints(player.mySQLIndex, damage * RaidsPoints.POINTS_PER_DMG);
		}
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (greatOlm != null && greatOlm.onLastStage())
			greatOlm.applyFinalDeath();
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
