package com.soulplay.content.raids.boss.greatolm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.combat.data.CombatFormulas;
import com.soulplay.content.npcs.combat.NpcHitPlayer;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.ProtectionPrayer;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.raids.CoxHiscores;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.raids.RaidsRewards;
import com.soulplay.content.raids.map.RaidsRoom;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.save.HighscoresSaveNew.HiscoreSlot;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;
import com.soulplay.util.TempVar;

public class GreatOlmInstanceHandler {
	
	private static final int HEAD_OBJ_SPAWN = 29880;
	private static final int HEAD_OBJ_ALIVE = 29881;
	private static final int HEAD_OBJ_DEAD = 29882;
	
	private static final int LEFT_HAND_OBJ_SPAWN = 29883;
	private static final int LEFT_HAND_OBJ_ALIVE = 29884;
	private static final int LEFT_HAND_OBJ_DEAD = 29885;
	
	private static final int RIGHT_HAND_OBJ_SPAWN = 29886;
	private static final int RIGHT_HAND_OBJ_ALIVE = 29887;
	private static final int RIGHT_HAND_OBJ_DEAD = 29888;
	
	public static final int HEAD_NPC_ID = 9000;
	public static final int RIGHT_CLAW_NPC_ID = 8991;
	public static final int LEFT_CLAW_NPC_ID = 8990;
	
	private static String OLM_BURN_VAR = "olm_burn";
	
	private static final int PROJ_START_DELAY = Misc.serverToClientTick(1);
	
	private List<GreatOlmPhase> phasesLeft = new ArrayList<>(3);
	private List<GreatOlmPhase> phasesUnlocked = new ArrayList<>();
	
	private Map<Location, AcidSpray> acidPools = new HashMap<>();
	
	public Map<Location, AcidSpray> getAcidPools() {
		return acidPools;
	}
	
	RevivingClaw reviveClaw = null;
	
	public boolean isWest, bombs, startFlinching;
	
	public boolean isActive = true;
	
	public boolean spawnedOlm = false, leftClawRespawned, rightClawRespawned;
	private int face = -1;
	private boolean ritualising, omega, switching, turnedFaceLastAttack, hasPossibleTargetsThisTick, skippedAnAttack;
	private int lastHeal, render, stage, lastStage;
	public boolean penultimate;
	public CycleEventContainer penultimateCycle = null;
	public int attackCycles = -1;
	
	private AttackCycle nextAttack = AttackCycle.HEAD_ATTACK;
	
	private boolean[] nextSpecialAttack = new boolean[3];
	
	private int nextSpecialAttackTick = 6;
	
	private static final int SPEC_DELAYS = 68;
	private TickTimer acidDripTimer = new TickTimer();
	private TickTimer fallingCrystalsTimer = new TickTimer();
	private TickTimer burnTimer = new TickTimer();
	
	private int headHP = 800;
	private int clawHP = 600;
	
	private OlmAttack attack;
	
	private GameObject crystal;
	
	public Location chestLocation;
	public Location[][] projStart = new Location[2][3];
	
	public int[] clawDamageTaken = new int[2];
	
	private TempVar vars = new TempVar();
	
	public Object getAttribute(final String key) {
		return vars == null ? null : vars.get(key);
	}
	
	public TempVar getAttributes() {
		return this.vars;
	}
	
	public List<Player> playersInside = new ArrayList<>();
	
	private GameObject headObj;
	private GameObject leftHandObj;
	private GameObject rightHandObj;
	
	public GameObject getHeadObj() {
		return headObj;
	}
	
	public GameObject getLeftObj() {
		return leftHandObj;
	}
	
	public GameObject getRightObj() {
		return rightHandObj;
	}
	
	private NPC headNpc;
	private NPC leftClawNpc;
	private NPC rightClawNpc;
	
	public NPC getHeadNpc() {
		return headNpc;
	}
	
	public NPC getRightClawNpc() {
		return rightClawNpc;
	}
	
	public NPC getLeftClawNpc() {
		return leftClawNpc;
	}
	
	private Rectangle[][] RADIUS;
	
	private Rectangle WHOLE_ROOM_RECTANGLE;
	private Rectangle WHOLE_ROOM_SMALLER_RECTANGLE; // rectange that only includes the OLM object areas
	
	private Location roomLoc = Location.create(0, 0);
	
	private RaidsRoom room;
	
	public void setRoom(RaidsRoom room) {
		this.room = room;
		
		configureRoom();
	}
	
	private void configureRoom() {
		
		roomLoc = Location.create(raids.getRoomX(getRoom()), raids.getRoomY(getRoom()), raids.height);
		
		int westX = roomLoc.getX() + 28;
		int eastX = roomLoc.getX() + 37;
		
		Rectangle upper = new Rectangle(Location.create(westX, roomLoc.getY()+44), Location.create(eastX, roomLoc.getY()+52));
		Rectangle middle = new Rectangle(Location.create(westX, roomLoc.getY()+39), Location.create(eastX, roomLoc.getY()+48));
		Rectangle westMiddle = new Rectangle(Location.create(westX, roomLoc.getY()+40), Location.create(eastX, roomLoc.getY()+49));
		Rectangle lower = new Rectangle(Location.create(westX, roomLoc.getY()+34), Location.create(eastX, roomLoc.getY()+44));
		
		RADIUS = new Rectangle[][] {
			
			//west side
			{
				upper, //left claw area
				westMiddle,
				lower, //right claw area
			},
			
			//east side
			{
				upper, // left claw area
				middle,
				lower, // right claw area
			}
			
		};
		
		WHOLE_ROOM_RECTANGLE = new Rectangle(Location.create(westX, roomLoc.getY()+34), Location.create(eastX, roomLoc.getY()+52));
		
		WHOLE_ROOM_SMALLER_RECTANGLE = new Rectangle(Location.create(westX, roomLoc.getY()+37), Location.create(eastX, roomLoc.getY()+51));
		
		//get location of the crystal to remove
		crystal = raids.objectManager.getObject(roomLoc.getX()+32, roomLoc.getY()+53, raids.height);
		chestLocation = Location.create(roomLoc.getX()+33, roomLoc.getY()+55, raids.height);
		
		projStart[0][0] = Location.create(roomLoc.getX()+26, roomLoc.getY()+45, raids.height);
		projStart[0][1] = Location.create(roomLoc.getX()+26, roomLoc.getY()+44, raids.height); //center
		projStart[0][2] = Location.create(roomLoc.getX()+26, roomLoc.getY()+43, raids.height); //right?
		
		//east
		projStart[1][0] = Location.create(roomLoc.getX()+39, roomLoc.getY()+45, raids.height);
		projStart[1][1] = Location.create(roomLoc.getX()+39, roomLoc.getY()+44, raids.height); //center
		projStart[1][2] = Location.create(roomLoc.getX()+39, roomLoc.getY()+43, raids.height);

		//randomize which spot he spawns at on spawn
		isWest = Misc.random(1) == 0;
		
		phasesLeft.add(GreatOlmPhase.CRYSTAL);
		phasesLeft.add(GreatOlmPhase.ACID);
		phasesLeft.add(GreatOlmPhase.FLAME);
		Collections.shuffle(phasesLeft);
		
		spawnOlmObject();
		
	}
	
	public RaidsRoom getRoom() {
		return this.room;
	}
	
	private void spawnOlmObject() {
		
		int z = raids.height;//getRoom().getRoomZ();
		
		int x = this.raids.getRoomX(this.getRoom()) + (isWest ? 20 : 38);
		int headY = this.raids.getRoomY(this.getRoom()) + 42;
		int leftY = this.raids.getRoomY(this.getRoom()) + (isWest ? 47 : 37);
		int rightY = this.raids.getRoomY(this.getRoom()) + (isWest ? 37 : 47);
		int orientation = isWest ? 3 : 1;
		
		Location headLoc = Location.create(x, headY, z);
		Location leftLoc = Location.create(x, leftY, z);
		Location rightLoc = Location.create(x, rightY, z);
		
		headObj = GameObject.createTickObject(HEAD_OBJ_SPAWN, headLoc, orientation, 10, Integer.MAX_VALUE, HEAD_OBJ_DEAD);
		raids.objectManager.addObject(headObj);
		
		leftHandObj = GameObject.createTickObject(LEFT_HAND_OBJ_SPAWN, leftLoc, orientation, 10, Integer.MAX_VALUE, LEFT_HAND_OBJ_DEAD);
		raids.objectManager.addObject(leftHandObj);
		
		rightHandObj = GameObject.createTickObject(RIGHT_HAND_OBJ_SPAWN, rightLoc, orientation, 10, Integer.MAX_VALUE, RIGHT_HAND_OBJ_DEAD);
		raids.objectManager.addObject(rightHandObj);
		
	}
	
	private void wakeOlm() {

		Server.getTaskScheduler().schedule(new Task() {
			int ticks = 0;
			@Override
			protected void execute() {

				if (!isActive) {
					this.stop();
					return;
				}
				
				switch (ticks) {
				case 6:
					phasesUnlocked.clear();
					if (onLastStage()) {
						phasesUnlocked.addAll(phasesLeft);
					} else {
						GreatOlmPhase thisPhase = phasesLeft.get(stage % phasesLeft.size());
						phasesUnlocked.add(thisPhase);
						raids.party.executeForMember(player -> {
							player.sendMessage(thisPhase.getMessage());
						});
					}
					
					break;
					
				case 8:
					headObj.animate(Animation.getOsrsAnimId(7335));
					leftHandObj.animate(Animation.getOsrsAnimId(7354));
					rightHandObj.animate(Animation.getOsrsAnimId(7350));
					break;
					
				case 13:
					
					spawnNpcs();
					
					raids.objectManager.transformObjectNoClippingChange(headObj, HEAD_OBJ_ALIVE);
					raids.objectManager.transformObjectNoClippingChange(leftHandObj, LEFT_HAND_OBJ_ALIVE);
					raids.objectManager.transformObjectNoClippingChange(rightHandObj, RIGHT_HAND_OBJ_ALIVE);
					break;
					
				case 15:
					leftHandObj.animate(Animation.getOsrsAnimId(7355));
					rightHandObj.animate(Animation.getOsrsAnimId(7351));
					
					switching = false;
					setRitualising(false);
					face = 1;
					checkFace();
					
					createNextSpecialAttackTick();
					
					leftClawNpc.setAttackTimer(Misc.random(10, 11));
					break;
				}
				
				ticks++;
				
				if (ticks >= 16)
					this.stop();
				
			}
		});
		
	}
	
	public void respawnClaw(boolean rightClaw) {
		
		int z = raids.height;//getRoom().getRoomZ();
		int x = this.raids.getRoomX(this.getRoom()) + (isWest ? 20 : 38);
		int orientation = isWest ? 3 : 1;
		
		if (!rightClaw) {

			int leftY = this.raids.getRoomY(this.getRoom()) + (isWest ? 47 : 37);
			
			Location leftLoc = Location.create(x, leftY, z);
			
			leftHandObj = GameObject.createTickObject(LEFT_HAND_OBJ_SPAWN, leftLoc, orientation, 10, Integer.MAX_VALUE, LEFT_HAND_OBJ_DEAD);
			raids.objectManager.addObject(leftHandObj);

			spawnLeftClaw();
		} else {
			
			int rightY = this.raids.getRoomY(this.getRoom()) + (isWest ? 37 : 47);
			
			Location rightLoc = Location.create(x, rightY, z);
			
			rightHandObj = GameObject.createTickObject(RIGHT_HAND_OBJ_SPAWN, rightLoc, orientation, 10, Integer.MAX_VALUE, RIGHT_HAND_OBJ_DEAD);
			raids.objectManager.addObject(rightHandObj);

			spawnRightClaw();
		}
		
		Server.getTaskScheduler().schedule(new Task() {
			int ticks = 0;
			@Override
			protected void execute() {

				if (!isActive) {
					this.stop();
					return;
				}
				
				switch (ticks) {
				case 0:
					if (!rightClaw) {
						leftHandObj.animate(Animation.getOsrsAnimId(7354));
					} else {
						rightHandObj.animate(Animation.getOsrsAnimId(7350));
					}
					break;
					
				case 5:
					
					if (!rightClaw) {
						try {
							raids.objectManager.transformObjectNoClippingChange(leftHandObj, LEFT_HAND_OBJ_ALIVE);
						} catch (Exception e) {
							System.out.println("raids null:"+ (raids == null) + " objManager null:" + (raids.objectManager == null) + " leftHandObj null:"+ (leftHandObj == null));
							e.printStackTrace();
						}
						leftClawRespawned = true;
					} else {
						try {
							raids.objectManager.transformObjectNoClippingChange(rightHandObj, RIGHT_HAND_OBJ_ALIVE);
						} catch (Exception e) {
							System.out.println("raids null:"+ (raids == null) + " objManager null:" + (raids.objectManager == null) + " rightHandObj null:"+ (rightHandObj == null));
							e.printStackTrace();
						}
						rightClawRespawned = true;
					}
					
					break;
					
				case 7:
					if (!rightClaw) {
						leftHandObj.animate(Animation.getOsrsAnimId(7355));
						leftClawNpc.setAttackTimer(getHeadNpc().getAttackTimer()+12); // to sync with the head attacks
					} else
						rightHandObj.animate(Animation.getOsrsAnimId(7351));
					
					break;
				}
				
				ticks++;
				
				if (ticks >= 7)
					this.stop();
				
			}
		});
	}
	
	public void passGate(Player player, GameObject object, RaidsManager raid) {
		if (!player.getStopWatch().checkThenStart(1)) {
			return;
		}
		walkThroughBarrier(player, object);
		
		if (spawnedOlm)
			return;
		spawnedOlm = true;
		
		//spawn olm
		wakeOlm();
	}
	
	private void walkThroughBarrier(Player player, GameObject object) {
		
		playersInside.add(player);

		Direction dir = Direction.NORTH;
		
		ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX(), dir.getStepY()*2, 2, 0, dir);
		player.setForceMovement(mask);
		player.startAnimation(player.getMovement().getWalkAnim());
		
		player.sendMessage("As you pass through the barrier, a sense of dread washes over you.");
		
	}
	
	private final RaidsManager raids;
	
	public GreatOlmInstanceHandler(RaidsManager raid) {
		this.raids = raid;
	}

	public RaidsManager getRaids() {
		return raids;
	}
	
	public void configureOlmHP() {

		int partySize = raids.party.getMembers().size();
		
		int headBaseHP = 800; // base hp is 800
		int clawBaseHP = 600; // 600 is base hp
		
		headHP = headBaseHP * partySize;
		clawHP = clawBaseHP * partySize;
		
		
		//raid max stage
		lastStage = partySize > 7 ? 3 : 2;
	}
	
	private void spawnNpcs() {
		
		setRitualising(true);
		face = 1;
		attack = Misc.randomBoolean(1) ? OlmAttack.DEFAULT_MAGIC : OlmAttack.DEFAULT_RANGED;
		
		int faceX = isWest ? -5 : 5;
		
		Location npcLoc = createHeadNpcLoc();
		headNpc = raids.spawnNPC(9000, npcLoc.getX(), npcLoc.getY(), false, getRoom());
		headNpc.turnNpc(headNpc.getX() + faceX, headNpc.getY() + 2);
		headNpc.dungVariables[0] = this;
		headNpc.setOrGetBoss().onSpawn(headNpc); // call this again to apply reference to greatolminstancehandler
		
		//set stats for npc
		headNpc.getSkills().setStaticLevel(Skills.HITPOINTS, headHP);
		
		spawnLeftClaw();

		spawnRightClaw();
		
		attackCycles = -1;
		
		//randomize which special attack to start with
		for (int i = 0; i < nextSpecialAttack.length; i++) {
			nextSpecialAttack[i] = Misc.randomBoolean(1);
		}
	}
	
	public void spawnRightClaw() {
		
		int faceX = isWest ? 5 : -5;
		
		Location npcLoc = createRightClawNpcLoc();
		rightClawNpc = raids.spawnNPC(RIGHT_CLAW_NPC_ID, npcLoc.getX(), npcLoc.getY(), false, getRoom());
		rightClawNpc.turnNpc(rightClawNpc.getX() + faceX, rightClawNpc.getY() + 2);
		rightClawNpc.dungVariables[0] = this;
		rightClawNpc.setOrGetBoss().onSpawn(rightClawNpc); // call this again to apply reference to greatolminstancehandler
		
		//set stats for npc
		rightClawNpc.getSkills().setStaticLevel(Skills.HITPOINTS, clawHP);
	}
	
	public void spawnLeftClaw() {
		
		int faceX = isWest ? -5 : -5;
		
		Location npcLoc = createLeftClawNpcLoc();
		leftClawNpc = raids.spawnNPC(LEFT_CLAW_NPC_ID, npcLoc.getX(), npcLoc.getY(), false, getRoom());
		leftClawNpc.turnNpc(leftClawNpc.getX() + faceX, leftClawNpc.getY() + 2);
		leftClawNpc.dungVariables[0] = this;
		leftClawNpc.setOrGetBoss().onSpawn(leftClawNpc); // call this again to apply reference to greatolminstancehandler
		
		//set stats for npc
		leftClawNpc.getSkills().setStaticLevel(Skills.HITPOINTS, clawHP);
		
		leftClawNpc.setAttackTimer(6);
	}
	
	private Location createHeadNpcLoc() {
		if (isWest)
			return Location.create(22, 42);
		else
			return Location.create(39, 42);
	}
	
	private Location createLeftClawNpcLoc() {
		if (isWest)
			return Location.create(23, 47);
		else
			return Location.create(38, 37);
	}
	
	private Location createRightClawNpcLoc() {
		if (isWest)
			return Location.create(22, 38);
		else
			return Location.create(39, 46);
	}
	

	
	public void render(int anim) {
		headObj.animate(omegaAlt(anim));
	}
	
	public void render() {
		if (!isWest) {
			this.render = face == 1 ? 27336 : face == 0 ? 27337 : 27338;
		} else {
			this.render = face == 1 ? 27336 : face == 0 ? 27338 : 27337;
		}
	}
	
	private int omegaAlt(int anim) {
		if (penultimate) {
			switch (anim) {
				case 27336:
					return 27374;
				case 27337:
					return 27376;
				case 27338:
					return 27375;
				case 27347:
					return 27372;
				case 27345:
					return 27371;
				case 27346:
					return 27373;
			}
		}
		return anim;
	}
	
	public void face(int face) {
		if (this.face == face) {
			setRitualising(false);
			return;
		}
		
		turnedFaceLastAttack = true;
		
		setRitualising(true);
		switch (this.face) {
			case 0:
				render(face == 1 ? !isWest ? 27340 : 27342 : !isWest ? 27344 : 27343);
				break;
			case 1:
				render(face == 0 ? !isWest ? 27339 : 27341 : !isWest ? 27341 : 27339);
				break;
			case 2:
				render(face == 1 ? !isWest ? 27342 : 27340 : !isWest ? 27343 : 27344);
				break;
		}
		this.face = face;
		render();
		getHeadNpc().setAttackTimer(4);
		
		if (nextAttack == AttackCycle.HEAD_ATTACK)
			skippedAnAttack = true;
		
		Server.getTaskScheduler().schedule(2, () -> {
			if (!isActive) return;

			render(render);
			setRitualising(false);
		});
	}
	
	private void checkFace() {
		if (getHeadNpc().getSkills().getLifepoints() == 0 || switching) {
			return;
		}
		
		int face = -1;
		
		int count0 = count(0);
		int count1 = count(1);
		int count2 = count(2);
		
		//if left claw took most damage, focus looking left
		if (clawDamageTaken[0] != 0 && clawDamageTaken[0] > clawDamageTaken[1]) {
			if (this.face == 1 && (count0 >= count1 || count2 > count1)) {
				face = count0 < count2 ? 2 : 0;
			}
			if (this.face == 0 && (count1 > count0 || count2 > count0)) {
				face = count1 < count2 ? 2 : 1;
			}
			if (this.face == 2 && (count0 > count2 || count1 >= count2)) {
				face = count1 > count0 ? 1 : 0;
			}
			//clawDamageTaken[0] = clawDamageTaken[1] = 0; //reset here or inside the if checks?
			//getHeadNpc().forceChat("here1 " + face + " curFace:"+this.face + "   count0:"+count0+"  count1:"+count1+"  count2:"+count2);
		}
		//if right claw took most damage, focus looking right
		else if (clawDamageTaken[1] != 0 && clawDamageTaken[1] > clawDamageTaken[0]) {
			if (this.face == 0 && (count1 >= count0 || count2 > count0)) {
				face = count2 < count1 ? 1 : 2;
			}
			if (this.face == 1 && (count0 > count1 || count2 >= count1)) {
				face = count0 < count2 ? 2 : 0;
			}
			if (this.face == 2 && (count0 > count2 || count1 > count2)) {
				face = count1 < count0 ? 0 : 1;
			}
			//clawDamageTaken[0] = clawDamageTaken[1] = 0; //reset here or inside the if checks?
			//getHeadNpc().forceChat("here2 " + face + " curFace:"+this.face + "   count0:"+count0+"  count1:"+count1+"  count2:"+count2);
		}
		
		else {

			//getHeadNpc().forceChat("here3");
			if (this.face == 0 && (count1 > count0 || count2 > count0)) {
				face = count1 < count2 ? 2 : 1;
			}
			if (this.face == 1 && (count0 > count1 || count2 > count1)) {
				face = count0 < count2 ? 2 : 0;
			}
			if (this.face == 2 && (count0 > count2 || count1 > count2)) {
				face = count1 < count0 ? 0 : 1;
			}
		
		}
		
		if (face > -1) {
			face(face);
		}
	}
	
	private int count(int side) {
		int count = 0;
		Rectangle area = RADIUS[isWest ? 0 : 1][side];
		int players = area.getNumberOfPlayers(playersInside);
		if (players > count || (players >= count && side == 1)) {
			count = players;
		}
		return count;
	}
	
	public void checkSwitch() {
		if (Misc.randomBoolean(3))
			attack = attack == OlmAttack.DEFAULT_RANGED ? OlmAttack.DEFAULT_MAGIC : OlmAttack.DEFAULT_RANGED;
	}
	
	public int getAttackAnimation(int face) {
		if (!isWest) {
			return face == 0 ? 27346 : face == 1 ? 27345 : 27347;
		} else {
			return face == 0 ? 27347 : face == 1 ? 27345 : 27346;
		}
	}
	
	private static final Graphic GLOW_RED = Graphic.create(Graphic.getOsrsId(246));
	
	private void fallingCrystalsSpell() {
		
		startAttackAnimation();
		
		final int projEndDelay = Misc.serverToClientTick(2);
		
		Player target = Misc.random(getPossibleTargets());
		
		if (target != null) {
			
			for (Player player : players()) {
				player.sendMessage("The Great Olm sounds a cry...");
			}
			
			render(getAttackAnimation(face));
			
			target.sendMessage("<col=ff1a1a>The Great Olm has chosen you as its target - watch out!");
			
			Server.getTaskScheduler().schedule(new Task(2, true) {
				
				int crystalCount = 0;
				
				@Override
				protected void execute() {
					if (!isActive || !target.isActive() || target.isDead() || target.raidsManager == null) {
						this.stop();
						return;
					}
					
					target.startGraphic(GLOW_RED);
					final Location targetLocation = target.getCurrentLocation().copyNew();
					Location start = targetLocation.transform(-1, 0, 0);
					GlobalPackets.stillGfx(playersInside, targetLocation, Graphic.getOsrsId(1353), 0, projEndDelay);
					
					Projectile proj = new Projectile(Graphic.getOsrsId(1352), start, targetLocation);
					proj.setStartHeight(255);
					proj.setEndHeight(10);
					proj.setStartDelay(0);
					proj.setEndDelay(projEndDelay);
					proj.setSlope(2);
					
					GlobalPackets.createProjectile(proj);
					
					Server.getTaskScheduler().schedule(2, () -> {
						if (!isActive) return;

						for (Player player : players()) {
							if (player.getCurrentLocation().equals(targetLocation)) {
								player.dealTrueDamage(Misc.random(10, 25), 0, null, getHeadNpc());
							}
						}
					});
					
					if (crystalCount >= 10) { // counted 11 in videos so set to 10
						this.stop();
					}
					
					crystalCount++;
					
				}
			});
			
		}
	}
	
	private void crystalBombSpell() {
		//animate object
		startAttackAnimation();
		
		int targetCount = 0;
		int maxSize = playersInside.size() > 7 ? 3 : 2;
		
		while (targetCount < maxSize) {
			final Location target = getRandomLocationForFace();
			
			if (raids.clipping.getClipping(target.getX(), target.getY(), raids.height) != 0) {
				continue;
			}

			targetCount++;
			
			Location start = createStartFromHead(target);
			
			int distance = (int)target.getDistance(start);
			
			Projectile proj = new Projectile(Graphic.getOsrsId(1357), start, target);
			proj.setStartDelay(PROJ_START_DELAY);
			proj.setEndDelay(getProjectileEndDelay(proj.getStartDelay(), distance));
			proj.setSlope(2);
			proj.setStartHeight(45);
			proj.setEndHeight(28);
			
			GlobalPackets.createProjectile(proj);
			
			Server.getTaskScheduler().schedule(Misc.clientToServerTick(proj.getEndDelay()), ()-> {
				if (!isActive) return;
				
				GameObject bomb = new CrystalBomb(raids.greatOlmManager, target);
				raids.objectManager.addObject(bomb);
			});
		}
	}
	
	private static final int FALLING_CRYSTAL_EXPLODED = Graphic.getOsrsId(1358);

	public void fallingCrystalMiniBombs(Location p) {
		final Location target = p != null ? p.copyNew() : getProbableTiles();
		
		GlobalPackets.stillGfx(playersInside, target, Graphic.getOsrsId(1448), 10, 0);
		
		Projectile proj = new Projectile(Graphic.getOsrsId(1357), target.transform(Direction.getRandom()), target);
		proj.setStartHeight(255);
		proj.setEndHeight(0);
		proj.setStartDelay(0);
		proj.setEndDelay(140);
		proj.setSlope(0);
		GlobalPackets.createProjectile(proj);
		
		GlobalPackets.stillGfx(playersInside, target, FALLING_CRYSTAL_EXPLODED, 0, Misc.serverToClientTick(4));
		
		Server.getTaskScheduler().schedule(5, () -> {
			if (!isActive) return;
			
			for (Player player : playersInside) {
				Location location = player.getCurrentLocation();
				if (location.isWithinDeltaDistance(target, 1)) {
					int damage = Misc.random(5, 13);
					if (Location.equals(location, target)) {
						damage *= 2;
					}
					player.dealTrueDamage(damage, 0, null, getHeadNpc());
				}
			}
		});
	}
	
	private Location getProbableTiles() {
		Location ran = WHOLE_ROOM_SMALLER_RECTANGLE.getRandomTile();
		ran.setZ(raids.height);
		return ran;
	}
	
	public Location getProbableTilesClipped() {
		Location ran = WHOLE_ROOM_SMALLER_RECTANGLE.getRandomTile();
		ran.setZ(raids.height);
//		while (raids.clipping.getClipping(ran.getX(), ran.getY(), ran.getZ()) == 0)
//			return ran;
		return ran;
	}
	
	public Rectangle getRoomRectangle() {
		return WHOLE_ROOM_RECTANGLE;
	}
	
	private Location getRandomLocationForFace() {
		Location loc = RADIUS[isWest ? 0 : 1][face].getRandomTile();
		loc.setZ(raids.height);//cheaphax raids bomb
		return loc;
	}
	
	public void throttleFarcast(Player victim, CombatType combatStyle, int damage, int projectile) {

		startAttackAnimation();
		
		Location start = createStartFromHead(victim.getCurrentLocation());
		
		int distance = (int)victim.getCurrentLocation().getDistance(start);
		
		Projectile proj = new Projectile(Graphic.getOsrsId(projectile), start, victim.getCurrentLocation());
		proj.setStartHeight(50);
		proj.setEndHeight(28);
		proj.setStartDelay(PROJ_START_DELAY);
		proj.setEndDelay(getProjectileEndDelay(proj.getStartDelay(), distance));
		proj.setSlope(2);
		proj.setLockon(victim.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		
		int hitDelay = Misc.clientToServerTick(proj.getEndDelay())+1;
		
		if (combatStyle == CombatType.MAGIC) {
			NpcHitPlayer.dealMagicDamage(getHeadNpc(), victim, damage, hitDelay, null);
		} else {
			NpcHitPlayer.dealRangeDamage(getHeadNpc(), victim, damage, hitDelay, null);
		}
		
	}
	
	private void shootBurnSpell() {
		Player target = Misc.random(getPossibleTargets());
		if (target == null) {
			return;
		}
		
		startAttackAnimation();
		
		Location start = createStartFromHead(target.getCurrentLocation());
		
		int distance = (int)target.getCurrentLocation().getDistance(start);
		
		Projectile proj = new Projectile(Graphic.getOsrsId(1349), start, target.getCurrentLocation());
		proj.setStartDelay(PROJ_START_DELAY);
		proj.setEndDelay(getProjectileEndDelay(proj.getStartDelay(), distance));
		proj.setSlope(0);
		proj.setStartHeight(50);
		proj.setEndHeight(28);
		proj.setLockon(target.getProjectileLockon());
		
		GlobalPackets.createProjectile(proj);
		
		Server.getTaskScheduler().schedule(Misc.clientToServerTick(proj.getEndDelay())-1, () -> {
			if (!isActive) return;
			
			if (target.isActive())
				burn(target, true);
		});
	}
	
	private void burn(Player target, boolean message) {
		int lastBurnTicks = target.getTempVar().getInt(OLM_BURN_VAR, -1);
		if (lastBurnTicks > Server.getTotalTicks()) {
			return;
		}
		if (message) {
			target.forceChat("Burn with me!");
		}
		target.getTempVar().put(OLM_BURN_VAR, Server.getTotalTicks() + 100);
		
		CycleEventHandler.getSingleton().addEvent(target, true, new CycleEvent() {
			
			int burnsLeft = 5;
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!isActive || target.isDead() || target.raidsManager == null) {
					container.stop();
					return;
				}

				for (int i = 0; i < Skills.STAT_COUNT; i++) {
					switch (i) {
					case Skills.HITPOINTS:
						break;
					case Skills.PRAYER:
						target.getSkills().decrementPrayerPoints(2);
						break;
						
						default:
							target.getSkills().updateLevel(i, -2, 1);
							break;
					}
				}
				
				target.dealTrueDamage(5, -1, null, getHeadNpc());
				if (burnsLeft < 5)
					target.forceChat("Burn with me!");
				
				for (Player player : players()) {
					if (player == target) {
						continue;
					}
					int thisLastBurnTick = player.getTempVar().getInt(OLM_BURN_VAR, -1);
					if (thisLastBurnTick > Server.getTotalTicks()) {
						continue;
					}
					if (!player.getCurrentLocation().isWithinDistance(target.getCurrentLocation(), 1)) {
						continue;
					}
					
					Projectile proj = new Projectile(Graphic.getOsrsId(1349), target.getCurrentLocation(), player.getCurrentLocation());
					proj.setStartDelay(0);
					proj.setEndDelay(PROJ_START_DELAY);
					proj.setSlope(0);
					proj.setStartHeight(28);
					proj.setEndHeight(28);
					proj.setLockon(player.getProjectileLockon());
					
					GlobalPackets.createProjectile(proj);
					
					player.forceChat("I will burn with you");
					burn(player, false);
					burnsLeft = 5;
				}
				
				burnsLeft--;
				if (burnsLeft <= 0) {
					target.sendMessage("You feel the deep burning inside dissipate.");
					target.getTempVar().remove(OLM_BURN_VAR);
					container.stop();
					return;
				}
			}
		}, 7).execute();
		
	}
	
	
	public void handleTickActions() {
		
		if (lastHeal > 0) {
			lastHeal--;
		}
		
		if (lastHeal <= 0 && (CombatFormulas.consideredForHit(rightClawNpc)
				|| CombatFormulas.consideredForHit(leftClawNpc))
				&& getHeadNpc().getSkills().getLifepoints() < getHeadNpc().getSkills().getMaximumLifepoints()) {
			getHeadNpc().healNpc(getHeadNpc().getSkills().getMaximumLifepoints() - getHeadNpc().getSkills().getLifepoints());
			lastHeal = 8;
		}
		
		if (omega && getHeadNpc().getSkills().getLifepoints() > 0) {
			fallingCrystalMiniBombs(null);
			
			if (Server.getTotalTicks() % 5 == 0) {
				Player p = Misc.random(players());
				if (p != null && p.isActive && p.raidsManager != null)
					fallingCrystalMiniBombs(p.getCurrentLocation());
			}
		}
		
		if (isRitualising()) {
			return;
		}
		
		if (getHeadNpc().getAttackTimer() <= 0) {
			attackCycles++;
			
			nextAttack = AttackCycle.getNext(attackCycles);
			
			cycleLeftClawAttacks();
		}
		
		if (getHeadNpc().getAttackTimer() <= 0) {
			
			hasPossibleTargetsThisTick = headHasPossibleTargets();
			
			if (hasPossibleTargetsThisTick) {
				
				if (!turnedFaceLastAttack) {
					checkFace();
				}
				
			} else {
				checkFace();
				
			}
		}
		
	}
	
	private void cycleLeftClawAttacks() {
		if (nextAttack == AttackCycle.LEFT_CLAW_ATTACK && CombatFormulas.consideredForHit(getLeftClawNpc()) && getLeftClawNpc().setOrGetBoss() != null) {
			GreatOlmLeftHandBoss boss = (GreatOlmLeftHandBoss) getLeftClawNpc().setOrGetBoss();
			if (boss != null) {
				boss.assignSpell();
			}
		}
	}
	
	public void doSwing() {
		
		boolean useSpecial = attackCycles >= nextSpecialAttackTick;
		
		if (isRitualising()) {
			return;
		}
		
		turnedFaceLastAttack = false;
		
		if (startFlinching) {
			if (CombatFormulas.consideredForHit(getLeftClawNpc()) && getLeftClawNpc().setOrGetBoss() != null) {
				GreatOlmLeftHandBoss boss = (GreatOlmLeftHandBoss) getLeftClawNpc().setOrGetBoss();
				boss.flinch();
				raids.party.executeForMember(player -> {
					player.sendMessage("The Great Olm's left claw clenches to protect itself temporarily.");
				});
			}
			startFlinching = false;
		}
		
		if (nextAttack == AttackCycle.LEFT_CLAW_ATTACK) {
			
			castLeftClawSpell();
			
			if (skippedAnAttack)
				castHeadSpell(useSpecial);

		} else if (nextAttack == AttackCycle.HEAD_ATTACK) {

			castHeadSpell(useSpecial);

		} else if (nextAttack == AttackCycle.EMPTY) {
			if (skippedAnAttack)
				castHeadSpell(useSpecial);
		}
		
	}
	
	private boolean castLeftClawSpell() {
		
		if (CombatFormulas.consideredForHit(getLeftClawNpc()) && getLeftClawNpc().getAttackTimer() <= 0 && getLeftClawNpc().setOrGetBoss() != null) {
			GreatOlmLeftHandBoss boss = (GreatOlmLeftHandBoss) getLeftClawNpc().setOrGetBoss();
//			boss.assignSpell(); //asign the spells so players can skip them, then call attack after.

			if (!hasPossibleTargetsThisTick || startFlinching) {
				return false;
			}
			
			return boss.startAttack();
		}
		
		return false;
	}
	
	private void castHeadSpell(boolean usingSpecial) {
		
		if (!hasPossibleTargetsThisTick) {
			skippedAnAttack = true;
			return;
		}
		
		skippedAnAttack = false;
		
		if (usingSpecial) {
			createNextSpecialAttackTick();
		}
		
		boolean hasCastSpecialAttack = false;
		
		//cast spells here
		switch (Misc.random(phasesUnlocked)) {
		case ACID:
			if (usingSpecial) {
				if (nextSpecialAttack[0]) {
					if (acidDripTimer.complete()) {
						acidDripTimer.startTimer(SPEC_DELAYS);
						acidDripSpell();
						hasCastSpecialAttack = true;
					}
				} else {
					acidSpraySpell();
					hasCastSpecialAttack = true;
				}
				nextSpecialAttack[0] = !nextSpecialAttack[0];
			}
			break;

		case FLAME:
			if (usingSpecial) {
				if (nextSpecialAttack[1]) {
					if (burnTimer.complete()) {
						burnTimer.startTimer(SPEC_DELAYS);
						shootBurnSpell();
						hasCastSpecialAttack = true;
					}
				} else {// eden has = else if (!hasAttribute("flame_wall")) {
					fireWallSpell();
					hasCastSpecialAttack = true;
				}
				nextSpecialAttack[1] = !nextSpecialAttack[1];
			}
			break;

		case CRYSTAL:
			if (usingSpecial) {
				if (nextSpecialAttack[2]) {
					crystalBombSpell();
					hasCastSpecialAttack = true;
				} else {
					if (fallingCrystalsTimer.complete()) {
						fallingCrystalsTimer.startTimer(SPEC_DELAYS);
						fallingCrystalsSpell();
						hasCastSpecialAttack = true;
					}
				}
				nextSpecialAttack[2] = !nextSpecialAttack[2];
			}
			break;

		case OMEGA:
			if (usingSpecial) {
				lifeSiphonSpell();
				hasCastSpecialAttack = true;
			}
			break;

		}

		//basic attacks or orb
		if (!hasCastSpecialAttack) {
			if (Misc.randomBoolean(20)) {
				spheresAttack();
			} else {
				for (Player player : getPossibleTargets()) {
					switch (attack) {
					case DEFAULT_MAGIC:
						throttleFarcast(player, CombatType.MAGIC, Misc.random(40), 1339);
						break;
					case DEFAULT_RANGED:
						throttleFarcast(player, CombatType.RANGED, Misc.random(40), 1340);
						break;
					}
				}
				checkSwitch();
			}
		}
	}
	
	private static final Graphic SIPHON_HIT_GFX = Graphic.create(147, GraphicType.HIGH);
	
	private void lifeSiphonSpell() {
		
		startAttackAnimation();
		
		final Location l1 = getRandomLocationForFace();
		l1.setZ(raids.height);
		final Location l2 = getRandomLocationForFace();
		l2.setZ(raids.height);
		
		int hitDelayClientside = Misc.serverToClientTick(3);
		
		//center of head
		Location start = Location.create(getHeadNpc().getX()+5, getHeadNpc().getY()+2, getHeadNpc().getZ());
		
		Projectile proj = new Projectile(Graphic.getOsrsId(1355), start, l1);
		proj.setStartDelay(PROJ_START_DELAY);
		proj.setEndDelay(hitDelayClientside);
		proj.setStartHeight(50);
		proj.setEndHeight(0);
		proj.setSlope(2);
		
		GlobalPackets.createProjectile(proj);
		
		proj.setEndLoc(l2);
		
		GlobalPackets.createProjectile(proj);
		
		int spawnPoolsDelay = Misc.serverToClientTick(2);
		
		GlobalPackets.stillGfx(playersInside, l1, Graphic.getOsrsId(1363), 10, spawnPoolsDelay);
		
		GlobalPackets.stillGfx(playersInside, l2, Graphic.getOsrsId(1363), 10, spawnPoolsDelay);
		
		
		Server.getTaskScheduler().schedule(new Task() {
			
			int ticks = 0;
			int totalDamage = 0;
			
			@Override
			protected void execute() {

				if (!isActive) {
					this.stop();
					return;
				}

				if (ticks == 9) {
					
					this.stop();
					
					for (Player player : players()) {
						if (!player.getCurrentLocation().matches(l1) && !player.getCurrentLocation().matches(l2)) {
							int damage = Misc.random(10, 30);
							totalDamage += damage;

							player.dealTrueDamage(damage, 0, SIPHON_HIT_GFX, getHeadNpc());
						}
					}
					
					getHeadNpc().healNpc(totalDamage * 5);
				}

				ticks++;
				
			}
		});
	}
	
	private void acidDripSpell() {
		
		startAttackAnimation();
		
		Player target = Misc.random(getPossibleTargets());
		
		if (target == null) return;
		
		int dist = (int)target.getCurrentLocation().getDistance(getHeadNpc().getCurrentLocation());
		
		Location start = createStartFromHead(target.getCurrentLocation());
		
		Projectile proj = new Projectile(Graphic.getOsrsId(1354), start, target.getCurrentLocation());
		proj.setStartDelay(PROJ_START_DELAY);
		
		proj.setEndDelay(getProjectileEndDelay(proj.getStartDelay(), dist));
		proj.setStartHeight(45);
		proj.setEndHeight(28);
		proj.setSlope(2);
		proj.setLockon(target.getProjectileLockon());
		GlobalPackets.createProjectile(proj);
		

		int hitDelay = Misc.clientToServerTick(proj.getEndDelay());
		
		Server.getTaskScheduler().schedule(hitDelay, ()-> {
			if (!isActive) return;
			
			if (target.isActive()) {
				target.sendMessage("<col=ff1a1a>The Great Olm has smothered you in acid. It starts to drip off slowly.");
				buildAcidPool(target.getCurrentLocation().copyNew(), true);
				target.getTempVar().put("acid_pool", true);

				CycleEventHandler.getSingleton().addEvent(target, new CycleEvent() {

					@Override
					public void stop() {
					}

					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
						target.getTempVar().remove("acid_pool");
					}
				}, 20);

			}

		});
		
	}
	
	public void buildAcidPool(Location loc, boolean hitOnSpawn) {
		if (acidPools != null && !acidPools.containsKey(loc)) {
			AcidSpray acidSpray = new AcidSpray(this, loc, hitOnSpawn);
			raids.objectManager.addObject(acidSpray);
			acidPools.put(loc, acidSpray);
		}
	}
	
	private void acidSpraySpell() {
		
		startAttackAnimation();
		
		int amount = Misc.random(8, 10);
		
		List<Location> tiles = new ArrayList<>(amount);
		int count = 0;
		
		while (count < amount) {
			Location tile = getRandomLocationForFace();
			if (!tiles.contains(tile)) {
				tiles.add(tile);
				count++;
			}
		}

		Location start = createStartFromHead(getHeadNpc().getCurrentLocation());
		
		int hitDelay = 4;
		int projDelay = Misc.serverToClientTick(hitDelay);
		
		for (Location tile : tiles) {
			Projectile proj = new Projectile(Graphic.getOsrsId(1354), start, tile);
			proj.setStartHeight(50);
			proj.setEndHeight(0);
			proj.setSlope(2);
			proj.setStartDelay(PROJ_START_DELAY);
			proj.setEndDelay(projDelay);
			GlobalPackets.createProjectile(proj);
		}
		
		Server.getTaskScheduler().schedule(hitDelay, ()-> {
			if (!isActive) return;
			
			for (Location tile : tiles) {
				buildAcidPool(tile, false);
			}
			tiles.clear();
		});
		
	}
	
	private void startAttackAnimation() {
		render(getAttackAnimation(face));
		
		Server.getTaskScheduler().schedule(2, ()-> {
			if (!isActive || headNpc == null || headNpc.isDead()) return;
			
			render();
			render(render);
		});
	}
	
	private void fireWallSpell() {
		//animate object
		startAttackAnimation();
		
		Player target = Misc.random(getPossibleTargets());
		
		if (target == null)
			return;
		
		int northLimit = roomLoc.getY() + 50;
		int southLimit = roomLoc.getY() + 37;
		
		if (target.getY() < southLimit || target.getY() > northLimit)
			return;
		
		final int centerY = target.getY();
		final int z = getHeadNpc().getZ();

		final int endX = isWest ? WHOLE_ROOM_RECTANGLE.getNorthEast().getX() : WHOLE_ROOM_RECTANGLE.getSouthWest().getX();

		final Location start = Projectile.getLocationOffset(getHeadNpc().getCurrentLocation(), target.getCurrentLocation(), getHeadNpc().getSize());

		final Location endNorth = Location.create(endX, centerY + 1, z);

		final Location endSouth = Location.create(endX, centerY - 1, z);

		Projectile proj1 = new Projectile(Graphic.getOsrsId(1347), start, endNorth);
		proj1.setStartHeight(45);
		proj1.setEndHeight(10);
		proj1.setStartDelay(PROJ_START_DELAY);
		proj1.setEndDelay(Misc.serverToClientTick(2));
		proj1.setSlope(2);

		GlobalPackets.createProjectile(proj1);

		proj1.setEndLoc(endSouth);
		GlobalPackets.createProjectile(proj1);

		Projectile smallFireProjectile = new Projectile(Graphic.getOsrsId(1348), endNorth, endNorth);
		smallFireProjectile.setStartHeight(28);
		smallFireProjectile.setEndHeight(28);
		smallFireProjectile.setStartDelay(Misc.serverToClientTick(2));
		smallFireProjectile.setEndDelay(Misc.serverToClientTick(3));
		smallFireProjectile.setSlope(0);

		Location fireEnd = endNorth.copyNew();

		final List<Location> fireLocs = new ArrayList<>();
		fireLocs.add(fireEnd.copyNew());

		//north side
		for (int i = 0; i < 9; i++) {
			fireEnd.transformMe(isWest ? -1 : 1, 0, 0);
			fireLocs.add(fireEnd.copyNew());
			smallFireProjectile.setEndLoc(fireEnd);
			GlobalPackets.createProjectile(smallFireProjectile);
		}
		//south side
		smallFireProjectile.setStartLoc(endSouth);
		fireEnd = endSouth.copyNew();
		fireLocs.add(fireEnd.copyNew());
		for (int i = 0; i < 9; i++) {
			fireEnd.transformMe(isWest ? -1 : 1, 0, 0);
			fireLocs.add(fireEnd.copyNew());
			smallFireProjectile.setEndLoc(fireEnd);
			GlobalPackets.createProjectile(smallFireProjectile);
		}


		Server.getTaskScheduler().schedule(1, ()-> {
			if (!isActive) return;
			
			for (int i = 0, len = fireLocs.size(); i < len; i++) {
				Location loc = fireLocs.get(i);
				raids.clipping.addClipping(loc.getX(), loc.getY(), loc.getZ(), RegionClip.WALKING_TILE_BLOCKED);
			}

			//move players who are standing on top of the fire
			for (Player p : playersInside) {

				if (p != null && p.isActive) {
					boolean northFire = p.getY() == endNorth.getY();
					boolean southFire = p.getY() == endSouth.getY();
					if (northFire || southFire) {
						p.dealTrueDamage(5, 0, null, getHeadNpc());
						//direction is outside the walls
						Direction dir = northFire ? Direction.NORTH : Direction.SOUTH;
						//move target inside the walls
						if (target == p) {
							dir = dir == Direction.NORTH ? Direction.SOUTH : Direction.NORTH;
						} else {
							p.sendMessage("You leap away from the flame, getting slightly scorched in the process.");
						}
						ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, dir).addSecondDirection(dir.getStepX(), dir.getStepY(), 1, 0);
						p.setForceMovement(mask);
					}

				}

			}
		});

		fireEnd = endNorth.copyNew();

		Server.getTaskScheduler().schedule(2, ()-> {

			if (!isActive) return;

			for (int i = 0, len = fireLocs.size(); i < len; i++) {
				GameObject fire = GameObject.createTickObject(32297, fireLocs.get(i), 0, 10, 7, -1);
				raids.objectManager.addObject(fire);
			}
			fireLocs.clear();

		});

		Server.getTaskScheduler().schedule(8, ()-> {

			if (!isActive) return;
			
			for (Player p : playersInside) {
				if (p != null && p.isActive) {
					if (p.getY() == centerY) {
						p.dealTrueDamage(Misc.random(30, 60), 0, null, getHeadNpc());
						p.sendMessage("You are scorched by the imbued flames.");
					}
				}

			}

		});
	}
	
	public void spheresAttack() {
		startAttackAnimation();
		
		List<Player> randomPlayers = getPossibleTargets();
		
		if (randomPlayers.isEmpty()) {
			return;
		}
		
		Collections.shuffle(randomPlayers);
		
		int count = Math.min(2, randomPlayers.size());
		
		for (int i = 0; i < count; i++) {
			Player player = randomPlayers.get(i);
			
			sendSphere(player, Sphere.getRandom());
		}
		
	}
	
	private void sendSphere(Player player, Sphere sphere) {
		int hitDelay = 6;
		
		Location start = createStartFromHead(player.getCurrentLocation());
		
		Projectile proj = new Projectile(sphere.getStartGfx(), start, player.getCurrentLocation());
		proj.setStartDelay(PROJ_START_DELAY);
		proj.setEndDelay(Misc.serverToClientTick(hitDelay));
		proj.setStartHeight(50);
		proj.setEndHeight(28);
		proj.setSlope(5);
		proj.setLockon(player.getProjectileLockon());
		
		GlobalPackets.createProjectile(proj);
		
		final boolean overhead = CombatPrayer.usingProtectPrayer(player, ProtectionPrayer.PROTECT_MELEE.ordinal())
				|| CombatPrayer.usingProtectPrayer(player, ProtectionPrayer.PROTECT_MAGIC.ordinal())
				|| CombatPrayer.usingProtectPrayer(player, ProtectionPrayer.PROTECT_RANGE.ordinal());
		
		if (overhead) {
			CombatPrayer.disableProtectPrayer(player);
			player.getSkills().decrementPrayerPoints(player.getSkills().getPrayerPoints() / 2);;
		}
		
		player.sendMessage(sphere.getMessage());
		if (overhead)
			player.sendMessage("Your prayers have been sapped."); //we don't have word wrapping yet...
		
		CycleEventHandler.getSingleton().addEvent(player, true, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				
				if (CombatPrayer.usingProtectPrayer(player, sphere.getPrayer().ordinal())) {
					player.dealTrueDamage(0, -1, null, getHeadNpc());
				} else {
					player.dealTrueDamage(player.getSkills().getLifepoints() / 2, -1, sphere.getEndGfx(), getHeadNpc());
				}
			}
		}, hitDelay+1);
		
	}
	
	public void startPenultimate(NPC starter) {
		if (penultimateCycle != null) {
			return;
		}
		
		final int ticks = 36; //eden is 33
		
		final boolean rightClawKilled = starter.npcType == RIGHT_CLAW_NPC_ID;
		
		NPC toCheck;
		if (rightClawKilled) {
			toCheck = getLeftClawNpc();
		} else {
			toCheck = getRightClawNpc();
		}
		
		penultimateCycle = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			int cycle = 0;
			
			@Override
			public void stop() {
				if (reviveClaw != null && !reviveClaw.isDead()) {
					reviveClaw.nullNPC();
				}
				penultimateCycle = null;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				
				if (!isActive) {
					container.stop();
					return;
				}
				
				if (!CombatFormulas.consideredForHit(toCheck)) {
					
					container.stop();
					
					if (getLeftClawNpc() != null)
						getLeftClawNpc().actionTimer = 0;
					if (getRightClawNpc() != null)
						getRightClawNpc().actionTimer = 0;
					
					return;
				}
				
				if (cycle == 4) {
					int npcIndex = Misc.findFreeNpcSlot();
					reviveClaw = new RevivingClaw(npcIndex, rightClawKilled ? 9010 : 9001, 33);
					if (npcIndex != -1) {
						NPCHandler.spawnNpc(reviveClaw, starter.getX(), starter.getY(), starter.getZ(), 0);
						reviveClaw.setBarFill(0, 10, 1, 1000, false, 0xFFD700, 0xD45656);
						reviveClaw.faceLocation(reviveClaw.getX() + 2, reviveClaw.getY() + (isWest ? 5 : -5));
					}
				}
				
				if (cycle >= ticks) {
					container.stop();
					
					respawnClaw(rightClawKilled);
				}
				
				cycle++;
				
			}
		}, 1);
			
	}

	public void onClawKilled(NPC claw, boolean proceed) {
		if (!CombatFormulas.consideredForHit(getRightClawNpc()) && !CombatFormulas.consideredForHit(getLeftClawNpc()) && !omega) {
			
			if (onLastStage()) {
				shakeCameraForMembers();
				omega = true;
				phasesUnlocked.add(GreatOlmPhase.OMEGA);
				return;
			}
			
			if (switching)
				return;
			
			setRitualising(true);
			switching = true;
			getHeadNpc().nullNPC();
			
			Server.getTaskScheduler().schedule(new Task() {
				int count;
				
				@Override
				protected void execute() {
					if (!isActive) {
						this.stop();
						return;
					}
					
					 if (count == 1) {
						 render(Animation.getOsrsAnimId(7348));
					 } else if (count == 5) {
						 //despawn objects maybe?
						 getHeadObj().setTicks(0);
						 getRightObj().setTicks(0);
						 getLeftObj().setTicks(0);
						 
						 if (!proceed) {
							 this.stop();
							 return;
						 }
					 } else if (count == 8) {
						 bombs = true;
						 shakeCameraForMembers();
					 } else if (count == 36) {
						 bombs = false;
						 resetCameraShakeForAllMembers();
					 } else if (count == 41) {
						 
						 this.stop();
						 
						 stage++;
						 
						 if (onLastStage()) {
							 penultimate = true;
						 }
						 
						 if (stage < lastStage+1) {
							 isWest = !isWest;
							 
							 spawnOlmObject();
							 wakeOlm();
						 }
						 
					 }
					 
					 if (bombs) {
						 if (Server.getTotalTicks() % 5 == 0) {
							 Player p = Misc.random(players());
							 if (p != null && p.isActive && p.raidsManager != null)
								 fallingCrystalMiniBombs(p.getCurrentLocation());
						 }
						 fallingCrystalMiniBombs(null);
					 }
					 
					 count++;
					
				}
			});
			
			
		} else if (claw.npcType == RIGHT_CLAW_NPC_ID) {
			if (CombatFormulas.consideredForHit(getLeftClawNpc())) {
				GreatOlmLeftHandBoss boss = (GreatOlmLeftHandBoss) getLeftClawNpc().setOrGetBoss();
				if (boss.flinching) {
					boss.unflinch();
				}
			}
		}
	}

	private void shakeCameraForMembers() {
		raids.party.executeForMember(player -> {
			player.getPacketSender().camShake(0, 6, 0, 0);
		});
	}

	private void resetCameraShakeForAllMembers() {
		raids.party.executeForMember(player -> {
			player.getPacketSender().camReset();
		});
	}

	private List<Player> getPossibleTargets() {
		List<Player> possibleTargets = new ArrayList<>();
		for (Player player : playersInside) {
			if (player == null || player.raidsManager == null) {
				continue;
			}
			if (!getHeadNpc().consideredForHit(player) || !player.isActive() || !RADIUS[isWest ? 0 : 1][face].inside(player.getCurrentLocation())) {
				continue;
			}
			possibleTargets.add(player);
		}
		return possibleTargets;
	}
	
	public List<Player> players() {
		return playersInside;
	}
	
	
	public void clear() {
		//TODO: code cleanup
		if (headNpc != null) {
			headNpc.nullNPC();
		}
		if (leftClawNpc != null) {
			leftClawNpc.nullNPC();
		}
		if (rightClawNpc != null) {
			rightClawNpc.nullNPC();
		}
		this.isActive = false;
	}

	public void applyFinalDeath() {
		render(Animation.getOsrsAnimId(7348));
		
		//save to highscores
		CoxHiscores.saveScore(raids);
		final int ticks = Server.getTotalTicks() - raids.startTicks;
		
		getHeadObj().setTicks(5);
		raids.party.executeForMember(player -> {
			RaidsRewards.genRewards(player, raids.points.getPoints(player.mySQLIndex));
			
			//personal best
			int currentBest = player.getHiscores().getValue(HiscoreSlot.CHAMBERS_OF_XERIC);
			if (currentBest == 0 || currentBest > ticks) {
				player.getHiscores().setValue(HiscoreSlot.CHAMBERS_OF_XERIC, ticks, true);
				
				player.sendMessage("Your new best time: " + Misc.ticksToTimeShort(ticks));
			}
		});

		crystal.animate(Animation.getOsrsAnimId(7506));
		
		//remove the crystal that blocks path
		Server.getTaskScheduler().schedule(3, ()-> {
			if (!isActive) return;

			raids.objectManager.removeObject(crystal, Integer.MAX_VALUE);
			
			//spawn chest
			GameObject openChest = new GameObject(30028, chestLocation, 0, 10);
			openChest.setTicks(Integer.MAX_VALUE);
			raids.objectManager.addObject(openChest);
			
			
			//this packet is possible to spawn object on floor 1 i think as a dummy object
			//player.getPacketSender().addConstructionObjectPacket(new GameObject(lightId, player.getCurrentLocation().transform(0, 0, 1), 0, 10));

			//dummy lights will make the spawned lights above chest show up in 3 ticks
//			GameObject light = new GameObject(30029, chestLocation.transform(-21, 1, 0), 0, 10);
//			light.setTicks(Integer.MAX_VALUE);
//			raids.objectManager.addObject(light);
//			light = new GameObject(30030, chestLocation.transform(-21, 0, 0), 0, 10);
//			light.setTicks(Integer.MAX_VALUE);
//			raids.objectManager.addObject(light);
			
		});

		raids.party.executeForMember(player -> {
			player.getPacketSender().camReset();
			player.sendMessage("As the Great Olm collapses, the crystal blocking your exit has been shattered.");
		});
	}
	
	private Location createStartFromHead(Location targetLoc) {
		return projStart[isWest ? 0 : 1][face];
	}
	
	private int getProjectileEndDelay(int startDelay, int distance) {
		int delay = startDelay + Misc.serverToClientTick(1);
		if (distance == 0 || distance == 1) {
		} else if (distance >= 2 && distance <= 5) {
		} else if (distance >= 6 && distance <= 8) {
			delay += Misc.serverToClientTick(1);
		} else {
			delay += Misc.serverToClientTick(2);
		}
		return delay;
	}
	
	public boolean headHasPossibleTargets() {
		for (Player player : playersInside) {
			if (player == null) {
				continue;
			}
			if (!getHeadNpc().consideredForHit(player) || !player.isActive() || !RADIUS[isWest ? 0 : 1][face].inside(player.getCurrentLocation())) {
				continue;
			}
			return true;
		}
		return false;
	}

	public boolean isRitualising() {
		return ritualising;
	}

	public void setRitualising(boolean ritualising) {
		this.ritualising = ritualising;
	}
	
	/**
	 * 
	 * @param p - Player who left
	 * @return - true if player was removed from olms battle room.
	 */
	public boolean playerDiedOrLeft(Player p) {
		
		if (playersInside.remove(p)) {
			p.getTempVar().remove("acid_pool");
			return true;
		}
		return false;
	}
	
	public void logout(Player p) {
		if (playerDiedOrLeft(p)) {
			p.getPA().movePlayer(raids.respawnLoc);
		}
	}
	
	public boolean onLastStage() {
		return stage == lastStage;
	}
	
	public int getStage() {
		return stage;
	}
	
	public boolean isInOmegaStage() {
		return omega;
	}
	
	public void leftClawHit(BattleState state, boolean alreadyClamped) {
		
		if (alreadyClamped) {
			state.setCurrentHit(0);
			return;
		}
		
		if (!onLastStage() && state.getCurrentHit() > 0 && CombatFormulas.consideredForHit(getRightClawNpc()) && !alreadyClamped) {
			if (Misc.randomBoolean(2))
				startFlinching = true;
		}
		
		if (getAttributes().getBoolean("claw_heal")) {
			int heal = state.getCurrentHit();
			if (heal > 0) {
				state.setCurrentHit(0);
				getLeftClawNpc().healNpc(heal);
			}
			return;
		}
	}
	
	public void createNextSpecialAttackTick() {
		int ran = 7 + Misc.random(7)*2;
		nextSpecialAttackTick = attackCycles + ran;
	}
}
