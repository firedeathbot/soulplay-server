package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.game.model.npc.NPC;

public class RevivingClaw extends NPC {

	public RevivingClaw(int npcSlot, int npcId, int ticks) {
		super(npcSlot, npcId);
		setAliveTick(ticks);
	}

}
