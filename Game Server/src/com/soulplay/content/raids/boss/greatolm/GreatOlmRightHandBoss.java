package com.soulplay.content.raids.boss.greatolm;

import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.content.raids.RaidsPoints;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Mob;

@BossMeta(ids = {8991})
public class GreatOlmRightHandBoss extends Boss {

	private GreatOlmInstanceHandler greatOlm;
	
	public GreatOlmRightHandBoss(NPC boss) {
		super(boss);
	}

	@Override
	public void combatLogic(NPC npc, Client c) {
		
	}

	@Override
	public void process(NPC npc) {
		if (!npc.isDead() && npc.underAttackBy < 1)
			greatOlm.clawDamageTaken[greatOlm.isWest ? 1 : 0] = 0;
	}

	@Override
	public Prayers protectPrayer(int npcId) {
		return null;
	}

	@Override
	public int protectPrayerDamageReduction(int damage) {
		return 0;
	}

	@Override
	public void onSpawn(NPC npc) {
		//NOTE: this is called twice
		npc.setRetaliates(false);
		npc.isAggressive = false;
		npc.stopAttack = true;
		
		if (npc.dungVariables[0] != null)
			greatOlm = (GreatOlmInstanceHandler) npc.dungVariables[0];
		
		npc.getSkills().setStaticLevel(Skills.MAGIC, 87);
		npc.getCombatStats().setMagicAccBonus(60);//using this for now temporarily
		npc.getCombatStats().setMagicDefBonus(50);//using this for now temporarily
		npc.getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] = 60;
		npc.getPlayerBonus()[EquipmentBonuses.DEFENSE_MAGIC.getId()] = 50;
		
		npc.getSkills().setStaticLevel(Skills.RANGED, 250);
		npc.getPlayerBonus()[EquipmentBonuses.ATTACK_RANGED.getId()] = 60;
		npc.getPlayerBonus()[EquipmentBonuses.DEFENSE_RANGED.getId()] = 50;
		npc.getCombatStats().setRangedAccBonus(60);//using this for now temporarily
		npc.getCombatStats().setRangedDefBonus(50);//using this for now temporarily
		
		npc.getSkills().setStaticLevel(Skills.DEFENSE, 150);
	}

	@Override
	public boolean ignoreMeleeBonus() {
		return false;
	}

	@Override
	public boolean ignoreRangeBonus() {
		return false;
	}

	@Override
	public boolean ignoreMageBonus() {
		return false;
	}

	@Override
	public int followDistance() {
		return 0;
	}

	@Override
	public boolean canMove() {
		return false;
	}

	@Override
	public int extraAttackDistance() {
		return 0;
	}

	@Override
	public void proxyDamage(NPC npc, Hit hit, Mob source) {
		if (hit.getDamage() <= 0 || !source.isPlayer()) {
			return;
		}

		Player player = source.toPlayerFast();
		if (player.raidsManager == null) {
			return;
		}

		if (player.raidsManager.greatOlmManager.rightClawRespawned) {
			return;
		}
		
		int damage = Math.min(hit.getDamage(), npc.getSkills().getLifepoints());
		
		greatOlm.clawDamageTaken[greatOlm.isWest ? 1 : 0] += damage;

		player.raidsManager.points.increasePoints(player.mySQLIndex, damage * RaidsPoints.POINTS_PER_DMG);
	}

	@Override
	public void onDeath(NPC npc, Client c) {
		if (greatOlm != null) {
			
			greatOlm.clawDamageTaken[greatOlm.isWest ? 1 : 0] = 0;
			
			greatOlm.onClawKilled(npc, true);
			
			greatOlm.getRightObj().animate(Animation.getOsrsAnimId(7352));
			greatOlm.getRightObj().setTicks(3);
			
			if (greatOlm.getLeftClawNpc() != null && greatOlm.getLeftClawNpc().getSkills().getLifepoints() > 0) {
				greatOlm.getLeftObj().animate(Animation.getOsrsAnimId(7363));
				Server.getTaskScheduler().schedule(2, ()-> {
					greatOlm.getLeftObj().animate(Animation.getOsrsAnimId(7365));
				});
			}
			
			if (greatOlm.penultimate && greatOlm.penultimateCycle == null) {
				greatOlm.startPenultimate(npc);
				return;
			}
		}
	}

	@Override
	public void afterNpcAttack(NPC npc, Player p, int damage) {
		
	}

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
		//TODO: this code probably needs to be moved to boss.onDeath() function to avoid bugs
//		if (greatOlm.penultimate) {
//			boolean active = false;
//			int damage = 0;
//			if (state.getCurrentHit() > 0) {
//				damage += state.getCurrentHit();
//			}
//			
//			if (npc.getSkills().getLifepoints() - damage < 1) {
//				damage = npc.getSkills().getLifepoints() - 1;
//				active = true;
//			}
//			
//			if (active && greatOlm.penultimateCycle == null) {
//				state.setCurrentHit(damage);
//				
//				greatOlm.startPenultimate(npc);
//				return;
//			}
//		}
	}

}