package com.soulplay.content.click.npc;

import com.soulplay.Config;
import com.soulplay.config.WorldType;
import com.soulplay.content.donate.DonationInterface;
import com.soulplay.content.instances.InstanceInterface;
import com.soulplay.content.items.pos.BuyOffers;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.partyroom.DropParty;
import com.soulplay.content.minigames.taibwowannaicleanup.TaiBwoWannaiNpcs;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.ClueManager;
import com.soulplay.content.minigames.treasuretrails.ClueType;
import com.soulplay.content.minigames.treasuretrails.Emote2StepClue;
import com.soulplay.content.minigames.treasuretrails.EmoteDoubleAgentClue;
import com.soulplay.content.minigames.warriorsguild.WarriorsGuild;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.skills.Fishing;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.HouseStyle;
import com.soulplay.content.player.skills.crafting.TanHide;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.DungeoneeringTutorDialogue;
import com.soulplay.content.player.skills.dungeoneeringv2.npcs.RewardTraderDialogue;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonNpcs;
import com.soulplay.content.player.skills.hunter.falconry.Falconry;
import com.soulplay.content.player.skills.hunting.Hunter;
import com.soulplay.content.player.skills.runecrafting.Runecrafting;
import com.soulplay.content.player.skills.summoning.FamiliarInteraction;
import com.soulplay.content.shops.ShopType;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.net.packet.in.ClickNPC;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.util.Misc;

public final class NpcFirstClick {
	
	private NpcFirstClick() {
		
	}
	
	public static void execute(Client c, int npcType) {
		final int npcIndex = c.npcClickIndex;

		ClickNPC.resetNpcClickVariables(c);

		if (c.isDead()) {
			return;
		}

		final NPC npc = NPCHandler.npcs[npcIndex];

		if (npc == null || !npc.isActive || npc.isDead()) {
			return;
		}

		if (npc != null) {
			npc.turnNpc(c.getX(), c.getY());
			c.face(npc);
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, npc, InstanceInteractType.NPC, InstanceInteractIndex.FIRST)) {
			return;
		}

		if (Hunter.handlingHunterNpc(c, npc)) {
			return;
		}

		if (DungeonNpcs.handle1(c, npc, npcType)) {
			return;
		}
		
		if (TaiBwoWannaiNpcs.OptionOne(c, npc, npcType) && c.inTaiBwoWannai()) {
			return;
		}

		if (npc.isPetNpc()) {
			if (npc.spawnedBy == c.getId()) {
				Pet.pickupPet(c, Pet.getPetForNPC(c.npcType));
			} else {
				c.sendMessage("This is not your pet!");
			}
			return;
		}
		
		if (FamiliarInteraction.interactWithFamiliar(c, npcType)) {
			return;
		}

		final PluginResult<NpcClickExtension> pluginResult = PluginManager.search(NpcClickExtension.class, npc.npcType);
		if (pluginResult.invoke(() -> pluginResult.extension.onNpcOption1(c, npc))) {
			return;
		}

		if (Fishing.isFishingNPC(c, 1, npcType, npcIndex)) {
			return;
		}

		/*
		 * if(Fishing.fishingNPC(c, npcType)) { Fishing.fishingNPC(c, 1,
		 * npcType); return; }
		 */
		switch (npcType) {
		case 5141:
			if (npc.spawnedBy != c.getId()) {
				c.sendMessage("Nothing interesting happens.");
				return;
			}

			for (ClueDifficulty difficulty : ClueDifficulty.values) {
				int data = c.getTreasureTrailManager().getCluesData()[difficulty.getIndex()];
				if (data == -1) {
					continue;
				}

				ClueType clueType = ClueManager.clues.get(difficulty.getIndex()).get(Misc.getFirst(data)).get(Misc.getSecond(data));
				if (clueType instanceof Emote2StepClue) {
					int stage = c.attr().getOrDefault("2stepemote", 0);
					if (stage == 2) {
						c.getDialogueBuilder().sendNpcChat(5141, ClueManager.URI_QUOTES[Misc.randomNoPlus(ClueManager.URI_QUOTES.length)]).sendPlayerChat("What?");
						clueType.checkComplete(c, difficulty);
					}
				} else if (clueType instanceof EmoteDoubleAgentClue) {
					int stage = c.attr().getOrDefault("doubleagent", 0);
					if (stage == 3) {
						c.getDialogueBuilder().sendNpcChat(5141, ClueManager.URI_QUOTES[Misc.randomNoPlus(ClueManager.URI_QUOTES.length)]).sendPlayerChat("What?");
						clueType.checkComplete(c, difficulty);
					}
				}
			}
			break;
		case 108369:
			if (c.tobManager == null) {
				break;
			}

			if (!c.tobManager.started()) {
				c.sendMessage("The Raid hasn't started yet.");
				break;
			}

			c.getDialogueBuilder().sendNpcChat("Now that was quite the show!", "I haven't been that entertained in a long time.")
			.sendNpcChat("Of course, you know I can't let you leave here alive.", "Time for your final performance...")
			.sendOption("Is your party ready to fight?", "Yes, let's begin.", () -> {
				c.getDialogueBuilder().sendPlayerChat("Yes, let's begin.").sendNpcChat("Oh I'm going to enjoy this...").sendAction(() -> {
					if (c.tobManager == null || (c.tobManager.currentRoom != null && !(c.tobManager.currentRoom instanceof VerzikRoom))) {
						return;
					}

					VerzikRoom verzikRoom = (VerzikRoom) c.tobManager.currentRoom;
					verzikRoom.getVerzik().phase1();
				});
			}, "No, don't start yet.", () -> {
				c.getDialogueBuilder().sendPlayerChat("No, don't start yet.");
			}).execute();
			break;
		case 8971:
			npc.startAnimation(27950);
			npc.faceLocation(8672, 4058);
			final int x = npc.getX(), y = npc.getY(), z = npc.getZ();
			npc.removeNpcSafe(true);
			
			final NPC boss = NPCHandler.spawnNpc(c, 8970, x, y, z, 0, false, false);
			boss.startAnimation(27950);
			boss.setAttackable(false);
			
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					boss.setAttackable(true);
				
				}
					
			}, 7);
			break;
		case 1866:
			c.getShops().openShop(65);
			c.sendMessage("<col=ff0000>You currently have " + c.lmsPoints + " LMS Points");
			break;
		case 3921:
			c.getShops().openShop(63);
			break;
		case 4312:
			c.getShops().openShop(62);
			break;
	    case 9712:
	    	DungeoneeringTutorDialogue.dungTutorChat(c, npcType);
		    break;
		case 560:
			c.getShops().openShop(59);
			break;
		case 106964:
			c.getShops().openShop(79);
			break;
			
		case 9711:
			RewardTraderDialogue.rewardTraderChat(c);
			break;
		
		case 2240: // grand exchange workers
		case 2241: // grand exchange workers
			c.getDialogueBuilder()
				.sendNpcChat(npcType, "Welcome to SoulPlay Exchange!", "How may I help you?")
				.sendOption("Open Player Owned Shop Manager", ()-> {
					c.getPacketSender().showInterface(45000);
				}, "Open Trading Post(Beta)", ()-> {
					if (c.getStopWatch().checkThenStart(1))
						BuyOffers.openPublicBuyOffers(c, 0);
				}, "Check Unclaimed Items", ()-> {
					c.getClaimItems().openInterface();
				}, "What is this place?", ()-> {
					c.getDialogueBuilder().sendNpcChat(npcType,
							"You can open up the market interface to search",
							"for items in player owned shops or",
							"you can explore Purchase offers made by players.")
					.setClose(true);
				}).execute(false);
			break;
		
			case 4247:
				c.getDialogueBuilder().sendNpcChat(npcType, "Hello. Welcome to the SoulPlay Housing Agency!", "What can I do for you?")
						.sendOption("I would like to purchase a house.", () -> {
							if (c.playerHasHouse()) {
								c.getDialogueBuilder().sendNpcChat(npcType, "You already have a house!");
								return;
							}

							c.getDialogueBuilder().sendNpcChat(npcType, "Would you like to purchase land for 10 Million coins?")
									.sendOption("Yes", () -> {
										if (c.playerHasHouse()) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You already have a house!");
											return;
										}

										if (!c.getItems().takeCoins(10_000_000)) {
											c.getDialogueBuilder().sendNpcChat(npcType, "Come back when you have 10 Million coins.");
											return;
										}

										c.getPOH().buyHouse();
										c.getDialogueBuilder().sendNpcChat(npcType, "When you're ready to visit your house", "enter the portal right over there.");
									}, "No", () -> {

									});
						}, "Can you redecorate my house please?", () -> {
							c.getDialogueBuilder().sendPlayerChat("Can you redecorate my house please?")
									.sendNpcChat(npcType, "Certainly. My magic can rebuild the house in a", "completely new style! What style would you like?")
									.createCheckpoint()
									.sendOption(String.format("Basic wood (%d)", HouseStyle.BASIC_WOOD.getCost()), () -> {

										HouseStyle style = HouseStyle.BASIC_WOOD;

										if (c.getPOH().getHouseStyle() == style) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You already have this house style!");
											return;
										}

										final int cost = style.getCost();

										if (!c.getItems().playerHasItem(995, cost)) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You do not have enough coins to purchase this.");
											return;
										}

										c.getItems().deleteItem2(995, cost);
										c.getPOH().setHouseStyle(style);
										c.getDialogueBuilder().sendNpcChat(npcType, "Your house style is now Basic wood.");

									}, String.format("Basic stone (%d)", HouseStyle.BASIC_STONE.getCost()), () -> {
										HouseStyle style = HouseStyle.BASIC_STONE;

										if (c.getPOH().getHouseStyle() == style) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You already have this house style!");
											return;
										}

										final int cost = style.getCost();

										if (!c.getItems().playerHasItem(995, cost)) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You do not have enough coins to purchase this.");
											return;
										}

										c.getItems().deleteItem2(995, cost);
										c.getPOH().setHouseStyle(style);
										c.getDialogueBuilder().sendNpcChat(npcType, "Your house style is now Basic stone.");
									}, String.format("Whitewashed stone (%d)", HouseStyle.WHITEWASHED_STONE.getCost()), () -> {
										HouseStyle style = HouseStyle.WHITEWASHED_STONE;

										if (c.getPOH().getHouseStyle() == style) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You already have this house style!");
											return;
										}

										final int cost = style.getCost();

										if (!c.getItems().playerHasItem(995, cost)) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You do not have enough coins to purchase this.");
											return;
										}

										c.getItems().deleteItem2(995, cost);
										c.getPOH().setHouseStyle(style);
										c.getDialogueBuilder().sendNpcChat(npcType, "Your house style is now Whitewashed stone.");
									}, String.format("Fremennik-style wood (%d)", HouseStyle.FREMENNIK_STYLE_WOOD.getCost()), () -> {
										HouseStyle style = HouseStyle.FREMENNIK_STYLE_WOOD;

										if (c.getPOH().getHouseStyle() == style) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You already have this house style!");
											return;
										}

										final int cost = style.getCost();

										if (!c.getItems().playerHasItem(995, cost)) {
											c.getDialogueBuilder().sendNpcChat(npcType, "You do not have enough coins to purchase this.");
											return;
										}

										c.getItems().deleteItem2(995, cost);
										c.getPOH().setHouseStyle(style);
										c.getDialogueBuilder().sendNpcChat(npcType, "Your house style is now Fremennik-style wood.");
									}, "More...", () -> {
										c.getDialogueBuilder().sendOption(String.format("Tropical wood (%d)", HouseStyle.TROPICAL_WOOD.getCost()), () -> {
											HouseStyle style = HouseStyle.TROPICAL_WOOD;

											if (c.getPOH().getHouseStyle() == style) {
												c.getDialogueBuilder().sendNpcChat(npcType, "You already have this house style!");
												return;
											}

											final int cost = style.getCost();

											if (!c.getItems().playerHasItem(995, cost)) {
												c.getDialogueBuilder().sendNpcChat(npcType, "You do not have enough coins to purchase this.");
												return;
											}

											c.getItems().deleteItem2(995, cost);
											c.getPOH().setHouseStyle(style);
											c.getDialogueBuilder().sendNpcChat(npcType, "Your house style is now tropical wood.");
										}, String.format("Fancy stone (%d)", HouseStyle.FANCY_STONE.getCost()), () -> {
											HouseStyle style = HouseStyle.FANCY_STONE;

											if (c.getPOH().getHouseStyle() == style) {
												c.getDialogueBuilder().sendNpcChat(npcType, "You already have this house style!");
												return;
											}

											final int cost = style.getCost();

											if (!c.getItems().playerHasItem(995, cost)) {
												c.getDialogueBuilder().sendNpcChat(npcType, "You do not have enough coins to purchase this.");
												return;
											}

											c.getItems().deleteItem2(995, cost);
											c.getPOH().setHouseStyle(style);
											c.getDialogueBuilder().sendNpcChat(npcType, "Your house style is now Fancy stone.");
										}, "...Previous", () -> {
											c.getDialogueBuilder().returnToCheckpoint();
										});
									});
						}).execute();
				break;
		
		case DropParty.PARTY_PETE_NPC_ID:
			if (DropParty.isLockedLever())
				c.getDialogueBuilder().sendNpcChat(DropParty.PARTY_PETE_NPC_ID, "The mods have locked the Lever for an event!", "Are you ready for it?!?!?!");
			c.getDialogueBuilder().sendNpcChat(DropParty.PARTY_PETE_NPC_ID, "This party is lit!").execute();
			break;
			
		case TamingQuestHandler.TAMING_MASTER:
			 TamingQuestHandler.startTamingQuest(c, npc);
		     break;
		
		    case 8591:
		    	c.getDH().sendDialogues(8591, c.npcType);
		    	break;
		    case 5029:
		    	c.getDH().sendDialogues(5029, c.npcType);
		    	break;
		    	
		    case 5030:
				c.getDH().sendNpcChat1("Would you like to watch the guide on youtube?", 5030, "Captain Cain");
				c.nextChat = 5031;
		    	break;
		    	
			case 8174:
				if (WorldType.equalsType(WorldType.SPAWN)) {
					c.getShops().openShop(159);
				}
				break;
			case 585: // halloween rommik
				c.getDH().sendDialogues(5850, c.npcType);
				break;
			case 336: // halloween da vinci
				c.getDH().sendDialogues(3360, c.npcType);
				break;
			case 340: // halloween hops
				c.getDH().sendDialogues(3400, c.npcType);
				break;
			case 338: // halloween chancy
				c.getDH().sendDialogues(33800, c.npcType);
				break;
			case 526: // halloween shopkeeper
				c.getDH().sendDialogues(5260, c.npcType);
				break;
			case 529: // halloween shop assistant
				c.getDH().sendDialogues(5290, c.npcType);
				break;
			case 307: // halloween hetty
				c.getDH().sendDialogues(3070, c.npcType);
				break;
			case 11699: // halloween gerrant
				c.getDH().sendDialogues(11699, c.npcType);
				break;
			case 557: // halloween wydin
				c.getDH().sendDialogues(5570, c.npcType);
				break;
			case 556: // halloween grum
				c.getDH().sendDialogues(5560, c.npcType);
				break;
			case 559: // halloween brian
				c.getDH().sendDialogues(5590, c.npcType);
				break;
			case 583: // halloween betty
				c.getDH().sendDialogues(5830, c.npcType);
				break;
			case 2692: // halloween ahab
				c.getDH().sendDialogues(26920, c.npcType);
				break;
			case 8078: // hallloween maggie
				c.getDH().sendDialogues(80780, c.npcType);
				break;
			case 8201: // halloween wendy
				c.getDH().sendDialogues(82010, c.npcType);
				break;
			case 3021:
				c.getDH().sendDialogues(3021, c.npcType);
				break;
			case 6892:
				c.getDH().sendDialogues(6893, c.npcType);
				break;
			case 2479:
				c.getDH().sendDialogues(2479, 2479);
				break;
			case 8002:
				c.getDH().sendDialogues(8002, c.npcType);
				break;
			case 958:
				c.getDH().sendDialogues(20795, 958);
				break;
			case 648:
				if (!c.isIronMan()) {
					if (c.getPA().getTotalLevel() >= 2496) {
						if (c.absX >= 3218 && c.absX <= 3226 && c.absY >= 3468
								&& c.absY <= 3479) {
							if (c.difficulty == PlayerDifficulty.NORMAL) {
								c.getDH().sendDialogues(6490, c.npcType);
							} else if (c.difficulty == PlayerDifficulty.PRESTIGE_ONE) {
								c.getDH().sendDialogues(6470, c.npcType);
							} else if (c.difficulty == PlayerDifficulty.PRESTIGE_TWO) {
								c.getDH().sendDialogues(6460, c.npcType);
							}
						} else {
							c.getDH().sendDialogues(6480, c.npcType);
						}
					} else {
						c.getDH().sendNpcChat1(
								"We have nothing to discuss yet.", c.npcType,
								"King Roald");
						c.dialogueAction = 0;
						c.nextChat = 0;
					}
				} else if (c.isIronMan()) {
					if (c.getPA().getTotalLevel() >= 1920) {
						c.getDH().sendDialogues(6465, c.npcType);
					} else {
						c.sendMessage(
								"You need a total level of 1920 to upgrade to super ironman");
					}
				}
				break;

			case 12228: // champion
				if (c.getItems().playerHasItem(20558, 3)) {
					c.getDH().sendDialogues(12229, c.npcType);
				} else {
					c.getDH().sendDialogues(12228, c.npcType);
				}
				break;

			case 13651: // easter event
				if (c.getItems().playerHasItem(12657, 15)) {
					c.getDH().sendDialogues(13777, c.npcType);
				} else if (c.getItems().playerHasItem(12656)) {
					c.getDH().sendDialogues(13751, c.npcType);
				} else {
					c.getDH().sendDialogues(13651, c.npcType);
				}
				break;

			case 7410: // easter event
				c.getDH().sendDialogues(7410, c.npcType);
				break;

			case 7115:
				c.getDH().sendDialogues(7114, 7115);
				break;

			case 8948:
				c.getDH().sendDialogues(8900, c.npcType);
				break;

			case 7605:
				c.getDH().sendDialogues(7605, c.npcType);
			break;

			case 3637:
				c.getDH().sendDialogues(3637, c.npcType);
				break;

			case 8091:
				c.getDH().sendDialogues(8091, c.npcType);
				break;

			case 463:
				c.getDH().sendDialogues(463, npcType);
				break;

			case 1512: // wildy key adverturer
				c.getDH().sendDialogues(1512, npcType);
				break;

			case 4251: // gardener supplier shop
				c.getShops().openShop(17);
				break;
			case 1552:
				if (c.getItems().hasAllMario()) {
					c.getDH().sendDialogues(5500, npcType);
				} else if (c.getItems().playerHasItem(946)) {
					c.getDH().sendDialogues(5200, npcType);
				} else {
					c.getDH().sendDialogues(5400, npcType);
				}
				break;
			case 3082:
				if (c.getItems().hasAllMari()) {
					c.getDH().sendDialogues(6300, npcType);
				} else if (c.getItems().playerHasItem(8794)) {
					c.getDH().sendDialogues(6200, npcType);
				} else if (c.getItems().playerHasItem(946)) {
					c.getDH().sendDialogues(6100, npcType);
				} else {
					c.getDH().sendDialogues(6000, npcType);
				}
				break;
			case 7935: // Hans
				c.getDH().sendDialogues(7935, c.npcType);
				break;
			case 882: // gypsy
				c.getDH().sendDialogues(882, c.npcType);
				break;
			case 8929: // lord marshal brogan
				c.getDH().sendDialogues(8929, c.npcType);
				break;
			case 1526:// castlewars ticket vendor
				c.getDH().sendDialogues(1526, npcType);
				// c.getShops().openShop(45);
				break;
			case 820:
				c.getDH().sendDialogues(820, c.npcType);
				break;
			case 437:
				c.getShops().openShop(43);
				c.sendMessage("You currently have <col=ff>" + c.getAgilityPoints()
						+ "</col> Agility Tokens.");
				break;
			case 262:
				c.getDH().sendDialogues(262, c.npcType);
				break;
			case 9713:
				c.getDialogueBuilder()
				.sendNpcChat("What you want with Thok?")
				.sendPlayerChat("Can I buy a Skillcape of Dungeoneering?")
				.sendNpcChat("Thok say he impressed. For being true master of skill you", "have option for two skillcapes.")
				.sendOption("Buy the level 99 skillcape", () -> {
					if (c.getSkills().getStaticLevel(Skills.DUNGEONEERING) < 99) {
						c.getDialogueBuilder().sendNpcChat("You need Dungeoneering level of 99 to buy skillcape.");
						return;
					}

					c.getDialogueBuilder().sendPlayerChat("Can I buy a Skillcape of Dungeoneering?")
					.sendNpcChat("Thok must ask for 99,000 coins for materials, though.")
					.sendOption("No that's too much", () -> {

					}, "Okay, that seems reasonable", () -> {
						if (c.getItems().freeSlots() <= 1) {
							c.sendMessage("Not enough space in your inventory.");
							return;
						}

						if (c.getItems().deleteItem2(995, 99_000)) {
							c.getItems().addItem(18508, 1);
							c.getItems().addItem(18510, 1);
						} else {
							c.sendMessage("You don't have enough money.");
						}
					});
				}, "Buy the level 120 skillcape", () -> {
					if (c.getSkills().getStaticLevel(Skills.DUNGEONEERING) < 120) {
						c.getDialogueBuilder().sendNpcChat("You need Dungeoneering level of 120 to buy skillcape.");
						return;
					}

					c.getDialogueBuilder().sendPlayerChat("Can I buy a master Skillcape of Dungeoneering?")
					.sendNpcChat("Thok must ask for 120,000 coins for materials, though.")
					.sendOption("No that's too much", () -> {

					}, "Okay, that seems reasonable", () -> {
						if (c.getItems().freeSlots() <= 0) {
							c.sendMessage("Not enough space in your inventory.");
							return;
						}

						if (c.getItems().deleteItem2(995, 120_000)) {
							c.getItems().addItem(19709, 1);
						} else {
							c.sendMessage("You don't have enough money.");
						}
					});
				})
				.execute();
				break;
			case 3374:
				c.getDH().sendOption2("Completionist Cape", "Max Cape");
				c.dialogueAction = 3373;
				break;
			case 14372:
				c.getDH().sendDialogues(14372, c.npcType);
				break;
			case 8009:
				c.getShops().openShop(100);
				break;
			case 8678:
				c.getDH().sendDialogues(8678, npcType);
				break;
			case 2253:
				c.getDH().sendDialogues(2253, npcType);
				break;
			case 2244:
				c.getDH().sendDialogues(2253, npcType);
				break;
			case 4289:
				WarriorsGuild.handleKamfreena(c, false);
				break;
			case 2320:
				TanHide.openTanning(c);
				break;
			case 13727: // loyalty points shop, xuan
				c.getDH()
						.sendNpcChat1("You currently have <col=ff>"
								+ c.getLoyaltyPoints() + "</col> LoyaltyPoints.",
								c.npcType, "King's messenger");
				c.nextChat = 1210;
				break;
			case 587:
				c.getDH().sendOption2(
						"Where can i get the ingredients for making potions",
						"Open herblore shop.");
				c.dialogueAction = 587;
				break;
			case 6201:
				c.getDH().sendDialogues(6201, npcType);
				break;
			case 4708:
				c.getDH().sendDialogues(4708, npcType);
				break;
			case 5497:
				c.getDH().sendDialogues(5497, npcType);
				break;
			case 11:
				c.getDH().sendDialogues(1111, npcType);
				break;
			case 957:
				c.getDH().sendDialogues(957, npcType);
				break;
			case 962:
			case 959:
			case 960:
			case 961:
				c.getDH().sendDialogues(959, npcType);
				break;
			case 1008:
				c.getDH().sendDialogues(1008, npcType);
				break;
			case 963:
				c.getDH().sendDialogues(963, npcType);
				break;
			case 652:
				c.getDH().sendDialogues(652, npcType);
				break;
			case 651:
				c.getDH().sendDialogues(651, npcType);
				break;
			case 649:
				c.getDH().sendDialogues(649, npcType);
				break;
			case 1152:
				c.getDH().sendDialogues(1152, npcType);
				break;
//			case 4250:
//				c.getDH().sendDialogues(4250, npcType);
//				break;
			case 665:
				c.getDH().sendDialogues(665, npcType);
				break;
			case 1840:
				c.getDH().sendDialogues(1840, npcType);
				break;

			case 605:
				if (FunPkTournament.isTournamentOpen()) {
					c.sendMessage("The tournament is currently running.");
					return;
				}
				if (c.playerRights >= 1) {
					c.dialogueAction = 778;
					c.getDH().sendOption2(
							"Start the donator FunPK tournament for "
									+ FunPkTournament.DONATORHOSTPRICE
									+ " Donator Points",
							"No Thanks.");

				} else {
					c.sendMessage(
							"This is a donator only feature for FunPK Tournaments.");
				}
				break;

			case 7852:
				c.getDH().sendOption2("Gnome agility course",
						"Barbarian agility course");
				c.dialogueAction = 7852;
				break;
			// c.getPA().startTeleport(2474, 3438, 0, "modern");
			// break;
			case 303: // Vote
				VoteInterface.open(c);
				/*c.getDH().sendOption2("Vote Tickets shop",
						"Loyalty Points shop");
				c.dialogueAction = 303;*/
				// c.getShops().openShop(25);
				break;
			case 576: // Harry - fishing shop
				c.getShops().openShop(26);
				break;
			case 5964: // Ed Wood - Woodcutting shop
				c.getShops().openShop(27);
				break;
			case 594: // Nurmof - Mining shop
				c.getShops().openShop(28);
				break;
			case 11674: // shopkeeper donor island
				c.getShops().openShop(21);
				break;
			case 944:
				c.getDH().sendOption5("Hill Giants", "Hellhounds",
						"Lesser Demons", "Chaos Dwarf", "-- Next Page --");
				c.teleAction = 7;
				break;
			case 659:
				c.getShops().openShop(11);
				break;

			case 528:
			case 563:
			case 522:
			case 530:
			case 591:
			case 532:
				if (c.isIronMan()) {
					c.getShops().openShop(101);
				} else if (!c.isIronMan()) {
					c.getShops().openShop(1);
				}
				break;

			case 3789:
				c.sendMessage("You currently have <col=ff0000>" + c.getPcPoints()
						+ "</col> Pest Control points.");
				break;

			case 13721:
				c.getDH().sendDialogues(13720, npcType);
				break;

			case 953: // Banker
			case 2574: // Banker
			case 166: // Gnome Banker
			case 1702: // Ghost Banker
			case 494: // Banker
			case 495: // Banker
			case 496: // Banker
			case 497: // Banker
			case 498: // Banker
			case 499: // Banker
			case 567: // Banker
			case 1036: // Banker
			case 1360: // Banker
			case 2163: // Banker
			case 2164: // Banker
			case 2354: // Banker
			case 2355: // Banker
			case 2568: // Banker
			case 2569: // Banker
			case 2570: // Banker
			case 9710: //dungeoneering banker
		case 13455: // Ashuelot reis banker
			c.getDialogueBuilder().sendNpcChat(npcType, "Good day. How may I help you?")
					.sendOption("I'd like to access my bank account, please.", () -> {
						c.getPA().openUpBank();
					}, "I'd like to check my PIN settings.", () -> {
						c.getBankPin().bankPinSettings();
						c.openBankPinInterface = true;
					}, "What is this place?", () -> {
						c.getDialogueBuilder().sendPlayerChat("What is this place?").sendNpcChat(npcType,
								"This is the bank of " + Config.SERVER_NAME + ".",
								"We have many branches in many towns.").setClose(true);
					}).execute(false);
			// c.getDH().sendDialogues(1013, c.npcType);
			break;
			case 1986:
				c.getDH().sendDialogues(2244, c.npcType);
				break;

			case 586:
				c.getDH().sendOption3("Stone statuette shop",
						"Golden statuette shop",
						"Jewelled diamond statuette shop");
				c.dialogueAction = 586;
				break;

			case 644:
				//c.getDH().sendDialogues(662, c.npcType);
				DonationInterface.open(c);
				c.sendMessage("You currently have <col=ff0000>" + c.getDonPoints()
						+ "</col> donation points.");
				break;

			case 2305:
				c.getShops().openShop(16);
				break;

			case 550:
				c.getShops().openShop(4);
				break;

			case 1785:
				c.getShops().openShop(40);
				break;

			case 1860:
				c.getShops().openShop(6);
				break;
				
			case 13281:
				c.getShops().openShop(67, ShopType.SCROLL);
				break;

			case 519:
				if (c.isIronMan()) {
					c.getShops().openShop(107);
				} else if (!c.isIronMan()) {
					c.getShops().openShop(7);
				}
				break;

			case 548:
			case 1301:
				//c.getDH().sendDialogues(69, c.npcType);
				c.getDialogueBuilder().reset();
				c.getDialogueBuilder().sendNpcChat(npcType, "How can I help?")
				.sendOption("Trade kebbit fur", ()-> {
					c.getDialogueBuilder().setClose(false);
					Falconry.openFurTradingDialogue(c, npcType);
				}, "Change clothes", ()-> {
					if (!c.isNaked()) {
						c.sendMessage(
								"You must remove your equipment before changing your appearence.");
						c.canChangeAppearance = false;
					} else {
						c.getPacketSender().showInterface(3559);
						c.canChangeAppearance = true;
					}
				}).execute();
				
				break;

			case 2676:
				if (!c.isNaked()) {
					c.sendMessage(
							"You must remove your equipment before changing your appearence.");
					c.canChangeAppearance = false;
				} else {
					c.getPacketSender().showInterface(3559);
					c.canChangeAppearance = true;
				}
				break;

			case 2258:
				c.getDialogueBuilder().sendOption("Teleport me to Runecrafting Abyss.", () -> {
					final Location loc = Misc.random(Runecrafting.RANDOM_ABYS_SPAWNS);
					c.getPA().startTeleport(loc.getX(), loc.getY(), 0, TeleportType.MODERN);
				}, "I want to stay here, thanks.", () -> {}).execute();
				break;

			case 546:
				c.getShops().openShop(5);
				c.getDH().sendOption4("Buy 1,000 barrage spells [2482k]",
						"Buy 1,000 vengeance spells [2484k]",
						"Buy 1,000 of all runes [1986k]", "Open mage shop");
				c.dialogueAction = 1658;
				break;

			case 549:
				c.getShops().openShop(3);
				break;

			case 541:
				c.getShops().openShop(2);
				break;

			case 198:
				c.getDH().sendDialogues(1980, npcType);
				// c.getShops().openSkillCape();
				break;

			case 6537:
				c.getDH().sendDialogues(6537, npcType);// mandrith
				break;
				
			case 970:
				c.getShops().openShop(64);
				break;
				
			case 883:
				InstanceInterface.open(c);
				break;

			/**
			 * Ikov firstClickNpc
			 */

			case 520:
				c.getShops().openShop(86);
				break;

			case 2463:
				c.getShops().openShop(72);
				break;

			case 278:
				c.getShops().openShop(73);
				break;

			case 682:
				c.getShops().openShop(75);
				break;

			case 1513:
				c.getShops().openShop(76);
				break;

			case 455:
				c.getShops().openShop(77);
				break;

			case 1878:
				c.getShops().openShop(90);
				break;

			case 5112:
				c.getShops().openShop(78);
				break;

			case 457:
				c.getShops().openShop(79);
				break;

			case 3299:
				c.getShops().openShop(80);
				break;

			case 945:
				c.getDH().sendDialogues(9450, c.npcType);
				break;

			case 14328:
				c.getDH().sendDialogues(14328, c.npcType);
				break;

			case 13945:
				c.getDH().sendDialogues(534, c.npcType);
				break;

			/*case 883:
				c.getShops().openShop(85);
				break;*/

			// SoulPvP firstclicknpc

			case 9174:
				c.getShops().openShop(150);
				break;

			case 13824:
				c.getShops().openShop(152);
				break;

			case 11303:
				c.getShops().openShop(153);
				break;

			case 9691:
				c.getShops().openShop(151);
				break;

			case 581:
				c.getShops().openShop(154);
				break;

			case 807:
				c.getShops().openShop(155);
				break;

			case 366:
				c.getShops().openShop(156);
				break;
				
			case 2306:
				c.getShops().openShop(158);
				break;

			/**
			 * Make over mage.
			 */

		}
	}


}
