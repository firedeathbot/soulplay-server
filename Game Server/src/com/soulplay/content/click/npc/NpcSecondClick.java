package com.soulplay.content.click.npc;

import com.soulplay.content.minigames.penguin.Penguin;
import com.soulplay.content.minigames.penguin.PenguinHandler;
import com.soulplay.content.minigames.penguin.PenguinUtil;
import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.skills.Fishing;
import com.soulplay.content.player.skills.Thieving;
import com.soulplay.content.player.skills.dungeoneeringv2.items.shop.DungeonRewardShop;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonNpcs;
import com.soulplay.content.shops.ShopType;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.net.packet.in.ClickNPC;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.NpcClickExtension;

public final class NpcSecondClick {

	private NpcSecondClick() {

	}

	public static void execute(Client c, int npcType) {
		final int npcIndex = c.npcClickIndex;
		
		ClickNPC.resetNpcClickVariables(c);

		if (c.isDead()) {
			return;
		}
		
		final NPC npc = NPCHandler.npcs[npcIndex];

		if (npc != null && !npc.isDead() && npc.isActive) {
			
			if (DungeonNpcs.handle2(c, npc)) {
				return;
			}
			
			if (npc.summonedFor > 0) {
				if (npc.summonedFor != c.getId()) {
					c.sendMessage("This isn't your to interact!.");
					c.getMovement().resetWalkingQueue();
					return;
				}

				if (npc.isPokemon) {
					if (npc.npcType == TamingQuestHandler.TAMING_GOBLIN) {
						PokemonButtons.deSpawn(c);
					} else {
						PokemonButtons.pickupPet(c, false);
					}
					return;
				} else {
					c.getSummoning().openBoB();
					c.getMovement().resetWalkingQueue();
					return;
				}
			}

			if (Thieving.pickpocketNPC(c, npcType)) {
				Thieving.attemptToPickpocket(c, npc);
				return;
			}
			
			npc.turnNpc(c.getX(), c.getY());
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, npc, InstanceInteractType.NPC, InstanceInteractIndex.SECOND)) {
			return;
		}

		/*
		 * if(Fishing.fishingNPC(c, npcType)) { Fishing.fishingNPC(c, 2,
		 * npcType); return; }
		 */

		for (Penguin penguin : PenguinHandler.PENGUINS) {
			if (npc == penguin.getNpc() && PenguinUtil.spyOn(penguin, c)) {
				return;
			}
		}

		final PluginResult<NpcClickExtension> pluginResult = PluginManager.search(NpcClickExtension.class, npcType);
		if (pluginResult.invoke(() -> pluginResult.extension.onNpcOption2(c, npc))) {
			return;
		}
		
		if (npc != null && !npc.isDead() && npc.isActive)
			if (Fishing.isFishingNPC(c, 2, npcType, npc.npcIndex)) {
				return;
			}

		switch (npcType) {
			
		case 7666:
			    c.getShops().openShop(80);
			break;
		case 303: // Vote
				c.getDH().sendOption2("Vote Tickets shop",
						"Loyalty Points shop");
				c.dialogueAction = 303;
				//c.getShops().openShop(25);
			break;
		case 106964:
			c.getShops().openShop(79);
			break;
		case 6537:
			c.getShops().openShop(8, ShopType.SCROLL);
			break;
		case 1866:
			c.getShops().openShop(65);
			c.sendMessage("<col=ff0000>You currently have " + c.lmsPoints + " LMS Points");
			break;
		case 970:
			c.getShops().openShop(64);
			break;
		case 3921:
			c.getShops().openShop(63);
			break;
		case 4312:
			c.getShops().openShop(62);
			break;	
		case 2520:
			c.getShops().openShop(61);
		    break;
		case 560:
			c.getShops().openShop(59);
			break;

		case 9711:
			DungeonRewardShop.open(c);
			break;
		case 8591:
			c.getDH().sendDialogues(8591, c.npcType);
			break;
		case 5030:
			c.getDH().sendNpcChat1("Would you like to watch the guide on youtube?", 5030, "Captain Cain");
			c.nextChat = 5031;
			break;
		case 5029:
			c.getDH().sendDialogues(5029, c.npcType);
			break;
		case 8948:
			c.getDH().sendStatement("Please use the item on me that you would like to open.");
			break;
		case 2240:
		case 2241:
			c.getPacketSender().showInterface(45000);
			break;
		case 6892:
			c.getShops().openShop(48);
			break;
		case 4251:
			c.getShops().openShop(17);
			break;
		case 11674: // shopkeepr donor island
			c.getShops().openShop(21);
			break;
		case 546:
			c.getShops().openShop(5);
			break;

		// case 3788:
		// c.getShops().openShop(18);
		// c.sendMessage("You currently have <col=ff0000>" + c.pcPoints
		// + "</col> Pest control points.");
		// break;
		case 3789:
			c.getShops().openShop(18);
			break;
		case 953: // Banker
		case 2574: // Banker
		case 166: // Gnome Banker
		case 1702: // Ghost Banker
		case 958: // fadli banker
		case 494: // Banker
		case 495: // Banker
		case 496: // Banker
		case 497: // Banker
		case 498: // Banker
		case 499: // Banker
		case 567: // Banker
		case 1036: // Banker
		case 1360: // Banker
		case 2163: // Banker
		case 2164: // Banker
		case 2354: // Banker
		case 2355: // Banker
		case 2568: // Banker
		case 2569: // Banker
		case 2570: // Banker
		case 9710: //dungeoneering banker
		case 13455: // Ashuelot reis banker
			c.getPA().openUpBank();
			break;

		case 1785:
			c.getShops().openShop(40);
			break;

		case 550:
			c.getShops().openShop(4);
			break;

		case 3796:
			c.getShops().openShop(6);
			break;

		case 1860:
			c.getShops().openShop(6);
			break;

		case 519:
			if (c.isIronMan()) {
				c.getShops().openShop(107);
			} else if (!c.isIronMan()) {
				c.getShops().openShop(7);
			}
			break;

		case 1301:
			c.getShops().openShop(63);
			break;
		case 548:
			c.getShops().openShop(44, ShopType.SCROLL);
			break;

		case 6970: // summoning shop
			c.getShops().openShop(39);
			break;

		case 587:
			c.getShops().openShop(37);
			break;

		case 13727: // loyalty points shop
			c.getShops().openShop(36);
			break;

		case 2258:
			c.sendMessage("I have nothing for sale.");
			// c.getPA().startTeleport(3039, 4834, 0, "modern"); //first
			// click
			// teleports second click open shops
			break;

		case 541:
			c.getShops().openShop(2);
			break;

		case 528:
		case 563:
		case 522:
		case 530:
		case 520:
		case 591:
		case 532:
			if (c.isIronMan()) {
				c.getShops().openShop(101);
			} else if (!(c.isIronMan())) {
				c.getShops().openShop(1);
			}
			break;

		case 2676:
			if (!c.isNaked()) {
				c.sendMessage("You must remove your equipment before changing your appearence.");
				c.canChangeAppearance = false;
			} else {
				c.getPacketSender().showInterface(3559);
				c.canChangeAppearance = true;
			}
			break;

		case 962:
		case 959:
		case 960:
		case 961:
			c.getSkills().restore();
			c.setFreezeTimer(-1);
			c.frozenBy = -1;
			c.sendMessage("You have been healed.");

		}
	}

}
