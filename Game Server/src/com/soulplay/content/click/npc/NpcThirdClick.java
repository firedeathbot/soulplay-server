package com.soulplay.content.click.npc;

import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.skills.dungeoneeringv2.Binds;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.net.packet.in.ClickNPC;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.NpcClickExtension;

public final class NpcThirdClick {
	
	private NpcThirdClick() {
		
	}
	
	public static void execute(Client c, int npcType) {
		final int npcIndex = c.npcClickIndex;

		ClickNPC.resetNpcClickVariables(c);

		if (c.isDead()) {
			return;
		}
		
		final NPC npc = NPCHandler.npcs[npcIndex];
		
		if (npc == null || !npc.isActive || npc.isDead())
			return;
		
		if (npc.summonedFor > 0) {
			if (npc.summonedFor != c.getId()) {
				c.sendMessage("This isn't your to interact!.");
				c.getMovement().resetWalkingQueue();
				return;
			}

			if (npc.isPokemon) {
				PokemonButtons.feedPet(c);
				return;
			}
		}

		npc.turnNpc(c.getX(), c.getY());
		final PluginResult<NpcClickExtension> pluginResult = PluginManager.search(NpcClickExtension.class, npcType);
		if (pluginResult.invoke(() -> pluginResult.extension.onNpcOption3(c, npc))) {
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, npc, InstanceInteractType.NPC, InstanceInteractIndex.THIRD)) {
			return;
		}

		switch (npcType) {
		
	 	    case 1866:
			    c.getShops().openShop(66);
			    c.sendMessage("<col=ff0000>You currently have " + c.lmsPoints + " LMS Points");
			break;
			
		    case 11226://open player binding
		    	Binds.open(c);
		    	break;
			case 8948:
				c.getPA().openUpBank();
				break;
			case 7605:
				PlayerOwnedShops.openPlayerShop(c, c);
				break;

			case 3021:
				c.getDH().sendDialogues(3022, c.npcType);
				break;

			case 8273:
				c.getShops().openShop(10);
				c.sendMessage("You currently have <col=ff0000>"
						+ c.getSlayerPoints().getTotalValue()
						+ "</col> slayer points.");
				break;

			case 1526:// castlewars ticket vendor
				c.getDH().sendDialogues(1526, npcType);
				break;

			case 548:
			case 1301:
				if (!c.isNaked()) {
					c.sendMessage(
							"You must remove your equipment before changing your appearence.");
					c.canChangeAppearance = false;
				} else {
					c.getPacketSender().showInterface(3559);
					c.canChangeAppearance = true;
				}
				break;
			case 8678:// polar bear chuck
				c.getShops().openShop(38);
				break;
			case 836:
				c.getShops().openShop(103);
				break;

			case 13727: // loyalty points clear title
				c.getDH().sendOption2("Yes i would like to remove my title.",
						"No thank you i would like to keep my current title.");
				c.dialogueAction = 1225;
				break;

		}
	}

}
