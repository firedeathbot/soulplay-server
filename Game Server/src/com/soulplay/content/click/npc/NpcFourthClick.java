package com.soulplay.content.click.npc;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.net.packet.in.ClickNPC;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.NpcClickExtension;

public final class NpcFourthClick {
	
	private NpcFourthClick() {
		
	}
	
	public static void execute(Client c, int npcType) {
		final int npcIndex = c.npcClickIndex;
		
		ClickNPC.resetNpcClickVariables(c);

		if (c.isDead()) {
			return;
		}

		final NPC npc = NPCHandler.npcs[npcIndex];
		
		if (npc == null || !npc.isActive || npc.isDead())
			return;
			
			
		npc.turnNpc(c.getX(), c.getY());

		final PluginResult<NpcClickExtension> pluginResult = PluginManager.search(NpcClickExtension.class, npcType);
		if (pluginResult.invoke(() -> pluginResult.extension.onNpcOption4(c, npc))) {
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, npc, InstanceInteractType.NPC, InstanceInteractIndex.FOURTH)) {
			return;
		}

		switch (npcType) {
			case 6823:
				if (npc.summonedFor > 0) {
					if (npc.summonedFor != c.getId()) {
						c.sendMessage("This isn't your to interact!.");
						c.getMovement().resetWalkingQueue();
						return;
					}

					if (!c.getSummoning().handleSpecialMoveCost(2)) {
						c.sendMessage("You don't have enough summoning special points to cure poison.");
						c.getMovement().resetWalkingQueue();
						return;
					}

					c.getDotManager().curePoison();
					c.sendMessage("Your poison has been cured.");
				}
				break;
			case 8929: // lord marshal brogan
				c.getDH().sendDialogues(8932, c.npcType);
				break;
		}
	}

}
