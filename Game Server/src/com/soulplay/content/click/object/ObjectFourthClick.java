package com.soulplay.content.click.object;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonObjects;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public final class ObjectFourthClick {
	
	public static void execute(Client c, GameObject clickedObject) {			

		// if (c.clickedObject)
		// return;
		// c.clickedObject = true;

		c.clickObjectType = 0;
		c.walkingToObject = false;
		
		final int objectId = clickedObject.getId();
		final int obX = clickedObject.getX();
		final int obY = clickedObject.getY();
	
		if (c.isDead()) {
			return;
		}
		
		final PluginResult<ObjectClickExtension> pluginResult = PluginManager.search(ObjectClickExtension.class, objectId);
		if (pluginResult.invoke(() -> pluginResult.extension.onObjectOption4(c, clickedObject))) {
			return;
		}
		
		if (Construction.handleFourthClickObject(c, objectId, obX, obY, c.getZ())) {
		    return;
		}
		
		if (c.playerIsInHouse) { //TODO: remove after construction objects are all added!
			c.sendMessage("This object isn't added to Poh yet! ERROR 411");
			return;
		}

		if (DungeonObjects.handle4(c, objectId, obX, obY)) {
			return;
		}

		final GameObject o = RegionClip.getGameObject(objectId, obX, obY,
				c.heightLevel);
		if (o == null) {
			c.sendMessage("ObjectFourthClick Object Null - Report Error");
			return;
		}
		
		Misc.faceObject(c, o.getX(), o.getY(), o.getSizeY());

		RSObject spawnedTemp = ObjectManager.getObject(obX, obY, c.heightLevel);
		if (spawnedTemp != null && spawnedTemp.getTick() == 0) { // do not let
																	// players
																	// click on
																	// despawned
																	// objects.
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, o, InstanceInteractType.OBJECT, InstanceInteractIndex.FOURTH)) {
			return;
		}

		// Construction.handleFifthObjectClick(obX, obY, objectType, c);
		switch (objectId) {
		
			default:
				if (c.playerRights == 3 || c.playerRights == 8 || c.isDev()) {
					System.out.println("objectClick5_" + objectId + "_" + obX
							+ "_" + obY);
				}
				break;
		}
	}

}
