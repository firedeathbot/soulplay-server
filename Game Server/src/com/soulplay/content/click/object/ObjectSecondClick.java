package com.soulplay.content.click.object;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.event.randomevent.ShootingStar;
import com.soulplay.content.items.impl.Flax;
import com.soulplay.content.minigames.castlewars.CastleWarsObjects;
import com.soulplay.content.minigames.soulwars.SWObjectHandler;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.skills.Thieving;
import com.soulplay.content.player.skills.crafting.Crafting;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonObjects;
import com.soulplay.content.player.skills.farming.FarmingManager;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.content.zone.kourend.KaruulmDungeon;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public final class ObjectSecondClick {
	
	private ObjectSecondClick() {
		
	}
	
	public static void execute(Client c, GameObject o) {
		c.clickObjectType = 0;
		c.walkingToObject = false;

		if (c.isDead()) {
			return;
		}

		final int objectType = o.getId();
		final int obX = o.getX();
		final int obY = o.getY();

		final PluginResult<ObjectClickExtension> pluginResult = PluginManager.search(ObjectClickExtension.class, objectType);
		if (pluginResult.invoke(() -> pluginResult.extension.onObjectOption2(c, o))) {
			return;
		}

		if (c.playerIsInHouse) { //TODO: remove after construction objects are all added!
		    c.sendMessage("This object isn't added to Poh yet! ERROR 211");
			return;
		}

		if (DungeonObjects.handle2(c, objectType, obX, obY)) {
			return;
		}

		Misc.faceObject(c, obX, obY, o.getSizeY());

		final ObjectDef def = ObjectDef.getObjectDef(objectType);
		if (def == null) {
			c.sendMessage("ObjectClickTwo Object Def Null - Report Error");
			return;
		}

		RSObject spawnedTemp = ObjectManager.getObject(o.getX(), o.getY(),
				o.getZ());
		if (spawnedTemp != null && spawnedTemp.getTick() == 0) { // do not let
																	// players
																	// click on
																	// despawned
																	// objects.
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, o, InstanceInteractType.OBJECT, InstanceInteractIndex.SECOND)) {
			return;
		}

		if (SWObjectHandler.handleSWSecondClickObject(c, objectType))
			return;

		if (FarmingManager.inspect(c, o.getX(), o.getY())) {
			return;
		}
		if (KaruulmDungeon.objectSecondClick(c, o)) {
			return;
		}
		if (ObjectSecondClick.secondClickObjectCases(c, o, def)) {
			return;
		}

		if (objectType == 882) {
			if (spawnedTemp != null) {
				spawnedTemp.setTick(0);
				return;
			}
		}

		if (objectType == 4187) { // climb down to ferminik trails quest
			c.sendMessage("not working yet");
			return;
		}

		c.walkToX = c.walkToY = -1;

		if (def.hasName()) {
			if (def.name.toLowerCase().equals("trapdoor")) {
				RSObject rso = ObjectManager.getObject(o.getX(), o.getY(),
						o.getZ());
				if (rso != null) {
					rso.setTick(0);
					c.startAnimation(827);
				}
				rso = null;
				return;
			}
			if (def.name.equals("Ladder") && !(objectType == 6501)) {
				c.getPA().movePlayerDelayed(c.getX(), c.getY(), c.getZ() + 1, 828, 1);
				
				TamingQuestHandler.spawnGoblinAfterMovingUpstairs(c, o.getLocation());
				return;
			}

			if (def.name.equals("Staircase")) {
				// c.sendMessage("xLength "+def.xLength()+" yLength
				// "+def.yLength());
//				c.sendMessage("Actions " + def.itemActions[1]);
				// int faceOffset[][] = { { 1, 1 }, { 1, -1 }, { -1, -1 },
				// { -1, 1 } };
				// if(def.xLength() == 1 && def.yLength() == 1 &&
				// def.itemActions[1].equals("Climb-down")) {
				// c.getPA().movePlayer(c.absX-faceOffset[o.getFace()][0],
				// c.absY-faceOffset[o.getFace()][1], c.heightLevel-1);
				// return;
				// }
				if (def.xLength() == 2 && def.yLength() == 2
						&& def.hasActions() && def.itemActions[1].equals("Climb-up")) {
					c.getPA().movePlayer(c.absX, c.absY, c.heightLevel + 1);
					return;
				}
			}
		}
	}

	private static boolean secondClickObjectCases(Client c, GameObject o, ObjectDef def) {

		final int obX = o.getX();
		final int obY = o.getY();
		final int objectType = o.getId();

		switch (objectType) {
			case 38660:
			case 38661:
			case 38662:
			case 38663:
			case 38664:
			case 38665:
			case 38666:
			case 38667:
			case 38668:
				ShootingStar.inspectStar(c, o);
				break;
			case 6:
				if (c.cannonStatus == 0) {
					return true;
				}
				if (c.cannonBalls > 0) {
					if (c.getItems().freeSlots() < 5) {
						c.sendMessage(
								"You don't have enough room in your Inventory.");
						return true;
					}
				}
				if (c.getItems().freeSlots() < 4) {
					c.sendMessage(
							"You don't have enough room in your Inventory.");
					return true;
				}
				RSObject rO = ObjectManager.getObject(obX, obY, c.heightLevel);
				if (rO == null) {
					return true;
				}
				if (rO.getObjectOwnerLong() == c.getLongName()) {
					rO.setTick(1);
					rO.setOriginalId(-1);
					c.getItems().addItem(6, 1);
					c.getItems().addItem(8, 1);
					c.getItems().addItem(10, 1);
					c.getItems().addItem(12, 1);
					if (c.cannonBalls > 0) {
						c.getItems().addItem(2, c.cannonBalls);
						c.cannonBalls = 0;
					}
					c.cannonStatus = 0;
					c.cannonRegion = null;
				}
				break;

			case 2644:
				Crafting.spinningWheel(c);
				break;

			case 17010:
				c.getPA().closeAllWindows();
				if (c.playerMagicBook == SpellBook.NORMAL || c.playerMagicBook == SpellBook.ANCIENT) {
					c.sendMessage("You switch spellbook to lunar magic.");
					c.getPA().resetAutocast();
					c.setSpellbook(SpellBook.LUNARS);
					break;
				}
				if (c.playerMagicBook == SpellBook.LUNARS) {
					c.sendMessage("You feel a drain on your memory.");
					c.getPA().resetAutocast();
					c.setSpellbook(SpellBook.NORMAL);
					break;
				}
				break;
			case 2781:
			case 26814:
			case 126300:
			case 11666:
			case 3044:
			case 21393:
			case 21303:
			case 124009: //osrs wild
				Smelting.sendSmelting(c);
				break;
			// new theiving stalls
			case 4876:
				Thieving.stealFromStall(c, o, 995, 11000, 60, 50,
						objectType, obX, obY, 2);
				break;
			case 4877:
				Thieving.stealFromStall(c, o, 995, 15000, 100, 75,
						objectType, obX, obY, 0);
				break;
			case 4875:
				Thieving.stealFromStall(c, o, 995, 6000, 10, 1, objectType,
						obX, obY, 0);
				break;
			case 4878:
				Thieving.stealFromStall(c, o, 995, 18000, 160, 90,
						objectType, obX, obY, 3);
				break;
			case 4874:
				Thieving.stealFromStall(c, o, 995, 9000, 30, 25, objectType,
						obX, obY, obX == 2667 ? 3 : 1);
				break;
			// end of new theiving stalls

			case 2565:
				Thieving.stealFromStall(c, o, 995, 11000, 54, 50, objectType,
						obX, obY, 2);
				break;
			case 2564:
				Thieving.stealFromStall(c, o, 995, 15000, 81, 65, objectType,
						obX, obY, 0);
				break;
			case 2563:
				Thieving.stealFromStall(c, o, 995, 9000, 36, 35, objectType,
						obX, obY, 0);
				break;
			case 2562:
				Thieving.stealFromStall(c, o, 995, 18000, 16, 75, objectType,
						obX, obY, 3);
				break;
			case 2561:
				Thieving.stealFromStall(c, o, 995, 6000, 16, 1, objectType, obX,
						obY, obX == 2667 ? 3 : 1);
				break;
			case 2560:
				Thieving.stealFromStall(c, o, 995, 1000, 24, 20, objectType,
						obX, obY, obX == 2662 ? 2 : 1);
				break;

			case 14011:
				Thieving.stealFromStall(c, o, 995, 1000, 10, 1, objectType, obX,
						obY, 3);
				break;
			case 7053:
				Thieving.stealFromStall(c, o, 995, 10000, 18, 10, objectType,
						obX, obY, obX == 3079 ? 2 : 1);
				break;
			case 4705:
				Thieving.stealFromStall(c, o, 995, 20000, 42, 42, objectType,
						obX, obY, obX == 3079 ? 2 : 1);
				break;
			case 4706:
				Thieving.stealFromStall(c, o, 995, 16000, 10, 2, objectType,
						obX, obY, obX == 3079 ? 2 : 1);
				break;
			case 4707:
				Thieving.stealFromStall(c, o, 995, 21000, 42, 42, objectType,
						obX, obY, obX == 3079 ? 2 : 1);
				break;
			case 4708:
				Thieving.stealFromStall(c, o, 995, 17000, 10, 2, objectType,
						obX, obY, obX == 3079 ? 2 : 1);
				break;
			case 2646:
				Flax.pickFlax(c, obX, obY);
				break;
			case 4458:
			case 4459:
			case 4460:
			case 4461:
			case 4462: // table with ropes
			case 4463:
			case 4464:
				CastleWarsObjects.handleObjectSecond(c, o);
				break;
			/**
			 * Opening the bank.
			 */
			case 2213:
			case 14367:
			case 11758:
			case 10517:
			case 26972:
			case 27718:
			case 27719:
			case 27720:
			case 27721:
			case 32666: // Tob
				c.getPA().openUpBank();
				break;

			default:
				return false;

		}
		return true;
	}

}
