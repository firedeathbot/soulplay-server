package com.soulplay.content.click.object;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ObjectDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.event.randomevent.ShootingStar;
import com.soulplay.content.items.Ammunition;
import com.soulplay.content.items.WeaponType;
import com.soulplay.content.items.impl.FairyRing;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.minigames.FightCaves;
import com.soulplay.content.minigames.FightPitsTournament;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.Sailing;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.castlewars.CastleWarsObjects;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.minigames.lastmanstanding.LmsArenaObjects;
import com.soulplay.content.minigames.lastmanstanding.LmsLobbyObjects;
import com.soulplay.content.minigames.partyroom.DropParty;
import com.soulplay.content.minigames.pestcontrol.PestDoorHandler;
import com.soulplay.content.minigames.soulwars.SWObjectHandler;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.ClueManager;
import com.soulplay.content.minigames.treasuretrails.ClueType;
import com.soulplay.content.minigames.treasuretrails.ObjectClue;
import com.soulplay.content.minigames.warriorsguild.WarriorsGuild;
import com.soulplay.content.objects.impl.DwarfMultiCannon;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.StrongholdOfSecurity;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.quests.tamingquest.TamingQuestHandler;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.Woodcutting;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.content.player.skills.agility.AgilityShortcuts;
import com.soulplay.content.player.skills.agility.courses.BarbarianCourse;
import com.soulplay.content.player.skills.agility.courses.WildyCourse;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.crafting.Pottery;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonObjects;
import com.soulplay.content.player.skills.farming.FarmingManager;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.content.player.skills.prayer.Prayer;
import com.soulplay.content.player.skills.runecrafting.Runecrafting;
import com.soulplay.content.player.skills.runecrafting.RunecraftingData;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.content.poll.PollManager;
import com.soulplay.game.LocationConstants;
import com.soulplay.content.raids.RaidsObjects;
import com.soulplay.content.zone.JormungandsPrison;
import com.soulplay.content.zone.fossilisland.LithkrenVault;
import com.soulplay.content.zone.gielinor.StrongholdSlayerCave;
import com.soulplay.content.zone.kourend.CatacombsOfKourend;
import com.soulplay.content.zone.kourend.KaruulmDungeon;
import com.soulplay.content.zone.kourend.LizardmanCaves;
import com.soulplay.content.zone.kourend.WoodcuttingGuild;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.dialog.impl.OptionDialogue;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.scoregrab.ClwRedPortalScoreBoard;
import com.soulplay.util.sql.scoregrab.WildyEloRatingSeason;

public final class ObjectFirstClick {

	private ObjectFirstClick() {
		
	}

	private final static int faceOffset[][] = {{1, 1}, {1, -1}, {-1, -1}, {-1, 1}};
	
	public static void execute(Client c, GameObject o) {
		c.clickObjectType = 0;
		c.walkingToObject = false;
		c.walkToX = c.walkToY = -1;
		c.finalLocalDestX = c.finalLocalDestY = 0;

		if (c.isDead()) {
			return;
		}

		final int clickedObjectId = o.getId();
		final int obX = o.getX();
		final int obY = o.getY();
		final int obZ = o.getZ();

		final PluginResult<ObjectClickExtension> pluginResult = PluginManager.search(ObjectClickExtension.class, clickedObjectId);
		if (pluginResult.invoke(() -> pluginResult.extension.onObjectOption1(c, o))) {
			return;
		}

		if (Construction.handleFirstObjectClick(c, clickedObjectId, obX, obY)) {
			return;
		}
		if (c.playerIsInHouse) { //TODO: remove after construction objects are all added!
			c.sendMessage("This object isn't added to Poh yet! ERROR 511");
			return;
		}
		if (DungeonObjects.handle1(c, clickedObjectId, obX, obY)) {
			return;
		}
		if (RaidsObjects.handle1(c, clickedObjectId, obX, obY)) {
			return;
		}
		if (LmsArenaObjects.searchLMSObjects(c, clickedObjectId, obX, obY, obZ)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (KaruulmDungeon.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (StrongholdSlayerCave.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (LithkrenVault.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (LizardmanCaves.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (JormungandsPrison.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (CatacombsOfKourend.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}
		if (WoodcuttingGuild.objectFirstClick(c, o)) {
			Misc.faceObject(c, obX, obY, o.getSizeY());
			return;
		}

		if (!o.isActive()) {
			c.sendMessage("ObjectClickOne Object Null - Report Error");
			return;
		}
		// if (o.getId() != objectType) // stop false object clicking
		// return;

		// c.sendMessage("clipping "+PathFinder.foundRoute(c, o.getX(),
		// o.getY()));

		RSObject spawnedTemp = ObjectManager.getObject(o.getX(), o.getY(),
				o.getZ());
		if (spawnedTemp != null && spawnedTemp.getTick() == 0) { // do not let
																	// players
																	// click on
																	// despawned
																	// objects.
			return;
		}
		// c.sendMess("X:"+o.getX() + " Y:" + o.getY());
		// c.sendMess("Face:"+ o.getFace());
		Misc.faceObject(c, o.getX(), o.getY(), o.getSizeY());

		final ObjectDef def = ObjectDef.getObjectDef(clickedObjectId);
		if (def == null) {
			c.sendMessage("ObjectClickOne Object Def Null - Report Error");
			return;
		}

//		if (!c.getPA().canUseObjectFromLoc(o, c.finalLocalDestX,
//				c.finalLocalDestY)) {
//			return;
//		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, o, InstanceInteractType.OBJECT, InstanceInteractIndex.FIRST)) {
			return;
		}

		if (AgilityShortcuts.handleObject(c, o)) {
			return;
		}
		
		if (LmsLobbyObjects.searchLMSObjects(c, o)) {
			return;
		}

		if (FarmingManager.harvest(c, o.getX(), o.getY())) {
			return;
		} else if (FarmingManager.checkHealth(c, o.getX(), o.getY())) {
			return;
		} else if (FarmingManager.clear(c, -1, o.getX(), o.getY())) {
			return;
		} else if (FarmingManager.chopDown(c, o.getX(), o.getY())) {
			return;
		} else if (ClanWarHandler.handleObjects(c, o)) {
			return;
		}
		
		if (DropParty.usingPartyObject(c, o))
			return;

		if (firstClickObjectCases(c, o, def)) {
			return;
		}

		if (Woodcutting.initNormalWc(c, o)) {
			return;
		}
		
		if (Mining.attemptData(c, o)) {
			return;
		}
		
		if (ObjectClue.entries.contains(o.getId())) {
			for (ClueDifficulty difficulty : ClueDifficulty.values) {
				int data = c.getTreasureTrailManager().getCluesData()[difficulty.getIndex()];
				if (data == -1) {
					continue;
				}

				ClueType clueType = ClueManager.clues.get(difficulty.getIndex()).get(Misc.getFirst(data)).get(Misc.getSecond(data));
				if (!(clueType instanceof ObjectClue)) {
					continue;
				}

				clueType.checkComplete(c, difficulty);
			}
		}
		
		if (c.getAgility().agilityObstacle(c, clickedObjectId)) {
			if (o.getId() == 2286 || o.getId() == 2285 || o.getId() == 2313
					|| o.getId() == 2314) {
				c.startAnimation(828);
			}
			Server.getTaskScheduler().schedule(new Task() {

				@Override
				protected void execute() {
					if (c != null && c.isActive) {
						c.getAgility().agilityCourse(c, clickedObjectId, o);
					}
					this.stop();
				}
			});
			return;
		}

		if (def.hasName() && def.name.equals("Trapdoor")
				&& (clickedObjectId != 4472 && clickedObjectId != 4471
						&& clickedObjectId != 10804 && clickedObjectId != 10699)) {
			if (def.itemActions[0] != null
					&& def.itemActions[0].equals("Climb-down")) {
				// c.startAnimation(828);

				if (o.getId() == 5268) { // ectofunctus ladder climbing
											// underground
					c.getPA().movePlayerDelayed(3669, 9888, 3, 828, 1);
					return;
				}

				if (o.getX() == 2764 && o.getY() == 2769) {
					c.getPA().movePlayerDelayed(2765, 9165, 0, 828, 1);
					return;
				}
				if (o.getX() == 3013 && o.getY() == 3179) {
					c.getPA().movePlayerDelayed(3014, 3179, 1, 828, 1);
					return;
				}
				if (c.getX() > 3500) { // canfis +6 thingy
					if (c.getY() < 6400 && (c.heightLevel & 3) == 0) {
						c.getPA().movePlayerDelayed(c.getX() + 6,
								c.getY() + 6402, c.heightLevel, 828, 1);
						return;
					} else {
						c.getPA().movePlayerDelayed(c.absX, c.absY,
								c.heightLevel - 1, 828, 1);
						return;
					}
				} else {
					c.getPA().movePlayerDelayed(c.getX(), c.getY() + 6400,
							c.heightLevel, 828, 1);
					return;
				}
			}
			/*if (def.itemActions[0] != null
					&& def.itemActions[0].equals("Open")) {
				if (clickedObjectId == 1568) {
					c.startAnimation(827);
					ObjectManager.addObject(new RSObject(o.getId() + 2, o.getX(), o.getY(), o.getZ(),
							o.getOrientation(), o.getType(), o.getId(), 30));
					return;
				}
				c.startAnimation(827);
				ObjectManager.addObject(new RSObject(o.getId() + 1, o.getX(), o.getY(), o.getZ(),
						o.getOrientation(), o.getType(), o.getId(), 30));
				return;
			}*/
			// TODO: add open trapdoor, actions[0].equals open
		}

		/*
		 * if (def.hasName() && (def.name.equals("Door") ||
		 * def.name.equals("Gate") || def.name.equals("Large door"))) { if
		 * (o.getId() == 6919 || o.getId() == 6920) { return; } if (!(o.getId()
		 * >= 4420 && o.getId() <= 4470)) { new RSObject(-1, o.getX(), o.getY(),
		 * o.getZ(), o.getFace(), o.getType(), o.getId(), 30); return; } }
		 */

		if (def.hasName() && def.name.equals("Ladder") && !(clickedObjectId == 6501)
				&& !(clickedObjectId == 15116) && !(clickedObjectId == 15115)
				&& !(clickedObjectId == 10707) && !(clickedObjectId == 37684)) {
			if (o.getId() == 1752 || o.getId() == 16736) { // broken looking
															// ladder
				return;
			}
			
			if (o.getId() == 10216 && o.getX() == 1890 && o.getY() == 4408 && o.getZ() == 0) {
				c.getPA().movePlayerDelayed(1890, 4406, 1, 828, 1);
				return;
			}

			if (o.getId() == 5264) { // ectofunctus ladder climbing aboveground
				c.getPA().movePlayerDelayed(3654, 3519, 0, 828, 1);
				return;
			}

			if (o.getId() == 10322) { // ladder located at ardongue going
										// underground
				return;
			}

			if (o.getId() == 4158) { // some ladder at rock crabs area needs
										// look at
				c.getPA().movePlayerDelayed(2665, 10036, 0, 828, 1);
				return;
			}

			if (o.getId() == 16148) { // ladder too stronghold of security floor
										// 2
				c.getPA().movePlayerDelayed(3081, 3421, 0, 828, 1);
				return;
			}

			if (o.getId() == 16080) { // ladder too stronghold of security floor
										// 1
				c.getPA().movePlayerDelayed(1902, 5223, 0, 828, 1);
				return;
			}

			if (o.getId() == 16149) { // ladder at stronghold of security floor
										// 2
				if (c.getStrongholdLevel() >= 1) {
					c.getPA().movePlayerDelayed(2042, 5245, 0, 828, 1);
				} else {
					c.sendMessage(
							"You must open the chest before going to the next floor.");
				}
				return;
			}

			if (o.getId() == 16081) { // ladder too stronghold of security floor
										// 3
				if (c.getStrongholdLevel() >= 2) {
					c.getPA().movePlayerDelayed(2122, 5251, 0, 828, 1);
				} else {
					c.sendMessage(
							"You must search the grain before going to the next floor.");
				}
				return;
			}

			if (o.getId() == 10707 || o.getId() == 10708) {
				if (def.hasActions() && def.itemActions[0].equals("Climb-up")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 2),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 2),
							c.getHeightLevel() + 1, 828, 1);
					return;
				}
				if (def.hasActions()
						&& def.itemActions[0].equals("Climb-down")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][1] * 2),
							c.getY() - (Misc.DIRECTION[o.getOrientation()][0] * 2),
							c.getHeightLevel() - 1, 828, 1);
					return;
				}
			}

			if (o.getId() == 24361 || o.getId() == 24362) { // ladders like in
															// varrock who need
															// to move you two
															// squares to the
															// side
				if (def.hasActions() && def.itemActions[0].equals("Climb-up")) {
					c.getPA().movePlayerDelayed(
							c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 2),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 2),
							c.getHeightLevel() + 1, 828, 1);
					return;
				}
				if (def.hasActions()
						&& def.itemActions[0].equals("Climb-down")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 2),
							c.getY() - (Misc.DIRECTION[o.getOrientation()][1] * 2),
							c.getHeightLevel() - 1, 828, 1);
					return;
				}
			}

			if (o.getId() >= 22121 && o.getId() <= 22122) {
				if (def.hasActions() && def.itemActions[0].equals("Climb-up")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 1),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 1),
							c.getHeightLevel() + 1, 828, 1);
					return;
				}
				if (def.hasActions()
						&& def.itemActions[0].equals("Climb-down")) {
					c.getPA().movePlayerDelayed(
							c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 1),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 1),
							c.getHeightLevel() - 1, 828, 1);
					return;
				}
			}

			if (o.getId() >= 10227 && o.getId() <= 10228) {
				if (def.hasActions() && def.itemActions[0].equals("Climb-up")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 1),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 1),
							c.getHeightLevel() + 1, 828, 1);
					return;
				}
				if (def.hasActions()
						&& def.itemActions[0].equals("Climb-down")) {
					c.getPA().movePlayerDelayed(
							c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 1),
							c.getY() - (Misc.DIRECTION[o.getOrientation()][1] * 1),
							c.getHeightLevel() - 1, 828, 1);
					return;
				}
			}

			if (o.getId() == 10193) {
				c.getPA().movePlayerDelayed(2401, 3888, 0, 828, 1);
				return;
			}
			if (o.getId() == 10194) {
				c.getPA().movePlayerDelayed(2316, 3893, 0, 828, 1);
				return;
			}
			if (o.getId() == 10198) {
				c.getPA().movePlayerDelayed(
						c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 2),
						c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 2),
						c.getHeightLevel() + 1, 828, 1);
				return;
			}
			if (o.getId() == 10199) {
				c.getPA().movePlayerDelayed(1834, 4387, 2, 828, 1);
				return;
			}
			if (o.getId() == 10200) {
				c.getPA().movePlayerDelayed(1834, 4390, 3, 828, 1);
				return;
			}
			if (o.getId() == 10201) {
				c.getPA().movePlayerDelayed(1809, 4394, 1, 828, 1);
				return;
			}
			if (o.getId() == 10202) {
				c.getPA().movePlayerDelayed(1812, 4394, 2, 828, 1);
				return;
			}
			if (o.getId() == 10203) {
				c.getPA().movePlayerDelayed(
						c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 3),
						c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 3),
						c.getHeightLevel() + 1, 828, 1);
				return;
			}
			if (o.getId() == 10204) {
				c.getPA().movePlayerDelayed(
						c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 3),
						c.getY() - (Misc.DIRECTION[o.getOrientation()][1] * 3),
						c.getHeightLevel() - 1, 828, 1);
				return;
			}
			if (o.getId() >= 10190 && o.getId() <= 10208) {
				if (def.hasActions() && def.itemActions[0].equals("Climb-up")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 3),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 3),
							c.getHeightLevel() + 1, 828, 1);
					return;
				}
				if (def.hasActions()
						&& def.itemActions[0].equals("Climb-down")) {
					c.getPA().movePlayerDelayed(
							c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 2),
							c.getY() - (Misc.DIRECTION[o.getOrientation()][1] * 2),
							c.getHeightLevel() - 1, 828, 1);
					return;
				}
			}
			if (o.getId() >= 10209 && o.getId() <= 10222) {
				if (def.hasActions() && def.itemActions[0].equals("Climb-up")) {
					c.getPA().movePlayerDelayed(
							c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 3),
							c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 3),
							c.getHeightLevel() + 1, 828, 1);
					return;
				}
				if (def.hasActions()
						&& def.itemActions[0].equals("Climb-down")) {
					c.getPA().movePlayerDelayed(
							c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 3),
							c.getY() - (Misc.DIRECTION[o.getOrientation()][1] * 3),
							c.getHeightLevel() - 1, 828, 1);
					return;
				}
			}
			if (o.getId() == 10217 || o.getId() == 10214 || o.getId() == 10211
					|| o.getId() == 10210) {
				c.getPA().movePlayerDelayed(
						c.getX() + (Misc.DIRECTION[o.getOrientation()][0] * 3),
						c.getY() + (Misc.DIRECTION[o.getOrientation()][1] * 3),
						o.getZ() + 1, 828, 1);
				return;
			}
			if (o.getId() == 10218 || o.getId() == 10213 || o.getId() == 10212
					|| o.getId() == 10209) {
				c.getPA().movePlayerDelayed(
						c.getX() - (Misc.DIRECTION[o.getOrientation()][0] * 3),
						c.getY() - (Misc.DIRECTION[o.getOrientation()][1] * 3),
						o.getZ() - 1, 828, 1);
				return;
			}
			if (o.getId() == 21514) { // ladders that climb up and to the side
				if (o.getOrientation() == 1) {
					c.getPA().movePlayerDelayed(c.getX() - 2, c.getY(), 1, 828,
							1);
				} else {
					c.sendMessage("Not yet added, notify Owner - 332");
				}
				return;
			}
			if (o.getId() == 21515) { // ladders that climb up and to the side
				if (o.getOrientation() == 1) {
					c.getPA().movePlayerDelayed(c.getX() + 2, c.getY(), 0, 828,
							1);
				} else {
					c.sendMessage("Not yet added, notify Owner - 333");
				}
				return;
			}
			if (o.getId() == 21512) {
				// c.startAnimation(828);
				c.getPA().movePlayerDelayed(2364, 3799, 2, 828, 1);
				c.sendMessage("You climb up the ladder");
				return;
			}
			if (o.getId() == 21513) {
				// c.startAnimation(828);
				c.getPA().movePlayerDelayed(2362, 3799, 0, 828, 1);
				c.sendMessage("You climb down the ladder");
				return;
			}
			if (o.getId() == 10230) {
				// c.startAnimation(828);
				c.getPA().movePlayerDelayed(2900, 4449, 0, 828, 1);
				c.sendMessage("You climb down the ladder");
				return;
			}
			if (o.getId() == 10229) {
				// c.startAnimation(828);
				c.getPA().movePlayerDelayed(1912, 4367, 0, 828, 1);
				c.sendMessage("You climb up the ladder");
				return;
			}
			if (o.getId() == 5493) {
				// c.startAnimation(828);
				c.getPA().movePlayerDelayed(3166, 3251, 0, 828, 1);
				c.sendMessage("You climb up the ladder");
				return;
			}
			// c.sendMessage("Action "+def.itemActions[1]);
			if (def.itemActions[1] != null) {
				c.getDialogueBuilder().sendOption("Climb up", () -> {
					c.getPA().movePlayerDelayed(c.absX,
							c.absY, c.heightLevel + 1, 828, 1);
					
					TamingQuestHandler.spawnGoblinAfterMovingUpstairs(c, o.getLocation());
					
				}, "Climb down", () -> {
					c.getPA().movePlayerDelayed(c.absX, c.absY, c.heightLevel - 1, 828, 1);
				}).execute(); 
				return;
			}
			// c.startAnimation(828);
			if (o.getId() == 1755 && o.getX() == 3405 && o.getY() == 9907) {
				c.getPA().movePlayerDelayed(3405, 3506, 0, 828, 1);
				return;
			}
			if (def.itemActions[0].equals("Climb-up")) {
				if (obX == 3069 && obY == 10256) {
					// go above ground, to the demons
					c.getPA().movePlayerDelayed(3017, 3850, 0, 828, 1);
					return;
				}
				if (obX == 3103 && obY == 9576) { // wizard tower basement to upper floor
					c.getPA().movePlayerDelayed(3105, 3162, 0, 828, 1);
					return;
				}
				if (obX == 3017 && obY == 10249) {
					// go to above ground where demons are not at
					c.getPA().movePlayerDelayed(3069, 3857, 0, 828, 1);
					return;
				}
				if (c.getX() > 3500) { // canfis has to -6 or something
					if (c.getY() > 6400) {
						c.getPA().movePlayerDelayed(
								obX + Misc.DIRECTION[o.getOrientation()][0] - 6,
								obY + Misc.DIRECTION[o.getOrientation()][1] - 6402,
								c.heightLevel, 828, 1);
						// c.sendMessage("X "+obX+Misc.DIRECTION[o.getFace()][0]);
						return;
					} else {
						c.getPA().movePlayerDelayed(c.absX, c.absY,
								c.heightLevel + 1, 828, 1);
						return;
					}
				} else {
					if (c.getY() > 6400) {
						c.getPA().movePlayerDelayed(
								obX + Misc.DIRECTION[o.getOrientation()][0],
								obY + Misc.DIRECTION[o.getOrientation()][1] - 6400,
								c.heightLevel, 828, 1);
						// c.sendMessage("X "+obX+Misc.DIRECTION[o.getFace()][0]);
						return;
					} else {
						c.getPA().movePlayerDelayed(c.absX, c.absY,
								c.heightLevel + 1, 828, 1);
						return;
					}
				}
			}
			if (def.itemActions[0].equalsIgnoreCase("Climb-down")) {
				c.sendMessage("You climb down.");
				if (obX == 3104 && obY == 3162) { // wizard tower climb to basement
					c.getPA().movePlayerDelayed(3104, 9576, 0, 828, 1);
					return;
				}
				if (obX == 3017 && obY == 3849) {
					// go to lever spot
					c.getPA().movePlayerDelayed(3069, 10257, 0, 828, 1);
					return;
				}
				if (obX == 3069 && obY == 3856) {
					// Go to the island on the volcano
					c.getPA().movePlayerDelayed(3017, 10248, 0, 828, 1);
					return;
				}
				if (c.getX() > 3500) { // canfis +6 thingy
					if (c.getY() < 6400 && (c.heightLevel & 3) == 0) {
						c.getPA().movePlayerDelayed(c.getX() + 6,
								c.getY() + 6400, c.heightLevel, 828, 1);
						// c.sendMessage("X "+obX+Misc.DIRECTION[o.getFace()][0]);
						return;
					} else {
						c.getPA().movePlayerDelayed(c.absX, c.absY,
								c.heightLevel - 1, 828, 1);
						return;
					}
				} else {
					// c.sendMessage("poop");
					if (c.getY() < 6400 && (c.heightLevel & 3) == 0) {
						c.getPA().movePlayerDelayed(c.getX(), c.getY() + 6400,
								c.heightLevel, 828, 1);
						// c.sendMessage("X "+obX+Misc.DIRECTION[o.getFace()][0]);
						// c.sendMessage("poop");
						return;
					} else {
						// c.sendMessage("poop");
						c.getPA().movePlayerDelayed(c.absX, c.absY,
								c.heightLevel - 1, 828, 1);
						return;
					}
				}
			}
		}

		if (def.hasName() && (def.name.equals("Staircase")
				|| def.name.equals("Stairs"))) {
			// c.sendMessage("xLength "+def.xLength()+" yLength
			// "+def.yLength());
			if (clickedObjectId == 6372 && obX == 2721 && obY == 4884) {
				c.getPA().movePlayer(3491, 3090, 0);
				return;
			}
			if (clickedObjectId == 24360 && obX == 3187 && obY == 3433) {
				c.getPA().movePlayer(3190, 9833, 0);
				return;
			}
			if (clickedObjectId == 6373 && obX == 3492 && obY == 3090) { // above
																	// ground in
																	// desert
				c.getPA().movePlayer(2721, 4886, 0);
				return;
			}
			if (clickedObjectId == 1742) {
				c.getPA().movePlayer(2445, 3425, 1);
				return;
			}
			if (clickedObjectId == 1744) {
				c.getPA().movePlayer(2447, 3435, 0);
				return;
			}
			if (clickedObjectId == 1738) {
				if (o.getX() == 2839 && o.getY() == 3537 && o.getZ() == 0) {
					c.getPA().movePlayer(2840, 3539, 2);
					return;
				}
			}
			if (clickedObjectId == 1738) {
				c.getPA().movePlayer(c.absX, c.absY, c.heightLevel + 1);
				return;
			}
			if (clickedObjectId == 4494 && obX == 2884 && obY == 5272) {
				c.getPA().movePlayer(2856, 5221, 0);
				return;
			}
			if (clickedObjectId == 1739) {
				c.sendMessage("Send buttons to go up or down");
				return;
			}
			if (clickedObjectId == 1740) {
				c.getPA().movePlayer(c.absX, c.absY, c.heightLevel - 1);
				return;
			}

			if (def.xLength() == 2 && def.yLength() == 2 && def.itemActions[0].equals("Climb")) {
				c.getDialogueBuilder().sendOption("Climb up", () -> {
					c.getPA().movePlayerDelayed(c.absX,
							c.absY, c.heightLevel + 1, 828, 1);
				}, "Climb down", () -> {
					c.getPA().movePlayerDelayed(c.absX, c.absY, c.heightLevel - 1, 828, 1);
				}).execute(); 
				return;
			}
			
			if (def.xLength() == 2
					&& (def.yLength() == 3 || def.yLength() == 4)) { // Climb up
																		// staircase,
																		// usually
				if (def.itemActions[0].equals("Climb-up")
						|| def.itemActions[0].equals("Walk-up")) {
					if (c.getY() > 6400) { // from underground to upstairs
						c.getPA().movePlayer(
								c.absX + ((def.xLength(o.getOrientation()) + 1)
										* Misc.DIRECTION[o.getOrientation()][0]),
								c.absY + ((def.yLength(o.getOrientation()) + 1)
										* Misc.DIRECTION[o.getOrientation()][1]) - 6400,
								c.heightLevel);
						return;
					} else { // regular heigth change
						c.getPA().movePlayer(
								c.absX + ((def.xLength(o.getOrientation()) + 1)
										* Misc.DIRECTION[o.getOrientation()][0]),
								c.absY + ((def.yLength(o.getOrientation()) + 1)
										* Misc.DIRECTION[o.getOrientation()][1]),
								c.heightLevel + 1);
						return;
					}
				} else if (def.itemActions[0].equals("Climb-down")) {
					if (c.heightLevel == 0) { // going underground
						c.getPA().movePlayer(
								c.absX + (-(def.xLength(o.getOrientation()) + 1)
										* Misc.DIRECTION[o.getOrientation()][0]),
								c.absY + (-(def.yLength(o.getOrientation()) + 1)
										* Misc.DIRECTION[o.getOrientation()][1]) + 6400,
								c.heightLevel);
						return;
					}
				} /*
					 * else { switch(o.getFace()) { case 0: // move north
					 * c.getPA().movePlayer(c.absX, c.absY+(1+def.yLength()),
					 * c.heightLevel+1); break; case 1: // move west
					 * c.getPA().movePlayer(c.absX+(1+def.yLength()), c.absY,
					 * c.heightLevel+1); break; case 2: // move south
					 * c.getPA().movePlayer(c.absX, c.absY-(1+def.yLength()),
					 * c.heightLevel+1); break; case 3: // move east
					 * c.getPA().movePlayer(c.absX-(1+def.yLength()), c.absY,
					 * c.heightLevel+1); break; } return; }
					 */
			}
			if (def.xLength() == 2 && def.yLength() == 2
					&& def.itemActions[0].equals("Climb-down")) {
				if (clickedObjectId == 55 || clickedObjectId == 57) { // going underground
															// from staircase
															// that moves 3
															// squares to the
															// front
					switch (o.getOrientation()) {
						case 0: // move south
							c.getPA().movePlayerDelayed(c.absX,
									6400 + c.absY - (2 + def.yLength()),
									c.heightLevel, 0, 1);
							break;
						case 1: // move west
							c.getPA().movePlayerDelayed(
									c.absX - (2 + def.yLength()), 6400 + c.absY,
									c.heightLevel, 0, 1);
							break;
						case 2: // move north
							c.getPA().movePlayerDelayed(c.absX,
									6400 + c.absY + (2 + def.yLength()),
									c.heightLevel, 0, 1);
							break;
						case 3: // move east
							c.getPA().movePlayerDelayed(
									c.absX + (2 + def.yLength()), 6400 + c.absY,
									c.heightLevel, 0, 1);
							break;
					}
					return;
				}
				ObjectDef def2 = ObjectDef.getObjectDef(clickedObjectId - 1);
				if (def2.yLength() == 3 || def2.yLength() == 4) { // climb down
																	// staircase(the
																	// length of
																	// the
																	// stairs)
					switch (o.getOrientation()) {
						case 0: // move south
							c.getPA().movePlayerDelayed(c.absX,
									c.absY - (1 + def2.yLength()),
									c.heightLevel - 1, 0, 1);
							break;
						case 1: // move west
							c.getPA().movePlayerDelayed(
									c.absX - (1 + def2.yLength()), c.absY,
									c.heightLevel - 1, 0, 1);
							break;
						case 2: // move north
							c.getPA().movePlayerDelayed(c.absX,
									c.absY + (1 + def2.yLength()),
									c.heightLevel - 1, 0, 1);
							break;
						case 3: // move east
							c.getPA().movePlayerDelayed(
									c.absX + (1 + def2.yLength()), c.absY,
									c.heightLevel - 1, 0, 1);
							break;
					}
					return;
				}
			}

			// c.sendMess("Face "+o.getFace());

			if (def.xLength() == 1 && def.yLength() == 1
					&& def.itemActions[0].equals("Climb-down")) {
				c.getPA().movePlayer(c.absX - faceOffset[o.getOrientation()][0],
						c.absY - faceOffset[o.getOrientation()][1], c.heightLevel - 1);
				return;
			}
			if (def.xLength() == 2 && def.yLength() == 2
					&& def.itemActions[0].equals("Climb-up")) {
				c.getPA().movePlayer(c.absX + faceOffset[o.getOrientation()][0],
						c.absY + faceOffset[o.getOrientation()][1], c.heightLevel + 1);
				return;
			}

			if (def.xLength() == 1 && def.yLength() == 3
					&& def.itemActions[0].equals("Climb-up")
					&& (!(clickedObjectId == 6702)) && (!(clickedObjectId == 6703))
					&& (!(clickedObjectId == 6704)) && (!(clickedObjectId == 6705))
					&& (!(clickedObjectId == 6706)) && (!(clickedObjectId == 6707))) {
				if (clickedObjectId == 11724 || clickedObjectId == 1737) { // crazy one in
																	// falador
																	// palace
					int moveX = c.absX - faceOffset[o.getOrientation()][0];
					int moveY = c.absY - faceOffset[o.getOrientation()][1];
					switch (o.getOrientation()) {
						case 0:
							moveY -= 2;
							break;
						case 1:
							moveX -= 2;
							break;
						case 2:
							moveY += 2;
							break;
						case 3:
							moveX += 2;
							break;
					}
					c.getPA().movePlayer(moveX, moveY, c.heightLevel + 1);
					return;
				}
				if (clickedObjectId >= 6702 && clickedObjectId <= 6707) { // barrows
																// stairs
					c.getBarrows().useStairs();
					return;
				}
				c.sendMessage("Not yet added. Notify Developers.");
				return;
			}

			if (def.xLength() == 1 && def.yLength() == 2
					&& def.itemActions[0].equals("Climb-down")) {

				if (clickedObjectId == 11725 || clickedObjectId == 1736) { // crazy one in
																	// falador
																	// palace
					int moveX = c.absX + faceOffset[o.getOrientation()][0];
					int moveY = c.absY + faceOffset[o.getOrientation()][1];
					switch (o.getOrientation()) {
						case 0:
							moveY += 2;
							break;
						case 1:
							moveX += 2;
							break;
						case 2:
							moveY -= 2;
							break;
						case 3:
							moveX -= 2;
							break;
					}
					c.getPA().movePlayer(moveX, moveY, c.heightLevel - 1);
					return;
				}

				c.sendMessage("Not yet added. Notify Developers.");
				return;
			}

		}

		// c.sendMessage("Object Type "+o.getType());
	}
	
	private static boolean firstClickObjectCases(Client c, final GameObject o, ObjectDef def) {
		final int obX = o.getX();
		final int obY = o.getY();
		final int objectType = o.getId();
		final RSObject rsObject = ObjectManager.getObjectByNewId(o.getId(), obX,
				obY, c.heightLevel);
		//System.out.println(objectType);
		
		if (TamingQuestHandler.handleTamingQuestObjFirstClick(c, objectType)) {
			return true;
		}
		
		if (SWObjectHandler.handleSWFirstClickObject(c, objectType)) {
			return true;
		}
	
		if (BarbarianAssault.handleFirstClickObject(c, objectType)) {
			return true;
		}
		
		switch (objectType) {
			case 31842: {
				int offset = obY > c.getY() ? 2 : -2;
				Location dest = c.getCurrentLocation().transform(0, offset);
				dest.setZ(0);

				Direction direction = Direction.getDirection(c.getCurrentLocation(), dest);
				ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 2, 2, 839, direction);
				mask1.addStartAnim(839);
				mask1.setTeleLoc(dest);
				c.setForceMovement(mask1);
				return true;
			}
	    case 8929:
			c.getPA().movePlayer(Location.create(2442, 10146));
			return true;
			
	    case 8966:
	    	if (obX == 2440 && obY == 10146) {
	    		c.getPA().movePlayer(Location.create(2523, 3740));
	    	}
	    	return true;

		case 380://easter event object
			if (obX == 2590 && obY == 3093 && c.getZ() == 2) {
				if (!c.getItems().playerHasItem(12646)) {
					c.getItems().addItem(12646, 1);
					c.sendMessage("You find a chocolate egg!");
				} else {
					c.sendMessage("You already found egg here.");
				}
				return true;
			}
			return false;
		case 356://easter event object
			if (obX == 2569 && obY == 3095) {
				if (!c.getItems().playerHasItem(12647)) {
					c.getItems().addItem(12647, 1);
					c.sendMessage("You find a chocolate egg!");
				} else {
					c.sendMessage("You already found egg here.");
				}
				return true;
			}
			return false;
		case 357://easter event object
			if (obX == 2598 && obY == 3104) {
				if (!c.getItems().playerHasItem(12648)) {
					c.getItems().addItem(12648, 1);
					c.sendMessage("You find a chocolate egg!");
				} else {
					c.sendMessage("You already found egg here.");
				}
				return true;
			}
			return false;
		case 61334:
			Pottery.fireUrn(c);
			return true;
		case MagicalCrate.CRATE_OBJECT_ID:
			MagicalCrate.pickupCrate(c, o, rsObject);
			return true;
		case 31561: {// rev cave pillar
			
			int req = 75;
			
			if (o.getX() == 9641 && o.getY() == 10145) {
				req = 89;
			} else if (o.getX() == 9620 && o.getY() == 10086) {
				req = 65;
			}
			
			if (c.getSkills().getLevel(Skills.AGILITY) < req) {
				c.sendMessage("You need agility level " + req + " to cross here.");
				return true;
			}
			
			
			final Direction face = Direction.getLogicalDirection(c.getCurrentLocation(), o.getLocation());
			
			final int dirX = face.getStepX()*2;
			final int dirY = face.getStepY()*2;

//			ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, face).addSecondDirection(dirX, dirY, 1, 0)/*.setWalkStanceId(AgilityAnimations.JUMP_OVER_1TICKS_OSRS.getId())*/.setTelePlayerAtEnd(false);
//			c.setForceMovement(mask);
//			c.startAnimation(AgilityAnimations.JUMP_OVER_1TICKS_OSRS_DELAYED_HALF_TICK);
//			final int standAnim = c.getMovement().getStandAnim();
//			
////			c.getMovement().changeStance(AgilityAnimations.JUMP_OVER_1TICKS_OSRS.getId());
//			
//			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
//				int timer = 0;
//				@Override
//				public void stop() {
//					c.performingAction = false;
//				}
//				
//				@Override
//				public void execute(CycleEventContainer container) {
//					timer++;
//					if (timer == 2) {
//						ForceMovementMask mask = ForceMovementMask.createMask(dirX, dirY, 1, 0, face).setTelePlayerAtEnd(false).setNewOffset(c.getX()+dirX, c.getY()+dirY);
//						c.setForceMovement(mask);
//					}
//					if (timer == 3) {
//						c.getPA().movePlayer(c.getX()+face.getStepX()*4, c.getY()+face.getStepY()*4, c.getZ());
////						c.getMovement().changeStance(standAnim);
////						container.stop();
//					}
//					if (timer == 4) {
//						container.stop();
//						c.getMovement().revertMovementAnims();
//					}
//				}
//			}, 1);
			
			c.startAnimation(AgilityAnimations.JUMP_REV_PILLAR);
			
			c.getMovement().setTempMovementAnim(AgilityAnimations.JUMP_OVER_1TICKS_OSRS.getId());
			
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				int timer = 0;
				@Override
				public void stop() {
					c.performingAction = false;
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					timer++;
					if (timer == 1) {
						ForceMovementMask mask = ForceMovementMask.createMask(dirX, dirY, 1, 0, face).addSecondDirection(dirX, dirY, 1, 0);
						c.setForceMovement(mask);
					}
					if (timer == 3) {
						container.stop();
						c.getMovement().revertMovementAnims();
					}
				}
			}, 1);
			
			break;
		}
		case 31557:
			c.getPA().movePlayerDelayed(3074+Misc.random(1), 3653, 0, 844, 1);
			return true;
		case 31558:
			c.getPA().movePlayerDelayed(3126, 3832+Misc.random(1), 0, 844, 1);
			return true;
		case 31555:
		case 131555:
			c.getPA().movePlayerDelayed(9596, 10056, 0, 844, 1);
			return true;
		case 31556:
		case 131556:
			c.getPA().movePlayerDelayed(9641, 10234, 0, 844, 1);
			return true;
		case 50552:
			if (c.absX == 3454 && c.absY == 3723 && c.heightLevel == 1) {
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					@Override
					public void stop() {
					}

					@Override
					public void execute(CycleEventContainer container) {
						ForceMovementMask slide = ForceMovementMask.createMask(0, 1, 1, 0, Direction.NORTH)
								.addStartAnim(13760);
						c.setForceMovement(slide);
						container.stop();
					}
				}, 1);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					@Override
					public void stop() {
					}

					@Override
					public void execute(CycleEventContainer container) {
						c.getPA().movePlayer(c.getX(), c.getY() + 1, c.getHeightLevel() - 1);
						c.getPA().restorePlayer(true);
						c.getCombat().resetPrayers();
						container.stop();
					}
				}, 3);
			}
			return true;
		case 38150:
			c.getPacketSender().showInterface(59000);
			PollManager.open(c);
			return true;
		case 15638:
			if (o.getX() == 2840 && o.getY() == 3538) {
				c.getPA().movePlayer(2841, 3538, 0);
			}
			return true;
		case 14918: { // lava dragons shortcut
			int yDir1 = 1;
			int yDir2 = 2;
			Direction face = Direction.NORTH;
			if (c.getY() == 3810) {
				yDir1 = -2;
				yDir2 = -1;
				face = Direction.SOUTH;
			}
			final int dir1Final = yDir1;
			final int dir2Final = yDir2;
			final Direction faceFinal = face;
			
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void stop() {
				}

				@Override
				public void execute(CycleEventContainer container) {
					c.getMovement().setTempMovementAnim(807);
					ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, faceFinal).addStartAnim(807).addSecondDirection(0, dir1Final, 1, 0).setCustomSpeed(15, 30);
					c.setForceMovement(mask);
					container.stop();

				}
			}, 1);
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					ForceMovementMask mask = ForceMovementMask.createMask(0, dir2Final, 1, 0, faceFinal).addStartAnim(807).setCustomSpeed(30, 0);
					c.setForceMovement(mask);
					c.getMovement().revertMovementAnims();
					container.stop();
				}
			}, 3);
			break;
		}
		

		case 2618: { // varrock lumber crawl over fence
			if (c.getY() <= o.getY()) {
				int diffX = o.getX() - c.getX();
				int diffY = o.getY() - c.getY();
				int tick = 2;
				ForceMovementMask mask = ForceMovementMask.createMask(diffX, diffY, tick, 0, Direction.getDirection(diffX, diffY)).addStartAnim(823);
				c.setForceMovement(mask);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					int tick = 0;
					@Override
					public void execute(CycleEventContainer container) {
						if (tick == 1) {
							c.startAnimation(AgilityAnimations.CRAWL_OVER_FENCE);
							ForceMovementMask mask2 = ForceMovementMask.createMask(0, 1, 3, 0, Direction.NORTH);
							c.setForceMovement(mask2);
						}
						if (tick == 0) {
							c.faceLocation(o.getX(), o.getY()+1);
						}
						if (tick > 0)
							container.stop();
						tick++;
					}
				}, tick);
				
			} else {
				int diffX = o.getX() - c.getX();
				int diffY = o.getY() - c.getY();
				int tick = 3;
				c.startAnimation(c.isMoving ? AgilityAnimations.CRAWL_OVER_FENCE_DELAYED : AgilityAnimations.CRAWL_OVER_FENCE);
				ForceMovementMask mask = ForceMovementMask.createMask(diffX, diffY, tick, 0, Direction.getDirection(diffX, diffY)).addSecondDirection(0, -1, 2, 823);
				c.setForceMovement(mask);
			}
			break;
		}
			case 9311: { //GE shortcut
				int firstTick = 2;
				int secondTick = 3;
				ForceMovementMask mask = ForceMovementMask.createMask(c.getClickedObject().getX() - c.absX, c.getClickedObject().getY() - c.absY, firstTick, 0, Direction.EAST).addStartAnim(2589).addSecondDirection(4, -2, secondTick, 2590);
				c.setForceMovement(mask);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
						c.startAnimation(2591);
						ForceMovementMask mask2 = ForceMovementMask.createMask(1, 0, 1, 0, Direction.EAST);
						c.setForceMovement(mask2);
						
					}
				}, firstTick + secondTick+1);
				break;
			}
			case 9312: {//GE shortcut
				final int firstTick = 1;
				final int secondTick = 3;
				ForceMovementMask mask = ForceMovementMask.createMask(c.getClickedObject().getX() - c.absX, c.getClickedObject().getY() - c.absY, firstTick, 2589, Direction.WEST).addStartAnim(2589).addSecondDirection(-4, 2, secondTick, 2590);
				c.setForceMovement(mask);
				CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						container.stop();
						c.startAnimation(2591);
						ForceMovementMask mask2 = ForceMovementMask.createMask(-1, 0, firstTick, 0, Direction.WEST);
						c.setForceMovement(mask2);
						
					}
				}, firstTick + secondTick+1);
				break;
			}
		
	        case 27254:
	        	c.getDH().sendNpcChat3("You should hurry up human,", "if you want to save your cities MuHAhaHAhaHaahA.", "Zombies are taking over them all!", 6390, "Death");
	        	c.nextChat = 27254;
	        	break;

			case 42219:
				c.getPA().movePlayer(1886, 3178, 0);
				break;

			case 42220:
				c.getPA().movePlayer(3082, 3478, 0);
				break;

			case 12230:
				c.getPA().movePlayer(2984, 3387, 0);
				break;

			/*case 26961:
				if (!c.getItems().playerHasItem(12648) && c.absX == 3083
						&& c.absY == 3511) { // change this location for next easter event
					c.getDH().sendDialogues(23134, c.npcType);
				}
				return true;*/
			case 1597: // left gate?
			case 1728: // left gate?
				if (rsObject != null && rsObject.getTick() > 0) {
					RSObject revertedDoor = ObjectManager.getObjectByNewId(-1,
							rsObject.getOriginalX(), rsObject.getOriginalY(),
							o.getZ());
					if (revertedDoor != null && revertedDoor.getTick() > 0) {
						revertedDoor.setTick(0);
					} else {
						return true;
					}
					rsObject.setTick(0);
					return true;
				}
				ObjectManager.addObject(new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(),
						o.getType(), o.getId(), RSConstants.DOOR_LIFE));
				ObjectManager.addObject(RSObject.createDoor(1597,
						o.getX() + Misc.DIRECTION[(o.getOrientation() + 2) % 3][0],
						o.getY() + Misc.DIRECTION[(o.getOrientation() + 2) % 3][1],
						o.getZ(), o.getOrientation() + 1, o.getType(), -1, o.getX(),
						o.getY(), RSConstants.DOOR_LIFE));
				
				return true;
			case 1596: // right gate?
				if (rsObject != null && rsObject.getTick() > 0) {
					RSObject revertedDoor = ObjectManager.getObjectByNewId(-1,
							rsObject.getOriginalX(), rsObject.getOriginalY(),
							o.getZ());
					if (revertedDoor != null && revertedDoor.getTick() > 0) {
						revertedDoor.setTick(0);
					} else {
						return true;
					}
					rsObject.setTick(0);
					return true;
				}
				ObjectManager.addObject(new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(),
						o.getType(), o.getId(), RSConstants.DOOR_LIFE));
				ObjectManager.addObject(RSObject.createDoor(1596,
						o.getX() + Misc.DIRECTION[(o.getOrientation() + 2) % 3][0],
						o.getY() + Misc.DIRECTION[(o.getOrientation() + 2) % 3][1],
						o.getZ(), o.getOrientation() + 3, o.getType(), -1, o.getX(),
						o.getY(), RSConstants.DOOR_LIFE));
				return true;

			case 96: // boots of lightness area staircase
				c.getPA().movePlayerDelayed(c.getX() + 8, c.getY() + 41, 0, 0,
						1);
				break;
			case 98: // going to boots of lightness area staircase
				// NOTE: if player does not have candle, then it should do Y -
				// 41 - 23 -23 if player does not have candle
				c.getPA().movePlayerDelayed(c.getX() - 8, c.getY() - 41, 0, 0,
						1);
				break;

			case 5263: // climb down ectofunctus stairs
				if (c.getZ() == 3) {
					c.getPA().movePlayerDelayed(c.getX() - 4, c.getY(), 2, 0,
							1);
				} else if (c.getZ() == 2) {
					c.getPA().movePlayerDelayed(c.getX() + 4, c.getY(), 1, 0,
							1);
				} else if (c.getZ() == 1) {
					c.getPA().movePlayerDelayed(c.getX() - 4, c.getY(), 0, 0,
							1);
				}
				break;
			case 5262: // climb up ectofunctus stairs
				if (c.getZ() == 3) {
					c.getPA().movePlayerDelayed(c.getX() + 4, c.getY(), 3, 0,
							1);
				} else if (c.getZ() == 2) {
					c.getPA().movePlayerDelayed(c.getX() - 4, c.getY(), 2, 0,
							1);
				} else if (c.getZ() == 0) {
					c.getPA().movePlayerDelayed(c.getX() + 4, c.getY(), 1, 0,
							1);
				}
				break;

			case 10771: // north of duel, some stairs in minigame
				c.getPA().movePlayerDelayed(3369, 3307, 1, 0, 1);
				break;
			case 10773: // stairs
				c.getPA().movePlayerDelayed(3366, 3306, 0, 0, 1);
				break;
			case 10775: // north of duel, some stairs in minigame
				c.getPA().movePlayerDelayed(3357, 3307, 1, 0, 1);
				break;
			case 10776: // stairs
				c.getPA().movePlayerDelayed(3360, 3306, 0, 0, 1);
				break;

			// draynor manor
			case 2616:
				if (c.getZones().isInDmmTourn()) {
					c.getDialogueBuilder().sendOption("Exit to Edgeville", ()-> {
						if (c.getZones().isInDmmTourn()) {
							c.getPA().movePlayer(LocationConstants.EDGEVILLE);
						}
					}, "Stay here", ()-> {
						c.getDialogueBuilder().close();
					}).executeIfNotActive();
				} else {
					c.getPA().movePlayer(c.getX() - 38, c.getY() + 6415, c.getZ());
				}
				break;
			case 2617:
				c.getPA().movePlayer(c.getX() + 38, c.getY() - 6415, c.getZ());
				break;
			case 11498:
				c.getPA().movePlayer(c.getX(), c.getY() + 5, 1);
				break;
			case 11499:
				c.getPA().movePlayer(c.getX(), c.getY() - 5, 0);
				break;

			case 55301:
				c.getPA().openClanInterface();
				break;

			case 7147:
			case 7148:
			case 7149:
			case 7150:
			case 7151:
			case 7152:
			case 7153:
			case 7143:// mine
			case 7144:// tendrils
			case 7145:// boil burn down
			case 7146:// eyes
				c.getPA().movePlayer(3039, 4834, 0);
				break;

			case 8987:
				c.getPA().movePlayer(3087, 3499, 0);
				c.sendMessage("You leave the Fishing Contest Island.");
				break;

			case 57211:
				if (c.getItems().playerHasItem(20120)) {
					c.getItems().deleteItemInOneSlot(20120, 1);
					c.getPA().movePlayer(2856, 5222, 0);
					c.sendMessage("Your key broke when opening the door.");
				} else {
					c.sendMessage("You need a frozen key to open this door.");
				}
				break;

			case 16154:
			case 16089:
			case 16090:
			case 16123:
			case 16124:
			case 16065:
			case 16066:
			case 16114:
			case 16115:
			case 16112:
			case 16049:
			case 16044:
			case 16043:
			case 16078:
			case 16135:
			case 16077:
			case 16150:
			case 16082:
			case 16116:
			case 16118:
			case 16047:
			case 16050:
				StrongholdOfSecurity.handleObject(c, objectType, obX, obY);
				break;

			// easter event
			case 29869:
				if (c.getItems().playerHasItem(12654, 1)) {
					c.getItems().deleteItemInOneSlot(12654, 1);
					c.getItems().addItem(12655, 1);
					c.getDH().sendStatement(
							"You dip your egg into chocolate pool and get chocatrice egg.");
				} else {
					c.getDH().sendStatement("You need a cockatrice egg.");
				}
				break;

			case 29866:
			case 29865:
				if (c.getItems().playerHasItem(12654)) {
					c.getDH().sendStatement(
							"You already have a cockatrice egg.");
				} else {
					c.getItems().addItem(12654, 1);
					c.sendMessage("You take a cockatrice egg.");
				}
				break;

			case 29867:
				c.getItems().addItem(1925, 1);
				c.sendMessage("You take a bucket.");
				break;

			case 29868:
				if (c.getItems().playerHasItem(1925, 1)) {
					c.getItems().deleteItemInOneSlot(1925, 1);
					c.getItems().addItem(12652, 1);
					c.sendMessage("You fill your bucket with coal.");
				}
				break;

			case 29871:
				if (c.getItems().playerHasItem(1925, 1)) {
					c.getItems().deleteItemInOneSlot(1925, 1);
					c.getItems().addItem(12651, 1);
					c.sendMessage("You fill your bucket with water.");
				}
				break;

			case 29857:
				if (c.getItems().playerHasItem(12655, 1)
						&& c.getItems().playerHasItem(12652, 5)
						&& c.getItems().playerHasItem(12651, 5)) {
					c.getItems().deleteItemInOneSlot(12655, 1);
					c.getItems().deleteItemInOneSlot(12652, 1);
					c.getItems().deleteItemInOneSlot(12652, 1);
					c.getItems().deleteItemInOneSlot(12652, 1);
					c.getItems().deleteItemInOneSlot(12652, 1);
					c.getItems().deleteItemInOneSlot(12652, 1);
					c.getItems().deleteItemInOneSlot(12651, 1);
					c.getItems().deleteItemInOneSlot(12651, 1);
					c.getItems().deleteItemInOneSlot(12651, 1);
					c.getItems().deleteItemInOneSlot(12651, 1);
					c.getItems().deleteItemInOneSlot(12651, 1);
					c.getItems().addItem(12656, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.getItems().addItem(1925, 1);
					c.sendMessage("You have hatched a chocatrice. Well done!");
					c.getDH().sendDialogues(13800, 7201);
				} else if (!c.getItems().playerHasItem(12656)){
					c.getDH().sendStatement("You need a chockatrice egg,", "5 buckets of coal and 5 buckets of water.");
				} else {
					c.sendMessage("You have already created the creature.");
				}
				break;

			case 29830:
				c.getPA().movePlayer(2461, 5308, 0);
				c.getDH().sendDialogues(13803, 7201);
				break;

			case 29841:
				if (!c.getItems().playerHasItem(12657, 15)) {
					c.getDH().sendStatement("You need 15 chocolate chunks.");
					break;
				}

				if (c.getItems().freeSlots() <= 4) {
					c.sendMessage("You don't have enough space to get rewarded!");
					break;
				}

				c.getItems().deleteItemInOneSlot(12657, 15);
				// c.getItems().addItem(12634, 1);// event 2017
				// c.getItems().addItem(14728, 1);// event 2017
				c.getItems().addItem(30116, 1);
				c.getItems().addItem(30117, 1);
				c.getItems().addItem(30118, 1);
				c.getItems().addItem(30119, 1);
				c.getDH().sendDialogues(13766, 13651);
				break;

			case 29829:
				c.transform(-1);
				c.getPA().resetMovementAnimation();
				c.getPA().movePlayer(3086, 3493, 0);
				break;

			case 23117:
				if (c.getItems().playerHasItem(12646)
						&& c.getItems().playerHasItem(12647)
						&& c.getItems().playerHasItem(12648)) {
					c.getPA().movePlayer(2457, 5321, 0);
					c.getDH().sendDialogues(23136, 7200);
				} else {
					c.getDH().sendDialogues(23117, 7200);
				}
				break;

			/*case 365:
				if (c.absX == 3070 && c.absY == 3516) {
					c.getItems().addItem(12646, 1);
					c.sendMessage("You found a chocolate egg.");
				}
				break;*/
			/*case 26962://easter event
				if (c.absX == 3092 && c.absY == 3510) {
					c.getItems().addItem(12647, 1);
					c.sendMessage("You found a chocolate egg.");
				}
				break;*/
			// easter event end

			case 28714:
				c.getPA().movePlayer(3087, 3499, 0);
				break;

			case 38660:
			case 38661:
			case 38662:
			case 38663:
			case 38664:
			case 38665:
			case 38666:
			case 38667:
			case 38668:
				ShootingStar.mineStar(c, objectType, obX, obY);
				break;

			case 38700: // funpk clan wars
				if (o.getX() == 3006 && o.getY() == 5509)
					c.getPA().movePlayer(3274+Misc.random(1), 3692, 0);
				else
					c.getPA().movePlayer(3268+Misc.random(1), 3692, 0);
				break;
			case 38698: // funpk clan wars
				c.getPA().movePlayer(2815, 5511, 0);
				break;
			// case 4387:
			// CastleWars.addToWaitRoom(c, 1); //saradomin
			// break;
			// case 4388:
			// CastleWars.addToWaitRoom(c, 2); // zamorak
			// break;
			// case 4408:
			// CastleWars.addToWaitRoom(c, 3); //guthix
			// break;
			// case 4389: //saradomin
			// case 4390: //zamorak
			// CastleWars.leaveWaitingRoom(c);
			// break;

			case 492: // karamja to the dungeon
				c.getPA().movePlayer(2857, 9569, 0);
				break;

			case 1764: // at demons to go up the rope
				if (obX == 2856 && obY == 9569) {
					c.getPA().movePlayer(2856, 3167, 0);
				}
				break;

			case 4889: // ape atoll underground rope to climb above ground
				if (o.getX() == 2764 && o.getY() == 9165) {
					c.getPA().movePlayer(2764, 2770, 0);
				}
				break;

			case 12128: // fairy ring teleport from main location
				if (obX == 2412 && obY == 4434) {
					FairyRing.openFairyRing(c);
				}
				break;

			case 14067:
			case 14070: // some red place
			case 14097: // fairy ring teleport to teleport to main location
			case 14076: // penguin
			case 14109: // penguin
			case 14115: // draynor?
			case 14151: // castlewars
			case 14127: // cosmic
			case 14130: // sinclair
			case 14058:
				FairyRing.ringTele(c, -1);
				break;

			case 28296:
				c.getItems().addItem(10501, 1);
				c.startAnimation(7529);
				c.sendMessage("You make a snowball.");
				if (Misc.random(100) == 0) {
					ObjectManager.addObject(new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(),
							o.getType(), o.getId(), 40 + Misc.random(100)));
				}
				break;
			// case 10804:
			// c.sendMessage("The trapdoor seems to be locked.");
			// def.if (c.absX >= 3090 && c.absX <= 3091 && c.absY >= 3274 &&
			// c.absY <= 3275) {
			// c.getPA().movePlayer(2004, 4426, 1);
			// }
			// break;
			case 4879:
				if (c.absX == 2806 && c.absY == 2785) {
					//c.startAnimation(827);
					c.getPA().movePlayer(2807, 9201, 0);
				}
				break;
			case 4881:
				if (c.absX == 2807 && c.absY == 9201) {
					c.getPA().movePlayer(2806, 2785, 0);
				}
				break;
			case 10699:
				if (c.absX == 2004 && c.absY == 4426) {
					c.getPA().movePlayer(3091, 3275, 0);
				}
				break;
			case 10707:
				if (c.absX == 2008 && c.absY == 4431) {
					c.getPA().movePlayer(2010, 4431, 1);
				} else if (c.absX == 2007 && c.absY == 4431) {
					c.getPA().movePlayer(2005, 4431, 1);
				}
				break;
			case 10687:
				c.getItems().addItem(6875, 1);
				break;
			case 10691:
				c.getItems().addItem(6871, 1);
				break;
			case 10695:
				c.getItems().addItem(6879, 1);
				break;
			case 10686:
				if (c.getItems().hasBlueTorso()) {
					c.getItems().deleteItemInOneSlot(6875,
							c.getItems().getItemSlot(6875), 1);
					c.getItems().addItem(6876, 1);
				} else {
					c.sendMessage("You need to get a Torso first!");
					return true;
				}
				break;
			case 10690:
				if (c.getItems().hasRedTorso()) {
					c.getItems().deleteItemInOneSlot(6871,
							c.getItems().getItemSlot(6871), 1);
					c.getItems().addItem(6872, 1);
				} else {
					c.sendMessage("You need to get a Torso first!");
					return true;
				}
				break;
			case 10694:
				if (c.getItems().hasGreenTorso()) {
					c.getItems().deleteItemInOneSlot(6879,
							c.getItems().getItemSlot(6879), 1);
					c.getItems().addItem(6880, 1);
				} else {
					c.sendMessage("You need to get a Torso first!");
					return true;
				}
				break;
			case 10688:
				if (c.getItems().hasBlueHead()) {
					c.getItems().deleteItemInOneSlot(6876,
							c.getItems().getItemSlot(6876), 1);
					c.getItems().addItem(6877, 1);
				} else {
					c.sendMessage("You need to add a Head first!");
					return true;
				}
				break;
			case 10692:
				if (c.getItems().hasRedHead()) {
					c.getItems().deleteItemInOneSlot(6872,
							c.getItems().getItemSlot(6872), 1);
					c.getItems().addItem(6873, 1);
				} else {
					c.sendMessage("You need to add a Head first!");
					return true;
				}
				break;
			case 10696:
				if (c.getItems().hasGreenHead()) {
					c.getItems().deleteItemInOneSlot(6880,
							c.getItems().getItemSlot(6880), 1);
					c.getItems().addItem(6881, 1);
				} else {
					c.sendMessage("You need to add a Head first!");
					return true;
				}
				break;
			case 10689:
				if (c.getItems().hasBlueArms()) {
					c.getItems().deleteItemInOneSlot(6877,
							c.getItems().getItemSlot(6877), 1);
					c.getItems().addItem(6878, 1);
				} else {
					c.sendMessage("You need to add Arms first!");
					return true;
				}
				break;
			case 10693:
				if (c.getItems().hasRedArms()) {
					c.getItems().deleteItemInOneSlot(6873,
							c.getItems().getItemSlot(6873), 1);
					c.getItems().addItem(6874, 1);
				} else {
					c.sendMessage("You need to add Arms first!");
					return true;
				}
				break;
			case 10697:
				if (c.getItems().hasGreenArms()) {
					c.getItems().deleteItemInOneSlot(6881,
							c.getItems().getItemSlot(6881), 1);
					c.getItems().addItem(6882, 1);
				} else {
					c.sendMessage("You need to add Arms first!");
					return true;
				}
				break;

			case 37684:
				c.getPA().movePlayer(3226, 3253, 0);
				c.getItems().deleteAllItems();
				break;

			case 3730:
				if (c.getDonatedTotalAmount() >= 250) {
					if (c.absX == 2585 && c.absY == 3879
							|| c.absX == 2582 && c.absY == 3879) {
						c.getPA().movePlayer(c.absX == 2585 ? 2582 : 2585, 3879,
								0);
					} else if (c.absX == 2585 && c.absY == 3880
							|| c.absX == 2582 && c.absY == 3880) {
						c.getPA().movePlayer(c.absX == 2585 ? 2582 : 2585, 3880,
								0);
					}
				} else {
					c.sendMessage(
							"You need to be a Supreme Donator or higher to enter this place");
				}
				break;

			case 6702:
			case 6703:
			case 6704:
			case 6705:
			case 6706:
			case 6707:
				c.getBarrows().useStairs();
				// c.sendMessage("woorks poop.");
				break;

			case 10284:
				if (!WorldType.equalsType(WorldType.SPAWN)) {
					c.getBarrows().useChest();
				} else {
					c.sendMessage(
							"Barrows minigame has been disabled for SoulPvP");
				}
				break;

			case 6823:
				if (c.barrows[5] == 0) {
					int spawnX = c.getX(), spawnY = c.getY();
					boolean breaking = false;
					for (int x = 0; x < 2; x++) {
						if (breaking) {
							break;
						}
						for (int y = 0; y < 2; y++) {
							if (x == 0 && y == 0) {
								continue;
							}
							if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
									c.getHeightLevel(), x, y)) {
								spawnX = c.getX() + x;
								spawnY = c.getY() + y;
								breaking = true;
								break;
							}
						}
					}
					NPCHandler.spawnNpc(c, 2030, spawnX, spawnY, 3, 0, true, true);
					c.barrows[5] = 1;
				} else {
					c.sendMessage(
							"You have already searched in this sarcophagus.");
				}
				break;

			case 6772:
				if (c.barrows[4] == 0) {
					int spawnX = c.getX(), spawnY = c.getY();
					boolean breaking = false;
					for (int x = 0; x < 2; x++) {
						if (breaking) {
							break;
						}
						for (int y = 0; y < 2; y++) {
							if (x == 0 && y == 0) {
								continue;
							}
							if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
									c.getHeightLevel(), x, y)) {
								spawnX = c.getX() + x;
								spawnY = c.getY() + y;
								breaking = true;
								break;
							}
						}
					}
					NPCHandler.spawnNpc(c, 2029, spawnX, spawnY, 3, 0, true, true);
					c.barrows[4] = 1;
				} else {
					c.sendMessage(
							"You have already searched in this sarcophagus.");
				}
				break;

			case 6822:
				if (c.barrows[3] == 0) {
					int spawnX = c.getX(), spawnY = c.getY();
					boolean breaking = false;
					for (int x = 0; x < 2; x++) {
						if (breaking) {
							break;
						}
						for (int y = 0; y < 2; y++) {
							if (x == 0 && y == 0) {
								continue;
							}
							if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
									c.getHeightLevel(), x, y)) {
								spawnX = c.getX() + x;
								spawnY = c.getY() + y;
								breaking = true;
								break;
							}
						}
					}
					NPCHandler.spawnNpc(c, 2028, spawnX, spawnY, 3, 0, true, true);
					c.barrows[3] = 1;
				} else {
					c.sendMessage(
							"You have already searched in this sarcophagus.");
				}
				break;

			case 6773:
				if (c.barrows[2] == 0) {
					int spawnX = c.getX(), spawnY = c.getY();
					boolean breaking = false;
					for (int x = 0; x < 2; x++) {
						if (breaking) {
							break;
						}
						for (int y = 0; y < 2; y++) {
							if (x == 0 && y == 0) {
								continue;
							}
							if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
									c.getHeightLevel(), x, y)) {
								spawnX = c.getX() + x;
								spawnY = c.getY() + y;
								breaking = true;
								break;
							}
						}
					}
					NPCHandler.spawnNpc(c, 2027, spawnX, spawnY, 3, 0, true, true);
					c.barrows[2] = 1;
				} else {
					c.sendMessage(
							"You have already searched in this sarcophagus.");
				}
				break;

			case 6771:
				c.getDH().sendDialogues(2999, 2026);
				break;

			case 6821:
				if (c.barrows[0] == 0) {
					int spawnX = c.getX(), spawnY = c.getY();
					boolean breaking = false;
					for (int x = 0; x < 2; x++) {
						if (breaking) {
							break;
						}
						for (int y = 0; y < 2; y++) {
							if (x == 0 && y == 0) {
								continue;
							}
							if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
									c.getHeightLevel(), x, y)) {
								spawnX = c.getX() + x;
								spawnY = c.getY() + y;
								breaking = true;
								break;
							}
						}
					}
					NPCHandler.spawnNpc(c, 2025, spawnX, spawnY, 3, 0, true, true);
					c.barrows[0] = 1;
				} else {
					c.sendMessage(
							"You have already searched in this sarcophagus.");
				}
				break;

			// case 3237:
			// if (c.absX == 2412 && c.absY == 9605 || c.absX == 2419 && c.absY
			// == 9605) {
			// c.getPA().movePlayer(c.absX == 2412 ? 2419 : 2412, 9605, 0);
			// }
			// break;

			// summoning puches and scrolls
			case 28716:
				c.getPacketSender().showInterface(39700);
				break;

			case 15644:
			case 15641:
				if (c.heightLevel == 2) {
					WarriorsGuild.handleKamfreena(c, true);
				} else if (c.heightLevel == 0) {
					if (!ObjectManager.objectExists(-1, o.getX(), o.getY(), o.getZ()))
						ObjectManager.addObject(new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(),
								o.getType(), o.getId(), 60));
				}
				break;

			case 6:
				if (rsObject != null) {
					if (rsObject.getObjectOwnerLong() == c.getLongName()) {
						if (c.getItems().playerHasItem(2)) {
							DwarfMultiCannon.loadCannon(c);
						}
					}
				}
				break;

			case 7:
				if (rsObject != null) {
					if (c.getItems().freeSlots() < 1) {
						c.sendMessage(
								"You don't have enough room in your Inventory.");
						return true;
					}
					if (rsObject.getObjectOwnerLong() == c.getLongName()
							&& c.cannonStatus == 1) {
						rsObject.setTick(1);
						rsObject.setOriginalId(-1);
						c.getItems().addItem(6, 1);
						c.cannonStatus = 0;
					}
				}
				break;
			case 8:
				if (rsObject != null) {
					if (c.getItems().freeSlots() < 2) {
						c.sendMessage(
								"You don't have enough room in your Inventory.");
						return true;
					}
					if (rsObject.getObjectOwnerLong() == c.getLongName()
							&& c.cannonStatus == 2) {
						rsObject.setTick(1);
						rsObject.setOriginalId(-1);
						c.getItems().addItem(6, 1);
						c.getItems().addItem(8, 1);
						c.cannonStatus = 0;
					}
				}
				break;
			case 9:
				if (rsObject != null) {
					if (c.getItems().freeSlots() < 3) {
						c.sendMessage(
								"You don't have enough room in your Inventory.");
						return true;
					}
					if (rsObject.getObjectOwnerLong() == c.getLongName()
							&& c.cannonStatus == 3) {
						rsObject.setTick(1);
						rsObject.setOriginalId(-1);
						c.getItems().addItem(6, 1);
						c.getItems().addItem(8, 1);
						c.getItems().addItem(10, 1);
						c.cannonStatus = 0;

					}
				}
				break;
			
			// blue portal
			/*case 42029:
				SoulWars.addToWaitRoom(c, SoulWars.Team.BLUE);
				c.poisonDamage = -1;
				break;

			// red portal
			case 42030:
				SoulWars.addToWaitRoom(c, SoulWars.Team.RED);
				c.poisonDamage = -1;
				break;

			// green portal
			case 42031:
				SoulWars.addToWaitRoom(c, SoulWars.Team.RANDOM);
				c.poisonDamage = -1;
				break;*/

			case 4411:
			case 4415:
			case 4417:
			case 4418:
			case 4419:
			case 4420:
			case 4469:
			case 4470:
				// case 4911:
				// case 4912:
			case 4437:
			case 4438:
			case 6281:
			case 6280:
			case 4472:
			case 4471:
			case 4406:
			case 4407:
			case 4902:
			case 4903:
			case 4900:
			case 4901:
			case 4458:
			case 4459:
			case 4460:
			case 4461:
			case 4462: // table with ropes
			case 4463:
			case 4464:
			case 4377:
			case 4378:
			case 4423: // sara large door west
			case 4424: // sara large door east
			case 4425:
			case 4426:
			case 4427: // zammy large door east
			case 4428: // zammy large door west
			case 4429:
			case 4430:
			case 4444: // castle wars climbing rope object
			case 4448: // collapse rock walls
			case 4465: // sara side door closed
			case 4466: // sara side door opened
			case 4467: // zammy side door closed
			case 4468: // zammy side door opened
				CastleWarsObjects.handleObjectFirst(c, o);
				break;

			case 4387:
				CastleWars.addToWaitRoom(c, 1);
				c.getDotManager().curePoison();
				// Server.castleWars.joinWait(c,1);
				break;

			case 4388:
				CastleWars.addToWaitRoom(c, 2);
				c.getDotManager().curePoison();
				// Server.castleWars.joinWait(c,2);
				break;

			case 4408:
				CastleWars.addToWaitRoom(c, 3);
				c.getDotManager().curePoison();
				// Server.castleWars.joinWait(c,3);
				break;

			case 4389: // saradomin exit portal
			case 4390: // zamorak exit portal
				CastleWars.leaveWaitingRoom(c);
				break;

			case 1948:
				if (c.getX() > o.getX()) {
					return true;
				}
				BarbarianCourse.climbCrumblingWall(c, o);
				break;
			// chaos tunnel portals aleksander noob
			case 28779:
				if (obX == 3254 && obY == 5451) {
					c.getPA().movePlayer(3250, 5448, 0);
				} else if (obX == 3250 && obY == 5448) {
					c.getPA().movePlayer(3254, 5451, 0);
				} else if (obX == 3241 && obY == 5445) {
					c.getPA().movePlayer(3233, 5445, 0);
				} else if (obX == 3233 && obY == 5445) {
					c.getPA().movePlayer(3241, 5445, 0);
				} else if (obX == 3259 && obY == 5446) {
					c.getPA().movePlayer(3265, 5491, 0);
				} else if (obX == 3265 && obY == 5491) {
					c.getPA().movePlayer(3259, 5446, 0);
				} else if (obX == 3260 && obY == 5491) {
					c.getPA().movePlayer(3266, 5446, 0);
				} else if (obX == 3266 && obY == 5446) {
					c.getPA().movePlayer(3260, 5491, 0);
				} else if (obX == 3241 && obY == 5469) {
					c.getPA().movePlayer(3233, 5470, 0);
				} else if (obX == 3233 && obY == 5470) {
					c.getPA().movePlayer(3241, 5469, 0);
				} else if (obX == 3235 && obY == 5457) {
					c.getPA().movePlayer(3229, 5454, 0);
				} else if (obX == 3229 && obY == 5454) {
					c.getPA().movePlayer(3235, 5457, 0);
				} else if (obX == 3280 && obY == 5460) {
					c.getPA().movePlayer(3273, 5460, 0);
				} else if (obX == 3273 && obY == 5460) {
					c.getPA().movePlayer(3280, 5460, 0);
				} else if (obX == 3283 && obY == 5448) {
					c.getPA().movePlayer(3287, 5448, 0);
				} else if (obX == 3287 && obY == 5448) {
					c.getPA().movePlayer(3283, 5448, 0);
				} else if (obX == 3244 && obY == 5495) {
					c.getPA().movePlayer(3239, 5498, 0);
				} else if (obX == 3239 && obY == 5498) {
					c.getPA().movePlayer(3244, 5495, 0);
				} else if (obX == 3232 && obY == 5501) {
					c.getPA().movePlayer(3238, 5507, 0);
				} else if (obX == 3238 && obY == 5507) {
					c.getPA().movePlayer(3232, 5501, 0);
				} else if (obX == 3218 && obY == 5497) {
					c.getPA().movePlayer(3222, 5488, 0);
				} else if (obX == 3222 && obY == 5488) {
					c.getPA().movePlayer(3218, 5497, 0);
				} else if (obX == 3218 && obY == 5478) {
					c.getPA().movePlayer(3215, 5475, 0);
				} else if (obX == 3215 && obY == 5475) {
					c.getPA().movePlayer(3218, 5478, 0);
				} else if (obX == 3224 && obY == 5479) {
					c.getPA().movePlayer(3222, 5474, 0);
				} else if (obX == 3222 && obY == 5474) {
					c.getPA().movePlayer(3224, 5479, 0);
				} else if (obX == 3208 && obY == 5471) {
					c.getPA().movePlayer(3210, 5477, 0);
				} else if (obX == 3210 && obY == 5477) {
					c.getPA().movePlayer(3208, 5471, 0);
				} else if (obX == 3214 && obY == 5456) {
					c.getPA().movePlayer(3212, 5452, 0);
				} else if (obX == 3212 && obY == 5452) {
					c.getPA().movePlayer(3214, 5456, 0);
				} else if (obX == 3204 && obY == 5445) {
					c.getPA().movePlayer(3197, 5448, 0);
				} else if (obX == 3197 && obY == 5448) {
					c.getPA().movePlayer(3204, 5445, 0);
				} else if (obX == 3189 && obY == 5444) {
					c.getPA().movePlayer(3187, 5460, 0);
				} else if (obX == 3187 && obY == 5460) {
					c.getPA().movePlayer(3189, 5444, 0);
				} else if (obX == 3192 && obY == 5472) {
					c.getPA().movePlayer(3186, 5472, 0);
				} else if (obX == 3186 && obY == 5472) {
					c.getPA().movePlayer(3192, 5472, 0);
				} else if (obX == 3185 && obY == 5478) {
					c.getPA().movePlayer(3191, 5482, 0);
				} else if (obX == 3191 && obY == 5482) {
					c.getPA().movePlayer(3185, 5478, 0);
				} else if (obX == 3171 && obY == 5473) {
					c.getPA().movePlayer(3167, 5471, 0);
				} else if (obX == 3167 && obY == 5471) {
					c.getPA().movePlayer(3171, 5473, 0);
				} else if (obX == 3171 && obY == 5478) {
					c.getPA().movePlayer(3167, 5478, 0);
				} else if (obX == 3167 && obY == 5478) {
					c.getPA().movePlayer(3171, 5478, 0);
				} else if (obX == 3168 && obY == 5456) {
					c.getPA().movePlayer(3178, 5460, 0);
				} else if (obX == 3178 && obY == 5460) {
					c.getPA().movePlayer(3168, 5456, 0);
				} else if (obX == 3191 && obY == 5495) {
					c.getPA().movePlayer(3194, 5490, 0);
				} else if (obX == 3194 && obY == 5490) {
					c.getPA().movePlayer(3191, 5495, 0);
				} else if (obX == 3141 && obY == 5480) {
					c.getPA().movePlayer(3142, 5489, 0);
				} else if (obX == 3142 && obY == 5489) {
					c.getPA().movePlayer(3141, 5480, 0);
				} else if (obX == 3142 && obY == 5462) {
					c.getPA().movePlayer(3154, 5462, 0);
				} else if (obX == 3154 && obY == 5462) {
					c.getPA().movePlayer(3142, 5462, 0);
				} else if (obX == 3143 && obY == 5443) {
					c.getPA().movePlayer(3155, 5449, 0);
				} else if (obX == 3155 && obY == 5449) {
					c.getPA().movePlayer(3143, 5443, 0);
				} else if (obX == 3307 && obY == 5496) {
					c.getPA().movePlayer(3317, 5496, 0);
				} else if (obX == 3317 && obY == 5496) {
					c.getPA().movePlayer(3307, 5496, 0);
				} else if (obX == 3318 && obY == 5481) {
					c.getPA().movePlayer(3322, 5480, 0);
				} else if (obX == 3322 && obY == 5480) {
					c.getPA().movePlayer(3318, 5481, 0);
				} else if (obX == 3299 && obY == 5484) {
					c.getPA().movePlayer(3303, 5477, 0);
				} else if (obX == 3303 && obY == 5477) {
					c.getPA().movePlayer(3299, 5484, 0);
				} else if (obX == 3286 && obY == 5470) {
					c.getPA().movePlayer(3285, 5474, 0);
				} else if (obX == 3285 && obY == 5474) {
					c.getPA().movePlayer(3286, 5470, 0);
				} else if (obX == 3290 && obY == 5463) {
					c.getPA().movePlayer(3302, 5469, 0);
				} else if (obX == 3302 && obY == 5469) {
					c.getPA().movePlayer(3290, 5463, 0);
				} else if (obX == 3296 && obY == 5455) {
					c.getPA().movePlayer(3299, 5450, 0);
				} else if (obX == 3299 && obY == 5450) {
					c.getPA().movePlayer(3296, 5455, 0);
				} else if (obX == 3280 && obY == 5501) {
					c.getPA().movePlayer(3285, 5508, 0);
				} else if (obX == 3285 && obY == 5508) {
					c.getPA().movePlayer(3280, 5501, 0);
				} else if (obX == 3300 && obY == 5514) {
					c.getPA().movePlayer(3297, 5510, 0);
				} else if (obX == 3297 && obY == 5510) {
					c.getPA().movePlayer(3300, 5514, 0);
				} else if (obX == 3289 && obY == 5533) {
					c.getPA().movePlayer(3288, 5536, 0);
				} else if (obX == 3288 && obY == 5536) {
					c.getPA().movePlayer(3289, 5533, 0);
				} else if (obX == 3285 && obY == 5527) {
					c.getPA().movePlayer(3282, 5531, 0);
				} else if (obX == 3282 && obY == 5531) {
					c.getPA().movePlayer(3285, 5527, 0);
				} else if (obX == 3325 && obY == 5518) {
					c.getPA().movePlayer(3323, 5531, 0);
				} else if (obX == 3323 && obY == 5531) {
					c.getPA().movePlayer(3325, 5518, 0);
				} else if (obX == 3299 && obY == 5533) {
					c.getPA().movePlayer(3297, 5536, 0);
				} else if (obX == 3297 && obY == 5536) {
					c.getPA().movePlayer(3299, 5533, 0);
				} else if (obX == 3321 && obY == 5554) {
					c.getPA().movePlayer(3315, 5552, 0);
				} else if (obX == 3315 && obY == 5552) {
					c.getPA().movePlayer(3321, 5554, 0);
				} else if (obX == 3291 && obY == 5555) {
					c.getPA().movePlayer(3285, 5556, 0);
				} else if (obX == 3285 && obY == 5556) {
					c.getPA().movePlayer(3291, 5555, 0);
				} else if (obX == 3266 && obY == 5552) {
					c.getPA().movePlayer(3262, 5552, 0);
				} else if (obX == 3262 && obY == 5552) {
					c.getPA().movePlayer(3266, 5552, 0);
				} else if (obX == 3256 && obY == 5561) {
					c.getPA().movePlayer(3253, 5561, 0);
				} else if (obX == 3253 && obY == 5561) {
					c.getPA().movePlayer(3256, 5561, 0);
				} else if (obX == 3249 && obY == 5546) {
					c.getPA().movePlayer(3252, 5543, 0);
				} else if (obX == 3252 && obY == 5543) {
					c.getPA().movePlayer(3249, 5546, 0);
				} else if (obX == 3261 && obY == 5536) {
					c.getPA().movePlayer(3268, 5534, 0);
				} else if (obX == 3268 && obY == 5534) {
					c.getPA().movePlayer(3261, 5536, 0);
				} else if (obX == 3243 && obY == 5526) {
					c.getPA().movePlayer(3241, 5529, 0);
				} else if (obX == 3241 && obY == 5529) {
					c.getPA().movePlayer(3243, 5526, 0);
				} else if (obX == 3230 && obY == 5547) {
					c.getPA().movePlayer(3226, 5553, 0);
				} else if (obX == 3226 && obY == 5553) {
					c.getPA().movePlayer(3230, 5547, 0);
				} else if (obX == 3206 && obY == 5553) {
					c.getPA().movePlayer(3204, 5546, 0);
				} else if (obX == 3204 && obY == 5546) {
					c.getPA().movePlayer(3206, 5553, 0);
				} else if (obX == 3211 && obY == 5533) {
					c.getPA().movePlayer(3214, 5533, 0);
				} else if (obX == 3214 && obY == 5533) {
					c.getPA().movePlayer(3211, 5533, 0);
				} else if (obX == 3208 && obY == 5527) {
					c.getPA().movePlayer(3211, 5523, 0);
				} else if (obX == 3211 && obY == 5523) {
					c.getPA().movePlayer(3208, 5527, 0);
				} else if (obX == 3201 && obY == 5531) {
					c.getPA().movePlayer(3197, 5529, 0);
				} else if (obX == 3197 && obY == 5529) {
					c.getPA().movePlayer(3201, 5531, 0);
				} else if (obX == 3202 && obY == 5515) {
					c.getPA().movePlayer(3196, 5512, 0);
				} else if (obX == 3196 && obY == 5512) {
					c.getPA().movePlayer(3202, 5515, 0);
				} else if (obX == 3190 && obY == 5515) {
					c.getPA().movePlayer(3190, 5519, 0);
				} else if (obX == 3190 && obY == 5519) {
					c.getPA().movePlayer(3190, 5515, 0);
				} else if (obX == 3185 && obY == 5518) {
					c.getPA().movePlayer(3181, 5517, 0);
				} else if (obX == 3181 && obY == 5517) {
					c.getPA().movePlayer(3185, 5518, 0);
				} else if (obX == 3187 && obY == 5531) {
					c.getPA().movePlayer(3182, 5530, 0);
				} else if (obX == 3182 && obY == 5530) {
					c.getPA().movePlayer(3187, 5531, 0);
				} else if (obX == 3169 && obY == 5510) {
					c.getPA().movePlayer(3159, 5501, 0);
				} else if (obX == 3159 && obY == 5501) {
					c.getPA().movePlayer(3169, 5510, 0);
				} else if (obX == 3165 && obY == 5515) {
					c.getPA().movePlayer(3173, 5530, 0);
				} else if (obX == 3173 && obY == 5530) {
					c.getPA().movePlayer(3165, 5515, 0);
				} else if (obX == 3156 && obY == 5523) {
					c.getPA().movePlayer(3152, 5520, 0);
				} else if (obX == 3152 && obY == 5520) {
					c.getPA().movePlayer(3156, 5523, 0);
				} else if (obX == 3148 && obY == 5533) {
					c.getPA().movePlayer(3153, 5537, 0);
				} else if (obX == 3153 && obY == 5537) {
					c.getPA().movePlayer(3148, 5533, 0);
				} else if (obX == 3143 && obY == 5535) {
					c.getPA().movePlayer(3147, 5541, 0);
				} else if (obX == 3147 && obY == 5541) {
					c.getPA().movePlayer(3143, 5535, 0);
				} else if (obX == 3168 && obY == 5541) {
					c.getPA().movePlayer(3171, 5542, 0);
				} else if (obX == 3171 && obY == 5542) {
					c.getPA().movePlayer(3168, 5541, 0);
				} else if (obX == 3190 && obY == 5549) {
					c.getPA().movePlayer(3190, 5554, 0);
				} else if (obX == 3190 && obY == 5554) {
					c.getPA().movePlayer(3190, 5549, 0);
				} else if (obX == 3180 && obY == 5557) {
					c.getPA().movePlayer(3174, 5558, 0);
				} else if (obX == 3174 && obY == 5558) {
					c.getPA().movePlayer(3180, 5557, 0);
				} else if (obX == 3162 && obY == 5557) {
					c.getPA().movePlayer(3158, 5561, 0);
				} else if (obX == 3158 && obY == 5561) {
					c.getPA().movePlayer(3162, 5557, 0);
				} else if (obX == 3166 && obY == 5553) {
					c.getPA().movePlayer(3162, 5545, 0);
				} else if (obX == 3162 && obY == 5545) {
					c.getPA().movePlayer(3166, 5553, 0);
				} else if (obX == 3142 && obY == 5545) {
					c.getPA().movePlayer(3115, 5528, 0);
				}
				break;
			// Borks portal
			case 29537:
				if (obX == 3115 && obY == 5528) {
					c.getPA().movePlayer(3142, 5545, 0);
				}
				break;
			case 4543:
				if (c.absX >= 2513 && c.absX <= 2516 && c.absY == 4626) {
					c.getPA().movePlayer(2514, 4628, 1);
				} else if (c.absX >= 2513 && c.absX <= 2516 && c.absY >= 4627
						&& c.absY <= 4628) {
					c.getPA().movePlayer(2514, 4626, 1);
				}
				break;
			case 4544:
				if (c.absX >= 2513 && c.absX <= 2516 && c.absY == 4626) {
					c.getPA().movePlayer(2514, 4628, 1);
				} else if (c.absX >= 2513 && c.absX <= 2516 && c.absY >= 4627
						&& c.absY <= 4628) {
					c.getPA().movePlayer(2514, 4626, 1);
				}
				break;
			case 4545:
				if (c.absX >= 2513 && c.absX <= 2516 && c.absY == 4626) {
					c.getPA().movePlayer(2514, 4628, 1);
				} else if (c.absX >= 2513 && c.absX <= 2516 && c.absY >= 4627
						&& c.absY <= 4628) {
					c.getPA().movePlayer(2514, 4626, 1);
				}
				break;
			case 4546:
				if (c.absX >= 2513 && c.absX <= 2516 && c.absY == 4626) {
					c.getPA().movePlayer(2514, 4628, 1);
				} else if (c.absX >= 2513 && c.absX <= 2516 && c.absY >= 4627
						&& c.absY <= 4628) {
					c.getPA().movePlayer(2514, 4626, 1);
				}
				break;
			case 8960:
				if (c.absX == 2490 && c.absY >= 10130 && c.absY <= 10132) {
					c.getPA().movePlayer(2492, 10131, 0);
				} else if (c.absX == 2492 && c.absY >= 10130
						&& c.absY <= 10132) {
					c.getPA().movePlayer(2490, 10131, 0);
				}
				break;
			case 8959:
				if (c.absX == 2490 && c.absY >= 10146 && c.absY <= 10148) {
					c.getPA().movePlayer(2492, 10147, 0);
				} else if (c.absX == 2492 && c.absY >= 10146
						&& c.absY <= 10148) {
					c.getPA().movePlayer(2490, 10147, 0);
				}
				break;
			case 8958:
				if (c.absX == 2490 && c.absY == 10162 && c.absY <= 10164) {
					c.getPA().movePlayer(2492, 10163, 0);
				} else if (c.absX == 2492 && c.absY >= 10162
						&& c.absY <= 10164) {
					c.getPA().movePlayer(2490, 10163, 0);
				}
				break;
			case 10212:
				c.getPA().movePlayer(2519, 4619, 1);
				c.sendMessage("You climb up the ladder.");
				break;
			case 10193:
				c.getPA().movePlayer(1863, 4389, 2);
				c.sendMessage("You climb up the ladder.");
				break;
			case 10213:
				c.getPA().movePlayer(1864, 4389, 1);
				c.sendMessage("You climb up the ladder.");
				break;
			case 10214:
				c.getPA().movePlayer(1864, 4387, 2);
				c.sendMessage("You climb up the ladder.");
				break;
			// case 10193:
			// c.getPA().movePlayer(2519, 4619, 1);
			// c.sendMessage("You climb up the ladder.");
			// break;
			case 10177:
				c.getPA().movePlayer(2519, 4619, 1);
				c.sendMessage("You climb down the ladder.");
				break;
			case 4383:
				c.getPA().movePlayer(2519, 4619, 1);
				c.sendMessage("You climb down the iron ladder.");
				break;
			case 4412:
				c.getPA().movePlayer(2510, 3644, 0);
				c.sendMessage("You climb up the iron ladder.");
				break;
			case 881: // closed manhole
				ObjectManager.addObject(new RSObject(882, o.getX(), o.getY(), o.getZ(), o.getOrientation(),
						o.getType(), o.getId(), 100 + Misc.random(100)));
				break;
			case 882: // climb down manhole
				c.getPA().movePlayerDelayed(c.getX(), c.getY() + 6400, c.getZ(),
						828, 1);
				break;
			case 24264:
				c.getPA().movePlayer(3229, 9904, 0);
				c.sendMessage("You climb down through the hole."); // varrock
																	// dungeon
																	// bear hole
				break;
			case 2352:
				c.getPA().movePlayer(2457, 4380, 0);
				c.sendMessage("You climb up the rope."); // climb up black
															// dragon rope
				break;
			case 12254:
				c.getPA().movePlayer(2830, 9827, 0);
				c.sendMessage("You climb down the rope."); // climb down the
															// black dragon rope
				break;
			case 5492:
				if (c.absX >= 3165 && c.absX <= 3166 && c.absY == 3251) {
					c.getPA().movePlayer(3149, 9652, 0);
				}
				break;
			// case 13999: // nex climb up rope
			// c.getPA().movePlayer(2884, 5274, 2);
			// break;
			// case 6455: // nex tunnel
			// c.getPA().movePlayer(2862, 5219, 0);
			// break;
			// case 6456: // nex tunnel
			// c.getPA().movePlayer(2859, 5219, 0);
			// break;
			// case 2794: // nex lever
			// c.getPA().movePlayer(2899, 5204, 0);
			// break;
			// case 2795: // nex lever
			// // add here 20 killcounts to pull lever!
			// c.getPA().movePlayer(2900, 5204, 0);
			// break;

			case 6673:
				if (c.absX == 2920 && c.absY == 5276) {
					c.getPA().movePlayer(2920, 5274, 0);
				}
				break;

			case 57225: // landslide to nex room
				if (c.absX == 2907 && c.absY == 5204) {
					c.getDH().sendDialogues(6673, 9985);
				} else {
					c.sendMessage("It's too slippery to climb.");
				}
				break;
			case 2362: // nomad brick
				if (c.absX == 3379 && c.absY == 9826) {
					c.getDH().sendDialogues(6675, 9985);
				} else {
					c.sendMessage("It's too far away to climb.");
				}
				break;
			case 57258: //
				if (c.getX() == 2900 && c.getY() == 5204) { // outside nex bank
															// room
					c.getPA().movePlayer(2899, 5204, 0);
					c.getCombat().resetPlayerAttack();
					c.inBossRoom = false;
				} else if (c.getX() == 2899 && c.getY() == 5204) { // inside nex  bank room
					if (c.hasFullAncientCeremonial()) {
						c.getPA().movePlayer(2900, 5204, 0);
						c.sendMessage("The ancient ceremonial allows you to pass through the door without minions kills.");
						c.inBossRoom = true;
						return true;
					}
					if (c.getZarosKC() > 14) {
						c.getPA().movePlayer(2900, 5204, 0);
						// c.getTempVar().put("ZarosKC",
						// c.getTempVar().getInt("ZarosKC") - 20);
						c.setZarosKC(-15, true);
						c.getPacketSender()
								.sendFrame126("" + c.getZarosKC() + "", 16221);
						c.inBossRoom = true;
					} else {
						c.sendMessage(
								"You need at least 15 Zaros KC to enter here!");
					}
				}
				break;

			case 57234:
				if (o.getX() == 2860 && o.getY() == 5219) {
					if (c.getPlayerLevel()[Skills.AGILITY] < 70) {
						c.sendMessage(
								"You need agility level of at least 70 to enter this course.");
						return true;
					}
				}
				if (c.getX() <= 2860) {
					if (c.getX() == 2859 && c.getY() == 5219) {
						CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

							@Override
							public void stop() {
							}

							@Override
							public void execute(CycleEventContainer container) {
								container.stop();
								c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
								c.setForceMovement(ForceMovementMask.createMask(3, 0, 3, 0, Direction.EAST));
							}
						}, 1);
					} else {
						c.getPA().playerWalk(2859, 5219);
					}
				} else {
					if (c.getX() == 2862 && c.getY() == 5219) {
						CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
							
							@Override
							public void stop() {
							}
							
							@Override
							public void execute(CycleEventContainer container) {
								container.stop();
								c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
								c.setForceMovement(ForceMovementMask.createMask(-3, 0, 3, 0, Direction.WEST));
							}
						}, 1);
					} else {
						c.getPA().playerWalk(2862, 5219);
					}
				}
				break;

			case 2282: // rope swing
				if (c.getX() == 2551 && c.getY() == 3554) {
					BarbarianCourse.ropeSwing(c, o);
				} else {
					c.getPA().playerWalk(2551, 3554);
				}
				break;
			case 2284: // climb up net
				if (c.getX() > o.getX()) {
					c.getPA().movePlayer(c.getX() - 1, c.getY(), 1);
					if (c.getAgility().barbarianCourseStatus == 2) {
						c.getAgility().barbarianCourseStatus = 3;
					}
					c.getPA().addSkillXP(8, Skills.AGILITY);
				} else {
					c.getPA().playerWalk(o.getX() + 1, o.getY());
				}
				break;
			case 2287: // squeeze through pipes to enter course
				if (o.getX() == 2552 && o.getY() == 3559) {
					if (c.getPlayerLevel()[Skills.AGILITY] < 35) {
						c.sendMessage(
								"You need agility level of at least 35 to enter this course.");
						return true;
					}
				}
				if (c.getY() >= 3560) {
					if (c.getX() == 2552 && c.getY() == 3561) {
						BarbarianCourse.pipeSqueeze(c, o);
					} else {
						c.getPA().playerWalk(2552, 3561);
					}
				} else {
					if (c.getX() == 2552 && c.getY() == 3558) {
						BarbarianCourse.pipeSqueeze(c, o);
					} else {
						c.getPA().playerWalk(2552, 3558);
					}
				}
				break;
			case 2294: // log balance
				BarbarianCourse.logBalance(c, o);
				break;
			case 2302: // walk ledge
				BarbarianCourse.balanceLedge(c, o);
				break;

			case 21314:
				// if (c.absX == 2355 && c.absY == 3839) {
				// c.getPA().walkTo(0, 0, 0, 9);
				// return true;
				// }
				c.getPA().walkTo(0, 9, true);
				break;

			case 21315:
				// if (c.absX == 2378 && c.absY == 3839) {
				// c.getPA().walkTo(0, 0, 0, -9);
				// return true;
				// }
				c.getPA().walkTo(0, -9, true);
				break;

			case 21312:
				// if (c.absX == 2355 && c.absY == 3839) {
				// c.getPA().walkTo(0, 0, 0, 9);
				// return true;
				// }
				c.getPA().walkTo(0, 9, true);
				break;

			case 21313:
				// if (c.absX == 2355 && c.absY == 3848) {
				// c.getPA().walkTo(0, 0, 0, -9);
				// return true;
				// }
				c.getPA().walkTo(0, -9, true);
				break;

			case 21308:
				// if (c.absX == 2343 && c.absY == 3820) {
				// c.getPA().walkTo(0, 0, 0, 9);
				// return true;
				// }
				c.getPA().walkTo(0, 9, true);
				break;

			case 21309:
				// if (c.absX == 2344 && c.absY == 3830) {
				// c.getPA().walkTo(0, 0, 0, -9);
				// return true;
				// }
				c.getPA().walkTo(0, -9, true);
				break;

			case 21306:
				// if (c.absX == 2317 && c.absY == 3823) {
				// c.getPA().walkTo(0, 0, 0, 9);
				// return true;
				// }
				c.getPA().walkTo(0, 9, true);
				break;

			case 21307:
				// if (c.absX == 2317 && c.absY == 3832) {
				// c.getPA().walkTo(0, 0, 0, -9);
				// return true;
				// }
				c.getPA().walkTo(0, -9, true);
				break;

			case 21310:
				// if (c.absX == 2314 && c.absY == 3839) {
				// c.getPA().walkTo(0, 0, 0, 9);
				// return true;
				// }
				c.getPA().walkTo(0, 9, true);
				break;

			case 21311:
				// if (c.absX == 2314 && c.absY == 3848) {
				// c.getPA().walkTo(0, -9, true);
				// return true;
				// }
				c.getPA().walkTo(0, -9, true);
				break;

			// wildy course
			case 2288:
				WildyCourse.obstaclePipe(c, o);
				break;

			case 2283: // rope swing
				if (c.getX() == 3005 && c.getY() == 3953) {
					WildyCourse.ropeSwing(c, o);
				} else {
					c.getPA().playerWalk(3005, 3953);
				}
				break;

			case 2311:
				WildyCourse.steppingStones(c, o);
				break;

			case 2297:
				WildyCourse.logBalance(c, o);
				break;

			case 2328:
				WildyCourse.climbRocks(c, o);
				break;

			case 2309:
			case 123555:
				if (c.getPlayerLevel()[Skills.AGILITY] < 52) {
					c.sendMessage(
							"You need agility level of at least 52 to enter this course.");
					return true;
				}
				WildyCourse.walkToWildyCourse(c, o);
				break;

			case 2307:
			case 123552:
				WildyCourse.leaveWildyCourse(c, o.getX(), o.getY());
				break;

			case 2308:
			case 123554:
			{
				final int gateX = 2998;
				final int gateY = 3931;

				if (c.getX() == gateX && gateY == c.getY()) {
					WildyCourse.leaveWildyCourse(c, gateX, gateY);
				}
				c.getPA().playerWalk(gateX, gateY);
				c.walkingToObject = true;
				c.clickObjectType = 1;
				c.objectId = 2307;
				c.objectX = gateX;
				c.objectY = gateY;
				c.finalLocalDestX = gateX;
				c.finalLocalDestY = gateY;
			}
				break;

			case 3192:
				if (o.getX() == 3356 && o.getY() == 3154) {
					ClwRedPortalScoreBoard.openScoreboard(c);
				} else
				if (o.getX() == 3083 && o.getY() == 3514) {
					c.getDialogueBuilder().sendOption("Select the Scoreboard to lookup",
							"Seasonal PK Scoreboard", ()->{
								WildyEloRatingSeason.openPKEloScoreboard(c);
							},
							"Overall Rating", ()->{
								c.getPA().openPKEloScoreboard();
							})
					.execute();
				} else if (o.getX() == 3368 && o.getY() == 3149) {
					c.getPA().openClanWarsEloScoreboard();
				} else {
					c.getPA().openDuelScoreboard();
				}
				break;

			case 2542: // revenant dungeon pipe
				c.getPA().movePlayer(3219, 9618, 0);
				c.sendMessage("You enter the tunnel.");
				break;

			case 26937:
			case 26941:
			case 26935:
			case 26939:
				c.getDH().sendOption4("Deep Wildy Lever <col=ff0000>(55 Wilderness)",
						"Ice Plateau <col=ff0000>(55 Wildereness)",
						"Wilderness Castle <col=ff0000>(55 Wilderness)",
						"Greater Demons Teleport <col=ff0000>(51 Wilderness)");
				c.dialogueAction = 26937;
				c.sendMessage(
						"<col=ff0000>You find a mysterious coffin with dangerous teleporting abilites.");
				break;

			case 103: // donator chest
				if (c.getDonatedTotalAmount() > 0) {
					c.dialogueAction = 777;
					c.getDH().sendOption2(
							"Open Donator Chest for 10 Donator Points",
							"No Thanks.");

				} else {
					c.sendMessage("This is a donator only chest.");
				}
				break;

			case 2403: // FunPK winners chest
				if (c.claimedReward) {
					return true;
				}
				if (rsObject != null) {
					if (rsObject.getObjectOwnerLong() == c.getLongName()) {
						if (c.getItems().freeSlots() < 7) { // needs to be the
															// amount of items
															// we
															// add to players
							c.sendMessage(
									"you don't have enough free space in your inventory.");
							return true;
						}
						if (FunPkTournament.donatorHost != null) { // reward
																	// donator
																	// tournament
							FunPkTournament.openChestFromDonatorTournament(c);
							rsObject.setTick(0);
							rsObject.setObjectOwnerLong(0);
							return true;
						}
						c.claimedReward = true;
						c.getPA().movePlayer(3303, 3123, 0);
						// TODO: add awards
						c.getItems().addItem(995,
								50000000 + Misc.random(100000000));
						c.getItems().addItem(6585, 1);
						c.getItems()
								.addItem(FunPkTournament.tournyRewards[Misc
										.random(FunPkTournament.tournyRewards.length
												- 1)],
										1);
						if (c.getClan() != null) {
							c.getClan().addClanPoints(c, 500, "win_funpk");
						}
						c.pkp += 1500;

						rsObject.setTick(0);
						rsObject.setObjectOwnerLong(0);
						return true;
					}
					// c.sendMessage("RSOBject != null and if did not move
					// player then in")
				}
				break;

			case 32665: // Tob deposit box
			case 9398:// deposit
				c.getPacketSender().sendFrame126(
						"The Bank of " + Config.SERVER_NAME + " - Deposit Box",
						7421);
				c.getPacketSender().sendFrame248(4465, 197);// 197 just because
															// you can't
				// see
				// it =\
				c.getItems().resetItems(7423);
				break;

			case 14233: // pest control doors
			case 14234:
			case 14235:
			case 14236:
				// c.sendMessage("Face "+o.getFace());
				PestDoorHandler.initializeDoors(c, objectType, obX, obY,
						c.heightLevel, def);
				break;

			// case 4465:
			// GameObject rO = RegionClip.getGameObject(obX,obY,0);
			// if(rO == null) return true;
			// new RSObject(-1, obX, obY, 0, rO.getFace(), 0, -1, 0, 1);
			// new RSObject(4466, obX-1, obY, 0, 1, 0, 4466, 0, 1);
			// break;
			//
			// case 4466:
			// GameObject rO2 = RegionClip.getGameObject(obX,obY,0);
			// if(rO2 == null) return true;
			// new RSObject(-1, obX, obY, 0, rO2.getFace(), 0, -1, 0, 1);
			// new RSObject(4465, obX+1, obY, 0, 0, 0, 4465, 0, 1);
			// break;

			case 8981: // bank chest in donator zone
			case 2693: // funpk bank
			case 102693: // bank chest in lms?
				c.getPA().openUpBank();
				break;
			case 21301: // neitiznot
				c.getPA().openUpBank();
				break;

			case 4031: // funpk crossing stone

				if (c.underAttackByPlayer()) {
					c.sendMessage("You cannot exit while in combat!");
					return true;
				}
				if (c.underAttackBy > 0) {
					c.sendMessage("You cannot exit while in combat!");
					return true;
				}

				if (c.noClip) {
					return true;
				}

				if (FunPkTournament.isTournamentOpen()) {
					if (c.absY == 3115) { // inside funpk area
						if (c.absX < 3303) {
							if (FunPkTournament.leaveGame(c)) {
								c.getPA().walkTo(1, 0, 0, 3);
								return true;
							}
						} else if (c.absX > 3304) {
							if (FunPkTournament.leaveGame(c)) {
								c.getPA().walkTo(-1, 0, 0, 3);
								return true;
							}
						}
						if (FunPkTournament.leaveGame(c)) { // i wonder wtf this
															// is lol, you added
															// this
							c.getPA().walkTo(0, 3, true);
							return true;
						}
					} else {
						if (FunPkTournament.enterGame(c)) {
							c.getPA().walkTo(0, -3, true);
							c.getDotManager().curePoison();
							return true;
						}
					}
				} else {

					if (c.absY == 3115) { // inside funpk area
						if (c.absX < 3303) {
							c.getPA().walkTo(1, 0, 0, 3);
							return true;
						} else if (c.absX > 3304) {
							c.getPA().walkTo(-1, 0, 0, 3);
							return true;
						}

						c.getPA().walkTo(0, 3, true);
						return true;
					} else {
						c.getPA().walkTo(0, -3, true);
						// for (int i = 0; i < c.playerLevel.length; i++) {
						// c.playerLevel[i] = c.getLevelForXP(c.playerXP[i]);
						// c.getPA().refreshSkill(i);
						// }
						c.getDotManager().curePoison();
						// c.getPA().refreshSkill(5);
						return true;
					}
				}
				break;

			case 9368: // enter the fight pits arena
				if (c.getY() == 5169) {
					FightPitsTournament.enterGame(c);
				} else if (c.getY() == 5167) {
					FightPitsTournament.leaveGame(c);
				}
				break;

			case 9369: // Enter fight pits wait/watch room
				if (c.getY() == 5177) {
					c.getPA().walkTo(0, -2, true);
				} else if (c.getY() == 5175) {
					c.getPA().walkTo(0, 2, true);
				}
				break;

			case 1816:
				if ((o.getX() == 3067) && (o.getY() == 10253)) {
					c.getPA().startLeverTeleport(2271, 4681, 0, "lever");
				}
				break;
			// 3003 3849
			case 1817:// torm && kbd
				if ((o.getX() == 2271) && (o.getY() == 4680)) { // kbd
					c.getPA().startLeverTeleport(3067, 10253, 0, "lever");
					break;
				}
				c.getPA().movePlayer(3087, 3499, 0);
				break;
			case 9357:// tzhaar
				c.getPA().movePlayer(2438, 5168, 0);
				break;
			case 1734:
				if (obX == 3059 && obY == 9776) {
					c.getPA().movePlayer(3061, 3376, 0);
				} else if (obX == 3044 && obY == 10324) {
					c.getPA().movePlayer(3045, 3927, 0);
				}
				break;
			case 14314:
				c.getPA().movePlayer(2657, 2639, 0);
				break;
			case 14315:
				// c.sendMessage("Sorry Pest Control has been temporarily
				// disabled.");
				if (c.combatLevel < 40) {
					c.sendMessage(
							"Your combat must at least be 40 to enter this boat.");
					return true;
				}
				if (!DBConfig.MINI_GAMES) {
					c.sendMessage(
							"Minigames have temporarily been disabled by the admins.");
					return true;
				}
				c.getPA().movePlayer(2661, 2639, 0);
				break;
			case 245:
				c.getPA().movePlayer(c.absX, c.absY + 2, 2);
				break;
			case 246:
				c.getPA().movePlayer(c.absX, c.absY - 2, 1);
				break;
			case 272:
				c.getPA().movePlayer(c.absX, c.absY, 1);
				break;
			case 273:
				c.getPA().movePlayer(c.absX, c.absY, 0);
				break;
			/* Godwars Door */
			case 26426: // armadyl
				/*
				 * if (c.getTempVar().getInt("ArmaKC") < 5 && c.absX == 2839 &&
				 * c.absY == 5295) {
				 * c.sendMessage("You need atleast 5 Armadyl KC to enter here!"
				 * ); return true; }
				 */
				if (c.absX == 2839 && c.absY == 5295) {
					// c.Arma -= 5;
					// c.getTempVar().remove("ArmaKC");
					// c.getTempVar().put("ArmaKC",
					// c.getTempVar().getInt("ArmaKC")
					// - 5);
					c.getPA().movePlayer(2839, 5296, 2);
					c.sendMessage("<col=ff>Good luck!");
					c.inBossRoom = true;
				} else {
					c.getPA().movePlayer(2839, 5295, 2);
					// c.autoRet = 0;
					c.getCombat().resetPlayerAttack();
					c.inBossRoom = false;
				}
				break;
			case 26425: // bandos
				/*
				 * if (c.getTempVar().getInt("BandosKC") < 5 && c.absX == 2863
				 * && c.absY == 5354) {
				 * c.sendMessage("You need atleast 5 Bandos KC to enter here!");
				 * return true; }
				 */
				if (c.absX == 2863 && c.absY == 5354) {
					// c.Band -= 5;
					// c.getTempVar().remove("BandosKC");
					// c.getTempVar().put("BandosKC",
					// c.getTempVar().getInt("BandosKC") - 5);
					c.getPA().movePlayer(2864, 5354, 2);
					c.sendMessage("<col=ff>Good luck!");
					c.inBossRoom = true;
				} else {
					c.getPA().movePlayer(2863, 5354, 2);
					// c.autoRet = 0;
					c.getCombat().resetPlayerAttack();
					c.inBossRoom = false;
				}
				break;
			case 26428: // Zammy
				/*
				 * if (c.getTempVar().getInt("ZammyKC") < 5 && c.absX == 2925 &&
				 * c.absY == 5332) {
				 * c.sendMessage("You need atleast 5 Armadyl KC to enter here!"
				 * ); return true; }
				 */
				if (c.absX == 2925 && c.absY == 5332) {
					// c.Zammy -= 5;
					// c.getTempVar().remove("ZammyKC");
					// c.getTempVar().put("ZammyKC",
					// c.getTempVar().getInt("ZammyKC") - 5);
					c.getPA().movePlayer(2925, 5331, 2);
					c.sendMessage("<col=ff>Good luck!");
					c.inBossRoom = true;
				} else {
					c.getPA().movePlayer(2925, 5332, 2);
					// c.autoRet = 0;
					c.getCombat().resetPlayerAttack();
					c.inBossRoom = false;
				}
				break;
			case 26427: // sara
				/*
				 * if (c.getTempVar().getInt("SaraKC") < 5 && c.absX == 2908 &&
				 * c.absY == 5265) { c.
				 * sendMessage("You need atleast 5 Saradomin KC to enter here!"
				 * ); return true; }
				 */
				if (c.absX == 2908 && c.absY == 5265) {
					// c.getTempVar().remove("SaraKC");
					// c.getTempVar().put("SaraKC",
					// c.getTempVar().getInt("SaraKC")
					// - 5);
					// c.Sara -= 5;
					c.getPA().movePlayer(2907, 5265, 0);
					c.sendMessage("<col=ff>Good luck!");
					c.inBossRoom = true;
				} else {
					c.getPA().movePlayer(2908, 5265, 0);
					// c.autoRet = 0;
					c.getCombat().resetPlayerAttack();
					c.inBossRoom = false;
				}
				break;
			case 26384:// bandos room
				if (c.absX == 2851 && c.absY == 5333) {
					c.getPA().movePlayer(2850, 5333, 2);
					c.sendMessage("<col=ff>Bandos Room!");
				} else {
					c.getPA().movePlayer(2851, 5333, 2);
				}
				break;
			case 26303:// Armdayl Room
				if (c.absX == 2871 && c.absY == 5279) {
					c.getPA().movePlayer(2871, 5269, 2);
					c.sendMessage("<col=ff>Armadyl Room!");
				} else {
					c.getPA().movePlayer(2871, 5279, 2);
				}
				break;
			case 6672:// saradomin room
				if (c.absX == 2921 && c.absY == 5276) {
					c.getPA().movePlayer(2921, 5274, 0);
				} else {
					if (c.absX == 2920 && c.absY == 5276) {
						c.getPA().movePlayer(2920, 5274, 0);
					} else {
						if (c.absX == 2912 && c.absY == 5300) {
							c.getPA().movePlayer(2914, 5300, 1);
						}
					}
				}
				break;
			case 9299:// Lumbridge swamp fence
				if (c.absX == 3240 && c.absY == 3191) {
					c.getPA().movePlayer(3240, 3190, 0);
					c.sendMessage("You squeeze through the fence");
				} else {
					if (c.absX == 3240 && c.absY == 3190) {
						c.getPA().movePlayer(3240, 3191, 0);
						c.sendMessage("You squeeze through the fence");
					}
				}
				break;
			case 9326:// slayer dungeon
				if (c.absX == 2775 && c.absY == 10003) {
					c.getPA().movePlayer(2768, 10002, 0);
					c.sendMessage("You jump over the trap!");
				} else {
					c.getPA().movePlayer(2775, 10003, 0);
					c.sendMessage("You jump over the trap!");
				}
				break;
			case 9321:// slayer dungeon
				if (c.absX == 2735 && c.absY == 10008) {
					c.getPA().movePlayer(2730, 10008, 0);
					c.sendMessage("You squeeze through the wall");
				} else {
					c.getPA().movePlayer(2735, 10008, 0);
				}
				break;
			case 5960:
				if (o.getX() == 3090 && o.getY() == 3475) {
					c.getPA().startLeverTeleport(3155, 3925, 0, "lever");
				} else {
					c.getPA().startLeverTeleport(3090, 3956, 0, "lever");
				}
				break;
			case 5008:// rock crabs tunnel
				if (c.absX == 2691 && c.absY >= 3718 && c.absY <= 3719) {
					c.getPA().movePlayer(2687, 3719, 0);
					c.sendMessage("You walk through the tunnel.");
				} else if (c.absX == 2687 && c.absY >= 3718 & c.absY <= 3720) {
					c.getPA().movePlayer(2691, 3719, 0);
					c.sendMessage("You walk through the tunnel.");
				}
				break;
			
			 case 15115:// dz dungeon c.getPA().movePlayer(2509, 3847, 0);
			      c.sendMessage("You climb up the ladder"); break; 
			 case 15116:// dz dungeon 
				 c.getPA().movePlayer(2509, 10247, 0);
			     c.sendMessage("You climb up the ladder"); 
			 break;
			 
			case 15186:// dz dungeon
				c.getPA().movePlayer(2505, 10283, 0);
				c.sendMessage("You squeeze through the wall");
				break;
			case 15187:// dz dungeon
				c.getPA().movePlayer(2505, 10280, 0);
				c.sendMessage("You squeeze through the wall");
				break;
			case 4413:// iron ladder
				if (c.absX == 2507 && c.absY == 10287) {
					c.getPA().movePlayer(2510, 10287, 1);
					c.sendMessage("You climb up the ladder");
				} else if (c.absX == 2515 && c.absY == 4632) {
					c.getPA().movePlayer(2515, 4629, 1);
					c.sendMessage("You climb up the ladder");
				}
				break;
			case 4485:// iron ladder
				if (c.absX == 2510 && c.absY == 10287) {
					c.getPA().movePlayer(2507, 10287, 0);
					c.sendMessage("You climb down the ladder");
				} else if (c.absX == 2515 && c.absY == 4629) {
					c.getPA().movePlayer(2515, 4632, 0);
					c.sendMessage("You climb down the ladder");
				}
				break;
			case 15188:// dz dungeon
				if (c.getDonatedTotalAmount() > 0) {
					c.getPA().movePlayer(2514, 10291, 0);
					c.sendMessage("You enter the dungeon");
				} else {
					c.sendMessage(
							"You need to be bronze donator or higher to enter this dungeon!");
				}
				break;
			case 15189:// dz dungeon
				c.getPA().movePlayer(2510, 10287, 1);
				c.sendMessage("You leave the dungeon");
				break;
			case 3254:// dz dungeon
				if (c.getDonatedTotalAmount() >= 50) {
					if (c.getX() == 2542) {
						c.getPA().movePlayer(2538, 10298, 0);
					} else {
						c.getPA().movePlayer(2542, 10298, 0);
						c.sendMessage("You cross the bridge");
					}
				} else {
					c.sendMessage(
							"You need to be silver donator or higher to enter this dungeon!");
				}
				break;
			case 15213: // stepping stone
				if (c.getX() == 2549) {
					c.getPA().movePlayer(2538, 10286, 0);
				} else {
					c.getPA().movePlayer(2549, 10288, 0);
				}
				break;
			case 15216:// dz dungeon
				c.getPA().movePlayer(2537, 10297, 0);
				c.sendMessage("You swing on the rope");
				break;
			/*
			 * case 5847:// dz dungeon if (c.donatedTotalAmount >= 200) { if
			 * (c.absX == 2538) { c.getPA().movePlayer(2541, 10278, 0);
			 * c.sendMessage("You climb over the rock"); } if (c.absX == 2541 /*
			 * &&c.absY == 10279
			 *//*
				 * ) { c.sendMessage("You climb over the rock");
				 * c.getPA().movePlayer(2538, 10278, 0);
				 * c.sendMessage("You climb over the rock"); } } else { c.
				 * sendMessage("You need to be gold donator or higher to enter this dungeon!"
				 * ); } break;
				 */
			case 15194:
				if (c.getDonatedTotalAmount() >= 500) {
					c.getPA().movePlayer(2585, 10262, 0);
					c.sendMessage("You squeeze through the wall");
				} else {
					c.sendMessage(
							"You need to be platinum donator or higher to enter this dungeon!");
				}
				break;
			case 15195:// dz dungeon
				c.getPA().movePlayer(2585, 10259, 0);
				c.sendMessage("You squeeze through the wall");
				break;
			case 15196:
				if (c.getDonatedTotalAmount() >= 1000) {
					c.getPA().movePlayer(2617, 10274, 0);
					c.sendMessage("You squeeze through the wall");
				} else {
					c.sendMessage(
							"You need to be diamond donator to enter this dungeon!");
				}
				break;
			case 15197:// dz dungeon
				c.getPA().movePlayer(2617, 10271, 0);
				c.sendMessage("You squeeze through the wall");
				break;

			case 411:
			case 100411://osrs wild
				if (c.playerPrayerBook == PrayerBook.NORMAL) {
					c.playerPrayerBook = PrayerBook.CURSES;
					c.sendMessage("You sense a surge of power flow through your body!");
				} else {
					c.playerPrayerBook = PrayerBook.NORMAL;
					c.sendMessage("You sense a surge of purity flow through your body!");
				}

				c.getCombat().resetPrayers();
				c.startAnimation(645);
				c.updatePrayerBook();
				break;

			case 21764: // supreme donator zone altar
				if (System.currentTimeMillis() - c.gwdPrayerDelay > 60000) {
					c.getSkills().healToFull();
					c.getSkills().setPrayerPointsToMax();
					c.startAnimation(645);
					c.specAmount = 10.0;
					c.getItems().addSpecialBar(true);
					c.getDotManager().clearPoison();
					c.getDotManager().clearVenom();
					c.sendMessage(
							"<col=ff0000>You've recharged your special attack, prayer points, hitpoints and cured venom.");
					c.gwdPrayerDelay = System.currentTimeMillis();
				} else {
					c.sendMessage("You can only use the altar every 1 minute.");
				}
				break;

			case 3223:
			case 26289:
			case 26288:
			case 26287:
			case 26286:
				if (System.currentTimeMillis() - c.gwdPrayerDelay > 300000) {
					c.getSkills().healToFull();
					c.getSkills().setPrayerPointsToMax();
					c.startAnimation(645);
					c.specAmount = 10.0;
					c.getItems().addSpecialBar(true);
					c.sendMessage(
							"<col=ff0000>You've recharged your special attack, prayer points and hitpoints.");
					c.gwdPrayerDelay = System.currentTimeMillis();
				} else {
					c.sendMessage(
							"You can only use the altar every 5 minutes.");
				}
				break;
			case 9706:
			case 109706:
				c.getPA().startLeverTeleport(3105, 3951, 0, "lever");
				break;
			case 9707:
			case 109707:
				c.getPA().startLeverTeleport(3105, 3956, 0, "lever");
				break;
			case 5959:
			case 105959:
				c.getPA().startLeverTeleport(2539, 4712, 0, "lever");
				break;
			case 1814:
				c.getPA().startLeverTeleport(3153, 3923, 0, "lever");
				break;
			case 1815:
				if (o.getX() == 3153 && o.getY() == 3923) { // lever in wildy to
															// go to ardy
					c.getPA().startLeverTeleport(2561, 3311, 0, "lever");
				} else {
					c.getPA().startLeverTeleport(3087, 3500, 0, "lever");
				}
				break;
			/* Start Brimhavem Dungeon */
			case 2879:
				c.getPA().movePlayer(2542, 4718, 0);
				break;
			case 2878:
				c.getPA().movePlayer(2509, 4689, 0);
				break;
			case 5083:
				c.getPA().movePlayer(2713, 9564, 0);
				c.sendMessage("You enter the dungeon.");
				break;

			case 5103:
				if (c.absX == 2691 && c.absY == 9564) {
					c.getPA().movePlayer(2689, 9564, 0);
				} else if (c.absX == 2689 && c.absY == 9564) {
					c.getPA().movePlayer(2691, 9564, 0);
				}
				break;

			case 5106:
				if (c.absX == 2674 && c.absY == 9479) {
					c.getPA().movePlayer(2676, 9479, 0);
				} else if (c.absX == 2676 && c.absY == 9479) {
					c.getPA().movePlayer(2674, 9479, 0);
				}
				break;

			case 5105:
				if (c.absX == 2672 && c.absY == 9499) {
					c.getPA().movePlayer(2674, 9499, 0);
				} else if (c.absX == 2674 && c.absY == 9499) {
					c.getPA().movePlayer(2672, 9499, 0);
				}
				break;

			case 5107:
				if (c.absX == 2693 && c.absY == 9482) {
					c.getPA().movePlayer(2695, 9482, 0);
				} else if (c.absX == 2695 && c.absY == 9482) {
					c.getPA().movePlayer(2693, 9482, 0);
				}
				break;

			case 5104:
				if (c.absX == 2683 && c.absY == 9568) {
					c.getPA().movePlayer(2683, 9570, 0);
				} else if (c.absX == 2683 && c.absY == 9570) {
					c.getPA().movePlayer(2683, 9568, 0);
				}
				break;

			case 5100:
				if (o.getX() == 2655 && o.getY() == 9567
						|| o.getX() == 2655 && o.getY() == 9572) {
					if (c.getPlayerLevel()[Skills.AGILITY] < 22) {
						c.sendMessage(
								"You need agility level of at least 22 to enter this course.");
						return true;
					}
				}
				if (c.getY() == 9566) {
					if (c.getX() == 2655 && c.getY() == 9566) {
						if (c.getY() == 9566) {
							CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
								int timer = 0;
								@Override
								public void stop() {
								}

								@Override
								public void execute(CycleEventContainer container) {
									timer++;
									if (timer == 1) {
										c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
										c.setForceMovement(ForceMovementMask.createMask(0, 7, 7, 0, Direction.NORTH));
									}
									
									if (timer == 5) {
										c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
										container.stop();
									}
								}
							}, 1);
						} else {
							c.getPA().playerWalk(2655, 9566);
						}
					}
				} else {
					if (c.getX() == 2655 && c.getY() == 9573) {
						if (c.getY() == 9573) {
							CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
								int timer = 0;
								@Override
								public void stop() {
								}

								@Override
								public void execute(CycleEventContainer container) {
									timer++;
									if (timer == 1) {
									c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
									c.setForceMovement(ForceMovementMask.createMask(0, -7, 7, 0, Direction.SOUTH));
									}
									
									if (timer == 5) {
										c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
										container.stop();
									}
								}
							}, 1);
						} else {
							c.getPA().playerWalk(2655, 9573);
						}
					}
				}
				break;

			case 5099:
				if (c.absY <= 9493) {
					c.getPA().movePlayer(2698, 9500, 0);
				} else if (c.absY >= 9499) {
					c.getPA().movePlayer(2698, 9492, 0);
				}
				break;

			case 5088:
				c.getPA().movePlayer(2687, 9506, 0);
				c.sendMessage("You cross the lava");
				break;
			case 5090:
				c.getPA().movePlayer(2682, 9506, 0);
				c.sendMessage("You cross the lava");
				break;
			case 10596:
				c.getPA().movePlayer(3056, 9555, 0);
				break;
			case 10595:
				c.getPA().movePlayer(3056, 9562, 0);
				break;
			case 23156:// forbidden dungeon
				if (c.absX == 2707 && c.absY == 10147) {
					c.getPA().movePlayer(2706, 10147, 0);
					c.sendMessage(
							"<col=ff>You search the picture wall and find a secret door");
				} else if (c.absX == 2706 && c.absY == 10147) {
					c.getPA().movePlayer(2707, 10147, 0);
					c.sendMessage("<col=ff>You enter the secret door");
				}
				break;
			case 23216:// forbidden dungeon
				if (c.absX == 2725 && c.absY == 10168) {
					c.getPA().movePlayer(2726, 10168, 0);
					c.sendMessage("<col=ff>You enter the gate");
				} else if (c.absX == 2726 && c.absY == 10168) {
					c.getPA().movePlayer(2725, 10168, 0);
					c.sendMessage("<col=ff>You enter the gate");
				}
				break;
			case 5947:// dark hole mith drags
				if (obX == 1764 && obY == 9504) {
					c.getPA().movePlayer(1764, 5365, 1);
					c.sendMessage("You climb down the dark hole");
				}
				break;
			case 3829:// mith drag rope
				c.getPA().movePlayer(2741, 9503, 0);
				c.sendMessage("You climb up the rope");
				break;
			case 3832:// kq
				c.getPA().movePlayer(3510, 9496, 2);
				c.sendMessage("You climb up the rope");
				break;
			case 23610:// kq
				c.getPA().movePlayer(3507, 9494, 0);
				c.sendMessage("You climb down the rope");
				break;
			case 1762:// kq
				if (o.getX() == 3462 && o.getY() == 9481) {
					c.getPA().movePlayer(2721, 4911, 0);
				} else if (o.getX() == 2833 && o.getY() == 9657) {
					c.getPA().movePlayer(2834, 3255, 0);
				} else {
					c.sendMessage("This rope isn't coded yet. Report this to the staff please. X:"+ o.getX() + " Y:"+o.getY()+ " Z:"+ o.getZ());
					break;
				}
				c.sendMessage("You climb up the rope");
				break;
			case 3831:// kq
				c.getPA().movePlayer(3463, 9481, 2);
				c.sendMessage("You climb down the rope");
				break;
			case 25338:// mith drag stairs
				if (c.absX == 1768 && c.absY == 5366) {
					c.getPA().movePlayer(1772, 5366, 0);
				} else if (c.absX == 1744 && c.absY == 5321) {
					c.getPA().movePlayer(1744, 5325, 0);
				}
				break;
			case 25339:// mith drag stairs
				c.getPA().movePlayer(1778, 5343, 1);
				break;
			case 25340:// mith drag stairs
				c.getPA().movePlayer(1778, 5346, 0);
				break;
			case 25336:// mith drag stairs
				if (c.absX == 1772 && c.absY == 5366) {
					c.getPA().movePlayer(1768, 5366, 1);
				} else if (c.absX == 1744 && c.absY == 5325) {
					c.getPA().movePlayer(1744, 5321, 1);
				}
				break;
			case 25337:// mith drag stairs
				c.getPA().movePlayer(1744, 5321, 1);
				break;
			// slayer tower
			case 4493:
				c.getPA().movePlayer(3433, 3538, 1);
				break;
			case 4494:
				c.getPA().movePlayer(3438, 3538, 0);
				break;
			case 4495:
				c.getPA().movePlayer(3417, 3540, 2);
				break;
			case 4496:
				c.getPA().movePlayer(3412, 3540, 1);
				break;
			case 9320:
				if (o.getLocation().getX() == 3422 && o.getLocation().getY() == 3550 && o.getLocation().getZ() == 1) {
					c.getPA().movePlayer(3422, 3549, 0);
				} else if (o.getLocation().getX() == 3447 && o.getLocation().getY() == 3576 && o.getLocation().getZ() == 2) {
					c.getPA().movePlayer(3447, 3575, 1);
				}
				break;
			case 9319:
				if (o.getLocation().getX() == 3422 && o.getLocation().getY() == 3550 && o.getLocation().getZ() == 0) {
					c.getPA().movePlayer(3422, 3551, 1);
				} else if (o.getLocation().getX() == 3447 && o.getLocation().getY() == 3576 && o.getLocation().getZ() == 1) {
					c.getPA().movePlayer(3447, 3577, 2);
				}
				break;
			// end slayer tower
			case 5110:
				c.getPA().movePlayer(2647, 9557, 0);
				c.sendMessage("You cross the lava");
				break;

			case 5111:
				c.getPA().movePlayer(2649, 9562, 0);
				c.sendMessage("You cross the lava");
				break;

			case 5084:
				c.getPA().movePlayer(2744, 3151, 0);
				c.sendMessage("You exit the dungeon.");
				break;
			/* End Brimhavem Dungeon */
			case 5098:
				c.getPA().movePlayer(2637, 9517, 0);
				break;
			case 5097:
				c.getPA().movePlayer(2637, 9510, 2);
				break;
			// case 6481:
			// c.getPA().movePlayer(3233, 9315, 0);
			// break;
			case 17010:
				if (c.getItems().playerHasItem(Runecrafting.RUNE_ESSENCE) || c.getItems().playerHasItem(Runecrafting.PURE_ESSENCE)) {
					c.getRunecrafting().craftRunes(RunecraftingData.ASTRAL_RUNE);
					break;
				}

				SpellBook switchTo;
				if (c.playerMagicBook == SpellBook.NORMAL || c.playerMagicBook == SpellBook.ANCIENT) {
					c.sendMessage("You switch spellbook to lunar magic.");
					switchTo = SpellBook.LUNARS;
				} else {
					switchTo = SpellBook.NORMAL;
					c.sendMessage("You feel a drain on your memory.");
				}

				c.getPA().closeAllWindows();
				c.getPA().resetAutocast();
				c.setSpellbook(switchTo);
				break;

			/*
			 * case 1551: if (c.absX == 3252 && c.absY == 3266) {
			 * c.getPA().movePlayer(3253, 3266, 0); } if (c.absX == 3253 &&
			 * c.absY == 3266) { c.getPA().movePlayer(3252, 3266, 0); } break;
			 * case 1553: if (c.absX == 3252 && c.absY == 3267) {
			 * c.getPA().movePlayer(3253, 3266, 0); } if (c.absX == 3253 &&
			 * c.absY == 3267) { c.getPA().movePlayer(3252, 3266, 0); } break;
			 */

			case 2491:
				Mining.mineEss(c, objectType);
				break;
			case 3044:
			case 21303:
				Smelting.sendSmelting(c);
				break;
			// abyss rifts
			case 7129: // fire riff
				c.getPA().movePlayer(Runecrafting.FIRE_ALTAR);
				break;
				
			case 2482: // fire altar
			    c.getRunecrafting().craftRunes(RunecraftingData.FIRE_RUNE);
			    break;
				
			case 7130: // earth riff
				c.getPA().movePlayer(Runecrafting.EARTH_ALTAR);
				break;
				
			case 2481: // earth altar
			    c.getRunecrafting().craftRunes(RunecraftingData.EARTH_RUNE);
			    break;				
				
			case 7131: // body riff
				c.getPA().movePlayer(Runecrafting.BODY_ALTAR);
				break;
				
			case 2483: // body altar
			    c.getRunecrafting().craftRunes(RunecraftingData.BODY_RUNE);
			    break;				
				
			case 7132: // cosmic riff
				c.getPA().movePlayer(Runecrafting.COSMIC_ALTAR);
				break;
				
			case 2484: // cosmic altar
			    c.getRunecrafting().craftRunes(RunecraftingData.COSMIC_RUNE);
			    break;				
				
			case 7133: // nat riff				
				c.getPA().movePlayer(Runecrafting.NATURE_ALTAR);
				break;
				
			case 2486: // nature altar
			    c.getRunecrafting().craftRunes(RunecraftingData.NATURE_RUNE);
			    break;
				
			case 2487: // chaos altar			    
			    c.getRunecrafting().craftRunes(RunecraftingData.CHAOS_RUNE);
			    break;
			    
			case 7134: // chaos riff
				c.getPA().movePlayer(Runecrafting.CHAOS_ALTAR);
				break;				
				
			case 7135: // law riff				
				c.getPA().movePlayer(Runecrafting.LAW_ALTAR);
				break;
				
			case 2485:
			    c.getRunecrafting().craftRunes(RunecraftingData.LAW_RUNE);
			    break;
				
			case 7136: // death riff				
				c.getPA().movePlayer(Runecrafting.DEATH_ALTAR);
				break;
				
			case 2488:
			    c.getRunecrafting().craftRunes(RunecraftingData.DEATH_RUNE);
			    break;
				
			case 7137: // water riff				
				c.getPA().movePlayer(Runecrafting.WATER_ALTAR);
				break;
				
			case 2480:
			    c.getRunecrafting().craftRunes(RunecraftingData.WATER_RUNE);
			    break;			
				
			case 7138: // soul riff
			    // have to use the rift because soul altar didn't come out during this time
			    c.getRunecrafting().craftRunes(RunecraftingData.SOUL_RUNE);
			    break;
				
			case 7139: // air riff				
				c.getPA().movePlayer(Runecrafting.AIR_ALTAR);
				break;
				
			case 2478:
			    
			    if (c.getItems().playerHasItem(Runecrafting.DUST_OF_ARMADYL)) {
				c.getRunecrafting().craftArmadylRunes();
			    } else {
				c.getRunecrafting().craftRunes(RunecraftingData.AIR_RUNE);
			    }
			    break;				
				
			case 7140: // mind riff				
				c.getPA().movePlayer(Runecrafting.MIND_ALTAR);
				break;
				
			case 2479:
			    c.getRunecrafting().craftRunes(RunecraftingData.MIND_RUNE);
			    break;
				
			case 7141: // blood rift
			    	c.getPA().movePlayer(Runecrafting.BLOOD_ALTAR);
				break;
				
			case 30624:
			    c.getRunecrafting().craftRunes(RunecraftingData.BLOOD_RUNE);
			    break;

			/* AL KHARID */
			case 2883:
			case 2882:
				c.getDH().sendDialogues(1023, 925);
				break;
			case 2412:
				Sailing.startTravel(c, 1);
				break;
			case 2414:
				Sailing.startTravel(c, 2);
				break;
			case 2083:
				Sailing.startTravel(c, 5);
				break;
			case 2081:
				Sailing.startTravel(c, 6);
				break;
			case 14304:
				Sailing.startTravel(c, 14);
				break;
			case 14306:
				Sailing.startTravel(c, 15);
				break;

			case 2213:
			case 3045:
			case 14367:
			case 11758:
			case 3193:
			case 30087:
			case 10517:
			case 11402:
			case 26972:
			case 4483:
			case 16700:
			case 29085:
			case 42192:
			case 19230:
				c.getPA().openUpBank();
				break;

			/**
			 * Entering the Fight Caves.
			 */
			case 9356:
				FightCaves.enterCaves(c);
				c.sendMessage("Good Luck!");
				break;

			/**
			 * Clicking on the Ancient Altar.
			 */
			case 6552:
				c.getPA().closeAllWindows();
				if (c.playerMagicBook == SpellBook.NORMAL) {
					c.setSpellbook(SpellBook.ANCIENT);
					c.sendMessage("An ancient wisdomin fills your mind.");
					c.getPA().resetAutocast();
					c.dialogueAction = 0;
					c.getPA().closeAllWindows();
				} else {
					c.setSpellbook(SpellBook.NORMAL);
					c.sendMessage("You feel a drain on your memory.");
					c.getPA().resetAutocast();
					c.dialogueAction = 0;
					c.getPA().closeAllWindows();
				}
				break;

			/**
			 * Recharing prayer points.
			 */
			case 409:
				Prayer.clickAltar(c);
				break;

			/**
			 * Aquring god capes.
			 */
			case 2873:
				if (!c.getItems().ownsCape()) {
					c.startAnimation(645);
					c.sendMessage("Saradomin blesses you with a cape.");
					c.getItems().addItem(2412, 1);
				} else {
					c.sendMessage("You already have a cape");
				}
				break;
			case 2875:
				if (!c.getItems().ownsCape()) {
					c.startAnimation(645);
					c.sendMessage("Guthix blesses you with a cape.");
					c.getItems().addItem(2413, 1);
				} else {
					c.sendMessage("You already have a cape");
				}
				break;
			case 2874:
				if (!c.getItems().ownsCape()) {
					c.startAnimation(645);
					c.sendMessage("Zamorak blesses you with a cape.");
					c.getItems().addItem(2414, 1);
				} else {
					c.sendMessage("You already have a cape");
				}
				break;

			/**
			 * Clicking certain doors.
			 */
			case 6749:
				if (obX == 3562 && obY == 9678) {
					c.getPacketSender().addObjectPacket(3562, 9678, 6749, 0, -3);
					c.getPacketSender().addObjectPacket(3562, 9677, 6730, 0, -1);
				} else if (obX == 3558 && obY == 9677) {
					c.getPacketSender().addObjectPacket(3558, 9677, 6749, 0, -1);
					c.getPacketSender().addObjectPacket(3558, 9678, 6730, 0, -3);
				}
				break;

			case 6730:
				if (obX == 3558 && obY == 9677) {
					c.getPacketSender().addObjectPacket(3562, 9678, 6749, 0, -3);
					c.getPacketSender().addObjectPacket(3562, 9677, 6730, 0, -1);
				} else if (obX == 3558 && obY == 9678) {
					c.getPacketSender().addObjectPacket(3558, 9677, 6749, 0, -1);
					c.getPacketSender().addObjectPacket(3558, 9678, 6730, 0, -3);
				}
				break;

			case 6727:
				if (obX == 3551 && obY == 9684) {
					c.sendMessage("You cant open this door..");
				}
				break;

			case 6746:
				if (obX == 3552 && obY == 9684) {
					c.sendMessage("You cant open this door..");
				}
				break;

			case 6748:
				if (obX == 3545 && obY == 9678) {
					c.getPacketSender().addObjectPacket(3545, 9678, 6748, 0, -3);
					c.getPacketSender().addObjectPacket(3545, 9677, 6729, 0, -1);
				} else if (obX == 3541 && obY == 9677) {
					c.getPacketSender().addObjectPacket(3541, 9677, 6748, 0, -1);
					c.getPacketSender().addObjectPacket(3541, 9678, 6729, 0, -3);
				}
				break;

			case 6729:
				if (obX == 3545 && obY == 9677) {
					c.getPacketSender().addObjectPacket(3545, 9678, 6748, 0, -3);
					c.getPacketSender().addObjectPacket(3545, 9677, 6729, 0, -1);
				} else if (obX == 3541 && obY == 9678) {
					c.getPacketSender().addObjectPacket(3541, 9677, 6748, 0, -1);
					c.getPacketSender().addObjectPacket(3541, 9678, 6729, 0, -3);
				}
				break;

			case 6726:
				if (obX == 3534 && obY == 9684) {
					c.getPacketSender().addObjectPacket(3534, 9684, 6726, 0, -4);
					c.getPacketSender().addObjectPacket(3535, 9684, 6745, 0, -2);
				} else if (obX == 3535 && obY == 9688) {
					c.getPacketSender().addObjectPacket(3535, 9688, 6726, 0, -2);
					c.getPacketSender().addObjectPacket(3534, 9688, 6745, 0, -4);
				}
				break;

			case 6745:
				if (obX == 3535 && obY == 9684) {
					c.getPacketSender().addObjectPacket(3534, 9684, 6726, 0, -4);
					c.getPacketSender().addObjectPacket(3535, 9684, 6745, 0, -2);
				} else if (obX == 3534 && obY == 9688) {
					c.getPacketSender().addObjectPacket(3535, 9688, 6726, 0, -2);
					c.getPacketSender().addObjectPacket(3534, 9688, 6745, 0, -4);
				}
				break;

			case 6743:
				if (obX == 3545 && obY == 9695) {
					c.getPacketSender().addObjectPacket(3545, 9694, 6724, 0, -1);
					c.getPacketSender().addObjectPacket(3545, 9695, 6743, 0, -3);
				} else if (obX == 3541 && obY == 9694) {
					c.getPacketSender().addObjectPacket(3541, 9694, 6724, 0, -1);
					c.getPacketSender().addObjectPacket(3541, 9695, 6743, 0, -3);
				}
				break;

			case 6724:
				if (obX == 3545 && obY == 9694) {
					c.getPacketSender().addObjectPacket(3545, 9694, 6724, 0, -1);
					c.getPacketSender().addObjectPacket(3545, 9695, 6743, 0, -3);
				} else if (obX == 3541 && obY == 9695) {
					c.getPacketSender().addObjectPacket(3541, 9694, 6724, 0, -1);
					c.getPacketSender().addObjectPacket(3541, 9695, 6743, 0, -3);
				}
				break;

			// burthrope minigame area
			case 4627: // stairs to go aboveground
				if (obX == 2205 && obY == 4935) {
					c.getPA().movePlayerDelayed(2899, 3565, 0, 0, 1);
					return true;
				} else {
					return false;
				}

			case 4624: // burthrope stairs to go underground
				c.getPA().movePlayerDelayed(2206, 4934, 1, 0, 1);
				break;

			case 4625: // burthrope stairs going down
				if (obX == 2897 && obY == 3567) {
					c.getPA().movePlayerDelayed(c.getX(), c.getY() - 4, 0, 0,
							1);
					return true;
				} else {
					return false;
				}

			case 4620: // burthrope stairs going down to games room
				if (obX == 2207 && obY == 4935) {
					c.getPA().movePlayerDelayed(c.getX(), c.getY() + 4, 0, 0,
							1);
					return true;
				} else if (obX == 2212 && obY == 4942) {
					c.getPA().movePlayerDelayed(c.getX(), c.getY() - 4, 0, 0,
							1);
					return true;
				} else {
					return false;
				}

			case 12389: // goblin stairs
				c.getPA().movePlayerDelayed(2981, 9915, 0, 828, 1);
				break;
			case 12390: // goblin stairs going above ground
				c.getPA().movePlayerDelayed(2960, 3506, 0, 828, 1);
				break;

			// DOORS UNDER HERE!!!!!!!!!!!!!!!
			case 2712: // cooking guild door
			case 2624: // legends guild double doors
			case 2625: // legends guild double doors

			case 2552: // ardy dungeon gates
			case 2553: // ardy dungeon gates
			case 1516: // games room double doors
			case 1519: // games room double doors
			case 11196: // games room regular doors
			case 11197: // games room regular doors
			case 4629: // games room double doors
			case 4631: // games room double doors
			case 4632: // games room double doors
			case 4633: // games room double doors
			case 4636: // games room regular doors
			case 4637: // games room regular doors
			case 4638: // games room regular doors
			case 4640: // games room regular doors
			case 4743: // games room regular doors
			case 12444: // goblin village door
			case 12446: // goblin village doubledoor
			case 12448: // goblin village doubledoor
			case 3014:
			case 1512:
			case 1530:
			case 59:
			case 1531:
			case 1533:
			case 1534:
			case 1536:
			case 11712:
			case 11711:
			case 11707:
			case 11708:
			case 6725:
			case 3198:
			case 3197:
			case 2647://crafting guild
				// case 1596:
				// case 1597:
			case 2025:
			case 4696:
			case 21340:
			case 21341:
			case 26910:
			case 26913:
			case 26908:
			case 26906:
			case 1506:
			case 1508:
			case 1553:
			case 1551:
			case 1558:
			case 1557:
			case 11716:
			case 11721:
			case 11714:
			case 14751:
			case 14752:
			case 14749:
			case 21507:
			case 21505:
			case 21600:
			case 1600:
			case 1601:
			case 4250:
			case 4247:
			case 4165:
			case 4166:
			case 4148:
			case 4312:
			case 4311:
			case 81:
			case 24384:
			case 24381:
			case 24378:
			case 24376:
			case 2399:
			case 15536:
			case 2627:
			case 2621:
			case 2622:
			case 2116:
			case 2115:
			case 15204:
			case 8789:
			case 7050:
			case 4490:
			case 4487:
			case 1528:
			case 4577:
			case 779:
			case 2623:
			case 11470: // draynor manor doors
			case 134:
			case 135:
			case 136:
			case 22:
			case 2069:
			case 26810:
			case 1591: //ranging guild tower door
			case 24561://gate between varrock and canifis
			case 24560://gate between varrock and canifis
			case 24369://gate between varrock and canifis
			case 24370://gate between varrock and canifis
			case 3507://gate from canafis to the swamp
			case 3506://gate from canafis to the swamp
			case 5183:
			case 5186:
			case 12816://gate in canafis swamp
			case 12817://gate in canafis swamp
			case 2439://gate in karamja
			case 2438://gate in karamja
			case 2262://gate out of city in karamja
			case 2261://gate out of city in karamja
			case 2260://gate out of city in karamja
			case 2259://gate out of city in karamja
			case 2391://legends guild gate
			case 2392://legends guild gate
			case 52: //gate in camelot
			case 63: //gate in camelot
			case 8739: //ardougne wall door
			case 8738: //ardougne wall door
			case 2406: //lumby swamp shack door
			case 2041: //ogre combat training area gate
			case 2039: //ogre combat training area gate
				// Server.objectHandler.doorHandling(objectType, obX, obY, 0);
				if (!ObjectManager.objectExists(-1, o.getX(), o.getY(), o.getZ()))
					ObjectManager.addObject(new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(),	o.getType(), o.getId(), 60));
				break;

			case 5126:
				if (c.absY == 3554) {
					c.getPA().walkTo(0, 1, true);
				} else {
					c.getPA().walkTo(0, -1, true);
				}
				break;

			case 1759:
				if (obX == 2884 && obY == 3397) {
					c.getPA().movePlayer(c.absX, c.absY + 6400, 0);
				}
				break;

			/*
			 * case 1558: if (c.absX == 3041 && c.absY == 10308) {
			 * c.getPA().movePlayer(3040, 10308, 0); } else if (c.absX == 3040
			 * && c.absY == 10308) { c.getPA().movePlayer(3041, 10308, 0); }
			 * else if (c.absX == 3040 && c.absY == 10307) {
			 * c.getPA().movePlayer(3041, 10307, 0); } else if (c.absX == 3041
			 * && c.absY == 10307) { c.getPA().movePlayer(3040, 10307, 0); }
			 * else if (c.absX == 3044 && c.absY == 10341) {
			 * c.getPA().movePlayer(3045, 10341, 0); } else if (c.absX == 3045
			 * && c.absY == 10341) { c.getPA().movePlayer(3044, 10341, 0); }
			 * else if (c.absX == 3044 && c.absY == 10342) {
			 * c.getPA().movePlayer(3045, 10342, 0); } else if (c.absX == 3045
			 * && c.absY == 10342) { c.getPA().movePlayer(3044, 10343, 0); }
			 * break; case 1557: if (c.absX == 3023 && c.absY == 10312) {
			 * c.getPA().movePlayer(3022, 10312, 0); } else if (c.absX == 3022
			 * && c.absY == 10312) { c.getPA().movePlayer(3023, 10312, 0); }
			 * else if (c.absX == 3023 && c.absY == 10311) {
			 * c.getPA().movePlayer(3022, 10311, 0); } else if (c.absX == 3022
			 * && c.absY == 10311) { c.getPA().movePlayer(3023, 10311, 0); }
			 * break;
			 */
			case 111735:
			case 2566:

				if (c.absX == 3043 && c.absY == 3957) {

					if (!c.getItems().playerHasItem(1543, 1)) {
						c.sendMessage(
								"You need a wildy key to open this chest!");
						return true;
					}

					if (c.getItems().freeSlots() > 3) {
						if (c.getItems().playerHasItem(1543, 1)) {
							c.getItems().deleteItemInOneSlot(1543, 1);
							
							c.getExtraHiscores().incWildyKeysUsed();

							int redWildyKeyResult = c.getPA().redWildyKey();

							if (redWildyKeyResult == 3140) {
								c.getAchievement().findDragonChain();
							}

							c.getItems().addItem(redWildyKeyResult, 1);
							c.getItems().addItem(c.getPA().wildyKeySupply(),
									5 + Misc.random(20));

							if (c.getClan() != null) {
								c.getClan().addClanPoints(c, 20, "wildy_key");
							}

							if (!WorldType.equalsType(WorldType.SPAWN)) {
								c.getItems().addItem(995,
										500000 + Misc.random(2000000));
							}

							if (Misc.random(15) == 1) {
								c.getItems().addItem(2528, 1);
							} else if (Misc.random(5) == 1) {
								c.getItems().addItem(987, 1);
							} else if (Misc.random(5) == 2) {
								c.getItems().addItem(988, 1);
							} else if (Misc.random(300) == 5
									&& !WorldType.equalsType(WorldType.SPAWN)) {
								c.getItems().addItem(20949, 1); // red
							}
						}
					} else {
						c.sendMessage(
								"You must have at least 4 inventory spaces to buy this item.");
					}
				} else if (c.absX == 3043 && c.absY == 3951) {

					if (!c.getItems().playerHasItem(1546, 1)) {
						c.sendMessage(
								"You need a wildy key to open this chest!");
						return true;
					}

					if (c.getItems().freeSlots() > 3) {
						if (c.getItems().playerHasItem(1546, 1)) {
							c.getItems().deleteItemInOneSlot(1546, 1);
							c.getExtraHiscores().incWildyKeysUsed();
							int blueWildyKeyResult = c.getPA().blueWildyKey();

							if (blueWildyKeyResult == 3140) {
								c.getAchievement().findDragonChain();
							}

							c.getItems().addItem(blueWildyKeyResult, 1);
							c.getItems().addItem(c.getPA().wildyKeySupply(),
									5 + Misc.random(20));
							if (c.getClan() != null) {
								c.getClan().addClanPoints(c, 70, "wildy_key");
							}
							if (!WorldType.equalsType(WorldType.SPAWN)) {
								c.getItems().addItem(995,
										700000 + Misc.random(3000000));
							}
							if (Misc.random(15) == 1) {
								c.getItems().addItem(2528, 1);
							} else if (Misc.random(5) == 1) {
								c.getItems().addItem(987, 1);
							} else if (Misc.random(5) == 2) {
								c.getItems().addItem(988, 1);
							} else if (Misc.random(300) == 5
									&& !WorldType.equalsType(WorldType.SPAWN)) {
								c.getItems().addItem(20951, 1); // blue
							}
						}
					} else {
						c.sendMessage(
								"You must have at least 4 inventory spaces to buy this item.");
					}
				}

			case 111736:
			case 2567:
				if (c.absX != 3042 && c.absY != 3950) {
					return true;
				}

				if (!c.getItems().playerHasItem(1547, 1)) {
					c.sendMessage("You need a wildy key to open this chest!");
					return true;
				}

				if (c.getItems().freeSlots() > 3) {
					if (c.getItems().playerHasItem(1547, 1)) {
						c.getItems().deleteItemInOneSlot(1547, 1);
						c.getExtraHiscores().incWildyKeysUsed();
						int purpleResult = c.getPA().purpleWildyKey();

						if (purpleResult == 3140) {
							c.getAchievement().findDragonChain();
						}

						c.getItems().addItem(purpleResult, 1);
						c.getItems().addItem(c.getPA().wildyKeySupply(),
								5 + Misc.random(20));
						if (c.getClan() != null) {
							c.getClan().addClanPoints(c, 70, "wildy_key");
						}
						if (!WorldType.equalsType(WorldType.SPAWN)) {
							c.getItems().addItem(995,
									300000 + Misc.random(1000000));
						}
						if (Misc.random(15) == 1) {
							c.getItems().addItem(2528, 1);
						} else if (Misc.random(5) == 1) {
							c.getItems().addItem(987, 1);
						} else if (Misc.random(5) == 2) {
							c.getItems().addItem(988, 1);
						} else if (Misc.random(300) == 5
								&& !WorldType.equalsType(WorldType.SPAWN)) {
							c.getItems().addItem(20952, 1); // white
						}
					}
				} else {
					c.sendMessage(
							"You must have at least 4 inventory spaces to buy this item.");
				}
				break;

			case 111737:
			case 2568:
				if (c.absX != 3040 && c.absY != 3950) {
					return true;
				}

				if (!c.getItems().playerHasItem(1548, 1)) {
					c.sendMessage("You need a wildy key to open this chest!");
					return true;
				}

				if (c.getItems().freeSlots() > 3) {
					if (c.getItems().playerHasItem(1548, 1)) {
						c.getItems().deleteItemInOneSlot(1548, 1);
						c.getExtraHiscores().incWildyKeysUsed();
						int greenResult = c.getPA().greenWildyKey();

						if (greenResult == 3140) {
							c.getAchievement().findDragonChain();
						}

						c.getItems().addItem(greenResult, 1);
						c.getItems().addItem(c.getPA().wildyKeySupply(),
								5 + Misc.random(20));
						if (c.getClan() != null) {
							c.getClan().addClanPoints(c, 70, "wildy_key");
						}
						if (!WorldType.equalsType(WorldType.SPAWN)) {
							c.getItems().addItem(995,
									500000 + Misc.random(2000000));
						}
						if (Misc.random(15) == 1) {
							c.getItems().addItem(2528, 1);
						} else if (Misc.random(5) == 1) {
							c.getItems().addItem(987, 1);
						} else if (Misc.random(5) == 2) {
							c.getItems().addItem(988, 1);
						} else if (Misc.random(300) == 5
								&& !WorldType.equalsType(WorldType.SPAWN)) {
							c.getItems().addItem(20950, 1); // yellow
						}
					}
				} else {
					c.sendMessage(
							"You must have at least 4 inventory spaces to buy this item.");
				}
				break;

			case 111727:
			case 2558:
				if (c.absX == 3044 && c.absY == 3956) {
					c.getPA().movePlayer(3045, 3956, 0);
				}
				if (c.absX == 3041 && c.absY == 3959) {
					c.getPA().movePlayer(3041, 3960, 0);
				}
				if (c.absX == 3038 && c.absY == 3956) {
					c.getPA().movePlayer(3037, 3956, 0);
				}
				if (c.absX == 3045 && c.absY == 3956
						|| c.absX == 3041 && c.absY == 3960
						|| c.absX == 3037 && c.absY == 3956) {
					if (!(c.getItems().playerHasItem(1544, 1))) {
						c.sendMessage("You need a key to open this door.");
						return true;
					}
					if (c.wildyKeys == 0) {
						c.wildyKeys = 1;
						int spawnX = c.getX(), spawnY = c.getY();
						boolean breaking = false;
						for (int x = 0; x < 2; x++) {
							if (breaking) {
								break;
							}
							for (int y = 0; y < 2; y++) {
								if (x == 0 && y == 0) {
									continue;
								}
								if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
										c.getHeightLevel(), x, y)) {
									spawnX = c.getX() + x;
									spawnY = c.getY() + y;
									breaking = true;
									break;
								}
							}
						}

						NPCHandler.spawnNpc(c, 221, spawnX, spawnY, 0, 0, true,
								true);
					}
					if (c.wildyKeys == 2) {
						c.getPA().movePlayer(3041, 3956, 0);
						c.getItems().deleteItemInOneSlot(1544, 1);
						c.wildyKeys = 0;
					} else {
						c.sendMessage("You did not defeat the knight!");
					}
				}
				break;

			case 9294:
				if (o.getX() == 2879 && o.getY() == 9813) {// || (o.getX() ==
															// 2878 && o.getY()
															// == 9813)) {
					if (c.getPlayerLevel()[Skills.AGILITY] < 80) {
						c.sendMessage(
								"You need agility level of at least 80 to enter this course.");
						return true;
					}
				}
				if (c.getX() <= 2879) {
					if (c.getX() == 2878 && c.getY() == 9813) {
						if (c.getX() == 2878) {
							c.startAnimation(3067);
							ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, Direction.EAST).addSecondDirection(2, 0, 1, 0);
							c.setForceMovement(mask);
							
						} else {
							c.getPA().playerWalk(2878, 9813);
						}
					}
				} else {
					if (c.getX() == 2880 && c.getY() == 9813) {
						if (c.getX() == 2880) {
							c.startAnimation(3067);
							ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, Direction.WEST).addSecondDirection(-2, 0, 1, 0);
							c.setForceMovement(mask);
						} else {
							c.getPA().playerWalk(2880, 9813);
						}
					}
				}
				break;

			case 9293:
				if (o.getX() == 2887 && o.getY() == 9799
						|| o.getX() == 2890 && o.getY() == 9799) {
					if (c.getPlayerLevel()[Skills.AGILITY] < 70) {
						c.sendMessage(
								"You need agility level of at least 70 to enter this course.");
						return true;
					}
				}
				if (c.getX() == 2886) {
					if (c.getX() == 2886 && c.getY() == 9799) {
						if (c.getX() == 2886) {
							
							CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

								@Override
								public void stop() {
								}

								@Override
								public void execute(CycleEventContainer container) {
									container.stop();
									c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
									c.setForceMovement(ForceMovementMask.createMask(3, 0, 3, 0, Direction.EAST).addSecondDirection(3, 0, 4, 0).setSecondMovementAnim(AgilityAnimations.PIPE_SQUEEZE_ANIM.getId()));
								}
							}, 1);
							
						} else {
							c.getPA().playerWalk(2886, 9799);
						}
					}
				} else {
					if (c.getX() == 2892 && c.getY() == 9799) {
						if (c.getX() == 2892) {
							CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

								@Override
								public void stop() {
								}

								@Override
								public void execute(CycleEventContainer container) {
									container.stop();
									c.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM);
									c.setForceMovement(ForceMovementMask.createMask(-3, 0, 3, 0, Direction.WEST).addSecondDirection(-3, 0, 4, 0).setSecondMovementAnim(AgilityAnimations.PIPE_SQUEEZE_ANIM.getId()));
								}
							}, 1);
							
						} else {
							c.getPA().playerWalk(2892, 9799);
						}
					}
				}
				c.increaseProgress(TaskType.AGILITY, o.getId());
				break;
			// if (c.absX < obX) {
			// c.getPA().movePlayer(2892, 9799, 0);
			// } else {
			// c.getPA().movePlayer(2886, 9799, 0);
			// }
			// break;

			case 10529:
			case 10527:
				if (c.absY <= obY) {
					c.getPA().walkTo(0, 1, true);
				} else {
					c.getPA().walkTo(0, -1, true);
				}
				break;

			case 21584:
				if (o.getX() == 2401 && o.getY() == 3889) {
					c.sendMessage("You enter the cave.");
					c.getPA().movePlayer(1798, 4408, 3);
					return true;
				}
				break;

			case 21585:
				if (o.getX() == 2316 && o.getY() == 3894) {
					c.sendMessage("You enter the cave.");
					c.getPA().movePlayer(1975, 4407, 3);
					return true;
				}
				break;

			case 21586:
				if (o.getX() == 2364 && o.getY() == 3894) {
					c.sendMessage("You fall into the cave. haha");
					c.getPA().movePlayer(1850, 4408, 3);
					return true;
				}
				break;

			/**
			 * Forfeiting a duel.
			 */
			case 3203:
				c.sendMessage("Forfeiting has been disabled.");
				break;
				
			case 48496:
				if (c.dungParty != null) {
					if (c.dungParty.partyMembers.get(0) != c) {
						c.sendMessage("Only the party leader can start a dungeon.");
						return true;
					}

					int index = c.getItems().getItemSlot(DungeonConstants.RING_OF_KINSHIP);
					
					if (index == -1 && !c.getItems().playerHasEquipped(DungeonConstants.RING_OF_KINSHIP)) {
						c.sendMessage("You need Ring of Kinship to enter a dungeon.");
						return true;
					}

					if (c.summoned != null) {
						c.sendMessage("You can't bring any pets into Daemonheim.");
						return true;
					}

					int size = c.dungParty.partyMembers.size();
					
					//check for items
					for (int i = 0; i < size; i++) {
						Client partyMember = c.dungParty.partyMembers.get(i);

						if (!partyMember.inDaemonheim()) {
							c.sendMessage(partyMember.getNameSmartUp() + " is outside Daemonheim lobby.");
							return true;
						}

						int kinshipIndex = partyMember.getItems().getItemSlot(DungeonConstants.RING_OF_KINSHIP);

						if (kinshipIndex == -1 && !partyMember.getItems().playerHasEquipped(DungeonConstants.RING_OF_KINSHIP)) {
							c.sendMessage(partyMember.getNameSmartUp() + " doesn't have Ring of Kinship in his inventory.");
							return true;
						}

						if (partyMember.summoned != null) {
							c.sendMessage(partyMember.getNameSmartUp() + " has a pet summoned and cannot enter Daemonheim.");
							return true;
						}

						for (int j = 0, length = partyMember.playerItems.length; j < length; j++) {
							int id = partyMember.playerItems[j] - 1;
							if (id != -1 && id != DungeonConstants.RING_OF_KINSHIP) {
								c.sendMessage(partyMember.getNameSmartUp() + " is carrying items that cannot be take into Daemonheim.");
								return true;
							}
						}

						for (int j = 0, length = partyMember.playerEquipment.length; j < length; j++) {
							int id = partyMember.playerEquipment[j];
							if (id != -1 && id != DungeonConstants.RING_OF_KINSHIP) {
								c.sendMessage(partyMember.getNameSmartUp() + " is wearing items that cannot be take into Daemonheim.");
								return true;
							}
						}
						
						//check in trade or duel
						if (partyMember.getTradeAndDuel().hasItemsInTradeOrDuel()) {
							c.sendMessage(partyMember.getNameSmartUp() + " is carrying items that cannot be take into Daemonheim.");
							return true;
						}
					}
					
					if (size > 1) {
						OptionDialogue dialog = null;
						if (size == 2) {
							dialog = new OptionDialogue("Select an Option.", "1", () -> {difficultySafeSet(c, 1);}, "2", () -> {difficultySafeSet(c, 2);});
						} else if (size == 3) {
							dialog = new OptionDialogue("Select an Option.", "1", () -> {difficultySafeSet(c, 1);}, "2", () -> {difficultySafeSet(c, 2);}, "3", () -> {difficultySafeSet(c, 3);});
						} else if (size == 4) {
							dialog = new OptionDialogue("Select an Option.", "1", () -> {difficultySafeSet(c, 1);}, "2", () -> {difficultySafeSet(c, 2);}, "3", () -> {difficultySafeSet(c, 3);}, "4", () -> {difficultySafeSet(c, 4);});
						} else if (size == 5) {
							dialog = new OptionDialogue("Select an Option.", "1", () -> {difficultySafeSet(c, 1);}, "2", () -> {difficultySafeSet(c, 2);}, "3", () -> {difficultySafeSet(c, 3);}, "4", () -> {difficultySafeSet(c, 4);}, "5", () -> {difficultySafeSet(c, 5);});
						}

						if (dialog == null) {
							c.sendMessage("Error.");
							return true;
						}

						c.getDialogueBuilder().sendStatement("Please select the number of players you want the dungeon to be",
								"designed for. <col=7f0000>You may not be able to complete a dungeon if too", "<col=7f0000>many people leave.")
						.appendOption(dialog).sendAction(() -> {askForSize(c, size, false);}).execute();
					} else {
						askForSize(c, size, true);
					}
				} else {
					int index = c.getItems().getItemSlot(DungeonConstants.RING_OF_KINSHIP);

					if (index == -1 && !c.getItems().playerHasEquipped(DungeonConstants.RING_OF_KINSHIP)) {
						c.sendMessage("You need Ring of Kinship to start the party.");
						return true;
					}

					c.getDialogueBuilder().sendOption("Would you like to start a party?", "Yes.", () -> {Party.createParty(c, true);}, "No.", () -> {}).execute();
				}
				return true;

			default:
				// ScriptManager.callFunc("objectClick1_" + objectType, c,
				// objectType,
				// obX, obY);
				return false;

		}
		return true;
	}
	
	public static void shootRangingGuildTarget(Player player, int obId, int obX, int obY) {
		if (player.getAttackTimer() > 0) {
			return;
		}
		GameObject o = RegionClip.getGameObject(obId, obX, obY, player.getZ());
		if (o == null)
			return;
		
		if (player.getX() >= 2671 && player.getX() <= 2680 && player.getY() >= 3417 && player.getY() <= 3420) {
			if (player.getRangeWeapon() == null) {
				player.sendMessage("You can't reach that.");
				return;
			}
			
			player.getMovement().resetWalkingQueue();
			
			player.faceLocation(o.getLocation());
			
			
			Ammunition ammo = null;
			if (player.getRangeWeapon().getItemId() == BlowPipeCharges.BLOWPIPE_FULL) { // toxic blowpipe cheaphax
				ammo = Ammunition.get(player.getBlowpipeAmmoType());
				if (ammo == null) { // no ammo in blowpipe
					player.sendMessage("Your blowpipe has ran out of ammo.");
					return;
				}
			} else if (player.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerWeapon)
				ammo = Ammunition.get(player.playerEquipment[PlayerConstants.playerWeapon]);
			else if (player.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerArrows)
				ammo = Ammunition.get(player.playerEquipment[PlayerConstants.playerArrows]);
			
			if (ammo == null) {
				player.sendMessage("This weapon is not finished! Needs ammo added to database!");
				return;
			}
			
			Projectile projectile = ammo.getProjectile().copy();
			
			final int dist = Misc.distanceToPoint(player.getX(), player.getY(), obX, obY);
			
			projectile.setSlope(projectile.getSlope() + dist);
			
			projectile.setStartLoc(player.getCurrentLocation());
			projectile.setEndLoc(Location.create(obX, obY, player.getZ()));
			projectile.setEndDelay(projectile.getEndDelay() + (5*dist));
			
			if (player.getRangeWeapon().getItemId() == 12926) { // toxic blowpipe cheaphax
				projectile.setStartHeight(34);
				projectile.setSlope(0+dist);
			}
			
			final int random = Misc.newRandom(1, 40);
			
			projectile.setEndHeight(random);

			Graphic startGraphic = Graphic.create(ammo.getStartGraphics().getId(), ammo.getStartGraphics().getType());
			
			if (startGraphic.getType() == GraphicType.BOWARROWPULL && player.getRangeWeapon().getWeaponType() == WeaponType.THROWN) {
				startGraphic = Graphic.create(startGraphic.getId(), GraphicType.HIGH);
			}
			
			if (startGraphic != null && !player.getItems().hasEquippedWeapon(12926))
				player.startGraphic(startGraphic);
			
			if (projectile != null)
				GlobalPackets.createProjectile(projectile);
			
			player.resetAnimations(); // using this instead of priority check for now.
			player.startAnimation(player.getWeaponAnimation());
			player.setPauseAnimReset(1);
			
			player.setAttackTimer(4);
			
			if (random >= 21 && random <= 26) {
				player.forceChat("Bullseye!");
			}
			
		} else {
			player.sendMessage("I can't reach that from here");
		}
	}

	private static void difficultySafeSet(Client c, int difficulty) {
		if (c.dungParty == null) {
			return;
		}

		c.dungParty.difficulty = difficulty;
	}

	private static void sizeSafeSet(Client c, int size) {
		if (c.dungParty == null) {
			return;
		}

		c.dungParty.size = size;
		c.dungParty.startDungeon();
	}

	private static void askForSize(Client c, int size, boolean execute) {
		if (c.dungParty.getComplexity() <= 5) {
			c.dungParty.size = DungeonConstants.SMALL_DUNGEON;
			c.dungParty.startDungeon();
			return;
		}

		OptionDialogue dialog = null;
		if (size > 2) {
			dialog = new OptionDialogue("What size of dungeon would you like?", "Small.", () -> {sizeSafeSet(c, DungeonConstants.SMALL_DUNGEON);}, "Medium.", () -> {sizeSafeSet(c, DungeonConstants.MEDIUM_DUNGEON);}, "Large.", () -> {sizeSafeSet(c, DungeonConstants.LARGE_DUNGEON);});
		} else {
			dialog = new OptionDialogue("What size of dungeon would you like?", "Small.", () -> {sizeSafeSet(c, DungeonConstants.SMALL_DUNGEON);}, "Medium.", () -> {sizeSafeSet(c, DungeonConstants.MEDIUM_DUNGEON);});
		}
		c.getDialogueBuilder().appendOption(dialog);
		if (execute) {
			c.getDialogueBuilder().execute();
		}
	}

}
