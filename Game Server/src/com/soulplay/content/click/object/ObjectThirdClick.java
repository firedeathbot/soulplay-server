package com.soulplay.content.click.object;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.minigames.castlewars.CastleWarsObjects;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonObjects;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public final class ObjectThirdClick {
	
	private ObjectThirdClick() {
		
	}
	
	public static void execute(Client c, GameObject clickedObject) {

		// if (c.clickedObject)
		// return;
		// c.clickedObject = true;

		c.clickObjectType = 0;
		c.walkingToObject = false;
		
		final int objectType = clickedObject.getId();
		final int obX = clickedObject.getX();
		final int obY = clickedObject.getY();

		if (c.isDead()) {
			return;
		}
		
		final PluginResult<ObjectClickExtension> pluginResult = PluginManager.search(ObjectClickExtension.class, objectType);
		if (pluginResult.invoke(() -> pluginResult.extension.onObjectOption3(c, clickedObject))) {
			return;
		}

		// if (c.isOwner())
		// c.sendMessage("ObjectClick3 type: " + objectType);
		

		if (c.playerIsInHouse) { //TODO: remove after construction objects are all added!
			c.sendMessage("This object isn't added to Poh yet! ERROR 311");
			return;
		}

		if (DungeonObjects.handle3(c, objectType, obX, obY)) {
			return;
		}

		final GameObject o = RegionClip.getGameObject(objectType, obX, obY,
				c.heightLevel);
		if (o == null) {
			c.sendMessage("ObjectClickThree Object Null - Report Error");
			return;
		}

		Misc.faceObject(c, o.getX(), o.getY(), o.getSizeY());
		final ObjectDef def = ObjectDef.getObjectDef(objectType);
		
		if (def == null) {
			c.sendMessage(String.format("[ObjectThirdClick] object=%d does not exist at %s", objectType, Location.create(obX,  obY)));
			return;
		}

		RSObject spawnedTemp = ObjectManager.getObject(o.getX(), o.getY(),
				o.getZ());
		if (spawnedTemp != null && spawnedTemp.getTick() == 0) { // do not let
																	// players
																	// click on
																	// despawned
																	// objects.
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, o, InstanceInteractType.OBJECT, InstanceInteractIndex.THIRD)) {
			return;
		}

		c.walkToX = c.walkToY = -1;

		switch (objectType) {
			case 4187:
				c.getPA().movePlayer(c.absX, c.absY, c.heightLevel + 1);
				return;
			case 4458:
			case 4459:
			case 4460:
			case 4461:
			case 4462: // table with ropes
			case 4463:
			case 4464:
				CastleWarsObjects.handleObjectThird(c, o);
				return;
		}

		if (def.hasName()) {
			if (def.name.equals("Ladder") && !(objectType == 6501)) {
				c.getPA().movePlayerDelayed(c.absX, c.absY, c.heightLevel - 1, 828, 1);
				return;
			}

			if (def.name.equals("Staircase")) {
				if (def.xLength() == 1 && def.yLength() == 1
						&& def.itemActions[3].equals("Climb-down")) {
					c.getPA().movePlayer(c.absX, c.absY, c.heightLevel - 1);
					return;
				}
				if (def.xLength() == 2 && def.yLength() == 2) {
					c.getPA().movePlayer(c.absX, c.absY, c.heightLevel - 1);
					return;
				}
			}
		}

	}

}
