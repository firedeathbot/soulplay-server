package com.soulplay.content.click.object;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonObjects;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class ObjectFifthClick {
	
	private ObjectFifthClick() {
		
	}
	
	public static void execute(Client c, GameObject clickedObject) {

		// if (c.clickedObject)
		// return;
		// c.clickedObject = true;

		c.clickObjectType = 0;
		c.walkingToObject = false;
		c.finalLocalDestX = c.finalLocalDestY = 0;
		
		final int objectType = clickedObject.getId();
		final int obX = clickedObject.getX();
		final int obY = clickedObject.getY();

		if (c.isDead()) {
			return;
		}
		
		if (Construction.handleFifthObjectClick(c, c.getClickedObject())) {
		    return;
		}
		
		if (c.playerIsInHouse) { //TODO: remove after construction objects are all added!
			c.sendMessage("This object isn't added to Poh yet! ERROR 5");
			return;
		}

		if (DungeonObjects.handle5(c, objectType, obX, obY)) {
			return;
		}

		final GameObject o = RegionClip.getGameObject(objectType, obX, obY,
				c.heightLevel);
		if (o == null) {
			c.sendMessage("ObjectFifthhClick Object Null - Report Error");
			return;
		}

		Misc.faceObject(c, o.getX(), o.getY(), o.getSizeY());
//		if (!c.getPA().canUseObjectFromLoc(o, c.finalLocalDestX,
//				c.finalLocalDestY)) {
//			return;
//		}

		RSObject spawnedTemp = ObjectManager.getObject(obX, obY, c.heightLevel);
		if (spawnedTemp != null && spawnedTemp.getTick() == 0) { // do not let
																	// players
																	// click on
																	// despawned
																	// objects.
			return;
		}

		final PluginResult<ObjectClickExtension> pluginResult = PluginManager.search(ObjectClickExtension.class, objectType);
		if (pluginResult.invoke(() -> pluginResult.extension.onObjectOption5(c, clickedObject))) {
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().interact(c, o, InstanceInteractType.OBJECT, InstanceInteractIndex.FIFTH)) {
			return;
		}

		switch (objectType) {

			default:
				if (c.playerRights == 3 || c.playerRights == 8 || c.isDev()) {
					System.out.println("objectClick5:" + objectType + " X:"
							+ obX + " Y:" + obY);
				}
				break;
		}
	}

	// public void dunnoClickObject(int objectType, int obX, int obY) {
	// c.clickObjectType = 0;
	// c.walkingToObject = false;
	//// int faceX = 0, faceY = 0;
	//
	//
	// RSObject spawnedTemp = ObjectManager.getObject(obX, obY, c.heightLevel);
	// if (spawnedTemp != null && spawnedTemp.getTick() == 0) { // do not let
	// players click on despawned objects.
	// return;
	// }
	//
	// // if (c.isOwner())
	//
	//
	// switch (objectType) {
	//
	//
	// default:
	// // ScriptManager.callFunc("objectClick3_" + objectType, c,
	// // objectType,
	// // obX, obY);
	// break;
	// }
	// }

}
