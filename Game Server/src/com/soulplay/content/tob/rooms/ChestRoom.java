package com.soulplay.content.tob.rooms;

import com.soulplay.content.clans.ClanMember;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;

public class ChestRoom extends TobRoom {

	public static final Animation CHEST_ANIM = Animation.osrs(8106);
	public static final Animation OPEN_CHEST_ANIM = Animation.create(536);
	public static final int[] CHESTS_X = { 33, 40, 40, 26, 26 };
	public static final int[] CHESTS_Y = { 42, 35, 39, 35, 39 };
	public static final int NORMAL_CHEST = 32990;
	public static final int PURPLE_CHEST = 32991;
	public static final int NORMAL_CHEST_ARROW = 32992;
	public static final int PURPLE_CHEST_ARROW = 32993;
	public static final int OPENED_CHEST = 32994;
	private final Location enterLocation;
	private ChestData[] chests;

	public ChestRoom(TobManager tobManager) {
		super(tobManager, null);
		this.enterLocation = base().transform(37, 20);
	}

	@Override
	public int getRegionId() {
		return 38467;
	}

	@Override
	public Location enterLocation() {
		return enterLocation;
	}

	@Override
	public TobJailLocation[] getJailLocations() {
		return null;
	}

	public void initChests() {
		TobManager tobManager = getTobManager();
		int partySize = tobManager.party.partySize();
		int chestSize = Math.min(partySize, 5);
		chests = new ChestData[chestSize];

		for (int i = 0; i < chestSize; i++) {
			chests[i] = new ChestData();
		}

		for (int i = 0; i < partySize; i++) {
			ClanMember clanMember = tobManager.party.getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			long mid = clanMember.getMysqlID();
			Player player = PlayerHandler.getPlayerByMID(mid);
			if (player == null || !player.isActive) {
				continue;
			}

			int chestIndex = i % 5;
			chests[chestIndex].setLoot(mid, TobRewardsInterface.pick(player));
		}

		for (int i = 0; i < chestSize; i++) {
			generateChest(ChestRoom.CHESTS_X[i], ChestRoom.CHESTS_Y[i], i);
		}
	}

	private void generateChest(int x, int y, int index) {
		int rotation = 0;
		if (x == 40) {
			rotation = 1;
		} else if (x == 26) {
			rotation = 3;
		}

		boolean jackpot = false;
		ChestData chestData = chests[index];
		for (Inventory items : chestData.getRewardsToClaim().values()) {
			if (TobRewardsInterface.checkForJackpot(items.getItems())) {
				jackpot = true;
				break;
			}
		}

		GameObject chest = GameObject.createTickObject(jackpot ? ChestRoom.PURPLE_CHEST : ChestRoom.NORMAL_CHEST, base().transform(x, y), rotation, 10, Integer.MAX_VALUE, -1);
		chestData.setChestObject(chest);
		chestData.setJackpot(jackpot);

		getTobManager().objectManager.addObject(chest);
	}

	public ChestData findChestForPlayer(Player player) {
		if (chests == null) {
			return null;
		}

		for (int i = 0, length = chests.length; i < length; i++) {
			ChestData chestData = chests[i];
			if (chestData == null) {
				continue;
			}

			if (chestData.getRewardsToClaim().containsKey((long) player.mySQLIndex)) {
				return chestData;
			}
		}

		return null;
	}

}
