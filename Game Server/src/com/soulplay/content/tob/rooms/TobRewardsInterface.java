package com.soulplay.content.tob.rooms;

import static com.soulplay.content.tob.rooms.LootEntry.make;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class TobRewardsInterface {

	public static int jackpotChance = 150;
	private static final LootEntry[] UNCOMMON_TABLE = { make(245, 27, 44), make(1373, 2, 3), make(1127, 2, 3),
			make(1113, 2, 3), make(5289, 2, 2), make(5315, 2, 2), make(121488, 5, 9), make(1775, 167, 374),
			make(122446, 31, 60), make(560, 306, 543), make(565, 493, 493), make(1939, 367, 530), make(3138, 42, 54),
			make(1391, 12, 12), make(449, 94, 140), make(451, 43, 116), make(453, 374, 395), make(444, 250, 348),
			make(207, 23, 27), make(3049, 28, 39), make(209, 26, 55), make(211, 19, 35), make(217, 19, 48),
			make(3051, 20, 34), make(215, 31, 49), make(2485, 11, 24), make(219, 15, 43), };
	private static final List<LootEntry> UNCOMMON = new ArrayList<>();
	public static final int ID = 54900;
	public static final int INVENTORY_ID = 54905;

	public static void refresh(Player player, Inventory inventory) {
		player.getPacketSender().sendInventory(INVENTORY_ID, inventory.getItems(), 1);
	}

	public static void open(Player player) {
		player.getPacketSender().showInterface(ID);
	}

	public static void openRefresh(Player player, Inventory inventory) {
		refresh(player, inventory);
		open(player);
	}

	public static void inventoryClick(Player player, int slot) {
		TobManager tobManager = player.tobManager;
		if (tobManager == null || !tobManager.started()) {
			return;
		}

		TobRoom currentRoom = tobManager.currentRoom;
		if (currentRoom == null || !(currentRoom instanceof VerzikRoom)) {
			return;
		}

		ChestRoom chestRoom = (ChestRoom) currentRoom.getNextRoom();

		ChestData chestData = chestRoom.findChestForPlayer(player);
		if (chestData == null) {
			player.sendMessage("You contain no loot.");
			return;
		}

		Inventory inventory = chestData.getRewardsToClaim().get((long) player.mySQLIndex);
		Item item = inventory.get(slot);
		if (item == null) {
			return;
		}

		Item itemToAdd = item;
		final int notedItemId = ItemDef.getPossibleNotedId(item.getId());
		if (notedItemId != item.getId()) {
			itemToAdd = new Item(notedItemId, item.getAmount(), item.getSlot());
		}

		if (!player.getItems().hasInventorySpaceFor(itemToAdd)) {
			player.sendMessage("Not enough space in your inventory.");
			return;
		}

		if (player.getItems().addItem(itemToAdd.getId(), itemToAdd.getAmount())) {
			inventory.remove(item);
			refresh(player, inventory);
		}
	}

	public static Item[] pick(Player player) {
		boolean jackpot = Misc.randomBoolean(jackpotChance);
		if (jackpot) {
			int chance = Misc.random(20);
			Item item;
			if (chance >= 11) {
				item = new Item(122477, 1);
			} else if (chance >= 9) {
				item = new Item(122326, 1);
			} else if (chance >= 7) {
				item = new Item(122328, 1);
			} else if (chance >= 5) {
				item = new Item(122327, 1);
			} else if (chance >= 3) {
				item = new Item(122481, 1);
			} else if (chance >= 1) {
				item = new Item(122324, 1);
			} else {
				item = new Item(30327, 1);
			}

			return new Item[] { item };
		}

		List<LootEntry> entries = new ArrayList<>(UNCOMMON);
		int rolls = 3;
		Item[] items = new Item[rolls];
		for (int i = 0; i < rolls; i++) {
			int randomIndex = Misc.randomNoPlus(entries.size());
			LootEntry entry = entries.remove(randomIndex);
			items[i] = new Item(entry.getId(), Misc.random(entry.getMin(), entry.getMax()));
		}

		return items;
	}

	public static boolean checkForJackpot(Item[] items) {
		if (items == null) {
			return false;
		}

		if (items.length >= 1) {
			switch (items[0].getId()) {
				case 122477:
				case 122326:
				case 122328:
				case 122327:
				case 122481:
				case 122486:
				case 122324:
					return true;
				default:
					return false;
			}
		}

		return false;
	}

	static {
		UNCOMMON.addAll(Arrays.asList(UNCOMMON_TABLE));
	}

}
