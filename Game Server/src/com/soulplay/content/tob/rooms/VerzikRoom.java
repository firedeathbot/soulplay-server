package com.soulplay.content.tob.rooms;

import com.soulplay.content.tob.TobManager;
import com.soulplay.content.tob.npcs.SupportingPillar;
import com.soulplay.content.tob.npcs.VerzikVitur;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class VerzikRoom extends TobRoom {

	private static final TobJailLocation[] JAIL_COORDS = {
		new TobJailLocation(21, 37),
		new TobJailLocation(23, 37),
		new TobJailLocation(25, 37),
		new TobJailLocation(39, 37),
		new TobJailLocation(41, 37)
	};

	private VerzikVitur verzik;
	private SupportingPillar[] pillars = new SupportingPillar[6];
	private final Location enterLocation;

	public VerzikRoom(TobManager tobManager) {
		super(tobManager, new ChestRoom(tobManager));
		this.enterLocation = base().transform(32, 10);
	}

	@Override
	public void configure() {
		super.configure();

		verzik = (VerzikVitur) spawnNpc(108369, 30, 35);
		verzik.verzikRoom = this;

		int pillarIndex = 0;
		for (int y = 18; y <= 30; y += 6) {
			spawnPillar(25, y, pillarIndex++);
			spawnPillar(37, y, pillarIndex++);
		}
	}

	public VerzikVitur getVerzik() {
		return verzik;
	}

	public SupportingPillar[] getPillars() {
		return pillars;
	}

	private void spawnPillar(int x, int y, int index) {
		pillars[index] = (SupportingPillar) spawnNpc(108379, x, y);
		pillars[index].verzikRoom = this;
	}

	@Override
	public int getRegionId() {
		return 38211;
	}

	@Override
	public void complete() {
		super.complete();

		TobManager tobManager = getTobManager();
		tobManager.objectManager.addObject(GameObject.createTickObject(132738, base().transform(31, 36), 0, 10, Integer.MAX_VALUE, -1));

		((ChestRoom) getNextRoom()).initChests();
	}

	@Override
	public Location enterLocation() {
		return enterLocation;
	}

	@Override
	public TobJailLocation[] getJailLocations() {
		return JAIL_COORDS;
	}

}
