package com.soulplay.content.tob.rooms;

public class LootEntry {
	
	private int id, min, max;
	
	public static LootEntry make(int id, int min, int max) {
		return new LootEntry(id, min, max);
	}
	
	public LootEntry(int id, int min, int max) {
		this.id = id;
		this.min = min;
		this.max = max;
	}
	
	public int getId() {
		return id;
	}
	
	public int getMin() {
		return min;
	}
	
	public int getMax() {
		return max;
	}
	
}
