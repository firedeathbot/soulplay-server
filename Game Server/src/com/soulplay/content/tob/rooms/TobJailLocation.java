package com.soulplay.content.tob.rooms;

import com.soulplay.game.world.Direction;

public class TobJailLocation {

	private final int localX, localY, plane;
	private final Direction faceDirection;

	public TobJailLocation(int localX, int localY, int plane, Direction faceDirection) {
		this.localX = localX;
		this.localY = localY;
		this.plane = plane;
		this.faceDirection = faceDirection;
	}

	public TobJailLocation(int localX, int localY, int plane) {
		this(localX, localY, plane, Direction.SOUTH);
	}

	public TobJailLocation(int localX, int localY, Direction faceDirection) {
		this(localX, localY, 0, faceDirection);
	}

	public TobJailLocation(int localX, int localY) {
		this(localX, localY, 0);
	}

	public int getLocalX() {
		return localX;
	}

	public int getLocalY() {
		return localY;
	}

	public int getPlane() {
		return plane;
	}

	public Direction getFaceDirection() {
		return faceDirection;
	}

}
