package com.soulplay.content.tob.rooms;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.item.ContainerType;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.world.GameObject;

public class ChestData {

	private boolean jackpot;
	private final Map<Long, Inventory> rewardsToClaim = new HashMap<>();
	private GameObject chestObject;

	public void setChestObject(GameObject chestObject) {
		this.chestObject = chestObject;
	}

	public void setJackpot(boolean jackpot) {
		this.jackpot = jackpot;
	}

	public boolean isJackpot() {
		return jackpot;
	}

	public Map<Long, Inventory> getRewardsToClaim() {
		return rewardsToClaim;
	}

	public void setLoot(long index, Item[] items) {
		Inventory loot = new Inventory(4, ContainerType.ALWAYS_STACK);
		loot.add(items);
		this.rewardsToClaim.put(index, loot);
	}

	public GameObject getChestObject() {
		return chestObject;
	}
}