package com.soulplay.content.tob.rooms;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.MapTools;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.ObjectDefVersion;

public abstract class TobRoom {

	private TobManager tobManager;
	private int deathCount;
	private TobRoom nextRoom;
	private boolean completed;

	public TobRoom(TobManager tobManager, TobRoom nextRoom) {
		this.tobManager = tobManager;
		this.nextRoom = nextRoom;
	}

	public abstract int getRegionId();

	public void configure() {
		Location base = base();
		MapTools.copyStaticData(base.getX(), base.getY(), 64, 64, base.getZ(), tobManager.objectManager.getStaticObjects(), tobManager.objectManager.getSpawnedObjects(), tobManager.clipping, tobManager, ObjectDefVersion.OSRS_DEFS, 0);
	}

	public Location base() {
		int startX = (getRegionId() >> 8) << 6;
		int startY = (getRegionId() & 0xff) << 6;
		return Location.create(startX, startY, tobManager.height);
	}

	public abstract Location enterLocation();

	public NPC spawnNpc(int id, int x, int y) {
		Location base = base().transform(x, y);

		NPC npc = NPCHandler.spawnNpc(id, base.getX(), base.getY(), base.getZ());
		npc.setDynamicRegionClip(tobManager.clipping);
		npc.tobManager = tobManager;
		npc.isAggressive = true;
		npc.setRetargetTick(18);
		npc.bossRoomNpc = false;
		npc.setWalksHome(false);
		npc.setAggroRadius(64);
		npc.ignoreAggroTolerence();
		npc.getDistanceRules().setFollowDistance(64);
		tobManager.addNpc(npc);
		return npc;
	}

	public TobManager getTobManager() {
		return tobManager;
	}

	public void incDeathCount() {
		this.deathCount++;
	}

	public abstract TobJailLocation[] getJailLocations();

	public TobJailLocation getNextJailLocation() {
		int count = deathCount % getJailLocations().length;
		return getJailLocations()[count];
	}

	public void complete() {
		completed = true;
		tobManager.clearNpcs();
		tobManager.party.executeForMemberNoCmb(player -> {
			if (player.tobDead) {
				player.tobDead = false;
				player.getPA().movePlayer(enterLocation());
			}
		});
	}

	public boolean isCompleted() {
		return completed;
	}

	public TobRoom getNextRoom() {
		return nextRoom;
	}

}
