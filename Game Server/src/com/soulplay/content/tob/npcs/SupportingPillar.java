package com.soulplay.content.tob.npcs;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108377, 108378, 108379 })
public class SupportingPillar extends AbstractNpc {

	public static final int OBJECT_ID = 132687;
	public static final int BROKEN_OBJECT_ID = 132688;
	private GameObject gameObject;
	private List<Location> behindSpots;
	public int peopleBehind;
	public VerzikRoom verzikRoom;

	public SupportingPillar(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new SupportingPillar(slot, npcType);
	}

	/*@Override
	public void process() {
		super.process();
		
		if (behindSpots == null || behindSpots.isEmpty()) {
			return;
		}

		for (Location location : behindSpots) {
			Server.getStillGraphicsManager().createGfx(location, 369);
		}
	}*/

	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttack = true;
		getSkills().setMaximumLifepoints(350);
		behindSpots = new ArrayList<>();

		Location location = getCurrentLocation().copyNew();
		if (location.getLocalX() == 25) {
			Location southWest = location.transform(-3, -3);
			Location northEast = location.transform(0, 0);
			for (int x = southWest.getX(); x <= northEast.getX(); x++) {
				for (int y = southWest.getY(); y <= northEast.getY(); y++) {
					Location behindSpot = Location.create(x, y, location.getZ());
					behindSpots.add(behindSpot);
				}
			}
			behindSpots.add(location.transform(1, -1));
			behindSpots.add(location.transform(-1, 1));
		} else if (location.getLocalX() == 37) {
			Location southWest = location.transform(2, -2);
			Location northEast = location.transform(4, 0);
			for (int x = southWest.getX(); x <= northEast.getX(); x++) {
				for (int y = southWest.getY(); y <= northEast.getY(); y++) {
					Location behindSpot = Location.create(x, y, location.getZ());
					behindSpots.add(behindSpot);
				}
			}
			behindSpots.add(location.transform(1, -1));
			behindSpots.add(location.transform(3, 1));
		}

		gameObject = GameObject.createTickObject(OBJECT_ID, location, 0, 10, Integer.MAX_VALUE, -1);

		pulse(() -> {
			if (tobManager != null && tobManager.objectManager != null) {
				tobManager.objectManager.addObject(gameObject);
			}
		});
	}

	@Override
	public void onDeath() {
		super.onDeath();

		if (tobManager != null && tobManager.objectManager != null) {
			tobManager.objectManager.removeObject(gameObject, Integer.MAX_VALUE);
		}

		//TODO figure out why broken anim goofing
		Location location = getCurrentLocation();
		tobManager.objectManager.addObject(GameObject.createTickObject(BROKEN_OBJECT_ID, location.transform(-1, -1), 0, 10, 4, -1));

		verzikRoom.getTobManager().party.executeForMember(player -> {
			if (player.isActive() && location.isWithinDistance(player.getCurrentLocation().copyNew(), 3)) {
				int maxDamage = getCenterLocation().isWithinDistance(player.getCurrentLocation().copyNew(), 2) ? 85 : 55;
				player.dealDamage(new Hit(Misc.random(maxDamage / 2, maxDamage), 0, -1));
				player.sendMessage("The collapsing pillar greatly harms you.");
			}
		});
	}

	@Override
	public void npcHandlerProcessDestroy() {
		super.npcHandlerProcessDestroy();

		if (tobManager != null && tobManager.objectManager != null) {
			tobManager.objectManager.removeObject(gameObject, Integer.MAX_VALUE);
		}
	}

	public boolean playerIsBehind(Player player) {
		return isActive && behindSpots.contains(player.getCurrentLocation().copyNew());
	}

}
