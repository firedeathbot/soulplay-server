package com.soulplay.content.tob.npcs;

public enum VerzikVirturAtk {

	NYLOCAS(34),
	WEB(47),
	DOT(92),
	METEOR(47);

	public static final VerzikVirturAtk[] values = values();
	private final int nextDelayTicks;

	private VerzikVirturAtk(int nextDelayTicks) {
		this.nextDelayTicks = nextDelayTicks;
	}

	public int getNextDelayTicks() {
		return nextDelayTicks;
	}

}
