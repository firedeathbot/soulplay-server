package com.soulplay.content.tob.npcs;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 108381 })
public class NylocasIschyros extends NylocasBombNpc {

	public static final int ID = 108381;

	public NylocasIschyros(int slot, int npcType) {
		super(slot, npcType, CombatType.MELEE);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NylocasIschyros(slot, npcType);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(ID, Animation.getOsrsAnimId(8005), -1, -1);
	}

	@Override
	public int explodeAnim() {
		return Animation.getOsrsAnimId(8006);
	}

}
