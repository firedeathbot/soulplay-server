package com.soulplay.content.tob.npcs;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 108353 })
public class NylocasHagios extends NylocasBombNpc {

	public static final int ID = 108353;

	public NylocasHagios(int slot, int npcType) {
		super(slot, npcType, CombatType.MAGIC);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NylocasHagios(slot, npcType);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(ID, Animation.getOsrsAnimId(7991), -1, -1);
	}

	@Override
	public int explodeAnim() {
		return Animation.getOsrsAnimId(7992);
	}

}
