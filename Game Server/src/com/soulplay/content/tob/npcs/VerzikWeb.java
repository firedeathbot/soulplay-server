package com.soulplay.content.tob.npcs;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108376 })
public class VerzikWeb extends AbstractNpc {

	public static final int ID = 108376;
	private GameObject gameObject;
	private boolean webAppeared;

	public VerzikWeb(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new VerzikWeb(slot, npcType);
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(ID, 10, 0, 0, 0);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		stopAttack = true;
		gameObject = GameObject.createTickObject(132734, getCurrentLocation(), 0, 10, Integer.MAX_VALUE, -1);
		pulse(() -> {
			webAppeared = true;
			tobManager.objectManager.addObject(gameObject);
		});

		pulse(() -> {
			tobManager.party.executeForMember(player -> {
				if (player.getCurrentLocation().equals(getCurrentLocation())) {
					player.startGraphic(Graphic.create(Graphic.getOsrsId(1600)));
					player.pulse(() -> {
						player.sendMessage("The web explodes and you are released.");
						player.dealDamage(new Hit(Misc.random(40), 0, -1));
					});
				}
			});

			pulse(() -> {
				removeNpcSafe(true);
			});
		}, 20);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		if (tobManager != null && tobManager.objectManager != null) {
			tobManager.objectManager.removeObject(gameObject, Integer.MAX_VALUE);
		}
	}

	@Override
	public void npcHandlerProcessDestroy() {
		super.npcHandlerProcessDestroy();

		if (tobManager != null && tobManager.objectManager != null) {
			tobManager.objectManager.removeObject(gameObject, Integer.MAX_VALUE);
		}
	}

	public boolean isWebAppeared() {
		return webAppeared;
	}

}
