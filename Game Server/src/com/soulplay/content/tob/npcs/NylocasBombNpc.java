package com.soulplay.content.tob.npcs;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

public abstract class NylocasBombNpc extends CombatNPC {

	private CombatType combatType;
	private boolean exploded;

	public NylocasBombNpc(int slot, int npcType, CombatType combatType) {
		super(slot, npcType);
		this.combatType = combatType;
		this.stopAttackDefault = true;
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		pulse(() -> {
			dealDamage(new Hit(getSkills().getLifepoints()));
		}, 25);
	}

	@Override
	public int dealDamage(Hit hit) {
		Mob source = hit.getSource();
		
		if (source != null && source.isPlayer() && hit.getHitIcon() != combatType.getId()) {
			Player player = source.toPlayerFast();
			player.sendMessage("You can only damage the " + getNpcName() + " with " + combatType.getName() + ".");
			return 0;
		}

		return super.dealDamage(hit);
	}

	@Override
	public void process() {
		super.process();

		if (exploded) {
			return;
		}

		Player target = getKiller();
		if (target == null || !target.isActive || target.isDead() || target.getSkills().getLifepoints() <= 0 || target.tobDead) {
			return;
		}

		Location targetLoc = target.getCenterLocation().copyNew();
		walkToLocation(targetLoc);
		int dist = (int) getCenterLocation().getDistance(targetLoc);
		if (dist <= 2) {
			resetWalkTemp();
			setCanWalk(false);
			startAnimation(explodeAnim());
			exploded = true;
			pulse(() -> {
				int newDist = (int) getCenterLocation().getDistance(target.getCenterLocation().copyNew());
				if (newDist <= 2) {
					target.dealDamage(new Hit(Misc.random(50, 80)));
				}

				despawnNextTick = true;
			}, 3);
		}
	}

	public abstract int explodeAnim();

}
