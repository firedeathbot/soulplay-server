package com.soulplay.content.tob.npcs;

import com.soulplay.content.combat.CombatType;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 108382 })
public class NylocasToxobolos extends NylocasBombNpc {

	public static final int ID = 108382;

	public NylocasToxobolos(int slot, int npcType) {
		super(slot, npcType, CombatType.RANGED);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NylocasToxobolos(slot, npcType);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(ID, Animation.getOsrsAnimId(7998), -1, -1);
	}

	@Override
	public int explodeAnim() {
		return Animation.getOsrsAnimId(8000);
	}

}
