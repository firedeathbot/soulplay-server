package com.soulplay.content.tob.npcs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.soulplay.Server;
import com.soulplay.content.clans.ClanMember;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108369, 108370, 108372, 108373, 108374 })
public class VerzikVitur extends AbstractNpc {

	public static final int DAWNBRINGER_DMG_REDUCTION = 4;
	public VerzikRoom verzikRoom;
	public GameObject throneObject;
	private Location walkToLocation;
	private Runnable afterWalk;
	private int lastShield = -1;
	private float nextSpiderSpawn = 0.95f;
	private VerzikVirturAtk passiveSpell = VerzikVirturAtk.NYLOCAS;
	private int lastPassiveSpell = Server.getTotalTicks();
	private boolean spawnedTornados;
	private boolean switchingPhases;

	public VerzikVitur(int slot, int npcType) {
		super(slot, npcType);

		dontFace = true;
		stopAttack = true;

		noClip = true;
		randomWalk = false;
		setWalksHome(false);
		setWalkingHome(false);
		prayerReductionMelee = 0.4;
		prayerReductionRanged = 0.4;
		prayerReductionMagic = 0.4;
		setAttackTimer(10);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new VerzikVitur(slot, npcType);
	}

	@Override
	public void process() {
		super.process();

		if (walkToLocation != null) {
			walkToLocation(walkToLocation);

			int dist = (int) getCurrentLocation().getDistance(walkToLocation);
			if (dist <= 1) {
				walkToLocation = null;
				resetWalkTemp();

				pulse(() -> {
					afterWalk.run();
					afterWalk = null;
				});
			}
		}

		int phase = getPhaseId();

		if (getAttackTimer() != 0 || switchingPhases) {
			return;
		}

		switch (phase) {
			case 1:
				setAttackTimer(17);

				for (SupportingPillar pillar : verzikRoom.getPillars()) {
					pillar.peopleBehind = 0;
				}

				startAnimation(Animation.getOsrsAnimId(8110));
				pulse(() -> {
					if (getPhaseId() != 1) {
						return;
					}

					startAnimation(Animation.getOsrsAnimId(8109));

					tobManager.party.executeForMember(player -> {
						if (getPhaseId() != 1) {
							return;
						}

						boolean attack = true;
						for (SupportingPillar pillar : verzikRoom.getPillars()) {
							if (pillar.playerIsBehind(player)) {
								attack = false;
								pillar.peopleBehind++;
								break;
							}
						}
	
						if (!attack) {
							return;
						}
	
						face(player);
						pulse(() -> {
							Projectile projectile = Projectile.create(this, player, Graphic.getOsrsId(1580));
							projectile.setStartHeight(126);
							projectile.setEndHeight(28);
							projectile.setStartDelay(20);
							projectile.setEndDelay(250);
							projectile.setSlope(29);
							projectile.send();
							player.dealMagicDamage(Misc.random(255), 8, Graphic.create(Graphic.getOsrsId(1581)), this);
						}, 2);
					});
	
					int max = 0;
					SupportingPillar mostPeople = null;
					for (SupportingPillar pillar : verzikRoom.getPillars()) {
						if (max < pillar.peopleBehind) {
							max = pillar.peopleBehind;
							mostPeople = pillar;
						}
					}
	
					if (mostPeople == null) {
						return;
					}
	
					final SupportingPillar finalPillar = mostPeople;
					face(finalPillar);
					pulse(() -> {
						if (getPhaseId() != 1) {
							return;
						}

						Projectile projectile = Projectile.create(this, finalPillar, Graphic.getOsrsId(1580));
						projectile.setStartHeight(126);
						projectile.setEndHeight(52);
						projectile.setStartDelay(20);
						projectile.setEndDelay(250);
						projectile.setSlope(29);
						projectile.send();
						pulse(() -> {
							if (getPhaseId() != 1) {
								return;
							}

							finalPillar.startGraphic(Graphic.create(Graphic.getOsrsId(1582)));
							finalPillar.dealDamage(new Hit(Misc.random(40, 60), 0, -1));
						}, 8);
					}, 2);
				}, 2);
				break;
			case 2:
				if (isShielded()) {
					return;
				}

				setAttackTimer(4);
				float hpPerc = (float) getSkills().getLifepoints() / (float) getSkills().getMaximumLifepoints();
				if (nextSpiderSpawn > 0.05f && hpPerc <= nextSpiderSpawn) {
					if (nextSpiderSpawn < 0.35f) {
						nextSpiderSpawn -= 0.1f;
						spawnMatomenos();
					} else {
						// regular spiders
						nextSpiderSpawn -= 0.3f;
						spawnPurpleCrab();
						spawnCrabs(1, Math.max(1, tobManager.party.getMembers().size()));
					}
				} else {
					AtomicBoolean bounce = new AtomicBoolean(false);
					tobManager.party.executeForMember(player -> {
						if (underVerzik(player)) {
							bounce.set(true);
						}
					});

					if (bounce.get() && Misc.randomBoolean(2)) {
						bounceAttack();
					} else if (Misc.randomBoolean(25)) {
						electricAttack();
					} else if (hpPerc <= 0.35 && Misc.randomBoolean(hpPerc <= 0.15 ? 3 : hpPerc <= 0.25 ? 4 : 5)) {
						bloodAttack();
					} else {
						lampAttack();
					}
				}
				break;
			case 3:
				setAttackTimer(8);
				int now = Server.getTotalTicks();
				if (!spawnedTornados && ((double) getSkills().getLifepoints() / (double) getSkills().getMaximumLifepoints()) <= 0.20) {
					spawnedTornados = true;
					spawnTornados();
				} else if (now - lastPassiveSpell >= passiveSpell.getNextDelayTicks()) {
					switch(passiveSpell) {
						case NYLOCAS:
							spawnCrabs(1, Math.max(1, tobManager.party.getMembers().size()));
							break;
						case WEB:
							castWeb();
							break;
						case DOT:
							castDot();
							break;
						case METEOR:
							castMeteor();
							break;
						default:
							System.out.println("unhandled spell " +passiveSpell);
							break;
					}

					passiveSpell = VerzikVirturAtk.values[(passiveSpell.ordinal() + 1) % VerzikVirturAtk.values.length];
					lastPassiveSpell = now;
				} else {
					final Client p = (Client) PlayerHandler.players[getKillerId()];
					if (p != null && p.getPA().withinDistanceToNpc(this, getHitOffset() + 1) && Misc.randomBoolean()) {
						meleeAttack(p);
					} else if (Misc.randomBoolean()) {
						magicAttack();
					} else {
						rangedAttack();
					}
				}
				break;
		}
	}

	private void castMeteor() {
		Player random = Misc.random(tobManager.party.getMembers()).getPlayer();
		if (random == null) {
			return;
		}

		random.sendMessage("Verzik Vitur fires a powerful projectile in your direction...");
		startAnimation(Animation.create(Animation.getOsrsAnimId(8125)));
		pulse(() -> {
			ball(this, random, 0);
		}, 2);
	}

	private void ball(Mob from, Mob to, int bounces) {
		Projectile projectile = Projectile.create(from, to, Graphic.getOsrsId(1598));
		projectile.setStartHeight(from.isNpc() ? 48 : 28);
		projectile.setEndHeight(28);
		projectile.setStartDelay(0);
		projectile.setEndDelay(180);
		projectile.setSlope(9);
		projectile.setStartOffset(0);
		projectile.send();

		pulse(() -> {
			Player closest = null;
			int distance = Integer.MAX_VALUE;
			for (ClanMember clanMember : tobManager.party.getMembers()) {
				Player player = clanMember.getPlayer();
				if (player == null || player == to) {
					continue;
				}

				int thisDistance = player.getCurrentLocation().getDistanceInt(to.getCurrentLocation());
				if (thisDistance < distance) {
					closest = player;
					distance = thisDistance;
				}
			}

			if (closest == null || distance > 4) {
				int hit = 89 - 15 * (tobManager.party.getMembers().size() - bounces);
				to.startGraphic(Graphic.create(Graphic.getOsrsId(1600), GraphicType.HIGH));
				if (hit > 0) {
					to.dealDamage(new Hit(hit, 0, -1));
				}
			} else {
				to.dealDamage(new Hit(Misc.random(15), 0, -1));
				ball(to, closest, bounces + 1);
			}
		}, 6);
	}

	private void castDot() {
		transform(108373);

		setAttackTimer(10);
		setCanWalk(false);
		resetWalkTemp();
		getLockMovement().startTimer(20);
		startAnimation(Animation.create(Animation.getOsrsAnimId(8126)));
		Set<Location> dots = new HashSet<>();
		tobManager.party.executeForMember(player -> {
			Location dot = createDot(player, dots);
			if (dot == null) {
				return;
			}

			dots.add(dot);
			Server.getStillGraphicsManager().createGfx(dot, Graphic.getOsrsId(1595));
			pulse(() -> {
				Projectile projectile = Projectile.create(getCenterLocation(), dot, Graphic.getOsrsId(1596));
				projectile.setStartHeight(186);
				projectile.setEndHeight(0);
				projectile.setStartDelay(0);
				projectile.setEndDelay(120);
				projectile.setSlope(59);
				projectile.setStartOffset(0);
				projectile.send();
			}, 9);
		});

		int dotDuration = 6;
		for (int i = 1; i <= 2; i++) {
			pulse(() -> {
				for (Location dot : dots) {
					Server.getStillGraphicsManager().createGfx(dot, Graphic.getOsrsId(1595));
				}
			}, (dotDuration - 1) * i);
		}

		pulse(() -> {
			transform(108374);
			setCanWalk(true);
			pulse(() -> {
				Set<Location> dotsOccupied = new HashSet<>();
				for (Location dot : dots) {
					tobManager.party.executeForMember(player -> {
						if (player.getCurrentLocation().equals(dot) && !dotsOccupied.contains(dot)) {
							dotsOccupied.add(dot);
							player.startGraphic(Graphic.create(Graphic.getOsrsId(1597)));
							player.sendMessage("The power resonating here protects you from the blast.");
							return;
						}

						player.dealMagicDamage(Misc.random(31, 50), 0, Graphic.create(Graphic.getOsrsId(1600)), this);
					});
				}
			}, 3);
		}, 10);
	}

	private Location createDot(Player player, Set<Location> dots) {
		Location near = null;

		for (int i = 0; i < 5; i++) {
			near = player.getCurrentLocation().transform(Direction.getRandom(), Misc.random(0, 3));
			if (!dots.contains(near) && RegionClip.canStandOnSpot(near.getX(), near.getY(), near.getZ(), getDynamicRegionClip())) {
				break;
			}
		}

		return near;
	}

	private void castWeb() {
		transform(108373);

		setAttackTimer(30);
		walkTo(verzikRoom.base().transform(29, 22));
		afterWalk = new Runnable() {

			@Override
			public void run() {
				transform(108374);
				faceDirection(Direction.SOUTH);
				pulse(() -> {
					int durationTicks = 31;
					startAnimation(Animation.create(Animation.getOsrsAnimId(8127)));
					Set<Location> webLocs = new HashSet<>();
					AtomicInteger index = new AtomicInteger();

					pulse(() -> {
						enableAggression();
					}, durationTicks);

					for (int i = 0; i <= durationTicks - 13; i++) {
						pulse(() -> {
							ClanMember clanMember = tobManager.party.getMembers().get(index.getAndIncrement());
							if (index.get() >= tobManager.party.getMembers().size()) {
								index.set(0);
							}

							if (clanMember != null) {
								Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
								if (player != null && player.isActive && !player.isDead() && player.getSkills().getLifepoints() > 0) {
									face(player);
									Direction logicalDir = Direction.getDirection(getCenterLocation(), player.getCurrentLocation());
									Location startLoc = getCenterLocation().transform(logicalDir, 3);
									Location endLoc = player.getCurrentLocation();

									attemptWeb(startLoc, endLoc, webLocs);
									for (int j = 1; j <= 2; j++) {
										Location nearEndLoc = endLoc.transform(Direction.getRandom(), Misc.random(1, 3));
										attemptWeb(startLoc, nearEndLoc, webLocs);
									}
								}
							}
						}, i + 3);
					}
				});
			}
		};
	}

	private void attemptWeb(Location startLoc, Location webLoc, Set<Location> webLocs) {
		if (webLocs.contains(webLoc)) {
			return;
		}

		webLocs.add(webLoc);
		Projectile projectile = Projectile.create(startLoc, webLoc, Graphic.getOsrsId(1601));
		projectile.setStartHeight(132);
		projectile.setEndHeight(0);
		projectile.setStartDelay(0);
		projectile.setEndDelay(120);
		projectile.setSlope(14);
		projectile.setStartOffset(128);
		projectile.send();

		pulse(() -> {
			Location base = verzikRoom.base();
			Location spawnLocBase = webLoc.transform(-base.getX(), -base.getY());

			verzikRoom.spawnNpc(VerzikWeb.ID, spawnLocBase.getX(), spawnLocBase.getY());
		}, 3);
	}

	private void spawnTornados() {
		forceChat("I'm not done with you just yet!");
		tobManager.party.executeForMember(player -> {
			Location tornadoLoc = findTornadoLocation(player);
			if (tornadoLoc == null) {
				return;
			}

			Location base = verzikRoom.base();
			Location spawnLocBase = tornadoLoc.transform(-base.getX(), -base.getY());
			PurpleTornado purleTornado = (PurpleTornado) verzikRoom.spawnNpc(PurpleTornado.ID, spawnLocBase.getX(), spawnLocBase.getY());
			purleTornado.target = player;
		});
	}

	public Location findTornadoLocation(Player player) {
		for (int i = 0; i < 32; i++) {
			Location gen = player.getCurrentLocation().transform(Direction.getRandom(), Misc.random(4, 5));
			if (RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}
		
		return null;
	}

	private void meleeAttack(Player player) {
		startAnimation(Animation.getOsrsAnimId(8123));
		player.dealMeleeDamage(Misc.random(31, 50), 2, null, this);
	}

	private void magicAttack() {
		startAnimation(Animation.getOsrsAnimId(8124));
		pulse(() -> {
			tobManager.party.executeForMember(player -> {
				player.pulse(() -> {
					Projectile projectile = Projectile.create(getCenterLocation(), player, Graphic.getOsrsId(1594));
					projectile.setStartHeight(32);
					projectile.setEndHeight(28);
					projectile.setStartDelay(0);
					projectile.setEndDelay(90);
					projectile.setSlope(32);
					projectile.setStartOffset(128);
					projectile.send();

					player.dealMagicDamage(Misc.random(31, 50), 3, null, this);
				});
			});
		});
	}

	private void rangedAttack() {
		startAnimation(Animation.getOsrsAnimId(8125));
		pulse(() -> {
			tobManager.party.executeForMember(player -> {
				player.pulse(() -> {
					Projectile projectile = Projectile.create(getCenterLocation(), player, Graphic.getOsrsId(1593));
					projectile.setStartHeight(32);
					projectile.setEndHeight(28);
					projectile.setStartDelay(0);
					projectile.setEndDelay(90);
					projectile.setSlope(32);
					projectile.setStartOffset(128);
					projectile.send();

					player.dealRangeDamage(Misc.random(31, 50), 3, null, this);
				});
			});
		});
	}

	private void spawnCrabs(int startI, int endI) {
		for (int i = startI; i <= endI; i++) {
			Location spot = findCrabLocation();
			if (spot == null) {
				continue;
			}

			int id;
			switch (Misc.random(2)) {
				case 0:
					id = NylocasHagios.ID;
					break;
				case 1:
					id = NylocasToxobolos.ID;
					break;
				default:
					id = NylocasIschyros.ID;
					break;
			}

			Location base = verzikRoom.base();
			Location spawnLocBase = spot.transform(-base.getX(), -base.getY());
			verzikRoom.spawnNpc(id, spawnLocBase.getX(), spawnLocBase.getY());
		}
	}

	private Location findCrabLocation() {
		for (int i = 0; i < 10; i++) {
			Location gen = getCenterLocation().transform(Direction.getRandom(), Misc.random(5, 11));
			if (RegionClip.canStandOnSpot(gen.getX(), gen.getY(), gen.getZ(), getDynamicRegionClip())) {
				return gen;
			}
		}
		
		return null;
	}

	private void bloodAttack() {
		startAnimation(Animation.getOsrsAnimId(8114));

		ClanMember clanMember = Misc.random(tobManager.party.getMembers());
		if (clanMember == null) {
			return;
		}

		Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
		if (player == null || !player.isActive || player.isDead() || player.getSkills().getLifepoints() <= 0) {
			return;
		}

		Projectile projectile = Projectile.create(getCenterLocation(), player, Graphic.getOsrsId(1591));
		projectile.setStartHeight(54);
		projectile.setEndHeight(26);
		projectile.setStartDelay(20);
		projectile.setEndDelay(60);
		projectile.setSlope(7);
		projectile.setStartOffset(50);
		projectile.send();

		pulse(() -> {
			int damage = Misc.random(45);
			player.dealMagicDamage(damage, 0, Graphic.create(Graphic.getOsrsId(1592)), this);
			healNpc(damage / 2);
		}, 3);
	}

	private void electricAttack() {
		List<ClanMember> order = new ArrayList<>(tobManager.party.getMembers());
		order.removeIf(clanMember -> {
			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			return player == null || !player.isActive || player.isDead() || player.getSkills().getLifepoints() <= 0;
		});

		if (order.isEmpty()) {
			return;
		}

		Collections.shuffle(order);
		startAnimation(Animation.getOsrsAnimId(8114));
		int maxHit = 28;

		ClanMember lastPlayer = order.get(0);
		sendElectricBall(this, PlayerHandler.getPlayerByMID(lastPlayer.getMysqlID()), maxHit);
		//TODO test this

		if (order.size() > 1) {
			for (int i = 1; i < order.size(); i++) {
				ClanMember sendTo = order.get(i);
				ClanMember sendFrom = lastPlayer;

				final int index = i;
				pulse(() -> {
					sendElectricBall(PlayerHandler.getPlayerByMID(sendFrom.getMysqlID()), PlayerHandler.getPlayerByMID(sendTo.getMysqlID()), (int) Math.max(0, Math.ceil(maxHit * (1 - (index * 0.2)))));
				}, 3 * i);
				lastPlayer = sendTo;
			}
		}
	}

	private void sendElectricBall(Mob from, Player player, int maxHit) {
		if (from == null || player == null) {
			return;
		}

		Projectile projectile = Projectile.create(from, player, Graphic.getOsrsId(1585));
		projectile.setStartHeight(48);
		projectile.setEndHeight(0);
		projectile.setStartDelay(0);
		projectile.setEndDelay(90);
		projectile.setSlope(9);
		projectile.setStartOffset(50);
		projectile.send();

		player.dealMagicDamage(Misc.random(maxHit), 4, null, this);
	}

	private void bounceAttack() {
		if (Misc.randomBoolean(4)) {
			forceChat("There's nothing under there for you!");
		}

		startAnimation(Animation.getOsrsAnimId(8116));
		pulse(() -> {
			tobManager.party.executeForMember(player -> {
				if (underVerzik(player)) {
					player.stunEntity(6, Graphic.create(80, GraphicType.HIGH));
					player.getCombat().resetPlayerAttack();

					//TODO dmg? lol idk
					player.dealMeleeDamage(Misc.random(31, 50), 0, null, this);
					Direction direction = Direction.getDirection(getCenterLocation(), player.getCurrentLocation());

					ForceMovementMask mask1 = ForceMovementMask.createMask(direction.getStepX() * 2, direction.getStepY() * 2, 1, Animation.getOsrsAnimId(8043), direction);
					mask1.addStartAnim(Animation.getOsrsAnimId(8043));
					player.setForceMovement(mask1);
				}
			});
		});
	}

	private void lampAttack() {
		startAnimation(Animation.getOsrsAnimId(8114));
		pulse(() -> {
			tobManager.party.executeForMember(player -> {
				boolean fast = Misc.randomBoolean(5);
				Location endLoc = player.getCurrentLocation().copyNew();
				Projectile projectile = Projectile.create(getCenterLocation(), endLoc, Graphic.getOsrsId(1583));
				projectile.setStartHeight(52);
				projectile.setEndHeight(0);
				projectile.setStartDelay(20);
				projectile.setEndDelay(fast ? 60 : 90);
				projectile.setSlope(14);
				projectile.setStartOffset(50);
				projectile.send();

				pulse(() -> {
					Server.getStillGraphicsManager().createGfx(endLoc, Graphic.getOsrsId(1584));
					if (player.getCurrentLocation().equals(endLoc)) {
						player.dealRangeDamage(Misc.random(31, 50), 0, null, this);
					}
				}, fast ? 2 : 3);
			});
		});
	}

	private boolean underVerzik(Player player) {
		Location l = player.getCurrentLocation().copyNew();
		Location base = verzikRoom.base();
		Location bl = base.transform(29, 24);
		Location tr = base.transform(33, 27);
		return l.getX() >= bl.getX() && l.getY() >= bl.getY() && l.getX() <= tr.getX() && l.getY() <= tr.getY();
	}

	private void spawnPurpleCrab() {
		Location center = getCenterLocation();
		Location spawnLoc = center.transform(Direction.getRandom(), Misc.random(3, 6));
		Projectile projectile = Projectile.create(center, spawnLoc, Graphic.getOsrsId(1586));
		projectile.setStartHeight(54);
		projectile.setEndHeight(0);
		projectile.setStartDelay(0);
		projectile.setEndDelay(240);
		projectile.setSlope(7);
		projectile.send();

		pulse(() -> {
			Location base = verzikRoom.base();
			Location spawnLocBase = spawnLoc.transform(-base.getX(), -base.getY());
			verzikRoom.spawnNpc(NylocasAthanatos.ID, spawnLocBase.getX(), spawnLocBase.getY());
		}, 8);
	}

	private void spawnMatomenos() {
		lastShield = Server.getTotalTicks();
		startAnimation(Animation.getOsrsAnimId(8117));

		for (NPC npc : tobManager.npcs) {
			if (npc == null || !npc.isActive || npc.isDead() || npc.getSkills().getLifepoints() <= 0) {
				continue;
			}

			if (!(npc instanceof NylocasMatomenos)) {
				continue;
			}

			getSkills().healToFull();
		}

		verzikRoom.spawnNpc(NylocasMatomenos.ID, 38, 24);
		verzikRoom.spawnNpc(NylocasMatomenos.ID, 26, 24);
	}

	public void phase1() {
		transform(108370);
		getSkills().setStaticLevel(Skills.HITPOINTS, 2000);
		disableAggression();
		setAttackTimer(17);
	}

	public void phase2() {
		transform(108372);
		getSkills().setStaticLevel(Skills.HITPOINTS, 2000);
		enableAggression();
		walkingType = 0;
		takeDamage = true;
		switchingPhases = false;

		dontFace = false;
		faceLocation(getCurrentLocation().transform(Direction.SOUTH, 10));
		dontFace = true;
		resetWalkTemp();
		setCanWalk(false);
		stopFollow = true;
	}

	@Override
	public int dealDamage(Hit hit) {
		int phase = getPhaseId();
		if (phase == 1) {
			hit.setDamage(hit.getDamage() / DAWNBRINGER_DMG_REDUCTION);
		}

		int damage = super.dealDamage(hit);

		if (getSkills().getLifepoints() <= 0) {
			getSkills().healToFull();
			takeDamage = false;
			switchingPhases = true;
			switch (phase) {
				case 1:
					startPhase2();
					break;
				case 2:
					startPhase3();
					break;
				case 3:
					disableAggression();
					resetAnimations();
					resetFace();
					dontFace = true;
					setCanWalk(false);
					pulse(() -> {
						startAnimation(Animation.getOsrsAnimId(8128));
						pulse(() -> {
							transform(108375);
							resetAnimations();
							pulse(() -> {
								removeNpcSafe(true);
								verzikRoom.complete();
							}, 7);
						}, 3);
					});
					break;
			}
		}

		return damage;
	}

	public void startPhase3() {
		resetAnimations();
		pulse(() -> {
			startAnimation(Animation.getOsrsAnimId(8118));
			getSkills().healToFull();
			pulse(() -> {
				transform(108373);
				startAnimation(Animation.getOsrsAnimId(8119));

				pulse(() -> {
					resetAnimations();
					transform(108374);
					takeDamage = true;
					dontFace = false;
					switchingPhases = false;
					setCanWalk(true);
					stopFollow = false;
					enableAggression();
					setAttackTimer(1);
				}, 4);
			}, 2);
		});
	}

	public void startPhase2() {
		tobManager.wipeDawnbringer();
		resetAnimations();
		pulse(() -> {
			startAnimation(Animation.getOsrsAnimId(8111));
			getSkills().healToFull();
			pulse(() -> {
				transform(108371);
				startAnimation(Animation.getOsrsAnimId(8112));

				Location base = verzikRoom.base();

				throneObject = GameObject.createTickObject(132737, base.transform(31, 36), 0, 10, Integer.MAX_VALUE, -1);
				tobManager.objectManager.addObject(throneObject);

				pulse(() -> {
					resetAnimations();
					pulse(() -> {
						walkTo(base.transform(30, 24));
						afterWalk = new Runnable() {

							@Override
							public void run() {
								phase2();
							}

						};

						for (SupportingPillar pillar : verzikRoom.getPillars()) {
							pillar.setDead(true);
						}
					});
				});
			}, 4);
		});
	}

	public int getPhaseId() {
		switch (getNpcTypeSmart()) {
			case 108370:
				return 1;
			case 108372:
				return 2;
			case 108374:
				return 3;
			default:
				return 0;
		}
	}

	public boolean isShielded() {
		return lastShield >= 0 && Server.getTotalTicks() - lastShield < 9;
	}

	public void disableAggression() {
		isAggressive = false;
		setRetaliates(false);
		stopFollow = true;
		resetAttack();
		face(null);
	}

	public void enableAggression() {
		isAggressive = true;
		setRetaliates(true);
		stopFollow = false;
	}

	public void walkTo(Location location) {
		disableAggression();
		resetWalkTemp();
		walkToLocation = location;
	}

}
