package com.soulplay.content.tob.npcs;

import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.tob.rooms.TobRoom;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 108384 })
public class NylocasAthanatos extends AbstractNpc {

	public static final int ID = 108384;
	private boolean hitByPoisonWeapon;

	public NylocasAthanatos(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NylocasAthanatos(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		startAnimation(Animation.getOsrsAnimId(8079));
		stopAttack = true;
		isAggressive = false;
		setRetaliates(false);
	}

	@Override
	public int dealDamage(Hit hit) {
		Mob source = hit.getSource();
		if (source != null && source.isPlayer()) {
			Player player = source.toPlayerFast();
			int weaponId = player.playerEquipment[PlayerConstants.playerWeapon];
			if (weaponId > 0) {
				String weaponName = ItemConstants.getItemName(weaponId);
				boolean poisonWeapons = weaponName.contains("(p)") || weaponName.contains("(p+)")
						|| weaponName.contains("(p++)") || weaponId == 15444 || weaponId == 12006 || weaponId == 30011
						|| weaponId == BlowPipeCharges.BLOWPIPE_FULL;
				if (hit.getDamage() > 0 && poisonWeapons) {
					hitByPoisonWeapon = true;
					setAttackTimer(0);
					startGraphic(Graphic.osrs(1590));
				}
			}
		}

		return super.dealDamage(hit);
	}

	@Override
	public void process() {
		super.process();

		if (tobManager == null || isDead() || getSkills().getLifepoints() <= 0) {
			return;
		}

		TobRoom tobRoom = tobManager.currentRoom;
		if (tobRoom == null || !(tobRoom instanceof VerzikRoom)) {
			return;
		}

		VerzikVitur verzik = ((VerzikRoom) tobRoom).getVerzik();
		if (verzik == null || !verzik.isActive || verzik.isDead()) {
			return;
		}

		face(verzik);

		if (getAttackTimer() <= 0) {
			setAttackTimer(6);
			Projectile projectile = Projectile.create(this, verzik, Graphic.getOsrsId(hitByPoisonWeapon ? 1588 : 1587));
			projectile.setStartHeight(28);
			projectile.setEndHeight(32);
			projectile.setStartDelay(0);
			projectile.setEndDelay(60);
			projectile.setSlope(7);
			projectile.send();
			verzik.pulse(() -> {
				if (hitByPoisonWeapon) {
					verzik.dealDamage(new Hit(Misc.random(70, 75), 2));
					pulse(() -> {
						setDead(true);
					});
				} else {
					verzik.getSkills().heal(Misc.random(9, 11));
				}
			}, 2);
		}
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(ID, Animation.getOsrsAnimId(8078), -1, -1);
	}

}
