package com.soulplay.content.tob.npcs;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;

@NpcMeta(ids = { 108386 })
public class PurpleTornado extends AbstractNpc {

	public static final int ID = 108386;
	public Player target;

	public PurpleTornado(int slot, int npcType) {
		super(slot, npcType);

		walksOnTopOfNpcs = true;
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new PurpleTornado(slot, npcType);
	}
	
	@Override
	public void process() {
		super.process();

		if (target == null || target.isDead() || target.getSkills().getLifepoints() <= 0 || target.tobDead || !isVisible()) {
			return;
		}

		walkToLocation(target.getCurrentLocation());
		if (!getCurrentLocation().equals(target.getCurrentLocation())) {
			return;
		}

		target.startGraphic(Graphic.create(Graphic.getOsrsId(1602)));
		setVisible(false);
		pulse(() -> {
			int damage = target.getSkills().getLifepoints() / 2;
			target.dealDamage(new Hit(damage, 0, -1));
			((VerzikRoom) tobManager.currentRoom).getVerzik().healNpc(damage);
			target.sendMessage("Verzik Vitur saps your health and powers up!");
		});
		pulse(() -> {
			Location location = ((VerzikRoom) tobManager.currentRoom).getVerzik().findTornadoLocation(target);
			if (location != null) {
				 moveNpcNow(location);
			}
			
			pulse(() -> {
				setVisible(true);
			});
		}, 6);
	}

	@Override
	public void nullNPC() {
		super.nullNPC();

		target = null;
	}

}
