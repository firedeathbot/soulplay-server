package com.soulplay.content.tob.npcs;

import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.world.entity.Animation;

@NpcMeta(ids = { 108385 })
public class NylocasMatomenos extends AbstractNpc {

	public static final int ID = 108385;

	public NylocasMatomenos(int slot, int npcType) {
		super(slot, npcType);

		stopAttack = true;
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new NylocasMatomenos(slot, npcType);
	}

	@Override
	public void defineConfigs() {
		NPCAnimations.define(ID, Animation.getOsrsAnimId(8097), -1, -1);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		actionTimer = 3;
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		startAnimation(Animation.getOsrsAnimId(8098));
		pulse(() -> {
			if (!isActive || isDead() || getSkills().getLifepoints() <= 0) {
				return;
			}

			if (tobManager != null && tobManager.currentRoom != null && tobManager.currentRoom instanceof VerzikRoom) {
				VerzikRoom verzikRoom = (VerzikRoom) tobManager.currentRoom;
				verzikRoom.getVerzik().healNpc(getSkills().getLifepoints());
				startAnimation(Animation.getOsrsAnimId(8097));
				setDead(true);
			}
		}, 50);// 50
	}

}
