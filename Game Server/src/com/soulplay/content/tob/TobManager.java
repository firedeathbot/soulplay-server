package com.soulplay.content.tob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.clans.ClanMember;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.raids.RaidsParty;
import com.soulplay.content.tob.rooms.TobRoom;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.instances.DynSingleInstanceManager;
import com.soulplay.game.world.map.DynamicObjectManager;
import com.soulplay.game.world.map.ObjectDefVersion;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.Zone;

import plugin.item.DawnbringerPlugin;

public class TobManager {

	public static boolean tobEnabled = true;

	private static final Map<Integer, TobManager> instances = new HashMap<>();
	public static DynSingleInstanceManager zInstanceManager = new DynSingleInstanceManager();

	private Clan clan;
	private int instanceId;
	public int height;
	public int startTicks;
	public List<Region> regions = new ArrayList<>();
	public DynamicRegionClip clipping = new DynamicRegionClip();
	public DynamicObjectManager objectManager = new DynamicObjectManager(clipping);
	public List<NPC> npcs = new ArrayList<>();
	public RaidsParty party;
	public int startedPlayerMID;
	public CycleEventContainer cleanupEvent;
	public CycleEventContainer endTimer;
	public boolean destroyed = false;
	public TobRoom currentRoom;
	public Zone zone;
	private int deathCount;
	public List<GroundItem> items = new ArrayList<>();

	public TobManager(Clan clan, Player startedPlayer) {
		this.startedPlayerMID = startedPlayer.mySQLIndex;
		this.clan = clan;
		this.instanceId = this.height = getInstanceId(startedPlayer.mySQLIndex);
		this.objectManager.setDefsVersion(ObjectDefVersion.OSRS_DEFS);
	}
	
	public static boolean instanceExists(int leaderUniqueID) {
		return instances.get(getInstanceId(leaderUniqueID)) != null;
	}

	public static int getInstanceId(int uniqueId) {
		return zInstanceManager.getOrCreateZ(uniqueId);
	}

	public void generate() {
		instances.put(instanceId, this);
		setRoom(new VerzikRoom(this));
	}

	private void setRoom(TobRoom room) {
		this.currentRoom = room;
		this.currentRoom.configure();

		Location base1 = room.base();
		Rectangle rec2 = null;
		TobRoom nextRoom = room.getNextRoom();
		if (nextRoom != null) {
			nextRoom.configure();

			Location base2 = nextRoom.base();
			rec2 = new Rectangle(base2, base2.transform(64, 64));
		}

		zone = new TobZone(new Rectangle(base1, base1.transform(64, 64)), rec2);
	}

	public void startTob(Player starterPlayer) {
		if (clan == null) {
			destroy();
			return;
		}

		party = new RaidsParty(clan.getClanMysqlId());

		for (int i = 0, length = clan.activeMembers.size(); i < length; i++) {
			ClanMember clanMember = clan.activeMembers.get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null) {
				continue;
			}

			if (ItemRetrievalManager.hasItems(player, false)) {
				starterPlayer.sendMessage("Unable to start. " + player.getNameSmartUp() + " has unclaimed items from the Grim reaper.");
				party = null;
				return;
			}

			if (player.tobManager != this) {
				continue;
			}

			Variables.TOB_INSTANCE_ID.setValue(player, instanceId);

			party.addMember(clanMember);

			// TODO do we need this?
			if (zone != null) {
				player.getZones().addDynamicZone(zone);
			}
		}

		clan.tobManager = null;
		clan = null;

		for (int i = 0, length = party.getMembers().size(); i < length; i++) {
			ClanMember clanMember = party.getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (player == null) {
				continue;
			}

			refreshSidebar((Client) player);
		}

		startTicks = Server.getTotalTicks();

		Location dawnBringer = currentRoom.base().transform(37, 34);
		spawnItem(DawnbringerPlugin.ID, 1, dawnBringer.getX(), dawnBringer.getY());

		if (party.getMembers().size() == 0) {
			destroy();
		}
	}

	public void spawnItem(int item, int count, int x, int y) {
		if (items == null) {
			return;
		}

		GroundItem groundItem = new GroundItem(item, x, y, height, count, 0);
		ItemHandler.createGroundItem(groundItem, true);
		groundItem.setInMinigame(true);
		items.add(groundItem);
	}

	public boolean started() {
		return party != null;
	}

	public static void leaveTobDialog(Player player) {
		if (player.isInCombatDelay()) {
			player.sendMessage("You can't leave raids in combat.");
			return;
		}

		player.getDialogueBuilder().sendStatement("Are you sure you want to leave this raid?").sendOption("Yes.", () -> {
			if (player.tobManager == null) {
				return;
			}

			player.tobManager.leaveTob(player, true);
		}, "No.", () -> {

		}).execute();
	}

	public static void checkRaidStart(Player player) {
		{
			TobManager raids = player.tobManager;
			if (raids == null) {
				return;
			}

			if (raids.startedPlayerMID != player.mySQLIndex) {
				player.sendMessage("Only leader may start a raid.");
				return;
			}

			if (raids.started()) {
				return;
			}
		}

		player.getDialogueBuilder().sendOption("No-one may join the party after the raid begins.", "Begin the raid.", () -> {
				if (player.getClan() == null) {
					player.sendMessage("You must be in a clan to start raiding.");
					return;
				}

				TobManager raids = player.tobManager;
				if (raids == null) {
					player.sendMessage("You must be in a raiding session to start raiding.");
					return;
				}

				if (raids.started()) {
					return;
				}

				raids.startTob(player);
			}, "Don't begin the raid yet.", () -> {
		}).execute();
	}

	public static void refreshSidebar(Client c) {
		TobManager tobManager = c.tobManager;
		if (tobManager == null) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(7);

		List<ClanMember> members = null;
		if (tobManager.party != null) {
			members = tobManager.party.getMembers();
		} else if (tobManager.clan != null) {
			members = tobManager.clan.activeMembers;
		}

		int length;
		if (members == null) {
			length = 0;
			c.getOutStream().writeByte(length);
		} else {
			length = members.size();
			c.getOutStream().writeByte(length);
			for (int i = 0; i < length; i++) {
				ClanMember clanMember = members.get(i);
				if (clanMember == null) {
					c.getOutStream().writeString("");
					continue;
				}

				Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (member == null) {
					c.getOutStream().writeString("");
					continue;
				}

				if (member.tobManager == tobManager) {
					if (member.mySQLIndex == tobManager.startedPlayerMID) {
						c.getOutStream().writeString("<col=ffffff>" + member.getNameSmartUp());
					} else {
						c.getOutStream().writeString(member.getNameSmartUp());
					}
				} else {
					c.getOutStream().writeString("<col=757170>" + member.getNameSmartUp());
				}

				c.getOutStream().writeByte(member.combatLevel);
				c.getOutStream().writeShort(member.getSkills().getTotalLevel());
			}
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();

		if (tobManager.startedPlayerMID == c.mySQLIndex && !tobManager.started()) {
			c.getPacketSender().setVisibility(67209, false);
			c.getPacketSender().setVisibility(67212, true);
		} else {
			c.getPacketSender().setVisibility(67209, true);
			c.getPacketSender().setVisibility(67212, false);
		}

		if (tobManager.started()) {
			c.getPacketSender().sendString("Your party has reached the bottom...", 67213);
		} else {
			c.getPacketSender().sendString("Waiting for your leader to begin the raid...", 67213);
		}

		c.getPacketSender().sendString("Party Size: <col=ffffff>" + length + "</col>", 67207);
	}

	public void checkWiped() {
		if (endTimer != null || !started()) {
			return;
		}

		endTimer = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				if (party.isPartyWiped()) {
					activityFailed();
				}

				container.stop();
			}

			@Override
			public void stop() {
				endTimer = null;
			}
		}, 4);
	}

	public void leaveTob(Player player, boolean movePlayer) {
		boolean tobStarted = started();

		if (!tobStarted && clan != null) {
			int playersInTob = 0;

			for (int i = 0, length = clan.activeMembers.size(); i < length; i++) {
				ClanMember clanMember = clan.activeMembers.get(i);
				if (clanMember == null) {
					continue;
				}

				Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (member == null) {
					continue;
				}

				if (member.tobManager == this) {
					playersInTob++;
				}
			}

			if (playersInTob <= 1) {
				clan.tobManager = null;
				destroy();
			}
		}

		if (tobStarted) {
			party.removePlayer(player.mySQLIndex);

			for (int i = 0, length = party.getMembers().size(); i < length; i++) {
				ClanMember clanMember = party.getMembers().get(i);
				if (clanMember == null) {
					continue;
				}

				Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (member == null) {
					continue;
				}

				refreshSidebar((Client) member);
			}

			if (party.getMembers().isEmpty()) {
				destroy();
			}
		}

		revertPlayerDefaults(player, movePlayer);
	}

	public void revertPlayerDefaults(Player player, boolean movePlayer) {
		Variables.TOB_INSTANCE_ID.setValue(player, 0);
		wipeDawnbringer(player);
		player.extendedNpcRendering = false;
		player.setDynamicRegionClip(null);
		player.dynObjectManager = null;
		player.tobManager = null;
		player.tobDead = false;
		player.getPacketSender().setSidebarInterface(2, 638);
		checkWiped();

		if (movePlayer) {
			movePlayerOutside(player);
		}
	}

	public void wipeDawnbringer(Player player) {
		wipeDawnbringer(player, !destroyed);
	}

	public void wipeDawnbringer() {
		clearItems();
		party.executeForMemberNoCmb(player -> {
			wipeDawnbringer(player, false);
		});
	}

	public void wipeDawnbringer(Player player, boolean drop) {
		boolean dropDawnbringer = false;

		for (int i = 0, length = player.playerItems.length; i < length; i++) {
			int itemId = player.playerItems[i] - 1;
			if (itemId < 0) {
				continue;
			}

			if (itemId == DawnbringerPlugin.ID) {
				player.getItems().deleteItemInSlot(i);
				dropDawnbringer = true;
			}
		}

		for (int i = 0, length = player.playerEquipment.length; i < length; i++) {
			int itemId = player.playerEquipment[i];
			if (itemId < 0) {
				continue;
			}

			if (itemId == DawnbringerPlugin.ID) {
				player.getItems().deleteEquipment(itemId, i);
				dropDawnbringer = true;
			}
		}

		if (drop && dropDawnbringer) {
			spawnItem(DawnbringerPlugin.ID, 1, player.absX, player.absY);
		}
	}

	public void teleportPlayer(Player player) {
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				Location location = Location.create(currentRoom.enterLocation().getX(), currentRoom.enterLocation().getY(), height);
				player.getPA().movePlayer(location.getX(), location.getY(), location.getZ());
				player.pulse(() -> {
					player.blockRun = true;
					player.noClip = true;
					player.getMovement().addToWalkingQueue(player.getLocalX(), player.getLocalY() + 5);
					player.pulse(() -> {
						player.blockRun = false;
						player.noClip = false;
					}, 5);
				});

				applyInstance(player);

				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 1);
	}
	
	public void applyInstance(Player player) {
		player.tobManager = this;
		player.tobDead = false;
		player.extendedNpcRendering = true;
		player.setDynamicRegionClip(clipping);
		player.dynObjectManager = objectManager;
		player.getPacketSender().setSidebarInterface(2, 67200);
		refreshSidebar((Client) player);

		if (zone != null) {
			player.getZones().addDynamicZone(zone);
		}
	}

	public void addNpc(NPC npc) {
		npcs.add(npc);
	}

	public GameObject getRealObject(int id, int x, int y, int z) {
		if (objectManager == null) {
			System.out.println("what? null object manager? why?");
			return null;
		}
		
		// look for spawned object first
		GameObject o = objectManager.getObject(id, x, y, z);
		if (o != null && o.isActive())
			return o;
		
		for (int i = 0; i < 4; i++) {
			o = objectManager.getStaticObjects().get(GameObject.generateKey(x, y, z, i));
			if (o != null && o.getId() == id) {
				return o;
			}
		}

		return null;
	}

	public void handleLogout(Player p) {
		boolean started = started();

		if (!started) {
			leaveTob(p, true);
		} else {
			//count how much active users, if none, start cleanup timer
			int activeUsers = countActiveUsers() - 1;

			if (activeUsers == 0) {
				startCleanupEvent();
			}
		}
	}

	public int countActiveUsers() {
		int activeUsers = 0;
		
		for (int i = 0, length = party.getMembers().size(); i < length; i++) {
			ClanMember clanMember = party.getMembers().get(i);
			if (clanMember == null) {
				continue;
			}

			Player member = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
			if (member == null) {
				continue;
			}

			if (!member.disconnected) {
				activeUsers++;
			}
		}
		
		return activeUsers;
	}
	
	public static void handleLoginInsideRaids(Player p) {
		int instanceId = Variables.TOB_INSTANCE_ID.getValue(p);
		if (instanceId == 0) {
			return;
		}

		TobManager tob = instances.get(instanceId);
		
		boolean inParty = tob != null && tob.party != null && tob.party.isMember(p.mySQLIndex);
		
		if (inParty) {
			if (tob != null && tob.zone.isInZoneCoordinates(p)) {
				tob.applyInstance(p);
				if (tob.countActiveUsers() > 0)
					tob.endCleanup();
				return;
			}
		}
		
		//remove from old instance
		Variables.TOB_INSTANCE_ID.setValue(p, 0);
		
		movePlayerOutside(p);
	}
	
	public static void movePlayerOutside(Player p) {
		p.getPA().movePlayer(10076, 3219, 0);
	}
	
	public void endCleanup() {
		if (cleanupEvent == null) {
			return;
		}

		cleanupEvent.stop();
		cleanupEvent = null;
	}

	public void startCleanupEvent() {
		if (cleanupEvent != null) {
			return;
		}

		if (PlayerHandler.updateRunning) {
			destroy();
			return;
		}

		cleanupEvent = CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				destroy();
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 100);
	}
	
	public void destroy() {
		if (destroyed) {
			return;
		}

		destroyed = true;

		//removing players on destruct
		if (!started() && clan != null) {
			for (int i = 0, length = clan.activeMembers.size(); i < length; i++) {
				ClanMember clanMember = clan.activeMembers.get(i);
				if (clanMember == null) {
					continue;
				}

				Player player = PlayerHandler.getPlayerByMID(clanMember.getMysqlID());
				if (player == null) {
					continue;
				}

				if (player.tobManager == null || player.tobManager != this) {
					continue;
				}

				leaveTob(player, true);
			}
		}

		endCleanup();

		if (endTimer != null) {
			endTimer.stop();
			endTimer = null;
		}

		if (objectManager != null) {
			objectManager.cleanup();
			objectManager = null;
		}

		if (clipping != null) {
			clipping.cleanup();
			clipping = null;
		}

		clearItems();
		items = null;

		clearNpcs();
		npcs = null;

		instances.remove(instanceId);
	}

	public void clearItems() {
		if (items == null) {
			return;
		}

		for (int i = items.size() - 1; i >= 0; i--) {
			items.get(i).setRemoveTicks(2);
		}

		items.clear();
	}

	public void clearNpcs() {
		if (npcs == null) {
			return;
		}

		for (int i = npcs.size() - 1; i >= 0; i--) {
			npcs.get(i).nullNPC();
		}

		npcs.clear();
	}

	public void incDeathCount() {
		this.deathCount++;
	}

	public int getDeathCount() {
		return deathCount;
	}

	public void activityFailed() {
		party.executeForMemberNoCmb(player -> {
			player.sendMessage("Your party has failed.");
			revertPlayerDefaults(player, true);
			loseItems(player);
		});

		destroy();
	}

	private void loseItems(Player player) {
		final boolean keepItemOnDeath = player.getPA().keepItemsOnDeathPrayerOn();
		player.getItems().resetKeepItems();
		player.getItems().grabUntradeables();

		player.getItems().keepItem(0, true);
		player.getItems().keepItem(1, true);
		player.getItems().keepItem(2, true);
		if (keepItemOnDeath) {
			player.getItems().keepItem(3, true);
		}

		if (ItemRetrievalManager.transfer(player)) {
			player.getItems().deleteAllItems();

			for (int i1 = 0; i1 < 3; i1++) {
				if (player.itemKeptId[i1] > 0) {
					player.getItems().addItem(player.itemKeptId[i1], 1);
				}
			}

			if (keepItemOnDeath) {
				if (player.itemKeptId[3] > 0) {
					player.getItems().addItem(player.itemKeptId[3], 1);
				}
			}

			player.getItems().giveBackUntradeables(null, false);
		}

		player.itemsToKeep.clear();
		player.getItems().resetKeepItems();
	}

}
