package com.soulplay.content.tob;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.DRectangleZone;

public class TobZone extends DRectangleZone {

	public TobZone(Rectangle rec1, Rectangle rec2) {
		super(rec1, rec2);
		makeDynamicZone();
	}

	@Override
	public void onExit(Player p) {
		super.onExit(p);

		if (p.tobManager != null) {
			p.tobManager.leaveTob(p, false);
		}
	}

	@Override
	public void onLogout(Player player) {
		super.onLogout(player);

		if (player.tobManager != null) {
			player.tobManager.handleLogout(player);
		}
	}

}
