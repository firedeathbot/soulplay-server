package com.soulplay.content.shops;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;

public class Sell {

	String seller;

	private int mysqlIndex;

	GameItem item;

	int price;

	int shop;

	public Sell(Client seller, GameItem item, int price, int shop) {
		this.seller = seller.getName();
		this.setMysqlIndex(seller.mySQLIndex);
		this.item = item;
		this.price = price;
		this.shop = shop;
	}

	public GameItem getItem() {
		return item;
	}

	public int getMysqlIndex() {
		return mysqlIndex;
	}

	public int getPrice() {
		return price;
	}

	public String getSeller() {
		return seller;
	}

	public int getShop() {
		return shop;
	}

	public void setMysqlIndex(int mysqlIndex) {
		this.mysqlIndex = mysqlIndex;
	}
}
