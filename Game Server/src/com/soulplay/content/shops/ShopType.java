package com.soulplay.content.shops;

public enum ShopType {
	REGULAR(3824, 3901, 3900, 40),
	BIG(44000, 44002, 44011, 160),
	SCROLL(67100, 67102, 67111, 160);

	private final int id, titleId, inventoryId, maxItems;

	private ShopType(int id, int titleId, int inventoryId, int maxItems) {
		this.id = id;
		this.titleId = titleId;
		this.inventoryId = inventoryId;
		this.maxItems = maxItems;
	}

	public int getId() {
		return id;
	}

	public int getTitleId() {
		return titleId;
	}

	public int getInventoryId() {
		return inventoryId;
	}

	public int getMaxItems() {
		return maxItems;
	}

}
