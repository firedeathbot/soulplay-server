package com.soulplay.content.shops;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;

public class ShopPrice {

	public static int getSeasonalShopItemValue(Player player, int id) {
		int price;
		switch (id) {
			case 455:
				price = 5;
				break;
			default:
				price = 0;
				break;
		}

		int mult = 100;
		int boughtAmount = player.getSeasonalData().getBoughtAmount(id);
		if (boughtAmount > 0) {
			mult += 20 * boughtAmount;
		}

		return price * mult / 100;
	}

	public static int getGabootyShopItemValue(int id) {
	    switch (id) {
	    
	    case 6341: //brown top
	    case 6351: //blue top
	    case 6361: //blue and brown top
	    case 6371: //white and red top
	    	return 600;
	    	
	    case 6343: //brown robe
	    case 6353: //blue robe
	    case 6363: //blue and brown robe
	    case 6373: //white and red robe
	    	return 550;
	    	
	    case 6345: //brown hat
	    case 6355: //blue hat
	    case 6365: //blue and brown hat
	    case 6375: //white and red hat
	    	return 500;
	    	
	    case 6349: //brown sandals
	    case 6359: //blue sandals
	    case 6369: //blue and brown sandals
	    case 6379: //white and red sandals
	    	return 450;
	    	
	    case 6347: //brown armband
	    case 6357: //blue armband
	    case 6367: //blue and brown armband
	    case 6377: //white and red armband
	    	return 450;
	    	
	    case 6313: //opal machete
	    	return 1000;
	    case 6315: //jade machete
	    	return 1400;
	    case 6317: //red topaz machete
	    	return 2600;
	    	
	    case 6311: //gout tuber
	    case 1613: //red topaz
			return 240;
			
		case 1625: //uncut opal
			return 24;
			
		case 1627: //uncut jade
			return 36;
			
		case 1629: //uncut red topaz
			return 48;
			
		case 1609: //opal
			return 120;
			
		case 1611: //jade
			return 180;
		
	    default:
		    return 9999;
	    }
	}

	public static int getSoulWarsShopItemValue(int id) {
		switch (id) {
		
		case 30379: //movement speed tile 3
				return 2;
		case 30380: //movement speed tile 7
				return 8;
		case 15389:// dungeoneering exp lamp
		case 15390:// hunter exp lamp
			return 1;
			
		case 21474:// vanguard legs
		case 21464:// battle mage legs
		case 21469:// trickster legs
	    case 21473:// vanguard body
	    case 21468:// trickster robe
	    case 21463:// battle mage robe
			return 185;
			
		case 21472:// vanguard helm
		case 21467:// trickster helm
		case 21462:// battle mage helm
			return 170;
			
		case 21475:// vanguard gloves
		case 21476:// vanguard boots
		case 21470:// trickster gloves
		case 21471:// trickster boots
		case 21465:// battle mage gloves
		case 21466:// battle mage boots
			return 125;
		
		 default:
			    return 99999;
		}
	}

	public static int getBarbShopItemValue(int id) {
	    switch (id) {
	    
	    case 10549:
	    case 10550:
	    case 10548:
	    case 10547:
	    	return 800;
	    	
	    case 10551:
	    case 10555:
	    	return 1200;
	    	
	    case 10552:
	    case 10553:
	    	return 250;
	    	
		case 1763:
		case 1765:
		case 1767:
		case 1771:
		case 3188:
			return 4800;
			
		case 15246:
			return 280;
			
		case 30049:
			return 7200;
			
		case 30091:
			return 22000;
			
		case 30092:
			return 29000;
			
		case 21473:// vanguard body
		case 21468:// trickster robe
		case 21463:// battle mage robe
			
		//case 21474:// vanguard legs
		//case 21464:// battle mage legs
		//case 21469:// trickster legs
			return 22000;
		
	    default:
		    return 9999;
	    }
	}

	public static int getAgilityShopItemValue(int id) {
		switch (id) {
		
		case 3033: // agility pot
			return 50;

		case 14218: // energy pot (5)
			return 70;

		case 4447: // antique lamp
			return 1000;

		case 7219: // summer pie
			return 100;
			
		case 1005: // white apron
		case 1757: // brown apron
			return 10_000;
			
		//skirt and woven skirts
		case 5048:
		case 5050:
		case 5052:
		case 5024:
		case 5026:
		case 5028:
			return 15_000;

		case 14936:
		case 14938:
			return 3500;

		case 2997:
			return 5000;

		default:
			return 9999;
		}
	}

	public static int getSurvivalShopItemValue(int id) {
		switch (id) {
	
			case 7946:  //monkfish
			case 3022:  //super energy(1)
			case 853:   //maple shortbow
			case 954:  //rope
			    return 2;
			case 385:   //shark
			case 1333:  //rune scimitar
				return 3;
			case 143:   //prayer potion(1)
			case 3105:  //climbing boots
			case 4097:  //mystic boots
			case 6328:  //snakeskin boots
				return 4;
			case 7458:  //mithril gloves
			case 14225: // stamina flask (1)
				return 5;
			case 1478:  //amulet of accuracy
				return 6;
			case 30102: //adamant arrow pack
			case 30101: //elemental rune pack
			case 1127:  //rune platebody
			case 1079:  //rune platelegs
			case 30132: //3rd age robe top
			case 30133: //3rd age robe
			case 2503:  //black d'hide body
			case 2497:  //black d'hide chaps
				return 8;
			case 30103: //rune arrow pack
			case 30100: //cataclytic rune pack
			case 30104: //ancient magicks tablet
			case 6523:  //toktz-xil-ak
			    return 10;
			case 861:   //magic shortbow
				return 15;
			case 4587:  //dragon scimitar
				return 20;
			case 1215:  //dragon dagger
				return 12;
			case 30124: //abyssal whip
				return 35;
				
			default:
				return 9999;
		}
	}

	static int getCastleWarsItemValue(int id) {
		switch (id) {
	
			case 30379: //movement speed tile 3
				return 4;
		    case 30380: //movement speed tile 7
				return 8;
			case 18739:// castle wars flag cape
				return 50;
			case 18740:// castle wars kills cape
				return 50;
			case 18741:// castle wars hoppyist cape
				return 125;
			case 18742:// castle wars enthusiast cape
				return 250;
			case 18743:// castle wars professional cape
				return 500;
	
			case 4068:// basic sword
			case 4069:// basic platebody
			case 4070:// basic platelegs
			case 4071:// basic helm
			case 4072:// basic shield
				return 12;
	
			case 4503:// detailed sword
			case 4504:// detailed platebody
			case 4505:// detailed platelegs
			case 4506:// detailed helm
			case 4507:// detailed shield
				return 40;
	
			case 4508:// intricate sword
			case 4509:// intricate platebody
			case 4510:// intricate platelegs
			case 4511:// intricate helm
			case 4512:// intricate shield
				return 100;
	
			case 18705:// profound sword
			case 18706:// profound platebody
			case 18707:// profound platelegs
			case 18708:// profound helm
			case 18709:// profound shield\
				return 500;
	
			case 21472:// vanguard helm
			case 21467:// trickster helm
			case 21462:// battle mage helm
				return 170;
				
		    case 21474:// vanguard legs
		    case 21464:// battle mage legs
		    case 21469:// trickster legs
		    case 21473:// vanguard body
		    case 21468:// trickster robe
		    case 21463:// battle mage robe
		    	return 185;
				
			case 21475:// vanguard gloves
			case 21476:// vanguard boots
			case 21470:// trickster gloves
			case 21471:// trickster boots
			case 21465:// battle mage gloves
			case 21466:// battle mage boots
				return 125;
	
			case 15389:// dungeoneering exp lamp
			case 15390:// hunter exp lamp
				return 1;
	
			case 12186:// zamorak pet
			case 12187:// guthix pet
			case 12185:// saradomin pet
				return 600;
	
			case 18747:// faithful shield
				return 80;
	
			case 18744:// guthix halo
			case 18745:// saradomin halo
			case 18746:// zamorak halo
				return 300;
	
			case 10446:// saradomin cloak
			case 10450:// zamorak cloak
				return 10;
	
			case 2528:// exp lamp
				return 3;
	
			default:
				return 9999;
		}
	}

	public static int getDonDiamondShopItemValue(int id) {
		switch (id) {
	
			case 14484:
			case 11694:
			case 19780:
				return 20;
	
			case 13746:
			case 13752:
				return 18;
	
			case 13748:
			case 13750:
				return 22;
	
			case 11730:
			case 11724:
			case 11726:
			case 11728:
			case 11718:
			case 11720:
			case 11722:
				return 17;
	
			case 21787:
			case 21790:
			case 21793:
				return 25;
	
			case 21371:
				return 19;
	
		}
		return 9999;
	}

	public static int getDonGoldShopItemValue(int id) {
		switch (id) {
	
			case 13887:
			case 13893:
			case 13899:
			case 13905:
				return 18;
	
			case 13896:
			case 13884:
			case 13890:
			case 13902:
			case 15486:
				return 14;
	
			case 13876:
			case 13873:
			case 13870:
			case 13864:
			case 13861:
			case 13858:
			case 13867:
				return 8;
	
			case 11730:
			case 11698:
			case 11700:
			case 11696:
				return 25;
	
		}
		return 9999;
	}

	public static int getDonorShopItemValue(int id) {
		switch (id) {
	
		case 30176: //ultra mystery box
			return 15;
		case 30175: //legendary mystery box
			return 10;
		case 30174: //super mystery box
			return 5;
	
		case 19780: // korasi
			return 12;
	
		case 11694: // armadyl godsword
			return 21;
	
		case 14484: // dragon claws
			return 12;
	
		case 20171: // zaryte bow
			return 16;
	
		case 11696: // bgs
		case 11700: // zgs
			return 7;
	
		case 11730: // ss sword
			return 5;
	
		case 11698: // sgs
			return 8;
	
		case 18359: // shield
		case 18361: // shield
		case 18363: // shield
			return 10;
	
		case 18349: // rapier
		case 18351: // longsword
		case 18353: // maul
		case 18355: // staff
		case 18357: // crossbow
			return 18;
	
		case 18335: // arcane stream necklace 15%
		case 11283: // dfs
		case 15486: // staff of light
			return 7;
	
		case 12006: // abyssal tentacle
			return 10;
	
		case 11791: // staff of dead
		case 11785: // armadyl crossbow
			return 16;
	
		case 13740: // divine spirit shield
			return 21;
	
		case 13742: // elysian spirit shield
			return 17;
	
		case 13738: // arcane spirit shield
		case 13744: // spectral spirit shield
			return 11;
	
		case 18334: // arcane pulse necklace 10%
			return 4;
	
		case 18333: // arcane blast necklace 5%
			return 3;
	
		case 20139: // torva platebody
		case 20143: // torva platelegs
		case 20135: // torva full helm
		case 20147:
		case 20151:
		case 20155:
		case 20159:
		case 20163:
		case 20167:
			return 40;
	
		case 11724:
		case 11726:
		case 11718:
		case 11720:
		case 11722:
			return 16;
	
		case 11728:
			return 6;
	
		case 21787:
		case 21790:
		case 21793:
			return 18;
	
		case 4708: // barrows
		case 4710: // barrows
		case 4712: // barrows
		case 4714: // barrows
		case 4716: // barrows
		case 4718: // barrows
		case 4720: // barrows
		case 4722: // barrows
		case 4724: // barrows
		case 4726: // barrows
		case 4728: // barrows
		case 4730: // barrows
		case 4732: // barrows
		case 4734: // barrows
		case 4736: // barrows
		case 4738: // barrows
		case 4745: // barrows
		case 4747: // barrows
		case 4749: // barrows
		case 4751: // barrows
		case 4753: // barrows
		case 4755: // barrows
		case 4757: // barrows
		case 4759: // barrows
			return 3;
	
		case 11846: // ahrims
		case 11852: // karil
		case 11850: // guthan
		case 11854: // torag
		case 11856: // verac
			return 2;
		case 11848: // dharok
			return 4;
	
		case 761: // 2x Skilling Exp Scroll
			return 2;
	
		case 10831: // empty tax bag
		case 4151: // abyssal whip
		case 15300:
			return 1;
	
		case 10832: // light tax bag
			return 30;
		case 10833: // normal taax bag
			return 55;
		case 10834: // hefty tax bag
			return 80;
		case 10835: // bulging tax bag
			return 105;
	
		case 23639: // tokhaar-kal
			return 12;
	
		case 3062: // random box
			return 30;
	
		case 15098: // dice bag
			return 350;
			
		case 12502: // season pass level up 5 lvls scroll
			return 15;
	
		}
		return 9999;
	
	}

	public static int getDonStoneShopItemValue(int id) {
		switch (id) {
	
			case 4151:
				return 2;
	
			case 11235:
			case 4087:
			case 4585:
				return 4;
	
			case 6585:
				return 10;
	
			case 11335:
				return 20;
	
			case 14479:
			case 11283:
				return 25;
	
		}
		return 9999;
	}

	public static int getDungShopItemValue(int id) {
		switch (id) {
	
			case 18349:
			case 18351:
			case 18353:
			case 18355:
			case 18357:
			case 18359:
				return 12000;
	
			case 18361:
			case 18363:
				return 10500;
	
			case 18346:
				return 6500;
	
			case 9952:
				return 6000;
	
			case 18333:
				return 500;
	
			case 18334:
				return 1000;
	
			case 18335:
			case 19669:
				return 7000;
	
			case 18347:
				return 3500;
	
			case 15389:
				return 750;
	
			case 19894:
				return 20000;
			case 19895:
				return 30000;
	
		}
		return 999999;
	}

	public static int getDuoSlayerShopItemValue(int id) {
		switch (id) {
	
			case 14653: // minitrice
			case 14655: // baby kurask
			case 14651: // abyssal minion
				return 20000;
	
			default:
				return 9999;
		}
	
	}

	public static int getLmsShopItemValue(int id) {
	    switch (id) {
	    
	    case 30177: //white skin color
	    case 30178: //blue skin color
	    case 30179: //green skin color
	    	return 17_000;
	    	
	    case 19668: // pet strong taming rope
	    	return 6_400;
	    	
	    case 15360: //pet double exp scroll
	    	return 1_250;
	    	
	    case 2528: // "Lamp"
	    	return 1_800;
	    	
	    case 15389: // Experience lamp
	    case 15390: // Hunter lamp
	    	return 800;
	    	
	    //Untradeables
	    	
	    case 13685:
	    case 13686:
	    case 13687:
	    case 13688:
	    case 13689:
	    case 13690:
	    	return 37_500;
	    	
	    case 13691:
	    case 13692:
	    case 13693:
	    case 13694:
	    case 13695:
	    case 13696:
	    	return 38_500;
	    	
	    case 13697:
	    case 13698:
	    case 13699:
	    case 13700:
	    case 13701:
	    case 13702:
	    	return 38_000;
	    	
	    	//flasks
	    case 14208:
	    case 14218:
	    case 14228:
	    case 14238:
	    case 14248:
	    case 14258:
	    case 14268:
	    	return 15;
	    	
	    case 30093:
	    	return 10;
		
	    default:
		    return 9999;
	    }
	}
	
	public static int getDominionTokensShopItemValue(int id) {
		 switch (id) {
		    case 20841: //power up
		    	return 10_000;
		    case 20667:
		    	return 5_000;
		    case 385:
		    	return 100;
		    case 2440:
		    case 2436:
		    case 2442:
		    case 2444:
		    case 3040:
		    	return 250;
		    case 15332:
		    	return 600;
		    case 20706:
		    	return 1000;
		    default:
		    	return 9999;
		 }
	 }

	public static int getTaskShopItemValue(int id) {
		 switch (id) {
		    case 30210: //Bloodhound
		    	return 10_000;
		    case 30047: //rune pouch
		    	return 500;
		    case 30300: //Red
		    case 30301: //Purple
		    case 30302: //Black skin colors
		    	return 2_500;
		    case 30193: //nature ring
		    	return 7_000;
		    case 30192: //gp ring
		    	return 10_000;
		    case 21520: //tiger shark
		    	return 5;
		    case 20429: //fury shark
		    	return 25;
		    case 19668: //strong taming rope
		    	return 700;
		    case 21821: //pet stat reset
		    	return 150;
		    case 21403: //pet boost
		    	return 50;
		    case 15360: //pet double xp
		    	return 300;
		    case 30267: //bounty teleport
		    	return 4_500;
		    //Outfit pieces
		    case 21446:
		    case 21447:
		    case 21448:
		    case 21449:
		    case 21450:
		    case 10941:
		    case 10939:
		    case 10940:
		    case 10933:
		    case 30303:
		    case 30304:
		    case 30305:
		    case 30306:
		    case 30308:
		    case 30309:
		    case 30310:
		    case 30311:
		    case 30318:
		    case 30319:
		    case 30320:
		    case 30321:
		    	return 400;
		    case 120716:
		    	return 600;
		    default:
		    	return 9999;
		 }
	 }

	public static int getIronVoteShopItemValue(int id) {
		switch (id) {
	
			case 18338:
				return 14;
	
			case 10499:
				return 2;
	
			case 3751:
			case 3749:
			case 2528:
				return 6;
	
			case 10828:
				return 10;
	
			case 5574:
			case 5575:
			case 5576:
				return 5;
	
			case 9721:
				return 1;
				
			case 607: // rare drop boost
				return 5;
				
			case 626: // pink boots
			case 636: // pink robe top
			case 646: // pink bottoms
			case 656: // pink hat
				return 40;
				
			case 30378: // reset wildy kill count
			case 30379: // speed scroll
			case 30380: // flash scroll
				return getVoteShopItemValue(id);
	
		}
		return getVoteShopItemValue(id);
	}

	public static int getItemShopValue(int itemId) {
		ItemDefinition def = ItemDefinition.forId(itemId);
		if (def != null)
			return def.getShopValue();
		return 0;
	}

	public static int getLoyaltyShopItemValue(int id) {
		switch (id) {
	
			// supply shop
			case 537: // dragon bones
				return 50;
			case 2510: // black dragon leather
				return 65;
			case 2354: // steel bars
				return 20;
			case 5973: // papaya fruit
				return 200;
			case 12539: // grenwall spikes
				return 200;
			case 9594: // ground mud runes
				return 200;
			case 220: // grimy torstol
				return 650;
			case 212: // grimy avantoe
				return 200;
			case 218: // grimy dwarf weed
				return 200;
	
			// case 15021:
			case 15021:
			case 15022:
			case 15023:
			case 15024:
			case 15025:
			case 15026:
				return 1800;
	
			case 15027:
			case 15028:
			case 15029:
			case 15030:
			case 15031:
			case 15032:
				return 3250;
	
			case 15033:
			case 15034:
			case 15035:
			case 15036:
			case 15037:
			case 15038:
				return 5500;
	
			case 15039:
			case 15040:
			case 15041:
			case 15042:
			case 15043:
			case 15044:
				return 8000;
	
			default:
				return 9999;
		}
	}

	public static int getPcShopItemValue(int id) {
		switch (id) {
	
			case 14598:
				return 7;
			case 19712: // void deflector
			case 8839:
			case 8840:
				return 100;
	
			case 8842:
				return 30;
	
			case 11663:
			case 11664:
			case 11665:
				return 70;
	
			case 19785:
			case 19786:
			case 19787:
			case 19788:
			case 19789:
			case 19790:
				return 250;
	
			case 8841:
				return 400;
	
			case 15602:
			case 15600:
			case 15604:
			case 15608:
			case 15606:
			case 15610:
			case 15612:
			case 15614:
			case 15616:
			case 15618:
			case 15620:
			case 15622:
				return 50;
	
			case 1763:
			case 1765:
			case 1767:
			case 1771:
			case 3188:
				return 2000;
	
			case 21475:// vanguard gloves
			case 21476:// vanguard boots
			case 21470:// trickster gloves
			case 21471:// trickster boots
			case 21465:// battle mage gloves
			case 21466:// battle mage boots
				return 4450;
	
			default:
				return 9999;
		}
	
	}

	public static int getPenguinShopItemValue(int id) {
		switch (id) {
	
			case 2528:
				return 5;
			case 10832:
				return 21;
			case 12483:
				return 23;
	
			default:
				return 9999;
		}
	}

	public static int getPKShopItemValue(int id) {
		switch (id) {
	
		// pk store
		case 13896://statius helm
			return 400;//950 before
		case 13884://statius platebody
			return 450;//1200 before
		case 13890://statius legs
		case 13902://statius warhammer
			return 700;//1000 before
		case 30067:
		case 30055://magic shortbow upgrade
			return 600;//1200 before
		case 15347://ardougne cloak 2
		case 4151: //abyssal whip
		case 11235: //dark bow
		case 6585: //amulet of fury
		case 4153: //granite maul
			return 160;
		case 17273: // flameburst defender
		case 17275: // frostbite dagger
			return 1000;
		case 13887://vesta chainbody
			return 650;//1650 before
		case 13893://vesta chainskirt
			return 900;//1550 before
		case 13899://vesta longsword
			return 950;//1800 before
		case 13905://vesta spear
			return 800;//1700 before
		case 15349://ardougne cloak 3
		case 6570: //firecape
		case 6889: //mage's book
		case 6914: //master wand
			return 300;//3000 before
		case 18334: // arcane blast necklace 10%
			return 1000;
		case 13876:// morrigan coif
		case 13864:// zuriel hood
			return 650;
		case 13873:// morrigan chaps
		case 13861:// zuriel bottom
			return 1000;
		case 13870:
		case 13858:
		case 13867:
		case 21371:
			return 950;
		case 18333: // arcane pulse necklace 5%
		case 15345:
			return 1000;
		case 13883:
		case 13879:
			return 10;
		case 13734:
			return 550;
		case 4447:
			return 500;
		case 19780: //korasi
		case 30047: //rune pouch
		case 15241: //hand cannon
			return 1500;
		case 20072: //dragon defender
		case 1033:
		case 1035:
			return 250;
		case 4712:
		case 4714:
			return 400;
		case 23639: //tokhaar-kal cape
			return 1000;
		case 4736:
		case 4738:
		case 15126: //ranging amulet
		case 30266:
		case 30265:
		case 30264:
			return 3_500;
		case 30267:
			return 5_000;
		case 14484: //dragon claws
		    return 5000;
		case 11694: //armadyl godsword
			return 6000;
		case 30093: //dragon thrownaxes
			return 100;
		case 10551: //fighter torso
			return 300;
		case 30090: //elder maul
			return 6000;
		case 30095: //Dexterous prayer scroll
		case 30096: //Arcane prayer scroll
		case 30097: //Torn prayer scroll
			return 5000;
		case 18831: //frost dragon bones
			return 30;
		case 9740: //combat potions
			return 35;
		case 19669: //ring of vigour
			return 1500;
		case 21805: //wilderness key
			return 6000;
		case 15332: //overloads
			return 100;
		case 15300: //super restore
			return 30;
		case 4278: // pk points token
			return 100;
		case 30378: // reset wildy kill count
			return 1000;
			
		default:
			return 9999;
		}
	
	}

	public static int getSlayerShopItemValue(int id) {
		switch (id) {
	
			case 30081:
			case 30082:
			case 30083:
				return 1000;
			case 405: // 2,5 mill casket
				return 25;
			case 9244:
			case 9242:
			case 11212:
			case 15273:
				return 1;
			case 7462:
			case 8850:
			case 6543:
				return 50;
			case 10548:
			case 10550:
			case 10549:
			case 10547:
				return 60;
			case 11732:
			case 8921:
				return 20;
			case 6916:
			case 6918:
			case 6920:
			case 6922:
			case 6924:
			case 6889:
			case 7509:
			case 7510:
			case 5312:// oak seed
				return 20;
			case 6585:
				return 70;
			case 4151:
			case 2572:
			case 5314: // maple seed
				return 40;
			case 7053: // lit bug lantern
				return 10;
			case 11235:
			case 13263:
			case 18337:
			case 5315: // yew seed
			case 20671: // Brackish blade
				return 60;
			case 14652:
			case 14654: // basilisk pet
				return 5000;
			case 5313: // willow seed
				return 30;
			case 5316:
			case 10551:
				return 85;
			case 30369: // vial of blood
				return 1;
	
			default:
				return 9999;
		}
	
	}

	/**
	 * Vote shop item value with tickets
	 *
	 * @param id
	 *            - Item ID
	 * @return returns price of item with vote tickets
	 */
	public static int getVoteShopItemValue(int id) {
		switch (id) {
	
		case 12502: // season pass level up 5 lvls scroll
			return 200;
		
		case 607: // rare drop boost
			return 5;
		case 20120:
			return 5;
		case 1052:
		case 608: //pure pk boost scroll
		case 2403: //main pk boost scroll
			return 3;
		case 20714:
			return 400;
		case 21596:
		case 21597:
		case 21598:
		case 21599:
			return 250;
		case 13316:
		case 13317:
			return 150;
		case 19780: // korasi
		return 300;
		case 6199: // mystery box
			return 4;
		case 6542: // pk package
			return 2;
		case 3062: // coin box
			return 1;
		case 11694: // ags
		case 14484: // claws
		case 11730: // sarasowrd
			return 100;
		case 1037: // bunny ears
		case 9470: // scarf
		case 6818: // bowsword
		case 15486: // sol
			return 110;
		case 761: // double exp
		case 15501: // skilling package
			return 4;
		case 11235: // dbow
		case 11732: // dboots
			return 15;
		case 11283: // dfs
			return 90;
		case 6585: // fury
		case 20072: // d defender
			return 10;
		case 6570: // firecape
			return 6;
		case 10831: // empty tax bag
		case 4151: // whip
		case 15332:
		case 299: // mithril seed
			return 5;
		case 19290:
		case 19287:
		case 19284:
		case 19281:
			return 12;
		case 11789: // grim reaper hood
			return 60;
		case 21369:
		case 13101:
		case 4566: // rubber chicken
			return 25;
		case 13672:
		case 13673:
		case 13674:
		case 13675:
			return 40;
	
		case 6950: // vengeance orb
		case 405: // 2,5 mill casket
			return 5;
	
		case 4: // ammo mould
		case 6: // cannon base
		case 8: // cannon stand
		case 10: // cannon barrel
		case 12: // cannon furnace
			return 1;
			
		case 626: // pink boots
		case 636: // pink robe top
		case 646: // pink bottoms
		case 656: // pink hat
			return 40;
			
		case 30378: // reset wildy kill count
			return 100;
			
		case 30379: // speed scroll
			return 10;
			
		case 30380: // flash scroll
			return 80;
			
		case 24324: // queen's guard
		case 24325: // queen's guard
		case 24326: // queen's guard
		case 24327: // queen's guard
		case 24328: // queen's guard
		case 15503: // royal shirt
		case 15505: // royal leggings
		case 15507: // royal sceptre
		case 15509: // royal crown
		case 15511: // royal amulet
			return 85;
	
		}
		return 9999;
	}

	public static int getSurvivalShopSellValue(int id) {
		switch (id) {
	
			case 7946:  //monkfish
			case 143:   //prayer potion(1)
			case 3022:  //super energy(1)
			case 853:   //maple shortbow
			case 954:   //rope
			case 6526:  //toktz-mej-tal
			    return 1;
			case 385:   //shark
			case 1333:  //rune scimitar
			case 1319:  //rune 2h sword
			case 1373:  //rune battleaxe
			case 1478:  //amulet of accuracy
			case 7458:  //mithril gloves
			case 3105:  //climbing boots
			case 4097:  //mystic boots
			case 6328:  //snakeskin boots
			case 141:   //prayer potion(2)
			case 3020:  //super energy(2)
			case 1161:  //adamant full helm
			case 1123:  //adamant platebody
			case 1073:  //adamant platelegs
			case 2499:  //blue d'hide body
			case 2493:  //blue d'hide chaps
			case 6908:  //beginner wand
			case 14225: // stamina flask (1)
				return 2;
			case 30100: //cataclytic rune pack
			case 30101: //elemental rune pack
			case 6523:  //toktz-xil-ak
			case 139:   //prayer potion(3)
			case 3018:  //super energy(3)
			case 857:   //yew shortbow
			case 2495:  //red d'hide chaps
			case 2501:  //red d'hide body
			case 6910:  //apprentice wand
			case 4091:  //mystic robe top
			case 4093:  //mystic robe bottom
				return 3;
			case 30102: //adamant arrow pack
			case 30104: //ancient magicks tablet
			case 4587:  //dragon scimitar
			case 1215:  //dragon dagger
			case 4153:  //granite maul
			case 1127:  //rune platebody
			case 1079:  //rune platelegs
			case 2503:  //black d'hide body
			case 2497:  //black d'hide chaps
			case 2434:  //prayer potion(4)
			case 3016:  //super energy(4)
			case 9672:  //proselyte sallet
			case 9674:  //proselyte hauberk
			case 9676:  //proselyte cuisse
			case 3385:  //splitbark helm
			case 6916:  //infinity top
			case 6924:  //infinity bottom
			case 1704:  //amulet of glory
			case 7460:  //rune gloves
				return 4;
			case 30103: //rune arrow pack
			case 861:   //magic shortbow
			case 30132: //3rd age robe top
			case 30133: //3rd age robe
			case 30123:  //dragon 2h sword
			case 3753:  //warrior helm
			case 3749:  //archer helm
			case 3755:  //farseer helm
			case 30131:  //master wand
			    return 5;
			case 30129: //dragon platelegs
			case 30128: //dragon chainbody
			case 4675: //ancient staff
				return 6;
			case 30134: //ahrim's robetop
			case 30135: //ahrim's robeskirt
				return 7;
			case 30124:  //abyssal whip
			case 30130: //dark bow
				return 10;
			case 30121: //armadyl godsword
			case 30125: //dragon warhammer
			case 30122: //dragon claws
			case 30127: //elder maul
				return 15;
				
			default:
				return 0;
		}
	}

	public static int getGabootyShopSellValue(int id) {
		switch (id) {
	
		case 6311: // gout tuber
		case 1613: // red topaz
			return 120;
	
		case 1625: // uncut opal
			return 12;
	
		case 1627: // uncut jade
			return 18;
	
		case 1629: // uncut red topaz
			return 18;
	
		case 1609: // opal
			return 60;
	
		case 1611: // jade
			return 90;
	
		default:
			return 0;
		}
	}
	
	public static int getChristmasShopItemValue(int id) {
		switch (id) {
	
			case 30337: // santa mask
			case 30338: // santa jacket
			case 30339: // santa legs
				return 1000;
			case 30340: // santa gloves
			case 30341: // santa boots
				return 500;
			case 11955: // hats
			case 11956:
			case 11957:
			case 11958:
			case 11959:
				return 500;
	
			default:
				return 9999;
		}
	
	}

}
