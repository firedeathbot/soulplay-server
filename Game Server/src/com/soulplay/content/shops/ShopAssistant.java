package com.soulplay.content.shops;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.minigames.ddmtournament.SpawnTable;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.untradeables.UntradeableManager;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.ShopHandler;
import com.soulplay.util.Misc;

public class ShopAssistant {

	private Client c;

	public ShopAssistant(Client client) {
		this.c = client;
	}
	
	public boolean addShopItem(int itemID, int amount) {
		boolean Added = false;
		if (amount <= 0) {
			return false;
		}
		// if (Item.itemIsNote[itemID] == true) {
		// ItemDefinition item = ItemDefinition.forId(itemID);
		// if(ItemDefinition.forId(itemID-1).getName().equals(item.getName()))
		// itemID--;
		// // itemID = c.getItems().getUnnotedItem(itemID);
		// }
		for (int i = 0; i < ShopHandler.ShopItems.length; i++) {
			if ((ShopHandler.ShopItems[c.myShopId][i] - 1) == itemID) {
				ShopHandler.ShopItemsN[c.myShopId][i] += amount;
				Added = true;
			}
		}
		if (Added == false) {
			for (int i = 0; i < ShopHandler.ShopItems.length; i++) {
				if (ShopHandler.ShopItems[c.myShopId][i] == 0) {
					ShopHandler.ShopItems[c.myShopId][i] = (itemID + 1);
					ShopHandler.ShopItemsN[c.myShopId][i] = amount;
					ShopHandler.ShopItemsDelay[c.myShopId][i] = 0;
					break;
				}
			}
		}
		return true;
	}
	
	private static long getTotalPrice(long price, int amount) {
		try {
			return Math.multiplyExact(price, amount);
		} catch (ArithmeticException e) {
			return -1;
		}
	}

	/**
	 * @param itemID
	 * @param fromSlot
	 * @param amount
	 * @return
	 */
	public boolean buyFromPOS(int itemID, int fromSlot, int amount) {
		if (fromSlot < 0) {
			return false;
		}
		if (!c.inMarket()) {
			return false;
		}
		if (c.myShopClient == null || !c.myShopClient.correctlyInitialized
				|| !c.correctlyInitialized || c.myShopClient.inVerificationState
				|| c.inVerificationState) {
			return false;
		}
		if (c.myShopClient.myShopClient != null
				&& c.myShopClient == c.myShopClient.myShopClient
				&& c != c.myShopClient) { // if the shop owner did ::myshop
			c.myShopClient.sendMessage(
					"A player is trying to buy from your shop but you are still setting up shop.");
			c.myShopClient.sendMessage(
					"Close your shop to allow players to buy from your shop.");
			c.sendMessage(
					"Please wait while the shop keeper is setting up his shop.");
			return false;
		}
		// if (c !=null){
		// c.sendMessage("disabled.");
		// return false;
		// }

		if (c.myShopClient.disconnected) {
			c.getPA().closeAllWindows();
			return false;
		}
		
		final boolean takingOutOwnItem = c.mySQLIndex == c.myShopClient.mySQLIndex;
		

		if (amount < 1) {
			if (!takingOutOwnItem)
				return true;
			c.getDialogueBuilder().sendEnterAmount(() -> {
				if (c.myShopClient == null)
					return;
				long newPrice = c.enterAmount;
				if (c.enterAmount < 1) {
					c.sendMessage("Your price must be greater than 0.");
				} else {
					long oldPrice = c.myShopClient.playerShopItemsPrice[fromSlot];
					c.myShopClient.playerShopItemsPrice[fromSlot] = newPrice;
					String itemName = ItemConstants.getItemName(c.myShopClient.playerShopItems[fromSlot]);
					c.sendMessage("You have changed the price of "+ itemName +" from "+ Misc.formatNumbersWithCommas(oldPrice) + " to " + Misc.formatNumbersWithCommas(newPrice));
				}
			}).execute(false);
			c.getPacketSender().sendInputDialogState(PlayerConstants.SET_SELL_PRICE_STATE);
			return true;
		}

		int buyerFreeInvSlots = c.getItems().freeSlots();

		// buying from other people's shop
		if (c.myShopId == 7390
				&& !c.getName().equals(c.myShopClient.getName())) {
			if (c.myShopClient.disconnected || c.myShopClient.forceDisconnect) {
				c.getPA().removeAllWindows();
				return false;
			}
			if (fromSlot >= c.myShopClient.playerShopItems.length) {
				return false;
			}
			if (c.myShopClient.playerShopItems[fromSlot] != itemID) {
				return false;
			}
			int bought = 0;
			long price = c.myShopClient.playerShopItemsPrice[fromSlot];
			if (amount > c.myShopClient.playerShopItemsN[fromSlot]) {
				amount = c.myShopClient.playerShopItemsN[fromSlot];
			}

			int notedId = c.myShopClient.playerShopItems[fromSlot];
			ItemDef def = ItemDef.forID(notedId);
			if (def.itemCanBeNoted()) {
				notedId = def.getNoteId();
			}

			if (!ItemProjectInsanity.itemIsStackable(notedId) && buyerFreeInvSlots == 0) {
				c.sendMessage("You do not have enought space.");
				return false;
			} else if (ItemProjectInsanity.itemIsStackable(notedId) && buyerFreeInvSlots == 0
					&& !c.getItems().playerHasItem(notedId)) {
				c.sendMessage(
						"You do not have enought space in your Inventory.");
				return false;
			}
			if (!ItemProjectInsanity.itemIsStackable(notedId)) {
				if (amount > buyerFreeInvSlots) {
					amount = buyerFreeInvSlots;
				}
			}
			long checkAmount = getTotalPrice(c.myShopClient.playerShopItemsPrice[fromSlot], amount);
			
			if (checkAmount == -1) {
				c.sendMessage("Wrong.");
				return false;
			}
			
			boolean canBuyWithGP = true;
			if (checkAmount > Integer.MAX_VALUE) {
				// c.sendMessage("You do not have enough money to buy that
				// amount.");
				// return false;
				canBuyWithGP = false;
			}
			if (canBuyWithGP && c.getItems().playerHasItem(995,
					(int) c.myShopClient.playerShopItemsPrice[fromSlot]
							* amount)) {
				if (c.getItems().addItem(
						notedId/* c.myShopClient.playerShopItems[fromSlot] */,
						amount)) { // retarded pi code for (int x = 0; x <
									// amount; x++) {
					c.getItems().deleteItem2(995,
							(int) c.myShopClient.playerShopItemsPrice[fromSlot]
									* amount);
					c.myShopClient.playerShopItemsN[fromSlot] -= amount;
					if (c.myShopClient.getDonatedTotalAmount() > 49
							&& c.myShopClient.getDonatedTotalAmount() < 1000) {
						c.myShopClient.playerCollect += c.myShopClient.playerShopItemsPrice[fromSlot]
								* amount * 0.985;
					} else if (c.myShopClient.getDonatedTotalAmount() > 999) {
						c.myShopClient.playerCollect += c.myShopClient.playerShopItemsPrice[fromSlot]
								* amount * 0.99;
					} else if (c.myShopClient.getDonatedTotalAmount() < 50) {
						c.myShopClient.playerCollect += c.myShopClient.playerShopItemsPrice[fromSlot]
								* amount * 0.98;
					}
					if (c.myShopClient.playerShopItemsN[fromSlot] == 0) {
						c.myShopClient.playerShopItems[fromSlot] = 0;
						c.myShopClient.playerShopItemsPrice[fromSlot] = 0;
					}
					bought += amount;
				} else {
					c.sendMessage("You do not have enough space.");
					return false;
				}
			} else if ((!canBuyWithGP || !c.getItems().playerHasItem(995))
					&& c.getMoneyPouch() >= checkAmount) {
				if (c.getItems().addItem(
						notedId/* c.myShopClient.playerShopItems[fromSlot] */,
						amount)) { // retarded pi code for (int x = 0; x <
									// amount; x++) {
					MoneyPouch.removeFromMoneyPouch(c, checkAmount);

					c.myShopClient.playerShopItemsN[fromSlot] -= amount;
					if (c.myShopClient.getDonatedTotalAmount() > 49
							&& c.myShopClient.getDonatedTotalAmount() < 1000) {
						c.myShopClient.playerCollect += c.myShopClient.playerShopItemsPrice[fromSlot]
								* amount * 0.985;
					} else if (c.myShopClient.getDonatedTotalAmount() > 999) {
						c.myShopClient.playerCollect += c.myShopClient.playerShopItemsPrice[fromSlot]
								* amount * 0.99;
					} else if (c.myShopClient.getDonatedTotalAmount() < 50) {
						c.myShopClient.playerCollect += c.myShopClient.playerShopItemsPrice[fromSlot]
								* amount * 0.98;
					}
					if (c.myShopClient.playerShopItemsN[fromSlot] == 0) {
						c.myShopClient.playerShopItems[fromSlot] = 0;
						c.myShopClient.playerShopItemsPrice[fromSlot] = 0;
					}
					bought += amount;
				} else {
					c.sendMessage("You do not have enough space.");
					return false;
				}

			} else {
				c.sendMessage(
						"You do not have enough money to buy that amount.");
				return false;
			}
			if (bought > 0) {
				PlayerOwnedShops.resetShop(c, c.myShopClient);
				c.getItems().resetItems(3823);;
				c.sendMessage("You just bought " + bought + " "
						+ ItemConstants.getItemName(itemID) + " for "
						+ (bought * price) + " Gold.");
				c.myShopClient.sendMessage(c.getNameSmartUp() + " has bought " + bought
						+ " " + ItemConstants.getItemName(itemID)
						+ " from you for " + (bought * price) + " GP!");
				if (WorldType.equalsType(WorldType.SPAWN)) {
					c.myShopClient.sendMessage(
							"You now have " + Misc.formatNumbersWithCommas(c.myShopClient.playerCollect)
									+ " Blood money to collect (::collect)");
				} else {
					c.myShopClient.sendMessage(
							"You now have " + Misc.formatNumbersWithCommas(c.myShopClient.playerCollect)
									+ " coins to collect (::collect)");
				}
				PlayerOwnedShops.fixShop(c.myShopClient);
				updatePlayerShop();
				PlayerOwnedShops.resetShop(c, c.myShopClient);
				PlayerOwnedShops.updatePlayerOwnedShop(c.myShopClient);
				if (DBConfig.LOG_PSHOP_BUY) {
					Server.getLogWriter()
							.addToBuyPOSList(new BuyPOS(c.getName(),
									c.mySQLIndex, c.myShopClient.getName(),
									c.myShopClient.mySQLIndex,
									new GameItem(itemID, bought),
									(bought * price), c.myShopId));
				}
			}
			return false;
		}
		// buying from own shop - taking out my own items from my own shop
		else if (c.myShopId == 7390 && takingOutOwnItem) {
			if (amount > c.myShopClient.playerShopItemsN[fromSlot]) {
				amount = c.myShopClient.playerShopItemsN[fromSlot];
			}

			int notedId = c.myShopClient.playerShopItems[fromSlot];
			ItemDef def = ItemDef.forID(notedId);
			if (def.itemCanBeNoted()) {
				notedId = def.getNoteId();
			}
			if (buyerFreeInvSlots == 0 && !ItemProjectInsanity.itemIsStackable(notedId)) {
				c.sendMessage("You do not have enought space.");
				return false;
			} else if (buyerFreeInvSlots == 0 && ItemProjectInsanity.itemIsStackable(notedId)
					&& !c.getItems().playerHasItem(notedId)) {
				c.sendMessage(
						"You do not have enought space in your Inventory.");
				return false;
			}
			if (!ItemProjectInsanity.itemIsStackable(notedId)) {
				if (amount > buyerFreeInvSlots) {
					amount = buyerFreeInvSlots;
				}
			}
			if (c.getItems().addItem(
					notedId/* c.myShopClient.playerShopItems[fromSlot] */,
					amount)) {

				c.myShopClient.playerShopItemsN[fromSlot] -= amount;
				if (c.myShopClient.playerShopItemsN[fromSlot] == 0) {
					c.myShopClient.playerShopItems[fromSlot] = 0;
					c.myShopClient.playerShopItemsPrice[fromSlot] = 0;
					PlayerOwnedShops.fixShop(c);
					updatePlayerShop();
					PlayerOwnedShops.resetShop(c, c.myShopClient);
					PlayerOwnedShops.updatePlayerOwnedShop(c.myShopClient);
				}
			} else {
				c.sendMessage("Not enought space.");
				return false;
			}
			PlayerOwnedShops.resetShop(c, c.myShopClient);
			PlayerOwnedShops.updatePlayerOwnedShop(c.myShopClient);
			c.getItems().resetItems(3823);
			return false;
		} else if (c.myShopId == 7390) {
			return false;
		}
		return true;
	}

	/**
	 * buy item from shop (Shop Price)
	 **/

	public void buyFromShopPrice(int removeId, int removeSlot) {
		if (removeSlot < 0 || removeSlot > ShopHandler.getMaxShopItems(c)) {
			return;
		}

		if (c.myShopId == UntradeableManager.SHOP_ID) {
			String itemName = ItemConstants.getItemName(removeId);

			int bloodPrice = UntradeableManager.price(removeId) / 20000;
			int price = UntradeableManager.price(removeId);
			if (WorldType.equalsType(WorldType.SPAWN)) {
				int amountToServer = (int) Math.floor(price * 0.25 / 20000);
				int amountOnPlayer = (int) Math.ceil(price * 0.75 / 20000);
				c.sendMessage(String.format(
						"%s will cost you %d blood money (%d to server, %d to player).",
						itemName, bloodPrice, amountToServer, amountOnPlayer));
				return;
			} else {
				int amountToServer = (int) Math.floor(price * 0.25);
				int amountOnPlayer = (int) Math.ceil(price * 0.75);
				c.sendMessage(String.format(
						"%s will cost you %d coins (%d to server, %d to player).",
						itemName, price, amountToServer, amountOnPlayer));
				return;
			}
		}

		if (c.myShopId == 7390 && c.myShopClient != null
				&& c.myShopClient != c) {
			if (c.myShopClient.disconnected || c.myShopClient.forceDisconnect) {
				c.getPA().removeAllWindows();
				return;
			}
			if (c.myShopClient.myShopClient != null
					&& c.myShopClient == c.myShopClient.myShopClient) { // if
																		// the
																		// shop
																		// owner
																		// did
																		// ::myshop
				c.myShopClient.sendMessage(
						"A player is trying to buy from your shop but you are still setting up shop.");
				c.myShopClient.sendMessage(
						"Close your shop to allow players to buy from your shop.");
				c.sendMessage(
						"Please wait while the shop keeper is setting up his shop.");
				return;
			}
			long value = c.myShopClient.playerShopItemsPrice[removeSlot];
			String add = "";
			if (value >= 1000 && value < 1000000) {
				add = " (" + (value / 1000) + "K)";
			} else if (value >= 1000000) {
				add = " (" + (value / 1000000) + "M)";
			}
			if (WorldType.equalsType(WorldType.SPAWN)) {
				c.sendMessage(ItemConstants.getItemName(removeId)
						+ ": currently cost " + Misc.format(value)
						+ " Blood Money." + add);
			} else {
				c.sendMessage(ItemConstants.getItemName(removeId)
						+ ": currently cost " + Misc.format(value) + " coins."
						+ add);
			}
			return;
		} else if (c.myShopId == 7390 && c.myShopClient != null
				&& c.myShopClient == c) {
			if (WorldType.equalsType(WorldType.SPAWN)) {
				c.sendMessage(ItemConstants.getItemName(removeId)
						+ ": currently costs "
						+ Misc.format(c.playerShopItemsPrice[removeSlot])
						+ " Blood Money.");
			} else {
				c.sendMessage(ItemConstants.getItemName(removeId)
						+ ": currently costs "
						+ Misc.format(c.playerShopItemsPrice[removeSlot])
						+ " coins.");
			}
			return;
		}

		if (c.myShopId == 7390) {
			c.sendMessage("Error 7985");
			return;
		}

		int ShopValue = (int) Math
				.floor(getItemShopValue(removeId, 0, removeSlot));
		if (c.myShopId == 14) { // retarded skill cape shop cheapfix
			ShopValue = 99000;
		}
		String ShopAdd = "";
		if (c.myShopId == 80) {
			c.sendMessage(
					ItemConstants.getItemName(removeId) + ": currently costs "
							+ ShopPrice.getSeasonalShopItemValue(c, removeId) + " tokens.");
			return;
		}
		if (c.myShopId == 8 || c.myShopId == 82) {
			c.sendMessage(
					ItemConstants.getItemName(removeId) + ": currently costs "
							+ ShopPrice.getPKShopItemValue(removeId) + " pk points.");
			return;
		}
		if (c.myShopId == 10 || c.myShopId == 105) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getSlayerShopItemValue(removeId)
					+ " slayer points.");
			return;
		}
		if (c.myShopId == 49) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getDuoSlayerShopItemValue(removeId)
					+ " duo points.");
			return;
		}
		if (c.myShopId == 9 || c.myShopId == 12 || c.myShopId == 13
				|| c.myShopId == 15) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getDonorShopItemValue(removeId)
					+ " donator points.");
			return;
		}
		// if (c.myShopId == 15) {
		// c.sendMessage("This item current costs "
		// + c.getItems().getUntradePrice(removeId) + " coins.");
		// return;
		// }
		if (c.myShopId == 18) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getPcShopItemValue(removeId)
					+ " Pest Control points.");
			return;
		}
		if (c.myShopId == 25 || c.myShopId == 84) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getVoteShopItemValue(removeId)
					+ " Vote Tickets.");
			return;
		}
		if (c.myShopId == 102) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getIronVoteShopItemValue(removeId)
					+ " Vote Tickets.");
			return;
		}
		if (c.myShopId == 33) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getDonStoneShopItemValue(removeId)
					+ " Stone statuettes.");
			return;
		}
		if (c.myShopId == 34) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getDonGoldShopItemValue(removeId)
					+ " Golden statuettes.");
			return;
		}
		if (c.myShopId == 35) {
			c.sendMessage(
					ItemConstants.getItemName(removeId) + ": currently costs "
							+ ShopPrice.getDonDiamondShopItemValue(removeId)
							+ " Jewelled diamond statuettes");
			return;
		}
		if (c.myShopId == 36 || c.myShopId == 46) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getLoyaltyShopItemValue(removeId)
					+ " Loyalty points.");
			return;
		}
		if (c.myShopId == 38) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getPenguinShopItemValue(removeId)
					+ " Penguin points.");
			return;
		}
		if (c.myShopId == 41) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getDungShopItemValue(removeId)
					+ " Dungeoneering Tokens.");
			return;
		}
		if (c.myShopId == 43) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getAgilityShopItemValue(removeId)
					+ " Agility Tokens.");
			return;
		}
		if (c.myShopId == 45 || c.myShopId == 50) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getCastleWarsItemValue(removeId)
					+ " castle wars tickets.");
			return;
		}
		if (c.myShopId == 52) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getBarbShopItemValue(removeId)
					+ " honour points.");
			return;
		}
		if (c.myShopId == 53) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getSoulWarsShopItemValue(removeId)
					+ " Zeals.");
			return;
		}
		if (c.myShopId == 60) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getSurvivalShopItemValue(removeId)
					+ " Survival tokens.");
			return;
		}
		if (c.myShopId == 61) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getGabootyShopItemValue(removeId)
					+ " Trading sticks.");
			return;
		}
		if (c.myShopId == 65 || c.myShopId == 66) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getLmsShopItemValue(removeId)
					+ " LMS Points.");
			return;
		}
		if (c.myShopId == 67) {
			c.sendMessage(ItemDef.forID(removeId).getName()
					+ " currently costs " + Misc.formatNumbersWithCommas(ShopPrice.getTaskShopItemValue(removeId))
					+ " Task Points.");
			return;
		}
		if (c.myShopId == 76) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopPrice.getChristmasShopItemValue(removeId)
					+ " Christmas points.");
			return;
		}
		if (c.myShopId == 77) {
			c.sendMessage(ItemDef.forID(removeId).getName()
					+ " currently costs " + Misc.formatNumbersWithCommas(ShopPrice.getDominionTokensShopItemValue(removeId))
					+ " Dominion tokens.");
			return;
		}
		if (ShopValue >= 1000 && ShopValue < 1000000) {
			ShopAdd = " (" + (ShopValue / 1000) + "K)";
		} else if (ShopValue >= 1000000) {
			ShopAdd = " (" + (ShopValue / 1000000) + " million)";
		}
		if (WorldType.equalsType(WorldType.SPAWN)) {
			c.sendMessage(
					ItemConstants.getItemName(removeId) + ": currently costs "
							+ ShopValue + " Blood money" + ShopAdd);
		} else {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": currently costs " + ShopValue + " coins" + ShopAdd);
		}
	}

	public boolean buyItem(int itemID, int fromSlot, int amount) { // TODO:
																	// check for
																	// any
																	// exploits
																	// here
		
		if (c.myShopId == SpawnTable.SHOP_ID) {
			SpawnTable.spawnItemFromSlot(c, fromSlot, amount);
			return true;
		}
		
		
		if (!c.isShopping) {
			return false;
		}

		if (fromSlot < 0 || fromSlot > ShopHandler.MaxShopItems) { // dupe fix,
																	// must be
																	// on top of
																	// all buy
																	// shop
																	// methods
			return false;
		}

		if (itemID < 0) {
			return false;
		}

		if (c.myShopId == 7390) {
			return buyFromPOS(itemID, fromSlot, amount);
		}

		if (amount < 1) {// anythign less than 1 in POS is changePrice for owner
			return false;
		}

		if (c.myShopId == UntradeableManager.SHOP_ID) {
			UntradeableManager.buy(c, itemID, amount, fromSlot);
			return false;
		}

		if (c.myShopId == 14) {
			skillBuy(itemID);
			return false;
		} /*
			 * else if (c.myShopId == 15) { buyVoid(itemID); return false; }
			 */

		if (c.myShopId < 0 || c.myShopId > ShopHandler.MaxShops) {
			return false;
		}

		if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] < 1) {
			return false;
		}
		if (ShopHandler.ShopItems[c.myShopId][fromSlot] < 1) {
			return false;
		}

		if (ShopHandler.ShopItems[c.myShopId][fromSlot] != itemID + 1) {
			return false;
		}

		if (!shopSellsItem(itemID)) {
			return false;
		}
		/*
		 * if (itemID != ShopHandler.ShopItems[c.myShopId][fromSlot]-1) {
		 * c.sendMessage("Error code: 5537 - Report to Owner"); return false; }
		 */
		if (amount > 0) {
			if (amount > ShopHandler.ShopItemsN[c.myShopId][fromSlot]) {
				amount = ShopHandler.ShopItemsN[c.myShopId][fromSlot];
			}
			if (amount < 1 && (fromSlot
					+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
				ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
				ShopHandler.ShopItemsN[c.myShopId][fromSlot] = 0;
				resetShop(c.myShopId);
				return false;
			}
			// double ShopValue;
			// double TotPrice;
			int TotPrice2 = 0;
			// int Overstock;
			int Slot = 0;
			int Slot1 = 0;// Tokkul

			if (ShopHandler.OTHER_SHOPS_SET.contains(c.myShopId)) {
				handleOtherShop(itemID, amount, fromSlot);
				return false;
			}
			
			if (amount > 10) {
				double priceCheck = Math
						.floor(getItemShopValue(itemID, 0, fromSlot) * amount);
				if (priceCheck < 0 || priceCheck > Integer.MAX_VALUE) {
					c.sendMessage("Error 6928 - Report to Administrators.");
					return false;
				}

				int totalPrice = (int) priceCheck;
				int goldSlot = c.getItems().getItemSlot(getGoldType());
				if (!c.getItems().isStackable(itemID)) {
					if (c.getItems().freeSlots() == 0) {
						c.sendMessage(
								"You don't have enough space in your inventory.");
						return false;
					}
					if (amount > c.getItems().freeSlots()) {
						amount = c.getItems().freeSlots();
						totalPrice = (int) Math.floor(
								getItemShopValue(itemID, 0, fromSlot) * amount);
						if (c.getItems().playerHasItem(getGoldType(), goldSlot,
								totalPrice)) {
							c.getItems().deleteItemInOneSlot(getGoldType(),
									goldSlot, totalPrice);
							c.getItems().addItem(itemID, amount);
							c.sendMessage(
									"You have ran out of space in your inventory.");

							if ((fromSlot + 1) > ShopHandler.ShopItemsStandard[c.myShopId])
								ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= amount;
							if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] == 0
									&& (fromSlot
											+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}

							if (DBConfig.LOG_SHOP_BUY) {
								Server.getLogWriter()
										.addToBuyList(new Buy(c.getName(),
												c.mySQLIndex,
												new GameItem(itemID, amount),
												totalPrice, c.myShopId));
							}
							c.getItems().resetItems(3823);
							resetShop(c.myShopId);
							updatePlayerShop();
							return true;
						} else if (c.getShops().canBuyWithPouch() && c.getMoneyPouch() >= totalPrice) { // buying
																		// with
																		// moneypouch
							MoneyPouch.removeFromMoneyPouch(c, totalPrice);
							c.getItems().addItem(itemID, amount);
							c.sendMessage(
									"You have ran out of space in your inventory.");

							if ((fromSlot + 1) > ShopHandler.ShopItemsStandard[c.myShopId])
								ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= amount;
							if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] == 0
									&& (fromSlot
											+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}

							if (DBConfig.LOG_SHOP_BUY) {
								Server.getLogWriter()
										.addToBuyList(new Buy(c.getName(),
												c.mySQLIndex,
												new GameItem(itemID, amount),
												totalPrice, c.myShopId));
							}
							c.getItems().resetItems(3823);
							resetShop(c.myShopId);
							updatePlayerShop();
							return true;
						} else {
							if (WorldType.equalsType(WorldType.SPAWN)) {
								c.sendMessage(
										"You don't have enough Blood money.");
							} else {
								c.sendMessage("You don't have enough coins.");
							}
							return false;
						}
					} else {
						if (c.getItems().playerHasItem(getGoldType(), goldSlot,
								totalPrice)) {
							c.getItems().deleteItemInOneSlot(getGoldType(),
									goldSlot, totalPrice);
							c.getItems().addItem(itemID, amount);

							if ((fromSlot + 1) > ShopHandler.ShopItemsStandard[c.myShopId])
								ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= amount;
							if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] == 0
									&& (fromSlot
											+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}

							c.getItems().resetItems(3823);
							if (DBConfig.LOG_SHOP_BUY) {
								Server.getLogWriter()
										.addToBuyList(new Buy(c.getName(),
												c.mySQLIndex,
												new GameItem(itemID, amount),
												totalPrice, c.myShopId));
							}
							resetShop(c.myShopId);
							updatePlayerShop();
							return true;
						} else if (c.getShops().canBuyWithPouch() && c.getMoneyPouch() >= totalPrice) { // buying
																		// from
																		// moneypouch
							MoneyPouch.removeFromMoneyPouch(c, totalPrice);
							c.getItems().addItem(itemID, amount);

							if ((fromSlot + 1) > ShopHandler.ShopItemsStandard[c.myShopId])
								ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= amount;
							if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] == 0
									&& (fromSlot
											+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}

							c.getItems().resetItems(3823);
							if (DBConfig.LOG_SHOP_BUY) {
								Server.getLogWriter()
										.addToBuyList(new Buy(c.getName(),
												c.mySQLIndex,
												new GameItem(itemID, amount),
												totalPrice, c.myShopId));
							}
							resetShop(c.myShopId);
							updatePlayerShop();
							return true;
						} else {
							if (WorldType.equalsType(WorldType.SPAWN)) {
								c.sendMessage(
										"You don't have enough Blood money.");
							} else {
								c.sendMessage("You don't have enough coins..");
							}
							return false;
						}
					}
				} else {
					if (c.getItems().playerHasItem(getGoldType(), goldSlot,
							totalPrice)) {
						c.getItems().deleteItemInOneSlot(getGoldType(),
								goldSlot, totalPrice);
						c.getItems().addItem(itemID, amount);

						if ((fromSlot + 1) > ShopHandler.ShopItemsStandard[c.myShopId])
							ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= amount;
						if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] == 0
								&& (fromSlot
										+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
							ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
						}

						c.getItems().resetItems(3823);
						if (DBConfig.LOG_SHOP_BUY) {
							Server.getLogWriter().addToBuyList(
									new Buy(c.getName(), c.mySQLIndex,
											new GameItem(itemID, amount),
											totalPrice, c.myShopId));
						}
						resetShop(c.myShopId);
						updatePlayerShop();
						return true;
					} else if (c.getShops().canBuyWithPouch() && c.getMoneyPouch() >= totalPrice) { // buying with
																	// moneypouch
						MoneyPouch.removeFromMoneyPouch(c, totalPrice);
						c.getItems().addItem(itemID, amount);

						if ((fromSlot + 1) > ShopHandler.ShopItemsStandard[c.myShopId])
							ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= amount;
						if (ShopHandler.ShopItemsN[c.myShopId][fromSlot] == 0
								&& (fromSlot
										+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
							ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
						}

						c.getItems().resetItems(3823);
						if (DBConfig.LOG_SHOP_BUY) {
							Server.getLogWriter().addToBuyList(
									new Buy(c.getName(), c.mySQLIndex,
											new GameItem(itemID, amount),
											totalPrice, c.myShopId));
						}
						resetShop(c.myShopId);
						updatePlayerShop();
						return true;
					} else {
						if (WorldType.equalsType(WorldType.SPAWN)) {
							c.sendMessage("You don't have enough Blood money.");
						} else {
							c.sendMessage("You don't have enough coins.");
						}
						return false;
					}
				}
			}
			int sold = 0;
			for (int i = amount; i > 0; i--) {

				TotPrice2 = (int) Math
						.round(getItemShopValue(itemID, 0, fromSlot));
				Slot = c.getItems().getItemSlot(getGoldType());
				Slot1 = c.getItems().getItemSlot(6529);

				final boolean useMoneyPouch = Slot == -1 && c.getMoneyPouch() >= TotPrice2;

				if (!useMoneyPouch && Slot == -1 && c.myShopId != 29 && c.myShopId != 30 && c.myShopId != 31) {
					if (WorldType.equalsType(WorldType.SPAWN)) {
						c.sendMessage("You don't have enough Blood money.");
					} else {
						c.sendMessage("You don't have enough coins.");
					}
					break;
				}
				if (Slot1 == -1 && c.myShopId == 29 || c.myShopId == 30
						|| c.myShopId == 31) {
					c.sendMessage("You don't have enough tokkul.");
					break;
				}
				if (TotPrice2 <= 1) {
					TotPrice2 = (int) Math
							.floor(getItemShopValue(itemID, 0, fromSlot));
					TotPrice2 *= 1.66;
				}
				if (c.myShopId != 29 || c.myShopId != 30 || c.myShopId != 31) {
					if (useMoneyPouch && c.getShops().canBuyWithPouch()) {
						if (c.getItems().freeSlots() > 0) {
							MoneyPouch.removeFromMoneyPouch(c, TotPrice2);
							c.getItems().addItem(itemID, 1);
							ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= 1;
							ShopHandler.ShopItemsDelay[c.myShopId][fromSlot] = 0;
							if ((fromSlot
									+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}
							sold++;
						} else {
							c.sendMessage(
									"You don't have enough space in your inventory.");
							break;
						}
					} else if (c.playerItemsN[Slot] >= TotPrice2) {
						if (c.getItems().freeSlots() > 0) {
							c.getItems().deleteItemInOneSlot(getGoldType(),
									c.getItems().getItemSlot(getGoldType()),
									TotPrice2);
							c.getItems().addItem(itemID, 1);
							ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= 1;
							ShopHandler.ShopItemsDelay[c.myShopId][fromSlot] = 0;
							if ((fromSlot
									+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}
							sold++;
						} else {
							c.sendMessage(
									"You don't have enough space in your inventory.");
							break;
						}
					} else {
						if (WorldType.equalsType(WorldType.SPAWN)) {
							c.sendMessage("You don't have enough Blood Money.");
						} else {
							c.sendMessage("You don't have enough coins.");
						}
						break;
					}
				}
				if (c.myShopId == 29 || c.myShopId == 30 || c.myShopId == 31) {
					if (c.playerItemsN[Slot1] >= TotPrice2) {
						if (c.getItems().freeSlots() > 0) {
							c.getItems().deleteItemInOneSlot(6529,
									c.getItems().getItemSlot(6529), TotPrice2);
							c.getItems().addItem(itemID, 1);
							ShopHandler.ShopItemsN[c.myShopId][fromSlot] -= 1;
							ShopHandler.ShopItemsDelay[c.myShopId][fromSlot] = 0;
							if ((fromSlot
									+ 1) > ShopHandler.ShopItemsStandard[c.myShopId]) {
								ShopHandler.ShopItems[c.myShopId][fromSlot] = 0;
							}
							sold++;
						} else {
							c.sendMessage(
									"You don't have enough space in your inventory.");
							break;
						}
					} else {
						c.sendMessage("You don't have enough tokkul.");
						break;
					}
				}
			}
			if (sold > 0) {
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter()
							.addToBuyList(new Buy(c.getName(), c.mySQLIndex,
									new GameItem(itemID, sold), TotPrice2,
									c.myShopId));
				}
			}
			c.getItems().resetItems(3823);
			resetShop(c.myShopId);
			// PlayerSave.saveGame(c);
			updatePlayerShop();
			return true;
		}
		return false;
	}
	
	public boolean canBuyWithPouch() {
		return true;
	}

	public int get99Count() {
		int count = 0;
		for (int j = 0; j < c.getPlayerLevel().length; j++) {
			if (c.getSkills().getStaticLevel(j) >= 99) {
				count++;
			}
		}
		return count;
	}
	
	  private int getGoldType() {
		if (WorldType.equalsType(WorldType.SPAWN)) {
			return 8890;
		}
		if (c.myShopId >= 55 && c.myShopId <= 58) {
			return DungeonConstants.RUSTY_COINS;
		}
		if (c.myShopId == 60) {
			return 30107;
		}
		if (c.myShopId == 61) {
			return 6306;
		}
		return 995;
	}

	/**
	 * @param ItemID
	 * @param buy0Sell1
	 *            -- 0=buying -- 1=selling
	 * @param fromSlot
	 * @return
	 */
	public double getItemShopValue(int ItemID, int buy0Sell1, int fromSlot) {
		if (c.myShopId == SpawnTable.SHOP_ID) {
			return 0;
		}
		if (buy0Sell1 == 0) {
			if (ShopHandler.ShopItems[c.myShopId][fromSlot] != ItemID + 1) {
				return Integer.MAX_VALUE;
			}
		}

		if (c.myShopId == UntradeableManager.SHOP_ID) {
			int price = UntradeableManager.price(ItemID);
			if (price > 0) {
				return price;
			}
		}

		ItemDefinition itemDef = ItemDefinition.forId(ItemID);
		if (itemDef == null) {
			c.sendMessage("Item is null! report error 3392");
			return 0;
		}

		int price = itemDef.getShopValue();
		if (Config.isSpawnPVP() && price != 0) {
			return 0;
		}

		float finalPrice = price;

		if (c.myShopId >= 55 && c.myShopId <= 58) {
			if (buy0Sell1 == 1) {
				finalPrice *= 0.20;
			}
		} else {
			if (buy0Sell1 == 0) { // make buying more expensive than selling to shop
				finalPrice *= 1.15;
			}
		}
		return finalPrice;
	}

	public void handleOtherShop(int itemID, int amountToBuy, int slot) {
		boolean itemStackable = ItemProjectInsanity.itemIsStackable(itemID);
		int freeSlots = c.getItems().freeSlots();
		if (itemStackable) {
			if (freeSlots == 0 && !c.getItems().playerHasItem(itemID)) { // no
																			// more
																			// slots
				c.sendMessage("Your inventory is full.");
				return;
			}
		} else { // items not stackable
			if (freeSlots == 0) {
				c.sendMessage("Your inventory is full.");
				return;
			}
			if (amountToBuy > freeSlots) {
				amountToBuy = freeSlots;
				c.sendMessage("Your inventory is full.");
			}
		}
		if (c.myShopId == 80) {
			int value = ShopPrice.getSeasonalShopItemValue(c, itemID) * amountToBuy;
			if (c.getItems().getItemAmount(11180) >= value) {
				c.getItems().deleteItemInOneSlot(11180, value);
				if (itemID == 455) {
				 	Variables.SEASONAL_INTERFACE_POINTS.addValue(c, amountToBuy);
				 	int newPts = Variables.SEASONAL_INTERFACE_POINTS.getValue(c);
				 	c.sendMessage("You've purchased " + amountToBuy + " power " + (amountToBuy == 1 ? "point" : "points") + ". Now you have " + newPts + " " + (newPts == 1 ? "point" : "points") + ".");
				} else {
					c.getItems().addItem(itemID, amountToBuy);
				}
				c.getSeasonalData().addBought(itemID, amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter()
							.addToBuyList(new Buy(c.getName(), c.mySQLIndex,
									new GameItem(itemID, 1),
									value,
									c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough tokens to buy this item.");
			}
		} else if (c.myShopId == 8 || c.myShopId == 82) {
			if (c.pkp >= ShopPrice.getPKShopItemValue(itemID) * amountToBuy) {
				c.pkp -= ShopPrice.getPKShopItemValue(itemID) * amountToBuy;
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				c.getItems().resetItems(3823);
				c.getPacketSender().sendFrame126(
						"<col=ffffff>Pk Points: <col=ff00>" + c.pkp + " ", 7333);
			} else {
				c.sendMessage(
						"You do not have enough pk points to buy this item.");
			}
		} else if (c.myShopId == 9 || c.myShopId == 12 || c.myShopId == 13
				|| c.myShopId == 15) {
			if (c.getDonPoints() >= ShopPrice.getDonorShopItemValue(itemID) * amountToBuy) {
				c.setDonPoints(c.getDonPoints() - ShopPrice.getDonorShopItemValue(itemID) * amountToBuy, true);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter()
							.addToBuyList(new Buy(c.getName(), c.mySQLIndex,
									new GameItem(itemID, 1),
									ShopPrice.getDonorShopItemValue(itemID) * amountToBuy,
									c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough donator points to buy this item.");
			}
		} else if (c.myShopId == 10 || c.myShopId == 105) {
			if (c.getSlayerPoints()
					.getTotalValue() >= ShopPrice.getSlayerShopItemValue(itemID)
							* amountToBuy) {
				c.getSlayerPoints()
						.remove(ShopPrice.getSlayerShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getSlayerShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough slayer points to buy this item.");
			}
		} else if (c.myShopId == 49) {
			if (c.getDuoPoints()
					.getTotalValue() >= ShopPrice.getDuoSlayerShopItemValue(itemID)
							* amountToBuy) {
				c.getDuoPoints().remove(
						ShopPrice.getDuoSlayerShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getDuoSlayerShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough duo points to buy this item.");
			}
		} else if (c.myShopId == 18) {
			if (c.getPcPoints() >= ShopPrice.getPcShopItemValue(itemID) * amountToBuy) {
				c.setPcPoints(c.getPcPoints()
						- ShopPrice.getPcShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter()
							.addToBuyList(new Buy(c.getName(), c.mySQLIndex,
									new GameItem(itemID, 1),
									ShopPrice.getPcShopItemValue(itemID) * amountToBuy,
									c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Pest Control points to buy this item.");
			}
		} else if (c.myShopId == 25 || c.myShopId == 84) {
			if (c.getItems().getItemAmount(2996) >= ShopPrice.getVoteShopItemValue(itemID)
					* amountToBuy) {
				c.getItems().deleteItemInOneSlot(2996,
						ShopPrice.getVoteShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter()
							.addToBuyList(new Buy(c.getName(), c.mySQLIndex,
									new GameItem(itemID, 1),
									ShopPrice.getVoteShopItemValue(itemID) * amountToBuy,
									c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Vote Tickets to buy this item.");
			}
		} else if (c.myShopId == 102) {
			if (c.getItems().getItemAmount(
					2996) >= ShopPrice.getIronVoteShopItemValue(itemID) * amountToBuy) {
				c.getItems().deleteItemInOneSlot(2996,
						ShopPrice.getIronVoteShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getIronVoteShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Vote Tickets to buy this item.");
			}
		} else if (c.myShopId == 33) {
			if (c.getItems().getItemAmount(
					9038) >= ShopPrice.getDonStoneShopItemValue(itemID) * amountToBuy) {
				c.getItems().deleteItem2(9038,
						ShopPrice.getDonStoneShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getDonStoneShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Stone statuettes to buy this item.");
			}
		} else if (c.myShopId == 34) {
			if (c.getItems().getItemAmount(
					9034) >= ShopPrice.getDonGoldShopItemValue(itemID) * amountToBuy) {
				c.getItems().deleteItem2(9034,
						ShopPrice.getDonGoldShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getDonGoldShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Golden statuettes to buy this item.");
			}
		} else if (c.myShopId == 35) {
			if (c.getItems()
					.getItemAmount(21570) >= ShopPrice.getDonDiamondShopItemValue(itemID)
							* amountToBuy) {
				c.getItems().deleteItem2(21570,
						ShopPrice.getDonDiamondShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getDonDiamondShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Jewelled diamond statuettes to buy this item.");
			}
		} else if (c.myShopId == 36 || c.myShopId == 46) {
			if (c.getLoyaltyPoints() >= ShopPrice.getLoyaltyShopItemValue(itemID)
					* amountToBuy) {
				c.setLoyaltyPoints(
						c.getLoyaltyPoints()
								- ShopPrice.getLoyaltyShopItemValue(itemID) * amountToBuy,
						true);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getLoyaltyShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Loyalty points to buy this item.");
			}
		} else if (c.myShopId == 76) {
			if (c.getChristmasPoints() >= ShopPrice.getChristmasShopItemValue(itemID) * amountToBuy) {
				c.setChristmasPoints(c.getChristmasPoints() - ShopPrice.getChristmasShopItemValue(itemID) * amountToBuy);

				c.sendMessage("You have " + c.getChristmasPoints() + " Christmas points.");
				
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getChristmasShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough Christmas points to buy this item.");
			}
		} else if (c.myShopId == 38) {
			if (c.penguinPoints >= ShopPrice.getPenguinShopItemValue(itemID)
					* amountToBuy) {
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				c.penguinPoints -= ShopPrice.getPenguinShopItemValue(itemID)
						* amountToBuy;
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getPenguinShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
				// } else {
				// c.sendMessage("You do not have enough Penguin points to buy
				// this item.");
			}
		} else if (c.myShopId == 41) {
			if (c.lmsPoints >= ShopPrice.getDungShopItemValue(itemID) * amountToBuy) {
				c.lmsPoints -= ShopPrice.getDungShopItemValue(itemID) * amountToBuy;
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(), c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getDungShopItemValue(itemID) * amountToBuy, c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough Dungeoneering Tokens.");
			}
		} else if (c.myShopId == 43) {
			if (c.getAgilityPoints() >= ShopPrice.getAgilityShopItemValue(itemID)
					* amountToBuy) {
				c.setAgilityPoints(c.getAgilityPoints()
						- ShopPrice.getAgilityShopItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getAgilityShopItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Agility Tokens to buy this item.");
			}
		} else if (c.myShopId == 45 || c.myShopId == 50) {
			if (c.getItems().getItemAmount(
					4067) >= ShopPrice.getCastleWarsItemValue(itemID) * amountToBuy) {
				c.getItems().deleteItemInOneSlot(4067,
						ShopPrice.getCastleWarsItemValue(itemID) * amountToBuy);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getCastleWarsItemValue(itemID) * amountToBuy,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage(
						"You do not have enough Castle wars tickets to buy this item.");
			}
		} else if (c.myShopId == 52) {
			if (c.getBarbarianAssault().getHonorPoints() >= ShopPrice.getBarbShopItemValue(itemID) * amountToBuy) {
				c.getBarbarianAssault().setHonorPoints(
						c.getBarbarianAssault().getHonorPoints() - ShopPrice.getBarbShopItemValue(itemID) * amountToBuy, true);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(), c.mySQLIndex, new GameItem(itemID, 1),
							ShopPrice.getBarbShopItemValue(itemID) * amountToBuy, c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough honour points to buy this item.");
			}
		} else if (c.myShopId == 53) {
			int totalValue = ShopPrice.getSoulWarsShopItemValue(itemID) * amountToBuy;
			if (c.getZeals() >= totalValue) {
				c.setZeals(c.getZeals() - totalValue);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(), c.mySQLIndex, new GameItem(itemID, 1),
							totalValue, c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough zeals to buy this item.");
			}
		} else if (c.myShopId >= 55 && c.myShopId <= 58) {
			long totalValue = (long) ((int) getItemShopValue(itemID, 0, slot) * amountToBuy);
			if (totalValue > Integer.MAX_VALUE) {
				totalValue = Integer.MAX_VALUE;
			}

			if ((long) c.getItems().getItemAmount(DungeonConstants.RUSTY_COINS) >= totalValue) {
				c.getItems().deleteItem2(DungeonConstants.RUSTY_COINS, (int) totalValue);
				c.getItems().addItem(itemID, 1 * amountToBuy);
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough rusty coins to buy this item.");
			}
		} else if (c.myShopId == 60) {
			int value = ShopPrice.getSurvivalShopItemValue(itemID);
			int tokenAmount = c.getItems().getItemAmount(30107);
			int cost = value * amountToBuy;

			if (cost >= tokenAmount && tokenAmount >= value) {
				amountToBuy = tokenAmount / value;
				cost = value * amountToBuy;
			}

			if (tokenAmount >= cost) {
				c.getItems().deleteItemInOneSlot(30107, cost);
				c.getItems().addItem(itemID, amountToBuy);
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough Survival tokens to buy this item.");
			}
		} else if (c.myShopId == 61) {
			int value = ShopPrice.getGabootyShopItemValue(itemID) * amountToBuy;
			if (c.getItems().getItemAmount(6306) >= value) {
				c.getItems().deleteItemInOneSlot(6306, value);
				c.getPA().loadQuests();
				c.getItems().addItem(itemID, 1 * amountToBuy);
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(),
							c.mySQLIndex, new GameItem(itemID, 1),
							value,
							c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough Trading sticks to buy this item.");
			}
		} else if (c.myShopId == 65 || c.myShopId == 66) {
			int value = ShopPrice.getLmsShopItemValue(itemID) * amountToBuy;
			if (c.lmsPoints >= value) {
				c.lmsPoints -= value;
				c.getItems().addItem(itemID, 1 * amountToBuy);
				c.getPA().loadQuests();
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(), c.mySQLIndex, new GameItem(itemID, 1), value, c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough LMS Points to buy this item.");
			}
		} else if (c.myShopId == 67) {
			int value = ShopPrice.getTaskShopItemValue(itemID) * amountToBuy;
			if (c.dailyTasksPoints >= value) {
				c.dailyTasksPoints -= value;
				c.getItems().addItem(itemID, 1 * amountToBuy);
				c.getPA().loadQuests();
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(), c.mySQLIndex, new GameItem(itemID, 1), value, c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough Daily Points to buy this item.");
			}
		} else if (c.myShopId == 77) {
			int value = ShopPrice.getDominionTokensShopItemValue(itemID) * amountToBuy;
			if (c.dominionTokens >= value) {
				c.dominionTokens -= value;
				c.getItems().addItem(itemID, 1 * amountToBuy);
				c.getPA().loadQuests();
				if (DBConfig.LOG_SHOP_BUY) {
					Server.getLogWriter().addToBuyList(new Buy(c.getName(), c.mySQLIndex, new GameItem(itemID, 1), value, c.myShopId));
				}
				c.getItems().resetItems(3823);
			} else {
				c.sendMessage("You do not have enough Dominion tokens to buy this item.");
			}
		}
	}

	/**
	 * Shops
	 **/

	public void openShop(int ShopID) {
		openShop(ShopID, ShopType.REGULAR);
	}

	public void openShop(int ShopID, ShopType shopType) {
		if (c.getDialogueBuilder().isActive()) {
			c.getDialogueBuilder().setClose(false);
		}

		if (c.isIronMan() && (ShopID == 1
				|| ShopID == 2 || ShopID == 3 || ShopID == 4 || ShopID == 5
				|| ShopID == 6 || ShopID == 7 || ShopID == 8 || ShopID == 25
				|| ShopID == 9 || ShopID == 12 || ShopID == 13 || ShopID == 15
				|| ShopID == 26 || ShopID == 27 || ShopID == 28 || ShopID == 51
				|| ShopID == 77 || ShopID == 80
				|| ShopID == 81 || ShopID == 82 || ShopID == 83 || ShopID == 84
				|| ShopID == 85 || ShopID == 86 || ShopID == 90)) {
			c.sendMessage(
					"IronMan Mode does not allow buying/selling with shopkeepers.");
			return;
		}

		c.shopType = shopType;
		c.isShopping = true;
		c.myShopId = ShopID;
		c.getItems().resetItems(3823);
		resetShop(ShopID);
		c.getPacketSender().sendFrame248(shopType.getId(), 3822);
		c.getPacketSender().sendFrame126(ShopHandler.ShopName[ShopID], shopType.getTitleId());
	}

	public String updateBigShopPrices(int id, int slot) {
		int ShopValue = (int) Math
				.floor(getItemShopValue(id, 0, slot));
		if (ShopValue >= 10000 && ShopValue < 1000000) {
			return (ShopValue / 1000) + "K";
		} else if (ShopValue >= 1000000) {
			return (ShopValue / 1000000) + "M";
		}
		
		return Integer.toString(ShopValue);
	}

	public void openSkillCape() {
		int capes = get99Count();
		if (capes > 1) {
			capes = 1;
		} else {
			capes = 0;
		}
		c.myShopId = 14;
		setupSkillCapes(capes, get99Count());
	}

	public void resetShop(int ShopID) {
		
		int totalItems = 0;
		for (int i = ShopHandler.MaxShopItems - 1; i >= 0; i--) {
			if (ShopHandler.ShopItems[ShopID][i] > 0) {
				totalItems++;
			}
		}

		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(c.shopType.getInventoryId());
		c.getOutStream().writeShort(totalItems);
		for (int i = 0; i < totalItems; i++) {
			int count = ShopHandler.ShopItemsN[ShopID][i];
			if (count > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(count);
			} else {
				c.getOutStream().writeByte(count);
			}
			
			int id = ShopHandler.ShopItems[ShopID][i];
			if (id > Config.ITEM_LIMIT
					|| id < 0) {
				id = Config.ITEM_LIMIT;
			}
			c.getOutStream().write3Byte(id);
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();

		if (c.shopType == ShopType.BIG) {
			int id = 2;
			for (int i = 0; i < totalItems; i++) {
				c.getPacketSender().sendDynamicComponentText(updateBigShopPrices(ShopHandler.ShopItems[ShopID][i] - 1, i), 44010, id);
				id += 3;
			}
		}
	}

	public boolean allowSell(int itemId) {
		//EVENT ITEMS
		if (itemId == 12109 || itemId == 12117 || itemId == 12119 || itemId == 12121
				|| itemId == 6950 || itemId == 14713 || itemId == 10501) {
			return false;
		}

		if (!ItemProjectInsanity.itemIsTradeable(itemId)) {
			return false;
		}
		
		return true;
	}
	
	public boolean sellItem(int itemID, int fromSlot, int amount) {
		if (fromSlot < 0 || fromSlot >= c.playerItems.length) {
			return false;
		}
		
		if (c.myShopId == SpawnTable.SHOP_ID) {
			SpawnTable.addItemToSpawnTable(c, new Item(itemID, amount));
			return true;
		}

		if (!c.isShopping || c.inTrade) {
			return false;
		}

		final int actualAmount = c.getItems().getItemAmount(itemID);

		if (actualAmount <= 0) {
			return false;
		}
		if (amount > actualAmount) {
			amount = actualAmount;
		}

		if (!c.getItems().playerHasItem(itemID, amount)) {
			return false;
		}
		
		if (c.myShopId == UntradeableManager.SHOP_ID) {
			c.sendMessage("You can't sell items to the Untradeable Shop.");
			return false;
		}
		for (int element : ShopHandler.UNSELLABLE_SHOPS) {
			if (c.myShopId == element) {
				c.sendMessage("You can't sell to this shop.");
				return false;
			}
		}
		
		if (!allowSell(itemID)) {
			c.sendMessage("You can't sell this item.");
			return false;
		}
	
		if (CompletionistCape.isCompletionistCape(itemID)) {
			c.sendMessage("You can't sell the capes.");
			return false;
		}

		// for (int i : Config.ITEM_SELLABLE) {
		// if (i == itemID) {
		// c.sendMessage("You can't sell "
		// + ItemConstants.getItemName(itemID).toLowerCase() + ".");
		// return false;
		// }
		// }
		if (c.playerRights == 2 && !DBConfig.ADMIN_CAN_SELL_ITEMS && Config.RUN_ON_DEDI) {
			c.sendMessage("Selling items as an admin has been disabled.");
			return false;
		}

		if (c.myShopId == 7390) {
			// for (int i : Config.ITEM_TRADEABLE) {
			// if (i == itemID) {
			// c.sendMessage("You can't sell this item.");
			// return false;
			// }
			// }
			if (c.myShopClient == null) { // exploit?
				c.getPA().closeAllWindows();
				return false;
			}
			if (!c.inMarket()) {
				return false;
			}
			if (WorldType.equalsType(WorldType.SPAWN)) {
				c.sendMessage("You can't sell this item in SoulPvP.");
				c.resetMyShopSellingVars();
				return false;
			}

			if (itemID == 995 || itemID == 8890 || itemID == 6529 || itemID == 10831) {
				c.sendMessage("You cannot sell that here.");
				c.resetMyShopSellingVars();
				return false;
			}

			if (!ItemProjectInsanity.itemIsTradeable(itemID)) {
				c.sendMessage("You can't sell this item.");
				c.resetMyShopSellingVars();
				return false;
			}
			
			for (int i : DBConfig.DUNG_REWARDS) {
				if (i == itemID) {
					c.sendMessage("You can't sell this item.");
					c.resetMyShopSellingVars();
					return false;
				}
			}
			if (c.getName().equals(c.myShopClient.getName())) {
				PlayerOwnedShops.addItemToPlayerOwnedShop(c, itemID, amount,
						fromSlot);
			} else {
				c.sendMessage("You can only sell items on your own store.");
			}
			return true;
		}

		if (!ItemProjectInsanity.itemIsSellable(itemID) && (c.myShopId == 60 && itemID != 7458)) {//Lms mithril gloves
			c.sendMessage("You can't sell "
					+ ItemConstants.getItemName(itemID).toLowerCase() + ".");
			return false;
		}

		if (!WorldType.equalsType(WorldType.SPAWN)) {
			/*if (getDonStoneShopItemValue(itemID) != 9999) {
				c.sendMessage("You can't sell this item to store.");
				return false;
			}

			if (getDonGoldShopItemValue(itemID) != 9999) {
				c.sendMessage("You can't sell this item to store.");
				return false;
			}

			if (getDonDiamondShopItemValue(itemID) != 9999) {
				c.sendMessage("You can't sell this item to store.");
				return false;
			}*/

			if (ShopPrice.getPcShopItemValue(itemID) != 9999 && c.myShopId == 18) {
				c.sendMessage("You can't sell Pc shop items.");
				return false;
			}

			if (ShopPrice.getLoyaltyShopItemValue(itemID) != 9999 && c.myShopId == 36) {
				c.sendMessage("You can't sell Loyalty shop items.");
				return false;
			}
			
			if (ShopPrice.getChristmasShopItemValue(itemID)!= 9999 && c.myShopId == 76) {
				c.sendMessage("You can't sell christmas shop items.");
				return false;
			}

			if (ShopPrice.getVoteShopItemValue(itemID) != 9999 && c.myShopId == 25) {
				c.sendMessage("You can't sell vote shop items.");
				return false;
			}

			if (ShopPrice.getIronVoteShopItemValue(itemID) != 9999 && c.myShopId == 102) {
				c.sendMessage("You can't sell vote shop items.");
				return false;
			}

			if (ShopPrice.getDonorShopItemValue(itemID) != 9999 && (c.myShopId == 9 || c.myShopId == 12 || c.myShopId == 13
					|| c.myShopId == 15)) {
				c.sendMessage("You can't sell Donor shop items.");
				return false;
			}

			if (ShopPrice.getPKShopItemValue(itemID) != 9999 && c.myShopId == 8) {
				c.sendMessage("You can't sell PK shop items.");
				return false;
			}

			if (ShopPrice.getSlayerShopItemValue(itemID) != 9999 && c.myShopId == 10) {
				c.sendMessage("You can't sell Slayer shop items.");
				return false;
			}

			if (ShopPrice.getDuoSlayerShopItemValue(itemID) != 9999 && c.myShopId == 49) {
				c.sendMessage("You can't sell Slayer shop items.");
				return false;
			}

			if (ShopPrice.getPenguinShopItemValue(itemID) != 9999 && c.myShopId == 38) {
				c.sendMessage("You can't sell this item to store.");
				return false;
			}

			if (ShopPrice.getAgilityShopItemValue(itemID) != 9999 && c.myShopId == 43) {
				c.sendMessage("You can't sell this item to store.");
				return false;
			}	

			if (ShopPrice.getBarbShopItemValue(itemID) != 9999 && c.myShopId == 52) {
				c.sendMessage("You can't sell this item to store.");
				return false;
			}
			
			if (ShopPrice.getLmsShopItemValue(itemID) != 9999 && (c.myShopId == 65 || c.myShopId == 66)) {
				c.sendMessage("You can't sell LMS shop items.");
				return false;
			}
			if (ShopPrice.getTaskShopItemValue(itemID) != 9999 && (c.myShopId == 67)) {
				c.sendMessage("You can't sell Task shop items.");
				return false;
			}
			if (ShopPrice.getDominionTokensShopItemValue(itemID) != 9999 && (c.myShopId == 77)) {
				c.sendMessage("You can't sell dominion shop items.");
				return false;
			}


		}
		if (amount > 0 && itemID == (c.playerItems[fromSlot] - 1)) {
			if (!canSellToStore(itemID, fromSlot)) {
				c.sendMessage("You can't sell "
						+ ItemConstants.getItemName(itemID).toLowerCase()
						+ " to this store.");
				return false;
			}

			if (ItemProjectInsanity.itemStackable[(c.playerItems[fromSlot] - 1)]
					&& amount > c.playerItemsN[fromSlot]) {
				amount = c.playerItemsN[fromSlot];
			} else if (!ItemProjectInsanity.itemStackable[(c.playerItems[fromSlot] - 1)]
					&& amount > c.getItems().getItemAmount(itemID)) {
				amount = c.getItems().getItemAmount(itemID);
			}

			final boolean maxCash = (c.getItems().itemAmountLong(getGoldType()) + getItemShopValue(itemID, 1, fromSlot) * amount) > Integer.MAX_VALUE;

			if (maxCash) {
				c.sendMessage("You have too much gold with you. Please deposit your gold then try again.");
				return true;
			}

			int itemsSold = 0;

			if (!ItemProjectInsanity.itemIsStackable(itemID)) {
				if (amount == 1) {
					c.getItems().deleteItemInOneSlot(itemID, fromSlot, 1);
					itemsSold++;
				} else {
					for (int i = 0; i < amount; i++) {
						if (c.getItems().playerHasItem(itemID, 1)) {
							c.getItems().deleteItemInOneSlot(itemID, c.getItems().getItemSlot(itemID), 1);
							itemsSold++;
						}
					}
				}
			} else {

				final int amountLeft = c.getItems().getItemAmount(itemID);

				if ((amount < amountLeft) && c.getItems().freeSlots() <= 0 && !c.getItems().playerHasItem(getGoldType())) {
					c.sendMessage("Not enough space in your inventory.");
				} else {
					c.getItems().deleteItemInOneSlot(itemID, fromSlot, amount);
					itemsSold += amount;
				}

			}

			final int value = (int) (getItemShopValue(itemID, 1, fromSlot) * itemsSold);
			final int survivalShopValue = (int) (ShopPrice.getSurvivalShopSellValue(itemID) * itemsSold);
			final int gabootyShopValue = (int) (ShopPrice.getGabootyShopSellValue(itemID) * itemsSold);

			if (c.myShopId == 60) {
				c.getItems().addItem(getGoldType(), survivalShopValue);
			} else if (c.myShopId == 61) {
				c.getItems().addItem(getGoldType(), gabootyShopValue);
			} else {
			    c.getItems().addItem(getGoldType(), WorldType.equalsType(WorldType.SPAWN) ? (int)((double)value * 75 / 100) : value);
			}

			c.getItems().resetItems(3823);
			resetShop(c.myShopId);
			updatePlayerShop();

			if (amount > 0 && DBConfig.LOG_SHOP_SELL) {
				Server.getLogWriter().addToSellList(new Sell(c,
						new GameItem(itemID, amount), value, c.myShopId));
			}
			return true;
		}
		return true;
	}

	/**
	 * Sell item to shop (Shop Price)
	 **/
	public void sellToShopPrice(int removeId, int removeSlot) {
		if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
			return;
			// for (int i : Config.ITEM_SELLABLE) {
			// if (i == removeId) {
			// c.sendMessage("You can't sell "
			// + ItemConstants.getItemName(removeId).toLowerCase()
			// + ".");
			// return;
			// }
			// }
		}

		if (c.myShopId == UntradeableManager.SHOP_ID) {
			c.sendMessage("You can't sell items to the Untradeable Shop.");
			return;
		}

		// if (WorldType.equalsType(WorldType.SPAWN)) {
		// c.sendMessage("You can't sell " +
		// ItemConstants.getItemName(removeId).toLowerCase() + "in SoulPvP.");
		// return;
		// }

		if (c.myShopId == 7390) {
			c.sendMessage("You choose your price when using POS.");
			return;
		}

		if (!ItemProjectInsanity.itemIsSellable(removeId) && (c.myShopId == 60 && removeId != 7458)) {//Lms mithril gloves
			c.sendMessage("You can't sell "
					+ ItemConstants.getItemName(removeId).toLowerCase() + ".");
			return;
		}

		if (c.myShopId == 25 || c.myShopId == 84 || c.myShopId == 102) {
			c.sendMessage("You can't sell to this shop.");
			return;
		}

		if (!canSellToStore(removeId, removeSlot)) {
			c.sendMessage("You can't sell "
					+ ItemConstants.getItemName(removeId).toLowerCase()
					+ " to this store.");
			return;
		}
		
		if (c.myShopId == 60) {
            c.sendMessage("You can trade a " + ItemConstants.getItemName(removeId).toLowerCase() + " for " + ShopPrice.getSurvivalShopSellValue(removeId) + " tokens.");
			 return;
	    }
		
		if (c.myShopId == 61) {
            c.sendMessage("You can sell a " + ItemConstants.getItemName(removeId).toLowerCase() + " for " + ShopPrice.getGabootyShopSellValue(removeId) + " Trading sticks.");
			 return;
	    }

		int ShopValue = (int) Math.floor(getItemShopValue(removeId, 1, removeSlot));
		String ShopAdd = "";
		if (ShopValue >= 1000 && ShopValue < 1000000) {
			ShopAdd = " (" + (ShopValue / 1000) + "K)";
		} else if (ShopValue >= 1000000) {
			ShopAdd = " (" + (ShopValue / 1000000) + " million)";
		}
		if (WorldType.equalsType(WorldType.SPAWN)) {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": shop will buy for " + ShopValue * 75 / 100
					+ " Blood money" + ShopAdd);
		} else {
			c.sendMessage(ItemConstants.getItemName(removeId)
					+ ": shop will buy for " + ShopValue + " coins"
					+ ShopAdd);
		}
	}
	
	public boolean canSellToStore(int id, int slot) {
		if (c.myShopId == SpawnTable.SHOP_ID) {
			return true;
		}
		if (c.myShopId >= 55 && c.myShopId <= 58) {
			int value = (int) getItemShopValue(id, 1, slot);
			if (value == 0) {
				return false;
			}

			return id >= 15750 && id <= 18329;
		}
		
		if (c.myShopId == 60) {
			return ShopPrice.getSurvivalShopSellValue(id) > 0;
		}
		if (c.myShopId == 61) {
			return ShopPrice.getGabootyShopSellValue(id) > 0;
		}

		id++;
		boolean found = false;
		if (ShopHandler.ShopSModifier[c.myShopId] > 1) {
			for (int j = 0; j <= ShopHandler.ShopItemsStandard[c.myShopId]; j++) {
				if (id == ShopHandler.ShopItems[c.myShopId][j]) {
					found = true;
					break;
				}
			}
		} else {
			found = true;
		}

		return found;
	}

	public void setupSkillCapes(int capes, int capes2) {
		// synchronized (c) {
		c.getItems().resetItems(3823);
		c.isShopping = true;
		c.myShopId = 14;
		c.getPacketSender().sendFrame248(3824, 3822);
		c.getPacketSender().sendFrame126("Skillcape Shop", 3901);

		int TotalItems = 0;
		TotalItems = capes2;
		if (TotalItems > ShopHandler.MaxShopItems) {
			TotalItems = ShopHandler.MaxShopItems;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(3900);
		c.getOutStream().writeShort(TotalItems);
		for (int i = 0; i < ShopHandler.skillCapes.length; i++) {
			if (c.getSkills().getStaticLevel(i) < 99) {
				continue;
			}
			c.getOutStream().writeByte(1);
			c.getOutStream().write3Byte(ShopHandler.skillCapes[i] + 2);
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public boolean shopSellsItem(int itemID) {
		for (int i = 0; i < ShopHandler.ShopItems.length; i++) {
			if (itemID == (ShopHandler.ShopItems[c.myShopId][i] - 1)) {
				return true;
			}
		}
		return false;
	}

	public void skillBuy(int item) {
		int nn = get99Count();
		if (nn > 1) {
			nn = 1;
		} else {
			nn = 0;
		}
		for (int j = 0; j < ShopHandler.skillCapes.length; j++) {
			if (ShopHandler.skillCapes[j] == item || ShopHandler.skillCapes[j] + 1 == item) {
				if (c.getItems().freeSlots() > 1) {
					if (c.getItems().playerHasItem(995, 99000)) {
						if (c.getSkills().getStaticLevel(j) >= 99) {
							c.getItems().deleteItemInOneSlot(995,
									c.getItems().getItemSlot(995), 99000);
							c.getItems().addItem(ShopHandler.skillCapes[j] + nn, 1);
							c.getItems().addItem(ShopHandler.skillCapes[j] + 2, 1);
						} else {
							c.sendMessage(
									"You must have 99 in the skill of the cape you're trying to buy.");
						}
					} else {
						c.sendMessage("You need 99k to buy this item.");
					}
				} else {
					c.sendMessage(
							"You must have at least 1 inventory spaces to buy this item.");
				}
			}
		}
		c.getItems().resetItems(3823);
	}

	public void updatePlayerShop() {
		for (int i = 1; i < Config.MAX_PLAYERS; i++) {
			if (PlayerHandler.players[i] != null) {
				if (PlayerHandler.players[i].isShopping == true
						&& PlayerHandler.players[i].myShopId == c.myShopId
						&& i != c.getId()) {
					PlayerHandler.players[i].updateShop = true;
				}
			}
		}
	}

	public void updateshop(int i) {
		resetShop(i);
	}

}
