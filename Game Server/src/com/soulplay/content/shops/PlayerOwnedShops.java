package com.soulplay.content.shops;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.minigames.duel.TradeAndDuel;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

public class PlayerOwnedShops {

	public static void addItemToPlayerOwnedShop(Client c, int itemId,
			int amount, int itemSlot) {
		c.sellingIdOriginal = itemId;
		ItemDef def = ItemDef.forID(itemId);
		if (def != null && def.itemIsInNotePosition) {
			itemId = def.getNoteId();
		}
		// c.sendMess("xinterfaceID:"+c.xInterfaceId);
		long priceSet = PlayerOwnedShops.getPriceOfShopItem(c, itemId);
		if (priceSet < 1) { // if they did not set the item in the shop yet
			c.sellingId = itemId;
			c.sellingN = 1;// amount;
			c.sellingS = itemSlot;
			c.xInterfaceId = 3823; // adds items from own inventory interface
									// which is 3823
//			c.getPA().setInputDialogState(PlayerConstants.SET_SELL_PRICE_STATE);
			if (!c.getMarketOffer().isActive()) {
				c.getMarketOffer().openSellOffer(c.sellingIdOriginal);
				int amountReal = c.getItems().getItemAmountInSlot(itemSlot);
				if (amount > amountReal)
					amount = amountReal;
				c.getMarketOffer().setQuantity(amount);
			}
		} else { // item is already in shop, so stack it into own shop
			c.sellingId = itemId;
			c.sellingN = amount;
			c.sellingS = itemSlot;
			if (amount == 777) {
				// c.xInterfaceId = 7390;
				c.getPacketSender().sendEnterAmountInterface(); // enter amount
																// to sell
			} else {
				restockMyShop(c, amount, priceSet);
			}
		}
	}

	public static int[] fixArray(int[] array) {
		int arrayPos = 0;
		int[] newArray = new int[array.length];
		for (int element : array) {
			if (element != 0) {
				newArray[arrayPos] = element;
				arrayPos++;
			}
		}
		return newArray;
	}

	public static long[] fixArray(long[] array) {
		int arrayPos = 0;
		long[] newArray = new long[array.length];
		for (long element : array) {
			if (element != 0) {
				newArray[arrayPos] = element;
				arrayPos++;
			}
		}
		return newArray;
	}

	public static void fixShop(Client o) {
		o.playerShopItems = fixArray(o.playerShopItems);
		o.playerShopItemsN = fixArray(o.playerShopItemsN);
		o.playerShopItemsPrice = fixArray(o.playerShopItemsPrice);
	}

	public static int getItemInShopSlot(Client c, int itemId) {
		for (int x = 0; x < c.playerShopItems.length; x++) {
			if (c.playerShopItems[x] == itemId) {
				return x;
			}
		}
		return -1;
	}

	// public static void openShop(Client c, Client shopOwner) {
	// if (!DBConfig.ADMIN_CAN_SELL_ITEMS && (c.playerRights == 2 ||
	// shopOwner.playerRights == 2)) {
	// c.sendMessage("Can't do that.");
	// return;
	// }
	// if (c.difficulty == PlayerDifficulty.IRONMAN || shopOwner.difficulty ==
	// PlayerDifficulty.IRONMAN) {
	// c.sendMessage("Ironmen cannot use shop");
	// return;
	// }
	// c.getItems().resetItems(3823);
	// resetShop(c, shopOwner);
	// c.isShopping = true;
	// c.myShopId = 7390;
	// c.getPA().sendFrame248(3824, 3822);
	// c.getPA().sendFrame126(shopOwner.getName()+"'s Shop", 3901);
	// }

	// public static void updatePlayerShop() {
	// for (int i = 1; i < Config.MAX_PLAYERS; i++) {
	// if (PlayerHandler.players[i] != null) {
	// if (PlayerHandler.players[i].isShopping == true
	// && PlayerHandler.players[i].myShopId == c.myShopId
	// && i != c.playerId) {
	// Client sh = (Client) PlayerHandler.players[i];
	// //sh.getShops().openShop(sh.myShopId);
	// PlayerHandler.players[i].updateShop = true;
	// }
	// }
	// }
	// }

	public static long getPriceOfShopItem(Client c, int itemId) {
		for (int x = 0; x < c.playerShopItems.length; x++) {
			if (c.playerShopItems[x] == itemId) {
				return c.playerShopItemsPrice[x];
			}
		}
		return -1;
	}

	public static void openPlayerShop(Client c, Client shopOwner) {
		if (shopOwner == null || !shopOwner.isActive || shopOwner.properLogout
				|| shopOwner.disconnected) {
			return;
		}
		if (!c.inMarket()) {
			return;
		}
		// if (!shopOwner.inMarket())
		// return;
		if (!DBConfig.ADMIN_CAN_SELL_ITEMS
				&& (c.playerRights == 2 || shopOwner.playerRights == 2)) {
			c.sendMessage("Can't do that.");
			return;
		}
		if (c.isIronMan()
				|| shopOwner.isIronMan()) {
			c.sendMessage("Ironmen cannot use shop");
			return;
		}
		c.getItems().resetItems(3823);
		resetShop(c, shopOwner);
		c.myShopClient = shopOwner;
		c.myShopId = 7390;
		c.isShopping = true;
		c.getPacketSender().sendFrame248(3824, 3822);
		c.getPacketSender().sendFrame126(shopOwner.getNameSmartUp() + "'s Shop", 3901);
		if (c.equals(shopOwner)) {
			c.getPacketSender().sendConfig(ConfigCodes.CHANGE_BUY_1, 1);
		}
	}

	/**
	 * Sets the price of the item to sell
	 *
	 * @param c
	 *            - player shop owner
	 * @param xAmount
	 *            - sell price
	 */
	public static void placeFirstItemWithSetPrice(Client c, long xAmount) {
		if (!TradeAndDuel.allowedToTradeItem(c, c.sellingId, false)) {
			c.sendMessage("You can't sell non-tradeables.");
			c.resetMyShopSellingVars();
			return;
		}
		
		if (!ItemProjectInsanity.itemIsSellable(c.sellingId)) {
			c.sendMessage("You can't sell this item.");
			c.resetMyShopSellingVars();
			return;
		}

		if (xAmount < 1) {
			c.sendMessage(
					"You can't sell items for 0 gold. Please do not go over 2B");
			c.resetMyShopSellingVars();
			return;
		}

		for (int i : DBConfig.DUNG_REWARDS) {
			if (i == c.sellingId) {
				c.sendMessage("You can't sell this item.2");
				c.resetMyShopSellingVars();
				return;
			}
		}

		int slot = -1;
		for (int x = 0; x < c.playerShopItems.length; x++) {
			if (c.playerShopItems[x] == 0) {
				slot = x;
				break;
			}
		}
		if (slot == -1) {
			c.sendMessage("You can only be selling " + c.playerShopSlots
					+ " items at once!");
			c.resetMyShopSellingVars();
			return;
		}

		if (c.playerShopItems.length < c.playerShopSlots) {
			c.sendMessage("Please re-log for shop size effects to take place.");
			return;
		}

		if (c.getItems().playerHasItem(c.sellingIdOriginal, c.sellingS, 1)) {
			c.getItems().deleteItem2(c.sellingIdOriginal, 1);
			c.playerShopItems[slot] = c.sellingId;
			c.playerShopItemsN[slot] = 1;
			c.playerShopItemsPrice[slot] = xAmount;
			c.sendMessage("You put your item on sale for "
					+ Misc.format(xAmount) + " Gold.");
		}
		PlayerOwnedShops.openPlayerShop(c, c);
		PlayerOwnedShops.updatePlayerOwnedShop(c);

		c.resetMyShopSellingVars();

	}
	
	public static void placeFirstItemWithSetPrice(Client c, int itemId, int amount, long price, int notNotedId) {
		if (!ItemProjectInsanity.itemIsTradeable(itemId)) {
			c.sendMessage("You can't sell non-tradeables.");
			c.resetMyShopSellingVars();
			return;
		}

		if (price < 1) {
			c.sendMessage(
					"You can't sell items for 0 gold. Please do not go over 2B");
			c.resetMyShopSellingVars();
			return;
		}

		for (int i : DBConfig.DUNG_REWARDS) {
			if (i == itemId) {
				c.sendMessage("You can't sell this item.2");
				c.resetMyShopSellingVars();
				return;
			}
		}

		int slot = -1;
		for (int x = 0; x < c.playerShopItems.length; x++) {
			if (c.playerShopItems[x] == 0) {
				slot = x;
				break;
			}
		}
		if (slot == -1) {
			c.sendMessage("You can only be selling " + c.playerShopSlots
					+ " items at once!");
			c.resetMyShopSellingVars();
			return;
		}

		if (c.playerShopItems.length < c.playerShopSlots) {
			c.sendMessage("Please re-log for shop size effects to take place.");
			return;
		}
		
		int itemAmount = c.getItems().itemAmount(itemId+1);
		
		if (amount > itemAmount)
			amount = itemAmount;

		if (c.getItems().deleteItem2(itemId, amount)) {
			c.playerShopItems[slot] = notNotedId;
			c.playerShopItemsN[slot] = amount;
			c.playerShopItemsPrice[slot] = price;
			c.sendMessage("You put your item on sale for "
					+ Misc.format(price) + " Gold.");
		} else {
			c.sendMessage("You don't have that itemID:"+itemId+" amount:"+amount);
		}
		PlayerOwnedShops.openPlayerShop(c, c);
		PlayerOwnedShops.updatePlayerOwnedShop(c);

		c.resetMyShopSellingVars();

	}

	public static void resetShop(Client c, Client shopOwner) {
		// synchronized (c) {
		fixShop(shopOwner);
		for (int x = 0; x < shopOwner.playerShopItems.length; x++) {
			if (shopOwner.playerShopItemsN[x] <= 0) {
				shopOwner.playerShopItems[x] = 0;
			}
		}
		int TotalItems = 0;
		for (int playerShopItem : shopOwner.playerShopItems) {
			if (playerShopItem > 0) {
				TotalItems++;
			}
		}
		if (TotalItems > shopOwner.playerShopItems.length) {
			TotalItems = shopOwner.playerShopItems.length;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(3900);
		c.getOutStream().writeShort(TotalItems);
		int TotalCount = 0;
		for (int i = 0; i < shopOwner.playerShopItems.length; i++) {
			if (shopOwner.playerShopItems[i] > 0) {
				if (shopOwner.playerShopItemsN[i] > 254) {
					c.getOutStream().writeByte(255);
					c.getOutStream()
							.writeDWord_v2(shopOwner.playerShopItemsN[i]);
				} else {
					c.getOutStream().writeByte(shopOwner.playerShopItemsN[i]);
				}
				c.getOutStream().write3Byte(
						(shopOwner.playerShopItems[i] + 1));
				TotalCount++;
			}
			if (TotalCount > TotalItems) {
				break;
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public static void restockMyShop(Client c, int xAmount, long price) {
		if (!ItemProjectInsanity.itemIsTradeable(c.sellingId)) {
			c.sendMessage("You can't sell non-tradeables..");
			c.resetMyShopSellingVars();
			return;
		}
		c.sellingN = xAmount;
		// if (xAmount < 1) {
		// c.sendMessage("You can't sell items for 0 gold. Please do not go over
		// 2B");
		// c.resetMyShopSellingVars();
		// return;
		// }

		for (int i : DBConfig.DUNG_REWARDS) {
			if (i == c.sellingId) {
				c.sendMessage("You can't sell this item...");
				c.resetMyShopSellingVars();
				return;
			}
		}
		if (c.sellingN > c.getItems().getItemAmount(c.sellingIdOriginal)) {
			c.sellingN = c.getItems().getItemAmount(c.sellingIdOriginal);
		}

		// if (!Item.itemIsStackable(c.sellingId) && c.sellingN > 1) {
		// c.sellingN = 1;
		// c.sendMessage("<col=800000>You can only sell 1 of unstackable items.");
		// }

		// for(int x = 0; x < c.playerShopItems.length; x++) {
		// if(c.playerShopItems[x] == c.sellingId) {
		// c.sendMessage("This item is already in the shop. You must relist that
		// Item.");
		// c.resetMyShopSellingVars();
		// return;
		//
		// }
		// }

		// int slot = -1;
		// for(int x = 0; x < c.playerShopItems.length; x++) {
		// if(c.playerShopItems[x] == 0){
		// slot = x;
		// break;
		// }
		// }
		// if(slot == -1){
		// c.sendMessage("You can only be selling "+c.playerShopSlots+" items at
		// once!");
		// c.resetMyShopSellingVars();
		// return;
		// }

		int slot = PlayerOwnedShops.getItemInShopSlot(c, c.sellingId);
		if (slot == -1) { // we stack items here, so if this is -1 taht means
							// something is wrong
			c.sendMessage("Error Code: 6328 - Report this to the staff.");
			c.resetMyShopSellingVars();
			return;
		}
		if (c.playerShopItems.length < c.playerShopSlots) {
			c.sendMessage("Please re-log for shop size effects to take place.");
			return;
		}

		// if (c.getItems().playerHasItem(c.sellingId, c.sellingN))
		// {//if(c.getItems().playerHasItem(c.sellingId, c.sellingS,
		// c.sellingN)) {
		if (c.getItems().deleteItem2(c.sellingIdOriginal, c.sellingN)) {
			c.playerShopItems[slot] = c.sellingId;
			c.playerShopItemsN[slot] += c.sellingN;
			c.playerShopItemsPrice[slot] = price;
			c.sendMessage("You put your items on sale.");
		}
		// }
		PlayerOwnedShops.openPlayerShop(c, c);
		PlayerOwnedShops.updatePlayerOwnedShop(c);

		c.resetMyShopSellingVars();
	}

	public static void updatePlayerOwnedShop(Client shopOwner) {
		if (shopOwner == null) {
			return;
		}
		for (int i = 1; i < Config.MAX_PLAYERS; i++) {
			if (PlayerHandler.players[i] != null) {
				if (PlayerHandler.players[i].myShopClient != null) {
					if (shopOwner
							.equals(PlayerHandler.players[i].myShopClient)) {
						final Client sh = (Client) PlayerHandler.players[i];
						openPlayerShop(sh, shopOwner);
					}
				}
			}
		}
	}

}
