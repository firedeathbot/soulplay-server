package com.soulplay.content.shops;

import com.soulplay.game.model.item.GameItem;

public class Buy {

	String buyer;

	private int mysqlIndex;

	GameItem item;

	long price;

	int shop;

	public Buy(String buyer, int sqlIndex, GameItem item, long price,
			int shop) {
		this.buyer = buyer;
		this.item = item;
		this.price = price;
		this.shop = shop;
		this.setMysqlIndex(sqlIndex);

	}

	public int buyerMID() {
		return mysqlIndex;
	}

	public String getBuyer() {
		return buyer;
	}

	public GameItem getItem() {
		return item;
	}

	public long getPrice() {
		return price;
	}

	public int getShop() {
		return shop;
	}

	public void setMysqlIndex(int mysqlIndex) {
		this.mysqlIndex = mysqlIndex;
	}

}
