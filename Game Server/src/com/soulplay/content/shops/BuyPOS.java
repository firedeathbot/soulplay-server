package com.soulplay.content.shops;

import com.soulplay.game.model.item.GameItem;

public class BuyPOS extends Buy {

	String seller;

	private int sellerMID;

	public BuyPOS(String buyer, int buyerMID, String seller, int sellerMID,
			GameItem item, long price, int shop) {
		super(buyer, buyerMID, item, price, shop);
		this.seller = seller;
		this.setSellerMID(sellerMID);
	}

	public String getSeller() {
		return seller;
	}

	public int getSellerMID() {
		return sellerMID;
	}

	public void setSellerMID(int sellerMID) {
		this.sellerMID = sellerMID;
	}
}
