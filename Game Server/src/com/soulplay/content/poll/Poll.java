package com.soulplay.content.poll;

public class Poll {

	public static final int ENTRY_COUNT = 10;

	private final int id;
	private final String title;
	private String info;
	private final PollEntry[] entries = new PollEntry[ENTRY_COUNT];
	private int pos;

	public Poll(int id, String title, String info) {
		this.id = id;
		this.title = title;
		this.info = info;
	}

	public void addEntry(PollEntry entry) {
		entries[pos++] = entry;
	}

	public PollEntry getEntry(int index) {
		return entries[index];
	}

	public int getPos() {
		return pos;
	}

	public int getId() {
		return id;
	}

	public String getInfo() {
		return info;
	}
	
	public void setInfo(String str) {
		this.info = str;
	}

	public String getTitle() {
		return title;
	}

}