package com.soulplay.content.poll;

public class PollEntry {

	private final int id;
	private final String question;
	private int yesAmount, noAmount, skipAmount;

	public PollEntry(int id, String question, int yesAmount, int noAmount, int skipAmount) {
		this.id = id;
		this.question = question;
		this.yesAmount = yesAmount;
		this.noAmount = noAmount;
		this.skipAmount = skipAmount;
	}

	public int getId() {
		return id;
	}

	public String getQuestion() {
		return question;
	}

	public int getYesAmount() {
		return yesAmount;
	}
	
	public void addYes() {
		yesAmount++;
	}

	public int getNoAmount() {
		return noAmount;
	}
	
	public void addNo() {
		noAmount++;
	}

	public int getSkipAmount() {
		return skipAmount;
	}
	
	public void addSkip() {
		skipAmount++;
	}
	
	public void resetCount() {
		this.yesAmount = this.noAmount = this.skipAmount = 0;
	}

}
