package com.soulplay.content.poll;

import java.util.Map.Entry;

import com.soulplay.Server;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.sql.vote.VotePollSql;

public class PollManager {
	
	public static boolean pollOpen = true;
	
	public static final int configCode = 8050;
	
	public static final int MAX_POLLS = 10;
	public static final Poll[] polls = new Poll[MAX_POLLS];
	
	public static final int RADIO_BUTTON_START = 230143;
	
	public static final int INTERFACE_ID = 59000;
	
	public static String pollInfo = "In this poll we'd like to know your opinion about update suggestions. Answer these question so we can know what the community are interested in.";
	
	public static void open(Client c) {
		Poll toOpen = polls[0];
		c.getPacketSender().sendVoteData(toOpen);
		c.getPacketSender().showInterface(INTERFACE_ID);
		c.getPacketSender().setScrollPos(59010, 0);
		loadPlayerVoted(c, toOpen);
	}
	
	private static void loadPlayerVoted(Player p, final Poll toOpen) {
		if (p.votePollLoading)
			return;
		p.votePollLoading = true;
		//TODO: add visual to display that the vote polls are loading
		p.votedInPoll = 1;
		final String ip = p.connectedFrom;
		final String uuid = p.UUID;
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				if (p.disconnected)
					return;
				p.pollData = new byte[toOpen.getPos()];
				for (int i = 0; i < toOpen.getPos(); i++) {
					PollEntry entry = toOpen.getEntry(i);
					p.pollData[i] = (byte) VotePollSql.alreadyVoted(entry.getId(), ip, uuid);
					if (p.pollData[i] == -1)
						p.votedInPoll = 0;
				}
				Server.getTaskScheduler().schedule(new Runnable() {
					
					@Override
					public void run() {
						
						if (p.isActive && p.pollData != null) {
							for (int i = 0; i < p.pollData.length; i++) {
								p.getPacketSender().sendConfig(configCode + i, p.pollData[i]+1);
							}
							p.votePollLoading = false;
							//TODO: add visual to display that the vote polls are finished loading
						}
					}
				});
			}
		});
	}

	public static void loadPolls() {
		//SQL loading here
//		Poll poll = new Poll(0, "Soulplay poll!", "In this poll we'd like to know your opinion about update suggestions. Answer these question so we can know what the community are interested in.");
//		poll.addEntry(new PollEntry(0, "Should we add ornamet kits to upgrade godswords and dragon scimitar and other?", 0, 0, 0));
//		poll.addEntry(new PollEntry(1, "Should we add other colors for the serp helm such as taz and magma?", 0, 0, 0));
//		poll.addEntry(new PollEntry(2, "Should we add the new OSRS boss vorkath?", 0, 0, 0));
//		poll.addEntry(new PollEntry(3, "Should hybrid armor cost be reduced from CW shop to same price as SW shop?", 0, 0, 0));
//		poll.addEntry(new PollEntry(4, "Should there be a interface where players can block up to 3 players from attacking them in the wilderness?", 0, 0, 0));
//		polls[0] = poll;
		reloadAllVoteEntries();
	}
	
	public static void radioButtonClick(Player c, int buttonId) {
		if (c.pollData == null) {
			return;
		}
		
		if (c.votePollLoading) {
			c.sendMessage("Please wait while vote poll is loading.");
			return;
		}

		if (buttonId >= 231000) {
			buttonId -= 1000;
			buttonId += 256;
		}
		int newId = buttonId - RADIO_BUTTON_START;

		int questionId = newId / 17;
		int optionId = newId % 17;
		c.pollData[questionId] = (byte) optionId;
	}
	
	public static void voteButtonClick(Player p) {
		
		if (p.votePollLoading) {
			p.sendMessage("Please wait while vote poll is loading.");
			return;
		}
		
		if (p.votedInPoll == 1) {
			p.sendMessage("You've already voted in this poll.");
			return;
		}
		
		if (pollOpen == false) {
			p.sendMessage("The poll is currently closed for voting.");
			return;
		}

		if (p.getHoursPlayed() < 5 && (p.getPA().getTotalLevel() < 1500 && p.difficulty == PlayerDifficulty.NORMAL)) {
			p.sendMessage("You need to have a total level of at least 1500 and more than 5 hours of play-time.");
			return;
		}

		for (int i = 0, length = p.pollData.length; i < length; i++) {
			int value = p.pollData[i];
			if (value == -1) {
				p.sendMessage("You must answer all poll questions to vote.");
				return;
			}
		}

		addVoteToDatabase(p.pollData, p.connectedFrom, p.UUID);
		

		if (!(p.playerRights == 3))
		    p.votedInPoll = 1;
		p.getPA().closeAllWindows();
		p.sendMessage("You have voted in the poll!");
		p.getDialogueBuilder().sendStatement("Thank you for voting!", "You've gained 15 minutes of Double XP for voting.").execute();
		p.setDoubleExpLength(p.getDoubleExpLength() + 1500);
	}
	
	private static void addVoteToDatabase(final byte[] pollData, final String ip, final String uuid) {
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				
				boolean addedVote = false;
				
				for (int i = 0, length = pollData.length; i < length; i++) {
					int value = pollData[i];
					PollEntry entry = polls[0].getEntry(i);
					int entryId = entry.getId();
					if (!VotePollSql.voteEntryOpen(entryId) || VotePollSql.alreadyVoted(entryId, ip, uuid) != -1)
						continue;
					VotePollSql.addVoteToVoteTable(entryId, ip, uuid, value);
					addedVote = true;
				}
				
				if (addedVote)
					reloadVoteCount();
				
			}
		});
		
	}
	
	public static void reloadAllVoteEntries() {
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				
				VotePollSql.grabVoteEntries();
				
				reloadVoteCount();
				
				Server.getTaskScheduler().schedule(new Task() {
					
					@Override
					protected void execute() {
						this.stop();
						
						for (Entry<Long, Player> entry : PlayerHandler.getPlayerMap().entrySet()) {
							Player p = entry.getValue();
							if (!p.disconnected) {
								p.votedInPoll = 0;
								
								if (p.interfaceIdOpenMainScreen == INTERFACE_ID) {
									p.getPA().closeAllWindows();
									p.sendMessage("<col=800000>The vote poll has been updated. Reopen the vote poll.");
								}
							}
						}
						
					}
				});
				
			}
		});
	}
	
	public static void reloadVoteCount() {
		TaskExecutor.executeSQL(new Runnable() {

			@Override
			public void run() {

				Poll toOpen = polls[0];
				for (int i = 0; i < toOpen.getPos(); i++) {
					PollEntry entry = toOpen.getEntry(i);
					
					VotePollSql.countEntry(entry);
				}

			}
		});
	}

}
