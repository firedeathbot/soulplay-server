package com.soulplay.content.minigames.championsguild;

import java.util.HashMap;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;

public class ChampionsGuild {

	/**
	 * Todo: jaxidor/aleks - Correct stats for each of them - The rest of the
	 * rules - The rest of what's not allowed in each fight
	 **/
	/**
	 * Handles the data for the champions (Scroll id, champion NPC id and the
	 * text for the challenge interface)
	 **/

	public static enum Champions {

		LESSER_DEMON(6805, 3064,
				new String[][]{{"Champion of Lesser Demons"},
						{"Come to the Champions' Guild so I can banish"},
						{"you mortal!"}}),
		GOBLIN(6801, 3060,
				new String[][]{{"Champion of Goblins"},
						{"Fight me if you think you can, human. I'll wait for"},
						{"you in the Champions' Guild."}}),
		HOBGOBLIN(6802, 3061,
				new String[][]{{"Champion of Hobgoblins"},
						{"You won't defeat me, though you're welcome to"},
						{"try at the Champions' Guild."}}),
		IMP(6803, 3062,
				new String[][]{{"Champion of Imps"},
						{"'Ow about picking on some'un yer own size? I'll"},
						{"see you at the Champions' Guild, guv."}}),
		SKELETON(6806, 3065,
				new String[][]{{"Champion of Skeletons"},
						{"I'll be waiting at the Champions' Guild to collect"},
						{"your bones."}}),
		ZOMBIE(6807, 3066,
				new String[][]{{"Champion of Zombies"},
						{"You come to Champions' Guild, you fight me, I"},
						{"squish you, I get brains!"}}),
		GIANT(6800, 3058,
				new String[][]{{"Champion of Giants"},
						{"Get yourself to the Champions' Guild, if you dare"},
						{"to face me, puny human."}}),
		BANSHEE(15357, 8986,
				new String[][]{{"Champion of Banshees"},
						{"Come duel me at the Champions' GUild and I shall"},
						{"scream to your defeat."}}),
		GHOUL(6799, 3059,
				new String[][]{{"Champion of Ghouls"},
						{"Come duel me at the Champions' Guild. I'll"},
						{"make sure nothing goes to waste."}}),
		ABERRANTS_PECTRE(15356, 8987,
				new String[][]{{"Champion of Aberrant Spectres"},
						{"I smell defeat for you if you dare join me for a"},
						{"fight at the Champions' Guild."}}),
		EARTH_WARRIOR(6798, 3057, new String[][]{{"Champion of Earth Warriors"},
				{"I challenge you to a duel, come to the arena"},
				{"beneath the Champions' Guild and fight me if you dare."}}),
		JOGRE(6804, 3063,
				new String[][]{{"Champion of Jogres"},
						{"You think you can defeat me? Come to the"},
						{"Champions' Guild and prove it!"}}),
		MUMMY(15355, 8988,
				new String[][]{{"Champion of Mummies"},
						{"I challenge you to a fight! Meet me at the"},
						{"Champions' Guild so we can wrap this up."}});

		private static HashMap<Integer, Champions> champions = new HashMap<>();

		static {
			for (Champions c : Champions.values()) {
				champions.put(c.getItemId(), c);
			}
		}
		
	    public static void load() {
	    	/* empty */
	    }

		private static Champions forId(int id) {
			return champions.get(id);
		}

		private int itemId;

		private int npcId;

		private String interfaceText[][];

		private Champions(int itemId, int npcId, String[][] interfaceText) {
			this.itemId = itemId;
			this.npcId = npcId;
			this.interfaceText = interfaceText;
		}

		private String getInterfaceText(int row) {
			return interfaceText[row][0];
		}

		private int getItemId() {
			return itemId;
		}

		private int getNpcId() {
			return npcId;
		}

	}

	/**
	 * The arena rules Every rule is unique to each champion For example assign
	 * a champion rule 0, being no other attack style allowed other than magic
	 * Usage: Scroll ID, Rule ID
	 **/

	private static int[][] rules = {{6802, 0}};

	/**
	 * Giving the player a reward after killing a champion Using the data from
	 * the reward ints
	 **/

	public static void addReward(final Client c) {
	}

	/**
	 * Handles the rest of the dialogue telling you what you're allowed to use
	 * in the arena Unique to each champion
	 **/

	public static String getRestOfDialogue(Client c, int part) {
		Champions champion = Champions.forId(c.championScrollUsed);
		switch (part) {
			case 0:
				switch (champion.getItemId()) {
					case 6801:
						return "you're only allowed to use Magic Attacks, no";
					default:
						return "None";
				}
			case 1:
				switch (champion.getItemId()) {
					case 6801:
						return "Melee or Ranged.";
					default:
						return "None";
				}
			default:
				return "None";
		}
	}

	/**
	 * Fetch the rule ID from the array Prevents people from using magic if the
	 * champion doesn't allow it
	 **/

	private static int getRuleID(int champion) {
		for (int i = 0; i < rules.length; i++) {
			if (i == champion) {
				return rules[i][1];
			}
		}
		return 0;
	}

	/**
	 * Checking if the item is a champion scroll Used when clicking the scroll
	 **/

	public static boolean isChampion(int itemId) {
		Champions champion = Champions.forId(itemId);
		if (champion != null) {
			return true;
		}
		return false;
	}

	/**
	 * Opens the champion challenge interface Sends the 'invitation' text lines
	 * which also are unique to each NPC
	 **/

	public static void openChallenge(Client c, int itemId) {
		Champions champion = Champions.forId(itemId);
		c.getPacketSender().showInterface(1136);
		c.getPacketSender().sendFrame126(champion.getInterfaceText(1), 1139);
		c.getPacketSender().sendFrame126(champion.getInterfaceText(2), 1140);
		c.getPacketSender().sendFrame126(champion.getInterfaceText(0), 1142);
	}

	/**
	 * Spawning the champion in the arena
	 **/

	public static void spawnChampion(final Client c) {
		final Champions champ = Champions.forId(c.championScrollUsed);
		final int x = 3175; // x
		final int y = 9758; // y
		c.championRules[getRuleID(
				Champions.forId(c.championScrollUsed).getItemId())] = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				NPCHandler.spawnNpc(c, champ.getNpcId(), x, y, 0, 0,
						true,
						false);
				container.stop();
			}

			@Override
			public void stop() {
			}
		}, 10);
	}
}
