package com.soulplay.content.minigames.flowers;

import com.soulplay.content.objects.impl.Flower;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

public class FlowerGame {

	public static int getRandomFlowerObject() {
		int ran = Misc.random(Flower.values.length - 1 - 2); // -2 to get rid
																// of the black
																// and white
		if (Misc.random(777) == 7) {
			if (Misc.random(1) == 0) { // black
				return Flower.BLACK_FLOWERS.getObjectId();
			} else { // white
				return Flower.WHITE_FLOWERS.getObjectId();
			}
		}
		return Flower.values[ran].getObjectId();
	}

	public static void pickFlower(Client c) {
		RSObject o = c.interactingObject;
		if (o != null && o.getTick() > 0) {
			c.interactingObject = null;
			for (Flower f : Flower.values) {
				if (f.getObjectId() == o.getNewObjectId()) {
					o.setTick(0);
					c.getItems().addItem(f.getItemId(), 1);
					c.startAnimation(827);
					break;
				}
			}
		}
		o = null;
		c.getPA().closeAllWindows();
	}

	public static void plantFlower(Client c) {
		if (c.inMinigame()) {
			c.sendMessage("You cannot do that here.");
			return;
		}
		c.getItems().deleteItemInOneSlot(299, 1);
		c.faceLocation(c.getX(), c.getY());
		c.getPA().forceStepAside();
		final int objectId = FlowerGame.getRandomFlowerObject();
		c.interactingObject = new RSObject(objectId,
				c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
		ObjectManager.addObject(c.interactingObject);
//		c.getDH().sendOption2("Pick the flowers.", "Leave the flowers.");
//		c.dialogueAction = 299;
		
		c.getDialogueBuilder().sendOption("Pick the flowers", ()-> {
			pickFlower(c);
		}, "Leave the flowers", ()-> {
			c.getDialogueBuilder().closeNew();
		}).executeIfNotActive();
		
		c.getGambleLogger().logFlower(objectId);
		
	}

}
