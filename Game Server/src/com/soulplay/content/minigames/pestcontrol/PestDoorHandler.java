package com.soulplay.content.minigames.pestcontrol;

import com.soulplay.cache.ObjectDef;
import com.soulplay.content.minigames.PestControl;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;

public class PestDoorHandler {

	public static class PestDoor {

		int index = -1;

		int doorId;

		int doorX;

		int doorY;

		int doorZ;

		int HP;

		int face;

		boolean open;

		int neighborX;

		int neighborY;

		int neighborIndex;

		public PestDoor(int index, int id, int x, int y, int z, int face,
				int hp, int neighhborX, int neighborY) {
			this.index = index;
			this.doorId = id;
			this.doorX = x;
			this.doorY = y;
			this.doorZ = z;
			this.face = face;
			this.HP = hp;
			this.neighborX = neighhborX;
			this.neighborY = neighborY;
			doors[index] = this;
		}

		public int getX() {
			return doorX;
		}

		public int getY() {
			return doorY;
		}
	}

	private static PestDoor[] doors = new PestDoor[6];

	private static /* final */ int[][] openOffsets = {{-1, 0}, // flipped and
																// inverted
			{0, 1}, // moved to side
			{1, 0}, // flipped and inverted
			{0, -1} // moved to side
	};

	private static /* final */ int[][] closeOffsets = {{0, -1}, {-1, 0},
			{0, -1}, {-1, 0}};

	public final static int ticks = PestControl.GAME_TIMER;

	public static void clickDoor() {

	}

	public static void createNewDoors() {
		doors = new PestDoor[6];
		new PestDoor(0, 14233, 2643, 2593, 0, 0, 10, 2643, 2592);
		new PestDoor(1, 14235, 2643, 2592, 0, 0, 10, 2643, 2593);

		new PestDoor(2, 14233, 2656, 2585, 0, 3, 10, 2657, 2585);
		new PestDoor(3, 14235, 2657, 2585, 0, 3, 10, 2656, 2585);

		new PestDoor(4, 14233, 2670, 2592, 0, 2, 10, 2670, 2593);
		new PestDoor(5, 14235, 2670, 2593, 0, 2, 10, 2670, 2592);

	}

	public static PestDoor getDoor(int x, int y, int z) {
		for (PestDoor d : doors) {
			if (d == null) {
				continue;
			}
			if (d.getX() == x && d.getY() == y && d.doorZ == z) {
				return d;
			}
		}
		return null;
	}

	public static void initializeDoors(Client c, int id, int x, int y, int z,
			ObjectDef def) {
		GameObject o = RegionClip.getGameObject(id, x, y, z);

		if (o == null) {
			c.sendMessage("Door is null");
			return;
		}

		PestDoor p = getDoor(x, y, z);
		if (p == null) {
			c.sendMessage("Pest Door is null");
			return;
		}
		
		PestDoor p2 = getDoor(p.neighborX, p.neighborY, z);
		if (p2 == null) {
			c.sendMessage("Pest Door2 is null");
			return;
		}

		// closing doors
		RSObject doorToReverse = ObjectManager.getObject(x, y, z);
		if (doorToReverse != null) {

			RSObject r2 = ObjectManager.getObject(p.neighborX, p.neighborY,
					p.doorZ);
			if (r2 == null) {
				return;
			}
			
			RSObject r3 = ObjectManager.getObject(doorToReverse.getOriginalX(),
					doorToReverse.getOriginalY(), doorToReverse.getHeight());

			RSObject r4 = ObjectManager.getObject(r2.getOriginalX(),
					r2.getOriginalY(), r2.getHeight());
			if (r3 == null) {
				return;
			}
			if (r4 == null) {
				return;
			}
			
			p2.doorX = p.neighborX = r3.getX();
			p2.doorY = p.neighborY = r3.getY();
			p.doorX = p2.neighborX = r4.getX();
			p.doorY = p2.neighborY = r4.getY();
			
			doorToReverse.setTick(0);
			r2.setTick(0);
			r3.setTick(0);
			r4.setTick(0);
			return;
		}

		// else opening doors
		GameObject o2 = RegionClip.getGameObject(p.neighborX, p.neighborY,
				p.doorZ, o.getType(), true);
		if (o2 == null) {
			return;
		}

		int doorStatus; // -1 = close; 1 = open;
		if (def.itemActions[0].equals("Open")) {
			doorStatus = 1;
		} else {
			doorStatus = -1;
		}

		RSObject rsO1 = new RSObject(-1, x, y, z, o.getOrientation(), o.getType(), o.getId(), ticks); // removes
																				// only
																				// the
																				// clicked
																				// door
		RSObject rsO2 = new RSObject(-1, o2.getX(), o2.getY(), z, o2.getOrientation(), o2.getType(),
				o2.getId(), ticks);

		ObjectManager.addObject(rsO1);
		ObjectManager.addObject(rsO2);
		rsO2.setObjectClass(3);
		rsO1.setObjectClass(3);

		if (o.getId() < o2.getId() /* rightDoor(o, o2) */) {
			// c.sendMessage("You clicked on Right door.");
			// Right Door // DONE!!!!
			RSObject rsO3 = RSObject.createDoor(o.getId() + doorStatus,
					o.getX() + (doorStatus == 1
							? openOffsets[o.getOrientation()][0]
							: closeOffsets[o.getOrientation()][0]),
					o.getY() + (doorStatus == 1
							? openOffsets[o.getOrientation()][1]
							: closeOffsets[o.getOrientation()][1]),
					z, o.getOrientation() + doorStatus, o.getType(), -1, o.getX(),
					o.getY(), ticks);

			// Left Door
			RSObject rsO4 = RSObject.createDoor(o2.getId() + doorStatus,
					o2.getX() + (doorStatus == 1
							? openOffsets[o2.getOrientation()][0]
							: closeOffsets[o2.getOrientation()][0]),
					o2.getY() + (doorStatus == 1
							? openOffsets[o2.getOrientation()][1]
							: closeOffsets[o2.getOrientation()][1]),
					z, o2.getOrientation() + doorStatus + 2, o2.getType(), -1,
					o2.getX(), o2.getY(), ticks);
			
			ObjectManager.addObject(rsO3);
			ObjectManager.addObject(rsO4);

			rsO3.setObjectClass(3);
			rsO4.setObjectClass(3);
			
			p2.neighborX = p.doorX = rsO3.getX();
			p2.neighborY = p.doorY = rsO3.getY();
			p2.doorX = p.neighborX = rsO4.getX();
			p2.doorY = p.neighborY = rsO4.getY();
			
			// c.sendMessage("Y status "+(doorStatus == 1 ?
			// openOffsets[o2.getCurrentFace()][1] :
			// closeOffsets[o2.getCurrentFace()][1]));
		} else {
			// c.sendMessage("Clicked on the Left door.");
			// Left Door //TODO: MAKE IT WORK RIGHT!
			RSObject rsO3 = RSObject.createDoor(o.getId() + doorStatus,
					o.getX() + (doorStatus == 1
							? openOffsets[o.getOrientation()][0]
							: closeOffsets[o.getOrientation()][0]),
					o.getY() + (doorStatus == 1
							? openOffsets[o.getOrientation()][1]
							: closeOffsets[o.getOrientation()][1]),
					z, o.getOrientation() + doorStatus + 2, o.getType(), -1, o.getX(),
					o.getY(), ticks);

			// Right Door //TEST THIS ONE
			RSObject rsO4 = RSObject.createDoor(o2.getId() + doorStatus,
					o2.getX() + (doorStatus == 1
							? openOffsets[o2.getOrientation()][0]
							: closeOffsets[o2.getOrientation()][0]),
					o2.getY() + (doorStatus == 1
							? openOffsets[o2.getOrientation()][1]
							: closeOffsets[o2.getOrientation()][1]),
					z, o2.getOrientation() + doorStatus, o2.getType(), -1, o2.getX(),
					o2.getY(), ticks);
			
			ObjectManager.addObject(rsO3);
			ObjectManager.addObject(rsO4);
			
			rsO3.setObjectClass(3);
			rsO4.setObjectClass(3);

			p2.neighborX = p.doorX = rsO3.getX();
			p2.neighborY = p.doorY = rsO3.getY();
			p2.doorX = p.neighborX = rsO4.getX();
			p2.doorY = p.neighborY = rsO4.getY();
			
		}

	}

	public RSObject getSecondTempDoor(Client c, GameObject o) {

		return null;
	}

}
