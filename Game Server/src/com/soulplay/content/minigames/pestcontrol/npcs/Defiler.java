package com.soulplay.content.minigames.pestcontrol.npcs;

import java.util.ArrayList;

import com.soulplay.content.minigames.PestControl;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class Defiler {

	public int stuckCount = 0;

	public NPC defiler;

	public Defiler(NPC n) {
		defiler = n;
		n.startGraphic(Graphic.create(654));
		process();
	}

	public void destroy() {
		defiler = null;
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public int getCloseRandomPlayer() {
		ArrayList<Integer> players = new ArrayList<>();
		for (Player p : PestControl.players) {
			if (p != null) {
				if (!p.inPcGame()) {
					continue;
				}
				if ((p.underAttackBy <= 0 && p.underAttackBy2 <= 0)) {
					if (!p.hidePlayer) {
						players.add(p.getId());
					}
				}
			}
		}
		if (players.size() > 0) {
			int index = players.get(Misc.random(players.size() - 1));
			players.clear();
			return index;
		}
		return 0;
	}

	public NPC getKnight() {
		for (NPC n : PestControl.npcs) {
			if (n == null) {
				continue;
			}
			if (n.npcType == 3782) {
				return n;
			}
		}
		return null;
	}

	public void process() {
		CycleEventHandler.getSingleton().addEvent(defiler, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				if (defiler == null) {
					container.stop();
					destroy();
					return;
				}

				if (defiler.isDead()) {
					container.stop();
					// defiler = null;
					destroy();
					return;
				}

				if (defiler.teleporting) {
					return;
				}

				if (defiler.getKillerId() > 0) {
					Player p = PlayerHandler.players[defiler.getKillerId()];
					if (p != null && p.inPcGame()) {
						if (!NPCHandler.goodDistance(defiler.absX, defiler.absY,
								p.getX(), p.getY(), NPCHandler
										.distanceRequired(defiler.npcIndex))) {
							// System.out.println("stuck "+stuckCount);
							if (stuckCount > 5) {
								defiler.setKillerId(0);
							}
							stuckCount++;
						}
					} else {
						defiler.setKillerId(0);
					}
					return;
				}
				if (defiler.underAttackBy > 0) {
					Player p = PlayerHandler.players[defiler.underAttackBy];
					if (p != null && p.inPcGame()) {

					} else {
						defiler.underAttackBy = 0;
					}
					return;
				}

				int random = Misc.random(2);
				if (random == 0 && defiler.getKillerId() < 1
						&& defiler.underAttackBy < 1 && defiler.killNpc < 1) { // attack
																				// npc
					NPC knight = getKnight();
					if (knight != null) {
						defiler.killNpc = knight.npcIndex;
					}
				} else if (defiler.killNpc < 1 && defiler.underAttackBy < 1
						&& defiler.underAttackBy2 < 1) { // else attack a random
															// close player
					int randomPlayerIndex = getCloseRandomPlayer();
					if (randomPlayerIndex != 0) {
						Player p = PlayerHandler.players[randomPlayerIndex];
						if (p != null && p.inPcGame()) {
							defiler.setKillerId(p.getId());
						} else {
							defiler.setKillerId(0);
							defiler.underAttackBy = 0;
						}
					}
				}

			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 2);
	}

}
