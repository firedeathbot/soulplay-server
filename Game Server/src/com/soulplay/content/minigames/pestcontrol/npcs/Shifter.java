package com.soulplay.content.minigames.pestcontrol.npcs;

import java.util.ArrayList;

import com.soulplay.content.minigames.PestControl;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Shifter {

	public NPC shifter;

	public Shifter(NPC n) {
		shifter = n;
		n.startGraphic(Graphic.create(654));
		process();
	}

	public void destroy() {
		shifter = null;
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public int getCloseRandomPlayer() {
		ArrayList<Integer> players = new ArrayList<>();
		for (Player p : PestControl.players) {
			if (p != null) {
				if (!p.inPcGame()) {
					continue;
				}
				if ((p.underAttackBy <= 0 && p.underAttackBy2 <= 0)) {
					if (!p.hidePlayer) {
						players.add(p.getId());
					}
				}
			}
		}
		if (players.size() > 0) {
			int index = players.get(Misc.random(players.size() - 1));
			players.clear();
			return index;
		}
		return 0;
	}

	/*
	 * public void teleportTo(int teleX, int teleY) { shifter.moveX =
	 * shifter.moveY = 0; boolean breaking = false; int spawnX = teleX; int
	 * spawnY = teleY; for (int x = 0; x < 2; x++) { if (breaking) break; for
	 * (int y = 0; y < 2; y++) { if(x == 0 && y == 0) continue;
	 * if(PathFinder.canMove(teleX, teleY, 0, x, y)) { spawnX = teleX+x; spawnY
	 * = teleY+y; breaking = true; break; } } } shifter.teleport(spawnX, spawnY,
	 * 0, -1, -1, 654, 654); }
	 */

	public NPC getKnight() {
		for (NPC n : PestControl.npcs) {
			if (n == null) {
				continue;
			}
			if (n.npcType == 3782) {
				return n;
			}
		}
		return null;
	}

	public void process() {
		CycleEventHandler.getSingleton().addEvent(shifter, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				if (shifter == null) {
					container.stop();
					destroy();
					return;
				}

				if (shifter.isDead()) {
					container.stop();
					// shifter = null;
					destroy();
					return;
				}

				if (shifter.teleporting) {
					return;
				}

				if (shifter.getKillerId() > 0) {
					Player p = PlayerHandler.players[shifter.getKillerId()];
					if (p != null && p.isActive && !p.isDead() && p.inPcGame()) {
						if (!p.goodDistance(shifter, 3)) {
							// teleportTo(p.getX(), p.getY());
							teleportToPlayer(p);
						}
						if (RegionClip.rayTraceBlocked(p.getX(), p.getY(),
								shifter.getX(), shifter.getY(),
								shifter.getHeightLevel(), false)) {
							// teleportTo(p.getX(), p.getY());
							teleportToPlayer(p);
						}
					} else {
						shifter.setKillerId(0);
					}
					return;
				}
				if (shifter.underAttackBy > 0) {
					Player p = PlayerHandler.players[shifter.underAttackBy];
					if (p != null && p.inPcGame()) {
						if (!p.goodDistance(shifter, 3)) {
							// teleportTo(p.getX(), p.getY());
							teleportToPlayer(p);
						}
						if (RegionClip.rayTraceBlocked(p.getX(), p.getY(),
								shifter.getX(), shifter.getY(),
								shifter.getHeightLevel(), false)) {
							// teleportTo(p.getX(), p.getY());
							teleportToPlayer(p);
						}
					} else {
						shifter.underAttackBy = 0;
					}
					return;
				}

				int random = Misc.random(2);
				if (random == 0 && shifter.getKillerId() < 1
						&& shifter.underAttackBy < 1 && shifter.killNpc < 1) { // attack
																				// npc
					NPC knight = getKnight();
					if (knight != null) {
						teleportTo(knight.getX(), knight.getY());
						// shifter.teleport(knight.getX()+1, knight.getY(), 0,
						// -1, -1, 654, 654);
						shifter.killNpc = knight.npcIndex;
					}
				} else if (shifter.killNpc < 1 && shifter.underAttackBy < 1
						&& shifter.underAttackBy2 < 1) { // else attack a random
															// close player
					int randomPlayerIndex = getCloseRandomPlayer(); // Server.npcHandler.getCloseRandomPlayer(shifter.npcIndex);
					if (randomPlayerIndex != 0) {
						Player p = PlayerHandler.players[randomPlayerIndex];
						if (p != null && p.inPcGame()) {
							if (!p.goodDistance(shifter, 3)) {
								shifter.setKillerId(p.getId());
								// teleportTo(p.getX(), p.getY());
								teleportToPlayer(p);
							} else {
								shifter.setKillerId(p.getId());
							}
						} else {
							shifter.setKillerId(0);
							shifter.underAttackBy = 0;
						}
					}
				}

			}

			@Override
			public void stop() {
				if (shifter != null) {
					shifter.setDead(true);
					// shifter = null;
					destroy();
				}
				// NPC n = NPCHandler.npcs[shifter.npcIndex];
				// if (n != null) {
				// n.needRespawn = true;
				// n.destroy();
				// n = null;
				// }
				// if (shifter != null) {
				// shifter.needRespawn = true;
				// shifter.destroy();
				// shifter = null;
				// }

			}
		}, 2);
	}

	public void teleportTo(int teleX, int teleY) {
		// shifter.forceChat("teleporting");
		shifter.teleporting = true;
		shifter.startGraphic(Graphic.create(654));
		shifter.moveX = 0;
		shifter.moveY = 0;
		int spawnX = teleX, spawnY = teleY;
		boolean breaking = false;
		for (int x = 0; x < 2; x++) {
			if (breaking) {
				break;
			}
			for (int y = 0; y < 2; y++) {
				if (x == 0 && y == 0) {
					continue;
				}
				if (PathFinder.canMoveInStaticRegion(teleX, teleY, 0, x, y)) {
					spawnX = teleX + x;
					spawnY = teleY + y;
					breaking = true;
					break;
				}
			}
		}

		// shifter.absX = spawnX;
		// shifter.absY = spawnY;
		final int teleXFinal = spawnX;
		final int teleYFinal = spawnY;
		// shifter.moveNpc(spawnX, spawnY);

		CycleEventHandler.getSingleton().addEvent(shifter, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (shifter == null || shifter.isDead()) {
					container.stop();
					return;
				}
				shifter.moveX = 0;
				shifter.moveY = 0;
				shifter.setLastLocation(Location.create(shifter.getX(), shifter.getY(), shifter.getZ()));
				shifter.absX = teleXFinal;
				shifter.absY = teleYFinal;
				shifter.didTeleport = true;
				shifter.setLocation(shifter.absX, shifter.absY);
				shifter.startGraphic(Graphic.create(654));
				container.stop();

			}

			@Override
			public void stop() {
				if (shifter != null) {
					shifter.teleporting = false;
					if (shifter.isDead()) {
						// shifter = null;
						destroy();
					}
				}
			}
		}, 2);
	}

	public void teleportToPlayer(final Player p) {
		shifter.teleporting = true;
		shifter.startGraphic(Graphic.create(654));
		shifter.moveX = 0;
		shifter.moveY = 0;

		CycleEventHandler.getSingleton().addEvent(shifter, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (shifter == null || shifter.isDead()) {
					container.stop();
					return;
				}

				int spawnX = p.getX(), spawnY = p.getY();
				boolean breaking = false;
				for (int x = 0; x < 2; x++) {
					if (breaking) {
						break;
					}
					for (int y = 0; y < 2; y++) {
						if (x == 0 && y == 0) {
							continue;
						}
						if (PathFinder.canMoveInStaticRegion(p.getX(), p.getY(), 0, x, y)) {
							spawnX = p.getX() + x;
							spawnY = p.getY() + y;
							breaking = true;
							break;
						}
					}
				}

				shifter.moveX = 0;
				shifter.moveY = 0;
				shifter.setLastLocation(Location.create(shifter.getX(), shifter.getY(), shifter.getZ()));
				shifter.absX = spawnX;
				shifter.absY = spawnY;
				shifter.didTeleport = true;
				shifter.setLocation(shifter.absX, shifter.absY);
				shifter.startGraphic(Graphic.create(654));
				container.stop();

			}

			@Override
			public void stop() {
				if (shifter != null) {
					shifter.teleporting = false;
					if (shifter.isDead()) {
						// shifter = null;
						destroy();
					}
				}
			}
		}, 2);
	}

}
