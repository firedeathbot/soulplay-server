package com.soulplay.content.minigames.pestcontrol.npcs;

import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class Spinner {

	public NPC spinner;

	private int portalOwner = 0; // index of the portal owner

	public Spinner(NPC n) {
		spinner = n;

		if (n.absX == 2631 && n.absY == 2592) {
			setPortalOwner(PestControl.portal0Index);
		} else if (n.absX == 2679 && n.absY == 2589) {
			setPortalOwner(PestControl.portal1Index);
		} else if (n.absX == 2670 && n.absY == 2573) {
			setPortalOwner(PestControl.portal2Index);
		} else {
			setPortalOwner(PestControl.portal3Index);
		}

		n.startGraphic(Graphic.create(654));
		process();
	}

	public void damageSurroundingPlayers() {

		if (spinner == null) {
			return;
		}

		final int deadX = spinner.absX;
		final int deadY = spinner.absY;

		CycleEventHandler.getSingleton().addEvent(spinner, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				if (spinner == null) {
					container.stop();
					return;
				}

				for (Player p : PestControl.players) {
					if (p == null) {
						continue;
					}
					if (!p.inPcGame()) {
						continue;
					}
					for (int i = -1; i < 2; i++) {
						for (int j = -1; j < 2; j++) {
							// Server.getStillGraphicsManager().stillGraphics((Client)p,
							// p.getX()+ i, p.getY()+j, 0, 5, 0);
							if (deadX + i == p.getX()
									&& deadY + j == p.getY()) {
								int damageRan = 5 + Misc.random(10);
								damageRan = p.applyDamageReduction(damageRan, null);
								damageRan = p.dealDamage(new Hit(damageRan, 0, 2));

								p.getDamageMap().incrementTotalDamage(
										spinner.getNpcIndex(), damageRan);
								p.getDotManager().applyPoison(3, spinner);
								continue;
							}
						}
					}
				}

				container.stop();
			}

			@Override
			public void stop() {
				if (spinner != null) {
					// spinner = null;
					destroy();
				}

			}
		}, 2);

	}

	public void destroy() {
		spinner = null;
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public int getPortalOwnerIndex() {
		return portalOwner;
	}

	public void process() {
		CycleEventHandler.getSingleton().addEvent(spinner, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				if (spinner == null) {
					container.stop();
					destroy();
					return;
				}

				if (spinner.isDead()) {
					container.stop();
					// spinner = null;
					destroy();
					return;
				}

				if (PestControl.npcs[portalOwner] == null
						|| PestControl.npcs[portalOwner].isDead()) {
					container.stop();
					return;
				}

				// if (spinner.teleporting) {
				// return;
				// }
				// System.out.println("Hello");

				if (Misc.random(7) == 1) { // random chance of healing the
											// nearest portal
					// System.out.println("strting attack");
					if (portalOwner > 0) {
						if (PestControl.npcs[portalOwner] != null
								&& PestControl.npcs[portalOwner].getSkills().getLifepoints() > 0
								&& PestControl.npcs[portalOwner].getSkills().getLifepoints() < PestControl.npcs[portalOwner].getSkills().getMaximumLifepoints()) {
							spinner.setKillerId(0);
							spinner.killNpc = PestControl.npcs[portalOwner].npcIndex;
							// System.out.println("set
							// attack"+PestControl.npcs[portalOwner].npcIndex);
						} else if (PestControl.npcs[portalOwner] == null
								|| PestControl.npcs[portalOwner].isDead()
								|| PestControl.npcs[portalOwner].getSkills().getLifepoints() <= 0) {
							container.stop();
						}
					}
				}

			}

			@Override
			public void stop() {
				if (spinner != null) {
					if (!spinner.isDead()) {
						damageSurroundingPlayers();
						spinner.setDead(true);
					}
				}
			}
		}, 2);

	}

	public void setPortalOwner(int portalIndex) {
		portalOwner = portalIndex;
	}

}
