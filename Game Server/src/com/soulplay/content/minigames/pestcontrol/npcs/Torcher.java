package com.soulplay.content.minigames.pestcontrol.npcs;

import java.util.ArrayList;

import com.soulplay.content.minigames.PestControl;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class Torcher {

	public int stuckCount = 0;

	public NPC torcher;

	public Torcher(NPC n) {
		torcher = n;
		n.startGraphic(Graphic.create(654));
		process();
	}

	public void destroy() {
		torcher = null;
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public int getCloseRandomPlayer() {
		ArrayList<Integer> players = new ArrayList<>();
		for (Player p : PestControl.players) {
			if (p != null) {
				if (!p.inPcGame()) {
					continue;
				}
				if ((p.underAttackBy <= 0 && p.underAttackBy2 <= 0)) {
					if (!p.hidePlayer) {
						players.add(p.getId());
					}
				}
			}
		}
		if (players.size() > 0) {
			int index = players.get(Misc.random(players.size() - 1));
			players.clear();
			return index;
		}
		return 0;
	}

	public NPC getKnight() {
		for (NPC n : PestControl.npcs) {
			if (n == null) {
				continue;
			}
			if (n.npcType == 3782) {
				return n;
			}
		}
		return null;
	}

	public void process() {
		CycleEventHandler.getSingleton().addEvent(torcher, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {

				if (torcher == null || !torcher.isActive) {
					container.stop();
					destroy();
					return;
				}

				if (torcher.isDead()) {
					container.stop();
					// torcher = null;
					destroy();
					return;
				}

				if (torcher.teleporting) {
					return;
				}

				if (torcher.getKillerId() > 0) {
					Player p = PlayerHandler.players[torcher.getKillerId()];
					if (p != null && p.inPcGame()) {
						if (!NPCHandler.goodDistance(torcher.absX, torcher.absY,
								p.getX(), p.getY(), NPCHandler
										.distanceRequired(torcher.npcIndex))) {
							// System.out.println("stuck "+stuckCount);
							if (stuckCount > 5) {
								torcher.setKillerId(0);
							}
							stuckCount++;
						}
					} else {
						torcher.setKillerId(0);
					}
					return;
				}
				if (torcher.underAttackBy > 0) {
					Player p = PlayerHandler.players[torcher.underAttackBy];
					if (p != null && p.inPcGame()) {

					} else {
						torcher.underAttackBy = 0;
					}
					return;
				}

				int random = Misc.random(2);
				if (random == 0 && torcher.getKillerId() < 1
						&& torcher.underAttackBy < 1 && torcher.killNpc < 1) { // attack
																				// npc
					NPC knight = getKnight();
					if (knight != null) {
						torcher.killNpc = knight.npcIndex;
					}
				} else if (torcher.killNpc < 1 && torcher.underAttackBy < 1
						&& torcher.underAttackBy2 < 1) { // else attack a random
															// close player
					int randomPlayerIndex = getCloseRandomPlayer();
					if (randomPlayerIndex != 0) {
						Player p = PlayerHandler.players[randomPlayerIndex];
						if (p != null && p.inPcGame()) {
							torcher.setKillerId(p.getId());
						} else {
							torcher.setKillerId(0);
							torcher.underAttackBy = 0;
						}
					}
				}

			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 2);
	}

}
