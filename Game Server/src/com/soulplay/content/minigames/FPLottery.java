package com.soulplay.content.minigames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class FPLottery {

	// public FPLottery() { //constructor
	//
	// }

	public static ArrayList<String> lotteryPlayerNames = new ArrayList<>(); // players
																					// (playername)

	public static ArrayList<String> unclaimedWinners = new ArrayList<>(); // Winners
																				// that
																				// havent
																				// claimed

	public static int lotteryFund = 0;

	public static final int prizeAmount = 300; // jackpot amount (in millions)

	public static final int entryPrice = 10; // price to enter lottery (in
												// millions)

	public static final int maximumEntryTimes = 5; // maximum times 1 player can
													// enter per draw

	public static long lastAnnouncment;

	public static long lastUptimeUpdate;

	public static final int announcmentFrequency = 10; // announcment frequency
														// in mins

	public static final String[] dir = {"./Data/lottery/lotteryNames.dat",
			"./Data/lottery/unclaimedWinners.dat", "./Data/lottery/fund.dat"};

	public static String[] msgs = {
			// "<shad=40960>[Lottery]</shad> The Lottery Fund is currently at
			// " + FPLottery.lotteryFund + "m. Enter by talking to Gambler at
			// Edgeville",

			"Like our Facebook (<col=ff>::facebook</col>) for giveaways and more!",
			"Subscribe to our Youtube (<col=ff>::youtube</col>) for awesome videos!",
			"Follow us on twitter (<col=ff>::twitter</col>) to stay up to date!",
			"Visit our Forums (<col=ff>::forums</col>) for information!",
			"Join the Clan Chat 'Help' if you need any help!"};

	public static void announceFund() {
		int fund = lotteryFund / 1000000;
		for (Player player : PlayerHandler.players) {
			if (player != null) {
				// final Client all = (Client)PlayerHandler.players[j];
				if (player.isBot()) {
					continue;
				}
				player.sendMessage(
						"<shad=40960>[Lottery]</shad> The current Lottery Pot is at "
								+ fund + "M!");
			}
		}
	}

	public static void checkUnclaimedWinners(Client c) {
		if (unclaimedWinners.contains(c.getName())) {
			if (c.getItems().freeSlots() > 0) {
				c.sendMessage("It's your lucky day! you have won the lottery");
				c.getItems().addItem(995, prizeAmount * 1000000);
				unclaimedWinners.remove(unclaimedWinners.indexOf(c.getName()));
			} else {
				c.sendMessage(
						"You have won the lottery but do not have space for the reward!");
			}
		}
	}

	public static void drawLottery() {
		boolean prizeGiven = false;
		int arraySize = lotteryPlayerNames.size() - 1;
		int winner = Misc.random(arraySize);
		try {
			String player = lotteryPlayerNames.get(winner);
			for (Player player2 : PlayerHandler.players) {
				if (player2 == null) {
					continue;
				}
				if (player2 != null) {
					if (player2.getName().equalsIgnoreCase(player)) {
						final Client c = (Client) player2;
						c.sendMessage("You have won the lottery!");
						Titles.winLottery(c);
						prizeGiven = true;
						if (c.getItems().freeSlots() > 0 && !c.inMinigame()) {
							c.getItems().addItem(995, prizeAmount * 1000000);
						} else {
							c.sendMessage(
									"You do not have enough room in your inventory to claim your reward!");
							c.sendMessage(
									"We will try to add your reward again when you next login.");
							unclaimedWinners.add(c.getName());
						}
					}
					player2.sendMessage(
							"<shad=40960>[Lottery]</shad> The Lottery has been won by "
									+ lotteryPlayerNames.get(winner));
				}
			}
			if (prizeGiven == false) {
				unclaimedWinners.add(lotteryPlayerNames.get(winner));
				prizeGiven = true;
			}
			// for (int j = 0; j < PlayerHandler.players.length; j++) {
			// if (PlayerHandler.players[j] != null) {
			// //final Client all = (Client)PlayerHandler.players[j];
			// PlayerHandler.players[j].sendMessage("<shad=40960>[Lottery]</shad>
			// The Lottery has been won by " + lotteryPlayerNames.get(winner));
			// }
			// }
		} catch (Exception e) {
			System.out.println("Lottery draw failed!");
		}
		lotteryFund = 0;
		lotteryPlayerNames.clear();
		prizeGiven = false;
		saveLists();
	}

	/* unserialize arraylist */
	public static void loadFund() {
		// System.out.println("unserializing list");
		try {
			FileInputStream fin = new FileInputStream(
					"./Data/lottery/fund.dat");
			ObjectInputStream ois = new ObjectInputStream(fin);
			// List<String> list = (ArrayList) ois.readObject();
			// lotteryFund = ois.readObject();
			lotteryFund = ois.readInt();

			// for (String s : lotteryFund) {
			// System.out.println(s);
			// }

			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// aleksandr update, save and load arrayList

	/* unserialize arraylist */
	@SuppressWarnings("unchecked")
	public static void loadList(int i) {
		try {
			FileInputStream fin = new FileInputStream(dir[i]);
			ObjectInputStream ois = new ObjectInputStream(fin);
			if (i == 2) {
				lotteryFund = ois.readInt();
			} else {
				if (i == 0) {
					lotteryPlayerNames = (ArrayList<String>) ois.readObject();
				} else {
					unclaimedWinners = (ArrayList<String>) ois.readObject();
				}
			}

			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadLists() {
		for (int i = 0; i < 3; i++) {
			loadList(i);
		}
	}

	public static void process() {
		if (lotteryFund >= prizeAmount * 1000000) {
			drawLottery();
		}
		// if (System.currentTimeMillis() - lastUptimeUpdate > (1000 * 10) &&
		// Config.UPDATE_UPTIME && !Config.SERVER_DEBUG/* && Config.SERVER_PORT
		// == 43599*/) {
		// lastUptimeUpdate = System.currentTimeMillis();
		// SqlHandler.updateUptime();
		// }
		if (System.currentTimeMillis()
				- lastAnnouncment > (1000 * 60 * announcmentFrequency * 3)) {
			announceFund();
			lastAnnouncment = System.currentTimeMillis();
			// if(!Config.SERVER_DEBUG && Config.SERVER_PORT == 43599)
			// try {
			// SqlHandler.updatePlayerCount(PlayerHandler.getPlayerCount(),
			// PlayerHandler.getStaffCount());//SimpleSql.updatePlayerCount();
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
		}

	}

	public static void saveFund() {
		try {
			// System.out.println("serializing arraylist for lottery");
			FileOutputStream fout = new FileOutputStream(
					"./Data/lottery/fund.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			// oos.writeObject(lotteryFund);
			oos.writeInt(lotteryFund);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* serialize arraylist */
	public static void saveList(int i) {
		try {
			FileOutputStream fout = new FileOutputStream(dir[i]);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			if (i == 2) {
				oos.writeInt(lotteryFund);
			} else {
				oos.writeObject(i == 0 ? lotteryPlayerNames : unclaimedWinners);
			}
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveLists() {
		// serializeArrayListLotteryPlayerNames();
		// serializeArrayListUnclaimedWinners();
		// saveFund();
		for (int i = 0; i < 3; i++) {
			saveList(i);
		}
	}

	/* serialize arraylist */
	public static void serializeArrayListLotteryPlayerNames() {
		try {
			// System.out.println("serializing arraylist for lottery");
			FileOutputStream fout = new FileOutputStream(
					"./Data/lottery/lotteryNames.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(lotteryPlayerNames);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* serialize arraylist */
	public static void serializeArrayListUnclaimedWinners() {
		try {
			// System.out.println("serializing arraylist for lottery");
			FileOutputStream fout = new FileOutputStream(
					"./Data/lottery/unclaimedWinners.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(unclaimedWinners);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void test() {
		// System.out.println("Printing names");
		for (String s : lotteryPlayerNames) {
			System.out.println(s);
		}
		for (String s1 : unclaimedWinners) {
			System.out.println(s1);
		}
		System.out.println("lottery fund: " + lotteryFund);

	}

	/* unserialize arraylist */
	@SuppressWarnings("unchecked")
	public static void unserializeArrayListLotteryPlayerNames() {
		// System.out.println("unserializing list");
		try {
			FileInputStream fin = new FileInputStream(
					"./Data/lottery/lotteryNames.dat");
			ObjectInputStream ois = new ObjectInputStream(fin);
			// List<String> list = (ArrayList) ois.readObject();
			lotteryPlayerNames = (ArrayList<String>) ois.readObject();

			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* unserialize arraylist */
	@SuppressWarnings("unchecked")
	public static void unserializeArrayListUnclaimedWinners() {
		// System.out.println("unserializing list");
		try {
			FileInputStream fin = new FileInputStream(
					"./Data/lottery/unclaimedWinners.dat");
			ObjectInputStream ois = new ObjectInputStream(fin);
			// List<String> list = (ArrayList) ois.readObject();
			unclaimedWinners = (ArrayList<String>) ois.readObject();

			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
