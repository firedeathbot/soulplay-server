package com.soulplay.content.minigames.castlewars;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.content.player.skills.mining.Pickaxes;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

public class CastleWarsObjects {
	
	public static final int SARA_FLAG1 = 4902;
	public static final int SARA_FLAG2 = 4377;
	
	public static final int ZAMMY_FLAG1 = 4903;
	public static final int ZAMMY_FLAG2 = 4378;

	public static void handleObjectFirst(final Client c, final GameObject o) {
		if (!canInteract(c)) {
			return;
		}

		switch (o.getId()) {
			case 4444: // casltewars rope object
						// c.sendMessage("Face "+o.getFace());
				if (o.getOrientation() == 0) {
					c.onCwWalls = true;
					c.getPA().movePlayerDelayed(c.getX() - 1, c.getY(),
							c.heightLevel, 828, 1);
				} else if (o.getOrientation() == 1) {
					c.onCwWalls = true;
					c.getPA().movePlayerDelayed(c.getX(), c.getY() + 1,
							c.heightLevel, 828, 1);
				} else if (o.getOrientation() == 2) {
					c.onCwWalls = true;
					c.getPA().movePlayerDelayed(c.getX() + 1, c.getY(),
							c.heightLevel, 828, 1);
				} else if (o.getOrientation() == 3) {
					c.onCwWalls = true;
					c.getPA().movePlayerDelayed(c.getX(), c.getY() - 1,
							c.heightLevel, 828, 1);
				}
				break;

			case 4448: { // collapse rock walls
				if (c.skillingEvent != null) {
					return;
				}

				Pickaxes pickaxe = Mining.getPickaxe(c);
				if (pickaxe == null) {
					c.sendMessage("You need a pickaxe or use an explosive potion on the wall.");
					return;
				}

				c.startAnimation(pickaxe.getAnimation());
				c.startAction(new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						
						c.skillingTick++;

						if (c.skillingTick % 3 == 0) {
							c.skillingTick = 0;

							c.startAnimation(pickaxe.getAnimation());

							if (CastleWars.successRoll(c.getSkills().getLevel(Skills.MINING))) {
								c.resetAnimations();
								
								if ((o.getX() == 2399 || o.getX() == 2402)
										&& (o.getY() == 9511 || o.getY() == 9514)) {// north cave side
									if (CastleWars.northCave) {
										c.sendMessage("Cave is already collapsed");
										container.stop();
										return;
									}

									RSObject rso = ObjectManager.getObject(2400, 9512, 0);
									if (rso != null) {
										rso.setTick(0);
										CastleWars.collapseCave(0);
										CastleWars.northCave = true;
									}
									rso = null;
								}
								if ((o.getX() == 2408 || o.getX() == 2411)
										&& (o.getY() == 9502 || o.getY() == 9505)) { // east cave side
									if (CastleWars.eastCave) {
										c.sendMessage("Cave is already collapsed");
										container.stop();
										return;
									}

									RSObject rso = ObjectManager.getObject(2409, 9503, 0);
									if (rso != null) {
										rso.setTick(0);
										CastleWars.collapseCave(1);
										CastleWars.eastCave = true;
									}
									rso = null;
								}
								if ((o.getX() == 2400 || o.getX() == 2403)
										&& (o.getY() == 9493 || o.getY() == 9496)) { // south
									// cave
									// side
									if (CastleWars.southCave) {
										c.sendMessage("Cave is already collapsed");
										container.stop();
										return;
									}

									RSObject rso = ObjectManager.getObject(2401, 9494, 0);
									if (rso != null) {
										rso.setTick(0);
										CastleWars.collapseCave(2);
										CastleWars.southCave = true;
									}
									rso = null;
								}
								if ((o.getX() == 2390 || o.getX() == 2393)
										&& (o.getY() == 9500 || o.getY() == 9503)) { // west
									// cave
									// side
									if (CastleWars.westCave) {
										c.sendMessage("Cave is already collapsed");
										container.stop();
										return;
									}

									RSObject rso = ObjectManager.getObject(2391, 9501, 0);
									if (rso != null) {
										rso.setTick(0);
										CastleWars.collapseCave(3);
										CastleWars.westCave = true;
									}
									rso = null;
								}

								container.stop();
							}
						}
					}

					@Override
					public void stop() {
						c.skillingEvent = null;
					}
				}, 1);
				break;
			}
			case 4469:
				if (c.getCWWaitingRoomCountDown() > 0) {
					c.sendMessage("You must wait "
							+ (Misc.ticksIntoSeconds(
									c.getCWWaitingRoomCountDown()))
							+ " seconds before you can exit.");
					break;
				}
				if (c.getItems().playerHasEquipped(CastleWars.SARA_BANNER)
						|| c.getItems()
								.playerHasEquipped(CastleWars.ZAMMY_BANNER)) {
					c.sendMessage("You cannot come inside with the flag.");
					break;
				}
				if (CastleWars.getTeamNumber(c) == 2) {
					c.sendMessage(
							"You are not allowed in the other teams spawn point.");
					break;
				}
				if (o.getX() == 2426) { // north portal
					if (c.getY() == 3080) { // inside
						c.getPA().walkTo(0, 1, true);
						// c.getPA().movePlayer(2426, 3081, c.heightLevel, 0,
						// 1);
					} else if (c.getY() == 3081) {
						if (c.getX() == o.getX()) { // if only one step required
							c.getPA().walkTo(0, -1, true);
						} else {
							c.getPA().walkTo(o.getX() - c.getX(), 0, 0, -1);
						}
						// c.getPA().movePlayer(2426, 3080, c.heightLevel, 0,
						// 1);
					}
				} else if (o.getX() == 2422) { // west portal
					if (c.getX() == 2422) { // outside
						if (c.getY() == o.getY()) { // if only one step required
							c.getPA().walkTo(1, 0, true);
						} else {
							c.getPA().walkTo(0, o.getY() - c.getY(), 1, 0);
						}
						// c.getPA().movePlayer(2423, 3076, c.heightLevel, 0,
						// 1);
					} else if (c.getX() == 2423) { // inside
						c.getPA().walkTo(-1, 0, true);
						// c.getPA().movePlayer(2422, 3076, c.heightLevel, 0,
						// 1);
					}
				}
				break;
			case 4470:
				if (c.getCWWaitingRoomCountDown() > 0) {
					c.sendMessage("You must wait "
							+ (Misc.ticksIntoSeconds(
									c.getCWWaitingRoomCountDown()))
							+ " seconds before you can exit.");
					break;
				}
				if (c.getItems().playerHasEquipped(CastleWars.SARA_BANNER)
						|| c.getItems()
								.playerHasEquipped(CastleWars.ZAMMY_BANNER)) {
					c.sendMessage("You cannot come inside with the flag.");
					break;
				}
				if (CastleWars.getTeamNumber(c) == 1) {
					c.sendMessage(
							"You are not allowed in the other teams spawn point.");
					break;
				}
				if (o.getX() == 2373 && o.getY() == 3126) { // south door
					if (c.getY() == 3126) { // outside
						if (c.getX() == o.getX()) { // if only one step required
							c.getPA().walkTo(0, 1, true);
						} else {
							c.getPA().walkTo(o.getX() - c.getX(), 0, 0, 1);
						}
					} else if (c.getY() == 3127) { // inside
						c.getPA().walkTo(0, -1, true);
					}
				} else if (o.getX() == 2377 && o.getY() == 3131) { // east door
					if (c.getX() == 2376) { // inside
						c.getPA().walkTo(1, 0, true);
					} else if (c.getX() == 2377) { // outside
						if (c.getY() == o.getY()) { // if only one step required
							c.getPA().walkTo(-1, 0, true);
						} else {
							c.getPA().walkTo(0, o.getY() - c.getY(), -1, 0);
						}
					}
				}
				break;
			case 4417:
				if (o.getX() == 2428 && o.getY() == 3081
						&& c.heightLevel == 1) {
					c.getPA().movePlayer(2430, 3080, 2);
				}
				if (o.getX() == 2425 && o.getY() == 3074
						&& c.heightLevel == 2) {
					c.getPA().movePlayer(2426, 3074, 3);
				}
				if (o.getX() == 2419 && o.getY() == 3078
						&& c.heightLevel == 0) {
					c.getPA().movePlayer(2420, 3080, 1);
				}
				break;
			case 4415:
				if (o.getX() == 2419 && o.getY() == 3080
						&& c.heightLevel == 1) {
					c.getPA().movePlayer(2419, 3077, 0);
				}
				if (o.getX() == 2430 && o.getY() == 3081
						&& c.heightLevel == 2) {
					c.getPA().movePlayer(2427, 3081, 1);
				}
				if (o.getX() == 2425 && o.getY() == 3074
						&& c.heightLevel == 3) {
					c.getPA().movePlayer(2425, 3077, 2);
				}
				if (o.getX() == 2374 && o.getY() == 3133
						&& c.heightLevel == 3) {
					c.getPA().movePlayer(2374, 3130, 2);
				}
				if (o.getX() == 2369 && o.getY() == 3126
						&& c.heightLevel == 2) {
					c.getPA().movePlayer(2372, 3126, 1);
				}
				if (o.getX() == 2380 && o.getY() == 3127
						&& c.heightLevel == 1) {
					c.getPA().movePlayer(2380, 3130, 0);
				}
				break;
			case 4411: // castle wars jumping stones
				int i = o.getX() - c.getXorTeleX();
				int j = o.getY() - c.getYorTeleY();
				
				if ((c.getXorTeleX() == o.getX() && c.getYorTeleY() != o.getY()) || (c.getYorTeleY() == o.getY() && c.getXorTeleX() != o.getX())) {
					
					Direction dir = Direction.getDirection(i, j);
					ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, dir).addSecondDirection(i, j, 1, 0).setLockActionsTick(2).setNewOffset(c.getXorTeleX(), c.getYorTeleY());
					c.setForceMovement(mask);
					c.startAnimation(AgilityAnimations.JUMP_OVER_1TICKS_OSRS_DELAYED_HALF_TICK);
					c.performingAction = false; // not using this cheaphaxed shit so setting to false so players can get around faster
				} else {
					if (c.getXorTeleX() != o.getX() && c.getYorTeleY() != o.getY())
						c.sendMessage("I can't reach that!");
				}

				break;
			case 4419:
				if (c.getX() < 2417) {
					c.getPA().movePlayer(2417, 3077, 0);
				} else {
					c.getPA().movePlayer(2416, 3074, 0);
					c.onCwWalls = true;
				}
				// if (o.getX() == 2417 && o.getY() == 3074 && c.heightLevel ==
				// 0) {
				// c.getPA().movePlayer(2416, 3074, 0);
				// }
				// if (o.getX() == 2417 && o.getY() == 3074 && c.heightLevel ==
				// 1) {
				// c.getPA().movePlayer(2417, 3073, 0);
				// }
				break;
			case 4911:
				if (o.getX() == 2421 && o.getY() == 3073 && c.absX >= 2420
						&& c.absX <= 2422 && c.absY >= 3072 && c.absY <= 3074
						&& c.heightLevel == 1) {
					c.getPA().movePlayerDelayed(2421, 3074, 0, 0, 1);
				}
				if (o.getX() == 2378 && o.getY() == 3134
						&& c.heightLevel == 1) {
					c.getPA().movePlayerDelayed(2378, 3133, 0, 0, 1);
				}
				break;
			case 1747:
				if (o.getX() == 2421 && o.getY() == 3073
						&& c.heightLevel == 0) {
					c.getPA().movePlayerDelayed(2421, 3074, 1, 0, 1);
				}
				if (o.getX() == 2378 && o.getY() == 3134
						&& c.heightLevel == 0) {
					c.getPA().movePlayerDelayed(2378, 3133, 1, 0, 1);
				}
				break;
			case 4912:
				if (o.getX() == 2430 && o.getY() == 3082
						&& c.heightLevel == 0) {
					c.getPA().movePlayerDelayed(c.getX(), c.getY() + 6400, 0, 0,
							1);
				}
				if (o.getX() == 2369 && o.getY() == 3125
						&& c.heightLevel == 0) {
					c.getPA().movePlayerDelayed(c.getX(), c.getY() + 6400, 0, 0,
							1);
				}
				break;
			case 1757:
				if (o.getX() == 2430 && o.getY() == 9482) {
					c.getPA().movePlayerDelayed(2430, 3081, 0, 0, 1);
				} else {
					c.getPA().movePlayerDelayed(2369, 3126, 0, 0, 1);
				}
				break;

			case 4418:
				if (o.getX() == 2380 && o.getY() == 3127
						&& c.heightLevel == 0) {
					c.getPA().movePlayer(2379, 3127, 1);
				}
				if (o.getX() == 2369 && o.getY() == 3126
						&& c.heightLevel == 1) {
					c.getPA().movePlayer(2369, 3127, 2);
				}
				if (o.getX() == 2374 && o.getY() == 3131
						&& c.heightLevel == 2) {
					c.getPA().movePlayer(2373, 3133, 3);
				}
				break;
			case 4420:
				if (o.getX() == 2382 && o.getY() == 3131) {
					if (c.getX() >= 2383 && c.getX() <= 2385) {
						c.getPA().movePlayer(2382, 3130, 0);
					} else {
						c.getPA().movePlayer(2383, 3133, 0);
						c.onCwWalls = true;
					}
				}
				break;
			case 4437:
			case 4438:
				Pickaxes pickaxe = Mining.getPickaxe(c);
				if (pickaxe == null) {
					break;
				}

				CastleWars.mineRocks(c, o.getId(), o.getX(), o.getY(), o, pickaxe);
				break;
			case 1568:
				if (o.getX() == 2399 && o.getY() == 3099) {
					c.getPA().movePlayerDelayed(2399, 9500, 0, 0, 1);
				} else {
					c.getPA().movePlayerDelayed(2400, 9507, 0, 0, 1);
				}
				break;
			case 6281:
				if (c.getCWWaitingRoomCountDown() > 0) {
					c.sendMessage("You must wait "
							+ (Misc.ticksIntoSeconds(
									c.getCWWaitingRoomCountDown()))
							+ " seconds before you can exit.");
					break;
				}
				c.getPA().movePlayerDelayed(2370, 3132, 2, 0, 1);
				break;
			case 4472: {
				// if (c.getCWWaitingRoomCountDown() > 0) {
				// c.sendMessage("You must wait
				// "+(Misc.ticksIntoSeconds(c.getCWWaitingRoomCountDown()))+"
				// seconds before you can exit.");
				// break;
				// }
				if (c.getItems().playerHasEquipped(CastleWars.SARA_BANNER)
						|| c.getItems()
								.playerHasEquipped(CastleWars.ZAMMY_BANNER)) {
					c.sendMessage("You cannot come inside with the flag.");
					break;
				}
				if (CastleWars.getTeamNumber(c) == 1) {
					c.sendMessage(
							"You are not allowed in the other teams spawn point.");
					break;
				}

				final boolean onWall = c.onCwWalls;

				c.performingAction = true;
				c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						c.getPA().movePlayer(2370, 3132, 1);
						if (onWall)
							c.onCwWalls = true;
						CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

							@Override
							public void execute(CycleEventContainer container) {
								c.performingAction = false;
								container.stop();
							}

							@Override
							public void stop() {
								/* empty */
							}

						}, 1);
						container.stop();
					}

					@Override
					public void stop() {
						/* empty */
					}

				}, 1);
				break;
			}
			case 6280:
				if (c.getCWWaitingRoomCountDown() > 0) {
					c.sendMessage("You must wait "
							+ (Misc.ticksIntoSeconds(
									c.getCWWaitingRoomCountDown()))
							+ " seconds before you can exit.");
					break;
				}
				c.getPA().movePlayerDelayed(2429, 3075, 2, 0, 1);
				break;
			case 4471: {
				// if (c.getCWWaitingRoomCountDown() > 0) {
				// c.sendMessage("You must wait
				// "+(Misc.ticksIntoSeconds(c.getCWWaitingRoomCountDown()))+"
				// seconds before you can exit.");
				// break;
				// }
				if (c.getItems().playerHasEquipped(CastleWars.SARA_BANNER)
						|| c.getItems()
								.playerHasEquipped(CastleWars.ZAMMY_BANNER)) {
					c.sendMessage("You cannot come inside with the flag.");
					break;
				}
				if (CastleWars.getTeamNumber(c) == 2) {
					c.sendMessage(
							"You are not allowed in the other teams spawn point.");
					break;
				}

				final boolean onWall = c.onCwWalls;

				c.performingAction = true;
				c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						c.getPA().movePlayer(2429, 3075, 1);
						if (onWall)
							c.onCwWalls = true;
						CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

							@Override
							public void execute(CycleEventContainer container) {
								c.performingAction = false;
								container.stop();
							}

							@Override
							public void stop() {
								/* empty */
							}

						}, 1);
						container.stop();
					}

					@Override
					public void stop() {
						/* empty */
					}

				}, 1);
				break;
			}
			case 4406:
				CastleWars.removePlayerFromCw(c);
				break;
			case 4407:
				CastleWars.removePlayerFromCw(c);
				break;
			case SARA_FLAG1: // sara standard with flag
			case SARA_FLAG2: // sara standard without flag

				switch (CastleWars.getTeamNumber(c)) {

					case CastleWars.TEAM_SARA:

//						if (c.playerEquipment[PlayerConstants.playerWeapon] == CastleWars.ZAMMY_BANNER
//								&& CastleWars.saraFlag != CastleWars.STATE_SAFE) {
//							c.sendMessage(
//									"You cannot capture a flag when your teams flag has been taken!");
//							break;
//						}

						if (c.playerEquipment[PlayerConstants.playerWeapon] != CastleWars.ZAMMY_BANNER) {
							c.sendMessage("You cannot take your own flag.");
							break;
						}

						CastleWars.returnFlag(c,
								c.playerEquipment[PlayerConstants.playerWeapon],
								o);
						break;

					case CastleWars.TEAM_ZAMMY:

						if (c.playerEquipment[PlayerConstants.playerWeapon] == CastleWars.ZAMMY_BANNER) {
							c.sendMessage(
									"You cannot capture your own flag here.");
							break;
						}

						CastleWars.captureFlag(c, o);
						break;
				}
				break;

			case ZAMMY_FLAG1: // zammy flag
			case ZAMMY_FLAG2:
				switch (CastleWars.getTeamNumber(c)) {

					case CastleWars.TEAM_SARA:

						if (c.playerEquipment[PlayerConstants.playerWeapon] == CastleWars.SARA_BANNER) {
							c.sendMessage(
									"You cannot capture your own flag here.");
							break;
						}

						CastleWars.captureFlag(c, o);
						break;

					case CastleWars.TEAM_ZAMMY:

//						if (c.playerEquipment[PlayerConstants.playerWeapon] == CastleWars.SARA_BANNER
//								&& CastleWars.zammyFlag != CastleWars.STATE_SAFE) {
//							c.sendMessage(
//									"You cannot capture a flag when your teams flag has been taken!");
//							break;
//						}

						if (c.playerEquipment[PlayerConstants.playerWeapon] != CastleWars.SARA_BANNER) {
							c.sendMessage("You cannot take your own flag.");
							break;
						}

						CastleWars.returnFlag(c,
								c.playerEquipment[PlayerConstants.playerWeapon],
								o);
						break;
				}
				break;
			case 4458: // bandages
				if (c.getItems().addItem(4049, 1)) {
					c.sendMessage("You get some bandages!");
					c.startAnimation(832);
				}
				break;
			case 4459: // tinderbox
				if (c.getItems().addItem(590, 1)) {
					c.sendMessage("You get a tinderbox!");
					c.startAnimation(832);
				}
				break;
			case 4461: // barricades
				if (c.getItems().addItem(4053, 1)) {
					c.sendMessage("You get a barricade!");
					c.startAnimation(832);
				}
				break;
			case 4462: // rope
				if (c.getItems().addItem(4047, 1)) {
					c.sendMessage("You get a rope!");
					c.startAnimation(832);
				}
				break;
			case 4463: // explosive potion!
				if (c.getItems().addItem(4045, 1)) {
					c.sendMessage("You get an explosive potion!");
					c.startAnimation(832);
				}
				break;
			case 4464: // pickaxe table
				if (c.getItems().addItem(1265, 1)) {
					c.sendMessage("You get a bronzen pickaxe for mining.");
					c.startAnimation(832);
				}
				break;
			case 4900:
			case 4901:
				if (CastleWars.isGameStarted())
					CastleWars.pickupFlag(c, o);
				break;

			case 4423: // sara large doors closed
			case 4424:

				if (CastleWars.getTeamNumber(c) == 1) {
					CastleWars.openSaraMainDoors(o);
				} else if (CastleWars.getTeamNumber(c) == 2) {
					if (c.getY() <= 3087) {
						CastleWars.openSaraMainDoors(o);
					} else {
						c.sendMessage(
								"Unable to open the door from the outside.");
					}
				}
				break;
			case 4425: // sara large doors opened
			case 4426:
				CastleWars.closeSaraMainDoors(o);
				break;

			case 4427: // zammy large doors closed
			case 4428:
				if (CastleWars.getTeamNumber(c) == 2) {
					CastleWars.openZammyMainDoors(o);
				} else if (CastleWars.getTeamNumber(c) == 1) {
					if (c.getY() >= 3120) {
						CastleWars.openZammyMainDoors(o);
					} else {
						c.sendMessage(
								"Unable to open the door from the outside.");
					}
				}
				break;

			case 4429: // zammy large doors opened
			case 4430:
				CastleWars.closeZammyMainDoors(o);
				break;

			case 4465: // sara side door closed
				// CastleWars.openSaraDoor(o);
				CastleWars.pickLock(c, o);
				break;

			case 4466: // sara side door opened
				CastleWars.closeSaraDoor(o);
				// CastleWars.pickLock(c, o);
				break;

			case 4467: // zammy side door closed
				// CastleWars.openZammyDoor(o);
				CastleWars.pickLock(c, o);
				break;

			case 4468: // zammy side door opened
				CastleWars.closeZammyDoor(o);
				// CastleWars.pickLock(c, o);
				break;
			default:
				break;

		}

	}
	
	public static void handleObjectSecond(final Client c, final GameObject o) {
		if (!canInteract(c)) {
			return;
		}

		switch (o.getId()) {
		case 4458: // bandages
			if (c.getItems().addItem(4049, 5)) {
				c.sendMessage("You get some bandages!");
				c.startAnimation(832);
			}
			break;
		case 4459: // tinderbox
			if (c.getItems().addItem(590, 5)) {
				c.sendMessage("You get some tinderboxes!");
				c.startAnimation(832);
			}
			break;
		case 4461: // barricades
			if (c.getItems().addItem(4053, 5)) {
				c.sendMessage("You get some barricades!");
				c.startAnimation(832);
			}
			break;
		case 4462: // rope
			if (c.getItems().addItem(4047, 5)) {
				c.sendMessage("You get some ropes!");
				c.startAnimation(832);
			}
			break;
		case 4463: // explosive potion!
			if (c.getItems().addItem(4045, 5)) {
				c.sendMessage("You get 5 explosive potions!");
				c.startAnimation(832);
			}
			break;
		case 4464: // pickaxe table
			if (c.getItems().addItem(1265, 5)) {
				c.sendMessage("You get 5 bronzen pickaxes for mining.");
				c.startAnimation(832);
			}
			break;
		}
	}

	public static void handleObjectThird(final Client c, final GameObject o) {
		if (!canInteract(c)) {
			return;
		}

		switch (o.getId()) {
		case 4458: // bandages
			if (c.getItems().addItem(4049, 10)) {
				c.sendMessage("You get some bandages!");
				c.startAnimation(832);
			}
			break;
		case 4459: // tinderbox
			if (c.getItems().addItem(590, 10)) {
				c.sendMessage("You get some tinderboxes!");
				c.startAnimation(832);
			}
			break;
		case 4461: // barricades
			if (c.getItems().addItem(4053, 10)) {
				c.sendMessage("You get some barricades!");
				c.startAnimation(832);
			}
			break;
		case 4462: // rope
			if (c.getItems().addItem(4047, 10)) {
				c.sendMessage("You get some ropes!");
				c.startAnimation(832);
			}
			break;
		case 4463: // explosive potion!
			if (c.getItems().addItem(4045, 10)) {
				c.sendMessage("You get 10 explosive potions!");
				c.startAnimation(832);
			}
			break;
		case 4464: // pickaxe table
			if (c.getItems().addItem(1265, 10)) {
				c.sendMessage("You get 5 bronzen pickaxes for mining.");
				c.startAnimation(832);
			}
			break;
		}
	}

	private static boolean canInteract(Client c) {
		if (!c.inCw() && c.playerRights != 3) {
			c.sendMessage("You gotta be in castle wars before you can use these objects");
			return false;
		}
		if (!c.isDev() && !CastleWars.isGameStarted()) {
			c.sendMessage("The game has not started yet.");
			return false;
		}

		return true;
	}
}
