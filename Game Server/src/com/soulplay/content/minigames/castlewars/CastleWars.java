package com.soulplay.content.minigames.castlewars;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.minigames.MinigameRules;
import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.npcs.impl.Barricades;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.mining.Pickaxes;
import com.soulplay.content.player.skills.summoning.Creation;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.Task;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.util.Misc;

/**
 * @AUTHOR OF SHIT Aleksandr and couple others
 */
public class CastleWars {

	/*
	 * Game timers.
	 */
	public static final int GAME_TIMER = 2050;//1500; // 1500 * 600 = 900000ms = 15
												// minutes

	private static final int GAME_START_TIMER = 250;

	private static final int SPAWN_ROOM_KICK = 200;

	public static final int NEWCOMER_TIMER = 375; // about 5 minutes, based on 2
													// tick intervals
	// private static final int FlagX, FlagY;

	public static int MINIMUM_PLAYERS = 4;

	public static final int SECONDS_TO_EXIT_SPAWN = 10;

	public static boolean GREEN_PORTAL_ONLY = false;
	
	public static boolean EXTRA_REWARD = false;

	/*
	 * Hashmap for the waitingroom players
	 */
	private static HashMap<Player, Integer> waitingRoom = new HashMap<>();

	/*
	 * hashmap for the gameRoom players
	 */
	public static HashMap<Player, Integer> gameRoom = new HashMap<>();

	/*
	 * if they leave the castlewars, i don't want them to join a different team,
	 * they can only rejoin same team waiting room.
	 */
	public static HashMap<String, Integer> loyalToTeam = new HashMap<>();

	/*
	 * The coordinates for the waitingRoom both sara/zammy
	 */
	private static final int[][] WAIT_ROOM = {{2377, 9485}, // sara
			{2421, 9524} // zammy
	};

	/*
	 * The coordinates for the gameRoom both sara/zammy
	 */
	private static final int[][] GAME_ROOM = {{2426, 3076}, // sara
			{2372, 3131} // zammy
	};

//	private static final int[][] FLAG_STANDS = {{2429, 3074}, // sara {X-Coord,
//																// Y-Coord)
//			{2370, 3133} // zammy
//	};

	private static final int[][] CASTLE_ZONES = {{2414, 2431, 3071, 3088}, // sara
																			// castle
			{2367, 2385, 3118, 3136} // zammy castle
	};

	private static final int[][] CAVE_WALL = { // collapsing rocks coords, fix
												// them up if you need
			{2399, 2402, 9511, 9514}, // north X Y coords zammy 0
			{2408, 2411, 9502, 9505}, // east X Y coords sara 1
			{2400, 2403, 9493, 9496}, // south X Y coords sara 2
			{2390, 2393, 9500, 9503} // west X Y coords zammy 3
	};

	private static final int[][] ROCKS = {{2400, 9512}, // north rocks 0
			{2409, 9503}, // east rocks 1
			{2401, 9494}, // south rocks 2
			{2391, 9501} // west rocks 3
	};

	/*
	 * Checking if caves are collapsed or not
	 */
	public static boolean northCave = true; // zammy cave

	public static boolean eastCave = true; // sara cave

	public static boolean southCave = true; // sara cave

	public static boolean westCave = true; // zammy cave
	// private static final String sCollapsed = "<col=ff00>Collapsed";
	// private static final String sCleared = "<col=ff0000>Cleared";

	private static int zammyMainGateHP = 100;

	private static int saraMainGateHP = 100;

	private static boolean saraSideDoor = false;

	private static boolean zammySideDoor = false;
	// private static final String sLocked = "<col=ff00>Locked";
	// private static final String sUnlocked = "<col=ff0000>Unlocked";

	private static boolean saraCatapult = true;

	private static boolean zammyCatapult = true;

	/*
	 * Scores for saradomin and zamorak!
	 */
	private static int[] scores = {0, 0};

	/*
	 * Booleans to check if a team's flag is safe
	 */
	public static int zammyFlag = 0;

	public static int saraFlag = 0;

	public static int saraFlagHolder = 0;

	public static int zammyFlagHolder = 0;

	public static int lockedSaraCount = 200;

	public static int lockedZammyCount = 200;

	private static int lanthusIndex;

	private static boolean canRejoin = true;

	public static final int TEAM_SARA = 1;

	public static final int TEAM_ZAMMY = 2;

	public static final int STATE_SAFE = 0;

	public static final int STATE_TAKEN = 1;

	public static final int STATE_DROPPED = 2;

	/*
	 * Zamorak and saradomin banner/capes item ID's
	 */
	public static final int SARA_BANNER = 4037;

	public static final int ZAMMY_BANNER = 4039;

	public static final int SARA_CAPE = 4041;

	public static final int ZAMMY_CAPE = 4042;

	public static final int SARA_HOOD = 4513;

	public static final int ZAMMY_HOOD = 4515;

	public static final int BARRICADE_OBJECT = 4421; // 4421

	public static final int BARRICADE_BURNING_OBJECT = 4422;

	public static final int BANDAGES = 4049;
	
	public static final int EXPLOSIVE_POTION = 4045;

	// private static int properTimer = 0;
	private static int gameTimer = -1;

	private static int sendDialogueDelay = 10;

	private static int waitTimer = GAME_START_TIMER;

	private static boolean gameStarted = false;

	private static int spawnTimer = -1;

	private static int announceTimer = 10;
	
	private static int discordStartTime, discordEndTime;
	
	public static boolean isGameStarted() {
		return gameStarted;
	}
	
	private static final int[] BANNED_ITEMS = {391, 385, 379, 333, 329, 373, 361, 7946, 397, 1891, 365,
			339, 1942, 6701, 6705, 7056, 7054, 7058, 7060, 315, 347, 325,
			1897, 2289, 2293, 2297, 2301, 2309, 2323, 2325, 2327, 351, 6703,
			1963, 6883, 2108, 2118, 2116, 15272, 3144, 6, 8, 10, 12, 12047,
			12043, 12059, 12019, 12009, 12778, 12049, 12808, 12067, 12063,
			12091, 12800, 12055, 12053, 12065, 12021, 12818, 12780, 12798,
			12814, 12073, 12087, 12071, 12051, 12095, 12097, 12099, 12101,
			12103, 12105, 12107, 12075, 12816, 12041, 12061, 12007, 12035,
			12027, 12077, 12531, 12812, 12784, 12810, 12085, 12037, 12015,
			12045, 12079, 12123, 12031, 12029, 12033, 12820, 12057, 12792,
			12069, 12011, 12782, 12081, 12794, 12013, 12802, 12804, 12806,
			12025, 12788, 12776, 12083, 12039, 12786, 12089, 12796, 12822,
			12093, 12790, 6583, 995, 7927, 9433, 30193, 30192};
	
	private static final Set<Integer> bannedItems = new HashSet<Integer>();
	
	private static boolean isBannedInventoryItem(int id) {
		return bannedItems.contains(id);
	}

	/**
	 * Will add a cape to a player's equip
	 *
	 * @param player
	 *            the player
	 * @param capeId
	 *            the capeId
	 */
	public static void addCapes(Player player, int team) {
		int capeId = 0;
		switch (team) {
			case 1:
				capeId = SARA_CAPE;
				break;
			case 2:
				capeId = ZAMMY_CAPE;
				break;
		}
		player.displayEquipment[PlayerConstants.playerCape] = capeId;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);

	}

	/**
	 * Method that will add the flag to a player's weapon slot
	 *
	 * @param player
	 *            the player who's getting the flag
	 * @param flagId
	 *            the banner id.
	 */
	public static void addFlag(Player player, int flagId) {
		player.getItems().setEquipment(flagId, 1, PlayerConstants.playerWeapon);
		// createHintIcon(player, flagId); // fix hint icons?
		if (flagId == SARA_BANNER) {
			saraFlagHolder = player.getId();
		}
		if (flagId == ZAMMY_BANNER) {
			zammyFlagHolder = player.getId();
		}
		updateHintIcon();
	}

	public static void addToSaraGame(final Player c) {
		waitingRoom.remove(c);
		c.inCwGame = true;
		moveToSaraSpawn(c);// c.getPA().movePlayer(GAME_ROOM[0][0] +
							// Misc.random(3), GAME_ROOM[0][1] - Misc.random(3),
							// 1);
		c.getPA().walkableInterface(11146);
		c.getPA().setCastleWarsWaitingTimer(
				Misc.secondsToTicks(20/* SECONDS_TO_EXIT_SPAWN */));
		gameRoom.put(c, 1);
		c.castleWarsTeam = 1;
		c.soulWarsTeam = Team.BLUE;
		loyalToTeam.put(c.UUID, 1);
	}

	/**
	 * Method we use to add someone to the waitinroom in a different method,
	 * this will filter out some error messages
	 *
	 * @param player
	 *            the player that wants to join
	 * @param team
	 *            the team!
	 */
	public static void addToWaitRoom(Player player, int team) {
		if (!MinigameRules.canJoinMinigame(player))
			return;
		
		if (player.summoned != null) {
			player.sendMessage("You cannot bring your pet inside.");
			return;
		}
		if (waitingRoom.containsKey(player)) {
			leaveWaitingRoom(player);
			return;
		}

		if (GREEN_PORTAL_ONLY && (team == 1 || team == 2)) {
			player.sendMessage(
					"You can only use the Green Portal at the moment.");
			return;
		}
		
		int loyalty = loyalToTeam.getOrDefault(player.UUID, 0);

		if (gameTimer > 0 && gameTimer <= NEWCOMER_TIMER
				&& loyalty == 0) { // don't let
																	// new
																	// players
																	// join,
																	// only
																	// those who
																	// left can
																	// join now.
			player.sendMessage(
					"Newcomers cannot join the game at the moment. Please wait "
							+ (gameTimer * 600 / 1000)
							+ " seconds for the new game.");
			return;
		}

		if (team != 3 && loyalty != 0) {
			if (loyalty != team) {
				player.sendMessage(
						"This is not your team! Your loyalty is with "
								+ (team == 1 ? "Zamorak" : "Saradomin")
								+ " team!");
				return;
			}
		}

		boolean sameUUID = false;

		if (gameStarted) {
			if (!gameRoom.isEmpty()) {
				Iterator<Player> iterator = gameRoom.keySet().iterator();
				while (iterator.hasNext()) {
					Player teamPlayer = iterator.next();
					if (!teamPlayer.isActive) {
						iterator.remove();
						continue;
					}

					if (teamPlayer.UUID.equals(player.UUID)) {
						sameUUID = true;
						break;
					}
				}
			}
		}

		for (Player p : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[0][0], WAIT_ROOM[0][1], 0)
				.getPlayers()) { // sara room
			if (p == null || p.disconnected || p.forceDisconnect) {
				continue;
			}
			if (!RSConstants.cwSaraWaitingRoom(p.getX(), p.getY())) {
				continue;
			}
			if (p.UUID.equals(player.UUID)) {
				sameUUID = true;
				break;
			}
		}

		for (Player p : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[1][0], WAIT_ROOM[1][1], 0)
				.getPlayers()) { // zammy room
			if (p == null || p.disconnected || p.forceDisconnect) {
				continue;
			}
			if (!RSConstants.cwZammyWaitingRoom(p.getX(), p.getY())) {
				continue;
			}
			if (p.UUID.equals(player.UUID)) {
				sameUUID = true;
				break;
			}
		}

		if (sameUUID && !player.isOwner() && Config.RUN_ON_DEDI) {
			player.sendMessage(
					"Sorry but you cannot enter the game with multiple accounts.");
			return;
		}

		for (int i = 0; i < player.playerItems.length; i++) {
			int itemId = player.playerItems[i]-1;
			if (isBannedInventoryItem(itemId)) {
				player.sendMessage(String.format("%s is not allowed in Castle Wars.", ItemProjectInsanity.getItemName(itemId)));
				player.sendMessage(
						"You may not bring your own food, coind, ring of stone, easter ring, summoning pouches");
				player.sendMessage("or dwarf cannon inside of castle wars.");
				return;
			}
		}
		player.wantsToRejoin = true;
		toWaitingRoom(player, team);
		player.getPA().walkableInterface(6673);
	}

	public static void addToZammyGame(final Player c) {
		waitingRoom.remove(c);
		c.inCwGame = true;
		moveToZammySpawn(c);
		c.getPA().walkableInterface(11146);
		c.getPA().setCastleWarsWaitingTimer(
				Misc.secondsToTicks(20/* SECONDS_TO_EXIT_SPAWN */));
		gameRoom.put(c, 2);
		c.castleWarsTeam = 2;
		c.soulWarsTeam = Team.RED;
		loyalToTeam.put(c.UUID, 2);
	}

	public static void announceLanthus(NPC n) {
		if (gameTimer > 0) {
			n.forceChat("The next game begins in " + getNextGameMinutes()
					+ " minutes!");
		} else if (waitTimer > 0) {
			if (getSaraPlayersWaiting() >= MINIMUM_PLAYERS
					&& getZammyPlayersWaiting() >= MINIMUM_PLAYERS) {
				n.forceChat("The game will begin in "
						+ ((int) Misc.ticksIntoSeconds(waitTimer))
						+ " seconds!");
			}
		}
	}

	public static void breakSaraMainDoors(final GameObject o) {
		final RSObject rso1 = new RSObject(4431, 2426, 3088, o.getZ(), 0, 0, 4431, GAME_TIMER);
		final RSObject rso2 = new RSObject(4432, 2427, 3088, o.getZ(), 2, 0, 4432, GAME_TIMER);
		rso1.setObjectClass(1);
		rso2.setObjectClass(1);
		ObjectManager.addObject(rso1);
		ObjectManager.addObject(rso2);
	}

	public static void breakZammyMainDoors(final GameObject o) {
		final RSObject rso1 = new RSObject(4433, 2373, 3119, o.getZ(), 2, 0, 4433, GAME_TIMER);
		final RSObject rso2 = new RSObject(4434, 2372, 3119, o.getZ(), 0, 0, 4434, GAME_TIMER);
		rso1.setObjectClass(1);
		rso2.setObjectClass(1);
		ObjectManager.addObject(rso1);
		ObjectManager.addObject(rso2);
	}

	public static boolean canRejoinGame() {
		// if (gameTimer <= NEWCOMER_TIMER) {
		// return true;
		// }
		// return false;
		return canRejoin;
	}

	/**
	 * Method that will capture a flag when being taken by the enemy team!
	 *
	 * @param player
	 *            the player who returned the flag
	 * @param team
	 */
	public static void captureFlag(Player player, GameObject o) {

		int team = player.castleWarsTeam;// gameRoom.get(player);

		// team 1 = Saradomin
		// team 2 = Zammy

		if (o.getId() == CastleWarsObjects.SARA_FLAG1 && team == CastleWars.TEAM_ZAMMY && saraFlag == STATE_SAFE) { // sara
																		// flag

			if (player.playerEquipment[PlayerConstants.playerWeapon] > 0) {
				if (!player.getItems().removeItem(
						player.playerEquipment[PlayerConstants.playerWeapon],
						PlayerConstants.playerWeapon)) {
					player.sendMessage(
							"You do not have enough space in your inventory to unequip your weapon.");
					return;
				}
			}

			if (player.playerEquipment[PlayerConstants.playerShield] > 0) {
				if (!player.getItems().removeItem(
						player.playerEquipment[PlayerConstants.playerShield],
						PlayerConstants.playerShield)) {
					player.sendMessage(
							"You do not have enough space in your inventory to unequip your shield.");
					return;
				}
			}

			setSaraFlag(1);
			addFlag(player, SARA_BANNER);
			captureFlagObjectChange(o, CastleWarsObjects.SARA_FLAG2);
		} else if (o.getId() == CastleWarsObjects.ZAMMY_FLAG1 && team == TEAM_SARA && zammyFlag == STATE_SAFE) {

			if (player.playerEquipment[PlayerConstants.playerWeapon] > 0) {
				if (!player.getItems().removeItem(
						player.playerEquipment[PlayerConstants.playerWeapon],
						PlayerConstants.playerWeapon)) {
					player.sendMessage(
							"You do not have enough space in your inventory to unequip your weapon.");
					return;
				}
			}

			if (player.playerEquipment[PlayerConstants.playerShield] > 0) {
				if (!player.getItems().removeItem(
						player.playerEquipment[PlayerConstants.playerShield],
						PlayerConstants.playerShield)) {
					player.sendMessage(
							"You do not have enough space in your inventory to unequip your shield.");
					return;
				}
			}

			setZammyFlag(1);
			addFlag(player, ZAMMY_BANNER);
			captureFlagObjectChange(o, CastleWarsObjects.ZAMMY_FLAG2);
		}

		// 0 safe

		// TODO check if a team is carrying the right flag and if they are at
		// the wrong capture point

	}

	public static void captureFlagObjectChange(GameObject o, int newId) {
		if (ObjectManager.getObject(o.getX(), o.getY(), o.getZ()) == null) {
			final RSObject rso = new RSObject(newId, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), o.getId(), GAME_TIMER);
			rso.setObjectClass(1);
			ObjectManager.addObject(rso);
		}
	}
	
	public static void returnFlagToSaraStand() {
		RSObject oldObj = ObjectManager.getObject(2429, 3074, 3);
		if (oldObj != null)
			oldObj.setTick(0);
	}
	
	public static void returnFlagToZammyStand() {
		RSObject oldObj = ObjectManager.getObject(2370, 3133, 3);
		if (oldObj != null)
			oldObj.setTick(0);
	}
	
	public static int caveLocation(int x, int y) {
		for (int i = 0; i < CAVE_WALL.length; i++) {
			if ((x == CAVE_WALL[i][0] || x == CAVE_WALL[i][1])
					&& (y == CAVE_WALL[i][2] || y == CAVE_WALL[i][3])) {
				return i;
			}
		}
		return -1;
	}

	public static boolean clearedCave(int cave) {
		switch (cave) {
			case 0:
				return northCave;
			case 1:
				return eastCave;
			case 2:
				return southCave;
			case 3:
				return westCave;
		}
		return false;
	}

	public static boolean clearHalfRocks(final Player c, final GameObject o) {
		if (o == null) {
			return false;
		}
		final RSObject rso = new RSObject(4438, o.getX(), o.getY(), c.heightLevel, 0, o.getType(),
				o.getId(), CastleWars.GAME_TIMER);
		rso.setObjectClass(1);
		ObjectManager.addObject(rso);
		return true;
	}

	public static void clearRocks(int x, int y) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (CAVE_WALL[i][j] + 1 == x) {
					for (int k = 0; k < 4; k++) {
						if (CAVE_WALL[i][k] + 1 == y) {
							setCaveStatus(i, false);
							return;
						}
					}
				}
			}
		}
	}

	public static void clearRocksCompletely(final Player c,
			final GameObject o) {
		RSObject rso = ObjectManager.getPlacedObject(o.getX(), o.getY(),
				o.getZ(), o.getType());
		if (rso != null) {
			rso.setNewObjectId(-1);
			ObjectManager.updateNewObject(rso);
			clearRocks(o.getX(), o.getY());
		}
		rso = null;
	}

	public static void closeSaraDoor(final GameObject o) {
		if (!saraSideDoor) {
			return;
		}
		saraSideDoor = false;
		closeSideDoor(o);
	}

	public static void closeSaraMainDoors(final GameObject o) {
		int/* id1 = 4425, */ x1 = 2426, y1 = 3087; // closed doos loc
		int /* id2 = 4426, */ x2 = 2427, y2 = 3087; // closed doors loc
		int WEST_OPEN = 4425;
		int EAST_OPEN = 4426;

		RSObject westDoor = ObjectManager.getObjectByNewId(WEST_OPEN, x1, y1,
				o.getZ()); // closed doors location
		if (westDoor == null) {
			// System.out.println("West door is null");
			return;
		}
		RSObject westDoor2 = ObjectManager.getObjectByNewId(-1,
				westDoor.getOriginalX(), westDoor.getOriginalY(), o.getZ()); // open
																				// doors
																				// loc
		if (westDoor2 == null) {
			// System.out.println("West door2 is null");
			return;
		}
		RSObject eastDoor = ObjectManager.getObjectByNewId(EAST_OPEN, x2, y2,
				o.getZ()); // closed door loc
		if (eastDoor == null) {
			// System.out.println("East door is null");
			return;
		}
		RSObject eastDoor2 = ObjectManager.getObjectByNewId(-1,
				eastDoor.getOriginalX(), eastDoor.getOriginalY(), o.getZ()); // open
																				// door
																				// loc
		if (eastDoor2 == null) {
			// System.out.println("East door2 is null");
			return;
		}
		westDoor.setTick(0);
		westDoor2.setTick(0);
		eastDoor.setTick(0);
		eastDoor2.setTick(0);

		westDoor = null;
		westDoor2 = null;
		eastDoor = null;
		eastDoor2 = null;

	}

	public static void closeSideDoor(final GameObject o) {
		RSObject o1 = ObjectManager.getObjectByNewId(o.getId(), o.getX(),
				o.getY(), o.getZ());
		if (o1 == null) {
			return;
		}
		RSObject o2 = ObjectManager.getObjectByNewId(-1, o1.getOriginalX(),
				o1.getOriginalY(), o1.getHeight());
		if (o2 == null) {
			return;
		}
		
		o.setActive(false);

		o1.setTick(0);
		o2.setTick(0);

		o1 = null;
		o2 = null;
	}

	public static void closeZammyDoor(final GameObject o) {
		if (!zammySideDoor) {
			return;
		}
		zammySideDoor = false;
		closeSideDoor(o);
	}

	public static void closeZammyMainDoors(final GameObject o) {
		int x1 = 2373;
		int y1 = 3120;
		int x2 = 2372;
		int y2 = 3120;
		int WEST_DOOR = 4430;
		int EAST_DOOR = 4429;
		RSObject westDoor = ObjectManager.getObjectByNewId(EAST_DOOR, x1, y1,
				o.getZ());
		if (westDoor == null) {
			// System.out.println("West door is null"); // east door XD
			return;
		}
		RSObject westDoor2 = ObjectManager.getObjectByNewId(-1,
				westDoor.getOriginalX(), westDoor.getOriginalY(), o.getZ());
		if (westDoor2 == null) {
			// System.out.println("West door2 is null");
			return;
		}
		RSObject eastDoor = ObjectManager.getObjectByNewId(WEST_DOOR, x2, y2,
				o.getZ());
		if (eastDoor == null) {
			// System.out.println("East door is null"); // west door XD
			return;
		}
		RSObject eastDoor2 = ObjectManager.getObjectByNewId(-1,
				eastDoor.getOriginalX(), eastDoor.getOriginalY(), o.getZ());
		if (eastDoor2 == null) {
			// System.out.println("East door2 is null");
			return;
		}
		
		o.setActive(false);
		
		westDoor.setTick(0);
		westDoor2.setTick(0);
		eastDoor.setTick(0);
		eastDoor2.setTick(0);

		westDoor = null;
		westDoor2 = null;
		eastDoor = null;
		eastDoor2 = null;
	}

	public static void collapseCave(int cave) {
		RSObject rso = ObjectManager.getObject(ROCKS[cave][0], ROCKS[cave][1], 0);
		if (rso == null) {
			return;
		}
//		Iterator<Player> iterator = gameRoom.keySet().iterator();
//		while (iterator.hasNext()) {
//			Player teamPlayer = iterator.next();
//
//			if (teamPlayer == null || !teamPlayer.isActive) {
//				iterator.remove();
//				return;
//			}
//
//			if ((teamPlayer.absX > CAVE_WALL[cave][0]
//					&& teamPlayer.absX < CAVE_WALL[cave][1])
//					&& (teamPlayer.absY > CAVE_WALL[cave][2]
//							&& teamPlayer.absY < CAVE_WALL[cave][3])) {
//				teamPlayer.sendMessage("The Cave has been collapsed on you");
//				int dmg = teamPlayer.getPlayerLevel()[3];
//				dmg = teamPlayer.dealDamage(new Hit(dmg, 0, 0));
//				teamPlayer.startGraphic(Graphic.create(2149));
//			}
//		}
		
		for (Player p : Server.getRegionManager().getLocalPlayersByLocation(rso.getX(), rso.getY(), rso.getHeight())) {
			if ((p.getX() > CAVE_WALL[cave][0]
					&& p.getX() < CAVE_WALL[cave][1])
					&& (p.getY() > CAVE_WALL[cave][2]
							&& p.getY() < CAVE_WALL[cave][3])) {
				p.sendMessage("The Cave has been collapsed on you");
				int dmg = p.getPlayerLevel()[3];
				dmg = p.dealDamage(new Hit(dmg, 0, 0));
				p.startGraphic(Graphic.create(2149));
			}
		}
		
		for (NPC n : Server.getRegionManager().getLocalNpcsByLocation(rso.getX(), rso.getY(), rso.getHeight())) {
			if ((n.getX() > CAVE_WALL[cave][0]
					&& n.getX() < CAVE_WALL[cave][1])
					&& (n.getY() > CAVE_WALL[cave][2]
							&& n.getY() < CAVE_WALL[cave][3])) {
				n.setDead(true);
			}
		}

		if (rso != null) {
			rso.setTick(0);
		}
		rso = null;
		setCaveStatus(cave, true);
	}

	/**
	 * Hint icons appear to your team when a enemy steals flag
	 *
	 * @param player
	 *            the player who took the flag
	 * @param t
	 *            team of the opponent team. (:
	 */
	public static void createFlagHintIcon(int x, int y, int flag) {
		Iterator<Player> iterator = gameRoom.keySet().iterator();
		while (iterator.hasNext()) {
			Player teamPlayer = iterator.next();
			if (!teamPlayer.isActive) {
				iterator.remove();
				continue;
			}
			if (getTeamNumber(teamPlayer) == flag) {
				continue;
			}
			teamPlayer.getPacketSender().createObjectHints(x, y, 170, 2);
		}
	}

	public static void createGroundItems() {
		
		GroundItem zammyMatches = new GroundItem(590, 2368, 3135, 0, 1, 50); // zammy matches
		GroundItem zammyBucket = new GroundItem(1925, 2371, 3128, 0, 1, 50); // zammy bucket
		GroundItem zammyBucket2 = new GroundItem(1925, 2371, 3127, 0, 1, 50); // zammy bucket
		GroundItem saraMatches = new GroundItem(590, 2431, 3072, 0, 1, 50); // sara matches
		GroundItem saraBucket = new GroundItem(1925, 2428, 3079, 0, 1, 50); // sara bucket
		GroundItem saraBucket2 = new GroundItem(1925, 2428, 3080, 0, 1, 50); // sara bucket
		ItemHandler.createGroundItem(zammyMatches, true);
		ItemHandler.createGroundItem(zammyBucket, true);
		ItemHandler.createGroundItem(zammyBucket2, true);
		ItemHandler.createGroundItem(saraMatches, true);
		ItemHandler.createGroundItem(saraBucket, true);
		ItemHandler.createGroundItem(saraBucket2, true);
		if (gameTimer > 0) {
			spawnTimer = 50;
		}
	}

	private static final int[] GAME_ITEMS = {4049, 1265, 1925, 1929, 4047, 4051, EXPLOSIVE_POTION, 4053, 4043,
			4042, 4041, 4037, 4039, 4515, 4513};

	/**
	 * This method will delete all items received in game. Easy to add items to
	 * the array. (:
	 *
	 * @param player
	 *            the player who want the game items deleted from.
	 */
	public static void deleteGameItems(Player player) {
		player.goldObtainedInMinigames = 0;
		switch (player.playerEquipment[PlayerConstants.playerWeapon]) {
			case 4037:
			case 4039:
				player.getItems().deleteEquipmentBySlot(PlayerConstants.playerWeapon);
				break;
		}
		removeCapes(player);
		for (int i = 0; i < GAME_ITEMS.length; i++) {
			int item = GAME_ITEMS[i];
			if (player.getItems().playerHasItem(item)) {
				player.getItems().deleteItem2(item,
						player.getItems().getItemAmount(item));
			}
		}
	}

	/**
	 * Method we use to handle the flag dropping
	 *
	 * @param player
	 *            the player who dropped the flag/died
	 * @param flagId
	 *            the flag item ID
	 */
	public static void dropFlag(Player player, int flagId) {
		int object = -1;
		int objectX = player.absX;
		int objectY = player.absY;
		int objectZ = player.getZ();
		
		player.getItems().deleteEquipmentBySlot(PlayerConstants.playerWeapon);

		if (flagId == SARA_BANNER) { // sara
			if (saraFlag == 2) {
				return;
			}
			setSaraFlag(2);
			object = 4900;
		} else if (flagId == ZAMMY_BANNER) { // zammy
			if (zammyFlag == 2) {
				return;
			}
			setZammyFlag(2);
			object = 4901;
		}
		int rocks = rockLocation2(player.absX, player.absY);
		if (rocks != -1) {
			for (int i = 0; i < 4; i++) {
				for (int j = -1; j < 2; j++) {
					if (player.absY + j == CAVE_WALL[rocks][i]
							&& (rocks == 0 || rocks == 2)) {
						objectY = CAVE_WALL[rocks][i];
						objectX = player.absX;
					}
					if (player.absX + j == CAVE_WALL[rocks][i]
							&& (rocks == 1 || rocks == 3)) {
						objectX = CAVE_WALL[rocks][i];
						objectY = player.absY;
					}
				}
			}
		} else if ((player.absX >= 2377 && player.absX <= 2379)
				&& (player.absY >= 3084 && player.absY <= 3088)) {
			if (player.absY >= 3086) {
				objectX = 2377;
				objectY = 3089;
			} else {
				objectX = 2378;
				objectY = 3083;
			}
		} else if ((player.absX >= 2418 && player.absX <= 2420)
				&& (player.absY >= 3123 && player.absY <= 3125)) {
			int random = -1;
			if (player.absX == 2419 && player.absY == 3124) {
				random = Misc.random(1);
				// player.sendMessage("random "+random);
			}
			if (player.absY > 3124 || random == 0) {
				objectX = 2418;
				objectY = 3126;
			} else if (player.absY < 3214 || random == 1) {
				objectX = 2420;
				objectY = 3122;
			}
		} else {
			objectX = player.absX;
			objectY = player.absY;
		}
		
		if (!RSConstants.inCastleWarsArena(objectX, objectY)) { // just a precaution for now.
			returnFlagOnly(player.getName(), flagId);
			return;
		}

		final String playerName = player.getName();
		final int objectFinal = object;
		final int obX = objectX;
		final int obY = objectY;
		final int obZ = player.getZ();
		final int objectFace = 0;
		final int objectType = 10;
		final int objectClass = 1; // 1 for castlewars objects

		if (ObjectManager.objectInSpotTypeTen(obX, obY, obZ)) { // another
																// object in
																// this location
																// so pause the
																// object
																// spawning
			
			if (RegionClip.getClippingDirection(objectX+1, objectY, obZ, 1, 0, null)) {
				objectX++;
			} else if (RegionClip.getClippingDirection(objectX, objectY+1, obZ, 0, 1, null)) {
				objectY++;
			} else if (RegionClip.getClippingDirection(objectX-1, objectY, obZ, -1, 0, null)) {
				objectX--;
			} else if (RegionClip.getClippingDirection(objectX, objectY-1, obZ, 0, -1, null)) {
				objectY--;
			}
			
			
			final RSObject rso = new RSObject(object, objectX, objectY, objectZ,
					objectFace, objectType, -1, GAME_TIMER);
			rso.setObjectClass(objectClass);
			ObjectManager.addObject(rso);
			
//			Server.getTaskScheduler().schedule(new Task() {
//
//				int timer = 0;
//
//				@Override
//				protected void execute() {
//					if (!ObjectManager.objectInSpotTypeTen(obX, obY, obZ)) {
//							final RSObject rso = new RSObject(objectFinal, obX, obY, obZ, objectFace,
//									objectType, -1, GAME_TIMER);
//							rso.setObjectClass(objectClass);
//							ObjectManager.addObject(rso);
//
//						this.stop();
//						return;
//					} else { // object is on top of here
//						
//					}
//					if (timer > 10) { // bugged object most likely
//						returnFlagOnly(playerName, flagId);
//						this.stop();
//					}
//					timer++;
//				}
//			});
		} else {

			final RSObject rso = new RSObject(object, objectX, objectY, objectZ,
					objectFace, objectType, -1, GAME_TIMER);
			rso.setObjectClass(objectClass);
			ObjectManager.addObject(rso);
		}

		createFlagHintIcon(objectX, objectY, (flagId == SARA_BANNER ? 1 : 2));
	}

	/*
	 * Method we use to end an ongoing cw game.
	 */
	public static void endGame() {
		// Iterator<Player> iterator = gameRoom.keySet().iterator();
		// while (iterator.hasNext()) {
		// Player player = (Player) iterator.next();
		// int team = gameRoom.get(player);
		for (int i = 0; i < PlayerHandler.players.length; i++) {
			if (PlayerHandler.players[i] == null
					|| !PlayerHandler.players[i].isActive) {
				continue;
			}
			if (!PlayerHandler.players[i].inCwGame) {
				continue;
			}
			Player player = PlayerHandler.players[i];
			
			// player.onPlatform = false;
			player.getPA().safeDeathSecureTimer();
			deleteGameItems(player);
			player.setDead(false);
			player.respawnTimer = -5;
			// player.restoreSkills();
			// player.cwGames++;
			player.inCwGame = false;
			removeCapes(player);
			final int teleX = 2440 + Misc.random(3), teleY = 3089 - Misc.random(3), teleZ = 0;
			player.getPA().movePlayer(teleX, teleY, 0);
			player.changeRespawn(teleX, teleY, 0);
			player.sendMessage(
					"[<col=13434880>CASTLE WARS</col>] The Castle Wars Game has ended!");
			// player.sendMessage("[<col=13434880>CASTLE WARS</col>] Kills:
			// <col=13434880> " + player.cwKills + " </col>Deaths:<col=13434880>
			// " + player.cwDeaths + "</col> Games Played: <col=13434880>" +
			// player.cwGames + "</col>.");
			player.getPacketSender().createPlayerHints(10, -1);
			player.getPacketSender().createObjectHints(1, 1, 170, -1);
			// deleteGameItems(player);
			// Server.objectManager.removeCwFlags(player);
			player.getPA().restorePlayer(false);
			player.underAttackBy = 0;
			player.underAttackBy2 = 0;
			player.getCombat().resetPrayers();
			final int CLANWINPOINTS = 250;
			final int CLANLOSEPOINTS = 75;
			final int CLAN_TIE_POINTS = 125;
			if (gameTimer < 5) {
				int team = player.castleWarsTeam; // gameRoom.get(player);
				if (scores[0] == scores[1]) {
					player.getItems().addOrDrop(new GameItem(4067, 2));
					player.sendMessage(
							"Tie game! You gain 2 CastleWars tickets!");
					if (player.getClan() != null) {
						player.getClan().addClanPoints(player, CLAN_TIE_POINTS,
								"cw_tie");
					}
				} else if (team == 1) {
					if (scores[0] > scores[1]) {
						player.getExtraHiscores().incCastleWarsWins();
						player.getItems().addOrDrop(new GameItem(4067, 3));
						player.sendMessage(
								"You won the CastleWars Game. You received 3 CastleWars Tickets!");
						if (player.getClan() != null) {
							player.getClan().addClanPoints(player,
									CLANWINPOINTS, "cw_win");
						}
					} else if (scores[0] < scores[1]) {
						player.getItems().addOrDrop(new GameItem(4067, 1));
						player.sendMessage(
								"You lost the CastleWars Game. You received 1 CastleWars Tickets!");
						if (player.getClan() != null) {
							player.getClan().addClanPoints(player,
									CLANLOSEPOINTS, "cw_lose");
						}
					}
				} else if (team == 2) {
					if (scores[1] > scores[0]) {
						player.getExtraHiscores().incCastleWarsWins();
						player.getItems().addOrDrop(new GameItem(4067, 3));
						player.sendMessage(
								"You won the CastleWars Game. You received 3 CastleWars Tickets!");
						if (player.getClan() != null) {
							player.getClan().addClanPoints(player,
									CLANWINPOINTS, "cw_win");
						}
					} else if (scores[1] < scores[0]) {
						player.getItems().addOrDrop(new GameItem(4067, 1));
						player.sendMessage(
								"You lost the CastleWars Game. You received 1 CastleWars Tickets!");
						if (player.getClan() != null) {
							player.getClan().addClanPoints(player,
									CLANLOSEPOINTS, "cw_lose");
						}
					}
				}
				if (EXTRA_REWARD) {
					player.getItems().addOrDrop(new GameItem(4067, 1));
					player.sendMessage("You have received extra ticket from the castle war event!");
				}
			}
			player.castleWarsTeam = 0;
			player.soulWarsTeam = Team.NONE;
			// player.gameInterface = -1;
		}
		resetGame();
		loyalToTeam.clear();
	}

	public static int findLanthus() {
		for (NPC n : NPCHandler.npcs) {
			if (n == null || n.isDead()) {
				continue;
			}
			if (n.npcType == 1526) {
				return n.npcIndex;
			}
		}
		return 0;
	}

	public static int getNextGameMinutes() {
		int time = gameTimer + waitTimer;
		if (time <= 0)
			time = 1;

		time = (int) Misc.ticksIntoSeconds(time) / 60;

		return time;
	}

	/**
	 * Method we use to get the saradomin players!
	 *
	 * @return the amount of players in the saradomin team!
	 */
	public static int getSaraPlayers() {
		int players = 0;
		Iterator<Integer> iterator = gameRoom.values().iterator();
		while (iterator.hasNext()) {
			if (iterator.next() == 1) {
				players++;
			}
		}
		return players;

	}

	public static int getSaraPlayersWaiting() {
		int amount = 0;
		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[0][0], WAIT_ROOM[0][1], 0)
				.getPlayers()) { // sara room
			if (!player.isActive) {
				continue;
			}
			if (!RSConstants.cwSaraWaitingRoom(player.getX(), player.getY())) {
				continue;
			}
			amount++;
		}
		return amount;
	}

	public static int getSaraPlayersWaitingBonuses() {
		int amount = 0;
		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[0][0], WAIT_ROOM[0][1], 0)
				.getPlayers()) { // sara room
			if (!player.isActive) {
				continue;
			}
			if (!RSConstants.cwSaraWaitingRoom(player.getX(), player.getY())) {
				continue;
			}
			amount += player.getItems().getTotalBonuses();
		}
		return amount;
	}

	/**
	 * This method is used to get the teamNumber of a certain player
	 *
	 * @param player
	 * @return 1 for Sara team - 2 for Zammy team
	 */
	public static int getTeamNumber(Player player) {
		if (player == null) {
			return -1;
		}
		return player.castleWarsTeam;
		// if (gameRoom.containsKey(player)) {
		// return gameRoom.get(player);
		// }
		// return -1;
	}

	public static int getWaitTimer() {
		return waitTimer;
	}

	public static int getGameTimer() {
		return gameTimer;
	}

	/**
	 * Methode we use to get the zamorak players
	 *
	 * @return the amount of players in the zamorakian team!
	 */
	public static int getZammyPlayers() {
		int players = 0;
		Iterator<Integer> iterator = gameRoom.values().iterator();
		while (iterator.hasNext()) {
			if (iterator.next() == 2) {
				players++;
			}
		}
		return players;

	}

	public static int getZammyPlayersWaiting() {
		int amount = 0;
		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[1][0], WAIT_ROOM[1][1], 0)
				.getPlayers()) { // zammy room
			if (player == null || player.disconnected
					|| player.forceDisconnect) {
				continue;
			}
			if (!RSConstants.cwZammyWaitingRoom(player.getX(), player.getY())) {
				continue;
			}
			amount++;
		}
		return amount;
	}

	public static int getZammyPlayersWaitingBonuses() {
		int amount = 0;
		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[1][0], WAIT_ROOM[1][1], 0)
				.getPlayers()) { // zammy room
			if (player == null || player.disconnected
					|| player.forceDisconnect) {
				continue;
			}
			if (!RSConstants.cwZammyWaitingRoom(player.getX(), player.getY())) {
				continue;
			}
			amount += player.getItems().getTotalBonuses();
		}
		return amount;
	}



	/**
	 *
	 */
	public static boolean hasFlag(Player c) {
		if (c == null) {
			return false;
		}
		if (c.getItems().playerHasEquippedWeapon(SARA_BANNER)) {
			return true;
		}
		if (c.getItems().playerHasEquippedWeapon(ZAMMY_BANNER)) {
			return true;
		}
		return false;
	}

	public static void init() {

		for (int bannedItem : BANNED_ITEMS) {
			bannedItems.add(bannedItem);
		}
		bannedItems.add(Creation.POUCH_ID);
		for (Creation.Data cData : Creation.Data.values) {
			bannedItems.add(cData.getPouchId());
		}
		
		lanthusIndex = findLanthus();

		Server.getTaskScheduler().schedule(new Task(1) {

			@Override
			protected void execute() {

				if (announceTimer > 0) {
					announceTimer--;
				}

				if (announceTimer == 0) {
					announceTimer = 10;
					if (lanthusIndex == 0) {
						lanthusIndex = findLanthus();
					} else {
						if (NPCHandler.npcs[lanthusIndex] != null) {
							announceLanthus(NPCHandler.npcs[lanthusIndex]);
						}
					}
				}

				if (spawnTimer > 0) {
					spawnTimer--;
				}
				// if (properTimer > 0) {
				// properTimer--;
				// return;
				// } else {
				// properTimer = 1;
				// }

				if (waitTimer > 0) {
					waitTimer--;
					/// updatePlayers();
				} else if (waitTimer == 0) {
					startGame();
				}

				if (gameTimer > 0) {
					gameTimer--;
//System.out.println("gameTimer:"+gameTimer);
					// updateInGamePlayers();
					if (spawnTimer == 0) {
						createGroundItems();
					}
					if (gameTimer == 50) { // if 1 minutes are left, can't
											// rejoin anymore
						canRejoin = false;
					} else if (gameTimer == GAME_TIMER / 2) { // half minute
																// mark, set max
																// players of
																// each team
						lockedSaraCount = (getSaraPlayers() >= getZammyPlayers()
								? getSaraPlayers()
								: getZammyPlayers());
						lockedZammyCount = (getSaraPlayers() >= getZammyPlayers()
								? getSaraPlayers()
								: getZammyPlayers());
					}
					if ((getSaraPlayers() == 0 || getZammyPlayers() == 0)
							&& Config.RUN_ON_DEDI) {
//						endGame();
					} else {
						if (sendDialogueDelay > 0) {
							sendDialogueDelay--;
						}
						if (canRejoin && sendDialogueDelay == 0) {
							sendWaitingRoomDialogueToJoinGame();
							sendDialogueDelay = 3;
						}
					}
				} else if (gameTimer == 0) {
					endGame();
				}

			}
		});
	}

	/**
	 * Method we use for checking if the player is in the gameRoom
	 *
	 * @param player
	 *            player who will be checking
	 * @return
	 */
	public static boolean isInCw(Player player) {
		return gameRoom.containsKey(player);
	}

	/**
	 * Method we use for checking if the player is in the waitingRoom
	 *
	 * @param player
	 *            player who will be checking
	 * @return
	 */
	public static boolean isInCwWait(Player player) {
		return waitingRoom.containsKey(player);
	}

	public static boolean isInDoorLocations(int x, int y) {
		if (x >= 2426 && x <= 2427 && y >= 3087 && y <= 3088) { // sara double
																// doors
			return true;
		}
		if (y == 3073 && (x >= 2414 && x <= 2416)) { // sara side doors
			return true;
		}

		if (x >= 2372 && x <= 2373 && y >= 3119 && y <= 3120) { // zammy double
																// doors
			return true;
		}
		if (y == 3134 && (x >= 2383 && x <= 2385)) { // zammy side doors
			return true;
		}

		return false;
	}

	/**
	 * The leaving method will be used on click object or log out
	 *
	 * @param player
	 *            player who wants to leave
	 */
	public static void leaveWaitingRoom(Player player) {
		if (player == null) {
			return;
		}

		if (CastleWars.waitTimer >= 0 && CastleWars.waitTimer <= 1) {
			return;
		}

		if (waitingRoom.containsKey(player)) {
			waitingRoom.remove(player);
			player.getPacketSender().createPlayerHints(10, -1);
			player.sendMessage("You left your team!");
			removeCapes(player);
			player.getPA().movePlayer(2439 + Misc.random(4),
					3085 + Misc.random(5), 0);
			return;
		}
		removeCapes(player);
		deleteGameItems(player);
		player.getPA().movePlayer(2439 + Misc.random(4), 3085 + Misc.random(5),
				0);
	}

	public static void mineRocks(final Player c, final int id, final int X,
			final int Y, final GameObject o2, Pickaxes pickaxe) {

		c.startAnimation(pickaxe.getAnimation());
		
		c.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				c.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer b) {
				c.skillingTick++;
				
				if (c.skillingTick % 3 == 0) {
					c.skillingTick = 0;
					
					c.startAnimation(pickaxe.getAnimation());
					
					GameObject currentObj = RegionClip.getGameObject(X, Y, c.getZ(), o2.getType(), true);
					if (currentObj == null) {
						b.stop();
						return;
					}
					
					switch (currentObj.getId()) {
					case 4437:
						if (successRoll(c.getSkills().getLevel(Skills.MINING))) {
							if (!clearHalfRocks(c, RegionClip.getGameObject(id, X, Y, 0))) {
								c.resetAnimations();
								b.stop();
								c.startAnimation(Animation.RESET);
								return;
							}
						}
						break;
						
					case 4438:
						if (successRoll(c.getSkills().getLevel(Skills.MINING))) {
							c.resetAnimations();
							clearRocksCompletely(c, o2);
							c.startAnimation(Animation.RESET);
							b.stop();
							return;
						}
						break;
						
					default:
						c.startAnimation(Animation.RESET);
						b.stop();
						break;
					}
				}
				
			}
		}, 1);
		
	}
	
	public static boolean successRoll(int playerLevel) {
		return Skills.successRoll(1, 255, playerLevel);
	}

	public static void moveToSaraSpawn(final Player c) {
		int x = GAME_ROOM[0][0] + Misc.random(3);
		int y = GAME_ROOM[0][1] - Misc.random(3);
		while (!RegionClip.canStandOnSpot(x, y, 1)) {
			x = GAME_ROOM[0][0] + Misc.random(3);
			y = GAME_ROOM[0][1] - Misc.random(3);
		}
		c.getPA().movePlayer(x, y, 1);
		updateDiscord(c);
	}

	public static void moveToZammySpawn(final Player c) {
		int x = GAME_ROOM[1][0] + Misc.random(3);
		int y = GAME_ROOM[1][1] - Misc.random(3);
		while (!RegionClip.canStandOnSpot(x, y, 1)) {
			x = GAME_ROOM[1][0] + Misc.random(3);
			y = GAME_ROOM[1][1] - Misc.random(3);
		}
		c.getPA().movePlayer(x, y, 1);
		updateDiscord(c);
	}
	
	public static void updateDiscord(Player p) {
		p.getPacketSender().sendDiscordUpdate("", "CastleWars", 0, 0, discordStartTime, discordEndTime);
	}

	/*
	 * CastleWars Doors
	 */
	public static void openSaraDoor(final GameObject o) {
		if (saraSideDoor) {
			return;
		}
		saraSideDoor = true;
		o.setActive(false);

		final RSObject d1 = new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(), 0,
				o.getId(), GAME_TIMER);
		final RSObject d2 = RSObject.createDoor(o.getId() + 1, o.getX() - 1, o.getY(), o.getZ(),
				(o.getOrientation() + 1) % 4, o.getType(), -1, o.getX(), o.getY(), GAME_TIMER);
		d1.setObjectClass(1);
		d2.setObjectClass(1);
		ObjectManager.addObject(d1);
		ObjectManager.addObject(d2);
	}

	public static void openSaraMainDoors(final GameObject o) {
		int id1 = 4423;
		int x1 = 2426;
		int y1 = 3088; // west object loc
		int id2 = 4424;
		int x2 = 2427;
		int y2 = 3088; // east object loc
		GameObject o2;
		if (o.getX() == x1) { // this is west door, grab east door
			o2 = RegionClip.getGameObject(id2, x2, y2, o.getZ());
		} else { // east door, grab west door
			o2 = RegionClip.getGameObject(id1, x1, y1, o.getZ());
		}
		if (o2 == null) {
			// System.out.println("O2 is null- FIX THIS");
			return;
		}
		o.setActive(false);
		o2.setActive(false);
		final RSObject d1 = new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(),
				o.getId(), GAME_TIMER); // Remove First Object
		final RSObject d2 = new RSObject(-1, o2.getX(), o2.getY(), o2.getZ(), o2.getOrientation(),
				o2.getType(), o2.getId(), GAME_TIMER); // remove
																	// Second
																	// object
		final RSObject d3 = RSObject.createDoor(o.getId() + 2/* 4425 */, o.getX(), o.getY() - 1, o.getZ(),
				(o.getId() < o2.getId()
						? (o.getOrientation() + 1)
						: (o.getOrientation() + 3))
						% 4/* (o.getId() == 4423 ? 0 : 2) *//* 0 */,
				o.getType(), -1, o.getX(), o.getY(), GAME_TIMER); // Add First Object
		final RSObject d4 = RSObject.createDoor(o2.getId() + 2/* 4426 */, o2.getX(), o2.getY() - 1,
				o.getZ(), (o.getId() < o2.getId()
						? (o.getOrientation() + 3)
						: (o.getOrientation() + 1))
						% 4/* (o2.getId() == 4423 ? 0 : 2) *//* 2 */,
				o2.getType(), -1, o2.getX(), o2.getY(),
				GAME_TIMER); // Add Second Object
		
		d1.setObjectClass(1);
		d2.setObjectClass(1);
		d3.setObjectClass(1);
		d4.setObjectClass(1);
		
		d3.setOriginalFace(o.getOrientation());
		d4.setOriginalFace(o2.getOrientation());

		ObjectManager.addObject(d1);
		ObjectManager.addObject(d2);
		ObjectManager.addObject(d3);
		ObjectManager.addObject(d4);
	}

	public static void openZammyDoor(final GameObject o) {
		if (zammySideDoor) {
			return;
		}
		zammySideDoor = true;
		
		o.setActive(false);

		final RSObject d1 = new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(), 0,
				o.getId(), GAME_TIMER);
		final RSObject d2 = RSObject.createDoor(o.getId() + 1, o.getX() + 1, o.getY(), o.getZ(),
				(o.getOrientation() + 1) % 4, o.getType(), -1, o.getX(), o.getY(), GAME_TIMER);
		d1.setObjectClass(1);
		d2.setObjectClass(1);
		ObjectManager.addObject(d1);
		ObjectManager.addObject(d2);
	}

	public static void openZammyMainDoors(final GameObject o) {
		int id1 = 4427;
		int x1 = 2373;
		int y1 = 3119;
		int id2 = 4428;
		int x2 = 2372;
		int y2 = 3119;
		GameObject o2;
		if (o.getX() == x1) { // this is west door, grab east door
			o2 = RegionClip.getGameObject(id2, x2, y2, o.getZ());
		} else { // east door, grab west door
			o2 = RegionClip.getGameObject(id1, x1, y1, o.getZ());
		}
		if (o2 == null) {
			// System.out.println("O2 is null- FIX THIS");
			return;
		}
		o.setActive(false);
		o2.setActive(false);
		final RSObject d1 = new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(),
				o.getId(), GAME_TIMER); // Remove First Object
		final RSObject d2 = new RSObject(-1, o2.getX(), o2.getY(), o2.getZ(), o2.getOrientation(),
				o2.getType(), o2.getId(), GAME_TIMER); // remove
																	// Second
																	// object
		final RSObject d3 = RSObject.createDoor(o.getId() + 2/* 4425 */, o.getX(), o.getY() + 1, o.getZ(),
				(o.getId() < o2.getId()
						? (o.getOrientation() + 1)
						: (o.getOrientation() + 3))
						% 4/* (o.getId() == 4423 ? 0 : 2) *//* 0 */,
				o.getType(), -1, o.getX(), o.getY(), GAME_TIMER); // Add First Object
		final RSObject d4 = RSObject.createDoor(o2.getId() + 2/* 4426 */, o2.getX(), o2.getY() + 1,
				o.getZ(), (o.getId() < o2.getId()
						? (o.getOrientation() + 3)
						: (o.getOrientation() + 1))
						% 4/* (o2.getId() == 4423 ? 0 : 2) *//* 2 */,
				o2.getType(), -1, o2.getX(), o2.getY(),
				GAME_TIMER); // Add Second Object

		d1.setObjectClass(1);
		d2.setObjectClass(1);
		d3.setObjectClass(1);
		d4.setObjectClass(1);
		
		ObjectManager.addObject(d1);
		ObjectManager.addObject(d2);
		ObjectManager.addObject(d3);
		ObjectManager.addObject(d4);
	}

	public static void pickLock(final Player c, final GameObject o) { //TODO: rewrite this garbage
		// c.sendMessage("Team number:"+getTeamNumber(c));
		if (o.getId() == 4465 && ((c.getX() == 2415 && c.getY() == 3073)
				|| getTeamNumber(c) == 1)) {
			openSaraDoor(o);
			return;
		}
		if (o.getId() == 4467 && ((c.getX() == 2384 && c.getY() == 3134)
				|| getTeamNumber(c) == 2)) {
			openZammyDoor(o);
			return;
		}
		
		c.startAnimation(2246);
		
		c.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer b) {
				final GameObject o2 = RegionClip.getGameObject(o.getX(), o.getY(), o.getZ(), o.getType(), true);
				if (o2 == null) {
					b.stop();
					return;
				}
				if (!o.isActive() || c.isDead() == true || c.forceDisconnect) {
					b.stop();
					return;
				}
				if (o2.getId() == 4465 && saraSideDoor) {
					b.stop();
					return;
				}
				if (o2.getId() == 4467 && zammySideDoor) {
					b.stop();
					return;
				}
				c.startAnimation(2246);
				c.skillingTick++;
				if (c.skillingTick % 2 == 0) {
					c.skillingTick = 0;
					if (successRoll(c.getSkills().getLevel(Skills.THIEVING))) {
						c.resetAnimations();
						c.startAnimation(Animation.RESET);
						if (o2.getId() == 4467) {
							openZammyDoor(o2);
						} else if (o2.getId() == 4465) {
							openSaraDoor(o2);
						}
						b.stop();
					}
				}

			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, 1);
	}

	/**
	 * Method we use to pickup the flag when it was dropped/lost
	 *
	 * @param Player
	 *            the player who's picking it up
	 * @param objectId
	 *            the flag object id.
	 */
	public static void pickupFlag(final Player c, final GameObject o) {
		if (!gameRoom.containsKey(c)) {
			return;
		}

		RSObject groundFlag = ObjectManager.getObjectByNewId(o.getId(),
				o.getX(), o.getY(), c.heightLevel);
		if (groundFlag == null) {
			c.sendMessage("What what chicken butt?");
			return;
		}

		int team = getTeamNumber(c);
		int flag = 0;
		if (o.getId() == 4900) { // clicking on picking up sara flag on ground
			if (saraFlag != 2) { // flag is supposed to be dropped
				// c.sendMessage("Flag already taken 'test'");
				return;
			}

			// if (team == 1) { // sara team cannot pickup their own flag. flag
			// moves only one direction
			// c.sendMessage("You cannot pickup your own flag.");
			// return;
			// }

			if (c.playerEquipment[PlayerConstants.playerWeapon] > 0) {
				// c.sendMessage("Please remove your weapon before attempting to
				// get the flag again!");
				// return;
				if (!c.getItems().removeItem(
						c.playerEquipment[PlayerConstants.playerWeapon],
						PlayerConstants.playerWeapon)) {
					c.sendMessage(
							"You do not have enough space in your inventory to unequip your weapon.");
					return;
				}
			}
			if (c.playerEquipment[PlayerConstants.playerShield] > 0) {
				if (!c.getItems().removeItem(
						c.playerEquipment[PlayerConstants.playerShield],
						PlayerConstants.playerShield)) {
					c.sendMessage(
							"You do not have enough space in your inventory to unequip your shield.");
					return;
				}
			}

			groundFlag.setTick(0);
			flag = 1;
			if (team == 1
					&& (c.absX >= CASTLE_ZONES[team - 1][0]
							&& c.absX <= CASTLE_ZONES[team - 1][1])
					&& (c.absY >= CASTLE_ZONES[team - 1][2]
							&& c.absY <= CASTLE_ZONES[team - 1][3])) { // if
																		// team
																		// picks
																		// up
																		// their
																		// own
																		// flag
																		// inside
																		// their
																		// castle
																		// then
																		// return
																		// it
				if (saraFlag == 0) {
					return;
				}
				setSaraFlag(0);
			} else {
				if (saraFlag == 1) {
					return;
				}
				setSaraFlag(1);
				addFlag(c, 4037);
			}
		} else if (o.getId() == 4901) { // zammy
			if (zammyFlag != 2) {
				return;
			}

			if (c.playerEquipment[PlayerConstants.playerWeapon] > 0) {
				if (!c.getItems().removeItem(
						c.playerEquipment[PlayerConstants.playerWeapon],
						PlayerConstants.playerWeapon)) {
					c.sendMessage(
							"You do not have enough space in your inventory to unequip your weapon.");
					return;
				}
			}
			if (c.playerEquipment[PlayerConstants.playerShield] > 0) {
				if (!c.getItems().removeItem(
						c.playerEquipment[PlayerConstants.playerShield],
						PlayerConstants.playerShield)) {
					c.sendMessage(
							"You do not have enough space in your inventory to unequip your shield.");
					return;
				}
			}

			groundFlag.setTick(0);
			flag = 2;
			if (team == 2 && ((c.absX >= CASTLE_ZONES[team - 1][0]
					&& c.absX <= CASTLE_ZONES[team - 1][1])
					&& (c.absY >= CASTLE_ZONES[team - 1][2]
							&& c.absY <= CASTLE_ZONES[team - 1][3]))) {
				if (zammyFlag == 0) {
					return;
				}
				setZammyFlag(0);
			} else {
				if (zammyFlag == 1) {
					return;
				}
				setZammyFlag(1);
				addFlag(c, 4039);
			}
		}

		Iterator<Player> iterator = gameRoom.keySet().iterator();
		while (iterator.hasNext()) {
			Player teamPlayer = iterator.next();
			if (!teamPlayer.isActive) {
				iterator.remove();
				continue;
			}
			if (flag == 0) {
				continue;
			}
			if (flag == getTeamNumber(teamPlayer)) {
				continue;
			}
			teamPlayer.getPacketSender().createObjectHints(o.getX(), o.getY(),
					170, -1);
		}
		updateHintIcon();
		iterator = null;
		return;
	}

	public static void rejoinGameToSara(final Player c) {
		if (!canRejoinGame()) {
			c.sendMessage("You can no longer rejoin the game.");
			return;
		}

		if (getSaraPlayers() > getZammyPlayers()
				|| getSaraPlayers() >= lockedSaraCount) {
			c.sendMessage(
					"Sorry but someone has already filled the empty spot.");
			return;
		}

		addCapes(c, 1);
		addToSaraGame(c);
	}

	public static void rejoinGameToZammy(final Player c) {
		if (!canRejoinGame()) {
			c.getPA().closeAllWindows();
			c.sendMessage("You can no longer rejoin the game.");
			return;
		}

		if (getZammyPlayers() > getSaraPlayers()
				|| getZammyPlayers() >= lockedZammyCount) {
			c.getPA().closeAllWindows();
			c.sendMessage(
					"Sorry but someone has already filled the empty spot.");
			return;
		}

		addCapes(c, 2);
		addToZammyGame(c);
	}

	/**
	 * Will remove a cape from a player's equip
	 *
	 * @param player
	 *            the player
	 * @param capeId
	 *            the capeId
	 */
	public static void removeCapes(Player player) {
		player.displayEquipment[PlayerConstants.playerCape] = -1;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}

	/**
	 * Method we use to remove a player from the game
	 *
	 * @param player
	 *            the player we want to be removed
	 */
	public static void removePlayerFromCw(Player player) {
		if (player == null || !player.isActive) {
			return;
		}
		player.setWaitingRoomCampTimer(0);
		if (gameRoom.containsKey(player)) {
			if (!loyalToTeam.containsKey(player.UUID)) {
				loyalToTeam.put(player.UUID, player.castleWarsTeam);
			}
			/*
			 * Logging/leaving with flag
			 */
			player.sendMessage(
					"[<col=13434880>CASTLE WARS</col>] The Casle Wars Game has ended for you!");
			player.getPacketSender().createPlayerHints(10, -1);
			gameRoom.remove(player);
		}

		if (player.getItems().playerHasEquipped(SARA_BANNER)) {
			if (saraFlag != 2) {
				player.getItems().removeItem(player.playerEquipment[PlayerConstants.playerWeapon], PlayerConstants.playerWeapon);
				setSaraFlag(2); // dropped flag
			}
		} else if (player.getItems().playerHasEquipped(ZAMMY_BANNER)) {
			if (zammyFlag != 2) {
				player.getItems().removeItem(player.playerEquipment[PlayerConstants.playerWeapon], PlayerConstants.playerWeapon);
				setZammyFlag(2); // dropped flag
			}
		}
		player.inCwGame = false;
		removeCapes(player);
		deleteGameItems(player);
		player.getPA().movePlayer(2440, 3089, 0);

		player.getPA().safeDeathSecureTimer();
		player.getPA().restorePlayer(false);

		if (gameStarted && (getZammyPlayers() <= 0 || getSaraPlayers() <= 0)
				&& !Config.SERVER_DEBUG) {
//			endGame();
		}
		player.castleWarsTeam = 0;
		player.soulWarsTeam = Team.NONE;
		player.setDefaultDiscordStatus();
	}

	public static void repairSaraMainDoors(final GameObject o) {
		final RSObject rso1 = new RSObject(4423, 2426, 3088, o.getZ(), 3, 0, 4423, GAME_TIMER);
		final RSObject rso2 = new RSObject(4424, 2427, 3088, o.getZ(), 3, 0, 4424, GAME_TIMER);
		rso1.setObjectClass(1);
		rso2.setObjectClass(1);
		ObjectManager.addObject(rso1);
		ObjectManager.addObject(rso2);
		saraMainGateHP = 100;
	}

	public static void repairZammyMainDoors(final GameObject o) {
		final RSObject rso1 = new RSObject(4427, 2373, 3119, o.getZ(), 1, 0, 4427, GAME_TIMER);
		final RSObject rso2 = new RSObject(4428, 2372, 3119, o.getZ(), 1, 0, 4428, GAME_TIMER);
		rso1.setObjectClass(1);
		rso2.setObjectClass(1);
		ObjectManager.addObject(rso1);
		ObjectManager.addObject(rso2);
		zammyMainGateHP = 100;
	}

	/**
	 * reset the game variables
	 */
	public static void resetGame() {
		ObjectManager.revertObjectsByClass(1);
		/*
		 * northCave = true; southCave = true; eastCave = true; westCave = true;
		 */

		for (int i = 0; i < 4; i++) {
			setCaveStatus(i, true);
		}
		Barricades.killAllCades(true);

		lockedSaraCount = 200;
		lockedZammyCount = 200;

		// closeSaraDoor(2414, 3073, 0);
		// closeZammyDoor(2385, 3134, 0);
		// closeSaraMainDoors(2426, 3088, 0);
		// closeZammyMainDoors(2373,3119, 0);
		saraSideDoor = false;
		zammySideDoor = false;
		saraCatapult = true;
		zammyCatapult = true;
		zammyMainGateHP = 100;
		saraMainGateHP = 100;
		spawnTimer = -1;
		setSaraFlag(0);
		setZammyFlag(0);
		scores[0] = 0;
		scores[1] = 0;
		gameTimer = -1;
		sendDialogueDelay = 10;
		// System.out.println("Resetting Castle Wars game.");
		waitTimer = GAME_START_TIMER;
		gameStarted = false;
		canRejoin = false;
		gameRoom.clear();
	}

	/**
	 * Method to add score to scoring team
	 *
	 * @param player
	 *            the player who scored
	 * @param banner
	 *            banner id!
	 */
	public static void returnFlag(Player player, int wearItem, GameObject o) {
		if (player == null) {
			return;
		}

		if (wearItem != SARA_BANNER && wearItem != ZAMMY_BANNER) {
			return;
		}

		if (!gameRoom.containsKey(player)) {
			return;
		}

		int team = player.castleWarsTeam; // gameRoom.get(player);

		if (team == 1) {
			if (wearItem == SARA_BANNER) { // returning the flag
				if (saraFlag == 0) {
					return;
				}
				setSaraFlag(0);
				//player.sendMessage("returned the sara flag!");
			} else if (wearItem == ZAMMY_BANNER) { // capturing the zamorak flag into the saradomin standard
				// if (o.getId() == CastleWarsObjects.SARA_FLAG2 || o.getId() == CastleWarsObjects.ZAMMY_FLAG2) {
				// player.sendMessage("You must recover your flag first.");
				// return;
				// }
				if (zammyFlag == 0 || (o.getId() != CastleWarsObjects.SARA_FLAG2 && o.getId() != CastleWarsObjects.SARA_FLAG1)) {
					return;
				}
				setZammyFlag(0);
				scores[0]++;
				player.sendMessage("The team of Saradomin scores 1 point!");
			}
		} else if (team == 2) {
			if (wearItem == ZAMMY_BANNER) { // returning the zamorak flag to zamorak standard
				if (zammyFlag == 0) {
					return;
				}
				setZammyFlag(0);
				//player.sendMessage("returned the zammy flag!");
			} else if (wearItem == SARA_BANNER) { // capturing the saradomin flag into the zamorak standard
				// if (o.getId() == CastleWarsObjects.SARA_FLAG2 || o.getId() == CastleWarsObjects.ZAMMY_FLAG2) {
				// player.sendMessage("You must recover your flag first.");
				// return;
				// }
				if (saraFlag == 0 || (o.getId() != CastleWarsObjects.ZAMMY_FLAG2 && o.getId() != CastleWarsObjects.ZAMMY_FLAG1)) {
					return;
				}
				setSaraFlag(0);
				scores[1]++;
				player.sendMessage("The team of Zamorak scores 1 point!");
			}

		}
		updateHintIcon();
		player.getItems().deleteEquipmentBySlot(PlayerConstants.playerWeapon);
	}

	public static void returnFlagOnly(String buggerName, int flagItemId) {
		System.out.println(
				"returned bugged flag???? BUG ABUSER:" + buggerName);
		if (flagItemId == SARA_BANNER) { // sara flag return
			if (saraFlag == 0) {
				return;
			}
			setSaraFlag(0);
		} else if (flagItemId == ZAMMY_BANNER) { // return zammy flag
			if (zammyFlag == 0) {
				return;
			}
			setZammyFlag(0);
		}
	}

	public static int rockLocation(int x, int y) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (CAVE_WALL[i][j] + 1 == x) {
					for (int k = 0; k < 4; k++) {
						if (CAVE_WALL[i][k] + 1 == y) {
							return i;
						}
					}
				}
			}
		}
		return -1;
	}

	/**
	 * Method to find out the location of the rocks which are 2 by 2 size.
	 *
	 * @param x
	 *            X coord
	 * @param y
	 *            Y coord
	 * @return returns -1 if not under these rocks, else returns 0,1,2,3
	 *         depending on which side
	 */
	public static int rockLocation2(int x, int y) {
		for (int i = 0; i < CAVE_WALL.length; i++) {
			if ((x > CAVE_WALL[i][0] && x < CAVE_WALL[i][1])
					&& (y > CAVE_WALL[i][2] && y < CAVE_WALL[i][3])) {
				return i;
			}
		}
		return -1;
	}

	public static void sendPlayerIngame(final Player c) {
		if (RSConstants.cwSaraWaitingRoom(c.getX(), c.getY())) {
			rejoinGameToSara(c);
		} else if (RSConstants.cwZammyWaitingRoom(c.getX(), c.getY())) {
			rejoinGameToZammy(c);
		}
	}

	/**
	 * Method we use the update the player's interface in the game room
	 */
	// public static void updateInGamePlayers() {
	// if (getSaraPlayers() > 0 && getZammyPlayers() > 0) {
	// Iterator<Player> iterator = gameRoom.keySet().iterator();
	// while (iterator.hasNext()) {
	// Player player = (Player) iterator.next();
	// //int config;
	// if (player == null) {
	// continue;
	// }
	// /*if(saraFlag == 1) {
	// //createHintIcon(1, player);
	// }
	// if(zammyFlag == 1) {
	// //createHintIcon(2, player);
	// }*/
	//// if(player.getItems().playerHasEquipped(SARA_BANNER) ||
	// player.getItems().playerHasEquipped(ZAMMY_BANNER)) {
	//// createHintIcons(player);
	//// }
	// //createHintIcons(player);
	// updateCastleWarsScreen(player);
	// //player.getPA().walkableInterface(11146);
	// /*player.getPacketSender().sendFrame126("Zamorak = " + scores[1], 11147);
	// player.getPacketSender().sendFrame126(scores[0] + " = Saradomin", 11148);
	// player.getPacketSender().sendFrame126(gameTimer * 3 + " secs", 11155);
	// config = (2097152 * saraFlag);
	// player.getPA().sendFrame87(378, config);
	// config = (2097152 * zammyFlag); //flags 0 = safe 1 = taken 2 = dropped
	// player.getPA().sendFrame87(377, config);
	//
	// /*int team = gameRoom.get(player);
	// if(team == 1) {
	// player.getPacketSender().sendFrame126((!unlockedSaraDoor) ? sLocked :
	// sUnlocked,
	// 11158);
	// player.getPacketSender().sendFrame126((eastCave) ? sCollapsed : sCleared,
	// 11160);
	// player.getPacketSender().sendFrame126((southCave) ? sCollapsed :
	// sCleared, 11162);
	// //player.getPacketSender().sendFrame126("f", 11164); //catapult
	// } else {
	// player.getPacketSender().sendFrame126((!unlockedZammyDoor) ? sLocked :
	// sUnlocked,
	// 11158);
	// player.getPacketSender().sendFrame126((northCave) ? sCollapsed :
	// sCleared, 11160);
	// player.getPacketSender().sendFrame126((westCave) ? sCollapsed : sCleared,
	// 11162);
	// //player.getPacketSender().sendFrame126("f", 11164); // catapult
	// }*/
	// }
	// }
	// }

	public static void sendWaitingRoomDialogueToJoinGame() {

		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[0][0], WAIT_ROOM[0][1], 0)
				.getPlayers()) { // sara room
			if (getSaraPlayers() >= lockedSaraCount) {
				break;
			}
			if (player == null || player.disconnected
					|| player.forceDisconnect) {
				continue;
			}
			if (!RSConstants.cwSaraWaitingRoom(player.getX(), player.getY())) {
				continue;
			}
			if (player.wantsToRejoin && !player.getDialogueBuilder().isActive()) {
				createDialogueToRejoin(player);
			}
		}

		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[1][0], WAIT_ROOM[1][1], 0)
				.getPlayers()) { // zammy room
			if (getZammyPlayers() >= lockedZammyCount) {
				break;
			}
			if (player == null || player.disconnected
					|| player.forceDisconnect) {
				continue;
			}
			if (!RSConstants.cwZammyWaitingRoom(player.getX(), player.getY())) {
				continue;
			}
			if (player.wantsToRejoin && !player.getDialogueBuilder().isActive()) {
				createDialogueToRejoin(player);
			}
		}

	}
	
	private static void createDialogueToRejoin(Player p) {
		p.getDialogueBuilder().sendOption("Join ongoing game?", "I would like to join the ongoing game.", ()-> {
			CastleWars.sendPlayerIngame(p);
		}, "Stay here and wait for next Castle Wars game.", ()-> {
			p.wantsToRejoin = false;
		}).execute();
	}

	public static void sendWaitingScreen(Player c) {
		int waitSeconds = Misc.ticksToSeconds(waitTimer);
		int gameSeconds = Misc.ticksToSeconds(gameTimer);

		c.getPacketSender().sendFrame126(waitSeconds > 0 ? "Next Game Begins In: " + (waitSeconds < 60 ? waitSeconds + " secs" : waitSeconds / 60 + " min")
						: "<col=ff00>Game in Progress - Time Left: " + (gameSeconds > 60 ? gameSeconds / 60 + " mins": gameSeconds + " sec"), 6570);
		c.getPacketSender().sendFrame126("Zam Players: <col=ff0000>" + getZammyPlayersWaiting() + "</col>.", 6572);
		c.getPacketSender().sendFrame126("Sara Players: <col=ff0000>" + getSaraPlayersWaiting() + "</col>.", 6664);
	}

	public static void setCaveStatus(int cave, boolean collapsed) {
		switch (cave) {
			case 0:
				northCave = collapsed;
				break;
			case 1:
				eastCave = collapsed;
				break;
			case 2:
				southCave = collapsed;
				break;
			case 3:
				westCave = collapsed;
				break;
		}
	}

	/**
	 * Method to make sara flag change status 0 = safe, 1 = taken, 2 = dropped
	 *
	 * @param status
	 */
	public static void setSaraFlag(int status) {
		saraFlag = status;
		if (status == 0)
			returnFlagToSaraStand();
	}

	public static void setWaitTimer(int waitTimer) {
		CastleWars.waitTimer = waitTimer;
	}

	/**
	 * Method to make zammy flag change status 0 = safe, 1 = taken, 2 = dropped
	 *
	 * @param status
	 */
	public static void setZammyFlag(int status) {
		zammyFlag = status;
		if (status == 0)
			returnFlagToZammyStand();
	}

	public static void spawnObjectAfterBarricadeDeath(final int obId,
			final int obX, final int obY, final int obZ, final int objectFace,
			final int objectType, final int objectClass) {
		Server.getTaskScheduler().schedule(new Task() {

			@Override
			protected void execute() {
				final RSObject rso = new RSObject(obId, obX, obY, obZ, objectFace, objectType, -1,
						GAME_TIMER);
				rso.setObjectClass(objectClass);
				ObjectManager.addObject(rso);
				this.stop();
			}
		});
	}

	/*
	 * Method that will start the game when there's enough players.
	 */
	public static void startGame() {
		loyalToTeam.clear();
		if ((getSaraPlayersWaiting() < MINIMUM_PLAYERS
				|| getZammyPlayersWaiting() < MINIMUM_PLAYERS)
				&& !Config.SERVER_DEBUG) {
			waitTimer = GAME_START_TIMER;

			for (Player player : Server.getRegionManager()
					.getRegionByLocation(WAIT_ROOM[0][0], WAIT_ROOM[0][1], 0)
					.getPlayers()) { // sara room
				if (player == null || player.isNotReadyForMinigame()) {
					continue;
				}
				player.sendMessage("Minimum " + MINIMUM_PLAYERS
						+ " players required in each team to start the game.");
			}

			for (Player player : Server.getRegionManager()
					.getRegionByLocation(WAIT_ROOM[1][0], WAIT_ROOM[1][1], 0)
					.getPlayers()) { // zammy room
				if (player == null || player.isNotReadyForMinigame()) {
					continue;
				}
				player.sendMessage("Minimum " + MINIMUM_PLAYERS
						+ " players required in each team to start the game.");
			}

			return;
		}
		// if (!Config.SERVER_DEBUG) {
		// waitTimer = GAME_START_TIMER;
		// return;
		// }
		
		discordStartTime = (int) (System.currentTimeMillis()/1000);
		discordEndTime = discordStartTime + Misc.ticksToSeconds(GAME_TIMER);
		
		resetGame();
		waitTimer = -1;
		spawnTimer = 0;
		// System.out.println("Starting Castle Wars game.");
		gameTimer = GAME_TIMER;
		final int secondsToTicksForSpawnExit = Misc
				.secondsToTicks(SECONDS_TO_EXIT_SPAWN);
		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[0][0], WAIT_ROOM[0][1], 0)
				.getPlayers()) { // sara room
			if (!player.isActive || player.disconnected) {
				continue;
			}
			player.inCwGame = true;
			moveToSaraSpawn(player);// c.getPA().movePlayer(GAME_ROOM[0][0] +
								// Misc.random(3), GAME_ROOM[0][1] -
								// Misc.random(3), 1);
			player.getPA().walkableInterface(11146);
			player.getPA().setCastleWarsWaitingTimer(secondsToTicksForSpawnExit);
			gameRoom.put(player, 1);
			player.castleWarsTeam = 1;
			player.soulWarsTeam = Team.BLUE;
			loyalToTeam.put(player.UUID, 1);
		}

		for (Player player : Server.getRegionManager()
				.getRegionByLocation(WAIT_ROOM[1][0], WAIT_ROOM[1][1], 0)
				.getPlayers()) { // zammy room
			if (!player.isActive || player.disconnected) {
				continue;
			}
			player.inCwGame = true;
			moveToZammySpawn(player);// c.getPA().movePlayer(GAME_ROOM[1][0] +
								// Misc.random(3), GAME_ROOM[1][1] -
								// Misc.random(3), 1);
			player.getPA().walkableInterface(11146);
			player.getPA().setCastleWarsWaitingTimer(secondsToTicksForSpawnExit);
			gameRoom.put(player, 2);
			player.castleWarsTeam = 2;
			player.soulWarsTeam = Team.RED;
			loyalToTeam.put(player.UUID, 2);
		}
		gameStarted = true;
		canRejoin = true;
		
		// Iterator<Player> iterator = waitingRoom.keySet().iterator();
		// while (iterator.hasNext()) {
		// Player player = (Player) iterator.next();
		// if (player == null) {
		// continue;
		// }
		// int team = waitingRoom.get(player);
		// if (team < 1 || team > 2) continue;
		// player.inCwGame = true;
		// //player.getPA().walkableInterface(-1);
		// if ((team - 1) < 0 || (team -1) > 1) {
		// System.out.println("bug abuser: "+player.playerName);
		// continue;
		// }
		// player.getPA().movePlayer(GAME_ROOM[team - 1][0] + Misc.random(3),
		// GAME_ROOM[team - 1][1] - Misc.random(3), 1);
		//// player.getPA().movePlayer(GAME_ROOM[team - 1][0] + Misc.random(3),
		// GAME_ROOM[team - 1][1] - Misc.random(3), 1);
		// player.getPA().walkableInterface(11146);
		// gameRoom.put(player, team);
		// //player = null;
		// }
		waitingRoom.clear();
		// iterator = null;
	}

	/**
	 * Method we use to transfer to player from the outside to the waitingroom
	 * (:
	 *
	 * @param player
	 *            the player that wants to join
	 * @param team
	 *            team he wants to be in - team = 1 (saradomin), team = 2
	 *            (zamorak), team = 3 (random)
	 */
	public static void toWaitingRoom(Player player, int team) {
		int saraBonuses = getSaraPlayersWaitingBonuses();
		int zammyBonuses = getZammyPlayersWaitingBonuses();
		
		int loyalty = loyalToTeam.getOrDefault(player.UUID, 0);
		
		if (player.combatLevel > 74) {
			if (team == 1) {
				if (/*getSaraPlayersWaiting() > 0
						&& getSaraPlayersWaiting() > getZammyPlayersWaiting()
						&& */saraBonuses - zammyBonuses > 2000) {
					player.sendMessage(
							"The saradomin team is full, try again later!");
					return;
				}
				player.getPA().movePlayer(
						WAIT_ROOM[team - 1][0] + Misc.random(5),
						WAIT_ROOM[team - 1][1] + Misc.random(5), 0);
				player.sendMessage(
						"You have been added to the <col=13434880>Saradomin</col> team.");
				player.sendMessage("Next Game Begins In:<col=13434880> "
						+ ((waitTimer + gameTimer) * 600 / 1000)
						+ " </col>seconds.");
				addCapes(player, team);// SARA_CAPE
				waitingRoom.put(player, team);
				if (GREEN_PORTAL_ONLY && loyalty == 0) {
					loyalToTeam.put(player.UUID, 1);
				}
			} else if (team == 2) {
				if (/*getZammyPlayersWaiting() > 0
						&& getZammyPlayersWaiting() > getSaraPlayersWaiting()
						&& */zammyBonuses - saraBonuses > 2000) {
					player.sendMessage(
							"The zamorak team is full, try again later!");
					return;
				}
				player.getPA().movePlayer(
						WAIT_ROOM[team - 1][0] + Misc.random(5),
						WAIT_ROOM[team - 1][1] + Misc.random(4), 0);
				player.sendMessage(
						"[<col=13434880>RANDOM TEAM</col>] You have been added to the <col=13434880>Zamorak</col> team.");
				player.sendMessage("Next Game Begins In:<col=13434880> "
						+ ((waitTimer + gameTimer) * 600 / 1000)
						+ " </col>seconds.");
				addCapes(player, team);
				waitingRoom.put(player, team);
				if (GREEN_PORTAL_ONLY && loyalty == 0) {
					loyalToTeam.put(player.UUID, 2);
				}
			} else if (team == 3) {
				if (loyalty != 0) {
					toWaitingRoom(player, loyalty);
					return;
				}
				if (/*getZammyPlayersWaiting() == getSaraPlayersWaiting() && */zammyBonuses == saraBonuses)
					toWaitingRoom(player, 1 + Misc.random(1));
				else
					toWaitingRoom(player, (/*getZammyPlayersWaiting() > getSaraPlayersWaiting() ||*/ zammyBonuses - saraBonuses > 2000) ? 1 : 2);
				return;
			}
		} else {
			player.sendMessage(
					"You need a combat level of 75 + to enter Castle Wars.");
			return;
		}
	}

	public static void updateCastleWarsScreen(Player c) {
		if (c.isBot()) {
			return;
		}
		// if (c.inSaraWait() || c.inZammyWait()) {
		/*
		 * if (isInCwWait(c)) { // if (c.gameInterface != 6673) { //
		 * c.gameInterface = 6673; // }
		 * c.getPacketSender().sendFrame126(waitTimer > 0 ?
		 * "    Next Game Begins In: " + (waitTimer < 60 ? "" + waitTimer +
		 * " secs" : "" + waitTimer / 60 + " min") :
		 * "<col=ff00>Game in Progress - Time Left: " + (gameTimer > 60 ? gameTimer /
		 * 60 + " mins" : gameTimer + " sec"), 6570);
		 * c.getPacketSender().sendFrame126("Zamorak Players: <col=ff0000>" +
		 * getZammyPlayers() + "<col=ffffff>.", 6572);
		 * c.getPacketSender().sendFrame126("Saradomin Players: <col=ff0000>" +
		 * getSaraPlayers() + "<col=ffffff>.", 6664); //} else if (c.inCwGame()) { }
		 * else if (isInCw(c)) {
		 */
		// if (c.gameInterface != 11344) {
		// c.gameInterface = 11344;
		// }
		// if (c.isMod() || c.isOwner() || c.isAdmin())
		// return;

		if (c.soulWarsTeam == Team.NONE  && (!c.isDev() && !c.isMod() && !c.isAdmin())) {
			removePlayerFromCw(c);
			// c.sendMessage("removed from cwars.");
			return;
		}
		if (gameTimer > GAME_TIMER - 2) {
			return;
		}

		if (RSConstants.cwSaraSpawnRoom(c.getX(), c.getY(), c.getHeightLevel())
				|| RSConstants.cwZammySpawnRoom(c.getX(), c.getY(),
						c.getHeightLevel())) {
			c.setWaitingRoomCampTimer(c.getWaitingRoomCampTimer() + 1);
			c.getPacketSender().sendString(
					"Leave waiting room: " + (int) (Misc.ticksIntoSeconds(
							SPAWN_ROOM_KICK - c.getWaitingRoomCampTimer())),
					12837);
		} else {
			c.setWaitingRoomCampTimer(0);
			c.getPacketSender().sendString("", 12837);
		}
		if (c.getWaitingRoomCampTimer() > SPAWN_ROOM_KICK) { // 2 minutes
			removePlayerFromCw(c);
			return;
		}

		int team = c.castleWarsTeam; // gameRoom.get(c);
		c.getPacketSender().sendFrame126(
				(gameTimer * 600 / 1000) > 60
						? (gameTimer * 600 / 1000) / 60 + " Mins"
						: (gameTimer * 600 / 1000) + " Sec",
				11155/* 11353 */);
		// c.getPacketSender().sendFrame126("test", 11353);
		int config = zammyMainGateHP;
		if (team == 1 ? saraSideDoor : zammySideDoor) {
			config += 128;
		}
		if (!northCave) {
			config += 256;
		}
		if (!westCave) {
			config += 512;
		}
		if (team == 2 ? !zammyCatapult : !saraCatapult) {
			config += 1024;
		}
		if (team == 2) {
			config += 2097152 * zammyFlag;
		} else if (team == 1) {
			config += 2097152 * saraFlag;
		}
		/*
		 * if (team == 2 ? ZflagDropped : SflagDropped) { config += 2097152 * 2;
		 * } else if (team == 2 ? ZammyFlagTaken : SaraFlagTaken) { config +=
		 * 2097152 * 1; }
		 */
		config += 16777216 * (team == 2 ? scores[1] : scores[0]);
		c.getPacketSender().sendFrame87(team == 2 ? 377 : 378, config);

		config = saraMainGateHP;
		if (team == 2 ? zammySideDoor : saraSideDoor) {
			config += 128;
		}
		if (!eastCave) {
			config += 256;
		}
		if (!southCave) {
			config += 512;
		}
		if (!saraCatapult) {
			config += 1024;
		}
		if (team == 2) {
			config += 2097152 * saraFlag;
		} else if (team == 1) {
			config += 2097152 * zammyFlag;
		}
		/*
		 * if (c.zammyTeam() ? SflagDropped : ZflagDropped) { config += 2097152
		 * * 2; } else if (c.zammyTeam() ? SaraFlagTaken : ZammyFlagTaken) {
		 * config += 2097152 * 1; }
		 */
		config += 16777216 * (team == 2 ? scores[0] : scores[1]);
		c.getPacketSender().sendFrame87(team == 2 ? 378 : 377, config);

		if (team == 1
				&& c.getItems().playerHasItemInSlot(CastleWars.SARA_BANNER,
						PlayerConstants.playerWeapon)
				&& (c.absX >= CASTLE_ZONES[team - 1][0]
						&& c.absX <= CASTLE_ZONES[team - 1][1])
				&& (c.absY >= CASTLE_ZONES[team - 1][2]
						&& c.absY <= CASTLE_ZONES[team - 1][3])) { // if team
																	// picks up
																	// their own
																	// flag
																	// inside
																	// their
																	// castle
																	// then
																	// return it
			if (saraFlag == 0) {
				return;
			}
			setSaraFlag(0);
		} else if (team == 2
				&& c.getItems().playerHasItemInSlot(CastleWars.ZAMMY_BANNER,
						PlayerConstants.playerWeapon)
				&& ((c.absX >= CASTLE_ZONES[team - 1][0]
						&& c.absX <= CASTLE_ZONES[team - 1][1])
						&& (c.absY >= CASTLE_ZONES[team - 1][2]
								&& c.absY <= CASTLE_ZONES[team - 1][3]))) {
			if (zammyFlag == 0) {
				return;
			}
			setZammyFlag(0);
		}

		// }
	}

	public static void updateHintIcon() {
		Iterator<Player> iterator = gameRoom.keySet().iterator();
		while (iterator.hasNext()) {
			Player c = iterator.next();
			if (!c.isActive) {
				iterator.remove();
				continue;
			}
			if (saraFlag == 1) { // sara taken
				if (getTeamNumber(c) == 2) { // zammy team
					c.getPacketSender().createPlayerHints(10, saraFlagHolder);
				}
			}
			if (zammyFlag == 1) { // zammy flag taken
				if (getTeamNumber(c) == 1) {
					c.getPacketSender().createPlayerHints(10, zammyFlagHolder);
				}
			}
		}
	}

	public static void useBandages(Player c) {
		if (!c.inCw()) {
			return;
		}
		if (c.getItems().playerHasItem(4049)) {
			if (System.currentTimeMillis() - c.anyDelay2 >= 500
					&& c.getPlayerLevel()[3] > 0) {
				// int totalHP = c.getLevelForXP(c.playerXP[3]);
				int healAmount = (int) Math
						.round(c.getSkills().getMaximumLifepoints()
								* 20.0 / 100);
				c.getCombat().resetPlayerAttack();
				// c.attackTimer += 2;
				c.startAnimation(829);
				c.getItems().deleteItemInOneSlot(4049, 1);
				c.healHitPoints(healAmount);
				c.anyDelay2 = System.currentTimeMillis();
				c.sendMessage("You heal using a bandage.");
				c.getMovement().restoreRunEnergy(30);
				c.getDotManager().clearPoison();
				c.getDotManager().clearVenom();
			}
		}
	}

}
