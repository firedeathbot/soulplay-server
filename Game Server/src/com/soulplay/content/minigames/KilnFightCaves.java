package com.soulplay.content.minigames;

import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class KilnFightCaves {

	// IN EACH BRACKET IS THE AMOUNT OF DIFFERENT NPCS TO SPAWN IN A WAVE
	private final static int[][] WAVES = {
			// Wave 0
			{2743, 2743},

			// Wave 1
			{2743, 2743, 2741, 2631},

			// Wave 2
			{2745},

			// Wave 3
			{2745, 2741},

			// Wave 4
			{2745, 2741, 2631},

			// Wave 5
			{2745, 2741, 2741, 2631},

			// Wave 6
			{2745, 2743},

			// Wave 7
			{2745, 2743, 2630},

			// Wave 8
			{2743, 2743, 2743, 2741, 2741, 2631},

			// Wave 9
			{2745, 2745}

	};

	// NOTE: SIZE MUST BE THE AMOUNT OF NPCS SPAWNING IN A WAVE
	private static final int[][] coordinates = {{2398, 5086}, {2387, 5095},
			{2407, 5098}, {2417, 5082}, {2390, 5076}, {2410, 5090}};
	
	private static final int TICKS = 17;

	public static void enterCaves(final Client c) {
		c.getPA().movePlayer(2413, 5117, c.getId() * 4);
		c.waveId = 0;
		c.tzhaarToKill = -1;
		c.tzhaarKilled = -1;
		c.inKilnFightCave = true;
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			boolean delayedSpawn = false;

			@Override
			public void execute(CycleEventContainer e) {
				if (c.isDead() || c.disconnected
						|| !c.inFightCaves() || c.waveId == -1) {
					e.stop();
					return;
				}
				if (!delayedSpawn) {
					if (c.tzhaarToKill < 1) {
						if (c.waveId >= 10) {
							e.stop();
							rewardPlayer(c);
							return;
						}
						c.sendMessage(String.format("Wave %d will spawn in %d seconds.", c.waveId + 1, Misc.ticksToSeconds(TICKS)));
						delayedSpawn = true;
						CycleEventHandler.getSingleton().addEvent(c,
								new CycleEvent() {
									int timer = TICKS;
									@Override
									public void execute(CycleEventContainer e) {
										if (c.isDead() || c.disconnected
												|| !c.inFightCaves()
												|| c.waveId == -1) {
											e.stop();
											return;
										}

										if (timer == 0) {
											e.stop();
											spawnNextWave(c);
											c.waveId++;
											delayedSpawn = false;
											return;
										}
										if (timer > 2)
											c.forceChat(Integer.toString(Misc.ticksToSeconds(timer)));
										timer--;
									}

									@Override
									public void stop() {
									}
								}, 1);
					}
				}
			}

			@Override
			public void stop() {
				if (c != null) {
					c.inKilnFightCave = false;
				}
			}
		}, 1);
	}

	public static void rewardPlayer(Client c) {
		c.getPA().movePlayer(2438, 5168, 0);
		c.waveId = -1;
		c.getAchievement().obtainTokhaar();
		c.getPA().safeDeathSecureTimer();
		Titles.defeatKiln(c);
		if (c.getItems().freeSlots() > 0) {
			c.getItems().addItem(23639, 1);
			if (Misc.random(3) == 1) {
				c.getItems().addOrBankItem(6571, 1);
				c.sendMessage("You received a uncut onyx.");
			}
			if (c.getClan() != null) {
				c.getClan().addClanPoints(c, 150, "kiln_caves");
			}
			c.sendMessage("Congratulations on defeating Kiln Caves!");
		} else if (c.getItems().freeSlots() < 1) {
			c.getItems().addItemToBank(23639, 1);
			c.sendMessage("Your Kiln cape has been added to your bank.");
			c.sendMessage("Congratulations on defeating Kiln Caves!");
		}
	}

	public static void spawnNextWave(Client c) {
		if (c != null) {
			if (c.waveId >= WAVES.length) {
				c.waveId = 0;
				return;
			}
			if (c.waveId < 0) {
				return;
			}
			int npcAmount = WAVES[c.waveId].length;
			for (int j = 0; j < npcAmount; j++) {
				int npc = WAVES[c.waveId][j];
				int X = coordinates[j][0];
				int Y = coordinates[j][1];
				int H = c.heightLevel;
				NPC n = NPCHandler.spawnNpc(c, npc, X, Y, H, 0, true, false);
				n.setFightCavesNPC(true);
				n.spawnedBy = c.getId();
			}
			c.tzhaarToKill = npcAmount;
			c.tzhaarKilled = 0;
			
			c.sendMessage("<col=ff0000>Wave: " + (c.waveId + 1));
		}
	}

}
