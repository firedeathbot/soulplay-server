package com.soulplay.content.minigames.duel;

public class DuelRules {

	public static final int SAME_WEAPONS = 0;

	public static final int NO_MOVEMENT = 1;

	public static final int NO_RANGE = 2;

	public static final int NO_MELEE = 3;

	public static final int NO_MAGIC = 4;

	public static final int NO_DRINKS = 5;

	public static final int NO_FOOD = 6;

	public static final int NO_PRAYER = 7;

	public static final int OBSTACLES = 8;

	public static final int FUN_WEAPONS = 9;

	public static final int NO_SPECIAL_ATK = 10;

	public static final int NO_HAT = 11;

	public static final int NO_CAPE = 12;

	public static final int NO_AMULET = 13;

	public static final int NO_WEAPON = 14;

	public static final int NO_BODY = 15;

	public static final int NO_SHIELD = 16;

	public static final int NO_LEGS = 17;

	public static final int NO_GLOVES = 18;

	public static final int NO_BOOTS = 19;

	public static final int NO_RINGS = 20;

	public static final int NO_ARROWS = 21;

}
