package com.soulplay.content.minigames.duel;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;

public class Stake {

	private String loser;

	private int loserMID;

	private String winner;

	private int winnerMID;

	private GameItem item;

	private String itemOwner;

	Stake(Client loser, Client winner, GameItem item, String itemOwner) {
		this.loser = loser.getName();
		this.winner = winner.getName();
		this.item = item;
		this.itemOwner = itemOwner;
		this.setLoserMID(loser.mySQLIndex);
		this.setWinnerMID(winner.mySQLIndex);
	}

	public GameItem getItem() {
		return item;
	}

	public String getItemOwner() {
		return itemOwner;
	}

	public String getLoser() {
		return loser;
	}

	public int getLoserMID() {
		return loserMID;
	}

	public String getWinner() {
		return winner;
	}

	public int getWinnerMID() {
		return winnerMID;
	}

	public void setLoserMID(int loserMID) {
		this.loserMID = loserMID;
	}

	public void setWinnerMID(int winnerMID) {
		this.winnerMID = winnerMID;
	}
}
