package com.soulplay.content.minigames.duel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.player.trading.Trade;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

import plugin.location.wilderness.LarransChestPlugin;

public class TradeAndDuel {

	public static void giveItems(Client o1, Client o2) {
		if (o1 == null) {
			return;
		}
		if (o2 == null) {
			return;
		}

		List<Trade> tradeList = new ArrayList<>();
		try {
			for (GameItem item : o1.getTradeAndDuel().offeredItems) {
				if (item.getId() > 0) {
					if (!o2.getPA().checkForMax(item)) {
						o2.getItems().addItem(item.getId(), item.getAmount());
					}

					// c.getTradeLog().tradeReceived(ItemAssistant.getItemName(item.id),
					// item.amount);
					// o.getTradeLog().tradeGive(ItemAssistant.getItemName(item.id),
					// item.amount);

					tradeList.add(new Trade(o1.getName(), o1.mySQLIndex,
							o2.getName(), o2.mySQLIndex,
							new GameItem(item.getId(), item.getAmount())));
					// try {
					// if(!Debug.SERVER_DEBUG) {
					// TradeLog.tradeReceived(o2, o1,
					// ItemConstants.getItemName(item.id), item.amount);
					// TradeLog.tradeGive(o1, o2,
					// ItemConstants.getItemName(item.id), item.amount);
					// }
					// } catch (Exception ex) {
					// ex.printStackTrace();
					// }
				}
			}

			for (GameItem item : o2.getTradeAndDuel().offeredItems) {
				if (item.getId() > 0) {
					if (!o1.getPA().checkForMax(item)) {
						o1.getItems().addItem(item.getId(), item.getAmount());
					}

					// c.getTradeLog().tradeReceived(ItemAssistant.getItemName(item.id),
					// item.amount);
					// o.getTradeLog().tradeGive(ItemAssistant.getItemName(item.id),
					// item.amount);

					tradeList.add(new Trade(o2.getName(), o2.mySQLIndex,
							o1.getName(), o1.mySQLIndex,
							new GameItem(item.getId(), item.getAmount())));
					// try {
					// if(!Debug.SERVER_DEBUG) {
					// TradeLog.tradeReceived(o1, o2,
					// ItemConstants.getItemName(item.id), item.amount);
					// TradeLog.tradeGive(o2, o1,
					// ItemConstants.getItemName(item.id), item.amount);
					// }
					// } catch (Exception ex) {
					// ex.printStackTrace();
					// }
				}
			}

			// if ((o2.getTradeAndDuel().offeredItems.size() == 1 &&
			// o2.getTradeAndDuel().offeredItems.get(0).getId() == 995) ||
			// (o1.getTradeAndDuel().offeredItems.size() == 1 &&
			// o1.getTradeAndDuel().offeredItems.get(0).getId() == 995)) {
			// if (o1.getTradeAndDuel().offeredItems.size() != 0 &&
			// o2.getTradeAndDuel().offeredItems.size() != 0)
			// PriceChecker.addToPriceCheck(o1.getName(),
			// o1.getTradeAndDuel().offeredItems.clone(), o2.getName(),
			// o2.getTradeAndDuel().offeredItems.clone());
			// }

			o2.getTradeAndDuel().resetTrade();
			o1.getTradeAndDuel().resetTrade();

			o2.sendMessage("Trade Accepted.");

			o1.sendMessage("Trade Accepted.");
			
			o1.getGambleLogger().hookTrade(o2);
			o2.getGambleLogger().hookTrade(o1);

			o1.getPA().closeAllWindows();
			o2.getPA().closeAllWindows();

			// PlayerSave.saveGame(o1); //TODO: remove after fixing server
			// crashes
			// PlayerSave.saveGame(o2); //TODO: remove after fixing server
			// crashes

			// SqlHandler.logTrade(tradeList);
			// SqlHandler.logTrade2(tradeList);
			if (DBConfig.LOG_TRADE && !tradeList.isEmpty()) {
				Server.getLogWriter().addToTradeListList(tradeList);
			}
			tradeList = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean twoTraders(Client c, Client o) {
		int count = 0;
		for (Player player : PlayerHandler.players) {
			Client temp = (Client) player;
			if (temp != null) {
				if (temp.tradeWith == c.getId()
						|| temp.tradeWith == o.getId()) {
					count++;
				}
			}
		}
		return count == 2;
	}

	private Client c;
	
	public boolean hasItemsInTradeOrDuel() {
		return !offeredItems.isEmpty() || !stakedItems.isEmpty();
	}

	/**
	 * Trading ~ SS
	 **/
	public CopyOnWriteArrayList<GameItem> offeredItems = new CopyOnWriteArrayList<>();

	/**
	 * Dueling
	 **/

	public ArrayList<GameItem> otherStakedItems = new ArrayList<>();

	public CopyOnWriteArrayList<GameItem> stakedItems = new CopyOnWriteArrayList<>();

	public ArrayList<GameItem> stakeWinsDisplay = new ArrayList<>();

	public ArrayList<GameItem> wonStakeItems = new ArrayList<>();

	public TradeAndDuel(Client Client) {
		this.c = Client;
	}

	public void bothDeclineDuel() {
		Client c3 = c.getDuelingClient();// (Client)
											// PlayerHandler.players[c.duelingWith];
		declineDuel();
		c3.getTradeAndDuel().declineDuel();
		c.sendMessage("<col=ff0000>The duel has been declined.");
		c3.sendMessage("<col=ff0000>The duel has been declined.");
	}

	public void changeDuelStuff() {
		Client o = c.getDuelingClient();// (Client)
										// PlayerHandler.players[c.duelingWith];
		if (o == null) {
			return;
		}
		if (o.duelStatus == 2) {
			o.duelStatus = 1;
		}
		if (c.duelStatus == 2) {
			c.duelStatus = 1;
		}
		o.getPacketSender().sendFrame126("", 6684);
		c.getPacketSender().sendFrame126("", 6684);

		c.setDuelRuleTimer(8);
		c.getPacketSender().sendFrame126("<col=ff0000>Accept", 6680);
		o.setDuelRuleTimer(8);
		o.getPacketSender().sendFrame126("<col=ff0000>Accept", 6680);
	}

	public void claimStakedItems() {
		if (c.duelStatus != 6) {
			resetDuel();
			// c.sendMessage("Reset duel cuz status not 6");
			return;
		}
		// c.sendMessage("Claiming staked items.");
		List<Stake> stakeList = new ArrayList<>();
		for (GameItem item : otherStakedItems) {
			if (item.getId() > 0 && item.getAmount() > 0) {
				if (item.getId() == 995) {
					if (c.getItems().playerHasItem(995)) {
						if ((long) (c.getItems().getItemAmount(995) + item.getAmount()) > Integer.MAX_VALUE) {
							int addableAmount = Integer.MAX_VALUE - c.getItems().getItemAmount(995);
							int remainingAmount = item.getAmount() - addableAmount;
							c.getItems().addItem(item.getId(), addableAmount);
							MoneyPouch.addToMoneyPouch(c, remainingAmount);
							c.sendMessage("<col=ff0000>Cash pile full, the remaining " + remainingAmount + " gold was added to your pouch.");
						} else {
							c.getItems().addItem(item.getId(), item.getAmount());
						}
					}
				} else {
					if (ItemProjectInsanity.itemStackable[item.getId()]) {
						if (!c.getPA().checkForMax(item)) {
							if (!c.getItems().addItem(item.getId(),
									item.getAmount())) {
								if (c.teleportToX > 0) {
									ItemHandler.createGroundItem(c, item.getId(),
											c.teleportToX, c.teleportToY,
											c.teleportToZ, item.getAmount());
								} else {
									ItemHandler.createGroundItem(c, item.getId(),
											c.getX(), c.getY(), c.getHeightLevel(),
											item.getAmount());
								}
							}
						}
					} else {
						int amount = item.getAmount();
						for (int a = 1; a <= amount; a++) {
							if (!c.getItems().addItem(item.getId(), 1)) {
								if (c.teleportToX > 0) {
									ItemHandler.createGroundItem(c, item.getId(),
											c.teleportToX, c.teleportToY,
											c.teleportToZ, 1);
								} else {
									ItemHandler.createGroundItem(c, item.getId(),
											c.getX(), c.getY(), c.getHeightLevel(),
											1);
								}
							}
						}
					}
				}
				// System.out.println("trying to do this1");
				stakeList.add(new Stake(c.getDuelingClient(), c, item,
						c.getDuelingClient().getName()));
				// try {
				// if(!Debug.SERVER_DEBUG) {
				// TradeLog.duelReceived(c, c.getDuelingClient(),
				// ItemConstants.getItemName(item.getId()), item.getAmount());
				// TradeLog.duelGive(c.getDuelingClient(), c,
				// ItemConstants.getItemName(item.getId()), item.getAmount());
				// }
				// } catch (Exception ex) {
				// ex.printStackTrace();
				// }

			}
		}
		for (GameItem item : stakedItems) {
			if (item.getId() > 0 && item.getAmount() > 0) {
				if (item.getId() == 995) {
					if (c.getItems().playerHasItem(995)) {
						if ((long) (c.getItems().getItemAmount(995) + item.getAmount()) > Integer.MAX_VALUE) {
							int addableAmount = Integer.MAX_VALUE - c.getItems().getItemAmount(995);
							int remainingAmount = item.getAmount() - addableAmount;
							c.getItems().addItem(item.getId(), addableAmount);
							MoneyPouch.addToMoneyPouch(c, remainingAmount);
							c.sendMessage("<col=ff0000>Cash pile full, the remaining " + remainingAmount + " gold was added to your pouch.");
						} else {
							c.getItems().addItem(item.getId(), item.getAmount());
						}
					}
				} else {
					if (ItemProjectInsanity.itemStackable[item.getId()]) {
						if (!c.getPA().checkForMax(item)) {
							if (!c.getItems().addItem(item.getId(),
									item.getAmount())) {
								if (c.teleportToX > 0) {
									ItemHandler.createGroundItem(c, item.getId(),
											c.teleportToX, c.teleportToY,
											c.getHeightLevel(), item.getAmount());
								} else {
									ItemHandler.createGroundItem(c, item.getId(),
											c.getX(), c.getY(), c.getHeightLevel(),
											item.getAmount());
								}
							}
						}
					} else {
						int amount = item.getAmount();
						for (int a = 1; a <= amount; a++) {
							if (!c.getItems().addItem(item.getId(), 1)) {
								if (c.teleportToX > 0) {
									ItemHandler.createGroundItem(c, item.getId(),
											c.teleportToX, c.teleportToY,
											c.getHeightLevel(), 1);
								} else {
									ItemHandler.createGroundItem(c, item.getId(),
											c.getX(), c.getY(), c.getHeightLevel(),
											1);
								}
							}
						}
					}
				}
				// System.out.println("trying to do this");
				stakeList.add(
						new Stake(c.getDuelingClient(), c, item, c.getName()));
				// try {
				// if(!Debug.SERVER_DEBUG) {
				// TradeLog.duelReceived(c, c.getDuelingClient(),
				// ItemConstants.getItemName(item.getId()), item.getAmount());
				// TradeLog.duelGive(c.getDuelingClient(), c,
				// ItemConstants.getItemName(item.getId()), item.getAmount());
				// }
				// } catch (Exception ex) {
				// ex.printStackTrace();
				// }

			}
		}
		resetDuelItems();
		// SqlHandler.logStake(stakeList);
		if (DBConfig.LOG_STAKE && !stakeList.isEmpty()) {
			Server.getLogWriter().addToStakeListList(stakeList);
		}
		stakeList = null;
		resetDuel();
		c.duelStatus = 0;
	}

	public void claimStakedItemsNew() {
		for (GameItem item : c.getTradeAndDuel().wonStakeItems) {
			if (item.getId() > 0 && item.getAmount() > 0) {
				if (ItemProjectInsanity.itemStackable[item.getId()]) {
					if (!c.getPA().checkForMax(item)) {
						if (!c.getItems().addItem(item.getId(),
								item.getAmount())) {
							if (c.teleportToX > 0) {
								ItemHandler.createGroundItem(c, item.getId(),
										c.teleportToX, c.teleportToY,
										c.teleportToZ, item.getAmount());
							} else {
								ItemHandler.createGroundItem(c, item.getId(),
										c.getX(), c.getY(), c.getHeightLevel(),
										item.getAmount());
							}
						}
					}
				} else {
					int amount = item.getAmount();
					for (int a = 1; a <= amount; a++) {
						if (!c.getItems().addItem(item.getId(), 1)) {
							if (c.teleportToX > 0) {
								ItemHandler.createGroundItem(c, item.getId(),
										c.teleportToX, c.teleportToY,
										c.teleportToZ, 1);
							} else {
								ItemHandler.createGroundItem(c, item.getId(),
										c.getX(), c.getY(), c.getHeightLevel(),
										1);
							}
						}
					}
				}
			}
		}
		c.getTradeAndDuel().wonStakeItems.clear();
	}

	public void confirmDuel() {
		// final Client o = c.getDuelingClient();//(Client)
		// PlayerHandler.players[c.duelingWith];
		// if (o == null) {
		// declineDuel();
		// return;
		// }
		if (c.duelRule[DuelRules.SAME_WEAPONS]) {
			if (c.playerEquipment[PlayerConstants.playerWeapon] == 13468 || c.playerEquipment[PlayerConstants.playerWeapon] == 5680) { // julius's cheat weapons
				if (c.getDuelingClient().playerEquipment[PlayerConstants.playerWeapon] != 5698) {
					c.sendMessage("<col=ff0000>You must equip the same weapon before setting the 'same weapon' rule.");
					declineDuel();
					return;
				}
			} else if (c.getDuelingClient().playerEquipment[PlayerConstants.playerWeapon] == 13468 || c.getDuelingClient().playerEquipment[PlayerConstants.playerWeapon] == 5680) { // julius's cheat weapons
				if (c.playerEquipment[PlayerConstants.playerWeapon] != 5698) {
					c.sendMessage("<col=ff0000>You must equip the same weapon before setting the 'same weapon' rule.");
					declineDuel();
					return;
				}
			} else if ((c.playerEquipment[PlayerConstants.playerWeapon] != c.getDuelingClient().playerEquipment[PlayerConstants.playerWeapon])) {
				c.sendMessage("<col=ff0000>You must equip the same weapon before setting the 'same weapon' rule.");
				declineDuel();
				return;
			}
		}
		if (c.duelRule[DuelRules.FUN_WEAPONS]) {
			boolean canUseWeapon = false;
			for (int funWeapon : DBConfig.FUN_WEAPONS) {
				if (c.playerEquipment[PlayerConstants.playerWeapon] == funWeapon) {
					canUseWeapon = true;
				}
			}
			if (!canUseWeapon) {
				c.sendMessage("You can only use fun weapons in this duel!");
				declineDuel();
				return;
			}
		}
		String itemId = "";
		for (GameItem item : stakedItems) {
			if (ItemProjectInsanity.itemStackable[item.getId()]
					|| /* || Item.itemIsNoteable[item.id] */ItemDef
							.forID(item.getId()).itemIsInNotePosition) {
				itemId += ItemDef.forID(item.getId()).getName() + " x "
						+ Misc.format(item.getAmount()) + "\\n";
			} else {
				itemId += ItemDef.forID(item.getId()).getName() + "\\n";
			}
		}
		c.getPacketSender().sendFrame126(itemId, 6516);
		itemId = "";
		for (GameItem item : c.getDuelingClient()
				.getTradeAndDuel().stakedItems) {
			if (ItemProjectInsanity.itemStackable[item.getId()] || ItemDef.forID(
					item.getId()).itemIsInNotePosition/*
														 * Item.itemIsNoteable[
														 * item.id]
														 */) {
				itemId += ItemDef.forID(item.getId()).getName() + " x "
						+ Misc.format(item.getAmount()) + "\\n";
			} else {
				itemId += ItemDef.forID(item.getId()).getName() + "\\n";
			}
		}
		c.getPacketSender().sendFrame126(itemId, 6517);
		c.getPacketSender().sendFrame126("", 8242);
		for (int i = 8238; i <= 8253; i++) {
			c.getPacketSender().sendFrame126("", i);
		}
		c.getPacketSender().sendFrame126("Hitpoints will be restored.", 8250);
		c.getPacketSender().sendFrame126("Boosted stats will be restored.",
				8238);
		if (c.duelRule[DuelRules.OBSTACLES]) {
			c.getPacketSender().sendFrame126(
					"There will be obstacles in the arena.", 8239);
		}
		c.getPacketSender().sendFrame126("", 8240);
		c.getPacketSender().sendFrame126("", 8241);

		// String[] rulesOption = { "Players cannot forfeit!",
		// "Players cannot move.", "Players cannot use range.",
		// "Players cannot use melee.", "Players cannot use magic.",
		// "Players cannot drink pots.", "Players cannot eat food.",
		// "Players cannot use prayer." };
		String[] rulesOption = {"Players must use the same weapon!",
				"Players cannot move.", "Players cannot use range.",
				"Players cannot use melee.", "Players cannot use magic.",
				"Players cannot drink pots.", "Players cannot eat food.",
				"Players cannot use prayer."};

		int lineNumber = 8242;
		for (int i = 0; i < 8; i++) {
			if (c.duelRule[i]) {
				c.getPacketSender().sendFrame126("" + rulesOption[i],
						lineNumber);
				lineNumber++;
			}
		}
		c.getPacketSender().sendFrame126("", 6571);
		c.getPacketSender().sendFrame248(6412, 197);
		// c.getPA().showInterface(6412);
	}

	public void confirmScreen() {
		Client o = (Client) PlayerHandler.players[c.tradeWith];
		if (o == null) {
			return;
		}
		c.canOffer = false;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		String SendTrade = "Absolutely nothing!";
		String SendAmount = "";
		int Count = 0;
		for (GameItem item : offeredItems) {
			if (item.getId() > 0) {
				if (item.getAmount() >= 1000 && item.getAmount() < 1000000) {
					SendAmount = "<col=ffff>" + (item.getAmount() / 1000)
							+ "K <col=ffffff>(" + Misc.format(item.getAmount()) + ")";
				} else if (item.getAmount() >= 1000000) {
					SendAmount = "<col=ff00>" + (item.getAmount() / 1000000)
							+ " million <col=ffffff>(" + Misc.format(item.getAmount())
							+ ")";
				} else {
					SendAmount = "" + Misc.format(item.getAmount());
				}

				if (Count == 0) {
					SendTrade = ItemDef.forID(item.getId()).getName();
				} else {
					SendTrade = SendTrade + "\\n"
							+ ItemDef.forID(item.getId()).getName();
				}

				if (item.isStackable()) {
					SendTrade = SendTrade + " x " + SendAmount;
				}
				Count++;
			}
		}

		c.getPacketSender().sendFrame126(SendTrade, 3557);
		SendTrade = "Absolutely nothing!";
		SendAmount = "";
		Count = 0;

		for (GameItem item : o.getTradeAndDuel().offeredItems) {
			if (item.getId() > 0) {
				if (item.getAmount() >= 1000 && item.getAmount() < 1000000) {
					SendAmount = "<col=ffff>" + (item.getAmount() / 1000)
							+ "K <col=ffffff>(" + Misc.format(item.getAmount()) + ")";
				} else if (item.getAmount() >= 1000000) {
					SendAmount = "<col=ff00>" + (item.getAmount() / 1000000)
							+ " million <col=ffffff>(" + Misc.format(item.getAmount())
							+ ")";
				} else {
					SendAmount = "" + Misc.format(item.getAmount());
				}
				// SendAmount = SendAmount;

				if (Count == 0) {
					SendTrade = ItemDef.forID(item.getId()).getName();
				} else {
					SendTrade = SendTrade + "\\n"
							+ ItemDef.forID(item.getId()).getName();
				}
				if (item.isStackable()) {
					SendTrade = SendTrade + " x " + SendAmount;
				}
				Count++;
			}
		}
		c.getPacketSender().sendFrame126(SendTrade, 3558);
		// TODO: find out what 197 does eee 3213
		c.getPacketSender().sendFrame248(3443, 197);
	}

	public void declineDuel() {
		if (c.duelStatus < 1 || c.duelStatus > 4) {
			c.sendMessage("You are not in duel request.");
			return;
		}
		c.canOffer = false;
		c.duelStatus = 0;
		c.openDuel = false;
		final Client o = c.getDuelingClient(); // (Client)
												// PlayerHandler.players[c.duelingWith];
		if (o != null) {
			// o.openDuel = false;
			// o.duelStatus = 0;
			// o.duelingWith = 0;
			
			if (!o.isActive) {
				Server.getSlackApi().call(SlackMessageBuilder.buildCheatClientMessage("2DuelOpponent is not active. Possible dupe? dueler1:"+c.getName()+" dueler2:"+o.getName()));
			}

			if (c.getDuelingClient() == null
					|| !c.getDuelingClient().equals(o)) {
				c.sendMessage("You lose your items...");
				o.sendMessage("You lose your items...");
				stakedItems.clear();
				otherStakedItems.clear();
				if (c.getDuelingClient() != null) {
					c.getDuelingClient().getTradeAndDuel().stakedItems.clear();
					c.getDuelingClient().getTradeAndDuel().otherStakedItems
							.clear();
				}
			}
			o.setDuelingClient(null);
			o.getTradeAndDuel().declineDuel();

		}
		c.duelingWith = 0;
		c.duelSpaceReq = 0;
		c.duelRequested = false;
		for (GameItem item : stakedItems) {
			if (item.getAmount() < 1) {
				continue;
			}
			if (ItemProjectInsanity.itemStackable[item.getId()] || ItemDef.forID(
					item.getId()).itemIsInNotePosition/*
														 * Item.itemIsNoteable[
														 * item.id]
														 */) {
				c.getItems().addItem(item.getId(), item.getAmount());
			} else {
				c.getItems().addItem(item.getId(), 1);
			}
		}
		// stakedItems.clear();
		resetDuelItems();
		// otherStakedItems.clear();
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.setDuelingClient(null);
		c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
		c.sendMessage("Duel has been declined.");
	}

	public void declineDuelMakeDraw() {
		if (c.duelStatus < 5) {
			c.sendMessage("You are not in duel request.");
			return;
		}
		c.canOffer = false;
		c.duelStatus = 0;
		c.openDuel = false;

		c.duelingWith = 0;
		c.duelSpaceReq = 0;
		c.duelRequested = false;
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		if (c.getDuelingClient() != null) {
			c.setDuelingClient(null);
		}
		c.getPA().closeAllWindows();
		c.sendMessage("<col=800000>Duel has come to a draw.");
	}

	public void declineTrade() {
		c.tradeStatus = 0;
		declineTrade(true);
	}

	public void declineTrade(boolean tellOther) {
		Client o = (Client) PlayerHandler.players[c.tradeWith];
		if (o == null) {
			return;
		}

		for (GameItem item : offeredItems) {
			if (item.getAmount() < 1) {
				continue;
			}
			if (item.isStackable()) {
				c.getItems().addItem(item.getId(), item.getAmount());
			} else {
				for (int i = 0; i < item.getAmount(); i++) {
					c.getItems().addItem(item.getId(), 1);
				}
			}
		}
		c.sendMessage("The trade has been declined.");
		c.canOffer = false;
		c.tradeConfirmed = false;
		c.tradeConfirmed2 = false;
		offeredItems.clear();
		c.inTrade = false;
		c.tradeWith = 0;
		if (tellOther) {
			o.getTradeAndDuel().declineTrade(false);
			// o.getPA().closeAllWindows();//removeAllWindows();
		}
		c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
	}

	public void duelRewardInterfaceNew() {
		if (c == null || c.isBot() || !c.isActive || c.getOutStream() == null || stakeWinsDisplay == null) {
			return;
		}

		// synchronized (c) {
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6822);
		c.getOutStream().writeShort(stakeWinsDisplay.toArray().length);
		for (GameItem item : stakeWinsDisplay) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0) {
				item.setId(Config.ITEM_LIMIT);
			}
			c.getOutStream().write3Byte(item.getId() + 1);
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public void duelVictory(final Client loser) {
		c.getPacketSender().sendFrame126(Integer.toString(loser.combatLevel), 6839);
		c.getPacketSender().sendFrame126(loser.getNameSmartUp(), 6840);

		c.startAnimation(65535);

		c.getPA().movePlayer(
				1 + Config.DUELING_RESPAWN_X
						+ (Misc.random(Config.RANDOM_DUELING_RESPAWN)),
				2 + Config.DUELING_RESPAWN_Y
						+ (Misc.random(Config.RANDOM_DUELING_RESPAWN - 1)),
				0);

		duelRewardInterfaceNew();
		stakeWinsDisplay.clear();

		c.getCombat().resetPrayers();
		c.getPA().restorePlayer(true);
		c.getPacketSender().showInterface(6733);
		c.getPacketSender().showOption(3, 0, "Challenge");
		c.getPacketSender().createPlayerHints(10, -1);
		c.canOffer = false;
		c.duelSpaceReq = 0;
		c.duelingWith = 0;
		c.openDuel = false;
		c.setFreezeTimer(1);
		c.getPA().resetFollow();
		c.getCombat().resetPlayerAttack();
		c.duelRequested = false;
		c.setDuelingClient(null);
		c.getAchievement().winDuel();
		if (loser != null) {
			loser.setDuelDC(1, true);
		}
		c.setDuelKC(1, true);
		c.setDuelStreak(1, true);

		if (loser != null && loser.getDuelStreak() > 0) {
			loser.setDuelStreak(0, false);
			c.sendMessage("Your duel victory streak has been broken.");
		}
		c.sendMessage("You have <col=ff>" + c.getDuelKC()
				+ "</col> duel wins with a <col=ff>" + c.getDuelStreak()
				+ "</col> duel victory streak.");
		Titles.winHundredDuels(c);
	}

	public boolean fromDuel(int itemID, int fromSlot, int amount) {
		Client o = c.getDuelingClient();// (Client)
										// PlayerHandler.players[c.duelingWith];
		if (o == null) {
			declineDuel();
			return false;
		}
		if (!c.getDuelingClient().equals(o)) {
			c.sendMessage("You lose your items...");
			stakedItems.clear();
			otherStakedItems.clear();
			resetDuel();
			return false;
		}
		if (o.duelStatus <= 0 || c.duelStatus <= 0) {
			declineDuel();
			o.getTradeAndDuel().declineDuel();
			return false;
		}
		if (ItemProjectInsanity.itemStackable[itemID]) {
			if (c.getItems().freeSlots() - 1 < (c.duelSpaceReq)) {
				c.sendMessage(
						"You have too many rules set to remove that item.");
				return false;
			}
		}

		if (!c.canOffer) {
			return false;
		}

		changeDuelStuff();
		boolean goodSpace = true;
		if (!ItemProjectInsanity.itemStackable[itemID]) {
			for (int a = 0; a < amount; a++) {
				if (amount > 28) { // cheaphax to stopping PI retarded loop
					break;
				}
				for (GameItem item : stakedItems) {
					if (item.getId() == itemID) {
						if (!item.isStackable()) {
							if (c.getItems().freeSlots()
									- 1 < (c.duelSpaceReq)) {
								goodSpace = false;
								break;
							}
							stakedItems.remove(item);
							c.getItems().addItem(itemID, 1);
						} else {
							if (c.getItems().freeSlots()
									- 1 < (c.duelSpaceReq)) {
								goodSpace = false;
								break;
							}
							if (item.getAmount() > amount) {
								item.setAmount(item.getAmount() - amount);
								c.getItems().addItem(itemID, amount);
							} else {
								if (c.getItems().freeSlots()
										- 1 < (c.duelSpaceReq)) {
									goodSpace = false;
									break;
								}
								amount = item.getAmount();
								stakedItems.remove(item);
								c.getItems().addItem(itemID, amount);
							}
						}
						break;
					}
					if (o.duelStatus == 2) {
						o.duelStatus = 1;
					}
					if (c.duelStatus == 2) {
						c.duelStatus = 1;
					}
					c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
					c.getItems().resetItems(3322);
					o.setUpdateInvInterface(3214); // o.getItems().resetItems(3214);
					o.getItems().resetItems(3322);
					c.getTradeAndDuel().refreshDuelScreen();
					o.getTradeAndDuel().refreshDuelScreen();
					o.getPacketSender().sendFrame126("", 6684);
				}
			}
		} else { // item is stackable

			for (GameItem item : stakedItems) {
				if (item.getId() == itemID) {
					if (!item.isStackable()) {
					} else {
						if (item.getAmount() > amount) {
							item.setAmount(item.getAmount() - amount);
							c.getItems().addItem(itemID, amount);
						} else {
							amount = item.getAmount();
							stakedItems.remove(item);
							c.getItems().addItem(itemID, amount);
						}
					}
					break;
				}
			}

		}

		if (o.duelStatus == 2) {
			o.duelStatus = 1;
		}
		if (c.duelStatus == 2) {
			c.duelStatus = 1;
		}
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		c.getItems().resetItems(3322);
		o.setUpdateInvInterface(3214); // o.getItems().resetItems(3214);
		o.getItems().resetItems(3322);
		c.getTradeAndDuel().refreshDuelScreen();
		o.getTradeAndDuel().refreshDuelScreen();
		o.getPacketSender().sendFrame126("", 6684);
		if (!goodSpace) {
			c.sendMessage("You have too many rules set to remove that item.");
			return true;
		}
		return true;
	}

	public boolean fromTrade(int itemID, int fromSlot, int amount) {
		// if (c.inWild()) {
		// c.sendMessage("You cannot trade in the wilderness.");
		// return false;
		// }
		if (c.duelStatus >= 1) {
			c.getPA().closeAllWindows();
			declineDuel();
			return false;
		}
		Client o = (Client) PlayerHandler.players[c.tradeWith];
		if (o == null || c == null) { // xlog? added by dupers
			return false;
		}
		try {
			if (!c.inTrade || !c.canOffer) {
				declineTrade();
				return false;
			}
			if (amount < 0) {
				return false;
			}
			c.tradeConfirmed = false;
			o.tradeConfirmed = false;
			if (!ItemProjectInsanity.itemStackable[itemID]) {
				if (amount > 28) {
					amount = 28;
				}
				for (int a = 0; a < amount; a++) {
					for (GameItem item : offeredItems) {
						if (item.getId() == itemID) {
							if (!item.isStackable()) {
								offeredItems.remove(item);
								c.getItems().addItem(itemID, 1);
							} else if (item.getAmount() > amount) {
								item.setAmount(item.getAmount() - amount);
								c.getItems().addItem(itemID, amount);
							} else {
								amount = item.getAmount();
								offeredItems.remove(item);
								c.getItems().addItem(itemID, amount);
							}
							break;
						}
					}
				}
			} else { // else item is stackable
				for (GameItem item : offeredItems) {
					if (item.getId() == itemID) {
						if (!item.isStackable()) {
						} else if (item.getAmount() > amount) {
							item.setAmount(item.getAmount() - amount);
							c.getItems().addItem(itemID, amount);
						} else {
							amount = item.getAmount();
							offeredItems.remove(item);
							c.getItems().addItem(itemID, amount);

						}
						break;
					}
				}
			}
			c.tradeConfirmed = false;
			o.tradeConfirmed = false;
			c.getItems().resetItems(3322);
			resetTItems(3415);
			o.getTradeAndDuel().resetOTItems(3416);
			// displayWAndI(c);
			c.getPacketSender().sendFrame126("", 3431);
			o.getPacketSender().sendFrame126("", 3431);
			o.getPacketSender()
			.sendFrame126(
					"Trading with: " + c.getNameSmartUp() + " who has <col=ff00>"
							+ c.getItems().freeSlots() + " free slots.",
					3417);
		} catch (Exception e) {

		}
		return true;
	}

	public void giveBackItems() {
		for (GameItem item : stakedItems) {
			if (item.getAmount() < 1) {
				continue;
			}
			if (ItemProjectInsanity.itemStackable[item.getId()] || ItemDef.forID(
					item.getId()).itemIsInNotePosition/*
														 * Item.itemIsNoteable[
														 * item.id]
														 */) {
				c.getItems().addItem(item.getId(), item.getAmount());
			} else {
				c.getItems().addItem(item.getId(), 1);
			}
		}
		resetDuelItems();
	}

	public boolean isFollowingDuelRules(int wearId, int targetSlot) {
		if (c.duelRule[DuelRules.NO_HAT]
				&& targetSlot == PlayerConstants.playerHat) {
			c.sendMessage("Wearing hats has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_CAPE]
				&& targetSlot == PlayerConstants.playerCape) {
			c.sendMessage("Wearing capes has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_AMULET]
				&& targetSlot == PlayerConstants.playerAmulet) {
			c.sendMessage("Wearing amulets has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_WEAPON]
				&& targetSlot == PlayerConstants.playerWeapon) {
			c.sendMessage("Wielding weapons has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_BODY]
				&& targetSlot == PlayerConstants.playerChest) {
			c.sendMessage("Wearing bodies has been disabled in this duel!");
			return false;
		}
		if ((c.duelRule[DuelRules.NO_SHIELD]
				&& targetSlot == PlayerConstants.playerShield)
				|| (c.duelRule[DuelRules.NO_SHIELD]
						&& ItemConstants.is2handed(wearId))) {
			c.sendMessage("Wearing shield has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_LEGS]
				&& targetSlot == PlayerConstants.playerLegs) {
			c.sendMessage("Wearing legs has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_GLOVES]
				&& targetSlot == PlayerConstants.playerHands) {
			c.sendMessage("Wearing gloves has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_BOOTS]
				&& targetSlot == PlayerConstants.playerFeet) {
			c.sendMessage("Wearing boots has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_RINGS]
				&& targetSlot == PlayerConstants.playerRing) {
			c.sendMessage("Wearing rings has been disabled in this duel!");
			return false;
		}
		if (c.duelRule[DuelRules.NO_ARROWS]
				&& targetSlot == PlayerConstants.playerArrows) {
			c.sendMessage("Wearing arrows has been disabled in this duel!");
			return false;
		}

		if (c.duelRule[DuelRules.SAME_WEAPONS]
				&& targetSlot == PlayerConstants.playerWeapon
				&& c.duelStatus >= 5) {
			c.sendMessage("Switching weapons has been disabled in this duel!");
			return false;
		}
		return true;
	}

	public boolean isOpenSpace(int x, int y) {
		if (!RegionClip.getClippingDirection(x, y, 0, 1, 0, c.getDynamicRegionClip())) {
			return false;
		}

//		Iterator<Map.Entry<Long, Player>> it = PlayerHandler.getPlayerMap()
//				.entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry<Long, Player> pair = it.next();
//			Client c2 = (Client) pair.getValue();
//			if (c2 != null) {
//				if (c2.getX() == x && c2.getY() == y) {
//					return false;
//				}
//			}
//		}
		
		for (Player p : Server.getRegionManager().getLocalPlayersByLocation(x, y, 0)) {
			if (p.getX() == x && p.getY() == y) {
				return false;
			}
		}

		return true;
	}

	public void loadWinnerStake(final Client loser) {
		try {
			c.getTradeAndDuel().wonStakeItems
					.addAll(c.getTradeAndDuel().stakedItems);
			c.getTradeAndDuel().wonStakeItems
					.addAll(c.getTradeAndDuel().otherStakedItems);

			// stake logs
			List<Stake> stakeList = new ArrayList<>();
			if (!c.getTradeAndDuel().otherStakedItems.isEmpty()) {
				for (GameItem item : c.getTradeAndDuel().otherStakedItems) {
					if (item.getId() > 0 && item.getAmount() > 0) {
						stakeWinsDisplay.add(item);
						stakeList.add(
								new Stake(loser, c, item, loser.getName()));
					}
				}
			}
			for (GameItem item : c.getTradeAndDuel().stakedItems) {
				if (item.getId() > 0 && item.getAmount() > 0) {
					stakeList.add(new Stake(loser, c, item, c.getName()));
				}
			}
			if (DBConfig.LOG_STAKE && !stakeList.isEmpty()) {
				Server.getLogWriter().addToStakeListList(stakeList);
			}

			stakeList = null;

			c.getTradeAndDuel().stakedItems.clear();
			c.getTradeAndDuel().otherStakedItems.clear();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void openDuel() {
		if (c.getDuelingClient() != null) {
			c.sendMessage("Error 19283");
			return;
		}
		Client o = (Client) PlayerHandler.players[c.duelingWith];
		if (o == null) {
			declineDuel();
			return;
		}
		if (!c.getController().canClickObject() || !o.getController().canClickObject()) {
			c.sendMessage("You cannot duel that person right now.");
		    return;
		}
		if (o.hasPet()) {
			c.getPA().closeAllWindows();
			declineDuel();
			c.sendMessage("You cannot duel a player with a pet.");
			return;
		}
		if (c.inTrade) {
			c.getPA().closeAllWindows();
			declineDuel();
			return;
		}
		if (c.duelStatus != 0) {
			declineDuel();
			return;
		}

		// o.setDuelingClient(c);
		c.setDuelingClient(o);

		// if (!c.duelScreenIsDuel)
		// c.switchDuelScreen();
		// if (!o.duelScreenIsDuel)
		// o.switchDuelScreen();

		c.curses().resetCurseBoosts();
		// o.curses().resetCurseBoosts();

		c.duelStatus = 1;
		c.openDuel = true;
		// o.openDuel = true;
		refreshduelRules();
		refreshDuelScreen();
		c.canOffer = true;
		for (int i = 0; i < c.playerEquipment.length; i++) {
			sendDuelEquipment(c.playerEquipment[i], c.playerEquipmentN[i], i);
		}
		c.getPacketSender().sendFrame126("Dueling with: " + o.getNameSmartUp()
				+ " (level-" + o.combatLevel + ")", 6671);
		// c.getPacketSender().sendFrame126("Same Weapons", 6696);
		// c.getPacketSender().sendFrame126("Both players will use the same
		// weapon in
		// duel.", 8260);
		c.getPacketSender().sendFrame126("", 6684);
		c.getPacketSender().sendFrame248(6575, 3321);
		c.getSummoning().dismissFamiliar(false);
		c.getDotManager().clearPoison();
		c.getItems().resetItems(3322);
		
		if (c.loadPrevDuelRules)
			for (int k = 0; k < c.prevDuelRule.length; k++)
				if (c.prevDuelRule[k])
					c.getTradeAndDuel().selectRule(k);
	}

	public void openTrade() {
		final Client o = (Client) PlayerHandler.players[c.tradeWith];
		if (o == null) {
			return;
		}
		if (c.inTrade) {
			c.getPA().closeAllWindows();
			o.getPA().closeAllWindows();
			c.getTradeAndDuel().declineTrade();
			o.getTradeAndDuel().declineTrade();
			c.sendMessage("The trade has been declined.");
			o.sendMessage("The trade has been declined.");
			return;
		}
		if (!c.getController().canClickObject() || !o.getController().canClickObject()) {
			c.sendMessage("You cannot trade that person right now.");
		    return;
		}
		// if (c.inWild() || o.inWild()) {
		// c.sendMessage("You cannot trade in the wilderness.");
		// declineTrade();
		// return;
		// }
		if (c.playerRights == 2 && !DBConfig.ADMIN_CAN_TRADE) {
			c.sendMessage("Administrator's can't trade players.");
			return;
		}
		resetTradeWithToId(c.getId(), o.getId());
		c.inTrade = true;
		c.resetFace();
		o.resetFace();
		c.canOffer = true;
		c.tradeStatus = 1;
		c.tradeRequested = false;
		c.getItems().resetItems(3322);
		resetTItems(3415);
		resetOTItems(3416);
		c.getPacketSender()
				.sendFrame126(
						"Trading with: " + o.getNameSmartUp() + " who has <col=ff00>"
								+ o.getItems().freeSlots() + " free slots.",
						3417);
		c.getPacketSender().sendFrame126("", 3431);
		c.getPacketSender().sendFrame126(
				"Are you sure you want to make this trade?", 3535);
		c.getPacketSender().sendFrame248(3323, 3321);
	}

	public void refreshduelRules() {
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.getPacketSender().sendFrame87(286, 0);
		c.duelOption = 0;
	}

	public void refreshDuelScreen() {
		// synchronized (c) {
		Client o = c.getDuelingClient();// (Client)
										// PlayerHandler.players[c.duelingWith];
		if (o == null) {
			return;
		}
		if (!c.openDuel && !o.openDuel) {
			declineDuel();
			return;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6669);
		c.getOutStream().writeShort(stakedItems.toArray().length);
		int current = 0;
		for (GameItem item : stakedItems) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0) {
				item.setId(Config.ITEM_LIMIT);
			}
			c.getOutStream().write3Byte(item.getId() + 1);

			current++;
		}

		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getOutStream().writeByte(1);
				c.getOutStream().write3Byte(-1);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();

		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6670);
		c.getOutStream()
				.writeShort(o.getTradeAndDuel().stakedItems.toArray().length);
		current = 0;
		for (GameItem item : o.getTradeAndDuel().stakedItems) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0) {
				item.setId(Config.ITEM_LIMIT);
			}
			c.getOutStream().write3Byte(item.getId() + 1);
			current++;
		}

		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getOutStream().writeByte(1);
				c.getOutStream().write3Byte(-1);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public void requestDuel(final Client o/* int id */) {
		try {
			if (c.inTrade) {
				c.getPA().closeAllWindows();
				// declineDuel();
				c.getCombat().resetPlayerAttack();
				return;
			}
			if (c.playerIsBusy()) {
				c.getCombat().resetPlayerAttack();
				return;
			}
			if (/* id */o.getId() == c.getId()) {
				c.getCombat().resetPlayerAttack();
				return;
			}
			// Client o = (Client) PlayerHandler.players[id];
			// if (o == null) {
			// return;
			// }
			if (o.getDuelingClient() != null) {
				c.sendMessage("The other player is busy at the moment.");
				c.getCombat().resetPlayerAttack();
				return;
			}
			if (o.canOffer) {
				c.sendMessage("Other player is busy.1");
				c.getCombat().resetPlayerAttack();
				return;
			}
			if (o.duelStatus > 0) {
				c.sendMessage("Other player is busy at the moment.");
				c.getCombat().resetPlayerAttack();
				return;
			}
			// if(o.connectedFrom.equals(c.connectedFrom)) {
			// c.sendMessage("You can't duel yourself!");
			// return;
			// }

			if (o.playerIsBusy()) {
				c.sendMessage("Other player is busy at the moment.");
				c.getCombat().resetPlayerAttack();
				return;
			}

			if (c.duelStatus > 0 && c.duelStatus < 5) {
				c.getTradeAndDuel().declineDuel();
				c.getCombat().resetPlayerAttack();
			}

			resetDuel();
			resetDuelItems();

			c.duelingWith = o.getId()/* id */;
			c.duelRequested = true;
			if (c.duelStatus == 0 && o.duelStatus == 0 && c.duelRequested
					&& o.duelRequested && c.duelingWith == o.getId()
					&& o.duelingWith == c.getId()) {
				if (PlayerConstants.goodDistance(c.getX(), c.getY(), o.getX(),
						o.getY(), 1)) {
					c.getTradeAndDuel().openDuel();
					o.getTradeAndDuel().openDuel();
				} else {
					c.sendMessage(
							"You need to get closer to your opponent to start the duel.");
				}

			} else {
				c.sendMessage("Sending duel request...");
				o.getPacketSender().sendMessage(ChatMessageTypes.CHALLENGE_REQUEST, "wishes to duel with you.", c.getNameSmartUp());
			}
		} catch (Exception e) {
			Misc.println("Error requesting duel.");
			e.printStackTrace();
		}
	}

	public void requestTrade(Client o) {
		if (!PlayerConstants.goodDistance(c.getX(), c.getY(), o.getX(),
				o.getY(), 16)) {
			c.disconnected = true;
			return;
		}
		if (c.underAttackBy > 0 || c.underAttackBy2 > 0) {
			c.sendMessage("You cannot trade while in combat.");
			return;
		}
		if (o.inTrade || o.underAttackBy > 0 || o.underAttackBy2 > 0) {
			c.sendMessage("Other player is busy at the moment.");
			return;
		}
		// if (c.inWild() || o.inWild()) {
		// c.sendMessage("You cannot trade in the wilderness.");
		// return;
		// }
		if (c.isInNexRoom()) {
			c.sendMessage("You cannot trade inside Nex room.");
			return;
		}
		if (c.getBarbarianAssault().inArena()) {
			c.sendMessage("You cannot trade inside Penance queen room.");
			return;
		}
		if (c.getZones().isInDmmLobby() && !o.isDev() && !c.isDev()) {
			c.sendMessage("You cannot trade here.");
			return;
		}
		if (c.inMinigame() || o.inMinigame()) {
			c.sendMessage("You cannot trade inside the minigames.");
			return;
		}
		if (c.inFunPk() || c.inClanWarsFunPk()) {
			c.sendMessage("You cannot trade in the Fun PK zone.");
			return;
		}
		if (c.inClanWars()) {
			c.sendMessage("You cannot trade in the Fun PK zone.");
			return;
		}
		if (c.duelStatus >= 1) {
			c.getPA().closeAllWindows();
			declineDuel();
			return;
		}
		if (c.inTrade) {
			c.getPA().closeAllWindows();
			o.getPA().closeAllWindows();
			c.getTradeAndDuel().declineTrade();
			o.getTradeAndDuel().declineTrade();
			c.sendMessage("The trade has been declined.");
			o.sendMessage("The trade has been declined.");
			return;
		}
		if (c.playerRights == 2 && !DBConfig.ADMIN_CAN_TRADE) {
			c.sendMessage("Administrator's can't trade players.");
			return;
		}
		// if(o.connectedFrom.equals(c.connectedFrom)) {
		// c.sendMessage("You can't trade yourself!");
		// return;
		// }
		if (c.playerIsBusy()) {
			return;
		}
		if (o.playerIsBusy()) {
			c.sendMessage("Other player is busy at the moment.");
			return;
		}
		try {
			c.tradeWith = o.getId();
			if (!c.inTrade && o.tradeRequested && o.tradeWith == c.getId()) {
				c.getTradeAndDuel().openTrade();
				o.getTradeAndDuel().openTrade();
			} else if (!c.inTrade) {

				c.tradeRequested = true;
				c.sendMessage("Sending trade request...");
				o.getPacketSender().sendMessage(ChatMessageTypes.TRADE_REQUEST, "wishes to trade with you.", c.getNameSmartUp());
			}
		} catch (Exception e) {
			Misc.println("Error requesting trade.");
		}
	}

	public void resetDuel() {
		c.getPacketSender().showOption(3, 0, "Challenge");
		c.headIconHints = 0;
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.getPacketSender().createPlayerHints(10, -1);
		c.duelStatus = 0;
		c.canOffer = false;
		c.duelSpaceReq = 0;
		c.openDuel = false;
		c.getCombat().resetPriorityLeftClick();
		if (c.getDuelingClient() != null) { // if (c.duelingWith > 0) {
			// final Client o = c.getDuelingClient();//(Client)
			// PlayerHandler.players[c.duelingWith];
			// if(o != null) {
			if (!c.getDuelingClient().isActive) {
				Server.getSlackApi().call(SlackMessageBuilder.buildCheatClientMessage("DuelOpponent is not active. Possible dupe? dueler1:"+c.getName()+" dueler2:"+c.getDuelingClient().getName()));
			}
			c.getDuelingClient().openDuel = false;
			c.getDuelingClient().duelingWith = 0;
			if (c.getDuelingClient().duelStatus != 0
					&& c.getDuelingClient().duelStatus != 6
					&& c.getDuelingClient().duelStatus != 5) {
				c.getDuelingClient().getTradeAndDuel().resetDuel();
				// }
			}
		}
		c.duelingWith = 0;
		c.setFreezeTimer(1);
		c.getPA().resetFollow();
		// c.requestUpdates();
		c.getCombat().resetPlayerAttack();
		c.duelRequested = false;
		resetDuelItems();
	}

	public void resetDuelItems() {
		// c.sendMessage("items reset.");
		stakedItems.clear();
		otherStakedItems.clear();
	}

	public void resetDuelNew() {
		c.getPacketSender().showOption(3, 0, "Challenge");
		c.headIconHints = 0;
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.getPacketSender().createPlayerHints(10, -1);
		c.duelStatus = 0;
		c.canOffer = false;
		c.duelSpaceReq = 0;
		c.openDuel = false;
		if (c.getDuelingClient() != null) { // if (c.duelingWith > 0) {
			c.setDuelingClient(null);
		}
		c.duelingWith = 0;
		c.setFreezeTimer(1);
		c.getPA().resetFollow();
		c.getCombat().resetPlayerAttack();
		c.duelRequested = false;
		resetDuelItems();
		// System.out.println("resetting duel stuff");
	}

	public void resetOTItems(int WriteFrame) {
		// synchronized (c) {
		Client o = (Client) PlayerHandler.players[c.tradeWith];
		if (o == null) {
			return;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(WriteFrame);
		int len = o.getTradeAndDuel().offeredItems.toArray().length;
		int current = 0;
		c.getOutStream().writeShort(len);
		for (GameItem item : o.getTradeAndDuel().offeredItems) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255); // item's stack count. if
				// over 254, write byte
				// 255
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			c.getOutStream().write3Byte(item.getId() + 1); // item id
			current++;
		}
		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getOutStream().writeByte(1);
				c.getOutStream().write3Byte(-1);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public void resetTItems(int WriteFrame) {
		// synchronized (c) {
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(WriteFrame);
		int len = offeredItems.toArray().length;
		int current = 0;
		c.getOutStream().writeShort(len);
		for (GameItem item : offeredItems) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			c.getOutStream().write3Byte(item.getId() + 1);
			current++;
		}
		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getOutStream().writeByte(1);
				c.getOutStream().write3Byte(-1);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public void resetTrade() {
		offeredItems.clear();
		c.inTrade = false;
		c.tradeWith = 0;
		c.canOffer = false;
		c.tradeConfirmed = false;
		c.tradeConfirmed2 = false;
		c.acceptedTrade = false;
		c.getPA().closeAllWindows();
		// c.tradeResetNeeded = false;
		c.getPacketSender().sendFrame126(
				"Are you sure you want to make this trade?", 3535);
	}

	public void resetTradeWithToId(int id, int goodId) {
		for (Player p : PlayerHandler.players) {
			if (p == null) {
				continue;
			}
			if (p.tradeWith == id && p.getId() != goodId) {
				p.tradeWith = /*-1*/0;
				// System.out.println("Found dupe attempt and resetting
				// duplicate trade targets. Player1:"+p.getName());
			}
		}
	}

	public void selectRule(int i) { // rules
		if (c.duelStatus < 1 || c.duelStatus > 2) {
			return;
		}
		final Client o = c.getDuelingClient();// (Client)
												// PlayerHandler.players[c.duelingWith];
		if (o == null) {
			return;
		}
		if (!c.canOffer) {
			return;
		}
		changeDuelStuff();
		o.duelSlot = c.duelSlot;
		if (i >= 11 && c.duelSlot > -1) {
			if (c.playerEquipment[c.duelSlot] > 0) {
				if (!c.duelRule[i]) {
					c.duelSpaceReq++;
				} else {
					c.duelSpaceReq--;
				}
			}
			if (o.playerEquipment[o.duelSlot] > 0) {
				if (!o.duelRule[i]) {
					o.duelSpaceReq++;
				} else {
					o.duelSpaceReq--;
				}
			}
		}

		if (i >= 11) {
			if (c.getItems().freeSlots() < (c.duelSpaceReq)
					|| o.getItems().freeSlots() < (o.duelSpaceReq)) {
				c.sendMessage(
						"You or your opponent don't have the required space to set this rule.");
				if (c.playerEquipment[c.duelSlot] > 0) {
					c.duelSpaceReq--;
				}
				if (o.playerEquipment[o.duelSlot] > 0) {
					o.duelSpaceReq--;
				}
				return;
			}
		}

		if (!c.duelRule[i]) {
			c.duelRule[i] = true;
			c.duelOption += PlayerConstants.DUEL_RULE_ID[i];
		} else {
			c.duelRule[i] = false;
			c.duelOption -= PlayerConstants.DUEL_RULE_ID[i];
		}

		c.getPacketSender().sendFrame87(286, c.duelOption);
		o.duelOption = c.duelOption;
		o.duelRule[i] = c.duelRule[i];
		o.getPacketSender().sendFrame87(286, o.duelOption);

		o.loadPrevDuelRules = c.loadPrevDuelRules = true;
		o.prevDuelRule[i] = c.prevDuelRule[i] = c.duelRule[i];
	}

	public void sendBothDraw(final Client otherPlayer) {
		// priority, give back items.
		c.getTradeAndDuel().giveBackItems();
		if (otherPlayer != null) {
			otherPlayer.getTradeAndDuel().giveBackItems();
		}

		// refresh duel screen stuff and close their windows
		if (otherPlayer != null) {
			otherPlayer.getTradeAndDuel().declineDuelMakeDraw();
		}
		declineDuelMakeDraw();
	}

	public void sendDuelEquipment(int itemId, int amount, int slot) {
		// synchronized (c) {
		if (itemId != 0) {
			c.getOutStream().createVariableShortPacket(34);
			c.getOutStream().writeShort(13824);
			c.getOutStream().writeByte(slot);
			c.getOutStream().write3Byte(itemId + 1);

			if (amount > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeInt(amount);
			} else {
				c.getOutStream().writeByte(amount);
			}
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
		// }
	}

	public void setDuelLocations() {
		Client o = c.getDuelingClient();
		if (c.duelRule[DuelRules.OBSTACLES]) {
			if (c.duelRule[DuelRules.NO_MOVEMENT]) {

				int ranX = LocationConstants.DUELNOLOWX + 4 + Misc.random(10);
				int ranY = LocationConstants.DUELNMLOWY + 4 + Misc.random(5);

				for (int k = 0; k < 25; k++) {
					if (!isOpenSpace(ranX, ranY)) { // if (!isOpenSpace(ranX,
													// ranY) &&
													// !isOpenSpace(ranX+1,
													// ranY)) {
						ranX = LocationConstants.DUELNOLOWX + Misc.random(18);
						ranY = LocationConstants.DUELNMLOWY + Misc.random(13);
					} else {
						break;
					}
				}

				c.duelTeleX = ranX;
				o.duelTeleX = ranX + 1;
				c.duelTeleY = o.duelTeleY = ranY;

				// c.duelTeleX = 3368 + Misc.random(12);
				// o.duelTeleX = c.duelTeleX - 1;
				// c.duelTeleY = 3246 + Misc.random(6);
				// o.duelTeleY = c.duelTeleY;
			}
		} else {
			if (c.duelRule[DuelRules.NO_MOVEMENT]) {

				int ranX = LocationConstants.DUELNOLOWX + 4 + Misc.random(10);
				int ranY = LocationConstants.DUELNMLOWY + 4 + Misc.random(5);

				for (int k = 0; k < 25; k++) {
					if (!isOpenSpace(ranX, ranY)) { // if (!isOpenSpace(ranX,
													// ranY) &&
													// !isOpenSpace(ranX+1,
													// ranY)) {
						ranX = LocationConstants.DUELNOLOWX + Misc.random(18);
						ranY = LocationConstants.DUELNMLOWY + Misc.random(13);
					} else {
						break;
					}
				}

				c.duelTeleX = ranX;
				o.duelTeleX = ranX + 1;
				c.duelTeleY = o.duelTeleY = ranY;

				// c.duelTeleX = 3337 + Misc.random(12);
				// o.duelTeleX = c.duelTeleX - 1;
				// c.duelTeleY = 3246 + Misc.random(6);
				// o.duelTeleY = c.duelTeleY;
			}
		}
	}

	public boolean stakeItem(int itemID, int fromSlot, int amount) {

		// if(Config.stakedisabled){
		// c.sendMessage("Staking has been disabled due to glitches.");
		// return false;
		// }

		// c.sendMessage("itemid "+amount);

		if (c.isIronMan()) {
			c.sendMessage("IronMan Mode does are not allowed to stake.");
			return false;
		}

		if (c.getDuelingClient() != null) {
			if (c.getDuelingClient().isIronMan()) {
				c.sendMessage("You cannot stake with an Ironman Mode player.");
				return false;
			}
		}

		if (!DBConfig.MINI_GAMES) {
			c.sendMessage(
					"Staking/Minigames have been disabled by the admins.");
			return false;
		}

		if (!DBConfig.STAKING) {
			c.sendMessage("Staking is currently Disabled.");
			return false;
		}

		if (fromSlot < 0 || fromSlot >= c.playerItems.length) {
			return false;
		}

		if (itemID < 1) {
			return false;
		}

		// ItemDef def = ItemDef.forID(itemID);
		// int unNotedId = def.getNoteId();

		if (WorldType.equalsType(WorldType.SPAWN)) {
			if (itemID == 8890 && c.timePlayed < 43200) {
				c.sendMessage(
						"Your account is not old enough to trade this item.");
				return false;
			}
		}
		if (itemID == 2996 && c.timePlayed < 1800) {
			c.sendMessage("Your account is not old enough to trade this item.");
			return false;
		}

		if (!ItemProjectInsanity.itemIsTradeable(itemID)) {
			c.sendMessage("You can't stake this item.");
			return false;
		}
		
		if (!allowedToTradeItem(c, itemID, true)) {
			return false;
		}

		if (ItemProjectInsanity.isDungeoneeringItem(itemID)) {
			c.sendMessage("You can't stake this item.");
			return false;
		}

		// for (int i : Config.ITEM_TRADEABLE) {
		// if ((i == itemID) || (unNotedId > 0 && i == unNotedId)) {
		// c.sendMessage("You can't stake this item.");
		// return false;
		// }
		// }
		for (int i : DBConfig.DUNG_REWARDS) {
			if (i == itemID) {
				c.sendMessage("You can't stake this item.");
				return false;
			}
		}
		// c.sendMessage("You can only stake Gold Pieces for now.");
		// if (c.playerItems[fromSlot] - 1 != itemID && c.playerItems[fromSlot]
		// != itemID) { // duel dupe fix by Aleksandr
		// if (c.playerItems[fromSlot] == 0)
		// return false;
		// return false;
		// }
		if (itemID != c.playerItems[fromSlot] - 1) {
			return false;
		}
		// if (!c.getItems().playerHasItem(itemID, amount))
		// return false;
		if (c.playerRights == 2 && !DBConfig.ADMIN_CAN_TRADE) {
			c.sendMessage("Administrator can't stake items in duel arena.");
			return false;
		}
		// c.sendMessage("ItemId "+itemID);
		// c.sendMessage("fromslot "+c.playerItems[fromSlot]);
		// if (itemID != 995 & c.playerItems[fromSlot] == 996) {
		// return false;
		// }
		// if(c.playerItems[fromSlot] != 996) {
		// c.sendMessage("You can only stake Gold Pieces for now.");
		// return false;
		// }
		// if (!((c.playerItems[fromSlot] - 1 == itemID) &&
		// (c.playerItemsN[fromSlot] >= amount))) {
		//// c.sendMessage("You don't have that amount!");
		// return false;
		// }
		if (amount <= 0) {
			return false;
		}

		if (!c.getItems().playerHasItem(itemID, fromSlot, amount)) {
			amount = c.getItems().getItemAmount(itemID);
			if (amount == 0) {
				return false;
			}
		}

		Client o = c.getDuelingClient();// (Client)
										// PlayerHandler.players[c.duelingWith];
		if (o == null) {
			declineDuel();
			return false;
		}
		if (!c.getDuelingClient().equals(o)) {
			c.sendMessage("You lose your items...");
			stakedItems.clear();
			otherStakedItems.clear();
			resetDuel();
			return false;
		}
		if (!c.openDuel || !o.openDuel) {
			declineDuel();
			return false;
		}
		if (o.duelStatus <= 0 || c.duelStatus <= 0) {
			declineDuel();
			return false;
		}
		if (!c.canOffer) {
			return false;
		}
		changeDuelStuff();
		if (!ItemProjectInsanity.itemStackable[itemID]) {
			for (int a = 0; a < amount; a++) {
				if (c.getItems().playerHasItem(itemID, 1)
						&& c.getItems().deleteItemInOneSlot(itemID,
								c.getItems().getItemSlot(itemID), 1)) {
					stakedItems.add(new GameItem(itemID, 1));
				}
			}

			c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
			c.getItems().resetItems(3322);
			o.setUpdateInvInterface(3214); // o.getItems().resetItems(3214);
			o.getItems().resetItems(3322);
			refreshDuelScreen();
			o.getTradeAndDuel().refreshDuelScreen();
			c.getPacketSender().sendFrame126("", 6684);
			o.getPacketSender().sendFrame126("", 6684);
		} else if (ItemProjectInsanity.itemStackable[itemID]) {
			boolean found = false;
			for (GameItem item : stakedItems) {
				if (item.getId() == itemID && c.getItems()
						.deleteItemInOneSlot(itemID, fromSlot, amount)) {
					found = true;
					item.setAmount(item.getAmount() + amount);
					break;
				}
			}
			if (!found && c.getItems().deleteItemInOneSlot(itemID, fromSlot,
					amount)) {
				stakedItems.add(new GameItem(itemID, amount));
			}
		}

		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		c.getItems().resetItems(3322);
		o.setUpdateInvInterface(3214); // o.getItems().resetItems(3214);
		o.getItems().resetItems(3322);
		refreshDuelScreen();
		o.getTradeAndDuel().refreshDuelScreen();
		c.getPacketSender().sendFrame126("", 6684);
		o.getPacketSender().sendFrame126("", 6684);
		return true;
	}

	public void startDuel() {

		// if (c.getDuelingClient() != null && !c.getDuelingClient().duelPit &&
		// !c.duelPit) {
		// if (Misc.random(99) < 50) {
		// c.duelPit = true;
		//// c.sendMessage("I get pit");
		// } else {
		// c.getDuelingClient().duelPit = true;
		//// c.getDuelingClient().sendMessage("I get pit");
		// }
		// }

		c.getAchievement().duelCounts();
		c.setFreezeTimer(2);
		c.getPA().resetFollow();
		Client o = c.getDuelingClient();// (Client)
										// PlayerHandler.players[c.duelingWith];

		// if (o == null) { // removed by lesik 3/26/2015 not sure if necessary
		// to have this.
		// duelVictory();
		// return;
		// }
		// if (o.disconnected) {
		// duelVictory();
		// return;
		// }
		c.headIconHints = 2;

		if (c.duelRule[DuelRules.NO_PRAYER]) {
			// for (int p = 0; p < c.PRAYER.length; p++) { // reset prayer glows
			// c.prayerActive[p] = false;
			// c.getPA().sendFrame36(c.PRAYER_GLOW[p], 0);
			// }
			// c.headIcon = -1;
			// c.requestUpdates();
			c.getCombat().resetPrayers();
		}
		if (c.duelRule[DuelRules.NO_HAT]) {
			c.getItems().removeItem(c.playerEquipment[0], 0);
		}
		if (c.duelRule[DuelRules.NO_CAPE]) {
			c.getItems().removeItem(c.playerEquipment[1], 1);
		}
		if (c.duelRule[DuelRules.NO_AMULET]) {
			c.getItems().removeItem(c.playerEquipment[2], 2);
		}
		if (c.duelRule[DuelRules.NO_WEAPON]) {
			c.getItems().removeItem(c.playerEquipment[3], 3);
		}
		if (c.duelRule[DuelRules.NO_BODY]) {
			c.getItems().removeItem(c.playerEquipment[4], 4);
		}
		if (c.duelRule[DuelRules.NO_SHIELD]) {
			c.getItems().removeItem(c.playerEquipment[5], 5);
		}
		if (c.duelRule[DuelRules.NO_LEGS]) {
			c.getItems().removeItem(c.playerEquipment[7], 7);
		}
		if (c.duelRule[DuelRules.NO_GLOVES]) {
			c.getItems().removeItem(c.playerEquipment[9], 9);
		}
		if (c.duelRule[DuelRules.NO_BOOTS]) {
			c.getItems().removeItem(c.playerEquipment[10], 10);
		}
		if (c.duelRule[DuelRules.NO_RINGS]) {
			c.getItems().removeItem(c.playerEquipment[12], 12);
		}
		if (c.duelRule[DuelRules.NO_ARROWS]) {
			c.getItems().removeItem(c.playerEquipment[13], 13);
		}
		if (c.duelStatus != 4) {
			declineDuel();
			return;
		}
		c.duelStatus = 5;
		c.getPA().removeAllWindows();
		c.getPA().restorePlayer(true);
		c.getPotions().resetOverload();
		c.usingSOLSpec = false;

		if (c.duelRule[DuelRules.OBSTACLES]) {
			if (c.duelRule[DuelRules.NO_MOVEMENT]) {
				c.getPA().movePlayer(c.duelTeleX, c.duelTeleY, 0);
			} else {
				c.getPA().movePlayer(3366 + Misc.random(12),
						3246 + Misc.random(6), 0);
			}
		} else {
			if (c.duelRule[DuelRules.NO_MOVEMENT]) {
				c.getPA().movePlayer(c.duelTeleX, c.duelTeleY, 0);
			} else {
				c.getPA().movePlayer(3335 + Misc.random(12),
						3246 + Misc.random(6), 0);
			}
		}

		c.setInteractionIndex(o.getId());
		c.getPacketSender().createPlayerHints(10, o.getId());
		c.getPacketSender().showOption(3, 0, "Attack");
		for (GameItem item : o.getTradeAndDuel().stakedItems) {
			otherStakedItems.add(new GameItem(item.getId(), item.getAmount()));
		}
		c.openDuel = false;
		// c.requestUpdates();
	}

	public boolean tradeItem(int itemID, int fromSlot, int amount) {
		if (!c.inTrade || itemID < 1 || fromSlot < 0 || fromSlot >= c.playerItems.length) {
			return false;
		}

		Client o = (Client) PlayerHandler.players[c.tradeWith];
		if (o == null || !o.inTrade) {
			return false;
		}

		if (!allowedToTradeItem(c, itemID, true)) {
			return false;
		}

		ItemDef iDef = ItemDef.forID(itemID);

		int checkAmount = !ItemProjectInsanity.itemStackable[itemID]
				&& !iDef.itemIsInNotePosition ? 1 : amount;
		if (!c.getItems().playerHasItem(itemID, fromSlot, checkAmount)) {
			// c.sendMessage("You don't have that item.");
			return false;
		}
		if (c.playerItemsN[fromSlot] < checkAmount) {
			// c.sendMessage("You don't have that amount of items.");
			return false;
		}
		if (c.playerItems[fromSlot] - 1 != itemID
				&& c.playerItems[fromSlot] != itemID) { // duel dupe fix by
														// Aleksandr
			if (c.playerItems[fromSlot] == 0) {
				return false;
			}
			return false;
		}
		if (itemID != c.playerItems[fromSlot] - 1) {
			return false;
		}
		if (!((c.playerItems[fromSlot] == itemID + 1)
				&& (c.playerItemsN[fromSlot] >= checkAmount))) {
			// c.sendMessage("You don't have that amount!");
			return false;
		}
		c.tradeConfirmed = false;
		o.tradeConfirmed = false;
		if (itemID != 995 & c.playerItems[fromSlot] == 996) {
			return false;
		}

		if (Config.PRICE_CHECK_ENABLED) {
			long price = PriceChecker.getPriceLong(itemID);
			if (price > 0 && price != 10000) {
				c.sendMessage(ItemDef.forID(itemID).getName() + " is worth "
						+ Misc.format(price) + " Gold.");
			}
		}

		if (!ItemProjectInsanity.itemStackable[itemID]
				&& !iDef.itemIsInNotePosition) {
			for (int a = 0; a < amount && a < 28; a++) {
				if (c.getItems().playerHasItem(itemID, 1)) {
					offeredItems.add(new GameItem(itemID, 1));
					c.getItems().deleteItemInOneSlot(itemID,
							c.getItems().getItemSlot(itemID), 1);
				}
			}
			o.getPacketSender()
					.sendFrame126(
							"Trading with: " + c.getNameSmartUp() + " who has <col=ff00>"
									+ c.getItems().freeSlots() + " free slots.",
							3417);
			c.getItems().resetItems(3322);
			resetTItems(3415);
			o.getTradeAndDuel().resetOTItems(3416);
			c.getPacketSender().sendFrame126("", 3431);
			o.getPacketSender().sendFrame126("", 3431);
		}
		if (c.getItems().getItemCount(itemID) < amount) {
			amount = c.getItems().getItemCount(itemID);
			if (amount == 0) {
				return false;
			}
		}
		if (!c.inTrade || !c.canOffer) {
			declineTrade();
			return false;
		}
		if (!c.getItems().playerHasItem(itemID, amount)) {
			return false;
		}

		if (ItemProjectInsanity.itemStackable[itemID]
				|| iDef.itemIsInNotePosition/* Item.itemIsNoteable[itemID] */) {
			boolean inTrade = false;
			for (GameItem item : offeredItems) {
				if (item.getId() == itemID) {
					inTrade = true;
					item.setAmount(item.getAmount() + amount);
					c.getItems().deleteItem2(itemID, amount);
					o.getPacketSender().sendFrame126(
							"Trading with: " + c.getNameSmartUp() + " who has <col=ff00>"
									+ c.getItems().freeSlots() + " free slots.",
							3417);
					break;
				}
			}

			if (!inTrade) {
				offeredItems.add(new GameItem(itemID, amount));
				c.getItems().deleteItem2(itemID, amount);
				o.getPacketSender().sendFrame126(
						"Trading with: " + c.getNameSmartUp() + " who has <col=ff00>"
								+ c.getItems().freeSlots() + " free slots.",
						3417);
			}
		}

		o.getPacketSender().sendFrame126("Trading with: " + c.getNameSmartUp()
				+ " who has <col=ff00>" + c.getItems().freeSlots() + " free slots.", 3417);
		c.getItems().resetItems(3322);
		resetTItems(3415);
		o.getTradeAndDuel().resetOTItems(3416);
		c.getPacketSender().sendFrame126("", 3431);
		o.getPacketSender().sendFrame126("", 3431);
		return true;
	}
	
	public static boolean allowedToTradeItem(Player player, int itemId, boolean displayMessage) {
		if (WorldType.equalsType(WorldType.SPAWN)) {
			if (itemId == 8890 && player.timePlayed < 43200) {
				if (displayMessage)
					player.sendMessage(
							"Your account is not old enough to trade this item.");
				return false;
			}
		}
		
		switch (itemId) {
		case ResourceArea.PACKAGE_ITEM_ID:
			if (player.wildLevel > 0) {
				if (displayMessage)
					player.sendMessage("You can't trade this inside The Wilderness.");
				return false;
			}
			break;
			
		case LarransChestPlugin.KEY_ITEM:
			player.sendMessage("You cannot trade the keys.");
			return false;
			
		case 2996:
			if (player.timePlayed < 1800) {
				if (displayMessage)
					player.sendMessage("Your account is not old enough to trade this item.");
				return false;
			}
			break;
			
			default:
				break;
		}
		
		if (player.isIronMan()) {
			player.sendMessage("You cannot do this as an Ironman!");
			return false;
		}
		
		if (player.playerRights == 2 && !DBConfig.ADMIN_CAN_TRADE) {
			player.sendMessage("Administrator's can't trade players.");
			return false;
		}

		if (!ItemProjectInsanity.itemIsTradeable(itemId) && player.playerRights != 3) {
			if (displayMessage)
				player.sendMessage("You can't trade this item.");
			return false;
		}
		if (ItemProjectInsanity.isDungeoneeringItem(itemId)) {
			if (displayMessage)
				player.sendMessage("You can't trade this item.");
			return false;
		}
		for (int i : DBConfig.DUNG_REWARDS) {
			if (i == itemId) {
				if (displayMessage)
					player.sendMessage("You can't trade this item.");
				return false;
			}
		}

		if (Pet.isPetItem(itemId)) {
			if (displayMessage)
				player.sendMessage("You can't trade your pet.");
			return false;
		}

		if (CompletionistCape.isCompletionistCape(itemId)) {
			if (displayMessage)
				player.sendMessage("You can't trade this item.");
			return false;
		}
		return true;
	}

}
