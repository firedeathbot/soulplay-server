package com.soulplay.content.minigames.soulwars;

import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Client;

public final class Overlay {

	public static void sendWaitingScreen(Client c) {
		c.getPA().walkableInterface(23773);

		c.getPacketSender().sendString("Players In Game", 23774);
		c.getPacketSender().sendString("<col=ff>Blue: " + SoulWars.getBluePlayersWaiting(), 23775);
		c.getPacketSender().sendString("<col=ff0000>Red: " + SoulWars.getRedPlayersWaiting(), 23776);
		c.getPacketSender().sendString((SoulWars.waitTimer * 1200 / 1000) > 0 ? "Next Game: " + ((SoulWars.waitTimer * 1200 / 1000) / 60 + " min")
						: "Next Game: " + ((SoulWars.gameTimer * 1200 / 1000) / 60 + SoulWars.GAME_START_TIMER * 1200 / 1000 / 60) + " mins", 23777);
		c.getPacketSender().sendString("", 23778);
		c.getPacketSender().sendString("", 23779);

	}

	public static void soulWarsInterface(Client c) {
		if (c.soulWarsTeam == Team.NONE)
			return;
		c.getPacketSender().setConfig(8000, SoulWars.obelisk.team.getTeamId());
		c.getPacketSender().setConfig(8001, SoulWars.blueGraveyard.team.getTeamId());
		c.getPacketSender().setConfig(8002, SoulWars.redGraveyard.team.getTeamId());
		
		int mask = 255;
		if (c.capturableProgress != -1) {
			mask = c.capturableProgress;
		}

		c.getPacketSender().setConfig(8003, mask);
		c.getPacketSender().sendConfig(8004, 1000 - c.getSoulWarsInactivity());
		
		c.getPA().walkableInterface(29266);
		
		block: if (SoulWars.gameTimer < 900) {
			
			if (c.getTransformId() != -1 || RSConstants.inSoulWarsObeliskArea(c.absX, c.absY)) {
				break block;
			}
			
			if (c.underAttackBy > 0 || c.underAttackBy2 > 0 || !c.isResetCurseBoost()) {
				c.decreaseInactivity(50);
				break block;
			}
			
			if ((SoulWars.getTeam(c) == Team.BLUE && RSConstants.inSoulWarsRedBoss(c.absX, c.absY))
					|| (SoulWars.getTeam(c) == Team.RED && RSConstants.inSoulWarsBlueBoss(c.absX, c.absY))) {
				c.decreaseInactivity(50);
				break block;
			}
			
			
		    c.increaseInactivity(3);
		}
		//c.sendMess("gametimer:" + SoulWars.gameTimer);
		
		c.getPacketSender().sendFrame126("" + SoulWars.gameTimer * 1200 / 1000 / 60 + " mins", 29287);
		c.getPacketSender().sendFrame126("" + SoulWars.blueAvatar.deaths, 29278);
		c.getPacketSender().sendFrame126("" + SoulWars.redAvatar.deaths, 29279);
		c.getPacketSender().sendFrame126("" + SoulWars.blueAvatar.level, 29281);
		c.getPacketSender().sendFrame126("" + SoulWars.redAvatar.level, 29282);
		c.getPacketSender().sendFrame126("" + SoulWars.blueAvatar.health / 100 + "%", 29284);
		c.getPacketSender().sendFrame126("" + SoulWars.redAvatar.health / 100 + "%", 29285);
	}
	
	public static void soulWarsRewardInterface(Client c) {
		c.getPacketSender().showInterface(29293);
		
		c.getPacketSender().sendFrame126("Zeals: " + c.getZeals(), 29333);
		
		
	}
}
