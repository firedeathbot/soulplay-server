package com.soulplay.content.minigames.soulwars;

import com.soulplay.game.model.player.Client;

public final class Bandages {
	
	public static void useSWBandages(Client c) {
		if (!c.inSoulWars()) {
			return;
		}
		if (c.getItems().playerHasItem(4049)) {
			if (System.currentTimeMillis() - c.anyDelay2 >= 500
					&& c.getPlayerLevel()[3] > 0) {
				int healAmount = (int) Math
						.round(c.getSkills().getMaximumLifepoints()
								* 20.0 / 100);
				c.getCombat().resetPlayerAttack();
				// c.attackTimer += 2;
				c.startAnimation(829);
				c.getItems().deleteItemInOneSlot(4049, 1);
				c.healHitPoints(healAmount);
				c.anyDelay2 = System.currentTimeMillis();
				c.sendMessage("You heal using a bandage.");
				c.getMovement().restoreRunEnergy(30);
				c.getDotManager().reset();
			}
		}
	}

}
