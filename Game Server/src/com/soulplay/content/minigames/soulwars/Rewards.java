package com.soulplay.content.minigames.soulwars;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class Rewards {

	public static final int ATTACK_EXP = 114111;
	public static final int STRENGTH_EXP = 114114;
	public static final int DEFENCE_EXP = 114117;
	public static final int HITPOINT_EXP = 114120;
	public static final int RANGE_EXP = 114123;
	public static final int MAGIC_EXP = 114126;
	public static final int PRAYER_EXP = 114129;
	public static final int SLAYER_EXP = 114132;

	public static final int GOLD_CHARM_BUTTON = 114155;
	public static final int GREEN_CHARM_BUTTON = 114158;
	public static final int CRIMSON_CHARM_BUTTON = 114161;
	public static final int BLUE_CHARM_BUTTON = 114164;

	public static final int GOLD_CHARM_ID = 12158;
	public static final int GREEN_CHARM_ID = 12159;
	public static final int CRIMSON_CHARM_ID = 12160;
	public static final int BLUE_CHARM_ID = 12163;

	public static final int CRAWLING_HAND_BUTTON = 114179;
	public static final int MINITRICE_BUTTON = 114182;
	public static final int BABY_BASILISK_BUTTON = 114185;
	public static final int BABY_KURASK_BUTTON = 114188;
	public static final int ABYSSAL_MINION_BUTTON = 114191;
	public static final int TZREK_JAD_BUTTON = 114194;

	public static final int CRAWLING_HAND_ITEM = 7982;
	public static final int COCKATRICE_HEAD = 7976;
	public static final int BASILISK_HEAD = 7984;
	public static final int KURASK_HEAD = 7985;
	public static final int ABYSSAL_HEAD = 7979;
	public static final int TOKHAAR_KAL = 23639;

	public static final int CRAWLING_HAND_PET = 14652;
	public static final int MINITRICE_PET = 14653;
	public static final int BABY_BASILISK_PET = 14654;
	public static final int BABY_KURASK_PET = 14655;
	public static final int ABYSSAL_MINION_PET = 14651;
	public static final int TZREK_JAD_PET = 21512;

	public static final int LUCK_BUTTON = 114197;

	private static final int[][] EASY_REWARDS = { 
			{ 995, 4000000 + Misc.random(10000000) }, // coins	
			{ 995, 4000000 + Misc.random(10000000) }, // coins
			{ 995, 4000000 + Misc.random(10000000) }, // coins
			{ 995, 4000000 + Misc.random(10000000) }, // coins
			{ 995, 4000000 + Misc.random(10000000) }, // coins//																					// coins
			{ 212, 140 + Misc.random(60) }, // avantoe herb
			{ 220, 140 + Misc.random(60) }, // torstol herb
			{ 2486, 140 + Misc.random(60) }, // lantadyme herb
			{ 218, 140 + Misc.random(60) }, // dwarf weed herb
			{ 18831, 35 + Misc.random(20) }, // 5 - frost dragon bones
			{ 4835, 100 + Misc.random(50) }, // 6 - ourg bones
			{ 537,  200 + Misc.random(70) }, // 7 - dragon bones
			{ 3145, 100 + Misc.random(50) }, // 8 - karambwan
			{ 15273, 140 + Misc.random(60) }, // 9 - rocktails
			{ 5316, 2 + Misc.random(3) }, // 10 - magic seeds
			{ 12539, 30 + Misc.random(20) }, // 11 - grenwall spikes
			{ 6572, 1 + Misc.random(2) }, // 13 - uncut onyx
			{ 1632, 12 + Misc.random(8) }, // 14 - uncut dragonstone
			{ 299, 15 + Misc.random(15) }, // 15 - mithril seeds
			{ 452, 60 + Misc.random(40) }, // 16 - rune ores
			{ 450, 140 + Misc.random(60) }, // 17 - addy ores
			{ 9244, 200 + Misc.random(50) }, // 18 - dragon bolts e
			{ 2498, 35 + Misc.random(15) }, // 19 - black d hide chaps
			{ 2504, 35 + Misc.random(15) }, // 20 - black d hide tops
			{ 2492, 35 + Misc.random(15) }, // 21 - black d hide vambs
			{ 21631, 15 + Misc.random(12) }, // 21 - prayer renewals
			{ 2, 1600 + Misc.random(1000) }, // 22 - cannonballs
			{ 9245, 140 + Misc.random(65) }, // 23 - onyx bolts e
			{ 2354, 200 + Misc.random(50) }, // 24 - steel bars
			{ 21773, 290 + Misc.random(65) }, // 25 - armadyl runes
			{ 1514, 1000 + Misc.random(700) }, // 26 - magic logs
	};
	private static final int[][] HARD_REWARDS = { 
			{ 15433, 1 }, // 27 - blue cape
			{ 15432, 1 }, // 28 - red cape
			{ 15220, 1 }, // 29 - berserker ring i
			{ 15018, 1 }, // 30 - seers ring i
			{ 15019, 1 }, // 31 - archers ring i
			{ 15020, 1 }, // 32 - warrior ring i
			{ 12604, 1 }, // 33 - tyrannical ring i
			{ 12606, 1 }, // 34 - treasonous ring i
			{ 23639, 1 }, // 35 - tokhaar kal
			{ 11924, 1 }, // 37 - malediction ward
			{ 11926, 1 }, // 38 - odium ward
			{ 14484, 1 }, // 39 - dragon claws
			{ 30087, 1 }, // 40 - dragon hunter crossbow
			{ 11702, 1 }, // 41 - armadyl hilt
			{ 11708, 1 }, // 42 - zamorak hilt
			{ 11704, 1 }, // 43 - bandos hilt
			{ 11706, 1 }, // 44 - saradomin hilt
			{ 13748, 1 }, // 45 - divine sigil
			{ 13750, 1 }, // 46 - elysian sigil
			{ 13752, 1 }, // 47 - spectral sigil
			{ 13746, 1 }, // 48 - arcane sigil
	};
	
	private static final int[][] EXTREME_REWARDS = { 
			{ 19335, 1 }, // 36 - amulet of fury o
			{ 30060, 1 }, // 49 - 3rd cloak
			{ 30061, 1 }, // 50 - 3rd age bow
			{ 30062, 1 }, // 51 - 3rd age longsword
			{ 30063, 1 }, // 52 - 3rd age wand
	};

	public static int getLengthEasy() {
		return EASY_REWARDS.length;
	}

	public static int getLengthHard() {
		return HARD_REWARDS.length;
	}
	
	public static int getLengthExtreme() {
		return EXTREME_REWARDS.length;
	}

	public static boolean soulWarRewards(final Client c, int actionButton) {
		switch (actionButton) {
		case ATTACK_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int attLevel = c.getSkills().getStaticLevel(Skills.ATTACK);
					int totalAttExp = (int)Math.floor(attLevel * attLevel / 200.0 * 525.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalAttExp, Skills.ATTACK);
					c.sendMessage("You received " + totalAttExp + " attack experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase attak experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case STRENGTH_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int strLevel = c.getSkills().getStaticLevel(Skills.STRENGTH);
					int totalStrExp = (int)Math.floor(strLevel * strLevel / 200.0 * 525.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalStrExp, Skills.STRENGTH);
					c.sendMessage("You received " + totalStrExp + " strength experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase strength experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case DEFENCE_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int defLevel = c.getSkills().getStaticLevel(Skills.DEFENSE);
					int totalDefExp = (int)Math.floor(defLevel * defLevel / 200.0 * 525.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalDefExp, Skills.DEFENSE);
					c.sendMessage("You received " + totalDefExp + " defence experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase defence experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case HITPOINT_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int hpLevel = c.getSkills().getStaticLevel(Skills.HITPOINTS);
					int totalHpExp = (int)Math.floor(hpLevel * hpLevel / 200.0 * 525.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalHpExp, Skills.HITPOINTS);
					c.sendMessage("You received " + totalHpExp + " hitpoints experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase hitpoints experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case RANGE_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int rangeLevel = c.getSkills().getStaticLevel(Skills.RANGED);
					int totalRangeExp = (int)Math.floor(rangeLevel * rangeLevel / 200.0 * 525.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalRangeExp, Skills.RANGED);
					c.sendMessage("You received " + totalRangeExp + " range experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase range experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case MAGIC_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int magicLevel = c.getSkills().getStaticLevel(Skills.MAGIC);
					int totalMagicExp = (int)Math.floor(magicLevel * magicLevel / 200.0 * 525.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalMagicExp, Skills.MAGIC);
					c.sendMessage("You received " + totalMagicExp + " magic experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase magic experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case PRAYER_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int prayerLevel = c.getSkills().getStaticLevel(Skills.PRAYER);
					int totalPrayerExp = (int)Math.floor(prayerLevel * prayerLevel / 600.0 * 270.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalPrayerExp, Skills.PRAYER);
					c.sendMessage("You received " + totalPrayerExp + " prayer experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase prayer experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case SLAYER_EXP:
			if (c.getDoubleExpLength() < 1) {
				if (c.getZeals() >= 1) {
					int slayerLevel = c.getSkills().getStaticLevel(Skills.SLAYER);
					int totalSlayerExp = (int)Math.floor(slayerLevel * slayerLevel / 200.0 * 150.0);
					c.setZeals(c.getZeals() - 1);
					c.getPA().addSkillXP(totalSlayerExp, Skills.SLAYER);
					c.sendMessage("You received " + totalSlayerExp + " slayer experience");
				} else
					c.sendMessage("You need at least 1 zeals to purchase slayer experience.");
			} else
				c.sendMessage("You cannot purchase this while double exp is on!");
			return true;
		case GOLD_CHARM_BUTTON:
			if (c.getItems().freeSlots() > 0) {
				if (c.getZeals() >= 1) {
					c.setZeals(c.getZeals() - 1);
					c.getItems().addItem(GOLD_CHARM_ID, 40);
				} else
					c.sendMessage("You need at least 1 zeals to purchase gold charms.");
			} else
				c.sendMessage("You do not have enough space in your inventory to buy gold charms.");
			return true;
		case GREEN_CHARM_BUTTON:
			if (c.getItems().freeSlots() > 0) {
				if (c.getZeals() >= 1) {
					c.setZeals(c.getZeals() - 1);
					c.getItems().addItem(GREEN_CHARM_ID, 25);
				} else
					c.sendMessage("You need at least 1 zeals to purchase green charms.");
			} else
				c.sendMessage("You do not have enough space in your inventory to buy green charms.");
			return true;
		case CRIMSON_CHARM_BUTTON:
			if (c.getItems().freeSlots() > 0) {
				if (c.getZeals() >= 1) {
					c.setZeals(c.getZeals() - 1);
					c.getItems().addItem(CRIMSON_CHARM_ID, 14);
				} else
					c.sendMessage("You need at least 1 zeals to purchase crimson charms.");
			} else
				c.sendMessage("You do not have enough space in your inventory to buy crimson charms.");
			return true;
		case BLUE_CHARM_BUTTON:
			if (c.getItems().freeSlots() > 0) {
				if (c.getZeals() >= 1) {
					c.setZeals(c.getZeals() - 1);
					c.getItems().addItem(BLUE_CHARM_ID, 7);
				} else
					c.sendMessage("You need at least 1 zeals to purchase blue charms.");
			} else
				c.sendMessage("You do not have enough space in your inventory to buy blue charms.");
			return true;
		case CRAWLING_HAND_BUTTON:
			if (c.getItems().playerHasItem(CRAWLING_HAND_ITEM)) {
				if (c.getZeals() >= 5) {
					c.setZeals(c.getZeals() - 5);
					c.getItems().deleteItemInOneSlot(CRAWLING_HAND_ITEM, 1);
					c.getItems().addItem(CRAWLING_HAND_PET, 1);
					c.sendMessage("You have received a crawling hand pet.");
				} else
					c.sendMessage("You need at least 5 zeals to buy the crawling hand pet.");
			} else
				c.sendMessage("You need a crawling hand to buy crawling hand pet.");
			return true;
		case MINITRICE_BUTTON:
			if (c.getItems().playerHasItem(COCKATRICE_HEAD)) {
				if (c.getZeals() >= 25) {
					c.setZeals(c.getZeals() - 25);
					c.getItems().deleteItemInOneSlot(COCKATRICE_HEAD, 1);
					c.getItems().addItem(MINITRICE_PET, 1);
					c.sendMessage("You have received a minitrice.");
				} else
					c.sendMessage("You need at least 25 zeals to buy the minitrice.");
			} else
				c.sendMessage("You need a cockatrice head to buy minitrice.");
			return true;
		case BABY_BASILISK_BUTTON:
			if (c.getItems().playerHasItem(BASILISK_HEAD)) {
				if (c.getZeals() >= 40) {
					c.setZeals(c.getZeals() - 40);
					c.getItems().deleteItemInOneSlot(BASILISK_HEAD, 1);
					c.getItems().addItem(BABY_BASILISK_PET, 1);
					c.sendMessage("You have received a baby basilisk.");
				} else
					c.sendMessage("You need at least 40 zeals to buy the baby basilisk.");
			} else
				c.sendMessage("You need a basilisk head to buy baby basilisk.");
			return true;
		case BABY_KURASK_BUTTON:
			if (c.getItems().playerHasItem(KURASK_HEAD)) {
				if (c.getZeals() >= 70) {
					c.setZeals(c.getZeals() - 70);
					c.getItems().deleteItemInOneSlot(KURASK_HEAD, 1);
					c.getItems().addItem(BABY_KURASK_PET, 1);
					c.sendMessage("You have received a baby kurask.");
				} else
					c.sendMessage("You need at least 70 zeals to buy the baby kurask.");
			} else
				c.sendMessage("You need a kurask head to buy baby kurask.");
			return true;
		case ABYSSAL_MINION_BUTTON:
			if (c.getItems().playerHasItem(ABYSSAL_HEAD)) {
				if (c.getZeals() >= 85) {
					c.setZeals(c.getZeals() - 85);
					c.getItems().deleteItemInOneSlot(ABYSSAL_HEAD, 1);
					c.getItems().addItem(ABYSSAL_MINION_PET, 1);
					c.sendMessage("You have received a abyssal minion.");
				} else
					c.sendMessage("You need at least 85 zeals to buy the abyssal minion.");
			} else
				c.sendMessage("You need a abyssal head to buy abyssal minion.");
			return true;
		case TZREK_JAD_BUTTON:
			if (c.getItems().playerHasItem(TOKHAAR_KAL)) {
				if (c.getZeals() >= 100) {
					c.setZeals(c.getZeals() - 100);
					c.getItems().deleteItemInOneSlot(TOKHAAR_KAL, 1);
					c.getItems().addItem(TZREK_JAD_PET, 1);
					c.sendMessage("You have received a TzRek Jad.");
				} else
					c.sendMessage("You need at least 100 zeals to buy the TzRek Jad.");
			} else
				c.sendMessage("You need a tokhaar-kal cape to buy TzRek Jad.");
			return true;
		case LUCK_BUTTON:
			if (c.getItems().freeSlots() > 0) {
				if (c.getZeals() >= 3) {
					c.setZeals(c.getZeals() - 3);
					if (Misc.random(100) == 0) {
						int[] hardReward = HARD_REWARDS[Misc.random(HARD_REWARDS.length - 1)];
						c.getItems().addItem(hardReward[0], hardReward[1]);
						c.sendMessage("You have received a good reward from soulwars!");
						PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " has received " + ItemConstants.getItemName(hardReward[0]) + " from soulwars!");
					} else if (Misc.random(1000) == 1) {
						int[] extremeReward = EXTREME_REWARDS[Misc.random(EXTREME_REWARDS.length - 1)];
						c.getItems().addItem(extremeReward[0], extremeReward[1]);
						c.sendMessage("You have received a ultimate reward from soulwars!");
						PlayerHandler.messageAllPlayers("<img=9><col=4848b7> " + c.getNameSmartUp() + " has received " + ItemConstants.getItemName(extremeReward[0]) + " from soulwars!");
					} else {
						int[] easyReward = EASY_REWARDS[Misc.random(EASY_REWARDS.length - 1)];
						c.getItems().addItem(easyReward[0], easyReward[1]);
						c.sendMessage("You received a normal reward from soulwars, better luck next time!");
					}
				} else
					c.sendMessage("You need at least 3 zeals to try your luck!.");
			} else
				c.sendMessage("You do not have enough space in your inventory to try your luck!.");
			return true;
		}
		return false;
	}

}
