package com.soulplay.content.minigames.soulwars;

import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class SWObjectHandler {

	private static final int RED_BARRIER = 42018;
	
	private static final int LOBBY_RED_BARRIER = 42030;
	
	private static final int BLUE_BARRIER = 42015;
	
	private static final int LOBBY_BLUE_BARRIER = 42029;
	
	private static final int GREEN_PORTAL = 42031;
	
	private static final int BANDAGE_ZAMMY = 42024;
	private static final int BANDAGE_SARA = 42023;
	
	private static final int BARRICADE_ZAMMY = 42026;
	private static final int BARRICADE_SARA = 42025;
	
	private static final int EXPLOSIVE_ZAMMY = 42028;
	private static final int EXPLOSIVE_SARA = 42027;
	
	private static final int BLUE_LEAVE = 42021;
	
	private static final int RED_LEAVE = 42022;
	
	private static final int RED_GRAVEYARD_BARRIER = 42019;
	private static final int BLUE_GRAVEYARD_BARRIER = 42020;
	
	public static boolean handleSWFirstClickObject(Client c, int objectId) {
		
		if (c.inSoulWars() && !c.getStopWatch().complete()) {
			c.sendMessage("You must wait " + Misc.ticksToSeconds(c.getStopWatch().getRemainingTicks()) + " seconds to re-enter the battle.");
			return true;
		}
		
		switch (objectId) {
			
		case BANDAGE_ZAMMY:
			if (SoulWars.getTeam(c) == Team.BLUE) {
				c.sendMessage("You are not allowed to take anything from this table.");
				return true;
			}
			c.startAnimation(832);
			c.getItems().addItem(4049, 1);
			c.sendMessage("You get some bandages");
			return true;
			
		case BANDAGE_SARA:
			if (SoulWars.getTeam(c) == Team.RED) {
				c.sendMessage("You are not allowed to take anything from this table.");
				return true;
			}
			c.startAnimation(832);
			c.getItems().addItem(4049, 1);
			c.sendMessage("You get some bandages");
			return true;
			
		case BARRICADE_ZAMMY:
			if (SoulWars.getTeam(c) == Team.BLUE) {
				c.sendMessage("You are not allowed to take anything from this table.");
				return true;
			}
			c.startAnimation(832);
			c.sendMessage("You take a barricade");
			c.getItems().addItem(4053, 1);
			return true;
			
		case BARRICADE_SARA:
			if (SoulWars.getTeam(c) == Team.RED) {
				c.sendMessage("You are not allowed to take anything from this table.");
				return true;
			}
			c.startAnimation(832);
			c.sendMessage("You take a barricade");
			c.getItems().addItem(4053, 1);
			return true;
			
		case EXPLOSIVE_ZAMMY:
			if (SoulWars.getTeam(c) == Team.BLUE) {
				c.sendMessage("You are not allowed to take anything from this table.");
				return true;
			}
			c.startAnimation(832);
			c.sendMessage("You take a explosive potion");
			c.getItems().addItem(4045, 1);
			return true;
			
		case EXPLOSIVE_SARA:
			if (SoulWars.getTeam(c) == Team.RED) {
				c.sendMessage("You are not allowed to take anything from this table.");
				return true;
			}
			c.startAnimation(832);
			c.sendMessage("You take a explosive potion");
			c.getItems().addItem(4045, 1);
			return true;
			
		case RED_BARRIER:
			if (SoulWars.getTeam(c) == Team.BLUE) {
				c.sendMessage("You are not allowed in the other teams spawn point.");
				return true;
			}
			if (c.absX == 1958 && c.absY == 3239) {
				c.getPA().walkTo(1, 0, true);
			} else if (c.absX == 1959 && c.absY == 3239) {
				c.getPA().walkTo(-1, 0, true);
			}
			return true;
			
		case BLUE_BARRIER:
			if (SoulWars.getTeam(c) == Team.RED) {
				c.sendMessage("You are not allowed in the other teams spawn point.");
				return true;
			}
			if (c.absX == 1816 && c.absY == 3225) {
				c.getPA().walkTo(-1, 0, true);
			} else if (c.absX == 1815 && c.absY == 3225) {
				c.getPA().walkTo(1, 0, true);
			}
			return true;
			
		case BLUE_LEAVE:
		case RED_LEAVE:
			SoulWars.removePlayerFromGame(c);
		   return true;
		   
		case LOBBY_BLUE_BARRIER:
			SoulWars.addToWaitRoom(c, SoulWars.Team.BLUE);
			return true;

		case LOBBY_RED_BARRIER:
			SoulWars.addToWaitRoom(c, SoulWars.Team.RED);
			return true;

		case GREEN_PORTAL:
			SoulWars.addToWaitRoom(c, SoulWars.Team.RANDOM);
			return true;
		case BLUE_GRAVEYARD_BARRIER:
			if (c.absX == 1842 && c.absY == 3219) {
				c.getPA().walkTo(0, 1, true);
			}
			return true;
		
		case RED_GRAVEYARD_BARRIER:
			if (c.absX == 1933 && c.absY == 3244) {
				c.getPA().walkTo(0, -1, true);
			}
			return true;
		}
		return false;
	}
	
	public static boolean handleSWSecondClickObject(Client c, int objectId) {
		switch (objectId) {
		case BANDAGE_SARA:
		case BANDAGE_ZAMMY:
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 929292;
			break;
			
		case EXPLOSIVE_SARA:
		case EXPLOSIVE_ZAMMY:
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 939393;
			break;
		}
		
		return false;
	}
	
}
