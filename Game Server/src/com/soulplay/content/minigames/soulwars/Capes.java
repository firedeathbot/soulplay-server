package com.soulplay.content.minigames.soulwars;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.UpdateFlag;

public final class Capes {
	
	public static final int BLUE_CAPE = 14642;

	public static final int RED_CAPE = 14641;

	public static void addCapes(Client player, SoulWars.Team team) {
		int capeId = -1;
		switch (team) {
			
			case BLUE:
				capeId = BLUE_CAPE;
				break;
			case RED:
				capeId = RED_CAPE;
				break;
				
				default:
					break;
		}
		player.displayEquipment[PlayerConstants.playerCape] = capeId;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	
	public static void removeCapes(Client player) {
		player.displayEquipment[PlayerConstants.playerCape] = -1;
		player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
	}
	
}
