package com.soulplay.content.minigames.soulwars;

import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.game.model.player.Client;

public class SoulObelisk {
	
	private static final int SOUL_FRAGMENT = 14639; 

	public static boolean handle(Client player, int objectID, int itemID) {
		if (objectID == 42010 && itemID == SOUL_FRAGMENT) {
			if (SoulWars.obelisk.team == Team.NONE) {
				player.sendMessage("Obelisk is uncaptured.");
			} else if (SoulWars.obelisk.team == Team.BLUE && player.soulWarsTeam == Team.BLUE) {
				decreaseBossLevel(Team.RED, player);
				player.decreaseInactivity(500);
			} else if (SoulWars.obelisk.team == Team.RED && player.soulWarsTeam == Team.RED) {
				decreaseBossLevel(Team.BLUE, player);
				player.decreaseInactivity(500);
			} else {
				player.sendMessage("Your team doesn't have obelisk captured.");
			}

			return true;
		}
		
		return false;
	}
	
	public static void decreaseBossLevel(Team team, Client player) {
		Avatar avatar = null;
		
		if (team == Team.RED) {
			avatar = SoulWars.redAvatar;
		} else if (team == Team.BLUE) {
			avatar = SoulWars.blueAvatar;
		}
		
		if (avatar == null) {
			return;
		}
		
		int count = player.getItems().getItemCount(SOUL_FRAGMENT);
		int level = avatar.level;
		int deduct = count;
		
		if (level - count < 1) {
			deduct -= Math.abs(level - count - 1);
		}

		avatar.decreaseLevel(deduct);
		player.getItems().deleteItem2(SOUL_FRAGMENT, deduct);
		player.decreaseInactivity(300);
		player.sendMessage("wallah?");
		//player.getPacketSender().sendFrame126("" + SoulWars.blueAvatar.level, 29281);
		//player.getPacketSender().sendFrame126("" + SoulWars.redAvatar.level, 29282);
		//System.out.println(team+" - " + avatar.level);
	}

}
