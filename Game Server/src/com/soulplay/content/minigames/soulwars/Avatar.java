package com.soulplay.content.minigames.soulwars;

import com.soulplay.content.minigames.soulwars.SoulWars.Team;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;

public class Avatar {
	public int deaths;
	public int level;
	public int health;
	public int respawn;
	
	public void reset() {
		level = 125;
		deaths = 0;
		health = 10_000;
		respawn = -1;
	}
	
	public Avatar() {
		reset();
	}
	
	public void decreaseLevel(int level) {
		this.level -= level;
		if (level < 1) {
			level = 1;
		}
	}
	
	public void increaseLevel() {
		if (level < 125) {
			level++;
		}
	}
	
	public static boolean canAttackAvatar(Client c, NPC n, boolean resetAttackOnFalse) {
		if (n.npcType == 8596) {
			if (c.soulWarsTeam == Team.RED) {
				c.sendMessage("You can't atack your own team avatar.");
				return false;
			} else if (c.getPlayerLevel()[Skills.SLAYER] < SoulWars.redAvatar.level) {
				c.sendMessage(
						"You need a Slayer Level of at least " + SoulWars.redAvatar.level + " to fight this.");
				if (resetAttackOnFalse) {
					c.getCombat().resetPlayerAttack();
				}
				return false;
			} else if (!RSConstants.inSoulWarsRedBoss(c.absX, c.absY)) {
				return false;
			}
		} else if (n.npcType == 8597) {
			if (c.soulWarsTeam == Team.BLUE) {
				c.sendMessage("You can't atack your own team avatar.");
				return false;
			} else if (c.getPlayerLevel()[Skills.SLAYER] < SoulWars.blueAvatar.level) {
				c.sendMessage(
						"You need a Slayer Level of at least " + SoulWars.blueAvatar.level + " to fight this.");
				if (resetAttackOnFalse) {
					c.getCombat().resetPlayerAttack();
				}
				return false;
			} else if (!RSConstants.inSoulWarsBlueBoss(c.absX, c.absY)) {
				return false;
			}
		}
		return true;
	}
}
