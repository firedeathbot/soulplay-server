package com.soulplay.content.minigames.soulwars;

import com.soulplay.content.minigames.soulwars.SoulWars.Team;

public class Capturable {
	
	public Team team;
	public int state;
	public int absX;
	public int absY;
	
	public Capturable(int absX, int absY) {
		this.absX = absX;
		this.absY = absY;
	}
	
	public Capturable() {
		reset();
	}
	
	public int distance(int playerX, int playerY) {
		return (int) Math.sqrt(Math.pow((playerX - absX), 2) + (Math.pow((playerY - absY), 2)));
	}
	
	public void reset() {
		team = Team.NONE;
		state = 50;
	}
	
	public void increase() {
		if (state < 100) {
			state += 5;
			team = Team.NONE;
		} else {
			state = 100;
			team = Team.RED;
		}
	}
	
	//0 is blue 100 is red
	
	public void decrease() {
		if (state > 0) {
			state -= 5;
			team = Team.NONE;
		} else {
			state = 0;
			team = Team.BLUE;
		}
	}
	
	public void normalize() {
		if (state > 50) {
			state -= 5;
		} else if (state < 50) {
			state += 5;
		}
		team = Team.NONE;
	}

}
