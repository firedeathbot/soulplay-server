package com.soulplay.content.minigames.soulwars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.items.Food;
import com.soulplay.content.minigames.MinigameRules;
import com.soulplay.content.npcs.impl.Barricades;
import com.soulplay.content.player.dailytasks.DailyTaskRequirement;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class SoulWars {

	public static final int GAME_TIMER = 2050; // 1500 * 600 = 900000ms = 15
												// minutes

	public static final int REJOIN_TIMER = 100;
	
	public static /*final*/ int MINIMUM_PLAYERS = 4;

	public static int gameTimer = -1;

	public static final int GAME_START_TIMER = 250/2; //divide by 2 for now, till we fix the soulwars task running every 1200 ms

	public static int waitTimer = GAME_START_TIMER;
	
	private static int discordStartTime, discordEndTime;

	public static boolean gameStarted = false;

	public static final List<Client> waitingRed = new ArrayList<>();
	public static final List<Client> waitingBlue = new ArrayList<>();
	public static final List<Client> soulWarsPlayers = new ArrayList<>();
	
	private static HashMap<String, Team> loyalToTeam = new HashMap<>();

	private static final Location BLUE_WAIT_ROOM = Location.create(1879, 3162);

	private static final Location RED_WAIT_ROOM = Location.create(1900, 3162);

	private static final Location BLUE_GAME_ROOM = Location.create(1819, 3225);

	private static final Location RED_GAME_ROOM = Location.create(1955, 3239);
	public static final int RED_CAPE_ID = 14641;
	public static final int BLUE_CAPE_ID = 14642;
	
	public static final int SOUL_FRAGMENT = 14639;
	
	public static final int BONE = 526;
	
	public static Capturable redGraveyard = new Capturable(1933, 3245);
	public static Capturable obelisk = new Capturable();
	public static Capturable blueGraveyard  = new Capturable(1842, 3218);
	public static Avatar redAvatar = new Avatar();
	public static Avatar blueAvatar = new Avatar();
	
	public static boolean EXTRA_REWARD = false;
	
	public static boolean greenPortalOnly = false;
	
	public static final int GRAVEYARD_WAIT_TICKS = 25; // 25 ticks = 15 seconds.

	private static final int[] prohibitedItems = { 6, 995, 6583, 7927, BONE, SOUL_FRAGMENT, 30193, 30192 };
	
	private static int RED_TEAM_GEAR_BONUSES = 0;
	private static int BLUE_TEAM_GEAR_BONUSES = 0;
	
	public static boolean checkGearBonus = false;

	public static enum Team {
		BLUE(1),

		RED(2),

		RANDOM(3),

		NONE(0);
		
		private final int teamId;
		
		Team(int teamId) {
			this.teamId = teamId;
		}
		
		public int getTeamId() {
			return teamId;
		}
	}

	public static void addToWaitRoom(Client player, Team team) {
		if (!MinigameRules.canJoinMinigame(player))
			return;
		Team existingTeam = Team.NONE;
		if (waitingRed.contains(player)) {
			existingTeam = Team.RED;
		} else if (waitingBlue.contains(player)) {
			existingTeam = Team.BLUE;
		}

		if (existingTeam != Team.NONE) {
			leaveWaitingRoom(player, existingTeam);
			return;
		}
		
		if (SoulWars.greenPortalOnly && team != Team.RANDOM) { // green portal lock
			player.sendMessage("Sorry but you can only use the green portal at the moment.");
			return;
		}

		boolean sameUUID = false;
		if (gameStarted) {
			if (!soulWarsPlayers.isEmpty()) {
				for (Client teamPlayer : soulWarsPlayers) {
					if (teamPlayer == null || !teamPlayer.isActive || teamPlayer.disconnected) {
						return;
					}

					if (teamPlayer.UUID.equals(player.UUID)) {
						sameUUID = true;
						break;
					}
				}
			}
		}

		// We already know he has an alt don't need to check again
		if (!sameUUID) {
			for (Player p : Server.getRegionManager().getRegionByLocation(BLUE_WAIT_ROOM).getPlayers()) { // blue
																											// room
				if (p == null || p.disconnected || p.forceDisconnect) {
					continue;
				}
				if (!RSConstants.inSoulWarsBlueRoom(p.getX(), p.getY())) {
					continue;
				}
				if (p.UUID.equals(player.UUID)) {
					sameUUID = true;
					break;
				}
			}
		}

		// Same goes here
		if (!sameUUID) {
			for (Player p : Server.getRegionManager().getRegionByLocation(RED_WAIT_ROOM).getPlayers()) { // red
																											// room
				if (p == null || p.disconnected || p.forceDisconnect) {
					continue;
				}
				if (!RSConstants.inSoulWarsRedRoom(p.getX(), p.getY())) {
					continue;
				}
				if (p.UUID.equals(player.UUID)) {
					sameUUID = true;
					break;
				}
			}
		}

		if (sameUUID && !player.isOwner() && !Config.SERVER_DEBUG) {
			player.sendMessage("Sorry, but you can't enter the game with multiple accounts.");
			return;
		}

		for (int itemId : player.playerItems) {
			itemId--;
			if (itemId > 0) {
				for (int prohItem : prohibitedItems) {
					if (itemId == prohItem) {
						player.sendMessage("You can't bring " + ItemConstants.getItemName(itemId) + " in here.");
						return;
					}
				}
				if (Food.isFood(itemId)) {
					player.sendMessage("You can't bring any food in here.");
					return;
				}
			}
		}
		
		enterWaitingRoom(player, team, Team.NONE);
	}

	/*
	 * public static void addToRedGame(final Client c) { waitingRooms.remove(c);
	 * c.inSoulGame = true; moveToRedSpawn(c); // TODO: ADD ingame interface
	 * soulWarsPlayers.put(c, Team.RED); c.soulWarsTeam = 2; }
	 * 
	 * public static void addToBlueGame(final Client c) {
	 * waitingRooms.remove(c); c.inSoulGame = true; moveToBlueSpawn(c); // TODO:
	 * ADD ingame interface soulWarsPlayers.put(c, Team.BLUE); c.soulWarsTeam =
	 * 1; }
	 */

	public static void leaveWaitingRoom(Client player, Team team) {
		if (player == null || !player.isActive) {
			return;
		}

		// Game about to start
		if (SoulWars.waitTimer >= 0 && SoulWars.waitTimer <= 1) {
			return;
		}

		boolean removed = false;
		if (team == Team.RED) {
			removed = waitingRed.remove(player);
			if (removed)
				RED_TEAM_GEAR_BONUSES -= player.getItems().getTotalBonuses();
		} else if (team == Team.BLUE) {
			removed = waitingBlue.remove(player);
			if (removed)
				BLUE_TEAM_GEAR_BONUSES -= player.getItems().getTotalBonuses();
		}
		
		//Shouldn' happen but just incase
		if (removed == false) {
			return;
		}
		
		Capes.removeCapes(player);
		player.getPacketSender().createPlayerHints(10, -1);
		player.sendMessage("You left your team!");
		if (RSConstants.inSoulWarsBlueRoom(player.getX(), player.getY())) {
			player.getPA().walkTo(1, 0, true);
		} else {
			player.getPA().walkTo(-1, 0, true);
		}
	}
	
	public static void removePlayerWaitingRoom(Client player) {
		boolean removed = waitingRed.remove(player);
		if (!removed) {
			removed = waitingBlue.remove(player);
			BLUE_TEAM_GEAR_BONUSES -= player.getItems().getTotalBonuses();
		} else {
			RED_TEAM_GEAR_BONUSES -= player.getItems().getTotalBonuses();
		}
		
		Capes.removeCapes(player);
		
		if (!removed) {
			return;
		}

		player.getPacketSender().createPlayerHints(10, -1);
		player.getPA().movePlayer(1890, 3164, 0);
	}

	public static void leaveGame(Client player, Team team) {
		Capes.removeCapes(player);
		deleteGameItems(player);
		int x = 1890;
		int y = 3177;
		if (team == Team.BLUE) {
			x = 1886;
			y = 3171;
		} else if (team == Team.RED) {
			x = 1894;
			y = 3171;
		}

		player.resetInactivity();
		player.getPA().walkableInterface(-1);
		final int teleX = x + Misc.random(1), teleY = y + Misc.random(1), teleZ = 0;
		player.getPA().movePlayer(teleX, teleY, teleZ);
		player.changeRespawn(teleX, teleY, teleZ);
		
		player.setDefaultDiscordStatus();
	}

	public static void enterWaitingRoom(Client player, Team team, Team from) {
//		int bluePlayers = getBluePlayersWaiting();
//		int redPlayers = getRedPlayersWaiting();

		if (!DailyTaskRequirement.canParticipateInMinigame(player)) {
			return;
		}
		
		if ((team == Team.RED || team == Team.BLUE) && loyalToTeam.containsKey(player.getName())) {
			Team teamLoyalty = loyalToTeam.get(player.getName());
			if (teamLoyalty != team) {
				player.sendMessage(
						"This is not your team! Your loyalty is with "
								+ (teamLoyalty == Team.BLUE ? "Blue" : "Red")
								+ " team!");
				return;
			}
		} else if (team == Team.NONE && loyalToTeam.containsKey(player.getName())) { // green portal
			team = loyalToTeam.get(player.getName());
		}
		
		if (gameStarted) { // rejoin the game after it started

			int redPlayers = getRedPlayersWaiting();
			int bluePlayers = getBluePlayersWaiting();
			
			if (team == Team.BLUE) {
				if (!checkGearBonus ? (bluePlayers > redPlayers) : (blueTeamGearOP())) {
					player.sendMessage("The Blue team is imbalanced right now. You must join other team or wait.");
					return;
				}
			} else if (team == Team.RED) {
				if (!checkGearBonus ? (redPlayers > bluePlayers) : (redTeamGearOP())) {
					player.sendMessage("The Red team is imbalanced right now. You must join other team or wait.");
					return;
				}
			}
			final Team joining = team;
			if (!player.getDialogueBuilder().isActive())
				player.getDialogueBuilder().sendPlayerChat("Would you like to join the ongoing SoulWars game?")
					.sendOption("Yes, I would like to join the ongoing game.", ()-> { handlePlayerRejoin(player, joining); }, "No", () -> {}).execute(true);
			return;
		}
		
		if (team == Team.BLUE) {
			if (!checkTeamBalance(player, team)) {
				if (player.debug) {
					player.sendMessage("here 222");
				}
				return;
			}
			
			if (from == Team.RANDOM) {
				player.getPA().movePlayer(BLUE_WAIT_ROOM);
			} else {
				player.getPA().walkTo(-1, 0, true);
			}
			player.sendMessage("You have been added to the <col=13434880>Blue</col> team.");
			player.sendMessage("Next Game Begins In:<col=13434880> " + ((waitTimer + gameTimer) * 1200 / 1000)
					+ " </col>seconds.");
			player.getDotManager().clearPoison();
			waitingBlue.add(player);
			Capes.addCapes(player, Team.BLUE);
			BLUE_TEAM_GEAR_BONUSES += player.getItems().getTotalBonuses();
			if (!loyalToTeam.containsKey(player.getName()))
				loyalToTeam.put(player.getName(), team);
		} else if (team == Team.RED) {
			if (!checkTeamBalance(player, team)) {
				if (player.debug) {
					player.sendMessage("here 111");
				}
				return;
			}

			if (from == Team.RANDOM) {
				player.getPA().movePlayer(RED_WAIT_ROOM);
			} else {
				player.getPA().walkTo(1, 0, true);
			}
			player.sendMessage(
					"[<col=13434880>SoulWars</col>] You have been added to the <col=13434880>Red</col> team.");
			player.sendMessage("Next Game Begins In:<col=13434880> " + ((waitTimer + gameTimer) * 1200 / 1000)
					+ " </col>seconds.");
			player.getDotManager().clearPoison();
			waitingRed.add(player);
			Capes.addCapes(player, Team.RED);
			RED_TEAM_GEAR_BONUSES += player.getItems().getTotalBonuses();
			if (!loyalToTeam.containsKey(player.getName()))
				loyalToTeam.put(player.getName(), team);
		} else if (team == Team.RANDOM) {
			Team toEnterTeam = Team.NONE;
			
			int redPlayers = getRedPlayersWaiting();
			int bluePlayers = getBluePlayersWaiting();
			
			if (!checkGearBonus ? (bluePlayers >= redPlayers) : (bluePlayers >= redPlayers || blueTeamGearOP())) {
				toEnterTeam = Team.RED;
			} else /*if (redPlayers > bluePlayers) */{
				toEnterTeam = Team.BLUE;
			}
			
			if (toEnterTeam == Team.NONE) {
				//System.out.println("Goofd");
				return;
			}
			
			enterWaitingRoom(player, toEnterTeam, team);
		}
	}
	
	private static boolean checkTeamBalance(Client c, Team team) {
		int redPlayers = getRedPlayersWaiting();
		int bluePlayers = getBluePlayersWaiting();
		if (team == Team.RED) {
			if (!checkGearBonus ? (redPlayers > 0 && redPlayers > bluePlayers) : (redTeamGearOP())) {
				c.sendMessage("The red team is imbalanced, try blue team instead!");
				return false;
			}
		} else if (team == Team.BLUE) {
			if (!checkGearBonus ? (bluePlayers > 0 && bluePlayers > redPlayers) : (blueTeamGearOP())) {
				c.sendMessage("The blue team is imbalanced, try red team instead!");
				return false;
			}
		}
		return true;
	}
	
	private static boolean redTeamGearOP() {
		return RED_TEAM_GEAR_BONUSES - BLUE_TEAM_GEAR_BONUSES > 2000;
	}
	
	private static boolean blueTeamGearOP() {
		return BLUE_TEAM_GEAR_BONUSES - RED_TEAM_GEAR_BONUSES > 2000;
	}

	private static void handlePlayerRejoin(Client c, Team team) {
		if (gameTimer < REJOIN_TIMER) {
			c.sendMessage("Sorry but it is too late to join the ongoing SoulWars Game.");
			return;
		}
		boolean teamNoneOrRandom = team == Team.NONE || team == Team.RANDOM;
		if (teamNoneOrRandom && !loyalToTeam.containsKey(c.getName())) {
			if (BLUE_TEAM_GEAR_BONUSES - RED_TEAM_GEAR_BONUSES > 2000) {
				team = Team.RED;
			} else {
				team = Team.BLUE;
			}
		} else if (teamNoneOrRandom && loyalToTeam.containsKey(c.getName())) {
			team = loyalToTeam.get(c.getName());
		}
		if (team == Team.BLUE) {
			if (BLUE_TEAM_GEAR_BONUSES - RED_TEAM_GEAR_BONUSES > 2000) {
				c.sendMessage("The Blue team is imbalanced right now. You must join other team or wait.");
				return;
			}
			c.soulWarsTeam = Team.BLUE;
			soulWarsPlayers.add(c);
			addGameItems(c);
			moveToSpawn(c);
			Capes.addCapes(c, Team.BLUE);
			BLUE_TEAM_GEAR_BONUSES += c.getItems().getTotalBonuses();
			if (!loyalToTeam.containsKey(c.getName()))
				loyalToTeam.put(c.getName(), team);
		} else if (team == Team.RED) {
			if (RED_TEAM_GEAR_BONUSES - BLUE_TEAM_GEAR_BONUSES > 2000) {
				c.sendMessage("The Red team is imbalanced right now. You must join other team or wait.");
				return;
			}
			c.soulWarsTeam = Team.RED;
			soulWarsPlayers.add(c);
			addGameItems(c);
			moveToSpawn(c);
			Capes.addCapes(c, Team.RED);
			RED_TEAM_GEAR_BONUSES += c.getItems().getTotalBonuses();
			if (!loyalToTeam.containsKey(c.getName()))
				loyalToTeam.put(c.getName(), team);
		} else {
			System.out.println("What the flip???  Team:"+team.name());
		}
	}
	
	public static void moveToSpawn(final Client c) {
		Location location;
		if (c.soulWarsTeam == Team.RED) {
			location = RED_GAME_ROOM;
		} else {
			location = BLUE_GAME_ROOM;
		}

		int x = location.getX() + Misc.random(3);
		int y = location.getY() - Misc.random(3);
		while (!RegionClip.canStandOnSpot(x, y, 0)) {
			x = location.getX() + Misc.random(3);
			y = location.getY() - Misc.random(3);
		}

		c.getPA().movePlayer(x, y, 0);
		
		updateDiscord(c);
	}
	
	public static void updateDiscord(Player p) {
		p.getPacketSender().sendDiscordUpdate("", "SoulWars", 0, 0, discordStartTime, discordEndTime);
	}

	public static int getBluePlayersWaiting() {
		return waitingBlue.size();
	}

	/**
	 * This method is used to get the teamNumber of a certain player
	 *
	 * @param player
	 * @return 1 for blue team - 2 for red team
	 */
	public static Team getTeam(Player player) {
		if (player == null) {
			return Team.NONE;
		}

		return player.soulWarsTeam;
	}

	public static int getWaitTimer() {
		return waitTimer;
	}

	public static int countPlayersInTeam(Team team) {
		int players = 0;
		
		for (Client player : soulWarsPlayers) {
			if (player == null || !player.isActive || player.disconnected) {
				continue;
			}
			
			if (player.soulWarsTeam == team) {
				players++;
			}
		}

		return players;
	}

	public static int getRedPlayersWaiting() {
		return waitingRed.size();
	}

	public static int getRedPlayersWaitingBonuses() {
		int amount = 0;
		for (Player player : Server.getRegionManager().getRegionByLocation(RED_WAIT_ROOM).getPlayers()) { // red
																											// room
			if (player == null || player.disconnected || player.forceDisconnect) {
				continue;
			}
			if (!RSConstants.inSoulWarsRedRoom(player.getX(), player.getY())) {
				continue;
			}
			amount += player.getItems().getTotalBonuses();
		}
		return amount;
	}

	public static void startGame() {
		if ((getBluePlayersWaiting() < MINIMUM_PLAYERS || getRedPlayersWaiting() < MINIMUM_PLAYERS)
				&& !Config.SERVER_DEBUG) {
			
			waitTimer = GAME_START_TIMER;
			loyalToTeam.clear(); // game did not start so reset the loyalty stuff

			for (Player player : waitingBlue) { // blue team
				if (player == null || player.disconnected || player.forceDisconnect) {
					continue;
				}
				player.sendMessage("Minimum " + MINIMUM_PLAYERS + " players required in each team to start the game.");
			}

			for (Player player : waitingRed) { // red team
				if (player == null || player.disconnected || player.forceDisconnect) {
					continue;
				}

				player.sendMessage("Minimum " + MINIMUM_PLAYERS + " players required in each team to start the game.");
			}

			return;
		}

		resetGameVars();
		waitTimer = -1;
		gameTimer = GAME_TIMER / 2;
		
		discordStartTime = (int) (System.currentTimeMillis()/1000);
		discordEndTime = discordStartTime + Misc.ticksToSeconds(gameTimer);

		for (Client player : waitingBlue) { // blue
			if (player == null || player.isNotReadyForMinigame()) {
				continue;
			}

			player.soulWarsTeam = Team.BLUE;
			soulWarsPlayers.add(player);
			addGameItems(player);
			moveToSpawn(player);
		}

		for (Client player : waitingRed) { // red room
			if (player == null || player.isNotReadyForMinigame()) {
				continue;
			}

			player.soulWarsTeam = Team.RED;
			soulWarsPlayers.add(player);
			addGameItems(player);
			moveToSpawn(player);
		}

		waitingRed.clear();
		waitingBlue.clear();
		gameStarted = true;
	}

	public static void resetGameVars() {
		ObjectManager.revertObjectsByClass(1);
		Barricades.killAllCades(false);
		healBosses();
		gameTimer = -1;
		waitTimer = GAME_START_TIMER;
		gameStarted = false;
		soulWarsPlayers.clear();
		redGraveyard.reset();
		obelisk.reset();
		blueGraveyard.reset();
		redAvatar.reset();
		blueAvatar.reset();
		resetGearBonuses();
	}
	
	public static void resetGearBonuses() {
		BLUE_TEAM_GEAR_BONUSES = 0;
		RED_TEAM_GEAR_BONUSES = 0;
		for (Player p : waitingRed) {
			if (p == null || p.getItems() == null) {
				continue;
			}

			RED_TEAM_GEAR_BONUSES += p.getItems().getTotalBonuses();
		}
		for (Player p : waitingBlue) {
			if (p == null || p.getItems() == null) {
				continue;
			}

			BLUE_TEAM_GEAR_BONUSES += p.getItems().getTotalBonuses();
		}
	}

	public static void init() {
		Server.getTaskScheduler().schedule(new Task(2) {

			@Override
			protected void execute() {
				if (redAvatar.respawn > 0) {
					redAvatar.respawn--;
				}
				
				if (blueAvatar.respawn > 0) {
					blueAvatar.respawn--;
				}

				if (waitTimer > 0) {
					waitTimer--;
				} else if (waitTimer == 0) {
					startGame();
				}

				if (gameTimer > 0) {
					gameTimer--;

				} else if (gameTimer == 0) {
					endGame();
				}
				
				
				if (gameStarted) {
					obeliskProcess();
					blueGraveYardProcess();
					redGraveYardProcess();
					//Overlay.soulWarsInterface(c);
				} 

			}
		});
	}
	
	private static void blueGraveYardProcess() {
		int redTeam = 0;
		int blueTeam = 0;
		for (Player player : soulWarsPlayers) {//blue graveyard
			if (player == null || player.disconnected || player.forceDisconnect) {
				continue;
			}

			if (RSConstants.inBlueGraveyard(player.absX, player.absY) && !RSConstants.inBlueGraveyardDeath(player.absX, player.absY)) {
				if (player.soulWarsTeam == Team.RED) {
					redTeam++;
				} else if (player.soulWarsTeam == Team.BLUE) {
					blueTeam++;
				} else {
					//System.out.println("cheater, has no team " + player.getName());
				}

				player.capturableProgress = blueGraveyard.state;
				
				if (player.soulWarsTeam == Team.BLUE && player.capturableProgress > 0) {
					player.decreaseInactivity(50);
				} else if (player.capturableProgress < 100) {
					player.decreaseInactivity(50);
				}
			}
		}
//		System.out.println(blueTeam+":"+redTeam+" - blue graveyard="+blueGraveyard.state);

		if (redTeam > blueTeam) {
			blueGraveyard.increase();
		} else if (blueTeam > redTeam) {
			blueGraveyard.decrease();
		}
	}
	
	private static void obeliskProcess() {
		int redTeam = 0;
		int blueTeam = 0;
		for (Player player : soulWarsPlayers) {//blue graveyard
			if (player == null || player.disconnected || player.forceDisconnect) {
				continue;
			}
			
			player.capturableProgress = -1;
			if (RSConstants.inSoulWarsObeliskArea(player.absX, player.absY)) {
				if (player.soulWarsTeam == Team.RED) {
					redTeam++;
				} else if (player.soulWarsTeam == Team.BLUE) {
					blueTeam++;
				} else {
					//System.out.println("cheater, has no team " + player.getName());
				}
				
				player.capturableProgress = obelisk.state;
				
				if (player.soulWarsTeam == Team.BLUE && player.capturableProgress > 0) {
					player.decreaseInactivity(50);
				} else if (player.capturableProgress < 100) {
					player.decreaseInactivity(50);
				}
			}
		}
//		System.out.println(blueTeam+":"+redTeam+" - obelisk="+obelisk.state);
		
		if (redTeam > blueTeam) {
			obelisk.increase();
		} else if (blueTeam > redTeam) {
			obelisk.decrease();
		}
	}
	
	private static void redGraveYardProcess() {
		int redTeam = 0;
		int blueTeam = 0;
		for (Player player : soulWarsPlayers) {
			if (player == null || player.disconnected || player.forceDisconnect) {
				continue;
			}

			if (RSConstants.inRedGraveyard(player.absX, player.absY) && !RSConstants.inRedGraveyardRoomDeath(player.absX, player.absY)) {
				if (player.soulWarsTeam == Team.RED) {
					redTeam++;
				} else if (player.soulWarsTeam == Team.BLUE) {
					blueTeam++;
				} else {
					//System.out.println("cheater, has no team " + player.getName());
				}
				
				player.capturableProgress = redGraveyard.state;
				
				if (player.soulWarsTeam == Team.BLUE && player.capturableProgress > 0) {
					player.decreaseInactivity(50);
				} else if (player.capturableProgress < 100) {
					player.decreaseInactivity(50);
				}
			}
		}
		//System.out.println(blueTeam+":"+redTeam+" - red graveyard="+redGraveyard.state);

		if (redTeam > blueTeam) {
			redGraveyard.increase();
		} else if (blueTeam > redTeam) {
			redGraveyard.decrease();
		}
	}
	
	public static void endGame() {
		Team winningTeam = Team.NONE;//None is draw/tie
		if (redAvatar.deaths > blueAvatar.deaths) {
			winningTeam = Team.BLUE;
		} else if (blueAvatar.deaths > redAvatar.deaths) {
			winningTeam = Team.RED;
			
		}
		
		
		if (gameTimer < 100) {
			for (Client player : soulWarsPlayers) {
				if (player == null || !player.isActive) {
					continue;
				}

				if (winningTeam == Team.NONE) {
					player.setZeals(player.getZeals() + 2);
					player.sendMessage("Tie game! You gain 2 Zeals");
				} else if (player.soulWarsTeam == winningTeam) {
					player.getExtraHiscores().incSoulWarsWins();
					player.setZeals(player.getZeals() + 3);
					player.sendMessage("You won the Soulwars Game. You received 3 Zeals!");
				} else {
					player.setZeals(player.getZeals() + 1);
					player.sendMessage("You lost the Soulwars Game. You received 1 Zeals!");
				}
				if (EXTRA_REWARD) {
					player.setZeals(player.getZeals() + 1);
					player.sendMessage("You have received extra zeal from the soulwars event!");
				}

				leaveGame(player, player.soulWarsTeam);
				player.soulWarsTeam = Team.NONE;
				player.getPA().safeDeathSecureTimer();
				player.setDead(false);
				player.respawnTimer = -5;
				player.sendMessage("[<col=13434880>SoulWars</col>] The SoulWars Game has ended!");
				player.getPacketSender().createPlayerHints(10, -1);
				player.getPacketSender().createObjectHints(1, 1, 170, -1);
				player.getPA().restorePlayer(false);
				player.transform(-1);
				player.getPA().resetMovementAnimation();
				player.resetAnimations();
				player.getCombat().resetPrayers();
			}
		}

		resetGameVars();
		loyalToTeam.clear();
		PlayerHandler.messageAllPlayers("<img=9><col=4848b7> Next Soulwars game is starting in 2 minutes, hurry up!");
	}

	public static void removePlayerFromGame(Client player) {
		if (player == null) {
			return;
		}

		soulWarsPlayers.remove(player);
		if (player.soulWarsTeam == Team.BLUE)
			BLUE_TEAM_GEAR_BONUSES -= player.getItems().getTotalBonuses();
		else
			RED_TEAM_GEAR_BONUSES -= player.getItems().getTotalBonuses();
		if (player.isActive) {
			player.setWaitingRoomCampTimer(0);
			player.sendMessage("[<col=13434880>SoulWars</col>] The SoulWars Game has ended for you!");
			player.getPacketSender().createPlayerHints(10, -1);
		}

		player.getPA().restorePlayer(false);
		player.getPA().safeDeathSecureTimer();
		leaveGame(player, player.soulWarsTeam);
		if (gameStarted && (countPlayersInTeam(Team.RED) <= 0 || countPlayersInTeam(Team.BLUE) <= 0) && !Config.SERVER_DEBUG) {
			endGame();
		}

		player.soulWarsTeam = Team.NONE;
		player.goldObtainedInMinigames = 0;
	}
	
	public static void addGameItems(Client player) {
		
	}
	

	public static void deleteGameItems(Client player) {
		
		int soulsAmount = player.getItems().getItemAmount(SOUL_FRAGMENT);
		player.getItems().deleteItemInOneSlot(SOUL_FRAGMENT, soulsAmount);
		
		player.getItems().deleteItem3(4049, 1);
		player.getItems().deleteItem3(4045, 1);
		player.getItems().deleteItem3(4053, 1);
	}

	public static void onDeath(Client c, Client killer) {
		if (!RSConstants.inSoulWars(c.absX, c.absY))
			return;
		
		//handle drops here
		int amount = c.getItems().getItemAmount(SOUL_FRAGMENT);
		if (c.getItems().deleteItemInOneSlot(SOUL_FRAGMENT, amount)) {
			ItemHandler.createGroundItemFromPlayerDeath((killer != null ? killer : c),
					c.killedByPlayerName, SOUL_FRAGMENT,
					c.getX(), c.getY(), c.getHeightLevel(), amount, c);
		}
		//bones
		for (int i =0 ; i < c.playerItems.length; i++) {
			if (c.playerItems[i]-1 == BONE) {
				if (c.getItems().deleteItemInSlot(i)) {
					ItemHandler.createGroundItemFromPlayerDeath((killer != null ? killer : c),
							c.killedByPlayerName, BONE,
							c.getX(), c.getY(), c.getHeightLevel(), 1, c);
				}
			}
		}
		ItemHandler.createGroundItemFromPlayerDeath((killer != null ? killer : c),
				c.killedByPlayerName, 526,
				c.getX(), c.getY(), c.getHeightLevel(), 1, c);

		c.transform(8623);
		c.resetAnimations();
		c.getPA().resetMovementAnimation();
		c.decreaseInactivity(500);
		c.getStopWatch().startTimer(GRAVEYARD_WAIT_TICKS);
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				c.transform(-1);
				c.getPA().resetMovementAnimation();
				c.resetAnimations();
				c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon], ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			}

			@Override
			public void stop() {
			}

		}, 27);
	}

	public static boolean cantTeleport(Client c) {
		return (c.soulWarsTeam != Team.NONE || waitingRed.contains(c) || waitingBlue.contains(c)) && (c.inSoulWars()
				|| RSConstants.inSoulWarsBlueRoom(c.absX, c.absY) || RSConstants.inSoulWarsRedRoom(c.absX, c.absY));
	}

	public static boolean inSoulWars(Client c) {
		return RSConstants.inSoulWars(c.absX, c.absY) || RSConstants.inSoulWarsBlueRoom(c.absX, c.absY)
				|| RSConstants.inSoulWarsRedRoom(c.absX, c.absY) || RSConstants.inSoulWarsLobby(c.absX, c.absY);
	}
	
	public static void healBosses() {
		int count = 0;
		for (int i = 0; i < NPCHandler.npcs.length; i++) {
			if (count == 2)
				break;
			if (NPCHandler.npcs[i] != null && (NPCHandler.npcs[i].npcType == 8596 || NPCHandler.npcs[i].npcType == 8597) && !NPCHandler.npcs[i].isDead()) {
				count++;
				NPCHandler.npcs[i].setHP(10000);
			}
		}
	}
	
	public static int getTimeRemainingInMunites() {
		return (waitTimer + gameTimer) * 1200 / 1000 / 60;
	}
}
