package com.soulplay.content.minigames.dominiontower;


import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 751 })
public final class MeleeNPC extends CombatNPC {

	private static final int MELEE_ATTACK_ANIM = 6726;
	private static final int DEATH_ANIM = 6727;
	public static final int SPEC_GFX_ID = 1066;

	public MeleeNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = true;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
	}

	@Override
	public void startAttack(Player p) {
		if (Misc.random(4) == 1) {
			drainLife(p);
		} else {	
			startAnimation((MELEE_ATTACK_ANIM));
			meleeSwing(p, 20, MELEE_ATTACK_ANIM);
			p.sendMessage("normal attack");
		}
	}

	@Override
	public void defineConfigs() {
		NpcStats.define(751, 250, 20, 140, 120);
		NPCAnimations.define(751, DEATH_ANIM, -1, MELEE_ATTACK_ANIM);
	}
	
	private void drainLife(final Player p) {
		
		int dmg = Misc.newRandom(25, 50);
		int healAmount = dmg / 2;
		int hpMissing = getSkills().getLifepoints() - getSkills().getMaximumLifepoints();
		
		startAnimation((MELEE_ATTACK_ANIM));
		p.dealDamage(new Hit(dmg, 0, -1));
		
		if (hpMissing > healAmount) {
			healNpc(healAmount);
			startGraphic(Graphic.create(2345, GraphicType.HIGH));
		} else if (!(getSkills().getLifepoints() == getSkills().getMaximumLifepoints())) {
			healNpc(hpMissing);
			startGraphic(Graphic.create(2345, GraphicType.HIGH));
		} else {
		}
		

		
		healNpc(healAmount);
		
		
		
	}
}
