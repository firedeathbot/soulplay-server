package com.soulplay.content.minigames.dominiontower;

import com.soulplay.Config;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.InstanceInteractIndex;
import com.soulplay.game.world.map.InstanceInteractType;
import com.soulplay.game.world.map.InstanceManager;
import com.soulplay.util.Misc;

public class DominionManager extends InstanceManager {

	private DominionWave wave;
	private int waveIndex;

	public DominionManager(Player player) {
		super("Dominion tower", player);
		this.setWave();
	}

	public void advanceWave() {
		waveIndex++;
		if (waveIndex >= DominionWave.values.length) {
			waveIndex = 0;
		}

		setWave();
	}

	public void setWave() {
		if (wave != null) {
			destroy(false);

			// Remove current zone, because we are making new one, so it doesn't bug out.
			getPlayer().getZones().removeDynamicZone(getZone());
			getZone().dontProcessNextTick(true);
		}

		wave = DominionWave.values[waveIndex];
		generateMap();
	}

	public void generateMap() {
		createStaticRegion(wave.getRegionLocation(), wave.getRegionWidth(), wave.getRegionHeight());
		wave.getSpawns().accept(this);
	}

	public void teleport() {
		Player player = getPlayer();
		initPlayerDefaults(player);
		player.pulse(() -> {
			player.getPA().movePlayer(base(wave.getStartLocation()));
		});
	}

	@Override
	public boolean isMinigame() {
		return true;
	}

	@Override
	public boolean interact(Player player, Object interactingWith, InstanceInteractType type, InstanceInteractIndex index) {
		Location location = player.getLocationInstance();
		switch (type) {
			case NPC: {
				NPC npc = (NPC) interactingWith;
				switch (npc.npcType) {
					case 13986:
						if (index == InstanceInteractIndex.FIRST) {
							player.getDialogueBuilder().sendNpcChat(npc.npcType, "Greetings. How may I help you?")
								.sendOption("I'd like to purchase some supplies from you", () -> {
								player.getShops().openShop(77);
							}, "What is this place?", () -> {
								player.getDialogueBuilder().sendPlayerChat("What is this place?").sendNpcChat(npc.npcType,
										"This is the bank of " + Config.SERVER_NAME + ".",
										"We have many branches in many towns.").setClose(true);
							}).execute(false);
						} else if (index == InstanceInteractIndex.SECOND) {
							player.getShops().openShop(77);
						}
						return true;
				}

				break;
			}
			case OBJECT: {
				GameObject object = (GameObject) interactingWith;
				switch (object.getId()) {
					case 104113: //bronze chest
						GameObject bronzeChest = GameObject.createTickObject(110607, object.getLocation(), object.getOrientation(), object.getType(), Integer.MAX_VALUE, -1);
						player.dynObjectManager.addObject(bronzeChest);
						player.startAnimation(new Animation(536));
						player.sendMessage("You open the chest.");
						if (Misc.random(50) == 1) {
							player.getItems().addItem(20851, 1);
						} else {
							 player.setDominionTokens(player.getDominionTokens() + 1000 + Misc.random(1000));
							 player.sendMessage("You find some dominion tokens.");
						}
						return true;
					case 104130: //silver chest
						GameObject silverChest = GameObject.createTickObject(110621, object.getLocation(), object.getOrientation(), object.getType(), Integer.MAX_VALUE, -1);
					    player.dynObjectManager.addObject(silverChest);
					    player.startAnimation(new Animation(536));
						player.sendMessage("You open the chest.");
					    if (Misc.random(25) == 1) {
							player.getItems().addItem(20851, 1);
						} else {
						    player.setDominionTokens(player.getDominionTokens() + 1500 + Misc.random(1500));
						    player.sendMessage("You find some dominion tokens.");
						}
						return true;
					case 21299: //gold chest
						GameObject goldChest = GameObject.createTickObject(21300, object.getLocation(), object.getOrientation(), object.getType(), Integer.MAX_VALUE, -1);
					    player.dynObjectManager.addObject(goldChest);
					    player.startAnimation(new Animation(536));
						player.sendMessage("You open the chest.");
					    if (Misc.random(5) == 1) {
							player.getItems().addItem(20851, 1);
						} else {
							player.setDominionTokens(player.getDominionTokens() + 3000 + Misc.random(3000));
						    player.sendMessage("You find some dominion tokens.");
						}
						return true;
						
					case 21318: //bridge
					case 21319: //bridge
						player.sendMessage("hello");
						if (location.matches(Location.create(30, 18, player.getZ()))) {
							player.getPA().walkTo(-5, 0, 0, 0);
							player.sendMessage("walking over bridge");
						} else {
							player.sendMessage("walking back over bridge " + location + " create " + Location.create(30, 18, player.getZ()));
							player.getPA().walkTo(5, 0, 0, 0);
							
						}
						return true;
					case 21509: //bridge
					case 21317: //bridge
						if (location.matches(Location.create(17, 19, player.getZ()))) {
							player.getPA().walkTo(0, 5, 0, 0);
							player.sendMessage("walking over bridge");
						} else {
							player.sendMessage("walking back over bridge");
							player.getPA().walkTo(0, -5, 0, 0);
							
						}
						return true;
				}
				break;
			}
			default:
				break;
		}

		return super.interact(player, interactingWith, type, index);
	}
	
    @Override
    public void npcDeath(NPC npc) {
    	final int killerMID = npc.getKillerBySQLIndex();
    	final Client c = (Client) PlayerHandler.getPlayerByMID(killerMID);
    	if (c != null && c.isActive) {
    		switch (npc.npcType) {
    	    case 751:
    	    case 8441:
    	    case 107559:
    	    	c.setDominionTokens(c.getDominionTokens() + 600 + Misc.random(600));
    	    	c.sendMessage("You found some Dominion tokens.");
    		    return;
    	}
	}
	super.npcDeath(npc);
}

}
