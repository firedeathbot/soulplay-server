package com.soulplay.content.minigames.dominiontower;

import java.util.function.Consumer;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public enum DominionWave {
	
	//Barrelchest
	WAVE_0(Location.create(8790, 10254), 64, 64, Location.create(52, 38, 1), manager -> { //small 2 bridge dungeon
		manager.spawnNpc(13986, 46, 35, 1, Direction.NORTH); //shop
		manager.spawnNpc(751, 34, 19, 1, Direction.NORTH);
		manager.spawnNpc(8441, 24, 14, 1, Direction.NORTH);
		manager.spawnNpc(107559, 19, 6, 1, Direction.NORTH);
		manager.spawnNpc(8441, 13, 11, 1, Direction.NORTH);
		manager.spawnNpc(751, 13, 11, 1, Direction.NORTH);
		manager.spawnNpc(107559, 17, 17, 1, Direction.NORTH);
		manager.spawnNpc(8441, 18, 26, 1, Direction.NORTH);
		manager.spawnNpc(751, 24, 33, 1, Direction.NORTH);
		manager.spawnNpc(107559, 15, 37, 1, Direction.NORTH);
		manager.spawnNpc(5666, 26, 52, 1, Direction.SOUTH);
		manager.spawnObject(104113, 14, 3, 1, Direction.NORTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 15, 40, 1, Direction.EAST.toInteger(), 10);
	}),

	//Chaos elemental
	WAVE_1(Location.create(7569, 9973), 64, 64, Location.create(17, 53), manager -> { //red raid small dungeon
		manager.spawnNpc(13986, 25, 55, 0, Direction.NORTH); //shop
		manager.spawnNpc(751, 40, 52, 0, Direction.NORTH);
		manager.spawnNpc(8441, 48, 52, 0, Direction.NORTH);
		manager.spawnNpc(107559, 50, 41, 0, Direction.NORTH);
		manager.spawnNpc(8441, 48, 36, 0, Direction.NORTH);
		manager.spawnNpc(751, 40, 36, 0, Direction.NORTH);
		manager.spawnNpc(107559, 30, 36, 0, Direction.NORTH);
		manager.spawnNpc(8441, 25, 36, 0, Direction.NORTH);
		manager.spawnNpc(751, 19, 32, 0, Direction.NORTH);
		manager.spawnNpc(3200, 19, 16, 0, Direction.SOUTH);
		manager.spawnObject(randomChest(), 19, 37, 0, Direction.EAST.toInteger(), 10);
		
	}),
	
	//chaos fanatic
	WAVE_2(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
		manager.spawnNpc(107559, 152, 111, 0, Direction.NORTH);
		manager.spawnNpc(751, 156, 112, 0, Direction.NORTH);
		manager.spawnNpc(8441, 142, 102, 0, Direction.NORTH);
		manager.spawnNpc(107559, 162, 89, 0, Direction.NORTH);
		manager.spawnNpc(8441, 162, 84, 0, Direction.NORTH);
		manager.spawnNpc(751, 143, 74, 0, Direction.NORTH);
		manager.spawnNpc(107559, 140, 79, 0, Direction.NORTH);
		manager.spawnNpc(8441, 141, 85, 0, Direction.NORTH);
		manager.spawnNpc(751, 117, 75, 0, Direction.NORTH);
		manager.spawnNpc(107559, 114, 74, 0, Direction.NORTH);
		manager.spawnNpc(4190, 97, 111, 0, Direction.SOUTH);
		manager.spawnObject(randomChest(), 103, 74, 0, Direction.WEST.toInteger(), 10);
		manager.spawnObject(randomChest(), 170, 70, 0, Direction.NORTH.toInteger(), 10);	
	}),
	//Giant mole
	WAVE_3(Location.create(1753, 5140), 64, 128, Location.create(25, 117), manager -> { //giant mole map
		manager.spawnNpc(13986, 176, 21, 114, Direction.NORTH); //shop	
		manager.spawnNpc(107559, 9, 90, 0, Direction.NORTH);
		manager.spawnNpc(751, 16, 88, 0, Direction.NORTH);
		manager.spawnNpc(8441, 44, 82, 0, Direction.NORTH);
		manager.spawnNpc(107559, 52, 68, 0, Direction.NORTH);
		manager.spawnNpc(8441, 49, 117, 0, Direction.NORTH);
		manager.spawnNpc(751, 17, 51, 0, Direction.NORTH);
		manager.spawnNpc(107559, 33, 43, 0, Direction.NORTH);
		manager.spawnNpc(8441, 50, 45, 0, Direction.NORTH);
		manager.spawnNpc(751, 9, 106, 0, Direction.NORTH);
		manager.spawnNpc(107559, 27, 83, 0, Direction.NORTH);
		manager.spawnNpc(3340, 33, 62, 0, Direction.SOUTH);
		manager.spawnObject(randomChest(), 48, 124, 0, Direction.SOUTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 32, 48, 0, Direction.NORTH.toInteger(), 10);	
	
	}),
	//king black dragon
	WAVE_4(Location.create(1991, 4625), 192, 192, Location.create(7, 17), manager -> { //bright brown dungeon
		manager.spawnNpc(13986, 13, 18, 0, Direction.NORTH); //shop
		manager.spawnNpc(107559, 41, 29, 0, Direction.NORTH);
		manager.spawnNpc(751, 33, 30, 0, Direction.NORTH);
		manager.spawnNpc(8441, 37, 19, 0, Direction.NORTH);
		manager.spawnNpc(107559, 23, 11, 0, Direction.NORTH);
		manager.spawnNpc(8441, 38, 10, 0, Direction.NORTH);
		manager.spawnNpc(751, 54, 28, 0, Direction.NORTH);
		manager.spawnNpc(107559, 23, 36, 0, Direction.NORTH);
		manager.spawnNpc(8441, 60, 35, 0, Direction.NORTH);
		manager.spawnNpc(751, 17, 5, 0, Direction.NORTH);
		manager.spawnNpc(107559, 9, 38, 0, Direction.NORTH);
		manager.spawnNpc(50, 8, 53, 0, Direction.SOUTH);
		manager.spawnObject(randomChest(), 14, 3, 0, Direction.EAST.toInteger(), 10);
		manager.spawnObject(randomChest(), 53, 20, 0, Direction.NORTH.toInteger(), 10);	
    }),
	//callisto
	WAVE_5(Location.create(9657, 9224), 64, 64, Location.create(57, 8), manager -> { //brown small dungeon
		manager.spawnNpc(13986, 56, 11, 0, Direction.SOUTH); //shop
		manager.spawnNpc(107559, 42, 5, 0, Direction.NORTH);
		manager.spawnNpc(751, 40, 10, 0, Direction.NORTH);
		manager.spawnNpc(8441, 39, 47, 0, Direction.NORTH);
		manager.spawnNpc(107559, 54, 21, 0, Direction.NORTH);
		manager.spawnNpc(8441, 50, 15, 0, Direction.NORTH);
		manager.spawnNpc(751, 57, 31, 0, Direction.NORTH);
		manager.spawnNpc(107559, 47, 29, 0, Direction.NORTH);
		manager.spawnNpc(4174, 30, 34, 0, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 26, 52, 0, Direction.EAST.toInteger(), 10);
    }),
	//scorpia
	WAVE_6(Location.create(8347, 4868), 64, 64, Location.create(27, 4, 2), manager -> { //brown small dungeon
		manager.spawnNpc(13986, 24, 9, 2, Direction.SOUTH); //shop
		manager.spawnNpc(107559, 13, 5, 2, Direction.NORTH);
		manager.spawnNpc(751, 14, 13, 2, Direction.NORTH);
		manager.spawnNpc(8441, 23, 20, 2, Direction.NORTH);
		manager.spawnNpc(107559, 42, 11, 2, Direction.NORTH);
		manager.spawnNpc(8441, 41, 5, 2, Direction.NORTH);
		manager.spawnNpc(751, 52, 5, 2, Direction.NORTH);
		manager.spawnNpc(107559, 38, 20, 2, Direction.NORTH);
		manager.spawnNpc(4172, 52, 18, 2, Direction.SOUTH); //boss
		manager.spawnNpc(109, 49, 18, 2, Direction.SOUTH); //boss
		manager.spawnNpc(109, 49, 15, 2, Direction.SOUTH); //boss
		manager.spawnNpc(109, 52, 15, 2, Direction.SOUTH); //boss
		manager.spawnNpc(109, 54, 14, 2, Direction.SOUTH); //boss
		manager.spawnNpc(109, 55, 17, 2, Direction.SOUTH); //boss
		manager.spawnNpc(109, 52, 16, 2, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 6, 15, 2, Direction.EAST.toInteger(), 10);
    }),
	//crazy archeologist
	WAVE_7(Location.create(2386, 4707), 64, 64, Location.create(10, 20), manager -> { //frankenstein
		manager.spawnNpc(13986, 9, 13, 0, Direction.NORTH); //shop
		manager.spawnNpc(107559, 32, 8, 0, Direction.NORTH);
		manager.spawnNpc(751, 40, 5, 0, Direction.NORTH);
		manager.spawnNpc(8441, 44, 9, 0, Direction.NORTH);
		manager.spawnNpc(107559, 48, 7, 0, Direction.NORTH);
		manager.spawnNpc(8441, 53, 10, 0, Direction.NORTH);
		manager.spawnNpc(751, 54, 28, 0, Direction.NORTH);
		manager.spawnNpc(107559, 55, 31, 0, Direction.NORTH);
		manager.spawnNpc(8441, 54, 48, 0, Direction.NORTH);
		manager.spawnNpc(751, 38, 41, 0, Direction.NORTH);
		manager.spawnNpc(107559, 34, 56, 0, Direction.NORTH);
		manager.spawnNpc(4186, 17, 35, 0, Direction.SOUTH);
		manager.spawnObject(randomChest(), 62, 8, 0, Direction.EAST.toInteger(), 10);
		manager.spawnObject(randomChest(), 41, 37, 0, Direction.EAST.toInteger(), 10);
		   
    }),
	//venenatis
	WAVE_8(Location.create(9657, 9224), 64, 64, Location.create(57, 8), manager -> { //brown small dungeon
		manager.spawnNpc(13986, 56, 11, 0, Direction.SOUTH); //shop
		manager.spawnNpc(107559, 40, 10, 0, Direction.NORTH);
		manager.spawnNpc(751, 42, 5, 0, Direction.NORTH);
		manager.spawnNpc(8441, 54, 21, 0, Direction.NORTH);
		manager.spawnNpc(107559, 39, 47, 0, Direction.NORTH);
		manager.spawnNpc(8441, 47, 29, 0, Direction.NORTH);
		manager.spawnNpc(751, 57, 31, 0, Direction.NORTH);
		manager.spawnNpc(107559, 50, 15, 0, Direction.NORTH);
		manager.spawnNpc(4173, 30, 34, 0, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 24, 19, 0, Direction.NORTH.toInteger(), 10);
    }),
	//kalphite queen
	WAVE_9(Location.create(8300, 9002), 128, 128, Location.create(83, 8, 1), manager -> { //frankenstein
		manager.spawnNpc(13986, 86, 12, 1, Direction.EAST); //shop
		manager.spawnNpc(107559, 85, 32, 1, Direction.NORTH);
		manager.spawnNpc(751, 78, 29, 1, Direction.NORTH);
		manager.spawnNpc(8441, 93, 26, 1, Direction.NORTH);
		manager.spawnNpc(107559, 71, 35, 1, Direction.NORTH);
		manager.spawnNpc(8441, 66, 40, 1, Direction.NORTH);
		manager.spawnNpc(751, 58, 37, 1, Direction.NORTH);
		manager.spawnNpc(107559, 53, 39, 1, Direction.NORTH);
		manager.spawnNpc(8441, 58, 44, 1, Direction.NORTH);
		manager.spawnNpc(751, 69, 52, 1, Direction.NORTH);
		manager.spawnNpc(107559, 77, 50, 1, Direction.NORTH);
		manager.spawnNpc(1158, 36, 41, 1, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 77, 50, 1, Direction.EAST.toInteger(), 10);
		
    }),
	//thermonuclear smoke devil
	WAVE_10(Location.create(2406, 10229), 64, 64, Location.create(38, 11), manager -> { //gray dungeon with hole in middle
		manager.spawnNpc(13986, 36, 10, 0, Direction.SOUTH); //shop
		manager.spawnNpc(107559, 40, 39, 0, Direction.NORTH);
		manager.spawnNpc(751, 52, 23, 0, Direction.NORTH);
		manager.spawnNpc(8441, 43, 24, 0, Direction.NORTH);
		manager.spawnNpc(107559, 53, 21, 0, Direction.NORTH);
		manager.spawnNpc(8441, 24, 50, 0, Direction.NORTH);
		manager.spawnNpc(751, 10, 35, 0, Direction.NORTH);
		manager.spawnNpc(107559, 15, 17, 0, Direction.NORTH);
		manager.spawnNpc(100499, 22, 24, 0, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 38, 54, 0, Direction.NORTH.toInteger(), 10);
    }),
	//tormented demon
	WAVE_11(Location.create(2496, 5707), 192, 192, Location.create(29, 114), manager -> { //tormented demon map
		manager.spawnNpc(13986, 33, 107, 0, Direction.NORTH); //shop
		manager.spawnNpc(107559, 44, 91, 0, Direction.NORTH);
		manager.spawnNpc(751, 50, 83, 0, Direction.NORTH);
		manager.spawnNpc(8441, 81, 114, 0, Direction.NORTH);
		manager.spawnNpc(107559, 76, 114, 0, Direction.NORTH);
		manager.spawnNpc(8441, 102, 113, 0, Direction.NORTH);
		manager.spawnNpc(751, 108, 98, 0, Direction.NORTH);
		manager.spawnNpc(107559, 114, 99, 0, Direction.NORTH);
		manager.spawnNpc(8441, 120, 76, 0, Direction.NORTH);
		manager.spawnNpc(751, 42, 77, 0, Direction.NORTH);
		manager.spawnNpc(107559, 13, 97, 0, Direction.NORTH);
		manager.spawnNpc(8441, 7, 98, 0, Direction.NORTH);
		manager.spawnNpc(751, 9, 68, 0, Direction.NORTH);
		manager.spawnNpc(107559, 8, 36, 0, Direction.NORTH);
		manager.spawnNpc(8441, 9, 29, 0, Direction.NORTH);
		manager.spawnNpc(751, 53, 19, 0, Direction.NORTH);
		manager.spawnNpc(107559, 51, 56, 0, Direction.NORTH);
		manager.spawnNpc(8441, 58, 48, 0, Direction.NORTH);
		manager.spawnNpc(107559, 73, 40, 0, Direction.NORTH);
		manager.spawnNpc(8349, 101, 31, 0, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 120, 70, 0, Direction.NORTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 120, 98, 0, Direction.NORTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 109, 116, 0, Direction.NORTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 70, 117, 0, Direction.NORTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 6, 68, 0, Direction.EAST.toInteger(), 10);
		manager.spawnObject(randomChest(), 9, 101, 0, Direction.SOUTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 8, 24, 0, Direction.NORTH.toInteger(), 10);
		manager.spawnObject(randomChest(), 58, 23, 0, Direction.WEST.toInteger(), 10);	   
    }),
	//vetion
	WAVE_12(Location.create(2527, 5836), 64, 64, Location.create(31, 12), manager -> { //frankenstein
		manager.spawnNpc(13986, 29, 11, 0, Direction.NORTH); //shop
		manager.spawnNpc(107559, 39, 16, 0, Direction.NORTH);
		manager.spawnNpc(751, 41, 21, 0, Direction.NORTH);
		manager.spawnNpc(8441, 22, 18, 0, Direction.NORTH);
		manager.spawnNpc(107559, 21, 23, 0, Direction.NORTH);
		manager.spawnNpc(8441, 33, 24, 0, Direction.NORTH);
		manager.spawnNpc(751, 32, 34, 0, Direction.NORTH);
		manager.spawnNpc(107559, 23, 46, 0, Direction.NORTH);
		manager.spawnNpc(8441, 33, 24, 0, Direction.NORTH);	
		manager.spawnNpc(4175, 31, 54, 0, Direction.SOUTH); //boss
		manager.spawnObject(randomChest(), 21, 24, 0, Direction.NORTH.toInteger(), 10);
   
	}),
	//deranged archeologist
	WAVE_13(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//dagganoth kings
	WAVE_14(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//kril tsusaroth
	WAVE_15(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//commander zilyana
	WAVE_16(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//general graardor
	WAVE_17(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//lizardsaman
	WAVE_18(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//corporeal beast
	WAVE_19(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//nex
	WAVE_20(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
    }),
	//queen black dragon
	WAVE_21(Location.create(9895, 9685), 192, 192, Location.create(181, 31), manager -> { //frankenstein
		manager.spawnNpc(13986, 176, 32, 0, Direction.NORTH); //shop
	});

	public static final DominionWave[] values = values();
	private final Location regionLocation;
	private final int regionWidth;
	private final int regionHeight;
	private final Location startLocation;
	private final Consumer<DominionManager> spawns;

	DominionWave(Location regionLocation, Location startLocation, Consumer<DominionManager> spawns) {
		this(regionLocation, 64, 64, startLocation, spawns);
	}

	DominionWave(Location regionLocation, int regionWidth, int regionHeight, Location startLocation, Consumer<DominionManager> spawns) {
		this.regionLocation = regionLocation;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
		this.startLocation = startLocation;
		this.spawns = spawns;
	}

	public Location getRegionLocation() {
		return regionLocation;
	}

	public int getRegionWidth() {
		return regionWidth;
	}

	public int getRegionHeight() {
		return regionHeight;
	}

	public Location getStartLocation() {
		return startLocation;
	}
	
	public Consumer<DominionManager> getSpawns() {
		return spawns;
	}

	public static final int[] CHESTS = { 104113, 104130, 21299 };

	public static int randomChest() {
		return Misc.random(CHESTS);
	}

}