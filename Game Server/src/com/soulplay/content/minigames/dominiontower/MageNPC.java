package com.soulplay.content.minigames.dominiontower;

import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 8441 })
public final class MageNPC extends CombatNPC {

	private static final int MAGE_ATTACK_ANIM = 27855;
	private static final int DEATH_ANIM = 836;

	public MageNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setFollowDistance(8);
		getDistanceRules().setMeleeDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (Misc.random(4) == 1) {
			startAnimation(MAGE_ATTACK_ANIM);
			startGraphic(Graphic.create(5464, GraphicType.HIGH));
			Location end = p.getCurrentLocation().copyNew();
			Projectile projectile = Projectile.create(getCenterLocation(), end, 5465);
			projectile.setEndHeight(1);
			projectile.setEndDelay(130);
			projectile.setStartDelay(35);
			projectile.send();
			pulse(() -> {
				if (p.getCurrentLocation().equals(end)) {
					p.dealDamage(new Hit(Misc.random(40, 60), 0, -1));
				}
				Graphic.create(5466).send(end);
			}, 6);
		} else {
				startAnimation((MAGE_ATTACK_ANIM));
				if (Misc.random(2) == 1) {
				    startGraphic(Graphic.create(5461, GraphicType.HIGH));
				    projectileSwing(p, 5462, 15, 3, Graphic.create(5463, GraphicType.HIGH), CombatType.MAGIC);
				} else if (Misc.random(2) == 2) {
				    startGraphic(Graphic.create(5458, GraphicType.HIGH));
				    projectileSwing(p, 5459, 12, 3, Graphic.create(5460, GraphicType.HIGH), CombatType.MAGIC);
				} else {
				    startGraphic(Graphic.create(5455, GraphicType.HIGH));
				    projectileSwing(p, 5456, 10, 3, Graphic.create(5457, GraphicType.HIGH), CombatType.MAGIC);
				}
			}
		}

	@Override
	public void defineConfigs() {
		NpcStats.define(8441, 250, 15, 140, 120);
		NPCAnimations.define(8441, DEATH_ANIM, -1, MAGE_ATTACK_ANIM);
	}
}
