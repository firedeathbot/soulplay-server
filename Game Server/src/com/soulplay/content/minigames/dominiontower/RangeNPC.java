package com.soulplay.content.minigames.dominiontower;

import com.soulplay.Server;
import com.soulplay.content.combat.CombatType;
import com.soulplay.content.npcs.NpcStats;
import com.soulplay.content.npcs.impl.bosses.vorkath.VorkathBoss;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 107559 })
public final class RangeNPC extends CombatNPC {

	private static final int RANGE_ATTACK_ANIM = 426;
	private static final int DEATH_ANIM = 836;
	private static final int DELAY = 1;
	private static final int DELAY2 = Misc.serverToClientTick(DELAY);
	public static final int SPEC_GFX_ID = 1066;

	public RangeNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		walkUpToHit = false;
		stopAttackDefault = true;
		isAggressive = true;
		setCanWalk(true);
		walksOnTopOfNpcs = true;
		getDistanceRules().setFollowDistance(8);
		getDistanceRules().setMeleeDistance(8);
	}

	@Override
	public void startAttack(Player p) {
		if (Misc.random(4) == 1) {
			setAttackTimer(10);
			CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
				int tick = 0;
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					if (p.isDead() || isDead() || tick > 5 || p.getZ() != getZ()) {
						container.stop();
						setAttackTimer(getCombatStats().getAttackSpeed());
						return;
					}
					
					if (tick % 2 == 0) {
						p.pulse(() -> {
							resetAnimations();
						});
						startAnimation((RANGE_ATTACK_ANIM));
						startGraphic(Graphic.create(2962, GraphicType.HIGH));
				        p.pulse(() -> {
				        	startGraphic(Graphic.create(65535, GraphicType.HIGH));
							singleRapidFireBreath(p);
							
				        });
					}
			        tick++;
				}
			}, 1);
		} else {
				startAnimation((RANGE_ATTACK_ANIM));
				startGraphic(Graphic.create(250, GraphicType.HIGH));
				projectileSwing(p, 249, 15, 3, null, CombatType.RANGED);
			}
		}

	@Override
	public void defineConfigs() {
		NpcStats.define(107559, 250, 15, 140, 120);
		NPCAnimations.define(107559, DEATH_ANIM, -1, RANGE_ATTACK_ANIM);
	}
	
	private void singleRapidFireBreath(final Player p) {
		
		final Location end = p.getCurrentLocation().copyNew();

		Projectile proj = new Projectile(SPEC_GFX_ID, getCurrentLocation(),  end);
		proj.setStartHeight(30);
		proj.setStartDelay(0);
		proj.setEndDelay(DELAY2);
		proj.setSlope(0);

		GlobalPackets.createProjectileForPlayerOnly(p, proj);
		
		Server.getStillGraphicsManager().stillGraphics(p, end.getX(), end.getY(), 0, 2189, DELAY2);
		Server.getStillGraphicsManager().stillGraphics(p, end.getX(), end.getY(), 0, 2190, DELAY2);
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				if (Location.equals(p.getCurrentLocation(), end)) {
					p.dealDamage(new Hit(Misc.newRandom(12, 20), 0, -1));
				}

			}
		}, DELAY);
		
	}
}
