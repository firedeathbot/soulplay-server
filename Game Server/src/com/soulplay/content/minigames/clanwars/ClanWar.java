package com.soulplay.content.minigames.clanwars;

import java.util.ArrayList;

import com.soulplay.content.clans.Clan;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;

/**
 * @author Some retard
 */
public class ClanWar {

	private ArrayList<GameItem> clanWarStakes = new ArrayList<>();

	public ArrayList<Client> deadPlayers = new ArrayList<>();

	public ArrayList<Client> clan1Players = new ArrayList<>();

	public ArrayList<Client> clan2Players = new ArrayList<>();

	private Clan clan1;

	private Clan clan2;

	private String clan1Founder;

	private String clan2Founder;

	private String clan1Name;

	private String clan2Name;

	private int playerId;

	private int otherPlayerId;

	private int heightLevel;

	private int clanWarId;

	private int wallTime;

	private int clan1Kills;

	private int clan2Kills;

	private boolean clanWarRules[] = new boolean[11];

	private int duelConfig = 0;

	private boolean endGame = false;

	private boolean rankedGame = false;

	public ClanWar(int playerId, int otherPlayerId, int heightLevel,
			int clanWarId, int wallTime, Clan clan1, Clan clan2) {
		this.playerId = playerId;
		this.otherPlayerId = otherPlayerId;
		this.heightLevel = heightLevel;
		this.clanWarId = clanWarId;
		this.wallTime = wallTime;
		this.clan1 = clan1;
		this.clan2 = clan2;
	}

	public Clan getClan1Clan() {
		return clan1;
	}

	public String getClan1Founder() {
		return clan1Founder;
	}

	public int getClan1Kills() {
		return clan1Kills;
	}

	public String getClan1Name() {
		return clan1Name;
	}

	public Clan getClan2Clan() {
		return clan2;
	}

	public String getClan2Founder() {
		return clan2Founder;
	}

	public int getClan2Kills() {
		return clan2Kills;
	}

	public String getClan2Name() {
		return clan2Name;
	}

	public boolean[] getClanWarRules() {
		return clanWarRules;
	}

	public ArrayList<GameItem> getClanWarStakes() {
		return clanWarStakes;
	}

	public int getDuelConfig() {
		return duelConfig;
	}

	public int getHeight() {
		return heightLevel;
	}

	public int getId() {
		return playerId;
	}

	public int getOtherId() {
		return otherPlayerId;
	}

	public int getWallTime() {
		return wallTime;
	}

	public int getWarId() {
		return clanWarId;
	}

	public boolean isEndGame() {
		return endGame;
	}

	public boolean isRankedGame() {
		return rankedGame;
	}

	public void setClan1Clan(Clan clan1Clan) {
		this.clan1 = clan1Clan;
	}

	public void setClan1Founder(String clan1) {
		this.clan1Founder = clan1;
	}

	public void setClan1Kills(int clan1Kills) {
		this.clan1Kills = clan1Kills;
	}

	public void setClan1Name(String clan1Name) {
		this.clan1Name = clan1Name;
	}

	public void setClan2Clan(Clan clan2Clan) {
		this.clan2 = clan2Clan;
	}

	public void setClan2Founder(String clan2) {
		this.clan2Founder = clan2;
	}

	public void setClan2Kills(int clan2Kills) {
		this.clan2Kills = clan2Kills;
	}

	public void setClan2Name(String clan2Name) {
		this.clan2Name = clan2Name;
	}

	public void setClanWarRules(boolean clanWarRules[]) {
		this.clanWarRules = clanWarRules;
	}

	public void setClanWarStakes(ArrayList<GameItem> clanWarStakes) {
		this.clanWarStakes = clanWarStakes;
	}

	public void setDuelConfig(int duelConfig) {
		this.duelConfig = duelConfig;
	}

	public void setEndGame(boolean endGame) {
		this.endGame = endGame;
	}

	public void setRankedGame(boolean rankedGame) {
		this.rankedGame = rankedGame;
	}

	public int setWallTime(int time) {
		return this.wallTime = time;
	}
}
