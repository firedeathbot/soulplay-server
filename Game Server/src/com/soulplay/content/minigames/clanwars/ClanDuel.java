package com.soulplay.content.minigames.clanwars;

import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.clans.Clan;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.util.interfacedata.InterfaceIds;

public class ClanDuel {

	public static boolean canStake(Client player1, Client player2, int itemID,
			int fromSlot) {

		if (player1.isIronMan()) {
			player1.sendMessage("IronMan Mode does are not allowed to stake.");
			return false;
		}

		if (player2.isIronMan()) {
			player1.sendMessage(
					"You cannot stake with an Ironman Mode player.");
			return false;
		}

		if (!DBConfig.MINI_GAMES) {
			player1.sendMessage(
					"Staking/Minigames have been disabled by the admins.");
			return false;
		}

		if (!DBConfig.STAKING) {
			player1.sendMessage("Staking is currently Disabled.");
			return false;
		}

		if (fromSlot < 0 || fromSlot >= player1.playerItems.length) {
			return false;
		}

		if (itemID < 1) {
			return false;
		}

		if (WorldType.equalsType(WorldType.SPAWN)) {
			if (itemID == 995 && player1.timePlayed < 43200) {
				player1.sendMessage(
						"Your account is not old enough to trade this item.");
				return false;
			}
		}
		if (itemID == 2996 && player1.timePlayed < 1800) {
			player1.sendMessage(
					"Your account is not old enough to trade this item.");
			return false;
		}

		if (!ItemProjectInsanity.itemIsTradeable(itemID)) {
			player1.sendMessage("You can't stake this item.");
			return false;
		}

		if (ItemProjectInsanity.isDungeoneeringItem(itemID)) {
			player1.sendMessage("You can't stake this item.");
			return false;
		}

		for (int i : DBConfig.DUNG_REWARDS) {
			if (i == itemID) {
				player1.sendMessage("You can't stake this item.");
				return false;
			}
		}

		if (itemID != player1.playerItems[fromSlot] - 1) {
			return false;
		}

		if (player1.playerRights == 2 && !DBConfig.ADMIN_CAN_TRADE) {
			player1.sendMessage("Administrator can't stake items.");
			return false;
		}

		return true;
	}

	public static void changedDuelRulesOrStake(Client c, Client o) {

		if (c.chalAccepted) {
			c.chalAccepted = false;
		}
		if (o.chalAccepted) {
			o.chalAccepted = false;
		}

		o.getPacketSender().sendFrame126("", 6684);
		c.getPacketSender().sendFrame126("", 6684);

		c.setDuelRuleTimer(8);
		c.getPacketSender().sendFrame126("<col=ff0000>Accept", 6680);
		o.setDuelRuleTimer(8);
		o.getPacketSender().sendFrame126("<col=ff0000>Accept", 6680);

	}

	public static void declineStake(Client player1, Client player2) {

		if (player1 != null && player2 != null) {
			// give back player1 items
			if (!player1.getClanWarPlayerStake().isEmpty()) {
				for (GameItem item : player1.getClanWarPlayerStake()) {
					player1.getItems().addItem(item.getId(), item.getAmount());
				}
				player1.getClanWarPlayerStake().clear();
			}
			// give back player2 items
			if (!player2.getClanWarPlayerStake().isEmpty()) {
				for (GameItem item : player2.getClanWarPlayerStake()) {
					player2.getItems().addItem(item.getId(), item.getAmount());
				}
				player2.getClanWarPlayerStake().clear();
			}
		}

		if (player1 != null) {
			if (player1.getDuelingClanWarClient() != null) {
				player1.setDuelingClientClanWar(null);
			}
			if (player1.interfaceIdOpenMainScreen != 0) {
				player1.getPA().closeAllWindows();
			}
			resetClanDuelStuff(player1);

			player1.setUpdateInvInterface(3214);
		}
		if (player2 != null) {
			if (player2.getDuelingClanWarClient() != null) {
				player2.setDuelingClientClanWar(null);
			}
			if (player2.interfaceIdOpenMainScreen != 0) {
				player2.getPA().closeAllWindows();
			}
			resetClanDuelStuff(player2);

			player2.setUpdateInvInterface(3214);
		}
	}

	/**
	 * the game has been accepted by both parties, so place the staked items
	 * into the pot.
	 *
	 * @param c
	 *            - player1
	 * @param c2
	 *            - player2
	 * @param clanWar
	 *            - the assigned clan war to those two clans
	 */
	public static void insertStakeItems(Client c, Client c2, ClanWar clanWar) {
		if (!c.getClanWarPlayerStake().isEmpty()) {
			clanWar.getClanWarStakes().addAll(c.getClanWarPlayerStake());
			c.getClanWarPlayerStake().clear();
		}
		if (!c2.getClanWarPlayerStake().isEmpty()) {
			clanWar.getClanWarStakes().addAll(c2.getClanWarPlayerStake());
			c2.getClanWarPlayerStake().clear();
		}
	}

	public static void lootClanChest(Client c) {
		if (c.getClan() == null) {
			c.sendMessage("This chest is for Clan War winning loot.");
			return;
		}

		if (!c.getClan().getClanWarLoot().isEmpty()) {
			if (c.getClan().getRank(c.getName()) < Clan.Rank.GENERAL) {
				c.sendMessage(
						"In order to loot from the Clan War chest, you must be General+ Rank.");
				return;
			}

			ArrayList<GameItem> temp = new ArrayList<>();
			for (GameItem item : c.getClan().getClanWarLoot()) {
				if (c.getItems().freeSlots() == 0) {
					c.sendMessage("Please make more room in your inventory.");
					break;
				}
				if (ItemProjectInsanity.itemIsStackable(item.getId())) { // check for max stack
					long totalAmount = item.getAmount();
					totalAmount += c.getItems().getItemAmount(item.getId());
					if (totalAmount > Integer.MAX_VALUE) {
						c.sendMessage(
								"Please make more room in your inventory.");
						break;
					}
					c.getItems().addItem(item.getId(), item.getAmount());
					temp.add(item);
				} else { // non stackable items.
					c.getItems().addItem(item.getId(), item.getAmount());
					temp.add(item);
				}

			}

			if (!temp.isEmpty()) {
				for (GameItem item : temp) {
					c.getClan().getClanWarLoot().remove(item);
				}
				temp.clear();
			}

		} else {
			c.sendMessage(
					"This chest is for Clan War winning loot. It is empty.");
			return;
		}
	}

	public static void openDuelingScreen(Client c, Client c2) {

		// if (c.duelScreenIsDuel)
		// c.switchDuelScreen();
		// if (c2.duelScreenIsDuel)
		// c2.switchDuelScreen();

		c.duelOption = c2.duelOption = 0;
		c.clanWarRule = c2.clanWarRule = new boolean[11];

		c.getPacketSender().sendFrame87(286, 0); // reset rules interface
		c2.getPacketSender().sendFrame87(286, 0); // reset rules interface

		c.getPacketSender().sendFrame248(6575, 3321); // duel screen interface
		refreshDuelScreen(c);
		c.getPacketSender().sendFrame126b(
				"Challenging clan: " + c2.getClan().getTitle(), 6671);
		c.setUpdateInvInterface(3322); // c.getItems().resetItems(3322);

		c2.getPacketSender().sendFrame248(6575, 3321); // duel screen interface
		refreshDuelScreen(c2);
		c2.getPacketSender().sendFrame126b(
				"Challenging clan: " + c.getClan().getTitle(), 6671);
		c2.setUpdateInvInterface(3322); // c2.getItems().resetItems(3322);
	}

	public static void refreshDuelScreen(Client c) {
		Client o = c.getDuelingClanWarClient();
		if (o == null) {
			return;
		}
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6669);
		c.getOutStream().writeShort(c.getClanWarPlayerStake().toArray().length);
		int current = 0;
		for (GameItem item : c.getClanWarPlayerStake()) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0) {
				item.setId(Config.ITEM_LIMIT);
			}
			c.getOutStream().write3Byte(item.getId() + 1);

			current++;
		}

		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getOutStream().writeByte(1);
				c.getOutStream().write3Byte(-1);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();

		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6670);
		c.getOutStream().writeShort(o.getClanWarPlayerStake().toArray().length);
		current = 0;
		for (GameItem item : o.getClanWarPlayerStake()) {
			if (item.getAmount() > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(item.getAmount());
			} else {
				c.getOutStream().writeByte(item.getAmount());
			}
			if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0) {
				item.setId(Config.ITEM_LIMIT);
			}
			c.getOutStream().write3Byte(item.getId() + 1);
			current++;
		}

		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getOutStream().writeByte(1);
				c.getOutStream().write3Byte(-1);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public static void removeStakedItem(Client c, int itemId, int itemSlot,
			int amount, int interfaceId) {

		if (c.interfaceIdOpenMainScreen != 6575) {
			c.sendMessage("Error 3838.");
			return;
		}

		if (c.getDuelingClanWarClient() == null) { // null?
			System.out.println(
					"Challenger is nulled??????????????????????????????????????????????????????????????????????");
			return;
		}

		changedDuelRulesOrStake(c, c.getDuelingClanWarClient());

		ArrayList<GameItem> cleanUpStakes = new ArrayList<>();

		for (GameItem item : c.getClanWarPlayerStake()) {
			if (amount <= 0) {
				break;
			}
			if (item.getAmount() <= 0) {
				break;
			}
			if (!ItemProjectInsanity.itemIsStackable(itemId)) { // if item does not stack, then
													// loop for amount till we
													// runout of item
				item.setAmount(0);
				cleanUpStakes.add(item);
				c.getItems().addItem(item.getId(), 1);
				amount--;
				continue;
			} else { // check stackable items
				if (amount == 28) {
					amount = item.getAmount();
				}
				if (amount > item.getAmount()) {
					amount = item.getAmount();
				}
				c.getItems().addItem(item.getId(), amount);
				item.setAmount(item.getAmount() - amount);
				if (item.getAmount() <= 0) {
					cleanUpStakes.add(item);
				}
				break;
			}
		}

		if (!cleanUpStakes.isEmpty()) {
			for (GameItem item : cleanUpStakes) {
				c.getClanWarPlayerStake().remove(item);
			}
		}

		cleanUpStakes.clear();

		refreshDuelScreen(c);
		refreshDuelScreen(c.getDuelingClanWarClient());
		c.setUpdateInvInterface(3322);

	}

	public static void resetClanDuelStuff(Client c) {
		if (c.clanWarOpponent != -1) {
			c.clanWarOpponent = -1;
		}
		if (c.chalAccepted) {
			c.chalAccepted = false;
		}
		c.clanWarRule = new boolean[11];
		c.duelOption = 0;
	}

	public static void rewardWinningClanWithStake(ClanWar clanWar, Clan clan) {
		if (!clanWar.getClanWarStakes().isEmpty()) {
			clan.getClanWarLoot().addAll(clanWar.getClanWarStakes());
			clanWar.getClanWarStakes().clear();
		}
	}

	public static void selectRule(Client c, int i) {
		if (c.getDuelingClanWarClient() == null || c.getClan() == null) {
			return;
		}

		if (c.interfaceIdOpenMainScreen != InterfaceIds.DUEL_STAKE_SCREEN) {
			c.getPA().closeAllWindows();
			return;
		}

		changedDuelRulesOrStake(c, c.getDuelingClanWarClient());

		if (!c.clanWarRule[i]) {
			c.clanWarRule[i] = true;
			c.duelOption += PlayerConstants.DUEL_RULE_ID[i];
		} else {
			c.clanWarRule[i] = false;
			c.duelOption -= PlayerConstants.DUEL_RULE_ID[i];
		}

		c.getPacketSender().sendFrame87(286, c.duelOption);
		c.getDuelingClanWarClient().duelOption = c.duelOption;
		c.getDuelingClanWarClient().clanWarRule[i] = c.clanWarRule[i];
		c.getDuelingClanWarClient().getPacketSender().sendFrame87(286,
				c.getDuelingClanWarClient().duelOption);
	}

	public static void stakeItem(Client c, int itemId, int itemSlot, int amount,
			int interfaceId) {

		if (c.interfaceIdOpenMainScreen != 6575) {
			c.sendMessage("Error 3838.");
			return;
		}

		if (!DBConfig.CLAN_STAKING) {
			c.sendMessage("Staking in Clan Wars has been disabled for now.");
			return;
		}

		if (c.getDuelingClanWarClient() == null) { // null?
			System.out.println(
					"Challenger is nulled??????????????????????????????????????????????????????????????????????");
			return;
		}

		if (!canStake(c, c.getDuelingClanWarClient(), itemId, itemSlot)) {
			// c.sendMessage("can't stake?");
			return;
		}

		ItemDef def = ItemDef.forID(itemId);

		if (def == null) {
			c.sendMessage("Error 82673. Report to Developers.");
			return;
		}

		changedDuelRulesOrStake(c, c.getDuelingClanWarClient());

		if (!ItemProjectInsanity.itemIsStackable(itemId)) { // if item does not stack, then
												// loop for amount till we
												// runout of item
			for (int i = 0; i < amount; i++) {
				if (!c.getItems().playerHasItem(itemId, 1)) {
					break;
				}
				if (c.getItems().deleteItem2(itemId, 1)) {
					c.getClanWarPlayerStake().add(new GameItem(itemId, 1));

					refreshDuelScreen(c);
					refreshDuelScreen(c.getDuelingClanWarClient());
					c.setUpdateInvInterface(3322);
				}
			}
		} else { // item is stackable, so put in the amount or amount leftover
					// in stack.
			int totalAmount = c.getItems().getItemCount(itemId);
			if (amount > totalAmount) {
				amount = totalAmount;
			}
			boolean containsItemStackable = false;
			for (GameItem item : c.getClanWarPlayerStake()) {
				if (item.getId() == itemId) {
					if (c.getItems().deleteItemInOneSlot(itemId, itemSlot,
							amount)) {
						item.setAmount(item.getAmount() + amount);
						refreshDuelScreen(c);
						refreshDuelScreen(c.getDuelingClanWarClient());
						c.setUpdateInvInterface(3322);
					}
					containsItemStackable = true;
					break;
				}
			}
			if (!containsItemStackable) {
				if (c.getItems().deleteItemInOneSlot(itemId, itemSlot,
						amount)) {
					c.getClanWarPlayerStake().add(new GameItem(itemId, amount));

					refreshDuelScreen(c);
					refreshDuelScreen(c.getDuelingClanWarClient());
					c.setUpdateInvInterface(3322);
				}
			}
		}

	}

}
