package com.soulplay.content.minigames.clanwars;

public class CWRules {

	public static final int ITEMS_LOST_ON_DEATH = 0;

	public static final int SINGLE_COMBAT = 1;

	public static final int NO_RANGED = 2;

	public static final int NO_MELEE = 3;

	public static final int NO_MAGIC = 4;

	public static final int NO_DRINKS = 5;

	public static final int NO_FOOD = 6;

	public static final int NO_PRAYER = 7;

	public static final int NO_SPIRITSHIELD = 8;

	public static final int NO_SUMMONING = 9;

	public static final int NO_SPECIAL_ATK = 10;

}
