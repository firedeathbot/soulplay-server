package com.soulplay.content.minigames.clanwars;

import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.game.event.Task;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;
import com.soulplay.util.interfacedata.InterfaceIds;
import com.soulplay.util.sql.clanelo.ClanEloHandler;

public class ClanWarHandler {

	public static ClanWar[] clanWars = new ClanWar[100];

	public static int MIN_PLAYERS_FOR_RANKED = 3;

	public static void acceptedRules(Client c, ClanWar clanWar) {
		if (clanWar == null) {
			c.sendMessage("Error code: 28390");
			return;
		}
		if (c.hasHybridGear()) {
			c.sendMessage(
					"You cannot enter the war with the Hybrid Armor Pieces.");
			return;
		}
		if (clanWar.getClanWarRules()[CWRules.NO_SUMMONING]
				&& c.summoned != null) {
			c.sendMessage(
					"Summoning is disabled in this war. Dismiss your familiar before entering.");
			return;
		}

		if (clanWar.getClanWarRules()[CWRules.NO_SPIRITSHIELD]
				&& hasSpiritShield(c)) {
			c.sendMessage("You may not bring Spirit Shields into this war.");
			return;
		}

		int x = 0;
		int y = 0;
		int z = clanWar.getHeight();
		if (clanWar.getWallTime() <= 0) {
			c.sendMessage("The clan fight has already begun.");
			return;
		}
		if (clanWar.deadPlayers.contains(c)) {
			clanWar.deadPlayers.remove(c);
		}
		if (c.getClan() == clanWar.getClan1Clan()) {
			x = ClanWarConstants.CLAN1X;
			y = ClanWarConstants.CLAN1Y;
			if (!clanWar.clan1Players.contains(c)) {
				clanWar.clan1Players.add(c);
				addCapes(c, 4371);
			}
		}
		if (c.getClan() == clanWar.getClan2Clan()) {
			x = ClanWarConstants.CLAN2X;
			y = ClanWarConstants.CLAN2Y;
			if (!clanWar.clan2Players.contains(c)) {
				clanWar.clan2Players.add(c);
				addCapes(c, 4411);
			}
		}
		c.clanWarRule = clanWar.getClanWarRules().clone();
		enterMatch(c, x, y, z);
	}

	public static void addCapes(Client c, int capeId) {
		c.displayEquipment[PlayerConstants.playerCape] = capeId;
	}

	public static void challengeClicked(Client c) {
		Client c2 = (Client) PlayerHandler.players[c.clanWarOpponent];
		if (c2 == null) {
			c.sendMessage("Error 2816. Please report to Staff.");
			return;
		}

		if (true) {
			c.sendMessage("Clan war is disabled.");
			c2.sendMessage("Clan war is disabled.");
			return;
		}

		c.setDuelingClientClanWar(c2);
		c2.setDuelingClientClanWar(c);

		// open duel screen
		ClanDuel.openDuelingScreen(c, c2);

		c.chalInterface = c2.chalInterface = true;

		c.sendMessage(
				"<col=800000>Stake at your own risk. Clan Wars is in Beta stage.");
		c2.sendMessage(
				"<col=800000>Stake at your own risk. Clan Wars is in Beta stage.");
	}

	public static void enterMatch(Client c, int x, int y, int z) {
		c.getPA().movePlayer(x, y, z);
		c.getPA().closeAllWindows();
		c.getDamageMap().reset();
		if (c.clanWarRule[CWRules.ITEMS_LOST_ON_DEATH]) {
			c.setSkullTimer(Config.SKULL_TIMER);
			c.setSkull(true);
		}
	}

	public static boolean gameOver(ClanWar cw) {
		if (cw.getWallTime() > 0) {
			return false;
		}

		if (cw.getClan1Clan().isLockedExitPortals()) {
			cw.getClan1Clan().setLockedExitPortals(false);
		}
		if (cw.getClan2Clan().isLockedExitPortals()) {
			cw.getClan2Clan().setLockedExitPortals(false);
		}

		int team1 = 0;
		for (Client c1 : cw.clan1Players) {
			if (c1 != null && !c1.disconnected) {
				if (cw.deadPlayers.contains(c1)) {
					continue;
				}
				team1++;
			}
		}

		if (team1 == 0) {
			if (cw.isRankedGame()) {
				ClanEloHandler.updateClanElo(cw.getClan2Founder().toLowerCase(),
						cw.getClan1Founder().toLowerCase());
			}
			ClanDuel.rewardWinningClanWithStake(cw, cw.getClan2Clan());
			handleWin("clan2", cw);
			return true;
		}

		int team2 = 0;
		for (Client c1 : cw.clan2Players) {
			if (c1 != null && !c1.disconnected) {
				if (cw.deadPlayers.contains(c1)) {
					continue;
				}
				team2++;
			}
		}

		if (team2 == 0) {
			if (cw.isRankedGame()) {
				ClanEloHandler.updateClanElo(cw.getClan1Founder().toLowerCase(),
						cw.getClan2Founder().toLowerCase());
			}
			ClanDuel.rewardWinningClanWithStake(cw, cw.getClan1Clan());
			handleWin("clan1", cw);
			return true;
		}

		return false;
	}

	public static boolean handleButtons(Client c, int actionButtonId) {

		if (actionButtonId == 26069 || actionButtonId == 26042) { // no ranged
			ClanDuel.selectRule(c, CWRules.NO_RANGED);
			return true;
		}
		if (actionButtonId == 26070 || actionButtonId == 26043) { // no melee
			ClanDuel.selectRule(c, CWRules.NO_MELEE);
			return true;
		}
		if (actionButtonId == 26071 || actionButtonId == 26041) { // no magic
			ClanDuel.selectRule(c, CWRules.NO_MAGIC);
			return true;
		}
		if (actionButtonId == 30136 || actionButtonId == 30137) { // no sp atk
			ClanDuel.selectRule(c, CWRules.NO_SPECIAL_ATK);
			return true;
		}
		if (actionButtonId == 2158 || actionButtonId == 2157) { // no summoning
			ClanDuel.selectRule(c, CWRules.NO_SUMMONING);
			return true;
		}
		if (actionButtonId == 26065 || actionButtonId == 26040) { // items lost
																	// on death
			ClanDuel.selectRule(c, CWRules.ITEMS_LOST_ON_DEATH);
			return true;
		}
		if (actionButtonId == 26072 || actionButtonId == 26045) { // no drinks
			ClanDuel.selectRule(c, CWRules.NO_DRINKS);
			return true;
		}
		if (actionButtonId == 26073 || actionButtonId == 26046) { // no food
			ClanDuel.selectRule(c, CWRules.NO_FOOD);
			return true;
		}
		if (actionButtonId == 26074 || actionButtonId == 26047) { // no prayer
			ClanDuel.selectRule(c, CWRules.NO_PRAYER);
			return true;
		}
		if (actionButtonId == 26066 || actionButtonId == 26048) { // single
																	// combat
			ClanDuel.selectRule(c, CWRules.SINGLE_COMBAT);
			return true;
		}
		if (actionButtonId == 26076 || actionButtonId == 26075) { // no spirit
																	// shield
			ClanDuel.selectRule(c, CWRules.NO_SPIRITSHIELD);
			return true;
		}

		if (actionButtonId == 26018) { // accept button

			if (c.interfaceIdOpenMainScreen != InterfaceIds.DUEL_STAKE_SCREEN) {
				c.getPA().closeAllWindows();
				return true;
			}

			if (c.getClan() == null) {
				return true;
			}

			if (c.getClan().getClanWarId() != -1) { // clan war is in session,
													// show rules and then they
													// must accept to join war
				acceptedRules(c, clanWars[c.getClan().getClanWarId()]);
				return true;
			}

			if (c.getDuelRuleTimer() > 0) {
				c.sendMessage(
						"Please wait 5 seconds after changing rules to accept.");
				return true;
			}

			if (c.clanWarRule[CWRules.NO_RANGED]
					&& c.clanWarRule[CWRules.NO_MELEE]
					&& c.clanWarRule[CWRules.NO_MAGIC]) {
				c.sendMessage(
						"You won't be able to attack the player with the rules you have set.");
				return true;
			}

			if (c.getDuelingClanWarClient() == null) {
				c.getPA().closeAllWindows();
				return true;
			}

			c.chalAccepted = true;

			if (c.chalAccepted) {
				c.getPacketSender().sendFrame126("Waiting for other player...",
						6684);
				c.getDuelingClanWarClient().getPacketSender()
						.sendFrame126("Other player has accepted.", 6684);
			}
			if (c.getDuelingClanWarClient().chalAccepted) {
				c.getDuelingClanWarClient().getPacketSender()
						.sendFrame126("Waiting for other player...", 6684);
				c.getPacketSender().sendFrame126("Other player has accepted.",
						6684);
			}

			if (c.chalAccepted && c.getDuelingClanWarClient().chalAccepted) { // both
																				// accepted
																				// so
																				// start
																				// match
				c.getDuelingClanWarClient().setUpdateInvInterface(3214);
				startMatch(c, c.getDuelingClanWarClient());
				c.setUpdateInvInterface(3214);
			}

			return true;
		}
		return false;
	}

	public static void handleDeath(Client c) {
		if (c != null && c.getClan() != null
				&& c.getClan().getClanWarId() != -1) {
			ClanWar clanWar = clanWars[c.getClan().getClanWarId()];
			if (clanWar != null) {
				if (clanWar.deadPlayers.contains(c)) {
					c.sendMessage("Tell a moderator about this22.");
					return;
				}
				if (clanWar.clan1Players.contains(c)) {
					clanWar.setClan2Kills(clanWar.getClan2Kills() + 1);
					movePlayerOnDeath(c, true, clanWar.getHeight());
				} else {
					clanWar.setClan1Kills(clanWar.getClan1Kills() + 1);
					movePlayerOnDeath(c, false, clanWar.getHeight());
				}
				clanWar.deadPlayers.add(c);
			}
		}
	}

	public static void handleKill(Client c, Client c2) {
		ClanWar clanWar = clanWars[c.getClan().getClanWarId()];
		if (clanWar == null) {
			c.sendMessage("clan war null");
		}
		if (clanWar.clan2Players.contains(c)) {
			clanWar.setClan1Kills(clanWar.getClan1Kills() + 1);
			/*
			 * if(clanWar.getClan1Kills() >= MAX_KILLS) {
			 * c.sendMessage("called"); handleWin("clan1", clanWar); }
			 */
		} else if (clanWar.clan1Players.contains(c)) {
			clanWar.setClan2Kills(clanWar.getClan2Kills() + 1);
			/*
			 * if(clanWar.getClan2Kills() >= MAX_KILLS) {
			 * c.sendMessage("called"); handleWin("clan2", clanWar); }
			 */
		}
	}

	public static boolean handleObjects(Client c, GameObject o) {
		if (o.getId() == 4130 && o.getX() == 3374 && o.getY() == 3159) {
			ClanDuel.lootClanChest(c);
			return true;
		}
		if (o.getId() == 28213) {
			if (c.getClan() == null) {
				c.sendMessage("You are not in any clan.");
				return true;
			}
			if (c.getClan().getClanWarId() == -1) {
				c.sendMessage("There is no Clan War started yet.");
				return true;
			}
		}
		if (o.getId() == 26727/*28214*/ || o.getId() == 26738/*28140*/) { // leave portal, 28214
														// is main fight area
														// portal
			if (o.getId() == /*28214*/26727) { //exit ongoing game portal
				if (c.clanWarRule[CWRules.ITEMS_LOST_ON_DEATH]
						&& c.getClan() != null
						&& c.getClan().isLockedExitPortals()) {
					c.sendMessage(
							"The walls are down so the portal is disabled now.");
					return true;
				}
			}
			if (c.getClan() != null && c.getClan().getClanWarId() == -1) {
				removePlayer(c, null);
			} else {
				removePlayer(c, clanWars[c.getClan().getClanWarId()]);
			}
			return true;
		}
		if (c.getClan() == null || c.getClan().getClanWarId() == -1) {
			return false;
		}
		ClanWar clanWar = clanWars[c.getClan().getClanWarId()];
		switch (o.getId()) {
			case 38697: // leave spectator wall object
				moveToClanWarsLobby(c); // c.getPA().movePlayer(DEATHX
													// + Misc.random(3), DEATHY
													// + Misc.random(3), 0);
				if (clanWar.deadPlayers.contains(c)) {
					clanWar.deadPlayers.remove(c);
				}
				// removePlayer(c, clanWar);
				return true;
				
//			case 28213: // enter clan wars
//
//				if (c.getClan() == null || c.getClan().getClanWarId() == -1) {
//					c.sendMessage("You are not in a Clan.");
//					return true;
//				}
//
//				showRules(c, clanWar);
//				return true;

			case 26741://28194: // viewing orb
				c.getPA().movePlayer(/*3316*/9842, 4735, c.getZ());
				c.spectatorMode = true;
				c.hidePlayer = true;
				return true;

		}
		return false;
	}
	
	public static void clickedPurplePortal(Player player) {
		if (player.getClan() == null || player.getClan().getClanWarId() == -1) {
			return;
		}
		
		ClanWar clanWar = ClanWarHandler.clanWars[player.getClan().getClanWarId()];
		
		if (player.getClan() == null || player.getClan().getClanWarId() == -1) {
			player.sendMessage("You are not in a Clan.");
			return;
		}

		ClanWarHandler.showRules(player, clanWar);
	}

	public static void handleWin(String clan, ClanWar c) {
		final int CLAN_WIN_POINTS = 350;
		final int CLAN_LOSE_POINTS = 80;
		if (clan.equals("clan1")) {
			for (Client clan1 : c.clan1Players) {
				if (clan1 == null || clan1.disconnected) {
					continue;
				}
				clan1.sendMessage("Congratulations, you have defeated "
						+ c.getClan2Name());
				if (clan1.getClan() != null) {
					clan1.getClan().addClanPoints(clan1, CLAN_WIN_POINTS,
							"clw_win");
					// clan1.getPA().moveToClanWarsLobby();
					// //clan1.getPA().movePlayer(DEATHX, DEATHY, 0);
					// if (clan1.getClan() != null &&
					// clan1.getClan().getClanWarId()
					// != -1)
					// clan1.getClan().setClanWarId(-1);
				}
			}
			for (Client clan2 : c.clan2Players) {
				if (clan2 == null || clan2.disconnected) {
					continue;
				}
				clan2.sendMessage("You were defeated by " + c.getClan1Name());
				if (clan2.getClan() != null) {
					clan2.getClan().addClanPoints(clan2, CLAN_LOSE_POINTS,
							"clw_lose");
					// clan2.getPA().moveToClanWarsLobby();
					// //getPA().movePlayer(DEATHX, DEATHY, 0);
					// if (clan2.getClan() != null &&
					// clan2.getClan().getClanWarId()
					// != -1)
					// clan2.getClan().setClanWarId(-1);
				}
			}
		} else if (clan.equals("clan2")) {
			for (Client clan2 : c.clan2Players) {
				if (clan2 == null || clan2.disconnected) {
					continue;
				}
				clan2.sendMessage("Congratulations, you have defeated "
						+ c.getClan1Name());
				if (clan2.getClan() != null) {
					clan2.getClan().addClanPoints(clan2, CLAN_WIN_POINTS,
							"clw_win");
					// clan2.getPA().moveToClanWarsLobby();
					// //getPA().movePlayer(DEATHX, DEATHY, 0);
					// if (clan2.getClan() != null &&
					// clan2.getClan().getClanWarId()
					// != -1)
					// clan2.getClan().setClanWarId(-1);
				}
			}
			for (Client clan1 : c.clan1Players) {
				if (clan1 == null || clan1.disconnected) {
					continue;
				}
				clan1.sendMessage("You were defeated by " + c.getClan2Name());
				if (clan1.getClan() != null) {
					clan1.getClan().addClanPoints(clan1, CLAN_LOSE_POINTS,
							"clw_lose");
					// clan1.getPA().moveToClanWarsLobby();
					// //getPA().movePlayer(DEATHX, DEATHY, 0);
					// if (clan1.getClan() != null &&
					// clan1.getClan().getClanWarId()
					// != -1)
					// clan1.getClan().setClanWarId(-1);
				}
			}
		}
		c = null;
	}

	public static boolean hasSpiritShield(Client c) {
		if (c.playerEquipment[PlayerConstants.playerShield] == 13740
				|| c.playerEquipment[PlayerConstants.playerShield] == 13742) {
			return true;
		}
		if (c.getItems().playerHasItem(13740)
				|| c.getItems().playerHasItem(13742)) {
			return true;
		}
		return false;
	}

	public static void kickAllPlayersAndNull(final int clanWarId) {
		try {
			if (clanWars[clanWarId] == null) {
				return;
			}

			clanWars[clanWarId].getClan1Clan().setClanWarId(-1);
			clanWars[clanWarId].getClan2Clan().setClanWarId(-1);

			// refresh team1
			for (Client c1 : clanWars[clanWarId].clan1Players) {
				if (c1 != null && !c1.disconnected) {
					// TODO: add clan1 interface details here
					moveToClanWarsLobby(c1);
				}
			}

			// refresh team2
			for (Client c1 : clanWars[clanWarId].clan2Players) {
				if (c1 != null && !c1.disconnected) {
					// TODO: add clan2 interface details here
					moveToClanWarsLobby(c1);
				}
			}

			// refresh deadplayers
			for (Client c1 : clanWars[clanWarId].deadPlayers) {
				if (c1 != null && !c1.disconnected) {
					// TODO: add deadplayers interface details here
					moveToClanWarsLobby(c1);
				}
			}

			clanWars[clanWarId] = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int openSlot() {
		for (int j = 0; j < clanWars.length; j++) {
			if (clanWars[j] == null || clanWars[j].getId() == -1) {
				return j;
			}
		}
		return -1;
	}

	public static void removeCapes(Client c) {
		if (c.displayEquipment[PlayerConstants.playerCape] > 0) {
			c.displayEquipment[PlayerConstants.playerCape] = -1;

		}
	}

	public static void removePlayer(Client c, ClanWar c2) {
		if (c2 != null) {
			if (c2.clan1Players.contains(c)) {
				c2.clan1Players.remove(c);
			} else if (c2.clan2Players.contains(c)) {
				c2.clan2Players.remove(c);
			} else if (c2.deadPlayers.contains(c)) {
				c2.deadPlayers.remove(c);
			}
		}
		if (c.inClanWars()) {
			moveToClanWarsLobby(c);// c.getPA().movePlayer(DEATHX,
											// DEATHY, 0);
		}
		removeCapes(c);
		ClanDuel.resetClanDuelStuff(c);
	}

	public static void requestChallenge(Client c,
			Client c2/* int otherPlayerId */) throws NullPointerException {
		if (c == null || c2 == null) {
			System.out.println("[Clan Wars] An error has occured");
			return;
		}
//		if (Config.NODE_ID == 1 && !Config.isOwner(c)) {
//			c.sendMessage("Clan wars are only available on World 2 and 5.");
//			return;
//		}
		if (c.getClan() == null) {
			return;
		}
		// Client c2 = (Client) PlayerHandler.players[otherPlayerId];
		if (c2.getClan() == null) {
			return;
		}
		if (ClanWarConstants.OWNERS_ONLY) {
			if (!c.getClan().isGeneralPlusRank(c.getName())) {
				c.sendMessage(
						"You are not high enough rank to start clan wars. Must be General or higher.");
				return;
			}
			if (!c2.getClan().isGeneralPlusRank(c2.getName())) {
				c.sendMessage(
						"This player is not a high enough rank to accept matches.");
				return;
			}
		}

		if (!c.getClan().getClanWarLoot().isEmpty()) {
			c.sendMessage(
					"Leaders must empty the clan war loot chest before starting Clan Wars.");
			return;
		}

		if (c.getClan() == c2.getClan()) {
			c.sendMessage(
					"You cannot challenge someone in your Clan Chat to a Clan War.");
			return;
		}

		if (c.getClan().getClanWarId() != -1) {
			c.sendMessage("Your clan is already in a war.");
			return;
		}

		if (c2.getClan().getClanWarId() != -1) {
			c.sendMessage("Their clan is already in a war.");
			return;
		}

		// if(c.clanId <= 0) {
		// c.sendMessage("You must be in a Clan Chat to start a Clan War.");
		// return;
		// }
		// if(c2.clanId <= 0) {
		// c.sendMessage("This player is not in a Clan Chat.");
		// return;
		// }

		if (c.getDuelingClanWarClient() != null) {
			ClanDuel.declineStake(c, c.getDuelingClanWarClient());
		}

		if (c2.getDuelingClanWarClient() != null) {
			c.sendMessage("This player is currently busy.");
			return;
		}
		if (c.inClanWarsLobby() && c2.inClanWarsLobby()) {
			if (c.interfaceIdOpenMainScreen != 0) {
				c.sendMessage("Closed request window.");
				c.clanWarOpponent = 0;
				c.getPA().closeAllWindows();
				return;
			}
			if (c2.interfaceIdOpenMainScreen != 0) {
				c.sendMessage("That player is currently busy.");
				return;
			}
			c2.clanWarOpponent = c.getId();
			c.clanWarOpponent = c2.getId();
        	c2.getPacketSender().sendMessage(ChatMessageTypes.CHALLENGE_REQUEST, "wishes to fight your clan.", c.getNameSmartUp());
			c.sendMessage("Sending " + c2.getNameSmartUp() + " challenge request.");
			int[] closestLoc = c.getPA().getClosestXY(c2.getX(), c2.getY());
			c.getPA().playerWalk(closestLoc[0], closestLoc[1]);
		}
	}

	public static void sendInterfaces(Client c, ClanWar clan) {
		if (!wallDown(clan)) {
			c.getPA().walkableInterface(31500);

			c.getPacketSender().sendString(clan.getClan1Name(), 31502);
			c.getPacketSender()
					.sendString("Players: " + clan.clan1Players.size(), 31505);

			c.getPacketSender().sendString(clan.getClan2Name(), 31503);
			c.getPacketSender().sendString(
					"Players: " + clan.clan2Players.size() + "", 31506);

			c.getPacketSender()
					.sendString((int) Misc.ticksIntoSeconds(clan.getWallTime())
							+ " seconds.", 31507);
		} else { // wall is down, so fight
			c.getPA().walkableInterface(32000);

			c.getPacketSender().sendString(clan.getClan1Name(), 32002);
			c.getPacketSender().sendString(
					"Players: " + clan.clan1Players.size() + "", 32005);

			c.getPacketSender().sendString(clan.getClan2Name(), 32003);
			c.getPacketSender().sendString(
					"Players: " + clan.clan2Players.size() + "", 32006);

			c.getPacketSender()
					.sendString("Kills: " + clan.getClan1Kills() + "", 32004);// kills1
			c.getPacketSender()
					.sendString("Kills: " + clan.getClan2Kills() + "", 32007);// kills2
		}
	}

	private static void showRules(Player c, ClanWar clanWar) {
		c.getPacketSender().sendFrame87(286, clanWar.getDuelConfig());
		c.getPacketSender().sendFrame126b("Challenging clan: "
				+ (c.getClan().getFounder().equals(clanWar.getClan1Founder())
						? clanWar.getClan2Name()
						: clanWar.getClan1Name()),
				6671);
		c.getPacketSender().showInterface(6575);
		c.getPacketSender()
				.sendFrame126("<col=ffffff>Enter the War with these Rules set?", 6684);
	}

	public static void spawnWalls(/* ClanWar c, */int id, int height) {
		// if(height == c.getHeight()) {
		for (int i = /*3269*/9795; i <= /*3317*/9845; i++) {
			ObjectManager.addObject(new BarrierRSObject(id, i, /*3776*/4736, height, 0, 10, -1,
					ClanWarConstants.START_COUNTDOWN));
		}
//		for (int i = /*3269*/9795; i <= /*3317*/9845; i++) {
//			ObjectManager.addObject(new BarrierRSObject(id, i, /*3775*/4735, height, 2, 10, -1,
//					ClanWarConstants.START_COUNTDOWN));
//		}
		// }
		// System.out.println("Spawning walls at height: "+c.getHeight());
	}

	public static void startClanWarFight(ClanWar cw) {
		if (cw.getClanWarRules()[CWRules.ITEMS_LOST_ON_DEATH]) {
			cw.getClan1Clan().setLockedExitPortals(true);
			cw.getClan2Clan().setLockedExitPortals(true);
		}
		if (cw.clan1Players.size() > MIN_PLAYERS_FOR_RANKED
				&& cw.clan2Players.size() > MIN_PLAYERS_FOR_RANKED) {
			cw.setRankedGame(true);
		}
	}

	// public static void resetChallengeVariables(Client c) {
	// Client c2 = (Client) PlayerHandler.players[c.clanWarOpponent];
	// if(c != null && c2 != null) {
	// c2.chalAccepted = false;
	// c.chalAccepted = false;
	// c2.chalInterface = false;
	// c.chalInterface = false;
	// }
	// }

	// public static void checkLeave(Client c, ClanWar c2) {
	// if (c.getClan().isFounder(c.getName())) {
	//// if(Server.clanChat.clans[c.clanId].owner.equalsIgnoreCase(c.getName()))
	// {
	// if (c.clanWarOpponent > 0 && PlayerHandler.players[c.clanWarOpponent] !=
	// null)
	// PlayerHandler.players[c.clanWarOpponent].getClan().sendClanMessage("The
	// other team's leader has forfeited.");
	// c.getClan().sendClanMessage("Your owner left the Clan Chat. The match was
	// forfeited.");
	//// if(c2.clan1Players.contains(c))
	//// handleWin("clan2", c2);
	//// else if(c2.clan2Players.contains(c))
	//// handleWin("clan1", c2);
	// }
	// removePlayer(c, c2);
	// }

	public static void startMatch(Client c, Client c2) {
		if (c.disconnected || c2 == null || c2.disconnected) {
			c.sendMessage("error clan wars 45283");
			return;
		}
		int clanWarId = openSlot();
		if (clanWarId == -1) {
			c.sendMessage("Error starting clan war. Error 82099");
			c2.sendMessage("Error starting clan war. Error 82099");
			return;
		}
		clanWars[clanWarId] = new ClanWar(c.getId(), c2.getId(),
				((c.getId() + c2.getId()) * 4), clanWarId,
				ClanWarConstants.START_COUNTDOWN, c.getClan(), c2.getClan());

		c.getClan().setClanWarId(clanWarId);
		c2.getClan().setClanWarId(clanWarId);

		c.getClan().setClanWarRules(c.clanWarRule.clone());
		c2.getClan().setClanWarRules(c.clanWarRule.clone());

		clanWars[clanWarId].setClanWarRules(c.clanWarRule.clone());
		clanWars[clanWarId].setDuelConfig(c.duelOption);

		ClanDuel.insertStakeItems(c, c2, clanWars[clanWarId]);

		// for (int pp = 0; pp < PlayerHandler.players.length; pp++) {
		// if (PlayerHandler.players[pp] == null ||
		// PlayerHandler.players[pp].getClan() == null) continue;
		// if (PlayerHandler.players[pp].getClan() == c.getClan()) {
		// final Client clan = (Client)PlayerHandler.players[pp];
		// clan.clanWarId = clanWarId;
		// clan.sendMessage(ClanWarConstants.MESSAGE1);
		// clan.sendMessage(ClanWarConstants.MESSAGE2);
		// clan.sendMessage(ClanWarConstants.MESSAGE3);
		// clan.clanWarId = c.clanWarId = c2.clanWarId = clanWarId;
		// }
		// if (PlayerHandler.players[pp].getClan() == c2.getClan()) {
		// final Client clan = (Client)PlayerHandler.players[pp];
		// clan.clanWarId = clanWarId;
		// clan.sendMessage(ClanWarConstants.MESSAGE1);
		// clan.sendMessage(ClanWarConstants.MESSAGE2);
		// clan.sendMessage(ClanWarConstants.MESSAGE3);
		// clan.clanWarId = c.clanWarId = c2.clanWarId = clanWarId;
		// }
		// }
		if (c != null) {
			c.sendMessage("Clan Wars request accepted.");
			// enterMatch(c, ClanWarConstants.CLAN1X, ClanWarConstants.CLAN1Y);
			// clanWars[clanWarId].clan1Players.add(c);
		}
		if (c2 != null) {
			c2.sendMessage("Clan Wars request accepted.");
			// enterMatch(c2, ClanWarConstants.CLAN2X, ClanWarConstants.CLAN2Y);
			// clanWars[clanWarId].clan2Players.add(c2);
		}
		clanWars[clanWarId].setClan1Founder(c.getClan().getFounder());
		clanWars[clanWarId].setClan2Founder(c2.getClan().getFounder());
		clanWars[clanWarId].setClan1Name(c.getClan().getTitle());
		clanWars[clanWarId].setClan2Name(c2.getClan().getTitle());
		clanWars[clanWarId].setWallTime(ClanWarConstants.START_COUNTDOWN);

		// wallTimer(clanWars[clanWarId]);
		spawnWalls(/* clanWars[clanWarId], */ClanWarConstants.WALL_ID,
				clanWars[clanWarId].getHeight());
		startTimer(clanWarId);

		c.getClan().sendAlertToLS(ClanWarConstants.MESSAGE1);
		c.getClan().sendAlertToLS(ClanWarConstants.MESSAGE2);
		c.getClan().sendAlertToLS(ClanWarConstants.MESSAGE3);

		c2.getClan().sendAlertToLS(ClanWarConstants.MESSAGE1);
		c2.getClan().sendAlertToLS(ClanWarConstants.MESSAGE2);
		c2.getClan().sendAlertToLS(ClanWarConstants.MESSAGE3);

		c.setDuelingClientClanWar(null);
		c2.setDuelingClientClanWar(null);
		c.getPA().closeAllWindows();
		c2.getPA().closeAllWindows();

		if (c.clanWarOpponent != -1) {
			c.clanWarOpponent = -1;
		}
		if (c.chalAccepted) {
			c.chalAccepted = false;
		}
		if (c2.clanWarOpponent != -1) {
			c2.clanWarOpponent = -1;
		}
		if (c2.chalAccepted) {
			c2.chalAccepted = false;
		}

		c.clanWarRule = c2.clanWarRule = new boolean[11]; // since they are not
															// automatically put
															// in duel, reset
															// their rules.

		// c.sendMessage("Height level is gonna be:
		// "+clanWars[c.clanWarId].getHeight());
		// c2.sendMessage("Height level is gonna be:
		// "+clanWars[c.clanWarId].getHeight());
	}

	// TODO: add removal of players from array lists properly to make sure this
	// task ends eventually or set time limit to games
	public static void startTimer(final int clanWarId/* ClanWar clanWar */) {

		Server.getTaskScheduler().schedule(new Task(true) {

			int timerToCloseAll = 100;

			@Override
			protected void execute() {

				if (clanWars[clanWarId] == null) {
					kickAllPlayersAndNull(clanWarId);
					this.stop();
					return;
				}
				// if (clanWars[clanWarId].getWallTime() > 1)
				// clanWars[clanWarId].setWallTime(1);
				if (clanWars[clanWarId].getWallTime() == 0
						&& clanWars[clanWarId].clan1Players
								.isEmpty()
						&& clanWars[clanWarId].clan2Players
								.isEmpty()/*
											 * &&
											 * clanWars[clanWarId].deadPlayers.
											 * isEmpty()
											 */) {
					kickAllPlayersAndNull(clanWarId);
					this.stop();
					return;
				}

				if (!clanWars[clanWarId].isEndGame()
						&& gameOver(clanWars[clanWarId])) {
					clanWars[clanWarId].setEndGame(true);
					timerToCloseAll = 99;
				}

				// System.out.println("running task still.");
				updateInterface(clanWarId);

				if (clanWars[clanWarId].getWallTime() > 0) {
					clanWars[clanWarId]
							.setWallTime(clanWars[clanWarId].getWallTime() - 1);
					if (clanWars[clanWarId].getWallTime() == 0) {
						startClanWarFight(clanWars[clanWarId]);
					}
				}

				if (clanWars[clanWarId].getWallTime() == 0
						&& timerToCloseAll == 100
						&& (clanWars[clanWarId].clan1Players.isEmpty()
								|| clanWars[clanWarId].clan2Players
										.isEmpty())) {
					timerToCloseAll = 99;
				}

				if (timerToCloseAll <= 0) {
					kickAllPlayersAndNull(clanWarId);
					this.stop();
					return;
				}

				if (timerToCloseAll > 0 && timerToCloseAll < 100) {
					timerToCloseAll--;
				}

			}
		});

	}

	public static void updateInterface(int clanWarId) {
		try {

			if (clanWars[clanWarId] == null) {
				return;
			}
			ArrayList<Client> removedPlayers1 = new ArrayList<>();
			ArrayList<Client> removedPlayers2 = new ArrayList<>();
			// refresh team1
			for (Client c1 : clanWars[clanWarId].clan1Players) {
				if (c1 != null && !c1.disconnected) {
					// TODO: add clan1 interface details here
					sendInterfaces(c1, clanWars[clanWarId]);
				}
				if (c1.disconnected || !c1.isActive || !c1.inClanWars()) {
					removedPlayers1.add(c1);
				}
			}

			// refresh team2
			for (Client c1 : clanWars[clanWarId].clan2Players) {
				if (c1 != null && !c1.disconnected) {
					// TODO: add clan2 interface details here
					sendInterfaces(c1, clanWars[clanWarId]);
				}
				if (c1.disconnected || !c1.isActive || !c1.inClanWars()) {
					removedPlayers2.add(c1);
				}
			}

			if (removedPlayers1.size() > 0) {
				for (Client c : removedPlayers1) {
					clanWars[clanWarId].clan1Players.remove(c);
				}
			}

			if (removedPlayers2.size() > 0) {
				for (Client c : removedPlayers2) {
					clanWars[clanWarId].clan2Players.remove(c);
				}
			}

			removedPlayers1.clear();
			removedPlayers2.clear();

			// //refresh deadplayers
			// for (Client c1 : clanWars[clanWarId].deadPlayers) {
			// if (c1 != null && !c1.disconnected) {
			// //TODO: add deadplayers interface details here
			// sendInterfaces(c1, clanWars[clanWarId]);
			// }
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean wallDown(ClanWar c) {
		if (c == null) {
			return false;
		}
		return c.getWallTime() == 0;
	}
	
	private static void movePlayerOnDeath(Player p, boolean team1, int z) {
		Location spectatorLoc = team1 ? ClanWarConstants.team1Dead.getRandomTile() : ClanWarConstants.team2Dead.getRandomTile();
		p.getPA().movePlayer(spectatorLoc.getX(), spectatorLoc.getY(), z);
	}
	
	public static void moveToClanWarsLobby(Player c) {
		c.getPA().movePlayer(3368, 3169, 0);
		c.getPA().walkableInterface(-1);
		if (c.isDead()) {
			c.safeDeath = true;
		}
		c.healHitPoints(99);
	}
}
