package com.soulplay.content.minigames.clanwars;

import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;

public class ClanWarConstants {

	/**
	 * If only owners can challenge another clan if set to true ANYONE in the
	 * clan will be able to
	 */
	public static final boolean OWNERS_ONLY = true;

	/**
	 * start countdown in ticks
	 */
	public static final int START_COUNTDOWN = 200;

	/**
	 * The coords that the 1st clan spawns at
	 */
	public static final int CLAN1X = 9824, CLAN1Y = 4788;

	/**
	 * The coords that the 2nd clan spawns at
	 */
	public static final int CLAN2X = 9822, CLAN2Y = 4683;

	/**
	 * The amount of kills needed to win
	 */
	public static final int MAX_KILLS = 3;

	/**
	 * The object ID of the wall that spawns
	 */
	public static final int WALL_ID = /*28176*/26784;
	public static final int ANIMATED_WALL_ID = 26785; //OSRS OBJ

	public static final Rectangle team1Dead = new Rectangle(Location.create(9846, 4738), Location.create(9851, 4750));
	
	public static final Rectangle team2Dead = new Rectangle(Location.create(9846, 4721), Location.create(9851, 4733));

	/**
	 * X coord you spawn around when the match is over
	 */
	public static final int DEATHX = 3267;

	/**
	 * Y coord you spawn around when the match is over
	 */
	public static final int DEATHY = 3679;

	/**
	 * First message sent when challenge is accepted
	 */
	public static final String MESSAGE1 = "<col=ff0000>Your clan has been challenged to a clan war!";

	/**
	 * Second message sent when challenge is accepted
	 */
	public static final String MESSAGE2 = "<col=ff0000>Step through the purple portal in the Challenge Hall.";

	/**
	 * Third message sent when challenge is accepted
	 */
	public static final String MESSAGE3 = "<col=ff0000>Battle will commence in 2 minutes.";

}
