package com.soulplay.content.minigames.clanwars;

import com.soulplay.game.model.objects.RSObject;

public class BarrierRSObject extends RSObject {

	public BarrierRSObject(int newId, int x, int y, int height, int face, int type, int originalId, int ticks) {
		super(newId, x, y, height, face, type, originalId, ticks);
	}
	
	@Override
	public void process() {
		super.process();
		if (getTick() == 5) {
			animate(26110);
		}
	}

}
