package com.soulplay.content.minigames.taibwowannaicleanup;

import java.util.HashMap;
import java.util.Map;

public enum MacheteEnum {
	
	MACHETE(975, 1.0),
	OPAL_MACHETE(6313, 0.9),
	JADE_MACHETE(6315, 0.8),
	RED_TOPAZ_MACHETE(6317, 0.7);
	
	private final int itemId;
	private final double difficultyReduction;
	
	private MacheteEnum(int id, double dif) {
		this.itemId = id;
		this.difficultyReduction = dif;
	}

	public int getItemId() {
		return itemId;
	}

	public double getDifficultyReduction() {
		return difficultyReduction;
	}
	
	private static Map<Integer, MacheteEnum> map = new HashMap<>();
	
	public static MacheteEnum get(int id) {
		return map.get(id);
	}
	
	public static void init() {
		for (MacheteEnum m : MacheteEnum.values()) {
			map.put(m.getItemId(), m);
		}
	}

}
