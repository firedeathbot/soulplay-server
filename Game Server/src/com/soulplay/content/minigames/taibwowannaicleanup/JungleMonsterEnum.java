package com.soulplay.content.minigames.taibwowannaicleanup;

public enum JungleMonsterEnum {

	LARGE_MOSQUITO(2493, 22, 2, 20, 20, "You disturb a giant mosquito nest."),
	MOSQUITO_SWARM(2494, 24, 2, 25, 25, "You disturb a giant mosquito nest."),
	TRIBESMAN(191, 45, 4, 60, 60, "You disturb a tribesman hiding in the jungle."),
	BUSH_SNAKE(2489, 35, 4, 50, 50, "A snake strikes by surprise."),
	JUNGLER_SPIDER(1478, 35, 4, 50, 50, "A spider crawls out."),
	GREEN_BROODO(2499, 65, 5, 90, 90, "You disturb the grave of a broodoo victim - an entity appears before you"),
	YELLOW_BROODO(2501, 65, 5, 90, 90, "You disturb the grave of a broodoo victim - an entity appears before you"),
	WHITE_BROODO(2503, 65, 5, 90, 90, "You disturb the grave of a broodoo victim - an entity appears before you");
	
	private final int npcId;
	private final int hp;
	private final int maxHit;
	private final int attack;
	private final int defence;
	private final String msg;
	
	private JungleMonsterEnum(int id, int hp, int maxhit, int attack, int defence, String msg) {
		this.npcId = id;
		this.hp = hp;
		this.maxHit = maxhit;
		this.attack = attack;
		this.defence = defence;
		this.msg = msg;
		
	}
	
	public int getNpcId() {
		return npcId;
	}

	public int getHp() {
		return hp;
	}

	public int getMaxHit() {
		return maxHit;
	}

	public int getAttack() {
		return attack;
	}

	public int getDefence() {
		return defence;
	}

	public String getMsg() {
		return msg;
	}

	public static final JungleMonsterEnum[] values = JungleMonsterEnum.values();
}
