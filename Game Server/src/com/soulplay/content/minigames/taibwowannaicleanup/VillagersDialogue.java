package com.soulplay.content.minigames.taibwowannaicleanup;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;

public class VillagersDialogue {
	
	private static final int TRADING_STICK = 6306;
	public static final Location OUTSIDE_GROVE = Location.create(2816, 3083);
	
	public static void InterestingHere(Client c) {
		c.getDialogueBuilder().sendPlayerChat("Is there anything interesting to do around here?")
		.sendNpcChat("Well, I think that there's some work to be done...perhaps", "Murcaily can help you. He usually tends to the", "hardwood grove to the east of Trufitu's hut.");
	}
	
	public static void gabootyReason(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What do you do here?")
		.sendNpcChat("Not much really... I run a local shop which earns me a ", "few trading ticks, but that's about it. I keep myself to", "myself really.")
		.sendOption("What are trading sticks", ()-> {
			gabootyTradingSticks(c);
		}, "You run a shop?", ()-> {
			gabootyShop(c);
		}, "Ok thanks", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Ok, thanks.");
		});
	}
	
	public static void gabootyTradingSticks(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What are trading sticks?")
		.sendNpcChat("They're the local currency Bwana, it's used in Tai Bwo", "Wannai, there's usually some odd jobs that need doing", "around the village which you could do to earn some", "trading sticks.")
		.sendNpcChat("On, if you have something which the local villagers", "might like, you could sell it to me and I'll pay you for", "it in trading sticks.")
		.sendOption("You run a shop?", ()-> {
			gabootyShop(c);
		}, "Ok thanks", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Ok, thanks.");
		});
	}
	
	public static void whatAreTradingSticks(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What are trading sticks?")
		.sendNpcChat("They're the local currency Bwana, it's used in Tai Bwo", "Wannai, there's usually some odd jobs that need doing", "around the village which you could do to earn some", "trading sticks.")
		.sendNpcChat("On, if you have something which the local villagers", "might like, you could sell it to me and I'll pay you for", "it in trading sticks.");
	}
	
	public static void gabootyShop(Client c) {
		c.getDialogueBuilder().sendPlayerChat("You run a shop?")
		.sendNpcChat("Yes, it's the Tai Bwo coopreative... catchy name huh!", "We sell a few local village trinkets and tools. Also, there", "are a few items that we're actually looking to stock for", "the locals' sake.")
		.sendNpcChat("If you happen across any of them please bring them to", "me, I'll pay a good price for them. I'm sure you'll find", "the prices very reasonable.")
		.sendPlayerChat("Can I take a look at what you have to sell?")
		.sendAction(()-> {
			c.getShops().openShop(61);
		}).setClose(false);
	}
	
	public static void requestTradingStickReward(Client c) {
		if (c.getTaiBwoFavour() > 0) {
			if (c.getTaiBwoCleanup() < 2) {
				c.getDialogueBuilder().sendNpcChat("Yeah, you've been doing a lot of good work around",
						"here. We still need help fixing the fence", "Before you can claim rewards.");
				return;
			}
			c.getDialogueBuilder().sendPlayerChat("I've been doing some work around the village.")
					.sendNpcChat("Yeah, you've been doing a lot of good work around",
							"here. Let me give you something for your time and", "effort")
					.sendAction(() -> {
						c.getDialogueBuilder().sendItemStatement(TRADING_STICK,
								"You get " + c.getTaiBwoFavour() * 2 + " trading sticks. You are left with 0%", "favour.");
						if (c.getItems().addItem(TRADING_STICK, c.getTaiBwoFavour() * 2)) {
							c.setTaiBwoFavour(0, true);
						}
					});
		} else {
			c.getDialogueBuilder().sendNpcChat("You have 0% favours and that means you have",
					"not been doing work around the village");
		}
	}

	public static void TradingStickReward(Client c) {
		if (c.getTaiBwoFavour() > 0) {
			if (c.getTaiBwoCleanup() > 1) {
				c.getDialogueBuilder().sendNpcChat("Yeah, you've been doing a lot of good work around",
						"here. We still need help fixing the fence", "Before you can claim rewards.");
				return;
			}
			c.getDialogueBuilder().sendNpcChat("Yeah, you've been doing a lot of good work around",
					"here. Let me give you something for your time and", "effort").sendAction(() -> {
						c.getDialogueBuilder().sendItemStatement(TRADING_STICK,
								"You get " + c.getTaiBwoFavour() * 2 + " trading sticks. You are left with 0%", "favour.");
						if (c.getItems().addItem(TRADING_STICK, c.getTaiBwoFavour() * 2)) {
							c.setTaiBwoFavour(0, true);
						}
					});
		} else {
			c.getDialogueBuilder().sendNpcChat("You have 0% favours and that means you have",
					"not been doing work around the village");
		}
	}
	
	public static void hardwoodGroveDoors(Player c) {
		if (c.absX == 2816) {
			if (c.getItems().playerHasItem(TRADING_STICK, 100)) {
				c.getDialogueBuilder().sendNpcChat(2529, "Hey there, you wanna go in there to chop da special", "wood, it's gonna cost you's 100 trading sticks my", "friend.")
				.sendOption("Ok, I'll pay 100 trading sticks to enter.", () -> {
					payAndEnter(c);
				}, "Nah thanks, I'll give it a miss at the moment I think.", () -> {
					c.getDialogueBuilder().sendPlayerChat("Nah thanks, I'll give it a miss at the moment I think.")
					.sendNpcChat(2529, "Fair enough, pop back if you change your mind.");
				}).execute();
			} else {
				c.getDialogueBuilder().sendNpcChat(2529, "Hey there, you wanna go in there to chop da special", "wood, it's gonna cost you's 100 trading sticks my", "friend.")
				.sendPlayerChat("Sorry Bwana, but I don't have 100 trading sticks.")
				.sendNpcChat(2529, "In which case Bwana, I'm afraid you cannot enter.").execute();
			}
		} else if (c.absX == 2817) {
			c.getDialogueBuilder().sendStatement("You don't seem to have any Teak or Mahogany logs, are you sure", "you wish to leave?")
			.sendOption("Yes, I wanna get out of here!", ()-> {
				c.getPA().movePlayer(OUTSIDE_GROVE);
			}, "Erk! No, I want to stay in the grove.", ()-> {}).execute();
		}
	}
	
	public static void payAndEnter(Player c) {
		if (c.getItems().deleteItemInOneSlot(TRADING_STICK, 100)) {
			c.getPA().movePlayer(Location.create(2817, 3083));
		} else {
			c.sendMessage("You don't have enough trading sticks.");
		}
	}

}
