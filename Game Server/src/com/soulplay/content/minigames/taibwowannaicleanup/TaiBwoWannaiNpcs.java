package com.soulplay.content.minigames.taibwowannaicleanup;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;

public class TaiBwoWannaiNpcs {
	
	private static final int GABOOTY = 2520;
	private static final int MURCAILY = 2529;
	
	public static boolean OptionOne(Client c, NPC npc, int npcType) {

		switch(npc.npcType) {
			case GABOOTY:
				GabootyDialogue.gabootyChat(c);
				return true;
			case MURCAILY:
				MurcailyDialogue.murcailyChat(c);
				return true;
				
				
		}

		return false;
	}

}
