package com.soulplay.content.minigames.taibwowannaicleanup;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class JungleMonsters {
	
	private static final Animation SPOOK_ANIM = new Animation(2390);

	public static void jungleMonsters(Player c, Location objectLoc) {
		
		final int spawnX = c.getX(), spawnY = c.getY();
		
		final int ran = Misc.random(JungleMonsterEnum.values.length-1);
		
		final JungleMonsterEnum jm = JungleMonsterEnum.values[ran];
		
		final NPC n = NPCHandler.spawnNpc(c, jm.getNpcId(), spawnX, spawnY,
				c.heightLevel, 0, true, false);
		if (n != null)
			n.setAliveTick(200);
		
		c.sendMessage(jm.getMsg());
		
		final Direction dir = Direction.getLogicalDirection(c.getCurrentLocation(), objectLoc);
		final Direction oppositeDir = dir.getOpposite();
		
		c.startAnimation(SPOOK_ANIM);
		final ForceMovementMask mask = ForceMovementMask.createMask(oppositeDir.getStepX(), oppositeDir.getStepY(), 1, 0, dir).setNewOffset(c.getX(), c.getY());
		c.setForceMovement(mask);
		
	}
}