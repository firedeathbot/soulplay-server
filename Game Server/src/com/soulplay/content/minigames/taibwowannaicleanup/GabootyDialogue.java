package com.soulplay.content.minigames.taibwowannaicleanup;

import com.soulplay.game.model.player.Client;

public class GabootyDialogue extends VillagersDialogue {
	
public static void gabootyChat(Client c) {
		
		if (c.getTaiBwoFavour() > 499) {
			TradingStickReward(c);
		} else {
			c.getDialogueBuilder().sendOption("What do you do here?", () -> {
				gabootyReason(c);
			}, "Is there anything interesting to do here?", () -> {
				InterestingHere(c);
			}, "I've been doing some work around the village.", () -> {
				requestTradingStickReward(c);
			}, "Ok, thanks.", () -> {
				c.getDialogueBuilder().sendPlayerChat("Ok, thanks.");
			}).execute();
		}
	}

}
