package com.soulplay.content.minigames.taibwowannaicleanup;

import java.util.HashMap;
import java.util.Map;

public enum JungleEnum {

	LIGHT(9010, 9010, 10, 6281, 32, 3, 100),
	LIGHT1(9010, 9011, 10, 6281, 32, 3, 100),
	LIGHT2(9010, 9012, 10, 6281, 32, 3, 100),
	LIGHT3(9010, 9013, 10, 6281, 32, 3, 100),
	MEDIUM(9015, 9015, 20, 6283, 55, 4, 100),
	MEDIUM1(9015, 9016, 20, 6283, 55, 4, 100),
	MEDIUM2(9015, 9017, 20, 6283, 55, 4, 100),
	MEDIUM3(9015, 9018, 20, 6283, 55, 4, 100),
	DENSE(9020, 9020, 35, 6285, 80, 5, 100),
	DENSE1(9020, 9021, 35, 6285, 80, 5, 100),
	DENSE2(9020, 9022, 35, 6285, 80, 5, 100),
	DENSE3(9020, 9023, 35, 6285, 80, 5, 100),
	FENCE1(9025, 9025, 1, -1, 28, 3, 1600),
	FENCE2(9025, 9026, 1, -1, 28, 3, 1600),
    FENCE3(9025, 9027, 1, -1, 28, 3, 1600),
    FENCE4(9025, 9028, 1, -1, 28, 3, 1600),
    FENCE5(9025, 9029, 1, -1, 28, 3, 1600)
	;
	
	private final int originalObjectId;
	private final int currentId;
	private final int levelReq;
	private final int reward;
	private final int exp;
	private final int favor;
	private final int respawnTime;
	private final int stumpId;
	
	private JungleEnum(int origId, int currentId, int lvl, int reward, int exp, int favor, int respawnTime) {
		this.originalObjectId = origId;
		this.currentId = currentId;
		this.levelReq = lvl;
		this.reward = reward;
		this.exp = exp;
		this.favor = favor;
		this.respawnTime = respawnTime;
		this.stumpId = origId + 4;
	}

	public int getOriginalObjectId() {
		return originalObjectId;
	}

	public int getCurrentId() {
		return currentId;
	}

	public int getLevelReq() {
		return levelReq;
	}

	public int getReward() {
		return reward;
	}

	public int getExp() {
		return exp;
	}

	public int getFavor() {
		return favor;
	}

	public int getRespawnTime() {
		return respawnTime;
	}

	private static final Map<Integer, JungleEnum> map = new HashMap<>();
	
	public static JungleEnum get(int id) {
		return map.get(id);
	}
	
	public static void init() {
		for (JungleEnum j : JungleEnum.values()) {
			map.put(j.getCurrentId(), j);
		}
	}

	public int getStumpId() {
		return stumpId;
	}
	
}
