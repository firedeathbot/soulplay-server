package com.soulplay.content.minigames.taibwowannaicleanup;

import java.util.HashMap;
import java.util.Map;

public enum GemRocks {

	OPAL(9030, 9031, 1625),
	JADE(9031, 9032, 1627),
	RED_TOPAZ(9032, -1, 1629);
	
	private final int objectId;
	private final int nextStageObjectId;
	private final int reward;
	
	private GemRocks(int objId, int nextObjId, int reward) {
		this.objectId = objId;
		this.nextStageObjectId = nextObjId;
		this.reward = reward;
	}

	public int getObjectId() {
		return objectId;
	}

	public int getNextStageObjectId() {
		return nextStageObjectId;
	}

	public int getReward() {
		return reward;
	}
	
	private static final Map<Integer, GemRocks> map = new HashMap<>();
	
	public static GemRocks get(int id) {
		return map.get(id);
	}
	
	public static void init() {
		for (GemRocks j : GemRocks.values()) {
			map.put(j.getObjectId(), j);
		}
	}
}
