package com.soulplay.content.minigames.taibwowannaicleanup;

import com.soulplay.game.model.player.Client;

public class MurcailyDialogue extends VillagersDialogue{

	public static void murcailyChat(Client c) {
		if (c.getTaiBwoFavour() > 499) {
			TradingStickReward(c);
		} else {
			c.getDialogueBuilder().sendOption("What do you do here?", () -> {
				murcailyReason(c);
			}, "Is there anything interesting to do here?", () -> {
				if (c.getTaiBwoCleanup() < 1) {
					startCleanup(c);
					c.setTaiBwoCleanup(1);
				} else {
					c.getDialogueBuilder().sendPlayerChat("Is there anything interesting to do here?").sendNpcChat(
							"Apart from cutting back the jungle and repairing the",
							"fence, I think we've got most things covered now", "Bwana.");
				}
			}, "I've been doing some work around the village.", () -> {
				requestTradingStickReward(c);
			}, "Ok, thanks.", () -> {
				c.getDialogueBuilder().sendPlayerChat("Ok, thanks.");
			}).execute();
		}
	}
	
	public static void startCleanup(Client c) {
		c.getDialogueBuilder().sendPlayerChat("Is there anything interesting to do around here?")
		.sendNpcChat("Well Bwana, if you're stuck for something to do, you", "could always do a bit of handymanship about the village", "and cut away the jungle that lies here about.")
		.sendNpcChat("If you's is good with da machete Bwana, you's can get's", "some thatching spars and put them to use repairing the", "run-down village fence.")
		.sendOption("Do I get aything for doing this?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Do I get anything for doing this?")
			.sendNpcChat("Yes Bwana, you gain experience in woodcutting. In", "addition many members of the comunity will reward", "you with trading sticks, our local currency, for you", "efforts.")
			.sendPlayerChat("Trading sticks?")
			.sendNpcChat("Correct Bwana, trading sticks! It's our form of local", "currency! Everything you do to help out in the village", "will give you some favour, but there's a maximum limit!")
			.sendNpcChat("When you've reached one hundred percent, you cannot", "gain any more favour, you need to cash in your", "favour for trading sticks before you can increase", "favour again.")
			.sendOption("Who do I get trading sticks from?", ()-> {
				c.getDialogueBuilder().sendPlayerChat("Who do I get trading sticks from?")
				.sendNpcChat("You can talk to any villager and they will reward you", " trading sticks for your favours");
			}, "Ok, thanks", ()-> {
				c.getDialogueBuilder().sendPlayerChat("Ok, thanks.");
			});
		}, "Where can I get a machete from?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Where can I get a machete from?")
			.sendNpcChat("You can buy machete from Jiminua North West", "from the village.");
		}, "Where is this jungle?", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Where is this jungle?")
			.sendNpcChat("You can find the jungle in the village and around it");
		}, "Ok, thanks.", ()-> {
			c.getDialogueBuilder().sendPlayerChat("Ok, thanks.");
		});
	}
	
	public static void murcailyReason(Client c) {
		c.getDialogueBuilder().sendPlayerChat("What do you do here?")
		.sendNpcChat("Well, I tend to hardwood grove nearby, you can enter", "if you like and harvest some of the very fine hardwood", "that grows within.")
		.sendNpcChat("Apart from that I'm a simple Tai Bwo Wannai", "villager...I like to try and find out what's going on in", "the mainland and the rest of the known world.");
	}
}
