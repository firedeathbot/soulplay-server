package com.soulplay.content.minigames;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class FunPkTournament {

	static Player[] players = new Player[50];

	static boolean tournamentOn = false;

	static boolean gameOn = false;

	// public static String donatorHost = "";
	public static Client donatorHost = null;

	public static final int DONATORHOSTPRICE = 7; // the amount of donator
													// points required for
													// hosting the tournament.

	public static final int PLAYERS_REQUIRED = 10;

	public static int maxBonuses = 1200;

	public static final int[] tournyRewards = {13887, 13893, 13884, 13896,
			13870, 13873, 13876, 13858, 13861, 13864, 13899, 13905, 13902,
			13867, 13902, 4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724,
			4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751,
			4753, 4755, 4757, 4759, 13899, 13905, 14876, 14877, 14878, 14879,
			14880, 14881, 14882, 14883, 14884, 14885, 14886, 14887, 14888,
			14889, 14890, 14891, 14892};
	
	public static int generateReward() {
		return tournyRewards[Misc.random(tournyRewards.length - 1)];
	}

	static boolean allowFight = false;

	static int playerCount = 0;

	public static boolean allowFight() {
		return allowFight;
	}

	public static void beginDonatorHostedGame() {
		if (!tournamentOn) {
			return;
		}
		gameOn = true;
		Server.getTaskScheduler().schedule(new Task(true) {

			int countDown = 10;

			int timer = 1200; // about 10 minutes

			boolean secondTik = false;

			@Override
			public void execute() {
				if (allowFight) {
					timer--;
				}
				if (!tournamentOn) {
					endGame();
					this.stop();
					return;
				}
				refreshPlayerCount();
				if (getPlayerCount() == 0) {
					endGame();
					this.stop();
					return;
				}
				if (getPlayerCount() == 1 && allowFight) {
					rewardWinnerOfDonatorTournament();
					timer = 120;
				}
				if (countDown > 0) {
					countDown--;
				}
				if (countDown == 1) {
					allowFight = true;
				}
				secondTik = !secondTik;

				for (Player p : players) {
					if (p == null) {
						continue;
					}
					Client c = (Client) p;
					if (!c.inFunPk()) {
						forceKick(c);
						continue;
					}
					if (countDown > 1 && secondTik) {
						c.forceChat("" + (countDown / 2));
					}
					if (countDown == 1) {
						c.forceChat("FIGHT");
					}
					c.getPacketSender().sendFrame126("P:" + getPlayerCount()
							+ "-<col=ff00>" + (timer * 600 / 1000), 199);
				}
				if (timer == 300) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>3 Minutes till the FunPK Tournament is over!");
				}
				if (timer == 300) {
					collectAllPlayersToCenter();
				}
				if (timer < 0) {
					this.stop();
					if (getPlayerCount() > 1) {
						if (donatorHost != null) {
							refundDonatorHost(donatorHost);
						}
						PlayerHandler.messageAllPlayers(
								"<col=800000>There were no winners in the Fun PK tournament. LOSERS!");
					}
					endGame();
				}

			}
		});
	}

	public static void beginFunPkTourneyQueue() {
		maxBonuses = 500 + Misc.random(3000);

		PlayerHandler.messageAllPlayers(
				"<col=800000>Today's Max Gear Bonuses must be below " + maxBonuses
						+ ". ::bonuses");

		Server.getTaskScheduler().schedule(new Task(true) {

			int waitTime = 840;// 600;

			@Override
			public void execute() {
				if (tournamentOn && waitTime < 1) {
					// beginGame();
					this.stop();
					return;
				}
				if (!tournamentOn || waitTime < 1) {
					this.stop();
					return;
				}
				refreshPlayerCount();

				if (waitTime > 0) {
					waitTime--;
				}

				if (waitTime == 500) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 5 minutes!");
				}
				if (waitTime == 200) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 2 minutes!");
				}
				if (waitTime == 20) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 10 seconds!");
				}
				if (waitTime == 6) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 3 seconds!");
				}
				if (waitTime == 4) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 2 seconds!");
				}
				if (waitTime == 2) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 1 seconds!");
				}
				if (waitTime < 1) {
					if (getPlayerCount() < PLAYERS_REQUIRED) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Not enough players in the Fun PK.");
						endGame();
					} else {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fun PK Tournament has started!");
						beginGame();
					}
					this.stop();
					return;
				}

				for (Player p : players) {
					if (p == null || p.disconnected) {
						continue;
					}
					Client c = (Client) p;
					if (!c.inFunPk()) {
						forceKick(c);
						break;
					}

					// c.getPacketSender().sendFrame126("<col=ff00>"+(waitTime/2),
					// 199);
					c.getPacketSender().sendFrame126("Safe PK Tournament",
							6570);
					c.getPacketSender().sendFrame126(
							"Player Count: " + getPlayerCount(), 6572);
					c.getPacketSender().sendFrame126(
							"<col=ff00>" + (waitTime * 600 / 1000) + " seconds",
							6664);

				}

			}
		});

	}

	public static void beginGame() {
		if (!tournamentOn) {
			return;
		}
		gameOn = true;
		Server.getTaskScheduler().schedule(new Task(true) {

			int gameTime = 1200; // 840

			int countDown = 10;

			boolean secondTik = false;

			@Override
			public void execute() {
				if (!tournamentOn || gameTime < 1) {
					endGame();
					this.stop();
					return;
				}
				refreshPlayerCount();
				if (getPlayerCount() == 0) {
					endGame();
					this.stop();
					return;
				}
				if (getPlayerCount() == 1 && allowFight) {
					rewardWinner();
				}
				if (countDown > 0) {
					countDown--;
				}
				if (countDown == 1) {
					allowFight = true;
				}
				if (allowFight) {
					gameTime--;
				}

				if (gameTime == 300) {
					collectAllPlayersToCenter();
				}

				secondTik = !secondTik;

				for (Player p : players) {
					if (p == null || p.disconnected) {
						continue;
					}
					final Client c = (Client) p;
					if (!c.inFunPk()) {
						forceKick(c);
						break;
					}
					if (countDown > 1 && secondTik) {
						c.forceChat("" + (countDown / 2));
					}
					if (countDown == 1) {
						c.forceChat("FIGHT");
					}
					if (allowFight) {
						c.getPacketSender().sendFrame126(
								"<col=ff00>" + (gameTime * 600 / 1000), 199);
						if (c.underAttackBy < 1 && (c.playerIndex > 0
								&& PlayerHandler.players[c.playerIndex] != null
								&& PlayerHandler.players[c.playerIndex]
										.getFreezeTimer() > 0)) {
							if (Misc.random(75) == 7) {
								c.startGraphic(Graphic.create(2102));
								c.sendMessage(
										"You get hit by Zeus for not being under attack.");
								int damage = 10 + Misc.random(25);
								damage = c.applyDamageReduction(damage, null);
								damage = c.dealDamage(new Hit(damage, 2, 2));
							}
						}
					} else {
						c.getPacketSender().sendFrame126(
								"<col=ff00>" + (countDown * 600 / 1000), 199);
						c.getPacketSender().sendFrame126("Safe PK Tournament",
								6570);
						c.getPacketSender().sendFrame126(
								"Player Count: " + getPlayerCount(), 6572);
						c.getPacketSender().sendFrame126(" ", 6664);
					}

				}

			}
		});
	}

	public static void collectAllPlayersToCenter() {
		for (Player p : players) {
			if (p == null) {
				continue;
			}
			Client c = (Client) p;
			c.getPA().resetFollow();
			c.getCombat().resetPlayerAttack();
			c.getPA().movePlayer(3303, 3109, 0);
			c.sendMessage("Moving everyone to the beginning of the arena.");
		}
	}

	public static void donatorOpenTournament(Client c) {
		if (!DBConfig.MINI_GAMES) {
			c.sendMessage(
					"The minigames have been disabled by the admins temporarily.");
			return;
		}
		tournamentOn = true;
		donatorHost = c;// .playerName;
		PlayerHandler.messageAllPlayers(
				"<col=800000>FunPK Tournament has been opened by " + c.getNameSmartUp()
						+ "! Come on in! FunPK Tele is in PK Teleports.");
		startDonatorHostProcess();
	}

	public static void endGame() {
		players = new Player[50];
		playerCount = 0;
		tournamentOn = false;
		allowFight = false;
		gameOn = false;
		donatorHost = null;// "";
	}

	public static boolean enterGame(Client c) {
		if (!tournamentOn) {
			return false;
		}
		if (gameOn) {
			c.sendMessage("The tournament is currently running.");
			return false;
		}
		if (!MinigameRules.canJoinMinigame(c))
			return false;
		if (c.getItems().getTotalBonuses() > maxBonuses) {
			c.sendMessage("Your Total Bonuses equal up to "
					+ c.getItems().getTotalBonuses()
					+ ". Tournament max limit is at " + maxBonuses);
			return false;
		}
		for (Player player : players) {
			if (player != null && player.UUID.equals(c.UUID)
					&& !Config.isOwner(c)) {
				c.sendMessage("You already have an account inside FunPk.");
				return false;
			}
		}
		for (int i = 0; i < players.length; i++) {
			if (players[i] == null) {
				players[i] = c;
				c.sendMessage("You enter the Fun PK Tournament.");
				return true;
			}
		}
		return false;
	}

	public static boolean forceKick(Client c) {
		for (int i = 0; i < players.length; i++) {
			if (players[i] != null) {
				if (players[i] == c) {
					c.getPA().movePlayer(3303, 3121, 0);
					players[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public static int getPlayerCount() {
		return playerCount;
	}

	public static boolean isTournamentOpen() {
		return tournamentOn;
	}

	public static boolean leaveGame(Client c) {
		if (!tournamentOn) {
			return false;
		}
		if (gameOn) {
			c.sendMessage("You can not exit game right now.");
			return false;
		}
		for (int i = 0; i < players.length; i++) {
			if (players[i] != null) {
				if (players[i] == c) {
					players[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public static void openChestFromDonatorTournament(Client c) {
		c.giveDonatorChestReward();
		c.getPA().movePlayer(3303, 3123, 0);
		c.pkp += 200;
	}

	public static void openTournament() {
		if (tournamentOn) {
			return;
		}
		if (gameOn) {
			return;
		}
		tournamentOn = true;
		PlayerHandler.messageAllPlayers(
				"<col=800000>FunPK Tournament has been opened! Come on in! FunPK Tele is in PK Teleports.");
		PlayerHandler.messageAllPlayers(
				"<col=800000>You have 8 minutes to join the Fun PK Tournament! ::funpk");
		beginFunPkTourneyQueue();
	}

	public static void refreshPlayerCount() {
		int count = 0;
		// for (int i = 0; i < players.length; i++) {
		// if(players[i] != null) {
		// count++;
		// }
		// }
		for (int i = 0; i < players.length; i++) { // for(Player p : players) {
			if (players[i] == null) {
				continue;
			}
			final Client c = (Client) players[i];
			if (c == null || c.forceDisconnect || c.disconnected) {
				players[i] = null;
				continue;
			}
			// if(c != null && !c.inFunPk()) {
			//// forceKick(c);
			// c.getPA().movePlayer(3303, 3121, 0);
			// players[i] = null;
			// continue;
			// }
			count++;
		}
		playerCount = count;
	}

	public static void refundDonatorHost(Client c) {
		if (!c.isActive) {
			return;
		}
		c.setDonPoints(c.getDonPoints() + DONATORHOSTPRICE, true);
		c.sendMessage("Your donator points have been refunded.");
	}

	public static void rewardWinner() {
		allowFight = false;
		for (Player p : players) {
			if (p == null) {
				continue;
			}
			Client c = (Client) p;
			PlayerHandler.messageAllPlayers(
					"<col=800000>" + c.getNameSmartUp() + " has won the Fun PK Tournament!");
			if (c.getPlayerLevel()[3] == 0) {
				c.getPlayerLevel()[3] = 10;
			}
			c.getExtraHiscores().incFunPkTournamentWins();
			c.setDead(false);
			c.getPA().restorePlayer(true);
			c.getAchievement().winFunPKTourny();
			c.getPA().movePlayer(3304, 3108, 0);
			c.sendMessage("Open the chest to receive your reward.");
			final RSObject o = new RSObject(2403, 3304, 3109, 0, 0, 10, -1, 100);
			o.setObjectOwnerLong(c.getLongName());
			ObjectManager.addObject(o);
			c.claimedReward = false;
			break;
		}
	}

	public static void rewardWinnerOfDonatorTournament() {
		allowFight = false;
		for (Player p : players) {
			if (p == null) {
				continue;
			}
			Client c = (Client) p;
			PlayerHandler.messageAllPlayers(
					"<col=800000>" + c.getNameSmartUp() + " has won the Fun PK Tournament!");
			c.getExtraHiscores().incFunPkTournamentWins();
			c.getPA().restorePlayer(true);
			c.getPA().movePlayer(3304, 3108, 0);
			c.sendMessage("<col=800000> You have 1 minute to collect your prize!");
			c.getAchievement().winFunPKTourny();
			final RSObject o = new RSObject(2403, 3304, 3109, 0, 0, 10, -1, 100);
			o.setObjectOwnerLong(c.getLongName());
			ObjectManager.addObject(o);
			c.claimedReward = false;
			break;
		}
	}

	public static void startDonatorHostProcess() {
		PlayerHandler.messageAllPlayers(
				"<col=800000>Fun PK Tournament will start in 5 minutes!");
		Server.getTaskScheduler().schedule(new Task(true) {

			int countDown = 500; // 600 ticks is about 5 minutes

			boolean secondTik = false;

			@Override
			public void execute() {
				countDown--;
				refreshPlayerCount();
				if (countDown < 1
						&& getPlayerCount() < PLAYERS_REQUIRED) {
					this.stop();
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament requires at least "
									+ PLAYERS_REQUIRED + " players!");
					if (donatorHost != null) {
						refundDonatorHost(donatorHost);
					}
					endGame();
					return;
				}
				if (countDown == 300) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 3 minutes!");
				}
				if (countDown == 6) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 3 seconds!");
				}
				if (countDown == 4) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 2 seconds!");
				}
				if (countDown == 2) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament will start in 1 seconds!");
				}
				if (countDown < 0) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>Fun PK Tournament has started!");
					beginDonatorHostedGame();
					this.stop();
					return;
				}

				secondTik = !secondTik;
				for (Player p : players) {
					if (p == null) {
						continue;
					}
					Client c = (Client) p;
					if (!c.inFunPk()) {
						forceKick(c);
						continue;
					}
					// c.getPacketSender().sendFrame126("<col=ff00>"+(countDown/2),
					// 199);
					c.getPacketSender().sendFrame126("Safe PK Tournament",
							6570);
					c.getPacketSender().sendFrame126(
							"Player Count: " + getPlayerCount(), 6572);
					c.getPacketSender().sendFrame126(
							"Count Down " + (countDown * 600 / 1000), 6664);
				}

			}
		});

	}
	
	public static boolean inFunPKTournament(String playername) {
		for (Player p : players) {
			if (p != null && p.getName().equals(playername))
				return true;
		}
		return false;
	}

}
