package com.soulplay.content.minigames.rfd;

import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.world.instances.PlayerSingleInstance;

public class RecipeForDisaster {
	
	public static final int MAX_STAGE = 6;
	
	private static final int CULINOMANCER_ID = 3492;
	
	private static final int[][] GLOVES = { {7453}, {7454, 7455, 7456, 7457}, {7458}, {7459}, {7460}, {7461}, {7462} };
	
	private static final String[][] CULI_MESSAGES = { { "You DARE come HERE?!?!" }, {"You should not meddle in affairs that do not concern", "you!"}, { "I still have a few tricks left!" }, {"It seems I will have to deal with you myself!" } };
	
	private static final String NOOO_MSG = "NOOOooo...";
	private static final String[] NOOO_MSG2 = { "You have caused me enough grief!", "I guess I'll have to finish you off myself!" };
	
	private static String[] getRandomCullMessage(Player p) {
		switch (p.getRFDProgress()) {
		case 1:
		case 2:
		case 3:
		return CULI_MESSAGES[1];
		case 4:
			return CULI_MESSAGES[2];
		case 5:
			return CULI_MESSAGES[3];
			default:
				return CULI_MESSAGES[0];
		}
	}

	private static void moveToArena(Player p, int z) {
		p.getPA().movePlayer(1900, 5346, z);
		CombatPrayer.resetPrayers(p);
	}
	
	private static int getZ(Player p) {
		return PlayerSingleInstance.getOrCreateZ(p.mySQLIndex) + 2;
	}
	
	public static void enterArena(Player p) {
		final int z = getZ(p);
		moveToArena(p, z);
		p.getDialogueBuilder()
		.sendNpcChat(CULINOMANCER_ID, Expression.ANGRY_YELL, getRandomCullMessage(p))
		.onReset(()-> {
			spawnBoss(p, z);
		})
		.executeIfNotActive();
	}
	
	public static void defeatedBoss(Player p) {
		
		p.increaseRfdProgress();
		
		int rfdProgress = p.getRFDProgress();
		
		if (rfdProgress >= MAX_STAGE) {
			//set different npc dialogue here.
			p.getPacketSender().sendConfig(678, 5); //vanish the portal
			return;
		}
		
		final int z = getZ(p);
		
		if (rfdProgress == MAX_STAGE-1) {
			p.getDialogueBuilder()
			.sendNpcChat(CULINOMANCER_ID, Expression.ANGRY, NOOO_MSG2)
			.onReset(()-> {
				spawnBoss(p, z);
			})
			.executeIfNotActive();
		} else {
			p.getDialogueBuilder()
			.sendNpcChat(CULINOMANCER_ID, Expression.ANGRY, NOOO_MSG)
			.onReset(()-> {
				spawnBoss(p, z);
			})
			.executeIfNotActive();
		}
	}
	
	private static void spawnBoss(Player p, int z) {
		if (p.getZ() != z)
			return;
		
		int stage = p.getRFDProgress();
		
		switch (stage) {
		
			case 0: { //banana
				NPCHandler.spawnNpc(p, 3493, 1900, 5350, z, 0, true, true);
				break;
			}
	
			case 1: { // flambeed
				NPCHandler.spawnNpc(p, 3494, 1900, 5350, z, 0, true, true);
				break;
			}
	
			case 2: { // karamel
				NPCHandler.spawnNpc(p, 3495, 1900, 5350, z, 0, true, true);
				break;
			}
	
			case 3: { // dessourt
				NPCHandler.spawnNpc(p, 3496, 1900, 5350, z, 0, true, true);
				break;
			}
	
			case 4: { // gelatinnoth mother
				NPCHandler.spawnNpc(p, 3497, 1900, 5350, z, 0, true, true);
				break;
			}
	
			case 5: { // Culinaromancer
				NPCHandler.spawnNpc(p, 3491, 1900, 5350, z, 0, true, false);
				break;
			}
			
		}
		
	}
	
	public static void openShop(Player p) {
		int stage = p.getRFDProgress();
		if (stage > MAX_STAGE)
			stage = MAX_STAGE;
		
		
	}
}
