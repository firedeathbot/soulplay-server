package com.soulplay.content.minigames.barrows;

import com.soulplay.game.model.item.ItemAssistant;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.util.Misc;

public class Barrows {

	/**
	 * Variables used for reward.
	 */
	public static int Barrows[] = {4708, 4710, 4712, 4714, 4716, 4718, 4720,
			4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747,
			4749, 4751, 4753, 4755, 4757, 4759, 4708, 4710, 4712, 4714, 4716, 4718, 4720,
			4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747,
			4749, 4751, 4753, 4755, 4757, 4759, 21736, 21744, 21752, 21760 };

	public static int Runes[] = {554, 555, 556, 557, 558, 559, 560, 561, 562,
			563, 564, 565};

	public static int Pots[] = {121, 123, 125, 127, 119, 2428, 2430, 2434, 2432,
			2444};

	/**
	 * All of the barrow data.
	 */
	public static int[][] barrowData = {
			/** ID Coffin X Y Stair X Y */
			{2026, 6771, 3556, 9716, 6703, 3574, 3297}, /** Dharoks */
			{2030, 6823, 3575, 9706, 6707, 3557, 3297}, /** Veracs */
			{2025, 6821, 3557, 9700, 6702, 3565, 3288}, /** Ahrims */
			{2029, 6772, 3568, 9685, 6706, 3554, 3282}, /** Torags */
			{2027, 6773, 3537, 9703, 6704, 3577, 3282}, /** Guthans */
			{2028, 6822, 3549, 9682, 6705, 3566, 3275} /** Karils */
	};

	private Client c;

	public boolean cantWalk;

	/**
	 * All of the spade data
	 */
	public static int[][] spadeData = {
			/** X Y X1 Y1 toX toY */
			{3553, 3294, 3561, 3301, 3578, 9706},
			{3550, 3278, 3557, 3287, 3568, 9683},
			{3561, 3285, 3568, 3292, 3557, 9703},
			{3570, 3293, 3579, 3302, 3556, 9718},
			{3571, 3278, 3582, 3285, 3534, 9704},
			{3562, 3273, 3569, 3279, 3546, 9684},
			/*
			 * {2984, 3387, 2984, 3387, 3546, 9684}, {2987, 3387, 2987, 3387,
			 * 3546, 9684}, {2989, 3378, 2989, 3378, 3546, 9684}, {2996, 3377,
			 * 2996, 3377, 3546, 9684}, {2999, 3375, 2999, 3375, 3546, 9684},
			 * {2984, 3387, 2984, 3387, 3546, 9684},
			 */
	};

	public Barrows(Client c) {
		this.c = c;
	}

	/**
	 * Checking if you have killed all of the brothers.
	 *
	 * @return
	 */
	public boolean checkBarrows() {
		// if (c.barrowsNpcs[2][1] == 2 &&
		// c.barrowsNpcs[3][1] == 2 &&
		// c.barrowsNpcs[1][1] == 2 ||
		// c.barrowsNpcs[5][1] == 2 ||
		// c.barrowsNpcs[0][1] == 2 ||
		// c.barrowsNpcs[4][1] == 2) {
		// return true;
		// }
		// return false;
		for (int i = 0; i < 6; i++) {
			if (i == 1) {
				continue;
			}
			if (c.barrows[i] != 2) {
				return false;
			}
		}
		return true;
	}

	public void checkCoffins() {
		if (!checkBarrows()) { // if (c.barrowsKillCount < 5) {
			c.sendMessage("You still have to kill the following brothers:");
			if (c.barrows[3] == 0) {
				c.sendMessage("- Karils");
			}
			if (c.barrows[2] == 0) {
				c.sendMessage("- Guthans");
			}
			if (c.barrows[4] == 0) {
				c.sendMessage("- Torags");
			}
			if (c.barrows[0] == 0) {
				c.sendMessage("- Ahrims");
			}
			if (c.barrows[5] == 0) {
				c.sendMessage("- Veracs");
			}
			c.getPA().removeAllWindows();
		} else if (c.barrows[1] == 0 && !(c.barrows[1] == 1)) { // } else if
																// (c.barrowsKillCount
																// == 5) {

			int spawnX = c.getX(), spawnY = c.getY();
			boolean breaking = false;
			for (int x = 0; x < 2; x++) {
				if (breaking) {
					break;
				}
				for (int y = 0; y < 2; y++) {
					if (x == 0 && y == 0) {
						continue;
					}
					if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
							c.getHeightLevel(), x, y)) {
						spawnX = c.getX() + x;
						spawnY = c.getY() + y;
						breaking = true;
						break;
					}
				}
			}
			NPCHandler.spawnNpc(c, 2026, spawnX, spawnY, 3, 0, true, true);
			c.getPA().removeAllWindows();
			c.barrows[1] = 1;
		} else if (c.barrows[1] == 2) {
			c.getPA().movePlayer(3551, 9694, 0);
			c.sendMessage("You teleport to the chest.");
			c.getPA().removeAllWindows();
		}
	}

	public void fixAllBarrows() {
		int totalCost = 0;
		int cashAmount = c.getItems().getItemAmount(995);
		for (int j = 0; j < c.playerItems.length; j++) {
			boolean breakOut = false;
			for (int[] brokenBarrow : ItemAssistant.brokenBarrows) {
				if (c.playerItems[j] - 1 == brokenBarrow[1]) {
					if (totalCost + 80000 > cashAmount) {
						breakOut = true;
						c.sendMessage("You have run out of money.");
						break;
					} else {
						totalCost += 80000;
					}
					c.playerItems[j] = brokenBarrow[0] + 1;
				}
			}
			if (breakOut) {
				break;
			}
		}
		if (totalCost > 0) {
			c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995),
					totalCost);
		}
	}

	/**
	 * Getting random barrow pieces.
	 *
	 * @return
	 */
	public int randomBarrows() {
		return Barrows[(int) (Math.random() * Barrows.length)];
	}

	/**
	 * Getting random pots.
	 *
	 * @return
	 */
	public int randomPots() {
		return Pots[(int) (Math.random() * Pots.length)];
	}

	/**
	 * Getting random runes.
	 *
	 * @return
	 */
	public int randomRunes() {
		return Runes[(int) (Math.random() * Runes.length)];
	}

	/**
	 * Resetting the minigame.
	 */
	public void resetBarrows() {
		for (int i = 0; i < 6; i++) {
			c.barrows[i] = 0;
		}
		// c.barrowsNpcs[0][1] = 0;
		// c.barrowsNpcs[1][1] = 0;
		// c.barrowsNpcs[2][1] = 0;
		// c.barrowsNpcs[3][1] = 0;
		// c.barrowsNpcs[4][1] = 0;
		// c.barrowsNpcs[5][1] = 0;
		c.getPacketSender().sendFrame126("Karils", 16135);
		c.getPacketSender().sendFrame126("Guthans", 16134);
		c.getPacketSender().sendFrame126("Torags", 16133);
		c.getPacketSender().sendFrame126("Ahrims", 16132);
		c.getPacketSender().sendFrame126("Veracs", 16131);
		c.getPacketSender().sendFrame126("Dharoks", 16130);
		if (c.barrows[6] >= 150) {
			c.barrows[6] = 0;
		}
	}

	/**
	 * Grabs the reward based on random chance depending on what your killcount
	 * is.
	 */
	public void reward() {
		c.getExtraHiscores().incBarrowsCompleted();
		c.getItems().addItem(4740, 20);
		c.getItems().addItem(randomRunes(), Misc.random(150) + 100);
		c.getItems().addItem(randomRunes(), Misc.random(150) + 100);
		c.getItems().addItem(randomPots(), 1);
		if (c.getClan() != null) {
			c.getClan().addClanPoints(c, 40, "barrows");
		}
		if (c.barrows[6] >= 6 && c.barrows[6] <= 108) {
			if (Misc.random(4) == 1) {
				c.getItems().addItem(randomBarrows(), 1);
			}
		} else if (c.barrows[6] >= 109 && c.barrows[6] <= 149) {
			if (Misc.random(3) == 1) {
				c.getItems().addItem(randomBarrows(), 1);
			}
		} else if (c.barrows[6] >= 150) {
			c.getItems().addItem(randomBarrows(), 1);
		}
	}

	/**
	 * Spade digging data
	 */
	public void spadeDigging() {
		if (c.inArea(spadeData[0][0], spadeData[0][1], spadeData[0][2],
				spadeData[0][3])) {
			c.getPA().movePlayer(spadeData[0][4], spadeData[0][5], 3);
		} else if (c.inArea(spadeData[1][0], spadeData[1][1], spadeData[1][2],
				spadeData[1][3])) {
			c.getPA().movePlayer(spadeData[1][4], spadeData[1][5], 3);
		} else if (c.inArea(spadeData[2][0], spadeData[2][1], spadeData[2][2],
				spadeData[2][3])) {
			c.getPA().movePlayer(spadeData[2][4], spadeData[2][5], 3);
		} else if (c.inArea(spadeData[3][0], spadeData[3][1], spadeData[3][2],
				spadeData[3][3])) {
			c.getPA().movePlayer(spadeData[3][4], spadeData[3][5], 3);
		} else if (c.inArea(spadeData[4][0], spadeData[4][1], spadeData[4][2],
				spadeData[4][3])) {
			c.getPA().movePlayer(spadeData[4][4], spadeData[4][5], 3);
		} else if (c.inArea(spadeData[5][0], spadeData[5][1], spadeData[5][2],
				spadeData[5][3])) {
			c.getPA().movePlayer(spadeData[5][4], spadeData[5][5], 3);
		} else if (c.getX() == 2984 && c.getY() == 3387) {
			c.getPA().movePlayer(1753, 5237, 0);
		} else if (c.getX() == 2987 && c.getY() == 3387) {
			c.getPA().movePlayer(1753, 5237, 0);
		} else if (c.getX() == 2989 && c.getY() == 3378) {
			c.getPA().movePlayer(1753, 5237, 0);
		} else if (c.getX() == 2996 && c.getY() == 3377) {
			c.getPA().movePlayer(1753, 5237, 0);
		} else if (c.getX() == 2999 && c.getY() == 3375) {
			c.getPA().movePlayer(1753, 5237, 0);
		} else if (c.getX() == 2984 && c.getY() == 3387) {
			c.getPA().movePlayer(1753, 5237, 0);
		}
	}

	/**
	 * Using the chest.
	 */
	public void useChest() {
		if (!checkBarrows()) {
			c.sendMessage("You haven't killed all the brothers!");
			return;
		}
		// if (c.barrows[6] >= 5) {//if (c.barrowsKillCount >= 5) {
		// if (c.barrowsNpcs[4][1] == 0 && c.barrowsNpcs[3][1] == 2 &&
		// c.barrowsNpcs[2][1] == 2 && c.barrowsNpcs[5][1] == 2 &&
		// c.barrowsNpcs[0][1] == 2 && c.barrowsNpcs[1][1] == 2) {
		// NPCHandler.spawnNpc(c, 2026, c.getX(), c.getY()-1, 0, 0, 120, 25,
		// 200, 200, true, true);
		// }
		// c.barrowsNpcs[4][1] = 1;
		// }
		if (c.barrows[6] >= 5) {
			if (c.barrows[1] == 0) {// (c.barrows[0] == 2 && c.barrows[2] == 2
									// && c.barrows[3] == 2 && c.barrows[4] == 2
									// && c.barrows[5] == 2) {

				int spawnX = c.getX(), spawnY = c.getY();
				boolean breaking = false;
				for (int x = 0; x < 2; x++) {
					if (breaking) {
						break;
					}
					for (int y = 0; y < 2; y++) {
						if (x == 0 && y == 0) {
							continue;
						}
						if (PathFinder.canMoveInStaticRegion(c.getX(), c.getY(),
								c.getHeightLevel(), x, y)) {
							spawnX = c.getX() + x;
							spawnY = c.getY() + y;
							breaking = true;
							break;
						}
					}
				}

				NPCHandler.spawnNpc(c, 2026, spawnX, spawnY, 0, 0, true, true);
				c.barrows[1] = 1;
			}
		}
		if (c.barrows[6] > 5 && checkBarrows()) {
			if (c.getItems().freeSlots() >= 4) {
				reward();
				resetBarrows();
				int canTryMinigame = Misc.random(4);
				if (canTryMinigame == 0) {
					cantWalk = true;
				}
			} else {
				c.sendMessage(
						"You need more inventory slots to open the chest.");
			}
		}
	}

	/*
	 * {2984, 3387, 2984, 3387, 3546, 9684}, {2987, 3387, 2987, 3387, 3546,
	 * 9684}, {2989, 3378, 2989, 3378, 3546, 9684}, {2996, 3377, 2996, 3377,
	 * 3546, 9684}, {2999, 3375, 2999, 3375, 3546, 9684}, {2984, 3387, 2984,
	 * 3387, 3546, 9684},
	 */
	/**
	 * Stair data
	 */
	public void useStairs() {
		for (int i = 0; i < c.barrows.length - 1; i++) {
			if (c.barrows[i] == 1) {
				c.barrows[i] = 0;
				// c.sendMessage("The barrows monster has been reset.");
			}
		}
		switch (c.objectId) {
			case 6703:
				c.getPA().movePlayer(barrowData[0][5], barrowData[0][6], 0);
				break;
			case 6707:
				c.getPA().movePlayer(barrowData[1][5], barrowData[1][6], 0);
				break;
			case 6702:
				c.getPA().movePlayer(barrowData[2][5], barrowData[2][6], 0);
				break;
			case 6706:
				c.getPA().movePlayer(barrowData[3][5], barrowData[3][6], 0);
				break;
			case 6704:
				c.getPA().movePlayer(barrowData[4][5], barrowData[4][6], 0);
				break;
			case 6705:
				c.getPA().movePlayer(barrowData[5][5], barrowData[5][6], 0);
				break;
		}
	}

}
