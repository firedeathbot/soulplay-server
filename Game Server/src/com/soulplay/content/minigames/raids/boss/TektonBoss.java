package com.soulplay.content.minigames.raids.boss;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

import java.util.Collection;

@BossMeta(ids = {7540, 7541, 7542, 7543, 7544, 7545})
public class TektonBoss extends Boss {

    private static final int INITIAL_STATE = 0;
    private static final int ANVIL_STATE = 1;
    private static final int WALK_STATE = 2;
    private static final int ATTACK_STATE = 3;
    private static final int FIRE_BALL_STATE = 4;
    private static final int WALK_TO_ANVIL_STATE = 5;

    private static final int ANVIL = 29867;

    public static final int ATTACK_ANIM = 7484 + Config.OSRS_ANIM_OFFSET;

    public static final int ATTACK_SPEED = 4;

    public static final int PROJECTILE_START = Config.OSRS_GFX_OFFSET + 660;
    public static final int PROJETILE_END = Config.OSRS_GFX_OFFSET + 659;

    public TektonBoss(NPC boss) {
        super(boss);
    }

    @Override
    public void combatLogic(NPC npc, Client c) {

    }

    @Override
    public void process(NPC npc) {
        npc.resetAttack();
        npc.resetFollow();
        int state = npc.attr().getOrDefault("state", -1);
        int tick = npc.attr().getOrDefault("tick", 0);

        Collection<Player> players = Server.getRegionManager().getLocalPlayers(npc);

        if (players == null) {
            return;
        }

        if (state == WALK_TO_ANVIL_STATE) {
            if (npc.getX() == 9740 && npc.getY() == 5299) {
                npc.transform(7540);
                npc.attr().put("state", ANVIL_STATE);
                return;
            }
            npc.resetAttack();
            npc.resetFollow();
            npc.walkToLocation(Location.create( 9740, 5299));
            return;
        }

        for (Player player : players) {
            if (player == null) {
                continue;
            }

            //npc.forceChat("my state is: " + state + " x: " + location.getX() + " y: " + location.getY());

            if (state == INITIAL_STATE) {
                if (player.getCurrentLocation().isWithinDistance(npc.getCurrentLocation(), 2)) {
                    npc.transform(7541);
                    npc.attr().put("cooldown", 5);
                    npc.attr().put("state", WALK_STATE);
                    break;
                }
                npc.resetFollow();
                npc.resetAttack();
                npc.faceLocation(9742, 5306);
            } else if (state == ANVIL_STATE) {
                int currentHp = npc.getSkills().getLifepoints();
                int maxHp = npc.getSkills().getStaticLevel(Skills.HITPOINTS);

                if (currentHp < maxHp) {
                    currentHp *= 1.25;

                    if (currentHp > maxHp) {
                        currentHp = maxHp;
                    }

                    npc.setHP(currentHp);
                }

                npc.attr().put("state", FIRE_BALL_STATE);
                npc.attr().put("cooldown", 60);
                npc.resetFollow();
                npc.resetAttack();
                npc.faceLocation(9742, 5306);
                break;
            } else if (state == FIRE_BALL_STATE) {
                int cooldown = npc.attr().getOrDefault("cooldown", -1);

                if (cooldown <= 0) {
                    npc.attr().put("state", WALK_STATE);
                    npc.transform(7543);
                    break;
                }

                if (tick % 4 == 0) {
                    Location[] locs = new Location[]{Location.create(9733, 5299, player.getZ()), Location.create(9732, 5306, player.getZ()), Location.create(9738, 5307, player.getZ()), Location.create(9748, 5300, player.getZ()),
                    Location.create(9751, 5293, player.getZ()), Location.create(9747, 5289, player.getZ()), Location.create(9740, 5287, player.getZ()), Location.create(9739, 5287, player.getZ()), Location.create(9736, 5291, player.getZ()),
                    Location.create(9732, 5291, player.getZ())};

                    Location endLoc = player.getCurrentLocation().copyNew();

                    Projectile projectile = new Projectile(Config.OSRS_GFX_OFFSET + 660, Misc.random(locs), endLoc);
                    GlobalPackets.createProjectile(projectile);

                    Server.getStillGraphicsManager().createGfx(endLoc.getX(), endLoc.getY(), endLoc.getZ(), Config.OSRS_GFX_OFFSET + 659, Graphic.GraphicType.MEDIUM.getHeight(), 112);

                    CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
                        @Override
                        public void execute(CycleEventContainer container) {
                            container.stop();
                        }

                        @Override
                        public void stop() {
                            if (player.getX() == endLoc.getX() && player.getY() == endLoc.getY()) {
                                int damage = Misc.random(65);

                                if (player.prayerActive[Prayers.PROTECT_FROM_MAGIC.ordinal()]) {
                                    damage *= 0.75;
                                }

                                player.dealDamage(new Hit(damage, 0, Hit.HIT_ICON_MAGIC));
                            }
                        }

                    }, 5);
                }
            } else if (state == WALK_STATE) {
                if (npc.getCurrentLocation().isWithinDistance(player.getCurrentLocation(), 1)) {

                    int cooldown = npc.attr().getOrDefault("cooldown", 0);

                    if (cooldown <= 0) {
                        npc.attr().put("state", ATTACK_STATE);
                        npc.transform(7542);
                    } else {
                        npc.attr().put("cooldown", --cooldown);
                    }
                    break;
                }
                npc.walkToLocation(player.getCurrentLocation());
                break;
            } else if (state == ATTACK_STATE) {
                int attackSpeed = npc.attr().getOrDefault("attack_speed", ATTACK_SPEED);

                if (tick % attackSpeed == 0) {
                    if (!isInDistnace(npc, player)) {
                        npc.attr().put("state", WALK_TO_ANVIL_STATE);
                        break;
                    }

                    npc.startAnimation(ATTACK_ANIM);
                    npc.faceLocation(player.getCurrentLocation(), true);
                } else if (tick % attackSpeed == 3) {
                    applyDamage(npc);
                }

                break;
            }


        }

        int cooldown = npc.attr().getOrDefault("cooldown", 0);

        if (cooldown > 0) {
            npc.attr().put("cooldown", --cooldown);
        }

        npc.attr().put("tick", ++tick % 100);
    }

    private static boolean isInDistnace(NPC npc, Player player) {
        final int dx = npc.getCurrentLocation().getDx(player.getCurrentLocation());
        final int dy = npc.getCurrentLocation().getDy(player.getCurrentLocation());

        // outside
        if (dx >= -3 && dx <= 0 && (dy == 1 || dy == -4)) {
            return true;
        } else if (dx == -4 && dy >= -3 && dy <= 0) {
            return true;
        } else if (dx == 1 && dy >= -3 && dy <= 0) {
            return true;
        }

        // inside
        if ((dx >= -3 && dx <= 0) && (dy >= -3 && dy <= 0)) {
            return true;
        }

        return false;
    }

    private static boolean applyDamage(NPC npc) {
        Location loc = npc.getLastFacingLocation();

        Direction direction = getFacingDirection(npc);

        for (Player p : Server.getRegionManager().getLocalPlayers(npc)) {

            Location pl = p.getCurrentLocation();
            final int dx = Math.abs(pl.getDx(loc));
            final int dy = Math.abs(pl.getDy(loc));

            boolean active = p.prayerActive[Prayers.PROTECT_FROM_MELEE.ordinal()];

            int damage = Misc.random(65);

            if (active) {
                damage *= 0.75;
            }

            if (direction == Direction.NORTH && dy == 0) {
                p.dealDamage(new Hit(damage, 0, 0));
            } else if (direction == Direction.SOUTH && dy == 0) {
                p.dealDamage(new Hit(damage, 0, 0));
            } else if (direction == Direction.WEST && dx == 0) {
                p.dealDamage(new Hit(damage, 0, 0));
            } else if (direction == Direction.EAST && dx == 0) {
                p.dealDamage(new Hit(damage, 0, 0));
            }
        }
        return false;
    }

    private static Direction getFacingDirection(NPC npc) {
        Location curr = npc.getCurrentLocation();
        Location loc = npc.getLastFacingLocation();
        int dx = curr.getDx(loc);
        int dy = curr.getDy(loc);

        if (dx > 0) {
            return Direction.WEST;
        } else if (dx < -3) {
            return Direction.EAST;
        } else if (dy < -3) {
            return Direction.NORTH;
        } else {
            return Direction.SOUTH;
        }
    }

    @Override
    public Prayers protectPrayer(int npcId) {
        return null;
    }

    @Override
    public int protectPrayerDamageReduction(int damage) {
        return 0;
    }

    @Override
    public void onSpawn(NPC npc) {
        if (Misc.isPointInCircle(npc.getX(), npc.getY(),9742, 5295, 12)) {
            npc.attr().put("map", "left");
            npc.moveNpc(9740, 5299, 1);
            npc.faceLocation(9742, 5306);
        } else if (Misc.isPointInCircle(npc.getX(), npc.getY(),9712, 5329, 12)) {
            npc.attr().put("map", "mid");
        } else if (Misc.isPointInCircle(npc.getX(), npc.getY(),9742, 5328, 12)) {
            npc.attr().put("map", "right");
        }
        npc.attr().put("state", ANVIL_STATE);
        npc.attr().put("attack_speed", ATTACK_SPEED);
        npc.attr().put("tick", 0);

        Collection<Player> players = Server.getRegionManager().getLocalPlayers(npc);
        npc.getSkills().setStaticLevel(Skills.HITPOINTS, 100 * players.size() + 2);
    }

    @Override
    public boolean ignoreMeleeBonus() {
        return false;
    }

    @Override
    public boolean ignoreRangeBonus() {
        return true;
    }

    @Override
    public boolean ignoreMageBonus() {
        return true;
    }

    @Override
    public int followDistance() {
        return 2;
    }

    @Override
    public boolean canMove() {
        return true;
    }

    @Override
    public int extraAttackDistance() {
        return 0;
    }

    @Override
    public void proxyDamage(NPC npc, Hit hit, Mob source) {

    }

    @Override
    public void onDeath(NPC npc, Client c) {

    }

    @Override
    public void afterNpcAttack(NPC npc, Player p, int damage) {

    }

    public static void test(Player player) {
        leftMap(player);
    }

    private static void leftMap(Player player) {
        player.getPA().movePlayer(9744, 5282, 1);
        NPC npc = NPCHandler.spawnNpc(player, 7540, 9742, 5295, player.heightLevel, 0, false, false);
        npc.attr().put("map", "left");
        npc.attr().put("state", INITIAL_STATE);
        npc.faceLocation(9681, 5340);
    }

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
