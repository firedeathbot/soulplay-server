package com.soulplay.content.minigames.raids.boss;

import com.soulplay.Config;
import com.soulplay.content.combat.BattleState;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.Boss;
import com.soulplay.content.player.skills.dungeoneeringv2.bosses.BossMeta;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.util.Misc;

@BossMeta(ids = {7602, 7603, 7604})
public class SkeletalMysticBoss extends Boss {

    private static Location[] spawnLocations = new Location[] {
            Location.create(9738, 5259, 1), Location.create(9746, 5258, 1),
            Location.create(9738, 5268, 1), Location.create(9746, 5270, 1)
    };

    private static final int RED_NPC = 7602;
    private static final int GREEN_NPC = 7603;
    private static final int PURPLE_NPC = 7604;

    private static int[] npcs = new int[]{RED_NPC, GREEN_NPC, PURPLE_NPC};

    public static final int DEATH_ANIM = 5527 + Config.OSRS_ANIM_OFFSET;
    public static final int ATTACK_ANIM = 5528 + Config.OSRS_ANIM_OFFSET;

    public SkeletalMysticBoss(NPC boss) {
        super(boss);
    }

    @Override
    public void combatLogic(NPC npc, Client c) {
        boolean inDistance = npc.getCurrentLocation().isWithinDistance(c.getCurrentLocation(), 1);

        if (Misc.random(3) == 1) {
            npc.attackType = 2;
            npc.startGraphic(Graphic.create(Config.OSRS_GFX_OFFSET + 167, Graphic.GraphicType.EVEN_HIGHER));
            npc.projectileId = Config.OSRS_GFX_OFFSET + 168;
            npc.projectileDelay = 3;
            npc.projectiletStartHeight = 43;
            npc.projectiletEndHeight = 31;
            npc.endGfx = Config.OSRS_GFX_OFFSET + 169;
        } else if (Misc.random(3) == 1 && inDistance) {
            npc.attackType = 0;
        } else {
            npc.attackType = 2;
            npc.startGraphic(Graphic.create(Config.OSRS_GFX_OFFSET + 129, Graphic.GraphicType.EVEN_HIGHER));
            npc.projectileId = Config.OSRS_GFX_OFFSET + 130;
            npc.projectileDelay = 46;
            npc.projectiletStartHeight = 43;
            npc.projectiletEndHeight = 31;
            npc.endGfx = Config.OSRS_GFX_OFFSET + 131;
        }
    }

    @Override
    public void process(NPC npc) {

    }

    @Override
    public Prayers protectPrayer(int npcId) {
        return null;
    }

    @Override
    public int protectPrayerDamageReduction(int damage) {
        return 0;
    }

    @Override
    public void onSpawn(NPC npc) {

    }

    @Override
    public boolean ignoreMeleeBonus() {
        return false;
    }

    @Override
    public boolean ignoreRangeBonus() {
        return false;
    }

    @Override
    public boolean ignoreMageBonus() {
        return false;
    }

    @Override
    public int followDistance() {
        return 10;
    }

    @Override
    public boolean canMove() {
        return true;
    }

    @Override
    public int extraAttackDistance() {
        return 0;
    }

    @Override
    public void proxyDamage(NPC npc, Hit hit, Mob source) {

    }

    @Override
    public void onDeath(NPC npc, Client c) {

    }

    @Override
    public void afterNpcAttack(NPC npc, Player p, int damage) {

    }

    public static void test(Player player) {
        player.getPA().movePlayer(9742, 5251, 1);

        for (Location location : spawnLocations) {
            // TODO scale health by number of players
            NPCHandler.spawnNpc(player, Misc.random(npcs), location.getX(), location.getY(), player.heightLevel, 1, true, false);
        }
    }

    @Override
    public int getAttackAnimation(int attackType) {
        if (attackType == 0 || attackType == 2) {
            return ATTACK_ANIM;
        }
        return -1;
    }

    @Override
    public int getDeathAnimation() {
        return DEATH_ANIM;
    }

	@Override
	public void onHitFromAttackMob(NPC npc, BattleState state) {
		
	}

}
