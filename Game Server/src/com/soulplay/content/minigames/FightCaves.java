package com.soulplay.content.minigames;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class FightCaves {

	private static final int[][] WAVES = {{2743, 2741, 2631, 2630, 2630},
			{2743, 2741, 2631, 2631}, {2743, 2741, 2741}, {2743, 2743}, {2745}};

	private static final int[][] coordinates = {{2398, 5086}, {2387, 5095}, {2407, 5098},
			{2417, 5082}, {2390, 5076}, {2410, 5090}};

	public static void spawnNextWave(Client c) {
		if (c.waveId >= WAVES.length) {
			c.waveId = 0;
			return;
		}

		if (c.waveId < 0) {
			return;
		}

		int npcAmount = WAVES[c.waveId].length;
		for (int j = 0; j < npcAmount; j++) {
			int npc = WAVES[c.waveId][j];
			int X = coordinates[j][0];
			int Y = coordinates[j][1];
			int H = c.heightLevel;
			NPC n = NPCHandler.spawnNpc(c, npc, X, Y, H, 0, true, false);
			n.setFightCavesNPC(true);
			n.spawnedBy = c.getId();
		}
		c.tzhaarToKill = npcAmount;
		c.tzhaarKilled = 0;
		c.sendMessage("<col=ff0000>Wave: " + (c.waveId + 1));
	}

	public static void killedTzhaar(NPC npc) {
		final Client c = (Client) PlayerHandler.players[npc.spawnedBy];
		if (c == null) {
			return;
		}
		if (c.inKilnFightCave) {
			c.tzhaarToKill--;
			return;
		}
		c.tzhaarKilled++;
		if (c.tzhaarKilled == c.tzhaarToKill) {
			c.waveId++;
			spawnNextWave(c);
		}
	}
	
	public static void enterCaves(final Client c) {
		if (!c.getSpamTick().checkThenStart(1)) {
			return;
		}
		c.getDamageMap().reset();
		c.getPA().movePlayer(2413, 5117, c.getId() * 4);
		c.waveId = 0;
		c.tzhaarToKill = -1;
		c.tzhaarKilled = -1;
		final int ticks = 17;
		c.forceChat(String.format("Wave will start in %d seconds", Misc.ticksToSeconds(ticks)));
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			int timer = ticks;
			@Override
			public void execute(CycleEventContainer e) {
				if (c.disconnected) {
					e.stop();
					return;
				}
				if (timer == 0) {
					FightCaves.spawnNextWave(c);
					e.stop();
					return;
				}
				if (timer > 2 && timer < 6)
					c.forceChat(Integer.toString(Misc.ticksToSeconds(timer)));
				timer--;
			}

			@Override
			public void stop() {
			}
		}, 1);
	}

}
