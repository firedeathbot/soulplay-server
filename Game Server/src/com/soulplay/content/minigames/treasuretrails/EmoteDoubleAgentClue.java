package com.soulplay.content.minigames.treasuretrails;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class EmoteDoubleAgentClue extends ClueType {

	private final int emoteButton;
	private final Location emoteLocation;
	private final int distance;
	private final int[][] items;
	private final String clue1;
	private final String clue2;
	private final String clue3;
	private final String clue4;
	private final String clue5;

	public EmoteDoubleAgentClue(int index, int clueId, int emoteButton, Location emoteLocation, int distance, String clue1, String clue2, String clue3, String clue4, String clue5, int[][] items) {
		super(Misc.toBitpack(index, clueId));
		this.emoteButton = emoteButton;
		this.emoteLocation = emoteLocation;
		this.distance = distance;
		this.items = items;
		this.clue1 = clue1;
		this.clue2 = clue2;
		this.clue3 = clue3;
		this.clue4 = clue4;
		this.clue5 = clue5;

		EmoteClue.entries.add(emoteButton);
	}

	@Override
	public void openHint(Player player) {
		for (int i = 6968; i < 6976; i++) {
			player.getPacketSender().sendFrame126("", i);
		}

		player.getPacketSender().sendFrame126(clue1, 6969);
		player.getPacketSender().sendFrame126(clue2, 6970);
		player.getPacketSender().sendFrame126(clue3, 6971);
		player.getPacketSender().sendFrame126(clue4, 6972);
		player.getPacketSender().sendFrame126(clue5, 6973);
		player.getPacketSender().showInterface(6965);
	}

	public int getEmoteButton() {
		return emoteButton;
	}

	@Override
	public void checkComplete(Player player, ClueDifficulty difficulty) {
		if (checkReqs(player)) {
			player.getTreasureTrailManager().nextStage(difficulty);
		}
	}

	public boolean checkReqs(Player player) {
		if (player.getX() >= emoteLocation.getX() - distance && player.getY() >= emoteLocation.getY() - distance && player.getX() <= emoteLocation.getX() + distance && player.getY() <= emoteLocation.getY() + distance) {
			int found = 0;
			int req = items.length;
			for (int i = 0; i < items.length; i++) {
				int slot = items[i][0];
				for (int j = 1; j < items[i].length; j++) {
					int itemId = items[i][j];
					if (player.playerEquipment[slot] == itemId) {
						found++;
						break;
					}
				}
			}
			
			if (found == req) return true;

			return false;
		}

		return false;
	}

}
