package com.soulplay.content.minigames.treasuretrails;

import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class TreasureTrails {

	public static final int LOW_CLUE = 0;

	public static final int MED_CLUE = 1;

	public static final int HIGH_CLUE = 2;

	public static int lowLevelReward[] = {1069, 1083, 1119, 1107, 1125, 1325,
			1323, 1155, 1177, 1075, 1087, 1077, 1089, 1067, 1081, 1115, 1069,
			1083, 1119, 1107, 1125, 1131, 1129, 1133, 1511, 1168, 1165, 1179,
			1195, 1217, 1283, 1297, 1313, 1327, 1341, 1361, 1367, 1426, 1325,
			1323, 1155, 1177, 1075, 1087, 1077, 1089, 1067, 1081, 1115, 1069,
			1083, 1119, 1107, 1125, 1131, 1129, 1133, 1511, 1168, 1165, 1179,
			1195, 1217, 1283, 1297, 1313, 1327, 1341, 1361, 1367, 1426, 1168,
			1165, 1179, 1195, 1217, 1283, 1297, 1313, 1327, 1341, 1361, 1367,
			1426, 2633, 2635, 2637, 7388, 7386, 7392, 7390, 7396, 7394, 2631,
			7364, 7362, 7368, 7366, 2583, 2585, 2587, 2589, 2591, 2593, 2595,
			2597, 7332, 7338, 7350, 7356, 10396, 10394, 10462, 10466, 10458,
			10464, 10460, 10468, 19382, 19390, 19384, 19388, 19380, 19386,
			10392, 10398,
			// new
			10420, 10424, 10426, 10428, 10430, 10432, 10434, 10316, 10318,
			10320, 10322, 10324, 10284, 10280, 10282, 10366, 13095};

	public static int mediemLevelReward[] = {1211, 1245, 1271, 1287, 1301, 1317,
			1071, 1085, 1121, 1329, 1725, 1727, 1729, 1731, 1073, 1091, 1099,
			1111, 1135, 1124, 1145, 1161, 1169, 1183, 1199, 1211, 1245, 1271,
			1287, 1301, 1317, 1331, 1357, 1371, 1430, 1071, 1085, 1121, 1329,
			1725, 1727, 1729, 1731, 1073, 1091, 1099, 1111, 1135, 1124, 1145,
			1161, 1169, 1183, 1199, 1211, 1245, 1271, 1287, 1301, 1317, 1331,
			1357, 1371, 1430, 1071, 1085, 1121, 1329, 1725, 1727, 1729, 1731,
			1073, 1091, 1099, 1111, 1135, 1124, 1145, 1161, 1169, 1183, 1199,
			1211, 1245, 1271, 1287, 1301, 1317, 1331, 1357, 1371, 1430, 2599,
			1071, 1085, 1121, 1329, 1725, 1727, 1729, 1731,

			2599, 2601, 2603, 2605, 2607, 2609, 2611, 2613, 7334, 7340, 7346,
			7352, 7358, 7319, 7321, 7323, 7325, 7327, 7372, 7370, 7380, 7378,
			2645, 2647, 2649, 2577, 2579, 6916, 6918, 6920, 6922, 6924, 10400,
			10402, 10416, 10418, 10420, 10422, 10436, 10438, 10446, 10448,
			10450, 10452, 10454, 10456, 6889,
			// NEW SHIT
			10284, 10280, 10282, 10296, 19173, 19175, 10666, 10364, 13097,
			13103, 19380, 19386, 19384, 19388, 19382, 19390};

	public static int highLevelReward[] = {2651, 1079, 1093, 1113, 1127, 1147,
			1163, 1185, 1201, 1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497,
			2503, 861, 859, 7454, 7460, 1079, 1093, 1113, 1127, 1148, 1164,
			1185, 1201, 1213, 1247, 1275, 1289, 1303, 1319, 1333, 1347, 1359,
			1374, 1432, 1079, 1093, 1113, 1127, 1147, 1163, 1185, 1201, 1275,
			1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503, 861, 859, 2651,
			1079, 1093, 1113, 1127, 1147, 1163, 1185, 1201, 1275, 1303, 1319,
			1333, 1359, 1373, 2491, 2497, 2503, 861, 859, 7454, 7460, 1079,
			1093, 1113, 1127, 1148, 1164, 1185, 1201, 1213, 1247, 1275, 1289,
			1303, 1319, 1333, 1347, 1359, 1374, 1432, 1079, 1093, 1113, 1127,
			1147, 1163, 1185, 1201, 1275, 1303, 1319, 1333, 1359, 1373, 2491,
			2497, 2503, 861, 859, 2651, 1079, 1093, 1113, 1127, 1147, 1163,
			1185, 1201, 1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503,
			861, 859, 1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503, 861,
			859, 2651, 1079, 1093, 1113, 1127, 1147, 1163, 1185, 1201, 1275,
			1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503, 861, 859, 2651,
			2615, 2617, 2619, 2621, 2623, 2625, 2627, 2629, 2639, 2641, 2643,
			2651, 2653, 2655, 2657, 2659, 2661, 2663, 2665, 2667, 2669, 2671,
			2673, 2675, 7342, 7348, 7374, 7376, 7382, 7384, 7398, 7399, 7400,
			3481, 3483, 3485, 3486, 3488, 2615, 2617, 2619, 2621, 2623, 10330,
			10338, 10348, 10332, 10340, 10346, 10334, 10342, 10350, 10336,
			10344, 10352, 10368, 10376, 10384, 10370, 10378, 10386, 10372,
			10380, 10388, 10374, 10382, 10390, 10470, 10472, 10474, 10440,
			10442, 10444, 6914, 2581, 2577, 19449, 19449, 19445, 19445, 19447,
			19447, 19443, 19443, 19457, 19457, 19453, 19453, 19455, 19455,
			19451, 19451, 19747, 19747, 11280, 11280, 13103, 13103, 13101,
			19290, 19287, 19284, 19281, 15503, 15505, 15507, 15509, 15511,
			19333, 10362, 19275, 19278, 19368, 19370, 19732, 19374, 19376,
			19378, 13099, 19422, 19413, 19416, 19425, 19437, 19428, 19431,
			19440, 19407, 19398, 19401, 10410, 19465, 19461, 19463, 19459,
			19457, 19453, 19455, 19451, 19449, 19445, 19447, 19433

	};

	public static int lowLevelStacks[] = {995, 380, 561, 886,};

	public static int mediumLevelStacks[] = {995, 374, 561, 563, 890,};

	public static int highLevelStacks[] = {995, 386, 561, 563, 560, 892};

	public static void addClueReward(Client c, int clueLevel) {
		int chanceReward = Misc.random(2);
		if (clueLevel == 0) {
			switch (chanceReward) {
				case 0:
					displayReward(c, lowLevelReward[Misc.random(78)], 1,
							lowLevelReward[Misc.random(78)], 1,
							lowLevelStacks[Misc.random(3)],
							1 + Misc.random(150));
					break;
				case 1:
					displayReward(c, lowLevelReward[Misc.random(120)], 1,
							lowLevelStacks[Misc.random(3)],
							1 + Misc.random(150), -1, 1);
					break;
				case 2:
					displayReward(c, lowLevelReward[Misc.random(78)], 1,
							lowLevelReward[Misc.random(120)], 1, -1, 1);
					break;
			}
		} else if (clueLevel == 1) {
			switch (chanceReward) {
				case 0:
					displayReward(c, mediemLevelReward[Misc.random(86)], 1,
							mediemLevelReward[Misc.random(86)], 1,
							mediumLevelStacks[Misc.random(4)],
							1 + Misc.random(200));
					break;
				case 1:
					displayReward(c, mediemLevelReward[Misc.random(133)], 1,
							mediumLevelStacks[Misc.random(4)],
							1 + Misc.random(200), -1, 1);
					break;
				case 2:
					displayReward(c, mediemLevelReward[Misc.random(86)], 1,
							mediemLevelReward[Misc.random(133)], 1, -1, 1);
					break;
			}
		} else if (clueLevel == 2) {
			switch (chanceReward) {
				case 0:
					displayReward(c, highLevelReward[Misc.random(129)], 1,
							highLevelReward[Misc.random(129)], 1,
							highLevelStacks[Misc.random(5)],
							1 + Misc.random(350));
					break;
				case 1:
					displayReward(c, highLevelReward[Misc.random(216)], 1,
							highLevelStacks[Misc.random(5)],
							1 + Misc.random(350), -1, 1);
					break;
				case 2:
					displayReward(c, highLevelReward[Misc.random(129)], 1,
							highLevelReward[Misc.random(206)], 1, -1, 1);
					break;
			}
		}
	}

	public static void displayReward(Client c, int item, int amount, int item2,
			int amount2, int item3, int amount3) {
		int[] items = {item, item2, item3};
		int[] amounts = {amount, amount2, amount3};
		c.getItems().addItem(item, amount);
		c.getItems().addItem(item2, amount2);
		c.getItems().addItem(item3, amount3);
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(6963);
		c.getOutStream().writeShort(items.length);
		for (int i = 0; i < items.length; i++) {
			if (amounts[i] > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(amounts[i]);
			} else {
				c.getOutStream().writeByte(amounts[i]);
			}
			if (items[i] > 0) {
				c.getOutStream().write3Byte(items[i] + 1);
			} else {
				c.getOutStream().write3Byte(0);
			}
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		c.getPacketSender().showInterface(6960);
	}

}
