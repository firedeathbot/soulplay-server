package com.soulplay.content.minigames.treasuretrails;

import com.soulplay.Server;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 5145 })
public class DoubleAgentNpc extends AbstractNpc {

	public DoubleAgentNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new DoubleAgentNpc(slot, npcType);
	}

	@Override
	public void onSpawn() {
		super.onSpawn();

		startGraphic(Graphic.create(Graphic.getOsrsId(86)));
		forceChat("I expect you to die!");
	}

	@Override
	public void onDeath() {
		super.onDeath();

		Server.getTaskScheduler().schedule(5, () -> {
			Player c = PlayerHandler.players[spawnedBy];

			if (c == null) {
				return;
			}

			int stage = c.attr().getOrDefault("doubleagent", 0);
			if (stage != 1) {
				return;
			}

			c.attr().put("doubleagent", 2);
			for (ClueDifficulty difficulty : ClueDifficulty.values) {
				int data = c.getTreasureTrailManager().getCluesData()[difficulty.getIndex()];
				if (data == -1) {
					continue;
				}

				ClueType clueType = ClueManager.clues.get(difficulty.getIndex()).get(Misc.getFirst(data)).get(Misc.getSecond(data));
				if (clueType instanceof EmoteDoubleAgentClue) {
					EmoteDoubleAgentClue emoteClue = (EmoteDoubleAgentClue) clueType;
					if (emoteClue.checkReqs(c)) {
						ClueManager.spawnUri(c);
						c.attr().put("doubleagent", 3);
					}
				}
			}
		});
	}

}
