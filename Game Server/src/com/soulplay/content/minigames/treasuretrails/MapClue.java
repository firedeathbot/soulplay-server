package com.soulplay.content.minigames.treasuretrails;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class MapClue extends ClueType {

	private final Location digLocation;

	private final int interfaceId;

	public MapClue(int index, int clueId, Location digLocation, int interfaceId) {
		super(Misc.toBitpack(index, clueId));
		this.digLocation = digLocation;
		this.interfaceId = interfaceId;
	}

	@Override
	public void openHint(Player player) {
		player.getPacketSender().showInterface(interfaceId);
	}

	@Override
	public void checkComplete(Player player, ClueDifficulty difficulty) {
		if (player.getX() >= digLocation.getX() - 1 && player.getY() >= digLocation.getY() - 1 && player.getX() <= digLocation.getX() + 1 && player.getY() <= digLocation.getY() + 1) {
			player.getTreasureTrailManager().nextStage(difficulty);
		}
	}


}
