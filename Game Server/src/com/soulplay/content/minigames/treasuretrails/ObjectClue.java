package com.soulplay.content.minigames.treasuretrails;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class ObjectClue extends ClueType {
	
	public static final Set<Integer> entries = new HashSet<>();
	
	private final int objectId;
	private final Location objectLocation;
	private final int interfaceId;

	public ObjectClue(int index, int clueId, int objectId, Location objectLocation, int interfaceId) {
		super(Misc.toBitpack(index, clueId));
		this.objectId = objectId;
		this.objectLocation = objectLocation;
		this.interfaceId = interfaceId;

		entries.add(objectId);
	}

	@Override
	public void openHint(Player player) {
		player.getPacketSender().showInterface(interfaceId);
	}

	@Override
	public void checkComplete(Player player, ClueDifficulty difficulty) {
		if (player.getClickedObject() != null && player.getClickedObject().getId() == objectId && player.getClickedObject().getLocation().matches(objectLocation)) {
			player.getTreasureTrailManager().nextStage(difficulty);
		}
	}
	
}
