package com.soulplay.content.minigames.treasuretrails.rewards;

public class MediumItemLoot {
	
	//itemId, minAmount, maxAmount, itemId2
	
	public static final int[][] COMMON_UNIQUE = { 
			{10364, 1, 1}, //strength amulet (t)
			//{-1, 1, 1}, //gnomish firelighter missing
			{10282, 1, 1}, //yew comp bow
	};
	
	public static final int[][] UNCOMMON_UNIQUE = { 
			{2577, 1, 1}, //ranger boots
			{2579, 1, 1}, //wizard boots
			//{-1, 1, 1}, //holy sandals missing
			//adamant (t) missing
			//adamant (g) missing.
			//adamant kiteshield (h) missing
			{10296, 1, 1}, //adamant helm (h1)
			{10298, 1, 1}, //adamant helm (h2)
			{10300, 1, 1}, //adamant helm (h3)
			{10302, 1, 1}, //adamant helm (h4)
			{10304, 1, 1}, //adamant helm (h5)
			//mithril (t) missing
			//mithril (g) missing
			{7370, 1, 1}, //green dragonhide body (g)
			{7378, 1, 1}, //green dragonhide chaps (g)
			{7372, 1, 1}, //green dragonhide body (t)
			{7380, 1, 1}, //green dragonhide chaps (t)
			{10446, 1, 1}, //saradomin mitre
			{10452, 1, 1}, //saradomin cloak
			{10454, 1, 1}, //guthix mitre
			{10448, 1, 1}, //guthix cloak
			{10456, 1, 1}, //zamorak mitre
			{10450, 1, 1}, //zamorak cloak
			{19396, 1, 1}, //ancient stole
			{19366, 1, 1}, //ancient crozier
			{19374, 1, 1}, //armadyl mitre
			{19368, 1, 1}, //armadyl cloak
			{19392, 1, 1}, //armadyl stole
			{19362, 1, 1}, //armadyl crozier
			{19376, 1, 1}, //bandos mitre
			{19370, 1, 1}, //bandos cloak
			{19394, 1, 1}, //bandos stole
			{19364, 1, 1}, //bandos crozier
			{7319, 1, 1}, //red boater
			{7323, 1, 1}, //gree boater
			{7321, 1, 1}, //orange boater
			{7327, 1, 1}, //black boater
			{7325, 1, 1}, //blue boater
			//missing pink boater
			//missing purpler boater
			//missing white boater
			{2645, 1, 1}, //red headband
			{2647, 1, 1}, //black headband
			{2649, 1, 1}, //brown headband
			//missing white headband
			//missing blue headband
			//missing gold headband
			//missing pink headband
			//missing green headband
			//missing crier hat
			{13097, 1, 1}, //adamant cane
			{13113, 1, 1}, //cat mask
			{13109, 1, 1}, //penguin mask
			//missing leprechaun hat
			//crier coat
			//crier bell
			//all banners in donator shop expensive not sure if should add in clues
			//missing cabbage round shield
			//missing black leprechaun hat
			
			//rs3 stuff
			{13103, 1, 1}, //pith helmet
	};
	
	public static final int[][] RARE_UNIQUE = {
			{10416, 1, 1}, //purple elegant shirt
			{10418, 1, 1}, //purple elegant legs
			{10436, 1, 1}, //purple elegant blouse
			{10438, 1, 1}, //purple elegant skirt
			{10400, 1, 1}, //black elegant shirt
			{10402, 1, 1}, //black elegant legs
			//missing black elegant blouse
			//missing black elegant skirt
			//missing all pink elegant
			//missing all gold elegant
	};

	public static final int[][] RARER = {
			{19278, 1, 1}, //black unicorn mask
			{19275, 1, 1}, //white unicorn mask
	};

	public static final int[][] COMMON = { 
			{1161, 1, 1}, //adamant full helm
			{1123, 1, 1}, //adamant platebody
			{1073, 1, 1}, //adamant platelegs
			{1301, 1, 1}, //adamant longsword
			{1371, 1, 1}, //adamant battleaxe
			{1357, 1, 1}, //adamant axe
			{1271, 1, 1}, //adamant pickaxe
			{1135, 1, 1}, //green d hide body
			{1099, 1, 1}, //green d hide chaps
			{857, 1, 1}, //yew shortbow
			{855, 1, 1}, //yew longbow
			{1393, 1, 1}, //fire battlestaff
			{1731, 1, 1}, //amulet of power
			{380, 8, 12}, //lobster
			{374, 8, 12}, //swordfish
			{556, 50, 100}, //air rune
			{558, 50, 100}, //mind rune
			{555, 50, 100}, //water rune
			{557, 50, 100}, //earth rune
			{554, 50, 100}, //fire rune
			{562, 10, 20}, //chaos rune
			{561, 10, 20}, //nature rune
			{563, 10, 20}, //law rune
			{560, 10, 20}, //death rune
	};

}
