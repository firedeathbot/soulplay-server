package com.soulplay.content.minigames.treasuretrails.rewards;

public class MasterItemLoot {
	
	public static final int[][] UNCOMMON_UNIQUE = { 
			{120065, 1, 1}, //Occult ornament kit
			{120062, 1, 1}, //Torture ornament kit
			{122246, 1, 1}, //Anguish ornament kit
			{123348, 1, 1}, //Tormented ornament kit
			{30216, 1, 1}, //dragon defender ornament kit
			{30164, 1, 1}, //Hood of darkness
			{30165, 1, 1}, //robe top of darkness
			{30166, 1, 1}, //gloves of darkness
			{30167, 1, 1}, //robe bottom of darkness
			{30168, 1, 1}, //Boots of darkness		
			{30138, 1, 1}, //Samurai kasa
			{30139, 1, 1}, //Samurai shirt
			{30140, 1, 1}, //Samurai gloves
			{30141, 1, 1}, //Samurai greaves
			{30142, 1, 1}, //Samurai boots
			{30145, 1, 1}, //Arceuus hood
			{30148, 1, 1}, //Hosidius hood
			{30151, 1, 1}, //Lovakengj hood
			{30154, 1, 1}, //Piscarilius hood
			{30157, 1, 1}, //Shayzien hood
			{30190, 1, 1}, //Old demon mask
			{30187, 1, 1}, //Lesser demon mask
			{30188, 1, 1}, //Greater demon mask
			{30189, 1, 1}, //Black demon mask
			{30191, 1, 1}, //Jungle demon mask
			{119724, 1, 1}, //left eye patch
			{120110, 1, 1}, //bowl wig
			{120056, 1, 1}, //ale of the gods
			{120050, 1, 1}, //obsidian cape (r)
			{120008, 1, 1}, //fancy tiara //old 30214
			{30213, 1, 1}, //half moon spectacles
			
	};

	public static final int[][] MEGA_RARE_TABLE = {
			{10350, 1, 1}, //3rd age full helmet
			{10348, 1, 1}, //3rd age platebody
			{10346, 1, 1}, //3rd age platelegs
			{123242, 1, 1}, //3rd age plateskirt
			{10352, 1, 1}, //3rd age kiteshield
			{10334, 1, 1}, //3rd age range coif
			{10330, 1, 1}, //3rd age range top
			{10332, 1, 1}, //3rd age age range legs
			{10336, 1, 1}, //3rd age vambraces
			{10342, 1, 1}, //3rd age mage hat
			{10338, 1, 1}, //3rd age robe top
			{10340, 1, 1}, //3rd age robe legs
			{10344, 1, 1}, //3rd age amulet
			{19317, 1, 1}, //3rd age druidic robe top
			{19320, 1, 1}, //3rd age druidic robe bottoms
			{19311, 1, 1}, //3rd age druidic cloak
			{30062, 1, 1}, //3rd age sword
			{30061, 1, 1}, //3rd age bow
			{30063, 1, 1}, //3rd age wand
			{123342, 1, 1}, //3rd age druidic staff
			{30060, 1, 1}, //3rd age cloak
			{30064, 1, 1}, //3rd age pickaxe
			{30065, 1, 1}, //3rd age axe
	};

	public static final int[][] RARE_UNIQUE = {
			//{30157, 1, 1}, //armadyl godsword ornament kit
			//{30157, 1, 1}, //bandos godsword ornament kit
			//{30157, 1, 1}, //saradomin godsword ornament kit
			//{30157, 1, 1}, //zamorak dogsword ornament kit
			{19350, 1, 1}, //dragon platebody ornament kit
			{30159, 1, 1}, //ankou mask
			{30160, 1, 1}, //ankou top
			{30161, 1, 1}, //ankou gloves
			{30162, 1, 1}, //ankou leggings
			{30163, 1, 1}, //ankou socks
			{30170, 1, 1}, //mummy's body
			{30169, 1, 1}, //mummy's head
			{30171, 1, 1}, //mummy's hands
			{30172, 1, 1}, //mummy's legs
			{30173, 1, 1}, //mummy's feet
			//{30157, 1, 1}, //dragon kiteshield ornament kit
			{30053, 1, 1}, //bucket helm g
			{30192, 1, 1}, //ring of coins
			{1965, 3, 3}, //cabbage
			{5944, 15, 15}, //anti-venom
			{220, 50, 50}, //torstol
			{30052, 1, 1}, //gilded scimitar
			{12391, 1, 1}, //gilded boots
			{123258, 1, 1}, //gilded coif
			{123261, 1, 1}, //gilded d'hide vambracers
			{123264, 1, 1}, //gilded d'hide body
			{123267, 1, 1}, //gilded d'hide chaps
			{123276, 1, 1}, //gilded pickaxe
			{123279, 1, 1}, //gilded axe
			{123282, 1, 1}, //gilded spade
			
	};
	
	public static final int[][] SUPER_RARE_UNIQUE = {
			{3486, 1, 1}, //gilded full helm
			{3481, 1, 1}, //gilded platebody
			{3483, 1, 1}, //gilded platelegs
			{3485, 1, 1}, //gilded plateskirt
			{3488, 1, 1}, //gilded kiteshield
			{120146, 1, 1}, //gilded med helm
			{120149, 1, 1}, //gilded chainbody
			{120152, 1, 1}, //gilded square shield 
			{120155, 1, 1}, //gilded 2h sword 
			{120158, 1, 1}, //gilded spear
			{120161, 1, 1}, //gilded hasta
	};

	public static final int[][] COMMON = { 
			{1215, 1, 1}, //dragon dagger
			{1434, 1, 1}, //dragon mace
			{1305, 1, 1}, //dragon longsword
			{4587, 1, 1}, //dragon scimitar
			{1377, 1, 1}, //dragon battleaxe
			{3204, 1, 1}, //dragon halberd
			{392, 15, 25}, //manta ray
			{561, 100, 200}, //nature rune
			{560, 100, 200}, //death rune
			{565, 100, 200}, //blood rune
			{566, 100, 200}, //soul rune
			{9245, 16, 25}, //onyx bolt e
			{246, 35, 50}, //wine of zamorak
			{226, 40, 60}, //limpwurt root
			{208, 5, 10}, //grimy ranarr weed
			{3050, 25, 35}, //grimy toadflax
			{3052, 5, 10}, //grimy snapdragon
			{452, 5, 8}, //runite ore
			{2364, 5, 7}, //runite bar
			{1748, 5, 25}, //black dragonhide
			{985, 1, 1}, //tooth half of key
			{987, 1, 1}, //loop half of key
			{5289, 1, 2}, //palm tree seed
			{5315, 1, 2}, //yew seed
			{5316, 1, 2}, //magic seed
	};

}
