package com.soulplay.content.minigames.treasuretrails.rewards;

public class EasyItemLoot {
	
	public static final int[][] COMMON_UNIQUE = { 
			{10366, 1, 1}, //amulet of magic (t)
			{10282, 1, 1}, //yew comp bow
	};
	
	public static final int[][] UNCOMMON_UNIQUE = { //20%
			//bronze (t)
			//bronze (g)
			//iron (t)
			//iron (g)
			{2583, 1, 1}, //black platebody (t)
			{2591, 1, 1}, //black platebody (g)
			{2587, 1, 1}, //black full helm (t)
			{2595, 1, 1}, //black full helm (g)
			{2585, 1, 1}, //black platelegs (t)
			{2593, 1, 1}, //black platelegs (g)
			{3472, 1, 1}, //black plateskirt (t)
			{3473, 1, 1}, //black plateskirt (g)
			{2589, 1, 1}, //black kiteshield (t)
			{2597, 1, 1}, //black kiteshield (g)
			{2635, 1, 1}, //black beret
			{2633, 1, 1}, //blue beret
			{2637, 1, 1}, //white beret
			//red beret
			{2631, 1, 1}, //highwayman mask
			//beanie
			{7390, 1, 1}, //blue wizard robe (g)
			{7392, 1, 1}, //blue wizard robe (t)
			{7394, 1, 1}, //blue wizard hat (g)
			{7396, 1, 1}, //blue wizard hat (t)
			{7386, 1, 1}, //blue wizard bottom (g)
			{7388, 1, 1}, //blue wizard bottom (t)
			//black wizard set (g)
			//black wizard set (t)
			{7362, 1, 1}, //studded body (g)
			{7364, 1, 1}, //studded body (t)
			{7366, 1, 1}, //studded chaps (g)
			{7368, 1, 1}, //studded chaps (t)
			{10306, 1, 1}, //black helm (h1)
			{10308, 1, 1}, //black helm (h2)
			{10310, 1, 1}, //black helm (h3)
			{10312, 1, 1}, //black helm (h4)
			{10314, 1, 1}, //black helm (h5)
			{7332, 1, 1}, //black shield (h1)
			{7338, 1, 1}, //black shield (h2)
			{7344, 1, 1}, //black shield (h3)
			{7350, 1, 1}, //black shield (h4)
			{7356, 1, 1}, //black shield (h5)
			//{10452, 1, 1}, //saradomin mitre
			{10458, 1, 1}, //saradomin robe top
			{10464, 1, 1}, //saradomin robe legs
			//{10454, 1, 1}, //guthix mitre
			{10462, 1, 1}, //guthix robe top
			{10466, 1, 1}, //guthix robe legs
			//{10456, 1, 1}, //zamorak mitre
			{10460, 1, 1}, //zamorak robe top
			{10468, 1, 1}, //zamorak robe legs	
			//{19378, 1, 1}, //ancient mitre
			{19382, 1, 1}, //ancient robe top
			{19390, 1, 1}, //ancient robe legs
			//{19376, 1, 1}, //bandos mitre
			{19384, 1, 1}, //bandos robe top
			{19388, 1, 1}, //bandos robe legs
			//{19374, 1, 1}, //armadyl mitre
			{19380, 1, 1}, //armadyl robe top
			{19386, 1, 1}, //armadyl robe legs
			{10714, 1, 1}, //red bob shirt
			{10715, 1, 1}, //blue bob shirt
			{10716, 1, 1}, //green bob shirt
			{10717, 1, 1}, //black bob shirt
			{10718, 1, 1}, //purple bob shirt
			{10398, 1, 1}, //sleeping cap
			{10396, 1, 1}, //pantaloons
			{10394, 1, 1}, //flared trousers
			{10392, 1, 1}, //a powdered wig
			{13095, 1, 1}, //black cane
			{30249, 1, 1}, //imp mask
			
			//rs3 rewards
			{13105, 1, 1}, //spiked helmet
	};
	
	public static final int[][] RARE_UNIQUE = { //unknown
			//steel (t)
			//steel(g)
			{30253, 1, 1}, //wooden shield (g)
			{30247, 1, 1}, //large spade
	};
	
	public static final int[][] SUPER_RARE_UNIQUE = { //1.6%
			{10408, 1, 1}, //blue elegant shirt
			{10410, 1, 1}, //blue elegant legs
			{10428, 1, 1}, //blue elegant blouse
			{10430, 1, 1}, //blue elegant skirt
			{10412, 1, 1}, //green elegant shirt
			{10414, 1, 1}, //green elegant legs
			{10432, 1, 1}, //green elegant blouse
			{10434, 1, 1}, //green elegant skirt
			//missing rest of green ele
			{10404, 1, 1}, //red elegant shirt
			{10406, 1, 1}, //red elegant legs
			{10424, 1, 1}, //red elegant blouse
			{10426, 1, 1}, //red elegant skirt
			//missing golden chefs hat
			//missing golden apron
	};
	
	public static final int[][] ULTRA_RARE_UNIQUE = { //0.16%
			{30250, 1, 1}, //team cape zero
			{30251, 1, 1}, //team cape x
			{30252, 1, 1}, //team cape i
	        //missing gold-trimmed monk's robes
	};

	public static final int[][] COMMON = { 
			//missing black pickaxe
			{1165, 1, 1}, //black full helm
			{1125, 1, 1}, //black platebody
			{1077, 1, 1}, //black platelegs
			{1297, 1, 1}, //black longsword
			{1367, 1, 1}, //black battleaxe
			{1361, 1, 1}, //black axe
			{1217, 1, 1}, //black pickaxe
			{1135, 1, 1}, //green d hide body
			{1099, 1, 1}, //green d hide chaps
			{857, 1, 1}, //yew shortbow
			{855, 1, 1}, //yew longbow
			{1395, 1, 1}, //water battlestaff
			{1725, 1, 1}, //amulet of strength
			{362, 8, 12}, //lobster
			{380, 6, 8}, //lobster
			{374, 6, 8}, //swordfish
			{556, 50, 80}, //air rune
			{558, 50, 80}, //mind rune
			{555, 50, 80}, //water rune
			{557, 50, 80}, //earth rune
			{554, 50, 80}, //fire rune
			{562, 10, 15}, //chaos rune
	};

}
