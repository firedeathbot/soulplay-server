package com.soulplay.content.minigames.treasuretrails.rewards;

public class HardItemLoot {
	
	public static final int[][] UNCOMMON_UNIQUE = { 
			{2627, 1, 1}, //rune full helm (t)
			{2623, 1, 1}, //rune platebody (t)
			{2625, 1, 1}, //rune platelegs (t)
			{3477, 1, 1}, //rune plateskirt (t)
			{2629, 1, 1}, //rune kiteshield (t)
			{2620, 1, 1}, //rune full helm (g)
			{2615, 1, 1}, //rune platebody (g)
			{2617, 1, 1}, //rune platelegs (g)
			{3476, 1, 1}, //rune plateskirt (g)
			{2621, 1, 1}, //rune kiteshield (g)
			{2673, 1, 1}, //guthix full helm
			{2669, 1, 1}, //guthix platebody
			{2671, 1, 1}, //guthix platelegs
			{3480, 1, 1}, //guthix plateskirt
			{2675, 1, 1}, //guthix kiteshield
			{2665, 1, 1}, //saradomin full helm
			{2661, 1, 1}, //saradomin platebody
			{2663, 1, 1}, //saradomin platelegs
			{3479, 1, 1}, //saradomin plateskirt
			{2667, 1, 1}, //saradomin kiteshield
			{2657, 1, 1}, //zamorak full helm
			{2653, 1, 1}, //zamorak platebody
			{2655, 1, 1}, //zamorak platelegs
			{3478, 1, 1}, //zamorak plateskirt
			{2659, 1, 1}, //zamorak kiteshield
			{19407, 1, 1}, //ancient full helm
			{19398, 1, 1}, //ancient platebody
			{19401, 1, 1}, //ancient platelegs
			{19404, 1, 1}, //ancient plateskirt
			{19410, 1, 1}, //ancient kiteshield
			{19437, 1, 1}, //bandos full helm
			{19428, 1, 1}, //bandos platebody
			{19431, 1, 1}, //bandos platelegs
			{19434, 1, 1}, //bandos plateskirt
			{19440, 1, 1}, //bandos kiteshield
			{19422, 1, 1}, //armadyl full helm
			{19413, 1, 1}, //armadyl platebody
			{19416, 1, 1}, //armadyl platelegs
			{19419, 1, 1}, //armadyl plateskirt
			{19425, 1, 1}, //armadyl kiteshield
			{10286, 1, 1}, //rune helm (h1)
			{10288, 1, 1}, //rune helm (h2)
			{10290, 1, 1}, //rune helm (h3)
			{10292, 1, 1}, //rune helm (h4)
			{10294, 1, 1}, //rune helm (h5)
			{10667, 1, 1}, //rune shield (h1)
			{10670, 1, 1}, //rune shield (h2)
			{10673, 1, 1}, //rune shield (h3)
			{10676, 1, 1}, //rune shield (h4)
			{10679, 1, 1}, //rune shield (h5)
			//missing blue d hide trimmed
			//missing blue d hide gold
			//missing red d hide trimmed
			//missing red d hide gold
			{7398, 1, 1}, //enchanted bottoms
			{7399, 1, 1}, //enchanted top
			{7400, 1, 1}, //enchanted hat
			{2581, 1, 1}, //robin hood hat
			{2639, 1, 1}, //tan cavalier
			{2641, 1, 1}, //dark cavalier
			{2643, 1, 1}, //black cavalier
			//missing white cavalier
			//missing red cavalier
			//missing blue cavalier
			{10376, 1, 1}, //guthix bracers
			{10378, 1, 1}, //guthix dragonhide top
			{10380, 1, 1}, //guthix chaps
			{10382, 1, 1}, //guthix coif
			{10384, 1, 1}, //saradomin bracers
			{10386, 1, 1}, //saradomin dragonhide top
			{10388, 1, 1}, //saradomin chaps
			{10390, 1, 1}, //saradomin coif
			{10368, 1, 1}, //zamorak bracers
			{10370, 1, 1}, //zamorak dragonhide top
			{10372, 1, 1}, //zamorak chaps
			{10374, 1, 1}, //zamorak coif
			{19459, 1, 1}, //armadyl vambraces
			{19461, 1, 1}, //armadyl dragonhide top
			{19463, 1, 1}, //armadyl chaps
			{19465, 1, 1}, //armadyl coif
			{19443, 1, 1}, //ancient vambraces
			{19445, 1, 1}, //ancient dragonhide top
			{19447, 1, 1}, //ancient chaps
			{19449, 1, 1}, //ancient coif
			{19451, 1, 1}, //brandos vambraces
			{19453, 1, 1}, //brandos dragonhide top
			{19455, 1, 1}, //brandos chaps
			{19457, 1, 1}, //brandos coif
			//missing blessed boots
			{10440, 1, 1}, //saradomin crozier
			{10444, 1, 1}, //zamorak crozier
			{10442, 1, 1}, //guthix crozier
			{10362, 1, 1}, //armadyl crozier
			{10364, 1, 1}, //bandos crozier
			{10366, 1, 1}, //ancient crozier
			{10470, 1, 1}, //saradomin stole
			{10472, 1, 1}, //guthix stole
			{10474, 1, 1}, //zamorak stole
			{19392, 1, 1}, //armadyl stole
			{19394, 1, 1}, //bandos stole
			{19396, 1, 1}, //ancient stole
			{19281, 1, 1}, //green dragon mask
			{19284, 1, 1}, //blue dragon mask
			{19287, 1, 1}, //red dragon mask
			{19290, 1, 1}, //black dragon mask
			{13103, 1, 1}, //pith helmet
			//explorer bckpack missing
			{13099, 1, 1}, //rune cane
			{30255, 1, 1}, //zombie head
			{30256, 1, 1}, //cyclops head
			{30257, 1, 1}, //nunchaku
			//missing dragon boots ornament kit	
	};

	public static final int[][] RARER_UNIQUE = {
			{10350, 1, 1}, //3rd age full helmet
			{10348, 1, 1}, //3rd age platebody
			{10346, 1, 1}, //3rd age platelegs
			{10352, 1, 1}, //3rd age kiteshield
			{10334, 1, 1}, //3rd age range coif
			{10330, 1, 1}, //3rd age range top
			{10332, 1, 1}, //3rd age age range legs
			{10336, 1, 1}, //3rd age vambraces
			{10342, 1, 1}, //3rd age mage hat
			{10338, 1, 1}, //3rd age robe top
			{10340, 1, 1}, //3rd age robe legs
			{10344, 1, 1}, //3rd age amulet
	};

	public static final int[][] RARE_UNIQUE = {
			{3486, 1, 1}, //gilded full helm
			{3481, 1, 1}, //gilded platebody
			{3483, 1, 1}, //gilded platelegs
			{3485, 1, 1}, //gilded plateskirt
			{3488, 1, 1}, //gilded kiteshield
			//{19425, 1, 1}, //gilded med helm MISSING
			//{19425, 1, 1}, //gilded chainbody MISSING
			//{19425, 1, 1}, //gilded square shield MISSING
			//{19425, 1, 1}, //gilded 2h sword MISSING
			//{19425, 1, 1}, //gilded spear MISSING
			//{19425, 1, 1}, //gilded hasta MISSING
	};

	public static final int[][] COMMON = { 
			{1163, 1, 1}, //rune full helm
			{1127, 1, 1}, //rune platebody
			{1079, 1, 1}, //rune platelegs
			{1093, 1, 1}, //rune plateskirt
			{1303, 1, 1}, //rune longsword
			{1373, 1, 1}, //rune battleaxe
			{1359, 1, 1}, //rune axe
			{1275, 1, 1}, //rune pickaxe
			{1135, 1, 1}, //green d hide body
			{1099, 1, 1}, //green d hide chaps
			{857, 1, 1}, //yew shortbow
			{855, 1, 1}, //yew longbow
			{3053, 1, 1}, //lava battlestaff
			{1393, 1, 1}, //lava battlestaff
			{1704, 1, 1}, //amulet of glory
			{386, 8, 12}, //shark
			{392, 8, 12}, //manta ray
			{556, 50, 150}, //air rune
			{558, 50, 150}, //mind rune
			{555, 50, 150}, //water rune
			{557, 50, 150}, //earth rune
			{554, 50, 150}, //fire rune
			{562, 10, 150}, //chaos rune
			{561, 10, 50}, //nature rune
			{563, 10, 50}, //law rune
			{560, 10, 50}, //death rune
	};

}
