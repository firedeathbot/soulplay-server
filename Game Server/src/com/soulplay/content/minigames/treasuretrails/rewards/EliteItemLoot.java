package com.soulplay.content.minigames.treasuretrails.rewards;

public class EliteItemLoot {
	
	public static final int[][] COMMON_UNIQUE = { 
	};
	
	public static final int[][] UNCOMMON_UNIQUE = { 
			{30218, 1, 1}, //dragon scimitar ornament kit (or)
			{19346, 1, 1}, //dragon full helm ornament kit (or)
			{19348, 1, 1}, //dragon platelegs/skirt ornament kit (or)
			{19352, 1, 1}, //dragon sq shield ornament kit (or)
			//dragon chainbody (or) kit missing
			{15503, 1, 1},//royal shirt
			{15505, 1, 1},//royal leggings
			{15507, 1, 1},//royal sceptre
			//missing infinity ornament kits
			{19333, 1, 1}, ////fury ornament kit (or)
			//missing musketeer outfit
			//missing black d hide chaps and body trimmer
			//missing black d hide chaps and body gold
			{30137, 1, 1}, //ranger's tunic
			{30136, 1, 1}, //ranger gloves
			{19296, 1, 1}, //iron dragon mask
			{19299, 1, 1}, //iron dragon mask
			{19302, 1, 1}, //steel dragon mask
			{19305, 1, 1}, //mithril dragon mask
			{19293, 1, 1}, //frost dragonmask
			{30144, 1, 1},//arceuus scarf
			{30147, 1, 1}, //hosidius scarf
			{30150, 1, 1}, //lovakengj scarf
			{30153, 1, 1}, //piscarilius scarf
			{30156, 1, 1}, //shayzien scarf
			//katana
			{19747, 1, 1},//rainbow afro
			{13101, 1, 1}, //top hat
			{30158, 1, 1}, ////dragon cane
			//missing brief case
			//missing sagacious spectacles
			{18650, 1, 1}, //craftsman's monocle
			//big pirate hat
			//deerstalker
			//great kourand scarfs
			//missing blacksmiths helm
			//missing bucket helm
			//missing holy wraps
			//missing dark tuxedo outfit
			//missing light tuxedo outfit
			//heavy casket should not be added to clues
	};
	
	public static final int[][] RARE_UNIQUE = {
			{30193, 1, 1}, //ring of nature
			{989, 1, 1}, //crystal key
			{30057, 1, 1}, //lava dragon mask
			{1392, 100, 100}, //battlestaff
			//{30157, 1, 1}, //extended antifire
			{3025, 30, 30}, //super restore
			{6686, 30, 30}, //saradomin brew
			{2445, 30, 30}, //ranging potion			
			{30052, 1, 1}, //gilded scimitar
			{12391, 1, 1}, //gilded boots
			{123258, 1, 1}, //gilded coif
			{123261, 1, 1}, //gilded d'hide vambracers
			{123264, 1, 1}, //gilded d'hide body
			{123267, 1, 1}, //gilded d'hide chaps
			{123276, 1, 1}, //gilded pickaxe
			{123279, 1, 1}, //gilded axe
			{123282, 1, 1}, //gilded spade
			
	};
	
	public static final int[][] SUPER_RARE_UNIQUE = {
			{3486, 1, 1}, //gilded full helm
			{3481, 1, 1}, //gilded platebody
			{3483, 1, 1}, //gilded platelegs
			{3485, 1, 1}, //gilded plateskirt
			{3488, 1, 1}, //gilded kiteshield
			{120146, 1, 1}, //gilded med helm
			{120149, 1, 1}, //gilded chainbody
			{120152, 1, 1}, //gilded square shield 
			{120155, 1, 1}, //gilded 2h sword 
			{120158, 1, 1}, //gilded spear
			{120161, 1, 1}, //gilded hasta
	};
	
	public static final int[][] MEGA_RARE_TABLE = {
			{10350, 1, 1}, //3rd age full helmet
			{10348, 1, 1}, //3rd age platebody
			{10346, 1, 1}, //3rd age platelegs
			{123242, 1, 1}, //3rd age plateskirt
			{10352, 1, 1}, //3rd age kiteshield
			{10334, 1, 1}, //3rd age range coif
			{10330, 1, 1}, //3rd age range top
			{10332, 1, 1}, //3rd age age range legs
			{10336, 1, 1}, //3rd age vambraces
			{10342, 1, 1}, //3rd age mage hat
			{10338, 1, 1}, //3rd age robe top
			{10340, 1, 1}, //3rd age robe legs
			{10344, 1, 1}, //3rd age amulet
			{19317, 1, 1}, //3rd age druidic robe top
			{19320, 1, 1}, //3rd age druidic robe bottoms
			{19311, 1, 1}, //3rd age druidic cloak
			{30062, 1, 1}, //3rd age sword
			{30061, 1, 1}, //3rd age bow
	};

	public static final int[][] COMMON = { 
			{1163, 1, 1}, //rune full helm
			{1127, 1, 1}, //rune platebody
			{1079, 1, 1}, //rune platelegs
			{1093, 1, 1}, //rune plateskirt
			{1303, 1, 1}, //rune longsword
			{1373, 1, 1}, //rune battleaxe
			{1359, 1, 1}, //rune axe
			{1275, 1, 1}, //rune pickaxe
			{1135, 1, 1}, //green d hide body
			{1099, 1, 1}, //green d hide chaps
			{857, 1, 1}, //yew shortbow
			{855, 1, 1}, //yew longbow
			{3053, 1, 1}, //lava battlestaff
			{1393, 1, 1}, //lava battlestaff
			{1704, 1, 1}, //amulet of glory
			{386, 8, 12}, //shark
			{392, 8, 12}, //manta ray
			{556, 50, 150}, //air rune
			{558, 50, 150}, //mind rune
			{555, 50, 150}, //water rune
			{557, 50, 150}, //earth rune
			{554, 50, 150}, //fire rune
			{562, 10, 150}, //chaos rune
			{561, 10, 50}, //nature rune
			{563, 10, 50}, //law rune
			{560, 10, 50}, //death rune
	};

}
