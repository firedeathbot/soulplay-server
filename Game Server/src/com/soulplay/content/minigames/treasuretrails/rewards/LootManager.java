package com.soulplay.content.minigames.treasuretrails.rewards;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class LootManager {

	public static void rollEasy(Player player) {
		int defaultRolls = 2 + Misc.random(2);
		List<Item> items = new ArrayList<>(defaultRolls);

		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(7000);
			if (random >= 6995) {
				addLoot(EasyItemLoot.ULTRA_RARE_UNIQUE, items);
			} else if (random >= 6960) {
				addLoot(EasyItemLoot.SUPER_RARE_UNIQUE, items);
			} else if (random >= 6951) {
				addLoot(EasyItemLoot.RARE_UNIQUE, items);
			} else if (random >= 6600) {
				addLoot(EasyItemLoot.UNCOMMON_UNIQUE, items);
			} else if (random >= 6560) {
				addLoot(EasyItemLoot.COMMON_UNIQUE, items);
			} else if (random >= 2050) {
				addLoot(EasyItemLoot.COMMON, items);
			} else {
				addItem(items, new Item(995, 100_000 + Misc.random(300_000)));
			}
		}

		for (int i = 0, length = items.size(); i < length; i++) {
			Item item = items.get(i);
			player.getItems().addItem(item.getId(), item.getAmount());
		}

		showReward((Client) player, items);
	}

	public static void rollMedium(Player player) {
		int defaultRolls = 3 + Misc.random(2);
		List<Item> items = new ArrayList<>(defaultRolls);

		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(7000);
			if (random >= 6994) {
				addLoot(MediumItemLoot.RARER, items);
			} else if (random >= 6970) {
				addLoot(MediumItemLoot.RARE_UNIQUE, items);
			} else if (random >= 6680) {
				addLoot(MediumItemLoot.UNCOMMON_UNIQUE, items);
			} else if (random >= 6635) {
				addLoot(MediumItemLoot.COMMON_UNIQUE, items);
			} else if (random >= 1500) {
				addLoot(MediumItemLoot.COMMON, items);
			} else {
				addItem(items, new Item(995, 100_000 + Misc.random(300_000)));
			}
		}

		for (int i = 0, length = items.size(); i < length; i++) {
			Item item = items.get(i);
			player.getItems().addItem(item.getId(), item.getAmount());
		}

		showReward((Client) player, items);
	}

	public static void rollHard(Player player) {
		int defaultRolls = 3 + Misc.random(2);
		List<Item> items = new ArrayList<>(defaultRolls);

		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(15000);
			if (random >= 14999) {
				addLoot(HardItemLoot.RARER_UNIQUE, items);
			} else if (random >= 14995) {
				addLoot(HardItemLoot.RARE_UNIQUE, items);
			} else if (random >= 13950) {
				addLoot(HardItemLoot.UNCOMMON_UNIQUE, items);
			} else if (random >= 1200) {
				addLoot(HardItemLoot.COMMON, items);
			} else {
				addItem(items, new Item(995, 1_000_000 + Misc.random(3_000_000)));
			}
		}

		for (int i = 0, length = items.size(); i < length; i++) {
			Item item = items.get(i);
			player.getItems().addItem(item.getId(), item.getAmount());
		}

		showReward((Client) player, items);
	}

	public static void rollElite(Player player) {
		int defaultRolls = 4 + Misc.random(2);
		List<Item> items = new ArrayList<>(defaultRolls);

		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(40_000);
			if (random >= 39_999) {
				addLoot(MasterItemLoot.MEGA_RARE_TABLE, items);
			} else if (random >= 39_995) {
				addLoot(MasterItemLoot.SUPER_RARE_UNIQUE, items);
			} else if (random >= 39_982) {
				addLoot(MasterItemLoot.RARE_UNIQUE, items);
			/*} else if (random >= 23_713) {
				addLoot(SharedItemLoot.COMMON, items);*/
			} else if (random >= 6_269) {
				addLoot(MasterItemLoot.COMMON, items);
			} else {
				addItem(items, new Item(995, 1_500_000 + Misc.random(3_000_000)));
			}
		}

		for (int i = 0, length = items.size(); i < length; i++) {
			Item item = items.get(i);
			player.getItems().addItem(item.getId(), item.getAmount());
		}

		showReward((Client) player, items);
	}
	
	public static void rollMaster(Player player) {
		int defaultRolls = 5 + Misc.random(2);
		List<Item> items = new ArrayList<>(defaultRolls);

		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(31_618);
			if (random >= 31_617) {
				addLoot(MasterItemLoot.MEGA_RARE_TABLE, items);
			} else if (random >= 31_614) {
				addLoot(MasterItemLoot.SUPER_RARE_UNIQUE, items);
			} else if (random >= 31_592) {
				addLoot(MasterItemLoot.RARE_UNIQUE, items);
			/*} else if (random >= 23_713) {
				addLoot(SharedItemLoot.COMMON, items);*/
			} else if (random >= 5_269) {
				addLoot(MasterItemLoot.COMMON, items);
			} else {
				addItem(items, new Item(995, 2_000_000 + Misc.random(5_000_000)));
			}
		}

		for (int i = 0, length = items.size(); i < length; i++) {
			Item item = items.get(i);
			player.getItems().addItem(item.getId(), item.getAmount());
		}

		showReward((Client) player, items);
	}

	public static void addLoot(int[][] table, List<Item> items) {
		int random = Misc.randomNoPlus(table.length);

		int itemId = table[random][0];
		int min = table[random][1];
		int max = table[random][2];
		int amount;
		if (min == max) {
			amount = min;
		} else {
			amount = Misc.randomNoPlus(min, max);
		}

		addItem(items, new Item(itemId, amount));
	}

	private static void addItem(List<Item> items, Item toAddItem) {
		int toAddId = toAddItem.getId();
		Item thisItem = null;
		int index = -1;

		for (int i = 0, length = items.size(); i < length; i++) {
			Item item = items.get(i);
			if (item.getId() == toAddId) {
				thisItem = item;
				index = i;
				break;
			}
		}

		if (index == -1) {
			items.add(toAddItem);
			return;
		}

		items.set(index, new Item(toAddId, toAddItem.getAmount() + thisItem.getAmount()));
	}

	private static void showReward(Client c, List<Item> items) {
		c.getPacketSender().itemsOnInterface(6963, items, 1);
		c.getPacketSender().showInterface(6960);
	}

}
