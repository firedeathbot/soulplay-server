package com.soulplay.content.minigames.treasuretrails;

import java.util.Arrays;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class TreasureTrailManager {

	public static boolean enabled = true;

	private final Player player;
	private final int[] cluesData;
	private final int[] cluesStage;
	private final int[] cluesLength;
//	private final int[] cluesCompleted;

	public TreasureTrailManager(Player player) {
		this.player = player;

		int length = ClueDifficulty.values.length;
		this.cluesData = new int[length];
		this.cluesStage = new int[length];
		this.cluesLength = new int[length];
//		this.cluesCompleted = new int[length];

		Arrays.fill(this.cluesData, -1);
	}

	public void addClue(ClueDifficulty type, boolean giveClue) {
		addClue(type, giveClue, hasClue(type));
	}

	public void addClue(ClueDifficulty type, boolean giveClue, boolean hasClue) {
		if (hasClue) {
			player.sendMessage("Already have a clue.");
			return;
		}

		startTrail(type);
		giveNewChallenge(type);
		if (giveClue) {
			player.getItems().addItem(type.getScrollId(), 1);
		}
	}

	public void giveNewChallenge(ClueDifficulty type) {
		cluesData[type.getIndex()] = ClueManager.getRandomChallenge(type).getData();
		//cluesData[type.getIndex()] = ClueManager.getChallengeByIndex(type, 19).getData(); // testing
	}

	public void startTrail(ClueDifficulty type) {
		int index = type.getIndex();

		cluesStage[index] = 0;
		cluesLength[index] = Misc.random(type.getMin(), type.getMax());
	}

	public void clearTrail(ClueDifficulty type) {
		int index = type.getIndex();

		cluesData[index] = -1;
		cluesStage[index] = 0;
		cluesLength[index] = 0;
	}

	public boolean hasClue(ClueDifficulty type) {
		if (player == null) {
			return true;
		}

		/*int casketId = type.getCasketId();
		if (player.getItems().playerHasItem(casketId)
				|| player.getItems().bankHasItemAmount(casketId, 1)
				|| player.getSummoning().getItemCount(casketId) > 0) {
			return true;
		}*/

		int scrollId = type.getScrollId();
		if (player.getItems().playerHasItem(scrollId)
				|| player.getItems().bankHasItemAmount(scrollId, 1)
				|| player.getSummoning().getItemCount(scrollId) > 0) {
			return true;
		}

		return false;
	}

	public void incrementStage(ClueDifficulty type) {
		cluesStage[type.getIndex()] += 1;
	}

	public void openClueThingy(ClueDifficulty type, int itemId) {
		if (!enabled) {
			player.sendMessage("Treasure trails are currently disabled.");
			return;
		}

		if (hasClue(type)) {
			player.sendMessage("Already have a clue.");
			return;
		}

		player.getItems().deleteItem2(itemId, 1);
		addClue(type, true);
	}

	public void rollClueThingy(int produceLevel, int rate, int skill, String message) {
		if (rate <= 0) {
			return;
		}

		int chance = rate / (100 + player.getSkills().getStaticLevel(skill));

		if (player.inWild()) {
			chance /= 2;
		}

		if (Misc.random(chance) != 0) {
			return;
		}

		ClueDifficulty diff;

		int level = produceLevel;
		if (level >= 80) {
			if (Misc.random(1) == 0) {
				diff = ClueDifficulty.ELITE;
			} else {
				diff = ClueDifficulty.HARD;
			}
		} else if (level >= 60) {
			if (Misc.random(1) == 0) {
				diff = ClueDifficulty.HARD;
			} else {
				diff = ClueDifficulty.MEDIUM;
			}
		} else if (level >= 40) {
			if (Misc.random(1) == 0) {
				diff = ClueDifficulty.MEDIUM;
			} else {
				diff = ClueDifficulty.EASY;
			}
		} else {
			diff = ClueDifficulty.EASY;
		}

		int item = diff.getNestId();
		if (skill == Skills.FISHING) {
			item = diff.getBottleId();
		} else if (skill == Skills.MINING) {
			item = diff.getGeodeId();
		}

		if (player.getTreasureTrailManager().giveClueThingy(diff, item)) {
			player.sendMessage(message);
		}
	}

	public boolean giveClueThingy(ClueDifficulty type, int itemId) {
		if (player.getItems().playerHasItem(itemId)) {
			return false;
		}

		if (player.getSummoning().getItemCount(itemId) > 0) {
			return false;
		}

		if (player.getItems().bankHasItemAmount(itemId, 1)) {
			return false;
		}

		player.getItems().addOrDrop(new GameItem(itemId, 1));
		return true;
	}

	public void nextStage(ClueDifficulty type) {
		int currentStage = getTrailStage(type);
		if (currentStage >= getTrailLength(type)) {
			clearTrail(type);
			incrementClues(type);
			player.getDialogueBuilder().sendItemStatement(2714, "You've obtained a casket!").onReset(() -> {
				NPC uri = (NPC) player.attr().get("uri");
				if (uri != null) {
					player.attr().remove("uri");
					uri.startAnimation(863);
					CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

						@Override
						public void execute(CycleEventContainer container) {
							uri.removeNpcSafe(false);
							Server.getStillGraphicsManager().createGfx(uri.absX, uri.absY, uri.heightLevel, Graphic.getOsrsId(86), 0, 0);
							container.stop();
						}

						@Override
						public void stop() {
						}
						
					}, 2);
				}
			}).execute();
			player.getItems().replaceItem(type.getScrollId(), type.getCasketId());
		} else {
			incrementStage(type);
			player.getDialogueBuilder().sendItemStatement(2677, "You've found a new clue!").onReset(() -> {
				NPC uri = (NPC) player.attr().get("uri");
				if (uri != null) {
					player.attr().remove("uri");
					uri.startAnimation(863);
					CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

						@Override
						public void execute(CycleEventContainer container) {
							uri.removeNpcSafe(false);
							Server.getStillGraphicsManager().createGfx(uri.absX, uri.absY, uri.heightLevel, Graphic.getOsrsId(86), 0, 0);
							container.stop();
						}

						@Override
						public void stop() {
						}
						
					}, 2);
				}
			}).execute();
			giveNewChallenge(type);
		}

		player.attr().remove("2stepemote");
		player.attr().remove("doubleagent");
	}

	public boolean isCompleted(ClueDifficulty type) {
		int index = type.getIndex();
		return cluesStage[index] != 0 && cluesLength[index] != 0 && cluesStage[index] >= cluesLength[index];
	}

	public boolean hasTrail(ClueDifficulty type) {
		int index = type.getIndex();
		return cluesData[index] != -1;
	}

	public void incrementClues(ClueDifficulty type) {
//		cluesCompleted[type.getIndex()]++;
		
		switch (type) {
		case EASY:
			getPlayer().getExtraHiscores().incClueEasyCompleted();
			break;
			
		case MEDIUM:
			getPlayer().getExtraHiscores().incClueMediumCompleted();
			break;
			
		case HARD:
			getPlayer().getExtraHiscores().incClueHardCompleted();
			break;
			
		case ELITE:
			getPlayer().getExtraHiscores().incClueEliteCompleted();
			break;
			
		case MASTER:
			getPlayer().getExtraHiscores().incClueMasterCompleted();
			break;
			
			default:
				break;
		}
	}

	public int getCompletedClues(ClueDifficulty type) {
		//return cluesCompleted[type.getIndex()];
		switch (type) {
		case EASY:
			return getPlayer().getExtraHiscores().getClueEasyCompleted();
			
		case MEDIUM:
			return getPlayer().getExtraHiscores().getClueMediumCompleted();
			
		case HARD:
			return getPlayer().getExtraHiscores().getClueHardCompleted();
			
		case ELITE:
			return getPlayer().getExtraHiscores().getClueEliteCompleted();
			
		case MASTER:
			return getPlayer().getExtraHiscores().getClueMasterCompleted();
			
			default:
				return 0;
		}
	}

	public Player getPlayer() {
		return player;
	}

	public int getCluesData(ClueDifficulty type) {
		return cluesData[type.getIndex()];
	}

	public int[] getCluesData() {
		return cluesData;
	}
	
	public int[] getCluesStage() {
		return cluesStage;
	}
	
	public int[] getCluesLength() {
		return cluesLength;
	}
	
	public int getTrailLength(ClueDifficulty type) {
		return cluesLength[type.getIndex()];
	}

	public int getTrailStage(ClueDifficulty type) {
		return cluesStage[type.getIndex()];
	}

}
