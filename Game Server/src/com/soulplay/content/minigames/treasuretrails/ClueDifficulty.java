package com.soulplay.content.minigames.treasuretrails;

import java.util.HashMap;
import java.util.Map;

public enum ClueDifficulty {

	EASY("Easy", 0, 2677, 2715, 2, 4, 30231, 30224, 30220),
	MEDIUM("Medium", 1, 2819, 13054, 3, 5, 30232, 30225, 30221),
	HARD("Hard", 2, 2739, 13037, 4, 6, 30233, 30226, 30222),
	ELITE("Elite", 3, 19043, 19039, 5, 7, 30234, 30227, 30223),
	MASTER("Master", 4, 19064, 13077, 6, 8, -1, -1, -1);

	public static final ClueDifficulty[] values = values();
	
	private static final Map<Integer, ClueDifficulty> map = new HashMap<>();
	
	public static void init() {
		for (ClueDifficulty clueDifficulty : ClueDifficulty.values) {
			map.put(clueDifficulty.getScrollId(), clueDifficulty);
		}
	}

	public static ClueDifficulty list(int itemId) {
		return map.get(itemId);
	}

	public static ClueDifficulty forScroll(int itemId) {
		return map.getOrDefault(itemId, EASY);
	}

	private final int index, scrollId, casketId, min, max, bottleId, geodeId, nestId;
	private final String name;

	private ClueDifficulty(String name, int index, int scrollId, int casketId, int min, int max, int bottleId, int geodeId, int nestId) {
		this.name = name;
		this.index = index;
		this.scrollId = scrollId;
		this.casketId = casketId;
		this.min = min;
		this.max = max;
		this.bottleId = bottleId;
		this.geodeId = geodeId;
		this.nestId = nestId;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}
	
	public int getScrollId() {
		return scrollId;
	}
	
	public int getCasketId() {
		return casketId;
	}
	
	public int getMin() {
		return min;
	}
	
	public int getMax() {
		return max;
	}

	public int getBottleId() {
		return bottleId;
	}

	public int getGeodeId() {
		return geodeId;
	}

	public int getNestId() {
		return nestId;
	}

}
