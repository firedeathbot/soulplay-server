package com.soulplay.content.minigames.treasuretrails;

import com.soulplay.game.model.player.Player;

public abstract class ClueType {

	private int data;

	public ClueType(int data) {
		this.data = data;
	}

	public int getData() {
		return data;
	}

	public abstract void openHint(Player player);

	public abstract void checkComplete(Player player, ClueDifficulty difficulty);

}
