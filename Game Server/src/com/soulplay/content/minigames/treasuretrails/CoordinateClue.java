package com.soulplay.content.minigames.treasuretrails;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.util.Misc;

public class CoordinateClue extends ClueType {

	private final Rectangle digLocation;

	private final String clue;

	public CoordinateClue(int index, int clueId, Rectangle digLocation, String clue) {
		super(Misc.toBitpack(index, clueId));
		this.digLocation = digLocation;
		this.clue = clue;
	}
	
	@Override
	public void openHint(Player player) {
        for (int i = 6968; i < 6976; i++) {
            player.getPacketSender().sendFrame126("", i);
        }

        String[] lines = clue.split("<br>");
        for (int i = 0; i < lines.length; i++) {
            player.getPacketSender().sendFrame126(lines[i], 6971 + i);
        }

        player.getPacketSender().showInterface(6965);
    }

	/*@Override
	public void openHint(Player player) {
		for (int i = 6968; i < 6976; i++) {
			player.getPacketSender().sendFrame126("", i);
		}

		player.getPacketSender().sendFrame126(clue, 6971);
		player.getPacketSender().showInterface(6965);
	}*/

	@Override
	public void checkComplete(Player player, ClueDifficulty difficulty) {
		if (digLocation.inside(player.getCurrentLocation()) && digLocation.getSouthWest().getZ() == player.getZ()) {
			player.getTreasureTrailManager().nextStage(difficulty);
		}
	}

}
