package com.soulplay.content.minigames.treasuretrails;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.util.Misc;

public class ClueManager {
	
	public static List<List<List<ClueType>>> clues;
	public static List<List<ClueType>> cluesGrouped;
	
	public static void load() {
		clues = new ArrayList<>(ClueDifficulty.values.length);

		//Don't change order of the load methoods.
		loadEasy();
		loadMedium();
		loadHard();
		loadElite();
		loadMaster();

		cluesGrouped = new ArrayList<>(ClueDifficulty.values.length);
		for (int i = 0; i < clues.size(); i++) {
			List<ClueType> allClues = new ArrayList<>();

			List<List<ClueType>> thisClues = clues.get(i);
			for (List<ClueType> clues2 : thisClues) {
				for (ClueType clue : clues2) {
					allClues.add(clue);
				}
			}

			cluesGrouped.add(allClues);
		}
	}

	private static void loadEasy() {
		List<List<ClueType>> cluesTypes = new ArrayList<>(2);
		clues.add(cluesTypes);

		List<ClueType> mapClues = new ArrayList<>(1);
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3166, 3360), 6994));//1
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3110, 3152), 9275));//3
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2612, 3482), 9108));//1
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3043, 3399), 7271));//5
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3289, 3374), 7045));//6
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2970, 3414), 17537));//7
		
		cluesTypes.add(mapClues);
		

		List<ClueType> emoteClues = new ArrayList<>(30);
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52055, Location.create(2602, 3278), 6, "Blow a raspberry at the", "monkey cage in Ardougne Zoo.", "Equip a studded leather body, bronze", "platelegs and a normal staff", "with no orb", new int[][] {{PlayerConstants.playerChest, 1133}, {PlayerConstants.playerLegs, 1075}, {PlayerConstants.playerWeapon, 1379}}));//1
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52055, Location.create(2762, 3402), 1, "Blow raspberries outside the", "entrance to Keep Le Faye.", "Equip a coif, an iron platebody", "and leather gloves.", "", new int[][] {{PlayerConstants.playerHat, 1169}, {PlayerConstants.playerChest, 1115}, {PlayerConstants.playerHands, 1059}}));//2
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(3313, 3242), 3, "Bow in the ticket office", "of the duel arena.", "Equip an iron chain body,", "leather chaps and a coif.", "", new int[][] {{PlayerConstants.playerChest, 1101}, {PlayerConstants.playerLegs, 1095}, {PlayerConstants.playerHat, 1169}}));//3
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(2728, 3348), 1, "Bow outside the entrance to", "the Legends' Guild.", "Equip iron platelegs, an", "emerald amulet and an oak", "longbow.", new int[][] {{PlayerConstants.playerLegs, 1067}, {PlayerConstants.playerAmulet, 1696}, {PlayerConstants.playerWeapon, 845}}));//4
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 171, Location.create(2925, 3482), 6, "Cheer at the Druids' circle.", "Equip a blue wizard hat, a bronze", "two-handed sword and HAM boots.", "", "", new int[][] {{PlayerConstants.playerHat, 579, 7394, 7396}, {PlayerConstants.playerWeapon, 1307}, {PlayerConstants.playerFeet, 4310}}));//5
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 171, Location.create(2207, 4959), 15, "Cheer at the games room.", "Have nothing equipped at all when", "you do.", "", "", new int[][] {{PlayerConstants.playerHat, -1}, {PlayerConstants.playerAmulet, -1}, {PlayerConstants.playerChest, -1}, {PlayerConstants.playerLegs, -1}, {PlayerConstants.playerCape, -1}, {PlayerConstants.playerArrows, -1}, {PlayerConstants.playerHands, -1}, {PlayerConstants.playerFeet, -1}, {PlayerConstants.playerRing, -1}, {PlayerConstants.playerShield, -1}, {PlayerConstants.playerWeapon, -1}}));//6
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 171, Location.create(3047, 3235), 8, "Cheer for the monks at", "Port Sarim.", "Equip a coif, steel", "plateskirt and a sapphire", "necklace.", new int[][] {{PlayerConstants.playerHat, 1169}, {PlayerConstants.playerLegs, 1083}, {PlayerConstants.playerAmulet, 1656}}));//7
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 172, Location.create(3362, 3340), 2, "Clap in the main exam room", "in the Exam Centre", "Equip a white apron, green gnome", "boots and leather gloves", "", new int[][] {{PlayerConstants.playerChest, 1005}, {PlayerConstants.playerFeet, 628}, {PlayerConstants.playerLegs, 1059}}));//8
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 172, Location.create(3114, 3189), 17, "Clap on the causeway to", "the Wizards' Tower.", "Equip an iron med helm,", "emerald ring and a white apron.", "", new int[][] {{PlayerConstants.playerHat, 1137}, {PlayerConstants.playerRing, 1639}, {PlayerConstants.playerChest, 1005}}));//9
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 172, Location.create(2633, 3385, 2), 4, "Clap on the top level of", "the mill, north of East Ardougne.", "Equip a blue gnome robe top, HAM", "robe bottom and an unenchanted tiara.", "", new int[][] {{PlayerConstants.playerChest, 640}, {PlayerConstants.playerLegs, 4300}, {PlayerConstants.playerHat, 5525}}));//10
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52051, Location.create(2611, 3390), 3, "Dance a jig by the entrance to", "the Fishing Guild.", "Equip an emerald ring, a sapphire", "amulet, and a bronce chain body.", "", new int[][] {{PlayerConstants.playerRing, 1639}, {PlayerConstants.playerAmulet, 1694}, {PlayerConstants.playerWeapon, 839}}));//11
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 166, Location.create(3109, 3295), 2, "Dance at the crossroads north", "of Draynor.", "Equip an iron chain body,", "a sapphire ring and a", "longbow", new int[][] {{PlayerConstants.playerChest, 1101}, {PlayerConstants.playerRing, 1637}, {PlayerConstants.playerWeapon, 839}}));//12
		//Dance at the entrance to the Grand Exchange. Equip a pink skirt, pink robe top and a body tiara.. 13
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 166, Location.create(2737, 3468), 8, "Dance in the Party Room.", "Equip a steel full helmet, steel", "platebody and an iron plateskirt.", "", "", new int[][] {{PlayerConstants.playerHat, 1157}, {PlayerConstants.playerChest, 1119}, {PlayerConstants.playerLegs, 1081}}));//14
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 166, Location.create(3203, 3168), 1, "Dance in the shack in Lumbridge", "swamp.", "Equip a bronze dagger, iron full", "helmet and a gold ring.", "", new int[][] {{PlayerConstants.playerWeapon, 1205}, {PlayerConstants.playerHat, 1153}, {PlayerConstants.playerRing, 1635}}));//15
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52051, Location.create(3253, 3401), 3, "Do a jig in Varrock's rune store.", "Equip an air tiara and a staff of", "water.", "", "", new int[][] {{PlayerConstants.playerHat, 5527}, {PlayerConstants.playerWeapon, 1383}}));//16
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52053, Location.create(3299, 3296), 16, "Headbang in the mine", "north of Al Kharid. Equip a", "polar camo top, leather", "gloves and leather boots.", "", new int[][] {{PlayerConstants.playerChest, 10065}, {PlayerConstants.playerHands, 1059}, {PlayerConstants.playerFeet, 1061}}));//17
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52054, Location.create(2759, 3444), 7, "Jump for joy at the beehives.", "Equip a desert shirt, green", "gnome robe bottoms and", "a steel axe.", "", new int[][] {{PlayerConstants.playerChest, 1833}, {PlayerConstants.playerLegs, 648}, {PlayerConstants.playerWeapon, 1353}}));//18
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 170, Location.create(2739, 3536), 3, "Laugh at the crossroads south", "of the Sinclair mansion.", "Equip a cowl, a blue wizard", "robe top and an iron scimitar.", "", new int[][] {{PlayerConstants.playerHat, 1167}, {PlayerConstants.playerChest, 577, 7390, 7392}, {PlayerConstants.playerWeapon, 1323}}));//19
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52050, Location.create(3371, 3499), 3, "Panic in the limestone mine.", "Equip bronze platelegs, a", "steel pickaxe and a steel", "medium helmet.", "", new int[][] {{PlayerConstants.playerLegs, 1075}, {PlayerConstants.playerWeapon, 1269}, {PlayerConstants.playerHat, 1141}}));//20
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52050, Location.create(2676, 3168), 5, "Panic on the pier where", "you catch the Fishing trawler.", "Have nothing equipped at all when", "you do", "", new int[][] {{PlayerConstants.playerHat, -1}, {PlayerConstants.playerAmulet, -1}, {PlayerConstants.playerChest, -1}, {PlayerConstants.playerLegs, -1}, {PlayerConstants.playerCape, -1}, {PlayerConstants.playerArrows, -1}, {PlayerConstants.playerHands, -1}, {PlayerConstants.playerFeet, -1}, {PlayerConstants.playerRing, -1}, {PlayerConstants.playerShield, -1}, {PlayerConstants.playerWeapon, -1}}));//21
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52058, Location.create(2977, 3239), 12, "Shrug in the mine near Rimmington.", "Equip a gold necklace", "a gold ring and a bronze", "spear.", "", new int[][] {{PlayerConstants.playerAmulet, 1654}, {PlayerConstants.playerRing, 1635}, {PlayerConstants.playerWeapon, 1237}}));//22
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52052, Location.create(2981, 3276), 3, "Spin at the crossroads north", "of Rimmington.", "Equip a green gnome hat, cream", "gnome top and leather chaps.", "", new int[][] {{PlayerConstants.playerHat, 658}, {PlayerConstants.playerChest, 642}, {PlayerConstants.playerLegs, 1095}}));//23
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52052, Location.create(3087, 3334), 3, "Spin in Draynor Manor by", "the fountain.", "Equip an iron platebody, studded", "leather chaps and a bronze", "full helmet.", new int[][] {{PlayerConstants.playerChest, 1115}, {PlayerConstants.playerLegs, 1097, 7366, 7368}, {PlayerConstants.playerHat, 1155}}));//24
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52052, Location.create(3213, 3652), 5, "Spin in the Varrock", "Castle courtyard.", "Equip a black axe, a coif", "and a ruby ring.", "", new int[][] {{PlayerConstants.playerWeapon, 1361}, {PlayerConstants.playerHat, 1169}, {PlayerConstants.playerRing, 1641}}));//25
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 162, Location.create(3158, 3299), 3, "Think in middle of the wheat", "field by the lumbridge mill.", "Equip a blue gnome robetop,", "a turquoise gnome robe bottom and", "an oak shortbow.", new int[][] {{PlayerConstants.playerChest, 640}, {PlayerConstants.playerLegs, 654}, {PlayerConstants.playerWeapon, 843}}));//26
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 163, Location.create(3307, 3492), 7, "Wave along the south fence", "of the Lumber Yard.", "Equip a hard leather body,", "leather chaps and a bronze axe.", "", new int[][] {{PlayerConstants.playerChest, 1131}, {PlayerConstants.playerLegs, 1095}, {PlayerConstants.playerWeapon, 1351}}));//27
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 163, Location.create(2945, 3335), 2, "Wave in the", "Falador gem store.", "Equip a mithril pickaxe, black", "platebody and an iron kiteshield.", "", new int[][] {{PlayerConstants.playerWeapon, 1273}, {PlayerConstants.playerChest, 1125}, {PlayerConstants.playerShield, 1191}}));//28
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 163, Location.create(2994, 3117), 13, "Wave on Mudskipper Point.", "Equip a black cape, leather", "chaps and a steel mace.", "", "", new int[][] {{PlayerConstants.playerCape, 1019}, {PlayerConstants.playerLegs, 1095}, {PlayerConstants.playerWeapon, 1424}}));//29
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52056, Location.create(3080, 3250), 5, "Yawn in Draynor Marketplace.", "Equip studded leather chaps, an iron", "kiteshield and a steel longsword.", "", "", new int[][] {{PlayerConstants.playerLegs, 1097}, {PlayerConstants.playerShield, 1191}, {PlayerConstants.playerWeapon, 1295}}));//30
		emoteClues.add(new EmoteClue(cluesTypes.size(), emoteClues.size(), 52056, Location.create(3211, 3494), 4, "Yawn in the Varrock library.", "Equip a green gnome robe top, HAM", "robe bottom and an iron", "warhammer.", "", new int[][] {{PlayerConstants.playerChest, 638}, {PlayerConstants.playerLegs, 4300}, {PlayerConstants.playerWeapon, 1335}}));//31
		cluesTypes.add(emoteClues);

	}

	private static void loadMedium() {
		List<List<ClueType>> cluesTypes = new ArrayList<>(4);
		clues.add(cluesTypes);
		
		List<ClueType> objectClues = new ArrayList<>(2);
		//MAP CLUES
		objectClues.add(new ObjectClue(cluesTypes.size(), objectClues.size(), 354, Location.create(2565, 3248), 9720)); //1
		objectClues.add(new ObjectClue(cluesTypes.size(), objectClues.size(), 354, Location.create(2658, 3488), 9196)); //2
		cluesTypes.add(objectClues);

		List<ClueType> mapClues = new ArrayList<>(7);
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2455, 3230), 17888));//4
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3434, 3266), 17774));//5
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2651, 3230), 9632));//6
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2536, 3865), 17687));//7
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2924, 3209), 9839));//8
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2906, 3293), 4305));//9
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3091, 3227), 7113));//11
		cluesTypes.add(mapClues);
		
		List<ClueType> coordinateClues = new ArrayList<>(35);
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2478, 3158, 0, 3), "00 degrees 05 minutes south<br>01 degrees 13 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2888, 3153, 0, 3), "00 degrees 13 minutes south<br>13 degrees 58 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2743, 3151, 0, 3), "00 degrees 18 minutes south<br>09 degrees 28 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3183, 3151, 0, 3), "00 degrees 20 minutes south<br>23 degrees 15 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3217, 3178, 0, 3), "00 degrees 30 minutes north<br>24 degrees 16 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3007, 3145, 0, 3), "00 degrees 31 minutes south<br>17 degrees 43 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2896, 3119, 0, 3), "01 degrees 18 minutes south<br>14 degrees 15 minutes east"));
		//DONT WORK Some strange island no idea how to get there must check.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2696, 3206, 0), "01 degrees 26 minutes north<br>08 degrees 01 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2680, 3111, 0, 3), "01 degrees 35 minutes south<br>07 degrees 28 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3510, 3075, 0, 3), "02 degrees 43 minutes south<br>33 degrees 26 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3161, 3251, 0, 3), "02 degrees 48 minutes north<br>22 degrees 30 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2644, 3251, 0, 3), "02 degrees 50 minutes north<br>06 degrees 20 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2322, 3060, 0, 3), "03 degrees 07 minutes south<br>03 degrees 41 minutes west"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2874, 3047, 0, 3), "03 degrees 35 minutes south<br>13 degrees 35 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2849, 3033, 0, 3), "04 degrees 00 minutes south<br>12 degrees 46 minutes east"));
		//DONT WORK Some crazy island no idea how we should make players get here.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2848, 3297, 0), "04 degrees 13 minutes north<br>12 degrees 45 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2583, 2990, 0, 3), "05 degrees 20 minutes south<br>04 degrees 28 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3179, 3343, 0, 3), "05 degrees 43 minutes north<br>23 degrees 05 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2383, 3369, 0, 3), "06 degrees 31 minutes north<br>01 degrees 46 minutes west"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3312, 3375, 0, 3), "06 degrees 41 minutes north<br>27 degrees 15 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3120, 3384, 0, 3), "06 degrees 58 minutes north<br>21 degrees 16 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3430, 3388, 0, 3), "07 degrees 05 minutes north<br>30 degrees 56 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2920, 3404, 0, 3), "07 degrees 33 minutes north<br>15 degrees 00 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2593, 2899, 0, 3), "08 degrees 11 minutes south<br>4 degrees 48 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2387, 3435, 0, 3), "08 degrees 33 minutes north<br>01 degrees 39 minutes west"));
		//new
		//DONT WORK Some crazy island no idea how we should make players get here.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2512, 3466, 0), "09 degrees 33 minutes north<br>02 degrees 15 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2382, 3467, 0, 3), "09 degrees 35 minutes north<br>01 degrees 50 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3006, 3475, 0, 3), "09 degrees 48 minutes north<br>17 degrees 39 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2585, 3505, 0, 3), "10 degrees 45 minutes north<br>04 degrees 31 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3442, 3515, 0, 3), "11 degrees 03 minutes north<br>31 degrees 20 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2416, 3515, 0, 3), "11 degrees 05 minutes north<br>00 degrees 45 minutes west"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3428, 3523, 0, 3), "11 degrees 18 minutes north<br>30 degrees 54 minutes east"));
		//DONT WORK Some crazy island no idea how we should make players get here.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2362, 3531, 0), "11 degrees 33 minutes north<br>02 degrees 24 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2920, 3534, 0, 3), "11 degrees 41 minutes north<br>14 degrees 58 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3548, 3559, 0, 3), "12 degrees 28 minutes north<br>34 degrees 37 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3391, 3592, 0, 3), "13 degrees 28 minutes north<br>29 degrees 43 minutes east"));
		//new OSRS only
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3391, 3592, 0), "14 degrees 20 minutes north<br>30 degrees 45 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2735, 3639, 0, 3), "14 degrees 54 minutes north<br>9 degrees 13 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2680, 3652, 0, 3), "15 degrees 22 minutes north<br>07 degrees 31 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2537, 3881, 0, 3), "22 degrees 30 minutes north<br>03 degrees 01 minutes east"));
		cluesTypes.add(coordinateClues);
		
		List<ClueType> emoteClues = new ArrayList<>(7);
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 167, 172, Location.create(2791, 3065), 13, "Beckon in Tai Bwo Wannai.", "Clap before you talk to me.", "Equip green dragonhide chaps, a", "ring of dueling and a mithril", "medium helmet.", new int[][] {{PlayerConstants.playerLegs, 1099, 7378, 7380}, {PlayerConstants.playerRing, 2552, 2554, 2556, 2558, 2560, 2562, 2564, 2566}, {PlayerConstants.playerHat, 1143}}));//1
		//emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 167, 172, Location.create(2791, 3065), 13, "Beckon in Tai Bwo Wannai.", "Clap before you talk to me.", "Equip green dragonhide chaps, a", "ring of dueling and a mithril", "medium helmet.", new int[][] {{PlayerConstants.playerLegs, 1099, 7378, 7380}, {PlayerConstants.playerRing, 2552, 2554, 2556, 2558, 2560, 2562, 2564, 2566}, {PlayerConstants.playerHat, 1143}}));//2 location not found on soulplay
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 167, 164, Location.create(3370, 3427), 3, "Beckon in the Digsite, near the eastern", "winch. Bow before you talk to me.", "Equip a green gnome hat, snakeskin", "boots and an iron pickaxe.", "", new int[][] {{PlayerConstants.playerHat, 658}, {PlayerConstants.playerFeet, 6328}, {PlayerConstants.playerWeapon, 1267}}));//3
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 171, 52053, Location.create(2541, 3549), 8, "Cheer in the Barbarian Agility Arena.", "Headbang before you talk to me.", "Equip a steel platebody,", "maple shortbow and a Wilderness", "cape.", new int[][] {{PlayerConstants.playerChest, 1119}, {PlayerConstants.playerWeapon, 853}, {PlayerConstants.playerCape, 4328, 4329, 4330, 4331, 4332, 4333, 4334, 4335, 4336, 4337, 4338, 4339, 4340, 4341, 4342, 4343, 4344, 4345, 4346, 4347, 4348, 4349, 4350, 4351, 4352, 4353, 4354, 4355, 4356, 4357, 4358, 4359, 4360, 4361, 4362, 4363, 4364, 4365, 4366, 4367, 4368, 4369, 4370, 4371, 4372, 4373, 4374, 4375, 4376, 4377, 4378, 4379, 4380, 4381, 4382, 4383, 4384, 4385, 4386, 4387, 4388, 4389, 4390, 4391, 4392, 4393, 4394, 4395, 4396, 4397, 4398, 4399, 4400, 4401, 4402, 4403, 4404, 4405, 4406, 4407, 4408, 4409, 4410, 4411, 4412, 4413, 4414}}));//4
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 171, 166, Location.create(3080, 3510), 3, "Cheer in the Edgeville general store.", "Dance before you talk to me. Equip a", "brown apron, leather boots and leather", "gloves.", "", new int[][] {{PlayerConstants.playerChest, 1757}, {PlayerConstants.playerFeet, 1061}, {PlayerConstants.playerHands, 1059}}));//5	
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 171, 165, Location.create(2528, 3375), 2, "Cheer in the Ogre Pen in the", "Training Camp. Show you are angry", "before you talk to me.", "Equip a green dragonhide body and", "chaps and a steel square shield.", new int[][] {{PlayerConstants.playerLegs, 1099}, {PlayerConstants.playerChest, 1135}, {PlayerConstants.playerShield, 1177}}));//6
		//emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 172, 52052, Location.create(2528, 3375), 2, "Clap in the Seers court house.", "Spin before you talk to me.", "Equip an adamant halberd, blue mystic", "robe bottom", "and a diamond ring.", new int[][] {{PlayerConstants.playerLegs, 1065}, {PlayerConstants.playerChest, 1135}, {PlayerConstants.playerShield, 1177}}));//7 no seers court house
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 161, 164, Location.create(2823, 3443), 2, "Cry in the Catherby Ranging shop.", "Bow before you talk to me.", "Equip blue gnome boots, a hard", "leather body and an unblessed silver", "sickle.", new int[][] {{PlayerConstants.playerFeet, 630}, {PlayerConstants.playerChest, 1131}, {PlayerConstants.playerWeapon, 2961}}));//8
		emoteClues.add(new Emote2StepClue(cluesTypes.size(), emoteClues.size(), 161, 52054, Location.create(3126, 3243), 5, "Cry in the Draynor village jail.", "Jump for joy before you talk to me.", "Equip an adamant sword, a sapphire", "amulet and an adamant plateskirt.", "", new int[][] {{PlayerConstants.playerWeapon, 1287}, {PlayerConstants.playerAmulet, 1694}, {PlayerConstants.playerLegs, 1091}}));//9
		
		//add more emote clues
		
		cluesTypes.add(emoteClues);
	}

	private static void loadHard() {
		List<List<ClueType>> cluesTypes = new ArrayList<>(3);
		clues.add(cluesTypes);
		
		List<ClueType> objectClues = new ArrayList<>(3);
		//MAP CLUES
		objectClues.add(new ObjectClue(cluesTypes.size(), objectClues.size(), 2620, Location.create(3309, 3503), 7221)); //1
		objectClues.add(new ObjectClue(cluesTypes.size(), objectClues.size(), 356, Location.create(2460, 3179), 9454)); //2
		objectClues.add(new ObjectClue(cluesTypes.size(), objectClues.size(), 100354, Location.create(3026, 3628), 9507)); //3
		cluesTypes.add(objectClues);

		List<ClueType> mapClues = new ArrayList<>(4);
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2616, 3077), 9043));//4
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(3021, 3911), 17620));//5
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2723, 3338), 17634));//6
		mapClues.add(new MapClue(cluesTypes.size(), mapClues.size(), Location.create(2488, 3308), 9359));//7
		cluesTypes.add(mapClues);
		
		List<ClueType> coordinateClues = new ArrayList<>(25);
		
		//new
		//DONTRectangle.createy island no idea how we should make players get here.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2208, 3161, 0), "00 degrees 00 minutes north<br>07 degrees 13 minutes west"));
		//new
		//DONT WORK Some crazy island no idea how we should make players get here.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2182, 3205, 0), "01 degrees 24 minutes north<br>08 degrees 05 minutes west"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3080, 3208, 0, 3), "01 degrees 30 minutes north<br>20 degrees 01 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3374, 3250, 0, 3), "02 degrees 46 minutes north<br>29 degrees 11 minutes east"));
		//new MISSING FAIRY RING ON SOULPLAY
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2700, 3250, 0), "02 degrees 48 minutes north<br>08 degrees 05 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3543, 3257, 0, 3), "02 degrees 58 minutes north<br>34 degrees 30 minutes east"));
		//new
		//DONT WORK Some crazy island no idea how we should make players get here.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2841, 3267, 0), "03 degrees 18 minutes north<br>12 degrees 31 minutes east"));
		//DONT WORK desert and cant reach further because of funpk zone.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3168, 3041, 0), "03 degrees 45 minutes south<br>22 degrees 45 minutes east"));
		//new
		//DONT WORK gotta jump over shit and stuff.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2542, 3031, 0), "04 degrees 03 minutes south<br>03 degrees 11 minutes east"));
		//new
		//DONT WORK gotta jump over shit and stuff.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2580, 3029, 0), "04 degrees 05 minutes south<br>04 degrees 24 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2961, 3024, 0, 3), "04 degrees 16 minutes south<br>16 degrees 16 minutes east"));
		//new
		//DONT WORK behind ardougne.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2339, 3310, 0), "04 degrees 41 minutes north<br>03 degrees 09 minutes west"));
		//new
		//DONT WORK in canafis swamp jump over crap.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3441, 3341, 0), "05 degrees 37 minutes north<br>31 degrees 15 minutes east"));
		//DONT WORK iclimb down some rocks.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2764, 2974, 0), "05 degrees 50 minutes south<br>10 degrees 05 minutes east"));
		//DONT WORK somewhere deep in karamja with blocked trees.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3139, 2969, 0), "06 degrees 00 minutes south<br>21 degrees 48 minutes east"));
		//new
		//DONT WORK somewhere far in the desert.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2924, 2963, 0), "06 degrees 11 minutes south<br>15 degrees 07 minutes east"));
		//new
		//DONT WORK somewhere deep in karamja with blocked trees.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2839, 2915, 0), "07 degrees 43 minutes south<br>12 degrees 26 minutes east"));
		//fixed coords
        coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3441, 3420, 0, 3), "08 degrees 03 minutes north<br>31 degrees 16 minutes east"));
		//new
		//DONT WORK somewhere deep in karamja with blocked trees.
       // coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2950, 2903, 0), "08 degrees 05 minutes south<br>15 degrees 56 minutes east"));
		//new
		//DONT WORK somewhere deep in karamja with blocked trees.
        //coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2774, 2891, 0), "08 degrees 26 minutes south<br>10 degrees 28 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3113, 3602, 0, 3), "13 degrees 46 minutes north<br>21 degrees 01 minutes east"));
		//fixed coords
		//DONT WORK somewhere deep in trollheim.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2892, 3675, 0), "16 degrees 03 minutes north<br>14 degrees 07 minutes east"));
		//OSRS Wildy coord fixed from bounty hunter
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3168, 3677, 0, 3), "16 degrees 07 minutes north<br>22 degrees 45 minutes east"));
		//DONT WORK somewhere deep in trollheim.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2853, 3689, 0), "16 degrees 31 minutes north<br>12 degrees 54 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3304, 3692, 0, 3), "16 degrees 35 minutes north<br>27 degrees 01 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3055, 3696, 0, 3), "16 degrees 43 minutes north<br>19 degrees 13 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3302, 3696, 0, 3), "16 degrees 43 minutes north<br>26 degrees 56 minutes east"));
		//new OSRS Map
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1478, 3695, 0), "16 degrees 43 minutes north<br>30 degrees 01 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2711, 3732, 0, 3), "17 degrees 50 minutes north<br>08 degrees 30 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2970, 3749, 0, 3), "18 degrees 22 minutes north<br>16 degrees 33 minutes east"));
		//OSRS Wildy coord fixed from bounty hunter
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3095, 3765, 0, 3), "18 degrees 50 minutes north<br>20 degrees 26 minutes east"));
		//new OSRS Wildy coord at clan wars
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3312, 3768, 0), "19 degrees 00 minutes north<br>27 degrees 13 minutes east"));
		//new OSRS Map
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1460, 3782, 0), "19 degrees 24 minutes north,<br>30 degrees 37 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3244, 3792, 0, 3), "19 degrees 43 minutes north<br>25 degrees 07 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3139, 3803, 0, 3), "20 degrees 05 minutes north<br>21 degrees 52 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2946, 3818, 0, 3), "20 degrees 33 minutes north<br>15 degrees 48 minutes east"));
		//new OSRS Map
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3771, 3825, 0), "20 degrees 45 minutes north<br>41 degrees 35 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3014, 3847, 0, 3), "21 degrees 24 minutes north<br>17 degrees 54 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3059, 3884, 0, 3), "22 degrees 35 minutes north<br>19 degrees 18 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3290, 3889, 0, 3), "22 degrees 45 minutes north<br>26 degrees 33 minutes east"));
		//new OSRS Map fossil island
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3770, 3897, 0), "23 degrees 00 minutes north<br>41 degrees 33 minutes east"));
		//new
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2505, 3899, 0, 3), "23 degrees 03 minutes north<br>02 degrees 01 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3285, 3943, 0, 3), "24 degrees 24 minutes north<br>26 degrees 24 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3158, 3958, 0, 3), "24 degrees 56 minutes north<br>22 degrees 28 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3040, 3961, 0, 3), "24 degrees 58 minutes north<br>18 degrees 43 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2987, 3964, 0, 3), "25 degrees 03 minutes north<br>17 degrees 05 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3190, 3963, 0, 3), "25 degrees 03 minutes north<br>23 degrees 24 minutes east"));
		cluesTypes.add(coordinateClues);
		
		List<ClueType> emoteClues = new ArrayList<>(5);
		//emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 167, Location.create(2975, 2928), 17, "Beckon on the east coast of the", "Kharazi", "jungle. Beware of double agents!", "Equip any vestment stole and a", "heraldic rune shield.", new int[][] {{PlayerConstants.playerHat,}, {PlayerConstants.playerWeapon, 
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 43092, Location.create(2852, 2954), 3, "Blow a kiss between the tables in", "Shilo Village bank.", "Beware of the double agents!", "Equip a blue mystic hat, bone", "spear and rune platebody.", new int[][] {{PlayerConstants.playerWeapon, 5016}, {PlayerConstants.playerHat, 4089}, {PlayerConstants.playerChest, 1127}})); 
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52055, Location.create(2588, 3419), 3, "Blow a raspberry in the Fishing Guild", "bank. Beware of double agents!", "Equip an elemental shield, blue", "dragonhide chaps and a rune", "warhammer.", new int[][] {{PlayerConstants.playerShield, 2890}, {PlayerConstants.playerLegs, 2493}, {PlayerConstants.playerWeapon, 1347}}));
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(2508, 3641, 2), 5, "Bow at the top of the lighthouse.", "Beware of double agents!", "Equip a blue dragonhide body,", "blue dragonhide vambraces and no", "jewelry.", new int[][] {{PlayerConstants.playerRing, -1}, {PlayerConstants.playerAmulet, -1}, {PlayerConstants.playerChest, 2499}, {PlayerConstants.playerHands, 2487}})); 
		
		//emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(2508, 3641), 5, "Bow at the top of the lighthouse.", "Beware of double agents!", "Equip a blue dragonhide body,", "blue dragonhide vambraces and no", "jewelry.", new int[][] {{PlayerConstants.playerRing, -1}, {PlayerConstants.playerAmulet, -1}, {PlayerConstants.playerChest, 2499}, {PlayerConstants.playerHands, 2487}})); 
		//emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(2508, 3641), 5, "Bow at the top of the lighthouse.", "Beware of double agents!", "Equip a blue dragonhide body,", "blue dragonhide vambraces and no", "jewelry.", new int[][] {{PlayerConstants.playerRing, -1}, {PlayerConstants.playerAmulet, -1}, {PlayerConstants.playerChest, 2499}, {PlayerConstants.playerHands, 2487}})); 
		
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52053, Location.create(3365, 3339), 8, "Headbang at the exam center. Beware", "of double", "agents! Equip a fire battlestaff,", "a diamond necklace and rune boots.", "", new int[][] {{PlayerConstants.playerWeapon, 1393}, {PlayerConstants.playerAmulet, 1662}, {PlayerConstants.playerChest, 2499}, {PlayerConstants.playerFeet, 4131}})); 
		
		//emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52053, Location.create(3365, 3339), 8, "Headbang at the exam center. Beware", "of double", "agents! Equip a fire battlestaff,", "a diamond necklace and rune boots.", "", new int[][] {{PlayerConstants.playerWeapon, 1393}, {PlayerConstants.playerAmulet, 1662}, {PlayerConstants.playerChest, 2499}, {PlayerConstants.playerFeet, 4131}})); 
		//emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52053, Location.create(3365, 3339), 8, "Headbang at the exam center. Beware", "of double", "agents! Equip a fire battlestaff,", "a diamond necklace and rune boots.", "", new int[][] {{PlayerConstants.playerWeapon, 1393}, {PlayerConstants.playerAmulet, 1662}, {PlayerConstants.playerChest, 2499}, {PlayerConstants.playerFeet, 4131}})); 
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52050, Location.create(2849, 3499), 4, "Panic by the pilot on White Wolf", "Mountain.", "Beware of double agents!", "Equip mithril platelegs, a ring of life", "and a rune axe.", new int[][] {{PlayerConstants.playerLegs, 1071}, {PlayerConstants.playerRing, 2570}, {PlayerConstants.playerWeapon, 1359}})); 
		
		
	}

	private static void loadElite() {
		List<List<ClueType>> cluesTypes = new ArrayList<>(2);
		clues.add(cluesTypes);


		List<ClueType> coordinateClues = new ArrayList<>(12);
		//DONT WORK crazy place behind ardougne.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2357, 3151, 0), "00 degrees 18 minutes south<br>02 degrees 35 minutes west"));
		//DONT WORK crazy place on the way to theatre of blood.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3587, 3180, 0), "00 degrees 35 minutes north<br>35 degrees 50 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2820, 3078, 0, 3), "02 degrees 35 minutes south<br>11 degrees 52 minutes east"));
		//mos le harmless soulplay dont got
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3811, 3060, 0), "03 degrees 09 minutes south<br>42 degrees 50 minutes east"));
		//DONT WORK crazy place behind ardougne.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2180, 3282, 0), "03 degrees 46 minutes north<br>08 degrees 07 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2870, 2997, 0, 3), "05 degrees 07 minutes south<br>13 degrees 26 minutes east"));
		//DONT WORK far in the desert.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3302, 2988, 0), "05 degrees 24 minutes south<br>26 degrees 56 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2511, 2980, 0, 3), "05 degrees 39 minutes south<br>02 degrees 13 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2732, 3372, 0, 3), "06 degrees 35 minutes north<br>09 degrees 07 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3573, 3425, 0, 3), "08 degrees 15 minutes north<br>35 degrees 24 minutes east"));
		//DONT WORK on some wierd island.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3828, 2848, 0), "09 degrees 46 minutes south,<br>43 degrees 22 minutes east"));
		//DONT WORK somewhere far in the desert.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3225, 2838, 0), "10 degrees 05 minutes south,<br>24 degrees 31 minutes east"));
		//OSRS MAP FOR 
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1773, 3510, 0), "10 degrees 54 minutes north,<br>20 degrees 50 minutes west"));
		//soulplay donator island
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2548, 3865, 0, 3), "22 degrees 0 minutes north,<br>3 degrees 22 minutes east"));
		//DONT WORK lots of jumping.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3603, 3564, 0), "12 degrees 35 minutes north, <br>36 degrees 20 minutes east"));
		//DONT WORK on crashed island.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2936, 2721, 0), "13 degrees 45 minutes south,<br>15 degrees 30 minutes east"));
		//DONT WORK monkey madness crap.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2697, 2705, 0), "14 degrees 15 minutes south,<br>08 degrees 01 minutes east"));
		//DONT WORK puoshing boulder.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2778, 3678, 0), "16 degrees 09 minutes north,<br>10 degrees 33 minutes east"));
		//DONT WORK somewhere deep in trollheim.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2827, 3740, 0), "18 degrees 05 minutes north,<br>12 degrees 05 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2359, 3799, 0, 3), "19 degrees 56 minutes north,<br>02 degrees 31 minutes west"));
		//DONT WORK some wierd island.
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2194, 3807, 0), "20 degrees 11 minutes north,<br>07 degrees 41 minutes west"));
		//MAP DOSENT EXIST ON SOULPLAY
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2700, 3808, 0), "20 degrees 13 minutes north,<br>08 degrees 07 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3215, 3835, 0, 3), "21 degrees 03 minutes north,<br>24 degrees 13 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3369, 3894, 0, 3), "22 degrees 54 minutes north,<br>29 degrees 01 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2065, 3923, 0, 3), "23 degrees 48 minutes north,<br>11 degrees 43 minutes west"));
		//OSRS MAP wilderness resource area
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3188, 3933, 0), "24 degrees 07 minutes north,<br>23 degrees 22 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2997, 3953, 0, 3), "24 degrees 45 minutes north,<br>17 degrees 24 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3339, 3930, 0, 3), "24 degrees 01 minutes north,<br>28 degrees 5 minutes east"));
		cluesTypes.add(coordinateClues);
		
		List<ClueType> emoteClues = new ArrayList<>(5);
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 43092, Location.create(2925, 5334, 2), 2, "Blow a kiss outside K'ril Tsutsaroth's", "chamber. Beware of double agents!", "Equip a Zamorak full helm and the", "shadow sword.", "", new int[][] {{PlayerConstants.playerHat, 2657}, {PlayerConstants.playerWeapon, 10858}}));//1
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52055, Location.create(2845, 3542), 3, "Blow a raspberry at the bank of the", "Warrior's guild. Beware of double", "agents! Equip a dragon battleaxe, a", "dragon defender and a slayer helm of", "any kind", new int[][] {{PlayerConstants.playerWeapon, 1377}, {PlayerConstants.playerShield, 20072}, {PlayerConstants.playerHat, 13263, 14636, 14637, 15492, 15496, 15497, 30072, 30073, 30074, 30075, 30076, 30077, 30330, 30331} }));//2
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(2204, 3252), 5, "Bow near Lord Iorwerth. Beware of", "double agents! Equip a", "crystal bow.", "", "", new int[][] {{PlayerConstants.playerWeapon, 4212, 4213, 4214, 4215, 4216, 4217, 4218, 4219, 4220, 4221, 4222, 4223}}));//3
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 171, Location.create(2850, 3349), 6, "Cheer in the Entrana church. Beware", "of double agents! Equip a full set of", "black dragonhide armour.", "", "", new int[][] {{PlayerConstants.playerHands, 2491}, {PlayerConstants.playerLegs, 2497}, {PlayerConstants.playerChest, 2503}}));//4
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 172, Location.create(3190, 3960), 3, "Clap in the magic axe hut. Beware of", "of double agents! Equip only some flared", "trousers.", "", "", new int[][] {{PlayerConstants.playerLegs, 10394}}));//5

		cluesTypes.add(emoteClues);
	}

	private static void loadMaster() {
		List<List<ClueType>> cluesTypes = new ArrayList<>(2);
		clues.add(cluesTypes);
		
		List<ClueType> coordinateClues = new ArrayList<>(7);
		//DONT WORK somewhere far in jungle behind ardougnee
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2178, 3209, 0, 3), "01 degrees 30 minutes north,<br>08 degrees 11 minutes west"));
		//DONT WORK somewhere far in jungle behind ardougne near zulrah swamp
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2155, 3100, 0), "01 degrees 54 minutes south,<br>08 degrees 54 minutes west"));
		//DONT WORK fair ring at zulrah missing
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2217, 3092, 0), "02 degrees 09 minutes south,<br>06 degrees 58 minutes west"));
		//DOES NOT EXIST ON SOULPLAY MAP
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3830, 3060, 0), "03 degrees 09 minutes south,<br>43 degrees 26 minutes east"));
		//crandor
		//DONT WORK crandor island
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2833, 3271, 0), "03 degrees 26 minutes north,<br>12 degrees 18 minutes east"));
		//DONT WORK maps dosent exist
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2732, 3284, 0), "03 degrees 50 minutes north,<br>09 degrees 07 minutes east"));
		//DONT WORK somewhere far in the crazy town near theatre of blood
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3622, 3320, 0), "04 degrees 58 minutes north,<br>36 degrees 56 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3570, 3407, 0, 3), "07 degrees 37 minutes north,<br>35 degrees 18 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2841, 3423, 0, 3), "08 degrees 11 minutes north,<br>12 degrees 30 minutes east"));
		//DONT WORK lots of jumping
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3604, 3564, 0), "12 degrees 35 minutes north,<br>36 degrees 22 minutes east"));
		//DONT WORK missing ladder climbing
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3085, 3569, 0), "12 degrees 45 minutes north,<br>20 degrees 09 minutes east"));
		//DONT WORK crashed island
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2934, 2727, 0), "13 degrees 33 minutes south,<br>15 degrees 26 minutes east"));
		//OSRS MAP .
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1451, 3695, 0), "16 degrees 41 minutes north,<br>30 degrees 54 minutes west"));
		//DONT WORK waterbirth island
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2538, 3739, 0), "18 degrees 03 minutes north, <br>03 degrees 03 minutes east"));
		//OSRS MAP 
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1698, 3792, 0), "19 degrees 43 minutes north,<br>23 degrees 11 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2951, 3820, 0, 3), "20 degrees 35 minutes north,<br>15 degrees 58 minutes east"));
		//DONT WORK some wierd island
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2202, 3825, 0), "20 degrees 45 minutes north,<br>7 degrees 26 degrees west"));
		//OSRS MAP
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1761, 3853, 0), "21 degrees 37 minutes north,<br>21 degrees 13 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2090, 3863, 0, 3), "21 degrees 56 minutes north,<br>10 degrees 56 minutes west"));
		//OSRS MAP
		//coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(1442, 3878, 0), "22 degrees 24 minutes north, <br>31 degrees 11 minutes west"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3367, 3929, 0, 3), "24 degrees 00 minutes north,<br>28 degrees 58 minutes east"));
		//OSRS MAP WILDERNESS SKILLING RESOURCE AREA
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(3304, 3940, 0, 3), "24 degrees 22 minutes north,<br>27 degrees 00 minutes east"));
		coordinateClues.add(new CoordinateClue(cluesTypes.size(), coordinateClues.size(), Rectangle.create(2994, 3961, 0, 3), "25 degrees 00 minutes north,<br>17 degrees 18 minutes east"));
		cluesTypes.add(coordinateClues);
		
		List<ClueType> emoteClues = new ArrayList<>(5);
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 43092, Location.create(2925, 5334, 2), 2, "Blow a kiss outside K'ril Tsutsaroth's", "chamber. Beware of double agents!", "Equip a Zamorak full helm and the", "shadow sword.", "", new int[][] {{PlayerConstants.playerHat, 2657}, {PlayerConstants.playerWeapon, 10858}}));//1
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 52055, Location.create(2845, 3542), 3, "Blow a raspberry at the bank of the", "Warrior's guild. Beware of double", "agents! Equip a dragon battleaxe, a", "dragon defender and a slayer helm of", "any kind", new int[][] {{PlayerConstants.playerWeapon, 1377}, {PlayerConstants.playerShield, 20072}, {PlayerConstants.playerHat, 13263, 14636, 14637, 15492, 15496, 15497, 30072, 30073, 30074, 30075, 30076, 30077, 30330, 30331} }));//2
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 164, Location.create(2204, 3252), 5, "Bow near Lord Iorwerth. Beware of", "double agents! Equip a", "crystal bow.", "", "", new int[][] {{PlayerConstants.playerWeapon, 4212, 4213, 4214, 4215, 4216, 4217, 4218, 4219, 4220, 4221, 4222, 4223}}));//3
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 171, Location.create(2850, 3349), 6, "Cheer in the Entrana church. Beware", "of double agents! Equip a full set of", "black dragonhide armour.", "", "", new int[][] {{PlayerConstants.playerHands, 2491}, {PlayerConstants.playerLegs, 2497}, {PlayerConstants.playerChest, 2503}}));//4
		emoteClues.add(new EmoteDoubleAgentClue(cluesTypes.size(), emoteClues.size(), 172, Location.create(3190, 3960), 3, "Clap in the magic axe hut. Beware of", "of double agents! Equip only some flared", "trousers.", "", "", new int[][] {{PlayerConstants.playerLegs, 10394}}));//5

		cluesTypes.add(emoteClues);
	}

	public static ClueType getRandomChallenge(ClueDifficulty difficulty) {
		List<ClueType> clues = cluesGrouped.get(difficulty.getIndex());
		return clues.get(Misc.randomNoPlus(clues.size()));
	}
	
	public static ClueType getChallengeByIndex(ClueDifficulty difficulty, int index) {
		List<ClueType> clues = cluesGrouped.get(difficulty.getIndex());
		return clues.get(index);
	}

	public static final String[] URI_QUOTES = new String[]{"Once, I was a poor man, but then I found a party hat.", "There were three goblins in a bar, which one left first?", "Would you like to buy a pewter spoon?", "In the end, only the three-legged survive.", "I heard that the tall man fears only strong winds.", "In Canifis the men are known for eating much spam.", "I am the egg man, are you one of the egg men?", "I believe that it is very rainy in Varrock.", "The slowest of fishermen catch the swiftest of fish.", "It is quite easy being green.", "Don't forget to find the jade monkey.", "Don't forget to find the jade monkey.", "Do you want ants? Because that's how you get ants.", "I once named a duck after a girl. Big mistake.", "Loser says what.", "I'm looking for a girl named Molly. I can't find her.", "Guys, let's lake dive!", "I gave you what you needed; not what you think you needed.", "Want to see me bend a spoon?", "Is that Deziree?", "This is the last night you'll spend alone.", "(Breathing intensifies)", "Init doe. Lyk, I hope yer reward iz goodd aye?"};

	public static void spawnUri(Player c) {
		 NPC npc = NPCHandler.spawnNpc(c, 5141, c.absX + 1, c.absY, c.heightLevel, 0, false, false);
		 npc.startGraphic(Graphic.create(Graphic.getOsrsId(86)));
		 c.attr().put("uri", npc);
	}

	public static void spawnDoubleAgent(Player c) {
		int slot = Misc.findFreeNpcSlot();
		
		if (slot == -1) {
			return;
		}
		
		DoubleAgentNpc npc = new DoubleAgentNpc(slot, 5145);
		npc.setKillerId(c.getId());
		npc.spawnedBy = c.getId();
		NPCHandler.spawnNpc(npc, c.absX + 1, c.absY, c.heightLevel, 0);
	}

}
