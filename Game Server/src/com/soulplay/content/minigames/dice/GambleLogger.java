package com.soulplay.content.minigames.dice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.WebsiteSQL;

public class GambleLogger {
	
	public final Player p;
	
	public int otherMID = -1;
	
	public GambleLogger(Player p) {
		this.p = p;
	}
	
	public void hookTrade(Player other) {
		otherMID = other.mySQLIndex;
		//new trade, so clear previous logs
		myLogs.clear();
		otherLogs.clear();
	}
	
	private List<GambleLog> otherLogs = new ArrayList<>();
	
	private List<GambleLog> myLogs = new ArrayList<>();
	
	public void logFlower(int objectId) {
		GambleLog log = new GambleLog(GambleType.FLOWER, 0, FlowerColor.forId(objectId));
		addLogToList(log);
	}
	
	public void logDice(GambleType type, int roll) {
		GambleLog log = new GambleLog(type, roll, FlowerColor.NOT_ADDED);
		addLogToList(log);
	}
	
	private void addLogToList(GambleLog log) {
		
		myLogs.add(log);
		
		Player lastTradePlayer = PlayerHandler.getPlayerByMID(otherMID);
		
		if (lastTradePlayer != null && lastTradePlayer.isActive) {
			lastTradePlayer.getGambleLogger().otherLogs.add(log);
		}
	}
	
	public void sendReport() {
		if (!Config.RUN_ON_DEDI || otherMID == -1)
			return;
		
		if (!p.getStopWatch().checkThenStart(2)) {
			p.sendMessage("Error when sending report!");
			p.sendMessage("Please try again in a few seconds!");
			return;
		}
		
		p.sendMessage("Sending scam report.");
		
		final int reportedBy = p.mySQLIndex;
		final int scammer = otherMID;
		
//		final String myActions = PlayerSaveSql.createJsonString(myLogs);
//		final String scammerActions = PlayerSaveSql.createJsonString(otherLogs);
		
		final List<GambleLog> myLog = new ArrayList<>(this.myLogs);
		final List<GambleLog> otherLog = new ArrayList<>(this.otherLogs);
		
		this.myLogs.clear();
		this.otherLogs.clear();
		
		final Timestamp timeStamp = Misc.getESTTimestamp();
		
		TaskExecutor.executeWebsiteSQL(() -> {

			try (Connection connection = WebsiteSQL.getHikari().getConnection();
					PreparedStatement ps = connection.prepareStatement(QUERY);) {
				
				
				createBatch(ps, myLog, reportedBy, false, timeStamp);
				createBatch(ps, otherLog, scammer, true, timeStamp);
				
				ps.executeBatch();

			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
		
	}
	
	private static void createBatch(PreparedStatement ps, List<GambleLog> list, int playerMid, boolean isScammer, Timestamp timeStamp) throws SQLException {
		for (int i = 0, len = list.size(); i < len; i++) {
			GambleLog log = list.get(i);
			
			ps.setInt(1, playerMid);
			ps.setBoolean(2, isScammer);
			ps.setInt(3, log.getType().ordinal());
			ps.setInt(4, log.getColor().ordinal());
			ps.setInt(5, log.getRoll());
			ps.setTimestamp(6, timeStamp);
			ps.addBatch();
		}
	}
	
	private static final String QUERY = "INSERT INTO gamble_report (player_id, scammer, type, flower, roll, timestamp) VALUES(?, ?, ?, ?, ?, ?)";

}
