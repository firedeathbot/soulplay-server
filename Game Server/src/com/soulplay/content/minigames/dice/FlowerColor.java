package com.soulplay.content.minigames.dice;

import java.util.HashMap;
import java.util.Map;

public enum FlowerColor {

	NOT_ADDED(0),
	BLUE_PURPLE(2980),
	RED(2981),
	BLUE(2982),
	YELLOW(2983),
	PURPLE(2984),
	ORANGE(2985),
	RED_YELLOW_BLUE(2986),
	WHITE(2987),
	BLACK(2988);
	
	private final int objectId;
	
	private FlowerColor(int obj) {
		this.objectId = obj;
	}
	
	private static Map<Integer, FlowerColor> map = new HashMap<>();
	
	static {
		for (FlowerColor col : values()) {
			map.put(col.objectId, col);
		}
	}
	
	public static FlowerColor forId(int objectId) {
		return map.getOrDefault(objectId, NOT_ADDED);
	}
}
