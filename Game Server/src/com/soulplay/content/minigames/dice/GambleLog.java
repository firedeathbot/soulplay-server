package com.soulplay.content.minigames.dice;

public class GambleLog {
	
	private final GambleType type;
	
	private final Integer roll;
	
	private final FlowerColor color;
	
	public GambleLog(GambleType type, Integer roll, FlowerColor color) {
		this.type = type;
		this.roll = roll;
		this.color = color;
	}

	public GambleType getType() {
		return type;
	}

	public int getRoll() {
		return roll;
	}

	public FlowerColor getColor() {
		return color;
	}
	
	

}
