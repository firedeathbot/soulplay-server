package com.soulplay.content.minigames;

import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.minigames.pestcontrol.PestDoorHandler;
import com.soulplay.content.minigames.pestcontrol.npcs.Defiler;
import com.soulplay.content.minigames.pestcontrol.npcs.Shifter;
import com.soulplay.content.minigames.pestcontrol.npcs.Spinner;
import com.soulplay.content.minigames.pestcontrol.npcs.Torcher;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

public class PestControl {

	private final static int PLAYER_SIZE = 25;

	private final static int NPC_SIZE = 50;

	private final static int PLAYERS_REQUIRED_TO_START = 5;

	public static boolean EXTRA_REWARD = false;

	public static Player[] players = new Player[PLAYER_SIZE];

	public static NPC[] npcs = new NPC[NPC_SIZE];

	public static final int GAME_TIMER = 300;

	// PestDoorHandler pestDoorHandler = new PestDoorHandler();
	//
	// public PestDoorHandler getPestDoor() {
	// return pestDoorHandler;
	// }

	public static final int WAIT_TIMER = 40;

	public static int gameTimer = -1;

	public static int waitTimer = 40;

	public static int properTimer = 0;

	public static int Portal1kill = 0;

	public static int Portal2kill = 0;

	public static int Portal3kill = 0;

	public static int Portal4kill = 0;

	public static int portal0Index = 0;

	public static int portal1Index = 0;

	public static int portal2Index = 0;

	public static int portal3Index = 0;

	public static int portalHP1 = 0;

	public static int portalHP2 = 0;

	public static int portalHP3 = 0;

	public static int portalHP4 = 0;

	public static int knightHP = 0;

	public static void addNpc(NPC n) {
		for (int i = 0; i < npcs.length; i++) {
			if (npcs[i] == null) {
				npcs[i] = n;
				if (n.npcType == 6142) {
					portal0Index = i;
				} else if (n.npcType == 6143) {
					portal1Index = i;
				} else if (n.npcType == 6144) {
					portal2Index = i;
				} else if (n.npcType == 6145) {
					portal3Index = i;
				}
				updateNpcHP(n);
				if (NPCHandler.isShifter(n.npcType)) {
					new Shifter(n);
				} else if (NPCHandler.isSpinner(n.npcType)) {
					new Spinner(n);
				} else if (NPCHandler.isDefiler(n.npcType)) {
					new Defiler(n);
				} else if (NPCHandler.isTorcher(n.npcType)) {
					new Torcher(n);
				}
				return;
			}
		}
	}

	public static boolean allPortalsDead() {
		int count = 0;
		// for (NPC npc : npcs) {
		// if (npc == null) continue;
		// if (npc.npcType >= 6142 && npc.npcType <= 6145)
		// if (npc.needRespawn)
		// count++;
		// }

		if ((npcs[portal0Index] == null || npcs[portal0Index].isDead())
				&& (npcs[portal1Index] == null || npcs[portal1Index].isDead())
				&& (npcs[portal2Index] == null || npcs[portal2Index].isDead())
				&& (npcs[portal3Index] == null || npcs[portal3Index].isDead())) {
			return true;
		}

		if (portalHP1 <= 0) {
			count++;
		}
		if (portalHP2 <= 0) {
			count++;
		}
		if (portalHP3 <= 0) {
			count++;
		}
		if (portalHP4 <= 0) {
			count++;
		}
		return count >= 4;
	}

	public static void endGame(boolean won) {
		gameTimer = -1;
		waitTimer = WAIT_TIMER;
		for (Player p : players) {
			if (p == null) {
				continue;
			}
			if (p.inPcGame()) {
				p.resetAnimations();
				Client c = (Client) p;
				c.getPA().safeDeathSecureTimer();
				c.getPA().restorePlayer(false);
				c.getPA().movePlayer(2657, 2639, 0);
				if (won /* && c.pcDamage > 4 */) {
					c.getExtraHiscores().incPestControlWins();
					// c.getDH().sendDialogues(79, 3790);
					c.getAchievement().win10PestControl();
					c.getAchievement().win50PestControl();
					Titles.winFiftyPestControl(c);
					if (c.pcParticipation > 50) {
						int winAmount = 4;
						if (c.pcParticipation > 100) {
							winAmount += (c.pcParticipation - 50) / 50;
						}
						c.sendMessage("You receive " + winAmount + " PC Points.");
						if (c.getDonatedTotalAmount() > 4999) {
							winAmount += 3;
							c.sendMessage(
									"You have received 3 extra PC points.");
						} else if (c.getDonatedTotalAmount() > 249) {
							winAmount += 2;
							c.sendMessage(
									"You have received 2 extra PC points.");
						} else if (c.getDonatedTotalAmount() > 9) {
							winAmount += 1;
							c.sendMessage(
									"You have received 1 extra PC point.");
						}
						if (EXTRA_REWARD) {
							winAmount += 3;
							c.sendMessage("You have received extra PC Points from the Pest Control event!");
						}
						if (c.getClan() != null) {
							c.getClan().addClanPoints(c, 15, "pest_control");
						}
						if (c.getClan() != null) {
							if (c.getClan().getClanPoints() > 101332
									&& c.getClan().getClanPoints() < 3258594) {
								winAmount += 1;
							} else if (c.getClan().getClanPoints() > 3258593
									&& c.getClan().getClanPoints() < 38737661) {
								winAmount += 2;
							} else if (c.getClan().getClanPoints() > 38737660) {
								winAmount += 3;
							}
						}
						c.setPcPoints(c.getPcPoints() + winAmount);
						c.sendMessage(
								"You currently have <col=ff0000>" + c.getPcPoints()
										+ "</col> Pest Control Points.");
						// c.getPacketSender().sendFrame126("<col=ffb000>Pest-Control
						// Points:
						// <col=ff00>" + c.getPcPoints() + "", 29166);
					} else {
						c.sendMessage(
								"You did not participate enough in the game to receive points.");
						Titles.lazyPestControl(c);
					}

					// } else if (won) {
					// c.getDH().sendDialogues(77, 3790);
				} else {
					c.sendMessage("You did not win the pest control.");
					// c.getDH().sendDialogues(78, 3790);
				}
				// c.pcDamage = 0;

				if (!WorldType.equalsType(WorldType.SPAWN)) {
					c.getItems().addItem(995, c.combatLevel * 54);
				}

				c.getCombat().resetPrayers();
				
				c.clearLists();

			}
		}

		ObjectManager.revertObjectsByClass(3);

		Portal1kill = 0;
		Portal2kill = 0;
		Portal3kill = 0;
		Portal4kill = 0;

		portalHP1 = 0;
		portalHP2 = 0;
		portalHP3 = 0;
		portalHP4 = 0;

		// for (NPC npc : npcs) {
		for (int i = 0; i < npcs.length; i++) {
			if (npcs[i] == null) {
				continue;
			}
			npcs[i].setDead(true);
			// npcs[i] = null;
			// npcs[i].destroy();
			// NPCHandler.npcs[npcs[i].npcIndex] = null;
			// npcs[i] = null;
		}

		players = new Player[PLAYER_SIZE];
		npcs = new NPC[NPC_SIZE];

		portal0Index = portal1Index = portal2Index = portal3Index = 0;
	}

	public static int getIngamePlayerCount() {
		int count = 0;
		for (Player p : players) {
			if (p == null) {
				continue;
			}
			if (!p.inPcGame()) {
				continue;
			}
			count++;
		}
		return count;
	}

	public static NPC getKnight() {
		for (NPC n : npcs) {
			if (n == null) {
				continue;
			}
			if (n.npcType == 3782) {
				return n;
			}
		}
		return null;
	}

	public static void movePlayerToGame(Player p) {
		Client c = (Client) p;
		c.getPA().movePlayer(c.getX() - 4, c.getY() - 29, 0);
		c.getPA().walkableInterface(21100);
		c.getPacketSender().sendFrame126("0", 21116);
		for (int i = 0; i < players.length; i++) {
			if (players[i] == null) {
				players[i] = p;
				return;
			}
		}
	}

	public static int playersInBoat() {
		int count = 0;
		// for (int j = 0; j < PlayerHandler.players.length; j++) {
		// if (PlayerHandler.players[j] != null) {
		// if (PlayerHandler.players[j].inPcBoat()) {
		// count++;
		// }
		// }
		// }
		// System.out.println("players
		// :"+Server.getRegionManager().getRegion(2661,
		// 2641).getPlayers().size());
		for (Player p : Server.getRegionManager()
				.getRegionByLocation(2661, 2641, 0).getPlayers()) {
			if (p != null && p.inPcBoat()) {
				count++;
			}
		}
		return count;
	}

	public static void process() {
		if (properTimer > 0) {
			properTimer--;
			return;
		} else {
			properTimer = 1;
		}
		setInterface();
		if (waitTimer > 0) {
			waitTimer--;
		} else if (waitTimer == 0) {
			startGame(false);
		}
		if (gameTimer > 0) {
			gameTimer--;
			int playerCount = getIngamePlayerCount();
			if (Misc.random(
					(playerCount > 20 ? 0 : (playerCount > 15 ? 1 : 2))) == 0) {
				spawnRandomNpc(Misc.random(3));
			}
			if (allPortalsDead()) {
				endGame(true);
			}
		} else if (gameTimer == 0) {
			endGame(false);
		}
	}

	public static void removePlayer(Player p) {
		for (int i = 0; i < players.length; i++) {
			if (players[i] == p) {
				players[i] = null;
				return;
			}
		}
		p.getPA().movePlayer(2657, 2639, 0);
		// System.out.println("Called"+p.teleportToX);
	}

	public static void setInterface() {
		for (Player p : Server.getRegionManager()
				.getRegionByLocation(2661, 2641, 0).getPlayers()) {
			if (p != null && p.inPcBoat()) {
				Client c = (Client) p;
				c.getPacketSender().sendFrame126("Next Departure: "
						+ (gameTimer > 0 ? (gameTimer + 25) : waitTimer) + "",
						21120);
				c.getPacketSender().sendFrame126(
						"Players Ready: " + playersInBoat() + "", 21121);
				c.getPacketSender().sendFrame126("(Need 5 to 25 players)",
						21122);
				c.getPacketSender()
						.sendFrame126("Points: " + c.getPcPoints() + "", 21123);
			}
		}

		if (gameTimer > 0) {
			int playerCount = 0;
			for (Player p : players) {
				if (p == null) {
					continue;
				}
				if (p.inPcGame()) {
					Client c = (Client) p;
					playerCount++;

					// c.getPacketSender().sendFrame126("0", 21116);
					c.getPacketSender().sendFrame126(
							"Time remaining: " + gameTimer + "", 21117);
				} else {
					if (gameTimer < GAME_TIMER - 4) {
						removePlayer(p);
					}
				}
			}
			if (playerCount < 1) {
				// System.out.println("Ended pest control.");
				endGame(false);
			}
			if (knightHP < 1) {
				endGame(false);
			}
		}
	}

	public static void spawnNpcs() {
		int portalHp = 800;
		int playerCount = getIngamePlayerCount();
		if (playerCount > 15) {
			portalHp = 1000;
		} else if (playerCount > 20) {
			portalHp = 1200;
		}
		NPC npc = NPCHandler.spawnNpc2(3782, 2656, 2592, 0, 0);
		npc = NPCHandler.spawnNpc2(6142, 2628, 2591, 0, 0);
		npc.getSkills().setStaticLevel(Skills.HITPOINTS, portalHp);
		npc = NPCHandler.spawnNpc2(6143, 2680, 2588, 0, 0);
		npc.getSkills().setStaticLevel(Skills.HITPOINTS, portalHp);
		npc = NPCHandler.spawnNpc2(6144, 2669, 2570, 0, 0);
		npc.getSkills().setStaticLevel(Skills.HITPOINTS, portalHp);
		npc = NPCHandler.spawnNpc2(6145, 2645, 2569, 0, 0);
		npc.getSkills().setStaticLevel(Skills.HITPOINTS, portalHp);
	}

	public static void spawnRandomNpc(int portalId) {
		if (portalId == 0 && portalHP1 <= 0) {
			return;
		}
		if (portalId == 1 && portalHP2 <= 0) {
			return;
		}
		if (portalId == 2 && portalHP3 <= 0) {
			return;
		}
		if (portalId == 3 && portalHP4 <= 0) {
			return;
		}
		int count = 0;
		for (NPC n : npcs) {
			if (n == null) {
				count++;
			}
		}
		if (count <= 0) {
			return;
		}
		int spawnX = 2679;
		int spawnY = 2589;
		if (portalId == 0) { // 6142 // west portal
			spawnX = 2631;
			spawnY = 2592;
		} else if (portalId == 1) { // east portal
			spawnX = 2679;
			spawnY = 2589;
		} else if (portalId == 2) { // south east portal
			spawnX = 2670;
			spawnY = 2573;
		} else if (portalId == 3) { // south west portal
			spawnX = 2646;
			spawnY = 2572;
		}
		int random = Misc.random(3);

		switch (random) {

			case 0:
				int randomShifter = 3732 + Misc.random(9);
				NPCHandler.spawnNpc2(randomShifter, spawnX, spawnY, 0, 1); // base attack/defense/armor for this type of npc
				break;

			case 1:
				int randomSpinner = 3747 + Misc.random(4);
				NPCHandler.spawnNpc2(randomSpinner, spawnX, spawnY, 0, 1); // base attack/defense/armor for this type of npc
				break;

			case 2:
				int randomDefiler = 3762 + Misc.random(9);
				NPCHandler.spawnNpc2(randomDefiler, spawnX, spawnY, 0, 1); // base attack/defense/armor for this type of npc
				break;
			case 3:
				int randomTorcher = 3752 + Misc.random(9);
				NPCHandler.spawnNpc2(randomTorcher, spawnX, spawnY, 0, 1); // base attack/defense/armor for this type of npc
				break;
		}
	}

	public static void startGame(boolean force) {
		if (playersInBoat() >= PLAYERS_REQUIRED_TO_START || force) {
			gameTimer = GAME_TIMER;
			waitTimer = -1;
			for (Player p : Server.getRegionManager()
					.getRegionByLocation(2661, 2641, 0).getPlayers()) {
				if (p != null && p.inPcBoat()) {
					movePlayerToGame(p);
					p.pcParticipation = 0;
				}
			}

			spawnNpcs();
			PestDoorHandler.createNewDoors();

		} else {
			waitTimer = WAIT_TIMER;
			for (Player p : Server.getRegionManager()
					.getRegionByLocation(2661, 2641, 0).getPlayers()) {
				if (p != null && p.inPcBoat()) {
					p.sendMessage(
							"There need to be at least 5 players to start a game of pest control.");
				}
			}
		}
	}

	public static void updateNpcHP(NPC npc) {
		if (npc == null) {
			return;
		}
		for (Player p : players) {
			if (p == null || !p.isActive) {
				continue;
			}
			if (!p.inPcGame() && gameTimer < GAME_TIMER - 4) {
				removePlayer(p);
				continue;
			}
			Client c = (Client) p;
			// if (c == null) continue;

			switch (npc.npcType) {
				case 3782:
					knightHP = npc.getSkills().getLifepoints();
					c.getPacketSender().sendFrame126("" + knightHP + "", 21115);
					break;

				case 6142:
					portalHP1 = npc.getSkills().getLifepoints();
					if (npc.getSkills().getLifepoints() <= 0 || npc.isDead()) {
						NPC n = getKnight();
						if (n != null && Portal1kill == 0) {
							n.getSkills().heal(50);
							Portal1kill++;
						}
						c.getPacketSender().sendFrame126("<col=ff0000>0", 21111);
					} else {
						c.getPacketSender().sendFrame126(
								"" + portalHP1/* npc.getSkills().getLifepoints() */ + "", 21111);
					}
					break;

				case 6143:
					portalHP2 = npc.getSkills().getLifepoints();
					if (npc.getSkills().getLifepoints() <= 0 || npc.isDead()) {
						NPC n = getKnight();
						if (n != null && Portal2kill == 0) {
							n.getSkills().heal(50);
							Portal2kill++;
						}
						c.getPacketSender().sendFrame126("<col=ff0000>0", 21112);
					} else {
						c.getPacketSender().sendFrame126(
								"" + portalHP2/* npc.getSkills().getLifepoints() */ + "", 21112);
					}
					break;

				case 6144:
					portalHP3 = npc.getSkills().getLifepoints();
					if (npc.getSkills().getLifepoints() <= 0 || npc.isDead()) {
						NPC n = getKnight();
						if (n != null && Portal3kill == 0) {
							n.getSkills().heal(50);
							Portal3kill++;
						}
						c.getPacketSender().sendFrame126("<col=ff0000>0", 21113);
					} else {
						c.getPacketSender().sendFrame126(
								"" + portalHP3/* npc.getSkills().getLifepoints() */ + "", 21113);
					}
					break;
				case 6145:
					portalHP4 = npc.getSkills().getLifepoints();
					if (npc.getSkills().getLifepoints() <= 0 || npc.isDead()) {
						NPC n = getKnight();
						if (n != null && Portal4kill == 0) {
							n.getSkills().heal(50);
							Portal4kill++;
						}
						c.getPacketSender().sendFrame126("<col=ff0000>0", 21114);
					} else {
						c.getPacketSender().sendFrame126(
								"" + portalHP4/* npc.getSkills().getLifepoints() */ + "", 21114);
					}
					break;

			}

		}
	}

	/**
	 * Slightly Revised Pest-Control Forgotten Paradise
	 */

	public PestControl() {

	}

}
