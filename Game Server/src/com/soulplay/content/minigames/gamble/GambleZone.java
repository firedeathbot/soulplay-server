package com.soulplay.content.minigames.gamble;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class GambleZone extends Zone {

	@Override
	public void onChange(Player p) {
		/* empty */
	}

	@Override
	public void onEntry(Player p) {
		p.getPacketSender().showOption(7, 0, "Gamble");
	}

	@Override
	public void onExit(Player p) {
		p.getPacketSender().showOption(7, 0, "null");
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.getX() >= 1441 && p.getX() <= 1464 && p.getY() >= 4444 && p.getY() <= 4470;
	}

}
