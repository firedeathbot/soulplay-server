package com.soulplay.content.minigames.gamble;

import com.soulplay.content.minigames.gamble.flowers.FlowerPlayer;
import com.soulplay.content.minigames.gamble.flowers.FlowerZone;
import com.soulplay.content.player.ChatMessageTypes;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Location;

public class GamblingManager {

	public static boolean enabled = true;
	public static final int INVITE_EXPIRE_TICKS = 50;
	private static final FlowerZone[] flowerZones = new FlowerZone[4];

	public static void offerGamble(Player player, Player opponent) {
		player.expireTicks = INVITE_EXPIRE_TICKS;
		player.onExpire = () -> {
			if (player.gambleOfferTo != -1) {
				Client invited = (Client) PlayerHandler.players[player.gambleOfferTo];
				if (invited != null) {
					invited.sendMessage("Gamble offer from " + player.getNameSmartUp() + " has expired.");
				}
			}

			player.sendMessage("Your gamble offer to " + opponent.getNameSmartUp() + " has expired.");
			player.gambleOfferTo = -1;
		};

		player.gambleOfferTo = opponent.getId();
		player.sendMessage("Sending gamble offer to " + opponent.getNameSmartUp() + "...");
		opponent.getPacketSender().sendMessage(ChatMessageTypes.GAMBLE_REQUEST, "has offered you to gamble.",
				player.getNameSmartUp());
	}

	public static void resetGambleOffer(Player player) {
		player.gambleOfferTo = -1;
		player.onExpire = null;
		player.expireTicks = 0;
	}

	public static void initGamble(Player player, Player opponent) {
		if (!enabled) {
			player.sendMessage("Unable to gamble at this moment.");
			return;
		}

		if (player.isIronMan() || opponent.isIronMan()) {
			player.sendMessage("You cannot gamble as an ironman.");
			return;
		}

		initGamblePlayer(player, opponent);
		initGamblePlayer(opponent, player);
	}

	public static void initGamblePlayer(Player player, Player opponent) {
		GamblingInterface.openSetup(player, opponent);
		resetGambleOffer(player);
		setStage(player, GambleStage.INIT);
		player.gambleOpponent = opponent;
	}

	public static void setStage(Player player, GambleStage stage) {
		player.gambleStage = stage;
	}

	public static void hardClose(Player player) {
		player.getPacketSender().closeInterfaces();

		Player opponent = player.gambleOpponent;
		player.gambleOpponent = null;
		if (opponent != null && opponent.isActive) {
			hardClose(opponent);
		}

		softClose(player);
	}

	public static void softClose(Player player) {
		Inventory stake = player.gambleStake;
		int stakeSize = stake.size();
		if (stakeSize > 0) {
			for (int i = 0, length = stake.capacity(); i < length; i++) {
				GameItem item = stake.get(i);
				if (item == null) {
					continue;
				}

				player.getItems().addOrBankOrDropItem(item.getId(), item.getAmount(), "<col=ff0000>Your stake items were added to the bank because your inventory is full!", "<col=ff0000>Your stake items were added to the bank because your inventory is full!");
			}

			stake.clear();
		}

		FlowerPlayer flowerPlayer = player.gambleFlowerPlayer;
		if (flowerPlayer != null) {
			flowerPlayer.getFlowerZone().end();
			player.gambleFlowerPlayer = null;
		}

		player.gambleNextFlowerRule = null;
		player.gambleBlockTimer.reset();
		player.gambleUpdateData = true;
		player.gambleAccepted = false;
		player.gambleStage = GambleStage.NONE;
		player.gambleOpponent = null;
		player.performingAction = false;
	}

	public static FlowerZone findFreeZone() {
		for (int i = 0, length = flowerZones.length; i < length; i++) {
			FlowerZone flowerZone = flowerZones[i];
			if (flowerZone != null && flowerZone.isInactive()) {
				return flowerZone;
			}
		}

		return null;
	}

	static {
		flowerZones[2] = new FlowerZone(Location.create(1458, 4467), Location.create(1458, 4466));
		flowerZones[0] = new FlowerZone(Location.create(1458, 4461), Location.create(1458, 4460));
		flowerZones[1] = new FlowerZone(Location.create(1458, 4454), Location.create(1458, 4453));
		flowerZones[3] = new FlowerZone(Location.create(1458, 4448), Location.create(1458, 4447));
	}

}
