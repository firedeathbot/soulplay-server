package com.soulplay.content.minigames.gamble;

public enum GambleStage {

	NONE,
	INIT,
	CHECK,
	DUEL;

}
