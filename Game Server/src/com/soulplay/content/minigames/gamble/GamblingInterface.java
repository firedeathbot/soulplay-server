package com.soulplay.content.minigames.gamble;

import java.util.function.Consumer;

import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.minigames.duel.TradeAndDuel;
import com.soulplay.content.minigames.gamble.flowers.FlowerZone;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

public class GamblingInterface {

	public static final int SETUP_ID = 73000;
	public static final int SETUP_INV_ID = 73031;
	public static final int SETUP_INV_OTHER_ID = 73032;
	public static final int SETUP_INFO_TEXT = 73024;
	public static final int SETUP_PRICE1_TEXT = 73013;
	public static final int SETUP_PRICE2_TEXT = 73014;
	public static final int SETUP_OPPONENT_INV_ID = 73101;
	public static final int SIDE_ID = 73033;
	public static final int SIDE_INV_ID = 73034;
	public static final int PREVIEW_ID = 73500;
	public static final int PREVIEW_INV_ID = 73520;
	public static final int PREVIEW_INV_OTHER_ID = 73521;

	public static void openSetup(Player player, Player opponent) {
		player.getPacketSender().sendFrame248(SETUP_ID, SIDE_ID);
		player.getPacketSender().resetInvenory(SETUP_INV_ID);
		player.getPacketSender().resetInvenory(SETUP_INV_OTHER_ID);
		changeSetupInfoText(player, "");
		updateSide(player);
		player.getPacketSender().sendConfig(ConfigCodes.GAMBLE_GAME_MODE, player.gambleGame.getIndex());

		if (opponent != null) {
			player.getPacketSender().itemsOnInterface(SETUP_OPPONENT_INV_ID, opponent.playerItems, opponent.playerItemsN);
			opponent.getPacketSender().itemsOnInterface(SETUP_OPPONENT_INV_ID, player.playerItems, player.playerItemsN);
			player.getPacketSender().sendString(opponent.getNameSmartUp() + " stake:", 73012);
			updatePriceTextSetup(player, opponent, 0);
		}
	}

	public static void openPreview(Player player, Player opponent, Item[] playerItems, Item[] opponentItems) {
		player.getPacketSender().sendFrame248(PREVIEW_ID, -1);
		updateStake(player, PREVIEW_INV_ID, playerItems);
		updateStake(player, PREVIEW_INV_OTHER_ID, opponentItems);
		updatePriceText(player, price -> updatePriceTextPreview(player, opponent, price));
		player.getPacketSender().sendString(opponent.getNameSmartUp() + " stake:", 73515);
		player.getPacketSender().sendString("Game Mode: <col=BFFF09>" + player.gambleGame.getName(), 73552);
		player.getPacketSender().sendString("Name: <col=BFFF09>" + opponent.getNameSmartUp(), 73554);
		player.gambleBlockTimer.startTimer(2);
		player.getPacketSender().sendWaitButton(true);
		changePreviewInfoText(player, "");
	}

	public static boolean buttonClick(Player player, int buttonId) {
		switch (buttonId) {
			case 73020: {// Accept
				if (player.gambleStage != GambleStage.INIT) {
					return true;
				}

				Player opponent = player.gambleOpponent;
				if (opponent == null || !opponent.isActive) {
					GamblingManager.hardClose(player);
					return true;
				}

				if (!player.gambleBlockTimer.complete()) {
					return true;
				}

				if (!player.getItems().playerHasItem(299, 5)) {
					String text = player.getNameSmartUp() + " doesn't have enough Mithril seeds to accept the game.";
					changeSetupInfoText(player, text);
					changeSetupInfoText(opponent, text);
					return true;
				}

				player.gambleAccepted = true;
				changeSetupInfoText(player, "Waiting for other player...");
				changeSetupInfoText(opponent, "Other player has accepted.");
				if (opponent.gambleAccepted && player.gambleAccepted) {
					player.gambleAccepted = false;
					opponent.gambleAccepted = false;
					player.gambleStage = GambleStage.CHECK;
					opponent.gambleStage = GambleStage.CHECK;
					GamblingInterface.openPreview(player, opponent, player.gambleStake.getItems(), opponent.gambleStake.getItems());
					GamblingInterface.openPreview(opponent, player, opponent.gambleStake.getItems(), player.gambleStake.getItems());
				}
				return true;
			}
			case 73021:// Decline
			case 73507: {// Decline button on preview
				GamblingManager.hardClose(player);
				return true;
			}
			case 73506: {// Accept button on preview
				if (player.gambleStage != GambleStage.CHECK) {
					return true;
				}

				Player opponent = player.gambleOpponent;
				if (opponent == null || !opponent.isActive) {
					GamblingManager.hardClose(player);
					return true;
				}

				if (!player.gambleBlockTimer.complete()) {
					return true;
				}

				player.gambleAccepted = true;
				changePreviewInfoText(player, "Waiting for other player...");
				changePreviewInfoText(opponent, "Other player has accepted.");
				if (opponent.gambleAccepted && player.gambleAccepted) {
					FlowerZone flowerZone = GamblingManager.findFreeZone();
					if (flowerZone == null) {
						GamblingManager.hardClose(player);
						player.getDialogueBuilder().sendStatement("All gamble zones are full. Wait until they are free.").execute();
						opponent.getDialogueBuilder().sendStatement("All gamble zones are full. Wait until they are free.").execute();
						return true;
					}

					flowerZone.init(player, opponent);
					player.gambleAccepted = false;
					opponent.gambleAccepted = false;
					player.gambleStage = GambleStage.DUEL;
					opponent.gambleStage = GambleStage.DUEL;
					player.performingAction = true;
					opponent.performingAction = true;

					Location location1;
					Location location2;
					if (Misc.randomBoolean()) {
						location1 = flowerZone.getPlayer1Location();
						location2 = flowerZone.getPlayer2Location();
					} else {
						location1 = flowerZone.getPlayer2Location();
						location2 = flowerZone.getPlayer1Location();
					}

					player.getPA().movePlayer(location1);
					opponent.getPA().movePlayer(location2);
					player.getPacketSender().closeInterfaces();
					opponent.getPacketSender().closeInterfaces();
					player.faceLocation(location1.transform(Direction.EAST));
					opponent.faceLocation(location2.transform(Direction.EAST));

					player.gambleLastStake.copy(player.gambleStake);
					opponent.gambleLastStake.copy(opponent.gambleStake);
				}
				return true;
			}
			case 73015: {// All in
				if (player.gambleStage != GambleStage.INIT) {
					return true;
				}

				Player opponent = player.gambleOpponent;
				if (opponent == null || !opponent.isActive) {
					GamblingManager.hardClose(player);
					return true;
				}

				player.gambleUpdateData = false;
				for (int i = 0; i < 28; i++) {
					offerItem(player, Integer.MAX_VALUE, i);
				}

				player.gambleUpdateData = true;
				onStakeItemChange(player, opponent);
				return true;
			}
			case 73016: {
				if (player.gambleStage != GambleStage.INIT) {
					return true;
				}

				Player opponent = player.gambleOpponent;
				if (opponent == null || !opponent.isActive) {
					GamblingManager.hardClose(player);
					return true;
				}

				player.gambleUpdateData = false;
				for (int i = 0; i < 28; i++) {
					Item item = player.gambleLastStake.get(i);
					if (item == null) {
						continue;
					}

					int index = player.getItems().getItemSlot(item.getId());
					if (index == -1) {
						continue;
					}

					offerItem(player, item.getAmount(), index);
				}

				player.gambleUpdateData = true;
				onStakeItemChange(player, opponent);
				return true;
			}
			default:
				return false;
		}
	}

	public static void offerItem(Player player, int offerAmount, int slot) {
		if (player.gambleStage != GambleStage.INIT) {
			return;
		}

		Player opponent = player.gambleOpponent;
		if (opponent == null || !opponent.isActive) {
			GamblingManager.hardClose(player);
			return;
		}

		if (slot < 0 || slot >= player.playerItems.length) {
			return;
		}

		int itemId = player.playerItems[slot] - 1;
		if (itemId < 0) {
			return;
		}

		if (!TradeAndDuel.allowedToTradeItem(player, itemId, true)) {
			player.sendMessage("You can't gamble this item.");
			return;
		}

		int itemAmount = player.getItems().getItemAmount(itemId);
		if (itemAmount < 1) {
			return;
		}

		if (offerAmount > itemAmount) {
			offerAmount = itemAmount;
		}

		if (!player.getItems().playerHasItem(itemId, offerAmount)) {
			return;
		}

		if (!player.gambleStake.add(new Item(itemId, offerAmount))) {
			return;
		}

		player.getItems().deleteItem2(itemId, offerAmount);
		onStakeItemChange(player, opponent);
	}

	public static void takeItem(Player player, int takeAmount, int slot) {
		if (player.gambleStage != GambleStage.INIT) {
			return;
		}

		Player opponent = player.gambleOpponent;
		if (opponent == null || !opponent.isActive) {
			GamblingManager.hardClose(player);
			return;
		}

		Inventory inventory = player.gambleStake;
		if (slot < 0 || slot >= inventory.capacity()) {
			return;
		}

		Item item = inventory.get(slot);
		if (item == null) {
			return;
		}

		int itemId = item.getId();
		if (itemId < 0) {
			return;
		}

		int itemAmount = inventory.getAmount(item);
		if (itemAmount < 1) {
			return;
		}

		if (takeAmount > itemAmount) {
			takeAmount = itemAmount;
		}

		Item finalItem = new Item(itemId, takeAmount);

		if (!player.gambleStake.containsItem(finalItem)) {
			return;
		}

		if (!player.getItems().addItem(finalItem.getId(), finalItem.getAmount())) {
			return;
		}

		player.gambleStake.remove(finalItem);
		onStakeItemChange(player, opponent);
	}

	public static void onStakeItemChange(Player player, Player opponent) {
		if (!player.gambleUpdateData) {
			return;
		}

		player.gambleAccepted = false;
		opponent.gambleAccepted = false;
		updateSide(player);
		updateStake(player, SETUP_INV_ID, player.gambleStake.getItems());
		updateStake(opponent, SETUP_INV_OTHER_ID, player.gambleStake.getItems());
		changeSetupInfoText(player, "");
		changeSetupInfoText(opponent, "Gamble has changed - check before accepting!");
		opponent.getPacketSender().itemsOnInterface(SETUP_OPPONENT_INV_ID, player.playerItems, player.playerItemsN);
		updatePriceText(player, price -> updatePriceTextSetup(player, opponent, price));
		player.getPacketSender().sendWaitButton(false);
		opponent.getPacketSender().sendWaitButton(false);
		player.gambleBlockTimer.startTimer(2);
		opponent.gambleBlockTimer.startTimer(2);
	}

	public static void updateStake(Player player, int side, Item[] items) {
		player.getPacketSender().sendInventory(side, items, 1);
	}

	public static void updateSide(Player player) {
		player.getItems().resetItems(73034);
	}

	public static void changeSetupInfoText(Player player, String text) {
		player.getPacketSender().sendString(text, SETUP_INFO_TEXT);
	}

	public static void changePreviewInfoText(Player player, String text) {
		player.getPacketSender().sendString(text, 73558);
	}

	public static void updatePriceText(Player player, Consumer<Long> textUpdate) {
		long price = 0;

		for (int i = 0, length = player.gambleStake.capacity(); i < length; i++) {
			Item item = player.gambleStake.get(i);
			if (item == null) {
				continue;
			}

			price += PriceChecker.getPriceLong(item.getId()) * item.getAmount();
		}

		textUpdate.accept(price);
	}

	public static void updatePriceTextSetup(Player player, Player opponent, long price) {
		updatePriceText(player, opponent, price, SETUP_PRICE1_TEXT, SETUP_PRICE2_TEXT);
	}

	public static void updatePriceTextPreview(Player player, Player opponent, long price) {
		updatePriceText(player, opponent, price, 73516, 73517);
	}

	public static void updatePriceText(Player player, Player opponent, long price, int textId1, int textId2) {
		String priceString = Misc.format(price) + " gp";
		player.getPacketSender().sendString(priceString, textId1);
		opponent.getPacketSender().sendString(priceString, textId2);
	}

}
