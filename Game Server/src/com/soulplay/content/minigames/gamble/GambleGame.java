package com.soulplay.content.minigames.gamble;

public enum GambleGame {

	FLOWER_POKER(0, "Flower Poker"),
	HOT_AND_COLD(1, "Hot & Cold"),
	DICE(2, "55x2"),
	BLACKJACK(3, "21 Blackjack"),
	DICE_DUEL(4, "Dice Duel");

	private final int index;
	private final String name;

	GambleGame(int index, String name) {
		this.index = index;
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

}
