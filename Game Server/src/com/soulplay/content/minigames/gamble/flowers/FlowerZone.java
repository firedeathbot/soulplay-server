package com.soulplay.content.minigames.gamble.flowers;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.minigames.gamble.GamblingManager;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class FlowerZone {

	private static final int[] WIN_ANIMATIONS = { 862, 5316, 7071, 7531, 7700, 10296, 10301, 12472, 12473, 14002, 14118, 14582, 15399, 15419, 17079, 17111, 17115, 784, 818, 866 };
	private static final int[] LOSE_ANIMATIONS = { 860, 5315, 5763, 7188, 9922, 11893, 12436, 13985, 14830, 1745, 1746 };

	public static final int TIMEOUT_TICKS = 200;
	private final Location player1Location, player2Location;
	private FlowerPlayer player1, player2;
	private List<RSObject> flowers = new ArrayList<>(10);
	private CycleEventContainer idleTimeout;
	private CycleEventContainer winEvent;
	private Object cycleIntance = new Object();

	public FlowerZone(Location player1Location, Location player2Location) {
		this.player1Location = player1Location;
		this.player2Location = player2Location;
	}

	public void init(Player player1, Player player2) {
		this.player1 = new FlowerPlayer(player1, this);
		this.player2 = new FlowerPlayer(player2, this);

		idleTimeout = CycleEventHandler.getSingleton().addEvent(cycleIntance, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				checkVictory(true);
				container.stop();
			}

		}, TIMEOUT_TICKS);
	}

	public void checkVictory(boolean timeout) {
		int player1Flowers = player1.flowersPlanted();
		int player2Flowers = player2.flowersPlanted();
		int totalPlanted = player1Flowers + player2Flowers;
		if (totalPlanted < 10 && !timeout) {
			return;
		}

		if (timeout) {
			if (player1Flowers < 5) {
				int toPlant = 5 - player1Flowers;
				player1.forcePlant(toPlant);
			}

			if (player2Flowers < 5) {
				int toPlant = 5 - player2Flowers;
				player2.forcePlant(toPlant);
			}
		}

		FlowerRule player1Score = player1.getResult();
		FlowerRule player2Score = player2.getResult();
		Player p1 = player1.getPlayer();
		Player p2 = player2.getPlayer();

		final boolean draw;
		Player winnerP;
		Player loserP;
		FlowerRule winnerScore;
		FlowerRule loserScore;
		if (player1Score.getPrecedence() > player2Score.getPrecedence()) {
			draw = false;
			winnerP = p1;
			loserP = p2;
			winnerScore = player1Score;
			loserScore = player2Score;
		} else if (player2Score.getPrecedence() > player1Score.getPrecedence()) {
			draw = false;
			winnerP = p2;
			loserP = p1;
			winnerScore = player2Score;
			loserScore = player1Score;
		} else {
			winnerP = p1;
			loserP = p2;
			winnerScore = player1Score;
			loserScore = player2Score;
			draw = true;
		}


		winEvent = CycleEventHandler.getSingleton().addEvent(cycleIntance, new CycleEvent() {

			int ticks = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (ticks == 1) {
					if (draw) {
						winnerP.startAnimation(857);
						loserP.startAnimation(857);
						winnerP.sendMessage("It's a tie. You and <col=E5E5E5>" + loserP.getNameSmartUp() + "</col> both rolled a <col=F0F060>" + winnerScore.getName() + "</col>.");
						loserP.sendMessage("It's a tie. You and <col=E5E5E5>" + winnerP.getNameSmartUp() + "</col> both rolled a <col=F0F060>" + loserScore.getName() + "</col>.");
					} else {
						winnerP.startAnimation(Misc.random(WIN_ANIMATIONS));
						loserP.startAnimation(Misc.random(LOSE_ANIMATIONS));
						winnerP.sendMessage("You've won against <col=E5E5E5>" + loserP.getNameSmartUp() + "</col>! Score: <col=22B322>" + winnerScore.getName() + "</col> vs <col=ff0000>" + loserScore.getName() + "</col>.");
						loserP.sendMessage("You've lost against <col=E5E5E5>" + winnerP.getNameSmartUp() + "</col>. Score: <col=ff0000>" + loserScore.getName() + "</col> vs <col=22B322>" + winnerScore.getName() + "</col>.");
					}
				} else if (ticks == 3) {
					if (!draw) {
						Inventory pot = new Inventory(28);
						pot.copy(loserP.gambleStake);
						loserP.gambleStake.clear();

						winnerP.getPacketSender().showInterface(6733);
						GamblingInterface.updateStake(winnerP, 6822, pot.getItems());
						for (int i = 0, length = pot.capacity(); i < length; i++) {
							Item item = pot.get(i);
							if (item == null) {
								continue;
							}

							winnerP.getItems().addOrDrop(item, "Your winnings were dropped on the ground because you don't have any inventory space.");
						}
					}

					GamblingManager.softClose(winnerP);
					GamblingManager.softClose(loserP);
					container.stop();
				}

				ticks++;
			}

		}, 1);
	}

	public void end() {
		if (idleTimeout != null) {
			if (idleTimeout.isRunning()) {
				idleTimeout.stop();
			}

			idleTimeout = null;
		}

		if (winEvent != null) {
			if (winEvent.isRunning()) {
				winEvent.stop();
			}

			winEvent = null;
		}

		if (player1 != null) {
			player1.cleanup();
			player1 = null;
		}

		if (player2 != null) {
			player2.cleanup();
			player2 = null;
		}

		for (int i = 0, length = flowers.size(); i < length; i++) {
			RSObject flowerObject = flowers.get(i);
			if (flowerObject == null) {
				continue;
			}

			flowerObject.setTick(0);
			flowerObject.changeObject(-1);
		}
	}

	public boolean isInactive() {
		return player1 == null || player2 == null;
	}

	public Location getPlayer1Location() {
		return player1Location;
	}

	public Location getPlayer2Location() {
		return player2Location;
	}

	public List<RSObject> getFlowers() {
		return flowers;
	}

}
