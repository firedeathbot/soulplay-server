package com.soulplay.content.minigames.gamble.flowers;

public enum FlowerRule {

	FIVE_OF_A_KIND(6, "Five of a kind"),
	FOUR_OF_A_KIND(5, "Four of a kind"),
	FULL_HOUSE(4, "Full house"),
	THREE_OF_A_KIND(3, "Three of a kind"),
	TWO_PAIR(2, "Two pair"),
	ONE_PAIR(1, "One pair"),
	NOTHING(0, "Nothing");

	public static final FlowerRule[] values = values();
	private final int precedence;
	private final String name;

	FlowerRule(int precedence, String name) {
		this.precedence = precedence;
		this.name = name;
	}

	public int getPrecedence() {
		return precedence;
	}

	public String getName() {
		return name;
	}

}
