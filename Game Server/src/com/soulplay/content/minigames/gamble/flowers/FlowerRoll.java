package com.soulplay.content.minigames.gamble.flowers;

import java.util.Objects;

public class FlowerRoll {

	private final FlowerType type;
	private int count = 1;

	public FlowerRoll(FlowerType type) {
		this.type = type;
	}

	public FlowerType getType() {
		return type;
	}

	public int getCount() {
		return count;
	}

	public void increaseCount() {
		count++;
	}

	@Override
	public int hashCode() {
		return Objects.hash(type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlowerRoll other = (FlowerRoll) obj;
		return type == other.type;
	}

	@Override
	public String toString() {
		return "FlowerRoll [type=" + type + ", count=" + count + "]";
	}

}
