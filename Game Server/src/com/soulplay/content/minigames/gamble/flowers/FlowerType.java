package com.soulplay.content.minigames.gamble.flowers;

import java.util.Arrays;
import java.util.List;

public enum FlowerType {

	BLUE_PURPLE(0, 2980),
	RED(1, 2981),
	BLUE(2, 2982),
	YELLOW(3, 2983),
	PURPLE(4, 2984),
	ORANGE(5, 2985),
	RED_YELLOW_BLUE(6, 2986);

	public static final FlowerType[] values = values();
	public static final List<FlowerType> list = Arrays.asList(FlowerType.values);
	private final int index, objectId;

	private FlowerType(int index, int objectId) {
		this.index = index;
		this.objectId = objectId;
	}

	public int getIndex() {
		return index;
	}

	public int getObjectId() {
		return objectId;
	}

}
