package com.soulplay.content.minigames.gamble.flowers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

public class FlowerPlayer {

	private final Player player;
	private final List<FlowerRoll> flowers;
	private final FlowerZone flowerZone;
	private final List<FlowerType> premadeFlowers;
	private int premadeFlowersIndex;

	public FlowerPlayer(Player player, FlowerZone flowerZone) {
		this.player = player;
		this.player.gambleFlowerPlayer = this;
		this.flowers = new ArrayList<>();
		this.flowerZone = flowerZone;
		FlowerRule thisRule = player.gambleNextFlowerRule;
		if (thisRule != null) {
			this.premadeFlowers = new ArrayList<>();
			addPremadeFlowers(thisRule);
		} else {
			this.premadeFlowers = null;
		}
	}

	private void addPremadeFlowers(FlowerRule thisRule) {
		List<FlowerType> flowers = new ArrayList<>(FlowerType.list);
		Collections.shuffle(flowers);

		switch (thisRule) {
			case FIVE_OF_A_KIND:
				for (int i = 0; i < 5; i++) {
					premadeFlowers.add(flowers.get(0));
				}
				break;
			case FOUR_OF_A_KIND:
				for (int i = 0; i < 4; i++) {
					premadeFlowers.add(flowers.get(0));
				}
				premadeFlowers.add(flowers.get(1));
				break;
			case FULL_HOUSE:
				for (int i = 0; i < 3; i++) {
					premadeFlowers.add(flowers.get(0));
				}
				for (int i = 0; i < 2; i++) {
					premadeFlowers.add(flowers.get(1));
				}
				break;
			case THREE_OF_A_KIND:
				for (int i = 0; i < 3; i++) {
					premadeFlowers.add(flowers.get(0));
				}
				for (int i = 0; i < 2; i++) {
					premadeFlowers.add(flowers.get(1 + i));
				}
				break;
			case TWO_PAIR:
				for (int i = 0; i < 2; i++) {
					premadeFlowers.add(flowers.get(0));
				}
				for (int i = 0; i < 2; i++) {
					premadeFlowers.add(flowers.get(1));
				}
				premadeFlowers.add(flowers.get(2));
				break;
			case ONE_PAIR:
				for (int i = 0; i < 2; i++) {
					premadeFlowers.add(flowers.get(0));
				}
				for (int i = 0; i < 3; i++) {
					premadeFlowers.add(flowers.get(1 + i));
				}
				break;
			case NOTHING:
				for (int i = 0; i < 5; i++) {
					premadeFlowers.add(flowers.get(i));
				}
				break;
		}

		Collections.shuffle(premadeFlowers);
	}

	private FlowerType getFlower() {
		if (premadeFlowers != null) {
			return premadeFlowers.get(premadeFlowersIndex++);
		}

		return Misc.random(FlowerType.values);
	}

	public boolean plantFlower() {
		if (flowersPlanted() >= 5) {
			return false;
		}

		int playerX = player.getX();
		int playerY = player.getY();
		FlowerType flowerType = getFlower();
		player.getItems().deleteItemInOneSlot(299, 1);
		player.faceLocation(playerX, playerY);

		player.getMovement().resetWalkingQueue();

		int x = 1, y = 0;
		Direction dir = Direction.getDirection(x, y);
		ForceMovementMask mask = ForceMovementMask.createMask(x, y, 1, 0, dir);
		player.setForceMovement(mask);

		RSObject flowerObject = new RSObject(flowerType.getObjectId(), playerX, playerY, player.getZ(), 0, 10, -1, Integer.MAX_VALUE);
		ObjectManager.addObject(flowerObject);
		flowerZone.getFlowers().add(flowerObject);

		addFlower(flowerType);

		flowerZone.checkVictory(false);
		return true;
	}

	public void addFlower(FlowerType flowerType) {
		FlowerRoll flowerRoll = new FlowerRoll(flowerType);
		int index = flowers.indexOf(flowerRoll);
		if (index == -1) {
			flowers.add(new FlowerRoll(flowerType));
		} else {
			flowers.get(index).increaseCount();
		}
	}

	public void forcePlant(int amount) {
		player.getItems().deleteItemInOneSlot(299, amount);

		for (int i = 0; i < amount; i++) {
			FlowerType flowerType = getFlower();
			addFlower(flowerType);
		}
	}

	public int flowersPlanted() {
		int count = 0;

		for (int i = 0, length = flowers.size(); i < length; i++) {
			FlowerRoll flowerRoll = flowers.get(i);
			count += flowerRoll.getCount();
		}

		return count;
	}

	public void cleanup() {
		player.gambleFlowerPlayer = null;
		flowers.clear();
	}

	public FlowerRule getResult() {
		int size = flowers.size();
		if (size == 1 && flowers.get(0).getCount() == 5) {
			return FlowerRule.FIVE_OF_A_KIND;
		} else if (size == 2) {
			int count1 = flowers.get(0).getCount();
			int count2 = flowers.get(1).getCount();
			if (count1 == 4 && count2 == 1 || count1 == 1 && count2 == 4) {
				return FlowerRule.FOUR_OF_A_KIND;
			} else if (count1 == 3 && count2 == 2 || count1 == 2 && count2 == 3) {
				return FlowerRule.FULL_HOUSE;
			}
		} else if (size == 3) {
			int count1 = flowers.get(0).getCount();
			int count2 = flowers.get(1).getCount();
			int count3 = flowers.get(2).getCount();
			if (count1 == 3 || count2 == 3 || count3 == 3) {
				return FlowerRule.THREE_OF_A_KIND;
			}
			if (count1 == 2 && count2 == 2 || count1 == 2 && count3 == 2 || count2 == 2 && count3 == 2) {
				return FlowerRule.TWO_PAIR;
			}
		} else if (size == 4) {
			int max = 0;
			for (int i = 0; i < size; i++) {
				int count = flowers.get(i).getCount();
				if (count > max) {
					max = count;
				}
			}

			if (max == 2) {
				return FlowerRule.ONE_PAIR;
			}
		}

		return FlowerRule.NOTHING;
	}

	public List<FlowerRoll> getFlowers() {
		return flowers;
	}

	public FlowerZone getFlowerZone() {
		return flowerZone;
	}

	public Player getPlayer() {
		return player;
	}

}
