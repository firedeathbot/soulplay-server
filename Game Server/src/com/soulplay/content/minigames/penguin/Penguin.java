package com.soulplay.content.minigames.penguin;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;

public class Penguin {

	private final NPC npc;
	private final PenguinData data;
	private final Set<Integer> spottedPlayers = new HashSet<>();
	private final int index;

	public Penguin(NPC npc, PenguinData data, int index) {
		this.npc = npc;
		this.data = data;
		this.index = index;
	}

	public NPC getNpc() {
		return npc;
	}

	public PenguinData getData() {
		return data;
	}

	public int getIndex() {
		return index;
	}

	public Set<Integer> getSpottedPlayers() {
		return spottedPlayers;
	}

	public void addSpottedPlayer(int id) {
		spottedPlayers.add(id);
	}

	public boolean containsPlayer(Player player) {
		return spottedPlayers.contains(player.mySQLIndex);
	}

}