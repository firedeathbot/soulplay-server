package com.soulplay.content.minigames.penguin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.game.event.Task;

public class PenguinHandler {

	public static final List<Penguin> PENGUINS = Collections.synchronizedList(new LinkedList<>());
	private static final String PENGUINS_DIRECTORY = "Data/penguins/";

	public static void initialize() {
		Server.getTaskScheduler().schedule(new Task(2) {

			@Override
			protected void execute() {
				PenguinUtil.spawnPenguins();
				loadPenguinNames();
				this.stop();
			}
		});
	}

	public static void loadPenguinNames() {
		for (Penguin penguin : PENGUINS) {
			try {
				penguin.getSpottedPlayers().clear();

				File directory = new File(PENGUINS_DIRECTORY);
				if (!directory.exists()) {
					directory.mkdirs();
				}

				File file = penguinFile(penguin.getData());
				if (!file.exists()) {
					file.createNewFile();
				}

				for (String id : Files.readAllLines(file.toPath())) {
					if (id == null || id.isEmpty()) {
						continue;
					}

					penguin.addSpottedPlayer(Integer.parseInt(id));
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	private static File penguinFile(PenguinData penguin) {
		return new File(PENGUINS_DIRECTORY + penguin.name().toLowerCase() + ".txt");
	}

	public static void resetPenguins() {
		PenguinUtil.despawnPenguins();
		Server.getTaskScheduler().schedule(new Task(2) {

			@Override
			protected void execute() {
				PenguinUtil.spawnPenguins();
				savePenguinNames();
				this.stop();
			}
		});
	}

	public static void savePenguinNames() {
		for (Penguin penguin : PENGUINS) {
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(penguinFile(penguin.getData())))) {
				for (int playerName : penguin.getSpottedPlayers()) {
					writer.append(Integer.toString(playerName));
					writer.newLine();
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

}