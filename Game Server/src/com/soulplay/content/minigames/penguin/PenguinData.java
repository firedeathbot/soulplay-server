package com.soulplay.content.minigames.penguin;

public enum PenguinData {
	// Stone Training
	PENGUIN1(8109, new PenguinLocation(2324, 3785, 0, "Yaks"),
			new PenguinLocation(2669, 3726, 0, "Rock Crabs"),
			new PenguinLocation(3550, 9933, 0, "Experiments"),
			new PenguinLocation(3449, 3577, 0, "Crawling Hands"),
			new PenguinLocation(2728, 10010, 0, "Turoths"),
			new PenguinLocation(3245, 9567, 0, "Cave Bug"),
			new PenguinLocation(3131, 9917, 0, "Thugs"),
			new PenguinLocation(3173, 9892, 0, "Red Spiders"),
			new PenguinLocation(2674, 3709, 0, "Rock Crabs"),
			new PenguinLocation(2692, 3723, 0, "Rock Crabs")),
	// Barrel Minigames
	PENGUIN2(8104, new PenguinLocation(2670, 2648, 0, "Pest Control"),
			new PenguinLocation(3382, 3273, 0, "Duel Arena"),
			new PenguinLocation(3561, 3309, 0, "Barrows"),
			new PenguinLocation(2451, 5178, 0, "TzHaar"),
			new PenguinLocation(2847, 3554, 0, "Warriors Guild"),
			new PenguinLocation(2441, 3099, 0, "Castle Wars"),
			new PenguinLocation(3572, 3314, 0, "Barrows"),
			new PenguinLocation(3354, 3268, 0, "Duel Arena"),
			new PenguinLocation(2439, 5178, 0, "TzHaar"),
			new PenguinLocation(2593, 3276, 0, "Penguin Hunt")),
	// Bush Dungeon
	PENGUIN3(8105, new PenguinLocation(2906, 9811, 0, "Blue Dragons"),
			new PenguinLocation(2699, 9519, 0, "Red Dragons"),
			new PenguinLocation(3468, 9476, 2, "Desert Strykewyrms"),
			new PenguinLocation(3020, 9575, 0, "Ice Warriors"),
			new PenguinLocation(3036, 9778, 0, "Chaos Dwarf Hand Cannoneer"),
			new PenguinLocation(2642, 4002, 1, "Glacors"),
			new PenguinLocation(2839, 9511, 0, "Mutaded Jadinkos"),
			new PenguinLocation(3302, 3083, 0, "Jungle Horrors"),
			new PenguinLocation(3181, 5466, 0, "Gargoyles"),
			new PenguinLocation(3278, 5559, 0, "Cave Bugs")),
	// Bush Dungeon
	PENGUIN4(8106, new PenguinLocation(2460, 4364, 0, "Black Dragons"),
			new PenguinLocation(2720, 9455, 0, "Steel and Iron Dragons"),
			new PenguinLocation(3499, 9490, 0, "Kalphite Guardians"),
			new PenguinLocation(3044, 9555, 0, "Frost Dragons"),
			new PenguinLocation(3023, 9844, 0, "Dwarf Boot"),
			new PenguinLocation(2658, 3988, 1, "Glacors"),
			new PenguinLocation(2874, 9509, 0, "Jungle Strykewyrms"),
			new PenguinLocation(3297, 5473, 0, "Crypt Spiders"),
			new PenguinLocation(3256, 5496, 0, "Giant Rock Crabs")),
	// Cactus
	PENGUIN5(8107, new PenguinLocation(3300, 3128, 0, "FunPK"),
			new PenguinLocation(3020, 3367, 0, "Falador"),
			new PenguinLocation(3110, 5530, 0, "Bork"),
			new PenguinLocation(3030, 3380, 0, "Falador"),
			new PenguinLocation(2801, 3464, 0, "Farming Camelot"),
			new PenguinLocation(2466, 3429, 0, "Gnome Agility"),
			new PenguinLocation(2555, 3556, 0, "Barbarian Agility"),
			new PenguinLocation(2524, 3865, 0, "Donor Home"),
			new PenguinLocation(3074, 3485, 0, "Edgeville Home"),
			new PenguinLocation(3105, 3488, 0, "Edgeville Home")),
	// Crate
	PENGUIN6(8108, new PenguinLocation(2832, 5293, 2, "Kree'arra"),
			new PenguinLocation(2860, 5360, 2, "General Graardor"),
			new PenguinLocation(2922, 5337, 2, "K'ril Tsutsaroth"),
			new PenguinLocation(2910, 5271, 0, "Commander Zilyana"),
			new PenguinLocation(2888, 5275, 2, "Nex"),
			new PenguinLocation(1779, 5359, 1, "Mithril Dragons"),
			new PenguinLocation(3212, 9615, 0, "Lumbridge basement")),
	// Toadstool
	PENGUIN7(8110, new PenguinLocation(2287, 4687, 0, "King Black Dragon"),
			new PenguinLocation(2510, 3035, 0, "Barrelchest"),
			new PenguinLocation(2901, 4456, 0, "Daggannoth Kings"),
			new PenguinLocation(1889, 4385, 0, "Aquanites"),
			new PenguinLocation(2729, 9819, 0, "Penance Queen"),
			new PenguinLocation(2943, 4361, 0, "Corporeal Beast"),
			new PenguinLocation(2933, 4395, 0, "Corporeal Beast"));

	public static final PenguinData[] values = values();
	private final int npcId;
	private final PenguinLocation[] location;

	private PenguinData(int npcId, PenguinLocation... location) {
		this.npcId = npcId;
		this.location = location;
	}

	public PenguinLocation[] getLocation() {
		return location;
	}

	public int getNpcId() {
		return npcId;
	}

}