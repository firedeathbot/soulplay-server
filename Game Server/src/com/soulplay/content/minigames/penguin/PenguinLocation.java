package com.soulplay.content.minigames.penguin;

import com.soulplay.game.world.entity.Location;

public class PenguinLocation {

	private final Location location;
	private final String hint;

	public PenguinLocation(int x, int y, int z, String hint) {
		this.location = Location.create(x, y, z);
		this.hint = hint;
	}

	public String getHint() {
		return hint;
	}

	public int getX() {
		return location.getX();
	}

	public int getY() {
		return location.getY();
	}

	public int getZ() {
		return location.getZ();
	}
}
