package com.soulplay.content.minigames.penguin;

import com.soulplay.DBConfig;
import com.soulplay.content.player.DonatorRank;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class PenguinUtil {

	public static void despawnPenguins() {
		System.out.println("Despawned");
		for (Penguin penguin : PenguinHandler.PENGUINS) {
			if (penguin.getNpc() == null) {
				System.out.println("NPC IS NULL, PROBLEM. - PenguinUtil.");
				continue;
			}

			penguin.getNpc().nullNPC();
		}

		PenguinHandler.PENGUINS.clear();
	}

	public static void generateHint(Player player) {
		for (Penguin penguin : PenguinHandler.PENGUINS) {
			if (!penguin.containsPlayer(player)) {
				player.sendMessage(penguin.getData().getLocation()[penguin.getIndex()].getHint());
			}
		}
	}

	public static void spawnPenguins() {
		for (PenguinData data : PenguinData.values) {
			int index = Misc.randomNoPlus(data.getLocation().length);
			PenguinLocation location = data.getLocation()[index];
			NPC npc = NPCHandler.spawnPenguin(data.getNpcId(), location.getX(), location.getY(), location.getZ(), 1);
			//System.out.println("Spawned in: " + data + " at: " + location.getX() + ", " + location.getY() + ", " + location.getZ());
			PenguinHandler.PENGUINS.add(new Penguin(npc, data, index));
		}
	}

	public static boolean spyOn(Penguin penguin, Player player) {
		if (!DBConfig.PENGUINS) {
			player.sendMessage("Penguin hunt is disabled for now.");
			return false;
		}

		if (DonatorRank.check(player) == DonatorRank.NONE) {
			player.sendMessage("You can't spy on a penguin with a non donator account.");
			return false;
		}

		if (penguin.containsPlayer(player)) {
			player.sendMessage("You've already spied on this penguin.");
			return false;
		}

		penguin.addSpottedPlayer(player.mySQLIndex);
		player.penguinPoints++;
		player.sendMessage("You've found a penguin, report to Chuck for points.");
		return true;
	}

}