package com.soulplay.content.minigames;

import com.soulplay.game.model.player.Player;

public class MinigameRules {

	public static boolean canJoinMinigame(Player c) {
		if (c.getMinigameBanTimer() > 0) {
			c.sendMessage("You are currently banned from PvP Minigames for today.");
			return false;
		}
		if (c.getMinigamePunishmentPoints() > 2) {
			c.sendMessage("You have been permanently banned from PvP Minigames.");
			return false;
		}
		return true;
	}
	
}
