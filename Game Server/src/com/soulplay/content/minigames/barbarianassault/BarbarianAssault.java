package com.soulplay.content.minigames.barbarianassault;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import com.soulplay.Server;
import com.soulplay.content.items.Food;
import com.soulplay.content.items.Potions;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.herblore.Herblore;
import com.soulplay.content.player.skills.summoning.Summoning;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.config.CycleEventIds;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.net.packet.in.ItemOnPlayer;
import com.soulplay.util.Misc;

public class BarbarianAssault {

	public static enum Role {
		ATTACKER("Attacker"), DEFENDER("Defender"), HEALER("Healer"), COLLECTOR("Collector"), NONE("None set");
		
		public static final Role[] values = values();

		private String name;

		private Role(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		@Override
		public String toString() {
			return getName();
		}

	}

	// after each win, how much barbarian points a player gets
	private static final int HONOR_POINT_RATE = 150;

	private static final int ROLE_POINT_RATE = 30;

	private static final int ROLE_OTHER_POINT_RATE = 3;

	private static final Location MAP = Location.create(1877, 5457);

	public static final Location LOBBY = Location.create(2594, 5263);

	public static final Location WAITING_ROOM = Location.create(2588, 5275);

	private static final int SCROLL_INTERFACE = 6965;

	private static final int SCROLL = 10512;

	private static final int MAX_GROUP_SIZE = 4;

	private static final int PENANCE_QUEEN = 5247;

	public static final int PENANCE_HEALER = 5245;

	private static final int PENANCE_RUNNER = 5227;

	private static final int PENANCE_FIGHTER_61 = 5217;

	private static final int PENANCE_FIGHTER_68 = 5218;

	private static final int PENANCE_FIGHTER_77 = 5219;

	private static final int PENANCE_RANGER_57 = 5235;

	private static final int PENANCE_RANGER_64 = 5236;

	private static final int PENANCE_RANGER_72 = 5237;

	private static final int GREEN_EGG = 10531;

	private static final int RED_EGG = 10532;

	private static final int BLUE_EGG = 10533;

	private static final int YELLOW_EGG = 10534;

	private static final int POISONED_EGG = 10535;

	private static final int SPIKES = 10561;

	private static final int SPIKED_EGG = 10536;

	private static final int OMEGA_EGG = 10537;

	private static final int POISON_CRATER_BIG = 20235;

	private static final int POISON_CRATER_SMALL = 20233;

	private static final int LAVA_CRATER_BIG = 20234;

	private static final int LAVA_CRATER_SMALL = 20232;

	private static final int SPIKED_MUSHROOMS = 20250;

	private static final int POISONED_MEAT = 10541;

	private static final int POISONED_WORMS = 10540;

	private static final int ATTACKER_CAPE_LVL_1 = 15445;

	private static final int DEFENDER_CAPE_LVL_1 = 15455;

	private static final int HEALER_CAPE_LVL_1 = 15460;

	private static final int COLLECTOR_CAPE_LVL_1 = 15450;

	private static final Location RUNNER_EXIT1 = Location.create(1886, 5454);

	private static final Location RUNNER_EXIT2 = Location.create(1887, 5454);

	private static final Location[] spawns = new Location[] { Location.create(1892, 5487), Location.create(1898, 5486),
			Location.create(1880, 5487), Location.create(1874, 5486) };

	private static final int[] eggDropArray = new int[] { GREEN_EGG, RED_EGG, BLUE_EGG };

	private static final int[] baItems = new int[] { 10542, 10543, 10544, 10545, 10546, 10541, 10540, 15439, GREEN_EGG,
			RED_EGG, BLUE_EGG, YELLOW_EGG, POISONED_EGG, SPIKED_EGG, OMEGA_EGG, SPIKES };

	private boolean queenSpawned;

	public static boolean canPickupItem(Player c) {
		return c.getBarbarianAssault().getRole() == Role.COLLECTOR && c.getBarbarianAssault().inArena();
	}

	public static boolean canTeleport(Player c) {
		return !c.getBarbarianAssault().inArena() && !c.getBarbarianAssault().inLobby();
	}

	//TODO: convert this to traditional loop
	private static Optional<NPC> getRandomDamagedNpc(Collection<NPC> npcs) {
		return Arrays.asList(npcs.stream().toArray(NPC[]::new)).stream().filter(Objects::nonNull)
				.filter(it -> it.getSkills().getLifepoints() < it.getSkills().getMaximumLifepoints()).findAny();
	}

	public boolean canUseCommands() {
		return (!inArena() && !inLobby()) || c.playerRights == 3;
	}

	public static boolean handleAttackNpc(Player c, int npcIndex) {

		NPC npc = NPCHandler.npcs[npcIndex];

		if (npc == null) {
			return false;
		}

		switch (npc.npcType) {

		case PENANCE_QUEEN:
			c.sendMessage("You need Omega eggs to hurt the queen.");
			c.getCombat().resetPlayerAttack();
			return true;

		case PENANCE_RANGER_57:
		case PENANCE_RANGER_64:
		case PENANCE_RANGER_72:
			if ((c.getBarbarianAssault().getRole() != Role.ATTACKER)
					&& (c.getBarbarianAssault().getRole() != Role.DEFENDER)) {
				c.sendMessage("You need to be an attacker or defender to attack this creature.");
				c.getCombat().resetPlayerAttack();
				return true;
			}
			return false;

		case PENANCE_FIGHTER_61:
		case PENANCE_FIGHTER_68:
		case PENANCE_FIGHTER_77:
			if (c.getBarbarianAssault().getRole() != Role.ATTACKER) {
				c.sendMessage("You need to be an attacker to attack this creature.");
				c.getCombat().resetPlayerAttack();
				return true;
			}
			return false;

		}
		return false;
	}

	public static boolean handleFirstClickObject(Player c, int objectId) {

		switch (objectId) {

		case POISON_CRATER_BIG:
		case POISON_CRATER_SMALL:
			if (c.getItems().playerHasItem(YELLOW_EGG, 1)) {
				c.getItems().deleteItem2(YELLOW_EGG, 1);
				c.startAnimation(827);
				Server.schedule(2, () -> {
					c.getItems().addItem(POISONED_EGG, 1);
				});
			}
			return true;

		case LAVA_CRATER_BIG:
		case LAVA_CRATER_SMALL:
			if (c.getItems().playerHasItem(SPIKED_EGG, 1)) {
				c.getItems().deleteItem2(SPIKED_EGG, 1);
				c.startAnimation(827);
				Server.schedule(2, () -> {
					c.getItems().addItem(OMEGA_EGG, 1);
				});
			}
			return true;

		case SPIKED_MUSHROOMS:

			if (c.getBarbarianAssault().role != Role.ATTACKER) {
				c.sendMessage("You need to be an attacker to pickup these spikes.");
				return true;
			}

			c.startAnimation(827);
			Server.schedule(2, () -> {
				c.getItems().addItem(SPIKES, 1);
			});
			return true;

		// exit barbarian assault
		case 20194:
			c.startAnimation(Animation.CLIMB_UP, 2, new Callable<Boolean>() {

				@Override
				public Boolean call() throws Exception {
					c.getBarbarianAssault().endGame(false);
					return true;
				}

			});
			return true;

		// runner exit cave
		case 20151:
			return true;

		case 20227:

			c.getBarbarianAssault().leaveGroup(); // to avoid exploits
			c.startAnimation(Animation.CLIMB_UP, 1, new Callable<Boolean>() {

				@Override
				public Boolean call() throws Exception {
					c.getPA().movePlayer(LocationConstants.EDGE);
					return true;
				}

			});
			return true;

		case 20193:
			if (c.getBarbarianAssault().group.size() < MAX_GROUP_SIZE) {
				c.sendMessage("You need " + (MAX_GROUP_SIZE - c.getBarbarianAssault().group.size()) + " more players.");
				return true;
			}

			if (!c.getItems().playerHasItem(SCROLL)) {
				c.getDialogueBuilder().sendStatement("You need to grab a scroll before entering..").execute();
				return true;
			}
			
			c.getDialogueBuilder().sendOption("Start Game", "Enter", ()-> {
				c.getBarbarianAssault().startGame();
			}, "Nevermind", ()->{ }).execute();

			return true;

		// lobby
		case 20208:

			for (int itemId : c.playerItems) {
				int realId = itemId - 1;
				if (Food.isFood(realId)) {
					c.sendMessage("You can't bring food in here.");
					return true;
				}
				if (Summoning.Familiar.isPouchItem(realId)) {
					c.sendMessage("You can't bring pouches in here.");
					return true;
				}
				if (Herblore.isHerbloreIngredient(realId)) {
					c.sendMessage("You can't bring herblore ingredients in here.");
					return true;
				}
				if (Potions.isPotion(realId)) {
					c.sendMessage("You can't bring potions in here.");
					return true;
				}
			}
			
			
		/*	for (Herblore.Herb herb : Herblore.Herb.values) {
				if (herb.getName().equals("null")) continue;
				if (c.getItems().playerHasItemSafe(herb.getUncleanedID())
						|| c.getItems().playerHasItemSafe(herb.getCleanedID())) {
					c.sendMessage("You can't bring herbs in here.");
					return true;
				}
			}*/

			if (c.absX == 2587 && c.absY == 5279) {
				c.getPA().walkTo(0, -1, true);

				if (c.getItems().playerHasItem(SCROLL)) {
					c.getItems().deleteItemInOneSlot(SCROLL, 1);
				}
			} else if (c.absX == 2587 && c.absY == 5278) {
				c.getPA().walkTo(0, 1, true);

				if (c.getItems().playerHasItem(SCROLL)) {
					c.getItems().deleteItemInOneSlot(SCROLL, 1);
				}
			} else if (c.absX == 2588 && c.absY == 5278) {
				if (c.getItems().playerHasItem(SCROLL)) {
					c.getItems().deleteItemInOneSlot(SCROLL, 1);
				}
				c.getPA().walkTo(-1, 0, true);
				Server.schedule(1, () -> {
					c.getPA().walkTo(0, 1, true);
				});
			} else if (c.absX == 2586 && c.absY == 5278) {
				if (c.getItems().playerHasItem(SCROLL)) {
					c.getItems().deleteItemInOneSlot(SCROLL, 1);
				}
				c.getPA().walkTo(1, 0, true);
				Server.schedule(1, () -> {
					c.getPA().walkTo(0, 1, true);
				});
			}

			c.getCombat().resetPrayers();
			((Client)c).getSummoning().dismissFamiliar(false);

			c.getBarbarianAssault().leaveGroup();
			return true;

		// table
		case 20149:

			if (c.getItems().freeSlots() <= 0) {
				c.sendMessage("You do not have enough inventory space.");
				return true;
			}

			if (c.getItems().playerHasItem(SCROLL)) {
				return true;
			}

			c.getDialogueBuilder().sendStatement("You received a scroll!").executeIfNotActive();
			c.getItems().addItem(SCROLL, 1);
			return true;

		// collector item machine
		case 21250:
			if (c.getBarbarianAssault().getRole() != Role.COLLECTOR) {
				c.sendMessage("You need to be a collector to use this.");
				return true;
			}
			return true;

		// defender item machine
		case 20242:

			if (c.getBarbarianAssault().getRole() != Role.DEFENDER) {
				c.sendMessage("You need to have the role of defender to use this.");
				return true;
			}

			if (c.getItems().freeSlots() < 20) {
				c.sendMessage("You need more inventory space.");
				return true;
			}

			c.getItems().addItem(10540, 20);
			c.sendMessage("Use the poisoned worms to kill the runners.");
			return true;

		// healer item machine
		case 20243:

			if (c.getBarbarianAssault().getRole() != Role.HEALER) {
				c.sendMessage("You need to have the role of healer to use this.");
				return true;
			}

			if (c.getItems().freeSlots() == 0) {
				c.sendMessage("You need more inventory space to use this.");
				return true;
			}

			c.getItems().addItem(10541, c.getItems().freeSlots());
			c.sendMessage("Use the poisoned meats to kill the healers.");
			return true;

		// healer spring
		case 20150:
			c.getPA().restorePlayer(false);
			c.startAnimation(827);
			c.sendMessage("You take a drink from the spring...");
			return true;

		}
		return false;
	}

	public static boolean handleItemOnNpc(Player c, NPC npc, int itemId) {

		switch (itemId) {

		case POISONED_MEAT:
			if (npc.npcType == PENANCE_HEALER) {

				final int currentLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.HEALER));
				npc.dealDamage(new Hit((currentLevel * 5) + 20, 2, -1));
				c.getItems().deleteItemInOneSlot(POISONED_MEAT, 1);
				c.sendMessage("You poisoned the healer.");
				c.getBarbarianAssault().giveParticipationPoints(5);
			}
			return true;

		case POISONED_WORMS:
			if (npc.npcType == PENANCE_RUNNER) {

				final int currentLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.DEFENDER));

				npc.dealDamage(new Hit((currentLevel * 5) + 20, 2, -1));
				c.getItems().deleteItemInOneSlot(POISONED_WORMS, 1);
				c.sendMessage("You poisoned the runner.");
				c.getBarbarianAssault().giveParticipationPoints(5);
			}
			return true;

		case GREEN_EGG:
			if (!npc.isBarbAssaultNpc()) {
				return true;
			}

			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				private int tickCount = 0;

				@Override
				public void execute(CycleEventContainer container) {

					final int collectorLevel = c.getBarbarianAssault()
							.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.COLLECTOR));

					if (tickCount >= 4 + (collectorLevel + 1)) {
						container.stop();
						return;
					}

					npc.dealDamage(new Hit(3, 2, -1));

					tickCount++;
				}

				@Override
				public void stop() {

				}

			}, 2);

			c.getItems().deleteItemInOneSlot(GREEN_EGG, 1);
			c.sendMessage("You poisoned the minion.");
			c.getBarbarianAssault().giveParticipationPoints(5);
			return true;

		case RED_EGG:
			if (!npc.isBarbAssaultNpc()) {
				return true;
			}

			c.getBarbarianAssault().dealDamageToNpcsInRadius(npc, 7, 1);

			c.getItems().deleteItemInOneSlot(RED_EGG, 1);
			c.sendMessage("You damaged the minion.");
			c.getBarbarianAssault().giveParticipationPoints(5);
			return true;

		case BLUE_EGG:
			if (!npc.isBarbAssaultNpc()) {
				return true;
			}

			final int collectorLevel = c.getBarbarianAssault()
					.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.COLLECTOR));

			npc.lockMovement(6 + (collectorLevel + 1));
			npc.startGraphic(Graphic.create(179, GraphicType.HIGH));

			c.getItems().deleteItemInOneSlot(BLUE_EGG, 1);
			c.sendMessage("You stunned the minion.");
			c.getBarbarianAssault().giveParticipationPoints(5);
			return true;

		case OMEGA_EGG:
			if (npc.npcType == PENANCE_QUEEN) {

				npc.dealDamage(new Hit(100, 2, -1));

				c.sendMessage("You poisoned the queen.");
			}

			c.getItems().deleteItemInOneSlot(OMEGA_EGG, 1);
			c.getBarbarianAssault().giveParticipationPoints(5);
			return true;

		}

		return false;
	}

	private void dealDamageToNpcsInRadius(NPC npc, int damage, int radius) {
		for (Iterator<NPC> itr = spawnedNpcs.values().iterator(); itr.hasNext();) {

			NPC next = itr.next();

			Location first = Location.create(npc.absX, npc.absY, npc.heightLevel);

			Location second = Location.create(next.absX, next.absY, next.heightLevel);

			final int collectorLevel = c.getBarbarianAssault()
					.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.COLLECTOR));

			if (first.isWithinDistance(second, radius)) {
				int random = Misc.random(damage) + 7 + (collectorLevel + 1);

				next.dealDamage(new Hit(random, 0, -1));
				next.startGraphic(Graphic.create(542));
			}
		}
	}

	public static boolean handleItemOnPlayer(Player c, Player o, int itemId) {
		switch (itemId) {

		case SCROLL:
			c.getBarbarianAssault().askToJoinGroup(o);
			return true;

		case POISONED_EGG:
			if (c.getBarbarianAssault().getRole() != Role.COLLECTOR) {
				return true;
			}

			if (o.getBarbarianAssault().getRole() != Role.ATTACKER) {
				c.sendMessage("You can only give this item to an attacker.");
				return true;
			}

			if (o.getItems().freeSlots() < 1) {
				c.sendMessage("That player doesn't have any inventory space.");
				return true;
			}

			c.getBarbarianAssault().giveParticipationPoints(5);

			c.getItems().deleteItemInOneSlot(POISONED_EGG, 1);
			o.getItems().addItem(POISONED_EGG, 1);

			c.sendMessage("You gave " + o.getNameSmartUp() + " the poisoned egg.");
			o.sendMessage("You received the poisoned egg from " + c.getNameSmartUp() + ".");
			return true;

		case SPIKED_EGG:
			if (c.getBarbarianAssault().getRole() != Role.ATTACKER) {
				return true;
			}

			if (o.getBarbarianAssault().getRole() != Role.COLLECTOR) {
				c.sendMessage("You can only give this item to a collector.");
				return true;
			}

			if (o.getItems().freeSlots() < 1) {
				c.sendMessage("That player doesn't have any inventory space.");
				return true;
			}

			c.getBarbarianAssault().giveParticipationPoints(5);

			c.getItems().deleteItemInOneSlot(SPIKED_EGG, 1);
			o.getItems().addItem(SPIKED_EGG, 1);

			c.sendMessage("You gave " + o.getNameSmartUp() + " the spiked egg.");
			o.sendMessage("You received the spiked egg from " + c.getNameSmartUp() + ".");
			return true;

		case 10542:
		case 10543:
		case 10544:
		case 10545:
			ItemOnPlayer.handleHealingVial(c, itemId, o.getId());
			c.getBarbarianAssault().giveParticipationPoints(5);
			return true;

		}

		return false;
	}

	public static boolean handleNpcSpell(Player c, NPC npc) {
		int npcId = npc.npcType;

		switch (npcId) {
		case PENANCE_QUEEN:
			if (npc.canWalk()) {
				npc.setCanWalk(false);
			}

			npc.setAliveTick(2000);

			if (Misc.random(7) == 1) {
				npc.attackType = 0;
				npc.projectileId = -1;
				npc.endGfx = -1;
			} else {
				npc.attackType = 1;
				// npc.gfx100(870);
				npc.projectileId = 871;
				npc.endGfx = 872;
			}
			return true;
		}

		return false;
	}

	public static boolean handleSecondClickObject(Player c, int objectId) {
		switch (objectId) {

		// attacker item machine
		case 20241:
			return true;

		case 20243:

			if (c.getBarbarianAssault().getRole() != Role.HEALER) {
				c.sendMessage("You need to have the role of healer to use this.");
				return true;
			}

			if (c.getItems().freeSlots() < 1) {
				c.sendMessage("You need more inventory space.");
				return true;
			}

			if (c.getItems().playerHasItem(10542) || c.getItems().playerHasItem(10543)
					|| c.getItems().playerHasItem(10544) || c.getItems().playerHasItem(10545)
					|| c.getItems().playerHasItem(10546)) {
				c.sendMessage("You already have this item.");
				return true;
			}

			c.getItems().addItem(10546, 1);
			return true;

		}

		return false;
	}

	public static boolean handleThirdClickObject(Player c, int objectId) {
		switch (objectId) {
		case 20150:
			if (c.getItems().playerHasItem(10546) || c.getItems().playerHasItem(10545)
					|| c.getItems().playerHasItem(10544) || c.getItems().playerHasItem(10543)) {
				if (c.getItems().playerHasItem(10546)) {
					c.getItems().deleteItemInOneSlot(10546, 1);
				} else if (c.getItems().playerHasItem(10545)) {
					c.getItems().deleteItemInOneSlot(10545, 1);
				} else if (c.getItems().playerHasItem(10544)) {
					c.getItems().deleteItemInOneSlot(10544, 1);
				} else if (c.getItems().playerHasItem(10543)) {
					c.getItems().deleteItemInOneSlot(10543, 1);
				}
				c.getItems().addItem(10542, 1);
				c.startAnimation(827);
				c.sendMessage("You fill the healing vial.");
			}
			return true;
		}
		return false;
	}

//	public static boolean isBarbarianAssaultNpc(int npcId) {
//		switch (npcId) {
//		case PENANCE_HEALER:
//		case PENANCE_RUNNER:
//		case PENANCE_FIGHTER_61:
//		case PENANCE_FIGHTER_68:
//		case PENANCE_FIGHTER_77:
//		case PENANCE_RANGER_57:
//		case PENANCE_RANGER_64:
//		case PENANCE_RANGER_72:
//			return true;
//		}
//		return false;
//	}

	public static boolean isPenanceFighter(int npcIndex) {

		NPC npc = NPCHandler.npcs[npcIndex];

		if (npc == null) {
			return false;
		}

		switch (npc.npcType) {

		case PENANCE_FIGHTER_61:
		case PENANCE_FIGHTER_68:
		case PENANCE_FIGHTER_77:
			return true;

		}
		return false;
	}

	public static boolean isPenanceRanger(int npcIndex) {

		NPC npc = NPCHandler.npcs[npcIndex];

		if (npc == null) {
			return false;
		}

		switch (npc.npcType) {
		case PENANCE_RANGER_57:
		case PENANCE_RANGER_64:
		case PENANCE_RANGER_72:
			return true;
		}
		return false;
	}

	public static void onAttackNpc(NPC attacker, NPC defender) {

		if (attacker == null || defender == null) {
			return;
		}

		if (attacker.npcType != BarbarianAssault.PENANCE_HEALER
				&& !defender.isBarbAssaultNpc()) {
			return;
		}

		attacker.startGraphic(Graphic.create(864));
		attacker.startAnimation(5105);

		defender.healNpc(5);
	}

	public static void onDeath(Player c) {
		if (!c.getBarbarianAssault().inArena())
			return;
		c.getBarbarianAssault().endGame(false);
	}

	public static boolean onNpcDeath(Player c, NPC npc) {

		if (!c.getBarbarianAssault().inArena())
			return false;

		if (npc == null) {
			return false;
		}

		switch (npc.npcType) {

		case PENANCE_QUEEN:

			for (Player next : c.getBarbarianAssault().group.values()) {
				final int participation = next.getBarbarianAssault().participation;

				if (participation >= 50) {
					next.getBarbarianAssault().setHonorPoints(next.getBarbarianAssault().getHonorPoints() + HONOR_POINT_RATE, true);

					next.sendMessage(String.format("You received %d honor points! points: %d", HONOR_POINT_RATE,
							next.getBarbarianAssault().honorPoints));
				}

			}

			c.getBarbarianAssault().endGame(true);
			return true;

		case PENANCE_HEALER:
		case PENANCE_RUNNER:
		case PENANCE_RANGER_57:
		case PENANCE_RANGER_64:
		case PENANCE_RANGER_72:
		case PENANCE_FIGHTER_77:
		case PENANCE_FIGHTER_61:
		case PENANCE_FIGHTER_68:
			c.getBarbarianAssault().giveParticipationPoints(5);

			int startX = npc.getX() - 1;
			int startY = npc.getY() - 1;

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					GroundItem egg = new GroundItem(Misc.random(eggDropArray), startX + i, startY + j, c.heightLevel, 1,
							0);
					egg.setRemoveTicks(Misc.random(250) + 50);
					ItemHandler.createGroundItem(egg, true);
				}

			}

			c.getBarbarianAssault().spawnedNpcs.remove(npc.npcIndex);

			return true;

		}

		return false;
	}

	public static void process(Player c) {

		// only run process for 1 player in each group
		if (c.getBarbarianAssault().getRole() != Role.ATTACKER && !c.getBarbarianAssault().gameRunning) {
			return;
		}

		if (!c.getBarbarianAssault().gameRunning) {
			return;
		}

		final int height = c.heightLevel;

		final boolean queenSpawned = c.getBarbarianAssault().queenSpawned;

		Map<Integer, NPC> npcs = c.getBarbarianAssault().spawnedNpcs;

		if (Misc.random(npcs.size() > 2 ? npcs.size() * 13 : 1) == 1) {

			if (Misc.random(6) == 1) {

				Location spawn = Misc.random(spawns);

				NPC ranger = NPCHandler.spawnNpc(c.getBarbarianAssault().getRandomPlayer(), PENANCE_RANGER_57,
						spawn.getX(), spawn.getY(), height, 1, true, false);
				ranger.setBarbAssaultNpc(true);

				ranger.setAliveTick(400);

				npcs.put(ranger.npcIndex, ranger);

			} else if (Misc.random(4) == 1) {

				Location spawn = Misc.random(spawns);

				NPC ranger = NPCHandler.spawnNpc(c.getBarbarianAssault().getRandomPlayer(), PENANCE_RANGER_64,
						spawn.getX(), spawn.getY(), height, 1, true, false);
				ranger.setAliveTick(400);

				ranger.setBarbAssaultNpc(true);

				npcs.put(ranger.npcIndex, ranger);

			} else {

				Location spawn = Misc.random(spawns);

				NPC ranger = NPCHandler.spawnNpc(c.getBarbarianAssault().getRandomPlayer(), PENANCE_RANGER_72,
						spawn.getX(), spawn.getY(), height, 1, true, false);
				ranger.setBarbAssaultNpc(true);
				ranger.setAliveTick(400);

				npcs.put(ranger.npcIndex, ranger);

			}
		}

		if (Misc.random(npcs.size() > 2 ? npcs.size() * 15 : 1) == 1) {
			if (Misc.random(6) == 1) {

				Location spawn = Misc.random(spawns);

				NPC fighter = NPCHandler.spawnNpc(c.getBarbarianAssault().getRandomPlayer(), PENANCE_FIGHTER_61,
						spawn.getX(), spawn.getY(), height, 1, true, false);
				fighter.setBarbAssaultNpc(true);
				fighter.setAliveTick(400);

				npcs.put(fighter.npcIndex, fighter);

			} else if (Misc.random(4) == 1) {

				Location spawn = Misc.random(spawns);

				NPC fighter = NPCHandler.spawnNpc(c.getBarbarianAssault().getRandomPlayer(), PENANCE_FIGHTER_68,
						spawn.getX(), spawn.getY(), height, 1, true, false);
				fighter.setBarbAssaultNpc(true);
				fighter.setAliveTick(400);

				npcs.put(fighter.npcIndex, fighter);

			} else {

				Location spawn = Misc.random(spawns);

				NPC fighter = NPCHandler.spawnNpc(c.getBarbarianAssault().getRandomPlayer(), PENANCE_FIGHTER_77,
						spawn.getX(), spawn.getY(), height, 1, true, false);
				fighter.setBarbAssaultNpc(true);
				fighter.setAliveTick(400);

				npcs.put(fighter.npcIndex, fighter);

			}
		}

		if (Misc.random(queenSpawned ? 2000 : (npcs.size() + 1) * 20) == 1) {
			Location spawn = Misc.random(spawns);

			NPC healer = NPCHandler.spawnNpc2(PENANCE_HEALER, spawn.getX(), spawn.getY(), height, 1);
			healer.setBarbAssaultNpc(true);
			healer.setAliveTick(1000);

			npcs.put(healer.npcIndex, healer);
		}

		if (Misc.random(queenSpawned ? 2000 : (npcs.size() + 1) * 15) == 1) {

			Location spawn = Misc.random(spawns);

			NPC runner = NPCHandler.spawnNpc(c, PENANCE_RUNNER, spawn.getX(), spawn.getY(), height, 0, 100);

			runner.setBarbAssaultNpc(true);
			npcs.put(runner.npcIndex, runner);

		}

		processNpcs(npcs);

	}

	private static int tick = 0;

	private static void processNpcs(Map<Integer, NPC> npcs) {
		if (npcs.isEmpty()) {
			return;
		}

		if (tick == 100) {
			tick = 0;
		}

		for (NPC next : npcs.values()) {

			if (next == null || !next.isActive) {
				continue;
			}

			if (next.npcType == PENANCE_QUEEN) {

				if ((tick % 30) == 0) {

					if (next.spawnedBy != -1) {

						Player player = (Player) PlayerHandler.players[next.spawnedBy];

						if (player != null) {
							GroundItem egg = new GroundItem(YELLOW_EGG,
									next.absX + next.getSize()
											+ Misc.random(new int[] { -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 }),
									next.absY + next.getSize()
											+ Misc.random(new int[] { -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 }),
									next.heightLevel, 1, 0);
							egg.setRemoveTicks(50);
							ItemHandler.createGroundItem(egg, true);
						}

					}

				}
			}

			if (next.npcType == PENANCE_RUNNER) {

				if (next.destinationReached(RUNNER_EXIT1) || next.destinationReached(RUNNER_EXIT2)) {

					Player p = (Player) PlayerHandler.players[next.spawnedBy];

					if (p == null) {
						continue;
					}

					final Map<Role, Player> group = p.getBarbarianAssault().getGroup();

					for (Iterator<Player> itr = group.values().iterator(); itr.hasNext();) {

						Player nextp = itr.next();

						if (nextp == null) {
							continue;
						}

						final int damage = Misc.random(30) + 20;

						nextp.startGraphic(Graphic.create(542, GraphicType.LOW));
						nextp.dealDamage(new Hit(damage, 0, 2));

					}

					next.die();

				} else {
					if ((tick % 5) == 0) {
						next.walkToLocation(Misc.random(new Location[] { RUNNER_EXIT1, RUNNER_EXIT2 }));
					}
				}

			}

			if (next.npcType == PENANCE_HEALER) {

				if (next.killNpc != -1) {
					NPC n = NPCHandler.npcs[next.killNpc];

					if (n == null) {
						next.killNpc = -1;
					} else if (n.getSkills().getLifepoints() >= n.getSkills().getMaximumLifepoints()) {
						next.killNpc = -1;
					}

				}

				// get random npc that is damaged

				Optional<NPC> result = getRandomDamagedNpc(npcs.values());

				if (!result.isPresent()) {
					continue;
				}

				NPC npcToHeal = result.get();

				if (npcToHeal == null || npcToHeal.npcIndex == next.npcIndex || npcToHeal.npcType == PENANCE_HEALER) {
					continue;
				}

				if (next.killNpc == -1) {
					next.killNpc = npcToHeal.npcIndex;
				}

			}

		}

		tick++;

	}

	public static boolean readScroll(Player c, int itemId) {
		if (itemId == 10512) {
			c.getPacketSender().sendString("<col=ffffff>- Your recruits -", 6968);
			c.getPacketSender().sendString("", 6969);

			if (c.getBarbarianAssault().getGroup().containsKey(Role.ATTACKER)) {

				Player other = c.getBarbarianAssault().getGroup().get(Role.ATTACKER);

				c.getPacketSender()
						.sendString(
								"<col=ff0000>" + other.getBarbarianAssault().getRole().getName() + ": <col=D9D9D9>"
										+ other.getNameSmartUp() + " (Level "
										+ other.getBarbarianAssault().getRoleLevel(
												other.getBarbarianAssault().getRoleExperience(Role.ATTACKER))
										+ ")",
								6970);
			} else {
				c.getPacketSender().sendString("<col=ff0000>Attacker: <col=D9D9D9>none set", 6970);
			}

			if (c.getBarbarianAssault().getGroup().containsKey(Role.DEFENDER)) {

				Player other = c.getBarbarianAssault().getGroup().get(Role.DEFENDER);

				c.getPacketSender()
						.sendString(
								"<col=ff>" + other.getBarbarianAssault().getRole().getName() + ": <col=D9D9D9>"
										+ other.getNameSmartUp() + " (Level "
										+ other.getBarbarianAssault().getRoleLevel(
												other.getBarbarianAssault().getRoleExperience(Role.DEFENDER))
										+ ")",
								6971);
			} else {
				c.getPacketSender().sendString("<col=ff>Defender: <col=D9D9D9>none set", 6971);
			}

			if (c.getBarbarianAssault().getGroup().containsKey(Role.HEALER)) {

				Player other = c.getBarbarianAssault().getGroup().get(Role.HEALER);

				c.getPacketSender().sendString(
						"<col=ff00>" + other.getBarbarianAssault().getRole().getName() + ": <col=D9D9D9>" + other.getNameSmartUp()
								+ " (Level "
								+ other.getBarbarianAssault()
										.getRoleLevel(other.getBarbarianAssault().getRoleExperience(Role.HEALER))
								+ ")",
						6972);
			} else {
				c.getPacketSender().sendString("<col=ff00>Healer: <col=D9D9D9>none set", 6972);
			}

			if (c.getBarbarianAssault().getGroup().containsKey(Role.COLLECTOR)) {

				Player other = c.getBarbarianAssault().getGroup().get(Role.COLLECTOR);

				c.getPacketSender()
						.sendString(
								"<col=ffff00>" + other.getBarbarianAssault().getRole().getName() + " : <col=D9D9D9>"
										+ other.getNameSmartUp() + " (Level "
										+ other.getBarbarianAssault().getRoleLevel(
												other.getBarbarianAssault().getRoleExperience(Role.COLLECTOR))
										+ ")",
								6973);
			} else {
				c.getPacketSender().sendString("<col=ffff00>Collector: <col=D9D9D9>none set", 6973);
			}

			c.getPacketSender().sendString("", 6974);
			c.getPacketSender().sendString("<col=ffb000>My role: <col=D9D9D9>" + c.getBarbarianAssault().getRole().getName(), 6975);
			c.getPacketSender().showInterface(SCROLL_INTERFACE);
			return true;
		}
		return false;
	}

	public void showReward(int honorPoints, int attackerPoints, int defenderPoints, int healerPoints,
			int collectorPoints) {
		c.getPacketSender().sendString("<col=ffffff>Wave Complete!", 6968);
		c.getPacketSender().sendString("<col=ffffff>The Queen Is Dead!", 6969);
		c.getPacketSender().sendString("<col=ff7000>Reward:", 6970);

		c.getPacketSender().sendString("<col=ffffff>" + honorPoints + " <col=A10081>honour points", 6971);
		c.getPacketSender().sendString("<col=ffffff>" + attackerPoints + " <col=ff0000>attacker points", 6972);
		c.getPacketSender().sendString("<col=ffffff>" + defenderPoints + " <col=ff>defender points", 6973);
		c.getPacketSender().sendString("<col=ffffff>" + healerPoints + " <col=ff00>healer points", 6974);
		c.getPacketSender().sendString("<col=ffffff>" + collectorPoints + " <col=ffff00>collector points", 6975);

		c.getPacketSender().showInterface(6965);
	}

	public static boolean writeRole(Player c, int itemId) {
		if (itemId == SCROLL) {
			((Client)c).getDH().sendOption4("Attacker", "Defender", "Healer", "Collector");
			c.dialogueAction = 21;
			return true;
		}
		return false;
	}

	private boolean gameRunning;

	private Map<Role, Player> group = new HashMap<>(MAX_GROUP_SIZE);

	private Map<Integer, NPC> spawnedNpcs = new HashMap<>();

	private int participation;

	private int honorPoints;

	private Player c;

	private Role role = Role.NONE;

	private Player partyLeader;

	public BarbarianAssault(Player c) {
		this.c = c;
	}

	public boolean askToJoinGroup(Player other) {

		if (c.getBarbarianAssault().group.size() > 1 && c != partyLeader) {
			c.sendMessage("You can only invite players if you are the party leader.");
			return false;
		}

		if (other.getBarbarianAssault().group.containsValue(c)) {
			c.sendMessage("That player is already in your group.");
			return false;
		}

		if (other.getBarbarianAssault().group.size() >= MAX_GROUP_SIZE) {
			c.sendMessage("That player already has a full party.");
			return false;
		}

		if (!other.getBarbarianAssault().group.isEmpty()) {
			c.sendMessage("That player is already in a group.");
			return false;
		}

		if (c.getBarbarianAssault().getGroup().size() >= MAX_GROUP_SIZE) {
			c.sendMessage("You have a full party.");
			return false;
		}

		if (hasRole(other)) {
			c.sendMessage("This player is " + Misc.aOrAn(other.getBarbarianAssault().getRole().getName()) + " "
					+ other.getBarbarianAssault().getRole().getName() + " you already have this role.");
			return false;
		}

		if (role == Role.NONE) {
			((Client)c).getDH().sendStatement("You need to choose a role first.");
			c.nextChat = 600;
			return false;
		}

		invitePlayer(other);
		return true;
	}

	public boolean assignRole(Role role) {

		if (this.role == role) {
			c.sendMessage("You are already assigned to this role.");
			c.getPA().closeAllWindows();
			return false;
		}

		if (group.containsKey(role)) {
			c.sendMessage("This role is already being used.");
			c.getPA().closeAllWindows();
			return false;
		}

		// previous role
		group.remove(this.role);

		this.role = role;

		// current role
		getGroup().put(role, c);

		c.sendMessage("You have assigned yourself the role of: " + role.getName());

		c.getPA().closeAllWindows();

		return true;
	}

	public boolean canLogout() {
		if (inArena()) {
			return false;
		}

		return true;
	}

	public boolean canUsePrayer() {
		return !inArena() && !inLobby();
	}

	public void displayLobbyInterface() {

		c.getPA().walkableInterface(23773);

		if (partyLeader == null) {

			c.getPacketSender().sendString("Leader:  -----", 23775);

			for (int i = 0; i < 3; i++) {
				c.getPacketSender().sendString("Empty" + ": -----", i + 23776);
			}

		} else {

			final int leaderLevel = c.getBarbarianAssault()
					.getRoleLevel(c.getBarbarianAssault().getRoleExperience(c.getBarbarianAssault().getRole()));

			c.getPacketSender().sendString("Leader:  " + partyLeader.getNameSmartUp() + "  Lvl " + leaderLevel, 23775);

			Role[] otherRoles = partyLeader.getBarbarianAssault().getActiveRolesExcludingSelf();

			for (int i = 0; i < otherRoles.length; i++) {

				Player other = partyLeader.getBarbarianAssault().group.get(otherRoles[i]);

				final int roleLevel = other.getBarbarianAssault().getRoleLevel(
						other.getBarbarianAssault().getRoleExperience(other.getBarbarianAssault().getRole()));

				final Role otherRole = other.getBarbarianAssault().getRole();

				c.getPacketSender().sendString(otherRole.getName() + ": " + other.getNameSmartUp() + "  Lvl " + roleLevel,
						i + 23776);

			}

			for (int i = otherRoles.length; i < 3; i++) {
				c.getPacketSender().sendString("Empty" + ": -----", i + 23776);
			}

		}

		c.getPacketSender().sendString("", 23779);

	}

	public void displayArenaInterface() {

		c.getPA().walkableInterface(24439);

		c.getPacketSender().sendString("<col=ff981f>Team", 24440);

		for (int i = 0; i < Role.values.length; i++) {

			Player other = c.getBarbarianAssault().group.get(Role.values[i]);

			if (other == null || !other.isActive) {
				c.getPacketSender().sendString("<col=ff981f>Player" + (i + 1) + ": <col=0>--%", i + 24441);
				continue;
			}

			final int percent = (int) (((double) other.getHitPoints() / other.getSkills().getMaximumLifepoints()) * 100);

			final String color = percent > 75 ? "<col=ff00>"
					: percent > 50 && percent <= 75 ? "<col=ffff00>" : percent > 25 && percent <= 50 ? "<col=ffb000>" : "<col=ff0000>";

			c.getPacketSender().sendString(
					String.format("%s%s: %s%s", "<col=ff981f>", other.getNameSmartUp(), color, percent + "%"), i + 24441);

		}

		// player 5
		c.getPacketSender().sendString("", 24445);

	}

	public void endGame(boolean won) {
		// remove all the npcs

		for (Iterator<NPC> itr = spawnedNpcs.values().iterator(); itr.hasNext();) {

			NPC npc = itr.next();

			if (npc != null) {
				npc.barbAssaultRunning = false;
				npc.setDead(true);
			}

			itr.remove();
		}

		// handle the players
		for (Iterator<Entry<Role, Player>> keys = group.entrySet().iterator(); keys.hasNext();) {
			Entry<Role, Player> next = keys.next();

			Player other = next.getValue();

			if (other == null || !other.isActive) {
				continue;
			}

			if (c == other) {
				other.sendMessage(c.isDead() ? "You died..." : "You left the game.");
			} else {
				other.sendMessage(c.isDead() ? c.getNameSmartUp() + " died..." : c.getNameSmartUp() + " left the game.");
			}

			if (!other.isDead()) {
				other.getPA().movePlayer(
						BarbarianAssault.WAITING_ROOM.getX() + Misc.random(new int[] { -2, -1, 0, 1, 2 }),
						BarbarianAssault.WAITING_ROOM.getY() + Misc.random(new int[] { -2, -1, 0, 1, 2 }), 0);
			}

			other.sendMessage(won ? "You won!" : "You lost! Better luck next time.");

			if (won) {
				other.getExtraHiscores().incBarbarianAssaultWins();
				other.getBarbarianAssault().givePoints(other.getBarbarianAssault().getRole());

				final Role r = other.getBarbarianAssault().getRole();

				other.getBarbarianAssault().showReward(HONOR_POINT_RATE, r == Role.ATTACKER ? 30 : 3,
						r == Role.DEFENDER ? 30 : 3, r == Role.HEALER ? 30 : 3, r == Role.COLLECTOR ? 30 : 3);
				// TODO display scroll
			}

			other.getBarbarianAssault().reset(false);

		}

	}

	private void saveExp() {
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

			final String sql = "UPDATE `accounts` SET barbRoleExp = ? WHERE ID = ?;";

			try (java.sql.Connection connection = Server.getConnection();
					PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, PlayerSaveSql.createIntArrayWithSeperator(c.roleExp));
				statement.setInt(2, c.mySQLIndex);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	private void saveHonorPoints() {
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

			final String sql = "UPDATE `accounts` SET honorPoints = ? WHERE ID = ?;";

			try (java.sql.Connection connection = Server.getConnection();
					PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, c.getBarbarianAssault().getHonorPoints());
				statement.setInt(2, c.mySQLIndex);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	private void saveRolePoints() {
		PlayerSaveSql.SAVE_EXECUTOR.submit(() -> {

			final String sql = "UPDATE `accounts` SET barbRolePoints = ? WHERE ID = ?;";

			try (java.sql.Connection connection = Server.getConnection();
					PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, PlayerSaveSql.createIntArrayWithSeperator(c.rolePoints));
				statement.setInt(2, c.mySQLIndex);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public int getCostForLevel(int level) {
		switch (level) {
		case 2:
			return 200;

		case 3:
			return 300;

		case 4:
			return 400;

		case 5:
			return 500;

		default:
			return -1;
		}
	}

	public void givePoints(Role role) {
		switch (role) {

		case ATTACKER:
			c.rolePoints[0] += ROLE_POINT_RATE;
			c.rolePoints[1] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[2] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[3] += ROLE_OTHER_POINT_RATE;
			break;

		case DEFENDER:
			c.rolePoints[0] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[1] += ROLE_POINT_RATE;
			c.rolePoints[2] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[3] += ROLE_OTHER_POINT_RATE;
			break;

		case HEALER:
			c.rolePoints[0] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[1] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[2] += ROLE_POINT_RATE;
			c.rolePoints[3] += ROLE_OTHER_POINT_RATE;
			break;

		case COLLECTOR:
			c.rolePoints[0] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[1] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[2] += ROLE_OTHER_POINT_RATE;
			c.rolePoints[3] += ROLE_POINT_RATE;
			break;

		case NONE:
			break;

		default:
			break;

		}

	}

	public Role[] getActiveRoles() {

		List<Role> list = new ArrayList<>();

		if (group.containsKey(Role.ATTACKER)) {
			list.add(Role.ATTACKER);
		}

		if (group.containsKey(Role.DEFENDER)) {
			list.add(Role.DEFENDER);
		}

		if (group.containsKey(Role.HEALER)) {
			list.add(Role.HEALER);
		}

		if (group.containsKey(Role.COLLECTOR)) {
			list.add(Role.COLLECTOR);
		}

		return list.stream().toArray(Role[]::new);
	}

	public Role[] getActiveRolesExcludingSelf() {

		List<Role> list = new ArrayList<>();

		if (group.containsKey(Role.ATTACKER) && role != Role.ATTACKER) {
			list.add(Role.ATTACKER);
		}

		if (group.containsKey(Role.DEFENDER) && role != Role.DEFENDER) {
			list.add(Role.DEFENDER);
		}

		if (group.containsKey(Role.HEALER) && role != Role.HEALER) {
			list.add(Role.HEALER);
		}

		if (group.containsKey(Role.COLLECTOR) && role != Role.COLLECTOR) {
			list.add(Role.COLLECTOR);
		}

		return list.stream().toArray(Role[]::new);
	}

	public int getBarbarianPoints() {
		return honorPoints;
	}

	public Map<Role, Player> getGroup() {
		return group;
	}

	public Role[] getInactiveRoles() {
		Set<Role> list = new LinkedHashSet<>();

		if (!group.containsKey(Role.ATTACKER)) {
			list.add(Role.ATTACKER);
		}

		if (!group.containsKey(Role.DEFENDER)) {
			list.add(Role.DEFENDER);
		}

		if (!group.containsKey(Role.HEALER)) {
			list.add(Role.HEALER);
		}

		if (!group.containsKey(Role.COLLECTOR)) {
			list.add(Role.COLLECTOR);
		}

		return list.stream().toArray(Role[]::new);
	}

	public int getParticipation() {
		return participation;
	}

	public Player getPartyLeader() {
		return partyLeader;
	}

	public Player getRandomPlayer() {

		Role[] activeRoles = getActiveRoles();

		return group.get(activeRoles[Misc.random(activeRoles.length - 1)]);
	}

	public Role getRole() {
		return role;
	}

	public void giveParticipationPoints(int participation) {
		this.participation += participation;
	}

	public boolean hasRole(Player other) {
		return other.getBarbarianAssault().getGroup().containsKey(role);
	}

	public boolean inArena() {
		return c.inArea(1860, 5450, 1910, 5495);
	}

	public boolean inLobby() {
		return c.inArea(2584, 5271, 2591, 5278);
	}

	public void invitePlayer(Player other) {
		c.face(other);
		this.partyLeader = (Player) PlayerHandler.players[c.getId()];
		other.getBarbarianAssault().partyLeader = this.partyLeader;

		other.getDialogueBuilder().sendStatement(c.getNameSmartUp() + " has invited you to the group.").executeIfNotActive();
		other.nextChat = 599;
	}

	public boolean isGameRunning() {
		return gameRunning;
	}

	public void joinGroup(Player other) {
		other.getBarbarianAssault().getGroup().put(role, c);

		group = other.getBarbarianAssault().getGroup();

		final int otherLevel = c.getBarbarianAssault()
				.getRoleLevel(c.getBarbarianAssault().getRoleExperience(c.getBarbarianAssault().getRole()));

		c.sendMessage("You have joined: " + other.getNameSmartUp() + "'s group as " + Misc.aOrAn(role.getName()) + " "
				+ role.getName() + " Lvl " + otherLevel);

		Role[] neededRoles = getInactiveRoles();

		c.sendMessage(neededRoles.length == 0 ? "Your team is full." : "Roles needed: " + Arrays.toString(neededRoles));

		other.sendMessage(c.getNameSmartUp() + " has joined your group as " + Misc.aOrAn(role.getName()) + " "
				+ role.getName() + " Lvl " + otherLevel);

		other.sendMessage(
				neededRoles.length == 0 ? "You have a full party" : "Roles needed: " + Arrays.toString(neededRoles));
	}

	public void kickPlayer(Player other) {

		if (inArena() && gameRunning) {
			c.sendMessage("You can't kick a player while in game.");
			return;
		}

		for (Iterator<Entry<Role, Player>> keys = group.entrySet().iterator(); keys.hasNext();) {
			Entry<Role, Player> next = keys.next();

			Player value = next.getValue();

			if (value != c && other != value) {
				value.sendMessage(other.getNameSmartUp() + " was kicked by " + c.getNameSmartUp() + ".");
			}

			if (next.getKey() == other.getBarbarianAssault().getRole()) {
				keys.remove();
			}

		}

		c.sendMessage("You kicked " + other.getNameSmartUp() + ".");
		other.sendMessage(c.getNameSmartUp() + " kicked you.");
		other.getBarbarianAssault().reset(true);

		c.getPA().closeAllWindows();
	}

	public void leaveGroup() {
		if (group.isEmpty()) {
			return;
		}

		try {

			for (Iterator<Entry<Role, Player>> keys = group.entrySet().iterator(); keys.hasNext();) {
				Entry<Role, Player> next = keys.next();

				Player other = next.getValue();

				if (c == other) {
					c.sendMessage(c == partyLeader ? "Your team has been dismantled." : "You left the group.");
				} else {

					if (c == partyLeader) {
						other.sendMessage("Your team has been dismantled.");
						other.getBarbarianAssault().reset(true);
					} else {
						other.sendMessage(c.getNameSmartUp() + " has left the group. Roles needed: "
								+ Arrays.toString(other.getBarbarianAssault().getInactiveRoles()));
					}

				}

				if (next.getKey() == this.role) {
					keys.remove();
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		reset(true);

		c.getPA().closeAllWindows();
		return;
	}

	public void onLogin() {
		c.getPA().movePlayer(BarbarianAssault.LOBBY.getX() + Misc.random(new int[] { -2, -1, 0, 1, 2 }),
				BarbarianAssault.LOBBY.getY() + Misc.random(new int[] { -2, -1, 0, 1, 2 }), 0);
		c.getBarbarianAssault().reset(true);
	}

	public void displayCape() {

		final int capeId = getRoleCape();

		if (capeId == -1) {
			return;
		}

		c.displayEquipment[PlayerConstants.playerCape] = getRoleCape();
	}

	public void removeCape() {
		c.displayEquipment[PlayerConstants.playerCape] = -1;
	}

	public void reset(boolean resetGroup) {
		
		if (!c.isActive)
			return;

		if (resetGroup) {
			partyLeader = null;
			role = Role.NONE;
			group = new HashMap<>();
		}

		spawnedNpcs = new HashMap<>();

		for (int itemId : baItems) {
			if (c.getItems().playerHasItem(itemId)) {

				int amount = c.getItems().getItemCount(itemId);

				c.getItems().deleteItem2(itemId, amount);
			}
		}

		gameRunning = false;

		CycleEventHandler.getSingleton().stopEvents(c, CycleEventIds.BARB_ASSULT.ordinal());
		CycleEventHandler.getSingleton().stopEvents(c, CycleEventIds.BARB_ASSULT2.ordinal());

		removeCape();
		
		saveExp();
		saveHonorPoints();
		saveRolePoints();

		queenSpawned = false;

		c.getPA().restorePlayer(false);
	}

	public void setHonorPoints(int honorPoints, boolean saveGame) {
		this.honorPoints = honorPoints;
	}

	public int getHonorPoints() {
		return honorPoints;
	}

	public void setParticipation(int participation) {
		this.participation = participation;
	}

	public void startGame() {
		c.getPA().closeAllWindows();
		c.startAnimation(Animation.CLIMB_UP, 2, new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception {
				Player attacker = group.get(Role.ATTACKER);

				if (attacker == null) {
					return false;
				}

				for (Entry<Role, Player> next : group.entrySet()) {
					Player client = next.getValue();

					if (client == null || !client.isActive) {
						c.getBarbarianAssault().endGame(false);
						return false;
					}

					if (client.isActive) {

						client.getPA().closeAllWindows();

						client.getBarbarianAssault().spawnedNpcs = attacker.getBarbarianAssault().spawnedNpcs;

						client.getBarbarianAssault().displayCape();

						client.getPA().restorePlayer(true);

						client.getPA().movePlayer(MAP.getX(), MAP.getY(), attacker.getId() * 4);

						client.getItems().deleteItem2(SCROLL, 1);

						client.sendMessage("Minigame starts in 10 seconds.");

					}

				}

				CycleEventHandler.getSingleton().addEvent(CycleEventIds.BARB_ASSULT.ordinal(), attacker,
						new CycleEvent() {

							private int tick = 0;

							@Override
							public void execute(CycleEventContainer container) {

								if (Misc.ticksIntoSeconds(tick) >= 10) {
									container.stop();
									return;
								}

								for (Player next : attacker.getBarbarianAssault().group.values()) {

									if (next == null || !next.isActive) {
										attacker.getBarbarianAssault().endGame(false);
										container.stop();
										return;
									}

									final int ticks = Misc.secondsToTicks(10);

									final int countDown = ticks - tick;

									if (Misc.ticksIntoSeconds(countDown) <= 0) {
										next.sendMessage("Minigame has started, good luck!");

										next.getBarbarianAssault().gameRunning = true;
									}

								}

								tick++;

							}

							@Override
							public void stop() {
								tick = 0;
							}

						}, 2);

				CycleEventHandler.getSingleton().addEvent(CycleEventIds.BARB_ASSULT2.ordinal(), attacker,
						new CycleEvent() {

							@Override
							public void execute(CycleEventContainer container) {
								NPC npc = NPCHandler.spawnNpc(attacker, PENANCE_QUEEN, 1884, 5465, attacker.getId() * 4,
										0, true, true);
								npc.setBarbAssaultNpc(true);

								attacker.getBarbarianAssault().spawnedNpcs.put(npc.npcIndex, npc);

								for (Iterator<Player> itr = attacker.getBarbarianAssault().group.values()
										.iterator(); itr.hasNext();) {

									Player next = itr.next();

									if (next == null || !next.isActive) {
										attacker.getBarbarianAssault().endGame(false);
										container.stop();
										return;
									}

									next.getBarbarianAssault().queenSpawned = true;

								}

								container.stop();
							}

							@Override
							public void stop() {
							}

						}, 500);
				return true;
			}

		});
	}

	public boolean canUseSummoning() {
		return !inArena() && !inLobby();
	}

	public int getRoleLevel(int experience) {

		if (experience > 1399) {
			return 5;
		} else if (experience > 899) {
			return 4;
		} else if (experience > 499) {
			return 3;
		} else if (experience > 199) {
			return 2;
		} else {
			return 1;
		}
	}

	public int getRoleExperience(Role role) {
		switch (role) {
		case ATTACKER:
			return c.roleExp[0];

		case DEFENDER:
			return c.roleExp[1];

		case HEALER:
			return c.roleExp[2];

		case COLLECTOR:
			return c.roleExp[3];

		default:
			return 0;
		}

	}

	public int getRoleCape() {
		final int level = getRoleLevel(getRoleExperience(role));

		if (role == Role.ATTACKER) {
			return ATTACKER_CAPE_LVL_1 + level;
		} else if (role == Role.DEFENDER) {
			return DEFENDER_CAPE_LVL_1 + level;
		} else if (role == Role.HEALER) {
			return HEALER_CAPE_LVL_1 + level;
		} else if (role == Role.COLLECTOR) {
			return COLLECTOR_CAPE_LVL_1 + level;
		} else {
			return -1;
		}
	}

	public void addAttackerExp(int attackerExp) {
		c.roleExp[0] = attackerExp;
	}

	public void addDefenderExp(int defenderExp) {
		c.roleExp[1] = defenderExp;
	}

	public void addHealerExp(int healerExp) {
		c.roleExp[2] = healerExp;
	}

	public void addCollectorExp(int collectorExp) {
		c.roleExp[3] = collectorExp;
	}

}
