package com.soulplay.content.minigames.ddmtournament;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.game.model.player.Player;

public class Settings {
	
	private int lowestCmbLevel = 3;
	
	private int highestCmbLevel = 200;
	
	private int[] restockItems;
	
	private boolean safeDeath;
	
	private boolean spawnGear;
	
	public Set<Integer> bannedItems = new HashSet<>();
	
	private boolean disablePrayers;
	
	private boolean disableProtectPrayers;
	
	private boolean disableMembersPrayers;
	
	private SpellBook spellBook;
	
	private PrayerBook prayerBook;
	
	private boolean spawnSkills;
	
	private boolean officialTournament;
	
	private Presets presetType = Presets.BOXING;

	private static final int[] DEFAULT_RESTOCK_ITEMS = { 9739, 171, 6685, 3024, 3024, 385, 385, 385, 385, 385, 385, 385, 385, 385, 385, 385, 385, 385, 3144, 3144 };
	
	private static final int[] NH_RESTOCK_ITEMS = { 9739, 3040, 2444, 3024, 3024, 3024, 6685, 6685, 6685, 6685, 6685, 6685, 391, 391, 391, 391, 391, 391, 391, 391, 391 };
	
	private static final int[] F2P_RESTOCK_ITEMS = { 113, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 373, 2297, 2297, 2297, 2297, 2297, 2297 };
	
	private static final int[] BOXING_RESTOCK_ITEMS = { 7671 };
	
	public void createDefaults() {
		restockItems = Arrays.copyOf(Settings.DEFAULT_RESTOCK_ITEMS, Settings.DEFAULT_RESTOCK_ITEMS.length);
		safeDeath = false;
		spawnGear = true;//false;
		disablePrayers = false;
		spellBook = SpellBook.NORMAL;
		prayerBook = PrayerBook.NORMAL;
		bannedItems.clear();
		spawnSkills = false;
		lowestCmbLevel = 3;
		highestCmbLevel = 200;
		disableProtectPrayers = false;
		disableMembersPrayers = false;
	}
	
	public void setNhRestockItems() {
		restockItems = Arrays.copyOf(Settings.NH_RESTOCK_ITEMS, Settings.NH_RESTOCK_ITEMS.length);
	}
	
	public void setDefaultRestockItems() {
		restockItems = Arrays.copyOf(Settings.DEFAULT_RESTOCK_ITEMS, Settings.DEFAULT_RESTOCK_ITEMS.length);
	}
	
	public void setBoxingRestockItems() {
		restockItems = Arrays.copyOf(Settings.BOXING_RESTOCK_ITEMS, Settings.BOXING_RESTOCK_ITEMS.length);
	}
	
	public void setF2PRestockItems() {
		restockItems = Arrays.copyOf(Settings.F2P_RESTOCK_ITEMS, Settings.F2P_RESTOCK_ITEMS.length);
	}
	
	public void copy(Settings copyFrom) {
		this.lowestCmbLevel = copyFrom.lowestCmbLevel;
		this.highestCmbLevel = copyFrom.highestCmbLevel;
		this.restockItems = Arrays.copyOf(copyFrom.restockItems, copyFrom.restockItems.length);
		this.safeDeath = copyFrom.safeDeath;
		this.bannedItems.addAll(copyFrom.bannedItems);
		this.disablePrayers = copyFrom.disablePrayers;
		this.spellBook = SpellBook.find(copyFrom.spellBook.getId());
		this.prayerBook = PrayerBook.find(copyFrom.getPrayerBook().getId());
		this.spawnSkills = copyFrom.spawnSkills;
		this.spawnGear = copyFrom.spawnGear;
		this.presetType = copyFrom.presetType;
		this.disableProtectPrayers = copyFrom.disableProtectPrayers;
	}
	
	public SpellBook getSpellBook() {
		return spellBook;
	}

	public void setSpellBook(SpellBook spellBook) {
		this.spellBook = spellBook;
	}

	public PrayerBook getPrayerBook() {
		return prayerBook;
	}

	public void setPrayerBook(PrayerBook prayerBook) {
		this.prayerBook = prayerBook;
	}

	public int[] getRestockItems() {
		return restockItems;
	}

	public void setRestockItems(int[] restockItems) {
		this.restockItems = restockItems;
	}

	public boolean isSafeDeath() {
		return safeDeath;
	}

	public void setSafeDeath(boolean safeDeath) {
		this.safeDeath = safeDeath;
	}

	public boolean isSpawnGear() {
		return spawnGear;
	}

	public void setSpawnGear(boolean spawnGear) {
		this.spawnGear = spawnGear;
	}

	public Set<Integer> getBannedItems() {
		return bannedItems;
	}

	public void setBannedItems(Set<Integer> bannedItems) {
		this.bannedItems = bannedItems;
	}

	public boolean isDisablePrayers() {
		return disablePrayers;
	}

	public void setDisablePrayers(boolean disablePrayers) {
		this.disablePrayers = disablePrayers;
	}
	
	public boolean isDisableProtectPrayers() {
		return disableProtectPrayers;
	}

	public void setDisableProtectPrayers(boolean disableProtectPrayers) {
		this.disableProtectPrayers = disableProtectPrayers;
	}
	
	public boolean isDisableMembersPrayers() {
		return disableMembersPrayers;
	}

	public void setDisableMembersPrayers(boolean disableMembersPrayers) {
		this.disableMembersPrayers = disableMembersPrayers;
	}

	public boolean isSpawnSkills() {
		return spawnSkills;
	}

	public void setSpawnSkills(boolean spawnSkills) {
		this.spawnSkills = spawnSkills;
	}

	public int getLowestCmbLevel() {
		return lowestCmbLevel;
	}

	public void setLowestCmbLevel(int lowestCmbLevel) {
		this.lowestCmbLevel = lowestCmbLevel;
	}

	public int getHighestCmbLevel() {
		return highestCmbLevel;
	}

	public void setHighestCmbLevel(int highestCmbLevel) {
		this.highestCmbLevel = highestCmbLevel;
	}

	public Presets getPresetType() {
		return presetType;
	}

	public void setPresetType(Presets type) {
		this.presetType = type;
	}

	public static boolean checkPrayers(Player p, int i, boolean checkCurses) {
		if (p.tournSettings != null) {
			if (p.tournSettings.isDisableMembersPrayers() && (checkCurses || (!checkCurses && Prayers.values[i].isMembers()))) {
				p.getPacketSender().setConfig(checkCurses ? Curses.values[i].getConfigCode() : Prayers.values[i].getConfigCode(), 0);
				p.sendMessage("Members only prayers have are disabled in this tournament.");
				return false;
			}
			if (p.tournSettings.isDisablePrayers()) {
				p.getPacketSender().setConfig(checkCurses ? Curses.values[i].getConfigCode() : Prayers.values[i].getConfigCode(), 0);
				p.sendMessage("Prayers have been disabled.");
				return false;
			}
			if (p.tournSettings.isDisableProtectPrayers()) {
				if ((checkCurses && (i == Curses.DEFLECT_MELEE.ordinal() || i == Curses.DEFLECT_MISSILES.ordinal() || i == Curses.DEFLECT_MAGIC.ordinal()))
						|| (!checkCurses && (i == Prayers.PROTECT_FROM_MELEE.ordinal() || i == Prayers.PROTECT_FROM_MISSILES.ordinal() || i == Prayers.PROTECT_FROM_MAGIC.ordinal()))) {
					p.getPacketSender().setConfig(checkCurses ? Curses.values[i].getConfigCode() : Prayers.values[i].getConfigCode(), 0);
					p.sendMessage("Combat Protection Prayers are disabled.");
					return false;
				}
			}
		}
		return true;
	}

	public boolean isOfficialTournament() {
		return officialTournament;
	}

	public void setOfficialTournament(boolean officialTournament) {
		this.officialTournament = officialTournament;
	}
}
