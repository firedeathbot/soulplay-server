package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor16 {

	ROOM1(9996, 9095, 0),
	ROOM2(9996, 9105, 0),
	ROOM3(10006, 9095, 0),
	ROOM4(10006, 9105, 0),
	
	ROOM5(10025, 9124, 0),
	ROOM6(10025, 9134, 0),
	ROOM7(10035, 9124, 0),
	ROOM8(10035, 9134, 0),
	
	ROOM9(10055, 9124, 0),
	ROOM10(10055, 9134, 0),
	ROOM11(10065, 9124, 0),
	ROOM12(10065, 9134, 0),
	
	ROOM13(10084, 9095, 0),
	ROOM14(10084, 9105, 0),
	ROOM15(10094, 9095, 0),
	ROOM16(10094, 9105, 0),
	
	;
	
	private final Location loc;
	
	private Floor16(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor16[] values = values();
	
}
