package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor2 {
	
	ROOM1(10040, 9089, 0),
	ROOM2(10048, 9089, 0),
	
	;
	
	private final Location loc;
	
	private Floor2(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor2[] values = values();
	
}
