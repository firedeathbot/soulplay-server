package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor1 {

	ROOM1(10012, 8988, 0);
	
	private final Location loc;
	
	private Floor1(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor1[] values = values();
}
