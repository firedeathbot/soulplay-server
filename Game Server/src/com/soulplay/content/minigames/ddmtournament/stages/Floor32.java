package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor32 {

	ROOM1(9987, 9100, 1),
	ROOM2(9995, 9093, 1),
	ROOM3(9995, 9100, 1),
	ROOM4(9995, 9107, 1),

	ROOM5(10007, 9093, 1),
	ROOM6(10007, 9100, 1),
	ROOM7(10007, 9107, 1),
	ROOM8(10015, 9100, 1),
	
	ROOM9(10030, 9115, 1),
	ROOM10(10023, 9123, 1),
	ROOM11(10030, 9123, 1),
	ROOM12(10037, 9123, 1),
	

	ROOM13(10023, 9135, 1),
	ROOM14(10030, 9135, 1),
	ROOM15(10037, 9135, 1),
	ROOM16(10030, 9143, 1),

	ROOM17(10060, 9115, 1),
	ROOM18(10053, 9123, 1),
	ROOM19(10060, 9123, 1),
	ROOM20(10067, 9123, 1),
	

	ROOM21(10053, 9135, 1),
	ROOM22(10060, 9135, 1),
	ROOM23(10067, 9135, 1),
	ROOM24(10060, 9143, 1),
	
	ROOM25(10075, 9100, 1),
	ROOM26(10083, 9093, 1),
	ROOM27(10083, 9100, 1),
	ROOM28(10083, 9107, 1),

	ROOM29(10095, 9093, 1),
	ROOM30(10095, 9100, 1),
	ROOM31(10095, 9107, 1),
	ROOM32(10103, 9100, 1),
	
	;
	
	private final Location loc;
	
	private Floor32(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor32[] values = values();
	
}
