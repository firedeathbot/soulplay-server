package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor8 {

	ROOM1(9992, 9127, 0),
	ROOM2(10002, 9117, 0),
	ROOM3(10002, 9137, 0),
	ROOM4(10012, 9127, 0),

	ROOM5(10077, 9127, 0),
	ROOM6(10087, 9117, 0),
	ROOM7(10087, 9137, 0),
	ROOM8(10097, 9127, 0),
	
	;
	
	private final Location loc;
	
	private Floor8(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor8[] values = values();
	
}
