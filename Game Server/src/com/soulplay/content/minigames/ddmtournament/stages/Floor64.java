package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor64 {

	ROOM1(9993, 9092, 2),
	ROOM2(9998, 9092, 2),
	ROOM3(9993, 9097, 2),
	ROOM4(9998, 9097, 2),
	
	ROOM5(9993, 9104, 2),
	ROOM6(9998, 9104, 2),
	ROOM7(9993, 9109, 2),
	ROOM8(9998, 9109, 2),
	
	ROOM9(10005, 9092, 2),
	ROOM10(10005, 9097, 2),
	ROOM11(10010, 9092, 2),
	ROOM12(10010, 9097, 2),

	ROOM13(10005, 9104, 2),
	ROOM14(10005, 9109, 2),
	ROOM15(10010, 9104, 2),
	ROOM16(10010, 9109, 2),
	
	ROOM17(10022, 9121, 2),
	ROOM18(10022, 9126, 2),
	ROOM19(10027, 9121, 2),
	ROOM20(10027, 9126, 2),
	
	ROOM21(10022, 9133, 2),
	ROOM22(10022, 9138, 2),
	ROOM23(10027, 9133, 2),
	ROOM24(10027, 9138, 2),
	
	ROOM25(10034, 9121, 2),
	ROOM26(10034, 9126, 2),
	ROOM27(10039, 9121, 2),
	ROOM28(10039, 9126, 2),
	
	ROOM29(10034, 9133, 2),
	ROOM30(10034, 9138, 2),
	ROOM31(10039, 9133, 2),
	ROOM32(10039, 9138, 2),
	
	ROOM33(10052, 9121, 2),
	ROOM34(10052, 9126, 2),
	ROOM35(10057, 9121, 2),
	ROOM36(10057, 9126, 2),
	
	ROOM37(10052, 9133, 2),
	ROOM38(10052, 9138, 2),
	ROOM39(10057, 9133, 2),
	ROOM40(10057, 9138, 2),
	
	ROOM41(10064, 9121, 2),
	ROOM42(10064, 9126, 2),
	ROOM43(10069, 9121, 2),
	ROOM44(10069, 9126, 2),
	
	ROOM45(10064, 9133, 2),
	ROOM46(10064, 9138, 2),
	ROOM47(10069, 9133, 2),
	ROOM48(10069, 9138, 2),
	
	ROOM49(10081, 9092, 2),
	ROOM50(10081, 9097, 2),
	ROOM51(10086, 9092, 2),
	ROOM52(10086, 9097, 2),
	
	ROOM53(10081, 9104, 2),
	ROOM54(10081, 9109, 2),
	ROOM55(10086, 9104, 2),
	ROOM56(10086, 9109, 2),
	
	ROOM57(10093, 9092, 2),
	ROOM58(10093, 9097, 2),
	ROOM59(10098, 9092, 2),
	ROOM60(10098, 9097, 2),
	
	ROOM61(10093, 9104, 2),
	ROOM62(10093, 9109, 2),
	ROOM63(10098, 9104, 2),
	ROOM64(10098, 9109, 2)
	
	;
	
	private final Location loc;
	
	private Floor64(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor64[] values = values();
	
}
