package com.soulplay.content.minigames.ddmtournament.stages;

import com.soulplay.game.world.entity.Location;

public enum Floor4 {

	ROOM1(10028, 9089, 0),
	ROOM2(10039, 9100, 0),
	ROOM3(10049, 9100, 0),
	ROOM4(10060, 9089, 0),
	
	;
	
	private final Location loc;
	
	private Floor4(int x, int y, int z) {
		this.loc = Location.create(x, y, z);
	}

	public Location getLoc() {
		return loc;
	}
	
	public static final Floor4[] values = values();
	
}
