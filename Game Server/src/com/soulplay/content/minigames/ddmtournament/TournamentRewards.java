package com.soulplay.content.minigames.ddmtournament;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.soulplay.game.LocationConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

public class TournamentRewards {

	private static final String WINNER_MSG = "%s has won the %s PK Tournament!";
	
	public static void rewardParticipation(Player p, List<Item> list, boolean movePlayer) {

		if (p != null) {
			p.sendMessage("Thank you for participating in the tournament!");
			
			if (movePlayer)
				p.getPA().movePlayer(LocationConstants.EDGE);

			//delay reward giving to next tick.
			if (!list.isEmpty()) {
				
				final List<Item> rewardsF = new ArrayList<>(list);
			
				CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {

					@Override
					public void stop() {

					}

					@Override
					public void execute(CycleEventContainer container) {
						
						if (p.isDead()) {
							return;
						}
						
						container.stop();

						if (p.isActive) {
							for (int i = 0, len = rewardsF.size(); i < len; i++) {
								Item reward = rewardsF.get(i);
								p.getItems().addOrBankItem(reward.getId(), reward.getAmount());
								p.sendMessage("Your participation reward is " + Misc.formatNumbersWithCommas(reward.getAmount()) + " " + ItemProjectInsanity.getItemName(reward.getId()));
							}
						}

					}
				}, 1);
				
			}
			
		}
		
	}
	
	public static void rewardFirstPlaceWinner(Tournament tourn, Competitor comp) {
		PlayerHandler.broadcastMessageLocalWorld(String.format(WINNER_MSG, comp.getDisplayName(), tourn.getSettings().getPresetType().getPrettyName()));

		Player p = PlayerHandler.getPlayerByMID(comp.getMysqlId());

		if (p != null) {
			p.sendMessage("You have won the PK Tournament! Your reward will be in your bank.");
			p.getPA().movePlayer(LocationConstants.EDGE);
			p.getExtraHiscores().incPkTournamentWins();

			//delay reward giving to next tick.
			if (!tourn.instancedWinnerRewards.isEmpty()) {
				final List<Item> rewardsF = new ArrayList<>(tourn.instancedWinnerRewards);
				tourn.instancedWinnerRewards.clear();
				CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {

					@Override
					public void stop() {

					}

					@Override
					public void execute(CycleEventContainer container) {
						container.stop();

						if (p.isActive) {
							for (int i = 0, len = rewardsF.size(); i < len; i++) {
								Item reward = rewardsF.get(i);
								p.getItems().addOrBankItem(reward.getId(), reward.getAmount());
								p.sendMessage("You have won " + ItemProjectInsanity.getItemName(reward.getId()));
							}
						}

					}
				}, 1);
			}
		}
	}
	
	private static final String UPLOAD_QUERY = "INSERT INTO zaryte.pk_tournament_winners (player_id, amount_won, type, timestamp) VALUES(?, ?, ?, ?)";
	
	public static void uploadWinnerToWebsite(long sqlId, int amountWon, String tournamentType) {
		
		final Timestamp time = Misc.getESTTimestamp();
		
		TaskExecutor.executeWebsiteSQL(new Runnable() {
			
			@Override
			public void run() {

				try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
						PreparedStatement ps = connection.prepareStatement(UPLOAD_QUERY);) {
					
					ps.setLong(1, sqlId);
					ps.setInt(2, amountWon);
					ps.setString(3, tournamentType);
					ps.setTimestamp(4, time);
					
					ps.executeUpdate();

				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		
	}

}
