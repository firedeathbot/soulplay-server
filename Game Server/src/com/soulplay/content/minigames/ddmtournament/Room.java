package com.soulplay.content.minigames.ddmtournament;

import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class Room {

	private Competitor competitor1;
	private Competitor competitor2;
	
	private boolean isActive;
	
	private final Location corner;
	
	private TickTimer stopWatch = new TickTimer();
	
	private boolean roomFinished;
	
	private boolean canFight;
	
	private boolean fightCountdownStarted;
	
	private boolean smitePlayers;
	
	private TickTimer fightCountdown = new TickTimer();
	
	public final int stage;
	
	public Room(Location corner, int stage) {
		this.corner = corner.copyNew();
		this.stage = stage;
	}
	
	public boolean addCompetitor(Competitor comp) {
		if (this.competitor1 == null) {
			this.competitor1 = comp;
			Player p = PlayerHandler.getPlayerByMID(comp.getMysqlId());
			if (p != null && p.isActive) {
				movePlayerIntoRoom(p);
			}
			isActive = true;
			return true;
		}
		if (this.competitor2 == null) {
			this.competitor2 = comp;
			Player p = PlayerHandler.getPlayerByMID(comp.getMysqlId());
			if (p != null && p.isActive) {
				movePlayerIntoRoom(p);
			}
			isActive = true;
			return true;
		}
		return false;
	}
	
	public void movePlayerIntoRoom(Player p) {
		
		//p.tournamentInstance is null sometimes, so we will just cheaphax by calling Tournament.onPlayerLogin()
		if (p.tournamentInstance == null) {
			Tournament.onPlayerLogin(p);
			return;
		}
		
		
		p.getPA().movePlayer(corner.getX()+Misc.random2(3), corner.getY()+Misc.random2(3), corner.getZ());
		p.getPacketSender().showOption(3, 0, "Attack");
		p.tournamentRoom = this;
		CombatPrayer.resetPrayers(p);
		p.getSkills().setPrayerPointsToMax();
		p.getSkills().restore();
		p.restoreSpecialAttackFull();
		if (!p.tournamentInstance.onFirstStage(stage)) {
			Tournament.restockFoodPots(p);
		}
		p.tournSettings = p.tournamentInstance.getSettings();
	}
	
	public void removePlayerFromRoomReference(Player p) {
		if (competitor1 != null && competitor1.getMysqlId() == p.mySQLIndex) {
			competitor1 = null;
			return;
		}
		if (competitor2 != null && competitor2.getMysqlId() == p.mySQLIndex) {
			competitor2 = null;
			return;
		}
	}
	
	public Competitor getCompetitor1() {
		return competitor1;
	}
	public void setCompetitor1(Competitor competitor1) {
		this.competitor1 = competitor1;
	}
	public Competitor getCompetitor2() {
		return competitor2;
	}
	public void setCompetitor2(Competitor competitor2) {
		this.competitor2 = competitor2;
	}
	
	public boolean isFull() {
		return competitor1 != null && competitor2 != null;
	}
	
	public boolean isEmpty() {
		return competitor1 == null && competitor2 == null;
	}

	public Location getCorner() {
		return corner;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
		if (!isActive) {
			this.competitor1 = null;
			this.competitor2 = null;
		}
	}

	public boolean fightStarted() {
		return canFight;
	}

	public void setCanFight(boolean canFight) {
		this.canFight = canFight;
	}
	
	public boolean isRoomFinished() {
		return roomFinished;
	}

	public void setPlayerDied() {
		stopWatch.startTimer(50);
		roomFinished = true;
	}
	
	public boolean movePlayerNow() {
		return stopWatch.complete();
	}
	
	public boolean fightCountDownStarted() {
		return fightCountdownStarted;
	}
	
	public void startFightCountDown() {
		fightCountdownStarted = true;
		fightCountdown.startTimer(70);
	}
	
	public int getFightCountdown() {
		return fightCountdown.getRemainingTicks();
	}

	public boolean smitePlayers() {
		return smitePlayers;
	}

	public void setSmitePlayers(boolean startSmite) {
		this.smitePlayers = startSmite;
	}
}
