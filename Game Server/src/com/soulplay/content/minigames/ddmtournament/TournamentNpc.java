package com.soulplay.content.minigames.ddmtournament;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;

public class TournamentNpc {
	
	public static final int NPC_ID = 1990;
	
	public static final Location LOCATION = Location.create(3084, 3496, 0);
	
	public static NPC npc;
	
	public static void talkToNpc(Player p) {
		p.getDialogueBuilder()
		.sendNpcChat(NPC_ID, "Would you like to join the tournament?")
		.sendOption("Yes", ()-> {
			Tournament.requestToJoinLobby(p);
		}, "Not right now.", ()-> {
			p.getDialogueBuilder().close();
		})
		.execute();
	}
	
	public static void spawnNpc() {
		if (npc != null && npc.isActive) {
			return;
		}
		
		npc = NPCHandler.spawnNpc2(NPC_ID, LOCATION.getX(), LOCATION.getY(), LOCATION.getZ(), 0);
	}
	
	public static void despawnNpc() {
		if (npc != null && npc.isActive) {
			npc.removeNpcSafe(true);
			npc = null;
		}
	}
}
