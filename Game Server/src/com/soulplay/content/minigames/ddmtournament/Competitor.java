package com.soulplay.content.minigames.ddmtournament;

public class Competitor {

	private final long mysqlId;
	private final String displayName;
	
	public Competitor(long mysqlId, String name) {
		this.mysqlId = mysqlId;
		this.displayName = name;
	}

	public long getMysqlId() {
		return mysqlId;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Competitor) {
			Competitor comp2 = (Competitor) obj;
			return comp2.getMysqlId() == getMysqlId();
			
		}
		return false;
	}
	
}
