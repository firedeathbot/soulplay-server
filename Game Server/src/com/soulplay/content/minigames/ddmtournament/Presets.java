package com.soulplay.content.minigames.ddmtournament;

import com.soulplay.content.items.RunePouch;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.util.Misc;

public enum Presets {
	
	BOXING(0, "Boxing") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);
			
			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 50);
				p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
			}
			
			autoEquip(p, AutoSpawnPresets.BOXING);
		}
		
		@Override
		public void setSettings() {
			super.setSettings();
			
			Tournament.pre_settings.setBoxingRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setSpawnSkills(true);
			Tournament.pre_settings.setDisableProtectPrayers(true);
		}
	},
	DH(1, "DH") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);
			
			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
			}
			
			autoEquip(p, AutoSpawnPresets.DH);
		}
		
		@Override
		public void setSettings() {
			super.setSettings();
			
			for (int i = 0, l = SpawnTablePreset.BARROWS.getItems().length; i < l; i++) {
				Item item = SpawnTablePreset.BARROWS.getItems()[i];
				SpawnTable.getSpawnTableItems().add(item);
			}
			Tournament.pre_settings.setDefaultRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setSpellBook(SpellBook.LUNARS);
			Tournament.pre_settings.setPrayerBook(PrayerBook.CURSES);
			Tournament.pre_settings.setSpawnSkills(true);
			Tournament.pre_settings.setDisableProtectPrayers(true);
		}
	},
	F2P(2, "F2P") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);
			
			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 70);
		    	p.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 52);
		    	p.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
			}
			
			autoEquip(p, AutoSpawnPresets.F2P);
		}
		
		@Override
		public void setSettings() {
			super.setSettings();
			for (int i = 0, l = SpawnTablePreset.F2P.getItems().length; i < l; i++) {
				Item item = SpawnTablePreset.F2P.getItems()[i];
				SpawnTable.getSpawnTableItems().add(item);
			}
			Tournament.pre_settings.setF2PRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setPrayerBook(PrayerBook.NORMAL);
			Tournament.pre_settings.setSpawnSkills(true);
			Tournament.pre_settings.setDisableProtectPrayers(true);
			Tournament.pre_settings.setDisableMembersPrayers(true);
		}
	},
	NH_MAX(3, "Max NH") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);
			
			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
			}
			
			autoEquip(p, AutoSpawnPresets.MAX_NH);
		}
		
		@Override
		public void setSettings() {
			super.setSettings();
			
			for (int i = 0, l = SpawnTablePreset.NH.getItems().length; i < l; i++) {
				Item item = SpawnTablePreset.NH.getItems()[i];
				SpawnTable.getSpawnTableItems().add(item);
			}
			Tournament.pre_settings.setNhRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setDisableProtectPrayers(false);
			Tournament.pre_settings.setSpellBook(SpellBook.ANCIENT);
			Tournament.pre_settings.setPrayerBook(PrayerBook.CURSES);
			Tournament.pre_settings.setSpawnSkills(true);
		}
	},
	PURE_NH(4, "Pure NH") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);

			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 75);
		    	p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 1);
		    	p.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 52);
		    	p.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
		    	p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
				
			}
			
			autoEquip(p, AutoSpawnPresets.PURE_NH);
		}
		
		@Override
		public void setSettings() {
			super.setSettings();
			
			for (int i = 0, l = SpawnTablePreset.PURE_NH.getItems().length; i < l; i++) {
				Item item = SpawnTablePreset.PURE_NH.getItems()[i];
				SpawnTable.getSpawnTableItems().add(item);
			}
			
			Tournament.pre_settings.setNhRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setDisableProtectPrayers(false);
			Tournament.pre_settings.setSpellBook(SpellBook.ANCIENT);
			Tournament.pre_settings.setPrayerBook(PrayerBook.NORMAL);
			Tournament.pre_settings.setSpawnSkills(true);
		}
	},
	ZERKER1(5, "Zerker") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);
			
			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 75);
				p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 45);
				p.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
				
			}
			
			autoEquip(p, AutoSpawnPresets.ZERKER);
		}
		
		@Override
		public void setSettings() {
			super.setSettings();

			for (int i = 0, l = SpawnTablePreset.ZERKER1.getItems().length; i < l; i++) {
				Item item = SpawnTablePreset.ZERKER1.getItems()[i];
				SpawnTable.getSpawnTableItems().add(item);
			}
			Tournament.pre_settings.setDefaultRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setSpellBook(SpellBook.LUNARS);
			Tournament.pre_settings.setPrayerBook(PrayerBook.NORMAL);
			Tournament.pre_settings.setSpawnSkills(true);
			Tournament.pre_settings.setDisableProtectPrayers(true);
		}
	},
	EMPTY(5, "Empty") {
		@Override
		public void setPreset(Player p) {
			super.setPreset(p);
			
			if (Tournament.pre_settings.isSpawnSkills()) {
				p.setSpawnedSkills(new Skills(p));
				p.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
				p.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
				p.calcCombat();
				
			}
			
		}
		
		@Override
		public void setSettings() {
			super.setSettings();

			Tournament.pre_settings.setDefaultRestockItems();
			Tournament.pre_settings.setSpawnGear(true);
			Tournament.pre_settings.setSpellBook(SpellBook.LUNARS);
			Tournament.pre_settings.setPrayerBook(PrayerBook.NORMAL);
			Tournament.pre_settings.setSpawnSkills(true);
			Tournament.pre_settings.setDisableProtectPrayers(true);
		}
	}
	
	;
	
	private final int index;
	
	private final String prettyName;
	
	private Presets(int id, String name) {
		this.index = id;
		this.prettyName = name;
	}

	public int getIndex() {
		return index;
	}
	
	public String getPrettyName() {
		return prettyName;
	}
	
	public void setPreset(Player p) {
		p.playerPrayerBook = Tournament.pre_settings.getPrayerBook();
		p.updatePrayerBook();
		p.getPA().changeSpellBook(Tournament.pre_settings.getSpellBook());
	}
	
	public void setSettings() {
		SpawnTable.getSpawnTableItems().clear();

		Tournament.pre_settings.setPresetType(this);
	}
	
	public void autoEquip(Player p, AutoSpawnPresets set) {
		
		if (Tournament.pre_settings.isSpawnGear()) {
			
			for (int i = 0, len = p.playerEquipment.length; i < len; i++) {
				switch (i) {
				case PlayerConstants.playerHat:
					if (set.getHead() > 0) {
						p.playerEquipment[i] = set.getHead();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerCape:
					if (set.getCape() > 0) {
						p.playerEquipment[i] = set.getCape();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerAmulet:
					if (set.getNecklace() > 0) {
						p.playerEquipment[i] = set.getNecklace();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerWeapon:
					if (set.getWeapon() > 0) {
						p.playerEquipment[i] = set.getWeapon();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerChest:
					if (set.getBody() > 0) {
						p.playerEquipment[i] = set.getBody();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerShield:
					if (set.getShield() > 0) {
						p.playerEquipment[i] = set.getShield();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerLegs:
					if (set.getLegs() > 0) {
						p.playerEquipment[i] = set.getLegs();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerHands:
					if (set.getGloves() > 0) {
						p.playerEquipment[i] = set.getGloves();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerFeet:
					if (set.getBoots() > 0) {
						p.playerEquipment[i] = set.getBoots();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerRing:
					if (set.getRing() > 0) {
						p.playerEquipment[i] = set.getRing();
						p.playerEquipmentN[i] = 1;	
					}
					break;
				case PlayerConstants.playerArrows:
					if (set.getArrows() != null) {
						p.playerEquipment[i] = set.getArrows().getId();
						p.playerEquipmentN[i] = set.getArrows().getAmount();
					}
					break;

				}
				
			}
			
			for (int i = 0, len = set.getInv().length; i < len; i++) {
				Item item = set.getInv()[i];
				p.getItems().addItem(item.getId(), item.getAmount());
			}
			
			p.getUpdateFlags().add(UpdateFlag.APPEARANCE);
			for (int i = 0, len = p.playerEquipment.length; i < len; i++)
				p.setUpdateEquipment(true, i);
			p.getItems().refreshWeapon();
			p.setUpdateInvInterface(3214);
			
			if (set.getPouch() != null) {
				p.runePouchItems[0] = set.getPouch().getRune1().getId()+1;
				p.runePouchItemsN[0] = set.getPouch().getRune1().getAmount();
				p.runePouchItems[1] = set.getPouch().getRune2().getId()+1;
				p.runePouchItemsN[1] = set.getPouch().getRune2().getAmount();
				p.runePouchItems[2] = set.getPouch().getRune3().getId()+1;
				p.runePouchItemsN[2] = set.getPouch().getRune3().getAmount();
				RunePouch.updateRunes(p);
			}
			
		}
		
	}
	
	private static final Presets[] PK_TOURNAMENT = { DH, DH, DH, F2P, NH_MAX, NH_MAX, NH_MAX, PURE_NH, PURE_NH, PURE_NH, ZERKER1, ZERKER1, BOXING };
	
	public static Presets getPkTournyPresetRandom() {
		return PK_TOURNAMENT[Misc.random(PK_TOURNAMENT.length-1)];
	}

}
