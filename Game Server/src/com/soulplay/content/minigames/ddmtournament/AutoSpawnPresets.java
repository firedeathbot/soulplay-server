package com.soulplay.content.minigames.ddmtournament;

import com.soulplay.game.model.item.Item;

public enum AutoSpawnPresets {
	
	DH(RunePouchPreset.VENG, null, 4716, 4720, 4722, 11732, 7462, 4718, -1, 6570, 6585, 2550, new Item(30047), new Item(385, 2), new Item(12006), new Item(20072), new Item(5698), new Item(6685, 2), new Item(3024, 3), new Item(9739), new Item(3144, 5), new Item(385, 11)),

	MAX_NH(RunePouchPreset.BARRAGE, new Item (9244, 500), 30086, 30085, 30084, 21793, 7462, 18355, 13738, 20767, 18335, 15220,
			new Item(11785), new Item(12006), new Item(20072), new Item(9739),
			new Item(30003), new Item(11720), new Item(11722), new Item(2444),
			new Item(391, 3), new Item(3040), 
			new Item(391), new Item(3144), new Item(3024), new Item(6685),
			new Item(391), new Item(3144), new Item(3024), new Item(6685), 
			new Item(391), new Item(11694), new Item(3024), new Item(6685), 
			new Item(391, 3), new Item(30047)),
	PURE_NH(RunePouchPreset.BARRAGE, new Item (9244, 500), 662, 6107, 6108, 3105, 11118, 15486, 3842, 2414, 6585, 15220,
			new Item(11785), new Item(10499), new Item(385, 2),
			new Item(2497), new Item(4587), new Item(385), new Item(2444),
			new Item(385, 3), new Item(9739), 
			new Item(385), new Item(3144), new Item(3024), new Item(6685),
			new Item(385), new Item(3144), new Item(3024), new Item(6685), 
			new Item(385), new Item(5698), new Item(3024), new Item(6685), 
			new Item(385, 3), new Item(30047)),
	ZERKER(RunePouchPreset.VENG, null, 3751, 10551, 1079, 3105, 7462, 12006, 8850, 23639, 30208, 15220,
		    new Item(385, 5), new Item(3144),
			new Item(385, 4), 
			new Item(385), new Item(9739),
			new Item(385), new Item(3144), new Item(3024), new Item(6685), 
			new Item(385, 1), new Item(3144), new Item(3024), new Item(6685), 
			new Item(385), new Item(11694), new Item(3024), new Item(6685), 
			new Item(385, 3), new Item(30047)),
	F2P(null, new Item(890, 200), 7394, 10686, 1099, 630, 1065, 853, -1, 1052, 1725, -1,
			new Item(1333), new Item(1319),
			new Item(373, 20)),
	BOXING(null, null, -1, -1, -1, -1, 7671, -1, -1, -1, -1, -1,
			new Item(7673))
	;
	
	private final RunePouchPreset pouch;
	
	private final int head, body, legs, boots, gloves, weapon, shield, cape, necklace, ring;
	
	private final Item arrows;

	private final Item[] inv;

	private AutoSpawnPresets(RunePouchPreset runePouch, Item arrows, int head, int body, int legs, int boots, int gloves, int weapon, int shield, int cape, int neck, int ring, Item... inv) {
		this.pouch = runePouch;
		this.head = head;
		this.body = body;
		this.legs = legs;
		this.boots = boots;
		this.gloves = gloves;
		this.weapon = weapon;
		this.shield = shield;
		this.cape = cape;
		this.necklace = neck;
		this.ring = ring;
		this.arrows = arrows;
		this.inv = inv;
	}
	
	public int getHead() {
		return head;
	}

	public int getBody() {
		return body;
	}

	public int getLegs() {
		return legs;
	}

	public int getBoots() {
		return boots;
	}

	public int getGloves() {
		return gloves;
	}

	public int getWeapon() {
		return weapon;
	}

	public int getShield() {
		return shield;
	}

	public int getCape() {
		return cape;
	}

	public int getNecklace() {
		return necklace;
	}

	public int getRing() {
		return ring;
	}

	public Item getArrows() {
		return arrows;
	}

	public Item[] getInv() {
		return inv;
	}

	public RunePouchPreset getPouch() {
		return pouch;
	}

}
