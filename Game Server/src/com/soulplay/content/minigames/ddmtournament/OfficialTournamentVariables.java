package com.soulplay.content.minigames.ddmtournament;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.sql.SqlHandler;

public class OfficialTournamentVariables {
	
	public static boolean updateSettings = false;

	//pk tournament shit
	public static int pkTournamentStartHour = 15; // 16 = 4 PM EST
	
	public static int pktFirstPlaceItemId = 30367;//osrs coin
	
	public static int pktFirstPlaceItemAmount = 25_000_000;
	
	public static int pktParticipationItemId = 995; // Gold coins
	
	public static int pktParticipationItemAmount = 20_000_000;
	
	
	//lms shit
	public static int lmsStartHour = 16; // 16 = 4 PM EST
	
	public static int lmsFirstPlaceItemId = 30367;//osrs coin
	
	public static int lmsFirstPlaceItemAmount = 25_000_000;
	
	public static int lmsParticipationItemId = 995; // Gold coins
	
	public static int lmsParticipationItemAmount = 20_000_000;
	
	private final static String SQL_QUERY = "SELECT * from `zaryte`.`tournament_reward`";
	
	public static void pullSettings() {
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				
				try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
						PreparedStatement ps = connection.prepareStatement(SQL_QUERY);
						ResultSet results = ps.executeQuery();) {
					
					while (results.next()) {
						
						int id = results.getInt("ID");
						int hour = results.getInt("hour");
						int winItemId = results.getInt("winner_item_id");
						int winItemAmount = results.getInt("winner_item_amount");
						int partItemId = results.getInt("participation_item_id");
						int partItemAmount = results.getInt("participation_item_amount");
						
						
						switch (id) {
						
						case 0: // pk tournament
							pkTournamentStartHour = hour;
							pktFirstPlaceItemId = winItemId;
							pktFirstPlaceItemAmount = winItemAmount;
							pktParticipationItemId = partItemId;
							pktParticipationItemAmount = partItemAmount;
							break;
							
						case 1: // mass lms event
							lmsStartHour = hour;
							lmsFirstPlaceItemId = winItemId;
							lmsFirstPlaceItemAmount = winItemAmount;
							lmsParticipationItemId = partItemId;
							lmsParticipationItemAmount = partItemAmount;
							break;
						
						}
						
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});

	}
	
}
