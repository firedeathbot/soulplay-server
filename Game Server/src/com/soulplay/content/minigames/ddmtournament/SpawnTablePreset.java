package com.soulplay.content.minigames.ddmtournament;

import com.soulplay.game.model.item.Item;

public enum SpawnTablePreset {

	BARROWS(new Item(4718, 1), new Item(4716, 1), new Item(4720, 1), new Item(4722, 1), new Item(6570, 1), new Item(20072, 1), new Item(7462, 1), new Item(11732, 1), new Item(6585, 1), new Item(12006, 1), new Item(5698, 1), new Item(14484, 1), new Item(11698), new Item(11694), new Item(6685, 2), new Item(3024, 3), new Item(9739, 1), new Item(385, 28), new Item(3144, 5), new Item(560, 200), new Item(557, 1000), new Item(9075, 400), new Item(2550, 3)),
	F2P(new Item(1319, 1), new Item(1333, 1), new Item(1373, 1), new Item(853, 1), new Item(890, 200), new Item(1725, 1), new Item(1065, 1), new Item(1061, 1), new Item(1153, 1), new Item(544), new Item(542), new Item(113, 1), new Item(2297, 20), new Item(373, 28)),
	NH(new Item(30047, 1), new Item(555, 6000), new Item(560, 4000), new Item(565, 2000), new Item(1712, 1), new Item(2579, 1), new Item(2503, 1), new Item(2497, 1), new Item(1079, 1), new Item(10828, 1), new Item(7462, 1), new Item(2412, 1), new Item(10499, 1), new Item(9185, 1), new Item(9244, 150), new Item(4675, 1), new Item(13736, 1), new Item(4151, 1), new Item(5698, 1), new Item(12603, 1), new Item(20072, 1), new Item(4091, 1), new Item(4093, 1), new Item(2444, 1), new Item(6685, 4), new Item(3024, 10), new Item(3040, 1), new Item(9739, 2), new Item(391, 28), new Item(299, 100)),
	PURE_NH(new Item(385, 28), new Item(3144, 2), new Item(3024, 3), new Item(6685, 3), new Item(9739, 1), new Item(2444, 1), new Item(18335, 1), new Item(7458, 1), new Item(14484, 1), new Item(299, 100)),
	ZERKER1(new Item(4587, 1), new Item(1215, 1), new Item(23639, 1), new Item(1725, 1), new Item(3751, 1), new Item(10551, 1), new Item(1079, 1), new Item(1093, 1), new Item(1201, 1), new Item(4131, 1), new Item(7462, 1), new Item(557, 200), new Item(9075, 80), new Item(560, 40), new Item(385, 28), new Item(3144, 4), new Item(6685, 4), new Item(3024, 2), new Item(2436, 1), new Item(2440, 1), new Item(4417, 3)),
	
	;
	
	private final Item[] items;
	
	private SpawnTablePreset(Item... items) {
		this.items = items;
	}

	public Item[] getItems() {
		return items;
	}
}
