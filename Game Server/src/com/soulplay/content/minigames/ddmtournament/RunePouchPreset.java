package com.soulplay.content.minigames.ddmtournament;

import com.soulplay.game.model.item.Item;

public enum RunePouchPreset {
	
	VENG(new Item(560, 1000), new Item(9075, 2000), new Item(557, 5000)),
	BARRAGE(new Item(565, 1000), new Item(560, 2000), new Item(555, 3000))
	
	;
	
	public Item getRune1() {
		return rune1;
	}

	public Item getRune2() {
		return rune2;
	}

	public Item getRune3() {
		return rune3;
	}

	private final Item rune1, rune2, rune3;
	
	private RunePouchPreset(Item item1, Item item2, Item item3) {
		this.rune1 = item1;
		this.rune2 = item2;
		this.rune3 = item3;
	}

}
