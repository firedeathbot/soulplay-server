package com.soulplay.content.minigames.ddmtournament;

import java.util.ArrayList;
import java.util.List;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;

public class SpawnTable {

	private static final String SHOP_NAME = "Spawn Table";

	private static final int SHOP_LIMIT = 40;
	
	private static final List<Item> spawnTableItems = new ArrayList<>(SHOP_LIMIT);
	
	public static final int SHOP_ID = 7777;

	public static List<Item> getSpawnTableItems() {
		return spawnTableItems;
	}

	public static void addItemToSpawnTable(Player p, Item item) {
		if (!p.getZones().isInDmmLobby() && canEditShop(p)) {
			if (spawnTableItems.size() >= SHOP_LIMIT) {
				p.sendMessage("Spawn Shop is full.");
				return;
			}
			spawnTableItems.add(item);
			refreshTable(p);
		} else if (p.getZones().isInDmmLobby()) {
			p.getItems().deleteItem(item);
			refreshInventory(p);
		}
	}
	
	private static final String ITEM_LIMIT_MSG = "You can only spawn %d of this item.";
	
	public static void spawnItemFromSlot(Player p, int slot, int amount) {
		
		if (slot < 0 || slot >= spawnTableItems.size()) {
			return;
		}
		
		if (!p.getZones().isInDmmLobby()) {
			if (canEditShop(p)) { //remove item from spawn table
				spawnTableItems.remove(slot);
				refreshTable(p);
			} else
				p.getPA().closeAllWindows();
			return;
		}
		
		if (!Tournament.pre_settings.isSpawnGear()) {
			p.sendMessage("wrong");
			return;
		}
		
		Item item = spawnTableItems.get(slot);
		if (item == null) {
			p.sendMessage("Item doesn't exist.");
			return;
		}
		
		final int ownsItemCount = p.getItems().countItems(item.getId());
		
		if (ownsItemCount >= item.getAmount()) {
			p.sendMessage(String.format(ITEM_LIMIT_MSG, item.getAmount()));
			return;
		}
		
		if (ownsItemCount > 0) {
			amount = Math.max(0, item.getAmount() - ownsItemCount);
		}
		
		amount = Math.min(item.getAmount(), amount);
		
		if (amount == 0) {
			p.sendMessage("oh poop");
			return;
		}
		
		p.getItems().addItem(item.getId(), amount);
		refreshInventory(p);
	}
	
	public static void openTable(Player p) {
		final boolean canEditShop = canEditShop(p);
		if (!p.getZones().isInDmmLobby() && !canEditShop) {
			return;
		}
		
		if (!Tournament.pre_settings.isSpawnGear() && !canEditShop) {
			p.sendMessage("Can't spawn items this tournament.");
			return;
		}
		
		refreshTable(p);

		refreshInventory(p);
		p.getPacketSender().sendFrame248(3824, 3822);
		p.getPacketSender().sendFrame126(SHOP_NAME, 3901);
		p.myShopId = SHOP_ID;
		
	}
	
	private static boolean canEditShop(Player p) {
		return p.isMod();
	}
	
	private static void refreshInventory(Player p) {
		p.getItems().resetItems(3823);
	}
	
	private static void refreshTable(Player p) {
		p.getPacketSender().itemsOnInterface(3900, spawnTableItems, 1);
	}
}
