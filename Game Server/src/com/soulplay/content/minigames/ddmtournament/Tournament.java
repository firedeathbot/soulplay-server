package com.soulplay.content.minigames.ddmtournament;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.items.Food;
import com.soulplay.content.items.ItemIds;
import com.soulplay.content.items.Potions;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.clickitem.Potion;
import com.soulplay.content.minigames.MinigameRules;
import com.soulplay.content.minigames.ddmtournament.stages.Floor1;
import com.soulplay.content.minigames.ddmtournament.stages.Floor16;
import com.soulplay.content.minigames.ddmtournament.stages.Floor2;
import com.soulplay.content.minigames.ddmtournament.stages.Floor32;
import com.soulplay.content.minigames.ddmtournament.stages.Floor4;
import com.soulplay.content.minigames.ddmtournament.stages.Floor64;
import com.soulplay.content.minigames.ddmtournament.stages.Floor8;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.Misc;

public class Tournament {
	
	//TODO: if safe death is false and fight already started, need to kill player somehow, not allow to log out. if spawned gear, dc, if not, kill

	public static final int MAX_ROOMS = 64;
	
	public static final int MAX_PLAYERS = MAX_ROOMS * 2;
	
	private static final int START_COUNTDOWN = 100;//1_000; // 10 minutes to connect and fight.
	
	public static final int MAX_FIGHT_DURATION = 1_500; // 15 minutes
	
	public static final int SMITE_TICK = 1_000; // 10 minutes in, smite players at first stages.
	
	public static final int SMITE_TICK_WARN = SMITE_TICK+30;
	
	public static List<Player> lobbyPlayers = new ArrayList<>(MAX_PLAYERS);
	
	public static int lobby_id;
	
	public static Map<Integer, Tournament> tournInstances = new HashMap<>();
	
	public static Settings pre_settings;
	
	public static boolean lobbyOpen = false;
	
	public static Tournament lastInstance = null;
	
	private static final String BANNED_GEAR_MSG = "You cannot bring %s.";
	
	private static final String NEXT_FIGHT_MSG = "Next fight will start in about 30 seconds!";
	
	private Map<Integer, Competitor> competitors = new HashMap<>();
	
	private Multimap<Integer, Room> rooms = ArrayListMultimap.create();
	
	private boolean isActive;
	
	private int instanceId;
	
	private int stages = 0;
	
	private int stageStartedAt;
	
	private int timer = START_COUNTDOWN;
	
	private Settings instanceSettings = new Settings();
	
	public Settings getSettings() {
		return instanceSettings;
	}
	
	public static boolean OFFICIAL_ENABLED = true;
	
	private static final int[] AUTOMATED_REWARD = { 3062, 9946, 15422, 15423, 15425,
			15441, 15442, 15443, 15444, 18830, 19308, 19311, 19314, 19317,
			19320, 19370, 19323, 19372, 19368, 15018, 15019, 15020, 19354,
			19356, 19358, 19360, 15220, 15259, 13346, 13348, 13350, 13352,
			13354, 13355, 13360, 13358, /* 13655, */13362, 13366, 13370, 13340,
			13342, 13344, 20070, 20072, 4718, 4720, 4712, 4714, 4724, 4734,
			4736, 4738, 4749, 4753, 4757, 4759, 14484, 13899, 13902, 13742,
			13740, 13738, 11728, 11724, 11722, 11720, 11718, 11708, 11706,
			11704, 11702, 18333, 18334, 18335, 1050 };
	
	//rewards to add to the tournament instance
//	private static List<Item> rewardsToAdd = new ArrayList<>();
	
	public List<Item> instancedWinnerRewards = new ArrayList<>();
	
	public List<Item> instancedParticipationRewards = new ArrayList<>();
	
	private Object cycleIntance = new Object();
	
	private CycleEventContainer processContainer = null;
	
	public CycleEventContainer getContainer() {
		return processContainer;
	}
	
	static {
		resetSettings();
	}
	
	public static void resetSettings() {
		pre_settings = new Settings();
		pre_settings.createDefaults();
	}
	
	private boolean firstStage = true;
	
	private TickTimer stopWatch = new TickTimer();
	
	public void endGame() {
		timer = 0;
		stages = 0;
		isActive = false;
		kickAllPlayers();
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public static void toggleLobby() {
		Tournament.lobbyOpen = !Tournament.lobbyOpen;
		if (Tournament.lobbyOpen) {
			TournamentNpc.spawnNpc();
			lobby_id = Server.getTotalTicks();
			AnnouncementHandler.announce("Announcement: PKing Tournament is Open ::join");
		} else {
			TournamentNpc.despawnNpc();
			AnnouncementHandler.removeAnnouncement();
		}
	}
	
	public static void requestToJoinLobby(Player p) {
		if (p.inMinigame()) {
			p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "You must first exit the minigame.").executeIfNotActive();
			return;
		}
		if (p.wildLevel > 0) {
			p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "You cannot use this command in wild.").executeIfNotActive();
			return;
		}
		if (p.isInCombatDelay()) {
			p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "You cannot use this command during combat.").executeIfNotActive();
			return;
		}
		if (pre_settings.isSpawnGear() && !RunePouch.isEmpty(p)) {
			p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "Your rune pouch will be cleared if you join.", "If you want to keep your runes,", "empty your rune pouch that is in your bank/inventory.")
			.sendOption("Clear rune pouch?", "Clear my runepouch and join.", ()-> {
				addToLobby(p);
			}, "No.", ()-> {
				p.getDialogueBuilder().closeNew();
			}).executeIfNotActive();
			return;
		}
		
		addToLobby(p);
	}
	
	private static final String CMB_REQ_MSG = "Your combat level must be between %d - %d.";
	
	public static void addToLobby(Player p) {
		
		if (!Tournament.lobbyOpen) {
			p.sendMessage("The lobby is currently closed.");
			return;
		}
		
		if (!pre_settings.isSpawnSkills()) {
			if (p.combatLevel < pre_settings.getLowestCmbLevel() || p.combatLevel > pre_settings.getHighestCmbLevel()) {
				p.sendMessage(String.format(CMB_REQ_MSG, pre_settings.getLowestCmbLevel(), pre_settings.getHighestCmbLevel()));
				return;
			}
		}
		
		if (!p.getSpamTick().checkThenStart(1)) {
			return;
		}
		
		if (p.summoned != null) {
			p.sendMessage("You cannot bring your pet inside.");
			return;
		}
		
		if (!MinigameRules.canJoinMinigame(p))
			return;
		
		if (lobbyPlayers.size() >= MAX_PLAYERS) {
			p.sendMessage("Sorry tournament is full.");
			return;
		}
		
		if (pre_settings.isSpawnGear()) {
//			for (int k = 0; k < p.runePouchItems.length; k++) {
//				if (p.runePouchItems[k] > 0) {
//					p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "You must empty your rune pouch before you enter this", "PK Tournament.").execute();
//					return;
//				}
//			}
			for (int j = 0, length = p.playerItems.length; j < length; j++) {
				int id = p.playerItems[j] - 1;
				if (id != -1) {
					p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "You can't carry any items with you in the", "PK Tournament.").executeIfNotActive();
					return;
				}
			}
			for (int j = 0, length = p.playerEquipment.length; j < length; j++) {
				int id = p.playerEquipment[j];
				if (id != -1) {
					p.getDialogueBuilder().sendNpcChat(TournamentNpc.NPC_ID, "You are wearing items that cannot be taken into the", "PK Tournament.").executeIfNotActive();
					return;
				}
			}
		} else { //bring own gear
			
			boolean hasBannedGear = false;
			
			for (int j = 0, length = p.playerItems.length; j < length; j++) {
				int id = p.playerItems[j] - 1;
				if (id > 0 && pre_settings.getBannedItems().contains(id)) {
					p.sendMessage(String.format(BANNED_GEAR_MSG, ItemProjectInsanity.getItemName(id)));
					hasBannedGear = true;
				}
			}
			for (int j = 0, length = p.playerEquipment.length; j < length; j++) {
				int id = p.playerEquipment[j];
				if (id > 0 && pre_settings.getBannedItems().contains(id)) {
					p.sendMessage(String.format(BANNED_GEAR_MSG, ItemProjectInsanity.getItemName(id)));
					hasBannedGear = true;
				}
			}
			
			if (hasBannedGear)
				return;
			
		}
		
		boolean sameUUID = false;
		for (int i = 0, len = lobbyPlayers.size(); i < len; i++) {
			Player other = lobbyPlayers.get(i);
			
			if (other == null || other.disconnected || other.forceDisconnect) {
				continue;
			}
			
			if (other.UUID.equals(p.UUID) ) {
				sameUUID = true;
				break;
			}
		}
		
		if (sameUUID && Config.RUN_ON_DEDI) {
			p.sendMessage("Sorry, but you can't enter the game with multiple accounts.");
			return;
		}
		
		p.getPA().closeAllWindows();
		p.getPA().movePlayer(2730+Misc.random(1), 5511, 4);
		
		if (Tournament.pre_settings.isSpawnGear()) {
			CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() { //cheaphax this shit!

				@Override
				public void stop() {
				}

				@Override
				public void execute(CycleEventContainer container) {
					container.stop();
					p.getPacketSender().addObjectPacket(55220, 2728, 5516, 10, 0);
				}
			}, 1);
		}
		
		p.getPA().restorePlayer(true);
		p.getSkills().healToFull();
		lobbyPlayers.add(p);
		p.sendMessage("You have joined the PK Tournament Lobby.");
		

		updateDiscordStatus(p);
		updateLobbyCount(p);
		updateLobbyCount();
	}
	
	public static void removeFromLobby(Player p, boolean kickedAll) {
		if (Tournament.pre_settings.isSpawnGear()) {
			Tournament.clearAllItems(p);
		}
		if (!kickedAll) {
			lobbyPlayers.remove(p);
			updateLobbyCount();
		}
		p.resetSpawnedSkills();
		p.getPA().movePlayer(LocationConstants.EDGE);
		
		p.setDefaultDiscordStatus();
	}
	
	public static void kickAllFromLobby() {
		for (int i = 0, size = lobbyPlayers.size(); i < size; i++) {
			Player p = lobbyPlayers.get(i);
			if (p == null || p.disconnected || !p.getZones().isInDmmLobby()) continue;
			
			removeFromLobby(p, true);
		}
		lobbyPlayers.clear();
	}
	
	private static final String LOBBY_COUNT_STR = "Lobby Player Count: %d";
	
	private static void updateLobbyCount() {
		final int size = lobbyPlayers.size();
		final String str = String.format(LOBBY_COUNT_STR, size);
		for (int i = 0; i < size; i++) {
			Player p = lobbyPlayers.get(i);
			if (p == null || p.disconnected || !p.getZones().isInDmmLobby()) continue;

			updateLobbyCountInterface(p, str);
			updateDiscordStatus(p);
		}
	}
	
	private static void updateLobbyCount(Player p) {//cheaphax lobby count for entering lobby
		final int size = lobbyPlayers.size();
		final String str = String.format(LOBBY_COUNT_STR, size);
		updateLobbyCountInterface(p, str);
	}
	
	private static void updateLobbyTimer(Player p) {
		if (lobbyWaitTime > 0) {
			p.getPacketSender().sendFrame126("Starting in " + Misc.ticksToTimeShort(lobbyWaitTime), 6570);
		} else
			p.getPacketSender().sendFrame126("", 6570);
	}
	
	private static void updateLobbyCountInterface(Player p, String str) {
		updateLobbyTimer(p);
		p.getPacketSender().sendFrame126("", 6572);
		p.getPacketSender().sendFrame126(str, 6664);
	}
	
	private static void updateDiscordStatus(Player p) {
		if (lobbyWaitTime > 0) { // official pk tournament
			final int size = lobbyPlayers.size();
			p.getPacketSender().sendDiscordUpdate("Lobby", "Pk Tournament", size, MAX_PLAYERS, lobbyStartTimestamp, lobbyEndTimestamp);
		}
	}
	
	public static void startGame() {
		final int size = lobbyPlayers.size();
		if (size == 0 || size % 2 == 1) {
			PlayerHandler.alertStaff("Player count must be even number to start PK tournament.");
			return;
		}
		
		final Tournament t = new Tournament();
		
		lastInstance = t;
		
		t.instanceSettings.copy(pre_settings);
		
		t.isActive = true;
		
		t.instanceId = Server.getTotalTicks();
		
		tournInstances.put(t.instanceId, t);
		
		t.createRooms(size);
		
		Collections.shuffle(lobbyPlayers); //randomize players list
		
		t.fillRoomsWithPlayers(size);
		
		t.initInstance();
		
		lobbyPlayers.clear();
		
		if (lobbyOpen)
			toggleLobby();
		
		resetSettings();
	}
	
	private void initInstance() {
		
		processContainer = CycleEventHandler.getSingleton().addEvent(cycleIntance, new CycleEvent() {
			
			@Override
			public void stop() {
				isActive = false;
				tournInstances.remove(instanceId);
				processContainer = null;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				timer--;

				boolean fightCountdown = false;
				if (timer <= 0) {
					if (firstStage) {
						firstStage = false;
					} else {
						stages--;
					}
					timer = MAX_FIGHT_DURATION;
					fightCountdown = true;
				}
				
				if (stages < 0) {
					container.stop();
					return;
				}
				
				if (rooms.get(stages).size() == 0 && stopWatch.checkThenStart(10)) {
					timer = 1;
				}
				loopRooms(fightCountdown);
				
			}
		}, 1);
	}
	
	private List<Room> toRemove = new ArrayList<>();
	
	private void loopRooms(boolean startCountdown) {
		if (startCountdown)
			removeAllEmptyOnThisStage();//cheaphax to removing empty rooms that i filled in too much XD
		
		//create better iterator
		for (Room room : rooms.values()) {
			if (!room.isActive()) continue;
			Player p1 = null;
			Player p2 = null;
			if (room.getCompetitor1() != null) {
				p1 = PlayerHandler.getPlayerByMID(room.getCompetitor1().getMysqlId());
			}
			if (room.getCompetitor2() != null) {
				p2 = PlayerHandler.getPlayerByMID(room.getCompetitor2().getMysqlId());
			}
			
			if (room.fightStarted() && !room.isRoomFinished()) {
				
				checkPlayerDeathOrMIA(p1, p2, room, 1);
				checkPlayerDeathOrMIA(p2, p1, room, 2);
				
			}
			if (startCountdown) {
				room.startFightCountDown();
			}
			
			if (p1 != null && p1.isActive) {
				processPlayer(p1, room, startCountdown ? NEXT_FIGHT_MSG : null);
			}
			if (p2 != null && p2.isActive) {
				processPlayer(p2, room, startCountdown ? NEXT_FIGHT_MSG : null);
			}
			
			if (room.isRoomFinished() && room.movePlayerNow()) {
				if (room.getCompetitor1() != null) {
					movePlayerToNextStage(room.getCompetitor1());
				} else if (room.getCompetitor2() != null) {
					movePlayerToNextStage(room.getCompetitor2());
				}
				
				
				room.setActive(false);
				toRemove.add(room);
			}
			
        }
		
		if (!toRemove.isEmpty()) {
			for (int i = 0, len = toRemove.size(); i < len; i++) {
				Room rRemove = toRemove.get(i);
				rooms.remove(rRemove.stage, rRemove);
			}
			toRemove.clear();
		}
	}
	
	private void removeAllEmptyOnThisStage() {
		if (stages < 0)
			return;
		Iterator<Room> it = rooms.get(stages).iterator();
		while (it.hasNext()) {
			Room room = (Room) it.next();
			if (room == null) continue;
			if (room.isEmpty()) {
				room.setActive(false);
				it.remove();
			}
			
        }
		
	}
	
	private void checkPlayerDeathOrMIA(Player p, Player winner, Room room, int compDied) {
		if (p == null || p.isDead()) {
			String name = "Mr Poop";
			room.setPlayerDied();
			room.setSmitePlayers(false); //to stop dealing damage to winner
			if (compDied == 1) {
				if (room.getCompetitor1() != null) {
					name = room.getCompetitor1().getDisplayName();
				}
				room.setCompetitor1(null);
			} else {
				if (room.getCompetitor2() != null) {
					name = room.getCompetitor2().getDisplayName();
				}
				room.setCompetitor2(null);
			}
				
			if (p != null && p.isActive) {
				flushPlayerFromTournament(p, false);
				TournamentRewards.rewardParticipation(p, instancedParticipationRewards, false);
			}
			
			//heal winner
			if (winner != null && winner.isActive) {
				healWinner(winner, name);
			}
			
		}
	}
	
	private static final String DEFEATED_MSG = "You have defeated %s!";
	private static final String MOVE_MSG = "You will be moved in 30 seconds.";
	
	private static final Graphic WIN_GFX = Graphic.create(122);
	
	private void healWinner(Player p, String loserName) {
		p.startDamageImmunity();
		p.getSkills().healToFull();
		p.getDotManager().reset();
		p.getSkills().setPrayerPointsToMax();
		p.resetDeathEvent();
		p.startGraphic(WIN_GFX);
		p.sendMessage(String.format(DEFEATED_MSG, loserName));
		p.sendMessage(MOVE_MSG);
	}
	
	private void processPlayer(Player p, Room room, String notification) {
		if (notification != null) {
			p.sendMessage(notification);
		}
		if (timer >= 50 && timer % 50 == 0)
			updateTimer(p);
		if (!room.fightStarted() && room.fightCountDownStarted()) {
			final int countDown = room.getFightCountdown();
			if (countDown == 0) {
				room.setCanFight(true);
			} else if (countDown > 0 && countDown < 11) {
				p.forceChat(Integer.toString(countDown));
			}
		}
		if (room.stage > 2 && room.fightStarted() && !room.isRoomFinished()) {
			if (timer == SMITE_TICK_WARN) {
				p.sendMessage("You will soon start taking damage from fog.");
			} else if (timer == SMITE_TICK) {
				room.setSmitePlayers(true);
			}
			if (room.smitePlayers() && timer % 2 == 0) {
				p.dealTrueDamage(2, 0, null, null);
			}
		}
	}
	
	private static Tournament getInstanceById(int id) {
		return tournInstances.get(id);
	}
	
	public static void onPlayerLogin(Player p) {
		p.tournamentInstance = getInstanceById(p.getTournamentInstanceId());
		
		if (p.tournamentInstance != null && p.tournamentInstance.isActive) {
			p.tournamentInstance.putBackIntoGameOrKick(p);
		} else {
			flushPlayerFromTournament(p, true);
		}
	}
	
	private void putBackIntoGameOrKick(Player p) {
		p.tournSettings = p.tournamentInstance.getSettings();
		for (Room room : rooms.values()) {
			if (!room.isActive() || room.fightStarted()) continue;
			if (room.getCompetitor1() != null && room.getCompetitor1().getMysqlId() == p.mySQLIndex) {
				p.tournSettings.getPresetType().setPreset(p);
				room.movePlayerIntoRoom(p);
				return;
			}
			if (room.getCompetitor2() != null && room.getCompetitor2().getMysqlId() == p.mySQLIndex) {
				p.tournSettings.getPresetType().setPreset(p);
				room.movePlayerIntoRoom(p);
				return;
			}
		}
		//unable to put back into game so flush the player out
		flushPlayerFromTournament(p, true);
	}
	
	private void movePlayerToNextStage(Competitor comp) {
		
		if (comp == null) {
			System.out.println("comp is null?");
			return;
		}
		
		final int nextStage = stages-1;
		
		if (nextStage < 0) {
			
			TournamentRewards.rewardFirstPlaceWinner(this, comp);
			
			if (instanceSettings.isOfficialTournament()) {
				TournamentRewards.uploadWinnerToWebsite(comp.getMysqlId(), OfficialTournamentVariables.pktFirstPlaceItemAmount, instanceSettings.getPresetType().toString());
			}
			
			stages = -1;
			return;
		}
		boolean movedIn = false;
		for (Room room : rooms.get(nextStage)) {
			if (room.addCompetitor(comp)) {
				movedIn = true;
				break;
			}
		}
		if (!movedIn)
			System.out.println("could not find room for player name:"+comp.getDisplayName()+" next stage:"+nextStage+" curStage:"+stages);
	}
	
	private void fillRoomsWithPlayers(int size) {
		for (int i = 0; i < size; i++) {
			Player p = lobbyPlayers.get(i);
			if (p == null || p.disconnected || !p.getZones().isInDmmLobby()) continue;
			
			p.setTournamentInstanceId(instanceId);
			
			p.tournamentInstance = this;
			
			updateTimer(p);
			
			boolean putIntoRoom = false;
			
			Competitor comp = new Competitor(p.mySQLIndex, p.getNameSmartUp());
			competitors.put(p.mySQLIndex, comp);
			for (Room room : rooms.get(stages)) {
				if (room.addCompetitor(comp)) {
					putIntoRoom = true;
					break;
				}
			}
			
			if (putIntoRoom) {
				
				if (instanceSettings.isSpawnGear()) {
					p.hasSpawnedGear = true;
					createGearCopy(p);
				}
				

				p.getPacketSender().sendDiscordUpdate("Solo", "Pk Tournament", size, MAX_PLAYERS, System.currentTimeMillis()/1000, 0);
				
			} else {
				System.out.println("DID NOT PUT PLAYER IN FOR SOME REASON. PLAYERNAME:"+p.getNameSmart());
			}
			
		}
	}
	
	private static void createGearCopy(Player p) {
			p.tournEquip = Arrays.copyOf(p.playerEquipment, p.playerEquipment.length);
			p.tournEquipN = Arrays.copyOf(p.playerEquipmentN, p.playerEquipmentN.length);
			p.tournInventory = Arrays.copyOf(p.playerItems, p.playerItems.length);
			p.tournInventoryN = Arrays.copyOf(p.playerItemsN, p.playerItems.length);
			p.runePouchItemsSpawned = Arrays.copyOf(p.runePouchItems, p.runePouchItems.length);
			p.runePouchItemsSpawnedN = Arrays.copyOf(p.runePouchItemsN, p.runePouchItemsN.length);
	}
	
	private void createRooms(int size) {
		stages = 0;
		Room tRoom = new Room(Floor1.ROOM1.getLoc(), stages);
		rooms.put(stages, tRoom);
		if (size > 2) {
			int roomSize = size/2;
			stages = 1;
			for (int i = 0; i < roomSize; i++) {
				if (i >= Floor2.values.length)
					break;
				Room room = new Room(Floor2.values[i].getLoc(), stages);
				rooms.put(stages, room);
			}
		}
		if (size > 4) {
			int roomSize = size/2;
			stages = 2;
			for (int i = 0; i < roomSize; i++) {
				if (i >= Floor4.values.length)
					break;
				Room room = new Room(Floor4.values[i].getLoc(), stages);
				rooms.put(stages, room);
			}
		}
		if (size > 8) {
			int roomSize = size/2;
			stages = 3;
			for (int i = 0; i < roomSize; i++) {
				if (i >= Floor8.values.length)
					break;
				Room room = new Room(Floor8.values[i].getLoc(), stages);
				rooms.put(stages, room);
			}
		}
		if (size > 16) {
			int roomSize = size/2;
			stages = 4;
			for (int i = 0; i < roomSize; i++) {
				if (i >= Floor16.values.length)
					break;
				Room room = new Room(Floor16.values[i].getLoc(), stages);
				rooms.put(stages, room);
			}
		}
		if (size > 32) {
			int roomSize = size/2;
			stages = 5;
			for (int i = 0; i < roomSize; i++) {
				if (i >= Floor32.values.length)
					break;
				Room room = new Room(Floor32.values[i].getLoc(), stages);
				rooms.put(stages, room);
			}
		}
		if (size > 64) { //64 rooms then
			int roomSize = size/2;
			stages = 6;
			for (int i = 0; i < roomSize; i++) {
				Room room = new Room(Floor64.values[i].getLoc(), stages);
				rooms.put(stages, room);
			}
		}
		
		stageStartedAt = stages;
	}
	
	private void updateTimer(Player p) {
		p.getPacketSender().sendFrame126(getTimeDisplay(), 199);
	}
	
	private String getTimeDisplay() {
		return Misc.ticksToTimeShort(timer);
	}
	
	public void setTimer(int tick) {
		this.timer = tick;
	}
	
	private void kickAllPlayers() {
		for (Room room : rooms.values()) {
			if (!room.isActive()) continue;
			Player p1 = null;
			Player p2 = null;
			if (room.getCompetitor1() != null) {
				p1 = PlayerHandler.getPlayerByMID(room.getCompetitor1().getMysqlId());
			}
			if (room.getCompetitor2() != null) {
				p2 = PlayerHandler.getPlayerByMID(room.getCompetitor2().getMysqlId());
			}
			
			if (p1 != null && p1.isActive) {
				flushPlayerFromTournament(p1, true);
			}
			if (p2 != null && p2.isActive) {
				flushPlayerFromTournament(p2, true);
			}
			
			room.setCompetitor1(null);
			room.setCompetitor2(null);
        }
	}
	
	public static void flushPlayerFromTournament(Player p, boolean movePlayer) {
		if (movePlayer) {
			p.getPA().movePlayer(LocationConstants.EDGE);
			if (p.deathContainer != null)
				p.deathContainer.stop();
			p.deathContainer = null;
		}
		p.tournamentRoom = null;
		p.tournamentInstance = null;
		p.setSpawnedSkills(null);
		p.getSkills().restore();
		p.calcCombat();
		p.setDefaultDiscordStatus();
	}
	
	public static void restockFoodPots(Player p) {
		for (int i = 0; i < p.playerItems.length; i++) {
			int itemId = p.playerItems[i]-1;
			
			if (itemId == -1) continue;
			
			Potion pot = Potion.forId(itemId);
			if (pot != null || Food.isFood(itemId) || itemId == ItemIds.VIAL || Potions.isPotion(itemId) || itemId == 3144) {
				p.getItems().deleteItemInSlot(i);
			}
			
		}
		
		for (int i = 0, size = p.tournSettings.getRestockItems().length; i < size; i++) {
			int itemId = p.tournSettings.getRestockItems()[i];
			p.getItems().addOrDrop(new GameItem(itemId, 1));
		}
		
	}
	
	public boolean onFirstStage(int currentStage) {
		return currentStage == stageStartedAt;
	}
	
	public static void clearAllItems(Player p) {
		p.getItems().deleteAllItems();
		RunePouch.clearRunePouch(p);
	}
	
	public static boolean insideArenaWithSpawnedGear(Player p) {
		return p.getZones().isInDmmTourn() && p.hasSpawnedGear;
	}
	
	private static final int WAIT_TIME = 6000; //840;
	
	public static int lobbyWaitTime = 0;
	
	private static long lobbyStartTimestamp = 0;
	private static long lobbyEndTimestamp = 0;
	
	public static void beginPkTourneyQueue() {
		
		if (Tournament.lobbyOpen || Config.NODE_ID != 1 || !OFFICIAL_ENABLED) {
			return;
		}
		
		Tournament.toggleLobby();
		
		Presets preset = Presets.getPkTournyPresetRandom();
		
		String msg = "<col=800000>PKing Tournament will start in " + (Misc.ticksToSeconds(WAIT_TIME)/60) + " minutes. Tournament type: "+preset.getPrettyName();
		
		PlayerHandler.messageAllPlayers(msg);
		
		PlayerHandler.messageAllPlayersInAllWorlds(msg, "");
		
		String partRewardName = ItemProjectInsanity.getItemName(OfficialTournamentVariables.pktParticipationItemId);
		if (partRewardName == null)
			partRewardName = "NOTHING";
		final String partAmount = Misc.formatShortPrice(OfficialTournamentVariables.pktParticipationItemAmount);
		
		final String partMsg = "<col=800000>PKing Tournament will give " + partAmount + " " + partRewardName + " to anyone who participates.";
		
		PlayerHandler.messageAllPlayers(partMsg);
		
		final String rewardName = ItemProjectInsanity.getItemName(OfficialTournamentVariables.pktFirstPlaceItemId);
		final String amount = Misc.formatShortPrice(OfficialTournamentVariables.pktFirstPlaceItemAmount);
		AnnouncementHandler.announce(preset.getPrettyName() + " PK Tournament lobby is open for 1 hour. Winner will get " + amount + " " + rewardName + ". To join type ::join");

		preset.setSettings();

		lobbyWaitTime = WAIT_TIME;
		
		long currentTimestamp = System.currentTimeMillis()/1000;
		
		lobbyStartTimestamp = currentTimestamp;
		lobbyEndTimestamp = currentTimestamp  + (lobbyWaitTime * 600 / 1000);
		
		Server.getTaskScheduler().schedule(new Task(true) {

			@Override
			public void execute() {
				if (!Tournament.lobbyOpen || lobbyWaitTime < 1) {
					this.stop();
					return;
				}

				if (lobbyWaitTime > 0) {
					lobbyWaitTime--;
					
					final int size = lobbyPlayers.size();
					for (int i = 0; i < size; i++) {
						Player p = lobbyPlayers.get(i);
						if (p != null && p.isActive) {
							updateLobbyTimer(p);
						}
					}
				}

				if (lobbyWaitTime == 3000) {
					String msg = preset.getPrettyName() + " PK Tournament will start in 30 minutes! Winner will get " + amount + " " + rewardName + ". ::join";
					AnnouncementHandler.announce(msg);
					msg = "<col=800000>" + msg;
					PlayerHandler.messageAllPlayers(msg);
					PlayerHandler.messageAllPlayers(partMsg);
					
				}

				if (lobbyWaitTime == 1500) {
					String msg = preset.getPrettyName() + " PK Tournament will start in 15 minutes! Winner will get " + amount + " " + rewardName + ". ::join";
					AnnouncementHandler.announce(msg);
					msg = "<col=800000>" + msg;
					PlayerHandler.messageAllPlayers(msg);
					PlayerHandler.messageAllPlayers(partMsg);
				}

				if (lobbyWaitTime == 1000) {
					String msg = preset.getPrettyName() + " PK Tournament will start in 10 minutes! Winner will get " + amount + " " + rewardName + ". ::join";
					AnnouncementHandler.announce(msg);
					msg = "<col=800000>" + msg;
					PlayerHandler.messageAllPlayers(msg);
					PlayerHandler.messageAllPlayers(partMsg);
				}

				if (lobbyWaitTime == 500) {
					String msg = preset.getPrettyName() + " PK Tournament will start in 5 minutes! Winner will get " + amount + " " + rewardName + ". ::join";
					AnnouncementHandler.announce(msg);
					msg = "<col=800000>" + msg;
					PlayerHandler.messageAllPlayers(msg);
					PlayerHandler.messageAllPlayers(partMsg);
				}
				if (lobbyWaitTime == 200) {
					String msg = "<col=800000>" + preset.getPrettyName() + " PK Tournament will start in 2 minutes! Winner will get " + amount + " " + rewardName + ". ::join";
					PlayerHandler.messageAllPlayers(msg);
					PlayerHandler.messageAllPlayers(partMsg);
				}
				if (lobbyWaitTime == 100) {
					String msg = preset.getPrettyName() + " PK Tournament will start in 1 minutes! Winner will get " + amount + " " + rewardName + ". ::join";
					AnnouncementHandler.announce(msg);
					msg = "<col=800000>" + msg;
					PlayerHandler.messageAllPlayers(msg);
					PlayerHandler.messageAllPlayers(partMsg);
				}
				if (lobbyWaitTime == 20) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>" + preset.getPrettyName() + " PK Tournament will start in 10 seconds!");
				}
				if (lobbyWaitTime == 6) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>" + preset.getPrettyName() + " PK Tournament will start in 3 seconds!");
				}
				if (lobbyWaitTime == 4) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>" + preset.getPrettyName() + " PK Tournament will start in 2 seconds!");
				}
				if (lobbyWaitTime == 2) {
					PlayerHandler.messageAllPlayers(
							"<col=800000>" + preset.getPrettyName() + " PK Tournament will start in 1 seconds!");
				}
				if (lobbyWaitTime < 1) {
					this.stop();
					AnnouncementHandler.removeAnnouncement();
					final int size = lobbyPlayers.size();
					if (size % 2 == 1) {
						Player lastPlayer = lobbyPlayers.get(size-1);
						removeFromLobby(lastPlayer, false);
					}
					if (size < 2) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Not enough players in the PK tournament.");
						if (Tournament.lobbyOpen)
							Tournament.toggleLobby();
						kickAllFromLobby();
					} else {
						PlayerHandler.messageAllPlayers(
								"<col=800000>" + preset.getPrettyName() + " PK Tournament has started!");
						startGame();
						if (lastInstance != null && lastInstance.isActive) {
							lastInstance.instancedWinnerRewards.add(generateFirstPlaceReward());
							lastInstance.instancedParticipationRewards.add(generateParticipationReward());
							lastInstance.getSettings().setOfficialTournament(true);
						}
					}
					return;
				}


			}
		});

	}
	
	private static int generateReward() {
		return AUTOMATED_REWARD[Misc.random(AUTOMATED_REWARD.length - 1)];
	}
	
	private static Item generateFirstPlaceReward() {
		return new Item(OfficialTournamentVariables.pktFirstPlaceItemId, OfficialTournamentVariables.pktFirstPlaceItemAmount);
	}
	
	private static Item generateParticipationReward() {
		return new Item(OfficialTournamentVariables.pktParticipationItemId, OfficialTournamentVariables.pktParticipationItemAmount);
	}
	
}
