package com.soulplay.content.minigames.lastmanstanding;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.GameObject;

public class LmsLobbyObjects {

	private static final int forfeitDoor = 29067;
	private static final int coffer = 29087;
	
	public static boolean searchLMSObjects(Client c, GameObject o) {
		
		switch (o.getId()) {
		
		case coffer:
		case 129087:
			if (!c.getItems().playerHasItem(995, 100_000)) {
				if (c.getLmsCoffer() == 0) {
				    c.getDialogueBuilder().sendItemStatement(1004, "The coffer is empty.", "You need at least 100,000 coins to make a deposit.").execute();
				} else if (c.getLmsCoffer() > 0) {
					c.getDialogueBuilder().sendItemStatement(1004, "You have " + c.getLmsCoffer() + " coins stored in the coffer.", "You need at least 100,000 coins to make a deposit.")
					.sendOption("Withdraw money.", ()-> {
						c.getDialogueBuilder().sendEnterAmount(()-> {
							withdraw(c);
				    	 });
					}, "Cancel", ()-> {}).execute();
				}
				return true;			
			}
			if (c.getLmsCoffer() > 0) {
			    c.getDialogueBuilder().sendItemStatement(1004, "You have " + c.getLmsCoffer() + " coins stored in the coffer.")
			    .sendOption("Deposit money.", ()-> {
			    	 c.getDialogueBuilder().sendEnterAmount(()-> {
			    		deposit(c);
			    	 });
			    }, "Withdraw money.", ()-> {
			    	c.getDialogueBuilder().sendEnterAmount(()-> {
			    		withdraw(c);
			    	 });
			    }, "Cancel.", ()-> {}).execute();
		  	} else {
				c.getDialogueBuilder().sendItemStatement(1004, "The coffer is empty.")
				.sendOption("Deposit money.", ()-> {
					c.getDialogueBuilder().sendEnterAmount(()-> {
					deposit(c);
			    	 });
				}, "Cancel.", ()-> {}).execute();
			}
			return true;
		
		case forfeitDoor:
		case 129067:
			c.getDialogueBuilder().sendOption("Yes forfeit.", ()-> {
				LmsManager.removePlayerWaitingRoom(c);
			}, "No don't forfeit.", ()-> {}).execute();
			return true;
		
		}
		return false;
	}

	public static void deposit(Client c) {
		 long toDeposit = c.enterAmount;
		 if (toDeposit <= 0 || toDeposit >= Integer.MAX_VALUE) {
			 c.sendMessage("The amount is too high!");
			 return;
		 }

		 int toDepositInt = (int) toDeposit;

		 int current = c.getItems().itemAmount(995 + 1);
		 if (toDepositInt > current) {
			 toDepositInt = current;
		 }

		 if (toDepositInt < 100_000) {
			 c.getDialogueBuilder().sendStatement("You can only deposit multiplies of 100,000 coins.");
    		 return;
		 }

		 int amountToMax = Integer.MAX_VALUE - c.getLmsCoffer();
		 if (amountToMax <= toDepositInt) {
			 c.sendMessage("Your cofffer is full.");
			 toDepositInt = amountToMax;
		 }

		 try {
			 int toAdd = Math.addExact(c.getLmsCoffer(), toDepositInt);
			 c.getItems().deleteItemInOneSlot(995, toDepositInt);
			 c.setLmsCoffer(toAdd);
			 c.getPacketSender().sendString(Integer.toString(c.getLmsCoffer()), 66118);
		 } catch (ArithmeticException e) {
			 c.sendMessage("Wrong.");
		 }
	}

	public static void withdraw(Client c) {
		 long toWithdraw = c.enterAmount;
		 if (toWithdraw <= 0 || toWithdraw >= Integer.MAX_VALUE) {
			 c.sendMessage("The amount is too high!");
			 return;
		 }

		 int toWithdrawInt = (int) toWithdraw;
		 if (toWithdrawInt > c.getLmsCoffer()) {
			 toWithdrawInt = c.getLmsCoffer();
		 }

		 int amountToMax = Integer.MAX_VALUE - c.getItems().itemAmount(995 + 1);
		 if (amountToMax <= toWithdrawInt) {
			 c.sendMessage("Your inventory can't hold more coins.");
			 toWithdrawInt = amountToMax;
		 }

		 try {
			 int deduct = Math.subtractExact(c.getLmsCoffer(), toWithdrawInt);
			 deduct = Math.max(0, deduct);
			 c.setLmsCoffer(deduct);
			 c.getItems().addItem(995, toWithdrawInt);
			 c.getPacketSender().sendString(Integer.toString(c.getLmsCoffer()), 66118);
		 } catch (ArithmeticException e) {
			 c.sendMessage("Wrong.");
		 }
	}
}
