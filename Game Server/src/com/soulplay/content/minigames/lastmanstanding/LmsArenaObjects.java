package com.soulplay.content.minigames.lastmanstanding;

import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class LmsArenaObjects {
	
	public static final int CHEST_OBJ = 29069;
	public static final int CHEST_OPEN_OBJ = 29070;
	public static final int DRAWER_OBJ = 29073;
	public static final int DRAWER_OPEN_OBJ = 29074;
	public static final int CUPBOARD_OBJ = 29077;
	public static final int CUPBOARD_OPEN_OBJ = 29078;
	public static final int ROWBOAT = 29082;
	
	private static final Animation openChest = new Animation(536);
	private static final Animation openChestDelayed = new Animation(536, 20);
	private static final Animation openDrawerCupboard = new Animation(535);
	private static final Animation openDrawerCupboardDelayed = new Animation(535, 20);
	public static final Animation searchChest = new Animation(1464);
	private static final Animation searchChestDelayed = new Animation(1464, 20);
	
	public static final int bloodyKey = 30105;
	public static final int bloodierKey = 30106;

	public static boolean searchLMSObjects(Client c, int objectId, int objectX, int objectY, int objectZ) {
		if (c.dynObjectManager == null) {
			return false;
		}

		final GameObject o = c.dynObjectManager.getObject(objectId, objectX, objectY, objectZ);
		if (o == null) {
			return false;
		}

		switch (objectId) {
		case 29091:
			c.getShops().openShop(60);
			return true;
		case 29062:
			if (c.lmsManager.lockUpstairs() && !(o.getX() == 1916 && o.getY() == 3881)) {
				c.sendMessage("You cannot go upstairs anymore.");
			} else {
				c.getPA().movePlayerDelayed(c.absX, c.absY, c.getHeightLevel() + 1, 828, 1);
			}
	        return true;
		case 29089:
	        c.getPA().movePlayerDelayed(c.absX, c.absY, c.getHeightLevel() - 1, 827, 1);
			return true;
		case 29092:
			final Direction dir = Direction.getNonDiagonal(o.getOrientation()+2);
			
			final Location upstairs = Location.create(o.getX()+dir.getStepX(), o.getY()+dir.getStepY(), c.getZ());
			
			if (c.getCurrentLocation().matches(upstairs)) {
			    c.getPA().movePlayerDelayed(o.getX()-dir.getStepX(),
					o.getY() - dir.getStepY(),
					c.getHeightLevel(), 0, 1);
				c.startAnimation(Animation.PICK_UP);
			} else {
				c.getPA().movePlayerDelayed(o.getX()+dir.getStepX(), o.getY()+dir.getStepY(), c.getZ(), 0, 1);
				c.startAnimation(c.isMoving ? Animation.CLIMB_UP_DELAYED : Animation.CLIMB_UP);
			}
			return true;
		case MagicalCrate.CRATE_OBJECT_ID:
			MagicalCrate.lootLmsCrate(c, o);
			return true;
		case ROWBOAT:
			c.getDialogueBuilder().sendOption("Do you really want to forfeit?", "Yes.", () -> {
				if (!c.isInLmsArena()) {
					return;
				}

				//c.lmsManager.removePlayerFromArena(c, true);
				//c.getItems().deleteAllItems(); // double delete just in case
				c.dealDamage(new Hit(99, 0, 0));
			}, "No.", () -> {}).execute();
			return true;
		case CHEST_OBJ:
		case CHEST_OPEN_OBJ:
		case DRAWER_OBJ:
		case DRAWER_OPEN_OBJ:
		case CUPBOARD_OBJ:
		case CUPBOARD_OPEN_OBJ:
			openAndLoot(c, o);
			return true;
		}

		return false;
	}
	
	public static void openAndLoot(Player c, final GameObject o) {
		if (c.skillingEvent != null) {
			return;
		}

		final int x = o.getLocation().getX();
		final int y = o.getLocation().getY();
		final int z = o.getLocation().getZ();
		final int typeConverted = Misc.OBJECT_SLOTS[o.getType()];
		final long uniqueId = GameObject.generateKey(x, y, z, typeConverted);
		
		final boolean chestLootedThisTick = c.lmsManager.chestsLooted.containsKey(uniqueId);

		if (chestLootedThisTick) {
			final boolean playerHasKey = c.getItems().playerHasItem(bloodyKey) || c.getItems().playerHasItem(bloodierKey);

			if (playerHasKey) {
				lootWithKey(c);
				return;
			} else {
				c.sendMessage("This seems to be empty.");
				return;
			}
		}
		
		c.lmsUniqueId = uniqueId;

		c.startAction(new CycleEvent() {

			int timer = 6;
			
			@Override
			public void execute(CycleEventContainer container) {
		
				if (c.disconnected || c.dynObjectManager == null) {
					container.stop();
					return;
				}
				
				if (timer == 6 && (c.objectId == CHEST_OPEN_OBJ || c.objectId == DRAWER_OPEN_OBJ || c.objectId == CUPBOARD_OPEN_OBJ)) {
					timer = 4;
				}
				
				if (timer == 6) {
					if (c.objectId == CHEST_OBJ) {
						if (c.dynObjectManager.getObject(CHEST_OPEN_OBJ, x, y, z, typeConverted) != null) {
							container.stop();
							return;
						}
					    c.startAnimation(c.isMoving ? openChestDelayed : openChest);
					    GameObject obj = GameObject.createTickObject(CHEST_OPEN_OBJ, o.getLocation(), o.getOrientation(), o.getType(), Integer.MAX_VALUE, -1);
					    c.dynObjectManager.addObject(obj);
					} else if (c.objectId == DRAWER_OBJ) {
						if (c.dynObjectManager.getObject(DRAWER_OPEN_OBJ, x, y, z, typeConverted) != null) {
							container.stop();
							return;
						}
						c.startAnimation(c.isMoving ? openDrawerCupboardDelayed : openDrawerCupboard);
						GameObject obj = GameObject.createTickObject(DRAWER_OPEN_OBJ, o.getLocation(), o.getOrientation(), o.getType(), Integer.MAX_VALUE, -1);
					    c.dynObjectManager.addObject(obj);
					} else if (c.objectId == CUPBOARD_OBJ) {
						if (c.dynObjectManager.getObject(CUPBOARD_OPEN_OBJ, x, y, z, typeConverted) != null) {
							container.stop();
							return;
						}
						c.startAnimation(c.isMoving ? openDrawerCupboardDelayed : openDrawerCupboard);
						GameObject obj = GameObject.createTickObject(CUPBOARD_OPEN_OBJ, o.getLocation(), o.getOrientation(), o.getType(), Integer.MAX_VALUE, -1);
					    c.dynObjectManager.addObject(obj);
					}
				}

				if (timer == 3) {
					// TODO: find proper searching on knee
					if (c.objectId == CHEST_OBJ || c.objectId == CHEST_OPEN_OBJ) {
					    c.startAnimation(c.isMoving ? searchChestDelayed : searchChest);
					} else if (c.objectId == DRAWER_OBJ || c.objectId == DRAWER_OPEN_OBJ) {
						c.startAnimation(c.isMoving ? openDrawerCupboardDelayed : openDrawerCupboard);
					} else if (c.objectId == CUPBOARD_OBJ || c.objectId == CUPBOARD_OPEN_OBJ) {
						c.startAnimation(c.isMoving ? openDrawerCupboardDelayed : openDrawerCupboard);
					}
					c.sendMessage("You search the container...");
				}
				
				if (timer == 3) {
					c.sendMessage("...you rummage around...");
				}
				
				if (timer == 2) {
					c.sendMessage("...you feel something...");
				}		

				if (timer == 1) {
					if (!c.lmsManager.chestsLooted.containsKey(uniqueId)) {
	                	c.lmsManager.chestsLooted.put(uniqueId, true);
						LootManager.addLoot(c, false);
					}
				}

				timer--;
				if (timer == 0) {
					container.stop();
				}
			}

			@Override
			public void stop() {
				c.resetAnimations();
				c.lmsUniqueId = 0;
			}
		}, 1);
	}
	
	public static void lootWithKey(Player c) {
		
		if (c.getSkills().getLifepoints() == 0)
			return;

		if (c.getItems().deleteItemInOneSlot(bloodyKey, 1)) {
			LootManager.addLoot(c, false);
			c.getSkills().healToFull();
			c.sendMessage("You feel invigorated as the bloody key is consumed and you find some loot!");
			c.startAnimation(c.isMoving ? openDrawerCupboardDelayed : openDrawerCupboard);
			return;
		} else if (c.getItems().deleteItemInOneSlot(bloodierKey, 1)) {
			LootManager.addLoot(c, true);
			c.getItems().addItem(385, 2 + Misc.random(1));
			c.getSkills().healToFull();
			c.getSkills().incrementPrayerPoints(25);
			c.setUpdateInvInterface(3214);
			c.getMovement().restoreRunEnergy(35);
			c.getPacketSender().sendRunEnergy();
			c.sendMessage("You feel invigorated as the bloodier key is consumed and you find some loot!");
			c.startAnimation(c.isMoving ? openDrawerCupboardDelayed : openDrawerCupboard);
			return;
		}
		
	}
} 
