package com.soulplay.content.minigames.lastmanstanding;

import com.soulplay.Server;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

public class Lisa {
	
	public static final int LISA_NPC_ID = 5000;
	
	public static final int DONATION_VALUE = 200_000_000;
	
	public static boolean COMPETITIVE_ENABLED = false;
	
	public static final String[] DENY_ITEM_STR = { "You must have an item worth,", Misc.format(DONATION_VALUE) + " Gold to create an event." };
	
	public static final String DONATION_PART_1 = "Would you like to donate";
	public static final String DONATION_PART_2 = "to create an LMS event?";

	public static void firstClick(Player player, NPC npc) {
		player.getDialogueBuilder()
		.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Welcome, are you looking to take part in a game of", "Last Man Standing?")
		.sendOption("Join Last Man Standing.", ()-> {
			player.getDialogueBuilder().sendPlayerChat("I'd like to join Last Man Standing.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Excellent, which mode would you like to participate in?")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Remember, you'll need to bank all of your items and", "If you choose competitve you need to have the entrance fee", "ready in your coffer if.")
			.sendOption("Normal.", ()-> {
				LmsManager.addToWaitRoom(player, LmsManager.serverWideMode);
			}, "Competitive.", ()-> {
				if (!COMPETITIVE_ENABLED) {
					player.sendMessage("Competitive mode is disabled so lisa sends you to Normal mode.");
					LmsManager.addToWaitRoom(player, LmsManager.serverWideMode);
				} else
					LmsManager.addToWaitRoom(player, LmsManager.Mode.COMPETETIVE);
			}, "Cancel.", ()-> {});
			

		}, "Tell me more.", ()-> {
			player.getDialogueBuilder().sendPlayerChat("Tell me more.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "It's simple. A group of competitors are taken to the ", "Island, only one comes out alive.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "You can't take any of your belongings to the island,", "you can only use what you find there.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "The fight for survival will boost your physical and", "mental abilities, ensuring you will make a worthy", "contender as the Last Mna Standing.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Be warned, however, that your prayer will be heavily", "drained upon entering the island.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Lethal fog will approach the island after a set amount of", "time, forcing contenders to designate safezone.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Remember, this is a solo competition, anyone attempting", "to form teams for an unfair advantage will be banished", "from further participation.")
			.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Now that you know more, would you like to take part", "In a game of Last Man Standing?")
			.sendOption("Join Last Man Standing", ()-> {
				player.getDialogueBuilder().sendPlayerChat("I'd like to join Last Man Standing.")
				.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Excellent, which mode would you like to participate in?")
				.sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, "Remember, you'll need to bank all of your items and", "If you choose competitive you need to have the entrance fee", "ready in your coffer if.")
				.sendOption("Normal.", ()-> {
					LmsManager.addToWaitRoom(player, LmsManager.serverWideMode);
				}, "Competitive.", ()-> {
					if (!COMPETITIVE_ENABLED) {
						player.sendMessage("Competitive mode is disabled so lisa sends you to Normal mode.");
						LmsManager.addToWaitRoom(player, LmsManager.serverWideMode);
					} else
						LmsManager.addToWaitRoom(player, LmsManager.Mode.COMPETETIVE);
				}, "Cancel.", ()-> {});
			}, "Not Right Now.", ()-> {});
		}, "Not right now.", ()-> {}).execute();
	}
	
	public static void secondClick(Player player, NPC npc) {
		player.getDialogueBuilder().sendOption("Normal.", ()-> {
			LmsManager.addToWaitRoom(player, LmsManager.serverWideMode);
		}, "Competitive.", ()-> {
			if (!COMPETITIVE_ENABLED) {
				player.sendMessage("Competitive mode is disabled so lisa sends you to Normal mode.");
				LmsManager.addToWaitRoom(player, LmsManager.serverWideMode);
			} else
				LmsManager.addToWaitRoom(player, LmsManager.Mode.COMPETETIVE);
		}, "Cancel.", ()-> {}).execute();
	}
	
	public static void itemOnLisa(final Player player, final int itemId, final int itemSlot) {
		if (LmsManager.normalReward != null) {
			player.sendMessage("Can't donate during an ongoing LMS event.");
			return;
		}

		if (itemId == 10831) {
			player.sendMessage("This item has been blacklisted.");
			return;
		}

		final long itemValue = PriceChecker.getPriceLong(itemId);
		
		final boolean itemStackable = ItemProjectInsanity.itemIsStackable(itemId);
		
		final int itemAmount = player.getItems().getItemAmountInSlot(itemSlot);
		
		final long totalValue = (itemStackable ? itemValue * itemAmount : itemValue);
		
		if (itemStackable && itemId != 995) {
			long pricePer1k = itemValue * 100;
			if (pricePer1k < 200_000_000) {
				player.sendMessage("This item is not allowed.");
				return;
			}
		}
		
		if (totalValue >= DONATION_VALUE) {
			final String itemName = ItemProjectInsanity.getItemName(itemId);
			final Item donation = new Item(itemId, itemAmount, itemSlot);
			
			final StringBuilder itemNameAndAmount = new StringBuilder();
			itemNameAndAmount.append(itemAmount).append(" of ").append(itemName);
			
			player.getDialogueBuilder().sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, DONATION_PART_1, itemNameAndAmount.toString(), DONATION_PART_2)
			.sendOption("Yes", ()-> {
				donateItem(player, donation, itemAmount, itemName);
			}, "No", ()-> {
				player.getDialogueBuilder().jumpToStage(100);
			})
			.execute();
		} else {
			player.getDialogueBuilder().sendNpcChat(LISA_NPC_ID, Expression.DEFAULT_OLD, DENY_ITEM_STR).execute();
		}
	}
	
	public static void donateItem(final Player player, final Item item, final int itemAmount, final String itemName) {
		if (LmsManager.normalReward != null) {
			player.sendMessage("Can't donate during an ongoing LMS event.");
			return;
		}
		
		if (!player.getItems().playerHasItem(item)) {
			player.sendMessage("Please try again.");
			return;
		}
		
		Server.getSlackApi().call(SlackMessageBuilder.buildLisaDonationMessage(player.getNameSmart(), item));
		
		if (player.getItems().deleteItemInOneSlot(item.getId(), itemAmount))
			LmsManager.createLMSNormalEvent(item, Misc.formatNumbersWithCommas(itemAmount), itemName);
	}
	
}
