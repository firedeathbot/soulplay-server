package com.soulplay.content.minigames.lastmanstanding;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.util.Misc;

public class LootManager {

	public static void addLoot(Player p, boolean bloodierKey) {// loot
		int defaultRolls = 2;

		for (int i = 0; i < defaultRolls; i++) {
			int random = Misc.randomNoPlus(1000);
			if (random >= 850) {
				doLooting(p, ItemLoot.COMMON_FOOD);
			} else if (random >= 700) {
				doLooting(p, ItemLoot.UNCOMMON_FOOD);
			} else if (random >= 380) {
				doLooting(p, ItemLoot.COMMON_WEAPONS);
			} else if (random >= 160) {
				doLooting(p, ItemLoot.COMMON_ARMOUR);
			} else if (random >= 95) {
				doLooting(p, ItemLoot.UNCOMMON_WEAPONS);
			} else if (random >= 35) {
				doLooting(p, ItemLoot.UNCOMMON_ARMOUR);
			} else if (random >= 25) {
				doLooting(p, ItemLoot.COMMON_SPEC_WEAPONS);
			} else if (random >= 10) {
				doLooting(p, ItemLoot.RARE_WEAPONS);
			} else if (random >= 2) {
				doLooting(p, ItemLoot.RARE_ARMOUR);
			} else if (random >= 0) {
				doLooting(p, ItemLoot.SUPER_RARE_WEAPONS);
			}
		}

		if (bloodierKey) {
			if (Misc.random(150) == 1) {
				doLooting(p, ItemLoot.SUPER_RARE_WEAPONS);
			}
		}
	}

	private static void doLooting(Player c, int[][] items) {
		int random = Misc.randomNoPlus(items.length);

//		System.out.println("itemId: " + items[random] + " min: " + items[random][1] + " max: " + items[random][2]);
//		System.out.println("itemId: " + items[random][3]);

		c.getItems().addOrDrop(new GameItem(items[random][0], Misc.randomNoPlus(items[random][1], items[random][2])));
		if (items[random][3] < 1) {
			return;
		}
		c.getItems().addOrDrop(new GameItem(items[random][3], 1));
	}
}
