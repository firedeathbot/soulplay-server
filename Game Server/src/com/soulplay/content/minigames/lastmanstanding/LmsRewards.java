package com.soulplay.content.minigames.lastmanstanding;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

public class LmsRewards {
	
	private static final List<LmsRewards> rewards = new ArrayList<>();
	
	public final Item reward;
	public final int mysqlIndex;
	
	public LmsRewards(Item reward, int mysqlIndex) {
		this.reward = new Item(reward.getId(), reward.getAmount());
		this.mysqlIndex = mysqlIndex;
	}
	
	public static void addReward(Player p, LmsRewards reward) {
		rewards.add(reward);
		p.lmsRewardsToClaim.add(new Item(reward.reward.getId()+1, reward.reward.getAmount())); //just to display on interface
	}
	
	public static boolean checkRewards(Player p) {
		Iterator<LmsRewards> itr = rewards.iterator();
		boolean rewarded = false;
		while (itr.hasNext()) {
			LmsRewards r = itr.next();
			if (r.mysqlIndex == p.mySQLIndex) {
				if (ItemProjectInsanity.itemIsStackable(r.reward.getId())) {
					p.getItems().addItem(r.reward.getId(), r.reward.getAmount(), true);
				} else {
					for (int i = 0; i < r.reward.getAmount(); i++) {
						p.getItems().addItem(r.reward.getId(), 1, true);
					}
				}
				itr.remove();
				rewarded = true;
			}
		}
		return rewarded;
	}
	
	private static final String UPLOAD_QUERY = "INSERT INTO zaryte.lms_event_winners (player_id, item_id, item_amount, total_players, timestamp) VALUES(?, ?, ?, ?, ?)";
	
	public static void uploadWinnerToWebsite(long sqlId, int itemId, int itemAmount, int totalPlayers) {
		
		if (!Config.RUN_ON_DEDI)
			return;
		
		final Timestamp time = Misc.getESTTimestamp();
		
		TaskExecutor.executeWebsiteSQL(new Runnable() {
			
			@Override
			public void run() {

				try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
						PreparedStatement ps = connection.prepareStatement(UPLOAD_QUERY);) {
					
					ps.setLong(1, sqlId);
					ps.setInt(2, itemId);
					ps.setInt(3, itemAmount);
					ps.setInt(4, totalPlayers);
					ps.setTimestamp(5, time);
					
					ps.executeUpdate();

				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		
	}

}
