package com.soulplay.content.minigames.lastmanstanding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.announcement.AnnouncementHandler;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.event.randomevent.MagicalCrate.LMSLocationData;
import com.soulplay.content.minigames.MinigameRules;
import com.soulplay.content.minigames.ddmtournament.OfficialTournamentVariables;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.achievement.Achievs;
import com.soulplay.content.player.combat.DamageMap.TotalDamage;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.MapTools;
import com.soulplay.content.shops.ShopPrice;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.GroundItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Location2D;
import com.soulplay.game.world.map.DynamicObjectManager;
import com.soulplay.game.world.map.ObjectDefVersion;
import com.soulplay.game.world.map.travel.ZoneManager;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.EloRatingSystem;
import com.soulplay.util.Misc;

public class LmsManager {

	private static final int FOG_INIT_RADIUS = 128;
	private static final int FOG_DECREASE = 1;
	private static final int FOG_DECREASE_TICKS = 6;//6
	private static final int FOG_MIN_RADIUS = 3;
	private static final int FOG_SAFE_TILES = 10;
	
	public static final int ARMADYL_GODSWORD = 30121;
	public static final int DRAGON_CLAWS = 30122;
	public static final int DRAGON_2H = 30123;
	public static final int ABYSSAL_WHIP = 30124;
	public static final int DRAGON_WARHAMMER = 30125;
	public static final int DARK_BOW = 30130;
	
	// points to reward
	public static final int POINTS_PER_KILL = 20;
	public static final int FIRST_PLACE = 1000;
	public static final int SECOND_PLACE = 900;
	public static final int THIRD_PLACE = 800;
	public static final int FOURTH_PLACE = 700;
	public static final int FIFTH_PLACE = 600;
	private static final int THE_REST = 400;
	
	private static final int GOLD_TO_REWARD_TOP_5 = 20_000_000;
	private static final int GOLD_TO_REWARD_PARTICIPATION = 10_000_000;
	
	public static boolean EXTRA_REWARD = false;

	public static long uid;
	public static final Map<Long, LmsManager> instances = new HashMap<>();
	public DynamicRegionClip clipping = new DynamicRegionClip();
	public DynamicObjectManager objectManager = new DynamicObjectManager(clipping);
	public long instanceId;
	public int height;
	public List<Region> regions = new ArrayList<>();
	public static List<Player> normalWaiting = new ArrayList<>();
	public static List<Player> competetiveWaiting = new ArrayList<>();
	public long createTime;
	public List<LocationData> spawns;
	public int spawnsPos;

	public List<Player> players = new ArrayList<>();
	
	private Object cycleIntance = new Object();
	
	private CycleEventContainer processContainer = null;

	public int playerSize = 0;
	public int playerSizeAtStart = 0;
	
	public boolean isActive = false;
	
	public Map<Long, Boolean> chestsLooted = new HashMap<>();
	
	public boolean is24PeopleGame = false;
	public boolean hasMetMinReq = false;
	
	public Mode currentGameMode = Mode.NORMAL;
	
	private Item reward = null;
	private Item participationReward = null;
	
	private static TickTimer staticWatch = new TickTimer();
	private static final int REWARD_AUTO_START_WAIT = 1000; //10 minutes
	
	private int cofferRewardAmount = 0;

	private int fogRadius = -1;
	private int ticks;
	private int crateSpawnTick;
	private int crateAnnouncementRemoveTick;
	private int crateSpawnTicksStep;

	private int averageWinRating = 800;

	private static int MOSER_SETTLEMENT = 1;
	private static int DEBTOR_HIDEOUT = 2;
	private static int NEW_PLACE = 3;
	private static int TRINITY_OUTPOST = 4;
	private static int MIDDLE = 5;
	
	private static final Location LMS_WAITING_ROOM = Location.create(3411 + Misc.random(3), 3181 + Misc.random(5));
//	private static final Location LMS_LOBBY = Location.create(9803 + Misc.random(4), 3173 + Misc.random(4));
	
	public static Item normalReward = null;
	
	public static Item PARTICIPATION_REWARD = null;
	public static int GAMES_TO_REWARD_AMOUNT = 0;
	
	public static Location getLobbyRandom() {
		return Location.create(3403 + Misc.random(4), 3173 + Misc.random(4));
	}
	
	private static Location getWaitingRoomRandom() {
		return Location.create(3411 + Misc.random(3), 3181 + Misc.random(5));
	}
	
	private static final int MAX_PLAYERS = 24;
	private static final int MIN_PLAYERS = 12;
	
	private static final int START_INTERVAL = 500; // 500 = 5 minutes

	public static Mode serverWideMode = Mode.NORMAL;
	public static int eventStartTick;
	public static final int EVENT_TICKS_TO_START = 6000;

	public static enum Mode {
		NORMAL,
		COMPETETIVE,
		EVENT;
	}

	public static enum FogData {
		LOCATION_1(1832, 3859, "Mountain."),
		LOCATION_2(1874, 3805, "Moser Settlement."),
		LOCATION_3(1895, 3887, "Trinity Outpost."),
		LOCATION_4(1805, 3819, "Debtor Hideout.");
		
		private static final FogData[] values = values();
		private final int safeZoneX;
		private final int safeZoneY;
		private final String place;
		
		private FogData(int safeZoneX, int safeZoneY, String place) {
			this.safeZoneX = safeZoneX;
			this.safeZoneY = safeZoneY;
			this.place = place;
		}
		
		public int getSafeZoneX() {
			return safeZoneX;
		}
		
		public int getSafeZoneY() {
			return safeZoneY;
		}
		
		public String getPlace() {
			return place;
		}
	}
	
	public static enum LocationData {
		LOCATION_1(Location2D.create(1896, 3798), MOSER_SETTLEMENT, Location2D.create(1893, 3805), Location2D.create(1893, 3810)),
		LOCATION_2(Location2D.create(1885, 3785), MOSER_SETTLEMENT, Location2D.create(1890, 3784), Location2D.create(1895, 3784)),
		LOCATION_3(Location2D.create(1880, 3782), MOSER_SETTLEMENT, Location2D.create(1877, 3785), Location2D.create(1885, 3781)),
		LOCATION_4(Location2D.create(1896, 3794), MOSER_SETTLEMENT, Location2D.create(1897, 3786), Location2D.create(1902, 3784)),
		LOCATION_5(Location2D.create(1905, 3799), MOSER_SETTLEMENT, Location2D.create(1911, 3806), Location2D.create(1909, 3794)),
		LOCATION_6(Location2D.create(1802, 3793), DEBTOR_HIDEOUT, Location2D.create(1809, 3793), Location2D.create(1799, 3786)),
		LOCATION_7(Location2D.create(1793, 3798), DEBTOR_HIDEOUT, Location2D.create(1797, 3803), Location2D.create(1806, 3803)),
		LOCATION_8(Location2D.create(1830, 3800), DEBTOR_HIDEOUT, Location2D.create(1824, 3800), Location2D.create(1830, 3795)),
		LOCATION_9(Location2D.create(1838, 3787), DEBTOR_HIDEOUT, Location2D.create(1846, 3787), Location2D.create(1853, 3790)),
		LOCATION_10(Location2D.create(1819, 3811), DEBTOR_HIDEOUT, Location2D.create(1826, 3808), Location2D.create(1826, 3816)),
		LOCATION_11(Location2D.create(1803, 3857), NEW_PLACE, Location2D.create(1809, 3857), Location2D.create(1811, 3851)),
		LOCATION_12(Location2D.create(1796, 3849), NEW_PLACE, Location2D.create(1797, 3841), Location2D.create(1805, 3842)),
		LOCATION_13(Location2D.create(1807, 3871), NEW_PLACE, Location2D.create(1797, 3893), Location2D.create(1798, 3899)),
		LOCATION_14(Location2D.create(1809, 3876), NEW_PLACE, Location2D.create(1804, 3899), Location2D.create(1810, 3899)),
		LOCATION_15(Location2D.create(1812, 3868), NEW_PLACE, Location2D.create(1810, 3896), Location2D.create(1819, 3892)),
		LOCATION_16(Location2D.create(1893, 3869), TRINITY_OUTPOST, Location2D.create(1889, 3867), Location2D.create(1899, 3871)),
		LOCATION_17(Location2D.create(1882, 3872), TRINITY_OUTPOST, Location2D.create(1882, 3877), Location2D.create(1879, 3867)),
		LOCATION_18(Location2D.create(1884, 3893), TRINITY_OUTPOST, Location2D.create(1877, 3896), Location2D.create(1880, 3900)),
		LOCATION_19(Location2D.create(1911, 3901), TRINITY_OUTPOST, Location2D.create(1905, 3900), Location2D.create(1916, 3899)),
		LOCATION_20(Location2D.create(1911, 3877), TRINITY_OUTPOST, Location2D.create(1910, 3873), Location2D.create(1901, 3878)),
		LOCATION_21(Location2D.create(1914, 3886), TRINITY_OUTPOST, Location2D.create(1916, 3889), Location2D.create(1917, 3885)),
		LOCATION_22(Location2D.create(1884, 3889), TRINITY_OUTPOST, Location2D.create(1889, 3885), Location2D.create(1881, 3882)),
		LOCATION_23(Location2D.create(1865, 3845), MIDDLE, Location2D.create(1859, 3848), Location2D.create(1866, 3838)),
		LOCATION_24(Location2D.create(1832, 3831), MIDDLE, Location2D.create(1838, 3827), Location2D.create(1828, 3841));

		public static final List<LocationData> values = new ArrayList<>();
		private final Location2D spawnPos;
		private final int place;
		private final Location2D[] extraLootLocations;

		private LocationData(Location2D spawnPos, int place, Location2D... extraLootLocations) {
			this.spawnPos = spawnPos;
			this.place = place;
			this.extraLootLocations = extraLootLocations;
		}

		public Location2D getSpawnPos() {
			return spawnPos;
		}
		
		public int getPlace() {
			return place;
		}

		public Location2D[] getExtraLootLocations() {
			return extraLootLocations;
		}

	    public static void load() {
	    	/* empty */
	    }
	    
		static {
			for (LocationData data : values()) {
				values.add(data);
			}
		}
	}
	
	FogData data = FogData.values[Misc.randomNoPlus(FogData.values.length)];

	public LmsManager() {
		this.instanceId = uid++;
		height = (int) ((instanceId + 1) * 4);
	}

	public void start() {
		spawns = new ArrayList<>(LocationData.values);
		Collections.shuffle(spawns);

		isActive = true;
		createTime = System.currentTimeMillis();
		instances.put(createTime, this);
		objectManager.setDefsVersion(ObjectDefVersion.OSRS_DEFS); // set osrs object defs

		int startX = 1792;
		int startY = 3776;

		MapTools.copyStaticData(startX, startY, 128, 128, height, objectManager.getStaticObjects(), objectManager.getSpawnedObjects(), clipping, ObjectDefVersion.OSRS_DEFS, 0, 1);
	}

	public void spawnExtraLoot(boolean eventGame) {
		if (!eventGame) {
			return;
		}

		if (playerSizeAtStart <= MAX_PLAYERS) {
			return;
		}

		//Obtain how many extra players we have in the game
		int playerDelta = playerSizeAtStart - MAX_PLAYERS;

		//Check how many loops we'll need to do, each extra player gets 2 chests near the spawn.
		//There are 8 chests around the spawn point, 
		int newIndexPlayers = MAX_PLAYERS * 4;
		//+ 1 so loop actually works out lol.
		int loops = (playerDelta / newIndexPlayers) + 1;

		//Remainder.
		playerDelta %= newIndexPlayers;

		for (int i = 0; i < loops; i++) {
			for (int j = 0; j < playerDelta; j++) {
				int spawnRotation = (j / MAX_PLAYERS) * 2;
				int spawnPos = j % MAX_PLAYERS;
				LocationData locationData = spawns.get(spawnPos);

				for (int k = 0; k < 2; k++) {
					Direction direction = Direction.get(spawnRotation + k);
					Location2D chestBase = locationData.getExtraLootLocations()[Math.min(i, locationData.getExtraLootLocations().length - 1)].transform(direction);
					GameObject gameObject = GameObject.createTickObject(LmsArenaObjects.CHEST_OBJ, chestBase.toLocation(height), Direction.normalize(direction).toInteger(), 10, Integer.MAX_VALUE, -1);
					objectManager.addObject(gameObject);
				}
			}
		}
	}

	public void startCycleEvent() {
		processContainer = CycleEventHandler.getSingleton().addEvent(cycleIntance, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!isActive) {
					container.stop();
					return;
				}
				if (playerSize <= 1) {
					container.stop();
					if (players.size() > 0) {
						try {
							Player p = players.get(0);
							if (p != null && p.isActive) {
								rewardWinner(p);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (isActive) // in case something happens? dunno
						destroyLMS();
					return;
				}
				
				ticks++;
				
				if (ticks > 119) {
					String unit = Long.toString(TimeUnit.SECONDS.toMinutes(ticks * 600 / 1000));
					executeForPlayers(player -> {
						player.getPacketSender().sendString(unit, 66121);
					});
				}
				if (ticks == 100) {//100 = 1 min
					String announceMsg = "Lethal fog will soon fill the island! The safezone is the " + data.getPlace();
					String chatMsg = "<shad=1><col=6B8E23>Broadcast: </shad><col=000000>Lethal fog will soon fill the island! The safezone is the " + data.getPlace();
					executeForPlayers(player -> {
						player.getPacketSender().updateAnnouncement(announceMsg);
						player.sendMessage(chatMsg);
						ZoneManager.startArrow(player, data.safeZoneX, data.safeZoneY, 10, 2);
						player.getPacketSender().sendString("Approaching", 66119);
					});
				}
				if (ticks == 300) {
					fogRadius = FOG_INIT_RADIUS;
				}

				handleCrate();

				if (fogRadius != -1) {
					if (fogRadius > FOG_MIN_RADIUS && ticks % FOG_DECREASE_TICKS == 0) {
						fogRadius -= FOG_DECREASE;
						if (fogRadius < FOG_MIN_RADIUS) {
							fogRadius = FOG_MIN_RADIUS;
						}

						executeForPlayers(player -> {
							sendFog(player, fogRadius);
						});
					}

					for (int i = 0, length = players.size(); i < length; i++) {
						Player player = players.get(i);
						if (!player.isActive) continue;

						int highest = Misc.getRadius(data.getSafeZoneX(), data.getSafeZoneY(), player.absX, player.absY);
						int damageMod = 1;
						int radius = fogRadius;
						if (lockUpstairs() && player.heightLevel > height) {
							radius = 0;
							damageMod = 3;
							sendFog(player, radius);
						}
						
						if (length == 4 && player.isResetCurseBoost() && ticks % 2 == 0) {
							player.dealDamage(new Hit(2, 0, -1));
							player.sendMessage("You are smited for not fighting!");
						}

						int radius1 = radius + 8;
						int radius2 = radius - 8;
						if (highest <= radius2) {
							player.lmsFogTicks = 0;
							//System.out.println("not in fog " + highest+":"+radius1+":"+radius2);
							player.getPacketSender().sendString("Approaching", 66119);
						} else if (highest >= radius1) {
							player.lmsFogTicks++;
							player.getPacketSender().sendString("Danger", 66119);
							if (player.lmsFogTicks % 2 == 0) {
								player.dealDamage(new Hit(getDmg(player), 0, -1));
								player.startLogoutDelay();
							}
							//System.out.println("danger zone fog " + highest+":"+radius1+":"+radius2);
						} else {
							highest -= radius2;
							//System.out.println("in safer fog " + highest+":"+radius1+":"+radius2);
							player.getPacketSender().sendString("Danger", 66119);
							if (highest <= FOG_SAFE_TILES) {
								player.lmsFogTicks = 0;
							} else {
								player.lmsFogTicks++;
								if (player.lmsFogTicks % 2 == 0) {
									player.dealDamage(new Hit(getDmg(player) * damageMod, 0, -1));
									player.startLogoutDelay();
								}
							}
						}
					}
				}
			}
		}, 1);
	}

	public void executeForPlayers(Consumer<Player> consumer) {
		for (int i = 0, length = players.size(); i < length; i++) {
			Player player = players.get(i);
			if (!player.isActive) {
				continue;
			}

			consumer.accept(player);
		}
	}

	private void calcCrateSpawn() {
		if (currentGameMode == Mode.EVENT) {
			int playersOverflow = playerSizeAtStart - MAX_PLAYERS;
			crateSpawnTicksStep = Misc.interpolateSafe(200, 100, 0, 50, playersOverflow);
			crateSpawnTick = 100;
		} else {
			crateSpawnTicksStep = 200;
			crateSpawnTick = 250;
		}
	}

	private void handleCrate() {
		if (ticks == crateSpawnTick) {
			crateSpawnTick += crateSpawnTicksStep;
			crateAnnouncementRemoveTick = crateSpawnTick + (crateSpawnTicksStep / 2);

			LMSLocationData data = MagicalCrate.getRandomLMS();
			GameObject crate = GameObject.createTickObject(MagicalCrate.CRATE_OBJECT_ID, Location.create(data.getLMSSpawnPos().getX(), data.getLMSSpawnPos().getY(), height), 0, 10, 600, -1);
			objectManager.addObject(crate);
			String announcementMsg = "A magical loot crate has appeared near the " + data.getLMSClue() + "!";
			String gameMsg = "<shad=1><col=6B8E23>Broadcast: </shad><col=000000>A magical loot crate has appeared near the " + data.getLMSClue() + "!";

			executeForPlayers(player -> {
			    player.getPacketSender().updateAnnouncement(announcementMsg);
			    player.sendMessage(gameMsg);
			});
		}

		if (ticks == crateAnnouncementRemoveTick) {
			executeForPlayers(player -> {
				player.getPacketSender().updateAnnouncement("");
			});
		}
	}

	public boolean lockUpstairs() {
		return ticks >= 300;
	}

	private static int getDmg(Player player) {
		int ticks = player.lmsFogTicks;
		if (ticks > 0 && ticks < 7) {
			return 1;
		}
		if (ticks > 6 && ticks < 11) {
			return 2;
		}
		if (ticks > 10 && ticks < 37) {
			return 3;
		}
		if (ticks > 36) {
			if (player.underAttackBy > 0 || player.playerIndex > 0) {
				return 3;
			}
			return 10;
		}
		return 1;
	}

	public void sendFog(Player player, int radius) {
		player.getPacketSender().sendLmsFog(data.getSafeZoneX(), data.getSafeZoneY(), radius);
	}

	public static String getTimeRemainingInMinutes() {
		return Integer.toString(Misc.ticksToSeconds(START_INTERVAL - (Server.getTotalTicks() % START_INTERVAL))/60);
	}
	
	private static final String ALERT_MSG = "<img=9><col=4848b7> Next Last Man Standing game is starting in 2 minutes, hurry up! ::lms";

	public static void init() {
		Server.getTaskScheduler().schedule(new Task(1) {

			@Override
			protected void execute() {
				if (serverWideMode == Mode.EVENT) {
					int ticksPassed = Server.getTotalTicks() - eventStartTick;
					
					int ticksRemaining = EVENT_TICKS_TO_START - ticksPassed;
					
					switch (ticksRemaining) {
					case 1000:
						PlayerHandler.messageAllPlayers("<col=800000>Mass LMS will start in 10 minutes! ::lms");
						break;
					}
					
					if (ticksPassed >= EVENT_TICKS_TO_START) {
						startGame(Mode.EVENT);
					}

					return;
				}

				if (competetiveWaiting.size() >= MAX_PLAYERS) {
					startGame(Mode.COMPETETIVE);
				}

				if (normalWaiting.size() >= MAX_PLAYERS) {
					startGame(Mode.NORMAL);
				}
				
				//if game didn't start but meets min req then start every 5 minutes.
				if (normalReward == null && Server.getTotalTicks() >= START_INTERVAL) {
					int cycle = Server.getTotalTicks() % START_INTERVAL;
					if (cycle == 0) {
						if (normalWaiting.size() >= MIN_PLAYERS) {
							startGame(Mode.NORMAL);
						}
					}
					
					if (cycle == 300 && normalWaiting.size() >= MIN_PLAYERS-2) {
						PlayerHandler.messageAllPlayers(ALERT_MSG);
					}
				} else if (normalReward != null) {
					if (staticWatch.complete()) {
						if (normalWaiting.size() >= MIN_PLAYERS) {
							startGame(Mode.NORMAL);
						} else {
							staticWatch.startTimer(REWARD_AUTO_START_WAIT);
						}
					}
				}
				
			}
		});
	}
	
	public static void updateLobbyInterface(Mode mode, Player player) {
		player.getPacketSender().sendConfig(ConfigCodes.LMS_BOX, 0);//either 1 or 0

		player.getPacketSender().sendString("Rank:", 66114);
		player.getPacketSender().sendString("Mode:", 66116);

		player.getPacketSender().sendString("Noob", 66117);
		player.getPacketSender().sendString("None", 66119);

		player.getPacketSender().sendString("", 66120);
		player.getPacketSender().sendString("", 66121);
		
		if (mode == Mode.NORMAL || mode == Mode.EVENT) {
			int len = normalWaiting.size();
			String playersText;
			String coffersText;
			String coffersValue;
			if (mode == Mode.NORMAL) {
				coffersText = "Coffer:";
				playersText = "Normal (" + len + "/" + MAX_PLAYERS + ")";
				coffersValue = Integer.toString(player.getLmsCoffer());
			} else {
				coffersText = "Time left:";
				playersText = "Event ( " + len + " )";
				int ticks = EVENT_TICKS_TO_START - (Server.getTotalTicks() - eventStartTick);
				coffersValue = "   " + Misc.ticksToTime(Math.max(ticks, 0), false);
			}

			player.getPacketSender().sendString(coffersText, 66115);
			player.getPacketSender().sendString(coffersValue, 66118);

			for (int i = 0; i < len; i++) {
				Player p = normalWaiting.get(i);
				if (!p.isActive) {
					continue;
				}

				p.getPacketSender().sendString(playersText, 66119);
			}
		} else if (mode == Mode.COMPETETIVE) {
			player.getPacketSender().sendString("Coffer:", 66115);
			player.getPacketSender().sendString(Integer.toString(player.getLmsCoffer()), 66118);

			int len = competetiveWaiting.size();
			String text = "Competitive (" + len + "/" + MAX_PLAYERS + ")";

			for (int i = 0; i < len; i++) {
				Player p = competetiveWaiting.get(i);
				if (!p.isActive) {
					continue;
				}
				
				p.getPacketSender().sendString(text, 66119);
				
			}
		}

		player.getPA().walkableInterface(66110);
	}
	
	public static void updateArenaInterface(Player player) {

		player.getPacketSender().sendConfig(ConfigCodes.LMS_BOX, 1);//either 1 or 0
		
		player.getPacketSender().sendString("Survivors:", 66114);
		player.getPacketSender().sendString("Kills:", 66115);
		player.getPacketSender().sendString("Fog:", 66116);
		
		updateSurvivorsString(player);
		updateKillsPerGameInterface(player);
		player.getPacketSender().sendString("Safe", 66119);
		
		player.getPacketSender().sendString("mins", 66120);
		player.getPacketSender().sendString("0", 66121);
		//player.getPA().walkableInterface(66110);
	}
	
	private static void updateSurvivorsString(Player p) {
		p.getPacketSender().sendString(Integer.toString(p.lmsManager.playerSize), 66117);
	}
	
	public static void updateKillsPerGameInterface(Player p) {
		p.getPacketSender().sendString(Integer.toString(p.lmsKillsPerGame), 66118);
		
	}
	
	public static void createLMSNormalEvent(Item item, final String itemAmountStr, final String itemName) {
		LmsManager.GAMES_TO_REWARD_AMOUNT = 1;
		
		LmsManager.normalReward = item;
		
		staticWatch.startTimer(REWARD_AUTO_START_WAIT);
		
		AnnouncementHandler.announce("Announcement: Normal LMS event has started winner reward " + itemAmountStr + " " + itemName + "!");
		PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000> Normal LMS event has started winner reward " + itemAmountStr + " " + itemName + "!");
	}
	
	public static void startGame(final Mode mode) {
		boolean eventGame = false;

		final LmsManager lms = new LmsManager();

		int averageRating = 0;
		
		lms.start();
		
		lms.currentGameMode = mode;

		List<Player> modeList = null;
		int size = 0;
		if (mode == Mode.NORMAL || mode == Mode.EVENT) {
			modeList = normalWaiting;
			size = normalWaiting.size();
			if (size == 0) {
				lms.destroyLMS();
				return;
			}
			if (mode == Mode.NORMAL) {
				lms.reward = normalReward;
			} else {

				if (OfficialTournamentVariables.lmsParticipationItemId > 0)
					lms.participationReward = new Item(OfficialTournamentVariables.lmsParticipationItemId, OfficialTournamentVariables.lmsParticipationItemAmount);
				if (OfficialTournamentVariables.lmsFirstPlaceItemId > 0)
					lms.reward = new Item(OfficialTournamentVariables.lmsFirstPlaceItemId, OfficialTournamentVariables.lmsFirstPlaceItemAmount);
					

				eventGame = true;
				serverWideMode = Mode.NORMAL;
			}

			if (lms.participationReward == null)
				lms.participationReward = PARTICIPATION_REWARD;
			if (GAMES_TO_REWARD_AMOUNT > 0) {
				GAMES_TO_REWARD_AMOUNT--;
				if (GAMES_TO_REWARD_AMOUNT == 0) {
					if (normalReward != null && mode == Mode.NORMAL) {
						normalReward = null;
					}

					PARTICIPATION_REWARD = null;
					AnnouncementHandler.announce("Announcement: LMS event has just ended!");
	    			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>LMS event has just ended!");
				}
			}
		} else if (mode == Mode.COMPETETIVE) {
			modeList = competetiveWaiting;
			size = competetiveWaiting.size();
			if (size == 0) {
				lms.destroyLMS();
				return;
			}
		}
		
		for (int i = 0; i < size; i++) {
			final Player player = modeList.get(i);
			
			if (player.isNotReadyForMinigame() || !player.inLMSLobby()) {
				continue;
			}

			player.getPA().closeAllWindows();
			
	    	lms.addPlayerToArena(player, mode);
	    	player.startLogoutDelay();
	    	
	    	player.getPacketSender().sendDiscordUpdate(mode.toString(), "Last Man Standing", 1, size, System.currentTimeMillis()/1000, 0);
	    	
	    	player.getPacketSender().playSound(Sounds.UNKNOWN_LMS_START);
	    	
	    	int playerWinRating = player.getExtraHiscores().getLmsWinRating();
	    	
	    	if (playerWinRating == 0) {
	    		playerWinRating = EloRatingSystem.DEFAULT_ELO_START_RATING;
	    		player.getExtraHiscores().setLmsWinRating(playerWinRating);
	    	}
	    	
	    	if (player.getExtraHiscores().getLmsKillRating() == 0) {
	    		player.getExtraHiscores().setLmsKillRating(EloRatingSystem.DEFAULT_ELO_START_RATING);
	    	}
	    	
	    	averageRating += playerWinRating;
	    	
	    	lms.kickAFKer(player);
	    	
			player.performingAction = true;
	
	    	CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
	    		int timer = 8;
				@Override
				public void execute(CycleEventContainer container) {
					timer--;
					
					if (!player.isInLmsArena()) {
						container.stop();
						return;
					}
					
					if (timer >= 6) {
						updateArenaInterface(player);
					}
					if (timer <= 0) {
						container.stop();
						player.forceChat("GO!");
						player.sendMessage("The grace period has begun!");
					} else if (timer < 6) {
						player.forceChat(Integer.toString(timer));
					}
			    }

			    @Override
			    public void stop() {
					player.performingAction = false;
			    }
		    }, 1);
	    	
		}

		modeList.clear();

		lms.hasMetMinReq = lms.playerSize >= MIN_PLAYERS;
		lms.is24PeopleGame = lms.playerSize == MAX_PLAYERS;
		lms.playerSizeAtStart = lms.playerSize;
		
		lms.setAverageWinRating(averageRating / size);

		lms.startCycleEvent();
		lms.spawnExtraLoot(eventGame);
		lms.calcCrateSpawn();
	}
	
	public void kickAFKer(final Player p) {
		if (p.isIdle) {
			CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
				
				@Override
				public void stop() {
					/* empty */
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					if (p.lmsManager == null) {
						container.stop();
						return;
					}

					if (p.isIdle) {
						removePlayerFromArena(p, true);
						container.stop();
					}
				}
			}, 30);
		}
	}
	
	public void addPlayerToArena(final Player p, Mode mode) {
		if (spawnsPos >= spawns.size()) {
			spawnsPos = 0;
		}

		LocationData data = spawns.get(spawnsPos++);
		p.getPA().movePlayer(data.spawnPos.getX(), data.getSpawnPos()
				.getY(), height);
		p.increaseLMSGamesPlayed();
		
		p.getPA().restorePlayer(true); // reset pots and other shit
		addStarterKit(p);
		setSkills(p);
		
		p.dynObjectManager = this.objectManager;
    	p.setDynamicRegionClip(this.clipping);
    	p.lmsManager = this;
    	
    	p.lmsDamageMap.reset();
    	
    	CombatPrayer.resetPrayers(p);
    	
    	p.lmsKillsPerGame = 0;
    	p.lmsRoundPlacement = MAX_PLAYERS;
    	p.lmsPointsPerRound = 0;
    	p.usingSOLSpec = false;
    	p.previousMagicBook = p.playerMagicBook;
    	p.setSpellbook(SpellBook.NORMAL);
    	p.previousPrayerBook = p.playerPrayerBook;
    	p.playerPrayerBook = PrayerBook.NORMAL;
    	p.updatePrayerBook();
		p.getAchievement().play30LMS();

		players.add(p);
		playerSize++;

		if (mode == Mode.COMPETETIVE) {
			int amount = 20_000_000;
			this.cofferRewardAmount += amount;
			p.setLmsCoffer(p.getLmsCoffer() - amount);
		}
	}
	
	public void decreaseSize() { // decrease size on logout and death
		playerSize--;
		if (playerSize <= 0) {
			return;
		}

		for (int i = 0, size = players.size(); i < size; i++) {
			Player p = players.get(i);
			if (p.isActive || !p.disconnected) {
				updateSurvivorsString(p);
			}
		}
	}
	
	public void removePlayerFromArena(final Player p, boolean telePlayer) {
		if (telePlayer) {
			p.getItems().deleteAllItems();
			p.getPA().movePlayer(getLobbyRandom());
		}
		if (p.lmsManager.isActive && p.lmsManager.participationReward != null) {
			Item reward = p.lmsManager.participationReward;
			LmsRewards.addReward(p, new LmsRewards(reward, p.mySQLIndex));
			p.sendMessage("Participation reward has been added to the reward chest!");
		}
		
		//win rating
		if (this.hasMetMinReq) {
			final int previousRating = p.getExtraHiscores().getLmsKillRating();
			
			//start from 0.2?
			final double normalizedValue = (double) Misc.interpolateSafe(11, 2, 1, this.playerSizeAtStart, p.lmsRoundPlacement) / 10.0d;
			
			final int newRating = EloRatingSystem.getNewRating(previousRating, this.averageWinRating, normalizedValue);
			p.getExtraHiscores().setLmsWinRating(newRating);
			
			p.sendMessage(String.format("Win Rating change:", newRating - previousRating));
			
			final int goldReward = (int)Math.round(p.lmsRoundPlacement <= 5 ? GOLD_TO_REWARD_TOP_5 : GOLD_TO_REWARD_PARTICIPATION * normalizedValue);
			final Item item = new Item(995, goldReward);
			LmsRewards.addReward(p, new LmsRewards(item, p.mySQLIndex));
		}

		//apply lms points for placement
		switch (p.lmsRoundPlacement) {
		case 1:
			p.lmsPointsPerRound += FIRST_PLACE;
			break;
		case 2:
			p.lmsPointsPerRound += SECOND_PLACE;
			break;
		case 3:
			p.lmsPointsPerRound += THIRD_PLACE;
			break;
		case 4:
			p.lmsPointsPerRound += FOURTH_PLACE;
			break;
		case 5:
			p.lmsPointsPerRound += FIFTH_PLACE;
			break;
			default:
				final int scalingPoints = Misc.interpolateSafe(THE_REST, 10, 6, this.playerSizeAtStart, p.lmsRoundPlacement);
				p.lmsPointsPerRound += scalingPoints;
				break;
		}
		
		p.sendMessage(String.format("You had %d kills and your placement was %d", p.lmsKillsPerGame, p.lmsRoundPlacement));
		p.setDead(false);
		p.lmsManager = null;
		p.setSpawnedSkills(null);
    	p.calcCombat();
		p.setDynamicRegionClip(null);
		p.dynObjectManager = null;
		p.getPacketSender().resetFog();
		p.getPacketSender().updateAnnouncement("");
		p.getPacketSender().resetArrow();
		p.getSkills().restore();
		p.getPacketSender().showOption(3, 0, "Null");
		p.getCombat().resetPrayers();
		p.getCombat().getPlayerAnimIndex(p.playerEquipment[PlayerConstants.playerWeapon]);
		p.playerPrayerBook = p.previousPrayerBook;
		p.updatePrayerBook();
		p.setSpellbook(p.previousMagicBook);

		applyActivityPoints(p);
		
		p.setDefaultDiscordStatus();
		
		p.getPacketSender().sendFrame126("Elo: "+p.getExtraHiscores().getLmsWinRating(), 6839);
		p.getPacketSender().sendFrame126(String.format("%d kills", p.lmsKillsPerGame), 6840);
		p.getPacketSender().showInterface(6733);
		if (!p.lmsRewardsToClaim.isEmpty()) { //display the rewards
			p.getPacketSender().createObjectHints(3403, 3185, 0, 2);
			p.getPacketSender().itemsOnInterface(6822, p.lmsRewardsToClaim);
			p.lmsRewardsToClaim.clear();
			p.sendMessage("Rewards have been added to the chest!");
		} else {
			p.getPacketSender().resetInvenory(6822);
		}

		if (isActive) { // when player dies, he will be removed from lms but if last to die, lms is destroyed already
			players.remove(p);
			decreaseSize();
		}
	}

	public void removePlayerFromLMSDamageMaps(int sqlId) {
		executeForPlayers(player -> {
			 player.getDamageMap().removeFromDamageMap(sqlId);
		});
	}

	private void applyActivityPoints(final Player p) {
		for (Entry<Integer, TotalDamage> entry : p.lmsDamageMap.getTotalDamages().entrySet()) {
			TotalDamage damage = entry.getValue();
			
			p.lmsPointsPerRound += damage.getDamage();
			
		}
		
		if (p.lmsPointsPerRound > 0) {
			int extraPoints = 0;
			if (EXTRA_REWARD)
				extraPoints = (int)Math.round(p.lmsPointsPerRound * 0.5);
			p.lmsPoints += p.lmsPointsPerRound + extraPoints;
			if (extraPoints > 0)
				p.sendMessage(String.format("You have earned %d Extra LmsPoints from the event.", extraPoints));
			p.sendMessage(String.format("You have earned %d LmsPoints this round. Total Points: %d", p.lmsPointsPerRound, p.lmsPoints));
		}
	}
	
	public static Item FRYING_PAN = new Item(7441, 1);
	
	public void rewardWinner(final Player p) {
		if (reward != null) {
			if (p.isIronMan() && currentGameMode != Mode.EVENT) {
				//if ironman mode account wins lms, drop the item outside of LMS lobby
				int x = LocationConstants.LMS.getX() + Misc.newRandom(4);
				int y = LocationConstants.LMS.getY() + Misc.newRandom(4);
				int z = LocationConstants.LMS.getZ();
				ItemHandler.createGroundItem(new GroundItem(reward.getId(), x, y, z, reward.getAmount(), ItemHandler.HIDE_TICKS*2), true);
			} else {
				LmsRewards.addReward(p, new LmsRewards(reward, p.mySQLIndex));
			}
			if (is24PeopleGame)
				PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + p.getNameSmartUp() + " has just won the LMS event!");
			p.sendMessage("You are victorious! Rewards have been added to the reward chest!");
			
			if (currentGameMode == Mode.EVENT) {
				LmsRewards.uploadWinnerToWebsite(p.mySQLIndex, reward.getId(), reward.getAmount(), playerSizeAtStart);
			}
		}
		if (currentGameMode == Mode.EVENT) {
			LmsRewards.addReward(p, new LmsRewards(FRYING_PAN, p.mySQLIndex));
		}
		if (hasMetMinReq) {
			p.getExtraHiscores().incLmsWins();
			p.getAchievement().increaseProgress(Achievs.WIN_1_GAME_OF_LMS);
		}
		
		if (cofferRewardAmount > 0) {
			MoneyPouch.addToMoneyPouch(p, cofferRewardAmount);
			p.sendMessage(String.format("You have won %s gold! It has been put into money pouch.", Misc.formatNumbersWithCommas(this.cofferRewardAmount)));
			cofferRewardAmount = 0;
		}

		p.lmsRoundPlacement = 1;
		removePlayerFromArena(p, true);
		destroyLMS();
		p.sendMessage("You have won the Last Man Standing game!");
	}
	
	public void destroyLMS() {
		this.isActive = false;
		instances.remove(this.createTime);
		if (this.processContainer != null)
			this.processContainer.stop();
		this.processContainer = null;
		this.players.clear();
		this.players = null;
		this.objectManager.cleanup();
		this.objectManager = null;
		this.clipping.cleanup();
		this.clipping = null;
		this.regions.clear();
		this.regions = null;
		this.chestsLooted.clear();
		this.chestsLooted = null;
	}
	
    private static void addStarterKit(Player player) {
    	int STARTER_SPEAR = 6526;
    	
    	player.getItems().setEquipment(STARTER_SPEAR, 1, PlayerConstants.playerWeapon);
    	player.getItems().refreshWeapon();

	}
    
    private static void setSkills(Player player) {
    	player.setSpawnedSkills(new Skills(player));
    	player.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
    	player.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
    	player.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
    	player.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, 70);
    	player.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
    	player.getSpawnedSkills().setStaticLevel(Skills.PRAYER, 99);
    	player.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
    	player.getSpawnedSkills().setStaticLevel(Skills.AGILITY, 99);
    	player.getSpawnedSkills().setLevel(Skills.PRAYER, 10);
    	player.calcCombat();
    }
	
	public static void addToWaitRoom(Player player, Mode mode) {
		if (!player.getSpamTick().checkThenStart(1)) {
			return;
		}
		
		if (!MinigameRules.canJoinMinigame(player))
			return;
		
		if (player.summoned != null) {
			player.sendMessage("You can't bring any pets into Last Man Standing.");
			return;
		}
		
		if (normalWaiting.contains(player)) {
			removePlayerWaitingRoom(player);
		} else if (competetiveWaiting.contains(player)) {
			removePlayerWaitingRoom(player);
		}
			
		boolean sameUUID = false;
		for (Player p : Server.getRegionManager().getRegionByLocation(LMS_WAITING_ROOM).getPlayers()) {
																								
			if (p == null || p.disconnected || p.forceDisconnect) {
				continue;
			}
			
			if (!RSConstants.inLMSWaitingRoom(p.getX(), p.getY())) {
				continue;
			}
			
			if (p.UUID.equals(player.UUID) ) {
				sameUUID = true;
				break;
			}
		}
		
		if (sameUUID && Config.RUN_ON_DEDI) {
			player.sendMessage("Sorry, but you can't enter the game with multiple accounts.");
			return;
		}
		
		if (mode == Mode.COMPETETIVE && player.getLmsCoffer() < 20_000_000) {
			player.getDialogueBuilder().sendNpcChat(Lisa.LISA_NPC_ID, Expression.DEFAULT_OLD, "You need to have at least 20 Million coins in the", "coffer to participate in the competetive mode.").executeIfNotActive();
			return;
		}
		
		for (int j = 0, length = player.playerItems.length; j < length; j++) {
			int id = player.playerItems[j] - 1;
			if (id != -1) {
				player.getDialogueBuilder().sendNpcChat(Lisa.LISA_NPC_ID, Expression.DEFAULT_OLD, "You can't carry any items with you in the", "Last Man Standing.").executeIfNotActive();
				return;
			}
		}
		for (int j = 0, length = player.playerEquipment.length; j < length; j++) {
			int id = player.playerEquipment[j];
			if (id != -1) {
				player.getDialogueBuilder().sendNpcChat(Lisa.LISA_NPC_ID, Expression.DEFAULT_OLD, "You are wearing items that cannot be taken into the", "Last Man Standing.").executeIfNotActive();
				return;
			}
		}
		
		if (((Client)player).getTradeAndDuel().hasItemsInTradeOrDuel()) {
			player.getDialogueBuilder().sendNpcChat(Lisa.LISA_NPC_ID, Expression.DEFAULT_OLD, "You can't carry any items with you in the", "Last Man Standing.").executeIfNotActive();
			return;
		}
		
		enterWaitingRoom(player, mode);
	}
	
	public static void enterWaitingRoom(Player player, Mode mode) {
		player.getPA().movePlayer(getWaitingRoomRandom());
		player.getDotManager().reset();
		player.getPotions().resetOverload();
		if (player.prayerRenew > 1)
			player.prayerRenew = 1;
		player.sendMessage("You have been added to the <col=13434880>" + mode + "</col> mode.");
		
		if (mode == Mode.NORMAL || mode == Mode.EVENT) {
			normalWaiting.add(player);
			updateLobbyInterface(mode, player);
		} else if (mode == Mode.COMPETETIVE) {
			competetiveWaiting.add(player);
			updateLobbyInterface(mode, player);
		}
		
		player.getPacketSender().sendDiscordUpdate("Lobby: "+mode.toString(), "Last Man Standing", 0, 0, System.currentTimeMillis()/1000, 0);
	}
	
	public static void removePlayerWaitingRoom(Player player) {
		boolean removed = normalWaiting.remove(player);
		if (!removed) {
			removed = competetiveWaiting.remove(player);
			updateLobbyInterface(Mode.COMPETETIVE, player);
		} else { // removed from normal
			updateLobbyInterface(serverWideMode, player);
		}

		player.getPacketSender().createPlayerHints(10, -1);
		player.getPacketSender().sendString("None", 66119);
		player.getPA().movePlayer(getLobbyRandom());
		player.setDefaultDiscordStatus();
	}
	
	public static int getNormalPlayersWaiting() {
		return normalWaiting.size();
	}
	
	public static int getCompetetivePlayersWaiting() {
		return competetiveWaiting.size();
	}

	public int getAverageWinRating() {
		return averageWinRating;
	}

	public void setAverageWinRating(int averageWinRating) {
		this.averageWinRating = averageWinRating;
	}
	
	public static int hasLMSItemsOnLogin(Player p) {
		for (int i = 0; i < p.playerItems.length; i++) {
			int id = p.playerItems[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
		}
		for (int i = 0; i < p.playerShopItems.length; i++) {
			int id = p.playerShopItems[i];
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
		}
		for (int i = 0; i < p.playerEquipment.length; i++) {
			int id = p.playerEquipment[i];
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
		}
		for (int i = 0; i < p.bankItems0.length; i++) {
			int id = p.bankItems0[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems1[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems2[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems3[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems4[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems5[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems6[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems7[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
			id = p.bankItems8[i]-1;
			if (ShopPrice.getSurvivalShopItemValue(id) != 9999)
				return id;
		}
		
		return 0;
	}
	
	public static boolean OFFICIAL_ENABLED = true;

	public static void startMassLMSEvent() {
		if (!OFFICIAL_ENABLED)
			return;
		LmsManager.eventStartTick = Server.getTotalTicks();
		LmsManager.serverWideMode = Mode.EVENT;
		String rewardName = ItemProjectInsanity.getItemName(OfficialTournamentVariables.lmsFirstPlaceItemId);
		String amount = Misc.formatShortPrice(OfficialTournamentVariables.lmsFirstPlaceItemAmount);
		final String msg = "LMS Mass Event will start in 1 hour. Winner will get " + amount + " " + rewardName + ". To join type ::lms";
		AnnouncementHandler.announce(msg);
		PlayerHandler.messageAllPlayersInAllWorlds("<col=800000>" + msg, "");
		
		String partRewardName = ItemProjectInsanity.getItemName(OfficialTournamentVariables.lmsParticipationItemId);
		if (partRewardName == null)
			partRewardName = "NOTHING";
		final String partAmount = Misc.formatShortPrice(OfficialTournamentVariables.lmsParticipationItemAmount);
		
		final String partMsg = "<col=800000>LMS Event will give " + partAmount + " " + partRewardName + " to anyone who participates.";
		
		PlayerHandler.messageAllPlayers(partMsg);
	}
	
}
