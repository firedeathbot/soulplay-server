package com.soulplay.content.minigames;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class FightPitsTournament {

	static Player[] players = new Player[200];

	public static boolean nextGameTournament = false;

	static boolean gameQueueOn = false;

	static boolean officialTourney = false;

	static boolean gameOn = false;

	public static final int PLAYERS_REQUIRED = 10;

	public static final int WAIT_TIME = 420;

	public static final int TOURNEY_WAIT_TIME = 840;

	public static final int FIGHT_TIME = 1200;

	public static int waitTime = 0;

	public static int gameTime = 0;
	// public static int maxBonuses = 1200;

	public static boolean FFA = true;

	public static final int[] tournyRewards = {13911, 13917, 13908, 13914,
			13920, 13944, 13947, 13950, 13932, 13935, 13938, 4708, 4710, 4712,
			4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728, 4730, 4732, 4734,
			4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755, 4757, 4759, 13899,
			13905, 14876, 14877, 14878, 14879, 14880, 14881, 14882, 14883,
			14884, 14885, 14886, 14887, 14888, 14889, 14890, 14891, 14892};

	public static final int[] hybridItems = {21476, 21471, 21466};

	static boolean allowFight = false;

	static int playerCount = 0;

	public static boolean allowFight() {
		return allowFight;
	}

	public static void beginGame() {
		if (!gameQueueOn) {
			return;
		}
		gameOn = true;
		gameTime = FIGHT_TIME;
		Server.getTaskScheduler().schedule(new Task(true) {

			// int gameTime = 1200; //840
			int countDown = 10;

			boolean secondTik = false;

			@Override
			public void execute() {
				if (!gameQueueOn || gameTime < 1) {
					endGame();
					this.stop();
					return;
				}
				refreshPlayerCount();
				if (getPlayerCount() == 0) {
					endGame();
					this.stop();
					return;
				}
				if (getPlayerCount() == 1 && allowFight) {
					rewardWinner();
				}
				if (countDown > 0) {
					countDown--;
				}
				if (countDown == 1) {
					allowFight = true;
				}
				if (allowFight) {
					gameTime--;
				}

				secondTik = !secondTik;

				for (Player p : players) {
					if (p == null || p.disconnected) {
						continue;
					}
					final Client c = (Client) p;
					if (!RSConstants.fightPitsRoom(c.getX(), c.getY())) {
						forceKick(c);
						break;
					}
					if (countDown > 1 && secondTik) {
						c.forceChat("" + (countDown / 2));
					}
					if (countDown == 1) {
						c.forceChat("FIGHT");
					}
					if (allowFight) {
						if (!FFA && secondTik) {
							// c.sendMessage("target: "+c.tournamentTarget);
							// TODO: Grab targets here
							if (c.minigameTarget > 0) { // check if target is
														// dead, if so, find new
														// target
								if (PlayerHandler.players[c.minigameTarget] == null
										|| PlayerHandler.players[c.minigameTarget].isDead()
										|| !RSConstants.fightPitsRoom(
												PlayerHandler.players[c.minigameTarget]
														.getX(),
												PlayerHandler.players[c.minigameTarget]
														.getY())) {
									c.minigameTarget = 0;
									c.getPacketSender().createPlayerHints(10,
											-1);
								}
							}
							if (c.minigameTarget == 0) { // find new target
								pairUp(c);
								// c.sendMessage("Trying to pair up");
							}
						}

						if (officialTourney) {
							c.getPacketSender()
									.sendFrame126(
											"Fight Pits Tournament "
													+ (FFA ? "FFA" : "1v1"),
											6570);
						} else {
							c.getPacketSender().sendFrame126(
									"Fight Pits " + (FFA ? "FFA" : "1v1"),
									6570);
						}
						c.getPacketSender().sendFrame126(
								"Player Count: " + getPlayerCount(), 6572);
						c.getPacketSender().sendFrame126(
								"<col=ff00>" + (gameTime * 600 / 1000), 6664);

						// c.getPacketSender().sendFrame126("<col=ff00>"+(gameTime*600/1000),
						// 199);
					} else {
						// c.getPacketSender().sendFrame126("<col=ff00>"+(countDown*600/1000),
						// 199);
						if (officialTourney) {
							c.getPacketSender()
									.sendFrame126(
											"Fight Pits Tournament "
													+ (FFA ? "FFA" : "1v1"),
											6570);
						} else {
							c.getPacketSender().sendFrame126(
									"Fight Pits " + (FFA ? "FFA" : "1v1"),
									6570);
						}
						c.getPacketSender().sendFrame126(
								"Player Count: " + getPlayerCount(), 6572);
						c.getPacketSender().sendFrame126(
								"<col=ff00>" + (gameTime * 600 / 1000), 6664);
					}
					if (gameTime < 99 && !c.isDead() && c.getHitPoints() > 0) {
						int dmg = 1;
						dmg = c.dealDamage(new Hit(dmg, 0, -1));
					}
				}

			}
		});
	}

	public static void beginGameQueue() {

		if (Misc.random(99) <= 50) {
			FFA = false;
			if (officialTourney) {
				PlayerHandler.messageAllPlayers(
						"<col=800000>Fight Pits Tournament has been set to Single Combat Mode!");
			}
		} else {
			FFA = true;
			if (officialTourney) {
				PlayerHandler.messageAllPlayers(
						"<col=800000>Fight Pits Tournament has been set to Free For All!");
			}
		}

		// maxBonuses = 500 + Misc.random(3000);
		//
		// PlayerHandler.messageAllPlayers("<col=800000>Today's Max Gear Bonuses must
		// be below "+maxBonuses+". ::bonuses");
		if (officialTourney) {
			waitTime = TOURNEY_WAIT_TIME;
		} else {
			waitTime = WAIT_TIME;
		}
		Server.getTaskScheduler().schedule(new Task(true) {

			// int waitTime = 20;//600; 840
			@Override
			public void execute() {
				if (gameQueueOn && waitTime < 1) {
					this.stop();
					return;
				}
				if (!gameQueueOn || waitTime < 1) {
					this.stop();
					return;
				}
				refreshPlayerCount();

				if (waitTime > 0) {
					waitTime--;
				}

				if (waitTime == 500) {
					if (officialTourney) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fight Pits Tournament will start in 5 minutes!");
					}
				}
				if (waitTime == 200) {
					if (officialTourney) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fight Pits Tournament will start in 2 minutes!");
					}
				}
				if (waitTime == 20) {
					if (officialTourney) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fight Pits Tournament will start in 10 seconds!");
					}
				}
				if (waitTime == 6) {
					if (officialTourney) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fight Pits Tournament will start in 3 seconds!");
					}
				}
				if (waitTime == 4) {
					if (officialTourney) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fight Pits Tournament will start in 2 seconds!");
					}
				}
				if (waitTime == 2) {
					if (officialTourney) {
						PlayerHandler.messageAllPlayers(
								"<col=800000>Fight Pits Tournament will start in 1 seconds!");
					}
				}
				if (waitTime < 1) {
					if (getPlayerCount() < PLAYERS_REQUIRED) {
						if (officialTourney) {
							PlayerHandler.messageAllPlayers(
									"<col=800000>Not enough players in the Fight Pits.");
						}
						endGame();
					} else {
						if (officialTourney) {
							PlayerHandler.messageAllPlayers(
									"<col=800000>Fight Pits Tournament has started!");
						}
						beginGame();
					}
					this.stop();
					return;
				}

				for (Player p : players) {
					if (p == null || p.disconnected) {
						continue;
					}
					Client c = (Client) p;
					if (!RSConstants.fightPitsRoom(c.getX(), c.getY())) {
						forceKick(c);
						break;
					}

					if (officialTourney) {
						c.getPacketSender()
								.sendFrame126("Fight Pits Tournament "
										+ (FFA ? "FFA" : "1v1"), 6570);
					} else {
						c.getPacketSender().sendFrame126(
								"Fight Pits " + (FFA ? "FFA" : "1v1"), 6570);
					}
					c.getPacketSender().sendFrame126(
							"Player Count: " + getPlayerCount(), 6572);
					c.getPacketSender().sendFrame126(
							"<col=ff00>" + (waitTime * 600 / 1000) + " seconds",
							6664);

				}

			}
		});

	}

	public static void endGame() {
		for (int i = 0; i < players.length; i++) { // for(Player p : players) {
			if (players[i] == null) {
				continue;
			}
			final Client c = (Client) players[i];
			if (c == null || c.forceDisconnect || c.disconnected) {
				players[i] = null;
				continue;
			}
			forceKick(c);
		}
		players = new Player[200];
		playerCount = 0;
		gameQueueOn = false;
		allowFight = false;
		gameOn = false;
		if (officialTourney) {
			officialTourney = false;
		}
		if (nextGameTournament) {
			openTournament();
			nextGameTournament = false;
		} else {
			startProcess();
		}
	}

	public static boolean enterGame(Client c) {
		if (!gameQueueOn) {
			c.sendMessage("The pits are not open right now.");
			return false;
		}
		if (gameOn) {
			c.sendMessage("The game is currently running.");
			return false;
		}
		if (!MinigameRules.canJoinMinigame(c))
			return false;
		for (Player player : players) {
			if (player != null && player.UUID.equals(c.UUID)
					&& !Config.isOwner(c)) {
				c.sendMessage("You already have an account inside.");
				return false;
			}
		}
		for (int i = 0; i < players.length; i++) {
			if (players[i] == null) {
				players[i] = c;
				c.sendMessage("You enter the Fight Pits Tournament.");
				c.getPA().walkTo(0, -2, true);
				c.minigameTarget = 0;
				c.getPacketSender().createPlayerHints(10, -1);
				return true;
			}
		}
		return false;
	}

	public static boolean forceKick(Client c) {
		for (int i = 0; i < players.length; i++) {
			if (players[i] != null) {
				if (players[i] == c) {
					c.getPA().movePlayer(2399, 5173, 0);
					c.minigameTarget = 0;
					c.getPacketSender().createPlayerHints(10, -1);
					players[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public static int getPlayerCount() {
		return playerCount;
	}

	public static boolean isTournamentOpen() {
		return gameQueueOn;
	}

	public static boolean leaveGame(Client c) {
		if (!gameQueueOn) {
			return false;
		}
		if (gameOn) {
			c.sendMessage("You can not exit game right now.");
			return false;
		}
		for (int i = 0; i < players.length; i++) {
			if (players[i] != null) {
				if (players[i] == c) {
					players[i] = null;
					c.minigameTarget = 0;
					c.getPacketSender().createPlayerHints(10, -1);
					c.getPA().walkTo(0, 2, true);
					return true;
				}
			}
		}
		return false;
	}

	public static void openTournament() {
		if (gameQueueOn) {
			return;
		}
		if (gameOn) {
			return;
		}
		gameQueueOn = true;
		PlayerHandler
				.messageAllPlayers("<col=800000>Fight Pits Tournament has been open!");
		PlayerHandler.messageAllPlayers(
				"<col=800000>You have 8 minutes to join the Fight Pits Tournament!");
		officialTourney = true;
		beginGameQueue();
	}

	public static void pairUp(Client c) {
		for (int i = 0; i < players.length; i++) {
			if (players[i] == null || players[i].getId() == c.getId()
					|| players[i].isDead() || players[i].minigameTarget > 0
					|| !RSConstants.fightPitsRoom(players[i].getX(),
							players[i].getY())) {
				continue;
			}
			if (players[i].minigameTarget == 0
					&& players[i].getId() != c.getId()) {
				c.minigameTarget = players[i].getId();
				c.getPacketSender().createPlayerHints(10, players[i].getId());
				players[i].minigameTarget = c.getId();
				players[i].getPacketSender().createPlayerHints(10, c.getId());
				return;
			}
		}
	}

	public static void refreshPlayerCount() {
		int count = 0;
		for (int i = 0; i < players.length; i++) { // for(Player p : players) {
			if (players[i] == null) {
				continue;
			}
			final Client c = (Client) players[i];
			if (c == null || c.forceDisconnect || c.disconnected) {
				players[i] = null;
				continue;
			}
			count++;
		}
		playerCount = count;
	}

	public static void rewardWinner() {
		allowFight = false;
		for (Player p : players) {
			if (p == null) {
				continue;
			}
			Client c = (Client) p;
			if (gameTime < FIGHT_TIME - 300) { // waits at least 3 minutes
												// before rewarding anyone
				if (officialTourney) { // tournament rewards
					PlayerHandler.messageAllPlayers("<col=800000>" + c.getNameSmartUp()
							+ " has won the Fight Pits Tournament!");
					if (!WorldType.equalsType(WorldType.SPAWN)) {
						c.getItems().addItem(995, 500000 + Misc.random(500000));
					}
					c.getItems()
							.addItem(
									tournyRewards[Misc
											.random(tournyRewards.length - 1)],
									1);
					c.pkp += 300;
					if (Misc.random(8) == 1) {
						c.getItems().addItem(hybridItems[Misc
								.random(hybridItems.length - 1)], 1);
					}
				} else { // regular rewards
					if (!WorldType.equalsType(WorldType.SPAWN)) {
						c.getItems().addItem(995, 100000 + Misc.random(200000));
					}
					c.pkp += 150;
					if (Misc.random(35) == 1) {
						c.getItems().addItem(hybridItems[Misc
								.random(hybridItems.length - 1)], 1);
					}
				}
			}
			if (c.getPlayerLevel()[3] == 0) {
				c.getPlayerLevel()[3] = 10;
			}
			c.addToHp(99);
			c.setDead(false);
			c.getPA().movePlayer(2399, 5173, 0);
			c.getPA().restorePlayer(true);
			c.getExtraHiscores().incFightPitsWins();
			break;
		}
	}

	public static void startProcess() {
		gameQueueOn = true;
		beginGameQueue();
	}

}
