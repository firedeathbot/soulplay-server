package com.soulplay.content.minigames.warriorsguild;

import com.soulplay.content.minigames.warriorsguild.WarriorsGuild.ArmorData;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.CombatNPC;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;

@NpcMeta(ids = { 4278, 4279, 4280, 4281, 4282, 4283, 4284 })
public class AnimatedArmorNpc extends CombatNPC {

	public AnimatedArmorNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new AnimatedArmorNpc(slot, npcType);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		Player spawnedBy = getSpawnedByPlayer();
		if (spawnedBy == null) {
			return;
		}

		ArmorData armorData = spawnedBy.animatedArmorSpawned;
		if (armorData == null) {
			return;
		}

		ItemHandler.createGroundItem(spawnedBy.getName(), WarriorsGuild.TOKEN, absX, absY, heightLevel, armorData.getTokens(), true,
				spawnedBy.isIronMan());
		spawnedBy.animatedArmorSpawned = null;
	}

	@Override
	public void npcHandlerProcessDestroy() {
		super.npcHandlerProcessDestroy();

		Player spawnedBy = getSpawnedByPlayer();
		if (spawnedBy != null) {
			spawnedBy.animatedArmorSpawned = null;
		}
	}

}
