package com.soulplay.content.minigames.warriorsguild;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 4291, 4292 })
public class CyclopesNpc extends AbstractNpc {

	public CyclopesNpc(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new CyclopesNpc(slot, npcType);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		Player killer = getKiller();
		if (killer == null || !killer.getZones().isInCyclopesZone()) {
			return;
		}

		if (Misc.randomBoolean(10)) {
			ItemHandler.createGroundItem(killer.getName(), WarriorsGuild.findDroppedDefender(killer).getItemId(), absX, absY,
					heightLevel, 1, true, killer.isIronMan());
		}
	}

}
