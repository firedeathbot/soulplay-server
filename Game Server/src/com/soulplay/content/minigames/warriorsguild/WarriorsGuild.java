package com.soulplay.content.minigames.warriorsguild;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.DonatorRank;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;

public class WarriorsGuild {

	public static final int TOKEN = 8851;

	public static enum ArmorData {
		BRONZE(new int[] { 1075, 1117, 1155 }, 4278, 10),
		IRON(new int[] { 1067, 1115, 1153 }, 4279, 20),
		STEEL(new int[] { 1069, 1119, 1157 }, 4280, 30),
		BLACK(new int[] { 1077, 1125, 1165 }, 4281, 40),
		MITHRIL(new int[] { 1071, 1121, 1159 }, 4282, 50),
		ADAMANT(new int[] { 1073, 1123, 1161 }, 4283, 60),
		RUNE(new int[] { 1079, 1127, 1163 }, 4284, 70),
		;

		public static Map<Integer, ArmorData> itemToData = new HashMap<>();
		private final int[] itemIds;
		private final int npcId, tokens;

		ArmorData(int[] itemIds, int npcId, int tokens) {
			this.itemIds = itemIds;
			this.npcId = npcId;
			this.tokens = tokens;
		}

		public int[] getItemIds() {
			return itemIds;
		}

		public int getNpcId() {
			return npcId;
		}

		public int getTokens() {
			return tokens;
		}

		static {
			for (ArmorData data : values()) {
				for (int id : data.getItemIds()) {
					itemToData.put(id, data);
				}
			}
		}
	}

	public static enum DefenderData {
		BRONZE("Bronze", 8844),
		IRON("Iron", 8845),
		STEEL("Steel", 8846),
		BLACK("Black", 8847),
		MITHRIL("Mithril", 8848),
		ADAMANT("Adamant", 8849),
		RUNE("Rune", 8850),
		DRAGON("Dragon", 20072);

		public static final DefenderData[] values = values();
		private final String name;
		private final int itemId;

		DefenderData(String name, int itemId) {
			this.name = name;
			this.itemId = itemId;
		}

		public String getName() {
			return name;
		}

		public int getItemId() {
			return itemId;
		}

	}

	public static DefenderData findDroppedDefender(Player player) {
		DefenderData[] values = DefenderData.values;
		for (int i = values.length - 1; i >= 0; i--) {
			DefenderData defenderData = values[i];
			int defenderId = defenderData.getItemId();
			if (player.getItems().playerHasEquipped(defenderId)
				|| player.getItems().playerHasItem(defenderId)) {
				if (defenderData == DefenderData.DRAGON) {
					return defenderData;
				} else {
					return values[i + 1];
				}
			}
		}

		return DefenderData.BRONZE;
	}

	public static void handleArmor(Player player, int itemId, int objectX, int objectY) {
		if (player.animatedArmorSpawned != null) {
			player.sendMessage("You already have an armor animated!");
			return;
		}

		ArmorData armorData = ArmorData.itemToData.get(itemId);
		if (armorData == null) {
			player.sendMessage("Nothing interesting happens.");
			return;
		}

		int[] ids = armorData.getItemIds();
		if (!player.getItems().playerHasItem(ids[0]) || !player.getItems().playerHasItem(ids[1])
				|| !player.getItems().playerHasItem(ids[2])) {
			player.sendMessage("You need to have all 3 armor pieces to animate.");
			return;
		}

		player.getItems().deleteItemInOneSlot(ids[0], 1);
		player.getItems().deleteItemInOneSlot(ids[1], 1);
		player.getItems().deleteItemInOneSlot(ids[2], 1);
		player.getPA().walkTo(0, 3);
		player.animatedArmorSpawned = armorData;
		NPC npc = NPCHandler.spawnNpc(player, armorData.getNpcId(), objectX, objectY, 0, 0, true, true);
		npc.forceChat("I'M ALIVE!");
		npc.startAnimation(4166);
		player.startAnimation(827);
	}

	public static void handleKamfreena(Player player, boolean door) {
		if (door) {
			if (!player.getZones().isInCyclopesZone() && !player.getItems().playerHasItem(8851, 100)) {
				player.sendMessage("You need at least 100 tokens to go in!");
				return;
			}

			dialog(player);
			if (player.absX < 2847) {
				player.getPA().movePlayer(2847, player.absY, 2);
				startEvent(player);
			} else {
				player.getPA().movePlayer(2846, player.absY, 2);
			}
		} else {
			dialog(player);
		}
	}

	public static void dialog(Player player) {
		DefenderData defenderData = findDroppedDefender(player);
		if (defenderData == DefenderData.BRONZE) {
			player.getDialogueBuilder().sendNpcChat(4289, "Well, since you haven't shown me a defender to", "prove your prowess as a warrior.").
			sendNpcChat(4289, "I'll release some Cyclopes which might drop bronze",
					"defenders for you to start off with, unless you show me", "another. Have fun in there.").execute();
		} else {
			player.getDialogueBuilder().sendNpcChat(4289, "You have a defender, so I'll release some cyclopes",
					"which may drop " + defenderData.getName() + " defenders.").execute();
		}
	}

	private static void startEvent(Player player) {
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer e) {
				if (player == null || player.disconnected || !player.getZones().isInCyclopesZone()) {
					e.stop();
					return;
				}

				if (player.getZones().isInCyclopesZone()) {
					int tokensRequired = DonatorRank.check(player).getWarriorGuildTokensConsumed();
					if (player.getItems().playerHasItem(TOKEN, tokensRequired)) {
						if (tokensRequired == 1) {
							player.sendMessage("You lose a token.");
						} else {
							player.sendMessage("You lose " + tokensRequired + " tokens.");
						}
						player.getItems().deleteItem2(TOKEN, tokensRequired);
					} else {
						player.sendMessage("You have ran out of tokens!");
						player.getPA().safeDeathSecureTimer();
						player.getPA().movePlayer(2846, 3541, 2);
						e.stop();
					}
				}
			}

		}, 100);
	}

}
