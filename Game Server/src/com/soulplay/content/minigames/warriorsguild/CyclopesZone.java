package com.soulplay.content.minigames.warriorsguild;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.Zone;

public class CyclopesZone extends Zone {

	@Override
	public void onChange(Player p) {
		/* empty */
	}

	@Override
	public void onEntry(Player p) {
		/* empty */
	}

	@Override
	public void onExit(Player p) {
		/* empty */
	}

	@Override
	public void onLogout(Player player) {
		super.onLogout(player);

		player.getPA().movePlayer(2844, 3540, 2);
	}

	@Override
	public boolean isInZoneCoordinates(Player p) {
		return p.heightLevel == 2 && (p.absX >= 2847 && p.absX <= 2877 && p.absY >= 3533 && p.absY <= 3557
				|| p.absX >= 2837 && p.absX <= 2852 && p.absY >= 3543 && p.absY <= 3556);
	}

}
