package com.soulplay.content.minigames.inferno;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 9030 })
public class JalZek extends InfernoNPC {

	public JalZek(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new JalZek(slot, npcType);
	}

	@Override
	public void onSpawn() {
		getCombatStats().setAttackSpeed(4);
		setAttackTimer(2);
		stopAttack = true;
		attackType = 2;
		walkUpToHit = false;
		resetAttackType = false;
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}
	
	@Override
	public void onDeath() {
		super.onDeath();
		actionTimer = 3;
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0 || infernoManager == null) {
			return;
		}

		Player player = infernoManager.player;

		if (player == null || player.isDead()) {
			return;
		}

		face(player);

		if (getAttackTimer() == 0) {
			if (NPCHandler.goodDistance(player, this, 1)) {
				attackType = Misc.random(1);
			} else {
				attackType = 1;
			}

			switch (attackType) {
				case 0:
					attackType = 0;
					startAnimation(NPCAnimations.getCombatEmote(npcType));
					break;
				case 1:
					attackType = 2;
					startAnimation(new Animation(Animation.getOsrsAnimId(7610)));
					Location targetLoc = Location.create(player.getX(), player.getY(), player.getZ());

					Location startLoc = Projectile.getLocationOffset(getCenterLocation(), targetLoc, 3);
					Projectile proj = new Projectile(Graphic.getOsrsId(1376), startLoc, targetLoc);
					proj.setStartHeight(80);
					proj.setSlope(1);
					proj.setEndHeight(31);
					proj.setStartDelay(45);
					proj.setLockon(player.getProjectileLockon());
					proj.setEndDelay(80);
					GlobalPackets.createProjectile(proj);
					break;
			}

			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					NPCHandler.applyDamage(getId(), (Client) player, false, null);

					container.stop();
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 4);

			setAttackTimer(getCombatStats().getAttackSpeed());
		}
	}

}
