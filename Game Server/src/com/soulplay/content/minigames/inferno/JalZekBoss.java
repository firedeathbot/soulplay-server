package com.soulplay.content.minigames.inferno;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;

@NpcMeta(ids = { 9065 })
public class JalZekBoss extends InfernoNPC {

	public AncestralGlyph shield;
	private boolean interrupted = false;

	public JalZekBoss(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new JalZekBoss(slot, npcType);
	}

	@Override
	public void onSpawn() {
		getCombatStats().setAttackSpeed(4);
		stopAttack = true;
		attackType = 2;
		walkUpToHit = false;
		resetAttackType = false;
		setCanWalk(false);
		setKillerId(0);
		isAggressive = false;
	}
	
	@Override
	public void onDeath() {
		super.onDeath();
		actionTimer = 3;
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0 || infernoManager == null) {
			return;
		}

		if (underAttackBy > 0 && !interrupted) {
			setKillerId(underAttackBy);
			interrupted = true;
		}

		Mob mob = null;
		if (interrupted || shield.isDead()) {
			mob = infernoManager.player;
		} else {
			mob = shield;
		}

		if (mob == null) {
			return;
		}

		face(mob);

		if (getAttackTimer() == 0) {
			attackType = 2;
			startAnimation(new Animation(Animation.getOsrsAnimId(7610)));
			Location targetLoc = Location.create(mob.absX, mob.absY, mob.heightLevel);

			Location startLoc = Projectile.getLocationOffset(getCenterLocation(), targetLoc, 3);
			Projectile proj = new Projectile(Graphic.getOsrsId(1376), startLoc, targetLoc);
			proj.setStartHeight(80);
			proj.setSlope(1);
			proj.setEndHeight(31);
			proj.setStartDelay(45);
			proj.setLockon(mob.getProjectileLockon());
			proj.setEndDelay(80);
			GlobalPackets.createProjectile(proj);

			final Mob finalMob = mob;
			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (finalMob.isPlayer()) {
						NPCHandler.applyDamage(getId(), (Client) finalMob, false, null);
					} else {
						NPCHandler.applyDamageToNpc(getId(), (NPC) finalMob);
					}

					container.stop();
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 4);

			setAttackTimer(getCombatStats().getAttackSpeed());
		}
	}

}
