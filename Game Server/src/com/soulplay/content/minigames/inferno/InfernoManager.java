package com.soulplay.content.minigames.inferno;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.instances.DynSingleInstanceManager;
import com.soulplay.game.world.map.DynamicObjectManager;
import com.soulplay.game.world.map.ObjectDefVersion;
import com.soulplay.game.world.map.Rectangle;
import com.soulplay.game.world.map.travel.zone.Zone;
import com.soulplay.game.world.map.travel.zone.impl.InfernoZone;
import com.soulplay.util.Misc;

public class InfernoManager {

	private List<Location> spawnCoords = new ArrayList<>();
	private static DynSingleInstanceManager zInstanceManager = new DynSingleInstanceManager();
	public int startX = 500;
	public int startY = 700;
	public Player player;
	public int height;
	private DynamicRegionClip clipping = new DynamicRegionClip();
	public DynamicObjectManager objectManager = new DynamicObjectManager(clipping);
	private int waveIndex;
	public InfernoWave wave;
	public int monstersLeft;
	private boolean destroyed;
	private CycleEventContainer waveEvent;
	public Zone zone;
	public Zuk finalBoss;

	public InfernoManager(Player player) {
		this.player = player;
		this.height = getInstanceId(player.mySQLIndex);

		this.spawnCoords.add(Location.create(27, 38));
		this.spawnCoords.add(Location.create(36, 36));
		this.spawnCoords.add(Location.create(36, 27));
		this.spawnCoords.add(Location.create(36, 18));
		this.spawnCoords.add(Location.create(27, 18));
		this.spawnCoords.add(Location.create(18, 18));
		this.spawnCoords.add(Location.create(18, 26));
		this.spawnCoords.add(Location.create(18, 38));
	}

	public void copy() {
		int paletteStartX = 8640;
		int paletteStartY = 5313;
		int paletteEndX = 8703;
		int paletteEndY = 5375;

		objectManager.setDefsVersion(ObjectDefVersion.OSRS_DEFS);

		int toChunkX = startX >> 3;
		for (int fromChunkX = paletteStartX >> 3; fromChunkX <= paletteEndX >> 3; fromChunkX++) {
			int toChunkY = startY >> 3;
			for (int fromChunkY = paletteStartY >> 3; fromChunkY <= paletteEndY >> 3; fromChunkY++) {
				Region toRegion = Server.getRegionManager().getRegionByLocation(toChunkX << 3, toChunkY << 3, height);
				toRegion.makeDynamicData();
				int regionOffsetX = toChunkX - ((toChunkX >> 3) << 3);
				int regionOffsetY = toChunkY - ((toChunkY >> 3) << 3);
				toRegion.setDynamicData(fromChunkX, fromChunkY, 0, regionOffsetX, regionOffsetY, height, 0);

				DynamicRegionClip.copy(fromChunkX << 3, fromChunkY << 3, 0, toChunkX << 3, toChunkY << 3, height, objectManager.getStaticObjects(), 0, clipping, ObjectDefVersion.OSRS_DEFS);

				toChunkY++;
			}

			toChunkX++;
		}

		zone = new InfernoZone(new Rectangle(Location.create(startX, startY), Location.create(startX + 64, startY + 64)));
	}

	public Location base(int x, int y) {
		return Location.create(startX + x, startY + y, height);
	}

	public void teleportPlayer() {
		player.infernoManager = this;
		
		player.getPacketSender().sendDiscordUpdate("", "Inferno", 0, 0, System.currentTimeMillis()/1000, 0);
		
		player.getDamageMap().reset();

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				player.getPA().movePlayer(base(27, 14));
				player.setDynamicRegionClip(clipping);
				player.getPacketSender().sendConstructMapRegionPacket(height);
				player.updateRegion = true;
				player.extendedNpcRendering = true;
				player.dynObjectManager = objectManager;
				player.sendMessage("Inferno challenge will start in 12 seconds...");
				spawnWave();
				if (zone != null) {
					player.getZones().addDynamicZone(zone);
				}

				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 1);
	}

	public void updateHud() {
		if (wave != InfernoWave.WAVE_69 || finalBoss == null) {
			return;
		}

		player.getPA().walkableInterface(67240);
		player.getPacketSender().sendHpHud(finalBoss.getNpcName(), finalBoss.getSkills().getLifepoints(), finalBoss.getSkills().getMaximumLifepoints());
	}

	public void completeInferno() {
		teleportOut();
		player.getDialogueBuilder().sendNpcChat(2617, "You are very impressive for a Jal'Yt. You managed to",
				"defeat TzKal-Zuk! Please accept this cape as a token of", "appreciation.").execute();
		player.getItems().addOrDrop(new GameItem(30368, 1));
		
		if (Misc.randomBoolean(777)) {
			player.sendMessage("You got a pet!");
			player.getItems().addOrDrop(new GameItem(30377, 1));
		}
	}

	public void teleportOut() {
		player.getPA().movePlayer(LocationConstants.INFERNO);
	}

	public void cleanup() {
		if (destroyed) {
			return;
		}

		destroyed = true;
		player.infernoManager = null;
		player.extendedNpcRendering = false;
		player.dynObjectManager = null;
		player.setDynamicRegionClip(null);
		player.updateRegion = true;
		player.mapRegionDidChange = true;

		if (objectManager != null) {
			objectManager.cleanup();
			objectManager = null;
		}

		if (waveEvent != null) {
			waveEvent.stop();
			waveEvent = null;
		}
	}

	public void checkWaveSpawn() {
		if (monstersLeft > 0 || wave == InfernoWave.WAVE_69) {
			return;
		}

		spawnWave();
		player.sendMessage("Wave completed!");
	}

	private void startFinalWave() {
		GameObject leftRock = GameObject.createTickObject(30346, base(24, 48), 3, 10, Integer.MAX_VALUE, -1);
		objectManager.addObject(leftRock);
		GameObject rightRock = GameObject.createTickObject(30345, base(29, 48), 3, 10, Integer.MAX_VALUE, -1);
		objectManager.addObject(rightRock);
		GameObject shield = new GameObject(30345, base(26, 47), 0, 10);
		objectManager.removeObject(shield, Integer.MAX_VALUE);
		GameObject rightRockCorner = GameObject.createTickObject(30339, base(31, 48), 3, 10, Integer.MAX_VALUE, -1);
		objectManager.addObject(rightRockCorner);
		GameObject leftRockCorner = GameObject.createTickObject(30340, base(23, 48), 1, 10, Integer.MAX_VALUE, -1);
		objectManager.addObject(leftRockCorner);
		GameObject leftWall = GameObject.createTickObject(30342, base(23, 50), 1, 10, Integer.MAX_VALUE, -1);
		objectManager.addObject(leftWall);
		GameObject rightWall = GameObject.createTickObject(30341, base(31, 50), 3, 10, Integer.MAX_VALUE, -1);
		objectManager.addObject(rightWall);
		AncestralGlyph shieldNpc = (AncestralGlyph) spawnNpc(InfernoMonsters.ANCESTRAL_GLYPH, 26, 44);
		finalBoss = (Zuk) spawnNpc(InfernoMonsters.ZUK, 24, 48);
		finalBoss.shield = shieldNpc;
	}

	public void spawnWave() {
		wave = InfernoWave.waves[waveIndex];
		waveEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();

				if (wave == InfernoWave.WAVE_69) {
					startFinalWave();
				} else {
					waveIndex++;
					monstersLeft = wave.getNpcs().length;
					if (wave == InfernoWave.WAVE_68) {
						JalTokJad jad1 = (JalTokJad) spawnNpc(InfernoMonsters.JAD, 31, 30);
						jad1.healerCount = 3;
						jad1.setAttackTimer(3);
						JalTokJad jad2 = (JalTokJad) spawnNpc(InfernoMonsters.JAD, 19, 30);
						jad2.healerCount = 3;
						jad2.setAttackTimer(6);
						JalTokJad jad3 = (JalTokJad) spawnNpc(InfernoMonsters.JAD, 24, 20);
						jad3.healerCount = 3;
						jad3.setAttackTimer(9);
					} else if (wave != InfernoWave.WAVE_69) {
						Collections.shuffle(spawnCoords);

						int pos = 0;
						for (InfernoMonsters npc : wave.getNpcs()) {
							Location coords = spawnCoords.get(Math.min(spawnCoords.size() - 1, pos++));
							spawnNpc(npc, coords.getX(), coords.getY());
						}
					}
				}

				player.sendMessage("<col=ff0000>" + wave);
				player.getPacketSender().sendDiscordUpdate("Wave: " + wave.getWaveNumber(), "Inferno", 0, 0, -1, -1);
				waveEvent = null;
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, Misc.secondsToTicks(12));
	}

	public NPC spawnNpc(InfernoMonsters npcData, int x, int y) {
		int slot = Misc.findFreeNpcSlot();

		if (slot == -1) {
			return null;
		}

		Location spawnLoc = base(x, y);
		InfernoNPC npc;
		if (npcData == InfernoMonsters.JAD) {
			npc = new JalTokJad(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.JAD_HEALER) {
			npc = new JadHealer(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.ANCESTRAL_GLYPH) {
			npc = new AncestralGlyph(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.ZUK) {
			npc = new Zuk(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.ZUK_HEALER) {
			npc = new ZukHealer(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.RANGER) {
			npc = new JalXil(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.MAGER) {
			npc = new JalZek(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.RANGER_BOSS) {
			npc = new JalXilBoss(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.MAGER_BOSS) {
			npc = new JalZekBoss(slot, npcData.getId());
		} else if (npcData == InfernoMonsters.JAD_BOSS) {
			npc = new JalTokJadBoss(slot, npcData.getId());
		} else {
			npc = new InfernoNPC(slot, npcData.getId());
		}

		npc.setDynamicRegionClip(clipping);
		npc.infernoManager = this;
		npc.inMultiZone = true;
		npc.setKillerId(player.getId());
		npc.spawnedBy = player.getId();
		npc.isAggressive = true;
		return NPCHandler.spawnNpc(npc, spawnLoc.getX(), spawnLoc.getY(), spawnLoc.getZ(), 0);
	}

	public static int getInstanceId(int uniqueId) {
		return zInstanceManager.getOrCreateZ(uniqueId);
	}

}
