package com.soulplay.content.minigames.inferno;

import static com.soulplay.content.minigames.inferno.InfernoMonsters.*;

public enum InfernoWave {

	WAVE_64(64, MELEE, MELEE, RANGER, MAGER),
	WAVE_65(65, RANGER, RANGER, MAGER),
	WAVE_66(66, MAGER, MAGER),
	WAVE_67(67, JAD),
	WAVE_68(68, JAD, JAD, JAD),
	WAVE_69(69);

	public static final InfernoWave[] waves = values();
	private final InfernoMonsters[] npcs;
	private final int waveNumber;

	private InfernoWave(int wave, InfernoMonsters... npcs) {
		this.waveNumber = wave;
		this.npcs = npcs;
	}

	public InfernoMonsters[] getNpcs() {
		return npcs;
	}

	public int getWaveNumber() {
		return waveNumber;
	}

}
