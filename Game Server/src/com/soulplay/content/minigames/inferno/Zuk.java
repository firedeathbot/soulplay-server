package com.soulplay.content.minigames.inferno;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 9041 })
public class Zuk extends InfernoNPC {

	public AncestralGlyph shield;
	private int waveTimer = 0;
	private int spawnWaveOn = 79;
	private boolean pauseTimer = false;
	private boolean timerIncreased = false;
	private boolean jadSpawned = false;
	private boolean healersSpawned = false;
	private List<NPC> minions = new ArrayList<>();

	public Zuk(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new Zuk(slot, npcType);
	}

	@Override
	public void onSpawn() {
		stopAttack = true;
		noClip = true;
		setCanWalk(false);
		dontFace = true;
		getCombatStats().setAttackSpeed(13);
		getSkills().setLevel(Skills.MAGIC, 150);
		getCombatStats().setMagicAccBonus(550);
		getCombatStats().setMagicDefBonus(350);
		getPlayerBonus()[EquipmentBonuses.ATTACK_MAGIC.getId()] = 550; // for twisted bow
	}

	@Override
	public int dealDamage(Hit hit) {
		int dmg = super.dealDamage(hit);

		if (getSkills().getLifepoints() <= 600 && getSkills().getLifepoints() >= 481) {
			pauseTimer = true;
			if (!timerIncreased) {
				timerIncreased = true;

				waveTimer -= 175;
			}
		} else if (getSkills().getLifepoints() <= 480 && !jadSpawned) {
			pauseTimer = false;
			if (!jadSpawned) {
				jadSpawned = true;

				JalTokJadBoss jad = (JalTokJadBoss) infernoManager.spawnNpc(InfernoMonsters.JAD_BOSS, 26, 31);
				jad.setAttackTimer(6);
				jad.shield = shield;

				minions.add(jad);
			}
		} else if (getSkills().getLifepoints() <= 240) {
			if (!healersSpawned) {
				healersSpawned = true;

				ZukHealer healer1 = (ZukHealer) infernoManager.spawnNpc(InfernoMonsters.ZUK_HEALER, 18, 47);
				healer1.setAttackTimer(3);
				healer1.zuk = this;
				ZukHealer healer2 = (ZukHealer) infernoManager.spawnNpc(InfernoMonsters.ZUK_HEALER, 22, 47);
				healer2.setAttackTimer(3);
				healer2.zuk = this;
				ZukHealer healer3 = (ZukHealer) infernoManager.spawnNpc(InfernoMonsters.ZUK_HEALER, 31, 47);
				healer3.setAttackTimer(3);
				healer3.zuk = this;
				ZukHealer healer4 = (ZukHealer) infernoManager.spawnNpc(InfernoMonsters.ZUK_HEALER, 36, 47);
				healer4.setAttackTimer(3);
				healer4.zuk = this;

				minions.add(healer1);
				minions.add(healer2);
				minions.add(healer3);
				minions.add(healer4);
			}
		}

		return dmg;
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void onDeath() {
		super.onDeath();
		for (NPC minion : minions) {
			minion.dealDamage(new Hit(minion.getSkills().getLifepoints(), 0, -1));
		}

		shield.removeNpcSafe(false);
		actionTimer = 7;

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				infernoManager.completeInferno();
				container.stop();
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, actionTimer);
	}

	private void spawnWave() {
		JalZekBoss mager = (JalZekBoss) infernoManager.spawnNpc(InfernoMonsters.MAGER_BOSS, 23, 36);
		mager.shield = shield;
		mager.setAttackTimer(6);

		JalXilBoss ranger = (JalXilBoss) infernoManager.spawnNpc(InfernoMonsters.RANGER_BOSS, 33, 35);
		ranger.shield = shield;
		ranger.setAttackTimer(7);

		waveTimer = 0;
		spawnWaveOn = 350;

		minions.add(mager);
		minions.add(ranger);
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || infernoManager == null) {
			return;
		}

		if (!pauseTimer) {
			waveTimer++;
		}

		if (waveTimer == spawnWaveOn) {
			spawnWave();
		}

		if (getAttackTimer() == 0) {
			// Each attack is approx 10 ticks, 1st attack is delayed by few ticks?
			setAttackTimer(10);

			Player player = infernoManager.player;

			boolean blockedByShield = false;
			if (!shield.isDead()) {
				int distanceX = (int) Math.sqrt(Math.pow(player.absX - (shield.absX + 1), 2));
				if (distanceX <= 2) {
					blockedByShield = true;
				}
			}

			Location targetLoc;
			int lockon;
			if (!blockedByShield && player != null) {
				targetLoc = Location.create(player.getX(), player.getY(), player.getZ());
				lockon = player.getProjectileLockon();
			} else {
				targetLoc = shield.getCurrentLocation().copyNew().transform(0, 5);
				lockon = shield.getProjectileLockon();
			}

			Location startLoc = Projectile.getLocationOffset(getCurrentLocation(), targetLoc, 5);
			Projectile proj = new Projectile(Graphic.getOsrsId(1375), startLoc, targetLoc);
			proj.setStartHeight(50);
			proj.setSlope(1);
			proj.setEndHeight(31);
			proj.setStartDelay(75);
			proj.setLockon(lockon);
			proj.setEndDelay(140);
			GlobalPackets.createProjectile(proj);
			
			final NPC zukNpc = this;

			startAnimation(new Animation(Animation.getOsrsAnimId(7566)));

			final boolean finalBlockedByShield = blockedByShield;
			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (!finalBlockedByShield && player != null) {
						player.dealTrueDamage(Misc.random(251), 0, null, zukNpc);
					} else {
						shield.startAnimation(new Animation(Animation.getOsrsAnimId(7568)));
					}

					container.stop();
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 6);
		}
	}

}
