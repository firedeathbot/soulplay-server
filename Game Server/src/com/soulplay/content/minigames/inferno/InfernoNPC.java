package com.soulplay.content.minigames.inferno;

import com.soulplay.game.model.npc.plugins.AbstractNpc;

public class InfernoNPC extends AbstractNpc {

	public InfernoManager infernoManager;

	public InfernoNPC(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new InfernoNPC(slot, npcType);
	}

	@Override
	public void onDeath() {
		super.onDeath();

		infernoManager.monstersLeft--;
		infernoManager.checkWaveSpawn();
	}

}
