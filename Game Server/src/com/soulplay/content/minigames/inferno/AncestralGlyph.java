package com.soulplay.content.minigames.inferno;

import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;

@NpcMeta(ids = { 9063 })
public class AncestralGlyph extends InfernoNPC {

	private final int[] ROTATION_X = {13, 39};
	private int rotationId;
	private int waitTicks;

	public AncestralGlyph(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new AncestralGlyph(slot, npcType);
	}

	@Override
	public void onSpawn() {
		stopAttack = true;
		setKillerId(0);
		isAggressive = false;
		noClip = true;
		setRetaliates(false);
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || infernoManager == null) {
			return;
		}

		int destX = infernoManager.base(ROTATION_X[rotationId], 0).getX();
		if (absX > destX) {
			moveX = -1;
		} else if (absX < destX) {
			moveX = 1;
		} else if (absX == destX) {
			waitTicks++;
			if (waitTicks >= 5) {
				rotationId++;
				if (rotationId >= ROTATION_X.length) {
					rotationId = 0;
				}

				waitTicks = 0;
			}
		}
	}

}
