package com.soulplay.content.minigames.inferno;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 9021 })
public class JalXil extends InfernoNPC {

	public JalXil(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new JalXil(slot, npcType);
	}

	@Override
	public void onSpawn() {
		getCombatStats().setAttackSpeed(4);
		stopAttack = true;
		attackType = 1;
		walkUpToHit = false;
		resetAttackType = false;
	}
	
	@Override
	public void onDeath() {
		super.onDeath();
		actionTimer = 4;
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0 || infernoManager == null) {
			return;
		}

		Player player = infernoManager.player;

		if (player == null || player.isDead()) {
			return;
		}

		face(player);

		if (getAttackTimer() == 0) {
			if (NPCHandler.goodDistance(player, this, 1)) {
				attackType = Misc.random(1);
			} else {
				attackType = 1;
			}

			switch (attackType) {
				case 0:
					startAnimation(NPCAnimations.getCombatEmote(npcType));
					break;
				case 1:
					startAnimation(new Animation(Animation.getOsrsAnimId(7605)));
					Location targetLoc = Location.create(player.getX(), player.getY(), player.getZ());

					Location startLoc = Projectile.getLocationOffset(getCurrentLocation(), targetLoc, 3);
					Projectile proj = new Projectile(Graphic.getOsrsId(1377), startLoc, targetLoc);
					proj.setStartHeight(70);
					proj.setSlope(1);
					proj.setEndHeight(31);
					proj.setStartDelay(55);
					proj.setLockon(player.getProjectileLockon());
					proj.setEndDelay(80);
					GlobalPackets.createProjectile(proj);

					startLoc = Projectile.getLocationOffset(getCenterLocation(), targetLoc, 3);
					proj = new Projectile(Graphic.getOsrsId(1377), startLoc, targetLoc);
					proj.setStartHeight(70);
					proj.setSlope(1);
					proj.setEndHeight(31);
					proj.setStartDelay(55);
					proj.setLockon(player.getProjectileLockon());
					proj.setEndDelay(80);
					GlobalPackets.createProjectile(proj);
					break;
			}

			CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					NPCHandler.applyDamage(getId(), (Client) player, false, null);

					container.stop();
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 4);

			setAttackTimer(getCombatStats().getAttackSpeed());
		}
	}

}
