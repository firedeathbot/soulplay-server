package com.soulplay.content.minigames.inferno;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.animations.NPCAnimations;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 9031 })
public class JadHealer extends InfernoNPC {

	public NPC jad;
	private boolean interrupted;

	public JadHealer(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new JadHealer(slot, npcType);
	}

	@Override
	public void onSpawn() {
		stopAttack = true;
		getCombatStats().setAttackSpeed(4);
		setKillerId(0);
		isAggressive = false;
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void onDeath() {
		/* empty */
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0 || jad == null) {
			return;
		}

		if (!interrupted) {
			NPCHandler.followNpc(npcIndex, jad.npcIndex);
		}

		if (underAttackBy > 0) {
			setKillerId(underAttackBy);
			interrupted = true;
		}

		if (getAttackTimer() == 0) {
			if (!interrupted && Misc.distanceToPoint(getX(), getY(), jad.getX() + 2, jad.getY() + 2) <= 4
					&& jad.getSkills().getLifepoints() < jad.getSkills().getMaximumLifepoints()) {
				jad.getSkills().heal(5);// TODO proper value?
				jad.startGraphic(Graphic.create(444, GraphicType.EVEN_HIGHER));
				startAnimation(new Animation(Animation.getOsrsAnimId(2639)));
			} else if (getKillerId() > 0) {
				Player player = PlayerHandler.players[getKillerId()];

				if (player != null && Misc.distanceToPoint(getX(), getY(), player.getX(), player.getY()) == 1) {
					startAnimation(NPCAnimations.getCombatEmote(npcType));
					NPCHandler.applyDamage(getId(), (Client) player, false, null);
				}
			}

			setAttackTimer(getCombatStats().getAttackSpeed());
		}
	}

}
