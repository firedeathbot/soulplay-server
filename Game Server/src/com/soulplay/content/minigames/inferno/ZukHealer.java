package com.soulplay.content.minigames.inferno;

import com.soulplay.Server;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 9062 })
public class ZukHealer extends InfernoNPC {

	public Zuk zuk;
	private boolean interrupted = false;

	public ZukHealer(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new ZukHealer(slot, npcType);
	}

	@Override
	public void onSpawn() {
		stopAttack = true;
		getCombatStats().setAttackSpeed(3);
		setKillerId(0);
		isAggressive = false;
		setCanWalk(false);
		startAnimation(new Animation(Animation.getOsrsAnimId(2864)));
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0 || infernoManager == null) {
			return;
		}

		if (underAttackBy > 0) {
			interrupted = true;
		}

		if (getAttackTimer() == 0) {
			if (!interrupted) {
				face(zuk);

				Location targetLoc = Projectile.getLocationOffset(getCurrentLocation(), zuk.getCenterLocation(),
						zuk.getSize());
				Projectile proj = new Projectile(Graphic.getOsrsId(660), getCurrentLocation(), targetLoc);
				proj.setStartHeight(10);
				proj.setSlope(10);
				proj.setEndHeight(31);
				proj.setStartDelay(0);
				proj.setLockon(zuk.getProjectileLockon());
				proj.setEndDelay(140);
				GlobalPackets.createProjectile(proj);

				zuk.getSkills().heal(Misc.random(15, 30));
				zuk.startGraphic(Graphic.create(444, GraphicType.EVEN_HIGHER));
			} else {
				Player player = infernoManager.player;
				face(player);

				int baseX = absX;
				int baseY = absY - 5;
				int minPosX = baseX - 4;
				int maxPosX = baseX + 4;
				int minPosY = baseY;
				int maxPosY = baseY - 3;

				for (int i = 0; i < 3; i++) {
					int destX = Misc.random(minPosX, maxPosX);
					int destY = Misc.random(minPosY, maxPosY);
					final Location targetLocation = Location.create(destX, destY, infernoManager.height);
					Projectile proj = new Projectile(Graphic.getOsrsId(660), getCurrentLocation(), targetLocation);
					proj.setStartHeight(1);
					proj.setSlope(50);
					proj.setEndHeight(10);
					proj.setStartDelay(0);
					proj.setLockon(-1);
					proj.setEndDelay(90);
					GlobalPackets.createProjectile(proj);

					CycleEventHandler.getSingleton().addEvent(zuk, new CycleEvent() {

						@Override
						public void execute(CycleEventContainer container) {
							container.stop();
							Server.getStillGraphicsManager().createGfx(targetLocation, Graphic.getOsrsId(659), 0, 0);

							int dist = Misc.distanceToPoint(player.absX, player.absY, targetLocation.getX(),
									targetLocation.getY());
							if (dist <= 1) {
								player.dealDamage(new Hit(Misc.random(8, 10), 0, -1));
							}
						}

						@Override
						public void stop() {
							/* empty */
						}

					}, 4);
				}
			}

			startAnimation(new Animation(Animation.getOsrsAnimId(2868)));
			setAttackTimer(getCombatStats().getAttackSpeed());
		}
	}

}
