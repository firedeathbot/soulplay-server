package com.soulplay.content.minigames.inferno;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.npc.plugins.AbstractNpc;
import com.soulplay.game.model.npc.plugins.NpcMeta;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.GraphicCollection;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.entity.Mob;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.util.Misc;

@NpcMeta(ids = { 9104 })
public class JalTokJadBoss extends InfernoNPC {

	public AncestralGlyph shield;
	private boolean spawnedHealers;
	private int healerCount = 3;
	private List<NPC> healers = new ArrayList<>(healerCount);
	private boolean interrupted = false;

	public JalTokJadBoss(int slot, int npcType) {
		super(slot, npcType);
	}

	@Override
	public AbstractNpc construct(int slot, int npcType) {
		return new JalTokJadBoss(slot, npcType);
	}

	@Override
	public void onSpawn() {
		// Range attack type so they don't walk.
		attackType = 1;
		stopAttack = true;
		getCombatStats().setAttackSpeed(9);
		walkUpToHit = false;
		resetAttackType = false;
		setCanWalk(false);
		setKillerId(0);
		isAggressive = false;
		getSkills().setLevel(Skills.MAGIC, 510);
		getCombatStats().setMagicAccBonus(100);
	}

	@Override
	public void onDeath() {
		super.onDeath();
		actionTimer = 6;
		for (NPC npc : healers) {
			npc.removeNpcSafe(false);
		}

		healers.clear();
	}

	@Override
	public boolean loadedSpell(Player p) {
		return true;
	}

	@Override
	public void process() {
		super.process();

		if (isDead() || getSkills().getLifepoints() <= 0 || infernoManager == null) {
			return;
		}

		if (underAttackBy > 0 && !interrupted) {
			setKillerId(underAttackBy);
			interrupted = true;
		}

		if (!spawnedHealers && getSkills().getLifepoints() <= getSkills().getMaximumLifepoints() / 2) {
			spawnedHealers = true;

			for (int i = 0; i < healerCount; i++) {
				Location center = getCenterLocation();
				int absX = center.getX() - infernoManager.startX;
				int absY = center.getY() - infernoManager.startY;

				Location spawn = Location.create(Misc.random(absX - 2, absX + 2), absY).transform(Direction.NORTH, 4);

				JadHealer npc = (JadHealer) infernoManager.spawnNpc(InfernoMonsters.JAD_HEALER, spawn.getX(), spawn.getY());
				npc.jad = this;
				healers.add(npc);
			}
		}

		Mob mob = null;
		if (interrupted || shield.isDead()) {
			mob = infernoManager.player;
		} else {
			mob = shield;
		}

		if (mob == null) {
			return;
		}

		face(mob);

		if (getAttackTimer() == 0) {
			int attack = 0;
			int attackDelay = 0;

			if (NPCHandler.goodDistance(mob, this, 1)) {
				attack = Misc.random(2);
			} else {
				attack = Misc.random(1);
			}

			Location targetLoc = Location.create(mob.absX, mob.absY, mob.heightLevel);
			Graphic endGraphic = null;
			switch (attack) {
			case 0:// mage
				attackDelay = 6;
				attackType = 2;
				endGraphic = GraphicCollection.EXPLOSION;
				startAnimation(Animation.getOsrsAnimId(7592));

				Location startLoc = Projectile.getLocationOffset(getCurrentLocation(), targetLoc, 5);
				Projectile proj = new Projectile(Graphic.getOsrsId(448), startLoc, targetLoc);
				proj.setStartHeight(130);
				proj.setSlope(1);
				proj.setEndHeight(31);
				proj.setStartDelay(65);
				proj.setLockon(mob.getProjectileLockon());
				proj.setEndDelay(130);
				GlobalPackets.createProjectile(proj);

				proj = new Projectile(Graphic.getOsrsId(449), startLoc, targetLoc);
				proj.setStartHeight(130);
				proj.setSlope(1);
				proj.setEndHeight(31);
				proj.setStartDelay(70);
				proj.setLockon(mob.getProjectileLockon());
				proj.setEndDelay(135);
				GlobalPackets.createProjectile(proj);

				proj = new Projectile(Graphic.getOsrsId(450), startLoc, targetLoc);
				proj.setStartHeight(130);
				proj.setSlope(1);
				proj.setEndHeight(31);
				proj.setStartDelay(76);
				proj.setLockon(mob.getProjectileLockon());
				proj.setEndDelay(141);
				GlobalPackets.createProjectile(proj);
				break;
			case 1:// range
				attackDelay = 6;
				attackType = 1;
				Server.getStillGraphicsManager().createGfx(targetLoc, Graphic.getOsrsId(451), 0, 75);
				endGraphic = GraphicCollection.EXPLOSION;
				startAnimation(Animation.getOsrsAnimId(7593));
				break;
			case 2:// melee
				attackDelay = 2;
				attackType = 0;
				startAnimation(Animation.getOsrsAnimId(7590));
				break;
			}

			setAttackTimer(getCombatStats().getAttackSpeed());

			final Graphic finalEndGraphic = endGraphic;
			final Mob finalMob = mob;
			CycleEventHandler.getSingleton().addEvent(mob, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer e) {
					if (finalEndGraphic != null) {
						finalMob.startGraphic(finalEndGraphic);
					}

					if (finalMob.isPlayer()) {
						NPCHandler.applyDamage(getId(), (Client) finalMob, false, null);
					} else {
						NPCHandler.applyDamageToNpc(getId(), (NPC) finalMob);
					}

					e.stop();
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, attackDelay);
		}

	}

}
