package com.soulplay.content.minigames.inferno;

public enum InfernoMonsters {

	MELEE(9020),
	RANGER(9021),
	MAGER(9030),
	JAD_HEALER(9031),
	JAD(9040),
	ANCESTRAL_GLYPH(9063),
	ZUK(9041),
	RANGER_BOSS(9064),
	MAGER_BOSS(9065),
	JAD_BOSS(9104),
	ZUK_HEALER(9062);

	private final int id;

	private InfernoMonsters(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
