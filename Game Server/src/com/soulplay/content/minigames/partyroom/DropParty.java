package com.soulplay.content.minigames.partyroom;

import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.game.event.Task;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class DropParty {

	public static final int PARTY_INTERFACE = 2156;
	public static final int PARTY_INV_INTERFACE = 2005;
	
	public static final int ITEMS_INVENTORY_INTERFACE = 2006;
	public static final int ITEMS_INV_SIZE = 8;
	
	public static final int ITEMS_OFFERED_INTERFACE = 2273;
	public static final int CHEST_ITEMS_SIZE = 216;
	
	public static final int ITEMS_OFFERING_INTERFACE = 2274;
	public static final int ITEMS_OFFERING_SIZE = 8;
	
	public static final int BALLOON_OBJECT_ID = 1234, PARTY_PETE_NPC_ID = 12234;
	
	private static final int MAX_BALLOONS = 80;
	
	public static final int STOMP_EMOTE = 794;
	
	private static final int DROP_DELAY = 50; // 500= 5 minutes
	
	private static final int PRICE_TO_START_PARTY = 50_000_000;
	
	private static final int BALLOON_ALIVE_TICK = 500; // 500=5minutes how long the balloon object is alive for.
	
	private static final int LOW_X = 2729, HIGH_X = 2746, LOW_Y = 3461, HIGH_Y = 3476;
	
	private static final ArrayList<Item> itemsToDrop = new ArrayList<Item>();
	
	public static ArrayList<Item> getItemstodrop() {
		return itemsToDrop;
	}
	
	private static int totalCashValue = 0;

	private static int totalToDrop = 0, dropped = 0;
	
	private static boolean isActive = false;
	
	private static boolean finishedDrop = false;
	
	private static long lastPull = 0;
	
	private static boolean lockedLever = false;
	private static boolean lockedChest = false;
	
	private static NPC partyPete;
	private static int peteCountdown = DROP_DELAY;
	
	//TODO: make sure items offered does not reach itemToDrop size past 216
	
	public static void openChest(Client c) {
		if (c.interfaceIdOpenMainScreen == 2156)
			return;
		
		if (!c.getPartyOffer().openInterface()) {
			c.sendMessage("Something went wrong. Code:33");
		}
	}
	
	private static void findPartyPete() {
		for (NPC n : Server.getRegionManager().getLocalNpcsByLocation(2730, 3469, 0)) {
			if (n.npcType == PARTY_PETE_NPC_ID) {
				DropParty.setPartyPete(n);
				break;
			}
		}
	}
	
	public static void pullLever() {
		if (isActive || itemsToDrop.isEmpty()) {
			return;
		}
		
		isActive = true;
		
		findPartyPete();
		
		if (totalCashValue > Integer.MAX_VALUE) {
			String msg = "Drop Party will begin in "+Misc.ticksIntoSeconds(DROP_DELAY)+" seconds in World: "+Config.NODE_ID+"! Cash Value:"+Misc.formatNumbersWithCommas(totalCashValue);
			//PlayerHandler.messageAllPlayersInAllWorlds(msg, "");
			PlayerHandler.messageAllPlayers(msg);
		}
		
		if (itemsToDrop.size() < CHEST_ITEMS_SIZE) { // fill in balloons with empties
			for (int i = itemsToDrop.size(); i < CHEST_ITEMS_SIZE; i++) {
				itemsToDrop.add(new Item(0, 0));
			}
		}
		
		totalToDrop = itemsToDrop.size();
		
		Server.getTaskScheduler().schedule(new Task() {
			int tick = 0;
			@Override
			protected void execute() {
				
				if (DropParty.finishedDrop && itemsToDrop.isEmpty() && tick > 20) {
					DropParty.reset();
					this.stop();
					return;
				} else if (DropParty.finishedDrop && !itemsToDrop.isEmpty() && tick > BALLOON_ALIVE_TICK) { // delete all items if drop party items were not picked up
					DropParty.reset();
					this.stop();
					return;
				}
				if (peteCountdown > 0) {
					if (getPartyPete() != null) {
						getPartyPete().forceChat(Integer.toString((int)Misc.ticksIntoSeconds(peteCountdown)));
					}
					peteCountdown--;
				}
				if (DropParty.dropped < DropParty.totalToDrop) {
					if (tick >= DropParty.DROP_DELAY) {
						for (int i = 0; i < 6; i++) {
							if (!DropParty.maxBalloons()) {
								//drop balloon
								DropParty.dropBalloon();
							} else {
								break;
							}
						}
					}
				} else {
					if (!DropParty.finishedDrop) {
						DropParty.finishedDrop = true;
						tick = 0;
					}
				}
				
				tick++;
			}
		});
	}
	
	private static int getBalloonCount() {
		int count = 0;
		for (RSObject o : ObjectManager.objects) {
			if (o.getTick() <= 0)
				continue;
			for (Balloons b : Balloons.values) {
				if (o.getNewObjectId() == b.getDroppingId()) {
					count++;
					break;
				}
			}
			if (count > MAX_BALLOONS)
				break;
		}
		return count;
	}
	
	private static boolean maxBalloons() {
		return getBalloonCount() >= MAX_BALLOONS;
	}
	
	private static void reset() {
		isActive = false;
		totalCashValue = totalToDrop = dropped = 0;
		itemsToDrop.clear();
		lockedChest = lockedLever = finishedDrop = false;
		reloadInterfaceForPlayers();
		peteCountdown = DROP_DELAY;
	}
	
//	private static final Location MID = Location.create(2734, 3469, 0);
	
	private static Location generateLoc() {
		for (int i = 0; i < 3; i++) {
			int ranX = LOW_X + Misc.random(HIGH_X - LOW_X);
			int ranY = LOW_Y + Misc.random(HIGH_Y - LOW_Y);
			if (RegionClip.getClipping(ranX, ranY, 0) != 0)
				continue;
//			for (RSObject rsO : Server.getRegionManager().getLocalRSObjects(MID)) { //toAdd is not used here! so don't use this, loop all for now :/
//				if (rsO.getX() == ranX && rsO.getY() == ranY)
//					continue;
//			}
			if (ObjectManager.objectInSpotTypeTen(ranX, ranY, 0))
				continue;
			return Location.create(ranX, ranY, 0);
		}
		
		return null;
	}
	
	private static void dropBalloon() {
		Location loc = generateLoc();
		if (loc == null)
			return;
		
		int objectId = Balloons.YELLOW.getDroppingId() + Misc.random(Balloons.values.length-1);
		
		ObjectManager.addObject(new RSObject(objectId, loc.getX(), loc.getY(), loc.getZ(), Misc.random(3), 10, -1, BALLOON_ALIVE_TICK));
		dropped++;
	}
	
	public static void popBalloon(Client c, GameObject o, Balloons ball) {
		if (c.isIronMan()) {
			c.sendMessage("Sorry but IronMan are now allowed to participate in Drop Parties.");
			return;
		}
		RSObject rsO = ObjectManager.getObjectByNewId(o.getId(), o.getX(), o.getY(), o.getZ());
		if (rsO == null || rsO.getTick() <= 0)
			return;

		rsO.setTick(0);
		
		c.startAnimation(STOMP_EMOTE);
		if (itemsToDrop.isEmpty()) {
			return;
		}
		// animate the object?
		ObjectManager.addObject(new RSObject(ball.getPopId(), o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), -1, 3));
		
		int ran = Misc.random(itemsToDrop.size()-1);
		Item item = itemsToDrop.get(ran);
		if (item == null) {
			c.sendMessage("WTF DID YOU DO?!?!?!!?!?");
		}
		if (item.getId() > 1)
			ItemHandler.createGroundItem(c, item.getId()-1, o.getX(), o.getY(), o.getZ(), item.getAmount());
		itemsToDrop.remove(item);
	}
	
	private static boolean canPullLever(Client c) {
		if (isActive) {
			c.sendMessage("The lever has already been pulled. Wait for drops to empty.");
			return false;
		}
		if (itemsToDrop.isEmpty()) {
			c.sendMessage("The Drop Party Chest is empty!");
			return false;
		}
		if (System.currentTimeMillis() - lastPull <= 60000 || (totalCashValue < 1_000_000_000 && itemsToDrop.size() < 10)) {
			c.sendMessage("Please add more items before pulling the lever.");
			if (System.currentTimeMillis() - lastPull > 60000)
				lastPull = System.currentTimeMillis();
			return false;
		}
		if (DropParty.lockedLever) {
			if (c.isMod() || c.isAdmin() || c.isOwner()) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}
	
	public static boolean usingPartyObject(Client c, GameObject o) {
		if (o.getId() == 160 && o.getX() == 2729 && o.getY() == 3468) { // lever
			
			String priceToPay = Misc.formatNumbersWithCommas(PRICE_TO_START_PARTY);
			if (c.getItems().playerHasItem(995, PRICE_TO_START_PARTY)) {
				c.getDialogueBuilder()
				.sendNpcChat(PARTY_PETE_NPC_ID, "Would you like to pull the lever for "+priceToPay+" GP?")
				.sendOption("Yes, pay to start the Drop Party!", ()-> {
					if (DropParty.canPullLever(c)) {
						if (c.getItems().deleteItem2(995, PRICE_TO_START_PARTY)) {
							pullLever();
							c.startAnimation(2140);
							c.sendMessage("You pull the lever.");
						} else {
							c.sendMessage("You don't have enough gold to start the party!");
						}
					}
				}, "No, I'm a party pooper...", ()-> {})
				.execute();
			} else {
				c.sendMessage("You need "+priceToPay+" Gold to start the drop party.");
			}
			return true;
		}
		if (o.getId() == 103 && o.getX() == 2729 && o.getY() == 3469) { // chest
			openChest(c);
			return true;
		}
		Balloons balloon = Balloons.forId(o.getId());
		if (balloon != null) {
			DropParty.popBalloon(c, o, balloon);
			return true;
		}
		return false;
	}
	
	public static boolean addToChest(Item item) {
		if (itemsToDrop.size() >= CHEST_ITEMS_SIZE)
			return false;
		itemsToDrop.add(item);
		long price = PriceChecker.getPriceLong(item.getId());
		if (price > 1)
			totalCashValue += price;
		return true;
	}
	
	public static boolean isActive() {
		return isActive;
	}
	
	public static void reloadInterfaceForPlayers() {
		for (Player p : Server.getRegionManager().getLocalPlayersByLocation(2730, 3469, 0)) {
			Client c = (Client)p;
			if (c.getPartyOffer().isOpen())
				c.getPartyOffer().loadRewardsInterface();
		}
	}

	public static boolean isLockedLever() {
		return lockedLever;
	}

	public static void setLockedLever(boolean lockedLever) {
		DropParty.lockedLever = lockedLever;
	}

	public static NPC getPartyPete() {
		return partyPete;
	}

	public static void setPartyPete(NPC pete) {
		partyPete = pete;
	}
	
	public static boolean spawnToChest(Item item) {
		if (!maxBalloons()) {
			itemsToDrop.add(item);
			return true;
		}
		return false;
	}

	public static boolean isLockedChest() {
		return lockedChest;
	}

	public static void setLockedChest(boolean lockedChest) {
		DropParty.lockedChest = lockedChest;
	}
	
}
