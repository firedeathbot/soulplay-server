package com.soulplay.content.minigames.partyroom;

public enum Balloons {
	
	YELLOW(115, 123, 499),
	RED(116, 124, 499),
	BLUE(117, 125, 499),
	GREEN(118, 126, 499),
	PURPLE(119, 127, 499),
	WHITE(120, 128, 499),
	BLUE_GREN(121, 129, 500),
	TRI_COLOR(122, 130, 501)
	;
	
	public static final Balloons[] values = values();
	
	private final int droppingId;
	private final int popId;
	private final int animId;
	
	private Balloons(int id, int popId, int anim) {
		this.droppingId = id;
		this.popId = popId;
		this.animId = anim;
	}

	public int getDroppingId() {
		return droppingId;
	}

	public int getPopId() {
		return popId;
	}

	public int getAnimId() {
		return animId;
	}
	
	public static Balloons forId(int objId) {
		for (Balloons b : Balloons.values)
			if (b.getDroppingId() == objId)
				return b;
		return null;
	}
}
