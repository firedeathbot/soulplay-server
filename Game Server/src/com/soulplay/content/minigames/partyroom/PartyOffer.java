package com.soulplay.content.minigames.partyroom;

import java.util.ArrayList;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;

public class PartyOffer {

	final Client c;
	
	private boolean isActive = false;
	
	private final ArrayList<Item> inventory = new ArrayList<Item>();
	
	private final ArrayList<Item> offered = new ArrayList<Item>();
	
	public PartyOffer(Client c) {
		this.c = c;
	}
	
	public boolean openInterface() {
		if (isActive) {
			return false;
		}

		inventory.clear();
		offered.clear();
		isActive = true;
		
		c.getPacketSender().openItemsInterface(DropParty.PARTY_INTERFACE, DropParty.PARTY_INV_INTERFACE);
		addInventory();
		fillOfferedItems();
		loadRewardsInterface();
		reloadOfferInterface();
		return true;
	}
	
	private void fillOfferedItems() {
		for (int i = 0; i < DropParty.ITEMS_OFFERING_SIZE; i++) {
			offered.add(new Item(0, 0, i));
		}
	}
	
	private void addInventory() {
		c.getPacketSender().resetInvenory(DropParty.ITEMS_INVENTORY_INTERFACE);

		for (int i = 0, length = c.playerItems.length; i < length; i++) {
			if (c.playerItems[i] <= 0) {
				continue;
			}

			inventory.add(new Item(c.playerItems[i], c.playerItemsN[i], i));
		}
		
		reloadInventory();
	}
	
	public void reloadInventory() {
		c.getPacketSender().updateInventoryPartial(DropParty.ITEMS_INVENTORY_INTERFACE, inventory);
//		for (Item item : inventory) {
//			c.getPacketSender().itemOnInterface(DropParty.ITEMS_INVENTORY_INTERFACE, item.getSlot(), (item.getAmount() == 0 ? -1 : item.getId()-1), item.getAmount());
//		}
	}
	
	public void loadRewardsInterface() {
		resetRewardItemsInterface();
		if (!DropParty.getItemstodrop().isEmpty()) {
			c.getPacketSender().itemsOnInterface(DropParty.ITEMS_OFFERED_INTERFACE, DropParty.getItemstodrop());
		}
	}
	
	private void resetRewardItemsInterface() {
		c.getPacketSender().itemOnInterface(DropParty.ITEMS_OFFERED_INTERFACE, 0);
//		c.getPacketSender().itemsOnInterface(DropParty.ITEMS_OFFERED_INTERFACE, new int[DropParty.CHEST_ITEMS_SIZE], new int[DropParty.CHEST_ITEMS_SIZE]);
	}
	
	private void reloadOfferInterface() {
		c.getPacketSender().itemsOnInterface(DropParty.ITEMS_OFFERING_INTERFACE, offered);
//		int slot = 0;
//		for (Item item : offered) {
//			c.getPacketSender().itemOnInterface(DropParty.ITEMS_OFFERING_INTERFACE, slot++, (item.getAmount() == 0 ? -1 : item.getId()-1), 0);
//		}
	}
	
	public void close() {
		isActive = false;
		inventory.clear();
		offered.clear();
	}
	
	public boolean isOpen() {
		return isActive;
	}
	
	public void depositItem(int slot, int amount) {
		if (!isOpen())
			return;
		Item item = getInvFromSlot(slot); // filled in null items so it's fine.
		if (item == null || item.getId() < 1)
			return;
		if (!c.getShops().allowSell(item.getId()-1)) {
			c.sendMessage("You cannot offer this item.");
			return;
		}
		boolean stackable = ItemProjectInsanity.itemIsStackable(item.getId()-1);
		if (!stackable) {
			for (int count = 0; count < amount; count++) {
				Item item2 = getInvForId(item.getId());
				if (item2 == null)
					break;
				Item freeOfferedSlot = getFreeSlotOffered();
				if (freeOfferedSlot == null) {
					c.sendMessage("You can only offer up to "+ DropParty.ITEMS_OFFERING_SIZE +" items.");
					break;
				}
				offered.add(new Item(item2.getId(), deleteInvItem(item2, amount), item2.getSlot()));
				offered.remove(freeOfferedSlot);
			}
		} else { // stackables
			
			Item offeredSlot = getOfferedForId(item.getId());
			if (offeredSlot == null)
				offeredSlot = getFreeSlotOffered();
			if (offeredSlot == null) {
				c.sendMessage("You can only offer up to "+ DropParty.ITEMS_OFFERING_SIZE +" items.");
				return;
			}
			int newAmount = deleteInvItem(item, amount) + offeredSlot.getAmount();
			offered.add(new Item(item.getId(), newAmount, item.getSlot()));
			offered.remove(offeredSlot);
			//TODO: finish stackables
			
		}
		reloadInventory();
		reloadOfferInterface();
	}
	
	private Item getInvFromSlot(int slot) {
		for (Item item : inventory) {
			if (item.getSlot() == slot)
				return item;
		}
		return null;
	}
	
	private Item getInvForId(int id) {
		for (Item item : inventory) {
			if (item.getId() == id)
				return item;
		}
		return null;
	}
	
	public void withdrawItem(int slot, int amount) {
		if (!isOpen())
			return;
		Item item = offered.get(slot);
		if (item == null || item.getId() < 1)
			return;
		
		boolean stackable = ItemProjectInsanity.itemIsStackable(item.getId()-1);
		if (!stackable) {
			int removed = 0;
			for (int k = 0; k < DropParty.ITEMS_OFFERING_SIZE; k++) {
				if (removed == amount)
					break;
				Item item2 = getOfferedForId(item.getId());
				if (item2 == null)
					break;
				Item prevItemSlot = getInvFromSlot(item2.getSlot());
				if (prevItemSlot == null) {
					break;
				}
				inventory.add(new Item(item.getId(), deleteOfferedItem(item2, amount), item2.getSlot()));
				inventory.remove(prevItemSlot);
			}
		} else { //stackables
			Item prevItem = getInvFromSlot(item.getSlot());
			if (prevItem == null) {
				return;
			}
			inventory.add(new Item(item.getId(), deleteOfferedItem(item, amount)+prevItem.getAmount(), item.getSlot()));
			inventory.remove(prevItem);
		}
			
		reloadInventory();
		reloadOfferInterface();
	}
	
	private int deleteInvItem(Item item, int amount) {
		int id = item.getId();
		if (amount >= item.getAmount()) {
			amount = item.getAmount();
			id = 0;
		}
		inventory.add(new Item(id, item.getAmount() - amount, item.getSlot()));
		inventory.remove(item);
		return amount;
	}
	
	private Item getFreeSlotOffered() {
		for (Item item : offered) {
			if (item.getAmount() == 0) {
				return item;
			}
		}
		return null;
	}
	
	private Item getOfferedForId(int id) {
		for (Item item : offered) {
			if (item.getId() == id)
				return item;
		}
		return null;
	}
	
	private int deleteOfferedItem(Item itemOffer, int amount) {
		int id = itemOffer.getId();
		if (amount >= itemOffer.getAmount()) {
			amount = itemOffer.getAmount();
			id = 0;
		}
		offered.add(new Item(id, itemOffer.getAmount() - amount, itemOffer.getSlot()));
		offered.remove(itemOffer);
		return amount;
	}
	
	public void accept() {
		boolean offeredItems = false;
		if (DropParty.isActive()) {
			c.sendMessage("Please wait till the Drop Party is over to offer more items.");
			return;
		}
		if (DropParty.isLockedChest() && (!c.isMod() && !c.isAdmin() && !c.isOwner() && !c.isDev())) {
			c.sendMessage("Only the mods+ can place items into the chest at the moment.");
			return;
		}
		if (!offered.isEmpty()) {
			for (Item item : offered) {
				if (item.getAmount() == 0)
					continue;
				if (c.getItems().deleteItemInOneSlot(item.getId()-1, item.getSlot(), item.getAmount())) {
					if (!DropParty.addToChest(item)) {
						c.sendMessage("The Drop Party Chest is full.");
						break;
					}
					offeredItems = true;
				} else {
					c.sendMess("cannot delete item slot:"+item.getSlot()+" amount:"+item.getAmount()+" itemID:"+item.getId());
					break;
				}
			}
			if (offeredItems) {
				c.sendMessage("You deposit your items into the Party Drop Chest.");
				DropParty.reloadInterfaceForPlayers();
				close();
				openInterface();
			} else {
				c.sendMessage("You must put something in the offer in order to accept.");
			}
		}
	}

}
