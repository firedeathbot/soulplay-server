package com.soulplay.content.minigames.junkmachine;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.content.shops.ShopPrice;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class JunkMachine {

	private static final int REWARD_AMOUNT = 500000; // 10m

	private static final int CHANCES = 20;

	public static Set<Integer> shopItems = new HashSet<>();

	public static void addShopItem(int itemId) {
		shopItems.add(itemId);
	}

	public static void cashJunkItem(final Client c, int itemId) {
		if (c.performingAction) {
			return;
		}
		final int randomNumber = Misc.random(CHANCES);
		if (!c.getItems().playerHasItem(itemId)) {
			return;
		}
		if (c.getItems().freeSlots() < 1) {
			c.sendMessage("You need at least one free slot in your inventory.");
			return;
		}
		if (shopItems.contains(itemId)) {
			c.sendMessage("You cannot use items that can be bought in stores.");
			return;
		}
		ItemDefinition itemDef = ItemDefinition.forId(itemId);
		if (itemDef != null) {
			if (itemDef.getDropValue() > REWARD_AMOUNT) {
				c.sendMessage("Sorry but you can't put in this item.");
				return;
			}
			if (itemDef.isStackable()) {
				c.sendMessage("You can't dispose stackable items.");
				return;
			}
		}
		if (ShopPrice.getItemShopValue(itemId) > REWARD_AMOUNT) {
			c.sendMessage("Sorry but you can't put in this item.");
			return;
		}
		if (ItemConstants.getItemName(itemId).toLowerCase().contains("potion")
				|| ItemConstants.getItemName(itemId).toLowerCase()
						.contains("vial")) {
			c.sendMessage("You cannot use potions on this.");
			return;
		}

		c.startAnimation(883);

		c.sendMessage("You put in " + ItemConstants.getItemName(itemId));
		c.getItems().deleteItemInOneSlot(itemId,
				1/* c.getItems().getItemCount(itemId) */);
		c.performingAction = true;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer e) {
				if (c == null || c.disconnected) {
					e.stop();
					return;
				}

				if (randomNumber == 1) {
					c.sendMessage("Congratulations! You won " + REWARD_AMOUNT
							+ " GP!");
					c.getItems().addItem(995, REWARD_AMOUNT);
				} else {
					c.sendMessage("Sorry, better luck next time!");
				}

				e.stop();

			}

			@Override
			public void stop() {
				if (c != null) {
					c.performingAction = false;
				}
			}
		}, 2);
	}

	public static int getRewardAmount() {
		return REWARD_AMOUNT;
	}

	public static void openInterface(Client c, int itemId) {
		c.dialogueAction = 3114;
		c.getTempVar().put("junkmachine", itemId);
		c.getDH().sendOption2(
				"Yes I would like to dispose my item - <col=ff0000>"
						+ ItemConstants.getItemName(itemId),
				"No I do not want to dispose my item.");
	}
}
