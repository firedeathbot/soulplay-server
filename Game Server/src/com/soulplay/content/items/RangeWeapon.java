package com.soulplay.content.items;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soulplay.game.world.entity.Animation;

/**
 * A class holding all the range weapon definitions.
 * @author Emperor
 */
public final class RangeWeapon {

	/**
	 * The range weapons mapping.
	 */
	private static final Map<Integer, RangeWeapon> RANGE_WEAPONS = new HashMap<>();

	/**
	 * The mapping containing all the possible ammunition types.
	 */
	private final List<Integer> ammunition;

	/**
	 * The item id.
	 */
	private final int itemId;

	/**
	 * The attack animation id.
	 */
	private final Animation animation;

	/**
	 * The attack speed.
	 */
	private final int attackSpeed;

	/**
	 * The equipment slot the ammunition uses, if any.
	 */
	private final int ammunitionSlot;

	/**
	 * The weapon type.
	 */
	private final int type;

	/**
	 * If we should drop ammo.
	 */
	private final boolean dropAmmo;

	private int combatDistance = 4;
	
	/**
	 * Constructs a new {@code RangeWeapon} {@code Object}.
	 * @param itemId The item id.
	 * @param animation The animation.
	 * @param attackSpeed The attack speed.
	 * @param ammunitionSlot The ammunition's equipment slot, or -1 if not worn.
	 * @param type The weapon type.
	 * @param dropAmmo If the ammunition should be dropped.
	 * @param ammunition The possible ammunition vector list.
	 */
	public RangeWeapon(int itemId, Animation animation, int attackSpeed, int ammunitionSlot, int type, boolean dropAmmo, List<Integer> ammunition) {
		this.itemId = itemId;
		this.animation = animation;
		this.attackSpeed = attackSpeed;
		this.ammunitionSlot = ammunitionSlot;
		this.type = type;
		this.dropAmmo = dropAmmo;
		this.ammunition = ammunition;
	}

	/**
	 * Gets the range weapons mapping.
	 * @return The range weapons.
	 */
	public static Map<Integer, RangeWeapon> getRangeWeapons() {
		return RANGE_WEAPONS;
	}

	/**
	 * Gets a range weapon instance from the mapping.
	 * @param id The item id.
	 * @return The instance.
	 */
	public static RangeWeapon get(int id) {
		return RANGE_WEAPONS.get(id);
	}

	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @return the animation
	 */
	public Animation getAnimation() {
		return animation;
	}

	/**
	 * @return the ammunitionSlot
	 */
	public int getAmmunitionSlot() {
		return ammunitionSlot;
	}

	/**
	 * @return the ammunition
	 */
	public List<Integer> getAmmunition() {
		return ammunition;
	}

	/**
	 * @return the attackSpeed
	 */
	public int getAttackSpeed() {
		return attackSpeed;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Gets the weapon type.
	 * @return The weapon type.
	 */
	public WeaponType getWeaponType() {
		return WeaponType.values[type];
	}

	/**
	 * @return the dropAmmo
	 */
	public boolean isDropAmmo() {
		return dropAmmo;
	}

	public int getCombatDistance() {
		return combatDistance;
	}

	public void setCombatDistance(int combatDistance) {
		this.combatDistance = combatDistance;
	}
}