package com.soulplay.content.items;

import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;

public class PotionMixing {

	public static void mixPotion2(Player c, int id, int id2) {
		if (ItemProjectInsanity.itemStackable[id] || ItemProjectInsanity.itemStackable[id2]) {
			return;
		}
		String id11 = ItemConstants.getItemName(id);
		String id22 = ItemConstants.getItemName(id2);
		if (id11.equals("Unarmed") || id22.equals("Unarmed")) {
			return;
		}
		if (id11.substring(0, id11.indexOf("("))
				.equalsIgnoreCase(id22.substring(0, id22.indexOf("(")))) {
			try {
				if (!id11
						.substring(id11.indexOf("(") + 1, id11.indexOf("(") + 2)
						.matches("[0-9]+") || !id22
								.substring(id22.indexOf("(") + 1,
										id22.indexOf("(") + 2)
								.matches("[0-9]+")) {
					return;
				}
				int amount1 = Integer.parseInt(id11.substring(
						id11.indexOf("(") + 1, id11.indexOf("(") + 2));
				int amount2 = Integer.parseInt(id22.substring(
						id22.indexOf("(") + 1, id22.indexOf("(") + 2));
				int totalAmount = amount1 + amount2;
				if (totalAmount > 4) {
					amount1 = 4;
					amount2 = totalAmount - 4;
					String item1 = id11.substring(0, id11.indexOf("(") + 1)
							+ amount1 + ")";
					String item2 = id11.substring(0, id11.indexOf("(") + 1)
							+ amount2 + ")";
					c.getItems().deleteItemInOneSlot(id,
							c.getItems().getItemSlot(id), 1);
					c.getItems().deleteItemInOneSlot(id2,
							c.getItems().getItemSlot(id2), 1);
					c.getItems();
					c.getItems().addItem(ItemConstants.getItemId(item1), 1);
					c.getItems();
					c.getItems().addItem(ItemConstants.getItemId(item2), 1);
				} else {
					amount1 = totalAmount;
					String item1 = id11.substring(0, id11.indexOf("(") + 1)
							+ amount1 + ")";
					c.getItems().deleteItemInOneSlot(id,
							c.getItems().getItemSlot(id), 1);
					c.getItems().deleteItemInOneSlot(id2,
							c.getItems().getItemSlot(id2), 1);
					c.getItems();
					c.getItems().addItem(ItemConstants.getItemId(item1), 1);
					c.getItems().addItem(229, 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
