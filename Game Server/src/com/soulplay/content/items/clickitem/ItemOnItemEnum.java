package com.soulplay.content.items.clickitem;

import java.util.HashMap;
import java.util.Map;

public enum ItemOnItemEnum {

	DRAGON_FULL_HELM_SPIKE(11335, 19354, 19341),
	DRAGON_PLATELEGS_SPIKE(4087, 19356, 19343),
	DRAGON_PLATESKIRT_SPIKE(4585, 19356, 19344),
	DRAGON_PLATEBODY_SPIKE(14479, 19358, 19342),
	DRAGON_SQUARE_SHIELD_SPIKE(1187, 19360, 19345),
	DRAGON_FULL_HELM_OR(11335, 19346, 19336),
	DRAGON_PLATELEGS_OR(4087, 19348, 19338),
	DRAGON_PLATESKIRT_OR(4585, 19348, 19339),
	DRAGON_PLATEBODY_OR(14479, 19350, 19337),
	DRAGON_SQUARE_SHIELD_OR(1187, 19352, 19340),
	DRAGON_SCIMITAR_OR(4587, 30218, 30219, true),
	NIGHTMARE_STAFF_HARMONISED(124422, 124511, 124423),
	NIGHTMARE_STAFF_VOLATILE(124422, 124514, 124424),
	NIGHTMARE_STAFF_ELDRITCH(124422, 124517, 124425),
	;

	public static final Map<Integer, ItemOnItemEnum> values = new HashMap<>();
	private final int itemId1, itemId2, productId;
	private final boolean irreversible;

	ItemOnItemEnum(int itemId1, int itemId2, int productId) {
		this(itemId1, itemId2, productId, false);
	}

	ItemOnItemEnum(int itemId1, int itemId2, int productId, boolean irreversible) {
		this.itemId1 = itemId1;
		this.itemId2 = itemId2;
		this.productId = productId;
		this.irreversible = irreversible;
	}

	public int getItemId1() {
		return itemId1;
	}

	public int getItemId2() {
		return itemId2;
	}

	public int getProductId() {
		return productId;
	}

	public boolean isIrreversible() {
		return irreversible;
	}

	static {
		for (ItemOnItemEnum data : values()) {
			values.put(data.getItemId1(), data);
			values.put(data.getItemId2(), data);
		}
	}

}
