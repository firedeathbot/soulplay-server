package com.soulplay.content.items.clickitem;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.content.items.ItemIds;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.sounds.Sounds;

public enum Potion {
	ANTI_VENOM(5943, 5945, 5947, 5949) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getDotManager().curePoison(1500);
				c.getDotManager().cureVenom(300);
			}
			return true;
		}
	},
	PRAYER_FLASK(14207, 14209, 14211, 14213, 14215) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().prayerRenewal();//c.getPotions().prayerPotion();
			}
			return true;
		}
	},
	ENERGY_FLASK(14217, 14219, 14221, 14223, 14225) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().staminaPotionEffect();
			}
			return true;
		}
	},
	SUPER_ATTACK_FLASK(14227, 14229, 14231, 14233, 14235) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().enchanceStat2(Skills.ATTACK, true);
			}
			return true;
		}
	},
	SUPER_STR_FLASK(14237, 14239, 14241, 14243, 14245) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().enchanceStat2(Skills.STRENGTH, true);
			}
			return true;
		}
	},
	RANGING_FLASK(14247, 14249, 14251, 14253, 14255) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().enchanceStat2(Skills.RANGED, true);
			}
			return true;
		}
	},
	DEFENSE_FLASK(14257, 14259, 14261, 14263, 14265) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().enchanceStat2(Skills.DEFENSE, true);
			}
			return true;
		}
	},
	MAGIC_FLASK(14267, 14269, 14271, 14273, 14275) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getPotions().enchanceStat2(Skills.MAGIC, true);
			}
			return true;
		}
	},
	SUMMONING_FLASK(14277, 14279, 14281, 14283, 14285),//summoning is shit anyways
	GUTHIX_BREW(4417, 4419, 4421, 4423, ItemIds.EMPTY_CUP) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				if (!c.getDotManager().convertVenomToPoison()) {
					if (c.getDotManager().isPoisonActive()) {
						int damage = c.getDotManager().getPoison().getDamage();
						if (damage > 1) {
							c.getDotManager().getPoison().setDamage(damage - 1);
						}
					}
				}

				c.getMovement().restoreRunEnergy(5.0);
				final int heal = 5;
				final int max = c.calculateMaxLifePoints() + 5;
				if (c.getSkills().getLifepoints() < max) {
					if (c.getSkills().getLifepoints() + heal > max) {
						c.getAchievement().heal50000HP(heal - (max - c.getSkills().getLifepoints()));
					} else {
						c.getAchievement().heal50000HP(heal);
					}
				}

				c.getSkills().updateLevel(Skills.HITPOINTS, heal, max);
			}
			return true;
		}
	},
	AGILITY_POTION(3032, 3034, 3036, 3038) {
		@Override
		public boolean drink(Client c, Item item) {
			if (super.drink(c, item)) {
				c.getSkills().updateLevel(Skills.AGILITY, 3);
			}
			return true;
		}
	},
	
	
	
	;
	
	private int[] ids;
	
	private Potion(int... ids) {
		this.ids = ids;
	}
	
	public int[] getIds() {
		return ids;
	}
	
	private boolean canDrink(Client c) {
		return Server.getTotalTicks() >= c.getPotionTick();
	}
	
	public boolean drink(Client c, Item item) {
		if (c.duelRule[DuelRules.NO_DRINKS]) {
			c.sendMessage("You may not drink potions in this duel.");
			return false;
		}
		if (c.clanWarRule[CWRules.NO_DRINKS]) {
			c.sendMessage("You may not drink potions in this war.");
			return false;
		}
		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			return false;
		}
		if (!canDrink(c) || c.isDead() || c.getHitPoints() <= 0) {
			return false;
		}
		if (c.getZones().isInDmmLobby() && Tournament.pre_settings.isSpawnGear()) {
			c.sendMessage("You can't do that right now.");
			return false;
		}
		if (c.isLockActions()) {
			return false;
		}

		if (c.getItems().playerHasItemInSlot(item.getId(), item.getSlot())) {
			int nextStep = 0;
			for (int step : ids) {
				nextStep++;
				if (item.getId() == step)
					break;
			}
			if (nextStep == ids.length) { // last sip
				if (c.isBreakVials())
					c.getItems().deleteItemInSlot(item.getSlot());
				else
					c.getItems().replaceItemSlot(ItemIds.VIAL, item.getSlot());
			} else {
				int itemId = getIds()[nextStep];
				if (itemId == -1)
					c.getItems().deleteItemInSlot(item.getSlot());
				else
					c.getItems().replaceItemSlot(getIds()[nextStep], item.getSlot());
			}
			c.startAnimation(DRINK_ANIM);
			c.getCombat().resetPlayerAttack();
			c.setPotionTick();
			c.getPacketSender().playSound(Sounds.DRINK_POT);
			return true;
		}
		return false;
	}
	
	private static final Map<Integer, Potion> potions = new HashMap<>();
	
	public static final Animation DRINK_ANIM = new Animation(829);
	
	public static Potion forId(int itemId) {
		return potions.get(itemId);
	}
	
	static {
		for (Potion p : Potion.values()) {
			for (int id : p.getIds()) {
				if (id == -1 || id == ItemIds.EMPTY_CUP)
					continue;
				potions.put(id, p);
			}
		}
	}

}
