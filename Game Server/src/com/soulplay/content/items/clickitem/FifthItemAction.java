package com.soulplay.content.items.clickitem;

import java.util.HashMap;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.items.degradeable.DegradeData;
import com.soulplay.content.items.impl.DuellistsCap;
import com.soulplay.content.items.urn.SkillingUrn;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.items.useitem.DonationChest;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.dungeoneeringv2.BindingData;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonItems;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.game.model.item.DroppedItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.SerpentineHelm;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.sounds.Sounds;

public enum FifthItemAction {
	DROP_ITEM() {
		@Override
		public void init(Player c, Item item) {
			if (c.playerRights == 2 && !DBConfig.ADMIN_DROP_ITEMS && Config.RUN_ON_DEDI) {
				c.getDialogueBuilder().sendNpcChat(2163, "You are dropping itemID: "+item.getId(),
						"Admins are not allowed to drop. Would you like to destroy the item?").
				sendOption("Yes destroy the item ID:"+item.getId(), ()-> {
					c.getItems().deleteItem(item);
				}, "No, I would like to keep it.", ()-> {
					c.getPA().closeAllWindows();
				})
				.execute();
				return;
			}

			if (c.getZones().isInDmmLobby() && Tournament.pre_settings.isSpawnGear()) {
				c.getItems().deleteItemInSlot(item.getSlot());
				return;
			}

			if (c.isIronMan() && item.getId() == DonationChest.KEY) {
				c.getDialogueBuilder().sendDestoryItem(item.getId()).execute();
				return;
			}

			if (c.playerIsInHouse) {
				ConstructionUtils.dropItemsInPoh(c, item, c.getX(), c.getY());
				return;
			}

			if (DungeonItems.drop(c, item)) {
				c.getPacketSender().playSound(Sounds.DROP_ITEM);
				return;
			}

			if (c.insideInstance()) {
				if (c.getItems().deleteItem(item)) {
					c.droppingItem = true;
					c.getInstanceManager().spawnBelow(item, c);
					c.droppingItem = false;
				}

				return;
			}

			if (c.getItems().deleteItem(item)) {
				c.getPacketSender().playSound(Sounds.DROP_ITEM);
				c.droppingItem = true;
				ItemHandler.createGroundItem(c, item.getId(), c.getX(), c.getY(), c.getHeightLevel(), item.getAmount());
				c.droppingItem = false;

				if (DBConfig.LOG_DROP) {
					Server.getLogWriter().addToDropList(new DroppedItem(c, item.getId(), item.getAmount(), "drop"));
				}
			}
		}
	},
	DESTROY() {
		@Override
		public void init(Player c, Item item) {
			c.getPA().destroyItem(item.getId(), c.getItems().getItemAmount(item.getId(), item.getSlot()),
					() -> {
						c.getItems().deleteItemInSlot(item.getSlot());
					}, null, "Are you sure you want to drop this item?", "This item is valuable, you will not", "get it back once lost.");
		}
	},
	UNBIND() {
		@Override
		public void init(Player c, Item item) {
			if (!c.insideDungeoneering()) {
				c.sendMessage("You hacker!");
				c.getItems().deleteItem(item);
				return;
			}

			int index = c.getBinds().findBound(item.getId());
			if (index == -1) {
				if (c.getBinds().unBindAmmunition(item.getId())) {
					return;
				}

				c.sendMessage("You hacker!");
				c.getItems().deleteItem(item);
				return;
			}

			c.getBinds().deleteBind(index, null);
		}
	},
	PET_DROP() {
		@Override
		public void init(Player c, Item item) {
			Pet.spawnPet(c, Pet.getPetForItem(item.getId()), item.getId(), item.getSlot());
		}
	},
	UNDROPPABLE() {
		@Override
		public void init(Player c, Item item) {
			c.sendMessage("You cannot drop this item.");
		}
	},
	UNCHARGE_BLOWPIPE() {
		@Override
		public void init(Player c, Item item) {
			BlowPipeCharges.uncharge(c);
		}
	},
	UNCHARGE_SERP_HELM() {
		@Override
		public void init(Player c, Item item) {
			if (Tournament.insideArenaWithSpawnedGear(c)) {
				c.sendMessage("Can't do that here.");
				return;
			}
			if (c.getItems().freeSlots() < 1) {
				c.sendMessage("You need at least 1 inventory slots open to uncharge this item.");
				return;
			}
			if (c.getItems().deleteItem(item)) {
				c.getItems().addItem(SerpentineHelm.SERP_HELM.getUnchargedId(), 1);
				int charges = c.resetCharge(item.getId());
				if (charges > 0)
					c.getItems().addItem(BlowPipeCharges.ZULRAH_SCALES, charges);
			}
		}
	},
	UNCHARGE_MAGMA_HELM() {
		@Override
		public void init(Player c, Item item) {
			if (Tournament.insideArenaWithSpawnedGear(c)) {
				c.sendMessage("Can't do that here.");
				return;
			}
			if (c.getItems().freeSlots() < 1) {
				c.sendMessage("You need at least 1 inventory slots open to uncharge this item.");
				return;
			}
			if (c.getItems().deleteItem(item)) {
				c.getItems().addItem(SerpentineHelm.MAGMA_HELM.getUnchargedId(), 1);
				int charges = c.resetCharge(item.getId());
				if (charges > 0)
					c.getItems().addItem(BlowPipeCharges.ZULRAH_SCALES, charges);
			}
		}
	},
	UNCHARGE_TANZANITE_HELM() {
		@Override
		public void init(Player c, Item item) {
			if (Tournament.insideArenaWithSpawnedGear(c)) {
				c.sendMessage("Can't do that here.");
				return;
			}
			if (c.getItems().freeSlots() < 1) {
				c.sendMessage("You need at least 1 inventory slots open to uncharge this item.");
				return;
			}
			if (c.getItems().deleteItem(item)) {
				c.getItems().addItem(SerpentineHelm.TANZANITE_HELM.getUnchargedId(), 1);
				int charges = c.resetCharge(item.getId());
				if (charges > 0)
					c.getItems().addItem(BlowPipeCharges.ZULRAH_SCALES, charges);
			}
		}
	},
	UNCHARGE_TSOTD() {
		@Override
		public void init(Player c, Item item) {
			if (Tournament.insideArenaWithSpawnedGear(c)) {
				c.sendMessage("Can't do that here.");
				return;
			}
			if (c.getItems().freeSlots() < 1) {
				c.sendMessage("You need at least 1 inventory slots open to uncharge this item.");
				return;
			}
			if (c.getItems().deleteItem(item)) {
				c.getItems().addItem(DegradeData.TSOTD_UNCHARGED, 1);
				int charges = c.resetCharge(item.getId());
				if (charges > 0)
					c.getItems().addItem(BlowPipeCharges.ZULRAH_SCALES, charges);
			}
		}
	};
	
	private FifthItemAction() {
		/* empty */
	}
	
	public void init(Player c, Item item) {
		/* empty */
	}

	private static HashMap<Integer, FifthItemAction> map = new HashMap<>();
	
	public static FifthItemAction get(int id) {
		return map.get(id);
	}
	
	public static void loadMap() {
		map.clear();
		
		//zulrah items
		map.put(30011, UNCHARGE_TSOTD);
		map.put(30003, UNCHARGE_SERP_HELM);
		map.put(30315, UNCHARGE_MAGMA_HELM);
		map.put(30317, UNCHARGE_TANZANITE_HELM);
		map.put(BlowPipeCharges.BLOWPIPE_FULL, UNCHARGE_BLOWPIPE);
		
		//undroppable junk
//		for (int itemId : DBConfig.UNDROPPABLE_ITEMS)
//			map.put(itemId, UNDROPPABLE);
		for (int id : DBConfig.DUNG_REWARDS)
			map.put(id, UNDROPPABLE);
//		for (CompletionistCape cape : CompletionistCape.values)
//			map.put(cape.getId(), UNDROPPABLE);

		for (int itemId : Config.destroyableItems)
			map.put(itemId, DESTROY);

		for (SkillingUrn urn : SkillingUrns.URNS) {
			map.put(urn.getFull(), DESTROY);
			map.put(urn.getActive(), DESTROY);
		}

		for (BindingData data : BindingData.values.values()) {
			map.put(data.getBindItemId(), UNBIND);
		}

		for (Pet pet : Pet.values())
			map.put(pet.getItemId(), PET_DROP);

		for (int itemId : UNDROPPABLE_ITEMS)
			map.put(itemId, UNDROPPABLE);

		for (DuellistsCap cap : DuellistsCap.values()) {
			map.put(cap.id, DESTROY);
		}
	}

	static int[] UNDROPPABLE_ITEMS = new int[] {13739, 13741, 13743, 13745, ResourceArea.PACKAGE_ITEM_ID};

}
