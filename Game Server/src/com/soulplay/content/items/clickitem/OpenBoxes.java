package com.soulplay.content.items.clickitem;

import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class OpenBoxes {
	
	private static final int GOLD_CHARM = 12158;
	private static final int GREEN_CHARM = 12159;
	private static final int CRIMSON_CHARM = 12160;
	private static final int BLUE_CHARM = 12163;
	
	private static final int charmPackage = 15246;
	private static final int superMysteryBox = 30174;
	private static final int legendaryMysteryBox = 30175;
	private static final int ultraMysteryBox = 30176;
	
	public static void charmPackage(Client c) { // charm package
		if (c.getItems().playerHasItem(charmPackage) && (c.getItems().freeSlots() > 3)) {
			c.getItems().deleteItemInOneSlot(charmPackage, 1);
			c.getItems().addItem(GOLD_CHARM, 5 + Misc.random(20));
			c.getItems().addItem(GREEN_CHARM, 5 + Misc.random(20));
			c.getItems().addItem(CRIMSON_CHARM, 5 + Misc.random(10));
			c.getItems().addItem(BLUE_CHARM, 5 + Misc.random(10));
		} else {
			c.sendMessage("You need 4 free slots.");
		}
	}
	
	public static void superMysteryBox(Client c) {
		if (c.getItems().playerHasItem(superMysteryBox)) {
			c.getItems().deleteItemInOneSlot(superMysteryBox, 1);
			MysteryBoxLoot.superBoxLoot(c);
		}
	}
	
	public static void legendaryMysteryBox(Client c) {
		if (c.getItems().playerHasItem(legendaryMysteryBox)) {
			c.getItems().deleteItemInOneSlot(legendaryMysteryBox, 1);
			MysteryBoxLoot.legendaryBoxLoot(c);
		}
	}
	
	public static void ultraMysteryBox(Client c) {
		if (c.getItems().playerHasItem(ultraMysteryBox)) {
			c.getItems().deleteItemInOneSlot(ultraMysteryBox, 1);
			MysteryBoxLoot.ultraBoxLoot(c);
		}
	}
	
}
