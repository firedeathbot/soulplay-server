package com.soulplay.content.items.clickitem;

import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.util.Misc;

public class MysteryBoxLoot {
	//itemId, minAmount, maxAmount, itemId2
	
	public static final int[][] COMMON = { 
			{6731, 1, 1}, // seers ring
			{6733, 1, 1}, // archers ring
			{6735, 1, 1}, // warrior ring
			{6737, 1, 1}, // berserker ring
			{12605, 1, 1}, // treasonous ring
			{12601, 1, 1}, // ring of the gods
			{12603, 1, 1}, // tyrannical ring
			{4708, 1, 1}, // ahrims hood
			{4710, 1, 1}, // ahrims staff
			{4712, 1, 1}, // ahrims robetop
			{4714, 1, 1}, // ahrims robeskirt
			{4716, 1, 1}, // dharoks helm
			{4718, 1, 1}, // dharoks greataxe
			{4720, 1, 1}, // dharoks platebody
			{4722, 1, 1}, // dharoks platelegs
			{4724, 1, 1}, // guthans helm
			{4726, 1, 1}, // guthans warspear
			{4728, 1, 1}, // guthans platebody
			{4730, 1, 1}, // guthans platelegs
			{4732, 1, 1}, // karils coif
			{4734, 1, 1}, // karils crossbow
			{4736, 1, 1}, // karils leathertop
			{4738, 1, 1}, // karils leatherskirt
			{4745, 1, 1}, // torags helm
			{4747, 1, 1}, // torags hammers
			{4749, 1, 1}, // torags platebody
			{4751, 1, 1}, // torags platelegs
			{4753, 1, 1}, // veracs helm
			{4755, 1, 1}, // veracs flail
			{4757, 1, 1}, // veracs brassard
			{4759, 1, 1}, // veracs plateskirt
			{13663, 1, 2}, // spin tickets
			{13663, 1, 2}, // spin tickets
			{13663, 1, 2}, // spin tickets
			{4151, 1, 1}, // abyssal whip
			{11286, 1, 1}, // visage
			{11235, 1, 1}, // dark bow
			{13887, 1, 1}, // vesta's chainbody
			{13893, 1, 1}, // vesta's plateskirt
			{13899, 1, 1}, // vesta's longsword
			{13905, 1, 1}, // vesta's spear
			{13884, 1, 1}, // statiu's platebody
			{13890, 1, 1}, // statiu's platelegs
			{13896, 1, 1}, // statiu's full helm
			{13902, 1, 1}, // statiu's warhammer
			{13858, 1, 1}, // zuriel's robe top
			{13861, 1, 1}, // zuriel's robe bottom
			{13864, 1, 1}, // zuriel's hood
			{13867, 1, 1}, // zuriel's staff
			{13870, 1, 1}, // morrigan's leather body
			{13873, 1, 1}, // morrigan's leather chaps
			{13876, 1, 1}, // morrigan's coif
			{13879, 1, 100}, // morrigan's javelin
			{13883, 1, 100}, // morrigan's throwin axe
			{537, 40, 60}, // dragon bones
			{18831, 20, 30}, // frost dragon bones
			{30174, 1, 1}, // super mystery box
			{995, 20_000_000, 19_000_000}, // coins
	};
	
	public static final int[][] UNCOMMON = { 
			{15241, 1, 1}, // hand cannon
			{21371, 1, 1}, // vine whip
			{11730, 1, 1}, // saradomin sword
			{15486, 1, 1}, // staff of light
			{11722, 1, 1}, // armadyl plateskirt
			{11720, 1, 1}, // armadyl chestplate
			{11718, 1, 1}, // armadyl helmet
			{15259, 1, 1}, // dragon pickaxe
			{11728, 1, 1}, // bandos boots
			{11726, 1, 1}, // bandos tasset
			{11724, 1, 1}, // bandos chestplate
			{11704, 1, 1}, // bandos hilt
			{11706, 1, 1}, // saradomin hilt
			{11708, 1, 1}, // zamorak hilt
			{11926, 1, 1}, // odium ward
			{11924, 1, 1}, // meladiction ward
			{12004, 1, 1}, // abyssal tentacle
			{30174, 1, 1}, // super mystery box
			{995, 33_000_000, 46_000_000}, // coins	
			
			
	};

	public static final int[][] RARE = { 
			{14484, 1, 1}, //dragon claws
			{11702, 1, 1}, // armadyl hilt	
			{20135, 1, 1}, // torva full helm	
			{20139, 1, 1}, // torva platebody	
			{20143, 1, 1}, // torva platelegs	
			{20147, 1, 1}, // pernix cowl	
			{20151, 1, 1}, // pernix body	
			{20155, 1, 1}, // pernix chaps	
			{20159, 1, 1}, // virtus mask	
			{20163, 1, 1}, // virtus robe top	
			{20167, 1, 1}, // virtus robe legs	
			{21787, 1, 1}, // steadfast boots	
			{21793, 1, 1}, // ragefire boots	
			{21790, 1, 1}, // glaiven boots	
			{13740, 1, 1}, // divine spirit shield	
			{13742, 1, 1}, // elysian spirit shield	
			{13738, 1, 1}, // arcane spirit shield	
			{13744, 1, 1}, // spectral spirit shield
			{20171, 1, 1}, // zaryte bow
			{19780, 1, 1}, // korasi's sword	
			{12924, 1, 1}, // blowpipe
			{30022, 1, 1}, // dragon warhammer
			{11791, 1, 1}, // staff of the dead
			{30002, 1, 1}, // serpentine helm
			{15220, 1, 1}, // berserker ring (i)
			{15020, 1, 1}, // warrior ring (i)
			{15019, 1, 1}, // archers ring (i)
			{15018, 1, 1}, // seers ring (i)
			{12604, 1, 1}, // tyrannical ring (i)
			{12606, 1, 1}, // treasonous ring (i)
			{12602, 1, 1}, // ring of the gods (i)
			{13663, 1, 4}, // spin tickets	
			{13663, 1, 4}, // spin tickets	
			{4273, 1, 1}, // golden key	
			{30174, 1, 1}, // super mystery box	
			{995, 78_000_000, 90_000_000}, // coins		
	};
	
	public static final int[][] SUPER_RARE = { 
			{30092, 1, 1}, // twisted bow
			{30091, 1, 1}, // twisted buckler
			{30136, 1, 1}, // ranger gloves
			{30087, 1, 1}, // dragon hunter crossbow
			{786, 1, 1}, // $10 scroll
			{30175, 1, 1}, // legendary box
			{30176, 1, 1}, // ultra box
			{995, 1_500_000_000, 1_800_000_000}, // coins
			
	};
	
	public static final int[][] EXTREMELY_RARE = { 
			{1050, 1, 1}, // santa hat
			{1050, 1, 1}, // santa hat
			{1053, 1, 1}, // green h'ween mask
			{1055, 1, 1}, // blue h'ween mask
			{1057, 1, 1}, // red h'ween mask
			{1053, 1, 1}, // green h'ween mask
			{1055, 1, 1}, // blue h'ween mask
			{1057, 1, 1}, // red h'ween mask
			{1037, 1, 1}, // bunny ears
			{1037, 1, 1}, // bunny ears
			{2581, 1, 1}, // robin hood hat
			{30137, 1, 1}, // ranger's tunic
			{1038, 1, 1}, // red partyhat
			{1040, 1, 1}, // yellow partyhat
			{1042, 1, 1}, // blue partyhat
			{1044, 1, 1}, // green partyhat
			{1046, 1, 1}, // purple partyhat
			{1048, 1, 1}, // white partyhat
			{4084, 1, 1}, // sled
			{1505, 1, 1}, // $50 scroll
			{20061, 1, 1}, // random pet box
			{30176, 1, 1}, // ultra box
	};
	
	public static final int[][] ULTRA_RARE = { 
			{30058, 1, 1}, //black h'ween mask
			{15740, 1, 1}, // black santa hat
			{15741, 1, 1}, // black partyhat
			{15742, 1, 1}, // pink partyhat
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{2396, 1, 1}, // $100 scroll
			{24338, 1, 1}, // Royal Crossbow
			
	};
	
    public static void superBoxLoot(Player p) {
		
		int random = Misc.randomNoPlus(1000);
		if (random <= 1) {
			loot(p, ULTRA_RARE);
		} else if (random <= 7) {
			loot(p, EXTREMELY_RARE);
		} else if (random <= 17) {
			loot(p, SUPER_RARE);
		} else if (random <= 54) {
			loot(p, RARE);
		} else if (random <= 400) {
			loot(p, UNCOMMON);
		} else if (random <= 1000) {
			loot(p, COMMON);
		}
	}
    
     public static void legendaryBoxLoot(Player p) {
		
 		int random = Misc.randomNoPlus(1000);
 		if (random <= 2) {
 			loot(p, ULTRA_RARE);
 		} else if (random <= 17) {
 			loot(p, EXTREMELY_RARE);
 		} else if (random <= 37) {
 			loot(p, SUPER_RARE);
 		} else if (random <= 117) {
 			loot(p, RARE);
 		} else if (random <= 500) {
 			loot(p, UNCOMMON);
 		} else if (random <= 1000) {
 			loot(p, COMMON);
 		}
	}
 
     public static void ultraBoxLoot(Player p) {
		
 		int random = Misc.randomNoPlus(1000);
 		if (random <= 5) {
 			loot(p, ULTRA_RARE);
 		} else if (random <= 29) {
 			loot(p, EXTREMELY_RARE);
 		} else if (random <= 51) {
 			loot(p, SUPER_RARE);
 		} else if (random <= 161) {
 			loot(p, RARE);
 		} else if (random <= 600) {
 			loot(p, UNCOMMON);
 		} else if (random <= 1000) {
 			loot(p, COMMON);
 		}
 	}
	
	private static void loot(Player c, int[][] items) {
		int random = Misc.randomNoPlus(items.length);

		c.getItems().addOrDrop(new GameItem(items[random][0], Misc.randomNoPlus(items[random][1], items[random][2])));

		c.sendMessage("You received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " mystery box!");
		if (items == ULTRA_RARE || items == EXTREMELY_RARE) {
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + c.getNameSmartUp() + " received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from a mystery box!");
		}
	}
	
	public static void easyChest(Player p) {
		
		int random = Misc.randomNoPlus(1000);
		if (random == 0) {
			lootLarrans(p, ULTRA_RARE);
		} else if (random <= 1) {
			lootLarrans(p, EXTREMELY_RARE);
		} else if (random <= 17) {
			lootLarrans(p, SUPER_RARE);
		} else if (random <= 54) {
			lootLarrans(p, RARE);
		} else if (random <= 400) {
			lootLarrans(p, UNCOMMON);
		} else if (random <= 1000) {
			lootLarrans(p, COMMON);
		}
	}
    
     public static void hardChest(Player p) {
		
 		int random = Misc.randomNoPlus(1000);
 		if (random <= 1) {
 			lootLarrans(p, ULTRA_RARE);
 		} else if (random <= 3) {
 			lootLarrans(p, EXTREMELY_RARE);
 		} else if (random <= 40) {
 			lootLarrans(p, SUPER_RARE);
 		} else if (random <= 120) {
 			lootLarrans(p, RARE);
 		} else if (random <= 500) {
 			lootLarrans(p, UNCOMMON);
 		} else if (random <= 1000) {
 			lootLarrans(p, COMMON);
 		}
	}
	
	private static void lootLarrans(Player c, int[][] items) {
		int random = Misc.randomNoPlus(items.length);

		c.getItems().addOrDrop(new GameItem(items[random][0], Misc.randomNoPlus(items[random][1], items[random][2])));

		c.sendMessage("You received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from Larran's chest!");
		if (items == ULTRA_RARE || items == EXTREMELY_RARE) {
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + c.getNameSmartUp() + " received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from Larran's Chest!");
		}
	}
	
}
