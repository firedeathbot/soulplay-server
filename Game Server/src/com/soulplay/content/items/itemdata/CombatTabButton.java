package com.soulplay.content.items.itemdata;

import com.soulplay.content.combat.data.FightExp;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Animation;

public class CombatTabButton {
	
	public static final CombatTabButton UNARMED_PUNCH = new CombatTabButton(422, FightExp.MELEE_ATTACK, MeleeSwingStyle.CRUSH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton UNARMED_KICK = new CombatTabButton(423, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton UNARMED_BLOCK = new CombatTabButton(422, FightExp.MELEE_DEFENSE, MeleeSwingStyle.CRUSH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton SWORD_STAB = new CombatTabButton(PlayerConstants.EMOTE_STAB_SWORD, FightExp.MELEE_ATTACK, MeleeSwingStyle.STAB, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton SWORD_LUNGE = new CombatTabButton(PlayerConstants.EMOTE_STAB_SWORD, FightExp.MELEE_STRENGTH, MeleeSwingStyle.STAB, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton SWORD_SLASH = new CombatTabButton(PlayerConstants.EMOTE_SLASH_SWORD, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton SWORD_BLOCK = new CombatTabButton(PlayerConstants.EMOTE_STAB_SWORD, FightExp.MELEE_DEFENSE, MeleeSwingStyle.STAB, WeaponAttackTypes.DEFENSIVE);

	public static final CombatTabButton WHIP_FLICK = new CombatTabButton(1658, FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton WHIP_LASH = new CombatTabButton(1658, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton WHIP_DEFLECT = new CombatTabButton(1658, FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);

	public static final CombatTabButton TWO_H_CHOP = new CombatTabButton(7041, FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton TWO_H_SLASH = new CombatTabButton(7041, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton TWO_H_SMASH = new CombatTabButton(7048, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton TWO_H_BLOCK = new CombatTabButton(7049, FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);

	public static final CombatTabButton SHORTBOW_ACC = new CombatTabButton(426, FightExp.RANGED, MeleeSwingStyle.OTHER, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton SHORTBOW_RAPID = new CombatTabButton(426, FightExp.RANGED, MeleeSwingStyle.OTHER, WeaponAttackTypes.RAPID);
	public static final CombatTabButton SHORTBOW_LONG = new CombatTabButton(426, FightExp.RANGED_DEFENSE, MeleeSwingStyle.OTHER, WeaponAttackTypes.LONG_RANGED);
	
	public static final CombatTabButton HALBERD_JAB = new CombatTabButton(440, FightExp.MELEE_SHARED, MeleeSwingStyle.STAB, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton HALBERD_SWIPE = new CombatTabButton(440, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton HALBERD_FEND = new CombatTabButton(440, FightExp.MELEE_DEFENSE, MeleeSwingStyle.STAB, WeaponAttackTypes.DEFENSIVE);

	public static final CombatTabButton COMBAT_STICK_BASH = new CombatTabButton(414, FightExp.MELEE_ATTACK, MeleeSwingStyle.CRUSH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton COMBAT_STICK_POUND = new CombatTabButton(414, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton COMBAT_STICK_BLOCK = new CombatTabButton(414, FightExp.MELEE_DEFENSE, MeleeSwingStyle.CRUSH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton LONGSWORD_CHOP = new CombatTabButton(PlayerConstants.EMOTE_SLASH_LONGSWORD, FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton LONGSWORD_SLASH = new CombatTabButton(PlayerConstants.EMOTE_SLASH_LONGSWORD, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton LONGSWORD_LUNGE = new CombatTabButton(PlayerConstants.EMOTE_STAB_LONGSWORD, FightExp.MELEE_SHARED, MeleeSwingStyle.STAB, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton LONGSWORD_BLOCK = new CombatTabButton(PlayerConstants.EMOTE_SLASH_LONGSWORD, FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton MAUL_HAMMER_POUND = new CombatTabButton(401, FightExp.MELEE_SHARED, MeleeSwingStyle.CRUSH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton MAUL_HAMMER_PUMMEL = new CombatTabButton(401, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton MAUL_HAMMER_BLOCK = new CombatTabButton(401, FightExp.MELEE_DEFENSE, MeleeSwingStyle.CRUSH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton CLAWS_CHOP = new CombatTabButton(393, FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton CLAWS_SLASH = new CombatTabButton(393, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton CLAWS_LUNGE = new CombatTabButton(393, FightExp.MELEE_SHARED, MeleeSwingStyle.STAB, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton CLAWS_BLOCK = new CombatTabButton(393, FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton MACE_POUND = new CombatTabButton(390, FightExp.MELEE_ATTACK, MeleeSwingStyle.CRUSH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton MACE_PUMMEL = new CombatTabButton(390, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton MACE_SPIKE = new CombatTabButton(390, FightExp.MELEE_SHARED, MeleeSwingStyle.STAB, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton MACE_BLOCK = new CombatTabButton(390, FightExp.MELEE_DEFENSE, MeleeSwingStyle.CRUSH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton SPEAR_LUNGE = new CombatTabButton(PlayerConstants.EMOTE_STAB_SPEAR, FightExp.MELEE_SHARED, MeleeSwingStyle.STAB, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton SPEAR_SWIPE = new CombatTabButton(PlayerConstants.EMOTE_SLASH_SPEAR, FightExp.MELEE_SHARED, MeleeSwingStyle.SLASH, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton SPEAR_POUND = new CombatTabButton(PlayerConstants.EMOTE_CRUSH_SPEAR, FightExp.MELEE_SHARED, MeleeSwingStyle.CRUSH, WeaponAttackTypes.CONTROLLED);
	public static final CombatTabButton SPEAR_BLOCK = new CombatTabButton(PlayerConstants.EMOTE_STAB_SPEAR, FightExp.MELEE_DEFENSE, MeleeSwingStyle.STAB, WeaponAttackTypes.DEFENSIVE);

	public static final CombatTabButton BATTLEAXE_CHOP = new CombatTabButton(395, FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton BATTLEAXE_HACK = new CombatTabButton(395, FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton BATTLEAXE_SMASH = new CombatTabButton(395, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton BATTLEAXE_BLOCK = new CombatTabButton(395, FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton PICKAXE_SPIKE = new CombatTabButton(390, FightExp.MELEE_ATTACK, MeleeSwingStyle.STAB, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton PICKAXE_IMPALE = new CombatTabButton(390, FightExp.MELEE_STRENGTH, MeleeSwingStyle.STAB, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton PICKAXE_SMASH = new CombatTabButton(390, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton PICKAXE_BLOCK = new CombatTabButton(390, FightExp.MELEE_DEFENSE, MeleeSwingStyle.STAB, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton SCYTHE_REAP = new CombatTabButton(PlayerConstants.EMOTE_CRUSH_SPEAR, FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton SCYTHE_CHOP = new CombatTabButton(PlayerConstants.EMOTE_CRUSH_SPEAR, FightExp.MELEE_STRENGTH, MeleeSwingStyle.STAB, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton SCYTHE_JAB = new CombatTabButton(PlayerConstants.EMOTE_CRUSH_SPEAR, FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton SCYTHE_BLOCK = new CombatTabButton(PlayerConstants.EMOTE_CRUSH_SPEAR, FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton SCYTHE_OF_VITUR_REAP = new CombatTabButton(Animation.getOsrsAnimId(8056), FightExp.MELEE_ATTACK, MeleeSwingStyle.SLASH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton SCYTHE_OF_VITUR_CHOP = new CombatTabButton(Animation.getOsrsAnimId(8056), FightExp.MELEE_STRENGTH, MeleeSwingStyle.SLASH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton SCYTHE_OF_VITUR_JAB = new CombatTabButton(Animation.getOsrsAnimId(8056), FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton SCYTHE_OF_VITUR_BLOCK = new CombatTabButton(Animation.getOsrsAnimId(8056), FightExp.MELEE_DEFENSE, MeleeSwingStyle.SLASH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton BULWARK_POUND = new CombatTabButton(Animation.getOsrsAnimId(7511), FightExp.MELEE_ATTACK, MeleeSwingStyle.CRUSH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton BULWARK_PUMMEL = new CombatTabButton(Animation.getOsrsAnimId(7511), FightExp.MELEE_STRENGTH, MeleeSwingStyle.CRUSH, WeaponAttackTypes.AGGRESSIVE);
	public static final CombatTabButton BULWARK_BLOCK = new CombatTabButton(Animation.getOsrsAnimId(7511), FightExp.MELEE_DEFENSE, MeleeSwingStyle.CRUSH, WeaponAttackTypes.DEFENSIVE);
	
	public static final CombatTabButton BOXING_PUNCH = new CombatTabButton(3678, FightExp.MELEE_ATTACK, MeleeSwingStyle.CRUSH, WeaponAttackTypes.ACCURATE);
	public static final CombatTabButton BOXING_BLOCK = new CombatTabButton(3678, FightExp.MELEE_DEFENSE, MeleeSwingStyle.CRUSH, WeaponAttackTypes.DEFENSIVE);
	
	private Animation attackEmote;
	private FightExp fightExp;
	private MeleeSwingStyle swingStyle;
	private WeaponAttackTypes attackType;
	
	
	public CombatTabButton(int emote, FightExp exp, MeleeSwingStyle swingStyle, WeaponAttackTypes attackType) {
		this.attackEmote = new Animation(emote);
		this.fightExp = exp;
		this.swingStyle = swingStyle;
		this.attackType = attackType;
	}

	public Animation getAttackEmote() {
		return attackEmote;
	}

	public CombatTabButton createNewAttackEmoteButton(int attackEmote) {
		return new CombatTabButton(attackEmote, getFightExp(), getSwingStyle(), getAttackType());
	}

	public FightExp getFightExp() {
		return fightExp;
	}

	public MeleeSwingStyle getSwingStyle() {
		return swingStyle;
	}

	public WeaponAttackTypes getAttackType() {
		return attackType;
	}
	
}
