package com.soulplay.content.items.itemdata;

import java.util.HashMap;

import com.soulplay.game.model.player.Player;

public class CombatTab {
	
	public static CombatTab UNARMED = new CombatTab(5855, 5857, CombatTabButton.UNARMED_PUNCH, CombatTabButton.UNARMED_KICK, CombatTabButton.UNARMED_BLOCK);
	
	private int tabId;
	private int interfaceStringId;
	private CombatTabButton[] buttons;
	
	public CombatTab(int tabId, int interfaceStringId, CombatTabButton... buttons) {
		this.tabId = tabId;
		this.interfaceStringId = interfaceStringId;
		this.buttons = buttons;
	}
	
	public CombatTab copy() {
		return new CombatTab(getTabId(), getInterfaceStringId(), getButtons());
	}

	public CombatTabButton[] getButtons() {
		return buttons;
	}

	public void setButtons(CombatTabButton[] buttons) {
		this.buttons = buttons;
	}

	public int getTabId() {
		return tabId;
	}

	public int getInterfaceStringId() {
		return interfaceStringId;
	}
	
	public static final HashMap<Integer, CombatTab> combatTabButtons = new HashMap<Integer, CombatTab>();
	
	public static CombatTab getByWeapon(int weaponId) {
		return combatTabButtons.get(weaponId);
	}
	
	public static CombatTab getByWeaponOrUnarmed(int weaponId) {
		CombatTab value = combatTabButtons.get(weaponId);
		if (value != null)
			return value;

		return CombatTab.UNARMED;
	}
	
	public static void setButton(Player player, int button, CombatTab tab) {
		if (button >= tab.getButtons().length)
			button = tab.getButtons().length-1;
		player.fightMode = button;
		player.setCombatTabButtonToggle(tab.getButtons()[button]);
		CombatTabButton buttonToggle = player.getCombatTabButtonToggle();
		player.setFightXp(buttonToggle.getFightExp());
		player.setMeleeSwingStyle(buttonToggle.getSwingStyle());
		player.setWeaponAttackType(buttonToggle.getAttackType());
	}
	
}
