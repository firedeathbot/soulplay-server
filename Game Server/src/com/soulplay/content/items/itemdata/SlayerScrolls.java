package com.soulplay.content.items.itemdata;

public enum SlayerScrolls {
	UNHOLY_HELMET(0, 30081),
	KING_BLACK_BONNET(1, 30082),
	KALPHITE_KHAT(2, 30083);
	
	public static final SlayerScrolls[] values = values();

	public final int scrollType;

	public final int itemID;

	private SlayerScrolls(int scrollType, int itemID) {
		this.scrollType = scrollType;
		this.itemID = itemID;
	}

}
