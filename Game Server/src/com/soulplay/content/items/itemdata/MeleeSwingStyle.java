package com.soulplay.content.items.itemdata;

import com.soulplay.content.combat.CombatStats;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Mob;

public enum MeleeSwingStyle {
	STAB,
	SLASH,
	CRUSH,
	OTHER;

	public int getBonusOff(Mob mob) {
		if (this == OTHER) {
			return 0;
		}

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			return player.getPlayerBonus()[ordinal()];
		} else {
			NPC npc = mob.toNpcFast();
			CombatStats combatStats = npc.getCombatStats();
			if (combatStats == null) {
				return 0;
			}

			int attackBonus = combatStats.getAttackStr();
			int styleBonus;
			switch (this) {
				case STAB:
					styleBonus = combatStats.getStabBonus();
				case SLASH:
					styleBonus = combatStats.getSlashBonus();
				case CRUSH:
					styleBonus = combatStats.getCrushBonus();
				default:
					styleBonus = 0;
					break;
			}

			return attackBonus + styleBonus;
		}
	}

	public int getBonusDef(Mob mob) {
		if (this == OTHER) {
			return 0;
		}

		if (mob.isPlayer()) {
			Player player = mob.toPlayerFast();
			return player.getPlayerBonus()[5 + ordinal()];
		} else {
			NPC npc = mob.toNpcFast();
			CombatStats combatStats = npc.getCombatStats();
			if (combatStats == null) {
				return 0;
			}

			switch (this) {
				case STAB:
					return combatStats.getStabDefBonus();
				case SLASH:
					return combatStats.getSlashDefBonus();
				case CRUSH:
					return combatStats.getCrushDefBonus();
				default:
					return 0;
			}
		}
	}

}
