package com.soulplay.content.items.itemdata;

public enum WeaponAttackTypes {
	ACCURATE,
	AGGRESSIVE,
	CONTROLLED,
	DEFENSIVE,
	RAPID,
	LONG_RANGED;
}
