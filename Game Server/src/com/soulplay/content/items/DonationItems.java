package com.soulplay.content.items;

import com.soulplay.Config;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.util.sql.payment.ClaimPayments;

public class DonationItems {

	private static final Graphic CLAIM_GRAPHIC = Graphic.create(2015);

	public static enum DonationConsumable {
		SCROLL_10("Scroll", 10, 786, 2),
		SCROLL_50("Scroll", 50, 1505, 2),
		SCROLL_100("Scroll", 100, 2396, 20),
		CHEST_500("Chest", 500, 30209, 100);

		private final int amount, itemId, extraYells;
		private final String name;

		private DonationConsumable(String name, int amount, int itemId, int extraYells) {
			this.name = name;
			this.amount = amount;
			this.itemId = itemId;
			this.extraYells = extraYells;
		}

		public String getName() {
			return name;
		}

		public int getAmount() {
			return amount;
		}

		public int getItemId() {
			return itemId;
		}

		public int getExtraYells() {
			return extraYells;
		}

	}

	public static void clickItem(Player player, DonationConsumable consumable) {
		player.getDialogueBuilder().sendOption("Would like to claim this " + consumable.getName() + "?",
				"Yes, I want to claim it.", () -> {
					claimItem(player, consumable);
				}, "No, I'd like to keep it.", () -> {

				}).execute();
	}

	private static void claimItem(Player player, DonationConsumable consumable) {
		if (System.currentTimeMillis() - player.lastWhatDropCommand < 5000) {
			player.sendMessage("You must wait 5 seconds before claiming this " + consumable.getName() + ".");
			return;
		}

		player.lastWhatDropCommand = System.currentTimeMillis();
		if (!player.getItems().deleteItemInOneSlot(consumable.getItemId(), 1)) {
			player.sendMessage("You don't have a " + consumable.getName() + " to claim.");
			return;
		}

		player.setDonatedTotalAmount(player.getDonatedTotalAmount() + consumable.getAmount(), true);
		player.setDonPoints(player.getDonPoints() + consumable.getAmount(), true);
		if (Config.increasedFPPoints > 0) {
			int extra = (int) Math.round(consumable.getAmount() * Config.increasedFPPoints);
			player.setDonPoints(player.getDonPoints() + extra, true);
			player.yellPoints += consumable.getExtraYells();
			player.sendMessage("You have received " + consumable.getExtraYells() + " yells. You can yell "
					+ player.yellPoints + " times.");
			player.sendMessage("<col=800000>You receive " + extra + " extra points.");
		}

		player.sendMessage("<col=ff0000>You have received " + consumable.getAmount() + " donation points!");
		player.startGraphic(CLAIM_GRAPHIC);
		PlayerHandler.scrollsUsed++;
		ClaimPayments.setPlayerRights(player);
	}

}
