package com.soulplay.content.items.presets;

import java.util.List;

import com.soulplay.content.player.DonatorRank;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemAssistant;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.UpdateFlag;

public class PresetsInterface {

	public static void open(Player player) {
		player.getPacketSender().showInterface(70600);
		player.getPacketSender().sendPresetsList();
		player.getPacketSender().sendSharedPresetsList();
		updateSelectedPreset(player);
	}

	public static boolean buttonClick(Player player, int buttonId) {
		switch (buttonId) {
			case 70607: {//Load
				if (!canAlterPreset(player)) {
					player.sendMessage("You can't load the preset at this moment.");
					return true;
				}

				Preset preset = player.selectedPreset;
				if (preset == null) {
					return true;
				}

				player.refreshBank = false;

				//bank all inventory
				for (int i = 0, length = player.playerItems.length; i < length; i++) {
					int itemId = player.playerItems[i] - 1;
					if (itemId >= 0) {
						int amount = player.playerItemsN[i];
						if (player.getItems().addItemToBank(itemId, amount)) {
							player.getItems().deleteItemInOneSlot(itemId, i, amount);
						} else {
							player.sendMessage("Bank is full! Failed to load the preset.");
							return true;
						}
					}
				}

				//bank equipment
				player.getItems().bankEquipment();
				int weapon = player.playerEquipment[PlayerConstants.playerWeapon];
				player.getItems().sendWeapon(weapon, ItemConstants.getItemName(weapon));
				player.setUpdateEquipment(true, 3);
				player.getCombat().getPlayerAnimIndex(weapon);

				//load preset
				List<Item> inventoryItems = preset.getInventory();
				for (int i = 0, length = inventoryItems.size(); i < length; i++) {
					Item item = inventoryItems.get(i);
					int itemId = item.getId() - 1;
					if (itemId == -1) {
						continue;
					}

					int itemAmount = item.getAmount();
					if (player.getItems().bankHasItemAmount(itemId, itemAmount)) {
						player.getItems().removeBankItem(itemId, itemAmount);
						player.playerItems[i] = item.getId();
						player.playerItemsN[i] = itemAmount;
					} else {
						player.sendMessageSpam("<col=f00000>Failed to obtain " + ItemDefinition.getName(itemId) + " x " + itemAmount + " from the bank.");
					}
				}

				List<Item> equpmentItems = preset.getEquipment();
				for (int i = 0, length = equpmentItems.size(); i < length; i++) {
					Item item = equpmentItems.get(i);
					int itemId = item.getId();
					if (itemId == -1) {
						continue;
					}

					int itemAmount = item.getAmount();
					if (!ItemAssistant.canWearItem(player, ItemDefinition.forId(itemId), false)) {
						player.sendMessageSpam("<col=f00000>Failed to equip " + ItemDefinition.getName(itemId) + " x " + itemAmount + " from the bank.");
						continue;
					}

					if (player.getItems().bankHasItemAmount(itemId, itemAmount)) {
						player.getItems().removeBankItem(itemId, itemAmount);
						player.playerEquipment[i] = item.getId();
						player.playerEquipmentN[i] = itemAmount;
					} else {
						player.sendMessageSpam("<col=f00000>Failed to obtain " + ItemDefinition.getName(itemId) + " x " + itemAmount + " from the bank.");
					}
				}

				player.sendMessage("<col=4C4C15>Unfilter game messages to check for errors while loading the preset.");
				player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
				for (int i = 0, len = player.playerEquipment.length; i < len; i++)
					player.setUpdateEquipment(true, i);
				player.getItems().refreshWeapon();
				player.setUpdateInvInterface(3214);
				player.setSpellbook(preset.getSpellbook());
				player.getPA().resetAutocast();
				player.refreshBank = true;
				return true;
			}
			case 70609: {//Save
				if (!canAlterPreset(player)) {
					player.sendMessage("You can't save the preset at this moment.");
					return true;
				}

				Preset preset = player.selectedPreset;
				if (preset == null) {
					return true;
				}

				player.getDialogueBuilder().sendOption("Are you sure you want to override this preset?", "Yes, override '" + preset.getName() + "'.", () -> {
					if (!canAlterPreset(player)) {
						player.sendMessage("You can't save the preset at this moment.");
						return;
					}

					Preset finalPreset = player.selectedPreset;
					if (finalPreset == null) {
						return;
					}

					int index = finalPreset.getIndex();
					Preset newPreset = createPreset(player, index, finalPreset.getName());
					player.presets.set(index, newPreset);
					player.selectedPreset = newPreset;
					updateSelectedPreset(player);
				}, "No.", () -> {}).sendAction(() -> player.getPacketSender().sendChatInterface(-1, false)).execute(false);
				return true;
			}
			case 70656: {// Delete shared
				Preset preset = player.selectedSharedPreset;
				if (preset == null) {
					return true;
				}

				player.sharedPresets.remove(preset.getIndex());
				player.getPacketSender().sendSharedPresetsList();
				player.selectedSharedPreset = null;
				return true;
			}
			case 70655: {//Save shared
				int currentSize = player.presets.size();
				if (!canAddPreset(player, currentSize)) {
					return true;
				}

				Preset preset = player.selectedSharedPreset;
				if (preset == null) {
					return true;
				}

				player.sharedPresets.remove(preset.getIndex());
				Preset newPreset = preset.copy(currentSize);
				player.presets.add(newPreset);
				selectPreset(player, newPreset);
				return true;
			}
			default:
				return false;
		}
	}

	private static boolean canAlterPreset(Player player) {
		if (player.isInCombatDelay()) {
			return false;
		}
		
		if (player.underAttackBy > 0 || player.underAttackBy2 > 0) {
			return false;
		}

		if (player.insideInstance()) {
			return false;
		}

		if (!player.inEdgeville()) {
			return false;
		}

		if (player.inMinigame()) {
			return false;
		}

		if (player.wildLevel > 0) {
			return false;
		}

		return true;
	}

	public static boolean handlePresetsList(Player player, int buttonId, int componentId, int option) {
		switch (buttonId) {
			case 70651: {
				if (componentId < 0 || option < 0) {
					return true;
				}

				List<Preset> presets = player.presets;
				int buttonInList = componentId / 2;
				switch (option) {
					case 0: {
						int currentSize = presets.size();
						if (buttonInList == currentSize) {
							if (!canAddPreset(player, currentSize)) {
								return true;
							}

							player.getDialogueBuilder().sendEnterText(() -> {
								 if (!canAlterPreset(player)) {
									 player.sendMessage("You add a new preset at this moment.");
									 return;
								 }

								 final String text = player.getDialogueBuilder().getEnterText().trim();
								 if (text.isEmpty() || text.length() > 10) {
									 player.getDialogueBuilder().sendStatement("Invalid preset name entered.").sendAction(() -> player.getPacketSender().sendChatInterface(-1, false));
									 return;
								 }

								 Preset preset = createPreset(player, currentSize, text);
								 player.presets.add(preset);
								 selectPreset(player, preset);
							}).execute(false);
						} else {
							if (buttonInList >= presets.size()) {
								return true;
							}

							selectPreset(player, presets.get(buttonInList));
						}
						return true;
					}
					case 1: {
						if (buttonInList >= presets.size()) {
							return true;
						}

						Preset preset = presets.get(buttonInList);
						player.getDialogueBuilder().sendStatement("Are you sure you want to <col=f00000>delete</col> '" + preset.getName() + "' preset?").sendOption("Yes, <col=f00000>delete</col> '" + preset.getName() + "'.", () -> {
							if (buttonInList >= presets.size()) {
								return;
							}

							presets.remove(buttonInList);
							player.selectedPreset = null;
							player.getPacketSender().sendPresetsList();
							updatePreset(player, null);
							player.getPacketSender().sendString("", 70612);
							player.getDialogueBuilder().sendStatement("You've <col=f00000>deleted</col> the '" + preset.getName() + "' preset.").sendAction(() -> player.getPacketSender().sendChatInterface(-1, false));
						}, "No.", () -> player.getPacketSender().sendChatInterface(-1, false)).execute(false);
						return true;
					}
					case 2: {
						if (buttonInList >= presets.size()) {
							return true;
						}

						player.getDialogueBuilder().sendEnterText(() -> {
							Client otherPlayer = (Client) PlayerHandler.getPlayer(player.getDialogueBuilder().getEnterText());
							if (otherPlayer == null || !otherPlayer.isActive) {
								player.sendMessage("This player is not online.");
								return;
							}

							if (otherPlayer.isIgnored(player)) {
								player.sendMessage("This player has privacy mode on.");
								return;
							}

							if (otherPlayer.sharedPresets.size() >= 10) {
								player.sendMessage("This player shared preset list is full.");
								return;
							}

							Preset preset = presets.get(buttonInList);
							otherPlayer.sharedPresets.add(preset.copy(otherPlayer.sharedPresets.size()));
							otherPlayer.getPacketSender().sendSharedPresetsList();
							otherPlayer.sendMessage(player.getDisplayName() + " has shared a preset with you!");
							player.sendMessage("You've shared a preset with " + player.getDisplayName() + ".");
						}).execute(false);
						return true;
					}
				}
				return true;
			}
			case 70657: {
				if (componentId < 0 || option < 0) {
					return true;
				}

				List<Preset> presets = player.sharedPresets;
				int buttonInList = componentId / 2;
				if (buttonInList >= presets.size()) {
					return true;
				}

				player.selectedPreset = null;
				player.selectedSharedPreset = presets.get(buttonInList);
				player.getPacketSender().sendPresetsList();
				player.getPacketSender().sendSharedPresetsList();
				player.getPacketSender().sendString(player.selectedSharedPreset.getName(), 70612);
				updatePreset(player, player.selectedSharedPreset);
				return true;
			}
			default:
				return false;
		}
	}

	private static boolean canAddPreset(Player player, int currentSize) {
		DonatorRank donatorRank = DonatorRank.check(player);
		int maxPresets = donatorRank.getMaxPresets();
		if (currentSize >= maxPresets) {
			String[] lines;
			if (donatorRank == DonatorRank.INSANE) {
				lines = new String[] { "You've reached the presets limit, why do you need so many?" };
			} else {
				int extras = DonatorRank.nextRank(donatorRank).getMaxPresets() - maxPresets;
				lines = new String[] { "You've reached the presets limit.", "Upgrade to next donator rank to unlock <col=f00000>" + extras + "</col> extra " + (extras == 1 ? "preset" : "presets") + "."};
			}

			player.getDialogueBuilder().sendStatement(lines).sendAction(() -> player.getPacketSender().sendChatInterface(-1, false)).execute(false);
			return false;
		}

		return true;
	}

	public static void selectPreset(Player player, Preset preset) {
		player.selectedPreset = preset;
		player.selectedSharedPreset = null;
		if (preset == null) {
			player.getPacketSender().sendString("", 70612);
			return;
		}

		player.getPacketSender().sendString(preset.getName(), 70612);
		player.getPacketSender().sendPresetsList();
		player.getPacketSender().sendSharedPresetsList();
		updateSelectedPreset(player);
	}

	private static Preset createPreset(Player player, int index, String name) {
		return new Preset(index, name, player.playerItems, player.playerItemsN, player.playerEquipment, player.playerEquipmentN, player.playerMagicBook);
	}

	public static void updateSelectedPreset(Player player) {
		updatePreset(player, player.selectedPreset);
	}

	public static void updatePreset(Player player, Preset preset) {
		List<Item> inventory = null;
		List<Item> equipment = null;
		if (preset != null) {
			inventory = preset.getInventory();
			equipment = preset.getEquipment();
		}

		player.getPacketSender().sendPresetInventory(inventory);
		player.getPacketSender().sendPresetEquipment(equipment);
	}

}
