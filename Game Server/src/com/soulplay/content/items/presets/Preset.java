package com.soulplay.content.items.presets;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.soulplay.content.player.SpellBook;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.save.GsonSave;

public class Preset {

	private final int index;
	private final String name;
	private final List<Item> inventory;
	private final List<Item> equipment;
	private final SpellBook spellbook;

	public Preset(int index, String name, int[] inventory, int[] inventoryAmount, int[] equipment,
			int[] equipmentAmount, SpellBook spellbook) {
		this.index = index;
		this.name = name;
		this.inventory = new ArrayList<>();
		for (int i = 0, length = inventory.length; i < length; i++) {
			this.inventory.add(new Item(inventory[i], inventoryAmount[i]));
		}

		this.equipment = new ArrayList<>();
		for (int i = 0, length = equipment.length; i < length; i++) {
			this.equipment.add(new Item(equipment[i], equipmentAmount[i]));
		}

		this.spellbook = spellbook;
	}

	public Preset(int index, String name, List<Item> inventory, List<Item> equipment, SpellBook spellbook) {
		this.index = index;
		this.name = name;
		this.inventory = inventory;
		this.equipment = equipment;
		this.spellbook = spellbook;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

	public List<Item> getInventory() {
		return inventory;
	}

	public List<Item> getEquipment() {
		return equipment;
	}

	public SpellBook getSpellbook() {
		return spellbook;
	}

	public Preset copy(int index) {
		return new Preset(index, name, new ArrayList<>(inventory), new ArrayList<>(equipment), spellbook);
	}

	public String getInventoryJson() {
		return toJson(inventory);
	}

	public String getEquipmentJson() {
		return toJson(equipment);
	}

	public static String toJson(List<Item> items) {
		return GsonSave.gsond.toJson(items);
	}

	public static List<Item> loadItemsFromJson(String str) {
		List<Item> items = GsonSave.gsond.fromJson(str, new TypeToken<List<Item>>() { }.getType());
		if (items == null)
			items = new ArrayList<>();
		return items;
	}

}
