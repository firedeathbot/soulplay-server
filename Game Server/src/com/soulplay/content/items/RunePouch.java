package com.soulplay.content.items;

import com.soulplay.game.model.player.Player;

public class RunePouch {
	
	public static final int RUNE_POUCH_ITEM = 30047;
	private static final int RUNE_POUCH_INTERFACE = 43660;
	public static final int RUNE_POUCH_INTERFACE_RUNES = 43671;
	public static final int RUNE_POUCH_INTERFACE_INV = 43670;
	public static final int LIMIT = 16_000;
	
	public static boolean handleRunePouchOpen(Player p, int itemId) {
		if (itemId != RUNE_POUCH_ITEM) {
			return false;
		}
		
		p.getPacketSender().showInterface(RUNE_POUCH_INTERFACE);
		updateInterfaces(p);
		return true;
	}
	
	public static void updateRunes(Player p) {
		p.getPacketSender().itemsOnInterface(RUNE_POUCH_INTERFACE_RUNES, p.runePouchItems, p.runePouchItemsN);
	}
	
	public static void updateInterfaces(Player p) {
		p.getPacketSender().itemsOnInterface(RUNE_POUCH_INTERFACE_INV, p.playerItems, p.playerItemsN);
		updateRunes(p);
	}
	
	public static boolean handleRune(Player p, int interfaceId, int id, int slot, int count) {
		if (interfaceId != RUNE_POUCH_INTERFACE_INV && interfaceId != RUNE_POUCH_INTERFACE_RUNES) {
			return false;
		}

		if (interfaceId == RUNE_POUCH_INTERFACE_INV) {
			if (count == -1) {
				count = p.playerItemsN[slot];
			}

			depositRune(p, id, slot, count);
		} else if (interfaceId == RUNE_POUCH_INTERFACE_RUNES) {
			withdrawRune(p, id, slot, count);
		}

		return true;
	}
	
	private static boolean isRune(int itemId) {
		switch (itemId) {
			case 554:
			case 555:
			case 556:
			case 557:
			case 558:
			case 559:
			case 560:
			case 561:
			case 562:
			case 563:
			case 564:
			case 565:
			case 566:
			case 4694:
			case 4695:
			case 4696:
			case 4697:
			case 4698:
			case 4699:
			case 9075:
				return true;
		}

		return false;
	}
	
	public static void depositRune(Player p, int id, int slot, int count) {
		if (!isRune(id)) {
			p.sendMessage("Can only stores runes in your rune pouch.");
			return;
		}

		int addToSlot = getFreePouchIndex(p, id);
		if (addToSlot == -1) {
			p.sendMessage("Your rune pouch is full.");
			return;
		}

		int current = p.runePouchItemsN[addToSlot];
		int deduct = Math.min(p.getItems().getItemAmount(id), count);

		if (current + count > LIMIT) {
			deduct = LIMIT - current;
		}

		if (deduct <= 0) {
			p.sendMessage("Can't store more than 16000 runes at the time.");
			return;
		}

		if (p.getItems().deleteItem2(id, deduct)) {
			p.runePouchItems[addToSlot] = id + 1;
			p.runePouchItemsN[addToSlot] += deduct;
		}

		updateInterfaces(p);
	}
	
	private static int getFreePouchIndex(Player p, int itemId) {
		int free = -1;
		
		for (int i = 0; i < p.runePouchItems.length; i++) {
			int currentId = p.runePouchItems[i] - 1;
			if (currentId == -1 || currentId == itemId) {
				free = i;
				break;
			}
		}
		
		return free;
	}

	public static int getRuneIndex(Player p, int id, int amount) {
		id++;
		for (int i = 0; i < p.runePouchItems.length; i++) {
			if (p.runePouchItems[i] == id && p.runePouchItemsN[i] >= amount) {
				return i;
			}
		}

		return -1;
	}

	public static int getRuneIndex(Player p, int id) {
		id++;
		for (int i = 0; i < p.runePouchItems.length; i++) {
			if (p.runePouchItems[i] == id) {
				return i;
			}
		}

		return -1;
	}

	public static void deleteRune(Player p, int id, int count) {
		int am = count;
		for (int i = 0; i < p.runePouchItems.length; i++) {
			if (am == 0) {
				break;
			}
			if (p.runePouchItems[i] == (id + 1)) {
				if (p.runePouchItemsN[i] > count) {
					p.runePouchItemsN[i] -= count;
					break;
				} else {
					p.runePouchItems[i] = 0;
					p.runePouchItemsN[i] = 0;
					am--;
				}
			}
		}
		
		updateRunes(p);
	}
	
	private static void withdrawRune(Player p, int id, int slot, int count) {
		if (count == -1) {
			count = p.runePouchItemsN[slot];
		} else {
			count = Math.min(count, p.runePouchItemsN[slot]);
		}

		deleteRune(p, id, count);
		p.getItems().addItem(id, count);

		updateInterfaces(p);
	}
	
	public static boolean emptyRunePouch(Player p, int id) {
		if (id != RUNE_POUCH_ITEM) {
			return false;
		}
		
		int free = -1;
		switch(getFreePouchIndex(p, -1)) {
		case -1:
			free = 3;
			break;
		case 1:
			free = 1;
			break;
		case 2:
			free = 2;
			break;
		}
		
		if (free == -1) {
			return false;
		}
		
		if (p.getPA().freeSlots() < free) {
			p.sendMessage("You need at least "+ free +" more empty slots to empty Rune Pouch.");
			return false;
		}
		
		for(int i = 0; i < 3; i++) {
			withdrawRune(p, p.runePouchItems[i] - 1, i, -1);
		}

		return true;
	}
	
	public static void clearRunePouch(Player p) {
		for (int i = 0; i < 3; i++) {
			p.runePouchItems[i] = 0;
			p.runePouchItemsN[i] = 0;
		}
	}
	
	public static boolean isEmpty(Player p) {
		for (int i = 0; i < 3; i++) {
			if (p.runePouchItems[i] > 0)
				return false;
		}
		return true;
	}
	
	public static boolean removeRunePouchWildy(Player p) {
		if (!p.getItems().playerHasItem(RUNE_POUCH_ITEM)) {
			return false;
		}
		if (!p.inWild() || !(p.wildLevel > 0)) {
			return false;
		}
		
		for(int i = 0; i < 3; i++) {
			withdrawRune(p, p.runePouchItems[i] - 1, i, -1);
		}
		p.getItems().deleteItem2(30047, 1);
		p.sendMessage("Your rune pouch is ruined after dieing in wilderness.");
		
		return true;
	}

}
