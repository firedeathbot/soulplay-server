package com.soulplay.content.items.useitem;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;

public class ItemsOnObjects {
	
		public static enum Items {

			UNCUT_ZENYTE(6573, 30198, -1, 30196, new int[] {4765, 4766}, 1, 15),
			ZENYTE_RING(1592, 30195, 2357, 30201, new int[] {}, 89, 150),
			ZENYTE_NECKLACE(1597, 30195, 2357, 30200, new int[] {}, 92, 165),
			ZENYTE_BRACELET(11065, 30195, 2357, 30199, new int[] {}, 95, 185),
			ZENYTE_AMULET(1595, 30195, 2357, 30197, new int[] {}, 98, 200),
			//26814, 2781, 21303
			;
			
			private static final Items[] values = values();

			private final int itemId1;
			private final int itemId2;
			private final int itemId3;
			private final int rewardId;
			private final int[] objectId;
			private final int craftLvl;
			private final int craftExp;

			private Items(int itemId1, int itemId2, int itemId3, int rewardId, int[] objectId, int craftLvl, int craftExp) {
				this.itemId1 = itemId1;
				this.itemId2 = itemId2;
				this.itemId3 = itemId3;
				this.rewardId = rewardId;
				this.objectId = objectId;
				this.craftLvl = craftLvl;
				this.craftExp = craftExp;
			}

			public static Items[] getValues() {
				return values;
			}

			public int getItemId1() {
				return itemId1;
			}

			public int getItemId2() {
				return itemId2;
			}

			public int getItemId3() {
				return itemId3;
			}

			public int getRewardId() {
				return rewardId;
			}

			public int[] getObjectId() {
				return objectId;
			}

			public int getCraftLvl() {
				return craftLvl;
			}

			public int getCraftExp() {
				return craftExp;
			}

			
		}
		
		public static boolean handle(Client c, int used, int on) {
			for (Items item : Items.values) { 
				for (int i = 0, length = item.objectId.length; i < length; i++) {
					if (on == item.objectId[i] && (used == item.itemId1 || used == item.itemId2 || used == item.itemId3)) {
						if (c.getPlayerLevel()[Skills.CRAFTING] < item.craftLvl) {
							c.sendMessage("You need " + item.craftLvl + " crafting level to craft this item.");
							return true;
						}
						if ((item.itemId1 == -1 || c.getItems().playerHasItem(item.itemId1, 1)) &&
								(item.itemId2 == -1 || c.getItems().playerHasItem(item.itemId2, 1)) &&
								(item.itemId3 == -1 || c.getItems().playerHasItem(item.itemId3, 1))) {
			
							if (item.itemId1 != -1) {
								c.getItems().deleteItemInOneSlot(item.itemId1, 1);
							}
							if (item.itemId2 != -1) {
								c.getItems().deleteItemInOneSlot(item.itemId2, 1);
							}
							if (item.itemId3 != -1) {
								c.getItems().deleteItemInOneSlot(item.itemId3, 1);
							}
								c.getItems().addItem(item.rewardId, 1);
								c.getPA().addSkillXP(item.craftExp, Skills.CRAFTING);
							
						} else {
							if (item.itemId2 == -1 && item.itemId3 == -1) {
							    c.sendMessage("You need " + ItemDef.forID(item.itemId1).getName() + " to craft this item.");
							} else if (item.itemId3 == -1) {
								c.sendMessage("You need " + ItemDef.forID(item.itemId1).getName() + " and " + ItemDef.forID(item.itemId2).getName() + " to craft this item.");
							} else {
								c.sendMessage("You need " + ItemDef.forID(item.itemId1).getName() + ", " + ItemDef.forID(item.itemId2).getName() + " and " + ItemDef.forID(item.itemId3).getName() + " to craft this item.");
									
							}
						}
				  		return true;
				    }
				}
		    }

		    return false;
	    }

}
