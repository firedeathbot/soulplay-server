package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;

public class AmuletStringing {
	
	private enum Items {

		GOLD_AMULET(1673, 1759, 1692),
		SAPPHIRE_AMULET(1675, 1759, 1694),
		EMERALD_AMULET(1677, 1759, 1696),
		RUBY_AMULET(1679, 1759, 1698),
		DIAMOND_AMULET(1681, 1759, 1700),
		DRAGONSTONE_AMMY(1683, 1759, 1712),
		ONYX_AMULET(6579, 1759, 6581),
		ZENYTE_AMULET(30197, 1759, 30202),
		;
		
		private static final Items[] values = values();

		private final int useItemID;
		private final int onItemID;
		private final int itemID;

		private Items(int useItemID, int onItemID, int itemID) {
			this.useItemID = useItemID;
			this.onItemID = onItemID;
			this.itemID = itemID;
		}
	}
	
	public static boolean handle(Client c, int used, int on) {
			for (Items item : Items.values) { 
				if (used == item.useItemID || on == item.useItemID) {
					boolean onItem = on == item.useItemID;
				    if ((onItem && used == item.onItemID) || (!onItem && on == item.onItemID)) {
					    if (c.getItems().deleteItemInOneSlot(used, 1) && c.getItems().deleteItemInOneSlot(on, 1)) {
						    c.getItems().addItem(item.itemID, 1);
					    }
					    return true;
				    }
				}
		}
		return false;
	}

}
