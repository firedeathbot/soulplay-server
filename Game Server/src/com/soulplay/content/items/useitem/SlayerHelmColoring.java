package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;

public final class SlayerHelmColoring {

	private enum BossHeads {

		KBD_HEAD(7980, 30072),
		KALPHITE_HEAD(7981, 30074),
		ABYSSAL_HEAD(7979, 30076),
		VORKATH_HEAD(30329, 30330),
		HYDRA_HEAD(123077, 123073),
		;

		public static final BossHeads[] values = values();

		private final int headID;

		private final int helmID;

		private BossHeads(int headID, int helmID) {
			this.headID = headID;
			this.helmID = helmID;
		}
	}

	private static final int SLAYER_HELM = 13263;

	public static boolean handle(Client c, int used, int on) {
		if (used == SLAYER_HELM || on == SLAYER_HELM) {
			boolean onHelm = on == SLAYER_HELM;
			for (BossHeads head : BossHeads.values) {
				if ((onHelm && used == head.headID)
						|| (!onHelm && on == head.headID)) {
					if (head.helmID == 30076
							&& c.slayerScrollUnlocked(0) == false) {
						c.sendMessage(
								"You must unlock the ability to craft a red slayer helmet first.");
						return true;
					}
					if (head.helmID == 30072
							&& c.slayerScrollUnlocked(1) == false) {
						c.sendMessage(
								"You must unlock the ability to craft a black slayer helmet first.");
						return true;
					}
					if (head.helmID == 30074
							&& c.slayerScrollUnlocked(2) == false) {
						c.sendMessage(
								"You must unlock the ability to craft a green slayer helmet first.");
						return true;
					}
					if (c.getItems().deleteItemInOneSlot(used, 1)
							&& c.getItems().deleteItemInOneSlot(on, 1)) {
						c.getItems().addItem(head.helmID, 1);
					}
					return true;
				}
			}
		}

		return false;
	}

}
