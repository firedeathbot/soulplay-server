package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Player;

public final class WeaponUpgrades {

	public enum Weapons {

		GRANITE_MAUL(4153, 30067, 30066),
		MAGIC_SHORTBOW(861, 30055, 30056),
		DRAGON_PICKAXE(15259, 30049, 30054),
		TOXIC_STAFF_OF_THE_DEAD(11791, 30004, 30098),
		SKELETAL_STAFF_OF_AIR(21488, 1381, 21490),
		SKELETAL_STAFF_OF_EARTH(21488, 1385, 21492),
		SKELETAL_STAFF_OF_FIRE(21488, 1387, 21493),
		SKELETAL_STAFF_OF_WATER(21488, 1383, 21491),
		SKELETAL_BATTLESTAFF_OF_AIR(21488, 1397, 21496),
		SKELETAL_BATTLESTAFF_OF_EARTH(21488, 1399, 21497),
		SKELETAL_BATTLESTAFF_OF_FIRE(21488, 1393, 21494),
		SKELETAL_BATTLESTAFF_OF_WATER(21488, 1395, 21495),
		SKELETAL_LAVA_BATTLESTAFF(21488, 3053, 21502),
		SKELETAL_MUD_BATTLESTAFF(21488, 6562, 21504),
		SKELETAL_STEAM_BATTLESTAFF(21488, 11736, 21506),
		NECROMANCERS_AIR_STAFF(21488, 1405, 21500),
		NECROMANCERS_EARTH_STAFF(21488, 1407, 21501),
		NECROMANCERS_FIRE_STAFF(21488, 1401, 21498),
		NECROMANCERS_WATER_STAFF(21488, 1403, 21499),
		NECROMANCERS_LAVA_STAFF(21488, 3054, 21503),
		NECROMANCERS_MUD_STAFF(21488, 6563, 21505),
		NECROMANCERS_STEAM_STAFF(21488, 11738, 21507),
		DRAGONFIRE_WARD(30335, 1540, 30336),
		TOXIC_TRIDENT(30016, 30004, 30349)
		

		;
		
		private static final Weapons[] values = values();

		private final int useItemID;
		public int getUseItemID() {
			return useItemID;
		}

		public int getOnItemID() {
			return onItemID;
		}

		private final int onItemID;
		private final int weaponID;

		private Weapons(int useItemID, int onItemID, int weaponID) {
			this.useItemID = useItemID;
			this.onItemID = onItemID;
			this.weaponID = weaponID;
		}
	}
	
	public static boolean handle(Player c, int used, int on) {
			for (Weapons weapon : Weapons.values) {
				if (used == weapon.useItemID || on == weapon.useItemID) {
					boolean onWeapon = on == weapon.useItemID;
				    if ((onWeapon && used == weapon.onItemID) || (!onWeapon && on == weapon.onItemID)) {
					    if (c.getItems().deleteItemInOneSlot(used, 1) && c.getItems().deleteItemInOneSlot(on, 1)) {
						    c.getItems().addItem(weapon.weaponID, 1);
					    }
					    return true;
				    }
				}
		}
		return false;
	}

}
