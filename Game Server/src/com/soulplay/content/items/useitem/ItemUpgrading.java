package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;

public final class ItemUpgrading {

	private enum Items {

		RED_LOG(7329, 1511, 7404),
		GREEN_LOG(7330, 1511, 7405),
		BLUE_LOG(7331, 1511, 7406),
		PURPLE_LOG(10326, 1511, 10329),
		WHITE_LOG(10327, 1511, 10328),
		MAGMA_HELM(30313, 30002, 30314),
		TANZANITE_HELM(30312, 30002, 30316),
		NEITIZNOT_FACEGUARD(124268, 10828, 124271),
		AVERNIC_DEFENDER(122477, 20072, 122322),
		NECKLACE_OF_ANGUISH_OR(30206, 122246, 122249),
		;
		
		private static final Items[] values = values();

		private final int useItemID;
		private final int onItemID;
		private final int itemID;

		private Items(int useItemID, int onItemID, int itemID) {
			this.useItemID = useItemID;
			this.onItemID = onItemID;
			this.itemID = itemID;
		}
	}
	
	public static boolean handle(Client c, int used, int on) {
			for (Items item : Items.values) { 
				if (used == item.useItemID || on == item.useItemID) {
					boolean onItem = on == item.useItemID;
				    if ((onItem && used == item.onItemID) || (!onItem && on == item.onItemID)) {
					    if (c.getItems().deleteItemInOneSlot(used, 1) && c.getItems().deleteItemInOneSlot(on, 1)) {
						    c.getItems().addItem(item.itemID, 1);
					    }
					    return true;
				    }
				}
		}
		return false;
	}

}