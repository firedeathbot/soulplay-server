package com.soulplay.content.items.useitem;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;

public class BucketWaterFilling {
	
	public static final int EMPTY_BUCKET = 1925;
	public static final int FILLED_BUCKET = 1929;
	public static final int EMPTY_JUG = 1935;
	public static final int FILLED_JUG = 1937;


	public static void waterFilling(Player player) {
		if (!(player.getItems().playerHasItem(EMPTY_JUG) || player.getItems().playerHasItem(EMPTY_BUCKET))) {
			player.sendMessage("You need a empty jug or bucket to fill with water.");
			return;
		}

		if (player.skillingEvent != null) {
			return;
		}

		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				player.startAnimation(832);
				if (player.getItems().playerHasItem(EMPTY_BUCKET)) {
					player.getItems().deleteItemInOneSlot(EMPTY_BUCKET, 1);
					player.getItems().addItem(FILLED_BUCKET, 1);
					player.sendMessage("You fill your bucket of water.");
				} else {
					player.getItems().deleteItemInOneSlot(EMPTY_JUG, 1);
					player.getItems().addItem(FILLED_JUG, 1);
					player.sendMessage("You fill your jug of water.");
				}

				if (!(player.getItems().playerHasItem(EMPTY_JUG) || player.getItems().playerHasItem(EMPTY_BUCKET))) {
					player.resetAnimations();
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
			}
		}, 2);
	}
}
