package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class ItemColouring {
	
	private enum ColorItem {
		DARK_BOW(11235, 15701, 15702, 15704, 15703),
		ABYSSAL_WHIP(4151, 15441, 15442, 15444, 15443),
		STAFF_OF_LIGHT(22207, 15486, 22210, 22212, 22208),
		VINE_WHIP(21371, 21372, 21373, 21375, 21374);
		
		public static final ColorItem[] values = values();
		
		private final int red, yellow, blue, green, white;
		private final int[] items = new int[5];

		private ColorItem(int red, int yellow, int blue, int green, int white) {
			this.red = items[0] = red;
			this.yellow = items[1] = yellow;
			this.blue = items[2] = blue;
			this.green = items[3] = green;
			this.white = items[4] = white;
		}
		
		private boolean matches(int matchItem) {
			for (int item : items)
				if (matchItem == item)
					return true;
			return false;
		}
		
		private static ColorItem matches(int used, int on) {
			for (ColorItem value : values)
				if (value.matches(used) || value.matches(on))
					return value;
			return null;
		}
	}
	
	private enum Color {
		RED(1763),
		YELLOW(1765),
		BLUE(1767),
		GREEN(1771),
		WHITE(3188);
		
		public static final Color[] values = values();
		
		private final int dye;
		
		Color(int dye) {
			this.dye = dye;
		}
		
		private static Color matches(int... ids) {
			for (Color value : values) for (int id : ids)
				if (value.dye == id)
					return value;
			return null;
		}
	}
	
	private static boolean useItem(int item1, int item2, int reward, Client c, int used, int on) {
		if ((item1 != used && item1 != on) || (item2 != used && item2 != on)) return false;
		if (c.getItems().deleteItemInOneSlot(on, c.getItems().getItemSlot(on), 1)
				&& c.getItems().deleteItemInOneSlot(used, c.getItems().getItemSlot(used), 1)) {
			c.getItems().addItem(reward, 1);
			return true;
		}
		return true;
	}
	
	public static boolean colorItem(Client c, int used, int on) {
		if (used == on) return false;
		
		ColorItem colorItem = ColorItem.matches(used, on);
		Color color = Color.matches(used, on);
		if (colorItem == null || color == null) return false;
		
		String itemName = Misc.enumName(colorItem);
		String colorName = Misc.enumName(color).toLowerCase();
		
		int reward = colorItem.items[color.ordinal()];
		if (used == reward || on == reward) {
			c.sendMessage("Your " + itemName + " is already " + colorName + ".");
			return true;
		}
		
		boolean success = useItem(color.dye, used, reward, c, used, on);
		if (success) c.sendMessage("You successfully color your " + itemName + " " + colorName + ".");
		return success;
	}
	
}
