package com.soulplay.content.items.useitem;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;

public class AncientEmblemsExchange {
	
	public static final Map<Integer, Integer> emblemPrices = new HashMap<>();
	
	static {
		emblemPrices.put(30181, 3_000_000);
		emblemPrices.put(30182, 5_000_000);
		emblemPrices.put(30183, 10_000_000);
		emblemPrices.put(30184, 50_000_000);
		emblemPrices.put(30185, 115_000_000);
		emblemPrices.put(30186, 200_000_000);
	}
	
	public static void useOnNpc(NPC n, Client c) {
		long totalGp = 0;
		int currentGp = c.getItems().getItemAmount(995);

		for (int i = 0; i < c.playerItems.length; i++) {
			int itemId = c.playerItems[i] - 1;
			if (itemId == -1) {
				continue;
			}

			Integer price = emblemPrices.get(itemId);
			if (price == null) {
				continue;
			}
			
			if (totalGp >= Integer.MAX_VALUE || totalGp + currentGp >= Integer.MAX_VALUE || totalGp + currentGp + price >= Integer.MAX_VALUE) {
				c.sendMessage("The Exchange will result in to much total Coins in your inventory.");
				break;
			}

			c.getItems().deleteItemInSlot(i);
			totalGp += price; 
		}

		if (totalGp<= 0) {
			return;
		}

		if (totalGp >= Integer.MAX_VALUE) {
			c.getItems().addOrDrop(new GameItem(995, Integer.MAX_VALUE));
			c.getItems().addOrDrop(new GameItem(995, (int) (totalGp - Integer.MAX_VALUE)));
			c.sendMessage("Some of your cash has been <col=ff0000>dropped</col> on the ground!.");
		} else {
			c.getItems().addItem(995, (int) totalGp);
		}

		c.increaseProgress(TaskType.EXCHANGE_EMBLEM, 0);
		c.sendMessage("<col=ff0000>You exchange all of your ancient emblems for " + totalGp + " Coins");
	}

}
