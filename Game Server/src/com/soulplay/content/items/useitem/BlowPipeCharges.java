package com.soulplay.content.items.useitem;

import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public class BlowPipeCharges {
	
	public static final int BLOWPIPE_EMPTY = 12924;
	public static final int BLOWPIPE_FULL = 12926;
	public static final int ZULRAH_SCALES = 30005;
	
	private static final int MAX_CHARGES = 15_000;

	public static void addScales(Client c) {
		if (Tournament.insideArenaWithSpawnedGear(c)) {
			c.sendMessage("You can't do that here.");
			return;
		}
		int amount = c.getItems().getItemAmount(ZULRAH_SCALES);
		if (amount < 1)
			return;
		
		final int maxCharges = getMaxLimit(c);
		
		if (c.getCharges(BLOWPIPE_FULL) >= maxCharges) {
			return;
		}
		if (c.getItems().playerHasItem(BLOWPIPE_EMPTY)) {
			if (c.getItems().deleteItem2(ZULRAH_SCALES, amount) && c.getItems().deleteItem2(BLOWPIPE_EMPTY, 1)) {
				c.getItems().addItem(BLOWPIPE_FULL, 1);
				int leftOver = c.addCharge(BLOWPIPE_FULL, amount, maxCharges);
				if (leftOver > 0)
					c.getItems().addItem(ZULRAH_SCALES, leftOver);
			}
		} else {
			if (c.getItems().deleteItem2(ZULRAH_SCALES, amount)) {
				int leftOver = c.addCharge(BLOWPIPE_FULL, amount, maxCharges);
				if (leftOver > 0)
					c.getItems().addItem(ZULRAH_SCALES, leftOver);
			}
		}
	}
	
	private static int getMaxLimit(Player c) {
		int addon = 0;
		if (c.getDonatedTotalAmount() > 0)
			addon = 5 * c.getDonatedTotalAmount();
		return MAX_CHARGES + addon;
	}
	
	public static void uncharge(Player c) {
		if (Tournament.insideArenaWithSpawnedGear(c)) {
			c.sendMessage("Can't do that here.");
			return;
		}
		int bpCharges = c.getCharges(BlowPipeCharges.BLOWPIPE_FULL);
		if (bpCharges < 1) {
			return;
		}
		if (c.getItems().addItem(ZULRAH_SCALES, bpCharges)) {
			c.resetCharge(BLOWPIPE_FULL);
			
		} else {
			c.sendMessage("Your inventory is full.");
			return;
		}
		if (c.getItems().deleteItem2(BLOWPIPE_FULL, 1)) {
			c.getItems().addItem(BLOWPIPE_EMPTY, 1);
		}
		
	}
}
