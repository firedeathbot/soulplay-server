package com.soulplay.content.items.useitem;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;

public class ChiselCrafting {
	
	private enum CraftItems{

		TOXIC_BLOWPIPE(30000, 12924, 0, 0, 53, 120),
		SERPENTINE_HELM(30001, 30002, 52, 120, 0, 0);
		
		public static final CraftItems[] values = values();

		private final int itemID;
		private final int itemUsed;
		private final int craftLevelReq;
		private final int craftExp;
		private final int fletchLevelReq;
		private final int fletchExp;

		private CraftItems(int itemUsed ,int itemID, int craftLevelReq, int craftExp, int fletchLevelReq, int fletchExp) {
			this.itemID = itemID;
			this.itemUsed = itemUsed;
			this.craftLevelReq = craftLevelReq;
			this.craftExp = craftExp;
			this.fletchLevelReq = fletchLevelReq;
			this.fletchExp = fletchExp;
		}
	}

	private static final int CHISEL = 1755;

	public static boolean handle(Client c, int used, int on) {
		
	
		if (used == CHISEL) {
			boolean onItem = on == CHISEL;
			for (CraftItems craft : CraftItems.values) {
				if ((onItem && used == craft.itemUsed) || (!onItem && on == craft.itemUsed)) {
					if (c.getPlayerLevel()[Skills.CRAFTING] < craft.craftLevelReq) {
						c.sendMessage("You need higher crafting level to craft this item.");
						return true;
					}
					if (c.getPlayerLevel()[Skills.FLETCHING] < craft.fletchLevelReq) {
						c.sendMessage("You need higher fletching level to craft this item.");
						return true;
					}
					if (c.getItems().deleteItemInOneSlot(on, 1)) {
						c.getItems().addItem(craft.itemID, 1);
						c.getPA().addSkillXP(craft.fletchExp, Skills.FLETCHING);
						c.getPA().addSkillXP(craft.craftExp, Skills.CRAFTING);
					}
					return true;
				}
			}
		}

		return false;
	}	
}
