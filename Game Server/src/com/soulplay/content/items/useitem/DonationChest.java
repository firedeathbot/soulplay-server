package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

public class DonationChest {
	
	public static final int KEY = 4273;
	
	public static final int CHEST = 6759;
	
	private static final int[] REWARDS = {12602, 12604, 12606, 15018, 15019, 15020,
			15220, 11726, 11724, 14484, 11720, 11722, 11718, 20135, 20139, 20143, 21777,
			20147, 20151, 20155, 20159, 20163, 20167, 13738, 13740, 13742, 13744, 11694,
			30091, 30092, 11785, 30011, 12924, 30003, 19780, 21787, 21790, 21793, 20171 };
	
	public static boolean handleDonationChest(Client c, int used, int on) {
		
		if (used == KEY || on == KEY) {
			boolean onChest = on == KEY;
				if ((onChest && used == CHEST)
						|| (!onChest && on == CHEST)) {
					if (c.getItems().deleteItemInOneSlot(used, 1)
							&& c.getItems().deleteItemInOneSlot(on, 1)) {
						c.getItems().addItem(REWARDS[Misc.random(REWARDS.length - 1)], 1);
					}
					return true;
				}
		}
		return false;
	}	
}
