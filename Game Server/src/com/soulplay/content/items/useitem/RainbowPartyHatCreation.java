package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;

public class RainbowPartyHatCreation {

	private static final int[] partyHats = new int[] { 1038, 1040, 1042, 1044, 1046, 1048 };
	
	private static final int RAINBOW_PARTYHAT = 30059;
	
	public static void partyHatOnPartyHat(Client c) {
		c.getDialogueBuilder().sendNpcChat(7090, "Would you like to mix all your PartyHats", "into a Rainbow Partyhat?")
	    .sendOption("Yes", () -> {
			initCreation(c);
	    }, "No, I would like to keep my partyhats.",() -> {
			c.getPA().closeAllWindows();
	    }).execute();
	}
	
	public static void initCreation(Client c) {
		if (!hasAllPhats(c)) {
			c.sendMessage("You need to have all party hats in your inventory to create the Rainbow phat.");
			c.getPA().closeAllWindows();
			return;
		}
		for (int element : partyHats) {
			c.getItems().deleteItem2(element, 1);
		}
		c.getItems().addItem(RAINBOW_PARTYHAT, 1);
		c.sendMessage("You have crafted a Rainbow Partyhat!");
	}
	
	public static boolean hasAllPhats(Client c) {
		for (int element : partyHats) {
			if (!c.getItems().playerHasItem(element))
				return false;
		}
		return true;
	}
	
	public static boolean usedPhatOnPhat(Client c, int item1, int item2) {
		boolean item1Found = false;
		boolean item2Found = false;
		for (int element : partyHats) {
			if (element == item1)
				item1Found = true;
			if (element == item2)
				item2Found = true;
		}
		
		return item1Found && item2Found;
	}
}
