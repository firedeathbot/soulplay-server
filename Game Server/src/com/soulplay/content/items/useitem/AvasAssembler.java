package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;

public class AvasAssembler {
	
	private enum Items {

		ASSEMBLER(30329, 10499, 122109, 888),
		;
		
		private static final Items[] values = values();

		private final int useItemID;
		private final int onItemID;
		private final int itemID;
		private final int arrows;

		private Items(int useItemID, int onItemID, int itemID, int arrows) {
			this.useItemID = useItemID;
			this.onItemID = onItemID;
			this.itemID = itemID;
			this.arrows = arrows;
		}
	}
	
	public static boolean handle(Client c, int used, int on) {
			for (Items item : Items.values) { 
				if (used == item.useItemID || on == item.useItemID) {
					boolean onItem = on == item.useItemID;
				    if ((onItem && used == item.onItemID) || (!onItem && on == item.onItemID)) {
				    	if (c.getItems().playerHasItem(item.arrows, 75)) {
					        if (c.getItems().deleteItemInOneSlot(used, 1) && c.getItems().deleteItemInOneSlot(on, 1) &&
					    		   c.getItems().deleteItemInOneSlot(item.arrows, 75)) {
						        c.getItems().addItem(item.itemID, 1);
					         }
					    } else {
					    	c.sendMessage("You need vorkath's head, ava's accumulator and 75 mithril arrows to do this");
					    }
					    return true;
				    }
				}
		}
		return false;
	}
			
}
