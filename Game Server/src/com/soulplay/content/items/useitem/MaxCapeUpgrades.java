package com.soulplay.content.items.useitem;

import com.soulplay.game.model.player.Client;

public class MaxCapeUpgrades {

	private static final int MAX_CAPE_HOOD_ID = 20768;

	public static enum Items {

		ASSEMBLER_MAX_CAPE(122109, 20767, 121898, 121900),
		INFERNAL_MAX_CAPE(30368, 20767, 121285, 121282),
		FIRE_MAX_CAPE(6570, 20767, 113329, 113330),
		IMBUED_GUTHIX_CAPE(123603, 20767, 121784, 121786),
		IMBUED_ZAMORAK_MAX_CAPE(123605, 20767, 121780, 121782),
		IMBUED_SARADOMIN_MAX_CAPE(123607, 20767, 121776, 121778),
		;

		public static final Items[] values = values();

		private final int useItemID;
		private final int onMaxCapeID;
		private final int newCapeID;
		private final int newHoodID;

		private Items(int useItemID, int onMaxCapeID, int newCapeID, int newHoodID) {
			this.useItemID = useItemID;
			this.onMaxCapeID = onMaxCapeID;
			this.newCapeID = newCapeID;
			this.newHoodID = newHoodID;
		}

		public int getNewCapeID() {
			return newCapeID;
		}

		public int getNewHoodID() {
			return newHoodID;
		}

		public int getUseItemID() {
			return useItemID;
		}

	}

	public static boolean handle(Client c, int used, int on) {
		for (Items item : Items.values) {
			if (used == item.useItemID || on == item.useItemID) {
				boolean onItem = on == item.useItemID;
				if ((onItem && used == item.onMaxCapeID) || (!onItem && on == item.onMaxCapeID)) {
					if (c.getItems().playerHasItem(MAX_CAPE_HOOD_ID)) {
						if (c.getItems().deleteItemInOneSlot(used, 1) && c.getItems().deleteItemInOneSlot(on, 1) &&
								c.getItems().deleteItemInOneSlot(MAX_CAPE_HOOD_ID, 1)) {
							c.getItems().addItem(item.newCapeID, 1);
							c.getItems().addItem(item.newHoodID, 1);
						}
					} else {
						c.sendMessage("You need to have max cape hood in your inventory to do this");
					}
					return true;
				}
			}
		}

		return false;
	}

}
