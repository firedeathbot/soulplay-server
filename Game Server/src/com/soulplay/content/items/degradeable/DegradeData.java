package com.soulplay.content.items.degradeable;

public class DegradeData {

	public static final int[][] pvpBodyNormal = {{13887, 13889}, {13884, 13886}, {13870, 13872}, {13858, 13860}};
	
	public static final int[][] pvpLegsNormal = {{13893, 13895}, {13890, 13892}, {13873, 13875}, {13861, 13863}};
	
	public static final int[][] pvpHelmNormal = {{13896, 13898}, {13876, 13878}, {13864, 13866}};
	
	public static final int[][] pvpBodyCorrupted = {{13911, 13913}, {13908, 13910}, {13944, 13946}, {13932, 13934}};
	
	public static final int[][] pvpLegsCorrupted = {{13917, 13919}, {13914, 13916}, {13947, 13949}, {13935, 13937}};
	
	public static final int[][] pvpHelmCorrupted = {{13920, 13922}, {13950, 13952}, {13938, 13940}};
	
	public static final int[][] pvpWeaponNormal = {{13899, 13901}, {13905, 13907}, {13902, 13904}, {13867, 13869}}; // 0 - normal 1 - degrade
	
	public static final int[][] pvpWeaponCorrupted = {{13923, 13925}, {13929, 13931}, {13926, 13928}, {13941, 13943}}; // 0 - normal 1 - degrade
	
	public static final int SERP_HELM = 30003;
	public static final int SERP_HELM_UNCHARGED = 30002;
	
	public static final int TSOTD = 30011;
	public static final int TSOTD_UNCHARGED = 30098;

}
