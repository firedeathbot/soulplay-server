package com.soulplay.content.items.degradeable;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.PlayerConstants;

public enum PvpDegradeableEnum {

	VESTA_BODY(13887, 13889, PlayerConstants.playerChest, 2_500, 20_000_000),
	VESTA_BODY_DEG(13889, -1, PlayerConstants.playerChest, 20_000, 20_000_000),
	STATIUS_BODY(13884, 13886, PlayerConstants.playerChest, 2_500, 20_000_000),
	STATIUS_BODY_DEG(13886, -1, PlayerConstants.playerChest, 20_000, 20_000_000),
	MORRIGAN_BODY(13870, 13872, PlayerConstants.playerChest, 2_500, 20_000_000),
	MORRIGAN_BODY_DEG(13872, -1, PlayerConstants.playerChest, 20_000, 20_000_000),
	ZURIEL_BODY(13858, 13860, PlayerConstants.playerChest, 2_500, 20_000_000),
	ZURIEL_BODY_DEG(13860, -1, PlayerConstants.playerChest, 20_000, 20_000_000),
	C_VESTA_BODY(13911, 13913, PlayerConstants.playerChest, 1_250, 10_000_000),
	C_VESTA_BODY_DEG(13913, -1, PlayerConstants.playerChest, 10_000, 10_000_000),
	C_STATIUS_BODY(13908, 13910, PlayerConstants.playerChest, 1_250, 10_000_000),
	C_STATIUS_BODY_DEG(13910, -1, PlayerConstants.playerChest, 10_000, 10_000_000),
	C_MORRIGAN_BODY(13944, 13946, PlayerConstants.playerChest, 1_250, 10_000_000),
	C_MORRIGAN_BODY_DEG(13946, -1, PlayerConstants.playerChest, 10_000, 10_000_000),
	C_ZURIEL_BODY(13932, 13934, PlayerConstants.playerChest, 1_250, 10_000_000),
	C_ZURIEL_BODY_DEG(13934, -1, PlayerConstants.playerChest, 10_000, 10_000_000),
	
	VESTA_LEGS(13893, 13895, PlayerConstants.playerLegs, 2_500, 20_000_000),
	VESTA_LEGS_DEG(13895, -1, PlayerConstants.playerLegs, 20_000, 20_000_000),
	STATIUS_LEGS(13890, 13892, PlayerConstants.playerLegs, 2_500, 20_000_000),
	STATIUS_LEGS_DEG(13892, -1, PlayerConstants.playerLegs, 20_000, 20_000_000),
	MORRIGAN_LEGS(13873, 13875, PlayerConstants.playerLegs, 2_500, 20_000_000),
	MORRIGAN_LEGS_DEG(13875, -1, PlayerConstants.playerLegs, 20_000, 20_000_000),
	ZURIEL_LEGS(13861, 13863, PlayerConstants.playerLegs, 2_500, 20_000_000),
	ZURIEL_LEGS_DEG(13863, -1, PlayerConstants.playerLegs, 20_000, 20_000_000),
	C_VESTA_LEGS(13917, 13919, PlayerConstants.playerLegs, 1_250, 10_000_000),
	C_VESTA_LEGS_DEG(13919, -1, PlayerConstants.playerLegs, 10_000, 10_000_000),
	C_STATIUS_LEGS(13914, 13916, PlayerConstants.playerLegs, 1_250, 10_000_000),
	C_STATIUS_LEGS_DEG(13916, -1, PlayerConstants.playerLegs, 10_000, 10_000_000),
	C_MORRIGAN_LEGS(13947, 13949, PlayerConstants.playerLegs, 1_250, 10_000_000),
	C_MORRIGAN_LEGS_DEG(13949, -1, PlayerConstants.playerLegs, 10_000, 10_000_000),
	C_ZURIEL_LEGS(13935, 13937, PlayerConstants.playerLegs, 1_250, 10_000_000),
	C_ZURIEL_LEGS_DEG(13937, -1, PlayerConstants.playerLegs, 10_000, 10_000_000),
	

	STATIUS_HELM(13896, 13898, PlayerConstants.playerHat, 2_500, 7_000_000),
	STATIUS_HELM_DEG(13898, -1, PlayerConstants.playerHat, 20_000, 7_000_000),
	MORRIGAN_HELM(13876, 13878, PlayerConstants.playerHat, 2_500, 7_000_000),
	MORRIGAN_HELM_DEG(13878, -1, PlayerConstants.playerHat, 20_000, 7_000_000),
	ZURIEL_HELM(13864, 13866, PlayerConstants.playerHat, 2_500, 7_000_000),
	ZURIEL_HELM_DEG(13866, -1, PlayerConstants.playerHat, 20_000, 7_000_000),
	C_STATIUS_HELM(13920, 13922, PlayerConstants.playerHat, 1_250, 4_000_000),
	C_STATIUS_HELM_DEG(13922, -1, PlayerConstants.playerHat, 10_000, 4_000_000),
	C_MORRIGAN_HELM(13950, 13952, PlayerConstants.playerHat, 1_250, 4_000_000),
	C_MORRIGAN_HELM_DEG(13952, -1, PlayerConstants.playerHat, 10_000, 4_000_000),
	C_ZURIEL_HELM(13938, 13940, PlayerConstants.playerHat, 1_250, 4_000_000),
	C_ZURIEL_HELM_DEG(13940, -1, PlayerConstants.playerHat, 10_000, 4_000_000),
	

	VESTA_LOGSWORD(13899, 13901, PlayerConstants.playerWeapon, 2_500, 50_000_000),
	VESTA_LOGSWORD_DEG(13901, -1, PlayerConstants.playerWeapon, 20_000, 50_000_000),
	VESTA_SPEAR(13905, 13907, PlayerConstants.playerWeapon, 2_500, 40_000_000),
	VESTA_SPEAR_DEG(13907, -1, PlayerConstants.playerWeapon, 20_000, 40_000_000),
	STATIUS_WARHAMMER(13902, 13904, PlayerConstants.playerWeapon, 2_500, 30_000_000),
	STATIUS_WARHAMMER_DEG(13904, -1, PlayerConstants.playerWeapon, 20_000, 30_000_000),
	ZURIEL_STAFF(13867, 13869, PlayerConstants.playerWeapon, 2_500, 20_000_000),
	ZURIEL_STAFF_DEG(13869, -1, PlayerConstants.playerWeapon, 20_000, 20_000_000),
	C_VESTA_LOGSWORD(13923, 13925, PlayerConstants.playerWeapon, 1_250, 25_000_000),
	C_VESTA_LOGSWORD_DEG(13925, -1, PlayerConstants.playerWeapon, 10_000, 25_000_000),
	C_VESTA_SPEAR(13929, 13931, PlayerConstants.playerWeapon, 1_250, 15_000_000),
	C_VESTA_SPEAR_DEG(13931, -1, PlayerConstants.playerWeapon, 10_000, 15_000_000),
	C_STATIUS_WARHAMMER(13926, 13928, PlayerConstants.playerWeapon, 1_250, 15_000_000),
	C_STATIUS_WARHAMMER_DEG(13928, -1, PlayerConstants.playerWeapon, 10_000, 15_000_000),
	C_ZURIEL_STAFF(13941, 13943, PlayerConstants.playerWeapon, 1_250, 10_000_000),
	C_ZURIEL_STAFF_DEG(13943, -1, PlayerConstants.playerWeapon, 10_000, 10_000_000),
	
	
	//crystal bow
	CRYSTAL_BOW_NEW(4212, 4214, PlayerConstants.playerWeapon, 1_000, 500_000),
	CRYSTAL_BOW_FULL(4214, -1, PlayerConstants.playerWeapon, 2, 450_000),
	
	;
	
	
	
	private final int itemId, replaceId, slot, chance, goldReward;
	
	private PvpDegradeableEnum(int itemId, int replaceId, int slot, int chance, int goldReward) {
		this.itemId = itemId;
		this.replaceId = replaceId;
		this.slot = slot;
		this.chance = chance;
		this.goldReward = goldReward;
	}

	public int getItemId() {
		return itemId;
	}

	public int getReplaceId() {
		return replaceId;
	}

	public int getSlot() {
		return slot;
	}

	public int getChance() {
		return chance;
	}
	
	public int getGoldReward() {
		return goldReward;
	}
	
	private static final Map<Integer, PvpDegradeableEnum> bodyMap = new HashMap<>();
	private static final Map<Integer, PvpDegradeableEnum> legsMap = new HashMap<>();
	private static final Map<Integer, PvpDegradeableEnum> helmMap = new HashMap<>();
	private static final Map<Integer, PvpDegradeableEnum> weaponMap = new HashMap<>();
	
	public static void init() {
		for (PvpDegradeableEnum pvp : PvpDegradeableEnum.values()) {
			switch (pvp.slot) {
			case PlayerConstants.playerChest:
				bodyMap.put(pvp.getItemId(), pvp);
				break;
			case PlayerConstants.playerLegs:
				legsMap.put(pvp.getItemId(), pvp);
				break;
			case PlayerConstants.playerHat:
				helmMap.put(pvp.getItemId(), pvp);
				break;
			case PlayerConstants.playerWeapon:
				weaponMap.put(pvp.getItemId(), pvp);
				break;
			}
		}
	}
	
	public static PvpDegradeableEnum getBody(int itemId) {
		return bodyMap.get(itemId);
	}

	public static PvpDegradeableEnum getLegs(int itemId) {
		return legsMap.get(itemId);
	}

	public static PvpDegradeableEnum getHelm(int itemId) {
		return helmMap.get(itemId);
	}

	public static PvpDegradeableEnum getWeapon(int itemId) {
		return weaponMap.get(itemId);
	}
}
