package com.soulplay.content.items.degradeable;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.util.Misc;

public enum BrawlerGloveEnum {

	MELEE(13845, 1000, Skills.ATTACK, Skills.STRENGTH, Skills.DEFENSE),
	RANGED(13846, 1000, Skills.RANGED),
	MAGIC(13847, 1000, Skills.MAGIC),
	PRAYER(13848, 400, Skills.PRAYER),
	AGILITY(13849, 300, Skills.AGILITY),
	WC(13850, 400, Skills.WOODCUTTING),
	FM(13851, 400, Skills.FIREMAKING),
	MINING(13852, 400, Skills.MINING),
	HUNTER(13853, 400, Skills.HUNTER),
	THIEVING(13854, 450, Skills.THIEVING),
	SMITHING(13855, 400, Skills.SMITHING),
	FISHING(13856, 400, Skills.FISHING),
	COOKING(13857, 400, Skills.COOKING)
	;
	
	private final int id;
	private final int charges;
	private final int[] skillIds;
	
	private BrawlerGloveEnum(int id, int charges, int... skillIds) {
		this.id = id;
		this.charges = charges;
		this.skillIds = skillIds;
	}

	public int getId() {
		return id;
	}
	
	public int getCharges() {
		return charges;
	}
	
	public boolean isCorrectSkillId(int skillId) {
		for (int i = 0; i < skillIds.length; i++) {
			if (skillId == skillIds[i]) {
				return true;
			}
		}
		return false;
	}
	
	public static final BrawlerGloveEnum[] values = BrawlerGloveEnum.values();
	
	private static final Map<Integer, BrawlerGloveEnum> mapByItemId = new HashMap<>();
	
	static {
		for (BrawlerGloveEnum e : values) {
			mapByItemId.put(e.getId(), e);
		}
	}
	
	public static BrawlerGloveEnum get(int id) {
		return mapByItemId.get(id);
	}
	
	public static int getRandomGlove() {
		return values[Misc.newRandom(values.length-1)].getId();
	}

}
