package com.soulplay.content.items.degradeable;

import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.util.Misc;

public class Degrade extends DegradeData {

	private static final String DEG_MSG = "Your %s has ran out of charges.";

	public static void degradeArmor(Client c) {
		
		if (Tournament.insideArenaWithSpawnedGear(c))
			return;

		// CHEST
		if (c.playerEquipment[PlayerConstants.playerChest] > 0) { // dung degradeable body for defending player

			int itemId = c.playerEquipment[PlayerConstants.playerChest];

			ItemDefinition def = ItemDefinition.forId(itemId);

			if (def != null && def.isCharged()) {
				if (c.reduceCharge(itemId, 1) == 0) {
					c.getItems().replaceEquipment(PlayerConstants.playerChest, def.getChargeInfo().getUnchargedId());
					c.sendMessage(String.format(DEG_MSG, def.getName()));
				}
				return;
			}

			final PvpDegradeableEnum body = PvpDegradeableEnum.getBody(itemId);

			if (body != null) {
				c.getItems().replaceEquipment(PlayerConstants.playerChest, body.getReplaceId());
				if (body.getReplaceId() == -1)
					c.sendMessage("Your armor piece turns into dust.");
				else {
					c.sendMessage("Your armor begins to degrade.");
					c.addCharge(body.getReplaceId(), body.getChance(), body.getChance());
				}
			}
		}

		//LEGS
		if (c.playerEquipment[PlayerConstants.playerLegs] > 0) { // dung degradeable legs for defending player

			int itemId = c.playerEquipment[PlayerConstants.playerLegs];

			ItemDefinition def = ItemDefinition.forId(itemId);

			if (def.isCharged()) {
				if (c.reduceCharge(itemId, 1) == 0) {
					c.getItems().replaceEquipment(PlayerConstants.playerLegs, def.getChargeInfo().getUnchargedId());
					c.sendMessage(String.format(DEG_MSG, def.getName()));
				}
				return;
			}

			final PvpDegradeableEnum legs = PvpDegradeableEnum.getLegs(itemId);

			if (legs != null) {
				c.getItems().replaceEquipment(PlayerConstants.playerLegs, legs.getReplaceId());
				if (legs.getReplaceId() == -1)
					c.sendMessage("Your armor piece turns into dust.");
				else {
					c.sendMessage("Your armor begins to degrade.");
					c.addCharge(legs.getReplaceId(), legs.getChance(), legs.getChance());
				}
			}
		}

		// HAT
		if (c.playerEquipment[PlayerConstants.playerHat] > 0) { // dung degradeable helm for defending player

			int itemId = c.playerEquipment[PlayerConstants.playerHat];

			ItemDefinition def = ItemDefinition.forId(itemId);

			if (def.isCharged()) {
				if (c.reduceCharge(itemId, 1) == 0) {
					c.getItems().replaceEquipment(PlayerConstants.playerHat, def.getChargeInfo().getUnchargedId());
					c.sendMessage(String.format(DEG_MSG, def.getName()));
				}
				return;
			}

			final PvpDegradeableEnum helm = PvpDegradeableEnum.getHelm(itemId);

			if (helm != null) {
				c.getItems().replaceEquipment(PlayerConstants.playerHat, helm.getReplaceId());
				if (helm.getReplaceId() == -1)
					c.sendMessage("Your head gear turns into dust.");
				else {
					c.sendMessage("Your head gear begins to degrade.");
					c.addCharge(helm.getReplaceId(), helm.getChance(), helm.getChance());
				}
			}
		}
		
		//GLOVES HANDS
		if (c.playerEquipment[PlayerConstants.playerHands] > 0) { // degradeable hands/gloves for defending player

			int itemId = c.playerEquipment[PlayerConstants.playerHands];

			ItemDefinition def = ItemDefinition.forId(itemId);

			if (def.isCharged()) {
				if (c.reduceCharge(itemId, 1) == 0) {
					c.getItems().replaceEquipment(PlayerConstants.playerHands, def.getChargeInfo().getUnchargedId());
					c.sendMessage(String.format(DEG_MSG, def.getName()));
				}
				return;
			}

		}
	}

	public static void degradeWeapons(Client c) { // melee weapons degrade?
		
		if (Tournament.insideArenaWithSpawnedGear(c))
			return;
		
		if (c.playerEquipment[PlayerConstants.playerWeapon] > 0) { // pvp degradeable weapons

			int itemId = c.playerEquipment[PlayerConstants.playerWeapon];

			ItemDefinition def = ItemDefinition.forId(itemId);

			if (def != null && def.isCharged()) {
				if (c.reduceCharge(itemId, 1) == 0) {
					c.getItems().replaceEquipment(PlayerConstants.playerWeapon, def.getChargeInfo().getUnchargedId());
					c.sendMessage(String.format(DEG_MSG, def.getName()));
				}
				return;
			}

			final PvpDegradeableEnum weapon = PvpDegradeableEnum.getWeapon(itemId);

			if (weapon != null) {
				c.getItems().replaceEquipment(PlayerConstants.playerWeapon, weapon.getReplaceId());
				if (weapon.getReplaceId() == -1)
					c.sendMessage("Your weapon turns into dust.");
				else {
					c.sendMessage("Your weapon begins to degrade.");
					c.addCharge(weapon.getReplaceId(), weapon.getChance(), weapon.getChance());
				}
				return;
			}

			degradeZBow(c);
		}
	}

	public static void degradeZBow(Client c) {
		
		if (Tournament.insideArenaWithSpawnedGear(c))
			return;
		
		if (c.playerEquipment[PlayerConstants.playerWeapon] == 20173 && Misc.random(30000) == 7) { // second degrade
			c.sendMessage("Your Zaryte Bow broke.");
			c.getItems().replaceEquipment(PlayerConstants.playerWeapon, 20174);
		}

		if (c.playerEquipment[PlayerConstants.playerWeapon] == 20171 && Misc.random(30000) == 7) { // first degrade
			c.getItems().replaceEquipment(PlayerConstants.playerWeapon, 20173);
		}
	}

}
