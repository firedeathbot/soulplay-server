package com.soulplay.content.items.pricechecker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.commons.io.FileUtils;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.ShopHandler;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.LogHandler;
import com.soulplay.util.sql.WebsiteSQL;

public class PriceChecker {

	private final static String FILE_LOCATION = "./Data/databases/pricecheck.json";

	private final static String FILE_LOCATION2 = "./Data/databases/cachedPrices.json";

	private static HashMap<Integer, ArrayList<PriceCheckTrade>> prices = new HashMap<>();

	private static long[] cachedPrices = new long[Config.ITEM_LIMIT];

	public static void updatePrices() {
		
		if (!Config.RUN_ON_DEDI) {
			return;
		}
		
		System.out.println("Updating all prices!!!!!!");
		
		TaskExecutor.executeSlack(new Runnable() {
			
			@Override
			public void run() {
				int updatedAmount = 0;
				cachedPrices = new long[Config.ITEM_LIMIT];
				for (int itemId = 0; itemId < cachedPrices.length; itemId++) {
					
					if (ShopHandler.REGULAR_SHOP_ITEMS.contains(itemId)) {
						continue;
					}

					try (Connection connection = LogHandler.getLogHikari().getConnection();
							PreparedStatement ps = connection.prepareStatement("SELECT `amount`, `price` FROM `buy_pos_log` WHERE `item` = "+itemId+" ORDER BY `buy_pos_log`.`timestamp` DESC LIMIT 200");
							ResultSet rs = ps.executeQuery() ) {
						
						int totalCount = 0;
						long totalPrice = 0;
						
						long averagePrice = 0;
						
						while (rs.next()) {
							int amountBought = rs.getInt("amount");
							long priceForOnePurchase = rs.getLong("price");
							
							if (priceForOnePurchase == 1) // ignore 1 gp item prices
								continue;
							
							if (amountBought == 1) {
								if (cachedPrices[itemId] > 1 && totalCount > 10) {
									//test if price is way over 40%, then ignore it.
									long crazyHighPrice = (long)(cachedPrices[itemId] * 1.4);
									long crazyLowPrice = (long)(cachedPrices[itemId] * 1.4);
									if (priceForOnePurchase < crazyLowPrice || priceForOnePurchase > crazyHighPrice)
										continue;
								}
								totalPrice += priceForOnePurchase;
							} else {
								final long dividedPrice = priceForOnePurchase/amountBought;
								
								if (dividedPrice == 1) continue; // ignore 1 gp item prices
								
								if (cachedPrices[itemId] > 1 && totalCount > 10) {
									//test if price is way over 40%, then ignore it.
									long crazyHighPrice = (long)(cachedPrices[itemId] * 1.4);
									long crazyLowPrice = (long)(cachedPrices[itemId] * 0.4);
									if (dividedPrice < crazyLowPrice || dividedPrice > crazyHighPrice)
										continue;
								}
								
								totalPrice += dividedPrice;
							}
							
							totalCount++;
						}
						if (totalCount > 0) {
							averagePrice = totalPrice/totalCount;
							cachedPrices[itemId] = averagePrice;
							ItemDef def = ItemDef.forID(itemId);
							if (def != null && def.itemCanBeNoted() && !def.isInNotePosition())
								cachedPrices[def.getNoteId()] = averagePrice;
							updatedAmount++;
						}
					} catch (SQLException e) {
						e.printStackTrace();
						break;
					}

				}
				
				saveCachedPrices();
				setDropPrices();
				System.out.println("FINISHED Updating all prices!!!!!! COUNT:"+updatedAmount);
			}
		});
	}
	
	private static String BACKUP_PRICE = "INSERT INTO `zaryte`.`item_price`(`item_id`, `average_price`) VALUES (?, ?) ON DUPLICATE KEY UPDATE item_id = VALUES(item_id), average_price = VALUES(average_price)";

	private static void uploadAveragePriceToDB(int itemId, long averagePrice) {
		try (Connection connection = WebsiteSQL.getHikari().getConnection();
				PreparedStatement stmt = connection.prepareStatement(BACKUP_PRICE)) {
			stmt.setInt(1, itemId);
			stmt.setLong(2, averagePrice);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void addToPriceCheck(final String player1,
			final Object object1, final String player2, final Object object2) {

		if (Config.NODE_ID != 1) {
			return;
		}

		TaskExecutor.executeSlack(() -> {

			try {

				CopyOnWriteArrayList<GameItem> offer1 = (CopyOnWriteArrayList<GameItem>) object1;
				CopyOnWriteArrayList<GameItem> offer2 = (CopyOnWriteArrayList<GameItem>) object2;

				String buyerName = "";
				int buyPrice = 0;
				int itemBought = 0;
				int itemBoughtAmount = 0;
				boolean addToPriceChecker = true;

				if (offer1.size() == 1 && offer1.get(0).getId() == 995) { // get
																			// a
																			// trade
																			// with
																			// gold
																			// only
					buyerName = player1;
					buyPrice = offer1.get(0).getAmount();
					int sameItem = offer2.get(0).getId();
					for (GameItem item : offer2) {
						if (sameItem != item.getId()) {
							addToPriceChecker = false;
							break;
						}
						itemBought = item.getId();
						itemBoughtAmount += item.getAmount();
					}
				} else if (offer2.size() == 1 && offer2.get(0).getId() == 995) { // get
																					// a
																					// trade
																					// with
																					// gold
																					// only
					buyerName = player2;
					buyPrice = offer2.get(0).getAmount();
					int sameItem = offer1.get(0).getId();
					for (GameItem item : offer1) {
						if (sameItem != item.getId()) {
							addToPriceChecker = false;
							break;
						}
						itemBought = item.getId();
						itemBoughtAmount += item.getAmount();
					}
				}

				if (!addToPriceChecker) {
					return;
				}

				if (itemBoughtAmount > 1) { // divide gold to get gold value of
											// only one item
					double buyPriceL = buyPrice;
					double itemBoughtL = itemBoughtAmount;
					double newPrice = Math.round(buyPriceL / itemBoughtL);

					buyPrice = (int) newPrice;

				}

				int day = Server.getCalendar().getDayOfYear();

				if (!prices.containsKey(itemBought)) { // if item is not in
														// price checker tool
														// then start new price
					ArrayList<PriceCheckTrade> list = new ArrayList<>();
					PriceCheckTrade trade = new PriceCheckTrade(buyerName,
							itemBought, buyPrice, day);
					list.add(trade);
					prices.put(itemBought, list);
					return;
				} else {
					long averagePrice = 0;
					// this is where we check for same player name who already
					// bought item recently(to filter exploit)
					for (PriceCheckTrade trade : prices.get(itemBought)) {
						if (trade.getBuyer().equals(buyerName)
								&& trade.getTradeDate() == day) {
							addToPriceChecker = false;
							break;
						}
						averagePrice += trade.getItemCost();
					}
					if (!addToPriceChecker) {
						return;
					}

					int listSize = prices.get(itemBought).size();

					double averageDouble = averagePrice / listSize;

					PriceCheckTrade trade = new PriceCheckTrade(buyerName,
							itemBought, buyPrice, day);

					if (listSize > 20) {

						if (Config.PRICE_CHECK_PERCENTAGE_FILTER) {

							int tenPercent = (int) (averageDouble
									* (70.0f / 100.0f));

							if (buyPrice >= (int) averageDouble - tenPercent
									&& buyPrice <= (int) averageDouble
											+ tenPercent) {
								prices.get(itemBought).add(trade);
								if (listSize > 200) {
									prices.remove(0);
								}
							}
						} else {
							prices.get(itemBought).add(trade);
							if (listSize > 200) {
								prices.remove(0);
							}
						}

					} else {
						prices.get(itemBought).add(trade);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		sendAveragePriceToLoginServer();
	}

	public static long getPriceLong(int itemId) {
		if (itemId == 995)
			return 1;
		if (getCachedPrices() == null || itemId >= getCachedPrices().length)
			return 0;
		long rawInt = getCachedPrices()[itemId];

		return rawInt; // Misc.roundNearest(rawInt);
	}

	public static int getAveragePriceOLDCODE(int itemId) {
		if (prices.containsKey(itemId)) {

			long averageTotal = 0;

			for (PriceCheckTrade trade : prices.get(itemId)) {
				averageTotal += trade.getItemCost();
			}

			int listSize = prices.get(itemId).size();

			double averageDouble = averageTotal / listSize;

			return (int) averageDouble;// Misc.roundNearest((int)
										// averageDouble);

		}
		return 0;
	}

	public static long[] getCachedPrices() {
		return cachedPrices;
	}

	// public static void cachePrices() {
	// service.submit(() -> {
	//
	// for (int i = 0; i < cachedPrices.length; i++) {
	// int averagePrice = getAveragePrice(i);
	// if (averagePrice > 0) {
	// getCachedPrices()[i] = averagePrice;
	// }
	// }
	//
	// });
	// }

	public static void load() throws IOException {
		if (Config.NODE_ID != 1) {
			return;
		}
		prices = GsonSave.gsond.fromJson(
				FileUtils.readFileToString(new File(FILE_LOCATION)),
				new TypeToken<HashMap<Integer, ArrayList<PriceCheckTrade>>>() {
				}.getType());
	}

	public static void loadCachedPrices() throws IOException {
		File file = new File(FILE_LOCATION2);

		if (!file.exists()) {
			return;
		}

		cachedPrices = new long[Config.ITEM_LIMIT];
		cachedPrices = GsonSave.gsond.fromJson(
				FileUtils.readFileToString(new File(FILE_LOCATION2)),
				new TypeToken<long[]>() {
				}.getType());
		
		setDropPrices();
	}

	public static boolean resetAveragePriceItem(int itemId) {
		if (prices.containsKey(itemId)) {
			prices.get(itemId).clear();
			return true;
		}
		return false;
	}

	public static void saveCachedPrices() {
		try (Writer writer = new FileWriter(FILE_LOCATION2)) {
			GsonSave.gsond.toJson(cachedPrices, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveToFile() {
		if (Config.NODE_ID != 1) {
			return;
		}

		try (Writer writer = new FileWriter(FILE_LOCATION)) {
			GsonSave.gsond.toJson(prices, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void sendAveragePriceToLoginServer() {
		if (Config.NODE_ID != 1) {
			return;
			// service.submit(() -> {
		}

		LoginServerConnection.instance().submit(() -> {
			for (int i = 0; i < Config.ITEM_LIMIT; i++) {
				int averagePrice = getAveragePriceOLDCODE(i);
				if (averagePrice > 0 && averagePrice != getCachedPrices()[i]) {
					LoginServerConnection.instance().sendPrice(i, averagePrice);
				}
			}

		});

		// });
	}
	
	public static void setDropPrices() {
		long minOut = (long)1;
		long maxOut = (long)Integer.MAX_VALUE;
		long inputMin = (long)10;
		long inputMax = 20_000_000_000l;
		for (int itemId = 0; itemId < cachedPrices.length; itemId++) {
			if (cachedPrices[itemId] > 10) {
				double newValueLong = Misc.interpolateDouble(minOut, maxOut, inputMin, inputMax, cachedPrices[itemId]);
				int newValue = (int)newValueLong;
				ItemDefinition def = ItemDefinition.forId(itemId);
				if (def != null && def.getDropValue() > 5_000) {
					//if (def.getDropValue() < newValue)
						def.setDropValue(newValue);
						
						if (itemId == 12924) {
							ItemDefinition chargedPipe = ItemDefinition.forId(12926);
							if (chargedPipe != null)
								chargedPipe.setDropValue(newValue);
						}
						if (itemId == 30002) {
							ItemDefinition chargedHelm = ItemDefinition.forId(30003);
							if (chargedHelm != null)
								chargedHelm.setDropValue(newValue);
						}
						if (itemId == 30098) {
							ItemDefinition chargedStaff = ItemDefinition.forId(30011);
							if (chargedStaff != null)
								chargedStaff.setDropValue(newValue);
						}
						
						if (def.getChargeInfo() != null) {
							if (def.getChargeInfo().getChargedId() != -1) {
								ItemDefinition charged = ItemDefinition.forId(def.getChargeInfo().getChargedId());
								if (charged != null)
									charged.setDropValue(newValue);
							}
							if (def.getChargeInfo().getUnchargedId() != -1) {
								ItemDefinition uncharged = ItemDefinition.forId(def.getChargeInfo().getUnchargedId());
								if (uncharged != null)
									uncharged.setDropValue(newValue);
							}
						}
				}
			}
		}
	}

}
