package com.soulplay.content.items.pricechecker;

public class PriceCheckTrade {

	private String buyer;

	private int itemBought;

	private int itemCost;

	private int tradeDate;

	public PriceCheckTrade(String buyer, int itemId, int itemCost,
			int tradeDate) {
		this.setBuyer(buyer);
		this.setItemBought(itemId);
		this.setItemCost(itemCost);
		this.setTradeDate(tradeDate);
	}

	public String getBuyer() {
		return buyer;
	}

	public int getItemBought() {
		return itemBought;
	}

	public int getItemCost() {
		return itemCost;
	}

	public int getTradeDate() {
		return tradeDate;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public void setItemBought(int itemBought) {
		this.itemBought = itemBought;
	}

	public void setItemCost(int itemCost) {
		this.itemCost = itemCost;
	}

	public void setTradeDate(int tradeDate) {
		this.tradeDate = tradeDate;
	}

}
