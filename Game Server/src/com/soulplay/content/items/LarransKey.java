package com.soulplay.content.items;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.util.Misc;

import plugin.location.wilderness.LarransChestPlugin;

public class LarransKey {

	public static void onSlayerMobKilled(Player p, NPC npc) {
		int maxLp = npc.getSkills().getMaximumLifepoints();
		if (maxLp <= 0) {
			return;
		}
		
		int chance = Misc.interpolateSafe(1972, 50, 5, 500, maxLp);
		
		if (Misc.randomBoolean(chance)) {
			p.sendMessage("<col=ff0000>Untradeable drop: Larran's key</col>");
			ItemHandler.createGroundItem(p.getName(), LarransChestPlugin.KEY_ITEM, npc.absX, npc.absY, npc.heightLevel, 1, true, p.isIronMan());
		}
	}
}
