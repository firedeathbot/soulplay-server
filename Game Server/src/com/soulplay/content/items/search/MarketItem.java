package com.soulplay.content.items.search;

public class MarketItem implements Comparable<Object> {

	private String playerName;

	private int playerShopIndex;

	private long itemPrice;

	private int itemSoldAmount;

	public MarketItem(String name, int pIndex, long itemPrice,
			int itemSoldAmount) {
		this.setPlayerName(name);
		this.setPlayerShopIndex(pIndex);
		this.setItemPrice(itemPrice);
		this.setItemSoldAmount(itemSoldAmount);
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}

	public long getItemPrice() {
		return itemPrice;
	}

	public int getItemSoldAmount() {
		return itemSoldAmount;
	}

	public String getPlayerName() {
		return playerName;
	}

	public int getPlayerShopIndex() {
		return playerShopIndex;
	}

	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}

	public void setItemSoldAmount(int itemSoldAmount) {
		this.itemSoldAmount = itemSoldAmount;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setPlayerShopIndex(int playerShopIndex) {
		this.playerShopIndex = playerShopIndex;
	}

}
