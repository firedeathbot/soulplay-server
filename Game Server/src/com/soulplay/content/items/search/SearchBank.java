package com.soulplay.content.items.search;

import com.soulplay.cache.ItemDef;
import com.soulplay.game.model.player.Client;

public class SearchBank {

	public static boolean searchBank(Client c, int itemId) {

		ItemDef searchedDef = ItemDef.forID(itemId);

		if (searchedDef == null || searchedDef.name == null) {
			c.getPA().closeAllWindows();
			c.sendMessage("Error finding that item.");
			return false;
		}

		String searchedName = searchedDef.getName();

		for (int i = 0; i < c.bankItems0.length; i++) {
			if (c.bankItems0[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems0[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 0, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems1.length; i++) {
			if (c.bankItems1[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems1[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 1, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems2.length; i++) {
			if (c.bankItems2[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems2[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 2, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems3.length; i++) {
			if (c.bankItems3[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems3[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 3, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems4.length; i++) {
			if (c.bankItems4[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems4[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 4, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems5.length; i++) {
			if (c.bankItems5[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems5[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 5, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems6.length; i++) {
			if (c.bankItems6[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems6[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 6, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems7.length; i++) {
			if (c.bankItems7[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems7[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 7, i);
				return true;
			}
		}
		for (int i = 0; i < c.bankItems8.length; i++) {
			if (c.bankItems8[i] <= 0) {
				break;
			}
			ItemDef def = ItemDef.forID(c.bankItems8[i] - 1);
			if (def == null) {
				continue;
			}
			String name = def.getName();
			if (name == null) {
				continue;
			}
			if (name.equals(searchedName)) {
				setScrollPosition(c, 8, i);
				return true;
			}
		}

		c.sendMessage("You do not have this item in the bank.");
		return false;
	}

	public static void setScrollPosition(Client c, int bankTabId,
			int bankItemIndex) {
		c.getPA().openUpBank(bankTabId);
		int scrollPosition = 38 * (bankItemIndex / 10);
		c.getPacketSender().setScrollPos(5385, scrollPosition);
	}

}
