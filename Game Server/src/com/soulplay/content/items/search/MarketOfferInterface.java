package com.soulplay.content.items.search;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.DBConfig;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.pos.BuyOffers;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.util.Misc;

public class MarketOfferInterface {
	
	private final Client c;
	
	private int sellingItemId;
	private int notNotedId;
	private int buyOfferItemId;
	private int quantity;
	private long price;
	private int prevInterface;
	private boolean isActive = false;
	private boolean isInShop = false;
	private boolean loadingOwnOffers = false;
	private int slotToUse;
	private boolean loadingPublicOffers = false;
	private boolean saleInProgress = false;
	
	private final TickTimer timer = new TickTimer();
	
	public MarketOfferInterface(Client c) {
		this.c = c;
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public boolean openSellOffer(int itemId) {
		if (c.playerRights == 2 && !DBConfig.ADMIN_CAN_SELL_ITEMS) {
			c.sendMessage("Selling items as an admin has been disabled.");
			return true;
		}
		if (itemId == 995 || itemId == 8890 || itemId == 6529 || itemId == 10831) {
			c.sendMessage("You can't sell this.");
			return true;
		}
		if (isActive)
			return false;
		prevInterface = c.interfaceIdOpenMainScreen;
		c.getPacketSender().showInterface(19500);
		notNotedId = sellingItemId = itemId;
		ItemDef def = ItemDef.forID(itemId);
		if (def != null && def.itemIsInNotePosition) {
			notNotedId = def.getNoteId();
		}
		setQuantity(1);
		setPrice(1);
		long priceP = PriceChecker.getPriceLong(itemId);
		if (priceP > 0) {
			setPrice(priceP);
		}
		updateAveragePrice();
		c.getPacketSender().changeItemId(19503, notNotedId);
		isActive = true;
		isInShop = true;
		return true;
	}
	
	public boolean openBuyOffer(int slot) {
		if (isActive)
			return false;
		if (c.getOwnBuyOffers().containsKey(slot)) {
			c.sendMessage("You already have an order on that slot durp...");
			return false;
		}
		prevInterface = c.interfaceIdOpenMainScreen;
		c.getPacketSender().showInterface(19500);
		c.getPacketSender().changeItemId(19503, -1);
		c.getMarketOffer().setQuantity(0);
		c.getMarketOffer().setPrice(0);
		c.getMarketOffer().setSlotToUse(slot);
		updateAveragePrice();
		isActive = true;
		isInShop = false;
		return true;
	}
	
	public boolean clickButton(Client c, int button) {
		if (!isActive)
			return false;
		if (button == 76052) { // back button
			if (prevInterface == 3824) { // shop interface?
				PlayerOwnedShops.openPlayerShop(c, c);
			}
			close();
			return true;
		} else if (button == 76050) { // confirm button
			if (isInShop) {
				if (price == 0) {
					c.sendMessage("Please set a price first before confirming the offer.");
					return true;
				}

				PlayerOwnedShops.placeFirstItemWithSetPrice(c, sellingItemId, quantity, price, notNotedId);
				close();
			} else { // creating buy offer
				long totalPrice = getTotalPrice();
				if (totalPrice == -1) {
					c.sendMessage("Wrong.");
					return true;
				}

				if (c.getMoneyPouch() < totalPrice) {
					c.sendMessage("Your money pouch does not have enough Gold to create this offer.");
					c.sendMessage("Please add Gold to your Money Pouch then create an offer.");
					return true;
				}
				if (price < 1 || quantity < 1 || buyOfferItemId < 1) {
					return true;
				}
				if (isLoadingOwnOffers()) {
					c.sendMessage("Please try again later.");
					return true;
				}
				BuyOffers.createBuyOffer(c, new Item(buyOfferItemId, quantity, getSlotToUse()), totalPrice, getPrice());
				close();
				c.getPA().closeAllWindows();
			}
			return true;
		} else if (button == 76058) { // quantity button
			c.getDialogueBuilder().sendEnterAmount(()-> {
				if (c.enterAmount < 1 || c.enterAmount > Integer.MAX_VALUE) {
					c.sendMessage("You must enter a value below 2B.");
				} else {
					int amount = (int) c.enterAmount;
					int realAmount = c.getItems().getItemAmount(sellingItemId);
					if (isInShop && amount > realAmount)
						amount = realAmount;
					c.getMarketOffer().setQuantity(amount);
				}
			}).execute(false);
			return true;
		} else if (button == 76061) { // price button
			c.getDialogueBuilder().sendEnterAmount(()-> {
				if (c.enterAmount < 1) {
					c.sendMessage("You must enter a value above 0.");
				} else {
					c.getMarketOffer().setPrice(c.enterAmount);
				}
			}).execute(false);
			if (isInShop) // if selling item pretty much(in POS you can only sell items, so this interface will show for it)
				c.getPacketSender().sendInputDialogState(PlayerConstants.SET_SELL_PRICE_STATE); // overwrite the other enteramount interface XD
			return true;
		} else if (button == 76048) { // search item button that displays item in box
			if (isInShop)
				return true;
			c.getPacketSender().sendInputDialogState(PlayerConstants.SEARCH_STATE);
			return true;
		}
		return false;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
		this.c.getPacketSender().sendString(Misc.formatNumbersWithCommas(quantity), 19516);
		updateTotalPrice();
	}

	public long getPrice() {
		return price;
	}
	
	public long getTotalPrice() {
		try {
			return Math.multiplyExact(price, quantity);
		} catch (ArithmeticException e) {
			return -1;
		}
	}

	public void setPrice(long price) {
		this.price = price;
		this.c.getPacketSender().sendString(Misc.formatNumbersWithCommas(price), 19519);
		updateTotalPrice();
	}
	
	private void updateTotalPrice() {
		this.c.getPacketSender().sendString(Misc.formatNumbersWithCommas(this.price*this.quantity), 19523);
	}
	
	public void updateAveragePrice() {
		this.c.getPacketSender().sendString(Misc.formatNumbersWithCommas(this.price), 19521);
	}
	
	public void close() {
		isActive = false;
		sellingItemId = 0;
		notNotedId = 0;
		price = 0;
		quantity = 0;
		prevInterface = 0;
		isInShop = false;
		buyOfferItemId = 0;
	}

	public int getBuyOfferItemId() {
		return buyOfferItemId;
	}

	public void setBuyOfferItemId(int buyOfferItemId) {
		this.buyOfferItemId = buyOfferItemId;
		c.getPacketSender().changeItemId(19503, buyOfferItemId);
	}
	
	public int getPrevInterface() {
		return prevInterface;
	}
	
	public void setPrevInterface(int id) {
		prevInterface = id;
	}

	public boolean isLoadingOwnOffers() {
		return loadingOwnOffers;
	}

	public void setLoadingOwnOffers(boolean loadingOffers, boolean updateString) {
		this.loadingOwnOffers = loadingOffers;
		if (updateString)
			c.getPacketSender().sendFrame126b(loadingOffers ? "Loading..." : "", 19707);
	}

	public int getSlotToUse() {
		return slotToUse;
	}

	public void setSlotToUse(int slotToUse) {
		this.slotToUse = slotToUse;
	}

	public boolean isLoadingPublicOffers() {
		return loadingPublicOffers;
	}

	public void setLoadingPublicOffers(boolean loadingPublicOffers, boolean updateString) {
		this.loadingPublicOffers = loadingPublicOffers;
		if (updateString)
			c.getPacketSender().sendFrame126b(loadingPublicOffers ? "Loading..." : "", 19609);
	}
	

	
	private final Map<Integer, BuyOffers> filteredBuyOffers = new HashMap<Integer, BuyOffers>();

	public Map<Integer, BuyOffers> getFilteredBuyOffers() {
		return filteredBuyOffers;
	}

	public TickTimer getTimer() {
		return timer;
	}

	public boolean isSaleInProgress() {
		return saleInProgress;
	}

	public void setSaleInProgress(boolean loadingSale) {
		this.saleInProgress = loadingSale;
	}

}
