package com.soulplay.content.items.search;

import java.util.Collections;

import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

public class MarketSearch {

	public static int MARKET_STRING_ID = 45071;

	public static int MARKET_ITEM_NAME_STRING_ID = 45043;

	public static void clearMarketInterface(Client c) {
		c.getPacketSender().sendConfig(ConfigCodes.CLEAR_INTERFACE, 1);
	}

	public static void fillMarketInterface(final Client c, final String itemName) {
		

		Server.getTaskScheduler().schedule(new Task(true) {

			@Override
			protected void execute() {
				this.stop();
				
				if (c.marketSearchResults.isEmpty()) {
					c.getPacketSender().sendFrame126b("ITEM NOT FOUND", MARKET_ITEM_NAME_STRING_ID);//c.getPacketSender().sendInputDialogState(0);
					c.sendMessage("This item cannot be found in shops.");
//					c.getDialogueBuilder().sendStatement("This item cannot be found in shops.").sendAction(()-> {
//						c.getPacketSender().sendInputDialogState(PlayerConstants.SEARCH_STATE);
//					}).executeIfNotActive();
					return;
				}
				
				MarketSearch.clearMarketInterface(c);
				
				c.getPacketSender().sendFrame126b(itemName, MARKET_ITEM_NAME_STRING_ID);

				int nameInterface = MARKET_STRING_ID;
				int priceInterface = MARKET_STRING_ID + 20;
				int amountInterface = MARKET_STRING_ID + 40;
				int count = 0;
				if (c != null && c.isActive) {
					for (MarketItem item : c.marketSearchResults) {
						if (count == 20) {
							break;
						}

						// update playername
						c.getPacketSender().sendFrame126b(item.getPlayerName(),
								nameInterface);
						// update item price
						c.getPacketSender().sendFrame126b(
								"Price: " + Misc.format(item.getItemPrice()),
								priceInterface);
						// update amount
						c.getPacketSender().sendFrame126b(
								"Amount: " + item.getItemSoldAmount(),
								amountInterface);

						nameInterface++;
						priceInterface++;
						amountInterface++;
						count++;
					}
				}
			}
		});

	}

	public static boolean marketButtonClicked(Client c, int buttonId) {

		if (buttonId == 175204) {
			c.marketSearchResults.clear();
			c.lastMarketSearchIndex = -1;
			MarketSearch.clearMarketInterface(c);
			return true;
		}
		if (buttonId == 175214) { // open searched item player shop button
			if (c.lastMarketSearchIndex < 0) {
				c.sendMessage(
						"<col=ff0000>Please select a shop in the search results first, then click \"Open Shop\"");
				c.getPacketSender().sendInputDialogState(0); // c.getPA().closeAllWindows();
				return true;
			}
			if (c.marketSearchResults.size() < c.lastMarketSearchIndex + 1) {
				c.sendMessage(
						"<col=ff0000>Something went wrong. Please try searching again.");
				c.getPacketSender().sendInputDialogState(0); // c.getPA().closeAllWindows();
				return true;
			}
			MarketItem item = c.marketSearchResults
					.get(c.lastMarketSearchIndex);
			if (item == null) {
				c.getPA().closeAllWindows();
				return true;
			}
			Client c2 = (Client) PlayerHandler.getPlayer(item.getPlayerName());
			if (c2 == null || !c2.isActive) {
				c.sendMessage(
						"The shop you tried to open is no longer active.");
				c.getPA().closeAllWindows();
				return true;
			}

			PlayerOwnedShops.openPlayerShop(c, c2);
			// c.marketSearchResults.clear();
			// c.lastMarketSearchIndex = -1;
			// MarketSearch.clearMarketInterface(c);
			return true;
		}
		if (buttonId == 175218) { // open my own shop button
			if (!c.inMarket()) {
				c.sendMessage("You must be in Market zone to open shop.");
				return true;
			}
			PlayerOwnedShops.openPlayerShop(c, c);
			return true;
		}
		if (buttonId >= 175251 && buttonId <= 175255) { // upper section of shop
														// items
			int searchIndex = buttonId - 175251;
			if (c.marketSearchResults.size() < searchIndex + 1) { // clicked
																	// button
																	// beyond
																	// the
																	// search
				resetConfig(c);
				return true;
			}
			c.lastMarketSearchIndex = searchIndex;
			MarketSearch.toggleConfigOn(c, searchIndex+1);
			return true;
		}
		if (buttonId >= 176000 && buttonId <= 176014) { // lower section of shop
														// items
			int searchIndex = buttonId - 175995;
			if (c.marketSearchResults.size() < searchIndex + 1) { // clicked
																	// button
																	// beyond
																	// the
																	// search
				resetConfig(c);
				return true;
			}
			c.lastMarketSearchIndex = searchIndex;
			MarketSearch.toggleConfigOn(c, searchIndex+1);
			return true;
		}
		return false;
	}

	public static void resetConfig(Client c) {
		c.getPacketSender().sendConfig(800, 0);
	}

	public static void searchMarket(Client c, final String itemName) {

		// if (c.inMarket()) {
		// c.sendMessage("You must be in market to search.");
		// return;
		// }

		if (System.currentTimeMillis() - c.lastWhatDropCommand < 2000) {
			c.sendMessage("Please wait 2 seconds to search again.");
			c.getPacketSender().sendInputDialogState(0);
			return;
		}

		c.getPacketSender().sendFrame126b(itemName, MARKET_ITEM_NAME_STRING_ID);
		c.marketSearchResults.clear();

		c.lastWhatDropCommand = System.currentTimeMillis();

		TaskExecutor.execute(() -> {

			for (Player p : PlayerHandler.players) {
				if (p == null || p.disconnected /* || !p.inMarket() */) {
					continue;
				}
				for (int i = 0; i < p.playerShopItems.length; i++) {
					if (p.playerShopItems[i] > 0) {

						if (itemName.equals(ItemDef.forID(p.playerShopItems[i])
								.getName())) {
							c.marketSearchResults
									.add(new MarketItem(p.getName(), p.getId(),
											p.playerShopItemsPrice[i],
											p.playerShopItemsN[i]));
						}
					}
				}
			}

			MarketSearch.sortList(c);

			MarketSearch.fillMarketInterface(c, itemName);

		});

	}

	public static void sortList(Client c) {
		// Sorting
		Collections.sort(c.marketSearchResults, (m1, m2) -> {
			if (m1.getItemPrice() == m2.getItemPrice()) {
				return 0;
			}
			return m1.getItemPrice() < m2.getItemPrice() ? -1 : 1;
		});
	}

	public static void toggleConfigOn(Client c, int marketSearchIndex) {
		c.getPacketSender().sendConfig(800, marketSearchIndex);
	}

}
