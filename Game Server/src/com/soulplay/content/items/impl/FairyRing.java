package com.soulplay.content.items.impl;

import java.util.HashMap;

import com.soulplay.content.player.TeleportType;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Tiles;

public final class FairyRing {

	public static enum FairyRingLoc {
		MAIN_LOCATION(-1, new Tiles(2412, 4434, 0)),
		PENGUIN_ISLAND(142, new Tiles(2500, 3896, 0)),
		// WITCH(113, new Tiles(2700, 3247, 0)),
		// NARAGI(A, 12, new Tiles(, , )), Naragi Homeworld: Naragi homeworld -
		// The RuneScape Wiki
		// DORGESHKAAN(A, 44, new Tiles(, , )), Dark cave south of Dorgesh-Kaan
		// Dorgesh-Kaan South Dungeon - The RuneScape Wiki
		GOLDENAPPLE(143, new Tiles(2780, 3613, 0)),
		PENGUIN(124, new Tiles(2500, 3896, 0)),
		PICTORIS(134, new Tiles(2319, 3619, 0)),
		FELDIP(132, new Tiles(2571, 2956, 0)),
		JIGJIG(214, new Tiles(2472, 3027, 0)),
		MORYTANIA(442, new Tiles(2472, 3027, 0)),
		ABYSS(123, new Tiles(3059, 4875, 0)),
		MCGRUBOR(411, new Tiles(2644, 3495, 0)),
		// POLYPORE(111, new Tiles(3410, 3324, 0)),
		// KALPHITE(414, new Tiles(3251, 3095, 0)),
		SPARSE(131, new Tiles(2460, 4384, 0)),
		ARDOUGNE(412, new Tiles(2635, 3266, 0)),
		ANCIENT(444, new Tiles(1737, 5342, 0)),
		FISHERREALM(443, new Tiles(2650, 4730, 0)),
		CASTLEWARS(431, new Tiles(2385, 3035, 0)),
		ENCHANTED(434, new Tiles(3041, 4532, 0)),
		MORTMYRE(433, new Tiles(3469, 3431, 0)),
		// TZHAAR(421, new Tiles(4622, 5147, 0)),
		// YUBIUSK(424, new Tiles(2229, 4244, 1)),
		LEGENDS(423, new Tiles(2740, 3350, 0)), // test
		ZANARIS(422, new Tiles(2412, 4434, 0)),
		MISCELLANIA(311, new Tiles(2513, 3884, 0)),
		YANILLE(314, new Tiles(2528, 3127, 0)),
		// BOB(312, new Tiles(, , )), Other realms: ScapeRune (Evil Bob's
		// island): ScapeRune - The RuneScape Wiki
		SINCLAIR(343, new Tiles(2705, 3578, 0)), // portal at 2705 3576
		COSMIC(331, new Tiles(2075, 4848, 0)),
		WANNAI(333, new Tiles(2801, 3003, 0)), // karambwans fishing for small
												// food to use as bait for
												// karambwans
		MUSAPOINT(231, new Tiles(2900, 3111, 0)), // fish for karambwans here
		CANIFIS(332, new Tiles(3447, 3470, 0)),
		DRAYNOR(321, new Tiles(3082, 3206, 0)),
		APEATOLL(323, new Tiles(2735, 2742, 0)),
		ISLAND(322, new Tiles(2682, 3081, 0)),
		// MOSLE(211, new Tiles(3763, 2930, 0)),
		GORAK(213, new Tiles(3038, 5348, 0)),
		WIZARDTOWER(212, new Tiles(3108, 3149, 0)),
		TOL(241, new Tiles(2658, 3230, 0)),
		SINCLAIR2(243, new Tiles(2676, 3587, 0)),
		// GLACORS(234, new Tiles(4183, 5726, 0)),
		// SNOWY(232, new Tiles(2744, 3719, 0)),
		// KHARIDIAN(224, new Tiles(3423, 3016, 0)),
		// POISON(223, new Tiles(2213, 3099, 0));
		// MYREQUE(222, new Tiles(, , )); Dungeons: Myreque hideout under The
		// Hollows: Myreque Hideout - The RuneScape Wiki

		EXCHANGE(233, new Tiles(3129, 3496, 0)); // edgeville

		int combo = -1;

		Tiles tiles = new Tiles(2412, 4434, 0);

		private FairyRingLoc(int combo, Tiles tile) {
			this.combo = combo;
			this.tiles = tile;
		}
	}
	
	private static final HashMap<Integer, FairyRingLoc> map = new HashMap<Integer, FairyRingLoc>();
	
	static {
		for (FairyRingLoc f : FairyRingLoc.values()) {
			map.put(f.combo, f);
		}
	}
	
    public static void load() {
    	/* empty */
    }
    
	private static void fairyTeleport(final Player p, int x, int y,
			final int height) {
		final int endX = x;
		final int endY = y;
		p.getMovement().resetWalkingQueue();
		refresh(p);

		p.getPA().startTeleport(endX, endY, height, TeleportType.FAIRY);
	}

	public static void openFairyRing(Player p) {
		p.getPacketSender().sendEnterAmountInterface();
		p.dialogueAction = 9985;
	}

	public static void refresh(Player player) {
		player.fairyCombo = -1;
		player.dialogueAction = 0;
	}

	public static void ringTele(Player player, int combo) {
		// final int a = 1;
		// final int i = 1;
		// final int p = 1;
		// final int d = 2;
		// final int l = 2;
		// final int s = 2;
		// final int c = 3;
		// final int k = 3;
		// final int r = 3;
		// final int b = 4;
		// final int j = 4;
		// final int q = 4;
		if (player.isTeleBlocked()) {
			player.sendMessage(
					"You cannot use the fairyring when teleblocked!");
			return;
		}

		if (combo == 311 && player.getDonatedTotalAmount() < 1) { // dzone area,
																	// miscellania
			player.sendMessage("This is a zone for Donator+ rank.");
			return;
		}

		/**
		 * START OF SEQUENCES
		 */
		
		FairyRingLoc f = map.get(combo);

		if (f != null) {
			player.getPA().closeAllWindows();
			fairyTeleport(player, f.tiles.getX(), f.tiles.getY() - 1,
					f.tiles.getH());
			refresh(player);
			return;
		}

		player.sendMessage(
				"The combination must be a 3 digit number from 1 to 4! Example: 233");
	}

	public static void warningInterface(Player player) {
		player.getPA().closeAllWindows();
		fairyTeleport(player, 2735, 5221, 0);
	}

}
