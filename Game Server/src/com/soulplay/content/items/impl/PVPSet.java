package com.soulplay.content.items.impl;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.map.travel.zone.addon.EdgevilleSpawnSkillZone;
import com.soulplay.util.Misc;

public enum PVPSet {

	MELEE(73106, "melee", PVPSet::setMelee, 20, 1, 2_500_000, 99, 99),
	HYBRID(73109, "hybrid", PVPSet::setHybrid, 22, 1, 2_500_000, 99, 99), //too much runes make this set go too high
	ZERKER(73112, "zerker", PVPSet::setZerker, 8, 1, 2_500_000, 45, 52),
	PURE_NH(73115, "purenh", PVPSet::setPure, 10, 1, 2_000_000, 1, 52), //too much runes make this set go too high
	PURE(73118, "pure", PVPSet::setMeleePure, 8, 1, 1_000_000, 1, 52);
	
	public static final PVPSet[] values = values();

	@FunctionalInterface
	public interface ClientAcceptor {

		void accept(Player c);
	}

	public static void pvpSet(Player c, PVPSet pvpSet) {
		if (System.currentTimeMillis() - c.anyDelay2 < 3000) {
			c.sendMessage("You need to wait 3 seconds before spawning a new setup.");
			return;
		}

		if (!pvpSet.levelCheck(c)) {
			c.sendMessage("Your combat levels needs to be equal or higher than this setup.");
			return;
		}

		c.getDialogueBuilder().sendOption("Purchase " + pvpSet.command + " setup for", 
		"<col=ff>" + Misc.formatNumbersWithCommas(pvpSet.gpPrice) + "</col> Coins.", () -> {
			if (!c.getItems().takeItem(995, pvpSet.gpPrice, true)) {
				c.sendMessage("You don't have enough coins to purchase this set.");
				return;
			}

			buySet(pvpSet, c);
		},  "<col=ff>" + Misc.formatNumbersWithCommas(pvpSet.pkPrice) + "</col> Pk Points ", () -> {
			if (c.pkp < pvpSet.pkPrice) {
				c.sendMessage("You don't have enough pk points to purchase this set.");
				return;
			}

			c.pkp -= pvpSet.pkPrice;
			buySet(pvpSet, c);
		},  "<col=ff>" + Misc.formatNumbersWithCommas(pvpSet.votePrice) + "</col> Vote tickets", () -> {
			if (!c.getItems().takeItem(2996, pvpSet.votePrice, true)) {
				c.sendMessage("You don't have enough vote tickets to purchase this set.");
				return;
			}

			buySet(pvpSet, c);
		}, "Nevermind", () -> {

		}).execute();
	}

	private static void buySet(PVPSet pvpSet, Player c) {
		c.isBanking = true;
		c.getItems().bankContainer(c.playerItems, c.playerItemsN, -1,
				c.getItems()::deleteItemInOneSlot, null);
		c.refreshBank = true;
		c.getItems().bankEquipment();
		pvpSet.acceptor.accept(c);
		c.anyDelay2 = System.currentTimeMillis();
		c.getCombat().resetPrayers();
		c.sendMessage(
				"Your previously inventory and equipments have been banked.");
		c.isBanking = false;
		c.getPA().closeAllWindows();
		pvpSet.applyLevels(c);
		EdgevilleSpawnSkillZone.assignZone(c);
	}

	public static void setHybrid(Player c) {
		c.getItems().setEquipment(10828, 1, PlayerConstants.playerHat);
		c.getItems().setEquipment(1712, 1, PlayerConstants.playerAmulet);
		c.getItems().setEquipment(2412, 1, PlayerConstants.playerCape);
		c.getItems().setEquipment(4091, 1, PlayerConstants.playerChest);
		c.getItems().setEquipment(4675, 1, PlayerConstants.playerWeapon);
		c.getItems().setEquipment(3842, 1, PlayerConstants.playerShield);
		c.getItems().setEquipment(4093, 1, PlayerConstants.playerLegs);
		c.getItems().setEquipment(3105, 1, PlayerConstants.playerFeet);
		c.getItems().setEquipment(7461, 1, PlayerConstants.playerHands);
		c.getItems().addItem(5698, 1);
		c.getItems().addItem(4587, 1);
		c.getItems().addItem(1127, 1);
		c.getItems().addItem(1079, 1);
		c.getItems().addItem(1201, 1);
		c.getItems().addItem(2503, 1);
		c.getItems().addItem(6685, 2);
		c.getItems().addItem(3024, 2);
		c.getItems().addItem(2440, 1);
		c.getItems().addItem(2436, 1);
		c.getItems().addItem(391, 12);
		c.getItems().addItem(565, 200);
		c.getItems().addItem(555, 600);
		c.getItems().addItem(560, 400);
		c.getItems().addItem(8013, 10);
		c.getPA().changeSpellBookToAncient(); // ancient spell book
	}

	public static void setMelee(Player c) {
		c.getItems().setEquipment(10828, 1, PlayerConstants.playerHat);
		c.getItems().setEquipment(1712, 1, PlayerConstants.playerAmulet);
		c.getItems().setEquipment(6568, 1, PlayerConstants.playerCape);
		c.getItems().setEquipment(1127, 1, PlayerConstants.playerChest);
		c.getItems().setEquipment(4587, 1, PlayerConstants.playerWeapon);
		c.getItems().setEquipment(1201, 1, PlayerConstants.playerShield);
		c.getItems().setEquipment(1079, 1, PlayerConstants.playerLegs);
		c.getItems().setEquipment(3105, 1, PlayerConstants.playerFeet);
		c.getItems().setEquipment(7461, 1, PlayerConstants.playerHands); // 
		c.getItems().addItem(5698, 1);
		c.getItems().addItem(6685, 2);
		c.getItems().addItem(3024, 2);
		c.getItems().addItem(2440, 1);
		c.getItems().addItem(2436, 1);
		c.getItems().addItem(391, 17); //food
		c.getItems().addItem(9075, 400); // veng set
		c.getItems().addItem(560, 200); // veng set
		c.getItems().addItem(557, 1000); // veng set
		c.getPA().changeSpellBookToLunar(); // veng spell book
		c.getItems().addItem(8013, 10);
	}

	public static void setPure(Player c) {
		c.getItems().setEquipment(6109, 1, PlayerConstants.playerHat);
		c.getItems().setEquipment(1712, 1, PlayerConstants.playerAmulet);
		c.getItems().setEquipment(2414, 1, PlayerConstants.playerCape);
		c.getItems().setEquipment(6107, 1, PlayerConstants.playerChest);
		c.getItems().setEquipment(4675, 1, PlayerConstants.playerWeapon);
		c.getItems().setEquipment(3841, 1, PlayerConstants.playerShield);
		c.getItems().setEquipment(6108, 1, PlayerConstants.playerLegs);
		c.getItems().setEquipment(3105, 1, PlayerConstants.playerFeet);
		c.getItems().setEquipment(7458, 1, PlayerConstants.playerHands);
		c.getItems().setEquipment(9244, 30, PlayerConstants.playerArrows);
		c.getItems().addItem(5698, 1);
		c.getItems().addItem(10499, 1);
		c.getItems().addItem(9185, 1);
		c.getItems().addItem(2497, 1);
		c.getItems().addItem(2444, 1);
		c.getItems().addItem(3040, 1);
		c.getItems().addItem(2440, 1);
		c.getItems().addItem(6685, 1);
		c.getItems().addItem(3024, 1);
		c.getItems().addItem(391, 15);
		c.getItems().addItem(565, 200);
		c.getItems().addItem(555, 600);
		c.getItems().addItem(560, 400);
		c.getItems().addItem(8013, 10);
		c.getPA().changeSpellBookToAncient(); // ancient spell book
	}
	
	public static void setMeleePure(Player c) {
		c.getItems().setEquipment(2930, 1, PlayerConstants.playerHat);
		c.getItems().setEquipment(1712, 1, PlayerConstants.playerAmulet);
		c.getItems().setEquipment(2414, 1, PlayerConstants.playerCape);
		c.getItems().setEquipment(2926, 1, PlayerConstants.playerChest);
		c.getItems().setEquipment(4587, 1, PlayerConstants.playerWeapon);
		c.getItems().setEquipment(3841, 1, PlayerConstants.playerShield);
		c.getItems().setEquipment(2497, 1, PlayerConstants.playerLegs);
		c.getItems().setEquipment(3105, 1, PlayerConstants.playerFeet);
		c.getItems().setEquipment(7458, 1, PlayerConstants.playerHands);
		c.getItems().addItem(2440, 1);
		c.getItems().addItem(2436, 1);
		c.getItems().addItem(6685, 1);
		c.getItems().addItem(3024, 1);
		c.getItems().addItem(391, 18);
		c.getItems().addItem(5698, 1);
		c.getItems().addItem(391, 4);
		c.getItems().addItem(8013, 10);
	}

	public static void setZerker(Player c) {
		c.getItems().setEquipment(3751, 1, PlayerConstants.playerHat);
		c.getItems().setEquipment(1712, 1, PlayerConstants.playerAmulet);
		c.getItems().setEquipment(6568, 1, PlayerConstants.playerCape);
		c.getItems().setEquipment(1127, 1, PlayerConstants.playerChest);
		c.getItems().setEquipment(4587, 1, PlayerConstants.playerWeapon);
		c.getItems().setEquipment(1201, 1, PlayerConstants.playerShield);
		c.getItems().setEquipment(1079, 1, PlayerConstants.playerLegs);
		c.getItems().setEquipment(4131, 1, PlayerConstants.playerFeet);
		c.getItems().setEquipment(7461, 1, PlayerConstants.playerHands);
		c.getItems().addItem(5698, 1);
		c.getItems().addItem(6685, 1);
		c.getItems().addItem(2436, 1);
		c.getItems().addItem(2440, 1);
		c.getItems().addItem(3024, 1);
		c.getItems().addItem(10925, 2);
		c.getItems().addItem(391, 16);
		c.getItems().addItem(3144, 1);
		c.getItems().addItem(9075, 400); // veng set
		c.getItems().addItem(560, 200); // veng set
		c.getItems().addItem(557, 1000); // veng set
		c.getPA().changeSpellBookToLunar(); // veng spell book
		c.getItems().addItem(8013, 10);
	}

	public final int buttonId;

	public final String command;

	public final ClientAcceptor acceptor;
	
	public final int pkPrice, votePrice, gpPrice, def, prayer;

	private PVPSet(int buttonID, String command, ClientAcceptor acceptor, int pkPrice, int votePrice, int gpPrice, int def, int prayer) {
		this.buttonId = buttonID;
		this.command = command;
		this.acceptor = acceptor;
		this.pkPrice = pkPrice;
		this.votePrice = votePrice;
		this.gpPrice = gpPrice;
		this.def = def;
		this.prayer = prayer;
	}

	public boolean levelCheck(Player player) {
		if (player.getSkills().getStaticLevel(Skills.ATTACK) >= 99 && player.getSkills().getStaticLevel(Skills.STRENGTH) >= 99 &&
			player.getSkills().getStaticLevel(Skills.DEFENSE) >= def && player.getSkills().getStaticLevel(Skills.RANGED) >= 99 &&
			player.getSkills().getStaticLevel(Skills.PRAYER) >= prayer && player.getSkills().getStaticLevel(Skills.MAGIC) >= 99 &&
			player.getSkills().getStaticLevel(Skills.HITPOINTS) >= 99) {
			return true;
		}

		return false;
	}

	public void applyLevels(Player player) {
		player.setSpawnedSkills(new Skills(player));
		player.getSpawnedSkills().setStaticLevel(Skills.ATTACK, 99);
		player.getSpawnedSkills().setStaticLevel(Skills.STRENGTH, 99);
		player.getSpawnedSkills().setStaticLevel(Skills.DEFENSE, def);
		player.getSpawnedSkills().setStaticLevel(Skills.RANGED, 99);
		player.getSpawnedSkills().setStaticLevel(Skills.PRAYER, prayer);
		player.getSpawnedSkills().setStaticLevel(Skills.MAGIC, 99);
		player.getSpawnedSkills().setStaticLevel(Skills.HITPOINTS, 99);
		player.calcCombat();
	}

}
