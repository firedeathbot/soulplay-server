package com.soulplay.content.items.impl;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Animation;

public enum DuellistsCap {
	TIER_1(20795, "Tier 1", 0, 2719),
	TIER_2(20796, "Tier 2", 10, 2720),
	TIER_3(20797, "Tier 3", 100, 2721),
	TIER_4(20798, "Tier 4", 500, 3092),
	TIER_5(20799, "Tier 5", 2000, 3131),
	TIER_6(20800, "Tier 6", 5000, 3224);

	public static final int OPTIONS = 1;

	public static final int CHANGE_LOOK = 2;

	public static DuellistsCap getCapForID(int itemID) {
		switch (itemID) {
			case 20795:
				return TIER_1;
			case 20796:
				return TIER_2;
			case 20797:
				return TIER_3;
			case 20798:
				return TIER_4;
			case 20799:
				return TIER_5;
			case 20800:
				return TIER_6;
			default:
				return null;
		}
	}

	public static int getRemaningKillsForNextTier(Client c) {
		if (c.getDuelKC() >= TIER_6.killcount) {
			return 0;
		} else if (c.getDuelKC() >= TIER_5.killcount) {
			return TIER_6.killcount - c.getDuelKC();
		} else if (c.getDuelKC() >= TIER_4.killcount) {
			return TIER_5.killcount - c.getDuelKC();
		} else if (c.getDuelKC() >= TIER_3.killcount) {
			return TIER_4.killcount - c.getDuelKC();
		} else if (c.getDuelKC() >= TIER_2.killcount) {
			return TIER_3.killcount - c.getDuelKC();
		} else {
			return TIER_2.killcount - c.getDuelKC();
		}
	}

	private static void handleChangeLookDialogue(Client c, DuellistsCap cap) {
		if (c.getDuelKC() >= TIER_6.killcount) {
			if (cap == TIER_6) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_6.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
			    }, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_6.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_6.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_6.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_6.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}).execute();
			} else if (cap == TIER_5) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
			    }, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, TIER_6.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_6.id, 1);
				}).execute();
			} else if (cap == TIER_4) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
			    }, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, TIER_6.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_6.id, 1);
				}).execute();
			} else if (cap == TIER_3) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
			    }, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, TIER_6.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_6.id, 1);
				}).execute();
			} else if (cap == TIER_2) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
			    }, TIER_6.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_6.id, 1);
				}).execute();
			} else {
				c.getDialogueBuilder().sendOption(TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, TIER_6.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_6.id, 1);
				}).execute();
			}
		} else if (c.getDuelKC() >= TIER_5.killcount) {
			if (cap == TIER_5) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_5.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else if (cap == TIER_4) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else if (cap == TIER_3) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else if (cap == TIER_2) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else {
				c.getDialogueBuilder().sendOption(TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_5.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_5.id, 1);
				}, "Cancel", ()-> {}).execute();
			}
		} else if (c.getDuelKC() >= TIER_4.killcount) {
			if (cap == TIER_4) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_4.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else if (cap == TIER_3) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else if (cap == TIER_2) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else {
				c.getDialogueBuilder().sendOption(TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, TIER_4.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_4.id, 1);
				}, "Cancel", ()-> {}).execute();
			}
		} else if (c.getDuelKC() >= TIER_3.killcount) {
			if (cap == TIER_3) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_3.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else if (cap == TIER_2) {
				c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, "Cancel", ()-> {}).execute();
			} else {
				c.getDialogueBuilder().sendOption(TIER_2.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
				}, TIER_3.name, ()-> {
					c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
					c.getItems().addItem(DuellistsCap.TIER_3.id, 1);
				}, "Cancel", ()-> {}).execute();
			}
		} else if (c.getDuelKC() >= TIER_2.killcount) {
			if (cap == TIER_2) {
			    c.getDialogueBuilder().sendOption(TIER_1.name, ()-> {
				c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_2.id, 1);
				c.getItems().addItem(DuellistsCap.TIER_1.id, 1);
			}, "Cancel", ()-> {}).execute();
		    } else {
			    c.getDialogueBuilder().sendOption(TIER_2.name, ()-> {
				    c.getItems().deleteItemInOneSlot(DuellistsCap.TIER_1.id, 1);
				    c.getItems().addItem(DuellistsCap.TIER_2.id, 1);
			    }, "Cancel", ()-> {}).execute();
		    }
	    } else {
	 	    c.getDialogueBuilder().sendStatement("You need an additional " + (TIER_2.killcount - c.getDuelKC()) + " duel wins to unlock this feature.").execute();
	    }
	}

	public static void handleDialogues(Client c, int type, DuellistsCap cap) {
		if (type == OPTIONS) {
			handleOptionsDialogue(c, cap);
		} else if (type == CHANGE_LOOK) {
			handleChangeLookDialogue(c, cap);
		}
	}

	private static void handleOptionsDialogue(Client c, DuellistsCap cap) {
		c.getDialogueBuilder().sendOption("Show stats", ()-> {
			c.getDialogueBuilder().sendStatement("Duel Wins: " + c.getDuelKC(), "Duel Losses: " + c.getDuelDC(),
						"Current Streak: " + c.getDuelStreak(),
						"Wins Until Next Tier: " + DuellistsCap.getRemaningKillsForNextTier(c));
		}, "Hat Emote", ()-> {
			if (cap == TIER_6) {
				c.startAnimation(new Animation(510));//broken animation should be using something else TEMP 510
		    } else if (cap == TIER_5) {
				c.startAnimation(new Animation(511));//broken animation should be 512 TEMP 510
			} else if (cap == TIER_4) {
				c.startAnimation(new Animation(511));
			} else if (cap == TIER_3) {
				c.startAnimation(new Animation(509));
			} else if (cap == TIER_2) {
				c.startAnimation(new Animation(508));
			} else if (cap == TIER_1) {
				c.startAnimation(new Animation(507));
			}
		}).execute();
	}

	public final int id;

	public final String name;

	public final int killcount;

	public final int npc;

	DuellistsCap(int id, String name, int killcount, int npc) {
		this.id = id;
		this.name = name;
		this.killcount = killcount;
		this.npc = npc;
	}
}
