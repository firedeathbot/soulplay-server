
package com.soulplay.content.items.impl;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.items.CompCape;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.entity.UpdateFlag;

public enum CompletionistCape {
	RED(14007, "red", 54178, PlayerDifficulty.PRESTIGE_ONE),
	GREEN(14016, "green", 54179, PlayerDifficulty.PRESTIGE_ONE),
	BLUE(14017, "blue", 54180, PlayerDifficulty.PRESTIGE_ONE),
	PURPLE(14008, "purple", 54181, PlayerDifficulty.PRESTIGE_ONE),
	YELLOW(14009, "yellow", 54182, PlayerDifficulty.PRESTIGE_ONE),
	BROWN(14023, "brown", 54183, PlayerDifficulty.PRESTIGE_ONE),
	WHITE(14001, "white", 54184, PlayerDifficulty.PRESTIGE_ONE),
	CYAN(14006, "cyan", 54185, PlayerDifficulty.PRESTIGE_ONE),
	RED_WHITE(14015, "red-white", 54186, PlayerDifficulty.PRESTIGE_TWO),
	GREEN_WHITE(14020, "green-white", 54187, PlayerDifficulty.PRESTIGE_TWO),
	BLUE_WHITE(14012, "blue-white", 54188, PlayerDifficulty.PRESTIGE_TWO),
	PURPLE_WHITE(14022, "purple-white", 54189, PlayerDifficulty.PRESTIGE_TWO),
	YELLOW_WHITE(14011, "yellow-white", 54190, PlayerDifficulty.PRESTIGE_TWO),
	BROWN_WHITE(14024, "brown-white", 54191, PlayerDifficulty.PRESTIGE_TWO),
	BLACK_WHITE(14004, "black-white", 54192, PlayerDifficulty.PRESTIGE_TWO),
	ORANGE(14010, "orange", 54193, PlayerDifficulty.PRESTIGE_TWO),
	RED_BLACK(14014, "red-black", 54194, PlayerDifficulty.PRESTIGE_THREE),
	GREEN_BLACK(14019, "green-black", 54195, PlayerDifficulty.PRESTIGE_THREE),
	BLUE_BLACK(14013, "blue-black", 54196, PlayerDifficulty.PRESTIGE_THREE),
	PURPLE_BLACK(14021, "purple-black", 54197, PlayerDifficulty.PRESTIGE_THREE),
	YELLOW_BLACK(14005, "yellow-black", 54198, PlayerDifficulty.PRESTIGE_THREE),
	GREY(14002, "grey", 54199, PlayerDifficulty.PRESTIGE_THREE),
	BLACK(14003, "black", 54200, PlayerDifficulty.PRESTIGE_THREE),
	PINK(14018, "pink", 54201, PlayerDifficulty.PRESTIGE_THREE);

	public final static int OPTION_CUSTOMISE = 0;

	public final static int OPTION_FEATURES = 1;

	public final static int NORMAL_CAPE = 20769;
	
	private static Map<Integer, CompletionistCape> mapForButton = new HashMap<>();
	private static Map<Integer, CompletionistCape> mapForItemId = new HashMap<>();
	
	static {
		for (CompletionistCape cape : CompletionistCape.values()) {
			mapForButton.put(cape.buttonId, cape);
			mapForItemId.put(cape.id, cape);
		}
	}
	
    public static void load() {
    	/* empty */
    }

	private static void handleCompCapeButtonClick(Client c,
			CompletionistCape cape) {
		if ((c.difficulty < cape.difficulty)
				&& !c.isIronMan()) {
			c.sendMessage(
					"You can't select this colour yet as you need a higher mode.");
			return;
		} else {
			if (c.getItems().playerHasItem(995, 239000)) {
				if (c.capeClicked > 0) {
					if (c.getItems().deleteItem2(c.capeClicked, 1)) {
						c.getItems().addItem(cape.id, 1);
					}
				}
			} else {
				c.sendMessage(
						"You need 239.000 coins to pay for the painting.");
			}
		}
	}

	private static void handleCompCapeItemClick(Client c, int capeId,
			int option) {
		if (option == OPTION_CUSTOMISE) {
			c.sendMessage("<col=ff>Switching Cape color costs 239k");
			c.getPacketSender().showInterface(14000);
			c.capeClicked = capeId;
		} else {
			c.getDH().sendStatement("Coming soon!");
			c.nextChat = 0;
			c.dialogueAction = 0;
		}
	}

	public static boolean handleCompletionistCapeButton(Client c,
			int buttonId) {
		CompletionistCape cape = mapForButton.get(buttonId);
		if (cape != null) {
			handleCompCapeButtonClick(c, cape);
			return true;
		}
		return false;
	}
	
	public static void openCustomizeInterface(Client c) {
		if (c.getCompCape() == null) {
			c.setCompCape(CompCape.create());
			c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		}
		c.getPacketSender().sendRecolorItem(25316, 20769, c.getCompCape().getColors());
		c.getPacketSender().showInterface(25300);
	}

	public static boolean handleCompletionistsCapeItemClick(Client c,
			int capeId, int option) {
		if (capeId == 20769) {
			if (option == OPTION_CUSTOMISE) {
				openCustomizeInterface(c);
			} else if (option == OPTION_FEATURES) {
				if (Variables.SPIRIT_CAPE_ADDED_TO_COMP.getValue(c) == 1) {
					c.getDialogueBuilder().sendStatement("You currently have Spirit Cape effect on your completionist cape.").execute();
				} else {
					c.getDialogueBuilder().sendStatement("You currently have no features on your completionist cape.").execute();
				}
			}
			return true;
		}
//		for (CompletionistCape cape : CompletionistCape.values()) {
//			if (cape.id == capeId) {
//				handleCompCapeItemClick(c, cape.id, option);
//				return true;
//			}
//		}
		return false;
	}

	public static boolean isCompletionistCape(int itemId) {
		if (itemId == NORMAL_CAPE) {
			return true;
		}
		return mapForItemId.containsKey(itemId);
		
	}

	int id;

	String name;

	int buttonId;

	int difficulty;

	private CompletionistCape(int id, String name, int buttonId,
			int difficulty) {
		this.id = id;
		this.name = name;
		this.buttonId = buttonId;
		this.difficulty = difficulty;
	}
	
	public int getId() {
		return id;
	}
}
