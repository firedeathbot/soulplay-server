package com.soulplay.content.items.impl;

import java.util.HashSet;
import java.util.Set;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;

public enum MagicStaff {

	FIRE(1387, 1393, 1401, 3053, 3054, 11736, 11738),
	WATER(1383, 1395, 1403, 6562, 6563, 11736, 11738, 30089),
	AIR(1381, 1397, 1405, 21777),
	EARTH(1385, 1399, 1407, 3053, 3054, 6563, 6562),
	NATURE(18341),
	LAW(18342),
	FIRE_DUNG(16167, 16168, 17005, 17007),
	WATER_DUNG(16163, 16164, 16997, 16999),
	AIR_DUNG(16169, 16170, 17009, 17011),
	EARTH_DUNG(16165, 16166, 17001, 17003);

	private final Set<Integer> staves = new HashSet<>();

	private MagicStaff(int... staves) {
		for (int staff : staves) {
			this.staves.add(staff);
		}
	}

	public Set<Integer> getStaves() {
		return staves;
	}

	public static boolean wearingStaff(Player c, MagicStaff type) {
		int weapon = c.playerEquipment[PlayerConstants.playerWeapon];
		if (weapon == -1) {
			return false;
		}

		return type.getStaves().contains(weapon);
	}
}