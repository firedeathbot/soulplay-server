package com.soulplay.content.items.impl;

import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;

public class DegradableItem {

	// julius
	public static enum Degradable {
		ZARYTE_BOW_BROKEN(20171, 20174, 10000000),
		ZARYTE_BOW_DEGRADED(20173, 20174, 5000000);

		public int originalItem;

		public int degradedItem;

		public int brokenItem;

		public int repairCost;

		private Degradable(int original, int broken, int cost) {
			originalItem = original;
			brokenItem = broken;
			repairCost = cost;
		}
	}

	public static void repairItem(Client c, Degradable degradable) {
		if (degradable == null) {
			return;
		}
		if (c.getItems().playerHasItem(degradable.brokenItem)) {
			if (c.getItems().playerHasItem(995, degradable.repairCost)) {
				c.getItems().deleteItemInOneSlot(995, degradable.repairCost);
				c.getItems().deleteItemInOneSlot(degradable.brokenItem, 1);
				c.getItems().addItem(degradable.originalItem, 1);
				c.sendMessage("Bob repairs your "
						+ ItemConstants.getItemName(degradable.originalItem)
						+ ".");
				c.degradedItem = null;
			} else {
				c.sendMessage("You don't have enough money to afford this.");
			}
		}
	}
}
