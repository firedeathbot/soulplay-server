package com.soulplay.content.items.impl;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.util.Misc;

/**
 * Flaxpicking. OUTDATED
 *
 * @author Derive
 */
public class Flax {

	// public static ArrayList<int[]> flaxRemoved = new ArrayList<int[]>();

	public static void pickFlax(final Client c, final int x, final int y) {
		if (c.getItems().freeSlots() != 0) {
			if (c.runningTask) {
				return;
			}
			c.getItems().addItem(1779, 1);
			c.startAnimation(827);
			c.getAchievement().pickFlax();
			c.sendMessage("You pick some flax.");
			if (Misc.random(3) == 1) {
				// flaxRemoved.add(new int[] { x, y });
				// Server.objectManager.removeObject(x, y);
				ObjectManager.addObject(new RSObject(-1, x, y, c.heightLevel, 0, 10, 2646, 100));
			}
			c.runningTask = true;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					c.runningTask = true;

					container.stop();
				}

				@Override
				public void stop() {
					c.runningTask = false;
				}
			}, 4);
		} else {
			c.sendMessage("Not enough space in your inventory.");
			return;
		}

	}
}
