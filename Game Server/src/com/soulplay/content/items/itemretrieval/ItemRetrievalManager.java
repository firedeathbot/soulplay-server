package com.soulplay.content.items.itemretrieval;

import com.soulplay.content.player.DonatorRank;
import com.soulplay.game.model.item.ComparableItem;
import com.soulplay.game.model.item.ContainerType;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

public class ItemRetrievalManager {

	public static final int ID = 54800;
	public static final int FOOTER_TEXT_ID = 54811;
	public static final int UNLOCK_LAYER_ID = 54812;
	public static final int DISCARD_LAYER_ID = 54813;
	public static final int INVENTORY_CAPACITY = 80;
	public static final int INVENTORY_ID = 54803;

	public static void openInterface(Player player) {
		ItemRetrievalInstance instance = player.itemRetrieval;
		if (instance != null && instance.getInventory().isEmpty()) {
			player.itemRetrieval = null;
		}

		update(player);
		player.getPacketSender().showInterface(ID);
	}

	public static void update(Player player) {
		ItemRetrievalInstance instance = player.itemRetrieval;
		if (instance == null) {
			player.getPacketSender().resetInvenory(INVENTORY_ID);
			player.getPacketSender().sendString("Stack count: <col=ffffff>0</col><br>Guide value: <col=ffffff>0</col> (approximately)", FOOTER_TEXT_ID);
			player.getPacketSender().setVisibility(UNLOCK_LAYER_ID, true);
			player.getPacketSender().setVisibility(DISCARD_LAYER_ID, true);
			player.getPacketSender().sendConfig(ConfigCodes.ITEM_RETRIEVAL_UNLOCKED, 0);
			return;
		}

		Inventory inventory = instance.getInventory();
		player.getPacketSender().sendInventory(INVENTORY_ID, inventory.getItems(), 1);
		player.getPacketSender().sendString("Stack count: <col=ffffff>" + inventory.itemCount() + "</col><br>Guide value: <col=ffffff>" + inventory.getWealthString() + "</col> (approximately)", FOOTER_TEXT_ID);
		player.getPacketSender().setVisibility(UNLOCK_LAYER_ID, false);
		player.getPacketSender().setVisibility(DISCARD_LAYER_ID, false);
		player.getPacketSender().sendConfig(ConfigCodes.ITEM_RETRIEVAL_UNLOCKED, instance.isUnlocked() ? 1 : 0); 
	}

	public static boolean inventoryClick(Player player, int slot, boolean update) {
		ItemRetrievalInstance instance = player.itemRetrieval;
		if (instance == null) {
			if (update) {
				update(player);
			}
			return false;
		}

		if (!instance.isUnlocked()) {
			player.sendMessage("You must pay the price before taking items out.");
			return false;
		}

		Inventory inventory = instance.getInventory();
		if (inventory == null) {
			if (update) {
				update(player);
			}
			return false;
		}

		Item item = inventory.get(slot);
		if (item == null) {
			if (update) {
				update(player);
			}
			return false;
		}

		if (!player.getItems().hasInventorySpaceFor(item)) {
			player.sendMessage("Not enough space in your inventory.");
			return false;
		}

		if (player.getItems().addItem(item.getId(), item.getAmount(), true)) {
			inventory.remove(item);
			if (inventory.isEmpty()) {
				player.itemRetrieval = null;
			}

			if (update) {
				update(player);
			}
		}

		return true;
	}

	public static boolean buttonClick(Player player, int buttonId) {
		switch (buttonId) {
			case 54809: {
				ItemRetrievalInstance instance = player.itemRetrieval;
				if (instance == null) {
					return true;
				}

				player.getDialogueBuilder().sendOption("Are you sure you want to discard all items?", "Yes.", () -> {
					player.itemRetrieval = null;
					openInterface(player);
				}, "No.", () -> { openInterface(player); }).execute();
				return true;
			}
			case 54807: {
				ItemRetrievalInstance instance = player.itemRetrieval;
				if (instance == null) {
					return true;
				}

				if (instance.isUnlocked()) {
					//Take all if unlocked.
					Inventory inventory = instance.getInventory();
					if (inventory == null) {
						return true;
					}

					for (int i = 0, length = inventory.capacity(); i < length; i++) {
						Item item = inventory.get(i);
						if (item == null) {
							continue;
						}

						if (!inventoryClick(player, i, false)) {
							break;
						}
					}

					update(player);
				} else {
					int price = instance.getPrice();
					player.getDialogueBuilder().sendOption("Pay " + Misc.format(price) + " x Coins?", "Yes, pay " + Misc.format(price) + " x Coins.", () -> {
						if (player.itemRetrieval != null) {
							if (player.getItems().takeCoins(price)) {
								instance.setUnlocked(true);
							} else {
								player.sendMessage("You don't have enough coins.");
							}
						}
	
						openInterface(player);
					}, "No, don't pay.", () -> { openInterface(player); }).execute();
				}
				return true;
			}
		}

		return false;
	}

	public static boolean hasItems(Player player, boolean dialog) {
		if (player.itemRetrieval != null) {
			if (dialog) {
				player.getDialogueBuilder().sendNpcChat(6390, "Talk to me in Edgeville to claim your lost items", " from me before you can enter.").executeIfNotActive();
			}
			return true;
		}

		return false;
	}
	
	public static boolean transfer(Player player) {
		if (hasItems(player, false)) {
			return false;
		}

		Inventory inventory = new Inventory(INVENTORY_CAPACITY, ContainerType.ALWAYS_STACK);
		for (int e = 0, length = player.playerEquipment.length; e < length; e++) {
    		int itemId = player.playerEquipment[e];
    		if (itemId < 0) {
    			continue;
    		}

    		int itemAmount = player.playerEquipmentN[e];
    		if (player.itemsToKeep.contains(new ComparableItem(itemId, itemAmount))) {
    			continue;
    		}

    		inventory.add(new Item(itemId, itemAmount));
		}
		
		for (int i = 0, length = player.playerItems.length; i < length; i++) {
    		int itemId = player.playerItems[i] - 1;
    		if (itemId < 0) {
    			continue;
    		}

    		int itemAmount = player.playerItemsN[i];
    		if (player.itemsToKeep.contains(new ComparableItem(itemId, itemAmount))) {
    			continue;
    		}

    		inventory.add(new Item(itemId, itemAmount));
		}

		if (inventory.isEmpty()) {
			return true;
		}

		boolean unlocked = false;
		int price =  DonatorRank.check(player).getItemReclaimPrice();
		if (price <= 0) {
			unlocked = true;
		}

		player.itemRetrieval = new ItemRetrievalInstance(inventory, unlocked, price);
		return true;
	}

}
