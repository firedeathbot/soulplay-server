package com.soulplay.content.items.itemretrieval;

import com.soulplay.game.model.item.Inventory;

public class ItemRetrievalInstance {

	private final Inventory inventory;
	private boolean unlocked;
	private final int price;

	public ItemRetrievalInstance(Inventory inventory, boolean unlocked, int price) {
		this.inventory = inventory;
		this.unlocked = unlocked;
		this.price = price;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setUnlocked(boolean unlocked) {
		this.unlocked = unlocked;
	}

	public boolean isUnlocked() {
		return unlocked;
	}

	public int getPrice() {
		return price;
	}

}
