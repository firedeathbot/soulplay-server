package com.soulplay.content.items.pos;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.minigames.duel.TradeAndDuel;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.trading.TradingPostOrder;
import com.soulplay.content.player.trading.TradingPostType;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.pos.MarketSql;

public class BuyOffers {
	
	private static final Map<Integer, BuyOffers> buyOffers = new HashMap<Integer, BuyOffers>();

	public static Map<Integer, BuyOffers> getPublicBuyOffers() {
		return buyOffers;
	}
	
	public static TickTimer refreshTimer = new TickTimer();
	
	public static void loadPublicOffers(final Client c, final int itemId) {
		if (c.getMarketOffer().isLoadingPublicOffers()) {
			c.sendMessage("Already loading public offers.");
			return;
		}
		if ((itemId <= 0 && !refreshTimer.checkThenStart(10)) || (itemId > 0 && !c.getMarketOffer().getTimer().checkThenStart(10))) {
			fillPublicOffers(c, itemId);
			return;
		}
		c.getMarketOffer().setLoadingPublicOffers(true, true);
		clearOffersInterface(c);
		TaskExecutor.executeSQL(new Runnable() {

			@Override
			public void run() {
				if (itemId > 0) {
					c.getMarketOffer().getFilteredBuyOffers().clear();
					MarketSql.loadPublicPurchaseOffers(c, false, itemId);
				} else {
					BuyOffers.getPublicBuyOffers().clear();
					MarketSql.loadPublicPurchaseOffers(c, true, 0);
				}

				if (c.isActiveAtomic()) {
					Server.getTaskScheduler().schedule(new Task() {

						@Override
						protected void execute() {
							this.stop();

							if (c.isLoggedIn()) {
								fillPublicOffers(c, itemId);
								c.getMarketOffer().setLoadingPublicOffers(false, true);
							}
						}
					});
				}
			}
		});
	}
	
	public static void fillPublicOffers(Client c, int itemId) {
		for (BuyOffers b : (itemId > 0 ? c.getMarketOffer().getFilteredBuyOffers().values() : getPublicBuyOffers().values())) {
//			if (b.getBuyerMID() == c.mySQLIndex) // makes the slots empty in public offers section if they are my own offers
//				continue;
			fillInterfaceSlot(c, b.getItem(), b.getFilterSlot(), b.getPricePerItem(), false);
		}
	}
	
	public static void openPublicBuyOffers(Client c, int itemId) {
		loadPublicOffers(c, itemId);
		c.getPacketSender().showInterface(19600);
		c.getMarketOffer().setBuyOfferItemId(itemId);
		if (itemId > 0) {
			c.getPacketSender().sendFrame126b("Filter: " + ItemConstants.getItemName(itemId), 19603);
		}
	}

	public static void createBuyOffer(final Client c, final Item item, final long priceOffer, final long pricePerItem) {
//		if (Config.NODE_ID != 1) {
//			c.sendMessage("You can only create buy offers from world 1.");
//			return;
//		}
		if (!TradeAndDuel.allowedToTradeItem(c, item.getId(), true)) {
			return;
		}
		if (!ItemProjectInsanity.itemIsSellable(item.getId())) {
			c.sendMessage("You cannot buy such an item.");
			return;
		}
		if (!c.inMarket()) {
			c.sendMessage("You must be in market to create an offer.");
			return;
		}
		if (c.isInCombatDelay()) {
			c.sendMessage("Please wait 10 seconds and try again.");
			return;
		}
		c.startLogoutDelay();
		MoneyPouch.removeFromMoneyPouch(c, priceOffer);
		c.sendMessage("Your Money Pouch was used to create a purchase offer.");
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				MarketSql.addBuyOfferToDB(c.getName(), c.mySQLIndex, item, pricePerItem);
			}
		});
		
	}
	
	public static void removeAllBuyOffers(final Client c) {
//		if (Config.NODE_ID != 1) {
//		c.sendMessage("You can only create buy offers from world 1.");
//		return;
//	}

		if (!c.inMarket()) {
			c.sendMessage("You must be in market to create an offer.");
			return;
		}
		if (!c.isInCombatDelay()) {
			c.sendMessage("Please wait 10 seconds and try again.");
			return;
		}
		c.startLogoutDelay();
		c.sendMessage("Removing All Buy Offers.");
		final int mysqlIndex = c.mySQLIndex;
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				long moneyPouch = 0;
				while (true) {
					long value = MarketSql.removeNextBuyOffer(mysqlIndex);
					if (value == 0)
						break;
					moneyPouch += value;
				}
				if (moneyPouch > 0 && c != null && c.isActive && !c.disconnected) {
					final long moneyFinal = moneyPouch;
					Server.getTaskScheduler().schedule(new Task(true) {
						
						@Override
						protected void execute() {
							this.stop();
							if (c.isLoggedIn()) {
								MoneyPouch.addToMoneyPouch(c, moneyFinal, true, 7);
								MoneyPouch.sendMoneyPouchAmount(c);
								c.sendMessage("Returned "+Misc.formatNumbersWithCommas(moneyFinal)+" gold to the Money Pouch.");
							}
						}
					});
				}
			}
		});
		
	}
	
	public static void removeBuyOrder(final Client c, final BuyOffers offer) {
//		if (Config.NODE_ID != 1) {
//		c.sendMessage("You can only create buy offers from world 1.");
//		return;
//	}
		
		if (c.getMarketOffer().isLoadingOwnOffers()) {
			return;
		}
		
		c.getMarketOffer().setLoadingOwnOffers(true, true);
		c.startLogoutDelay();
		c.sendMessage("Removing offer.");
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				if (!c.isActiveAtomic())
					return;
				
				long moneyPouch = 0;
				final long value = MarketSql.removeBuyOfferByID(offer.getMysqlID());
				if (value > 0)
					moneyPouch += value;

				final long moneyFinal = moneyPouch;
				Server.getTaskScheduler().schedule(new Task(true) {

					@Override
					protected void execute() {
						this.stop();
						if (c != null && c.isActive && !c.disconnected) {
							c.getMarketOffer().setLoadingOwnOffers(false, true);
							if (moneyFinal > 0) {
								MoneyPouch.addToMoneyPouch(c, moneyFinal, true, 7);
								c.sendMessage("Returned "+Misc.formatNumbersWithCommas(moneyFinal)+" gold to the Money Pouch.");
								refreshPlayerOwnedPurchaseOffers(c);
							} else {
								c.sendMessage("That offer has already been claimed. Reloading Trading Post.");
								refreshPlayerOwnedPurchaseOffers(c);
							}
						}
					}
				});
			}
		});
		
	}
	
	public static void completePublicSale(final Client c, final int itemId, final int slot, final int amountSold, final BuyOffers offer, final String itemName) {
		if (!Config.TRADING_POST_ENABLED) {
			c.sendMessage("Trading Post is currently Disabled");
			return;
		}
		if (!TradeAndDuel.allowedToTradeItem(c, itemId, true)) {
			return;
		}
		if (!ItemProjectInsanity.itemIsSellable(itemId)) {
			c.sendMessage("You cannot sell such an item.");
			return;
		}
		if (!c.getMarketOffer().getTimer().checkThenStart(4)) {
			c.sendMessage("Please slow down.");
			return;
		}
		
		if (c.getMarketOffer().isSaleInProgress()) {
			c.sendMessage("Please wait for your purchase to be completed.");
			return;
		}
		
		if (!c.inMarket()) {
			c.sendMessage("You must be in market to create an offer.");
			return;
		}
		if (c.isInCombatDelay()) {
			c.sendMessage("Please wait 10 seconds and try again.");
			return;
		}

		//Delete item first, if offer is no longer available, give back item.
		final Item itemDeleted = new Item(itemId, amountSold);
		if (!c.getItems().playerHasItem(itemId, amountSold)) {
			c.sendMessage("An error has occured. Please try again.");
			return;
		}
		
		c.getItems().deleteItem2(itemId, amountSold);
		
		final int lastX = c.getX(), lastY = c.getY();
		c.getMarketOffer().setSaleInProgress(true);
		c.startLogoutDelay();
		c.performingAction = true;
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
				if (!c.isActiveAtomic() || c.disconnected) {
					return;
				}
				if (lastX != c.getX() || lastY != c.getY()) {
					c.getMarketOffer().setSaleInProgress(false);
					c.performingAction = false;
					return;
				}
				c.startLogoutDelay();
				
				long moneyPouch = 0;
				final long value = MarketSql.sellItemToSlotOwner(offer.getMysqlID(), offer.getItem().getSlot(), amountSold, c);
				if (value > 0) {
					moneyPouch += value*amountSold;
				}

				final long moneyFinal = moneyPouch;
				Server.getTaskScheduler().schedule(new Task(true) {

					@Override
					protected void execute() {
						this.stop();
						c.performingAction = false;
						if (c.isActive && !c.disconnected) {
							c.getMarketOffer().setSaleInProgress(false);
							c.getPA().closeAllWindows();
							if (moneyFinal > 0) {
								MoneyPouch.addToMoneyPouch(c, moneyFinal, true, 7);
								MoneyPouch.sendMoneyPouchAmount(c);
								StringBuilder msg = new StringBuilder("You have sold ");
								msg.append(amountSold);
								msg.append(" ");
								msg.append(itemName);
								msg.append(" for ");
								msg.append(Misc.formatNumbersWithCommas(moneyFinal));
								msg.append(" gold.");
								c.sendMessage(msg.toString());
								c.sendMessage("Your gold was sent to the Money Pouch.");
								
								if (DBConfig.LOG_TRADING_POST) {
									TradingPostOrder order = new TradingPostOrder(offer.getBuyerName(), offer.getBuyerMID(), itemDeleted, value, c.getName(), c.mySQLIndex, TradingPostType.SELL_COMPLETED);
									Server.getLogWriter().addToTradingPostSaleList(order);
								}
								
							} else {
								c.sendMessage("That offer has already been claimed. Reloading Trading Post.");
								//give back item deleted because offer was already claimed by someone else on diff world
								c.getItems().addItem(itemDeleted.getId(), itemDeleted.getAmount());
							}
						}
					}
				});
			}
		});
		
	}
	
	public static void selectSearchedItem(Client c, int itemId) {
		c.getMarketOffer().setBuyOfferItemId(itemId);
		c.getMarketOffer().setQuantity(1);
		c.getMarketOffer().setPrice(1);
		long priceP = PriceChecker.getPriceLong(itemId);
		if (priceP > 0) {
			c.getMarketOffer().setPrice(priceP);
		}
		c.getMarketOffer().updateAveragePrice();
	}
	
	public static void refreshPlayerOwnedPurchaseOffers(final Client c) {
//		if (!c.getMarketOffer().isActive()) {
//			c.sendMess("poop");
//			return;
//		}
		if (c.getMarketOffer().isLoadingOwnOffers()) {
			return;
		}
		c.startLogoutDelay();
		c.getMarketOffer().setLoadingOwnOffers(true, true);

		clearOffersInterface(c);
		TaskExecutor.executeSQL(new Runnable() {

			@Override
			public void run() {

				c.getOwnBuyOffers().clear();
				MarketSql.loadPlayerPurchaseOffers(c);

				Server.getTaskScheduler().schedule(new Task() {
					
					@Override
					protected void execute() {
						this.stop();
						if (c != null && c.isActive) {
							for (BuyOffers b : c.getOwnBuyOffers().values()) {
								fillInterfaceSlot(c, b.getItem(), b.getItem().getSlot(), b.getPricePerItem(), true);
							}
							c.getMarketOffer().setLoadingOwnOffers(false, true);
						}
					}
				});
				
			}
		});
	}
	
	public static final int INTERFACE_ID_OWN_PURCHASES = 19751;
	public static final int INTERFACE_ID_PUBLIC_PURCHASES = 19616;
	
	public static void fillInterfaceSlot(Player c, Item item, int slot, long price, boolean playerOwnedPurchases) {
		slot *= 4;
		int itemNameInterface = (playerOwnedPurchases ? INTERFACE_ID_OWN_PURCHASES : INTERFACE_ID_PUBLIC_PURCHASES) + slot + 1;
		int amountInterface = (playerOwnedPurchases ? INTERFACE_ID_OWN_PURCHASES : INTERFACE_ID_PUBLIC_PURCHASES) + slot + 2;
		int priceInterface = (playerOwnedPurchases ? INTERFACE_ID_OWN_PURCHASES : INTERFACE_ID_PUBLIC_PURCHASES) + slot + 3;
		
		c.getPacketSender().sendFrame126b(ItemConstants.getItemName(item.getId()), itemNameInterface);
		c.getPacketSender().sendFrame126b(Misc.formatNumbersWithCommas(item.getAmount()), amountInterface);
		c.getPacketSender().sendFrame126b(Misc.formatNumbersWithCommas(price), priceInterface);
	}
	
	public static void clearOffersInterface(Client c) {
		c.getPacketSender().sendConfig(ConfigCodes.CLEAR_INTERFACE, 2);
	}
	
	private int mysqlID;
	private String buyerName;
	private int buyerMID;
	private Item item;
	private long pricePerItem;
	private int filterSlot;
	
	public BuyOffers(int mysqlID, String buyerName, int buyerMID, Item item, long pricePerItem) {
		this.mysqlID = mysqlID;
		this.buyerName = buyerName;
		this.buyerMID = buyerMID;
		this.item = item;
		this.pricePerItem = pricePerItem;
	}

	public int getMysqlID() {
		return mysqlID;
	}

	public void setMysqlID(int mysqlID) {
		this.mysqlID = mysqlID;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public int getBuyerMID() {
		return buyerMID;
	}

	public void setBuyerMID(int buyerMID) {
		this.buyerMID = buyerMID;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public long getPricePerItem() {
		return pricePerItem;
	}

	public void setPricePerItem(int pricePerItem) {
		this.pricePerItem = pricePerItem;
	}

	public int getFilterSlot() {
		return filterSlot;
	}

	public void setFilterSlot(int filterSlot) {
		this.filterSlot = filterSlot;
	}

}
