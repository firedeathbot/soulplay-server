package com.soulplay.content.items.pos;

import com.soulplay.game.model.item.Item;

public class UnclaimedItem {
	
	private final int mysqlID;
	private final int playerMID;
	private final String playerName;
	private final Item item;
	private final int slot;
	
	public UnclaimedItem(int mysqlId, int playerMID, String playerName, Item item, int slot) {
		this.mysqlID = mysqlId;
		this.playerMID = playerMID;
		this.playerName = playerName;
		this.item = item;
		this.slot = slot;
	}

	public int getMysqlID() {
		return mysqlID;
	}

	public int getPlayerMID() {
		return playerMID;
	}

	public String getPlayerName() {
		return playerName;
	}

	public Item getItem() {
		return item;
	}

	public int getSlot() {
		return slot;
	}

}
