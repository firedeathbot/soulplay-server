package com.soulplay.content.items.pos;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.cache.ItemDef;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.util.Misc;

public class TradingPostButtons {
	
	private static final int BUTTON_OWN_START = 77039, BUTTON_OWN_END = 77115, BUTTON_PUBLIC_START = 76160, BUTTON_PUBLIC_END = 76236;
	
	public static boolean handleButton(Client c, int buttonId) {
		//public offers
		if (buttonId >= BUTTON_PUBLIC_START && buttonId <= BUTTON_PUBLIC_END) {
			if (!tradingPostEnabled(c))
				return true;
			if (c.getMarketOffer().isLoadingPublicOffers()) {
				c.sendMessage("Please wait for the database to load.");
				return true;
			}
			int slot = buttonId - BUTTON_PUBLIC_START;
			if (slot > 0)
				slot = slot/4;
			final BuyOffers offer = c.getMarketOffer().getBuyOfferItemId() > 0 ?
					c.getMarketOffer().getFilteredBuyOffers().get(slot) :
						BuyOffers.getPublicBuyOffers().get(slot);
			if (offer != null) {
				if (offer.getBuyerMID() == c.mySQLIndex) {
					c.sendMessage("That is your Buy Order.");
					return true;
				}
				
				if (!c.getStopWatch().checkThenStart(1)) {
					c.sendMessage("Please try again.");
					return true;
				}
				
				final int noteditemID = ItemDef.getPossibleNotedId(offer.getItem().getId());

				final int playerItemCount = c.getItems().getItemAmount(offer.getItem().getId());
				final int notedItemCount = c.getItems().getItemAmount(noteditemID);
				if (playerItemCount < 1 && notedItemCount < 1) {
					c.sendMessage("You don't have that item to sell.");
					return true;
				}


				c.getDialogueBuilder().sendEnterAmount(()-> {
					if (c.enterAmount < 1 || c.enterAmount > Integer.MAX_VALUE) {
						c.sendMessage("You must enter a value below 2B.");
					} else {

						

						int sellingItemId = offer.getItem().getId();
						if (playerItemCount <= 0)
							sellingItemId = noteditemID;
						final int itemIdFinal = sellingItemId;
						int sellAmt = playerItemCount;
						if (sellAmt <= 0) 
							sellAmt = notedItemCount;
						if (sellAmt > offer.getItem().getAmount())
							sellAmt = offer.getItem().getAmount();
						
						if (c.enterAmount < sellAmt)
							sellAmt = (int)c.enterAmount;
						
						final int sellAmount = sellAmt;
						final String itemName = ItemConstants.getItemName(offer.getItem().getId());
						final String totalPrice = Misc.formatNumbersWithCommas(offer.getPricePerItem()*sellAmount);
						c.getDialogueBuilder().sendStatement("<col=ff0000>Confirm Sale?",
								"Item: "+itemName,
								"Amount to sell: "+sellAmount,
								"Price per item: " + Misc.formatNumbersWithCommas(offer.getPricePerItem()),
								"Total Price: <col=800000>" + totalPrice)
						.sendOption("Yes", () -> {
							BuyOffers.completePublicSale(c, itemIdFinal, offer.getItem().getSlot(), sellAmount, offer, itemName);
						} , "No", () -> {
							c.getPA().closeChatInterface();
							BuyOffers.openPublicBuyOffers(c, c.getMarketOffer().getBuyOfferItemId());
						});

					}
				}).execute(false);
				c.getPacketSender().sendInputDialogState(PlayerConstants.SET_DIALOG_AMOUNT_TO_SELL);
			}
			return true;
		}
		
		// player owned market offers
		if (buttonId >= BUTTON_OWN_START && buttonId <= BUTTON_OWN_END) {
			if (!tradingPostEnabled(c))
				return true;
			if (c.getMarketOffer().isLoadingOwnOffers()) {
				c.sendMessage("Please wait for the database to load.");
				return true;
			}
			if (!c.getStopWatch().checkThenStart(1)) {
				c.sendMessage("Please try again.");
				return true;
			}
			int slot = buttonId - BUTTON_OWN_START;
			if (slot > 0)
				slot = slot/4;
			final int slotFinal = slot;
			if (c.getClaimItems().isActive()) {
				c.getClaimItems().handleButtonClick(slotFinal);
				return true;
			}
			final BuyOffers offer = c.getOwnBuyOffers().get(slot);
			if (offer == null) {
				c.getDialogueBuilder()
					.sendStatement("Would you like to create a Buy Order?")
					.sendOption("Yes, create a buy order.", ()-> {
						c.getMarketOffer().openBuyOffer(slotFinal);
						c.getMarketOffer().setPrevInterface(19700);
					}, "No", ()-> {
						c.getPacketSender().showInterface(19700);
					} ).execute(false);
			} else {
				if (c.getMarketOffer().isLoadingOwnOffers()) {
					c.sendMessage("Please wait while your Trading Post is loading.");
					c.getPacketSender().showInterface(19700); // cheaphax to closing chat dialogue
				} else if (!c.inMarket()) {
					c.sendMessage("You must be in market to create an offer.");
					c.getPacketSender().showInterface(19700); // cheaphax to closing chat dialogue
				} else if (c.getStopWatch().checkThenStart(1)) {
					c.sendMessage("Please wait 1 second and try again.");
					c.getPacketSender().showInterface(19700); // cheaphax to closing chat dialogue
				} else {
					c.getDialogueBuilder()
					.sendStatement("Would you like to remove this offer?")
					.sendOption(
							"Yes, remove this offer.", ()-> {
								BuyOffers.removeBuyOrder(c, offer);
								c.getPacketSender().showInterface(19700); // cheaphax to closing chat dialogue
							}, 
							"No", ()->{
								c.getPacketSender().showInterface(19700); // cheaphax to closing chat dialogue
							}).execute(false);
				}
			}
			return true;
		}
		
		if (buttonId == 76048 && c.interfaceIdOpenMainScreen == 19600) {
			if (!tradingPostEnabled(c))
				return true;
			c.getPacketSender().sendInputDialogState(PlayerConstants.SEARCH_STATE);
			return true;
		}
		
		switch (buttonId) {
		case 76148: // open my own buy offers
			if (!tradingPostEnabled(c))
				return true;
			if (c.getMarketOffer().isLoadingOwnOffers()) {
				c.sendMessage("Please try again later, Trading Post is loading.");
				return true;
			}
			if (!c.getStopWatch().checkThenStart(1)) {
				c.sendMessage("Please try again.");
				return true;
			}
			BuyOffers.refreshPlayerOwnedPurchaseOffers(c);
			c.getMarketOffer().setPrevInterface(19600);
			c.getPacketSender().showInterface(19700);
			return true;
			
		case 76246: // back
			if (!tradingPostEnabled(c))
				return true;
			if (c.getMarketOffer().getPrevInterface() == 19600) {
				if (c.getStopWatch().checkThenStart(3)) {
					int itemId = 0;
					if (!c.getMarketOffer().getFilteredBuyOffers().isEmpty() && c.getMarketOffer().getBuyOfferItemId() > 0)
						itemId = c.getMarketOffer().getBuyOfferItemId();
					BuyOffers.openPublicBuyOffers(c, itemId);
				} else {
					c.sendMessage("Try again");
				}
			}
			return true;
		
		default:
			return false;
		}
	}
	
	public static boolean tradingPostEnabled(Client c) {
		if (c.playerRights == 2 && !DBConfig.ADMIN_CAN_SELL_ITEMS) {
			c.sendMessage("Selling/Buying items as an admin has been disabled.");
			return false;
		}
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			c.sendMessage("Wrong.");
			return false;
		}
		if (!Config.TRADING_POST_ENABLED) {
			c.sendMessage("Trading post has been disabled");
			return false;
		}
		return true;
	}
}
