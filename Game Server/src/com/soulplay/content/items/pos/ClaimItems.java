package com.soulplay.content.items.pos;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.timers.TickTimer;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.sql.pos.MarketSql;

public class ClaimItems {

	/**
	 * just a reference to use to load from database
	 */
	private final Map<Integer, UnclaimedItem> unclaimedItems = new HashMap<Integer, UnclaimedItem>();

	public Map<Integer, UnclaimedItem> getUnclaimedItems() {
		return unclaimedItems;
	}
	
	private boolean active = false;
	private boolean claiming = false;
	
	private boolean notifyPlayer = false;
	
	private TickTimer timer = new TickTimer();
	
	private final Client c;
	
	public ClaimItems(Client c) {
		this.c = c;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public void close() {
		setActive(false);
		unclaimedItems.clear();
	}
	
	public void openInterface() {
		c.getPacketSender().showInterface(19700);
		refreshInterface();
		setActive(true);
	}
	
	private void refreshInterface() {
		if (c.getMarketOffer().isLoadingOwnOffers()) {
			c.sendMessage("Loading");
			return;
		}
		if (!timer.checkThenStart(2)) {
			c.sendMessage("Slow down please.");
			return;
		}
		c.getMarketOffer().setLoadingOwnOffers(true, true);
		c.getPacketSender().sendConfig(ConfigCodes.CLEAR_INTERFACE, 3);
		c.getClaimItems().getUnclaimedItems().clear();
		
		TaskExecutor.executeSQL(new Runnable() {

			@Override
			public void run() {
				if (!c.isActiveAtomic())
					return;
				MarketSql.loadUnclaimedItems(c);
				
				Server.getTaskScheduler().schedule(new Task() {
					
					@Override
					protected void execute() {
						this.stop();
						if (c != null && c.isLoggedIn()) {
							c.getMarketOffer().setLoadingOwnOffers(false, true);
							
							for (UnclaimedItem uItem : unclaimedItems.values()) {
								BuyOffers.fillInterfaceSlot(c, uItem.getItem(), uItem.getSlot(), 0, true);
							}
						}
					}
				});
			}
		});
		
	}
	
	public void handleButtonClick(int slot) {
		final UnclaimedItem uItem = getUnclaimedItems().get(slot);
		
		if (uItem == null) {
			c.sendMessage("Item does not exist. Reload your interface.");
			return;
		}
		

		final Item item = uItem.getItem();
		final int itemIdFinal = ItemDef.getPossibleNotedId(item.getId());
		
		final int freeSlots = c.getItems().freeSlots();
		final boolean stackable = ItemProjectInsanity.itemIsStackable(itemIdFinal);
		final long claimAmount = item.getAmount();
		final long stackableCount = stackable ? c.getItems().getItemCount(itemIdFinal) : 0;
		final boolean moreThan2b = stackableCount + claimAmount > Integer.MAX_VALUE-1;
		if (freeSlots < 1 && stackableCount > 0 && moreThan2b) {
			c.sendMessage("You must have at least one empty slot.");
			if (moreThan2b)
				c.sendMessage("You already have too of this item in your inventory. Bank it first.");
			return;
		}
		
		if (!stackable && claimAmount > freeSlots) {
			c.sendMessage("You don't have enough room to withdraw this.");
			return;
		}

		if (claiming) {
			c.sendMessage("Please wait to receive your item.");
			return;
		}
		c.getPacketSender().sendString("Claiming...", 19707);
		claiming = true;
		
		TaskExecutor.executeSQL(new Runnable() {

			@Override
			public void run() {
				if (!c.isActiveAtomic())
					return;
				final int actualAmount = MarketSql.claimItem(uItem.getMysqlID());
				
				Server.getTaskScheduler().schedule(new Task() {
					
					@Override
					protected void execute() {
						this.stop();
						if (c != null && c.isLoggedIn()) {
							if (c.inMarket() && actualAmount > 0 && c.getClaimItems().isActive() && c.getItems().addItem(itemIdFinal, actualAmount)) {
								deleteClaimedAmount(uItem.getMysqlID(), actualAmount, item.getId(), c.mySQLIndex);
								c.sendMessage("You have claimed "+actualAmount+" "+ItemProjectInsanity.getItemName(itemIdFinal));
							} else {
								c.sendMessage("Please refresh your Interface");
								if (actualAmount <= 0)
									resetClaiming();
								refreshInterface();
							}
								
						}
					}
				});
			}
		});
	}
	
	private void deleteClaimedAmount(final int mysqlID, final int amount, final int itemId, int playerMID) {

		TaskExecutor.executeSQL(new Runnable() {

			@Override
			public void run() {
				final boolean removed = MarketSql.removeClaimAmount(mysqlID, amount);
				MarketSql.removeNotificationForCompleteOrder(playerMID);
				if(!removed) {
					System.out.println("FATAL ERROR IN CLAIMING! ID:"+mysqlID+" PLAYERID:"+ playerMID +" AMOUNT:"+amount+" ITEM:"+itemId);
				} else if (removed) {
					Server.getTaskScheduler().schedule(new Task() {
						
						@Override
						protected void execute() {
							this.stop();
							if (c != null && c.isLoggedIn()) {
								resetClaiming();
								refreshInterface();
							}
						}
					});
				}
			}
		});
	}
	
	private void resetClaiming() {
		claiming = false;
		timer.reset(); // reset the timer so their interface can refresh properly.
		c.getPacketSender().sendString("", 19707);
	}

	public boolean notifyPlayer() {
		return notifyPlayer;
	}

	public void setNotifyPlayer(boolean notifiedPlayer) {
		this.notifyPlayer = notifiedPlayer;
	}
	
}
