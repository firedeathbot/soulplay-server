package com.soulplay.content.items;

public enum BallistaCreationEnum {
	
	LIGHT(30028, 30032, 30024),
	HEAVY(30027, 30033, 30023);
	
	private final int frameId, incompleteId, completeId;
	
	private BallistaCreationEnum(int frameId, int incomplId, int complId) {
		this.frameId = frameId;
		this.incompleteId = incomplId;
		this.completeId = complId;
	}

	public int getFrameId() {
		return frameId;
	}

	public int getIncompleteId() {
		return incompleteId;
	}

	public int getCompleteId() {
		return completeId;
	}
	
	public static final BallistaCreationEnum[] values = values();
	
	public static BallistaCreationEnum get(int itemId) {
		for (int i = 0; i < values.length; i++) {
			BallistaCreationEnum bal = values[i];
			if (bal.getFrameId() == itemId || bal.getIncompleteId() == itemId)
				return bal;
		}
		return null;
	}

	public static final int BALLISTA_LIMBS = 30026;
	
	public static final int BALLISTA_SPRING = 30031;
	
}
