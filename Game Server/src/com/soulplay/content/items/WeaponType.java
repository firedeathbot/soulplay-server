package com.soulplay.content.items;

public enum WeaponType {
	DEFAULT, CROSSBOW, DOUBLE_SHOT, THROWN, DEGRADING, STAFF, CHINCHOMPA;
	
	public static final WeaponType[] values = values();
}
