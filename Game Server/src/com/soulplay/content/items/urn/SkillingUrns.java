package com.soulplay.content.items.urn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerAssistant;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.util.Misc;

public final class SkillingUrns {

	public static void load() {
		/* empty */
	}

	public static final SkillingUrn[] URNS = SkillingUrn.values();
	public static final Map<Integer, SkillingUrn> ACTIVE_URNS = new HashMap<>();
	public static final Map<Integer, SkillingUrn> FULL_URNS = new HashMap<>();
	public static final Map<Integer, SkillingUrn> NO_RUNE_URNS = new HashMap<>();

	static {
		for (int i = 0, length = URNS.length; i < length; i++) {
			SkillingUrn urn = URNS[i];

			ACTIVE_URNS.put(urn.getActive(), urn);
			FULL_URNS.put(urn.getFull(), urn);
			NO_RUNE_URNS.put(urn.getNoRune(), urn);
		}
	}

	public static void handleUrn(Player player, int skill, int level, int exp) {
		SkillingUrn activeUrn = null;

		for (SkillingUrn urn : URNS) {
			if (urn.getSkill() == skill && level <= urn.getMaxLevel()
					&& player.getItems().playerHasItem(urn.getActive())) {
				activeUrn = urn;
				break;
			}
		}

		boolean hasActiveUrn = activeUrn != null;

		if (hasActiveUrn) {
			int index = activeUrn.getIndex();
			int startCharge = player.urnData[index];
			int endCharge = startCharge + exp;

			String urnName = ItemDefinition.getName(activeUrn.getActive()).toLowerCase();

			// if the urn should turn into a full urn
			if (endCharge >= activeUrn.getCharge()) {

				int urnItemSlot = player.getItems().getItemSlot(activeUrn.getActive());
				if (urnItemSlot != -1) {
					player.getItems().replaceItemSlot(activeUrn.getFull(), urnItemSlot);
				}

				player.urnData[index] = 0;
				player.sendMessage("<col=ff0000>Your " + urnName + " is full.</col>");
				findAndActivateUrn(player, skill, level, endCharge - activeUrn.getCharge());
			} else {
				int startPercent = (startCharge * 100 / activeUrn.getCharge());
				int endPercent = (endCharge * 100 / activeUrn.getCharge());
				if (startPercent < 75 && endPercent >= 75) {
					player.sendMessage("<col=E47410>Your " + urnName + " is three-quarters full.</col>");
				} else if (startPercent < 50 && endPercent >= 50) {
					player.sendMessage("<col=E47410>Your " + urnName + " is half full.</col>");
				} else if (startPercent < 25 && endPercent >= 25) {
					player.sendMessage("<col=E47410>Your " + urnName + " is a quarter full.</col>");
				}

				player.urnData[index] = (short) endCharge;
			}

			// no need to look for inactive urn since we have an active one already
			return;
		}

		findAndActivateUrn(player, skill, level, exp);
	}

	private static void findAndActivateUrn(Player player, int skill, int level, int exp) {
		// activating a new urn
		SkillingUrn newUrn = null;

		for (SkillingUrn urn : URNS) {
			if (urn.getSkill() == skill && level <= urn.getMaxLevel()
					&& player.getItems().playerHasItem(urn.getRune())) {
				newUrn = urn;
				break;
			}
		}

		if (newUrn == null) {
			return;
		}

		String urnName = ItemDefinition.getName(newUrn.getActive()).toLowerCase();

		if (player.getItems().itemInBankNotPlaceholder(newUrn.getActive())) {
			player.sendMessage("You already have an active " + urnName + " in your bank.");
			return;
		}

		int totalFullUrns = player.getItems().getBankItemCount(newUrn.getFull())
				+ player.getItems().getItemAmount(newUrn.getFull());

		if (totalFullUrns >= 10) {
			player.sendMessage("You already have 10 full " + urnName + "s.");
			return;
		}

		int urnItemSlot = player.getItems().getItemSlot(newUrn.getRune());
		if (urnItemSlot != -1) {
			player.getItems().replaceItemSlot(newUrn.getActive(), urnItemSlot);
		}
		player.urnData[newUrn.getIndex()] = (short) exp;
		player.sendMessage("<col=E47410>You start a new " + urnName + ".</col>");
	}

	private static boolean handleActiveUrn(Player player, int item, int option) {
		SkillingUrn urn = ACTIVE_URNS.get(item);
		if (urn == null) {
			return false;
		}

		String urnName = ItemDefinition.getName(urn.getActive()).toLowerCase();

		switch (option) {
			case 1:
				int currentChange = player.urnData[urn.getIndex()];
				int percent = currentChange * 100 / urn.getCharge();
				player.sendMessage("This " + urnName + " is " + percent + "% full.");
				break;
		}

		return true;
	}

	private static boolean handleFullUrn(Player player, int item, int option) {
		SkillingUrn urn = FULL_URNS.get(item);
		if (urn == null) {
			return false;
		}

		String urnName = ItemDefinition.getName(urn.getActive()).toLowerCase();

		switch (option) {
			case 1:
				int Coins = 995;
				int expReward = urn.getReward();
				player.getItems().deleteItemInOneSlot(urn.getFull(), 1);
				player.getPA().addSkillXP(expReward, urn.getSkill());
				player.getItems().addItem(Coins, urn.getCoinAmt());
				player.sendMessage("You send the " + urnName + " away and it grants " + Misc.format(PlayerAssistant.getMultiplier(urn.getSkill(), player) * expReward) + " experience.");
				player.sendMessage("You also get " + Misc.format(urn.getCoinAmt()) + " Coins!");
				player.startAnimation(urn.getTeleportAnimation());
				break;
	
			case 2:
				player.sendMessage("This " + urnName + " is 100% full.");
				break;
		}
		return true;
	}

	public static boolean handleUrnClick(Player player, int item, int option) {
		if (handleActiveUrn(player, item, option)) {
			return true;
		}

		if (handleFullUrn(player, item, option)) {
			return true;
		}

		return false;
	}
	
	private static final String MISC_SAVE_QUERY = "INSERT INTO `misc`(`player_id`, `skilling_urns`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `skilling_urns` = VALUES(`skilling_urns`)";

	public static void saveToDbMisc(Player player) {
		if (!Config.RUN_ON_DEDI)
			return;
		
		PlayerSaveSql.SAVE_EXECUTOR.execute(new Runnable() {
			
			@Override
			public void run() {

				try (Connection connection = Server.getConnection();
						PreparedStatement ps = connection.prepareStatement(MISC_SAVE_QUERY);) {
					ps.setInt(1, player.mySQLIndex);
					ps.setString(2, getJsonString(player));
					ps.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		});
	}
	
	private static final String getJsonString(Player player) {
		boolean setNull = true;
		for (int i = 0; i < player.urnData.length; i++) {
			if (player.urnData[i] != 0) {
				setNull = false;
				break;
			}
		}
		if (setNull)
			return null;
		return PlayerSaveSql.createJsonString(player.urnData);
	}
	
	
	public static void loadUrnDataJsonString(Player player, String jsonString) {
		if (jsonString == null)
			return;
		player.urnData = Arrays.copyOf((short[])GsonSave.gsond.fromJson(jsonString, new TypeToken<short[]>(){}.getType()), SkillingUrn.SIZE);
	}

}
