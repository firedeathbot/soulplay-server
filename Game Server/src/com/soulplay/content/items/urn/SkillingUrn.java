package com.soulplay.content.items.urn;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.world.entity.Animation;

public enum SkillingUrn {

	CRACKED_COOKING(Skills.COOKING, 0, 20349, 2_000, 10, 400, 6794, 2, 12, 18, 554, 8649, 142_000),
	FRAGILE_COOKING(Skills.COOKING, 1, 20355, 2_750, 25, 550, 6795, 12, 16, 24, 554, 8651, 195_000),
	NORMAL_COOKING(Skills.COOKING, 2, 20361, 4_750, 40, 950, 7126, 36, 28, 42, 554, 8652, 337_000),
	STRONG_COOKING(Skills.COOKING, 3, 20367, 5_250, 55, 1_050, 7133, 51, 35, 52, 554, 8654, 374_000),
	DECORATED_COOKING(Skills.COOKING, 4, 20373, 7_738, 99, 1_548, 8629, 81, 52, 78, 554, 8691, 550_000),

	CRACKED_FISHING(Skills.FISHING, 5, 20319, 750, 10, 150, 6394, 2, 12, 18, 555, 6474, 158_000),
	FRAGILE_FISHING(Skills.FISHING, 6, 20325, 1_750, 30, 350, 6463, 15, 20, 30, 555, 6475, 368_000),
	NORMAL_FISHING(Skills.FISHING, 7, 20331, 2_500, 50, 500, 6471, 41, 31, 46, 555, 6769, 520_000),
	STRONG_FISHING(Skills.FISHING, 8, 20337, 3_000, 70, 600, 6472, 53, 36, 54, 555, 6770, 630_000),
	DECORATED_FISHING(Skills.FISHING, 9, 20343, 9_500, 99, 1900, 6473, 76, 48, 72, 555, 6789, 2_000_000),

	CRACKED_MINING(Skills.MINING, 10, 20379, 438, 1, 88, 10829, 1, 11, 16, 557, 11420, 210_000),
	FRAGILE_MINING(Skills.MINING, 11, 20385, 1_000, 20, 200, 10830, 17, 21, 31, 557, 11421, 480_000),
	NORMAL_MINING(Skills.MINING, 12, 20391, 1_625, 40, 200, 10831, 32, 27, 40, 557, 11425, 780_000),
	STRONG_MINING(Skills.MINING, 13, 20397, 2_000, 55, 400, 10947, 48, 30, 49, 557, 11447, 960_000),
	DECORATED_MINING(Skills.MINING, 14, 20403, 3_125, 85, 625, 11419, 59, 38, 57, 557, 11448, 1_500_000),

	IMPIOUS_PRAYER(-1, 15, 20409, 0, 0, 0, 4292, 2, 12, 18, 556, 4567, 1),
	ACCURSED_PRAYER(-1, 16, 20415, 0, 0, 0, 4541, 26, 25, 37, 556, 4569, 1),
	INFERNAL_PRAYER(-1, 17, 20421, 0, 0, 0, 4542, 62, 40, 60, 556, 4578, 1),

	CRACKED_SMELTING(Skills.SMITHING, 18, 20271, 200, 1, 40, 4580, 4, 15, 23, 554, 6384, 120_000),
	FRAGILE_SMELTING(Skills.SMITHING, 19, 20277, 313, 20, 63, 6380, 17, 21, 31, 554, 6385, 187_000),
	NORMAL_SMELTING(Skills.SMITHING, 20, 20283, 750, 50, 150, 6381, 35, 28, 42, 554, 6386, 450_000),
	STRONG_SMELTING(Skills.SMITHING, 21, 20289, 1_250, 85, 250, 6382, 49, 33, 50, 554, 6387, 750_000),

	CRACKED_WOODCUTTING(Skills.WOODCUTTING, 22, 20295, 800, 10, 160, 8713, 4, 15, 23, 557, 10279, 100_000),
	FRAGILE_WOODCUTTING(Skills.WOODCUTTING, 23, 20301, 2_125, 35, 425, 8727, 15, 20, 30, 557, 10280, 255_000),
	NORMAL_WOODCUTTING(Skills.WOODCUTTING, 24, 20307, 4_125, 58, 825, 8729, 44, 32, 48, 557, 10281, 500_000),
	STRONG_WOODCUTTING(Skills.WOODCUTTING, 25, 20313, 8_313, 75, 1663, 8730, 61, 38, 58, 557, 10828, 1_000_000);

	private final int skill, index, item, charge, maxLevel, reward, makeLevel, makeXp, fireXp, runeId, coinAmt;
	private final Animation teleportAnimation, chargeAnimation;

	SkillingUrn(int skill, int index, int item, int charge, int maxLevel, int reward, int teleportAnimation, int makeLevel, int makeXp, int fireXp, int runeId, int chargeAnimation, int coinAmt) {
		this.skill = skill;
		this.item = item;
		this.index = index;
		this.charge = charge;
		this.maxLevel = maxLevel;
		this.reward = reward;
		this.teleportAnimation = new Animation(teleportAnimation);
		this.makeLevel = makeLevel;
		this.makeXp = makeXp;
		this.fireXp = fireXp;
		this.runeId = runeId;
		this.chargeAnimation = new Animation(chargeAnimation);
		this.coinAmt = coinAmt;
	}

	public int getSkill() {
		return skill;
	}

	public int getIndex() {
		return index;
	}

	public int getUnfired() {
		return item;
	}

	public int getNoRune() {
		return item + 1;
	}

	public int getRune() {
		return item + 3;
	}

	public int getActive() {
		return item + 4;
	}

	public int getFull() {
		return item + 5;
	}

	public int getCharge() {
		return charge;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public int getReward() {
		return reward;
	}

	public Animation getTeleportAnimation() {
		return teleportAnimation;
	}

	public int getMakeLevel() {
		return makeLevel;
	}

	public int getMakeXp() {
		return makeXp;
	}

	public int getFireXp() {
		return fireXp;
	}

	public int getRuneId() {
		return runeId;
	}

	public Animation getChargeAnimation() {
		return chargeAnimation;
	}
	
	public int getCoinAmt() {
		return coinAmt;
	}

	public static final int SIZE = values().length;
	
}