package com.soulplay.content.items;

import java.util.function.Function;

import com.soulplay.content.items.degradeable.BrawlerGloveEnum;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerAssistant;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.packet.ConfigCodes;
import com.soulplay.util.Misc;

public class Lamps {

	public static enum LampType {
		REGULAR_LAMP(2528, level -> {
			if (level < 99) {
				return level * 50;
			} else {
				return level * 30;
			}
		}),
		ANTIQUE_LAMP(4447, level -> {
			if (level < 99) {
				return level * 50;
			} else {
				return level * 30;
			}
		}),
		DRAGONKIN_LAMP(18782, level -> {
			return (level * level * level - 2 * level * level + 100 * level) / 20 / 40 * 4;
		});

		private final int itemId;
		private final Function<Integer, Integer> experience;

		LampType(int itemId, Function<Integer, Integer> experience) {
			this.itemId = itemId;
			this.experience = experience;
		}

	}

	public static boolean buttonClick(Player player, int buttonId) {
		switch (buttonId) {
			case 2812:
				selectSkill(player, Skills.ATTACK);
				return true;
			case 2813:
				selectSkill(player, Skills.STRENGTH);
				return true;
			case 2814:
				selectSkill(player, Skills.RANGED);
				return true;
			case 2815:
				selectSkill(player, Skills.MAGIC);
				return true;
			case 2816:
				selectSkill(player, Skills.DEFENSE);
				return true;
			case 2817:
				selectSkill(player, Skills.HITPOINTS);
				return true;
			case 2818:
				selectSkill(player, Skills.PRAYER);
				return true;
			case 2819:
				selectSkill(player, Skills.AGILITY);
				return true;
			case 2820:
				selectSkill(player, Skills.HERBLORE);
				return true;
			case 2821:
				selectSkill(player, Skills.THIEVING);
				return true;
			case 2822:
				selectSkill(player, Skills.CRAFTING);
				return true;
			case 2823:
				selectSkill(player, Skills.RUNECRAFTING);
				return true;
			case 12034:
				selectSkill(player, Skills.SLAYER);
				return true;
			case 13914:
				selectSkill(player, Skills.FARMING);
				return true;
			case 2824:
				selectSkill(player, Skills.MINING);
				return true;
			case 2825:
				selectSkill(player, Skills.SMITHING);
				return true;
			case 2826:
				selectSkill(player, Skills.FISHING);
				return true;
			case 2827:
				selectSkill(player, Skills.COOKING);
				return true;
			case 2828:
				selectSkill(player, Skills.FIREMAKING);
				return true;
			case 2829:
				selectSkill(player, Skills.WOODCUTTING);
				return true;
			case 2830:
				selectSkill(player, Skills.FLETCHING);
				return true;
			case 2831:
				if (!checkRequirements(player)) {
					return true;
				}

				LampType lampType = player.selectedLampType;
				if (lampType == null) {
					return true;
				}

				int selectedSkill = player.selectedLampSkill;
				if (selectedSkill < 0) {
					player.sendMessage("You must select a skill to use the lamp on.");
					return true;
				}

				if (player.getItems().playerHasItem(lampType.itemId)) {
					int xp = lampType.experience.apply(player.getSkills().getStaticLevel(selectedSkill));
					if (xp <= 0) {
						player.sendMessage("Your selected skill level is too low to use the lamp on it.");
						return true;
					}

					player.getItems().deleteItem2(lampType.itemId, 1);
					player.getPA().addSkillXP(xp, selectedSkill);
					player.getPA().closeAllWindows();
					player.sendMessage("The lamp mysteriously vanishes... Rewarding you " + Misc.format(xp * PlayerAssistant.getMultiplier(selectedSkill, player)) + " xp in " + Skills.SKILL_NAME[selectedSkill] + ".");
				}

				return true;
			default:
				return false;
		}
	}

	public static void open(Player player, LampType lampType) {
		if (!checkRequirements(player)) {
			return;
		}

		player.selectedLampType = lampType;
		player.selectedLampSkill = -1;
		updateSelect(player);
		player.getPacketSender().showInterface(2808);
	}

	private static int skillToVarp(int skill) {
		switch (skill) {
			case Skills.ATTACK:
				return 1;
			case Skills.STRENGTH:
				return 2;
			case Skills.RANGED:
				return 3;
			case Skills.MAGIC:
				return 4;
			case Skills.DEFENSE:
				return 5;
			case Skills.HITPOINTS:
				return 6;
			case Skills.PRAYER:
				return 7;
			case Skills.AGILITY:
				return 8;
			case Skills.HERBLORE:
				return 9;
			case Skills.THIEVING:
				return 10;
			case Skills.CRAFTING:
				return 11;
			case Skills.RUNECRAFTING:
				return 12;
			case Skills.SLAYER:
				return 20;
			case Skills.FARMING:
				return 21;
			case Skills.MINING:
				return 13;
			case Skills.SMITHING:
				return 14;
			case Skills.FISHING:
				return 15;
			case Skills.COOKING:
				return 16;
			case Skills.FIREMAKING:
				return 17;
			case Skills.WOODCUTTING:
				return 18;
			case Skills.FLETCHING:
				return 19;
			default:
				return 0;
		}
	}

	private static void selectSkill(Player player, int skill) {
		player.selectedLampSkill = skill;
		updateSelect(player);
	}

	private static void updateSelect(Player player) {
		player.getPacketSender().sendConfig(ConfigCodes.TEMP, skillToVarp(player.selectedLampSkill));
	}

	public static boolean checkRequirements(Player player) {
		if (BrawlerGloveEnum.get(player.playerEquipment[PlayerConstants.playerHands]) != null) {
			player.sendMessage("You cannot use lamps with Brawling gloves on.");
			return false;
		}

		if (player.getDoubleExpLength() >= 1) {
			player.sendMessage("You cannot use lamp with double experience active.");
			return false;
		}

		return true;
	}

}
