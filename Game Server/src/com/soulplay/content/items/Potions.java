package com.soulplay.content.items;

import com.soulplay.Server;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.Sounds;

/**
 * @author Sanity
 */

public class Potions {
	
	private static final int DUNGEONEERING_VIAL = 17490;

	private Player c;

	public Potions(Player c) {
		this.c = c;
	}


	public void handleEmptyPots(int slot) {
		if (c.isBreakVials())
			c.getItems().deleteItemInSlot(slot);
		else
			c.getItems().replaceItemSlot(EMPTY_VIAL, slot);
	}

	public void antifirePot(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.getPA().antiFirePotion();
		c.sendMessage(
				"<col=BF3EFF>Your immunity against dragon fire has been increased.");
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);

	}

	public void doEnergyPotion(int itemId, int replaceItem, int slot,
			int recoverAmount) {
		c.startAnimation(829);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214);
		energyPotion(recoverAmount);
	}
	
	public void energyPotion(int recoverAmount) {
		c.getMovement().restoreRunEnergy(recoverAmount);
		c.getPacketSender().sendRunEnergy();
	}
	
	public void staminaPotionEffect() {
		energyPotion(20);
		c.startStaminaTimer();
	}

	public void doingWolperSpecial(int itemId, int replaceItem, int slot,
			int stat, boolean sup) {
		c.startAnimation(829);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		doWolperSpecial(stat, sup);

	}

	public int doneWolperSpecial(int skill, boolean sup) {
		int increaseBy = 0;
		if (sup) {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .09);
		} else {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .09);
		}
		if (c.getSkills().getLevel(skill)
				+ increaseBy > c.getSkills().getStaticLevel(skill)
						+ increaseBy + 1) {
			return c.getSkills().getStaticLevel(skill) + increaseBy
					- c.getSkills().getLevel(skill);
		}
		return increaseBy;
	}

	/********************************************************
	 * Allantois additions
	 *******************************************/
	public void doOverload(int itemId, int replaceItem, int slot) {
		if (c.safeTimer > 0) {
			c.sendMessage("You cannot drink the Overload in the Wilderness.");
			return;
		}
		if (c.inWild()) {
			c.sendMessage("You cannot drink the Overload in the Wilderness.");
			return;
		}
		if (c.isInJail()) {
			c.sendMessage("You cannot drink the overload in jail.");
			return;
		}

		int health = c.getSkills().getLifepoints();
		if (health < 50) {
			c.sendMessage("I should get some more lifepoints before using this!");
			return;
		}

		resetOverload();// c.hasOverloadBoost = false;
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		c.hasOverloadBoost = true;
		doOverloadBoost();
		handleOverloadTimers();
	}

	public void doOverloadBoost() {
		int[] toIncrease = {0, 1, 2, 4, 6};
		int boost;
		for (int i = 0; i < toIncrease.length; i++) {
			boost = (int) (getOverloadBoost(toIncrease[i]));
			c.getSkills().updateLevel(toIncrease[i], boost);
		}
	}

	public void doPrayerRenewal(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214);
		prayerRenewal();
	}
	
	public void prayerRenewal() {
		c.getPA().prayerRenewal(500);
		c.startGraphic(Client.PRAYER_RENEW_GFX);
	}

	public void doTheBrew(int itemId, int replaceItem, int slot) {
		if (c.duelRule[DuelRules.NO_FOOD]) {
			c.sendMessage("You may not eat in this duel.");
			return;
		}
		if (c.clanWarRule[CWRules.NO_FOOD]) {
			c.sendMessage("Food disabled in this war.");
			return;
		}
		if (c.getSkills().getLifepoints() < 1) {
			return;
		}
		if (c.getZones().isInDmmLobby() && Tournament.pre_settings.isSpawnGear()) {
			c.sendMessage("You can't do that right now.");
			return;
		}
		c.startAnimation(829);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);

		c.getSkills().drainLevel(Skills.ATTACK, 0.10, 1);
		c.getSkills().drainLevel(Skills.STRENGTH, 0.10, 1);
		c.getSkills().drainLevel(Skills.RANGED, 0.10, 1);
		c.getSkills().drainLevel(Skills.MAGIC, 0.10, 1);
		c.getSkills().updateLevel(Skills.DEFENSE, getBrewStat(Skills.DEFENSE, 0.20), (int)(c.getSkills().getStaticLevel(Skills.DEFENSE) * 1.2));

		int max = (int) (c.calculateMaxLifePoints() * 1.15);
		double heal =  max * 0.15f;
		if (c.getSeasonalData().containsPowerUp(PowerUpData.MOVEMENT)) {
			heal += heal * 0.2f;
		}

		int healInt = (int) heal;
		if (c.getSkills().getLifepoints() < max) {
			if (c.getSkills().getLifepoints() + healInt > max) {
				c.getAchievement().heal50000HP(healInt - (max - c.getSkills().getLifepoints()));
			} else {
				c.getAchievement().heal50000HP(healInt);
			}
		}

		c.getSkills().updateLevel(Skills.HITPOINTS, healInt, max);
	}

	public void doTheBrewzam(int itemId, int replaceItem, int slot) { // by
																		// hontiris1
		if (c.duelRule[DuelRules.NO_FOOD]) {
			c.sendMessage("You may not eat in this duel.");
			return;
		}

		if (c.clanWarRule[CWRules.NO_FOOD]) {
			c.sendMessage("Food disabled in this war.");
			return;
		}
		c.startAnimation(829);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		
		c.getSkills().drainLevel(Skills.DEFENSE, 0.10, 1);
		c.getSkills().drainLevel(Skills.HITPOINTS, 0.10, 1);
		
		c.getSkills().updateLevel(Skills.ATTACK, getBrewStat(Skills.ATTACK, 0.20), (int)(c.getSkills().getStaticLevel(Skills.ATTACK) * 1.2));
		c.getSkills().updateLevel(Skills.STRENGTH, getBrewStat(Skills.STRENGTH, 0.12), (int)(c.getSkills().getStaticLevel(Skills.STRENGTH) * 1.2));
		c.getSkills().updateLevel(Skills.PRAYER, getBrewStat(Skills.PRAYER, 0.10), c.getSkills().getStaticLevel(Skills.PRAYER));
	}

	public void doTitanSpecial(int skill) {
		int addon = 0;
		addon = c.getSkills().getStaticLevel(skill);
		addon *= 0.125;
		c.getSkills().setLevel(skill, c.getSkills().getStaticLevel(skill)
				+ addon);
	}

	public void doTortoiseSpecial() {
		c.getSkills().updateLevel(Skills.DEFENSE, 9);
	}

	public void doWolperSpecial() {
		c.getSkills().updateLevel(Skills.MAGIC, 7);
	}

	public void doWolperSpecial(int skillID, boolean sup) {
//		c.getSkills().setLevel(Skills.MAGIC, c.getSkills().getLevel(Skills.MAGIC) + doneWolperSpecial(Skills.MAGIC, sup));
		c.getSkills().updateLevel(Skills.MAGIC, c.getSkills().getStaticLevel(Skills.MAGIC) + 7);
	}

	public void drinkAntiPoison(int itemId, int replaceItem, int slot, int ticks) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		c.getDotManager().consumeAntiPoison(ticks);
	}

	public void drinkCombatPotion(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		enchanceStat(Skills.ATTACK, true);
		enchanceStat(Skills.STRENGTH, true);
	}

	public void drinkExtremePotion(int itemId, int replaceItem, int slot,
			int stat, boolean sup) {
		if (c.safeTimer > 0) {
			c.sendMessage("You cannot drink the Extremes in the Wilderness.");
			return;
		}
		if (c.inWild()) {
			c.sendMessage("You cannot drink the Extremes in the Wilderness.");
			return;
		}
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		enchanceStat2(stat, sup);
	}

	public void drinkSuperPrayer(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214);
		c.getSkills().incrementPrayerPoints((int) (c.getSkills().getStaticLevel(Skills.PRAYER) / 2.9 + 7));
	}

	public void drinkMagicPotion(int itemId, int replaceItem, int slot,
			int stat, boolean sup) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		enchanceMagic(stat, sup);
	}

	public void drinkPrayerPot(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214);
		prayerPotion();
	}
	
	public void prayerPotion() {
		if (c.inLMSArenaCoords()) {
			c.getSkills().incrementPrayerPoints(10);
		} else {
		    c.getSkills().incrementPrayerPoints(Math.round(7.0f + c.getSkills().getStaticLevel(Skills.PRAYER) * 0.25f));
		}
	}

	public void drinkSuperRestore(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214);
		restoreStats(8.0f);
	}
	
	public void drinkSanfew(int itemId, int replaceItem, int slot) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214);
		restoreStats(9.0f);
		c.getDotManager().consumeAntiPoison(600);

		if (c.attr().has("nightmare_parasite")) {
			c.attr().remove("nightmare_parasite");
			c.sendMessage("<col=229628>The parasite within you has been weakened.");
		}
	}

	public void drinkStatPotion(int itemId, int replaceItem, int slot, int stat,
			boolean sup) {
		c.startAnimation(829);
		c.getPacketSender().playSound(Sounds.DRINK_POT);
		if (replaceItem == EMPTY_VIAL) {
			handleEmptyPots(slot);
		} else
			c.playerItems[slot] = replaceItem + 1;
		c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		enchanceStat(stat, sup);
	}

	public void enchanceMagic(int skillID, boolean sup) {
		c.getSkills().updateLevel(skillID, getBoostedMagic(skillID, sup));
		if (c.wildLevel > 0) {
			if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_MAGE) {
				c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_MAGE);
			}
		}
	}

	public void enchanceStat(int skillID, boolean sup) {
		c.getSkills().updateLevel(skillID, getBoostedStat(skillID, sup));

		if (c.wildLevel > 0) {
			switch (skillID) {
				case RSConstants.ATTACK:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_ATT) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_ATT);
					}
					break;
				case RSConstants.STRENGTH:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_STR) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_STR);
					}
					break;
				case RSConstants.DEFENCE:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_DEF) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_DEF);
					}
					break;
				case RSConstants.RANGED:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_RNG) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_RNG);
					}
					break;
				case RSConstants.MAGIC:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_MAGE) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_MAGE);
					}
					break;
			}
		}

	}

	public void enchanceStat2(int skillID, boolean sup) {
		c.getSkills().updateLevel(skillID, getExtremeStat(skillID, sup));
		if (c.wildLevel > 0) {
			switch (skillID) {
				case RSConstants.ATTACK:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_ATT) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_ATT);
					}
					break;
				case RSConstants.STRENGTH:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_STR) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_STR);
					}
					break;
				case RSConstants.DEFENCE:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_DEF) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_DEF);
					}
					break;
				case RSConstants.RANGED:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_RNG) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_RNG);
					}
					break;
				case RSConstants.MAGIC:
					if (c.getSkills().getLevel(skillID) > RSConstants.MAX_WILD_MAGE) {
						c.getSkills().setLevel(skillID, RSConstants.MAX_WILD_MAGE);
					}
					break;
			}
		}
	}

	public int getBoostedMagic(int skill, boolean sup) {
		int increaseBy = 0;
		if (sup) {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .06);
		} else {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .06);
		}
		/*if (c.getSkills().getLevel(skill)
				+ increaseBy > c.getSkills().getStaticLevel(skill)
						+ increaseBy + 1) {
			return c.getSkills().getStaticLevel(skill) + increaseBy
					- c.getSkills().getLevel(skill);
		}*/
		return increaseBy;
	}

	public int getBoostedStat(int skill, boolean sup) {
		int increaseBy = 0;
		if (sup) {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .20);
		} else {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .13) + 1;
		}
		/*if (c.getSkills().getLevel(skill)
				+ increaseBy > c.getSkills().getStaticLevel(skill)
						+ increaseBy + 1) {
			return c.getSkills().getStaticLevel(skill) + increaseBy
					- c.getSkills().getLevel(skill);
		}*/
		return increaseBy;
	}

	public int getBrewStat(int skill, double amount) {
		return (int) (c.getSkills().getStaticLevel(skill)
				* amount);
	}

	public int getExtremeStat(int skill, boolean sup) {
		int increaseBy = 0;
		if (skill == 6) {
			// double boost = PlayerConstants.getLevelForXP(c.playerXP[skill]) *
			// .10 + 3;
			increaseBy = 7;// (int)Math.round(boost);

			/*if (c.getSkills().getLevel(skill) + increaseBy > c.getSkills().getStaticLevel(skill) + increaseBy + 1) {
				return c.getSkills().getStaticLevel(skill)
						+ increaseBy - c.getSkills().getLevel(skill);
			}*/
			return increaseBy;
		}
		if (sup) {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .22) + 4;
		} else {
			increaseBy = (int) (c.getSkills().getStaticLevel(skill)
					* .22) + 5;
		}
		/*if (c.getSkills().getLevel(skill)
				+ increaseBy > c.getSkills().getStaticLevel(skill)
						+ increaseBy + 1) {
			return c.getSkills().getStaticLevel(skill) + increaseBy
					- c.getSkills().getLevel(skill);
		}*/
		return increaseBy;
	}

	public double getOverloadBoost(int skill) {
		double boost = 1;
		switch (skill) {
			case 0:
			case 1:
			case 2:
				boost = 5 + (c.getSkills().getStaticLevel(Skills.HITPOINTS)
						* .22);
				break;
			case 4:
				boost = 3 + (c.getSkills().getStaticLevel(Skills.HITPOINTS)
						* .22);
				break;
			case 6:
				boost = 7;
				break;
		}
		return boost;
	}

	public void handleOverloadTimers() {
		c.overload1 = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (c.disconnected) {
							container.stop();
							return;
						}

						c.hasOverloadBoost = false;
						c.getSkills().heal(50);
						container.stop();
					}

					@Override
					public void stop() {
						if (c != null || !c.disconnected) {
							c.sendMessage("Your overloads effect has ended.");
						}
					}
				}, 500); // 5 minutes
		
		c.overload2 = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (c.disconnected) {
							container.stop();
							return;
						}
						if (c != null) {
							if (c.hasOverloadBoost) {
								doOverloadBoost();
							} else {
								container.stop();
								resetOverload();
							}
						} else {
							container.stop();
						}
					}

					@Override
					public void stop() {

					}
				}, 25); // 15 seconds
		
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int counter2 = 0;

			@Override
			public void execute(CycleEventContainer container) {
				if (c.disconnected || c.getHitPoints() <= 11) {
					container.stop();
					return;
				}
				if (counter2 < 5) {
					c.startAnimation(2383);
					int hitAmount = c.dealDamage(new Hit(10, 0, -1));
					counter2++;
					if (hitAmount < 10) {
						container.stop();
					}
				} else {
					container.stop();
				}
			}

			@Override
			public void stop() {

			}
		}, 1);
	}
	
	private boolean canDrink() {
		return Server.getTotalTicks() >= c.getPotionTick();
	}

	public void handlePotion(final int itemId, final int slot) {
		if (c.duelRule[DuelRules.NO_DRINKS]) {
			c.sendMessage("You may not drink potions in this duel.");
			return;
		}
		if (c.clanWarRule[CWRules.NO_DRINKS]) {
			c.sendMessage("You may not drink potions in this war.");
			return;
		}
		if (c.isDead()) {
			return;
		}
		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			return;
		}
		if (c.getHitPoints() <= 0 || !canDrink()) {
			return;
		}
		if (c.isLockActions()) {
			return;
		}
		// if (System.currentTimeMillis() - c.potDelay >= 1200) {
		// c.potDelay = System.currentTimeMillis();
		// c.foodDelay = c.potDelay;
		c.getCombat().resetPlayerAttack();
		// c.attackTimer++;


		// System.out.println("Food delay "+(System.currentTimeMillis() -
		// c.foodDelay));
		// c.foodDelay = System.currentTimeMillis();

		c.setPotionTick();
		switch (itemId) {
			case 17564://weak restore
				doRestorePotion(slot, 5.2f, 0.12f);
				break;
			case 17590://restore
				doRestorePotion(slot, 7f, 0.17f);
				break;
			case 17614://strong restore
				doRestorePotion(slot, 10, 0.24f);
				break;
			case 17570://weak rejuvenation
				doRejuvenationPotion(slot, 4f, 0.08f);
				break;
			case 17594://rejuvenation
				doRejuvenationPotion(slot, 7f, 0.15f);
				break;
			case 17618://strong rejuvenation
				doRejuvenationPotion(slot, 10, 0.22f);
				break;
			case 17574://weak gatherer
				doBoostPotion(slot, 3, 0.02f, Skills.WOODCUTTING, Skills.MINING, Skills.FISHING);
				break;
			case 17576://weak artisan
				doBoostPotion(slot, 3, 0.02f, Skills.SMITHING, Skills.CRAFTING, Skills.FLETCHING, Skills.CONSTRUCTION, Skills.FIREMAKING);
				break;
			case 17578://weak naturalist
				doBoostPotion(slot, 3, 0.02f, Skills.COOKING, Skills.FARMING, Skills.HERBLORE, Skills.RUNECRAFTING);
				break;
			case 17580://weak survivalist
				doBoostPotion(slot, 3, 0.02f, Skills.AGILITY, Skills.HUNTER, Skills.THIEVING, Skills.SLAYER);
				break;
			case 17598://gatherer
				doBoostPotion(slot, 4, 0.04f, Skills.WOODCUTTING, Skills.MINING, Skills.FISHING);
				break;
			case 17600://artisan
				doBoostPotion(slot, 4, 0.04f, Skills.SMITHING, Skills.CRAFTING, Skills.FLETCHING, Skills.CONSTRUCTION, Skills.FIREMAKING);
				break;
			case 17602://naturalist
				doBoostPotion(slot, 4, 0.04f, Skills.COOKING, Skills.FARMING, Skills.HERBLORE, Skills.RUNECRAFTING);
				break;
			case 17604://survivalist
				doBoostPotion(slot, 4, 0.04f, Skills.AGILITY, Skills.HUNTER, Skills.THIEVING, Skills.SLAYER);
				break;
			case 17622://strong gatherer
				doBoostPotion(slot, 6, 0.06f, Skills.WOODCUTTING, Skills.MINING, Skills.FISHING);
				break;
			case 17624://strong artisan
				doBoostPotion(slot, 6, 0.06f, Skills.SMITHING, Skills.CRAFTING, Skills.FLETCHING, Skills.CONSTRUCTION, Skills.FIREMAKING);
				break;
			case 17626://strong naturalist
				doBoostPotion(slot, 6, 0.06f, Skills.COOKING, Skills.FARMING, Skills.HERBLORE, Skills.RUNECRAFTING);
				break;
			case 17628://strong survivalist
				doBoostPotion(slot, 6, 0.06f, Skills.AGILITY, Skills.HUNTER, Skills.THIEVING, Skills.SLAYER);
				break;
			case 17556://weak magic
				doBoostPotion(slot, 2, 0.07f, Skills.MAGIC);
				break;
			case 17558://weak ranged
				doBoostPotion(slot, 2, 0.07f, Skills.RANGED);
				break;
			case 17560://weak melee
				doBoostPotion(slot, 2, 0.07f, Skills.ATTACK, Skills.STRENGTH);
				break;
			case 17562://weak def
				doBoostPotion(slot, 2, 0.07f, Skills.DEFENSE);
				break;
			case 17582://magic
				doBoostPotion(slot, 2.5f, 0.11f, Skills.MAGIC);
				break;
			case 17584://ranged
				doBoostPotion(slot, 2.5f, 0.11f, Skills.RANGED);
				break;
			case 17586://melee
				doBoostPotion(slot, 2.5f, 0.11f, Skills.ATTACK, Skills.STRENGTH);
				break;
			case 17588://def
				doBoostPotion(slot, 2.5f, 0.11f, Skills.DEFENSE);
				break;
			case 17606://strong magic
				doBoostPotion(slot, 3, 0.15f, Skills.MAGIC);
				break;
			case 17608://strong ranged
				doBoostPotion(slot, 3, 0.15f, Skills.RANGED);
				break;
			case 17610://strong melee
				doBoostPotion(slot, 3, 0.15f, Skills.ATTACK, Skills.STRENGTH);
				break;
			case 17612://strong def
				doBoostPotion(slot, 3, 0.15f, Skills.DEFENSE);
				break;
			case 3008: // energy potion 4
				doEnergyPotion(itemId, 3010, slot, 10);
				break;
			case 3010: // energy potion 3
				doEnergyPotion(itemId, 3012, slot, 10);
				break;
			case 3012: // energy potion 2
				doEnergyPotion(itemId, 3014, slot, 10);
				break;
			case 3014: // energy potion 1
				doEnergyPotion(itemId, EMPTY_VIAL, slot, 10);
				break;
			case 3016: // super energy potion 4
				doEnergyPotion(itemId, 3018, slot, 20);
				break;
			case 3018: // super energy potion 3
				doEnergyPotion(itemId, 3020, slot, 20);
				break;
			case 3020: // super energy potion 2
				doEnergyPotion(itemId, 3022, slot, 20);
				break;
			case 3022: // super energy potion 1
				doEnergyPotion(itemId, EMPTY_VIAL, slot, 20);
				break;
			case 2450: // zamy brew by hontiris1
				doTheBrewzam(itemId, 189, slot);
				break;
			case 189:
				doTheBrewzam(itemId, 191, slot);
				break;
			case 191:
				doTheBrewzam(itemId, 193, slot);
				break;
			case 193:
				doTheBrewzam(itemId, EMPTY_VIAL, slot);
				break;
			case 2452:
				antifirePot(itemId, 2454, slot);
				break;
			case 2454:
				antifirePot(itemId, 2456, slot);
				break;
			case 2456:
				antifirePot(itemId, 2458, slot);
				break;
			case 2458:
				antifirePot(itemId, EMPTY_VIAL, slot);
				break;
			case 3040:
				drinkMagicPotion(itemId, 3042, slot, 6, false); // Magic pots
				break;
			case 3042:
				drinkMagicPotion(itemId, 3044, slot, 6, false);
				break;
			case 3044:
				drinkMagicPotion(itemId, 3046, slot, 6, false);
				break;
			case 3046:
				drinkMagicPotion(itemId, EMPTY_VIAL, slot, 6, false);
				break;
			case 6685: // brews
				doTheBrew(itemId, 6687, slot);
				break;
			case 6687:
				doTheBrew(itemId, 6689, slot);
				break;
			case 6689:
				doTheBrew(itemId, 6691, slot);
				break;
			case 6691:
				doTheBrew(itemId, EMPTY_VIAL, slot);
				break;
			case 9739: // combat potion
				drinkCombatPotion(itemId, 9741, slot);
				break;
			case 9741: // combat potion
				drinkCombatPotion(itemId, 9743, slot);
				break;
			case 9743: // combat potion
				drinkCombatPotion(itemId, 9745, slot);
				break;
			case 9745: // combat potion
				drinkCombatPotion(itemId, EMPTY_VIAL, slot);
				break;
			case 2436:
				drinkStatPotion(itemId, 145, slot, 0, true); // sup attack
				break;
			case 145:
				drinkStatPotion(itemId, 147, slot, 0, true);
				break;
			case 147:
				drinkStatPotion(itemId, 149, slot, 0, true);
				break;
			case 149:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 0, true);
				break;
			case 2440:
				drinkStatPotion(itemId, 157, slot, 2, true); // sup str
				break;
			case 157:
				drinkStatPotion(itemId, 159, slot, 2, true);
				break;
			case 159:
				drinkStatPotion(itemId, 161, slot, 2, true);
				break;
			case 161:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 2, true);
				break;
			case 2444:
				drinkStatPotion(itemId, 169, slot, 4, false); // range pot
				break;
			case 169:
				drinkStatPotion(itemId, 171, slot, 4, false);
				break;
			case 171:
				drinkStatPotion(itemId, 173, slot, 4, false);
				break;
			case 173:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 4, false);
				break;
			case 2432:
				drinkStatPotion(itemId, 133, slot, 1, false); // def pot
				break;
			case 133:
				drinkStatPotion(itemId, 135, slot, 1, false);
				break;
			case 135:
				drinkStatPotion(itemId, 137, slot, 1, false);
				break;
			case 137:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 1, false);
				break;
			case 113:
				drinkStatPotion(itemId, 115, slot, 2, false); // str pot
				break;
			case 115:
				drinkStatPotion(itemId, 117, slot, 2, false);
				break;
			case 117:
				drinkStatPotion(itemId, 119, slot, 2, false);
				break;
			case 119:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 2, false);
				break;
			case 2428:
				drinkStatPotion(itemId, 121, slot, 0, false); // attack pot
				break;
			case 121:
				drinkStatPotion(itemId, 123, slot, 0, false);
				break;
			case 123:
				drinkStatPotion(itemId, 125, slot, 0, false);
				break;
			case 125:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 0, false);
				break;
			case 2442:
				drinkStatPotion(itemId, 163, slot, 1, true); // super def pot
				break;
			case 163:
				drinkStatPotion(itemId, 165, slot, 1, true);
				break;
			case 165:
				drinkStatPotion(itemId, 167, slot, 1, true);
				break;
			case 167:
				drinkStatPotion(itemId, EMPTY_VIAL, slot, 1, true);
				break;
			case 3024:
				drinkSuperRestore(itemId, 3026, slot); // sup restore
				break;
			case 3026:
				drinkSuperRestore(itemId, 3028, slot);
				break;
			case 3028:
				drinkSuperRestore(itemId, 3030, slot);
				break;
			case 3030:
				drinkSuperRestore(itemId, EMPTY_VIAL, slot);
				break;
			case 10925:
				drinkSanfew(itemId, 10927, slot); // sanfew serums
				break;
			case 10927:
				drinkSanfew(itemId, 10929, slot);
				break;
			case 10929:
				drinkSanfew(itemId, 10931, slot);
				break;
			case 10931:
				drinkSanfew(itemId, EMPTY_VIAL, slot);
				break;
			case 2434:
				drinkPrayerPot(itemId, 139, slot); // pray pot
				break;
			case 139:
				drinkPrayerPot(itemId, 141, slot);
				break;
			case 141:
				drinkPrayerPot(itemId, 143, slot);
				break;
			case 143:
				drinkPrayerPot(itemId, EMPTY_VIAL, slot);
				break;
			case 2446:
				drinkAntiPoison(itemId, 175, slot, 150); // anti poisons
				break;
			case 175:
				drinkAntiPoison(itemId, 177, slot, 150);
				break;
			case 177:
				drinkAntiPoison(itemId, 179, slot, 150);
				break;
			case 179:
				drinkAntiPoison(itemId, EMPTY_VIAL, slot, 150);
				break;
			case 2448:
				drinkAntiPoison(itemId, 181, slot, 600); // anti poisons
				break;
			case 181:
				drinkAntiPoison(itemId, 183, slot, 600);
				break;
			case 183:
				drinkAntiPoison(itemId, 185, slot, 600);
				break;
			case 185:
				drinkAntiPoison(itemId, EMPTY_VIAL, slot, 600);
				break;

			/******************************
			 * Allantois additions
			 ***************************************/
			case 15312: // Extreme Strength
				drinkExtremePotion(itemId, 15313, slot, 2, false);
				break;
			case 15313: // Extreme Strength
				drinkExtremePotion(itemId, 15314, slot, 2, false);
				break;
			case 15314: // Extreme Strength
				drinkExtremePotion(itemId, 15315, slot, 2, false);
				break;
			case 15315: // Extreme Strength
				drinkExtremePotion(itemId, EMPTY_VIAL, slot, 2, false);
				break;
			case 15308: // Extreme Attack
				drinkExtremePotion(itemId, 15309, slot, 0, false);
				break;
			case 15309: // Extreme Attack
				drinkExtremePotion(itemId, 15310, slot, 0, false);
				break;
			case 15310: // Extreme Attack
				drinkExtremePotion(itemId, 15311, slot, 0, false);
				break;
			case 15311: // Extreme Attack
				drinkExtremePotion(itemId, EMPTY_VIAL, slot, 0, false);
				break;
			case 15316: // Extreme Defence
				drinkExtremePotion(itemId, 15317, slot, 1, false);
				break;
			case 15317: // Extreme Defence
				drinkExtremePotion(itemId, 15318, slot, 1, false);
				break;
			case 15318: // Extreme Defence
				drinkExtremePotion(itemId, 15319, slot, 1, false);
				break;
			case 15319: // Extreme Defence
				drinkExtremePotion(itemId, EMPTY_VIAL, slot, 1, false);
				break;
			case 15324: // Extreme Ranging
				drinkExtremePotion(itemId, 15325, slot, 4, false);
				break;
			case 15325: // Extreme Ranging
				drinkExtremePotion(itemId, 15326, slot, 4, false);
				break;
			case 15326: // Extreme Ranging
				drinkExtremePotion(itemId, 15327, slot, 4, false);
				break;
			case 15327: // Extreme Ranging
				drinkExtremePotion(itemId, EMPTY_VIAL, slot, 4, false);
				break;
			case 15320: // Extreme Magic
				drinkExtremePotion(itemId, 15321, slot, 6, false);
				break;
			case 15321: // Extreme Magic
				drinkExtremePotion(itemId, 15322, slot, 6, false);
				break;
			case 15322: // Extreme Magic
				drinkExtremePotion(itemId, 15323, slot, 6, false);
				break;
			case 15323: // Extreme Magic
				drinkExtremePotion(itemId, EMPTY_VIAL, slot, 6, false);
				break;
			case 15328: // Super Prayer
				drinkSuperPrayer(itemId, 15329, slot);
				break;
			case 15329: // Super Prayer
				drinkSuperPrayer(itemId, 15330, slot);
				break;
			case 15330: // Super Prayer
				drinkSuperPrayer(itemId, 15331, slot);
				break;
			case 15331: // Super Prayer
				drinkSuperPrayer(itemId, EMPTY_VIAL, slot);
				break;
			case 15300: // Recover Special
				recoverSpecial(itemId, 15301, slot);
				break;
			case 15301: // Recover Special
				recoverSpecial(itemId, 15302, slot);
				break;
			case 15302: // Recover Special
				recoverSpecial(itemId, 15303, slot);
				break;
			case 15303: // Recover Special
				recoverSpecial(itemId, EMPTY_VIAL, slot);
				break;
			case 15332:
				doOverload(itemId, 15333, slot);
				break;
			case 15333:
				doOverload(itemId, 15334, slot);
				break;
			case 15334:
				doOverload(itemId, 15335, slot);
				break;
			case 15335:
				doOverload(itemId, EMPTY_VIAL, slot);
				break;
			case 21630:
				doPrayerRenewal(itemId, 21632, slot);
				break;
			case 21632:
				doPrayerRenewal(itemId, 21634, slot);
				break;
			case 21634:
				doPrayerRenewal(itemId, 21636, slot);
				break;
			case 21636:
				doPrayerRenewal(itemId, EMPTY_VIAL, slot);
				break;

		}

		// }

	}

	public static boolean isPotion(int itemId) {
		switch (itemId) {
			case 17564://weak restore
			case 17590://restore
			case 17614://strong restore
			case 17570://weak rejuvenation
			case 17594://rejuvenation
			case 17618://strong rejuvenation
			case 17574://weak gatherer
			case 17576://weak artisan
			case 17578://weak naturalist
			case 17580://weak survivalist
			case 17598://gatherer
			case 17600://artisan
			case 17602://naturalist
			case 17604://survivalist
			case 17622://strong gatherer
			case 17624://strong artisan
			case 17626://strong naturalist
			case 17628://strong survivalist
			case 17556:
			case 17558:
			case 17560:
			case 17562:
			case 17582:
			case 17584:
			case 17586:
			case 17588:
			case 17606:
			case 17608:
			case 17610:
			case 17612:
				return true;
		}

		String name = ItemConstants.getItemName(itemId);
		return name != null && (name.contains("(4)") || name.contains("(3)")
				|| name.contains("(2)") || name.contains("(1)"));
	}

	public void recoverSpecial(int itemId, int replaceItem, int slot) {
		if (c.inWild()) {
			c.sendMessage(
					"You are unable to restore special in the wilderness.");
			return;
		} else if (c.specAmount >= 7.5) {
			c.sendMessage(
					"You are unable to drink the potion as your special is above 75%.");
		} else {
			if (System.currentTimeMillis() - c.restoreDelay >= 30000) {
				c.specAmount += 2.5;
				c.startAnimation(829);
				c.getPacketSender().playSound(Sounds.DRINK_POT);
				c.sendMessage(
						"As you drink drink the potion, you feel your special attack slightly regenerate.");
				if (replaceItem == EMPTY_VIAL) {
					handleEmptyPots(slot);
				} else
					c.playerItems[slot] = replaceItem + 1;
				c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
				c.getItems().updateSpecialBar();
				c.restoreDelay = System.currentTimeMillis();
			} else {
				c.sendMessage(
						"You can only restore your special once every 30 seconds.");
			}
		}
	}

	public void applyDungeoneeringBoost(int statId, float flat, float percent) {
		int base = c.getSkills().getStaticLevel(statId);
		int stat = c.getSkills().getLevel(statId);
		int levelToMod = stat > base ? base : stat;
		c.getSkills().setLevel(statId, (int) (levelToMod + flat + (base * percent)));
	}

	public void doBoostPotion(int slot, float flat, float percent, int... statIds) {
		c.startAnimation(829);
		c.playerItems[slot] = DUNGEONEERING_VIAL + 1;
		c.setUpdateInvInterface(3214);
		for (int statId : statIds) {
			applyDungeoneeringBoost(statId, flat, percent);
		}
	}

	public void applyDungeoneeringRejuvenation(int statId, float flat, float percent) {
		int base = c.getSkills().getStaticLevel(statId);
		int stat = c.getSkills().getLevel(statId);
		if (stat > base) {
			return;
		}

		int restore = (int) (flat + (percent * base));
		int newStat = stat + restore;
		if (stat + restore > base) {
			newStat = base;
		}

		c.getSkills().setLevel(statId, newStat);
	}

	public void applyDungeoneeringRestore(int statId, float flat, float percent) {
		int base = c.getSkills().getStaticLevel(statId);
		int stat = c.getSkills().getLevel(statId);
		if (stat > base) {
			return;
		}

		int restore = (int) (flat + (percent * stat));
		int newStat = stat + restore;
		if (stat + restore > base) {
			newStat = base;
		}

		c.getSkills().setLevel(statId, newStat);
	}

	public void doRestorePotion(int slot, float flat, float percent) {
		c.startAnimation(829);
		c.playerItems[slot] = DUNGEONEERING_VIAL + 1;
		c.setUpdateInvInterface(3214);
		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			if (i == Skills.PRAYER || i == Skills.SUMMONING || i == Skills.HITPOINTS) {
				continue;
			}

			applyDungeoneeringRestore(i, flat, percent);
		}
	}

	public void doRejuvenationPotion(int slot, float flat, float percent) {
		c.startAnimation(829);
		c.playerItems[slot] = DUNGEONEERING_VIAL + 1;
		c.setUpdateInvInterface(3214);
		applyDungeoneeringRestore(Skills.SUMMONING, flat, percent);
		applyDungeoneeringRejuvenation(Skills.PRAYER, flat, percent);
	}

	private static final int[] toNormalise = {0, 1, 2, 4, 6};

	public void resetOverload() {
		if (c.overload1 != null)
			c.overload1.stop();
		if (c.overload2 != null)
			c.overload2.stop();
		
		if (!c.hasOverloadBoost) {
			return;
		}
		c.hasOverloadBoost = false;
		for (int i = 0; i < toNormalise.length; i++) {
			c.getSkills().setLevel(toNormalise[i], c.getSkills().getStaticLevel(toNormalise[i]));
		}
		c.sendMessage("The effects of the potion have worn off...");
		if (c.getSkills().getLifepoints() > c.getSkills().getMaximumLifepoints()) { // this will never happen??? wtf is this PI junk shit? TODO: fix overload reset.
			c.getSkills().healToFull();
		}
	}

	public void restoreStats(float flat) {
		for (int i = 0; i < Skills.STAT_COUNT; i++) {
			if (i == Skills.HITPOINTS) {
				continue;
			}

			c.getSkills().updateLevel(i, (int) (flat + c.getSkills().getStaticLevel(i) * 0.25f), c.getSkills().getStaticLevel(i));
		}
	}

	public static final int EMPTY_VIAL = 229;
	
}
