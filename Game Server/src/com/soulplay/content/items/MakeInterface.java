package com.soulplay.content.items;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;

public class MakeInterface {

	public static void make(Player c, int product1, Runnable product1Code) {
		make(c, product1, product1Code, -1, null, -1, null, -1, null, -1, null);
	}

	public static void make(Player c, int product1, Runnable product1Code, int product2, Runnable product2Code, int product3, Runnable product3Code) {
		make(c, product1, product1Code, product2, product2Code, product3, product3Code, -1, null, -1, null);
	}

	public static void make(Player c, int product1, Runnable product1Code, int product2, Runnable product2Code,
			int product3, Runnable product3Code, int product4, Runnable product4Code) {
		make(c, product1, product1Code, product2, product2Code, product3, product3Code, product4, product4Code, -1, null);
	}

	public static void make(Player c, int product1, Runnable product1Code, int product2, Runnable product2Code,
			int product3, Runnable product3Code, int product4, Runnable product4Code, int product5,
			Runnable product5Code) {
		c.makeActions.clear();
		c.makeActions.add(product1Code);
		c.makeActions.add(product2Code);
		c.makeActions.add(product3Code);
		c.makeActions.add(product4Code);
		c.makeActions.add(product5Code);
		if (product1Code != null && product2Code != null && product3Code != null && product4Code != null
				&& product5Code != null) {
			c.getPacketSender().sendChatInterface(8938);
			c.getPacketSender().sendFrame246(8941, 250, product1);
			c.getPacketSender().sendFrame246(8942, 250, product2);
			c.getPacketSender().sendFrame246(8943, 250, product3);
			c.getPacketSender().sendFrame246(8944, 250, product4);
			c.getPacketSender().sendFrame246(8945, 250, product5);
		} else if (product1Code != null && product2Code != null && product3Code != null && product4Code != null
				&& product5Code == null) {
			c.getPacketSender().sendChatInterface(8899);
			c.getPacketSender().sendFrame246(8902, 250, product1);
			c.getPacketSender().sendFrame246(8903, 250, product2);
			c.getPacketSender().sendFrame246(8904, 250, product3);
			c.getPacketSender().sendFrame246(8905, 250, product4);
		} else if (product1Code != null && product2Code != null && product3Code != null && product4Code == null
				&& product5Code == null) {
			c.getPacketSender().sendChatInterface(8880);
			c.getPacketSender().sendFrame246(8883, 250, product1);
			c.getPacketSender().sendFrame246(8884, 250, product2);
			c.getPacketSender().sendFrame246(8885, 250, product3);
			ItemDefinition itemDef = ItemDefinition.forId(product1);
			if (itemDef != null) {
				c.getPacketSender().sendFrame126(itemDef.getName(), 8889);
			}

			itemDef = ItemDefinition.forId(product2);
			if (itemDef != null) {
				c.getPacketSender().sendFrame126(itemDef.getName(), 8893);
			}

			itemDef = ItemDefinition.forId(product3);
			if (itemDef != null) {
				c.getPacketSender().sendFrame126(itemDef.getName(), 8897);
			}
		} else if (product1Code != null && product2Code != null && product3Code == null && product4Code == null
				&& product5Code == null) {
			c.getPacketSender().sendChatInterface(8866);
			c.getPacketSender().sendFrame246(8869, 250, product1);
			c.getPacketSender().sendFrame246(8870, 250, product2);
		} else if (product1Code != null && product2Code == null && product3Code == null && product4Code == null
				&& product5Code == null) {
			c.getPacketSender().sendChatInterface(4429);
			c.getPacketSender().sendFrame246(1746, 250, product1);
		}
	}

	public static void runProduct(Client c, int buttonId) {
		if (c.makeActions.size() == 0) {
			return;
		}

		int count = 0;
		Runnable product = null;
		switch (buttonId) {
		// First
		case 10239:
		case 34170:
		case 34185:
		case 34205:
		case 34245:
			count = 1;
			product = c.makeActions.get(0);
			break;
		case 10238:
		case 34169:
		case 34184:
		case 34204:
		case 34244:
			count = 5;
			product = c.makeActions.get(0);
			break;
		case 6212:
		case 34168:
		case 34183:
		case 34203:
		case 34243:
			count = 10;
			product = c.makeActions.get(0);
			break;
		case 6211:
		case 34167:
		case 34182:
		case 34202:
		case 34242:
			count = 35;
			product = c.makeActions.get(0);
			break;

		// Second
		case 34174:
		case 34189:
		case 34209:
		case 34249:
			count = 1;
			product = c.makeActions.get(1);
			break;
		case 34173:
		case 34188:
		case 34208:
		case 34248:
			count = 5;
			product = c.makeActions.get(1);
			break;
		case 34172:
		case 34187:
		case 34207:
		case 34247:
			count = 10;
			product = c.makeActions.get(1);
			break;
		case 34171:
		case 34186:
		case 34206:
		case 34246:
			count = 35;
			product = c.makeActions.get(1);
			break;

		// Third
		case 34193:
		case 34213:
		case 34253:
			count = 1;
			product = c.makeActions.get(2);
			break;
		case 34192:
		case 34212:
		case 34252:
			count = 5;
			product = c.makeActions.get(2);
			break;
		case 34191:
		case 34211:
		case 34251:
			count = 10;
			product = c.makeActions.get(2);
			break;
		case 34190:
		case 34210:
		case 34250:
			count = 35;
			product = c.makeActions.get(2);
			break;

		// Fourth
		case 34217:
		case 35001:
			count = 1;
			product = c.makeActions.get(3);
			break;
		case 34216:
		case 35000:
			count = 5;
			product = c.makeActions.get(3);
			break;
		case 34215:
		case 34255:
			count = 10;
			product = c.makeActions.get(3);
			break;
		case 34214:
		case 34254:
			count = 35;
			product = c.makeActions.get(3);
			break;

		// Fifth
		case 35005:
			count = 1;
			product = c.makeActions.get(4);
			break;
		case 35004:
			count = 5;
			product = c.makeActions.get(4);
			break;
		case 35003:
			count = 10;
			product = c.makeActions.get(4);
			break;
		case 35002:
			count = 35;
			product = c.makeActions.get(4);
			break;
		}

		if (product == null) {
			return;
		}

		c.makeCount = count;
		product.run();
		c.makeActions.clear();
	}

}
