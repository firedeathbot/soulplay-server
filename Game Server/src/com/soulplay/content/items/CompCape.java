package com.soulplay.content.items;

public class CompCape {

	public static final int MAX_COLORS = 4;
	
	private short[] colors = new short[MAX_COLORS]; // 4 max colors i guess
	
	private CompCape(short... colors) {
		this.setColors(colors);
	}

	public short[] getColors() {
		return colors;
	}

	public void setColors(short[] colors) {
		this.colors = colors;
	}
	
	public void changeColor(int slot, short color) {
		this.colors[slot] = color;
	}
	
	public static CompCape create() {
		return new CompCape((short)65214, (short)65200, (short)65186, (short)62995);
	}
}
