package com.soulplay.content.items;

import java.util.HashMap;

import com.soulplay.Server;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.sounds.Sounds;

public class Food {

	public enum FoodToEat {
		Easter_Egg(1961, 12, "Easter Egg", 0, 2),
		Pumpkin(1959, 14, "Pumpkin", 0, 2),
		Half_Jug_of_Wine(1989, 7, "Half Full Wine Jug", 0, 2),
		Wine(1993, 11, "Wine", 0, 2),
		COOKED_MEAT(2142, 3, "Cooked meat", 0, 2),
		MANTA(391, 22, "Manta Ray", 0, 3),
		SHARK(385, 20, "Shark", 0, 3),
		MACKEREL(355, 6, "Mackerel", 0, 2),
		LOBSTER(379, 12, "Lobster", 0, 2),
		BEER(1917, 1, "Beer", 0, 2),
		GREENMANS_ALE(1909, 1, "Greenman's Ale", 0, 2),
		TROUT(333, 7, "Trout", 0, 2),
		SALMON(329, 9, "Salmon", 0, 2),
		SWORDFISH(373, 14, "Swordfish", 0, 2),
		TUNA(361, 10, "Tuna", 0, 2),
		MONKFISH(7946, 16, "Monkfish", 0, 2),
		SEA_TURTLE(397, 21, "Sea Turtle", 0, 2),
		CABBAGE(1965, 1, "Cabbage", 0, 2),
		CAKE(1891, 4, "Cake", 1893, 2),
		CAKE2THIRD(1893, 4, "Cake", 1895, 2),
		CAKE_SLICE(1895, 4, "Cake", 0, 2),
		BASS(365, 13, "Bass", 0, 2),
		COD(339, 7, "Cod", 0, 2),
		POTATO(1942, 1, "Potato", 0, 2),
		BAKED_POTATO(6701, 4, "Baked Potato", 0, 2),
		POTATO_WITH_CHEESE(6705, 16, "Potato with Cheese", 0, 2),
		EGG_POTATO(7056, 16, "Egg Potato", 0, 2),
		CHILLI_POTATO(7054, 14, "Chilli Potato", 0, 2),
		MUSHROOM_POTATO(7058, 20, "Mushroom Potato", 0, 2),
		TUNA_POTATO(7060, 22, "Tuna Potato", 0, 2),
		SHRIMPS(315, 3, "Shrimps", 0, 2),
		HERRING(347, 5, "Herring", 0, 2),
		SARDINE(325, 4, "Sardine", 0, 2),
		CHOCOLATE_CAKE(1897, 5, "Chocolate Cake", 1899, 2),
		HALF_CHOCOLATE_CAKE(1899, 5, "2/3 Chocolate Cake", 1901, 2),
		CHOCOLATE_SLICE(1901, 5, "Chocolate Slice", 0, 2),
		ANCHOVIES(319, 1, "Anchovies", 0, 2),
		PLAIN_PIZZA(2289, 7, "Plain Pizza", 2291, 2),
		HALF_PLAIN_PIZZA(2291, 7, "1/2 Plain pizza", 0, 2),
		MEAT_PIZZA(2293, 8, "Meat Pizza", 2295, 2),
		HALF_MEAT_PIZZA(2295, 8, "1/2 Meat Pizza", 0, 2),
		ANCHOVY_PIZZA(2297, 9, "Anchovy Pizza", 2299, 2),
		HALF_ANCHOVY_PIZZA(2299, 9, "1/2 Anchovy Pizza", 0, 2),
		PINEAPPLE_PIZZA(2301, 11, "Pineapple Pizza", 2303, 2),
		HALF_PINEAPPLE_PIZZA(2303, 11, "1/2 Pineapple Pizza", 0, 2),
		BREAD(2309, 5, "Bread", 0, 2),
		APPLE_PIE(2323, 7, "Apple Pie", 2335, 2),
		HALF_APPLE_PIE(2335, 7, "Half Apple Pie", ItemIds.PIE_DISH, 2),
		REDBERRY_PIE(2325, 5, "Redberry Pie", 2333, 2),
		HALF_REDBERRY_PIE(2333, 5, "Half Redberry Pie", ItemIds.PIE_DISH, 2),
		Ugthanki_kebab(1883, 2, "Ugthanki kebab", 0, 2),
		MEAT_PIE(2327, 6, "Meat Pie", 2331, 2),
		HALF_MEAT_PIE(2331, 6, "Half Meat Pie", ItemIds.PIE_DISH, 2),
		SUMMER_PIE(7218, 11, "Summer Pie", 7220, 2),
		HALF_SUMMER_PIE(7220, 11, "Half Summer Pie", ItemIds.PIE_DISH, 2),
		PIKE(351, 8, "Pike", 0, 2),
		POTATO_WITH_BUTTER(6703, 14, "Potato with Butter", 0, 2),
		BANANA(1963, 2, "Banana", 0, 2),
		PEACH(6883, 8, "Peach", 0, 2),
		ORANGE(2108, 2, "Orange", 0, 2),
		PINEAPPLE_RINGS(2118, 2, "Pineapple Rings", 0, 2),
		PINEAPPLE_CHUNKS(2116, 2, "Pineapple Chunks", 0, 2),
		EASTER_EGG(7928, 1, "Easter Egg", 0, 2),
		EASTER_EGG2(7929, 1, "Easter Egg", 0, 2),
		EASTER_EGG3(7930, 1, "Easter Egg", 0, 2),
		EASTER_EGG4(7931, 1, "Easter Egg", 0, 2),
		EASTER_EGG5(7932, 1, "Easter Egg", 0, 2),
		EASTER_EGG6(7933, 1, "Easter Egg", 0, 2),
		PURPLE_SWEETS(10476, 9, "Purple Sweets", 0, 2),
		POT_OF_CREAM(2130, 1, "Pot of cream", 0, 2),
		BANDAGES(4049, 3, "Bandages", 0, 2),
		ROCKTAIL(15272, 23, "Rocktail", 0, 3),
		RAW_CAVE_POTATO(17817, 2, "Raw cave potato", 0, 2),
		BAKED_CAVE_POTATO(18093, 2, "Baked cave potato", 0, 2),
		GISSEL_POTATO(18095, 6, "Gissel potato", 0, 2),
		EDICAP_POTATO(18097, 12, "Edicap potato", 0, 2),
		HEIM_CRAB(18159, 2, "Heim crab", 0, 2),
		HEIM_CRAB_POTATO(18099, 5, "Heim crab potato", 0, 2),
		HEIM_CRAB_GISSEL_POTATO(18119, 8, "Heim crab & gissel potato", 0, 2),
		HEIM_CRAB_EDICAP_POTATO(18139, 14, "Heim crab & edicap potato", 0, 2),
		RED_EYE(18161, 5, "Red-eye", 0, 2),
		RED_EYE_POTATO(18101, 8, "Red-eye potato", 0, 2),
		RED_EYE_GISSEL_POTATO(18121, 11, "Red-eye & gissel potato", 0, 2),
		RED_EYE_EDICAP_POTATO(18141, 17, "Red-eye & edicap potato", 0, 2),
		DUSK_EEL(18163, 7, "Dusk eel", 0, 2),
		DUSK_EEL_POTATO(18103, 10, "Dusk eel potato", 0, 2),
		DUSK_EEL_GISSEL_POTATO(18123, 13, "Dusk eel & gissel potato", 0, 2),
		DUSK_EEL_EDICAP_POTATO(18143, 19, "Dusk eel & edicap potato", 0, 2),
		GIANT_FLATFISH(18165, 10, "Giant flatfish", 0, 2),
		GIANT_FLATFISH_POTATO(18105, 13, "Giant flatfish potato", 0, 2),
		GIANT_FLATFISH_GISSEL_POTATO(18125, 16, "Flatfish & gissel potato", 0, 2),
		GIANT_FLATFISH_EDICAP_POTATO(18145, 22, "Flatfist & edicap potato", 0, 2),
		SHORT_FINNED_EEL(18167, 12, "Short-finned eel", 0, 2),
		SHORT_FINNED_POTATO(18107, 15, "Short-fin eel potato", 0, 2),
		SHORT_FINNED_GISSEL_POTATO(18127, 18, "Short-fin & gissel potato", 0, 2),
		SHORT_FINNED_EDICAP_POTATO(18147, 24, "Short-fin & edicap potato", 0, 2),
		WEB_SNIPPER(18169, 15, "Web snipper", 0, 2),
		WEB_SNIPPER_POTATO(18109, 18, "Snippet potato", 0, 2),
		WEB_SNIPPER_GISSEL_POTATO(18129, 21, "Snipper & gissel potato", 0, 2),
		WEB_SNIPPER_EDICAP_POTATO(18149, 27, "Snipper & edicap potato", 0, 2),
		BOULDABASS(18171, 17, "Bouldabass", 0, 2),
		BOULDABASS_POTATO(18111, 20, "Bouldabass potato", 0, 2),
		BOULDABASS_GISSEL_POTATO(18131, 23, "Bouldabass & gissel potato", 0, 2),
		BOULDABASS_EDICAP_POTATO(18151, 29, "Bouldabass & edicap potato", 0, 2),
		SALVE_EEL(18173, 20, "Salve eel", 0, 2),
		SALVE_EEL_POTATO(18113, 23, "Salve eel potato", 0, 2),
		SALVE_EEL_GISSEL_POTATO(18133, 26, "Salve eel & gissel potato", 0, 2),
		SALVE_EEL_EDICAP_POTATO(18153, 32, "Salve eel & edicap potato", 0, 2),
		BLUE_CRAB(18175, 22, "Blue crab", 0, 2),
		BLUE_CRAB_POTATO(18115, 25, "Blue crab potato", 0, 2),
		BLUE_CRAB_GISSEL_POTATO(18135, 28, "Blue crab & gissel potato", 0, 2),
		BLUE_CRAB_EDICAP_POTATO(18155, 34, "Blue crab & edicap potato", 0, 2),
		CAVE_MORAY(18177, 25, "Cave moray", 0, 2),
		CAVE_MORAY_POTATO(18117, 28, "Cave moray potato", 0, 2),
		CAVE_MORAY_GISSEL_POTATO(18137, 31, "Moray & gissel potato", 0, 2),
		CAVE_MORAY_EDICAP_POTATO(18157, 37, "Moray & edicap potato", 0, 2),
		ANGLERFISH(113441, 22, "Anglerfish", 0, 2);

		private final int id;

		private final int heal;

		private final String name;
		
		private final int secondId;
		
		private final int ticks;

		FoodToEat(int id, int heal, String name, int secondId, int ticks) {
			this.id = id;
			this.heal = heal;
			this.name = name;
			this.secondId = secondId;
			this.ticks = ticks;
		}

		public int getHeal() {
			return heal;
		}

		public int getId() {
			return id;
		}

		public int getTicks() {
			return ticks;
		}

		public int getSecondId() {
			return secondId;
		}

		public String getName() {
			return name;
		}

	}

	public enum FoodToWin {
		MANTA(391, 22, "Manta Ray"),
		SHARK(385, 20, "Shark"),
		LOBSTER(379, 12, "Lobster"),
		SALMON(329, 9, "Salmon"),
		SWORDFISH(373, 14, "Swordfish"),
		TUNA(361, 10, "Tuna"),
		MONKFISH(7946, 16, "Monkfish"),
		SEA_TURTLE(397, 21, "Sea Turtle"),
		BASS(365, 13, "Bass"),
		POTATO_WITH_CHEESE(6705, 16, "Potato with Cheese"),
		EGG_POTATO(7056, 16, "Egg Potato"),
		CHILLI_POTATO(7054, 14, "Chilli Potato"),
		MUSHROOM_POTATO(7058, 20, "Mushroom Potato"),
		TUNA_POTATO(7060, 22, "Tuna Potato"),
		PIKE(351, 8, "Pike"),
		ROCKTAIL(15272, 23, "Rocktail"),
		KARAMBWANS(3144, 18, "Karambwan"),
		TROUT(333, 7, "Trout"),
		ANGLERFISH(113441, 22, "Anglerfish");

		public static final FoodToWin[] values = values();

		private final int id;

		private final int heal;

		private final String name;

		private FoodToWin(int id, int heal, String name) {
			this.id = id;
			this.heal = heal;
			this.name = name;
		}

		public int getHeal() {
			return heal;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

	}

	public static HashMap<Integer, FoodToEat> food = new HashMap<>();
	
	public static boolean canEat(Client c) {
		return c.getFoodTick() <= Server.getTotalTicks();
	}
	
	public static boolean canEatKaram(Client c) {
		return c.getKaramTick() <= Server.getTotalTicks();
	}

	public static void eat(final Client c, FoodToEat food, int id, int slot) {
		if (c.duelRule[DuelRules.NO_FOOD]) {
			c.sendMessage("You may not eat in this duel.");
			return;
		}
		if (c.clanWarRule[CWRules.NO_FOOD]) {
			c.sendMessage("Food disabled in this war.");
			return;
		}
		if (c.isDead()) {
			return;
		}
		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			return;
		}
		if (!canEat(c)) {
			// c.sendMessage("Already eating");
			return;
		}
		if (!canEatKaram(c)) {
			// c.sendMessage("Already eating");
			return;
		}
		if (c.getSkills().getLifepoints() < 1) {
			return;
		}
		if (c.isLockActions()) {
			return;
		}

//		 System.out.println("Food delay "+(System.currentTimeMillis() - c.anyDelay2));
//		 c.anyDelay2 = System.currentTimeMillis();

		// if (System.currentTimeMillis() - c.foodDelay >= 1550 &&
		// c.playerLevel[3] > 0) {
		c.getCombat().resetPlayerAttack();
		c.setAttackTimer(c.getAttackTimer() + 2);// c.getCombat().getAttackDelay(ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]).toLowerCase());

		final int maxHp;
		int heal;
		switch (food) {
			case SUMMER_PIE:
			case HALF_SUMMER_PIE:
				c.getSkills().updateLevel(Skills.AGILITY, 5);
				maxHp = c.calculateMaxLifePoints();
				heal = food.getHeal();
				break;
			case ROCKTAIL:
				maxHp = c.calculateMaxLifePoints() + 14;
				heal = food.getHeal();
				break;
			case LOBSTER:
				c.getAchievement().eatLobsters();
				maxHp = c.calculateMaxLifePoints();
				heal = food.getHeal();
				break;
			case ANGLERFISH:
				int hp = c.calculateMaxLifePoints();
				heal = getHealMod(hp);
				int boostHp = c.isInCombatDelay() && c.isInCombatWithPlayer() ? 0 : heal;
				maxHp = hp + boostHp;
				break;
			case BANDAGES:
				c.getDotManager().clearPoison();
				c.getDotManager().clearVenom();
				maxHp = c.calculateMaxLifePoints();
				heal = c.getSkills().getMaximumLifepoints() / 10;
				break;
			default:
				maxHp = c.calculateMaxLifePoints();
				heal = food.getHeal();
				break;
		}

		if (c.getSeasonalData().containsPowerUp(PowerUpData.MOVEMENT)) {
			heal += heal * 0.2f;
		}

		c.startAnimation(829);
		
		if (food.getSecondId() > 0)
			c.getItems().replaceItemSlot(food.getSecondId(), slot);
		else
			c.getItems().deleteItemInOneSlot(id, slot, 1);

		if (c.getSkills().getLifepoints() < maxHp) {
			if (c.getSkills().getLifepoints() + heal > maxHp) {
				c.getAchievement().heal50000HP(heal - (maxHp - c.getSkills().getLifepoints()));
			} else {
				c.getAchievement().heal50000HP(heal);
			}

			c.getSkills().updateLevel(Skills.HITPOINTS, heal, maxHp);
		}

		c.setFoodTick(food.getTicks());
		if (c.getSeasonalData().containsPowerUp(PowerUpData.EATING)) {
			c.getSkills().incrementPrayerPoints(1);
		}

		c.getPacketSender().playSound(Sounds.EAT);
		c.sendMessage("You eat the " + food.getName() + ".");
	}

	private static int getHealMod(int hitpoints) {
		int constant = hitpoints < 24 ? 2 : hitpoints >= 25 && hitpoints <= 49 ? 4 : hitpoints >= 50 && hitpoints <= 74 ? 6 : hitpoints >= 75 && hitpoints <= 92 ? 8 : 13;
		return (int) (Math.floor(hitpoints / 10) + constant);
	}

	public static void eatKaramb(final Client c, int id, int slot) {
		if (c.duelRule[DuelRules.NO_FOOD]) {
			c.sendMessage("You may not eat in this duel.");
			return;
		}
		if (c.clanWarRule[CWRules.NO_FOOD]) {
			c.sendMessage("Food disabled in this war.");
			return;
		}
		if (c.isDead()) {
			return;
		}
		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			return;
		}
		if (!canEatKaram(c)) {
			// c.sendMessage("Already eating");
			return;
		}
		if (c.getSkills().getLifepoints() < 1) {
			return;
		}
		if (c.isLockActions()) {
			return;
		}
		// System.out.println("Food delay "+(System.currentTimeMillis() -
		// c.foodDelay));
		// c.foodDelay = System.currentTimeMillis();

		// if (System.currentTimeMillis() - c.foodDelay >= 1550 &&
		// c.playerLevel[3] > 0) {
		final int maxHp = c.calculateMaxLifePoints();// c.hasFullTorva() ?
														// PlayerConstants.getLevelForXP(c.playerXP[3])
														// + 22 :
														// PlayerConstants.getLevelForXP(c.playerXP[3]);
		c.getCombat().resetPlayerAttack();

		if (c.getAttackTimer() > 0) {
			c.setAttackTimer(c.getAttackTimer() + 3);
		}

		c.startAnimation(829);
		c.getItems().deleteItemInOneSlot(id, slot, 1);

		int heal = 18;
		if (c.getSkills().getLifepoints() < maxHp) {
			if (c.getSkills().getLifepoints() + heal > maxHp) {
				c.getAchievement().heal50000HP(heal - (maxHp - c.getSkills().getLifepoints()));
			} else {
				c.getAchievement().heal50000HP(heal);
			}

			c.getSkills().updateLevel(Skills.HITPOINTS, heal, maxHp);
		}

		c.sendMessage("You eat a Cooked Karambwan.");
		if (c.getSeasonalData().containsPowerUp(PowerUpData.EATING)) {
			c.getSkills().incrementPrayerPoints(1);
		}

		c.setKaramTick();

		// }
	}

	public static FoodToEat forId(int id) {
		return food.get(id);
	}

	static {
		for (FoodToEat f : FoodToEat.values()) {
			food.put(f.getId(), f);
		}
	}
	
    public static void load() {
    	/* empty */
    }
    
	public static boolean isFood(int id) {
		return food.containsKey(id);
	}

}
