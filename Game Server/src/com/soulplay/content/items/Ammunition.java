package com.soulplay.content.items;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.combat.range.BoltEffect;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;

/**
 * Represents range ammunition types.
 * @author Emperor
 */
public final class Ammunition {

	/**
	 * The ammunition mapping.
	 */
	private static final Map<Integer, Ammunition> AMMUNITION = new HashMap<Integer, Ammunition>();

	/**
	 * The ammunition item id.
	 */
	private final int itemId;

	/**
	 * The start graphics.
	 */
	private final Graphic startGraphics;

	/**
	 * The start graphics when using Dark bow.
	 */
	private final Graphic darkBowGraphics;

	/**
	 * The projectile.
	 */
	private final Projectile projectile;

	/**
	 * The poison damage.
	 */
	private final int poisonDamage;

	/**
	 * The bolt effect.
	 */
	private BoltEffect effect;

	/**
	 * Constructs a new {@code Ammunition} object.
	 * @param itemId The item id.
	 * @param startGraphics The start graphics.
	 * @param darkBowGraphics The dark bow start graphics.
	 * @param projectile The projectile.
	 * @param poisonDamage The poison damage the ammunition can do.
	 */
	public Ammunition(int itemId, Graphic startGraphics, Graphic darkBowGraphics, Projectile projectile, int poisonDamage) {
		this.itemId = itemId;
		this.startGraphics = startGraphics;
		this.darkBowGraphics = darkBowGraphics;
		this.poisonDamage = poisonDamage;
		this.projectile = projectile;
	}

	/**
	 * Gets the ammunition mapping.
	 * @return The mapping.
	 */
	public static Map<Integer, Ammunition> getAmmunition() {
		return AMMUNITION;
	}

	/**
	 * Gets an ammunition object from the mapping.
	 * @param id The ammo id.
	 * @return The ammunition object.
	 */
	public static final Ammunition get(int id) {
		return AMMUNITION.get(id);
	}

	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @return the startGraphics
	 */
	public Graphic getStartGraphics() {
		return startGraphics;
	}

	/**
	 * @return the darkBowGraphics
	 */
	public Graphic getDarkBowGraphics() {
		return darkBowGraphics;
	}

	/**
	 * @return the projectile
	 */
	public Projectile getProjectile() {
		return projectile;
	}

	/**
	 * @return the poisonDamage
	 */
	public int getPoisonDamage() {
		return poisonDamage;
	}

	/**
	 * Gets the effect.
	 * @return the effect
	 */
	public BoltEffect getEffect() {
		return effect;
	}

	/**
	 * Sets the baeffect.
	 * @param effect the effect to set.
	 */
	public void setEffect(BoltEffect effect) {
		this.effect = effect;
	}

	@Override
	public String toString() {
		return "Ammunition [itemId=" + itemId + ", startGraphics=" + startGraphics + 
				", darkBowGraphics=" + darkBowGraphics + ", projectile=" + projectile +
				", poisonDamage=" + poisonDamage + ", effect=" + "effect" + "]";
	}
}