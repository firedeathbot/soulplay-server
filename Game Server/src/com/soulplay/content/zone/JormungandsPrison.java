package com.soulplay.content.zone;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class JormungandsPrison {
	
	public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 37417:
				climbSteps(c);
				return true;
			case 37410:
				enterCave(c);
				return true;
			case 37411:
				exitSteps(c);
				return true;
			default:
				return false;
		}
	}
	
	public static void climbSteps(Player p) {
		
		if (p.getX() == 8871 && p.getY() == 10403) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() - 3, p.getCurrentLocation().getY()));
		} else if (p.getX() == 8868 && p.getY() == 10403) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() + 3, p.getCurrentLocation().getY()));
		}
	}
	
	public static void exitSteps(Player p) {
		if (p.getX() == 8861 && p.getY() == 10417) {
			p.performingAction = true;
			p.getPacketSender().setBlackout(0x80);
			p.pulse(() -> {
				p.getPacketSender().setBlackout(0);
				p.getPA().movePlayer(8865, 4010, 0);
				p.performingAction = false;
			}, 2);
		}
	}
	
	public static void enterCave(Player p) {
		if (p.getY() == 4010) {
			p.performingAction = true;
			p.getPacketSender().setBlackout(0x80);
			p.pulse(() -> {
				p.getPacketSender().setBlackout(0);
				p.getPA().movePlayer(8861, 10417, 0);
				p.performingAction = false;
			}, 2);
		}
	}

}
