package com.soulplay.content.zone.gielinor;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class StrongholdSlayerCave {
	
	 private static final int CRAWL_ANIM = 844;
	
	 public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 4006:
				if (o.getX() == 2429) {
					enter(c);
				}
				return true;
			case 27257:
			case 27258:
				leave(c);
				return true;
			case 30175:
			case 30174:
				tunnel(c);
				return true;
			default:
				return false;
		}
	}

    public static void enter(Player p) {
    	if (p.getX() != 2431 || p.getY() != 3425) {
    	    p.getPA().playerWalk(2431, 3425);
    	    p.pulse(() -> {
    	    	p.startAnimation(CRAWL_ANIM);
    			p.pulse(() -> {
    				p.getPA().movePlayer(Location.create(8829, 9824));
    			}, 1);
			}, 4);
    	} else {
    		p.startAnimation(CRAWL_ANIM);
			p.pulse(() -> {
				p.getPA().movePlayer(Location.create(8829, 9824));
			}, 1);
    	}
    }
	public static void leave(Player p) {
		if (p.getX() == 8829) {
			p.startAnimation(CRAWL_ANIM);
			p.pulse(() -> {
				p.getPA().movePlayer(Location.create(2431, 3425));
			}, 1);
		}
		
	}
	
	public static void tunnel(Player p) {
		if (p.getSkills().getLevel(Skills.AGILITY) < 72) {
			p.getDialogueBuilder()
			.sendStatement("You need agility level 72 to get through there.")
			.executeIfNotActive();
			return;
		}
		if (p.getX() == 8829) {
			p.startAnimation(CRAWL_ANIM);
			p.pulse(() -> {
				p.getPA().movePlayer(Location.create(p.getX() + 6, p.getY()));
			}, 1);
		} else if (p.getX() == 8835) {
			p.startAnimation(CRAWL_ANIM);
			p.pulse(() -> {
				p.getPA().movePlayer(Location.create(p.getX() - 6, p.getY()));
			}, 1);
		}
		
	}

}
