package com.soulplay.content.zone.kourend;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class LizardmanCaves {

	public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 30384:
			case 30385:
			case 30383:
			case 30382:
				squeezeThroughCrevice(c);
				return true;
			default:
				return false;
		}
	}

    public static void squeezeThroughCrevice(Player p) {
		if (p.getX() == 7719 || p.getX() == 7695) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() + 4, p.getCurrentLocation().getY(), p.getZ()));
		} else if (p.getX() == 7723 || p.getX() == 7699) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() - 4, p.getCurrentLocation().getY(), p.getZ()));
		}
		
		if (p.getY() == 9957 || p.getY() == 9960) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX(), p.getCurrentLocation().getY() - 4, p.getZ()));
		} else if (p.getY() == 9953 || p.getY() == 9956) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX(), p.getCurrentLocation().getY() + 4, p.getZ()));
		}
	}

}
