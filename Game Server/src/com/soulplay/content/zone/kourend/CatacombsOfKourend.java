package com.soulplay.content.zone.kourend;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;

public class CatacombsOfKourend {

	public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 28893:
				jumpToStone(c, o);
				return true;
			case 28892:
				squeezeThroughCrack(c);
				return true;
			default:
				return false;
		}
	}
	
	public static void jumpToStone(Player p, GameObject o) {
		if (p.getY() > o.getY()) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(0, -1));
			p.pulse(() -> {
				ForceMovementMask mask = ForceMovementMask.createMask(0, direction.getStepY(), 1, Animation.getOsrsAnimId(741), direction);
                mask.addStartAnim(Animation.getOsrsAnimId(741));
                p.setForceMovement(mask);
			}, 1);
		} else if (o.getY() > p.getY()) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(0, 1));
			p.pulse(() -> {
				ForceMovementMask mask = ForceMovementMask.createMask(0, direction.getStepY(), 1, Animation.getOsrsAnimId(741), direction);
                mask.addStartAnim(Animation.getOsrsAnimId(741));
                p.setForceMovement(mask);
			}, 1);
		} else if (p.getX() > o.getX()) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(-1, 0));
			p.pulse(() -> {
				ForceMovementMask mask = ForceMovementMask.createMask(direction.getStepX(), 0, 1, Animation.getOsrsAnimId(741), direction);
                mask.addStartAnim(Animation.getOsrsAnimId(741));
                p.setForceMovement(mask);
			}, 1);
		} else if (o.getX() > p.getX()) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(1, 0));
			p.pulse(() -> {
				ForceMovementMask mask = ForceMovementMask.createMask(direction.getStepX(), 0, 1, Animation.getOsrsAnimId(741), direction);
                mask.addStartAnim(Animation.getOsrsAnimId(741));
                p.setForceMovement(mask);
			}, 1);
		}
	}
	
	public static void squeezeThroughCrack(Player p) {
		if (p.getPlayerLevel()[Skills.AGILITY] < 70) {
			p.sendMessage("You must have an agility level of at least 70 to use this shortcut.");
			return;
		}
			
		if (p.getX() == 8116 && p.getY() == 10056) {
			p.getPA().movePlayer(Location.create(8106, 10078));
		} else if (p.getX() == 8106 && p.getY() == 10078) {
			p.getPA().movePlayer(Location.create(8116, 10056));
		}
	}
}
