package com.soulplay.content.zone.kourend;

import com.soulplay.Config;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class WoodcuttingGuild {

	public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 28857:
			case 28858:
				climbUpRopeLadder(c);
				return true;
			case 28851:
			case 28852: // wc guild doors
				if (c.absX > o.getX()) {
					c.getPA().walkTo(-1, 0, true);
				} else {
					if (c.getSkills().getLevel(Skills.WOODCUTTING) < 60) {
						c.sendMessage("You need a Woodcutting level of 60 to enter the guild.");
						return true;
					}

					c.getPA().walkTo(1, 0, true);
				}
				return true;
			case 26254:
				c.getPacketSender().sendFrame126("The Bank of " + Config.SERVER_NAME + " - Deposit Box", 7421);
				c.getPacketSender().sendFrame248(4465, 197);
				c.getItems().resetItems(7423);
				return true;
			default:
				return false;
		}
	}

    public static void climbUpRopeLadder(Player p) {
		if ((p.getX() == 7965 || p.getX() == 7966) && p.getZ() == 0) {
			p.pulse(() -> {
			    p.startAnimation(828);
				p.pulse(() -> {
					p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() + 7967 - p.getX(), p.getCurrentLocation().getY(), 1));
				}, 2);
			}, 1);
		} else if (p.getX() == 7967 && p.getZ() == 1) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() - 1, p.getCurrentLocation().getY(), 0));
		}
		
		if ((p.getX() == 7976 || p.getX() == 7975) && p.getZ() == 0) {
			p.pulse(() -> {
			    p.startAnimation(828);
				p.pulse(() -> {
					p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() + 7974 - p.getX(), p.getCurrentLocation().getY(), 1));
				}, 2);
			}, 1);
		} else if (p.getX() == 7974 && p.getZ() == 1) {
			p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX() + 1, p.getCurrentLocation().getY(), 0));
		}
	}

}
