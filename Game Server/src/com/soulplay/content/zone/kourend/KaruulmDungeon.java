package com.soulplay.content.zone.kourend;

import com.soulplay.content.npcs.impl.bosses.alchhydra.AlchemicalHydraInstance;
import com.soulplay.content.player.skills.slayer.Assignment;
import com.soulplay.content.player.skills.slayer.Master;
import com.soulplay.content.player.skills.slayer.SlayerTask;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.Misc;

public class KaruulmDungeon {

	public static boolean objectSecondClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 34554:
			case 34553:
				enterHydraRoom(c);
				return true;
			default:
				return false;
		}
	}

	public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 34544:
				climbOverRocks(c);
				return true;
			case 34548:
				hydraRock(c, o);
				return true;
			case 34515:
				jumpLavaGap(c);
				return true;
			case 34530:
				c.getPA().movePlayerDelayed(7734, 10206, 1, 828, 1);
				return true;
			case 34531:
				c.getPA().movePlayerDelayed(7729, 10206, 0, 827, 1);
				return true;
			case 34554:
			case 34553:
				hydraDoor(c, o);
				return true;
			default:
				return false;
		}
	}

	private static void enterHydraRoom(Player player) {
		player.getPA().movePlayer(player.getCurrentLocation().transform(1, 0));

		if (!player.insideInstance() || !(player.getInstanceManager() instanceof AlchemicalHydraInstance)) {
			return;
		}

		// Assuming player is in instance, attack him on enter
		AlchemicalHydraInstance instance = (AlchemicalHydraInstance) player.getInstanceManager();
		NPC hydra = instance.getHydra();
		if (hydra != null) {
			hydra.setKillerId(player.getId());
		}
	}

	private static void hydraDoor(Player player, GameObject gameObject) {
		if (player.getX() > gameObject.getX()) {
			player.getDialogueBuilder().sendStatement("This door is jammed.").execute();
			return;
		}

		player.getDialogueBuilder().sendOption("Enter the laboratory? The door is notorious for getting jammed.", "Yes.", () -> {
			enterHydraRoom(player);
		}, "No.", () -> {}).execute();
	}

	private static void hydraRock(Player player, GameObject gameObject) {
		if (player.getY() < gameObject.getY()) {
			hydraRockNorth(player);
		} else {
			hydraRockSouth(player);
		}
	}

	private static void hydraRockSouth(Player player) {
		Location dest = player.getCurrentLocation().transform(0, -2);
		dest.setZ(0);

		Direction direction = Direction.getDirection(player.getCurrentLocation(), dest);
		ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 2, 2, 839, direction);
		mask1.addStartAnim(839);
		mask1.setTeleLoc(dest);
		player.setForceMovement(mask1);
	}

	private static void hydraRockNorth(Player player) {
		//SlayerTask task = new SlayerTask(Assignment.HYDRAS_BY_KONAR, Master.KONAR_QUO_MATEN);
		//player.setSlayerTask(task);

		 SlayerTask slayerTask = player.getSlayerTask();
		 if (slayerTask == null) {
			 player.getDialogueBuilder().sendStatement("You must have a Hydras task by to enter this room.").execute();
			 return;
		 }

		 Assignment assignemnt = slayerTask.getAssignment();
		 if (assignemnt == null || assignemnt != Assignment.HYDRAS_BY_KONAR) {
			 player.getDialogueBuilder().sendStatement("You must have a Hydras task by to enter this room.").execute();
			 return;
		 }

		 Direction direction = Direction.getDirection(player.getCurrentLocation(), player.getCurrentLocation().transform(0, 2));
		 ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 2, 2, 839, direction);
		 mask1.addStartAnim(839);
		 player.setForceMovement(mask1);

		 player.startActionChecked(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				 new AlchemicalHydraInstance(player);
				 container.stop();
			}

		}, 1);
	}

	private static void jumpForwardChecked(Player player, Runnable action) {
		int bootsId = player.playerEquipment[PlayerConstants.playerFeet];
		if (bootsId == 123037 || bootsId == 122951 || bootsId == 121643) {
			action.run();
			return;
		}

		player.getDialogueBuilder().sendStatement("Warning: The heat of the ground beyond this point can burn you as", "you walk upon it. It is recommended you wear appropriate boots for", "this.").sendOption("Proceed regardless?", "Yes.", action, "No.", () -> {}).execute();
	}

	public static void climbOverRocks(Player p) {
		if (p.getX() == 7703) {
			jumpForwardChecked(p, () -> {
				Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(-2, 0));
				p.pulse(() -> {
					ForceMovementMask mask1 = ForceMovementMask.createMask(direction.getStepX() * 2, 0, 2, 839, direction);
	                mask1.addStartAnim(839);
	                p.setForceMovement(mask1);
				}, 1);
			});
		} else if (p.getX() == 7701) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(2, 0));
			p.pulse(() -> {
				ForceMovementMask mask1 = ForceMovementMask.createMask(direction.getStepX() * 2, 0, 2, 839, direction);
                mask1.addStartAnim(839);
                p.setForceMovement(mask1);
			}, 1);
		}
		
		if (p.getY() == 10214) {
			jumpForwardChecked(p, () -> {
				Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(0, 2));
				p.pulse(() -> {
					ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 2, 2, 839, direction);
	                mask1.addStartAnim(839);
	                p.setForceMovement(mask1);
				}, 1);
			});
		} else if (p.getY() == 10216) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(0, -2));
			p.pulse(() -> {
				ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 2, 2, 839, direction);
                mask1.addStartAnim(839);
                p.setForceMovement(mask1);
			}, 1);
		}
		
		if (p.getX() == 7720) {
			jumpForwardChecked(p, () -> {
				Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(2, 0));
				p.pulse(() -> {
					ForceMovementMask mask1 = ForceMovementMask.createMask(direction.getStepX() * 2, 0, 2, 839, direction);
	                mask1.addStartAnim(839);
	                p.setForceMovement(mask1);
				}, 1);
			});
		} else if (p.getX() == 7722) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(-2, 0));
			p.pulse(() -> {
				ForceMovementMask mask1 = ForceMovementMask.createMask(direction.getStepX() * 2, 0, 2, 839, direction);
                mask1.addStartAnim(839);
                p.setForceMovement(mask1);
			}, 1);
		}
	}
	
	public static void jumpLavaGap(Player p) {
		if (p.getY() == 10169 || p.getY() == 10251) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(0, 7));
			p.pulse(() -> {
				ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 7, 2, 839, direction);
                mask1.addStartAnim(839);
                p.setForceMovement(mask1);
			}, 1);
		} else if (p.getY() == 10176 || p.getY() == 10258) {
			Direction direction = Direction.getDirection(p.getCurrentLocation(), p.getCurrentLocation().transform(0, -7));
			p.pulse(() -> {
				ForceMovementMask mask1 = ForceMovementMask.createMask(0, direction.getStepY() * 7, 2, 839, direction);
                mask1.addStartAnim(839);
                p.setForceMovement(mask1);
			}, 1);
		}
		
	}
	
}
