package com.soulplay.content.zone.fossilisland;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

public class LithkrenVault {

	public static boolean objectFirstClick(Client c, GameObject o) {
		switch (o.getId()) {
			case 32113:
				climbStaircase(c);
				return true;
			case 32132:
			case 32117:
				enterGrandiosaDoor(c);
				return true;
			case 32153:
				passBarrier(c);
				return true;
			default:
				return false;
		}
	}

	public static void climbStaircase(Player p) {
		
		if (p.getY() == 10468) {
			p.pulse(() -> {
				p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX(), p.getCurrentLocation().getY() + 5));
			}, 1);
		} else if (p.getY() == 10473) {
			p.pulse(() -> {
				p.getPA().movePlayer(Location.create(p.getCurrentLocation().getX(), p.getCurrentLocation().getY() - 5));
			}, 1);
		}	
	}
	
	public static void enterGrandiosaDoor(Player p) {
		
		if (p.getY() == 10481) {
			p.performingAction = true;
			p.getPacketSender().setBlackout(0x80);
			p.pulse(() -> {
				p.getPacketSender().setBlackout(0);
				p.getPA().movePlayer(7968, 5061, 0);
				p.performingAction = false;
			}, 2);
		} else if (p.getY() == 5061) {
			p.performingAction = true;
			p.getPacketSender().setBlackout(0x80);
			p.pulse(() -> {
				p.getPacketSender().setBlackout(0);
				p.getPA().movePlayer(9949, 10481, 0);
				p.performingAction = false;
			}, 2);
		}
	}
	
    public static void passBarrier(Player p) {
		
		if (p.getX() == 7973) {
			p.getPA().walkTo(2, 0, true);
		} else if (p.getX() == 7975) {
			p.getPA().walkTo(-2, 0, true);
		} else if (p.getX() == 7962) {
			p.getPA().walkTo(-2, 0, true);
		} else if (p.getX() == 7960) {
			p.getPA().walkTo(2, 0, true);
		} 
		
		if (p.getY() == 5089) {
			p.getPA().walkTo(0, -2, true);
		} else if (p.getY() == 5087) {
			p.getPA().walkTo(0, 2, true);
		}
	}
	
}
