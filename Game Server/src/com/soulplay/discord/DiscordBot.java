package com.soulplay.discord;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author Jire
 */
public final class DiscordBot {
	
	private static final String TOKEN = "NTYzNzYxNzU3NjA1MDY4ODI2.XM0-rg.dM2rgKEamEeTYuWbGwQuMlGTJjU";
	private static final long GUILD_ID = 202555536640573440L;
	private static final long CHANNEL_ID = 484881628808609793L;
	
	private static final Executor executor = Executors.newSingleThreadExecutor();
	
	private static JDA jda;
	
	private DiscordBot() {
		throw new UnsupportedOperationException();
	}
	
	public static void init() {
		try {
			jda = new JDABuilder(AccountType.BOT)
					.setToken(TOKEN)
					.addEventListener(new DiscordBotEventListener())
					.build()
					.awaitReady();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendMessage(CharSequence message) {
		if (jda == null) {
			return;
		}
		
		executor.execute(() -> {
			Guild guild = jda.getGuildById(GUILD_ID);
			if (guild == null) {
				return;
			}
			
			TextChannel channel = guild.getTextChannelById(CHANNEL_ID);
			if (channel == null) {
				return;
			}
			
			channel
					.sendMessage(message)
					.submit();
		});
	}
	
	public static JDA getJDA() {
		return jda;
	}
	
}
