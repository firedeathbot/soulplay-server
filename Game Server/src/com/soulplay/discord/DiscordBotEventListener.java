package com.soulplay.discord;

import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.EventListener;

/**
 * @author Jire
 */
final class DiscordBotEventListener implements EventListener {
	
	@Override
	public void onEvent(Event event) {
		if (event instanceof ReadyEvent) {
			//System.out.println("Discord integration is ready.");
		}
	}
	
}
