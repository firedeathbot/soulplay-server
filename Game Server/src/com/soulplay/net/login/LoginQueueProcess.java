package com.soulplay.net.login;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class LoginQueueProcess {

	public LoginQueueProcess() throws InterruptedException {
		AtomicBoolean stop = new AtomicBoolean(false);
		Executors.newSingleThreadExecutor().execute(() -> {
			try {
				while (!stop.get()) {
					Thread.sleep(5000);
					// Server.decreaseLoginQueue();
					RS2LoginProtocol.clearQueue();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

}
