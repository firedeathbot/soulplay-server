package com.soulplay.net.login;

import java.util.List;

import com.soulplay.net.InPacket;
import com.soulplay.net.PacketSize;
import com.soulplay.net.packet.PacketHandler;
import com.soulplay.util.ISAACCipher;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public final class RS2Decoder extends ByteToMessageDecoder {

	private static final int VAR_BYTE = -1;

	private final ISAACCipher cipher;
	
	private int opcode;
	private int size;

	private State state = State.OPCODE;
	
	public RS2Decoder(ISAACCipher cipher) {
		this.cipher = cipher;
	}
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if (state == State.OPCODE) {
			decodeOpcode(in, out);
		} else if (state == State.SIZE) {
			decodeSize(in);
		} else {
			decodePayload(in, out);
		}
	}

	private void decodeOpcode(ByteBuf in, List<Object> out) {
		if (in.isReadable()) {
			opcode = (in.readByte() - cipher.getNextValue() & 0xFF);
			size = PacketSize.PACKET_SIZES[opcode];

			if (size == VAR_BYTE) {
				state = State.SIZE;
			} else if (size > 0) {
				state = State.PAYLOAD;
			} else {
				out.add(new InPacket(opcode, new byte[0]));
				state = State.OPCODE;
			}
		}
	}

	private void decodeSize(ByteBuf in) {
		if (in.isReadable()) {
			size = in.readUnsignedByte();
			if (size != 0) {
				state = State.PAYLOAD;
			}
		}
	}

	private void decodePayload(ByteBuf in, List<Object> out) {
		if (in.isReadable(size)) {
			if (opcode >= 0 && opcode < PacketHandler.packetId.length) {
				if (PacketHandler.packetId[opcode] != null) {
					final byte[] payload = new byte[size];
					in.readBytes(payload);
					out.add(new InPacket(opcode, payload));
				} else {
					System.out.println(String.format("No listener for client -> server packet=%d", opcode));
				}
			}
			state = State.OPCODE;
		}
	}

	private enum State {
		OPCODE,
		SIZE,
		PAYLOAD
	}
	
}
