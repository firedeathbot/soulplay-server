package com.soulplay.net.login;

import com.soulplay.net.ChannelHandler;
import com.soulplay.util.ISAACCipher;
import io.netty.channel.Channel;

public final class LoginServerRequest {
	
	public final Channel channel;
	public final int lastReturnCode;
	public final String pass;
	public final String uuid;
	public final ISAACCipher outCipher;
	public final String connectedFrom;
	public final int version;
	public final ChannelHandler channelHandler;
	public final long time;
	
	public LoginServerRequest(Channel channel, int lastReturnCode, String pass,
	                          String uuid, ISAACCipher outCipher, String connectedFrom,
	                          int version, ChannelHandler channelHandler, long time) {
		this.channel = channel;
		this.lastReturnCode = lastReturnCode;
		this.pass = pass;
		this.uuid = uuid;
		this.outCipher = outCipher;
		this.connectedFrom = connectedFrom;
		this.version = version;
		this.channelHandler = channelHandler;
		this.time = time;
	}
	
}
