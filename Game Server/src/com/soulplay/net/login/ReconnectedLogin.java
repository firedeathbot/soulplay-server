package com.soulplay.net.login;

public class ReconnectedLogin {

	private long time;

	private String playerName;

	public ReconnectedLogin(String playerName) {
		this.setPlayerName(playerName);
		this.time = System.currentTimeMillis();
	}

	public boolean containsName(String name) {
		if (name.equals(this.playerName)
				&& System.currentTimeMillis() - time < 10000) {
			return true;
		}
		return false;
	}

	public boolean expired() {
		return System.currentTimeMillis() - time > 10000;
	}

	public String getPlayerName() {
		return playerName;
	}

	public long getTime() {
		return time;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
