package com.soulplay.net.login;

import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.ChannelHandler;
import com.soulplay.net.Connection;
import com.soulplay.net.PacketBuilder;
import com.soulplay.net.login.spam.LoginAttemptHandler;
import com.soulplay.net.loginserver.GameServerPacketOpcode;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.loginserver.LoginServerPacket;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.ISAACCipher;
import com.soulplay.util.Misc;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class RS2LoginProtocol extends ByteToMessageDecoder {
	
	private final ChannelHandler channelHandler;
	
	public RS2LoginProtocol(ChannelHandler channelHandler) {
		this.channelHandler = channelHandler;
	}
	
	public static volatile boolean blockLogins = false;
	private static volatile long loginPause;
	
	private static final BigInteger RSA_MODULUS = new BigInteger("99977468824052788080849397977455972358018352050243140053654716971832573830973963058339294779679647370073315026578142713517643650475242966968026411179559915799453305287949071368070883142111718980245261875245072875729823177699880753218892622362474129739665672073227023119226736264300665353247408123527160756377");
	
	private static final BigInteger RSA_EXPONENT = new BigInteger("41051523354063514156211869624385312360258691329661762040432861341105246834482953841340165441219148125923873649468511229088298070315833624381793349174389379966990458765140463736693607358448056129820420933548282849380089692956495257420779579160226650520908264922699516599142625016109113011009503402969764806753");
	
	private static final int HANDSHAKE = 0;
	
	private static final int CONNECTION_TYPE = 1;

	private static final int PAYLOAD = 2;

	private static final int IGNORE = 3;
	
	public static final long LOGIN_TIMEOUT = 15_000;
	
	private static ArrayList<String> loginQueueSkip = new ArrayList<>();
	
	/**
	 * The pattern compiler.
	 */
	private static final Pattern PATTERN = Pattern.compile("[A-Za-z0-9 ]+");
	
	public static synchronized void addToLoginQueue(String name) {
		loginQueueSkip.add(name);
	}
	
	public static synchronized void clearQueue() {
		loginQueueSkip.clear();
	}
	
	public static synchronized int getLoginQueuePosition(String name) {
		if (Config.isDev(name))
			return -1;
		return loginQueueSkip.indexOf(name);
	}
	
	public static synchronized int getLoginQueueSize() {
		return loginQueueSkip.size();
	}
	
	public static synchronized ArrayList<String> getLoginQueueSkip() {
		return loginQueueSkip;
	}
	
	private static boolean loginRules(Channel channel, ISAACCipher inCipher, ISAACCipher outCipher, int version, String name, String pass, int clientVersion, final String UUID, final String ipAddress, boolean isReconnecting) {
//		if (LoginServerConnection.lsInUse.get() == true) {
//			sendReturnCode(channel, 24);
//			return false;
//		}
		
		if (Server.getUpdateServer() || !Server.isLaunched()) {
			sendReturnCode(channel, 14);
			return false;
		}
		if (PlayerHandler.getPlayerCount() >= Config.MAX_PLAYERS) { // this is handled on loginserver also
			sendReturnCode(channel, 7);
			return false;
		}
		if (name.length() < 1 || name.length() > 12) {
			sendReturnCode(channel, 3);
			return false;
		}
		if (blockLogins && !Config.canLogin(name)) {
			sendReturnCode(channel, 12);
			return false;
		}
		if (pass.length() > 20 || !validRegex(pass) || !validRegex(name) || name.endsWith(" ") || name.startsWith(" ") || name.contains("  ")) {
			sendReturnCode(channel, 3);
			return false;
		}
		
		// start of cheat client shit
		if (UUID.startsWith("LOFVMware")) {
			sendReturnCode(channel, 4);
			return false;
		}
		
		if (UUID.contains("\\")) {
			sendReturnCode(channel, 4);
			return false;
		}
		
		if (UUID.contains("RuneA")) {
			Server.getSlackApi().call(SlackMessageBuilder.buildHackerMessage(name));
			sendReturnCode(channel, 4);
			return false;
		}
		
		if (UUID.contains("rune agent")) {
			Server.getSlackApi().call(SlackMessageBuilder.buildHackerMessage(name));
			sendReturnCode(channel, 4);
			return false;
		}
		
		if (UUID.contains("C:\\")) {
			Server.getSlackApi().call(SlackMessageBuilder.buildHackerMessage(name));
			sendReturnCode(channel, 4);
			return false;
		}
		
		if (!UUID.startsWith("LOF") && !UUID.endsWith("LOF3")) {
			// System.out.println(
			// "UUID has been changed by cheat client. UUID:"
			// + UUID);
			// System.out.println("UUID changer name: " + name);
			// Connection.spammingConnections.add(ip);
			sendReturnCode(channel, 6);
			return false;
		}
		// end of cheat client shit
		
		if (Connection.spammingConnections.contains(ipAddress)) {
			sendReturnCode(channel, 4);
			return false;
		}
		
		// new filter for blocking spammed connections
		if (!Config.SERVER_DEBUG) {
			if (LoginAttemptHandler.maxedLoginAttempts(ipAddress) || LoginAttemptHandler.maxedWrongPasswordsPerHour(ipAddress)
					/*|| LoginAttemptHandler.maxedWrongPasswordAttempts(ipAddress)*/) {
				sendReturnCode(channel, 26);
				return false;
			}
		}
		
		LoginAttemptHandler.addHourlyLogin(name, ipAddress, UUID);
		
		if (name.length() <= 12 && PlayerHandler.isPlayerOnline(name)) { // login
			// server
			// handles
			// this
			sendReturnCode(channel, 5);
			return false;
		}
		
		// login queue start
		int loginQueuePosition = -1;//getLoginQueuePosition(ipAddress);
		if (isReconnecting) {
			loginQueuePosition = -1;
		}
		if (loginQueuePosition != -1) {
			sendReturnCodeLoginQueue(channel, 24, loginQueuePosition + 1);
			return false;
		}
		
		if (/*!isReconnecting && */loginQueuePosition != -1) {
			if (System.currentTimeMillis() - loginPause < 100) {
//				if (!getLoginQueueSkip().contains(ipAddress)) {
//					addToLoginQueue(ipAddress);
//				}
				sendReturnCodeLoginQueue(channel, 24, /*getLoginQueuePosition(ipAddress) +*/ 1);
				return false;
			}
		}
		
		//if (!isReconnecting) {
			loginPause = System.currentTimeMillis();
		//}
		// login queue end
		
		return true;
	}
	
	private void login(Channel channel, ISAACCipher inCipher, ISAACCipher outCipher, int version, String name, String pass, int clientVersion, final String UUID, final String ipAddress, boolean isReconnecting) {
		int returnCode = 2;
		
		// System.out.println("isReconnecting:"+isReconnecting);
		
		
		if (!loginRules(channel, inCipher, outCipher, version, name, pass, clientVersion, UUID, ipAddress, isReconnecting))
			return;
		
		if (isReconnecting) {
			
			// ---------------------------------------- reconnecting start
			// -----------------------------------------//
			
			
			isReconnecting = false; // disable reconnecting stuff
			
			
			// ---------------------------------reconnecting end
			// ---------------------------------------------------//
		}
		
		
		// if (name.toLowerCase().startsWith("mod ") ||
		// name.toLowerCase().startsWith("admin ")) {
		// returnCode = 3;
		// sendReturnCode(channel, returnCode);
		// return;
		// }
		
		// if (!Config.SERVER_DEBUG && Connection.isNamedBanned(name)) {
		// returnCode = 4;
		// }
		
		// if (name.equalsIgnoreCase("lesik")) {
		// System.out.println("Lesik IP "+ipAddress);
		// }
		// if (name.equalsIgnoreCase("The last one")) {
		// System.out.println("IP "+ipAddress);
		// }
		
		// TODO: add login queue here
		
		// int loginQueue = Server.getLoginQueue();
		// System.out.println("Login queue "+loginQueue);
		// if (Server.getLoginQueue() > 1) {
		// addToLoginQueue(name);
		// sendReturnCodeLoginQueue(channel, 24, loginQueue%254);
		// return;
		// }
		
		// if (getLoginQueuePosition(name) > 0) {
		// sendReturnCodeLoginQueue(channel, 24, getLoginQueueSize()%254);
		// return;
		// }
		
		// if (getLoginQueuePosition(name) == -1 && getLoginQueueSize() > 0) {
		// addToLoginQueue(name);
		// sendReturnCodeLoginQueue(channel, 24, getLoginQueueSize()%254);
		// return;
		// }
		//
		//
		//
		//// if (getLoginQueuePosition(name) == 0) {
		// removeFirstQueue();
		//// }
		// removeFromQueue(name);
		//
		//
		// if (getLoginQueueSize() > 300) {
		// clearQueue();
		// }
		
		int failedCount = Connection.getBankPinFailedAttemptAmount(name);
		
		if (failedCount > Config.BANK_PIN_FAIL_LIMIT) {
			returnCode = 4;
			sendReturnCode(channel, returnCode);
			return;
		}
		
		final boolean spammingIp = Connection.getLastIpConnecting().contains(ipAddress);
		
		if (!Config.SERVER_DEBUG && spammingIp) {
			returnCode = 16/* 9 */;
			sendReturnCode(channel, returnCode);
			return;
		}
		
		final boolean spammingUUID = Connection.getLastUUIDConnecting().contains(UUID);
		
		if (spammingUUID) {
			returnCode = 9;
			sendReturnCode(channel, returnCode);
			return;
		}
		
		if (!spammingIp) {
			Connection.getLastIpConnecting().add(ipAddress);
		}
		if (!spammingUUID) {
			Connection.getLastUUIDConnecting().add(UUID);
		}
		
		// System Serial Number20140620225720.000000-300 this is icejourney's
		// uuid needs to be covered in LOF
//		if ("LOFF9N0WU27918640D20151222155843.000000-300LOF3".equals(UUID)) {
//			Server.getSlackApi().call(SlackMessageBuilder.buildHackerMessage(name));
//		}
//
//		if ("LOFE1N0CX34042702820140227144427.000000-480LOF".equals(UUID)) {
//			Server.getSlackApi().call(SlackMessageBuilder.buildHackerMessage(name));
//		}
//
//		if ("71.59.189.21".equals(ipAddress)) {
//			Server.getSlackApi().call(SlackMessageBuilder.buildHackerMessage(name));
//		}
		
		
		if (returnCode != 2) {
			sendReturnCode(channel, returnCode);
			return;
		}
		
		
		if (returnCode == 2) {
			final long now = System.currentTimeMillis();
			final String loginName = name.toLowerCase();
			final Channel loginServerChannel = LoginServerConnection.instance().getChannel();
			final LoginServerRequest loginRequest = LoginServerConnection.instance().getNameToLoginRequests().get(loginName);
			if (loginRequest != null && now - loginRequest.time < LOGIN_TIMEOUT) {
				sendReturnCode(channel, 16);
				return;
			}
			if (isReconnecting) {
				returnCode = 33;
			} else if (loginServerChannel == null || !loginServerChannel.isActive()) {
				// Server.connectToLoginServer(); // reconnect?
				returnCode = 8;//doesn't even make sense
			} else {
				LoginServerConnection.instance().getNameToLoginRequests().put(loginName,
						new LoginServerRequest(channel, returnCode, pass, UUID, outCipher,
								ipAddress, version, channelHandler, now));

				final LoginServerPacket request = new LoginServerPacket(
						GameServerPacketOpcode.LOGIN_VERIFICATION);
				request.writeString(loginName);
				request.writeString(pass);
				request.writeString(ipAddress);
				request.writeString(UUID);
				loginServerChannel.writeAndFlush(request, loginServerChannel.voidPromise());
				return;
			}
		} else {
			// System.out.println("returncode:" + returnCode);
			
			if (returnCode == 3) { // wrong password
				LoginAttemptHandler.addWrongPassword(name, ipAddress, UUID);
				sendReturnCode(channel, 3);
				return;
			}
			
			if (returnCode == 21) {
				// cl = null;
				sendReturnCode21(channel, returnCode);
				return;
			}
			// cl = null;
			sendReturnCode(channel, returnCode);
			return;
		}
		
		// }
		
		// synchronized (PlayerHandler.lock) {
		// cl.initialize();
		// cl.initialized = true;
		// }
		sendReturnCode(channel, returnCode);
		return;
	}
	
	public static synchronized void removeFirstQueue() {
		if (loginQueueSkip.size() > 0) {
			loginQueueSkip.remove(0);
		}
	}
	
	public static synchronized void removeFromQueue(String name) {
		if (loginQueueSkip.size() > 0) {
			loginQueueSkip.remove(name);
		}
	}
	
	public static void sendReturnCode(final Channel channel, final int code) {
		channel.writeAndFlush(new PacketBuilder(channel.alloc()).put((byte) code).finish()).addListener(future -> channel.close());
	}
	
	public static void sendReturnCode21(final Channel channel, final int code) {
		channel.writeAndFlush(new PacketBuilder(channel.alloc()).put((byte) code).put((byte) 5).finish()).addListener(future -> channel.close());
	}
	
	public static void sendReturnCodeLoginQueue(final Channel channel, final int code, int queue) {
		channel.writeAndFlush(new PacketBuilder(channel.alloc()).put((byte) code).put((byte) queue).finish()).addListener(future -> channel.close());
	}
	
	/**
	 * Checks if a username is valid.
	 *
	 * @return {@code True} if so.
	 */
	public static boolean validRegex(final String username) {
		Matcher matcher = PATTERN.matcher(username);
		return matcher.matches();
		
	}
	
	private int state = HANDSHAKE;

	private int connectionType;

	private static final int LOGIN_PACKET_HEADER_SIZE = 47;

	private void decodeHandshake(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
		if (in.readableBytes() >= 3) {
			if (in.readUnsignedByte() != 14) {
				// System.out.println("Invalid login request: " + request);
				sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_REQUEST);
				return;
			}

			in.readUnsignedByte(); // hashed name

			if (in.readUnsignedByte() != 250) {
				sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_REQUEST);
				return;
			}

			ctx.writeAndFlush(new PacketBuilder(ctx.alloc())
							.putLong(0)
							.put((byte) 0)
							.putLong(new SecureRandom().nextLong())
							.finish(),
					ctx.voidPromise());

			state = CONNECTION_TYPE;
		}
	}

	private int loginPacketSize;

	private void decodeConnectionType(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
		if (in.readableBytes() >= 3) {
			connectionType = in.readUnsignedByte();
			loginPacketSize = in.readUnsignedShort();

			if (connectionType != 16 && connectionType != 18) {
				sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_CONNECTION);
				state = HANDSHAKE;
				return;
			}

			state = PAYLOAD;
		}
	}

	private void decodePayload(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
		if (!in.isReadable(loginPacketSize)) {
			return;
		}

		final String ip = ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();

		if (ip.isEmpty()) {
//			System.out.println("error: 3");
			sendReturnCode(ctx.channel(), LoginResponseCode.BAD_SESSION);
			state = HANDSHAKE;
			return;
		}

		if (Connection.spammingConnections.contains(ip)) {
//			System.out.println("error: 4");
			sendReturnCode(ctx.channel(), LoginResponseCode.LOGIN_LIMIT_EXCEEDED);
			state = HANDSHAKE;
			return;
		}

		final int magic = in.readUnsignedByte();

		if (magic != 0xFF) {
			// System.out.println("Wrong magic id.");
//			System.out.println("error: 5");
			sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_REQUEST);
			state = HANDSHAKE;
			return;
		}

		final int clientVersion = in.readUnsignedShort();

		if (clientVersion != Config.CLIENT_VERSION) {
//			System.out.println("error: 6");
			sendReturnCode(/* channel */ctx.channel(), LoginResponseCode.OUTDATED);
			state = HANDSHAKE;
			return;
		}

		final int random = in.readUnsignedByte();

		if (random != 145) {
//			System.out.println("error: 7");
			sendReturnCode(ctx.channel(), LoginResponseCode.OUTDATED);
			state = HANDSHAKE;
			return;
		}

		in.readByte(); // low memory

		for (int i = 0; i < 9; i++) {
			in.readInt();
		}

		final int javaVersion = in.readInt();
		final String UUID = Misc.getRS2String(in);

		final int expectedSize = in.readUnsignedByte();
		final int rsaPayloadSize = loginPacketSize - LOGIN_PACKET_HEADER_SIZE - UUID.length();

		if (expectedSize != rsaPayloadSize) {
//			System.out.println("error: 8");
			Misc.println("RS2Login: expectedSize does not match rsaPayloadSize.");
			sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_REQUEST);
			state = HANDSHAKE;
			return;
		}

		final byte[] rsaBlockArray = new byte[rsaPayloadSize];
		in.readBytes(rsaBlockArray);

		final ByteBuf rsaBuffer = Unpooled.wrappedBuffer(new BigInteger(rsaBlockArray).modPow(RSA_EXPONENT, RSA_MODULUS).toByteArray());

		if (rsaBuffer.readUnsignedByte() != 10) {
			 Misc.println("RS2Login: Encrypted id != 10.");
			sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_REQUEST);
			state = HANDSHAKE;
			return;
		}

		if (rsaBuffer.readUnsignedByte() != 245) {
			Misc.println("RS2Login: Encrypted id != 245.");
			sendReturnCode(ctx.channel(), LoginResponseCode.MALFORMED_LOGIN_REQUEST);
			state = HANDSHAKE;
			return;
		}

		final long clientHalf = rsaBuffer.readLong();
		final long serverHalf = rsaBuffer.readLong();

		final int[] isaacSeed = {(int) (clientHalf >> 32), (int) clientHalf, (int) (serverHalf >> 32), (int) serverHalf};
		final ISAACCipher inCipher = new ISAACCipher(isaacSeed);

		for (int i = 0; i < isaacSeed.length; i++) {
			isaacSeed[i] += 50;
		}

		final ISAACCipher outCipher = new ISAACCipher(isaacSeed);

		final String name = Misc.formatPlayerName(Misc.getRS2String(rsaBuffer));
		final String pass = Misc.getRS2String(rsaBuffer);

		final boolean reconnecting = connectionType == 18;

		ctx.pipeline().replace("decoder", "decoder", new RS2Decoder(inCipher));

		login(ctx.channel(), inCipher, outCipher, javaVersion, name, pass, clientVersion, UUID, ip, reconnecting);
		state = reconnecting ? HANDSHAKE : IGNORE;
	}
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> out) throws Exception {
		switch (state) {
			case HANDSHAKE:
				decodeHandshake(ctx, buffer);
				break;

			case CONNECTION_TYPE:
				decodeConnectionType(ctx, buffer);
				break;

			case PAYLOAD:
				decodePayload(ctx, buffer);
				break;
		}
	}
	
}
