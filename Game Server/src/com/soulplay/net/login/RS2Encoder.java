package com.soulplay.net.login;

import com.soulplay.net.Packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.ReferenceCountUtil;

@ChannelHandler.Sharable
public final class RS2Encoder extends MessageToByteEncoder<Packet> {
	
	@Override
	protected void encode(ChannelHandlerContext ctx, Packet msg, ByteBuf out) {
		final ByteBuf payload = msg.getPayload();
		try {
			out.writeBytes(payload);
		} finally {
			ReferenceCountUtil.release(payload);
		}
	}
	
}
