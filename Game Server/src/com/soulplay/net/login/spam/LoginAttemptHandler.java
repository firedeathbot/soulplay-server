package com.soulplay.net.login.spam;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.util.NameUtil;

public class LoginAttemptHandler {

	private static final int MAX_WRONG_PASSWORD_ATTEMPTS = 15;

	private static final int MAX_LOGIN_ATTEMPTS = 200;

	private static final Map<Long, LoginAttempt> loginMap = new HashMap<>();

	private static Object lock = new Object();

	private static final int MAX_NEW_ACCOUNTS = 10;

	private static final Map<String, Integer> newAccountsMap = new HashMap<>();
	
	private static final Map<Long, LoginAttempt> failedPasswordMap = new HashMap<>();

	public static void addHourlyLogin(String name, String IP, String UUID) {
		long key = NameUtil.encodeBase37(IP);
		synchronized (lock) {
			LoginAttempt attempt = loginMap.get(key);
			if (attempt != null) {
				attempt.incrememntLoginAttempt();
			} else {
				LoginAttempt login = new LoginAttempt(name, IP, UUID);
				login.incrememntLoginAttempt();
				loginMap.put(key, login);
			}
		}
	}

	public static void addWrongPassword(String name, String IP, String UUID) {
		long key = NameUtil.encodeBase37(IP);
		synchronized (lock) {
			LoginAttempt attempt = failedPasswordMap.get(key); //loginMap.get(key);
			if (attempt != null) {
				attempt.incrementWrongPassword();
			} else {
				LoginAttempt login = new LoginAttempt(name, IP, UUID);
				login.incrementWrongPassword();
				failedPasswordMap.put(key, login); //loginMap.put(key, login);
			}
		}
	}

	public static void clearLoginAttempts() {
//		synchronized (lock) {
			loginMap.clear();
//		}
	}
	
	public static void clearPasswordFails() {
		synchronized (lock) {
			failedPasswordMap.clear();
		}
	}

	public static void clearMaxAccountMap() {
		newAccountsMap.clear();
	}

	public synchronized static void incrementNewAccountMap(String UUID) {
		int oldValue = 0;
		if (newAccountsMap.containsKey(UUID)) {
			oldValue = newAccountsMap.get(UUID);
		}
		newAccountsMap.put(UUID, oldValue + 1);
	}

	public static boolean maxedLoginAttempts(String IP) {
		long key = NameUtil.encodeBase37(IP);
//		synchronized (lock) {
			if (loginMap.containsKey(key)) {
				return loginMap.get(key)
						.getWrongPasswordAttempt() >= MAX_LOGIN_ATTEMPTS;
			}
//		}
		return false;
	}

	public static boolean maxedNewAccountsForToday(String UUID) {
		int returnValue = 0;
		Integer value = newAccountsMap.get(UUID);
		if (value != null) {
			returnValue = value;
		}
		return returnValue >= MAX_NEW_ACCOUNTS;
	}

	public static boolean maxedWrongPasswordAttempts(String IP) {
		long key = NameUtil.encodeBase37(IP);
//		synchronized (lock) {
		
		LoginAttempt attempt = loginMap.get(key);
		if (attempt != null) {
			return attempt
					.getWrongPasswordAttempt() >= MAX_WRONG_PASSWORD_ATTEMPTS;
		}
//		}
		return false;
	}
	
	public static boolean maxedWrongPasswordsPerHour(String ip) {
		long key = NameUtil.encodeBase37(ip);
		
		LoginAttempt attempt = failedPasswordMap.get(key);
		
		if (attempt != null) {
			return attempt
					.getWrongPasswordAttempt() >= MAX_WRONG_PASSWORD_ATTEMPTS;
		}
		return false;
	}

}
