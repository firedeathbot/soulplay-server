package com.soulplay.net.login.spam;

public class LoginAttempt {

	private String name;

	private String IP;

	private String UUID;

	private int wrongPassword;

	private int loginAttempt;

	public LoginAttempt(String name, String IP, String UUID) {
		this.setName(name);
		this.setIP(IP);
		this.setUUID(UUID);
	}

	public String getIP() {
		return IP;
	}

	public int getLoginAttempt() {
		return loginAttempt;
	}

	public String getName() {
		return name;
	}

	public String getUUID() {
		return UUID;
	}

	public int getWrongPasswordAttempt() {
		return wrongPassword;
	}

	public void incrememntLoginAttempt() {
		this.loginAttempt++;
	}

	public void incrementWrongPassword() {
		this.wrongPassword++;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public void setLoginAttempt(int loginAttempt) {
		this.loginAttempt = loginAttempt;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

}
