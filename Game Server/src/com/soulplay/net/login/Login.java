package com.soulplay.net.login;

public class Login {

	String username;

	private int mysqlIndex;

	String ip;

	String UUID;

	public Login(String username, String ip, String UUID, int mysqlIndex) {
		this.username = username;
		this.ip = ip;
		this.UUID = UUID;
		this.setMysqlIndex(mysqlIndex);
	}

	public String getIP() {
		return ip;
	}

	public int getMysqlIndex() {
		return mysqlIndex;
	}

	public String getUsername() {
		return username;
	}

	public String getUUID() {
		return UUID;
	}

	public void setMysqlIndex(int mysqlIndex) {
		this.mysqlIndex = mysqlIndex;
	}
}
