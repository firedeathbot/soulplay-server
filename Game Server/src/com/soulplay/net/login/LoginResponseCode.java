package com.soulplay.net.login;

public class LoginResponseCode {

    public static final int DELAY_LOGIN = 1;
    public static final int LOGIN_SUCCESSFUL = 2;
    public static final int INVALID_CREDENTIALS = 3;
    public static final int DISABLED_ACCOUNT = 4;
    public static final int ACCOUNT_ALREADY_LOGGED_IN = 5;
    public static final int OUTDATED = 6;
    public static final int WORLD_FULL = 7;
    public static final int LOGIN_SERVER_OFFLINE = 8;
    public static final int LOGIN_LIMIT_EXCEEDED = 9;
    public static final int BAD_SESSION = 10;
    public static final int REJECTED_BY_LOGIN_SERVER = 11;
    public static final int MEMBERS_WORLD = 12;
    public static final int SERVER_OFFLINE = 13;
    public static final int UPDATE_IN_PROGRESS = 14;
    public static final int LOGIN_ATTEMPT_EXCEEDED = 16;
    public static final int MEMBERS_ONLY_AREA = 17;
    public static final int MALFORMED_LOGIN_REQUEST = 18;
    public static final int MALFORMED_LOGIN_CONNECTION = 19;
    public static final int INVALID_LOGIN_SERVER_REQUEST = 20;
    public static final int TRANSFER_WORLD = 21;
    public static final int BANNED = 22;
    public static final int RECONNECT = 23;

}
