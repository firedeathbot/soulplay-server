package com.soulplay.net;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.soulplay.Config;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.login.ReconnectedLogin;
import com.soulplay.net.login.spam.LoginAttemptHandler;

public class Connection {

	/**
	 * Bans & mutes.
	 */
	public static ArrayList<String> bannedIps = new ArrayList<>();

	public static ArrayList<String> bannedNames = new ArrayList<>();

	public static ArrayList<String> mutedIps = new ArrayList<>();

	public static ArrayList<String> mutedNames = new ArrayList<>();

	public static ArrayList<String> starterRecieved1 = new ArrayList<>();

	public static ArrayList<String> starterRecieved2 = new ArrayList<>();

	public static Collection<String> bannedUid = new ArrayList<>();

	public static Collection<String> mutedUid = new ArrayList<>();

	public static Collection<String> spammingConnections = new ArrayList<>();

	public static Collection<String> spammingConnectionsHacker2Thing = new ArrayList<>();

	private static HashMap<String, Integer> bankPinAttempts = new HashMap<>();

	private static ArrayList<String> lastIpConnecting = new ArrayList<>();

	private static ArrayList<String> lastUUIDConnecting = new ArrayList<>();

	public static boolean trackUUID = false;

	private static volatile List<ReconnectedLogin> recon = new ArrayList<>();

	/**
	 * Adds an IP address to the IPBanned list.
	 **/
	public static void addIpToBanList(String IP) {
		if (bannedIps.contains(IP)) {
			return;
		}
		bannedIps.add(IP);
	}

	/**
	 * Adds an IP address to the IPMuted list.
	 */
	public static void addIpToMuteList(String IP) {
		if (mutedIps.contains(IP)) {
			return;
		}
		mutedIps.add(IP);
	}

	public static void addIpToStarter1(String IP) {
		starterRecieved1.add(IP);
	}

	public static void addIpToStarter2(String IP) {
		starterRecieved2.add(IP);
	}

	/**
	 * Adds a user to the banned list.
	 **/
	public static void addNameToBanList(String name) {
		bannedNames.add(name.toLowerCase());
	}

	public static void addNameToMuteList(String name) {
		if (mutedNames.contains(name.toLowerCase())) {
			return;
		}
		mutedNames.add(name.toLowerCase());
	}

	public static synchronized void addToBankPinAttemptFailed(
			String playerName) {
		bankPinAttempts.put(playerName.toLowerCase(),
				getBankPinFailedAttemptAmount(playerName) + 1);
	}

	public static void addToReconList(String name) {
		if (!recon.isEmpty()) {
			List<ReconnectedLogin> reconTemp = new ArrayList<>();
			for (ReconnectedLogin r : recon) {
				if (r.expired()) {
					reconTemp.add(r);
				}
			}
			recon.removeAll(reconTemp);
			reconTemp.clear();
			reconTemp = null;
		}
		recon.add(new ReconnectedLogin(name));
	}

	public static void addUidToBanList(String UUID) {
		bannedUid.add(UUID);
	}

	public static void addUidToMuteList(String UUID) {
		mutedUid.add(UUID);
	}

	public static void banIp(String ip) {
		addIpToBanList(ip);
	}

	public static void banPlayerByName(String name) {
		addNameToBanList(name);
	}

	public static synchronized void clearBankPinAttempts() {
		bankPinAttempts.clear();
	}

	public static void clearLists() {
		bannedIps.clear();
		bannedNames.clear();
		mutedIps.clear();
		mutedNames.clear();
		bannedUid.clear();
		mutedUid.clear();
		clearBankPinAttempts();
		LoginAttemptHandler.clearPasswordFails();
	}

	public static synchronized int getBankPinFailedAttemptAmount(
			String playerName) {
		return bankPinAttempts.getOrDefault(playerName.toLowerCase(), 0);
	}
	
	public static final Map<String, Integer> IP_TO_CONNECTED = new ConcurrentHashMap<>(2000);

	public static synchronized ArrayList<String> getLastIpConnecting() {
		return lastIpConnecting;
	}

	public static synchronized ArrayList<String> getLastUUIDConnecting() {
		return lastUUIDConnecting;
	}

	public static boolean hasRecieved1stStarter(String IP) {
		if (starterRecieved1.contains(IP)) {
			return true;
		}
		return false;
	}

	public static boolean hasRecieved2ndStarter(String IP) {
		if (starterRecieved2.contains(IP)) {
			return true;
		}
		return false;
	}

	public static void initialize() {
		if (!Config.SERVER_DEBUG) {
			
		}
	}

	public static boolean inReconnectionList(String name) {
		if (recon.isEmpty()) {
			return false;
		}
		for (int i = 0; i < recon.size(); i++) {
			if (recon.get(i).containsName(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Contains banned IP addresses.
	 **/
	public static boolean isIpBanned(String IP) {
		if (bannedIps.contains(IP)) {
			return true;
		}
		return false;
	}

	/**
	 * Needed boolean for muting.
	 */
	public static boolean isMuted(Player c) {
		return c.isMuted() || mutedNames.contains(c.getName().toLowerCase())
				|| mutedIps.contains(c.connectedFrom)
				|| mutedUid.contains(c.UUID);
		// return false;
	}

	/**
	 * Contains banned users.
	 **/
	public static boolean isNamedBanned(String name) {
		return bannedNames.contains(name.toLowerCase());
	}

	public static boolean isUidBanned(String UUID) {
		return bannedUid.contains(UUID);
	}

	public static boolean isUidMuted(String UUID) {
		return mutedUid.contains(UUID);
	}

	/**
	 * Removes an IP address from the IPBanned list.
	 **/
	public static void removeIpFromBanList(String IP) {
		bannedIps.remove(IP);
	}

	/**
	 * Removes a banned user from the banned list.
	 **/
	public static void removeNameFromBanList(String name) {
		bannedNames.remove(name.toLowerCase());
	}

	public static void removeNameFromMuteList(String name) {
		bannedNames.remove(name.toLowerCase());
	}

	public static void removeUidFromBanList(String UUID) {
		bannedUid.remove(UUID);
	}

	public static void removeUidFromMuteList(String UUID) {
		mutedUid.remove(UUID);
	}

	public static void saveLists() {
		
	}

	public static synchronized void setLastIpConnecting(
			ArrayList<String> lastIpConnecting) {
		Connection.lastIpConnecting = lastIpConnecting;
	}

	public static synchronized void setLastUUIDConnecting(
			ArrayList<String> lastUUIDConnecting) {
		Connection.lastUUIDConnecting = lastUUIDConnecting;
	}

	/**
	 * Removes an IP address from the IPmuted list.
	 */
	public static void unIPMuteUser(String name) {
		mutedIps.remove(name);
	}

	/**
	 * Removes a muted user from the muted list.
	 */
	public static void unMuteUser(String name) {
		mutedNames.remove(name);
	}

	/**
	 * UUID Banning
	 */
	public static void unUidBanUser(String name) {
		bannedUid.remove(name);
	}

}
