package com.soulplay.net;

import com.soulplay.game.model.player.Client;

import io.netty.channel.Channel;

public class Session {

	private final Channel channel;

	private volatile Client client;

	public Session(Channel channel) {
		this.channel = channel;
	}

	public Channel getChannel() {
		return channel;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
