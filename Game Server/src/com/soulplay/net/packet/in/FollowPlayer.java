package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.PacketType;

public class FollowPlayer implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		int followPlayer = c.getInStream().readUnsignedLEShort();
		if (followPlayer < 0 || followPlayer >= Config.MAX_PLAYERS) {
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (PlayerHandler.players[followPlayer] == null) {
			return;
		}

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}
		if (c.isDead()) {
			return;
		}
		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}
		if (c.getAgility().doingAgility) {
			return;
		}

		if (c.inDuelArena()) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		c.isIdle = false;

		c.playerIndex = 0;
		c.npcIndex = 0;
		c.followDistance = 1;
		c.followId = followPlayer;
	}
}
