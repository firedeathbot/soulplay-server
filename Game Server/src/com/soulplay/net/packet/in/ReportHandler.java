package com.soulplay.net.packet.in;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.packet.in.CommandPacket.Punishment;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

/**
 * @author AMG A Bear
 */
public class ReportHandler {

	public static String[] savedNames = new String[500];

	public static String[] savedSpeach = new String[500];

	public static String[] savedTimes = new String[500];

	public static final String[] reportNames = {"Offensive language",
			"Item scamming", "Password scamming", "Bug abuse",
			"Staff impersonation", "Other", "Macroing", "Duping",
			"Encouraging others to break the rules", "Yell abuse",
			"Advertising", "Possible duped items",
			"Asking for personal details"};

	public static void addText(String name, byte[] data, int dataLength) {
		for (int i = 499; i > 0; i--) {
			savedNames[i] = savedNames[i - 1];
			savedSpeach[i] = savedSpeach[i - 1];
			savedTimes[i] = savedTimes[i - 1];
		}
		savedNames[0] = name;
		savedSpeach[0] = Misc.textUnpack(data, dataLength);
		String minute = new SimpleDateFormat("mm").format(new Date());
		String second = new SimpleDateFormat("ss").format(new Date());
		String hour = new SimpleDateFormat("hh").format(new Date());
		savedTimes[0] = hour + ":" + minute + ":" + second;
	}

	public static void addText(String name, String speech) {
		for (int i = 499; i > 0; i--) {
			savedNames[i] = savedNames[i - 1];
			savedSpeach[i] = savedSpeach[i - 1];
			savedTimes[i] = savedTimes[i - 1];
		}
		savedNames[0] = name;
		savedSpeach[0] = speech;
		Date thisDate = new Date();
		String minute = new SimpleDateFormat("mm").format(thisDate);
		String second = new SimpleDateFormat("ss").format(thisDate);
		String hour = new SimpleDateFormat("hh").format(thisDate);
		savedTimes[0] = hour + ":" + minute + ":" + second;
	}

	public static String getMonth(String s) {
		try {
			int i = Integer.parseInt(s);
			String[] months = {"", "January", "February", "March", "April",
					"May", "June", "July", "August", "September", "October",
					"November", "December"};
			return months[i];
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Unknown";
	}

	public static void handleReport(Client c, int packetType) throws Exception {
		final long l = c.getInStream().readLong();
		final byte rule = (byte) c.getInStream().readUnsignedByte();
		final byte canMute = (byte) c.getInStream().readUnsignedByte();

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| !c.correctlyInitialized) {
			return;
		}
		
		if (c.getReceivedPacket()[packetType]) {
			return;
		}
		c.setReceivedPacket(packetType, true);
		
		if (!c.getStopWatch().checkThenStart(1)) {
			return;
		}

		String player = Misc.longToPlayerName2(l);
		if (player == null) {
			return;
		}

		final String playerName = player;

		if (c.isOwner() || c.isMod() || c.isAdmin()) {
			LoginServerConnection.instance().submit(() -> {
				if (rule == 10) {
					LoginServerConnection.instance().punishPlayer(c.getName(),
							playerName,
							Punishment.MUTE_UUID.getPunishmentType(), 0,
							reportNames[rule]);
					Server.getSlackApi()
							.call(SlackMessageBuilder.buildJudgementMesssage(c,
									playerName, Punishment.MUTE_UUID.getDesc(),
									reportNames[rule]));
				} else {
					LoginServerConnection.instance().punishPlayer(c.getName(),
							playerName,
							Punishment.MUTE_NAME.getPunishmentType(), 0,
							reportNames[rule]);
					Server.getSlackApi()
							.call(SlackMessageBuilder.buildJudgementMesssage(c,
									playerName, Punishment.MUTE_NAME.getDesc(),
									reportNames[rule]));
				}
			});
			c.sendMessage("Sent mute request on player:" + playerName);
			return;
		}

		if (c.lastReported.equalsIgnoreCase(player)
				&& (System.currentTimeMillis() - c.lastReport) < 60000) {
			c.sendMessage(
					"You can only report a player once every 60 seconds.");
			return;
		}
		if (c.getNameSmart().equalsIgnoreCase(player)) {
			c.sendMessage("You cannot report yourself!");
			return;
		}
		if (hasSpoke(player)) {
			String sendText = "";

			for (int i = 499; i >= 0; i--) {
				if (savedNames[i] != null) {
					if (savedNames[i].equalsIgnoreCase(c.getName())
							|| savedNames[i].equalsIgnoreCase(player)) {
						// sendText += " -[" + savedTimes[i] + ": " +
						// savedNames[i] + "]: " + savedSpeach[i] + "\r\n";
						sendText += "<s>" + savedNames[i] + ": "
								+ savedSpeach[i];
					}
				}
			}

			sendText = sendText.replace("'", " ");
			// String month = getMonth(new SimpleDateFormat("MM").format(new
			// Date()));
			// String day = new SimpleDateFormat("dd").format(new Date());
			// Statement statement = Server.conn.createStatement();
			// writeReport(player + " was reported by " + c.getName() + ", " +
			// reportNames[rule] + ", " + month + ", " + day + "", sendText +
			// ".", reportNames[rule]);

			SqlHandler.reportPlayer(player, c.getName(), sendText,
					reportNames[rule]);
			
			PlayerHandler.alertStaff("Player :"+c.getNameSmart()+" has reported "+player+". Please check Reports on web.");
			
			Server.getSlackApi().call(SlackMessageBuilder.buildReportMadeMessage(player, c.getNameSmart()));

			c.sendMessage(
					"Thank you, your report has been received and will be reviewed.");
			c.lastReported = player;
			c.lastReport = System.currentTimeMillis();
			return;
		} else {
			c.sendMessage(
					"You can only report someone who has spoken in the last 60 seconds.");
			return;
		}
	}

	public static boolean hasSpoke(String s) {
		for (int i = 0; i < 500; i++) {
			if (savedNames[i] != null) {
				if (savedNames[i].equalsIgnoreCase(s)) {
					return true;
				}
			}
		}
		return false;
	}

	public static void writeLog(String text, String file, String dir) {
		// used for bans/mutes/chatlogs etc. -bakatool
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(dir + file + ".txt", true));
			bw.write(text);
			bw.newLine();
			bw.flush();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException ioe2) {
					System.out.println("Error writing system log.");
					// ioe2.printStackTrace();
				}
			}
		}
	}

	public static void writeReport(String data, String text, String file) {
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter(
					new FileWriter("./Data/reports/" + file + ".txt", true));
			bw.write(data);
			bw.newLine();
			bw.write(text);
			bw.newLine();
			bw.newLine();
			bw.flush();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException ioe2) {
					System.out.println("Error writing system log.");
					ioe2.printStackTrace();
				}
			}
		}

	}
}
