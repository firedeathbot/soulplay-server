package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Slient Packet
 **/
public class SilentPacket implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		// if (c.dialogueAction == 9999) {
		// c.logoutDelay = 0;
		// c.underAttackBy = c.underAttackBy2 = 0;
		// c.logout();
		// }

		if (packetType == 3) { // player focus on game screen or not
			c.hasFocusOnClient = c.getInStream().readUnsignedByte() == 1;
			return;
		}

		if (packetType == 77) { // testing connection?
			// some macro stuff?
		}
	}
}
