package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.net.packet.PacketType;

/**
 * Change appearance
 **/
public class ChangeAppearance implements PacketType {

	private static final int[][] MALE_VALUES = {{0, 8}, // head
			{10, 17}, // jaw
			{18, 25}, // torso
			{26, 31}, // arms
			{33, 34}, // hands
			{36, 40}, // legs
			{42, 43}, // feet
	};

	private static final int[][] FEMALE_VALUES = {{45, 54}, // head
			{-1, -1}, // jaw
			{56, 60}, // torso
			{61, 65}, // arms
			{67, 68}, // hands
			{70, 77}, // legs
			{79, 80}, // feet
	};

	private static final int[][] ALLOWED_COLORS = {{0, 11}, // hair color
			{0, 15}, // torso color
			{0, 15}, // legs color
			{0, 5}, // feet color
			{0, 13} // skin color
	};

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		int gender = c.getInStream().readByte();

		// if(gender != 0 && gender != 1) { // gender value check
		// gender = 0;
		// return;
		// }
		if (gender < 0) {
			gender = 0;
		}
		if (gender > 1) {
			gender = 0;
		}

		int[] apperances = new int[MALE_VALUES.length]; // apperance's value
														// check
		for (int i = 0; i < apperances.length; i++) {
			int value = c.getInStream().readByte();
			if (value < (gender == 0 ? MALE_VALUES[i][0] : FEMALE_VALUES[i][0])
					|| value > (gender == 0
							? MALE_VALUES[i][1]
							: FEMALE_VALUES[i][1])) {
				value = (gender == 0 ? MALE_VALUES[i][0] : FEMALE_VALUES[i][0]);
			}
			apperances[i] = value;
		}

		int[] colors = new int[ALLOWED_COLORS.length]; // color value check
		for (int i = 0; i < colors.length; i++) {
			int value = c.getInStream().readByte();

			if (value < ALLOWED_COLORS[i][0] || value > ALLOWED_COLORS[i][1]) {
				value = ALLOWED_COLORS[i][0];
			}

			if (i == colors.length - 1 && (value >= 8) && (value <= 10) && !Config.isLegendaryDonator(c) && c.playerAppearance[12] != value) {
				c.sendMessage("You must be a legendary donator to unlock this skin color!");
				return;
			}
			
			if (i == colors.length - 1 && (value >= 11) && (value <= 13) && !Config.isUberDonator(c) && c.playerAppearance[12] != value) {
				c.sendMessage("You must be a uber donator to unlock this skin color!");
				return;
			}

			colors[i] = value;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| !c.correctlyInitialized || c.inVerificationState) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (c.getReceivedPacket()[packetType]) {
			return;
		}
		c.setReceivedPacket(packetType, true);

		if (c.canChangeAppearance) {
			c.playerAppearance[0] = gender; // gender
			c.playerAppearance[1] = apperances[0]; // head
			c.playerAppearance[2] = apperances[2];// Torso
			c.playerAppearance[3] = apperances[3]; // arms
			c.playerAppearance[4] = apperances[4]; // hands
			c.playerAppearance[5] = apperances[5]; // legs
			c.playerAppearance[6] = apperances[6]; // feet
			c.playerAppearance[7] = apperances[1]; // beard
			c.playerAppearance[8] = colors[0]; // hair colour
			c.playerAppearance[9] = colors[1]; // torso colour
			c.playerAppearance[10] = colors[2]; // legs colour
			c.playerAppearance[11] = colors[3]; // feet colour
			c.playerAppearance[12] = colors[4]; // skin colour

			c.getAchievement().changeAppearnace();

			c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
			c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
			c.canChangeAppearance = false;
		}

		/*
		 * int gender = c.getInStream().readSignedByte(); int head =
		 * c.getInStream().readSignedByte(); int jaw =
		 * c.getInStream().readSignedByte(); int torso =
		 * c.getInStream().readSignedByte(); int arms =
		 * c.getInStream().readSignedByte(); int hands =
		 * c.getInStream().readSignedByte(); int legs =
		 * c.getInStream().readSignedByte(); int feet =
		 * c.getInStream().readSignedByte(); int hairColour =
		 * c.getInStream().readSignedByte(); int torsoColour =
		 * c.getInStream().readSignedByte(); int legsColour =
		 * c.getInStream().readSignedByte(); int feetColour =
		 * c.getInStream().readSignedByte(); int skinColour =
		 * c.getInStream().readSignedByte(); if (c.canChangeAppearance) {
		 * c.playerAppearance[0] = gender; // gender c.playerAppearance[1] =
		 * head; // head c.playerAppearance[2] = torso;// Torso
		 * c.playerAppearance[3] = arms; // arms c.playerAppearance[4] = hands;
		 * // hands c.playerAppearance[5] = legs; // legs c.playerAppearance[6]
		 * = feet; // feet c.playerAppearance[7] = jaw; // beard
		 * c.playerAppearance[8] = hairColour; // hair colour
		 * c.playerAppearance[9] = torsoColour; // torso colour
		 * c.playerAppearance[10] = legsColour; // legs colour
		 * c.playerAppearance[11] = feetColour; // feet colour
		 * c.playerAppearance[12] = skinColour; // skin colour
		 * c.getPA().removeAllWindows(); c.requestUpdates();
		 * c.canChangeAppearance = false; }
		 */
	}
}
