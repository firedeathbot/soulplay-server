package com.soulplay.net.packet.in;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.impl.DuellistsCap;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.skills.dungeoneeringv2.BindingData;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class ItemAction4 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int interfaceId = c.getInStream().readLEShortA();
		int slot = c.getInStream().readLEShort();
		int itemId = c.getInStream().read3Bytes();

		// c.sendMess("k:"+interfaceId+" j:"+unknownID+" i1:"+itemId);
		
		if (c.debug) {
			c.sendMessage(String.format("ItemClick3 - interfaceId: %d unknownID: %d itemId: %d", interfaceId, slot, itemId));
		}

		if (!c.correctlyInitialized) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}
		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.isStunned()) {
			return;
		}

		if (c.teleportToX > 0) { // do not handle packets for players that are
									// being moved
			return;
		}

		if (!c.getItems().playerHasItem(itemId, 1)) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		c.isIdle = false;

		final PluginResult<ItemClickExtension> pluginResult = PluginManager.search(ItemClickExtension.class, itemId);
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOption4(c, itemId, slot, interfaceId))) {
			return;
		}

		/*
		 * if (itemId >= DiceHandler.DICE_BAG && itemId <= 15100) {
		 * DiceHandler.putupDice(c, itemId); }
		 */

		if (c.getSummoning().summonFamiliar(itemId, false)) {
			return;
		}

		if (DuellistsCap.getCapForID(itemId) != null) {
			DuellistsCap.handleDialogues(c, DuellistsCap.CHANGE_LOOK,
					DuellistsCap.getCapForID(itemId));
			return;
		}

		if (CompletionistCape.handleCompletionistsCapeItemClick(c, itemId,
				CompletionistCape.OPTION_FEATURES)) {
			return;
		}
		
		if (RunePouch.emptyRunePouch(c, itemId)) {
			return;
		}

		if (itemId == 995 && interfaceId == 3214) {
			// add to money pouch

			if (!c.canWithDrawOrDepositMoneyPouch()) {
				c.sendMessage("You cannot do that here.");
				return;
			}
			
			if (c.moneyPouchOnCooldown()) {
				c.sendMessage("You cannot store to MoneyPouch for 1 minute.");
				return;
			}

			// c.getPA().sendFrame126b("<col=ffffff>"+Misc.formatShortPrice(999999999),
			// 12501);
			int goldAmount = c.getItems().getItemAmount(itemId);
			if (c.getItems().deleteItem2(itemId, goldAmount)) {
				MoneyPouch.addToMoneyPouch(c, goldAmount);
			}

			// c.xInterfaceId = interfaceId;
			// c.xRemoveId = itemId;
			// c.getPA().setInputDialogState(PlayerConstants.SET_DIALOG_AMOUNT_TO_WITHDRAW);
			return;
		}
		
		switch (interfaceId) {
		case 3214:
			switch (itemId) {
			case 1933: // pot of flour
				//empty
				c.getItems().replaceItemSlot(1931, slot); // already passed player has item chek so it's safe.
				c.sendMessage("You empty the contents of the pot on the floor.");
				break;
			case 1927: // empty bucket of milk
				c.getItems().replaceItemSlot(1925, slot);
				c.sendMessage("You empty the contents of the bucket on the floor.");
				break;
			}
			break;
		}
		
		switch (itemId) {
		case 18342:
			if (c.getLawStaffCharges() <= 0) {
				c.sendMessage("You have no charges to empty.");
				break;
			}

			if (c.getItems().addItem(MagicRequirements.LAW, c.getLawStaffCharges())) {
				c.emptyLawStaffCharges();
				c.sendMessage("You have emptied charges from your staff.");
			}
			break;
		case 18341:
			if (c.getNatureStaffCharges() <= 0) {
				c.sendMessage("You have no charges to empty.");
				break;
			}

			if (c.getItems().addItem(MagicRequirements.NATURE, c.getNatureStaffCharges())) {
				c.emptyNatureStaffCharges();
				c.sendMessage("You have emptied charges from your staff.");
			}
			break;
		case 15707:
			c.sendMessage("You cannot customise this ring.");
			break;
		
		case 4273:
			c.getDH().sendStatement("This key can be used on a Golden Chest.", "A Golden Chest contains super good Items.");
			break;
			
		case 6759:
			c.getDH().sendStatement("You need a Golden Key to open this chest.", "This Golden Chest Contains super good items such as", "AGS, Torva, Pernix, Virtus, Spirit Shields, T Bow, D Claws....   ");
			break;
		
			case 12926:
				if (c.getBlowpipeAmmo() > 0 && c.getBlowpipeAmmoType() > 0) {
					if (c.getItems().addItem(c.getBlowpipeAmmoType(), c.getBlowpipeAmmo())) {
						c.setBlowpipeAmmo(0);
						c.setBlowpipeAmmoType(0);
						c.sendMessage("You unload the Toxic Blowpipe ammo.");
					}
				} else {
					c.sendMessage("There is no ammo in the blowpipe.");
				}
				break;

			case 10512:
				if (c.getBarbarianAssault().getGroup().isEmpty()) {
					c.sendMessage("This scroll is already empty.");
					return;
				}

				if (c.getBarbarianAssault().inArena()) {
					c.sendMessage("You can't do this here.");
					return;
				}

				if (c == c.getBarbarianAssault().getPartyLeader()
						&& c.getBarbarianAssault().getGroup().size() > 1) {
					c.getDH().sendOption3("Kick player", "Leave group",
							"Go back");
					c.dialogueAction = 27;
				} else {
					c.getDH().sendOption2("Leave group", "Go back");
					c.dialogueAction = 22;
				}
				break;

			case 6865:
				c.startAnimation(3006);
				c.startGraphic(Graphic.create(514));
				c.sendMessage("You make your Marionette dance.");
				break;
			case 6867:
				c.startAnimation(3006);
				c.startGraphic(Graphic.create(510));
				c.sendMessage("You make your Marionette dance.");
				break;
			case 6866:
				c.startAnimation(3006);
				c.startGraphic(Graphic.create(518));
				c.sendMessage("You make your Marionette dance.");
				break;

			case 18338:
				c.getItems().addItem(1624, 20);
				c.getItems().addItem(1622, 15);
				c.getItems().addItem(1620, 10);
				c.getItems().addItem(1618, 4);
				c.sendMessage("You empty the gem bag.");
				break;

			case 2550: // breaking ring of recoil
				if (c.getItems().deleteItem2(itemId, 1)) {
					c.getRecoilRings().shatterRingOfRecoil(c);
					c.sendMessage("You shatter your ring of recoil.");
				}
				break;

			case 2552:
			case 2554:
			case 2556:
			case 2558:
			case 2560:
			case 2562:
			case 2564:
			case 2566:
				// c.getPA().ROD();
				c.getPA().startTeleport(3362, 3263, 0, TeleportType.MODERN);
				break;

			case 1712:
			case 1710:
			case 1708:
			case 1706:
			case 1704:
				c.getPA().handleGlory();
				break;

			case 3853:
			case 3855:
			case 3857:
			case 3859:
			case 3861:
			case 3863:
			case 3865:
			case 3867:
				c.getPA().gamesNeck();
				break;

			case 11105:
			case 11107:
			case 11109:
			case 11111:
				c.getPA().skillsNeck();
				break;

			case 20468:
			case 20469:
			case 20471:
			case 20477:
			case 20478:
			case 20479:
			case 20486:
				c.sendMessage(
						"Boot in the Dwarven mines might be able to help you fix this.");
				break;

			default:
				if (c.playerRights == 3 || c.isDev()) {
					Misc.println(c.getName() + " - Item3rdOption: " + itemId
							+ " : " + interfaceId + " : " + slot);
				}
				break;
		}

		if (c.insideDungeoneering()) {
			BindingData data = BindingData.values.get(itemId);
			if (data == null) {
				return;
			}

			if (!data.isStackable()) {
				if (c.getBinds().addBind(data.getBindItemId())) {
					c.getItems().deleteItemInOneSlot(itemId, slot, 1);
					c.sendMessage("You bind the " + ItemDef.forID(itemId).name + " to you.");
					c.sendMessage("Check in with the smuggler to manage your bound items.");
				}
			} else {
				c.getBinds().addAmmunition(itemId, slot, c.getItems().getItemAmountInSlot(slot), data.getBindItemId());
			}
			return;
		}
	}

}
