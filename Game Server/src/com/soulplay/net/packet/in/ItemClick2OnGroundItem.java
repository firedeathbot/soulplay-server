package com.soulplay.net.packet.in;

import com.soulplay.content.player.skills.firemaking.Firemaking;
import com.soulplay.content.player.skills.firemaking.LogData;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.GroundItemClickExtension;
import com.soulplay.util.Misc;

public class ItemClick2OnGroundItem implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		final int itemX = c.getInStream().readLEShort();
		final int itemY = c.getInStream().readLEShortA();
		final int itemId = c.getInStream().read3Bytes();
		if (c.isOwner()) {
			System.out.println("ItemClick2OnGroundItem - " + c.getName() + " - "
					+ itemId + " - " + itemX + " - " + itemY);
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}
		if (c.isDead()) {
			return;
		}
		if (c.performingAction) {
			return;
		}

		if (Misc.distanceToPoint(c.getX(), c.getY(), itemX, itemY) > 50) {
			return;
		}
		
		c.walkingToItem = true;
		
		c.pItemX = itemX;
		c.pItemY = itemY;

		if (!ItemHandler.itemExists(c, itemId, itemX, itemY,
				c.getHeightLevel())) {
			return;
		}
		
		final PluginResult<GroundItemClickExtension> pluginResult = PluginManager.search(GroundItemClickExtension.class, itemId);
		final Player player = c;
		if (pluginResult.invoke(() -> pluginResult.extension.onGroundItemOption2(player, itemId, itemX, itemY))) {
			return;
		}
		
		LogData l = LogData.getLog(itemId);
		if (l != null) {
			if (c.skillingEvent != null)
				c.resetSkillingEvent();
			
			c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
				
				@Override
				public void stop() {
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					if (!c.destinationReachedAny())
						return;
					
					container.stop();
					c.skillingEvent = null;
					
					Firemaking.attemptFire(c, 590, itemId, itemX, itemY, true);
				}
			}, 1);
		}
			
	}
}
