package com.soulplay.net.packet.in;

import com.soulplay.config.WorldType;
import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.shops.ShopPrice;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemClickExtension;

public class ItemAction5 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		final int itemId = c.getInStream().read3Bytes();
		final int interfaceId = c.getInStream().readUnsignedShort();
		final int slot = c.getInStream().readUnsignedShortA();

		if (slot < 0 || slot >= c.playerItems.length) {
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.isDead()) {
			return;
		}

		if (c.interfaceIdOpenMainScreen != 0) {
			c.getPA().closeAllWindows();
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.logoutOnVerificationState())
			return;

		if (c.duelStatus > 0) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.isStunned()) {
			return;
		}

		// do not handle packets for players that are
		// being moved
		if (c.teleportToX > 0) {
			return;
		}

		if (c.playerItems[slot] != itemId + 1) {
			return;
		}
		c.isIdle = false;

		c.getPA().closeAllWindows();
		
		final PluginResult<ItemClickExtension> pluginResult = PluginManager.search(ItemClickExtension.class, itemId);
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOption5(c, itemId, slot, interfaceId))) {
			return;
		}

		if (c.playerIsSkilling) {
			c.playerIsSkilling = false;
		}

		if (c.isOwner()) {
			// c.sendMessage("A: "+a+" B: " +b);
			// c.sendMessage("itemId:"+itemId+" slot:"+slot);
		}
		c.alchDelay = System.currentTimeMillis();
		if (c.duelFightArenas()) {
			c.sendMessage("You can't drop items inside the arena!");
			return;
		}
//		if (RSConstants.inCastleWarsArena(c.getX(), c.getY()) && itemId == 995) {
//			c.sendMessage("You can't drop coins inside the arena!");
//			return;
//		}
		if (c.inTrade) {
			return;
		}

		if (c.walkingToObject) {
			c.walkingToObject = false;
		}

//		if ((itemId == 13879 || itemId == 11212) && c.playerItemsN[slot] < 5) { // old exploit patch to the autoreset thing
//			c.sendMessage("You cannot drop small amounts of this item.");
//			return;
//		}

		if (c.playerItemsN[slot] != 0 && itemId > -1
				&& c.playerItems[slot] == itemId + 1) {
			
			if (c.playerItemsN[slot] < 1)
				return;
			
			// some prereqs
			if (c.underAttackBy > 0) {
				if (ShopPrice.getItemShopValue(itemId) > 1000) {
					c.sendMessage(
							"You may not drop items worth more than 1000 while in combat.");
					return;
				}
				ItemDefinition itemDef = ItemDefinition.forId(itemId);
				if (itemDef != null && itemDef.getDropValue() > 25000) {
					c.sendMessage(
							"You may not drop items worth more than 25,000gp while in combat.");
					return;
				}
			}
			if (WorldType.equalsType(WorldType.SPAWN)) {
				if (itemId == 8890 && c.timePlayed < 43200) {
					c.sendMessage(
							"Your account is not old enough to trade this item.");
					return;
				}
			}
			if (itemId == 2996 && c.timePlayed < 1800) {
				c.sendMessage(
						"Your account is not old enough to trade this item.");
				return;
			}
//			if (!c.isDungeoneering()) {
//				if (ItemProjectInsanity.isDungeoneeringItem(itemId)) {
//					c.sendMessage("You can't drop this item right now.");
//					return;
//				}
//			}
			// if (c.difficulty == PlayerDifficulty.IRONMAN) {
			// c.sendMessage("You cannot drop on Ironman mode, please use
			// ::empty.");
			// return;
			// }

			c.resetSkillingEvent();
			Item item = new Item(itemId, c.playerItemsN[slot], slot);
			FifthItemAction action = FifthItemAction.get(item.getId());
			
			if (action != null) {
				action.init(c, item);
			} else {
				FifthItemAction.DROP_ITEM.init(c, item);
			}
			
		}
	}
}
