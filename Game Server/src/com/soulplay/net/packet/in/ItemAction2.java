package com.soulplay.net.packet.in;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.soulplay.content.player.skills.runecrafting.PouchType;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.SoundManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemClickExtension;

/**
 * Wear Item
 **/
public class ItemAction2 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		final int itemId = c.getInStream().read3Bytes();
		final int itemSlot = c.getInStream().readUnsignedShortA();
		final int interfaceId = c.getInStream().readUnsignedShortA();
		
		c.interfaceId = interfaceId;
		
		if (c.debug) {
			c.sendMessage(String.format(
					"[ItemActionTwo] item_id=%d slot=%d interface=%d", itemId, itemSlot,
					interfaceId));
		}
		
		if (c.toggleBot && c.getBot() != null) {
			c.getBot().wearId = itemId;
			c.getBot().wearSlot = itemSlot;
			c.getBot().interfaceId = c.interfaceId;
			c = c.getBot();
		}

		if (!c.correctlyInitialized) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.openDuel || c.inTrade || c.isBanking) {
			// c.getPA().closeAllWindows();
			return;
		}

		if (c.getDuelingClanWarClient() != null) {
			c.getPA().closeAllWindows();
			return;
		}

		c.isIdle = false;
		c.resetSkillingEvent();

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.isDead()) {
			return;
		}
		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.isStunned()) {
			return;
		}

		if (c.teleportToX > 0) { // do not handle packets for players that are
									// being moved
			return;
		}

		if (c.interfaceId != 3214) {
			return;
		}

		if (itemSlot < 0 || itemSlot >= c.playerItems.length) {
			return;
		}

		if (c.playerItems[itemSlot] != itemId + 1) {
			return;
		}
		
		final PluginResult<ItemClickExtension> pluginResult = PluginManager.search(ItemClickExtension.class, itemId);
		final Client player = c;
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOption2(player, itemId, itemSlot, interfaceId))) {
			return;
		}

		if (ItemProjectInsanity.isDungeoneeringItem(itemId) && c.playerRights == 3 && (c.dungParty == null || (c.dungParty != null && c.dungParty.dungManager == null))) {
			c.sendMessage("You can't wear this here.");
			return;
		}
		
		// if (c.doingHandCannonSpec)
		// return;
		// try {
		// System.out.println("itemId "+wearId+"
		// itemSlot"+c.playerItems[wearSlot]);
		// } catch(Exception ex) {
		//
		// }

		// if (c.getItems().isNoted(wearId)) {
		// return;
		// }

		// if(c.getName().equalsIgnoreCase("sha512")) {
		// System.out.println("WearItem - wearId "+wearId+")
		// wearSlot:"+wearSlot+" c.interfaceId:"+c.interfaceId);
		// }

		if ((itemId == 20763 || itemId == 20764)) {
			long diff = Calendar.getInstance().getTimeInMillis()
					- c.getJoinedDate().getTime();
			if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) < 100) {
				c.sendMessage("Your account is not old enough yet.");
				return;
			}
		}

		c.alchDelay = System.currentTimeMillis(); // wtf is this shit?

		if ((c.playerIndex > 0 || c.npcIndex > 0)/* && wearId != 4153 */) {
			c.getCombat().resetPlayerAttack();
		}
		if (c.canChangeAppearance) {
			c.sendMessage("You can't wear an item while changing appearence.");
			return;
		}

		if (itemId == 6865) {
			c.startAnimation(3006);
			c.startGraphic(Graphic.create(514));
			c.sendMessage("You make your Marionette dance.");
			return;
		}
		if (itemId == 6866) {
			c.startAnimation(3006);
			c.startGraphic(Graphic.create(514));
			c.sendMessage("You make your Marionette dance.");
			return;
		}
		if (itemId == 6867) {
			c.startAnimation(3006);
			c.startGraphic(Graphic.create(514));
			c.sendMessage("You make your Marionette dance.");
			return;
		}

		if (itemId == 4155) {
			Slayer.checkKills(c);
			return;
		}

		if (itemId == 15262) { // SPIRIT PACK
			return;
		}

		if (itemId == 5509) {
		    c.getRunecrafting().getPouch(PouchType.SMALL).empty();
		    return;
		}
		
		if (itemId == 5510) {
		    c.getRunecrafting().getPouch(PouchType.MEDIUM).empty();
		    return;
		}
		
		if (itemId == 5512) {
		    c.getRunecrafting().getPouch(PouchType.LARGE).empty();
		    return;
		}

		if (itemId == 5514) {
		    c.getRunecrafting().getPouch(PouchType.GIANT).empty();
		    return;
		}

		// c.attackTimer = oldCombatTimer;

		if (c.toggledDfs) {
			c.toggledDfs = false;
		}

		wearItemCentral(c, itemId, itemSlot);
	}

	public static boolean wearItemCentral(Player c, int itemId, int itemSlot) {
		if (c.getItems().wearItem(itemId, itemSlot)) {
			final int soundId = SoundManager.getWearSound(c, itemId);
			if (soundId != -1) {
				c.getPacketSender().playSound(soundId);
			}

			return true;
		}

		return false;
	}

}