package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.cache.ItemDef;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.Smelting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.SmeltingDataDung;
import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Bank All Items
 **/
public class BankAll implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int removeSlot = c.getInStream().readUnsignedShortA();
		int interfaceId = c.getInStream().read3Bytes();
		int removeId = c.getInStream().read3Bytes();

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		c.interfaceId = interfaceId;

		switch (interfaceId) {
			case GamblingInterface.SETUP_INV_ID:
				GamblingInterface.takeItem(c, Integer.MAX_VALUE, removeSlot);
				break;
			case GamblingInterface.SIDE_INV_ID:
				GamblingInterface.offerItem(c, Integer.MAX_VALUE, removeSlot);
				break;
			case SmithingInterface.COLUMN_ONE_ID:
			case SmithingInterface.COLUMN_TWO_ID:
			case SmithingInterface.COLUMN_THREE_ID:
			case SmithingInterface.COLUMN_FOUR_ID:
			case SmithingInterface.COLUMN_FIVE_ID:
				Smithing.startSmithing(c, interfaceId, removeSlot, 28);
				break;
			case 7423: {
				// prevent cheat client, 4465 is main deposit box interface
				if (c.interfaceIdOpenMainScreen != 4465) {
					return;
				}

				if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
					return;
				}

				if (c.playerItems[removeSlot] != removeId + 1) {
					return;
				}

				final int amount = c.getItems().deleteInventoryItem(removeId, c.getItems().getItemAmount(removeId));

				if (amount > 0) {
					c.getItems().addItemToBank(removeId, amount);
					c.getItems().resetItems(7423);
				}
			}
			break;

			case 51001:
				Smelting.startSmelt(c, SmeltingDataDung.barToData.get(removeId), 28);
				break;
			case 2006: // drop party chest deposit
				c.getPartyOffer().depositItem(removeSlot, Integer.MAX_VALUE);
				break;
			case 2274: // drop party chest withdraw
				c.getPartyOffer().withdrawItem(removeSlot, Integer.MAX_VALUE);
				break;
			
			case 67111:
			case 44010:
			case 3900:
				c.getShops().buyItem(removeId, removeSlot, 10);
				break;

			case 3823:
				if (!c.getItems().playerHasItem(removeId)) {
					return;
				}
				c.getShops().sellItem(removeId, removeSlot, 10);
				break;

			case 5064:
				if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
					return;
				}
				if (c.isBobOpen) {
					if (ItemProjectInsanity.itemStackable[removeId]) {
						c.getSummoning().depositItem(c.playerItems[removeSlot],
								removeSlot, c.playerItemsN[removeSlot]);
					} else {
						c.getSummoning().depositItem(c.playerItems[removeSlot],
								removeSlot, c.getItems()
										.itemAmount(c.playerItems[removeSlot]));
					}
					return;
				}
				if (!c.isBanking) {
					return;
				}

				if (!c.toggleBot && !c.getItems().playerHasItem(removeId)) {
					return;
				}
				if (c.inTrade) {
					c.sendMessage("You can't store items while trading!");
					c.getTradeAndDuel().declineTrade(/* true */);
					return;
				}
				if (ItemProjectInsanity.itemStackable[removeId]) {
					if (c.toggleBot && c.getBot() != null) {
						c.getBot().getItems().bankItem(
								c.getBot().playerItems[removeSlot] - 1,
								removeSlot,
								c.getBot().playerItemsN[removeSlot]);
						c.getItems().resetInterfacesForToggleBot();
						return;
					}
					c.getItems().bankItem(c.playerItems[removeSlot] - 1,
							removeSlot, c.playerItemsN[removeSlot]);
				} else {
					if (c.toggleBot && c.getBot() != null) {
						c.getBot().getItems().bankItem(
								c.getBot().playerItems[removeSlot] - 1,
								removeSlot, c.getBot().getItems().itemAmount(
										c.getBot().playerItems[removeSlot]));
						c.getItems().resetInterfacesForToggleBot();
						return;
					}
					c.getItems().bankItem(c.playerItems[removeSlot] - 1,
							removeSlot,
							c.getItems().itemAmount(c.playerItems[removeSlot]));
				}
				break;

			case 5382:
				// System.out.println("Removed all?");
				if (removeSlot < 0 || removeSlot > c.bankItems0.length) {
					return;
				}
				if (!c.isBanking) {
					return;
				}
				c.interfaceId = 5383;

				if (c.toggleBot && c.getBot() != null) {
					int itemId = 0;
					int amount = 1;
					// c.getBot().interfaceId = 5383;
					switch (c.getBot().bankingTab) {
						case 0:
							itemId = c.getBot().bankItems0[removeSlot];
							amount = c.getBot().bankItems0N[removeSlot];
							break;
						case 1:
							itemId = c.getBot().bankItems1[removeSlot];
							amount = c.getBot().bankItems1N[removeSlot];
							break;
						case 2:
							itemId = c.getBot().bankItems2[removeSlot];
							amount = c.getBot().bankItems2N[removeSlot];
							break;
						case 3:
							itemId = c.getBot().bankItems3[removeSlot];
							amount = c.getBot().bankItems3N[removeSlot];
							break;
						case 4:
							itemId = c.getBot().bankItems4[removeSlot];
							amount = c.getBot().bankItems4N[removeSlot];
							break;
						case 5:
							itemId = c.getBot().bankItems5[removeSlot];
							amount = c.getBot().bankItems5N[removeSlot];
							break;
						case 6:
							itemId = c.getBot().bankItems6[removeSlot];
							amount = c.getBot().bankItems6N[removeSlot];
							break;
						case 7:
							itemId = c.getBot().bankItems7[removeSlot];
							amount = c.getBot().bankItems7N[removeSlot];
							break;
						case 8:
							itemId = c.getBot().bankItems8[removeSlot];
							amount = c.getBot().bankItems8N[removeSlot];
							break;
					}
					// System.out.println("Item id:"+itemId+" amount:"+amount);
					c.getItems().fromBank(itemId, removeSlot, amount);
					break;
				}

				int itemId = 0;
				int amount = 1;
				switch (c.bankingTab) {
					case 0:
						itemId = c.bankItems0[removeSlot];
						amount = c.bankItems0N[removeSlot];
						break;
					case 1:
						itemId = c.bankItems1[removeSlot];
						amount = c.bankItems1N[removeSlot];
						break;
					case 2:
						itemId = c.bankItems2[removeSlot];
						amount = c.bankItems2N[removeSlot];
						break;
					case 3:
						itemId = c.bankItems3[removeSlot];
						amount = c.bankItems3N[removeSlot];
						break;
					case 4:
						itemId = c.bankItems4[removeSlot];
						amount = c.bankItems4N[removeSlot];
						break;
					case 5:
						itemId = c.bankItems5[removeSlot];
						amount = c.bankItems5N[removeSlot];
						break;
					case 6:
						itemId = c.bankItems6[removeSlot];
						amount = c.bankItems6N[removeSlot];
						break;
					case 7:
						itemId = c.bankItems7[removeSlot];
						amount = c.bankItems7N[removeSlot];
						break;
					case 8:
						itemId = c.bankItems8[removeSlot];
						amount = c.bankItems8N[removeSlot];
						break;
				}


				if (removeId == 770) {
					boolean prevToggle = c.isPlaceholderEnabled();
					if (!c.isPlaceholderEnabled())
						c.togglePlaceholder();
					
					c.getItems().fromBank(itemId, removeSlot, amount);
					
					if (prevToggle != c.isPlaceholderEnabled()) {
						c.togglePlaceholder();
					}
				} else
					c.getItems().fromBank(itemId, removeSlot, amount);

				break;

			case 3322:
				if (removeId < 0 || removeId > Config.ITEM_LIMIT) {
					return;
				}

				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.stakeItem(c, removeId, removeSlot,
							c.getItems().getItemAmount(removeId, removeSlot),
							interfaceId);
					return;
				}

				if (c.duelStatus <= 0) {
					if (ItemProjectInsanity.itemStackable[removeId]) {
						c.getTradeAndDuel().tradeItem(removeId, removeSlot,
								c.playerItemsN[removeSlot]);
					} else {
						c.getTradeAndDuel().tradeItem(removeId, removeSlot, 28);
					}
				} else {
					if (ItemProjectInsanity.itemStackable[removeId] || ItemDef
							.forID(removeId).itemIsInNotePosition/*
																	 * Item.
																	 * itemIsNoteable
																	 * [removeId]
																	 */) {
						if (c.duelStatus == 1 || c.duelStatus == 2) {
							c.getTradeAndDuel().stakeItem(removeId, removeSlot,
									c.playerItemsN[removeSlot]);
						}
					} else {
						if (c.duelStatus == 1 || c.duelStatus == 2) {
							c.getTradeAndDuel().stakeItem(removeId, removeSlot,
									28);
						}
					}
				}
				break;

			case 3415:
				if (removeId < 0 || removeId > Config.ITEM_LIMIT) {
					return;
				}
				if (c.duelStatus <= 0) {
					if (ItemProjectInsanity.itemStackable[removeId]) {
						// for (GameItem item :
						// c.getTradeAndDuel().offeredItems) {
						// if (item.getId() == removeId) {
						c.getTradeAndDuel().fromTrade(removeId, removeSlot,
								Integer.MAX_VALUE);
						// }
						// }
					} else {
						// for (GameItem item :
						// c.getTradeAndDuel().offeredItems) {
						// if (item.getId() == removeId) {
						c.getTradeAndDuel().fromTrade(removeId, removeSlot, 28);
						// }
						// }
					}
				}
				break;

			case 7295:
				if (removeId < 0 || removeId > Config.ITEM_LIMIT) {
					return;
				}
				if (!c.isBanking) {
					return;
				}
				if (ItemProjectInsanity.itemStackable[removeId]) {
					c.getItems().bankItem(c.playerItems[removeSlot] - 1,
							removeSlot, c.playerItemsN[removeSlot]);
					c.getItems().resetItems(7423);
				} else {
					c.getItems().bankItem(c.playerItems[removeSlot] - 1,
							removeSlot,
							c.getItems().itemAmount(c.playerItems[removeSlot]));
					c.getItems().resetItems(7423);
				}
				break;

			case 6669:
				if (removeId < 0 || removeId > Config.ITEM_LIMIT) {
					return;
				}

				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.removeStakedItem(c, removeId, removeSlot, 28,
							interfaceId);
					break;
				}

				if (c.duelStatus == 1 || c.duelStatus == 2) {
					if (ItemProjectInsanity.itemStackable[removeId] || ItemDef
							.forID(removeId).itemIsInNotePosition/*
																	 * Item.
																	 * itemIsNoteable
																	 * [removeId]
																	 */) {
						for (GameItem item : c.getTradeAndDuel().stakedItems) {
							if (item.getId() == removeId) {
								c.getTradeAndDuel().fromDuel(removeId,
										removeSlot, item.getAmount());
							}
						}

					} else {
						c.getTradeAndDuel().fromDuel(removeId, removeSlot, 28);
					}
				}
				break;

			case 39502:
				if (c.isBobOpen) {
					c.getSummoning().withdrawItem(removeId, 30);
				}
				break;

		}
	}

}
