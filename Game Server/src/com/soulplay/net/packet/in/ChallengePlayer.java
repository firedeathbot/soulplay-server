package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.PacketType;

/**
 * Challenge Player
 **/
public class ChallengePlayer implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {

		switch (packetType) {
			case 128:
				final int answerPlayer = c.getInStream().readUnsignedShort();
				if (answerPlayer < 0 || answerPlayer >= Config.MAX_PLAYERS) {
					c.kickPlayer();
					return;
				}

				if (c.getTransformId() != -1 && !Config.isOwner(c)) {
					c.transform(-1);
					c.getMovement().resetWalkingQueue();
					c.getPA().resetMovementAnimation();
				}

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}
				if (c.noClip) {
					if (c.noClipX > 0 || c.noClipY > 0) {
						return;
					}
				}

				if (c.spectatorMode) {
					c.sendMessage("Can't do that in spectator mode.");
					return;
				}

				if (c.getAgility().doingAgility) {
					return;
				}

				if (c.logoutOnVerificationState())
					return;

				if (c.performingAction) {
					return;
				}

				if (c.duelStatus == 6) {
					return;
				}

				
				if (!c.getSpamTick().checkThenStart(1)) {
					c.sendMessage("Please slow down...");
					return;
				}

				if (answerPlayer == c.getId()) {
					return;
				}

				if (PlayerHandler.players[answerPlayer] == null) {
					return;
				}
				final Client o = (Client) PlayerHandler.players[answerPlayer];

				if (c.duelFightArenas() || c.duelStatus == 5
						|| c.duelStatus == 6) {
					c.sendMessage("You can't challenge inside the arena!");
					return;
				}
				
				c.walkingToMob = true;
				c.finalLocalDestX = c.finalLocalDestY = 0;
				c.destOffsetX = c.destOffsetY = 1;
				c.walkUpToPathBlockedMob = false;
				c.clickReachDistance = 1;

				c.followId = answerPlayer;
				
				walkToChallengePlayer(c, o);

				break;
		}
	}
	
	public static void walkToChallengePlayer(Client c, Client o) {
		if (c.skillingEvent != null)
			c.skillingEvent.stop();
		
		if (c.getMovement().wQueueWritePtr != (c.getMovement().wQueueWritePtr + 1) % c.getMovement().walkingQueueSize)
			PathFinder.findRoute(c, o.getX(), o.getY(), false, 0, 0, 1, 1, 0, 0, 0, 1);
		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			
			@Override
			public void stop() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (o == null || !o.isActive) {
					container.stop();
					return;
				}
					
				if (c.withinDistance(o, 1)) {
					if (c.inClanWarsLobby()) {
						if (c.clanWarOpponent > 0) {
							ClanWarHandler.challengeClicked(c);
							container.stop();
						}

					}
					if (c.inDuelArena()) {
						c.getTradeAndDuel().requestDuel(o);
						container.stop();
					}
				}
				
			}
		}, 1);
	}
}
