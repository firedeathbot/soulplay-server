package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 **/
public class MagicOnObjects implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		final int x = c.getInStream().readLEShort();
		final int spellId = c.getInStream().readUnsignedShortA();
		final int y = c.getInStream().readUnsignedShortA();
		final int id = c.getInStream().read3Bytes();

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		c.isIdle = false;

		if (c.performingAction) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (c.isDead()) {
			return;
		}
		
		if (!c.getStopWatch().checkThenStart(1)) {
			c.sendMessage("Please slow down.");
			return;
		}
		
		c.objectX = x;
		c.objectY = y;
		c.objectId = id;
		c.walkingToObject = true;
		c.clickObjectType = 17;
		c.spellOnObject = spellId;
		
	}

}
