package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.PacketType;

/**
 * Trading
 */
public class Trade implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		int tradeId = c.getInStream().readLEShort();

		c.isIdle = false;

		if (tradeId < 0 || tradeId >= Config.MAX_PLAYERS) {
			c.destinationX = 0;
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| !c.correctlyInitialized) {
			c.destinationX = 0;
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				c.destinationX = 0;
				return;
			}
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			c.destinationX = 0;
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			c.destinationX = 0;
			return;
		}

		if (c.performingAction) {
			c.destinationX = 0;
			return;
		}

		if (c.isLockActions()) {
			c.destinationX = 0;
			c.sendMessage("Your player is currently busy..");
			return;
		}
		
		if (tradeId == c.getId()) {// sending trade packet to self
			c.destinationX = 0;
			return;
		}

		if (tradeId < 1) {
			c.destinationX = 0;
			return;
		}
		
		if (c.logoutOnVerificationState()) {
			c.destinationX = 0;
			return;
		}

		/*
		 * if (PlayerHandler.players[tradeId].inWild()) {
		 * c.sendMessage("The other player is in the wilderness."); return; }
		 */
		if (c.isIronMan()) {
			c.sendMessage("IronMan Mode does not allow trading players.");
			c.destinationX = 0;
			return;
		}

		if (!c.getSpamTick().checkThenStart(1)) {
			c.sendMessage("Please slow down...");
			c.destinationX = 0;
			return;
		}

		c.getPA().resetFollow();

		if (c.duelFightArenas()) {
			c.sendMessage("You can't trade inside the arena!");
			c.destinationX = 0;
			return;
		}

		if (c.inTrade) {
			// c.getTradeAndDuel().declineTrade(true);
			c.getTradeAndDuel().declineTrade();
			c.sendMessage("Current trade has been declined.");
			c.destinationX = 0;
			return;
		}

//		c.followId = tradeId;
		
		c.walkingToMob = true;
		c.finalLocalDestX = c.finalLocalDestY = 0;
		c.destOffsetX = c.destOffsetY = 1;
		c.walkUpToPathBlockedMob = false;
		c.clickReachDistance = 1;
		
		final Client other = (Client) PlayerHandler.players[tradeId];

		if (other == null) {
			c.sendMessage("The other player is offline.");
			return;
		}

		c.face(other);
		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				if (other.disconnected) {
					container.stop();
					return;
				}
				if (c.withinDistance(other, 1) && !RegionClip.rayTraceBlocked(c.getX(), c.getY(), other.getX(), other.getY(), c.getZ(), false, c.getDynamicRegionClip())) {
					c.getTradeAndDuel().requestTrade(other);
					container.stop();
				}
			}
		}, 1);
		
	}

}
