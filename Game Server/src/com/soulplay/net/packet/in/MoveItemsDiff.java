package com.soulplay.net.packet.in;

import com.soulplay.content.player.skills.dungeoneeringv2.Binds;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class MoveItemsDiff implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int fromInterface = c.getInStream().readUnsignedShort();
		int toInterface = c.getInStream().readUnsignedShort();
		int from = c.getInStream().readUnsignedShort();
		int to = c.getInStream().readUnsignedShort();

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		c.isIdle = false;

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.isDead()) {
			return;
		}
		
		if (fromInterface != 29395) {
			return;
		}

		if (toInterface == 43952) {
			c.getBinds().deleteBind(from, () -> {Binds.open(c);});
		} else {
			c.getBinds().moveToLoudout(from, toInterface);
			c.getBinds().update();
		}

		if (c.inTrade) {
			c.getTradeAndDuel().declineTrade();
			return;
		}
		if (c.tradeStatus == 1) {
			c.getTradeAndDuel().declineTrade();
			return;
		}
		if (c.duelStatus == 1) {
			c.getTradeAndDuel().declineDuel();
			return;
		}
	}
}
