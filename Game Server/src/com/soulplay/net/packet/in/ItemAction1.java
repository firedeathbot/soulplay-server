package com.soulplay.net.packet.in;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.items.DonationItems;
import com.soulplay.content.items.Food;
import com.soulplay.content.items.Lamps;
import com.soulplay.content.items.Potions;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.DonationItems.DonationConsumable;
import com.soulplay.content.items.Food.FoodToEat;
import com.soulplay.content.items.Lamps.LampType;
import com.soulplay.content.items.clickitem.OpenBoxes;
import com.soulplay.content.items.clickitem.Potion;
import com.soulplay.content.items.degradeable.BrawlerGloveEnum;
import com.soulplay.content.items.itemdata.SlayerScrolls;
import com.soulplay.content.items.pos.BuyOffers;
import com.soulplay.content.items.pos.TradingPostButtons;
import com.soulplay.content.items.search.MarketSearch;
import com.soulplay.content.items.search.SearchBank;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.championsguild.ChampionsGuild;
import com.soulplay.content.minigames.clanwars.CWRules;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.minigames.flowers.FlowerGame;
import com.soulplay.content.minigames.gamble.GambleStage;
import com.soulplay.content.minigames.gamble.flowers.FlowerPlayer;
import com.soulplay.content.minigames.soulwars.Bandages;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.ClueManager;
import com.soulplay.content.minigames.treasuretrails.ClueType;
import com.soulplay.content.minigames.treasuretrails.CoordinateClue;
import com.soulplay.content.minigames.treasuretrails.MapClue;
import com.soulplay.content.minigames.treasuretrails.TreasureTrails;
import com.soulplay.content.npcs.impl.Barricades;
import com.soulplay.content.objects.impl.DwarfMultiCannon;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.pets.pokemon.Pokemon;
import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.pets.pokemon.PokemonStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonItems;
import com.soulplay.content.player.skills.herblore.Herblore;
import com.soulplay.content.player.skills.prayer.Prayer;
import com.soulplay.content.player.skills.runecrafting.PouchType;
import com.soulplay.content.player.skills.summoning.Creation;
import com.soulplay.content.player.wilderness.WildernessPackageLoot;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.attacks.NPCAttackDefinition;
import com.soulplay.game.model.npc.attacks.NPCAttackLoader;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.util.sql.SqlHandler;

/**
 * Clicking an item, bury bone, eat food etc
 **/
public class ItemAction1 implements PacketType {

	private static final Hit ROCK_CAKE = new Hit(1, 0, -1);
	private static final Animation DIG_ANIMATION = new Animation(830);
	private static final Animation MARIONETTE_ANIMATION = new Animation(3003);
	private static final Graphic GREEN_MARIONETTE_GFX = Graphic.create(515);
	private static final Graphic RED_MARIONETTE_GFX = Graphic.create(507);
	private static final Graphic BLUE_MARIONETTE_GFX = Graphic.create(511);
	private static final Animation KITE_ANIMATION = new Animation(8990);
	private static final Animation BROWN_HORSE = new Animation(920);
	private static final Animation WHITE_HORSE = new Animation(919);
	private static final Animation BLACK_HORSE = new Animation(920);
	private static final Animation GRAY_HORSE = new Animation(921);
	private static final Animation SPINNING_PLATE = new Animation(1902);
	private static final Animation SOUVENIR_MUG = new Animation(10942);
	private static final Graphic SLAYER_SCROLL_GFX = Graphic.create(974);

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int interfaceId = c.getInStream().readLEShortA();
		int itemSlot = c.getInStream().readUnsignedShortA();
		int itemId = c.getInStream().read3Bytes();

		if (c.debug) {
			c.sendMessage(String.format(
					"[ItemActionOne] item_id=%d slot=%d interface=%d", itemId, itemSlot,
					interfaceId));
		}

		if (!c.correctlyInitialized || c.inVerificationState) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.toggleBot) {
			if (c.getBot() == null) {
				c.sendMessage("Bot is null");
				return;
			}
			c = c.getBot();
		}

		if (c.performingAction && c.gambleStage != GambleStage.DUEL) {
			c.sendMessage("Your player is currently busy.");
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (itemSlot < 0 || itemSlot >= c.playerItems.length) {
			return;
		}

		if (c.duelStatus == 6) {
			return;
		}

		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			return;
		}

		if (c.tourStep > 0 && c.tourStep < 100) {
			c.getDH().sendOption2("I would like to stop the tour.",
					"Please continue the tour.");
			return;
		}

		if (c.isDead()) {
			return;
		}

		c.resetSkillingEvent();
		c.isIdle = false;

		if (interfaceId == 789) { // item search chat interface
			if (itemSlot == 0) { // Select Search Item
				if (itemId < 0) {
					return;
				}
				if (c.isBanking) {
					SearchBank.searchBank(c, itemId);
					return;
				}
				if (c.getMarketOffer().isActive()) {
					BuyOffers.selectSearchedItem(c, itemId);
					c.getPacketSender().sendInputDialogState(0);
					return;
				}
				if (c.interfaceIdOpenMainScreen == 45000) { // if market
															// interface open,
															// then search
															// markets

					ItemDef searchedDef = ItemDef.forID(itemId);

					if (searchedDef == null || searchedDef.name == null) {
						c.getPA().closeAllWindows();
						c.sendMessage("Error finding that item.");
						return;
					}

					MarketSearch.searchMarket(c, searchedDef.getName());

					return;
				} else if (c.interfaceIdOpenMainScreen == 19600) {
					if (!TradingPostButtons.tradingPostEnabled(c))
						return;
					if (c.getStopWatch().checkThenStart(2)) {
						c.getMarketOffer().getTimer().reset();
						BuyOffers.loadPublicOffers(c, itemId);
						c.getMarketOffer().setBuyOfferItemId(itemId);
						c.getPacketSender().sendFrame126b("Filter: " + ItemConstants.getItemName(itemId), 19603);
					}
					c.getPA().closeChatInterface();
					return;
				}

			} else if (itemSlot == 1) { // admin spawn command
				if (c.isAdmin() || c.isOwner()) {
					c.getItems().addItem(itemId, 1);
				}
			}
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}
		
		c.setLastClicked(itemId);

		if (!c.getItems().playerHasItem(itemId, itemSlot, 1)) {
			return;
		}

		FlowerPlayer flowerPlayer = c.gambleFlowerPlayer;
		if (flowerPlayer != null) {
			if (itemId == 299) {
				if (!flowerPlayer.plantFlower()) {
					c.sendMessage("You've planted all flowers you could.");
				}
				return;
			}

			if (itemId != 299 && c.performingAction) {
				c.sendMessage("Your player is currently busy.");
				return;
			}
		}
		
		final PluginResult<ItemClickExtension> pluginResult = PluginManager.search(ItemClickExtension.class, itemId);
		final Player player = c;
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOption1(player, itemId, itemSlot, interfaceId))) {
			return;
		}
		
		if (Herblore.cleanHerb(c, itemId, itemSlot)) {
			return;
		}
		if (RunePouch.handleRunePouchOpen(c, itemId)) {
			return;
		}
		if (MagicalCrate.handleOpenMagicalCrate(c, itemId)) {
			return;
		}

		Potion potion = Potion.forId(itemId);
		if (potion != null) {
			Item item = new Item(itemId, 1, itemSlot);
			potion.drink(c, item);
			return;
		}

		if (Potions.isPotion(itemId)) {
			c.getPotions().handlePotion(itemId, itemSlot);
			return;
		}

		switch (itemId) {
			case 111883: {
				if (player.getItems().deleteItem2(itemId, 1)) {
					player.getItems().addItem(313, 100);
					player.sendMessageSpam("You open the Bait pack and find 100 Fishing bait.");
				}
				return;
			}
			case 113432: {
				if (player.getItems().deleteItem2(itemId, 1)) {
					player.getItems().addItem(113431, 100);
					player.sendMessageSpam("You open the Sandworms pack and find 100 Sandworms.");
				}
				return;
			}
			case 120724: {
				if (c.duelRule[DuelRules.NO_DRINKS]) {
					c.sendMessage("You may not drink potions in this duel.");
					return;
				}

				if (c.clanWarRule[CWRules.NO_DRINKS]) {
					c.sendMessage("You may not drink potions in this war.");
					return;
				}

				if (c.getImbuedHeartTimer() > 0) {
					int min = (c.getImbuedHeartTimer() * 600 / 1000) / 60;
					c.sendMessage("The heart is still drained of its power. Judging by how it feels, it will be ready in");
					c.sendMessage(min < 1 ? "less than a minute." : (min == 1 ? "around one minute." : "around " + min + " minutes."));
					return;
				}

				c.getSkills().updateLevel(Skills.MAGIC, (int) Math.floor(1 + (player.getSkills().getStaticLevel(Skills.MAGIC) * 0.10)));
				c.setImbuedHeartTimer(700);//7 minutes
				c.startGraphic(Graphic.osrs(1316));
				return;
			}
		case 21403: {
			Pokemon pokemon = c.summonedPokemon;
			if (pokemon == null) {
				c.sendMessage("You need a pet summoned to use this.");
				return;
			}

			if (c.summoned.powerUp != null) {
				c.sendMessage("Your pet already has the boost active.");
				return;
			}

			final Client finalC = c;
			int oldHp = pokemon.stats.hp;
			int oldMaxHp = c.summoned.getSkills().getStaticLevel(Skills.HITPOINTS);
			int oldCurrentHp = c.summoned.getSkills().getLifepoints();
			c.summoned.powerUp = CycleEventHandler.getSingleton().addEvent(c.summoned, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					NPC summoned = finalC.summoned;
					if (summoned == null || pokemon == null) {
						container.stop();
						return;
					}

					summoned.powerUp = null;
					pokemon.updateScale(finalC);
					pokemon.updateDefence(finalC);
					pokemon.updatePokemonStats(finalC);

					pokemon.stats.hp = oldHp;
					summoned.getSkills().setStaticLevelClean(Skills.HITPOINTS, oldMaxHp);
					summoned.getSkills().setLifepoints(oldCurrentHp);

					finalC.sendMessage("Your pet boost has ran out.");
					container.stop();
				}

				@Override
				public void stop() {
					/* empty */
				}

			}, 200);
			pokemon.stats.hp += Pokemon.BOOST_HP;
			c.summoned.getSkills().setStaticLevelClean(Skills.HITPOINTS, c.summoned.getSkills().getStaticLevel(Skills.HITPOINTS) + Pokemon.BOOST_HP);
			c.summoned.getSkills().heal(Pokemon.BOOST_HP);

			pokemon.updateScale(c);
			pokemon.updateDefence(c);
			pokemon.updatePokemonStats(c);
			c.sendMessage("Your pet enrages!");
			c.getItems().deleteItem2(21403, 1);
			return;
		}
		case 21821: {
			if (c.summonedPokemon == null) {
				c.sendMessage("You need a pet summoned to reset its stats.");
				return;
			}

			if (c.summoned.powerUp != null) {
				c.sendMessage("You can't reset your pet stats while boost is active.");
				return;
			}

			final Client finalC = c;
			c.getDialogueBuilder().sendStatement("Would you like to reset your pet stats?", "This would <col=ff0000>consume</col> the scroll.").sendOption("Yes.", () -> {
				if (!finalC.getItems().playerHasItem(21821)) {
					finalC.sendMessage("You don't have a scroll to use.");
					return;
				}

				Pokemon pokemon = finalC.summonedPokemon;
				if (pokemon == null) {
					return;
				}

				int hpMultiplier = 1;
				NPCAttackDefinition attack = NPCAttackLoader.getPokemon(finalC.summoned.npcType);
				if (attack != null) {
					int styles = attack.countAttacks();
					if (styles == 1) {
						hpMultiplier = 3;
					} else if (styles == 2) {
						hpMultiplier = 2;
					}
				}

				PokemonStats baseStat = pokemon.data.baseStats;
				int points = (pokemon.stats.hp - baseStat.hp) / hpMultiplier + (pokemon.stats.strength - baseStat.strength) + (pokemon.stats.magic - baseStat.magic) + (pokemon.stats.range - baseStat.range);
				pokemon.stats.hp = baseStat.hp;
				pokemon.stats.strength = baseStat.strength;
				pokemon.stats.range = baseStat.range;
				pokemon.stats.magic = baseStat.magic;
				pokemon.statPoints += points * 10;
				pokemon.updatePokemonStats(finalC);
				finalC.getItems().deleteItem2(21821, 1);
			}, "NO.", () -> {}).execute();
			return;
		}
		case 290:
			if (player.wildLevel > 0) {
				player.sendMessage("You must be in a safe zone to open this package.");
				return;
			}

			if (player.getItems().deleteItem2(290, 1)) {
				WildernessPackageLoot.loot(c);
			}
			return;
		case 3144:
			Food.eatKaramb(c, itemId, itemSlot);
			return;
		case 30100:
			int freeSlots = 2;
			if (c.getItems().playerHasItem(565)) {
				freeSlots -= 1;
			}
			if (c.getItems().playerHasItem(560)) {
				freeSlots -= 1;
			}
			if (c.getItems().playerHasItem(562)) {
				freeSlots -= 1;
			}
			if (freeSlots < 0) {
				freeSlots = 0;
			}
			if (c.getPA().freeSlots() >= freeSlots) {
			    if (c.getItems().deleteItemInOneSlot(30100, 1)) {
			    	c.getItems().addItem(562, 50);
				    c.getItems().addItem(560, 50);
				    c.getItems().addItem(565, 50);
			    }
			} else {
				c.sendMessage("Not enough inventory space.");
			}
			return;
		case 30101:
			int freeSlot = 3;
			if (c.getItems().playerHasItem(556)) {
				freeSlot -= 1;
			}
			if (c.getItems().playerHasItem(555)) {
				freeSlot -= 1;
			}
			if (c.getItems().playerHasItem(557)) {
				freeSlot -= 1;
			}
			if (c.getItems().playerHasItem(554)) {
				freeSlot -= 1;
			}
			if (freeSlot < 0) {
				freeSlot = 0;
			}
			if (c.getPA().freeSlots() >= freeSlot) {
			    if (c.getItems().deleteItemInOneSlot(30101, 1)) {
			    	c.getItems().addItem(556, 100);
				    c.getItems().addItem(555, 100);
				    c.getItems().addItem(557, 100);
				    c.getItems().addItem(554, 100);
			    }
			} else {
				c.sendMessage("Not enough inventory space.");
			}
			return;
		case 30102:
			if (c.getItems().deleteItemInOneSlot(30102, 1)) {
				c.getItems().addItem(890, 50);
			}
			return;
		case 30103:
			if (c.getItems().deleteItemInOneSlot(30103, 1)) {
				c.getItems().addItem(892, 50);
			}
			return;
		case 30104:
			if (c.getItems().deleteItemInOneSlot(30104, 1)) {
				c.sendMessage("A strange wisdom is unleashed from the tablet and fills your mind.");
				c.startGraphic(Graphic.create(678));
				c.getPA().resetAutocast();
				c.setSpellbook(SpellBook.ANCIENT);
			}
			return;
		    case 20061:
				if (c.summoned != null) {
					c.sendMessage("You can't open the box when you have a pet following you.");
					return;
				}

				if (c.infernoManager != null || c.raidsManager != null) {
					c.sendMessage("You can't do this here.");
					return;
				}

		    	List<PokemonData> availablePokemons = new ArrayList<>();
		    	for (PokemonData data : PokemonData.data) {
		    		if (data == PokemonData.TUTORIAL_GOBLIN) {
		    			continue;
		    		}

		    		if (!c.isPokemonUnlocked(data)) {
		    			availablePokemons.add(data);
		    		}
		    	}
		    	
		    	int size = availablePokemons.size();
		    	if (size <= 0) {
		    		c.sendMessage("You've unlocked all pets in the bolt pouch!");
		    		return;
		    	}
		  
		    	PokemonData unlocked = availablePokemons.get(Misc.randomNoPlus(size));
		    	
		    	c.getItems().deleteItemInOneSlot(20061, 1);
		    	c.unlockPokemon(unlocked);
		    	c.sendMessage("Unlocked " + unlocked);
		    	PokemonButtons.spawnPokemon(c, unlocked, false);
		    	return;
			
		    case 20429:
			    c.sendMessage("Only pets can eat a fury shark.");
			    return;
			case 299:// mithril seeds
				c.getDialogueBuilder().reset();
				if (ObjectManager.getObject(c.getX(), c.getY(), c.getZ()) != null || c.getZones().isInGambleZone()) {
					c.sendMessage("You cannot plant here.");
					return;
				}
				FlowerGame.plantFlower(c);
				return;
			case 15707:
				Party.updateSidebar(c, true);
				return;
			case 4049:
				if (c.inCw()) {
					CastleWars.useBandages(c);
				} else if (c.inSoulWars()) {
					Bandages.useSWBandages(c);
				}
				return;
			case 4053:
				if (RSConstants.inRedGraveyardRoomDeath(c.absX, c.absY) || RSConstants.inBlueGraveyardDeath(c.absX, c.absY) || RSConstants.inRedGraveyardRoom(c.absX, c.absY)
						|| RSConstants.inBlueGraveyardRoom(c.absX, c.absY)) {
					c.sendMessage("You cannot place barricades here.");
					return;
				}
				if (RSConstants.inSoulWars(c.getX(), c.getY())) {
					Barricades.setupBarricade(c, false);
				} else {
					Barricades.setupBarricade(c, true);
				}
				return;
			case 9433:
				c.getPacketSender().showInterface(40262);
				PokemonButtons.refreshData(c);
				return;
			case 12844:
				c.startAnimation(KITE_ANIMATION);
				return;
			case 2526:
				c.startAnimation(GRAY_HORSE);
				return;
			case 2524:
				c.startAnimation(BLACK_HORSE);
				return;
			case 2522:
				c.startAnimation(WHITE_HORSE);
				return;
			case 2520:
				c.startAnimation(BROWN_HORSE);
				return;
			case 4613:
				c.startAnimation(SPINNING_PLATE);
				return;
			case 20725:
				c.startAnimation(SOUVENIR_MUG);
				return;
			case 20718:
				c.startAnimation(new Animation(10952));
				c.startGraphic(Graphic.create(1341));
				return;
			case 18338:
				c.sendMessage("You need to empty the bag.");
				return;
			case 19967: //nomad bags
				//c.getDH().sendDialogues(19967, c.npcType);
				//repurposed
				int[] boxes = { 3062, 6542, 15501, 6199 };
				int random = Misc.random(boxes);
				if (c.getItems().deleteItem2(itemId, 1)) {
					c.getItems().addOrDrop(new GameItem(random, 1));
				}
				return;
			case 21776:
				if (!c.getItems().playerHasItem(21776, itemSlot, 100)) {
					c.sendMessage("You need at least 100 Shards of armadyl to combine.");
					return;
				}

				if (c.getItems().deleteItemInOneSlot(21776, itemSlot, 100)) {
					c.getItems().addItem(21775, 1);
					c.sendMessage("You combine 100 Shards of armadyl");
				}
				return;
			case 14664: // christmas present
				c.sendMessage("You open your present under the Christmas tree!");
				c.getItems().deleteItemInOneSlot(14664, itemSlot, 1);
				c.getItems().addItem(6860, 1);
				c.getItems().addItem(6861, 1);
				return;
			case 11949:
				c.getPA().snowGlobeShake(c);
				return;
			case 6865:
				c.startAnimation(MARIONETTE_ANIMATION);
				c.startGraphic(BLUE_MARIONETTE_GFX);
				c.sendMessage("You make your Marionette jump.");
				return;
			case 6867:
				c.startAnimation(MARIONETTE_ANIMATION);
				c.startGraphic(RED_MARIONETTE_GFX);
				c.sendMessage("You make your Marionette jump.");
				return;
			case 6866:
				c.startAnimation(MARIONETTE_ANIMATION);
				c.startGraphic(GREEN_MARIONETTE_GFX);
				c.sendMessage("You make your Marionette jump.");
				return;
			case 4155:// slayer gem
				c.getDH().sendOption4("Take me to a slayer master",
						"Tell me about duo slayer", "Kills left", "Slayer info");
				c.dialogueAction = 4255;
				return;
			case 607:
				if (c.getDropRateBoostLength() < 1) {
					c.sendMessage("<col=ff0000>You read the drop boost scroll that lasts for 1 hour.");
					c.getItems().deleteItemInOneSlot(607, itemSlot, 1);
					c.setDropRateBoostLength(6000);
				} else {
					c.sendMessage("<col=ff0000>You already have drop boost active.");
				}
				return;
			case 761:// double exp thing for 30 minutes
				if (c.getDoubleExpLength() < 1) {
				c.sendMessage("<col=ff0000>You read the double exp scroll that lasts for 1 hour.");
				c.getItems().deleteItemInOneSlot(761, itemSlot, 1);
				c.setDoubleExpLength(6000);
				} else {
					c.sendMessage("<col=ff0000>You already have double exp active.");
				}
				return;
			case 6801:
				c.sendMessage("opening scroll... starting.....");
				c.championScrollUsed = itemId;
				ChampionsGuild.spawnChampion(c);
				return;
			case 30223:
			case 30227:
			case 30234: {
				player.getTreasureTrailManager().openClueThingy(ClueDifficulty.ELITE, itemId);
				return;
			}
			case 30222:
			case 30226:
			case 30233: {
				player.getTreasureTrailManager().openClueThingy(ClueDifficulty.HARD, itemId);
				return;
			}
			case 30221:
			case 30225:
			case 30232: {
				player.getTreasureTrailManager().openClueThingy(ClueDifficulty.MEDIUM, itemId);
				return;
			}
			case 30220:
			case 30224:
			case 30231: {
				player.getTreasureTrailManager().openClueThingy(ClueDifficulty.EASY, itemId);
				return;
			}
			case 952:
				c.startAnimation(DIG_ANIMATION);

				for (ClueDifficulty difficulty : ClueDifficulty.values) {
					int data = c.getTreasureTrailManager().getCluesData()[difficulty.getIndex()];
					if (data == -1) {
						continue;
					}

					ClueType clueType = ClueManager.clues.get(difficulty.getIndex()).get(Misc.getFirst(data)).get(Misc.getSecond(data));
					if (clueType instanceof CoordinateClue || clueType instanceof MapClue) {
						clueType.checkComplete(player, difficulty);
					}
				}

				if (WorldType.equalsType(WorldType.SPAWN)) {
					c.sendMessage("Barrows minigame has been disabled for SoulPvP");
					return;
				}

				final Client c2 = c;
				Server.schedule(() -> c2.getBarrows().spadeDigging());
				return;
			case 15262:
				c.getItems().deleteItemInOneSlot(15262, itemSlot, 1);
				c.getItems().addItem(Creation.SHARD_ID, 100000);
				return;
			case 1856:
				c.getDH().sendDialogues(1856, 945);
				return;
			case 2528:
				Lamps.open(c, LampType.REGULAR_LAMP);
				return;
			case 4447:
				Lamps.open(c, LampType.ANTIQUE_LAMP);
				return;
			case 18782:
				Lamps.open(c, LampType.DRAGONKIN_LAMP);
				return;
			case 744:
				if (Config.isValentines()) {
					c.sendMessage("Happy Valentine!");
				}

				c.getDH().sendStatement("\"I love you Ray\" ~ Ray");
				c.sendMessage("The heart glows and grants you a short xp boost, vanishing in the process");
				c.setDoubleExpLength(c.getDoubleExpLength() + 120);
				c.getItems().deleteItemInOneSlot(744, itemSlot, 1);
				return;
			// START TAX BAGS
			case 10831:
				c.sendMessage("<col=1532693>This bag is empty!</col>");
				return;
			case 10832:
				if (c.getItems().itemAmountLoweredID(995) >= 1647000000) {
					c.sendMessage("You have too much GP in your inventory.");
					return;
				}

				if (c.getItems().freeSlots() < 1) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(10832, itemSlot, 1);
				c.getItems().addItem(10831, 1);
				c.getItems().addItem(995, 500000000);
				c.sendMessage("<col=1532693>You open the bag and receive 500 Million Coins!</col>");
				return;
			case 10833:
				if (c.getItems().itemAmountLoweredID(995) >= 1147000000) {
					c.sendMessage("You have too much GP in your inventory.");
					return;
				}

				if (c.getItems().freeSlots() < 1) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(10833, itemSlot, 1);
				c.getItems().addItem(10831, 1);
				c.getItems().addItem(995, 1000000000);
				c.sendMessage("<col=1532693>You open the bag and receive 1 Billion Coins!</col>");
				return;
			case 10834:
				if (c.getItems().itemAmountLoweredID(995) >= 647000000) {
					c.sendMessage("You have too much GP in your inventory.");
					return;
				}

				if (c.getItems().freeSlots() < 1) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(10834, itemSlot, 1);
				c.getItems().addItem(10831, 1);
				c.getItems().addItem(995, 1500000000);
				c.sendMessage("<col=1532693>You open the bag and receive 1.5 Billion Coins!</col>");
				return;
			case 10835:
				if (c.getItems().itemAmountLoweredID(995) >= 147000000) {
					c.sendMessage("You have too much GP in your inventory.");
					return;
				}

				int slots = 2;

				if (c.getItems().playerHasItem(995)) {
					slots--;
				}

				if (c.getItems().freeSlots() < slots) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(10835, itemSlot, 1);
				c.getItems().addItem(10831, 1);
				c.getItems().addItem(995, 2000000000);
				c.sendMessage("<col=1532693>You open the bag and receive 2 Billion Coins!</col>");
				return;
			// END TAX BAGS

			// Start Bird nests
			case 5073:
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.getItems().addItem(5075, 1);
				c.getItems().addItem(RSConstants.birdNestSeeds[Misc.randomNoPlus(RSConstants.birdNestSeeds.length)], 1 + Misc.random(3));
				c.sendMessage("You search the nest");
				return;
			case 5074:
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.getItems().addItem(5075, 1);
				c.getItems().addItem(RSConstants.birdNestRings[Misc.randomNoPlus(RSConstants.birdNestRings.length)], 1);
				c.sendMessage("You search the nest");
				return;
			case 5070:
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.getItems().addItem(5075, 1);
				c.getItems().addItem(5076, 1);
				c.sendMessage("You search the nest");
				return;
			case 5071:
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.getItems().addItem(5075, 1);
				c.getItems().addItem(5078, 1);
				c.sendMessage("You search the nest");
				return;
			case 5072:
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.getItems().addItem(5075, 1);
				c.getItems().addItem(5077, 1);
				c.sendMessage("You search the nest");
				return;
			case 11966:
				if (c.getItems().freeSlots() < 2) {
					c.sendMessage("You don't have enough room in your Inventory.");
					return;
				}

				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.getItems().addItem(5075, 1);
				c.getItems().addItem(11964, 1);
				c.sendMessage("You search the nest");
				return;
				// frozen key
			case 20121:
				if (c.getItems().playerHasItem(20122, 1) && c.getItems().playerHasItem(20123, 1)
						&& c.getItems().playerHasItem(20124, 1)) {
					c.getItems().deleteItemInOneSlot(20121, 1);
					c.getItems().deleteItemInOneSlot(20122, 1);
					c.getItems().deleteItemInOneSlot(20123, 1);
					c.getItems().deleteItemInOneSlot(20124, 1);
					c.getItems().addItem(20120, 1);
					c.sendMessage("You put all four frozen key parts together and create a frozen key!");
					return;
				}
				return;
			case 20122:
				if (c.getItems().playerHasItem(20121, 1) && c.getItems().playerHasItem(20123, 1)
						&& c.getItems().playerHasItem(20124, 1)) {
					c.getItems().deleteItemInOneSlot(20121, 1);
					c.getItems().deleteItemInOneSlot(20122, 1);
					c.getItems().deleteItemInOneSlot(20123, 1);
					c.getItems().deleteItemInOneSlot(20124, 1);
					c.getItems().addItem(20120, 1);
					c.sendMessage("You put all four frozen key parts together and create a frozen key!");
					return;
				}
				return;
			case 20123:
				if (c.getItems().playerHasItem(20121, 1) && c.getItems().playerHasItem(20122, 1)
						&& c.getItems().playerHasItem(20124, 1)) {
					c.getItems().deleteItemInOneSlot(20121, 1);
					c.getItems().deleteItemInOneSlot(20122, 1);
					c.getItems().deleteItemInOneSlot(20123, 1);
					c.getItems().deleteItemInOneSlot(20124, 1);
					c.getItems().addItem(20120, 1);
					c.sendMessage("You put all four frozen key parts together and create a frozen key!");
					return;
				}
				return;
			case 20124:
				if (c.getItems().playerHasItem(20121, 1) && c.getItems().playerHasItem(20122, 1)
						&& c.getItems().playerHasItem(20123, 1)) {
					c.getItems().deleteItemInOneSlot(20121, 1);
					c.getItems().deleteItemInOneSlot(20122, 1);
					c.getItems().deleteItemInOneSlot(20123, 1);
					c.getItems().deleteItemInOneSlot(20124, 1);
					c.getItems().addItem(20120, 1);
					c.sendMessage("You put all four frozen key parts together and create a frozen key!");
					return;
				}
				return;
			case 6:
				DwarfMultiCannon.setupCannon(c);
				return;
			case 7956:
				if (c.getPA().freeSlots() > 0) {
					c.getItems().deleteItemInOneSlot(7956, itemSlot, 1);
					c.getItems().addItem(14598, 5);
				} else {
					c.sendMessage("You dont have enough space in your inventory.");
				}
				return;
			case 9721:
				c.yellPoints++;
				c.getItems().deleteItemInOneSlot(9721, itemSlot, 1);
				c.sendMessage("You have gained 1 yell!");
				return;
			case 786:
				DonationItems.clickItem(c, DonationConsumable.SCROLL_10);
				return;
			case 1505:
				DonationItems.clickItem(c, DonationConsumable.SCROLL_50);
				return;
			case 2396:
				DonationItems.clickItem(c, DonationConsumable.SCROLL_100);
				return;
			/* Mystery box */
			case 6199:
				c.giveReward();
				return;
			case 15501:
				c.skillingReward();
				return;
			case 15246:
				OpenBoxes.charmPackage(c);
				return;
			case 30174:
				OpenBoxes.superMysteryBox(c);
				return;
			case 30175:
				OpenBoxes.legendaryMysteryBox(c);
				return;
			case 30176:
				OpenBoxes.ultraMysteryBox(c);
				return;
			case 6542:
				c.pkingReward();
				return;
			case 15389:
				if (c.getDoubleExpLength() < 1) {
					if (BrawlerGloveEnum.get(c.playerEquipment[PlayerConstants.playerHands]) != null) {
						c.sendMessage("You cannot use lamps with Brawling gloves on.");
						return;
					}
					if (c.getItems().playerHasItem(15389)) {
						c.getPA().addSkillXP((c.getPlayerLevel()[Skills.DUNGEONEERING] * 6) * 5, Skills.DUNGEONEERING);
						c.getItems().deleteItem2(15389, 1);
						c.sendMessage("You rub the experience lamp and receive some dungeoneering experience");
					}
				} else {
					c.sendMessage("You cannot use lamp with double exp on.");
				}
				return;
			case 15390:
				if (c.getDoubleExpLength() < 1) {
					if (BrawlerGloveEnum.get(c.playerEquipment[PlayerConstants.playerHands]) != null) {
						c.sendMessage("You cannot use lamps with Brawling gloves on.");
						return;
					}
					if (c.getItems().playerHasItem(15390)) {
						c.getPA().addSkillXP((c.getPlayerLevel()[Skills.HUNTER] * 6) * 5, Skills.HUNTER);
						c.getItems().deleteItem2(15390, 1);
						c.sendMessage("You rub the experience lamp and receive some hunter experience");
					}
				} else {
					c.sendMessage("You cannot use lamp with double exp on.");
				}
				return;
			case 6543:
				if (c.getDoubleExpLength() < 1) {
					if (BrawlerGloveEnum.get(c.playerEquipment[PlayerConstants.playerHands]) != null) {
						c.sendMessage("You cannot use lamps with Brawling gloves on.");
						return;
					}
					if (c.getItems().playerHasItem(6543)) {
						c.getItems().deleteItemInOneSlot(6543, 1);
						if (c.getSkills().getStaticLevel(Skills.SLAYER) < 99) {
							c.sendMessage("The lamp mysteriously vanishes... You recive some slayer experience");
							c.getPA().addSkillXP((c.getPlayerLevel()[Skills.SLAYER] * 10) * 5, Skills.SLAYER);
						} else if (c.getSkills().getStaticLevel(Skills.SLAYER) == 99) {
							c.getPA().addSkillXP((c.getPlayerLevel()[Skills.SLAYER] * 6) * 5, Skills.SLAYER);
							c.sendMessage("The lamp mysteriously vanishes... You recive some slayer experience");
						}
					}
				} else {
					c.sendMessage("You cannot use lamp with double exp on.");
				}
				return;
			/* Coin box */
			case 3062:
				if (c.getItems().freeSlots() > 0
						|| c.getItems().playerHasItem(995)) {
					c.coinReward();
				} else {
					c.sendMessage("You need at least 1 free slot in your inventory.");
				}
				return;
			case 405:
				if (WorldType.equalsType(WorldType.SPAWN)) {
					return;
				}

				c.getItems().deleteItemInOneSlot(405, itemSlot, 1);
				c.getItems().addItem(995, 2500000);
				return;
			case 13663:
				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				try {
					SqlHandler.buySpin(c, Config.WHEEL_TABLE_NAME);
				} catch (Exception e) {
					e.printStackTrace();
				}
				c.sendMessage("You have received 1 spin, you can use your spin at ::sof");
				return;
			case 6950:
				if (c.duelRule[DuelRules.NO_MAGIC]) {
					c.sendMessage("Magic isn't allowed.");
					return;
				}
				
				c.getPA().castVeng(true);
				return;

			case 7509:// dwarven rock cake
            case 7510:
				if (System.currentTimeMillis() - c.anyDelay2 >= 500 && c.getSkills().getLifepoints() > 1) {
					c.startAnimation(Potion.DRINK_ANIM);
					c.getPacketSender().playSound(Sounds.ROCK_CAKE);
					c.getPacketSender().playSound(Sounds.PLAYER_SMALL_DAMAGE);
					c.dealDamage(ROCK_CAKE);
					c.forceChat("Ow! I nearly broke a tooth!");
					c.anyDelay2 = System.currentTimeMillis();
				}
				return;

			case 5509:
				c.getRunecrafting().getPouch(PouchType.SMALL).fill();
				return;
			case 5510:
				c.getRunecrafting().getPouch(PouchType.MEDIUM).fill();
				return;
			case 5512:
				c.getRunecrafting().getPouch(PouchType.LARGE).fill();
				return;
			case 5514:
				c.getRunecrafting().getPouch(PouchType.GIANT).fill();
				return;
			case 2714:// Easy Clue Scroll Casket
				if (c.getItems().freeSlots() > 3) {
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					TreasureTrails.addClueReward(c, 0);
				} else {
					c.sendMessage("You need 3 free slots in your inventory!");
				}
				return;
			case 2802: // Medium Clue Scroll Casket
					if (c.getItems().freeSlots() > 3) {
						c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
						TreasureTrails.addClueReward(c, 1);
					} else {
						c.sendMessage("You need 3 free slots in your inventory!");
					}
				return;
			case 2775: // Hard Clue Scroll Casket
					if (c.getItems().freeSlots() > 3) {
						c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
						TreasureTrails.addClueReward(c, 2);
					} else {
						c.sendMessage("You need 3 free slots in your inventory!");
					}
					return;
				// hard clues
			case 2737:
				if (c.inArea(2969, 3411, 2974, 3415)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2735, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(17537);
				}
				return;
			case 2735:
				if (c.inArea(2613, 3075, 2619, 3080)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2733, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(9043);
				}
				return;
			case 2733:
				if (c.inArea(3030, 3394, 3049, 3401)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3524, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(7271);
				}
				return;
			case 2731:
				if (c.inArea(3285, 3371, 3291, 3375)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3530, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(7045);
				}
				return;
			case 2729:
				if (c.inArea(3106, 3148, 3113, 3154)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2727, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(9275);
				}
				return;
			case 2727:
				if (c.inArea(3092, 3213, 3104, 3225)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2775, 1);
					c.setClueAmount(TreasureTrails.HIGH_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.HIGH_CLUE)
							+ "</col> hard clue scrolls.");
					c.sendMessage("You receive a HARD Casket!");
				} else {
					c.getPacketSender().showInterface(7113);
				}
				return;
			case 2725:
				if (c.inArea(2719, 3336, 2725, 3339)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2723, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(17634);
				}
				return;
			case 2723:
				if (c.inArea(3301, 3684, 3313, 3698)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3520, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					c.getPacketSender().showInterface(17620);
				}
				return;
			case 2722:
				if (c.inArea(2903, 3287, 2909, 3300)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2775, 1);
					c.setClueAmount(TreasureTrails.HIGH_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.HIGH_CLUE)
							+ "</col> hard clue scrolls.");
					c.sendMessage("You receive a HARD Casket!");
				} else {
					c.getPacketSender().showInterface(4305);
				}
				return;
	
			// danny hard clues
			case 3520:
				if (c.inArea(2977, 3760, 2983, 3766)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2722, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("I wonder what's inside in these", 6971);
					c.getPacketSender().sendFrame126("coffins in the wilderness.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3522:
				if (c.inArea(2950, 3816, 2958, 3826)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3550, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Deep Wilderness chaos altar.", 6971);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3524:
				if (c.inArea(3013, 3955, 3020, 3963)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3532, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Treasure lies in the ship in ", 6971);
					c.getPacketSender().sendFrame126("the Wilderness", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3525:
				if (c.inArea(3361, 3930, 3373, 3941)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2775, 1);
					c.setClueAmount(TreasureTrails.HIGH_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.HIGH_CLUE)
							+ "</col> hard clue scrolls.");
					c.sendMessage("You have received a HARD Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("The bridge will fall in the lava,", 6971);
					c.getPacketSender().sendFrame126("it must be repaired...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3526:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126("Tables lots of tables and chairs...", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3528:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126("Bring back my birthday cake, the werewolf took it.", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3530:
				if (c.inArea(3155, 3629, 3161, 3641)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2729, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("The water looks like eyes in the ...", 6971);
					c.getPacketSender().sendFrame126("Wilderness", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3532:
				if (c.inArea(3097, 3622, 3102, 3626)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2775, 1);
					c.setClueAmount(TreasureTrails.HIGH_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.HIGH_CLUE)
							+ "</col> hard clue scrolls.");
					c.sendMessage("You have received a HARD Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Flowers grows in the Wilderness near", 6971);
					c.getPacketSender().sendFrame126("statue of warrior,", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3534:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126("Light up the torches, there must be something going on...", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3536:
				if (c.inArea(3234, 3604, 3246, 3614)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3558, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Go to the chaos altar in the", 6971);
					c.getPacketSender().sendFrame126("Wilderness and do some research.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3538:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126("Only blood and skeletons left, we shouldn't give up now...", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3540:
				if (c.inArea(3154, 3724, 3162, 3731)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2775, 1);
					c.setClueAmount(TreasureTrails.HIGH_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.HIGH_CLUE)
							+ "</col> hard clue scrolls.");
					c.sendMessage("You have received a HARD Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("In Wilderness lies big bones", 6971);
					c.getPacketSender().sendFrame126("they must be from a dragon.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3542:
				if (c.inArea(3383, 3879, 3355, 3896)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2775, 1);
					c.setClueAmount(TreasureTrails.HIGH_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.HIGH_CLUE)
							+ "</col> hard clue scrolls.");
					c.sendMessage("You have received a HARD Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("There are alot of broken", 6971);
					c.getPacketSender().sendFrame126("walls in 50+ Wilderness", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3544:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126("The pillars cannot stand for too long in the lava...", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3546:
				if (c.inArea(3225, 3828, 3227, 3835)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3525, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("This must be a dragon egg, it's big!", 6971);
					// c.getPacketSender().sendFrame126("...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3548:
				if (c.inArea(3163, 3879, 3171, 3884)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3542, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("In Wilderness there is a cave", 6971);
					c.getPacketSender().sendFrame126("I wonder where this cave leads to...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3550:
				if (c.inArea(3252, 3694, 3260, 3702)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3546, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Two fire remains and two banners", 6971);
					c.getPacketSender().sendFrame126("found in the wilderness is your", 6972);
					c.getPacketSender().sendFrame126("next location.", 6973);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3552:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126(
						"Five dead trees close to each other found in the wilderness is your next location.", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3554:
				for (int i = 6968; i < 6976; i++) {
					c.getPacketSender().sendFrame126("", i);
				}
				c.getPacketSender().sendFrame126(
						"Three mushrooms close to each other found in the wilderness is your next location.", 6971);
				// c.getPacketSender().sendFrame126("...", 6972);
				c.getPacketSender().showInterface(6965);
				return;
			case 3556:
				if (c.inArea(3042, 3695, 3063, 3708)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3560, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Near rocktail shoal in the Wilderness.", 6971);
					c.getPacketSender().showInterface(6965);
				}
				return;
	
			case 3558:
				if (c.inArea(3027, 3669, 3038, 3689)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3580, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Near alott of runite rocks in the Wilderness.", 6971);
					// c.getPacketSender().sendFrame126("...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 3560:
				if (c.inArea(3207, 3736, 3212, 3743)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3548, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Furnace is very hot, it's dangerous and risky...", 6971);
					// c.getPacketSender().sendFrame126("...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
	
			case 3580:
				if (c.inArea(3341, 3955, 3366, 3966)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(3540, 1);
					c.sendMessage("You have received another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Ancient ship and skeletons", 6971);
					// c.getPacketSender().sendFrame126("...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			// easy clues
			case 2704:
				if (c.inArea(2259, 4680, 2287, 4711)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2703, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("In a lair of a boss lies", 6971);
					c.getPacketSender().sendFrame126("the next clue scroll!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2703:
				if (c.inArea(3217, 3207, 3225, 3213)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2702, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("I seek another clue just", 6971);
					c.getPacketSender().sendFrame126("west of the fountain, at the origin!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2702:
				if (c.inArea(2962, 3331, 2987, 3351)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2714, 1);
					c.setClueAmount(TreasureTrails.LOW_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.LOW_CLUE)
							+ "</col> easy clue scrolls.");
					c.sendMessage("You receive a EASY Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("We are here lying to protect", 6971);
					c.getPacketSender().sendFrame126("the castle that we truly love!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2701:
				if (c.inArea(3253, 3256, 3265, 3296)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2700, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("This has to be Bob's favorite", 6971);
					c.getPacketSender().sendFrame126("training spot in-game.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2700:
				if (c.inArea(3208, 3421, 3220, 3435)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2699, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("We all love water, especially", 6971);
					c.getPacketSender().sendFrame126("from big, clean, fountains!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2699:
				if (c.inArea(2665, 3309, 2670, 3313)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2698, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("I love to eat cake, maybe", 6971);
					c.getPacketSender().sendFrame126("you want to steal some?", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2698:
				if (c.inArea(3253, 3445, 3261, 3453)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2714, 1);
					c.setClueAmount(TreasureTrails.LOW_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.LOW_CLUE)
							+ "</col> easy clue scrolls.");
					c.sendMessage("You receive a EASY Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("We shall seek history within", 6971);
					c.getPacketSender().sendFrame126("the ancient museum.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			// medium clues
			case 2817:
				if (c.inArea(2953, 3365, 2977, 3392)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2815, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("We pay to Pk, especially", 6971);
					c.getPacketSender().sendFrame126("within a city named Falador.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2815:
				if (c.inArea(3228, 9860, 3259, 9873)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2813, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Rats! Rats! Rats!", 6971);
					c.getPacketSender().sendFrame126("The sewers are full of them!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2813:
				if (c.inArea(2875, 9763, 2904, 9776)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2802, 1);
					c.setClueAmount(TreasureTrails.MED_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.MED_CLUE)
							+ "</col> medium clue scrolls.");
					c.sendMessage("You receive a MEDIUM Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("These fish must be hot!", 6971);
					c.getPacketSender().sendFrame126("We shall call this, Lava Fishing", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2811:
				if (c.inArea(3074, 3407, 3085, 3436)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2809, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("This village contains torches,", 6971);
					c.getPacketSender().sendFrame126("rocks, and some kind of stronghold.", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2809:
				if (c.inArea(3074, 3245, 3085, 3255)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2807, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("We shall thieve Master Farmers!", 6971);
					c.getPacketSender().sendFrame126("I wonder where I can find them...", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2807:
				if (c.inArea(3044, 3255, 3055, 3259)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2802, 1);
					c.setClueAmount(TreasureTrails.MED_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.MED_CLUE)
							+ "</col> medium clue scrolls.");
					c.sendMessage("You receive a MEDIUM Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("arggggghhh mate,", 6971);
					c.getPacketSender().sendFrame126("Would you like some beer?", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2805:
				if (c.inArea(3041, 3284, 3067, 3298)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2803, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Cabbage!", 6971);
					c.getPacketSender().sendFrame126("Lots, and lots of Cabbage!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2803:
				if (c.inArea(3032, 9756, 3056, 9804)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2801, 1);
					c.sendMessage("You receive another scroll.");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("Ew, a scorpion.", 6971);
					c.getPacketSender().sendFrame126("Why are these mines so messed up!", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
			case 2801:
				if (c.inArea(2906, 3155, 2926, 3175)) {
					c.getPA().removeAllWindows();
					c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
					c.getItems().addItem(2802, 1);
					c.setClueAmount(TreasureTrails.MED_CLUE, 1, true);
					c.sendMessage("You've completed <col=ff>"
							+ c.getClueAmount(TreasureTrails.MED_CLUE)
							+ "</col> medium clue scrolls.");
					c.sendMessage("You receive a MEDIUM Casket!");
				} else {
					for (int i = 6968; i < 6976; i++) {
						c.getPacketSender().sendFrame126("", i);
					}
					c.getPacketSender().sendFrame126("I seek many, many, banana trees.", 6971);
					c.getPacketSender().sendFrame126("Do you know where it is?", 6972);
					c.getPacketSender().showInterface(6965);
				}
				return;
		}

		for (SlayerScrolls scroll : SlayerScrolls.values) {
			if (itemId == scroll.itemID) {
				c.getItems().deleteItemInOneSlot(itemId, itemSlot, 1);
				c.unlockSlayerScroll(scroll.scrollType);
				c.sendMessage("You have unlocked the " + scroll.name() + "");
				c.startGraphic(SLAYER_SCROLL_GFX);
				return;
			}
		}

		FoodToEat food = Food.forId(itemId);
		if (food != null) {
			Food.eat(c, food, itemId, itemSlot);
			return;
		}

		if (Prayer.buryBone(c, itemId, itemSlot)) {
			return;
		}

		if (BarbarianAssault.readScroll(c, itemId)) {
			return;
		}

		if (DungeonItems.handle1(c, itemId, itemSlot)) {
			return;
		}

	}

}
