package com.soulplay.net.packet.in;

import com.soulplay.content.minigames.gamble.GambleStage;
import com.soulplay.content.minigames.gamble.GamblingManager;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.PacketType;

public class InterfaceClose implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		if (!c.correctlyInitialized || c.inVerificationState) {
			return;
		}
		if (!c.setMode) {
			c.getPacketSender().closeInterfaces();
			c.getDialogueBuilder().reset();
			c.starterDialogue();
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}
		if (c.isDead()) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}

		if (c.myShopClient != null || c.getDuelingClanWarClient() != null) {
			c.getPA().closeAllWindows();
		}

		c.isIdle = false;

		if (c.inTrade) {
			if (!c.tradeConfirmed2) {
				Client o = (Client) PlayerHandler.players[c.tradeWith];
				c.getTradeAndDuel().declineTrade();
				o.getTradeAndDuel().declineTrade();
			}
		}
		// if (c.isBanking)
		// c.isBanking = false;

		// Client o = (Client) PlayerHandler.players[c.duelingWith];
		if (c.duelStatus == 5) {
			// c.sendMessage("You're funny sir.");
			return;
		}
		// if (o != null) {
		if (c.duelStatus >= 1 && c.duelStatus <= 4) {
			c.getTradeAndDuel().declineDuel();
			// o.getTradeAndDuel().declineDuel();
		}
		// }

		// if (c.duelStatus == 6) {
		// c.getTradeAndDuel().claimStakedItems();
		// }

		if (c.isBanking) {
			c.getPA().closeAllWindows();// c.isBanking = false;
		}
		if (c.isShopping) {
			c.isShopping = false;
		}
		if (c.isBobOpen) {
			c.isBobOpen = false;
		}
		if (c.openDuel && c.duelStatus >= 1 && c.duelStatus <= 4) {
			// if(o != null)
			// o.getTradeAndDuel().declineDuel();
			c.getTradeAndDuel().declineDuel();
		}
		if (c.inTrade) {
			if (!c.acceptedTrade) {
				c.getTradeAndDuel().declineTrade();
			}
		}
		c.openSmithingData = null;
		c.interfaceIdOpenMainScreen = 0;
		Client o = Party.getInviteOfferer(c);
		if (o != null && o.dungParty != null) {
			o.expireTicks = 0;
			o.dungPartyInvite = -1;
		}

		c.selectedDungReward = null;
		c.dungPartyInvite = -1;
		c.expireTicks = 0;

		if (c.gambleStage != GambleStage.NONE) {
			GamblingManager.hardClose(c);
		}
	}

}
