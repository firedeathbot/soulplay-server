package com.soulplay.net.packet.in;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Bank X Items
 **/
public class BankX1 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int itemSlot = c.getInStream().readUnsignedLEShort();
		int interfaceId = c.getInStream().read3Bytes();
		int itemId = c.getInStream().read3Bytes();

		if (!c.correctlyInitialized) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		switch (interfaceId) {
			case 67111:
			case 44010:
				c.getShops().buyItem(itemId, itemSlot, 50);
				return;
			default:
				c.xRemoveSlot = itemSlot;
				c.xInterfaceId = interfaceId;
				c.xRemoveId = itemId;
				break;
		}
			// synchronized (c) {
			// c.sendMess("TEST:"+c.xInterfaceId);
			if (c.myShopClient != null) {// if (c.interfaceIdOpenMainScreen ==
											// 3824 && c.myShopId == 7390) { //
											// player owned shops stuff
				// PlayerOwnedShops.addItemToPlayerOwnedShop(c, c.xRemoveId,
				// 777, c.xRemoveSlot);
				if (c.myShopClient.equals(c)) { // my own shop
					if (c.xInterfaceId == 3823) { // inventory interface selling
						int itemId1 = c.xRemoveId;
						ItemDef def = ItemDef.forID(itemId1);
						if (def != null && def.itemIsInNotePosition) {
							itemId1 = def.getNoteId();
						}
						long price = PlayerOwnedShops.getPriceOfShopItem(c,
								itemId1);
						if (price < 1) { // item not in shop yet
//							c.sendMessage(
//									"<col=800000>Please use \"Sell 1\" option when placing first item into shop.");
//							c.resetMyShopSellingVars();
//							c.getPA().setInputDialogState(0);
							
							if (!c.getMarketOffer().openSellOffer(c.xRemoveId)) {
								c.sendMessage("Your shop is bugged?");
							}
							return;
						}
					}
				}
				c.sellingIdOriginal = c.sellingId = c.xRemoveId; // stupid pi
																	// methods
																	// needs to
																	// be
																	// removed
				c.sellingS = c.xRemoveSlot; // stupid pi methods need to be
											// removed!!!! lazy!!!!
			}
			// }

	}
}
