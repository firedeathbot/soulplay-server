package com.soulplay.net.packet.in;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.TeleportInterface;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.instances.InstanceInterface;
import com.soulplay.content.items.Lamps;
import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.impl.DegradableItem;
import com.soulplay.content.items.impl.DiceHandler;
import com.soulplay.content.items.impl.DuellistsCap;
import com.soulplay.content.items.itemdata.CombatTab;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.items.pos.TradingPostButtons;
import com.soulplay.content.items.presets.PresetsInterface;
import com.soulplay.content.items.search.MarketSearch;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.minigames.FunPkTournament;
import com.soulplay.content.minigames.KilnFightCaves;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault.Role;
import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.minigames.duel.TradeAndDuel;
import com.soulplay.content.minigames.flowers.FlowerGame;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.minigames.junkmachine.JunkMachine;
import com.soulplay.content.minigames.soulwars.Overlay;
import com.soulplay.content.minigames.soulwars.Rewards;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.ClueManager;
import com.soulplay.content.minigames.treasuretrails.ClueType;
import com.soulplay.content.minigames.treasuretrails.Emote2StepClue;
import com.soulplay.content.minigames.treasuretrails.EmoteClue;
import com.soulplay.content.minigames.treasuretrails.EmoteDoubleAgentClue;
import com.soulplay.content.npcs.impl.bosses.nex.NexEvent;
import com.soulplay.content.player.ArtifactData;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.player.PrayerBook;
import com.soulplay.content.player.SpellBook;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.achievement.SkillCapes;
import com.soulplay.content.player.combat.magic.SpellsOfPI;
import com.soulplay.content.player.combat.prayer.CombatPrayer;
import com.soulplay.content.player.combat.prayer.Curses;
import com.soulplay.content.player.combat.prayer.Prayers;
import com.soulplay.content.player.combat.prayer.QuickCurses;
import com.soulplay.content.player.combat.prayer.QuickPrayers;
import com.soulplay.content.player.combat.specattack.SpecButton;
import com.soulplay.content.player.dailytasks.CategoryInterfaceHandler;
import com.soulplay.content.player.namechange.NameChange;
import com.soulplay.content.player.pets.Egg;
import com.soulplay.content.player.pets.pokemon.PokemonButtons;
import com.soulplay.content.player.skills.Cooking;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.popularhouse.PopularHouses;
import com.soulplay.content.player.skills.crafting.TanHide;
import com.soulplay.content.player.skills.dungeoneeringv2.Binds;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.items.shop.DungeonRewardShop;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonButtons;
import com.soulplay.content.player.skills.herblore.Herblore;
import com.soulplay.content.player.skills.slayer.Master;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.content.player.skills.smithing.Smelting;
import com.soulplay.content.player.skills.smithing.SmeltingData;
import com.soulplay.content.player.skills.summoning.Creation;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.content.player.untradeables.UntradeableManager;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.shops.ShopType;
import com.soulplay.content.tob.TobManager;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.content.vote.VoteInterfaceLuck;
import com.soulplay.content.vote.VoteInterfaceStore;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.blacklist.Blacklist;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.util.Misc;
import com.soulplay.game.sounds.Sounds;

/**
 * Clicking most buttons
 **/
public class ClickingButtons implements PacketType {

	private static final int FIRST_OF_TWO = 9157;

	private static final int SECOND_OF_TWO = 9158;

	private static final int FIRST_OF_THREE = 9167;

	private static final int SECOND_OF_THREE = 9168;

	private static final int THIRD_OF_THREE = 9169;

	private static final int FIRST_OF_FOUR = 9178;

	private static final int SECOND_OF_FOUR = 9179;

	private static final int THIRD_OF_FOUR = 9180;

	private static final int FOURTH_OF_FOUR = 9181;

	private static final int FIRST_OF_FIVE = 9190;

	private static final int SECOND_OF_FIVE = 9191;

	private static final int THIRD_OF_FIVE = 9192;

	private static final int FOURTH_OF_FIVE = 9193;

	private static final int FIFTH_OF_FIVE = 9194;

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		int data1 = c.getInStream().readUnsignedShort();
		int data2 = c.getInStream().readUnsignedShort();
		int realId = (data2 << 16) + data1;
		c.getInStream().currentOffset = 0;

		data1 = Misc.hexToInt(c.getInStream().buffer, 0, 2);
		data2 = Misc.hexToInt(c.getInStream().buffer, 2, 2) * 255255;//Magic value
		int actionButtonId = data1 + data2;

		c.getInStream().currentOffset += 4;
		int componentId = c.getInStream().readUnsignedShort();
		if (componentId == 65535) {
			componentId = -1;
		}

		if (actionButtonId < 0) {
			return;
		}

		if ((c.playerRights == 3 && (Config.SERVER_DEBUG || c.debug)) || c.getName().equals("Julius")) {
			c.sendMessage(String.format("[ActionButton] button=%d interface=%d", actionButtonId,
					c.interfaceIdOpenMainScreen));
		}
		
		if ((!c.correctlyInitialized || c.inVerificationState)  && actionButtonId != 9154) {
			return;
		}
		
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			if (actionButtonId != 9157 && actionButtonId != 9158 && actionButtonId != 9154 && actionButtonId != 9167 && actionButtonId != 9168 && actionButtonId != 9169 && actionButtonId != 9178
					&& actionButtonId != 9179 && actionButtonId != 9180 && actionButtonId != 9181) {
				return;
			}
		}
 
		 if (c.playerRights == 3) {
		 c.sendMessage/*Misc.println*/(c.getName() + " - actionbutton: " + actionButtonId + ", " + realId);
		 }

		c.isIdle = false;

		// use this button for new dialogues instead so we can keep the old dialogue buttons working.
		if (c.getDialogueBuilder().isActive()) {
			
			if (c.dialogueTick == Server.getTotalTicks()) // prevent bugging out the dialogue options.
				return;
			
			c.dialogueTick = Server.getTotalTicks();
			
			switch(actionButtonId) {

			case 9157:
			case 9167:
			case 9178:			
			case 9190:
				c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);
				c.getDialogueBuilder().executeOption(0);
				return;

			case 9158:
			case 9168:
			case 9179:
			case 9191:
				c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);
				c.getDialogueBuilder().executeOption(1);
				return;

			case 9169:
			case 9180:
			case 9192:
				c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);
				c.getDialogueBuilder().executeOption(2);
				return;

			case 9181:
			case 9193:
				c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);
				c.getDialogueBuilder().executeOption(3);
				return;

			case 9194:
				c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);
				c.getDialogueBuilder().executeOption(4);
				return;
			}
		}
		
		
		final PluginResult<ButtonClickExtension> pluginResult = PluginManager.search(ButtonClickExtension.class, actionButtonId);

		final int componentFinal = componentId;
		if (pluginResult.invoke(() -> pluginResult.extension.onClick(c, actionButtonId, componentFinal))) {
			c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);
			return;
		}

		if (c.insideInstance() && c.getInstanceManager().buttonClick(c, realId, componentId)) {
			return;
		}

		if (CombatPrayer.handlePrayerButton(c, actionButtonId)) {
			return;
		}

		c.getPacketSender().playSound(Sounds.CLICKING_BUTTONS);

		if (PokemonButtons.handle(c, actionButtonId)) {
			return;
		}
		
		if (NameChange.buttonClick(c, actionButtonId)) {
			return;
		}
		
		if (TeleportInterface.handleButton(c, actionButtonId)) {
			return;
		}

		if (ItemRetrievalManager.buttonClick(c, realId)) {
			return;
		}

		if (GamblingInterface.buttonClick(c, realId)) {
			return;
		}

		if (Lamps.buttonClick(c, realId)) {
			return;
		}

		if (VoteInterface.buttonClick(c, realId)) {
			return;
		}

		if (VoteInterfaceLuck.buttonClick(c, actionButtonId)) {
			return;
		}

		if (VoteInterfaceStore.buttonClick(c, actionButtonId)) {
			return;
		}

		if (DungeonRewardShop.buttonClick(c, actionButtonId)) {
			return;
		}

		if (PresetsInterface.buttonClick(c, realId)) {
			return;
		}

		if (InstanceInterface.handleClick(c, realId, componentId)) {
			return;
		}

		if (DungeonButtons.handle(c, actionButtonId)) {
			return;
		}
		if (Party.handleButtons(c, actionButtonId)) {
			return;
		}

		if (Blacklist.handleButton(c, actionButtonId, componentId)) {
			return;
		}

		if (Binds.button(c, actionButtonId)) {
			return;
		}
		if (SpellsOfPI.lunarSpells(c, actionButtonId)) {
			return;
		}
		
		if (c.getMarketOffer().isActive() && c.getMarketOffer().clickButton(c, actionButtonId))
			return;
		
		if (TradingPostButtons.handleButton(c, actionButtonId))
			return;

		if (actionButtonId == 19137) {
			QuickCurses.selectQuickInterface(c);
			return;
		} else if (actionButtonId == 19136) {
			c.getPA().toggleQuickPray();
			return;
		} else if (actionButtonId == 67079) {
			c.updatePrayerBook();
			return;
		} else if (actionButtonId >= 67050 && actionButtonId <= 67082) {
			if (c.playerPrayerBook == PrayerBook.NORMAL) {
				QuickPrayers.clickPray(c, actionButtonId);
			} else {
				QuickCurses.clickCurse(c, actionButtonId);
			}
			return;
		}

		if (actionButtonId == 179228) {
			if (!c.inMarket()) {
				c.sendMessage("You must be in ::market");
				return;
			}
			if (c.playerCollect > 0) {
				c.sendMessage("You succesfully collected " + c.playerCollect + " coins.");
				MoneyPouch.addToMoneyPouch(c, c.playerCollect);
				c.playerCollect = 0;
			} else {
				c.sendMessage("You dont have anything to collect");
			}
			return;
		}

		/*if (actionButtonId == 30108) { //pet attack hot key TODO fix, removed because was using dclaws id
			c.selectPokemonAttack = true;
			return;
		}*/

		if (EmoteClue.entries.contains(actionButtonId)) {
			for (ClueDifficulty difficulty : ClueDifficulty.values) {
				int data = c.getTreasureTrailManager().getCluesData()[difficulty.getIndex()];
				if (data == -1) {
					continue;
				}

				ClueType clueType = ClueManager.clues.get(difficulty.getIndex()).get(Misc.getFirst(data)).get(Misc.getSecond(data));
				if (clueType instanceof EmoteClue) {
					EmoteClue emoteClue = (EmoteClue) clueType;
					if (emoteClue.getEmoteButton() == actionButtonId) {
						clueType.checkComplete(c, difficulty);
					}
				} else if (clueType instanceof Emote2StepClue) {
					Emote2StepClue emoteClue = (Emote2StepClue) clueType;
					if (emoteClue.checkReqs(c)) {
						int stage = c.attr().getOrDefault("2stepemote", 0);
						if (emoteClue.getEmoteButton() == actionButtonId && stage == 0) {
							c.attr().put("2stepemote", 1);
							ClueManager.spawnUri(c);
						} else if (emoteClue.getEmoteButton2() == actionButtonId && stage == 1) {
							c.attr().put("2stepemote", 2);
						}
					}
				} else if (clueType instanceof EmoteDoubleAgentClue) {
					EmoteDoubleAgentClue emoteClue = (EmoteDoubleAgentClue) clueType;
					if (emoteClue.getEmoteButton() == actionButtonId && emoteClue.checkReqs(c)) {
						int stage = c.attr().getOrDefault("doubleagent", 0);
						if (stage == 0) {
							c.attr().put("doubleagent", 1);
							ClueManager.spawnDoubleAgent(c);
						} else if (stage == 2) {
							ClueManager.spawnUri(c);
							c.attr().put("doubleagent", 3);
						}
					}
				}
			}
		}

		if (Rewards.soulWarRewards(c, actionButtonId)) {
			c.getPacketSender().sendFrame126("Zeals: " + c.getZeals(), 29333);
			return;
		}

		if (CompletionistCape.handleCompletionistCapeButton(c, actionButtonId)) {
			c.getPA().closeAllWindows();
			return;
		}

		if (c.getPA().assignAutocast(realId)) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}

		if (c.interfaceIdOpenMainScreen == 5292) {
			if (actionButtonId == 85244) { // search bank button
				c.getPacketSender().sendInputDialogState(PlayerConstants.SEARCH_STATE);
				return;
			}
		}

		if (c.teleTimer > 0) {
			// c.sendMessage("Resetting all prayers.");
			// c.getCombat().resetPrayers();
			// CombatPrayer.refreshPrayerIcons(c);
			return;
		}

		if (c.tourStep > 0 && c.tourStep < 100
				&& (actionButtonId != 9154 && actionButtonId != 9157 && actionButtonId != 9158)) {
			c.getDH().sendOption2("I would like to stop the tour.", "Please continue the tour.");
			return;
		}

		if (c.inClanWarsLobby() && ClanWarHandler.handleButtons(c, actionButtonId)) {
			return;
		}

		if (c.getSummoning().handleButtonClick(actionButtonId)) {
			return;
		}

		if (Creation.handleButtons(c, actionButtonId)) {
			return;
		}

		if (c.interfaceIdOpenMainScreen == 45000 && MarketSearch.marketButtonClicked(c, actionButtonId)) {
			return;
		}

		if (actionButtonId >= 164020 && actionButtonId <= 166084) {
			Titles.clickButtons(c, actionButtonId);
			return;
		}

//		if (actionButtonId >= 74058 && actionButtonId <= 74118) {
//			if (c.difficulty == PlayerDifficulty.IRONMAN) {
//				c.sendMessage("You cannot change chat icon on ironman mode.");
//				return;
//			}
//			int index = (actionButtonId - 74058) / 4;
//			if (c.getUnlockedCrowns()[index] == 1) { // if crown is unlocked
//				c.setCrown(index);
//			}
//			c.getPacketSender().sendConfig(ChatCrown.RADIO_BUTTON_CONFIG, ChatCrown.values[c.getSetCrown()].ordinal());
//			return;
//		}
		
		MakeInterface.runProduct(c, actionButtonId);
		switch (actionButtonId) {
			case 257440:
				CategoryInterfaceHandler.open(c);
				break;
			case 261393:
				TobManager.refreshSidebar(c);
				RaidsManager.refreshSidebar(c);

				if (c.getClan() == null) {
					c.sendMessage("You must be in a clan to start raiding.");
					return;
				}

				RaidsManager.checkRaidStart(c);
				TobManager.checkRaidStart(c);
				return;
		    case 230128:
			    c.getPA().closeAllWindows();
			break;
			case 255254://dung worldmap
				if (c.dungParty == null || c.dungParty.dungManager == null) {
					break;
				}

				c.dungParty.dungManager.openMap(c);
				break;
			case 179184: // construction house option
				c.getPOH().enter(c, true, false);
				break;

			case 179186: // construction house option
				if (c.getPOH().isGuest()) {
					return;
				}

				// guests are present
				if (!c.getPOH().isGuest() && !c.getPOH().getGuests().isEmpty()) {
					return;
				}

				c.getPOH().enter(c, false, false);
				break;

			case 179188: // construction house option
				c.getPOH().setRenderDoorsOpen(true);
				break;

			case 179190: // construction house option
				c.getPOH().setRenderDoorsOpen(false);
				break;

			case 179192: // construction house option
				c.getPOH().setTeleportInside(true);
				break;

			case 179194: // construction house option
				c.getPOH().setTeleportInside(false);
				break;
				
			case 179196: // construction view popular houses
				PopularHouses.openInterface(c);
			    break;

			case 179185: // construction expel guests
					c.getPOH().expelGuests();
				break;

			case 179187: // construction leave house
				c.getDialogueBuilder().sendStatement("If you leave the house <col=ff0000>all</col> your dropped items will be left on ground", "or discarded.")
				.sendOption("Are you sure you want to leave the house?", "Yes", () -> {
					Construction.leaveHouse(c);
				}, "No", () -> {}).execute();
				break;

		case 8198: // accept button on drop party chest interface
			if (c.getPartyOffer().isOpen())
				c.getPartyOffer().accept();
			break;
		
		case 26010: // right click autocast disable on spellbook on the current selected spell
			c.getCombat().resetPlayerAttack();
			c.getPA().resetAutocast();
			break;

		// destroy item option yes
		case 55095:
			c.getPA().closeAllWindows();
			if (c.itemDestroyCode != null) {
				c.itemDestroyCode.run();
			}

			c.itemDestroyCode = null;
			if (c.itemDestroyPostClick != null) {
				c.itemDestroyPostClick.run();
			}

			c.itemDestroyPostClick = null;
			break;

		// destroy item option no
		case 55096:
			c.getPA().closeAllWindows();
			if (c.itemDestroyPostClick != null) {
				c.itemDestroyPostClick.run();
			}

			c.itemDestroyPostClick = null;
			break;

		case 19138: // withdraw money pouch
			if (!c.canWithDrawOrDepositMoneyPouch()) {
				c.sendMessage("You cannot do that here.");
				return;
			}
			if (c.sidebarInterfaceOpen != 0) {
				return;
			}

			c.xInterfaceId = 3214;
			c.xRemoveId = 995;
			c.getPacketSender().sendInputDialogState(PlayerConstants.SET_DIALOG_AMOUNT_TO_WITHDRAW);
			break;

		case 19139: // examine money pouch
			c.sendMessage(
					"Money Pouch currently contains: " + Misc.formatNumbersWithCommas(c.getMoneyPouch()) + " gold.");
			break;

		case 19140: // price cheker
			break;

		case 54202:
			c.capeClicked = 0;
			c.getPA().closeAllWindows();
			break;
		case 23112:
			// c.getPA().sendFrame200(4894, 9848);
			c.getPA().toggleQuickPray();
			break;

		/* Bank tabs buttons */
		case 86008:
			if (!c.isBanking) {
				return;
			}
			c.getPA().openUpBank(0);
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86009:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems1[0] > 0) {
				c.getPA().openUpBank(1);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86010:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems2[0] > 0) {
				c.getPA().openUpBank(2);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86011:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems3[0] > 0) {
				c.getPA().openUpBank(3);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86012:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems4[0] > 0) {
				c.getPA().openUpBank(4);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86013:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems5[0] > 0) {
				c.getPA().openUpBank(5);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86014:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems6[0] > 0) {
				c.getPA().openUpBank(6);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86015:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems7[0] > 0) {
				c.getPA().openUpBank(7);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;
		case 86016:
			if (!c.isBanking) {
				return;
			}
			if (c.bankItems8[0] > 0) {
				c.getPA().openUpBank(8);
			}
			c.getPacketSender().setScrollPos(5385, 0);
			c.firstBankTab = c.bankingTab;
			break;

		case 121135: // attack skill
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() || c.inFightCaves() || c.inPits || c.inFunPk()
					|| c.inClanWars() || c.inClanWarsFunPk() || c.inMinigame() || c.underAttackBy > 0
					|| c.underAttackBy2 > 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change skills here.");
				return;
			}
			if (c.difficulty != 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You can only change your stats on Normal mode.");
				return;
			}

			c.donationSkillReset = false;
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 11110;
			break;

		case 121140: // str skill
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() || c.inFightCaves() || c.inPits || c.inFunPk()
					|| c.inClanWars() || c.inMinigame() || c.underAttackBy > 0 || c.underAttackBy2 > 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change skills here.");
				return;
			}
			if (c.difficulty != 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You can only change your stats on Normal mode.");
				return;
			}
			c.donationSkillReset = false;
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 11112;
			break;

		case 121145: // def skill
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() || c.inFightCaves() || c.inPits || c.inFunPk()
					|| c.inClanWars() || c.inMinigame() || c.underAttackBy > 0 || c.underAttackBy2 > 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change skills here.");
				return;
			}
			if (c.difficulty != 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You can only change your stats on Normal mode.");
				return;
			}
			c.donationSkillReset = false;
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 11111;
			break;

		// case 121180: // hp skill
		// if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() ||
		// c.inFightCaves() || c.inPits || c.inFunPk() || c.inClanWars() ||
		// c.inMinigame() || c.underAttackBy > 0 || c.underAttackBy2 > 0) {
		// c.getPA().closeAllWindows();
		// c.sendMessage("You cannot change skills here.");
		// return;
		// }
		// if (c.difficulty != 0) {
		// c.getPA().closeAllWindows();
		// c.sendMessage("You can only change your stats on Normal mode.");
		// return;
		// }
		// c.donationSkillReset = false;
		// c.getPA().sendEnterAmountInterface();
		// c.dialogueAction = 11113;
		// break;

		case 121150: // range skill
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() || c.inFightCaves() || c.inPits || c.inFunPk()
					|| c.inClanWars() || c.inMinigame() || c.underAttackBy > 0 || c.underAttackBy2 > 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change skills here.");
				return;
			}
			if (c.difficulty != 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You can only change your stats on Normal mode.");
				return;
			}
			c.donationSkillReset = false;
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 11114;
			break;

		case 121155: // prayer skill
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() || c.inFightCaves() || c.inPits || c.inFunPk()
					|| c.inClanWars() || c.inMinigame() || c.underAttackBy > 0 || c.underAttackBy2 > 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change skills here.");
				return;
			}
			if (c.difficulty != 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You can only change your stats on Normal mode.");
				return;
			}
			if (c.getDonPoints() < 1) {
				c.getPA().closeAllWindows();
				c.sendMessage("You need Donator points to set this skill.");
				return;
			}
			c.donationSkillReset = true;
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 11115;
			break;

		case 121160: // magic skill
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			if (c.wildLevel > 0 || c.inPcGame() || c.inDuelArena() || c.inFightCaves() || c.inPits || c.inFunPk()
					|| c.inClanWars() || c.inMinigame() || c.underAttackBy > 0 || c.underAttackBy2 > 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change skills here.");
				return;
			}
			if (c.difficulty != 0) {
				c.getPA().closeAllWindows();
				c.sendMessage("You can only change your stats on Normal mode.");
				return;
			}
			c.donationSkillReset = false;
			c.getPacketSender().sendEnterAmountInterface();
			c.dialogueAction = 11116;
			break;

		case 62169: // kdr button
			c.forceChat("My KDR is " + RSConstants.getKDR(c.KC, c.DC) + " - Kills:" + c.KC + " - Deaths:" + c.DC
					+ " Top KillStreak:" + c.highestKillStreak + " - Current Streak:" + c.killStreak);
			break;

		case 73118:
			if (WorldType.equalsType(WorldType.SPAWN) && c.wildLevel < 1) {
				if (System.currentTimeMillis() - c.anyDelay2 >= 3000) {
					c.getItems().addItem(554, 1000);
					c.getItems().addItem(555, 1000);
					c.getItems().addItem(556, 1000);
					c.getItems().addItem(557, 1000);
					c.getItems().addItem(560, 1000);
					c.getItems().addItem(561, 1000);
					c.getItems().addItem(562, 1000);
					c.getItems().addItem(563, 1000);
					c.getItems().addItem(565, 1000);
					c.getItems().addItem(566, 1000);
					c.getItems().addItem(9075, 1000);
					c.anyDelay2 = System.currentTimeMillis();
				} else {
					c.sendMessage("You need to wait 3 seconds before spawning more runes.");
				}
			}
			break;

		case 73121:
			if (WorldType.equalsType(WorldType.SPAWN) && c.wildLevel < 1) {
				if (System.currentTimeMillis() - c.anyDelay2 >= 3000) {
					c.getItems().addItem(555, 6000);
					c.getItems().addItem(560, 4000);
					c.getItems().addItem(565, 2000);
					c.anyDelay2 = System.currentTimeMillis();
				} else {
					c.sendMessage("You need to wait 3 seconds before spawning barrage runes.");
				}
			}
			break;

		case 73124: // veng set
			if (WorldType.equalsType(WorldType.SPAWN) && c.wildLevel < 1) {
				if (System.currentTimeMillis() - c.anyDelay2 >= 3000) {
					c.getItems().addItem(9075, 4000);
					c.getItems().addItem(560, 2000);
					c.getItems().addItem(557, 10000);
					c.anyDelay2 = System.currentTimeMillis();
				} else {
					c.sendMessage("You need to wait 3 seconds before spawning veng runes.");
				}
			}
			break;

		case 73127:
			if (WorldType.equalsType(WorldType.SPAWN) && c.wildLevel < 1) {
				if (System.currentTimeMillis() - c.anyDelay2 >= 3000) {
					c.getItems().addItem(562, 1000);
					c.getItems().addItem(563, 1000);
					c.getItems().addItem(560, 1000);
					c.anyDelay2 = System.currentTimeMillis();
				} else {
					c.sendMessage("You need to wait 3 seconds before spawning TB runes.");
				}
			}
			break;

		case 73130:
			if (WorldType.equalsType(WorldType.SPAWN) && c.wildLevel < 1) {
				if (System.currentTimeMillis() - c.anyDelay2 >= 3000) {
					c.getItems().addItem(2440, 1);
					c.getItems().addItem(2436, 1);
					c.getItems().addItem(2442, 1);
					c.anyDelay2 = System.currentTimeMillis();
				} else {
					c.sendMessage("You need to wait 3 seconds before spawning super set.");
				}
			}
			break;

		case 73133:
			if (WorldType.equalsType(WorldType.SPAWN) && c.wildLevel < 1) {
				if (System.currentTimeMillis() - c.anyDelay2 >= 3000) {
					c.getItems().addItem(15272, 28);
					c.anyDelay2 = System.currentTimeMillis();
				} else {
					c.sendMessage("You need to wait 3 seconds before spawning more food.");
				}
			}
			break;

		case 71078: // lootshare
			Clan clan = c.getClan();
			if (clan == null) {
				c.sendMessage("You are not in a clan.");
				return;
			}
			if (clan.getRank(c.getName()) >= clan.getWhoCanChangeClanShare()) {
				clan.setClanShare(!clan.clanShare());
				clan.sendClanMessage(
						"Coin Share has been turned " + (clan.clanShare() ? "on" : "off") + " by " + c.getName(), null);
				clan.setClanShareFrame(clan.clanShare() ? "On" : "Off");
				// c.getPacketSender().sendFrame126(clan.clanShare() ? "On"
				// : "Off",
				// 18250);
				// c.sendMessage(":lootshare:"+(clan.clanShare() ? "on" :
				// "off"));
			}
			break;

		case 29307:
			break;

		case 85248: // note items
		case 82016:
			c.getPA().toggleNote(!c.takeAsNote);
			break;

		// case 82020: // store all from inventory bank all
		// case 89223: // Bank All
		case 85252: // bank all inventory
			if (c.toggleBot && c.getBot() != null) {
				c.getBot().getUtils().bankAll();
				c.getItems().resetInterfacesForToggleBot();
				return;
			}
			if (!c.isBanking) {
				return;
			}
			c.refreshBank = false;
			int currentTab = c.bankingTab;
			for (int i = 0; i < c.playerItems.length; i++) {
				if (c.playerItems[i] > 0) {
					// if (!c.getItems().bankItem(c.playerItems[i]-1, i,
					// c.playerItemsN[i])) continue;//break;
					// c.bankingTab = currentTab;

					if (c.getItems().addItemToBank(c.playerItems[i] - 1, c.playerItemsN[i])) {
						c.getItems().deleteItemInOneSlot(c.playerItems[i] - 1, i, c.playerItemsN[i]);
					}
				}
			}
			c.refreshBank = true;
			c.getPA().openUpBank(currentTab);
			break;

		case 86000: // Deposit Worn Items
			if (c.toggleBot && c.getBot() != null) {
				c.getBot().getUtils().bankAll();
				c.getItems().resetInterfacesForToggleBot();
				return;
			}
			if (!c.isBanking) {
				return;
			}
			c.getItems().bankEquipment();
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			c.setUpdateEquipment(true, 3);
			c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
			break;

		case 86004: // deposit BoB items into bank
			if (c.isBanking) {
				c.getItems().bankAllBoBToBank();
			}
			break;

		case 28215:
			c.getPA().closeAllWindows();
			c.sendMessage("MISSING BUTTON INFO.");
			// if (c.slayerTask <= 0) {
			// c.sendMessage("You do not have a task, please talk with a
			// slayer
			// master.");
			// } else {
			// c.forcedText = "I must slay another " + c.taskAmount + " "
			// + Server.npcHandler.getNpcListName(c.slayerTask) + ".";
			// c.forcedChatUpdateRequired = true;
			// c.updateRequired = true;
			// }
			break;
		/* End Quest */

		case 15147:
			Smelting.startSmelting(c, SmeltingData.BRONZE, 1);
			break;
		case 15146:
			Smelting.startSmelting(c, SmeltingData.BRONZE, 5);
			break;
		case 15247:
			Smelting.startSmelting(c, SmeltingData.BRONZE, 10);
			break;
		case 9110:
			Smelting.startSmelting(c, SmeltingData.BRONZE, 28);
			break;
		case 15151:
			Smelting.startSmelting(c, SmeltingData.IRON, 1);
			break;
		case 15150:
			Smelting.startSmelting(c, SmeltingData.IRON, 5);
			break;
		case 15149:
			Smelting.startSmelting(c, SmeltingData.IRON, 10);
			break;
		case 15148:
			Smelting.startSmelting(c, SmeltingData.IRON, 28);
			break;
		case 15155:
			Smelting.startSmelting(c, SmeltingData.SILVER, 1);
			break;
		case 15154:
			Smelting.startSmelting(c, SmeltingData.SILVER, 5);
			break;
		case 15153:
			Smelting.startSmelting(c, SmeltingData.SILVER, 10);
			break;
		case 15152:
			Smelting.startSmelting(c, SmeltingData.SILVER, 28);
			break;
		case 15159:
			Smelting.startSmelting(c, SmeltingData.STEEL, 1);
			break;
		case 15158:
			Smelting.startSmelting(c, SmeltingData.STEEL, 5);
			break;
		case 15157:
			Smelting.startSmelting(c, SmeltingData.STEEL, 10);
			break;
		case 15156:
			Smelting.startSmelting(c, SmeltingData.STEEL, 28);
			break;
		case 15163:
			Smelting.startSmelting(c, SmeltingData.GOLD, 1);
			break;
		case 15162:
			Smelting.startSmelting(c, SmeltingData.GOLD, 5);
			break;
		case 15161:
			Smelting.startSmelting(c, SmeltingData.GOLD, 10);
			break;
		case 15160:
			Smelting.startSmelting(c, SmeltingData.GOLD, 28);
			break;
		case 29017:
			Smelting.startSmelting(c, SmeltingData.MITHRIL, 1);
			break;
		case 29016:
			Smelting.startSmelting(c, SmeltingData.MITHRIL, 5);
			break;
		case 24253:
			Smelting.startSmelting(c, SmeltingData.MITHRIL, 10);
			break;
		case 16062:
			Smelting.startSmelting(c, SmeltingData.MITHRIL, 28);
			break;
		case 29022:
			Smelting.startSmelting(c, SmeltingData.ADAMANT, 1);
			break;
		case 29020:
			Smelting.startSmelting(c, SmeltingData.ADAMANT, 5);
			break;
		case 29019:
			Smelting.startSmelting(c, SmeltingData.ADAMANT, 10);
			break;
		case 29018:
			Smelting.startSmelting(c, SmeltingData.ADAMANT, 28);
			break;
		case 29026:
			Smelting.startSmelting(c, SmeltingData.RUNE, 1);
			break;
		case 29025:
			Smelting.startSmelting(c, SmeltingData.RUNE, 5);
			break;
		case 29024:
			Smelting.startSmelting(c, SmeltingData.RUNE, 10);
			break;
		case 29023:
			Smelting.startSmelting(c, SmeltingData.RUNE, 28);
			break;
		case 58074:
			c.getBankPin().closeBankPin();
			break;
		case 58073:
			if (c.interfaceIdOpenMainScreen != 7424) {
				return;
			}
			if (c.hasBankPin && !c.requestPinDelete) {
				c.requestPinDelete = true;
				c.getBankPin().setDateRequestedForPinDeletion();
				// FreeDialogues.handledialogue(c, 1017, 1);
				// c.getDH().sendDialogues(1017, c.npcType);
				c.getDH().sendDialogues(1017, 494);
				c.sendMessage("[Notice] A PIN delete has been requested. Your PIN will be deleted in "
						+ c.getBankPin().recovery_Delay + " days.");
				c.sendMessage("To cancel this change just type in the correct PIN.");
			} else {
				c.sendMessage("[Notice] Your PIN is already pending deletion. Please wait the entire 2 days.");
				c.getPA().closeAllWindows();
			}
			break;

		case 58025:
		case 58026:
		case 58027:
		case 58028:
		case 58029:
		case 58030:
		case 58031:
		case 58032:
		case 58033:
		case 58034:
			if (!c.openBankPinInterface) {
				break;
			}
			c.getBankPin().bankPinEnter(actionButtonId);
			break;

		case 58230:
			if (!c.hasBankPin) {
				c.getBankPin().openPin();
			} else if (c.hasBankPin && c.enterdBankpin) {
				c.getBankPin().resetBankPin();
				c.sendMessage("Your PIN has been deleted as requested.");
			} else {
				c.sendMessage("Please enter your Bank Pin before requesting a delete.");
				c.sendMessage("You can do this by simply opening your bank. This is to verify it's really you.");
				c.getPA().closeAllWindows();
			}
			break;

		/*
		 * case 58025: case 58026: case 58027: case 58028: case 58029: case
		 * 58030: case 58031: case 58032: case 58033: case 58034:
		 * c.getBankPin().pinEnter(actionButtonId); break;
		 */

		/**
		 * Herblore
		 **/
		// Updated by Vavbro
		case 10239:// make 1
			Herblore.handlePotionMaking(c, 1, c.herbloreItem1, c.herbloreItem2);
			break;

		case 10238:// make 5
			Herblore.handlePotionMaking(c, 5, c.herbloreItem1, c.herbloreItem2);
			break;
		case 6211:// Make all
		case 6212:// Make x
			Herblore.handlePotionMaking(c,
					c.getItems().getItemAmount(c.herbloreItem1) > c.getItems().getItemAmount(c.herbloreItem2)
							? c.getItems().getItemAmount(c.herbloreItem1) : c.getItems().getItemAmount(c.herbloreItem2),
					c.herbloreItem1, c.herbloreItem2);
			break;

		case 53152:
			Cooking.getAmount(c, 1);
			break;
		case 53151:
			Cooking.getAmount(c, 5);
			break;
		case 53150:
			Cooking.getAmount(c, 10);
			break;
		case 53149:
			Cooking.getAmount(c, 28);
			break;

		case 33206: // attack
			c.getSI().attackComplex(1);
			c.getSI().selected = 0;
			break;
		case 33209: // strength
			c.getSI().strengthComplex(1);
			c.getSI().selected = 1;
			break;
		case 33212: // Defence
			c.getSI().defenceComplex(1);
			c.getSI().selected = 2;
			break;
		case 33215: // range
			c.getSI().rangedComplex(1);
			c.getSI().selected = 3;
			break;
		case 33218: // prayer
			c.getSI().prayerComplex(1);
			c.getSI().selected = 4;
			break;
		case 33221: // mage
			c.getSI().magicComplex(1);
			c.getSI().selected = 5;
			break;
		case 33224: // runecrafting
			c.getSI().runecraftingComplex(1);
			c.getSI().selected = 6;
			break;
		case 33207: // hp
			c.getSI().hitpointsComplex(1);
			c.getSI().selected = 7;
			break;
		case 121185: // agility
			c.getSI().agilityComplex(1);
			c.getSI().selected = 8;
			break;
		case 121190: // herblore
			c.getSI().herbloreComplex(1);
			c.getSI().selected = 9;
			break;
		case 121195: // thieving
			c.getSI().thievingComplex(1);
			c.getSI().selected = 10;
			break;
		case 121200: // crafting
			c.getSI().craftingComplex(1);
			c.getSI().selected = 11;
			break;
		case 121205: // fletching
			c.getSI().fletchingComplex(1);
			c.getSI().selected = 12;
			break;
		case 121210:// slayer
			c.getSI().slayerComplex(1);
			c.getSI().selected = 13;
			break;
		case 121225:// mining
			c.getSI().miningComplex(1);
			c.getSI().selected = 14;
			break;
		case 121230: // smithing
			c.getSI().smithingComplex(1);
			c.getSI().selected = 15;
			break;
		case 121235: // fishing
			c.getSI().fishingComplex(1);
			c.getSI().selected = 16;
			break;
		case 121240: // cooking
			c.getSI().cookingComplex(1);
			c.getSI().selected = 17;
			break;
		case 121245: // firemaking
			c.getSI().firemakingComplex(1);
			c.getSI().selected = 18;
			break;
		case 121250: // woodcut
			c.getSI().woodcuttingComplex(1);
			c.getSI().selected = 19;
			break;
		case 121255: // farming
			c.getSI().farmingComplex(1);
			c.getSI().selected = 20;
			break;

		case 34142: // tab 1
			c.getSI().menuCompilation(1);
			break;

		case 34119: // tab 2
			c.getSI().menuCompilation(2);
			break;

		case 34120: // tab 3
			c.getSI().menuCompilation(3);
			break;

		case 34123: // tab 4
			c.getSI().menuCompilation(4);
			break;

		case 34133: // tab 5
			c.getSI().menuCompilation(5);
			break;

		case 34136: // tab 6
			c.getSI().menuCompilation(6);
			break;

		case 34139: // tab 7
			c.getSI().menuCompilation(7);
			break;

		case 34155: // tab 8
			c.getSI().menuCompilation(8);
			break;

		case 34158: // tab 9
			c.getSI().menuCompilation(9);
			break;

		case 34161: // tab 10
			c.getSI().menuCompilation(10);
			break;

		case 59199: // tab 11
			c.getSI().menuCompilation(11);
			break;

		case 59202: // tab 12
			c.getSI().menuCompilation(12);
			break;
		case 59203: // tab 13
			c.getSI().menuCompilation(13);
			break;

		case 150:
			c.autoRet = 1 - c.autoRet;

			c.getPacketSender().setConfig(172, c.autoRet);
			break;
		// 1st tele option
		case FIRST_OF_FIVE:

			switch (c.dialogueAction) {
			case 8466:
			case 1599:
			case 1562:
			case 7782:
			case 8469:
			case 9087:
			case 1190:
				Optional<Master> master = Master.lookup(c.npcType);
				if (master.isPresent()) {
					Slayer.assignTask(c, master.get());
					c.getPA().closeAllWindows();
					return;
				}
				break;

			case 20923:
				c.getItems().deleteItemInOneSlot(299, 1);
				c.faceLocation(c.getX(), c.getY());
				c.getPA().stepAway();
				c.interactingObject = new RSObject(2981, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
				ObjectManager.addObject(c.interactingObject);
				break;
				
			case 20924:
				c.getItems().deleteItemInOneSlot(299, 1);
				c.faceLocation(c.getX(), c.getY());
				c.getPA().stepAway();
				c.interactingObject = new RSObject(2982, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
				ObjectManager.addObject(c.interactingObject);
				break;

			// level up attacker role
			case 5030: {

				final int currentLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.ATTACKER));

				if (currentLevel == 5) {
					c.sendMessage("You reached the maximum level for this role.");
					c.getPA().closeAllWindows();
					break;
				}

				final int cost = c.getBarbarianAssault().getCostForLevel(currentLevel + 1);

				if (c.rolePoints[0] < cost) {
					c.sendMessage(String.format("You need %d attacker points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				if (c.getBarbarianAssault().getHonorPoints() < cost) {
					c.sendMessage(String.format("You need %d honour points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				switch (currentLevel) {

				case 1:
					c.getBarbarianAssault().addAttackerExp(200);
					c.sendMessage("Your attacker role is now level 2.");
					break;

				case 2:
					c.getBarbarianAssault().addAttackerExp(300);
					c.sendMessage("Your attacker role is now level 3.");
					break;

				case 3:
					c.getBarbarianAssault().addAttackerExp(400);
					c.sendMessage("Your attacker role is now level 4.");
					break;

				case 4:
					c.getBarbarianAssault().addAttackerExp(500);
					c.sendMessage("Your attacker role is now level 5.");
					break;

				default:
					break;

				}

				c.rolePoints[0] -= cost;
				c.getBarbarianAssault().setHonorPoints(c.getBarbarianAssault().getHonorPoints() - cost, true);
				c.getPA().closeAllWindows();
			}
				break;

			}

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 1) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[0]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 3022) {
				c.getPA().spellTeleport(LocationConstants.FARMING_ARDOUGNE.getX(),
						LocationConstants.FARMING_ARDOUGNE.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 3024) {
				c.getPA().spellTeleport(LocationConstants.VARROCK_TREE_FARM.getX(),
						LocationConstants.VARROCK_TREE_FARM.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 8093) {
				if (c.getItems().playerHasItem(13727, 100)) {
					c.getItems().deleteItemInOneSlot(13727, 100);
					c.getItems().addItem(995, 1000000);
					c.sendMessage("You have been rewarded 1.000.000 gp.");
					c.getPA().closeAllWindows();
				}
			}
			if (c.dialogueAction == 3638) {
				c.getDH().sendOption5("Lumbridge", "Varrock", "Camelot", "Edgeville", "Next Page");
				c.dialogueAction = 3539;
				c.teleAction = 20;
				break;
			}
			if (c.dialogueAction == 537) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy food in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				int sharksAmount = 1000;
				int totalPrice = 0;

				totalPrice += (3449 * sharksAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 1) {
					c.getItems().addItem(386, sharksAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The food has been added to your inventory");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 2 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 14329) {
				c.getDH().sendDialogues(14330, c.npcType);
				break;
			}

			if (c.dialogueAction == 8930) {
				if (c.getItems().playerHasItem(6575, 1)) {
					if (c.getItems().playerHasItem(14598, 86)) {
						c.getItems().deleteItemInOneSlot(14598, 86);
						c.getItems().deleteItemInOneSlot(6575, 1);
						c.getItems().addItem(15017, 1);
					} else {
						c.sendMessage("You need 86 enchanted stones to enchant this ring.");
						c.getPA().closeAllWindows();
					}
				} else {
					c.sendMessage("You dont have a Onyx ring to enchant.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 14374) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy Hybrid set with Iron Man mode.");
				} else {
					c.getItems().hybridSet();
				}
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 2250) {
				if (c.difficulty != 0) {
					c.getPA().closeAllWindows();
					c.sendMessage("You can only change your stats on Normal mode");
					return;
				}
				if (c.getLoyaltyPoints() >= 200) {
					c.getPacketSender().sendEnterAmountInterface();
					c.dialogueAction = 11110;
				} else {
					c.sendMessage("You need 200 Loyalty Points to set a skill.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 14330) {
				if (c.inWild()) {
					return;
				}
				if (!(c.getItems().playerHasItem(995, 2500000))) {
					c.sendMessage("You need 2.5m to reset this skill.");
					c.getPA().closeAllWindows();
					return;
				}
				for (int element : c.playerEquipment) {
					if (element > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						c.getPA().closeAllWindows();
						return;
					}
				}
				try {
					c.sendMessage("Your Attack level has been reset.");
					int skill = 0;
					int level = 1;
					c.getSkills().setStaticLevel(skill, level);
					c.calcCombat();
					c.getCombat().resetPrayers();
					c.getPA().closeAllWindows();
				} catch (Exception e) {
					return;
				}
				return;
			}

			if (c.dialogueAction == 1217) {
				c.getPA().spellTeleport(2786, 4839, 0);
				c.dialogueAction = -1;
				break;
			}
			// Herblore npcs dialogues
			if (c.dialogueAction == 589) {
				c.getDH().sendNpcChat4("You need clean Guam and vial of water to make",
						"Guam Potion (unf), then you need eye of newt. You can buy",
						"eye of newt from the store or get by killing monsters like",
						"Chaos druids, Giant spiders, Turoth, Goblins and alott more.", c.npcType, "Jatix");
				c.dialogueAction = -1;
			}
			if (c.teleAction == 12) {
				c.getPA().spellTeleport(3302, 9361, 0);
			}
			if (c.teleAction == 11) {
				c.getPA().spellTeleport(3228, 9392, 0);
			}
			if (c.teleAction == 10) {
				c.getPA().spellTeleport(2705, 9487, 0);
			}
			if (c.teleAction == 9) {
				c.getPA().spellTeleport(3226, 3263, 0);
			}
			if (c.teleAction == 8) {
				c.getPA().spellTeleport(3293, 3178, 0);
			}
			if (c.teleAction == 7) {
				c.getPA().spellTeleport(3118, 9851, 0);
			}
			if (c.teleAction == 100) {
				c.getPA().spellTeleport(LocationConstants.EDGE_PVP.getX(), LocationConstants.EDGE_PVP.getY(), 0);
			} else if (c.teleAction == 101) {
				c.getPA().spellTeleport(LocationConstants.KBD.getX(), LocationConstants.KBD.getY(), 0);
			} else if (c.teleAction == 102) {
				c.getPA().spellTeleport(LocationConstants.CALLISTO.getX(), LocationConstants.CALLISTO.getY(), 0);
			} else if (c.teleAction == 103) {
				c.getDH().sendOption4("Armadyl", "Bandos", "Zamorak", "Saradomin"); // godwars
				c.teleAction = 13;
			} else if (c.teleAction == 104) {
				c.getPA().spellTeleport(LocationConstants.NECHRYAEL.getX(), LocationConstants.NECHRYAEL.getY(), 2);
			} else if (c.teleAction == 105) {
				c.getPA().spellTeleport(LocationConstants.DWARVEN_MINES.getX(), LocationConstants.DWARVEN_MINES.getY(),
						0);
			} else if (c.teleAction == 42) {
				c.getPA().spellTeleport(LocationConstants.BANDOS_AVATAR.getX(), LocationConstants.BANDOS_AVATAR.getY(),
						0);
				c.inBossRoom = true;
			} else if (c.teleAction == 16) {
				c.getPA().spellTeleport(LocationConstants.DWARVEN_MINES.getX(), LocationConstants.DWARVEN_MINES.getY(),
						0);
			} else if (c.teleAction == 37) {
				c.getPA().spellTeleport(LocationConstants.REVENANTS.getX(), LocationConstants.REVENANTS.getY(), 0);
			} else if (c.teleAction == 21) {
				// trav
				c.getPA().spellTeleport(2884, 9798, 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(3046, 9779, 0);
			} else if (c.teleAction == 25) {
				c.getDH().sendOption3("Woodcutting (Camelot)", "Woodcutting (Draynor)", "Previous Page");
				c.teleAction = 28;
			} else if (c.teleAction == 26) {
				c.getDH().sendOption2("Runecrafting (Edgeville)", "Previous Page");
				c.teleAction = 32;
			} else if (c.teleAction == 27) {
				c.getPA().spellTeleport(LocationConstants.DUNGEONEERING.getX(), LocationConstants.DUNGEONEERING.getY(),
						0);
			} else if (c.teleAction == 35) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_TURAEL.getX(), LocationConstants.SLAYER_TURAEL.getY(),
						0);
			} else if (c.teleAction == 40) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_SUMONA.getX(), LocationConstants.SLAYER_SUMONA.getY(),
						0);
			} else if (c.teleAction == 39) {
				c.getPA().spellTeleport(3184, 5469, 0); // chaos tunnel
														// entrance
														// 1
			} else {
				DiceHandler.handleClick(c, actionButtonId);
			}
			
			break;

		case SECOND_OF_FIVE:

			switch (c.dialogueAction) {
			
			case 20923:
				c.getItems().deleteItemInOneSlot(299, 1);
				c.faceLocation(c.getX(), c.getY());
				c.getPA().stepAway();
				c.interactingObject = new RSObject(2983, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
				ObjectManager.addObject(c.interactingObject);
				break;
				
			case 20924:
				c.getItems().deleteItemInOneSlot(299, 1);
				c.faceLocation(c.getX(), c.getY());
				c.getPA().stepAway();
				c.interactingObject = new RSObject(2984, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
				ObjectManager.addObject(c.interactingObject);
				break;

			// level up defender role
			case 5030: {

				final int currentLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.DEFENDER));

				if (currentLevel == 5) {
					c.sendMessage("You reached the maximum level for this role.");
					c.getPA().closeAllWindows();
					break;
				}

				final int cost = c.getBarbarianAssault().getCostForLevel(currentLevel + 1);

				if (c.rolePoints[1] < cost) {
					c.sendMessage(String.format("You need %d defender points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				if (c.getBarbarianAssault().getHonorPoints() < cost) {
					c.sendMessage(String.format("You need %d honour points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				switch (currentLevel) {

				case 1:
					c.getBarbarianAssault().addDefenderExp(200);
					c.sendMessage("Your defender role is now level 2.");
					break;

				case 2:
					c.getBarbarianAssault().addDefenderExp(300);
					c.sendMessage("Your defender role is now level 3.");
					break;

				case 3:
					c.getBarbarianAssault().addDefenderExp(400);
					c.sendMessage("Your defender role is now level 4.");
					break;

				case 4:
					c.getBarbarianAssault().addDefenderExp(500);
					c.sendMessage("Your defender role is now level 5.");
					break;

				default:
					break;

				}

				c.rolePoints[1] -= cost;
				c.getBarbarianAssault().setHonorPoints(c.getBarbarianAssault().getHonorPoints() - cost, true);
				c.getPA().closeAllWindows();
			}
				break;

			}

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 2) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[1]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 3022) {
				c.getPA().spellTeleport(LocationConstants.FARMING_FALADOR.getX(),
						LocationConstants.FARMING_FALADOR.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 3024) {
				c.getPA().spellTeleport(LocationConstants.LUMBRDGE_TREE_FARM.getX(),
						LocationConstants.LUMBRDGE_TREE_FARM.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 8093) {
				if (c.getItems().playerHasItem(13727, 750)) {
					c.getItems().deleteItemInOneSlot(13727, 750);
					c.getItems().addItem(2528, 1);
					c.sendMessage("You have been rewarded a XP lamp.");
					c.getPA().closeAllWindows();
				}
			} else if (c.dialogueAction == 3638) {
				c.getDH().sendOption5("Taverly Dungeon", "Brimhaven Dungeon", "Kalphite Lair", "Asgarnian Ice Dungeon",
						"Next Page");
				c.dialogueAction = 3539;
				c.teleAction = 21;
				break;
			} else if (c.dialogueAction == 537) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy super sets in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				int superStrAmount = 500;
				int superAttAmount = 500;
				int superDefAmount = 500;
				int totalPrice = 0;

				totalPrice += (11385 * superStrAmount);
				totalPrice += (6727 * superAttAmount);
				totalPrice += (3794 * superDefAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 3) {
					c.getItems().addItem(2441, superStrAmount);
					c.getItems().addItem(2437, superAttAmount);
					c.getItems().addItem(2443, superDefAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The super sets has been added to your inventory");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 4 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 8930) {
				if (c.getItems().playerHasItem(6735, 1)) {
					if (c.getItems().playerHasItem(14598, 58)) {
						c.getItems().deleteItemInOneSlot(14598, 58);
						c.getItems().deleteItemInOneSlot(6735, 1);
						c.getItems().addItem(15020, 1);
					} else {
						c.sendMessage("You need 58 enchanted stones to enchant this ring.");
						c.getPA().closeAllWindows();
					}
				} else {
					c.sendMessage("You dont have a Warrior ring to enchant.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 14329) {
				c.getDH().sendNpcChat1("You currently have <col=ff>" + c.getLoyaltyPoints() + "</col> LoyaltyPoints.",
						c.npcType, "King's messenger");
				c.nextChat = 1210;
				break;
			} else if (c.dialogueAction == 14374) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy Pure set with Iron Man mode.");
				} else {
					c.getItems().pureSet();
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 2250) {
				if (c.difficulty != 0) {
					c.getPA().closeAllWindows();
					c.sendMessage("You can only change your stats on Normal mode.");
					return;
				}
				if (c.getLoyaltyPoints() >= 200) {
					c.dialogueAction = 11112;
					c.getPacketSender().sendEnterAmountInterface();
				} else {
					c.sendMessage("You need 200 Loyalty Points to set a skill.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 14330) {
				if (c.inWild()) {
					return;
				}
				if (!(c.getItems().playerHasItem(995, 2500000))) {
					c.sendMessage("You need 2.5m to reset this skill.");
					c.getPA().closeAllWindows();
					return;
				}
				for (int element : c.playerEquipment) {
					if (element > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						c.getPA().closeAllWindows();
						return;
					}
				}
				try {
					c.sendMessage("Your Strength level has been reset.");
					int skill = 2;
					int level = 1;
					c.getSkills().setStaticLevel(skill, level);
					c.calcCombat();
					c.getCombat().resetPrayers();
					c.getPA().closeAllWindows();
				} catch (Exception e) {
					return;
				}
				return;
			} else if (c.dialogueAction == 8463 || c.dialogueAction == 8466 || c.dialogueAction == 1599
					|| c.dialogueAction == 1562 || c.dialogueAction == 7782 || c.dialogueAction == 8469
					|| c.dialogueAction == 9087 || c.dialogueAction == 1190) { // Slayer
																				// masters
				c.getShops().openShop(54);
			} else if (c.teleAction == 12) {
				c.getPA().spellTeleport(2908, 9694, 0);
			} else if (c.teleAction == 11) {
				c.getPA().spellTeleport(3237, 9384, 0);
			} else if (c.teleAction == 10) {
				c.getPA().spellTeleport(3219, 9366, 0);
			} else if (c.teleAction == 9) {
				c.getPA().spellTeleport(2916, 9800, 0);
			} else if (c.teleAction == 8) {
				c.getPA().spellTeleport(2903, 9849, 0);
			} else if (c.teleAction == 7) {
				c.getPA().spellTeleport(2859, 9843, 0);
			} else if (c.teleAction == 100) {
				c.getPA().spellTeleport(LocationConstants.MAGEBANK_PVP.getX(), LocationConstants.MAGEBANK_PVP.getY(),
						0);
			} else if (c.teleAction == 101) {
				c.getPA().spellTeleport(LocationConstants.CHAOS_ELE.getX(), LocationConstants.CHAOS_ELE.getY(), 0);
			} else if (c.teleAction == 102) {
				c.getPA().spellTeleport(LocationConstants.VENENANTIS.getX(), LocationConstants.VENENANTIS.getY(), 0);
			} else if (c.teleAction == 42) {// nomad
				c.sendMessage("nomad is currently being reworked.");
				c.getPA().closeAllWindows();
				/*
				 * if (c.getItems().playerHasItem(19967)) {
				 * c.getDH().sendDialogues(19967, 0); } else {
				 * c.getDH().sendStatement(
				 * "You need a teleport bag to teleport to nomad."); c.nextChat
				 * = 0; c.dialogueAction = 0; }
				 */
			} else if (c.teleAction == 103) {
				c.getPA().spellTeleport(LocationConstants.DAG_KINGS.getX(), LocationConstants.DAG_KINGS.getY(), 0);
			} else if (c.teleAction == 104) {
				c.getPA().spellTeleport(LocationConstants.KALPHITE_QUEEN.getX(),
						LocationConstants.KALPHITE_QUEEN.getY(), 0);
			} else if (c.teleAction == 105) {
				c.getPA().spellTeleport(LocationConstants.TORM.getX(), LocationConstants.TORM.getY(), 0);
			} else if (c.teleAction == 16) {
				c.getPA().spellTeleport(LocationConstants.GLACORS.getX(), LocationConstants.GLACORS.getY(), 1);
			} else if (c.teleAction == 21) {
				c.getPA().spellTeleport(LocationConstants.BRIMHAVEN_DUNGEON.getX(),
						LocationConstants.BRIMHAVEN_DUNGEON.getY(), 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(3079, 9502, 0);
			} else if (c.teleAction == 25) {
				c.getDH().sendOption4("Fishing (Catherby)", "Fishing (Fishing Guild)", "Fishing (Karambwan)",
						"Previous Page");
				c.teleAction = 29;
			} else if (c.teleAction == 26) {
				c.getDH().sendOption4("Thieving (Varrock)", "Thieving (Edgeville)", "Thieving (Ardougne)",
						"Previous Page");
				c.teleAction = 33;
			} else if (c.teleAction == 37) {
				c.getPA().spellTeleport(1764, 5365, 1);// ancient cavern
			} else if (c.teleAction == 35) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_MAZCHNA.getX(),
						LocationConstants.SLAYER_MAZCHNA.getY(), 0);
			} else if (c.teleAction == 40) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_DURADEL.getX(),
						LocationConstants.SLAYER_DURADEL.getY(), 1);
			} else if (c.teleAction == 39) {
				c.getPA().spellTeleport(3293, 5480, 0); // chaos tunnel
														// entrance
														// 2
			}
			if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2796, 4818, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 11) {
				c.getPA().spellTeleport(2527, 4833, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 12) {
				c.getPA().spellTeleport(2464, 4834, 0);
				c.dialogueAction = -1;
			}
			break;
		// 3rd tele option

		case THIRD_OF_FIVE:

			switch (c.dialogueAction) {
			
			case 20923:
			    c.getItems().deleteItemInOneSlot(299, 1);
			    c.faceLocation(c.getX(), c.getY());
			    c.getPA().stepAway();
			    c.interactingObject = new RSObject(2985, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
			    ObjectManager.addObject(c.interactingObject);
			break;
			
			case 20924:
				c.getItems().deleteItemInOneSlot(299, 1);
				c.faceLocation(c.getX(), c.getY());
				c.getPA().stepAway();
				c.interactingObject = new RSObject(2980, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
				ObjectManager.addObject(c.interactingObject);
				break;

			// level up healer role
			case 5030: {

				final int currentLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.HEALER));

				if (currentLevel == 5) {
					c.sendMessage("You reached the maximum level for this role.");
					c.getPA().closeAllWindows();
					break;
				}

				final int cost = c.getBarbarianAssault().getCostForLevel(currentLevel + 1);

				if (c.rolePoints[2] < cost) {
					c.sendMessage(String.format("You need %d healer points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				if (c.getBarbarianAssault().getHonorPoints() < cost) {
					c.sendMessage(String.format("You need %d honour points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				switch (currentLevel) {

				case 1:
					c.getBarbarianAssault().addHealerExp(200);
					c.sendMessage("Your healer role is now level 2.");
					break;

				case 2:
					c.getBarbarianAssault().addHealerExp(300);
					c.sendMessage("Your healer role is now level 3.");
					break;

				case 3:
					c.getBarbarianAssault().addHealerExp(400);
					c.sendMessage("Your healer role is now level 4.");
					break;

				case 4:
					c.getBarbarianAssault().addHealerExp(500);
					c.sendMessage("Your healer role is now level 5.");
					break;

				default:
					break;

				}

				c.rolePoints[2] -= cost;
				c.getBarbarianAssault().setHonorPoints(c.getBarbarianAssault().getHonorPoints() - cost, true);
				c.getPA().closeAllWindows();
			}
				break;

			}

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 3) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[2]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 3022) {
				c.getPA().spellTeleport(LocationConstants.FARMING_CANIFIS.getX(),
						LocationConstants.FARMING_CANIFIS.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 3024) {
				c.getPA().spellTeleport(LocationConstants.FALADOR_TREE_FARM.getX(),
						LocationConstants.FALADOR_TREE_FARM.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 8093) {
				if (c.getItems().playerHasItem(13727, 450)) {
					if (c.getItems().freeSlots() > 2) {
						c.getItems().deleteItemInOneSlot(13727, 450);
						c.getItems().addItem(445, 97);
						c.getItems().addItem(450, 63);
						c.getItems().addItem(452, 21);
						c.sendMessage("You have been rewarded 97 gold ores, 53 addy ores and 21 rune ores");
						c.getPA().closeAllWindows();
					} else {
						c.sendMessage("You must have at least 3 inventory spaces to get this reward.");
						c.getPA().closeAllWindows();
					}
				}
				break;
			}

			if (c.dialogueAction == 3638) {
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.dialogueAction = 3539;
				c.teleAction = 25;
				break;
			}

			if (c.dialogueAction == 537) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy super restores in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				int superRestoreAmount = 500;
				int totalPrice = 0;

				totalPrice += (11385 * superRestoreAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 1) {
					c.getItems().addItem(3025, superRestoreAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The super restores has been added to your inventory");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 2 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 8930) {
				if (c.getItems().playerHasItem(6731, 1)) {
					if (c.getItems().playerHasItem(14598, 66)) {
						c.getItems().deleteItemInOneSlot(14598, 66);
						c.getItems().deleteItemInOneSlot(6731, 1);
						c.getItems().addItem(15018, 1);
					} else {
						c.sendMessage("You need 66 enchanted stones to enchant this ring.");
						c.getPA().closeAllWindows();
					}
				} else {
					c.sendMessage("You dont have a Seer's ring to enchant");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 14329) {
				c.getDH().sendDialogues(14332, c.npcType);
				break;
			}
			if (c.dialogueAction == 14374) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy melee set with Iron Man mode.");
				} else {
					c.getItems().meleeSet();
				}
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 2250) {
				if (c.difficulty != 0) {
					c.getPA().closeAllWindows();
					c.sendMessage("You can only change your stats on Normal mode.");
					return;
				}
				if (c.getLoyaltyPoints() >= 200) {
					c.dialogueAction = 11111;
					c.getPacketSender().sendEnterAmountInterface();
				} else {
					c.sendMessage("You need 200 Loyalty Points to set a skill.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 14330) {
				if (c.inWild()) {
					return;
				}
				if (!(c.getItems().playerHasItem(995, 2500000))) {
					c.sendMessage("You need 2.5m to reset this skill.");
					c.getPA().closeAllWindows();
					return;
				}
				for (int element : c.playerEquipment) {
					if (element > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						c.getPA().closeAllWindows();
						return;
					}
				}
				try {
					c.sendMessage("Your Defence level has been reset.");
					int skill = 1;
					int level = 1;
					c.getSkills().setStaticLevel(skill, level);
					c.calcCombat();
					c.getCombat().resetPrayers();
					c.getPA().closeAllWindows();
				} catch (Exception e) {
					return;
				}
				return;
			}

			if (c.dialogueAction == 8463 || c.dialogueAction == 8466 || c.dialogueAction == 1599
					|| c.dialogueAction == 1562 || c.dialogueAction == 7782 || c.dialogueAction == 8469
					|| c.dialogueAction == 9087 || c.dialogueAction == 1190) { // slayer
																				// masters
				c.getDH().sendDialogues(8466, c.npcType);
				return;
			}

			if (c.teleAction == 12) {
				c.getPA().spellTeleport(2739, 5088, 0);
				return;
			}
			if (c.teleAction == 11) {
				c.getPA().spellTeleport(3280, 9372, 0);
				return;
			}
			if (c.teleAction == 10) {
				c.getPA().spellTeleport(3241, 9364, 0);
				return;
			}
			if (c.teleAction == 9) {
				c.getPA().spellTeleport(3159, 9895, 0);
				return;
			}
			if (c.teleAction == 8) {
				c.getPA().spellTeleport(2912, 9831, 0);
				return;
			}
			if (c.teleAction == 7) {
				c.getPA().spellTeleport(2843, 9555, 0);
				return;
			}
			if (c.teleAction == 100) {
				c.getPA().spellTeleport(LocationConstants.CLANWARS_PVP.getX(), LocationConstants.CLANWARS_PVP.getY(),
						0);
			} else if (c.teleAction == 101) {
				c.getPA().spellTeleport(LocationConstants.SKELETAL_HORROR.getX(),
						LocationConstants.SKELETAL_HORROR.getY(), 0);
			} else if (c.teleAction == 102) {
				c.getPA().spellTeleport(LocationConstants.REVENANTS.getX(), LocationConstants.REVENANTS.getY(), 0);
			} else if (c.teleAction == 103) {
				c.getPA().spellTeleport(LocationConstants.ABYSSAL_DEMONS.getX(),
						LocationConstants.ABYSSAL_DEMONS.getY(), 2);
			} else if (c.teleAction == 104) {
				c.getPA().spellTeleport(LocationConstants.CORP.getX(), LocationConstants.CORP.getY(), 0);
			} else if (c.teleAction == 42) {
				c.getPA().spellTeleport(LocationConstants.GIANT_MOLE.getX(), LocationConstants.GIANT_MOLE.getY(), 0);
				c.sendMessage("Dig with a spade on top of the mole hills.");
			} else if (c.teleAction == 16) {
				// Forbidden Lair
				c.getPA().spellTeleport(2690, 10124, 0);
			} else if (c.teleAction == 21) {
				c.getPA().spellTeleport(3484, 9510, 2);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(2813, 3436, 0);
			} else if (c.teleAction == 25) {
				c.getDH().sendOption3("Mining (Neitiznot)", "Mining (Falador)", "Previous Page");
				c.teleAction = 30;
			} else if (c.teleAction == 26) {
				c.getDH().sendOption4("Agility (Gnome course)", "Agility (Barbarian outpost)",
						"Agility (wildy course) <col=ff0000>(53 Wildy)", "Previous Page");
				c.teleAction = 34;
			} else if (c.teleAction == 27) {
				c.getPA().spellTeleport(LocationConstants.SUMMONING.getX(), LocationConstants.SUMMONING.getY(), 0);
			} else if (c.teleAction == 35) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_VANNAKA.getX(),
						LocationConstants.SLAYER_VANNAKA.getY(), 0);
			} else if (c.teleAction == 40) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_KURADAL.getX(),
						LocationConstants.SLAYER_KURADAL.getY(), 1);
			} else if (c.teleAction == 37) {
				c.getDH().sendOption5("Entrance One", "Entrance Two", "Entrance Three", "Entrance Four",
						"Entrance Five");
				c.teleAction = 39;
			} else if (c.teleAction == 39) {
				c.getPA().spellTeleport(3290, 5539, 0); // chaos tunnel
														// entrance
														// 3
			} if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2713, 4836, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 11) {
				c.getPA().spellTeleport(2162, 4833, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 12) {
				c.getPA().spellTeleport(2207, 4836, 0);
				c.dialogueAction = -1;
			}
			break;

		// 4th tele option
		case FOURTH_OF_FIVE:

			switch (c.dialogueAction) {
			
			case 20923:
			    c.getItems().deleteItemInOneSlot(299, 1);
			    c.faceLocation(c.getX(), c.getY());
			    c.getPA().stepAway();
			    c.interactingObject = new RSObject(2986, c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
			    ObjectManager.addObject(c.interactingObject);
			break;
			
			case 20924:
				c.getItems().deleteItemInOneSlot(299, 1);
				c.faceLocation(c.getX(), c.getY());
				c.getPA().stepAway();
				c.interactingObject = new RSObject(2987 + Misc.random(1), c.getX(), c.getY(), c.getHeightLevel(), 0, 10, -1, 100);
				ObjectManager.addObject(c.interactingObject);
				break;

			// level up collector role
			case 5030: {

				final int currentLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.COLLECTOR));

				if (currentLevel == 5) {
					c.sendMessage("You reached the maximum level for this role.");
					c.getPA().closeAllWindows();
					break;
				}

				final int cost = c.getBarbarianAssault().getCostForLevel(currentLevel + 1);

				if (c.rolePoints[3] < cost) {
					c.sendMessage(String.format("You need %d collector points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				if (c.getBarbarianAssault().getHonorPoints() < cost) {
					c.sendMessage(String.format("You need %d honour points to buy this.", cost));
					c.getPA().closeAllWindows();
					return;
				}

				switch (currentLevel) {

				case 1:
					c.getBarbarianAssault().addCollectorExp(200);
					c.sendMessage("Your collector role is now level 2.");
					break;

				case 2:
					c.getBarbarianAssault().addCollectorExp(300);
					c.sendMessage("Your collector role is now level 3.");
					break;

				case 3:
					c.getBarbarianAssault().addCollectorExp(400);
					c.sendMessage("Your collector role is now level 4.");
					break;

				case 4:
					c.getBarbarianAssault().addCollectorExp(500);
					c.sendMessage("Your collector role is now level 5.");
					break;

				default:
					break;

				}

				c.rolePoints[3] -= cost;
				c.getBarbarianAssault().setHonorPoints(c.getBarbarianAssault().getHonorPoints() - cost, true);
				c.getPA().closeAllWindows();
			}
				break;

			}

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 4) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[3]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 3022) {
				c.getPA().spellTeleport(LocationConstants.FARMING_CATHERBY.getX(),
						LocationConstants.FARMING_CATHERBY.getY(), 0);
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 3024) {
				c.getPA().spellTeleport(LocationConstants.TAVERLY_TREE_FARM.getX(),
						LocationConstants.TAVERLY_TREE_FARM.getY(), 0);
				c.getPA().closeAllWindows();
			}
		    if (c.dialogueAction == 8093) {
				if (c.getItems().playerHasItem(13727, 11000)) {
					if (c.getItems().freeSlots() > 4) {
						c.getItems().deleteItemInOneSlot(13727, 11000);
						c.getItems().addItem(20787, 1);
						c.getItems().addItem(20788, 1);
						c.getItems().addItem(20789, 1);
						c.getItems().addItem(20790, 1);
						c.getItems().addItem(20791, 1);
						c.sendMessage("You have been rewarded a mining suite.");
						c.getPA().closeAllWindows();
					} else {
						c.sendMessage("You must have at least 5 inventory spaces to get this reward.");
						c.getPA().closeAllWindows();
					}
				}
				break;
			} else if (c.dialogueAction == 3638) {
				c.getDH().sendOption5("King Black Dragon <col=ff0000>(Wildy)", "Chaos Elemental <col=ff0000>(Wildy)", "Zulrah",
						"Godwars", "Next Page");
				c.dialogueAction = 3539;
				c.teleAction = 3;
				break;
			}

			if (c.dialogueAction == 537) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy sara brews in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				int superRestoreAmount = 500;
				int totalPrice = 0;

				totalPrice += (17020 * superRestoreAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 1) {
					c.getItems().addItem(6686, superRestoreAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The sara brews has been added to your inventory");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 2 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 8930) {
				if (c.getItems().playerHasItem(6733, 1)) {
					if (c.getItems().playerHasItem(14598, 106)) {
						c.getItems().deleteItemInOneSlot(14598, 106);
						c.getItems().deleteItemInOneSlot(6733, 1);
						c.getItems().addItem(15019, 1);
					} else {
						c.sendMessage("You need 106 enchanted stones to enchant this ring.");
						c.getPA().closeAllWindows();
					}
				} else {
					c.sendMessage("You dont have a Archer ring to enchant.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 14329) {
				c.getDH().sendOption4("Varrock", "Edgeville", "Falador", "Neitiznot");
				c.dialogueAction = 2255;
				break;
			}
			if (c.dialogueAction == 14374) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy range set with Iron Man mode.");
				} else {
					c.getItems().rangeSet();
				}
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 2250) {
				if (c.difficulty != 0) {
					c.getPA().closeAllWindows();
					c.sendMessage("You can only change your stats on Normal mode.");
					return;
				}
				if (c.getLoyaltyPoints() >= 200) {
					c.dialogueAction = 11114;
					c.getPacketSender().sendEnterAmountInterface();
				} else {
					c.sendMessage("You need 200 Loyalty Points to set a skill.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 14330) {
				if (c.inWild()) {
					return;
				}
				if (!(c.getItems().playerHasItem(995, 2500000))) {
					c.sendMessage("You need 2.5m to reset this skill.");
					c.getPA().closeAllWindows();
					return;
				}
				for (int element : c.playerEquipment) {
					if (element > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						c.getPA().closeAllWindows();
						return;
					}
				}
				try {
					c.sendMessage("Your Range level has been reset.");
					int skill = 4;
					int level = 1;
					c.getSkills().setStaticLevel(skill, level);
					c.calcCombat();
					c.getCombat().resetPrayers();
					c.getPA().closeAllWindows();
				} catch (Exception e) {
					return;
				}
				return;
			}
			if (c.teleAction == 11) {
				c.getDH().sendOption5("Black Demon", "Dust Devils", "Nechryael", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 10;
				break;
			}
			if (c.teleAction == 10) {
				c.getDH().sendOption5("Goblins", "Baby blue dragon", "Moss Giants", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 9;
				break;
			}
			if (c.teleAction == 9) {
				c.getDH().sendOption5("Al-kharid warrior", "Ghosts", "Giant Bats", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 8;
				break;
			}
			if (c.teleAction == 8) {
				c.getDH().sendOption5("Hill Giants", "Hellhounds", "Lesser Demons", "Chaos Dwarf", "-- Next Page --");
				c.teleAction = 7;
				break;
			}
			if (c.teleAction == 7) {
				c.getPA().spellTeleport(2923, 9759, 0);
			}
			if (c.teleAction == 100) {
				c.getPA().spellTeleport(LocationConstants.EASTDRAGONS_PVP.getX(),
						LocationConstants.EASTDRAGONS_PVP.getY(), 0);
			} else if (c.teleAction == 101) {
				c.getPA().spellTeleport(LocationConstants.SCORPIA.getX(), LocationConstants.SCORPIA.getY(), 0);
			} else if (c.teleAction == 102) {
				c.getPA().spellTeleport(LocationConstants.LAVA_DRAGONS.getX(), LocationConstants.LAVA_DRAGONS.getY(),
						0);
			} else if (c.teleAction == 103) {
				c.getPA().spellTeleport(LocationConstants.GLACORS.getX(), LocationConstants.GLACORS.getY(), 1);
			} else if (c.teleAction == 104) {
				c.getPA().spellTeleport(LocationConstants.DARK_BEASTS.getX(), LocationConstants.DARK_BEASTS.getY(), 0);
			} else if (c.teleAction == 105) {
				c.getPA().spellTeleport(LocationConstants.MUTATED_JADINKOS.getX(),
						LocationConstants.MUTATED_JADINKOS.getY(), 0);
			} else if (c.teleAction == 25) {
				c.getDH().sendOption2("Farming (Catherby)", "Previous Page");
				c.teleAction = 31;
			} else if (c.teleAction == 26) {
				c.getDH().sendOption5("Turael (no requierments)", "Mazchna (20 combat lvl)", "Vannaka (40 combat lvl)",
						"Chaeldar (75 combat lvl)", "Next Page");
				c.teleAction = 35;
			} else if (c.teleAction == 27) {
				c.getPA().spellTeleport(LocationConstants.HUNTER.getX(), LocationConstants.HUNTER.getY(), 0);
				c.sendMessage("Animals can be found in the jungle outside of the city.");
			} else if (c.teleAction == 35) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_CHAELDAR.getX(),
						LocationConstants.SLAYER_CHAELDAR.getY(), 0);
			} else if (c.teleAction == 40) {
				c.getPA().spellTeleport(LocationConstants.SLAYER_MORVAN.getX(), LocationConstants.SLAYER_MORVAN.getY(), 0);
			} else if (c.teleAction == 42) {
				c.getPA().spellTeleport(LocationConstants.BARRELCHEST.getX(), LocationConstants.BARRELCHEST.getY(), 0);
			} else if (c.teleAction == 39) {
				c.getPA().spellTeleport(3249, 5491, 0); // chaos tunnel
														// entrance
														// 4
				// } else if (c.teleAction == 37) {
				// c.getPA().spellTeleport(2509, 3634, 0); // light house
			} else if (c.teleAction == 37) {
				c.getPA().spellTeleport(2442, 10146, 0); // waterbirth
															// dungeon
			} else if (c.teleAction == 21) { // ice dungeon
				c.getPA().spellTeleport(3007, 9550, 0);
			} else if (c.teleAction == 16) { // Jungle dungeon
				c.getPA().spellTeleport(2850, 9478, 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(2724, 3484, 0);
				c.sendMessage("For magic logs, try north of the duel arena.");
			} if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2660, 4839, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 11) {
				// c.getPA().spellTeleport(2527, 4833, 0); astrals here
				// c.getRunecrafting().craftRunes(2489);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 12) {
				// c.getPA().spellTeleport(2464, 4834, 0); bloods here
				// c.getRunecrafting().craftRunes(2489);
				c.dialogueAction = -1;
			}
			break;
		case FIFTH_OF_FIVE:
			
		    if (c.dialogueAction == 20923) {
		    	c.getDH().sendOption5("blue", "purple", "blue and purple", "black or white", "back");
		    	c.dialogueAction = 20924;
				break;
		    }
		    
		    if (c.dialogueAction == 20924) {
		    	c.getDH().sendOption5("red", "red", "orange", "rainbow", "next");
		    	c.dialogueAction = 20923;
		    	break;
		    }

			if (c.dialogueAction == 5030) {
				c.getDH().sendNpcChat4(String.format("You currently have %d attacker points", c.rolePoints[0]),
						String.format("You currently have %d defender points", c.rolePoints[1]),
						String.format("You currently have %d healer points", c.rolePoints[2]),
						String.format("You currently have %d collector points", c.rolePoints[2]), c.npcType,
						"Commander Connad");
				c.nextChat = 0;
				break;
			}

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				partyLeader.sendMessage(c.getNameSmartUp() + "declined.");
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 3024) {
				c.getDH().sendDialogues(3022, c.npcType);
				break;
			}
			if (c.dialogueAction == 3022) {
				c.getDH().sendDialogues(3023, c.npcType);
				break;
			}
			if (c.dialogueAction == 8093) {
				if (c.getItems().playerHasItem(13727, 15000)) {
					c.getItems().deleteItemInOneSlot(13727, 15000);
					c.getItems().addItem(13661, 1);
					c.sendMessage("You have been rewarded a Inferno adze.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 3638) {
				c.getDH().sendOption2("Donator Zone", "Supreme Zone");
				c.dialogueAction = 8887;
				break;
			}

			if (c.dialogueAction == 537) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy prayer renewals in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				int prayerRenewalsAmount = 100;
				int totalPrice = 0;

				totalPrice += (79000 * prayerRenewalsAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 1) {
					c.getItems().addItem(21631, prayerRenewalsAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The prayer renewals has been added to your inventory");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 2 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 8930) {
				if (c.getItems().playerHasItem(6737, 1)) {
					if (c.getItems().playerHasItem(14598, 112)) {
						c.getItems().deleteItemInOneSlot(14598, 112);
						c.getItems().deleteItemInOneSlot(6737, 1);
						c.getItems().addItem(15220, 1);
					} else {
						c.sendMessage("You need 112 enchanted stones to enchant this ring.");
						c.getPA().closeAllWindows();
					}
				} else {
					c.sendMessage("You dont have a Berserker ring to enchant.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.dialogueAction == 14329) {
				c.getDH().sendDialogues(2247, c.npcType);
				break;
			}

			if (c.dialogueAction == 14374) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy magic set with Iron Man mode.");
				} else {
					c.getItems().magicSet();
				}
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 2250) {
				c.getDH().sendDialogues(2251, c.npcType);
				break;
			}
			if (c.dialogueAction == 14330) {
				c.getDH().sendDialogues(14331, c.npcType);
				break;
			}

			if (c.dialogueAction == 8466) { // Mazchna
				if (c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 1599) { // Vannaka
				if (c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 1562) { // Chaeldar
				if (c.getSlayerTask() == null || c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 7782) { // Sumona
				if (c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 8469) { // Duradel
				if (c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 9087) { // Kuradal
				if (c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 1190) { // Morvran
				if (c.getSlayerTask().getAssignment() == null) {
					c.sendMessage("You don't have a slayer task");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.isIronMan() || c.getItems().deleteItem3(995, 100000)) {
					c.setSlayerTasksCompleted(0);
					c.getSlayerTask().cancel(c);
					c.sendMessage("Your slayer task and tasks completed has been sucessfully reset");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You need 100.000 to reset your slayer task and tasks completed.");
					c.getPA().closeAllWindows();
				}
				break;
			}

			if (c.teleAction == 8) {
				c.getDH().sendOption5("Goblins", "Baby blue dragon", "Moss Giants", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 9;
				break;
			}
			if (c.teleAction == 9) {
				c.getDH().sendOption5("Black Demon", "Dust Devils", "Nechryael", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 10;
				break;
			}
			if (c.teleAction == 11) {
				c.getDH().sendOption5("Infernal Mage", "Dark Beasts", "Abyssal Demon", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 12;
				break;
			}
			if (c.teleAction == 10) {
				c.getDH().sendOption5("GarGoyle", "Bloodveld", "Banshee", "-- Previous Page --", "-- Next Page --");
				c.teleAction = 11;
				break;
			}
			if (c.teleAction == 7) {
				c.getDH().sendOption5("Al-kharid warrior", "Ghosts", "Giant Bats", "-- Previous Page --",
						"-- Next Page --");
				c.teleAction = 8;
				break;
			}
			/*
			 * if (c.teleAction == 1) { //island c.getPA().spellTeleport(2895,
			 * 2727, 0); } else if (c.teleAction == 2) { //last minigame spot c.
			 * sendMessage("Suggest something for this spot on the forums!" );
			 * c.getPA().closeAllWindows(); } else if (c.teleAction == 3) {
			 * //last monster spot c.
			 * sendMessage("Suggest something for this spot on the forums!" );
			 * c.getPA().closeAllWindows(); } else if (c.teleAction == 4) {
			 * //dark castle multi easts c.getPA().spellTeleport(3037, 3652, 0);
			 * } else if (c.teleAction == 5) {
			 * c.getPA().spellTeleport(2812,3463,0); }
			 */
			if (c.teleAction == 101) {
				c.getDH().sendOption5("Callisto", "Venenatis", "Revenants", "Lava Dragons", "Previous Page");
				c.teleAction = 102;
			} else if (c.teleAction == 103) {
				c.getDH().sendOption5("Nechryael", "Kalphite Queen", "Coproreal Beast", "Dark Beast", "Next Page");
				c.teleAction = 104;
			} else if (c.teleAction == 104) {
				c.getDH().sendOption5("Chaos Dwarf Hand Cannoneers", "Tormented Demons", "Zulrah",
						"Mutated Jadinkos", "Next Page");
				c.teleAction = 105;
			} else if (c.teleAction == 105) {
				c.getDH().sendOption5("Bandos Avatar", "Nomad", "Nex", "Give us ideas", "Previous Page");
				c.teleAction = 106;
			} else if (c.teleAction == 102) {
				c.getDH().sendOption5("King Black Dragon", "Chaos Elemental", "Skeletal Horror", "Scorpia",
						"Next Page");
				c.teleAction = 101;
			} else if (c.teleAction == 42) {
				c.getDH().sendOption5("King Black Dragon <col=ff0000>(Wildy)", "Chaos Elemental <col=ff0000>(Wildy)", "Zulrah",
						"Godwars", "Next Page");
				c.teleAction = 3;
			} else if (c.teleAction == 16) {
				c.getDH().sendOption5("Revenant Dungeon <col=ff0000>(Wildy)", "Ancient Cavern", "Chaos Tunnel",
						"Waterbirth Dungeon", "Previous Page");
				c.teleAction = 37;
			} else if (c.teleAction == 37) {
				c.getDH().sendOption5("Taverly Dungeon", "Brimhaven Dungeon", "Kalphite Lair", "Asgarnian Ice Dungeon",
						"Next Page");
				c.teleAction = 21;
			} else if (c.teleAction == 21) {
				c.getDH().sendOption5("Dwarven Mines", "Glacor Lair", "Forbidden Dungeon", "Jungle Dungeon",
						"Next Page");
				c.teleAction = 16;
			} else if (c.teleAction == 25) {
				c.getDH().sendOption5("Runecrafting", "Thieving", "Agility", "Slayer", "Next Page");
				c.teleAction = 26;
			} else if (c.teleAction == 26) {
				c.getDH().sendOption5("Dungeoneering", "Construction <col=ff0000>(Coming soon)", "Summoning", "Hunter",
						"Previous Page");
				c.teleAction = 27;
			} else if (c.teleAction == 27) {
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.teleAction = 25;
			} else if (c.teleAction == 35) {
				c.getDH().sendOption5("Sumona (95 combat lvl & 35 slayer lvl)",
						"Duradel (100 combat lvl & 50 slayer lvl)", "Kuradal (110 combat lvl & 75 slayer lvl)",
						"Morvran (120 combat lvl & 85 slayer lvl)", "Previous Page");
				c.teleAction = 40;
			} else if (c.teleAction == 40) {
				c.getDH().sendOption5("Turael (no requierments)", "Mazchna (20 combat lvl)", "Vannaka (40 combat lvl)",
						"Chaeldar (75 combat lvl)", "Next Page");
				c.teleAction = 35;
			} else if (c.teleAction == 39) {
				c.getPA().spellTeleport(3235, 5560, 0); // chaos tunnel
														// entrance
														// 5
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(2812, 3463, 0);
			} else if (c.teleAction == 100) {
				c.getPA().spellTeleport(LocationConstants.WESTDRAGONS_PVP.getX(), LocationConstants.WEST_DRAGONS.getY(),
						0);
			}
			if (c.dialogueAction == 10 || c.dialogueAction == 11) {
				c.dialogueId++;
				c.getDH().sendDialogues(c.dialogueId, 0);
			} else if (c.dialogueAction == 12) {
				c.dialogueId = 17;
				c.getDH().sendDialogues(c.dialogueId, 0);
			}
			break;

		case 57226:
		case 57225:
		case 57227:
		case 57228:
		case 57229:
		case 57230:
		case 57231:
		case 57232:
			TanHide.handleActionButton(c, actionButtonId);
			break;
		case 151045:
			c.getPacketSender().showInterface(39700);
			break;
		case 155026:
			c.getPacketSender().showInterface(38700);
			break;
		case 114143:
			c.getPacketSender().showInterface(29337);
			break;
		case 114174:
			c.getPacketSender().showInterface(29293);
			break;
		case 114144:
			c.getPacketSender().showInterface(29361);
			break;
		case 114208:
			c.getPacketSender().showInterface(29293);
			break;
		case 108005:
			c.getPacketSender().sendFrame106(3);
			c.getPacketSender().showInterface(15106);
			c.getItems().writeBonus();
			break;
		// case 58253:
		case 108006: // items kept on death
			c.getPacketSender().sendFrame126(Config.SERVER_NAME + " - Item's Kept on Death", 17103);
			c.getPA().StartBestItemScan();
			c.EquipStatus = 0;
			
			List<Item> temp = new ArrayList<>(42);
			
			c.getPacketSender().resetInvenory(10494);
			c.getPacketSender().resetInvenory(10600);
			
			int countScales = 0;
			
			boolean keepBlowpipe = c.WillKeepItem1 == BlowPipeCharges.BLOWPIPE_FULL || c.WillKeepItem2 == BlowPipeCharges.BLOWPIPE_FULL || c.WillKeepItem3 == BlowPipeCharges.BLOWPIPE_FULL || c.WillKeepItem4 == BlowPipeCharges.BLOWPIPE_FULL;
			boolean hasBlowpipe = false;
			
			if (c.WillKeepItem1 > 0) {
				c.getPacketSender().displayItemOnInterface(10494, c.WillKeepItem1, 0, c.WillKeepAmt1);
			}
			if (c.WillKeepItem2 > 0) {
				c.getPacketSender().displayItemOnInterface(10494, c.WillKeepItem2, 1, c.WillKeepAmt2);
			}
			if (c.WillKeepItem3 > 0) {
				c.getPacketSender().displayItemOnInterface(10494, c.WillKeepItem3, 2, c.WillKeepAmt3);
			}
			if (c.WillKeepItem4 > 0 && (c.playerPrayerBook == PrayerBook.NORMAL ? c.prayerActive[Prayers.PROTECT_ITEM.ordinal()]
					: c.curseActive[Curses.PROTECT_ITEM.ordinal()])) {
				c.getPacketSender().displayItemOnInterface(10494, c.WillKeepItem4, 3, 1);
			}
			// boolean foundItem = false;
			for (int ITEM = 0; ITEM < 28; ITEM++) {
				int itemId = c.playerItems[ITEM] - 1;
				
				switch (itemId) {
				case BlowPipeCharges.BLOWPIPE_EMPTY:
				case BlowPipeCharges.BLOWPIPE_FULL:
					hasBlowpipe = true;
					break;
				}
				
				if (itemId > 0) {
					boolean tradeable = ItemProjectInsanity.itemIsTradeable(itemId);
					if (!tradeable && !ItemProjectInsanity.itemIsChargeItem(itemId)) {
						if (!tradeable && UntradeableManager.price(itemId) > 0)
							temp.add(new Item(itemId, c.playerItemsN[ITEM]));
						continue;
					}
				}
				if (itemId > 0
						&& !(itemId == c.WillKeepItem1 && ITEM == c.WillKeepItem1Slot)
						&& !(itemId == c.WillKeepItem2 && ITEM == c.WillKeepItem2Slot)
						&& !(itemId == c.WillKeepItem3 && ITEM == c.WillKeepItem3Slot)
						&& !(itemId == c.WillKeepItem4 && ITEM == c.WillKeepItem4Slot)) {
					c.getPacketSender().displayItemOnInterface(10600, itemId, c.EquipStatus,
							c.playerItemsN[ITEM]);
					c.EquipStatus += 1;
					
					countScales += c.getCharges(itemId);
						
				} else if (itemId > 0
						&& (itemId == c.WillKeepItem1 && ITEM == c.WillKeepItem1Slot)
						&& c.playerItemsN[ITEM] > c.WillKeepAmt1) {
					c.getPacketSender().displayItemOnInterface(10600, itemId, c.EquipStatus,
							c.playerItemsN[ITEM] - c.WillKeepAmt1);
					c.EquipStatus += 1;
				} else if (itemId > 0
						&& (itemId == c.WillKeepItem2 && ITEM == c.WillKeepItem2Slot)
						&& c.playerItemsN[ITEM] > c.WillKeepAmt2) {
					c.getPacketSender().displayItemOnInterface(10600, itemId, c.EquipStatus,
							c.playerItemsN[ITEM] - c.WillKeepAmt2);
					c.EquipStatus += 1;
				} else if (itemId > 0
						&& (itemId == c.WillKeepItem3 && ITEM == c.WillKeepItem3Slot)
						&& c.playerItemsN[ITEM] > c.WillKeepAmt3) {
					c.getPacketSender().displayItemOnInterface(10600, itemId, c.EquipStatus,
							c.playerItemsN[ITEM] - c.WillKeepAmt3);
					c.EquipStatus += 1;
				} else if (itemId > 0
						&& (itemId == c.WillKeepItem4 && ITEM == c.WillKeepItem4Slot)
						&& c.playerItemsN[ITEM] > 1) {
					c.getPacketSender().displayItemOnInterface(10600, itemId, c.EquipStatus,
							c.playerItemsN[ITEM] - 1);
					c.EquipStatus += 1;
				}
				
				
			}
			
			for (int EQUIP = 0; EQUIP < 14; EQUIP++) {
				
				switch (c.playerEquipment[EQUIP]) {
				case BlowPipeCharges.BLOWPIPE_EMPTY:
				case BlowPipeCharges.BLOWPIPE_FULL:
					hasBlowpipe = true;
					break;
				}
				
				if (c.playerEquipment[EQUIP] > 0) {
					boolean tradeable = ItemProjectInsanity.itemIsTradeable(c.playerEquipment[EQUIP]);
					if (!tradeable && !ItemProjectInsanity.itemIsChargeItem(c.playerEquipment[EQUIP])) {
						if (!tradeable && UntradeableManager.price(c.playerEquipment[EQUIP]) > 0)
							temp.add(new Item(c.playerEquipment[EQUIP], c.playerEquipmentN[EQUIP]));
						continue;
					}
				}
				if (c.playerEquipment[EQUIP] > 0
						&& !(c.playerEquipment[EQUIP] == c.WillKeepItem1 && EQUIP + 28 == c.WillKeepItem1Slot)
						&& !(c.playerEquipment[EQUIP] == c.WillKeepItem2 && EQUIP + 28 == c.WillKeepItem2Slot)
						&& !(c.playerEquipment[EQUIP] == c.WillKeepItem3 && EQUIP + 28 == c.WillKeepItem3Slot)
						&& !(c.playerEquipment[EQUIP] == c.WillKeepItem4 && EQUIP + 28 == c.WillKeepItem4Slot)) {
					c.getPacketSender().displayItemOnInterface(10600, c.playerEquipment[EQUIP], c.EquipStatus,
							c.playerEquipmentN[EQUIP]);
					c.EquipStatus += 1;
					
					countScales += c.getCharges(c.playerEquipment[EQUIP]);
					
				} else if (c.playerEquipment[EQUIP] > 0
						&& (c.playerEquipment[EQUIP] == c.WillKeepItem1 && EQUIP + 28 == c.WillKeepItem1Slot)
						&& c.playerEquipmentN[EQUIP] > 1 && c.playerEquipmentN[EQUIP] - c.WillKeepAmt1 > 0) {
					c.getPacketSender().displayItemOnInterface(10600, c.playerEquipment[EQUIP], c.EquipStatus,
							c.playerEquipmentN[EQUIP] - c.WillKeepAmt1);
					c.EquipStatus += 1;
				} else if (c.playerEquipment[EQUIP] > 0
						&& (c.playerEquipment[EQUIP] == c.WillKeepItem2 && EQUIP + 28 == c.WillKeepItem2Slot)
						&& c.playerEquipmentN[EQUIP] > 1 && c.playerEquipmentN[EQUIP] - c.WillKeepAmt2 > 0) {
					c.getPacketSender().displayItemOnInterface(10600, c.playerEquipment[EQUIP], c.EquipStatus,
							c.playerEquipmentN[EQUIP] - c.WillKeepAmt2);
					c.EquipStatus += 1;
				} else if (c.playerEquipment[EQUIP] > 0
						&& (c.playerEquipment[EQUIP] == c.WillKeepItem3 && EQUIP + 28 == c.WillKeepItem3Slot)
						&& c.playerEquipmentN[EQUIP] > 1 && c.playerEquipmentN[EQUIP] - c.WillKeepAmt3 > 0) {
					c.getPacketSender().displayItemOnInterface(10600, c.playerEquipment[EQUIP], c.EquipStatus,
							c.playerEquipmentN[EQUIP] - c.WillKeepAmt3);
					c.EquipStatus += 1;
				} else if (c.playerEquipment[EQUIP] > 0
						&& (c.playerEquipment[EQUIP] == c.WillKeepItem4 && EQUIP + 28 == c.WillKeepItem4Slot)
						&& c.playerEquipmentN[EQUIP] > 1 && c.playerEquipmentN[EQUIP] - 1 > 0) {
					c.getPacketSender().displayItemOnInterface(10600, c.playerEquipment[EQUIP], c.EquipStatus,
							c.playerEquipmentN[EQUIP] - 1);
					c.EquipStatus += 1;
				}
				boolean breakit = false;
			}
			
			if (hasBlowpipe && !keepBlowpipe) {
				if (c.getBlowpipeAmmo() > 0) {
					c.getPacketSender().displayItemOnInterface(10600, c.getBlowpipeAmmoType(), c.EquipStatus, c.getBlowpipeAmmo());
					c.EquipStatus += 1;
				}
				
			}
			
			if (countScales > 0) {
				c.getPacketSender().displayItemOnInterface(10600, BlowPipeCharges.ZULRAH_SCALES, c.EquipStatus, countScales);
				c.EquipStatus += 1;
			}

			c.ResetKeepItems();
			c.getPacketSender().showInterface(17100);
			
			c.getPacketSender().itemsOnInterface(15290, temp, +1); // grim items
			c.getPacketSender().setScrollPos(15289, 0);
			temp.clear();
			temp = null;
			
			break;

		case 59004:
			c.getPA().closeAllWindows();
			break;

		case FIRST_OF_FOUR:

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 1) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[0]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.usingGamesNeck) {
				c.getPA().spellTeleport(3360, 3213, 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			} else if (c.usingSkillsNeck) {
				c.getPA().spellTeleport(2730, 3415, 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			}

			switch (c.dialogueAction) {

			case 2252:
				if (c.difficulty != 0) {
					c.getPA().closeAllWindows();
					c.sendMessage("You can only change your stats on Normal mode.");
					return;
				}
				if (c.getLoyaltyPoints() >= 200) {
					c.dialogueAction = 11116;
					c.getPacketSender().sendEnterAmountInterface();
				} else {
					c.sendMessage("You need 200 Loyalty Points to set a skill.");
					c.getPA().closeAllWindows();
				}
				break;

			case 14331:
				if (c.inWild()) {
					return;
				}
				if (!(c.getItems().playerHasItem(995, 2500000))) {
					c.sendMessage("You need 2.5m to reset this skill.");
					c.getPA().closeAllWindows();
					return;
				}
				for (int element : c.playerEquipment) {
					if (element > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						c.getPA().closeAllWindows();
						return;
					}
				}
				try {
					c.sendMessage("Your Magic level has been reset.");
					int skill = 6;
					int level = 1;
					c.getSkills().setStaticLevel(skill, level);
					c.calcCombat();
					c.getCombat().resetPrayers();
					c.getPA().closeAllWindows();
				} catch (Exception e) {
					return;
				}
				break;

			case 2254:
				c.getDH().sendOption4("Varrock", "Edgeville", "Falador", "Neitiznot");
				c.dialogueAction = 2255;
				break;

			case 4255:
				c.getDH().sendOption5("Turael (no requierments)", "Mazchna (20 combat lvl)", "Vannaka (40 combat lvl)",
						"Chaeldar (75 combat lvl)", "Next Page");
				c.teleAction = 35;
				break;

			case 8680:
				c.getDH().sendDialogues(8681, 8678);
				break;

			case 2255:
				c.homeTeleActions = 1;
				c.sendMessage("Your home teleport location has been changed to Varrock.");
				c.getPA().closeAllWindows();
				break;

			case 662:
				c.getShops().openShop(9);
				c.dialogueAction = 0;
				break;

			case 1857:
				c.getDH().sendDialogues(1858, 945);
				break;

			case 26937:
				c.getPA().startTeleport(3153, 3923, 0, TeleportType.MODERN);
				break;

			case 7115:
				c.getDH().sendDialogues(662, c.npcType);
				break;

			case 536:
			case 1658:
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy runes in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				int deathsAmount = 4000;
				int bloodsAmount = 2000;
				int watersAmount = 6000;
				int totalPrice = 0;

				totalPrice += (322 * deathsAmount);
				totalPrice += (459 * bloodsAmount);
				totalPrice += (46 * watersAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 3) {
					c.getItems().addItem(560, deathsAmount);
					c.getItems().addItem(565, bloodsAmount);
					c.getItems().addItem(555, watersAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The runes has been added to your inventory");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 4 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;

			case 2:
				c.getPA().startTeleport(LocationConstants.SLAYER_TOWER.getX(), LocationConstants.SLAYER_TOWER.getY(), 0,
						TeleportType.MODERN);
				break;

			case 3:
				c.getPA().startTeleport(LocationConstants.EDGEVILLE_X, LocationConstants.EDGEVILLE_Y, 0, TeleportType.MODERN);
				break;

			case 4:
				c.getPA().startTeleport(3565, 3314, 0, TeleportType.MODERN);
				break;

			case 20:
				c.getPA().startTeleport(2897, 3618, 4, TeleportType.MODERN);
				// c.setKillCount(0);
				c.resetGwdKc();
				break;

			case 21:
				c.getBarbarianAssault().assignRole(BarbarianAssault.Role.ATTACKER);
				break;
			}

			switch (c.teleAction) {

			case 2:
				// barrows
				c.getPA().spellTeleport(3565, 3314, 0);
				break;

			case 33:
				c.getPA().spellTeleport(LocationConstants.THIEVING_VARROCK.getX(),
						LocationConstants.THIEVING_VARROCK.getY(), 0);
				break;

			case 29:
				c.getPA().spellTeleport(LocationConstants.FISHING_CATHERBY.getX(),
						LocationConstants.FISHING_CATHERBY.getY(), 0);
				break;

			case 34:
				c.getPA().spellTeleport(LocationConstants.AGILITY_GNOME.getX(), LocationConstants.AGILITY_GNOME.getY(),
						0);
				break;

			case 13:
				c.getPA().spellTeleport(LocationConstants.KREE_ARRA.getX(), LocationConstants.KREE_ARRA.getY(), 2);
				c.sendMessage("You must know it's not easy, get a team to own that boss!");
				break;

			}
			break;

		case SECOND_OF_FOUR:

			if (c.usingGamesNeck) {
				c.getPA().spellTeleport(2441, 3090, 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			} else if (c.usingSkillsNeck) {
				c.getPA().spellTeleport(LocationConstants.FISHING_GUILD.getX(), LocationConstants.FISHING_GUILD.getY(),
						0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			}

			switch (c.dialogueAction) {

			case 26: {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 2) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[1]);

				c.getBarbarianAssault().joinGroup(partyLeader);
			}
				break;

			case 2252:
				if (c.difficulty != 0) {
					c.getPA().closeAllWindows();
					c.sendMessage("You can only change your stats on Normal mode.");
					return;
				}
				if (c.getDonPoints() >= 5) {
					c.dialogueAction = 11115;
					c.donationSkillReset = true;
					c.getPacketSender().sendEnterAmountInterface();
				} else {
					c.sendMessage("You need 5 Donor Points to set a skill.");
					c.getPA().closeAllWindows();
				}
				break;

			case 14331:
				if (c.inWild()) {
					return;
				}
				if (!(c.getItems().playerHasItem(995, 2500000))) {
					c.sendMessage("You need 2.5m to reset this skill.");
					c.getPA().closeAllWindows();
					return;
				}
				for (int element : c.playerEquipment) {
					if (element > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						c.getPA().closeAllWindows();
						return;
					}
				}
				try {
					c.sendMessage("Your Prayer has been reset");
					int skill = 5;
					int level = 1;
					c.getSkills().setStaticLevel(skill, level);
					c.calcCombat();
					c.getCombat().resetPrayers();
					c.getPA().closeAllWindows();
				} catch (Exception e) {
					return;
				}
				break;

			case 4255:
				c.getDH().sendDialogues(8500, c.npcType);
				break;

			case 2254:
				c.getDH().sendOption2("Lock my xp", "Unlock my xp");
				c.dialogueAction = 2256;
				break;

			case 8680:
				c.getDH().sendDialogues(8684, 8678);
				break;

			case 2255:
				c.homeTeleActions = 0;
				c.sendMessage("Your home teleport location has been changed to Edgeville.");
				c.getPA().closeAllWindows();
				break;

			case 662:
				c.getShops().openShop(12);
				c.dialogueAction = 0;
				break;

			case 1857:
				c.getDH().sendDialogues(1859, 945);
				break;

			case 26937:
				c.getPA().startTeleport(2976, 3923, 0, TeleportType.MODERN);
				break;

			case 7115:
				c.getPacketSender().openUrl("http://www.ikov2.com/store/");
				c.getPA().closeAllWindows();
				break;

			case 1658:
			case 536:
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy runes in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}

				int deathsAmount = 2000;
				int astralsAmount = 4000;
				int earthsAmount = 10000;
				int totalPrice = 0;

				totalPrice += (322 * deathsAmount);
				totalPrice += (345 * astralsAmount);
				totalPrice += (46 * earthsAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 3) {
					c.getItems().addItem(560, deathsAmount);
					c.getItems().addItem(9075, astralsAmount);
					c.getItems().addItem(557, earthsAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The runes has been added to your inventory.");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 4 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;

			case 2:
				c.getPA().startTeleport(2884, 3395, 0, TeleportType.MODERN);
				break;

			case 3:
				c.getPA().startTeleport(3243, 3513, 0, TeleportType.MODERN);
				break;

			case 4:
				c.getPA().startTeleport(2444, 5170, 0, TeleportType.MODERN);
				break;

			case 20:
				c.getPA().startTeleport(2897, 3618, 12, TeleportType.MODERN);
				// c.setKillCount(0);
				c.resetGwdKc();
				break;

			case 21:
				c.getBarbarianAssault().assignRole(BarbarianAssault.Role.DEFENDER);
				break;

			}

			switch (c.teleAction) {

			case 2:
				// assault
				c.getPA().spellTeleport(2605, 3153, 0);
				break;

			case 13:
				c.getPA().spellTeleport(LocationConstants.GENERAL_GRAARDOR.getX(),
						LocationConstants.GENERAL_GRAARDOR.getY(), 2);
				c.sendMessage("You must know it's not easy, get a team to own that boss!");
				break;

			case 33:
				c.getPA().spellTeleport(LocationConstants.THIEVING_EDGE.getX(), LocationConstants.THIEVING_EDGE.getY(),
						0);
				break;

			case 29:
				c.getPA().spellTeleport(LocationConstants.FISHING_GUILD.getX(), LocationConstants.FISHING_GUILD.getY(),
						0);
				break;

			case 34:
				c.getPA().spellTeleport(LocationConstants.AGILITY_BARB.getX(), LocationConstants.AGILITY_BARB.getY(),
						0);
				break;

			}

			break;

		case THIRD_OF_FOUR:

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 3) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[2]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 21) {
				c.getBarbarianAssault().assignRole(BarbarianAssault.Role.HEALER);
				break;
			}

			if (c.teleAction == 34) {
				c.getPA().startTeleport(LocationConstants.AGILITY_WILDY.getX(), LocationConstants.AGILITY_WILDY.getY(),
						0, TeleportType.MODERN);
				break;
			} else if (c.dialogueAction == 2254) {
				c.getDH().sendDialogues(2247, c.npcType);
				break;
			} else if (c.dialogueAction == 4255) {
				Slayer.checkKills(c);
				c.getPA().closeAllWindows();
				break;
			} else if (c.teleAction == 33) {
				c.getPA().spellTeleport(LocationConstants.THIEVING_ARDOUGEN.getX(),
						LocationConstants.THIEVING_ARDOUGEN.getY(), 0);
				break;
			} else if (c.teleAction == 29) {
				c.getPA().spellTeleport(LocationConstants.FISHING_KARAMBWAN.getX(),
						LocationConstants.FISHING_KARAMBWAN.getY(), 0);
				break;
			} else if (c.usingGamesNeck) {
				c.getPA().spellTeleport(LocationConstants.WARRIORS_GUILD.getX(),
						LocationConstants.WARRIORS_GUILD.getY(), 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			} else if (c.usingSkillsNeck) {
				c.getPA().spellTeleport(3046, 9751, 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			} else if (c.dialogueAction == 2255) {
				c.homeTeleActions = 3;
				c.sendMessage("Your home teleport location has been changed to falador.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 662) {
				c.getShops().openShop(13);
				c.dialogueAction = 0;
				break;
			} else if (c.dialogueAction == 1857) {
				c.getDH().sendDialogues(1860, 945);
			} else if (c.dialogueAction == 26937) {
				c.getPA().startTeleport(3292, 3924, 0, TeleportType.MODERN);
				break;
			} else if (c.teleAction == 13) {
				c.getPA().spellTeleport(LocationConstants.KRIL_TSUTSAROTH.getX(),
						LocationConstants.KRIL_TSUTSAROTH.getY(), 2);
				c.sendMessage("You must know it's not easy, get a team to own that boss!");
			} else if (c.dialogueAction == 1658) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy runes in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}
				if (!c.getItems().playerHasItem(995, 1986000)) {
					c.sendMessage("You must have 1,986,000 coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 12) {
					c.getItems().addItem(556, 1000);
					c.getItems().addItem(554, 1000);
					c.getItems().addItem(558, 1000);
					c.getItems().addItem(557, 1000);
					c.getItems().addItem(555, 1000);
					c.getItems().addItem(560, 1000);
					c.getItems().addItem(565, 1000);
					c.getItems().addItem(566, 1000);
					c.getItems().addItem(9075, 1000);
					c.getItems().addItem(562, 1000);
					c.getItems().addItem(561, 1000);
					c.getItems().addItem(563, 1000);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), 1986000);
					c.sendMessage("<col=ff0000>The runes has been added to your inventory.");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 13 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 7115) {
				c.sendMessage("Use the command ::claim to claim your price.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 536) {
				if (c.isIronMan()) {
					c.sendMessage("You cannot buy runes in Iron Man mode.");
					c.getPA().closeAllWindows();
					break;
				}

				int deathsAmount = 4000;
				int soulRunes = 1000;
				int bloodsAmount = 4000;
				int totalPrice = 0;

				totalPrice += (322 * deathsAmount);
				totalPrice += (345 * soulRunes);
				totalPrice += (459 * bloodsAmount);

				if (!c.getItems().playerHasItem(995, totalPrice)) {
					c.sendMessage("You must have " + totalPrice + " coins to buy this package.");
					c.getPA().closeAllWindows();
					break;
				}
				if (c.getItems().freeSlots() > 3) {
					c.getItems().addItem(560, deathsAmount);
					c.getItems().addItem(566, soulRunes);
					c.getItems().addItem(565, bloodsAmount);
					c.getItems().deleteItemInOneSlot(995, c.getItems().getItemSlot(995), totalPrice);
					c.sendMessage("<col=ff0000>The runes has been added to your inventory.");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You must have at least 4 inventory spaces to buy those runes.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 2) {
				c.getPA().startTeleport(2471, 10137, 0, TeleportType.MODERN);
			}
			if (c.dialogueAction == 3) {
				c.getPA().startTeleport(3363, 3676, 0, TeleportType.MODERN);
			}
			if (c.dialogueAction == 4) {
				c.getPA().startTeleport(2659, 2676, 0, TeleportType.MODERN);
			}
			if (c.dialogueAction == 20) {
				c.getPA().startTeleport(2897, 3618, 8, TeleportType.MODERN);
				c.resetGwdKc();
			}

			break;

		case FOURTH_OF_FOUR:

			if (c.dialogueAction == 26) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				partyLeader.sendMessage(c.getNameSmartUp() + " declined.");
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 7115) {
				c.getPA().closeAllWindows();
			} else if (c.dialogueAction == 536) {
				c.getPA().closeAllWindows();
			} else if (c.dialogueAction == 7936) {
				c.getDH().sendDialogues(7937, c.npcType);
				break;
			} else if (c.dialogueAction == 2252) {
				c.getDH().sendDialogues(2249, c.npcType);
				break;
			} else if (c.dialogueAction == 14331) {
				c.getDH().sendDialogues(14330, c.npcType);
				break;
			} else if (c.dialogueAction == 2254) {
				c.sendTourGuide();
				break;
			} else if (c.dialogueAction == 4255) {
				c.getDH().sendDialogues(8501, c.npcType);
			} else if (c.teleAction == 33) {
				c.getDH().sendOption5("Runecrafting", "Thieving", "Agility", "Slayer", "Next Page");
				c.teleAction = 26;
			} else if (c.teleAction == 29) {
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.teleAction = 25;
			} else if (c.teleAction == 34) {
				c.getDH().sendOption5("Runecrafting", "Thieving", "Agility", "Slayer", "Next Page");
				c.teleAction = 26;
			} else if (c.usingGamesNeck) {
				c.getPA().spellTeleport(LocationConstants.TZHAAR.getX(), LocationConstants.TZHAAR.getY(), 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			} else if (c.usingSkillsNeck) {
				c.getPA().spellTeleport(2343, 3808, 0);
				c.getPA().closeAllWindows();
				c.getPA().handleGamesNeck();
			} else if (c.dialogueAction == 2255) {
				c.homeTeleActions = 5;
				c.sendMessage("Your home teleport location has been changed to Neitiznot.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 1215) {
				c.getDH().sendDialogues(1216, c.npcType);
				break;
			} else if (c.dialogueAction == 662) {
				c.getShops().openShop(15);
				c.dialogueAction = 0;
				break;
			} else if (c.dialogueAction == 1857) {
				c.getDH().sendDialogues(1861, 945);
			} else if (c.dialogueAction == 26937) {
				c.getPA().startTeleport(3303, 3890, 0, TeleportType.MODERN);
				break;
			} else if (c.teleAction == 13) {
				c.getPA().spellTeleport(LocationConstants.ZILYANA.getX(), LocationConstants.ZILYANA.getY(), 0);
				c.sendMessage("You must know it's not easy, get a team to own that boss!");
			} else if (c.dialogueAction == 1658) {
				c.getShops().openShop(5);
				c.dialogueAction = 0;
			} else if (c.dialogueAction == 21) {
				c.getBarbarianAssault().assignRole(BarbarianAssault.Role.COLLECTOR);
				break;
			}
			if (c.dialogueAction == 2) {
				c.getPA().startTeleport(2669, 3714, 0, TeleportType.MODERN);
			}
			if (c.dialogueAction == 3) {
				c.getPA().startTeleport(2540, 4716, 0, TeleportType.MODERN);
			}
			if (c.dialogueAction == 4) {
				c.getPA().startTeleport(LocationConstants.DUEL_ARENA.getX(), LocationConstants.DUEL_ARENA.getY(), 0,
						TeleportType.MODERN);
			}
			break;

		case 1093:
		case 1094:
		case 1097:
			if (c.getAutoCastSpell() != null) {
				c.getPA().resetAutocast();
			} else {
				if (c.playerMagicBook == SpellBook.ANCIENT) {
					// if (c.playerEquipment[PlayerConstants.playerWeapon]
					// ==
					// 21777) {
					// c.sendMessage("You cannot use ancient magics with
					// this
					// staff.");
					// c.getPA().setConfig(108, 0);
					// return;
					// }
					// if (c.playerEquipment[Player.playerWeapon] == 4675)
					c.getPacketSender().setSidebarInterface(0, 1689);
					if (actionButtonId == 1093) {
						c.magicDef = true;
						// else
						// c.sendMessage("You can't autocast ancients
						// without an
						// ancient staff.");
					}
				} else if (c.playerMagicBook == SpellBook.NORMAL) {
					if (c.playerEquipment[PlayerConstants.playerWeapon] == 4170) {
						c.getPacketSender().setSidebarInterface(0, 12050);
						if (actionButtonId == 1093) {
							c.magicDef = true;
						}
					} else {
						c.getPacketSender().setSidebarInterface(0, 1829);
						if (actionButtonId == 1093) {
							c.magicDef = true;
						}
					}
				}

			}
			break;

		case 7212: // cancel autocast
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			c.getPA().resetAutocast();
			break;

		case FIRST_OF_TWO: // option one

			switch (c.dialogueAction) {

			case 8591:
				Overlay.soulWarsRewardInterface(c);
				c.dialogueAction = 0;
				break;
				
			case 31:
				c.getBarbarianAssault().startGame();
				c.dialogueAction = 0;
				break;

			case 5029: {

				final int attackerLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.ATTACKER));

				final int defenderLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.DEFENDER));

				final int healerLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.HEALER));

				final int collectorLevel = c.getBarbarianAssault()
						.getRoleLevel(c.getBarbarianAssault().getRoleExperience(Role.COLLECTOR));

				final int nextAttackerLevel = (attackerLevel + 1) > 5 ? 5 : attackerLevel + 1;

				final int nextDefenderLevel = (defenderLevel + 1) > 5 ? 5 : defenderLevel + 1;

				final int nextHealerLevel = (healerLevel + 1) > 5 ? 5 : healerLevel + 1;

				final int nextCollectorLevel = (collectorLevel + 1) > 5 ? 5 : collectorLevel + 1;

				c.getDH().sendOption5(
						String.format("Attacker Level %d (%d attacker and honour points)", nextAttackerLevel,
								c.getBarbarianAssault().getCostForLevel(nextAttackerLevel)),
						String.format("Defender Level %d (%d defender and honour points)", nextDefenderLevel,
								c.getBarbarianAssault().getCostForLevel(nextDefenderLevel)),
						String.format("Healer Level %d (%d healer and honour points)", nextHealerLevel,
								c.getBarbarianAssault().getCostForLevel(nextHealerLevel)),
						String.format("Collector Level %d (%d collector and honour points)", nextCollectorLevel,
								c.getBarbarianAssault().getCostForLevel(nextCollectorLevel)),
						"Check my role points");
				c.dialogueAction = 5030;
			}
				break;

			case 5032:
				c.getPacketSender().openUrl("https://www.youtube.com/watch?v=pJcsqk42sDY");
				c.getDH().sendDialogues(5032, 5030);
				break;

			case 28: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				if (activeRoles.length < 2) {
					return;
				}

				Player toKick = c.getBarbarianAssault().getGroup().get(activeRoles[0]);

				c.getBarbarianAssault().kickPlayer(toKick);
			}
				break;

			case 30: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				if (activeRoles.length < 1) {
					return;
				}

				Player toKick = c.getBarbarianAssault().getGroup().get(activeRoles[0]);

				c.getBarbarianAssault().kickPlayer(toKick);
			}
				break;

			}

			if (c.dialogueAction == 24) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 1) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[0]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 25) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 1) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[0]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 8900) {
				c.getShops().openShop(51);
				break;
			}

			if (c.dialogueAction == 22) {
				c.getBarbarianAssault().leaveGroup();
				break;
			}

			// TODO barbarian assault assign roles
			if (c.dialogueAction == 23) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					break;
				}

				if (c.getBarbarianAssault().getRole() == Role.NONE) {
					Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

					int length = neededRoles.length;

					switch (length) {

					case 1:
						c.getDH().sendOption2(neededRoles[0].getName(), "Nevermind");
						c.dialogueAction = 24;
						break;

					case 2:
						c.getDH().sendOption3(neededRoles[0].getName(), neededRoles[1].getName(), "Nevermind");
						c.dialogueAction = 25;
						break;

					case 3:
						c.getDH().sendOption4(neededRoles[0].getName(), neededRoles[1].getName(),
								neededRoles[2].getName(), "Nevermind");
						c.dialogueAction = 26;
						break;
					}

				}

				break;
			}

			if (c.dialogueAction == 1981) {
				c.demoteToRegularRank();
				c.sendMessage("<col=ff0000>You are no longer Ironman.");
				Titles.lockIronMan(c);
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 1980) {
				c.getShops().openSkillCape();
				break;
			}
			if (c.dialogueAction == 299) {
				FlowerGame.pickFlower(c);
				break;
			}
			if (c.dialogueAction == 80940) {
				c.getDH().sendDialogues(80950, c.npcType);
				break;
			}
			if (c.dialogueAction == 80970) {
				c.getDH().sendDialogues(80980, c.npcType);
				break;
			}
			if (c.dialogueAction == 1526) {
				c.getShops().openShop(45);
				break;
			}
			if (c.dialogueAction == 6895) {
				if (c.getDonPoints() >= 6) {
					c.setDonPoints(c.getDonPoints() - 6, true);
					c.getItems().addItem(4836, 1);
				} else {
					c.sendMessage("You don't have enough donator points");
				}
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 8467) {

			} else if (c.dialogueAction == 20798) {
				if (c.getItems().addItem(DuellistsCap.TIER_1.id, 1)) {
					c.sendMessage("Fadli gives you a Duelist's Cap");
				} else {
					c.sendMessage("You do not have enough inventory space for this.");
				}
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 464) {
				if (!(c.getItems().playerHasItem(7535) && c.getItems().playerHasItem(7534))) {
					c.getDH().sendDialogues(465, c.npcType);
					return;
				}
				c.getPA().movePlayer(3806, 3525, 0);
				c.getItems().deleteItemInOneSlot(7535, 1);
				c.getItems().deleteItemInOneSlot(7534, 1);
				c.sendMessage("You have lost your diving gear somewhere on the beach.");
				c.sendMessage("You dive under the water to the secret island.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 466) {
				if (!c.getItems().playerHasItem(995, 6000000)) {
					c.sendMessage("You need 6 mill to buy the diving gear!");
					;
					c.getPA().closeAllWindows();
					return;
				}
				if (c.getItems().freeSlots() > 1) {
					c.getItems().deleteItemInOneSlot(995, 6000000);
					c.getItems().addItem(7535, 1);
					c.getItems().addItem(7534, 1);
					c.getDH().sendDialogues(464, c.npcType);
				} else {
					c.sendMessage("You must have at least 2 inventory spaces to buy this item.");
					c.getPA().closeAllWindows();
				}
			} else if (c.dialogueAction == 7942) {
				if (!c.getItems().playerHasItem(995, 5000000)) {
					c.sendMessage("You need 5 mill to get the veteran cape and hood");
					c.getPA().closeAllWindows();
					return;
				}
				if (c.getItems().freeSlots() > 1) {
					c.getItems().deleteItemInOneSlot(995, 5000000);
					c.getItems().addItem(20763, 1);
					c.getItems().addItem(20764, 1);
				} else {
					c.sendMessage("You must have at least 2 inventory spaces to buy this item.");
					c.getPA().closeAllWindows();
				}
			} else if (c.dialogueAction == 9061) {
				DegradableItem.repairItem(c, c.degradedItem);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 8929) {
				c.getDH().sendDialogues(8931, c.npcType);
				break;
			} else if (c.dialogueAction == 820) {
				c.getDH().sendDialogues(821, c.npcType);
				break;
			} else if (c.dialogueAction == 822) {
				c.getDH().sendDialogues(823, c.npcType);
				break;
			} else if (c.dialogueAction == 8887 && c.getDonatedTotalAmount() >= 10) {
				c.getPA().startTeleport(2520, 3860, 0, TeleportType.MODERN);
				c.getPA().closeAllWindows();
			} else if (c.dialogueAction == 8887 && (c.getDonatedTotalAmount() < 10)) {
				c.getPA().closeAllWindows();
				c.sendMessage("You need to be a Donator or higher to enter this place");
				break;
			} else if (c.dialogueAction == 2999) {
				c.getBarrows().checkCoffins();
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 303) {
				if (c.isIronMan()) {
					c.getShops().openShop(102);
				} else if (!(c.isIronMan())) {
					c.getShops().openShop(25);
				}
				break;
			} else if (c.dialogueAction == 1211) {
				c.getPacketSender().showInterface(42000);
				c.sendMessage("Purchase Titles for 500 Loyalty Points each.");
				if (c.getItems().playerHasItem(10835, 10)) {
					Titles.tenBulgingBags(c);
				} else if (c.getItems().playerHasItem(15098)) {
					Titles.diceBag(c);
				}
				break;
			} else if (c.tourStep > 0 && c.tourStep < 100) {
				c.tourStep = 777; // stops the tour
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 2256) {
				c.expLock = true;
				c.sendMessage("Your experience is now locked. You will not gain experience.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 7411) {
				c.getPA().movePlayer(2461, 5317, 0);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 12230) {
				if (c.getItems().playerHasItem(20558, 3)) {
					if (c.getPA().freeSlots() > 0) {
						c.getItems().deleteItemInOneSlot(20558, 1);
						c.getItems().deleteItemInOneSlot(20558, 1);
						c.getItems().deleteItemInOneSlot(20558, 1);
						c.getItems().addItem(c.getPA().nomadReward(), 1);
						c.getDH().sendNpcChat2("Thank you! Oh by the way i have some armour that i",
								"would like to give you for all of your help.", 12228, "Champion");
						c.getPA().closeAllWindows();
					} else {
						c.sendMessage("You need to have at least 1 free slot in your inventory.");
						c.getPA().closeAllWindows();
					}
				}
				break;
			} else if (c.dialogueAction == 19968) {
				if (c.getItems().playerHasItem(19967, 1)) {
					c.getPA().spellTeleport(LocationConstants.NOMAD.getX(), LocationConstants.NOMAD.getY(), 0);
				}
				c.getItems().deleteItemInOneSlot(19967, 1);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 8003) {
				if (c.getItems().playerHasItem(2996, 1)) {
					c.getPA().movePlayer(2901, 5204, 0);
					c.getItems().deleteItemInOneSlot(2996, 1);
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You dont have a vote ticket.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 6481) {
				c.getPA().movePlayer(3222, 3477, 0);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6494) {
				if (c.difficulty == PlayerDifficulty.NORMAL) {
					c.getPlayerDifficulty().prestigeOne(c);
					c.getPA().closeAllWindows();
					c.getItems().bankEquipment();
					c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
							ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
					c.setUpdateEquipment(true, 3);
					c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
					c.sendMessage("Your equipments have been banked.");
				}
				break;
			} else if (c.dialogueAction == 6474) {
				if (c.difficulty == PlayerDifficulty.PRESTIGE_ONE) {
					c.getPlayerDifficulty().prestigeTwo(c);
					c.getPA().closeAllWindows();
					c.getItems().bankEquipment();
					c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
							ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
					c.setUpdateEquipment(true, 3);
					c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
					c.sendMessage("Your equipments have been banked.");
				}
				break;
			} else if (c.dialogueAction == 6464) {
				if (c.difficulty == PlayerDifficulty.PRESTIGE_TWO) {
					c.getPlayerDifficulty().prestigeThree(c);
					c.getPA().closeAllWindows();
					c.getItems().bankEquipment();
					c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
							ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
					c.setUpdateEquipment(true, 3);
					c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
					c.sendMessage("Your equipments have been banked.");
				}
				break;
			} else if (c.dialogueAction == 6465) {
				if (c.isIronMan()) {
					if (c.getItems().playerHasItem(995, 100000000)) {
						c.getPlayerDifficulty().ironmanUpgrade(c);
						c.getPA().closeAllWindows();
					} else {
						c.sendMessage("You dont have enough money to upgrade to super ironman.");
						c.getPA().closeAllWindows();
					}
				}
				break;
			} else if (c.dialogueAction == 1400) {
				if (c.homeTeleActions == 1) {// varrock
					c.getPA().startTeleport(3210, 3424, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 0) {// edgeville
					c.getPA().startTeleport(3093, 3492, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 2) {// lumbridge
					c.getPA().startTeleport(3222, 3218, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 3) {// falador
					c.getPA().startTeleport(2965, 3378, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 4) {// camelot
					c.getPA().startTeleport(2757, 3477, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 5) {// neitiznot
					c.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 6) {// ardougne
					c.getPA().startTeleport(2662, 3305, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 7) {// shilo village
					c.getPA().startTeleport(2852, 2960, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 8) {// yanille
					c.getPA().startTeleport(2605, 3093, 0, TeleportType.MODERN);
				} else if (c.homeTeleActions == 9) {// neitiznot
					c.getPA().startTeleport(2096, 3913, 0, TeleportType.MODERN);
					c.getPA().closeAllWindows();
					break;
				}
			} else if (c.dialogueAction == 9451) {
				c.sendTourGuide();
				break;
			} else if (c.dialogueAction == 4155) {
				if (c.getPendingRequest().getSender() == null) {
					c.getPA().closeAllWindows();
					return;
				}
				Slayer.joinGroup(c, c.getPendingRequest().getSender());
				// Slayer.invitePlayer(c, c);
				// c.sendMessage("You are now in a slayer duo group with " +
				// c.getPendingRequest().getSender().getName() + ".");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 123 && c.tourStep == 0) {
				c.getPA().startTour();
				c.dialogueAction = 0;
				break;
			} else if (c.dialogueAction == 1225) {
				c.playerTitle = "";
				c.titleColor = 0;
				c.sendMessage("Your title will be removed when you logout.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 7852) {
				c.getPA().startTeleport(2474, 3438, 0, TeleportType.MODERN);
				c.dialogueAction = 0;
				break;
			} else if (c.teleAction == 31) {
				c.getPA().spellTeleport(LocationConstants.FARMING_CATHERBY.getX(),
						LocationConstants.FARMING_CATHERBY.getY(), 0);
			} else if (c.teleAction == 32) {
				c.getPA().spellTeleport(LocationConstants.RUNECRAFTING_EDGE.getX(),
						LocationConstants.RUNECRAFTING_EDGE.getY(), 0);
			} else if (c.teleAction == 36) {
				c.getPA().spellTeleport(LocationConstants.EDGE_PVP.getX(), LocationConstants.EDGE_PVP.getY(), 0);
				c.getDH().sendDialogues(4500, 945);
			} else if (c.dialogueAction == 4710) {
				if (PlayerHandler.getPlayerCount() < 30) {
					c.getPA().closeAllWindows();
					c.sendMessage("<col=ff0000>Revenant dungeon is closed at the moment.");
					return;
				} else {
					c.getPA().movePlayer(3222, 9618, 0);
					c.sendMessage("The old man shows you the tunnel behind the Shelves.");
					c.getPA().closeAllWindows();
				}
			} else if (c.dialogueAction == 1113) {
				if (c.getItems().playerHasItem(995, 1)) {
					c.getItems().deleteItemInOneSlot(995, 1);
					c.getDH().sendDialogues(1115, c.npcType);
				} else {
					c.getDH().sendDialogues(1114, c.npcType);
				}
				break;
			} else if (c.dialogueAction == 965) {
				c.getDH().sendDialogues(966, c.npcType);
			} else if (c.dialogueAction == 6202) {
				c.getDH().sendDialogues(6203, c.npcType);
				return;
			} else if (c.dialogueAction == 6675) { // sliding down to nex prison
				if (NexEvent.isStarting()) {
					c.sendMessage("You can't enter the Nex prison as the event is starting.");
					c.getPA().closeAllWindows();
					return;
				}

				c.getPA().movePlayer(2911, 5204, c.getZ());
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6676) {
				if (c.getItems().playerHasEquipped(13738) || c.getItems().playerHasEquipped(13740)
						|| c.getItems().playerHasEquipped(13742) || c.getItems().playerHasEquipped(13744)) {
					c.sendMessage("You cannot bring in any Spirit shields.");
					c.getPA().closeAllWindows();
				} else if (c.getItems().playerHasItem(13738) || c.getItems().playerHasItem(13740)
						|| c.getItems().playerHasItem(13742) || c.getItems().playerHasItem(13744)) {
					c.sendMessage("You cannot bring in any Spirit shields.");
					c.getPA().closeAllWindows();
				} else if (c.getItems().playerHasItem(175) || c.getItems().playerHasItem(177)
						|| c.getItems().playerHasItem(179) || c.getItems().playerHasItem(2446)) {
					c.sendMessage("You cannot bring antipoison with you.");
					c.getPA().closeAllWindows();
				} else {
					c.getPA().movePlayer(3379, 9820, 0);
					c.getPA().closeAllWindows();
					c.getSummoning().dismissFamiliar(false);
					c.sendMessage("You climb over the rocks to Nomad.");
				}
				break;
			} else if (c.dialogueAction == 587) {
				c.getDH().sendDialogues(588, c.npcType);
				return;
			} else if (c.dialogueAction == 1211) {
				// c.getDH().sendDialogues(1212, c.npcType);
				c.sendMessage("Titles are being reworked. Thank you for your patience.");
				return;
			} else if (c.dialogueAction == 6209) {
				if (c.getItems().playerHasItem(20120, 1)) {
					c.getItems().deleteItemInOneSlot(20120, 1); // use this
																// when
																// nex is
																// completed.
					c.getPA().movePlayer(2856, 5222, 0);
					c.sendMessage("The Knight unlocked the door for you.");
					c.getPA().closeAllWindows();
				} else {
					c.getDH().sendDialogues(6210, c.npcType);
				}
				break;
			} else if (c.dialogueAction == 3114) { // junk machine/slot
													// machine
				JunkMachine.cashJunkItem(c, c.getTempVar().getInt("junkmachine"));
				c.getTempVar().remove("junkmachine");
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 777) { // Open donator chest for
													// 10
													// donpoints
				if (c.getDonPoints() >= 10) {
					c.setDonPoints(c.getDonPoints() - 10, true);
					c.giveDonatorChestReward();
					c.sendMessage("You have received your item!");
				} else {
					c.sendMessage("You don't have enough points");
				}
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 778) { // start fun pk tournament
													// for
													// donators for about 7
													// points
				if (FunPkTournament.isTournamentOpen()) {
					c.sendMessage("The tournament is currently running.");
					c.getPA().closeAllWindows();
					return;
				}
				if (c.getDonPoints() >= FunPkTournament.DONATORHOSTPRICE) {
					c.setDonPoints(c.getDonPoints() - FunPkTournament.DONATORHOSTPRICE, true);
					FunPkTournament.donatorOpenTournament(c);
				} else {
					c.sendMessage("You don't have enough points");
				}
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 669) {
				if (c.getItems().playerHasItem(20486, 1) && (c.getItems().playerHasItem(20477, 1) && (c.getItems()
						.playerHasItem(20478, 1)
						&& (c.getItems().playerHasItem(20479, 1) && (c.getItems().playerHasItem(20471, 1)
								&& (c.getItems().playerHasItem(20468, 1) && (c.getItems().playerHasItem(20469, 1)
										&& (c.getItems().playerHasItem(995, 50000))))))))) {
					c.getItems().deleteItem2(20486, 1);
					c.getItems().deleteItem2(20477, 1);
					c.getItems().deleteItem2(20478, 1);
					c.getItems().deleteItem2(20479, 1);
					c.getItems().deleteItem2(20471, 1);
					c.getItems().deleteItem2(20468, 1);
					c.getItems().deleteItem2(20469, 1);
					c.getItems().deleteItem2(995, 50000);
					c.getItems().addItem(6, 1);
					c.getItems().addItem(8, 1);
					c.getItems().addItem(10, 1);
					c.getItems().addItem(12, 1);
					c.sendMessage("Boot succesfully fixed your cannon!");
					c.getPA().closeAllWindows();
				} else {
					c.sendMessage("You either don't have all the parts or 50.000 Coins.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction == 13720) {
				c.getDH().sendDialogues(13721, c.npcType);
				break;
			} else if (c.dialogueAction == 13721) {
				if (!DBConfig.MINI_GAMES) {
					c.sendMessage("<col=800000>The minigames have been temporarily disabled by the admins.");
					c.getPA().closeAllWindows();
					return;
				}

				if (!c.getSpamTick().checkThenStart(2)) {
					return;
				}

				// if (c.getClan() != null &&
				// c.getClan().getRank(c.getName())
				// != -1 && c.getClan().getClanPoints() < 5346332) {
				if (c.getItems().playerHasItem(6570, 1)) {
					c.getItems().deleteItem2(6570, 1);
					KilnFightCaves.enterCaves(c);
					c.sendMessage("You unlocked the kiln.");
					c.getDH().sendDialogues(13723, c.npcType);
				} else {
					c.sendMessage("You don't have a firecape to sacrifice.");
					c.getPA().closeAllWindows();
				}
				/*
				 * } else { KilnFightCaves.enterCaves(c);
				 * c.sendMessage("You unlocked the kiln.");
				 * c.getDH().sendDialogues(13723, c.npcType); }
				 */
				break;
			} else if (c.dialogueAction == 13725) {
				if (c.getItems().playerHasItem(23639, 1)) {
					c.getItems().deleteItem2(23639, 1);
					// c.getPA().closeAllWindows();
					if (Misc.random(59) == 1) {
						c.getItems().addItem(21512, 1);
						c.sendMessage("You sacrifice your Kiln cape and received a jad pet!");
					} else {
						c.sendMessage("You sacrifice your Kiln cape but did not receive a jad pet.");
					}
				} else {
					c.sendMessage("You don't have a Kiln cape to sacrifice.");
					c.getPA().closeAllWindows();
				}
				break;
			} else if (c.dialogueAction > 0 && c.newPlayerAct == 1) {
				c.newPlayerAct = 0;
				c.getPA().startTeleport(LocationConstants.EDGEVILLE_X, LocationConstants.EDGEVILLE_Y, 0, TeleportType.MODERN);
				c.getPA().closeAllWindows();
			} else if (c.dialogueAction == 508) {
				c.getDH().sendDialogues(1030, 925);
				return;
			} else if (c.dialogueAction == 1) {
				c.getDH().sendDialogues(38, -1);
			}
			break;

		case SECOND_OF_TWO: // option two for sendoption2

			switch (c.dialogueAction) {
			
			case 8591:
				c.getShops().openShop(53);
				c.sendMessage("You currently have " + c.getZeals() + " Zeals.");
				c.dialogueAction = 0;
				break;

			case 5029:
				c.getShops().openShop(52);
				c.sendMessage(String.format("You currently have %d honour points.",
						c.getBarbarianAssault().getHonorPoints()));
				c.nextChat = 0;
				break;

			case 5032:
				c.getPA().closeAllWindows();
				break;

			case 28: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				if (activeRoles.length < 2) {
					return;
				}

				Player toKick = c.getBarbarianAssault().getGroup().get(activeRoles[1]);

				c.getBarbarianAssault().kickPlayer(toKick);
			}
				break;

			case 31:
				c.getPA().closeAllWindows();
				break;

			}

			if (c.dialogueAction == 24) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				partyLeader.sendMessage(c.getNameSmartUp() + " declined.");
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 25) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 2) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[1]);

				c.getBarbarianAssault().joinGroup(partyLeader);
				break;
			}

			if (c.dialogueAction == 8900) {
				c.getDH().sendNpcChat1("Please use the set on me that you would like to open.", c.talkingNpc,
						"Armour set Exchange");
				break;
			}

			if (c.dialogueAction == 22) {
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 23) {
				c.getBarbarianAssault().getPartyLeader().sendMessage(c.getNameSmartUp() + " declined.");
				c.sendMessage("You declined the invite.");
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 1981) {
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 1980) {
				if (c.isIronMan()) {
					c.getDH().sendDialogues(1981, c.npcType);
				} else {
					c.getDH().sendNpcChat1("You are not a Ironman", c.talkingNpc, "Guildmaster");
					c.getPA().closeAllWindows();
				}
				break;
			}
			if (c.dialogueAction == 299 || c.dialogueAction == 80940) {
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 80970) {
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 1526) {
				c.getShops().openShop(50);
				break;
			}
			if (c.dialogueAction == 6895) {
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 20798) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 368) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 464) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 466) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 7942) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 2798) {
				c.demoting = "";
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 8929) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 820) {
				c.getDH().sendOption2("Buy spin tickets for donation points", "Coming soon");
				c.dialogueAction = 822;
				break;
			} else if (c.dialogueAction == 1500) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 8887 && c.getDonatedTotalAmount() >= 250) {
				c.getPA().startTeleport(2603, 3876, 0, TeleportType.MODERN);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 8887 && (c.getDonatedTotalAmount() < 250)) {
				c.getPA().closeAllWindows();
				c.sendMessage("You need to be a Supreme Donator or higher to enter this place");
				break;
			} else if (c.dialogueAction == 303) {
				c.getShops().openShop(46);
				break;
			}  else if (c.dialogueAction == 940) {
				c.getDH().sendDialogues(941, c.npcType);
				break;
			} else if (c.dialogueAction == 944) {
				c.getDH().sendDialogues(939, c.npcType);
				break;
			} else if (c.dialogueAction == 10831) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 10832) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 10833) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 10834) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 10835) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.tourStep > 0 && c.tourStep < 100) {
				c.tourStep--; // setting this back on tour guide stuff.
				break;
			} else if (c.dialogueAction == 2256) {
				c.expLock = false;
				c.sendMessage("Your experience is now unlocked. You will gain experience.");
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 7411) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 12230) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 19968) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 8003) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6481) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6494) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6474) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6464) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 6465) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 1400) {
				c.getPA().startTeleport(3208, 3429, 0, TeleportType.MODERN);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 9451) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 4155) {
				Slayer.decline(c);
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 123) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 1225) {
				c.getPA().closeAllWindows();
				break;
			} else if (c.dialogueAction == 7852) {
				c.getPA().startTeleport(LocationConstants.AGILITY_BARB.getX(), LocationConstants.AGILITY_BARB.getY(), 0,
						TeleportType.MODERN);
				break;
			} else if (c.teleAction == 36) {
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.teleAction = 25;
			} else if (c.teleAction == 35) {
				c.getDH().sendOption5("Runecrafting", "Thieving", "Agility", "Slayer", "Next Page");
				c.teleAction = 26;
			} else if (c.teleAction == 32) {
				c.getDH().sendOption5("Runecrafting", "Thieving", "Agility", "Slayer", "Next Page");
				c.teleAction = 26;
			} else if (c.teleAction == 31) {
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.teleAction = 25;
			} else if (c.dialogueAction == 1113) {
				c.getDH().sendDialogues(1114, c.npcType);
				return;
			} else if (c.dialogueAction == 6202) {
				c.getDH().sendDialogues(6207, c.npcType);
				return;
			} else if (c.dialogueAction == 6209) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 1211) {
				c.getShops().openShop(36);
				return;
			} else if (c.dialogueAction == 587) {
				c.getShops().openShop(37);
				return;
			} else if (c.dialogueAction == 965) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 4250) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 3114) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 669) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 13720) {
				c.getDH().sendDialogues(13724, c.npcType);
				return;
			} else if (c.dialogueAction == 13721) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 13725) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 6675) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 6676) {
				c.getPA().closeAllWindows();
				return;
			} else if (c.dialogueAction == 777 || c.dialogueAction == 778) { // No
																				// Thanks.
																				// Donator
																				// chest
				c.getPA().closeAllWindows();
				return;
			} else if (c.newPlayerAct == 1) {
				c.newPlayerAct = 0;
				c.getPA().closeAllWindows();
			}

			if (c.dialogueAction == 27) {
				c.getPA().closeAllWindows();
			}
			break;

		case FIRST_OF_THREE: // option 1 out of 3

			switch (c.dialogueAction) {
			
			case 27255:
				c.getPA().spellTeleport(LocationConstants.VARROCK_X, LocationConstants.VARROCK_Y, 100);
				break;

			case 25: {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 1) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[0]);

				c.getBarbarianAssault().joinGroup(partyLeader);
			}
				break;

			// kick player barb assault
			case 27: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				int length = activeRoles.length;

				switch (length) {

				// party of 2 excluding the party leader so 1
				case 1: {
					Player result = c.getBarbarianAssault().getGroup().get(activeRoles[0]);

					c.getDH().sendOption2(result.getNameSmartUp() + " (" + activeRoles[0].getName() + ")", "");
					c.dialogueAction = 30;
				}
					break;

				// party of 3 excluding the party leader so 2
				case 2: {
					Player result1 = c.getBarbarianAssault().getGroup().get(activeRoles[0]);

					Player result2 = c.getBarbarianAssault().getGroup().get(activeRoles[1]);

					c.getDH().sendOption2(result1.getNameSmartUp() + " (" + activeRoles[0].getName() + ")",
							result2.getNameSmartUp() + " (" + activeRoles[1].getName() + ")");
					c.dialogueAction = 28;
				}
					break;

				// party of 4 excluding the party leader so 3
				case 3: {

					Player result1 = c.getBarbarianAssault().getGroup().get(activeRoles[0]);

					Player result2 = c.getBarbarianAssault().getGroup().get(activeRoles[1]);

					Player result3 = c.getBarbarianAssault().getGroup().get(activeRoles[2]);

					c.getDH().sendOption3(result1.getNameSmartUp() + " (" + activeRoles[0].getName() + ")",
							result2.getNameSmartUp() + " (" + activeRoles[1].getName() + ")",
							result3.getNameSmartUp() + " (" + activeRoles[2].getName() + ")");
					c.dialogueAction = 29;
				}

					break;

				default:
					break;

				}
			}
				break;

			case 29: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				if (activeRoles.length < 3) {
					return;
				}

				Player toKick = c.getBarbarianAssault().getGroup().get(activeRoles[0]);

				c.getBarbarianAssault().kickPlayer(toKick);
			}
				break;

			case 2993:
				c.getItems().addItem(299, 10);
				break;

			case 2994:
				c.getDH().sendDialogues(2998, c.npcType);
				break;

			case 80880:
				c.getDH().sendDialogues(80890, c.npcType);
				break;

			case 6893:
				c.getShops().openShop(48);
				break;

			case 1513:
				c.getDH().sendDialogues(1514, c.npcType);
				break;

			case 535:
				c.getDH().sendDialogues(536, c.npcType);
				break;

			case 824:
				if (c.getDonPoints() >= 1) {
					if (c.getItems().freeSlots() >= 1) {
						c.setDonPoints(c.getDonPoints() - 1, true);
						c.getItems().addItem(13663, 1);
					} else {
						c.sendMessage("You must have at least 1 inventory spaces to buy this item.");
					}
				} else {
					c.sendMessage("You don't have enough points");
				}
				c.getPA().closeAllWindows();
				break;
			case 8726:
				c.getDH().sendNpcChat2("You have " + c.KC + " kills and " + c.DC + " deaths. you're currently",
						"on a " + c.killStreak + " killstreak and you have " + c.pkp + " PK Points", c.talkingNpc,
						"Mandrith");
				break;

			case 586:
				c.getShops().openShop(33);
				c.dialogueAction = 0;
				break;

			case 2245:
				c.getPA().startTeleport(2110, 3915, 0, TeleportType.MODERN);
				c.sendMessage("High Priest teleported you to <col=ff0000>Lunar Island</col>.");
				c.getPA().closeAllWindows();
				break;

			case 508:
				c.getDH().sendDialogues(1030, 925);
				break;

			case 502:
				c.getDH().sendDialogues(1030, 925);
				break;

//			case 251:
//				c.getPA().openUpBank();
//				c.dialogueAction = 0;
//				break;

			case 1213:
				c.getDH().sendOption4("Title (<shad=12632256>The</shad>)",
						"Title (<shad=12632256>The Idiot</shad>)", "Title (<shad=12632256>The Awesome</shad>)",
						"Next Page");
				c.dialogueAction = 1215;
				break;

			}

			switch (c.teleAction) {

			case 43:
				c.getPA().spellTeleport(LocationConstants.CLAN_WARS.getX(), LocationConstants.CLAN_WARS.getY(), 0);
				break;

			case 28:
				c.getPA().spellTeleport(LocationConstants.WOODCUTTING_CAMELOT.getX(),
						LocationConstants.WOODCUTTING_CAMELOT.getY(), 0);
				break;

			case 30:
				c.getPA().spellTeleport(LocationConstants.MINING_NEITIZNOT.getX(),
						LocationConstants.MINING_NEITIZNOT.getY(), 0);
				break;

			}
			break;

		case SECOND_OF_THREE:

			switch (c.dialogueAction) {
			
			case 27255:
				c.getPA().spellTeleport(LocationConstants.FALADOR_X, LocationConstants.FALADOR_Y, 100);
				break;
			
			case 80880:
				c.getPA().closeAllWindows();
				break;

			case 25: {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				Role[] neededRoles = partyLeader.getBarbarianAssault().getInactiveRoles();

				if (neededRoles.length < 2) {
					break;
				}

				c.getBarbarianAssault().assignRole(neededRoles[1]);

				c.getBarbarianAssault().joinGroup(partyLeader);
			}
				break;

			case 27:
				c.getBarbarianAssault().leaveGroup();
				break;

			case 29: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				if (activeRoles.length < 3) {
					return;
				}

				Player toKick = c.getBarbarianAssault().getGroup().get(activeRoles[1]);

				c.getBarbarianAssault().kickPlayer(toKick);
			}
				break;

			case 2993:
				c.getItems().addItem(299, 100);
				break;

			case 2994:
				c.getDH().sendDialogues(2993, c.npcType);
				break;

			case 6893:
				if (c.getIncubatorEggId() > 0) {
					if (c.getEggTimer() < 288000) {
						c.getDH().sendNpcChat2("The egg takes 2 days of active playtime",
								"to hatch, you currently have " + c.getEggTimer() / 6000 + " hours playtime.", c.npcType, "Pet Shop Owner");
					} else if (c.getEggTimer() >= 288000) {
						Egg.incubatorEnd(c);
					}
				} else {
					c.getDH().sendNpcChat1("You don't have an egg in the incubator.", c.npcType, "Pet Shop Owner");
				}
				break;

			case 1513:
				c.getDH().sendDialogues(1515, c.npcType);
				break;

			case 535:
				c.getDH().sendDialogues(537, c.npcType);
				break;

			case 824:
				if (c.getDonPoints() >= 10) {
					if (c.getItems().freeSlots() > 9) {
						c.setDonPoints(c.getDonPoints() - 10, true);
						c.getItems().addItem(13663, 10);
					} else {
						c.sendMessage("You must have at least 10 inventory spaces to buy this item.");
					}
				} else {
					c.sendMessage("You don't have enough points");
				}
				c.getPA().closeAllWindows();
				break;
			case 8726:
				c.getShops().openShop(8, ShopType.SCROLL);
				c.dialogueAction = 0;
				break;

			case 586:
				c.getShops().openShop(34);
				c.dialogueAction = 0;
				break;

			case 1213:
				c.getDH().sendOption2("Yes i would like to remove my title.",
						"No thank you i would like to keep my current title.");
				c.dialogueAction = 1225;
				break;

			case 2245:
				c.getPA().startTeleport(3230, 2915, 0, TeleportType.MODERN);
				c.sendMessage("High Priest teleported you to <col=ff0000>Desert Pyramid</col>.");
				c.getPA().closeAllWindows();
				break;

			case 508:
				c.getDH().sendDialogues(1027, 925);
				break;

			case 502:
				c.getDH().sendDialogues(1027, 925);
				break;

//			case 251:
//				c.getBankPin().bankPinSettings();
//				c.openBankPinInterface = true;
//				c.dialogueAction = 0;
//				break;

			}

			switch (c.teleAction) {

			case 43:
				c.getPA().spellTeleport(LocationConstants.WHITE_PORTAL.getX(), LocationConstants.WHITE_PORTAL.getY(),
						0);
				break;

			case 28:
				c.getPA().spellTeleport(LocationConstants.WOODCUTTING_DRAYNOR.getX(),
						LocationConstants.WOODCUTTING_DRAYNOR.getY(), 0);
				break;

			case 30:
				c.getPA().spellTeleport(LocationConstants.MINING_FALADOR.getX(),
						LocationConstants.MINING_FALADOR.getY(), 0);
				break;

			}
			break;

		case THIRD_OF_THREE:

			switch (c.dialogueAction) {
			
			case 27255:
				c.getPA().spellTeleport(2996, 3271, 0);
				break;
			
			case 80880:
				c.getDH().sendNpcChat2("Just head to Rimmington and Port Sarim and", "trick-or-treat them.", c.npcType, "Maggie");
				c.dialogueAction = 80880;
				break;

			case 27:
				c.getPA().closeAllWindows();
				break;

			case 29: {
				Role[] activeRoles = c.getBarbarianAssault().getActiveRolesExcludingSelf();

				if (activeRoles.length < 3) {
					return;
				}

				Player toKick = c.getBarbarianAssault().getGroup().get(activeRoles[2]);

				c.getBarbarianAssault().kickPlayer(toKick);
			}
				break;

			}

			if (c.dialogueAction == 25) {
				Player partyLeader = c.getBarbarianAssault().getPartyLeader();

				if (partyLeader == null) {
					return;
				}

				partyLeader.sendMessage(c.getNameSmartUp() + " declined.");
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 2993) {
				c.getItems().addItem(299, 1000);
				break;
			}
			if (c.dialogueAction == 2994) {
				c.getPA().closeAllWindows();
				break;
			}

			if (c.dialogueAction == 6893) {
				c.getDH().sendDialogues(6894, c.npcType);
				break;
			}

			if (c.dialogueAction == 1513) {
				c.getDH().sendDialogues(1516, c.npcType);
				break;
			}

			if (c.dialogueAction == 535) {
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 824) {
				c.getPA().closeAllWindows();
				break;
			}
			if (c.dialogueAction == 8726) {
				ArtifactData.sellArtifacts(c);
				break;
			}
			if (c.dialogueAction == 586) {
				c.getShops().openShop(35);
				c.dialogueAction = 0;
				break;
			}
			if (c.teleAction == 28) { // (c.teleAction == 28 && c.teleAction
										// <=
										// 30)
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.teleAction = 25;
			}
			if (c.teleAction == 30) {
				c.getDH().sendOption5("Woodcutting", "Fishing", "Mining", "Farming", "Next Page");
				c.teleAction = 25;
			}
			if (c.dialogueAction == 1213) {
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 2245) {
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 508) {
				c.nextChat = 0;
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 502) {
				c.nextChat = 0;
				c.getPA().closeAllWindows();
			}
			if (c.dialogueAction == 251) {
				c.getDH().sendDialogues(1015, 494);
				break;
			}
			break;

		/** 474 Specials **/
		/* Daggers & Swords */
		case 29138:
			/* Scimitar */
		case 29163:
			/* Mace */
		case 29199:
			/* Battleaxe & Hatchets */
		case 29074:
			/* Halberd $ Staff of Light */
		case 33033:
			/* Spear */
		case 29238:
			/* Godswords & 2h Swords */
		case 30007:
			/* Claws */
		case 30108:
			/* Whip */
		case 48034:
			/* Warhammer & Mauls */
		case 29049:
			/* Pickaxe */
		case 30043:
			/* Bows */
		case 29124:
			/* Throwing Axe & Javelins */
		case 29213:
			/*
			 * case 30108: c.specBarId = 7812; c.usingSpecial = !c.usingSpecial;
			 * c.getItems().updateSpecialBar(); break;
			 */
		case 29188:
		case 29038:
		case 29063:
		case 48023:
			/*
			 * case 29138: c.specBarId = 7586; c.usingSpecial = !c.usingSpecial;
			 * c.getItems().updateSpecialBar(); break;
			 */
		case 23217: // spec bar on equipment such as staff of light
		case 29113:
			// new spec attack handler
			SpecButton.clickButton(c);
			break;

		/*
		 * case 29238: c.specBarId = 7686; c.usingSpecial = !c.usingSpecial;
		 * c.getItems().updateSpecialBar(); break;
		 */

		/** 474 End of Specials **/

		/** Dueling **/
		case 26065: // no forfeit
		case 26040:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.SAME_WEAPONS);
			break;

		case 26066: // no movement
		case 26048:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_MOVEMENT);
			break;

		case 26069: // no range
		case 26042:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_RANGE);
			break;

		case 26070: // no melee
		case 26043:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_MELEE);
			break;

		case 26071: // no mage
		case 26041:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_MAGIC);
			break;

		case 26072: // no drinks
		case 26045:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_DRINKS);
			break;

		case 26073: // no food
		case 26046:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_FOOD);
			break;

		case 26074: // no prayer
		case 26047:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_PRAYER);
			break;

		case 26076: // obstacles
		case 26075:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.OBSTACLES);
			break;

		case 2158: // fun weapons
		case 2157:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.FUN_WEAPONS);
			break;

		case 30136: // sp attack
		case 30137:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_SPECIAL_ATK);
			break;

		case 53245: // no helm
			c.duelSlot = 0;
			c.getTradeAndDuel().selectRule(DuelRules.NO_HAT);
			break;

		case 53246: // no cape
			c.duelSlot = 1;
			c.getTradeAndDuel().selectRule(DuelRules.NO_CAPE);
			break;

		case 53247: // no ammy
			c.duelSlot = 2;
			c.getTradeAndDuel().selectRule(DuelRules.NO_AMULET);
			break;

		case 53249: // no weapon.
			c.duelSlot = 3;
			c.getTradeAndDuel().selectRule(DuelRules.NO_WEAPON);
			break;

		case 53250: // no body
			c.duelSlot = 4;
			c.getTradeAndDuel().selectRule(DuelRules.NO_BODY);
			break;

		case 53251: // no shield
			c.duelSlot = 5;
			c.getTradeAndDuel().selectRule(DuelRules.NO_SHIELD);
			break;

		case 53252: // no legs
			c.duelSlot = 7;
			c.getTradeAndDuel().selectRule(DuelRules.NO_LEGS);
			break;

		case 53255: // no gloves
			c.duelSlot = 9;
			c.getTradeAndDuel().selectRule(DuelRules.NO_GLOVES);
			break;

		case 53254: // no boots
			c.duelSlot = 10;
			c.getTradeAndDuel().selectRule(DuelRules.NO_BOOTS);
			break;

		case 53253: // no rings
			c.duelSlot = 12;
			c.getTradeAndDuel().selectRule(DuelRules.NO_RINGS);
			break;

		case 53248: // no arrows
			c.duelSlot = 13;
			c.getTradeAndDuel().selectRule(DuelRules.NO_ARROWS);
			break;

		case 26018: // first duel screen accept button
			if (c.duelStatus == 5) {
				// c.sendMessage("You're funny sir.");
				return;
			}
			if (c.duelStatus < 1 || c.duelStatus > 2) {
				c.getTradeAndDuel().declineDuel();
				return;
			}
			if (c.inDuelArena()) {
				// Client o = (Client) PlayerHandler.players[c.duelingWith];
				if (c.getDuelingClient() == null || c.getDuelingClient().getTradeAndDuel() == null) {
					c.getTradeAndDuel().declineDuel();
					// o.getTradeAndDuel().declineDuel();
					return;
				}

				if (c.duelRule[DuelRules.NO_RANGE] && c.duelRule[DuelRules.NO_MELEE]
						&& c.duelRule[DuelRules.NO_MAGIC]) {
					c.sendMessage("You won't be able to attack the player with the rules you have set.");
					break;
				}
				if (c.getDuelRuleTimer() > 0) {
					c.sendMessage("Please wait 5 seconds after changing rules to accept.");
					return;
				}
				int slotsNeeded = 0;
				for (int i = 11; i < c.duelRule.length; i++) {
					if (c.duelRule[i]) {
						slotsNeeded++;
					}
				}
				if (c.getItems().freeSlots() < slotsNeeded) {
					c.sendMessage("You don't have enough inventory space to start a duel with these rules.");
					break;
				}
				// if (c.duelStatus == 1) {
				c.duelStatus = 2;
				if (c.duelStatus == 2) {
					c.getPacketSender().sendFrame126("Waiting for other player...", 6684);
					c.getDuelingClient().getPacketSender().sendFrame126("Other player has accepted.", 6684);
				}
				if (c.getDuelingClient().duelStatus == 2) {
					c.getDuelingClient().getPacketSender().sendFrame126("Waiting for other player...", 6684);
					c.getPacketSender().sendFrame126("Other player has accepted.", 6684);
				}
				// }

				if (c.getDuelingClient() == null || c.getDuelingClient().getTradeAndDuel() == null) {
					c.getTradeAndDuel().declineDuel();
					// o.getTradeAndDuel().declineDuel();
					return;
				}

				if (c.duelStatus == 2 && c.getDuelingClient().duelStatus == 2) { // null
																					// pointer
																					// here?
					if (c.getDuelingClient() == null) {
						c.getTradeAndDuel().declineDuel();
						return;
					}
					c.canOffer = false;
					c.getDuelingClient().canOffer = false;
					c.duelStatus = 3;
					c.getDuelingClient().duelStatus = 3;
					c.getTradeAndDuel().confirmDuel();
					if (c.getDuelingClient() == null || c.getDuelingClient().disconnected) {
						c.getTradeAndDuel().declineDuel();
						return;
					}
					c.getDuelingClient().getTradeAndDuel().confirmDuel();
				}
			} else {
				// Client o = (Client) PlayerHandler.players[c.duelingWith];
				c.getTradeAndDuel().declineDuel();
				// if (o != null)
				// o.getTradeAndDuel().declineDuel();
				c.sendMessage("You can't stake out of Duel Arena.");
			}
			break;

		case 25120: // second screen accept button
			if (c.duelStatus == 5) {
				// c.sendMessage("You're funny sir.");
				return;
			}
			if (c.inDuelArena()) {
				if (c.duelStatus == 5) {
					break;
				}
				final Client o1 = c.getDuelingClient();// (Client)
														// PlayerHandler.players[c.duelingWith];
				if (o1 == null) {
					c.getTradeAndDuel().declineDuel();
					return;
				}
				int slotsNeeded = 0;
				for (int i = 11; i < c.duelRule.length; i++) {
					if (c.duelRule[i]) {
						slotsNeeded++;
					}
				}
				if (c.getItems().freeSlots() < slotsNeeded) {
					c.sendMessage("You don't have enough inventory space to start a duel with these rules.");
					break;
				}
				if (c.duelStatus == 3) {
					c.duelStatus = 4;
				}
				if (o1.duelStatus == 4 && c.duelStatus == 4) {
					c.getTradeAndDuel().setDuelLocations();
					c.getTradeAndDuel().startDuel();
					o1.getTradeAndDuel().startDuel();
					o1.duelCount = 4;
					c.duelCount = 4;
					c.duelDelay = System.currentTimeMillis();
					o1.duelDelay = System.currentTimeMillis();
				} else {
					c.getPacketSender().sendFrame126("Waiting for other player...", 6571);
					o1.getPacketSender().sendFrame126("Other player has accepted", 6571);
				}
			} else {
				// Client o = (Client) PlayerHandler.players[c.duelingWith];
				c.getTradeAndDuel().declineDuel();
				// o.getTradeAndDuel().declineDuel();
				c.sendMessage("You can't stake out of Duel Arena.");
			}
			break;

		case 152: // run orb
		case 250006: // setting tab run
			c.getMovement().toggleRun();
			break;

		case 153:
			c.getMovement().startResting();
			break;

		case 9154:
			if (c.inClanWars() || c.inClanWarsDead()) {
				c.sendMessage("You cannot log out here.");
				return;
			}
			c.logout();
			break;

		// home teleports
		case 4128:
		case 117048:
		case 75010:
		case 50056:
			if (c.duelStatus == 5) {
				c.sendMessage("You can't teleport in during a duel.");
			} else if (c.homeTeleActions == 1) {// varrock
				c.getPA().startTeleport(3210, 3424, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 0) {// edgeville
				c.getPA().startTeleport(LocationConstants.EDGEVILLE_X, LocationConstants.EDGEVILLE_Y, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 2) {// lumbridge
				c.getPA().startTeleport(3222, 3218, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 3) {// falador
				c.getPA().startTeleport(2965, 3378, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 4) {// camelot
				c.getPA().startTeleport(2757, 3477, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 5) {// neitiznot
				c.getPA().startTeleport(2339, 3802, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 6) {// ardougne
				c.getPA().startTeleport(2662, 3305, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 7) {// shilo village
				c.getPA().startTeleport(2852, 2960, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 8) {// yanille
				c.getPA().startTeleport(2605, 3093, 0, TeleportType.MODERN);
			} else if (c.homeTeleActions == 9) {// neitiznot
				c.getPA().startTeleport(2096, 3913, 0, TeleportType.MODERN);
			}
			break;

		case 4142:
		case 50059:
		case 117055:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 1);//Monster
			break;

		case 4145:
		case 50078:
		case 117056:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 4);//Minigame
			break;

		case 4148:
		case 50081:
		case 117057:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 3);//Boss
			break;

		case 4150:
		case 50071:
		case 117060:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 5);//Pking
			break;

		case 4174:
		case 4159:
		case 50065:
		case 117061:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 6);//Skilling
			break;
			/*
			 * c.getDH().sendOption5("Woodcutting", "Fishing", "Mining",
			 * "Farming", "Next Page"); c.teleAction = 25;
			 */
			//if (!WorldType.equalsType(WorldType.SPAWN)) {
			//	c.getDH().sendOption2("Wilderness skilling", "Skilling locations");
			//	c.teleAction = 36;
			//	break;
			//}

		case 4164:
		case 117064:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 0);//Cities
			break;

		case 4171:
		case 117065:
			c.getPacketSender().showInterface(25411);
			TeleportInterface.toggleLayers(c, 2);//Dungeon
			break;
			/*if (!WorldType.equalsType(WorldType.SPAWN)) {
				c.getDH().sendOption5("Taverly Dungeon", "Brimhaven Dungeon", "Kalphite Lair", "Asgarnian Ice Dungeon",
						"Next Page");
				c.teleAction = 21;
				break;
			}*/

		case 3014: // reap scythe
		case 23249: // bash staff of light
		case 9125: // Accurate
		case 6221: // range accurate
		case 48010: // flick (whip)
		case 21200: // spike (pickaxe)
		case 1080: // bash (staff)
		case 6168: // chop (axe)
		case 6236: // accurate (long bow)
		case 17102: // accurate (darts)
		case 8234: // stab (dagger)
		case 18077: // lunge (spear)
		case 22230: // punch
		case 30088:
		case 1177:
		case 14218:
		case 18103: // chop (2h)
		case 33020: // jab hally
			c.fightMode = 0;
//			if (c.getAutoCastSpell() != null) {
//				c.getPA().resetAutocast();
//			}
			//System.out.println("0buttonID:"+actionButtonId);
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemProjectInsanity.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			CombatTab.setButton(c, 0, c.getCombatTab());
			break;

		case 3017: // chop scythe
		case 23248: // pound staff of light
		case 1079: // pound (staff)
		case 21203: // impale (pickaxe)
		case 48009: // lash (whip)
		case 6171: // hack (axe)
		case 33019: // swipe (hally)
		case 8237: // lunge (dagger)
		case 18080: // swipe (spear)
		case 17101: // rapid (darts)
		case 6235: // rapid (long bow)
		case 22231: // unarmed
		case 22229: // kick
		case 9128: // slash Aggressive
		case 14221:
		case 30091:
		case 1176:
		case 18106: // slash (2h)
			c.fightMode = 1;
//			if (c.getAutoCastSpell() != null) {
//				c.getPA().resetAutocast();
//			}
			//System.out.println("1buttonID:"+actionButtonId);
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemProjectInsanity.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			CombatTab.setButton(c, 1, c.getCombatTab());
			break;

		case 3016: // jab scythe
		case 23247: // block staff of light
		case 1078: // focus - block (staff)
		case 48008: // deflect (whip)
		case 9127: // lunge Controlled
		case 6220: // range rapid
		case 21202: // smash (pickaxe)
		case 6170: // smash (axe)
		case 18079: // pound (spear)
		case 8236: // slash (dagger)
		case 22228: // unarmed
		case 18105: // smash (2h)
		case 33018: // fend (hally)
		case 17100: // longrange (darts)
		case 6234: // longrange (long bow)
		case 14220:
		case 30090:
		case 1175:
			c.fightMode = 2;
//			if (c.getAutoCastSpell() != null) {
//				c.getPA().resetAutocast();
//			}
			//System.out.println("2buttonID:"+actionButtonId);
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemProjectInsanity.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			CombatTab.setButton(c, 2, c.getCombatTab());
			break;

		case 3015: // block scythe
		case 9126: // block Defensive
		case 21201: // block (pickaxe)
		case 6219: // longrange
		case 6169: // block (axe)
		case 18078: // block (spear)
		case 8235: // block (dagger)
		case 18104: // block (2h)
		case 30089:
		case 14219:
			c.fightMode = 3;
//			if (c.getAutoCastSpell() != null) {
//				c.getPA().resetAutocast();
//			}
			//System.out.println("3buttonID:"+actionButtonId);
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemProjectInsanity.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			CombatTab.setButton(c, 3, c.getCombatTab());
			break;

		case 13092:
			// if (c.inWild()) {
			// c.getTradeAndDuel().declineTrade();
			// break;
			// }
			if (System.currentTimeMillis() - c.lastButton < 400) {

				c.lastButton = System.currentTimeMillis();

				break;

			} else {

				c.lastButton = System.currentTimeMillis();

			}
			Client ot = (Client) PlayerHandler.players[c.tradeWith];
			if (ot == null) {
				c.getTradeAndDuel().declineTrade();
				c.sendMessage("Trade declined as the other player has disconnected.");
				break;
			}
			c.getPacketSender().sendFrame126("Waiting for other player...", 3431);
			ot.getPacketSender().sendFrame126("Other player has accepted", 3431);
			c.goodTrade = true;
			ot.goodTrade = true;

			for (GameItem item : c.getTradeAndDuel().offeredItems) {
				if (item.getId() > 0) {
					int amountOfNewItems = c.getTradeAndDuel().offeredItems.size();
					for (GameItem tradeitem : c.getTradeAndDuel().offeredItems) {
						if (tradeitem.isStackable() && ot.getItems().playerHasItem(tradeitem.getId())) {
							if ((long) (ot.getItems().getItemAmount(tradeitem.getId()) + tradeitem.getAmount()) <
							Integer.MAX_VALUE) {
								amountOfNewItems--;
							}
						}
					}
					if (ot.getItems().freeSlots() < amountOfNewItems) {
						c.sendMessage(ot.getNameSmartUp() + " only has " + ot.getItems().freeSlots()
								+ " free slots, please remove "
								+ (c.getTradeAndDuel().offeredItems.size() - ot.getItems().freeSlots()) + " items.");
						ot.sendMessage(c.getNameSmartUp() + " has to remove "
								+ (c.getTradeAndDuel().offeredItems.size() - ot.getItems().freeSlots())
								+ " items or you could offer them "
								+ (c.getTradeAndDuel().offeredItems.size() - ot.getItems().freeSlots()) + " items.");
						c.goodTrade = false;
						ot.goodTrade = false;
						c.getPacketSender().sendFrame126("Not enough inventory space...", 3431);
						ot.getPacketSender().sendFrame126("Not enough inventory space...", 3431);
						break;
					} else {
						c.getPacketSender().sendFrame126("Waiting for other player...", 3431);
						ot.getPacketSender().sendFrame126("Other player has accepted", 3431);
						c.goodTrade = true;
						ot.goodTrade = true;
					}
				}
			}
			if (c.inTrade && !c.tradeConfirmed && ot.goodTrade && c.goodTrade) {
				c.tradeConfirmed = true;
				if (ot.tradeConfirmed) {
					c.getTradeAndDuel().confirmScreen();
					ot.getTradeAndDuel().confirmScreen();
					break;
				}

			}

			break;

		case 13218:
			if (System.currentTimeMillis() - c.lastButton < 400) {

				c.lastButton = System.currentTimeMillis();

				break;

			} else {

				c.lastButton = System.currentTimeMillis();

			}
			Client ot1 = (Client) PlayerHandler.players[c.tradeWith];
			c.tradeAccepted = true;
			if (ot1 == null) {
				c.getTradeAndDuel().declineTrade();
				c.sendMessage("Trade declined as the other player has disconnected.");
				break;
			}

			if (c.inTrade && c.tradeConfirmed && ot1.tradeConfirmed && !c.tradeConfirmed2) {
				c.tradeConfirmed2 = true;
				if (ot1.tradeConfirmed2) {
					c.acceptedTrade = true;
					ot1.acceptedTrade = true;
					// c.getTradeAndDuel().giveItems();
					// ot1.getTradeAndDuel().giveItems();
					TradeAndDuel.giveItems(c, ot1);
					break;
				}
				ot1.getPacketSender().sendFrame126("Other player has accepted.", 3535);
				c.getPacketSender().sendFrame126("Waiting for other player...", 3535);
			}

			break;
		/* Rules Interface Buttons */
		case 125011: // Click agree
			if (!c.ruleAgreeButton) {
				c.ruleAgreeButton = true;
				c.getPacketSender().setConfig(701, 1);
			} else {
				c.ruleAgreeButton = false;
				c.getPacketSender().setConfig(701, 0);
			}
			break;
		case 125003:// Accept
			if (c.ruleAgreeButton) {
				c.getPacketSender().showInterface(3559);
				c.newPlayer = false;
			} else if (!c.ruleAgreeButton) {
				c.sendMessage("You need to click on you agree before you can continue on.");
			}
			break;
		case 125006:// Decline
			c.sendMessage("You have chosen to decline, Client will be disconnected from the server.");
			break;
		/* End Rules Interface Buttons */

		/* Player Options */
		case 74176:
			if (!c.mouseButton) {
				c.mouseButton = true;
				c.getPacketSender().setConfig(500, 1);
				c.getPacketSender().setConfig(170, 1);
			} else if (c.mouseButton) {
				c.mouseButton = false;
				c.getPacketSender().setConfig(500, 0);
				c.getPacketSender().setConfig(170, 0);
			}
			break;
			
		case 179175:
			c.getPacketSender().setSidebarInterface(11, 64000);
			break;
			

		case 3189:
		case 74184:
			if (c.splitChat == false) {
				c.splitChat = true;
				c.getPacketSender().setConfig(502, 1);
				c.getPacketSender().setConfig(287, 1);
			} else {
				c.splitChat = false;
				c.getPacketSender().setConfig(502, 0);
				c.getPacketSender().setConfig(287, 0);
			}
			break;

		case 74180:
			if (!c.chatEffects) {
				c.chatEffects = true;
				c.getPacketSender().setConfig(501, 1);
				c.getPacketSender().setConfig(171, 0);
			} else {
				c.chatEffects = false;
				c.getPacketSender().setConfig(501, 0);
				c.getPacketSender().setConfig(171, 1);
			}
			break;

		case 74188:
			if (!c.acceptAid) {
				c.acceptAid = true;
				c.getPacketSender().setConfig(503, 1);
				c.getPacketSender().setConfig(427, 1);
			} else {
				c.acceptAid = false;
				c.getPacketSender().setConfig(503, 0);
				c.getPacketSender().setConfig(427, 0);
			}
			break;

		case 74192:
			if (!c.isRunning) {
				c.isRunning = true;
				c.getPacketSender().setConfig(504, 1);
				c.getPacketSender().setConfig(173, 1);
			} else {
				c.isRunning = false;
				c.getPacketSender().setConfig(504, 0);
				c.getPacketSender().setConfig(173, 0);
			}
			break;

		case 74201:// brightness1
			c.getPacketSender().setConfig(505, 1);
			c.getPacketSender().setConfig(506, 0);
			c.getPacketSender().setConfig(507, 0);
			c.getPacketSender().setConfig(508, 0);
			c.getPacketSender().setConfig(166, 1);
			break;

		case 74203:// brightness2
			c.getPacketSender().setConfig(505, 0);
			c.getPacketSender().setConfig(506, 1);
			c.getPacketSender().setConfig(507, 0);
			c.getPacketSender().setConfig(508, 0);
			c.getPacketSender().setConfig(166, 2);
			break;

		case 74204:// brightness3
			c.getPacketSender().setConfig(505, 0);
			c.getPacketSender().setConfig(506, 0);
			c.getPacketSender().setConfig(507, 1);
			c.getPacketSender().setConfig(508, 0);
			c.getPacketSender().setConfig(166, 3);
			break;

		case 74205:// brightness4
			c.getPacketSender().setConfig(505, 0);
			c.getPacketSender().setConfig(506, 0);
			c.getPacketSender().setConfig(507, 0);
			c.getPacketSender().setConfig(508, 1);
			c.getPacketSender().setConfig(166, 4);
			break;
		case 74206:// area1
			c.getPacketSender().setConfig(509, 1);
			c.getPacketSender().setConfig(510, 0);
			c.getPacketSender().setConfig(511, 0);
			c.getPacketSender().setConfig(512, 0);
			break;
		case 74207:// area2
			c.getPacketSender().setConfig(509, 0);
			c.getPacketSender().setConfig(510, 1);
			c.getPacketSender().setConfig(511, 0);
			c.getPacketSender().setConfig(512, 0);
			break;
		case 74208:// area3
			c.getPacketSender().setConfig(509, 0);
			c.getPacketSender().setConfig(510, 0);
			c.getPacketSender().setConfig(511, 1);
			c.getPacketSender().setConfig(512, 0);
			break;
		case 74209:// area4
			c.getPacketSender().setConfig(509, 0);
			c.getPacketSender().setConfig(510, 0);
			c.getPacketSender().setConfig(511, 0);
			c.getPacketSender().setConfig(512, 1);
			break;

		/* EMOTES */
		case 154: // skill cape emote
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(8)) {
				return;
			}
			SkillCapes.emote(c);
			break;

		case 168:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(855);
			c.getMovement().resetWalkingQueue();
			break;
		case 169:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(856);
			c.getMovement().resetWalkingQueue();
			break;
		case 162:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(857);
			c.getMovement().resetWalkingQueue();
			break;
		case 164:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(858);
			c.getMovement().resetWalkingQueue();
			break;
		case 165:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(859);
			c.getMovement().resetWalkingQueue();
			break;
		case 161:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(860);
			c.getMovement().resetWalkingQueue();
			break;
		case 170:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(861);
			c.getMovement().resetWalkingQueue();
			break;
		case 171:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(862);
			c.getMovement().resetWalkingQueue();
			break;
		case 163:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(863);
			c.getMovement().resetWalkingQueue();
			break;
		case 167:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(864);
			c.getMovement().resetWalkingQueue();
			break;
		case 172:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(865);
			c.getMovement().resetWalkingQueue();
			break;
		case 166:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(866);
			c.getMovement().resetWalkingQueue();
			break;
		case 52050:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2105);
			c.getMovement().resetWalkingQueue();
			break;
		case 52051:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2106);
			c.getMovement().resetWalkingQueue();
			break;
		case 52052:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2107);
			c.getMovement().resetWalkingQueue();
			break;
		case 52053:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2108);
			c.getMovement().resetWalkingQueue();
			break;
		case 52054:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2109);
			c.getMovement().resetWalkingQueue();
			break;
		case 52055:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2110);
			c.getMovement().resetWalkingQueue();
			break;
		case 52056:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2111);
			c.getMovement().resetWalkingQueue();
			break;
		case 52057:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2112);
			c.getMovement().resetWalkingQueue();
			break;
		case 52058:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(2113);
			c.getMovement().resetWalkingQueue();
			break;
		case 43092:
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(2)) {
				return;
			}
			c.startAnimation(1374);
			c.getMovement().resetWalkingQueue();
			c.startGraphic(Graphic.create(1702));
			break;
		case 2155:
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(7)) {
				return;
			}
			c.startAnimation(11044);
			c.getMovement().resetWalkingQueue();
			c.startGraphic(Graphic.create(1973));
			break;
		case 25103:
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(5)) {
				return;
			}
			c.startAnimation(10530);
			c.getMovement().resetWalkingQueue();
			c.startGraphic(Graphic.create(1864));
			break;
		case 25106:
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(5)) {
				return;
			}
			c.startAnimation(8770);
			c.startGraphic(Graphic.create(1553));
			c.getMovement().resetWalkingQueue();
			break;
		case 2154:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(7531);
			c.getMovement().resetWalkingQueue();
			break;
		case 52071:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(0x84F);
			c.getMovement().resetWalkingQueue();
			break;
		case 52072:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(0x850);
			c.getMovement().resetWalkingQueue();
			break;
		case 73003:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(6111);
			c.getMovement().resetWalkingQueue();
			break;
		case 73001:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(3544);
			c.getMovement().resetWalkingQueue();
			break;
		case 73000:
			if (c.duelStatus > 3)
				return;
			if (c.isInCombatWithPlayer()) {
				c.sendMessage("You cannot do this emote in combat!");
				return;
			}
			c.startAnimation(3543);
			c.getMovement().resetWalkingQueue();
			break;
		case 250:
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(7)) {
				return;
			}
			c.startAnimation(9990);
			c.getMovement().resetWalkingQueue();
			c.startGraphic(Graphic.create(1734));
			break;
		case 251:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(4278);
			c.getMovement().resetWalkingQueue();
			break;
		case 59062:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(4280);
			c.getMovement().resetWalkingQueue();
			break;
		case 72254:
			if (c.duelStatus > 3)
				return;
			c.startAnimation(4275);
			c.getMovement().resetWalkingQueue();
			break;
		case 73004:
			if (c.duelStatus > 3)
				return;
			if (!c.getEmoteTimer().checkThenStart(2)) {
				return;
			}
			c.startAnimation(7272);
			c.getMovement().resetWalkingQueue();
			c.startGraphic(Graphic.create(1244));
			break;
		case 72255:
			if (c.duelStatus > 3)
				return;
			if (c.isInCombatWithPlayer()) {
				c.sendMessage("You cannot do this emote in combat!");
				c.getMovement().resetWalkingQueue();
				return;
			}
			if (!c.getEmoteTimer().checkThenStart(6)) {
				return;
			}
			c.startAnimation(2414);
			c.startGraphic(Graphic.create(1537));
			break;
		/* END OF EMOTES */

		case 24017:
			c.getPA().resetAutocast();
			// c.sendFrame246(329, 200, c.playerEquipment[c.playerWeapon]);
			c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
					ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
			// c.setSidebarInterface(0, 328);
			// c.setSidebarInterface(6, c.playerMagicBook == 0 ? 1151 :
			// c.playerMagicBook == 1 ? 12855 : 1151);
			break;
		}

	}

}
