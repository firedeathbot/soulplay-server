package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.click.object.ObjectFirstClick;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.raids.map.rooms.greatolm.DouseFire;
import com.soulplay.content.tob.rooms.ChestRoom;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;
import com.soulplay.util.Misc;

/**
 * Click Object
 */
public class ClickObject implements PacketType {

	public static final int FIRST_CLICK = 132, SECOND_CLICK = 252,
			THIRD_CLICK = 70, FOURTH_CLICK = 234, FIFTH_CLICK = 228;

	public void handleSpecialCase(Client c, int id, int x, int y) {

	}
	
	public static boolean canInteract(Client c) {
		if (c.isDead() || c.getHitPoints() <= 0) {
			return false;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2
				|| c.duelStatus == 3 || c.duelStatus == 4) {
			return false;
		}

		if (!c.correctlyInitialized) {
			return false;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter
				|| !c.setMode) {
			return false;
		}

		if (c.duelStatus == 6) {
			return false;
		}

		if (c.isInJail()) {
			return false;
		}

		if (c.logoutOnVerificationState())
			return false;

		if (c.performingAction) {
			return false;
		}

		if (c.teleTimer > 4) {
			return false;
		}

		if (c.isStunned()) {
			return false;
		}

//		if (c.teleportToX > 0) { // do not handle packets for
//									// players that are being moved //update 4/8/2018 no longer needed with new processing of object clicking
//			return false;
//		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return false;
			}
		}

		if (c.getAgility().doingAgility) {
			return false;
		}
		return true;
	}

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		if (!c.getController().canClickObject()) {
		    return;
		}
		
		final int uniqueActionId = Misc.generateRandomActionId();

		try {

			switch (packetType) {

				case FIRST_CLICK: {
					final int obX = c.getInStream().readLEShortA();
					int obId = c.getInStream().read3Bytes();
					final int obY = c.getInStream().readUnsignedShortA();
					
					if (!canInteract(c))
						return;

					if (c.playerIndex > 0 || c.npcIndex > 0) {
						c.getCombat().resetPlayerAttack();
					}

					if (Math.abs(c.getX() - obX) > 25
							|| Math.abs(c.getY() - obY) > 25) {
						c.clickObjectType = c.objectX = c.objectId = c.objectY = 0;
						break;
					}

					if (c.tobManager != null) {
						if (obId == ChestRoom.NORMAL_CHEST_ARROW) {
							obId = ChestRoom.NORMAL_CHEST;
						}
						if (obId == ChestRoom.PURPLE_CHEST_ARROW) {
							obId = ChestRoom.PURPLE_CHEST;
						}
					}

					c.uniqueActionId = uniqueActionId;

					c.prevObjectId = c.objectId;
					c.prevObjectX = c.objectX;
					c.prevObjectY = c.objectY;
					c.clickObjectType = c.objectX = c.objectId = c.objectY = 0;
					c.getPA().resetFollow();

					c.objectId = obId;
					c.objectX = obX;
					c.objectY = obY;

					if ((c.playerRights == 3 && Config.SERVER_DEBUG) || c.debug) {
						c.sendMessage(String.format(
								"[FirstObjectClick] id=%d x=%d y=%d", obId, obX,
								obY));
					}

					if (c.duelRule[DuelRules.NO_MOVEMENT] && c.duelStatus == 5) {
						return;
					}
					
					if (obId == 2513) {
						ObjectFirstClick.shootRangingGuildTarget(c, obId, obX, obY);
					}

					c.walkingToObject = true;
					c.clickObjectType = 1;
					
					if (obId == DouseFire.FIRE_OBJ) {
						DouseFire.douseFire(c, obId, obX, obY);
					}
					break;
				}

				case SECOND_CLICK: {
					final int obId2 = c.getInStream().read3Bytes();
					final int obY2 = c.getInStream().readLEShort();
					final int obX2 = c.getInStream().readUnsignedShortA();
					// c.objectDistance = 1;

					if (!canInteract(c))
						return;

					if (c.playerIndex > 0 || c.npcIndex > 0) {
						c.getCombat().resetPlayerAttack();
					}

					c.uniqueActionId = uniqueActionId;

					c.prevObjectId = c.objectId;
					c.prevObjectX = c.objectX;
					c.prevObjectY = c.objectY;
					c.clickObjectType = c.objectX = c.objectId = c.objectY = 0;
					c.getPA().resetFollow();

					c.objectId = obId2;
					c.objectX = obX2;
					c.objectY = obY2;
					c.clickObjectType = 2;
					
					c.walkingToObject = true;

					BarbarianAssault.handleSecondClickObject(c, obId2);

					if (c.playerRights == 3 && c.debug) {
						c.sendMessage(String.format(
								"[SecondObjectClick] id=%d x=%d y=%d", obId2,
								obX2, obY2));
					}

					break;
				}

				case THIRD_CLICK: {
					final int obX3 = c.getInStream().readLEShort();
					final int obY3 = c.getInStream().readUnsignedShort();
					final int obId3 = c.getInStream().read3Bytes();

					if (!canInteract(c))
						return;

					if (c.playerIndex > 0 || c.npcIndex > 0) {
						c.getCombat().resetPlayerAttack();
					}

					c.uniqueActionId = uniqueActionId;

					c.prevObjectId = c.objectId;
					c.prevObjectX = c.objectX;
					c.prevObjectY = c.objectY;
					c.clickObjectType = c.objectX = c.objectId = c.objectY = 0;
					c.getPA().resetFollow();

					c.objectId = obId3;
					c.objectX = obX3;
					c.objectY = obY3;

					c.clickObjectType = 3;
					
					c.walkingToObject = true;

					if (c.playerRights == 3 && c.debug) {
						c.sendMessage(String.format("[ThirdObjectClick] id=%d x=%d y=%d", obId3, obX3, obY3));
					}

					BarbarianAssault.handleThirdClickObject(c, obId3);

					break;
				}
					
				case FOURTH_CLICK: {
					final int x = c.getInStream().readUnsignedShort();
					final int id = c.getInStream().read3Bytes();
					final int y = c.getInStream().readUnsignedShort();

					if (!canInteract(c))
						return;

					if (c.playerIndex > 0 || c.npcIndex > 0) {
						c.getCombat().resetPlayerAttack();
					}

					
					if (c.debug) {
						c.sendMessage(String.format("ObjectClickFour.class - ID: %d - X: %d - Y: %d", id, x, y));
					}
					c.uniqueActionId = uniqueActionId;

					c.prevObjectId = c.objectId;
					c.prevObjectX = c.objectX;
					c.prevObjectY = c.objectY;
					c.clickObjectType = c.objectX = c.objectId = c.objectY = 0;
					c.getPA().resetFollow();

					c.objectId = id;
					c.objectX = x;
					c.objectY = y;
					
					c.walkingToObject = true;

					if (c.playerRights == 3 || c.isDev()) {
						Misc.println("ObjectFourthClick: objectId: " + c.objectId
								+ "  ObjectX: " + c.objectX + "  objectY: "
								+ c.objectY + " Xoff: " + (c.getX() - c.objectX)
								+ " Yoff: " + (c.getY() - c.objectY));
					}

					c.clickObjectType = 16;
					break;
				}

				case FIFTH_CLICK: {
					final int obId4 = c.getInStream().read3Bytes();
					final int obY4 = c.getInStream().readUnsignedShortA();
					final int obX4 = c.getInStream().readUnsignedShort();

					if (!canInteract(c))
						return;

					if (c.playerIndex > 0 || c.npcIndex > 0) {
						c.getCombat().resetPlayerAttack();
					}

					c.uniqueActionId = uniqueActionId;

					c.prevObjectId = c.objectId;
					c.prevObjectX = c.objectX;
					c.prevObjectY = c.objectY;
					c.clickObjectType = c.objectX = c.objectId = c.objectY = 0;
					c.getPA().resetFollow();

					c.objectId = obId4;
					c.objectX = obX4;
					c.objectY = obY4;
					
					c.walkingToObject = true;

					if (c.playerRights == 3 || c.isDev()) {
						Misc.println("ObjectFifthClick: objectId: " + c.objectId
								+ "  ObjectX: " + c.objectX + "  objectY: "
								+ c.objectY + " Xoff: " + (c.getX() - c.objectX)
								+ " Yoff: " + (c.getY() - c.objectY));
					}

					c.clickObjectType = 15;

					if (c.playerRights == 3 && Config.SERVER_DEBUG) {
						c.sendMessage(String.format(
								"[FifththObjectClick] id=%d x=%d y=%d", obId4,
								obX4, obY4));
					}
					break;
				}

			}

		} catch (Exception ex) {
			System.out.println("Someone tried to crash server. PlayerName:"
					+ c.getName() + " at X:" + c.getX() + " Y:" + c.getY()
					+ " Z:" + c.getZ() + " obj:" + c.objectId);
			ex.printStackTrace();
		}

	}

}
