package com.soulplay.net.packet.in;

import com.soulplay.content.player.skills.firemaking.Firemaking;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.net.packet.PacketType;
import com.soulplay.util.Misc;

public class ItemOnGroundItem implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		c.getInStream().readLEShort();
		int itemUsed = c.getInStream().read3Bytes();
		int groundItem = c.getInStream().read3Bytes();
		int gItemY = c.getInStream().readShortA();
		int itemUsedSlot = c.getInStream().readLEShortA();
		int gItemX = c.getInStream().readUnsignedShort();

		if (itemUsedSlot < 0 || itemUsedSlot > c.playerItems.length) {
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;
		
		if (c.getReceivedPacket()[packetType]) {
			return;
		}
		c.setReceivedPacket(packetType, true);

		if (c.isDead()) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}
		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (!c.getItems().playerHasItem(itemUsed, 1)) {
			return;
		}
		
		c.walkingToItem = true;
		c.pItemX = gItemX;
		c.pItemY = gItemY;
		
		if (!ItemHandler.itemExistsNearbyRegions(c, groundItem, gItemX, gItemY,
				c.getHeightLevel())) {
			return;
		}
		switch (itemUsed) {
			case 590:
				
				if (c.skillingEvent != null)
					c.resetSkillingEvent();
				
				c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
					
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						if (!c.destinationReachedAny())
							return;
						
						container.stop();
						c.skillingEvent = null;
						

						Firemaking.attemptFire(c, itemUsed, groundItem, gItemX, gItemY, true);
					}
				}, 1);
				break;

			default:
				if (c.playerRights == 3 || c.isDev()) {
					Misc.println("ItemUsed " + itemUsed + " on Ground Item "
							+ groundItem);
				}
				break;
		}
	}

}
