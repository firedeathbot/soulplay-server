package com.soulplay.net.packet.in;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.UseItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class ItemOnObject implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {

		c.getInStream().readUnsignedShort();
		final int objectId = c.getInStream().read3Bytes();
		final int objectY = c.getInStream().readLEShortA();
		final int itemSlot = c.getInStream().readLEShort();
		final int objectX = c.getInStream().readLEShortA();
		final int itemId = c.getInStream().read3Bytes();

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.performingAction) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;
		
		if (c.getReceivedPacket()[packetType]) {
			return;
		}
		c.setReceivedPacket(packetType, true);

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		c.prevObjectX = c.objectX = objectX;
		c.prevObjectY = c.objectY = objectY;
		c.objectId = objectId;

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (!c.getItems().playerHasItem(itemId, itemSlot, 1)) {
			return;
		}
		if (c.performingAction) {
			c.sendMessage("You are currently busy.");
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		c.isIdle = false;

		if (c.debug) {
			c.sendMessage(String.format("Item:%d - Object:%d - X:%d - Y:%d - Z:%d", itemId, objectId, objectX, objectY, c.getZ()));
			if (c.dynObjectManager != null) {
				c.sendMessage("object type:" + c.dynObjectManager.getObject(objectX, objectY, c.getZ()).getType());
			}
		}

		// if (o == null && !(c.objectId == 8151 || c.objectId == 8389 ||
		// c.objectId == 8132 || c.objectId == 7848)) {
		// c.sendMessage("Object null -item on object");
		// return;
		// }

		if (itemId == 19950) { // strange potato
			
			final GameObject o = RegionClip.getGameObject(c.objectId, c.objectX,
					c.objectY, c.heightLevel);
			
			c.oX = o.getX();
			c.oI = -1;
			c.oY = o.getY();
			c.oH = o.getZ();
			c.oF = o.getOrientation();
			c.oT = o.getType();
			c.getPacketSender().addObjectPacket(c.oI, c.oX, c.oY, c.oT, c.oF);
			c.sendMessage(
					"You have removed the object with the potato. use ::wobj to write to file");
			return;
		}
		
		c.walkingToObject = true;

		if (c.destinationReached()) {
			UseItem.ItemonObject(c, objectId, objectX, objectY, itemId,
					itemSlot);
		} else {
			c.clickObjectType = 4;
			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (c.disconnected) {
						container.stop();
						return;
					}
					if (c.clickObjectType == 4 && c.destinationReached()) {
						UseItem.ItemonObject(c, objectId, objectX, objectY,
								itemId, itemSlot);
						container.stop();
					}
					if (c.clickObjectType != 4) {
						container.stop();
					}
				}

				@Override
				public void stop() {
					if (c != null) {
						c.clickObjectType = 0;
					}
				}
			}, 1);
		}

	}

}
