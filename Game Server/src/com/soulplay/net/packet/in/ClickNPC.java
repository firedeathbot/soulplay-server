package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.player.combat.AttackNPC;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.skills.Fishing;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.InteractionMask;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;
import com.soulplay.util.Misc;

/**
 * Click NPC
 */
public class ClickNPC implements PacketType {

	public static final int ATTACK_NPC = 72, MAGE_NPC = 131, FIRST_CLICK = 155,
			SECOND_CLICK = 17, THIRD_CLICK = 21, FOURTH_CLICK = 18;

			@Override
			public void processPacket(final Client c, int packetType, int packetSize) {
				c.npcIndex = 0;
				c.npcClickIndex = 0;
				c.playerIndex = 0;
				c.clickNpcType = 0;
				c.destOffsetX = c.destOffsetY = c.clickReachDistance = 1;

				if (c.getDialogueBuilder().isActive()) {
					c.getDialogueBuilder().setClose(true).reset();
				}

				// c.getPA().resetFollow();
				// c.getPA().removeAllWindows();

				switch (packetType) {
				/**
				 * Attack npc melee or range
				 **/
				case ATTACK_NPC: {
					final int npcIndex = c.getInStream().readUnsignedShortA();

					if (c.getTransformId() != -1 && !Config.isOwner(c)) {
						c.transform(-1);
						c.getMovement().resetWalkingQueue();
						c.getPA().resetMovementAnimation();
					}

					if (c.spectatorMode) {
						c.sendMessage("Can't do that in spectator mode.");
						return;
					}

					if (c.isDead() || !c.inNpcList(npcIndex)) {
						return;
					}
					if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
							|| c.duelStatus == 4) {
						return;
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.saveFile || c.newPlayer || !c.saveCharacter
							|| !c.setMode) {
						return;
					}
					if (c.duelStatus == 6) {
						return;
					}
					if (c.performingAction) {
						return;
					}

					if (c.logoutOnVerificationState())
						return;

					if (c.teleTimer > 4) {
						return;
					}
					if (c.noClip) {
						if (c.noClipX > 0 || c.noClipY > 0) {
							return;
						}
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.getController().canClickNpc()) {
						return;
					}
					if (c.isStunned()) {
						return;
					}

					if (c.teleportToX > 0) { // do not handle packets for players
						// that are being moved
						return;
					}

					if (c.getAgility().doingAgility) {
						return;
					}

					c.npcIndex = npcIndex;

					if (c.npcIndex < 0 || c.npcIndex >= NPCHandler.maxNPCs) {
						c.npcIndex = 0;
						return;
					}

					NPC npc = NPCHandler.npcs[npcIndex];
					if (NPCHandler.npcs[c.npcIndex] == null) {
						c.npcIndex = 0;
						return;
					}

					//c.sendMessage("NPC ID:"+ npc.npcType);

					if (c.debugNpc) {
						c.sendMessage("printed the npc index:"+c.npcIndex+" to string.");
						try {
							System.out.println("NPC to String index:"+c.npcIndex+" STRING:"+npc.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					if (npc.isDead()) {
						c.getMovement().resetWalkingQueue();
						c.getCombat().resetPlayerAttack();
						c.npcIndex = 0;
						return;
					}

					if (npc.getSkills().getMaximumLifepoints() == 0) {
						c.npcIndex = 0;
						break;
					}

					if (c.isOwner() && c.noClip) {
						npc.addInfliction(c.mySQLIndex, 10);
						c.lastNpcAttacked = npc.npcIndex;
						// npc.killedBy = c.getId();
						// c.sendMessage("PlayerID "+c.getId());
						npc.underAttackBy = c.getId();
						npc.setDead(true);
						// c.sendMessage("inMulti "+npc.inMulti());
						return;
					}

					if (c.followId > 0) {
						c.getPA().resetFollow();
					}

					c.walkingToMob = true;
					c.finalLocalDestX = c.finalLocalDestY = 0;
					c.destOffsetX = c.destOffsetY = 1;
					c.walkUpToPathBlockedMob = false;
					c.clickReachDistance = 1;
					
					setNpcClickOffset(c, npc);
					
					c.followId2 = npcIndex;
					c.face(npc);

					break;
				}
					/**
					 * Attack npc with magic
					 **/
				case MAGE_NPC:
					final int npcIndex2 = c.getInStream().readLEShortA();

					int castingSpellId = c.getInStream().readUnsignedShortA();
					if (npcIndex2 < 0 || npcIndex2 >= NPCHandler.maxNPCs) {
						return;
					}

					if (c.spectatorMode) {
						c.sendMessage("Can't do that in spectator mode.");
						return;
					}

					if (c.isDead() || !c.inNpcList(npcIndex2)) {
						return;
					}
					if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
							|| c.duelStatus == 4) {
						return;
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.getController().canClickNpc()) {
						return;
					}
					if (!c.saveFile || c.newPlayer || !c.saveCharacter
							|| !c.setMode) {
						return;
					}
					if (c.duelStatus == 6) {
						return;
					}
					if (c.performingAction) {
						return;
					}

					if (c.logoutOnVerificationState())
						return;

					if (c.teleTimer > 4) {
						return;
					}

					if (c.noClip) {
						if (c.noClipX > 0 || c.noClipY > 0) {
							return;
						}
					}
					if (!c.correctlyInitialized) {
						return;
					}

					if (c.isStunned()) {
						return;
					}

					if (c.teleportToX > 0) { // do not handle packets for players
						// that are being moved
						return;
					}

					if (c.getAgility().doingAgility) {
						return;
					}

					if (c.sidebarInterfaceInfo.getOrDefault(6, -1) != c.playerMagicBook.getInterfaceId()) {
						return;
					}

					if (NPCHandler.npcs[npcIndex2] == null) {
						break;
					}

					if (!AttackNPC.canAttackNpc(NPCHandler.npcs[npcIndex2].npcType)) {
						return;
					}

					c.npcIndex = npcIndex2;

					if (NPCHandler.npcs[c.npcIndex].isDead()) {
						c.getMovement().resetWalkingQueue();
						c.getCombat().resetPlayerAttack();
						return;
					}

					if (NPCHandler.npcs[c.npcIndex].getSkills().getMaximumLifepoints() == 0) {
						c.sendMessage("You can't attack this npc.");
						break;
					}

					SpellsData spellData = SpellsData.getSpell(c, castingSpellId);
					if (spellData == null) {
						c.getMovement().resetWalkingQueue();
						c.getPacketSender().resetMinimapFlagPacket();
						c.getMovement().resetDestination();
						return;
					}

					c.setSingleCastSpell(spellData);

					c.usingSpellFromSpellBook = true;
					
					c.followId2 = npcIndex2;

					c.walkingToMob = true;
					c.finalLocalDestX = c.finalLocalDestY = 0;
					c.destOffsetX = c.destOffsetY = 1;
					c.walkUpToPathBlockedMob = false;
					c.clickReachDistance = 1;

					break;

				case FIRST_CLICK: {
					c.npcClickIndex = c.getInStream().readLEShort();
					if (c.npcClickIndex < 0
							|| c.npcClickIndex >= NPCHandler.maxNPCs) {
						c.npcClickIndex = 0;
						return;
					}

					if (c.spectatorMode) {
						c.sendMessage("Can't do that in spectator mode.");
						return;
					}

					if (c.isDead() || !c.inNpcList(c.npcClickIndex)) {
						return;
					}
					if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
							|| c.duelStatus == 4) {
						return;
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.saveFile || c.newPlayer || !c.saveCharacter
							|| !c.setMode) {
						return;
					}
					if (c.duelStatus == 6) {
						return;
					}
					if (c.performingAction) {
						return;
					}

					if (!c.getController().canClickNpc()) {
						return;
					}

					if (c.logoutOnVerificationState())
						return;

					if (c.teleTimer > 4) {
						return;
					}

					if (c.noClip) {
						if (c.noClipX > 0 || c.noClipY > 0) {
							return;
						}
					}
					if (!c.correctlyInitialized) {
						return;
					}

					if (c.isStunned()) {
						return;
					}

					if (c.teleportToX > 0) { // do not handle packets for players
						// that are being moved
						return;
					}

					if (c.getAgility().doingAgility) {
						return;
					}

					if (NPCHandler.npcs[c.npcClickIndex] == null) {
						return;
					}
					c.face(NPCHandler.npcs[c.npcClickIndex]);

					if (c.getX() == NPCHandler.npcs[c.npcClickIndex].getX() && c
							.getY() == NPCHandler.npcs[c.npcClickIndex].getY()) {
						c.getPA().stepAway();
					}

					
					c.walkingToMob = true;
					c.finalLocalDestX = c.finalLocalDestY = 0;
					c.destOffsetX = c.destOffsetY = 1;
					c.walkUpToPathBlockedMob = false;
					c.clickReachDistance = 1;
					
					setNpcClickOffset(c, NPCHandler.npcs[c.npcClickIndex]);

					c.npcType = NPCHandler.npcs[c.npcClickIndex].npcType;
					c.clickNpcType = 1;
				}
				break;

				case SECOND_CLICK:
					c.npcClickIndex = c.getInStream().readUnsignedLEShortA();
					if (c.npcClickIndex < 0
							|| c.npcClickIndex >= NPCHandler.maxNPCs) {
						c.npcClickIndex = 0;
						return;
					}

					if (c.spectatorMode) {
						c.sendMessage("Can't do that in spectator mode.");
						return;
					}

					if (c.isDead() || !c.inNpcList(c.npcClickIndex)) {
						return;
					}
					if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
							|| c.duelStatus == 4) {
						return;
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.saveFile || c.newPlayer || !c.saveCharacter
							|| !c.setMode) {
						return;
					}
					if (c.duelStatus == 6) {
						return;
					}
					if (c.performingAction) {
						return;
					}

					if (c.logoutOnVerificationState())
						return;

					if (c.teleTimer > 4) {
						return;
					}

					if (c.noClip) {
						if (c.noClipX > 0 || c.noClipY > 0) {
							return;
						}
					}
					if (!c.correctlyInitialized) {
						return;
					}

					if (!c.getController().canClickNpc()) {
						return;
					}

					if (c.isStunned()) {
						return;
					}

					if (c.teleportToX > 0) { // do not handle packets for players
						// that are being moved
						return;
					}

					if (c.getAgility().doingAgility) {
						return;
					}

					if (NPCHandler.npcs[c.npcClickIndex] == null) {
						return;
					}
					c.face(NPCHandler.npcs[c.npcClickIndex]);
					c.npcType = NPCHandler.npcs[c.npcClickIndex].npcType;

					if (NPCHandler.npcs[c.npcClickIndex].npcType == 7636) {//TODO temp code for now so we dont move to the npc lol
						c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

							int timer = 6;

							@Override
							public void execute(CycleEventContainer container) {

								if (c.disconnected) {
									container.stop();
									return;
								}

								if (!(c.getX() == 3298 && c.getY() == 4380) && !(c.getX() == 3297 && c.getY() == 4380) && c.getY() > 4359) {
									if (timer == 6) {
										if (Misc.random(1) == 0) {
											c.getPA().playerWalk(3297, 4379);
										} else
											c.getPA().playerWalk(3298, 4379);
									}

									timer--;
									if (timer == 2) {
										c.getPA().walkTo(0, 1, true);
									}
								} else if (!(c.getX() == 3297 && c.getY() == 4339) && !(c.getX() == 3296 && c.getY() == 4339)) {
									if (timer == 6) {
										if (Misc.random(1) == 0) {
											c.getPA().playerWalk(3296, 4340);
										} else
											c.getPA().playerWalk(3297, 4340);
									}

									timer--;
									if (timer == 2) {
										c.getPA().walkTo(0, -1, true);
									}
								} else {
									timer = 0;
								}
								if (timer == 0) { 
									container.stop();
								}
							}

							@Override
							public void stop() {
								Fishing.attemptdata(c, 14, c.npcClickIndex);

							}
						}, 1);
						return;
					}

					if (c.getX() == NPCHandler.npcs[c.npcClickIndex].getX() && c
							.getY() == NPCHandler.npcs[c.npcClickIndex].getY()) {
						c.getPA().stepAway();
					}
					
					c.walkingToMob = true;
					c.finalLocalDestX = c.finalLocalDestY = 0;
					c.destOffsetX = c.destOffsetY = 1;
					c.walkUpToPathBlockedMob = false;
					c.clickReachDistance = 1;
					
					setNpcClickOffset(c, NPCHandler.npcs[c.npcClickIndex]);

					c.clickNpcType = 2;
					break;

				case THIRD_CLICK:
					c.npcClickIndex = c.getInStream().readShort();
					if (c.npcClickIndex < 0
							|| c.npcClickIndex >= NPCHandler.maxNPCs) {
						c.npcClickIndex = 0;
						return;
					}

					if (c.spectatorMode) {
						c.sendMessage("Can't do that in spectator mode.");
						return;
					}

					if (c.isDead() || !c.inNpcList(c.npcClickIndex)) {
						return;
					}
					if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
							|| c.duelStatus == 4) {
						return;
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.saveFile || c.newPlayer || !c.saveCharacter
							|| !c.setMode) {
						return;
					}
					if (c.duelStatus == 6) {
						return;
					}
					if (c.performingAction) {
						return;
					}

					if (c.logoutOnVerificationState())
						return;

					if (c.teleTimer > 4) {
						return;
					}

					if (c.noClip) {
						if (c.noClipX > 0 || c.noClipY > 0) {
							return;
						}
					}
					if (!c.correctlyInitialized) {
						return;
					}
					if (!c.getController().canClickNpc()) {
						return;
					}

					if (c.isStunned()) {
						return;
					}

					if (c.teleportToX > 0) { // do not handle packets for players
						// that are being moved
						return;
					}

					if (c.getAgility().doingAgility) {
						return;
					}

					if (NPCHandler.npcs[c.npcClickIndex] == null) {
						return;
					}
					c.face(NPCHandler.npcs[c.npcClickIndex]);
					c.npcType = NPCHandler.npcs[c.npcClickIndex].npcType;

					c.walkingToMob = true;
					c.finalLocalDestX = c.finalLocalDestY = 0;
					c.destOffsetX = c.destOffsetY = 1;
					c.walkUpToPathBlockedMob = false;
					c.clickReachDistance = 1;

					if (c.getX() == NPCHandler.npcs[c.npcClickIndex].getX() && c
							.getY() == NPCHandler.npcs[c.npcClickIndex].getY()) {
						c.getPA().stepAway();
					}
					
					setNpcClickOffset(c, NPCHandler.npcs[c.npcClickIndex]);
					
					c.clickNpcType = 3;
					break;

				case FOURTH_CLICK:
					c.npcClickIndex = c.getInStream().readUnsignedLEShort();

					//System.out.println(c.npcClickIndex + " " + NPCHandler.maxNPCs);

					if (c.npcClickIndex < 0
							|| c.npcClickIndex >= NPCHandler.maxNPCs) {
						c.npcClickIndex = 0;
						return;
					}

					if (c.spectatorMode) {
						c.sendMessage("Can't do that in spectator mode.");
						return;
					}

					if (c.isDead() || !c.inNpcList(c.npcClickIndex)) {
						return;
					}

					if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
							|| c.duelStatus == 4) {
						return;
					}

					if (!c.correctlyInitialized) {
						return;
					}

					if (!c.saveFile || c.newPlayer || !c.saveCharacter
							|| !c.setMode) {
						return;
					}

					if (c.duelStatus == 6) {
						return;
					}

					if (c.performingAction) {
						return;
					}

					if (c.logoutOnVerificationState()) {
						return;
					}

					if (c.teleTimer > 4) {
						return;
					}

					if (c.noClip) {
						if (c.noClipX > 0 || c.noClipY > 0) {
							return;
						}
					}
					if (!c.correctlyInitialized) {
						return;
					}

					if (c.isStunned()) {
						return;
					}

					if (c.teleportToX > 0) { // do not handle packets for players
						// that are being moved
						return;
					}

					if (!c.getController().canClickNpc()) {
						return;
					}

					if (c.getAgility().doingAgility) {
						return;
					}

					if (c.npcClickIndex < 0	|| c.npcClickIndex > NPCHandler.maxNPCs) {
						return;
					}

					if (NPCHandler.npcs[c.npcClickIndex] == null) {
						return;
					}

					c.face(NPCHandler.npcs[c.npcClickIndex]);

					c.npcType = NPCHandler.npcs[c.npcClickIndex].npcType;

					c.walkingToMob = true;
					c.finalLocalDestX = c.finalLocalDestY = 0;
					c.destOffsetX = c.destOffsetY = 1;
					c.walkUpToPathBlockedMob = false;
					c.clickReachDistance = 1;

					if (c.getX() == NPCHandler.npcs[c.npcClickIndex].getX() && c
							.getY() == NPCHandler.npcs[c.npcClickIndex].getY()) {
						c.getPA().stepAway();
					}

					setNpcClickOffset(c, NPCHandler.npcs[c.npcClickIndex]);

					c.clickNpcType = 4;
					break;

				}

			}
			
			public static void setNpcClickOffset(Client c, NPC npc) {
				c.entityClickSizeX = c.entityClickSizeY = c.destOffsetX = c.destOffsetY = npc.getSize();
				c.entityWalkingFlag = 0; // 0 - all around npc
				c.clickNpcDistance = 1;
				switch (npc.npcType) {
				case 5098:
				case 5099:
				case 5100:
					c.disableInteractClip = true;
					c.clickReachDistance = 12;
					c.clickNpcDistance = 20;
					return;
				case 7663:
					c.disableInteractClip = true;
					return;
				case 325:
				case 8842:
				case 313:
					c.walkUpToPathBlockedMob = true;
					return;
				case 5000://lms manager
					//c.walkUpToPathBlockedMob = true;
					//c.destOffsetX = c.destOffsetY = 1;
					c.entityWalkingFlag = InteractionMask.SOUTH.getDirMask();
					c.clickReachDistance = 1;
					c.disableInteractClip = true;
					return;
				case 494://bankers
				case 2240://ge clerk
				case 2241://ge clerk
					c.clickReachDistance = 2;
					c.disableInteractClip = true;
					
					if (RSConstants.inGrandExchange(npc.getX(), npc.getY())) {
						//c.walkUpToPathBlockedMob = true;
						c.destOffsetX = c.destOffsetY = 1;
						c.clickReachDistance = 1;
					}
					return;
					
				case 8948: // armor set trader
					c.entityWalkingFlag = InteractionMask.SOUTH.getDirMask();
					c.clickReachDistance = 1;
					c.disableInteractClip = true;
					c.destOffsetY = 1;
					break;
				}

				if (npc.def != null && npc.def.getName().equalsIgnoreCase("sliding block")) {
					c.clickReachDistance = 16;
					c.walkUpToPathBlockedMob = true;
					c.clickNpcDistance = 10;
					c.disableInteractClip = true;
				}
			}
			
			
			public static void resetNpcClickVariables(Client c) {
				c.clickNpcType = 0;
				c.npcClickIndex = 0;
				c.walkingToObject = false;
				c.walkToX = c.walkToY = -1;
				c.getPA().resetFollow();
				c.clickReachDistance = 1;
				c.destOffsetX = c.destOffsetY = 0;
				c.entityClickSizeX = c.entityClickSizeY = 1;
			}
}
