package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.PacketType;

/**
 * Attack Player
 **/
public class AttackPlayer implements PacketType {

	public static final int ATTACK_PLAYER = 73, MAGE_PLAYER = 249;

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		c.playerIndex = 0;
		c.npcIndex = 0;

		if (c.walkingToObject) {
			c.walkingToObject = false;
		}
		
		if (c.logoutOnVerificationState())
			return;

		switch (packetType) {

				/**
				 * Attack player
				 **/
				case ATTACK_PLAYER:final int attackingPlayerIndex = c
						.getInStream().readLEShort();
				if (attackingPlayerIndex < 0
						|| attackingPlayerIndex >= Config.MAX_PLAYERS) {
					// c.playerIndex = 0;
					c.kickPlayer();
					return;
				}

				if (c.spectatorMode) {
					c.sendMessage("Can't do that in spectator mode.");
					return;
				}

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.isDead()) {
					c.getMovement().resetWalkingQueue();
					return;
				}
				if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
						|| c.duelStatus == 4) {
					return;
				}
				if (c.performingAction) {
					return;
				}

				if (c.isLockActions()) {
					c.sendMessage("Your player is currently busy..");
					return;
				}
				if (c.duelStatus == 6) {
					return;
				}

				if (c.noClip) {
					if (c.noClipX > 0 || c.noClipY > 0) {
						return;
					}
				}

				if (c.respawnTimer > -6) {
					c.getMovement().resetWalkingQueue();
					break;
				}
				
				c.playerIndex = attackingPlayerIndex;

				if (PlayerHandler.players[c.playerIndex] == null) {
					c.getCombat().resetPlayerAttack();
					break;
				}
				
				c.faceLocation(PlayerHandler.players[c.playerIndex].getX(), PlayerHandler.players[c.playerIndex].getY());

				c.face(PlayerHandler.players[c.playerIndex]);

				c.followId = c.playerIndex;
				c.walkingToMob = true;
				c.finalLocalDestX = c.finalLocalDestY = 0;
				c.destOffsetX = c.destOffsetY = 1;
				c.walkUpToPathBlockedMob = false;
				c.clickReachDistance = 1;
				
				if (!c.inMulti()) {
					c.attackingIndex = attackingPlayerIndex;
					c.startInteractionResetTimer();
					c.setInteractionIndex(c.attackingIndex);
				}
				
				break;

				/**
				 * Attack player with magic
				 **/
				case MAGE_PLAYER:

				final int attackPlayerIndex = c.getInStream().readShortA();
				int castingSpellId = c.getInStream().readLEShort();

				if (c.spectatorMode) {
					c.sendMessage("Can't do that in spectator mode.");
					return;
				}

				if (attackPlayerIndex < 0
						|| attackPlayerIndex >= Config.MAX_PLAYERS) {
					c.kickPlayer();
					return;
				}

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.isDead()) {
					return;
				}
				if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
						|| c.duelStatus == 4) {
					return;
				}
				if (c.performingAction) {
					return;
				}

				if (c.isLockActions()) {
					c.sendMessage("Your player is currently busy..");
					return;
				}
				if (c.duelStatus == 6) {
					return;
				}

				if (c.noClip) {
					if (c.noClipX > 0 || c.noClipY > 0) {
						return;
					}
				}

				SpellsData spellsData = SpellsData.getSpell(c, castingSpellId);

				if (spellsData == null) {
					c.getMovement().resetWalkingQueue();
					return;
				}

				c.playerIndex = attackPlayerIndex;

				if (PlayerHandler.players[c.playerIndex] == null) {
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					break;
				}
				
				if (!c.inMulti()) {
					c.attackingIndex = attackPlayerIndex;
					c.startInteractionResetTimer();
					c.setInteractionIndex(c.attackingIndex);
				}
				
				c.faceLocation(PlayerHandler.players[c.playerIndex].getX(), PlayerHandler.players[c.playerIndex].getY());
				
				if (c.respawnTimer > 0) {
					c.getCombat().resetPlayerAttack();
					c.getMovement().resetWalkingQueue();
					break;
				}

				c.setSingleCastSpell(spellsData);
				
				c.usingSpecial = false;
				c.getItems().updateSpecialBar();

				c.followId = c.playerIndex;
				
				c.walkingToMob = true;
				c.finalLocalDestX = c.finalLocalDestY = 0;
				c.destOffsetX = c.destOffsetY = 1;
				c.walkUpToPathBlockedMob = false;
				c.clickReachDistance = 1;

				c.usingSpellFromSpellBook = true;

				break;

		}

	}

}
