package com.soulplay.net.packet.in;

import com.soulplay.content.clans.ClanManager;
import com.soulplay.content.instances.InstanceInterface;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.login.RS2LoginProtocol;
import com.soulplay.net.packet.PacketType;
import com.soulplay.util.Misc;

public class EnterName implements PacketType {

	@Override
	public void processPacket(final Client c, final int packetType, final int packetSize) {
		String owner = Misc.longToPlayerName2(c.getInStream().readLong());
		if (owner == null) {
			return;
		}

		if (!c.lastAction.complete()) {
			return;
		}

		c.lastAction.startTimer(1);

		if (!RS2LoginProtocol.validRegex(owner)) {
			return;
		}
		if (owner.length() > 12) {
			return;
		}
		if (owner.endsWith(" ") || owner.startsWith(" ")) {
			return;
		}
		if (owner.contains("  ")) {
			return;
		}
		
		if (c.getDialogueBuilder().isActive()) {
			c.getDialogueBuilder().setEnterText(owner);
			c.getDialogueBuilder().executeEnterText();
			return;
		}

		switch (c.interfaceIdOpenMainScreen) {
			case 70200:
				InstanceInterface.createInstance(c, owner);
				return;
			case 45000:// market search by playername
				if (owner.isEmpty() || owner.equals("")) {
					c.sendMessage("Please enter a name to search for.");
					return;
				}

				Client c2 = (Client) PlayerHandler.getPlayerSmart(owner);
				if (c2 == null) {
					c.sendMessage(Misc.capitalize(owner) + " is not online.");
					return;
				}

				PlayerOwnedShops.openPlayerShop(c, c2);
				return;
		}

		if (owner.toLowerCase().startsWith("mod ")
				|| owner.toLowerCase().startsWith("admin ")) {
			return;
		}
		if (owner != null && owner.length() > 0) {
			if (c.getClan() == null) {

				ClanManager.joinClan(c, owner);

				// final String ownerFinal = owner;
				// player.sendMessage("Loading clan data...");
				// ClanManager.loadClan(ownerFinal);
				// CycleEventHandler.getSingleton().addEvent(player, new
				// CycleEvent() {
				//
				// @Override
				// public void stop() {
				//
				// }
				//
				// @Override
				// public void execute(CycleEventContainer container) {
				// Clan clan = /*Server.c*/ClanManager.getClan(ownerFinal);
				// if (clan != null) {
				// clan.addMember(player);
				// } else if (ownerFinal.equalsIgnoreCase(player.playerName)) {
				// /*Server.c*/ClanManager.create(player);
				// } else {
				// player.sendMessage(Misc.formatPlayerName(ownerFinal) + " has
				// not created a clan yet.");
				// }
				// player.getPA().setClanData();
				//
				// container.stop();
				//
				// }
				// }, 4);

			}
		}
	}

}
