package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault.Role;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.UseItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.net.packet.PacketType;

public class ItemOnPlayer implements PacketType {

	private static int getRandomRares() {
		int[] rares = {1038, 1040, 1042, 1044, 1048, 1046, 15741, 15742};
		return rares[(int) Math.floor(Math.random() * rares.length)];
	}

	public static void handleCrackers(Client c, int itemId, int playerId) {
		Client usedOn = (Client) PlayerHandler.players[playerId];
		if (!c.getItems().playerHasItem(itemId)) {
			return;
		}

		if (usedOn == null) {
			return;
		}

		if (usedOn.getItems().freeSlots() < 1) {
			c.sendMessage(
					"The other player doesn't have enough inventory space!");
			return;
		}

		final int randomItem = getRandomRares();

		c.getItems().deleteItemInOneSlot(962, 1);
		usedOn.getItems().addItem(randomItem, 1);
		usedOn.sendMessage("You got <col=ff>" + ItemProjectInsanity.getItemName(randomItem)
				+ "</col> from " + c.getNameSmartUp());
	}

	public static void handleGoldenCracker(Client c, int itemId) {

		if (c == null) {
			return;
		}

		if (!c.getItems().playerHasItem(itemId)) {
			return;
		}

		if (c.getItems().freeSlots() < 1) {
			c.sendMessage("You dont have enough inventory space!");
			return;
		}

		c.getItems().deleteItemInOneSlot(20083, 1);
		c.getItems().addItem(20084, 1);
		c.sendMessage("You got <col=ff>Golden hammer</col> from " + c.getNameSmartUp());
	}

	public static void handleHealingVial(Player c, int itemId, int playerId) {

		if (c == null) {
			return;
		}

		Client o = (Client) PlayerHandler.players[playerId];

		if (!c.getItems().playerHasItem(itemId)) {
			return;
		}

		if (!c.getBarbarianAssault().inArena()) {
			c.sendMessage(
					"You can only use this in the barbarian assault minigame.");
			return;
		}

		if (c.getBarbarianAssault().getRole() != Role.HEALER) {
			c.sendMessage("You need to have the role of healer to use this.");
			return;
		}

		if (c.getItems().playerHasItem(10545)) {
			c.getItems().deleteItemInOneSlot(10545, 1);
			c.getItems().addItem(10546, 1);
		} else if (c.getItems().playerHasItem(10544)) {
			c.getItems().deleteItemInOneSlot(10544, 1);
			c.getItems().addItem(10545, 1);
		} else if (c.getItems().playerHasItem(10543)) {
			c.getItems().deleteItemInOneSlot(10543, 1);
			c.getItems().addItem(10544, 1);
		} else if (c.getItems().playerHasItem(10542)) {
			c.getItems().deleteItemInOneSlot(10542, 1);
			c.getItems().addItem(10543, 1);
		}

		o.getPA().restorePlayer(false);

	}

	// private static int getRandomPhat() {
	// int[] phats = { 1046, 1046, 1046, 1046, 1046, 1046, 1046, 1046, 1046,
	// 1046, 1046, 1046, 1040, 1040, 1040, 1040, 1040, 1040, 1040,
	// 1040, 1040, 1044, 1044, 1044, 1044, 1044, 1044, 1038, 1038,
	// 1038 };
	// return phats[(int) Math.floor(Math.random() * phats.length)];
	// }
	//
	// private static int getRandomStuff() {
	// int[] phats = { 1965, 592 };
	// return phats[(int) Math.floor(Math.random() * phats.length)];
	// }

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		final int interfaceId = c.getInStream().readUnsignedShortA();
		final int playerId = c.getInStream().readUnsignedShort();
		final int itemId = c.getInStream().read3Bytes();
		final int itemSlot = c.getInStream().readLEShort();

		if (itemSlot < 0 || itemSlot >= c.playerItems.length) {
			return;
		}

		if (playerId < 0 || playerId >= Config.MAX_PLAYERS) {
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.isDead()) {
			return;
		}

		final Client o = (Client) PlayerHandler.players[playerId];
		if (o == null) {
			c.sendMessage("Player is offline.");
			return;
		}

		c.isIdle = false;
		
		int dist = 1;
		if (c.getMovement().isMovingOrHasWalkQueue()) {
			dist += 2;
		}
		
		c.walkingToMob = true;
		c.finalLocalDestX = c.finalLocalDestY = 0;
		c.destOffsetX = c.destOffsetY = 1;
		c.walkUpToPathBlockedMob = false;
		c.clickReachDistance = 1;

		if (PlayerConstants.goodDistance(o.getX(), o.getY(), c.getX(), c.getY(),
				dist) && canInteract(c.getCurrentLocation(), o.getCurrentLocation(), c.getDynamicRegionClip())) {
			c.face(o);
			UseItem.itemOnPlayer(c, interfaceId, o, itemId, itemSlot);
		} else {
			c.followId = o.getId();
			c.clickObjectType = 6;

			CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (o.disconnected) {
						c.sendMessage("Player is offline.");
						container.stop();
					}
					int dist = 1;
					if (c.getMovement().isMovingOrHasWalkQueue()) {
						dist += 2;
					}
					if (c.clickObjectType == 6 && PlayerConstants.goodDistance(o.getX(), o.getY(), c.getX(), c.getY(), dist) && canInteract(c.getCurrentLocation(), o.getCurrentLocation(), c.getDynamicRegionClip())) {
						UseItem.itemOnPlayer(c, interfaceId, o, itemId,
								itemSlot);
						container.stop();
					}
					if (c.clickObjectType < 6 || c.clickObjectType > 6) {
						container.stop();
					}
				}

				@Override
				public void stop() {
					c.followId = 0;
					c.clickObjectType = 0;
				}
			}, 1);

		}

	}
	
	public static boolean canInteract(Location player, Location other, DynamicRegionClip dynClip) {
		if (RegionClip.rayTraceBlocked(player.getX(), player.getY(), other.getX(), other.getY(), player.getZ(), true, dynClip))
			return false;
		return true;
	}
}
