package com.soulplay.net.packet.in;

import com.soulplay.content.player.reflectioncheck.ReflectionCheckEntry;
import com.soulplay.content.player.reflectioncheck.ReflectionCheckManager;
import com.soulplay.content.player.reflectioncheck.ReflectionCheckResponseType;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.PacketType;

public class ReflectionCheckResponse implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		if (!c.getInStream().crcCheck(packetSize)) {
			c.reflectionCheckErrorId = ReflectionCheckResponseType.FAILED_CRC_CHECK.getType();
			return;
		}

		int uid = c.getInStream().readInt();
		int type = c.getInStream().readByte();
		ReflectionCheckResponseType response = ReflectionCheckResponseType.values.getOrDefault(type, ReflectionCheckResponseType.UNKNOWN_RESPONSE);

		c.reflectionCheckErrorId = response.getType();

		ReflectionCheckEntry entry = ReflectionCheckManager.getEntryByUid(uid);

		if (entry == null) {
			c.reflectionCheckErrorId = ReflectionCheckResponseType.UNKNOWN_REFLECTION_CHECK_ENTRY.getType();
			return;
		}

		Player sender = entry.getSender();

		if (response != ReflectionCheckResponseType.SUCCESS) {
			if (sender != null) {
				sender.sendMessage("Player " + c.getNameSmartUp() + " failed reflection check. Type was: " + type);
				sender.sendMessage(response.getResponseMessage());
			}

			return;
		}

		switch (entry.getType()) {
			case ReflectionCheckManager.REQUEST_METHOD_MOD:
			case ReflectionCheckManager.REQUEST_FIELD:
				int value = c.getInStream().readInt();

				if (sender != null) {
					sender.sendMessage(c.getNameSmartUp() + " requested field returned: " + value);
					sender.sendMessage(entry.getClassName() + ":" + entry.getEntryName());
				}
				break;
		}
	}

}
