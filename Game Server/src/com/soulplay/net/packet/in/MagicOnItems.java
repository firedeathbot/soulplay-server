package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Magic on items
 **/
public class MagicOnItems implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int slot = c.getInStream().readShort();
		int itemId = c.getInStream().read3Bytes();
		int inventoryId = c.getInStream().readShort();
		int spellId = c.getInStream().readUnsignedShortA();
		
		if (slot == 777 && itemId == 777) {
			c.forceClosedClient = true;
			// System.out.println("force closed ");
			return;
		}

		if (slot == 856 && itemId == 645) {
			if (inventoryId == 1)
				c.passedCheatClientTest = true;
			
//			System.out.println("Junk: "+Character.forDigit(junk, Character.MAX_RADIX));
//			System.out.println("my uuid:"+c.UUID);
//			char hiddenChar = Character.forDigit(junk, Character.MAX_RADIX);
//			if (c.UUID.charAt(7) != hiddenChar) {
//				Server.getSlackApi().call(SlackMessageBuilder.buildCheatClientMessage(c.getName()));
//			}
			
			// if (c.firstHackTest) {
			// c.firstHackTest = false;
			// } else {
			// long diff = System.currentTimeMillis() -
			// c.lastHackPacketReceived;
			// if (diff < 50000 || diff > 70000) {
			// Server.getSlackApi().call(SlackMessageBuilder.buildCheatClientMessage(c.getName()));
			//
			// }
			// }
			// c.lastHackPacketReceived = System.currentTimeMillis();
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (slot == 777 && itemId == 778) {
			c.safeDeath = true;
			c.disconnected = true;
			System.out.println("force closed from crash on t1 or t2 PlayerName:"
					+ c.getName());
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		c.isIdle = false;

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.isDead()) {
			return;
		}

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (slot < 0 || slot >= c.playerItems.length) {
			return;
		}

		if (!c.getItems().playerHasItem(itemId, slot, 1)) {
			return;
		}
		c.getPA().magicOnItems(slot, itemId, spellId);

	}

}
