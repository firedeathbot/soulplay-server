package com.soulplay.net.packet.in;

import com.soulplay.content.items.RunePouch;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.Smelting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.SmeltingDataDung;
import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.EquipmentClickExtension;

/**
 * Bank 10 Items
 **/
public class Bank10 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int interfaceId = c.getInStream().read3Bytes();
		int removeId = c.getInStream().read3Bytes();
		int removeSlot = c.getInStream().readUnsignedShortA();

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;
		
		if (c.debug) {
			c.sendMessage(String.format("Bank10 - interfaceId: %d removeId: %d removeSlot: %d", interfaceId, removeId, removeSlot));
		}

		c.interfaceId = interfaceId;
		if (RunePouch.handleRune(c, interfaceId, removeId, removeSlot, -1)) {
			return;
		}

		switch (interfaceId) {
			case GamblingInterface.SETUP_INV_ID:
				GamblingInterface.takeItem(c, 10, removeSlot);
				break;
			case GamblingInterface.SIDE_INV_ID:
				GamblingInterface.offerItem(c, 10, removeSlot);
				break;
			case 7423: {
				// prevent cheat client, 4465 is main deposit box interface
				if (c.interfaceIdOpenMainScreen != 4465) {
					return;
				}

				if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
					return;
				}

				if (c.playerItems[removeSlot] != removeId + 1) {
					return;
				}

				final int amount = c.getItems().deleteInventoryItem(removeId, 10);

				if (amount > 0) {
					c.getItems().addItemToBank(removeId, amount);
					c.getItems().resetItems(7423);
				}
			}
			break;

			case 51001:
				Smelting.startSmelt(c, SmeltingDataDung.barToData.get(removeId), 10);
				break;
			case 2006: // drop party chest deposit
				c.getPartyOffer().depositItem(removeSlot, 10);
				break;
			case 2274: // drop party chest withdraw
				c.getPartyOffer().withdrawItem(removeSlot, 10);
				break;
			case SmithingInterface.COLUMN_ONE_ID:
			case SmithingInterface.COLUMN_TWO_ID:
			case SmithingInterface.COLUMN_THREE_ID:
			case SmithingInterface.COLUMN_FOUR_ID:
			case SmithingInterface.COLUMN_FIVE_ID:
				Smithing.startSmithing(c, interfaceId, removeSlot, 10);
				break;
			case 1688:
				if (c.playerEquipment[removeSlot] != removeId) {
					return;
				}
				
				final PluginResult<EquipmentClickExtension> pluginResult = PluginManager.search(EquipmentClickExtension.class, removeId);
				final Player player = c;
				if (pluginResult.invoke(() -> pluginResult.extension.onOperateClick(player, removeId, removeSlot, interfaceId))) {
					return;
				}
				
				c.getPA().useOperate(removeId);
				break;
			case 44010:
			case 3900:
			case 67111:
				c.getShops().buyItem(removeId, removeSlot, 5);
				break;
			case 3823: // sell 5
				c.getShops().sellItem(removeId, removeSlot, 5);
				if (c.xInterfaceId == 7390) {
					return; // do not reset interface stuff cuz we sent a second
							// screen to enter price to sell for
				}
				break;
			case 5064:
				if (c.isBobOpen) {
					c.getSummoning().depositItem(removeId, removeSlot, 10);
					return;
				}
				if (!c.isBanking) {
					return;
				}
				if (c.inTrade) {
					c.getTradeAndDuel().declineTrade(/* true */);
					return;
				}
				if (c.toggleBot && c.getBot() != null) {
					c.getBot().getItems().bankItem(removeId, removeSlot, 10);
					c.getItems().resetInterfacesForToggleBot();
					return;
				}
				c.getItems().bankItem(removeId, removeSlot, 10);
				break;

			case 5382:
				if (c.isBanking) {
					c.getItems().fromBank(removeId, removeSlot, 10);
				}
				break;

			case 3322:
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.stakeItem(c, removeId, removeSlot, 10,
							interfaceId);
					return;
				}
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().tradeItem(removeId, removeSlot, 10);
				} else {
					if (c.duelStatus == 1 || c.duelStatus == 2) {
						c.getTradeAndDuel().stakeItem(removeId, removeSlot, 10);
					}
				}
				break;

			case 3415:
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().fromTrade(removeId, removeSlot, 10);
				}
				break;

			case 6669:
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.removeStakedItem(c, removeId, removeSlot, 10,
							interfaceId);
					break;
				}
				if (c.duelStatus == 1 || c.duelStatus == 2) {
					c.getTradeAndDuel().fromDuel(removeId, removeSlot, 10);
				}
				break;

			case 39502:
				if (c.isBobOpen) {
					c.getSummoning().withdrawItem(removeId, 10);
				}
				break;

		}

	}

}
