package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class IdleLogout implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		// if (!c.playerName.equalsIgnoreCase("Sanity"))
		// c.logout();

		// if(c.isIdle && !c.disconnected && (c.underAttackBy > 0 ||
		// c.underAttackBy2 > 0)) {
		// System.out.println("Kicking idle player in combat: "+c.playerName);
		// c.disconnected = true;
		// return;
		// }
		if (!c.correctlyInitialized || c.inVerificationState) {
			c.resetInCombatTimer();
			c.underAttackBy = c.underAttackBy2 = 0;
			c.saveFile = false;
			c.kickPlayer();
			return;
		}
		if (c.isIdle && !c.disconnected
				&& ((c.npcIndex > 0 && c.npcClickIndex == 0)
						|| c.playerIndex > 0)) {
			// c.getPA().movePlayer(Config.VARROCK_X, Config.VARROCK_Y, 0);
			// PlayerHandler.alertStaff("Possible afk trainer.
			// PlayerName:"+c.playerName);
			c.getCombat().resetPlayerAttack();
			return;
		}

		if (Config.RUN_ON_DEDI)
			c.isIdle = true;

		if (c.inCwGame && !Config.isOwner(c)) {
			CastleWars.removePlayerFromCw(c);
		}

	}

}