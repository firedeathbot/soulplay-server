package com.soulplay.net.packet.in;

import com.soulplay.content.items.RunePouch;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.Smelting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.SmeltingDataDung;
import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Bank 5 Items
 **/
public class Bank5 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int interfaceId = c.getInStream().read3Bytes();
		int itemId = c.getInStream().read3Bytes();
		int itemSlot = c.getInStream().readUnsignedLEShort();
		
		if (c.debug) {
			c.sendMessage("Bank5.java: interface:"+interfaceId+" itemId:"+itemId+" itemSlot:"+itemSlot);
		}

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		c.interfaceId = interfaceId;
		if (RunePouch.handleRune(c, interfaceId, itemId, itemSlot, 5)) {
			return;
		}

		switch (interfaceId) {
			case GamblingInterface.SETUP_INV_ID:
				GamblingInterface.takeItem(c, 5, itemSlot);
				break;
			case GamblingInterface.SIDE_INV_ID:
				GamblingInterface.offerItem(c, 5, itemSlot);
				break;
			case 7423: {
				// prevent cheat client, 4465 is main deposit box interface
				if (c.interfaceIdOpenMainScreen != 4465) {
					return;
				}

				if (itemSlot < 0 || itemSlot >= c.playerItems.length) {
					return;
				}

				if (c.playerItems[itemSlot] != itemId + 1) {
					return;
				}

				final int amount = c.getItems().deleteInventoryItem(itemId, 5);

				if (amount > 0) {
					c.getItems().addItemToBank(itemId, amount);
					c.getItems().resetItems(7423); }
			}
			break;

		case 51001:
			Smelting.startSmelt(c, SmeltingDataDung.barToData.get(itemId), 5);
			break;
		case 2006: // drop party chest deposit
			c.getPartyOffer().depositItem(itemSlot, 5);
			break;
		case 2274: // drop party chest withdraw
			c.getPartyOffer().withdrawItem(itemSlot, 5);
			break;

			case SmithingInterface.COLUMN_ONE_ID:
			case SmithingInterface.COLUMN_TWO_ID:
			case SmithingInterface.COLUMN_THREE_ID:
			case SmithingInterface.COLUMN_FOUR_ID:
			case SmithingInterface.COLUMN_FIVE_ID:
				Smithing.startSmithing(c, interfaceId, itemSlot, 5);
				break;

			case 67111:
			case 44010:
			case 3900:
				if (c.getStorageObject().withdraw(itemId, itemSlot, 1))
					break;
				int amount = 1;
				if (c.myShopClient != null && c.mySQLIndex == c.myShopClient.mySQLIndex)
					amount = -7;
				c.getShops().buyItem(itemId, itemSlot, amount);
				break;

			case 3823:
				if (c.getStorageObject().deposit(itemId, itemSlot, 1)) {
					break;
				}
				c.getShops().sellItem(itemId, itemSlot, 1);
				if (c.xInterfaceId == 7390) {
					return; // do not reset interface stuff cuz we sent a second
							// screen to enter price to sell for
				}
				break;

			case 5064:
				if (c.isBobOpen) {
					c.getSummoning().depositItem(itemId, itemSlot, 5);
					return;
				}
				if (!c.isBanking) {
					return;
				}
				if (c.inTrade) {
					c.getTradeAndDuel().declineTrade(/* true */);
					return;
				}
				if (c.toggleBot && c.getBot() != null) {
					c.getBot().getItems().bankItem(itemId, itemSlot, 5);
					c.getItems().resetInterfacesForToggleBot();
					return;
				}
				c.getItems().bankItem(itemId, itemSlot, 5);
				break;

			case 5382:
				// aadd check here and in other places using that Method.class
				// see that check is for trade but not banking
				if (!c.isBanking) {
					c.getPA().closeAllWindows();
					return;
				}
				c.getItems().fromBank(itemId, itemSlot, 5);
				break;

			case 3322:
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.stakeItem(c, itemId, itemSlot, 5, interfaceId);
					return;
				}
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().tradeItem(itemId, itemSlot, 5);
				} else {
					if (c.duelStatus == 1 || c.duelStatus == 2) {
						c.getTradeAndDuel().stakeItem(itemId, itemSlot, 5);
					}
				}
				break;

			case 3415:
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().fromTrade(itemId, itemSlot, 5);
				}
				break;

			case 6669: // TODO:: add dueling check to stop exploits
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.removeStakedItem(c, itemId, itemSlot, 5,
							interfaceId);
					break;
				}
				if (c.duelStatus == 1 || c.duelStatus == 2) {
					c.getTradeAndDuel().fromDuel(itemId, itemSlot, 5);
				}
				break;

			case 39502:
				if (c.isBobOpen) {
					c.getSummoning().withdrawItem(itemId, 5);
				}
				break;

		}
	}

}
