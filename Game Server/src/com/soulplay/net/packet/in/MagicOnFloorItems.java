package com.soulplay.net.packet.in;

import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.combat.magic.SpellsOfPI;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.entity.Location;
import com.soulplay.net.packet.PacketType;

public class MagicOnFloorItems implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		final int itemY = c.getInStream().readLEShort();
		final int itemId = c.getInStream().read3Bytes();
		final int itemX = c.getInStream().readLEShort();
		final int spellId = c.getInStream().readUnsignedShortA();

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		c.isIdle = false;

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

//		if (!ItemHandler.itemExistsNearbyRegions(c, itemId, itemX, itemY,
//				c.getHeightLevel())) {
//			c.getMovement().resetWalkingQueue();
//			return;
//		}

		if (c.isDead()) {
			return;
		}
		
		if (!c.getStopWatch().checkThenStart(1)) {
			c.sendMessage("Please slow down.");
			return;
		}

		c.walkingToItem = true;

		//TODO: add clipping check
		if (!PlayerConstants.goodDistance(c.getX(), c.getY(), itemX, itemY,
				12)) {
			return;
		}

		SpellsData spellData = SpellsData.getSpell(c, spellId);
		if (spellData == null) {
			return;
		}

		if (!MagicRequirements.checkMagicReqsNew(c, spellData, true)) {
			c.getMovement().resetWalkingQueue();
			return;
		}

		switch (spellData) {
			case TELEGRAB:
				final Location itemLoc = Location.create(itemX, itemY, c.getZ());

				if (c.getAttackTimer() <= 0 && c.getCurrentLocation().isWithinDeltaDistance(itemLoc, RSConstants.COMBAT_SPELL_DIST) && !RegionClip.rayTraceBlocked(c.getX(), c.getY(), itemX, itemY, c.getZ(), true, c.getDynamicRegionClip())) {
					SpellsOfPI.castTeleGrab(c, itemId, itemX, itemY, spellData);
					c.walkingToItem = false;
					c.destinationX = 0;
					c.destinationY = 0;
				} else {
					if (c.skillingEvent != null)
						c.resetSkillingEvent();
					c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

						@Override
						public void execute(CycleEventContainer container) {
							if (c.disconnected) {
								container.stop();
								return;
							}
								
							boolean projectileBlocked = RegionClip.rayTraceBlocked(c.getX(), c.getY(), itemX, itemY, c.getZ(), true, c.getDynamicRegionClip());
								
							boolean withinDistance = c.getCurrentLocation().isWithinDeltaDistance(itemLoc, RSConstants.COMBAT_SPELL_DIST); 
								
							if (c.getMovement().isMovingOrHasWalkQueue() && withinDistance && !projectileBlocked)
								c.getMovement().resetWalkingQueue();
							if (c.getAttackTimer() > 0)
								return;
							if (withinDistance && !projectileBlocked) {
								SpellsOfPI.castTeleGrab(c, itemId, itemX, itemY, spellData);
								c.walkingToItem = false;
								c.destinationX = 0;
								c.destinationY = 0;
								container.stop();
							}
						}

						@Override
						public void stop() {
						}
					}, 1);
				}
				break;
			default:
				break;
		}
	}

}
