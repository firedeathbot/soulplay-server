package com.soulplay.net.packet.in;

import com.soulplay.content.minigames.soulwars.Avatar;
import com.soulplay.content.player.ArmourSets;
import com.soulplay.content.player.pets.pokemon.Pokemon;
import com.soulplay.game.RSConstants;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.UseItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class ItemOnNpc implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		final int itemId = c.getInStream().read3Bytes();
		final int i = c.getInStream().readShortA();
		final int slot = c.getInStream().readLEShort();
		// final int npcId = NPCHandler.npcs[i].npcType;

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.getReceivedPacket()[packetType]) {
			return;
		}
		c.setReceivedPacket(packetType, true);

		if (slot < 0 || slot >= c.playerItems.length) {
			return;
		}

		// if (npcId < 0 || npcId > NPCHandler.maxNPCs)
		// return;

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.isDead()) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}
		
		if (i < 0 || i >= NPCHandler.npcs.length)
			return;

		final NPC npc = NPCHandler.npcs[i];

		if (npc == null || !npc.isActive) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (!c.getItems().playerHasItem(itemId, 1)) {
			return;
		}

		if (c.npcType == 8948) {
			ArmourSets.exchangeSets(c, itemId);
			return;
		}
		
		if (itemId == 9433) { // bolt pouch
			Pokemon.sendPetToAttack(c, npc, npc.npcIndex);
		}

		c.isIdle = false;
		
		int dist = 1;
		if (c.getMovement().isMovingOrHasWalkQueue()) {
			dist += 1;
		}
		
		c.itemOnNpcItemId = itemId;
		c.itemOnNpcItemSlot = slot;
		c.npcClickIndex = i;

		c.clickNpcType = 5;
		
		dist = UseItem.getDistance(c, itemId, dist);

		c.walkingToMob = true;
		c.finalLocalDestX = c.finalLocalDestY = 0;
		c.destOffsetX = c.destOffsetY = 1;
		c.walkUpToPathBlockedMob = false;
		c.clickReachDistance = 1;
		
		ClickNPC.setNpcClickOffset(c, npc);

	}
	
}
