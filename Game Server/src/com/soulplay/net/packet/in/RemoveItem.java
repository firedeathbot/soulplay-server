package com.soulplay.net.packet.in;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.event.randomevent.MagicalCrate;
import com.soulplay.content.items.RunePouch;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.player.skills.crafting.Moulding;
import com.soulplay.content.player.skills.dungeoneeringv2.Binds;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.Smelting;
import com.soulplay.content.player.skills.dungeoneeringv2.skills.smithing.SmeltingDataDung;
import com.soulplay.content.player.skills.smithing.Smithing;
import com.soulplay.content.player.skills.smithing.SmithingInterface;
import com.soulplay.content.raids.RaidsRewardHolder;
import com.soulplay.content.raids.RaidsRewards;
import com.soulplay.content.tob.rooms.TobRewardsInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.EquipmentClickExtension;

/**
 * Remove Item
 **/
public class RemoveItem implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		final int interfaceId = c.getInStream().read3Bytes();
		final int removeSlot = c.getInStream().readUnsignedShortA();
		final int removeId = c.getInStream().read3Bytes();

		if (!c.correctlyInitialized) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}
		
		if (c.debug) {
			c.sendMessage("interface:"+interfaceId+" removeId:"+removeId+" removeSlot:"+removeSlot);
		}

		c.isIdle = false;

		if (removeId < 0) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.isStunned()) {
			return;
		}

		if (c.teleportToX > 0) { // do not handle packets for players that are
									// being moved
			return;
		}

		if (c.isDead()) {
			return;
		}

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		c.interfaceId = interfaceId;
		if (RunePouch.handleRune(c, interfaceId, removeId, removeSlot, 1)) {
			return;
		}

		// if(c.playerName.equalsIgnoreCase("sha512")) {
		// System.out.println("RemoveItem - interfaceId:"+interfaceId+"
		// removeSlot:"+removeSlot+" removeId:"+removeId);
		// }

		switch (interfaceId) {
			case TobRewardsInterface.INVENTORY_ID:
				TobRewardsInterface.inventoryClick(c, removeSlot);
				break;
			case ItemRetrievalManager.INVENTORY_ID:
				ItemRetrievalManager.inventoryClick(c, removeSlot, true);
				break;
			case GamblingInterface.SETUP_INV_ID:
				GamblingInterface.takeItem(c, 1, removeSlot);
				break;
			case GamblingInterface.SIDE_INV_ID:
				GamblingInterface.offerItem(c, 1, removeSlot);
				break;
		case 67234:
			if (c.raidsManager == null || c.raidsManager.rewardsToClaim == null) {
				break;
			}
			
			RaidsRewardHolder rewards = c.raidsManager.rewardsToClaim.get(c.mySQLIndex);
			
			if (rewards == null) {
				break;
			}
			
			if (removeSlot >= rewards.raidsRewards.length) {
				break;
			}

			if (rewards.raidsRewards[removeSlot] == 0 || rewards.raidsRewardsN[removeSlot] == 0) {
				break;
			}

			int itemId = rewards.raidsRewards[removeSlot] - 1;
			ItemDef itemDef = ItemDef.forID(itemId);
			if (itemDef != null && itemDef.noteId != -1) {
				itemId = itemDef.noteId;
			}

			if (c.getItems().addItem(itemId, rewards.raidsRewardsN[removeSlot])) {
				rewards.raidsRewards[removeSlot] = 0;
				rewards.raidsRewardsN[removeSlot] = 0;
				
				if (rewards.raidsRewards[0] == 0 && rewards.raidsRewards[1] == 0 && rewards.raidsRewards[2] == 0) { //if emptied chest, remove light
					if (c.raidsManager != null && c.raidsManager.greatOlmManager != null) {
						c.getPacketSender().addObjectPacket(-1, c.raidsManager.greatOlmManager.chestLocation, 0, 0);
					}
				}

				RaidsRewards.refreshReward(c);
			}
			break;
		case 51001:
			Smelting.startSmelt(c, SmeltingDataDung.barToData.get(removeId), 1);
			break;
		case 29991:
		case 29975:
		case 29974:
			int index = Binds.interfaceToIndex(interfaceId);
			if (index == -1 || removeSlot < 0 || removeSlot > Binds.ITEMS_PER_ROW) {
				break;
			}

			c.getBinds().removeLoudout(index, removeSlot);
			c.getBinds().update();
			break;
		case 2006: // drop party chest deposit
			c.getPartyOffer().depositItem(removeSlot, 1);
			break;
		case 2274: // drop party chest withdraw
			c.getPartyOffer().withdrawItem(removeSlot, 1);
			break;
		
		case 39600:
		case 39601:
		case 39602:
		case 39603:
		case 39604:
		case 39605:
		case 39606:
		case 39607:
		case 39608:
		    
		    if (!c.getPOH().getCurrentRoom().isPresent()) {
			break;
		    }
		    
		    c.getPOH().spawnObject(interfaceId);
		    break;
		
			case 7423: {
				// prevent cheat client, 4465 is main deposit box interface
				if (c.interfaceIdOpenMainScreen != 4465) {
					return;
				}

				if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
					return;
				}

				if (c.playerItems[removeSlot] != removeId + 1) {
					return;
				}

				final int amount = c.getItems().deleteItemFromSlot(removeSlot, 1);

				if (amount > 0) {
					c.getItems().addItemToBank(removeId, amount);
					c.getItems().resetItems(7423);
				}
			}
				break;

			case 1688:
				if (removeSlot < 0 || removeSlot >= c.playerEquipment.length) {
					return;
				}
				if (c.openDuel || c.inTrade || c.isBanking) {
					// c.getPA().closeAllWindows();
					return;
				}
				if (c.duelRule[DuelRules.SAME_WEAPONS] && removeSlot == 3) {
					c.sendMessage(
							"Removing weapons has been disabled in this duel!");
					return;
				}
				if (MagicalCrate.dropMagicalCrateInWild(c)) {
					c.sendMessage("The crate was dropped on the ground!");
					return;
				}
				if (c.toggleBot && c.getBot() != null) {
					c.getItems().removeItemForBot(removeId, removeSlot);
					c.getItems().resetInterfacesForToggleBot();
					return;
				}
				if (c.playerEquipment[removeSlot] != removeId) {
					// System.out.println("Dupe attempt in removeItem() by
					// "+c.playerName);
					return;
				}
				if (c.toggledDfs) {
					c.toggledDfs = false;
				}
				
				final PluginResult<EquipmentClickExtension> pluginResult = PluginManager.search(EquipmentClickExtension.class, removeId);
				final Player player = c;
				if (pluginResult.invoke(() -> pluginResult.extension.onRemoveClick(player, removeId, removeSlot, interfaceId))) {
					return;
				}
				
				c.getItems().removeItem(removeId, removeSlot);
				break;

			case 4233: // ring mould interface
			case 4239: // necklace mould interface
			case 4245: // amulet mould interface
				if (removeSlot < 0 || removeSlot > 6) {
					return;
				}
				Moulding.mouldItem(c, removeId, 1);
				break;

			case 5064:
				if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
					return;
				}
				if (c.isBobOpen) {
					c.getSummoning().depositItem(removeId, removeSlot, 1);
					return;
				}
				if (c.toggleBot && c.getBot() != null) {
					c.getBot().getItems().bankItem(removeId, removeSlot, 1);
					c.getItems().resetInterfacesForToggleBot();
					return;
				}
				if (c.playerItems[removeSlot] != removeId + 1) {

					return;
				}
				if (!c.isBanking) {
					return;
				}
				c.getItems().bankItem(removeId, removeSlot, 1);
				break;

			case 5382:
				if (!c.isBanking) {
					return;
				}
				if (removeSlot < 0 || removeSlot > c.bankItems0.length) {
					return;
				}
				if (c.toggleBot) {
					c.getItems().fromBank(removeId, removeSlot, 1);
					break;
				}
				if (c.bankItems0[removeSlot] != removeId + 1
						&& c.bankItems1[removeSlot] != removeId + 1
						&& c.bankItems2[removeSlot] != removeId + 1
						&& c.bankItems3[removeSlot] != removeId + 1
						&& c.bankItems4[removeSlot] != removeId + 1
						&& c.bankItems5[removeSlot] != removeId + 1
						&& c.bankItems6[removeSlot] != removeId + 1
						&& c.bankItems7[removeSlot] != removeId + 1
						&& c.bankItems8[removeSlot] != removeId + 1) {
					// System.out.println("poop"+c.playerName);
					return;
				}
				c.getItems().fromBank(removeId, removeSlot, 1);
				break;

			case 67111:
			case 44010:
			case 3900:
				c.getShops().buyFromShopPrice(removeId, removeSlot);
				break;

			case 3823:
				c.getShops().sellToShopPrice(removeId, removeSlot);
				break;

			case 3322:
				if (removeSlot < 0 || removeSlot >= c.playerItems.length) {
					return;
				}
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.stakeItem(c, removeId, removeSlot, 1, interfaceId);
					return;
				}
				if (!c.canOffer) {
					return;
				}
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().tradeItem(removeId, removeSlot, 1);
				} else {
					if (c.duelStatus == 1 || c.duelStatus == 2) {
						c.getTradeAndDuel().stakeItem(removeId, removeSlot, 1);
					}
				}
				break;

			case 3415:
				if (!c.canOffer) {
					return;
				}
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().fromTrade(removeId, removeSlot, 1);
				}
				break;

			case 6669:
				// c.sendMessage("removeSlot "+removeSlot);
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.removeStakedItem(c, removeId, removeSlot, 1,
							interfaceId);
					break;
				}
				if (c.duelStatus == 1 || c.duelStatus == 2) {
					c.getTradeAndDuel().fromDuel(removeId, removeSlot, 1);
				}
				break;

			case SmithingInterface.COLUMN_ONE_ID:
			case SmithingInterface.COLUMN_TWO_ID:
			case SmithingInterface.COLUMN_THREE_ID:
			case SmithingInterface.COLUMN_FOUR_ID:
			case SmithingInterface.COLUMN_FIVE_ID:
				Smithing.startSmithing(c, interfaceId, removeSlot, 1);
				break;

			case 39502:
				if (c.isBobOpen) {
					c.getSummoning().withdrawItem(removeId, 1);
				}
				break;

		}
	}

}
