package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class Report implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| !c.correctlyInitialized) {
			return;
		}
		if (c.logoutOnVerificationState())
			return;
		try {
			ReportHandler.handleReport(c, packetType);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
