package com.soulplay.net.packet.in;

import com.soulplay.content.player.skills.Cooking;
import com.soulplay.content.player.skills.herblore.Herblore;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Dialogue
 **/
public class DialoguePacket implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		int interfaceId = c.getInStream().readUnsignedShort();

		if (c.debug && c.playerRights == 3) {
			c.sendMessage("Dialogue action: " + interfaceId);
		}

		if (c.duelStatus == 3 || c.duelStatus == 4) {
			c.sendMessage("Stop this cheating!");
			return;
		}

		if (c.dialogueAction == 123) {
			c.getDH().sendOption2("Yes please. Show me around.",
					"No thanks, I'm familiar with this game.");
			return;
		}
		if (c.tourStep > 0 && c.tourStep < 100) {
			return;
		}
		if (c.getDialogueBuilder().isActive()) {
		    c.getDialogueBuilder().execute();
		    return;
		}
		if (c.nextChat > 0) {
			c.getDH().sendDialogues(c.nextChat, c.talkingNpc);
		} else {
			c.getDH().sendDialogues(0, -1);
		}
		
		
		if (c.getChatInterfaceId() > 0) {
			
			switch (c.getChatInterfaceId()) {
			case 1743: // cook interface
				c.doAmount = 28;
				Cooking.cook(c);
				break;
				
			case 4429: // potion making
				Herblore.handlePotionMaking(c, 28, c.herbloreItem1, c.herbloreItem2);
				break;
			}
			
		}
		
	}

}
