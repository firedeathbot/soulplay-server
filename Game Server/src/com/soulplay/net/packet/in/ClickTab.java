package com.soulplay.net.packet.in;

import com.soulplay.config.WorldType;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class ClickTab implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int tabIndex = c.getInStream().readUnsignedByte();

		if (c.debug) {
			c.sendMessage(String.format("[clickTab] index=%d", tabIndex));
		}

		if (!c.correctlyInitialized || c.inVerificationState) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3 || c.duelStatus == 4 || c.duelStatus == 6) {
			return;
		}

		c.isIdle = false;
		if (tabIndex == 13 && WorldType.isSeasonal()) {
			PowerUpInterface.open(c);
		}
	}

}
