package com.soulplay.net.packet.in;

import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Location;
import com.soulplay.net.packet.PacketType;

/**
 * Pickup Item
 **/
public class PickupItem implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| c.performingAction) {
			return;
		}
		final int itemY = c.pItemY = c.getInStream().readLEShort();
		final int itemId = c.pItemId = c.getInStream().read3Bytes();
		final int itemX = c.pItemX = c.getInStream().readLEShort();

		c.isIdle = false;

		if (Math.abs(c.getX() - itemX) > 25
				|| Math.abs(c.getY() - itemY) > 25) {
			c.sendMessage("I can't reach that.");
			c.getMovement().resetWalkingQueue();
			return;
		}
		if (itemId < 0) {
			// c.sendMessage("I can't reach that1.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.teleportToX > 0) { // do not handle packets for players that are
									// being moved
			return;
		}

		if (c.isDead()) {
			// c.sendMessage("I can't reach that2.");
			return;
		}

		if (c.isStunned()) {
			return;
		}

		if (!c.getController().canPickupItem()) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.duelStatus == 3 || c.duelStatus == 4) {
			c.sendMessage("Stop this cheating!");
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				c.sendMessage("I can't reach that3.");
				return;
			}
		}

		if (c.playerIsSkilling) {
			c.playerIsSkilling = false;
		}

		if (c.absX >= 9811 && c.absY >= 3181 && c.absX <= 9817 && c.absY <= 3186) {
			c.sendMessage("You cannot pickup items inside LMS lobby.");
			return;
		}
		
		switch (itemId) {
		case ResourceArea.PACKAGE_ITEM_ID:
			if (!ResourceArea.canLootChestBox(c)) {
				return;
			}
			break;
		}

		c.walkingToItem = true;

		c.getPA().closeAllWindows();
		c.getCombat().resetPlayerAttack();
		final Location itemLoc = Location.create(itemX, itemY, c.getZ());
		final boolean itemOnTable = (RegionClip.getClipping(itemLoc.getX(), itemLoc.getY(), itemLoc.getZ(), c.getDynamicRegionClip()) & 0x100) != 0;
		int dist = (itemOnTable || !c.getMovement().canMove()) ? 1 : 0;
		if (c.getCurrentLocation().isWithinDistance(itemLoc, dist) && ItemHandler.canPickupItem(itemLoc, c.getCurrentLocation(), c.getMovement().canMove(), c.getDynamicRegionClip())) {
			pickup(c, itemOnTable, itemId, itemLoc);
		} else {
			if (c.skillingEvent != null)
				c.resetSkillingEvent();
			c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {
					if (c.getCurrentLocation().isWithinDistance(itemLoc, (itemOnTable || !c.getMovement().canMove() ? 1 : 0)) && ItemHandler.canPickupItem(itemLoc, c.getCurrentLocation(), c.getMovement().canMove(), c.getDynamicRegionClip())) {
						pickup(c, itemOnTable, itemId, itemLoc);
						container.stop();
					}
				}

				@Override
				public void stop() {
				}
			}, 1);
		}
	}

	private static void pickup(Client c, boolean itemOnTable, int itemId, Location itemLoc) {
		c.walkingToItem = false;
		if (itemOnTable) {
			c.startAnimation(832);
			c.faceLocation(itemLoc);
		}
		try {
			ItemHandler.removeGroundItem(c, itemId, itemLoc.getX(), itemLoc.getY(), itemLoc.getZ(), true);
		} catch (Exception e) {
			System.out.println("player:"+c.getName()+" location:"+ c.getCurrentLocation().toString() + " itemID:"+itemId+" itemX:"+itemLoc.getX()+" itemY:"+ itemLoc.getY());
			e.printStackTrace();
		}
		switch (itemId) {
			case DungeonConstants.GATESTONE:
				if (c.insideDungeoneering()) {
					c.resetGatestone();
				}
				break;
			case 2677:
				if (c.getTreasureTrailManager().hasTrail(ClueDifficulty.EASY)) {
					return;
				}

				c.getTreasureTrailManager().addClue(ClueDifficulty.EASY, false, false);
				break;
			case 2819:
				if (c.getTreasureTrailManager().hasTrail(ClueDifficulty.MEDIUM)) {
					return;
				}

				c.getTreasureTrailManager().addClue(ClueDifficulty.MEDIUM, false, false);
				break;
			case 2739:
				if (c.getTreasureTrailManager().hasTrail(ClueDifficulty.HARD)) {
					return;
				}

				c.getTreasureTrailManager().addClue(ClueDifficulty.HARD, false, false);
				break;
			case 19043:
				if (c.getTreasureTrailManager().hasTrail(ClueDifficulty.ELITE)) {
					return;
				}

				c.getTreasureTrailManager().addClue(ClueDifficulty.ELITE, false, false);
				break;
			case 19064:
				if (c.getTreasureTrailManager().hasTrail(ClueDifficulty.MASTER)) {
					return;
				}

				c.getTreasureTrailManager().addClue(ClueDifficulty.MASTER, false, false);
				break;
		}
	}

}
