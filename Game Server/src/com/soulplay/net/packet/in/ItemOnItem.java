package com.soulplay.net.packet.in;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.UseItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class ItemOnItem implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		final int usedWithSlot = c.getInStream().readUnsignedShort();
		final int itemUsedSlot = c.getInStream().readUnsignedShortA();
		c.getInStream().read3Bytes();//usedWithId
		c.getInStream().readUnsignedShort();//itemUsedInterfaceId
		c.getInStream().read3Bytes();//itemUsedId
		c.getInStream().readUnsignedShort();//usedWithInterfaceId

		if (itemUsedSlot < 0 || itemUsedSlot >= c.playerItems.length) {
			return;
		}

		if (usedWithSlot < 0 || usedWithSlot >= c.playerItems.length) {
			return;
		}

		if (c.getReceivedPacket()[packetType]) {
			return;
		}

		c.setReceivedPacket(packetType, true);

		if (!c.correctlyInitialized || !c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.logoutOnVerificationState())
			return;

		c.isIdle = false;

		if (c.performingAction) {
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.isDead()) {
			return;
		}

		final Item itemUsed = new Item(c.playerItems[itemUsedSlot] - 1, c.playerItemsN[itemUsedSlot], itemUsedSlot);
		final Item itemUsedWith = new Item(c.playerItems[usedWithSlot] - 1, c.playerItemsN[usedWithSlot], usedWithSlot);

		if (!c.getItems().playerHasItem(itemUsedWith.getId(), usedWithSlot, 1)
				|| !c.getItems().playerHasItem(itemUsed.getId(), itemUsedSlot, 1)) {
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		c.resetSkillingEvent();

		UseItem.ItemonItem(c, itemUsed.getId(), itemUsedWith.getId(), itemUsedSlot, usedWithSlot);

		final PluginResult<ItemOnItemExtension> pluginResult = PluginManager.search(ItemOnItemExtension.class, itemUsed.getId(), itemUsedWith.getId());
		pluginResult.invoke(() -> pluginResult.extension.onItemOnItem(c, itemUsed, itemUsedSlot, itemUsedWith, usedWithSlot));
	}

}
