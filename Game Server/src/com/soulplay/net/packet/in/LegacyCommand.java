package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;

public class LegacyCommand {

	String issuer;

	private int issuerMID;

	String command;

	public LegacyCommand(Client issuer, String command) {
		this.issuer = issuer.getName();
		this.command = command;
		this.setIssuerMID(issuer.mySQLIndex);
	}

	public String getClientName() {
		return issuer;
	}

	public String getCommand() {
		return command;
	}

	public int getIssuerMID() {
		return issuerMID;
	}

	public void setIssuerMID(int issuerMID) {
		this.issuerMID = issuerMID;
	}
}
