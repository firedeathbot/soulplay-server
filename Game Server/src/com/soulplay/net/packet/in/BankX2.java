package com.soulplay.net.packet.in;

import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.items.impl.FairyRing;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.clanwars.ClanDuel;
import com.soulplay.content.minigames.gamble.GamblingInterface;
import com.soulplay.content.player.MoneyPouch;
import com.soulplay.content.shops.PlayerOwnedShops;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.net.Connection;
import com.soulplay.net.packet.PacketType;

/**
 * Bank X Items
 **/
public class BankX2 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		long longXAmount = Math.max(0, c.getInStream().readLong());

		// System.out.println("number:"+longXAmount);

		int xAmount = 0;
		if (longXAmount > Integer.MAX_VALUE) {
			xAmount = Integer.MAX_VALUE;
		} else {
			xAmount = (int) longXAmount;
		}

		if ((!c.correctlyInitialized || c.inVerificationState) && c.dialogueAction != 9998
				&& c.dialogueAction != 9999) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}

		// if(c.playerRights == 3)
		// c.sendMessage("xAmount"+Xamount);
		if (c.dialogueAction == 9999) {
			if (xAmount == 9654) {
				c.dialogueAction = 0;
				c.hasPassedNewUUIDTest();
			} else {
				c.resetInCombatTimer();
				c.underAttackBy = c.underAttackBy2 = 0;
				// c.saveFile = false;
				c.logout();
				c.disconnected = true;
				return;
			}
		}
		if (c.openBankPinInterface) {
			String sBankPin = "" + c.getBankPins()[0] + c.getBankPins()[1]
					+ c.getBankPins()[2] + c.getBankPins()[3];
			String enteredPin = "0000";
			if (xAmount > 999) {
				enteredPin = "" + xAmount;
			}
			if (xAmount < 1000) {
				enteredPin = "0" + xAmount;
			}
			if (xAmount < 100) {
				enteredPin = "00" + xAmount;
			}
			if (xAmount < 10) {
				enteredPin = "000" + xAmount;
			}
			
			c.getBankPin().bankPinEnterThroughText(sBankPin.equals(enteredPin));
			return;
		}
		if (c.inVerificationState/* c.dialogueAction == 9998 */) {
			String sBankPin = "" + c.getBankPins()[0] + c.getBankPins()[1]
					+ c.getBankPins()[2] + c.getBankPins()[3];
			String enteredPin = "0000";
			// System.out.println("bankPin:"+sBankPin);
			if (xAmount > 999) {
				enteredPin = "" + xAmount;
			}
			if (xAmount < 1000) {
				enteredPin = "0" + xAmount;
			}
			if (xAmount < 100) {
				enteredPin = "00" + xAmount;
			}
			if (xAmount < 10) {
				enteredPin = "000" + xAmount;
			}
			// System.out.println("Entered amount: "+enteredPin);
			// if (xAmount < 1000) {
			// if (sBankPin.equals(enteredPin)) {
			// c.dialogueAction = 0;
			// c.hidePlayer = false;
			// } else {
			// c.logoutDelay = 0;
			// c.underAttackBy = c.underAttackBy2 = 0;
			// c.saveFile = false;
			// c.logout();
			// }
			// } else {
			if (sBankPin.equals(enteredPin)) {
				c.hasPassedNewUUIDTest();
				// pvp world cheaphax
				if (c.addStarter) {
					if (WorldType.equalsType(WorldType.SPAWN)) {
						c.difficulty = PlayerDifficulty.NORMAL;
						c.setMode = true;
						c.sendTourGuide();
						
					} else {
						c.getDH().sendDialogues(939, 945);
					}
				}

			} else {

				// TODO: add player to 3 times limit per hour thing here
				Connection.addToBankPinAttemptFailed(c.getName());

				c.resetInCombatTimer();
				c.underAttackBy = c.underAttackBy2 = 0;
				// c.saveFile = false;
				c.disconnected = true;
				c.logout();
			}
			// }
		}
		if (c.dialogueAction >= 11110 && c.dialogueAction <= 11116) {
			if (WorldType.isSeasonal()) {
				c.sendMessage("This feature is disabled for seasonal world.");
				return;
			}

			for (int element : c.playerEquipment) {
				if (element > 0) {
					c.getPA().closeAllWindows();
					c.sendMessage(
							"Please take all your armour and weapons off before using this command.");
					return;
				}
			}
			if (c.absX == 3087 && c.absY == 3502) {
				c.getPA().closeAllWindows();
				c.sendMessage("You cannot change your skill in this spot.");
				return;
			}
			if (c.donationSkillReset) {
				final int DONPOINT_AMOUNT = 5;
				if (c.getDonPoints() < DONPOINT_AMOUNT) {
					c.getPA().closeAllWindows();
					c.sendMessage("You need " + DONPOINT_AMOUNT
							+ " Donor Points to set a skill.");
					return;
				} else {
					c.setDonPoints(c.getDonPoints() - DONPOINT_AMOUNT, true);
				}
			} else {
				final int LOYALTY_AMOUNT = 200;
				if (c.getLoyaltyPoints() < LOYALTY_AMOUNT) {
					c.getPA().closeAllWindows();
					c.sendMessage("You need " + LOYALTY_AMOUNT
							+ " Loyalty Points to set a skill.");
					return;
				} else {
					c.setLoyaltyPoints(c.getLoyaltyPoints() - LOYALTY_AMOUNT);
				}
			}
			if (xAmount <= 0) {
				xAmount = 1;
			}
			if (xAmount > 99) {
				xAmount = 99;
			}
			c.dialogueAction -= 11110;
			// if (c.dialogueAction == 3)
			// if (xAmount < 10)
			// xAmount = 10;
			if (xAmount < 10 && c.dialogueAction == 3) { // Constitution fix
				xAmount = 10;
			}
			// c.sendMessage(c.dialogueAction+"");
			c.getSkills().setStaticLevel(c.dialogueAction, xAmount);
			c.calcCombat();

			c.dialogueAction = 0;
			c.getPA().closeAllWindows();
			return;
		}

		if (c.getDialogueBuilder().isActive()) {
			c.enterAmount = longXAmount;
			c.getDialogueBuilder().executeEnterAmount();
			return;
		}
		
		// c.sendMess("id:"+c.sellingId+" sellingN:"+c.sellingN+"
		// interface:"+c.xInterfaceId);
		if (c.myShopClient != null) {
			if (c.myShopClient.equals(c)) { // handling own shop
				if (c.xInterfaceId == 3823) { // selling to own shop
					ItemDef def = ItemDef.forID(c.sellingIdOriginal);
					if (def != null && def.itemIsInNotePosition) {
						c.sellingId = def.getNoteId();
					}
					long price = PlayerOwnedShops.getPriceOfShopItem(c,
							c.sellingId);
					// c.sendMess("price:"+price);
					if (price > 0) {
						PlayerOwnedShops.restockMyShop(c, xAmount, price); // puts
																			// more
																			// items
																			// into
																			// shop,
																			// stacking
																			// with
																			// existing
																			// item
					} else {
//						if (!c.getMarketOffer().isActive())
//							PlayerOwnedShops.placeFirstItemWithSetPrice(c, longXAmount); // places the first item into shop and sets the price
					}
				} else if (c.xInterfaceId == 3900) { // buying from own shop
					c.getShops().buyFromPOS(c.sellingId, c.sellingS, xAmount);
				}
			} else { // handle other shop
				c.getShops().buyFromPOS(c.sellingId, c.sellingS, xAmount);
			}
			return;
		}

		if (c.dialogueAction == 9985) { // fairy ring teleport input
			FairyRing.ringTele(c, xAmount);
			c.getPA().resetXAmountInfo();
			return;
		}
		if (c.dialogueAction == 929292) { // takex from sw bandage table
			c.getItems().addItem(CastleWars.BANDAGES, xAmount);
			c.startAnimation(832);
			c.getPA().resetXAmountInfo();
			return;
		}
		if (c.dialogueAction == 939393) { // takex from soulwars potion table
			c.getItems().addItem(CastleWars.EXPLOSIVE_POTION, xAmount);
			c.startAnimation(832);
			c.getPA().resetXAmountInfo();
			return;
		}

		// c.sendMessage("interfaceID"+c.interfaceId);
		if (xAmount < 0)// this should work fine
		{
			xAmount = c.getItems().getItemAmount(c.xRemoveId);
		}
		if (xAmount == 0) {
			xAmount = 1;
		}

		switch (c.xInterfaceId) {
			case GamblingInterface.SETUP_INV_ID:
				GamblingInterface.takeItem(c, xAmount, c.xRemoveSlot);
				break;
			case GamblingInterface.SIDE_INV_ID:
				GamblingInterface.offerItem(c, xAmount, c.xRemoveSlot);
				break;
			case 7423: {
				// prevent cheat client, 4465 is main deposit box interface
				if (c.interfaceIdOpenMainScreen != 4465) {
					return;
				}

				if (c.xRemoveSlot < 0 || c.xRemoveSlot >= c.playerItems.length) {
					return;
				}

				if (c.playerItems[c.xRemoveSlot] != c.xRemoveId + 1) {
					return;
				}

				final int amount = c.getItems().deleteInventoryItem(c.xRemoveId, xAmount);

				if (amount > 0) {
					c.getItems().addItemToBank(c.xRemoveId, amount);
					c.getItems().resetItems(7423);
				}
			}
			break;
		
		case 2006: // drop party chest deposit
			c.getPartyOffer().depositItem(c.xRemoveSlot, xAmount);
			break;
		case 2274: // drop party chest withdraw
			c.getPartyOffer().withdrawItem(c.xRemoveSlot, xAmount);
			break;

			case 3214: // item inventory
				if (c.xRemoveId == 995) {// coins to moneypouch item click
					if (!c.canWithDrawOrDepositMoneyPouch()) {
						c.sendMessage("You cannot do that here.");
						return;
					}

					if (xAmount < 0) {
						xAmount = 0;
					}

					int freeSlots = c.getItems().freeSlots();
					long currentGoldAmount = c.getItems().getItemCount(995);
					if (xAmount > c.getMoneyPouch() && c.getMoneyPouch() < Integer.MAX_VALUE) {
						xAmount = (int) c.getMoneyPouch();
					}

					if (freeSlots == 0 && currentGoldAmount <= 0) {
						c.sendMessage("You do not have enough space in your inventory.");
						return;
					}

					if (currentGoldAmount > 0 && currentGoldAmount + xAmount >= Integer.MAX_VALUE) { // player has gold
						c.sendMessage("You have too much gold in your inventory.");
						return;
					}

					MoneyPouch.removeFromMoneyPouch(c, xAmount);
					c.getItems().addItem(995, xAmount);
					return;
				}

				break;

			case 3823: // SellX amount to shop
				c.getShops().sellItem(c.xRemoveId, c.xRemoveSlot, xAmount);
				if (c.xInterfaceId == 7390) {
					return; // do not reset interface stuff cuz we sent a second
							// screen to enter price to sell for
				}
				break;
			case 3900: // buyX amount from shop
				c.getShops().buyItem(c.xRemoveId, c.xRemoveSlot, xAmount);// buy
																			// 100
				break;
			case 5064:
				if (c.isBobOpen) {
					c.getSummoning().depositItem(c.xRemoveId, c.xRemoveSlot,
							xAmount);
					c.getPA().resetXAmountInfo();
					return;
				}
				if (!c.isBanking) {
					c.getPA().resetXAmountInfo();
					return;
				}
				if (c.inTrade) {
					c.getTradeAndDuel().declineTrade(/* true */);
					c.getPA().resetXAmountInfo();
					return;
				}
				if (c.toggleBot && c.getBot() != null) {
					c.getBot().getItems().bankItem(
							c.getBot().playerItems[c.xRemoveSlot] - 1,
							c.xRemoveSlot, xAmount);
					c.getItems().resetInterfacesForToggleBot();
					c.getPA().resetXAmountInfo();
					return;
				}
				c.getItems().bankItem(c.xRemoveId, c.xRemoveSlot, xAmount);
				break;

			case 5382:
				if (!c.isBanking) {
					c.getPA().resetXAmountInfo();
					return;
				}
				c.interfaceId = 5383;

				if (c.toggleBot) {
					if (xAmount < 0) {
						xAmount = c.getBot().getItems()
								.getItemAmount(c.xRemoveId);
					}
					if (xAmount == 0) {
						xAmount = 1;
					}
					c.getItems().fromBank(c.getBot().bankItems0[c.xRemoveSlot],
							c.xRemoveSlot, xAmount);
					c.getPA().resetXAmountInfo();
					return;
				}

				c.getItems().fromBank(c.bankItems0[c.xRemoveSlot],
						c.xRemoveSlot, xAmount);

				break;

			case 3322:
				// if (!c.getItems().playerHasItem(c.xRemoveId, xAmount)) {
				// c.getPA().resetInterfaceInfo();
				// return;
				// }
				// c.sendMessage("amount "+xAmount);
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.stakeItem(c, c.xRemoveId, c.xRemoveSlot, xAmount,
							c.xInterfaceId);
					break;
				}
				if (c.duelStatus <= 0) {
					if (xAmount > c.getItems().getItemAmount(c.xRemoveId)) {
						c.getTradeAndDuel().tradeItem(c.xRemoveId,
								c.xRemoveSlot,
								c.getItems().getItemAmount(c.xRemoveId));
					} else {
						c.getTradeAndDuel().tradeItem(c.xRemoveId,
								c.xRemoveSlot, xAmount);
					}
				} else {
					if (!c.getItems().playerHasItem(c.xRemoveId, c.xRemoveSlot,
							xAmount)) {
						xAmount = c.getItems().getItemAmount(c.xRemoveId);
						// c.sendMessage("amount "+xAmount);
						if (xAmount == 0) {
							c.getPA().resetXAmountInfo();
							return;
						}
					}
					// c.sendMessage("item "+c.xRemoveId);
					if (xAmount > c.getItems().getItemAmount(c.xRemoveId)) {
						if (c.duelStatus == 1 || c.duelStatus == 2) {
							c.getTradeAndDuel().stakeItem(c.xRemoveId,
									c.xRemoveSlot,
									c.getItems().getItemAmount(c.xRemoveId));
						}
					} else {
						if (c.duelStatus == 1 || c.duelStatus == 2) {
							c.getTradeAndDuel().stakeItem(c.xRemoveId,
									c.xRemoveSlot, xAmount);
						}
					}
				}
				break;

			case 3415:
				if (!c.getItems().playerHasItem(c.xRemoveId, xAmount)) {
					c.getPA().resetXAmountInfo();
					return;
				}
				if (c.duelStatus <= 0) {
					c.getTradeAndDuel().fromTrade(c.xRemoveId, c.xRemoveSlot,
							xAmount);
				}
				break;

			case 6669:
				// if (!c.getItems().playerHasItem(c.xRemoveId, xAmount)) {
				// c.getPA().resetInterfaceInfo();
				// return;
				// }
				if (c.getDuelingClanWarClient() != null) {
					ClanDuel.removeStakedItem(c, c.xRemoveId, c.xRemoveSlot,
							xAmount, c.xInterfaceId);
					break;
				}
				if (c.duelStatus == 1 || c.duelStatus == 2) {
					c.getTradeAndDuel().fromDuel(c.xRemoveId, c.xRemoveSlot,
							xAmount);
				}
				break;

			case 39502:
				if (c.isBobOpen) {
					c.getSummoning().withdrawItem(c.xRemoveId, xAmount);
				}
				break;

		}

		c.getPA().resetXAmountInfo();
	}
}
