package com.soulplay.net.packet.in;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.content.player.pets.Pet;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.net.Connection;
import com.soulplay.net.packet.PacketType;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

/**
 * Chat
 **/
public class Chat implements PacketType {

	public static List<String> spammingPlayers = new ArrayList<>();

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		if (Connection.isMuted(c)) {
			c.sendMessage("You are muted for breaking a rule.");
			return;
		}

		int chatTextEffects = c.getInStream().readUnsignedByteS();
		if (chatTextEffects < 0) {
			chatTextEffects = 0;
		}
		int chatTextColor = c.getInStream().readUnsignedByteS();
		if (chatTextColor < 0) {
			chatTextColor = 0;
		}

		c.setChatTextEffects(chatTextEffects);
		c.setChatTextColor(chatTextColor);
		c.setChatTextSize((byte) Math.min(80, (packetSize - 2)));
		c.getInStream().readBytes_reverseA(c.getChatText(), c.getChatTextSize(), 0);

		if (c.getReceivedPacket()[packetType]) {
			return;
		}
		c.setReceivedPacket(packetType, true);

		if (!c.correctlyInitialized || c.inVerificationState) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		c.isIdle = false;

		if (c.cantCommunicate()) {
			c.sendMessage("Please wait some time before chatting.");
			return;
		}

		// System.out.println("text is "+Misc.textUnpack(c.getChatText(),
		// packetSize -2));

		if (c.getTimedMute() > 0) {
			c.sendMessage("You are muted for breaking a rule.");
			return;
		}

		String text = Misc.textUnpack(c.getChatText(), c.getChatTextSize());

		ReportHandler.addText(c.getName(), text);

		text = text.replaceAll("<.+?>", ""); // remove all <img > etc
		if (Misc.isBad(text)) {
			PlayerHandler.alertStaff("Possible advertiser: " + c.getName(), "Text: " + text);
			c.getPA().spamWarning();
			if (!Connection.isMuted(c)) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildAdvertiserMessage(c.getName(), text, "Public"));
			}
		}
		
		if (c.hasPet()) {
			if (c.summonedPokemon != null) {
				c.summonedPokemon.data.textCommand(c.summoned, text);
			} else {
				Pet pet = Pet.getPetForNPC(c.summoned.npcType);
				if (pet != null) {
					pet.textCommand(c.summoned, text);
				}
			}
		}
		
		if (!c.isIdle && text.equals("afk")) {
			c.sendMessage("Set status to AFK.");
			c.isIdle = true;
		}

		if (Connection.isMuted(c)) {
			c.sendMessage("You are muted for breaking a rule.");
			return;
		}

		c.getUpdateFlags().add(UpdateFlag.CHAT);
	}
}
