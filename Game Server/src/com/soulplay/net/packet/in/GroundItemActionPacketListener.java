package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class GroundItemActionPacketListener implements PacketType {

    @Override
    public void processPacket(Client c, int packetType, int packetSize) {
        final int x = c.getInStream().readUnsignedShortA();
        final int y = c.getInStream().readUnsignedLEShort();
        final int itemId = c.getInStream().read3Bytes();

        if (c.debug && c.playerRights == 3) {
            c.sendMessage(String.format("[GroundItemAction#1] x=%d y=%d itemId=%d", x, y, itemId));
        }

    }

}
