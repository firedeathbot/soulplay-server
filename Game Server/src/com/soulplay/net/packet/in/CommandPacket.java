package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.commands.AdminCommands;
import com.soulplay.content.player.commands.Command;
import com.soulplay.content.player.commands.CommandLogEntry;
import com.soulplay.content.player.commands.CommandParser;
import com.soulplay.content.player.commands.GoldDonatorCommands;
import com.soulplay.content.player.commands.InformerCommands;
import com.soulplay.content.player.commands.LegendaryDonatorCommands;
import com.soulplay.content.player.commands.ModCommands;
import com.soulplay.content.player.commands.SemiAdminCommands;
import com.soulplay.content.player.commands.OwnerCommands;
import com.soulplay.content.player.commands.PlayerCommands;
import com.soulplay.content.player.commands.RegularDonatorCommands;
import com.soulplay.content.player.commands.SuperDonatorCommands;
import com.soulplay.content.player.tracking.GrabUUID;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.Connection;
import com.soulplay.net.packet.PacketType;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

public final class CommandPacket implements PacketType {

	private static final Command[] commands = {new PlayerCommands(), new RegularDonatorCommands(),
			new SuperDonatorCommands(), new GoldDonatorCommands(), new LegendaryDonatorCommands(),
			new InformerCommands(), new ModCommands(), new SemiAdminCommands(), new AdminCommands(),
			new OwnerCommands()};

	public enum Punishment {
		BAN_NAME(0, "Account Banned"), BAN_IP(1, "IP Banned"), MUTE_NAME(2, "Account Muted"), MUTE_IP(3,
				"IP Muted"), BAN_UUID(4, "MAC Banned"), MUTE_UUID(5, "MAC Muted"), UNBAN_NAME(6,
						"Unbanned Account"), UNBAN_IP(7, "Unbanned IP"), UNMUTED_NAME(8, "Unmuted Account"), UNMUTED_IP(
								9, "Unmuted IP"), UNBAN_UUID(10, "Unbanned MAC"), UNMUTE_UUID(11, "Unmuted MAC");

		private final int punishmentType;

		private final String desc;

		Punishment(int type, String description) {
			this.punishmentType = type;
			this.desc = description;
		}

		public String getDesc() {
			return desc;
		}

		public int getPunishmentType() {
			return punishmentType;
		}
	}

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {

		final String realCommand = c.getInStream().readString();
		final String playerCommand = realCommand.toLowerCase();

		if (playerCommand.startsWith("lmku")) {
			try {
				String uuid = realCommand.substring(5);
				if (uuid != null && !uuid.isEmpty()) {
					GrabUUID.hasRequest(c.getName(), uuid);
				} else {
					GrabUUID.hasRequest(c.getName(), "faked his uuid!");
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("lmku not passed by playername:"+c.getName());
			}
			return;
		}
		
		if (!c.getController().canUseCommands()) {
		    return;
		}

		if ("detectnull".equals(playerCommand)) {
			c.setDetectNull(!c.isDetectNull());
			c.sendMessage("Null detect set to :" + c.isDetectNull());
			return;
		}

		if (!c.correctlyInitialized) {
			if (c.isDetectNull()) {
				c.sendMessage("Null1234");
			}
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			if (c.isDetectNull()) {
				c.sendMessage("Null738");
			}
			return;
		}

		if (c.isDead() && !c.isOwner()) {
			if (c.isDetectNull()) {
				c.sendMessage("dead234");
			}
			return;
		}

		c.isIdle = false;

		if (c.noClip) {
			// if(c.noClipX > 0 || c.noClipY > 0) {
			// return;
			// }
			if (c.isDetectNull()) {
				c.sendMessage("Null896");
			}
		}

		if (c.getReceivedPacket()[packetType]) { // new spam protection method
			if (c.isDetectNull()) {
				c.sendMessage("Null777");
			}
			return;
		}
		c.setReceivedPacket(packetType, true);
		
		if (c.isLockActions()) {
			c.sendMessage("Wait before you do that.");
			return;
		}

		if (Config.SERVER_DEBUG) {
			Misc.println(c.getName() + " playerCommand: " + playerCommand);
		}

		if (!c.isDev() && (1 == c.duelStatus || 2 == c.duelStatus || 3 == c.duelStatus || 4 == c.duelStatus)) {
			if (c.isDetectNull()) {
				c.sendMessage("Null822");
			}
			return;
		}

		if (!c.getBarbarianAssault().canUseCommands()) {
			c.sendMessage("You can't use commands here.");
			return;
		}
		
		
		if (playerCommand.startsWith("//")) {
			PlayerHandler.handleYell(c, " " + CommandParser.split("//", playerCommand, " ").nextLine(), false);
			return;
		} else if (playerCommand.startsWith("/")) {
			if (Connection.isMuted(c)) {
				c.sendMessage("You are muted for breaking a rule.");
				return;
			}
			if (c.getTimedMute() > 0) {
				c.sendMessage("You are muted for breaking a rule.");
				return;
			}

			if (c.getClan() != null) {
				String message = Misc.escape(playerCommand.substring(1).trim());
				if (Misc.isBad(message)) {
					c.getPA().spamWarning();
					if (!Connection.isMuted(c)) {
						Server.getSlackApi().call(SlackMessageBuilder.buildAdvertiserMessage(c.getName(), message,
								"Clan (" + c.getClan().founder + ")"));
					}
				}
				c.getClan().sendChat(c, message);
			} else {
				c.sendMessage("You can only do this in a clan chat..");
			}
			return;
		}

		if (c.playerIsInHouse && !Config.isDev(c.getName()) && Config.RUN_ON_DEDI) {
			c.sendMessage("You cannot use commands in a player owned house.");
			return;
		}

		final CommandParser parser = CommandParser.split(playerCommand, " ");
		
//		System.out.println("CommandPacket:"+parser.getCommand() + " " + parser.toString());

		for (Command command : commands) {
			if (command.canAccess(c)) {
				try {
					if (command.execute(c, parser, realCommand)) {
						Server.getLogWriter().addToCommandList(new CommandLogEntry(c, playerCommand));
						break;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					c.sendMessage("Invalid syntax for command: " + parser.getCommand());
					break;
				}
			}
		}

	}

}
