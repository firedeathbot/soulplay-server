package com.soulplay.net.packet.in.action;

import com.soulplay.content.clans.Clan;
import com.soulplay.content.clans.ClanManager;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.login.RS2LoginProtocol;
import com.soulplay.net.packet.PacketType;
import com.soulplay.util.Misc;

public class ReceiveString implements PacketType {

	@Override
	public void processPacket(Client player, int packetType, int packetSize) {
		int type = player.getInStream().readUnsignedByte();
		String string = player.getInStream().readString();

		if (!player.lastAction.complete()) {
			return;
		}

		player.lastAction.startTimer(1);

		if (player.getClan() == null) {
			return;
		}

		// player.sendMessage("text:"+text+" index:"+index+" id:"+id+"
		// string:"+string);

		switch (type) {
			case 77:
//				System.out.println(
//						"Hacker name:" + player.getName() + " banned...");
//				Connection.addNameToBanList(player.getName());
//				player.kickPlayer();
				break;
			case 0: // leave chat button on sidebar
				if (player.getClan() != null) {
					if (player.raidsManager != null) {
						RaidsManager.leaveRaidDialog(player);
						return;
					} else if (player.tobManager != null) {
						TobManager.leaveTobDialog(player);
						return;
					}

					player.getClan().removeMember(player);
					player.lastClanChat = "";
				}
				break;
			case 1: // Change clan name
				if (player.getClan() == null) {
					return;
				}
				if (string.length() == 0) {
					break;
				} else if (string.length() > 15) {
					string = string.substring(0, 15);
				}
				Clan clan = player/* .getPA() */.getClan();
				if (clan != null) {
					if (!clan.isFounder(player.getName())) {
						player.sendMessage(
								"Only the owner of the clan can change clan name.");
						return;
					}
					if (!RS2LoginProtocol.validRegex(string)) {
						player.sendMessage(
								"Only letters and numbers allowed in Clan Title.");
						return;
					}

					clan.setTitle(string);
					player.getPacketSender().sendFrame126(clan.getTitle(),
							18306);
					clan.save();
				}
				break;
			case 2: // add ranked memeber
				if (string.length() == 0) {
					break;
				} else if (string.length() > 12) {
					string = string.substring(0, 12);
				}
				if (player.getClan().isFounder(string)) {
					player.sendMessage("Can't add owner to ranked list.");
					break;
				}
				if (string.equalsIgnoreCase(player.getName())) {
					break;
				}

				if (!player.getClan().isGeneralPlusRank(player.getName())) {
					return;
				}

				// if (!PlayerSave.playerExists(string)) {
				// player.sendMessage("This player doesn't exist!");
				// break;
				// }
				clan = player.getClan();
				if (clan == null) {
					player.sendMessage("You are not in a clan.");
					break;
				}
				if (clan.isBanned(string)) {
					player.sendMessage("You cannot promote a banned member.");
					break;
				}
				if (clan != null) {
					if (clan.setRank(Misc.formatPlayerName(string), 1)) {
						player.getPA().setClanData();
						clan.updateMembers();
						clan.save();
					} else {
						player.sendMessage("You cannot have more than "
								+ ClanManager.MAXIMUM_RANKED_MEMBERS
								+ " ranked clan members.");
					}
				}
				break;
			case 3: // add to banned members list
				if (string.length() == 0) {
					break;
				} else if (string.length() > 12) {
					string = string.substring(0, 12);
				}
				if (string.equalsIgnoreCase(player.getName())) {
					break;
				}
				if (!string.matches("[A-Za-z0-9 ]+")) {
					break;
				}
				if (string.endsWith(" ") || string.startsWith(" ")) {
					break;
				}
				if (string.contains("  ")) {
					break;
				}

				if (!player.getClan().isGeneralPlusRank(player.getName())) {
					return;
				}

				// if (!PlayerSave.playerExists(string)) {
				// player.sendMessage("This player doesn't exist!");
				// break;
				// }
				clan = player.getClan();
				if (clan == null) {
					player.sendMessage("You are not in a clan.");
					break;
				}
				if (!clan.canBan(player.getName())) {
					player.sendMessage(
							"You do not have sufficient rights do this.");
					break;
				}
				if (clan.isRanked(string)) {
					player.sendMessage("You cannot ban a ranked member.");
					break;
				}
				if (clan != null) {
					clan.banMember(Misc.formatPlayerName(string));
					player.getPA().setClanData();
					clan.save();
				}
				break;
			default:
				System.out.println("Received string: identifier=" + type
						+ ", string=" + string);
				break;
		}
	}

}
