package com.soulplay.net.packet.in.action;

import com.soulplay.content.clans.Clan;
import com.soulplay.content.clans.Clan.Rank;
import com.soulplay.content.items.presets.PresetsInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.content.clans.ClanManager;
import com.soulplay.content.clans.ClanMember;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.content.player.skills.dungeoneeringv2.packets.DungeonButtons;
import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.net.packet.PacketType;
import com.soulplay.util.Misc;

public class InterfaceAction implements PacketType {

	@Override
	public void processPacket(Client player, int packetType, int packetSize) {
		int id = player.getInStream().read3Bytes();
		int action = player.getInStream().readUnsignedByte();
		int component = player.getInStream().readUnsignedShort();
		if (component == 65535) {
			component = -1;
		}

		try {
			if (player.playerRights == 3) {
				Misc.println(player.getName() + " - id: " + id + " action: "
						+ action);
			}

			if (id < 0) {
				return;
			}
			if (action < 0) {
				return;
			}

			if (!player.lastAction.complete()) {
				return;
			}

			player.lastAction.startTimer(1);

			if (PowerUpInterface.handleOps(player, id, action, component)) {
				return;
			}

			if (PresetsInterface.handlePresetsList(player, id, component, action)) {
				return;
			}

			if (DungeonButtons.handleOp(player, id, action)) {
				return;
			}

			if (Party.handleOptions(player, id, action)) {
				return;
			}
			
			if (player.getClan() == null) {
				return;
			}

			if (!player.getClan().isGeneralPlusRank(player.getName())) {
				return;
			}

			switch (id) {
				case 18304: // change clan name
					if (action == 1) { // delete clan
						if (player.getClan() == null) {
							player.sendMessage("Clan doesn't exist.");
							return;
						}
						if (player.getClan().getClanWarId() != -1) {
							player.sendMessage(
									"Can't do that during ongoing clan wars.");
							return;
						}
						if (!player.getClan().isFounder(player.getName())) {
							player.sendMessage(
									"You must be owner of the clan to delete it.");
							return;
						}

						player.getClan().delete();
						player.getPA().setClanData();
						if (player.getClan() == null) {
							player.sendMessage("Your clan has been deleted.");
						}
					}
					break;
				case 18307: // who can enter chat?
				case 18310: // who can talk in chat?
				case 18313: // who can kick in chat?
				case 18316: // who can ban in chat?
				case 18580: // who can share loot?
					Clan clan = player.getClan(); // player.getPA().getClan();
					if (clan == null) {
						player.sendMessage("You are not in a clan.");
						return;
					}
					if (/* clan.isFounder(player.getName()) || */player
							.getClan().isGeneralPlusRank(player.getName())) {
						if (clan != null) {
							if (id == 18307) {
								clan.setRankCanJoin(action == 0 ? -1 : action);
							} else if (id == 18310) {
								clan.setRankCanTalk(action == 0 ? -1 : action);
							} else if (id == 18313) {
								clan.setRankCanKick(action == 0 ? -1 : action);
							} else if (id == 18316) {
								clan.setRankCanBan(action == 0 ? -1 : action);
							} else if (id == 18580) {
								clan.setWhoCanChangeClanShare(
										action == 0 ? -1 : action);
							}
							String title = "";
							if (id == 18307) {
								title = clan.getRankTitle(clan.whoCanJoin)
										+ (clan.whoCanJoin > Clan.Rank.ANYONE
												&& clan.whoCanJoin < Clan.Rank.OWNER
														? "+"
														: "");
							} else if (id == 18310) {
								title = clan.getRankTitle(clan.whoCanTalk)
										+ (clan.whoCanTalk > Clan.Rank.ANYONE
												&& clan.whoCanTalk < Clan.Rank.OWNER
														? "+"
														: "");
							} else if (id == 18313) {
								title = clan.getRankTitle(clan.whoCanKick)
										+ (clan.whoCanKick > Clan.Rank.ANYONE
												&& clan.whoCanKick < Clan.Rank.OWNER
														? "+"
														: "");
							} else if (id == 18316) {
								title = clan.getRankTitle(clan.whoCanBan)
										+ (clan.whoCanBan > Clan.Rank.ANYONE
												&& clan.whoCanBan < Clan.Rank.OWNER
														? "+"
														: "");
							} else if (id == 18580) {
								title = clan.getRankTitle(
										clan.whoCanChangeClanShare)
										+ (clan.whoCanChangeClanShare > Clan.Rank.ANYONE
												&& clan.whoCanChangeClanShare < Clan.Rank.OWNER
														? "+"
														: "");
							}
							player.getPacketSender().sendString(title, id + 2);
						}
					}
					break;

				default:
					// System.out.println("Interface action: [id=" + id
					// +",action=" +
					// action +"]");
					break;
			}

			/**
			 * Clan interface ranked members box
			 */
			if (id >= 18323 && id < 18423) {
				// player.sendMessage("rank
				// "+player.getClan().getRank(player.getName())+"
				// action:"+action);
				final Clan clan = player.getClan();
				if (clan != null && clan.rankedMembers != null
						&& !clan.rankedMembers.isEmpty()) {
					// int memberId = id - 18323;
					// if (memberId < 0 || memberId > clan.rankedMembers.size())
					// return; //TODO: -1 rankedMembers.size() i think
					final String member = clan.getMemberNameByInterfaceId(id); // clan.rankedMembers.get(id
																			// -
																			// 18323);
					if (member == null) {
						return;
					}
					switch (action) {
						case 0:
							player.getDialogueBuilder().sendOption("Demote " + member, ()->{
								if (player.getClan() != null) {
									if (player.getClan().demote(member)) {
										player.getPA().setClanData();
										clan.updateMembers();
									}
								}
								player.getPA().closeAllWindows();
							}, "Nevermind.", ()->{
								player.getDialogueBuilder().returnToCheckpoint(100);
							}).execute();
							
							break;
						default:
							if (action == 6) { // making general
								if (clan.getRank(
										player.getName()) <= Rank.GENERAL) {
									player.sendMessage("You cannot do that.");
									return;
								}
							}
							if (action == 7) {
								if (!clan.isFounder(player.getName())) {
									player.sendMessage("You cannot do that.");
									return;
								}
							}
							if (clan.setRank(member, action)) {
								player.getPA().setClanData();
								clan.updateMembers();
							} else {
								player.sendMessage("You cannot have more than "
										+ ClanManager.MAXIMUM_RANKED_MEMBERS
										+ " ranked clan members.");
							}
							break;
					}
				}
			}
			/**
			 * Clan interface banned members box
			 */
			if (id >= 18424 && id < 18524) {
				// Clan clan = player/*.getPA()*/.getClan();
				if (!player.getClan().isGeneralPlusRank(player.getName())) {
					player.sendMessage("You cannot do that.");
					return;
				}
				if (player.getClan() != null
						&& player.getClan().bannedMembers != null
						&& !player.getClan().bannedMembers.isEmpty()) {
					String member = player.getClan().bannedMembers
							.get(id - 18424);
					switch (action) {
						case 0:
							player.getClan().unbanMember(member);
							break;
					}
					player.getPA().setClanData();
				}
			}

			/**
			 * This is the side bar clan managing stuff.
			 */
			if (id >= 18144 && id < 18244) {
				final Clan clan = player.getClan();
				if (clan == null) {
					player.sendMessage("You are not in a clan.");
					return;
				}

				int index = id - 18144;
				if (index >= clan.activeMembers.size()) {
					return;
				}

				ClanMember member = clan.activeMembers.get(index);
				switch (action) {
					case 0: // Edit rank
						if (clan.isGeneralPlusRank(player.getName())) {
							player.getPacketSender().showInterface(18300);
						}
						break;
					case 1: // Kick
						if (member.getMysqlID() == player.mySQLIndex) {
							player.sendMessage("You can't kick yourself.");
							break;
						}

						if (!clan.canKick(player.getName())) {
							player.sendMessage("You do not have sufficient privileges to do this.");
							break;
						}

						clan.kickMember(member);
						break;
					case 2: // Ban
						if (member.getMysqlID() == player.mySQLIndex) {
							player.sendMessage("You can't ban yourself.");
							break;
						}
						if (!clan.canBan(player.getName())) {
							player.sendMessage("You do not have sufficient privileges to do this.");
							break;
						}
						if (clan.isRanked(member.getDisplayName())) {
							player.sendMessage("You cannot ban a ranked member.");
							break;
						}
						if (clan != null) {
							clan.banMember(Misc.formatPlayerName(member.getUsername()));
							player.getPA().setClanData();
							clan.updateMembers();
							clan.save();
						}
						break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
