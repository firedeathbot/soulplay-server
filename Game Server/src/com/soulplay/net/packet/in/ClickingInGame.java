package com.soulplay.net.packet.in;

import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

/**
 * Clicking in game
 **/
public class ClickingInGame implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		
		final int bitpacked = c.getInStream().readInt();
		
//		final int i1 = bitpacked >> 20;
//		final int i2 = bitpacked >> 19 & 1;
//		final int i3 = bitpacked & 0x7ffff;
		
//		System.out.println(String.format("i1:%d i2:%d i3:%d", i1, i2, i3));
		
		if (c.dialogueAction == 9999) {
			c.resetInCombatTimer();
			c.underAttackBy = c.underAttackBy2 = 0;
			c.saveFile = false;
			c.disconnected = true;
			c.logout();
		}

		if (c.inVerificationState/* c.dialogueAction == 9998 */) {
			c.getPacketSender().sendInputDialogState(7);
			c.dialogueAction = 9998;
			// c.logoutDelay = 0;
			// c.underAttackBy = c.underAttackBy2 = 0;
			// c.saveFile = false;
			// c.logout();
		}
		
		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Your player is currently busy.");
			return;
		}

		c.isIdle = false;

	}

}
