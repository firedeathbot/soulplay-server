package com.soulplay.net.packet.in;

import com.soulplay.cache.ItemDef;
import com.soulplay.config.WorldType;
import com.soulplay.content.items.DonationItems;
import com.soulplay.content.items.DonationItems.DonationConsumable;
import com.soulplay.content.items.clickitem.Potion;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.content.items.impl.DuellistsCap;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.minigames.barbarianassault.BarbarianAssault;
import com.soulplay.content.player.TeleportType;
import com.soulplay.content.player.combat.Hit;
import com.soulplay.content.player.skills.dungeoneeringv2.BindingData;
import com.soulplay.content.player.skills.hunting.ImplingJars;
import com.soulplay.content.player.skills.runecrafting.PouchType;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.info.AnimationsData;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.net.packet.PacketType;
import com.soulplay.plugin.PluginManager;
import com.soulplay.plugin.PluginResult;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class ItemAction3 implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int itemId = c.getInStream().read3Bytes();
		int slot = c.getInStream().readLEShortA();
		int interfaceId = c.getInStream().readLEShortA();
		if (c.isDead()) {
			return;
		}

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		if (c.noClip && (c.noClipX > 0 || c.noClipY > 0)) {
			return;
		}

		if (c.isStunned()) {
			return;
		}

		if (c.teleportToX > 0) { // do not handle packets for players that are
									// being moved
			return;
		}

		if (c.performingAction) {
			c.sendMessage("Performing action!");
			return;
		}

		if (c.isLockActions()) {
			c.sendMessage("Your player is currently busy..");
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (!c.getItems().playerHasItem(itemId, 1)) {
			return;
		}

		if (!WorldType.equalsType(WorldType.SPAWN)) {
			if (ImplingJars.openJar(c, itemId)) {
				return;
			}
		}
		
		if (c.logoutOnVerificationState())
			return;

		c.resetSkillingEvent();
		c.isIdle = false;

		final PluginResult<ItemClickExtension> pluginResult = PluginManager.search(ItemClickExtension.class, itemId);
		final Client player = c;
		if (pluginResult.invoke(() -> pluginResult.extension.onItemOption3(player, itemId, slot, interfaceId))) {
			return;
		}

		if (DuellistsCap.getCapForID(itemId) != null) {
			DuellistsCap.handleDialogues(c, DuellistsCap.OPTIONS,
					DuellistsCap.getCapForID(itemId));
			return;
		}

		if (CompletionistCape.handleCompletionistsCapeItemClick(c, itemId,
				CompletionistCape.OPTION_CUSTOMISE)) {
			return;
		}

		if (BarbarianAssault.writeRole(c, itemId)) {
			return;
		}

		switch (itemId) {
		case 30209:
			DonationItems.clickItem(c, DonationConsumable.CHEST_500);
			return;
		case 18342:
			if (c.getLawStaffCharges() == 0) {
				c.sendMessage("Your staff has no charges left.");
			} else {
				c.sendMessage("Your staff has " + c.getLawStaffCharges() + " charges left.");
			}
			break;
		case 18341:
			if (c.getNatureStaffCharges() == 0) {
				c.sendMessage("Your staff has no charges left.");
			} else {
				c.sendMessage("Your staff has " + c.getNatureStaffCharges() + " charges left.");
			}
			break;
		case 11283:
		case 11284:
		case 30336:
			if (c.dfsCharge == 1) {
				c.sendMessage("You have 1 charge left.");
			} else {
				c.sendMessage("You have " + c.dfsCharge + " charges left.");
			}
			break;
		case 7510:
		case 7509:
			if (System.currentTimeMillis() - c.anyDelay2 >= 500 && c.getSkills().getLifepoints() > 1) {
				int hpDeduct = Math.max(1, (int) Math.round(c.getHitPoints() * 0.1));
				c.startAnimation(Potion.DRINK_ANIM);
				c.getPacketSender().playSound(Sounds.ROCK_CAKE);
				c.getPacketSender().playSound(Sounds.PLAYER_SMALL_DAMAGE);
				c.dealDamage(new Hit(hpDeduct, 0, -1));
				c.forceChat("Ow! I nearly broke a tooth!");
				c.anyDelay2 = System.currentTimeMillis();
			}
			break;

		case 4566: // rubber chicken
			player.startAnimation(AnimationsData.CHICKEN_DANCE);
			break;
		
		case 15707:
			if (c.getPA().startTeleport(3450, 3698, 0, TeleportType.DUNG)) {
				c.sendMessage("You teleport to Daemonheim.");
			}
			break;
		
			case 12926: // toxic blowpipe check option
				StringBuilder str = new StringBuilder();
				if (c.getBlowpipeAmmoType() > 0) {
					String itemName = ItemConstants.getItemName(c.getBlowpipeAmmoType());
					if (!itemName.equals("Unarmed"))
						str.append("Darts: <col=ff>").append(itemName).append(" x ").append(c.getBlowpipeAmmo()).append("</col>. ");
				}
				str.append("Scales: ");
				str.append(c.getCharges(BlowPipeCharges.BLOWPIPE_FULL));
				c.sendMessage(str.toString());
				break;

			case 18338:
				if (c.getItems().playerHasItem(18338)) {
					c.getItems().deleteItemInOneSlot(18338, 1);
					c.getItems().addItem(1608, 40);
					c.getItems().addItem(1606, 30);
					c.getItems().addItem(1604, 20);
					c.getItems().addItem(1602, 8);
					c.sendMessage("You empty the gem bag.");
				}
				break;
				
			case 5509:
			    c.getRunecrafting().getPouch(PouchType.SMALL).check();
			    break;
			    
			case 5510:
			    c.getRunecrafting().getPouch(PouchType.MEDIUM).check();
			    break;
			    
			case 5512:
			    c.getRunecrafting().getPouch(PouchType.LARGE).check();
			    break;
			    
			case 5514:
			    c.getRunecrafting().getPouch(PouchType.GIANT).check();
			    break;

			case 6865:
				c.startAnimation(3005);
				c.startGraphic(Graphic.create(513));
				c.sendMessage("You make your Marionette bow.");
				break;
			case 6867:
				c.startAnimation(3005);
				c.startGraphic(Graphic.create(209));
				c.sendMessage("You make your Marionette bow.");
				break;
			case 6866:
				c.startAnimation(3005);
				c.startGraphic(Graphic.create(517));
				c.sendMessage("You make your Marionette bow.");
				break;
			case 11694:
				c.sendMessage("Dismantle has been disabled.");
				break;
			case 11696:
				c.sendMessage("Dismantle has been disabled.");
				break;
			case 11698:
				c.sendMessage("Dismantle has been disabled.");
				break;
			case 11700:
				c.sendMessage("Dismantle has been disabled.");
				break;
			default:
				if (c.playerRights == 3 || c.isDev()) {
					Misc.println(c.getName() + " - Item2ndOption: " + itemId);
				}
				break;
		}

		if (c.insideDungeoneering()) {
			BindingData data = BindingData.values.get(itemId);
			if (data == null) {
				return;
			}

			if (!data.isStackable()) {
				if (c.getBinds().addBind(data.getBindItemId())) {
					c.getItems().deleteItemInOneSlot(itemId, slot, 1);
					c.sendMessage("You bind the " + ItemDef.forID(itemId).name + " to you.");
					c.sendMessage("Check in with the smuggler to manage your bound items.");
				}
			} else {
				c.getBinds().addAmmunition(itemId, slot, c.getItems().getItemAmountInSlot(slot), data.getBindItemId());
			}
			return;
		}
	}

}
