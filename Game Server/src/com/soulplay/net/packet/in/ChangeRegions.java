package com.soulplay.net.packet.in;

import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.map.travel.ZoneManager;
import com.soulplay.net.packet.PacketType;

/**
 * Change Regions
 */
public class ChangeRegions implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		// Server.objectHandler.updateObjects(c);
		/*
		 * for (EntityRegion r : RegionHandler.regions) { if (r.regionX ==
		 * c.lastX/64 && r.regionY == c.lastY/64) { r.removePlayer(c); } if
		 * (r.regionX == c.absX/64 && r.regionY == c.absY/64) { r.addPlayer(c);
		 * c.currentRegion = r; break; } }
		 */
		// c.updateEntityRegion();

		if (c.reconnectAttempts > 0 && c.getOutStream() != null) {
			c.reconnectAttempts = 0;
		}

		if (!c.correctlyInitialized) {
			c.initializeAfterProperConnection();
			c.correctlyInitialized = true;
			// if (c.isRequiredPinVerification())
			// c.correctlyInitialized = false;

			if (Server.getPanel() != null) {
				Server.getPanel().addEntity(c.getName());
			}

			if (c.UUID.equals(c.oldUUID)) {
			    if (!c.UUID.equals(c.oldUUID) && !c.hasBankPin) { // new player
				    c.hidePlayer = false;
			    }
			}
		}

		c.setLoadedRegion(true);
		
		if (c.regionCheckX == c.absX && c.regionCheckY == c.absY
				&& c.regionCheckZ == c.heightLevel) {
			return;
		}

		c.regionCheckX = c.absX;
		c.regionCheckY = c.absY;
		c.regionCheckZ = c.heightLevel;
		
		ZoneManager.onRegionChangePacket(c);

		c.setNpcAggroTick(0); // reset npc aggro tick
		ItemHandler.reloadItems(c);
		ObjectManager.loadObjects(c);
		// c.getPA().castleWarsObjects();

		c.clearLists();

		c.saveFile = true;

		// if (c.getSkullTimer() > 0) {
		// c.isSkulled = true;
		// c.headIconPk = 0;
		// c.requestUpdates();
		// }

	}

}
