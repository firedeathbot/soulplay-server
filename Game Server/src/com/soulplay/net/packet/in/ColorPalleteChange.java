package com.soulplay.net.packet.in;

import com.soulplay.content.items.CompCape;
import com.soulplay.content.items.impl.CompletionistCape;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.net.packet.PacketType;

public class ColorPalleteChange implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {

		final int slot = c.getInStream().readUnsignedByte();
		final int color = c.getInStream().readUnsignedShort();// can include item id and pack into this number
		if (slot < 0 || slot >= CompCape.MAX_COLORS)
			return;
		if (c.getCompCape() != null) {
			c.getCompCape().changeColor(slot, (short)color);
			CompletionistCape.openCustomizeInterface(c);
			c.getUpdateFlags().add(UpdateFlag.APPEARANCE);
		}
		
	}

}
