package com.soulplay.net.packet.in;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.info.ChatSettings;
import com.soulplay.game.model.player.privatemessage.Friends;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.net.Connection;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.packet.PacketType;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.Misc;

public class PrivateMessaging implements PacketType {

	public static List<String> trackedPlayers = new ArrayList<>();

	public static final int ADD_FRIEND = 188, SEND_PM = 126, REMOVE_FRIEND = 215,
			CHANGE_PM_STATUS = 95, REMOVE_IGNORE = 59,
			REMOVE_IGNORE_RIGHTCLICK = 74, ADD_IGNORE = 133;

	@Override
	public void processPacket(final Client c, final int packetType,
			final int packetSize) {
		
		if (c.logoutOnVerificationState())
			return;

		c.isIdle = false;

		long longName = 0;

		switch (packetType) {

			case ADD_FRIEND:

				longName = c.getInStream().readLong();

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}

				try {
					final String displayNameString = Misc.longToPlayerName2(longName);
					if (displayNameString == null) {
						return;
					}

					if (c.addingFriend) {
						c.sendMessage("Please wait while friends list is loading.");
						return;
					}

					c.addingFriend = true;

					TaskExecutor.executeSQL(new Runnable() {

						@Override
						public void run() {

							final String username = PlayerSaveSql.getUserNameByDisplayName(displayNameString);
							
							Server.getTaskScheduler().schedule(new Runnable() {
								
								@Override
								public void run() {
									
									try {
										c.addingFriend = false;
											
										if (username == null) {
											c.sendMessage("Player does not exist.");
											return;
										}

										Friends.addFriend(Misc.playerNameToInt64(username), c);
										c.sendMessage("Added " + displayNameString + " to friends list.");
									} catch (Exception e) {
										c.addingFriend = false;
										c.sendMessage("Something went wrong...");
										e.printStackTrace();
									}
								}
							});
							
						}
					});


				} catch (Exception e) {
					//e.printStackTrace();
					return;
				}

				break;

			case SEND_PM:
				final long sendMessageToFriendId = c.getInStream().readLong();
				final int pmchatTextSize = Math.min(80, (packetSize - 8));
				final byte pmchatText[] = new byte[pmchatTextSize];
				c.getInStream().readBytes(pmchatText, pmchatTextSize, 0);

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}

				if (Connection.isMuted(c)) {
					break;
				}
				if (c.getTimedMute() > 0) {
					c.sendMessage("You are muted for breaking a rule.");
					return;
				}
				String text = Misc.textUnpack(pmchatText, pmchatTextSize);

				String friendName = Misc.longToPlayerName2(sendMessageToFriendId);
				if (friendName == null || friendName.length() > 12) {
					return;
				}
				if (Misc.isBad(text)) {
					PlayerHandler
							.alertStaff("Possible advertiser: " + c.getNameSmartUp(), "Text: " + text);
					c.getPA().spamWarning();
					if (!Connection.isMuted(c)) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildAdvertiserMessage(c.getName(), text,
										"Private (" + friendName + ")"));
					}
				}
				if (trackedPlayers.contains(c.getName())) {
					PlayerHandler.alertAdminsAndOwners(
							"<col=ff0000>" + c.getNameSmartUp() + " -> " + friendName, "<col=ff0000>" + text);
				}

				ReportHandler.addText(c.getName(),
						"PMTO:" + friendName + ":" + text);
				
				int chatCrown;
				
				if (!Config.RUN_ON_DEDI && !c.isDev()) {
					chatCrown = ChatCrown.REGULAR.getClientIcon();
				} else {
					chatCrown = ChatCrown.values[c.getSetCrown()].getClientIcon();
				}

				LoginServerConnection.instance()
						.submit(() -> LoginServerConnection.instance()
								.sendPrivateMessage(sendMessageToFriendId, pmchatText,
										c.getName(), chatCrown));
				break;

			case REMOVE_FRIEND:
				c.friendUpdate = true;
				longName = c.getInStream().readLong();
				if (longName < 0) {
					break;
				}

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}

				Friends.removeFriend(longName, c);
				break;

			case REMOVE_IGNORE: // some random packet that is unknown packet 59
								// lol
				c.getInStream().readInt();
				c.getInStream().readInt();
				c.getInStream().readInt();

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}

				// for other status changing
				// c.getPA().handleStatus(i,i2,i3);
				break;

			case REMOVE_IGNORE_RIGHTCLICK:
				longName = c.getInStream().readLong();
				if (longName < 0) {
					break;
				}

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}

				Friends.removeFromIgnore(longName, c);

				break;

			case CHANGE_PM_STATUS:
				int publicChat = c.getInStream().readUnsignedByte();
				int privateChat = c.getInStream().readUnsignedByte();
				int tradeAndCompete = c.getInStream().readUnsignedByte();

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}

				if (publicChat < 0
						|| publicChat > ChatSettings.PUBLIC.values.length) {
					return;
				}

				if (privateChat < 0
						|| privateChat > ChatSettings.PRIVATE.values.length) {
					return;
				}

				if (tradeAndCompete < 0 || tradeAndCompete > ChatSettings.TRADE
						.values.length) {
					return;
				}

				c.setPublicChatSettings(publicChat);
				c.setPrivateChatSettings(privateChat);
				c.setTradeChatSettings(tradeAndCompete);

				LoginServerConnection.instance()
						.submit(() -> LoginServerConnection.instance()
								.updatePrivateChatMode(privateChat,
										c.getName()));

				break;

			case ADD_IGNORE:
				longName = c.getInStream().readLong();
				if (longName < 0) {
					break;
				}

				if (!c.correctlyInitialized) {
					return;
				}
				if (!c.saveFile || c.newPlayer || !c.saveCharacter
						|| !c.setMode) {
					return;
				}

				if (c.getReceivedPacket()[packetType]) {
					// method
					return;
				}
				c.setReceivedPacket(packetType, true);

				if (c.cantCommunicate()) {
					c.sendMessage("Please wait some time before chatting.");
					return;
				}
				if (c.getLongNameSmart() == longName) {
					return;
				}
				Friends.addToIgnore(longName, c);
				break;

		}

	}
}
