package com.soulplay.net.packet.in;

import com.soulplay.content.minigames.duel.DuelRules;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.net.packet.PacketType;

/**
 * Walking packet
 **/
public class Walking implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		if (c.usingSpellFromSpellBook) {
			c.usingSpellFromSpellBook = false;
		}
		c.destinationX = c.destinationY = 0;
		c.destOffsetX = c.destOffsetY = c.entityWalkingFlag = c.clickReachDistance = 0;
		c.entityClickSizeX = c.entityClickSizeY = c.clickReachDistance = 1;
		if (c.disableInteractClip)
			c.disableInteractClip = false;
		
		if (c.getSingleCastSpell() != null)
			c.setSingleCastSpell(null);
		
		if (c.getDialogueBuilder().isActive()) {
			c.getDialogueBuilder().setClose(true).reset();
		}

		if (c.attr().getOrDefault("viewing_inventory", false)) {
			c.getItems().updateInventory();
		}

		if (c.attr().getOrDefault("viewing_equipment", false)) {
			c.getPacketSender().itemsOnInterface2(1688, c.playerEquipment, c.playerEquipmentN);
		}

		if (packetType == 248) {
			packetSize -= 14;
		}

		int steps = (packetSize - 5) / 2;
		if (steps < 0) {
			// c.disconnected = true;
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

//		int[][] path = new int[steps][2];

		int firstStepX1 = c.getInStream().readLEShortA();
//		for (int i = 0; i < steps; i++) {
//			path[i][0] = c.getInStream().readByte();
//			path[i][1] = c.getInStream().readByte();
//		}
		int firstStepY1 = c.getInStream().readLEShort();
		
//		c.sendMessage(String.format("x:%d y:%d", firstStepX1, firstStepY1));

		byte byteRun = c.getInStream().readByteC();

		
		c.isIdle = false;
		
		if (!c.getController().canWalk()) {
		    c.getPacketSender().resetMinimapFlagPacket();
		    return;
		}
		
		if (!c.correctlyInitialized || c.inVerificationState) {
			if (c.isDetectNull()) {
				c.sendMessage("Null1234 2:"+c.correctlyInitialized+ " 3:"+c.inVerificationState);
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}
		
		if (!c.setMode) {
			//open up pick mode dialogue
			c.starterDialogue();
			return;
		}
		
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| c.performingAction) {
			if (c.isDetectNull()) {
				c.sendMessage("Null738");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.getAgility().doingAgility) {
			if (c.isDetectNull()) {
				c.sendMessage("Null393");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}	

		if (c.tourStep > 0 && c.tourStep < 100) {
			c.getDH().sendOption2("I would like to stop the tour.",
					"Please continue the tour.");
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}
		if (c.nextChat == 946 || c.dialogueAction == 947
				|| c.dialogueAction == 123 || !c.setMode) {
			if (c.isDetectNull()) {
				c.sendMessage("Null546");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.teleTimer > 4) {
			if (c.isDetectNull()) {
				c.sendMessage("tele263");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				if (c.isDetectNull()) {
					c.sendMessage("Null895");
				}
				c.getPacketSender().resetMinimapFlagPacket();
				return;
			}
		}
		if (c.performingAction) {
			if (c.isDetectNull()) {
				c.sendMessage("Null783");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.isStunned()) {
			c.sendMessage("You are currently stunned.");
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.isDead()) {
			if (c.isDetectNull()) {
				c.sendMessage("dead887");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.canWalk == false) {
			if (c.isDetectNull()) {
				c.sendMessage("Null996");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (System.currentTimeMillis() - c.lastSpear < 4000) {
			c.sendMessage("You have been stunned.");
			c.playerIndex = 0;
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.teleportToX > 0) { // do not handle packets for players that are
									// being moved
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.playerIndex > 0 || c.npcIndex > 0) {
			c.getCombat().resetPlayerAttack();
		}

		if (c.canChangeAppearance) {
			c.canChangeAppearance = false;
		}

		c.uniqueActionId = 0;
		
		c.finalLocalDestX = c.finalLocalDestY = 0;

		if (c.walkingToObject) {
			c.walkingToObject = false;
		}
		c.walkingToItem = false;
		
		if (c.recentPlayerHitIndex > 0)
			c.recentPlayerHitIndex = 0;
		
		c.clickNpcType = 0;
		c.clickObjectType = 0;
		
		if (c.isBanking) {
			c.getPA().closeAllWindows();// c.isBanking = false;
		}
		if (c.tradeStatus >= 0) {
			c.tradeStatus = 0;
			c.getPA().closeAllWindows();
		}
		if (c.runRecoverBoost != 0) {
			c.getPA().closeAllWindows();
		}
		if (c.isBobOpen) {
			c.isBobOpen = false;
		}
		if (c.isShopping) {
			c.isShopping = false;
		}
		if (c.inTrade) {
			// c.sendMessage("You must decline the trade to start walking.");
			c.getTradeAndDuel().declineTrade();
			// return;
		}
		
		if (c.openDuel && c.duelStatus != 5 && c.duelStatus != 6) {
			c.getTradeAndDuel().declineDuel();
		}
		if ((c.duelStatus >= 1 && c.duelStatus <= 4) || c.duelStatus == 6) {
			if (c.isDetectNull()) {
				c.sendMessage("Null455");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.followId > 0 || c.followId2 > 0) {
			c.getPA().resetFollow();
		}

		if (packetType == 248 || packetType == 164) {
			c.clickObjectType = 0;
			c.clickNpcType = 0;
			c.sendPokemonAtNpcIndex = c.npcIndex = 0;
			c.playerIndex = 0;
		}
		
		if (c.interfaceIdOpenMainScreen != 0) {
			c.getPA().closeAllWindows(); // c.getPA().removeAllWindows();
		}
		if (c.sidebarInterfaceOpen != 0) { // TODO: redo this shit if i use this
											// for checking their regular
											// sidebar interface for inventory
			c.getPA().closeAllWindows();
		}
		c.resetFace();
		if (c.duelRule[DuelRules.NO_MOVEMENT] && c.duelStatus == 5) {
			if (c.getDuelingClient() != null) {
				if (!PlayerConstants.goodDistance(c.getX(), c.getY(),
						c.getDuelingClient().getX(),
						c.getDuelingClient().getY(), 1) || c.getAttackTimer() == 0) {
					c.sendMessage("Walking has been disabled in this duel!");
				}
			}
			c.playerIndex = 0;
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}

		if (c.getFreezeTimer() > 0) {
			if (packetType != 98) {
				c.sendMessage("A magical force stops you from moving.");
				c.playerIndex = 0;
			}
		}

		if (c.startedCheckSpins) {
			c.startedCheckSpins = false;
		}

		if (c.duelStatus >= 1 && c.duelStatus <= 4) {
			Client o = c.getDuelingClient();
			if (o != null) {
				o.getTradeAndDuel().declineDuel();
				o.duelStatus = 0;
			} else {
				System.out.println("Error in walking.java 111");
			}
			c.duelStatus = 0;
			c.getTradeAndDuel().declineDuel();
		}

		if (c.respawnTimer > 3) {
			if (c.isDetectNull()) {
				c.sendMessage("Null275");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}
		if (c.inTrade) {
			if (c.isDetectNull()) {
				c.sendMessage("Null148");
			}
			c.getPacketSender().resetMinimapFlagPacket();
			return;
		}
		
		c.getMovement().setNewWalkCmdIsRunning(/* c.getInStream().readSignedByteC() */byteRun == 1);
		
		c.destinationX = firstStepX1;
		c.nextChat = 0;
		c.destinationY = firstStepY1;

//		if (c.noClip) { // TODO::add custom objects to clipmap
//			c.getMovement().newWalkCmdSteps = (packetSize - 5) / 2;
//			if (++c.getMovement().newWalkCmdSteps > c.getMovement().walkingQueueSize) {
//				c.getMovement().newWalkCmdSteps = 0;
//				c.getPacketSender().resetMinimapFlagPacket();
//				return;
//			}
//
//			c.getMovement().getNewWalkCmdX()[0] = c.getMovement().getNewWalkCmdY()[0] = 0;
//
//			int firstStepX = /* c.getInStream().readSignedWordBigEndianA() */firstStepX1
//					- c.getMapRegionX() * 8;
//			for (int i = 1; i < c.getMovement().newWalkCmdSteps; i++) {
//				/*c.finalLocalDestX = */c.getMovement().getNewWalkCmdX()[i] = (steps > 0
//						? path[i - 1][0]
//						: path[i][0])/* c.getInStream().readSignedByte() */;
//				/*c.finalLocalDestY = */c.getMovement().getNewWalkCmdY()[i] = (steps > 0
//						? path[i - 1][1]
//						: path[i][1])/* c.getInStream().readSignedByte() */;
//			}
//
//			int firstStepY = /* c.getInStream().readSignedWordBigEndian() */firstStepY1
//					- c.getMapRegionY() * 8;
//			c.getMovement().setNewWalkCmdIsRunning(
//					(/* c.getInStream().readSignedByteC() */byteRun == 1));
//
//			for (int i1 = 0; i1 < c.getMovement().newWalkCmdSteps; i1++) {
//				/*c.finalLocalDestX = */c.getMovement().getNewWalkCmdX()[i1] += firstStepX;
//				/*c.finalLocalDestY = */c.getMovement().getNewWalkCmdY()[i1] += firstStepY;
//			}
//		} else {
//
//			int firstStepX = /* c.getInStream().readSignedWordBigEndianA() */firstStepX1;
//			int firstStepY = /* c.getInStream().readSignedWordBigEndian() */firstStepY1;
//
//			for (int i = 0; i < steps; i++) {
//				path[i][0] += firstStepX;
//				path[i][1] += firstStepY;
//			}
//			if (steps > 0) {
//				c.destinationX = path[steps - 1][0];
//				c.destinationY = path[steps - 1][1];
//				if (c.clickTravel) {
//					c.getPA().movePlayer(path[steps - 1][0], path[steps - 1][1],
//							c.getHeightLevel());
//					c.getPacketSender().resetMinimapFlagPacket();
//					return;
//				}
//				PathFinder.findRoute(c, path[steps - 1][0], path[steps - 1][1],
//						true, 1, 1);
//			} else {
//				c.destinationX = firstStepX;
//				c.destinationY = firstStepY;
//				if (c.clickTravel) {
//					c.getPA().movePlayer(firstStepX, firstStepY,
//							c.getHeightLevel());
//					c.getPacketSender().resetMinimapFlagPacket();
//					return;
//				}
//				PathFinder.findRoute(c, firstStepX, firstStepY, true, 1, 1);
//			}
//
//		}

		if (c.lmsUniqueId == 0 || packetType != 98)
			c.resetSkillingEvent();
	}

}
