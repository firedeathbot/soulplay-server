package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.packet.PacketType;

public class MoveItems implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int interfaceId = c.getInStream().readLEShortA();
		boolean insertMode = c.getInStream().readByteC() == 1;
		int from = c.getInStream().readLEShortA();
		int to = c.getInStream().readLEShort();
		// System.out.println("worked1234");

		if (!c.correctlyInitialized) {
			return;
		}
		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode) {
			return;
		}

		c.isIdle = false;

		if (c.performingAction) {
			c.setUpdateInvInterface(interfaceId);
			c.sendMessage("Your player is currently busy.");
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (c.isBanking) {
			if (interfaceId == 5) {
				c.getItems().toTab(0, from);
				return;
			} else if (interfaceId == 13) {
				c.getItems().toTab(1, from);
				return;
			} else if (interfaceId == 26) {
				c.getItems().toTab(2, from);
				return;
			} else if (interfaceId == 39) {
				c.getItems().toTab(3, from);
				return;
			} else if (interfaceId == 52) {
				c.getItems().toTab(4, from);
				return;
			} else if (interfaceId == 65) {
				c.getItems().toTab(5, from);
				return;
			} else if (interfaceId == 78) {
				c.getItems().toTab(6, from);
				return;
			} else if (interfaceId == 91) {
				c.getItems().toTab(7, from);
				return;
			} else if (interfaceId == 104) {
				c.getItems().toTab(8, from);
				return;
			}
			c.setUpdateInvInterface(3214); // c.getItems().resetItems(3214);
		}

		// c.sendMessage("interface: "+interfaceId);
		if (interfaceId != 3214 && interfaceId != 5382 && interfaceId != 5064) {
			return;
		}

		if (interfaceId == 3214 || interfaceId == 5064) {
			if (from < 0 || from >= c.playerItems.length) {
				return;
			}
			if (to < 0 || to >= c.playerItems.length) {
				return;
			}
		}

		if (c.isDead()) {
			return;
		}

		if (interfaceId == 5382) {
			if (from < 0 || from > Config.BANK_TAB_SIZE) {
				return;
			}
			if (to < 0 || to > Config.BANK_TAB_SIZE) {
				return;
			}
		}

		c.getItems().moveItems(from, to, interfaceId, insertMode);
		if (c.getDialogueBuilder().isActive()) {
			c.getDialogueBuilder().reset();
		}

		if (c.inTrade) {
			c.getTradeAndDuel().declineTrade();
			return;
		}
		if (c.tradeStatus == 1) {
			c.getTradeAndDuel().declineTrade();
			return;
		}
		if (c.duelStatus == 1) {
			c.getTradeAndDuel().declineDuel();
			return;
		}
	}
}
