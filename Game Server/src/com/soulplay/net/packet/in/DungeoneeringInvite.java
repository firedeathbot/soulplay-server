package com.soulplay.net.packet.in;

import com.soulplay.Config;
import com.soulplay.content.player.skills.dungeoneeringv2.Party;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.PacketType;

public class DungeoneeringInvite implements PacketType {

	@Override
	public void processPacket(final Client c, int packetType, int packetSize) {
		int playerId = c.getInStream().readLEShort();
		
		c.isIdle = false;

		if (playerId < 0 || playerId >= Config.MAX_PLAYERS) {
			return;
		}

		if (!c.saveFile || c.newPlayer || !c.saveCharacter || !c.setMode
				|| !c.correctlyInitialized) {
			return;
		}

		if (c.noClip) {
			if (c.noClipX > 0 || c.noClipY > 0) {
				return;
			}
		}

		if (c.spectatorMode) {
			c.sendMessage("Can't do that in spectator mode.");
			return;
		}

		if (c.duelStatus == 1 || c.duelStatus == 2 || c.duelStatus == 3
				|| c.duelStatus == 4) {
			return;
		}

		if (c.performingAction) {
			return;
		}

		if (playerId < 1 || PlayerHandler.players[playerId] == null) {
			return;
		}
		
		if (c.logoutOnVerificationState())
			return;

		if (!c.getSpamTick().checkThenStart(1)) {
			c.sendMessage("Please slow down...");
			return;
		}

		c.getPA().resetFollow();

		if (c.duelFightArenas()) {
			c.sendMessage("You can't invite inside the arena!");
			return;
		}

		/*if (c.inTrade) {
			c.getTradeAndDuel().declineTrade();
			c.sendMessage("Current trade has been declined.");
			return;
		}*/

		c.walkingToMob = true;
		c.finalLocalDestX = c.finalLocalDestY = 0;
		c.destOffsetX = c.destOffsetY = 1;
		c.walkUpToPathBlockedMob = false;
		c.clickReachDistance = 1;
		
		if (playerId == c.getId()) {
			return;
		}

		if (!c.inDaemonheim()) {
			c.sendMessage("You must be in Daemonheim to invite players.");
			return;
		}

		Client o = (Client) PlayerHandler.players[playerId];
		if (!o.inDaemonheim()) {
			c.sendMessage("The player you're trying to invite must be in Daemonheim.");
			return;
		}

		if (o.dungPartyInvite == c.getId()) {
			Party.openInviteInterface(c, o.dungParty, o);
		} else {
			if (c.dungParty == null) {
				c.sendMessage("You don't have an active party.");
				return;
			}

			c.dungParty.inviteToParty(c, o);
		}
	}

}
