package com.soulplay.net.packet.in;

public class DuelPlayer {

	public static int RULE_WALKING = 1, RULE_RANGED = 2, RULE_MELEE = 3,
			RULE_MAGIC = 4, RULE_POTIONS = 5, RULE_FOOD = 6, RULE_PRAYER = 7,
			RULE_OBSTACLES = 8, RULE_FUN_WEAPONS = 9, RULE_SPECIAL_ATTACK = 10,
			RULE_HATS = 11, RULE_CAPES = 12, RULE_AMULETS = 13,
			RULE_WEAPONS = 14, RULE_BODIES = 15, RULE_SHIELDS = 16,
			RULE_LEGS = 17, RULE_GLOVES = 18, RULE_BOOTS = 19, RULE_RINGS = 20,
			RULE_ARROWS = 21;

}
