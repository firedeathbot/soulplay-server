package com.soulplay.net.packet.out;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.soulplay.Server;
import com.soulplay.content.donate.DonationInterface;
import com.soulplay.content.donate.DonationInterfaceCategory;
import com.soulplay.content.donate.DonationInterfaceCategoryEntry;
import com.soulplay.content.instances.InstanceType;
import com.soulplay.content.items.presets.Preset;
import com.soulplay.content.player.dailytasks.PlayerCategory;
import com.soulplay.content.player.dailytasks.PlayerTask;
import com.soulplay.content.player.pets.pokemon.PokemonData;
import com.soulplay.content.player.reflectioncheck.ReflectionCheckManager;
import com.soulplay.content.player.seasonpass.Reward;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.HouseStyle;
import com.soulplay.content.player.skills.construction.Palette;
import com.soulplay.content.player.skills.construction.Poh;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.poll.Poll;
import com.soulplay.content.poll.PollEntry;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.seasonals.powerups.PowerUpRarity;
import com.soulplay.content.vote.VoteInterfaceStreak;
import com.soulplay.game.model.DisplayTimerType;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.region.Region;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.packet.ConfigCodes;

public class PacketSender {

	private final Client c;

	public PacketSender(Client c) {
		this.c = c;
	}

	public void reflectionCheckGetField(int uid, String className, String fieldName) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(180);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeInt(uid);

		c.getOutStream().writeByte(ReflectionCheckManager.REQUEST_FIELD);
		c.getOutStream().writeString(className);
		c.getOutStream().writeString(fieldName);

		c.getOutStream().endFrameVarSize();
	}

	public void reflectionCheckSetField(int uid, String className, String fieldName, int value) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(180);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeInt(uid);

		c.getOutStream().writeByte(1);
		c.getOutStream().writeString(className);
		c.getOutStream().writeString(fieldName);
		c.getOutStream().writeInt(value);

		c.getOutStream().endFrameVarSize();
	}

	public void reflectionCheckGetFieldMod(int uid, String className, String fieldName) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(180);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeInt(uid);

		c.getOutStream().writeByte(2);
		c.getOutStream().writeString(className);
		c.getOutStream().writeString(fieldName);

		c.getOutStream().endFrameVarSize();
	}

	public void reflectionCheckInvokeMethod(int uid, int type, String className, String methodName, String methodReturnType, String[] arguments, Object[] argumentData) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(180);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeInt(uid);

		c.getOutStream().writeByte(3);
		c.getOutStream().writeString(className);
		c.getOutStream().writeString(methodName);
		c.getOutStream().writeByte(arguments.length);
		for (int i = 0; i < arguments.length; i++) {
			c.getOutStream().writeString(arguments[i]);
		}
		c.getOutStream().writeString(methodReturnType);
		//TODO write argument data

		c.getOutStream().endFrameVarSize();
	}

	public void reflectionCheckGetMethodMod(int uid, String className, String methodName, String methodReturnType, String[] arguments) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(180);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeInt(uid);

		c.getOutStream().writeByte(4);
		c.getOutStream().writeString(className);
		c.getOutStream().writeString(methodName);
		c.getOutStream().writeByte(arguments.length);
		for (int i = 0; i < arguments.length; i++) {
			c.getOutStream().writeString(arguments[i]);
		}
		c.getOutStream().writeString(methodReturnType);

		c.getOutStream().endFrameVarSize();
	}

	public void updateLocalPlayerPacket(Location location) {
		if (!(c != null && c.isActive && c.getOutStream() != null)) {
			return;
		}
		c.getOutStream().createPacket(85);
		c.getOutStream().writeByteC(location.getY() - c.getMapRegionY() * 8);
		c.getOutStream().writeByteC(location.getX() - c.getMapRegionX() * 8);
		c.flushOutStream();
	}

	public void playSound(int id) {
		playSound(id, 0);
	}

	public void playSound(int id, int delay) {
		if (c == null || !c.isActive || c.getOutStream() == null || c.isBot() || id == -1) {
			return;
		}

		c.getOutStream().createPacket(174);
		c.getOutStream().writeShort(id);
		c.getOutStream().writeShort(delay);
		c.flushOutStream();
	}

	public void changeItemId(int component, int itemId) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(135);
		c.getOutStream().writeInt(component);
		c.getOutStream().write3Byte(itemId);
		c.flushOutStream();
	}

	public void camShake(int type, int baseStrength, int waveStrength, int wave1) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(35);
		c.getOutStream().writeByte(type);
		c.getOutStream().writeByte(baseStrength);
		c.getOutStream().writeByte(waveStrength);
		c.getOutStream().writeByte(wave1);
		c.flushOutStream();
	}

	public void sendLogoutPacket() {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(109);
		c.flushOutStream();
	}
	
	public void clearMapZone() {
//		int x = c.getLocalX();
//		int y = c.getLocalY();
//		x -= x/8;
//		y -= y/8;
//		clearMapZone(x, y);
		clearMapZone(c.getLocalX()-c.getCurrentLocation().getChunkOffsetX(), c.getLocalY()-c.getCurrentLocation().getChunkOffsetY());
	}
	
	public void clearAllRenderedMapZones() {
		for (int i = 0; i < 13; i++) {
			for (int j = 0; j < 13; j++) {
				clearMapZone(i*8, j*8);
			}
		}
	}
	
	public void clearMapZone(int localCornerX, int localCornerY) {
		if (c == null || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(64);
		c.getOutStream().writeByteC(localCornerX);
		c.getOutStream().writeByteS(localCornerY);
		c.flushOutStream();
	}

	public void showModelOnInterfacePacket(int interfaceId, int modelId) {	
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(8);
			c.getOutStream().writeLEShortA(interfaceId);
			c.getOutStream().writeShort(modelId);
			c.flushOutStream();
		}
	}

	public void addConstructionObjectPacket(GameObject object) {
		if (!(c != null && c.isActive && c.getOutStream() != null)) {
			return;
		}
		updateRegionPacket(object.getLocation());
		
		c.getOutStream().createPacket(152);
		c.getOutStream().writeByteA(0); //offset
		c.getOutStream().write3Byte(object.getId());
		c.getOutStream().writeByteS((object.getType() << 2) + (object.getOrientation() & 3));
		c.getOutStream().writeByte(object.getLocation().getZ() % 4);
		c.flushOutStream();
	}
	
	public void createPlayersObjectAnim(int x, int y, int animationID, int tileObjectType, int orientation) {
		createPlayersObjectAnim(x, y, animationID, tileObjectType, orientation, 0, 0);
	}

	public void createPlayersObjectAnim( final int x, final  int y, final  int animationID, final  int tileObjectType, final  int orientation, final int xOffset, final int yOffset) {
		if (c.isBot() || c == null || c.getOutStream() == null || !c.isActive) {
			return;
		}
		c.getOutStream().createPacket(85);
		c.getOutStream().writeByteC(y - (c.mapRegionY * 8));
		c.getOutStream().writeByteC(x - (c.mapRegionX * 8));
		c.getOutStream().createPacket(160);
		c.getOutStream().writeByteS(((xOffset & 7) << 4) + (yOffset & 7));// tiles away - could just send 0
		c.getOutStream().writeByteS((tileObjectType << 2) + (orientation & 3));
		c.getOutStream().writeShortA(animationID);// animation id
		c.flushOutStream();
	}

	public void sendMiniMapFlag(int localX, int localY) {
		if (c.isBot()) {
			return;
		}
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(78);
			c.getOutStream().writeByte(localX);
			c.getOutStream().writeByte(localY);
			c.flushOutStream();
		}
	}
	
	public void resetMinimapFlagPacket() {
		sendMiniMapFlag(0, 0);
	}
	
	public void sendMusic(int musicId) {
		if (c.isBot())
			return;
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(74);
			c.getOutStream().writeLEShort(musicId);
			c.flushOutStream();
		}
	}

	public void addObjectPacket(GameObject object) {
		addObjectPacket(object.getId(), object.getLocation(), object.getType(), object.getOrientation());
	}
	
	public void addObjectPacket(int id, Location loc, final int type, final int orientation) {
		addObjectPacket(id, loc.getX(), loc.getY(), type, orientation);
	}
	
	public void addObjectPacket(int id, int x, int y, final int type, final int orientation) {
		addObjectPacket(id, x, y, type, orientation, 0);
	}
	
	public void addObjectPacket(int id, int x, int y, final int type, final int orientation, final int offset) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) {
			return;
		}

		updateRegionPacket(x, y);

		if (id == -1) {
			c.getOutStream().createPacket(101);
			c.getOutStream().writeByteC((type << 2) + (orientation & 3));
			c.getOutStream().writeByte(offset);
		} else {
			c.getOutStream().createPacket(151);
			c.getOutStream().writeByteA(offset);
			c.getOutStream().write3Byte(id);
			c.getOutStream().writeByteS((type << 2) + (orientation & 3));
		}
		c.flushOutStream();
	}

	public void removeObjectPacket(GameObject object) {
		if (!(c != null && c.isActive && c.getOutStream() != null)) {
			return;
		}

		updateRegionPacket(object.getLocation());

		c.getOutStream().createPacket(101);
		c.getOutStream().writeByteC((object.getType() << 2) + (object.getOrientation() & 3));
		c.getOutStream().writeByte(0);
		c.flushOutStream();
	}

	public void updateRegionPacket(Location location) {
		updateRegionPacket(location.getX(), location.getY());
	}
	
	public void updateRegionPacket(int x, int y) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) {
			return;
		}

		c.getOutStream().createPacket(85);
		c.getOutStream().writeByteC(y - (c.mapRegionY * 8));
		c.getOutStream().writeByteC(x - (c.mapRegionX * 8));
		c.flushOutStream();
	}

	public void setBlackout(int state) { // used for disabling map
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(99);
		c.getOutStream().writeByte(state);
		c.flushOutStream();
	}

	public void createArrow(int type, int id) {
		// c.sendMessage("I got the arrows too");
		if (c.isBot()) {
			return;
		}
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		if (c != null) {
			c.getOutStream().createPacket(254); // The packet ID
			c.getOutStream().writeByte(type); // 1=NPC, 10=Player
			c.getOutStream().writeShort(id); // NPC/Player ID
			c.getOutStream().write3Byte(0); // Junk
			c.flushOutStream();
		}
		c.arrowX = c.arrowY = c.arrowZ = c.arrowP = 0;
	}

	public void resetArrow() {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(254); // The packet ID
		c.getOutStream().writeByte(0);
		c.getOutStream().writeShort(0); // X-Coord of Object
		c.getOutStream().writeShort(0); // Y-Coord of Object
		c.getOutStream().writeByte(0); // Height off Ground
		c.flushOutStream();
		c.arrowX = c.arrowY = c.arrowZ = c.arrowP = 0;
	}

	public void createArrow(int x, int y, int height, int pos) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(254); // The packet ID
		c.getOutStream().writeByte(pos); // Position on Square(2 = middle, 3
		// = west, 4 = east, 5 = south,
		// 6 = north)
		c.getOutStream().writeShort(x); // X-Coord of Object
		c.getOutStream().writeShort(y); // Y-Coord of Object
		c.getOutStream().writeByte(height); // Height off Ground
		c.flushOutStream();
	}

	public void createObjectHints(int x, int y, int height, int pos) {
		// synchronized(c) {
		if (c.isBot()) {
			return;
		}
		c.objectHintPos = pos;
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(254);
			c.getOutStream().writeByte(pos);
			c.getOutStream().writeShort(x);
			c.getOutStream().writeShort(y);
			c.getOutStream().writeByte(height);
			c.flushOutStream();
		}
		c.arrowX = c.arrowY = c.arrowZ = c.arrowP = 0;

	}

	public void createPlayerHints(int type, int id) {
		// synchronized(c) {
		if (c.isBot()) {
			return;
		}
		if (c.lastHintId == id) {
			return;
		}
		c.lastHintId = id;
		c.playerHintId = id;
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(254);
			c.getOutStream().writeByte(type);
			c.getOutStream().writeShort(id);
			c.getOutStream().write3Byte(0);
			c.flushOutStream();
		}

	}

	/**
	 * Creating projectile
	 **/
	public void createProjectile(int x, int y, int offX, int offY, int endDelay, int gfxMoving, int startHeight, int endHeight,
			int lockon, int startDelay, int slope, int startOffset) {
		if (c.isBot()) {
			return;
		}
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(85);
			c.getOutStream().writeByteC(y - (c.getMapRegionY() * 8));
			c.getOutStream().writeByteC(x - (c.getMapRegionX() * 8));
			c.getOutStream().createPacket(117);
			c.getOutStream().writeByte(0);//tile offset
			c.getOutStream().writeByte(offY);
			c.getOutStream().writeByte(offX);
			c.getOutStream().writeShort(lockon);
			c.getOutStream().writeShort(gfxMoving);
			c.getOutStream().writeByte(startHeight);
			c.getOutStream().writeByte(endHeight);
			c.getOutStream().writeShort(startDelay);
			c.getOutStream().writeShort(endDelay);
			c.getOutStream().writeByte(slope);
			c.getOutStream().writeByte(startOffset);
			c.flushOutStream();

		}
	}

	/**
	 * Draws Max 127 items on interface cuz Slot is byte.
	 * @param frame
	 * @param item
	 * @param slot
	 * @param amount
	 */
	public void displayItemOnInterface(int frame, int item, int slot, int amount) {
		updateInventoryPartial(frame, new Item(item, amount, slot));
	}
	
	public void updateInventoryPartial(int interfaceId, Item... items) {
		if (c != null && c.getOutStream() != null && c.isActive) {
			c.getOutStream().createVariableShortPacket(34);
			c.getOutStream().writeShort(interfaceId);
			for (int i = 0; i < items.length; i++) {
				c.getOutStream().writeSmart(items[i].getSlot());
				c.getOutStream().write3Byte(items[i].getId() + 1);
				int amount = items[i].getAmount();
				if (amount < 255)
					c.getOutStream().writeByte(amount);
				else {
					c.getOutStream().writeByte(255);
					c.getOutStream().writeInt(amount);
				}
			}
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
	}
	
	public void updateInventoryPartial(int interfaceId, ArrayList<Item> items) {
		if (c != null && c.getOutStream() != null && c.isActive) {
			c.getOutStream().createVariableShortPacket(34);
			c.getOutStream().writeShort(interfaceId);
			for (int i = 0; i < items.size(); i++) {
				c.getOutStream().writeSmart(items.get(i).getSlot());
				c.getOutStream().write3Byte(items.get(i).getId());
				int amount = items.get(i).getAmount();
				if (amount < 255)
					c.getOutStream().writeByte(amount);
				else {
					c.getOutStream().writeByte(255);
					c.getOutStream().writeInt(amount);
				}
			}
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
	}
	
	public void updateInventoryPartial(int interfaceId, int[] itemIds, int[] amounts, int[] slots) {
		if (c != null && c.getOutStream() != null && c.isActive) {
			c.getOutStream().createVariableShortPacket(34);
			c.getOutStream().writeShort(interfaceId);
			for (int i = 0; i < itemIds.length; i++) {
				int itemId = itemIds[i];
				int amount = amounts == null ? 1 : amounts[i];
				int slot = slots == null ? i : slots[i];
				c.getOutStream().writeSmart(slot);
				c.getOutStream().write3Byte(itemId <= 1 ? 0 : itemId + 1);
				if (amount < 255)
					c.getOutStream().writeByte(amount);
				else {
					c.getOutStream().writeByte(255);
					c.getOutStream().writeInt(amount);
				}
			}
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
	}
	
	public void updateInventoryPartial(int interfaceId, int[] itemIds, int[] amounts) {
		updateInventoryPartial(interfaceId, itemIds, amounts, null);
	}
	
	public void updateInventoryPartial(int interfaceId, int[] itemIds) {
		updateInventoryPartial(interfaceId, itemIds, null, null);
	}

	public void resetInvenory(int interfaceId) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(72);
		c.getOutStream().write3Byte(interfaceId);
		c.flushOutStream();
	}
	
	public void updateModelView(int interfaceId, int rotationX, int rotationY, int zoom) {
		if (c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(230);
			c.getOutStream().writeShortA(zoom);
			c.getOutStream().writeShort(interfaceId);
			c.getOutStream().writeShort(rotationY);
			c.getOutStream().writeLEShortA(rotationX);
			c.flushOutStream();
		}
	}
	
	public void setTextColor(int rsi_id, int color) {
		if (c.isBot())
			return;
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		c.getOutStream().createPacket(122);
		c.getOutStream().writeLEShortA(rsi_id);
		c.getOutStream().writeLEShortA(color);
		c.flushOutStream();
	}

	/**
	 * Show an arrow icon on the selected player.
	 *
	 * @Param i - Either 0 or 1; 1 is arrow, 0 is none.
	 * @Param j - The player/Npc that the arrow will be displayed above.
	 * @Param k - Keep this set as 0
	 * @Param l - Keep this set as 0
	 */
	public void drawHeadicon(int i, int j, int k, int l) {
		// synchronized(c) {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		c.getOutStream().createPacket(254);
		c.getOutStream().writeByte(i);

		if (i == 1 || i == 10) {
			c.getOutStream().writeShort(j);
			c.getOutStream().writeShort(k);
			c.getOutStream().writeByte(l);
		} else {
			c.getOutStream().writeShort(k);
			c.getOutStream().writeShort(l);
			c.getOutStream().writeByte(j);
		}
		c.flushOutStream();

	}

	public Client getPlayer() {
		return c;
	}

	/**
	 * Displays one item on an interface
	 * @param interfaceId - id of the interface
	 * @param itemId - ID of the item to display on the interface
	 */
	public void itemOnInterface(int interfaceId, int itemId) {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}

		// synchronized (c) {
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(interfaceId);
		c.getOutStream().writeShort(1);
		c.getOutStream().writeByte(1);
		c.getOutStream().write3Byte(itemId);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public void itemsOnInterface(int interfaceId, int[] items, int[] counts) {
		itemsOnInterface(interfaceId, items, counts, 0);
	}

	public void itemsOnInterface(int interfaceId, int[] items, int[] counts, int offset) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		int length = items.length;
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(interfaceId);
		c.getOutStream().writeShort(length);
		for (int i = 0; i < length; i++) {
			int id = items[i];
			int amount = counts[i];
			if (amount > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(amount);
			} else {
				c.getOutStream().writeByte(amount);
			}
	
			c.getOutStream().write3Byte(id + offset);
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void itemsOnInterface2(int interfaceId, int[] items, int[] counts) {
		itemsOnInterface(interfaceId, items, counts, 1);
	}

	public void itemsOnInterface(int interfaceId, List<Item> items) {
		itemsOnInterface(interfaceId, items, 0);
	}
	
	public void itemsOnInterface(int interfaceId, List<Item> items, int offset) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		int length = items.size();
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(interfaceId);
		c.getOutStream().writeShort(length);
		for (int i = 0; i < length; i++) {
			Item item = items.get(i);
			int itemId = item.getId();
			int itemAmount = item.getAmount();
			if (itemAmount > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(itemAmount);
			} else {
				c.getOutStream().writeByte(itemAmount);
			}

			c.getOutStream().write3Byte(itemId + offset);
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendInventory(int interfaceId, Item[] items) {
		sendInventory(interfaceId, items, 0);
	}

	public void sendInventory(int interfaceId, Item[] items, int offset) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		int length = items.length;
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(interfaceId);
		c.getOutStream().writeShort(length);
		for (int i = 0; i < length; i++) {
			Item item = items[i];
			if (item == null) {
				c.getOutStream().writeByte(0);
				c.getOutStream().write3Byte(0);
				continue;
			}

			int itemId = item.getId();
			int itemAmount = item.getAmount();
			if (itemAmount > 254) {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeDWord_v2(itemAmount);
			} else {
				c.getOutStream().writeByte(itemAmount);
			}

			c.getOutStream().write3Byte(itemId + offset);
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void itemsOnInterfaceNoCount(int interfaceId, List<Integer> items) {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}

		// synchronized (c) {
		c.getOutStream().createVariableShortPacket(53);
		c.getOutStream().write3Byte(interfaceId);
		c.getOutStream().writeShort(items.size());
		for (int i = 0, l = items.size(); i < l; i++) {
			Integer item = items.get(i);
			c.getOutStream().writeByte(1);
			c.getOutStream().write3Byte(item);
		}
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
		// }
	}

	public void multiWay(int i1) {
		if (c.isBot()) {
			return;
		}
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		// synchronized(c) {
		if (c.multiWay == i1) {
			return;
		}
		c.multiWay = i1;
		c.getOutStream().createPacket(61);
		c.getOutStream().writeByte(i1);
		c.flushOutStream();
		// c.updateRequired = true;
		// c.setAppearanceUpdateRequired(true);

	}

	public void removeObjects(int chunkX, int chunkY, int height) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(153);
			c.getOutStream().writeByte(chunkX);
			c.getOutStream().writeByte(chunkY);
			c.getOutStream().writeByte(height);
			c.flushOutStream();
		}
	}

	public void clearRegion() {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(154);
			c.flushOutStream();
		}
	}
	
	public void sendDialogInterface(int i) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(218);
			c.getOutStream().writeLEShortA(i);
			c.flushOutStream();
		}
	}

	public void sendChatInterface(int interfaceId) {
		sendChatInterface(interfaceId, true);
	}

	public void sendChatInterface(int interfaceId, boolean closeInterfaces) {
		if (c == null || !c.isActive || c.getOutStream() == null) {
			return;
		}
		
		c.setChatInterfaceOpen(interfaceId);

		c.getOutStream().createPacket(164);
		c.getOutStream().writeShort(interfaceId);
		c.getOutStream().writeByte(closeInterfaces ? 1 : 0);
		c.flushOutStream();
	}

	public void sendHpHud(String name, int currentHp, int maxHp) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(9);
		c.getOutStream().writeString(name);
		c.getOutStream().writeShort(currentHp);
		c.getOutStream().writeShort(maxHp);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}
	
	public void sendDiscordUpdate(String state, String details, int partySize, int maxPartySize, long startTimestamp, long endTimestamp) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(10);
		c.getOutStream().writeString(state); //	the user's current party status	- "Looking to Play", "Playing Solo", "In a Group"
		c.getOutStream().writeString(details); // what the player is currently doing - "Competitive - Captain's Mode", "In Queue", "Unranked PvP"
		c.getOutStream().writeShort(partySize); // the size of the party if there is one
		c.getOutStream().writeShort(maxPartySize); // max size of party (partySize of MaxPartySize) - if 0 then it won't display party size.
		c.getOutStream().writeLong(startTimestamp); //Sending startTimestamp will show "elapsed" as long as there is no endTimestamp sent in this packet.
		c.getOutStream().writeLong(endTimestamp); //Sending endTimestamp will always have the time displayed as "remaining" until the given time.
		c.getOutStream().writeString(""); // smallImageKey unused right now, but can use to display wildy skull or ranking, etc...
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendHpOrbState() {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(16);
		c.getOutStream().writeByte(c.getDotManager().isVenomActive() ? 2 : c.getDotManager().isPoisonActive() ? 1 : 0);
		c.getOutStream().endFrameVarSizeWord();
	}

	private void sendNightmareUpdate(Consumer<Client> extraData) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(17);
		extraData.accept(c);
		c.getOutStream().endFrameVarSizeWord();
	}

	public void sendNightmareBars(int swMax, int swLp, int seMax, int seLp, int nwMax, int nwLp, int neMax, int neLp, boolean instant) {
		sendNightmareUpdate(client -> {
			c.getOutStream().writeByte(0);

			c.getOutStream().writeShort(swMax);
			c.getOutStream().writeShort(swLp);
			
			c.getOutStream().writeShort(seMax);
			c.getOutStream().writeShort(seLp);
			
			c.getOutStream().writeShort(nwMax);
			c.getOutStream().writeShort(nwLp);
			
			c.getOutStream().writeShort(neMax);
			c.getOutStream().writeShort(neLp);

			c.getOutStream().writeByte(instant ? 1 : 0);
		});
	}

	public void sendNightmareFadeOut() {
		sendNightmareUpdate(client -> c.getOutStream().writeByte(1));
	}

	public void sendInstancesUpdate() {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(18);

		int length = InstanceType.values.length;
		c.getOutStream().writeByte(length);
		c.getOutStream().writeByte(c.selectedInstanceType == null ? -1 : c.selectedInstanceType.getIndex());
		for (int i = 0; i < length; i++) {
			InstanceType type = InstanceType.values[i];
			if (type.isDisabled()) {
				c.getOutStream().writeString("<str>" + type.getName());
			} else {
				c.getOutStream().writeString(type.getName());
			}
		}

		c.getOutStream().endFrameVarSizeWord();
	}

	private void sendPresetData(Consumer<Client> extraData) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(19);
		extraData.accept(c);
		c.getOutStream().endFrameVarSizeWord();
	}

	public void sendPresetInventory(List<Item> items) {
		sendPresetData(client -> {
			c.getOutStream().writeByte(0);

			if (items == null) {
				for (int i = 0; i < 28; i++) {
					c.getOutStream().write3Byte(-1);
					c.getOutStream().writeInt(0);
				}
			} else {
				for (int i = 0, length = items.size(); i < length; i++) {
					Item item = items.get(i);
					c.getOutStream().write3Byte(item.getId() - 1);
					c.getOutStream().writeInt(item.getAmount());
				}
			}
		});
	}

	public void sendPresetEquipment(List<Item> items) {
		sendPresetData(client -> {
			c.getOutStream().writeByte(1);

			if (items == null) {
				for (int i = 0; i < 14; i++) {
					c.getOutStream().write3Byte(-1);
					c.getOutStream().writeInt(0);
				}
			} else {
				for (int i = 0, length = items.size(); i < length; i++) {
					Item item = items.get(i);
					c.getOutStream().write3Byte(item.getId());
					c.getOutStream().writeInt(item.getAmount());
				}
			}
		});
	}

	public void sendPresetsList() {
		sendPresetData(client -> {
			c.getOutStream().writeByte(2);

			int length = c.presets.size();
			c.getOutStream().writeByte(length);
			c.getOutStream().writeByte(c.selectedPreset == null ? -1 : c.selectedPreset.getIndex());
			for (int i = 0; i < length; i++) {
				Preset preset = c.presets.get(i);
				c.getOutStream().writeString(preset.getName());
			}
		});
	}

	public void sendSharedPresetsList() {
		sendPresetData(client -> {
			c.getOutStream().writeByte(3);

			int length = c.sharedPresets.size();
			c.getOutStream().writeByte(length);
			c.getOutStream().writeByte(c.selectedSharedPreset == null ? -1 : c.selectedSharedPreset.getIndex());
			for (int i = 0; i < length; i++) {
				Preset preset = c.sharedPresets.get(i);
				c.getOutStream().writeString(preset.getName());
			}
		});
	}

	public void sendPowerUpData() {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(21);

		int length = PowerUpData.values.length;
		c.getOutStream().writeByte(c.getSeasonalData().powerUpSortType.getId());
		c.getOutStream().writeByte(length);
		for (int i = 0; i < length; i++) {
			PowerUpData powerUp = PowerUpData.values[i];
			PowerUpRarity rarity = powerUp.getRarity();
			c.getOutStream().writeString(powerUp.getName());
			c.getOutStream().writeString(powerUp.getDescription());
			c.getOutStream().writeByte(c.getSeasonalData().getPowerUpAmount(powerUp));
			c.getOutStream().writeByte(powerUp.getAmount());
			c.getOutStream().writeInt(rarity.getRgb());
			c.getOutStream().writeShort(powerUp.getImageId());
			c.getOutStream().writeByte(rarity.getId());
		}

		c.getOutStream().endFrameVarSizeWord();
	}

	public void sendPowerUpUnlock(List<PowerUpData> powerUps, PowerUpData rolled) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(22);
		c.getOutStream().writeString(rolled.getName());
		c.getOutStream().writeShort(rolled.getImageId());
		for (int i = 0; i < 8; i++) {
			c.getOutStream().writeShort(powerUps.get(i).getImageId());
		}

		c.getOutStream().endFrameVarSizeWord();
	}

	private void sendGambleData(Consumer<Client> extraData) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(20);
		extraData.accept(c);
		c.getOutStream().endFrameVarSizeWord();
	}

	public void sendWaitButton(boolean previewScreen) {
		sendGambleData(client -> {
			c.getOutStream().writeByte(0);
			c.getOutStream().writeByte(previewScreen ? 1 : 0);
		});
	}

	public void sendTaskData() {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;
		if (c.categories == null) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeByte(c.categories.length);
		for (int i = 0; i < c.categories.length; i++) {
			PlayerCategory category = c.categories[i];
			for (int j = 0; j < PlayerCategory.TASK_LENGTH; j++) {
				PlayerTask task = category.getTask(j);
				c.getOutStream().writeInt(task.getCompletedCount());
				c.getOutStream().writeInt(task.getRequiredCount());
				c.getOutStream().writeString(task.getDesc());
			}
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendSidebarData() {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;
		if (c.categories == null) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(2);
		c.getOutStream().writeByte(c.categories.length);
		for (int i = 0; i < c.categories.length; i++) {
			PlayerCategory category = c.categories[i];
			c.getOutStream().writeShort(category.getIconId());
			c.getOutStream().writeString(category.getName());
			int completed = 0;
			for (int j = 0; j < PlayerCategory.TASK_LENGTH; j++) {
				PlayerTask task = category.getTask(j);
				if (task.isCompleted()) {
					completed++;
				}
			}

			c.getOutStream().writeByte(completed);
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendBlacklistData() {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(3);
		c.getOutStream().writeByte(c.getWildyBlackList().size());
		for (int i = 0, l = c.getWildyBlackList().size(); i < l; i++) {
			c.getOutStream().writeString(c.getWildyBlackList().get(i).getName());
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}
	
	public void sendExpDrop(int skillId, int exp) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(4);
		c.getOutStream().writeByte(skillId);
		c.getOutStream().writeInt(exp);

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void updateInterfaceEmpty(int id) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(id);
		c.getOutStream().endFrameVarSizeWord();
	}

	public void sendSummoningOrbIndex(int id) {
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(6);
		c.getOutStream().writeByte(id);
		c.getOutStream().endFrameVarSizeWord();
	}

	public void updateRaidsHud(int totalPoints, int points, int time) {
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(8);
		c.getOutStream().writeInt(totalPoints);
		c.getOutStream().writeInt(points);
		c.getOutStream().writeShort(time);
		c.getOutStream().endFrameVarSizeWord();
	}

	public void sendVoteData(Poll poll) {
		if (poll == null) {
			return;
		}

		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(0);
		c.getOutStream().writeByte(c.votedInPoll);
		c.getOutStream().writeString(poll.getTitle());
		c.getOutStream().writeString(poll.getInfo());
		c.getOutStream().writeByte(poll.getPos());
		for(int i = 0; i < poll.getPos(); i++) {
			PollEntry entry = poll.getEntry(i);
			c.getOutStream().writeString(entry.getQuestion());
			c.getOutStream().writeShort(entry.getYesAmount());
			c.getOutStream().writeShort(entry.getNoAmount());
			c.getOutStream().writeShort(entry.getSkipAmount());
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}
	
	public void sendSeasonPassData(List<Reward> list) {
		if (list.isEmpty()) {
			c.sendMessage("No rewards to update.");
			return;
		}
		
		int listSize = list.size();
		
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(11);
		c.getOutStream().writeString(SeasonPassManager.title);
		c.getOutStream().writeShort(SeasonPassManager.getDaysLeft());
		c.getOutStream().writeShort(c.seasonPass.experience);
		c.getOutStream().writeByte(listSize);
		for(int i = 0; i < listSize; i++) {
			Reward entry = list.get(i);
			c.getOutStream().writeShort(entry.item.getId());
			c.getOutStream().writeShort(entry.levelRequired);
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendVoteInterfaceLuck(int slot, int itemId, int slot2, int itemId2) {
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(12);
		c.getOutStream().writeByte(slot);
		c.getOutStream().write3Byte(itemId);
		c.getOutStream().writeByte(slot2);
		c.getOutStream().write3Byte(itemId2);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendVoteInterfaceStreak() {
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(13);
		c.getOutStream().writeShort(Variables.VOTE_STREAK.getValue(c));

		for (int i = 0, length = VoteInterfaceStreak.values.length; i < length; i++) {
			VoteInterfaceStreak streak = VoteInterfaceStreak.values[i];
			c.getOutStream().writeShort(streak.getVotesRequired());
			c.getOutStream().writeString(streak.getTitle());
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}
	
	public void sendDonationInterfaceData() {
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(14);

		if (DonationInterface.getCategories().isEmpty()) {
			c.getOutStream().writeByte(0);
			c.getOutStream().writeByte(0);
			c.getOutStream().writeByte(-1);
		} else {
			DonationInterfaceCategory selectedCategory = DonationInterface.getCategories().get(c.donationInterfaceSelectedCategory);
			int tileCount = selectedCategory.getType().getElements();
			int startIndex = c.donationInterfaceSelectedPage * tileCount;
			int endIndex = Math.min(startIndex + tileCount, selectedCategory.getEntries().size());

			int length = endIndex - startIndex;
			c.getOutStream().writeByte(selectedCategory.getType().getId());
			c.getOutStream().writeByte(length);
			c.getOutStream().writeByte(c.donationInterfaceSelectedEntry);
			for (int i = startIndex; i < endIndex; i++) {
				DonationInterfaceCategoryEntry entry = selectedCategory.getEntries().get(i);
				c.getOutStream().writeShort(entry.getPrice());
				int mask = 0;
				if (entry.getDiscountPrice() > 0) {
					mask |= 0x1;
				}

				if (entry.isPet()) {
					mask |= 0x2;
				}

				c.getOutStream().writeByte(mask);
				if (entry.getDiscountPrice() > 0) {
					c.getOutStream().writeShort(entry.getDiscountPrice());
				}

				if (entry.isPet()) {
					PokemonData data = PokemonData.data[entry.getDisplayId() - 1];
					c.getOutStream().writeShort(data.displayZoom);
					c.getOutStream().write3Byte(data.npcId);
				} else {
					c.getOutStream().write3Byte(entry.getDisplayId());
				}

				c.getOutStream().writeString(entry.getTitle());
			}
		}

		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}
	
	public void sendDonationInterfaceCategoryData() {
		c.getOutStream().createVariableShortPacket(129);
		c.getOutStream().writeByte(15);
		c.getOutStream().writeByte(c.donationInterfaceSelectedCategory);

		int length = DonationInterface.getCategories().size();
		c.getOutStream().writeByte(length);
		for (int i = 0; i < length; i++) {
			c.getOutStream().writeString(DonationInterface.getCategories().get(i).getTitle());
		}
		
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendConfig(int id, int value) {
		if (value < 128) {
			setConfig(id, value);
		} else {
			sendFrame87(id, value);
		}
	}

	public void sendConstructMapRegionPacket(Room[][][] palettes, HouseStyle style) {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createVariableShortPacket(241);
		c.getOutStream().writeShortA(c.teleportToY > 0 ? c.teleportToY >> 3 : c.mapRegionY + 6);
		c.getOutStream().initBitAccess();

		for (int z = 0; z < 4; z++) {
			for (int x = 0; x < 13; x++) {
				for (int y = 0; y < 13; y++) {
					Room room = palettes[z][x][y];

					boolean skipFloor = c.getZ() != z;

					c.getOutStream().writeBits(1, skipFloor ? 0 : room != null ? 1 : 0);

					if (room != null && !skipFloor) {
						Palette palette = room.getPalette();
						c.getOutStream().writeBits(28, palette.hash(style.getRegionId()));
					}
				}
			}
		}
		c.getOutStream().finishBitAccess();
		c.getOutStream().writeShort(c.teleportToX > 0 ? c.teleportToX >> 3 : c.mapRegionX + 6);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendConstructMapRegionPacket(Poh house) {
		if (c.getOutStream() == null || !c.isActive || c.isBot()) {
			return;
		}

		c.getOutStream().createVariableShortPacket(241);
		c.getOutStream().writeShortA(c.teleportToY > 0 ? c.teleportToY >> 3 : c.mapRegionY + 6);
		c.getOutStream().initBitAccess();

		for (int z = 0; z < 4; z++) {
			for (int x = 0; x < 13; x++) {
				for (int y = 0; y < 13; y++) {
					Room room = house.getRoom(x, y, z);

					boolean skipFloor = c.getZ() != z;

					c.getOutStream().writeBits(1, skipFloor ? 0 : room != null ? 1 : 0);

					if (room != null && !skipFloor) {
						HouseStyle style = house.getHouseStyle();
						Palette palette = room.getPalette();
						c.getOutStream().writeBits(28, palette.hash(style.getRegionId()));
					}
				}
			}
		}
		c.getOutStream().finishBitAccess();
		c.getOutStream().writeShort(c.teleportToX > 0 ? c.teleportToX >> 3 : c.mapRegionX + 6);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendConstructMapRegionPacket(int height) {
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}

		int centerY = c.mapRegionY + 6;
		int centerX = c.mapRegionX + 6;
		c.getOutStream().createVariableShortPacket(241);
		c.getOutStream().writeShortA(centerY);
		c.getOutStream().initBitAccess();
		for (int z = 0; z < 4; z++) {
			for (int x = centerX - 6; x <= centerX + 6; x++) {
				int regionX = x >> 3;
				for (int y = centerY - 6; y <= centerY + 6; y++) {
					int regionY = y >> 3;
					Region region = Server.getRegionManager().getRegionByLocation(x << 3, y << 3, height);

					int newChunkX = 0;
					int newChunkY = 0;
					int newPlane = 0;
					int rotation = 0;
					if (region.getDynamicData() != null) {
						int[] palette = region.getDynamicData()[z][x - (regionX << 3)][y - (regionY << 3)];

						newChunkX = palette[0];
						newChunkY = palette[1];
						newPlane = palette[2];
						rotation = palette[3];
					/*} else {
						newChunkX = x;
						newChunkY = y;
						newPlane = z;
					*/}

					if (newChunkX == 0 || newChunkY == 0) {
						c.getOutStream().writeBits(1, 0);
					} else {
						c.getOutStream().writeBits(1, 1);
						c.getOutStream().writeBits(28, (rotation << 1) | (newPlane << 26) | (newChunkX << 15) | (newChunkY << 3));
					}
				}
			}
		}
		c.getOutStream().finishBitAccess();
		c.getOutStream().writeShort(centerX);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendEnterAmountInterface() {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(27);
			c.flushOutStream();
		}
	}

	public void sendEnterNameInterface() {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(187);
			c.flushOutStream();
		}
	}

	public void sendFrame106(int sideIcon) { // set sidebar
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(106);
			c.getOutStream().writeByteC(sideIcon);
			c.flushOutStream();
		}
	}

	public void setSidebarInterface(int menuId, int form) {
		if (c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.sidebarInterfaceInfo.put(menuId, form);
		if (c.getOutStream() != null && c.isActive) {
			c.getOutStream().createPacket(71);
			c.getOutStream().write3Byte(form);
			c.getOutStream().writeByte(menuId);
			c.flushOutStream();
		}
	}

	public void camReset() {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(107);
			c.flushOutStream();
		}
	}
	
	public void sendRunEnergy() {
		if (!c.restoreEnergy) {
			return;
		}
		c.restoreEnergy = false;
		if (c.isBot()) {
			return;
		}

		if (c != null && c.getOutStream() != null && c.isActive) {
			c.getOutStream().createPacket(110);
			c.getOutStream().writeByte((int)c.getRunEnergy());
			c.flushOutStream();
		}
	}

	public void setVisibility(int id, boolean hidden) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(127);
			c.getOutStream().writeInt(id);
			c.getOutStream().writeByte(hidden ? 1 : 0);
			c.flushOutStream();
		}
	}

	public void sendFrame126(String s, int id) {
		if (s == null || c == null || c.isBot() || !c.isActive || c.getOutStream() == null) {
			return;
		}

		if (c.wildLevel < 1 && !c.checkPacket126Update(s, id)) {
			return;
		}

		c.getOutStream().createVariableShortPacket(126);
		c.getOutStream().writeString(s);
		c.getOutStream().writeInt(id);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void openUrl(String url) {
		openUrl(url, false);
	}

	public void openUrl(String url, boolean spam) {
		if (url == null || c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createVariableShortPacket(128);
		c.getOutStream().writeString(url);
		c.getOutStream().writeByte(spam ? 1 : 0);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendFrame126b(String s, int id) {
		if (s == null || c == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		try {
			c.getOutStream().createVariableShortPacket(126);
			c.getOutStream().writeString(s);
			c.getOutStream().writeInt(id);
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		} catch (Exception e) {
			System.out.println("SendFrame126 ERROR");
			e.printStackTrace();
		}
	}

	public void sendLmsFog(int coordX, int coordZ, int radius) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(133);
		c.getOutStream().writeByte(1);
		c.getOutStream().writeShort(coordX);
		c.getOutStream().writeShort(coordZ);
		c.getOutStream().writeByte(radius);
		c.getOutStream().endFrameVarSize();
		c.flushOutStream();
	}

	public void resetFog() {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(133);
		c.getOutStream().writeByte(-1);
		c.getOutStream().endFrameVarSize();
		c.flushOutStream();
	}

	public void nightmareFog() {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(133);
		c.getOutStream().writeByte(2);
		c.getOutStream().endFrameVarSize();
		c.flushOutStream();
	}

	public void sendChat(String name, int priv, byte[] text, int textSize) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		try {
			c.getOutStream().createVariableShortPacket(132);
			c.getOutStream().writeString(name);
			c.getOutStream().writeByte(priv);
			c.getOutStream().writeByte(textSize);
			c.getOutStream().writeBytes_reverse(text, textSize, 0);
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendDynamicComponentText(String s, int id, int componentId) {
		if (s == null || c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		try {
			c.getOutStream().createVariableShortPacket(131);
			c.getOutStream().writeString(s);
			c.getOutStream().writeShort(id);
			c.getOutStream().writeShort(componentId);
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendFrame171(int true1False0, int interfaceId) { // Sets an
		// interface
		// to be
		// hidden
		// until
		// hovered
		// over.
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(171);
			c.getOutStream().writeByte(true1False0);
			c.getOutStream().writeShort(interfaceId);
			c.flushOutStream();

		}
	}

	// public void sendClan(String name, String message, String clan, int
	// rights) {
	// name = name.substring(0, 1).toUpperCase() + name.substring(1);
	// message = message.substring(0, 1).toUpperCase() + message.substring(1);
	// clan = clan.substring(0, 1).toUpperCase() + clan.substring(1);
	// if (c.isActive) {
	// c.getOutStream().createVariableShortPacket(217);
	// c.getOutStream().writeString(name);
	// c.getOutStream().writeString(message);
	// c.getOutStream().writeString(clan);
	// c.getOutStream().writeShort(rights);
	// c.getOutStream().endFrameVarSize();
	// c.flushOutStream();
	// }
	// }

	public void showPlayerHeadModelOnInterfacePacket(int interfaceId) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(185);
			c.getOutStream().writeLEShortA(interfaceId);
			c.flushOutStream();
		}
	}

	public void showAnimationOnInterfacePacket(int interfaceId, int animationId) { // animate
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(200);
			c.getOutStream().writeShort(interfaceId);
			c.getOutStream().writeShort(animationId);
			c.flushOutStream();
		}
	}

	// Draw Item on interface with a model zoom
	public void sendFrame246(int interfaceId, int modelZoom, int itemId) {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(246);
			c.getOutStream().writeLEShort(interfaceId);
			c.getOutStream().writeShort(modelZoom);
			c.getOutStream().write3Byte(itemId);
			c.flushOutStream();

		}
	}
	
	// draw customized items on interface
	public void sendRecolorItem(int interfaceId, int itemId, short... colors) {
		if (c.getOutStream() != null && c.isActive) {
			c.getOutStream().createFrameVarSize(247);
			c.getOutStream().writeShort(interfaceId);
			c.getOutStream().write3Byte(itemId);
			c.getOutStream().writeByte(colors.length);
			for (int i = 0; i < colors.length; i++) {
				c.getOutStream().writeByte(i);
				c.getOutStream().writeShort(colors[i]);
			}
			c.getOutStream().endFrameVarSize();
			c.flushOutStream();
		}
	}
	

	public void openItemsInterface(int mainFrame, int invFrame) {
		sendFrame248(mainFrame, invFrame);
	}

	public void closeInterfaces() {
		c.interfaceIdOpenMainScreen = 0;
		if (!c.isBot() && c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(219);
			c.flushOutStream();
		}
	}

	public void sendFrame248(int modalId, int sideId) {
		if (c == null || !c.isActive || c.getOutStream() == null) {
			return;
		}

		c.getOutStream().createPacket(248);
		c.getOutStream().write3Byte(modalId);
		c.interfaceIdOpenMainScreen = modalId;
		c.getOutStream().write3Byte(sideId);
		c.sidebarInterfaceOpen = sideId;
		c.flushOutStream();
	}

	public void setSidebarInterfaceAndRemoveButtons(int interfaceId) {
		if (c.getOutStream() != null && c.isActive) {

			if (c.getDialogueBuilder().isActive()) {
				c.getDialogueBuilder().setClose(false);
			}

			c.getOutStream().createPacket(142);
			c.getOutStream().writeLEShort(interfaceId);
			c.flushOutStream();
		}
	}

	public void sendFrame34(int id, int slot, int column, int amount) {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createVariableShortPacket(34); // init item to smith
			// screen
			c.getOutStream().writeShort(column); // Column Across Smith Screen
			c.getOutStream().writeByte(4); // Total Rows?
			c.getOutStream().writeInt(slot); // Row Down The Smith Screen
			c.getOutStream().write3Byte(id + 1); // item
			if (amount < 255)
				c.getOutStream().writeByte(amount); // how many there are?
			else {
				c.getOutStream().writeByte(255);
				c.getOutStream().writeInt(amount);
			}
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}

	}

	public void sendFrame70(int i, int o, int id) {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(70);
			c.getOutStream().writeShort(i);
			c.getOutStream().writeShort(o);
			c.getOutStream().writeLEShort(id);
			c.flushOutStream();
		}

	}

	// Draw an NPC on an interface
	public void drawNpcOnInterface(int npcId, int interfaceId) {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(75);
			c.getOutStream().write3Byte(npcId);
			c.getOutStream().writeLEShortA(interfaceId);
			c.flushOutStream();
		}

	}

	public void sendFrame87(int id, int state) {
		// synchronized(c) {
		if (c.isBot()) {
			return;
		}

		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(87);
			c.getOutStream().writeWordBigEndian_dup(id);
			c.getOutStream().writeDWord_v1(state);
			c.flushOutStream();
		}

	}

	public void sendMessage(int type, String message) {
		sendMessage(type, message, null);
	}

	public void sendMessage(int type, String message, String name) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createFrameVarSize(253);
		c.getOutStream().writeByte(type);
		if (name == null) {
			c.getOutStream().writeByte(0);
		} else {
			c.getOutStream().writeByte(1);
			c.getOutStream().writeString(name);
		}

		c.getOutStream().writeString(message);
		c.getOutStream().endFrameVarSize();
		c.flushOutStream();
	}

	public void updateAnnouncement(String s) {
		if (c.isBot() || c.getOutStream() == null || !c.isActive) {
			return;
		}

		if (c.getVariables()[Variables.ANNOUNCEMENT_MUTE.ordinal()] == 1) {
			s = "";
		}

		c.getOutStream().createFrameVarSize(130);
		c.getOutStream().writeString(s);
		c.getOutStream().endFrameVarSize();
		c.flushOutStream();
	}

	public void sendPM(long name, int rights, byte[] chatmessage, int messagesize) {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createFrameVarSize(196);
			c.getOutStream().writeLong(name);
			c.getOutStream().writeInt(c.lastChatId++);
			c.getOutStream().writeByte(rights);
			c.getOutStream().writeBytes(chatmessage, messagesize, 0);
			c.getOutStream().endFrameVarSize();
			c.flushOutStream();
			// String chatmessagegot = Misc.textUnpack(chatmessage,
			// messagesize);
			// String target = Misc.longToPlayerName(name);
		}

	}

	public void sendQuest(String s, int i) {
		if (c.isBot()) {
			return;
		}
		if (c.getOutStream() == null || !c.isActive) {
			return;
		}
		c.getOutStream().createVariableShortPacket(126);
		c.getOutStream().writeString(s);
		c.getOutStream().writeInt(i);
		c.getOutStream().endFrameVarSizeWord();
		c.flushOutStream();
	}

	public void sendStillGraphics(int id, int heightS, int y, int x, int timeBCS) {
		if (c.isBot()) {
			return;
		}

		if (c.getOutStream() == null || !c.isActive) {
			return;
		}

		c.getOutStream().createPacket(85);
		c.getOutStream().writeByteC(y - (c.mapRegionY * 8));
		c.getOutStream().writeByteC(x - (c.mapRegionX * 8));
		c.getOutStream().createPacket(4);
		c.getOutStream().writeByte(0);// Tiles away (X >> 4 + Y & 7)
		// //Tiles away from
		// absX and absY.
		c.getOutStream().writeShort(id); // Graphic ID.
		c.getOutStream().writeByte(heightS); // Height of the graphic when
		// cast.
		c.getOutStream().writeShort(timeBCS); // Time before the graphic
		// plays.
		c.flushOutStream();
	}

	public void sendString(final String s, final int id) {
		c.getPacketSender().sendFrame126(s, id);
		// if (c != null && c.isActive && c.getOutStream() != null) {
		// c.getOutStream().createVariableShortPacket(126);
		// c.getOutStream().writeString(s);
		// c.getOutStream().writeInt(id);
		// c.getOutStream().endFrameVarSizeWord();
		// c.flushOutStream();
		// }

	}

	public void setChatOptions(int publicChat, int privateChat, int tradeBlock) {
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(206);
			c.getOutStream().writeByte(publicChat);
			c.getOutStream().writeByte(privateChat);
			c.getOutStream().writeByte(tradeBlock);
			c.flushOutStream();
		}

	}

	public void setConfig(int id, int state) {
		if (c.isBot())
			return;
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(36);
			c.getOutStream().writeLEShort(id);
			c.getOutStream().writeByte(state);
			c.flushOutStream();
		}
	}

	public void sendWidget(DisplayTimerType type, int ticks) {
		if (c.isBot() || !c.isActive || c.getOutStream() == null) {
			return;
		}
		c.getOutStream().createPacket(178);
		c.getOutStream().writeByte(type.getId());
		c.getOutStream().writeShort(ticks);
		c.flushOutStream();
	}

	public void loadPM(long playerName, int world) {
		if (c.isBot()) {
			return;
		}

		if (c.getOutStream() != null && c.isActive) {

			if (world != 0) {
				world += 9;
			}

			c.getOutStream().createPacket(50);
			c.getOutStream().writeLong(playerName);
			c.getOutStream().writeByte(world);
			c.flushOutStream();
		}
	}
	
	public void setPrivateMessaging(int i) { // friends and ignore list status
		// synchronized(c) {
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createPacket(221);
			c.getOutStream().writeByte(i);
			c.flushOutStream();
		}

	}

	public void setSkillLevel(int skillNum, int currentLevel, int XP) {
		if (c.isBot() || !c.isActive || c.getOutStream() == null) {
			return;
		}

		if (skillNum == Skills.HITPOINTS) {
			c.getOutStream().createVariableShortPacket(129);
			c.getOutStream().writeByte(23);
			c.getOutStream().writeShort(currentLevel);
			c.getOutStream().writeShort(c.calculateMaxLifePoints());
			c.getOutStream().endFrameVarSizeWord();
		}

		c.getOutStream().createPacket(134);
		c.getOutStream().writeByte(skillNum);
		c.getOutStream().writeDWord_v1(XP);
		c.getOutStream().writeByte(currentLevel);
		c.flushOutStream();
	}

	public void setScrollPos(int interfaceId, int scrollPos) {
		// synchronized (c){
		if (c.getOutStream() != null && c != null && c.isActive) {
			c.getOutStream().createPacket(79);
			c.getOutStream().writeLEShort(interfaceId);
			c.getOutStream().writeShortA(scrollPos);
			c.flushOutStream();
		}
		// }
	}

	public void showInterface(int interfaceid) {
		if (c.insideDungeoneering() && !c.dungParty.dungManager.loaded) {
			return;
		}

		if (!c.isActive || c.getOutStream() == null) {
			return;
		}

		if (c.getDialogueBuilder().isActive()) {
			c.getDialogueBuilder().setClose(false);
		}

		c.interfaceIdOpenMainScreen = interfaceid;

		if (c.isBot()) {
			return;
		}

		c.getOutStream().createPacket(97);
		c.getOutStream().writeInt(interfaceid);
		c.flushOutStream();
	}

	public void showOverlay(int id) {
		if (!c.isActive || c.getOutStream() == null || c.isBot()) return;

		c.getOutStream().createPacket(208);
		c.getOutStream().writeInt(id);
		c.flushOutStream();
	}

	public void showOption(int index, int lowPrio, String op) {
		if (c == null || c.isBot() || !c.isActive || c.getOutStream() == null || !c.getPA().checkUpdateOption(op, index)) {
			return;
		}

		c.getOutStream().createFrameVarSize(104);
		c.getOutStream().writeByteC(index);
		c.getOutStream().writeByteA(lowPrio);
		c.getOutStream().writeString(op);
		c.getOutStream().endFrameVarSize();
		c.flushOutStream();
	}
	
	public void sendIgnoreList(long[] ignore) {
		if (c.isBot()) {
			return;
		}
		if (c != null && c.isActive && c.getOutStream() != null) {
			c.getOutStream().createVariableShortPacket(214);
			for (long l : ignore)
				if (l > 0)
					c.getOutStream().writeLong(l);
			c.getOutStream().endFrameVarSizeWord();
			c.flushOutStream();
		}
	}

	public void sendInputDialogState(int state) {
		c.getPacketSender().sendConfig(ConfigCodes.OPEN_CHATBOX_INTERFACE, state);
	}

}
