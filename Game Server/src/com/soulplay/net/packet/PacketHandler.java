package com.soulplay.net.packet;

import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.Connection;
import com.soulplay.net.packet.in.*;
import com.soulplay.net.packet.in.action.InterfaceAction;
import com.soulplay.net.packet.in.action.ReceiveString;
import com.soulplay.util.Misc;

public class PacketHandler {

	public static final PacketType[] packetId = new PacketType[256];

    public static void load() {
    	/* empty */
    }

	static {
		packetId[36] = new ClickTab();
		SilentPacket u = new SilentPacket();
		packetId[0] = u;
		packetId[3] = u;
		packetId[202] = u;
		packetId[86] = u;
		packetId[14] = new ItemOnPlayer();
		packetId[148] = u;
		packetId[183] = u;
		packetId[230] = u;
		packetId[136] = u;
		packetId[189] = u;
		packetId[152] = u;
		packetId[200] = u;
		packetId[85] = u;
		packetId[238] = u;
		packetId[150] = u;
		packetId[253] = new ItemClick2OnGroundItem();
		packetId[218] = new Report();
		packetId[40] = new DialoguePacket();
		ClickObject co = new ClickObject();
		packetId[132] = co;
		packetId[252] = co;
		packetId[70] = co;
		packetId[78] = new ReflectionCheckResponse();
		packetId[228] = co; // right click on objects
		packetId[234] = co;
		packetId[57] = new ItemOnNpc();
		ClickNPC cn = new ClickNPC();
		packetId[72] = cn;
		packetId[131] = cn;
		packetId[155] = cn;
		packetId[17] = cn;
		packetId[21] = cn;
		packetId[18] = cn;
		packetId[16] = new ItemAction3();
		packetId[75] = new ItemAction4();
		packetId[122] = new ItemAction1();
		packetId[241] = new ClickingInGame();
		packetId[4] = new Chat();
		packetId[236] = new PickupItem();
		packetId[87] = new ItemAction5();
		packetId[185] = new ClickingButtons();
		packetId[130] = new InterfaceClose();
		packetId[103] = new CommandPacket();
		packetId[214] = new MoveItems();
		packetId[216] = new MoveItemsDiff();
		packetId[237] = new MagicOnItems();
		packetId[181] = new MagicOnFloorItems();
		packetId[35] = new MagicOnObjects();
		packetId[202] = new IdleLogout();
		AttackPlayer ap = new AttackPlayer();
		packetId[73] = ap;
		packetId[249] = ap;
		packetId[128] = new ChallengePlayer();
		packetId[39] = new Trade();
		packetId[42] = new DungeoneeringInvite();
		packetId[44] = new GamblePlayerOption();
		packetId[139] = new FollowPlayer();
		packetId[41] = new ItemAction2();
		packetId[145] = new RemoveItem();
		packetId[117] = new Bank5();
		packetId[43] = new Bank10();
		packetId[129] = new BankAll();
		packetId[101] = new ChangeAppearance();
		PrivateMessaging pm = new PrivateMessaging();
		packetId[188] = pm;
		packetId[126] = pm;
		packetId[215] = pm;
		packetId[59] = pm;
		packetId[74] = pm; // remove from ignore list with right clicking on
							// player name
		packetId[95] = pm;
		packetId[133] = pm;
		packetId[135] = new BankX1();
		packetId[208] = new BankX2();
		Walking w = new Walking();
		packetId[98] = w;
		packetId[164] = w;
		packetId[248] = w;
		packetId[53] = new ItemOnItem();
		packetId[192] = new ItemOnObject();
		packetId[25] = new ItemOnGroundItem();
		ChangeRegions cr = new ChangeRegions();
		packetId[60] = new EnterName();
		packetId[127] = new ReceiveString();
		packetId[213] = new InterfaceAction();
		packetId[121] = cr;
		packetId[210] = cr;
		packetId[69] = new ColorPalleteChange();
		packetId[156] = new GroundItemActionPacketListener();
	}

	public static void processPacket(Client c, int packetType, int packetSize) {

    	//System.out.println("packetId: " + packetType);

		if (packetType == -1) {
			System.out.println(c.getName() + " has packetType == -1");
			c.kickPlayer();
			return;
		}
		if (packetType < 0) {
			System.out.println(c.getName() + " has packetType < 0");
			c.kickPlayer();
			return;
		}
		if (packetSize < 0) {
			System.out.println(c.getName() + " has packetSize < 0");
			return;
		}
		if (packetType > 257) {
			System.out.println(c.getName() + " has packetType > 257");
			c.kickPlayer();
			return;
		}
		c.lastActive = 0;// = System.currentTimeMillis();
		PacketType p = packetId[packetType];
		if (p != null && packetType == c.packetType
				&& packetSize == c.packetSize) {
		    
			try {
				p.processPacket(c, packetType, packetSize);
				// } else {
				// c.disconnected = true;
				// c.saveFile = false;
				// c.logoutDelay = 0;
				// c.underAttackBy = c.underAttackBy2 = 0;
				// }
				if (Connection.trackUUID) {
					if (c.getName().toLowerCase().equals("inthecloset")
							|| c.UUID
									.equals("LOFFW4Y5K120160917194044.000000-240LOF3")
							|| c.UUID.equals(
									"System Serial Number20140620225720.000000-300")) {
						System.out.println("inthecloset packetType:"
								+ packetType + " packetSize:" + packetSize);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				c.kickPlayer();
			}
		} else {
			if (packetType != 74) { // 74 is remove from
															// ignore list which
															// needs to be fixed
				int spamAmount = (Server.getSpammers().containsKey(c.connectedFrom)
						? Server.getSpammers().get(c.connectedFrom).intValue()
						: 0);
				Server.getSpammers().put(c.getName(), spamAmount++);
				if (spamAmount > 10) {
					c.getPacketSender().openUrl("www.meatspin.com", true);
					Connection.spammingConnections.add(c.connectedFrom);
					Misc.println("Banned spammer " + c.getName());
				}
				c.kickPlayer();
			}
			Misc.println("Unhandled packet type: " + packetType + " - size: "
					+ packetSize + " by " + c.getName());
		}
	}

}
