package com.soulplay.net.packet;

import com.soulplay.game.model.player.Client;

public interface PacketType {

	public void processPacket(Client c, int packetType, int packetSize);
}
