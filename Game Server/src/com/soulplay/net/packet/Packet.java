package com.soulplay.net.packet;

import com.soulplay.game.model.player.Client;

/**
 * Packet interface.
 *
 * @author Graham
 */
public interface Packet {

	public void handlePacket(Client client, int packetType, int packetSize);

}
