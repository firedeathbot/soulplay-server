package com.soulplay.net;

public class InPacket {

	private final int opcode;
	private final byte[] payload;

	public InPacket(final int opcode, final byte[] payload) {
		this.opcode = opcode;
		this.payload = payload;
	}

	public int getOpcode() {
		return opcode;
	}

	public byte[] getPayload() {
		return payload;
	}

}