package com.soulplay.net;

import com.soulplay.game.model.player.Client;
import com.soulplay.util.Misc;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.ReadTimeoutException;

public class ChannelHandler extends SimpleChannelInboundHandler<Object> {//??

	private Session session = null;

	public Session getSession() {
		return session;
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		if (session == null) {
			session = new Session(ctx.channel());
		}
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		if (session != null) {
//			final String ip = ((InetSocketAddress) ctx.channel()
//					.remoteAddress()).getAddress().getHostAddress()
//							.toString();
//			int connections = Connection.IP_TO_CONNECTED.getOrDefault(ip, 0);
//			if (connections > 0) {
//				Connection.IP_TO_CONNECTED.put(ip, connections - 1);
//			}
			
			Client client = session.getClient();
			if (client != null) {
				// if (client.reconnectPlayer) {
				//// System.out.println("don't kick reconnecting");
				// client.reconnectPlayer = false;
				// return;
				// }
				// ConnectionList.removeConnection(((InetSocketAddress)ctx.getChannel().getRemoteAddress()).getAddress().getHostAddress().toString());
				if (client.getClan() != null
						&& client.getClan().isLockedExitPortals()) {
					client.startLogoutDelay();
					client.applyDeath = true;
				}

				if (!client.properLogout) {
					// if (!client.forceClosedClient) {
					// client.logoutDelay = System.currentTimeMillis();
					// if (!client.isKicked/* &&
					// !Connection.inReconnectionList(client.getName())*/) {
					// client.reconnectPlayer = true;
					//// Connection.addToReconList(client.getName());
					// }
					// }

					Misc.println(
							"Forced disconnection of player " + client.getName()
									+ " - Poper logout:" + client.properLogout
									+ " reconnecting:" + client.reconnectPlayer
									+ " attemp:" + client.reconnectAttempts);

				}

				client.disconnected = true;

				// if (client.reconnectPlayer)
				// client.disconnected = false;
			}

			// if (client.reconnectPlayer) {
			// RS2LoginProtocol.sendReturnCode(session.getChannel(), 1);
			// }

			if (session.getChannel() != null) {
				session.getChannel().close();
			}
			session = null;
		}
	}

	@Override
	public void channelRead0(ChannelHandlerContext ctx, Object obj) {
		// if (e.getMessage() instanceof Client) {
		// session.setClient((Client) e.getMessage());
		// } else if (e.getMessage() instanceof Packet) {
		// if (session.getClient() != null) {
		// session.getClient().queueMessage((Packet) e.getMessage());
		// }
		// }

		if (obj instanceof InPacket) {
			if (session.getClient() != null && !session.getClient().disconnected) {
				session.getClient().queueMessage((InPacket) obj);
			}
		} else if (obj instanceof Client) {
			session.setClient((Client) obj);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		/*
		 * if (e.getCause() instanceof ReadTimeoutException) { if
		 * (session.getClient() != null) { System.out.println("Player " +
		 * session.getClient().getName() + " timed out!"); } } else
		 * if(!(e.getCause() instanceof java.io.IOException)){
		 * e.getCause().printStackTrace(); } ctx.getChannel().close();
		 */

		if (cause.getCause() instanceof ReadTimeoutException) {
			if (session.getClient() != null) {
				System.out.println("Player " + session.getClient().getName()
						+ " timed out!");
			}
		} else if (!(cause.getCause() instanceof java.io.IOException)) {
			cause.getCause().printStackTrace();
		}
		ctx.channel().close();
	}
	
}
