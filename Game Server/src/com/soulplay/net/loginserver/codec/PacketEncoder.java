package com.soulplay.net.loginserver.codec;

import com.soulplay.net.loginserver.LoginServerPacket;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.ReferenceCountUtil;

public final class PacketEncoder extends MessageToByteEncoder<LoginServerPacket> {
	
	@Override
	protected void encode(ChannelHandlerContext ctx, LoginServerPacket msg, ByteBuf out) {
		final ByteBuf outBuffer = msg.getOutBuffer();
		try {
			out.writeShort(msg.getLength());
			out.writeBytes(outBuffer);
		} finally {
			ReferenceCountUtil.release(outBuffer);
		}
	}
	
}
