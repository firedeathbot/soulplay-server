package com.soulplay.net.loginserver.codec;

import java.util.List;

import com.soulplay.net.loginserver.LoginServerPacket;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public final class PacketDecoder extends ByteToMessageDecoder {
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
		if (in.readableBytes() > 2) {
			in.markReaderIndex();
			final int packetLength = in.readShort();
			if (in.readableBytes() >= packetLength) {
				final byte[] data = new byte[packetLength];
				in.readBytes(data, 0, data.length);
				out.add(new LoginServerPacket(data));
			} else {
				in.resetReaderIndex();
			}
		}
	}
	
}
