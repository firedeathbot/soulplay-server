package com.soulplay.net.loginserver;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.clans.ClanManager;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.privatemessage.Friends;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.net.login.LoginServerRequest;
import com.soulplay.net.loginserver.codec.PacketDecoder;
import com.soulplay.net.loginserver.codec.PacketEncoder;
import com.soulplay.net.loginserver.codec.PacketHandler;
import com.soulplay.net.packet.in.CommandPacket;
import com.soulplay.util.Misc;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

/**
 * Login server connection handler.
 *
 * @author Ultimate1
 */

public class LoginServerConnection {
	
	public static final int NONE = 11;
	
	/**
	 * The logger.
	 */
	private static Logger logger = Logger
			.getLogger(LoginServerConnection.class.getName());
	
	private final static LoginServerConnection instance = new LoginServerConnection();
	
	public static final LoginServerConnection instance() {
		return instance;
	}
	
	/**
	 * The channel.
	 */
	private Channel channel;
	
	private final ConcurrentMap<String, LoginServerRequest> nameToLoginRequests = new ConcurrentHashMap<>();
	
	public ConcurrentMap<String, LoginServerRequest> getNameToLoginRequests() {
		return nameToLoginRequests;
	}
	
//	private final ExecutorService workService = Executors.newSingleThreadExecutor();
	
	/**
	 * The work service.
	 */
	
	public static AtomicBoolean lsInUse = new AtomicBoolean(false);
	
	/**
	 * Adds a friend to the list.
	 *
	 * @param friendIndex
	 * @param friend
	 * @param name
	 * @return
	 */
	public LoginServerConnection addFriend(int friendIndex, long friend,
	                                       String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.MODIFY_FRIENDS)
							.writeString(name.toLowerCase())
							.writeByte(friendIndex).writeLong(friend),
					channel.voidPromise());
		}
		return this;
	}
	
	public LoginServerConnection checkGlitchedLogin(String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(new LoginServerPacket(
							GameServerPacketOpcode.CHECK_DUPED_ACCOUNT)
							.writeString(name.toLowerCase())
							.writeByte(Config.NODE_ID),
					channel.voidPromise());
		}
		return this;
	}
	
	public static EventLoopGroup group;
	public static Class<? extends SocketChannel> channelClass;
	
	public ChannelFuture connect(String host, int port) {
		final Bootstrap strap = new Bootstrap();
		strap.group(group).channel(channelClass).handler(new ChannelInitializer<SocketChannel>() {
			@Override
			protected void initChannel(SocketChannel ch) {
				final ChannelPipeline pipeline = ch.pipeline();
				pipeline.addLast("encoder", new PacketEncoder());
				pipeline.addLast("decoder", new PacketDecoder());
				pipeline.addLast("handler", new PacketHandler());
			}
		}).option(ChannelOption.TCP_NODELAY, true);
		
		return strap.connect(host, port);
	}
	
	/**
	 * Finalise the login procedure.
	 *
	 * @param player
	 * @return
	 */
	public LoginServerConnection finaliseLogin(Client player) {
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.FINALIZE_LOGIN)
							.writeString(player.getName().toLowerCase())
							.writeByte(player.playerRights)
							.writeByte(ChatCrown.values[player.getSetCrown()].getClientIcon())
							.writeString(player.connectedFrom)
							.writeString(player.UUID)
							.writeInt(player.mySQLIndex)
							.writeLong(player.getLongNameSmart()),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Handles a login server packet.
	 *
	 * @param opcode
	 * @param packet
	 * @param channel
	 */
	public void handlePacket(final LoginServerPacket packet, Channel channel) {
		int opcode = packet.readByte();
		// if (opcode != 15)
		// System.out.println("received packet with opcode "+opcode);
		switch (opcode) {
			case 1: {
				final int code = packet.readByte();
				if (code == 2) {
					this.channel = channel;
					
					for (Player p : PlayerHandler.players) {
						if (p != null && !p.disconnected) {
							final Client cl = (Client) p;
							LoginServerConnection.instance().finaliseLogin(cl);
						}
					}
					
				} else {
					logger.warning(
							"Error registering node on login server (code "
									+ code + ").");
					channel.close();
				}
			}
			break;
			case 2: {
				final String name = packet.readString();
				final int returnCode = packet.readByte();
				
				PlayerSaveSql.SAVE_EXECUTOR.submit(new Runnable() {
					@Override
					public void run() {
						FinalizeLoginRequest.finalizeLoginRequest(name, returnCode);
					}
				});
			}
			break;
			case 4: { // receive pm
				
				final long sender = packet.readLong();
				final long nameOfRecipient = packet.readLong();
				final int senderRights = packet.readByte();
				final int messageSize = packet.readInt();
				final byte[] message = new byte[messageSize];
				for (int i = 0; i < message.length; i++) {
					message[i] = (byte) packet.readByte();
				}
				
				// System.out.println("rights test : "+senderRights);
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						this.stop();
						
						final String recName = Misc.longToPlayerName2(nameOfRecipient);
						if (recName == null) {
							return;
						}
						
						final Player recipient = PlayerHandler.getPlayerSmart(recName);
						
						if (recipient == null || !recipient.isActive) {
							return;
						}
						
						if (recipient != null && !recipient.disconnected) {
							recipient.getPacketSender().sendPM(sender,
									senderRights, message, messageSize);// recipient.getActionSender().sendPM(sender,
							// senderRights,
							// message);
							
							if (recipient.isIdle) {
								LoginServerConnection.instance()
										.submit(() -> LoginServerConnection
												.instance().sendMessageToPlayer(
														"<col=800000>" + recipient.getNameSmartUp()
																+ " is currently AFK.",
														Misc.longToPlayerName2(
																sender)));
							}
						}
						
					}
				});
				
			}
			break;
			case 5: {
				
				final String name = packet.readString();
				final long friend = packet.readLong();
				final int world = packet.readByte();
				// System.out.println("longFriend from LoginServer: "+friend);
				// System.out.println("longFriend from World: "+world);
				
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						
						this.stop();

						final Player player = PlayerHandler.getPlayer(name);
						
						if (player == null || !player.isActive) {
							return;
						}
						
						if (player != null && !player.disconnected) {
							player.getPacketSender().loadPM(friend, world); // player.getActionSender().sendFriend(friend,
							// world);
						}
						
					}
				});
				
			}
			break;
			case 6: { // TODO: use all these params for sending clan messages
				final int nodeId = packet.readByte();
				packet.readByte();
				final String sender = packet.readString();
				final String message = packet.readString();
				// System.out.println("rights "+rights);
				if (message.startsWith(":clan:")) {
					final String[] args = message.split(":", 4);
					// System.out.println("args1:"+args[4]);
					
					Server.getTaskScheduler().schedule(new Task() {
						
						@Override
						protected void execute() {
							this.stop();
							
							final Clan clan = ClanManager
									.getClanByOwnerName(args[2]);
							
							if (clan == null) {
								return;
							}
							
							final String worldSig = "<col=800000>[W" + nodeId + "]";
							clan.sendClanMessage(worldSig + args[3], sender); // fix
							// this
							// args[]
							// variable
							// to
							// grab
							// clan
							// name
							
							
						}
					});
					
					break;
				} else if (message.startsWith(":clan2:")) {
					final String[] args = message.split(":", 3);
					
					// System.out.println("msg:"+message);
					// System.out.println("name:"+sender);
					
					Server.getTaskScheduler().schedule(new Task() {
						
						@Override
						protected void execute() {
							this.stop();
							
							final Clan clan = ClanManager
									.getClanByOwnerName(sender);
							
							if (clan == null) {
								return;
							}
							clan.sendClanMessage(args[2], sender); // fix this args[]
							// variable to grab
							// clan name
							
							
						}
					});
					
					break;
				}
				
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						this.stop();
						
						//System.out.println("node:"+nodeId+" message:"+message +", sender:"+sender);
						PlayerHandler.messageAllPlayersFromYell("[W" + nodeId + "]" + message, sender);
					}
				});
			}
			break;
			case 7: {
				
				try {
					
					int[] count = new int[Config.MAX_WORLDS]; // { 0, 0, 0, 0,
					// 0, 0, 0};
					final int activeNodes = packet.readByte();
					for (int i = 0; i < activeNodes; i++) {
						final int nodeId = packet.readByte();
						count[nodeId - 1] = packet.readShort();
					}
					
					final int[] countFinal = count;
					
					// System.out.println("count1 "+countFinal[0]);
					
					Server.getTaskScheduler().schedule(new Task(true) {
						
						@Override
						protected void execute() {
							this.stop();
							
							for (int i = 0; i < Config.MAX_WORLDS; i++) {
								PlayerHandler.setNodePlayerCount(i,
										countFinal[i]);
							}
						}
					});
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
			}
			break;
			case 8: {
				
				final String name = packet.readString();
				final int chatMode = packet.readByte();
				final int friendCount = packet.readByte();
				final long[] friends = new long[friendCount];
				final byte[] worlds = new byte[friendCount];
				for (int i = 0; i < friendCount; i++) {
					friends[i] = packet.readLong();
					worlds[i] = (byte) packet.readByte();
				}
				final int ignoresCount = packet.readByte();
				final long[] ignores = new long[ignoresCount];
				for (int i = 0; i < ignoresCount; i++) {
					ignores[i] = packet.readLong();
				}
				
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						
						this.stop();

						final Player player = PlayerHandler.getPlayer(name);
						
						if (player == null || !player.isActive) {
							return;
						}
						
						if (player != null && !player.disconnected) {
							Friends.updateFriendsList(friends, worlds, chatMode,
									player);
							Friends.loadIgnoreList(player, ignores);
						}
						
					}
				});
				
				break;
				
			}
			
			case 9: { // punish player //TODO: send back the uuid and ip if
				// possible to ban from there instead of loading player
				// from playermap
				
				final String name = packet.readString();
				final int punishmentType = packet.readByte();
				// final long time = packet.readLong();
				
				
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						
						this.stop();
						
						final Player player = PlayerHandler.getPlayer(name);
						
						if (player == null || !player.isActive) {
							return;
						}
						
						for (Map.Entry<Long, Player> entry : PlayerHandler.getPlayerMap().entrySet()) {
							Player p = entry.getValue();
							if (p == null || !p.isActive) {
								continue;
							}
							if (punishmentType == CommandPacket.Punishment.MUTE_IP.getPunishmentType() 
									|| punishmentType == CommandPacket.Punishment.BAN_IP.getPunishmentType()) { // scan
								// ips
								if (p.connectedFrom.equals(player.connectedFrom)) {
									p.setMuted(true);
									p.sendMessage("You have been muted.");
								}
							}
							if (punishmentType == CommandPacket.Punishment.BAN_UUID.getPunishmentType() 
									|| punishmentType == CommandPacket.Punishment.MUTE_UUID.getPunishmentType()) { // scan
								// uuids
								if (player.UUID.equals(p.UUID)) {
									//p.getPA().movePlayer(2095, 4428, 4);
									p.kickPlayer();
								}
							}
						}
						
						
						if (player != null && !player.disconnected) {
							if (punishmentType == CommandPacket.Punishment.MUTE_IP.getPunishmentType() 
									|| punishmentType == CommandPacket.Punishment.MUTE_UUID.getPunishmentType()
									|| punishmentType == CommandPacket.Punishment.MUTE_NAME.getPunishmentType()) { // mutes
								player.setMuted(true);
								player.sendMessage("You have been muted.");
							} else { // else bans
//								player.getPA().movePlayer(2095, 4428, 4);
								player.kickPlayer();
							}
						}
						
					}
				});
				
				break;
			}
			
			case 10: { // kick player
				
				final String name = packet.readString();
				
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						
						this.stop();
						
						final Player player = PlayerHandler.getPlayer(name);
						
						if (player == null || !player.isActive) {
							return;
						}
						
						if (player != null && !player.disconnected) {
							player.kickPlayer();
						}
						
					}
				});
				
				break;
			}
			
			case 11: { // sendMessage to player
				
				final String name = packet.readString();
				final String message = packet.readString();
				
				// System.out.println("name:"+name+" msg:"+message);
				
				Server.getTaskScheduler().schedule(new Task(true) {
					
					@Override
					protected void execute() {
						
						this.stop();
						
						final Player player = PlayerHandler.getPlayer(name);
						
						if (player == null || !player.isActive) {
							return;
						}
						
						if (player != null && !player.disconnected) {
							player.sendMessage(message);
						}
						
					}
				});
				
				break;
			}
			
			case 12: { // receive item price
				int itemId = packet.readInt();
				int itemPrice = packet.readInt();
				
//				PriceChecker.getCachedPrices()[itemId] = itemPrice;
			}
			break;
			
			case 13:
				packet.readInt();
				for (Player p : PlayerHandler.players) {
					if (p == null) {
						continue;
					}
					PlayerSaveSql.saveGame(p, false);
				}
				break;
			
			case 15:
				// PlayerOnlineCalculation.playerCount = packet.readShort();
				break;
			
			case 16: // check what world player is logged into
				
				final String name = packet.readString();
				final int worldId = packet.readByte();
				
				if (!Config.RUN_ON_DEDI)
					return;
				
				final Player player = PlayerHandler.getPlayer(name);
				
				if (player == null || !player.isActiveAtomic()) {
					System.out.println(
							"Attempted to kick bugged login on PlayerName: "
									+ name);
					return;
				}
				
				if (worldId != Config.NODE_ID) {
					System.out.println("Bugged login. PlayerName:" + name);
					
					Server.getTaskScheduler().schedule(new Task(true) {
						
						@Override
						protected void execute() {
							
							this.stop();
							
							if (player != null && !player.disconnected) {
								// player.getPA().movePlayer(DBConfig.RESPAWN_X,
								// DBConfig.RESPAWN_Y, 0);
								player.kickPlayer();
							}
							
						}
					});
				} else {
					// TODO: passed verification. Let him on.
				}
				
				break;
			
		}
		
	}
	
	/**
	 * Checks if the login server is connected.
	 *
	 * @return true if the login server is connected.
	 */
	public boolean isActive() {
		return channel != null && channel.isActive();
	}
	
	/**
	 * Punish player with ban or mute.
	 *
	 * @param newPassword
	 * @param name
	 * @return
	 */
	public LoginServerConnection punishPlayer(String modName, String playerName,
	                                          int punishmentType, long time, String reason) {
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			return this;
		}
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.PUNISH_PLAYER)
							.writeString(modName.toLowerCase())
							.writeString(playerName.toLowerCase())
							.writeByte(punishmentType).writeLong(time)
							.writeString(reason),
					channel.voidPromise());
		}
		return this;
	}
	
	public Channel getChannel() {
		return channel;
	}
	
	/**
	 * Removes a friend from the list.
	 *
	 * @param friendIndex
	 * @param name
	 * @return
	 */
	public LoginServerConnection removeFriend(int friendIndex, String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.MODIFY_FRIENDS)
							.writeString(name.toLowerCase())
							.writeByte(friendIndex).writeLong(0),
					channel.voidPromise());
		}
		return this;
	}
	
	public LoginServerConnection modifyIgnores(String name, long ignoreLongName, boolean addingIgnore) {
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.MODIFY_IGNORES)
							.writeString(name.toLowerCase())
							.writeByte(addingIgnore ? 1 : 0).writeLong(ignoreLongName),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Requests the status of a friend.
	 *
	 * @param friend
	 * @param sessionId
	 */
	public void requestFriendStatus(long friend, String name) {
		if (!isActive()) {
			return;
		}
		channel.writeAndFlush(
				new LoginServerPacket(GameServerPacketOpcode.FRIEND_STATUS)
						.writeString(name.toLowerCase()).writeLong(friend),
				channel.voidPromise());
	}
	
	public LoginServerConnection saveAccountsOnWorld(int nodeId) {
		if (channel.isActive()) {
			channel.writeAndFlush(new LoginServerPacket(
							GameServerPacketOpcode.SAVE_CHARACTER_FILES)
							.writeInt(nodeId),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Sends a command to the login server
	 *
	 * @return
	 */
	public LoginServerConnection sendCommand(String command) {
		if (channel.isActive()) {
			channel.writeAndFlush(new LoginServerPacket(
							GameServerPacketOpcode.COMMAND_FROM_SERVER)
							.writeString(command),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Sends a global yell packet.
	 *
	 * @param message
	 * @param displayName
	 * @return
	 */
	public LoginServerConnection sendGlobalYell1(String message, String displayName) {
		if (channel.isActive()) {//this will never be backed up, because it is non-blocking. yep
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.GLOBAL_MESSAGE)
							.writeString(displayName)
							.writeString(message),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Sends a send message packet request
	 *
	 * @param message
	 * @param name
	 * @return
	 */
	public LoginServerConnection sendMessageToPlayer(String message,
	                                                 String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.SEND_MESSAGE)
							.writeString(name.toLowerCase())
							.writeString(message),
					channel.voidPromise());
		}
		return this;
	}
	
	public LoginServerConnection sendPrice(int itemId, int averagePrice) {
		if (channel.isActive()) {
			channel.writeAndFlush(
					new LoginServerPacket(GameServerPacketOpcode.PRICE_CHECK)
							.writeInt(itemId).writeInt(averagePrice),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Forwards a private message to the login server.
	 *
	 * @param nameOfRecipient
	 * @param message
	 * @param name
	 * @return
	 */
	public LoginServerConnection sendPrivateMessage(long nameOfRecipient,
	                                                byte[] message, String name, int senderCrown) {
		if (channel.isActive()) {
			final LoginServerPacket packet = new LoginServerPacket(
					GameServerPacketOpcode.PRIVATE_MESSAGE)
					.writeString(name)
					.writeByte(senderCrown)
					.writeLong(nameOfRecipient)
					.writeInt(message.length);
			for (final byte element : message) {
				packet.writeByte(element);
			}
			channel.writeAndFlush(packet, channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Submits work to the work service.
	 *
	 * @param runnable
	 */
	public void submit(final Runnable runnable) {
//		workService.submit(() -> {
		try {
			runnable.run();
		} catch (final Throwable t) {
			t.printStackTrace();
		}
//		});
	}
	
	/**
	 * Unregister the player.
	 *
	 * @param name
	 * @return
	 */
	public LoginServerConnection unregister(String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(new LoginServerPacket(
							GameServerPacketOpcode.UNREGISTER_PLAYER)
							.writeString(name.toLowerCase()),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Updates a player password.
	 *
	 * @param newPassword
	 * @param name
	 * @return
	 */
	public LoginServerConnection updatePassword(String newPassword,
	                                            String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(new LoginServerPacket(
							GameServerPacketOpcode.UPDATE_PASSWORD)
							.writeString(name.toLowerCase())
							.writeString(newPassword).writeLong(0),
					channel.voidPromise());
		}
		return this;
	}
	
	/**
	 * Updates the player's private chat mode.
	 *
	 * @param privateChatMode
	 * @param name
	 * @return
	 */
	public LoginServerConnection updatePrivateChatMode(int privateChatMode,
	                                                   String name) {
		if (channel.isActive()) {
			channel.writeAndFlush(new LoginServerPacket(
							GameServerPacketOpcode.UPDATE_CHAT_MODE)
							.writeString(name.toLowerCase())
							.writeByte(privateChatMode),
					channel.voidPromise());
		}
		return this;
	}
	
}
