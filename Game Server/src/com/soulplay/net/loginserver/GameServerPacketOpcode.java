package com.soulplay.net.loginserver;

public class GameServerPacketOpcode {

	public static final int REQUEST_CONNECTION_TO_LOGINSERVER = 1;

	public static final int UNUSED2 = 2;

	public static final int UNREGISTER_PLAYER = 3;

	public static final int PUNISH_PLAYER = 4;

	public static final int FRIEND_STATUS = 5;

	public static final int PRIVATE_MESSAGE = 6;

	public static final int LOGIN_VERIFICATION = 7;

	public static final int MODIFY_FRIENDS = 8;

	public static final int UPDATE_PASSWORD = 9;

	public static final int UPDATE_CHAT_MODE = 10;

	public static final int GLOBAL_MESSAGE = 11;

	public static final int SEND_MESSAGE = 12;

	public static final int COMMAND_FROM_SERVER = 13;

	public static final int FINALIZE_LOGIN = 14;

	public static final int PLAYERS_ONLINE_REQUEST = 15;

	public static final int CHECK_DUPED_ACCOUNT = 16;

	public static final int PRICE_CHECK = 17;

	public static final int SAVE_CHARACTER_FILES = 18;
	
	public static final int MODIFY_IGNORES = 19;

}
