package com.soulplay.net.loginserver;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

public class LoginServerPacket {

	private byte[] inBuffer;

	private ByteBuf outBuffer;

	private int caret;

	public LoginServerPacket(byte[] inBuffer) {
		this.inBuffer = inBuffer;
	}

	public LoginServerPacket(int opcode, ByteBufAllocator alloc) {
		outBuffer = alloc.buffer();
		writeByte(opcode);
	}
	
	public LoginServerPacket(int opcode) {
		this(opcode, ByteBufAllocator.DEFAULT);
	}

	public int getLength() {
		return outBuffer.writerIndex();
	}

	public ByteBuf getOutBuffer() {
		return outBuffer;
	}

	public int readByte() {
		return inBuffer[caret++] & 0xff;
	}

	public int readInt() {
		return readShort() << 16 | readShort();
	}

	public long readLong() {
		long value = 0;
		value |= (long) readByte() << 56L;
		value |= (long) readByte() << 48L;
		value |= (long) readByte() << 40L;
		value |= (long) readByte() << 32L;
		value |= (long) readByte() << 24L;
		value |= (long) readByte() << 16L;
		value |= (long) readByte() << 8L;
		value |= readByte();
		return value;
	}

	// public long readLong() {
	// return readInt() << 32 | readInt();
	// }

	public int readShort() {
		return readByte() << 8 | readByte();
	}

	public String readString() {
		int c;
		final StringBuilder builder = new StringBuilder();
		while ((c = readByte()) != 10) {
			builder.append((char) c);
		}
		return builder.toString();
	}

	public LoginServerPacket writeByte(int i) {
		outBuffer.writeByte(i);
		return this;
	}

	public LoginServerPacket writeInt(int i) {
		outBuffer.writeInt(i);
		return this;
	}

	public LoginServerPacket writeLong(long l) {
		outBuffer.writeLong(l);
		return this;
	}

	public LoginServerPacket writeShort(int i) {
		outBuffer.writeShort(i);
		return this;
	}

	public LoginServerPacket writeString(String s) {
		for (final byte b : s.getBytes()) {
			writeByte(b);
		}
		writeByte(10);
		return this;
	}

}
