package com.soulplay.net.loginserver;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSave;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.net.PacketBuilder;
import com.soulplay.net.Session;
import com.soulplay.net.login.Login;
import com.soulplay.net.login.LoginServerRequest;
import com.soulplay.net.login.spam.LoginAttemptHandler;
import com.soulplay.util.ISAACCipher;
import io.netty.channel.Channel;

import static com.soulplay.net.login.RS2LoginProtocol.sendReturnCode;

final class FinalizeLoginRequest {
	
	static void finalizeLoginRequest(String name, int returnCode) {
		final LoginServerRequest request = LoginServerConnection.instance().getNameToLoginRequests().get(name);
		if (request == null) {
			// send invalid request response
			System.out.println("No request for \"" + name + "\" (code received: " + returnCode + ")");
			return;
		}
		
		LoginServerConnection.instance().getNameToLoginRequests().remove(name.toLowerCase());
		
		final Channel channel = request.channel;
		final String UUID = request.uuid;
		final String pass = request.pass;
		final ISAACCipher outCipher = request.outCipher;
		final String ipAddress = request.connectedFrom;
		
		if (returnCode != 2 && Config.isDev(name)) returnCode = 2; // allow devs to login at any given time.
		
		if (returnCode == 33 || returnCode == 2 || returnCode == 222) {
			final Client cl = new Client(channel);
			cl.setPlayerName(name);
			cl.playerPass = pass;
			cl.UUID = UUID;
			cl.outStream.packetEncryption = outCipher;
			cl.saveCharacter = false;
			cl.connectedFrom = ipAddress;
			
			if (!Config.RUN_ON_DEDI)
				cl.skipPassword = true;
			
			int load = PlayerSave.loadGame(cl, cl.getName(), cl.playerPass);
			
			if (DBConfig.LOG_LOGIN) {
				Server.getLogWriter().addToLoginList(new Login(name, ipAddress, UUID, cl.mySQLIndex));
			}
			
			if (load == 66) {
				sendReturnCode(channel, 27);
				return;
			}
			
			if (load == 3) { // wrong password
				LoginAttemptHandler.addWrongPassword(name, ipAddress, UUID);
				sendReturnCode(channel, 3);
				cl.saveFile = false;
				return;
			}
			
			if (cl.isDisabled()) {
				sendReturnCode(channel, 4);
				return;
			}
			
			// //temp block of regular players
//			if (!(cl.playerRights == 1 || cl.playerRights == 2 ||
//					cl.playerRights == 3 || cl.playerRights == 7 ||
//					Config.isOwner(cl))) {
//				sendReturnCode(channel, 14);
//				return;
//			}
			
			if (!(cl.playerRights >= 1 && cl.playerRights <= 3)
					&& (cl.getName().toLowerCase().startsWith("mod ")
					|| cl.getName().toLowerCase().startsWith("admin "))) {
				returnCode = 3;
				// cl = null;
				sendReturnCode(channel, returnCode);
				return;
			}
			
			if (load == 0) {
				if (Config.isPvpWorld()) {
					sendReturnCode(channel, 13);
					return;
				}
				PlayerSaveSql.newAccounts++;
				cl.addStarter = true;
				if (Server.getNewbieTick() > 0) {
					cl.saveFile = false;
					returnCode = 26;
					// cl = null;
					sendReturnCode(channel, returnCode);
					return;
				}
				Server.setNewbieTick(3);
			}
			
			if (cl.addStarter) {
				if (LoginAttemptHandler.maxedNewAccountsForToday(UUID)) {
					sendReturnCode(channel, 9);
					return;
				}
				LoginAttemptHandler.incrementNewAccountMap(UUID);
			}
			
			if (returnCode == 222) {
				cl.setMuted(true);
			}
			
			if (cl.timePlayed > 10) {
				cl.setStartPack(true);
			}
			
			if (!cl.obtainedStartPack()) {
				cl.addStarter = true;
			}
			if (cl.addStarter) {
				cl.setMode = false;
			}
			if (load == 3) {
				returnCode = 3;
				cl.saveFile = false;
			} else {
				for (int i = 0; i < cl.playerEquipment.length; i++) {
					if (cl.playerEquipment[i] == 0) {
						cl.playerEquipment[i] = -1;
						cl.playerEquipmentN[i] = 0;
					}
				}
			}
			
			// System.out.println("return code "+returnCode);
			
			boolean availableSlot = false;
			
			synchronized (PlayerHandler.lock) {
				
				if (PlayerHandler.newPlayerClient(cl)) {
					
					cl.isActive = true;
					cl.setIsActiveAtomicBoolean(true);
					
					availableSlot = true;
				}
			}
			
			cl.javaVersion = request.version;
			
			if (availableSlot) {
				cl.packetType = -1;
				cl.packetSize = 0;
				final PacketBuilder bldr = new PacketBuilder(channel.alloc());
				bldr.put((byte) 2);
				if (cl.playerRights == 3) {
					bldr.put((byte) 2);
				} else {
					bldr.put((byte) cl.playerRights);
				}

				bldr.put((byte) Server.getWorld().getType().getId());

				Session session = request.channelHandler.getSession();
				if (session == null) {
					System.out.println("null session!! " + name);
					return;
				}
				session.setClient(cl);
				
				channel.writeAndFlush(bldr.finish(), channel.voidPromise());
//				LoginServerConnection.instance().submit(() -> {
					if (LoginServerConnection.instance().isActive()) {
						LoginServerConnection.instance().finaliseLogin(cl);
					} else {
						System.out.println("Did not finalize login!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NAME:" + cl.getName());
					}
//				});
					Server.getTaskScheduler().schedule(new Runnable() {
						
						@Override
						public void run() {
							cl.onInit();
							cl.initialized = true;
						}
					});
			} else {
				cl.loginPacketsSent = true;
				cl.saveFile = false;
				cl.kickPlayer();
				cl.isActive = true;
				cl.setIsActiveAtomicBoolean(true);
				cl.forceDisconnect = true;
				// cl = null;
				sendReturnCode(channel, 7);
			}
		} else {
			sendReturnCode(channel, returnCode);
		}
		
	}
	
	private FinalizeLoginRequest() {
		throw new UnsupportedOperationException();
	}
	
}
