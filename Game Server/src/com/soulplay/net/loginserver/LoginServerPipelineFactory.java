package com.soulplay.net.loginserver;

import com.soulplay.net.loginserver.codec.PacketDecoder;
import com.soulplay.net.loginserver.codec.PacketEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.NioSocketChannel;

public final class LoginServerPipelineFactory extends ChannelInitializer<NioSocketChannel> {
	
	@Override
	protected void initChannel(NioSocketChannel ch) {
		final ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast("encoder", new PacketEncoder());
		pipeline.addLast("decoder", new PacketDecoder());
		pipeline.addLast("handler", null);
	}
	
}
