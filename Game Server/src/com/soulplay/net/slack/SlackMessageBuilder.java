package com.soulplay.net.slack;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.soulplay.content.minigames.PestControl;
import com.soulplay.content.minigames.castlewars.CastleWars;
import com.soulplay.content.minigames.soulwars.SoulWars;
import com.soulplay.Server;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.ullink.slack.simpleslackapi.SlackPreparedMessage;

public class SlackMessageBuilder {

	final static SlackField serverField = new SlackField().setTitle("Server:")
			.setValue(Server.getWorld().getDesc())
			.setShorten(true);

	public static SlackMessage buildAdvertiserMessage(String username,
			String text, String chat) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField usernameField = new SlackField();
		SlackField messageField = new SlackField();
		SlackField chatField = new SlackField();

		sa.setColor("#FF0000");
		sa.setPretext("Possible Advertiser:");

		// Creating Fields for the message
		usernameField.setTitle("Username:").setValue(username).setShorten(true);
		messageField.setTitle("Message:").setValue(text).setShorten(true);
		chatField.setTitle("Chat:").setValue(chat).setShorten(true);

		// Needed for fallback (IRC / Notification message)
		sa.setFallback("Possible Advertiser: " + username + ": " + text
				+ " Chat: " + chat);

		sa.addFields(usernameField).addFields(messageField).addFields(chatField)
				.addFields(serverField);
		sm.addAttachments(sa);
		sm.setText(""); // Message can't be null.
		return sm;
	}

	public static SlackMessage buildAutoMuteMessage(String playerName,
			boolean macmute) {

		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField durationField = new SlackField();

		sa.setColor("#14CCA2");
		sa.setPretext("");

		nameField.setTitle("Auto Mute:").setValue(playerName + "");
		durationField.setTitle("Mute type:")
				.setValue(macmute ? "Mac Mute" : "Mute (10 minutes)");
		if (macmute) {
			Server.getSlackApi().call(SlackMessageBuilder.buildEvidenceMessage(
					playerName, "Link spamming", "MAC Mute"));
		}
		sa.setFallback("Auto Mute: " + playerName);

		sa.addFields(nameField).addFields(durationField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Chat Watchdog");
		sm.setIcon(":eyes:");
		return sm;
	}

	public static SlackMessage buildBotMessage(String name, String type) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField typeField = new SlackField();

		sa.setColor("#AF00C3");
		sa.setPretext("");

		nameField.setTitle("Player:").setValue(name);
		typeField.setTitle("Type:").setValue(type);
		sa.setFallback("Possible botter: " + name);

		sa.addFields(nameField).addFields(typeField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Bot Buster");
		sm.setIcon(":rustle:");
		return sm;
	}
	
	public static SlackMessage buildReportMadeMessage(String name, String reportedBy) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField reportedByField = new SlackField();

		sa.setColor("#AF00C3");
		sa.setPretext("");

		nameField.setTitle("Player:").setValue(name);
		reportedByField.setTitle("Reported By:").setValue(reportedBy);
		sa.setFallback("Report has been made by : " + name);

		sa.addFields(nameField).addFields(reportedByField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Reported Player");
		sm.setIcon(":rustle:");
		return sm;
	}

	public static SlackMessage buildCheatClientMessage(String name) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField playerField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		playerField.setTitle("Cheat Client Player:").setValue(name);

		sa.setFallback("Cheat Client Username: " + name);

		sa.addFields(playerField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Server Watchdog");
		sm.setIcon(":exclamation:");
		return sm;
	}
	
	public static SlackMessage buildGrabUUIDMessage(String name, String uuid) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField playerField = new SlackField();
		SlackField uuidField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		playerField.setTitle("UUID Grabbed of player:").setValue(name);
		uuidField.setTitle("UUID:").setValue(uuid);

		sa.setFallback("UUID Grabbed of Username: " + name);

		sa.addFields(playerField).addFields(uuidField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Server Watchdog");
		sm.setIcon(":exclamation:");
		return sm;
	}

	public static SlackMessage buildDonationClaimErrorMessage(String servername,
			String nameAndDonationType, String channel) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField serverField = new SlackField();
		SlackField debugField = new SlackField();

		sa.setColor("#ff0000");
		sa.setPretext("Donation Error");

		serverField.setTitle("Server:").setValue(servername);
		debugField.setTitle("Event: Donation Error: " + nameAndDonationType);

		sa.setFallback("Donation Error");

		sa.addFields(serverField).addFields(debugField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel(channel);
		sm.setUsername("Server Watchdog");
		sm.setIcon(":x:");
		return sm;
	}

	public static SlackMessage buildDonationScrollMessage(String name,
			String item, String type) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField itemField = new SlackField();
		SlackField typeField = new SlackField();

		sa.setColor("#14CCA2");
		sa.setPretext("");

		nameField.setTitle("Player:").setValue(name);
		itemField.setTitle("Item:").setValue(item);
		typeField.setTitle("Usage:").setValue(type);
		sa.setFallback("Donation Duper: " + name);

		sa.addFields(nameField).addFields(itemField).addFields(typeField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#admins");
		sm.setUsername("Detective Ray");
		sm.setIcon(":mag:");
		return sm;
	}

	public static SlackMessage buildDungMessage(String name,
			List<Integer> items) {
		StringBuilder itemString = new StringBuilder();
		for (int item : items) {
			itemString.append(item);
			itemString.append(", ");
		}
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField itemField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		nameField.setTitle("Possible Smuggler:").setValue(name + "")
				.setShorten(true);
		itemField.setTitle("Items:").setValue(itemString.toString())
				.setShorten(true);
		sa.setFallback("Possible Smuggler: " + name);

		sa.addFields(nameField).addFields(itemField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Dungeoneering Batch");
		sm.setIcon(":unicorn_face:");
		return sm;
	}

	public static SlackMessage buildDuperMessage(String name,
			Map<Integer, Integer> items) {
		StringBuilder itemString = new StringBuilder();
		for (Entry<Integer, Integer> entry : items.entrySet()) {
			itemString.append(ItemProjectInsanity.getItemName(entry.getKey()));
			itemString.append(" (");
			itemString.append(entry.getValue() + "x)");
			itemString.append(", ");
		}
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField itemField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		nameField.setTitle("Possible Duper:").setValue(name).setShorten(true);
		itemField.setTitle("Items:").setValue(itemString.toString())
				.setShorten(true);
		sa.setFallback("Possible Duper: " + name);

		sa.addFields(nameField).addFields(itemField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Duper Batch");
		sm.setIcon(":aliendance:");
		return sm;
	}
	
	public static SlackMessage buildMoneyPouchDuperMessage(String name,
			long amount) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField itemField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		nameField.setTitle("Possible moneypouch Duper:").setValue(name).setShorten(true);
		itemField.setTitle("Pouch change:").setValue(Long.toString(amount))
				.setShorten(true);
		sa.setFallback("Possible Moneypouch Duper: " + name);

		sa.addFields(nameField).addFields(itemField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Duper Batch");
		sm.setIcon(":aliendance:");
		return sm;
	}
	
	public static SlackMessage buildDuperMessage(String name,
			Item... items) {
		StringBuilder itemString = new StringBuilder();
		for (Item item : items) {
			itemString.append(ItemProjectInsanity.getItemName(item.getId()));
			itemString.append(" (");
			itemString.append(item.getAmount() + "x)");
			itemString.append(", ");
		}
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField itemField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		nameField.setTitle("Possible Duper:").setValue(name).setShorten(true);
		itemField.setTitle("Items:").setValue(itemString.toString())
				.setShorten(true);
		sa.setFallback("Possible Duper: " + name);

		sa.addFields(nameField).addFields(itemField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Duper Batch");
		sm.setIcon(":aliendance:");
		return sm;
	}

	private static SlackMessage buildEvidenceMessage(String playerName,
			String offense, String punishment) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField reasonField = new SlackField();
		SlackField punishmentField = new SlackField();

		sa.setColor("#14CCA2");
		sa.setPretext("");

		nameField.setTitle("Player:").setValue(playerName).setShorten(true);
		reasonField.setTitle("Offense:").setValue(offense).setShorten(true);
		punishmentField.setTitle("Punishment:").setValue(punishment)
				.setShorten(true);
		sa.setFallback("Evidence for: " + playerName);

		sa.addFields(nameField).addFields(reasonField)
				.addFields(punishmentField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#evidence");
		sm.setUsername("Detective Ray");
		sm.setIcon(":mag:");
		return sm;
	}

	public static SlackPreparedMessage buildEvidenceMessage(String player,
			String type, String punishment, String reason, String mod) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();

		sa.setColor("#14CCA2");
		sa.setPretext("");

		sa.addField("Player:", player, true);
		sa.addField("Offense:", type, true);
		sa.addField("Punishment:", punishment, true);
		sa.addField("Reason:", reason, true);
		sa.addField("Issuer:", mod, true);
		sa.setFallback("Evidence for: " + player);
		builder.addAttachment(sa);

		return builder.build();
	}

	public static SlackMessage buildExceptionMessage(Class<?> location,
			Throwable e) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField classField = new SlackField();
		SlackField exceptionField = new SlackField();
		SlackField stackTraceField = new SlackField();
		StringBuilder stackTrace = new StringBuilder();

		for (StackTraceElement s : e.getStackTrace()) {
			stackTrace.append(s.toString() + "\n");
		}

		sa.setColor("#FF0000");
		sa.setPretext("");

		classField.setTitle("Class:").setValue(location.toString());
		exceptionField.setTitle("Exception:").setValue(e.toString());
		stackTraceField.setTitle("Stack Trace:")
				.setValue(stackTrace.toString());
		sa.setFallback("Exception in " + e.getClass().toString());

		sa.addFields(classField).addFields(exceptionField)
				.addFields(stackTraceField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#exceptions");
		sm.setUsername("Exceptor");
		sm.setIcon(":exclamation:");
		return sm;
	}

	public static SlackPreparedMessage buildExceptionMessageNew(
			Class<?> location, Throwable e) {
		SlackPreparedMessage.Builder builder = new SlackPreparedMessage.Builder();
		com.ullink.slack.simpleslackapi.SlackAttachment sa = new com.ullink.slack.simpleslackapi.SlackAttachment();
		StringBuilder stackTrace = new StringBuilder();

		for (StackTraceElement s : e.getStackTrace()) {
			stackTrace.append(s.toString() + "\n");
		}

		sa.setColor("#FF0000");
		sa.setPretext("");

		sa.addField("Class:", location.toString(), false);
		sa.addField("Exception:", e.toString(), false);
		sa.addField("Stack Trace:", stackTrace.toString(), false);
		sa.setFallback("Exception in " + e.getClass().toString());

		builder.addAttachment(sa);
		builder.withMessage("");
		return builder.build();
	}

	public static SlackMessage buildGlitchedLoginMessage(String name) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField playerField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		playerField.setTitle("Bugged Login:").setValue(name);

		sa.setFallback("Bugged Login: " + name);

		sa.addFields(playerField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("Server Watchdog");
		sm.setIcon(":exclamation:");
		return sm;
	}

	public static SlackMessage buildHackerMessage(String name) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField serverField = new SlackField();
		SlackField debugField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		serverField.setTitle("Tracked Player:").setValue(name + " logged in.");

		sa.setFallback("Hacker/Tracked Player: " + name);

		sa.addFields(serverField).addFields(debugField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#admins");
		sm.setUsername("Server Watchdog");
		sm.setIcon(":exclamation:");
		return sm;
	}

	public static SlackMessage buildJudgementMesssage(Player player,
			String subject, String type, String reason) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField issuerField = new SlackField();
		SlackField subjectField = new SlackField();
		SlackField typeField = new SlackField();
		SlackField reasonField = new SlackField();
		sa.setPretext("Command logged");
		issuerField.setTitle("Issuer")
				.setValue((player == null ? "" : player.getName()))
				.setShorten(true);
		subjectField.setTitle("Subject").setValue(subject).setShorten(true);
		typeField.setTitle("Type").setValue(type).setShorten(true);
		sa.addFields(issuerField).addFields(subjectField).addFields(typeField);
		if (!"reason".equals("")) {
			reasonField.setTitle("reason").setValue(reason).setShorten(true);
			sa.addFields(reasonField);
		}
		sa.addFields(serverField);
		sm.setText("");
		sa.setFallback(subject + " " + type);
		sa.setColor("#FF0000");
		sm.setChannel("#judgement");
		sm.addAttachments(sa);
		sm.setUsername("Inspector Ray");
		sm.setIcon(":mag:");
		return sm;
	}

	public static SlackMessage buildLoginMessage(String server,
			String playerName, String channel, String ip, String uuid) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField playerField = new SlackField();
		SlackField ipField = new SlackField();
		SlackField uuidField = new SlackField();
		sa.setColor("#777777");
		sa.setPretext("New login:");

		playerField.setTitle("Account:").setValue(playerName).setShorten(true);
		ipField.setTitle("IP:").setValue(ip).setShorten(true);
		uuidField.setTitle("UUID:").setValue(uuid).setShorten(true);

		sa.setFallback("New login: " + ip);

		sa.addFields(playerField).addFields(ipField).addFields(uuidField)
				.addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("@" + channel);
		sm.setUsername("Login Watchdog");
		sm.setIcon(":warning:");
		return sm;
	}

	public static SlackMessage buildPlayerOnlineMessage(String channel,
			String sender) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		sa.setColor("#0000FF");
		int totalCount = 0;
		for (int i = 0; i < PlayerHandler.getNodePlayerCount().length; i++) {
			int playerCount = PlayerHandler.getNodePlayerCount()[i];
			if (playerCount > 0) {
				SlackField field = new SlackField();
				field.setShorten(true);
				field.setTitle("World " + (i + 1));
				field.setValue(Integer.toString(playerCount));
				sa.addFields(field);
				totalCount += PlayerHandler.getNodePlayerCount()[i];
			}
		}
		sa.setPretext("There's " + totalCount + " players online, @" + sender);
		sa.setFallback("Players online: " + totalCount);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#" + channel);
		sm.setUsername("SoulBot");
		sm.setIcon(":sp:");
		return sm;
	}

	public static SlackMessage buildServerUsageMessage(String status,
			int statusInt) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField statusMessage = new SlackField();
		SlackField statusField = new SlackField();

		String state = Server.getSystemMonitor().getStatus();

		switch (statusInt) {
			case 0:
				sa.setColor("#00FF00");
				break;
			case 1:
				sa.setColor("#FF9900");
				break;
			case 2:
				sa.setColor("#FF0000");
				break;
			case 3:
				sa.setColor("#FF0000");
				break;
		}

		sa.setPretext("");

		statusMessage.setTitle("State").setValue(state);
		statusField.setTitle("Details").setValue(status);

		sa.setFallback(state);

		sa.addFields(statusMessage).addFields(statusField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("System Monitor");
		if (statusInt > 0) {
			sm.setIcon(":chart_with_upwards_trend:");
		} else {
			sm.setIcon(":chart_with_downwards_trend:");
		}
		return sm;
	}

	public static SlackMessage buildSpammerMessage(String name) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField serverField = new SlackField();
		SlackField debugField = new SlackField();
		sa.setColor("#FF0000");
		sa.setPretext("");

		serverField.setTitle("Possible DDOSer or Lagger:").setValue(name);

		sa.setFallback("Lagger/DDOSer: " + name);

		sa.addFields(serverField).addFields(debugField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#admins");
		sm.setUsername("Server Watchdog");
		sm.setIcon(":exclamation:");
		return sm;
	}

	public static SlackMessage buildStartMessage(String servername,
			boolean shutdown, String channel) {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField serverField = new SlackField();
		SlackField debugField = new SlackField();

		if (shutdown) {
			sa.setColor("#ff0000");
		} else {
			sa.setColor("#00ff00");
		}
		sa.setPretext("Server " + (shutdown ? "Shutdown" : "Startup"));

		serverField.setTitle("Server:").setValue(servername);
		debugField.setTitle("Event: ")
				.setValue((shutdown ? "Shutdown" : "Startup"));

		sa.setFallback("Server " + (shutdown ? "Shutdown" : "Startup")
				+ " Server: " + servername);

		sa.addFields(serverField).addFields(debugField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel(channel);
		sm.setUsername("Server Watchdog");
		if (shutdown) {
			sm.setIcon(":x:");
		} else {
			sm.setIcon(":white_check_mark:");
		}
		return sm;
	}

	public static SlackMessage buildEventMessage(Client player, boolean started, String event) {
		SlackMessage sm = new SlackMessage();
		
		sm.setText((started ? ":heavy_check_mark:" : ":x:") + 
					"*" + event + "* event " + (started ? "started" : "ended") + 
					" by " + player.getName() +
					" (" + Server.getWorld().getDesc() + ")");

		sm.setChannel("events");
		sm.setUsername("Event Manager");
		sm.setIcon(":tada:");
		return sm;
	}
	
	public static SlackMessage buildEventStatusMessage() {
		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField soulWarsField = new SlackField();
		SlackField clanWarsField = new SlackField();
		SlackField castleWarsField = new SlackField();
		SlackField pestControlField = new SlackField();
		SlackField edgePKField = new SlackField();

		sa.setColor("#14CCA2");
		
		sa.setPretext("Event Statusses:");

		soulWarsField.setTitle("Soulwars:").setValue(SoulWars.EXTRA_REWARD ? ":heavy_check_mark:" : "x").setShorten(true);
		clanWarsField.setTitle("Clanwars:").setValue(Player.clanWarsWildyEvent ? ":heavy_check_mark:" : "x").setShorten(true);
		castleWarsField.setTitle("Castlewars:").setValue(CastleWars.EXTRA_REWARD ? ":heavy_check_mark:" : "x").setShorten(true);
		pestControlField.setTitle("PestControl:").setValue(PestControl.EXTRA_REWARD ? ":heavy_check_mark:" : "x").setShorten(true);
		edgePKField.setTitle("Edge PK:").setValue(Player.clanWarsWildyEvent ? ":heavy_check_mark:" : "x").setShorten(true);
		
		sa.setFallback("Event statusses");

		sa.addFields(soulWarsField).addFields(clanWarsField).addFields(castleWarsField).addFields(pestControlField).addFields(edgePKField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("events");
		sm.setUsername("Event Manager");
		sm.setIcon(":tada:");
		return sm;
	}
	
	public static SlackMessage buildLisaDonationMessage(String playerName, Item item) {

		SlackMessage sm = new SlackMessage();
		SlackAttachment sa = new SlackAttachment();
		SlackField nameField = new SlackField();
		SlackField itemField = new SlackField();

		sa.setColor("#14CCA2");
		sa.setPretext("");

		nameField.setTitle("Donation to Lisa by:").setValue(playerName + "");
		itemField.setTitle("Item: ")
				.setValue(ItemProjectInsanity.getItemName(item.getId())+ " - amount: "+item.getAmount());
		sa.setFallback("Player: "+playerName+" Item: "+ ItemProjectInsanity.getItemName(item.getId())+ " - amount: "+item.getAmount());

		sa.addFields(nameField).addFields(itemField).addFields(serverField);
		sm.addAttachments(sa);
		sm.setText("");
		sm.setChannel("#automation");
		sm.setUsername("LMS Watchdog");
		sm.setIcon(":eyes:");
		return sm;
	}
	
}
