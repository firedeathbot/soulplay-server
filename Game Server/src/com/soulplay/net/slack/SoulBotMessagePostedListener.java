package com.soulplay.net.slack;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.World;
import com.soulplay.config.WorldType;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;

public class SoulBotMessagePostedListener implements SlackMessagePostedListener {

	@Override
	public void onEvent(SlackMessagePosted event, SlackSession session) {
		if (event.getSender().getId().equals(session.sessionPersona().getId())) {
			return;
		}
		String[] command = event.getMessageContent().toLowerCase().split(" ", 3);
		if (event.getChannel().getName().equals("events")) {
			if ("eventstatus".equals(command[0].toLowerCase())) {
				if (World.getWorld().getType().equals(WorldType.ECONOMY) && !Config.SERVER_DEBUG) {
					Server.getSlackApi().call(SlackMessageBuilder.buildEventStatusMessage());
				}
			}
		}
	}
}
