package com.soulplay.net.slack;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import com.soulplay.Server;

public class SlackThread {

	public SlackThread() throws InterruptedException {
		Server.getSlackSession()
				.addMessagePostedListener(new SoulBotMessagePostedListener());
		AtomicBoolean stop = new AtomicBoolean(false);
		Executors.newSingleThreadExecutor().execute(() -> {
			try {
				while (!stop.get()) {
					Thread.sleep(10000);
				}
			} catch (Exception e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(this.getClass(), e));
			}
		});
	}

}
