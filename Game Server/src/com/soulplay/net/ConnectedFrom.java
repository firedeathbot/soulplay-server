package com.soulplay.net;

import com.soulplay.game.model.player.Client;

/**
 * @author Division Tracking ip's that logged in on your account.
 */

public class ConnectedFrom {

	public static void addConnectedFrom(Client client, String host, String uuid) {
		if (client.lastConnectedFrom.contains(host)) {
			removeHostFromList(client, host); // i guess this is to resort it so latest is shown first?
			client.lastConnectedFrom.add(host);
		} else {
			client.lastConnectedFrom.add(host);
		}
		
		if (!client.lastConnectedFromUUID.contains(uuid)) {
			client.lastConnectedFromUUID.add(uuid);
		}
	}

	public static void removeHostFromList(Client client, String host) {
		client.lastConnectedFrom.remove(host);
	}
}
