package com.soulplay.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.Type;

import com.soulplay.game.model.player.PlayerHandler;

public class ProxyCheck {

	public static boolean checkIP(String playerName, String reverseIP) {
		try {
			Lookup lookup = new Lookup(reverseIP + ".dnsbl.dronebl.org",
					Type.A);
			Record[] records = lookup.run();
			if (records != null) {
				for (Record record : records) {
					ARecord a = (ARecord) record;
					if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.8"))) {
						PlayerHandler.alertStaffSync(
								"Socks proxy in dronebl! Player " + playerName);
						return true;
					} else if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.9"))) {
						PlayerHandler.alertStaffSync(
								"Http proxy in dronebl! Player " + playerName);
						return true;
					} else if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.10"))) { // proxy
						PlayerHandler.alertStaffSync(
								"Proxy chain in dronebl! Player " + playerName);
						return true;
					} else if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.14"))) {
						PlayerHandler.alertStaffSync(
								"Winegate proxy in dronebl! Player "
										+ playerName);
						return true;
					} else {
						PlayerHandler.alertStaffSync(
								"Proxy in dronebl! Player " + playerName);
						return true;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

		// Check sectoor: Tor exit node BlackList
		try {
			Lookup lookup = new Lookup(reverseIP + ".tor.dnsbl.sectoor.de",
					Type.CNAME);
			Record[] records = lookup.run();
			if (records != null) {
				for (Record record : records) {
					CNAMERecord a = (CNAMERecord) record;
					if (a.getName().equals("torserver.tor.dnsbl.sectoor.de")) {
						PlayerHandler.alertStaffSync(
								"Tor exit node in sectoor.de! Player "
										+ playerName);
						return true;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

		// Check spamhaus' XBL list (proxies)
		try {
			Lookup lookup = new Lookup(reverseIP + ".xbl.spamhaus.org", Type.A);
			Record[] records = lookup.run();
			if (records != null) {
				PlayerHandler.alertStaffSync(
						"Record in spamhaus! Player " + playerName);
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

		// Check ProxyBL
		try {
			Lookup lookup = new Lookup(reverseIP + ".dnsbl.proxybl.org",
					Type.A);
			Record[] records = lookup.run();
			if (records != null) {
				PlayerHandler.alertStaffSync(
						"Proxy in ProxyBL! Player " + playerName);
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

		// Check Sorbs' proxy lists
		try {
			Lookup lookup = new Lookup(reverseIP + ".proxies.dnsbl.sorbs.net",
					Type.A);
			Record[] records = lookup.run();
			if (records != null) {
				PlayerHandler
						.alertStaffSync("Proxy in sorbs! Player " + playerName);
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

		// Check Torenvall list
		try {
			Lookup lookup = new Lookup(reverseIP + ".dnsbl.tornevall.org",
					Type.A);
			Record[] records = lookup.run();
			if (records != null) {
				for (Record record : records) {
					ARecord a = (ARecord) record;
					if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.2"))) {
						PlayerHandler.alertStaffSync(
								"Proxy in Tornevall! Player " + playerName);
						return true;
					} else if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.32"))) {
						PlayerHandler.alertStaffSync(
								"Proxy in Tornevall! Player " + playerName);
						return true;
					} else if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.64"))) {
						PlayerHandler.alertStaffSync(
								"Proxy in Tornevall! Player " + playerName);
						return true;
					} else if (a.getAddress()
							.equals(InetAddress.getByName("127.0.0.2"))) {
						PlayerHandler.alertStaffSync(
								"Proxy in Tornevall! Player " + playerName);
						return true;
					} else {
						PlayerHandler.alertStaffSync(
								"Possible proxy, listed in Tornevall. Player: "
										+ playerName);
						return true;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		return false;
	}

	private static JSONObject connectionToJSONObject(
			HttpURLConnection connection) {
		BufferedReader reader;
		try {
			connection.connect();

			if (connection.getResponseCode() == 500) {
				return null;
			}

			reader = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		StringBuilder builder = new StringBuilder();
		String aux = "";

		try {
			while ((aux = reader.readLine()) != null) {
				builder.append(aux);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		String response = builder.toString();

		JSONObject json;
		try {
			json = new JSONObject(response);
		} catch (JSONException e1) {
			e1.printStackTrace();
			return null;
		}

		return json;
	}

	public static boolean isProxyDroneBL(String ip) {
		try {
			String[] ipParts = ip.split("\\.");
			String reverseIP = new StringBuilder().append(ipParts[3] + ".")
					.append(ipParts[2] + ".").append(ipParts[1] + ".")
					.append(ipParts[0]).toString();

			URL urlDroneBL = new URL("http://api.statdns.com/" + reverseIP
					+ ".dnsbl.dronebl.org/a");

			HttpURLConnection connectionDroneBL = (HttpURLConnection) urlDroneBL
					.openConnection();
			connectionDroneBL.setRequestProperty("Accept", "application/json");

			JSONObject jsonDroneBL = connectionToJSONObject(connectionDroneBL);
			if (jsonDroneBL == null) {
				return false;
			}

			return jsonDroneBL.has("answer");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean isProxyRandom(String ip) {
		try {
			URL url = new URL("https://iphub.p.mashape.com/api.php?ip=" + ip
					+ "&showtype=4");

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestProperty("X-Mashape-Key",
					"RUp1iy4Y36mshjhcxgvFS4yASH7Qp13q9cojsnkfRMKKuGywpm");
			connection.setRequestProperty("Accept", "application/json");

			JSONObject json = connectionToJSONObject(connection);

			if (json == null) {
				return false;
			}

			boolean isProxy = json.getInt("proxy") != 0;

			connection.disconnect();

			return isProxy;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
