package com.soulplay.util.log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.net.login.Login;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.sql.LogHandler;
import com.soulplay.util.sql.SqlHandler;

public class TestLogWriter {

	static List<Login> loginList = new ArrayList<>();

	private static long lastUpdate = 0;

	public static void addToLoginList(Login login) {
		if (!Config.RUN_ON_DEDI) {
			return;
		}
		try {
			synchronized (loginList) {
				loginList.add(login);
			}
		} catch (Exception e) {
			Server.getSlackApi().call(SlackMessageBuilder
					.buildExceptionMessage(TestLogWriter.class, e));
		}
	}

	AtomicBoolean stop = new AtomicBoolean(false);

	/*
	 * Test logger class that is safe to crash without affecting other logs. :)
	 */
	public TestLogWriter() throws InterruptedException {
		Executors.newSingleThreadExecutor().execute(() -> {
			Thread.currentThread().setName("test-log-writer");
			try {
				while (!stop.get()) {
					Thread.sleep(100/* Server.cycleRate */);
					logLists();
					if (System.currentTimeMillis() - lastUpdate > (1000 * 60)) {
						if (DBConfig.UPDATE_UPTIME) {
							SqlHandler.updateUptime();
						}
						lastUpdate = System.currentTimeMillis();
					}

				}
			} catch (Exception e) {
				Server.getSlackApi().call(SlackMessageBuilder
						.buildExceptionMessage(this.getClass(), e));
			}
		});
	}

	public boolean getState() {
		return stop.get();
	}

	private void logLists() {
		logLogin();
	}

	private void logLogin() {
		List<Login> temp = new ArrayList<>();
		synchronized (loginList) {
			temp.addAll(loginList);
		}
		if (!temp.isEmpty()) {
			for (Login tempLogin : temp) {
				LogHandler.logLogin(tempLogin);
			}
			synchronized (loginList) {
				loginList.removeAll(temp);
			}
			temp.clear();
		}
	}

	public void toggleLogger() {
		boolean v = stop.get();
		stop.compareAndSet(v, !v);
	}
}
