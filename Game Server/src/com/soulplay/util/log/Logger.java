package com.soulplay.util.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.apache.log4j.Level;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.WorldType;

/**
 * @author nick
 */
public class Logger extends PrintStream {

	// private DateFormat dateFormat = new SimpleDateFormat();
	// private Date cachedDate = new Date();
	
	private static String logLoc = WorldType.equalsType(WorldType.SPAWN) ? "./SystemLogsPvP" : "./SystemLogs";

	public static void writeFile1(String str) throws IOException {
		File dir = new File(logLoc);
		
		if (!dir.exists()) {
			dir.mkdirs();
		}		
		
		String loc = dir.getCanonicalPath() + File.separator + ""
				+ Server.getCalendar().getMonth() + ""
				+ Server.getCalendar().currentDayOfMonth() + "-"
				+ Server.getCalendar().getYear() + "Logs.txt";
		
		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(loc, true)))) {
			out.println(str);
		}		

	}

	public Logger(PrintStream out) {
		super(out);
	}

	private String getPrefix() {
		return Server.getCalendar().getMonth() + " "
				+ Server.getCalendar().currentDayOfMonth() + ", "
				+ Server.getCalendar().getYear() + " - "
				+ Server.getCalendar().getTime();// dateFormat.format(cachedDate);
	}

	@Override
	public void print(String str) {
		// if (str.contains("[MOTIVOTE]") || str.contains("motivote")) {
		// return;
		// }
		if (str.startsWith("debug:")) {
			super.print("[" + getPrefix() + "] DEBUG: " + str.substring(6));
		} else {

			// if (Server.getPanel() != null) {
			// if (Server.getPanel().PANEL_ACTIVE) {
			// Server.getPanel().SERVER_CONSOLE.append(str + "\n");
			// Server.getPanel().CONSOL_SCROLLER.getVerticalScrollBar().setValue(Server.getPanel().CONSOL_SCROLLER.getVerticalScrollBar().getMaximum());
			// }
			// }

			super.print("[" + getPrefix() + "]: " + str);
			// log.log("Error", null, str, null);

			// writeToFile("[" + getPrefix() + "]: " + str);
			if (!Config.SERVER_DEBUG && Server.getSystemLogs()
					&& !str.contains("[MOTIVOTE]") && !str.contains("motivote")
					&& !str.contains("[MISC]")) {
				try {
					writeFile1("[" + getPrefix() + "]: " + str);
				} catch (IOException e) {
					Server.toggleSystemLogs();
					e.printStackTrace();
				}
			}

		}
	}
}
