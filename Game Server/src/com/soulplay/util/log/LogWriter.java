package com.soulplay.util.log;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import com.soulplay.DBConfig;
import com.soulplay.content.minigames.duel.Stake;
import com.soulplay.content.player.combat.Kill;
import com.soulplay.content.player.commands.CommandLogEntry;
import com.soulplay.content.player.trading.TradingPostOrder;
import com.soulplay.content.player.trading.Trade;
import com.soulplay.content.shops.Buy;
import com.soulplay.content.shops.BuyPOS;
import com.soulplay.content.shops.Sell;
import com.soulplay.game.model.item.DroppedItem;
import com.soulplay.game.model.item.PickedUpItem;
import com.soulplay.game.model.player.wrapper.Empty;
import com.soulplay.net.login.Login;
import com.soulplay.util.sql.LogHandler;

public class LogWriter {
	
	private static final ScheduledExecutorService LOG_EXECUTOR = Executors.newSingleThreadScheduledExecutor();

	public LogWriter() throws InterruptedException {
		LOG_EXECUTOR.submit(new Runnable() {
			
			@Override
			public void run() {
				Thread.currentThread().setName("log-writer");
			}
		});
	}

	public void addToAlchMap(final int mysqlIndex/* String name */, int i) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logAlch(mysqlIndex, i);
			}
		});
	}

	public void addToBuyList(final Buy buy) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logBuy(buy);
			}
		});
	}

	public void addToBuyPOSList(final BuyPOS buy) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logBuyPOS(buy);
			}
		});
	}
	
	public void addToTradingPostSaleList(final TradingPostOrder order) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logTradingPostSale(order);
			}
		});
	}
	
	public void addToNameChangeList(final NameChangeLog log) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logNameChange(log);
			}
		});
	}

	public void addToCommandList(final CommandLogEntry commandLogEntry) {
		if (!DBConfig.LOG_COMMANDS) {
			return;
		}
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logCommand(commandLogEntry);
			}
		});
	}

	public void addToDropList(final DroppedItem drop) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logDrop(drop);
			}
		});
	}

	public void addToDropList(final List<DroppedItem> drop) {
		
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logDrop(drop);
			}
		});
	}

	public void addToEmptyList(final Empty empty) {
		if (!DBConfig.LOG_EMPTY) {
			return;
		}
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logEmpty(empty);
			}
		});

	}

	public void addToKillList(final Kill kill) {
		if (!DBConfig.LOG_KILL) {
			return;
		}
		
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logKill(kill);
			}
		});
	}

	public void addToLoginList(final Login login) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logLogin(login);
			}
		});
	}

	public void addToPickedUpList(final PickedUpItem pickedup) {
		if (pickedup.getItem().getOriginalOwnerName() == null)
			return;
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logPickup(pickedup);
			}
		});
	}

	public void addToSellList(final Sell sell) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logSell(sell);
			}
		});
	}

	public void addToStakeListList(final List<Stake> stakeList) {
		LOG_EXECUTOR.submit(new Runnable() {

			@Override
			public void run() {
				LogHandler.logStake(stakeList);
			}
		});
	}

	public void addToTradeListList(final List<Trade> tradeList) {
		
		LOG_EXECUTOR.submit(new Runnable() {
			
			@Override
			public void run() {
				LogHandler.logTrade(tradeList);
				LogHandler.logTrade2(tradeList);
			}
		});
		
	}

}
