package com.soulplay.util.log;

import java.sql.Timestamp;

import com.soulplay.util.Misc;

public class NameChangeLog {
	
	private int mysqlId;
	private String username;
	private String prevDisplayName;
	private String newDisplayName;
	private Timestamp ts;
	
	public NameChangeLog(int id, String username, String oldName, String newName) {
		this.mysqlId = id;
		this.username = username;
		this.prevDisplayName = oldName;
		this.newDisplayName = newName;
		this.setTs(Misc.getESTTimestamp());
	}

	public int getMysqlId() {
		return mysqlId;
	}
	
	public void setMysqlId(int mysqlId) {
		this.mysqlId = mysqlId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPrevDisplayName() {
		return prevDisplayName;
	}
	public void setPrevDisplayName(String prevDisplayName) {
		this.prevDisplayName = prevDisplayName;
	}
	public String getNewDisplayName() {
		return newDisplayName;
	}
	public void setNewDisplayName(String newDisplayName) {
		this.newDisplayName = newDisplayName;
	}

	public Timestamp getTimestamp() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}
	
	

}
