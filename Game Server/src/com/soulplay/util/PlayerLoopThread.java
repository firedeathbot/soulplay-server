package com.soulplay.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.clans.ClanManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.AnnouncementManager;
import com.soulplay.net.ProxyCheck;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.sql.Highscore;
import com.soulplay.util.sql.SqlHandler;

public class PlayerLoopThread {

	private long dungListTimer = 1000 * 3600; // Hour

	private long dungListClean = System.currentTimeMillis();

	private List<String> smugglerList = new ArrayList<>();

	private List<Integer> infinityList = new ArrayList<>();

	private List<String> proxyList = new ArrayList<>();

	private List<String> duperList = new ArrayList<>();

	private long lastUpdate = 0;
	
	private long lastAnnounceUpdate = 0;

	final List<Integer> boltsandarrows = Arrays.asList(new Integer[]{
			// 882, 884, 886, 888, 890, 892, 11212, 9140, 9141, 9142, 9143,
			// 9144, 9242, 9243, 9244, 9245, 9339, 9340, 9341, 9342, //bolts

			// place other items here
			11694, 11695, 6585, 6586, 4151, 4152, 20135, 20136, 20139, 20140,
			20143, 20144, 20147, 20148, 20151, 20152, 20155, 20156, 20159,
			20160, 20163, 20164, 20167, 20168, 20171, 20173// sample

	});
	
	public PlayerLoopThread() throws InterruptedException {
		loadInfinity();
		AtomicBoolean stop = new AtomicBoolean(false);
		Executors.newSingleThreadExecutor().execute(() -> {
			Thread.currentThread().setName("player-loop-thread");
			try {
				while (!stop.get()) {
					if (Server.isRunningMainTask()) { // dunno if this will
														// cause lag.
						Thread.sleep(10);
						continue;
					}
					Thread.sleep(Server.CYCLE_RATE * 10);
					try {
						ClanManager.loadClanTag();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					try {
						Highscore.generateTopPvM();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					try {
						ClanManager.loadClanPointsData();
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						loopPlayers();
						cleanLists();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if (!Server.isRunningMainTask()
							&& System.currentTimeMillis()
									- lastUpdate > (1000 * 60)) {
						
						try {
							DBConfig.updateSettings();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						try {
							SqlHandler.updatePlayerCount(
									PlayerHandler.getPlayerCount(),
									PlayerHandler.getStaffCount(),
									PlayerHandler.getWildyCount(), PlayerHandler.getPlayersInSoulWars(), PlayerHandler.getPlayersInCastleWars(), PlayerHandler.getPlayersInClanWars(), PlayerHandler.getPlayersInPestControl(), PlayerHandler.getPlayersInDuelArena(), PlayerHandler.getPlayersInGamblingArea(), PlayerHandler.getPlayersInLMS());
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						lastUpdate = System.currentTimeMillis();
					}
					
					if (!Server.isRunningMainTask()
							&& System.currentTimeMillis()
							- lastAnnounceUpdate > (1000 * 60 * 5)) {
						try {
							if (DBConfig.UPDATE_ANNOUNCEMENTS) {
								AnnouncementManager.updateAnnouncements();
							}
							if (DBConfig.ANNOUNCE) {
								AnnouncementManager.announce();
							}
						} catch (Exception e) {
							Server.getSlackApi().call(SlackMessageBuilder
									.buildExceptionMessage(this.getClass(), e));
						}
						lastAnnounceUpdate = System.currentTimeMillis();
					}
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private void checkDungItems(Player p) {
		if (p == null || !p.isActive) {
			return;
		}
		if (p.insideDungeoneering()) {
			return;
		}
		if (Config.isOwner(p)) {
			return;
		}
		if (smugglerList.contains(p.getName())) {
			return;
		}
		boolean found = false;
		List<Integer> items = new ArrayList<>();
		for (int i = 15708; i <= 17260; i++) {
			if (inDungFilter(i)) {
				continue;
			}
			if (p != null) {
				if (p.getItems() != null) {
					try {
						int itemSlot = p.getItems().getItemSlot(i);
						if (itemSlot != -1) {
							items.add(i);
							found = true;
							if (p.isActive)
								p.getItems().deleteItemInSlot(itemSlot);
						}
						if (p.getItems().bankHasItemAmount(i, 1)) {
							items.add(i);
							found = true;
						}
						if (p.getItems().playerHasEquipped(i)) {
							items.add(i);
							found = true;
						}
						for (int playerShopItem : p.playerShopItems) {
							if (playerShopItem == i) {
								items.add(i);
								found = true;
							}
						}
					} catch (Exception e) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildExceptionMessage(this.getClass(), e));
					}
				}
			}
		}
		for (int i = 17297; i <= 18329; i++) {
			if (inDungFilter(i)) {
				continue;
			}

			if (p != null) {
				if (p.getItems() != null) {
					try {
						int itemSlot = p.getItems().getItemSlot(i);
						if (itemSlot != -1) {
							items.add(i);
							found = true;
							if (p.isActive)
								p.getItems().deleteItemInSlot(itemSlot);
						}
						if (p.getItems().bankHasItemAmount(i, 1)) {
							items.add(i);
							found = true;
						}
						if (p.getItems().playerHasEquipped(i)) {
							items.add(i);
							found = true;
						}
						for (int playerShopItem : p.playerShopItems) {
							if (playerShopItem == i) {
								items.add(i);
								found = true;
							}
						}
					} catch (Exception e) {
						Server.getSlackApi().call(SlackMessageBuilder
								.buildExceptionMessage(this.getClass(), e));
					}
				}
			}
		}
		if (found) {
			// synchronized(smugglerList) {
			smugglerList.add(p.getName());
			// }
			Server.getSlackApi().call(
					SlackMessageBuilder.buildDungMessage(p.getName(), items));
		}
		items.clear();
	}
	
	public static boolean inDungFilter(int i) {
		switch (i) {
		case 17676: //lava dragon bones
		case 17677: // lava dragon bones
		case 15740:
		case 15741:
		case 15742:
		case 18016:
			return true;
		}
		return false;
	}

	private void checkInfinity(Player p) {
		if (p == null) {
			return;
		}
		if (Config.isOwner(p)) {
			return;
		}
		if (duperList.contains(p.getName())) {
			return;
		}
		Map<Integer, Integer> items = new HashMap<>();
		int amount = Integer.MAX_VALUE;
		for (Integer id : infinityList) {
			if (p != null && p.getItems() != null) {
				try {
					if (p.getItems().getItemAmount(id) >= amount) {
						items.put(id, p.getItems().getItemAmount(id));
					}
					if (p.getItems().getBankItemCount(id) >= amount) {
						items.put(id, p.getItems().getBankItemCount(id));
					}
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(this.getClass(), e));
				}
			}
		}
		if (!items.isEmpty()) {
			duperList.add(p.getName());
			Server.getSlackApi().call(
					SlackMessageBuilder.buildDuperMessage(p.getName(), items));
		}
		items.clear();
	}

	private void checkProxy(Player p) {
		if (proxyList.contains(p.getName())) {
			return;
		}
		if (ProxyCheck.checkIP(p.getName(), p.connectedFrom)) {
			proxyList.add(p.getName());
		}
	}

	private void cleanLists() {
		if ((System.currentTimeMillis() - dungListClean) > dungListTimer) {
			smugglerList.clear();
			duperList.clear();
			dungListClean = System.currentTimeMillis();
			proxyList.clear();
		}
	}

	private void loadInfinity() {
//		for (ItemDefinition item : ItemDefinition.getDefinitions()) {
//			if (item == null) {
//				continue;
//			}
//			String itemName = item.getName();
//			if (itemName == null) {
//				continue;
//			}
//			if (itemName.toLowerCase().contains("infinity")) {
//				infinityList.add(Short.toUnsignedInt(item.getId()));
//			}
//		}
		infinityList.add(4049);
		infinityList.add(4045);
	}

	private void loopPlayers() {
		Map<Long, Player> tempPlayerMap = new HashMap<>();
		// synchronized (PlayerHandler.lock) {
		// try {
		// synchronized (PlayerHandler.lock) {
		tempPlayerMap.putAll(PlayerHandler.getPlayerMap());
		// }
		// } catch (Exception e) {
		// Server.getSlackApi().call(SlackMessageBuilder.buildExceptionMessage(this.getClass(),
		// e));
		// }
		// }
		for (Map.Entry<Long, Player> entry : tempPlayerMap.entrySet()) {
			Player tempPlayer;
			tempPlayer = entry.getValue();
			if (tempPlayer == null) {
				continue;
			}
			checkDungItems(tempPlayer);
			// checkBolts(tempPlayer);
			checkInfinity(tempPlayer);
			if (DBConfig.DETECT_PROXY) {
				checkProxy(tempPlayer);
			}
		}
		tempPlayerMap.clear();
	}
}
