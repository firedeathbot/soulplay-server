package com.soulplay.util;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.Timestamp;
import java.text.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.Server;
import com.soulplay.content.player.skills.construction.DynamicRegionClip;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.GsonSave;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;

import io.netty.buffer.ByteBuf;
import io.netty.util.internal.ThreadLocalRandom;

public class Misc {

	public static void test() {
		Map<Integer, ItemDefinition> defs = ItemDefinition.getDefinitions();

		try(BufferedWriter writer = new BufferedWriter(new FileWriter(new File("item_defs.json")))) {
			writer.write(GsonSave.gson.toJson(defs));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean isDiagonal(int x1, int y1, int x2, int y2) {
		return Math.abs(x1 - x2) == Math.abs(y1 - y2);
	}

	public static String formatServerTime() {
		StringBuilder builder = new StringBuilder();

		final int h = Server.getCalendar().getHour();

		if (h < 10) {
			builder.append("0");
		}

		builder.append(h).append(":");

		final int m = Server.getCalendar().getMinutes();

		if (m < 10) {
			builder.append("0");
		}

		builder.append(m).append(" EST");

		return builder.toString();
	}

	/**
	 * An algorithm to rotate a 2d array n times.
	 *
	 * @param input
	 *      The array to rotate.
	 *
	 * @param amount
	 *      The amount of times to rotate..
	 */
	public static <T> T[] rotateClockwise(T[] input, int amount) {
		final T[] array = Arrays.copyOf(input, input.length);

		for (int count = 0; count < amount; count++) {
			for (int i = array.length - 1; i > 0; i--) {
				final T first  = array[i];
				final T second = array[(i + 1) % array.length];

				array[(i + 1) % array.length] = first;
				array[i] = second;
			}
		}
		return array;
	}

	public static Field getField(Class<?> clazz, String name) {
	    while (clazz != null) {
	    	for (Field thisField : clazz.getDeclaredFields()) {
				if (thisField.getName().equalsIgnoreCase(name)) {
					return thisField;
				}
			}
	    	
	    	clazz = clazz.getSuperclass();
	    }

	    return null;
	}

	public static int getRadius(int x1, int y1, int x2, int y2) {
		int deltaX = x1 - x2;
		int deltaZ = y1 - y2;
		int calcX = (int) Math.pow(Math.pow(deltaX, 2), 1.0 / 2);
		int calcZ = (int) Math.pow(Math.pow(deltaZ, 2), 1.0 / 2);
		return Math.max(calcX, calcZ);
	}
	
	public static void faceObject(Player p, GameObject o) {
		faceObject(p, o.getX(), o.getY(), o.getSizeY());
	}

	public static void faceObject(Player c, int obX, int obY, int sizeY) {
		int faceX = 0;
		int faceY = 0;
		if (c.getY() >= obY && c.getY() <= obY + sizeY - 1) {
			faceX = obX;
			faceY = c.getY();
		} else {
			faceY = obY;
			faceX = c.getX();
		}

		if (faceX > 0 && faceY > 0) {
			c.faceLocation(faceX, faceY);
		}
	}

	public static boolean maxIntSum(long sum1, long sum2) {
		return sum1 + sum2 > Integer.MAX_VALUE;
	}
	
	public static void deleteElement(int[] array, int index) {
		for (int i = index, length = array.length - 1; i < length; i++) {
		    array[i] = array[i + 1];
		}
	}
	
	/**
	 * no negative output
	 */
	public static int interpolateSafe(int outputMin, int outputMax, int inputMin, int inputMax, int input) {
		input = Math.min(inputMax, Math.max(inputMin, input));
		return interpolate(outputMin, outputMax, inputMin, inputMax, input);
	}
	
	/**
	 * 
	 * @param outputMin - result based on inputMin
	 * @param outputMax - result based on inputMax
	 * @param inputMin - MUST BE LOWEST NUMBER
	 * @param inputMax - MUST BE HIGHEST NUMBER
	 * @param input - input
	 * @return scaled output based on the inputs
	 */
	public static double interpolateDouble(double outputMin, double outputMax, double inputMin, double inputMax, double input) {
		input = Math.min(inputMax, Math.max(inputMin, input));
		double divideBy = inputMax - inputMin;
		if (divideBy < 1) divideBy = 1;
		return outputMin + (outputMax - outputMin) * (input - inputMin) / (divideBy);
	}
	
	public static long interpolateLong(long outputMin, long outputMax, long inputMin, long inputMax, long input) {
		input = Math.min(inputMax, Math.max(inputMin, input));
		long divideBy = inputMax - inputMin;
		if (divideBy < 1) divideBy = 1;
		return outputMin + (outputMax - outputMin) * (input - inputMin) / (divideBy);
	}

	public static int interpolate(int outputMin, int outputMax, int inputMin, int inputMax, int input) {
		int divideBy = inputMax - inputMin;
		if (divideBy < 1) divideBy = 1;
		return outputMin + (outputMax - outputMin) * (input - inputMin) / (divideBy);
	}

	public static final int[] OBJECT_SLOTS = { 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3 };
	
	public static final boolean objectTypesCollide(int type1, int type2) {
		return OBJECT_SLOTS[type1] == OBJECT_SLOTS[type2];
	}

	public static String ticksToTime(int ticks, boolean appendHourZero) {
		StringBuilder sb = new StringBuilder(8);
		int math = (ticks * 600 / 1000);
		int seconds = math % 60;
		int minutes = math / 60;
		int hours = (minutes / 60);
		minutes = minutes % 60;

		if (hours < 10 && appendHourZero) {
			sb.append("0");
		}

		if (appendHourZero) {
			sb.append(hours);
			sb.append(":");
		} else if (hours > 0) {
			sb.append(hours);
			sb.append(":");
		}

		if (minutes < 10) {
			sb.append("0");
		}
		sb.append(minutes);
		sb.append(":");
		if (seconds < 10) {
			sb.append("0");
		}
		sb.append(seconds);

		return sb.toString();
	}
	
	public static String ticksToTimeShort(int ticks) {
		StringBuilder sb = new StringBuilder(8);
		int math = (ticks * 600 / 1000);
		int seconds = math % 60;
		int minutes = math / 60;
		minutes = minutes % 60;

		if (minutes < 10) {
			sb.append("0");
		}
		sb.append(minutes);
		sb.append(":");
		if (seconds < 10) {
			sb.append("0");
		}
		sb.append(seconds);

		return sb.toString();
	}
	
	public static final int MAX_MESSAGE_SIZE = 150;

	public static String escape(String text) {
		if (text == null) {
			return null;
		}

		int realLength = text.length();
		int newLength = 0;
		for (int i = 0; i < realLength; i++) {
			char c = text.charAt(i);
			if (c == '<' || c == '>') {
				newLength += 3;
			}
		}
		StringBuffer stringbuffer = new StringBuffer(realLength + newLength);
		for (int i = 0; i < realLength; i++) {
			char c = text.charAt(i);
			if (c == '<') {
				stringbuffer.append("<lt>");
			} else if (c == '>') {
				stringbuffer.append("<gt>");
			} else {
				stringbuffer.append(c);
			}
		}
		return stringbuffer.toString();
	}

    public static boolean colides(int x1, int y1, int size1, int x2, int y2, int size2) {
    	int distanceX = x1 - x2;
		int distanceY = y1 - y2;
		return distanceX < size2 && distanceX > -size1 && distanceY < size2 && distanceY > -size1;
    }

	public static boolean isTileFree(int plane, int x, int y, int size, DynamicRegionClip clip) {
		for (int tileX = x; tileX < x + size; tileX++) {
			for (int tileY = y; tileY < y + size; tileY++) {
				int clipMask = clip.getClipping(tileX, tileY, plane);
				if (!RegionClip.isFloorFree(clipMask) || !RegionClip.isWallsFree(clipMask)) {
					return false;
				}
			}
		}

		return true;
	}

	public static Location getFreeTile(Location center, int distance) {
		Location tile = center;
		for (int i = 0; i < 10; i++) {
			tile = new Location(center, distance);
			if (RegionClip.canStandOnSpot(tile.getX(), tile.getY(), tile.getZ())) {
				return tile;
			}
		}

		return center;
	}
	
	public static String enumName(Enum<?> e) {
		return Misc.capitalizeString(e.name().toLowerCase().replace('_', ' '));
	}
	
	public static int getNextTile(int x1, int x2) {		
		
		final int dx = x1 - x2;
		
		if ((dx) == 0) {
			return 0;
		} else if (dx < 0) {
			return 1;
		} else if (dx > 0) {
			return -1;
		}
		
		return 0;
	}

	public static String formatSentence(String string) {
		char[] chars = string.toLowerCase().toCharArray();
		boolean found = true;
		for (int i = 0; i < chars.length; i++) {
			if (found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = false;
			} else if (chars[i] == '.' || chars[i] == '!' || chars[i] == '?') {
				found = true;
			}
		}
		return String.valueOf(chars);
	}

	public static String capitalizeString(String string) {
		char[] chars = string.toLowerCase().toCharArray();
		boolean found = false;
		for (int i = 0; i < chars.length; i++) {
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
				found = false;
			}
		}
		return String.valueOf(chars);
	}

	public static final int[][] DIRECTION = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };

	/**
	 * changed "_" to " "
	 */
	private static final char[] playerNameXlateTable = { '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9' };

	private static char decodeBuf[] = new char[4096];

	public static final char xlateTable[] = { ' ', 'e', 't', 'a', 'o', 'i', 'h', 'n', 's', 'r', 'd', 'l', 'u', 'm', 'w',
			'c', 'y', 'f', 'g', 'p', 'b', 'v', 'k', 'x', 'j', 'q', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', ' ', '!', '?', '.', ',', ':', ';', '(', ')', '-', '&', '*', '\\', '\'', '@', '#', '+', '=', '\243',
			'$', '%', '"', '[', ']', '>', '<', '^', '`', '~', '_', '/', '{', '|', '}' };

	public static final byte directionDeltaX[] = new byte[] { 0, 1, 1, 1, 0, -1, -1, -1 };

	public static final byte directionDeltaY[] = new byte[] { 1, 1, 0, -1, -1, -1, 0, 1 };

	public static final byte xlateDirectionToClient[] = new byte[] { 1, 2, 4, 7, 6, 5, 3, 0 };
	
    public static final byte[] ROTATION_DIR_X = { -1, 0, 1, 0 };

    public static final byte[] ROTATION_DIR_Y = { 0, 1, 0, -1 };

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	private static int lastIndex = 1;
	public static int findFreePlayerSlot() {
		for (int count = 0; count < PlayerHandler.players.length; count++) {
			if (PlayerHandler.players[lastIndex] == null) {
				return lastIndex;
			}
			lastIndex++;
			if (lastIndex >= PlayerHandler.players.length)
				lastIndex = 1;
		}
		return -1;
	}

	private static int lastNpcIndex = 1;
	public static int findFreeNpcSlot() {
		for (int count = 1; count < NPCHandler.npcs.length; count++) {
			if (NPCHandler.npcs[lastNpcIndex] == null) {
				return lastNpcIndex;
			}
			lastNpcIndex++;
			if (lastNpcIndex >= NPCHandler.npcs.length)
				lastNpcIndex = 1;
		}
		return -1;
	}

	public static int localizeLocation(int coordinate, int mapRegion) {
		return coordinate - 8 * mapRegion;
	}

	public static String aOrAn(String word) {
		char c = word.toLowerCase().charAt(0);
		return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') ? "an" : "a";
	}

	public static String basicEncrypt(String s) {
		String toReturn = "";
		for (int j = 0; j < s.length(); j++) {
			toReturn += (int) s.charAt(j);
		}
		// System.out.println("Encrypt: " + toReturn);
		return toReturn;
	}

	public static String capitalize(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (i == 0) {
				s = String.format("%s%s", Character.toUpperCase(s.charAt(0)), s.substring(1));
			}
			if (!Character.isLetterOrDigit(s.charAt(i))) {
				if (i + 1 < s.length()) {
					s = String.format("%s%s%s", s.subSequence(0, i + 1), Character.toUpperCase(s.charAt(i + 1)),
							s.substring(i + 2));
				}
			}
		}
		return s;
	}

	public static Object cloneObject(Object obj) {
		try {
			Object clone = obj.getClass().newInstance();
			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				field.set(clone, field.get(obj));
			}
			return clone;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Map<TimeUnit, Long> computeDiff(Date date1, Date date2) {
		long diffInMillies = date2.getTime() - date1.getTime();
		List<TimeUnit> units = new ArrayList<>(EnumSet.allOf(TimeUnit.class));
		Collections.reverse(units);
		Map<TimeUnit, Long> result = new LinkedHashMap<>();
		long milliesRest = diffInMillies;
		for (TimeUnit unit : units) {
			long diff = unit.convert(milliesRest, TimeUnit.MILLISECONDS);
			long diffInMilliesForUnit = unit.toMillis(diff);
			milliesRest = milliesRest - diffInMilliesForUnit;
			result.put(unit, diff);
		}
		return result;
	}

	public static boolean contains(int value, int[] array) {
		return Arrays.stream(array).anyMatch(it -> it == value);
	}

	public static int direction(int srcX, int srcY, int destX, int destY) {
		final int x = destX - srcX;
		final int y = destY - srcY;

		final boolean north = (y > 0);
		final boolean south = (y < 0);
		final boolean east = (x > 0);
		final boolean west = (x < 0);

		final boolean northe = north && east && !west && !south;
		final boolean southe = south && east && !west && !north;
		final boolean northw = north && west && !east && !south;
		final boolean southw = south && west && !east && !north;

		if (srcX == destX && srcY == destY) {
			return -1; // Same square
		} else if (north && !northe && !northw) {
			return 0; // North
		} else if (north && northe && !northw) {
			return 2; // North east
		} else if (north && northw && !northe) {
			return 14; // North west
		} else if (south && !southe && !southw) {
			return 8; // South
		} else if (south && southe && !southw) {
			return 6; // South east
		} else if (south && southw && !southe) {
			return 10; // South west
		} else if (east && !southe && !northe) {
			return 4; // East
		} else if (west && !southw && !northw) {
			return 12; // West
		}

		return 1;
	}

	public static int distanceToPoint(int pointX, int pointY, int pointX2, int pointY2) {
		return (int) Math.sqrt(Math.pow(pointX2 - pointX, 2) + Math.pow(pointY2 - pointY, 2));
	}

	@SuppressWarnings("rawtypes")
	private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
		List<Class> classes = new ArrayList<>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(
						Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}

	public static String format(long num) {
		return NumberFormat.getInstance().format(num);
	}

	public static String formatNumbersWithCommas(long number) {
		return FORMATTER.format(number);
	}

	public static String formatPlayerName(String str) {
		str = ucFirst(str);
		str.replace("_", " ");
		return str;
	}

	public static String formatShortPrice(long price) {
		if (price >= 1000 && price < 1000000) {
			return "" + (price / 1000) + "K";
		} else if (price >= 1000000 && price < 1000000000) {
			return "" + (price / 1000000) + "M";
		} else if (price >= 1000000000 && price < 1000000000000L) {
			return "" + (price / 1000000000) + "B";
		} else if (price >= 1000000000000L) {
			return "" + (price / 1000000000000L) + "T";
		} else {
			return "" + price;
		}
	}

	public static int generateRandomActionId() {
		return Misc.random2(1000000);
	}

	public static UUID generateUUID() {
		return UUID.randomUUID();
	}

	@SuppressWarnings("rawtypes")
	public static Class[] getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class> classes = new ArrayList<>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return classes.toArray(new Class[classes.size()]);
	}

	// See "getSimilarityIndex" comments
	private static int getPlainSimilarityIndex(String base, String other) {
		String[] baseStrings = base.toLowerCase().split(" ");
		String[] otherStrings = other.toLowerCase().split(" ");
		if (baseStrings.length != otherStrings.length) {
			return -1;
		}
		boolean hasSameWord = false;
		for (String s : baseStrings) {
			for (String s2 : otherStrings) {
				if (!hasSameWord) {
					hasSameWord = s.equals(s2);
				}
			}
		}
		if (!hasSameWord) {
			return -1;
		}
		int index = 0;
		char[] baseChars = base.toCharArray();
		char[] otherChars = other.toCharArray();
		for (Character b : baseChars) {
			for (Character o : otherChars) {
				if (b.compareTo(o) == 0) {
					break;
				}
				index += 1;
			}
		}
		index = index < 0 ? index * -1 : index;

		return index;
	}

	public static Object getRandomValueFromArray(Object[] array) {
		return array[(int) (Math.random() * array.length)]; //return array[(int) (ThreadLocalRandom.current().nextDouble() * array.length)];
	}

	public static String getRS2String(final ByteBuf buf) {
		final StringBuilder bldr = new StringBuilder();
		byte b;
		while (buf.isReadable() && (b = buf.readByte()) != 10) {
			bldr.append((char) b);
		}
		// String pass = bldr.toString();
		// if (pass.contains("\r")) {
		// pass = pass.replaceAll("\r", "");
		// }
		return /* pass */bldr.toString();
	}

	// Just some on-the-fly percent error thing I made to determine if strings
	// were close.
	public static double getSimilarityIndex(String base, String other) {
		// long start = System.nanoTime();
		int baseIndex = getPlainSimilarityIndex(base, base);
		String[] baseStrings = base.toLowerCase().split(" ");
		String[] otherStrings = other.toLowerCase().split(" ");
		if (baseStrings.length != otherStrings.length) {
			return 1;
		}
		boolean hasSameWord = false;
		for (String s : baseStrings) {
			for (String s2 : otherStrings) {
				if (!hasSameWord) {
					hasSameWord = s.equals(s2);
				}
			}
		}
		if (!hasSameWord) {
			return 1;
		}
		int index = 0;
		char[] baseChars = base.toCharArray();
		char[] otherChars = other.toCharArray();
		for (Character b : baseChars) {
			for (Character o : otherChars) {
				if (b.compareTo(o) == 0) {
					break;
				}
				index += 1;
			}
		}
		index = index < 0 ? index * -1 : index;
		double percentError = ((double) baseIndex - ((double) index)) / baseIndex;
		// System.out.println("PETotal time: "+(System.nanoTime() - start) /
		// 1000000+"ms");
		return percentError < 0 ? percentError * -1 : percentError;
	}

	public static boolean goodDistance(int objectX, int objectY, int playerX, int playerY, int distance) {
		// return ((objectX - playerX <= distance && objectX - playerX >=
		// -distance) && (objectY - playerY <= distance && objectY - playerY >=
		// -distance));
		return distanceToPoint(objectX, objectY, playerX, playerY) <= distance;
	}

	public static String Hex(byte data[]) {
		return Hex(data, 0, data.length);
	}

	public static String Hex(byte data[], int offset, int len) {
		String temp = "";
		for (int cntr = 0; cntr < len; cntr++) {
			int num = data[offset + cntr] & 0xFF;
			String myStr;
			if (num < 16) {
				myStr = "0";
			} else {
				myStr = "";
			}
			temp += myStr + Integer.toHexString(num) + " ";
		}
		return temp.toUpperCase().trim();
	}

	public static int hexToInt(byte data[], int offset, int len) {
		int temp = 0;
		int i = 1000;
		for (int cntr = 0; cntr < len; cntr++) {
			int num = (data[offset + cntr] & 0xFF) * i;
			temp += num;
			if (i > 1) {
				i = i / 1000;
			}
		}
		return temp;
	}

	public static boolean inArea(int[] x, int[] y, int testX, int testY) {
		boolean c = false;
		for (int i = 0, j = x.length - 1; i < x.length; j = i++) {
			if (((y[i] > testY) != (y[j] > testY)) && (testX < (x[j] - x[i]) * (testY - y[i]) / (y[j] - y[i]) + x[i])) {
				c = !c;
			}
		}
		return c;
	}

	public static boolean isBad(String text) {
		for (String s : DBConfig.RSPS_NAMES) {
			if (text.toLowerCase().contains(s)) {
				return true;
			}
		}
		for (String s : DBConfig.SPLIT_SIGNS) {
			for (String s2 : DBConfig.BAD_WORDS) {
				if (text.lastIndexOf(s2) == text.length() - s2.length()) {
					if (text.equalsIgnoreCase(s + s2) || text.equalsIgnoreCase(s + " " + s2)) {
						return false;
					}
					if (text.startsWith(s + s2) || text.startsWith(s + " " + s2)) {
						return false;
					}
					if (text.contains(s + s2) || text.contains(s + " " + s2)) {
						for (String s3 : DBConfig.GOOD_WORDS) {
							if (text.toLowerCase().contains(s3)) {
								return false;
							}
						}
						return true;
					}
				} else {
					if ((text.contains(s + s2 + " ") || text.contains(s + " " + s2 + " "))) {
						for (String s3 : DBConfig.GOOD_WORDS) {
							if (text.toLowerCase().contains(s3)) {
								return false;
							}
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	public static String longToPlayerName2(long longValue) {
		if (longValue <= 0L || longValue >= 6582952005840035281L) {
			return null;
		}

		if (longValue % 37L == 0L) {
			return null;
		}

		int characterCount = 0;

		for (long val = longValue; val != 0L; val /= 37L) {
			characterCount++;
		}

		StringBuilder text = new StringBuilder(characterCount);
		while (longValue != 0L) {
			long var6 = longValue;
			longValue /= 37L;
			char character = playerNameXlateTable[(int) (var6 - longValue * 37L)];
			if (character == 95) {
				int previousCharIndex = text.length() - 1;
				text.setCharAt(previousCharIndex, Character.toUpperCase(text.charAt(previousCharIndex)));
				character = 32;
			}

			text.append(character);
		}

		text.reverse();
		return text.toString();
	}

	public static String optimizeText(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (i == 0) {
				s = String.format("%s%s", Character.toUpperCase(s.charAt(0)), s.substring(1));
			}
			if (!Character.isLetterOrDigit(s.charAt(i))) {
				if (i + 1 < s.length()) {
					s = String.format("%s%s%s", s.subSequence(0, i + 1), Character.toUpperCase(s.charAt(i + 1)),
							s.substring(i + 2));
				}
			}
		}
		return s;
	}

	public static long playerNameToInt64(String s) {
		long longValue = 0L;
		for (int i = 0, length = s.length(); i < length; i++) {
			char character = s.charAt(i);
			longValue *= 37L;
			if (character >= 'A' && character <= 'Z') {
				longValue += (1 + character) - 65;
			} else if (character >= 'a' && character <= 'z') {
				longValue += (1 + character) - 97;
			} else if (character >= '0' && character <= '9') {
				longValue += (27 + character) - 48;
			}

			if (longValue >= 177917621779460413L) {
				break;
			}
		}

		while (longValue % 37L == 0L && longValue != 0L) {
			longValue /= 37L;
		}

		return longValue;
	}

	public static void print(String str) {
		System.out.print(str);
	}

	public static void print_debug(String str) {
		System.out.print(str);
	}

	public static void println(String str) {
		if (DBConfig.PRINT_MISC) {
			System.out.println("[MISC]" + str);
		}
	}

	public static boolean isPointInCircle(double x, double y, double centerX, double centerY, double radius) {
		return Math.pow(x - centerX, 2) + Math.pow(y - centerY, 2) < Math.pow(radius, 2);
	}

	public static void println_debug(String str) {
		System.out.println(str);
	}
	
	public static int newRandom(int range) {
		if (range < 0)
			range = 0;
		return ThreadLocalRandom.current().nextInt(range + 1);//(int) (java.lang.Math.random() * (range + 1));
	}
	
	public static int newRandom(int min, int max) {
		if (max < 0)
			max = 0;
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	/**
	 * @param range
	 *            - range from 0 to specified range
	 * @return from 0 to specified range
	 */
	public static int random(int range) {
		return (int) (java.lang.Math.random() * (range + 1));
	}
	
	public static int randomNoPlus(int range) {
		return (int) (java.lang.Math.random() * range);
	}

    public static final int randomNoPlus(int min, int max) {
		final int n = Math.abs(max - min);
		return Math.min(min, max) + (n == 0 ? 0 : randomNoPlus(n));
    }

    public static final int random(int min, int max) {
		final int n = Math.abs(max - min);
		return Math.min(min, max) + (n == 0 ? 0 : random(n));
    }

	public static int random(int[] array) {
		return array[randomNoPlus(array.length)];
	}

	public static Location random(Location[] array) {
		return array[randomNoPlus(array.length)];
	}

	public static long random(long range) {
		return (long) (java.lang.Math.random() * (range + 1));
	}
	
	public static boolean randomBoolean(int chance) {
		return random(chance) == 0;
	}

	public static boolean randomBoolean() {
		return random(1) == 0;
	}

	public static <T> T random(List<T> list) {
		if (list == null || list.isEmpty()) return null;
		return list.get(randomNoPlus(list.size()));
	}

	// public static int direction(int srcX, int srcY, int x, int y) {
	// double dx = (double) x - srcX, dy = (double) y - srcY;
	// double angle = Math.atan(dy / dx);
	// angle = Math.toDegrees(angle);
	// if (Double.isNaN(angle))
	// return -1;
	// if (Math.signum(dx) < 0)
	// angle += 180.0;
	// return (int) ((((90 - angle) / 22.5) + 16) % 16);
	// }

	public static <T> T random(T[] array) {
		return array != null ? array[randomNoPlus(array.length)] : null;
	}

	public static <T> T random(T[][] twoDimensionalArray) {
		T[] oneDimensionalArray = twoDimensionalArray[randomNoPlus(twoDimensionalArray.length)];
		return oneDimensionalArray[randomNoPlus(oneDimensionalArray.length)];
	}

	/**
	 * @param range
	 *            - range of random number
	 * @return from 1 to specified range number
	 */
	public static int random2(int range) {
		return (int) ((java.lang.Math.random() * range) + 1); //return random(range) + 1;
	}

	public static double randomDouble(double min, double max) {
		return (Math.random() * (max - min) + min);
	}

	public static String removeSpaces(String s) {
		return s.replace(" ", "");
	}

	/**
	 * round to specific amount of decimal places
	 *
	 * @param value
	 * @param places
	 *            decimal places
	 * @return
	 */
	public static double round(double value, int places) {
		if (places < 0) {
			throw new IllegalArgumentException();
		}

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static int roundNearest(int number) {
		if (number == 0) {
			return 0;
		}
		int multiple = 0;

		if (number > 0) {
			multiple = number;
		}
		if (number > 1000) {
			multiple = 100;
		}
		if (number > 100000) {
			multiple = 1000;
		}
		if (number > 1000000) {
			multiple = 10000;
		}

		int result = multiple;

		// If not already multiple of given number

		if (number % multiple != 0) {

			int division = (number / multiple) + 1;

			result = division * multiple;

		}

		return result;

	}

	public static int roundNearest(int number, int multiple) {

		int result = multiple;

		// If not already multiple of given number

		if (number % multiple != 0) {

			int division = (number / multiple) + 1;

			result = division * multiple;

		}

		return result;

	}

	public static int secondsToTicks(int seconds) {
		return (seconds * 1000) / 600;
	}
	
	public static int serverToClientTick(int ticks) {
		return ticks * 30; //(ticks * 600) / 30;
	}
	
	public static int clientToServerTick(int ticks) {
		return ticks / 30; //(ticks * 600) / 30;
	}

	public static void textPack(byte packedData[], java.lang.String text) {
		if (text.length() > 80) {
			text = text.substring(0, 80);
		}
		text = text.toLowerCase();

		int carryOverNibble = -1;
		int ofs = 0;
		for (int idx = 0; idx < text.length(); idx++) {
			char c = text.charAt(idx);
			// int tableIdx = 0; //unused
			for (char element : xlateTable) {
				if (c == element) {
					// tableIdx = i; //unused
					break;
				}
			}
			packedData[ofs++] = (byte) (carryOverNibble);
		}
	}

	public static void textPackOld(byte packedData[], java.lang.String text) {
		if (text.length() > 80) {
			text = text.substring(0, 80);
		}
		text = text.toLowerCase();

		int carryOverNibble = -1;
		int ofs = 0;
		for (int idx = 0; idx < text.length(); idx++) {
			char c = text.charAt(idx);
			int tableIdx = 0;
			for (int i = 0; i < xlateTable.length; i++) {
				if (c == xlateTable[i]) {
					tableIdx = i;
					break;
				}
			}
			if (tableIdx > 12) {
				tableIdx += 195;
			}
			if (carryOverNibble == -1) {
				if (tableIdx < 13) {
					carryOverNibble = tableIdx;
				} else {
					packedData[ofs++] = (byte) (tableIdx);
				}
			} else if (tableIdx < 13) {
				packedData[ofs++] = (byte) ((carryOverNibble << 4) + tableIdx);
				carryOverNibble = -1;
			} else {
				packedData[ofs++] = (byte) ((carryOverNibble << 4) + (tableIdx >> 4));
				carryOverNibble = tableIdx & 0xf;
			}
		}

		if (carryOverNibble != -1) {
			packedData[ofs++] = (byte) (carryOverNibble << 4);
		}
	}

	public static String textUnpack(byte packedData[], int size) {
		int idx = 0; // highNibble = -1;
		for (int i = 0; i < size; i++) {
			int val = packedData[i];
			decodeBuf[idx++] = xlateTable[val];
		}
		return new String(decodeBuf, 0, idx);
	}

	public static String textUnpackOld(byte packedData[], int size) {
		int idx = 0, highNibble = -1;
		for (int i = 0; i < size * 2; i++) {
			int val = packedData[i / 2] >> (4 - 4 * (i % 2)) & 0xf;
			if (highNibble == -1) {
				if (val < 13) {
					decodeBuf[idx++] = xlateTable[val];
				} else {
					highNibble = val;
				}
			} else {
				decodeBuf[idx++] = xlateTable[((highNibble << 4) + val) - 195];
				highNibble = -1;
			}
		}

		return new String(decodeBuf, 0, idx);
	}

	public static double ticksIntoSeconds(int ticks) {
		return (ticks * 600d) / 1000d;
	}

	public static int ticksToSeconds(int ticks) {
		return ticks * 600 / 1000;
	}

	public static int minutesToTicks(int minutes) {
		return minutes * 100;
	}

	public static String ucFirst(String str) {
		str = str.toLowerCase();
		if (str.length() > 1) {
			str = str.substring(0, 1).toUpperCase() + str.substring(1);
		} else {
			return str.toUpperCase();
		}
		return str;
	}

	public static boolean validateEmailRegex(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	public static boolean xenLoginSuccessful(String email, String pass) {
		URL url;
		try {
			url = new URL(Config.FORUMS + "test.php?username=" + email + "&password=" + pass);

			BufferedReader in;
			in = new BufferedReader(new InputStreamReader(url.openStream()));

			String str = null;
			boolean ret = false;
			while ((str = in.readLine()) != null) {
				str = str.toLowerCase();
				if (str.contains("poop")) {
					ret = true;
					break;
				}
			}

			in.close();
			// System.out.println("str "+str);
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static Timestamp getESTTimestamp() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("EST"));
		return Timestamp.from(cal.toInstant());
	}

	public static int toBitpack(int x, int y) {
		return (x << 16) + y;
	}

	public static int getFirst(int bitpack) {
		return bitpack >> 16;
	}

	public static int getSecond(int bitpack) {
		return bitpack & 0xffff;
	}
	
	public static int setBit(int value, int bit) {
		return value | 1 << bit;
	}

	public static int clearBit(int value, int bit) {
		return value & ~(1 << bit);
	}

	public static boolean testBit(int value, int bit) {
		return (value & 1 << bit) != 0;
	}
	
	public static int addBits(int... bits) {
		int value = 0;
		for (int bit : bits) {
			value = setBit(value, bit);
		}

		return value;
	}

	private static final int[] crcTable = new int[256];
	private static final DecimalFormat FORMATTER;

	public static int getCrc32(byte[] buffer, int off, int length) {
		int crc = -1;
		for (int id = off; id < length; id++) {
			crc = crcTable[(crc ^ buffer[id]) & 0xff] ^ crc >>> 8;
		}

		crc ^= 0xffffffff;
		return crc;
	}

	static {
		DecimalFormatSymbols separator = new DecimalFormatSymbols();
		separator.setGroupingSeparator(',');
		FORMATTER = new DecimalFormat("#,###,###,###,###,###", separator);

		for (int index = 0; index < 256; index++) {
			int crc = index;
			for (int bit = 0; bit < 8; bit++) {
				if ((0x1 & crc) == 1) {
					crc = crc >>> 1 ^ ~0x12477cdf;
				} else {
					crc >>>= 1;
				}
			}
			crcTable[index] = crc;
		}
	}

	public static boolean isInsideRenderedArea(Player c, int oX, int oY) {
		final int x = c.getRenderedMapRegionX(oX);
		final int y = c.getRenderedMapRegionY(oY);
		return x >= 0 && x < 104 && y >= 0 && y < 104;
	}

	public static String formatName(final String name) {
		if (name.isEmpty()) {
			return name;
		}

		int length = name.length();
		StringBuilder sb = new StringBuilder(length);
		sb.append(Character.toUpperCase(name.charAt(0)));

		for (int index = 1; index < length; index++) {
			char character = name.charAt(index);

			if (character == '_' || character == ' ') {
				sb.append(character);

				int nextIndex = index + 1;
				if (nextIndex < length) {
					character = name.charAt(nextIndex);
					if (character >= 'a' && character <= 'z') {
						sb.append(Character.toUpperCase(character));
						index++;
					}
				}
			} else {
				sb.append(character);
			}
		}

		return sb.toString();
	}

	public static int addOrMaxInt(int value1, int value2) {
		try {
			return Math.addExact(value1, value2);
		} catch (ArithmeticException e) {
			return Integer.MAX_VALUE;
		}
	}
	
	public static int multiplyOrMaxInt(int value1, int value2) {
		try {
			return Math.multiplyExact(value1, value2);
		} catch (ArithmeticException e) {
			return Integer.MAX_VALUE;
		}
	}

	public static byte[] readFile(String path) {
		File file = new File(path);
		if (!file.exists()) {
			return null;
		}

		byte[] buffer = new byte[(int) file.length()];
		try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
			raf.readFully(buffer);
		} catch (Exception e) {
			return null;
		}

		return buffer;
	}

	private static byte[] gzipInputBuffer = new byte[999999];
	private static GzipDecompressor gzipDecompressor = new GzipDecompressor();

	public static byte[] readFileGzip(String path) {
		byte[] buffer = readFile(path);
		if (buffer == null) {
			return null;
		}

		int bufferlength = gzipDecompressor.decompress(buffer, gzipInputBuffer);
		byte[] inflated = new byte[bufferlength];
		System.arraycopy(gzipInputBuffer, 0, inflated, 0, bufferlength);
		if (inflated.length < 10) {
			return null;
		}

		return inflated;
	}

}
