package com.soulplay.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.util.log.Logger;

/**
 * This class modifies previous {@link System#out} logging and prints them with
 * information. We need to know which class printed data and at what time at all
 * times. - Thommas Napolian
 */
public final class OutLogger extends PrintStream {

	public OutLogger(OutputStream out) {
		super(out);
	}

	/**
	 * Gets the date in a formatted string.
	 *
	 * @return The date
	 */
	private String getFormattedDate() {
		return new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new Date());
	}

	/**
	 * Logging information to the super class after we have formatted it
	 *
	 * @param className
	 *            The name of the class the data is from
	 * @param text
	 *            The text that was originally printed
	 */
	private void log(String className, String text) {
		String message = "[" + className + "] " + text;

		super.print(message);

		if (message.contains("ArrayIndexOutOfBoundsException")
				|| message.contains("Null")) {
			Thread.dumpStack();
		}

		if (!Config.SERVER_DEBUG && Server.getSystemLogs()
				&& !message.contains("[MOTIVOTE]")
				&& !message.contains("motivote")
				&& !message.contains("[MISC]")) {
			try {
				Logger.writeFile1("[" + getFormattedDate() + "]: " + message);
			} catch (IOException e) {
				Server.toggleSystemLogs();
				e.printStackTrace();
			}
		}
	}

	@Override
	public void print(boolean message) {
		Throwable throwable = new Throwable();
		if (throwable.getStackTrace() == null
				|| throwable.getStackTrace()[2] == null
				|| throwable.getStackTrace()[2].getFileName() == null) {
			return;
		}
		String name = throwable.getStackTrace()[2].getFileName()
				.replace(".java", "");
		String line = String
				.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(double message) {
		Throwable throwable = new Throwable();
		String name = throwable.getStackTrace()[2].getFileName()
				.replace(".java", "");
		String line = String
				.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(int message) {
		Throwable throwable = new Throwable();
		if (throwable.getStackTrace() == null
				|| throwable.getStackTrace()[2] == null
				|| throwable.getStackTrace()[2].getFileName() == null) {
			return;
		}
		String name = throwable.getStackTrace()[2].getFileName()
				.replace(".java", "");
		String line = String
				.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(long message) {
		Throwable throwable = new Throwable();
		if (throwable.getStackTrace() == null
				|| throwable.getStackTrace()[2] == null
				|| throwable.getStackTrace()[2].getFileName() == null) {
			return;
		}
		String name = throwable.getStackTrace()[2].getFileName()
				.replace(".java", "");
		String line = String
				.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(Object message) {
		Throwable throwable = new Throwable();
		if (throwable.getStackTrace() == null
				|| throwable.getStackTrace()[2] == null
				|| throwable.getStackTrace()[2].getFileName() == null) {
			return;
		}
		String name = throwable.getStackTrace()[2].getFileName()
				.replace(".java", "");
		String line = String
				.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(String message) {
		Throwable throwable = new Throwable();
		if (throwable.getStackTrace() == null
				|| throwable.getStackTrace()[2] == null
				|| throwable.getStackTrace()[2].getFileName() == null) {
			return;
		}
		String name = throwable.getStackTrace()[2].getFileName()
				.replace(".java", "");
		String line = String
				.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

}
