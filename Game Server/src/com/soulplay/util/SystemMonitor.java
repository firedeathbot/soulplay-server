package com.soulplay.util;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.Uptime;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.net.slack.SlackMessageBuilder;

public class SystemMonitor {

	double processCpuLoadWarning = 0.80;

	double cpuLoadWarning = 0.90;

	double ramUsageWarning = 80.00;

	double usedMem = 0D;

	boolean memWarning = false;

	boolean cpuWarning = false;

	String status = "";

	String technicalStatus = "";

	int statusInt = 0;

	Sigar sigar = new Sigar();

	public SystemMonitor() throws InterruptedException {
		AtomicBoolean stop = new AtomicBoolean(false);
		Executors.newSingleThreadExecutor().execute(() -> {
			Thread.currentThread().setName("system-monitor-thread");
			while (!stop.get() && !Config.SERVER_DEBUG) {
				try {
					Thread.sleep(Config.CYCLE_TIME);
					processSystemStats();
				} catch (Exception e) {
					Server.getSlackApi().call(SlackMessageBuilder
							.buildExceptionMessage(SystemMonitor.class, e));
					// Server.getSlackSession().sendMessage(channel,
					// SlackMessageBuilder.buildExceptionMessage(location, e))
				}
			}
		});
	}

	public String getStatus() {
		return status;
	}

	public String getStatusColor() {
		switch (statusInt) {
			case 0:
				return "00FF00";
			case 1:
				return "FF9900";
			case 2:
			case 3:
				return "FF0000";
		}
		return "00FF00";
	}

	public String getSystemStats() {
		StringBuilder sb = new StringBuilder();
		try {
			Mem mem = sigar.getMem();
			CpuPerc cpuPerc = sigar.getCpuPerc();
			Uptime time = new Uptime();
			time.gather(sigar);

			sb.append(" Mem: " + Math.round(mem.getUsedPercent()));
			sb.append(" CPU: " + Math.round(cpuPerc.getCombined() * 100));
			sb.append(" Uptime: " + String.format("%d:%02d ",
					TimeUnit.SECONDS.toHours((int) time.getUptime()),
					TimeUnit.SECONDS.toMinutes((int) time.getUptime())
							- TimeUnit.HOURS.toMinutes((TimeUnit.SECONDS
									.toHours((int) time.getUptime())))));
		} catch (Exception e) {

		}
		return sb.toString();
	}

	private void processSystemStats() throws SigarException {
		Mem mem = sigar.getMem();
		usedMem = mem.getUsedPercent();

		// if (usedMem > ramUsageWarning) {
		// setMemWarning(true);
		// memtop++;
		// } else {
		// if (memtop > 1) {
		// memtop = 0;
		// setMemWarning(true);
		// }
		// memtop++;
		// }
		//
		// if (usedCpu > cpuLoadWarning) {
		// setCpuWarning(true);
		// }
	}
}
