package com.soulplay.util.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.soulplay.Config;
import com.soulplay.DBConfig;
import com.soulplay.content.minigames.duel.Stake;
import com.soulplay.content.player.combat.Kill;
import com.soulplay.content.player.commands.CommandLogEntry;
import com.soulplay.content.player.trading.TradingPostOrder;
import com.soulplay.content.player.trading.Trade;
import com.soulplay.content.shops.Buy;
import com.soulplay.content.shops.BuyPOS;
import com.soulplay.content.shops.Sell;
import com.soulplay.game.model.item.DroppedItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.PickedUpItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.wrapper.Empty;
import com.soulplay.net.login.Login;
import com.soulplay.util.Misc;
import com.soulplay.util.log.NameChangeLog;
import com.zaxxer.hikari.HikariDataSource;

public class LogHandler {

	static HikariDataSource hikari;
	
	public static HikariDataSource getLogHikari() {
		return hikari;
	}

	public static void logAlch(int mysqlIndex, int item) {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();
			ps = connection.prepareStatement(
					"INSERT INTO alch_log (playerMID, item, item_name, world) VALUES(?, ?, ?, ?)");
			ps.setInt(1, mysqlIndex); // ps.setString(1, playerName);
			ps.setInt(2, item);
			ps.setString(3, ItemProjectInsanity.getItemName(item));
			ps.setInt(4, Config.NODE_ID);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logBuy(Buy buy) {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();
			ps = connection.prepareStatement(
					"INSERT INTO buy_log (buyer, mid, item, item_name, amount, price, shop, world) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, buy.getBuyer());
			ps.setInt(2, buy.buyerMID());
			ps.setInt(3, buy.getItem().getId());
			ps.setString(4, ItemProjectInsanity.getItemName(buy.getItem().getId()));
			ps.setInt(5, buy.getItem().getAmount());
			ps.setLong(6, buy.getPrice());
			ps.setInt(7, buy.getShop());
			ps.setInt(8, Config.NODE_ID);
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logBuyPOS(BuyPOS buyPOS) {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO buy_pos_log (buyer, buyerMID, seller, sellerMID, item, item_name, amount, price, shop, world) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, buyPOS.getBuyer());
			ps.setInt(2, buyPOS.buyerMID());
			ps.setString(3, buyPOS.getSeller());
			ps.setInt(4, buyPOS.getSellerMID());
			ps.setInt(5, buyPOS.getItem().getId());
			ps.setString(6, ItemProjectInsanity.getItemName(buyPOS.getItem().getId()));
			ps.setInt(7, buyPOS.getItem().getAmount());
			ps.setLong(8, buyPOS.getPrice());
			ps.setInt(9, buyPOS.getShop());
			ps.setInt(10, Config.NODE_ID);
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void logTradingPostSale(TradingPostOrder order) {

		try (Connection con = hikari.getConnection();
				PreparedStatement ps = con.prepareStatement(
						"INSERT INTO trading_post_log (buyer_name, buyer_id, item_name, item_id, item_amount, price_per_item, seller_name, seller_id, trade_type, world, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");) {
			ps.setString(1, order.getBuyerName());
			ps.setInt(2, order.getBuyerMID());
			ps.setString(3, order.getItemName());
			ps.setInt(4, order.getItem().getId());
			ps.setInt(5, order.getItem().getAmount());
			ps.setLong(6, order.getPricePerItem());
			ps.setString(7, order.getSellerName());
			ps.setInt(8, order.getSellerMID());
			ps.setLong(9, order.getTradeType());
			ps.setInt(10, Config.NODE_ID);
			ps.setTimestamp(11, order.getTimestamp());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void logCommand(CommandLogEntry command) {
		if (!DBConfig.LOG_COMMANDS) {
			return;
		}
		if (command.getCommand().startsWith("/")
				&& !command.getCommand().startsWith("//")) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO command_log (player, mid, command, world, x, y, z) VALUES(?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, command.getPlayerName());
			ps.setInt(2, command.getPlayerId());
			ps.setString(3, command.getCommand());
			ps.setInt(4, Config.NODE_ID);
			ps.setInt(5, command.getLoc().getX());
			ps.setInt(6, command.getLoc().getY());
			ps.setInt(7, command.getLoc().getZ());
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logDrop(DroppedItem item) {
		if (!DBConfig.LOG_DROP) {
			return;
		}
		if (item == null) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO drop_log (playername, playerMID, itemName, itemID, itemAmount, bobdrop, world) VALUES(?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, item.getDropper());
			ps.setInt(2, item.getDropperMID());
			ps.setString(3, ItemConstants.getItemName(item.getItem().getId()));
			ps.setInt(4, item.getItem().getId());
			ps.setInt(5, item.getItem().getAmount());
			ps.setString(6, item.getCause());
			ps.setInt(7, Config.NODE_ID);
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void logDrop(List<DroppedItem> droppedItems) {
		if (!DBConfig.LOG_DROP) {
			return;
		}
		if (droppedItems.isEmpty()) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO drop_log (playername, playerMID, itemName, itemID, itemAmount, bobdrop, world) VALUES(?, ?, ?, ?, ?, ?, ?)");
			for (DroppedItem item : droppedItems) {
				ps.setString(1, item.getDropper());
				ps.setInt(2, item.getDropperMID());
				ps.setString(3,
						ItemConstants.getItemName(item.getItem().getId()));
				ps.setInt(4, item.getItem().getId());
				ps.setInt(5, item.getItem().getAmount());
				ps.setString(6, item.getCause());
				ps.setInt(7, Config.NODE_ID);
				ps.executeUpdate();
			}
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void logEmpty(Empty empty) {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = hikari.getConnection();
			ps = connection.prepareStatement(
					"INSERT INTO empty_log (username, mid, inventory_items, inventory_amounts, ip, uuid, world) VALUES(?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, empty.getPlayer());
			ps.setInt(2, empty.getMysqlIndex());
			StringBuilder items = new StringBuilder();
			for (int i : empty.getPlayerItems()) {
				items.append(i);
				items.append(";");
			}
			StringBuilder amounts = new StringBuilder();
			for (int i : empty.getPlayerItemsN()) {
				amounts.append(i);
				amounts.append(";");
			}
			ps.setString(3, items.toString());
			ps.setString(4, amounts.toString());
			ps.setString(5, empty.getConnectedFrom());
			ps.setString(6, empty.getUUID());
			ps.setInt(7, Config.NODE_ID);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logKill(Kill kill) {
		if (!DBConfig.LOG_KILL) {
			return;
		}
		if (kill == null) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();
			ps = connection.prepareStatement(
					"INSERT INTO kill_log (killer, killerMID, victim, victimMID, killerIP, victimIP, world) VALUES(?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, kill.getKillerName());
			ps.setInt(2, kill.getKillerMID());
			ps.setString(3, kill.getVictimName());
			ps.setInt(4, kill.getVictimMID());
			ps.setString(5, kill.getKillerIP());
			ps.setString(6, kill.getVictimIP());
			ps.setInt(7, Config.NODE_ID);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logLogin(Login login) {
		if (!DBConfig.LOG_LOGIN) {
			return;
		}
		if (login == null) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();
			ps = connection.prepareStatement(
					"INSERT INTO login_log (username, mid, ip, uuid, world) VALUES(?, ?, ?, ?, ?)");
			ps.setString(1, login.getUsername());
			ps.setInt(2, login.getMysqlIndex());
			ps.setString(3, login.getIP());
			ps.setString(4, login.getUUID());
			ps.setInt(5, Config.NODE_ID);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logPickup(PickedUpItem pui) {
		if (!DBConfig.LOG_PICKUP) {
			return;
		}
		if (pui == null) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO pickup_log (playername, playerMID, itemName, itemID, itemAmount, itemOwner, ownerMID, world) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, pui.getClientName());
			ps.setInt(2, pui.getPickerMID());
			ps.setString(3,
					ItemConstants.getItemName(pui.getItem().getItemId()));
			ps.setInt(4, pui.getItem().getItemId());
			ps.setInt(5, pui.getItem().getItemAmount());
			ps.setString(6, pui.getItem().getOriginalOwnerName());
			ps.setInt(7, pui.getOriginalOwnerMID());
			ps.setInt(8, Config.NODE_ID);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logSell(Sell sell) {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();
			ps = connection.prepareStatement(
					"INSERT INTO sell_log (seller, sellerMID, item, item_name, amount, price, shop, world) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, sell.getSeller());
			ps.setInt(2, sell.getMysqlIndex());
			ps.setInt(3, sell.getItem().getId());
			ps.setString(4, ItemProjectInsanity.getItemName(sell.getItem().getId()));
			ps.setInt(5, sell.getItem().getAmount());
			ps.setInt(6, sell.getPrice());
			ps.setInt(7, sell.getShop());
			ps.setInt(8, Config.NODE_ID);
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void logNameChange(NameChangeLog log) {
		
		try(Connection connection = hikari.getConnection();
				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO name_change_log (player_id, username, prev_name, new_name, timestamp) VALUES(?, ?, ?, ?, ?)");) {
			ps.setInt(1, log.getMysqlId());
			ps.setString(2, log.getUsername());
			ps.setString(3, log.getPrevDisplayName());
			ps.setString(4, log.getNewDisplayName());
			ps.setTimestamp(5, log.getTimestamp());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void logStaffCommand(Client c, Client o, String command) {
		if (!DBConfig.LOG_STAFF) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO log (player, other, otherIP, command, inWildy, otherInWildy, inCombat, otherInCombat, otherInDung, playersNearby) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, c.getName());
			if (o != null) {
				ps.setString(2, o.getName());
			} else {
				ps.setString(2, "nothing");
			}
			if (o != null) {
				ps.setString(3, o.connectedFrom);
			} else {
				ps.setString(3, "nothing");
			}
			ps.setString(4, command);
			ps.setString(5, Boolean.toString(c.inWild()));
			if (o != null) {
				ps.setString(6, Boolean.toString(o.inWild()));
			} else {
				ps.setString(6, "nothing");
			}
			ps.setLong(7, c.underAttackBy);
			if (o != null) {
				ps.setLong(8, o.underAttackBy);
			} else {
				ps.setString(8, "nothing");
			}
			if (o != null) {
				ps.setBoolean(9, o.insideDungeoneering());
			} else {
				ps.setString(9, "nothing");
			}
			StringBuilder sb = new StringBuilder();
			// for (Player p : PlayerHandler.players) {
			// if (p.inArea(c.getX()-50, c.getY()-50, c.getX()+50,c.getY()+50))
			// {
			// sb.append(p.playerName);
			// sb.append(";");
			// }
			// }
			ps.setString(10, sb.toString());
			// System.out.println(ps);
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logStake(List<Stake> stakeList) {
		if (!DBConfig.LOG_STAKE) {
			return;
		}
		if (stakeList.isEmpty()) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();
			Timestamp ts = Misc.getESTTimestamp(); //Timestamp.from(Instant.now());
			ps = connection.prepareStatement(
					"INSERT INTO stake_log (giver, giverMID, recipient, recipientMID, itemName, itemID, itemAmount, world, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (Stake stake : stakeList) {
				ps.setString(1, stake.getLoser());
				ps.setInt(2, stake.getLoserMID());
				ps.setString(3, stake.getWinner());
				ps.setInt(4, stake.getWinnerMID());
				ps.setString(5,
						ItemConstants.getItemName(stake.getItem().getId())
								+ " (" + stake.getItemOwner() + ")");
				ps.setInt(6, stake.getItem().getId());
				ps.setInt(7, stake.getItem().getAmount());
				ps.setInt(8, Config.NODE_ID);
				ps.setTimestamp(9, ts);
				ps.executeUpdate();
			}
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logTrade(List<Trade> tradeList) {
		if (!DBConfig.LOG_TRADE) {
			return;
		}
		if (tradeList.isEmpty()) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			Timestamp ts = Misc.getESTTimestamp();//Timestamp.from(Calendar.getInstance().toInstant());
			ps = connection.prepareStatement(
					"INSERT INTO trade_log (giver, giverMID, recipient, recipientMID, itemName, itemID, itemAmount, world, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (Trade trade : tradeList) {
				ps.setString(1, trade.getGiver());
				ps.setInt(2, trade.getGiverMID());
				ps.setString(3, trade.getRecipient());
				ps.setInt(4, trade.getRecipientMID());
				ps.setString(5,
						ItemConstants.getItemName(trade.getItem().getId()));
				ps.setInt(6, trade.getItem().getId());
				ps.setInt(7, trade.getItem().getAmount());
				ps.setInt(8, Config.NODE_ID);
				ps.setTimestamp(9, ts);
				ps.executeUpdate();
			}
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void logTrade2(List<Trade> tradeList) {
		if (!DBConfig.LOG_TRADE) {
			return;
		}
		if (tradeList.isEmpty()) {
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = hikari.getConnection();

			ps = connection.prepareStatement(
					"INSERT INTO trade2_log (trader1, trader1MID, trader2, trader2MID, given1, given2, world) VALUES(?, ?, ?, ?, ?, ?, ?)");
			String trader1 = tradeList.get(0).getGiver();
			int trader1MID = tradeList.get(0).getGiverMID();
			String trader2 = tradeList.get(0).getRecipient();
			int trader2MID = tradeList.get(0).getRecipientMID();
			StringBuilder given1 = new StringBuilder();
			StringBuilder given2 = new StringBuilder();
			for (Trade trade : tradeList) {
				if (trader1.equals(trade.getGiver())) {
					given1.append(trade.getItem().getId());
					given1.append(":");
					given1.append(trade.getItem().getAmount());
					given1.append(";");
				} else if (trader2.equals(trade.getGiver())) {
					given2.append(trade.getItem().getId());
					given2.append(":");
					given2.append(trade.getItem().getAmount());
					given2.append(";");
				}
			}

			ps.setString(1, trader1);
			ps.setInt(2, trader1MID);
			ps.setString(3, trader2);
			ps.setInt(4, trader2MID);
			ps.setString(5, given1.toString());
			ps.setString(6, given2.toString());
			ps.setInt(7, Config.NODE_ID);
			ps.executeUpdate();
			// ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public LogHandler() {
		System.out.println("Starting LogHandler...");
		hikari = new HikariDataSource();
		hikari.setMaximumPoolSize(10);
		hikari.setDataSourceClassName(
				"com.mysql.cj.jdbc.MysqlDataSource");
		hikari.addDataSourceProperty("serverName", Config.LOG_SQL_HOST);
		hikari.addDataSourceProperty("port", 3306);
		hikari.addDataSourceProperty("databaseName", Config.LOG_SQL_DATABASE);
		hikari.addDataSourceProperty("user", Config.LOG_SQL_USERNAME);
		hikari.addDataSourceProperty("password", Config.LOG_SQL_PASSWORD);
	}
}
