package com.soulplay.util.sql.payment;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.HttpPost;
import com.soulplay.util.Misc;

public class ClaimWheelReward {

	public static void claimWheel(final Client c, boolean usingCommand) {
		TaskExecutor.executeHttp(() -> {

			try {

				final String name = c.getName().toLowerCase().replace(" ",
						"_");

				int rewardIdNotFinal = 0;

				String resultString = HttpPost.getHttpResponseString(
						"http://www.soulplayps.com/excludecf/claim/claimwheel.php?player="
								+ name);

				if (resultString == null || resultString.equals("no")) {
					if (usingCommand) {
						PlayerHandler.sendMessageMainThread(c,
								"There are no more rewards to claim.");
						c.startedCheckSpins = false;
					}
					return;
				}

				rewardIdNotFinal = Integer.parseInt(resultString);

				final int rewardId = rewardIdNotFinal;

				Server.getTaskScheduler().schedule(new Task() {

					@Override
					protected void execute() {

						if (c == null || !c.isActive) {
							Server.getSlackApi().call(SlackMessageBuilder
									.buildDonationClaimErrorMessage(
											Config.SERVER_NAME,
											"Name:" + name + " wheelRewardId:"
													+ rewardId,
											"#developers"));
							this.stop();
							return;
						}

						int itemId = 0;
						int itemAmount = 0;

						// add items here
						// Package 1
						if (rewardId == 1) {
							itemId = 995; // gold
							itemAmount = 100000 + Misc.random(2000000); // 10
																		// million
																		// biggest
																		// prize

							// Package 2
						} else if (rewardId == 2) {
							itemId = 861; // magic shortbow
							itemAmount = 1;
						} else if (rewardId == 3) {
							itemId = 859; // magic longbow
							itemAmount = 1;
						} else if (rewardId == 4) {
							itemId = 3749; // archer helm
							itemAmount = 1;
						} else if (rewardId == 5) {
							itemId = 3751; // berserker helm
							itemAmount = 1;
						} else if (rewardId == 6) {
							itemId = 882; // bronze arrow
							itemAmount = 400;
						} else if (rewardId == 7) {
							itemId = 884; // iron arrow
							itemAmount = 200;
						} else if (rewardId == 8) {
							itemId = 886; // steel arrow
							itemAmount = 90;
						} else if (rewardId == 9) {
							itemId = 888; // mithril arrow
							itemAmount = 50;
						} else if (rewardId == 10) {
							itemId = 890; // adamant arrow
							itemAmount = 30;
						} else if (rewardId == 11) {
							itemId = 892; // rune arrow
							itemAmount = 20;
						} else if (rewardId == 12) {
							itemId = 1713; // amulet of glory (4)
							itemAmount = 1;
						} else if (rewardId == 13) {
							itemId = 841; // shortbow
							itemAmount = 1;
						} else if (rewardId == 14) {
							itemId = 843; // oak shortbow
							itemAmount = 1;
						} else if (rewardId == 15) {
							itemId = 849; // willow shortbow
							itemAmount = 1;
						} else if (rewardId == 16) {
							itemId = 853; // maple shortbow
							itemAmount = 1;
						} else if (rewardId == 17) {
							itemId = 9185; // rune crossbow
							itemAmount = 1;
						} else if (rewardId == 18) {
							itemId = 1135; // green dhide body
							itemAmount = 1;
						} else if (rewardId == 19) {
							itemId = 2499; // blue dhide body
							itemAmount = 1;
						} else if (rewardId == 20) {
							itemId = 2501; // red dhide body
							itemAmount = 1;
						} else if (rewardId == 21) {
							itemId = 1099; // green dhide chaps
							itemAmount = 1;
						} else if (rewardId == 22) {
							itemId = 2493; // blue dhide chaps
							itemAmount = 1;
						} else if (rewardId == 23) {
							itemId = 2495; // red dhide chaps
							itemAmount = 1;
						} else if (rewardId == 24) {
							itemId = 1065; // green dhide vambraces
							itemAmount = 1;
						} else if (rewardId == 25) {
							itemId = 2487; // blue dhide vambraces
							itemAmount = 1;
						} else if (rewardId == 26) {
							itemId = 2489; // red dhide vambraces
							itemAmount = 1;
						} else if (rewardId == 27) {
							itemId = 6322; // snakeskin body
							itemAmount = 1;
						} else if (rewardId == 28) {
							itemId = 6324; // snakeskin chaps
							itemAmount = 1;
						} else if (rewardId == 29) {
							itemId = 6328; // snakeskin boots
							itemAmount = 1;
						} else if (rewardId == 30) {
							itemId = 1129; // leather body
							itemAmount = 1;
						} else if (rewardId == 31) {
							itemId = 1133; // studded body
							itemAmount = 1;
						} else if (rewardId == 32) {
							itemId = 4334; // team 10 cape
							itemAmount = 1;
						} else if (rewardId == 33) {
							itemId = 4354; // team 20 cape
							itemAmount = 1;
						} else if (rewardId == 34) {
							itemId = 4326; // team 6 cape
							itemAmount = 1;
						} else if (rewardId == 35) {
							itemId = 4374; // team 30 cape
							itemAmount = 1;
						} else if (rewardId == 36) {
							itemId = 4393; // team 40 cape
							itemAmount = 1;
						} else if (rewardId == 37) {
							itemId = 6685; // sara brew (4)
							itemAmount = 1;
						} else if (rewardId == 38) {
							itemId = 6687; // sara brew [3]
							itemAmount = 1;
						} else if (rewardId == 39) {
							itemId = 6689; // sara brew [2]
							itemAmount = 1;
						} else if (rewardId == 40) {
							itemId = 6691; // sara brew [1]
							itemAmount = 1;
						} else if (rewardId == 41) {
							itemId = 2444; // ranging potion [4]
							itemAmount = 1;
						} else if (rewardId == 42) {
							itemId = 169; // ranging potion [3]
							itemAmount = 1;
						} else if (rewardId == 43) {
							itemId = 171; // ranging potion [2]
							itemAmount = 1;
						} else if (rewardId == 44) {
							itemId = 173; // ranging potion [1]
							itemAmount = 1;
						} else if (rewardId == 45) {
							itemId = 386; // shark
							itemAmount = 20;
						} else if (rewardId == 46) {
							itemId = 380; // lobster
							itemAmount = 40;
						} else if (rewardId == 47) {
							itemId = 392; // monkfish
							itemAmount = 15;
						} else if (rewardId == 48) {
							itemId = 2550; // ring of recoil
							itemAmount = 1;
						} else if (rewardId == 49) {
							itemId = 2552; // ring of dueling [8]
							itemAmount = 1;
						} else if (rewardId == 50) {
							itemId = 1203; // iron dagger
							itemAmount = 1;
						} else if (rewardId == 51) {
							itemId = 3192; // iron halberd
							itemAmount = 1;
						} else if (rewardId == 52) {
							itemId = 15298; // iron hatchet
							itemAmount = 1;
						} else if (rewardId == 53) {
							itemId = 1293; // iron longsword
							itemAmount = 1;
						} else if (rewardId == 54) {
							itemId = 1420; // iron mace
							itemAmount = 1;
						} else if (rewardId == 55) {
							itemId = 1323; // iron scimitar
							itemAmount = 1;
						} else if (rewardId == 56) {
							itemId = 1239; // iron spear
							itemAmount = 1;
						} else if (rewardId == 57) {
							itemId = 1279; // iron sword
							itemAmount = 1;
						} else if (rewardId == 58) {
							itemId = 1309; // iron 2h sword
							itemAmount = 1;
						} else if (rewardId == 59) {
							itemId = 1335; // iron warhammer
							itemAmount = 1;
						} else if (rewardId == 60) {
							itemId = 1363; // iron battleaxe
							itemAmount = 1;
						} else if (rewardId == 61) {
							itemId = 9177; // iron crossbow
							itemAmount = 1;
						} else if (rewardId == 62) {
							itemId = 1153; // iron full helm
							itemAmount = 1;
						} else if (rewardId == 63) {
							itemId = 1101; // iron chainbody
							itemAmount = 1;
						} else if (rewardId == 64) {
							itemId = 1115; // iron chainbody
							itemAmount = 1;
						} else if (rewardId == 65) {
							itemId = 1067; // iron platebody
							itemAmount = 1;
						} else if (rewardId == 66) {
							itemId = 1067; // iron platelegs
							itemAmount = 1;
						} else if (rewardId == 67) {
							itemId = 1081; // iron plateskirt
							itemAmount = 1;
						} else if (rewardId == 68) {
							itemId = 1175; // iron sq shield
							itemAmount = 1;
						} else if (rewardId == 69) {
							itemId = 1191; // iron kiteshield
							itemAmount = 1;
						} else if (rewardId == 70) {
							itemId = 12988; // iron gauntlets
							itemAmount = 1;
						} else if (rewardId == 71) {
							itemId = 4121; // iron boots
							itemAmount = 1;
						} else if (rewardId == 72) {
							itemId = 5574; // initiate sallet
							itemAmount = 1;
						} else if (rewardId == 73) {
							itemId = 5575; // initiate hauberk
							itemAmount = 1;
						} else if (rewardId == 74) {
							itemId = 5576; // initiate cuisse
							itemAmount = 1;
						} else if (rewardId == 75) {
							itemId = 1073; // adamant platelegs
							itemAmount = 1;
						} else if (rewardId == 76) {
							itemId = 1123; // adamant platebody
							itemAmount = 1;
						} else if (rewardId == 77) {
							itemId = 1161; // adamant full helm
							itemAmount = 1;
						} else if (rewardId == 78) {
							itemId = 1199; // adamant kiteshield
							itemAmount = 1;
						} else if (rewardId == 79) {
							itemId = 1317; // adamant 2h
							itemAmount = 1;
						} else if (rewardId == 80) {
							itemId = 1331; // adamant scimitar
							itemAmount = 1;
						} else if (rewardId == 81) {
							itemId = 1301; // adamant longsword
							itemAmount = 1;
						} else if (rewardId == 82) {
							itemId = 4129; // adamant boots
							itemAmount = 1;
						} else if (rewardId == 83) {
							itemId = 1207; // steel dagger
							itemAmount = 1;
						} else if (rewardId == 84) {
							itemId = 3194; // steel halberd
							itemAmount = 1;
						} else if (rewardId == 85) {
							itemId = 1295; // steel longsword
							itemAmount = 1;
						} else if (rewardId == 86) {
							itemId = 1424; // steel steel mace
							itemAmount = 1;
						} else if (rewardId == 87) {
							itemId = 1269; // steel pickaxe
							itemAmount = 1;
						} else if (rewardId == 88) {
							itemId = 1325; // steel scimitar
							itemAmount = 1;
						} else if (rewardId == 89) {
							itemId = 1241; // steel spear
							itemAmount = 1;
						} else if (rewardId == 90) {
							itemId = 1281; // steel sword
							itemAmount = 1;
						} else if (rewardId == 91) {
							itemId = 1311; // steel 2h sword
							itemAmount = 1;
						} else if (rewardId == 92) {
							itemId = 1339; // steel warhammer
							itemAmount = 1;
						} else if (rewardId == 93) {
							itemId = 1365; // steel battleaxe
							itemAmount = 1;
						} else if (rewardId == 94) {
							itemId = 9179; // steel crossbow
							itemAmount = 1;
						} else if (rewardId == 95) {
							itemId = 1157; // steel full helm
							itemAmount = 1;
						} else if (rewardId == 96) {
							itemId = 1105; // steel chainbody
							itemAmount = 1;
						} else if (rewardId == 97) {
							itemId = 1119; // steel platebody
							itemAmount = 1;
						} else if (rewardId == 98) {
							itemId = 1069; // steel platelegs
							itemAmount = 1;
						} else if (rewardId == 99) {
							itemId = 1083; // steel plateskirt
							itemAmount = 1;
						} else if (rewardId == 100) {
							itemId = 1193; // steel kiteshield
							itemAmount = 1;
						} else if (rewardId == 101) {
							itemId = 778; // steel gauntlets
							itemAmount = 1;
						} else if (rewardId == 102) {
							itemId = 4123; // steel boots
							itemAmount = 1;
						} else if (rewardId == 103) {
							itemId = 1217; // black dagger
							itemAmount = 1;
						} else if (rewardId == 104) {
							itemId = 3196; // black halberd
							itemAmount = 1;
						} else if (rewardId == 105) {
							itemId = 1297; // black longsword
							itemAmount = 1;
						} else if (rewardId == 106) {
							itemId = 1426; // black mace
							itemAmount = 1;
						} else if (rewardId == 107) {
							itemId = 1327; // black scimitar
							itemAmount = 1;
						} else if (rewardId == 108) {
							itemId = 4580; // black spear
							itemAmount = 1;
						} else if (rewardId == 109) {
							itemId = 1283; // black sword
							itemAmount = 1;
						} else if (rewardId == 110) {
							itemId = 1313; // black 2h sword
							itemAmount = 1;
						} else if (rewardId == 111) {
							itemId = 1341; // black warhammer
							itemAmount = 1;
						} else if (rewardId == 112) {
							itemId = 1367; // black battleaxe
							itemAmount = 1;
						} else if (rewardId == 113) {
							itemId = 13081; // black crossbow
							itemAmount = 1;
						} else if (rewardId == 114) {
							itemId = 1165; // black full helm
							itemAmount = 1;
						} else if (rewardId == 115) {
							itemId = 1107; // black chainbody
							itemAmount = 1;
						} else if (rewardId == 116) {
							itemId = 1125; // black platebody
							itemAmount = 1;
						} else if (rewardId == 117) {
							itemId = 1077; // black platelegs
							itemAmount = 1;
						} else if (rewardId == 118) {
							itemId = 1089; // black plateskirt
							itemAmount = 1;
						} else if (rewardId == 119) {
							itemId = 1179; // black sq shield
							itemAmount = 1;
						} else if (rewardId == 120) {
							itemId = 1195; // black kiteshield
							itemAmount = 1;
						} else if (rewardId == 121) {
							itemId = 12994; // black gauntlets
							itemAmount = 1;
						} else if (rewardId == 122) {
							itemId = 4125; // black boots
							itemAmount = 1;
						} else if (rewardId == 123) {
							itemId = 1209; // mithril dagger
							itemAmount = 1;
						} else if (rewardId == 124) {
							itemId = 3198; // mithril halberd
							itemAmount = 1;
						} else if (rewardId == 125) {
							itemId = 1299; // mithril longsword
							itemAmount = 1;
						} else if (rewardId == 126) {
							itemId = 1428; // mithril mace
							itemAmount = 1;
						} else if (rewardId == 127) {
							itemId = 1373; // mithril pickaxe
							itemAmount = 1;
						} else if (rewardId == 128) {
							itemId = 1329; // mithril scimitar
							itemAmount = 1;
						} else if (rewardId == 129) {
							itemId = 1243; // mithril spear
							itemAmount = 1;
						} else if (rewardId == 130) {
							itemId = 1285; // mithril sword
							itemAmount = 1;
						} else if (rewardId == 131) {
							itemId = 1315; // mithril 2h
							itemAmount = 1;
						} else if (rewardId == 132) {
							itemId = 1343; // mithril warhammer
							itemAmount = 1;
						} else if (rewardId == 133) {
							itemId = 1369; // mithril battleaxe
							itemAmount = 1;
						} else if (rewardId == 134) {
							itemId = 1159; // mithril full helm
							itemAmount = 1;
						} else if (rewardId == 135) {
							itemId = 1109; // mithril chainbody
							itemAmount = 1;
						} else if (rewardId == 136) {
							itemId = 1121; // mithril platebody
							itemAmount = 1;
						} else if (rewardId == 137) {
							itemId = 1071; // mithril platelegs
							itemAmount = 1;
						} else if (rewardId == 138) {
							itemId = 1085; // mithril plateskirt
							itemAmount = 1;
						} else if (rewardId == 139) {
							itemId = 1181; // mithril sq shield
							itemAmount = 1;
						} else if (rewardId == 140) {
							itemId = 1197; // mithril kiteshield
							itemAmount = 1;
						} else if (rewardId == 141) {
							itemId = 12997; // mithril gauntlets
							itemAmount = 1;
						} else if (rewardId == 142) {
							itemId = 4127; // mithril boots
							itemAmount = 1;
						} else if (rewardId == 143) {
							itemId = 1213; // rune dagger
							itemAmount = 1;
						} else if (rewardId == 144) {
							itemId = 3202; // rune halberd
							itemAmount = 1;
						} else if (rewardId == 145) {
							itemId = 1303; // rune longsword
							itemAmount = 1;
						} else if (rewardId == 146) {
							itemId = 1432; // rune mace
							itemAmount = 1;
						} else if (rewardId == 147) {
							itemId = 1275; // rune pickaxe
							itemAmount = 1;
						} else if (rewardId == 148) {
							itemId = 1333; // rune scimitar
							itemAmount = 1;
						} else if (rewardId == 149) {
							itemId = 1247; // rune spear
							itemAmount = 1;
						} else if (rewardId == 150) {
							itemId = 1289; // rune sword
							itemAmount = 1;
						} else if (rewardId == 151) {
							itemId = 1319; // rune 2h
							itemAmount = 1;
						} else if (rewardId == 152) {
							itemId = 1347; // rune warhammer
							itemAmount = 1;
						} else if (rewardId == 153) {
							itemId = 1373; // rune battleaxe
							itemAmount = 1;
						} else if (rewardId == 154) {
							itemId = 1163; // rune full helm
							itemAmount = 1;
						} else if (rewardId == 155) {
							itemId = 1113; // rune chainbody
							itemAmount = 1;
						} else if (rewardId == 156) {
							itemId = 1127; // rune platebody
							itemAmount = 1;
						} else if (rewardId == 157) {
							itemId = 1079; // rune platelegs
							itemAmount = 1;
						} else if (rewardId == 158) {
							itemId = 1093; // rune plateskirt
							itemAmount = 1;
						} else if (rewardId == 159) {
							itemId = 1185; // rune sq shield
							itemAmount = 1;
						} else if (rewardId == 160) {
							itemId = 1201; // rune kiteshield
							itemAmount = 1;
						} else if (rewardId == 161) {
							itemId = 13003; // rune gauntlets
							itemAmount = 1;
						} else if (rewardId == 162) {
							itemId = 4131; // rune boots
							itemAmount = 1;
						} else if (rewardId == 163) {
							itemId = 1705; // amulet of glory
							itemAmount = 1;
						} else if (rewardId == 164) {
							itemId = 1725; // amulet of strength
							itemAmount = 1;
						} else if (rewardId == 165) {
							itemId = 1727; // amulet of magic
							itemAmount = 1;
						} else if (rewardId == 166) {
							itemId = 1729; // amulet of defence
							itemAmount = 1;
						} else if (rewardId == 167) {
							itemId = 1731; // amulet of power
							itemAmount = 1;
						} else if (rewardId == 168) {
							itemId = 1381; // staff of air
							itemAmount = 1;
						} else if (rewardId == 169) {
							itemId = 1383; // staff of water
							itemAmount = 1;
						} else if (rewardId == 170) {
							itemId = 1385; // staff of earth
							itemAmount = 1;
						} else if (rewardId == 171) {
							itemId = 1387; // staff of fire
							itemAmount = 1;
						} else if (rewardId == 172) {
							itemId = 4675; // ancient staff
							itemAmount = 1;
						} else if (rewardId == 173) {
							itemId = 4089; // mystic hat
							itemAmount = 1;
						} else if (rewardId == 174) {
							itemId = 4091; // mystic robe top
							itemAmount = 1;
						} else if (rewardId == 175) {
							itemId = 4093; // mystic robe bottom
							itemAmount = 1;
						} else if (rewardId == 176) {
							itemId = 4095; // mystic gloves
							itemAmount = 1;
						} else if (rewardId == 177) {
							itemId = 4097; // mystic boots
							itemAmount = 1;
						} else if (rewardId == 178) {
							itemId = 6107; // ghostly robe top
							itemAmount = 1;
						} else if (rewardId == 179) {
							itemId = 6108; // ghostly robe bottom
							itemAmount = 1;
						} else if (rewardId == 180) {
							itemId = 6109; // ghostly hood
							itemAmount = 1;
						} else if (rewardId == 181) {
							itemId = 2890; // elemental shield
							itemAmount = 1;
						} else if (rewardId == 182) {
							itemId = 3840; // holy book
							itemAmount = 1;
						} else if (rewardId == 183) {
							itemId = 3844; // book of balance
							itemAmount = 1;
						} else if (rewardId == 184) {
							itemId = 3842; // unholy book
							itemAmount = 1;

							// Package 3
						} else if (rewardId == 300) {
							itemId = 2572; // Ring of wealth
							itemAmount = 1;
						} else if (rewardId == 301) {
							itemId = 6731; // seers ring
							itemAmount = 1;
						} else if (rewardId == 302) {
							itemId = 6733; // archers ring
							itemAmount = 1;
						} else if (rewardId == 303) {
							itemId = 6735; // warrior ring
							itemAmount = 1;
						} else if (rewardId == 304) {
							itemId = 6737; // berserker ring
							itemAmount = 1;
						} else if (rewardId == 305) {
							itemId = 4708; // ahrims hood
							itemAmount = 1;
						} else if (rewardId == 306) {
							itemId = 4710; // ahrims staff
							itemAmount = 1;
						} else if (rewardId == 307) {
							itemId = 4712; // ahrims robetop
							itemAmount = 1;
						} else if (rewardId == 308) {
							itemId = 4714; // ahrims robeskirt
							itemAmount = 1;
						} else if (rewardId == 309) {
							itemId = 4716; // dharoks helm
							itemAmount = 1;
						} else if (rewardId == 310) {
							itemId = 4718; // dharoks greataxe
							itemAmount = 1;
						} else if (rewardId == 311) {
							itemId = 4720; // dharoks platebody
							itemAmount = 1;
						} else if (rewardId == 312) {
							itemId = 4722; // dharoks platelegs
							itemAmount = 1;
						} else if (rewardId == 313) {
							itemId = 4724; // guthans helm
							itemAmount = 1;
						} else if (rewardId == 314) {
							itemId = 4726; // guthans warspear
							itemAmount = 1;
						} else if (rewardId == 315) {
							itemId = 4728; // guthans platebody
							itemAmount = 1;
						} else if (rewardId == 316) {
							itemId = 4730; // guthans platelegs
							itemAmount = 1;
						} else if (rewardId == 317) {
							itemId = 4732; // karils coif
							itemAmount = 1;
						} else if (rewardId == 318) {
							itemId = 4734; // karils crossbow
							itemAmount = 1;
						} else if (rewardId == 319) {
							itemId = 4736; // karils leathertop
							itemAmount = 1;
						} else if (rewardId == 320) {
							itemId = 4738; // karils leatherskirt
							itemAmount = 1;
						} else if (rewardId == 321) {
							itemId = 4745; // torags helm
							itemAmount = 1;
						} else if (rewardId == 322) {
							itemId = 4747; // torags hammers
							itemAmount = 1;
						} else if (rewardId == 323) {
							itemId = 4749; // torags platebody
							itemAmount = 1;
						} else if (rewardId == 324) {
							itemId = 4751; // torags platelegs
							itemAmount = 1;
						} else if (rewardId == 325) {
							itemId = 4753; // veracs helm
							itemAmount = 1;
						} else if (rewardId == 326) {
							itemId = 4755; // veracs flail
							itemAmount = 1;
						} else if (rewardId == 327) {
							itemId = 4757; // veracs brassard
							itemAmount = 1;
						} else if (rewardId == 328) {
							itemId = 4759; // veracs plateskirt
							itemAmount = 1;
						} else if (rewardId == 329) {
							itemId = 13887; // vesta's chainbody
							itemAmount = 1;
						} else if (rewardId == 330) {
							itemId = 13893; // vesta's plateskirt
							itemAmount = 1;
						} else if (rewardId == 331) {
							itemId = 13899; // vesta's longsword
							itemAmount = 1;
						} else if (rewardId == 332) {
							itemId = 13905; // vesta's spear
							itemAmount = 1;
						} else if (rewardId == 333) {
							itemId = 13884; // statiu's platebody
							itemAmount = 1;
						} else if (rewardId == 334) {
							itemId = 13890; // statiu's platelegs
							itemAmount = 1;
						} else if (rewardId == 335) {
							itemId = 13896; // statiu's full helm
							itemAmount = 1;
						} else if (rewardId == 336) {
							itemId = 13902; // statiu's warhammer
							itemAmount = 1;
						} else if (rewardId == 337) {
							itemId = 13858; // zuriel's robe top
							itemAmount = 1;
						} else if (rewardId == 338) {
							itemId = 13861; // zuriel's robe bottom
							itemAmount = 1;
						} else if (rewardId == 339) {
							itemId = 13864; // zuriel's hood
							itemAmount = 1;
						} else if (rewardId == 340) {
							itemId = 13867; // zuriel's staff
							itemAmount = 1;
						} else if (rewardId == 341) {
							itemId = 13870; // morrigan's leather body
							itemAmount = 1;
						} else if (rewardId == 342) {
							itemId = 13873; // morrigan's leather chaps
							itemAmount = 1;
						} else if (rewardId == 343) {
							itemId = 13876; // morrigan's coif
							itemAmount = 1;
						} else if (rewardId == 344) {
							itemId = 13879; // morrigan's javelin
							itemAmount = 100;
						} else if (rewardId == 345) {
							itemId = 13883; // morrigan's throwin axe
							itemAmount = 100;
						} else if (rewardId == 346) {
							itemId = 11286; // visage
							itemAmount = 1;
						} else if (rewardId == 347) {
							itemId = 4151; // abyssal whip
							itemAmount = 1;
						} else if (rewardId == 348) {
							itemId = 6570; // firecape
							itemAmount = 1;
						} else if (rewardId == 349) {
							itemId = 11235; // dark bow
							itemAmount = 1;

						} else if (rewardId == 350) {
							itemId = 15273; // noted rocktails
							itemAmount = 250;
						} else if (rewardId == 351) {
							itemId = 537; // dragon bones
							itemAmount = 50;
						} else if (rewardId == 352) {
							itemId = 18831; // frost bones
							itemAmount = 25;
						} else if (rewardId == 353) {
							itemId = 761; // doubble exp scroll
							itemAmount = 1;

							// Package 4
						} else if (rewardId == 400) {
							itemId = 11704; // bandos hilt
							itemAmount = 1;
						} else if (rewardId == 401) {
							itemId = 11724; // bandos chestplate
							itemAmount = 1;
						} else if (rewardId == 402) {
							itemId = 11726; // bandos tasset
							itemAmount = 1;
						} else if (rewardId == 403) {
							itemId = 11728; // bandos boots
							itemAmount = 1;
						} else if (rewardId == 404) {
							itemId = 11702; // armadyl hilt
							itemAmount = 1;
						} else if (rewardId == 405) {
							itemId = 11718; // armadyl helmet
							itemAmount = 1;
						} else if (rewardId == 406) {
							itemId = 11720; // armadyl chestplate
							itemAmount = 1;
						} else if (rewardId == 407) {
							itemId = 11722; // armadyl plateskirt
							itemAmount = 1;
						} else if (rewardId == 408) {
							itemId = 20135; // torva full helm
							itemAmount = 1;
						} else if (rewardId == 409) {
							itemId = 20139; // torva platebody
							itemAmount = 1;
						} else if (rewardId == 410) {
							itemId = 20143; // torva platelegs
							itemAmount = 1;
						} else if (rewardId == 411) {
							itemId = 20147; // pernix cowl
							itemAmount = 1;
						} else if (rewardId == 412) {
							itemId = 20151; // pernix body
							itemAmount = 1;
						} else if (rewardId == 413) {
							itemId = 20155; // pernix chaps
							itemAmount = 1;
						} else if (rewardId == 414) {
							itemId = 20159; // virtus mask
							itemAmount = 1;
						} else if (rewardId == 415) {
							itemId = 20163; // virtus robe top
							itemAmount = 1;
						} else if (rewardId == 416) {
							itemId = 20167; // virtus robe legs
							itemAmount = 1;
						} else if (rewardId == 417) {
							itemId = 21787; // steadfast boots
							itemAmount = 1;
						} else if (rewardId == 418) {
							itemId = 21793; // ragefire boots
							itemAmount = 1;
						} else if (rewardId == 419) {
							itemId = 21790; // glaiven boots
							itemAmount = 1;
						} else if (rewardId == 420) {
							itemId = 13740; // divine spirit shield
							itemAmount = 1;
						} else if (rewardId == 421) {
							itemId = 13742; // elysian spirit shield
							itemAmount = 1;
						} else if (rewardId == 422) {
							itemId = 13738; // arcane spirit shield
							itemAmount = 1;
						} else if (rewardId == 423) {
							itemId = 13744; // spectral spirit shield
							itemAmount = 1;
						} else if (rewardId == 424) {
							itemId = 18349; // chaotic rapier
							itemAmount = 1;
						} else if (rewardId == 425) {
							itemId = 18351; // chaotic longsword
							itemAmount = 1;
						} else if (rewardId == 426) {
							itemId = 18353; // chaotic maul
							itemAmount = 1;
						} else if (rewardId == 427) {
							itemId = 18357; // chaotic crossbow
							itemAmount = 1;
						} else if (rewardId == 428) {
							itemId = 18359; // chaotic kiteshield
							itemAmount = 1;
						} else if (rewardId == 429) {
							itemId = 18355; // chaotic staff
							itemAmount = 1;
						} else if (rewardId == 430) {
							itemId = 18363; // farseer kiteshield
							itemAmount = 1;
						} else if (rewardId == 431) {
							itemId = 18361; // eagle-eye kiteshield
							itemAmount = 1;
						} else if (rewardId == 432) {
							itemId = 15486; // staff of light
							itemAmount = 1;
						} else if (rewardId == 433) {
							itemId = 20171; // zaryte bow
							itemAmount = 1;
						} else if (rewardId == 434) {
							itemId = 14484; // dragon claws
							itemAmount = 1;
						} else if (rewardId == 435) {
							itemId = 11730; // saradomin sword
							itemAmount = 1;
						} else if (rewardId == 436) {
							itemId = 19780; // korasi's sword
							itemAmount = 1;
						} else if (rewardId == 437) {
							itemId = 23639; // tokhaar kal
							itemAmount = 1;
						} else if (rewardId == 438) {
							itemId = 18335; // arcane stream
							itemAmount = 1;
						} else if (rewardId == 439) {
							itemId = 21371; // vine whip
							itemAmount = 1;
						} else if (rewardId == 440) {
							itemId = 15241; // hand cannon
							itemAmount = 1;

							// Package 5
						} else if (rewardId == 500) {
							itemId = 1050; // santa hat
							itemAmount = 1;
						} else if (rewardId == 501) {
							itemId = 15740; // black santa hat
							itemAmount = 1;
						} else if (rewardId == 502) {
							itemId = 1038; // red partyhat
							itemAmount = 1;
						} else if (rewardId == 503) {
							itemId = 1040; // yellow partyhat
							itemAmount = 1;
						} else if (rewardId == 504) {
							itemId = 1042; // blue partyhat
							itemAmount = 1;
						} else if (rewardId == 505) {
							itemId = 1044; // green partyhat
							itemAmount = 1;
						} else if (rewardId == 506) {
							itemId = 1046; // purple partyhat
							itemAmount = 1;
						} else if (rewardId == 507) {
							itemId = 1048; // white partyhat
							itemAmount = 1;
						} else if (rewardId == 508) {
							itemId = 15742; // pink partyhat
							itemAmount = 1;
						} else if (rewardId == 509) {
							itemId = 15741; // black partyhat
							itemAmount = 1;
						} else if (rewardId == 510) {
							itemId = 1037; // bunny ears
							itemAmount = 1;
						} else if (rewardId == 511) {
							itemId = 4084; // sled
							itemAmount = 1;
						} else if (rewardId == 512) {
							itemId = 1053; // green h'ween mask
							itemAmount = 1;
						} else if (rewardId == 513) {
							itemId = 1055; // blue h'ween mask
							itemAmount = 1;
						} else if (rewardId == 514) {
							itemId = 1057; // red h'ween mask
							itemAmount = 1;
						} else if (rewardId == 515) {
							itemId = 1050; // santa hat
							itemAmount = 1;
						} else if (rewardId == 516) {
							itemId = 1038; // red partyhat
							itemAmount = 1;
						} else if (rewardId == 517) {
							itemId = 1040; // yellow partyhat
							itemAmount = 1;
						} else if (rewardId == 518) {
							itemId = 1042; // blue partyhat
							itemAmount = 1;
						} else if (rewardId == 519) {
							itemId = 1044; // green partyhat
							itemAmount = 1;
						} else if (rewardId == 520) {
							itemId = 1046; // purple partyhat
							itemAmount = 1;
						} else if (rewardId == 521) {
							itemId = 1048; // white partyhat
							itemAmount = 1;
						} else if (rewardId == 522) {
							itemId = 1037; // bunny ears
							itemAmount = 1;
						} else if (rewardId == 523) {
							itemId = 1053; // green h'ween mask
							itemAmount = 1;
						} else if (rewardId == 524) {
							itemId = 1055; // blue h'ween mask
							itemAmount = 1;
						} else if (rewardId == 525) {
							itemId = 1057; // red h'ween mask
							itemAmount = 1;
						} else if (rewardId == 526) {
							itemId = 1053; // green h'ween mask
							itemAmount = 1;
						} else if (rewardId == 527) {
							itemId = 1055; // blue h'ween mask
							itemAmount = 1;
						} else if (rewardId == 528) {
							itemId = 1057; // red h'ween mask
							itemAmount = 1;
						} else if (rewardId == 529) {
							itemId = 30058; // black h'ween mask
							itemAmount = 1;
						}

						if (itemId == 0 || itemAmount == 0) {
							c.sendMessage(
									"You do not have any prizes to claim.");
						} else if (itemId > 0 && itemAmount > 0
								&& c.getItems().addItem(itemId, itemAmount)) {
							c.sendMessage("Claimed spin ticket reward.");
						} else {
							c.sendMessage(
									"You do not have enough inventory space.");
						}

						this.stop();

					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

}
