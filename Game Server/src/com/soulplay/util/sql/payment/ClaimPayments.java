package com.soulplay.util.sql.payment;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.player.titles.Titles;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.util.sql.SqlHandler;

import plugin.button.infotab.InfoTabButtonPlugin;

public class ClaimPayments {

	public static void claimDonationOverWebSQL(final Client c) {
		
		if (!Config.RUN_ON_DEDI)
			return;

		// SqlHandler.getSQLService().submit(() -> {
		TaskExecutor.executeWebsiteSQL(() -> {

			try {

				final String name = c.getName().toLowerCase().replace(" ",
						"_");

				int prodNotFinal = 0;
				int priceNotFinal = 0;

				String breakString = SqlHandler.getDonatorTable(name); // HttpPost.getHttpResponseString("http://www.soulplayps.com/excludecf/claim/donations.php?player="+name);
				// System.out.println("string:"+breakString);

				if (breakString == null || breakString.equals("no")) {
					PlayerHandler.sendMessageMainThread(c,
							"You do not have any unclaimed donations at the moment.");
					return;
				}

				String[] args = breakString.split(":");

				prodNotFinal = Integer.parseInt(args[0]);
				priceNotFinal = Integer.parseInt(args[1]);

				// System.out.println("prod:"+prodNotFinal+"
				// price:"+priceNotFinal);

				final int prod = prodNotFinal;
				final int price = priceNotFinal;

				Server.getTaskScheduler().schedule(new Task(true) {

					@Override
					protected void execute() {

						if (c == null || !c.isActive) {
							Server.getSlackApi().call(SlackMessageBuilder
									.buildDonationClaimErrorMessage(
											Config.SERVER_NAME,
											"Name:" + name + " prod:" + prod
													+ " price:" + price,
											"#developers"));
							this.stop();
							return;
						}

						boolean b = false;

						if (Config.SERVER_NAME.equals("SoulEco")) {
							//new items
							if (prod == 10000 && price == 29) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30264, 1);
								c.getItems().addItem(30265, 1);
								c.getItems().addItem(30266, 1);
								c.sendMessage(
										"<col=ff0000>You have received elder chaos set.");
								b = true;
							}
							if (prod == 10001 && price == 220) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30327, 1);
								c.sendMessage(
										"<col=ff0000>You have received Scythe of vitur.");
								b = true;
							}
							if (prod == 10002 && price == 12) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30090, 1);
								c.sendMessage(
										"<col=ff0000>You have received Elder maul.");
								b = true;
							}
							if (prod == 10003 && price == 45) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30086, 1);
								c.getItems().addItem(30085, 1);
								c.getItems().addItem(30084, 1);
								c.sendMessage(
										"<col=ff0000>You have received Ancestral set.");
								b = true;
							}
							if (prod == 10004 && price == 11) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30089, 1);
								c.sendMessage(
										"<col=ff0000>You have received Kodai wand.");
								b = true;
							}
							if (prod == 10005 && price == 10) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30094, 1);
								c.sendMessage(
										"<col=ff0000>You have received Dinhs Bulwark.");
								b = true;
							}
							if (prod == 10006 && price == 5) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30094, 1);
								c.sendMessage(
										"<col=ff0000>You have received Dinhs Bulwark.");
								b = true;
							}
							if (prod == 10007 && price == 5) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30095, 1);
								c.sendMessage(
										"<col=ff0000>You have received Dexterous prayer scroll.");
								b = true;
							}
							if (prod == 10008 && price == 5) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(30096, 1);
								c.sendMessage(
										"<col=ff0000>You have received Arcane prayer scroll.");
								b = true;
							}
						
							
							
							if (prod == 1 && price == 10) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 10, true);// 1
																			// year
																			// anniversary
																			// should
																			// be
																			// 10
								c.sendMessage(
										"<col=ff0000>You have received 10 Donator Points!");// 1
																						// year
																						// anniversary
																						// should
																						// be
																						// 10
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											10 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 20;
									c.sendMessage(
											"You have received 2 yells You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							} else if (prod == 2 && price == 20) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 21, true);// 1
																			// year
																			// anniversary
																			// should
																			// be
																			// 21
								c.sendMessage(
										"<col=ff0000>You have received 21 Donator Points!");// 1
																						// year
																						// anniversary
																						// should
																						// be
																						// 21
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											20 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 42;
									c.sendMessage(
											"You have received 4 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							} else if (prod == 3 && price == 30) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 32, true);
								c.sendMessage(
										"<col=ff0000>You have received 32 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											30 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 65;
									c.sendMessage(
											"You have received 6 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							} else if (prod == 4 && price == 40) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 43, true);
								c.sendMessage(
										"<col=ff0000>You have received 43 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											40 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 90;
									c.sendMessage(
											"You have received 8 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 5 && price == 100) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 108, true);
								c.sendMessage(
										"<col=ff0000>You have received 108 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											100 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 225;
									c.sendMessage(
											"You have received 24 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 6 && price == 500) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 545, true);
								c.sendMessage(
										"<col=ff0000>You have received 545 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											500 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 1150;
									c.sendMessage(
											"You have received 125 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 7 && price == 1000) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.setDonPoints(c.getDonPoints() + 1200, true);//
								c.sendMessage(
										"<col=ff0000>You have received 1200 Donator Points!");//
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											1000 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 2500;
									c.sendMessage(
											"You have received 270 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 8 && price == 10) {
								c.getItems().addItem(786, 1);
								c.sendMessage(
										"<col=ff0000>You have received 10$ Donator Scroll.");
								b = true;
								PlayerHandler.scrollsBought++;
							}

							if (prod == 9 && price == 50) {
								c.getItems().addItem(1505, 1);
								c.sendMessage(
										"<col=ff0000>You have received 50$ Donator Scroll.");
								b = true;
								PlayerHandler.scrollsBought++;
							}

							if (prod == 10 && price == 100) {
								c.getItems().addItem(2396, 1);
								c.sendMessage(
										"<col=ff0000>You have received 100$ Donator Scroll.");
								b = true;
								PlayerHandler.scrollsBought++;
							}

							if (prod == 11 && price == 45) {
								c.getItems().addItem(15412, 1);
								c.sendMessage(
										"<col=ff0000>You have received 1 Monster egg.");
								b = true;
							}
							if (prod == 12 && price == 450) {
								c.getItems().addItem(30209, 1);
								c.sendMessage(
										"<col=ff0000>You have received 500$ Donator Casket.");
								b = true;
								PlayerHandler.scrollsBought++;
							}
							// weekly items
							if (prod == 5000 && price == 449) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1042, 1);
								c.sendMessage(
										"<col=ff0000>You have received blue Partyhat.");
								b = true;
							}

							if (prod == 5001 && price == 449) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1044, 1);
								c.sendMessage(
										"<col=ff0000>You have received green Partyhat.");
								b = true;
							}

							if (prod == 5002 && price == 449) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1040, 1);
								c.sendMessage(
										"<col=ff0000>You have received yellow Partyhat.");
								b = true;
							}

							if (prod == 5003 && price == 449) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1038, 1);
								c.sendMessage(
										"<col=ff0000>You have received red Partyhat.");
								b = true;
							}

							if (prod == 5004 && price == 449) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1046, 1);
								c.sendMessage(
										"<col=ff0000>You have received purple Partyhat.");
								b = true;
							}

							if (prod == 5005 && price == 655) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15742, 1);
								c.sendMessage(
										"<col=ff0000>You have received pink Partyhat.");
								b = true;
							}

							if (prod == 5006 && price == 899) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15741, 1);
								c.sendMessage(
										"<col=ff0000>You have received black Partyhat.");
								b = true;
							}

							if (prod == 5007 && price == 350) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(12399, 1);
								c.sendMessage(
										"<col=ff0000>You have received Partyhat Specs.");
								b = true;
							}

							if (prod == 5008 && price == 99) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1037, 1);
								c.sendMessage(
										"<col=ff0000>You have received Bunny Ears.");
								b = true;
							}

							if (prod == 5009 && price == 199) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1050, 1);
								c.sendMessage(
										"<col=ff0000>You have received Santa Hat.");
								b = true;
							}

							if (prod == 5010 && price == 799) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15740, 1);
								c.sendMessage(
										"<col=ff0000>You have received black Santa Hat.");
								b = true;
							}

							if (prod == 5011 && price == 250) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1419, 1);
								c.sendMessage("<col=ff0000>You have received Scythe.");
								b = true;
							}

							if (prod == 5012 && price == 485) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20929, 1);
								c.sendMessage("<col=ff0000>You have received Katana.");
								b = true;
							}

							if (prod == 5013 && price == 785) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4083, 1);
								c.sendMessage("<col=ff0000>You have received Sled.");
								b = true;
							}

							if (prod == 5014 && price == 180) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1057, 1);
								c.sendMessage(
										"<col=ff0000>You have received red H'Ween Mask.");
								b = true;
							}
							if (prod == 5015 && price == 180) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1055, 1);
								c.sendMessage(
										"<col=ff0000>You have received blue H'Ween Mask.");
								b = true;
							}
							if (prod == 5016 && price == 180) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1053, 1);
								c.sendMessage(
										"<col=ff0000>You have received green H'Ween Mask.");
								b = true;
							}

							if (prod == 5017 && price == 55) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19333, 1);
								c.sendMessage(
										"<col=ff0000>You have received Fury Ornament Kit.");
								b = true;
							}

							if (prod == 5018 && price == 550) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(11858, 1);
								c.sendMessage(
										"<col=ff0000>You have received full 3rd Melee Set.");
								b = true;
							}

							if (prod == 5019 && price == 320) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(11862, 1);
								c.sendMessage(
										"<col=ff0000>You have received full 3rd Mage Set.");
								b = true;
							}

							if (prod == 5020 && price == 320) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(11860, 1);
								c.sendMessage(
										"<col=ff0000>You have received full 3rd Range Set.");
								b = true;
							}

							if (prod == 5021 && price == 450) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19580, 1);
								c.sendMessage(
										"<col=ff0000>You have received full Third-age Druidic Set.");
								b = true;
							}

							if (prod == 5022 && price == 60) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(12391, 1);
								c.sendMessage(
										"<col=ff0000>You have received Gilded Boots.");
								b = true;
							}

							if (prod == 5023 && price == 100) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19323, 1);
								c.sendMessage(
										"<col=ff0000>You have received Dragon Staff.");
								b = true;
							}

							if (prod == 5024 && price == 99) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20786, 1);
								c.sendMessage(
										"<col=ff0000>You have received Gilded Dragon Pickaxe.");
								b = true;
							}

							if (prod == 5025 && price == 120) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(9050, 1);
								c.sendMessage(
										"<col=ff0000>You have received Pharaoh Sceptre.");
								b = true;
							}

							if (prod == 5026 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19281, 1);
								c.getItems().addItem(19284, 1);
								c.getItems().addItem(19287, 1);
								c.getItems().addItem(19290, 1);
								c.getItems().addItem(19293, 1);
								c.sendMessage(
										"<col=ff0000>You have received Dragon Masks.");
								b = true;
							}

							if (prod == 5027 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19296, 1);
								c.getItems().addItem(19299, 1);
								c.getItems().addItem(19302, 1);
								c.getItems().addItem(19305, 1);
								c.sendMessage(
										"<col=ff0000>You have received Metal Dragon Masks.");
								b = true;
							}

							if (prod == 5028 && price == 185) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19341, 1);
								c.getItems().addItem(19343, 1);
								c.getItems().addItem(19342, 1);
								c.sendMessage(
										"<col=ff0000>You have received full dragon (sp).");
								b = true;
							}

							if (prod == 5029 && price == 125) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20949, 1);
								c.sendMessage(
										"<col=ff0000>You have received red Robin Hood Hat.");
								b = true;
							}

							if (prod == 5030 && price == 125) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20950, 1);
								c.sendMessage(
										"<col=ff0000>You have received yellow Robin Hood Hat.");
								b = true;
							}

							if (prod == 5031 && price == 125) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20951, 1);
								c.sendMessage(
										"<col=ff0000>You have received blue Robin Hood Hat.");
								b = true;
							}

							if (prod == 5032 && price == 125) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20952, 1);
								c.sendMessage(
										"<col=ff0000>You have received white Robin Hood Hat.");
								b = true;
							}

							if (prod == 5033 && price == 285) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19337, 1);
								c.getItems().addItem(19336, 1);
								c.getItems().addItem(19338, 1);
								c.getItems().addItem(19340, 1);
								c.sendMessage(
										"<col=ff0000>You have received full dragon (or).");
								b = true;
							}

							if (prod == 5034 && price == 110) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20135, 1);
								c.getItems().addItem(20139, 1);
								c.getItems().addItem(20143, 1);
								c.sendMessage(
										"<col=ff0000>You have received full Torva!");
								c.yellPoints += 110;
								c.sendMessage(
										"You have received 110 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5035 && price == 110) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20147, 1);
								c.getItems().addItem(20151, 1);
								c.getItems().addItem(20155, 1);
								c.sendMessage(
										"<col=ff0000>You have received full Pernix!");
								c.yellPoints += 110;
								c.sendMessage(
										"You have received 110 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5036 && price == 110) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20159, 1);
								c.getItems().addItem(20163, 1);
								c.getItems().addItem(20167, 1);
								c.sendMessage(
										"<col=ff0000>You have received full Virtus!");
								c.yellPoints += 110;
								c.sendMessage(
										"You have received 110 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5037 && price == 7) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(2364, 1000);
								c.sendMessage(
										"<col=ff0000>You have received 1.000 Rune bars!");
								c.yellPoints += 7;
								c.sendMessage(
										"You have received 7 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5038 && price == 6) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(18831, 400);
								c.sendMessage(
										"<col=ff0000>You have received 400 Frost dragon bones!");
								c.yellPoints += 6;
								c.sendMessage(
										"You have received 6 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5039 && price == 50) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(13740, 1);
								c.getItems().addItem(13738, 1);
								c.getItems().addItem(13742, 1);
								c.getItems().addItem(13744, 1);
								c.getItems().addItem(13734, 4);
								c.sendMessage(
										"<col=ff0000>You have received Spirit shield pack!");
								c.yellPoints += 50;
								c.sendMessage(
										"You have received 50 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5040 && price == 5) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(11212, 5000);
								c.sendMessage(
										"<col=ff0000>You have received 5.000 Dragon arrows!");
								c.yellPoints += 5;
								c.sendMessage(
										"You have received 5 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5041 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(18353, 1);
								c.getItems().addItem(18349, 1);
								c.getItems().addItem(18351, 1);
								c.getItems().addItem(18357, 1);
								c.getItems().addItem(18355, 1);
								c.sendMessage(
										"<col=ff0000>You have received Chaotic weapon set!");
								c.yellPoints += 48;
								c.sendMessage(
										"You have received 48 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5042 && price == 295) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20135, 1);
								c.getItems().addItem(20139, 1);
								c.getItems().addItem(20143, 1);
								c.getItems().addItem(20147, 1);
								c.getItems().addItem(20151, 1);
								c.getItems().addItem(20155, 1);
								c.getItems().addItem(20159, 1);
								c.getItems().addItem(20163, 1);
								c.getItems().addItem(20167, 1);
								c.getItems().addItem(20171, 1);
								c.sendMessage(
										"<col=ff0000>You have received Nex gear pack!");
								c.yellPoints += 295;
								c.sendMessage(
										"You have received 295 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5043 && price == 2500) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(1038, 1);
								c.getItems().addItem(1040, 1);
								c.getItems().addItem(1042, 1);
								c.getItems().addItem(1044, 1);
								c.getItems().addItem(1046, 1);
								c.getItems().addItem(1048, 1);
								c.sendMessage(
										"<col=ff0000>You have received Partyhat pack!");
								c.yellPoints += 2500;
								c.sendMessage(
										"You have received 2500 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5044 && price == 30) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(8839, 1);
								c.getItems().addItem(8840, 1);
								c.getItems().addItem(8842, 1);
								c.getItems().addItem(11663, 1);
								c.getItems().addItem(11664, 1);
								c.getItems().addItem(11665, 1);
								c.sendMessage(
										"<col=ff0000>You have received Void pack!");
								c.yellPoints += 30;
								c.sendMessage(
										"You have received 30 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5045 && price == 5) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15241, 1);
								c.getItems().addItem(15243, 400);
								c.sendMessage(
										"<col=ff0000>You have received hand cannon package!");
								c.yellPoints += 5;
								c.sendMessage(
										"You have received 5 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5046 && price == 48) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15018, 1);
								c.getItems().addItem(15019, 1);
								c.getItems().addItem(15020, 1);
								c.getItems().addItem(15220, 1);
								c.sendMessage(
										"<col=ff0000>You have received Ring pack!");
								c.yellPoints += 48;
								c.sendMessage(
										"You have received 48 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5047 && price == 11) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(19669, 1);
								c.sendMessage(
										"<col=ff0000>You have received Ring of vigour!");
								c.yellPoints += 11;
								c.sendMessage(
										"You have received 11 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							// pets
							if (prod == 5048 && price == 100) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(21512, 1);
								c.sendMessage(
										"<col=ff0000>You have received Tzrek Jad pet!");
								c.yellPoints += 100;
								c.sendMessage(
										"You have received 100 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5049 && price == 75) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(3999, 1);
								c.sendMessage(
										"<col=ff0000>You have received Kree Arra pet!");
								c.yellPoints += 75;
								c.sendMessage(
										"You have received 75 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5050 && price == 75) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4005, 1);
								c.sendMessage(
										"<col=ff0000>You have received Zilyana pet!");
								c.yellPoints += 75;
								c.sendMessage(
										"You have received 75 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5051 && price == 75) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4001, 1);
								c.sendMessage(
										"<col=ff0000>You have received General Graardor pet!");
								c.yellPoints += 75;
								c.sendMessage(
										"You have received 75 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5052 && price == 75) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4004, 1);
								c.sendMessage(
										"<col=ff0000>You have received Kril Tsutsaroth pet!");
								c.yellPoints += 75;
								c.sendMessage(
										"You have received 75 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5053 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4006, 1);
								c.sendMessage(
										"<col=ff0000>You have received Supreme pet!");
								c.yellPoints += 65;
								c.sendMessage(
										"You have received 65 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5054 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4007, 1);
								c.sendMessage(
										"<col=ff0000>You have received Prime pet!");
								c.yellPoints += 65;
								c.sendMessage(
										"You have received 65 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5055 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4008, 1);
								c.sendMessage(
										"<col=ff0000>You have received Rex pet!");
								c.yellPoints += 65;
								c.sendMessage(
										"You have received 65 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5056 && price == 60) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(3998, 1);
								c.sendMessage(
										"<col=ff0000>You have received Baby Mole pet!");
								c.yellPoints += 60;
								c.sendMessage(
										"You have received 60 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5057 && price == 70) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4011, 1);
								c.sendMessage(
										"<col=ff0000>You have received Prince Black Dragon pet!");
								c.yellPoints += 70;
								c.sendMessage(
										"You have received 70 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5058 && price == 70) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4010, 1);
								c.sendMessage(
										"<col=ff0000>You have received Kalphite Princess pet!");
								c.yellPoints += 70;
								c.sendMessage(
										"You have received 70 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5059 && price == 70) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(3997, 1);
								c.sendMessage(
										"<col=ff0000>You have received Kalphite Princess pet!");
								c.yellPoints += 70;
								c.sendMessage(
										"You have received 70 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 5060 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(4002, 1);
								c.sendMessage(
										"<col=ff0000>You have received Chaos Elemental pet!");
								c.yellPoints += 65;
								c.sendMessage(
										"You have received 65 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5061 && price == 25) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30087, 1);
								c.getItems().addItem(9244, 300);
								c.sendMessage("<col=ff0000>You have received Dragon Hunter Crossbow and Dragon bolts (e)!");
								c.yellPoints += 25;
								c.sendMessage("You have received 25 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5062 && price == 10) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(9952, 1);
								c.sendMessage("<col=ff0000>You have received a charming imp that helps you collect charms!");
								c.yellPoints += 10;
								c.sendMessage("You have received 10 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5063 && price == 190) { 
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30092, 1);
								c.sendMessage("<col=ff0000>You have received a twisted bow!");
								c.yellPoints += 190;
								c.sendMessage("You have received 190 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5064 && price == 52) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30091, 1);
								c.sendMessage("<col=ff0000>You have received a twisted buckler!");
								c.yellPoints += 52;
								c.sendMessage("You have received 52 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							if (prod == 5065 && price == 45) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(20125, 1);
								c.getItems().addItem(20127, 1);
								c.getItems().addItem(20129, 1);
								c.getItems().addItem(20131, 1);
								c.getItems().addItem(20133, 1);
								c.sendMessage("<col=ff0000>You have received full Ancient ceremonial Set!");
								c.yellPoints += 45;
								c.sendMessage("You have received 45 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5066 && price == 12) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(4273, 1);
								c.sendMessage("<col=ff0000>You have received a golden key!");
								c.yellPoints += 12;
								c.sendMessage("You have received 12 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5067 && price == 8) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(6759, 1);
								c.sendMessage("<col=ff0000>You have received a golden chest!");
								c.yellPoints += 8;
								c.sendMessage("You have received 8 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5068 && price == 19) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(6759, 1);
								c.getItems().addItem(4273, 1);
								c.sendMessage("<col=ff0000>You have received a golden chest and a Golden key!");
								c.yellPoints += 19;
								c.sendMessage("You have received 19 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5069 && price == 90) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(6759, 5);
								c.getItems().addItem(4273, 5);
								c.sendMessage("<col=ff0000>You have received some golden chests and some Golden keys!");
								c.yellPoints += 90;
								c.sendMessage("You have received 90 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5070 && price == 35) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(6759, 5);
								c.sendMessage("<col=ff0000>You have received some golden chests!");
								c.yellPoints += 35;
								c.sendMessage("You have received 35 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5071 && price == 55) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(4273, 5);
								c.sendMessage("<col=ff0000>You have received some golden keys!");
								c.yellPoints += 55;
								c.sendMessage("You have received 55 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5072 && price == 650) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30159, 1);
								c.getItems().addItem(30160, 1);
								c.getItems().addItem(30161, 1);
								c.getItems().addItem(30162, 1);
								c.getItems().addItem(30163, 1);
								c.sendMessage("<col=ff0000>You have received full ankou's outfit!");
								c.yellPoints += 650;
								c.sendMessage("You have received 650 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5073 && price == 650) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30138, 1);
								c.getItems().addItem(30139, 1);
								c.getItems().addItem(30140, 1);
								c.getItems().addItem(30141, 1);
								c.getItems().addItem(30142, 1);
								c.sendMessage("<col=ff0000>You have received full samurai's outfit!");
								c.yellPoints += 650;
								c.sendMessage("You have received 650 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5074 && price == 650) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30164, 1);
								c.getItems().addItem(30165, 1);
								c.getItems().addItem(30166, 1);
								c.getItems().addItem(30167, 1);
								c.getItems().addItem(30168, 1);
								c.sendMessage("<col=ff0000>You have received full darkness outfit!");
								c.yellPoints += 650;
								c.sendMessage("You have received 650 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5075 && price == 650) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30169, 1);
								c.getItems().addItem(30170, 1);
								c.getItems().addItem(30171, 1);
								c.getItems().addItem(30172, 1);
								c.getItems().addItem(30173, 1);
								c.sendMessage("<col=ff0000>You have received mummy's outfit!");
								c.yellPoints += 650;
								c.sendMessage("You have received 650 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5076 && price == 900) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(2581, 1);
								c.getItems().addItem(2577, 1);
								c.getItems().addItem(30137, 1);
								c.getItems().addItem(30136, 1);
								c.sendMessage("<col=ff0000>You have received ranger's outfit!");
								c.yellPoints += 900;
								c.sendMessage("You have received 900 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5077 && price == 250) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30155, 1);
								c.getItems().addItem(30156, 1);
								c.getItems().addItem(30157, 1);
								c.sendMessage("<col=ff0000>You have received shayzien house outfit!");
								c.yellPoints += 250;
								c.sendMessage("You have received 250 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5078 && price == 250) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30152, 1);
								c.getItems().addItem(30153, 1);
								c.getItems().addItem(30154, 1);
								c.sendMessage("<col=ff0000>You have received piscarilius house outfit!");
								c.yellPoints += 250;
								c.sendMessage("You have received 250 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5079 && price == 250) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30149, 1);
								c.getItems().addItem(30150, 1);
								c.getItems().addItem(30151, 1);
								c.sendMessage("<col=ff0000>You have received lovakengj house outfit!");
								c.yellPoints += 250;
								c.sendMessage("You have received 250 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5080 && price == 250) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30146, 1);
								c.getItems().addItem(30147, 1);
								c.getItems().addItem(30148, 1);
								c.sendMessage("<col=ff0000>You have received hosidius house outfit!");
								c.yellPoints += 250;
								c.sendMessage("You have received 250 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5081 && price == 250) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30143, 1);
								c.getItems().addItem(30144, 1);
								c.getItems().addItem(30145, 1);
								c.sendMessage("<col=ff0000>You have received arceuus house outfit!");
								c.yellPoints += 250;
								c.sendMessage("You have received 250 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5082 && price == 4) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30174, 1);
								c.sendMessage("<col=ff0000>You have received a super mystery box!");
								c.yellPoints += 4;
								c.sendMessage("You have received 4 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5083 && price == 9) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30175, 1);
								c.sendMessage("<col=ff0000>You have received a legendary mystery box!");
								c.yellPoints += 9;
								c.sendMessage("You have received 9 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5084 && price == 14) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30176, 1);
								c.sendMessage("<col=ff0000>You have received a ultra mystery box!");
								c.yellPoints += 14;
								c.sendMessage("You have received 14 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5085 && price == 24) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30176, 1);
								c.getItems().addItem(30175, 1);
								c.getItems().addItem(30174, 1);
								c.sendMessage("<col=ff0000>You have received super, legendary and ultra mystery boxes!");
								c.yellPoints += 25;
								c.sendMessage("You have received 25 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5086 && price == 18) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30174, 1);
								c.getItems().addItem(30174, 1);
								c.getItems().addItem(30174, 1);
								c.getItems().addItem(30174, 1);
								c.getItems().addItem(30174, 1);
								c.sendMessage("<col=ff0000>You have received 5 super mystery boxes!");
								c.yellPoints += 18;
								c.sendMessage("You have received 18 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5087 && price == 39) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30175, 1);
								c.getItems().addItem(30175, 1);
								c.getItems().addItem(30175, 1);
								c.getItems().addItem(30175, 1);
								c.getItems().addItem(30175, 1);
								c.sendMessage("<col=ff0000>You have received 5 legendary mystery boxes!");
								c.yellPoints += 9;
								c.sendMessage("You have received 9 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5088 && price == 64) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30176, 1);
								c.sendMessage("<col=ff0000>You have received 5 ultra mystery boxes!");
								c.yellPoints += 64;
								c.sendMessage("You have received 64 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5089 && price == 49) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(20061, 1);
								c.getItems().addItem(15360, 1);
								c.sendMessage("<col=ff0000>You have received a random pet box & pet double exp scroll!");
								c.yellPoints += 49;
								c.sendMessage("You have received 49 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							
							if (prod == 5090 && price == 14) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30179, 1);
								c.getItems().addItem(30178, 1);
								c.getItems().addItem(30177, 1);
								c.sendMessage("<col=ff0000>You have received 3 skin color potions!");
								c.yellPoints += 14;
								c.sendMessage("You have received 14 yells. You can yell " + c.yellPoints + " time(s).");
								b = true;
							}
							//more weekly items promo
							if (prod == 5091 && price == 350) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30357, 1);
								c.sendMessage(
										"<col=ff0000>You have received orange partyhat.");
								b = true;
							}
							if (prod == 5092 && price == 550) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30354, 1);
								c.sendMessage("<col=ff0000>You have received Golden Scythe.");
								b = true;
							}
							if (prod == 5093 && price == 450) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30353, 1);
								c.sendMessage(
										"<col=ff0000>You have received golden h'ween mask.");
								b = true;
							}
							if (prod == 5094 && price == 350) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30352, 1);
								c.sendMessage(
										"<col=ff0000>You have received white h'ween mask.");
								b = true;
							}
							if (prod == 5095 && price == 350) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30351, 1);
								c.sendMessage(
										"<col=ff0000>You have received pink h'ween mask.");
								b = true;
							}
							if (prod == 5096 && price == 495) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(15459, 1);
								c.sendMessage(
										"<col=ff0000>You have received defender level 5.");
								b = true;
							}
							if (prod == 5097 && price == 495) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(15464, 1);
								c.sendMessage(
										"<col=ff0000>You have received healer level 5.");
								b = true;
							}
							if (prod == 5098 && price == 495) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(15454, 1);
								c.sendMessage(
										"<col=ff0000>You have received collector level 5.");
								b = true;
							}
							if (prod == 5099 && price == 495) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(15449, 1);
								c.sendMessage(
										"<col=ff0000>You have received attacker level 5.");
								b = true;
							}

							// Xmas event items
							if (prod == 6000 && price == 150) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(11949, 1);
								c.sendMessage(
										"<col=ff0000>You have received Snow globe.");
								b = true;
							}

							if (prod == 6001 && price == 200) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(20083, 1);
								c.sendMessage(
										"<col=ff0000>You have received Golden cracker.");
								b = true;
							}
							if (prod == 6002 && price == 200) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15422, 1);
								c.getItems().addItem(15423, 1);
								c.getItems().addItem(15425, 1);
								c.sendMessage(
										"<col=ff0000>You have received Christmas ghost set.");
								b = true;
							}
							if (prod == 6003 && price == 450) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(15426, 1);
								c.sendMessage(
										"<col=ff0000>You have received Candy cane.");
								b = true;
							}
							if (prod == 6004 && price == 599) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(962, 1);
								c.sendMessage(
										"<col=ff0000>You have received Christmas cracker.");
								b = true;
							}

							if (prod == 6005 && price == 8) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 10, true);
								c.setDonPoints(c.getDonPoints() + 10, true);
								c.sendMessage(
										"<col=ff0000>You have received 10 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											10 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 20;
									c.sendMessage(
											"You have received 2 yells You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							} else if (prod == 6006 && price == 13) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 20, true);
								c.setDonPoints(c.getDonPoints() + 21, true);// 1
																			// year
																			// anniversary
																			// should
																			// be
																			// 21
								c.sendMessage(
										"<col=ff0000>You have received 21 Donator Points!");// 1
																						// year
																						// anniversary
																						// should
																						// be
																						// 21
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											20 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 42;
									c.sendMessage(
											"You have received 4 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							} else if (prod == 6007 && price == 20) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 30, true);
								c.setDonPoints(c.getDonPoints() + 32, true);
								c.sendMessage(
										"<col=ff0000>You have received 32 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											30 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 65;
									c.sendMessage(
											"You have received 6 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							} else if (prod == 6008 && price == 26) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 40, true);
								c.setDonPoints(c.getDonPoints() + 43, true);
								c.sendMessage(
										"<col=ff0000>You have received 43 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											40 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 90;
									c.sendMessage(
											"You have received 8 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 6009 && price == 75) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 100, true);
								c.setDonPoints(c.getDonPoints() + 108, true);
								c.sendMessage(
										"<col=ff0000>You have received 108 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											100 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 225;
									c.sendMessage(
											"You have received 24 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 6010 && price == 330) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 500, true);
								c.setDonPoints(c.getDonPoints() + 545, true);
								c.sendMessage(
										"<col=ff0000>You have received 545 Donator Points!");
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											500 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 1150;
									c.sendMessage(
											"You have received 125 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 6011 && price == 750) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + 1000, true);
								c.setDonPoints(c.getDonPoints() + 1200, true);//
								c.sendMessage(
										"<col=ff0000>You have received 1200 Donator Points!");//
								if (Config.increasedFPPoints > 0) {
									int extra = (int) Math.round(
											1000 * Config.increasedFPPoints);
									c.setDonPoints(c.getDonPoints() + extra,
											true);
									c.yellPoints += 2500;
									c.sendMessage(
											"You have received 270 yells. You can yell "
													+ c.yellPoints
													+ " time(s).");
									c.sendMessage("<col=800000>You receive " + extra
											+ " extra points.");
								}
								b = true;
							}

							if (prod == 6012 && price == 8) {
								c.getItems().addItem(786, 1);
								c.sendMessage(
										"<col=ff0000>You have received 10$ Donator Scroll.");
								b = true;
								PlayerHandler.scrollsBought++;
							}

							if (prod == 6013 && price == 33) {
								c.getItems().addItem(1505, 1);
								c.sendMessage(
										"<col=ff0000>You have received 50$ Donator Scroll.");
								b = true;
								PlayerHandler.scrollsBought++;
							}

							if (prod == 6014 && price == 65) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(2396, 1);
								c.sendMessage(
										"<col=ff0000>You have received 100$ Donator Scroll.");
								b = true;
								PlayerHandler.scrollsBought++;
							}

							if (prod == 6015 && price == 59) {
								c.getItems().addItem(15412, 1);
								c.sendMessage(
										"<col=ff0000>You have received 1 Monster egg.");
								b = true;
							}
							if (prod == 6016 && price == 99) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30342, 1);
								c.getItems().addItem(30343, 1);
								c.getItems().addItem(30344, 1);
								c.getItems().addItem(30345, 1);
								c.getItems().addItem(30346, 1);
								c.sendMessage("<col=ff0000>You have received Antisanta outfit.");
								b = true;
							}
							if (prod == 6017 && price == 9) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30347, 1);
								c.sendMessage("<col=ff0000>You have received a giant present");
								b = true;
							}
							if (prod == 6018 && price == 40) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30347, 5);
								c.sendMessage("<col=ff0000>You have received 5 giant presents");
								b = true;
							}
							if (prod == 6019 && price == 75) {
								c.setDonatedTotalAmount(c.getDonatedTotalAmount() + price, true);
								c.getItems().addItem(30347, 10);
								c.sendMessage("<col=ff0000>You have received 10 giant presents");
								b = true;
							}
							// Xmas event end
						} else if (WorldType.equalsType(WorldType.SPAWN)) {
							if (prod == 200 && price == 10) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(8890, 15000);
								c.sendMessage(
										"<col=ff0000>You have received 15.000 Blood Money!");
								c.yellPoints += 10;
								c.sendMessage(
										"You have received 10 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 201 && price == 25) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(8890, 40000);
								c.sendMessage(
										"<col=ff0000>You have received 40.000 Blood Money!");
								c.yellPoints += 25;
								c.sendMessage(
										"You have received 25 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 202 && price == 50) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(8890, 84000);
								c.sendMessage(
										"<col=ff0000>You have received 85.000 Blood Money!");
								c.yellPoints += 50;
								c.sendMessage(
										"You have received 50 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}

							if (prod == 203 && price == 100) {
								c.setDonatedTotalAmount(
										c.getDonatedTotalAmount() + price,
										true);
								c.getItems().addItem(8890, 175000);
								c.sendMessage(
										"<col=ff0000>You have received 175.000 Blood Money!");
								c.yellPoints += 100;
								c.sendMessage(
										"You have received 100 yells. You can yell "
												+ c.yellPoints + " time(s).");
								b = true;
							}
						}

						if (b) {
							setPlayerRights(c);
							c.sendMessage("Thank you for your donations!");
							c.addDonatedRealTotalAmount(price);
						} else {
							c.sendMessage("You do not have any donations to claim. If donated, give 4 hours.");
						}

						this.stop();
					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private static void setCrownSafe(Player c, ChatCrown crown) {
		ChatCrownsSql.unlockChatCrown(c, crown);
		if (c.isIronMan()) {
			return;
		}

		c.setCrown(crown.ordinal());
	}

	public static void setPlayerRights(Player c) {
		if (c.playerRights == 0 && c.getDonatedTotalAmount() >= 10) {
			c.playerRights = 4;
			Titles.donate10(c);
			setCrownSafe(c, ChatCrown.RED_DONOR);
		}
		if (c.getDonatedTotalAmount() >= 50) {
			if (c.playerRights == 4) {
				c.playerRights = 5;
			}
			Titles.donate50(c);
			setCrownSafe(c, ChatCrown.BLUE_DONOR);
		}
		if (c.getDonatedTotalAmount() >= 250) {
			if (c.playerRights == 5) {
				c.playerRights = 6;
			}
			Titles.donate250(c);
			setCrownSafe(c, ChatCrown.GREEN_DONOR);
		}
		if (c.getDonatedTotalAmount() >= 1000) {
			if (c.playerRights == 6) {
				c.playerRights = 11;
			}
			Titles.donate1000(c);
			setCrownSafe(c, ChatCrown.PURPLE_DONOR);
		}
		if (c.getDonatedTotalAmount() >= 5000) {
			if (c.playerRights == 11) {
				c.playerRights = 12;
			}
			Titles.donate5000(c);
			setCrownSafe(c, ChatCrown.GOLD_DONOR);
		}
		if (c.getDonatedTotalAmount() >= 10000) {
			/*if (c.playerRights == 11) {
				c.playerRights = 12;
			}*/
			setCrownSafe(c, ChatCrown.INSANE);
		}

		InfoTabButtonPlugin.update(c);
	}

}
