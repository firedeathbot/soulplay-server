package com.soulplay.util.sql.clanelo;

public class ClanInfo {

	private String clanTitle;

	private int clanElo;

	private int clanWins;

	private int clanLosses;

	public ClanInfo(String title, int clanElo, int clanWins, int clanLosses) {
		this.clanTitle = title;
		this.clanElo = clanElo;
		this.clanWins = clanWins;
		this.clanLosses = clanLosses;
	}

	public int getClanElo() {
		return clanElo;
	}

	public int getClanLosses() {
		return clanLosses;
	}

	public String getClanTitle() {
		return clanTitle;
	}

	public int getClanWins() {
		return clanWins;
	}

	public void setClanElo(int clanElo) {
		this.clanElo = clanElo;
	}

	public void setClanLosses(int clanLosses) {
		this.clanLosses = clanLosses;
	}

	public void setClanTitle(String clanTitle) {
		this.clanTitle = clanTitle;
	}

	public void setClanWins(int clanWins) {
		this.clanWins = clanWins;
	}

}
