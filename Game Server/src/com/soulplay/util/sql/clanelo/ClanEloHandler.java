package com.soulplay.util.sql.clanelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.EloRatingSystem;
import com.soulplay.util.sql.SqlHandler;

public class ClanEloHandler {

	private static ArrayList<ClanInfo> topClanWarClans = new ArrayList<>();

	public static ArrayList<ClanInfo> getTopClanWarClans() {
		return topClanWarClans;
	}

	public static void reloadClanEloScores() {

		if (!Config.RUN_ON_DEDI) {
			return;
		}

		topClanWarClans.clear();

		// SqlHandler.getSQLService().submit(() -> {
		TaskExecutor.executeSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;

			try {
				connection = SqlHandler.getHikariZaryteTable().getConnection();

				ps = connection.prepareStatement(
						"SELECT clanTitle, clw_elo, clw_wins, clw_losses FROM clans ORDER BY clw_elo DESC LIMIT 50");
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					String title = rs.getString("clanTitle");
					int elo = rs.getInt("clw_elo");
					int wins = rs.getInt("clw_wins");
					int losses = rs.getInt("clw_losses");
					ClanInfo clan = new ClanInfo(title, elo, wins, losses);
					topClanWarClans.add(clan);
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {

				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
		});
	}

	private static boolean setElo(String winningFounder, String losingFounder) {

		Connection connection = null;
		PreparedStatement ps = null;

		int winnerElo = 800;
		int loserElo = 800;
		int winAmount = 0;
		int loseAmount = 0;

		boolean saved = false;

		try {
			connection = SqlHandler.getHikariZaryteTable().getConnection();

			// load winner data
			ps = connection.prepareStatement(
					"SELECT clw_elo, clw_wins FROM clans WHERE founder = ? LIMIT 1");
			ps.setString(1, winningFounder);
			ResultSet results = ps.executeQuery();

			while (results.next()) {
				winnerElo = results.getInt("clw_elo");
				winAmount = results.getInt("clw_wins");
			}
			winAmount++;

			results.close();
			ps.close();

			// load loser data
			ps = connection.prepareStatement(
					"SELECT clw_elo, clw_losses FROM clans WHERE founder = ? LIMIT 1");
			ps.setString(1, losingFounder);
			results = ps.executeQuery();

			while (results.next()) {
				loserElo = results.getInt("clw_elo");
				loseAmount = results.getInt("clw_losses");
			}
			loseAmount++;

			results.close();
			ps.close();

			// System.out.println("winner elo:"+winnerElo+"
			// loserElo:"+loserElo);

			int newWinElo = EloRatingSystem.getNewRating(winnerElo, loserElo,
					EloRatingSystem.WIN);
			int newLoserElo = EloRatingSystem.getNewRating(loserElo, winnerElo,
					EloRatingSystem.LOSE);

			// save winner data
			ps = connection.prepareStatement(
					"UPDATE clans SET clw_elo = ?, clw_wins = ? WHERE founder = '"
							+ winningFounder + "'");
			ps.setInt(1, newWinElo);
			ps.setInt(2, winAmount);
			ps.executeUpdate();

			ps.close();

			// save loser data
			ps = connection.prepareStatement(
					"UPDATE clans SET clw_elo = ?, clw_losses = ? WHERE founder = '"
							+ losingFounder + "'");
			ps.setInt(1, newLoserElo);
			ps.setInt(2, loseAmount);
			ps.executeUpdate();

			ps.close();

			saved = true;

		} catch (SQLException e) {
			e.printStackTrace();
			saved = false;
		} finally {

			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return saved;

	}

	public static void updateClanElo(final String winnerFounder,
			final String loserFounder) {

		// SqlHandler.getSQLService().submit(() -> {
		TaskExecutor.executeSQL(() -> {
			while (!setElo(winnerFounder, loserFounder)) {
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});

	}

}
