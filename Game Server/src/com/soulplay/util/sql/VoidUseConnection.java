package com.soulplay.util.sql;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Jire
 */
@FunctionalInterface
public interface VoidUseConnection extends UseConnection<Void> {
	
	@Override
	default Void used(Connection connection) throws SQLException {
		use(connection);
		return null;
	}
	
	void use(Connection connection) throws SQLException;
	
}
