package com.soulplay.util.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.soulplay.Config;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;

public class Statistics {
	
	private static final String DAILY_UNIQUE_QUERY = "INSERT INTO `zaryte`.`unique_visitors`(`ID`, `world`, `uuid_count`, `ip_count`, `timestamp`) VALUES (NULL, ?, ?, ?, ?)";

	public static void updateDailyUniqueCount(final int uuidCount, final int ipCount) {
		
		TaskExecutor.executeWebsiteSQL(() -> {

			try (Connection con = WebsiteSQL.getHikari().getConnection();
					PreparedStatement ps = con.prepareStatement(DAILY_UNIQUE_QUERY)) {

				ps.setInt(1, Config.NODE_ID);
				ps.setInt(2, uuidCount);
				ps.setInt(3, ipCount);
				ps.setTimestamp(4, Misc.getESTTimestamp());
				ps.executeUpdate();

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		});
	}
}
