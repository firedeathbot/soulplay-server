package com.soulplay.util.sql.configs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.items.Ammunition;
import com.soulplay.content.items.RangeWeapon;
import com.soulplay.content.player.combat.range.BoltEffect;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Priority;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.entity.Location;
import com.soulplay.util.sql.SqlHandler;

public class CombatConfigs {

	
	public static void parse() throws SQLException {
		
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement statement = connection.prepareStatement("SELECT * FROM zaryte.range_configs");
				ResultSet set = statement.executeQuery(); ) {
			
				while (set.next()) {
					parseWeapon(set.getInt(1), set);
				}
				
				
		
		
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement statement = connection.prepareStatement("SELECT * FROM zaryte.ammo_configs");
				ResultSet set = statement.executeQuery(); ) {

			while (set.next()) {
				parseAmmo(set.getInt(1), set);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		for (int i = 4212; i <= 4223; i++) {
			RangeWeapon rangeWeapon = RangeWeapon.get(i);
			if (rangeWeapon == null) {
				continue;
			}

			rangeWeapon.setCombatDistance(10);
		}
	}
	
	
	
	/**
	 * Parses a range weapon from the result set. 
	 * @param itemId The item id.
	 * @param set The result set.
	 * @throws SQLException The exception.
	 */
	private static void parseWeapon(int itemId, ResultSet set) throws SQLException {
		List<Integer> ammunition = new ArrayList<>();
		String[] tokens = set.getString("ammunition").replace(" ", "").split(",");
		for (String s : tokens) {
			ammunition.add(Integer.parseInt(s));
		}

		int speed = 4;
		ItemDefinition itemDef = ItemDefinition.forId(itemId);
		if (itemDef != null) {
			speed = itemDef.getCombatStats().getAttackSpeed();
		}

		RangeWeapon weapon = new RangeWeapon(itemId, new Animation(Priority.HIGH, set.getInt("animation")), speed, set.getInt("ammo_slot"), set.getInt("weapon_type"), set.getBoolean("drop_ammo"), ammunition);
		weapon.setCombatDistance(ItemConstants.getRangeDistReqDump(itemId));
		RangeWeapon.getRangeWeapons().put(itemId, weapon);
	}

	/**
	 * Parses range ammunition from the result set.
	 * @param itemId The item id.
	 * @param set The result set.
	 * @throws SQLException The SQL Exception.
	 */
	private static void parseAmmo(int itemId, ResultSet set) throws SQLException {
		String[] startGfx = set.getString("start_graphic").split(",");
		int graphicId = Integer.parseInt(startGfx[0]);
		int gfxHeight = Integer.parseInt(startGfx[1]);
		Graphic start = new Graphic(Priority.HIGH, graphicId, 0, gfxHeight == 0 ? GraphicType.LOW : GraphicType.BOWARROWPULL);
		Graphic darkBow = null;
		String dbowStr = set.getString("darkbow_graphic");
		if (!dbowStr.equals("")) {
			String[] tok = dbowStr.split(",");
			int graphicId2 = Integer.parseInt(tok[0]);
			int gfxHeight2 = Integer.parseInt(tok[1]);
			darkBow = new Graphic(Priority.HIGH, graphicId2, 0, gfxHeight2 == 0 ? GraphicType.LOW : GraphicType.BOWARROWPULL);
		}
		String[] p = set.getString("projectile").split(",");
		int gfxId = Integer.parseInt(p[0]);				// 15
		int startHeight = Integer.parseInt(p[1]);		// 40
		int endHeight = Integer.parseInt(p[2]);			// 36
		int startDelay = Integer.parseInt(p[3]);		// 41 // 33
		int endDelay = Integer.parseInt(p[4]);				// 46 // 33
		int angle = Integer.parseInt(p[5]);				// 5
		int distanceOffset = Integer.parseInt(p[6]);
		Projectile proj = new Projectile(gfxId, Location.create(3333, 3333), Location.create(3333, 3333));
		proj.setStartHeight(startHeight);
		proj.setEndHeight(endHeight);
		proj.setStartDelay(startDelay);
		proj.setEndDelay(endDelay); // this varries with distance also
		proj.setSlope(angle);
		proj.setStartOffset(distanceOffset);
		Ammunition ammo = new Ammunition(itemId, start, darkBow, proj, set.getInt("poison_damage"));
		BoltEffect effect = BoltEffect.forId(itemId);
		if (effect != null) {
			ammo.setEffect(effect);
		}
		Ammunition.getAmmunition().put(itemId, ammo);
	}
	
	
	public static void reloadConfigs() {
		RangeWeapon.getRangeWeapons().clear();
		Ammunition.getAmmunition().clear();
		
		try {
			CombatConfigs.parse();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
