package com.soulplay.util.sql.configs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.content.combat.CombatStats;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPCDefinitions;
import com.soulplay.util.sql.SqlHandler;

public class NpcConfigs {

	public static void parse() throws SQLException {

		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement statement = connection.prepareStatement("SELECT * FROM `zaryte`.`npc_def`");
				ResultSet set = statement.executeQuery(); ) {

			while (set.next()) {
				parseNpc(set.getInt(1), set);
			}




		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	private static void parseNpc(int npcId, ResultSet set) throws SQLException {
		NPCDefinitions def = NPCDefinitions.getDefinitions()[npcId];
		if (def == null)
			def = NPCDefinitions.getDefinitions()[npcId] = new NPCDefinitions(npcId);
		
		def.setSkills(new Skills(null));
		def.setStats(new CombatStats());
		
		def.setNpcName(set.getString(2));
		def.setNpcCombat(set.getInt(3));
		def.setNpcHealth(set.getInt(4));
		def.setAggressive(set.getBoolean(5));
		def.setPoisonous(set.getBoolean(6));
		def.setVenomous(set.getBoolean(7));
		def.loadWeakness(set.getString(8));
		def.getSkills().setStaticLevel(Skills.ATTACK, set.getInt(9));
		def.getSkills().setStaticLevel(Skills.STRENGTH, set.getInt(10));
		def.getSkills().setStaticLevel(Skills.DEFENSE, set.getInt(11));
		def.getSkills().setStaticLevel(Skills.MAGIC, set.getInt(12));
		def.getSkills().setStaticLevel(Skills.RANGED, set.getInt(13));
		def.getStats().setStabBonus(set.getInt(14));
		def.getStats().setSlashBonus(set.getInt(15));
		def.getStats().setCrushBonus(set.getInt(16));
		def.getStats().setMagicAccBonus(set.getInt(17));
		def.getStats().setRangedAccBonus(set.getInt(18));
		def.getStats().setStabDefBonus(set.getInt(19));
		def.getStats().setSlashDefBonus(set.getInt(20));
		def.getStats().setCrushDefBonus(set.getInt(21));
		def.getStats().setMagicDefBonus(set.getInt(22));
		def.getStats().setRangedDefBonus(set.getInt(23));
		def.getStats().setStrengthBonus(set.getInt(24));
		def.getStats().setRangeStrengthBonus(set.getInt(25));
		def.getStats().setMagicStrengthBonus(set.getInt(26));
		def.getStats().setAttackStrength(set.getInt(27));
		def.getStats().setAttackSpeed(set.getInt(28));
		def.setPoisonImmune(set.getBoolean(29));
		def.setVenomImmune(set.getBoolean(30));
		
	}
	
}
