package com.soulplay.util.sql.upload;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.cache.EntityDef;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.npc.DropTable;
import com.soulplay.game.model.npc.DropTableEntry;
import com.soulplay.game.model.npc.DropTableItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.sql.WebsiteSQL;

public class NpcDropTableSql {

	private static final String TRUNCATE_TABLE = "TRUNCATE zaryte.drop_table";
	
	public static void uploadToDb(Player p) {
		
		TaskExecutor.executeWebsiteSQL(new Runnable() {

			@Override
			public void run() {


				System.out.println("----------- Starting upload of drop table -------------");
				EntityDef def = EntityDef.forID(0);
				try (Connection con = WebsiteSQL.getHikari().getConnection();
						PreparedStatement ps = con.prepareStatement(TRUNCATE_TABLE)) {
					ps.execute();
					ps.close();

					for (int i = 0; i < Config.MAX_NPC_ID; i++) {
						DropTableEntry entry = DropTable.getEntry(i);
						if (entry != null) {
							def = EntityDef.forID(i);
							if (def == null) {
								System.out.println("Something went wrong! NPC DEF NULL FOR ID:" + i);
								continue;
							}
							for (DropTableItem item : entry.getItems()) {
								if (item != null) {
									addColumn(con, i, def.getName(), item.getItemId(), ItemProjectInsanity.getItemName(item.getItemId()), item.getMinAmount(), item.getMaxAmount(), item.getRawProbability());
								} else {
									System.out.println("item is null on drop table NPCID:"+i);
								}
							}
						}
					}

				} catch (SQLException e) {
					e.printStackTrace();
					
					if (p != null) {
						Server.getTaskScheduler().schedule(new Task() {
							
							@Override
							protected void execute() {
								this.stop();
								if (!p.disconnected) {
									p.sendMessage("Something went wrong. contact Java developers.");
								}
							}
						});
					}
				}

				if (p != null) {
					Server.getTaskScheduler().schedule(new Task() {
						
						@Override
						protected void execute() {
							this.stop();
							if (!p.disconnected) {
								p.sendMessage("Uploaded NPC drop table to database.");
							}
						}
					});
				}
				
				System.out.println("----------- Finished upload of drop table -------------");


			}
		});
		
	}
	
	private static final String QUERY = "INSERT INTO `zaryte`.`drop_table`(`npc_id`, `npc_name`, `item_id`, `item_name`, `min`, `max`, `chance`) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `npc_id` = VALUES(`npc_id`), `npc_name` = VALUES(`npc_name`), `item_id` = VALUES(`item_id`), `item_name` = VALUES(`item_name`), `min` = VALUES(`min`), `max` = VALUES(`max`), `chance` = VALUES(`chance`)";
	
	private static void addColumn(Connection con, int npcId, String npcName, int itemId, String itemName, int min, int max, int chance) {
		try (PreparedStatement ps = con.prepareStatement(QUERY);) {
			ps.setInt(1, npcId);
			ps.setString(2, npcName);
			ps.setInt(3, itemId);
			ps.setString(4, itemName);
			ps.setInt(5, min);
			ps.setInt(6, max);
			ps.setInt(7, chance);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
