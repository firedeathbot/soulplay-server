package com.soulplay.util.sql;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.soulplay.DBConfig;
import com.zaxxer.hikari.HikariDataSource;

public class WebsiteSQL {

	/**
	 * Hikari stuff
	 */

	private static HikariDataSource hikari;

	private static final Logger LOG = Logger
			.getLogger(DBConfig.class.getName());

	private static Properties props = new Properties();

	public static void connectToDatabase() {
		String address = props.getProperty("host");
		String port = props.getProperty("port");
		String name = props.getProperty("name");
		String username = props.getProperty("username");
		String password = props.getProperty("password");

		hikari = new HikariDataSource();
		hikari.setMaximumPoolSize(10);
		hikari.setDataSourceClassName(
				"com.mysql.cj.jdbc.MysqlDataSource");
		hikari.addDataSourceProperty("serverName", address);
		hikari.addDataSourceProperty("port", port);
		hikari.addDataSourceProperty("databaseName", name);
		hikari.addDataSourceProperty("user", username);
		hikari.addDataSourceProperty("password", password);
	}

	public static HikariDataSource getHikari() {
		return hikari;
	}

	public static void init() throws Exception {
		LOG.log(Level.INFO,
				"initiating hiscores database config connection...");
		try {
			FileInputStream fis = new FileInputStream("./Data/hiscoresdb.xml");
			props.loadFromXML(fis);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "error loading database properties", e);
			throw new Exception("error loading database properties");
		}
		connectToDatabase();
	}

}
