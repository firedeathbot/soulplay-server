package com.soulplay.util.sql;

import com.soulplay.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Jire
 */
public final class SQL {
	
	private static final int POOL_SIZE = Runtime.getRuntime().availableProcessors() + 1;
	private static final int LEAK_DETECTION_THRESHOLD = 10_000;
	private static final int CONNECTION_TIMEOUT = 5_000;
	
	public static HikariConfig getDefaultConfig() {
		HikariConfig config = new HikariConfig();
		
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "500");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		config.addDataSourceProperty("useServerPrepStmts", "true");
		
		config.addDataSourceProperty("useLocalSessionState", "true");
		config.addDataSourceProperty("useLocalTransactionState", "true");
		config.addDataSourceProperty("rewriteBatchedStatements", "true");
		config.addDataSourceProperty("cacheResultSetMetadata", "true");
		config.addDataSourceProperty("cacheServerConfiguration", "true");
		config.addDataSourceProperty("elideSetAutoCommits", "true");
		config.addDataSourceProperty("maintainTimeStats", "false");
		
		config.setLeakDetectionThreshold(LEAK_DETECTION_THRESHOLD);
		config.setConnectionTimeout(CONNECTION_TIMEOUT);
		config.setMaximumPoolSize(POOL_SIZE);
		
		return config;
	}
	
	public static HikariDataSource buildDataSource(String jdbcURL, String username, String password) {
		HikariConfig config = getDefaultConfig();
		config.setJdbcUrl(jdbcURL);
		config.setUsername(username);
		config.setPassword(password);
		return new HikariDataSource(config);
	}
	
	public static HikariDataSource buildDataSource(String databaseHost, int databasePort, String databaseName,
	                                               String username, String password) {
		return buildDataSource("jdbc:mysql://" + databaseHost + ":" + databasePort
						+ "/" + databaseName + Config.MYSQL_DB_EXTENSIONS,
				username,
				password);
	}
	
	public static void initializeMySQLDriver() throws ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
	}
	
	public static Connection getConnection(HikariDataSource dataSource) {
		if (dataSource == null) {
			return null;
		}
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean useConnection(HikariDataSource dataSource, VoidUseConnection useConnection) {
		Connection connection = getConnection(dataSource);
		if (connection == null) {
			return false;
		}
		try {
			useConnection.used(connection);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			closeConnection(connection);
		}
	}
	
	public static <R> R useConnection(HikariDataSource dataSource, UseConnection<R> useConnection) {
		Connection connection = getConnection(dataSource);
		if (connection == null) {
			return null;
		}
		try {
			return useConnection.used(connection);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			closeConnection(connection);
		}
	}
	
	public static boolean closeConnection(Connection connection) {
		if (connection == null) {
			return false;
		}
		try {
			connection.close();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	
}
