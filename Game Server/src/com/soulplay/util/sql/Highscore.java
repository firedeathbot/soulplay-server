package com.soulplay.util.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.task.TaskExecutor;

public class Highscore {

	private static String TOPPVM = "";

	// private static ExecutorService hiscoreService =
	// Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	private static String updateStatement = "UPDATE `zaryte`.`highscores` SET `playername` = ?, `player_rights` = ?, `player_difficulty` = ?, `total_lvl`=?, `attack_lvl`=?, `attack_exp`=?, `defence_lvl`=?, `defence_exp`=?, `strength_lvl`=?, `strength_exp`=?, `hitpoints_lvl`=?, `hitpoints_exp`=?, `range_lvl`=?, `range_exp`=?, `prayer_lvl`=?, `prayer_exp`=?, `magic_lvl`=?, `magic_exp`=?, `cooking_lvl`=?, `cooking_exp`=?, `woodcutting_lvl`=?, `woodcutting_exp`=?, `fletching_lvl`=?, `fletching_exp`=?, `fishing_lvl`=?, `fishing_exp`=?, `firemaking_lvl`=?, `firemaking_exp`=?, `crafting_lvl`=?, `crafting_exp`=?, `smithing_lvl`=?, `smithing_exp`=?, `mining_lvl`=?, `mining_exp`=?, `herblore_lvl`=?, `herblore_exp`=?, `agility_lvl`=?, `agility_exp`=?, `thieving_lvl`=?, `thieving_exp`=?, `slayer_lvl`=?, `slayer_exp`=?, `farming_lvl`=?, `farming_exp`=?, `runecrafting_lvl`=?, `runecrafting_exp`=?, `dungeoneering_lvl`=?, `dungeoneering_exp`=?, `hunter_lvl`=?, `hunter_exp`=?, `summoning_lvl`=?, `summoning_exp`=?, `construction_lvl` =?, `construction_exp`=? WHERE `player_id`=?;";

	public static void createAndUpdateHighscoresEntry(int playerID,
			final Client client) {
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			return;
		}
		TaskExecutor.executeWebsiteSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;
			try {

				connection = WebsiteSQL.getHikari().getConnection();
				int amount = 0;
				ps = connection.prepareStatement(
						"SELECT `playername` from `highscores` WHERE `player_id` = ?");
				ps.setInt(1, playerID);
				ResultSet result = ps.executeQuery();
				while (result.next()) {
					amount++;
					break;
				}
				ps.close();

				if (amount > 0) {
					Highscore.updateHighscore(client);
				} else {
					Highscore.createHighscoreEntryThenUpdate(playerID, client);
				}
			} catch (Exception e1) {
				Server.getSlackSession().sendMessage(
						Server.getSlackSession()
								.findChannelByName("exceptions"),
						com.soulplay.net.slack.SlackMessageBuilder
								.buildExceptionMessageNew(Highscore.class, e1));
			} finally {

				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}

		});
	}

	public static void createHighscoreEntryThenUpdate(int mysqlIndex,
			final Client client) throws SQLException {
		// hiscoreService.submit(() -> {

		Connection connection = null;

		try {

			connection = WebsiteSQL.getHikari().getConnection();
			connection.createStatement().executeUpdate(
					"INSERT INTO `highscores` (player_id, playername) values ('"
							+ mysqlIndex + "' , '" + client.getNameSmartUp() + "')");
			connection.close();
			Highscore.updateHighscore(client);
		} catch (Exception e) {
			Server.getSlackSession().sendMessage(
					Server.getSlackSession().findChannelByName("exceptions"),
					com.soulplay.net.slack.SlackMessageBuilder
							.buildExceptionMessageNew(Highscore.class, e));
		} finally {

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		// });
	}

	public static void generateTopPvM() {
		if (Config.SERVER_DEBUG || !Config.RUN_ON_DEDI) {
			return;
			// TaskExecutor.executeSQL(new Runnable() {
			// //hiscoreService.submit(()
			// -> {
			// @Override
			// public void run() {
		}

		Connection connection = null;
		PreparedStatement ps = null;
		try {

			String pvmer = "test";
			connection = Server.getConnection();
			ps = connection.prepareStatement(
					"SELECT PlayerName, npcKills FROM `accounts` ORDER BY `accounts`.`npcKills` DESC LIMIT 1");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				pvmer = rs.getString("PlayerName");
				break;
			}
			rs.close();

			setTopPvmer(pvmer);

		} catch (SQLException e) {
			Server.getSlackSession().sendMessage(
					Server.getSlackSession().findChannelByName("exceptions"),
					com.soulplay.net.slack.SlackMessageBuilder
							.buildExceptionMessageNew(Highscore.class, e));
		} finally {

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		// }
		// });
	}

	public static synchronized String getTopPvmer() {
		return TOPPVM;
	}

	public static synchronized void setTopPvmer(String name) {
		TOPPVM = name;
	}

	public static void updateHighscore(final Client cl) {
		// hiscoreService.submit(() -> {

		try (Connection connection = WebsiteSQL.getHikari().getConnection();
				PreparedStatement ps = connection.prepareStatement(updateStatement);){


			ps.setString(1, cl.getNameSmartUp());
			ps.setInt(2, cl.playerRights);
			ps.setInt(3, cl.difficulty);
			ps.setInt(4, cl.getPA().getTotalLevel());
			int psIndex = 5;
			for (int skill = 0; skill < cl.getSkills().getExperienceArray().length; skill++) {
				ps.setInt(psIndex++, cl.getSkills().getStaticLevel(skill));
				ps.setInt(psIndex++, cl.getSkills().getExperience(skill));
			}
			ps.setInt(psIndex, cl.mySQLIndex);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			// Server.getSlackSession().sendMessage(Server.getSlackSession().findChannelByName("exceptions"),
			// server.net.slack.SlackMessageBuilder.buildExceptionMessageNew(Highscore.class,
			// e));
		}
		// });
	}
}
