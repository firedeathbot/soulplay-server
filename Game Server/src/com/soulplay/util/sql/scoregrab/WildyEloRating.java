package com.soulplay.util.sql.scoregrab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.EloRatingSystem;

public class WildyEloRating {

	private static ArrayList<WildyRecord> wildyScores = new ArrayList<>();

	public static ArrayList<WildyRecord> getWildyScores() {
		return wildyScores;
	}

	public static void reloadWildyEloScores() {

		if (!Config.RUN_ON_DEDI) {
			return;
		}

		wildyScores.clear();

		TaskExecutor.executeSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;

			try {
				connection = Server.getConnection();

				ps = connection.prepareStatement(
						"SELECT `PlayerName`, `display_name`, `lastDayPkedOfYear`, `wildyPlayerKillCount`, `wildyPlayerDeathCount`, `wildyEloRating` FROM `accounts` WHERE TIMESTAMPDIFF(day,last_pk_timestamp,NOW()) < 7 ORDER BY wildyEloRating DESC LIMIT 50");
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					String playerName = rs.getString("PlayerName");
					String displayName = rs.getString("display_name");
					int lastDayPked = rs.getInt("lastDayPkedOfYear");
					int kills = rs.getInt("wildyPlayerKillCount");
					int deaths = rs.getInt("wildyPlayerDeathCount");
					int elo = rs.getInt("wildyEloRating");
					double kdr = RSConstants.getKDR(kills, deaths);
					final String nameSmart = (displayName == null || displayName.equals("") ? playerName : displayName);
					WildyRecord record = new WildyRecord(nameSmart, kills,
							deaths, kdr, elo, lastDayPked);
					wildyScores.add(record);
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {

				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
		});
	}
	
	public static void resetWildyEloForAll() {
		WildyEloRating.getWildyScores().clear();
		for (Player p : PlayerHandler.players) { // reset online first
			if (p == null || p.disconnected) {
				continue;
			}
			p.setEloRating(EloRatingSystem.DEFAULT_ELO_START_RATING, true);
		}
		PlayerSaveSql.resetEloRating(EloRatingSystem.DEFAULT_ELO_START_RATING,
				(WorldType.equalsType(WorldType.SPAWN) ? Config.MYSQL_PLAYER_TABLE_PVP : Config.MYSQL_PLAYER_TABLE));
	}

}
