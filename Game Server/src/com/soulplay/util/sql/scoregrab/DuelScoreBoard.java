package com.soulplay.util.sql.scoregrab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.RSConstants;
import com.soulplay.game.task.TaskExecutor;

public class DuelScoreBoard {

	private static ArrayList<DuelScore> duelScores = new ArrayList<>();

	public static ArrayList<DuelScore> getDuelScores() {
		return duelScores;
	}

	public static void reloadDuelScores() {

		if (!Config.RUN_ON_DEDI) {
			return;
		}

		duelScores.clear();

		TaskExecutor.executeSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;

			try {
				connection = Server.getConnection();

				ps = connection.prepareStatement(
						"SELECT `PlayerName`, `display_name`, `duel_wins`, `duel_losses`, `wildyEloRating` FROM `accounts` ORDER BY duel_wins DESC LIMIT 50");
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					String playerName = rs.getString("PlayerName");
					String displayName = rs.getString("display_name");
					int kills = rs.getInt("duel_wins");
					int deaths = rs.getInt("duel_losses");
					int elo = rs.getInt("wildyEloRating");
					double kdr = RSConstants.getKDR(kills, deaths);
					final String nameSmart = (displayName == null || displayName.equals("") ? playerName : displayName);
					DuelScore record = new DuelScore(nameSmart, kills, deaths,
							kdr, elo);
					duelScores.add(record);
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {

				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
		});
	}

}
