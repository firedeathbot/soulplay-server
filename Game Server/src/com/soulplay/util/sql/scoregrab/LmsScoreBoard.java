package com.soulplay.util.sql.scoregrab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.content.player.namechange.NameChange;
import com.soulplay.game.task.TaskExecutor;

public class LmsScoreBoard {

	private static ArrayList<LmsScore> scoreboard = new ArrayList<>();

	public static ArrayList<LmsScore> getLmsScores() {
		return scoreboard;
	}

	public static void reloadDuelScores() {

		if (!Config.RUN_ON_DEDI) {
			return;
		}

		scoreboard.clear();

		TaskExecutor.executeSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;

			try {
				connection = Server.getConnection();

				ps = connection.prepareStatement(
						"SELECT `player_id`, `lms_wins`, `lms_win_rating`, `lms_kills`, `lms_kill_rating` FROM `players`.`extra_hiscores` ORDER BY `lms_win_rating` DESC LIMIT 50");
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					int mID = rs.getInt("player_id");
					int wins = rs.getInt("lms_wins");
					int winRating = rs.getInt("lms_win_rating");
					int kills = rs.getInt("lms_kills");
					int killRating = rs.getInt("lms_kill_rating");
					String displayName = NameChange.getNameSmartFromDB(mID);
					LmsScore record = new LmsScore(displayName, wins, winRating, kills, killRating);
					scoreboard.add(record);
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {

				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
		});
	}
}
