package com.soulplay.util.sql.scoregrab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Deque;
import java.util.LinkedList;
import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.RSConstants;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.game.model.player.info.SeasonalWildScore;
import com.soulplay.game.model.player.save.ChatCrownsSql;
import com.soulplay.game.model.player.save.PlayerSaveSql;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.EloRatingSystem;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

public class WildyEloRatingSeason {

	private static ArrayList<WildyRecord> wildyScores = new ArrayList<>();

	public static ArrayList<WildyRecord> getWildyScores() {
		return wildyScores;
	}
	
	private static volatile boolean resetInProgress;

	private static String SELECT_QUERY = "SELECT s.elo, s.kill_count, s.death_count,"
			+ " acc.PlayerName, acc.display_name FROM seasonal_pk s "
			+ "LEFT JOIN accounts acc ON s.player_id = acc.ID ORDER BY elo DESC LIMIT 50";
	
	public static void reloadWildyEloScores() {

		if (!Config.RUN_ON_DEDI) {
			return;
		}

		wildyScores.clear();

		TaskExecutor.executeSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;

			try {
				connection = Server.getConnection();
				
				ps = connection.prepareStatement(SELECT_QUERY);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {

					final int elo = rs.getInt("elo");
					final int kills = rs.getInt("kill_count");
					final int deaths = rs.getInt("death_count");
					final String displayName = rs.getString("display_name");
					double kdr = RSConstants.getKDR(kills, deaths);
					WildyRecord record = new WildyRecord(displayName, kills,
							deaths, kdr, elo, 0);
					wildyScores.add(record);
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {

				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
		});
	}
	
	public static void resetWildyEloForAll() {
		wildyScores.clear();
		for (Player p : PlayerHandler.players) { // reset online first
			if (p == null || p.disconnected) {
				continue;
			}
			p.resetSeasonWildScore();
		}
		PlayerSaveSql.truncateSeasonalEloRating();
	}
	
	private static double minDiff = 100;
	private static double maxDiff = 550;
	
	public static void applyKill(Player winner, Player loser) {
		
		if (resetInProgress) {
			return;
		}
		
		SeasonalWildScore winnerScore = winner.getSeasonWildScore();
		SeasonalWildScore loserScore = loser.getSeasonWildScore();
		
		winnerScore.setKillCount(winnerScore.getKillCount() + 1);
		
		loserScore.setDeathCount(loserScore.getDeathCount() + 1);
		
		final int prevWinnerElo = winnerScore.getEloRating();
		final int prevLoserElo = loserScore.getEloRating();
		
		final boolean bothDead = winner.isDead() && loser.isDead();
		
		
		double winDouble = EloRatingSystem.GWIN;
		double lossDouble = EloRatingSystem.GLOSS;
		
		int difference = Math.abs(winner.getTotalBonuses() - loser.getTotalBonuses());
		if (difference > 1600) {
			double diff = Math.abs(winner.getTotalBonuses() - loser.getTotalBonuses());
			winDouble = Misc.interpolateDouble(EloRatingSystem.GWIN, EloRatingSystem.GDRAW, minDiff, maxDiff, diff);
			lossDouble = Misc.interpolateDouble(EloRatingSystem.GLOSS, EloRatingSystem.GDRAW, minDiff, maxDiff, diff);
		}
				
		int newWinnerRating = EloRatingSystem.getNewRating(prevWinnerElo, prevLoserElo, bothDead ? EloRatingSystem.GDRAW : winDouble);
		
		if (winnerScore.getEloRating() < newWinnerRating) // prevent from getting negative elo
			winnerScore.setEloRating(newWinnerRating);
		loserScore.setEloRating(EloRatingSystem.getNewRating(prevLoserElo, prevWinnerElo, bothDead ? EloRatingSystem.GDRAW : lossDouble));
		
		if (!ChatCrown.isCrownUnlocked(winner, ChatCrown.PK_DRAGON) && winnerScore.getEloRating() >= 2000)
			ChatCrownsSql.unlockChatCrown(winner, ChatCrown.PK_DRAGON);
		if (!ChatCrown.isCrownUnlocked(winner, ChatCrown.PK_RUNITE) && winnerScore.getEloRating() >= 1800)
			ChatCrownsSql.unlockChatCrown(winner, ChatCrown.PK_RUNITE);
		if (!ChatCrown.isCrownUnlocked(winner, ChatCrown.PK_ADAMANT) && winnerScore.getEloRating() >= 1600)
			ChatCrownsSql.unlockChatCrown(winner, ChatCrown.PK_ADAMANT);
		if (!ChatCrown.isCrownUnlocked(winner, ChatCrown.PK_STEEL) && winnerScore.getEloRating() >= 1400)
			ChatCrownsSql.unlockChatCrown(winner, ChatCrown.PK_STEEL);
		if (!ChatCrown.isCrownUnlocked(winner, ChatCrown.PK_BRONZE) && winnerScore.getEloRating() >= 1200)
			ChatCrownsSql.unlockChatCrown(winner, ChatCrown.PK_BRONZE);
		
		winner.sendMessage(String.format("You gained %d seasonal elo points.", winnerScore.getEloRating() - prevWinnerElo));
		loser.sendMessage(String.format("You lost %d seasonal elo points.", loserScore.getEloRating() - prevLoserElo));
		winner.getPA().loadQuests();
		loser.getPA().loadQuests();

		PlayerSaveSql.saveSeasonaScoreThreaded(winner);
		PlayerSaveSql.saveSeasonaScoreThreaded(loser);
	}
	
	public static void rewardAndReset(boolean forceUpdate) {
		int month = Server.getCalendar().currentMonth();
		int dayOfMonth = Server.getCalendar().currentDayOfMonth();
		
		if (!forceUpdate && dayOfMonth != 1) {
			return;
		}
		
		switch (month) {
		case Calendar.JANUARY:
		case Calendar.MAY:
		case Calendar.SEPTEMBER:
			rewardPlayers();
			break;
			default:
				if (forceUpdate)
					rewardPlayers();
				break;
		}
		
	}
	
	private static String SELECT_PKERS_REWARDS = "SELECT `player_id`, `item_id`, `amount` FROM `players`.`seasonal_pk_rewards` WHERE `player_id` = ?";
	
	public static void claimReward(Player p) {
		p.sendMessage("Attempting to claim Seasonal PK Rewards.");
		
		final int playerId = p.mySQLIndex;
		
		TaskExecutor.executeSQL(() -> {
			
			if (p.disconnected)
				return;
			
			try (Connection con = SqlHandler.getHikariZaryteTable().getConnection();
					PreparedStatement ps = con.prepareStatement(SELECT_PKERS_REWARDS);) {
				
				ps.setInt(1, playerId);
				
				Deque<GameItem> list = new LinkedList<>();
				
				try (ResultSet rs = ps.executeQuery()) {

					while (rs.next()) {
						int itemId = rs.getInt("item_id");
						int amount = rs.getInt("amount");

						GameItem item = new GameItem(itemId, amount);

						list.add(item);
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				if (!list.isEmpty()) {
					Server.getTaskScheduler().schedule(()-> {
						if (!p.isActive)
							return;

						list.stream().forEach(I -> p.getItems().addOrDrop(I));
					});
					clearRewards(playerId, con);
				}
				
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		});
	}
	
	private static String DELETE_REWARDS = "DELETE FROM `players`.`seasonal_pk_rewards` WHERE player_id = ?";
	
	private static void clearRewards(int playerId, Connection con) {
		try (PreparedStatement ps = con.prepareStatement(DELETE_REWARDS);) {
			ps.setInt(1, playerId);
			ps.executeUpdate();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	private static String SELECT_PKERS_Q = "SELECT `player_id`, `elo` FROM `players`.`seasonal_pk` WHERE `elo` >= 1200 ORDER BY `seasonal_pk`.`elo` DESC";
	
	private static void rewardPlayers() {
		resetInProgress = true;
		
		TaskExecutor.executeSQL(() -> {
			
			int placement = 1;
			
			try (Connection con = SqlHandler.getHikariZaryteTable().getConnection();
					PreparedStatement ps = con.prepareStatement(SELECT_PKERS_Q);
					ResultSet rs = ps.executeQuery();) {
				
				while (rs.next()) {
					long playerId = rs.getLong("player_id");
					int elo = rs.getInt("elo");
					
					int amount = 50_000_000;
					
					if (elo >= 2000)
						amount += 35_000_000;
					if (elo >= 1800)
						amount += 35_000_000;
					if (elo >= 1600)
						amount += 35_000_000;
					if (elo >= 1400)
						amount += 35_000_000;
					
					insertReward(playerId, 995, amount, con);
					
					switch (placement) {
					case 1:
						insertReward(playerId, 30367, 25_000_000, con); // osrs coin
						insertReward(playerId, 2396, 1, con); // 100$ scroll
						insertReward(playerId, 30057, 1, con); // lava dragon mask
						break;
					case 2:
						insertReward(playerId, 30367, 5_000_000, con); // osrs coin
						insertReward(playerId, 1505, 1, con); // 50$ scroll
						break;
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
						insertReward(playerId, 30367, 1_000_000, con); // osrs coin
						insertReward(playerId, 786, 1, con); // 10$ scroll
						break;
					}
					placement++;
				}
				
				
			} catch (SQLException e1) {
				e1.printStackTrace();
				resetInProgress = false;
				return;
			}
			
			Server.getTaskScheduler().schedule(()-> {
				resetWildyEloForAll();
				resetInProgress = false;
			});
			
		});
	}
	
	private static final String INSERT_REWARD = "INSERT INTO `players`.`seasonal_pk_rewards` (player_id, item_id, amount) VALUES(?, ?, ?)";
	
	private static void insertReward(long id, int itemId, int amount, Connection con) {
		
		if (con == null)
			try {
				con = SqlHandler.getHikariZaryteTable().getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
		
		try (PreparedStatement ps = con.prepareStatement(INSERT_REWARD);) {
			ps.setLong(1, id);
			ps.setInt(2, itemId);
			ps.setInt(3, amount);
			ps.executeUpdate();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	private static final String BOARDS_TITLE1 = "PKing Seasonal Ranking - Resets on Jan/May/Sep";//Config.SERVER_NAME + " Seasonal Top Pkers";
	private static final String BOARDS_TITLE2 = "PKing Seasonal Ranking";
	
	public static void openPKEloScoreboard(Player c) {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(BOARDS_TITLE2,
				6399);
		c.getPacketSender().sendFrame126(BOARDS_TITLE1,
				6400);

		int count = 0;
		
		final int scoreSize = wildyScores.size();
		
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < scoreSize) {
				WildyRecord r = wildyScores.get(count);
				if (r == null) {
					count++;
					continue;
				}
				c.getPacketSender()
						.sendFrame126((count + 1) + ": " + r.getPlayerName()
								+ "  -  ELO: " + r.getElo() + "  -  KDR: "
								+ r.getKdr() + "  -  Kills: " + r.getKills()
								+ "  -  Deaths: " + r.getDeaths(), i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}

			count++;
		}

	}

}
