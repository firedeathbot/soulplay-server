package com.soulplay.util.sql.scoregrab;

public class DuelScore {

	private String playerName;

	private int kills, deaths, elo;

	private double kdr;

	public DuelScore(String userName, int kills, int deaths, double kdr,
			int elo) {
		this.setPlayerName(userName);
		this.setKills(kills);
		this.setDeaths(deaths);
		this.setKdr(kdr);
		this.setElo(elo);
	}

	public int getDeaths() {
		return deaths;
	}

	public int getElo() {
		return elo;
	}

	public double getKdr() {
		return kdr;
	}

	public int getKills() {
		return kills;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public void setElo(int elo) {
		this.elo = elo;
	}

	public void setKdr(double kdr) {
		this.kdr = kdr;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
}
