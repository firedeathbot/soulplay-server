package com.soulplay.util.sql.scoregrab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.task.TaskExecutor;

public class ClwRedPortalScoreBoard {

	private static ArrayList<WildyRecord> scores = new ArrayList<>();

	public static ArrayList<WildyRecord> getScores() {
		return scores;
	}
	
	private static String SELECT_QUERY = "SELECT s.clw_red_portal_kills, "
			+ "acc.display_name FROM extra_hiscores s "
			+ "LEFT JOIN accounts acc ON s.player_id = acc.ID WHERE clw_red_portal_kills > 0 ORDER BY clw_red_portal_kills DESC LIMIT 50";
	
	public static void reloadScores() {

		if (!Config.RUN_ON_DEDI) {
			return;
		}

		scores.clear();

		TaskExecutor.executeSQL(() -> {

			Connection connection = null;
			PreparedStatement ps = null;

			try {
				connection = Server.getConnection();
				
				ps = connection.prepareStatement(SELECT_QUERY);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {

					final int killCount = rs.getInt("clw_red_portal_kills");
					final String displayName = rs.getString("display_name");
					WildyRecord record = new WildyRecord(displayName, killCount,
							0, 0, 0, 0);
					scores.add(record);
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {

				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
		});
	}
	
private static String BOARDS_TITLE = "Red Portal Scoreboard";
	
	public static void openScoreboard(Player c) {
		c.getPacketSender().showInterface(6308);
		c.getPacketSender().sendFrame126(BOARDS_TITLE,
				6399);
		c.getPacketSender().sendFrame126(BOARDS_TITLE,
				6400);

		int count = 0;
		
		final int scoreSize = scores.size();
		
		for (int i = 6402; i < 8618; i++) {
			if (i == 6412) {
				i = 8578;
			}
			if (count < scoreSize) {
				WildyRecord r = scores.get(count);
				if (r == null) {
					count++;
					continue;
				}
				c.getPacketSender()
						.sendFrame126((count + 1) + ": " + r.getPlayerName()
								+ "  -  Kills: " + r.getKills(), i);
			} else {
				c.getPacketSender().sendFrame126("", i);
			}

			count++;
		}

	}
}
