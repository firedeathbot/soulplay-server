package com.soulplay.util.sql.scoregrab;

public class LmsScore {

	private final String displayName;
	private final int wins;
	private final int winRating;
	private final int kills;
	private final int killRating;
	
	public LmsScore(String displayName, int wins, int winRating, int kills, int killRating) {
		this.displayName = displayName;
		this.wins = wins;
		this.winRating = winRating;
		this.kills = kills;
		this.killRating = killRating;
	}

	public String getDisplayName() {
		return displayName;
	}

	public int getWins() {
		return wins;
	}

	public int getWinRating() {
		return winRating;
	}

	public int getKills() {
		return kills;
	}

	public int getKillRating() {
		return killRating;
	}
	
}
