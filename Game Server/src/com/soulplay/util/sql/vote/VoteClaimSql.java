package com.soulplay.util.sql.vote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.event.VoteEvent;
import com.soulplay.content.player.dailytasks.PlayerTask;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.content.vote.VoteInterfaceRewards;
import com.soulplay.content.vote.VoteInterfaceRewardsSection;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.save.SoulPlayDateSave;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.soulplaydate.SoulPlayDate;
import com.soulplay.util.soulplaydate.SoulPlayDateEnum;
import com.soulplay.util.sql.WebsiteSQL;

import plugin.button.infotab.InfoTabButtonPlugin;

public class VoteClaimSql {
	
	private static final String QUERY = "SELECT `id`, `site`, `user`, `ip` FROM `zaryte`.`mv_votes` WHERE `user` = ? AND `ready` = 1 AND `fulfilled` = 0";
	private static final String QUERY_CLAIMED = "UPDATE `zaryte`.`mv_votes` SET `fulfilled` = 1 WHERE `id` = ?";
	
	public static void claim(final Player p, final String playerName) {
		
		TaskExecutor.executeWebsiteSQL(() -> {
			
			try (Connection connection = WebsiteSQL.getHikari().getConnection();
					PreparedStatement ps = connection.prepareStatement(QUERY);) {

				ps.setString(1, playerName);
				
				try (ResultSet rs = ps.executeQuery()) {

					if (p != null && p.isActiveAtomic()) {
						while (rs.next()) {
							final int sqlId = rs.getInt("id");

							Server.getTaskScheduler().schedule(new Task(true) {

								@Override
								protected void execute() {
									this.stop();
									if (!p.disconnected) {
										if (!p.inEdgeville()) {
											p.sendMessage("You must be in Edgeville to claim votes.");
											return;
										}
										VoteClaimSql.rewardPlayer(p);
										VoteClaimSql.claimedVote(sqlId);
									}
								}
							});

						}

					}

				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		});
	}
	
	public static void claimByUsernameNames(final Player c) {
		c.startLogoutDelay();
		claim(c, c.getName());
	}
	
	public static void rewardPlayer(final Player c) {
		VoteEvent.onVote();

		if (!WorldType.equalsType(WorldType.SPAWN)) {
			if (Server.getCalendar().isAnniversaryMonth()) {
				if (Misc.random(7) == 1) {
				    c.getItems().addOrDrop(new GameItem(3062, 2)); // anniversary
				    c.sendMessageSpam("You have received a coin box for anniversary celebration!");
				}
			}

			if (Server.getCalendar().isXmas()) {
				if (Misc.random(5) == 1) {
					c.getItems().addItem(13663, 1);
					c.sendMessage("<col=ff0000>Ho Ho Ho, Merry christmas you have received an extra reward.");
				} else if (Misc.random(3) == 1) {
					c.getItems().addItem(3062, 1);
					c.sendMessage("<col=ff0000>Ho Ho Ho, Merry christmas you have received an extra reward.");
				}
			}

			if (c.bonusTask != null && !c.bonusTask.isCompleted()) {
				c.bonusTask.increaseComplete(-1, c, 1);
				boolean complete = c.bonusTask.isCompleted();
				if (complete) {
					c.sendMessage("You have completed your bonus daily task.");
					c.seasonPass.onTaskCompleted(c, false);
				} else {
					c.sendMessage("You have made progress on completing your bonus daily task.");
				}

				c.dailyTasksPoints += PlayerTask.POINTS_FOR_COMPLETION_BONUS;
			}

			//c.seasonPass.onVote(c);

			Variables.TIMES_VOTED.addValue(c, 1);
			c.sendMessageSpam("You've received 10 voting points.");
			SoulPlayDateSave.setNewDay(c, SoulPlayDate.getDate(), SoulPlayDateEnum.LAST_VOTE);
			Variables.VOTE_STREAK.addValue(c, 1);
			VoteInterfaceRewardsSection.checkStreak(c);
			VoteInterface.updateVotePoints(c);
			VoteInterfaceRewards.applyReward(c);

			c.yellPoints++;
			if (c.yellPoints == 1) {
				c.sendMessageSpam("You have received a yell. You can now yell 1 time.");
			} else {
				c.sendMessageSpam("You have received a yell. You can now yell " + c.yellPoints + " times.");
			}

			c.getAchievement().VoteOnce();
			c.getAchievement().vote10Times();
			c.getAchievement().vote50Times();
			c.getAchievement().vote250Times();
			InfoTabButtonPlugin.update(c);
		} else {
			c.getItems().addOrDrop(new GameItem(8890, 200));
			c.setLoyaltyPoints(c.getLoyaltyPoints() + 500,
					true);
			c.sendMessageSpam(
					"You have received 500 Loyalty Points for voting!");
			c.sendMessageSpam(
					"You receive 200 Blood Money for voting.");
		}
	}
	
	public static void claimedVote(final int id) {
		TaskExecutor.executeWebsiteSQL(() -> {
			
			try (Connection connection = WebsiteSQL.getHikari().getConnection();
					PreparedStatement ps = connection.prepareStatement(QUERY_CLAIMED);) {

				ps.setInt(1, id);
				ps.executeUpdate();
				
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		});
	}

}
