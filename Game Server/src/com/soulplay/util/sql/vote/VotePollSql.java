package com.soulplay.util.sql.vote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.content.poll.Poll;
import com.soulplay.content.poll.PollEntry;
import com.soulplay.content.poll.PollManager;
import com.soulplay.util.sql.SqlHandler;

public class VotePollSql {
	
	private final static String CHECK_VOTED = "SELECT * from `zaryte`.`vote_poll_votes` WHERE `poll_id` = ? AND (`ip` = ? OR `uuid` = ?)";
	
	public static int alreadyVoted(final int voteEntry, final String ip, final String uuid) {
		int result = 2; // auto skip
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(CHECK_VOTED);) {

			ps.setInt(1, voteEntry);
			ps.setString(2, ip);
			ps.setString(3, uuid);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					result = rs.getInt("value");
				} else {
					result = -1; // not voted
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private final static String VOTE_OPEN = "SELECT `poll_id`, `open` from `zaryte`.`vote_polls` WHERE `poll_id` = ? AND `open` = 1";
	
	public static boolean voteEntryOpen(final int voteEntry) {
		boolean open = false;
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(VOTE_OPEN);) {

			ps.setInt(1, voteEntry);
			
			try (ResultSet results = ps.executeQuery();) {
				open = results.next();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return open;
	}
	
	private static final String INSERT_VOTE = "INSERT INTO `zaryte`.`vote_poll_votes` (`ID`, `poll_id`, `ip`, `uuid`, `value`) VALUES (NULL, ?, ?, ?, ?)";
	
	public static void addVoteToVoteTable(final int voteEntry, final String ip, final String uuid, int value) {
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(INSERT_VOTE);) {
			ps.setInt(1, voteEntry);
			ps.setString(2, ip);
			ps.setString(3, uuid);
			ps.setInt(4, value);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private final static String SELECT_VOTE_ENTRY = "SELECT `poll_id`, `value` from `zaryte`.`vote_poll_votes` WHERE `poll_id` = ?";
	
	public static void countEntry(PollEntry entry) {
//		if (entry.getId() <= 0) return;
		
		entry.resetCount();
		
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(SELECT_VOTE_ENTRY);) {
			ps.setInt(1, entry.getId());
			
			try (ResultSet rs = ps.executeQuery()) {
				
				while (rs.next()) {
					int value = rs.getInt("value");
					switch(value) {
					case 0:
						entry.addYes();
						break;
					case 1:
						entry.addNo();
						break;
					case 2:
						entry.addSkip();
						break;
					}
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private final static String GRAB_OPEN_VOTES = "SELECT * from `zaryte`.`vote_polls` WHERE `open` = 1 LIMIT "+PollManager.MAX_POLLS;
	
	public static void grabVoteEntries() {
		
		Poll poll = new Poll(0, "Soulplay poll!", PollManager.pollInfo);
		
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(GRAB_OPEN_VOTES);) {

			try (ResultSet results = ps.executeQuery();) {
				while (results.next()) {
					int id = results.getInt("poll_id");
					String text = results.getString("question");
					poll.addEntry(new PollEntry(id, text, 0, 0, 0));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		PollManager.polls[0] = poll;
	}

}
