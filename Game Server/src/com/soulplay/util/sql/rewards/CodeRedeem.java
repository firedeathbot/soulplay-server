package com.soulplay.util.sql.rewards;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.soulplay.Server;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.task.TaskExecutor;
import com.soulplay.util.Misc;
import com.soulplay.util.sql.SqlHandler;

public class CodeRedeem {

	public static void checkForCodes(Player p) {
		
		if (!p.getStopWatch().checkThenStart(1)) {
			p.sendMessage("Please slow down.");
			return;
		}
		
		p.startLogoutDelay();
		
		TaskExecutor.executeSQL(new Runnable() {
			
			@Override
			public void run() {
		
				try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
						PreparedStatement statement = connection.prepareStatement("SELECT id, type " +
								"FROM special_offer_codes " +
								"WHERE status = 'TO_BE_CLAIMED' " +
								"AND claimedBy = ?");
						) {

					statement.setString(1, p.getName());

					getRewardsAmount(p, connection, statement);

				} catch (SQLException ex) {
					ex.printStackTrace();
				}

			}
		});
	}
	
	private static int getRewardsAmount(Player p, Connection connection, PreparedStatement statement) {
		int rewardAmount = 0;
		List<Integer> rewardList = new ArrayList<>();
		Timestamp timeStamp = Misc.getESTTimestamp();
		try (Statement stmt = connection.createStatement();
				ResultSet rs = statement.executeQuery();) {
			while (rs.next()) {
				int id = rs.getInt("id");
				int type = rs.getInt("type");
				rewardAmount++;
				stmt.addBatch("UPDATE special_offer_codes SET status = 'CLAIMED', claimed_timestamp = '" + timeStamp + "' WHERE id = " + id);
				rewardList.add(type);
			}
			
			stmt.executeBatch();
			
			if (rewardList.isEmpty()) {
				sendPlayerNoRewards(p);
			} else {
				sendRewardsToGame(p, rewardList);
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return rewardAmount;
	}
	
	private static void sendRewardsToGame(Player p, List<Integer> list) {
		Server.getTaskScheduler().schedule(new Runnable() {
			
			@Override
			public void run() {
				
				if (p.isActive) {
					for (int i = 0, len = list.size(); i < len; i++) {
						Integer type = list.get(i);
						
						if (p.getDialogueBuilder().isActive()) {
							p.getDialogueBuilder().reset();
						}
						
						//TODO: get reward by type
						switch (type) {
						case 0: // type 0 rewards
							p.getItems().addOrDrop(new GameItem(2996, 1)); //test
							p.sendMessage("Claimed a vote ticket from code!");
							p.getDialogueBuilder().sendStatement("You have claimed vote ticket from code!").execute();
							break;
							
						case 1: // type 1 rewards
							
							break;
							
						case 2: // type 2 rewards
							
							break;
						}
					}
					
				}
				
			}
		});
	}
	
	private static void sendPlayerNoRewards(Player p) {
Server.getTaskScheduler().schedule(new Runnable() {
			
			@Override
			public void run() {
				
				if (p.isActive && !p.disconnected) {
					
					p.getDialogueBuilder().reset();
					p.getDialogueBuilder().sendStatement("You don't have any rewards to claim.").execute();
					
				}
				
			}
		});
	}
	
}
