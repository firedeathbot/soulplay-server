package com.soulplay.util.sql.pos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.soulplay.content.items.pos.BuyOffers;
import com.soulplay.content.items.pos.UnclaimedItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.util.sql.SqlHandler;

public class MarketSql {

	public static void addBuyOfferToDB(String buyer, int mysqlIndex, Item item, long pricePerItem) {
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("INSERT INTO `zaryte`.`buy_offers` "
						+ "(buyer_name, buyer_mid, item_id, amount, price_per_item, slot) VALUES(?, ?, ?, ?, ?, ?)")) {
			ps.setString(1, buyer);
			ps.setInt(2, mysqlIndex);
			ps.setInt(3, item.getId());
			ps.setInt(4, item.getAmount());
			ps.setLong(5, pricePerItem);
			ps.setInt(6, item.getSlot());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static long removeNextBuyOffer(int mysqlIndex) {
		long price = 0;
		int amount = 0;
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT `ID`, `buyer_mid`, `amount`, `price_per_item` FROM `zaryte`.`buy_offers` WHERE `buyer_mid` = "+mysqlIndex+" LIMIT 1");
				ResultSet results = ps.executeQuery();) {
			if (!results.next()) {
				price = 0;
			} else {
				int id = results.getInt("ID");
				amount = results.getInt("amount");
				price = results.getLong("price_per_item");
				ps.close();

				if (price > 0) {
					try (PreparedStatement ps2 = connection.prepareStatement("DELETE FROM `zaryte`.`buy_offers` WHERE `buy_offers`.`ID` = "+id);) {
						ps2.executeUpdate();
					} catch (SQLException ex2) {
						ex2.printStackTrace();
						price = 0;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return price*amount;
	}
	
	public static long removeBuyOfferByID(final int id) {
		long price = 0;
		int amount = 0;
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT `ID`, `buyer_mid`, `amount`, `price_per_item`, `slot` FROM `zaryte`.`buy_offers` WHERE `ID` = "+id);
				ResultSet results = ps.executeQuery();) {
			if (!results.next()) {
				price = 0;
			} else {
				amount = results.getInt("amount");
				price = results.getLong("price_per_item");
				ps.close();

				if (price > 0) {
					try (PreparedStatement ps2 = connection.prepareStatement("DELETE FROM `zaryte`.`buy_offers` WHERE `buy_offers`.`ID` = "+id);) {
						ps2.executeUpdate();
					} catch (SQLException ex2) {
						ex2.printStackTrace();
						price = 0;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return price*amount;
	}

	private static final String UPDATE_STRING1 = "UPDATE `zaryte`.`buy_offers` SET `amount` = ? WHERE `ID` = ?";
	public static long sellItemToSlotOwner(final int offerIndex, final int slot, final int amountSold, final Client seller) { //TODO: log the transaction of seller/buyer item
		long price = 0;
		int amount = 0;
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT * from `zaryte`.`buy_offers` WHERE `ID` = "+offerIndex);
				ResultSet results = ps.executeQuery();) {
			if (!results.next()) {
				price = 0;
			} else {
				final String buyerName = results.getString("buyer_name");
				final int buyerMID = results.getInt("buyer_mid");
				final int itemId = results.getInt("item_id");
				amount = results.getInt("amount");
				price = results.getLong("price_per_item");
				ps.close();

				if (amountSold > amount) {
					price = 0;
					amount = 0;
				} else if (amountSold == amount) {
					try (PreparedStatement ps2 = connection.prepareStatement("DELETE FROM `zaryte`.`buy_offers` WHERE `buy_offers`.`ID` = "+offerIndex);) {
						ps2.executeUpdate();
						addItemToClaim(buyerMID, itemId, amountSold, slot, buyerName);
					} catch (SQLException ex2) {
						ex2.printStackTrace();
						price = 0;
					}
				} else if (amountSold < amount) {
					try (PreparedStatement ps3 = connection.prepareStatement(UPDATE_STRING1);) {
						final int newAmount = amount - amountSold;
						ps3.setInt(1, newAmount);
						ps3.setInt(2, offerIndex);
						ps3.executeUpdate();
						addItemToClaim(buyerMID, itemId, amountSold, slot, buyerName);
					} catch (SQLException ex2) {
						ex2.printStackTrace();
						price = 0;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			price = 0;
		}
		return price/**amountSold*/;
	}
	
	private static void addItemToClaim(final int newOwnerMID, final int itemId, final int amount, final int slot, final String playerName) {
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("INSERT INTO `zaryte`.`buy_offers_claim` "
						+ "(player_mid, player_name, item_id, amount, slot) VALUES(?, ?, ?, ?, ?)")) {
			ps.setInt(1, newOwnerMID);
			ps.setString(2, playerName);
			ps.setInt(3, itemId);
			ps.setInt(4, amount);
			ps.setInt(5, slot);
			ps.executeUpdate();
			addNotificationForCompleteOrder(newOwnerMID, playerName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static final String NOTIFY_QUERY = "UPDATE `players`.`accounts` SET unclaimed_shop = ? WHERE `ID` = ?";
	private static void addNotificationForCompleteOrder(final int playerMID, final String playerName) {
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(NOTIFY_QUERY)) {
			ps.setInt(1, 1);
			ps.setInt(2, playerMID);
			ps.executeUpdate();
			
			LoginServerConnection.instance().submit(() -> LoginServerConnection.
					instance().sendMessageToPlayer("You have an item ready to be claimed from Trading Post!", playerName));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void removeNotificationForCompleteOrder(final int playerMID) {
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(NOTIFY_QUERY)) {
			ps.setInt(1, 0);
			ps.setInt(2, playerMID);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadPlayerPurchaseOffers(final Client c) {
		if (!c.isActiveAtomic())
			return;

		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT * FROM `zaryte`.`buy_offers` WHERE `buyer_mid` = "+c.mySQLIndex);
				ResultSet rs = ps.executeQuery()) {
			
			while (rs.next()) {
				int mysqlID = rs.getInt("ID");
				int itemId = rs.getInt("item_id");
				int amount = rs.getInt("amount");
				long price = rs.getLong("price_per_item");
				int slot = rs.getInt("slot");
				Item item = new Item(itemId, amount, slot);
				BuyOffers offer = new BuyOffers(mysqlID, c.getName(), c.mySQLIndex, item, price);
				c.getOwnBuyOffers().put(slot, offer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private static final String PUBLIC_OFFERS_QUERY = "SELECT * FROM `zaryte`.`buy_offers` ORDER BY `buy_offers`.`timestamp` DESC LIMIT 20";
//	private static final String FILTER_OFFERS_QUERY = "SELECT * FROM `zaryte`.`buy_offers` WHERE `buy_offers`.`item_id` = ? ORDER BY `buy_offers`.`price_per_item` DESC LIMIT 20";
	
	public static void loadPublicPurchaseOffers(final Client c, boolean publicOffers, int... itemIdToSearch) { //maybe include more item ids into the filter?

		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement(publicOffers ? PUBLIC_OFFERS_QUERY : buildFilteredQuery( c.mySQLIndex, itemIdToSearch));
				ResultSet rs = ps.executeQuery()) {
				int count = 0;
				while (rs.next()) {
					final int mysqlID = rs.getInt("ID");
					final String username = rs.getString("buyer_name");
					final int buyerMID = rs.getInt("buyer_mid");
					final int itemId = rs.getInt("item_id");
					final int amount = rs.getInt("amount");
					final long price = rs.getLong("price_per_item");
					final int slot = rs.getInt("slot");
					Item item = new Item(itemId, amount, slot);
					BuyOffers offer = new BuyOffers(mysqlID, username, buyerMID, item, price);
					offer.setFilterSlot(count);
					if (!publicOffers)
						c.getMarketOffer().getFilteredBuyOffers().put(count, offer);
					else
						BuyOffers.getPublicBuyOffers().put(count, offer);
					count++;
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private static final String END = "ORDER BY `buy_offers`.`price_per_item` DESC LIMIT 20";
	public static String buildFilteredQuery(int notPlayerMID, int... itemIds) {
		StringBuilder sb = new StringBuilder("SELECT * FROM `zaryte`.`buy_offers` WHERE");
		sb.append(" `buyer_mid` != ");
		sb.append(notPlayerMID);
		sb.append(" AND");
		int count = 0;
		for (int item : itemIds) {
			sb.append(" `buy_offers`.`item_id` = ");
			sb.append(Integer.toString(item));
			sb.append(" ");
			if (++count < itemIds.length)
				sb.append("||");
		}
		return sb.append(END).toString();
	}
	
	public static void loadUnclaimedItems(final Client c) {
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT * FROM `buy_offers_claim` WHERE `player_mid` = "+ c.mySQLIndex +" LIMIT 20");
				ResultSet rs = ps.executeQuery()) {
				int count = 0;
				while (rs.next()) {
					final int mysqlID = rs.getInt("ID");
					final int playerMID = rs.getInt("player_mid");
					final String username = rs.getString("player_name");
					final int itemId = rs.getInt("item_id");
					final int amount = rs.getInt("amount");
					Item item = new Item(itemId, amount);
					UnclaimedItem uItem = new UnclaimedItem(mysqlID, playerMID, username, item, count);
					c.getClaimItems().getUnclaimedItems().put(count, uItem);
					count++;
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static int claimItem(int mysqlID) {
		int amount = 0;
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT ID, amount from buy_offers_claim WHERE ID = "+mysqlID);
				ResultSet results = ps.executeQuery();) {
			if (!results.next()) {
				amount = 0;
			} else {
				amount = results.getInt("amount");
				ps.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return amount;
	}
	
	public static boolean removeClaimAmount(int mysqlID, int amountClaimed) {
		boolean removed = false;
		try (Connection connection = SqlHandler.getHikariZaryteTable().getConnection();
				PreparedStatement ps = connection.prepareStatement("SELECT ID, amount from buy_offers_claim WHERE ID = "+mysqlID);
				ResultSet results = ps.executeQuery();) {
			if (results.next()) {
				int currentAmount = results.getInt("amount");
				ps.close();

				if (amountClaimed > currentAmount) {
					removed = false;
				} else if (amountClaimed == currentAmount) {
					try (PreparedStatement ps2 = connection.prepareStatement("DELETE FROM `zaryte`.`buy_offers_claim` WHERE `buy_offers_claim`.`ID` = "+mysqlID);) {
						ps2.executeUpdate();
						removed = true;
					} catch (SQLException ex2) {
						ex2.printStackTrace();
					}
				} else if (amountClaimed < currentAmount) {
					try (PreparedStatement ps3 = connection.prepareStatement("UPDATE `zaryte`.`buy_offers_claim` SET amount = ? WHERE `ID` = "+mysqlID);) {
						final int newAmount = currentAmount - amountClaimed;
						ps3.setInt(1, newAmount);
						ps3.executeUpdate();
						removed = true;
					} catch (SQLException ex2) {
						ex2.printStackTrace();
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return removed;
	}
	
}
