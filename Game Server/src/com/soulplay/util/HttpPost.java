package com.soulplay.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class HttpPost {

	private final static String USER_AGENT = "Mozilla/5.0";

//	private static ExecutorService httpService = Executors
//			.newCachedThreadPool();

	public static String getHttpResponseString(final String targetURL) {

		HttpURLConnection connection = null;
		// String line = null;

		StringBuffer result = new StringBuffer();
		BufferedReader in = null;
		try {
			// // open the connection to your website, this will throw and error
			// if offline
			// connection = (HttpURLConnection) new
			// URL(targetURL).openConnection();
			// connection.setConnectTimeout(10000);
			// connection.setReadTimeout(10000);
			// in = new BufferedReader(new
			// InputStreamReader(connection.getInputStream()));
			// // read the result of the login
			// line = in.readLine().trim();

			String url = targetURL;

			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(url);

			// add request header
			request.addHeader("User-Agent", USER_AGENT);
			HttpResponse response = client.execute(request);

			// System.out.println("Response Code : " +
			// response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// e.printStackTrace();
				}
			}
			if (connection != null) {
				connection.disconnect();
			}
		}
		return result.toString();

	}

//	public static Future<String> query(String targetURL) {
//		return httpService.submit(() -> {
//			HttpURLConnection connection = null;
//
//			try {
//				// open the connection to your website, this will throw and
//				// error if offline
//				connection = (HttpURLConnection) new URL(targetURL)
//						.openConnection();
//				BufferedReader in = new BufferedReader(
//						new InputStreamReader(connection.getInputStream()));
//				String line = in.readLine();
//				return line;
//			} catch (Exception e) {
//				// e.printStackTrace();
//				return null;
//			} finally {
//				if (connection != null) {
//					connection.disconnect();
//				}
//			}
//		});
//	}

}
