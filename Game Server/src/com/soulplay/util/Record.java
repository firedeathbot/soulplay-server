package com.soulplay.util;

import java.io.Serializable;

public class Record implements Serializable {

	private static final long serialVersionUID = 1L;

	public String playerName;

	public int kills, deaths, elo;

	public double kdr;

	public int index;

	public int lastDayPkedOfYear;

	public Record(String userName, int kills, int deaths, double kdr, int elo,
			int lastDayPked) {
		this.playerName = userName;
		this.kills = kills;
		this.deaths = deaths;
		this.kdr = kdr;
		this.elo = elo;
		this.lastDayPkedOfYear = lastDayPked;
	}
}
