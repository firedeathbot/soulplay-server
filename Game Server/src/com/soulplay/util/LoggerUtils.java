package com.soulplay.util;

import java.util.logging.Logger;

public class LoggerUtils {

    public static <T> Logger getLogger(Class<T> clazz) {
        return Logger.getLogger(clazz.getSimpleName());
    }

}
