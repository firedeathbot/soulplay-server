package com.soulplay.util.interfacedata;

public class BitShifting {

	public static int compressHexColor(int color) {
		int newR = (color >> 17);
		int newG = (color >> 10) & 0xff;
		int newB = (color >> 3) & 0xff;

		int result = newR;
		result = (result << 4) | newG;
		result = (result << 4) | newB;

		return result;
	}

}
