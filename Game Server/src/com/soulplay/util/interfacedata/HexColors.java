package com.soulplay.util.interfacedata;

public class HexColors {

	public static int WHITE = 0xffffff;

	public static int BLACK = 0x000000;

	public static int GREEN = 0x00f000;

	public static int GREEN_DISABLED_COLOR = 0x008000;

	public static int RED_BRIGHT = 0xff0000; // this color merges with other
												// colors, anything with FF
												// merges colors

	public static int RED = 0xf80000;

	public static int RED_DARK = 0xc00000;

	public static int YELLOW = 0xf8f800;

}
