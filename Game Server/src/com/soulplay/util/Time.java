package com.soulplay.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {

	static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm");

	public final static int LESS_THAN = 0, LESS_THAN_OR_EQUAL = 1,
			GREATER_THAN = 2, GREATER_THAN_OR_EQUAL = 3, EQUAL = 4;

	public static Time getCurrentTime() {
		return new Time(
				timeFormat.format(new Date(System.currentTimeMillis())));
	}

	public static Time getRandomTimeBetween(Time time_1, Time time_2) {
		int mins_1 = time_1.toMinutes();
		int mins_2 = time_2.toMinutes();
		int out_mins;
		if (mins_1 > mins_2) {
			mins_2 += 24 * 60;
		}
		out_mins = mins_1 + (int) (Math.random() * (mins_2 - mins_1));
		if (out_mins > 24 * 60) {
			out_mins -= 24 * 60;
		}
		return new Time(0, out_mins);
	}

	int hour, minute;

	public Time(int hour, int minute) {
		this.hour = hour;
		this.minute = minute;
		fixFormat();
	}

	public Time(long milis) {
		this(timeFormat.format(new Date(milis)));
	}

	public Time(String time) {
		hour = Integer.parseInt(time.split(":")[0]);
		minute = Integer.parseInt(time.split(":")[1]);
		fixFormat();
	}

	public Time add(Time time) {
		return new Time(0, toMinutes() + time.toMinutes());
	}

	public boolean compareTo(Time comparison, int type) {
		switch (type) {
			case LESS_THAN:
				return toMinutes() < comparison.toMinutes();
			case LESS_THAN_OR_EQUAL:
				return toMinutes() <= comparison.toMinutes();
			case GREATER_THAN:
				return toMinutes() > comparison.toMinutes();
			case GREATER_THAN_OR_EQUAL:
				return toMinutes() >= comparison.toMinutes();
			case EQUAL:
				return toMinutes() == comparison.toMinutes();
		}
		return false;
	}

	public void fixFormat() {
		while (minute > 59) {
			hour++;
			minute -= 60;
		}
		while (hour > 24) {
			hour -= 24;
		}
	}

	public boolean hasTimePassed(Time time) {
		int current = getCurrentTime().toMinutes();
		if (current < toMinutes()) {
			current += 24 * 60;
		}
		return current <= add(time).toMinutes();
	}

	public boolean isBetween(Time time_1, Time time_2) {
		return compareTo(time_1, GREATER_THAN_OR_EQUAL)
				&& compareTo(time_2, LESS_THAN_OR_EQUAL)
				|| compareTo(time_2, GREATER_THAN_OR_EQUAL)
						&& compareTo(time_1, LESS_THAN_OR_EQUAL);
	}

	public int toMinutes() {
		return hour * 60 + minute;
	}

	@Override
	public String toString() {
		return hour + ":" + minute;
	}
}
