package com.soulplay.util.soulplaydate;

public class PlayerDates {

	private int weeklyResetDay, lastLoginDay, lastDayPked, lastVoted;

	public int getWeeklyResetDay() {
		return weeklyResetDay;
	}

	public void setWeeklyResetDay(int weeklyResetDay) {
		this.weeklyResetDay = weeklyResetDay;
	}

	public int getLastDayPked() {
		return lastDayPked;
	}

	public void setLastDayPked(int lastDayPked) {
		this.lastDayPked = lastDayPked;
	}

	public int getLastLoginDay() {
		return lastLoginDay;
	}

	public void setLastLogin(int lastLogin) {
		this.lastLoginDay = lastLogin;
	}

	public int getLastVoted() {
		return lastVoted;
	}

	public void setLastVoted(int lastVoted) {
		this.lastVoted = lastVoted;
	}

}
