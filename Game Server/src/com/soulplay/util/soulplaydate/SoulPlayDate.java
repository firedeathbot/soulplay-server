package com.soulplay.util.soulplaydate;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class SoulPlayDate {

	private static final long MINUTE_TO_MILLIS = 60000L;

	private static final long DAY_TO_MILLIS = 86400000L;

	private static final int SOULPLAY_LAUNCH_OFFSET = 16646;

	private static final Calendar CALENDAR = Calendar.getInstance();

	private static final ZoneId TIME_ZONE = ZoneId.of("GMT-5");

	public static int dayOffset = 0;
	
	//0= wednesday - 1=thursday - 2=friday - 3=saturday - 4=sunday - 5=monday - 6=tuesday - 7 = wednesday
	public static final int SUNDAY_OFFSET = 4;
	
	private static int lastSunday = 0;
	
	public static void createLastSunday() {
		int thisDay = SoulPlayDate.getDate();
		int offset = (thisDay - SoulPlayDate.SUNDAY_OFFSET) % 7;
		lastSunday = thisDay - offset;
	}
	
	public static int getLastSunday() {
		return lastSunday;
	}

	/**
	 * Returns the current date in minutes.
	 *
	 * @return The current date converted into minutes.
	 */
	public static int getMinutes() {
		return getMinutes(currentTimeMillis());
	}

	/**
	 * Gets the date in minutes for {@code timestamp}.
	 *
	 * @param timestamp The time.
	 * @return The date in minutes.
	 */
	public static int getMinutes(long timestamp) {
		return (int) (timestamp / MINUTE_TO_MILLIS);
	}

	/**
	 * Returns the current year.
	 *
	 * @return The current year.
	 */
	public static int getYear() {
		return getYear(currentTimeMillis());
	}

	/**
	 * Gets the year based on {@code timestamp}.
	 *
	 * @param timestamp The timestamp.
	 * @return The year the timestamp is in.
	 */
	public static int getYear(long timestamp) {
		CALENDAR.clear();
		CALENDAR.setTime(new Date(timestamp));

		return CALENDAR.get(Calendar.YEAR);
	}

	/**
	 * Returns the current date in SoulPlayDays.
	 *
	 * @return The current date converted into SoulPlayDays.
	 */
	public static int getDate() {
		return (int) (currentTimeMillis() / DAY_TO_MILLIS - SOULPLAY_LAUNCH_OFFSET) + dayOffset;
	}

	/**
	 * Converts {@code date} to the number of SoulPlayDays.
	 *
	 * @param date The date to convert.
	 * @return The converted amount of SoulPlayDays.
	 */
	public static int fromDate(LocalDate date) {
		CALENDAR.clear();
		CALENDAR.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
		CALENDAR.set(Calendar.HOUR_OF_DAY, 12);
		CALENDAR.set(date.getYear(), date.getMonthValue() - 1, date.getDayOfMonth());

		return (int) (CALENDAR.getTime().getTime() / DAY_TO_MILLIS) - SOULPLAY_LAUNCH_OFFSET;
	}

	/**
	 * @param runeDate
	 * @return
	 */
	public static LocalDate toDate(int runeDate) {
		long timestamp = DAY_TO_MILLIS * (SOULPLAY_LAUNCH_OFFSET + runeDate);

		CALENDAR.clear();
		CALENDAR.setTimeInMillis(timestamp);

		return CALENDAR.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	/**
	 * Determines if the given {@code year} is a leap year.
	 *
	 * @param year The year.
	 * @return {@code true} if the year is a leap year, otherwise {@code false}.
	 */
	public static boolean isLeapYear(int year) {
		if (year < 0) {
			return (year + 1) % 4 == 0;
		} else if (year < 1582) {
			return year % 4 == 0;
		} else if (year % 4 != 0) {
			return false;
		} else if (year % 100 != 0) {
			return true;
		} else if (year % 400 != 0) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the current time in milliseconds based on UTC timezone.
	 *
	 * @return The timestamp.
	 */
	public static long currentTimeMillis() {
		return Instant.now().atZone(TIME_ZONE).toInstant().toEpochMilli();
	}

	/**
	 * Converts the given {@code timestamp} to a {@link LocalDate}.
	 *
	 * @param timestamp The timestamp to convert.
	 * @return The local date instance.
	 */
	public static LocalDate localDateFromTimeStamp(long timestamp) {
		return Instant.ofEpochMilli(timestamp).atZone(TIME_ZONE).toLocalDate();
	}

	/**
	 * Get the time until the next {@code x} minute interval. Example usage is to
	 * find the time until the next :05 interval for delaying a task on startup.
	 *
	 * @param x The interval to use.
	 * @return The time in milliseconds until the next interval.
	 */
	public static long getOffsetForNextInterval(int x) {
		LocalDateTime now = LocalDateTime.now();

		int nextInterval = now.getMinute() + (x - now.getMinute() % x);
		int minuteOffset = nextInterval - now.getMinute() - 1;
		int secondOffset = 60 - now.getSecond();

		return ((minuteOffset * 60) + secondOffset) * 1_000;
	}

	/**
     * Private constructor for Util class
     */
    private SoulPlayDate() {

    }

}
