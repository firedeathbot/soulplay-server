package com.soulplay.util.soulplaydate;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.game.model.player.Player;

public enum SoulPlayDateEnum {

	ACCOUNT_CREATED(1),
	LAST_DAY_LOGIN(2) {
		@Override
		public void set(Player p, int value) {
			p.getSoulplayDate().setLastLogin(value);
		}
	},
	WEEKLY_TASK_RESET(3) {
		@Override
		public void set(Player p, int value) {
			p.getSoulplayDate().setWeeklyResetDay(value);
		}
	},
	LAST_DAY_PKED(4) {
		@Override
		public void set(Player p, int value) {
			p.getSoulplayDate().setLastDayPked(value);
		}
	},
	LAST_VOTE(5) {
		@Override
		public void set(Player p, int value) {
			p.getSoulplayDate().setLastVoted(value);
		}
	},
	;
	
	private final int id;
	
	private SoulPlayDateEnum(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void set(Player p, int value) {
	}
	
	private static final Map<Integer, SoulPlayDateEnum> map = new HashMap<>();
	
	static {
		for (SoulPlayDateEnum data : values()) {
			map.put(data.getId(), data);
		}
	}
	
	public static SoulPlayDateEnum forId(int id) {
		return map.getOrDefault(id, ACCOUNT_CREATED);
	}
	
}
