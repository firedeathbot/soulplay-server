package com.soulplay.util;

public class SQLUtils {
    
    public static void main(String[] args) {
	int[] array = new int[]{1000, 1500, 6000, 2000, 1000};
	
	// prints 1000:1500:6000:2000:1000
	
	System.out.println(createStringFromIntArray(array));
    }
    
    public static String createStringFromIntArray(int[] array) {	
	final StringBuilder builder = new StringBuilder();
	
	for (int index = 0; index < array.length; index++) {
	    final int value = array[index];
	    
	    builder.append(value);
	    
	    if (index + 1 < array.length) {
		builder.append(":");
	    }
	}
	
	return builder.toString();	
    }

}
