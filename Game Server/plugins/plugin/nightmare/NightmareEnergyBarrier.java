package plugin.nightmare;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class NightmareEnergyBarrier extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 37730);
		map(ObjectClickExtension.class, 137730);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);

		player.getDialogueBuilder().sendOption("Are you sure you want to leave?", "Yes.", () -> {
					player.getPacketSender().nightmareFog();
					player.pulse(() -> {
						player.getPA().movePlayer(Location.create(10208, 9756, 1));
						player.startAnimation(Animation.osrs(8583));
					}, 3);
				}, "No.", () -> {}).execute();
	}

}
