package plugin.nightmare;

import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareLobbyNPC;
import com.soulplay.content.npcs.impl.bosses.nightmare.NightmareManager;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class NightmarePlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, NightmareLobbyNPC.SLEEPING);
		map(NpcClickExtension.class, NightmareLobbyNPC.AWAKENING);
		map(NpcClickExtension.class, NightmareLobbyNPC.FIRST_PHASE);
		map(NpcClickExtension.class, NightmareLobbyNPC.SECOND_PHASE);
		map(NpcClickExtension.class, NightmareLobbyNPC.THIRD_PHASE);
	}

	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder().sendStatement("You are about to begin an encounter with the Nightmare. Dying",
				"during this encounter will not be considered as safe death. Are you",
				"sure you wish to begin?").sendOption("Are you sure you wish to begin?", "Yes.", () -> {
					NightmareManager.enter(player);
				}, "No.", () -> {}).execute();
		
	}

	@Override
	public void onNpcOption2(Player player, NPC npc) {
		player.getDialogueBuilder().sendStatement(NightmareManager.buildFirstInspectMessage(), "The fight has not yet started.").execute();
	}

}
