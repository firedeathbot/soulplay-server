package plugin.portsarim.npc;

import com.soulplay.content.minigames.Sailing;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class MonkOfEntranaPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 657);
		map(NpcClickExtension.class, 2728);
		map(NpcClickExtension.class, 2729);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder()
		.sendNpcChat(npc.npcType, "Would you like to travel to Entrana?",
				"You can get back here by boarding the ship.")
		.sendOption("Go to Entrana?", "Yes", ()-> {
			Sailing.startTravel(player, 1);
		}, "No", ()-> {
			player.getDialogueBuilder().reset();
		})
		.executeIfNotActive();
	}
	
	@Override
	public void onNpcOption2(Player player, NPC npc) {
		Sailing.startTravel(player, 1);
	}
	
}
