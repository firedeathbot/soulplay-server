package plugin.itemactions.two;

import java.util.LinkedList;
import java.util.List;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.net.packet.in.ItemAction2;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class GiantPresentPlugin extends Plugin implements ItemClickExtension {
	
	private static final int GIANT_PRESENT = 30347;
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, GIANT_PRESENT);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		final Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		ItemAction2.wearItemCentral(player, itemId, itemSlot);
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		
		if (itemId != GIANT_PRESENT) {
			return;
		}

		player.getDialogueBuilder().sendOption("Open the present", ()-> {
			addReward(player);
		}, "Don't open the present.", ()-> {}).executeIfNotActive();

	}
	
	public static void addReward(Player player) {//random reward
		if (!player.getItems().playerHasItem(GIANT_PRESENT)) {
			player.sendMessage("Stop right there! You have no crate in your inventory.");
			return;
		}
		
		player.getItems().deleteItemInOneSlot(GIANT_PRESENT, 1);
	 		
	  		int random = Misc.randomNoPlus(1000);
	  		if (random <= 2) {
	  			rewards(player, ULTRA_RARE);
	  		} else if (random <= 17) {
	  			rewards(player, EXTREMELY_RARE);
	  		} else if (random <= 37) {
	  			rewards(player, SUPER_RARE);
	  		} else if (random <= 117) {
	  			rewards(player, RARE);
	  		} else if (random <= 500) {
	  			rewards(player, UNCOMMON);
	  		} else if (random <= 1000) {
	  			rewards(player, COMMON);
	  		}
	}

	private static void rewards(Player player, int[][] items) {
		int random;
		List<Integer> indices = new LinkedList<>();
		for (int i = 0, length = items.length; i < length; i++) {
			indices.add(i);
		}
		if (indices.size() == 0) {
			return;
		}

		random = indices.get(Misc.randomNoPlus(indices.size()));

		player.getItems().addOrDrop(new GameItem(items[random][0], Misc.randomNoPlus(items[random][1], items[random][2])));
		player.sendMessage("You received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from a magical crate!");
		if (items == RARE || items == RARE || items == SUPER_RARE || items == EXTREMELY_RARE) {
			PlayerHandler.messageAllPlayers("<img=9> <shad=1><col=6B8E23>Broadcast: </shad><col=000000>" + player.getNameSmartUp() + " received " + ItemConstants.getItemName(items[random][0]).toLowerCase() + " from a giant present!");
		}
	}

	public static final int[][] COMMON = { 
			{13663, 1, 2}, // spin tickets
			{13663, 1, 2}, // spin tickets
			{30174, 1, 1}, // super mystery box
			{4151, 1, 1}, // abyssal whip
			{11235, 1, 1}, // dark bow
			{18831, 30, 40}, // frost dragon bones
			{30174, 1, 1}, // super mystery box
			{30175, 1, 1}, // legendary mystery box
			{995, 20_000_000, 25_000_000}, // coins
			{995, 20_000_000, 25_000_000}, // coins
			{995, 20_000_000, 25_000_000}, // coins
			{995, 20_000_000, 25_000_000}, // coins
	};
	
	public static final int[][] UNCOMMON = { 
			{21371, 1, 1}, // vine whip
			{11730, 1, 1}, // saradomin sword
			{15486, 1, 1}, // staff of light
			{11722, 1, 1}, // armadyl plateskirt
			{11720, 1, 1}, // armadyl chestplate
			{11718, 1, 1}, // armadyl helmet
			{11726, 1, 1}, // bandos tasset
			{11724, 1, 1}, // bandos chestplate
			{11704, 1, 1}, // bandos hilt
			{11706, 1, 1}, // saradomin hilt
			{11708, 1, 1}, // zamorak hilt
			{12004, 1, 1}, // abyssal tentacle
			{30174, 1, 1}, // super mystery box
			{30175, 1, 1}, // legendary mystery box
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
			{995, 33_000_000, 46_000_000}, // coins	
			{995, 33_000_000, 46_000_000}, // coins	
			{995, 33_000_000, 46_000_000}, // coins	
			{995, 33_000_000, 46_000_000}, // coins	
			
			
	};

	public static final int[][] RARE = { 
			{14484, 1, 1}, //dragon claws
			{11702, 1, 1}, // armadyl hilt	
			{20135, 1, 1}, // torva full helm	
			{20139, 1, 1}, // torva platebody	
			{20143, 1, 1}, // torva platelegs	
			{20147, 1, 1}, // pernix cowl	
			{20151, 1, 1}, // pernix body	
			{20155, 1, 1}, // pernix chaps	
			{20159, 1, 1}, // virtus mask	
			{20163, 1, 1}, // virtus robe top	
			{20167, 1, 1}, // virtus robe legs	
			{13740, 1, 1}, // divine spirit shield	
			{13742, 1, 1}, // elysian spirit shield	
			{13738, 1, 1}, // arcane spirit shield	
			{13744, 1, 1}, // spectral spirit shield
			{20171, 1, 1}, // zaryte bow
			{19780, 1, 1}, // korasi's sword	
			{12924, 1, 1}, // blowpipe
			{30022, 1, 1}, // dragon warhammer
			{11791, 1, 1}, // staff of the dead
			{30002, 1, 1}, // serpentine helm
			{13663, 1, 4}, // spin tickets	
			{13663, 1, 4}, // spin tickets	
			{4273, 1, 1}, // golden key	
			{4273, 1, 1}, // golden key	
			{4273, 1, 1}, // golden key	
			{4273, 1, 1}, // golden key	
			{30174, 1, 1}, // super mystery box	
			{30175, 1, 1}, // legendary mystery box
			{30175, 1, 1}, // legendary mystery box
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
			{995, 78_000_000, 90_000_000}, // coins		
			{995, 78_000_000, 90_000_000}, // coins	
			{995, 78_000_000, 90_000_000}, // coins	
			{995, 78_000_000, 90_000_000}, // coins	
			{995, 78_000_000, 90_000_000}, // coins	
	};
	
	public static final int[][] SUPER_RARE = { 
			{30092, 1, 1}, // twisted bow
			{30091, 1, 1}, // twisted buckler
			{30136, 1, 1}, // ranger gloves
			{30087, 1, 1}, // dragon hunter crossbow
			{786, 1, 1}, // $10 scroll
			{30175, 1, 1}, // legendary box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
			{995, 1_500_000_000, 1_800_000_000}, // coins
			{995, 1_500_000_000, 1_800_000_000}, // coins
			{995, 1_500_000_000, 1_800_000_000}, // coins
			
	};
	
	public static final int[][] EXTREMELY_RARE = { 
			{1050, 1, 1}, // santa hat
			{1050, 1, 1}, // santa hat
			{1053, 1, 1}, // green h'ween mask
			{1055, 1, 1}, // blue h'ween mask
			{1057, 1, 1}, // red h'ween mask
			{1053, 1, 1}, // green h'ween mask
			{1055, 1, 1}, // blue h'ween mask
			{1057, 1, 1}, // red h'ween mask
			{1037, 1, 1}, // bunny ears
			{1037, 1, 1}, // bunny ears
			{2581, 1, 1}, // robin hood hat
			{30137, 1, 1}, // ranger's tunic
			{1038, 1, 1}, // red partyhat
			{1040, 1, 1}, // yellow partyhat
			{1042, 1, 1}, // blue partyhat
			{1044, 1, 1}, // green partyhat
			{1046, 1, 1}, // purple partyhat
			{1048, 1, 1}, // white partyhat
			{4084, 1, 1}, // sled
			{1505, 1, 1}, // $50 scroll
			{20061, 1, 1}, // random pet box
			{30176, 1, 1}, // ultra box
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
	};
	
	public static final int[][] ULTRA_RARE = { 
			{30058, 1, 1}, //black h'ween mask
			{15740, 1, 1}, // black santa hat
			{15741, 1, 1}, // black partyhat
			{15742, 1, 1}, // pink partyhat
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{30176, 1, 1}, // ultra box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{20061, 1, 1}, // random pet box
			{2396, 1, 1}, // $100 scroll
			{30342, 1, 1}, // santa item
			{30343, 1, 1}, // santa item
			{30344, 1, 1}, // santa item
			{30345, 1, 1}, // santa item
			{30346, 1, 1}, // santa item
			
	};
}
