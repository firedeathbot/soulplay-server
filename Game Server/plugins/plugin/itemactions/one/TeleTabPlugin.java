package plugin.itemactions.one;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.player.TeleportType;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.definition.TeleTab;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class TeleTabPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		TeleTab.init();
		for (TeleTab t : TeleTab.values()) {
			map(ItemClickExtension.class, t.getItemId());
		}
	}
	
	@Override
	public void onItemOption1(final Player player, final int itemId, final int itemSlot, final int interfaceId) {
		if (player.teleTimer > 0 || player.teleportToX > 0) {
			return;
		}
		
		final TeleTab tab = TeleTab.forItemId(itemId);
		
		if (tab == null) // should never happen anyways
			return;
		
		startTeleport(player, tab, itemSlot);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		final Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
	
	public static void startTeleport(final Player player, final TeleTab tab, final int slot) {

		if (player.getPA().startTeleport(tab.getLoc().getX(), tab.getLoc().getY(), tab.getLoc().getZ(), TeleportType.TAB)) {
			player.getItems().deleteItemInOneSlot(tab.getItemId(), slot, 1);
		}
		
	}
	
}
