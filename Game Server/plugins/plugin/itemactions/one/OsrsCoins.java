package plugin.itemactions.one;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class OsrsCoins extends Plugin implements ItemClickExtension {
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, 30367);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		
		player.getDialogueBuilder()
		.sendStatement("You can exchange the OSRS Coins by contacting an admin.", "You can contact an admin on ::discord or ::forums.", "Note: You need at least 50M OSRS Coins to exchange it.");
		

		player.getDialogueBuilder().execute();

	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		final Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
}
