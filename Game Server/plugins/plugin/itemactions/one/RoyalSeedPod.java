package plugin.itemactions.one;

import com.soulplay.content.player.TeleportType;
import com.soulplay.game.model.item.definition.TeleTab;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class RoyalSeedPod extends Plugin implements ItemClickExtension {
	
	private static final int seed = 119564;
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, seed);
	}
	
	@Override
	public void onItemOption1(final Player player, final int itemId, final int itemSlot, final int interfaceId) {
		if (player.teleTimer > 0 || player.teleportToX > 0) {
			return;
		}
		
		if (player.getPA().canTeleport(TeleportType.ROYAL_SEED_POD)) {
			if (player.getSpamTick().checkThenStart(10)) {
				if (itemId == seed) {
					player.applyFreeze(11);
					player.startAnimation(4544);
					player.startGraphic(Graphic.create(767));
					player.pulse(() -> {
						player.transform(8254);
						player.pulse(() -> {
							player.startAnimation(4546);
							player.startGraphic(Graphic.create(769));
							player.getPA().movePlayer(2466, 3494, 0);
							player.pulse(() -> {
								player.transform(-1);
								player.pulse(() -> {
									player.resetFreeze();
							    }, 5);
					        }, 2);
				        }, 1);
			        }, 3);
		        }
		    }
		}
	}

}
