package plugin.itemactions.one;

import com.soulplay.content.player.pets.pokemon.Pokemon;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class PetDoubleExpScroll extends Plugin implements ItemClickExtension {
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, 15360);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		
		player.getDialogueBuilder()
		.sendStatement("Your Pet Double Exp has not expired yet.", "If you activate it now, the current double exp timer will be reset.")
		.sendStatement("Would you like to activate your Pet Double Exp?")
		.sendOption("Yes", ()-> {
			if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
				player.setPetDoubleExp(Pokemon.DOUBLE_EXP_SCROLL_TICKS);
				player.sendMessage(String.format("You have activated the Pet Double Exp for %d minutes.", (int)(Misc.ticksIntoSeconds(Pokemon.DOUBLE_EXP_SCROLL_TICKS)/60)));
			}
			player.getDialogueBuilder().jumpToStage(100);
		}, "No", ()-> {
			player.getDialogueBuilder().jumpToStage(100);
		});

		if (player.getPetDoubleExpLength() == 0)
			player.getDialogueBuilder().jumpToStage(1);

		player.getDialogueBuilder().execute();

	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendDestoryItem(itemId).execute();
	}
}
