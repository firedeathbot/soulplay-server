package plugin.itemactions.one;

import com.soulplay.content.minigames.treasuretrails.rewards.LootManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class ClueCaskets extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 2715);
		map(ItemClickExtension.class, 13054);
		map(ItemClickExtension.class, 13037);
		map(ItemClickExtension.class, 19039);
		map(ItemClickExtension.class, 13077);
		map(ItemClickExtension.class, 119836);
		map(ItemClickExtension.class, 120546);
		map(ItemClickExtension.class, 120545);
		map(ItemClickExtension.class, 120544);
		map(ItemClickExtension.class, 120543);

	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
			case 2715: // easy
			case 120546: // new easy
				if (player.getItems().freeSlots() > 3) {
					if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
						LootManager.rollEasy(player);
					}
				} else {
					player.sendMessage("You need 4 free slots in your inventory!");
				}
				return;
			case 13054: // medium
			case 120545: // new medium
				if (player.getItems().freeSlots() > 4) {
					if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
						LootManager.rollMedium(player);
					}
				} else {
					player.sendMessage("You need 5 free slots in your inventory!");
				}
				return;
			case 13037: // hard
			case 120544: // new hard
				if (player.getItems().freeSlots() > 5) {
					if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
						LootManager.rollHard(player);
					}
				} else {
					player.sendMessage("You need 5 free slots in your inventory!");
				}
				return;
			case 19039: // elite
			case 120543: // new elite
				if (player.getItems().freeSlots() > 6) {
					if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
						LootManager.rollElite(player);
					}
				} else {
					player.sendMessage("You need 6 free slots in your inventory!");
				}
				return;
			case 13077: // master
			case 119836: // new master
				if (player.getItems().freeSlots() > 6) {
					if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
						LootManager.rollMaster(player);
					}
				} else {
					player.sendMessage("You need 7 free slots in your inventory!");
				}
				return;
		}

	}
}
