package plugin.itemactions.one;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class WildernessKey extends Plugin implements ItemClickExtension {
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, 21805);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		
		player.getDialogueBuilder()
		.sendStatement("This key can be used to open up the", "Wilderness chest in the resource area.", "The chest contains high valued items.");
		player.getDialogueBuilder().execute();

	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		final Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
}
