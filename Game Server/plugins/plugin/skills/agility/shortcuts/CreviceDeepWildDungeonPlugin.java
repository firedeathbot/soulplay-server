package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class CreviceDeepWildDungeonPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 19043);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		if (player.getSkills().getLevel(Skills.AGILITY) < 46) {
			player.sendMessage("You need agility lvl 46 for this shortcut.");
			return;
		}
		
		player.startAnimation(AgilityAnimations.CRAWL_UNDER);
		
		int moveX = player.getX() == 9448 ? 9446 : 9448;
		int moveY = player.getY() == 10336 ? 10326 : 10336;
		
		player.getPA().movePlayerDelayed(moveX, moveY, player.getZ(), 0, 1);

	}
}
