package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class StilePlugin extends Plugin implements ObjectClickExtension {

	private static final String POSITION_MSG = "You must position yourself in front of the obstacle.";
	
	
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 993);
		map(ObjectClickExtension.class, 7527);
		map(ObjectClickExtension.class, 19222);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		
		switch (o.getOrientation()) {
		case 0:
		case 2:
			if (player.getX() != o.getX()) {
				player.sendMessage(POSITION_MSG);
				return;
			}
			break;
		case 1:
		case 3:
			if (player.getY() != o.getY()) {
				player.sendMessage(POSITION_MSG);
				return;
			}
			break;
		}
		
		Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), o.getLocation());
		
		ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, dir).addSecondDirection(dir.getStepX()*3, dir.getStepY()*3, 3, 0);
		
		player.setForceMovement(mask);
		player.startAnimation(AgilityAnimations.CRAWL_OVER_FENCE_DELAYED_1_TICK);
	}
}
