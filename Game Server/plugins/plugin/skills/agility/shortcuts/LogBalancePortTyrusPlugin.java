package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class LogBalancePortTyrusPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 3931);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(object.getLocation());
		
		if (player.getSkills().getLevel(Skills.AGILITY) < 60) {
			player.sendMessage("You need agility level 60 to cross this log.");
			return;
		}
		
		if (player.getY() != object.getY()) {
			player.sendMessage("Please stand in front of the log first.");
			return;
		}
		
		
		player.lockActions(8);
		player.lockMovement(8);
		
		final Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), object.getLocation());
		
		final Location endLoc = player.getCurrentLocation().transform(dir, 6);
		
		final ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*6, dir.getStepY()*6, 1, 0, dir).setCustomSpeed(180, 0);
		
		final int originalStance = player.getMovement().getStandAnim();
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					player.startAnimation(AgilityAnimations.LOG_WALK2.getId(), 0);
//					player.getMovement().changeStance(AgilityAnimations.LOG_WALK3.getId());
					player.setForceMovementRaw(mask);
					break;

				case 7:
					player.startAnimation(65535);
					break;

				case 8:
					container.stop();
					player.getMovement().changeStance(originalStance);
					player.lockActions(0);
					player.lockMovement(0);
					player.getPA().movePlayer(endLoc);
					break;
				
				}
				
			}
			
		}, 1);
		
	}
	
}
