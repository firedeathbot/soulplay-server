package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class PassageCorpLairPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 37929);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (player.getY() != object.getY()+2) {
			player.getPA().playerWalk(player.getX(), object.getY()+2);
		}
		
		final Direction dir = object.getX() > player.getX() ? Direction.EAST : Direction.WEST;
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (player.getY() == object.getY()+2) {
					container.stop();
					Misc.faceObject(player, object);
					
					final int stance = player.getMovement().getStandAnim();
					
					player.getMovement().changeStance(AgilityAnimations.CRAWL_UNDER.getId());
					
					player.lockMovement(5);
					player.lockActions(5);
					
					CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
						int ticks = 0;
						@Override
						public void stop() {
							player.getMovement().changeStance(stance);
						}
						
						@Override
						public void execute(CycleEventContainer cont) {
							ticks++;
							
							if (ticks > 5) {
								cont.stop();
							}
							
							switch (ticks) {
							
							case 2:
								ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*4, dir.getStepY()*4, 1, 0, dir);
								mask.setCustomSpeed(90, 0);
								
								player.setForceMovementRaw(mask);
								break;
								
							case 5:
								cont.stop();
								player.getPA().movePlayer(player.getCurrentLocation().transform(dir, 4));
								break;
							
							
							}
						}
					}, 1);
				}
				
			}
		}, 1);
	}

}
