package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class DenseForestPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 3937);
		map(ObjectClickExtension.class, 3938);
		map(ObjectClickExtension.class, 3939);
		map(ObjectClickExtension.class, 3998);
		map(ObjectClickExtension.class, 3999);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		if (player.getSkills().getLevel(Skills.AGILITY) < 56) {
			player.sendMessage("You need agility level 56 to cross this log.");
			return;
		}
		
		
		player.lockActions(5);
		player.lockMovement(5);
		
		final Direction dir = Direction.getLogicalDirectionForEntity(player, object);
		
		final int steps = 3;
		
		final Location endLoc = player.getCurrentLocation().transform(dir, steps);
		
		final ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*steps, dir.getStepY()*steps, 1, 0, dir).setCustomSpeed(70, 0);
		
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					player.startAnimation(AgilityAnimations.CRAWL_OVER_FENCE);
					player.setForceMovementRaw(mask);
					break;

				case 4:
					container.stop();
					player.lockActions(0);
					player.lockMovement(0);
					player.getPA().movePlayer(endLoc);
					break;
				
				}
				
			}
			
		}, 1);
		
	}
	
}
