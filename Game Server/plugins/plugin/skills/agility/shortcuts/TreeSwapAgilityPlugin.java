package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class TreeSwapAgilityPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 5005);
	}
	
	private static final Location NORTH_LOC = Location.create(3503, 3431);
	private static final Location SOUTH_LOC = Location.create(3502, 3425);
	
	private static final Location NORTH_TREE_LOC = Location.create(3502, 3431);
	private static final Location SOUTH_TREE_LOC = Location.create(3502, 3426);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		if (player.getSkills().getLevel(Skills.AGILITY) < 5) {
			player.sendMessage("You need agility level 5 to cross this bridge.");
			return;
		}
		
		
		player.lockActions(10);
		player.lockMovement(10);
		
		final boolean goingSouth = object.getY() == NORTH_TREE_LOC.getY();
		
		final Direction dir = goingSouth ? Direction.SOUTH : Direction.NORTH;
		
		final int steps = 3;
		
		final Animation walkAnim = new Animation(player.getMovement().getWalkAnim());
		
		final Location endLoc = goingSouth ? SOUTH_LOC : NORTH_LOC;
		
		final Location walkStartLoc = goingSouth ? NORTH_TREE_LOC.transform(Direction.SOUTH) : SOUTH_TREE_LOC.transform(Direction.NORTH);
		
		final ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*steps, dir.getStepY()*steps, steps, 0, dir);
		
		player.sendMessageSpam("You climb the tree.");

		player.startAnimation(Animation.CLIMB_UP_DELAYED);
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					break;
					
				case 2:
					player.getPA().movePlayer(walkStartLoc);
					break;
					
				case 3:
					player.setForceMovementRaw(mask);
					if (!goingSouth)
						player.startAnimation(walkAnim);
					break;
					
				case 4:
				case 5:
					if (!goingSouth)
						player.startAnimation(walkAnim);//cheaphax XD
					break;

				case 6:
					player.startAnimation(Animation.CLIMB_UP);
					break;
					
				case 7:
					container.stop();
					player.lockActions(0);
					player.lockMovement(0);
					player.getPA().movePlayer(endLoc);
					break;
				
				}
				
			}
			
		}, 1);
		
	}
	
}