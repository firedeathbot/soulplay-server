package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ArdyLogBalance extends Plugin implements ObjectClickExtension {

	@Override
    public void onInit() {
        map(ObjectClickExtension.class, 9328);
        map(ObjectClickExtension.class, 9330);
        map(ObjectClickExtension.class, 2296);
    }
	
	@Override
    public void onObjectOption1(Player player, GameObject o) {
		if (player.getSkills().getLevel(Skills.AGILITY) < 33) {
			player.sendMessage("You need Agility level of 33 to use this shortcut.");
			return;
		}
		if (o == null || !o.isActive()) {
			return;
		}
		
		Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), o.getLocation());
		
		ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, dir).addSecondDirection(dir.getStepX()*4, dir.getStepY()*4, 3, AgilityAnimations.LOG_WALK_DELAYED.getId()).setLockActionsTick(3).setWalkStanceId(AgilityAnimations.LOG_WALK_DELAYED.getId());
		player.setForceMovement(mask);
		player.startAnimation(AgilityAnimations.LOG_WALK_DELAYED);
		player.getPA().addSkillXP(4, Skills.AGILITY);
    }
}
