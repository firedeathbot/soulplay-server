package plugin.skills.agility.shortcuts;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class JuttingWallPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 26768);
	}

	private static final Location LOC_NORTH1 = Location.create(9466, 10149, 3);
	private static final Location LOC_SOUTH1 = Location.create(9466, 10147, 3);

	private static final Animation LEAN_WALL_NORTH = new Animation(752);
	private static final Animation LEAN_WALL_NORTH_DELAY = new Animation(752, 20);

	private static final Animation LEAN_WALL_SOUTH = new Animation(753);
	private static final Animation LEAN_WALL_SOUTH_DELAY = new Animation(753, 20);

	private static final Animation LEAN_WALL_NORTH_END = new Animation(Animation.getOsrsAnimId(758), 20);
	private static final Animation LEAN_WALL_SOUTH_END = new Animation(Animation.getOsrsAnimId(759), 20);

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);

		if (player.getSkills().getLevel(Skills.AGILITY) < 60) {
			player.sendMessage("You need Agility level 60 to pass this obstacle.");
			return;
		}

		player.lockActions(5);
		player.lockMovement(5);


		final int originalStance = player.getMovement().getStandAnim();

		final Location LOC;

		final boolean movingSouth = player.getY() == 10149;

		if (movingSouth) {
			LOC = LOC_SOUTH1;
			player.startAnimation(player.getMovement().isRunning() ? LEAN_WALL_SOUTH_DELAY : LEAN_WALL_SOUTH);
		} else {
			LOC = LOC_NORTH1;
			player.startAnimation(player.getMovement().isRunning() ? LEAN_WALL_NORTH_DELAY : LEAN_WALL_NORTH);
		}

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 10) {
					container.stop();
					return;
				}

				switch (ticks) {

				case 1:
					player.getMovement().changeStance(Animation.getOsrsAnimId(movingSouth ? 756 : 755));
					break;

				case 2:
					player.setForceMovementRaw(movingSouth ? MASK_SOUTH1 : MASK_NORTH2);
					break;

				case 3:
					player.startAnimation(movingSouth ? LEAN_WALL_SOUTH_END : LEAN_WALL_NORTH_END);
					player.getMovement().changeStance(originalStance);
					break;

				case 4:
					container.stop();
					player.getPA().addSkillXP(5, Skills.AGILITY);
					player.lockActions(0);
					player.lockMovement(0);
					player.getPA().movePlayer(LOC);
					break;

				}

			}

		}, 1);

	}

	private static final ForceMovementMask MASK_SOUTH1 = ForceMovementMask.createMask(0, -2, 1, 0, Direction.NORTH_EAST).setCustomSpeed(60, 0);
	private static final ForceMovementMask MASK_NORTH2 = ForceMovementMask.createMask(0, 2, 1, 0, Direction.NORTH_EAST).setCustomSpeed(60, 0);

}
