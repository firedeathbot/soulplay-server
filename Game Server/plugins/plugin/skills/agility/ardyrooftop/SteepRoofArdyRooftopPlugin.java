package plugin.skills.agility.ardyrooftop;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class SteepRoofArdyRooftopPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 128912);
	}

	private static final Location LOC = Location.create(2656, 3297, 3);

	public static final Animation START_ANIM = new Animation(Animation.getOsrsAnimId(753), 0);

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);

		if (player.getX() == 2653 && player.getY() == 3300) {

			player.lockActions(10);
			player.lockMovement(10);


			final int originalStance = player.getMovement().getStandAnim();

			CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
				int ticks = 0;
				@Override
				public void stop() {
				}

				@Override
				public void execute(CycleEventContainer container) {
					ticks++;

					if (ticks > 20) {
						container.stop();
						return;
					}

					switch (ticks) {

					case 1:
						player.faceLocation(LOC);
						player.startAnimation(START_ANIM);
						player.getMovement().changeStance(Animation.getOsrsAnimId(756));
						break;

					case 2:
						player.setForceMovementRaw(MASK);
						break;

					case 4:
						player.startAnimation(Animation.getOsrsAnimId(759), 20);
						player.getMovement().changeStance(originalStance);
						break;

					case 5:
						container.stop();
						player.getPA().addSkillXP(57, Skills.AGILITY);
						player.lockActions(0);
						player.lockMovement(0);
						player.getPA().movePlayer(LOC);
						break;

					}

				}

			}, 1);

		} else {
			player.sendMessage("I can't reach that.");
		}

	}

	private static final ForceMovementMask MASK = ForceMovementMask.createMask(3, -3, 1, 0, Direction.NORTH_EAST).setCustomSpeed(90, 0);

}
