package plugin.skills.agility.ardyrooftop;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class PlankArdyRooftopPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 126635);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(object.getLocation());
		
		
		player.lockActions(10);
		player.lockMovement(10);
		
		
		final int originalStance = player.getMovement().getStandAnim();
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					player.startAnimation(AgilityAnimations.LOG_WALK2.getId(), 0);
//					player.getMovement().changeStance(AgilityAnimations.LOG_WALK3.getId());
					player.setForceMovementRaw(MASK1);
					break;

				case 7:
					player.startAnimation(65535);
					break;

				case 8:
					container.stop();
					player.getMovement().changeStance(originalStance);
					player.getPA().addSkillXP(50, Skills.AGILITY);
					player.lockActions(0);
					player.lockMovement(0);
					player.getPA().movePlayer(LOC);
					break;
				
				}
				
			}
			
		}, 1);
		
	}
	
	private static final Location LOC = Location.create(2656, 3318, 3);
	
	private static final ForceMovementMask MASK1 = ForceMovementMask.createMask(-6, 0, 1, 0, Direction.WEST).setCustomSpeed(180, 0);
	
}
