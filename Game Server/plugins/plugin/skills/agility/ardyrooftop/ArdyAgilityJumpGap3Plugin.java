package plugin.skills.agility.ardyrooftop;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ArdyAgilityJumpGap3Plugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 115611);
	}
	
	private static final Location LOC = Location.create(2651, 3309, 3);
	
	private static final Animation JUMP_ANIM = new Animation(AgilityAnimations.LAND_OSRS.getId(), 25);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(LOC);
		
//		if (player.getSkills().getLevel(Skills.AGILITY) < 90) {
//			player.getDialogueBuilder()
//			.sendStatement("You need agility level 90 to use this obstacle.")
//			.executeIfNotActive();
//			return;
//		}
		
		player.lockActions(5);
		player.lockMovement(5);
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {

				case 1:
					player.startAnimation(JUMP_ANIM);
					break;
				case 2:
					container.stop();
					player.getPA().addSkillXP(28, Skills.AGILITY);
					player.getPA().movePlayer(LOC);
					player.lockActions(0);
					player.lockMovement(0);
					break;

				}

			}
			
		}, 1);
		
	}
	
}
