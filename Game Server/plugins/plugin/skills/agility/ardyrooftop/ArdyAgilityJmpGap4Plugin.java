package plugin.skills.agility.ardyrooftop;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ArdyAgilityJmpGap4Plugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 115612);
	}

	private static final Location LOC1 = Location.create(2658, 3298, 1);

	private static final Location LOC2 = Location.create(2663, 3297, 1);
	
	private static final Location LOC_FINAL = Location.create(2668, 3297, 0);
	
	private static final Animation JUMP_ANIM = new Animation(AgilityAnimations.LAND_OSRS.getId(), 29);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(player.getX()+1, player.getY());
		
		player.lockActions(10);
		player.performingAction = true;
		
		player.blockRun = true;
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
				player.performingAction = false;
				player.blockRun = false;
				player.noClip = false;
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					player.startAnimation(JUMP_ANIM);
					break;
					
				case 2:
					player.getPA().movePlayer(LOC1);
					break;
					
				case 4:
					player.getPA().walkTo(3, 0);
					break;
					
				case 8:
					player.startAnimation(Animation.getOsrsAnimId(741), 0);
					player.setForceMovementRaw(MASK1);
					break;
					
				case 9:
					player.getPA().movePlayerInstant(LOC2.getX(), LOC2.getY(), LOC2.getZ());
					player.noClip = true;
					player.getPA().walkTo(3, 0);
					break;

				case 14:
					player.startAnimation(LAST_ANIM);
					player.setForceMovementRaw(MASK2);
					break;
					
				case 15:
					player.startAnimation(LAST_ANIM2);
					player.setForceMovementRaw(MASK3);

					break;
					
				case 16:
					player.getPA().movePlayer(LOC_FINAL);
					break;
					

				case 17:
					container.stop();
					player.getPA().addSkillXP(529, Skills.AGILITY);
					player.lockActions(0);
					player.lockMovement(0);
					int amount = 10;
					player.rewardAgilityPoints(amount);
					break;
				
				}
				
			}
			
		}, 1);
		
	}
	
	private static final Animation LAST_ANIM2 = new Animation(Animation.getOsrsAnimId(2588), 12);
	
	private static final Animation LAST_ANIM = new Animation(Animation.getOsrsAnimId(741), 0);
	
	private static final ForceMovementMask MASK1 = ForceMovementMask.createMask(0, 0, 1, 0, Direction.EAST).setCustomSpeed(10, 20).addSecondDirection(2, -1, 1, 0);
	
	private static final ForceMovementMask MASK2 = ForceMovementMask.createMask(0, 0, 1, 0, Direction.EAST).setCustomSpeed(20, 30).addSecondDirection(1, 0, 1, 0);
			
	private static final ForceMovementMask MASK3 = ForceMovementMask.createMask(0, 0, 1, 0, Direction.EAST).setCustomSpeed(8, 18).addSecondDirection(1, 0, 1, 0).setNewOffset(2667, 3297);
	
}
