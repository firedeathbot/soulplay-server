package plugin.skills.agility.ardyrooftop;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class ArdyAgilityJumpGap1Plugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 115609);
	}
	
	private static final Location LOC1 = Location.create(2667, 3311, 1);
	
	private static final Location LOC2 = Location.create(2665, 3315, 1);
	
	private static final Location LOC3 = Location.create(2665, 3318, 3);
	
	private static final Location FAIL_LOC = Location.create(2670, 3309, 0);
	
	private static final Animation JUMP_ANIM1 = new Animation(AgilityAnimations.LAND_OSRS.getId(), 28);
	
	private static final Animation JUMP_ANIM2 = new Animation(AgilityAnimations.LAND_OSRS.getId(), 23);
	
	private static final Animation JUMP_ANIM3 = new Animation(AgilityAnimations.LAND_FROZEN_OSRS.getId(), 25);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(LOC1);
		
//		if (player.getSkills().getLevel(Skills.AGILITY) < 90) {
//			player.getDialogueBuilder()
//			.sendStatement("You need agility level 90 to use this obstacle.")
//			.executeIfNotActive();
//			return;
//		}
		
		player.lockActions(10);
		player.lockMovement(10);
		
		int playerRate = Misc.interpolate(50, 285, 1, 104, player.getSkills().getLevel(Skills.AGILITY));
		
		int random = Skills.randomBaseChance();
		
		final boolean failed = random > playerRate;
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {

				case 1:
					if (!failed)
						player.startAnimation(JUMP_ANIM1);
					else {
						ticks = 10;
						player.startAnimation(AgilityAnimations.FALL_LEFT);
					}
					break;
				case 2:
					player.faceLocation(LOC2);
					player.getPA().movePlayer(LOC1);
					break;
				case 3:
					player.startAnimation(JUMP_ANIM2);
					break;
					
				case 4:
					//player.faceLocation(loc33);
					player.getPA().movePlayer(LOC2);
					break;
				case 5:
					player.startAnimation(JUMP_ANIM3);
					break;
				case 6:
					container.stop();
					player.getPA().addSkillXP(65, Skills.AGILITY);
					player.getPA().movePlayer(LOC3);
					player.lockActions(0);
					player.lockMovement(0);
					break;

				case 11: // fail
					container.stop();
					player.getPA().movePlayer(FAIL_LOC);
					player.dealTrueDamage(Misc.random(4,10), 0, null, null);
					player.lockActions(0);
					player.lockMovement(0);
					break;

				}

			}
			
		}, 1);
		
	}
	
}
