package plugin.skills.agility.ardyrooftop;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class WoodenBeamsPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 115608);
	}
	
	public static Animation ANIMATION = new Animation(AgilityAnimations.CLIMB_WALL_OSRS.getId(), 30);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(player.getX(), player.getY()+1);
		
		if (player.getSkills().getLevel(Skills.AGILITY) < 90) {
			player.getDialogueBuilder()
			.sendStatement("You need agility level 90 to use this obstacle.")
			.executeIfNotActive();
			return;
		}
		
		player.lockActions(5);
		player.lockMovement(5);
		
		final int stance = player.getMovement().getStandAnim();
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
				player.getMovement().changeStance(stance);
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {

				case 1:
					player.startAnimation(ANIMATION);
					break;
				case 2:
					player.startAnimation(AgilityAnimations.CLIMB_WALL_OSRS);
					player.getPA().movePlayer(player.getX(), player.getY(), player.getZ()+1);
					break;
				case 3:
					player.getPA().movePlayer(player.getX(), player.getY(), player.getZ()+1);
					break;

				case 4:
					container.stop();
					player.startAnimation(AgilityAnimations.LAND_OSRS);
					player.getPA().addSkillXP(43, Skills.AGILITY);
					player.getPA().movePlayer(END_LOC);
					player.lockActions(0);
					player.lockMovement(0);
					break;

				}

			}
			
		}, 1);
		
	}
	
	private static final Location END_LOC = Location.create(2671, 3299, 3);

}
