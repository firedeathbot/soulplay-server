package plugin.skills.crafting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.crafting.CutGems;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class GemCutPlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for (CutGems gem : CutGems.values) {
			map(ItemOnItemExtension.class, gem.getUncut(), 1755);
			map(ItemOnItemExtension.class, 1755, gem.getUncut());
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		if (player.skillingEvent != null) {
			return;
		}

		int gemId = -1;
		if (used.getId() == 1755) {
			gemId = with.getId();
		} else if (with.getId() == 1755) {
			gemId = used.getId();
		}

		if (gemId == -1) {
			return;
		}

		final CutGems gem = CutGems.forId(gemId);

		if (gem == null) {
			return;
		}

		if (gem.getLevel() > player.getSkills().getLevel(Skills.CRAFTING)) {
			player.sendMessage("You need a crafting level of " + gem.getLevel() + " to cut this gem.");
			return;
		}

		String message = "You cut the " + ItemProjectInsanity.getItemName(gem.getGem()) + ".";

		player.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (player.getItems().deleteItem2(gem.getUncut(), 1)) {
					player.startAnimation(gem.getAnimation());
					player.getItems().addItem(gem.getGem(), 1);
					player.getPA().addSkillXP(gem.getExp(), Skills.CRAFTING);
					player.sendMessageSpam(message);
				} else {
					container.stop();
				}
			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 1);
	}

}
