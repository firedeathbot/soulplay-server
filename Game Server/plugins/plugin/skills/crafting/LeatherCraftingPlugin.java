package plugin.skills.crafting;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.crafting.Crafting;
import com.soulplay.content.player.skills.crafting.LeatherCraftingData;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;
import com.soulplay.util.interfacedata.BitShifting;

public class LeatherCraftingPlugin extends Plugin implements ButtonClickExtension, ItemOnItemExtension {

	private static final int LEATHER_ID = 1741;

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 33187);
		map(ButtonClickExtension.class, 33186);
		map(ButtonClickExtension.class, 33185);

		map(ButtonClickExtension.class, 33190);
		map(ButtonClickExtension.class, 33189);
		map(ButtonClickExtension.class, 33188);

		map(ButtonClickExtension.class, 33193);
		map(ButtonClickExtension.class, 33192);
		map(ButtonClickExtension.class, 33191);

		map(ButtonClickExtension.class, 33196);
		map(ButtonClickExtension.class, 33195);
		map(ButtonClickExtension.class, 33194);

		map(ButtonClickExtension.class, 33199);
		map(ButtonClickExtension.class, 33198);
		map(ButtonClickExtension.class, 33197);

		map(ButtonClickExtension.class, 33202);
		map(ButtonClickExtension.class, 33201);
		map(ButtonClickExtension.class, 33200);

		map(ButtonClickExtension.class, 33205);
		map(ButtonClickExtension.class, 33204);
		map(ButtonClickExtension.class, 33203);

		map(ItemOnItemExtension.class, LEATHER_ID, Crafting.NEEDLE);
		map(ItemOnItemExtension.class, Crafting.NEEDLE, LEATHER_ID);
	}

	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getPacketSender().showInterface(2311);
		updateInterfaceColors(player, 8620, LeatherCraftingData.BODY);
		updateInterfaceColors(player, 8622, LeatherCraftingData.GLOVES);
		updateInterfaceColors(player, 8624, LeatherCraftingData.BOOTS);
		updateInterfaceColors(player, 8626, LeatherCraftingData.VAMBRACES);
		updateInterfaceColors(player, 8628, LeatherCraftingData.CHAPS);
		updateInterfaceColors(player, 8630, LeatherCraftingData.COIF);
		updateInterfaceColors(player, 8632, LeatherCraftingData.COWL);
	}

	private void updateInterfaceColors(Player player, int id, LeatherCraftingData data) {
		int color = 0x00f000;
		if (data.getLevelReq() > player.getSkills().getLevel(Skills.CRAFTING)) {
			color = 0xf80000;
		}

		player.getPacketSender().setTextColor(id, BitShifting.compressHexColor(color));
	}

	@Override
	public void onClick(Player player, int button, int component) {
		if (player.interfaceIdOpenMainScreen != 2311) {
			return;
		}

		switch (button) {
		case 33187:
			startCraft(player, LeatherCraftingData.BODY, 1);
			break;
		case 33186:
			startCraft(player, LeatherCraftingData.BODY, 5);
			break;
		case 33185:
			startCraft(player, LeatherCraftingData.BODY, 10);
			break;

		case 33190:
			startCraft(player, LeatherCraftingData.GLOVES, 1);
			break;
		case 33189:
			startCraft(player, LeatherCraftingData.GLOVES, 5);
			break;
		case 33188:
			startCraft(player, LeatherCraftingData.GLOVES, 10);
			break;

		case 33193:
			startCraft(player, LeatherCraftingData.BOOTS, 1);
			break;
		case 33192:
			startCraft(player, LeatherCraftingData.BOOTS, 5);
			break;
		case 33191:
			startCraft(player, LeatherCraftingData.BOOTS, 10);
			break;

		case 33196:
			startCraft(player, LeatherCraftingData.VAMBRACES, 1);
			break;
		case 33195:
			startCraft(player, LeatherCraftingData.VAMBRACES, 5);
			break;
		case 33194:
			startCraft(player, LeatherCraftingData.VAMBRACES, 10);
			break;

		case 33199:
			startCraft(player, LeatherCraftingData.CHAPS, 1);
			break;
		case 33198:
			startCraft(player, LeatherCraftingData.CHAPS, 5);
			break;
		case 33197:
			startCraft(player, LeatherCraftingData.CHAPS, 10);
			break;

		case 33202:
			startCraft(player, LeatherCraftingData.COIF, 1);
			break;
		case 33201:
			startCraft(player, LeatherCraftingData.COIF, 5);
			break;
		case 33200:
			startCraft(player, LeatherCraftingData.COIF, 10);
			break;

		case 33205:
			startCraft(player, LeatherCraftingData.COWL, 1);
			break;
		case 33204:
			startCraft(player, LeatherCraftingData.COWL, 5);
			break;
		case 33203:
			startCraft(player, LeatherCraftingData.COWL, 10);
			break;
		}
	}

	private static boolean canCraft(Player c, LeatherCraftingData data) {
		if (!c.getItems().playerHasItem(Crafting.NEEDLE)) {
			c.sendMessage("You don't have a needle to craft with.");
			return false;
		}

		if (c.getPlayerLevel()[Skills.CRAFTING] < data.getLevelReq()) {
			c.sendMessage("You need a crafting level of " + data.getLevelReq() + " to craft this.");
			return false;
		}

		if (!c.getItems().playerHasItem(LEATHER_ID, 1)) {
			c.getDialogueBuilder().sendStatement("You don't have enough materials to craft.").execute();
			return false;
		}

		return true;
	}

	private static void startCraft(Player player, LeatherCraftingData data, int cyclesStart) {
		player.getPA().closeAllWindows();

		if (!canCraft(player, data) || player.skillingEvent != null) {
			return;
		}

		player.startAction(new CycleEvent() {

			int cycles = cyclesStart;

			@Override
			public void execute(CycleEventContainer container) {
				int productId = data.getProduct();
				player.getItems().deleteItem2(LEATHER_ID, 1);
				player.startAnimation(Crafting.LEATHER_CRAFT_ANIM);
				player.getItems().addItem(productId, 1);
				player.getPA().addSkillXP(data.getXp(), Skills.CRAFTING);
				player.sendMessageSpam("You craft " + ItemDef.forID(productId).getName() + ".");
				
				player.increaseProgress(TaskType.CRAFTING, productId);

				cycles--;
				if (cycles == 0 || !canCraft(player, data)) {
					container.stop();
					return;
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 3);
	}

}
