package plugin.skills.crafting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.crafting.BattleStaves;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class CraftBattleStavesPlugin extends Plugin implements  ItemOnItemExtension {

	public static final int BATTLE_STAFF = 1391;
	
	@Override
	public void onInit() {
		
		for (BattleStaves staff : BattleStaves.VALUES) {
			map(ItemOnItemExtension.class, BATTLE_STAFF, staff.getObelisk());
			map(ItemOnItemExtension.class, staff.getObelisk(), BATTLE_STAFF);
		}
		
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		
		BattleStaves orbEnum = BattleStaves.forId(used.getId() == BATTLE_STAFF ? with.getId() : used.getId());
		
		if (player.getSkills().getLevel(Skills.CRAFTING) < orbEnum.getLevel()) {
			player.sendMessage("You need a crafting level of " + orbEnum.getLevel() + " to make this.");
			return;
		}
		
		if (player.getItems().deleteItem(used) && player.getItems().deleteItem(with)) {
			player.getItems().addItem(orbEnum.getProduct(), 1);
			player.getPA().addSkillXP((int)orbEnum.getExp(), Skills.CRAFTING);
		}
		
		
		
	}
}
