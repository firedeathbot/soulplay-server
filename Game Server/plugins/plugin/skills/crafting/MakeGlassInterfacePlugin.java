package plugin.skills.crafting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.crafting.MoltenGlassEnum;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class MakeGlassInterfacePlugin extends Plugin implements ButtonClickExtension, ItemOnItemExtension {
	
	public static final int GLASSBLOWPING_PIPE = 1785;
	public static final Animation ANIM = new Animation(884);

	@Override
	public void onInit() {
		for (MoltenGlassEnum glass : MoltenGlassEnum.values()) {
			map(ButtonClickExtension.class, glass.getButtonId());
			map(ButtonClickExtension.class, glass.getMake5());
			map(ButtonClickExtension.class, glass.getMake10());
			map(ButtonClickExtension.class, glass.getMakeX());
		}
		
		
		map(ItemOnItemExtension.class, GLASSBLOWPING_PIPE, MoltenGlassMakePlugin.MOLTEN_GLASS);
		map(ItemOnItemExtension.class, MoltenGlassMakePlugin.MOLTEN_GLASS, GLASSBLOWPING_PIPE);
		
		MoltenGlassEnum.init();
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getPacketSender().showInterface(11462);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		final MoltenGlassEnum glass = MoltenGlassEnum.get(button);
		
		if (glass.getMakeX() == button) {
			player.getDialogueBuilder().sendEnterAmount(()-> {
				int amount = Math.min(28, Math.max(0, (int)player.enterAmount));
				makeGlassProduct(player, glass, amount);
			}).executeIfNotActive();
			return;
		}
		
		int amount = 1;
		
		if (glass.getMake10() == button)
			amount = 10;
		else if (glass.getMake5() == button)
			amount = 5;

		makeGlassProduct(player, glass, amount);
	}
	
	public static void makeGlassProduct(Player player, MoltenGlassEnum glass, int amount) {
		
		player.getPA().closeAllWindows();
		
		player.startAnimation(ANIM);
		
		player.startAction(new CycleEvent() {
			int makeCount = 0;
			@Override
			public void stop() {
				player.skillingEvent = null;
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (checkReqs(player, glass)) {
					
					player.startAnimation(ANIM);
					
					player.getItems().deleteItem2(MoltenGlassMakePlugin.MOLTEN_GLASS, 1);
					player.getItems().addItem(glass.getProductId(), 1);
					player.getPA().addSkillXP(glass.getExp(), Skills.CRAFTING);

					String name = null;
					ItemDefinition itemDef = ItemDefinition.forId(glass.getProductId());
					if (itemDef != null) {
						name = itemDef.getName().toLowerCase().replace("unpowered", "").trim();
					}

					if (name != null) {
						player.sendMessageSpam("You make a " + name + ".");
					}
				} else {
					container.stop();
					return;
				}
				makeCount++;
				if (makeCount >= amount) {
					container.stop();
				}
			}
		}, 2);
	}
	
	private static boolean checkReqs(Player player, MoltenGlassEnum glass) {
		
		if (!player.getItems().playerHasItemStrict(MoltenGlassMakePlugin.MOLTEN_GLASS, GLASSBLOWPING_PIPE)) {
			player.sendMessageSpam("You don't have the required items.");
			return false;
		}
		
		if (player.getSkills().getLevel(Skills.CRAFTING) < glass.getLvl()) {
			player.sendMessage("You need a crafting level of " + glass.getLvl() + " to make this.");
			return false;
		}
		
		return true;
	}
	
	
}
