package plugin.skills.crafting;

import com.soulplay.content.player.skills.crafting.TanHide;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class TanHidePlugin extends Plugin implements ButtonClickExtension, ItemOnItemExtension {

	public static final int GLASSBLOWPING_PIPE = 1785;
	public static final Animation ANIM = new Animation(884);

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 57225);
		map(ButtonClickExtension.class, 57226);
		map(ButtonClickExtension.class, 57227);
		map(ButtonClickExtension.class, 57228);
		map(ButtonClickExtension.class, 57229);
		map(ButtonClickExtension.class, 57230);
		map(ButtonClickExtension.class, 57231);
		map(ButtonClickExtension.class, 57232);
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		TanHide.openTanning(player);
	}

	@Override
	public void onClick(Player player, int button, int component) {
		TanHide.handleActionButton(player, button);
	}

}
