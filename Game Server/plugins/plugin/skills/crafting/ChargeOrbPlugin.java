package plugin.skills.crafting;

import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.MagicOnObjectExtension;

public class ChargeOrbPlugin extends Plugin implements MagicOnObjectExtension {

	@Override
	public void onInit() {
		map(MagicOnObjectExtension.class, 2151);
		map(MagicOnObjectExtension.class, 29415);
		map(MagicOnObjectExtension.class, 2153);
		map(MagicOnObjectExtension.class, 2152);
	}
	
	@Override
	public void magicOnObject(Player player, int spellId, GameObject object) {
		
		SpellsData spell = SpellsData.getSpell(player, spellId);
		
		if (spell == null)
			return;
		
		switch (object.getId()) {
		
		case 2151: // water obelisk
			if (spellId == SpellsData.CHARGE_WATER_ORB.getButtonId()) {
				startCharging(player, spell);
				return;
			}
			break;

		case 29415: // earth obelisk
			if (spellId == SpellsData.CHARGE_EARTH_ORB.getButtonId()) {
				startCharging(player, spell);
				return;
			}
			break;

		case 2153: // fire obelisk
			if (spellId == SpellsData.CHARGE_FIRE_ORB.getButtonId()) {
				startCharging(player, spell);
				return;
			}
			break;

		case 2152: // air obelisk
			if (spellId == SpellsData.CHARGE_AIR_ORB.getButtonId()) {
				startCharging(player, spell);
				return;
			}
			break;
			
		}

		player.sendMessage("You can't cast this spell on this object!");
		
	}
	
	private static void startCharging(Player player, SpellsData spell) {
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
				
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!MagicRequirements.checkMagicReqsNew(player, spell, true)) {
					container.stop();
					return;
				}
				
				player.getPA().addSkillXP(spell.getExpGain(), Skills.MAGIC);
				
				player.startAnimation(spell.getAnim());
				
				player.startGraphic(spell.getStartGfx());
				
				switch (spell) {
				case CHARGE_WATER_ORB:
					player.getItems().addItem(571, 1);
					break;
					
				case CHARGE_EARTH_ORB:
					player.getItems().addItem(575, 1);
					break;
					
				case CHARGE_FIRE_ORB:
					player.getItems().addItem(569, 1);
					break;
					
				case CHARGE_AIR_ORB:
					player.getItems().addItem(573, 1);
					break;
					
					default:
						break;
				}
			}
		}, 3);
	}
	
}
