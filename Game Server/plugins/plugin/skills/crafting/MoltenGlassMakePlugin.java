package plugin.skills.crafting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;

public class MoltenGlassMakePlugin extends Plugin implements ItemOnObjectExtension {

	private static final int[] OBJECTS = { 4304, 6189, 11010, 11666, 12100, 12809, 18497, 26814, 30021	, 30510, 36956	, 37651, 124009	};
	
	public static final Animation ANIM = new Animation(899);

	public static final int MOLTEN_GLASS = 1775;
	public static final int SODA_ASH = 1781;
	public static final int BUCEKT_OF_SAND = 1783;
	
	@Override
	public void onInit() {
		for (int objId : OBJECTS) {
			map(ItemOnObjectExtension.class, BUCEKT_OF_SAND, objId);
			map(ItemOnObjectExtension.class, SODA_ASH, objId);
		}
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		player.lockActions(3);
		player.lockMovement(3);
		player.startAnimation(ANIM);
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer cont) {
				cont.stop();
				if (checkReqs(player)) {
					player.getItems().deleteItem2(BUCEKT_OF_SAND, 1); // bucket of sand
					player.getItems().deleteItem2(SODA_ASH, 1); // soda ash
					player.getItems().addItem(MOLTEN_GLASS, 1); // molten glass
					player.getItems().addItem(1925, 1); // bucket
					
					player.getPA().addSkillXP(20, Skills.CRAFTING);
					player.sendMessageSpam("You heat the sand and soda ash in the furnace to make glass.");
					
				}
			}
		}, 2, false);
	}
	
	public static boolean checkReqs(Player player) {
		if (player.getItems().playerHasItemStrict(BUCEKT_OF_SAND, SODA_ASH)) {
			player.sendMessage("You need at least one heap of soda ash and one bucket of sand to do this.");
			return false;
		}
		return true;
	}
	
}
