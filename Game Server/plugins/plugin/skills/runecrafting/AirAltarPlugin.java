package plugin.skills.runecrafting;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.runecrafting.Runecrafting;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.sounds.Sounds;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class AirAltarPlugin extends Plugin implements ItemOnObjectExtension, ObjectClickExtension {
	
	public static final int AIR_TIARA = 5527;
	public static final int AIR_TALISMAN = 1438;
	public static final int AIR_TALI_STAFF = 13630;
	public static final int RUNECRAFTING_STAFF = 13629;
	

	@Override
	public void onInit() {
		map(ItemOnObjectExtension.class, AIR_TIARA, 2452);
		map(ItemOnObjectExtension.class, AIR_TALISMAN, 2452);
		map(ItemOnObjectExtension.class, AIR_TALI_STAFF, 2452);
		map(ItemOnObjectExtension.class, 13642, 2452); //omni talisman staff

		map(ObjectClickExtension.class, 2465); // portal back out

		map(ItemOnObjectExtension.class, RUNECRAFTING_STAFF, 2478);
		map(ItemOnObjectExtension.class, Runecrafting.TIARA, 2478);
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		switch (itemId) {
			case RUNECRAFTING_STAFF:
				if (player.getItems().deleteItem2(AIR_TALISMAN, 1)) {
					if (player.getItems().deleteItem2(RUNECRAFTING_STAFF, 1)) {
						player.getItems().addItem(AIR_TALI_STAFF, 1);
						player.getPA().addSkillXP(25, Skills.RUNECRAFTING);
						player.getPacketSender().playSound(Sounds.STRANGE_SPELL);
					}
				} else {
					player.sendMessage("You need an air talisman to create an Air Talisman Staff.");
				}
				break;
			case Runecrafting.TIARA:
				if (!player.getItems().deleteItem2(AIR_TALISMAN, 1)) {
					player.sendMessage("You don't have an air talisman to imbue the tiara.");
					break;
				}

				player.getItems().deleteItem2(Runecrafting.TIARA, 1);
				player.getItems().addItem(AIR_TIARA, 1);
				player.sendMessage("You bind the air talisman into the tiara.");
				break;
			default:
				player.getPA().movePlayer(2841, 4829, 0);
				player.getPacketSender().playSound(Sounds.STRANGE_SPELL);
				break;
		}
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		player.getPA().movePlayer(2983, 3293, 0);
	}
}
