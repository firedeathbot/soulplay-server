package plugin.skills.hunter;

import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.content.player.skills.hunter.falconry.FalconNpc;
import com.soulplay.content.player.skills.hunter.falconry.Falconry;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;

public class GyrFalconPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 5094);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		if (npc instanceof FalconNpc) {
			FalconNpc falcon = (FalconNpc) npc;
			Falconry.pickupFalcon(player, falcon);
		}
	}
}
