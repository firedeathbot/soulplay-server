package plugin.skills.hunter;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.player.skills.hunterlesik.HunterTrapType;
import com.soulplay.content.player.skills.hunterlesik.box.BoxTrapEnum;
import com.soulplay.content.player.skills.hunterlesik.box.BoxTrapObject;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.GroundItemClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class BoxTrapPlugin extends Plugin implements ItemClickExtension, GroundItemClickExtension, ObjectClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, HunterTrapType.BOX_TRAP.getId());
		map(GroundItemClickExtension.class, HunterTrapType.BOX_TRAP.getId());
		map(ObjectClickExtension.class, BoxTrapObject.OBJECT_ID);
		map(ObjectClickExtension.class, BoxTrapObject.BROKEN_OBJECT_ID);
		
		map(ObjectClickExtension.class, BoxTrapEnum.GRAY.getTrappedId());
		map(ObjectClickExtension.class, BoxTrapEnum.RED.getTrappedId());
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		//lay trap
		BoxTrapObject.setupTrap(player, itemId, itemSlot);
		
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		//drop
		Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
	@Override
	public void onGroundItemOption2(Player player, int itemId, int itemX, int itemY) {
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (player.getX() == itemX && player.getY() == itemY) {
					container.stop();
					BoxTrapObject.setupTrap(player, itemId, -1);
				}
			}
		}, 1);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		
		player.faceLocation(object.getLocation());
		
		switch (object.getId()) {
		
		case BoxTrapObject.OBJECT_ID:
			//dismantle
			BoxTrapObject.dismantleTrap(player, object);
			break;
			
		case BoxTrapObject.BROKEN_OBJECT_ID: // rearm trap
			BoxTrapObject.rearmBrokenTrap(player, object);
			break;
			
		case BoxTrapObject.GRAY_TRAPPED_OBJECT_ID:
		case BoxTrapObject.RED_TRAPPED_OBJECT_ID:
			BoxTrapObject.collectChinchompa(player, object);
			break;
			
		}
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {

		player.faceLocation(object.getLocation());
		
		switch (object.getId()) {

		case BoxTrapObject.OBJECT_ID:
			//check trap
			player.startAnimation(Animation.DAB);
			player.sendMessage("There seems to be nothing there.");
			break;

		case BoxTrapObject.BROKEN_OBJECT_ID: // dismantle trap
			BoxTrapObject.dismantleTrap(player, object);
			break;

		}
	}
	
}
