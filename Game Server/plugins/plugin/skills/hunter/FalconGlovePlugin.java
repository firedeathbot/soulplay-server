package plugin.skills.hunter;

import com.soulplay.content.player.skills.hunter.falconry.Falconry;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class FalconGlovePlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, Falconry.GLOVE_FALCON);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().reset();
		player.getDialogueBuilder().sendDestoryItem(itemId).execute();
	}
}
