package plugin.skills.hunter;

import com.soulplay.content.player.skills.hunter.falconry.FalconCatch;
import com.soulplay.content.player.skills.hunter.falconry.Falconry;
import com.soulplay.content.player.skills.hunter.falconry.KebbitNpc;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class KebbitPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		for (FalconCatch f : FalconCatch.values)
			map(NpcClickExtension.class, f.getNpc());
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		if (npc instanceof KebbitNpc) {
			KebbitNpc kebbitNpc = (KebbitNpc) npc;
			Falconry.catchKebbit(player, kebbitNpc);
		}
	}
}
