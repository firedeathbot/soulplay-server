package plugin.skills.hunter;

import com.soulplay.content.player.skills.hunter.falconry.Falconry;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class MatthiasPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 5092);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		Falconry.getGloveFromMatt(player);
	}
	
	@Override
	public void onNpcOption2(Player player, NPC npc) {
		Falconry.getGloveFromMatt(player);
	}
	
}
