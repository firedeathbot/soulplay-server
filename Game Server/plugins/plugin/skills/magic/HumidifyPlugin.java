package plugin.skills.magic;

import com.soulplay.content.player.combat.magic.MagicRequirements;
import com.soulplay.content.player.combat.magic.Requirement;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

import java.util.*;

public class HumidifyPlugin extends Plugin implements ButtonClickExtension {

    private static final int ASTRAL_RUNE = 9075;
    private static final int WATER_RUNE = 555;
    private static final int FIRE_RUNE = 554;
	private static final Requirement REQUIREMENT = new Requirement(new GameItem(ASTRAL_RUNE, 1), new GameItem(WATER_RUNE, 3), new GameItem(FIRE_RUNE, 1), null, 68);

    private static final int CLAY = 434;
    private static final int SOFT_CLAY = 1761;

    private static final int EMPTY_BUCKET = 1925;
    private static final int BUCKET_OF_WATER = 1929;

    private static final int JUG = 1935;
    private static final int JUG_OF_WATER = 1937;

    private static final int VIAL = 229;
    private static final int VIAL_OF_WATER = 227;

    private static final int WATERING_CAN = 5331;
    private static final int WATERING_CAN_FULL = 5340;

    private static final Animation ANIMATION = new Animation(6294);
    private static final Graphic GRAPHIC = Graphic.create(1061);

    // LinkedHashSet provides O(1) iteration, O(1) lookup. HashMap is O(n) iteration
    private static final Map<Integer, Integer> map = new LinkedHashMap<>();

    public static void load() {
    	/* empty */
    }

    static {
        map.put(CLAY, SOFT_CLAY);
        map.put(EMPTY_BUCKET, BUCKET_OF_WATER);
        map.put(JUG, JUG_OF_WATER);
        map.put(VIAL, VIAL_OF_WATER);
        map.put(WATERING_CAN, WATERING_CAN_FULL);
    }

    @Override
    public void onInit() {
        map(ButtonClickExtension.class, 117054);
    }

    @Override
    public void onClick(Player player, int button, int component) {
        final List<Integer> items = new ArrayList<>();

        for (int key : map.keySet()) {
            if (player.getItems().playerHasItem(key)) {
                items.add(key);
            }
        }

        if (items.isEmpty()) {
            player.sendMessage("You do not have any items to cast this spell on!");
            return;
        }
        if (!player.getStopWatch().checkThenStart(3)) {
          	return;
        }

    	if (!MagicRequirements.checkMagicReqsNew((Client) player, REQUIREMENT, null, true)) {
    		return;
    	}

        player.startAnimation(ANIMATION);
        player.startGraphic(GRAPHIC);

        convertItems(player, items);
    }

    private static void convertItems(Player player, List<Integer> items) {
        for (int itemId : items) {
            final int addId = map.getOrDefault(itemId, -1);

            if (addId == -1) {
                continue;
            }

            final int amount = player.getItems().getItemAmount(itemId);

            player.getItems().deleteItem2(itemId, amount);
            player.getItems().addItem(addId, amount);
        }

        player.getPA().addSkillXP(68, Skills.MAGIC);
    }

}
