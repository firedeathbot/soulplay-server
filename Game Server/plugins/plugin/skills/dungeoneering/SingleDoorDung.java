package plugin.skills.dungeoneering;

import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonConstants;
import com.soulplay.content.player.skills.dungeoneeringv2.dungeon.DungeonManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class SingleDoorDung extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, DungeonConstants.SINGLE_DOOR_CLOSED);
		map(ObjectClickExtension.class, DungeonConstants.SINGLE_DOOR_OPEN);
		map(ObjectClickExtension.class, 51256); //Frozen closed
		map(ObjectClickExtension.class, 51257); //Frozen open
		map(ObjectClickExtension.class, 50708); //Abandon closed
		map(ObjectClickExtension.class, 50709); //Abandon open
		map(ObjectClickExtension.class, 50706); //Abandon closed
		map(ObjectClickExtension.class, 50707); //Abandon open
		map(ObjectClickExtension.class, 51807); //Furnished closed
		map(ObjectClickExtension.class, 51808); //Furnished open
		map(ObjectClickExtension.class, 51805); //Furnished closed
		map(ObjectClickExtension.class, 51806); //Furnished open
		map(ObjectClickExtension.class, 54769); //Occult closed
		map(ObjectClickExtension.class, 54770); //Occult open
		map(ObjectClickExtension.class, 54767); //Occult closed
		map(ObjectClickExtension.class, 54768); //Occult open
		map(ObjectClickExtension.class, DungeonConstants.WARPED_SINGLE_DOOR_CLOSED);
		map(ObjectClickExtension.class, DungeonConstants.WARPED_SINGLE_DOOR_OPEN);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (!player.insideDungeoneering()) {
			return;
		}
		DungeonManager dg = player.dungParty.dungManager;
		
//		GameObject object = player.dungParty.dungManager.getRealObject(objectType, obX, obY, player.getZ());
//		if (object == null) {
//			return;
//		}
		openDoor(player, object, dg);
	}
	
	public static void openDoor(Player p, GameObject o, DungeonManager dg) {
		final Location loc = o.getLocation();

		final boolean openingDoor = o.getDef().itemActions[0].equals("Open");

		//doors are always spawned in closed position so i'm assuming that closing the door will be changing the temp object
		if (!openingDoor) { // the door is in changed format, so revert it
			
			if (!o.isActive()) { // or use !o.isactive
				return;
			}
			
			GameObject temp2 = dg.getObjectManager().getObject(-1, o.getOriginalDoorX(), o.getOriginalDoorY(), o.getZ(), o.getType());
			
			if (temp2 != null)
				dg.getObjectManager().revertObject(temp2);
			
			dg.getObjectManager().revertObject(o);

		} else {
			
			//check if object is already changed
			if (dg.getObjectManager().getObject(-1, o.getX(), o.getY(), o.getZ()) != null) {
				return;
			}

			final int otherId = openingDoor ? o.getId() + 1 : o.getId() - 1;

			int face = openingDoor ? (o.getOrientation() + 1) % 4 : (o.getOrientation() + 3) % 4;

			Direction dir = CLOSED_DOOR_BY_FACE[o.getOrientation()];

			Location newLoc = loc.transform(dir);
			
			GameObject del = GameObject.createTickObject(-1, loc, o.getOrientation(), o.getType(), TICKS, o.getId());
			GameObject newDoor = GameObject.createTickObject(otherId, newLoc, face, o.getType(), TICKS, -1);
			
			newDoor.setOriginalDoorX(loc.getX());
			newDoor.setOriginalDoorY(loc.getY());
			
			dg.getObjectManager().addObject(del);
			dg.getObjectManager().addObject(newDoor);

//			System.out.println(String.format("curFace:%s newFace:%s", o.getOrientation(), face));
//			System.out.println(String.format("curLoc:%s newLoc:%s", o.getLocation(), newLoc));
//			System.out.println(String.format("" + otherId));
		}

	}
	
	private static final Direction[] CLOSED_DOOR_BY_FACE = { Direction.WEST, Direction.NORTH, Direction.EAST, Direction.SOUTH };

	private static final int TICKS = 100;
}
