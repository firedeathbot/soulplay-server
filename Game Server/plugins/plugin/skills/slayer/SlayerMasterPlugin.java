package plugin.skills.slayer;

import com.soulplay.content.player.skills.slayer.Master;
import com.soulplay.content.player.skills.slayer.Slayer;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

import java.util.Optional;

public class SlayerMasterPlugin extends Plugin implements NpcClickExtension {

    @Override
    public void onInit() {
        map(NpcClickExtension.class, Master.TURAEL.getId());
        map(NpcClickExtension.class, Master.MAZCHNA.getId());
        map(NpcClickExtension.class, Master.CHAELDAR.getId());
        map(NpcClickExtension.class, Master.DURADEL.getId());
        map(NpcClickExtension.class, Master.KURADAL.getId());
        map(NpcClickExtension.class, Master.MORVRAN.getId());
        map(NpcClickExtension.class, Master.SUMONA.getId());
        map(NpcClickExtension.class, Master.VANNAKA.getId());
        map(NpcClickExtension.class, Master.KRYSTILIA.getId());
        map(NpcClickExtension.class, Master.KONAR_QUO_MATEN.getId());
        map(NpcClickExtension.class, Master.NIEVE.getId());
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        Optional<Master> result = Master.lookup(npc.npcType);
        if (!result.isPresent()) {
            return;
        }

        final Master master = result.get();

        player.getDialogueBuilder().sendNpcChat(String.format("Hello there, I'm %s.", master.getName()), "Can I help you?")
                .sendOption("I would like to have a Slayer task", () -> {
                    Slayer.assignTask((Client)player, master);
                }, "Open your store please", () -> {
                    ((Client)player).getShops().openShop(54);
                }, "Open your rewards store please", () -> {
                    player.getDialogueBuilder().sendOption("Open Slayer Point Store", () -> {
                    	openSlayerShop(player);
                    }, "Open Duo Point Store", () -> {
                        ((Client)player).getShops().openShop(49);
                    });
                }, "Tell me about duo Slayer", () -> {
                    player.getDialogueBuilder().sendNpcChat("Duo Slayer means you can do a task with another player.",
                            "It's way more fun slaying together then doing it alone.",
                            "You can start by using an enchanted gem on the user you",
                            "would like to duo with, buy them from any Slayer master.");
                }, "Cancel my Slayer task", () -> {
                    if (player.getSlayerTask() == null || player.getSlayerTask().getAssignment() == null) {
                        player.sendMessage("You don't have a Slayer task.");
                    } else if (player.isIronMan() || player.getItems().takeCoins(100_000)){
                        player.setSlayerTasksCompleted(0);
                        player.getSlayerTask().cancel((Client)player);
                        player.getDialogueBuilder().sendNpcChat("I have reset your Slayer task.");
                    } else {
                        player.getDialogueBuilder().sendNpcChat("You need 100,000 gp to reset your Slayer task.");
                    }
                }).execute();
    }

    @Override
    public void onNpcOption2(Player player, NPC npc) {
        Optional<Master> result = Master.lookup(npc.npcType);
        if (!result.isPresent()) {
            return;
        }

        Slayer.assignTask((Client)player, result.get());
    }

    @Override
    public void onNpcOption3(Player player, NPC npc) {
        ((Client)player).getShops().openShop(54);
    }

    @Override
    public void onNpcOption4(Player player, NPC npc) {
        player.getDialogueBuilder().sendOption("Slayer Point Rewards", () -> {
        	openSlayerShop(player);
        }, "Slayer Duo Rewards", () -> {
            ((Client)player).getShops().openShop(49);
        }).execute();
    }

    private static void openSlayerShop(Player player) {
        player.sendMessage("You currently have <col=ff0000>" + player.getSlayerPoints().getTotalValue() + "</col> Slayer points.");
        if (player.isIronMan()) {
            ((Client)player).getShops().openShop(105);
        } else {
            ((Client)player).getShops().openShop(10);
        }
    }

}
