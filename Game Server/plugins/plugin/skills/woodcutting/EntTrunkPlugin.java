package plugin.skills.woodcutting;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.Woodcutting;
import com.soulplay.content.player.skills.woodcutting.Hatchets;
import com.soulplay.content.player.skills.woodcutting.Trees;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.util.Misc;

public class EntTrunkPlugin extends Plugin implements NpcClickExtension {

	private static final GameItem ADZE = new GameItem(13661, 1);
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, 4189);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		
		final Hatchets axe = Woodcutting.getHatchet(player);
		
		if (axe == null) {
			return;
		}
		
		List<Trees> list = new ArrayList<>();
		
		for (int i = 0; i < Trees.VALUES.length; i++) {
			Trees tree = Trees.VALUES[i];
			
			if (tree == Trees.IVY) continue;
			
			if (player.getSkills().getStaticLevel(Skills.WOODCUTTING) >= tree.getLvlReq())
				list.add(tree);
		}
		
		player.startAnimation(axe.getAnim());
		
		int rate = Misc.interpolateSafe(16, 6, 1, 160, player.getSkills().getStaticLevel(Skills.WOODCUTTING)+axe.getRate());
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				player.skillingTick++;
				
				if (!npc.isActive || npc.isDead()) {
					container.stop();
					return;
				}
				
				if (player.skillingTick % 6 == 0) {
					player.startAnimation(axe.getAnim());
				}
				
				if (player.skillingTick % rate == 0) {
					
//					if (Misc.randomBoolean(10)) {
//						npc.setDead(true);
//					}
					
					if (Misc.randomBoolean(15000)) {// small random chance for fun loot
						player.getItems().addOrDrop(ADZE);
					}
					
					player.getPA().addSkillXP(25, Skills.WOODCUTTING);
					
					Trees ranTree = Misc.random(list);
					
					int notedItemId = ranTree.getLogId()+1;
					
					if (ranTree == Trees.MAHOGANY) {
						notedItemId = 8836;
					}
					
					if (!player.getItems().addItem(notedItemId, 2)) {
						container.stop();
					}
					
					npc.getSkills().setLifepoints(npc.getSkills().getLifepoints()-1);
					
					if (npc.getSkills().getLifepoints() == 0) {
						container.stop();
						npc.setDead(true);
						
					}
					
				}
				
			}
		}, 1);
	}
	
}
