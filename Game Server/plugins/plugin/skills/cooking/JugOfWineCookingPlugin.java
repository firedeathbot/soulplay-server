package plugin.skills.cooking;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class JugOfWineCookingPlugin extends Plugin implements ItemOnItemExtension {

	public static final int EMPTY_JUG = 1935;
	public static final int JUG_OF_WATER = 1937;
	public static final int GRAPES = 1987;
	public static final int JUG_OF_BAD_WINE = 1991;
	public static final int JUG_OF_WINE = 1993;
	
	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, JUG_OF_WATER, GRAPES);
		map(ItemOnItemExtension.class, GRAPES, JUG_OF_WATER);
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		
		boolean goodCookingLevel = player.getSkills().getLevel(Skills.COOKING) >= 35;
		
		if (player.getItems().deleteItem(used) && player.getItems().deleteItem(with)) {
			player.getItems().addItem(goodCookingLevel ? JUG_OF_WINE : JUG_OF_BAD_WINE, 1);
			if (goodCookingLevel) player.getPA().addSkillXP(200, Skills.COOKING);
		}
		
	}
}
