package plugin.skills.fishing;

import com.soulplay.cache.EntityDef;
import com.soulplay.content.player.skills.fishing.FishingHandler;
import com.soulplay.content.player.skills.fishing.FishingOption;
import com.soulplay.content.player.skills.fishing.FishingSpot;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class FishingNpcClickPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 319);
		map(NpcClickExtension.class, 329);
		map(NpcClickExtension.class, 323);
		map(NpcClickExtension.class, 326);
		map(NpcClickExtension.class, 327);
		map(NpcClickExtension.class, 330);
		map(NpcClickExtension.class, 332);
		map(NpcClickExtension.class, 316);
		map(NpcClickExtension.class, 325);
		map(NpcClickExtension.class, 334);
		map(NpcClickExtension.class, 313);
		map(NpcClickExtension.class, 322);
		map(NpcClickExtension.class, 309);
		map(NpcClickExtension.class, 310);
		map(NpcClickExtension.class, 314);
		map(NpcClickExtension.class, 315);
		map(NpcClickExtension.class, 317);
		map(NpcClickExtension.class, 318);
		map(NpcClickExtension.class, 328);
		map(NpcClickExtension.class, 331);
		map(NpcClickExtension.class, 312);
		map(NpcClickExtension.class, 321);
		map(NpcClickExtension.class, 324);
		map(NpcClickExtension.class, 1174);//952 old karambwanji fishing spot
		map(NpcClickExtension.class, 1178);//1236 old karambwan fishing spot
		map(NpcClickExtension.class, 3848);
		map(NpcClickExtension.class, 8842);
		map(NpcClickExtension.class, 1177);
	}
	

	@Override
	public void onNpcOption1(Player player, NPC npc) {
		
		FishingSpot spot = FishingSpot.forId(npc.npcType);
		
		if (spot == null)
			return;
		
		
		EntityDef def = EntityDef.forID(npc.npcType);
		
		if (def == null || def.actions == null || def.actions[0] == null)
			return;
		
		String option = def.actions[0].toLowerCase();
		
		handle(player, npc, spot, option);
	}
	
	@Override
	public void onNpcOption2(Player player, NPC npc) {
		
		FishingSpot spot = FishingSpot.forId(npc.npcType);
		
		if (spot == null)
			return;
		
		
		EntityDef def = EntityDef.forID(npc.npcType);
		
		if (def == null || def.actions == null || def.actions[2] == null)
			return;
		
		String option = def.actions[2].toLowerCase();
		
		handle(player, npc, spot, option);
	}
	
	private static void handle(Player player, NPC npc, FishingSpot spot, String option) {
		for (FishingOption o : spot.getOptions()) {
			if (o.getName().equals(option)) {
				FishingHandler.startFishing(player, npc, o);
				break;
			}
		}
	}
}
