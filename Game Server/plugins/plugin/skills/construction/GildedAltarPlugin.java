package plugin.skills.construction;

import com.soulplay.content.player.skills.prayer.PrayerBoneType;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;

public class GildedAltarPlugin extends Plugin implements ItemOnObjectExtension {

    @Override
    public void onInit() {
        for (PrayerBoneType bone : PrayerBoneType.values) {
            map(ItemOnObjectExtension.class, bone.getId(), 13185);
        }
    }

    @Override
    public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
    	PrayerBoneType bone = PrayerBoneType.list(itemId);
    	if (bone == null) {
    		return;
    	}

        if (player.attr().getOrDefault("usingAltar", false)) {
            return;
        }

        bonesOnAltar(player, bone);
    }

    private static void bonesOnAltar(Player player, PrayerBoneType bone) {
        if (player.skillingEvent != null) {
            return;
        }

        int itemId = bone.getId();
        player.attr().put("usingAltar", true);

        if (player.getItems().playerHasItem(itemId, 1)) {
            player.getItems().deleteItemInOneSlot(itemId, player.getItems().getItemSlot(itemId),1);
            player.sendMessageSpam("The gods are pleased with your offering.");
            player.getPA().addSkillXP(bone.getExp() * 2, 5);
            player.startAnimation(3705);
            player.getPA().stillGfx(624, player.prevObjectX, player.prevObjectY, 0,10);
        }

        player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

            @Override
            public void execute(CycleEventContainer altar) {
                if (player.getItems().playerHasItem(itemId, 1) && player.attr().getOrDefault("usingAltar", false)) {
                    player.getItems().deleteItemInOneSlot(itemId, player.getItems().getItemSlot(itemId), 1);
                    player.sendMessageSpam("The gods are pleased with your offering.");
                    player.getPA().addSkillXP(bone.getExp() * 2, 5);
                    player.startAnimation(3705);
                    player.getPA().stillGfx(624, player.prevObjectX, player.prevObjectY, 0, 10);
                } else {
                    altar.stop();
                }
            }

            @Override
            public void stop() {
                player.attr().put("usingAltar", false);
            }
        }, 6);
    }

}
