package plugin.skills.construction;

import com.soulplay.content.player.skills.construction.content.Portals;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PortalPlugin extends Plugin implements ObjectClickExtension {

    private static final Map<Integer, Portals> map = new HashMap<>();

    @Override
    public void onInit() {
        for (Portals type : Portals.values) {
            for (int objectId : type.getObjects()) {
                map(ObjectClickExtension.class, objectId);
                map.put(objectId, type);
            }
        }
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        if (!player.playerIsInHouse) {
            return;
        }

        Optional<Portals> result = Optional.ofNullable(map.get(o.getId()));
        result.ifPresent(it -> player.getPA().movePlayer(it.getDestination()));
    }

}
