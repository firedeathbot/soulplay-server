package plugin.skills.construction;

import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CombatRingPlugin extends Plugin implements ObjectClickExtension {

    @Override
    public void onInit() {
        map(ObjectClickExtension.class, 13137);
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        final Location location = player.getPOH().getCorner();
        final int dx = player.getX() - location.getX();
        final int dy = player.getY() - location.getY();

        int xOffset = 0;
        int yOffset = 0;

        Direction direction = Direction.SOUTH;

        boolean canAttack = false;

        if (dy == 6 && dx >= 2 && dx <= 5) { // north
            yOffset = -1;
            canAttack = true;
        } else if (dx == 6 && dy >= 2 && dy <= 5) { // east
            xOffset = -1;
            direction = Direction.WEST;
            canAttack = true;
        } else if (dy == 1 && dx >= 2 && dx <= 5) { // south
            yOffset = 1;
            direction = Direction.NORTH;
            canAttack = true;
        } else if (dx == 1 && dy >= 2 && dy <= 5) { // west
            xOffset = 1;
            direction = Direction.EAST;
            canAttack = true;
        } else if ((dy == 5 && dx >= 2 && dx <= 5) || (dx == 5 && dy >= 2 && dy <= 5) || (dy == 2 && dx >= 2 && dx <= 5) || (dx == 2 && dy >= 2 && dy <= 5)) {
            xOffset = o.getX() - player.getX();
            yOffset = o.getY() - player.getY();
            if (xOffset > 0) {
                direction = Direction.EAST;
            } else if (xOffset < 0){
                direction = Direction.WEST;
            } else if (yOffset > 0) {
                direction = Direction.NORTH;
            } else if (yOffset < 0) {
                direction = Direction.SOUTH;
            }
        } else {
            return;
        }

        int anim = 3688; // no glove emote

        final int weapon = player.playerEquipment[PlayerConstants.playerWeapon];

        if (weapon == 7671) { // red gloves
            anim = 3689; // red glove emote
        } else if (weapon == 7673) { // blue gloves
            anim = 3690; // blue glove emote
        }

        player.attr().put("canAttack", canAttack);
        player.setForceMovement(ForceMovementMask.createMask(xOffset, yOffset, 2, 0, direction).addStartAnim(anim));
    }

}
