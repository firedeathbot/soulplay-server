package plugin.skills.construction;

import com.google.common.base.Preconditions;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.content.Portals;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TeleportFocusPlugin extends Plugin implements ObjectClickExtension {

    private static final int PORTAL_FRAME_1 = 13636;
    private static final int PORTAL_FRAME_2 = 13637;
    private static final int PORTAL_FRAME_3 = 13638;

    public static void load() {
    	/* empty */
    }

    private static final Map<Integer, Integer> objectIdToArrayPos = new HashMap<>();

    static {
        for (Portals portals : Portals.values) {
            Preconditions.checkState(portals.getObjects().length == 3);
            objectIdToArrayPos.put(portals.getObjects()[0], 0);
            objectIdToArrayPos.put(portals.getObjects()[1], 1);
            objectIdToArrayPos.put(portals.getObjects()[2], 2);
        }
        objectIdToArrayPos.put(PORTAL_FRAME_1, 0);
        objectIdToArrayPos.put(PORTAL_FRAME_2, 1);
        objectIdToArrayPos.put(PORTAL_FRAME_3, 2);
    }

    @Override
    public void onInit() {
        map(ObjectClickExtension.class, 13639);
        map(ObjectClickExtension.class, 13640);
        map(ObjectClickExtension.class, 13641);
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        if (!player.playerIsInHouse && !player.getPOH().isGuest()) {
            return;
        }

        Optional<Room> result = player.getPOH().getCurrentRoom();

        if (!result.isPresent()) {
            return;
        }

        Room room = result.get();

        if (room.getType() != RoomType.PORTAL) {
            return;
        }

        final Portals[] portals = new Portals[3];

        RoomObject[] portal1 = room.getObjects().get(HotspotType.PORTAL_1).toArray(new RoomObject[1]);
        if (portal1[0] != null) {
            portals[0] = Portals.forObjectId(portal1[0].getId());
        }
        RoomObject[] portal2 = room.getObjects().get(HotspotType.PORTAL_2).toArray(new RoomObject[1]);
        if (portal2[0] != null) {
            portals[1] = Portals.forObjectId(portal2[0].getId());
        }
        RoomObject[] portal3 = room.getObjects().get(HotspotType.PORTAL_3).toArray(new RoomObject[1]);
        if (portal3[0] != null) {
            portals[2] = Portals.forObjectId(portal3[0].getId());
        }

            player.getDialogueBuilder().sendStatement("To direct a portal you need enough runes for <col=ff0000>100</col> castings of that", "teleport spell.", "(Combination runes and staffs cannot be used.)")
                    .sendOption("Redirect which portal?", String.format("1. %s", portals[0] == null ? "Nowhere" : portals[0].getName()), () -> {
                        setDestination(player, portal1[0], portal1[0] != null);
                    }, String.format("2. %s", portals[1] == null ? "Nowhere" : portals[1].getName()), () -> {
                        setDestination(player, portal2[0], portal2[0] != null);
                    }, String.format("3. %s", portals[2] == null ? "Nowhere" : portals[2].getName()), () -> {
                        setDestination(player, portal3[0], portal3[0] != null);
                    }).execute();

    }

    private static void setDestination(Player player, RoomObject roomObject, boolean isFramePresent) {
        if (!isFramePresent) {
            return;
        }

        player.getDialogueBuilder().createCheckpoint().sendOption("Select a destination", Portals.PORTAL_1.getName(), () -> {
            setPortal(player, Portals.PORTAL_1, roomObject);
        }, Portals.PORTAL_2.getName(), () -> {
            setPortal(player, Portals.PORTAL_2, roomObject);
        }, Portals.PORTAL_3.getName(), () -> {
            setPortal(player, Portals.PORTAL_3, roomObject);
        }, Portals.PORTAL_4.getName(), () -> {
            setPortal(player, Portals.PORTAL_4, roomObject);
        }, "More", () -> {
            player.getDialogueBuilder().sendOption(Portals.PORTAL_5.getName(), () -> {
                setPortal(player, Portals.PORTAL_5, roomObject);
            }, Portals.PORTAL_6.getName(), () -> {
                setPortal(player, Portals.PORTAL_6, roomObject);
            }, Portals.PORTAL_7.getName(), () -> {
                setPortal(player, Portals.PORTAL_7, roomObject);
            }, "Back", () -> {
                player.getDialogueBuilder().returnToCheckpoint();
            });
        });
    }

    private static void setPortal(Player player, Portals portal, RoomObject roomObject) {
        final int index = objectIdToArrayPos.getOrDefault(roomObject.getId(), -1);
        if (index == -1) {
            return;
        }

        if (player.getSkills().getLevel(Skills.MAGIC) < portal.getMagicLevel()) {
            player.sendMessage(String.format("You need a magic level of %d to use this.", portal.getMagicLevel()));
            return;
        }

        for (int i = 0; i < portal.getRequiredItems().length; i++) {
            if (portal.getRequiredItems()[i].length < 2) {
                return;
            }

            final int itemId = portal.getRequiredItems()[i][0];
            final int itemAmount = portal.getRequiredItems()[i][1];

            if (!player.getItems().playerHasItem(itemId, itemAmount)) {
                player.sendMessage("You do not have the required runes!");
                return;
            }

            player.getItems().deleteItem2(itemId, itemAmount);
        }

        roomObject.setId(portal.getObjects()[index]);
        player.getPacketSender().addObjectPacket(roomObject);
    }

}
