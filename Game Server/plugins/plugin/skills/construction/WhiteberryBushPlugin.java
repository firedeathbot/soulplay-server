package plugin.skills.construction;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.specialobjects.WhiteberryEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class WhiteberryBushPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		for (WhiteberryEnum berry : WhiteberryEnum.values())
			map(ObjectClickExtension.class, berry.getObject());
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (!player.playerIsInHouse) {
			return;
		}
		
		final int amountToGive = 1;// 10;
		
		if (player.getItems().freeSlots() < amountToGive) {
			player.sendMessage("You don't have enough inventory space.");
			return;
		}

		RoomObject o = Construction.getDecoration(player, object.getId(), object.getX(), object.getY());
		
		if (o == null) {
			return;
		}
		
		WhiteberryEnum berry = WhiteberryEnum.get(o.getId());
		
		if (berry == null)
			return;
		
		o.setId(berry.getPreviousObject());
		player.getPacketSender().addObjectPacket(o);
		
		player.getItems().addItem(239, amountToGive);
		
		player.startAnimation(834);
		
		player.sendMessage("You pick the whiteberries.");
		
	}
	
	@Override
	public void onObjectOption4(Player player, GameObject o) {
		Construction.handleFifthObjectClick(player, player.getClickedObject());
	}
	
}
