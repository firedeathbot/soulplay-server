package plugin.skills.construction;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.objects.ChairBenchEnum;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ChairBenchPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		ChairBenchEnum.init();
		for (ChairBenchEnum o : ChairBenchEnum.values()) {
			map(ObjectClickExtension.class, o.getObjectId());
		}
	}
	
	@Override
	public void onObjectOption1(Player c, GameObject o) {
		if (!c.playerIsInHouse) {
			return;
		}

//		GameObject o = Construction.getDecoration(c, objectId, objectX, objectY);
		
		if (o == null) {
			return;
		}
		
		final ChairBenchEnum cb = ChairBenchEnum.get(o.getId());
		
		final boolean diagonal = o.getType() == 11;
		
		int direction = o.getOrientation();
		
		int newFace = (direction+2) % 4;
		
		if (diagonal)
			newFace = newFace+4;
		
		final Direction dir = Direction.get(newFace);
		
		final Location turnLocation = o.getLocation().transform(dir.getStepX()*2, dir.getStepY()*2);// o.getLocation().transform(dir);
		
//		c.getPA().movePlayer(o.getLocation());
		
//		c.startAnimation(cb.getStartAnim());
		
		c.faceLocation(turnLocation);

		c.performingAction = true; // to stop them from moving around when sitting down using forcemovment
		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				c.startAnimation(cb.getStartAnim());
				int walkX = o.getLocation().getX() - c.getX();
				int walkY = o.getLocation().getY() - c.getY();
				ForceMovementMask mask = ForceMovementMask.createMask(walkX, walkY, 1, 0, dir);
				c.setForceMovement(mask);
			}
		}, 2);
		

		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			int timer = 160;
			@Override
			public void stop() {
				if (c.isActive)
					c.startAnimation(RESET_ANIM);
			}

			@Override
			public void execute(CycleEventContainer container) {
				timer++;
				if (timer == 163) {
					c.startAnimation(cb.getSitAnim());
					timer = 0;
				}
			}
		}, 1);
		
	}
	
	@Override
	public void onObjectOption2(Player c, GameObject o) {
		c.sendMessage("2");
	}
	
	@Override
	public void onObjectOption3(Player c, GameObject o) {
		c.sendMessage("3");
	}
	
	@Override
	public void onObjectOption4(Player c, GameObject o) {
		c.sendMessage("4");
	}
	@Override
	public void onObjectOption5(Player c, GameObject o) {
		c.sendMessage("5");
	}
	
	public static final Animation RESET_ANIM = new Animation(65535);

}
