package plugin.skills.construction;

import java.util.Optional;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.RoomObject;
import com.soulplay.content.player.skills.construction.hotspot.HotspotType;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class FireplacePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 13609);
		map(ObjectClickExtension.class, 13611);
		map(ObjectClickExtension.class, 13613);
	}

	@Override
	public void onObjectOption1(Player c, GameObject object) {
		if (!c.playerIsInHouse) {
			return;
		}

		boolean hasLogs = false, hasTinderbox = false;

		for (int i = 0, len = c.playerItems.length; i < len; i++) {
			if (c.playerItems[i] == 1512) {
				hasLogs = true;
			}
			if (c.playerItems[i] == 591) {
				hasTinderbox = true;
			}
			if (hasLogs && hasTinderbox)
				break;
		}

		if (!hasLogs || !hasTinderbox) {
			c.sendMessage("You need some logs and a tinderbox in order to light the fireplace.");
			return;
		}

		RoomObject o = Construction.getDecoration(c, object.getId(), object.getX(), object.getY());

		if (o == null) {
			return;
		}

		c.startAnimation(ANIMATION);
		
		c.performingAction = true;

		CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void stop() {
				c.performingAction = false;
			}

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();

				if (!o.isActive()) {
					return;
				}

				if (c.getItems().deleteItem2(1511, 1)) {
					c.getPA().addSkillXP(80, Skills.FIREMAKING);
					c.sendMessage("You light the fireplace.");
					startFireplaceTick(c, o);
				}
			}
		}, 2);


	}

	public static void startFireplaceTick(Player guest, RoomObject o) {
		Player owner = PlayerHandler.getPlayerByMID(guest.getHomeOwnerId());

		if (owner == null || !owner.isActive) {
			return;
		}

		Optional<Room> roomResult = owner.getPOH().getCurrentRoom(o.getLocation());

		if (!roomResult.isPresent()) {
			return;
		}

		final Room currentRoom = roomResult.get();

		if (currentRoom == null)
			return;

		final HotspotType hotspot = o.getHotspotType();

		final int hotspotId = o.getHotspotId();
		final int oldId = o.getId();
		final int newId = o.getId() + 1;
		final Location loc = o.getLocation().copyNew();
		final int face = o.getOrientation();
		final int type = o.getType();

		o.setActive(false);
		
		if (!currentRoom.getObjects().values().remove(o))
			return;

		final RoomObject fireplace = new RoomObject(hotspotId, newId, loc, face, type);
		
		final RoomObject fireplaceOff = new RoomObject(hotspotId, oldId, loc, face, type);

		currentRoom.getObjects().put(hotspot, fireplace);

		//reload object visuals for all guests
		refreshVisuals(owner, fireplace);

		CycleEventHandler.getSingleton().addEvent(owner, new CycleEvent() {

			boolean fullcycle = false;
			
			@Override
			public void stop() {
				if (!fullcycle && currentRoom.getObjects().values().remove(fireplace)) { // just in case player logs out, the fireplace gets reverted back to normal
					currentRoom.getObjects().put(hotspot, fireplaceOff);
				}
			}

			@Override
			public void execute(CycleEventContainer container) {

				fireplace.setActive(false);
				currentRoom.getObjects().values().remove(fireplace);
				
				
				currentRoom.getObjects().put(hotspot, fireplaceOff);
				
				//reload object visuals for all guests
				refreshVisuals(owner, fireplaceOff);

				fullcycle = true;
				container.stop();
			}
		}, 1000);

	}
	
	private static void refreshVisuals(Player owner, RoomObject fireplace) {
		//reload object visuals for all guests
		RSObject fireplaceRSO = new RSObject(fireplace.getId(), fireplace.getLocation().getX(), fireplace.getLocation().getY(), fireplace.getLocation().getZ(), fireplace.getOrientation(), fireplace.getType());
		for (Player p : owner.getPOH().getGuests()) {
			if (p.isActive) {
				if (p.getZ() == fireplace.getLocation().getZ()) {
					p.getPacketSender().addObjectPacket(fireplace);
				} else {
					p.objectToRemove.add(fireplaceRSO);
				}
			}
		}
		if (owner.isActive) {
			if (owner.getZ() == fireplace.getLocation().getZ()) {
				owner.getPacketSender().addObjectPacket(fireplace);
			} else {
				owner.objectToRemove.add(fireplaceRSO);
			}
		}
	}

	private static final Animation ANIMATION = new Animation(3658);
	
}
