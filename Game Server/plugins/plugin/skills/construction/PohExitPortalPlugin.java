package plugin.skills.construction;

import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class PohExitPortalPlugin extends Plugin implements ObjectClickExtension {

	 @Override
	 public void onInit() {
		 map(ObjectClickExtension.class, 13405);
	 }
	 
	 @Override
	 public void onObjectOption1(Player p, GameObject o) {
	 	Construction.leaveHouse(p);
	 }
	 
	 @Override
	 public void onObjectOption2(Player p, GameObject o) {
		 p.attr().put("poh_lock", !p.attr().getOrDefault("poh_lock", false));
		 p.sendMessage(String.format("Your house is now %s.", p.attr().getOrDefault("poh_lock", false) ? "locked" : "unlocked"));
	 }

}
