package plugin.skills.construction;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.construction.*;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.plugin.extension.ItemOnNpcExtension;
import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.util.Misc;

import java.util.Optional;

public class ButlerPlugin extends Plugin implements ButtonClickExtension, ItemOnNpcExtension, NpcClickExtension {

    @Override
    public void onInit() {
        map(ButtonClickExtension.class, 179189);
        for (ButlerType type : ButlerType.values) {
            map(NpcClickExtension.class, type.getPurchaseNpcId());
            map(NpcClickExtension.class, type.getNpcId());
            map(ItemOnNpcExtension.class, type.getNpcId(), ConstructionItems.PLANK);
            map(ItemOnNpcExtension.class, type.getNpcId(), ConstructionItems.OAK_PLANK);
            map(ItemOnNpcExtension.class, type.getNpcId(), ConstructionItems.TEAK_PLANK);
            map(ItemOnNpcExtension.class, type.getNpcId(), ConstructionItems.MAHOGANY_PLANK);
        }
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        final Optional<ButlerType> result = ButlerType.lookup(npc.npcType);
        if (!result.isPresent()) {
            return;
        }

        final ButlerType type = result.get();

        if (npc.npcType == type.getNpcId()) {
            speakTo(player, player.getPOH().getButler());
        } else {
            purchaseDialogue(player, type);
        }
    }

    @Override
    public void onNpcOption2(Player player, NPC npc) {
        final Poh poh = player.getPOH();
        final Butler butler = poh.getButler();

        if (poh.isGuest() || !poh.getOwner().playerIsInHouse) {
            return;
        }

        Optional<ButlerType> result = ButlerType.lookup(butler.getNpc().npcType);

        if (!result.isPresent()) {
            return;
        }

        if (butler.hasToPayWage()) {
            payWage(poh.getOwner(), butler);
            return;
        }

        player.getDialogueBuilder().sendOption("Wooden planks", () -> {
            fetchFromBank(player, butler, 960);
        }, "Oak planks", () -> {
            fetchFromBank(player, butler,8778);
        }, "Teak planks", () -> {
            fetchFromBank(player, butler,8780);
        }, "Mahogany planks", () -> {
            fetchFromBank(player, butler,8782);
        }, "More...", () -> {

        }).execute();
    }

    @Override
    public void onItemOnNpc(Player player, NPC npc, int itemId, int slot) {
        final Poh poh = player.getPOH();

        if (poh.isGuest() || !poh.getOwner().playerIsInHouse) {
            return;
        }

        final Butler butler = poh.getButler();

        final Optional<ButlerType> result = ButlerType.lookup(npc.npcType);

        if (!result.isPresent()) {
            return;
        }

        final ButlerType type = result.get();

        final ItemDefinition def = ItemDefinition.forId(itemId);
        if (def == null) {
            return;
        }

        if (butler.hasToPayWage()) {
            payWage(player, butler);
            return;
        }

        player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "Shall I redeem the certificate for sir?")
                .sendOption("Yes", () -> {
                    player.getDialogueBuilder().sendEnterAmount(() -> {
                    	
                    	final int itemCount = player.getItems().countItems(itemId);
                        if (player.enterAmount > itemCount)
                        	player.enterAmount = itemCount;

                        final int enteredAmount = (int) player.enterAmount;
                        
                        

                        if (enteredAmount <= 0 || enteredAmount > type.getCapacity()) {
                            player.getDialogueBuilder().sendNpcChat(type.getNpcId(), String.format("I can only hold 1-%d items.", type.getCapacity()));
                            butler.setState(Butler.ButlerState.IDLE);
                            return;
                        }

                        player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "Very good, sir.");
                        butler.setUsedOn(Optional.of(new Item(itemId, enteredAmount)));

                        for (int i = 0; i < enteredAmount; i++) {
                            if (player.getItems().deleteItem2(itemId, 1)) {
                                butler.getItems().add(new Item(itemId + (def.isNoted() ? - 1 : 1), 1));

                            }
                        }
                        butler.setCounter(0);
                        butler.incrementUses();
                        butler.setState(Butler.ButlerState.BANKING);
                    });
                }, "No, just take it to the bank", () -> {
                    player.getDialogueBuilder().sendEnterAmount(() -> {

                    	final int itemCount = player.getItems().countItems(itemId);
                        if (player.enterAmount > itemCount)
                        	player.enterAmount = itemCount;
                    	
                        final int enteredAmount = (int) player.enterAmount;

                        if (enteredAmount <= 0) {
                            butler.setState(Butler.ButlerState.IDLE);
                            return;
                        }

                        if (enteredAmount > type.getCapacity()) {
                            player.getDialogueBuilder().sendNpcChat(type.getNpcId(), String.format("I can only hold 1-%d items.", type.getCapacity()));
                            butler.setState(Butler.ButlerState.IDLE);
                            return;
                        }

                        butler.setUsedOn(Optional.of(new Item(itemId, enteredAmount)));

                        final int deleted = player.getItems().deleteInventoryItem(itemId, enteredAmount);
                        if (deleted > 0) {
                        	player.getItems().addItemToBank(itemId, deleted);
                        }

                        player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "Very good, sir.");
                        butler.setCounter(0);
                        butler.setState(Butler.ButlerState.BANKING);
                    });

                    butler.setCounter(0);
                    butler.setState(Butler.ButlerState.BANKING);
                }, "No never mind", () -> {

                }).execute();
    }

    @Override
    public void onClick(final Player player, int button, int component) {
        if (button == 179189) {
            if (!player.playerIsInHouse) {
                return;
            }

            final Poh poh = player.getPOH();
            final Butler butler = poh.getButler();

            if (butler.getNpc() != null) {
                player.getPOH().getButler().setState(Butler.ButlerState.WALK_TO_PLAYER);
                return;
            }

            if (butler.getType() == null) {
                player.sendMessage("You do not have a servant!");
                return;
            }

            CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

                int tick = 0;

                @Override
                public void execute(CycleEventContainer container) {

                    if (butler.getType() == null || !player.playerIsInHouse) {
                        container.stop();
                        return;
                    }

                    if (butler.getNpc() == null || (butler.getNpc() != null && (player.getZ() != butler.getNpc().getZ()))) {
                        NPC npc = NPCHandler.spawnNpc(player, butler.getType().getNpcId(), player.getX() + Misc.random(new int[] {-1, 0, 1}), player.getY() + Misc.random(new int[] {-1, 0, 1}), player.getHeightLevel(), 0, false, false);
                        if (npc != null) {
                            npc.setHomeOwnerId(player.getHomeOwnerId());
                        }
                        butler.setNpc(npc);
                    }

                    final NPC npc = butler.getNpc();
                    if (npc == null || !npc.isActive || npc.isDead()) {
                        container.stop();
                        return;
                    }

                    if (butler.getState() != Butler.ButlerState.IDLE) {
                        npc.resetFollow();
                    }

                    switch(butler.getState()) {

                        case WALK_TO_PLAYER: {
                            npc.face(poh.getOwner());
                            Direction dir = Direction.getDirection(player.getCurrentLocation(), npc.getCurrentLocation());
                            ConstructionPathFinder.findRoute(npc, player.getCurrentLocation().transform(dir.getStepX(), dir.getStepY()), true, 1, 1, player.getDynamicRegionClip());
                            butler.setState(Butler.ButlerState.SPEAK_TO_PLAYER);
                        }
                        break;

                        case SPEAK_TO_PLAYER:
                            if (npc.getCurrentLocation().isWithinDistance(player.getCurrentLocation(), 1)) {
                                speakTo(player, butler);
                                butler.setState(Butler.ButlerState.IDLE);
                            }
                            break;

                        case BANKING:
                            if (butler.getCounter() == 2) {
                                npc.setVisible(false);
                            } else if (butler.getCounter() == butler.getType().getTripTicks()) {
                                npc.setVisible(true);
                                butler.setState(Butler.ButlerState.WALK_TO_PLAYER);
                            }
                            butler.setCounter(butler.getCounter() + 1);
                            break;

                        case GREET:
                            if (tick % 5 == 1) {
                                npc.forceChat(String.format("Welcome to %s's house!", player.getNameSmartUp()));
                            }
                            break;

                        case DISMISS:
                            container.stop();
                            break;

                        case IDLE:
                            npc.face(poh.getOwner());
                            NPCHandler.followPlayer(npc, poh.getOwner());
                            npc.setVisible(true);
                            break;

                    }

                    tick++;

                }

                @Override
                public void stop() {
                    dismiss(butler, false);
                }
            }, 1);
        }
    }


    private static void dismiss(Butler butler, boolean fire) {
        if (butler.getNpc() == null) {
            return;
        }

        butler.getNpc().removeNpcSafe(false);
        butler.setNpc(null);
        butler.setState(Butler.ButlerState.WALK_TO_PLAYER);
        butler.setUsedOn(Optional.empty());
        butler.getItems().clear();

        if (fire) {
            butler.setType(null);
        }
    }

    private static void fetchFromBank(Player player, Butler butler, int itemId) {
        if (butler.getType() == null) {
            return;
        }

        ItemDefinition def = ItemDefinition.forId(itemId);

        if (def == null) {
            return;
        }

        player.getDialogueBuilder().sendEnterAmount(() -> {
        	
        	if (player.enterAmount > Integer.MAX_VALUE)
        		player.enterAmount = Integer.MAX_VALUE;
        	
            int amount = (int) player.enterAmount;

            if (amount <= 0) {
                return;
            }

            if ((amount > butler.getType().getCapacity()) || ((butler.getItems().size() + amount) > butler.getType().getCapacity())) {
                player.getDialogueBuilder().sendNpcChat(butler.getType().getNpcId(), "I don't have enough room to carry that!");
                return;
            }

            final int actualAmount = player.getItems().getBankItemCount(itemId);

            if (amount > actualAmount) {
                amount = actualAmount;
            }

            if (actualAmount <= 0) {
                player.getDialogueBuilder().sendNpcChat(butler.getType().getNpcId(), "Master, you don't have this item in your bank.");
                return;
            }

            for (int i = 0; i < amount; i++) {
                if (player.getItems().removeBankItem(itemId, 1)) {
                    butler.getItems().add(new Item(itemId, 1));
                }
            }

            if (!butler.getItems().isEmpty()) {
                butler.setUsedOn(Optional.of(new Item(itemId, amount)));
                butler.setCounter(0);
                butler.incrementUses();
                butler.setState(Butler.ButlerState.BANKING);
            }

        });

    }

    private static void payWage(Player player, Butler butler) {
        player.getDialogueBuilder().sendNpcChat(butler.getType().getNpcId(), String.format("Pay me %d for my continued services master!", butler.getType().getPurchaseCost()))
                .sendOption(String.format("Pay servant %d coins", butler.getType().getPurchaseCost()), () -> {

                    final int cost = butler.getType().getPurchaseCost();

                    if (player.getItems().takeCoins(cost)) {
                        butler.setUses(0);
                    } else {
                        player.getDialogueBuilder().sendNpcChat(butler.getType().getNpcId(), "You can't afford to pay me!");
                    }

                }, "Don't", () -> {

                }, "Fire servant", () -> {
                    dismiss(butler, true);
                }).execute();
    }

    private static boolean speakTo(Player player, Butler butler) {

        if (player.getPOH().isGuest()) {
            return true;
        }

        if (butler.hasToPayWage()) {
            payWage(player, butler);
            return true;
        }

        if (!butler.getItems().isEmpty()) {
            if (butler.getNpc().getCurrentLocation().isWithinDistance(player.getCurrentLocation(), 1)) {

                if (!butler.getUsedOn().isPresent()) {
                    return true;
                }

                Item used = butler.getUsedOn().get();

                ItemDefinition def = ItemDefinition.forId(used.getId());

                if (def == null) {
                    return true;
                }

                butler.getItems().removeIf(next -> player.getItems().addItem(next.getId(), next.getAmount()));
                player.getDialogueBuilder().sendNpcChat(butler.getType().getNpcId(), "Your good, sir.").execute();

                if (butler.getItems().isEmpty()) {
                    butler.incrementUses();
                    butler.setState(Butler.ButlerState.IDLE);
                }

            }
        } else {
            player.getDialogueBuilder().sendNpcChat(butler.getType().getNpcId(), "Yes, sir?")
                    .sendOption( "Go to bank...", () -> {
                        player.getDialogueBuilder().sendOption("Take something to the bank", () -> {
                            player.getDialogueBuilder().sendOption("Wooden planks", () -> {
                                takeItemstoBank(player, butler,960);
                            }, "Oak planks", () -> {
                                takeItemstoBank(player, butler,8778);
                            }, "Teak planks", () -> {
                                takeItemstoBank(player, butler,8780);
                            }, "Mahogany planks", () -> {
                                takeItemstoBank(player, butler,8782);
                            }, "More...", () -> {

                            });
                        }, "Bring something from the bank", () -> {
                            player.getDialogueBuilder().sendOption("Wooden planks", () -> {
                                fetchFromBank(player, butler,960);
                            }, "Oak planks", () -> {
                                fetchFromBank(player, butler,8778);
                            }, "Teak planks", () -> {
                                fetchFromBank(player, butler,8780);
                            }, "Mahogany planks", () -> {
                                fetchFromBank(player, butler,8782);
                            }, "More...", () -> {

                            });
                        });
                    }, butler.isGreeting() ? "Stop greeting" : "Greet guests.", () -> {
                        if (butler.isGreeting()) {
                            butler.setFaceFlag(false);
                            butler.setState(Butler.ButlerState.IDLE);
                        } else {
                            butler.incrementUses();
                            butler.setState(Butler.ButlerState.GREET);
                        }
                        butler.setGreeting(!butler.isGreeting());
                    }, "You're fired!", () -> {
                        dismiss(butler, true);
                        butler.setState(Butler.ButlerState.DISMISS);
                    }, "Dismiss.", () -> {
                        butler.setState(Butler.ButlerState.DISMISS);
                    }).execute();
        }
        return true;
    }

    private static void takeItemstoBank(Player player, Butler butler, int itemId) {
        if (butler.getType() == null) {
            return;
        }

        if (!player.getItems().playerHasItem(itemId)) {
            return;
        }

        player.getDialogueBuilder().sendEnterAmount(() -> {
        	
            final int amount = (int) player.enterAmount;

            final int deleted = player.getItems().deleteInventoryItem(itemId, amount);
            if (deleted > 0) {
            	player.getItems().addItemToBank(itemId, deleted);
            }

            butler.setCounter(0);
            butler.incrementUses();
            butler.setState(Butler.ButlerState.BANKING);
        });

    }

    private static boolean purchaseButler(Player player, ButlerType type) {
        final Butler butler = player.getPOH().getButler();
        if (butler.getType() == type) {
            player.getDialogueBuilder().sendNpcChat(type.getNpcId(), type == ButlerType.DEMON_BUTLER ? "<col=ff>I'm already working for you!": "I'm already working for you!");
            return false;
        }

        if (player.getSkills().getLevel(Skills.CONSTRUCTION) < type.getLvlRequired()) {
            player.getDialogueBuilder().sendNpcChat(type.getNpcId(), String.format(type == ButlerType.DEMON_BUTLER ? "<col=ff>You need a construction level of %d or more." : "You need a construction level of %d or more.", type.getLvlRequired()));
            return false;
        }

        if (!player.getItems().playerHasItem(995, type.getPurchaseCost())) {
            player.getDialogueBuilder().sendNpcChat(type.getNpcId(), type == ButlerType.DEMON_BUTLER ? "<col=ff>Come back when you have enough coins." : "Come back when you have enough coins.");
            return false;
        }

        player.getItems().deleteItem2(995, type.getPurchaseCost());
        butler.setType(type);
        return true;
    }

    private static void purchaseDialogue(Player player, ButlerType type) {
        switch(type) {
            case JACK:
                player.getDialogueBuilder().sendNpcChat(type.getNpcId(), String.format("'Allo mate! Got a job going? Only %d coins!", type.getPurchaseCost()))
                        .sendOption("What can you do?", () -> {
                            player.getDialogueBuilder().sendPlayerChat("What can you do?")
                                    .sendNpcChat(type.getNpcId(), "I'm a great cook, me! I used to work with a rat-catcher", "I used to cook for him. There's a dozen different ways", "you can cook rat!");
                        }, "You're hired!", () -> {
                            if (purchaseButler(player, type)) {
                                player.getDialogueBuilder().sendPlayerChat("You're hired!").sendNpcChat(type.getNpcId(), "Cheers, mate! Look forward to working with you!");
                            }
                        }).execute();
                break;

            case MAID:
                player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "Oh! Please hire me, sir! I'm very good, well, I'm not", String.format("bad, and my fee's only %d coins.", type.getPurchaseCost()))
                        .sendOption("What can you do?", () -> {
                            player.getDialogueBuilder().sendPlayerChat("What can you do?")
                                    .sendNpcChat(type.getNpcId(), "Well, I can, um. I can cook meals and make tea and", "everything, and I can even take things to and from", "bank for you. I won't make any mistakes this time and", "everything will be fine!");
                        }, "You're hired!", () -> {
                            if (purchaseButler(player, type)) {
                                player.getDialogueBuilder().sendPlayerChat("You're hired!").sendNpcChat(type.getNpcId(), "Alright, sir. I can start work immediately.");
                            }
                        }).execute();
                break;

            case COOK:
                player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "You're not aristocracy, but I suppose you'd do. Do you", String.format("want a good cook for %d coins?", type.getPurchaseCost()))
                        .sendOption("What can you do?", () -> {
                            player.getDialogueBuilder().sendPlayerChat("What can you do?")
                                    .sendNpcChat(type.getNpcId(), "I, sir am the finest cook in all SoulPlay! I can also", "make good time going to the bank or the sawmill.");
                        }, "You're hired!", () -> {
                            if (purchaseButler(player, type)) {
                                player.getDialogueBuilder().sendPlayerChat("You're hired!").sendNpcChat(type.getNpcId(), "Alright, sir. I can start work immediately.");
                            }
                        }).execute();
                break;

            case BUTLER:
                player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "Good day, sir. Would sir care to hire a good butler for", String.format("%d coins?", type.getPurchaseCost()))
                        .sendOption("What can you do?", () -> {
                            player.getDialogueBuilder().sendPlayerChat("What can you do?")
                                    .sendNpcChat(type.getNpcId(), "I can fulfill all sir's domestic service needs with", "efficiency and impeccable manners. I hate to boast, but", "I can say with confidence that no mortal can make", "trips to the bank or sawmill faster than I!");
                        }, "You're hired!", () -> {
                            if (purchaseButler(player, type)) {
                                player.getDialogueBuilder().sendPlayerChat("You're hired!").sendNpcChat(type.getNpcId(), "Alright, sir. I can start work immediately.");
                            }
                        }).execute();
                break;

            case DEMON_BUTLER:
                player.getDialogueBuilder().sendNpcChat(type.getNpcId(), "<col=ff>Greetings! I am Alathazdrat, butler to the Demon", String.format("<col=ff>Lords, and I offer thee my services for a mere %d", type.getPurchaseCost()), "<col=ff>coins!")
                        .sendOption("What can you do?", () -> {
                            player.getDialogueBuilder().sendPlayerChat("What can you do?")
                                    .sendNpcChat(type.getNpcId(), "<col=ff>I have learned my trade under the leash of some of the", "<col=ff>harshest masters of the Demon Dimensions. I can cook", "<col=ff>to satisfy the most infernal stomachs, and fly on wings", "<col=ff>of flame to deposit thine items in the bank or bring")
                                    .sendNpcChat(type.getNpcId(), "<col=ff>planks from the sawmill in seconds.");
                        }, "You're hired!", () -> {
                            if (purchaseButler(player, type)) {
                                player.getDialogueBuilder().sendPlayerChat("You're hired!")
                                        .sendNpcChat(type.getNpcId(), "<col=ff>I shall devote my every art to thy service, my Master.");
                            }
                        }).execute();
                break;
        }
    }

}
