package plugin.skills.construction;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class AmuletOfGloryPlugin extends Plugin implements ObjectClickExtension {

    @Override
    public void onInit() {
        map(ObjectClickExtension.class, 13523); // glory in poh
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        player.getPA().handleGlory();
    }

}
