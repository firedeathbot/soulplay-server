package plugin.skills.construction;

import com.soulplay.content.player.skills.firemaking.LogData;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class SawmillOperatorPlugin extends Plugin implements NpcClickExtension {

    private static final int LOGS = 1511;
    private static final int OAK_LOGS = 1521;
    private static final int TEAK_LOGS = 6333;
    private static final int MAHOGANY_LOGS = 6332;

    private static final int PLANK = 960;
    private static final int OAK_PLANK = 8778;
    private static final int TEAK_PLANK = 8780;
    private static final int MAHOGANY_PLANK = 8782;

    @Override
    public void onInit() {
        map(NpcClickExtension.class, 4250);
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        player.getDialogueBuilder().sendNpcChat(npc.npcType, "Would you like me to saw your logs", "into planks?")
                .sendOption("Yes", ()-> {
                    player.getDialogueBuilder().sendOption("Plank", () -> {
                        convertPlanks(player, npc, LogData.LOG);
                    }, "Oak plank", () -> {
                        convertPlanks(player, npc, LogData.OAK);
                    }, "Teak plank", () -> {
                        convertPlanks(player, npc, LogData.TEAK);
                    }, "Mahogany plank", () -> {
                        convertPlanks(player, npc, LogData.MAHOGANY);
                    });
                }, "No thanks!", ()->{}).execute();
    }

    @Override
    public void onNpcOption2(Player player, NPC npc) {
        player.getDialogueBuilder().sendOption("Plank", () -> {
            convertPlanks(player, npc, LogData.LOG);
        }, "Oak plank", () -> {
            convertPlanks(player, npc, LogData.OAK);
        }, "Teak plank", () -> {
            convertPlanks(player, npc, LogData.TEAK);
        }, "Mahogany plank", () -> {
            convertPlanks(player, npc, LogData.MAHOGANY);
        }, "Never mind", () -> {

        }).execute();
    }

    @Override
    public void onNpcOption3(Player player, NPC npc) {
        ((Client) player).getShops().openShop(20);
    }

    private void convertPlanks(Player player, NPC npc, LogData data) {
        if (!player.getItems().playerHasItem(data.getLogId())) {
            player.getDialogueBuilder().sendNpcChat(npc.npcType, "Come back when you have some logs!");
            return;
        }

        final int amount = player.getItems().getItemAmount(data.getLogId());
        int coins = 0;
        int logs = 0;
        int planks = 0;

        switch(data) {
            case LOG:
            	coins = amount * 100;
            	logs = LOGS;
            	planks = PLANK;
                break;

            case OAK:
            	coins = amount * 250;
            	logs = OAK_LOGS;
            	planks = OAK_PLANK;
                break;

            case TEAK:
            	coins = amount * 500;
            	logs = TEAK_LOGS;
            	planks = TEAK_PLANK;
                break;

            case MAHOGANY:
            	coins = amount * 1500;
            	logs = MAHOGANY_LOGS;
            	planks = MAHOGANY_PLANK;
                break;
            default:
            	break;
        }

        if (coins == 0) {
        	player.sendMessage("Wrong.");
        	return;
        }

        if (!player.getItems().takeCoins(coins)) {
            player.getDialogueBuilder().sendNpcChat(npc.npcType, String.format("You need %d coins to convert these logs into planks.", coins));
            return;
        }

        player.getItems().deleteItem2(logs, amount);
        player.getItems().addItem(planks, amount);
    }

}
