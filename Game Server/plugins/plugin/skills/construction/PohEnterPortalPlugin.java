package plugin.skills.construction;

import java.util.Optional;

import com.soulplay.content.player.skills.construction.popularhouse.PopularHouses;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.World;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class PohEnterPortalPlugin extends Plugin implements ObjectClickExtension {

    @Override
    public void onInit() {
        map(ObjectClickExtension.class, 15477);
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        player.getDialogueBuilder().sendOption("Go to your house", () -> {
            player.getPOH().enter(player, false);
        }, "Go to your house (building mode)", () -> {
            player.getPOH().enter(player, true);
        }, "Go to a friend's house", () -> {
            player.getDialogueBuilder().sendEnterText(()->{
                Optional<Client> owner = World.search(player.getDialogueBuilder().getEnterText());
                if (!owner.isPresent()) {
                    player.sendMessage("That player is not online.");
                    player.getPA().closeAllWindows();
                } else {
                	Client client = owner.get();
                	if (client.isActive) {
                		player.getPOH().enter(client);
                	} else {
                		player.sendMessage("That player is not online.");
                		player.getPA().closeAllWindows();
                	}
                }
            });
        }, "Never mind", () -> {
            player.getPA().closeAllWindows();
        }).execute(false);
    }

    @Override
    public void onObjectOption2(Player player, GameObject o) {
        player.getPOH().enter(player, false);
    }

    @Override
    public void onObjectOption3(Player player, GameObject o) {
        player.getPOH().enter(player, true);
    }

    @Override
    public void onObjectOption4(Player player, GameObject o) {
        player.getDialogueBuilder().sendEnterText(()->{
            Client owner = (Client) PlayerHandler.getPlayer(player.getDialogueBuilder().getEnterText());
            if (owner == null || !owner.isActive) {
                player.sendMessage("That player is not online.");
                player.getPA().closeAllWindows();
            } else {

                if (owner.attr().getOrDefault("poh_lock", false) && !owner.isFriend(player)) {
                    player.sendMessage("That player has privacy mode on.");
                    return;
                }

                player.getPOH().enter(owner);
            }
        }).execute();
    }

    @Override
    public void onObjectOption5(Player player, GameObject o) {
        PopularHouses.openInterface(player);
    }

}
