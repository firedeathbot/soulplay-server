package plugin.skills.construction;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class GloveRackPlugin extends Plugin implements ObjectClickExtension {

    private static final int RED_BOXING_GLOVES = 7671;
    private static final int BLUE_BOXING_GLOVES = 7673;

    @Override
    public void onInit() {
        map(ObjectClickExtension.class, 13381);
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        player.getDialogueBuilder().sendOption("What do you want to take?","Red boxing gloves", () -> player.getItems().addItem(RED_BOXING_GLOVES, 1), "Blue boxing gloves", () -> player.getItems().addItem(BLUE_BOXING_GLOVES, 1)).execute();
    }

}
