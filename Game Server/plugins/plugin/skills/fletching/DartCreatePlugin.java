package plugin.skills.fletching;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.darts.Dart;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;
import com.soulplay.util.Misc;

public final class DartCreatePlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for (Dart dart : Dart.values) {
			map(ItemOnItemExtension.class, dart.getItem().getId(), 314);
			map(ItemOnItemExtension.class, 314, dart.getItem().getId());
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		final Dart dart = Dart.forItem(used.getId() == 314 ? with : used);

		if (dart == null) {
			return;
		}

//		if (bolt == Bolt.BROAD_BOLT && !c.getSlayer().getLearned()[0]) {
//		    c.sendMessage("You need to unlock the ability to do this first.");
//		    return true;
//		}

		player.getPA().closeAllWindows();

		if (dart.getLevel() > player.getSkills().getLevel(Skills.FLETCHING)) {
			player.sendMessage("Your fletching must be level "+dart.getLevel()+".");
			return;
		}

		final int craftedSlot = player.getItems().getItemSlot(dart.getProduct().getId());

		if (player.getItems().freeSlots() < 1 && craftedSlot == -1) {
			player.sendMessage("You don't have enough room in your inventory.");
			return;
		}

		player.startAnimation(16402);

		final int actionId = Misc.generateRandomActionId();
		player.uniqueActionId = actionId;
		player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int sets = 20;

			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				if (player.uniqueActionId != actionId || sets <= 0) {
					container.stop();
					return;
				}

				if (player.getItems().freeSlots() < 1 && craftedSlot > -1 && player.playerItems[craftedSlot]-1 != dart.getProduct().getId()) {
					player.sendMessage("You don't have enough room in your inventory.");
					container.stop();
					return;
				}

				int amount = 15;

				if (player.getItems().getItemAmount(used.getId(), usedSlot) < amount) {
					amount = player.getItems().getItemAmount(used.getId(), usedSlot);
				}

				if (player.getItems().getItemAmount(with.getId(), withSlot) < amount) {
					amount = player.getItems().getItemAmount(with.getId(), withSlot);
				}

				if (amount <= 0) {
					player.sendMessage("You have ran out of items.");
					container.stop();
					return;
				}

				if (player.getItems().deleteItemInOneSlot(used.getId(), usedSlot, amount)
						&& player.getItems().deleteItemInOneSlot(with.getId(), withSlot, amount)) {
					player.getItems().addItem(dart.getProduct().getId(), amount);
					player.getPA().addSkillXP((int)(dart.getExperience() * amount), Skills.FLETCHING);
					player.startAnimation(16402);
					player.sendMessageSpam("You attach a feather to a dart.");
					sets--;
				} else {
					player.sendMessage("WTF?!?!?!?!?!?!?!?!?!?! 82382");
					container.stop();
				}
			}
		}, 1);
	}

}
