package plugin.skills.fletching;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.javs.JavelinEnum;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class JavelinCreatePlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for(JavelinEnum jav : JavelinEnum.values()) {
			map(ItemOnItemExtension.class, jav.getHeadId(), JavelinEnum.JAVELIN_SHAFT);
			map(ItemOnItemExtension.class, JavelinEnum.JAVELIN_SHAFT, jav.getHeadId());
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		if (player.skillingEvent != null) {
			return;
		}

		final JavelinEnum jav = JavelinEnum.get(used.getId() == JavelinEnum.JAVELIN_SHAFT ? with.getId() : used.getId());

		if (jav == null) {
			return;
		}

		if (jav.getLvlReq() > player.getSkills().getLevel(Skills.FLETCHING)) {
			player.sendMessage("Your Fletching must be level " + jav.getLvlReq() + ".");
			return;
		}

		final int craftedSlot = player.getItems().getItemSlot(jav.getProductId());

		if (player.getItems().freeSlots() < 1 && craftedSlot == -1) {
			player.sendMessage("You don't have enough room in your inventory.");
			return;
		}

		player.startAnimation(GemTipsOnBoltsPlugin.ANIMATION);
		player.startAction(new CycleEvent() {

			int madeSets = 0;

			@Override
			public void stop() {
				/* empty */
			}

			@Override
			public void execute(CycleEventContainer container) {
				if (player.getItems().freeSlots() < 1 && craftedSlot > -1 && player.playerItems[craftedSlot] - 1 != jav.getProductId()) {
					player.sendMessage("You don't have enough room in your inventory.");
					container.stop();
					return;
				}

				int amount = 15;

				if (player.getItems().getItemAmount(used.getId(), usedSlot) < amount) {
					amount = player.getItems().getItemAmount(used.getId(), usedSlot);
				}

				if (player.getItems().getItemAmount(with.getId(), withSlot) < amount) {
					amount = player.getItems().getItemAmount(with.getId(), withSlot);
				}

				if (amount <= 0) {
					player.sendMessage("You have ran out of materials.");
					container.stop();
					return;
				}
				
				player.startAnimation(GemTipsOnBoltsPlugin.ANIMATION);

				if (player.getItems().deleteItemInOneSlot(used.getId(), usedSlot, amount)
						&& player.getItems().deleteItemInOneSlot(with.getId(), withSlot, amount)) {
					player.getItems().addItem(jav.getProductId(), amount);
					player.getPA().addSkillXP((int) (jav.getExp() * amount), Skills.FLETCHING);
					player.startAnimation(GemTipsOnBoltsPlugin.ANIMATION);
					player.sendMessageSpam("You attach the javelin heads to the shafts.");
					madeSets++;
				} else {
					player.sendMessage("WTF?!?!?!?!?!?!?!?!?!?! 253522");
					container.stop();
				}

				if (madeSets == 20) {
					player.sendMessage("You have made 300 javelins.");
					container.stop();
				}
			}
		}, 1);
	}

}