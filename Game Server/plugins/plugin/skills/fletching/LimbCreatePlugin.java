package plugin.skills.fletching;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.stringing.LimbData;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class LimbCreatePlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for (LimbData data : LimbData.values) {
			map(ItemOnItemExtension.class, data.getLimb(), data.getStock());
			map(ItemOnItemExtension.class, data.getStock(), data.getLimb());
		}
	}

	@Override
	public void onItemOnItem(Player c, Item used, int usedSlot, Item with, int withSlot) {
		LimbData limbData = LimbData.forItems(used.getId(), with.getId());
		if (limbData == null || !passRequirements(c, limbData) || c.skillingEvent != null) {
			return;
		}

		final LimbData finalData = limbData;
		c.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				c.getItems().deleteItemInOneSlot(finalData.getLimb(), 1);
				c.getItems().deleteItemInOneSlot(finalData.getStock(), 1);
				c.getItems().addItem(finalData.getProduct(), 1);
				c.getPA().addSkillXP((int) finalData.getExperience(), Skills.FLETCHING);
				c.sendMessageSpam("You attach the metal limbs to the stock..");
				c.startAnimation(finalData.getAnimation());

				if (!passRequirements(c, finalData)) {
					container.stop();
				}
			}

			@Override
			public void stop() {
				/* empty */
			}
		}, 4);
	}

	private static boolean passRequirements(Player player, LimbData data) {
		if (!player.getItems().playerHasItem(data.getLimb()) || !player.getItems().playerHasItem(data.getStock())) {
			player.sendMessage("You don't have any materials left.");
			return false;
		}

		if (player.getPlayerLevel()[Skills.FLETCHING] < data.getLevel()) {
			player.sendMessage("You need a Fletching level of " + data.getLevel() + " to combine these items.");
			return false;
		}

		return true;
	}

}
