package plugin.skills.fletching;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.bolts.GemBolt;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class GemTipsOnBoltsPlugin extends Plugin implements ItemOnItemExtension {

	public static final Animation ANIMATION = new Animation(16402);

	@Override
	public void onInit() {
		for (GemBolt bolt : GemBolt.values) {
			map(ItemOnItemExtension.class, bolt.getTip().getId(), bolt.getBase().getId());
			map(ItemOnItemExtension.class, bolt.getBase().getId(), bolt.getTip().getId());
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		if (player.skillingEvent != null) {
			return;
		}

		final GemBolt bolt = GemBolt.forItems(used, with);

		if (bolt == null) {
			return;
		}

		if (bolt.getLevel() > player.getSkills().getLevel(Skills.FLETCHING)) {
			player.sendMessage("Your fletching must be level " + bolt.getLevel() +".");
			return;
		}

		final int craftedSlot = player.getItems().getItemSlot(bolt.getProduct().getId());

		if (player.getItems().freeSlots() < 1 && craftedSlot == -1) {
			player.sendMessage("You don't have enough room in your inventory.");
			return;
		}

		player.startAnimation(ANIMATION);
		player.startAction(new CycleEvent() {

			int madeSets = 0;

			@Override
			public void stop() {
				/* empty */
			}

			@Override
			public void execute(CycleEventContainer container) {
				if (player.getItems().freeSlots() < 1 && craftedSlot > -1 && player.playerItems[craftedSlot] - 1 != bolt.getProduct().getId()) {
					player.sendMessage("You don't have enough room in your inventory.");
					container.stop();
					return;
				}

				int amount = 15;

				if (player.getItems().getItemAmount(used.getId(), usedSlot) < amount) {
					amount = player.getItems().getItemAmount(used.getId(), usedSlot);
				}

				if (player.getItems().getItemAmount(with.getId(), withSlot) < amount) {
					amount = player.getItems().getItemAmount(with.getId(), withSlot);
				}

				if (amount <= 0) {
					player.sendMessage("You have ran out of items.");
					container.stop();
					return;
				}

				if (player.getItems().deleteItemInOneSlot(used.getId(), usedSlot, amount)
						&& player.getItems().deleteItemInOneSlot(with.getId(), withSlot, amount)) {
					player.getItems().addItem(bolt.getProduct().getId(), amount);
					player.getPA().addSkillXP((int) (bolt.getExperience() * amount), Skills.FLETCHING);
					player.startAnimation(ANIMATION);
					player.sendMessageSpam("You attach bolt tips to a bolt.");
					madeSets++;
				} else {
					player.sendMessage("WTF?!?!?!?!?!?!?!?!?!?! 42642");
					container.stop();
				}

				if (madeSets == 20) {
					player.sendMessage("You have made 300 bolts.");
					container.stop();
				}
			}
		}, 1);
	}

}