package plugin.skills.fletching;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.fletching.bolts.GemBoltTips;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public final class GemBoltPlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for(GemBoltTips gemBoltTips : GemBoltTips.values) {
			map(ItemOnItemExtension.class, gemBoltTips.getGem(), 1755);
			map(ItemOnItemExtension.class, 1755, gemBoltTips.getGem());
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		if (player.skillingEvent != null) {
			return;
		}

		int gemId = -1;
		if (used.getId() == 1755) {
			gemId = with.getId();
		} else if (with.getId() == 1755) {
			gemId = used.getId();
		}

		if (gemId == -1) {
			return;
		}

		final GemBoltTips gem = GemBoltTips.forItem(gemId);

		if (gem == null) {
			return;
		}

		if (gem.getLevel() > player.getSkills().getLevel(Skills.FLETCHING)) {
			player.sendMessage("Your Fletching must be level " + gem.getLevel() + ".");
			return;
		}

		final int craftedSlot = player.getItems().getItemSlot(gem.getBolt().getId());

		if (player.getItems().freeSlots() < 1 && craftedSlot == -1) {
			player.sendMessage("You don't have enough room in your inventory.");
			return;
		}

		player.startAnimation(gem.getAnimation());

		String message = "You fletch " + gem.getBolt().getAmount() + " bolt tips.";

		player.startAction(new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (player.getItems().freeSlots() < 1 && craftedSlot > -1 && player.playerItems[craftedSlot] - 1 != gem.getBolt().getId()) {
					player.sendMessage("You don't have enough room in your inventory.");
					container.stop();
					return;
				}

				if (player.getItems().deleteItemInOneSlot(gem.getGem(), 1)) {
					player.startAnimation(gem.getAnimation());
					player.getItems().addItem(gem.getBolt().getId(), gem.getBolt().getAmount());
					player.getPA().addSkillXP(gem.getExperience(), Skills.FLETCHING);
					player.sendMessageSpam(message);
				} else {
					container.stop();
				}

			}

			@Override
			public void stop() {
				/* empty */
			}

		}, 1);
	}

}
