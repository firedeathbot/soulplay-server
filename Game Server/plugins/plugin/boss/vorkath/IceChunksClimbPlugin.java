package plugin.boss.vorkath;

import com.soulplay.content.npcs.impl.bosses.vorkath.VorkathBoss;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class IceChunksClimbPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 31990);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (player.getY() == 4052)
			VorkathBoss.enterInstance(player);
		else {
			VorkathBoss.exitInstance(player);
		}
	}
	
}
