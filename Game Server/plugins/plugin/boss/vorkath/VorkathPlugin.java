package plugin.boss.vorkath;

import com.soulplay.content.npcs.impl.bosses.vorkath.VorkathBoss;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class VorkathPlugin extends Plugin implements NpcClickExtension {
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, 8971);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		VorkathBoss.pokeBoss(player, npc);
	}
	

}
