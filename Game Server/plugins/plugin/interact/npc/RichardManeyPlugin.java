package plugin.interact.npc;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class RichardManeyPlugin extends Plugin implements NpcClickExtension {

    private static final int RICHARD_MANEY = 11460;

    @Override
    public void onInit() {
        map(NpcClickExtension.class, RICHARD_MANEY);
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {

            player.getDialogueBuilder().sendNpcChat(RICHARD_MANEY, "How can i help you?")
                    .sendPlayerChat("Who are you?")
                    .sendNpcChat(RICHARD_MANEY, "A good question.", "I will be selling private Islands in the future!")
                    .sendPlayerChat("That sounds fantastic!")
                    .sendNpcChat(RICHARD_MANEY, "You will be able to invite friends", "to visit your Island and upgrade it with cool features.")
                    .sendPlayerChat("Thank you, i will come back later!").execute();
    }

}
