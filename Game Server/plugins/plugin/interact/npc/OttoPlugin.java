package plugin.interact.npc;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class OttoPlugin extends Plugin implements NpcClickExtension {

    @Override
    public void onInit() {
        map(NpcClickExtension.class, 2725);
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
    	player.getDialogueBuilder().sendOption("Can you forge me a Zamorakian hasta?", () -> {
    		player.getDialogueBuilder().sendNpcChat("Yes, I can convert a Zamorakian spear into a hasta.", "The spirits require me to request 500,000,000", "coins from you for this service.");
    		player.getDialogueBuilder().sendOption("Do you wish to convert your spear?", "Yes", () -> {
    			if (player.getItems().playerHasItem(11716) && player.getItems().playerHasItem(995, 500_000_000)) {
	    			player.getDialogueBuilder().sendStatement("Otto sets to work...").sendAction(() -> {
	    				if (player.getItems().takeCoins(500_000_000) && player.getItems().deleteItem2(11716, 1)) {
	    					player.getItems().addItem(111889, 1);
	    					player.getDialogueBuilder().sendItemStatement(111889, "Otto hands you the Zamorakian hasta.");
	    				}
	    			});
    			} else {
    				player.getDialogueBuilder().sendPlayerChat("Actually, forget it, I don't have that amount of money", "on me right now.").sendNpcChat("As you wish.").sendAction(() -> player.getDialogueBuilder().close());
    			}
    		}, "No", () -> {
    			player.getDialogueBuilder().sendPlayerChat("Actually, forget it, that's too much for me.").sendNpcChat("As you wish.").sendAction(() -> player.getDialogueBuilder().close());
    		});
    	}, "I have no more questions at this time.", () -> {
    		player.getDialogueBuilder().sendPlayerChat("I have no more questions at this time.").sendNpcChat("In that case, farewell.").sendAction(() -> player.getDialogueBuilder().close());
    	}).executeIfNotActive();
    }

}
