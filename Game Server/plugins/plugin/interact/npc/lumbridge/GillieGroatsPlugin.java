package plugin.interact.npc.lumbridge;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class GillieGroatsPlugin extends Plugin implements NpcClickExtension {

	private static final int GILLIE_GROATS_ID = 3807;
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, GILLIE_GROATS_ID);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		buildChat(player, false);
	}
	
	
	public static void buildChat(Player p, boolean clickCow) {
		p.npcType = GILLIE_GROATS_ID;
		
		if (clickCow) {
			p.getDialogueBuilder()
			.sendNpcChat("Tee hee! You've never milked a cow before, have you?")
			.sendPlayerChat("Erm... No. How could you tell?")
			.sendNpcChat(Expression.EXCITED_LAUGH, "Because you're spilling milk all over the floor. What a",
					"waste! You need something to hold the milk.")
			.sendPlayerChat("Ah yes, I really should have guessed that one, shouldn't", "I?")
			.sendNpcChat(Expression.EXCITED_LAUGH, "You're from the city, aren't you... Try it again with an", "empty bucket.")
			.sendPlayerChat("Right, I'll do that.")
			.execute();
		} else {
			p.getDialogueBuilder()
			.sendNpcChat("Hello, I'm Gillie the Milkmaid. What can I do for you?")
			.sendOption("Who are you?", ()->{
				p.getDialogueBuilder().jumpToStage(2);
			}, "Can you tell me how to milk a cow?", ()->{
				p.getDialogueBuilder().jumpToStage(7);
			}, "I'm fine, thanks.", ()->{
				p.getDialogueBuilder().jumpToStage(15);
			})
			
			.sendNpcChat("My name's Gillie Groats. My father is a farmer and I", "milk the cows for him.")
			.sendPlayerChat("Do you have any buckets of milk spare?")
			.sendNpcChat("I'm afraid not. We need all of our milk to sell to",
					"market, but you can milk the cow yourself if you need",
					"milk.")
			.sendPlayerChat("Thanks")
			.sendAction(()->{
				p.getDialogueBuilder().jumpToStage(100);
			})
			
			.sendPlayerChat("So how do you get milk from a cow then?")
			.sendNpcChat("It's very easy. First you need an empty bucket to hold",
					"the milk.")
			.sendNpcChat("Then find a dairy cow to milk - you can't milk just",
					"any cow.")
			.sendPlayerChat("How do I find a dairy cow?")
			.sendNpcChat("They are easy to spot - they are dark brown and",
					"white, unlike beef cows, which are light brown and white.",
					"We also tether them to a post to stop them wandering",
					"around all over the place.")
			.sendNpcChat("There are a couple very near, in this field.")
			.sendNpcChat("Then just milk the cow and your bucket will fill with",
					"tasy, nutritious milk.")
			.sendAction(()-> {
				p.getDialogueBuilder().jumpToStage(100);
			})
			
			.sendPlayerChat("I'm fine, thanks.").execute();
			
		}
	}
}
