package plugin.interact.npc.lumbridge;

import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.content.player.quests.cooksassistant.CooksAssistant;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class LumbridgeCookPlugin extends Plugin implements NpcClickExtension {

	public static final int COOK_NPC_ID = 278;
	
    @Override
    public void onInit() {
        map(NpcClickExtension.class, COOK_NPC_ID);
    }

	@Override
	public void onNpcOption1(Player p, NPC n) {
		startCooksAssistantQuestChat(p, n);
	}
	
	private static void startCooksAssistantQuestChat(Player p, NPC n) {
		
		if (QuestHandlerTemp.getQuestProgress(p, QuestIndex.COOKS_ASSISTANT) == 0) {
			buildCooksAssistNotStarted(p);
		} else if (!QuestHandlerTemp.completedQuest(p, QuestIndex.COOKS_ASSISTANT)) {
			buildCooksAssistInProgress(p, 30);
		} else { // regular chat
			buildRegularDialogue(p);
		}
	}
	
	private static void buildRegularDialogue(Player p) {
		p.getDialogueBuilder()
		.sendNpcChat("Hello friend, how is the adventuring going?")
		.sendOption("I am getting strong and mighty.", ()-> {
			// already jumps to the next stage on its own unless you add something in between
		}, "I keep on dying.", ()->{
			p.getDialogueBuilder().jumpToStage(5);
		}, "Can I use your range?", ()-> {
			p.getDialogueBuilder().jumpToStage(8);
		});
		
		p.getDialogueBuilder()
		.sendPlayerChat("I am getting strong and mighty. Grrr")
		.sendNpcChat("Glad to hear it.")
		.sendAction(()-> { p.getDialogueBuilder().jumpToStage(100);});
		
		p.getDialogueBuilder()
		.sendPlayerChat("I keep on dying.")
		.sendNpcChat("Ah well, at least you keep coming back to life!")
		.sendAction(()-> { p.getDialogueBuilder().jumpToStage(100);});
		
		p.getDialogueBuilder()
		.sendPlayerChat("Can I use your range?")
		.sendNpcChat("Go ahead - it's a very good range. It's easier to use",
				"than most other ranges.")
		.sendNpcChat("Its called the Cook-o-matic 100. It uses a combination of",
				"state of the art temperature regulation and magic.")
		.sendPlayerChat("Will it mean my food will burn less often?")
		.sendNpcChat("Well, that's what the salesman told us anyway...")
		.sendPlayerChat(Expression.CONFUSED, "Thanks?")
		.sendAction(()-> { p.getDialogueBuilder().jumpToStage(100);});
		
		
		p.getDialogueBuilder().execute();
	}
	
	private static void buildCooksAssistInProgress(Player p, int startStage) {
		p.getDialogueBuilder().sendNpcChat("Lesik").sendNpcChat(Expression.DEFAULT, "Oh thank you, thank you. I need milk, an egg and",
				"flour. I'd be very greatful if you can get them for me.")
		.sendAction(()-> {
			//skip to ingredients
			p.getDialogueBuilder().jumpToStage(10);
		})
		.sendNpcChat("How are you getting on with finding the ingredients?111")
		.sendPlayerChat("I haven't got any of them yet, I'm still looking.")
		.sendNpcChat("Please get the ingredients quickly. I'm running out of", "time! The Duke will throw me into the streets!")
		.sendStatement("You still need to get:", CooksAssistant.buildRequirementsString(p))
		.sendOption("I'll get right on it.", ()->{
			p.getDialogueBuilder().jumpToStage(8);
		}, "Can you remind me how to find these things again?", ()->{
			//skip to ingredients
			p.getDialogueBuilder().jumpToStage(10);
		})
		.sendPlayerChat("I'll get right on it.")
		.sendAction(()-> {
			p.getDialogueBuilder().jumpToStage(100);
			p.getDialogueBuilder().close();
		})
		.sendPlayerChat("So where do I find these ingredients then?")
		.sendOption("Where do I find some flour?", ()-> {
			p.getDialogueBuilder().jumpToStage(14);
		}, "How about milk?", ()-> {
			p.getDialogueBuilder().jumpToStage(17);
		}, "And eggs? Where are they found?", ()-> {
			p.getDialogueBuilder().jumpToStage(20);
		}, "Actually, I know where to find this stuff",  ()-> {
			p.getDialogueBuilder().jumpToStage(12);
		})
		.sendPlayerChat("Actually I know where to find this stuff.")
		.sendAction(() -> {
			p.getDialogueBuilder().jumpToStage(100);
			p.getDialogueBuilder().close();
		});
		
		//flour
		p.getDialogueBuilder()
		.sendNpcChat("There is a Mill fairly close, go North and then West.",
				"Mill Lane Mill is just off the road to Draynor. I",
				"usually get my flour from there.")
		.sendNpcChat("Talk to Millie, she'll help, she's a lovely girl and a fine", "Miller...")
		.sendAction(() -> {
			// jump back to ingredients page
			p.getDialogueBuilder().jumpToStage(10);
		});
		
		//milk
		p.getDialogueBuilder()
		.sendNpcChat("There is a cattle field on the other side of the river,",
				"Just across the road from the Groats' Farm.")
		.sendNpcChat("Talk to Gillie Groats, she looks after the Dairy cows -",
				"she'll tell you everything you need to know about",
				"milking cows!")
		.sendAction(() -> {
			// jump back to ingredients page
			p.getDialogueBuilder().jumpToStage(10);
		});
		
		//eggs
		p.getDialogueBuilder()
		.sendNpcChat("I normally get my eggs from the Groats' Farm, on the",
				"other side of the river.")
		.sendNpcChat("But any chicken should lay eggs.")
		.sendAction(() -> {
			// jump back to ingredients page
			p.getDialogueBuilder().jumpToStage(10);
		});
		
		p.getDialogueBuilder()
		.sendAction(()-> {
			//cheaphax to send to start
			p.getDialogueBuilder().jumpToStage(0);
		});
		
		p.getDialogueBuilder()
		.sendNpcChat("You've brought me everything I need! I am saved!", "Thank you!")
		.sendPlayerChat("So do I get to go to the Duke's Party?")
		.sendNpcChat("I'm afraid not, only the big cheeses get to dine with the",
				"Duke.")
		.sendPlayerChat("Well, maybe one day I'll be important enough to sit on",
				"the Duke's table.")
		.sendNpcChat("Maybe, but I won't be holding my breath.")
		.sendAction(()-> {
			//send completed quest!!!
			if (CooksAssistant.collectedAllIngredients(p)) { // double check just in case someone creates a bug XD
				p.getDialogueBuilder().onReset(()-> { 
					CooksAssistant.rewardCompletion(p);
				});
			}
			p.getDialogueBuilder().jumpToStage(100);//close out
			
		});
		
		p.getDialogueBuilder()
		.sendNpcChat("How are you getting on with finding the ingredients?");

		CooksAssistant.checkForIngredients(p);
		
		p.getDialogueBuilder()
		.sendAction(()-> {
			if (CooksAssistant.collectedAllIngredients(p)) {
				p.getDialogueBuilder().jumpToStage(23);
			} else if (!CooksAssistant.completedOneIngredient(p)) {
				p.getDialogueBuilder().jumpToStage(3);
			}
		})
		.sendNpcChat("Thanks for ingredients you have got so far, please get",
				"the rest quickly. I'm running out of time! The Duke",
				"will throw me into the streets!")
		.sendAction(()-> {
			p.getDialogueBuilder().jumpToStage(6);
		});
		
		if (startStage > 0) {
			p.getDialogueBuilder().jumpToStage(startStage);
			p.getDialogueBuilder().executeIfNotActive();
		}
	}
	
	private static void buildCooksAssistNotStarted(Player p) {

		p.getDialogueBuilder().sendNpcChat(Expression.DEPRESSED, "What am I to do?")
		.sendOption("What's wrong?", ()-> {
			p.getDialogueBuilder().jumpToStage(2);
		}, "Can you make me a cake?", ()->{
			p.getDialogueBuilder().returnToCheckpoint(2);
		}
		, "You don't look very happy.", ()->{
			p.getDialogueBuilder().returnToCheckpoint(4);
		}, "Nice hat!", ()-> {
			p.getDialogueBuilder().returnToCheckpoint(5);
		});
		
		whatsWrongDialogue(p);
		buildAcceptCooksAssistQuest(p);
		buildRejection(p);
		buildCakeDialogue(p);
		buildHappyDialogue(p);
		buildHappyDialoguePart2(p);
		createNiceHatCheckPoint(p);
		
		//execute at the very end
		p.getDialogueBuilder().executeIfNotActive();
	
	}
	
	private static void whatsWrongDialogue(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(1)
			.sendPlayerChat(Expression.CONFUSED, "What's wrong?")
			.sendNpcChat(Expression.DEPRESSED, "Oh dear, oh dear, oh dear, I'm in a terrible terrible",
					"mess! It's the Duke's birthday today, and I should be",
					"making him a lovely big birthday cake.")
			.sendNpcChat(Expression.DEPRESSED, "I've forgotten to buy the ingredients. I'll never get",
					"them in time now. He'll sack me! What will I do? I have",
					"four children and a goat to look after. Would you help",
					"me? Please?")
			.sendOption("I'm always happy to help a cook in distress.", ()-> {
				p.getDialogueBuilder().returnToCheckpoint(3);
			}, "I can't right now, Maybe later.", ()-> {
				p.getDialogueBuilder().jumpToStage(9);
			})
			;
	}
	
	private static void buildCakeDialogue(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(2)
			.sendPlayerChat(Expression.DEFAULT, "You're a cook, why don't you bake me a cake?")
			.sendNpcChat(Expression.DEPRESSED, "*sniff* Don't talk to me about cakes...")
			.sendAction(()-> {
				p.getDialogueBuilder().returnToCheckpointFromSendAction(1);
			});
	}
	
	private static void buildAcceptCooksAssistQuest(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(3)
		.sendPlayerChat(Expression.DEFAULT, "Yes, I'll help you.")
		.sendAction(()-> {
			if (QuestIndex.COOKS_ASSISTANT.getProgress(p) == 0) {
				QuestHandlerTemp.incQuestProgress(p, QuestIndex.COOKS_ASSISTANT);
				
				p.getDialogueBuilder().reset();
				buildCooksAssistInProgress(p, 0);
			}
		});
	}
	
	private static void buildRejection(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(9)
		.sendPlayerChat(Expression.DEFAULT, "No, I don't feel like it. Maybe later.")
		.sendNpcChat(Expression.DEPRESSED, "Fine. I always knew you Adventurer types were cllous",
				"beasts. Go on your merry way!")
		.sendAction(()-> {
			p.getDialogueBuilder().jumpToStage(100); //lesik's cheaphax to exit dialogues XD
			p.getDialogueBuilder().close();
		});
	}
	
	private static void buildHappyDialogue(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(4)
			.sendPlayerChat(Expression.CONFUSED, "You don't look very happy.")
			.sendNpcChat(Expression.DEPRESSED, "No, I'm not. The world is caving in around me - I am",
					"overcome by dark feelings of impending doom.")
			.sendOption("What's wrong?", ()-> {
				p.getDialogueBuilder().returnToCheckpoint(1);
			}, "I'd take the rest of the day off if I were you.", ()-> {
				p.getDialogueBuilder().returnToCheckpoint(8);
			});
	}
	
	private static void buildHappyDialoguePart2(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(8)
			.sendPlayerChat(Expression.WORRIED, "I'd take the rest of the day off if I were you.")
			.sendNpcChat(Expression.DEPRESSED, "No, that's the worst thing I could do. I'd get in terrible",
					"trouble.")
			.sendPlayerChat(Expression.NO_EXPRESSION, "Well maybe you need to take a holiday...")
			.sendNpcChat(Expression.DEPRESSED, "That would be nice, but the Duke doesn't allow holidays"
					, "for core staff.")
			.sendPlayerChat(Expression.WORRIED, "Hmm, why not run away to the sea and start a new"
					, "life as a Pirate?")
			.sendNpcChat(Expression.DEPRESSED, "My wife gets sea sick, and I have an irrational fear of"
					, "eyepatches. I don't see it working myself.")
			.sendPlayerChat(Expression.WORRIED, "I'm afraid I've run out of ideas.")
			.sendNpcChat(Expression.DEPRESSED, "I know I'm doomed.").sendAction(()-> {
				p.getDialogueBuilder().returnToCheckpointFromSendAction(1);
			});
	}
	
	private static void createNiceHatCheckPoint(Player p) {
		p.getDialogueBuilder().createCheckpointByListSize(5)
		.sendPlayerChat("Nice hat!")
		.sendNpcChat(Expression.DEPRESSED, "Err thank you. It's a pretty ordinary cooks hat really.")
		.sendPlayerChat("Still, suits you. The trousers are pretty special too.")
		.sendNpcChat(Expression.DEPRESSED, "It's all standard cook's issue uniform...")
		.sendPlayerChat(Expression.CALM_TALK, "The whole hat, apron, stripey trousers ensemble - it",
				"works. It make you looks like a real cook.")
		.sendNpcChat(Expression.MEAN_FACE, "I am a real cook! I haven't got time to be chatting",
				"about Culinary Fashion. I am in desperate need of help!")
		.sendAction(()-> {
			p.getDialogueBuilder().returnToCheckpointFromSendAction(1);
		});
	}
	
}
