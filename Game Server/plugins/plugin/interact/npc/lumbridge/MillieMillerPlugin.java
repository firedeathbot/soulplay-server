package plugin.interact.npc.lumbridge;

import com.soulplay.Config;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class MillieMillerPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 3806);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder().sendNpcChat("Hello Adventurer. Welcome to Mill Lane Mill. Can I", "help you?")
		.sendOption("Who are you?", ()-> {
			player.getDialogueBuilder().jumpToStage(14);
		}, "What is this place?", ()-> {
			player.getDialogueBuilder().jumpToStage(18);
		}, "How do I mill flour?", ()-> {
			player.getDialogueBuilder().jumpToStage(2);
		}, "I'm fine, thanks.", ()-> {
			player.getDialogueBuilder().jumpToStage(22);
		})
		.sendPlayerChat(Expression.CONFUSED, "How do I mill flour?")
		.sendNpcChat("Making flour is pretty easy. First of all you need to",
				"get some grain. You can pick some from wheat fields.",
				"There is one just outside the Mill, but there are many",
				"others scattered across "+Config.SERVER_NAME+". Feel free to pick")
		.sendNpcChat("wheat from our fields! There always seems to be plenty", "of wheat there.")
		.sendPlayerChat(Expression.CONFUSED, "Then I bring my wheat here?")
		.sendNpcChat("Yes, or one of the other mills in "+Config.SERVER_NAME+". They all",
				"work the same way. Just take your grain to the top",
				"floor of the mill (up two ladders, there are three floors",
				"including this one) and then place some grain into the")
		.sendNpcChat("hopper.")
		.sendNpcChat("Then you need to start the grinding process by pulling",
				"the hopper lever. You can add more grain, but each",
				"time you add grain you have to pull the hopper lever",
				"again.")
		.sendPlayerChat(Expression.CONFUSED, "So where does the flour go then?")
		.sendNpcChat("The flour appears in the room here, you'll need a pot",
				"to put the flour into. One pot will hold the flour made",
				"by one load of grain")
		.sendNpcChat("And that's it! You now have some pots of finely ground",
				"flour of the highest quality. Ideal for making tasty cakes",
				"or delicious bread. I'm not a cook so you'll have to ask a",
				"cook to find out how to bake things.")
		.sendPlayerChat("Great! Thanks for your help.")
		.sendAction(()-> {
			player.getDialogueBuilder().jumpToStage(100);
			player.getDialogueBuilder().close();
		})
		
		
		.sendPlayerChat("Who are you?")
		.sendNpcChat("I'm Miss Millicent Miller the Miller of Mill Lane Mill.",
				"Our family have been milling flour for generations.")
		.sendPlayerChat("It's a good business to be in. People will always need", "flour.")
		.sendAction(()-> {
			player.getDialogueBuilder().jumpToStage(1);
		})
		
		.sendPlayerChat("What is this place?")
		.sendNpcChat("This is Mill Lane Mill. Millers of the finest flour in",
				Config.SERVER_NAME+", and home to the Miller family for many",
				"generations.")
		.sendNpcChat("We take grain from the field nearby and mill into flour.")
		.sendAction(()-> {
			player.getDialogueBuilder().jumpToStage(1);
		})
		
		.sendPlayerChat("I'm fine, thanks.");
		
		player.getDialogueBuilder().execute();
	}
}
