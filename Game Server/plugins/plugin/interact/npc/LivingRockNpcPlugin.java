package plugin.interact.npc;

import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.mining.*;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.util.Misc;

public class LivingRockNpcPlugin extends Plugin implements NpcClickExtension {
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, 8832);
		map(NpcClickExtension.class, 8833);
		map(NpcClickExtension.class, 8834);
	}

	@Override
	public void onNpcOption1(Player c, NPC npc) {
		if (c.skillingEvent != null) {
			return;
		}

		Pickaxes pickaxe = Mining.getPickaxe(c);
		if (pickaxe == null) {
			return;
		}

		if (!c.getItems().playerHasItem(15263) && !Mining.hasInventorySpace(c, "mining")) {
			Mining.resetMining(c);
			return;
		}

		if (!Mining.hasRequiredLevel(c, Skills.MINING, 73, "mining",
				"mine here")) {
			return;
		}

		c.sendMessageSpam("You swing your pick at the rock.");
		c.startAnimation(pickaxe.getAnimation());

		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				if (!c.getItems().playerHasItem(15263) && !Mining.hasInventorySpace(c, "mining")) {
					Mining.resetMining(c);
					container.stop();
					return;
				}

				Item produce = new Item(15263, Misc.random(5, 24));
				c.increaseProgress(TaskType.MINING, produce.getId(), produce.getAmount());
				c.getItems().addItem(produce.getId(), produce.getAmount());
				c.sendMessageSpam("You manage to mine some " + ItemConstants.getItemName(produce.getId()).toLowerCase() + ".");
				int xp = 25;
				if (c.inWild()) {
					xp *= 2;
				}

				c.getPA().addSkillXP(Mining.applyMiningXpBoosts(c, xp), Skills.MINING);
				SkillingUrns.handleUrn(c, Skills.MINING, 73, xp);
				npc.nullNPC();
				c.resetAnimations();
				container.stop();
			}

			@Override
			public void stop() {
				c.skillingEvent = null;
			}
		}, 3);
	}
	
}
