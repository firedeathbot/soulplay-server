package plugin.interact.npc;

import com.soulplay.content.player.skills.herblore.Potion;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnNpcExtension;
import com.soulplay.plugin.extension.NpcClickExtension;

public class DecantPotionPlugin extends Plugin implements NpcClickExtension, ItemOnNpcExtension {

    @Override
    public void onInit() {
        map(NpcClickExtension.class, 367);

        for (Potion potion : Potion.values) {
            map(ItemOnNpcExtension.class, 367, potion.getDose1());
            map(ItemOnNpcExtension.class, 367, potion.getDose1Cert());
            map(ItemOnNpcExtension.class, 367, potion.getDose2());
            map(ItemOnNpcExtension.class, 367, potion.getDose2Cert());
            map(ItemOnNpcExtension.class, 367, potion.getDose3());
            map(ItemOnNpcExtension.class, 367, potion.getDose3Cert());
            map(ItemOnNpcExtension.class, 367, potion.getDose4());
            map(ItemOnNpcExtension.class, 367, potion.getDose4Cert());
        }
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        player.getDialogueBuilder().sendNpcChat(npc.npcType, "Hey there, I've discovered a new method of potion ",
                "decanting. I can decant all potions in your inventory at a",
                "fee of 500k. Would you like me to decant your potions?")
                .sendOption("Yes, please.", () -> {
                    if (player.getItems().playerHasItem(995, 500_000)) {
                        player.getDialogueBuilder().sendNpcChat(npc.npcType, "Choose how many doses you want your potions to be.")
                                .sendEnterAmount(() -> {
                                    final int dose = (int) player.enterAmount;

                                    if (dose >= 1 && dose <= 4) {
                                        if (startDecanting(player, dose)) {
                                            player.getItems().deleteItemInOneSlot(995, 500_000);
                                            player.getDialogueBuilder().sendNpcChat(npc.npcType, "I have finished decanting your potions!");
                                        } else {
                                            player.getDialogueBuilder().sendNpcChat(npc.npcType, "You don't have anything to decant!");
                                        }
                                    } else {
                                        player.getDialogueBuilder().sendNpcChat(npc.npcType, "You can only decant 1-4 doses.");
                                    }
                                });
                    } else {
                        player.sendMessage("You don't have enough coins to decant your potions!");
                    }
        }, "No thanks, I can decant them myself.", () -> {}).execute();
    }

    @Override
    public void onItemOnNpc(Player player, NPC npc, int itemId, int slot) {
        if (player.getItems().playerHasItem(995, 500_000)) {
            player.getDialogueBuilder().sendNpcChat(npc.npcType, "Choose how many doses you want your potions to be.")
                    .sendEnterAmount(() -> {
                        final int dose = (int) player.enterAmount;

                        if (dose >= 1 && dose <= 4) {
                            if (startDecanting(player, dose)) {
                                player.getItems().deleteItemInOneSlot(995, 500_000);
                                player.getDialogueBuilder().sendNpcChat(npc.npcType, "I have finished decanting your potions!");
                            } else {
                                player.getDialogueBuilder().sendNpcChat(npc.npcType, "You don't have anything to decant!");
                            }
                        } else {
                            player.getDialogueBuilder().sendNpcChat(npc.npcType, "You can only decant 1-4 doses.");
                        }
                    }).execute();
        } else {
            player.sendMessage("You don't have enough coins to decant your potions!");
        }
    }

    private static boolean startDecanting(Player player, int dose) {
        if (dose < 1 || dose > 4) {
            player.sendMessage("Invalid dose wanted.");
            return false;
        }

        boolean decanted = false;
        for (Potion potion : Potion.values) {
            int dosesFound = 0;
            for (int i = 0, length = player.playerItems.length; i < length; i++) {
                int itemId = player.playerItems[i] - 1;
                if (itemId == -1) {
                    continue;
                }

                //Check for normal items
                if (itemId == potion.getDose4()) {
                    dosesFound += 4;
                    player.getItems().removeItemInSlot(i);
                } else if (itemId == potion.getDose3()) {
                    dosesFound += 3;
                    player.getItems().removeItemInSlot(i);
                } else if (itemId == potion.getDose2()) {
                    dosesFound += 2;
                    player.getItems().removeItemInSlot(i);
                } else if (itemId == potion.getDose1()) {
                    dosesFound += 1;
                    player.getItems().removeItemInSlot(i);
                }

                //Check for certs
                if (itemId == potion.getDose4Cert()) {
                    dosesFound += player.playerItemsN[i] * 4;
                    player.getItems().removeItemInSlot(i);
                } else if (itemId == potion.getDose3Cert()) {
                    dosesFound += player.playerItemsN[i] * 3;
                    player.getItems().removeItemInSlot(i);
                } else if (itemId == potion.getDose2Cert()) {
                    dosesFound += player.playerItemsN[i] * 2;
                    player.getItems().removeItemInSlot(i);
                } else if (itemId == potion.getDose1Cert()) {
                    dosesFound += player.playerItemsN[i];
                    player.getItems().removeItemInSlot(i);
                }
            }

            if (dosesFound > 0) {
                int toAddPotion = 0;
                if (dose == 4) {
                    toAddPotion = potion.getDose4Cert();
                } else if (dose == 3) {
                    toAddPotion = potion.getDose3Cert();
                } else if (dose == 2) {
                    toAddPotion = potion.getDose2Cert();
                } else {
                    toAddPotion = potion.getDose1Cert();
                }

                int count = dosesFound / dose;
                int leftOver = dosesFound % dose;
                player.getItems().addItem(toAddPotion, count);
                if (leftOver == 1) {
                    player.getItems().addItem(potion.getDose1Cert(), 1);
                } else if (leftOver == 2) {
                    player.getItems().addItem(potion.getDose2Cert(), 1);
                } else if (leftOver == 3) {
                    player.getItems().addItem(potion.getDose3Cert(), 1);
                } else if (leftOver == 4) {
                    player.getItems().addItem(potion.getDose4Cert(), 1);
                }

                decanted = true;
            }
        }

        player.setUpdateInvInterface(3214);
        return decanted;
    }

}
