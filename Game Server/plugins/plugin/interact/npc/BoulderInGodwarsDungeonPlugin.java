package plugin.interact.npc;

import com.soulplay.Server;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class BoulderInGodwarsDungeonPlugin extends Plugin implements NpcClickExtension {

	public static boolean inUse = false;
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, 4187);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		
		if (player.getSkills().getLevel(Skills.STRENGTH) < 60) {
			player.sendMessage("You need a Strength level of 60 to move this boulder.");
			return;
		}
		
		if (inUse) {
			player.sendMessage("Please wait.");
			return;
		}
		
		final boolean movingWest = player.getX() == 9455;
		
		player.lockActions(4);
		player.lockMovement(4);
		
		inUse = true;
		
		Server.getTaskScheduler().schedule(4, ()-> {
			inUse = false;
		});
		
		Server.getTaskScheduler().schedule(3, ()-> {
			npc.moveY = -1;
			if (player.isActive)
				player.getPA().movePlayer(player.getX() + (movingWest ? -3 : 3), player.getY(), player.getZ());
		});
		
		npc.noClip = true;
		npc.moveY = 1;
		
		player.startAnimation(new Animation(Animation.getOsrsAnimId(6130)));
		
		Server.getTaskScheduler().schedule(1, ()-> {
			if (player.isActive)
				player.setForceMovementRaw(movingWest ? MASK_WEST5 : MASK_EAST5);
		});
	}
	
	private static final ForceMovementMask MASK_EAST5 = ForceMovementMask.createMask(0, 0, 1, 0, Direction.EAST).setCustomSpeed(30, 120).addSecondDirection(3, 0, 0, 0);
	private static final ForceMovementMask MASK_WEST5 = ForceMovementMask.createMask(0, 0, 1, 0, Direction.WEST).setCustomSpeed(30, 120).addSecondDirection(-3, 0, 0, 0);
}
