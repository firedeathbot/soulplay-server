package plugin.interact.npc;

import java.util.ArrayList;
import java.util.List;

import com.soulplay.config.WorldType;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class MysteriousPersonPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 7666);
	}

	@Override
	public void onNpcOption1(Player player, NPC npc) {
		if (!WorldType.isSeasonal()) {
			player.getDialogueBuilder().sendNpcChat("I don't have anything to offer you.").execute();
			return;
		}

		player.getDialogueBuilder().sendNpcChat("Hello, I can sell you some mysterious things.").sendOption("I'd like to trade my unused cards.", () -> {
			player.getDialogueBuilder().sendPlayerChat("I'd like to trade my unused cards.").sendNpcChat("Certainly, hand me 5 of them and I'll give u a black", "card which gives you a random one when opened.");
			player.getDialogueBuilder().sendAction(() -> {
				List<Integer> slots = new ArrayList<>();

				for (int i = 0, length = player.playerItems.length; i < length; i++) {
					int id = player.playerItems[i] - 1;
					if (id == -1 || !PowerUpData.cardsSet.contains(id)) {
						continue;
					}

					slots.add(i);
				}

				int amount = slots.size();
				int newCardAmount = amount / 5;
				if (newCardAmount <= 0) {
					player.getDialogueBuilder().sendNpcChat("Unfortunately, you didn't show me enough cards.", "I can't just give you cards for free.");
				} else {
					player.getDialogueBuilder().sendNpcChat("I see you have " + amount + " cards, I can exchange them", "for " + newCardAmount + " new " + (newCardAmount == 1 ? "card" : "cards") + ".");
					player.getDialogueBuilder().sendOption("Sure, I'll exchange them", () -> {
						player.getDialogueBuilder().sendPlayerChat("Sure, I'll exchange them").sendNpcChat("Certainly.").sendAction(() -> {
							for (int slot : slots) {
								if (!player.getItems().deleteItemInSlot(slot)) {
									return;
								}
							}

							for (int i = 0; i < newCardAmount; i++) {
								player.getItems().addItem(25000, 1);
							}
						});
					}, "No, I'll use them", () -> {
						player.getDialogueBuilder().sendPlayerChat("No, I'll use them.");
					});
				}
			});
		}, "Open your shop.", () -> player.getShops().openShop(80), "That's all for now.", () -> player.getDialogueBuilder().sendPlayerChat("That's all for now.")).execute();
	}

}
