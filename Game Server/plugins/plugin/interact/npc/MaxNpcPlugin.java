package plugin.interact.npc;

import com.soulplay.content.items.useitem.MaxCapeUpgrades;
import com.soulplay.content.items.useitem.MaxCapeUpgrades.Items;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerDifficulty;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class MaxNpcPlugin extends Plugin implements NpcClickExtension {

    private static final int MAX_NPC = 3374;
    private static final int COMP_CAPE_COST = 125_995_000;
    private static final int MAX_CAPE_COST = 52_475_000;
    private static final int MAX_CAPE_HOOD_ID = 20768;
    private static final int MAX_CAPE_ID = 20767;
    private static final int COMP_CAPE_ID = 20769;
    private static final int COMP_CAPE_HOOD_ID = 20770;

    @Override
    public void onInit() {
        map(NpcClickExtension.class, MAX_NPC);
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        player.getDialogueBuilder().sendOption("Completionist Cape", () -> {

            if (player.getPA().getTotalLevel() < Skills.MAX_TOTAL_LEVEL && player.difficulty < PlayerDifficulty.PRESTIGE_ONE) {
                player.getDialogueBuilder().sendNpcChat(MAX_NPC, String.format("Come back when you have a total level of %d.", Skills.MAX_TOTAL_LEVEL));
                return;
            }

            if (player.achievementsCompleted < 100) {
                player.getDialogueBuilder().sendNpcChat(MAX_NPC, "Come back when you have 100 or more achievement points.");
                return;
            }

            player.getDialogueBuilder().sendNpcChat(MAX_NPC, "How can i help you?")
                    .sendPlayerChat("Who are you?")
                    .sendNpcChat(MAX_NPC, "A good question. My name is Max.", "I reward those who are maxed with this cape i am wearing")
                    .sendPlayerChat("Nice to meet you max.", "I was actually wondering what that cape is for?")
                    .sendNpcChat(MAX_NPC, "This cape is a symbol that i have trained all", "of my skills to lvl 99 and completed",
                            "all the quests and achievements")
                    .sendPlayerChat("So have I!")
                    .sendNpcChat(MAX_NPC, "Indeed you have, would you like to buy this cape?", "<col=ff0000>(Price 125.995.000)")
                    .sendOption("I'll take one!", () -> {
                        if (!player.getItems().playerHasItem(995, COMP_CAPE_COST)) {
                            player.getDialogueBuilder().sendNpcChat(MAX_NPC, "Please come back when you have enough coins.");
                            return;
                        }
                        if (player.getItems().freeSlots() > 1) {
                            player.getItems().deleteItemInOneSlot(995, COMP_CAPE_COST);
                            player.getItems().addItem(COMP_CAPE_ID, 1);
                            player.getItems().addItem(COMP_CAPE_HOOD_ID, 1);
                        } else {
                            player.getDialogueBuilder().sendNpcChat(MAX_NPC, "Sorry, but your inventory seems to be full.",
                                    "Please come back with more space.");
                        }
                    }, "No Thanks", () -> {

                    });
        }, "Max Cape", () -> {
        	player.getDialogueBuilder().sendOption("Buy a max cape", () -> {
        		int levelReq = Skills.MAX_TOTAL_LEVEL - 21;
	            if (player.getPA().getTotalLevel() < levelReq && player.difficulty < PlayerDifficulty.PRESTIGE_ONE) {
	                player.getDialogueBuilder().sendNpcChat(MAX_NPC, String.format("Come back when you have a total level of %d.", levelReq));
	                return;
	            }

	            player.getDialogueBuilder().sendNpcChat(MAX_NPC, "How can i help you?")
	                    .sendPlayerChat("Who are you?")
	                    .sendNpcChat(MAX_NPC, "A good question. My name is Max.", "I reward those who are maxed with this cape i am wearing")
	                    .sendPlayerChat("Nice to meet you max.", "I was actually wondering what that cape is for?")
	                    .sendNpcChat(MAX_NPC, "This cape is a symbol that i have trained all", "of my skills to lvl 99.")
	                    .sendPlayerChat("So have I!")
	                    .sendNpcChat(MAX_NPC, "Indeed you have, would you like to buy this cape?", "<col=ff0000>(Price 52.475.000)")
	                    .sendOption("I'll take one!", () -> {
	                        if (!player.getItems().playerHasItem(995, MAX_CAPE_COST)) {
	                            player.getDialogueBuilder().sendNpcChat(MAX_NPC, "Please come back when you have enough coins.");
	                            return;
	                        }
	                        if (player.getItems().freeSlots() > 1) {
	                            player.getItems().deleteItemInOneSlot(995, MAX_CAPE_COST);
	                            player.getItems().addItem(MAX_CAPE_ID, 1);
	                            player.getItems().addItem(MAX_CAPE_HOOD_ID, 1);
	                        } else {
	                            player.getDialogueBuilder().sendNpcChat(MAX_NPC, "Sorry, but your inventory seems to be full.",
	                                    "Please come back with more space.");
	                        }
	                    }, "No Thanks", () -> {
	
	                    });
        	}, "Revert a max cape", () -> {
        		player.getDialogueBuilder().sendNpcChat("I can revert your max cape, it will cost you", "200,000,000 coins and the cape it self.");
        		player.getDialogueBuilder().sendOption("Revert my capes?", "Yes, please.", () -> {
        			player.getDialogueBuilder().sendNpcChat("Hand me the cape with the hood I'll revert to original item.");
        			player.getDialogueBuilder().sendAction(() -> {
        				boolean reverted = false;
        				boolean notEnoughGp = false;
        				for (Items item : MaxCapeUpgrades.Items.values) {
        					if (player.getItems().playerHasItem(item.getNewCapeID()) && player.getItems().playerHasItem(item.getNewHoodID())) {
        						if (player.getItems().takeCoins(200_000_000)) {
        							player.getItems().deleteItem2(item.getNewCapeID(), 1);
        							player.getItems().deleteItem2(item.getNewHoodID(), 1);
        							player.getItems().addItem(item.getUseItemID(), 1);
        							reverted = true;
        						} else {
        							notEnoughGp = true;
        						}
        					}
        				}

        				if (reverted) {
        					player.getDialogueBuilder().sendNpcChat("All done, enjoy mate.");
        				} else {
        					if (notEnoughGp) {
        						player.getDialogueBuilder().sendNpcChat("I see that you have my cape in your inventory.", "But you're 200,000,000 coins short for the service.");
        					} else {
        						player.getDialogueBuilder().sendNpcChat("Unfortunately, I didn't find anything in your inventory.");
        					}
        				}
        			});
        		}, "No, I think they look cool.", () -> {
        			player.getDialogueBuilder().sendPlayerChat("No, I think they look cool.");
        			player.getDialogueBuilder().sendNpcChat("Haha, I agree.");
        		});
        	});
        }).execute();
    }

}
