package plugin.interact.npc.edgeville;

import com.soulplay.content.minigames.ddmtournament.TournamentNpc;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class TournamentNpcPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, TournamentNpc.NPC_ID);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		TournamentNpc.talkToNpc(player);
	}
}
