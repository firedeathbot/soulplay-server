package plugin.interact.npc.edgeville;

import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnNpcExtension;
import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class EasterBunnyQuestPlugin extends Plugin implements NpcClickExtension, ObjectClickExtension, ItemOnNpcExtension {

	private static final int NPC_ID = 13651;
	
	private static final int REWARD = 24144;
	
	private static final int YELLOW_EGG = 7933, GREEN_EGG = 7930, BLUE_EGG = 7928;
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, NPC_ID);
		map(ObjectClickExtension.class, 23117);
		map(ItemOnNpcExtension.class, NPC_ID);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		
		player.getDialogueBuilder().reset();
		
		if (player.claimedEventReward) {
			player.getDialogueBuilder()
			.sendNpcChat(NPC_ID, "Thank you for all your help!")
			.execute();
			return;
		}
		
		if (player.getItems().playerHasItem(YELLOW_EGG, 3) && player.getItems().playerHasItem(GREEN_EGG, 3) && player.getItems().playerHasItem(BLUE_EGG, 3)) {
			player.getDialogueBuilder().reset();
			if (player.getItems().freeSlots() < 1) {
				player.getDialogueBuilder().sendNpcChat(NPC_ID, "You need at least one free inventory slot.").execute();
				return;
			}
			player.getItems().deleteItem2(YELLOW_EGG, 3);
			player.getItems().deleteItem2(GREEN_EGG, 3);
			player.getItems().deleteItem2(BLUE_EGG, 3);
			player.getDialogueBuilder()
			.sendNpcChat(NPC_ID, "You have brought me the eggs!", "Here is something for your trouble.")
			.sendPlayerChat(Expression.HAPPY_DRUNK, "Thank you! I will cherish it with my life!")
			.sendPlayerChat(Expression.CALM_TALK, "Or... I can give it back to you for a different reward?")
			.onReset(()-> {
				player.getItems().addItem(REWARD, 1);
				player.claimedEventReward = true;
				player.setDoubleExpLength(player.getDoubleExpLength() + 6000);
				player.sendMessage("You have received 1 hour of double exp and a great looking hat.");
			})
			.executeIfNotActive();
			return;
		}
		
		player.getDialogueBuilder().sendNpcChat(NPC_ID,
				"I need your help! The monsters have attacked",
				"The island and took our eggs!")
		.sendNpcChat("Could you please retrieve them for us?")
		.sendOption("I'll help you out!", ()-> {
		}, "I'm too busy...", ()-> {
			player.getDialogueBuilder().returnToCheckpoint(3);
		})
		.sendPlayerChat(Expression.HAPPY_TALK, "I'll help you out!", "What would you like me to do?")
		.sendNpcChat(NPC_ID, "Kill the monsters of SoulPlay and get back our eggs.", "I am in need of 3 yellow eggs, 3 blue eggs,", "and 3 green eggs.")
		.sendPlayerChat(Expression.HAPPY_TALK, "Can't wait to get them back!")
		.sendAction(()-> {
			player.getDialogueBuilder().reset();
		})
		.createCheckpointInstant(3)
		.sendPlayerChat(Expression.MEAN_FACE, "I'm too busy...")
		.executeIfNotActive();
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		Direction dir1 = Direction.getLogicalDirection(player.getCurrentLocation(), object.getLocation());
		ForceMovementMask mask1 = ForceMovementMask.createMask(dir1.getStepX(), dir1.getStepY(), 1, 0, dir1);
		
		player.lockActions(10);
		player.lockMovement(10);
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int cycle = 0;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (cycle > 20) {
					container.stop();
					return;
				}
				cycle++;
				
				switch (cycle) {
				case 1:
					player.startAnimation(AgilityAnimations.FAIL_WALL_CLIMB);
					player.setForceMovement(mask1);
					player.getPacketSender().setBlackout(2 + 0x80);
					break;
					
				case 3:
					player.getPA().movePlayer(HOLE_LOC);
					
					player.startAnimation(Animation.RESET);
					player.hidePlayer = true;
					
					break;
					
				case 4:
					player.hidePlayer = false;
					player.faceLocation(player.getX()+1, player.getY()+1);
					ForceMovementMask mask2 = ForceMovementMask.createMask(1, 1, 1, 0, Direction.NORTH_EAST).setCustomSpeed(90, 0);
					
					player.setForceMovementRaw(mask2);
					player.startAnimation(AgilityAnimations.CLIMB_FROM_UNDDERGROUND);
					
					break;
				case 5:
					player.getPacketSender().setBlackout(0);
					break;
					
				case 8:
					player.getPA().movePlayer(ISLAND_LOC);
					container.stop();
					break;
					
				}
				
			}
		}, 1);
		
	}
	
	private static final Location HOLE_LOC = Location.create(8676, 2790);
	
	private static final Location ISLAND_LOC = Location.create(8677, 2791);
	
	
	public static void dropRandomItem(Player p, int x, int y, int z, int hp) {
		if (p.inMinigame())
			return;
		
		int chance = Misc.interpolateSafe(10, 2, 5, 1000, hp);

		if (Misc.randomBoolean(chance)) {
			int eggId = Misc.random(EGGS);
			
			ItemHandler.createGroundItem(p.getName(),
					eggId, x, y,
					z, 1, true, p.isIronMan());
		}
		
	}
	
	private static final int[] EGGS = { YELLOW_EGG, YELLOW_EGG, YELLOW_EGG, YELLOW_EGG, YELLOW_EGG, GREEN_EGG, GREEN_EGG, GREEN_EGG, GREEN_EGG, GREEN_EGG, BLUE_EGG, BLUE_EGG, BLUE_EGG, BLUE_EGG, BLUE_EGG };

	@Override
	public void onItemOnNpc(Player player, NPC npc, int itemId, int slot) {
		switch (itemId) {
		case REWARD:
			if (player.getItems().deleteItemInOneSlot(itemId, slot, 1)) {
				player.getDialogueBuilder()
				.sendNpcChat(NPC_ID, Expression.HAPPY_TALK, "Here is your new reward!")
				.onReset(()-> {
					for (int i = 0; i < BUNNY_SUIT.length; i++)
						player.getItems().addOrDrop(BUNNY_SUIT[i]);
				}).execute();
			}
			break;
			
		default:
			player.getDialogueBuilder().sendNpcChat(NPC_ID, Expression.ANGRY, "I don't want that!").execute();
			break;
		}
		
	}
	
	private static final GameItem[] BUNNY_SUIT = { new GameItem(30116, 1), new GameItem(30117, 1), new GameItem(30118, 1), new GameItem(30119, 1), new GameItem(9975, 1) };
	
}
