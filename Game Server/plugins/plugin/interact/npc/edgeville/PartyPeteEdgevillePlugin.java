package plugin.interact.npc.edgeville;

import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class PartyPeteEdgevillePlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 12401);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		SeasonPassManager.openInterface(player);
	}
}
