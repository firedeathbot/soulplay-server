package plugin.interact.npc.edgeville;

import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.player.untradeables.UntradeableManager;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class GrimReaperPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 6390);
	}

	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder().sendOption("Untradeables recovery", () -> {
			UntradeableManager.open((Client) player);
			player.sendMessage("You currently have " + player.getUntradeableGoldToDrop() + " untradeable gold.");
		}, "Item retrieval", () -> {
			ItemRetrievalManager.openInterface(player);
		}).execute(false);
	}

}
