package plugin.interact.npc;

import com.soulplay.Config;
import com.soulplay.content.items.impl.DuellistsCap;
import com.soulplay.content.items.presets.PresetsInterface;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.Client;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class BankerPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 953); // Banker
		map(NpcClickExtension.class, 2574); // Banker
		map(NpcClickExtension.class, 166); // Gnome Banker
		map(NpcClickExtension.class, 1702); // Ghost Banker
		map(NpcClickExtension.class, 958); // fadli banker
		map(NpcClickExtension.class, 494); // Banker
		map(NpcClickExtension.class, 495); // Banker
		map(NpcClickExtension.class, 496); // Banker
		map(NpcClickExtension.class, 497); // Banker
		map(NpcClickExtension.class, 498); // Banker
		map(NpcClickExtension.class, 499); // Banker
		map(NpcClickExtension.class, 567); // Banker
		map(NpcClickExtension.class, 1036); // Banker
		map(NpcClickExtension.class, 1360); // Banker
		map(NpcClickExtension.class, 2163); // Banker
		map(NpcClickExtension.class, 2164); // Banker
		map(NpcClickExtension.class, 2354); // Banker
		map(NpcClickExtension.class, 2355); // Banker
		map(NpcClickExtension.class, 2568); // Banker
		map(NpcClickExtension.class, 2569); // Banker
		map(NpcClickExtension.class, 2570); // Banker
		map(NpcClickExtension.class, 9710); //dungeoneering banker
		map(NpcClickExtension.class, 13455);
	}
	
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder().sendNpcChat(npc.npcType, "Good day. How may I help you?");
		if (npc.npcType == 958) {
			player.getDialogueBuilder().sendOption("I'd like to access my bank account, please.", () -> {
				player.getPA().openUpBank();
			}, "I'd like to check my PIN settings.", () -> {
				Client c = (Client) player;
				c.getBankPin().bankPinSettings();
				player.openBankPinInterface = true;
			}, "What is this place?", () -> {
				player.getDialogueBuilder().sendPlayerChat("What is this place?").sendNpcChat(npc.npcType,
						"This is the bank of " + Config.SERVER_NAME + ".",
						"We have many branches in many towns.").setClose(true);
			}, "Give me a dueling hat.", () -> {
				if (player.getItems().addItem(DuellistsCap.TIER_1.id, 1)) {
					player.sendMessage("Fadli gives you a Duelist's Cap");
				} else {
					player.sendMessage("You do not have enough inventory space for this.");
				}
				player.getPA().closeAllWindows();
			}).execute(false);
		} else {
			player.getDialogueBuilder().sendOption("I'd like to access my bank account, please.", () -> {
				player.getPA().openUpBank();
			}, "I'd like to check my PIN settings.", () -> {
				Client c = (Client) player;
				c.getBankPin().bankPinSettings();
				player.openBankPinInterface = true;
			}, "What is this place?", () -> {
				player.getDialogueBuilder().sendPlayerChat("What is this place?").sendNpcChat(npc.npcType,
						"This is the bank of " + Config.SERVER_NAME + ".",
						"We have many branches in many towns.").setClose(true);
			}).execute(false);
		}
	}
	
	@Override
	public void onNpcOption2(Player player, NPC npc) {
		//open bank
		player.getPA().openUpBank();
	}
	
	@Override
	public void onNpcOption3(Player player, NPC npc) {
		//collect
		player.getPA().collectMoney(true);
	}
	@Override
	public void onNpcOption4(Player player, NPC npc) {
		PresetsInterface.open(player);
	}
	
}
