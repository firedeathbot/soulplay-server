package plugin.interact.npc.halloween;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class HalloweenGrimPlugin extends Plugin implements NpcClickExtension{
	
    private static final int GRIM = 12379;
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, GRIM);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder().sendNpcChat("Hello, i need your help to get something back for me.")
		.sendPlayerChat(Expression.LISTENING, "What do i need to get back for you and how?")
		.sendNpcChat("Some ghosts have stolen my hunting knife", "and my banshee outfit")
		.sendPlayerChat(Expression.LISTENING, "Where can i find these ghosts?")
		.sendNpcChat("You can find one around the cities, one in the Wilderness,", "one around minigame lobbies and", "one in dungeons somewhere.")
		.sendOption("I have found the knife and the banshee outfit.",  () -> {
			if (player.getItems().playerHasItem(30325) && 
					player.getItems().playerHasItem(30326) && 
					player.getItems().playerHasItem(30323) && 
					player.getItems().playerHasItem(30324)) {
				    player.getDialogueBuilder().sendNpcChat("Thank you very much, since you helped me get it back", "you can just keep it!");
				    //player.sendMessage("Thank you very much, since you helped me get it back you can just keep it!");
				} else {
					 player.getDialogueBuilder().sendNpcChat("You have not found all the pieces yet.");	
					// player.sendMessage("You have not found all the pieces yet.");
				}
        }, "I will start looking for your knife and your outfit.",  () -> {
        }).execute();
		
	}

}
