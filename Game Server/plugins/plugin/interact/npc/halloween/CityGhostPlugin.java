package plugin.interact.npc.halloween;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class CityGhostPlugin extends Plugin implements NpcClickExtension{
	
    private static final int CITY_GHOST = 8713;
	
	@Override
	public void onInit() {
		map(NpcClickExtension.class, CITY_GHOST);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder().sendNpcChat("What do you want?")
		.sendPlayerChat(Expression.ANGRY, "I am here to get back what you have stolen from Grim.")
		.sendNpcChat("I am sorry, here you go.").sendAction(()-> {
			player.getItems().addItem(30324, 1);
		}).sendItemStatement(30324, "The ghost hands you a banshee top.")
		.sendPlayerChat("You made the right decision.").execute();
		
	}

}
