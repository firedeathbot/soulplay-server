package plugin.interact.npc.taibwowannai;

import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class TiadechePlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 1164);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		player.getDialogueBuilder()
		.sendNpcChat("How may I help you?")
		.sendOption("I would like to see the shop.", ()-> {
			player.getDialogueBuilder().setClose(false);
			openShop(player);
		}, "Nothing.", ()-> {
			
		}).execute();
	}
	
	@Override
	public void onNpcOption2(Player player, NPC npc) {
		openShop(player);
	}
	
	private static void openShop(Player p) {
		((Client) p).getShops().openShop(47);
	}
}
