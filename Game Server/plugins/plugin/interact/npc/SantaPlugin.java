package plugin.interact.npc;

import com.soulplay.content.player.holiday.christmas.PresentSpawn;
import com.soulplay.content.player.holiday.christmas.PresentSpawnData;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class SantaPlugin extends Plugin implements NpcClickExtension {
	   private static final int SANTA = 8540;
	   private static final int PRESENT = 15420;
	   private static final int REWARD = 10507;


	    @Override
	    public void onInit() {
	        map(NpcClickExtension.class, SANTA);
	    }

	    @Override
	    public void onNpcOption1(Player player, NPC npc) {
	    	if (player.getItems().playerHasItem(PRESENT, 10)) {
	    		player.getDialogueBuilder().sendNpcChat(SANTA, "Ho Ho Ho, " + player.getNameSmart() + "! I see you have found", "10 presents, Thank you and merry christmas,", "here you go!")
	    		.sendItemStatement(REWARD, "Santa hands you a Reindeer hat")
	    		.sendAction(() -> {
	    			rewardPlayer(player);
	    		}).execute();
	    	} else if (player.getItems().playerHasItem(PRESENT)) {
	    		player.getDialogueBuilder().sendNpcChat(SANTA, "Ho Ho Ho, " + player.getNameSmart() + "! I see you have found", "some presents.")
	    		.sendOption("Yeah, but I'm not here for that", () -> {
	    			mainDialogue(player);
	    		}, "Yes, I am having hard time finding more", () -> {
	    			player.getDialogueBuilder().sendPlayerChat("Yes, I am having hard time finding more,", "could you tell me where i can find more?")
		    		.sendNpcChat("The presents are currently located in "+ PresentSpawnData.values[PresentSpawn.rotation].getDesc() +".", "Good luck!");
	    		}).execute();
	    	} else {
	    		mainDialogue(player);
	    	}
	    }
	    
	    private void mainDialogue(Player player) {
	    	 player.getDialogueBuilder().sendNpcChat(SANTA, "Ho Ho Ho, " + player.getNameSmart() + "!")
             .sendPlayerChat("Whoa, you're a large fellow.")
             .sendStatement("Santa gives you a face of disapproval.")             
             .sendNpcChat(SANTA, "Now that you've gotten that -OBVIOUS- fact out of the", "way, Ill tell you why I'm here visitting Edgeville.",
                     "I'm bringing gifts and fortune to the citizens of Soulplay.", "... even to those who insult me.")
             .sendPlayerChat("Huh. Maybe you're not so fat after all.")             
             .sendOption("Where can i find presents?", () -> {
             	player.getDialogueBuilder().sendPlayerChat("Where can i find presents?")
             	.sendNpcChat(SANTA, "There are currently a few presents in "+ PresentSpawnData.values[PresentSpawn.rotation].getDesc(), "If you can bring me 10 presents i will give you a reward!")
             	.sendPlayerChat("Are they only found in "+ PresentSpawnData.values[PresentSpawn.rotation].getDesc() + "?")
             	.sendNpcChat(SANTA, "Ho Ho Ho.. Of course not", "Once all the presents from "+ PresentSpawnData.values[PresentSpawn.rotation].getDesc() +" have been found", "new presents will appear somewhere else.")
             	.sendPlayerChat("Thanks, i will be back once i have collected", "10 presents for you!")
             	.sendAction(()->{
     				player.getDialogueBuilder().jumpToStage(4);
     			});	                
             }, "How do I obtain the Santa outfit/Christmas points?", () -> {
             	player.getDialogueBuilder().sendPlayerChat("How do I obtain your outfit and Christmas points?")
             	.sendNpcChat(SANTA, "My personal outfit is far too large for you, as you can", "see. However, I do have some spares handy.")
             	.sendNpcChat(SANTA, "I suppose I could hand one to you in exchange for favours.", "I'm looking for someone who is willing to spend", "time pelting the players of Soulplay with snowballs", "and make snowman.")
             	.sendNpcChat(SANTA, "All you have to do is spread the holiday spirit by tossing", "compact balls of snow at your peers", "and making snowman")
             	.sendPlayerChat("So... all I have to do is grab some snow from a nearby", "pile and hurt the person next to me with a snowball?", "What a strange way to spread the holiday spirit.")
             	.sendNpcChat(SANTA, "That is correct.", "Points will be automatically given to you after you", "hit someone, so get to work!")
     			.sendAction(()->{
     				player.getDialogueBuilder().jumpToStage(4);
     			});
             }, "Open Christmas point exchange.", () -> {
             	player.getShops().openShop(76);
             	player.sendMessage("You have " + player.getChristmasPoints() + " Christmas points to spend.");
             }).execute();
	    }
	    
	    private void rewardPlayer(Player player) {
	    	player.getItems().deleteItem2(PRESENT, 10);
	    	player.getItems().addItem(REWARD, 1);
	    	
	    }
}
