package plugin.interact.npc;

import com.soulplay.content.minigames.FPLottery;
import com.soulplay.content.player.titles.TitleData;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;
import com.soulplay.util.Misc;

public class GamblerNpcPlugin extends Plugin implements NpcClickExtension {

    @Override
    public void onInit() {
        map(NpcClickExtension.class, 2998);
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        player.getDialogueBuilder().sendNpcChat(2998, "Hi there, i manage the Lottery, the lottery pot ends",
                "at 500,000,000 (500m), one player can enter the", "Lottery up to 5 times each pot.",
                "Participation costs 10,000,000 (10m), would you like to try?")
                .createCheckpoint(0)
                .sendOption("Yes, i would like to participate.", () -> {
                    enterLottery(player, npc);
                }, "How much is currently in the lottery?", () -> {
                    player.getDialogueBuilder().sendNpcChat(2998, String.format("There is currently %s GP in the lottery.", Misc.formatNumbersWithCommas(FPLottery.lotteryFund)));
                },   "No Thanks.", () -> {
                    player.getDialogueBuilder().sendNpcChat(2998, "Good bye!");
                }).execute();
    }

    private static void enterLottery(Player player, NPC npc) {
        if (player.isIronMan()) {
            player.sendMessage("You cannot enter the lottery in Iron Man mode.");
            return;
        }

        if (checkEntriesCount(player) < FPLottery.maximumEntryTimes) {
            if (player.getItems().playerHasItem(995, FPLottery.entryPrice * 1_000_000)) {
                FPLottery.lotteryPlayerNames.add(player.getName());
                FPLottery.lotteryFund += ((FPLottery.entryPrice * 1000000) * .75);
                player.getItems().deleteItem2(995, FPLottery.entryPrice * 1000000);
                player.getAchievement().enterLottery();
                onEnterLottery(player);
                player.getDialogueBuilder().sendNpcChat(2998, "You have been entered into the lottery!", "Good luck!");
            } else {
                player.getDialogueBuilder().sendNpcChat(2998, String.format("Come back when you have at least %s coins.", Misc.formatNumbersWithCommas((FPLottery.entryPrice * 1_000_000))));
            }
        } else {
            player.getDialogueBuilder().sendNpcChat(2998, "You have already entered 5 times!");
        }
    }

    private static int checkEntriesCount(Player player) {
        int entries = 0;
        for (int indexes = 0; indexes < FPLottery.lotteryPlayerNames.size(); indexes++) {
            if (FPLottery.lotteryPlayerNames.get(indexes)
                    .equalsIgnoreCase("" + player.getName())) {
                entries += 1;
            }
        }
        return entries;
    }

    private static void onEnterLottery(Player player) {
        if (player.getTitleInt(
                TitleData.THE_FAITHFUL.ordinal()) < TitleData.THE_FAITHFUL
                .getUnlocked()) {
            player.setTitleInt(TitleData.THE_FAITHFUL.ordinal(),
                    player.getTitleInt(TitleData.THE_FAITHFUL.ordinal()) + 1);
            if (player.getTitleInt(
                    TitleData.THE_FAITHFUL.ordinal()) == TitleData.THE_FAITHFUL
                    .getUnlocked()) {
                player.getPacketSender().sendFrame126("<col=ff00>The Faithful",
                        +TitleData.THE_FAITHFUL.stringId());
                player.sendMessage("<img=6>Congratulation you've unlocked "
                        + TitleData.THE_FAITHFUL.getName() + " Title ");
            }
        }
    }

}
