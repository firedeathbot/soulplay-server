package plugin.interact.itemon.item;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class MakeClayPlugin extends Plugin implements ItemOnItemExtension {

    private static final int BUCKET_OF_WATER = 1929;
    private static final int EMPTY_BUCKET = 1925;
    private static final int CLAY = 434;
    private static final int SOFT_CLAY = 1761;

    @Override
    public void onInit() {
        map(ItemOnItemExtension.class, BUCKET_OF_WATER, CLAY);
        map(ItemOnItemExtension.class,  CLAY, BUCKET_OF_WATER);
    }

    @Override
    public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
        if (player.getItems().playerHasItem(BUCKET_OF_WATER) && player.getItems().playerHasItem(CLAY)) {
            player.getItems().deleteItem2(BUCKET_OF_WATER, 1);
            player.getItems().deleteItem2(CLAY, 1);
            player.getItems().addItem(EMPTY_BUCKET, 1);
            player.getItems().addItem(SOFT_CLAY, 1);
        }

        player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

            @Override
            public void execute(CycleEventContainer container) {
                if (player.getItems().playerHasItem(BUCKET_OF_WATER) && player.getItems().playerHasItem(CLAY)) {
                    player.getItems().deleteItem2(BUCKET_OF_WATER, 1);
                    player.getItems().deleteItem2(CLAY, 1);
                    player.getItems().addItem(EMPTY_BUCKET, 1);
                    player.getItems().addItem(SOFT_CLAY, 1);
                } else {
                    container.stop();
                }
            }

            @Override
            public void stop() {

            }
        }, 2);
    }

}
