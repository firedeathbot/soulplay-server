package plugin.interact.itemon.item;

import com.soulplay.content.player.skills.herblore.PestleAndMortarHerblore;
import com.soulplay.content.player.skills.herblore.PestleEnum;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class PestleAndMortarPlugin extends Plugin implements ItemOnItemExtension {
	
	private static final int PESTLE_AND_MORTAR = 233;
	
	@Override
	public void onInit() {
		PestleEnum.init();
		for (PestleEnum p : PestleEnum.values()) {
			map(ItemOnItemExtension.class, PESTLE_AND_MORTAR, p.getIngredient());
			map(ItemOnItemExtension.class, p.getIngredient(), PESTLE_AND_MORTAR);
		}
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		final Item otherItem = used.getId() == PESTLE_AND_MORTAR ? with : used;
		
		PestleEnum pestle = PestleEnum.get(otherItem.getId());
		
		if (pestle == null)
			return;
		
		PestleAndMortarHerblore.grindIngredient(player, otherItem, pestle);
		
	}

}
