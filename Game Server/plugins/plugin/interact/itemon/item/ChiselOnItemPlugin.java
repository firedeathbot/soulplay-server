package plugin.interact.itemon.item;

import com.soulplay.content.items.MakeInterface;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class ChiselOnItemPlugin extends Plugin implements ItemOnItemExtension {

    private static final int CRAFTING_ANIM = 2717;
    private static final int CHISEL = 1755;

    @Override
    public void onInit() {
        for (CraftItems item : CraftItems.values) {
            map(ItemOnItemExtension.class, CHISEL, item.itemId);
            map(ItemOnItemExtension.class, item.itemId, CHISEL);
        }
    }

    @Override
    public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
        final int product = used.getId() == CHISEL ? with.getId() : used.getId();

        final CraftItems craft = CraftItems.lookup(product);

        if (craft == null) {
            return;
        }

        MakeInterface.make(player, craft.productId, () -> startCrafting(player, craft));
    }

    private static void startCrafting(Player player, CraftItems craft) {
        player.getDialogueBuilder().close();

        final int amount = player.getItems().getItemAmount(craft.itemId);

        if (player.makeCount > amount) {
            player.makeCount = amount;
        }

        if (player.getPlayerLevel()[Skills.CRAFTING] < craft.craftLevelReq) {
            player.sendMessage("You need higher crafting level to craft this item.");
            return;
        }

        if (player.getPlayerLevel()[Skills.FLETCHING] < craft.fletchLevelReq) {
            player.sendMessage("You need higher fletching level to craft this item.");
            return;
        }

        makeItem(player, craft);

        if (player.makeCount <= 0) {
            return;
        }

        player.skillingEvent = CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

            @Override
            public void execute(CycleEventContainer container) {
                if (player.makeCount <= 0) {
                    container.stop();
                } else {
                    makeItem(player, craft);
                }
            }

            @Override
            public void stop() {
                player.resetAnimations();
            }

        }, 4);
    }

    private static void makeItem(Player player, CraftItems craft) {
        player.getItems().deleteItemInOneSlot(craft.itemId, 1);
        player.getItems().addItem(craft.productId, 1);
        player.getPA().addSkillXP(craft.fletchExp, Skills.FLETCHING);
        player.getPA().addSkillXP(craft.craftExp, Skills.CRAFTING);

        player.startAnimation(CRAFTING_ANIM);
        player.makeCount--;
    }

    private enum CraftItems{

        LIMESTONE_BRICK(3211, 3420, 12, 6, 0, 0);

        public static final CraftItems[] values = values();

        private final int itemId;
        private final int productId;
        private final int craftLevelReq;
        private final int craftExp;
        private final int fletchLevelReq;
        private final int fletchExp;

        private static CraftItems lookup(int itemID) {
            for (CraftItems item : values) {
                if (item.itemId == itemID) {
                    return item;
                }
            }
            return null;
        }

        CraftItems(int itemId, int productId, int craftLevelReq, int craftExp, int fletchLevelReq, int fletchExp) {
            this.itemId = itemId;
            this.productId = productId;
            this.craftLevelReq = craftLevelReq;
            this.craftExp = craftExp;
            this.fletchLevelReq = fletchLevelReq;
            this.fletchExp = fletchExp;
        }
    }

}
