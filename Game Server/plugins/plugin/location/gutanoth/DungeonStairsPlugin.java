package plugin.location.gutanoth;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class DungeonStairsPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		 map(ObjectClickExtension.class, 6841);
		 map(ObjectClickExtension.class, 6842);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		switch (object.getId()) {
		
		case 6841:
			player.getPA().movePlayerDelayed(2442, 9418, 0, 0, 1);
			break;
			
		case 6842:
			player.getPA().movePlayerDelayed(2485, 3045, 0, 0, 1);
			break;
		
		}
		
	}
	
}
