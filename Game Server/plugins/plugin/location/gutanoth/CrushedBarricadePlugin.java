package plugin.location.gutanoth;

import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class CrushedBarricadePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		 map(ObjectClickExtension.class, 6878);
		 map(ObjectClickExtension.class, 6879);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), object.getLocation());
		
		ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, dir).addSecondDirection(dir.getStepX()*2, dir.getStepY()*2, 2, 0);
		
		player.setForceMovement(mask);
		player.startAnimation(AgilityAnimations.CRAWL_OVER_FENCE_DELAYED_1_TICK);
		
	}
	
}
