package plugin.location.gutanoth;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class GapPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		 map(ObjectClickExtension.class, 2830);
		 map(ObjectClickExtension.class, 2831);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		
		switch (object.getId()) {
		
		case 2830:
			player.faceLocation(2531, 3029);
			player.getPA().movePlayerDelayed(2531, 3029, 0, 807, 1);
			break;
		
		case 2831:
			player.faceLocation(2531, 3025);
			player.getPA().movePlayerDelayed(2531, 3025, 0, 807, 1);
			break;
		
		}
		
	}
	
}
