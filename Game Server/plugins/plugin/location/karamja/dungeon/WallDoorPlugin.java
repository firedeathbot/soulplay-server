package plugin.location.karamja.dungeon;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class WallDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 2606);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (player.getY() < object.getY())
			player.getPA().movePlayer(object.getX(), object.getY(), object.getZ());
		else
			player.getPA().movePlayer(player.getX(), player.getY()-1, player.getZ());
	}
}
