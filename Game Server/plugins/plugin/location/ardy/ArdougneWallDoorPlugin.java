package plugin.location.ardy;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class ArdougneWallDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		 map(ObjectClickExtension.class, 8738);
		 map(ObjectClickExtension.class, 8739);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		if (player.getX() >= 2558) {
			player.getPA().movePlayer(2556, player.getY(), player.getZ());
		} else {
			player.getPA().movePlayer(2559, player.getY(), player.getZ());
		}
		
	}
	
}
