package plugin.location.varrock;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.info.AnimationsData;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class ChampionGuildTrapDoorPlugin extends Plugin implements ObjectClickExtension {

	public void onInit() {
		map(ObjectClickExtension.class, 10558);
		map(ObjectClickExtension.class, 10559);
		map(ObjectClickExtension.class, 10560);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		switch (object.getId()) {
		case 10558: // closed trapdoor
			RSObject rso = new RSObject(10559, object.getX(), object.getY(), object.getZ(), object.getOrientation(), object.getType(), object.getId(), 100 + Misc.random(50));
			ObjectManager.addObject(rso);
			player.startAnimation(AnimationsData.PICK_UP_FROM_GROUND);
			break;
			
		case 10559: // opened trapdoor
			player.getPA().movePlayerDelayed(3189, 9758, 0, 827, 1);
			break;
		case 10560: // underground ladder
			player.getPA().movePlayerDelayed(3191, 3355, 0, 828, 1);
			break;
		}
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		Misc.faceObject(player, object);
		switch (object.getId()) {
		case 10558: // closed trapdoor
			break;
			
		case 10559: // opened trapdoor
			//close
			RSObject rso = ObjectManager.getObject(object.getX(), object.getY(), object.getZ());
			
			if (rso != null && rso.isActive()) {
				rso.setTick(0);
			}
			break;
			default:
				break;
		}
	}
	
}
