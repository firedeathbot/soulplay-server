package plugin.location.varrock;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class ChampionGuildDoorPlugin extends Plugin implements ObjectClickExtension {

	public void onInit() {
		map(ObjectClickExtension.class, 1805);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		Direction dir = player.getY() >= 3363 ? Direction.SOUTH : Direction.NORTH;
		
		player.faceLocation(player.getY() < 3363 ? object.getLocation() : Location.create(3191, 3363));
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer con) {
				if (player.getX() != 3191) {
					if (!player.getMovement().isMovingOrHasWalkQueue())
						player.getPA().playerWalk(3191, player.getY());
					return;
				}
				
				con.stop();
				
				player.lockActions(2);
				player.lockMovement(2);
				ForceMovementMask mask = ForceMovementMask.createMask(0, dir.getStepY(), 1, 0, dir).setCustomSpeed(30, 0).setNewOffset(player.getX(), player.getY());
				

				
				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
					int ticks = 0;
					@Override
					public void stop() {
					}
					
					@Override
					public void execute(CycleEventContainer container) {
						
						ticks++;
						if (ticks > 5) {
							container.stop();
							return;
						}
						
						
						if (ticks == 1) {
							player.getPacketSender().addObjectPacket(-1, object.getLocation(), object.getType(), object.getOrientation());
							player.getPacketSender().addObjectPacket(object.getId(), object.getX(), object.getY()-1, object.getType(), 0);
							player.startAnimation(player.getMovement().getWalkAnim());
							player.setForceMovementRaw(mask);
						} else if (ticks == 2) {
							container.stop();
							player.getPA().movePlayerInstant(player.getX(), player.getY()+dir.getStepY(), player.getZ());
							player.getPacketSender().addObjectPacket(-1, object.getX(), object.getY()-1, object.getType(), 0);
							player.getPacketSender().addObjectPacket(object.getId(), object.getLocation(), object.getType(), object.getOrientation());
						}
						
					}
				}, 1);
				
			}
		}, 1);
		
	}
	
}
