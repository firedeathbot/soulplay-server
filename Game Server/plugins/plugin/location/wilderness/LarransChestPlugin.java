package plugin.location.wilderness;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.items.clickitem.MysteryBoxLoot;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

import plugin.object.CrystalChestPlugin;

public class LarransChestPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension, ItemClickExtension {

	private static final int OBJ = 133016;
	public static final int KEY_ITEM = 30385;
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, OBJ);
		map(ItemOnObjectExtension.class, KEY_ITEM, OBJ);
		map(ItemClickExtension.class, KEY_ITEM);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		if (!player.getItems().playerHasItem(KEY_ITEM)) {
            player.sendMessage("The chest is locked!");
            return;
        }
		
		openChest(player, object.getLocation());
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		player.faceLocation(objectX, objectY);
		
		if (!player.getItems().playerHasItem(KEY_ITEM)) {
            player.sendMessage("The chest is locked!");
            return;
        }
		
		openChest(player, Location.create(objectX, objectY));
	}
	
	private static void openChest(Player player, Location oLoc) {

		player.startAnimation(CrystalChestPlugin.OPEN_ANIMATION);
		
		player.getItems().deleteItemInOneSlot(KEY_ITEM, 1);
		
		player.skullPlayer();
		
		if (oLoc.getX() == 3018) {
			MysteryBoxLoot.hardChest(player);
		} else if (oLoc.getX() == 3280) {
			MysteryBoxLoot.easyChest(player);
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.isInCombatDelay()) {
			player.sendMessage("You cannot destroy the keys during combat.");
			return;
		}
		
		FifthItemAction.DESTROY.init(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot));
	}
	
}
