package plugin.location.wilderness;

import com.soulplay.content.minigames.lastmanstanding.LmsArenaObjects;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class MuddyChestPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension {

	private static final int KEY = 991;
	
	private static final int OBJECT_ID = 100170;
	
	public void onInit() {
		map(ObjectClickExtension.class, OBJECT_ID);
		map(ItemOnObjectExtension.class, KEY, OBJECT_ID);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		openChest(player, object);
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		GameObject o = RegionClip.getGameObject(objectID, objectX, objectY, 0);
		if (o != null)
			openChest(player, o);
	}
	
	private static void openChest(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		if (!player.getItems().playerHasItem(KEY)) {
			player.sendMessage("This chest is locked.");
			return;
		}
		
		if (player.getItems().deleteItem2(KEY, 1)) {
			player.startAnimation(LmsArenaObjects.searchChest);
			
			RSObject rsO = new RSObject(100171, object.getX(), object.getY(), object.getZ(), object.getOrientation(), object.getType(), object.getId(), 2);
			
			ObjectManager.addObject(rsO);
			
			if (Misc.randomBoolean(1000)) {
				player.getItems().addOrDrop(Misc.random(RARES));
				PlayerHandler.broadcastMessageLocalWorld(String.format(RARE_LOOT_MSG, player.getNameSmartUp()));
			}
			
			if (Misc.randomBoolean(7))
				player.getItems().addOrDrop(new GameItem(Misc.random(PlayerConstants.pvpGear), 1));
			
			player.getItems().addOrDrop(new GameItem(Misc.random(PlayerConstants.pvpGear), 1));
			
			player.getItems().addOrDrop(new GameItem(299, Misc.random(10, 30)));// random mith seeds drop from muddy chest
			
			for (int i = 0; i < LOOT.length; i++)
				player.getItems().addOrDrop(LOOT[i]);
		}
	}
	
	private static final String RARE_LOOT_MSG = "%s has obtained Dwarven Loot from the muddy chest!";
	
	private static GameItem[] LOOT = { new GameItem(1619, 1), new GameItem(2359, 1), new GameItem(1209, 1), new GameItem(2297, 1), new GameItem(563, 10), new GameItem(560, 10), new GameItem(562, 10), new GameItem(565, 100) };
			
	private static GameItem[] RARES = { new GameItem(11200, 1), new GameItem(21340, 1) };
}
