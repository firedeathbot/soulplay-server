package plugin.location.wilderness;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class LockpickDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 111726); //317map ID is 2557
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		
		// inside already
		if (object.getX() == 3191 && object.getY() == 3963) {
			if (player.absY == 3962) {
				player.getPA().walkTo(0, 1, true);
				return;
			}
		} else if (object.getX() == 3190 && object.getY() == 3957) {
			if (player.absY == 3958) {
				player.getPA().walkTo(0, -1, true);
				return;
			}
		}
		
		player.sendMessage("The door is locked.");
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		if (!player.lockPickTimer.complete()
				|| player.getFreezeTimer() > 0) {
			return;
		}
		player.lockPickTimer.startTimer(2);
		if (player.getItems().playerHasItem(1523, 1)) {

			if (Misc.random(10) <= 3) {
				player.sendMessage("You fail to pick the lock.");
				return;
			}
			if (object.getX() == 3191 && object.getY() == 3963) {
				if (player.absY == 3963) {
					player.getPA().walkTo(0, -1, true);
				} else if (player.absY == 3962) {
					player.getPA().walkTo(0, 1, true);
				}
			} else if (object.getX() == 3190 && object.getY() == 3957) {
				if (player.absY == 3957) {
					player.getPA().walkTo(0, 1, true);
				} else if (player.absY == 3958) {
					player.getPA().walkTo(0, -1, true);
				}
			}
		} else {
			player.sendMessage("I need a lockpick to pick this lock.");
		}
	}
}
