package plugin.location.wilderness;

import com.soulplay.Server;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.map.travel.zone.addon.WildGodWarsDungeonZone;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class WildGodWarsCavePlugin extends Plugin implements ObjectClickExtension {
	
	private static final int CREVICE_ENTRANCE = 26767;
	private static final int CREVICE_EXIT = 26769;
	private static final int CAVE_ENTRANCE = 126766;
	
	private static final Animation CRAWL = new Animation(844);
	private static final Animation CRAWL_DELAY = new Animation(844, 20);
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, CREVICE_ENTRANCE);
		map(ObjectClickExtension.class, CREVICE_EXIT);
		map(ObjectClickExtension.class, CAVE_ENTRANCE);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		switch (object.getId()) {
		case CREVICE_ENTRANCE:
			if (object.getX() == 9465 && object.getY() == 10160) { // exit to outside
				player.startAnimation(player.getMovement().isRunning() ? CRAWL_DELAY : CRAWL);
				player.getPA().movePlayerDelayed(3017, 3740, 0, 0, 1);
			} else if (object.getX() == 9466 && object.getY() == 10142) { // south entrance
				applyMulti(player, true);
				player.startAnimation(player.getMovement().isRunning() ? CRAWL_DELAY : CRAWL);
				player.getPA().movePlayerDelayed(9462, 10130, 0, 0, 1);
			} else if (object.getX() == 9449 && object.getY() == 10165) { // west entrance
				applyMulti(player, true);
				player.startAnimation(player.getMovement().isRunning() ? CRAWL_DELAY : CRAWL);
				player.getPA().movePlayerDelayed(9434, 10158, 0, 0, 1);
			}
			break;
			
		case CREVICE_EXIT:
			if (object.getY() == 10158) { // northern exit
				player.startAnimation(player.getMovement().isRunning() ? CRAWL_DELAY : CRAWL);
				player.getPA().movePlayerDelayed(9450, 10165, 3, 0, 1);
				applyMulti(player, false);
			} else if (object.getX() == 9462) { //southern exit
				player.startAnimation(player.getMovement().isRunning() ? CRAWL_DELAY : CRAWL);
				player.getPA().movePlayerDelayed(9466, 10143, 3, 0, 1);
				applyMulti(player, false);
			}
			break;
			
		case CAVE_ENTRANCE:
			//enter main cave x: 9465 y: 10159 z: 3
			player.startAnimation(player.getMovement().isRunning() ? CRAWL_DELAY : CRAWL);
			player.getPA().movePlayerDelayed(9465, 10159, 3, 0, 1);
			Server.getTaskScheduler().schedule(1, ()-> {
				if (player.isActive)
					player.getZones().addDynamicZone(WildGodWarsDungeonZone.createDynamic());
			});
			break;
		}
	}
	
	private static void applyMulti(Player player, boolean isOn) {
		Server.getTaskScheduler().schedule(()-> {
			if (player.isActive) {
				player.inMultiDynZone = isOn;
			}
		});
	}
}
