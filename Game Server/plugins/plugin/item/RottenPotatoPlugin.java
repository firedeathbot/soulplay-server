package plugin.item;

import com.soulplay.Server;
import com.soulplay.content.player.commands.ModCommands;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.model.player.PunishPlayerHandler;
import com.soulplay.game.world.World;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.game.world.map.travel.zone.addon.VorkathZone;
import com.soulplay.game.world.map.travel.zone.addon.ZulrahZone;
import com.soulplay.net.loginserver.LoginServerConnection;
import com.soulplay.net.packet.in.CommandPacket;
import com.soulplay.net.slack.SlackMessageBuilder;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnPlayerExtension;

import java.util.Optional;
import java.util.function.Predicate;

public class RottenPotatoPlugin extends Plugin implements ItemClickExtension, ItemOnPlayerExtension {

    public static final int ROTTEN_POTATO = 5733;

    @Override
    public void onInit() {
        map(ItemClickExtension.class, ROTTEN_POTATO);
        map(ItemOnPlayerExtension.class, ROTTEN_POTATO);
    }

    @Override
    public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
        if (player.playerRights != 1 && player.playerRights != 2 && player.playerRights != 3) {
            return;
        }

        player.getDialogueBuilder().sendOption("Set all stats", () -> {
        	if (player.playerRights != 2 && player.playerRights != 3) {
        		player.sendMessage("Not available for mods.");
        		player.getDialogueBuilder().closeNew();
                return;
            }
            player.getDialogueBuilder().sendOption("To max", () -> {
                for (int i = 0; i < player.getPlayerLevel().length; i++) {
                    player.getSkills().setStaticLevel(i, Skills.maxLevels[i]);
                }
                player.calcCombat();
            },"To min", () -> {
                for (int i = 0; i < player.getPlayerLevel().length; i++) {
                    player.getSkills().setStaticLevel(i, i == Skills.HITPOINTS ? 10 : 1);
                }
                player.calcCombat();
            });
        }, "Wipe inventory", () -> {
        	if (player.playerRights != 2 && player.playerRights != 3) {
        		player.sendMessage("Just use ::empty");
        		player.getDialogueBuilder().closeNew();
                return;
            }
            for (int i = 0; i < player.playerItems.length; i++) {
                int id = player.playerItems[i];

                if (id - 1 == ROTTEN_POTATO) {
                    continue;
                }

                player.playerItems[i] = 0;
                player.playerItemsN[i] = 0;
            }

            player.setUpdateInvInterface(3214);
        }, "Teleport to player", () -> {
            player.getDialogueBuilder().sendEnterText(() -> {
                String input = player.getDialogueBuilder().getEnterText();

                Optional<Client> result = World.search(input);
                if (result.isPresent()) {
                	Client other = result.get();
                	if (!ModCommands.teleportRestrictions(player, other)) {
                		return;
                	}

                	player.getPA().movePlayer(other.getCurrentLocation());
                }
            });
        }, "Open bank", () -> {
        	if (player.playerRights != 2 && player.playerRights != 3) {
        		player.sendMessage("Not available for mods.");
        		player.getDialogueBuilder().closeNew();
                return;
            }
            player.getPA().openUpBank();
        }, String.format("%s", player.hidePlayer ? "Reveal self" : "Hide self"), () -> {
            player.hidePlayer = !player.hidePlayer;
            player.headIconPk = player.hidePlayer ? 3 : -1;
            player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
            player.sendMessage(player.hidePlayer ? "You hide yourself." : "You reveal yourself to everyone.");
        }).execute();
    }

    @Override
    public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
        if (player.playerRights != 1 && player.playerRights != 2 && player.playerRights != 3) {
            return;
        }
        
        player.getDialogueBuilder().reset();
        
        player.searchIndex = 0;

        player.getDialogueBuilder()
        .sendOption("Search Zulrah Instances", ()-> {
        	player.getDialogueBuilder().returnToCheckpoint(0);
        }, "Search Vorkath Instances", ()-> {
        	player.getDialogueBuilder().returnToCheckpoint(1);
        }, "Exit", ()-> {
        	player.getDialogueBuilder().reset();
        })
        //zulrah instance search
        .createCheckpointInstant(0)
        .sendOption("Next Zulrah Instance", ()-> {
        	Predicate<Player> containsLetterA = p -> p.getZones().getZoneList().contains(ZulrahZone.ZONE); 
        	searchPlayersForward(player, containsLetterA);
        	
        	player.getDialogueBuilder().returnToCheckpoint(0);
        }, "Previous Zulrah Instance", ()-> {
        	Predicate<Player> containsLetterA = p -> p.getZones().getZoneList().contains(ZulrahZone.ZONE);
        	searchPlayersBackward(player, containsLetterA);
        	
        	player.getDialogueBuilder().returnToCheckpoint(0);
        })
        //vorkath instance search
        .createCheckpointInstant(1)
        .sendOption("Next Vorkath Instance", ()-> {
        	Predicate<Player> containsLetterA = p -> p.getZones().getZoneList().contains(VorkathZone.ZONE); 
        	searchPlayersForward(player, containsLetterA);
        	
        	player.getDialogueBuilder().returnToCheckpoint(1);
        }, "Previous Vorkath Instance", ()-> {
        	Predicate<Player> containsLetterA = p -> p.getZones().getZoneList().contains(VorkathZone.ZONE); 
        	searchPlayersBackward(player, containsLetterA);
        	
        	player.getDialogueBuilder().returnToCheckpoint(1);
        })
        
        .execute(false);
    }

    @Override
    public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
        if (player.playerRights != 2 && player.playerRights != 3) {
            return;
        }

        player.getDialogueBuilder().sendOption("Set your run speed", "Default", ()-> {
        	player.runTilesPerTick = 1;
        }, "3 tiles", ()-> {
        	player.runTilesPerTick = 2;
        }, "7 tiles", ()-> {
        	player.runTilesPerTick = 6;
        }, "Custom", ()-> {
        	player.getDialogueBuilder().sendEnterAmount(()-> {
        		int amount = Math.min(9, Math.max(2, (int)player.enterAmount));
        		player.sendMessage("set run tiles per tick to :" + (amount+1));
        		player.runTilesPerTick = amount;
        	});
        }).execute();
    }

    @Override
    public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
        if (player.playerRights != 2 && player.playerRights != 3) {
            return;
        }

        player.sendMessage("No options yet");
    }

    @Override
    public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
        player.getItems().deleteItemInSlot(itemSlot);
    }

    @Override
    public void onItemOnPlayer(Player player, Player other, Item item) {
        if (player.playerRights != 1 && player.playerRights != 2 && player.playerRights != 3) {
            return;
        }

        player.getDialogueBuilder().createCheckpoint().sendOption("View", () -> {
            player.getDialogueBuilder().sendOption("View inventory", () -> {
                player.getItems().showInventory(other);
            }, "View bank", () -> {
                player.getItems().showBank(other);
            }, "View equipment", () -> {
                player.getItems().showEquipment(other);
            }, "Go back", () -> {
                player.getDialogueBuilder().returnToCheckpoint();
            });
        }, "Copy", () -> {

        	if (player.playerRights == 2 || player.playerRights == 3) {
           
        		player.getDialogueBuilder().sendOption("Copy inventory", () -> {
        			player.getItems().copyInventory(other);
        		}, "Copy equipment", () -> {
        			player.getItems().copyEquipment(other);
        		}, "Copy bank", () -> {
        			player.getItems().copyBank(other);
        		}, "Copy stats", () -> {
        			for (int i = 0; i < Skills.STAT_COUNT; i++) {
        				player.getSkills().setStaticLevel(i, other.getSkills().getStaticLevel(i));
        			}
        			player.calcCombat();
        		}, "Go back", () -> {

        		});
            
        	}

        }, (other.getFreezeTimer() > 0 ? "Unfreeze" : "Freeze"), () -> {
            other.setFreezeTimer(other.getFreezeTimer() > 0 ? 0 : 20);
            other.sendMessage(String.format("%s %s you.", player.getNameSmartUp(), other.getFreezeTimer() > 0 ? "froze" : "unfroze"));
        }, "Punish", () -> {
            player.getDialogueBuilder().sendOption("Kick", () -> {
                player.getDialogueBuilder().sendOption("Are you sure?","Yes", () -> {
                    LoginServerConnection.instance().submit(() -> {
                        LoginServerConnection.instance().punishPlayer(player.getName(), other.getName(), 254, 0, "kicking");
                        Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage((Client)player, other.getName(), "Kicked", ""));
                    });
                }, "No", () -> {

                });
            }, "Ban", () -> {
                player.getDialogueBuilder().sendOption("Are you sure?","Yes", () -> {

                    player.getDialogueBuilder().sendStatement("Enter a reason").sendEnterText(() -> {
                        final String reason = player.getDialogueBuilder().getEnterText();

                        if (reason.isEmpty()) {
                            player.sendMessage("You need to have a reason!");
                            return;
                        }

                        LoginServerConnection.instance().submit(() -> {
                            LoginServerConnection.instance().punishPlayer(player.getName(), other.getName(),
                                    CommandPacket.Punishment.BAN_NAME.getPunishmentType(), 0, reason);
                            Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage((Client)player, other.getName(),
                                    CommandPacket.Punishment.BAN_NAME.getDesc(), reason));
                        });
                        player.sendMessage("Sent ban request on playername " + other.getName());
                    });

                }, "No", () -> {

                });
            }, "Mute", () -> {
                player.getDialogueBuilder().sendOption("Are you sure?","Yes", () -> {
                    player.getDialogueBuilder().sendStatement("Enter a reason").sendEnterText(() -> {

                        final String reason = player.getDialogueBuilder().getEnterText();

                        if (reason.length() < 1) {
                            player.sendMessage("You must enter a reason for your mute.");
                            return;
                        }

                        LoginServerConnection.instance().submit(() -> {
                            LoginServerConnection.instance().punishPlayer(player.getName(), other.getName(),
                                    CommandPacket.Punishment.MUTE_NAME.getPunishmentType(), 0, reason);
                            Server.getSlackApi().call(SlackMessageBuilder.buildJudgementMesssage((Client)player, other.getName(),
                                    CommandPacket.Punishment.MUTE_NAME.getDesc(), reason));
                        });
                        player.sendMessage("Sent mute request on playername: " + other.getName());
                    });
                }, "No", () -> {

                });
            }, "Jail", () -> {

            	player.getDialogueBuilder().sendOption("Are you sure?","Yes", () -> {
                    player.getDialogueBuilder().sendStatement("Enter a reason").sendEnterText(() -> {

                        final String reason = player.getDialogueBuilder().getEnterText();

                        if (reason.length() < 1) {
                            player.sendMessage("You must enter a reason for your mute.");
                            return;
                        }

                        PunishPlayerHandler.jailPlayer(player, other, 1, reason);
                        
                    });
                }, "No", () -> {

                });
            	
            }, "Go back", () -> {
                player.getDialogueBuilder().returnToCheckpoint();
            });
        }).execute();

    }

    
    private static void searchPlayersForward(Player player, Predicate<Player> otherPlayer) {
    	int playerSize = PlayerHandler.players.length;
    	if (player.searchIndex >= playerSize)
    		player.searchIndex = 0;
    	for (int i = (int) player.searchIndex; i < playerSize; i++) {
    		Player other = PlayerHandler.players[i];
    		if (other != null && otherPlayer.test(other)) {
    			player.getPA().movePlayer(other.getCurrentLocation());
    			player.searchIndex = i+1;
    			break;
    		}
    		player.searchIndex = 0;
    	}
    }
    
    private static void searchPlayersBackward(Player player, Predicate<Player> otherPlayer) {
    	int playerSize = PlayerHandler.players.length;
    	if (player.searchIndex < 1)
    		player.searchIndex = playerSize-1;
    	for (int i = (int) player.searchIndex; i > 0; i--) {
    		Player other = PlayerHandler.players[i];
    		if (other != null && otherPlayer.test(other)) {
    			player.getPA().movePlayer(other.getCurrentLocation());
    			player.searchIndex = i-1;
    			break;
    		}
    		player.searchIndex = playerSize-1;
    	}
    }
    
}
