package plugin.item;

import com.soulplay.content.items.degradeable.DegradeData;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class ChargeToxicStaffPlugin extends Plugin implements ItemOnItemExtension {
	
	public static final int MAX_CHARGE = 11_000;

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, DegradeData.TSOTD_UNCHARGED, BlowPipeCharges.ZULRAH_SCALES);
		map(ItemOnItemExtension.class, DegradeData.TSOTD, BlowPipeCharges.ZULRAH_SCALES);
		map(ItemOnItemExtension.class, BlowPipeCharges.ZULRAH_SCALES, DegradeData.TSOTD_UNCHARGED);
		map(ItemOnItemExtension.class, BlowPipeCharges.ZULRAH_SCALES, DegradeData.TSOTD);
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		Item scales = null;
		Item other = null;
		if (used.getId() == BlowPipeCharges.ZULRAH_SCALES) {
			scales = used;
			other = with;
		} else {
			scales = with;
			other = used;
		}
		
		if (other.getId() == DegradeData.TSOTD_UNCHARGED) {
			if (player.getItems().deleteItem(other))
				player.getItems().addItem(DegradeData.TSOTD, 1);
		}
		
		ChargedItems.chargeItem(player, DegradeData.TSOTD, scales, getChargeLimit(player));
		
	}
	
	private int getChargeLimit(Player p) {
		int addon = 0;
		if (p.getDonatedTotalAmount() > 0) {
			addon = 5 * p.getDonatedTotalAmount();
		}
		return MAX_CHARGE + addon;
	}
	
}
