package plugin.item;

import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;
import com.soulplay.content.minigames.treasuretrails.ClueDifficulty;
import com.soulplay.content.minigames.treasuretrails.ClueManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;

public class ClueScrollPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		ClueDifficulty.init();
		for (ClueDifficulty clueDifficulty : ClueDifficulty.values) {
			map(ItemClickExtension.class, clueDifficulty.getScrollId());
		}
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		ClueDifficulty clueDifficulty = ClueDifficulty.forScroll(itemId);
		
		int data = player.getTreasureTrailManager().getCluesData(clueDifficulty);
		
		if (data == -1) {
			return;
		}

		ClueManager.clues.get(clueDifficulty.getIndex()).get(Misc.getFirst(data)).get(Misc.getSecond(data)).openHint(player);
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		ClueDifficulty clueDifficulty = ClueDifficulty.forScroll(itemId);
		int data = player.getTreasureTrailManager().getCluesData(clueDifficulty);
		if (data == -1) {
			return;
		}

		int steps = player.getTreasureTrailManager().getTrailStage(clueDifficulty);
		player.sendMessage("You have completed " + steps + " " + (steps == 1 ? "step" : "steps") + " on this " + clueDifficulty.getName().toLowerCase() + " clue scroll.");
	}

}
