package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.in.ItemAction2;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class CapeOfSkullsPlugin extends Plugin implements ItemClickExtension {

	public static final int ID = 123351;

	@Override
	public void onInit() {
		map(ItemClickExtension.class, ID);
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendOption("Wearing this cape will give you a PK skull.", "Give me a PK skull.", () -> {
			ItemAction2.wearItemCentral(player, itemId, itemSlot);
			player.skullPlayer();
		}, "Cancel.", () -> {}).executeIfNotActive();
	}

}
