package plugin.item;

import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;

public class DawnbringerPlugin extends Plugin implements ItemClickExtension, EquipmentClickExtension {

	public static final int ID = 122516;

	@Override
	public void onInit() {
		map(EquipmentClickExtension.class, ID);
		map(ItemClickExtension.class, ID);
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().wearItem(itemId, itemSlot)) {
			player.getPA().resetAutocast();
			player.setAutoCastSpell(SpellsData.DAWNBRINGER);
		}
	}

	@Override
	public void onRemoveClick(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().removeItem(itemId, itemSlot))
			player.getPA().resetAutocast();
	}

}
