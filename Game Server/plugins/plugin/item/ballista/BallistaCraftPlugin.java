package plugin.item.ballista;

import com.soulplay.content.items.BallistaCreationEnum;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class BallistaCraftPlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for (BallistaCreationEnum bal : BallistaCreationEnum.values) {
			map(ItemOnItemExtension.class, bal.getFrameId(), BallistaCreationEnum.BALLISTA_LIMBS);
			map(ItemOnItemExtension.class, BallistaCreationEnum.BALLISTA_LIMBS, bal.getFrameId());
			
			map(ItemOnItemExtension.class, BallistaCreationEnum.BALLISTA_SPRING, bal.getIncompleteId());
			map(ItemOnItemExtension.class, bal.getIncompleteId(), BallistaCreationEnum.BALLISTA_SPRING);
		}
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		
		if (player.getSkills().getLevel(Skills.FLETCHING) < 72) {
			player.sendMessage("You need lvl 72 fletching to create the ballista.");
			return;
		}
		
		BallistaCreationEnum bal = BallistaCreationEnum.get(used.getId());
		if (bal == null)
			bal = BallistaCreationEnum.get(with.getId());
		
		boolean spring = used.getId() == BallistaCreationEnum.BALLISTA_SPRING || with.getId() == BallistaCreationEnum.BALLISTA_SPRING;
		
		final int result = spring ? bal.getCompleteId() : bal.getIncompleteId();
		
		if (player.getItems().deleteItem(used) && player.getItems().deleteItem(with)) {
			player.getItems().addItem(result, 1);
			player.getPA().addSkillXP(660, Skills.FLETCHING);
		}
	}
}
