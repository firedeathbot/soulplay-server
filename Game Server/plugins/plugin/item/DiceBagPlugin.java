package plugin.item;

import com.soulplay.Server;
import com.soulplay.content.clans.Clan;
import com.soulplay.content.minigames.dice.GambleType;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class DiceBagPlugin extends Plugin implements ItemClickExtension {

	public static final Animation DICE_ANIMATION = new Animation(11900, 15);
	public static final Graphic DICE_6_GFX = Graphic.create(2074);
	public static final Graphic DICE_100_GFX = Graphic.create(2075);

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 15088);
		map(ItemClickExtension.class, 15098);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getStopWatch().checkThenStart(10)) {
			Clan clan = player.getClan();
			if (clan == null) {
				player.sendMessage("You are not in a clan.");
				return;
			}

			int rank = clan.getRank(player.getName());
			String message;
			if (rank == -1) {
				message = "<col=0>[<col=ff>" + clan.getTitle() + "<col=0>] " + player.getNameSmartUp() + ": <col=ff0000>" + player.getNameSmartUp();
			} else {
				message = "<col=0>[<col=ff>" + clan.getTitle() + "<col=0>] <clan=" + rank + ">" + player.getNameSmartUp() + ": <col=ff0000>" + player.getNameSmartUp();
			}

			final boolean dice100 = itemId == 15098;
			
			final int roll = Misc.random2(dice100 ? 100 : 12);
			
			player.getGambleLogger().logDice(dice100 ? GambleType.BJ100 : GambleType.BJ21, roll);
			
			player.forceChat("My roll " + roll);
			player.getClan().sendClanMessage("[DICE]" + message + " rolled <col=ff0000>"
					+ roll
					+ "<col=0> on a " + ItemConstants.getItemName(itemId), null);
			player.startGraphic(dice100 ? DICE_100_GFX : DICE_6_GFX);
			player.startAnimation(DICE_ANIMATION);
			return;
		}
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getStopWatch().checkThenStart(10)) {
			final boolean dice100 = itemId == 15098;
			final int rollNumber = Misc.random2(dice100 ? 100 : 12);
			player.startGraphic(dice100 ? DICE_100_GFX : DICE_6_GFX);
			player.startAnimation(DICE_ANIMATION);
			player.forceChat("My roll " + rollNumber);
			
			player.getGambleLogger().logDice(dice100 ? GambleType.BJ100 : GambleType.BJ21, rollNumber);
			
			for (Player p : Server.getRegionManager().getLocalPlayers(player)) {
				if (!p.isActive) {
					continue;
				}
				if (player.withinDistance(p)) {
					p.sendMessage(player.getNameSmartUp()
					+ " rolled <col=ff0000>" + rollNumber + "</col> on a "
					+ ItemConstants.getItemName(itemId));

				}
			}
		}
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case 15098:
			player.getItems().deleteItemInOneSlot(itemId, 1);
			player.getItems().addItem(15088, 1);
			break;
		case 15088:
			player.getItems().deleteItemInOneSlot(itemId, 1);
			player.getItems().addItem(15098, 1);
			break;
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendDestoryItem(itemId).execute();
	}
}
