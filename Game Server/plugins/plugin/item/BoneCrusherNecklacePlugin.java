package plugin.item;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.in.ItemAction2;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class BoneCrusherNecklacePlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension {

	public static final int ID = 122986;

	@Override
	public void onInit() {
		map(ItemClickExtension.class, ID);

		map(ItemOnItemExtension.class, 30332, 18337);
		map(ItemOnItemExtension.class, 30332, 122988);
		map(ItemOnItemExtension.class, 18337, 30332);
		map(ItemOnItemExtension.class, 18337, 122988);
		map(ItemOnItemExtension.class, 122988, 30332);
		map(ItemOnItemExtension.class, 122988, 18337);
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		if (player.getItems().playerHasItem(30332) && player.getItems().playerHasItem(18337) && player.getItems().playerHasItem(122988)) {
			player.getDialogueBuilder().sendStatement("Are you sure you wish to combine the <col=ff0000>Dragonbone necklace", "<col=ff0000>Bonecrusher, and Hydra tail into the Bonecrusher necklace?", "<col=ff0000>This can not be reversed.")
			.sendOption("Proceed with the combination.", () -> {
				if (player.getItems().playerHasItem(30332) && player.getItems().playerHasItem(18337) && player.getItems().playerHasItem(122988)) {
					player.getItems().deleteItem2(30332, 1);
					player.getItems().deleteItem2(18337, 1);
					player.getItems().deleteItem2(122988, 1);
					player.getItems().addItem(ID, 1);
					player.getDialogueBuilder().sendItemStatement(ID, "You successfully combine the Dragonbone necklace, the ", "Bonecrusher, and the Hydra tail into the Bonecrusher", "necklace.");
				}
			}, "Cancel.", () -> {}).execute();
		} else {
			player.sendMessage("You need to have Dragonbone necklace, Bonecrusher and Hydra's tail in the");
			player.sendMessage("inventory to make the necklace.");
		}
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		if (ItemAction2.wearItemCentral(player, itemId, itemSlot)) {
			player.dragonBoneNecklaceTimer.startTimer(15);
			player.pulse(() -> {
				player.sendMessageSpam("Your " + ItemDefinition.getName(itemId) + " is now active.");
			}, 15);
		}
	}

}
