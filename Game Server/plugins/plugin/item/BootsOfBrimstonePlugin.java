package plugin.item;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class BootsOfBrimstonePlugin extends Plugin implements ItemOnItemExtension {

	public static final int ID = 122951;

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, 123037, 122957);
		map(ItemOnItemExtension.class, 122957, 123037);
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getDialogueBuilder().sendStatement("Are you sure you wish to combine the <col=ff0000>Boots of stone and the", "<col=ff0000>Drake's claw into Boots of brimstone?", "<col=ff0000>This can not be reversed.")
			.sendOption("Proceed with the combination.", () -> {
				if (player.getItems().playerHasItem(122957) && player.getItems().playerHasItem(123037)) {
					player.getItems().deleteItem2(122957, 1);
					player.getItems().deleteItem2(123037, 1);
					player.getItems().addItem(ID, 1);
					player.getDialogueBuilder().sendItemStatement(ID, "You successfully combine the Boots of stone", " and the Drake's claw into the Boots of brimstone.");
				}
			}, "Cancel.", () -> {}).execute();
	}

}
