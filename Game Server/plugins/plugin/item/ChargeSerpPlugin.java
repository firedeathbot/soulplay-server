package plugin.item;

import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.SerpentineHelm;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class ChargeSerpPlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		SerpentineHelm.init();
		for (SerpentineHelm helm : SerpentineHelm.values()) {
			map(ItemOnItemExtension.class, helm.getUnchargedId(), BlowPipeCharges.ZULRAH_SCALES);
			map(ItemOnItemExtension.class, helm.getChargedId(), BlowPipeCharges.ZULRAH_SCALES);
			map(ItemOnItemExtension.class, BlowPipeCharges.ZULRAH_SCALES, helm.getUnchargedId());
			map(ItemOnItemExtension.class, BlowPipeCharges.ZULRAH_SCALES, helm.getChargedId());
		}
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		Item scales = null;
		Item other = null;
		if (used.getId() == BlowPipeCharges.ZULRAH_SCALES) {
			scales = used;
			other = with;
		} else {
			scales = with;
			other = used;
		}
		
		SerpentineHelm helm = SerpentineHelm.getHelm(other.getId());
		if (helm == null) {
			player.sendMessage("You messed up.");
			return;
		}
		
		if (other.getId() == helm.getUnchargedId()) {
			if (player.getItems().deleteItem(other))
				player.getItems().addItem(helm.getChargedId(), 1);
		}
		
		int limitIncrease = 0;
		if (player.getDonatedTotalAmount() > 0)
			limitIncrease = 10 * player.getDonatedTotalAmount();
		
		ChargedItems.chargeItem(player, helm.getChargedId(), scales, helm.getMaxCharges() + limitIncrease);
		
	}
}
