package plugin.item;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.definition.RecoilRingEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class EyeOfTheRingsPlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension {

	@Override
	public void onInit() {
		int ether = 30237;
		RecoilRingEnum[] values = { RecoilRingEnum.EYE_OF_THE_MAGE, RecoilRingEnum.EYE_OF_THE_RANGER, RecoilRingEnum.EYE_OF_THE_WARRIOR };
		for (int i = 0; i < values.length; i++) {
			
			RecoilRingEnum ring = values[i];
			
			map(ItemClickExtension.class, ring.getItemId());
			
			map(ItemOnItemExtension.class, ring.getItemId(), ether);
			map(ItemOnItemExtension.class, ether, ring.getItemId());
		
		}
	}
	
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getRecoilRings().chargeEyeRings(used, with);
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getItems().wearItem(itemId, itemSlot);
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getRecoilRings().checkCharges(itemId);
	}
	
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendDestoryItem(itemId).execute();
	}
}
