package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class SeasonPassLevelUpPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 12502);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().reset();
		player.getDialogueBuilder()
		.sendStatement("Would you like to Redeem this scroll?",
				"It will level up the season pass by 5 levels.")
		.sendOption("Redeem Scroll?", "Redeem Season Pass Level-up", ()-> {
			player.seasonPass.seasonPasslevelUpScroll(player, itemId, itemSlot);
		}, "No", ()-> {
			player.getDialogueBuilder().reset();
		})
		.executeIfNotActive();

	}
	
}