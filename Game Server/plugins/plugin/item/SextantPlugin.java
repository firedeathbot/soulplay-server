package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class SextantPlugin extends Plugin implements ItemClickExtension {
	
	private static final Location MID = Location.create(2440, 3161);

	private static final float TILE = 1.875f;
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, 2574);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {

//		int playerY = player.getY() > 6400 ? player.getY() - 6400 : player.getY();
		
		if (player.getY() > 6400) {
			player.getDialogueBuilder().sendStatement("The ceiling looks great here.").execute();
			return;
		}
		
		int playerX = player.getX() >= 6400 ? player.getX() - 6400 : player.getX();
		
		int xOff = playerX - MID.getX();
		int yOff = player.getY() - MID.getY();
		
		int xDist = (int) (Math.abs(xOff) * TILE);
		int xDegrees = xDist / 60;
		int xMinutes = xDist % 60;
		
		int yDist = (int) (Math.abs(yOff) * TILE);
		int yDegrees = yDist / 60;
		int yMinutes = yDist % 60;
		
		String xDirection = xOff >= 0 ? "East" : "West";
		String yDirection = yOff >= 0 ? "North" : "South";
		
		if (player.getDialogueBuilder().isActive())
			player.getDialogueBuilder().reset();
		
		player.getDialogueBuilder()
		.sendStatement(yDegrees + " degrees " + yMinutes + " minutes " + yDirection,
						xDegrees + " degrees " + xMinutes + " minutes " + xDirection)
		.executeIfNotActive();
		
	}
}
