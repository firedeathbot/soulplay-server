package plugin.item;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.info.AnimationsData;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class RubberChickenPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 4566);
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getItems().wearItem(itemId, itemSlot);
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		player.startAnimation(AnimationsData.CHICKEN_DANCE);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
}
