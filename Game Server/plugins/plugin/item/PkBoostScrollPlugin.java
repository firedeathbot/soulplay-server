package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class PkBoostScrollPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 608);
		map(ItemClickExtension.class, 2403);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
			case 608:
				if (player.combatLevel >= 100) {
					player.sendMessage("Your combat level is too high to use this scroll.");
					return;
				}

				activePkpBoost(player, itemId);
				break;
			case 2403:
				if (player.combatLevel < 100) {
					player.sendMessage("Your combat level is too low to use this scroll.");
					return;
				}

				activePkpBoost(player, itemId);
				break;
		}
	}

	private static void activePkpBoost(Player player, int itemId) {
		player.getDialogueBuilder().sendOption("Activate the scroll?", "Yes.", () -> {
			if (player.pkpBoostActive()) {
				player.sendMessage("Pk points boost is already active.");
				return;
			}

			if (!player.getItems().deleteItemInOneSlot(itemId, 1)) {
				return;
			}

			player.sendMessage("You've used your pk points boosting scroll. It will last one hour.");
			player.setPkpBoostLength(Misc.secondsToTicks(3600));
		}, "No.", () -> {}).execute();
	}

}
