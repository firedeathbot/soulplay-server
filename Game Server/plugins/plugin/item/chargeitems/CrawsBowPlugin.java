package plugin.item.chargeitems;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class CrawsBowPlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension, EquipmentClickExtension {

	public static final int UNCHARGED_ID = 30382;
	public static final int CHARGED_ID = 30384;
	
	private static final int ETH = 30237;
	
	private static final int MAX_CHARGES = 17_000;
	
	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, ETH, UNCHARGED_ID);
		map(ItemOnItemExtension.class, UNCHARGED_ID, ETH);
		map(ItemOnItemExtension.class, ETH, CHARGED_ID);
		map(ItemOnItemExtension.class, CHARGED_ID, ETH);
		
		map(ItemClickExtension.class, UNCHARGED_ID);
		map(ItemClickExtension.class, CHARGED_ID);
		
		map(EquipmentClickExtension.class, CHARGED_ID);
	}
	

	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ID:
			//check charges
			checkCharges(player);
			break;
			
		case UNCHARGED_ID:
			player.getDialogueBuilder()
			.sendStatement("To charge, you must use Revenant Ether on the bow.")
			.execute();
			break;
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ID:
			//uncharge
			ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), true);
			break;
		case UNCHARGED_ID:
			Item item = new Item(UNCHARGED_ID, player.playerItemsN[itemSlot], itemSlot);
			FifthItemAction.DROP_ITEM.init((Client)player, item);
			break;
		}
		
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		int maxCharges = getMaxCharges(player);
		ChargedItems.chargeItems(player, used, with, maxCharges);
	}
	
	private static int getMaxCharges(Player player) {
		int maxCharges = MAX_CHARGES;
		if (player.getDonatedTotalAmount() > 0) {
			maxCharges += player.getDonatedTotalAmount() * 5;
		}
		return maxCharges;
	}
	
	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		checkCharges(player);
	}
	
	private static void checkCharges(Player player) {
		player.sendMessage("Craw's bow Charges: " + player.getCharges(CHARGED_ID) + " of " + getMaxCharges(player));
	}
	
}
