package plugin.item.chargeitems;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class ScytheOfViturPlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension {

	public static final int UNCHARGED_ID = 30327;
	public static final int CHARGED_ID = 30328;
	
	public static final int BLOOD_VIAL = 30369;
	
	private static final int MAX_CHARGES = 20000;
	
	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, BLOOD_VIAL, UNCHARGED_ID);
		map(ItemOnItemExtension.class, UNCHARGED_ID, BLOOD_VIAL);
		map(ItemOnItemExtension.class, BLOOD_VIAL, CHARGED_ID);
		map(ItemOnItemExtension.class, CHARGED_ID, BLOOD_VIAL);
		
		map(ItemClickExtension.class, UNCHARGED_ID);
		map(ItemClickExtension.class, CHARGED_ID);
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ID:
			//check charges
			player.sendMessage("Scythe Charges: " + player.getCharges(itemId) + " of " + getMaxCharges(player));
			break;
			
		case UNCHARGED_ID:
			player.getDialogueBuilder()
			.sendStatement("To charge, you must use Vial of Blood on the Scythe.")
			.execute();
			break;
		}
	}
	
	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		if (!player.getStopWatch().checkThenStart(1)) {
			return;
		}
		switch (itemId) {
		case CHARGED_ID:
			//uncharge
			player.getDialogueBuilder().reset();
			player.getDialogueBuilder().sendStatement("Your charges will vanish when uncharghing.", "Are you sure you would like to uncharge?")
			.sendOption("Uncharge Scythe?", "Yes", ()-> {
				player.getDialogueBuilder().reset();
				ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), false);
			}, "No", ()-> {
				player.getDialogueBuilder().reset();
			}).execute();
			break;
		case UNCHARGED_ID:
			break;
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ID:
			//uncharge
			ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), false);
			break;
		case UNCHARGED_ID:
			//dismantle
			break;
		}
		
		Item item = new Item(UNCHARGED_ID, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		int maxCharges = getMaxCharges(player);
		ChargedItems.chargeItems(player, used, with, maxCharges);
	}
	
	private static int getMaxCharges(Player player) {
		int maxCharges = MAX_CHARGES;
		if (player.getDonatedTotalAmount() > 0) {
			maxCharges += player.getDonatedTotalAmount() * 5;
		}
		return maxCharges;
	}
	
}
