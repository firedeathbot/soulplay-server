package plugin.item.chargeitems;

import com.soulplay.content.items.degradeable.PvpDegradeableEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;

public class PvpDegGearPlugin extends Plugin implements ItemClickExtension, EquipmentClickExtension {

	@Override
	public void onInit() {
		for (PvpDegradeableEnum pvp : PvpDegradeableEnum.values()) {
			if (pvp.getReplaceId() == -1) {
				map(ItemClickExtension.class, pvp.getItemId());
				map(EquipmentClickExtension.class, pvp.getItemId());
			}
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().reset();
		
		player.getDialogueBuilder().sendStatement("Would you like to destroy this item?", "By destroying this item you reset the charges on the account.")
		.sendOption("Reset charges?", "Yes", ()-> {
			if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
				player.resetCharge(itemId);
				player.sendMessage("You have destroyed the item and reset the charges on the account.");
			}
		}, "No", ()-> {
			
		})
		.execute();
	}
	
	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		player.sendMessage("Charges: " + player.getCharges(itemId));
	}
	
}
