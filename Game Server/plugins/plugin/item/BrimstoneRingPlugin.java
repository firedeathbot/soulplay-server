package plugin.item;

import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class BrimstoneRingPlugin extends Plugin implements ItemOnItemExtension {

	public static final int ID = 122975;
	public static final int HYDRAS_EYE = 122973;
	public static final int HYDRAS_FANG = 122971;
	public static final int HYDRAS_HEART = 122969;

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, HYDRAS_EYE, HYDRAS_HEART);
		map(ItemOnItemExtension.class, HYDRAS_EYE, HYDRAS_FANG);
		map(ItemOnItemExtension.class, HYDRAS_FANG, HYDRAS_HEART);
		map(ItemOnItemExtension.class, HYDRAS_FANG, HYDRAS_EYE);
		map(ItemOnItemExtension.class, HYDRAS_HEART, HYDRAS_FANG);
		map(ItemOnItemExtension.class, HYDRAS_HEART, HYDRAS_EYE);
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		if (player.getItems().playerHasItem(HYDRAS_EYE) && player.getItems().playerHasItem(HYDRAS_FANG) && player.getItems().playerHasItem(HYDRAS_HEART)) {
			player.getDialogueBuilder().sendStatement("Are you sure you wish to combine the <col=ff0000>Hydra Heart, the Hydra", "<col=ff0000>Eye, and Hydra Fang into the Brimstone Ring? This can not be", "<col=ff0000>reversed.")
			.sendOption("Proceed with the combination.", () -> {
				if (player.getItems().playerHasItem(HYDRAS_EYE) && player.getItems().playerHasItem(HYDRAS_FANG) && player.getItems().playerHasItem(HYDRAS_HEART)) {
					player.getItems().deleteItem2(HYDRAS_EYE, 1);
					player.getItems().deleteItem2(HYDRAS_FANG, 1);
					player.getItems().deleteItem2(HYDRAS_HEART, 1);
					player.getItems().addItem(ID, 1);
					player.getDialogueBuilder().sendItemStatement(ID, "You successfully combine the Hydra Heart, the Hydra", "Eye, and the Hydra Fang into the Brimstone Ring.");
				}
			}, "Cancel.", () -> {}).execute();
		} else {
			player.sendMessage("You need to have Hydra's eye, Hydra's fang and Hydra's heart in the inventory to");
			player.sendMessage("make the ring.");
		}
	}

}
