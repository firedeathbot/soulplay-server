package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class PrayerScrollPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 30095);
		map(ItemClickExtension.class, 30096);
	}
	
	public static final Animation SCROLL_READ_ANIM = new Animation(840);
	public static final Animation SCROLL_STOP_READ_ANIM = new Animation(3044);
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		
		player.startAnimation(SCROLL_READ_ANIM);
		
		switch (itemId) {
		
		case 30095:// dexterous prayer scroll
			if (player.isRigourUnlocked()) {
				displayAlreadyUnlocked(player, itemId);
				return;
			}
			sendUnlockDialogue(player, itemId, itemSlot);
			break;
			
		case 30096: // arcane prayer scroll
			if (player.isAuguryUnlocked()) {
				displayAlreadyUnlocked(player, itemId);
				return;
			}
			sendUnlockDialogue(player, itemId, itemSlot);
			break;
			
		}
		
	}
	
	private static void sendUnlockDialogue(Player player, int itemId, int itemSlot) {
		player.getDialogueBuilder()
		.sendItemStatement(itemId, "You can make out some faded words on the",
				"ancient parchment. It appears to be an archaic",
				"invocation of the gods! Would you like to absorb its",
				"power? (Warning: This will consume the scroll.)")
		.sendOption("Absorb power?", "Yes", ()-> {
			if (player.getItems().playerHasItem(itemId, itemSlot, 1)) {
				player.getItems().deleteItemInSlot(itemSlot);
				switch (itemId) {
				case 30095:// dexterous prayer scroll
					player.unlockRigour();
					break;
					
				case 30096: // arcane prayer scroll
					player.unlockAugury();
					break;
				}
			}
		}, "No", ()-> {
			player.getDialogueBuilder().closeNew();
		})
		.onReset(() -> {
			player.startAnimation(SCROLL_STOP_READ_ANIM);
		})
		.executeIfNotActive();
	}
	
	private static void displayAlreadyUnlocked(Player player, int itemId) {
		player.getDialogueBuilder()
		.sendItemStatement(itemId, "You can make out some faded words on",
				"the ancient parchment. It appears to",
				"be an archaic invocation of the gods.",
				"However there's nothing more for you to learn.")
		.onReset(() -> {
			player.startAnimation(SCROLL_STOP_READ_ANIM);
		})
		.executeIfNotActive();
	}
}
