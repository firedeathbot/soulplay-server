package plugin.item;

import com.soulplay.content.items.urn.SkillingUrn;
import com.soulplay.content.items.urn.SkillingUrns;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class SkillingUrnPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		SkillingUrns.load();
		
		for (SkillingUrn urn : SkillingUrns.URNS) {
			map(ItemClickExtension.class, urn.getActive());
			map(ItemClickExtension.class, urn.getFull());
			map(ItemClickExtension.class, urn.getNoRune());
		}
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		SkillingUrns.handleUrnClick(player, itemId, 1);
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		SkillingUrns.handleUrnClick(player, itemId, 2);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {

		player.getPA().destroyItem(itemId, player.getItems().getItemAmount(itemId, itemSlot),
				() -> {
					player.getItems().deleteItemInSlot(itemSlot);

					SkillingUrn urn = SkillingUrns.ACTIVE_URNS.get(itemId);
					if (urn == null) {
						urn = SkillingUrns.FULL_URNS.get(itemId);
					}

					if (urn != null) {
						player.urnData[urn.getIndex()] = 0;
					}
				}, null, "Are you sure you want to drop this item?", "This item is valuable, you will not", "get it back once lost.");
	
	}
}
