package plugin.item.funitems;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class SpeedScrollPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 30379);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (!player.isRunning && !player.getMovement().toggleRun()) {
			player.sendMessage("You cannot use the scroll without any run energy.");
			return;
		}
		if (player.wildLevel > 0) {
			player.sendMessage("You cannot use this scroll in wild.");
			return;
		}
		if (player.speedScrollTimer.checkThenStart(100)) {
			speedUpPlayer(player, 16, 2, itemId, itemSlot);
		} else {
			player.sendMessage("You cannot use this scroll for " + Misc.ticksToSeconds(player.speedScrollTimer.getRemainingTicks()) + " seconds.");
		}
	}
	
	public static void speedUpPlayer(Player player, int length, int speed, int itemId, int itemSlot) {
		
		if (!player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
			return;
		}
		
		player.runTilesPerTick = speed;
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				player.runTilesPerTick = 1;
			}
		}, length);
	}
	
}
