package plugin.item.funitems;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class RopeOnChairPlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, 954, 8310);
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.setDead(true);
	}
	
}
