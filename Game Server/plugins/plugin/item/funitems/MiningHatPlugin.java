package plugin.item.funitems;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class MiningHatPlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 5013);
		map(ItemOnItemExtension.class, 5014, 590);
		map(ItemOnItemExtension.class, 590, 5014);
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getItems().replaceItemSlot(5014, itemSlot);
		player.sendMessage("You extinguish the light in the helmet. Light it using the tinderbox.");
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getItems().replaceItemSlot(5013, used.getId() == 5014 ? used.getSlot() : with.getSlot());
		player.sendMessage("You light the mining helmet.");
	}

}
