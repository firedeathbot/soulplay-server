package plugin.item.funitems;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class FlashScrollPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 30380);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (!player.isRunning && !player.getMovement().toggleRun()) {
			player.sendMessage("You cannot use the scroll without any run energy.");
			return;
		}
		if (player.wildLevel > 0) {
			player.sendMessage("You cannot use this scroll in wild.");
			return;
		}
		if (player.flashScrollTimer.checkThenStart(500)) {
			SpeedScrollPlugin.speedUpPlayer(player, 10, 7, itemId, itemSlot);
		} else {
			player.sendMessage("You cannot use this scroll for " + Misc.ticksToSeconds(player.flashScrollTimer.getRemainingTicks()) + " seconds.");
		}
	}
	
}
