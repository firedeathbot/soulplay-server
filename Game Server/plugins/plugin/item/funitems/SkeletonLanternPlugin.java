package plugin.item.funitems;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;

public class SkeletonLanternPlugin extends Plugin implements EquipmentClickExtension {

	@Override
	public void onInit() {
		map(EquipmentClickExtension.class, ITEM_ID);
	}
	
	public static final int ITEM_ID = 30420;
	
	private static final int BAT_ID = 6835;
	
	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.wildLevel > 0 || player.inMinigame()) {
			player.sendMessage("You cannot transform here.");
			return;
		}
		if (player.getTransformId() > -1) {
			if (player.getTransformId() == BAT_ID) {
				transformBack(player);
				return;
			}
			player.sendMessage("You are already transformed.");
			return;
		}
			
		player.startGraphic(GRAPHIC1);
		player.transform(BAT_ID);
		player.getPA().resetMovementAnimation();
        player.resetAnimations();
        player.sendMessage("You transform into a bat!");
	}
	
	@Override
	public void onRemoveClick(Player player, int itemId, int itemSlot, int interfaceId) {
		transformBack(player);
		EquipmentClickExtension.super.onRemoveClick(player, itemId, itemSlot, interfaceId);
	}
	
	public static void transformBack(Player player) {
		if (player.getTransformId() == BAT_ID) {
			player.startGraphic(GRAPHIC1);
			player.transform(-1);
			player.getPA().resetMovementAnimation();
	        player.resetAnimations();
		}
	}
	
	
	private static final Graphic GRAPHIC1 = Graphic.create(Graphic.getOsrsId(86), GraphicType.HIGH);
}
