package plugin.item.seasonals;

import com.soulplay.content.seasonals.powerups.PowerUpInterface;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class BeginnerBookPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 19705);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.combatLevel > 3) {
			player.sendMessage("You can only re-learn powers while being a combat level of 3.");
			return;
		}

		player.getDialogueBuilder().sendStatement("If you agree to read this book, you will forget all your current", "powers and re-learn new ones.").sendOption("Are you sure you want to read this?", "Yes, I want new knowledge.", () -> {
			if (player.combatLevel > 3) {
				player.sendMessage("You can only re-learn powers while being a combat level of 3.");
				return;
			}

			player.getSeasonalData().powerUps.clear();
			PowerUpInterface.rollPowerUps(player);
			player.getPacketSender().sendChatInterface(-1, false);
			if (player.interfaceIdOpenMainScreen == PowerUpInterface.ID) {
				player.getPacketSender().sendPowerUpData();
			}
		}, "No, I'll keep my current ones.", () -> {}).execute(false);
	}

}
