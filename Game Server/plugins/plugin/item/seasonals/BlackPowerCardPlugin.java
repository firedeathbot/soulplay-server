package plugin.item.seasonals;

import java.util.List;

import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.content.seasonals.powerups.PowerUpRarity;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class BlackPowerCardPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 25000);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().deleteItem(new Item(itemId, 1, itemSlot))) {
			List<PowerUpData> list = chooseList();
			player.getItems().addItem(Misc.random(list).getCardId(), 1);
		}
	}

	private static List<PowerUpData> chooseList() {
		if (Misc.randomBoolean(PowerUpRarity.SUPER_RARE.getRoll())) {
			return PowerUpData.rarityToPerks.get(PowerUpRarity.SUPER_RARE);
		}

		if (Misc.randomBoolean(PowerUpRarity.RARE.getRoll())) {
			return PowerUpData.rarityToPerks.get(PowerUpRarity.RARE);
		}

		if (Misc.randomBoolean(PowerUpRarity.UNCOMMON.getRoll())) {
			return PowerUpData.rarityToPerks.get(PowerUpRarity.UNCOMMON);
		}

		return PowerUpData.rarityToPerks.get(PowerUpRarity.COMMON);
	}

}
