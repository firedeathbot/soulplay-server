package plugin.item.seasonals;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.seasonals.powerups.PowerUpData;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnPlayerExtension;

public class PowerCardsPlugin extends Plugin implements ItemClickExtension, ItemOnPlayerExtension {

	@Override
	public void onInit() {
		for (int i = 0, length = PowerUpData.values.length; i < length; i++) {
			int itemId = 25001 + i;
			map(ItemClickExtension.class, itemId);
			map(ItemOnPlayerExtension.class, itemId);
		}
	}

	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder()
				.sendStatement("You can use this card to gain a specific power on your alt account.",
						"Simply select use and click your alt account.",
						"A confirmation box will pop up to verify if you wish to proceed.")
				.execute();
	}

	@Override
	public void onItemOnPlayer(Player player, Player other, Item item) {
		if (!other.UUID.equals(player.UUID)) {
			player.sendMessage("You can only use this card on your alt account.");
			return;
		}

		if (other.combatLevel > 3) {
			player.sendMessage("You can only use this card on an account that is a level 3.");
			return;
		}

		if (other.getSeasonalData().powerLearnedFromCard != null) {
			other.getDialogueBuilder().sendStatement("You already have a card learned.", "If you wish to proceed it will overwrite your current one!");
		}

		other.getDialogueBuilder().sendItemStatement(item.getId(), "You're about to learn " + ItemDef.forID(item.getId()).getName() + ".").sendOption("Do you accept?", "Yes.", () -> {
			if (!player.isActive || player.disconnected) {
				return;
			}

			PowerUpData power = PowerUpData.itemToPerk.get(item.getId());
			if (power == null) {
				other.sendMessage("Invalid item.");
				player.sendMessage("Invalid item.");
				return;
			}

			if (player.getItems().deleteItem(item)) {
				other.getSeasonalData().powerLearnedFromCard = power;
				other.sendMessage("You've noted down the secrets for learning this power.");
			}
		}, "No.", () -> {}).execute();
	}

}
