package plugin.item.seasonals;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class PageOfDesnityPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 11341);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (Variables.SEASONAL_POD_UNLOCKED.getValue(player) > 0) {
			player.sendMessage("You already know the secret.");
			return;
		}

		if (player.combatLevel > 3) {
			player.sendMessage("You can only read Page of Destiny while being a combat level of 3.");
			return;
		}

		player.getDialogueBuilder().sendStatement("By reading this page, you will be able to forget powers without a cost.").sendOption("Are you sure you want to read this?", "Yes, I want new knowledge.", () -> {
			if (player.combatLevel > 3) {
				player.sendMessage("You can only read Page of Destiny while being a combat level of 3.");
				return;
			}

			if (player.getItems().deleteItem2(itemId, 1)) {
				Variables.SEASONAL_POD_UNLOCKED.setValue(player, 1);
				player.sendMessage("You've unlocked an ability to forget powers without a cost!");
			}
		}, "No, I'd rather sell it.", () -> {}).execute(true);
	}

}
