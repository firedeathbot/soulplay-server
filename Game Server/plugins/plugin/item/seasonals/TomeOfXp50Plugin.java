package plugin.item.seasonals;

import com.soulplay.game.model.DisplayTimerType;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class TomeOfXp50Plugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 9658);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getTomeExpLength() > 0) {
			player.getDialogueBuilder().sendStatement("You already have a Tome boost active.", "Wouldn't be wise to waste it.").execute();
			return;
		}

		if (player.getItems().deleteItem(new Item(itemId, 1, itemSlot))) {
			Variables.TOME_EXTRA_XP_MOD.setValue(player, 50);

			int ticks = Misc.minutesToTicks(30);
			player.setTomeExpLength(ticks);
			player.getPacketSender().sendWidget(DisplayTimerType.TOME_OF_XP50, ticks);
			player.sendMessage("You consume the Tome and gain 50% experience boost for 30 minutes.");
		}
	}

}
