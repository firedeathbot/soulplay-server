package plugin.item;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.definition.RecoilRingEnum;
import com.soulplay.game.model.item.definition.RingOfSufferingEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class RingOfSufferingFirstChargePlugin extends Plugin implements ItemOnItemExtension {
	
	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, RingOfSufferingEnum.RING_OF_SUFFERING.getUnchargeId(), RecoilRingEnum.RING_OF_RECOIL.getItemId());
		map(ItemOnItemExtension.class, RecoilRingEnum.RING_OF_RECOIL.getItemId(), RingOfSufferingEnum.RING_OF_SUFFERING.getUnchargeId());
		map(ItemOnItemExtension.class, RingOfSufferingEnum.RING_OF_SUFFERING_I.getUnchargeId(), RecoilRingEnum.RING_OF_RECOIL.getItemId());
		map(ItemOnItemExtension.class, RecoilRingEnum.RING_OF_RECOIL.getItemId(), RingOfSufferingEnum.RING_OF_SUFFERING_I.getUnchargeId());
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		boolean item1RecoilRing = used.getId() == RecoilRingEnum.RING_OF_RECOIL.getItemId();
		
		Item recoil = item1RecoilRing ? used : with;
		Item suffering = item1RecoilRing ? with : used;
		
		RingOfSufferingEnum ring = RingOfSufferingEnum.getByUncharged(suffering.getId());
		if (ring == null)
			return;
		
		player.getItems().replaceItemSlot(ring.getChargedId(), suffering.getSlot());
		
		Item newRing = new Item(ring.getChargedId(), suffering.getAmount(), suffering.getSlot());
		
		player.getRecoilRings().chargeSufferingRings(newRing, recoil);
		
	}

}
