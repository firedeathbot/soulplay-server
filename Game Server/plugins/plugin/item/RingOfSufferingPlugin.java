package plugin.item;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.definition.RecoilRingEnum;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class RingOfSufferingPlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, RecoilRingEnum.RING_OF_SUFFERING_R.getItemId(), RecoilRingEnum.RING_OF_RECOIL.getItemId());
		map(ItemOnItemExtension.class, RecoilRingEnum.RING_OF_RECOIL.getItemId(), RecoilRingEnum.RING_OF_SUFFERING_R.getItemId());
		map(ItemOnItemExtension.class, RecoilRingEnum.RING_OF_SUFFERING_RI.getItemId(), RecoilRingEnum.RING_OF_RECOIL.getItemId());
		map(ItemOnItemExtension.class, RecoilRingEnum.RING_OF_RECOIL.getItemId(), RecoilRingEnum.RING_OF_SUFFERING_RI.getItemId());
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getRecoilRings().chargeSufferingRings(used, with);
	}
}
