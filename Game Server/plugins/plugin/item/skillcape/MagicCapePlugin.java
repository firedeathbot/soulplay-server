package plugin.item.skillcape;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.content.player.SpellBook;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;

public class MagicCapePlugin extends Plugin implements EquipmentClickExtension, ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 9762);
		map(ItemClickExtension.class, 9763);
		map(EquipmentClickExtension.class, 9762);
		map(EquipmentClickExtension.class, 9763);
	}
	
	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		sendDialogue(player);
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		sendDialogue(player);
	}
	
	private static Map<Integer, Integer> uses = new HashMap<>();
	
	public static void resetUses() {
		uses.clear();
	}
	
	private static boolean canUse(Player player, boolean inc) {
		
		Integer amount = uses.getOrDefault(player.mySQLIndex, 0);
		
		if (amount >= 5) {
			player.sendMessage("You have already used up your magic cape for today.");
			return false;
		}
		
		if (inc) {
			int newAmount = amount+1;
			uses.put(player.mySQLIndex, newAmount);
			player.sendMessage("Today's Magic Cape uses: " + newAmount);
		}
		
		return true;
	}
	
	private static void sendDialogue(Player player) {
		if (player.playerMagicBook == SpellBook.NORMAL) {
			player.getDialogueBuilder().sendOption("Choose spellbook", "Ancient", ()->{
				swapTo(player, SpellBook.ANCIENT);
			}, "Lunar", ()->{
				swapTo(player, SpellBook.LUNARS);
			}).execute();
		} else if (player.playerMagicBook == SpellBook.ANCIENT) {
			player.getDialogueBuilder().sendOption("Choose spellbook", "Normal", ()->{
				swapTo(player, SpellBook.NORMAL);
			}, "Lunar", ()->{
				swapTo(player, SpellBook.LUNARS);
			}).execute();
		} else if (player.playerMagicBook == SpellBook.LUNARS) {
			player.getDialogueBuilder().sendOption("Choose spellbook", "Normal", ()->{
				swapTo(player, SpellBook.NORMAL);
			}, "Ancient", ()->{
				swapTo(player, SpellBook.ANCIENT);
			}).execute();
		}
	}
	
	private static void swapTo(Player player, SpellBook book) {
		if (canUse(player, true))
			player.getPA().changeSpellBook(book);
	}
	
}
