package plugin.item;

import com.soulplay.game.model.item.ComparableItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.ItemHandler;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class GraniteMaulWithClampPlugin extends Plugin implements ItemClickExtension {

	public static final int GRANITE_MAUL = 4153;
	public static final int GRANITE_MAUL_W_CLAMP = 30066;
	public static final int GRANITE_CLAMP = 30067;
	
	public static final int GOLD_CONV = 200_000_000;
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, GRANITE_MAUL_W_CLAMP);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().reset();
		
		player.getDialogueBuilder().sendStatement("Would you like to take off the clamp?")
		.sendOption("Remove Clamp?", "Yes", ()-> {
			if (player.getItems().freeSlots() == 0) {
				player.sendMessage("You need more room in your inventory.");
				return;
			}
			if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
				player.getItems().addItem(GRANITE_MAUL, 1);
				player.getItems().addItem(GRANITE_CLAMP, 1);
			}
		}, "No", ()-> {
			player.getDialogueBuilder().closeNew();
		}).execute();
	}
	
	
	public static void onDeath(Player c, Player killer) {
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {

				if (c.itemsToKeep.contains(new ComparableItem(c.playerItems[i] - 1, c.playerItemsN[i]))) {
				    continue;
				}

				c.playerItems[i] = GRANITE_MAUL+1;
				c.setUpdateInvInterface(3214);
				dropGold(c, killer);
			}
		}

		if (c.playerEquipment[PlayerConstants.playerWeapon] > 0) {

			if (!c.itemsToKeep.contains(new ComparableItem(c.playerEquipment[PlayerConstants.playerWeapon], c.playerEquipmentN[PlayerConstants.playerWeapon]))) {

				c.getItems().replaceEquipment(PlayerConstants.playerWeapon, GRANITE_MAUL);
				dropGold(c, killer);

			}
		}
		
	}
	
	private static void dropGold(Player c, Player killer) {
		ItemHandler.createGroundItemFromPlayerDeath(killer, killer.getName(), 995, killer.getX(), killer.getY(), killer.getZ(), GOLD_CONV, c);
	}
	
}
