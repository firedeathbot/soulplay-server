package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class InstanceTokenPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 10943);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendStatement("You can consume this token to gain private instance charges.").execute();
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendItemStatement(10943, "Token will be deleted once you redeem it.").sendOption("Redeen the token?", "Yes.", () -> {
			if (player.getItems().deleteItem2(itemId, 1)) {
				Variables.INSTANCE_CHARGES.addValue(player, 1);
				int charges = Variables.INSTANCE_CHARGES.getValue(player);
				player.getDialogueBuilder().sendItemStatement(10943, "You've redeemed an instance charge.", "Now you have " + charges + " " + (charges == 1 ? "charge" : "charges") + ".").executeIfNotActive();
			}
		}, "No.", () -> {}).execute();
	}
	
}
