package plugin.item.trident;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.definition.ChargeInfo;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;
import com.soulplay.util.Misc;

public class TridentOfTheSeasPlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension, EquipmentClickExtension {

	public static final int CHARGED_ITEM_ID = 30014;
	public static final int UNCHARGED_ITEM_ID = 30016;
	
	private static final int MAX_CHARGES = 2500;
	
	private static final int GOLD = 995;
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, CHARGED_ITEM_ID);
		map(ItemClickExtension.class, UNCHARGED_ITEM_ID);
		
		map(EquipmentClickExtension.class, CHARGED_ITEM_ID);
		map(EquipmentClickExtension.class, UNCHARGED_ITEM_ID);
		
		map(ItemOnItemExtension.class, UNCHARGED_ITEM_ID, GOLD);
		map(ItemOnItemExtension.class, GOLD, UNCHARGED_ITEM_ID);
		
		map(ItemOnItemExtension.class, CHARGED_ITEM_ID, GOLD);
		map(ItemOnItemExtension.class, GOLD, CHARGED_ITEM_ID);
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().wearItem(itemId, itemSlot)) {
			player.getPA().resetAutocast();
			player.setAutoCastSpell(SpellsData.TRIDENT_OF_THE_SEA);
		}
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ITEM_ID:
			player.sendMessage("Trident Charges: " + player.getCharges(itemId));
			break;
		case UNCHARGED_ITEM_ID:
			player.getDialogueBuilder()
			.sendStatement("To charge, you must use Gold Coins on the Trident.", "1,000 Coins gives you 1 charge.")
			.execute();
			break;
		}
	}
	
	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		if (!player.getStopWatch().checkThenStart(1)) {
			return;
		}
		switch (itemId) {
		case CHARGED_ITEM_ID:
			//uncharge
			ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), false);
			break;
		case UNCHARGED_ITEM_ID:
			//nothing
			break;
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ITEM_ID:
			//uncharge
			ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), false);
			break;
		case UNCHARGED_ITEM_ID:
			//dismantle
			break;
		}
		
		Item item = new Item(UNCHARGED_ITEM_ID, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		int maxCharges = MAX_CHARGES;
		if (player.getDonatedTotalAmount() > 0) {
			maxCharges += player.getDonatedTotalAmount() * 5;
		}
//		ChargedItems.chargeItems(player, used, with, maxCharges);
		
		chargeCustom(player, used, usedSlot, with, withSlot, maxCharges);
	}
	
	private static final int GP_MULT = 1000;
	
	private static void chargeCustom(Player player, Item item1, int usedSlot, Item item2, int withSlot, int maxCharges) {
		if (Tournament.insideArenaWithSpawnedGear(player)) {
			player.sendMessage("Can't do that here.");
			return;
		}
		
		
		ItemDefinition def1 = ItemDefinition.forId(item1.getId());
		if (def1 == null) {
			player.sendMessage("Invalid item: " + item1.getId());
			return;
		}

		ItemDefinition def2 = ItemDefinition.forId(item2.getId());
		if (def2 == null) {
			player.sendMessage("Invalid item: " + item2.getId());
			return;
		}
		
		ChargeInfo cInfo = null;
		Item chargeWith = null;
		Item toCharge = null;
		if (def1.isChargeClassItem()) {
			chargeWith = item2;
			toCharge = item1;
			cInfo = def1.getChargeInfo();
		} else if (def2.isChargeClassItem()) {
			chargeWith = item1;
			toCharge = item2;
			cInfo = def2.getChargeInfo();
		}
		
		if (cInfo == null) {
			player.sendMessage("Messed up somewhere...");
			return;
		}
		
		int chargedId = cInfo.isChargedItem() ? toCharge.getId() : cInfo.getChargedId();
		
		if (!cInfo.isChargedItem()) {
			if (player.getItems().deleteItem(toCharge))
				player.getItems().addItem(chargedId, 1);
		}
		
		if (player.getCharges(chargedId) >= maxCharges) {
			player.sendMessage("max charges reached");
			return;
		}
		
		int convertedMax = maxCharges * GP_MULT;
		
		int amount = player.getCharges(chargedId) * GP_MULT;
		
		amount = Misc.addOrMaxInt(amount, chargeWith.getAmount());
		
		if (amount > convertedMax) {
			amount = convertedMax;
		}
		
		/*int leftOver =*/ player.addCharge(chargedId, amount / GP_MULT, maxCharges);
		player.getItems().deleteItem2(chargeWith.getId(), amount);
		
	}
	
	@Override
	public void onRemoveClick(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().removeItem(itemId, itemSlot))
				player.getPA().resetAutocast();
	}
	
	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		player.sendMessage("Trident Charges: " + player.getCharges(itemId));
	}
	
}
