package plugin.item.trident;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.content.player.combat.magic.SpellsData;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class TridentOfTheSwampPlugin extends Plugin implements ItemClickExtension, ItemOnItemExtension, EquipmentClickExtension {
	
	public static final int CHARGED_ITEM_ID = 30348;
	public static final int UNCHARGED_ITEM_ID = 30349;
	
	private static final int MAX_CHARGES = 2500;

	@Override
	public void onInit() {
		map(ItemClickExtension.class, CHARGED_ITEM_ID);
		map(ItemClickExtension.class, UNCHARGED_ITEM_ID);
		
		map(EquipmentClickExtension.class, CHARGED_ITEM_ID);
		map(EquipmentClickExtension.class, UNCHARGED_ITEM_ID);
		
		map(ItemOnItemExtension.class, UNCHARGED_ITEM_ID, BlowPipeCharges.ZULRAH_SCALES);
		map(ItemOnItemExtension.class, BlowPipeCharges.ZULRAH_SCALES, UNCHARGED_ITEM_ID);
		
		map(ItemOnItemExtension.class, CHARGED_ITEM_ID, BlowPipeCharges.ZULRAH_SCALES);
		map(ItemOnItemExtension.class, BlowPipeCharges.ZULRAH_SCALES, CHARGED_ITEM_ID);
	}
	
	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().wearItem(itemId, itemSlot)) {
			player.setAutoCastSpell(SpellsData.TRIDENT_OF_THE_SWAMP);
		}
	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ITEM_ID:
			player.sendMessage("Trident Charges: " + player.getCharges(itemId));
			break;
		case UNCHARGED_ITEM_ID:
			player.getDialogueBuilder()
			.sendStatement("To charge, you must use Zulrah Scales on the Trident.", "One Scale gives you one charge.")
			.execute();
			break;
		}
	}
	
	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		if (!player.getStopWatch().checkThenStart(1)) {
			return;
		}
		switch (itemId) {
		case CHARGED_ITEM_ID:
			//uncharge
			ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), true);
			break;
		case UNCHARGED_ITEM_ID:
			//dismantle
			break;
		}
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
		case CHARGED_ITEM_ID:
			//uncharge
			ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), true);
			break;
		case UNCHARGED_ITEM_ID:
			//dismantle
			break;
		}
		
		Item item = new Item(UNCHARGED_ITEM_ID, player.playerItemsN[itemSlot], itemSlot);
		FifthItemAction.DROP_ITEM.init((Client)player, item);
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		int maxCharges = MAX_CHARGES;
		if (player.getDonatedTotalAmount() > 0) {
			maxCharges += player.getDonatedTotalAmount() * 5;
		}
		ChargedItems.chargeItems(player, used, with, maxCharges);
	}
	
	@Override
	public void onRemoveClick(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().removeItem(itemId, itemSlot))
				player.getPA().resetAutocast();
	}
	
	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		player.sendMessage("Trident Charges: " + player.getCharges(itemId));
	}
	
}
