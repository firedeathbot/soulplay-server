package plugin.item;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.in.ItemAction2;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class DragonBoneNecklacePlugin extends Plugin implements ItemClickExtension {

	public static final int ID = 30332;

	@Override
	public void onInit() {
		map(ItemClickExtension.class, ID);
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		if (ItemAction2.wearItemCentral(player, itemId, itemSlot)) {
			player.dragonBoneNecklaceTimer.startTimer(15);
			player.pulse(() -> {
				player.sendMessageSpam("Your " + ItemDefinition.getName(itemId) + " is now active.");
			}, 15);
		}
	}

}
