package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class VialPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 229);
	}
	
	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		player.toggleBreakVials();
	}
}
