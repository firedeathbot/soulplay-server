package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class HamonisedNightmareStaff extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 124423);
	}

	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getItems().freeSlots() < 1) {
			player.sendMessage("Not enough space in your inventory.");
			return;
		}

		player.getItems().replaceItemSlot(124422, itemSlot);
		player.getItems().addItem(124511, 1);
	}

}
