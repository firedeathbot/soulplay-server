package plugin.item;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class EffigyPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 18778);
		map(ItemClickExtension.class, 18779);
		map(ItemClickExtension.class, 18780);
		map(ItemClickExtension.class, 18781);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.getDoubleExpLength() >= 1) {
			player.sendMessage("You can't investigate " + ItemDefinition.getName(itemId) + " with double experience being active.");
			return;
		}

		player.getDialogueBuilder().reset();
		player.getDialogueBuilder().sendStatement("You clean off the dust off of the Ancient effigy.",
				"The relic begins to make some sort of weird noises.",
				"I think there may be something inside here.").sendAction(() -> {
					if (itemId == 18778) {
						player.getDialogueBuilder().sendStatement("This will require at least a level of 91 in one of the two skills",
								"to investigate. After investigation it becomes nourished,",
								"rewarding 15,000 experience in the skill used.");
					} else if (itemId == 18779) {
						player.getDialogueBuilder().sendStatement("This will require at least a level of 93 in one of the two skills",
								"to investigate. After investigation it becomes sated,",
								"rewarding 20,000 experience in the skill used.");
					} else if (itemId == 18780) {
						player.getDialogueBuilder().sendStatement("This will require at least a level of 95 in one of the two skills",
								"to investigate. After investigation it becomes gordged,",
								"rewarding 25,000 experience in the skill used.");
					} else if (itemId == 18781) {
						player.getDialogueBuilder().sendStatement("This will require at least a level of 97 in one of the two skills",
								"to investigate. After investigation it provides 30,000 ",
								"experience in the skill used and, then crumbles to dust,", "leaving behind a dragonkin lamp.");
					}
					player.getDialogueBuilder().sendAction(() -> {
						if (player.getEffigy() == 0) {
							player.setEffigy(1 + Misc.random(6));
						}

						switch (player.getEffigy()) {
							case 1:
								player.getDialogueBuilder().sendOption(
										"Crafting", () -> handleEffigies(player, itemId, Skills.CRAFTING), 
										"Agility", () -> handleEffigies(player, itemId, Skills.AGILITY));
								break;
							case 2:
								player.getDialogueBuilder().sendOption(
										"Runecrafting", () -> handleEffigies(player, itemId, Skills.RUNECRAFTING), 
										"Thieving", () -> handleEffigies(player, itemId, Skills.THIEVING));
								break;
							case 3:
								player.getDialogueBuilder().sendOption(
										"Cooking", () -> handleEffigies(player, itemId, Skills.COOKING), 
										"Firemaking", () -> handleEffigies(player, itemId, Skills.FIREMAKING));
								break;
							case 4:
								player.getDialogueBuilder().sendOption(
										"Farming", () -> handleEffigies(player, itemId, Skills.FARMING), 
										"Fishing", () -> handleEffigies(player, itemId, Skills.FISHING));
								break;
							case 5:
								player.getDialogueBuilder().sendOption(
										"Fletching", () -> handleEffigies(player, itemId, Skills.FLETCHING), 
										"Woodcutting", () -> handleEffigies(player, itemId, Skills.WOODCUTTING));
								break;
							case 6:
								player.getDialogueBuilder().sendOption(
										"Herblore", () -> handleEffigies(player, itemId, Skills.HERBLORE), 
										"Prayer", () -> handleEffigies(player, itemId, Skills.PRAYER));
								break;
							case 7:
								player.getDialogueBuilder().sendOption(
										"Smithing", () -> handleEffigies(player, itemId, Skills.SMITHING), 
										"Mining", () -> handleEffigies(player, itemId, Skills.MINING));
								break;
						}
					});
				}).execute();
	}

	public static void handleEffigies(Player player, int itemId, int skillId) {
		if (!player.miscTimer.complete()) {
			return;
		}

		player.miscTimer.startTimer(1);
		switch (itemId) {
			case 18778:
				if (player.getSkills().getSkillBoostable(skillId) >= 91) {
					if (player.getItems().deleteItemInOneSlot(itemId, 1)) {
						player.getItems().addItem(18779, 1);
						player.getPA().addSkillXP(1500, skillId);
						player.startAnimation(Animation.EFFIGY_OPEN);
						player.getPA().removeAllWindows();
						player.setEffigy(0);
					}
				} else {
					player.startAnimation(Animation.EFFIGY_OPEN_FAIL);
					player.sendMessage("Your level isn't high enough to open this effigy.");
					player.getPA().closeAllWindows();
				}
				return;
			case 18779:
				if (player.getSkills().getSkillBoostable(skillId) >= 93) {
					if (player.getItems().deleteItemInOneSlot(itemId, 1)) {
						player.getItems().addItem(18780, 1);
						player.getPA().addSkillXP(2000, skillId);
						player.startAnimation(Animation.EFFIGY_OPEN);
						player.getPA().removeAllWindows();
						player.setEffigy(0);
					}
				} else {
					player.startAnimation(Animation.EFFIGY_OPEN_FAIL);
					player.sendMessage("Your level isn't high enough to open this effigy.");
					player.getPA().closeAllWindows();
				}
				return;
			case 18780:
				if (player.getSkills().getSkillBoostable(skillId) >= 95) {
					if (player.getItems().deleteItemInOneSlot(itemId, 1)) {
						player.getItems().addItem(18781, 1);
						player.getPA().addSkillXP(2500, skillId);
						player.startAnimation(Animation.EFFIGY_OPEN);
						player.getPA().removeAllWindows();
						player.setEffigy(0);
					}
				} else {
					player.startAnimation(Animation.EFFIGY_OPEN_FAIL);
					player.sendMessage("Your level isn't high enough to open this effigy.");
					player.getPA().closeAllWindows();
				}
				return;
			case 18781:
				if (player.getSkills().getSkillBoostable(skillId) >= 97) {
					if (player.getItems().deleteItemInOneSlot(itemId, 1)) {
						player.getItems().addItem(18782, 1);
						player.getPA().addSkillXP(3000, skillId);
						player.startAnimation(Animation.EFFIGY_OPEN_FINAL);
						player.startGraphic(Graphic.create(2692));
						player.getPA().removeAllWindows();
						player.setEffigy(0);
					}
				} else {
					player.startAnimation(Animation.EFFIGY_OPEN_FAIL);
					player.sendMessage("Your level isn't high enough to open this effigy.");
					player.getPA().closeAllWindows();
				}
				return;
		}
	}

}
