package plugin.item;

import com.soulplay.game.model.item.definition.MorphingRing;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class MorphRingPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		for (MorphingRing value : MorphingRing.values()) {
			int id = value.getItemId();
			MorphingRing.values.put(id, value);
			map(ItemClickExtension.class, id);
		}
	}

	@Override
	public void onItemOption2(Player player, int itemId, int itemSlot, int interfaceId) {
		MorphingRing ring = MorphingRing.getRingByItemid(itemId);
		if (ring == null) {
			return;
		}

		ring.morph(player);
	}

}
