package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class ResetWildKillcountPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 30378);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		
		player.getDialogueBuilder().reset();
		
		player.getDialogueBuilder().sendStatement("Would you like to reset your wild kill count?", "This item will vanish once activated.")
		.sendOption("Reset wild kill count?", "Yes, reset kill count", ()-> {
			if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
				player.sendMessage("You have reset your kill count.");
				player.KC = 0;
				player.DC = 0;
			}
		}, "No", ()-> {
			
		}).execute();
	}
}
