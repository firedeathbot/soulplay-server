package plugin.item;

import com.soulplay.content.items.useitem.WeaponUpgrades;
import com.soulplay.content.items.useitem.WeaponUpgrades.Weapons;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class WeaponUpgradePlugin extends Plugin implements ItemOnItemExtension {

	@Override
	public void onInit() {
		for (Weapons weapon : Weapons.values()) { 
			map(ItemOnItemExtension.class, weapon.getUseItemID(), weapon.getOnItemID());
			map(ItemOnItemExtension.class, weapon.getOnItemID(), weapon.getUseItemID());
		}
	}
	
	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with,
			int withSlot) {
		WeaponUpgrades.handle(player, used.getId(), with.getId());
	}
}
