package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class BountyTargetScrollPlugin extends Plugin implements ItemClickExtension {

    // items
    private static final int BOUNTY_TARGET_SCROLL = 30267;

    @Override
    public void onInit() {
        map(ItemClickExtension.class, BOUNTY_TARGET_SCROLL);
    }

    @Override
    public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
        if (player.getBountyTargetSpell() > 0) {
        	player.sendMessage("You have already learned this spell.");
        	return;
        }

        player.getDialogueBuilder().sendStatement("The 'Teleport to Bounty Target' requires <col=ff0000>Level 85 Magic</col> and can be", "cast only <col=ff0000>within the Wilderness</col>, while you have a Bounty Target.", "This scroll will be destroyed when it has been read.")
        .sendOption("Yes, I want to learn the spell.", () -> {
            if (player.getItems().playerHasItem(itemId, itemSlot, 1)) {
                player.getItems().deleteItemFromSlot(itemSlot, 1);
                player.setBountyTargetSpell(1);
                player.getDialogueBuilder().sendItemStatement(BOUNTY_TARGET_SCROLL, "You have learned the 'Teleport to Bounty Target' spell.");
            }
        }, "No, I'll keep the scroll.", () -> {}).executeIfNotActive();
    }

	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		player.getDialogueBuilder().sendDestoryItem(itemId).execute();
	}
}
