package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.net.packet.in.ItemAction2;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class DihnBulwarkPlugin extends Plugin implements ItemClickExtension {

	public static final int ITEM_ID = 30094;
	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, ITEM_ID);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (ItemAction2.wearItemCentral(player, itemId, itemSlot)) {
			player.bulwarkDelay.startTimer(8);
		}
	}
	
}
