package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.UpdateFlag;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

import java.util.HashMap;
import java.util.Map;

public class ChangeSkinPlugin extends Plugin implements ItemClickExtension {

    // items
    private static final int GREEN_ITEM = 30179;
    private static final int BLUE_ITEM = 30178;
    private static final int WHITE_ITEM = 30177;
    private static final int RED_ITEM = 30300;
    private static final int PURPLE_ITEM = 30301;
    private static final int BLACK_ITEM = 30302;

    // skin colors
    private static final int GREEN_SKIN = 8;
    private static final int BLUE_SKIN = 9;
    private static final int WHITE_SKIN = 10;
    private static final int RED_SKIN = 11;
    private static final int PURPLE_SKIN = 12;
    private static final int BLACK_SKIN = 13;

    private static final Map<Integer, Integer> map = new HashMap<>();

    @Override
    public void onInit() {
        // bind the item to this plugin class
        map(ItemClickExtension.class, GREEN_ITEM);
        map(ItemClickExtension.class, BLUE_ITEM);
        map(ItemClickExtension.class, WHITE_ITEM);
        map(ItemClickExtension.class, RED_ITEM);
        map(ItemClickExtension.class, PURPLE_ITEM);
        map(ItemClickExtension.class, BLACK_ITEM);

        // bind the item to a skin color
        map.put(GREEN_ITEM, GREEN_SKIN);
        map.put(BLUE_ITEM, BLUE_SKIN);
        map.put(WHITE_ITEM, WHITE_SKIN);
        map.put(RED_ITEM, RED_SKIN);
        map.put(PURPLE_ITEM, PURPLE_SKIN);
        map.put(BLACK_ITEM, BLACK_SKIN);
    }

    @Override
    public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
        final int color = map.getOrDefault(itemId, -1);

        if (color == -1) {
            return;
        }

        player.getDialogueBuilder().sendStatement("This item is <col=ff0000>one</col> time use! Using this dye doesn't", "unlock a skin color only changes your current one.")
        .sendOption("Yes, change my current skin color.", () -> {
            if (player.getItems().playerHasItem(itemId, itemSlot, 1)) {
                player.getItems().deleteItemFromSlot(itemSlot, 1);
                player.playerAppearance[12] = color;
                player.getUpdateFlags().add(UpdateFlag.APPEARANCE);
            }
        }, "No, keep my current skin color.", () -> {}).execute();
    }

}
