package plugin.item;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class DragonHunterLancePlugin extends Plugin implements ItemOnItemExtension {

	public static final int ID = 122978;

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, 111889, 122966);
		map(ItemOnItemExtension.class, 122966, 111889);
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		player.getDialogueBuilder().sendStatement("Are you sure you wish to combine the <col=ff0000>Zamorakian hasta and the", "<col=ff0000>Hydra's claw into Dragon hunter lance?", "<col=ff0000>This can not be reversed.")
			.sendOption("Proceed with the combination.", () -> {
				if (player.getItems().playerHasItem(111889) && player.getItems().playerHasItem(122966)) {
					player.getItems().deleteItem2(111889, 1);
					player.getItems().deleteItem2(122966, 1);
					player.getItems().addItem(ID, 1);
					player.getDialogueBuilder().sendItemStatement(ID, "You successfully combine the Hydra claw and the", "Zamorakian hasta to create the Dragon hunter lance.");
				}
			}, "Cancel.", () -> {}).execute();
	}

}
