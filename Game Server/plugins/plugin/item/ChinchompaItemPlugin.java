package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;

public class ChinchompaItemPlugin extends Plugin implements ItemClickExtension {

	
	@Override
	public void onInit() {
		map(ItemClickExtension.class, 10033);
		map(ItemClickExtension.class, 10034);
		map(ItemClickExtension.class, 30356);
	}
	
	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.underAttackByPlayer()) {
			player.sendMessage("You cannot release them when you are under attack.");
			return;
		}
		player.getDialogueBuilder().sendDestoryItem(itemId).execute();
	}
	
}
