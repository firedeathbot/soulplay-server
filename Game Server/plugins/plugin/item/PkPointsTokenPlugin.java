package plugin.item;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.util.Misc;

public class PkPointsTokenPlugin extends Plugin implements ItemClickExtension {

	@Override
	public void onInit() {
		map(ItemClickExtension.class, 4278);
	}
	
	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.wildLevel > 0) {
			player.sendMessage("You cannot claim pk points in wild.");
			return;
		}
		player.getDialogueBuilder().reset();
		player.getDialogueBuilder()
		.sendStatement("Would you like to Redeem 100 Pk Points?",
				"The token will vanish once you claim the points.")
		.sendOption("Redeem 100 Pk Points?", "Redeem 100 Pk Points", ()-> {
			if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
				player.pkp += 100;
				player.sendMessage("You have claimed 100 Pk Points. You now have " + Misc.formatNumbersWithCommas(player.pkp) + " Pk points.");
			}
		}, "No", ()-> {
			player.getDialogueBuilder().reset();
		})
		.executeIfNotActive();

	}
	
	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		if (player.wildLevel > 0) {
			player.sendMessage("You cannot claim pk points in wild.");
			return;
		}
		
		int count = Math.min(5, player.getItems().countItems(itemId));
		
		int addPkp = count * 100;
		
		String amountStr = Integer.toString(addPkp);
		
		player.getDialogueBuilder().reset();
		player.getDialogueBuilder()
		.sendStatement("Would you like to Redeem " + amountStr + " Pk Points?",
				"The token will vanish once you claim the points.")
		.sendOption("Redeem Pk Points?", "Redeem " + amountStr + " Pk Points", ()-> {
			if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, count)) {
				player.pkp += addPkp;
				player.sendMessage("You have claimed " + amountStr + " Pk Points. You now have " + Misc.formatNumbersWithCommas(player.pkp) + " Pk points.");
			}
		}, "No", ()-> {
			player.getDialogueBuilder().reset();
		})
		.executeIfNotActive();
		
	}
	
}
