package plugin.item;

import com.soulplay.content.items.clickitem.FifthItemAction;
import com.soulplay.game.model.item.ChargedItems;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.EquipmentClickExtension;
import com.soulplay.plugin.extension.ItemClickExtension;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class TomeOfFirePlugin extends Plugin
		implements ItemClickExtension, ItemOnItemExtension, EquipmentClickExtension {

	public static final int CHARGED_ID = 120714;
	public static final int EMPTY_ID = 120716;
	public static final int PAGES_ID = 120718;
	private static final int MAX_CHARGES = 20_000;
	private static final int CHARGES_PER_PAGE = 20;

	@Override
	public void onInit() {
		map(ItemOnItemExtension.class, PAGES_ID, EMPTY_ID);
		map(ItemOnItemExtension.class, EMPTY_ID, PAGES_ID);
		map(ItemOnItemExtension.class, PAGES_ID, CHARGED_ID);
		map(ItemOnItemExtension.class, CHARGED_ID, PAGES_ID);

		map(ItemClickExtension.class, CHARGED_ID);
		map(ItemClickExtension.class, EMPTY_ID);

		map(EquipmentClickExtension.class, CHARGED_ID);
		map(EquipmentClickExtension.class, EMPTY_ID);
	}

	@Override
	public void onItemOption1(Player player, int itemId, int itemSlot, int interfaceId) {
	}

	@Override
	public void onOperateClick(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
			case CHARGED_ID:
			case EMPTY_ID:
				checkCharges(player);
				break;
		}
	}

	@Override
	public void onItemOption3(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
			case CHARGED_ID:
			case EMPTY_ID:
				checkCharges(player);
				break;
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		ChargedItems.chargeItems(player, used, with, MAX_CHARGES);
	}

	private static void checkCharges(Player player) {
		int charges = player.getCharges(CHARGED_ID);
		if (charges == 0) {
			player.sendMessage("Your Tome of Fire has no charges.");
		} else if (charges == 1) {
			player.sendMessage("Your Tome of Fire has 1 charge.");
		} else {
			player.sendMessage("Your Tome of Fire has " + player.getCharges(CHARGED_ID) + " charges.");
		}
	}

	@Override
	public void onItemOption4(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
			case CHARGED_ID: {
				player.getDialogueBuilder().sendOption("Add or remove pages", "Add pages", () -> {
					charge(player, itemId, itemSlot);
				}, "Remove pages", () -> {
					ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), true);
				}, "Cancel", () -> {}).execute();
				break;
			}
			case EMPTY_ID: {
				charge(player, itemId, itemSlot);
				break;
			}
		}
	}

	private static void charge(Player player, int itemId, int itemSlot) {
		int pagesIndex = player.getItems().getItemSlot(PAGES_ID);
		if (pagesIndex == -1) {
			player.sendMessage("You don't have any pages to charge with.");
			return;
		}

		ChargedItems.chargeItems(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), new Item(PAGES_ID, player.playerItemsN[pagesIndex], pagesIndex), MAX_CHARGES);
	}

	@Override
	public void onItemOption5(Player player, int itemId, int itemSlot, int interfaceId) {
		switch (itemId) {
			case CHARGED_ID:
				ChargedItems.unchargeItem(player, new Item(itemId, player.playerItemsN[itemSlot], itemSlot), true);
				break;
			case EMPTY_ID:
				Item item = new Item(itemId, player.playerItemsN[itemSlot], itemSlot);
				FifthItemAction.DROP_ITEM.init((Client)player, item);
				break;
		}
	}

}
