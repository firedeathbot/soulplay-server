package plugin.item;

import com.soulplay.content.items.clickitem.ItemOnItemEnum;
import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;

public class ItemOnItemPlugin extends Plugin implements ItemOnItemExtension {
	
	@Override
	public void onInit() {
		for (ItemOnItemEnum data : ItemOnItemEnum.values()) {
			map(ItemOnItemExtension.class, data.getItemId1(), data.getItemId2());
			map(ItemOnItemExtension.class, data.getItemId2(), data.getItemId1());
		}
	}

	@Override
	public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
		ItemOnItemEnum data = ItemOnItemEnum.values.get(used.getId());
		if (data == null) {
			player.sendMessage("Nothing interesting happens.");
			return;
		}

		if (data.isIrreversible()) {
			player.getDialogueBuilder().sendStatement("Are you sure you wish to combine the these items?", "<col=ff0000>This can not be reversed.")
			.sendOption("Proceed with the combination.", () -> {
				if (player.getItems().playerHasItem(data.getItemId1()) && player.getItems().playerHasItem(data.getItemId2())) {
					player.getItems().deleteItem2(data.getItemId1(), 1);
					player.getItems().deleteItem2(data.getItemId2(), 1);
					player.getItems().addItem(data.getProductId(), 1);
					player.sendMessage("You combine the two materials to create a " + ItemDefinition.getName(data.getProductId()) + ".");
				}
			}, "Cancel.", () -> {}).execute();
		} else {
			if (player.getItems().playerHasItem(data.getItemId1()) && player.getItems().playerHasItem(data.getItemId2())) {
				player.getItems().deleteItem2(data.getItemId1(), 1);
				player.getItems().deleteItem2(data.getItemId2(), 1);
				player.getItems().addItem(data.getProductId(), 1);
				player.sendMessage("You combine the two materials to create a " + ItemDefinition.getName(data.getProductId()) + ".");
			}
		}
	}

}
