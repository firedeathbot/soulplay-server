package plugin.minigames.lms;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class RopeOnRockPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension {

	private static final int ROPE = 954;
	private static final int ROCK = 29102;
	
	private static final Animation START_ANIM = new Animation(2910, 20);
	
	@Override
	public void onInit() {
		map(ItemOnObjectExtension.class, ROPE, ROCK);
		map(ObjectClickExtension.class, ROCK);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		ropeSwingRock(player, object.getId(), object.getX(), object.getY(), player.getZ(), -1);
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		ropeSwingRock(player, objectID, objectX, objectY, player.getZ(), itemSlot);
	}
	
	public static void ropeSwingRock(Player p, int objectId, int oX, int oY, int oZ, int slot) {
		
		if (!properLocation(p)) {
			return;
		}
		
		if (slot == -1) {
			slot = p.getItems().getItemSlot(ROPE);
		}
		if (slot == -1) {
			p.sendMessage("You need a rope to swing across.");
			return;
		}
		
		if (!p.isInLmsArena())
			return;
		
		if (p.dynObjectManager == null) {
			return;
		}
		
		GameObject o = p.dynObjectManager.getObject(objectId, oX, oY, oZ);
		
		if (o == null)
			return;
		
		Misc.faceObject(p, oX, oY, o.getSizeY());
		
		Direction dir = Direction.getLogicalDirection(p.getCurrentLocation(), o.getLocation());
		
		p.startAnimation(START_ANIM);
		
		p.lockMovement(8);
		p.lockActions(8);
		p.getMovement().hardResetWalkingQueue();
		
		CycleEventHandler.getSingleton().addEvent(p, new CycleEvent() {
			int timer = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				if (timer == 1) {
					p.getMovement().setTempMovementAnim(776); //Animation.getOsrsAnimId(6993) swimming in water anim
				}
				if (timer >= 3) {
					container.stop();
					p.resetAnimations();
					ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*5, dir.getStepY()*5, 5, 0, dir).setCustomMovePlayerTick(4);
					p.setForceMovement(mask);
					p.performingAction = false;
					p.getMovement().revertMovementAnims();
				}
				timer++;
			}
		}, 1);
		
	}
	
	public static boolean properLocation(Player p) {
		return (p.getX() == 1865 && p.getY() == 3789) || (p.getX() == 1871 && p.getY() == 3787);
	}
	
}
