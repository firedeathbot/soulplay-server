package plugin.minigames.lms;

import com.soulplay.content.minigames.lastmanstanding.LmsRewards;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class LastManStandingPlugin extends Plugin implements ButtonClickExtension, ObjectClickExtension {
	
	@Override
    public void onInit() {
		map(ObjectClickExtension.class, 29063);
		map(ObjectClickExtension.class, 129063);
    }
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (player.objectHintPos == 2)
			player.getPacketSender().createObjectHints(3333, 3333, 100, -1);
		if (!RegionClip.objectExists(object.getId(), object.getX(), object.getY(), player.getZ(), 10))
			return;
		if(LmsRewards.checkRewards(player)) {
			player.sendMessage("You claim your LMS rewards.");
		} else {
			player.sendMessage("You don't have any rewards in this chest.");
		}
	};
	
	@Override
    public void onClick(Player player, int button, int component) {
		
    }

}
