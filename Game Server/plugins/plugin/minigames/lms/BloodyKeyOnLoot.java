package plugin.minigames.lms;

import com.soulplay.content.minigames.lastmanstanding.LmsArenaObjects;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.util.Misc;

public class BloodyKeyOnLoot extends Plugin implements ItemOnObjectExtension {
	
	@Override
	public void onInit() {
		int[] keys = { LmsArenaObjects.bloodierKey, LmsArenaObjects.bloodyKey };
		for (int key : keys) {
			map(ItemOnObjectExtension.class, key, LmsArenaObjects.CHEST_OBJ);
			map(ItemOnObjectExtension.class, key, LmsArenaObjects.CHEST_OPEN_OBJ);
			map(ItemOnObjectExtension.class, key, LmsArenaObjects.DRAWER_OBJ);
			map(ItemOnObjectExtension.class, key, LmsArenaObjects.DRAWER_OPEN_OBJ);
			map(ItemOnObjectExtension.class, key, LmsArenaObjects.CUPBOARD_OBJ);
			map(ItemOnObjectExtension.class, key, LmsArenaObjects.CUPBOARD_OPEN_OBJ);
		}
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		if (!player.isInLmsArena())
			return;
		
		if (player.dynObjectManager == null) {
			return;
		}
		
		GameObject o = player.dynObjectManager.getObject(objectID, objectX, objectY, player.getZ());
		
		if (o == null)
			return;
		
		Misc.faceObject(player, objectX, objectY, o.getSizeY());
		
		LmsArenaObjects.lootWithKey(player);
		
	}

}
