package plugin.minigames.lms;

import com.soulplay.content.minigames.lastmanstanding.Lisa;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnNpcExtension;
import com.soulplay.plugin.extension.NpcClickExtension;

public class LisaPlugin extends Plugin implements NpcClickExtension, ItemOnNpcExtension {

	@Override
	public void onInit() {
		map(NpcClickExtension.class, 5000);
		map(ItemOnNpcExtension.class, 5000);
	}
	
	@Override
	public void onNpcOption1(Player player, NPC npc) {
		Lisa.firstClick(player, npc);
	}
	
	@Override
	public void onNpcOption2(Player player, NPC npc) {
		Lisa.secondClick(player, npc);
	}
	
	@Override
	public void onItemOnNpc(Player player, NPC npc, int itemId, int slot) {
		Lisa.itemOnLisa(player, itemId, slot);
	}

}
