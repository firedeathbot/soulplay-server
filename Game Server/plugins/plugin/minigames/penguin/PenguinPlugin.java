package plugin.minigames.penguin;

import com.soulplay.content.minigames.penguin.Penguin;
import com.soulplay.content.minigames.penguin.PenguinData;
import com.soulplay.content.minigames.penguin.PenguinHandler;
import com.soulplay.content.minigames.penguin.PenguinUtil;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class PenguinPlugin extends Plugin implements NpcClickExtension {

	@Override
	public void onInit() {
		for (PenguinData p : PenguinData.values) {
			map(NpcClickExtension.class, p.getNpcId());
		}
	}

	@Override
	public void onNpcOption1(Player player, NPC npc) {
		for (Penguin penguin : PenguinHandler.PENGUINS) {
			if (npc == penguin.getNpc()) {
				PenguinUtil.spyOn(penguin, (Client) player);
				return;
			}
		}
	}
}
