package plugin.minigames.penguin;

import com.soulplay.content.minigames.penguin.PenguinUtil;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.NpcClickExtension;

public class ChuckNpcPlugin extends Plugin implements NpcClickExtension {

    @Override
    public void onInit() {
        map(NpcClickExtension.class, 8678);
    }

    @Override
    public void onNpcOption1(Player player, NPC npc) {
        player.getDialogueBuilder().sendPlayerChat("Hi chuck, i wanted to talk to you about the penguins spies.")
                .sendOption("I've found more penguins.", () -> {
                    player.getDialogueBuilder().sendNpcChat("Hmm they move quickly, this was unanticipated.")
                            .sendPlayerChat("I have " + player.penguinPoints + " Penguin Points(s)");

                            if (player.penguinPoints >= 5) {
                                player.getDialogueBuilder().sendNpcChat("Well done, you have enough penguin points to claim a reward!")
                                        .sendAction(() -> ((Client)player).getShops().openShop(38));
                            } else {
                                player.getDialogueBuilder().sendNpcChat("Come back when you have at least 5 points", "then you can claim a reward!");
                            }

                }, "I'm having trouble finding the penguins; can i have a hint?", () -> {
                    player.getDialogueBuilder().sendNpcChat("Hmm, i've had some news of their movements.", "I have some locations they might be at.")
                            .sendOption("Yes please.", () -> {
                            	PenguinUtil.generateHint((Client)player);
                            }, "No thanks.", () -> {

                            });
                }, "I want to claim my reward.", () -> {
                    ((Client)player).getShops().openShop(38);
                }, "What do i need to do again?", () -> {
                    player.getDialogueBuilder().sendNpcChat("You humans do have a short attention span, don't you?")
                            .sendNpcChat("The penguins have dispatched spies all over the world.",
                                    "I need brave adventurers to find them and report their",
                                    "locations to me, as I cannot leave this place.").sendNpcChat("Whenever you spot a penguin, spy on it.",
                            "They're well trained and will change their positions", " every day, so keep your eyes peeled.").sendNpcChat("So, what should I do after I've spied on them?")
                            .sendNpcChat("Report back here and I will reward you for your efforts.",
                                    "I will count up the Penguin Points you have earned",
                                    " and you may choose cash or experience reward.");
                }).execute();
    }

    @Override
    public void onNpcOption2(Player player, NPC npc) {
        ((Client)player).getShops().openShop(38);
    }
}
