package plugin.minigames.inferno;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CaveExitPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 30283);
	}

	@Override
	public void onObjectOption1(Player player, GameObject o) {
		player.getDialogueBuilder().sendOption("Leave inferno?", "Yes.", () -> {
			leaveSafe(player);
		}, "No.", () -> {
		}).execute();
	}

	@Override
	public void onObjectOption2(Player player, GameObject object) {
		leaveSafe(player);
	}

	private static void leaveSafe(Player player) {
		if (player.infernoManager == null) {
			return;
		}

		player.infernoManager.teleportOut();
	}

}
