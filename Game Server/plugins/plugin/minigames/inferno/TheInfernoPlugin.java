package plugin.minigames.inferno;

import com.soulplay.cache.ItemDef;
import com.soulplay.content.minigames.inferno.InfernoManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class TheInfernoPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 30352);
	}

	@Override
	public void onObjectOption1(Player player, GameObject o) {
		if (player.infernoManager != null) {
			return;
		}

		int tries = Variables.INFERNO_TRIES.getValue(player);

		player.getDialogueBuilder().sendStatement("You currently have <col=ff0000>" + (tries == 1 ? "1 try" : tries + " tries") + "</col> on The Inferno.")
		.sendOption("Challenge", () -> {
			player.getDialogueBuilder().sendOption("Enter inferno?", "Yes.", () -> {
				if (player.infernoManager != null) {
					return;
				}

				if (player.summoned != null) {
					player.sendMessage("You can't bring a follower inside The Inferno.");
					return;
				}

				int thisTries = Variables.INFERNO_TRIES.getValue(player);
				if (thisTries <= 0) {
					player.sendMessage("You don't have tries left. Recharge to continue.");
					return;
				}

				player.getVariables()[Variables.INFERNO_TRIES.getCode()]--;
				InfernoManager inferno = new InfernoManager(player);
				inferno.copy();
				inferno.teleportPlayer();
			}, "No, too afraid to die.", () -> {
			}).executeIfNotActive();
		}, "Recharge", () -> {
			int capeCount = player.getItems().getItemAmount(23639);
			if (capeCount <= 0) {
				player.sendMessage("You don't have any " + ItemDef.forID(23639).name + " to sacrifice.");
				return;
			}

			int thisTries = Variables.INFERNO_TRIES.getValue(player);
			for (int i = 0; i < capeCount; i++) {
				if (player.getItems().deleteItemInOneSlot(23639, 1)) {
					player.getVariables()[Variables.INFERNO_TRIES.getCode()]++;
				}
			}

			int triesDelta = Variables.INFERNO_TRIES.getValue(player) - thisTries;
			if (triesDelta == 1) {
				player.sendMessage("You've gained 1 try on The Inferno.");
			} else {
				player.sendMessage("You've gained " + triesDelta + " tries on The Inferno.");
			}
		}).execute();
	}

}
