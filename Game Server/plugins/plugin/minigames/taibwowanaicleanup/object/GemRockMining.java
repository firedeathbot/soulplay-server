package plugin.minigames.taibwowanaicleanup.object;

import com.soulplay.content.minigames.taibwowannaicleanup.GemRocks;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.mining.Mining;
import com.soulplay.content.player.skills.mining.Pickaxes;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class GemRockMining extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		GemRocks.init();
		map(ObjectClickExtension.class, 9030);
		map(ObjectClickExtension.class, 9031);
		map(ObjectClickExtension.class, 9032);
	}
	
	@Override
	public void onObjectOption1(Player c, GameObject o) {
		if (c.getItems().freeSlots() == 0) {
			c.sendMessage("Your inventory is full.");
			return;
		}
		final GemRocks g = GemRocks.get(o.getId());
		if (g == null) {
			return;
		}
		if (o == null || !o.isActive()) {
			return;
		}
		mineGemRock(c, o, g);
	}
	
	public static void mineGemRock(final Player c, final GameObject o, final GemRocks g) {
		
		Misc.faceObject(c, o.getX(), o.getY(), o.getSizeY());
		
		final RSObject rso = ObjectManager.getObject(o.getX(), o.getY(), o.getZ());
		
		if (rso == null || !rso.isActive())
			return;
		
		final int vineRootId = rso.getOriginalId() + 4;
		
		final int newId = (g.getNextStageObjectId() == -1 ? vineRootId : g.getNextStageObjectId());
		
		Pickaxes pickaxe = Mining.getPickaxe(c);
		
		if (pickaxe == null) {
			return;
		}
		
		final int ticksReq = 5 + Misc.random(5);
		
		c.startAnimation(pickaxe.getAnimation());
		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			int timer = 0;
			@Override
			public void stop() {
				c.resetAnimations();
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				if (!rso.isActive()) {
					container.stop();
					return;
				}
				timer++;
				
				if (timer > ticksReq) {
					container.stop();
					
					c.getPA().addSkillXP(Mining.applyMiningXpBoosts(c, 20), Skills.MINING);
					
					if (g.getNextStageObjectId() == -1 && Misc.random(2) == 0) {
						if (Misc.random(6) == 1)
							c.getItems().addItem(6571, 1, true); // onyx
						else
							c.getItems().addItem(1631, 1, true); //dragonstone
						c.sendMessage("Your rock had a rare gem!");
					} else {
						c.getItems().addItem(g.getReward(), 1);
					}
					
					RSObject newRSO = new RSObject(newId, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), rso.getOriginalId(), 100);
					ObjectManager.addObject(newRSO);
				}
			}
		}, 1);
	}
	
}
