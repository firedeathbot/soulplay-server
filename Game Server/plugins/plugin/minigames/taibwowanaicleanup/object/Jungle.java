package plugin.minigames.taibwowanaicleanup.object;

import com.soulplay.content.minigames.taibwowannaicleanup.JungleEnum;
import com.soulplay.content.minigames.taibwowannaicleanup.JungleMonsters;
import com.soulplay.content.minigames.taibwowannaicleanup.MacheteEnum;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class Jungle extends Plugin implements ObjectClickExtension {
	
	public static final String MACHETE_MSG = "You need to be holding a machete to cut away this jungle.";
	
	private static final int LIGHT_JUNGLE_FULL = 9010;
	private static final int LIGHT_JUNGLE_LARGE = 9011;
	private static final int LIGHT_JUNGLE_MEDIUM = 9012;
	private static final int LIGHT_JUNGLE_SMALL = 9013;
	
	private static final int MEDIUM_JUNGLE_FULL = 9015;
	private static final int MEDIUM_JUNGLE_LARGE = 9016;
	private static final int MEDIUM_JUNGLE_MEDIUM = 9017;
	private static final int MEDIUM_JUNGLE_SMALL = 9018;
	
	private static final int DESNSE_JUNGLE_FULL = 9020;
	private static final int DENSE_JUNGLE_LARGE = 9021;
	private static final int DENSE_JUNGLE_MEDIUM = 9022;
	private static final int DENSE_JUNGLE_SMALL = 9023;
	
	private static final int GEM_ROCKS_FULL = 9030;
	
	private static final Animation HACK_ANIMATION = new Animation(2382);
	private static final Animation SEARCH_ANIM = new Animation(2388, Misc.serverToClientTick(2));
	private static final Animation SUCCESSFUL_CUT_ANIM = new Animation(2387, Misc.serverToClientTick(2));
	
	private static final int TICKS = 100;

	@Override
	public void onInit() {
		JungleEnum.init();
		MacheteEnum.init();
		map(ObjectClickExtension.class, LIGHT_JUNGLE_FULL);
		map(ObjectClickExtension.class, LIGHT_JUNGLE_LARGE);
		map(ObjectClickExtension.class, LIGHT_JUNGLE_MEDIUM);
		map(ObjectClickExtension.class, LIGHT_JUNGLE_SMALL);
		map(ObjectClickExtension.class, MEDIUM_JUNGLE_FULL);
		map(ObjectClickExtension.class, MEDIUM_JUNGLE_LARGE);
		map(ObjectClickExtension.class, MEDIUM_JUNGLE_MEDIUM);
		map(ObjectClickExtension.class, MEDIUM_JUNGLE_SMALL);
		map(ObjectClickExtension.class, DESNSE_JUNGLE_FULL);
		map(ObjectClickExtension.class, DENSE_JUNGLE_LARGE);
		map(ObjectClickExtension.class, DENSE_JUNGLE_MEDIUM);
		map(ObjectClickExtension.class, DENSE_JUNGLE_SMALL);
	}

	@Override
	public void onObjectOption4(Player c, GameObject o) {
		Jungle.hackJungle(c, o);
	}
	
	
	public static void hackJungle(final Player c, GameObject object) {
	
		final int weaponId = c.playerEquipment[PlayerConstants.playerWeapon];
		
		MacheteEnum machete = MacheteEnum.get(weaponId);

		final int x = object.getX();
		final int y = object.getY();
		final int z = object.getZ();
		
		if (object == null || !object.isActive()) {
			return;
		}
		
		if (c.getTaiBwoCleanup() < 1) {
			c.getDialogueBuilder().sendStatement("I should not be messing around with the village", "without permissions.").execute();
			return;
		}
		
		Misc.faceObject(c, object.getX(), object.getY(), object.getSizeY());
			
		if (machete == null) {
			c.sendMessage(MACHETE_MSG);
			return;
		}
		
		JungleEnum jungle = JungleEnum.get(object.getId());
		
		if (jungle == null) {
			return;
		}
		
		if (c.getPA().freeSlots() < 1) {
			c.sendMessage("Your inventory is full.");
			return;
		}
		
		final int difficulty = (int) Math.floor(jungle.getFavor() * 2 * machete.getDifficultyReduction());
		
		final int gemRarity = 13 - jungle.getFavor();
		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			int timer = 3;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				timer--;
				
				if (c.getPA().freeSlots() < 1) {
					c.sendMessage("Your inventory is full.");
					return;
				}
				
				GameObject o = RegionClip.getGameObject(x, y, z, 10, true);
				if (o == null || !o.isActive()) {
					container.stop();
					return;
				}
				
				JungleEnum jungle = JungleEnum.get(o.getId());
				
				if (jungle == null) {
					container.stop();
					return;
				}
				
				if (timer == 1) {
					if (Misc.random(30) == 1) {
						JungleMonsters.jungleMonsters(c, o.getLocation());
						container.stop();
						return;
					}
					c.startAnimation(HACK_ANIMATION);
				}
				
				if (timer <= 0) {
					if (Misc.random(difficulty) == 1) { // successful
						if (Misc.random(3) == 2) { // successful but not XD
							c.startAnimation(SEARCH_ANIM);
							c.getPA().addSkillXP(1, Skills.WOODCUTTING);
						} else { // real success
							int newId = jungle.getCurrentId()+1;
							int ticks = TICKS;
							if (newId == jungle.getStumpId()) { // add randomevent object id change here
								//TODO: add random event object chance and code
								// newId = randomevent
								if (Misc.random(gemRarity) == 1) {
									newId = GEM_ROCKS_FULL;
									ticks = 300;
								}
							}
							
							RSObject rso = new RSObject(newId, x, y, z, o.getOrientation(), o.getType(), jungle.getOriginalObjectId(), ticks);

							ObjectManager.addObject(rso);
							c.startAnimation(SUCCESSFUL_CUT_ANIM);
							c.getPA().addSkillXP(jungle.getExp(), Skills.WOODCUTTING);
							c.getItems().addItem(jungle.getReward(), 1);
							c.setTaiBwoFavour(c.getTaiBwoFavour() + jungle.getFavor(), true);
						}
						timer = 5;
					} else { // not successful, keep trying mate
						timer = 2;
					}
				}
			}
		}, 1);
		
	}
	
}
