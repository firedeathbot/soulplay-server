package plugin.minigames.taibwowanaicleanup.object;

import com.soulplay.content.minigames.taibwowannaicleanup.JungleEnum;
import com.soulplay.content.minigames.taibwowannaicleanup.MacheteEnum;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class Fence extends Plugin implements ObjectClickExtension {
	
	private static final int ROTTEN_FENCE_SMALL = 9025;
	private static final int ROTTEN_FENCE_MEDIUM = 9026;
	private static final int ROTTEN_FENCE_LARGE = 9027;
	private static final int ROTTEN_FENCE_FULL = 9028;
	
	private static final int LIGHT_THATCH = 6281;
	private static final int MED_THATCH = 6283;
	private static final int DENSE_THATCH = 6285;
	
	
	private static final Animation CUT_SPAR_ANIM = new Animation(2389);
	private static final Animation READY_REPAIR_ANIM = new Animation(2384);
	private static final Animation START_REPAIRING_ANIM = new Animation(2385);
	
	private static final int TICKS = 1600;

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, ROTTEN_FENCE_SMALL);
		map(ObjectClickExtension.class, ROTTEN_FENCE_MEDIUM);
		map(ObjectClickExtension.class, ROTTEN_FENCE_LARGE);
		map(ObjectClickExtension.class, ROTTEN_FENCE_FULL);
	}

	@Override
	public void onObjectOption1(Player c, GameObject o) {
		Fence.repairFence(c, o);
	}
	
	public static void repairFence(final Player c, GameObject object) {
	
		final int weaponId = c.playerEquipment[PlayerConstants.playerWeapon];
		
		MacheteEnum machete = MacheteEnum.get(weaponId);
		
		if (machete == null) {
			c.sendMessage(Jungle.MACHETE_MSG);
			return;
		}
		
		int type = 0;
		
		final int x = object.getX();
		final int y = object.getY();
		final int z = object.getZ();
		
		if (object == null || !object.isActive()) {
			type = 9; // diagonal fence check
			object = RegionClip.getGameObject(x, y, z, type, true);
		}
		
		if (object == null || !object.isActive()) {
			return;
		}
		
		Misc.faceObject(c, object.getX(), object.getY(), object.getSizeY());
			
		JungleEnum fence = JungleEnum.get(object.getId());
		
		if (fence == null) {
			return;
		}
		
		final int typeFinal = type;
		
		c.skillingEvent = CycleEventHandler.getSingleton().addEvent(c, new CycleEvent() {
			int timer = 11;
			@Override
			public void stop() {
			}
			
			@Override
			public void execute(CycleEventContainer container) {
				timer--;
				
				GameObject o = RegionClip.getGameObject(x, y, z, typeFinal, true);
				if (o == null || !o.isActive()) {
					container.stop();
					return;
				}
				
				if (c.getTaiBwoCleanup() < 1) {
					c.getDialogueBuilder().sendStatement("I should not be messing around with the village", "without permissions.").execute();
					return;
				}
				
				JungleEnum fence = JungleEnum.get(o.getId());
				if (fence == null) {
					container.stop();
					return;
				}
				
				int difficulty = 8;
				if (c.getItems().playerHasItem(LIGHT_THATCH)) {
					difficulty = 8;
				} else if (c.getItems().playerHasItem(MED_THATCH)) {
					difficulty = 6;
				} else if (c.getItems().playerHasItem(DENSE_THATCH)) {
					difficulty = 4;
				} else {
					c.sendMessage("You don't have any thatch to repair the fence.");
					container.stop();
				}
				
				if (timer == 9) {
					c.startAnimation(CUT_SPAR_ANIM);
				}
				
				if (timer == 5) {
					c.startAnimation(READY_REPAIR_ANIM);
				}
				
				if (timer == 4) {
					c.startAnimation(START_REPAIRING_ANIM);
				}
				
				int newId = fence.getCurrentId();
				if (timer == 1) {
					if (Misc.random(difficulty) == 1) { // successful
						if (fence.getCurrentId() <= 9028) {
						    newId = fence.getCurrentId() + 1;
						}
						
						RSObject rso = new RSObject(newId, x, y, z, o.getOrientation(), o.getType(),
								fence.getOriginalObjectId(), TICKS);

						if (c.getItems().deleteItem2(LIGHT_THATCH, 1)) {
							c.getPA().addSkillXP(fence.getExp(), Skills.WOODCUTTING);
						} else if (c.getItems().deleteItem2(MED_THATCH, 1)) {
							c.getPA().addSkillXP(42, Skills.WOODCUTTING);
						} else if (c.getItems().deleteItem2(DENSE_THATCH, 1)) {
							c.getPA().addSkillXP(65, Skills.WOODCUTTING);
						} else {
							container.stop();
							return;
						}
						
						ObjectManager.addObject(rso);
						c.setTaiBwoFavour(c.getTaiBwoFavour() + fence.getFavor(), true);
						c.startAnimation(Player.RESET_ANIM);
						
						timer = 10;
						if (newId == 9029) {
							timer = 1;
						}
					} else { // not successful, keep trying mate
						if (Misc.random(10) == 1) {
							timer = 10;
						} else {
						    timer = 2;
						}
					}
				}
				
				if (timer <= 0) {
					c.getDialogueBuilder().sendStatement("You have fixed this fence now, click on it again to reinforce it, or", "select another fence to repair.").execute();
					c.setTaiBwoCleanup(2);
					container.stop();
					return;
				}
			}
		}, 1);
		
	}
	
}
