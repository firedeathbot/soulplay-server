package plugin.object.raids;

import com.soulplay.content.clans.Clan;
import com.soulplay.content.raids.RaidsManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RaidsDoor extends Plugin implements ObjectClickExtension {

	@Override
    public void onInit() {
        map(ObjectClickExtension.class, 29777);
    }

	@Override
    public void onObjectOption1(Player player, GameObject object) {
		if (!RaidsManager.raidsEnabled) {
			player.sendMessage("Raids are currently disabled.");
			return;
		}

		if (player.summoned != null) {
			player.sendMessage("You can't bring pets into raids.");
			return;
		}

		Clan clan = player.getClan();

		if (clan == null) {
			player.sendMessage("You must be in a clan to start a raid.");
			return;
		}

		RaidsManager raidsManager = clan.raidsManager;
		if (raidsManager == null) {
			if (clan.getRank(player.getName()) < Clan.Rank.CAPTAIN) {
				player.sendMessage("You must be Captain or higher in clan to start a raid.");
				return;
			}
			
			//check for existing raid before craiting new one
			boolean raidsInstanceExists = RaidsManager.instanceExists(player.mySQLIndex);
			
			if (raidsInstanceExists) {
				player.sendMessage("The raids instance already exists for your clan.");
				player.sendMessage("Kick the members inside the raid to start a new raid.");
				return;
			}

			raidsManager = new RaidsManager(clan, player);
			raidsManager.generate();

			raidsManager.teleportPlayer(player);

			clan.raidsManager = raidsManager;
		} else {
			raidsManager.teleportPlayer(player);
		}
	}

}
