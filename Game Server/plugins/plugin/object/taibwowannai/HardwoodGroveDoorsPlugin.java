package plugin.object.taibwowannai;

import com.soulplay.content.minigames.taibwowannaicleanup.VillagersDialogue;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class HardwoodGroveDoorsPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 9038);
		map(ObjectClickExtension.class, 9039);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		VillagersDialogue.hardwoodGroveDoors(player);
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject o) {
		VillagersDialogue.payAndEnter(player);
	}
}
