package plugin.object.trapdoors;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

import plugin.object.doors.MetalGateDouble;

public class OpenTrapDoor extends Plugin implements ObjectClickExtension {

	@Override
    public void onInit() {
        map(ObjectClickExtension.class, 1568);
        map(ObjectClickExtension.class, 1569);//metal double door on osrs map
        map(ObjectClickExtension.class, 5131);
        map(ObjectClickExtension.class, 6434);
        map(ObjectClickExtension.class, 26933);
    }
	
	@Override
    public void onObjectOption1(Player player, GameObject o) {
		if (player.getX() >= 6400 && (o.getId() == 1568 || o.getId() == 1569)) {
			MetalGateDouble.openGates(o);
			return;
		}
		RSObject changedObject = ObjectManager.getObject(o.getX(), o.getY(), o.getZ());
		if (changedObject != null) {
			return;
		}
		
		if (o == null || !o.isActive()) {
			return;
		}
		
		final int newId = o.getId() == 1568 ? o.getId() + 2 : o.getId() + 1;
		
		player.startAnimation(827);
		
		RSObject rso = new RSObject(newId, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), o.getId(), 100);
		
		ObjectManager.addObject(rso);
    }
	
}
