package plugin.object;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class StaircaseClimbUpPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 16665);
		map(ObjectClickExtension.class, 116665);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		int moveX = player.getX() >= 6400 ? player.getX() - 6400 : player.getX();
		int moveY = player.getY() - 6400;

		moveX += (object.getDef().xLength(object.getOrientation()) + 1) * Misc.DIRECTION[object.getOrientation()][0];

		moveY += (object.getDef().yLength(object.getOrientation()) + 1) * Misc.DIRECTION[object.getOrientation()][1];

		player.getPA().movePlayer(moveX, moveY, player.getZ());

	}
}
