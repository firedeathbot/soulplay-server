package plugin.object;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class BankObjectPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 126707);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		player.getPA().openUpBank();
	}
	
}
