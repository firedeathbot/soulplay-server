package plugin.object;

import com.soulplay.Server;
import com.soulplay.content.player.TeleportType;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.objects.WildernessObeliskEnum;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class WildernessObelisk extends Plugin implements ObjectClickExtension {

	@Override
    public void onInit() {
		WildernessObeliskEnum.init();
        for (int i = 0, l = WildernessObeliskEnum.values.length; i < l; i++) {
        	WildernessObeliskEnum o = WildernessObeliskEnum.values[i];
        	map(ObjectClickExtension.class, o.getId());
        }
    }
	
	@Override
    public void onObjectOption1(Player player, GameObject o) {
		if (o == null || !o.isActive()) {
			return;
		}
		
		WildernessObeliskEnum obelisk = WildernessObeliskEnum.get(o.getId());
		
		if (active[obelisk.ordinal()]) // instead of checking if object has changed, i just use this
			return;

		WildernessObeliskEnum teleTo = WildernessObeliskEnum.getNextStep(obelisk.ordinal());//WildernessObeliskEnum.values[random];
		
		teleportObelisk(obelisk, teleTo);
		
		changeObjects(obelisk, o);
    }
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		giveOption(player, object);
	}
	
	@Override
	public void onObjectOption3(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		giveOption(player, object);
	}
	
	private void giveOption(Player player, GameObject object) {
		
		if (player.KC < 250) {
			player.sendMessage("You need a Player Kill Count of 250 to use this option.");
			return;
		}
		
		WildernessObeliskEnum obelisk = WildernessObeliskEnum.get(object.getId());

		WildernessObeliskEnum[] values = new WildernessObeliskEnum[5];

		int count = 0;
		for (int i = 0; i < WildernessObeliskEnum.ORDER_VALUES.length; i++) {
			WildernessObeliskEnum obelisk2 = WildernessObeliskEnum.ORDER_VALUES[i];
			if (obelisk == obelisk2) continue;
			values[count] = obelisk2;
			count++;
		}


		player.getDialogueBuilder().reset();

		player.getDialogueBuilder()
		.sendOption("Pick an obelisk to teleport to",
				values[0].getText(), ()-> {
					if (active[obelisk.ordinal()])
						return;

					teleportObelisk(obelisk, values[0]);

					changeObjects(obelisk, object);

				}, values[1].getText(), ()-> {
					if (active[obelisk.ordinal()])
						return;

					teleportObelisk(obelisk, values[1]);

					changeObjects(obelisk, object);

				}, values[2].getText(), ()-> {
					if (active[obelisk.ordinal()])
						return;

					teleportObelisk(obelisk, values[2]);

					changeObjects(obelisk, object);

				}, values[3].getText(), ()-> {
					if (active[obelisk.ordinal()])
						return;

					teleportObelisk(obelisk, values[3]);

					changeObjects(obelisk, object);

				}, values[4].getText(), ()-> {
					if (active[obelisk.ordinal()])
						return;

					teleportObelisk(obelisk, values[4]);

					changeObjects(obelisk, object);

				})
		.execute();
	}
	
	public static void teleportObelisk(final WildernessObeliskEnum obelisk, WildernessObeliskEnum teleTo) {

		final int currentIndex = obelisk.ordinal();
		
		active[currentIndex] = true;
		
		Server.getTaskScheduler().schedule(new Task(TICKS) {
			
			@Override
			protected void execute() {
				this.stop();

				active[currentIndex] = false;
				final Location obLoc = obelisk.getCornerLoc();
				//final int size = WildernessObeliskEnum.values.length-1;
				
				//int random = Misc.random(size);
				//while (random == currentIndex) {
				//	random = Misc.random(size);
				//}
				
				for (int x = 1; x < 4; x++)
					for (int y = 1; y < 4; y++) {
						Server.getStillGraphicsManager().createGfx(obLoc.getX()+x, obLoc.getY()+y, 0, 342, 0, 0);
					}
				
				for (Player player : Server.getRegionManager().getLocalPlayersByLocation(obelisk.getCornerLoc())) {

					if (player.getCurrentLocation().isWithinDistanceNoZ(obelisk.getCenter(), 1)) {
						int xOffset = player.getX() - obLoc.getX();
						int yOffset = player.getY() - obLoc.getY();
						player.getPA().startTeleport(teleTo.getCornerLoc().getX() + xOffset,
								teleTo.getCornerLoc().getY() + yOffset, 0, TeleportType.OBELISK);
						player.sendMessage(MSG);
					}
				}
				
			}
		});
		
	}
	
	private static void changeObjects(final WildernessObeliskEnum obelisk, final GameObject o) {
		ObjectManager.addObject(new RSObject(14825, obelisk.getCornerLoc().getX(),
				obelisk.getCornerLoc().getY(), 0, o.getOrientation(), o.getType(),
				o.getId(), TICKS));
		ObjectManager.addObject(new RSObject(14825, obelisk.getCornerLoc().getX() + 4,
				obelisk.getCornerLoc().getY(), 0, o.getOrientation(), o.getType(),
				o.getId(), TICKS));
		ObjectManager.addObject(new RSObject(14825, obelisk.getCornerLoc().getX(),
				obelisk.getCornerLoc().getY() + 4, 0, o.getOrientation(),
				o.getType(), o.getId(), TICKS));
		ObjectManager.addObject(new RSObject(14825, obelisk.getCornerLoc().getX() + 4,
				obelisk.getCornerLoc().getY() + 4, 0, o.getOrientation(),
				o.getType(), o.getId(), TICKS));
	}
	
	private static final int TICKS = 10;
	
	private static boolean[] active = new boolean[WildernessObeliskEnum.values.length];
	
	private static final String MSG = "Ancient magic teleports you somewhere in the wilderness";
	
}