package plugin.object.rangingguild;

import com.soulplay.content.items.Ammunition;
import com.soulplay.content.items.WeaponType;
import com.soulplay.content.items.useitem.BlowPipeCharges;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Graphic;
import com.soulplay.game.world.entity.Projectile;
import com.soulplay.game.world.entity.Graphic.GraphicType;
import com.soulplay.game.world.packet.GlobalPackets;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class RangingGuildTargetPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 2513);
	}
	

	@Override
	public void onObjectOption1(Player player, GameObject o) {
		if (player.getX() >= 2671 && player.getX() <= 2680 && player.getY() >= 3417 && player.getY() <= 3420) {
			if (player.getRangeWeapon() == null) {
				player.sendMessage("You can't reach that.");
				return;
			}
			
			
			Ammunition ammo = null;
			if (player.getRangeWeapon().getItemId() == BlowPipeCharges.BLOWPIPE_FULL) { // toxic blowpipe cheaphax
				ammo = Ammunition.get(player.getBlowpipeAmmoType());
				if (ammo == null) { // no ammo in blowpipe
					player.sendMessage("Your blowpipe has ran out of ammo.");
					return;
				}
			} else if (player.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerWeapon)
				ammo = Ammunition.get(player.playerEquipment[PlayerConstants.playerWeapon]);
			else if (player.getRangeWeapon().getAmmunitionSlot() == PlayerConstants.playerArrows)
				ammo = Ammunition.get(player.playerEquipment[PlayerConstants.playerArrows]);
			
			if (ammo == null) {
				player.sendMessage("This weapon is not finished! Needs ammo added to database!");
				return;
			}
			
			Projectile projectile = ammo.getProjectile().copy();
			
			final int dist = Misc.distanceToPoint(player.getX(), player.getY(), o.getX(), o.getY());
			
			projectile.setSlope(projectile.getSlope() + dist);
			
			projectile.setStartLoc(player.getCurrentLocation());
			projectile.setEndLoc(Location.create(o.getX(), o.getY(), player.getZ()));
			projectile.setEndDelay(projectile.getEndDelay() + (5*dist));
			
			if (player.getRangeWeapon().getItemId() == 12926) { // toxic blowpipe cheaphax
				projectile.setStartHeight(34);
				projectile.setSlope(0+dist);
			}

			Graphic startGraphic = Graphic.create(ammo.getStartGraphics().getId(), ammo.getStartGraphics().getType());
			
			if (startGraphic.getType() == GraphicType.BOWARROWPULL && player.getRangeWeapon().getWeaponType() == WeaponType.THROWN) {
				startGraphic = Graphic.create(startGraphic.getId(), GraphicType.HIGH);
			}
			
			if (startGraphic != null && !player.getItems().hasEquippedWeapon(12926))
				player.startGraphic(startGraphic);
			
			if (projectile != null)
				GlobalPackets.createProjectile(projectile);
			
		} else {
			player.sendMessage("can't reach that from here");
		}
	}
}
