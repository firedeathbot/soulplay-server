package plugin.object.rangingguild;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RangingGuildLadderPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 2511);
		map(ObjectClickExtension.class, 2512);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		player.faceLocation(o.getX(), o.getY());
		switch (o.getId()) {
		case 2511: // downstairs ladder
			player.getPA().movePlayerDelayed(player.getX(), player.getY(), player.getZ()+2, 828, 1);
			break;
			
		case 2512: // upstairs ladder
			player.getPA().movePlayerDelayed(player.getX(), player.getY(), player.getZ()-2, 828, 1);
			break;
		}
	}
}
