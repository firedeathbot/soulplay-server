package plugin.object.rangingguild;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RangingGuildDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 2514);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		
		if (player.getSkills().getStaticLevel(Skills.RANGED) < 50) {
			player.getDialogueBuilder().sendNpcChat(8723, LEVEL_REQ_MSG).executeIfNotActive();
			return;
		}
		
		final Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), DOOR_LOCATION);
		
		ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX(), dir.getStepY(), 1, 0, dir).addSecondDirection(dir.getStepX(), dir.getStepY(), 1, 0);
		
		player.setForceMovement(mask);
		player.startAnimation(823);
		
		player.getPacketSender().addObjectPacket(-1, o.getX(), o.getY(), 9, 1);
		player.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), 9, 1, 1);

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
				player.getPacketSender().addObjectPacket(-1, o.getX(), o.getY(), 9, 1, 1);
				player.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), 9, 0);
			}
		}, 2);

	}
	
	private static final Location DOOR_LOCATION = Location.create(2658, 3438, 0);
	
	private static final int LEVEL_REQ = 50;
	
	private static final String LEVEL_REQ_MSG = "You require Ranged level "+LEVEL_REQ + " to enter the guild.";
}
