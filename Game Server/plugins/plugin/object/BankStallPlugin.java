package plugin.object;

import com.soulplay.content.items.presets.PresetsInterface;
import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class BankStallPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 26972);
		map(ItemOnObjectExtension.class, 26972);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		player.getPA().openUpBank();
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		player.getPA().openUpBank();
	}
	
	@Override
	public void onObjectOption3(Player c, GameObject object) {
		Misc.faceObject(c, object);
		
		//bank all inventory
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {
				if (c.getItems().addItemToBank(c.playerItems[i] - 1, c.playerItemsN[i])) {
					c.getItems().deleteItemInOneSlot(c.playerItems[i] - 1, i, c.playerItemsN[i]);
				} else {
					// bank full
					c.sendMessage("Bank is full!!!");
					return;
				}
			}
		}
		
		//bank equipment
		c.getItems().bankEquipment();
		c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
				ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
		c.setUpdateEquipment(true, 3);
		c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
	}
	
	@Override
	public void onObjectOption4(Player c, GameObject object) {
		Misc.faceObject(c, object);
		PresetsInterface.open(c);
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		ItemDefinition def = ItemDefinition.forId(itemId);
		if (def == null)
			return;
		
		if (def.isNoted()) {
			for (int i = 0; i < player.playerItems.length; i++)
				if (!player.getItems().unNoteItem(itemId, itemSlot, 1)) {
					break;
				}
			return;
		}
		
		for (int i = 0; i < player.playerItems.length; i++)
			if (player.playerItems[i] == itemId+1)
				if (!player.getItems().switchToNote(itemId, i, 1)) {
					player.sendMessage("You cannot exchange this item into notes.");
					break;
				}
	}
	
}
