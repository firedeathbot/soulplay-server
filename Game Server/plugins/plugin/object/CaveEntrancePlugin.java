package plugin.object;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CaveEntrancePlugin extends Plugin implements ObjectClickExtension {

	private static final int ENTRANCE = 2804;
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, ENTRANCE);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.getPA().movePlayer(8676, 9988, 0);
	}
	
}