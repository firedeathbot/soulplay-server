package plugin.object;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class WheatPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 15506);
		map(ObjectClickExtension.class, 15507);
		map(ObjectClickExtension.class, 15508);
	}

	@Override
	public void onObjectOption2(Player player, GameObject o) {

		if (o == null || !o.isActive())
			return;

		RSObject rso = ObjectManager.getObject(o.getX(), o.getY(), o.getZ());
		if (rso != null)
			return;
		
		Misc.faceObject(player, o.getX(), o.getY(), o.getSizeY());

		if (player.getItems().addItem(1947, 1)) {

			RSObject empty = new RSObject(-1, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), o.getId(), 100);
			ObjectManager.addObject(empty);
			
			player.startAnimation(Animation.PICK_UP);

			if (player.getX() != o.getX() && player.getY() != o.getY())
				player.getPA().playerWalk(o.getX(), o.getX());					
			
			player.sendMessage("You pick some grain.");
		}
	}

}
