package plugin.object;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RedPortalClwPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 38699);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		player.getDialogueBuilder()
		.sendOption("Select your zone.", "Hybrid/NH", ()-> {
			player.getPA().movePlayer(BRID_NH);
		}, "Pure", ()-> {
			player.getPA().movePlayer(PURE);
		}, "Zerker", ()-> {
			player.getPA().movePlayer(ZERKER);
		}, "Veng/DH", ()-> {
			player.getPA().movePlayer(VENG_DH);
		})
		.execute();
	}
	
	private static final Location BRID_NH = Location.create(3007, 5510, 0);
	private static final Location PURE = Location.create(3007, 5510, 4);
	private static final Location ZERKER = Location.create(3007, 5510, 8);
	private static final Location VENG_DH = Location.create(3007, 5510, 12);
}
