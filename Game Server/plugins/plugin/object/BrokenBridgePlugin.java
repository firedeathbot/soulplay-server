package plugin.object;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class BrokenBridgePlugin extends Plugin implements ObjectClickExtension {

	private static final String[] MSG = { "What's this? The bridge is out - I'll need to find", "another way in. I can see the ladder up there coming",
		"out of a hole; there must be a tunnel taht leads here..." };
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 2834);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.getDialogueBuilder().reset();
		player.getDialogueBuilder().sendPlayerChat(MSG).execute();
	}
}
