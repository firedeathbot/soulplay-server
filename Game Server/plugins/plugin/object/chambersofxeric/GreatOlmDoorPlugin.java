package plugin.object.chambersofxeric;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class GreatOlmDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 29879);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (player.getY() > object.getY()) {
			player.sendMessage("You are blocked in.");
			return;
		}
		
		player.getDialogueBuilder()
		.sendPlayerChat("You sense that strange magic will prevent you from", "coming back through.", "Make sure you are prepared for what is ahead.")
		.sendOption("Go through?",
				"Yes", ()-> {
					if (player.raidsManager == null) {
						return;
					}

					if (!player.raidsManager.started()) {
						player.sendMessage("The raid hasn't started yet.");
						return;
					}

					player.raidsManager.greatOlmManager.passGate(player, object, player.raidsManager);
				}, "No", ()-> {
					player.getDialogueBuilder().closeNew();
				})
		.executeIfNotActive();
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		if (player.getY() > object.getY()) {
			player.sendMessage("You are blocked in.");
			return;
		}

		if (player.raidsManager == null) {
			return;
		}

		if (!player.raidsManager.started()) {
			player.sendMessage("The raid hasn't started yet.");
			return;
		}

		player.raidsManager.greatOlmManager.passGate(player, object, player.raidsManager);
	}
	
}
