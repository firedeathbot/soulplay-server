package plugin.object.chambersofxeric;

import com.soulplay.content.raids.RaidsManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class GreatOlmStairs extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 29778);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (player.raidsManager == null) {
			return;
		}

		if (!player.raidsManager.started()) {
			player.sendMessage("The raid hasn't started yet.");
			return;
		}

		RaidsManager.leaveRaidDialog(player);
	}

}