package plugin.object.pollnivneach;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CurtainDoorwayPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 1528); //closed
		map(ObjectClickExtension.class, 1529); //open
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		RSObject changedObject = ObjectManager.getObject(o.getX(), o.getY(), o.getZ());
		if (changedObject != null) {
			changedObject.setTick(0);
			return;
		}
		
		if (o == null || !o.isActive()) {
			return;
		}
		
		final int newId = o.getId() == 1528 ? o.getId() + 1 : o.getId() - 1;
		
		RSObject rso = new RSObject(newId, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), o.getId(), 100);
		
		ObjectManager.addObject(rso);
	}
}
