package plugin.object.lumbridge;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

import plugin.interact.npc.lumbridge.GillieGroatsPlugin;

public class DairyCowPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension {

	public static final int BUCKET = 1925;
	public static final int BUCKET_OF_MILK = 1927;
	public static final int DAIRY_COW = 8689;
	public static final Animation MILKING_ANIM = new Animation(2305, 20);
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, DAIRY_COW);
		map(ItemOnObjectExtension.class, BUCKET, DAIRY_COW);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		Misc.faceObject(player, o.getX(), o.getY(), 1);
		startMilkingCow(player, -1);
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject o) {
		player.sendMessage("Nothing interesting happens.");
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		Misc.faceObject(player, objectX, objectY, 1);
		startMilkingCow(player, itemSlot);
	}
	
	public static void startMilkingCow(Player p, int slot) {
		if (!p.getLockMovement().complete())
			return;
		if (!p.getItems().playerHasItem(BUCKET)) {
			GillieGroatsPlugin.buildChat(p, true);
			return;
		}
		if (slot == -1)
			slot = p.getItems().getItemSlot(BUCKET);
		if (slot == -1) {
			GillieGroatsPlugin.buildChat(p, true);
			return;
		}
		p.startAnimation(MILKING_ANIM);
		p.lockMovement(1);
		p.getItems().replaceItemSlot(BUCKET_OF_MILK, slot);
		
	}
}
