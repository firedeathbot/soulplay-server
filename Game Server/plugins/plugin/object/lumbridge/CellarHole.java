package plugin.object.lumbridge;

import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class CellarHole extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 6898);
		map(ObjectClickExtension.class, 2542);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		
		Misc.faceObject(player, o.getX(), o.getY(), o.getSizeY());
		
		Direction dir = Direction.getNonDiagonal(o.getOrientation()+3);
		
		ForceMovementMask mask = ForceMovementMask.createMask(0, 0, 1, 0, dir).addSecondDirection(dir.getStepX()*3, dir.getStepY()*3, 2, 0);
		
		player.setForceMovement(mask);
		
		player.startAnimation(AgilityAnimations.PIPE_SQUEEZE_ANIM_DELAYED);
		
	}
}
