package plugin.object.lumbridge;

import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class DiningRoomSingleDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 12348);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		if (!QuestIndex.COOKS_ASSISTANT.hasCompletedQuest(player)) {
			player.getDialogueBuilder().sendNpcChat(278, Expression.ANGRY_YELL, DENY_MSG).executeIfNotActive();
			return;
		}
		if (player.getY() <= 3217 && player.getX() != 3207) {
			player.getPA().playerWalk(3207, 3217);
		}

		final Direction dir = player.getY() <= 3217 ? Direction.NORTH : Direction.SOUTH;
		
		player.faceLocation(player.getX(), player.getY() + dir.getStepY());
		
		player.performingAction = true;

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int timer = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				timer++;
				if (timer > 3)
					container.stop();
				
				if (timer == 1) {
					
					ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX(), dir.getStepY(), 1, 0, dir);
					
					player.setForceMovement(mask);
					player.startAnimation(823);
					
					player.getPacketSender().addObjectPacket(-1, o.getX(), o.getY(), 0, 0);
					player.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), 0, 0, 1);
					
				}
				
				
				if (timer == 2) {
					player.getPacketSender().addObjectPacket(-1, o.getX(), o.getY(), 0, 0, 1);
					player.getPacketSender().addObjectPacket(o.getId(), o.getX(), o.getY(), 0, 1);
				}
			}
		}, 1);
	}
	
	private static final String DENY_MSG = "You are not allowed there!";
	
}
