package plugin.object.lumbridge;

import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RFDChestPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 12308);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		player.getPA().openUpBank();
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject o) {
		if (!QuestIndex.COOKS_ASSISTANT.hasCompletedQuest(player))
			return;
		
		player.getShops().openShop(68);
	}
	
	@Override
	public void onObjectOption3(Player player, GameObject o) {
		
		if (!QuestIndex.COOKS_ASSISTANT.hasCompletedQuest(player))
			return;
		
		switch (player.getRFDProgress()) {
		case 0:
			player.getShops().openShop(69);
			break;
		case 1:
			player.getShops().openShop(70);
		case 2:
			player.getShops().openShop(71);
			break;
		case 3:
			player.getShops().openShop(72);
			break;
		case 4:
			player.getShops().openShop(73);
			break;
		case 5:
			player.getShops().openShop(74);
			break;
		case 6:
			player.getShops().openShop(75);
			break;
		
		default: //reset it if it's bugged
			player.getVariables()[Variables.RFD_PROGRESS.getCode()] = 0;
			break;
		}
		
	}
}
