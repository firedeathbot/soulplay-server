package plugin.object.lumbridge;

import com.soulplay.content.minigames.rfd.RecipeForDisaster;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RecipeForDisasterPortalPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 12354); //entrance
		map(ObjectClickExtension.class, 12356); //exit
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		if (o == null)
			return;
		
		switch (o.getId()) {
		case 12354: //entrance
			if (!QuestIndex.COOKS_ASSISTANT.hasCompletedQuest(player))
				break;
			if (Variables.RFD_PROGRESS.getValue(player) == 0) {
				player.getDialogueBuilder()
				.sendNpcChat(882, Expression.WORRIED, WARNING_STR)
				.sendNpcChat(882, WARNING_STR2)
				.sendAction(()-> {
					RecipeForDisaster.enterArena(player);
				})
				.executeIfNotActive();
			} else {
				RecipeForDisaster.enterArena(player);
			}
			break;
			
		case 12356: //exit
			player.getPA().movePlayer(3209, 3218, 0);
			break;
		}
	}
	
	private static final String[] WARNING_STR = {"Trifid Musiq, I must warn you, inside that dimension", 
			"you will have no assistance from the gods, and your",
			"prayers will not work at all!"};
	private static final String[] WARNING_STR2 = {"Take care, I suspect the culinaromancer will not give", 
			"up so easily!"};
}
