package plugin.object.alkharid;

import com.soulplay.content.minigames.clanwars.ClanWarHandler;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ClanWarsPurplePortalPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 126642);
		map(ObjectClickExtension.class, 126643);
		map(ObjectClickExtension.class, 126644);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		ClanWarHandler.clickedPurplePortal(player);
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		ClanWarHandler.clickedPurplePortal(player);
	}
	
}
