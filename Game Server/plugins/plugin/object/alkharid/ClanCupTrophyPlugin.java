package plugin.object.alkharid;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ClanCupTrophyPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 107127);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.getPA().openClanWarsEloScoreboard();
	}
	
	
}
