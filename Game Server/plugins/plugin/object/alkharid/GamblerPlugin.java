package plugin.object.alkharid;

import com.soulplay.content.items.pricechecker.PriceChecker;
import com.soulplay.content.minigames.duel.TradeAndDuel;
import com.soulplay.game.model.item.GameItem;
import com.soulplay.game.model.item.ItemProjectInsanity;
import com.soulplay.game.model.npc.NPC;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnNpcExtension;
import com.soulplay.util.Misc;

import plugin.item.DiceBagPlugin;

public class GamblerPlugin extends Plugin implements ItemOnNpcExtension {

	@Override
	public void onInit() {
		map(ItemOnNpcExtension.class, 2998);
	}
	
	private static final String INVITE_MSG = "Yes, I would like to risk %d %s";
	private static final String ROLL_MSG = "I rolled a %d";
	private static final String ROLL_MSG2 = "The gambler rolled: %d";
	
	@Override
	public void onItemOnNpc(Player player, NPC npc, int itemId, int slot) {
		if (true) {
			player.getDialogueBuilder().sendNpcChat(npc.npcType, "Sorry I've gone bankrupt.").execute();
			return;
		}
		
		String itemName = ItemProjectInsanity.getItemName(itemId);
		if (itemId != 995 /*&& !player.isMod()*/) {
			player.sendMessage("You can only offer Gold.");
			return;
		}
		if (!TradeAndDuel.allowedToTradeItem(player, itemId, true)) {
			return;
		}
		final int amountOffered = player.isMod() ? player.playerItemsN[slot] : player.gamblerGP;
		final int price = Misc.addOrMaxInt(amountOffered, (int)PriceChecker.getPriceLong(itemId));
		if (price > 1_000_000_000) {
			player.sendMessage("You cannot offer more than 1b.");
			return;
		}
		player.getDialogueBuilder()
		.sendNpcChat(npc.npcType, "Would you like to play 55x2?", "If you win, you double up. If you lose, you're a loser.")
		.sendOption(String.format(INVITE_MSG, amountOffered, itemName), ()-> {
			if (amountOffered <= 1_000_000_000) {
				if (player.getItems().deleteItemInOneSlot(itemId, slot, amountOffered)) {
					npc.startGraphic(DiceBagPlugin.DICE_100_GFX);
					npc.startAnimation(DiceBagPlugin.DICE_ANIMATION);
					int roll = Misc.random2(100);
					player.sendMessage(String.format(ROLL_MSG2, roll));
					npc.forceChat(String.format(ROLL_MSG, roll));
					if (roll > 55) {
						player.gamblerGP *= 2;
						player.sendMessage("Congrats! You win!");
						player.getItems().addOrDrop(new GameItem(itemId, amountOffered));
						player.getItems().addOrDrop(new GameItem(itemId, amountOffered));
					} else {
						player.gamblerGP = 1;
						player.sendMessage("Sorry but you lose.");
					}
				} else {
					player.sendMessage("lol");
				}
			} else {
				player.sendMessage("You can only put in less than 1b amount.");
			}
			
		}, "No thank you, I want to keep my items.", ()-> {
			player.getPA().closeAllWindows();
			})
		.execute();
	}

	
}
