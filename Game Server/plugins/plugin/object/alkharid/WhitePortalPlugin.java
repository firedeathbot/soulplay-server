package plugin.object.alkharid;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class WhitePortalPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 38700);
		map(ObjectClickExtension.class, 26645);
		map(ObjectClickExtension.class, 126645);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		switch (o.getId()) {
		
		case 38700: // funpk clan wars
			if (o.getX() == 3006 && o.getY() == 5509) { //red portal exit
				player.getPA().movePlayer(3361+Misc.random(1), 3156, 0);
			} else if (o.getX() == 2814 && o.getY() == 5509) { // white portal zone exit
				player.getPA().movePlayer(3352, 3163+Misc.random(1), 0);
				player.getPA().restorePlayer(false);
			}
			break;
			
		case 26645:
		case 126645:
			player.getPA().movePlayer(2815, 5511, 0);
			break;
			
		}
	}
}
