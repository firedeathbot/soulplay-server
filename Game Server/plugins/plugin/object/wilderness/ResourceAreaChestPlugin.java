package plugin.object.wilderness;

import com.soulplay.Config;
import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class ResourceAreaChestPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 40093);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (object.getLocation().getX() != 3192 || object.getLocation().getY() != 3929 || player.wildLevel <= 0) {
			return;
		}
		
		if (!ResourceArea.canLootChestBox(player)) {
			return;
		}

		Misc.faceObject(player, object);
		if (Config.RUN_ON_DEDI && Config.NODE_ID != 1) {
			player.sendMessage("You can only open up this chest in World 1.");
			return;
		}
		ResourceArea.openChest(player, object);
	}

}
