package plugin.object.wilderness;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class LavaDragonSteppingStonePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 114918);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		
		int yDir1 = 1;
		int yDir2 = 2;
		Direction face = Direction.NORTH;
		if (player.getY() == 3810) {
			yDir1 = -2;
			yDir2 = -1;
			face = Direction.SOUTH;
		}
		final int dir1Final = yDir1;
		final int dir2Final = yDir2;
		final Direction faceFinal = face;
		
		final Location moveLoc = player.getCurrentLocation().transform(0, yDir1+yDir2);
		
		player.lockActions(4);
		player.lockMovement(4);
		
		
		player.faceLocation(object.getLocation());
		
		int stance = player.getMovement().getStandAnim();
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 4) {
					container.stop();
					return;
				}

				switch (ticks) {
				case 1: {
					player.startAnimation(ANIM1);
				}
				break;

				case 2: {
					player.getMovement().changeStance(741);//cheaphax for now, might change later
					ForceMovementMask mask = ForceMovementMask.createMask(0, dir1Final, 1, 0, faceFinal).setCustomSpeed(15, 0);
					player.setForceMovementRaw(mask);
				}
				break;

				case 3: {
					ForceMovementMask mask = ForceMovementMask.createMask(0, dir2Final, 1, 0, faceFinal).setCustomSpeed(15, 0).setNewOffset(player.getX(), player.getY()+dir1Final);
					player.setForceMovementRaw(mask);
				}
				break;

				case 4: {
					player.getMovement().changeStance(stance);
					player.getPA().movePlayer(moveLoc);
					container.stop();
				}
				break;

				}
				
			}
		}, 1);
	}
	
	private static final Animation ANIM1 = new Animation(741, 12);
	
}
