package plugin.object.wilderness;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.map.travel.zone.addon.ScorpiaZone;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class CreviceScorpiaPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 26763);
		map(ObjectClickExtension.class, 126762);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		Misc.faceObject(player, object);
		
		switch (object.getId()) {
		
		case 26763:
		{
			int x = Misc.random(1) + (object.getX() == 9643 ? 3242 : 3232);
			int y = object.getX() == 9632 ? 3950 : object.getX() == 9633 ? 3938 : 3948;
			player.getPA().movePlayerDelayed(x, y, player.getZ(), 844, 1);
			break;
		}
			
		case 126762:
		{
			player.inMultiDynZone = true;
			
			int x = object.getY() == 3951 ? 9632 : (object.getY() == 3949 ? 9643 : 9633);
			int y = object.getY() >= 3949 ? 10351 : 10332;
			player.getPA().movePlayerDelayed(x, y, player.getZ(), 844, 1);

			player.getZones().addDynamicZone(ScorpiaZone.createDynamic());
			break;
		}
		
		}
	}
	
}
