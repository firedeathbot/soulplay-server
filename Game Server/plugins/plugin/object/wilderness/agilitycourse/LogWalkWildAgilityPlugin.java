package plugin.object.wilderness.agilitycourse;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class LogWalkWildAgilityPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 123542);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(object.getLocation());
		
		if (player.getY() != 3945) {
			player.sendMessage("Make sure to stand infront of the log!");
			return;
		}
		
		player.lockActions(10);
		player.lockMovement(10);
		
		int playerRate = Misc.interpolate(100, 267, 1, 99, player.getSkills().getLevel(Skills.AGILITY));
		
		int random = Skills.randomBaseChance();
		
		final boolean failed = random > playerRate;
		
		final Location moveLoc = failed ? FAILED_LOC1 : SUCCESS_LOC;
		
		final int originalStance = player.getMovement().getStandAnim();
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					player.getMovement().changeStance(AgilityAnimations.LOG_WALK.getId());
					break;
				
				case 2:
				{
					if (!failed) {
						player.setForceMovementRaw(SUCCESS_MASK);
					} else {
						player.setForceMovementRaw(MASK_FAIL);
						ticks = 12;
						player.startAnimation(START_FAIL_ANIM);
					}
				}
				break;
				
				case 10: {
					container.stop();
					player.getMovement().changeStance(originalStance);
					if (player.getAgility().wildyCourseStatus == 3) {
						player.getAgility().wildyCourseStatus = 4;
					}
					player.getPA().addSkillXP(20, Skills.AGILITY);

					player.getPA().movePlayer(moveLoc);
				}
				break;
				
				case 16:
				{
					player.startAnimation(AgilityAnimations.LOG_WALK_FALL_ANIM);
					player.getMovement().changeStance(originalStance);
					player.dealTrueDamage(2, 1, null, null);
				}
				break;
				
				case 17:
				{
					player.getPA().movePlayer(moveLoc);
				}
					break;
					
				case 18:
				{
					container.stop();
					player.getPA().walkTo(0, -1);
					player.lockActions(0);
					player.lockMovement(0);
				}
				break;

				}
				
			}
			
		}, 1);
		
	}
	
	private static final Location SUCCESS_LOC = Location.create(2994, 3945, 0);
	
	private static final Location FAILED_LOC1 = Location.create(2998, 10346, 0);
	
	private static final ForceMovementMask SUCCESS_MASK = ForceMovementMask.createMask(-8, 0, 1, 0, Direction.WEST).setCustomSpeed(240, 0);
	
	private static final ForceMovementMask MASK_FAIL = ForceMovementMask.createMask(-4, 0, 1, 0, Direction.WEST).setCustomSpeed(120, 0);
	
	public static final Animation START_FAIL_ANIM = new Animation(763, 150);

}
