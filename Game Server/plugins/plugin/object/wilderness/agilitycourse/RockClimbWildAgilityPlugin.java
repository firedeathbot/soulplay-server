package plugin.object.wilderness.agilitycourse;

import com.soulplay.config.WorldType;
import com.soulplay.content.player.dailytasks.task.TaskType;
import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class RockClimbWildAgilityPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 123640);
	}
	
	private static final ForceMovementMask MASK = ForceMovementMask.createMask(0, -4, 1, 0, Direction.SOUTH).setCustomSpeed(120, 0);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
				
		player.faceLocation(object.getLocation());

		player.lockActions(8);
		player.lockMovement(8);
		
		final int stance = player.getMovement().getStandAnim();

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container2) {
				ticks++;

				if (ticks > 20) {
					container2.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
					player.getMovement().changeStance(AgilityAnimations.ROCK_CLIMB.getId());
					break;

				case 2:
					player.setForceMovementRaw(MASK);
					break;

				case 4:
					player.getMovement().changeStance(stance);
					break;

				case 6: {
					container2.stop();

					if (player.getAgility().wildyCourseStatus < 4) {
						player.getAgility().wildyCourseStatus = 0;
					}
					if (player.getAgility().wildyCourseStatus >= 4) {
						player.getPA().addSkillXP(499,
								Skills.AGILITY);
						if (!WorldType.equalsType(WorldType.SPAWN)) {
							player.getItems().addItem(995, 40000);
						}
						int tokens = 8;
						if (player.playerRights == 11 || player.playerRights == 12) {
							tokens += 3;
						}
						player.rewardAgilityPoints(tokens);
						player.increaseProgress(TaskType.AGILITY, 168);
					} else {
						player.getPA().addSkillXP(50, Skills.AGILITY);
					}
					
					player.getAgility().wildyCourseStatus = 0;

					player.getPA().movePlayer(player.getCurrentLocation().transform(0, -4));
					player.lockActions(0);
					player.lockMovement(0);
				}
				break;


				}

			}

		}, 1);

	}

}
