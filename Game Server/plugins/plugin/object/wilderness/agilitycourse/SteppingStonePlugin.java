package plugin.object.wilderness.agilitycourse;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class SteppingStonePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 123556);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(object.getLocation());
		
		final Direction dir = Direction.WEST;
		
		player.lockActions(12);
		player.lockMovement(12);
		
		int playerRate = Misc.interpolate(100, 267, 1, 99, player.getSkills().getLevel(Skills.AGILITY));
		
		int random = Skills.randomBaseChance();
		
		final boolean failed = random > playerRate;
		
		final Location moveLoc = failed ? player.getCurrentLocation().transform(0, 3) : player.getCurrentLocation().transform(dir.getStepX()*6, dir.getStepY()*6);
		
		ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX(), dir.getStepY(), 1, 0, dir).setCustomSpeed(30, 0).setNewOffset(player.getX(), player.getY());
		
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 20) {
					container.stop();
					return;
				}

				switch (ticks) {
				
				case 1:
				case 3:
				case 5:
				
				case 9:
				case 11:
				{
					player.startAnimation(AgilityAnimations.JUMP_OVER_1TICKS);
					player.setForceMovementRaw(mask);
					mask.transformOffset(dir.getStepX(), dir.getStepY());
				}
				break;
				
				case 7:
				{
					if (!failed) {
						player.startAnimation(AgilityAnimations.JUMP_OVER_1TICKS);
						player.setForceMovementRaw(mask);
						mask.transformOffset(dir.getStepX(), dir.getStepY());
					} else {
						ticks = 15;
						player.startAnimation(AgilityAnimations.FALL_NORTH);
						player.dealTrueDamage(Misc.random(1, 3), 1, null, null);
					}
				}
				break;

				case 12: {
					container.stop();
					if (player.getAgility().wildyCourseStatus == 2) {
						player.getAgility().wildyCourseStatus = 3;
					}
					player.getPA().addSkillXP(20, Skills.AGILITY);

					player.getPA().movePlayer(moveLoc);
				}
				break;
				
				case 16:
				{
					container.stop();
					player.getPA().movePlayer(moveLoc);
					player.lockActions(0);
					player.lockMovement(0);
				}
					break;

				}
				
			}
			
		}, 1);
		
	}

}
