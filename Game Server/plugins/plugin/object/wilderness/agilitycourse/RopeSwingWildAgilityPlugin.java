package plugin.object.wilderness.agilitycourse;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.content.player.skills.agility.AgilityAnimations;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class RopeSwingWildAgilityPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 123132);
	}
	
	private static final ForceMovementMask MASK = ForceMovementMask.createMask(0, 5, 1, 0, Direction.NORTH).setCustomSpeed(40, 0);
	
	private static final Location SUCCESS_LOC = Location.create(3005, 3958, 0);
	
	private static final Location FAILED_LOC = Location.create(3005, 10357, 0);
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		
//		if (true) {
//			object.animate(player.cannonX++);
//			player.sendMessage("animation : " + player.cannonX);
//			return;
//		}
		
		player.startAction(new CycleEvent() {
			
			@Override
			public void stop() {
				player.skillingEvent = null;
			}
			
			@Override
			public void execute(CycleEventContainer cont) {
				
				if (player.getX() != 3005 || player.getY() != 3953) {
					if (!player.getMovement().isMovingOrHasWalkQueue())
						player.getPA().playerWalk(3005, 3953);
					return;
				}
				
				cont.stop();
				
				player.faceLocation(SUCCESS_LOC);
				
				player.lockActions(5);
				player.lockMovement(5);
				
				int playerRate = Misc.interpolate(100, 267, 1, 99, player.getSkills().getLevel(Skills.AGILITY));
				
				int random = Skills.randomBaseChance();
				
				final boolean failed = random > playerRate;
				
				final Location moveLoc = failed ? FAILED_LOC : SUCCESS_LOC;
				
				if (failed) {
					player.getPA().movePlayer(moveLoc);
					player.dealTrueDamage(Misc.random(1, 3), 1, null, null);
					player.sendMessage("You slip and fall to the pit below.");
					player.startAnimation(AgilityAnimations.FALL_BOUNCE);
					return;
				}
				
				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
					int ticks = 0;
					@Override
					public void stop() {
					}

					@Override
					public void execute(CycleEventContainer container2) {
						ticks++;

						if (ticks > 20) {
							container2.stop();
							return;
						}

						switch (ticks) {
						
						case 2:
							player.startAnimation(AgilityAnimations.ROPE_SWING);
							break;
						
						case 3:
						{
							player.setForceMovementRaw(MASK);
							object.animate(83);
						}
						break;
						

						case 5: {
							container2.stop();
							if (player.getAgility().wildyCourseStatus == 1) {
								player.getAgility().wildyCourseStatus = 2;
							}
							player.getPA().addSkillXP(20, Skills.AGILITY);

							player.getPA().movePlayer(moveLoc);
						}
						break;
						

						}
						
					}
					
				}, 1);
				
			}
		}, 1);
		
	}

}
