package plugin.object.wilderness.agilitycourse;

import com.soulplay.content.player.skills.Skills;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.player.ForceMovementMask;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ObstaclePipePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 123137);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.faceLocation(object.getLocation());
		
		if (player.getFreezeTimer() > 0) {
			player.sendMessage("You are frozen.");
			return;
		}

		if ((player.getX() != 3004 && player.getY() != 3937) || (player.getX() != 3004 && player.getY() != 3950)) {
			player.sendMessage("Make sure to stand infront of the pipe!");
			return;
		}
		
		final Direction dir = Direction.getLogicalDirection(player.getCurrentLocation(), object.getLocation());
		
		player.lockActions(10);
		player.lockMovement(10);
		
		final Location moveLoc = player.getCurrentLocation().transform(dir.getStepX(), dir.getStepY() * 13);
		
		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
			int ticks = 0;
			@Override
			public void stop() {
			}

			@Override
			public void execute(CycleEventContainer container) {
				ticks++;

				if (ticks > 10) {
					container.stop();
					return;
				}

				switch (ticks) {
				case 1: {
					
				}
				break;

				case 2: {
					player.startAnimation(ANIM_START);
					ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*3, dir.getStepY()*3, 1, 0, dir).addSecondDirection(dir.getStepX()*7, dir.getStepY()*7, 1, 0).setCustomSpeed(90, 91);
					player.setForceMovementRaw(mask);
				}
				break;

				case 7: {
					player.startAnimation(ANIM_END);
					ForceMovementMask mask = ForceMovementMask.createMask(dir.getStepX()*3, dir.getStepY()*3, 1, 0, dir).setCustomSpeed(90, 0).setNewOffset(player.getX() + dir.getStepX()*10, player.getY() + dir.getStepY()*10);
					player.setForceMovementRaw(mask);
				}
				break;

				case 10: {
					container.stop();
					if (player.getAgility().wildyCourseStatus == 0) {
						player.getAgility().wildyCourseStatus = 1;
					}
					player.getPA().addSkillXP(13, Skills.AGILITY);
					
					player.getPA().movePlayer(moveLoc);
				}
				break;

				}
				
			}
		}, 1);
		
	}

	private static final Animation ANIM_START = new Animation(12457, 0);
	private static final Animation ANIM_END = new Animation(12458, 20);
	
}
