package plugin.object.wilderness;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CorporealCavePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 37928);
		map(ObjectClickExtension.class, 100678);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		switch (object.getId()) {
		case 37928:
			player.getPA().movePlayer(3206, 3681, 0);
			break;
			
		case 100678:
			player.sendMessage("You can't go this way!");
//			player.getPA().movePlayer(2885, 4372, 0);
			break;
		}
	}
}
