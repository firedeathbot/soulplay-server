package plugin.object.wilderness;

import com.soulplay.content.player.wilderness.ResourceArea;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ResourceAreaDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 126760);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		ResourceArea.payGate(player, object);
	}
	
}
