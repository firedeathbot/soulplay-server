package plugin.object.zulandra;

import com.soulplay.content.npcs.impl.bosses.zulrah.Zulrah;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class ZulAndraBoatPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 10068);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		Client c = (Client) player;
		player.getDialogueBuilder().sendOption("Fight Zulrah", ()-> {
			Zulrah.sendToZulrah(c, true);
		}, "I want to pick up my items.", ()-> {
			Zulrah.sendToZulrah(c, false);
		}).executeIfNotActive();
	}
}
