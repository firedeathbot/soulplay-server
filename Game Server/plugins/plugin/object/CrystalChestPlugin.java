package plugin.object;

import com.soulplay.game.model.item.Item;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnItemExtension;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class CrystalChestPlugin extends Plugin implements ItemOnItemExtension, ItemOnObjectExtension, ObjectClickExtension {

    @Override
    public void onInit() {
        map(ObjectClickExtension.class, 172);
        map(ItemOnObjectExtension.class, CRYSTAL_KEY, 172);
        map(ItemOnItemExtension.class, TOOTH_HALF, LOOP_HALF);
        map(ItemOnItemExtension.class, LOOP_HALF, TOOTH_HALF);
    }

    @Override
    public void onItemOnItem(Player player, Item used, int usedSlot, Item with, int withSlot) {
        if (player.getItems().playerHasItem(TOOTH_HALF, 1)
                && player.getItems().playerHasItem(LOOP_HALF, 1)) {
            player.getItems().deleteItemInOneSlot(TOOTH_HALF, 1);
            player.getItems().deleteItemInOneSlot(LOOP_HALF, 1);
            player.getItems().addItem(CRYSTAL_KEY, 1);
        }
    }

    @Override
    public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
        searchChest(player);
    }

    @Override
    public void onObjectOption1(Player player, GameObject o) {
        searchChest(player);
    }

    private static void searchChest(Player player) {
        if (!player.getItems().playerHasItem(CRYSTAL_KEY)) {
            player.sendMessage("The chest is locked");
            return;
        }

        player.getExtraHiscores().incCrystalChestsOpened();
        player.sendMessage("You unlock the chest with your key.");
        player.getItems().deleteItemInOneSlot(CRYSTAL_KEY, 1);
        player.getItems().addItem(995, Misc.random(213800));
        player.getItems().addItem(CHEST_REWARDS[Misc.random(CHEST_REWARDS.length - 1)],1);
        player.getItems().addItem(CHEST_REWARDS1[Misc.random(CHEST_REWARDS1.length - 1)], 1);
        player.startAnimation(OPEN_ANIMATION);
        player.sendMessage("You find some treasure in the chest.");
        player.getAchievement().openCrystalChests();
    }

    public static final Animation OPEN_ANIMATION = new Animation(881);
    private static final int CRYSTAL_KEY = 989;

    private static final int TOOTH_HALF = 985;
    private static final int LOOP_HALF = 987;

    private static final int[] CHEST_REWARDS1 = {1623, 1623, 1623, 1623, 1623,
            1621, 1621, 1621, 1621, 1619, 1619, 1619, 1617, 1617, 1631, 1631,
            6571};

    private static final int[] CHEST_REWARDS = {4224, 4224, 4224, 4212, 4212,
            4212, 18830, 389, 219, 217, 1513, 3040, 2440, 2444, 2436, 2442,
            1969, 451, 2361, 18830, 389, 219, 217, 1513, 3040, 2440, 2444, 2436,
            2442, 1969, 451, 2361, 18830, 389, 219, 217, 1513, 3040, 2440, 2444,
            2436, 2442, 1969, 451, 2361, 2631, 13107, 13109, 13111, 13113,
            13115, 10398, 2633, 2635, 2637, 10400, 10402, 10408, 10410, 10412,
            10414, 10404, 10406, 7390, 7392, 7394, 7396, 9629, 7372, 7370, 7374,
            7376, 7378, 7380, 7382, 7384, 2607, 2609, 2611, 2613, 2615, 2617,
            2619, 2621, 2599, 2601, 2603, 2605, 2623, 2625, 2627, 2629, 2583,
            2585, 2587, 2589, 2591, 2593, 2595, 2597, 10858};

}
