package plugin.object.edgeville;

import com.soulplay.content.event.randomevent.WishingWell;
import com.soulplay.content.items.useitem.BucketWaterFilling;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;

public class WishingWellPlugin extends Plugin implements ItemOnObjectExtension {

	@Override
	public void onInit() {
		map(ItemOnObjectExtension.class, BucketWaterFilling.EMPTY_BUCKET, WishingWell.WELL_ID);
		map(ItemOnObjectExtension.class, BucketWaterFilling.EMPTY_JUG, WishingWell.WELL_ID);
		map(ItemOnObjectExtension.class, 995, WishingWell.WELL_ID);
		map(ItemOnObjectExtension.class, 10832, WishingWell.WELL_ID);
		map(ItemOnObjectExtension.class, 10833, WishingWell.WELL_ID);
		map(ItemOnObjectExtension.class, 10834, WishingWell.WELL_ID);
		map(ItemOnObjectExtension.class, 10835, WishingWell.WELL_ID);
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		switch (itemId) {
			case BucketWaterFilling.EMPTY_BUCKET:
			case BucketWaterFilling.EMPTY_JUG:
				BucketWaterFilling.waterFilling(player);
				break;
				
			case 995:
				WishingWell.getWell().handleGoldDialogue(player);
				break;
				
			case 10832:
				WishingWell.getWell().handleBulgBagDialogue(player, itemId, 500000000);
				break;
			case 10833:
				WishingWell.getWell().handleBulgBagDialogue(player, itemId, 1000000000);
				break;
			case 10834:
				WishingWell.getWell().handleBulgBagDialogue(player, itemId, 1500000000);
				break;
			case 10835:
				WishingWell.getWell().handleBulgBagDialogue(player, itemId, 2000000000);
				break;
		}
	}
	
}
