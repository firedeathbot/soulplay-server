package plugin.object.tob;

import com.soulplay.content.tob.TobManager;
import com.soulplay.content.tob.rooms.ChestData;
import com.soulplay.content.tob.rooms.ChestRoom;
import com.soulplay.content.tob.rooms.TobRewardsInterface;
import com.soulplay.content.tob.rooms.TobRoom;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.model.item.Inventory;
import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class TobChest extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, ChestRoom.NORMAL_CHEST);
		map(ObjectClickExtension.class, ChestRoom.NORMAL_CHEST_ARROW);
		map(ObjectClickExtension.class, ChestRoom.PURPLE_CHEST);
		map(ObjectClickExtension.class, ChestRoom.PURPLE_CHEST_ARROW);
		map(ObjectClickExtension.class, ChestRoom.OPENED_CHEST);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		TobManager tobManager = player.tobManager;
		if (tobManager == null || !tobManager.started()) {
			return;
		}

		TobRoom currentRoom = tobManager.currentRoom;
		if (currentRoom == null || !(currentRoom instanceof VerzikRoom)) {
			return;
		}

		ChestRoom chestRoom = (ChestRoom) currentRoom.getNextRoom();

		ChestData chestData = chestRoom.findChestForPlayer(player);
		if (chestData == null) {
			player.sendMessage("You contain no loot.");
			return;
		}

		if (chestData.getChestObject() != object) {
			player.sendMessage("This isn't your chest to interact with.");
			return;
		}

		Misc.faceObject(player, object.getX(), object.getY(), object.getSizeY());
		switch (object.getId()) {
			case ChestRoom.OPENED_CHEST:
				TobRewardsInterface.openRefresh(player, chestData.getRewardsToClaim().get((long) player.mySQLIndex));
				return;
			default:
				player.pulse(() -> {
					player.startAnimation(ChestRoom.OPEN_CHEST_ANIM);
					tobManager.objectManager.transformObjectNoClippingChange(chestData.getChestObject(), ChestRoom.OPENED_CHEST);
					Inventory inventory = chestData.getRewardsToClaim().get((long) player.mySQLIndex);
					TobRewardsInterface.openRefresh(player, inventory);

					if (chestData.isJackpot()) {
						int itemId = inventory.get(0).getId();
						String itemName = ItemDefinition.getName(itemId);
						PlayerHandler.broadcastMessageLocalWorld(player.getNameSmartUp() + " has received " + Misc.aOrAn(itemName) + " <col=ff0000>" + itemName + "</col> in a Theatre of Blood.");

						final String partyMessage = player.getNameSmartUp() + " found something special: <col=ff0000>" + ItemDefinition.getName(itemId);
						tobManager.party.executeForMember(member -> {
							member.sendMessage(partyMessage);
						});
					}
				});
				return;
		}
	}

}
