package plugin.object.tob;

import com.soulplay.content.tob.TobManager;
import com.soulplay.content.tob.rooms.ChestData;
import com.soulplay.content.tob.rooms.ChestRoom;
import com.soulplay.content.tob.rooms.VerzikRoom;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class TobThrone extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 132738);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		TobManager tobManager = player.tobManager;
		if (tobManager == null || !(tobManager.currentRoom instanceof VerzikRoom)) {
			return;
		}

		VerzikRoom tobRoom = (VerzikRoom) tobManager.currentRoom;
		if (tobRoom == null) {
			return;
		}

		if (!tobRoom.isCompleted()) {
			player.sendMessage("You must first defeat The Verzik before collecting your treasures.");
			return;
		}

		ChestRoom nextRoom = (ChestRoom) tobRoom.getNextRoom();
		if (nextRoom == null) {
			player.sendMessage("Error. Unable to enter.");
			return;
		}

		player.getPacketSender().setBlackout(2 + 0x80);
		player.performingAction = true;
		player.pulse(() -> {
			player.getPA().movePlayer(nextRoom.enterLocation());
			player.pulse(() -> {
				player.performingAction = false;
				player.getPacketSender().setBlackout(0);

				ChestData chestData = nextRoom.findChestForPlayer(player);
				if (chestData != null) {
					int newId = chestData.isJackpot() ? ChestRoom.PURPLE_CHEST_ARROW : ChestRoom.NORMAL_CHEST_ARROW;
					GameObject myChest = chestData.getChestObject().copy(newId);
					player.getPacketSender().addObjectPacket(myChest);
					player.pulse(() -> {
						myChest.animate(ChestRoom.CHEST_ANIM);
					});
				}
			}, 2);
		}, 3);
	}

}
