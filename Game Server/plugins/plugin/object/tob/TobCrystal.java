package plugin.object.tob;

import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class TobCrystal extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 32996);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.getDialogueBuilder().sendOption("Make sure you've collected all your stuff!", "Leave the Theatre.", () -> {
			TobManager tobManager = player.tobManager;
			if (tobManager == null) {
				return;
			}

			tobManager.leaveTob(player, true);
		}, "Stay in the Theatre.", () -> {
		}).execute();
	}

}
