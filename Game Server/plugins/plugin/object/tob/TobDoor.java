package plugin.object.tob;

import com.soulplay.content.clans.Clan;
import com.soulplay.content.items.itemretrieval.ItemRetrievalManager;
import com.soulplay.content.tob.TobManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class TobDoor extends Plugin implements ObjectClickExtension {

	@Override
    public void onInit() {
        map(ObjectClickExtension.class, 32653);
    }

	@Override
    public void onObjectOption1(Player player, GameObject object) {
		if (!TobManager.tobEnabled) {
			player.sendMessage("Tob is currently disabled.");
			return;
		}

		if (player.summoned != null) {
			player.sendMessage("You can't bring pets into tob.");
			return;
		}

		if (ItemRetrievalManager.hasItems(player, true)) {
			return;
		}

		Clan clan = player.getClan();

		if (clan == null) {
			player.sendMessage("You must be in a clan to start a tob.");
			return;
		}

		TobManager tobManager = clan.tobManager;
		if (tobManager == null) {
			if (clan.getRank(player.getName()) < Clan.Rank.CAPTAIN) {
				player.sendMessage("You must be Captain or higher in clan to start a tob.");
				return;
			}
			
			//check for existing tob before craiting new one
			boolean tobInstanceExists = TobManager.instanceExists(player.mySQLIndex);
			
			if (tobInstanceExists) {
				player.sendMessage("The raids instance already exists for your clan.");
				player.sendMessage("Wait for clan members to finish the raid before starting a new one.");
				return;
			}

			tobManager = new TobManager(clan, player);
			tobManager.generate();
			tobManager.teleportPlayer(player);

			clan.tobManager = tobManager;
		} else {
			tobManager.teleportPlayer(player);
		}
	}
}
