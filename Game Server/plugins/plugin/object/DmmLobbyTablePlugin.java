package plugin.object;

import com.soulplay.content.minigames.ddmtournament.SpawnTable;
import com.soulplay.content.minigames.ddmtournament.Tournament;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class DmmLobbyTablePlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 55220);
		map(ObjectClickExtension.class, 28139);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		switch (o.getId()) {
		case 55220:
		SpawnTable.openTable(player);
		break;
		case 28139:
			//exit portal
			player.getDialogueBuilder().sendOption("Exit", ()-> {
				if (player.getZones().isInDmmLobby()) {
					Tournament.removeFromLobby(player, false);
				}
			}, "Stay", ()-> {
				player.getDialogueBuilder().close();
			}).executeIfNotActive();
			break;
		}
	}
	
}
