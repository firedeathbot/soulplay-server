package plugin.object.pictoris;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class PhoenixLairCavePlugin extends Plugin implements ObjectClickExtension {

	private static final int ENTRANCE = 41900;
	
	private static final int EXIT = 41902;
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, ENTRANCE);
		map(ObjectClickExtension.class, EXIT);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		switch (object.getId()) {
		case ENTRANCE:
			player.getPA().movePlayer(3535, 5186, 0);
		break;
		case EXIT:
			player.sendMessage("What?");
			break;
		}
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		player.getPA().movePlayer(2294, 3626, 0);
	}
}
