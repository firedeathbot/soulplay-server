package plugin.object.christmas;

import java.util.HashMap;
import java.util.Map;

import com.soulplay.Server;
import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.event.CycleEventHandler;
import com.soulplay.game.model.npc.NPCHandler;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class SnowmanPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension {

	private static final int SNOW_BALL = 10501;
	private static final int FULL_SNOWMAN = 28294;
	
	private static final int[] HATS = {11955, 11956, 11957, 11958, 11959};
	
	private static final int[] SNOWMAN = {28294, 28293, 28292, 28291, 28290,
			28289, 28288, 28287, 28286, 28285, 28284, 28283, 28282, 28281,
			28280, 28279, 28278, 28277, 28276, 28275, 28274, 28273, 28272,
			28271, 28270, 28269, 28268, 28267, 28266};
	
	public static Map<Integer, Integer> snowman = new HashMap<>();

	@Override
	public void onInit() {
		for (int snowman : SNOWMAN) {
			map(ObjectClickExtension.class, snowman);
			for (int hat : HATS) {
			    map(ItemOnObjectExtension.class, hat, snowman);
			}
		}
	}
	
	@Override
    public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		Misc.faceObject(player, objectX, objectY, 1);
		RSObject object = ObjectManager.getObjectByNewId(objectID, objectX, objectY, player.getHeightLevel());
		if (object == null) {
			player.sendMessage("null object.");
			return;
		}
		
		if (object.getNewObjectId() != 28294) {
			return;
		}
		
		int npcId = -1;
		switch (itemId) {
			case 11955:
				npcId = 6742;
				break;
			case 11956:
				npcId = 6743;
				break;
			case 11957:
				npcId = 6744;
				break;
			case 11958:
				npcId = 6745;
				break;
			case 11959:
				npcId = 6746;
				break;
		}
		
		if (npcId == -1) {
			return;
		}

		if (player.summoned != null) {
			player.sendMessage("You already have a pet following you!");
			return;
		}
		
		RSObject rso = new RSObject(28266, object.getX(), object.getY(), object.getHeight(), object.getFace(), object.getType(), 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(rso);
		player.summoned = NPCHandler.summonPokemon((Client) player, npcId,
				object.getX(), object.getY(),
				player.getHeightLevel(), 0, 0);
		player.summoned.applyBoss(true);
		player.summoned.forceChat("Thank you for letting me free!");
		player.summoned.isPokemon  = false;
		player.summoned.summoner = (Client) player;
		player.getItems().deleteItemInOneSlot(itemId, 1);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		if (!player.getItems().playerHasItem(SNOW_BALL)) {
			return;
		}
		if (object.getId() == FULL_SNOWMAN) {
			player.sendMessage("You need a snowman hat to add on the snowman.");
			return;
		}

		addSnow(player, object);
	}

	private void addSnow(Player player, GameObject o) {

		if (o == null || !o.isActive()) {
			return;
		}
	
		Misc.faceObject(player, o.getX(), o.getY(), 1);

		if (player.performingAction) {
			return;
		}
		player.performingAction = true;

		CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

			int timer = 2;

			@Override
			public void execute(CycleEventContainer container) {

				if (player.disconnected || player == null) {
					container.stop();
					return;
				}

				if (timer == 2) {
					player.getItems().deleteItemInOneSlot(SNOW_BALL, 1);
					player.startAnimation(7533);
					Server.getStillGraphicsManager().createGfx(o.getX(), o.getY(), 0, 1282, 35, 0);
					player.getMovement().resetWalkingQueue();
					
				}

				timer--;
				if (timer == 0) {
					int key = o.getLocation().toBitPacked();
					Integer amount = snowman.get(key);
					if (amount == null) {
						amount = 0;
					}
					amount++;
					snowman.put(key, amount);
					if (amount > 4) {
						RSObject rso = new RSObject(o.getId() + 1, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), 28266, 500);
						ObjectManager.addObject(rso);
					    snowman.put(key, 0);
					}
					player.setChristmasPoints(player.getChristmasPoints() + 2);
					player.sendMessage("You have " + player.getChristmasPoints() + " Christmas points.");
					container.stop();
				}
			}

			@Override
			public void stop() {
				player.performingAction = false;
			}
		}, 1);
	}

	public static void spawnSnow() {
		RSObject object1 = new RSObject(28266, 3077, 3504, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object1);
		RSObject object2 = new RSObject(28266, 3102, 3501, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object2);
		RSObject object3 = new RSObject(28266, 3096, 3503, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object3);
		RSObject object4 = new RSObject(28266, 3090, 3513, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object4);
		RSObject object5 = new RSObject(28266, 3084, 3495, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object5);
		RSObject object6 = new RSObject(28266, 3197, 3432, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object6);
		RSObject object7 = new RSObject(28266, 3221, 3421, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object7);
		RSObject object8 = new RSObject(28266, 3220, 3438, 0, Misc.random(3), 10, 28266, Integer.MAX_VALUE);
		ObjectManager.addObject(object8);
	}
}
