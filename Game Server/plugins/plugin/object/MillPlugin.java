package plugin.object;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class MillPlugin extends Plugin implements ObjectClickExtension, ItemOnObjectExtension {
	
	private static final int CONFIG_CODE = 695;
	private static final int[] HOPPER = { 24071, 2714};
	
	private static final int GRAIN_ITEM = 1947;
	private static final int MILL1 = 1781;
	private static final int MILL2 = 954;
	private static final int HOPPER_CONTROLS1 = 2718;
	private static final int HOPPER_CONTROLS2 = 24072;
	public static final int EMPTY_POT = 1931;
	public static final int POT_OF_FLOUR = 1933;
	
	private static final Animation HOPPER_ANIM_OSRS = new Animation(Animation.getOsrsAnimId(3571), 1);
	private static final Animation FILL_HOPPER_ANIM = new Animation(3572, 20);

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, MILL1); // mill collect
		map(ItemOnObjectExtension.class, EMPTY_POT, MILL1); // pot on mill
		map(ObjectClickExtension.class, MILL2); // mill collect
		map(ItemOnObjectExtension.class, EMPTY_POT, MILL2); // pot on mill
		map(ObjectClickExtension.class, HOPPER_CONTROLS1); // hopper controls
		map(ObjectClickExtension.class, HOPPER_CONTROLS2); // hopper controls
		
		for (int hopper : HOPPER) {
			map(ItemOnObjectExtension.class, GRAIN_ITEM, hopper);
		}
	}

	@Override
	public void onObjectOption1(Player player, GameObject o) {

		if (o == null || !o.isActive())
			return;
		
		if (ObjectManager.objectInSpotTypeTen(o.getX(), o.getY(), o.getZ())) {
			return;
		}
		
		Misc.faceObject(player, o.getX(), o.getY(), o.getSizeY());

		switch (o.getId()) {
		case MILL1:
		case MILL2:
			fillPotWithFlour(player, -1);
			break;
		case HOPPER_CONTROLS1:
		case HOPPER_CONTROLS2:
			int max = 30;
			if (o.getId() == HOPPER_CONTROLS2)
				max = 35;
			if (player.getMillInfo().getFlourCollectAmount() == max) {
				player.sendMessage("The flour bin is full.");
				break;
			}
			if (player.getMillInfo().isHopperFull()) {
				player.getMillInfo().increaseFlour();
				player.getPacketSender().sendConfig(CONFIG_CODE, 1);
				player.sendMessage("You operate the hopper. The grain slides down the chute.");
				player.getMillInfo().setHopperFull(false);
			} else {
				player.sendMessage("You operate the empty hopper. Nothing interesting happens.");
			}
			o.animate(1729);
			player.startAnimation(new Animation(HOPPER_ANIM_OSRS.getId(), 12));
			RSObject rso = new RSObject(2722, o.getX(), o.getY(), o.getZ(), o.getOrientation(), o.getType(), o.getId(), 5);
			ObjectManager.addObject(rso);
			break;
		}

	}
	
	@Override
    public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		Misc.faceObject(player, objectX, objectY, 1);
		switch (objectID) {
		case MILL1:
		case MILL2:
			fillPotWithFlour(player, itemSlot);
			break;
			default://the rest
				if (player.getMillInfo().isHopperFull()) {
					player.sendMessage("There is already grain in the hopper.");
					return;
				}
				if (player.getItems().deleteItemInOneSlot(itemId, itemSlot, 1)) {
					player.getMillInfo().setHopperFull(true);
					player.startAnimation(FILL_HOPPER_ANIM);
					player.sendMessage("You put the grain in the hopper.");
				}
				break;
		}
	}
	
	public static void fillPotWithFlour(Player player, int slot) {
		if (player.getMillInfo().getFlourCollectAmount() == 0) {
			player.sendMessage("The flour bin is empty.");
			return;
		}
		boolean deletedPot = (slot == -1 ? player.getItems().deleteItem2(EMPTY_POT, 1) : player.getItems().deleteItemInOneSlot(EMPTY_POT, slot, 1));
		if (deletedPot) {
			player.getItems().addItem(POT_OF_FLOUR, 1);
			player.getMillInfo().decreaseFlour();
			if (player.getMillInfo().getFlourCollectAmount() == 0) {
				player.getPacketSender().sendConfig(CONFIG_CODE, 0);
				player.sendMessage("You fill a pot with the last of the flour in the bin.");
			} else {
				player.sendMessage("You fill a pot with flour from the bin.");
			}
		} else {
			player.sendMessage("You need an empty pot to hold the flour in.");
		}
	}
	
}
