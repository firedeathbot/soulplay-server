package plugin.object;

import com.soulplay.game.model.item.ItemConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerConstants;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class BankDepositBoxPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 25937);
		map(ObjectClickExtension.class, 125937);
	}
	
	@Override
	public void onObjectOption1(Player c, GameObject object) {
		Misc.faceObject(c, object);
		
		//bank all inventory
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {
				if (c.getItems().addItemToBank(c.playerItems[i] - 1, c.playerItemsN[i])) {
					c.getItems().deleteItemInOneSlot(c.playerItems[i] - 1, i, c.playerItemsN[i]);
				} else {
					// bank full
					c.sendMessage("Bank is full!!!");
					return;
				}
			}
		}
		
		//bank equipment
		c.getItems().bankEquipment();
		c.getItems().sendWeapon(c.playerEquipment[PlayerConstants.playerWeapon],
				ItemConstants.getItemName(c.playerEquipment[PlayerConstants.playerWeapon]));
		c.setUpdateEquipment(true, 3);
		c.getCombat().getPlayerAnimIndex(c.playerEquipment[PlayerConstants.playerWeapon]);
	}
	
}
