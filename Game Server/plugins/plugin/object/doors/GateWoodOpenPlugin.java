package plugin.object.doors;

import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;

public class GateWoodOpenPlugin extends Plugin implements ObjectClickExtension {

	private static final int LEFT_SIDE = 1551;

	private static final int RIGHT_SIDE = 1553;
	
	public static final int[][] RIDE_SIDE_OFF = { {0, 1}, { 1, 0}, {0, -1}, {-1, 0} };
	
	public static final int[][] OPEN_OFFSET = { {-1,0}, {0, 1}, {1, 0}, {0,-1} };
	public static final int[][] OPEN_OFFSET_OUTSIDE = { {-2,-1}, {-1, 2}, {2, 1}, {1,-2} };
	
	private static final int DELAY = 100;
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, LEFT_SIDE);
		map(ObjectClickExtension.class, RIGHT_SIDE);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		
		RSObject rsO = ObjectManager.getObjectByNewId(o.getId(), o.getX(), o.getY(), o.getZ());
		
		if (rsO != null && rsO.isActive()) {
			if (rsO.getLinkedLoc() == null)
				return;
			
			RSObject rsO2 = ObjectManager.getObject(rsO.getLinkedLoc().getX(), rsO.getLinkedLoc().getY(), player.getZ());
			
			if (rsO2 == null)
				return;
			
			RSObject deleted1 = ObjectManager.getObject(rsO.getOriginalX(), rsO.getOriginalY(), rsO.getHeight());
			RSObject deleted2 = ObjectManager.getObject(rsO2.getOriginalX(), rsO2.getOriginalY(), rsO2.getHeight());
			
			if (deleted1 == null || deleted2 == null)
				return;
			
			rsO.setTick(0);
			rsO2.setTick(0);
			deleted1.setTick(0);
			deleted2.setTick(0);
			return;
		}

		if (o == null || !o.isActive())
			return;
		
		boolean isLeftSide = o.getId() == LEFT_SIDE;
		
		int indexOffset = isLeftSide ? o.getOrientation() : (o.getOrientation() + 2)%4;
		
		int otherX = o.getX() + RIDE_SIDE_OFF[indexOffset][0];
		int otherY = o.getY() + RIDE_SIDE_OFF[indexOffset][1];
		
		GameObject o2 = RegionClip.getGameObject(otherX, otherY, o.getZ(), o.getType(), true);
		
		if (o2 == null) {
			return;
		}
		
		GameObject left = isLeftSide ? o : o2;
		
		GameObject right = isLeftSide ? o2 : o;
		
		RSObject del1 = new RSObject(-1, left.getX(), left.getY(), left.getZ(), left.getOrientation(), left.getType(), left.getId(), DELAY);
		RSObject del2 = new RSObject(-1, right.getX(), right.getY(), right.getZ(), right.getOrientation(), right.getType(), right.getId(), DELAY);
		
		int leftX = left.getX() + OPEN_OFFSET[left.getOrientation()][0];
		int leftY = left.getY() + OPEN_OFFSET[left.getOrientation()][1];
		
		int rightX = right.getX() + OPEN_OFFSET_OUTSIDE[right.getOrientation()][0];
		int rightY = right.getY() + OPEN_OFFSET_OUTSIDE[right.getOrientation()][1];
		
		RSObject rs1 = new RSObject(left.getId()+1, leftX, leftY, left.getZ(), (left.getOrientation()+3)%4, left.getType(), -1, DELAY);
		RSObject rs2 = new RSObject(right.getId()+3, rightX, rightY, right.getZ(), (right.getOrientation()+3)%4, right.getType(), -1, DELAY);
		
		rs1.setLinkedLoc(Location.create(rs2.getX(), rs2.getY(), rs2.getHeight()));
		rs2.setLinkedLoc(Location.create(rs1.getX(), rs1.getY(), rs1.getHeight()));
		
		rs1.setOriginalX(del1.getX());
		rs1.setOriginalY(del1.getY());
		
		rs2.setOriginalX(del2.getX());
		rs2.setOriginalY(del2.getY());
		
		ObjectManager.addObject(del1);
		ObjectManager.addObject(rs1);
		
		ObjectManager.addObject(del2);
		ObjectManager.addObject(rs2);
		
	}
}
