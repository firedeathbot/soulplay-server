package plugin.object.doors;

import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class SingleDoorPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 1530);
		map(ObjectClickExtension.class, 1531);
		map(ObjectClickExtension.class, 1533);
		map(ObjectClickExtension.class, 1534);
		map(ObjectClickExtension.class, 1539);
		map(ObjectClickExtension.class, 1540);
		map(ObjectClickExtension.class, 8695);
		map(ObjectClickExtension.class, 8696);
		map(ObjectClickExtension.class, 11993);
		map(ObjectClickExtension.class, 11994);
		map(ObjectClickExtension.class, 14749);
		map(ObjectClickExtension.class, 14750);
		map(ObjectClickExtension.class, 114749);//osrs wild map
		map(ObjectClickExtension.class, 114750);//osrs wild map
		

		map(ObjectClickExtension.class, 101546);//osrs wild map
		map(ObjectClickExtension.class, 101547);//osrs wild map
		map(ObjectClickExtension.class, 101535);//osrs wild map
		map(ObjectClickExtension.class, 101536);//osrs wild map

		map(ObjectClickExtension.class, 24376);
		map(ObjectClickExtension.class, 24377);
	}

	@Override
	public void onObjectOption1(Player player, GameObject o) {
		if (o == null) {
			return;
		}
		openDoor(player, o);
	}	
	
	public static void openDoor(Player p, GameObject door) {
		final Location loc = door.getLocation();
		
		final boolean openingDoor = door.getDef().itemActions[0].equals("Open");
		
		// check if gate is already open
		RSObject rso = ObjectManager.getObject(loc.getX(), loc.getY(), loc.getZ());
		
		if (rso != null) { // the door is in changed format, so revert it
			
			if (rso.getType() == 9 && p.getX() == rso.getOriginalX() && p.getY() == rso.getOriginalY()) {
				stepAwayFromDiagonalDoor(p, door.getOrientation(), openingDoor);
				p.faceLocation(loc);
			}
			
			RSObject revertDoor = ObjectManager.getObject(rso.getOriginalX(), rso.getOriginalY(), loc.getZ());
			if (revertDoor != null)
				revertDoor.setTick(0);
			
			rso.setTick(0);
			
		} else {
			
			if (p.getZones().isInDmmTourn()) {
				RSObject delete = new RSObject(-1, loc.getX(), loc.getY(), loc.getZ(), door.getOrientation(), door.getType(), door.getId(), TICKS);
				ObjectManager.addObject(delete);
				return;
			}
			
			final boolean diagonalDoor = door.getType() == 9;
			
			final int otherId = openingDoor ? door.getId() + 1 : door.getId() - 1;
			
			int face = openingDoor ? (door.getOrientation() + 1) % 4 : (door.getOrientation() + 3) % 4;
			
			Direction dir = CLOSED_RIGHT_DOOR_BY_FACE[door.getOrientation()]; // default open position on nondiagonal door
			
			if (!openingDoor) { // closing door
				if (diagonalDoor)
					dir = CLOSED_RIGHT_DOOR_BY_FACE[(door.getOrientation() + 2) % 4];
				else
					dir = CLOSED_RIGHT_DOOR_BY_FACE[(door.getOrientation() + 1) % 4];
			} else { // opening
				if (diagonalDoor)
					dir = CLOSED_RIGHT_DOOR_BY_FACE[(door.getOrientation()+1) % 4];
			}

			Location newLoc = loc.transform(dir);
			
			if (diagonalDoor && p.getCurrentLocation().matches(newLoc)) { // should step away, then use object is how osrs does it
				stepAwayFromDiagonalDoor(p, door.getOrientation(), openingDoor);
				p.faceLocation(loc);
			}
			
			RSObject del1 = new RSObject(-1, loc.getX(), loc.getY(), loc.getZ(), door.getOrientation(), door.getType(), door.getId(), TICKS);
			RSObject newDoor = new RSObject(otherId, newLoc.getX(), newLoc.getY(), newLoc.getZ(), face, door.getType(), -1, TICKS);
			newDoor.setOriginalX(loc.getX());
			newDoor.setOriginalY(loc.getY());
			
//			System.out.println(String.format("curFace:%s newFace:%s", door.getOrientation(), face));
//			System.out.println(String.format("curLoc:%s newLoc:%s", door.getLocation(), newLoc));
			
			ObjectManager.addObject(del1);
			ObjectManager.addObject(newDoor);
		}
		
	}
	
	private static final Direction[] CLOSED_RIGHT_DOOR_BY_FACE = { Direction.WEST, Direction.NORTH, Direction.EAST, Direction.SOUTH };
	
	private static final int TICKS = 100;
	
	private static void stepAwayFromDiagonalDoor(Player p, int face, boolean opening) { // runescape's direction walking is diagonal, but maybe i'll add that later
		Location walkTo = p.getCurrentLocation().transform(Direction.get(opening ? (face+3)%4 : face + 4));
		p.getPA().playerWalk(walkTo.getX(), walkTo.getY());
	}
}
