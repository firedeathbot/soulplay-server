package plugin.object.doors;

import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class MetalGateDouble extends Plugin implements ObjectClickExtension {

	@Override
    public void onInit() {
        map(ObjectClickExtension.class, 1596);
        map(ObjectClickExtension.class, 1597);
        
        map(ObjectClickExtension.class, 101727);
        map(ObjectClickExtension.class, 101728);
        
//        map(ObjectClickExtension.class, 1568); //this is trap door? wtf look it up, probably trap door on 317 but gate on osrs
//        map(ObjectClickExtension.class, 1569);
        
        map(ObjectClickExtension.class, 101568);
        map(ObjectClickExtension.class, 101569);
        
        map(ObjectClickExtension.class, 2786);//Gu'Tanoth gate east
        map(ObjectClickExtension.class, 2787);//Gu'Tanoth gate east
        
        map(ObjectClickExtension.class, 2788);//Gu'Tanoth gate west
        map(ObjectClickExtension.class, 2789);//Gu'Tanoth gate west
    }
	
	@Override
    public void onObjectOption1(Player player, GameObject o) {
		if (o == null) {
			return;
		}
		openGates(o);
    }
	
	public static void openGates(GameObject gateOne) {
		final int gateOneId = gateOne.getId();
		
		final boolean leftDoor = gateOneId == 1597 || gateOneId == 1569 || gateOneId == 101728 || gateOneId == 101569 || gateOneId == 2789 || gateOneId == 2787;
		final Location loc1 = gateOne.getLocation();
		
		// check if gate is already open
		RSObject rso = ObjectManager.getObject(loc1.getX(), loc1.getY(), loc1.getZ());
		
		if (rso != null) { // the door is in changed format, so revert it
			Direction dir = CLOSED_RIGHT_DOOR_BY_FACE[(gateOne.getOrientation()+3) & 3];
			Location otherLoc = gateOne.getLocation().transform(dir);
			rso.setTick(0);
			RSObject rso2 = ObjectManager.getObject(otherLoc.getX(), otherLoc.getY(), otherLoc.getZ());
			if (rso2 != null) {
				rso2.setTick(0);
				RSObject rso4 = ObjectManager.getObject(rso2.getOriginalX(), rso2.getOriginalY(), loc1.getZ());
				if (rso4 != null)
					rso4.setTick(0);
			}
			
			RSObject rso3 = ObjectManager.getObject(rso.getOriginalX(), rso.getOriginalY(), loc1.getZ());
			if (rso3 != null)
				rso3.setTick(0);
			
		} else {
			Direction dir = leftDoor ? CLOSED_RIGHT_DOOR_BY_FACE[gateOne.getOrientation()] : CLOSED_RIGHT_DOOR_BY_FACE[(gateOne.getOrientation()+2)&3];
			Location otherLoc = gateOne.getLocation().transform(dir);
			final GameObject object2 = RegionClip.getGameObject(otherLoc.getX(), otherLoc.getY(), otherLoc.getZ(), gateOne.getType(), true);
			
			if (object2 == null)
				return;
			
			final int otherId = object2.getId();
			
			final int face1 = leftDoor ? (gateOne.getOrientation() + 1) & 3 : (gateOne.getOrientation() + 3) & 3;
			final int face2 = (face1 + 2) & 3;

			Location newLoc = loc1.transform(Direction.get(leftDoor ? face2 : face1));
			Location newLoc2 = otherLoc.transform(Direction.get(leftDoor ? face2 : face1));
			RSObject del1 = new RSObject(-1, loc1.getX(), loc1.getY(), loc1.getZ(), gateOne.getOrientation(), gateOne.getType(), gateOne.getId(), TICKS);
			RSObject del2 = new RSObject(-1, otherLoc.getX(), otherLoc.getY(), otherLoc.getZ(), object2.getOrientation(), object2.getType(), object2.getId(), TICKS);
			RSObject newGate1 = new RSObject(gateOneId, newLoc.getX(), newLoc.getY(), newLoc.getZ(), face1, gateOne.getType(), -1, TICKS);
			RSObject newGate2 = new RSObject(otherId, newLoc2.getX(), newLoc2.getY(), newLoc2.getZ(), face2, gateOne.getType(), -1, TICKS);
			newGate1.setOriginalX(loc1.getX());
			newGate1.setOriginalY(loc1.getY());
			newGate2.setOriginalX(otherLoc.getX());
			newGate2.setOriginalY(otherLoc.getY());
			
			ObjectManager.addObject(del1);
			ObjectManager.addObject(del2);
			ObjectManager.addObject(newGate1);
			ObjectManager.addObject(newGate2);
		}
		
	}
	
	private static final Direction[] CLOSED_RIGHT_DOOR_BY_FACE = { Direction.SOUTH, Direction.WEST, Direction.NORTH, Direction.EAST };
	
	private static final int TICKS = 100;
}
