package plugin.object.doors;

import com.soulplay.game.map.clip.region.RegionClip;
import com.soulplay.game.model.objects.DoubleDoorEnum;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.objects.SingleDoubleDoor;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class DoubleDoorPlugin extends Plugin implements ObjectClickExtension {

//	private static final int LEFT_SIDE = 1506;

//	private static final int RIGHT_SIDE = 1508;
	
//	public static final int[][] RIDE_SIDE_OFF = { {0, -1}, {-1, 0}, {0, 1}, {1, 0} };
	
//	public static final int[][] OPEN_OFFSET = { {-1,0}, {0, 1}, {1, 0}, {0,-1} };
	
	private static final int DELAY = 100;
	
	@Override
	public void onInit() {
		DoubleDoorEnum.init();
		for(SingleDoubleDoor door : SingleDoubleDoor.values())
			map(ObjectClickExtension.class, door.getObjectId());
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject o) {
		
		RSObject rsO = ObjectManager.getObjectByNewId(o.getId(), o.getX(), o.getY(), o.getZ());
		
		if (rsO != null && rsO.isActive()) {
			if (rsO.getLinkedLoc() == null) {
				if (player.debug) {
					player.sendMessage("rsO.getLinkedLoc() == null");
				}
				return;
			}
			
			RSObject rsO2 = ObjectManager.getObject(rsO.getLinkedLoc().getX(), rsO.getLinkedLoc().getY(), player.getZ());
			
			if (rsO2 == null) {
				if (player.debug) {
					player.sendMessage("rsO == null");
				}
				return;
			}
			
			RSObject deleted1 = ObjectManager.getObject(rsO.getOriginalX(), rsO.getOriginalY(), rsO.getHeight());
			RSObject deleted2 = ObjectManager.getObject(rsO2.getOriginalX(), rsO2.getOriginalY(), rsO2.getHeight());
			
			if (deleted1 == null || deleted2 == null) {
				if (player.debug) {
					player.sendMessage("deleted1Null:"+ (deleted1 == null) + "  -  deleted2Null:"+(deleted2==null));
				}
				return;
			}
			
			rsO.setTick(0);
			rsO2.setTick(0);
			deleted1.setTick(0);
			deleted2.setTick(0);
			return;
		}

		if (o == null || !o.isActive()) {
			if (player.debug) {
				player.sendMessage("oIsNull:" + (o==null) + " oIsActive:"+o.isActive());
			}
			return;
		}
		
		DoubleDoorEnum doubleDoors = DoubleDoorEnum.get(o.getId());
		
		if (doubleDoors == null) {
			if (player.debug) {
				player.sendMessage("doubleDoors == null");
			}
			return;
		}
		
		boolean isNorthSide = o.getId() == doubleDoors.getNorthDoor().getObjectId();
		
		SingleDoubleDoor door = isNorthSide ? doubleDoors.getNorthDoor() : doubleDoors.getSouthDoor();
		
//		boolean originalIsClosed = objectType == LEFT_SIDE || objectType == RIGHT_SIDE;
		
//		int indexOffset = isNorthSide ? o.getOrientation() : (o.getOrientation() + 2)%4;
		
//		if (!originalIsClosed) {
//			indexOffset = (o.getOrientation() + 3)%4;
//		}
		
		Location otherLoc = door.getOtherLoc(o.getOrientation());
		
		int otherX = o.getX() + otherLoc.getX();//RIDE_SIDE_OFF[indexOffset][0];
		int otherY = o.getY() + otherLoc.getY();//RIDE_SIDE_OFF[indexOffset][1];
		
		//Server.getStillGraphicsManager().createGfx(otherX, otherY, player.getZ(), 5, 0, 0);
		
		GameObject o2 = RegionClip.getGameObject(otherX, otherY, o.getZ(), o.getType(), true);
		
		if (o2 == null) {
			if (player.debug) {
				player.sendMessage("o2 == null");
			}
			return;
		}
		
		GameObject left = isNorthSide ? o : o2;
		
		GameObject right = isNorthSide ? o2 : o;
		
		RSObject del1 = new RSObject(-1, left.getX(), left.getY(), left.getZ(), left.getOrientation(), left.getType(), left.getId(), DELAY);
		RSObject del2 = new RSObject(-1, right.getX(), right.getY(), right.getZ(), right.getOrientation(), right.getType(), right.getId(), DELAY);
		
//		int leftOffsetIndex = originalIsClosed ? left.getOrientation() : (left.getOrientation()+1)%4;
//		int rightOffsetIndex = originalIsClosed ? right.getOrientation() : (right.getOrientation()+3)%4;
		
		Location leftOff = doubleDoors.getNorthDoor().getMoveOffset(left.getOrientation());
		Location rightOff = doubleDoors.getSouthDoor().getMoveOffset(right.getOrientation());
		
		int leftX = left.getX() + leftOff.getX();//OPEN_OFFSET[leftOffsetIndex][0];
		int leftY = left.getY() + leftOff.getY();//OPEN_OFFSET[leftOffsetIndex][1];
		
		int rightX = right.getX() + rightOff.getX();//OPEN_OFFSET[rightOffsetIndex][0];
		int rightY = right.getY() + rightOff.getY();//OPEN_OFFSET[rightOffsetIndex][1];
		
		int newRot1 = left.getOrientation() + doubleDoors.getNorthDoor().getTransformOrientationOff();//originalIsClosed ? (left.getOrientation()+1)%4 : (left.getOrientation()+3)%4;
		int newRot2 = right.getOrientation() + doubleDoors.getSouthDoor().getTransformOrientationOff();//// originalIsClosed ? (right.getOrientation()+3)%4 : (left.getOrientation()+3)%4;
		
		RSObject rs1 = new RSObject(doubleDoors.getNorthDoor().getTranformId(), leftX, leftY, left.getZ(), newRot1, left.getType(), -1, DELAY);
		RSObject rs2 = new RSObject(doubleDoors.getSouthDoor().getTranformId(), rightX, rightY, right.getZ(), newRot2, right.getType(), -1, DELAY);
		
		rs1.setLinkedLoc(Location.create(rs2.getX(), rs2.getY(), rs2.getHeight()));
		rs2.setLinkedLoc(Location.create(rs1.getX(), rs1.getY(), rs1.getHeight()));
		
		rs1.setOriginalX(del1.getX());
		rs1.setOriginalY(del1.getY());
		
		rs2.setOriginalX(del2.getX());
		rs2.setOriginalY(del2.getY());
		
		ObjectManager.addObject(del1);
		ObjectManager.addObject(rs1);
		
		ObjectManager.addObject(del2);
		ObjectManager.addObject(rs2);
		
	}
	
}
