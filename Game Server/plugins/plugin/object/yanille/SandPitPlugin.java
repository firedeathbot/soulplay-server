package plugin.object.yanille;

import com.soulplay.game.event.CycleEvent;
import com.soulplay.game.event.CycleEventContainer;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.entity.Animation;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ItemOnObjectExtension;

public class SandPitPlugin extends Plugin implements ItemOnObjectExtension {

	public static final int OBJECT_ID = 10814;
	public static final int OBJECT_ID1 = 4373;
	public static final int OBJECT_ID2 = 2645;
	
	public static final Animation ANIM = new Animation(895);
	
	@Override
	public void onInit() {
		map(ItemOnObjectExtension.class, OBJECT_ID);
		map(ItemOnObjectExtension.class, OBJECT_ID1);
		map(ItemOnObjectExtension.class, OBJECT_ID2);
	}
	
	@Override
	public void onItemOnObject(Player player, int objectID, int objectX, int objectY, int itemId, int itemSlot) {
		switch (itemId) {
		case 1925:
			int count = player.getItems().getItemCount(itemId);
			
			player.startAction(new CycleEvent() {
				int ticks = 0;
				@Override
				public void stop() {
					player.skillingEvent = null;
				}
				
				@Override
				public void execute(CycleEventContainer container) {
					if (ticks >= count) {
						container.stop();
						return;
					}
					if (player.getItems().deleteItem2(itemId, 1)) {
						player.getItems().addItem(1783, 1);
						player.startAnimation(ANIM);
					} else
						container.stop();
					ticks++;
					
				}
			}, 1);
			break;

		case 1783:
			player.sendMessage("The bucket is already filled to the brim with sand.");
			break;
			
			default:
				player.sendMessage("Nothing interesting happens.");
				break;
		}
	}
}
