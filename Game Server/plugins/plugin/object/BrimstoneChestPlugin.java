package plugin.object;

import com.soulplay.game.model.item.ItemDefinition;
import com.soulplay.game.model.item.RangeItem;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class BrimstoneChestPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 134660);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		int brimstoneKeySlot = player.getItems().getItemSlot(123083);
		if (brimstoneKeySlot == -1) {
			player.getDialogueBuilder().sendStatement("You need a Brimstone key to open this chest.").execute();
			return;
		}

		RangeItem item = rollTable();
		int rollAmount = Misc.random(item.getMinAmount(), item.getMaxAmount());

		if (!player.getItems().addItem(item.getId(), rollAmount, true)) {
			player.getDialogueBuilder().sendStatement("Not enough space in your inventory.").execute();
			return;
		}

		player.getItems().deleteItemFromSlot(brimstoneKeySlot, 1);
		player.sendMessage("<col=008800>" + player.getNameSmartUp() + " received a drop: " + Misc.format(rollAmount) + " x " + ItemDefinition.getName(item.getId()));
		player.startAnimation(536);
	}

	public static RangeItem rollTable() {
		RangeItem reward;
		if (Misc.randomBoolean(200)) {
			reward = new RangeItem(122731, 1);
		} else if (Misc.randomBoolean(10000)) {
			reward = new RangeItem(123047, 1);
		} else if (Misc.randomBoolean(10000)) {
			reward = new RangeItem(123050, 1);
		} else if (Misc.randomBoolean(10000)) {
			reward = new RangeItem(123053, 1);
		} else if (Misc.randomBoolean(10000)) {
			reward = new RangeItem(123056, 1);
		} else if (Misc.randomBoolean(10000)) {
			reward = new RangeItem(123059, 1);
		} else {
			int roll = Misc.random(60);
			if (roll <= 5) {
				reward = new RangeItem(1618, 25, 35);
			} else if (roll <= 10) {
				reward = new RangeItem(1620, 25, 35);
			} else if (roll <= 15) {
				reward = new RangeItem(454, 300, 500);
			} else if (roll <= 20) {
				reward = new RangeItem(995, 50_000_000, 150_000_000);
			} else if (roll <= 24) {
				reward = new RangeItem(445, 100, 200);
			} else if (roll <= 28) {
				reward = new RangeItem(11237, 50, 200);
			} else if (roll <= 31) {
				reward = new RangeItem(441, 350, 500);
			} else if (roll <= 34) {
				reward = new RangeItem(1164, 2, 4);
			} else if (roll <= 37) {
				reward = new RangeItem(1128, 1, 2);
			} else if (roll <= 40) {
				reward = new RangeItem(1080, 1, 2);
			} else if (roll <= 43) {
				reward = new RangeItem(390, 80, 160);
			} else if (roll <= 45) {
				reward = new RangeItem(452, 10, 15);
			} else if (roll <= 47) {
				reward = new RangeItem(2354, 300, 500);
			} else if (roll <= 49) {
				reward = new RangeItem(1514, 120, 160);
			} else if (roll <= 51) {
				reward = new RangeItem(11232, 40, 160);
			} else if (roll <= 52) {
				reward = new RangeItem(5289, 2, 4);
			} else if (roll <= 56) {
				reward = new RangeItem(5316, 2, 4);
			} else if (roll <= 57) {
				reward = new RangeItem(5304, 3, 5);
			} else if (roll <= 58) {
				reward = new RangeItem(5300, 3, 5);
			} else if (roll <= 59) {
				reward = new RangeItem(5295, 3, 5);
			} else {
				reward = new RangeItem(7937, 3000, 6000);
			}
		}

		return reward;
	}

}
