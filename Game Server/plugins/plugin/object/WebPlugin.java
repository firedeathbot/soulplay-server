package plugin.object;

import com.soulplay.content.player.skills.fletching.Fletching;
import com.soulplay.game.model.item.definition.EquipmentBonuses;
import com.soulplay.game.model.objects.RSObject;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.ObjectManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;
import com.soulplay.util.Misc;

public class WebPlugin extends Plugin implements ObjectClickExtension {

	@Override
	public void onInit() {
		map(ObjectClickExtension.class, 733);
		map(ObjectClickExtension.class, 100733);
	}

	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.startAnimation(393);
		if (success(player)) {
			ObjectManager.addObject(new RSObject(object.getId()+1, object.getX(), object.getY(), object.getZ(), object.getOrientation(), object.getType(), object.getId(), 50 + Misc.random(10)));
			player.sendMessageSpam("You successfully slash the web.");
		}
	}
	
	private static final boolean success(Player player) {
		int bonus = player.getItems().playerHasItem(Fletching.KNIFE, 1) ? 50 : 0;
		bonus += player.getPlayerBonus()[EquipmentBonuses.ATTACK_SLASH.getId()];
		
//		int chance = Misc.interpolateSafe(7, 0, 0, 150, bonus);
//		player.sendMessage("chance:1/" + chance);
		
		return Misc.randomBoolean(Misc.interpolateSafe(7, 0, 0, 150, bonus));
	}
}
