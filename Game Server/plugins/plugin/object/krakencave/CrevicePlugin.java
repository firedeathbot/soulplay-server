package plugin.object.krakencave;

import com.soulplay.content.npcs.impl.bosses.kraken.KrakenInstanceManager;
import com.soulplay.game.LocationConstants;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.dialog.Expression;
import com.soulplay.game.world.GameObject;
import com.soulplay.game.world.entity.Location;
import com.soulplay.game.world.map.travel.zone.impl.KrakenZone;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CrevicePlugin extends Plugin implements ObjectClickExtension {

	private static final int ENTER = 537;
	private static final int EXIT = 538;
	
	private static final Location ENTERED_LOC = Location.create(8680, 10022);
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, ENTER);
		map(ObjectClickExtension.class, EXIT);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		switch (object.getId()) {
		case ENTER: // enter public kraken
			player.getZones().addDynamicZone(KrakenZone.createPublicZone());
			player.getPA().movePlayer(ENTERED_LOC);
			break;
		case EXIT:
			player.getPA().movePlayer(LocationConstants.KRAKEN_ENTRANCE);
			break;
		}
	}
	
	@Override
	public void onObjectOption2(Player player, GameObject object) {
		//instanced kraken
		KrakenInstanceManager.enterInstance(player);
	}
	
	@Override
	public void onObjectOption3(Player player, GameObject object) {
		player.getDialogueBuilder()
		.sendNpcChat(7605, Expression.SECRETLY_TALKING, "Psst... What you looking at?")
		.execute();
	}
}
