package plugin.object.krakencave;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.GameObject;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ObjectClickExtension;

public class CaveExitPlugin extends Plugin implements ObjectClickExtension {

	private static final int OBJECT_ID = 30178;
	
	@Override
	public void onInit() {
		map(ObjectClickExtension.class, OBJECT_ID);
	}
	
	@Override
	public void onObjectOption1(Player player, GameObject object) {
		player.getPA().movePlayer(2288, 3607, 0);
	}
	
}