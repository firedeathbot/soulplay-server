package plugin.button.quests;

import com.soulplay.content.player.quests.QuestHandlerTemp;
import com.soulplay.content.player.quests.QuestIndex;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class QuestButtonPlugin extends Plugin implements ButtonClickExtension {

	private static final int BUTTON_START_ID = 242170;
	
    @Override
    public void onInit() {
    	for (int i = 0; i < QuestIndex.VALUES.length; i++) {
            map(ButtonClickExtension.class, BUTTON_START_ID+i);
    	}
    }

    @Override
    public void onClick(Player player, int button, int component) {
        final int buttonIndex = button - BUTTON_START_ID;
        
        QuestIndex quest = QuestIndex.VALUES[buttonIndex];
        
        if (QuestHandlerTemp.completedQuest(player, quest)) {
        	if (quest.getMsg() != null) {
        		player.sendMessage("Quest completed.");
        		return;
        	}
        }
        
        QuestHandlerTemp.clearQuestInterface(player);
        player.getPacketSender().sendFrame126b("<col=800000>" + quest.getTitle(), QuestHandlerTemp.TITLE_STRING_ID);
        QuestHandlerTemp.updateQuestInterface(player, quest);
        
    }
}
