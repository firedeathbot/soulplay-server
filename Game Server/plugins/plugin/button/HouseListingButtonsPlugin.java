package plugin.button;

import com.soulplay.content.player.skills.construction.popularhouse.PopularHouses;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class HouseListingButtonsPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, PopularHouses.CHANGE_TITLE); // change title
		map(ButtonClickExtension.class, PopularHouses.REFRESH); // refresh
		map(ButtonClickExtension.class, 257258);
		map(ButtonClickExtension.class, 257263);
		map(ButtonClickExtension.class, 257268);
		map(ButtonClickExtension.class, 257273);
		map(ButtonClickExtension.class, 257278);
		map(ButtonClickExtension.class, 257283);
		map(ButtonClickExtension.class, 257288);
		map(ButtonClickExtension.class, 257293);
		map(ButtonClickExtension.class, 257298);
		map(ButtonClickExtension.class, 257303);
	}

	@Override
	public void onClick(Player player, int button, int component) {
		PopularHouses.clickedButton(player, button);

	}
}
