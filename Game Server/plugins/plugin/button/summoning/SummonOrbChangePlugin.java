package plugin.button.summoning;

import com.soulplay.content.player.skills.summoning.SummonOrbButtons;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class SummonOrbChangePlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 4003);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		player.getDialogueBuilder().sendOption(SummonOrbButtons.CALL_FOLLOWER.getButtonName(), ()-> {
			player.getPacketSender().sendSummoningOrbIndex(SummonOrbButtons.CALL_FOLLOWER.getButtonIndex());
			player.setSummoningOrbSetting(SummonOrbButtons.CALL_FOLLOWER.getButtonIndex());
			player.getPA().closeAllWindows();
		}, SummonOrbButtons.RENEW_FAMILIAR.getButtonName(), ()-> {
			player.getPacketSender().sendSummoningOrbIndex(SummonOrbButtons.RENEW_FAMILIAR.getButtonIndex());
			player.setSummoningOrbSetting(SummonOrbButtons.RENEW_FAMILIAR.getButtonIndex());
			player.getPA().closeAllWindows();
		}, SummonOrbButtons.CAST.getButtonName(), ()-> {
			player.getPacketSender().sendSummoningOrbIndex(SummonOrbButtons.CAST.getButtonIndex());
			player.setSummoningOrbSetting(SummonOrbButtons.CAST.getButtonIndex());
			player.getPA().closeAllWindows();
		}, SummonOrbButtons.TAKE_BOB.getButtonName(), ()-> {
			player.getPacketSender().sendSummoningOrbIndex(SummonOrbButtons.TAKE_BOB.getButtonIndex());
			player.setSummoningOrbSetting(SummonOrbButtons.TAKE_BOB.getButtonIndex());
			player.getPA().closeAllWindows();
		}).execute();
	}
}
