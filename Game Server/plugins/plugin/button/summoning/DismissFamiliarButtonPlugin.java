package plugin.button.summoning;

import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class DismissFamiliarButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		//map(ButtonClickExtension.class, SummonOrbButtons.DISMISS.getActionButtonId());
		map(ButtonClickExtension.class, 61050);
		map(ButtonClickExtension.class, 66127);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		Client c = (Client) player;
		c.getSummoning().dismissFamiliar(false);
	}
}