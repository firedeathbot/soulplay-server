package plugin.button.summoning;

import com.soulplay.content.player.skills.summoning.SummonOrbButtons;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class TakeBoBPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, SummonOrbButtons.TAKE_BOB.getActionButtonId());
		map(ButtonClickExtension.class, 66122);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		Client c = (Client) player;
		c.getSummoning().openBoB();
	}
}
