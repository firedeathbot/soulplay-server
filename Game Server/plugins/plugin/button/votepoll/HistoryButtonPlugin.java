package plugin.button.votepoll;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class HistoryButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 230124);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		player.sendMessage("History is not available yet.");
	}
}
