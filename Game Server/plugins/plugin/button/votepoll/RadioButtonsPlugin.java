package plugin.button.votepoll;

import com.soulplay.content.poll.PollManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class RadioButtonsPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		int buttonId = PollManager.RADIO_BUTTON_START;
		for (int i = 0; i < PollManager.MAX_POLLS; i++) {
			int realButtonId = buttonId;
			if (realButtonId >= 230255) {
				realButtonId += 1000;
				realButtonId -= 256;
			}

			map(ButtonClickExtension.class, realButtonId++); // yes
			map(ButtonClickExtension.class, realButtonId++); // no
			map(ButtonClickExtension.class, realButtonId++); // skip
			buttonId += 17;
		}
	}

	@Override
	public void onClick(Player player, int button, int component) {
		PollManager.radioButtonClick(player, button);
	}

}
