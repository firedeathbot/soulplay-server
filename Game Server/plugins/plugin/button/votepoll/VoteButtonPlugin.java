package plugin.button.votepoll;

import com.soulplay.content.poll.PollManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class VoteButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 230126);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		PollManager.voteButtonClick(player);
	}
}
