package plugin.button;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class BrightnessButtonPlugin extends Plugin implements ButtonClickExtension {

    @Override
    public void onInit() {
        map(ButtonClickExtension.class, 3138);
        map(ButtonClickExtension.class, 3140);
        map(ButtonClickExtension.class, 3142);
        map(ButtonClickExtension.class, 3144);
    }

    @Override
    public void onClick(Player player, int button, int component) {
        if (button == 3138) {
            player.setVar(Variables.BRIGHTNESS, 1);
        } else if (button == 3140) {
            player.setVar(Variables.BRIGHTNESS, 2);
        } else if (button == 3142) {
            player.setVar(Variables.BRIGHTNESS, 3);
        } else if (button == 3144) {
            player.setVar(Variables.BRIGHTNESS, 4);
        }
    }

}
