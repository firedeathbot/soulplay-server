package plugin.button;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class ChargeSpellButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 4186);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		player.getGodSpell().clickChargeButton();
	}
	
}
