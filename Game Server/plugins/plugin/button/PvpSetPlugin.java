package plugin.button;

import com.soulplay.content.items.impl.PVPSet;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.map.travel.zone.addon.EdgevilleSpawnSkillZone;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class PvpSetPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		for (PVPSet value : PVPSet.values) {
			map(ButtonClickExtension.class, value.buttonId);
		}
	}

	@Override
	public void onClick(Player player, int button, int component) {
		if (player.wildLevel >= 1 || player.safeTimer >= 1 || !EdgevilleSpawnSkillZone.canChangeStats(player)) {
			player.sendMessage("You can't change sets at this moment.");
			return;
		}

		for (PVPSet pvpSet : PVPSet.values) {
			if (button == pvpSet.buttonId) {
				PVPSet.pvpSet(player, pvpSet);
				return;
			}
		}
	}

}
