package plugin.button;

import com.soulplay.Config;
import com.soulplay.content.donate.DonationInterface;
import com.soulplay.content.donate.DonationInterfaceCategory;
import com.soulplay.content.donate.DonationInterfaceCategoryEntry;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class DonationInterfacePlugin extends Plugin implements ButtonClickExtension {
	
	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 268403);
		map(ButtonClickExtension.class, 268404);
		map(ButtonClickExtension.class, 268406);
		map(ButtonClickExtension.class, 268491);
		map(ButtonClickExtension.class, 268400);
		map(ButtonClickExtension.class, 268397);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		switch (button) {
			case 268404: {
				if (DonationInterface.getCategories().isEmpty()) {
					return;
				}

				DonationInterfaceCategory selectedCategory = DonationInterface.getCategories().get(player.donationInterfaceSelectedCategory);
				int entriesSize = selectedCategory.getEntries().size();
				int tileCount = selectedCategory.getType().getElements();
				int pages = entriesSize / tileCount;
				if (entriesSize % tileCount != 0) {
					pages++;
				}

				if (player.donationInterfaceSelectedPage + 1 >= pages) {
					return;
				}

				player.donationInterfaceSelectedEntry = -1;
				player.donationInterfaceSelectedPage++;
				player.getPacketSender().sendDonationInterfaceData();
				player.getPacketSender().sendDonationInterfaceCategoryData();
				updatePage(player);
				return;
			}
			case 268403: {
				if (player.donationInterfaceSelectedPage <= 0) {
					return;
				}

				player.donationInterfaceSelectedEntry = -1;
				player.donationInterfaceSelectedPage--;
				player.getPacketSender().sendDonationInterfaceData();
				player.getPacketSender().sendDonationInterfaceCategoryData();
				updatePage(player);
				return;
			}
			case 268406: {
				int categoryId = component / 2;
				if (categoryId < 0 || categoryId >= DonationInterface.getCategories().size()) {
					return;
				}

				player.donationInterfaceSelectedCategory = categoryId;
				player.donationInterfaceSelectedPage = 0;
				player.donationInterfaceSelectedEntry = -1;
				player.getPacketSender().sendDonationInterfaceCategoryData();
				player.getPacketSender().sendDonationInterfaceData();
				updatePage(player);
				return;
			}
			case 268491: {
				if (DonationInterface.getCategories().isEmpty()) {
					return;
				}

				DonationInterfaceCategory selectedCategory = DonationInterface.getCategories().get(player.donationInterfaceSelectedCategory);
				int entryId = component / 6;
				if (entryId < 0 || entryId >= selectedCategory.getEntries().size()) {
					return;
				}

				player.donationInterfaceSelectedEntry = entryId;
				player.getPacketSender().sendDonationInterfaceData();
				return;
			}
			case 268400: {
				if (DonationInterface.getCategories().isEmpty()) {
					return;
				}

				DonationInterfaceCategory selectedCategory = DonationInterface.getCategories().get(player.donationInterfaceSelectedCategory);
				if (selectedCategory == null) {
					player.sendMessage("Unknown category, unable to purchase.");
					return;
				}

				int entryId = player.donationInterfaceSelectedEntry;
				if (entryId == -1) {
					player.sendMessage("Unknown entry selected, unable to purchase.");
					return;
				}

				int pageOffset = player.donationInterfaceSelectedPage * selectedCategory.getType().getElements();
				DonationInterfaceCategoryEntry entry = selectedCategory.getEntries().get(entryId + pageOffset);
				if (entry == null) {
					player.sendMessage("Non existing entry selected, unable to purchase.");
					return;
				}

				int currentPoints = player.getDonPoints();
				int price = entry.getBuyPrice();
				if (currentPoints < price) {
					player.sendMessage("Unable to purchase. Not enough donation points.");
					return;
				}

				String buyFunctionResponse = entry.getBuyFunction().apply(player);
				if (buyFunctionResponse != null) {
					player.sendMessage("Unable to purchase. " + buyFunctionResponse);
					return;
				}

				player.setDonPoints(currentPoints - price, true);
				if (entry.isPet()) {
					player.sendMessage("Congratulations, you've unlocked a new pet!");
				} else {
					player.sendMessage("You've bought the item successfully, it has been added to your inventory.");
					player.sendMessage("Or bank if your inventory is full.");
				}

				player.sendMessage("Your current donation points are " + currentPoints + ".");
				DonationInterface.updatePoints(player);
				return;
			}
			case 268397: {
				player.getPacketSender().openUrl(Config.URLS[3]);
				return;
			}
		}
	}

	public static void updatePage(Player player) {
		DonationInterfaceCategory selectedCategory = DonationInterface.getCategories().get(player.donationInterfaceSelectedCategory);
		int entriesSize = selectedCategory.getEntries().size();
		int tileCount = selectedCategory.getType().getElements();
		int pages = entriesSize / tileCount;
		if (entriesSize % tileCount != 0) {
			pages++;
		}

		player.getPacketSender().sendString("Page " + (player.donationInterfaceSelectedPage + 1) + "/" + pages, 69014);
	}

}
