package plugin.button.achiev;

import java.util.Map.Entry;

import com.soulplay.content.player.achievement.Achievs;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class AchievementButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		Achievs.fillButtons();
		for (Entry<Integer, Integer> button : Achievs.buttonIds.entrySet()) {
			int buttonId = button.getKey();
			
			map(ButtonClickExtension.class, buttonId);
		}
	}
	
	
	@Override
	public void onClick(Player player, int button, int component) {
		//put into try block just in case XD
		try {
			int buttonIndex = Achievs.buttonIds.get(button);
			
			int progress = player.getAchievInt(buttonIndex);
			Achievs achiev = Achievs.values[buttonIndex];
			if (progress < achiev.getCompletedAmount()) {
				player.sendMessage("Your current progress for this achievement is " + progress + " completed");
			} else {
				player.sendMessage("You have already completed this achievement!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
