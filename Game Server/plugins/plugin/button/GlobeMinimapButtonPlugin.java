package plugin.button;

import com.soulplay.content.TeleportInterface;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class GlobeMinimapButtonPlugin extends Plugin implements ButtonClickExtension {

    @Override
    public void onInit() {
        map(ButtonClickExtension.class, 19141);
    }

    @Override
    public void onClick(Player player, int button, int component) {
    	
    	if (player.dungParty != null && player.dungParty.dungManager != null) {
    		player.dungParty.dungManager.openMap(player);
    		return;
		}
    	
        player.getPacketSender().showInterface(25411);
        TeleportInterface.toggleLayers(player, player.lastTeleportsTab);
    }

}
