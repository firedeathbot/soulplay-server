package plugin.button.seasonpass;

import com.soulplay.content.player.dailytasks.CategoryInterfaceHandler;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class SeasonPassButtonsPlugin extends Plugin implements ButtonClickExtension {

	private static final int OPEN_DAILY_TASKS = 234100;
	private static final int BUY_PASS = 234102;
	private static final int OPEN_SEASON_PASS = 258262;
	
	@Override
	public void onInit() {
		map(ButtonClickExtension.class, OPEN_DAILY_TASKS);
		map(ButtonClickExtension.class, BUY_PASS);
		map(ButtonClickExtension.class, OPEN_SEASON_PASS);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		switch (button) {
		case OPEN_DAILY_TASKS:
			CategoryInterfaceHandler.open(player);
			break;
			
		case BUY_PASS:
			if (player.seasonPass.purchased) {
				player.getShops().openShop(15);
				player.sendMessage("You can find the Level up Pass scroll in this Donor Misc shop.");
			} else {
				SeasonPassManager.openPurchasePassDialogue(player);
			}
			break;
			
		case OPEN_SEASON_PASS:
			SeasonPassManager.openInterface(player);
			break;
		
		}
	}

}
