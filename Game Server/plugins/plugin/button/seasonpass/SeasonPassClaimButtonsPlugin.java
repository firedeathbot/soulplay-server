package plugin.button.seasonpass;

import com.soulplay.content.player.seasonpass.Reward;
import com.soulplay.content.player.seasonpass.SeasonPassClaim;
import com.soulplay.content.player.seasonpass.SeasonPassManager;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class SeasonPassClaimButtonsPlugin extends Plugin implements ButtonClickExtension {
	
	private static final int SCROLL_CHILDREN_SIZE = 5;
	private static final int BUTTON_START = 234115;

	@Override
	public void onInit() {
		for (int i = 0; i < 40; i++) {
			int buttonId = i * SCROLL_CHILDREN_SIZE + BUTTON_START;//change 5 to children size of the scroll interface
			map(ButtonClickExtension.class, buttonId);
		}
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		
		if (!player.getStopWatch().checkThenStart(1)) {
			player.sendMessage("Please slow down...");
			return;
		}
		
		int rewardIndex = (button - BUTTON_START) / SCROLL_CHILDREN_SIZE;
		
		if (rewardIndex < 0 || rewardIndex+1 > SeasonPassManager.rewards.size()) {
			System.out.println("too large or negative");
			return;
		}
		
		Reward r = SeasonPassManager.rewards.get(rewardIndex);
		
		if (r == null) {
			System.out.println("null reward");
			return;
		}
		
		if (player.seasonPass.level < r.levelRequired) {
			player.sendMessage("Your season pass level is too low to claim that item.");
			return;
		}
		
		SeasonPassClaim.claimReward(player, r);
	}
}
