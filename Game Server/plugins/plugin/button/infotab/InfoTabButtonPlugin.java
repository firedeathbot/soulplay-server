package plugin.button.infotab;

import com.soulplay.Config;
import com.soulplay.Server;
import com.soulplay.config.WorldType;
import com.soulplay.content.minigames.FPLottery;
import com.soulplay.content.player.DonatorRank;
import com.soulplay.content.vote.VoteInterface;
import com.soulplay.game.event.Task;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;
import com.soulplay.util.sql.payment.ClaimPayments;
import com.soulplay.util.sql.payment.ClaimWheelReward;

public class InfoTabButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 260497);
		map(ButtonClickExtension.class, 261255);
		map(ButtonClickExtension.class, 261256);
		map(ButtonClickExtension.class, 261257);
		map(ButtonClickExtension.class, 261258);
		map(ButtonClickExtension.class, 261259);
		map(ButtonClickExtension.class, 260491);
		map(ButtonClickExtension.class, 260509);
		map(ButtonClickExtension.class, 260510);
	}

	@Override
	public void onClick(Player player, int button, int component) {
		switch (button) {
			case 260497:// open store
				player.getPacketSender().openUrl(Config.URLS[3]);
				return;
	
			case 261255:// claim donations
				if (!player.inEdgeville()) {
					player.sendMessage("You must be in Edgeville to claim points.");
					return;
				}
	
				if (Config.isPvpWorld()) {
					player.sendMessageSpam("You cannot use this command in PVP world.");
					return;
				}
	
				if (player.getSpamTick().checkThenStart(5)) {
					if (player.getItems().freeSlots() < 12) {
						player.sendMessage("You must have at least 12 slots available in your Inventory.");
						return;
					}
					if (player.underAttackBy > 0 || player.underAttackBy2 > 0) {
						player.sendMessage("You are currently under attack. Can't claim at the moment.");
						return;
					}
					// SimpleSql.addDonateItems(c, c.getName());
	
					try {
						player.startLogoutDelay();
						player.sendMessage("Checking for Donations.");
						ClaimPayments.claimDonationOverWebSQL((Client) player);
					} catch (Exception e) {
						e.printStackTrace();
						player.sendMessage("The database is unreachable at the moment. Please try again later.");
					}
	
					player.sendMessage("Checking unclaimed Lottery prizes.");
					FPLottery.checkUnclaimedWinners((Client) player);
				} else {
					player.sendMessage("Please slow down.");
				}
				return;
	
			case 261256:// open spinning wheel
				player.getPacketSender().openUrl(Config.URLS[9]);
				return;
	
			case 261257:// claim spinning wheel rewards
				if (!WorldType.equalsType(WorldType.ECONOMY)) {
					player.sendMessage("You must be in economy world in order to use the command.");
					return;
				}
	
				if (!player.inEdgeville()) {
					player.sendMessage("You must be in Edgeville to use ::checkspins");
					return;
				}
	
				if (player.startedCheckSpins) {
					player.sendMessage("You are already checking for rewards. Walk to stop checking task.");
					return;
				}
	
				if (player.getSpamTick().checkThenStart(5)) {
					if (player.getItems().freeSlots() < 1) {
						player.sendMessage("You must have at least 3 slots available in your Inventory.");
						return;
					}
					if (player.inMinigame()) {
						player.sendMessage("You cannot claim your reward here.");
						return;
					}
					if (player.underAttackBy > 0 || player.underAttackBy2 > 0) {
						player.sendMessage("You are currently under attack. Can't claim at the moment.");
						return;
					}
	
					player.sendMessage("Checking for Wheel of fortune Rewards.");
					player.startedCheckSpins = true;
	
					Server.getTaskScheduler().schedule(new Task() {
	
						@Override
						protected void execute() {
							if (player == null || player.disconnected) {
								this.stop();
								return;
							}
	
							if (!player.startedCheckSpins) {
								player.sendMessage("Ended checking for rewards in SOF.");
								this.stop();
								return;
							}
	
							if (player.getItems().freeSlots() < 1) {
								player.sendMessage("Your inventory is full.");
								this.stop();
								return;
							}
	
							try {
	
								ClaimWheelReward.claimWheel((Client) player, true);
							} catch (Exception e) {
								e.printStackTrace();
								player.sendMessage("An error has occured. Please try again later.");
								this.stop();
								return;
							}
	
						}
					});
				} else {
					player.sendMessage("Please slow down.");
				}
				return;

			case 261258:
				VoteInterface.openVotePage(player);
				return;

			case 261259:
				VoteInterface.claimVotes(player);
				return;

			case 260491:
				player.getPacketSender().sendFrame106(11);
				player.getPacketSender().setSidebarInterface(11, 64800);
				return;
	
			case 260509:
				player.getPacketSender().openUrl(Config.URLS[17]);
				return;
	
			case 260510:
				player.getPacketSender().openUrl(Config.URLS[14]);
				return;
	
			default:
				break;

		}
	}

	public static void update(Player c) {
		c.getPacketSender().sendString("Donator rank: " + DonatorRank.check(c).getName(), 67051);
		c.getPacketSender().sendString("Total votes: " + c.getVar(Variables.TIMES_VOTED), 67057);
		c.getPacketSender().sendString("Name: " + c.getNameSmartUp(), 67061);
	}

}
