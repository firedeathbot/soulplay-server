package plugin.button;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class SoundEffectVolumePlugin extends Plugin implements ButtonClickExtension {

    @Override
    public void onInit() {
        map(ButtonClickExtension.class, 250106);
        map(ButtonClickExtension.class, 250108);
        map(ButtonClickExtension.class, 250110);
        map(ButtonClickExtension.class, 250112);
        map(ButtonClickExtension.class, 250114);
    }

    @Override
    public void onClick(Player player, int button, int component) {
        switch(button) {
            case 250106:
                player.setVar(Variables.SOUND_EFFECT_VOLUME, 4);
                break;

            case 250108:
                player.setVar(Variables.SOUND_EFFECT_VOLUME, 3);
                break;

            case 250110:
                player.setVar(Variables.SOUND_EFFECT_VOLUME, 2);
                break;

            case 250112:
                player.setVar(Variables.SOUND_EFFECT_VOLUME, 1);
                break;

            case 250114:
                player.setVar(Variables.SOUND_EFFECT_VOLUME, 0);
                break;
        }

    }

}
