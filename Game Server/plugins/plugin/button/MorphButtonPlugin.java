package plugin.button;

import com.soulplay.game.model.item.definition.MorphingRing;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class MorphButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 23132);
	}

	@Override
	public void onClick(Player player, int button, int component) {
		MorphingRing morphingRing = player.morphingRing;
		if (morphingRing == null) {
			return;
		}

		morphingRing.unmorph(player);
	}

}