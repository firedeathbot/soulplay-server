package plugin.button.bank;

import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class PlaceHolderButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 86027);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		player.togglePlaceholder();
	}
}
