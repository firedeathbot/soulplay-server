package plugin.button;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.chatcrown.ChatCrown;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class ChatCrownButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		ChatCrown.init();
		for (int i = 0; i < ChatCrown.values.length; i++) {
			ChatCrown crown = ChatCrown.values[i];
			if (crown.getButtonId() > 0)
				map(ButtonClickExtension.class, crown.getButtonId());
		}
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		if (player.isIronMan()) {
			player.sendMessage("You cannot change chat icon on ironman mode.");
			player.getPacketSender().sendConfig(ChatCrown.RADIO_BUTTON_CONFIG, -1);
			return;
		}
		final ChatCrown crown = ChatCrown.getByButton(button);
		if (crown == null) return;
		
		if (ChatCrown.isCrownUnlocked(player, crown)) { // if crown is unlocked
			player.setCrown(crown.ordinal());
		}
		player.getPacketSender().sendConfig(ChatCrown.RADIO_BUTTON_CONFIG, ChatCrown.values[player.getSetCrown()].getRadioId());
	}
	
}
