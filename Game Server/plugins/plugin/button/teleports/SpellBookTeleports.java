package plugin.button.teleports;

import com.soulplay.content.TeleportInterface;
import com.soulplay.game.model.player.Client;
import com.soulplay.game.model.player.PathFinder;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.PlayerHandler;
import com.soulplay.game.world.map.travel.ZoneManager;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class SpellBookTeleports extends Plugin implements ButtonClickExtension {
	
	@Override
	public void onInit() {
		map(ButtonClickExtension.class, 4142);
		map(ButtonClickExtension.class, 50059);
		map(ButtonClickExtension.class, 117055);
		map(ButtonClickExtension.class, 4145);
		map(ButtonClickExtension.class, 50078);
		map(ButtonClickExtension.class, 4148);
		map(ButtonClickExtension.class, 50081);
		map(ButtonClickExtension.class, 117057);
		map(ButtonClickExtension.class, 4153);
		map(ButtonClickExtension.class, 50071);
		map(ButtonClickExtension.class, 117060);
		map(ButtonClickExtension.class, 4174);
		map(ButtonClickExtension.class, 4159);
		map(ButtonClickExtension.class, 50065);
		map(ButtonClickExtension.class, 117061);
		map(ButtonClickExtension.class, 4164);
		map(ButtonClickExtension.class, 117064);
		map(ButtonClickExtension.class, 4171);
		map(ButtonClickExtension.class, 117065);
		map(ButtonClickExtension.class, 4191);
		map(ButtonClickExtension.class, 50075);
		map(ButtonClickExtension.class, 117076);
		
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		
		switch(button) {
			case 4142:
			case 50059:
			case 117055:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 1);//Monster
				break;

			case 4145:
			case 50078:
			case 117056:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 4);//Minigame
				break;

			case 4148:
			case 50081:
			case 117057:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 3);//Boss
				break;

			case 4153:
			case 50071:
			case 117060:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 5);//Pking
				break;

			case 4174:
			case 4159:
			case 50065:
			case 117061:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 6);//Skilling
				break;

			case 4164:
			case 117064:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 0);//Cities
				break;

			case 4171:
			case 117065:
				player.getPacketSender().showInterface(25411);
				TeleportInterface.toggleLayers((Client) player, 2);//Dungeon
				break;
				
			case 4191:
			case 50075:
			case 117076:
				if (player.getBountyTargetSpell() < 1) {
					player.sendMessage("You haven't learned how to cast this spell yet.");
					return;
				}

				if (player.huntPlayerIndex < 1 || !player.inWild()) {
					player.sendMessage("You don't have a target.");
					return;
				}
				
				Player other = PlayerHandler.players[player.huntPlayerIndex];
				
				if (other == null || !other.isActive) {
					player.sendMessage("You don't have a target.");
					return;
				}
				
				if (player.BountyTargetLimit > 2) {
					player.sendMessage("You can only teleport up to 3 times to a target!");
					return;
				}
				if (player.underAttackBy > 0) {
					player.sendMessage("You cannot do this while under attack!");
					return;
				}
				int opponentWildLevel = other.wildLevel;
				if (player.wildLevel > 20 && opponentWildLevel < player.wildLevel) {
					player.sendMessage("You cannot teleport from Wildy level " + player.wildLevel + " to Wildy level " + opponentWildLevel);
					return;
				}
				if (opponentWildLevel < 1) {
					player.sendMessage("Your opponent is no longer in the Wilderness.");
					return;
				}
				
				if (player.getPA().BountyTargetTeleport(PathFinder.findBountySpot(other))) {
					player.BountyTargetLimit++;			 
					ZoneManager.checkForAddonZones(player, other);
				}
				
				break;
				
			default:
				break;
		    }
	    }

}
