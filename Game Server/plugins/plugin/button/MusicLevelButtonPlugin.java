package plugin.button;

import com.soulplay.game.model.player.Player;
import com.soulplay.game.model.player.config.Variables;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class MusicLevelButtonPlugin extends Plugin implements ButtonClickExtension {

    @Override
    public void onInit() {
        map(ButtonClickExtension.class, 250105); // lvl 1
        map(ButtonClickExtension.class, 250107); // lvl 2
        map(ButtonClickExtension.class, 250109); // lvl 3
        map(ButtonClickExtension.class, 250111); // lvl 4
        map(ButtonClickExtension.class, 250113); // lvl 5
    }

    @Override
    public void onClick(Player player, int button, int component) {
        switch(button) {
            case 250113:
                player.setVar(Variables.MUSIC_LEVEL, 0);
                break;

            case 250111:
                player.setVar(Variables.MUSIC_LEVEL, 1);
                break;

            case 250109:
                player.setVar(Variables.MUSIC_LEVEL, 2);
                break;

            case 250107:
                player.setVar(Variables.MUSIC_LEVEL, 3);
                break;

            case 250105:
                player.setVar(Variables.MUSIC_LEVEL, 4);
                break;
        }

    }

}
