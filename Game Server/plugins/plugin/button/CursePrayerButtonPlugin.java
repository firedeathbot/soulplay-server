package plugin.button;

import com.soulplay.content.player.combat.prayer.CursesPrayer;
import com.soulplay.game.model.player.Player;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class CursePrayerButtonPlugin extends Plugin implements ButtonClickExtension {

	@Override
	public void onInit() {
		for (int button : CursesPrayer.buttonIds)
			map(ButtonClickExtension.class, button);
	}
	
	@Override
	public void onClick(Player player, int button, int component) {
		player.curses().curseButtons(button);
	}
}
