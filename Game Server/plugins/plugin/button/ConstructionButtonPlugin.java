package plugin.button;

import com.google.common.collect.ImmutableMap;
import com.soulplay.content.player.skills.construction.Construction;
import com.soulplay.content.player.skills.construction.ConstructionUtils;
import com.soulplay.content.player.skills.construction.Poh;
import com.soulplay.content.player.skills.construction.room.Room;
import com.soulplay.content.player.skills.construction.room.RoomFactory;
import com.soulplay.content.player.skills.construction.room.RoomType;
import com.soulplay.game.model.player.Player;
import com.soulplay.game.world.Direction;
import com.soulplay.game.world.entity.Location;
import com.soulplay.plugin.Plugin;
import com.soulplay.plugin.extension.ButtonClickExtension;

public class ConstructionButtonPlugin extends Plugin implements ButtonClickExtension {

    private static final ImmutableMap<Integer, RoomType> roomMap = new ImmutableMap.Builder<Integer, RoomType>()
            .put(111235, RoomType.GARDEN).put(111231, RoomType.PARLOUR)
            .put(111239, RoomType.KITCHEN)
            .put(111243, RoomType.DINING)
            .put(111247, RoomType.WORKSHOP)
            .put(111251, RoomType.BED)
            .put(111255, RoomType.SKILL_TROPHY_HALL)
            .put(112003, RoomType.GAMES)
            .put(112007, RoomType.COMBAT)
            .put(112011, RoomType.QUEST_TROPHY)
            .put(112015, RoomType.MENAGERY)
            .put(112019, RoomType.STUDY)
            .put(112023, RoomType.COSTUME)
            .put(112027, RoomType.CHAPEL)
            .put(112031, RoomType.PORTAL)
            .put(112035, RoomType.FORMAL_GARDEN)
            .put(112039, RoomType.THRONE)
            .put(112043, RoomType.DUNGEON_OUBLIETTE)
            .put(112055, RoomType.DUNGEON_STAIR_ROOM)
            .put(112047, RoomType.DUNGEON_CORRIDOR)
            .put(112051, RoomType.DUNGEON_JUNCTION)
            .put(112059, RoomType.DUNGEON_PIT)
            .put(112063, RoomType.DUNGEON_TREASURE_ROOM)
            .build();

    @Override
    public void onInit() {
        for (int button : roomMap.keySet()) {
            map(ButtonClickExtension.class, button);
        }
    }

    @Override
    public void onClick(Player player, int button, int component) {
        final RoomType type = roomMap.get(button);

        if (type == null) {
            return;
        }

        if (type == RoomType.MENAGERY || type == RoomType.DUNGEON_PIT) {
            player.sendMessage("This room will be coming soon!");
            return;
        }

        if (!player.getItems().playerHasItem(995, type.getCost())) {
            player.sendMessage(String.format("You need %d coins for this room.", type.getCost()));
            return;
        }

        final Direction direction = Construction.calculateDirection(player);

        final Location nextPaletteIndex = ConstructionUtils.calculateNextPaletteIndex(player);

        if (nextPaletteIndex.getX() == -1 || nextPaletteIndex.getY() == -1) {
            return;
        }

        if (player.getZ() != Poh.GROUND_FLOOR) {
            if (type == RoomType.GARDEN || type == RoomType.FORMAL_GARDEN) {
                player.getDialogueBuilder().sendStatement("You cannot build garden here.").execute();
                return;
            }
        }

        if (player.getZ() == Poh.GROUND_FLOOR) {
            buildGroundFloorRoom(player, type, nextPaletteIndex, direction);
        } else {
            buildDungOrSecondFloorRoom(player, type, nextPaletteIndex, direction, true);
        }
    }

    private static void buildDungOrSecondFloorRoom(Player player, RoomType type, Location nextPaletteIndex, Direction direction, boolean showRotateDialogue) {

        player.getPA().closeAllWindows();

        int z = nextPaletteIndex.getZ();

        Room room = RoomFactory.createRoom(type, nextPaletteIndex.getX(), nextPaletteIndex.getY(), z);
        room.setLocation(nextPaletteIndex);

        if (room.isDungeon() && nextPaletteIndex.getZ() != Poh.DUNGEON_FLOOR) {
            player.getDialogueBuilder().sendStatement("You can't build a dungeon room here.").execute();
            return;
        }

        // rules for rooms with staircases
        if (player.getZ() == Poh.DUNGEON_FLOOR || player.getZ() == Poh.GROUND_FLOOR) {

            Room roomAbove = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), player.getZ() == Poh.DUNGEON_FLOOR ? Poh.GROUND_FLOOR : Poh.SECOND_FLOOR);

            if (roomAbove != null && !roomAbove.hasStairs() && roomAbove.getStairsHotspot() != null) {
                room.setLocation(nextPaletteIndex);
                room.getPalette().rotate(roomAbove.getRotation());
                buildRoomFinal(player, room, type);
                player.getPacketSender().sendConstructMapRegionPacket(player.getPOH());
                return;
            }

        }
        if (player.getZ() == Poh.SECOND_FLOOR || player.getZ() == Poh.GROUND_FLOOR) {

            Room roomBelow = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), player.getZ() == Poh.SECOND_FLOOR ? Poh.GROUND_FLOOR : Poh.DUNGEON_FLOOR);

            if (roomBelow != null && !roomBelow.hasStairs() && roomBelow.getStairsHotspot() != null) {
                room.setLocation(nextPaletteIndex);
                room.getPalette().rotate(roomBelow.getRotation());
                buildRoomFinal(player, room, type);
                player.getPacketSender().sendConstructMapRegionPacket(player.getPOH());
                return;
            }

        }

        player.getPOH().copyRooms(player.getPOH().getRooms());
        player.getPOH().getCopyRooms()[room.getZ()][room.getX()][room.getY()] = room;

        if (!player.getPOH().canPlaceRoom(room, 3, true)) {
            player.sendMessage("You cannot place that room here.");
            player.getPOH().clearCopyRooms();
            return;
        }

        player.getPacketSender().sendConstructMapRegionPacket(player.getPOH().getCopyRooms(), player.getPOH().getHouseStyle());

        if (showRotateDialogue) {
            player.getDialogueBuilder().createCheckpoint().sendOption("Rotate clockwise", () -> {
                player.getDialogueBuilder().nullResetAction();
                room.rotate(player, 1);
                player.getDialogueBuilder().returnToCheckpoint();
            }, "Rotate anticlockwise", () -> {
                player.getDialogueBuilder().nullResetAction();
                room.rotate(player, -1);
                player.getDialogueBuilder().returnToCheckpoint();
            }, "Build", () -> {
            	buildRoomFinal(player, room, type);
            }, "Cancel", () -> {
                Construction.cancelBuild(player);
            }).onReset(()-> {
                Construction.cancelBuild(player);
            }).execute(false);
        } else {
        	Construction.cancelBuild(player);
        }
    }

    private static void buildGroundFloorRoom(Player player, RoomType type, Location nextPaletteIndex, Direction direction) {

        if (nextPaletteIndex.getZ() == Poh.GROUND_FLOOR) {
            Room nextRoom = player.getPOH().getRoom(nextPaletteIndex.getX() + ConstructionUtils.getDirectionOffsetX(direction), nextPaletteIndex.getY() + ConstructionUtils.getDirectionOffsetY(direction), player.getZ());

            // restrict players from building forever in 1 direction or having a gigantic region
            if (player.getPOH().getGridSize() > Poh.MAXIMUM_GRID_SIZE && nextRoom == null) {
                player.sendMessage(String.format("You can only have a %dx%d yard. Current size: %dx%d", Poh.MAXIMUM_GRID_SIZE, Poh.MAXIMUM_GRID_SIZE, player.getPOH().getGridSize(), player.getPOH().getGridSize()));
                return;
            }

            constructNewAreas(player, direction, nextPaletteIndex);
        }

        player.getPA().closeAllWindows();

        Room room = RoomFactory.createRoom(type, nextPaletteIndex.getX(), nextPaletteIndex.getY(), player.getZ());

        if (room.isDungeon() && nextPaletteIndex.getZ() != Poh.DUNGEON_FLOOR) {
            player.getDialogueBuilder().sendStatement("You can't build a dungeon room here.").execute();
            return;
        }

        room.setLocation(nextPaletteIndex);

        // rules for rooms with staircases
        if (player.getZ() == Poh.DUNGEON_FLOOR || player.getZ() == Poh.GROUND_FLOOR) {

            Room roomAbove = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), player.getZ() == Poh.DUNGEON_FLOOR ? Poh.GROUND_FLOOR : Poh.SECOND_FLOOR);

            if (roomAbove != null && !roomAbove.hasStairs() && roomAbove.getStairsHotspot() != null) {
                room.setLocation(nextPaletteIndex);
                room.getPalette().rotate(roomAbove.getRotation());
                buildRoomFinal(player, room, type);
                return;
            }

        }

        if (player.getZ() == Poh.SECOND_FLOOR || player.getZ() == Poh.GROUND_FLOOR) {

            Room roomBelow = player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY(), player.getZ() == Poh.SECOND_FLOOR ? Poh.GROUND_FLOOR : Poh.DUNGEON_FLOOR);

            if (roomBelow != null && !roomBelow.hasStairs() && roomBelow.getStairsHotspot() != null) {
                room.setLocation(nextPaletteIndex);
                room.getPalette().rotate(roomBelow.getRotation());
                buildRoomFinal(player, room, type);
                return;
            }

        }

        player.getPOH().copyRooms(player.getPOH().getRooms());
        player.getPOH().getCopyRooms()[room.getZ()][room.getX()][room.getY()] = room;

        if (!player.getPOH().canPlaceRoom(room, 3, true)) {
            player.sendMessage("You cannot place that room here.");
            player.getPOH().clearCopyRooms();
            return;
        }

        player.getPacketSender().sendConstructMapRegionPacket(player.getPOH().getCopyRooms(), player.getPOH().getHouseStyle());

        player.getDialogueBuilder().createCheckpoint().sendOption("Rotate clockwise", () -> {
            player.getDialogueBuilder().nullResetAction();
            room.rotate(player, 1);
            player.getDialogueBuilder().returnToCheckpoint();
        }, "Rotate anticlockwise", () -> {
            player.getDialogueBuilder().nullResetAction();
            room.rotate(player, -1);
            player.getDialogueBuilder().returnToCheckpoint();
        }, "Build", () -> {
            buildRoomFinal(player, room, type);
        }, "Cancel", () -> {
            Construction.cancelBuild(player);
        }).onReset(()-> {
            Construction.cancelBuild(player);
        }).execute(false);
    }

    private static void buildRoomFinal(Player player, Room room, RoomType type) {
        player.getPOH().placeRoom(room, false);
        player.getPOH().applyRoomRotation(room);
        player.getDialogueBuilder().nullResetAction();
        player.getDialogueBuilder().close();
        player.getItems().deleteItem2(995, type.getCost());
        player.getPacketSender().sendString(String.format("Number of rooms: %d", player.getPOH().getRoomCount()), 46005);
        player.getPOH().clearCopyRooms();
        player.getPOH().showAllObjects(player.getZ(), player);
    }

    private static void constructNewAreas(Player player, Direction direction, Location nextPaletteIndex) {
        final boolean flag = direction == Direction.NORTH && player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY() + 1, Poh.GROUND_FLOOR) == null
                || direction == Direction.SOUTH && player.getPOH().getRoom(nextPaletteIndex.getX(), nextPaletteIndex.getY() - 1, Poh.GROUND_FLOOR) == null
                || direction == Direction.EAST && player.getPOH().getRoom(nextPaletteIndex.getX() + 1, nextPaletteIndex.getY(), Poh.GROUND_FLOOR) == null
                || direction == Direction.WEST && player.getPOH().getRoom(nextPaletteIndex.getX() - 1, nextPaletteIndex.getY(), Poh.GROUND_FLOOR) == null;

        if (flag && player.getPOH().getGridSize() < Poh.MAXIMUM_GRID_SIZE) {
//			player.sendMessage(String.format("increasing grid size: %d to %d", player.getPOH().getGridSize(), player.getPOH().getGridSize() + 1));
            player.getPOH().incrementGridSize();
            player.getPOH().fillGrid();
        } else {
            player.getPOH().fillGrid();
//			player.sendMessage(String.format("You have reached the maximum grid size: %dx%d", player.getPOH().getGridSize(), player.getPOH().getGridSize()));
        }
    }

}
