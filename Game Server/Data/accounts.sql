-- phpMyAdmin SQL Dump
-- version 4.3.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 04, 2015 at 06:59 PM
-- Server version: 5.6.21-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `soulsplit`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `ID` int(11) NOT NULL,
  `PlayerName` varchar(12) NOT NULL,
  `Password` varchar(22) NOT NULL,
  `characterheight` int(5) DEFAULT '0',
  `characterposx` int(5) DEFAULT '2956',
  `characterposy` int(5) DEFAULT '3218',
  `characterrights` int(1) DEFAULT '0',
  `difficulty` int(1) DEFAULT '0',
  `homeTeleActions` int(1) DEFAULT '0',
  `lastClanChat` varchar(12) NOT NULL DEFAULT '',
  `timePlayed` bigint(5) DEFAULT '0',
  `lastDayLoggedIn` varchar(12) DEFAULT '',
  `lastDayPkedOfYear` int(5) DEFAULT '0',
  `playerTitle` varchar(12) NOT NULL DEFAULT '',
  `titleColor` varchar(12) NOT NULL DEFAULT '',
  `titles` text,
  `UUID` text,
  `connectedFrom` text,
  `allConnectedIps` text,
  `lastKilledPlayers` text,
  `achievements` text,
  `achievementsCompleted` int(5) DEFAULT '0',
  `quests` text,
  `questPoints` int(5) DEFAULT '0',
  `skullTimer` int(5) DEFAULT '0',
  `startPack` tinyint(1) DEFAULT '0',
  `setPin` tinyint(1) DEFAULT '0',
  `bankPin` varchar(12) NOT NULL DEFAULT '',
  `hasBankpin` tinyint(1) DEFAULT '0',
  `pinDeleteDateRequested` int(5) DEFAULT '0',
  `requestPinDelete` tinyint(1) DEFAULT '0',
  `playerMagicBook` int(5) DEFAULT '0',
  `playerPrayerBook` int(5) DEFAULT '0',
  `dfsCharge` tinyint(1) DEFAULT '0',
  `specAmount` double(5,0) DEFAULT '0',
  `wildyPlayerKillCount` int(5) DEFAULT '0',
  `wildyPlayerDeathCount` int(5) DEFAULT '0',
  `killStreak` int(5) DEFAULT '0',
  `highestKillStreak` int(5) DEFAULT '0',
  `wildyEloRating` int(5) DEFAULT '0',
  `xpLock` tinyint(1) DEFAULT '0',
  `someRandomShit` bigint(5) DEFAULT '0',
  `teleblocklength` bigint(5) DEFAULT '0',
  `wildernessTick` int(5) DEFAULT '0',
  `npcKills` int(5) DEFAULT '0',
  `fightMode` tinyint(1) DEFAULT '0',
  `autoRet` int(5) DEFAULT '0',
  `doubleExpLength` bigint(5) DEFAULT '0',
  `muteEnd` bigint(5) DEFAULT '0',
  `accountFlagged` int(5) DEFAULT '0',
  `barrows` text,
  `wave` int(5) DEFAULT '0',
  `tasksCompleted` int(5) DEFAULT '0',
  `taskName` text,
  `masterName` text,
  `assignmentAmount` int(5) DEFAULT '0',
  `slayerPoints` int(5) DEFAULT '0',
  `duoPoints` int(5) DEFAULT '0',
  `dungn` int(5) DEFAULT '0',
  `dungBindedItems` text,
  `dungtokens` int(5) DEFAULT '0',
  `magePoints` int(5) DEFAULT '0',
  `pkpoints` int(5) DEFAULT '0',
  `penguinPoints` int(5) DEFAULT '0',
  `donPoints` int(5) DEFAULT '0',
  `TotalDonatedAmount` int(5) DEFAULT '0',
  `yellPoints` int(5) DEFAULT '0',
  `loyaltyPoints` int(5) DEFAULT '0',
  `pcPoints` int(5) DEFAULT '0',
  `gwdkc` bigint(5) DEFAULT '0',
  `Bank` text,
  `Bank1` text,
  `Bank2` text,
  `Bank3` text,
  `Bank4` text,
  `Bank5` text,
  `Bank6` text,
  `Bank7` text,
  `Bank8` text,
  `Skill` text,
  `Look` text,
  `Inventory` text,
  `Equipment` text,
  `Friend` text
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD UNIQUE KEY `ID` (`ID`), ADD UNIQUE KEY `PlayerName` (`PlayerName`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
