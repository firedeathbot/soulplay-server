package com.soulplayps.util.worker;

import org.jctools.queues.MessagePassingQueue;

/**
 * @author Jire
 */
public final class WorkQueue {
	
	private static final MessagePassingQueue.Consumer<Runnable> CONSUMER_RUNNABLE_RUN = Runnable::run;
	
	private final int defaultWorkLimit;
	
	private final MessagePassingQueue<Runnable> queue;
	
	public WorkQueue(MessagePassingQueue<Runnable> queue, int defaultWorkLimit) {
		this.queue = queue;
		this.defaultWorkLimit = defaultWorkLimit;
	}
	
	public void work() {
		work(defaultWorkLimit);
	}
	
	public void work(final int workLimit) {
		queue.drain(CONSUMER_RUNNABLE_RUN, workLimit);
	}
	
	public boolean offer(Runnable work) {
		return queue.offer(work);
	}
	
}
