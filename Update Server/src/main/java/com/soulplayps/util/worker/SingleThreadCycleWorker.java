package com.soulplayps.util.worker;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Jire
 */
public final class SingleThreadCycleWorker extends Worker {
	
	private final ScheduledExecutorService service;
	private final long duration;
	private final TimeUnit durationUnit;
	
	public SingleThreadCycleWorker(WorkQueue queue,
	                               ScheduledExecutorService service,
	                               long duration, TimeUnit durationUnit) {
		super(queue);
		
		this.service = service;
		this.duration = duration;
		this.durationUnit = durationUnit;
	}
	
	public SingleThreadCycleWorker(WorkQueue queue, long duration, TimeUnit durationUnit) {
		this(queue, Executors.newSingleThreadScheduledExecutor(), duration, durationUnit);
	}
	
	@Override
	public void start() {
		service.scheduleWithFixedDelay(this, duration, duration, durationUnit);
	}
	
	@Override
	public void run() {
		getWorkQueue().work();
	}
	
}
