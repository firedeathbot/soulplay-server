package com.soulplayps.util.worker;

/**
 * @author Jire
 */
public abstract class Worker implements Runnable {
	
	private final WorkQueue workQueue;
	
	public Worker(WorkQueue workQueue) {
		this.workQueue = workQueue;
	}
	
	public WorkQueue getWorkQueue() {
		return workQueue;
	}
	
	public abstract void start();
	
}
