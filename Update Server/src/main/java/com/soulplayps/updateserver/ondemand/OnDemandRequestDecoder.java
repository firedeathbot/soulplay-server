package com.soulplayps.updateserver.ondemand;

import com.soulplayps.updateserver.UpdateServer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author Jire
 */
public final class OnDemandRequestDecoder extends ByteToMessageDecoder {
	
	/**
	 * The maximum length of a chunk, in {@code byte}s.
	 */
	private static final int CHUNK_LENGTH = 500;
	
	private static final ByteBuf HANDSHAKE_RESPONSE
			= Unpooled.unreleasableBuffer(Unpooled.buffer(8).writeLong(8));
	
	private volatile boolean handshook;
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if (!handshook) {
			if (in.isReadable()) {
				int handshake = in.readUnsignedByte();
				if (handshake != 15) {
					ctx.close();
					return;
				}
				ctx.writeAndFlush(HANDSHAKE_RESPONSE, ctx.voidPromise());
				handshook = true;
			} else {
				ctx.close();
				return;
			}
		}
		
		boolean flush = false;
		while (in.readableBytes() >= 6) {
			int type = in.readUnsignedByte() + 1;
			int file = in.readInt();
			int priority = in.readUnsignedByte();
			writeArchive(ctx, type, file, false);
			flush = true;
		}
		if (flush) ctx.flush();
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
	
	public static void writeArchive(ChannelHandlerContext ctx, final int type, final int file, final boolean flush) {
		final ByteBuf buffer = UpdateServer.indexedFileSystem.getCachedFile(type, file);
		if (buffer == null || !buffer.isReadable()) {
			final ByteBuf nullResponse = ctx.alloc().buffer(11);
			nullResponse.writeByte(type - 1);
			nullResponse.writeInt(file);
			nullResponse.writeInt(0);
			nullResponse.writeShort(0);
			
			ctx.write(nullResponse, ctx.voidPromise());
		} else {
			buffer.markReaderIndex();
			try {
				final int length = buffer.readableBytes();
				
				final ByteBuf fileResponse = ctx.alloc().buffer(length + ((length / CHUNK_LENGTH) * 11));
				for (int chunk = 0; buffer.readableBytes() > 0; chunk++) {
					fileResponse.writeByte(type - 1);
					fileResponse.writeInt(file);
					
					fileResponse.writeInt(length);
					fileResponse.writeShort(chunk);
					
					final int chunkSize = Math.min(buffer.readableBytes(), CHUNK_LENGTH);
					fileResponse.writeBytes(buffer, chunkSize);
				}
				
				ctx.write(fileResponse, ctx.voidPromise());
			} finally {
				buffer.resetReaderIndex();
			}
		}
		
		if (flush) ctx.flush();
	}
	
}
