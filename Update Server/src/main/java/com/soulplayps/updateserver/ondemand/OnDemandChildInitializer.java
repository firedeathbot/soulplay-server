package com.soulplayps.updateserver.ondemand;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * @author Jire
 */
@Sharable
final class OnDemandChildInitializer extends ChannelInitializer<SocketChannel> {
	
	@Override
	protected void initChannel(SocketChannel ch) {
		ch.pipeline().addLast(new OnDemandRequestDecoder());
	}
	
}
