package com.soulplayps.updateserver.ondemand;

import com.soulplayps.updateserver.UpdateServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;

/**
 * @author Jire
 */
public final class OnDemandServerBootstrap extends UpdateServerBootstrap {
	
	private static final ChannelHandler CHILD_HANDLER = new OnDemandChildInitializer();
	
	public OnDemandServerBootstrap(EventLoopGroup group, Class<? extends ServerSocketChannel> channelClass) {
		super(group, channelClass, CHILD_HANDLER);
	}
	
}
