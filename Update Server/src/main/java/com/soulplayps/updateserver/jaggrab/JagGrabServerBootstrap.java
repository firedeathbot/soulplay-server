package com.soulplayps.updateserver.jaggrab;

import com.soulplayps.updateserver.UpdateServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;

/**
 * @author Jire
 */
public final class JagGrabServerBootstrap extends UpdateServerBootstrap {
	
	private static final ChannelHandler CHILD_HANDLER = new JagGrabChildInitializer();
	
	public JagGrabServerBootstrap(EventLoopGroup group, Class<? extends ServerSocketChannel> channelClass) {
		super(group, channelClass, CHILD_HANDLER);
	}
	
}
