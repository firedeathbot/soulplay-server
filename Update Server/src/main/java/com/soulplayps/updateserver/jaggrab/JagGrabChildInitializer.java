package com.soulplayps.updateserver.jaggrab;

import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;

import java.nio.charset.Charset;

/**
 * @author Graham
 * @author Jire
 */
@Sharable
final class JagGrabChildInitializer extends ChannelInitializer<SocketChannel> {
	
	/**
	 * A buffer with two line feed (LF) characters in it.
	 */
	private static final ByteBuf DOUBLE_LINE_FEED_DELIMITER
			= Unpooled.unreleasableBuffer(Unpooled.directBuffer(2));
	
	/**
	 * The character set used in the request.
	 */
	private static final Charset JAGGRAB_CHARSET = Charsets.US_ASCII;
	
	/**
	 * The maximum length of a request, in bytes.
	 */
	private static final int MAX_REQUEST_LENGTH = 8192;
	
	/**
	 * The request decoder.
	 */
	private static final ChannelHandler REQUEST_ENCODER = new StringToJagGrabResponseEncoder();
	
	/**
	 * Populates the double line feed buffer.
	 */
	static {
		DOUBLE_LINE_FEED_DELIMITER.writeByte(10).writeByte(10);
	}
	
	@Override
	protected void initChannel(SocketChannel ch) {
		ch.pipeline()
				.addLast(new DelimiterBasedFrameDecoder(MAX_REQUEST_LENGTH, DOUBLE_LINE_FEED_DELIMITER))
				.addLast(new StringDecoder(JAGGRAB_CHARSET))
				.addLast(REQUEST_ENCODER);
	}
	
}
