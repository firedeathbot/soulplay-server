package com.soulplayps.updateserver.jaggrab;

import com.soulplayps.updateserver.UpdateServer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.MessageToByteEncoder;
import org.apollo.cache.IndexedFileSystem;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

/**
 * @author Graham
 * @author Jire
 */
@Sharable
public final class StringToJagGrabResponseEncoder extends SimpleChannelInboundHandler<String> {
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String request) {
		if (request.startsWith("JAGGRAB /")) {
			String filePath = request.substring(8).trim();
			try {
				final ByteBuffer buffer = pathToByteBuffer(filePath);
				if (buffer != null) {
					final int length = buffer.remaining();
					
					final ByteBuf response = ctx.alloc().buffer(4 + length);
					response.writeInt(length);
					response.writeBytes(buffer);
					ctx.writeAndFlush(response, ctx.voidPromise());
					
					buffer.rewind();
				} else {
					ctx.close();
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
				ctx.close();
			}
		} else {
			throw new IllegalArgumentException("Corrupted request line.");
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
	}
	
	private static ByteBuffer pathToByteBuffer(String path) throws IOException {
		IndexedFileSystem fs = UpdateServer.indexedFileSystem;
		if (path.startsWith("/crc")) {
			return fs.getCrcTable();
		} else if (path.startsWith("/title")) {
			return fs.getFile(0, 1);
		} else if (path.startsWith("/config")) {
			return fs.getFile(0, 2);
		} else if (path.startsWith("/interface")) {
			return fs.getFile(0, 3);
		} else if (path.startsWith("/media")) {
			return fs.getFile(0, 4);
		} else if (path.startsWith("/versionlist")) {
			return fs.getFile(0, 5);
		} else if (path.startsWith("/textures")) {
			return fs.getFile(0, 6);
		} else if (path.startsWith("/wordenc")) {
			return fs.getFile(0, 7);
		} else if (path.startsWith("/sounds")) {
			return fs.getFile(0, 8);
		}
		return null;
	}
	
}
