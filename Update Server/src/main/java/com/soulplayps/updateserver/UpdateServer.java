package com.soulplayps.updateserver;

import com.soulplayps.updateserver.jaggrab.JagGrabServerBootstrap;
import com.soulplayps.updateserver.ondemand.OnDemandServerBootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apollo.cache.IndexedFileSystem;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;

/**
 * @author Jire
 */
public final class UpdateServer {
	
	static {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}
	
	private static final String CACHE_PATH = "data/cache";
	
	private static final int JAGGRAB_PORT = 43595;
	private static final int ONDEMAND_PORT = 43594;
	
	public static IndexedFileSystem indexedFileSystem;
	
	public static void main(String[] args) {
		final Logger logger = Logger.getLogger(UpdateServer.class.getName());
		
		logger.info("Initializing file system...");
		try {
			indexedFileSystem = new IndexedFileSystem(Paths.get(CACHE_PATH), true);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return;
		}
		
		/* Determine amount of threads to use */
		final int nativeThreads = Runtime.getRuntime().availableProcessors();
		
		int leftoverThreads = Math.max(nativeThreads - 2, 1);
		int highPriorityOnDemandThreads = (int) Math.max(Math.round(leftoverThreads * 0.61), 1);
		int networkThreads = Math.max(leftoverThreads - highPriorityOnDemandThreads, 1);
		
		/* Determine network parameters */
		boolean epoll = Epoll.isAvailable();
		EventLoopGroup eventLoopGroup = epoll ?
				new EpollEventLoopGroup(networkThreads)
				: new NioEventLoopGroup(networkThreads);
		Class<? extends ServerSocketChannel> channelClass =
				epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class;
		
		ServerBootstrap jagGrabBootstrap = new JagGrabServerBootstrap(eventLoopGroup, channelClass);
		ServerBootstrap onDemandBootstrap = new OnDemandServerBootstrap(eventLoopGroup, channelClass);
		
		try {
			jagGrabBootstrap.bind(JAGGRAB_PORT).get();
			logger.info("Bound JagGrab service to port " + JAGGRAB_PORT);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		try {
			onDemandBootstrap.bind(ONDEMAND_PORT).get();
			logger.info("Bound OnDemand service to port " + ONDEMAND_PORT);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
	
}
