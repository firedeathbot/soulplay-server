package com.soulplayps.updateserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;

/**
 * @author Jire
 */
public abstract class UpdateServerBootstrap extends ServerBootstrap {
	
	public UpdateServerBootstrap(EventLoopGroup group,
	                             Class<? extends ServerSocketChannel> channelClass,
	                             ChannelHandler childHandler) {
		group(group);
		channel(channelClass);
		
		childOption(ChannelOption.SO_KEEPALIVE, true);
		childOption(ChannelOption.TCP_NODELAY, true);
		
		childHandler(childHandler);
	}
	
}
