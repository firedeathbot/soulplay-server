package org.apollo.jagcached.net.ondemand;

import org.apollo.jagcached.fs.FileDescriptor;

/**
 * Represents a single 'on-demand' response.
 * @author Graham
 */
public final class OnDemandNullFile {
	
	/**
	 * The file descriptor.
	 */
	private final FileDescriptor fileDescriptor;
	
	/**
	 * Creates the 'on-demand' response.
	 * @param fileDescriptor The file descriptor.
	 * @param fileSize The file size.
	 * @param chunkId The chunk id.
	 * @param chunkData The chunk data.
	 */
	public OnDemandNullFile(FileDescriptor fileDescriptor) {
		this.fileDescriptor = fileDescriptor;
	}
	
	/**
	 * Gets the file descriptor.
	 * @return The file descriptor.
	 */
	public FileDescriptor getFileDescriptor() {
		return fileDescriptor;
	}

}
