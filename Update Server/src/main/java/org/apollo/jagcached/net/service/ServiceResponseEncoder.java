package org.apollo.jagcached.net.service;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * A {@link SimpleChannelInboundHandler} which encodes {@link ServiceResponse} messages.
 *
 * @author Graham
 */
public final class ServiceResponseEncoder extends MessageToByteEncoder<ServiceResponse> {
	@Override
	protected void encode(ChannelHandlerContext ctx, ServiceResponse msg, ByteBuf out) {
		out.writeLong(0);
	}
}
