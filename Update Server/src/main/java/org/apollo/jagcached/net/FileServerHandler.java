package org.apollo.jagcached.net;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.util.ReferenceCountUtil;
import org.apollo.jagcached.FileServer;
import org.apollo.jagcached.dispatch.RequestDispatcher;
import org.apollo.jagcached.net.jaggrab.JagGrabRequest;
import org.apollo.jagcached.net.ondemand.OnDemandRequest;
import org.apollo.jagcached.net.service.ServiceRequest;
import org.apollo.jagcached.net.service.ServiceResponse;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An {@link ChannelInboundHandlerAdapter} for the {@link FileServer}.
 *
 * @author Graham
 */
@Sharable
public final class FileServerHandler extends ChannelInboundHandlerAdapter {
	
	/**
	 * The logger for this class.
	 */
	private static final Logger logger = Logger.getLogger(FileServerHandler.class.getName());
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		try {
			if (msg instanceof ServiceRequest) {
				ServiceRequest request = (ServiceRequest) msg;
				if (request.getId() != ServiceRequest.SERVICE_ONDEMAND) {
					ctx.close();
				} else {
					ctx.writeAndFlush(new ServiceResponse(), ctx.voidPromise());
				}
			} else if (msg instanceof OnDemandRequest) {
				RequestDispatcher.dispatch(ctx.channel(), (OnDemandRequest) msg);
			} else if (msg instanceof JagGrabRequest) {
				RequestDispatcher.dispatch(ctx.channel(), (JagGrabRequest) msg);
			} else {
				throw new Exception("unknown message type");
			}
		} finally {
			ReferenceCountUtil.release(msg);
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		if (!(cause instanceof ReadTimeoutException)) {
			logger.log(Level.SEVERE, "Exception occured, closing channel...", cause);
		}
		ctx.channel().close();
	}
	
}
