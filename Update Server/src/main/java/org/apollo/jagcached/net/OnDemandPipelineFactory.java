package org.apollo.jagcached.net;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import org.apollo.jagcached.net.ondemand.OnDemandNullFileEncoder;
import org.apollo.jagcached.net.ondemand.OnDemandRequestDecoder;
import org.apollo.jagcached.net.ondemand.OnDemandResponseEncoder;
import org.apollo.jagcached.net.service.ServiceRequestDecoder;
import org.apollo.jagcached.net.service.ServiceResponseEncoder;

/**
 * A {@link ChannelInitializer} for the 'on-demand' protocol.
 *
 * @author Graham
 */
@Sharable
public final class OnDemandPipelineFactory extends ChannelInitializer<SocketChannel> {
	
	/**
	 * The file server event handler.
	 */
	private final FileServerHandler handler;
	
	/**
	 * Creates an 'on-demand' pipeline factory.
	 *
	 * @param handler The file server event handler.
	 */
	public OnDemandPipelineFactory(FileServerHandler handler) {
		this.handler = handler;
	}
	
	@Override
	protected void initChannel(SocketChannel ch) {
		ChannelPipeline pipeline = ch.pipeline();
		
		// decoders
		pipeline.addLast("serviceDecoder", new ServiceRequestDecoder());
		pipeline.addLast("decoder", new OnDemandRequestDecoder());
		
		// encoders
		pipeline.addLast("serviceEncoder", new ServiceResponseEncoder());
		pipeline.addLast(new OnDemandResponseEncoder());
		pipeline.addLast(new OnDemandNullFileEncoder());
		
		// handler
		pipeline.addLast("handler", handler);
	}
	
}
