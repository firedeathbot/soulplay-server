package org.apollo.jagcached.net.jaggrab;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

/**
 * A {@link SimpleChannelInboundHandler} for the JAGGRAB protocol.
 *
 * @author Graham
 */
public final class JagGrabRequestDecoder extends MessageToMessageDecoder<String> {
	
	@Override
	protected void decode(ChannelHandlerContext ctx, String str, List<Object> out) throws Exception {
		if (str.startsWith("JAGGRAB /")) {
			String filePath = str.substring(8).trim();
			out.add(new JagGrabRequest(filePath));
		} else {
			throw new Exception("corrupted request line");
		}
	}
	
}
