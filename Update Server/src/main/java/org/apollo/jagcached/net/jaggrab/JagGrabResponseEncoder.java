package org.apollo.jagcached.net.jaggrab;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * A {@link MessageToByteEncoder} for the JAGGRAB protocol.
 *
 * @author Graham
 */
public final class JagGrabResponseEncoder extends MessageToByteEncoder<JagGrabResponse> {
	
	@Override
	protected void encode(ChannelHandlerContext ctx, JagGrabResponse msg, ByteBuf out) throws Exception {
		ByteBuf buffer = msg.getFileData();
		try {
			int size = buffer.readableBytes();
			
			ByteBuf buf = ctx.alloc().buffer(size + 4);
			buf.writeInt(size);
			buf.writeBytes(buffer);
			
			ctx.writeAndFlush(buf, ctx.voidPromise());
		} finally {
			buffer.release();
		}
	}
	
}
