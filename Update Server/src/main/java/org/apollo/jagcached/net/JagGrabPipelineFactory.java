package org.apollo.jagcached.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import org.apollo.jagcached.net.jaggrab.JagGrabRequestDecoder;
import org.apollo.jagcached.net.jaggrab.JagGrabResponseEncoder;

import java.nio.charset.Charset;

/**
 * A {@link ChannelInitializer} for the JAGGRAB protocol.
 *
 * @author Graham
 */
@Sharable
public final class JagGrabPipelineFactory extends ChannelInitializer<SocketChannel> {
	
	/**
	 * The maximum length of a request, in bytes.
	 */
	private static final int MAX_REQUEST_LENGTH = 8192;
	
	/**
	 * The character set used in the request.
	 */
	private static final Charset JAGGRAB_CHARSET = Charset.forName("US-ASCII");
	
	/**
	 * A buffer with two line feed (LF) characters in it.
	 */
	private static final ByteBuf DOUBLE_LINE_FEED_DELIMITER
			= Unpooled.unreleasableBuffer(Unpooled.buffer(2).writeByte(10).writeByte(10));
	
	/**
	 * The file server event handler.
	 */
	private final FileServerHandler handler;
	
	/**
	 * Creates a {@code JAGGRAB} pipeline factory.
	 *
	 * @param handler The file server event handler.
	 */
	public JagGrabPipelineFactory(FileServerHandler handler) {
		this.handler = handler;
	}
	
	@Override
	protected void initChannel(SocketChannel ch) {
		ChannelPipeline pipeline = ch.pipeline();
		
		// decoders
		pipeline.addLast("framer", new DelimiterBasedFrameDecoder(MAX_REQUEST_LENGTH, DOUBLE_LINE_FEED_DELIMITER));
		pipeline.addLast("string-decoder", new StringDecoder(JAGGRAB_CHARSET));
		pipeline.addLast("jaggrab-decoder", new JagGrabRequestDecoder());
		
		// encoders
		pipeline.addLast("jaggrab-encoder", new JagGrabResponseEncoder());
		
		// handler
		pipeline.addLast("handler", handler);
	}
	
}
