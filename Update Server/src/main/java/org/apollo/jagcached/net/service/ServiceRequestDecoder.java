package org.apollo.jagcached.net.service;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * A {@link ByteToMessageDecoder} which decodes {@link ServiceRequest} messages.
 *
 * @author Graham
 */
public final class ServiceRequestDecoder extends ByteToMessageDecoder {
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Object> out) {
		if (buf.isReadable()) {
			ServiceRequest request = new ServiceRequest(buf.readUnsignedByte());
			
			ChannelPipeline pipeline = ctx.pipeline();
			pipeline.remove(this);
			
			out.add(request);
		}
	}
	
}
