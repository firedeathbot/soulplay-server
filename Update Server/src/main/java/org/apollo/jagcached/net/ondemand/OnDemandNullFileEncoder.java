package org.apollo.jagcached.net.ondemand;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.apollo.jagcached.fs.FileDescriptor;

/**
 * @author Leanbow
 */
public final class OnDemandNullFileEncoder extends MessageToByteEncoder<OnDemandNullFile> {
	@Override
	protected void encode(ChannelHandlerContext ctx, OnDemandNullFile resp, ByteBuf buf) {
		FileDescriptor fileDescriptor = resp.getFileDescriptor();
		buf.writeByte(fileDescriptor.getType() - 1);
		buf.writeInt(fileDescriptor.getFile());
		buf.writeInt(0);
		buf.writeShort(0);
	}
}
