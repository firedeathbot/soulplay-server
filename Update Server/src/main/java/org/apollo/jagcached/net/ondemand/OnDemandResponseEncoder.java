package org.apollo.jagcached.net.ondemand;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.MessageToByteEncoder;
import org.apollo.jagcached.fs.FileDescriptor;

/**
 * A {@link ChannelOutboundHandlerAdapter} for the 'on-demand' protocol.
 *
 * @author Graham
 */
public final class OnDemandResponseEncoder extends MessageToByteEncoder<OnDemandResponse> {
	
	@Override
	protected void encode(ChannelHandlerContext ctx, OnDemandResponse resp, ByteBuf out) {
		ByteBuf chunkData = resp.getChunkData();
		try {
			FileDescriptor fileDescriptor = resp.getFileDescriptor();
			out.writeByte(fileDescriptor.getType() - 1);
			out.writeInt(fileDescriptor.getFile());
			
			int fileSize = resp.getFileSize();
			int chunkId = resp.getChunkId();
			out.writeInt(fileSize);
			out.writeShort(chunkId);
			
			out.writeBytes(chunkData);
		} finally {
			chunkData.release();
		}
	}
	
}
