package org.apollo.jagcached.dispatch;

import java.io.IOException;
import java.nio.ByteBuffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import org.apollo.jagcached.fs.IndexedFileSystem;
import org.apollo.jagcached.net.jaggrab.JagGrabRequest;
import org.apollo.jagcached.net.jaggrab.JagGrabResponse;
import org.apollo.jagcached.resource.ResourceProvider;
import org.apollo.jagcached.resource.VirtualResourceProvider;

/**
 * A worker which services JAGGRAB requests.
 *
 * @author Graham
 */
public final class JagGrabRequestWorker extends RequestWorker<JagGrabRequest, ResourceProvider> {
	
	/**
	 * Creates the JAGGRAB request worker.
	 *
	 * @param fs The file system.
	 */
	public JagGrabRequestWorker(IndexedFileSystem fs) {
		super(new VirtualResourceProvider(fs));
	}
	
	@Override
	protected ChannelRequest<JagGrabRequest> nextRequest() throws InterruptedException {
		return RequestDispatcher.nextJagGrabRequest();
	}
	
	@Override
	protected void service(ResourceProvider provider, Channel channel, JagGrabRequest request) throws IOException {
		ByteBuffer buf = provider.get(request.getFilePath());
		if (buf == null) {
			channel.close();
		} else {
			ByteBuf wrapped = channel.alloc().buffer(buf.remaining());
			wrapped.writeBytes(buf);
			channel.writeAndFlush(new JagGrabResponse(wrapped)).addListener(ChannelFutureListener.CLOSE);
		}
	}
	
}
