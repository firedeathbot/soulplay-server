package org.apollo.jagcached.dispatch;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import org.apollo.jagcached.fs.FileDescriptor;
import org.apollo.jagcached.fs.IndexedFileSystem;
import org.apollo.jagcached.net.ondemand.OnDemandNullFile;
import org.apollo.jagcached.net.ondemand.OnDemandRequest;
import org.apollo.jagcached.net.ondemand.OnDemandResponse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A worker which services 'on-demand' requests.
 *
 * @author Graham
 */
public final class OnDemandRequestWorker extends RequestWorker<OnDemandRequest, IndexedFileSystem> {
	
	/**
	 * The maximum length of a chunk, in bytes.
	 */
	private static final int CHUNK_LENGTH = 500;
	
	/**
	 * Creates the 'on-demand' request worker.
	 *
	 * @param fs The file system.
	 */
	public OnDemandRequestWorker(IndexedFileSystem fs) {
		super(fs);
	}
	
	@Override
	protected ChannelRequest<OnDemandRequest> nextRequest() throws InterruptedException {
		return RequestDispatcher.nextOnDemandRequest();
	}
	
	@Override
	protected void service(IndexedFileSystem fs, Channel channel, OnDemandRequest request) throws IOException {
		FileDescriptor desc = request.getFileDescriptor();
		
		ByteBuffer buf = null;
		try {
			buf = fs.getFile(desc);
		} catch (Exception e) {
			/* empty */
		}
		
		int length = buf != null ? buf.remaining() : 0;
		
		if (length <= 0) {
			channel.writeAndFlush(new OnDemandNullFile(desc), channel.voidPromise());
			return;
		}
		
		for (int chunk = 0; buf.remaining() > 0; chunk++) {
			int chunkSize = buf.remaining();
			
			ByteBuf chunkData;
			if (chunkSize > CHUNK_LENGTH) { // only have to do double copy if too big chunk
				chunkSize = CHUNK_LENGTH;
				
				byte[] tmp = new byte[chunkSize];
				buf.get(tmp, 0, tmp.length);
				chunkData = channel.alloc().buffer(tmp.length);
				chunkData.writeBytes(tmp);
			} else {
				chunkData = channel.alloc().buffer(chunkSize);
				chunkData.writeBytes(buf);
			}
			
			OnDemandResponse response = new OnDemandResponse(desc, length, chunk, chunkData);
			channel.write(response, channel.voidPromise());
		}
		channel.flush();
	}
	
}
