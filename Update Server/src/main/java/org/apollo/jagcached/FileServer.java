package org.apollo.jagcached;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apollo.jagcached.dispatch.RequestWorkerPool;
import org.apollo.jagcached.net.FileServerHandler;
import org.apollo.jagcached.net.JagGrabPipelineFactory;
import org.apollo.jagcached.net.NetworkConstants;
import org.apollo.jagcached.net.OnDemandPipelineFactory;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The core class of the file server.
 *
 * @author Graham
 */
public final class FileServer {
	
	/**
	 * The logger for this class.
	 */
	private static final Logger logger = Logger.getLogger(FileServer.class.getName());
	
	/**
	 * The entry point of the application.
	 *
	 * @param args The command-line arguments.
	 */
	public static void main(String[] args) {
		try {
			new FileServer().start();
		} catch (Throwable t) {
			logger.log(Level.SEVERE, "Error starting server.", t);
		}
	}
	
	/**
	 * The request worker pool.
	 */
	private final RequestWorkerPool pool = new RequestWorkerPool();
	
	/**
	 * The file server event handler.
	 */
	private final FileServerHandler handler = new FileServerHandler();
	
	/**
	 * Starts the file server.
	 *
	 * @throws Exception if an error occurs.
	 */
	public void start() throws Exception {
		logger.info("Starting workers...");
		pool.start();
		
		logger.info("Starting services...");
		EventLoopGroup eventLoopGroup = Epoll.isAvailable() ? new EpollEventLoopGroup() : new NioEventLoopGroup();
		
		start("JAGGRAB", eventLoopGroup, new JagGrabPipelineFactory(handler), NetworkConstants.JAGGRAB_PORT);
		start("ondemand", eventLoopGroup, new OnDemandPipelineFactory(handler), NetworkConstants.SERVICE_PORT);
		
		logger.info("Ready for connections.");
	}
	
	/**
	 * Starts the specified service.
	 *
	 * @param name        The name of the service.
	 * @param initializer The channel initializer.
	 * @param port        The port.
	 */
	private void start(String name, EventLoopGroup group, ChannelInitializer<SocketChannel> initializer, int port) {
		SocketAddress address = new InetSocketAddress(port);
		
		logger.info("Binding " + name + " service to " + address + "...");
		
		ServerBootstrap bootstrap = new ServerBootstrap()
				.group(group)
				.channel(Epoll.isAvailable() ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
				.childOption(ChannelOption.SO_KEEPALIVE, true)
				.childOption(ChannelOption.TCP_NODELAY, true)
				.childHandler(initializer);
		if (Epoll.isAvailable()) bootstrap.childOption(ChannelOption.SO_REUSEADDR, true);
		bootstrap.bind(address);
	}
	
}
